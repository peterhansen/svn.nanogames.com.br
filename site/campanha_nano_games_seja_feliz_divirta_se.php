<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Nano Games - Campanha Seja Feliz, Divirta-se!</title>
<META NAME="Title" CONTENT="Nano Games - Campanha Seja Feliz, Divirta-se!">
<META NAME="Author" CONTENT="Multiplace - www.mplace.com.br - Luis Felipe Pinheiro Sym">
<META NAME="Subject" CONTENT="A Nano Games é uma empresa brasileira, desenvolvedora de jogose aplicativos java para celular, fundada no Rio de Janeiro em 2007.">
<meta name="keywords" content="biritômetro, Nano, Games, Mobile, Joselito Contra a Farofada, FullPower, Drag Race, Bob Esponja, Fábrica de siriburguer, biritometro, olimpiadas, mini-olimpiadas, barrigadas, ornamentais, ginastica, away, dança, dança away,levantamento de peso, peso, penalti mtv, penalti com cleston, penalti, cleeeston, bitita, jimmy, jimmy neutron, enigma das supernovas, Nick, MTV, Hermes e Renato, jogos, celular, jogos baixar, jogos celular, jogos on line, jogos downloads, jogos java, jogos para baixar, aplicativos para celular, downloads para celular, jogos java celular, jogos para java, jogos para claro, jogos para vivo, jogos aplicativos celular, jogos para c650, jogos para oi, jogos para tim, jogo para celulares, jogos para mc60, jogos para v300, jogos razr, jogos v3">
<META NAME="organization name" CONTENT="Nano Games - www.nanogames.com.br.">
<META name="document-classification" content="Mobile Services, jogos para celular, mobile studio, aplicativos para celular">
<META name="description" content="Uma campanha que a Nano Games está iniciando para tornar a sua vida mais alegre e divertida.">
<META NAME="Language" CONTENT="Português">
<meta name="robots" content="index,follow">

<!--[if lt IE 7]> <link href="css_ie.css" rel="stylesheet" type="text/css"> <![endif]-->
<!--[if !lt IE 7]><![IGNORE[--><![IGNORE[]]> <link href="css.css" rel="stylesheet" type="text/css"> <!--<![endif]-->
<!--[if !IE]>--> <link href="css.css" rel="stylesheet" type="text/css"> <!--<![endif]-->

<script src="javascript/AC_RunActiveContent.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="javascript/highlight.js"></script>
<script src="javascript/javascript.js" type="text/JavaScript"></script>

<script type="text/javascript">
var woopra_id = '1580895495';
</script>
<script src="http://static.woopra.com/js/woopra.js"></script>


<style type="text/css">
<!--
.style1 {
	color: #FFFF00;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
</head>

<body onLoad="MM_preloadImages('imagens/nanoca_blog.gif','imagens/orkut.gif','imagens/my_space.gif','imagens/facebook.gif')">

<div id="Barradegrade">

<div id="geral">
	<div class="espacotopico">
	
	<div> <? include ("banner_right.php"); ?><? include ("banner_left.php"); ?> </div>
	
    <div class="menutopico" style="padding:10px">
       <div class="busca"><form method="get" action="busca.php">                                       
                          <input type="text" name="zoom_query" size="20" style="vertical-align:middle;" />
                          <input type="image" style="vertical-align:middle;" src="imagens/buscar_but.gif" align="middle" width="52" height="20"/>
                           </form>
	   </div>
	   
	   <div style="float:right;  margin-top:-2px ;_margin-top:0px; *margin-top:0px ">
	   <a href="http://blogdananoca.blogspot.com/" target="_blank" onMouseOver="MM_swapImage('Nanoca Blog','','imagens/nanoca_blog.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/nanoca_blog_V.gif" name="Nanoca Blog" width="63" height="26" border="0"></a>	
	   <a href="http://www.orkut.com.br/Main#Community.aspx?cmm=40037651" target="_blank" onMouseOver="MM_swapImage('Orkut','','imagens/orkut.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/orkut_V.gif" name="Orkut" width="26" height="26" border="0"></a>
       <a href="http://groups.myspace.com/nanogames" target="_blank" onMouseOver="MM_swapImage('My Space','','imagens/my_space.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/my_space_V.gif" name="My Space" width="26" height="26" border="0"></a>
	   <a href="http://www.facebook.com/group.php?gid=8924566823" target="_blank" onMouseOver="MM_swapImage('Facebook','','imagens/facebook.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/facebook_V.gif" name="Facebook" width="26" height="26" border="0"></a>
	   <a href="http://www.nanogames.com.br/rss.xml" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Rss','','imagens/rss.gif',1)"><img src="imagens/rss_v.gif" name="Rss" width="26" height="26" border="0"></a>
	   </div>
	</div>
	</div>
	<div class="linhatopicocima">
  </div>    
    
	<div class="topicoconteudo">
    	<div class="linha">
   		  <div class="coluna" style="width:249px;"><span class="coluna" style="width:249px;">
   		    <? include ("logo.php"); ?>
   		  </span></div>
          <div class="coluna" style="margin-top:3px; margin-botton:4px; background-color: rgb(0, 74, 83); height: 86px; width:410px;">          
             <? include ("banners.php"); ?>
		  </div>
      </div>     
    </div>
    <div class="linhatopicobaixo">
    </div> 
    
    
    <div class="espacotopico">

   <div class="barramenu"  style="position:relative; z-index:10">	
    <? include ("menu_principal.php"); ?> 
    </div>
    </div>
    

    
    <div class="conteudo" style="padding: 14px;">
    <h1>Campanha Seja Feliz, Divirta-se!<br>
</h1>
    <p><span class="fonte">A  Nano Games está iniciando uma campanha para tornar a sua vida mais alegre e divertida.</span></p>
    <p><span class="fonte">A campanha<em> Seja Feliz, Divirta-se! </em> &nbsp;tem o objetivo de trazer pequenos momentos de alegria pra te aliviar das coisas que chateiam no dia-a-dia.</span></p>
    <p><span class="fonte"> Você sabia que sorrir é contagioso? Quanto mais você sorrir, mais pessoas estará influenciando para que sorriam também. Assim, você estará diminuindo seu stress e criando um ambiente mais leve e divertido, aumentando a sua qualidade de vida.</span></p>
    <p><span class="fonte"> Além disso, não tem antivírus que dê conta de um belo sorriso! Então não perca tempo: <strong>baixe já a sua imagem e infecte seus amigos</strong>!</span><br>
      </p>
    <table width="587" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr align="center" valign="top">
        <td width="139"><span class="style1">Wallpaper para <br>
          o seu Desktop</span></td>
        <td width="15">&nbsp;</td>
        <td width="124"><span class="style1">Wallpaper para<br>
          o seu Celular</span></td>
        <td width="112"><span class="style1">Perfil do<br> 
          Orkut</span></td>
        <td width="122"><span class="style1">Perfil do<br>
          MSN/Gtalk</span></td>
        <td width="75"><span class="style1">Emoticon</span></td>
      </tr>
      <tr align="center" valign="top">
        <td height="19">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr align="center" valign="top">
        <td height="53"><img src="imagens/wallpaper.gif" width="111" height="85"></td>
        <td>&nbsp;</td>
        <td><img src="imagens/cel_wallpaper.gif" width="65" height="85"></td>
        <td><a href="imagens/seja_feliz/perfil_orkut.jpg"><img src="imagens/perfil_orkut.gif" width="65" height="86" border="0"></a></td>
        <td><a href="imagens/seja_feliz/msn.jpg"><img src="imagens/msn.gif" width="58" height="57" border="0"></a></td>
        <td><a href="imagens/seja_feliz/44x44.gif"><img src="imagens/emoticon.gif" width="50" height="50" border="0"></a></td>
      </tr>
      <tr align="center" valign="middle">
        <td height="58" class="tamanho"><p align="center">
		<a href="imagens/seja_feliz/800_600.png">800 x 600<br>
		</a><a href="imagens/seja_feliz/1024_768.png">1024 x 768<br>
		</a><a href="imagens/seja_feliz/1280_800.png">1280 x 800<br>
		</a><a href="imagens/seja_feliz/1280_1024.png">1280 x 1024</a></p>
          </td>
        <td class="tamanho">&nbsp;</td>
        <td class="tamanho"><p align="center">          <a href="imagens/seja_feliz/128_110.png">128 x 110 - </a><a href="imagens/seja_feliz/128_128.png">128 x 128</a>
              <a href="imagens/seja_feliz/128_160.png">128 x 160 - </a> <a href="imagens/seja_feliz/176_220.png">176 x 220</a>
              <a href="imagens/seja_feliz/240_260.png">240 x 260 - </a> <a href="imagens/seja_feliz/240_320.png">240 x 320</a>
              <a href="imagens/seja_feliz/320_240.png">320 x 240 - </a> <a href="imagens/seja_feliz/500_500.png">500 x 500 </a></p>
          </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <div align="right">
      <p>&nbsp;</p>
      <p><a href="#topo">:: Topo ::</a></p>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  	     
          <span class="fonte"></span>
            <br>
      <span class="fonte"><strong>Jogos e Aplicativos desenvolvidos:</strong></span><br>
&nbsp;&nbsp;<br>
				 <? include ("link_jogos.php"); ?> 
            <br>
    </div>
    

  <div class="rodape" style="height:50px ">
  <span class="cabecalho"><a href="index.php">Home</a> :: <a href="nano-games-desenvolvedora-de-jogos-celular.php">Sobre a Nano</a> :: <a href="index.php">Produtos </a>:: <a href="noticias-sobre-a-nano-games-desenvolvedora-de-jogos-celular.php">Notícias </a>:: <a href="parceiros-da-nano-games-desenvolvedora-de-jogos-celular.php">Parceiros </a>:: <a href="contato-nano-games.php">Contato</a></span>
    <div class="fonte"align="center" style="padding-top:10px "><? include ("rodape.php"); ?></div>
  </div>
    
    
</div>
</div>

</body>
</html>
