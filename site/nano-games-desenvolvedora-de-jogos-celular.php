<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Nano Games - Jogos e Aplicativos para Celular!</title>
<META NAME="Title" CONTENT="Nano Games - Jogos e Aplicativos para Celular!">
<META NAME="Author" CONTENT="Multiplace - www.mplace.com.br - Luis Felipe Pinheiro Sym">
<META NAME="Subject" CONTENT="A Nano Games é uma empresa brasileira, desenvolvedora de jogose aplicativos java para celular, fundada no Rio de Janeiro em 2007.">
<meta name="keywords" content="biritômetro, Nano, Games, Mobile, Joselito Contra a Farofada, FullPower, Drag Race, Bob Esponja, Fábrica de siriburguer, biritometro, olimpiadas, mini-olimpiadas, barrigadas, ornamentais, ginastica, away, dança, dança away,levantamento de peso, peso, penalti mtv, penalti com cleston, penalti, cleeeston, bitita, jimmy, jimmy neutron, enigma das supernovas, Nick, MTV, Hermes e Renato, jogos, celular, jogos baixar, jogos celular, jogos on line, jogos downloads, jogos java, jogos para baixar, aplicativos para celular, downloads para celular, jogos java celular, jogos para java, jogos para claro, jogos para vivo, jogos aplicativos celular, jogos para c650, jogos para oi, jogos para tim, jogo para celulares, jogos para mc60, jogos para v300, jogos razr, jogos v3">
<META NAME="organization name" CONTENT="Nano Games - www.nanogames.com.br.">
<META name="document-classification" content="Mobile Services, jogos para celular, mobile studio, aplicativos para celular">
<META name="description" content="Nosso objetivo é transformar mesmo os jogos mais simples em uma nova experiência de entretenimento móvel, produzindo jogos criativos com um padrão internacional de qualidade.">
<META NAME="Language" CONTENT="Português">
<meta name="robots" content="index,follow">

<!--[if lt IE 7]> <link href="css_ie.css" rel="stylesheet" type="text/css"> <![endif]-->
<!--[if !lt IE 7]><![IGNORE[--><![IGNORE[]]> <link href="css.css" rel="stylesheet" type="text/css"> <!--<![endif]-->
<!--[if !IE]>--> <link href="css.css" rel="stylesheet" type="text/css"> <!--<![endif]-->

<script src="javascript/AC_RunActiveContent.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="javascript/highlight.js"></script>
<script src="javascript/javascript.js" type="text/JavaScript"></script>

<script type="text/javascript">
var woopra_id = '1580895495';
</script>
<script src="http://static.woopra.com/js/woopra.js"></script>

</head>

<body onLoad="MM_preloadImages('imagens/nanoca_blog.gif','imagens/orkut.gif','imagens/my_space.gif','imagens/facebook.gif')">
<a name="topo"></a>
<div id="Barradegrade">

<div id="geral">
	<div class="espacotopico">
	
	<div> <? include ("banner_right.php"); ?><? include ("banner_left.php"); ?> </div>
	
    <div class="menutopico" style="padding:10px">
       <div class="busca"><form method="get" action="busca.php">                                       
                          <input type="text" name="zoom_query" size="20" style="vertical-align:middle;" />
                          <input type="image" style="vertical-align:middle;" src="imagens/buscar_but.gif" align="middle" width="52" height="20"/>
                           </form>
	   </div>
	   
	   <div style="float:right;  margin-top:-2px ;_margin-top:0px; *margin-top:0px ">
	   <a href="http://blogdananoca.blogspot.com/" target="_blank" onMouseOver="MM_swapImage('Nanoca Blog','','imagens/nanoca_blog.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/nanoca_blog_V.gif" name="Nanoca Blog" width="63" height="26" border="0"></a>	
	   <a href="http://www.orkut.com.br/Main#Community.aspx?cmm=40037651" target="_blank" onMouseOver="MM_swapImage('Orkut','','imagens/orkut.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/orkut_V.gif" name="Orkut" width="26" height="26" border="0"></a>
       <a href="http://groups.myspace.com/nanogames" target="_blank" onMouseOver="MM_swapImage('My Space','','imagens/my_space.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/my_space_V.gif" name="My Space" width="26" height="26" border="0"></a>
	   <a href="http://www.facebook.com/group.php?gid=8924566823" target="_blank" onMouseOver="MM_swapImage('Facebook','','imagens/facebook.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagens/facebook_V.gif" name="Facebook" width="26" height="26" border="0"></a>
	   <a href="http://www.nanogames.com.br/rss.xml" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Rss','','imagens/rss.gif',1)"><img src="imagens/rss_v.gif" name="Rss" width="26" height="26" border="0"></a>
	   </div>
	</div>
	</div>
	<div class="linhatopicocima">
  </div>    
    
	<div class="topicoconteudo">
    	<div class="linha">
   		  <div class="coluna" style="width:249px;"><span class="coluna" style="width:249px;">
   		    <? include ("logo.php"); ?>
   		  </span></div>
          <div class="coluna" style="margin-top:3px; margin-botton:4px; background-color: rgb(0, 74, 83); height: 86px; width:410px;">          
              <? include ("banners.php"); ?>
		  </div>
      </div>     
    </div>
    <div class="linhatopicobaixo">
    </div> 
    
    
    <div class="espacotopico">

   <div class="barramenu"  style="position:relative; z-index:10">	
    <? include ("menu_principal.php"); ?> 
    </div>
    </div>
    

    
    <div class="conteudo" style="padding: 14px;">
    <h1>Sobre a Nano Games<br>
</h1><br>
<span class="fonte">A Nano Games &eacute; uma empresa brasileira, fundada no Rio de Janeiro em 2007. Nossa equipe atua h&aacute; mais de 4 anos no desenvolvimento de jogos e aplicativos para celular, tendo participado diretamente da cria&ccedil;&atilde;o de conceito, desenvolvimento e porte de mais de 20 jogos e aplicativos m&oacute;veis.<br><br>Nosso objetivo &eacute; transformar mesmo os jogos mais simples em uma nova experi&ecirc;ncia de entretenimento m&oacute;vel, produzindo jogos criativos com um padr&atilde;o internacional de qualidade.</span><br>
<br>

    <div align="right"><a href="#topo">:: Topo ::</a></div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  	     
          <span class="fonte"></span>
            <br>
      <span class="fonte"><strong>Jogos e Aplicativos desenvolvidos:</strong></span><br>
&nbsp;&nbsp;<br>
				 <? include ("link_jogos.php"); ?> 
            <br>
    </div>
    

  <div class="rodape" style="height:50px ">
  <span class="cabecalho"><a href="index.php">Home</a> :: <a href="nano-games-desenvolvedora-de-jogos-celular.php">Sobre a Nano</a> :: <a href="index.php">Produtos </a>:: <a href="noticias-sobre-a-nano-games-desenvolvedora-de-jogos-celular.php">Notícias </a>:: <a href="parceiros-da-nano-games-desenvolvedora-de-jogos-celular.php">Parceiros </a>:: <a href="contato-nano-games.php">Contato</a></span>
    <div class="fonte"align="center" style="padding-top:10px "><? include ("rodape.php"); ?></div>
  </div>
    
    
</div>
</div>

</body>
</html>
