// How long will the banner be visible
var displayTime = 5000;

// The cycle of showing and hidding the banner
function showBanner(){
    $('.banner_switch').fadeIn(1500, function(){
        setTimeout('hideBanner()', displayTime);
    });
}

function hideBanner(){
    $('.banner_switch').fadeOut(1500, function(){
        setTimeout('showBanner()', displayTime);
    });
}

// Starts the display intelligence when the page is ready
$(document).ready(function(){
    setTimeout('showBanner()', displayTime);
});