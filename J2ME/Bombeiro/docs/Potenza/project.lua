-- descritor para conversão de arquivos para o formato Potenza
-- Peter Hansen

-- exemplos de aparelhos-chefe com poucos caracteres
-- gradiente gf690: grgf690
-- motorola v3: mtv3
-- nokia n76: nkn76
-- samsung d820: sgd820
-- samsung c420: c420
-- lg me970: lgme970
-- lg mg155: lgmg155

-- nome do aplicativo/jogo
APP_NAME = [[JimmySupernovas]]

-- caminho do arquivo original de especificação do aplicativo J2ME
PATH_APP_SPEC = [[326002-EST001-NanoGames.doc]]

-- caminho do formulário original de aprovação de aplicações Vivo Downloads
PATH_FAAVD = [[FAAVD_supportcomm.doc]]

-- caminho do arquivo original de solicitação de teste
PATH_SOL = [[326002-SOL001.01.1.xls]]

VERSIONS = {
	{
		-- big standard
		  ['jad'] = [[jnes_big.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.0]]
		, ['device'] = [[sew910]]
	},
	{
		-- big icon 60x40
		  ['jad'] = [[jnes_big_60x40.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.11]]
		, ['device'] = [[mta1200]]
	},
	{
		-- big samsung
		  ['jad'] = [[jnes_big_sam.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.1]]
		, ['device'] = [[sgd836]]
	},
	{
		-- landscape samsung
		  ['jad'] = [[jnes_lnd_sam.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.2]]
		, ['device'] = [[sgu106]]
	},
	{
		-- big flush graphics
		  ['jad'] = [[jnes_bfg.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.10]]
		, ['device'] = [[lgme970]]
	},
	{
		-- medium standard
		  ['jad'] = [[jnes_med.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.3]]
		, ['device'] = [[mtv3]]
	},
	{
		-- small standard
		  ['jad'] = [[jnes_sml.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.5]]
		, ['device'] = [[sew300]]
	},
	{
		-- small samsung
		  ['jad'] = [[jnes_sml_sam.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.9]]
		, ['device'] = [[sge256]]
	},
	{
		-- small nokia S40
		  ['jad'] = [[jnes_sml_s40.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.7]]
		, ['device'] = [[nk6061]]
	},
}

