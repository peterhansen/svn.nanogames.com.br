package jogo;
/*
 * MeuMidlet.java
 *
 * Created on June 15, 2007, 3:45 PM
 *
 */

/**
 *
 * @author malvezzi
 */
//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import net.rim.device.api.ui.Keypad;
import telas.Background;
import telas.TelaDeFimDeJogo;
import telas.TelaDeSplash;
import telas.TelaDeJogo;
import telas.TelaDeRecorde;


/**
 *
 * @author  malvezzi
 * @version
 */
public final class MeuMidlet extends AppMIDlet implements MenuListener, Constants {

	/** Pontua��o atual do jogador */
	private static int score;

	/** Posi��o em que a �ltima pontua��o entrar� na lista de recordes */
	public static byte lastScoreEntryIndex = TOTAL_RECORDS;

	/** Recordes armazenados */
	public static final int topRecords[] = new int[ TOTAL_RECORDS ];

	/** Fonte principal da aplica��o */
	public static ImageFont fontMain;

	/** Armazena a tela de jogo que ser� carregada na op��o "continuar" do menu principal */
	public static Drawable saveScreen;

	/** Objeto respons�vel por salvar e carregar do arquivo de dados os recordes do jogo */
	public static HighScoreManager highScoreManager = new HighScoreManager();

	/** Array de �cones de navega��o*/
	private static Drawable icons[];

	private static final int LOW_MEMORY_LIMIT = 900000;
	private static boolean lowMemory;

	//#if BLACKBERRY_API == "true"
//# 	private static boolean showRecommendScreen = true;
	//#endif

	/** Armazena o �ltimo momento em que houve uma troca de tela */
	private boolean changingScreens = false;

	private Background bkg;

	//#ifdef ReleaseSmall_Sagem_Gradiente
//# 		public MeuMidlet()
//# 		{
//# 			super( VENDOR_SAGEM_GRADIENTE, MAX_FRAME_TIME_DEFAULT );
//# 		}
	//#elifdef FORCE_MOTOROLA
//# 		public MeuMidlet()
//# 		{
//# 			super( VENDOR_MOTOROLA, MAX_FRAME_TIME_DEFAULT );
//# 		}
	//#endif

	
	public static final boolean isLowMemory() {
		return lowMemory;
	}

	
	protected final void loadResources() throws Exception {
//		switch ( getVendor() ) {
//			case VENDOR_SONYERICSSON:
//			case VENDOR_SIEMENS:
//			case VENDOR_BLACKBERRY:
//			case VENDOR_HTC:
//			break;
//
//			default:
//				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
//		}

		// Deixa o fundo preto
		manager.setBackgroundColor( 0 );

		// Aloca as fontes utilizadas pelo jogo
		fontMain = ImageFont.createMultiSpacedFont( FONT_MAIN_IMG, FONT_MAIN_DAT );
		fontMain.setCharOffset( -1 );

		loadTexts( TEXT_TOTAL, PATH_TEXTS );

		// Carrega todos os recursos do jogo
		loadResourcesFull();

		// Determina o idioma a ser utilizado pelo jogo
//		setLanguage( LANGUAGE_PORTUGUESE );


	//#if BLACKBERRY_API == "true"
//# 
//# 			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
//# 			int[][] keys = null;
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 },
//# 						{ 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 },
//# 						{ 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 },
//# 						{ 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 },
//# 						{ 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 },
//# 						{ 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 },
//# 						{ 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 },
//# 						{ 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 },
//# 						{ 'N', ScreenManager.KEY_NUM8 },
//# 
//# 						{ 'e', ScreenManager.KEY_NUM1 },
//# 						{ 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 },
//# 						{ 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 },
//# 						{ 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 },
//# 						{ 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 },
//# 						{ 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 },
//# 						{ 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 },
//# 						{ 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR },
//# 						{ 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR },
//# 						{ 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR },
//# 						{ 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR },
//# 						{ 'S', ScreenManager.KEY_STAR },
//# 
//# 						{ '0', ScreenManager.KEY_NUM0 },
//# 						{ ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//# 
//# 				default:
//# 					keys = new int[][] {
//# 						{ 'w', ScreenManager.KEY_NUM1 },
//# 						{ 'W', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM3 },
//# 						{ 'R', ScreenManager.KEY_NUM3 },
//# 						{ 'z', ScreenManager.KEY_NUM7 },
//# 						{ 'Z', ScreenManager.KEY_NUM7 },
//# 						{ 'c', ScreenManager.KEY_NUM9 },
//# 						{ 'C', ScreenManager.KEY_NUM9 },
//# 						{ 'e', ScreenManager.KEY_NUM2 },
//# 						{ 'E', ScreenManager.KEY_NUM2 },
//# 						{ 's', ScreenManager.KEY_NUM4 },
//# 						{ 'S', ScreenManager.KEY_NUM4 },
//# 						{ 'd', ScreenManager.KEY_NUM5 },
//# 						{ 'D', ScreenManager.KEY_NUM5 },
//# 						{ 'f', ScreenManager.KEY_NUM6 },
//# 						{ 'F', ScreenManager.KEY_NUM6 },
//# 						{ 'x', ScreenManager.KEY_NUM8 },
//# 						{ 'X', ScreenManager.KEY_NUM8 },
//# 
//# 						{ 'y', ScreenManager.KEY_NUM1 },
//# 						{ 'Y', ScreenManager.KEY_NUM1 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'b', ScreenManager.KEY_NUM7 },
//# 						{ 'B', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'u', ScreenManager.UP },
//# 						{ 'U', ScreenManager.UP },
//# 						{ 'h', ScreenManager.LEFT },
//# 						{ 'H', ScreenManager.LEFT },
//# 						{ 'j', ScreenManager.FIRE },
//# 						{ 'J', ScreenManager.FIRE },
//# 						{ 'k', ScreenManager.RIGHT },
//# 						{ 'K', ScreenManager.RIGHT },
//# 						{ 'n', ScreenManager.DOWN },
//# 						{ 'N', ScreenManager.DOWN },
//# 					 };
//# 			}
//# 			for ( byte i = 0; i < keys.length; ++i )
//# 				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
//# 
//# 			// em aparelhos antigos (e/ou com vers�es de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms n�o recebe os eventos de teclado corretamente, sumindo somente ap�s a aplica��o ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
//# 
	//#elif SCREEN_SIZE == "MEDIUM"

			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
			final int[][] keys = {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );

	//#endif


		// A tela inicial � a tela de sele��o de idioma
		setScreen( SCREEN_LOAD_RESOURCES );
	}


	private final boolean loadResourcesFull() {
		try {
			// Aloca, configura e determina o background comum a todas as telas do jogo
			bkg = new Background();
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			manager.setBackground( bkg, true );
			manager.setBackgroundColor( -1 );
			Thread.yield();

			// Aloca os �cones de navega��o
			final byte TOTAL_ICONS = 3;
			icons = new Drawable[ TOTAL_ICONS ];
			final String[] iconName = new String[] { IMG_PATH_ICON_OK, IMG_PATH_ICON_BACK, IMG_PATH_ICON_PAUSE };

			for ( byte i = 0; i < TOTAL_ICONS; ++i ) {
				icons[i] = new DrawableImage( IMG_PATH_ICONS + iconName[i] );
				Thread.yield();
			}

			//#if LOG == "true"
//# 				// Se n�o existir a database, tudo bem, n�o ir� disparar uma exce��o pois h� um catch interno.
//# 				// O que queremos � poder utilizar o Logger desde o in�cio da aplica��o
//# 				Logger.setRMS( DATABASE_NAME, DATABASE_IDX_LOG );
//# 				Logger.loadLog( DATABASE_NAME, DATABASE_IDX_LOG );
			//#endif

			try {
				// Se ainda n�o existir uma base de dados para a grava��o das op��es de jogo, cria uma
				if ( !createDatabase( DATABASE_NAME, DATABASE_IDX_TOTAL ) ) {
					// Carrega os recordes do jogo
					loadData( DATABASE_NAME, DATABASE_IDX_RECORD, highScoreManager );
				}
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif

				// N�o fazemos nada aqui. Se n�o conseguiu carregar os sons e/ou os recordes, continuaremos assim
				// mesmo. Serve apenas para n�o entrarmos no catch externo, o que causaria o abandono da aplica��o
			}

			// Tenta alocar o objeto de som
			try {
				// Inicializa o MediaPlayer
				MediaPlayer.init( DATABASE_NAME, DATABASE_IDX_SOUND_OPTIONS, new String[] { SOUND_PATH_DIR + SOUND_PATH_THEME,
																							SOUND_PATH_DIR + SOUND_PATH_CLEARED,
																							SOUND_PATH_DIR + SOUND_PATH_GAMEOVER
						} );
			} catch ( Exception ex ) {
				// N�o fazemos nada aqui. Se n�o conseguiu carregar os sons, continuaremos assim mesmo. Serve
				// apenas para n�o entrarmos no catch externo, o que causaria o abandono da aplica��o
				//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
			}
			return true;
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif

			// Se entrou nesse catch � porque alguma coisa necess�ria n�o pode ser alocada. Logo, abandonamos a
			// aplica��o
			exit();
			return false;
		}
	}


	public static final Drawable loadCursor() throws Exception {
		final Sprite cursor = new Sprite( PATH_IMAGES + "c" );
		cursor.defineReferencePixel( Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_RIGHT );
		return cursor;
	}


	public static final Background getBackground() {
		return ( ( MeuMidlet ) instance ).bkg;
	}


	public Drawable criaJogo() throws Throwable {
		final TelaDeJogo jogo = new TelaDeJogo();
		jogo.startLevelOne();
		jogo.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		return jogo;
	}


	public static Drawable criaRecorde() throws Exception {
		TelaDeRecorde tela = new TelaDeRecorde();
		tela.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		return tela;
	}


	private static final Drawable[] getHelpScreenSpecialChars() throws Exception {
		// Cria o array para conter os caracteres especiais
		final byte N_SPECIAL_CHARS = 2;
		Drawable[] specialChars = new Drawable[ N_SPECIAL_CHARS ];

		// Cria os sprites
		final byte N_SUPERNOVA_STATES = 4;
		final byte DIST_BETWEEN_SUPERNOVAS = 5;
		final UpdatableGroup supernovas = new UpdatableGroup( N_SUPERNOVA_STATES );

		final Supernova temp = Supernova.loadFrameSet();

		int totalWidth = 0;
		for ( byte i = 0; i < N_SUPERNOVA_STATES; ++i ) {
			final Supernova s = new Supernova( temp );
			supernovas.insertDrawable( s );
			s.setState( STATE_B1 + i );
			s.setPosition( totalWidth, 0 );

			totalWidth += s.getWidth() + DIST_BETWEEN_SUPERNOVAS;
		}
		// +2 = para compensar o tamanho da menor supernova dando uma melhor id�ia de centraliza��o das imagens
		supernovas.setSize( totalWidth - DIST_BETWEEN_SUPERNOVAS + 2, temp.getHeight() );

		// Determina o vetor de caracteres especiais
		specialChars[0] = supernovas;
		specialChars[1] = new AjudaSupernovaExplodindo( temp, SupernovaShard.loadFrameSet() );

		// Retorna o array criado
		return specialChars;
	}

	// Determina que �cone deve ser utilizado para representar a softKey

	public static final void setSoftKeyIcon( byte softKey, int iconIndex ) {
		( ( MeuMidlet ) instance ).manager.setSoftKey( softKey, iconIndex < 0 ? null : icons[iconIndex] );
	}


	public static final void setScore( int score ) {
		if ( score > MAX_POINTS ) {
			score = MAX_POINTS;
		} else if ( score < 0 ) {
			score = 0;
		}

		MeuMidlet.score = score;
	}


	public static final void increaseScore( int diff ) {
		setScore( score + diff );
	}


	public static final int getScore() {
		return score;
	}


	public static final int getCurrLanguageIndex() {
		return language;
	}


	/**
	 * Evita que duas threads tentem mudar de tela ao mesmo tempo
	 * @see #unlockScreenChange()
	 */
	private synchronized final void lockScreenChange() {
		while ( changingScreens ) {
			try {
				wait();
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
			}
		}
		changingScreens = true;
	}


	/**
	 * Libera a troca de telas
	 * @see #lockScreenChange()
	 */
	private synchronized final void unlockScreenChange() {
		changingScreens = false;
		notifyAll();
	}


	private String getAppVersion() {
		String version = getAppProperty( "MIDlet-Version" );
		if ( version == null ) {
			version = "0.0.0";
		}
		return "<ALN_H>VERS�O " + version;
	}


	public static final Point getIconSize() {
		return icons[ 0 ].getSize();
	}


	protected int changeScreen( int screen ) throws Exception {
		final MeuMidlet midlet = ( MeuMidlet ) instance;

		// Trava a troca de telas
		lockScreenChange();

		if ( screen != currentScreen ) {
			Drawable nextScreen = null;

			boolean maskVisible = true;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;

			int softKeyVisibleTime = 0;

			try {
				switch ( screen ) {
					case SCREEN_LOAD_GAME:
						nextScreen = new LoadScreen( midlet, SCREEN_JOGO );
						break;

					case SCREEN_MAIN_MENU:
						BasicMenu menu = null;

						if ( MediaPlayer.isVibrationSupported() ) {
							if ( saveScreen == null ) {
								menu = new BasicMenu( this, SCREEN_MAIN_MENU, fontMain, new int[] {
									TEXT_NEW_GAME,
									TEXT_TURN_VIBRATION_OFF,
									TEXT_RECORDS,
									TEXT_HELP,
									TEXT_CREDITS,
									TEXT_EXIT
								}, MENUS_ITEMS_SPACING, 0, OPT_EXIT );
								
								( ( Label ) menu.getDrawable( 1 ) ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
							} else {
								menu = new BasicMenu( this, SCREEN_MAIN_MENU, fontMain, new int[] {
									TEXT_NEW_GAME,
									TEXT_CONTINUE,
									TEXT_TURN_VIBRATION_OFF,
									TEXT_RECORDS,
									TEXT_HELP,
									TEXT_CREDITS,
									TEXT_EXIT
								}, MENUS_ITEMS_SPACING, 0, OPT_EXIT );

								( ( Label ) menu.getDrawable( 2 ) ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
							}
						} else {
							if ( saveScreen == null ) {
								menu = new BasicMenu( this, SCREEN_MAIN_MENU, fontMain, new int[] {
									TEXT_NEW_GAME,
									TEXT_RECORDS,
									TEXT_HELP,
									TEXT_CREDITS,
									TEXT_EXIT
								}, MENUS_ITEMS_SPACING, 0, OPT_EXIT );
							} else {
								menu = new BasicMenu( this, SCREEN_MAIN_MENU, fontMain, new int[] {
									TEXT_NEW_GAME,
									TEXT_CONTINUE,
									TEXT_RECORDS,
									TEXT_HELP,
									TEXT_CREDITS,
									TEXT_EXIT
								}, MENUS_ITEMS_SPACING, 0, OPT_EXIT );
							}
						}

						menu.setCursor( loadCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.LEFT | Graphics.VCENTER );

						nextScreen = menu;
						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;

						break;

					case SCREEN_JOGO:
						saveScreen = null;
						System.gc();

						nextScreen = criaJogo();
						saveScreen = nextScreen;

						indexSoftRight = MeuMidlet.ICON_PAUSE;

						maskVisible = false;

						MediaPlayer.stop();
						MediaPlayer.free();

						break;

					case SCREEN_CONTINUE:
						MediaPlayer.stop();
						MediaPlayer.free();
					// N�o tem break mesmo!!!

					case SCREEN_GAMEBACK:
						nextScreen = saveScreen;
						indexSoftRight = MeuMidlet.ICON_PAUSE;

						maskVisible = false;

						break;

					//#if LOG == "true"
					//# 				case SCREEN_LOG:
					//# 					nextScreen = Logger.getLogScreen( this, SCREEN_LOG, fontMain );
					//# 					break;
					//#endif

					case SCREEN_RECORD:
						nextScreen = criaRecorde();

						indexSoftRight = MeuMidlet.ICON_BACK;

						break;

					case SCREEN_HELP:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, fontMain, getText( TEXT_HELP_TEXT ) + "\n\n" + getAppVersion() + "\n\n", false, MeuMidlet.getHelpScreenSpecialChars() );

						final byte scrollWidth = 5;
						final Pattern scrollBkg = ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollFull();
						scrollBkg.setFillColor( ( 40 << 16 ) | ( 75 << 8 ) | ( 35 ) );
						scrollBkg.setSize( scrollWidth, 0 );

						final Pattern scrollPage = ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollPage();
						scrollPage.setFillColor( ( 69 << 16 ) | ( 154 << 8 ) | ( 0 ) );
						scrollPage.setSize( scrollWidth, 0 );

						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_CREDITOS:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, fontMain, getText( TEXT_CREDITS_TEXT ) + getMIDletVersion(), true );

						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_SPLASH_NANO:
						//#if SCREEN_SIZE == "SMALL"
//# 							nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, "/splash/", TEXT_SPLASH_NANO, SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						//#else
							nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, "/splash/", TEXT_SPLASH_NANO, SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						//#endif
						break;

					case SCREEN_OPTIONS:
						if ( MediaPlayer.isVibrationSupported() ) {
							nextScreen = new BasicOptionsScreen( midlet, screen, fontMain, new int[] {
										TEXT_TURN_SOUND_OFF,
										TEXT_TURN_VIBRATION_OFF,
										TEXT_BACK }, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
						} else {
							nextScreen = new BasicOptionsScreen( midlet, screen, fontMain, new int[] {
										TEXT_TURN_SOUND_OFF,
										TEXT_BACK, }, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, TEXT_TURN_VIBRATION_ON );
						}
//						nextScreen = new OptionsScreen( this, fontMain, SCREEN_OPTIONS );

						( ( BasicOptionsScreen ) nextScreen ).setCursor( loadCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_SPLASH_MARCA:
//						nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, -1, IMG_PATH_SPLASH, IMG_PATH_SPLASH + "nick.png", TEXT_SPLASH_NICK );
						break;

					case SCREEN_SPLASH_GAME:
						nextScreen = new TelaDeSplash();
						break;

					case SCREEN_YN_BACKGAME:
						saveScreen = manager.getCurrentScreen();
						nextScreen = new BasicConfirmScreen( this, SCREEN_YN_BACKGAME, fontMain, loadCursor(), TEXT_LEAVEGAME, TEXT_YES, TEXT_NO, true );
						( ( BasicConfirmScreen ) nextScreen ).setSpacing( MENUS_ITEMS_SPACING << 1, MENUS_ITEMS_SPACING );
						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_YN_SOUND:
//						nextScreen = new BasicConfirmScreen( this, SCREEN_YN_SOUND, fontMain, LoadCursor(), TEXT_WITH_SOUND, TEXT_YES, TEXT_NO, true );
//						( ( BasicConfirmScreen ) nextScreen ).setSpacing( MENUS_ITEMS_SPACING << 1, MENUS_ITEMS_SPACING );
//						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
//
//
//						indexSoftLeft = MeuMidlet.ICON_OK;
//						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_FIMDEJOGO:
						saveScreen = null;
						nextScreen = new TelaDeFimDeJogo( this, SCREEN_FIMDEJOGO );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;

						MediaPlayer.play( SOUND_IND_GAMEOVER, 1 );

						break;

					case SCREEN_SET_LANGUAGE:
						break;

					case SCREEN_LOAD_RESOURCES:
						nextScreen = new LoadScreen( midlet, SCREEN_SPLASH_NANO );
						break;

				} // fim switch( index )

				if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE ) {
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft == SOFT_KEY_REMOVE ? null : icons[indexSoftLeft] );
				}

				if ( indexSoftRight != SOFT_KEY_DONT_CHANGE ) {
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight == SOFT_KEY_REMOVE ? null : icons[indexSoftRight] );
				}
			} catch ( Throwable t ) {
				//#if DEBUG == "true"
				t.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, ent�o sai do jogo
				exit();
				unlockScreenChange();
				return -1;
			}

			if ( bkg != null ) {
				bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				bkg.setDark( maskVisible );
			}

			if ( nextScreen != null ) {
				unlockScreenChange();
				midlet.manager.setCurrentScreen( nextScreen );
				return screen;
			} else {
				//#if DEBUG == "true"
					System.out.println( "nextScreen null: " + screen );
				//#endif

				unlockScreenChange();
				return -1;
			}
		} // fim if ( screen != currentScreen )
		else {
			// Libera a troca de telas
			unlockScreenChange();
			return screen;
		}
	}


	public void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				final int originalIndex = index;
				if ( ( saveScreen == null ) && ( index >= OPT_CONTINUE ) ) {
					++index;
				}

				if ( !MediaPlayer.isVibrationSupported() && index >= OPT_OPTION )
					++index;

				switch ( index ) {
					case OPT_NEWGAME:
						setScreen( SCREEN_LOAD_GAME );
						break;
					case OPT_CONTINUE:
						setScreen( SCREEN_CONTINUE );
						break;
					case OPT_OPTION:
//						setScreen( SCREEN_OPTIONS );
						MediaPlayer.setVibration( !MediaPlayer.isVibration() );

						if ( MediaPlayer.isVibration() )
							MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );

						( ( Label ) menu.getDrawable( originalIndex ) ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
						break;
					case OPT_RECORD:
						setScreen( SCREEN_RECORD );
						break;
					case OPT_HELP:
						setScreen( SCREEN_HELP );
						break;
					case OPT_CREDITS:
						setScreen( SCREEN_CREDITOS );
						break;
					case OPT_EXIT:
						exit();
						break;
					//#if LOG == "true"
//# 						case OPT_LOG:
//# 							setScreen( SCREEN_LOG );
//# 							break;
					//#endif
				}
				break;

			//#if LOG == "true"
//# 			case SCREEN_LOG:
			//#endif
			case SCREEN_HELP:
			case SCREEN_CREDITOS:
				setScreen( SCREEN_MAIN_MENU );
				break;

			case SCREEN_YN_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				setScreen( SCREEN_SPLASH_NANO );
				break;

			case SCREEN_YN_BACKGAME:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );

						setScreen( SCREEN_MAIN_MENU );
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_GAMEBACK );
						break;
				}
				break;

			case SCREEN_OPTIONS:
				switch ( index ) {
					case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;
				}
			break;

			case SCREEN_FIMDEJOGO:
				switch ( index ) {
					case TelaDeFimDeJogo.OPT_ENDGAME_RETRY:
						setScreen( SCREEN_LOAD_GAME );
						break;

					case TelaDeFimDeJogo.OPT_ENDGAME_RECORD:
						setScreen( SCREEN_RECORD );

						MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						break;

					case TelaDeFimDeJogo.OPT_ENDGAME_BACK:
						setScreen( SCREEN_MAIN_MENU );

						MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						break;
				}
				break;

			case SCREEN_SET_LANGUAGE:
				try {
					// Determina o idioma a ser utilizado pelo jogo
					setLanguage( ( byte ) index );

					// Vai para a pr�xima tela
					changeScreen( SCREEN_YN_SOUND );
				} catch ( Exception ex ) {
					// Se n�o conseguiu carregar os textos por algum motivo, temos que abortar o jogo
					exit();
				}
				break;
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		// Implementada apenas por causa da requisi��o da interface MenuListener. O corpo deste m�todo � vazio de fato
	}


	public static final Point getBoardPosition() {
		return ( ( MeuMidlet ) instance ).bkg.getBoardPosition();
	}


	protected final void changeLanguage( byte language ) {
	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private byte dots;

		private final MeuMidlet midlet;

		private Thread loadThread;

		private final int nextScreen;

		private static final String loadingMsg = "LOADING";


		private LoadScreen( MeuMidlet midlet, int nextScreen ) throws Exception {
			super( MeuMidlet.fontMain, loadingMsg );
			this.midlet = midlet;
			this.nextScreen = nextScreen;
			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		protected void paint( Graphics g ) {
			// Coloca uma cor de fundo
			pushClip( g );
			g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			g.setColor( 0, 0, 0 );
			g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			popClip( g );

			// Desenha o label
			super.paint( g );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// as imagens do jogo s�o carregadas aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa (ou seja, a transi��o da TV estiver encerrada)
					if ( ScreenManager.getInstance().getCurrentScreen() == this ) {
						loadThread = new Thread() {

							public final void run() {
								try {
									MediaPlayer.free();
									MeuMidlet.setScreen( nextScreen );
								} catch ( Exception e ) {
									// Sai do jogo
									exit();
								}
							}


						};
						loadThread.start();
					}
				} else {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) % MAX_DOTS );
					String temp = new String( loadingMsg );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// Permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}
				}
			}
		}


	}


}