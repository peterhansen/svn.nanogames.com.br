package jogo;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
/*
 * Supernova.java
 *
 * Created on June 15, 2007, 5:14 PM
 *
 */

/**
 *
 * @author malvezzi
 */
public final class Supernova extends Sprite implements Constants, SpriteListener
{
	protected int state;
	
	/**
	 * Creates a new instance of Supernova
	 */
	public Supernova( Sprite s )
	{
		super( s );
		setListener( this, 0 );
	}
	
	
	public final int getState()
	{
		return state;
	}
	
	
	public final void setState( int state )
	{
		switch( state )
		{
			case STATE_VAZIO:
			case STATE_BOOM:
				this.setVisible( false );
				break;
				
			case STATE_B1:
			case STATE_B2:
			case STATE_B3:
			case STATE_B4:
				this.setSequence( state );
				this.setVisible( true );
				break;
				
			default:
				return;
		}
		this.state = state;
	}
	
	
	public static final Supernova loadFrameSet() throws Exception
	{
		return new Supernova( new Sprite( PATH_IMAGES + "f" ) );
	}
	
	
	public final boolean increaseState( boolean bl )
	{
		if( state == STATE_B4 )
		{
			setState( STATE_VAZIO );
			MeuMidlet.increaseScore( STATE_B4_POINTS );
			return false;
		}
		else
		{
			if( bl )
			{
				switch( state )
				{
					case STATE_B1:
						MeuMidlet.increaseScore( STATE_B1_POINTS );
						break;
						
					case STATE_B2:
						MeuMidlet.increaseScore( STATE_B2_POINTS );
						break;
						
					case STATE_B3:
						MeuMidlet.increaseScore( STATE_B3_POINTS );
						break;
				}
			}	
			setState( state + 1 );
			return true;
		}
	}
	
	
	public final void update( int delta )
	{
		if( state != STATE_VAZIO )
			super.update( delta );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if( sequenceIndex == STATE_BOOM )
			setState( STATE_VAZIO );		
	}

	public final Point getLargestSize()
	{
		int width, height;
		Point largest = new Point();
		
		for( int i = 0 ; i < sequences[STATE_B4].length ; ++i )
		{
			width = frames[ sequences[STATE_B4][i] ].getWidth();
			height = frames[ sequences[STATE_B4][i] ].getHeight();
			
			if( width > largest.x )
				largest.x = width;
			
			if( height > largest.y )
				largest.y = height;
		}	
		return largest;
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
}
