package jogo;
/*
 * Constants.java
 *
 * Created on June 15, 2007, 3:59 PM
 *
 */

/**
 *
 * @author malvezzi
 */
public interface Constants
{
	/** M�ximo de pontos que um jogador pode obter */
	public static final int MAX_POINTS = 999999;
	
	/** N�mero de tiros com que o jogador come�a */
	public final byte N_SHOTS_AT_BEGGINING = 20;
	
	/** Level no qual o jogador come�a */
	public final byte STARTING_LEVEL = 1;
	
	/** Maior level que o jogador pode alcan�ar*/
	public final short MAX_LEVEL = 99;
	
	/** Maior n�mero de tiros que o jogador pode alcan�ar */
	public static final short MAX_SHOTS = 999;
		
	// Defini��es de idioma
	public static final byte LANGUAGE_NONE		    = -1;
	public static final byte LANGUAGE_ENGLISH		=  0;
	public static final byte LANGUAGE_PORTUGUESE	=  1;
	public static final byte LANGUAGE_SPANISH		=  2;
	public static final byte LANGUAGE_TOTAL			=  3;
	
	// Arquivos de som
	public static final String SOUND_PATH_DIR      = "/";
	public static final String SOUND_PATH_THEME    = "Theme.mid";
	public static final String SOUND_PATH_CLEARED  = "LevelClear.mid";
	public static final String SOUND_PATH_GAMEOVER = "GameOver.mid";
	
	public static final byte SOUND_IND_THEME  = 0;
	public static final byte SOUND_IND_CLEARED  = 1;
	public static final byte SOUND_IND_GAMEOVER = 2;
	
	// Arquivos de imagem
	public static final String PATH_IMAGES	= "/";
	public static final String SPEC_IMG_PATH_DIR	= "/";

	public static final String IMG_PATH_TELADEJOGO	= SPEC_IMG_PATH_DIR + "teladejogo/";
	public static final String PATH_SPLASH 		= SPEC_IMG_PATH_DIR + "splash/";
	public static final String IMG_PATH_GAMESPLASH	= SPEC_IMG_PATH_DIR + "gameSplash/";
	public static final String IMG_PATH_ICONS		= SPEC_IMG_PATH_DIR + "icons/";
	public static final String IMG_PATH_TABULEIRO	= SPEC_IMG_PATH_DIR + "tabuleiro/";
	public static final String IMG_PATH_SUPERNOVAS	= IMG_PATH_TABULEIRO + "supernovas/";
	
	public static final String IMG_PATH_FLAG_EN = PATH_IMAGES + "flag_en.png";
	public static final String IMG_PATH_FLAG_BR = PATH_IMAGES + "flag_br.png";
	public static final String IMG_PATH_FLAG_ES = PATH_IMAGES + "flag_es.png";

	public static final String IMG_PATH_NEBULA01 = PATH_IMAGES + "nebula1.png";
	public static final String IMG_PATH_NEBULA02 = PATH_IMAGES + "nebula2.png";
	public static final String IMG_PATH_BIGSTAR  = PATH_IMAGES + "bigStar.png";
	public static final String IMG_PATH_SMALL_STAR = PATH_IMAGES + "smallStar.png";

	public static final String IMG_PATH_GAMESPLASH_JIMMY = "splash_jimmy.png";
	public static final String IMG_PATH_GAMESPLASH_DIVERSAO = "splash_titulo";
	public static final String IMG_PATH_GAMESPLASH_LOGO_JIMMY = "splash_logo_jimmy";
	
	public static final String IMG_PATH_ICON_OK = "icon_ok.png";
	public static final String IMG_PATH_ICON_BACK = "icon_back.png";
	public static final String IMG_PATH_ICON_PAUSE  = "icon_pause.png";
	
	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	public static final String IMG_PATH_ICON_SOUND_ON = "icon_sound_on.png";
//# 	public static final String IMG_PATH_ICON_SOUND_OFF = "icon_sound_off.png";
//# 	public static final String IMG_PATH_ICON_VIB_ON = "icon_vib_on.png";
//# 	public static final String IMG_PATH_ICON_VIB_OFF = "icon_vib_off.png";
//# 
	//#endif
	
	// Arquivos de fonte
	public static final String FONT_MAIN_IMG = PATH_IMAGES + "font_menu.png";
	public static final String FONT_MAIN_DAT = PATH_IMAGES + "font_menu.dat";
	
	// Arquivos de texto
	public static final String PATH_TEXTS = "/pt.dat";
	
	// Defini��es do arquivo de dados
	public static final String DATABASE_NAME = "n";
	public static final byte DATABASE_IDX_LANGUAGE = 1;
	public static final byte DATABASE_IDX_SOUND_OPTIONS = 2;
	public static final byte DATABASE_IDX_RECORD = 3;
	
	//#if LOG == "true"
//# 		public static final byte DATABASE_IDX_LOG = 4;
//# 		public static final byte DATABASE_IDX_TOTAL = 4;
	//#else
		public static final byte DATABASE_IDX_TOTAL = 3;
	//#endif
	
	// N�mero de recordes que s�o armazenados na base de dados
	public static final byte TOTAL_RECORDS = 5;
	
	// �ndices das telas do jogo
	public static final byte SCREEN_SPLASH_NANO		= 0;
	public static final byte SCREEN_SPLASH_MARCA	= 1;
	public static final byte SCREEN_SPLASH_GAME		= 2;
	public static final byte SCREEN_SET_LANGUAGE	= 3;
	public static final byte SCREEN_MAIN_MENU		= 4;
	public static final byte SCREEN_JOGO			= 5;
	public static final byte SCREEN_FIMDEJOGO		= 6;
	public static final byte SCREEN_NEWRECORD		= 7;
	public static final byte SCREEN_RECORD			= 8;
	public static final byte SCREEN_HELP			= 9;
	public static final byte SCREEN_CREDITOS		= 10;
	public static final byte SCREEN_CONTINUE		= 11;
	public static final byte SCREEN_YN_BACKGAME		= 12;
	public static final byte SCREEN_OPTIONS			= 13;
	public static final byte SCREEN_GAMEBACK		= 14;
	public static final byte SCREEN_YN_SOUND		= 15;
	public static final byte SCREEN_LOAD_RESOURCES	= 16;
	public static final byte SCREEN_LOAD_GAME		= 17;
	//#if LOG == "true"
//# 	public static final byte SCREEN_LOG				=  18;
	//#endif
	
	// �ndices das op��es e configura��o do menu principal
	public static final byte OPT_NEWGAME	= 0;
	public static final byte OPT_CONTINUE	= 1;
	public static final byte OPT_OPTION		= 2;
	public static final byte OPT_RECORD		= 3;
	public static final byte OPT_HELP		= 4;
	public static final byte OPT_CREDITS	= 5;
	public static final byte OPT_EXIT		= 6;
	//#if LOG == "true"
//# 	public static final byte OPT_LOG		= 7;
	//#endif

	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND = 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION = 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK = 2;

	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK = 2;
	
	// Espa�amento vertical em pixels entre as op��es dos menus
	//#if SCREEN_SIZE == "MEDIUM"
		public static final int MENUS_ITEMS_SPACING = 10;
	//#elif SCREEN_SIZE == "SMALL"
//# 	public static final int MENUS_ITEMS_SPACING = 3;
	//#endif
	
	// Defini��es do tabuleiro do jogo
	public static final byte NUMERO_CELL		= 6;
	public static final byte MAX_SUPERNOVAS		= NUMERO_CELL * NUMERO_CELL;
	
	// Defini��es do cursor e de sua anima��o
	public static final byte CURSOR_TERMINAL_IMAGES = 4;
	public static final byte CURSOR_NORMAL_IMAGES = 6;
	public static final byte CURSOR_TOTAL_IMAGES = CURSOR_TERMINAL_IMAGES + CURSOR_NORMAL_IMAGES;
	public static final byte NORMAL_CURSOR		= 0;
	public static final byte TERMINAL_CURSOR	= 1;
	public static final byte CURSOR_TOTAL_SEQS = 3;
	public static final short CURSOR_ANIM_TIME = 213;
	
	// Dire��es de movimento no tabuleiro
	public static final byte DIR_UP		= 0;
	public static final byte DIR_DOWN	= 1;
	public static final byte DIR_RIGHT	= 2;
	public static final byte DIR_LEFT	= 3;
	
	//Supernova
	public static final byte IMAGENS_TOTAL = 23;
	public static final byte TOTAL_ESTADOS = 6;
	
	public static final byte STATE_VAZIO = 0;
	public static final byte STATE_B1 	= 1;
	public static final byte STATE_B2 	= 2;
	public static final byte STATE_B3 	= 3;
	public static final byte STATE_B4 	= 4;
	public static final byte STATE_BOOM 	= 5;
	
	public static final short STATE_B1_TIME 	= 222;
	public static final short STATE_B2_TIME 	= 222;
	public static final short STATE_B3_TIME 	= 148;
	public static final short STATE_B4_TIME 	= 175;
	
	public static final short STATE_BOOM_TIME	= 544;
	public static final byte FILL_SUPERNOVA_SEED = 49;
	
	//Pontuacao
	public static final byte STATE_B1_POINTS = 42;
	public static final byte STATE_B2_POINTS = 23;
	public static final byte STATE_B3_POINTS = 11;
	public static final byte STATE_B4_POINTS = 12;
	public static final short SCORE_INCREMENT = 487;
	
	// Estilha�os da supernova
	public static final byte SHARDS_ANIM_SPEED = 66;
	public static final short MAX_SUPERNOVA_SHARDS = MAX_SUPERNOVAS * 4;
	public static final byte SHARDS_IMAGENS_TOTAL	= 5;
	
	// Defini��es das Telas
	
	// BackGround
	public static final byte NUMERO_SMALLSTAR	= 19;
	public static final byte NUMERO_BIGSTAR		= 3;
	public static final byte DELAYSMALL			= 3;
	public static final byte DELAYBIG			= 17;
	public static final byte NEBULASPEED		= -2;
	public static final byte MAX_STARS			= 30;
	
	//GameSplash
	public static final short GAME_SPLASH_TIMECONT = 3300;
	
	//Tela de Jogo
	public static final byte SPACER_LABEL_X		= 5;
	public static final byte SPACER_LABEL_Y		= 1;
	
	//Record
	public static final short SCOREBLINKDELAY		= 512;
	
	// Defini��es da vibra��o
	//#if SAMSUNG_BAD_SOUND == "true"
//# 		public static final short DEFAULT_VIBRATION_TIME = 80;
	//#else
		public static final short VIBRATION_TIME_DEFAULT = 300;
	//#endif
	
	//#if SAMSUNG_BAD_SOUND == "true"
//# 		public static final short SAMSUNG_BS_LOOP_INFINITE = 100;
	//#endif
		
	// Define os �ndices dos �cones de navega��o
	public final byte ICON_OK = 0;
	public final byte ICON_BACK = 1;
	public final byte ICON_PAUSE = 2;

	// Defini��es dos �ndices dos textos utilizados no jogo
	public static final byte TEXT_BACK					= 0;
	public static final byte TEXT_NEW_GAME				= 1;
	public static final byte TEXT_EXIT					= 2;
	public static final byte TEXT_OPTIONS				= 3;
	public static final byte TEXT_CREDITS				= 4;
	public static final byte TEXT_CREDITS_TEXT			= 5;
	public static final byte TEXT_HELP					= 6;
	public static final byte TEXT_HELP_TEXT				= 7;
	public static final byte TEXT_SCORE					= 8;
	public static final byte TEXT_LEVEL					= 9;
	public static final byte TEXT_SHOTS					= 10;
	public static final byte TEXT_TURN_SOUND_ON			= 11;
	public static final byte TEXT_TURN_SOUND_OFF		= 12;
	public static final byte TEXT_TURN_VIBRATION_ON		= 13;
	public static final byte TEXT_TURN_VIBRATION_OFF	= 14;
	public static final byte TEXT_CONTINUE				= 15;
	public static final byte TEXT_LANGUAGE				= 16;
	public static final byte TEXT_YES					= 17;
	public static final byte TEXT_NO					= 18;
	public static final byte TEXT_GAMEOVER				= 19;
	public static final byte TEXT_NEWRECORD				= 20;
	public static final byte TEXT_RECORDS				= 21;
	public static final byte TEXT_CURR_LANGUAGE			= 22;
	public static final byte TEXT_LEAVEGAME				= 23;
	public static final byte TEXT_RETRY					= 24;
	public static final byte TEXT_SPLASH_NANO			= 25;
	public static final byte TEXT_SPLASH_NICK			= 26;
	public static final byte TEXT_WITH_SOUND			= 27;

	public static final byte TEXT_TOTAL					= 28;

	/** velocidade m�nima de movimenta��o das nuvens, em pixels por segundo */
	public static final byte BEACH_CLOUD_MIN_SPEED = 1;
	/** velocidade m�xima de movimenta��o das nuvens, em pixels por segundo */
	public static final byte BEACH_CLOUD_MAX_SPEED = 13;

	public static final byte LEVEL_TYPE_CHANGE = 6;

	public static final byte ROWS = 6;
	public static final byte COLUMNS = 6;

	//#if SCREEN_SIZE == "MEDIUM"
		public static final short SHARDS_SPEED = 148;  // Velocidade em px/s
		public static final byte BUILDING_PATTERN_OFFSET_Y = 53;
		public static final byte HIDRANT_OFFSET_X = 39;
		public static final short BOARD_WIDTH = 150;
		public static final short BOARD_HEIGHT = 150;
		public static final byte SHOT_POS_Y = 6;
		public static final short PLAYSCREEN_DEFAULT_HEIGHT = 220;
		public static final byte TREES_DIVIDER = 10;

		public static final byte BOARD_OFFSET_X = 2;
		public static final byte BOARD_OFFSET_Y = 2;

		public static final short SPLASH_DEFAULT_WIDTH = 200;
		public static final short SPLASH_DEFAULT_HEIGHT = 200;

		public static final short SPLASH_FIREMAN_X = 49;
		public static final short SPLASH_FIREMAN_Y = 1;
		public static final short SPLASH_WATER_X = 6;
		public static final short SPLASH_WATER_Y = 87;
		public static final short SPLASH_TITLE_X = 12;
		public static final short SPLASH_TITLE_Y = 112;
	//#else
//# 		public static final short SHARDS_SPEED = 108;  // Velocidade em px/s
//# 
//# 		public static final byte BUILDING_PATTERN_OFFSET_Y = 33;
//# 		public static final byte HIDRANT_OFFSET_X = 13;
//# 		public static final short BOARD_WIDTH = 102;
//# 		public static final short BOARD_HEIGHT = 102;
//# 		public static final byte SHOT_POS_Y = 3;
//# 		public static final short PLAYSCREEN_DEFAULT_HEIGHT = 128;
//# 		public static final byte TREES_DIVIDER = 5;
//# 		public static final byte BOARD_OFFSET_X = 1;
//# 		public static final byte BOARD_OFFSET_Y = 1;
//# 
//# 		public static final short SPLASH_DEFAULT_WIDTH = 128;
//# 		public static final short SPLASH_DEFAULT_HEIGHT = 128;
//# 
//# 		public static final short SPLASH_FIREMAN_X = 34;
//# 		public static final short SPLASH_FIREMAN_Y = 20;
//# 		public static final short SPLASH_WATER_X = SPLASH_FIREMAN_X - 28;
//# 		public static final short SPLASH_WATER_Y = SPLASH_FIREMAN_Y + 57;
//# 		public static final short SPLASH_TITLE_X = 7;
//# 		public static final short SPLASH_TITLE_Y = 88;
	//#endif

	public static final short CELL_WIDTH = BOARD_WIDTH / ROWS;
	public static final short CELL_HEIGHT = BOARD_HEIGHT / COLUMNS;

}
