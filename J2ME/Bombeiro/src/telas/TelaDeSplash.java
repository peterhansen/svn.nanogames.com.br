package telas;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import jogo.MeuMidlet;
import jogo.Constants;

/**
 *
 * @author malvezzi
 */

public final class TelaDeSplash extends UpdatableGroup implements KeyListener, Constants
{
	/** Tempo de dura��o do splash */
	private int timeCont = GAME_SPLASH_TIMECONT;



	/** Creates a new instance of TelaDeSplash */
	public TelaDeSplash() throws Exception
	{
		// Inicializa a superclasse
		super( 3 );

		// Determina o tamanho do componente
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		final int OFFSET_X = ( size.x - SPLASH_DEFAULT_WIDTH ) >> 1;
		final int OFFSET_Y = ( size.y - SPLASH_DEFAULT_HEIGHT ) >> 1;
		
		final DrawableImage fireman = new DrawableImage( PATH_SPLASH + "fireman.png" );
		fireman.setPosition( SPLASH_FIREMAN_X + OFFSET_X, SPLASH_FIREMAN_Y + OFFSET_Y );
		insertDrawable( fireman );

		final DrawableImage title = new DrawableImage( PATH_SPLASH + "title.png" );
		title.setPosition( SPLASH_TITLE_X + OFFSET_X, SPLASH_TITLE_Y + OFFSET_Y );
		insertDrawable( title );

		final DrawableImage water = new DrawableImage( PATH_SPLASH + "water.png" );
		water.setPosition( SPLASH_WATER_X + OFFSET_X, SPLASH_WATER_Y + OFFSET_Y );
		insertDrawable( water );

	}
	
	
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				MeuMidlet.exit();
			break;
			
			// Os splashs n�o podem ser acelerados pelo usu�rio
			//#if DEBUG == "true"
			default:
				callNextScreen();
			//#endif
		}
	}
	
	
	public void keyReleased( int key )
	{
	}
	
	
	public void update( int delta )
	{
		super.update( delta );
		
		if( timeCont < 0 )
			callNextScreen();
		timeCont -=delta;
	}
	
	
	private final void callNextScreen()
	{
		MeuMidlet.setScreen( SCREEN_MAIN_MENU );
	}
}
