package telas;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import jogo.MeuMidlet;
import jogo.Constants;
/*
 * TelaDeRecorde.java
 *
 * Created on June 19, 2007, 5:47 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class TelaDeRecorde extends DrawableGroup implements KeyListener, Constants
{
	/** Creates a new instance of TelaDeRecorde */
	public TelaDeRecorde() throws Exception
	{
		// Inicializa a superclasse
		super( TOTAL_RECORDS + 1 );
		
		// Aloca o t�tulo da tela 
		Label aux = new Label( MeuMidlet.fontMain, MeuMidlet.getText( TEXT_RECORDS ) );
		aux.setPosition( ( ScreenManager.SCREEN_WIDTH - aux.getWidth() ) >> 1, aux.getHeight() );
		
		insertDrawable( aux );
		
		// Verifica qual o maior tamanho que um label de pontua��o pode chegar
		int labelsPosX = ( ScreenManager.SCREEN_WIDTH - aux.getFont().getTextWidth( "9) " + MAX_POINTS ) ) >> 1;
		
		// Aloca os labels que ir�o exibir os recordes
		for( int i = 0 ; i < TOTAL_RECORDS ; ++i )
		{
			aux = new Label( MeuMidlet.fontMain, ( i + 1 ) + ". " + getScoreLabelText( MeuMidlet.topRecords[i] ) );
			aux.setPosition( labelsPosX,
				( ScreenManager.SCREEN_HEIGHT * 30/100) + (ScreenManager.SCREEN_HALF_HEIGHT / TOTAL_RECORDS) * i );
			
			insertDrawable( aux );
		}
	}
	
	
	public final String getScoreLabelText( int score )
	{
		String res = "";
		final String pointsStr = String.valueOf( score );
		final String maxPointsStr = String.valueOf( MAX_POINTS );
		
		int nSpaces = maxPointsStr.length() - pointsStr.length();
		while( nSpaces > 0 )
		{
			res += "0";
			--nSpaces;
		}
			
		return new String( res + score );
	}
	
	
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				MeuMidlet.setScreen( SCREEN_MAIN_MENU );
				return;
		}
	}
	
	
	public final void keyReleased( int key )
	{
	}
}
