package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.Constants;
import jogo.Tabuleiro;
import jogo.Transition;
import jogo.TransitionListener;

/*
 * TelaDeJogo.java
 *
 * Created on June 15, 2007, 5:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class TelaDeJogo extends UpdatableGroup implements PointerListener, KeyListener, Constants, TransitionListener, ScreenListener {

	private static int scoreShown;

	private int dv;

	private int dva;

	private final Tabuleiro board;

	private final Label labelScore;

	//#if SCREEN_SIZE != "SMALL"
		private final DrawableImage fireman;
	//#endif

	private final Pattern scoreBoard;

	private final Label labelShots;

	private final DrawableImage shot;

	private Transition myTransition = null;


	/** Creates a new instance of TelaDeJogo */
	public TelaDeJogo() throws Exception {
		super( 12 );

		MeuMidlet.lastScoreEntryIndex = TOTAL_RECORDS;

		// A vari�vel tabuleiro precisa de lbPontos e lbTirosRestantes criados
		labelScore = new Label( ImageFont.createMonoSpacedFont( PATH_IMAGES + "font_1.png", "0123456789" ), String.valueOf( MAX_POINTS ) );
		labelShots = new Label( ImageFont.createMonoSpacedFont( PATH_IMAGES + "font_0.png", "0123456789" ), String.valueOf( MAX_SHOTS ) );

		board = new Tabuleiro( this );
		insertDrawable( board );

		scoreBoard = new Pattern( new DrawableImage( PATH_IMAGES + "bar.png" ) );
		insertDrawable( scoreBoard );

		shot = new DrawableImage( PATH_IMAGES + "shot.png" );
		insertDrawable( shot );

		//#if SCREEN_SIZE != "SMALL"
			fireman = new DrawableImage( PATH_IMAGES + "fireman.png" );
			insertDrawable( fireman );
			Thread.yield();
		//#endif

		insertDrawable( labelScore );
		insertDrawable( labelShots );

		myTransition = new Transition( this );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void startLevelOne() {
		MeuMidlet.setScore( 0 );
		scoreShown = 0;
		updateScoreLabel();

		board.startLevelOne();
	}


	public final void levelEnded( int newLevel ) {
		MediaPlayer.play( SOUND_IND_CLEARED, 1 );

		myTransition.reset( newLevel );

		MeuMidlet.setSoftKeyIcon( ScreenManager.SOFT_KEY_RIGHT, -1 );
	}


	public final void nextLevel() {
		board.nextLevel();
	}


	public final void updateScoreLabel() {
		final String pointsStr = String.valueOf( MeuMidlet.getScore() );
		final String maxPointsStr = String.valueOf( MAX_POINTS );

 		String res = "";

		int nSpaces = maxPointsStr.length() - pointsStr.length();
		while ( nSpaces > 0 ) {
			res += "0";
			--nSpaces;
		}

		labelScore.setText( res + MeuMidlet.getScore(), false );
	}


	public final void updateShotsLabel() {
		final String shotsStr = String.valueOf( board.getShots() );
		final String maxShotsStr = String.valueOf( Tabuleiro.MAX_SHOTS );

		String res = "";

		int nSpaces = maxShotsStr.length() - shotsStr.length();
		while ( nSpaces > 0 ) {
			res += "0";
			--nSpaces;
		}

		labelShots.setText( res + board.getShots(), false );
	}


	public final void keyPressed( int key ) {
		if ( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED ) {
			return;
		}

		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				MeuMidlet.setScreen( SCREEN_YN_BACKGAME );
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				board.moveCursor( DIR_RIGHT );
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				board.moveCursor( DIR_LEFT );
				break;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				board.moveCursor( DIR_UP );
				break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				board.moveCursor( DIR_DOWN );
				break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				fire();
				break;
		}
	}


	private final void fire() {
		board.fire();
	}


	public final void keyReleased( int key ) {
	}


	public final void resetScoreShown() {
		scoreShown = 0;
	}


	private final void updateScoreShown( boolean equalize, int delta ) {
		if ( scoreShown < MeuMidlet.getScore() ) {
			if ( equalize ) {
				scoreShown = MeuMidlet.getScore();
			} else {
				final short oneSecondInMS = 1000;
				dv = ( dva + ( delta * SCORE_INCREMENT ) ) / oneSecondInMS;
				dva = ( delta * SCORE_INCREMENT ) % oneSecondInMS;
				scoreShown += dv;
				if ( scoreShown > MeuMidlet.getScore() ) {
					scoreShown = MeuMidlet.getScore();
				}
			}
			updateScoreLabel();
		}
	}


	public final void update( int delta ) {
		if ( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED ) {
			myTransition.update( delta );
		}

		super.update( delta );
		updateScoreShown( false, delta );
	}


	public final void onClosed() {
		nextLevel();
	}


	public final void onEnded() {
		MeuMidlet.setSoftKeyIcon( ScreenManager.SOFT_KEY_RIGHT, ICON_PAUSE );
	}


	protected final void paint( Graphics g ) {
		super.paint( g );

		if ( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED ) {
			myTransition.draw( g );
		}
	}


	public final void onPointerDragged( int x, int y ) {
		// N�o faz nada
	}


	public final void onPointerPressed( int x, int y ) {
		if ( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED ) {
			return;
		}

		Point boardPos = board.getPosition();
		Point boardSize = board.getSize();

		// Verifica se colide com o tabuleiro
		if ( ( x < boardPos.x ) || ( x > boardPos.x + boardSize.x ) ) {
			return;
		}

		if ( ( y < boardPos.y ) || ( y > boardPos.y + boardSize.y ) ) {
			return;
		}

		// Transforma as coordenadas para ficarem relativas ao tabuleiro
		x -= boardPos.x;
		y -= boardPos.y;

		// Move o cursor at� a c�lula correta
		final Point boardCellSize = board.getBoardCellSize();
		board.setCursorAtCell( x / boardCellSize.x, y / boardCellSize.y );

		// Atira
		fire();
	}


	public void onPointerReleased( int x, int y ) {
		// N�o faz nada
	}


	public final void hideNotify() {
	}


	public final void showNotify() {
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			setSize( width, height );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		board.setPosition( MeuMidlet.getBoardPosition() );
		scoreBoard.setSize( width, scoreBoard.getFill().getHeight() );
		scoreBoard.setPosition( 0, height - scoreBoard.getHeight() );

		//#if SCREEN_SIZE == "SMALL"
//# 			shot.setPosition( 2, scoreBoard.getPosY() + SHOT_POS_Y );
		//#else
			fireman.setPosition( 0, height - fireman.getHeight() );
			shot.setPosition( fireman.getWidth(), scoreBoard.getPosY() + SHOT_POS_Y );
		//#endif

		labelShots.setPosition( shot.getPosX() + ( shot.getWidth() << 1 ), shot.getPosY() + shot.getHeight() - labelShots.getHeight() );
		labelScore.setPosition( width - labelScore.getWidth() - MeuMidlet.getIconSize().x - SPACER_LABEL_X, labelShots.getPosY() );
	}


}
