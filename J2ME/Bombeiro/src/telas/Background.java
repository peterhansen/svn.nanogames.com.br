/**
 * Background.java
 * 
 * Created on Jun 18, 2009, 5:03:00 PM
 *
 */

package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import jogo.Constants;
import jogo.MeuMidlet;

/**
 *
 * @author Peter
 */
public final class Background extends UpdatableGroup implements Constants {

	private static final int SKY_COLOR_BLUE = 0x4bb6f8;
	private static final int SKY_COLOR_ORANGE = 0xff9128;
	private static final int SKY_COLOR_DARK = 0x16364a;
	
	private static final int ASPHALT_COLOR_UP = 0xb0b0b0;
	private static final int ASPHALT_COLOR_MIDDLE = 0x585858;
	private static final int ASPHALT_COLOR_DOWN = 0x1c1c1c;

	private static final int ASPHALT_COLOR_UP_DARK = 0x454545;
	private static final int ASPHALT_COLOR_MIDDLE_DARK = 0x272727;
	private static final int ASPHALT_COLOR_DOWN_DARK = 0x111111;

	private static final byte MAX_CLOUDS = 5;

	/** n�mero total de imagens de nuvens */
	private static final byte CLOUDS_TOTAL_IMAGES = 3;

	private static final byte TREES_ROWS = 3;

	private final byte MAX_TREES_PER_ROW;

	private final DrawableGroup treesGroup;

	private final Cloud[] clouds = new Cloud[ MAX_CLOUDS ];

	private final Sprite building;

	private final Pattern buildingsLeft;
	private final Pattern buildingsRight;

	private final Pattern skyLeft;
	private final Pattern skyUp;
	private final Pattern skyRight;

	private final Pattern sidewalk;
	private final Sprite sidewalkSprite;

	private final Pattern skySolid;

	private final Pattern asphaltUp;
	private final Pattern asphaltMiddle;
	private final Pattern asphaltDown;

	private final Pattern sidewalkLeft;
	private final Pattern sidewalkRight;

	private final Sprite hidrant;

	private byte type;

	private static final byte TOTAL_TYPES = 2;
	public static final byte TYPE_BLUE = 0;
	public static final byte TYPE_ORANGE = 1;
	public static final byte TYPE_BLUE_DARK = 2;
	public static final byte TYPE_ORANGE_DARK = 3;


	private final DrawableImage[] imagesSky = new DrawableImage[ TOTAL_TYPES + 1 ];
	private final DrawableImage[] imagesBuildings = new DrawableImage[ TOTAL_TYPES + 1 ];


	public Background() throws Exception {
		super( MAX_CLOUDS + 20 );

		for ( byte i = 0; i < imagesSky.length; ++i ) {
			if ( !MeuMidlet.isLowMemory() || i != TYPE_ORANGE ) {
				imagesSky[ i ] = new DrawableImage( PATH_IMAGES + "sky_" + i + ".png" );
				imagesBuildings[ i ] = new DrawableImage( PATH_IMAGES + "bkg_" + i + ".png" );
			}
		}

		skyLeft = new Pattern( imagesSky[ 0 ] );
		skyUp = new Pattern( skyLeft.getFill() );
		skyRight = new Pattern( skyLeft.getFill() );
		insertDrawable( skyLeft );
		insertDrawable( skyUp );
		insertDrawable( skyRight );

		skySolid = new Pattern( SKY_COLOR_BLUE );
		insertDrawable( skySolid );

		buildingsLeft = new Pattern( imagesBuildings[ 0 ] );
		buildingsRight = new Pattern( buildingsLeft.getFill() );
		insertDrawable( buildingsLeft );
		insertDrawable( buildingsRight );

		// insere as nuvens
		final Sprite cloudSprite = new Sprite( PATH_IMAGES + "cloud" );

		for ( byte i = 0; i < MAX_CLOUDS; ++i ) {
			clouds[ i ] = new Cloud( this, cloudSprite );
			insertDrawable( clouds[ i ] );
		}

		sidewalkSprite = new Sprite( PATH_IMAGES + "sidewalk" );
		sidewalk = new Pattern( sidewalkSprite );
		insertDrawable( sidewalk );

		asphaltUp = new Pattern( ASPHALT_COLOR_UP );
		insertDrawable( asphaltUp );

		asphaltMiddle = new Pattern( ASPHALT_COLOR_MIDDLE );
		insertDrawable( asphaltMiddle );

		asphaltDown = new Pattern( ASPHALT_COLOR_DOWN );
		insertDrawable( asphaltDown );

		sidewalkLeft = new Pattern( sidewalk.getFill() );
		insertDrawable( sidewalkLeft );

		sidewalkRight = new Pattern( sidewalk.getFill() );
		insertDrawable( sidewalkRight );

		hidrant = new Sprite( PATH_IMAGES + "h" );
		insertDrawable( hidrant );

		final Sprite tree = new Sprite( PATH_IMAGES + "tree" );
		building = new Sprite( PATH_IMAGES + "building" );

		MAX_TREES_PER_ROW = ( byte ) ( ( Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) - building.getWidth() ) / TREES_DIVIDER );

		final int totalTrees = MAX_TREES_PER_ROW * TREES_ROWS;
		treesGroup = new DrawableGroup( MAX_TREES_PER_ROW * TREES_ROWS );
		for ( byte i = 0; i < totalTrees; ++i ) {
			final Sprite t = new Sprite( tree );
			t.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			treesGroup.insertDrawable( t );
		}

		insertDrawable( treesGroup );

		building.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( building );
	}


	public final Point getBoardPosition() {
		return building.getPosition();
	}


	public final void setDark( boolean dark ) {
		if ( dark ) {
			for ( byte i = 0; i < treesGroup.getUsedSlots(); ++i ) {
				( ( Sprite ) treesGroup.getDrawable( i ) ).setFrame( 1 );
			}

			for ( byte i = 0; i < clouds.length; ++i )
				clouds[ i ].setSequence( 2 );

			sidewalkSprite.setFrame( 1 );
			building.setFrame( 1 );
			hidrant.setFrame( 1 );

			buildingsLeft.setFill( imagesBuildings[ 2 ] );
			skyLeft.setFill( imagesSky[ 2 ] );
			skySolid.setFillColor( SKY_COLOR_DARK );

			asphaltUp.setFillColor( ASPHALT_COLOR_UP_DARK );
			asphaltMiddle.setFillColor( ASPHALT_COLOR_MIDDLE_DARK );
			asphaltDown.setFillColor( ASPHALT_COLOR_DOWN_DARK );
		} else {
			for ( byte i = 0; i < treesGroup.getUsedSlots(); ++i ) {
				( ( Sprite ) treesGroup.getDrawable( i ) ).setFrame( 0 );
			}
			sidewalkSprite.setFrame( 0 );
			building.setFrame( 0 );
			hidrant.setFrame( 0 );

			buildingsLeft.setFill( imagesBuildings[ type ] );
			skyLeft.setFill( imagesSky[ type ] );
			
			switch ( type ) {
				case TYPE_BLUE:
					skySolid.setFillColor( SKY_COLOR_BLUE );
				break;

				case TYPE_ORANGE:
					skySolid.setFillColor( SKY_COLOR_ORANGE );
				break;
			}
			for ( byte i = 0; i < clouds.length; ++i )
				clouds[ i ].setSequence( type );

			asphaltUp.setFillColor( ASPHALT_COLOR_UP );
			asphaltMiddle.setFillColor( ASPHALT_COLOR_MIDDLE );
			asphaltDown.setFillColor( ASPHALT_COLOR_DOWN );
		}
		buildingsRight.setFill( buildingsLeft.getFill() );
		skyRight.setFill( skyLeft.getFill() );
		skyUp.setFill( skyLeft.getFill() );
	}


	public final void setType( int type ) {
		if ( MeuMidlet.isLowMemory() )
			type = TYPE_BLUE;
		
		this.type = ( byte ) type;
		switch ( type ) {
			case TYPE_BLUE:
				skySolid.setFillColor( SKY_COLOR_BLUE );
			break;

			case TYPE_ORANGE:
				skySolid.setFillColor( SKY_COLOR_ORANGE );
			break;
		}

		for ( byte i = 0; i < clouds.length; ++i ) {
			clouds[ i ].setSequence( type );
			clouds[ i ].setFrame( NanoMath.randInt( CLOUDS_TOTAL_IMAGES ) );
		}

		skyLeft.setFill( imagesSky[ type ] );
		skyRight.setFill( imagesSky[ type ] );
		skyUp.setFill( imagesSky[ type ] );

		buildingsLeft.setFill( imagesBuildings[ type ] );
		buildingsRight.setFill( imagesBuildings[ type ] );
	}
	

	public final void setSize( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			super.setSize( width, height );

			building.setRefPixelPosition( width >> 1, Math.max( ( height - PLAYSCREEN_DEFAULT_HEIGHT ) >> 1, 0 ) );

			sidewalk.setSize( width, sidewalk.getFill().getHeight() );
			sidewalk.setPosition( 0, building.getPosY() + building.getHeight() - 1 );

			asphaltUp.setSize( width, 1 );
			asphaltUp.setPosition( 0, sidewalk.getPosY() + sidewalk.getHeight() );

			asphaltMiddle.setSize( asphaltUp.getSize() );
			asphaltMiddle.setPosition( 0, asphaltUp.getPosY() + asphaltUp.getHeight() );

			asphaltDown.setPosition( 0, asphaltMiddle.getPosY() + asphaltMiddle.getHeight() );
			asphaltDown.setSize( width, height - asphaltDown.getPosY() + asphaltDown.getHeight() );

			final int diff = ( width - building.getWidth() ) >> 1;

			buildingsLeft.setSize( diff + 1, buildingsLeft.getFill().getHeight() );
			buildingsLeft.setPosition( 0, building.getPosY() + BUILDING_PATTERN_OFFSET_Y );

			buildingsRight.setSize( buildingsLeft.getSize() );
			buildingsRight.setPosition( width - buildingsRight.getWidth(), building.getPosY() + BUILDING_PATTERN_OFFSET_Y );

			sidewalkLeft.setPosition( 0, buildingsLeft.getPosY() + buildingsLeft.getHeight() );
			sidewalkLeft.setSize( buildingsLeft.getWidth(), sidewalk.getPosY() - sidewalkLeft.getPosY() );

			sidewalkRight.setSize( sidewalkLeft.getSize() );
			sidewalkRight.setPosition( buildingsRight.getPosX(), sidewalkLeft.getPosY() );

			hidrant.setPosition( building.getPosX() + HIDRANT_OFFSET_X, sidewalk.getPosY() );

			skyLeft.setSize( buildingsLeft.getWidth(), skyLeft.getFill().getHeight() );
			skyLeft.setPosition( 0, buildingsLeft.getPosY() - skyLeft.getHeight() );

			skyUp.setSize( building.getWidth(), skyLeft.getHeight() );
			skyUp.setPosition( skyLeft.getWidth(), skyLeft.getPosY() );

			skyRight.setSize( skyLeft.getSize() );
			skyRight.setPosition( buildingsRight.getPosX(), skyLeft.getPosY() );

			skySolid.setSize( width, skyLeft.getPosY() );

			for ( byte i = 0; i < MAX_CLOUDS; ++i ) {
				clouds[ i ].reset( true );
			}

			treesGroup.setSize( size );

			final int WIDTH = ( width - building.getWidth() ) >> 1;
			final byte CURRENT_TREES_PER_ROW = ( byte ) ( WIDTH / TREES_DIVIDER );
			final byte TOTAL_TREES = ( byte ) ( CURRENT_TREES_PER_ROW * TREES_ROWS );
			final int ROW_HEIGHT = sidewalkLeft.getHeight() / TREES_ROWS;
			final int COLUMN_WIDTH = WIDTH / Math.max( CURRENT_TREES_PER_ROW >> 1, 1 );

			if ( CURRENT_TREES_PER_ROW > 0 ) {
				for ( int i = 0, y = sidewalkLeft.getPosY() + 14; i < TREES_ROWS; ++i, y += ROW_HEIGHT ) {
					for ( int j = 0, x = COLUMN_WIDTH >> 1; j < CURRENT_TREES_PER_ROW; ++j, x += COLUMN_WIDTH ) {
						final Drawable tree = treesGroup.getDrawable( ( i * CURRENT_TREES_PER_ROW ) + j );
						tree.setVisible( true );

						if ( NanoMath.randInt( 128 ) < 64 )
							tree.setTransform( TRANS_MIRROR_H );

						final int RANGE = ( COLUMN_WIDTH - 20 );
						final int TREE_X = x - ( RANGE >> 1 ) + NanoMath.randInt( RANGE );
						tree.setRefPixelPosition( TREE_X < WIDTH ? TREE_X : buildingsRight.getPosX() - WIDTH + TREE_X,
												  y + NanoMath.randInt( ROW_HEIGHT >> 1 ) );
					}
				}

				for ( byte i = TOTAL_TREES; i < treesGroup.getUsedSlots(); ++i )
					treesGroup.getDrawable( i ).setVisible( false );
			}
		}
	}


	// <editor-fold defaultstate="collapsed" desc="classe interna: Cloud">

	//#ifndef JAR_100_KB

	private static final class Cloud extends Sprite {

		private final MUV muv = new MUV();

		private final Background bkg;

		private Cloud( Background bkg, Sprite cloudSet ) throws Exception {
			super( cloudSet );

			this.bkg = bkg;
		}


		public final void update( int delta ) {
			move( muv.updateInt( delta ), 0 );

			if ( position.x >= bkg.getWidth() ) {
				reset( false );
			}
		}


		public final void reset( boolean useAllWidth  ) {
			muv.setSpeed( BEACH_CLOUD_MIN_SPEED + NanoMath.randInt( BEACH_CLOUD_MAX_SPEED - BEACH_CLOUD_MIN_SPEED ) );

			setFrame( NanoMath.randInt( CLOUDS_TOTAL_IMAGES ) );

			defineReferencePixel( 0, size.y >> 1 );

			if ( useAllWidth )
				setRefPixelPosition( NanoMath.randInt( bkg.getWidth() ), NanoMath.randInt( bkg.skyLeft.getHeight() ) );
			else
				setRefPixelPosition( -getWidth() - NanoMath.randInt( bkg.getWidth() ), NanoMath.randInt( bkg.skyLeft.getHeight() ) );
		}

	} // fim da classe interna Cloud

	//#endif

	//</editor-fold>

}
