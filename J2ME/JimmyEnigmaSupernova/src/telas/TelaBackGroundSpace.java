package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import util.MyRandom;
import jogo.Constants;

/*
 * TelaBackGroundSpace.java
 *
 * Created on 7 de Junho de 2007, 15:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Malvezzi
 */
public final class TelaBackGroundSpace extends UpdatableGroup implements Constants
{
	protected DrawableImage[] smallStar = new DrawableImage[NUMERO_SMALLSTAR];

	protected DrawableImage[] bigStar = new DrawableImage[NUMERO_BIGSTAR];

	protected DrawableImage nebula1;

	protected DrawableImage nebula2;

	protected Pattern space;

	protected MUV[] fisicaSmallStar = new MUV[NUMERO_SMALLSTAR];

	protected MUV[] fisicaBigStar = new MUV[NUMERO_BIGSTAR];

	protected MUV fisicaNebula1 = new MUV();

	protected MUV fisicaNebula2 = new MUV();

	private final MyRandom rand = new MyRandom();

	static long teste = System.currentTimeMillis();

	/** Creates a new instance of BackGroundSky */
	public TelaBackGroundSpace() throws Exception
	{
		super( MAX_STARS );

		// Pano de Fundo
		space = new Pattern( null );
		space.setFillColor( 0x000000 );
		space.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		this.insertDrawable( space );

		Thread.yield();

		criaAnimacao();
	}

	public final void setSize( int width, int height )
	{
		super.setSize( width, height );
		space.setSize( width, height );
		/// TODO nebula2.move( 0, ScreenManager.SCREEN_HEIGHT - ( nebula2.getPosition().y + nebula2.getSize().y ) );
	}

	public final void criaAnimacao() throws Exception
	{
		nebula1 = new DrawableImage( IMG_PATH_NEBULA01 );
		Thread.yield();
		insertDrawable( nebula1 );
		nebula1.setPosition( rand.nextInt( ScreenManager.SCREEN_WIDTH ), rand.nextInt( ScreenManager.SCREEN_HALF_HEIGHT >> 1 ) );

		nebula2 = new DrawableImage( IMG_PATH_NEBULA02 );
		Thread.yield();
		insertDrawable( nebula2 );
		nebula2.setPosition( rand.nextInt( ScreenManager.SCREEN_WIDTH ), ScreenManager.SCREEN_HEIGHT - nebula2.getSize().y );

		fisicaNebula1.setSpeed( NEBULASPEED );
		fisicaNebula2.setSpeed( NEBULASPEED );

		createStarArray( bigStar, fisicaBigStar, IMG_PATH_BIGSTAR );
		createStarArray( smallStar, fisicaSmallStar, IMG_PATH_SMALL_STAR );
		Thread.yield();
	}

	protected final void createStarArray( DrawableImage[] vetDraw, MUV[] vetFis, String imagem ) throws Exception
	{
		for( int i = 0; i < vetDraw.length; ++i )
		{
			vetFis[i] = new MUV();
			vetDraw[i] = new DrawableImage( imagem );
			Thread.yield();

			insertDrawable( vetDraw[i] );
			vetDraw[i].setPosition( -ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT );

		}
	}

	protected final void updateStarArray( DrawableImage[] vetDraw, MUV[] vetFis, int retardo, int delta )
	{
		for( int i = 0; i < vetDraw.length; i++ )
		{
			final int dx = vetFis[i].updateInt( delta );

			if( vetDraw[i].getPosition().x + dx < -vetDraw[i].getSize().x )
			{
				//altera a posicao da estrela
				final int height = rand.nextInt( ScreenManager.SCREEN_HEIGHT - 5 );
				vetDraw[i].setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt( ScreenManager.SCREEN_WIDTH ), height );

				//altera a velocidade da estrela
				final int velocidade = -( ( retardo == DELAYBIG ? DELAYSMALL : DELAYBIG ) + rand.nextInt( (( ScreenManager.SCREEN_HALF_WIDTH << 1 ) + ScreenManager.SCREEN_WIDTH )/ retardo ) );
				vetFis[i].setSpeed( velocidade, true );
			}
			else
			{
				vetDraw[i].move( dx, 0 );
			}
		}
	}

	public final void update( int delta )
	{
		super.update( delta );

		final int nebula1Dx = fisicaNebula1.updateInt( delta );

		if( nebula1.getPosition().x + nebula1Dx < -nebula1.getSize().x )
			nebula1.setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt( ScreenManager.SCREEN_HALF_WIDTH ), rand.nextInt( ScreenManager.SCREEN_HALF_HEIGHT ) );
		else
			nebula1.move( nebula1Dx, 0 );

		final int nebula2Dx = fisicaNebula2.updateInt( delta );
		
		if( nebula2.getPosition().x + nebula2Dx < -nebula2.getSize().x )
			nebula2.setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt( ScreenManager.SCREEN_HALF_WIDTH ), ScreenManager.SCREEN_HEIGHT - nebula2.getSize().y );
		else
			nebula2.move( nebula2Dx, 0 );

		updateStarArray( smallStar, fisicaSmallStar, DELAYSMALL, delta );
		updateStarArray( bigStar, fisicaBigStar, DELAYBIG, delta );

		space.setFillColor( 0x000000 );
		space.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
}
