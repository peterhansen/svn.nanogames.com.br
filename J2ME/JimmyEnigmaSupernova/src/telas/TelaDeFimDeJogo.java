package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.Constants;

/**
 *
 * @author malvezzi
 */

public final class TelaDeFimDeJogo extends Menu implements Constants
{
	/** �ndice do label "Novo Recorde" */
	private final int MENU_NEW_RECORD_LB_IND = 1;

	/** Define qual o �ndice do primeiro label da cole��o que realmente � uma op��o do menu */
	private int menuFirstOptInd = 3;
	
	/** Auxiliar de anima��o */
	private int newRecordLabelBlinkCounter;
	
	// Fim de Jogo
	public static final byte OPT_ENDGAME_RETRY		= 0;
	public static final byte OPT_ENDGAME_RECORD		= 1;
	public static final byte OPT_ENDGAME_BACK		= 2;

	/** Creates a new instance of TelaDeFimDeJogo */
	public TelaDeFimDeJogo( MenuListener listener, int id ) throws Exception
	{
		super( listener, id, 6 );

		// Op��es do menu
		String[] options = { MeuMidlet.getText( TEXT_GAMEOVER ), MeuMidlet.lastScoreEntryIndex >= TOTAL_RECORDS ? null : MeuMidlet.getText( TEXT_NEWRECORD ),
							 String.valueOf( MeuMidlet.getScore() ), MeuMidlet.getText( TEXT_RETRY ),
							 MeuMidlet.getText( TEXT_RECORDS ), MeuMidlet.getText( TEXT_BACK ) };
		
		int[] spacement = { MENUS_ITEMS_SPACING << 1, MENUS_ITEMS_SPACING, MENUS_ITEMS_SPACING << 1, MENUS_ITEMS_SPACING, MENUS_ITEMS_SPACING, MENUS_ITEMS_SPACING };
		
		int y = MENUS_ITEMS_SPACING;
		for( int i = 0 ; i < drawables.length ; ++i )
		{
			if( options[i] != null )
			{
				final Label label = new Label( MeuMidlet.fontMain, options[i] );
				insertDrawable( label );

				label.defineReferencePixel( label.getSize().x >> 1, 0 );
				label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, y );
				y += label.getSize().y + spacement[i];
			}
			else
			{
				--menuFirstOptInd;
			}
		}
		
		// Configura o menu
		setSize( ScreenManager.SCREEN_WIDTH, y );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		
		setCursor( MeuMidlet.LoadCursor(), Menu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT | Graphics.VCENTER );
		setCurrentIndex( menuFirstOptInd );
		
		setCircular( true );
	}

	
	// Evita que o cursor passe pelos labels do menu que n�o s�o op��es
	public final void setCurrentIndex( int index )
	{
		if( index < menuFirstOptInd )
		{
			if( currentIndex < menuFirstOptInd || currentIndex == ( activeDrawables - 1 ) )
				index = menuFirstOptInd;
			else
				index = activeDrawables - 1;
		}

		super.setCurrentIndex( index );
	}
	
	
	public final void keyReleased( int key )
	{
	}
	
	
	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				// Nesses 3 casos, consideramos que o usu�rio selecionou a op��o voltar
				listener.onChoose( null, menuId, OPT_ENDGAME_BACK );
				break;
				
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_SOFT_LEFT:
				listener.onChoose( null, menuId, currentIndex - menuFirstOptInd );
				break;
				
			default:
				super.keyPressed( key );
		}
	}
		
		
	public final void update( int delta )
	{
		if( MeuMidlet.lastScoreEntryIndex < TOTAL_RECORDS )
		{
			newRecordLabelBlinkCounter += delta;

			if( newRecordLabelBlinkCounter > SCOREBLINKDELAY )
			{
				newRecordLabelBlinkCounter = 0;

				Drawable newRecordLabel = getDrawable( MENU_NEW_RECORD_LB_IND );
				newRecordLabel.setVisible( !newRecordLabel.isVisible() );
			}
		}
		super.update( delta );
	}
}
