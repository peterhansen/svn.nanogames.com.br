/**
SetLanguageScreen.java
Created on October 8, 2007, 11:14 PM
 */
/** @author Daniel */
package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import jogo.Constants;
import jogo.MeuMidlet;

public final class SetLanguageScreen extends Menu implements Constants, ScreenListener
{

	/** Creates a new instance of SetLanguageScreen */
	public SetLanguageScreen( MenuListener listener, ImageFont font, int id ) throws Exception
	{
		// Chama o construtor da superclasse
		super( listener, id, LANGUAGE_TOTAL );

		// Idiomas dispon�veis para a sele��o
		final String[] langs = { "ENGLISH", "PORTUGU�S", "ESPA�OL" };

		// Bandeiras dos idiomas dispon�veis
		final DrawableImage[] flags = new DrawableImage[3];
		flags[0] = new DrawableImage( IMG_PATH_FLAG_EN );
		flags[1] = new DrawableImage( IMG_PATH_FLAG_BR );
		flags[2] = new DrawableImage( IMG_PATH_FLAG_ES );

		// Cria os labels que ir�o exibir as op��es de idioma
		RichLabel label;
		int y = 0;
		for( int i = 0; i < langs.length; ++i )
		{
			label = new RichLabel( font, langs[i] + " <ALN_V><IMG_0>", 0, new DrawableImage[]{ flags[i] } );
			insertDrawable( label );

			label.defineReferencePixel( label.getSize().x >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, y );
			y += label.getSize().y + MENUS_ITEMS_SPACING;
		}
		y -= MENUS_ITEMS_SPACING;

		// Configura o menu
		setSize( ScreenManager.SCREEN_WIDTH, y );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

		setCircular( true );

		// Cria e configura o cursor do menu
		setCursor( MeuMidlet.LoadCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );
	}

	public final void update( int delta )
	{
		final Drawable bkg = ScreenManager.getInstance().getBackground();
		if( bkg.getSize().y < ScreenManager.SCREEN_HEIGHT )
			sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		super.update( delta );
	}
	
	
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				MeuMidlet.exit();
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				super.keyPressed( ScreenManager.DOWN );
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				super.keyPressed( ScreenManager.UP );
				break;

			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
				break;

			default:
				super.keyPressed( key );
		}
	}

	public void hideNotify()
	{
	}

	public void showNotify()
	{
	}

	public void sizeChanged( int width, int height )
	{
		// Reconfigura o background
		final Drawable bkg = ScreenManager.getInstance().getBackground();
		bkg.setSize( width, height );
		
		// Reposiciona os labels que est�o exibindo as op��es de idioma
		RichLabel label;
		int y = 0, halfWidth = width >> 1, halfHeight = height >> 1;
		
		for( int i = 0; i < LANGUAGE_TOTAL; ++i )
		{
			label = ( RichLabel )getDrawable( i );

			label.defineReferencePixel( label.getSize().x >> 1, 0 );
			label.setRefPixelPosition( halfWidth, y );
			y += label.getSize().y + MENUS_ITEMS_SPACING;
		}
		y -= MENUS_ITEMS_SPACING;

		// Reconfigura o menu
		setSize( width, y );
		defineReferencePixel( halfWidth, size.y >> 1 );
		setRefPixelPosition( halfWidth, halfHeight );
		
		updateCursorPosition();
	}
}
