/**
 MainMenu.java
 Created on October 11, 2007, 12:32 AM
 */

/** @author Daniel */

package telas;

//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import jogo.Constants;
import jogo.MeuMidlet;

public final class MainMenu extends Menu implements Constants
{
	// N�mero de op��es no menu principal
	//#if LOG == "true"
//# 		static final byte MAIN_MENU_N_OPTIONS = 7;
	//#else
		static final byte MAIN_MENU_N_OPTIONS = 6;
	//#endif
	
	// Indica se a op��o de continuar est� sendo exibida na tela
	private boolean showingContinueOpt;
	
	/** Creates a new instance of MainMenu */
	public MainMenu( MenuListener listener, ImageFont font, int id, boolean canContinue ) throws Exception
	{
		// Chama o construtor da superclasse
		super( listener, id, MAIN_MENU_N_OPTIONS + ( canContinue ? 1 : 0 ) );
		showingContinueOpt = canContinue;
		
		// Op��es do menu
		//#if LOG == "true"
//# 		String[] options = { MeuMidlet.getText( TEXT_NEW_GAME ), null, MeuMidlet.getText( TEXT_OPTIONS ),
//# 							 MeuMidlet.getText( TEXT_RECORDS ), MeuMidlet.getText( TEXT_HELP ),
//# 							 MeuMidlet.getText( TEXT_CREDITS ), MeuMidlet.getText( TEXT_EXIT ), "LOG" };
		//#else
		String[] options = { MeuMidlet.getText( TEXT_NEW_GAME ), null, MeuMidlet.getText( TEXT_OPTIONS ),
							 MeuMidlet.getText( TEXT_RECORDS ), MeuMidlet.getText( TEXT_HELP ),
							 MeuMidlet.getText( TEXT_CREDITS ), MeuMidlet.getText( TEXT_EXIT ) };
		//#endif
		
		if( canContinue )
			options[1] = MeuMidlet.getText( TEXT_CONTINUE );
		
		// Cria os labels que ir�o exibir as op��es do menu principal
		Label label;
		int y = MENUS_ITEMS_SPACING;
		for( int i = 0; i < options.length; ++i )
		{
			if( options[i] != null )
			{
				label = new Label( font, options[ i ] );
				insertDrawable( label );

				label.defineReferencePixel( label.getSize().x >> 1, 0 );
				label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, y );
				y += label.getSize().y + MENUS_ITEMS_SPACING;
			}
		}		
		
		// Configura o menu
		setSize( ScreenManager.SCREEN_WIDTH, y );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		
		setCircular( true );
		
		// Cria e configura o cursor do menu
		setCursor( MeuMidlet.LoadCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.LEFT | Graphics.VCENTER );
		setCurrentIndex( 0 );
	}
	
	
	public final void keyPressed( int key )
	{
		// Por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				MeuMidlet.exit();
			break;
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				super.keyPressed( ScreenManager.DOWN );
				break;
				
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				super.keyPressed( ScreenManager.UP );
				break;
				
			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
				break;

			default:
				super.keyPressed( key );
		}
	}
	
	public final boolean hasContinueOpt()
	{
		return showingContinueOpt;
	}
}
