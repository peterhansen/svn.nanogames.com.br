package telas;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import jogo.MeuMidlet;
import jogo.Constants;

/**
 *
 * @author malvezzi
 */

public class TelaDeSplash extends UpdatableGroup implements KeyListener, Constants
{
	/** Tempo de dura��o do splash */
	private int timeCont = GAME_SPLASH_TIMECONT;
	
	/** Quanto de altura do Jimmy Neutron deve ficar atr�s do logo */
	//#if SCREEN_SIZE == "MEDIUM"
	private int JIMMY_HEIGHT_BEHIND_LOGO = 40;
	//#elif SCREEN_SIZE == "SMALL"
//# 	private int JIMMY_HEIGHT_BEHIND_LOGO = 32;
	//#elif SCREEN_SIZE == "LANDSCAPE"
//# 		private int JIMMY_HEIGHT_BEHIND_LOGO = 25;
	//#endif
	
	/** Quanto podemos ignorar da parte de baixo da imagem do Jimmy Neutron */
	//#if SCREEN_SIZE == "MEDIUM"
	private int JIMMY_HEIGHT_IGNORABLE = 25;
	//#elif SCREEN_SIZE == "SMALL"
//# 	private int JIMMY_HEIGHT_IGNORABLE = 30;
	//#elif SCREEN_SIZE == "LANDSCAPE"
//# 		private int JIMMY_HEIGHT_IGNORABLE = 0;
	//#endif

	/** Creates a new instance of TelaDeSplash */
	public TelaDeSplash() throws Exception
	{
		// Inicializa a superclasse
		super( 3 );
		
		// Determina o tamanho do componente
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// Aloca as imagens que comp�em o splash
		DrawableImage jimmy	= new DrawableImage( IMG_PATH_GAMESPLASH + IMG_PATH_GAMESPLASH_JIMMY );
		insertDrawable( jimmy );
		
		// Determina o logo e o t�tulo do jogo de acordo com o idioma escolhido
		final String[] languageSuffix = { "_en", "_br", "_es" };
		
		DrawableImage titulo = new DrawableImage( IMG_PATH_GAMESPLASH + IMG_PATH_GAMESPLASH_DIVERSAO + languageSuffix[MeuMidlet.getCurrLanguageIndex()] + ".png" );
		insertDrawable( titulo );
		
		DrawableImage logoJimmy = new DrawableImage( IMG_PATH_GAMESPLASH + IMG_PATH_GAMESPLASH_LOGO_JIMMY + languageSuffix[MeuMidlet.getCurrLanguageIndex()] + ".png" );
		insertDrawable( logoJimmy );

		// Posiciona as imagens
		final int totalHeight = logoJimmy.getSize().y + jimmy.getSize().y - JIMMY_HEIGHT_BEHIND_LOGO - JIMMY_HEIGHT_IGNORABLE;
		logoJimmy.setPosition( ( ScreenManager.SCREEN_WIDTH - logoJimmy.getSize().x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - totalHeight ) >> 1 );
		
		// Trata algumas telas irritantes
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
		final byte DIST_BETWEEN_TOP_N_LOGO = 1;
		//#elif SCREEN_SIZE == "SMALL"
//# 		final byte DIST_BETWEEN_TOP_N_LOGO = 3;
		//#endif

		if( logoJimmy.getPosition().y < 0 )
			logoJimmy.move( 0, ( -logoJimmy.getPosition().y ) + DIST_BETWEEN_TOP_N_LOGO );
		
		final int halfLogoPosX = logoJimmy.getPosition().x + ( logoJimmy.getSize().x >> 1 );
		//#if SCREEN_SIZE == "LANDSCAPE"
//# 			final byte DIST_FROM_TITLE_N_JIMMY = 10;
//# 			jimmy.setPosition( halfLogoPosX + ( DIST_FROM_TITLE_N_JIMMY >> 1 ), logoJimmy.getPosition().y + logoJimmy.getSize().y - JIMMY_HEIGHT_BEHIND_LOGO );
		//#else
		jimmy.setPosition( halfLogoPosX, logoJimmy.getPosition().y + logoJimmy.getSize().y - JIMMY_HEIGHT_BEHIND_LOGO );
		//#endif
		
		//#if SCREEN_SIZE == "SMALL"
//# 		// A imagem do Jimmy fica na diagonal. Ent�o, em telas curtas, temos a impress�o de que
//# 		// ela n�o est� bem posicionada no espa�o vazio. Por isso movo o sprite um pouco para a
//# 		// direita com o intuito de obter um melhor resultado
//# 		final byte MAGIC_NUM = 5; // Dessa vez � isso mesmo... Juro que n�o achei um nome melhor
//# 		final byte SHORTEST = 116;
//# 		if( ScreenManager.SCREEN_HEIGHT <= SHORTEST )
//# 		jimmy.move( MAGIC_NUM, 0 );
		//#endif
		
		//#if SCREEN_SIZE == "MEDIUM"
		titulo.setPosition( halfLogoPosX - titulo.getSize().x, jimmy.getPosition().y + ( ( jimmy.getSize().y - titulo.getSize().y ) >> 1 ) );
		//#elif SCREEN_SIZE == "SMALL"
//# 		titulo.setPosition( halfLogoPosX - titulo.getSize().x, ( jimmy.getPosition().y + ( ( jimmy.getSize().y - titulo.getSize().y ) >> 1 ) ) - ( titulo.getSize().y / 10 ) );
		//#elif SCREEN_SIZE == "LANDSCAPE"
//# 			titulo.setPosition( halfLogoPosX - titulo.getSize().x - ( DIST_FROM_TITLE_N_JIMMY >> 1 ), ( jimmy.getPosition().y + ( ( jimmy.getSize().y - titulo.getSize().y ) >> 1 ) ) - ( titulo.getSize().y / 10 ) );
		//#endif
	}
	
	
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				MeuMidlet.exit();
			break;
			
			// Os splashs n�o podem ser acelerados pelo usu�rio
			//#if DEBUG == "true"
			default:
				callNextScreen();
			//#endif
		}
	}
	
	
	public void keyReleased( int key )
	{
	}
	
	
	public void update( int delta )
	{
		super.update( delta );
		
		if( timeCont < 0 )
			callNextScreen();
		timeCont -=delta;
	}
	
	
	private final void callNextScreen()
	{
		( ( MeuMidlet )MeuMidlet.getInstance() ).setScreen( SCREEN_MAIN_MENU );
	}
}
