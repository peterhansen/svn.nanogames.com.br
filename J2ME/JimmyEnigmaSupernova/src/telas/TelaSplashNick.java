/*
 * TelaSplashNick.java
 *
 * Created on June 18, 2007, 7:35 PM
 *
 */

package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import javax.microedition.lcdui.Image;
import jogo.Constants;
import jogo.MeuMidlet;

/**
 *
 * @author peter
 */
public final class TelaSplashNick extends UpdatableGroup implements KeyListener, Constants
{
	/** Tempo de dura��o, em milisegundos,do splash da Nick */
	private final int TOTAL_LOGO_TIME = 3000;
	
	private int accTime;

	/** Construtor */
	public TelaSplashNick() throws Exception
	{
		super( 3 );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final DrawableImage logo = new DrawableImage( IMG_PATH_SPLASH + "nick.png" );
		insertDrawable( logo );
		
		final RichLabel label = new RichLabel( ImageFont.createMultiSpacedFont( Image.createImage( IMG_PATH_SPLASH + "font_nick.png" ), IMG_PATH_SPLASH + "font_credits.dat" ), "", 0, null );
		insertDrawable( label );
		
		int labelWidth = size.x * 3 / 4;
		label.setSize( labelWidth, 0 );
		label.setText( MeuMidlet.getText( TEXT_SPLASH_NICK ) );
		int labelHeight = label.getTextTotalHeight();
		label.setSize( labelWidth, labelHeight );
		label.setPosition( ( size.x - label.getSize().x ) >> 1, size.y - labelHeight );

		// Centraliza o logo na �rea vazia da tela
		logo.setPosition( ( size.x - logo.getSize().x ) >> 1, ( ( size.y - labelHeight )- logo.getSize().y ) >> 1 );
	}
	
	
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				MeuMidlet.exit();
			break;
			
			// Os splashs n�o podem ser acelerados pelo usu�rio
			//#if DEBUG == "true"
			default:
				callNextScreen();
			//#endif
		}
	}
	
	
	public final void update( int delta )
	{
		accTime += delta;
		
		if ( accTime >= TOTAL_LOGO_TIME )
			callNextScreen();
	}
	
	
	private final void callNextScreen()
	{
		( ( MeuMidlet )MeuMidlet.getInstance() ).setScreen( SCREEN_SPLASH_GAME );
	}

	
	public final void keyReleased( int key )
	{
	}
}
