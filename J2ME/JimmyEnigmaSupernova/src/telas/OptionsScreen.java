/*
 * OptionsScreen.java
 *
 * Created on June 4, 2007, 5:00 PM
 *
 */

package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import jogo.MeuMidlet;
import jogo.Constants;

/**
 *
 * @author peter
 */

public final class OptionsScreen extends Menu implements Constants
{
	// �ndices dos labels na cole��o
	public final byte INDEX_OPTIONS		= 0;
	public final byte INDEX_SOUND		= 1;
	public final byte INDEX_VIBRATION	= 2;
	public final byte INDEX_LANGUAGE	= 3;
	public final byte INDEX_BACK		= 4;
	
	// N�mero total de labels na cole��o
	private static final byte TOTAL_LABELS = 5;
	
	// N�mero de slots na sole��o
	private static final byte TOTAL_ITEMS = TOTAL_LABELS + 1;
	
	//#if SCREEN_SIZE == "SMALL"
//# 	
//# 		// �cone que representa o estado do som ( ligado / desligado )
//# 		private final byte ICON_SOUND_ON = 0;
//# 		private final byte ICON_SOUND_OFF = 1;
//# 		
//# 		private final Sprite toggleSoundIcon;
//# 
//# 		// �cone que representa o estado da vibra��o ( ligada / desligada )
//# 		private final byte ICON_VIB_ON = 0;
//# 		private final byte ICON_VIB_OFF = 1;
//# 		
//# 		private final Sprite toggleVibIcon;
//# 	
	//#endif
		
	/** �cone das bandeiras dos idiomas */
	private final Sprite flagsIcon;
	
	/** �ltima vez que disparamos um evento de tecla nesta tela */
	private long lastKeyPressedEvent = System.currentTimeMillis();
	
	/** Intervalo atual entre eventos de keyPressed */
	private long interval = 0;
	
	/** Intervalo m�nimo entre os eventos de (des)ativa��o de sons e vibra��o */
	private static int MIN_VIB_N_SOUND_TOGGLE_INTERVAL = 400;
	
	/** Creates a new instance of OptionsScreen */
	public OptionsScreen( MenuListener listener, ImageFont font, int id ) throws Exception
	{
		// Chama o construtor da superclasse
		super( listener, id, TOTAL_ITEMS );

		// Determina um tamanho provis�rio apenas para o c�lculo dos tamanhos dos labels
		setSize( ScreenManager.SCREEN_WIDTH, 0 );
		
		// Cria e posiciona os labels das op��es do menu
		int y = 0;
		String[] texts = getMenuStrings();
		
		// Aloca o �cone das bandeiras
		flagsIcon = new Sprite(	new Image[]{	Image.createImage( IMG_PATH_FLAG_EN ),
												Image.createImage( IMG_PATH_FLAG_BR ),
												Image.createImage( IMG_PATH_FLAG_ES ),
										   },
								new byte[][]{ { 0, 1, 2 } },
								new short[]{ 0 }
							  );
		
		// Exibe a bandeira do idioma atual
		flagsIcon.setFrame( MeuMidlet.getCurrLanguageIndex() );
		
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
		
			// Organiza tudo para o looping
			final Drawable[][] specialChars = { null, null, null, { flagsIcon }, null };
			
		//#elif SCREEN_SIZE == "SMALL"
//# 		
//# 			// Aloca o �cone de som
//# 			toggleSoundIcon = new Sprite(	new Image[]{	Image.createImage( IMG_PATH_ICONS + IMG_PATH_ICON_SOUND_ON ),
//# 															Image.createImage( IMG_PATH_ICONS + IMG_PATH_ICON_SOUND_OFF )
//# 													   },
//# 											new byte[][]{ { 0, 1 } },
//# 											new short[]{ 0 }
//# 										);
//# 			toggleSoundIcon.setFrame( MediaPlayer.isMuted() ? ICON_SOUND_OFF : ICON_SOUND_ON );
//# 			
//# 			// Aloca o �cone de vibra��o
//# 			toggleVibIcon = new Sprite(	new Image[]{	Image.createImage( IMG_PATH_ICONS + IMG_PATH_ICON_VIB_ON ),
//# 														Image.createImage( IMG_PATH_ICONS + IMG_PATH_ICON_VIB_OFF )
//# 												   },
//# 										new byte[][]{ { 0, 1 } },
//# 										new short[]{ 0 }
//# 									  );
//# 			toggleVibIcon.setFrame( MediaPlayer.isVibration() ? ICON_VIB_ON : ICON_VIB_OFF );
//# 		
//# 			// Organiza tudo para o looping
//# 			final Drawable[][] specialChars = { null, { toggleSoundIcon }, { toggleVibIcon }, { flagsIcon }, null };
//# 			
		//#endif
			

		
		for( int i = 0 ; i < TOTAL_LABELS ; ++i )
		{
			final RichLabel l = new RichLabel( font, texts[ i ], 0, specialChars[i] );
			l.defineReferencePixel( l.getSize().x >> 1, 0 );
			l.setRefPixelPosition( size.x >> 1, y );
			insertDrawable( l );

			y += l.getSize().y + MENUS_ITEMS_SPACING;

			if( i == 0 )
				y += MENUS_ITEMS_SPACING;
		}

		// Determina o tamanho final do componente
		setSize( size.x, y );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		
		// Cria o cursor do menu
		final Drawable menuCursor = MeuMidlet.LoadCursor();
		setCursor( menuCursor, Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );
		
		// Configura o menu
		setCircular( true );
		setCurrentIndex( 0 );
	}
	
	
	private final String[] getMenuStrings()
	{
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )

		return new String[] {
								MeuMidlet.getText( TEXT_OPTIONS ),
								MeuMidlet.getText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF ),
								MeuMidlet.getText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON ),
								MeuMidlet.getText( TEXT_LANGUAGE ) + " " + MeuMidlet.getText( TEXT_CURR_LANGUAGE ) + " <ALN_V><IMG_0>",
								MeuMidlet.getText( TEXT_BACK )
							};
		
		//#elif SCREEN_SIZE == "SMALL"
//# 		
//# 		return new String[] {
//# 								MeuMidlet.getText( TEXT_OPTIONS ),
//# 								MeuMidlet.getText( TEXT_TOGGLE_SOUND ) + " <ALN_V><IMG_0>",
//# 								MeuMidlet.getText( TEXT_TOGGLE_VIBRATION ) + " <ALN_V><IMG_0>",
//# 								MeuMidlet.getText( TEXT_CURR_LANGUAGE ) + " <ALN_V><IMG_0>",
//# 								MeuMidlet.getText( TEXT_BACK )
//# 							};
//# 		
		//#endif
	}
	
	
	public final void keyPressed( int key )
	{
		interval += System.currentTimeMillis() - lastKeyPressedEvent;
		lastKeyPressedEvent = System.currentTimeMillis();
		
		switch( key )
		{
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_SOFT_LEFT:
				switch( currentIndex )
				{
					case INDEX_SOUND:
						if( interval > MIN_VIB_N_SOUND_TOGGLE_INTERVAL )
						{
							interval = 0;
							toggleSound();
						}
						break;
						
					case INDEX_VIBRATION:
						if( interval > MIN_VIB_N_SOUND_TOGGLE_INTERVAL )
						{
							interval = 0;
							toggleVibration();
						}
						break;
						
					case INDEX_LANGUAGE:
						changeLanguage( ( key != ScreenManager.LEFT ) && ( key != ScreenManager.KEY_NUM4 ) );
						break;
						
					case INDEX_BACK:
						if( ( key == ScreenManager.FIRE ) || ( key == ScreenManager.KEY_NUM5 ) )
							MeuMidlet.setScreen( SCREEN_MAIN_MENU );
						return;
				}
				break;
				
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				MeuMidlet.setScreen( SCREEN_MAIN_MENU );
				return;

			default:
				super.keyPressed( key );
		}
	}
	
	
	private final void toggleSound()
	{
		if( MediaPlayer.isMuted() )
		{
			MediaPlayer.setMute( false );
			
			//#if SAMSUNG_BAD_SOUND == "true"
//# 				MediaPlayer.play( SOUND_IND_THEME, MeuMidlet.SAMSUNG_BS_LOOP_INFINITE );
			//#else
				MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
			//#endif
			
			//#if SCREEN_SIZE == "SMALL"
//# 				toggleSoundIcon.setFrame( ICON_SOUND_ON );
			//#endif
		}
		else
		{
			MediaPlayer.setMute( true );
			MediaPlayer.stop();
			MediaPlayer.free();
			
			//#if SCREEN_SIZE == "SMALL"
//# 				toggleSoundIcon.setFrame( ICON_SOUND_OFF );
			//#endif
		}
		resetLabels();
	}
	
	
	private final void toggleVibration()
	{
		MediaPlayer.setVibration( !MediaPlayer.isVibration() );

		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
		if( MediaPlayer.isVibration() )
			MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
		//#elif SCREEN_SIZE == "SMALL"
//# 		if( MediaPlayer.isVibration() )
//# 		{
//# 			MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
//# 			
//# 			toggleVibIcon.setFrame( ICON_VIB_ON );
//# 		}
//# 		else
//# 		{
//# 			toggleVibIcon.setFrame( ICON_VIB_OFF );
//# 		}
		//#endif
		
		resetLabels();
	}
	
	
	private final void changeLanguage( boolean next )
	{
		// Carrega os textos
		try
		{
			if( next )
				MeuMidlet.setLanguage( ( byte )( ( MeuMidlet.getCurrLanguageIndex() + 1 ) % LANGUAGE_TOTAL ) );
			else
				MeuMidlet.setLanguage( ( byte )( ( MeuMidlet.getCurrLanguageIndex() + LANGUAGE_TOTAL - 1 ) % LANGUAGE_TOTAL ) );

			flagsIcon.setFrame( MeuMidlet.getCurrLanguageIndex() );

			resetLabels();
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
				
			// Se n�o conseguiu mudar o idioma, deixa o usu�rio reclamando
		}
	}
	
	
	private final void resetLabels()
	{
		try
		{
			// Reseta o texto dos labels
			String[] texts = getMenuStrings();
			
			for( int i = 0 ; i < TOTAL_LABELS ; ++i )
			{
				final RichLabel l = ( RichLabel ) getDrawable( i );
				l.setText( texts[ i ], true, true );
				l.defineReferencePixel( l.getSize().x >> 1, 0 );
				l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
			}
			
			// Reposiciona o cursor, pois largura de uma das op��es pode ter mudado
			updateCursorPosition();
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}
	
	
	// Evita que o cursor passe pelo t�tulo do menu
	public final void setCurrentIndex( int index )
	{
		if( index == 0 )
		{
			if( currentIndex == 0 || currentIndex == ( activeDrawables - 1 ) )
				index = 1;
			else
				index = activeDrawables - 1;
		}
		super.setCurrentIndex( index );
	}
}
