package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.Constants;
import jogo.Tabuleiro;
import jogo.Transition;
import jogo.TransitionListener;

/*
 * TelaDeJogo.java
 *
 * Created on June 15, 2007, 5:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
/**
 *
 * @author malvezzi
 */
public final class TelaDeJogo extends UpdatableGroup implements PointerListener, KeyListener, Constants, TransitionListener
{
	private static int scoreShown;
	private int dv;
	private int dva;
	private final Tabuleiro tabuleiro;
	private final Label lbPontos;
	
	//#if ( SCREEN_SIZE == "MEDIUM" )
		private final Label lbFase;
	//#endif
	
	private final Label lbTirosRestantes;
	private Transition myTransition = null;

	/** Creates a new instance of TelaDeJogo */
	public TelaDeJogo() throws Exception
	{
		super( 6 );

		MeuMidlet.lastScoreEntryIndex = TOTAL_RECORDS;

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		// A vari�vel tabuleiro precisa de lbPontos e lbTirosRestantes criados
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
		
		lbPontos = new Label( MeuMidlet.fontMain, MeuMidlet.getText( MeuMidlet.TEXT_SCORE ) + ": " + MAX_POINTS );
		lbTirosRestantes = new Label( MeuMidlet.fontMain, MeuMidlet.getText( MeuMidlet.TEXT_SHOTS ) + ": " + MAX_SHOTS );
			
		//#elif SCREEN_SIZE == "SMALL"
//# 			
//# 			lbPontos = new Label( MeuMidlet.fontMain, String.valueOf( MAX_POINTS ) );
//# 			lbTirosRestantes = new Label( MeuMidlet.fontMain, String.valueOf( MAX_SHOTS ) );
//# 			
		//#endif
		
		tabuleiro = new Tabuleiro( this );
		insertDrawable( tabuleiro );
		
		DrawableImage placar = new DrawableImage( IMG_PATH_TELADEJOGO + "placar.png" );
		Thread.yield();
 		insertDrawable( placar );
		
		placar.setPosition( 0, ScreenManager.SCREEN_HEIGHT - placar.getSize().y );

		//#if SCREEN_SIZE == "MEDIUM"
		tabuleiro.setRefPixelPosition( ( size.x >> 1 ), ( size.y >> 1 ) - ( tabuleiro.getSize().y / 5 ) );
		if( tabuleiro.getPosition().y < 0 )
			tabuleiro.move( 0, -tabuleiro.getPosition().y );
		//#elif ( SCREEN_SIZE == "SMALL" ) || ( SCREEN_SIZE == "LANDSCAPE" )
//# 			tabuleiro.setRefPixelPosition( ( size.x - tabuleiro.getSize().x ) >> 1 , ( ( size.y - placar.getSize().y ) - tabuleiro.getSize().y ) >> 1 );
		//#endif

		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )

		DrawableImage jimmy = new DrawableImage( IMG_PATH_TELADEJOGO + "jimmy_game_play.png" );
		insertDrawable( jimmy );
		Thread.yield();

		jimmy.setPosition( 0, ScreenManager.SCREEN_HEIGHT - jimmy.getSize().y );
			
		//#elif SCREEN_SIZE == "SMALL"
//# 			
//# 		DrawableImage iconShots = new DrawableImage( IMG_PATH_TELADEJOGO + "icon_shots.png" );
//# 		Thread.yield();
//# 		insertDrawable( iconShots );
//# 		
//# 		DrawableImage iconPoints = new DrawableImage( IMG_PATH_TELADEJOGO + "icon_points.png" );
//# 		Thread.yield();
//# 		insertDrawable( iconPoints );
//# 
		//#endif
		
		insertDrawable( lbPontos );
		insertDrawable( lbTirosRestantes );

		//#if ( SCREEN_SIZE == "MEDIUM" )
		
		lbPontos.setPosition( ( placar.getPosition().x + placar.getSize().x - SPACER_LABEL_X ) - lbPontos.getSize().x, ScreenManager.SCREEN_HEIGHT - ( lbPontos.getSize().y << 1 ) + SPACER_LABEL_Y );
		lbTirosRestantes.setPosition( lbPontos.getPosition().x, ScreenManager.SCREEN_HEIGHT - lbTirosRestantes.getSize().y );
		
		// Cria o label indicativo da fase
		lbFase = new Label( MeuMidlet.fontMain, "" );
		insertDrawable( lbFase );
			
		lbFase.setText( MeuMidlet.getText( MeuMidlet.TEXT_LEVEL ) + ": " + STARTING_LEVEL );
		lbFase.setPosition( tabuleiro.getPosition().x + tabuleiro.getSize().x - lbFase.getSize().x, tabuleiro.getPosition().y + tabuleiro.getSize().y + SPACER_LABEL_Y );
		Thread.yield();
			
		//#elif SCREEN_SIZE == "SMALL"
//# 		
//# 		final byte SPACE_BETWEEN_WEAPON_N_LABEL = -3;
//# 		final byte SPACE_BETWEEN_LABEL_N_POINTS_ICON = 6;
//# 		final byte SPACE_BETWEEN_POINTS_ICON_N_LABEL = 4;
//# 
//# 		iconShots.setPosition( 0, size.y - iconShots.getSize().y );
//# 			
//# 		int aux = iconShots.getSize().x + SPACE_BETWEEN_WEAPON_N_LABEL;
//# 		lbTirosRestantes.setPosition( aux, size.y - lbTirosRestantes.getSize().y );
//# 
//# 		// Serve para retirar o espa�o de 1 pixel inclu�do na imagem da fonte
//# 		final byte FONT_SPACE = 1;
//# 			
//# 		aux += lbTirosRestantes.getSize().x - FONT_SPACE + SPACE_BETWEEN_LABEL_N_POINTS_ICON;
//# 		iconPoints.setPosition( aux, size.y - iconPoints.getSize().y );
//# 		lbPontos.setPosition( aux + iconPoints.getSize().x + SPACE_BETWEEN_POINTS_ICON_N_LABEL, size.y - lbPontos.getSize().y );
//# 		
//# 		Thread.yield();
//# 
		//#elif SCREEN_SIZE == "LANDSCAPE"
//# 		
//# 			final byte LABELS_DIST_FROM_BOTTOM = 1;
//# 			final byte SPACE_BETWEEN_JIMMY_N_SHOTS = -12;
//# 			final byte SPACE_BETWEEN_LABELS = 6;
//# 
//# 			int aux = jimmy.getSize().x + SPACE_BETWEEN_JIMMY_N_SHOTS;
//# 			lbTirosRestantes.setPosition( aux, size.y - lbPontos.getSize().y - LABELS_DIST_FROM_BOTTOM );
//# 			
//# 			aux += lbTirosRestantes.getSize().x + SPACE_BETWEEN_LABELS;
//# 			lbPontos.setPosition( aux, size.y - lbTirosRestantes.getSize().y - LABELS_DIST_FROM_BOTTOM );
//#				Thread.yield();
//# 		
		//#endif
		
		myTransition = new Transition( this );
	}

	public final void startLevelOne()
	{
		MeuMidlet.setScore( 0 );
		scoreShown = 0;
		updateScoreLabel();

		tabuleiro.startLevelOne();
	}

	public final void levelEnded( int newLevel )
	{
		MediaPlayer.free();
		MediaPlayer.play( SOUND_IND_CLEARED, 1 );

		myTransition.reset( newLevel );

		MeuMidlet.setSoftKeyIcon( ScreenManager.SOFT_KEY_RIGHT, -1 );
	}

	public final void nextLevel()
	{
		//#if ( SCREEN_SIZE == "MEDIUM" )
		lbFase.setText( MeuMidlet.getText( MeuMidlet.TEXT_LEVEL ) + ": " + tabuleiro.levelCount );
		lbFase.setPosition( tabuleiro.getPosition().x + tabuleiro.getSize().x - lbFase.getSize().x, tabuleiro.getPosition().y + tabuleiro.getSize().y + SPACER_LABEL_Y );
		//#endif

		tabuleiro.nextLevel();
	}

	public final void updateScoreLabel()
	{
		final String pointsStr = String.valueOf( MeuMidlet.getScore() );
		final String maxPointsStr = String.valueOf( MAX_POINTS );
		
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
		String res = MeuMidlet.getText( MeuMidlet.TEXT_SCORE ) + ": ";
		//#elif SCREEN_SIZE == "SMALL"
//# 		String res = "";
		//#endif

		int nSpaces = maxPointsStr.length() - pointsStr.length();
		while( nSpaces > 0 )
		{
			res += "0";
			--nSpaces;
		}

		lbPontos.setText( res + MeuMidlet.getScore(), false );
	}

	public final void updateShotsLabel()
	{
		final String shotsStr = String.valueOf( tabuleiro.getShots() );
		final String maxShotsStr = String.valueOf( Tabuleiro.MAX_SHOTS );
		
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
		String res = MeuMidlet.getText( MeuMidlet.TEXT_SHOTS ) + ": ";
		//#elif SCREEN_SIZE == "SMALL"
//# 		String res = "";
		//#endif

		int nSpaces = maxShotsStr.length() - shotsStr.length();
		while( nSpaces > 0 )
		{
			res += "0";
			--nSpaces;
		}

		lbTirosRestantes.setText( res + tabuleiro.getShots(), false );
	}

	public final void keyPressed( int key )
	{
		if( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED )
			return;

		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				MeuMidlet.setScreen( SCREEN_YN_BACKGAME );
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				tabuleiro.moveCursor( DIR_RIGHT );
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				tabuleiro.moveCursor( DIR_LEFT );
				break;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				tabuleiro.moveCursor( DIR_UP );
				break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				tabuleiro.moveCursor( DIR_DOWN );
				break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				fire();
				break;
		}
	}
	
	private final void fire()
	{	
		tabuleiro.fire();
	}

	public final void keyReleased( int key )
	{
	}

	public final void resetScoreShown()
	{
		scoreShown = 0;
	}

	private final void updateScoreShown( boolean equalize, int delta )
	{
		if( scoreShown < MeuMidlet.getScore() )
		{
			if( equalize )
			{
				scoreShown = MeuMidlet.getScore();
			}
			else
			{
				final short oneSecondInMS = 1000;
				dv = ( dva + ( delta * SCORE_INCREMENT ) ) / oneSecondInMS;
				dva = ( delta * SCORE_INCREMENT ) % oneSecondInMS;
				scoreShown += dv;
				if( scoreShown > MeuMidlet.getScore() )
				{
					scoreShown = MeuMidlet.getScore();
				}
			}
			updateScoreLabel();
		}
	}

	public void update( int delta )
	{
		if( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED )
			myTransition.update( delta );

		super.update( delta );
		updateScoreShown( false, delta );
	}

	public void onClosed()
	{
		nextLevel();
	}

	public void onEnded()
	{
		MeuMidlet.setSoftKeyIcon( ScreenManager.SOFT_KEY_RIGHT, ICON_PAUSE );
	}

	protected void paint( Graphics g )
	{
		super.paint( g );

		if( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED )
			myTransition.draw( g );
	}

	public void onPointerDragged( int x, int y )
	{
		// N�o faz nada
	}

	public void onPointerPressed( int x, int y )
	{
		if( myTransition.getTransitionStep() != myTransition.TRANSITION_STEP_ENDED )
			return;
		
		Point boardPos = tabuleiro.getPosition();
		Point boardSize = tabuleiro.getSize();
			
		// Verifica se colide com o tabuleiro
		if( ( x < boardPos.x ) || ( x > boardPos.x + boardSize.x ) )
			return;
		
		if( ( y < boardPos.y ) || ( y > boardPos.y + boardSize.y ) )
			return;
		
		// Transforma as coordenadas para ficarem relativas ao tabuleiro
		x -= boardPos.x;
		y -= boardPos.y;
		
		// Move o cursor at� a c�lula correta
		final Point boardCellSize = tabuleiro.getBoardCellSize();
		tabuleiro.setCursorAtCell( x / boardCellSize.x, y / boardCellSize.y );
		
		// Atira
		fire();
	}

	public void onPointerReleased( int x, int y )
	{
		// N�o faz nada
	}
}
