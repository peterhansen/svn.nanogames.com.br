/*
 * SplashNano.java
 *
 * Created on June 15, 2007, 11:32 AM
 *
 */
package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import jogo.Constants;
import jogo.MeuMidlet;

/**
 *
 * @author peter
 */

public final class TelaSplashNano extends UpdatableGroup implements Constants, KeyListener
{

	private final int BACKLIGHT_COLOR = 0x1A9DBA;
	private int lightModule;
	//#if SCREEN_SIZE == "SMALL"
//# 		// velocidade do efeito da luz em pixels por segundo
//# 		private final int LIGHT_MOVE_SPEED = 115;
//# 
//# 		private final int LIGHT_X_OFFSET = 0;
//# 		private final int LIGHT_X_LEFT;
//# 		private final int LIGHT_X_RIGHT;	
//# 
//# 		// offsets na posi��o dos logos em rela��o ao texto "Nano"
//# 		private final short IMG_GAMES_OFFSET_X = 26;
//# 		private final short IMG_GAMES_OFFSET_Y = 10;
//# 		private final short IMG_TURTLE_OFFSET_X = 24;
//# 		private final short IMG_TURTLE_OFFSET_Y = -32;	
	//#else
	// velocidade do efeito da luz em pixels por segundo
	private final int LIGHT_MOVE_SPEED = 157;
	private final int LIGHT_X_OFFSET = 8;
	private final int LIGHT_X_LEFT;
	private final int LIGHT_X_RIGHT;

	// offsets na posi��o dos logos em rela��o ao texto "Nano"
	private final short IMG_GAMES_OFFSET_X = 65;
	private final short IMG_GAMES_OFFSET_Y = 25;
	private final short IMG_TURTLE_OFFSET_X = 59;
	private final short IMG_TURTLE_OFFSET_Y = -69;
	//#endif
	private static final byte TOTAL_ITEMS = 9;
	private final DrawableImage imgNano;
	private final DrawableImage imgNanoLight;
	private final DrawableImage imgGames;
	private final DrawableImage imgTurtle;
	private final ImageFont font;
	private final Pattern transparency;
	private final Pattern backColor;
	private final Pattern blackLeft;
	private final Pattern blackRight;
	// estados da anima��o do splash
	private final byte STATE_NANO_APPEARS = 0;
	private final byte STATE_LIGHT_RIGHT = STATE_NANO_APPEARS + 1;
	private final byte STATE_LIGHT_LEFT = STATE_LIGHT_RIGHT + 1;
	private final byte STATE_WAIT = STATE_LIGHT_LEFT + 1;
	private final byte STATE_NONE = STATE_WAIT + 1;
	// dura��o em milisegundos de cada estado de anima��o
	private final short DURATION_NANO_APPEARS = 600;
	private final short DURATION_WAIT = 2100;
	/** pr�xima tela */
	private final byte NEXT_SCREEN_INDEX;

	private byte currentState;
	private int accTime;

	/** Creates a new instance of SplashNano */
	public TelaSplashNano( byte nextScreenIndex ) throws Exception
	{
		super( TOTAL_ITEMS );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		NEXT_SCREEN_INDEX = nextScreenIndex;

		// logo da Nano
		imgNano = new DrawableImage( IMG_PATH_SPLASH + "nano.png" );
		imgNano.defineReferencePixel( imgNano.getSize().x >> 1, 0 );

		//#if SCREEN_SIZE == "SMALL"
//# 			imgNano.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT - 6 );
		//#else
		imgNano.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		//#endif

		LIGHT_X_LEFT = imgNano.getPosition().x + LIGHT_X_OFFSET;
		LIGHT_X_RIGHT = imgNano.getPosition().x + imgNano.getSize().x;

		// efeito da luz passando
		imgNanoLight = new DrawableImage( IMG_PATH_SPLASH + "light.png" );
		imgNanoLight.setPosition( imgNano.getPosition().x - imgNanoLight.getSize().x, imgNano.getPosition().y );

		// cor de fundo do logo
		backColor = new Pattern( null );
		backColor.setFillColor( BACKLIGHT_COLOR );
		backColor.setSize( imgNano.getSize() );
		backColor.setPosition( imgNano.getPosition() );

		// adiciona as �reas pretas � direita e esquerda do texto "Nano", para evitar que o efeito da luz passando fique errado
		blackLeft = new Pattern( null );
		blackLeft.setFillColor( 0x000000 );
		blackLeft.setSize( imgNano.getPosition().x, imgNano.getSize().y );
		blackLeft.setPosition( 0, imgNano.getPosition().y );

		blackRight = new Pattern( null );
		blackRight.setFillColor( 0x000000 );
		blackRight.setSize( ScreenManager.SCREEN_WIDTH - LIGHT_X_RIGHT, imgNano.getSize().y );
		blackRight.defineReferencePixel( blackRight.getSize().x, 0 );
		blackRight.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, imgNano.getPosition().y );

		insertDrawable( backColor );
		insertDrawable( imgNanoLight );
		insertDrawable( blackLeft );
		insertDrawable( blackRight );
		insertDrawable( imgNano );

		imgGames = new DrawableImage( IMG_PATH_SPLASH + "games.png" );
		imgGames.setPosition( imgNano.getPosition().x + IMG_GAMES_OFFSET_X, imgNano.getPosition().y + IMG_GAMES_OFFSET_Y );
		insertDrawable( imgGames );

		imgTurtle = new DrawableImage( IMG_PATH_SPLASH + "turtle.png" );
		imgTurtle.setPosition( imgNano.getPosition().x + IMG_TURTLE_OFFSET_X, imgNano.getPosition().y + IMG_TURTLE_OFFSET_Y );
		insertDrawable( imgTurtle );

		font = ImageFont.createMultiSpacedFont( IMG_PATH_SPLASH + "font_credits.png", IMG_PATH_SPLASH + "font_credits.dat" );

		final RichLabel label = new RichLabel( font, MeuMidlet.getText( TEXT_SPLASH_NANO ), size.x * 3 / 4, null );
		label.setSize( label.getSize().x, label.getTextTotalHeight() );
		label.defineReferencePixel( label.getSize().x >> 1, label.getSize().y );
		label.setRefPixelPosition( size.x >> 1, size.y );
		insertDrawable( label );

		transparency = new Pattern( new DrawableImage( IMG_PATH_SPLASH + "transparency.png" ) );
		transparency.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		transparency.defineReferencePixel( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		transparency.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( transparency );

		setState( STATE_NANO_APPEARS );
	} // fim do construtor SplashNano( String )

	private final void setState( int state )
	{
		switch( state )
		{
			case STATE_NANO_APPEARS:
				break;

			case STATE_LIGHT_RIGHT:
				transparency.setVisible( false );
				
			case STATE_LIGHT_LEFT:
			case STATE_WAIT:
			case STATE_NONE:
				break;

			default:
				return;
		} // fim switch ( state )

		currentState = ( byte ) state;
		lightModule = 0;
		accTime = 0;
	} // fim do m�todo setState( byte )

	public final void update( int delta )
	{
		super.update( delta );

		accTime += delta;
		switch( currentState )
		{
			case STATE_NANO_APPEARS:
				if( accTime >= DURATION_NANO_APPEARS )
				{
					setState( currentState + 1 );
				}
				break;

			case STATE_LIGHT_RIGHT:
				final int dsRight = lightModule + ( LIGHT_MOVE_SPEED * delta );
				final int dxRight = dsRight / 1000;
				lightModule = dsRight % 1000;

				imgNanoLight.move( dxRight, 0 );

				if( imgNanoLight.getPosition().x >= LIGHT_X_RIGHT )
				{
					imgNanoLight.setPosition( LIGHT_X_RIGHT, imgNanoLight.getPosition().y );
					setState( currentState + 1 );
				}
				break;

			case STATE_LIGHT_LEFT:
				final int dsLeft = lightModule - ( LIGHT_MOVE_SPEED * delta );
				final int dxLeft = dsLeft / 1000;
				lightModule = dsLeft % 1000;

				imgNanoLight.move( dxLeft, 0 );

				if( imgNanoLight.getPosition().x <= LIGHT_X_LEFT )
				{
					imgNanoLight.setPosition( LIGHT_X_LEFT, imgNanoLight.getPosition().y );
					setState( currentState + 1 );
				}
				break;

			case STATE_WAIT:
				if( accTime >= DURATION_WAIT )
					callNextScreen();
				break;
		} // fim switch ( state )		
	} // fim do m�todo update( int )
	
	
	private final void callNextScreen()
	{
		setState( STATE_NONE );
		( ( MeuMidlet )MeuMidlet.getInstance() ).setScreen( NEXT_SCREEN_INDEX );
	}

	public void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				MeuMidlet.exit();
			break;
			
			// Os splashs n�o podem ser acelerados pelo usu�rio
			//#if DEBUG == "true"
			default:
				callNextScreen();
			//#endif
		}
	}
	
	public void keyReleased( int key )
	{
	}	
}
