/*
 * YesNoScreen.java
 *
 * Created on May 7, 2007, 12:36 PM
 *
 */

package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.Constants;


/**
 *
 * @author peter
 */
public final class YesNoScreen extends Menu implements Constants, ScreenListener
{
	public static final byte INDEX_TITLE = 0;
	public static final byte INDEX_YES = 1;
	public static final byte INDEX_NO  = 2;
	
	private final boolean exitOnCancelKeys;

	public YesNoScreen( MenuListener listener, int id, ImageFont font, int idTitle, boolean exitOnCancel ) throws Exception
	{
		super( listener, id, 3 );
		exitOnCancelKeys = exitOnCancel;
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		
		// Deixa uma margem de cada lado da tela, para distribuir melhor o texto do t�tulo
		final RichLabel title = new RichLabel( font, "", 0, null );
		title.setSize( ScreenManager.SCREEN_WIDTH * 9 / 10, title.getTextTotalHeight() );
		title.setText( MeuMidlet.getText( idTitle ) );
		title.setSize( title.getSize().x, title.getTextTotalHeight() );
		
		title.defineReferencePixel( title.getSize().x >> 1, title.getSize().y );
		title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( title );
		
		final Drawable cursor = MeuMidlet.LoadCursor();
		final int cursorWidth = cursor.getSize().x + ( MENUS_ITEMS_SPACING >> 1 );
			
		final int y = ScreenManager.SCREEN_HALF_HEIGHT + title.getSize().y;
		
		Label l = new Label( font, MeuMidlet.getText( TEXT_YES ) );
		l.defineReferencePixel( l.getSize().x, 0 );
		l.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH - cursorWidth, y );
		insertDrawable( l );
		
		l = new Label( font, MeuMidlet.getText( TEXT_NO ) );
		l.defineReferencePixel( 0, 0 );
		l.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH + cursorWidth, y );
		insertDrawable( l );
		
		setCursor( cursor, CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );
		
		setCurrentIndex( INDEX_NO );
	}
	
	public final void setCurrentIndex( int index )
	{
		// Pula o t�tulo do menu
		if( index == 0 )
			index = 1;
		
		super.setCurrentIndex( index );
	}
	
	public final void keyPressed( int key )
	{
		final int YES_NO_MENU_N_OPTIONS = 2;
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if( exitOnCancelKeys )
				{
					MeuMidlet.exit();
				}
				else
				{
					setCurrentIndex( INDEX_NO );
					super.keyPressed( ScreenManager.KEY_NUM5 );
				}
				return;
				
			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
				break;
				
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM4:
				// Lembrando que "& 1" � igual a "% 2"
				setCurrentIndex( ( ( currentIndex + YES_NO_MENU_N_OPTIONS ) & 1 ) + 1 );
				break;

			default:
				super.keyPressed( key );
				break;
		}
	}

	public void hideNotify()
	{
	}

	public void showNotify()
	{
	}

	public void update( int delta )
	{
		final Drawable bkg = ScreenManager.getInstance().getBackground();
		if( bkg.getSize().y < ScreenManager.SCREEN_HEIGHT )
			sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		super.update( delta );
	}

	
	public void sizeChanged( int width, int height )
	{
		// Reconfigura o background
		final Drawable bkg = ScreenManager.getInstance().getBackground();
		bkg.setSize( width, height );
		
		// Reconfigura a tela
		setSize( width, height );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		
		// Deixa uma margem de cada lado da tela, para distribuir melhor o texto do t�tulo
		final RichLabel title = ( RichLabel )getDrawable( INDEX_TITLE );		
		int halfWidth = width >> 1, halfHeight = height >> 1;
		title.defineReferencePixel( title.getSize().x >> 1, title.getSize().y );
		title.setRefPixelPosition( halfWidth, halfHeight );
			
		final int y = halfHeight + title.getSize().y;
		
		Label l = ( Label )getDrawable( INDEX_YES );
		l.defineReferencePixel( l.getSize().x, 0 );
		l.setRefPixelPosition( halfWidth - cursor.getSize().x, y );
		
		l = ( Label )getDrawable( INDEX_NO );
		l.defineReferencePixel( 0, 0 );
		l.setRefPixelPosition( halfWidth + cursor.getSize().x, y );
		
		updateCursorPosition();
	}
}
