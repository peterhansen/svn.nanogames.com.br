/**
 Transition.java
 Created on November 3, 2007, 6:25 PM
 */

/** @author Daniel */

package jogo;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;

public class Transition extends UpdatableGroup implements Constants
{
	// Poss�veis etapas de transi��o 
	public final byte TRANSITION_STEP_CLOSING = 0;
	public final byte TRANSITION_STEP_LEVEL = 1;
	public final byte TRANSITION_STEP_OPENING = 2;
	public final byte TRANSITION_STEP_ENDED = 3;
	
	/** Etapa em que a transi��o se encontra */
	private byte step = TRANSITION_STEP_ENDED;
	
	/** Vari�veis auxiliares de anima��o */
	private int counter = 0;
	private int timeCount = 0;
	
	/** Componente que ir� receber as informa��es do andamento da transi��o */
	private TransitionListener myListener = null;
	
	/** Define o quanto a transi��o se move a cada atualiza��o */
	private final int TRANSITION_MOVEMENT_SPEED = 5;
	
	/** Quantas atualiza��es esperamos com o n�mero da fase na tela antes de continuarmos com a transi��o */
	private final int TRANSITION_SHOWING_LEVEL_WAIT_TIME = 7;
	
	/** Define de quantos em quantos mil�simos faremos a atualiza��o da tela */
	private final int TRANSITION_UPDATE_WAIT_TIME =	42; // 1000 / 24 => 24 frames por segundo
	
	// Define os �ndices dos drawables na cole��o
	private final int TRANSITION_CHILD_IND_CURTAIN = 0;
	private final int TRANSITION_CHILD_IND_DOG = 1;
	private final int TRANSITION_CHILD_IND_LABEL = 2;
	
	/** Creates a new instance of Transition */
	public Transition( TransitionListener listener ) throws Exception
	{
		// Inicializa a superclasse
		super( 3 );
		
		// Armazena o objeto que ir� receber o feedback da transi��o
		myListener = listener;
		
		// Cria a "cortina" que ir� cobrir a tela
		DrawableRect curtain = new DrawableRect( 0x000000 );
		Thread.yield();
		insertDrawable( curtain );
		curtain.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// Aloca o cachorro do Jimmy Neutron que ir� puxar a "cortina" preta da esqueda para a direita
		TransitionDog jimmyDog = new TransitionDog();
		Thread.yield();
		insertDrawable( jimmyDog );
		
		// Cria o label que ir� exibir qual o n�mero da fase
		Label levelLabel = new Label( ImageFont.createMultiSpacedFont( SPEC_IMG_PATH_DIR + TRANSITION_FONT_IMG, SPEC_IMG_PATH_DIR + TRANSITION_FONT_DAT ), "" );
		Thread.yield();
		insertDrawable( levelLabel );
		
		// Determina o tamanho da cole��o
		setSize( ScreenManager.SCREEN_WIDTH + jimmyDog.getSize().x, ScreenManager.SCREEN_HEIGHT );
		
		// Posiciona os elementos na cole��o
		curtain.setPosition( 0, 0 );
		jimmyDog.setPosition( ScreenManager.SCREEN_WIDTH, ( size.y - jimmyDog.getSize().y ) >> 1 );
	}


	/** Atualiza o label que ir� exibir qual o n�mero da fase */
	public final void reset( int newLevel )
	{
		Label levelLabel = ( Label )getDrawable( TRANSITION_CHILD_IND_LABEL );
		levelLabel.setText( MeuMidlet.getText( TEXT_LEVEL ) + " " + newLevel );
		levelLabel.setPosition( ( ScreenManager.SCREEN_WIDTH - levelLabel.getSize().x ) >> 1, ( size.y - levelLabel.getSize().y ) >> 1 );

		setPosition( -size.x, 0 );
		
		step = TRANSITION_STEP_CLOSING;
	}
		
	
	/** Atualiza o estado do componente */
	public final void update( int delta )
	{
		super.update( delta );
		
		// Verifica se devemos atualizar a tela
		timeCount += delta;
		if( timeCount < TRANSITION_UPDATE_WAIT_TIME ) // 42 => 24 frames por segundo
			return;
		
		timeCount %= TRANSITION_UPDATE_WAIT_TIME;
		
		switch( step )
		{
			case TRANSITION_STEP_CLOSING:
				if( getPosition().x  + TRANSITION_MOVEMENT_SPEED < 0 )
				{
					move( TRANSITION_MOVEMENT_SPEED, 0 );
				}
				else
				{
					setPosition( 0, 0 );
					
					counter = 0;
					step = TRANSITION_STEP_LEVEL;
					
					myListener.onClosed();
				}
				break;
				
			case TRANSITION_STEP_LEVEL:
				if( counter++ == TRANSITION_SHOWING_LEVEL_WAIT_TIME )
					step = TRANSITION_STEP_OPENING;
				break;
				
			case TRANSITION_STEP_OPENING:
				if( getPosition().x < ScreenManager.SCREEN_WIDTH )
				{
					move( TRANSITION_MOVEMENT_SPEED, 0 );
				}
				else
				{
					step = TRANSITION_STEP_ENDED;
					myListener.onEnded();
				}
				break;
		}
	}
	
	
	/** Retorna em que etapa a transi��o se encontra */
	public final byte getTransitionStep()
	{
		return step;
	}
}
