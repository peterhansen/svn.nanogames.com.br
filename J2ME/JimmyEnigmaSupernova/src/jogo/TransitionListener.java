/**
 TransitionListener.java
 Created on November 10, 2007, 3:43 PM
 */

/** @author Daniel */

package jogo;

public interface TransitionListener
{
	/** M�todo chamado quando a transi��o cobre a tela por completo */
	public void onClosed();
	
	/** M�todo chamado assim que a transi��o termina */
	public void onEnded();
}
