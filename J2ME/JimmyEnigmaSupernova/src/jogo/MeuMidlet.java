package jogo;
/*
 * MeuMidlet.java
 *
 * Created on June 15, 2007, 3:45 PM
 *
 */

/**
 *
 * @author malvezzi
 */


//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.InputStream;
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;
import telas.MainMenu;
import telas.OptionsScreen;
import telas.TelaDeFimDeJogo;
import telas.TelaDeSplash;
import telas.YesNoScreen;
import telas.TelaDeJogo;
import telas.TelaDeRecorde;
import telas.TelaBackGroundSpace;
import telas.TelaSplashNano;
import telas.TelaSplashNick;
import telas.SetLanguageScreen;

/**
 *
 * @author  malvezzi
 * @version
 */
public final class MeuMidlet extends AppMIDlet implements MenuListener, Constants, Serializable
{
	/** Pontua��o atual do jogador */
	private static int score;
	
	/** Posi��o em que a �ltima pontua��o entrar� na lista de recordes */
	public static byte lastScoreEntryIndex = TOTAL_RECORDS;
	
	/** Recordes armazenados */
	public static final int topRecords[] = new int[TOTAL_RECORDS];
	
	/** Fonte principal da aplica��o */
	public static ImageFont fontMain;
	
	/** Armazena a tela de jogo que ser� carregada na op��o "continuar" do menu principal */
	public static Drawable saveScreen;
	
	/** Idioma que est� sendo utilizado pelo jogo */
	private static byte language;
	
	/** Objeto respons�vel por salvar e carregar do arquivo de dados os recordes do jogo */
	public static HighScoreManager highScoreManager = new HighScoreManager();
	
	/** Array de �cones de navega��o*/
	private static Drawable icons[]; 
	
	/** Armazena o �ltimo momento em que houve uma troca de tela */
	private boolean changingScreens = false;
	
	//#ifdef ReleaseSmall_Sagem_Gradiente
//# 		public MeuMidlet() throws MIDletStateChangeException
//# 		{
//# 			super( MeuMidlet.VENDOR_SAGEM_GRADIENTE, MeuMidlet.MAX_FRAME_TIME_DEFAULT );
//# 		}
	//#endif

	protected final void loadResources() throws Exception
	{
		try
		{
			// Deixa o fundo preto
			manager.setBackgroundColor( 0 );

			// Aloca as fontes utilizadas pelo jogo
			fontMain = ImageFont.createMultiSpacedFont( FONT_MAIN_IMG, FONT_MAIN_DAT );
			Thread.yield();

			// A tela inicial � a tela de sele��o de idioma
			setScreen( SCREEN_LOAD_RESOURCES );
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif

			// Se entrou nesse catch � porque alguma coisa necess�ria n�o pode ser alocada. Logo, abandonamos a
			// aplica��o
			exit();
		}
	}
	
	private final boolean loadResourcesFull()
	{
		try
		{			
			// Aloca, configura e determina o background comum a todas as telas do jogo
			TelaBackGroundSpace bkg = new TelaBackGroundSpace();
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			manager.setBackground( bkg, true );
			manager.setBackgroundColor( -1 );
			Thread.yield();
			
			// Aloca os �cones de navega��o
			final byte TOTAL_ICONS = 3;
			icons = new Drawable[TOTAL_ICONS];
			final String[] iconName = new String[]{ IMG_PATH_ICON_OK, IMG_PATH_ICON_BACK, IMG_PATH_ICON_PAUSE };
			
			for( byte i = 0 ; i < TOTAL_ICONS ; ++i )
			{
				icons[i] = new DrawableImage( IMG_PATH_ICONS + iconName[i] );
				Thread.yield();
			}

			// O usu�rio ainda n�o escolheu um idioma
			language = LANGUAGE_NONE;
			
			//#if LOG == "true"
//# 				// Se n�o existir a database, tudo bem, n�o ir� disparar uma exce��o pois h� um catch interno.
//# 				// O que queremos � poder utilizar o Logger desde o in�cio da aplica��o
//# 				Logger.setRMS( DATABASE_NAME, DATABASE_IDX_LOG );
//# 				Logger.loadLog( DATABASE_NAME, DATABASE_IDX_LOG );
			//#endif
			
			try
			{
				// Se ainda n�o existir uma base de dados para a grava��o das op��es de jogo, cria uma
				if( !createDatabase( DATABASE_NAME, DATABASE_IDX_TOTAL ) )
				{					
					// Tenta carregar o idioma previamente selecionado para o jogo
					LoadLanguage();

					// Carrega os recordes do jogo
					loadData( DATABASE_NAME, DATABASE_IDX_RECORD, highScoreManager );
				}
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
				
				// N�o fazemos nada aqui. Se n�o conseguiu carregar os sons e/ou os recordes, continuaremos assim
				// mesmo. Serve apenas para n�o entrarmos no catch externo, o que causaria o abandono da aplica��o
			}
			
			// Tenta alocar o objeto de som
			try
			{
				// Inicializa o MediaPlayer
				MediaPlayer.init( DATABASE_NAME, DATABASE_IDX_SOUND_OPTIONS, new String[] {	SOUND_PATH_DIR + SOUND_PATH_THEME,
	 																						SOUND_PATH_DIR + SOUND_PATH_CLEARED,
																							SOUND_PATH_DIR + SOUND_PATH_GAMEOVER
																						  } );
			}
			catch( Exception ex )
			{
				// N�o fazemos nada aqui. Se n�o conseguiu carregar os sons, continuaremos assim mesmo. Serve
				// apenas para n�o entrarmos no catch externo, o que causaria o abandono da aplica��o
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
			return true;
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif

			// Se entrou nesse catch � porque alguma coisa necess�ria n�o pode ser alocada. Logo, abandonamos a
			// aplica��o
			exit();
			return false;
		}
	}
	
	/**
	 * Carrega da base de dados do jogo o idioma padr�o selecionado
	 * @throws java.lang.Exception
	 */
	private static final void LoadLanguage()
	{
		try
		{
			loadData( DATABASE_NAME, DATABASE_IDX_LANGUAGE, ( MeuMidlet )instance );
		}
		catch( Exception ex )
		{
			// Se n�o conseguiu carregar o idioma, tudo bem. O jogador ter� que selecionar um idioma padr�o antes
			// de iniciar o jogo
		}
	}
	
	/**
	 * Armazena na base de dados do jogo o idioma padr�o selecionado
	 * @throws java.lang.Exception
	 */
	private static final void SaveLanguage()
	{
		try
		{
			saveData( DATABASE_NAME, DATABASE_IDX_LANGUAGE, ( MeuMidlet )instance );
		}
		catch( Exception ex )
		{
			// Se n�o conseguiu salvar o idioma, tudo bem. O jogador ter� que selecionar um idioma padr�o antes
			// de iniciar o jogo da pr�xima vez
		}
	}
	
	/**
	 * Armazena na base de dados do jogo informa��es serializadas do objeto MeuMidlet
	 * @throws java.lang.Exception
	 */
	public void write( DataOutputStream output ) throws Exception
	{
		output.writeByte( language );
	}
	
	/**
	 * Carrega da base de dados do jogo informa��es serializadas do objeto MeuMidlet
	 * @throws java.lang.Exception
	 */
	public void read( DataInputStream input ) throws Exception
	{
		language = input.readByte();
	}
	
	public static final Drawable LoadCursor() throws Exception
	{
		final byte CURSOR_N_IMAGES = 6;
		final byte CURSOR_N_SEQUENCES = 1;
		final byte CURSOR_FRAME_TIME = 100;
		
		//#if ( SCREEN_SIZE == "MEDIUM" ) || ( SCREEN_SIZE == "LANDSCAPE" )
			final byte CURSOR_DIST_FROM_OPTS = 5;
		//#elif SCREEN_SIZE == "SMALL"
//# 			final byte CURSOR_DIST_FROM_OPTS = 2;
		//#endif

		Image[] images = new Image[ CURSOR_N_IMAGES ];
		
		for( int i = 0 ; i < CURSOR_N_IMAGES ; ++i )
			images[ i ] = Image.createImage( GEN_IMG_PATH_DIR + "cursor_menu_" + i + ".png" );
		
		Sprite cursor = new Sprite( images, new byte[][]{ { 0, 1, 2, 3, 4, 5 } }, new short[]{ CURSOR_FRAME_TIME } );

		cursor.defineReferencePixel( cursor.getSize().x + CURSOR_DIST_FROM_OPTS, cursor.getSize().y >> 1 );
		return cursor;
	}
	
	public Drawable criaJogo() throws Throwable
	{
		final TelaDeJogo jogo = new TelaDeJogo();	
		jogo.startLevelOne();
		jogo.setSize( ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT );
		return jogo;
	}
	
	public static Drawable criaRecorde() throws Exception
	{
		TelaDeRecorde tela = new TelaDeRecorde();
		tela.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
		return tela;
	}
	
	private static final Drawable[] getHelpScreenSpecialChars() throws Exception
	{
		// Cria o array para conter os caracteres especiais
		final byte N_SPECIAL_CHARS = 3;
		Drawable[] specialChars = new Drawable[ N_SPECIAL_CHARS ];

		// Inicializa os framesets utilizados nesta tela
		final Supernova supernovaFrameSet = Supernova.loadFrameSet();
		final Sprite frameSetShards = SupernovaShard.loadFrameSet();

		// Cria os sprites
		final byte N_SUPERNOVA_STATES = 4;
		final byte DIST_BETWEEN_SUPERNOVAS = 5;
		final UpdatableGroup supernovas = new UpdatableGroup( N_SUPERNOVA_STATES );
		final Point largestSupernovaSize = supernovaFrameSet.getLargestSize();
		
		int totalWidth = 0;
		for( byte i = 0 ; i < N_SUPERNOVA_STATES ; ++i )
		{
			final Supernova s = new Supernova( supernovaFrameSet );
			supernovas.insertDrawable( s );
			s.setState( STATE_B1 + i );
			s.setPosition( totalWidth, ( largestSupernovaSize.y - s.getSize().y ) >> 1 );
			
			totalWidth += s.getSize().x + DIST_BETWEEN_SUPERNOVAS;
		}
		// +2 = para compensar o tamanho da menor supernova dando uma melhor id�ia de centraliza��o das imagens
		supernovas.setSize( totalWidth - DIST_BETWEEN_SUPERNOVAS + 2, largestSupernovaSize.y );

		// Determina o vetor de caracteres especiais
		specialChars[0] = supernovas;
		specialChars[1] = new AjudaSupernovaExplodindo( supernovaFrameSet, frameSetShards );
		specialChars[2] = new AjudaExplosaoEncadeada( supernovaFrameSet, frameSetShards );

		// Retorna o array criado
		return specialChars;
	}

	public static final void setLanguage( byte languageSelected ) throws Exception
	{
		// Carrega os textos do idioma selecionado
		MeuMidlet.LoadTexts( languageSelected );

		// Armazena o idioma selecionado para o jogo
		MeuMidlet.language = languageSelected;

		// Grava a op��o de idioma na base de dados
		MeuMidlet.SaveLanguage();
	}
	
	// Determina que �cone deve ser utilizado para representar a softKey
	public static final void setSoftKeyIcon( byte softKey, int iconIndex )
	{
		( ( MeuMidlet ) instance ).manager.setSoftKey( softKey, iconIndex < 0 ? null : icons[iconIndex] );
	}
	
	private static final void LoadTexts( byte textsLanguage ) throws Exception
	{
		// Cria um buffer auxiliar para lermos o arquivo que cont�m as strings
		final short MAX_BUFFER = 1600;
		final char[] buffer = new char[ MAX_BUFFER ];
		
		// Inicializa o vetor que ir� armazenar os textos do idioma escolhido
		texts = null;
		texts = new String[ TEXT_TOTAL ];
		
		// Codifica��o dos idiomas nos nomes dos arquivos
		final String[] langsIds = { "en", "pt", "es" };
		
		// Inicializa as vari�veis auxiliares
		int bufferIndex = 0;
		byte currentString = 0;
		
		InputStream inputStream = null;
		DataInputStream dataInputStream = null;
		
		try
		{
			inputStream = ( InputStream ) instance.getClass().getResourceAsStream( TEXTS_PATH_DIR + langsIds[ textsLanguage ] + ".dat" );
			dataInputStream = new DataInputStream( inputStream );
			
			while( currentString < TEXT_TOTAL )
			{
				char c = ( char ) dataInputStream.readUnsignedByte();
				
				switch( c )
				{
					case '\n':
						// chegou ao final de uma string
						MeuMidlet.texts[ currentString++ ] = new String( buffer, 0, bufferIndex );
						bufferIndex = 0;
						break;
						
					case '\\':
						c = '\n';
						
					default:
						buffer[ bufferIndex++ ] = c;
				}
			}
		}
		catch( EOFException ex )
		{
			// chegou ao final do arquivo; verifica se h� dados no buffer
			if( currentString < TEXT_TOTAL && bufferIndex > 0 )
				MeuMidlet.texts[ currentString ] = new String( buffer, 0, bufferIndex );
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
			throw ex;
		}
		finally
		{
			// Libera os recursos alocados
			if( dataInputStream != null )
			{
				try
				{
					dataInputStream.close();
				}
				catch( Exception ex )
				{
				}
				dataInputStream = null;
			}

			if( inputStream != null )
			{
				try
				{
					inputStream.close();
				}
				catch( Exception ex )
				{
				}
				inputStream = null;
			}
		} // fim finally
	}
	
	public static final void setScore( int score )
	{
		if( score > MAX_POINTS )
			score = MAX_POINTS;		
		else if ( score < 0 )
			score = 0;
		
		MeuMidlet.score = score;
	}
	
	public static final void increaseScore( int diff )
	{
		setScore( score + diff );
	}
	
	public static final int getScore()
	{
		return score;
	}
	
	public static final int getCurrLanguageIndex()
	{
		return language;
	}

	/**
	 * Evita que duas threads tentem mudar de tela ao mesmo tempo
	 * @see #unlockScreenChange()
	 */
	private synchronized final void lockScreenChange()
	{
		while( changingScreens )
		{
			try
			{
				wait();
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
		}
		changingScreens = true;		
	}
	
	/**
	 * Libera a troca de telas
	 * @see #lockScreenChange()
	 */
	private synchronized final void unlockScreenChange()
	{
		changingScreens = false;
		notifyAll();		
	}
	
	private String getAppVersion()
	{
		String version = getAppProperty( "MIDlet-Version" );
		if ( version == null )
			version = "0.0.9";
		return "<ALN_H>VERS�O " + version;
	}
	
	protected int changeScreen( int screen ) throws Exception
	{
		final MeuMidlet midlet = ( MeuMidlet ) instance;
		
		// Trava a troca de telas
		lockScreenChange();

		if( screen != currentScreen )
		{
			Drawable nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;
			
			int softKeyVisibleTime = 0;
			
			try
			{
				switch( screen )
				{
					case SCREEN_LOAD_GAME:
						nextScreen = new LoadScreen( midlet, SCREEN_JOGO );
						break;

					case SCREEN_MAIN_MENU:
						nextScreen = new MainMenu( this, fontMain, SCREEN_MAIN_MENU, saveScreen != null );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;

						break;

					case SCREEN_JOGO:

						saveScreen = null;
						System.gc();

						nextScreen = criaJogo();
						saveScreen = nextScreen;

						indexSoftRight = MeuMidlet.ICON_PAUSE;

						MediaPlayer.stop();
						MediaPlayer.free();

						break;

					case SCREEN_CONTINUE:
						MediaPlayer.stop();
						MediaPlayer.free();
						// N�o tem break mesmo!!!

					case SCREEN_GAMEBACK:
						nextScreen = saveScreen;
						indexSoftRight = MeuMidlet.ICON_PAUSE;

						break;

					//#if LOG == "true"
	//# 				case SCREEN_LOG:
	//# 					nextScreen = Logger.getLogScreen( this, SCREEN_LOG, fontMain );
	//# 					break;
					//#endif

					case SCREEN_RECORD:
						nextScreen = criaRecorde();

						indexSoftRight = MeuMidlet.ICON_BACK;

						break;

					case SCREEN_HELP:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, fontMain, getText( TEXT_HELP_TEXT ) + "\n\n" + getAppVersion() + "\n\n", false, MeuMidlet.getHelpScreenSpecialChars() );						
						
						final byte scrollWidth = 5;
						final Pattern scrollBkg = ( Pattern )(( BasicTextScreen )nextScreen ).getScrollFull();
						scrollBkg.setFillColor( ( 40 << 16 ) | (  75 << 8 ) | ( 35 ) );
						scrollBkg.setSize( scrollWidth, 0 );
						
						final Pattern scrollPage = ( Pattern )(( BasicTextScreen )nextScreen ).getScrollPage();
						scrollPage.setFillColor( ( 69 << 16 ) | ( 154 << 8 ) | ( 0 ) );
						scrollPage.setSize( scrollWidth, 0 );
						
						nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - icons[ ICON_BACK ].getHeight() );

						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_CREDITOS:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, fontMain, TEXT_CREDITS_TEXT, true );

						nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - icons[ ICON_BACK ].getHeight() );

						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_SPLASH_NANO:
						nextScreen = new TelaSplashNano( SCREEN_SPLASH_MARCA );

						MediaPlayer.free();
						//#if SAMSUNG_BAD_SOUND == "true"
//# 						MediaPlayer.play( SOUND_IND_THEME, MeuMidlet.SAMSUNG_BS_LOOP_INFINITE );
						//#else
						MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						//#endif

						break;

					case SCREEN_OPTIONS:
						nextScreen = new OptionsScreen( this, fontMain, SCREEN_OPTIONS );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_SPLASH_MARCA:
						nextScreen = new TelaSplashNick();
						break;

					case SCREEN_SPLASH_GAME:
						nextScreen = new TelaDeSplash();
						break;

					case SCREEN_YN_BACKGAME:
						saveScreen = manager.getCurrentScreen();
						nextScreen = new YesNoScreen( this, SCREEN_YN_BACKGAME, fontMain, TEXT_LEAVEGAME, false );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_YN_SOUND:
						nextScreen = new YesNoScreen( this, SCREEN_YN_SOUND, fontMain, TEXT_WITH_SOUND, true );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;
						break;

					case SCREEN_FIMDEJOGO:
						saveScreen = null;
						nextScreen = new TelaDeFimDeJogo( this, SCREEN_FIMDEJOGO );

						indexSoftLeft = MeuMidlet.ICON_OK;
						indexSoftRight = MeuMidlet.ICON_BACK;

						MediaPlayer.play( SOUND_IND_GAMEOVER, 1 );

						break;

					case SCREEN_SET_LANGUAGE:
						// Carrega todos os recursos do jogo
						if( !loadResourcesFull() )
							return -1;
							
						// Verifica se h� um idioma escolhido para o jogo
						if( language == LANGUAGE_NONE )
						{
							nextScreen = new SetLanguageScreen( this, fontMain, SCREEN_SET_LANGUAGE );

							indexSoftLeft = MeuMidlet.ICON_OK;
							indexSoftRight = MeuMidlet.ICON_BACK;
						}
						else
						{
							// Libera a troca de telas
							unlockScreenChange();

							// Vai para a pr�xima tela
							onChoose( null, SCREEN_SET_LANGUAGE, language );
							
							return -1;
						}
						break;
						
					case SCREEN_LOAD_RESOURCES:
						nextScreen = new LoadScreen( midlet, SCREEN_SET_LANGUAGE );
						break;

				} // fim switch( index )
				
				if( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft == SOFT_KEY_REMOVE ? null : icons[ indexSoftLeft ] );

				if( indexSoftRight != SOFT_KEY_DONT_CHANGE )
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight == SOFT_KEY_REMOVE ? null : icons[ indexSoftRight ] );
			}
			catch( Throwable t )
			{
				//#if DEBUG == "true"
					t.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, ent�o sai do jogo
				exit();
				unlockScreenChange();
				return -1;
			}

			if( nextScreen != null )
			{
				unlockScreenChange();
				midlet.manager.setCurrentScreen( nextScreen );
				return screen;
			}
			else
			{
				unlockScreenChange();
				return -1;
			}
		} // fim if ( screen != currentScreen )
		else
		{
			// Libera a troca de telas
			unlockScreenChange();
			return screen;
		}
	}

	public void onChoose( Menu menu, int id, int index )
	{
		switch( id )
		{
			case SCREEN_MAIN_MENU:
				{
					if( ( !( ( MainMenu )manager.getCurrentScreen() ).hasContinueOpt() ) && ( index >= OPT_CONTINUE ) )
						++index;

					switch( index )
					{
						case OPT_NEWGAME:
							setScreen( SCREEN_LOAD_GAME );
							break;
						case OPT_CONTINUE:
							setScreen( SCREEN_CONTINUE );
							break;
						case OPT_OPTION:
							setScreen( SCREEN_OPTIONS );
							break;
						case OPT_RECORD:
							setScreen( SCREEN_RECORD );
							break;
						case OPT_HELP:
							setScreen( SCREEN_HELP );
							break;
						case OPT_CREDITS:
							setScreen( SCREEN_CREDITOS );
							break;
						case OPT_EXIT:
							exit();
							break;
						//#if LOG == "true"
//# 						case OPT_LOG:
//# 							setScreen( SCREEN_LOG );
//# 							break;
						//#endif
					}
				}
				break;
			
			//#if LOG == "true"
//# 			case SCREEN_LOG:
			//#endif
			case SCREEN_HELP:
			case SCREEN_CREDITOS:
				setScreen( SCREEN_MAIN_MENU );
				break;
			
			case SCREEN_YN_SOUND:
				MediaPlayer.setMute( index == YesNoScreen.INDEX_NO );
				setScreen( SCREEN_SPLASH_NANO );
				break;
				
			case SCREEN_YN_BACKGAME:
				switch ( index )
				{
					case YesNoScreen.INDEX_YES:
						MediaPlayer.free();

						//#if SAMSUNG_BAD_SOUND == "true"
//# 						MediaPlayer.play( SOUND_IND_THEME, MeuMidlet.SAMSUNG_BS_LOOP_INFINITE );
						//#else
							MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						//#endif
						
						setScreen( SCREEN_MAIN_MENU );
						break;
						
					case YesNoScreen.INDEX_NO:
						setScreen( SCREEN_GAMEBACK );
						break;
				}
				break;
				
			case SCREEN_FIMDEJOGO:
				switch ( index )
				{
					case TelaDeFimDeJogo.OPT_ENDGAME_RETRY:
						setScreen( SCREEN_LOAD_GAME );
						break;
						
					case TelaDeFimDeJogo.OPT_ENDGAME_RECORD:
						setScreen( SCREEN_RECORD );
						
						MediaPlayer.free();
						//#if SAMSUNG_BAD_SOUND == "true"
//# 						MediaPlayer.play( SOUND_IND_THEME, MeuMidlet.SAMSUNG_BS_LOOP_INFINITE );
						//#else
							MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						//#endif
						break;
						
					case TelaDeFimDeJogo.OPT_ENDGAME_BACK:
						setScreen( SCREEN_MAIN_MENU );
						
						MediaPlayer.free();
						//#if SAMSUNG_BAD_SOUND == "true"
//# 						MediaPlayer.play( SOUND_IND_THEME, MeuMidlet.SAMSUNG_BS_LOOP_INFINITE );
						//#else
							MediaPlayer.play( SOUND_IND_THEME, MediaPlayer.LOOP_INFINITE );
						//#endif
						break;
				}
				break;
				
			case SCREEN_SET_LANGUAGE:
				try
				{
					// Determina o idioma a ser utilizado pelo jogo
					setLanguage( ( byte )index );
					
					// Vai para a pr�xima tela
					changeScreen( SCREEN_YN_SOUND );
				}
				catch( Exception ex )
				{
					// Se n�o conseguiu carregar os textos por algum motivo, temos que abortar o jogo
					exit();
				}
				break;
		}
	}

	public void onItemChanged( Menu menu, int id, int index )
	{
		// Implementada apenas por causa da requisi��o da interface MenuListener. O corpo deste m�todo � vazio de fato
	}
	
	private static final class LoadScreen extends Label implements Updatable
	{
		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;
		private long lastUpdateTime;
		private static final byte MAX_DOTS = 4;
		private byte dots;
		private final MeuMidlet midlet;
		private Thread loadThread;
		private final int nextScreen;
		private static final String loadingMsg = "LOADING";

		private LoadScreen( MeuMidlet midlet, int nextScreen ) throws Exception
		{
			super( MeuMidlet.fontMain, loadingMsg );
			this.midlet = midlet;
			this.nextScreen = nextScreen;
			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}

		protected void paint( Graphics g )
		{
			// Coloca uma cor de fundo
			pushClip( g );
			g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			g.setColor( 0, 0, 0 );
			g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			popClip( g );
		
			// Desenha o label
			super.paint( g );
		}

		public final void update( int delta )
		{
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if( interval >= CHANGE_TEXT_INTERVAL )
			{
				// as imagens do jogo s�o carregadas aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if( loadThread == null )
				{
					// s� inicia a thread quando a tela atual for a ativa (ou seja, a transi��o da TV estiver encerrada)
					if( ScreenManager.getInstance().getCurrentScreen() == this )
					{
						loadThread = new Thread()
						{
							public final void run()
							{
								try
								{
									MediaPlayer.free();
									MeuMidlet.setScreen( nextScreen );
								}
								catch( Exception e )
								{
									// Sai do jogo
									exit();
								}
							}
						};
						loadThread.start();
					}
				}
				else
				{
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) % MAX_DOTS );
					String temp = new String( loadingMsg );
					for( byte i = 0; i < dots; ++i )
					{
						temp += '.';
					}

					setText( temp );

					try
					{
						// Permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					}
					catch( Exception e )
					{
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
			}
		}
	}
}