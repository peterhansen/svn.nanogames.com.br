package jogo;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
/*
 * SupernovaShard.java
 *
 * Created on June 20, 2007, 6:38 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class SupernovaShard extends Sprite implements Constants
{

	private boolean active;
	private int indice;
	private int direction;
	private final Point v0 = new Point();
	private final Point ds = new Point();
	private final Point dx = new Point();
	private final Point dxa = new Point();
	private final int[] collisionList = new int[NUMERO_CELL - 1];
	private final Tabuleiro meuTabuleiro;
	public int carry;

	/**
	 * Creates a new instance of SupernovaShard
	 */
	public SupernovaShard( Sprite s, Tabuleiro tab )
	{
		super( s );
		meuTabuleiro = tab;
	}

	public final void setActive( boolean bl )
	{
		if( !bl )
		{
			Tabuleiro.cleanUp = true;
			carry = 0;
			setVisible( false );
		}
		else
		{
			setVisible( true );
		}
		active = bl;
	}

	public final boolean isActive()
	{
		return active;
	}

	public final void setIndex( int idx )
	{
		indice = idx;
	}

	public final void go( int idx, Point pt, int x, int y, int dir, int carry )
	{
		setActive( true );
		this.carry = carry;
		indice = idx;
		direction = dir;
		makeCollisionList( x, y );

		switch( dir )
		{
			case DIR_RIGHT:
				//setTransform(TRANS_ROT90);
				v0.set( SHARDS_SPEED, 0 );
				break;
			case DIR_LEFT:
				//setTransform(TRANS_ROT270);
				v0.set( -SHARDS_SPEED, 0 );
				break;
			case DIR_UP:
				//setTransform( TRANS_NONE );
				v0.set( 0, -SHARDS_SPEED );
				break;
			case DIR_DOWN:
				//setTransform(TRANS_ROT180);
				v0.set( 0, SHARDS_SPEED );
				break;
			default:
		}
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( pt );
	}

	public static Sprite loadFrameSet() throws Exception
	{
		Image[] images = new Image[SHARDS_IMAGENS_TOTAL];

		for( int i = 0; i < images.length; ++i )
		{
			images[i] = Image.createImage( IMG_PATH_SUPERNOVAS + "shard_" + i + ".png" );
			Thread.yield();
		}

		return new Sprite( images, new byte[][]{ { 0, 1, 2, 3, 4 } }, new short[]{ SHARDS_ANIM_SPEED } );
	}

	private void makeCollisionList( int x, int y )
	{
		switch( direction )
		{
			case DIR_RIGHT:
				//Preenche a lista de colisao
				for( int i = 1; i < NUMERO_CELL - x; ++i )
				{
					collisionList[i - 1] = NUMERO_CELL * y + x + i;
				}
				if( x > 0 ) // Cria uma flag para fim da lista
				{
					collisionList[( NUMERO_CELL - 1 ) - x] = -1;
				}

				break;
			case DIR_LEFT:
				for( int i = 1; i <= x; ++i )
				{
					collisionList[i - 1] = NUMERO_CELL * y + x - i;
				}
				if( x < NUMERO_CELL - 1 )
				{
					collisionList[x] = -1;
				}

				break;
			case DIR_UP:
				for( int i = 1; i <= y; ++i )
				{
					collisionList[i - 1] = NUMERO_CELL * ( y - i ) + x;
				}
				if( y < NUMERO_CELL - 1 )
				{
					collisionList[y] = -1;
				}

				break;
			case DIR_DOWN:
				for( int i = 1; i < NUMERO_CELL - y; ++i )
				{
					collisionList[i - 1] = NUMERO_CELL * ( y + i ) + x;
				}
				if( y > 0 )
				{
					collisionList[( NUMERO_CELL - 1 ) - y] = -1;
				}

				break;
			default:

		}
	}

	public final int getDir()
	{
		return direction;
	}

	public final int nextToCollision()
	{
		return collisionList[0];
	}

	final void setNextCollision()
	{
		for( int i = 0; i < collisionList.length - 1; ++i )
		{
			collisionList[i] = collisionList[i + 1];
		}
		collisionList[collisionList.length - 1] = -1;
	}

	public void update( int delta )
	{
		if( !active )
		{
			return;
		}
		super.update( delta );

		ds.set( dxa.x + ( v0.x * delta ), dxa.y + ( v0.y * delta ) );

		dx.x = ds.x / 1000;
		dxa.x = ds.x % 1000;
		dx.y = ds.y / 1000;
		dxa.y = ds.y % 1000;

		//preencher.add(dx);
		this.setPosition( this.getPosition().x + dx.x,
						  this.getPosition().y + dx.y );

		//Teste de Colisao
		if( meuTabuleiro.testCollision( indice ) )
		{
			//Colidiu
			this.setActive( false );
		//setNextCollision();
		}

		switch( direction )
		{
			case DIR_UP:
				if( position.y > 0 )
				{
					return;
				}
				break;

			case DIR_DOWN:
				if( getRefPixelY() < meuTabuleiro.getSize().y )
				{
					return;
				}
				break;

			case DIR_LEFT:
				if( position.x > 0 )
				{
					return;
				}
				break;

			case DIR_RIGHT:
				if( getRefPixelX() < meuTabuleiro.getSize().x )
				{
					return;
				}
				break;
			default:
				return;
		}
		//Todo: Animacao do minismack batendo!!!
		setActive( false );
		setVisible( false );

	}
}
