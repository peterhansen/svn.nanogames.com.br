package jogo;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
/*
 * Supernova.java
 *
 * Created on June 15, 2007, 5:14 PM
 *
 */

/**
 *
 * @author malvezzi
 */
public final class Supernova extends Sprite implements Constants, SpriteListener
{
	protected int state;
	
	/**
	 * Creates a new instance of Supernova
	 */
	public Supernova( Sprite s )
	{
		super( s );
		setListener( this, 0 );
	}
	
	
	public final int getState()
	{
		return state;
	}
	
	
	public final void setState( int state )
	{
		switch( state )
		{
			case STATE_VAZIO:
				this.setVisible( false );
				break;
				
			case STATE_B1:
			case STATE_B2:
			case STATE_B3:
			case STATE_B4:
			case STATE_BOOM:
				this.setVisible( true );
				this.setSequence( state );
				break;
				
			default:
				return;
		}
		this.state = state;
	}
	
	
	public static final Supernova loadFrameSet() throws Exception
	{
		
		//final FrameSet frameSet = new FrameSet( TOTAL_ESTADOS );
		
		Image[] images = new Image[ IMAGENS_TOTAL ];
		for ( int i = 0; i < IMAGENS_TOTAL; ++i )
		{
			images[ i ] = Image.createImage( IMG_PATH_SUPERNOVAS + "supernova_" + i +".png");
			Thread.yield();
		}
		
		return new Supernova(
								new Sprite( images, 
											new byte[][]{	{ 0 },
															{ 0, 1, 2, 3, 2, 1 },
															{ 4, 5, 6, 7, 6, 5 },
															{ 8, 9, 10, 11, 12, 13, 12, 11, 10, 9 },
															{ 14, 15, 16, 17, 18, 17, 16, 15 },
															{ 19, 20, 21, 22 } },
											new short[]{ 0, STATE_B1_TIME, STATE_B2_TIME, STATE_B3_TIME, STATE_B4_TIME, STATE_BOOM_TIME }
										  )
							);
	}
	
	
	
	public final void setSequence( int sequence )
	{
		final Point previousRefPixelPosition = getRefPixelPosition();
		
		super.setSequence( sequence );
		
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( previousRefPixelPosition );
	}
	
	
	public final void setFrame( int frame )
	{
		// armazena o ponto de refer�ncia anterior, e depois o redefine, de forma que uma poss�vel mudan�a nas dimens�es
		// reais do frame n�o alterem a posi��o do ingrediente
		final Point previousRefPoint = new Point( getRefPixelPosition() );
		
		super.setFrame( frame );
		
		final Image currentImage = getFrameImage( sequenceIndex, frameSequenceIndex );
		setSize( currentImage.getWidth(), currentImage.getHeight() );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( previousRefPoint );
	}

	public final boolean increaseState( boolean bl )
	{
		if( state == STATE_B4 )
		{
			setState( STATE_VAZIO );
			MeuMidlet.increaseScore( STATE_B4_POINTS );
			return false;
		}
		else
		{
			if( bl )
			{
				switch( state )
				{
					case STATE_B1:
						MeuMidlet.increaseScore( STATE_B1_POINTS );
						break;
						
					case STATE_B2:
						MeuMidlet.increaseScore( STATE_B2_POINTS );
						break;
						
					case STATE_B3:
						MeuMidlet.increaseScore( STATE_B3_POINTS );
						break;
				}
			}	
			setState( state + 1 );
			return true;
		}
	}
	
	
	public final void update( int delta )
	{
		if( state != STATE_VAZIO )
			super.update( delta );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if( sequenceIndex == STATE_BOOM )
			setState( STATE_VAZIO );		
	}

	public final Point getLargestSize()
	{
		int width, height;
		Point largest = new Point();
		
		for( int i = 0 ; i < sequences[STATE_B4].length ; ++i )
		{
			width = frames[ sequences[STATE_B4][i] ].getWidth();
			height = frames[ sequences[STATE_B4][i] ].getHeight();
			
			if( width > largest.x )
				largest.x = width;
			
			if( height > largest.y )
				largest.y = height;
		}	
		return largest;
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
}
