package jogo;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
import telas.TelaDeJogo;
import util.MyRandom;

/**
 *
 * @author malvezzi
 */

public final class Tabuleiro extends UpdatableGroup implements Constants
{
	private final Supernova[] supernovas = new Supernova[MAX_SUPERNOVAS];
	private final SupernovaShard[] supernovaShards = new SupernovaShard[MAX_SUPERNOVA_SHARDS];
	private static int shardsCount;
	public static boolean cleanUp;
	private final Sprite cursor;
	private final Point cursorCell = new Point();
	private final Point cellSize;
	private boolean cursorActive = true;
	private TelaDeJogo minhaTelaDeJogo;
	private Level level = new Level( 0 );
	
	/** N�mero de tiros atual */
	private short shots;
	
	/** Level atual */
	public short levelCount;

	/** Creates a new instance of Tabuleiro */
	public Tabuleiro( TelaDeJogo tela ) throws Exception
	{
		// +2 => Cursor e imagem do tabuleiro
		super( MAX_SUPERNOVAS + MAX_SUPERNOVA_SHARDS + 2 );
		
		minhaTelaDeJogo = tela;

		final DrawableImage board_img = new DrawableImage( IMG_PATH_TABULEIRO + "board_img.png" );
		Thread.yield();
		insertDrawable( board_img );
		board_img.setPosition( 0, 0 );
		
		setSize( board_img.getSize() );
		
		//#if SCREEN_SIZE == "MEDIUM"
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		//#endif

		cellSize = new Point( ( board_img.getSize().x / NUMERO_CELL ), ( board_img.getSize().y / NUMERO_CELL ) );

		// Cria as supernovas
		createSupernovas();

		// Cria os estilha�os das supernovas
		createSupernovasShards();

		// Cria o cursor que indica em que c�lula o jogador est� "mirando"
		cursor = createCursor();
		cursor.defineReferencePixel( cursor.getSize().x >> 1, cursor.getSize().y >> 1 );
		insertDrawable( cursor );

		try
		{
			MeuMidlet.loadData( DATABASE_NAME, DATABASE_IDX_RECORD, MeuMidlet.highScoreManager );
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
	}
	
	private final void createSupernovas() throws Exception
	{
		final Supernova base = Supernova.loadFrameSet();

		for( int i = 0; i < MAX_SUPERNOVAS; ++i )
		{
			supernovas[i] = new Supernova( base );
			insertDrawable( supernovas[i] );
			supernovas[i].defineReferencePixel( supernovas[i].getSize().x >> 1, supernovas[i].getSize().y >> 1 );
			Thread.yield();
		}
		fillCells( supernovas );
	}
	
	private final void createSupernovasShards() throws Exception
	{
		final Sprite base = SupernovaShard.loadFrameSet();

		for( int i = 0; i < MAX_SUPERNOVA_SHARDS; ++i )
		{
			supernovaShards[i] = new SupernovaShard( base, this );
			insertDrawable( supernovaShards[i] );
			supernovaShards[i].setActive( false );
			Thread.yield();
		}
	}
		
	/** Cria o cursor que indica em que c�lula o jogador est� "mirando" */
	private final Sprite createCursor() throws Exception
	{
		final Image[] cursorImages = new Image[CURSOR_TOTAL_IMAGES];
		for( int i = 0; i < cursorImages.length; ++i )
		{
			cursorImages[i] = Image.createImage( IMG_PATH_TABULEIRO + "cursor_" + i + ".png" );
			Thread.yield();
		}

		return new Sprite(	cursorImages,
							new byte[][]{	{ 0, 1, 2, 3, 4, 5, 4, 3, 2, 1 },
											{ 6, 7, 8, 9, 8, 7 } },
							new short[]{ CURSOR_ANIM_TIME, CURSOR_ANIM_TIME } );
	}

	public void startLevelOne()
	{
		levelCount = STARTING_LEVEL;
		setShots( N_SHOTS_AT_BEGGINING );
		fillSmacks();
	}

	public void nextLevel()
	{
		cursorTestSeq();
		fillSmacks();
	}

	public final void moveCursor( byte direction )
	{
		if( !cursorActive )
		{
			return;
		}

		switch( direction )
		{
			case DIR_RIGHT:
				++cursorCell.x;

				if( cursorCell.x >= NUMERO_CELL )
				{
					cursorCell.x = 0;
				}
				break;

			case DIR_LEFT:
				--cursorCell.x;

				if( cursorCell.x < 0 )
				{
					cursorCell.x = NUMERO_CELL - 1;
				}
				break;

			case DIR_UP:
				--cursorCell.y;

				if( cursorCell.y < 0 )
				{
					cursorCell.y = NUMERO_CELL - 1;
				}
				break;

			case DIR_DOWN:
				++cursorCell.y;

				if( cursorCell.y >= NUMERO_CELL )
				{
					cursorCell.y = 0;
				}
				break;

			default:
				return;
		}
		setDrawableAtCell( cursor, cursorCell );
	}

	private final void setDrawableAtCell( Drawable d, Point cell )
	{
		d.setRefPixelPosition( ( cellSize.x >> 1 ) + cell.x * cellSize.x, ( cellSize.y >> 1 ) + cell.y * cellSize.y );
	}
	
	public final void setCursorAtCell( int cellRow, int cellColumn )
	{
		cursorCell.x = cellRow;
		cursorCell.y = cellColumn;
		
		setDrawableAtCell( cursor, cursorCell );
	}

	private final void fillCells( Supernova[] smacks )
	{
		final Point cell = new Point();

		for( int i = 0; i < smacks.length; i++ )
		{
			cell.x = i % NUMERO_CELL;
			cell.y = i / NUMERO_CELL;

			setDrawableAtCell( smacks[i], cell );
		}
	}

	public final void fillSmacks()
	{
		for( int i = 0; i < supernovas.length; ++i )
			supernovas[i].setState( STATE_VAZIO );

		final MyRandom rand = new MyRandom();

		for( int i = 0; i < FILL_SUPERNOVA_SEED ; )
		{
			Supernova s = supernovas[( rand.nextInt( NUMERO_CELL ) * NUMERO_CELL ) + rand.nextInt( NUMERO_CELL )];
			
			if( s.getState() < STATE_B4 )
			{
				int temp = rand.nextInt( STATE_BOOM );
				final int percentage = rand.nextInt( 10 );
				
				final byte PERCENTAGE_MIN = 2, PERCENTAGE_MAX = 5, PERCENTAGE_LAST_LEVELS = 5;
				
				// Se o level est� entre 1 e 10, mais chance de termos os estados 3 e 4
				if( levelCount < 10 )
				{
					if( percentage < PERCENTAGE_MIN )
						temp = STATE_B4;
					else if( percentage < PERCENTAGE_MAX )
						temp = STATE_B3;
				}
				// Se o level est� entre 11 e 20, mais chance de termos os estados 2 e 3
				else if( ( levelCount > 10 ) && ( levelCount < 20 ) )
				{
					if( percentage < PERCENTAGE_MIN )
						temp = STATE_B3;
					else if( percentage < PERCENTAGE_MAX )
						temp = STATE_B2;
				}
				// Se o level est� entre 21 e 30, mais chance de termos os estados 1 e 2
				else if( ( levelCount > 20 ) && ( levelCount < 30 ) )
				{
					if( percentage < PERCENTAGE_MIN )
						temp = STATE_B2;
					else if( percentage < PERCENTAGE_MAX )
						temp = STATE_B1;
				}
				// Se o level est� acima de 30, mais chance de termos o estado 1
				else
				{
					if( percentage < PERCENTAGE_LAST_LEVELS )
						temp = STATE_B1;
				}

				temp = Math.min( s.getState() + temp, STATE_B4 );
				
				int diff = temp - s.getState();
				s.setState( temp );
				
				i += diff;
			}
			Thread.yield();
		}
	}

	private final void cursorActive( boolean bl )
	{
		if( bl )
		{
			cursor.setVisible( true );
		}
		else
		{
			cursor.setVisible( false );
		}
		cursorActive = bl;
		cursorTestSeq();
	}

	private final void cursorTestSeq()
	{
		//Trocando a sequencia do cursor.
		if( shots <= 3 )
		{
			if( cursor.getSequence() != TERMINAL_CURSOR )
				cursor.setSequence( TERMINAL_CURSOR );
		}
		else
		{
			if( cursor.getSequence() != NORMAL_CURSOR )
				cursor.setSequence( NORMAL_CURSOR );
		}

	}

	public final void fire()
	{
		if( !cursorActive )
			return;

		int pos = cursorCell.x + NUMERO_CELL * cursorCell.y;
		int supernovaState = supernovas[pos].getState();
		if( ( shots > 0 ) && ( supernovaState != STATE_VAZIO ) && ( supernovaState != STATE_BOOM ) )
		{
			updateShots( -1 );

			// Se for menor que STATE_B4, n�o ir� explodir com esse tiro, ent�o testa se
			// o jogo terminou por falta de tiros
			if( supernovaState < STATE_B4 )
				testEndLevel();

			cursorTestSeq();

			fire( pos, 0, false );
		}
		return;
	}

	public final void fire( int pos, int carry, boolean bl )
	{
		if( !supernovas[pos].increaseState( bl ) )
		{
			MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
			boom( pos, carry );
		}
	}

	private final void boom( int pos, int carry )
	{
		Point pt = new Point();
		int x = pos % NUMERO_CELL;
		int y = pos / NUMERO_CELL;
		supernovas[pos].setState( STATE_BOOM );
		if( carry > level.levelCarry )
		{
			updateShots( 1 );
			carry = -1;
		}

		cursorActive( false );

		pt.set( ( cellSize.x >> 1 ) + x * cellSize.x, ( cellSize.y >> 1 ) + y * cellSize.y );
		supernovaShards[shardsCount].go( shardsCount++, pt, x, y, DIR_UP, ++carry );
		supernovaShards[shardsCount].go( shardsCount++, pt, x, y, DIR_RIGHT, carry );
		supernovaShards[shardsCount].go( shardsCount++, pt, x, y, DIR_LEFT, carry );
		supernovaShards[shardsCount].go( shardsCount++, pt, x, y, DIR_DOWN, carry );

		MeuMidlet.increaseScore( shardsCount);
	}

	final boolean testCollision( int indice )
	{
		SupernovaShard ms = supernovaShards[indice];
		int next = ms.nextToCollision();

		int posNextCell = -1;
		if( next > ( NUMERO_CELL * NUMERO_CELL ) - 1 || next < 0 )
		{
			return false;
		}

		switch( ms.getDir() )
		{
			case DIR_UP:
				posNextCell = supernovas[next].getPosition().y + cellSize.y;
				if( posNextCell <= ms.getPosition().y )
				{
					return false;
				}
				break;
			case DIR_DOWN:
				posNextCell = supernovas[next].getPosition().y;
				if( posNextCell >= ms.getRefPixelY() )
				{
					return false;
				}

				break;

			case DIR_LEFT:
				posNextCell = supernovas[next].getPosition().x + cellSize.x;
				if( posNextCell <= ms.getRefPixelX() )
				{
					return false;
				}

				break;

			case DIR_RIGHT:
				posNextCell = supernovas[next].getPosition().x;
				if( posNextCell >= ms.getRefPixelX() )
				{
					return false;
				}

				break;
		}


		if( supernovas[next].getState() == STATE_VAZIO || supernovas[next].getState() == STATE_BOOM )
		{
			ms.setNextCollision();
			return false;
		}
		else
		{
			fire( next, ms.carry, true );
			return true;
		}

	}

	private final void setMiniSmackListCleanUp()
	{
		for( int i = 0; i < shardsCount; ++i )
		{
			if( !supernovaShards[i].isActive() )
			{
				for( ; shardsCount > i ; --shardsCount )
				{
					if( supernovaShards[shardsCount].isActive() )
					{
						doMiniSmaskListSwitch( i, shardsCount);
						break;
					}
				}
			}
		}

		if( shardsCount <= 0 )
		{
			cursorActive( true );
			testEndLevel();
		}

		cleanUp = false;
	}

	private final void doMiniSmaskListSwitch( int index1, int index2 )
	{
		final SupernovaShard msTemp = supernovaShards[index2];
		supernovaShards[index2] = supernovaShards[index1];
		supernovaShards[index2].setIndex( index2 );
		supernovaShards[index1] = msTemp;
		supernovaShards[index1].setIndex( index1 );
	}

	private final void testEndLevel()
	{
		int count = 0;
		for( int i = 0; i < supernovas.length; ++i )
		{
			if( supernovas[i].getState() != STATE_BOOM )
				count += supernovas[i].getState();
		}

		if( ( shots <= 0 ) && ( count > 0 ) )
		{
			gameOver();
		}
		else
		{
			if( count <= 0 )
				levelEnded();
		}
	}

	private final void levelEnded()
	{
		if( levelCount < MAX_LEVEL )
			++levelCount;

		updateShots( 1 );

		minhaTelaDeJogo.levelEnded( levelCount );
	}

	private final void gameOver()
	{
		// Cancela alguma vibra��o que por acaso esteja ativa
		MediaPlayer.vibrate( 0 );
		
		int aux;
		int curr = MeuMidlet.getScore();
		MeuMidlet.lastScoreEntryIndex = TOTAL_RECORDS;
		if( curr > MeuMidlet.topRecords[TOTAL_RECORDS - 1] )
		{
			for( byte i = 0; i < TOTAL_RECORDS; ++i )
			{
				if( curr > MeuMidlet.topRecords[i] )
				{
					aux = MeuMidlet.topRecords[i];
					MeuMidlet.topRecords[i] = curr;
					curr = aux;
					
					if( MeuMidlet.lastScoreEntryIndex > i )
						MeuMidlet.lastScoreEntryIndex = i;
				}
			}

			try
			{
				MeuMidlet.saveData( DATABASE_NAME, DATABASE_IDX_RECORD, MeuMidlet.highScoreManager );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif
			}
			MeuMidlet.setScreen( SCREEN_FIMDEJOGO );
		}
		else
		{
			MeuMidlet.setScreen( SCREEN_FIMDEJOGO );
		}
	}

	public final void update( int delta )
	{
		super.update( delta );
		if( cleanUp )
			setMiniSmackListCleanUp();
	}

	public final void setShots( int n )
	{
		if( n > MAX_SHOTS )
		{
			n = MAX_SHOTS;
		}
		else
		{
			if( n < 0 )
				n = 0;
		}

		this.shots = ( short ) n;

		if( minhaTelaDeJogo != null )
			minhaTelaDeJogo.updateShotsLabel();
	}

	public final void updateShots( int diff )
	{
		setShots( shots + diff );
	}

	public final int getShots()
	{
		return shots;
	}
	
	public final Point getBoardCellSize()
	{
		return cellSize;
	}
}
