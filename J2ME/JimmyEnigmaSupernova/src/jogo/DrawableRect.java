/**
 TransitionRect.java
 Created on November 3, 2007, 7:54 PM
 */

/** @author Daniel */

package jogo;

import br.com.nanogames.components.Drawable;
import javax.microedition.lcdui.Graphics;

public final class DrawableRect extends Drawable
{
	/** Cor do ret�ngulo */
	private int color = 0x000000;
		
	/** Construtor */
	public DrawableRect( int rectColor )
	{
		setColor( rectColor );
	}
	
	/** Determina a cor do ret�ngulo */
	public final void setColor( int rectColor )
	{
		color = rectColor;
	}
	
	/** Obt�m a cor do ret�ngulo */
	public final int getColor()
	{
		return color;
	}

	protected void paint( Graphics g )
	{
		g.setColor( color );
		g.fillRect( translate.x, translate.y, size.x, size.y );
	}
}
