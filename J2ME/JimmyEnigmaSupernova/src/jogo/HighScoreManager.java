/*
 * HighScoreManage.java
 *
 * Created on July 25, 2007, 12:26 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jogo;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author malvezzi
 */
public final class HighScoreManager implements Constants, Serializable
{	
	public final void write( DataOutputStream output ) throws Exception
	{
		for( int i=0 ; i < TOTAL_RECORDS ; ++i )
			output.writeInt( MeuMidlet.topRecords[i] );
	}

	public final void read( DataInputStream input ) throws Exception
	{
		for( int i=0 ; i < TOTAL_RECORDS ; ++i )
			MeuMidlet.topRecords[i] = input.readInt();
	}
}
