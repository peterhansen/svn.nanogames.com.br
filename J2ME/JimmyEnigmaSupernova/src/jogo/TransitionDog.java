/**
 TransitionDog.java
 Created on November 24, 2007, 3:49 PM
 */

/** @author Daniel */

package jogo;

import br.com.nanogames.components.Sprite;
import javax.microedition.lcdui.Image;

public final class TransitionDog extends Sprite implements Constants
{
	/** N�mero de frames do cahorro do Jimmy Neutron que ir� puxar a "cortina" preta da esqueda para a direita */
	static final int TRANSITION_JIMMY_DOG_N_FRAMES = 2;

	/** N�mero total de sequ�ncias de anima��o do cachorro do Jimmy Neutron */
	static final int TRANSITION_JIMMY_DOG_N_ANIM_SEQ = 1;

	/** Define a taxa de anima��o do cachorro do Jimmy Neutron */
	static final int TRANSITION_JIMMY_DOG_ANIM_TIME = 200;
		
	/** Construtor */
	public TransitionDog() throws Exception
	{
		super( getDogImages(), new byte[][]{ { 0, 1 } }, new short[]{ TRANSITION_JIMMY_DOG_ANIM_TIME } );
	}
	
	
	/** Retorna as imagens que comp�em a anima��o do sprite */
	private static Image[] getDogImages() throws Exception
	{
		Image[] images = new Image[ TRANSITION_JIMMY_DOG_N_FRAMES ];
		
		// Aloca as imagens
		for ( int i = 0; i < TRANSITION_JIMMY_DOG_N_FRAMES ; ++i )
		{
			images[ i ] = Image.createImage( GEN_IMG_PATH_DIR + TRANSITION_DOG + i + ".png" );
			Thread.yield();
		}
		
		return images;
	}
}
