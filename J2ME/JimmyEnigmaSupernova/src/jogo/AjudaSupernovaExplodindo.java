/**
 AjudaSupernovaExplodindo.java
 Created on October 15, 2007, 11:57 PM
 */

/** @author Daniel */

package jogo;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;

public final class AjudaSupernovaExplodindo extends UpdatableGroup implements Constants, SpriteListener
{
	/** N�mero de slots do grupo */
	private static final byte N_UPDATABLE_SLOTS = 5;
	
	// Define os �ndices dos objetos filhos da cole��o
	private final byte CHILD_IND_SUPERNOVA = 0;
	private final byte CHILD_IND_SHARD_UP = 1;
	private final byte CHILD_IND_SHARD_RIGHT = 2;
	private final byte CHILD_IND_SHARD_DOWN   = 3;
	private final byte CHILD_IND_SHARD_LEFT = 4;
	
	/** Quantas vezes o tamanho de um destro�o ser� percorrido por cada destro�o ap�s a explos�o da supernova */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte SHARDS_MULT = 5;
	//#else
		private final byte SHARDS_MULT = 5;
	//#endif
	
	/** Armazena o resto da divis�o do c�lculo da velocidade dos destro�os da supernova */
	private int lastSpeedModule = 0;

	/** Creates a new instance of AjudaSupernovaExplodindo */
	public AjudaSupernovaExplodindo( final Sprite frameSetSupernova, final Sprite frameSetShards ) throws Exception
	{
		// Inicializa a superclasse
		super( N_UPDATABLE_SLOTS );
		
		// Cria a supernova
		Supernova supernova = new Supernova( frameSetSupernova );
		supernova.setListener( this, 0 );
		insertDrawable( supernova );
		
		supernova.setState( STATE_B4 );
		
		// Cria os 4 destro�os da explos�o da supernova
		Sprite aux;
		Point sizeShards = new Point();
		for( byte i = 0 ; i < N_UPDATABLE_SLOTS-1 ; ++i )
		{
			aux = new Sprite( frameSetShards );
			sizeShards = aux.getSize();
			aux.defineReferencePixel( sizeShards.x >> 1, sizeShards.y >> 1 );
			insertDrawable( aux );
		}
		
		// Determina o tamanho da cole��o
		setSize( supernova.getSize().x + ( ( SHARDS_MULT * sizeShards.x ) << 1 ), supernova.getSize().y + (( SHARDS_MULT * sizeShards.y ) << 1 ));
		
		// Posiciona os elementos da cole��o
		for( byte i = 0 ; i < N_UPDATABLE_SLOTS ; ++i )
			getDrawable( i ).setRefPixelPosition( ( size.x >> 1 ) - ( getDrawable( i ).getSize().x & 1 ), size.y >> 1 );
	}
	
	public final void update( int delta )
	{
		// Anima a supernova
		Supernova supernova = ( Supernova )getDrawable( CHILD_IND_SUPERNOVA );
		supernova.update( delta );

		// Move os destro�os da explos�o
		Sprite aux;
		for( byte i = CHILD_IND_SHARD_UP ; i < N_UPDATABLE_SLOTS ; ++i )
		{
			// Obt�m o destro�o
			aux = ( Sprite ) getDrawable( i );

			aux.update( delta );

			final int ds = lastSpeedModule + ( SHARDS_SPEED * delta );
			final int dPos = ds / 1000;
			lastSpeedModule = ds % 1000;					

			// Se i for �mpar, estamos movendo os destro�os verticais
			// OBS: "( -2 + i )" e "( -3 + i )" -> jeito mais f�cil de fazer -sizeAux.y, sizeAux.y, -sizeAux.x, sizeAux.x
			if( ( i & 1 ) != 0 )
				aux.move( 0, ( -2 + i ) * dPos );
			// Sen�o, estamos movendo os destro�os horizontais
			else
				aux.move( ( -3 + i ) * dPos, 0 );
		}
	}
	
	private final void changeShardsState()
	{
		lastSpeedModule = 0;
		
		Drawable aux;
		for( byte i = CHILD_IND_SHARD_UP ; i < N_UPDATABLE_SLOTS ; ++i )
		{
			aux =  getDrawable( i );
			aux.setRefPixelPosition( size.x >> 1, size.y >> 1 );
		}
	}

	public void onSequenceEnded( int id, int sequence )
	{
		Supernova supernova = ( Supernova )getDrawable( CHILD_IND_SUPERNOVA );
		switch( sequence )
		{
			case STATE_B1:
			case STATE_B2:
			case STATE_B3:
			case STATE_BOOM:
				supernova.setState( ( supernova.getState() + 1 ) % STATE_BOOM );
				break;
			
			case STATE_B4:
				changeShardsState();
				supernova.setState( STATE_BOOM );
				break;
		}
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
}