/**
 AjudaExplosaoEncadeada.java
 Created on October 24, 2007, 9:44 PM
 */

/** @author Daniel */

package jogo;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;

public final class AjudaExplosaoEncadeada extends UpdatableGroup implements Constants, SpriteListener
{
	/** N�mero de supernovas na cole��o */
	private final byte N_SUPERNOVAS = 2;
	
	/** Define quantos sprites pertencem a cada supernova */
	private final byte N_SUPERNOVA_SPRITES = 5;
	
	/** Define o n�mero de slots necess�rios na cole��o ( N_SUPERNOVAS * N_SUPERNOVA_SPRITES )*/
	private static final byte N_UPDATABLE_SLOTS = 10;
	
	// Define os �ndices dos objetos filhos da cole��o
	private final byte CHILD_IND_SUPERNOVA_1   = 0;
	private final byte CHILD_IND_SHARD_UP_1    = 1;
	private final byte CHILD_IND_SHARD_RIGHT_1 = 2;
	private final byte CHILD_IND_SHARD_DOWN_1  = 3;
	private final byte CHILD_IND_SHARD_LEFT_1  = 4;
	private final byte CHILD_IND_SUPERNOVA_2   = 5;
	private final byte CHILD_IND_SHARD_UP_2    = 6;
	private final byte CHILD_IND_SHARD_RIGHT_2 = 7;
	private final byte CHILD_IND_SHARD_DOWN_2  = 8;
	private final byte CHILD_IND_SHARD_LEFT_2  = 9;

	/** N�mero de fragmentos emitidos na explos�o de cada supernova */
	private final byte SUPERNOVA_N_SHARDS = 4;
	
	/** Quantas vezes o tamanho de um destro�o ser� percorrido por cada destro�o ap�s a explos�o da supernova */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte SHARDS_MULT = 6;
	//#else
		private final byte SHARDS_MULT = 5;
	//#endif
	
	/** Armazena o resto da divis�o do c�lculo de velocidade dos destro�os das supernovas */
	private int[] lastSpeedModule = { 0, 0 };

	/** Cria uma nova inst�ncia de AjudaExplosaoEncadeada */
	public AjudaExplosaoEncadeada( final Sprite frameSetSupernova, final Sprite frameSetShards ) throws Exception
	{
		// Constr�i a superclasse
		super( N_UPDATABLE_SLOTS );

		// Cria as duas supernovas
		Supernova supernova = null;
		Point sizeShards = new Point();
		for( int i = 0 ; i < N_SUPERNOVAS ; ++i )
		{
			// Cria a supernova
			supernova = new Supernova( frameSetSupernova );
			supernova.setListener( this, i == 0 ? CHILD_IND_SUPERNOVA_1 : CHILD_IND_SUPERNOVA_2 );
			insertDrawable( supernova );

			supernova.setState( STATE_B4 );

			// Cria os 4 destro�os da explos�o da supernova
			Sprite aux;
			for( byte j = 0 ; j < SUPERNOVA_N_SHARDS ; ++j )
			{
				aux = new Sprite( frameSetShards );
				sizeShards = aux.getSize();
				aux.defineReferencePixel( sizeShards.x >> 1, sizeShards.y >> 1 );
				insertDrawable( aux );
			}
		}
		
		// Determina o tamanho da cole��o
		// Sendo:  o------------O-------------O-------------o
		// Onde:
		//			-  O = supernova
		//			-  o = fragmento
		//			- -- = caminho do fragmento, cujo tamanho � dado por ( SHARDS_MULT * sizeShards.x )
		// O c�lculo da altura segue a mesma id�ia
		Point shardDist = new Point( SHARDS_MULT * sizeShards.x, SHARDS_MULT * sizeShards.y );
		setSize( ( supernova.getSize().x << 1 ) + ( shardDist.x * 3 ), supernova.getSize().y + ( shardDist.y << 1 ) );
		
		// Posiciona os elementos da cole��o
		int aux = shardDist.x, indexMod = 0;
		Point supernovaHalf = new Point( supernova.getSize().x >> 1, supernova.getSize().y >> 1 );
		for( int i = 0 ; i < N_SUPERNOVAS ; ++i )
		{
			// Posiciona a supernova
			// -1 => largura �mpar
			getDrawable( indexMod + CHILD_IND_SUPERNOVA_1 ).setPosition( aux, shardDist.y );

			// Posiciona seus fragmentos
			for( byte j = CHILD_IND_SHARD_UP_1 ; j <= CHILD_IND_SHARD_LEFT_1 ; ++j )
				getDrawable( indexMod + j ).setRefPixelPosition( aux + supernovaHalf.x, shardDist.y + supernovaHalf.y );
			
			// Modifica o ponto de refer�ncia de posicionamento
			aux += supernova.getSize().x + shardDist.x;
			
			// Modifica o �ndice de refer�ncia
			indexMod = N_SUPERNOVA_SPRITES;
		}
	}
	
	
	/** Atualiza a tela */
	public final void update( int delta )
	{
		// Anima a supernova da esquerda
		Supernova supernova = ( Supernova )getDrawable( CHILD_IND_SUPERNOVA_1 );
		supernova.update( delta << 1 );

		// Move os destro�os da sua explos�o
		MoveShards( CHILD_IND_SHARD_UP_1, CHILD_IND_SHARD_LEFT_1, delta << 1, 0 );
		
		// Se o destro�o CHILD_IND_SHARD_RIGHT_1 atingiu CHILD_IND_SUPERNOVA_2, fazemos a �ltima aumentar de tamanho
		supernova = ( Supernova )getDrawable( CHILD_IND_SUPERNOVA_2 );
		Sprite rightShard = ( Sprite ) getDrawable( CHILD_IND_SHARD_RIGHT_1 );
		
		if( ( supernova.getState() != STATE_BOOM ) && ( rightShard.getPosition().x >=  supernova.getPosition().x ) )
		{
			// Deixa o fragmento invis�vel para n�o o movermos mais
			rightShard.setVisible( false );
			
			// Coloca o fragmento fora da tela para n�o entrarmos neste if repetidamente
			rightShard.setPosition( -rightShard.getSize().x, size.y >> 1 );
			
			// Modifica o estado da supernova
			int newState = supernova.getState() + 1;
			supernova.setState( newState );
			
			// Se est� para explodir, reposiciona os destro�os
			if( newState == STATE_BOOM )
				changeShardsState( CHILD_IND_SUPERNOVA_2 );
		}

		// Anima a supernova da direita
		supernova.update( delta );
		
		// Move os destro�os da sua explos�o
		MoveShards( CHILD_IND_SHARD_UP_2, CHILD_IND_SHARD_LEFT_2, delta, 1 );
	}
	
	
	/** Move os destro�os da explos�o da supernova */
	private final void MoveShards( byte min, byte max, int delta, int lastSpeedModuleIndex )
	{
		Sprite shard;
		for( byte i = min ; i <= max ; ++i )
		{
			// Obt�m o destro�o
			shard = ( Sprite ) getDrawable( i );
			
			if( shard.isVisible() )
			{
				// Anima-o
				shard.update( delta );

				// Faz o c�lculo de sua velocidade de movimenta��o
				final int ds = lastSpeedModule[ lastSpeedModuleIndex ] + ( SHARDS_SPEED * delta );
				final int dPos = ds / 1000;
				lastSpeedModule[ lastSpeedModuleIndex ] = ds % 1000;					

				if( min == CHILD_IND_SHARD_UP_1 )
				{
					// Se i for �mpar, estamos movendo os destro�os verticais
					// OBS: "( i - 2 )" e "( 3 - i )" -> jeito mais f�cil de fazer -sizeAux.y, sizeAux.y, sizeAux.x, -sizeAux.x
					if( ( i & 1 ) != 0 )
						shard.move( 0, ( i - 2 ) * dPos );
					// Sen�o, estamos movendo os destro�os horizontais
					else
						shard.move( ( 3 - i ) * dPos, 0 );
				}
				else
				{
					// Se i for par, estamos movendo os destro�os verticais
					// OBS: "( i - 7 )" e "( 8 - i )" -> jeito mais f�cil de fazer -sizeAux.y, sizeAux.y, sizeAux.x, -sizeAux.x
					if( ( i & 1 ) == 0 )
						shard.move( 0, ( i - 7 ) * dPos );
					// Sen�o, estamos movendo os destro�os horizontais
					else
						shard.move( ( 8 - i ) * dPos, 0 );
				}
			}
		}
	}
	
	
	/** Reposiciona os destro�os de uma supernova antes da explos�o */
	private final void changeShardsState( int supernovaId )
	{
		// Zera a cvari�vel que armazena o resto das divis�es dos c�lculos de velocidade
		lastSpeedModule[ supernovaId / CHILD_IND_SUPERNOVA_2 ] = 0;
		
		// Obt�m a maior largura poss�vel da supernova
		Supernova supernova = ( Supernova )getDrawable( CHILD_IND_SUPERNOVA_1 );
		int supernovaLargestWidth = supernova.getLargestSize().x;

		// Re-posiciona os fragmentos da explos�o
		final int shardDist = ( SHARDS_MULT * getDrawable( CHILD_IND_SHARD_UP_1 ).getSize().x );
		Point pos = new Point( shardDist + ( supernovaLargestWidth >> 1 ), size.y >> 1 );
		
		if( supernovaId == CHILD_IND_SUPERNOVA_2 )
			pos.x += shardDist + supernovaLargestWidth;
		else
			getDrawable( CHILD_IND_SHARD_RIGHT_1 ).setVisible( true );
		
		final int min = supernovaId + CHILD_IND_SHARD_UP_1;
		final int max = supernovaId + CHILD_IND_SHARD_LEFT_1;
		
		for( int i = min ; i <= max ; ++i )
			getDrawable( i ).setRefPixelPosition( pos );
	}

	
	/** Chamada quando a sequ�ncia de anima��o de algum sprite terminou */
	public void onSequenceEnded( int id, int sequence )
	{
		Supernova supernova = ( Supernova )getDrawable( id );
		switch( sequence )
		{
			case STATE_B1:
			case STATE_B2:
			case STATE_B3:
				if( id == CHILD_IND_SUPERNOVA_2 )
					return;
				// N�o possui o comando break mesmo !!!

			case STATE_BOOM:
				supernova.setState( ( supernova.getState() + 1 ) % STATE_BOOM );
				break;
			
			case STATE_B4:
				if( id == CHILD_IND_SUPERNOVA_1 )
				{
					changeShardsState( id );
					supernova.setState( STATE_BOOM );	
				}
				break;
		}
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
}
