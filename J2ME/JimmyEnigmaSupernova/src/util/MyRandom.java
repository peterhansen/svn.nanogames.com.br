/*
* MyRandom.java
*
* Created on 3 de Julho de 2007, 11:58
*
*/

package util;

import java.util.Random;


/**
*
* @author peter
*/
public final class MyRandom extends Random {
	 // n�mero primo utilizado para incrementar o seed a cada nova inst�ncia
	private static final int SEED_CHANGE = 999983;
	// esse valor � utilizado para reduzir a probabilidade de objetos Random alocados
	// consecutivamente utilizarem o mesmo seed e assim gerarem muitos padr�es de repeti��es
	private static long SEED = System.currentTimeMillis();
	/** Creates a new instance of MyRandom */
	public MyRandom() {
	super( SEED += SEED_CHANGE );
	}
	public final int nextInt( int n ) {
		return Math.abs( nextInt() % n );
	}
} 