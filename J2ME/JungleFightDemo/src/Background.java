import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
/*
 * Background.java
 *
 * Created on June 29, 2007, 5:36 PM
 *
 */

/**
 *
 * @author peter
 */
public final class Background extends DrawableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 5;
	
	/** Creates a new instance of Background */
	public Background() throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		Pattern fill = new Pattern( null );
		fill.setFillColor( 0x000000 );
		fill.setSize( size.x, size.y );
		insertDrawable( fill );
		
		DrawableImage bkg = new DrawableImage( PATH_SPLASH + "bkg_darker.png" );
		bkg.defineReferencePixel( bkg.getSize().x >> 1, bkg.getSize().y >> 1 );
		bkg.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, SPLASH_DEFAULT_HEIGHT >> 1 );
		insertDrawable( bkg );
		
		DrawableImage logo = new DrawableImage( PATH_SPLASH + "logo.png" );
		logo.defineReferencePixel( logo.getSize().x >> 1, logo.getSize().y >> 1 );
		logo.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, bkg.getPosition().y );
		insertDrawable( logo );
		
//		Pattern transparent = new Pattern( new DrawableImage( PATH_SPLASH + "pattern.png" ) );
//		transparent.setSize( size.x, size.y );
//		insertDrawable( transparent );
	}
	
}
