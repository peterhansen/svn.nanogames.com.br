/*
 * Constants.java
 *
 * Created on June 29, 2007, 10:30 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author peter
 */
public interface Constants {
	
    // caminhos dos recursos
	public static final String PATH_IMAGES				= "/images/";
	public static final String PATH_SPLASH				= PATH_IMAGES + "splash/";
    public static final String PATH_SOUNDS				= "sounds/";
	
	
	public static final String DATABASE_NAME			= "JUNGLE_FIGHT_DEMO";
	public static final byte DATABASE_INDEX_MEDIA		= 1;
	public static final byte DATABASE_TOTAL				= 1;
	
	
	public static final short DEFAULT_VIBRATION_TIME = 300;
	
	// telas do jogo
	public static final byte SCREEN_CHOOSE_SOUND		= 0;
	public static final byte SCREEN_SPLASH_NANO			= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_ARVATO		= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME			= SCREEN_SPLASH_ARVATO + 1;
	public static final byte SCREEN_MAIN_MENU			= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_OPTIONS				= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CREDITS				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP				= SCREEN_CREDITS + 1;
	public static final byte SCREEN_NEW_GAME			= SCREEN_HELP + 1;
	public static final byte SCREEN_CONTINUE_GAME		= SCREEN_NEW_GAME + 1;
	public static final byte SCREEN_PAUSE				= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_CONFIRM_MENU		= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT		= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_CHOOSE_CHARACTER	= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_ERROR				= SCREEN_CHOOSE_CHARACTER + 1;
	public static final byte SCREEN_GET_MORE			= SCREEN_ERROR + 1;
	
	
	// tela de splash
	public static final int SPLASH_DEFAULT_WIDTH	= 176;
	public static final int SPLASH_DEFAULT_HEIGHT	= 220;

	
	// espaçamento vertical entre os itens dos menus
	public static final short MENU_ITEMS_SPACING = 2;
	
	
	// definições dos textos
	public static final byte TEXT_OK					= 0;
	public static final byte TEXT_BACK					= 1;
	public static final byte TEXT_NEW_GAME				= 2;
	public static final byte TEXT_EXIT					= 3;
	public static final byte TEXT_OPTIONS				= 4;
	public static final byte TEXT_PAUSE					= 5;
	public static final byte TEXT_CREDITS				= 6;
	public static final byte TEXT_CREDITS_TEXT			= 7;
	public static final byte TEXT_HELP					= 8;
	public static final byte TEXT_HELP_TEXT				= 9;
	public static final byte TEXT_SCORE					= 10;
	public static final byte TEXT_LEVEL					= 11;
	public static final byte TEXT_TURN_SOUND_ON			= 12;
	public static final byte TEXT_TURN_SOUND_OFF		= 13;
	public static final byte TEXT_TURN_VIBRATION_ON		= 14;
	public static final byte TEXT_TURN_VIBRATION_OFF	= 15;
	public static final byte TEXT_CANCEL				= 16;
	public static final byte TEXT_CONTINUE				= 17;
	public static final byte TEXT_DO_YOU_WANT_SOUND		= 18;
	public static final byte TEXT_YES					= 19;
	public static final byte TEXT_NO					= 20;
	public static final byte TEXT_MAIN_MENU				= 21;
	public static final byte TEXT_CONFIRM_BACK_MENU		= 22;
	public static final byte TEXT_CONFIRM_EXIT			= 23;
	public static final byte TEXT_PRESS_ANY_KEY			= 24;
	public static final byte TEXT_CHOOSE_CHARACTER		= 25;
	public static final byte TEXT_LAST_ERROR			= 26;
	public static final byte TEXT_GAME_OVER				= 27;
	public static final byte TEXT_GET_MORE				= 28;
	public static final byte TEXT_GET_MORE_TEXT			= 29;
	
	
	public static final byte TOTAL_TEXTS				= 30;	
	
}
