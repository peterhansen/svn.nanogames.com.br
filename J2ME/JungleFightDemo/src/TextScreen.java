/*
 * TextScreen.java
 *
 * Created on June 4, 2007, 3:53 PM
 *
 */

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.AnimatedTransition;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class TextScreen extends RichLabel implements Constants {
	
	/**
	 * Creates a new instance of TextScreen
	 */
	public TextScreen( ImageFont font, byte textIndex ) throws Exception {
		super( font, DemoMIDlet.getText( textIndex ), null );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	
	public final void keyPressed( int key ) {
		switch ( ScreenManager.getKeyCodeGameAction( key ) ) {
			case ScreenManager.UP:
				key = ScreenManager.KEY_NUM2;
			break;
			
			case ScreenManager.DOWN:
				key = ScreenManager.KEY_NUM8;
			break;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )


		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				DemoMIDlet.setScreen( SCREEN_MAIN_MENU );
			return;
			
			default:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					break;

					case ScreenManager.KEY_NUM8:
					break;

					default:
						return;
				} // fim switch ( key )						
		}
	} // fim do m�todo keyPressed( int )
	
	
}
