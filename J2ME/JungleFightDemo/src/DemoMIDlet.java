/*
 * DemoMIDlet.java
 *
 * Created on June 29, 2007, 10:28 AM
 *
 */

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 *
 * @author peter
 */
public final class DemoMIDlet extends AppMIDlet implements Constants, MenuListener {
	
	private static byte currentScreen;
	
	private static ImageFont DEFAULT_FONT;
	
	private static String[] texts;
	
	// tempo m�ximo considerado de um frame
	private static final int MAX_FRAME_TIME = 100;	
	
	
	/** Creates a new instance of DemoMIDlet */
	public DemoMIDlet() {
	}

	
	protected final void startApp() throws MIDletStateChangeException {
		final DemoMIDlet instanceBefore = (DemoMIDlet) instance;
		super.startApp();
		
		if ( instance != instanceBefore ) {
			// iniciou a aplica��o
			try {
				manager = ScreenManager.createInstance( false, MAX_FRAME_TIME );
				manager.start();
				
				loadTexts();
				
//				MediaPlayer.init( DATABASE_NAME, DATABASE_INDEX_MEDIA, sounds );
				DEFAULT_FONT = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );				
				
				setScreen( SCREEN_SPLASH_NANO );
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static final void setScreen( byte index ) {
		if ( index != currentScreen ) {
			Drawable nextScreen = null;
			
			final DemoMIDlet midlet = ( DemoMIDlet ) instance;
			
			boolean hasBkg = true;
			int bgColor = 0x000000;
			try {
				setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
				
				switch ( index ) {
					case SCREEN_GET_MORE:
					case SCREEN_CHOOSE_SOUND:
					case SCREEN_CONFIRM_MENU:
					case SCREEN_CONFIRM_EXIT:
						int idTitle = 0;
						byte defaultOption = YesNoScreen.INDEX_NO;
						switch ( index ) {
							case SCREEN_CHOOSE_SOUND:
								idTitle = TEXT_DO_YOU_WANT_SOUND;
								if ( !MediaPlayer.isMuted() )
									defaultOption = YesNoScreen.INDEX_YES;
							break;
							
							case SCREEN_CONFIRM_MENU:
								idTitle = TEXT_CONFIRM_BACK_MENU;
							break;
							
							case SCREEN_CONFIRM_EXIT:
								idTitle = TEXT_CONFIRM_EXIT;
							break;
							
							case SCREEN_GET_MORE:
								idTitle = TEXT_GET_MORE_TEXT;
								defaultOption = YesNoScreen.INDEX_YES;
							break;
						} // fim switch ( index )
						
						nextScreen = new YesNoScreen( midlet, index, DEFAULT_FONT, idTitle );
						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
						hasBkg = false;
						
						( ( YesNoScreen ) nextScreen ).setCurrentIndex( defaultOption );
					break;
					
					case SCREEN_SPLASH_NANO:
						nextScreen = new SplashNano( PATH_SPLASH );
						hasBkg = false;
						bgColor = 0x000000;
					break;
					
					case SCREEN_SPLASH_ARVATO:
						nextScreen = new SplashArvato();
						hasBkg = false;
						bgColor = 0x000000;
					break;

					case SCREEN_SPLASH_GAME:
						nextScreen = new SplashGame( DEFAULT_FONT );
						bgColor = 0x000000;
					break;

					case SCREEN_MAIN_MENU:
						hasBkg = true;
//						playScreen = null;
						nextScreen = new MainMenu( midlet, DEFAULT_FONT, index );
						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_EXIT );
					break;

					case SCREEN_OPTIONS:
						nextScreen = new OptionsScreen( midlet, DEFAULT_FONT, index );
						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
					break;

					case SCREEN_CREDITS:
					case SCREEN_HELP:
						nextScreen = new TextScreen( DEFAULT_FONT, index == SCREEN_CREDITS ? TEXT_CREDITS_TEXT : TEXT_HELP_TEXT );
						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
					break;

					case SCREEN_NEW_GAME:
					case SCREEN_CONTINUE_GAME:
						nextScreen = new PlayScreen();
						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_MAIN_MENU );
						hasBkg = false;
						bgColor = -1;
					break;
					
					case SCREEN_PAUSE:
//						nextScreen = new PauseScreen( midlet, DEFAULT_FONT, index );
//						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
//						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_CONTINUE );									
					break;
					
					case SCREEN_CHOOSE_CHARACTER:
//						nextScreen = new ChooseCharacterScreen( midlet, index, DEFAULT_FONT );
//						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
//						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );															
					break;
					
					case SCREEN_ERROR:
//						nextScreen = new TextScreen( DEFAULT_FONT, TEXT_LAST_ERROR );
//						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
					break;
					
				}
				
				if ( hasBkg ) {
					if ( midlet.manager.getBackground() == null )
						midlet.manager.setBackground( new Background(), false );
				} else {
					midlet.manager.setBackgroundColor( bgColor );
				}
			} catch ( Exception e ) {
//				if ( index != SCREEN_ERROR ) {
//					texts[ TEXT_LAST_ERROR ] += e.toString() + "\n" + e.getMessage();
//					setScreen( SCREEN_ERROR );
//				}
				
				//#ifdef DEBUG
				e.printStackTrace();
				//#endif
				
//				return;
			}
			if ( nextScreen != null ) {
				currentScreen = index;
				midlet.manager.setCurrentScreen( nextScreen, ScreenManager.TRANSITION_TYPE_1_STAGE );
			}
		} // fim if ( index != currentScreen )
	}
	
	
	private final void loadTexts() {
		texts = new String[] { 
			"OK", 
			"VOLTAR", 
			"NOVO JOGO", 
			"SAIR",
			"OP��ES",
			"PAUSA",
			"CR�DITOS",
			"TEXTO CR�DITOS BLABLABLA",
			"AJUDA",
			"POR FAVOR ME AJUDEM!",
			"PONTOS",
			"N�VEL",
			"LIGAR SONS",
			"DESLIGAR SONS",
			"LIGAR VIBRA��O",
			"DESLIGAR VIBRA��O",
			"CANCELAR",
			"CONTINUAR",
			"ATIVAR SONS?",
			"SIM",
			"N�O",
			"MENU",
			"<ALN_H>ABANDONAR O JOGO E VOLTAR AO MENU PRINCIPAL?",
			"<ALN_H>ABANDONAR O JOGO E SAIR?",
			"<ALN_H>PRESSIONE QUALQUER TECLA",
			"<ALN_H>ESCOLHA SEU PERSONAGEM",
			"",
			"FIM DE JOGO",
			"MAIS PRODUTOS",
			"<ALN_H>DESEJA SAIR DO JOGO E VISITAR A P�GINA WAP DA ARVATO MOBILE?",
		};
	}
	
	
	public final void onChoose( int id, int index ) {
		if ( id == currentScreen ) {
			switch ( id ) {
				case SCREEN_CHOOSE_SOUND:
					switch ( index ) {
						case YesNoScreen.INDEX_YES:
						case YesNoScreen.INDEX_NO:
							MediaPlayer.setMute( index == YesNoScreen.INDEX_NO );
						default:
							setScreen( SCREEN_SPLASH_NANO );
					}				
				break;			

				case SCREEN_MAIN_MENU:
					switch ( index ) {
						case MainMenu.INDEX_NEW_GAME:
							setScreen( SCREEN_NEW_GAME );
						break;

						case MainMenu.INDEX_OPTIONS:
							setScreen( SCREEN_OPTIONS );
						break;
						
						case MainMenu.INDEX_GET_MORE:
							setScreen( SCREEN_GET_MORE );
						break;

						case MainMenu.INDEX_HELP:
							setScreen( SCREEN_HELP );
						break;

						case MainMenu.INDEX_CREDITS:
							setScreen( SCREEN_CREDITS );
						break;

						case MainMenu.INDEX_EXIT:
							exit();
						break;
					}
				break;

				case SCREEN_OPTIONS:
					switch ( index ) {
						case OptionsScreen.INDEX_BACK:
							setScreen( SCREEN_MAIN_MENU );
						break;
					}				
				break;
				
				case SCREEN_PAUSE:
				break;
				
				case SCREEN_CONFIRM_MENU:
					switch ( index ) {
						case YesNoScreen.INDEX_YES:
							setScreen( SCREEN_MAIN_MENU );
						break;
						
						default:
							setScreen( SCREEN_PAUSE );
					}				
				break;
				
				case SCREEN_CONFIRM_EXIT:
					switch ( index ) {
						case YesNoScreen.INDEX_YES:
							exit();
						break;
						
						default:
							setScreen( SCREEN_PAUSE );
					}				
				break;				
				
				case SCREEN_CHOOSE_CHARACTER:
					setScreen( SCREEN_NEW_GAME );
				break;
				
				case SCREEN_GET_MORE:
					switch ( index ) {
						case YesNoScreen.INDEX_YES:

							try {
								instance.platformRequest( "http://wapbr.tj.net" );
							} catch (ConnectionNotFoundException ex) {
								//#ifdef DEBUG
								ex.printStackTrace();
								//#endif
							}
							exit();
						break;
						
						default:
							setScreen( SCREEN_MAIN_MENU );
					}				
				break;
			} // fim switch ( id )
		} // fim if ( id == currentScreen )
	} // fim do m�todo onChoose( int, int )

	
	public final void itemChanged( int id, int index ) {
	}	
	
	
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		try {
			if ( textIndex < 0 )
				( ( DemoMIDlet ) instance ).manager.setSoftKey( softKey, null );
			else
				( ( DemoMIDlet ) instance ).manager.setSoftKey( softKey, new Label( DEFAULT_FONT, getText( textIndex ) ) );
		} catch ( Exception e ) {
			//#ifdef DEBUG
			e.printStackTrace();
			//#endif
		}
	}	
	
	
	public static final String getText( int index ) {
		return texts[ index ];
	}
	
	
	public static final Drawable loadCursor() {
		try {
			final DrawableImage cursor = new DrawableImage(PATH_IMAGES + "skull.png" );
			cursor.defineReferencePixel( cursor.getSize().x + 3, cursor.getSize().y >> 1 );
			
			return cursor;
		} catch ( Exception e ) {
			//#ifdef DEBUG
			e.printStackTrace();
			//#endif
			
			return null;
		}
	} // fim do m�todo loadCursor()	

	
	
	
}
