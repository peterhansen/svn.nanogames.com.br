import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
/*
 * PlayScreen.java
 *
 * Created on June 29, 2007, 3:55 PM
 *
 */

/**
 *
 * @author peter
 */
public final class PlayScreen extends DrawableImage implements KeyListener, Constants {
	
	/** Creates a new instance of PlayScreen */
	public PlayScreen() throws Exception {
		super( PATH_IMAGES + "fight.png" );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
	}

	
	public final void keyPressed(int key) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				DemoMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		}
	}

	
	public final void keyReleased(int key) {
	}
	
}
