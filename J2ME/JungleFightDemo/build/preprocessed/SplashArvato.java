import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
/*
 * SplashArvato.java
 *
 * Created on June 29, 2007, 3:20 PM
 *
 */

/**
 *
 * @author peter
 */
public final class SplashArvato extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 5;
	
	private int accTime;
	
	// tempo total em milisegundos em que a logo da Arvato � exibida
	private final int TOTAL_LOGO_TIME = 3000;	
	
	
	/** Creates a new instance of SplashArvato */
	public SplashArvato() throws Exception {
		super( TOTAL_ITEMS );
	
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		DrawableImage img = new DrawableImage( PATH_SPLASH + "arvato.png" );
		img.defineReferencePixel( img.getSize().x >> 1, img.getSize().y >> 1 );
		img.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( img );
		
		img = new DrawableImage( PATH_SPLASH + "text_arvato.png" );
		img.defineReferencePixel( img.getSize().x >> 1, img.getSize().y );
		img.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT );
		insertDrawable( img );
	}

	
	public final void update( int delta ) {
		accTime += delta;
		
		if ( accTime >= TOTAL_LOGO_TIME )
			DemoMIDlet.setScreen( SCREEN_SPLASH_GAME );		
	}
	
}
