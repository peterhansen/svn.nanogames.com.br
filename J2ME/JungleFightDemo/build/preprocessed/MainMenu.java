/*
 * MainMenu.java
 *
 * Created on June 4, 2007, 4:33 PM
 *
 */

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AnimatedTransition;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class MainMenu extends Menu implements Constants {
	
	public static final byte INDEX_NEW_GAME	= 0;
	public static final byte INDEX_OPTIONS	= 1;
	public static final byte INDEX_GET_MORE = 2;
	public static final byte INDEX_HELP		= 3;
	public static final byte INDEX_CREDITS	= 4;
	public static final byte INDEX_EXIT		= 5;

	
	/** Creates a new instance of MainMenu */
	public MainMenu( MenuListener listener, ImageFont font, int id ) throws Exception {
		super( listener, id, 8 );
		
		final String[] items = new String[] { DemoMIDlet.getText( TEXT_NEW_GAME ), 
											  DemoMIDlet.getText( TEXT_OPTIONS ), 
											  DemoMIDlet.getText( TEXT_GET_MORE ), 
											  DemoMIDlet.getText( TEXT_HELP ), 
											  DemoMIDlet.getText( TEXT_CREDITS ), 
											  DemoMIDlet.getText( TEXT_EXIT ) };
		
		Label label;
		int y = MENU_ITEMS_SPACING + font.getImage().getHeight();
		for ( int i = 0; i < items.length; ++i ) {
			label = new Label( font, items[ i ] );
			insertDrawable( label );
			
			label.defineReferencePixel( label.getSize().x >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, y );
			y += label.getSize().y + MENU_ITEMS_SPACING;
		}
		
		setSize( ScreenManager.SCREEN_WIDTH, y );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		
		setCircular( true );

		final Drawable cursor = DemoMIDlet.loadCursor();
		setCursor( cursor, Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );
	}
	
	
	public final void keyPressed( int key ) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				DemoMIDlet.exit();
			break;
			
			default:
				super.keyPressed( key );
		}
	} // fim do m�todo keyPressed( int )	

	
	
}
