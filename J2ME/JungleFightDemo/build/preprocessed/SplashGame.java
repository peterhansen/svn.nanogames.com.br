import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
/*
 * SplashGame.java
 *
 * Created on June 29, 2007, 3:20 PM
 *
 */

/**
 *
 * @author peter
 */
public final class SplashGame extends UpdatableGroup implements Constants, KeyListener {
	
	private static final byte TOTAL_ITEMS = 10;
	
	// tempo m�nimo que o logo do jogo � exibido
	private final short MIN_TIME = 3000;
	
	private int accTime;	
	
	private final short BLINK_RATE = 1199;
	
	private final RichLabel labelPressAnyKey;
	
	
	/** Creates a new instance of SplashGame */
	public SplashGame( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		DrawableImage bkg = new DrawableImage( PATH_SPLASH + "bkg.png" );
		bkg.defineReferencePixel( bkg.getSize().x >> 1, bkg.getSize().y >> 1 );
		bkg.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, SPLASH_DEFAULT_HEIGHT >> 1 );
		insertDrawable( bkg );
		
		DrawableImage img = new DrawableImage( PATH_SPLASH + "boss.png" );
		img.defineReferencePixel( img.getSize().x >> 1, img.getSize().y );
		img.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT );
		insertDrawable( img );		
		
		img = new DrawableImage( PATH_SPLASH + "ounce.png" );
		img.defineReferencePixel( 0, img.getSize().y );
		img.setRefPixelPosition( ( ScreenManager.SCREEN_WIDTH - SPLASH_DEFAULT_WIDTH ) >> 1, ScreenManager.SCREEN_HEIGHT );
		insertDrawable( img );		
		
		img = new DrawableImage( PATH_SPLASH + "monkey.png" );
		img.defineReferencePixel( 0, img.getSize().y );
		img.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT );
		insertDrawable( img );				
		
		img = new DrawableImage( PATH_SPLASH + "logo.png" );
		img.defineReferencePixel( img.getSize().x >> 1, img.getSize().y >> 1 );
		img.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, bkg.getPosition().y );
		insertDrawable( img );		
		
		
		labelPressAnyKey = new RichLabel( font, "", null );
		labelPressAnyKey.setSize( ScreenManager.SCREEN_WIDTH, 0 );
		labelPressAnyKey.setText( DemoMIDlet.getText( TEXT_PRESS_ANY_KEY ) );
		labelPressAnyKey.setSize( ScreenManager.SCREEN_WIDTH, labelPressAnyKey.getTextTotalHeight() );
		labelPressAnyKey.defineReferencePixel( labelPressAnyKey.getSize().x >> 1, labelPressAnyKey.getSize().y + MENU_ITEMS_SPACING );
		labelPressAnyKey.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT );
		labelPressAnyKey.setVisible( false );
		insertDrawable( labelPressAnyKey );		
		
	}

	
	public final void update( int delta ) {
		accTime += delta;
		
		if ( accTime >= MIN_TIME ) {
			final int diff = ( accTime - MIN_TIME ) % BLINK_RATE;
			if ( diff > ( BLINK_RATE >> 1 ) ) {
				accTime = MIN_TIME;
				labelPressAnyKey.setVisible( !labelPressAnyKey.isVisible() );
			}
		}
	}

	
	public final void keyPressed( int key ) {
		if ( accTime >= MIN_TIME )
			DemoMIDlet.setScreen( SCREEN_MAIN_MENU );
	}

	
	public final void keyReleased( int key ) {
	}
	
	
	
}
