/*
 * SplashGame.java
 *
 * Created on October 3, 2007, 12:35 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;

/**
 *
 * @author peter
 */
public final class SplashGame extends UpdatableGroup implements Constants, KeyListener
//#if SCREEN_SIZE == "MEDIUM_BIG"
		, PointerListener
//#endif

{
	
	private static final byte TOTAL_ITEMS = 10;
	
	// tempo m�nimo que o logo do jogo � exibido
	private int transitionTime = 3000;	
	
	private final MarqueeLabel labelPressAnyKey;
	
	
	/** Creates a new instance of SplashGame */
	public SplashGame() throws Exception {
		super( TOTAL_ITEMS );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			final Point diff = new Point( NanoMath.max( SPLASH_MAX_WIDTH - ScreenManager.SCREEN_WIDTH, 0 ), 
										  NanoMath.max( SPLASH_MAX_HEIGHT - ScreenManager.SCREEN_HEIGHT, 0 ) );
			
			final Pattern pattern = new Pattern( new DrawableImage( PATH_SPLASH + "pattern.png" ) );
			pattern.setPosition( 0, -diff.y >> 1 );
			pattern.setSize( ScreenManager.SCREEN_WIDTH, pattern.getFill().getHeight() );
			insertDrawable( pattern );
			
			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

			final DrawableImage car = new DrawableImage( PATH_SPLASH + "car.png" );
			car.setPosition( ( -diff.x >> 1 ) + SPLASH_CAR_X, -( diff.y >> 1 ) + SPLASH_CAR_Y );
			insertDrawable( car );

			if ( ScreenManager.SCREEN_HEIGHT > car.getHeight() ) {
				// insere a fuma�a da parte superior
				final DrawableImage top = new DrawableImage( PATH_SPLASH + "top.png" );
				top.defineReferencePixel( 0, top.getHeight() );
				top.setRefPixelPosition( -diff.x, car.getPosition().y );
				insertDrawable( top );

				if ( ScreenManager.SCREEN_HEIGHT > car.getPosY() + car.getHeight() ) {
					// insere o ch�o
					final DrawableImage bottom = new DrawableImage( PATH_SPLASH + "bottom.png" );
					bottom.setPosition( -diff.x >> 1, car.getPosY() + car.getHeight() );
					insertDrawable( bottom );
				}
			}

			if ( car.getPosition().x > 0 ) {
				// insere a parte da esquerda, com fuma�a
				final DrawableImage left = new DrawableImage( PATH_SPLASH + "left.png" );
				left.defineReferencePixel( left.getWidth(), 0 );
				left.setRefPixelPosition( car.getPosition() );
				insertDrawable( left );
			}

			// insere o logo Full Power
			final DrawableImage logo = new DrawableImage( PATH_IMAGES + "title.png" );
			logo.defineReferencePixel( logo.getWidth() >> 1, logo.getHeight() );
			logo.setRefPixelPosition( size.x >> 1, ( SPLASH_MAX_HEIGHT - diff.y ) >> 1 );
			insertDrawable( logo );
			
			// insere o logo Nano Games
			final DrawableImage nano = new DrawableImage( PATH_SPLASH + "nano_small.png" );
			nano.setPosition( 0, size.y - nano.getHeight() );
			insertDrawable( nano );			

			// insere o marqueeLabel indicando "pressione qualquer tecla"
			labelPressAnyKey = new MarqueeLabel( GameMIDlet.getDefaultFont(), GameMIDlet.getText( TEXT_PRESS_ANY_KEY ), size.x / 3, MarqueeLabel.SCROLL_MODE_NONE );
			labelPressAnyKey.setSize( size.x, labelPressAnyKey.getHeight() );
			labelPressAnyKey.setVisible( false );
			labelPressAnyKey.defineReferencePixel( 0, labelPressAnyKey.getHeight() );
			labelPressAnyKey.setRefPixelPosition( 0, size.y - MENU_ITEMS_SPACING - nano.getHeight() );
			insertDrawable( labelPressAnyKey );		
		
		//#else
//# 		
//# 			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//# 		
//# 			final DrawableImage splash = new DrawableImage( PATH_SPLASH + "splash.png" );
//# 			splash.defineReferencePixel( splash.getWidth() >> 1, splash.getHeight() >> 1 );
//# 			splash.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
//# 			
//# 			insertDrawable( splash );
//# 			
//#  			// insere o logo Full Power
//#  			final DrawableImage logo = new DrawableImage( PATH_IMAGES + "title.png" );
//#  			logo.defineReferencePixel( logo.getWidth() >> 1, logo.getHeight() );
//#  			logo.setRefPixelPosition( size.x >> 1, size.y >> 1 );
//#  			insertDrawable( logo );			
//# 			
//# 			// insere o logo Nano Games
//# 			final DrawableImage nano = new DrawableImage( PATH_SPLASH + "nano_small.png" );
//# 			nano.setPosition( 0, size.y - nano.getHeight() );
//# 			insertDrawable( nano );
//# 
//# 			// insere o marqueeLabel indicando "pressione qualquer tecla"
//# 			labelPressAnyKey = new MarqueeLabel( GameMIDlet.getDefaultFont(), GameMIDlet.getText( TEXT_PRESS_ANY_KEY ), size.x / 3, MarqueeLabel.SCROLL_MODE_NONE );
//# 			labelPressAnyKey.setSize( size.x, labelPressAnyKey.getHeight() );
//# 			labelPressAnyKey.setVisible( false );
//# 			labelPressAnyKey.defineReferencePixel( 0, labelPressAnyKey.getHeight() );
//# 			labelPressAnyKey.setRefPixelPosition( 0, size.y - MENU_ITEMS_SPACING - nano.getHeight() );
//# 			insertDrawable( labelPressAnyKey );			
//# 			
		//#endif
	}

	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( !labelPressAnyKey.isVisible() ) {
			transitionTime -= delta;
			if ( transitionTime <= 0 ) {
				labelPressAnyKey.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
				labelPressAnyKey.setTextOffset( size.x );
				labelPressAnyKey.setVisible( true );
			}
		}
	}

	
	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
			transitionTime = 0;
		//#endif
		
		if ( transitionTime <= 0 )
			GameMIDlet.setScreen( SCREEN_LOADING );
	}

	
	public final void keyReleased( int key ) {
	}
	
	
	//#if SCREEN_SIZE == "MEDIUM_BIG"

	public final void onPointerDragged(int x, int y) {
	}

	
	public final void onPointerPressed(int x, int y) {
		keyPressed( 0 );
	}

	
	public final void onPointerReleased(int x, int y) {
	}
	
	//#endif	
	
}
