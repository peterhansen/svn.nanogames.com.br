/*
 * PlayScreen.java
 *
 * Created on October 4, 2007, 4:30 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Asphalt;
import core.Constants;
import core.DashBoard;
import core.Engine;
import core.Message;
import core.Racer;
import core.RacerAI;
import core.Tree;
import core.TutorialMessage;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
		//#if SCREEN_SIZE == "MEDIUM_BIG"
		, PointerListener
		//#endif

{
	
	private static final byte TOTAL_ITEMS = 25;
	
	// extens�o da pista, em cm
	private int trackLength;
	
	private final Asphalt asphalt;
	
	private DashBoard board;
	
	// atributos da c�mera
	// dist�ncia real entre os corredores relativa � varia��o m�xima de posicionamento vertical do jogador
	private final int PLAYER_OFFSET_REAL_DISTANCE;
	private final int PLAYER_Y_MIN;
	private int PLAYER_OFFSET_MAX;
	private int PLAYER_Y_MAX;
	
	private Racer player;
	private Racer opponent;
	
	private byte drawablesToRemove;
	
	private byte tutorialDrawablesToRemove;
	
	// caixas que ficam entre as pistas, medindo a velocidade dos carros
	private final DrawableImage box;
	
	private final int DRAWABLE_INDEX_PLAYER;
	
	// indicador da luz de largada
	private final Tree tree;
	
	// bandeiras de largada queimada do jogador e do oponente
	private final DrawableImage[] playerFlags = new DrawableImage[ 2 ];
	private final DrawableImage[] opponentFlags = new DrawableImage[ 2 ];
	
	// dist�ncia real em cent�metros entre as caixas
	private static final short DISTANCE_REAL_BETWEEN_BOXES = 40 * METER;
	// dist�ncia em pixels entre as caixas medidoras de velocidade dos carros
	private static final int DISTANCE_PIXELS_BETWEEN_BOXES = GameMIDlet.convertToPixel( DISTANCE_REAL_BETWEEN_BOXES );
	
	private final DrawableImage startLineLeft;
	private final DrawableImage startLineRight;
	private final DrawableImage endLine;
	private final int END_LINE_INITIAL_Y = -GameMIDlet.convertToPixel( DEFAULT_TRACK_LENGTH );
	
	/** dura��o em milisegundos das etapas de pr�-est�gio e est�gio */
	private static final short TIME_PRE_STAGE	= 1500;
	private static final short TIME_STAGE		= TIME_PRE_STAGE << 1;
	
	// caso seja false, � sinal de que o jogador cancelou o carregamento da tela de jogo
	private static boolean load;	
	
	// estado atual da tela de jogo
	private byte state;
	private byte lastState;
	
	// tempo acumulado no estado atual da tela de jogo
	private int accTime;
	
	// label utilizado para dar alguma informa��o ao jogador (largada queimada, perdeu, ganhou, novo recorde, etc)
	private final Message message;
	
	// <editor-fold desc="treinamento">
	/** caixa de texto que d� as informa��es do treinamento para o jogador */
	private TutorialMessage tutorial;
	
	private static final byte TUTORIAL_STAGE_NONE					= 0;
	private static final byte TUTORIAL_STAGE_CONTROLS_AUTO			= TUTORIAL_STAGE_NONE + 1;
	private static final byte TUTORIAL_STAGE_CONTROLS_MANUAL		= TUTORIAL_STAGE_CONTROLS_AUTO + 1;
	private static final byte TUTORIAL_STAGE_START_AUTO				= TUTORIAL_STAGE_CONTROLS_MANUAL + 1;
	private static final byte TUTORIAL_STAGE_START_MANUAL			= TUTORIAL_STAGE_START_AUTO + 1;
	private static final byte TUTORIAL_STAGE_WAIT_START				= TUTORIAL_STAGE_START_MANUAL + 1;
	private static final byte TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT	= TUTORIAL_STAGE_WAIT_START + 1;
	private static final byte TUTORIAL_STAGE_WAIT_GEAR_DOWN			= TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT + 1;
	private static final byte TUTORIAL_STAGE_GEAR_DOWN_MESSAGE		= TUTORIAL_STAGE_WAIT_GEAR_DOWN + 1;
	private static final byte TUTORIAL_STAGE_GEAR_DOWN_OK			= TUTORIAL_STAGE_GEAR_DOWN_MESSAGE + 1;
	private static final byte TUTORIAL_STAGE_MESSAGE_SHIFT_LIGHT	= TUTORIAL_STAGE_GEAR_DOWN_OK + 1;
	private static final byte TUTORIAL_STAGE_MESSAGE_DAMAGE			= TUTORIAL_STAGE_MESSAGE_SHIFT_LIGHT + 1;
	private static final byte TUTORIAL_STAGE_SHIFT_LIGHT_STOP_ONLY	= TUTORIAL_STAGE_MESSAGE_DAMAGE + 1;
	private static final byte TUTORIAL_STAGE_GEAR_CHANGE_MANUAL		= TUTORIAL_STAGE_SHIFT_LIGHT_STOP_ONLY + 1;
	private static final byte TUTORIAL_STAGE_COMPLETED_OK			= TUTORIAL_STAGE_GEAR_CHANGE_MANUAL + 1;
	private static final byte TUTORIAL_STAGE_COMPLETED_ERROR		= TUTORIAL_STAGE_COMPLETED_OK + 1;
	
	private byte tutorialStage;
	
	private Indicator indicator1;
	private Indicator indicator2;
	
	// </editor-fold>
	
	// tipos de corrida
	public static final byte TYPE_SINGLE_RACE	= 0;
	public static final byte TYPE_TRAINING		= 1;
	public static final byte TYPE_CHAMPIONSHIP	= 2;
	
	private byte type;
	
	// tempo m�nimo ap�s a corrida se encerrar (carro quebrar ou jogador terminar de frear seu carro) at� que o jogador
	// possa sair da tela de jogo, ou iniciar uma nova corrida (no modo single race)
	private static final short TIME_RACE_ENDED = 3000;
	
	// tempo em que a luz indicadora de queimada de largada � exibida antes da mensagem
	private static final short TIME_RED_LIGHT = 666; // dem�in!

	private static boolean easyMode;
	
	private static final byte RED_LIGHT_NONE		= 0;
	private static final byte RED_LIGHT_PLAYER		= 1;
	private static final byte RED_LIGHT_OPPONENT	= 2;
	
	private byte redLightRacerIndex;
	
	/** posi��o y da "c�mera" */
	private int cameraY;
	
	// indicadores das dicas para o jogador
	private final Sprite indicatorGearUp;
	private final Sprite indicatorGearDown;
	private final Sprite indicatorPedal;
	
	private byte indicatorSequence;
	
	private static PlayScreen instance;
	
	
	public PlayScreen() throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// aloca e insere o asfalto
		asphalt = Asphalt.createInstance();
		asphalt.defineReferencePixel( asphalt.getWidth() >> 1, 0 );
		asphalt.setRefPixelPosition( size.x >> 1, 0 );		
		insertDrawable( asphalt );
		
		// insere as linhas de largada e chegada
		startLineLeft = new DrawableImage( PATH_IMAGES + "start_0.png" );
		startLineLeft.defineReferencePixel( startLineLeft.getWidth() >> 1, startLineLeft.getHeight() >> 1 );
		insertDrawable( startLineLeft );
		
		startLineRight = new DrawableImage( PATH_IMAGES + "start_1.png" );
		startLineRight.defineReferencePixel( startLineRight.getWidth() >> 1, startLineRight.getHeight() >> 1 );
		insertDrawable( startLineRight );
		
		endLine = new DrawableImage( PATH_IMAGES + "end.png" );
		endLine.defineReferencePixel( endLine.getWidth() >> 1, 0 );
		endLine.setRefPixelPosition( size.x >> 1, END_LINE_INITIAL_Y );
		insertDrawable( endLine );		
		
		box = new DrawableImage( PATH_IMAGES + "box.png" );
		box.defineReferencePixel( box.getWidth() >> 1, box.getHeight() );
		box.setRefPixelPosition( size.x >> 1, size.y );
		DRAWABLE_INDEX_PLAYER = insertDrawable( box ) + 1;
		
		PLAYER_OFFSET_REAL_DISTANCE = GameMIDlet.convertToCm( size.y );
		PLAYER_Y_MIN = PLAYSCREEN_MIN_PLAYER_Y_SCREEN_PERCENT * size.y / 100;
		
		// carrega e insere as bandeiras de queimada largada
		playerFlags[ 0 ] = new DrawableImage( PATH_IMAGES + "flag.png" );
		DrawableImage flag = playerFlags[ 0 ];
		flag.defineReferencePixel( 0, flag.getHeight() );
		flag.setVisible( false );
		insertDrawable( flag );
		
		playerFlags[ 1 ] = new DrawableImage( flag );
		flag = playerFlags[ 1 ];
		flag.defineReferencePixel( 0, flag.getHeight() );
		flag.setVisible( false );
		insertDrawable( flag );
		
		opponentFlags[ 0 ] = new DrawableImage( flag );
		flag = opponentFlags[ 0 ];
		flag.defineReferencePixel( flag.getWidth(), flag.getHeight() );
		flag.setVisible( false );
		insertDrawable( flag );
		
		opponentFlags[ 1 ] = new DrawableImage( flag );
		flag = opponentFlags[ 1 ];
		flag.defineReferencePixel( flag.getWidth(), flag.getHeight() );
		flag.setVisible( false );
		insertDrawable( flag );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
		if ( size.x > DASHBOARD_TOTAL_WIDTH ) {
			// a largura da tela � maior que a imagem do painel - complementa o restante com patterns
			final DrawableImage patternImg = new DrawableImage( PATH_BOARD + "pattern.png" );
			Pattern p = new Pattern( patternImg );
			p.setSize( ( size.x - DASHBOARD_TOTAL_WIDTH ) >> 1, patternImg.getHeight() );
			p.defineReferencePixel( 0, p.getHeight() );
			p.setRefPixelPosition( 0, size.y );
			insertDrawable( p );
			
			p = new Pattern( patternImg );
			p.setSize( ( size.x - DASHBOARD_TOTAL_WIDTH ) >> 1, patternImg.getHeight() );
			p.defineReferencePixel( p.getWidth(), p.getHeight() );
			p.setRefPixelPosition( size.x, size.y );
			insertDrawable( p );			
		}
		//#endif

		// insere a "�rvore de natal" (luzes de largada)
		tree = new Tree( this );
		insertDrawable( tree );
		
		// insere a anima��o do acelerador sendo pressionado
		indicatorPedal = new Sprite( PATH_IMAGES + "ok.dat", PATH_IMAGES + "ok" );
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			if ( ScreenManager.getInstance().hasPointerEvents() )		
				indicatorPedal.defineReferencePixel( indicatorPedal.getWidth() >> 1, indicatorPedal.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING_POINTER );
			else
				indicatorPedal.defineReferencePixel( indicatorPedal.getWidth() >> 1, indicatorPedal.getHeight() );
		//#else
//# 			indicatorPedal.defineReferencePixel( indicatorPedal.getWidth() >> 1, indicatorPedal.getHeight() );
		//#endif
		indicatorPedal.setListener( this, 0 );

		insertDrawable( indicatorPedal );

		// insere a anima��o indicando para subir a marcha
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			indicatorGearUp = new Sprite( PATH_IMAGES + ( GameMIDlet.isLowMemory() ? "up_low.dat" : "up.dat" ), PATH_IMAGES + "up" );
			
			if ( ScreenManager.getInstance().hasPointerEvents() )
				indicatorGearUp.defineReferencePixel( indicatorGearUp.getWidth() >> 1, indicatorGearUp.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING_POINTER );
			else
				indicatorGearUp.defineReferencePixel( indicatorGearUp.getWidth() >> 1, indicatorGearUp.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING );
		//#else
//# 			indicatorGearUp = new Sprite( PATH_IMAGES + "up.dat", PATH_IMAGES + "up" );
//# 			indicatorGearUp.defineReferencePixel( indicatorGearUp.getWidth() >> 1, indicatorGearUp.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING );
		//#endif
		
		insertDrawable( indicatorGearUp );

		// insere a anima��o indicando para reduzir a marcha
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			indicatorGearDown = new Sprite( PATH_IMAGES + ( GameMIDlet.isLowMemory() ? "down_low.dat" : "down.dat" ), PATH_IMAGES + "down" );
		//#else
//# 			indicatorGearDown = new Sprite( PATH_IMAGES + "down.dat", PATH_IMAGES + "down" );
		//#endif
		indicatorGearDown.defineReferencePixel( indicatorGearDown.getWidth() >> 1, indicatorGearDown.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING );

		insertDrawable( indicatorGearDown );
		
		// insere o label indicador de mensagens
		message = new Message();
		insertDrawable( message );
		
	}

	
	public static final PlayScreen createInstance( byte type, int length, byte playerIndex, byte opponentIndex, byte opponentSkillLevel ) throws Exception {
		if ( instance == null ) {
			instance = new PlayScreen();
		}
		
		instance.setInfo( type, length, new Racer( playerIndex, true ), new RacerAI( opponentIndex, true, opponentSkillLevel ) );
		
		return instance;
	}
	
	
	private final void setInfo( byte type, int length, Racer player, Racer opponent ) throws Exception {
		if ( length <= 0 ) {
			//#if DEBUG == "true"
				throw new Exception( "comprimento da pista deve ser maior que zero" );
			//#else
//# 				throw new Exception();
			//#endif
		}
		this.trackLength = length;
		
		removeOldDrawables();
		
		// cria e insere o painel de informa��es da parte inferior da tela
		board = new DashBoard( player );
		board.setRefPixelPosition( size.x >> 1, size.y ); 
		int index = insertDrawable( board );
		setDrawableIndex( index, DRAWABLE_INDEX_PLAYER );
		++drawablesToRemove;
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			if ( ScreenManager.getInstance().hasPointerEvents() ) {
				// posiciona os indicadores de comandos para o jogador
				indicatorGearDown.setRefPixelPosition( size.x >> 1, board.getPosition().y );

				indicatorPedal.setRefPixelPosition( size.x >> 1, indicatorGearDown.getPosition().y - PLAYSCREEN_INDICATOR_Y_SPACING_POINTER );
				indicatorGearUp.setRefPixelPosition( size.x >> 1, indicatorPedal.getPosition().y - PLAYSCREEN_INDICATOR_Y_SPACING_POINTER );
			} else {
 				// posiciona os indicadores de comandos para o jogador
 				indicatorPedal.setRefPixelPosition( size.x >> 1, board.getPosition().y );
 
 				indicatorGearUp.setRefPixelPosition( size.x >> 1, easyMode ? board.getPosition().y : indicatorPedal.getPosition().y );
 				indicatorGearDown.setRefPixelPosition( indicatorGearUp.getRefPixelPosition() );			
			}
		//#else
//# 			// posiciona os indicadores de comandos para o jogador
//# 			indicatorPedal.setRefPixelPosition( size.x >> 1, board.getPosition().y );
//# 
//# 			indicatorGearUp.setRefPixelPosition( size.x >> 1, easyMode ? board.getPosition().y : indicatorPedal.getPosition().y );
//# 			indicatorGearDown.setRefPixelPosition( indicatorGearUp.getRefPixelPosition() );
		//#endif

		player.engine.setPlayScreen( instance );

		indicatorSequence = -1;
		
		playerFlags[ 0 ].setRefPixelPosition( 0, board.getPosition().y );
		playerFlags[ 1 ].setRefPixelPosition( playerFlags[ 1 ].getWidth(), board.getPosition().y );				
		opponentFlags[ 0 ].setRefPixelPosition( size.x, board.getPosition().y );
		opponentFlags[ 1 ].setRefPixelPosition( size.x - opponentFlags[ 1 ].getWidth(), board.getPosition().y );

		this.player = player;
		PLAYER_Y_MAX = size.y - player.getCarBodyHeight();
		PLAYER_OFFSET_MAX = ( PLAYER_Y_MAX - PLAYER_Y_MIN );
		index = insertDrawable( player );
		setDrawableIndex( index, DRAWABLE_INDEX_PLAYER );
		++drawablesToRemove;
		
		this.opponent = opponent;
		index = insertDrawable( opponent );
		setDrawableIndex( index, DRAWABLE_INDEX_PLAYER );
		++drawablesToRemove;
		
		switch ( type ) {
			case TYPE_TRAINING:
				indicator1 = new Indicator();
				insertDrawable( indicator1 );
				++tutorialDrawablesToRemove;

				indicator2 = new Indicator();
				insertDrawable( indicator2 );			
				++tutorialDrawablesToRemove;

				tutorial = new TutorialMessage();
				insertDrawable( tutorial );	
				++tutorialDrawablesToRemove;
				
				setTutorialStage( TUTORIAL_STAGE_NONE );
			
			case TYPE_SINGLE_RACE:
			case TYPE_CHAMPIONSHIP:
				//#if LOW_JAR == "false"
				// no modo campeonato, mostra a torcida
				asphalt.setCrowdVisible( type == TYPE_CHAMPIONSHIP );
				//#endif
				
				player.loadCar();
				player.setRefPixelPosition( ( size.x >> 1 ) - PLAYSCREEN_CAR_OFFSET_X, size.y );
				
				opponent.loadCar();
				opponent.setRefPixelPosition( ( size.x >> 1 ) + PLAYSCREEN_CAR_OFFSET_X, size.y );
				
				startLineLeft.setRefPixelPosition( player.getRefPixelX(), 0 );
				startLineRight.setRefPixelPosition( opponent.getRefPixelX(), 0 );
			break;
			
			default:
				//#if DEBUG == "true"
					throw new Exception( "tipo inv�lido: " + type );
				//#else
//# 					throw new Exception();
				//#endif
		}
		this.type = type;
		setState( RACE_STATE_NONE );
		
		updateCamera();
	}
	
	
	public final void update( int delta ) {
		if ( checkUpdate() ) {
			switch ( state ) {
				case RACE_STATE_PRE_STAGING:
					if ( player.getState() == RACE_STATE_STAGING && opponent.getState() == RACE_STATE_STAGING )
						setState( RACE_STATE_STAGING );
					
					if ( accTime >= TIME_PRE_STAGE ) {
						tree.setState( Tree.STATE_PRE_STAGE_1 );
					}					
				break;

				case RACE_STATE_STAGING:
					switch ( player.getState() ) {
						case RACE_STATE_WRECKED:
							setState( RACE_STATE_FINISHED ); // jogador quebrou o carro na largada
						break;						
					}
					
					if ( player.getRealPosition() > 0 ) {
						// jogador queimou a largada
						redLightRacerIndex = RED_LIGHT_PLAYER;
						setState( RACE_STATE_RED_LIGHT );
					} else if ( opponent.getRealPosition() > 0 ) {
						// oponente queimou a largada
						redLightRacerIndex = RED_LIGHT_OPPONENT;
						setState( RACE_STATE_RED_LIGHT );
					}					
					
					if ( accTime >= TIME_STAGE ) {
						setState( RACE_STATE_COUNTDOWN );
						tree.setState( Tree.STATE_AMBER_1 );
					} else if ( accTime >= TIME_PRE_STAGE ) {
						tree.setState( Tree.STATE_PRE_STAGE_2 );
					}
				break;

				case RACE_STATE_COUNTDOWN:
					if ( !easyMode && !player.engine.getShiftLight().isVisible() || player.engine.getShiftLight().getFrameSequenceIndex() != Engine.SHIFT_LIGHT_FRAME_RED )
						setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
					
					// evita que o jogador fique acelerando at� estourar o motor no modo treinamento
					if ( type == TYPE_TRAINING && player.engine.getRotation() >= Engine.ROTATION_NEUTRAL_MAX_TORQUE_END )
						stopAcceleration();

					switch ( player.getState() ) {
						case RACE_STATE_WRECKED:
							setState( RACE_STATE_FINISHED ); // jogador quebrou o carro na largada
						break;
					}					
					
					if ( !tree.checkStart() ) {
						if ( player.getRealPosition() > 0 ) {
							// jogador queimou a largada
							redLightRacerIndex = RED_LIGHT_PLAYER;
							setState( RACE_STATE_RED_LIGHT );
						} else if ( opponent.getRealPosition() > 0 ) {
							// oponente queimou a largada
							redLightRacerIndex = RED_LIGHT_OPPONENT;
							setState( RACE_STATE_RED_LIGHT );
						}
					}
				break; // fim case RACE_STATE_COUNTDOWN

				case RACE_STATE_RED_LIGHT:
					if ( accTime >= TIME_RED_LIGHT )
						setState( RACE_STATE_RED_LIGHT_MESSAGE );
				break; // fim case RACE_STATE_RED_LIGHT

				case RACE_STATE_RED_LIGHT_MESSAGE:
					if ( !message.isVisible() ) {
						// mensagem de largada queimada foi removida; verifica se o corredor que queimou a largada foi eliminado
						Racer racer = null;
						switch ( redLightRacerIndex ) {
							case RED_LIGHT_PLAYER:
								racer = player;
							break;

							case RED_LIGHT_OPPONENT:
								racer = opponent;
							break;
						} // fim switch ( redLightRacerIndex )

						// verifica se foi a 1� ou segunda vez que o corredor queimou a largada
						if ( racer.increaseRedLights() ) {
							setState( RACE_STATE_FINISHED ); // um dos corredores queimou a largada 2 vezes
						} else {
							setState( RACE_STATE_PRE_STAGING );
						}					
					}
				break;

				case RACE_STATE_SHOW_RESULTS:
					if ( !message.isVisible() ) {
						if ( easyMode ) {
							// no modo f�cil, n�o pode submeter recordes, ent�o avan�a direto para a tela de resultados 
							setState( RACE_STATE_FINISHED );
						} else {
							// verifica se houve novo recorde
							switch ( type ) {
								case TYPE_CHAMPIONSHIP:
								case TYPE_SINGLE_RACE:
									if ( HighScoresScreen.setTime( player.getTrackTime() ) ) {
										setState( RACE_STATE_SHOW_TIME_RECORD );
										break;
									} else if ( HighScoresScreen.setSpeed( player.engine.getTopSpeed() ) ) {
										setState( RACE_STATE_SHOW_SPEED_RECORD );
										break;
									}

								default:
									// n�o bateu nenhum recorde, ou est� no modo treino
									setState( RACE_STATE_FINISHED );
							} // fim switch ( type )
						}
					} // fim if ( !message.isVisible() )
				break;

				case RACE_STATE_SHOW_TIME_RECORD:
					if ( !message.isVisible() ) {
						// n�o precisa verificar se est� no modo f�cil nem o tipo de corrida, pois s� pode estar nesse estado 
						// caso esteja no modo dif�cil e num modo que possa submeter recordes
						if ( HighScoresScreen.setSpeed( player.engine.getTopSpeed() ) ) {
							setState( RACE_STATE_SHOW_SPEED_RECORD );
						} else {
							// n�o bateu recorde de velocidade
							setState( RACE_STATE_FINISHED );	
						}
					}
				break;			

				case RACE_STATE_SHOW_SPEED_RECORD:
					if ( !message.isVisible() )
						setState( RACE_STATE_FINISHED );	
				break;

				case RACE_STATE_RACING:
					switch ( player.getState() ) {
						case RACE_STATE_RACING:
							switch ( tutorialStage ) {
								case TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT:
									// no modo treinamento, quando o shift light se acende, p�ra e informa ao jogador seu funcionamento
									if ( player.engine.getShiftLight().isVisible() && player.engine.getShiftLight().getFrameSequenceIndex() == Engine.SHIFT_LIGHT_FRAME_GREEN ) {
										switch ( player.engine.getGear() ) {
											case Engine.GEAR_NEUTRAL:
											case 1:
												setTutorialStage( TUTORIAL_STAGE_MESSAGE_SHIFT_LIGHT );
											break;
											
											case 2:
												setTutorialStage( TUTORIAL_STAGE_GEAR_DOWN_MESSAGE );
											break;
											
											case 3:
												setTutorialStage( TUTORIAL_STAGE_MESSAGE_DAMAGE );
											break;
											
											default:
												setTutorialStage( TUTORIAL_STAGE_SHIFT_LIGHT_STOP_ONLY );
										} // fim switch ( player.engine.getGear() )
									}
								break;
							}
							
							if ( player.getRealPosition() >= trackLength ) {
								player.setState( RACE_STATE_BRAKING );

								// jogador acabou de terminar a prova; faz ajuste do seu tempo
								player.adjustTrackTime();
							}

							if ( opponent.getRealPosition() >= trackLength ) {
								if ( opponent.getState() == RACE_STATE_RACING ) {
									// oponente acabou de terminar a prova; faz ajuste do seu tempo
									opponent.setState( RACE_STATE_BRAKING );
									opponent.adjustTrackTime();
								}
							}
						break;

						case RACE_STATE_WRECKED:
							setState( RACE_STATE_FINISHED ); // jogador quebrou o carro
						break;

						case RACE_STATE_BRAKING:
						case RACE_STATE_FINISHED:
							switch ( opponent.getState() ) {
								case RACE_STATE_WRECKED:
								case RACE_STATE_FINISHED:
									setState( RACE_STATE_SHOW_RESULTS ); // jogador cruzou a linha de chegada, e oponente quebrou ou tamb�m terminou a prova
								break;
							}
						break;
					} // fim switch ( player.getState() )

					switch ( opponent.getState() ) {
						case RACE_STATE_RACING:
							if ( opponent.getRealPosition() >= trackLength ) {
								opponent.setState( RACE_STATE_BRAKING );	

								// oponente acabou de terminar a prova; faz ajuste do seu tempo
								opponent.adjustTrackTime();
							}
						break;
					}
				break; // fim case RACE_STATE_RACING

				case RACE_STATE_FINISHED:
					if ( accTime >= TIME_RACE_ENDED ) {
						switch ( type ) {
							case TYPE_TRAINING:
								if ( player.getState() != RACE_STATE_WRECKED ) {
									if ( !tutorial.isVisible() )
										GameMIDlet.setScreen( SCREEN_MAIN_MENU );
									break;
								}
							
							case TYPE_SINGLE_RACE:
								setState( RACE_STATE_RACE_AGAIN );
							break;

							case TYPE_CHAMPIONSHIP:
								setState( RACE_STATE_BACK_TO_PLAYOFFS );
							break;
						} // fim switch ( type )
					} // fim if ( accTime >= TIME_RACE_ENDED )
				break; // fim case RACE_STATE_FINISHED

				case RACE_STATE_PAUSED:
				return;
			} // fim switch ( state )

			super.update( delta );

			accTime += delta;
			board.setMinimapPercent( player.getRealPosition() * 100 / trackLength, 
									 opponent.getRealPosition() * 100 / trackLength );

			updateCamera();
		} else {
			tutorial.update( delta );
			indicator1.update( delta );
			indicator2.update( delta );
			
			indicatorPedal.update( delta );
			indicatorGearUp.update( delta );
			indicatorGearDown.update( delta );
		}
	} // fim do m�todo update( int )
	
	
	private final void setCameraPosition( int y ) {
		if ( y < PLAYER_Y_MIN )
			y = PLAYER_Y_MIN;
		
		cameraY = y;
		
		final int playerY = GameMIDlet.convertToPixel( player.getRealPosition() );
		final int opponentY = GameMIDlet.convertToPixel( opponent.getRealPosition() );
		int offset =  ( opponentY - playerY ) * PLAYER_OFFSET_MAX / PLAYER_OFFSET_REAL_DISTANCE;
		if ( offset > PLAYER_OFFSET_MAX )
			offset = PLAYER_OFFSET_MAX;
		
		startLineLeft.setRefPixelPosition( startLineLeft.getRefPixelX(), y + offset );
		startLineRight.setRefPixelPosition( startLineRight.getRefPixelX(), y + offset );
		endLine.setRefPixelPosition( endLine.getRefPixelX(), y + offset + END_LINE_INITIAL_Y );

		// posiciona os cones
		box.setRefPixelPosition( box.getRefPixelX(), ( y + offset ) % DISTANCE_PIXELS_BETWEEN_BOXES );
		
		// atualiza o pattern do asfalto, para dar a impress�o de movimento dos carros em rela��o � pista
		asphalt.setDistance( cameraY + offset );
		asphalt.setSpeed( player.engine.getSpeed() );
		
		int playerScreenY = cameraY - playerY + offset;
		if ( state >= RACE_STATE_COUNTDOWN ) {
			if ( playerScreenY < PLAYER_Y_MIN )
				playerScreenY = PLAYER_Y_MIN;
			else if ( playerScreenY > PLAYER_Y_MAX )
				playerScreenY = PLAYER_Y_MAX;
		}
		
		player.setRefPixelPosition( player.getRefPixelX(), playerScreenY );
		
		opponent.setRefPixelPosition( opponent.getRefPixelX(), cameraY - offset - opponentY );
	}
	
	
	/**
	 * Atualiza a posi��o da c�mera da pista, posicionando os corredores, asfalto e outros elementos de acordo.
	 */
	private final void updateCamera() {
		switch ( state ) {
			case RACE_STATE_PRE_STAGING:
			case RACE_STATE_STAGING:
				setCameraPosition( PLAYER_Y_MIN );
			break;
			
			default:
				setCameraPosition( GameMIDlet.convertToPixel( player.getRealPosition() ) );
		}
	}
	
	
	public final void keyPressed( int key ) {
		if ( message.isVisible() ) {
			message.skip();
			return;
		}

		if ( !keyPressedTutorial( key ) ) {
			switch ( key ) {
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_BACK:
					switch ( state ) {
						case RACE_STATE_RACE_AGAIN:
							GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						break;
						
						default:
							setState( RACE_STATE_PAUSED );
					}
				break;
				
				default:
					switch ( state ) {
						case RACE_STATE_NONE:
						case RACE_STATE_PRE_STAGING:
						case RACE_STATE_FINISHED:
						case RACE_STATE_BRAKING:
						case RACE_STATE_PAUSED:
						case RACE_STATE_WRECKED:
						case RACE_STATE_SHOW_RESULTS:
						case RACE_STATE_RED_LIGHT:
						case RACE_STATE_SHOW_SPEED_RECORD:
						case RACE_STATE_SHOW_TIME_RECORD:
						case RACE_STATE_RED_LIGHT_MESSAGE:
						case RACE_STATE_BACK_TO_PLAYOFFS:
						break;
						
						default:
							drive( key );
					}
			}
		}
	}

	
	public final void keyReleased( int key ) {
		// no modo f�cil, n�o � necess�rio tratar esse evento, pois o controle de acelera��o � autom�tico
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.LEFT:
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				stopAcceleration();
			break;
			// fim default
		} // fim switch ( key )
	} // fim do m�todo keyReleased( int )

	
	public final void hideNotify() {
		switch ( state ) {
			case RACE_STATE_COUNTDOWN:
			case RACE_STATE_RACING:
			case RACE_STATE_STAGING:
				setState( RACE_STATE_PAUSED );
			break;
		}
	}

	
	public final void showNotify() {
		switch ( state ) {
			case RACE_STATE_PAUSED:
				setState( RACE_STATE_RACING );
			break;
			
			case RACE_STATE_RACING:
			case RACE_STATE_BRAKING:
				setState( RACE_STATE_PAUSED );
			break;
		}
	}
	
	
	public final boolean setState( int state ) {
		if ( state != this.state && !message.isVisible() ) {
			switch ( state ) {
				case RACE_STATE_NONE:
					tree.setState( Tree.STATE_HIDDEN );
					setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
					
					player.setVisible( false );
					opponent.setVisible( false );
				break;
				
				case RACE_STATE_COUNTDOWN:
					tree.setState( Tree.STATE_AMBER_1 );
					
					if ( easyMode )
						player.engine.setAcceleration( Engine.ACCELERATION_AUTOMATIC );
					else
						player.engine.setAcceleration( Engine.ACCELERATION_OFF );
					
					opponent.engine.setAcceleration( Engine.ACCELERATION_AUTOMATIC );
				break;
				
				case RACE_STATE_PRE_STAGING:
					setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
					
					tree.setState( Tree.STATE_APPEARING );
					player.setState( state );
					opponent.setState( state );
					updateCamera();
					
					switch ( player.getRedLights() ) {
						case 0:
							playerFlags[ 0 ].setVisible( false );
							playerFlags[ 1 ].setVisible( false );
						break;
						
						case 1:
							playerFlags[ 0 ].setVisible( true );
							playerFlags[ 1 ].setVisible( false );
						break;
						
						case 2:
							playerFlags[ 0 ].setVisible( true );
							playerFlags[ 1 ].setVisible( true );
						break;
					}
					
					switch ( opponent.getRedLights() ) {
						case 0:
							opponentFlags[ 0 ].setVisible( false );
							opponentFlags[ 1 ].setVisible( false );
						break;
						
						case 1:
							opponentFlags[ 0 ].setVisible( true );
							opponentFlags[ 1 ].setVisible( false );
						break;
						
						case 2:
							opponentFlags[ 0 ].setVisible( true );
							opponentFlags[ 1 ].setVisible( true );
						break;
					}
				break;
				
				case RACE_STATE_RACING:
					switch ( tutorialStage ) {
						case TUTORIAL_STAGE_CONTROLS_AUTO:
						case TUTORIAL_STAGE_CONTROLS_MANUAL:
						case TUTORIAL_STAGE_START_AUTO:
							setTutorialStage( TUTORIAL_STAGE_WAIT_START );
						break;

						case TUTORIAL_STAGE_START_MANUAL:
							switch ( this.state ) {
								case RACE_STATE_COUNTDOWN:
									// caso esteja no treino do modo manual, espera que jogador esteja com rota��o adequada para a largada
									if ( !easyMode && ( !player.engine.getShiftLight().isVisible() ||
												player.engine.getShiftLight().getFrameSequenceIndex() != Engine.SHIFT_LIGHT_FRAME_GREEN ) )
									{
										return false;
									}
							}
							
							if ( player.getState() == RACE_STATE_WRECKED )
								setState( RACE_STATE_RACE_AGAIN );
							else
								setTutorialStage( TUTORIAL_STAGE_WAIT_START );
						break;
					}
					
					tree.setState( Tree.STATE_HIDING );
					
				case RACE_STATE_STAGING:
					player.setState( state );
					opponent.setState( state );
					
					if ( state == RACE_STATE_STAGING ) {
						if ( type == TYPE_TRAINING && tutorialStage == TUTORIAL_STAGE_NONE )
							setTutorialStage( easyMode ? TUTORIAL_STAGE_CONTROLS_AUTO : TUTORIAL_STAGE_CONTROLS_MANUAL );						
					}
				break;
				
				case RACE_STATE_SHOW_RESULTS:
					setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
					
					String top = GameMIDlet.getText( TEXT_TIME ) + GameMIDlet.formatTime(  player.getTrackTime() );
					String bottom = GameMIDlet.getText( TEXT_SPEED ) + GameMIDlet.formatSpeed( player.engine.getTopSpeed() );
					
					top += GameMIDlet.getText( TEXT_VERSUS ) + GameMIDlet.formatTime( opponent.getTrackTime() );
					bottom += GameMIDlet.getText( TEXT_VERSUS ) + GameMIDlet.formatSpeed( opponent.engine.getTopSpeed() );
					
					showMessage( top, bottom );
				break;
				
				case RACE_STATE_SHOW_TIME_RECORD:
					showMessage( GameMIDlet.getText( TEXT_NEW_RECORD_TIME ), 
										  "\n" + GameMIDlet.formatTime( player.getTrackTime() ) + GameMIDlet.getText( TEXT_SECONDS ) );
				break;
				
				case RACE_STATE_SHOW_SPEED_RECORD:
					showMessage( GameMIDlet.getText( TEXT_NEW_RECORD_SPEED ), 
										  "\n" + GameMIDlet.formatSpeed( player.engine.getTopSpeed() ) + GameMIDlet.getText( TEXT_KMH ) );
				break;

				case RACE_STATE_FINISHED:
					setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
					stopAcceleration();
					
					switch ( type ) {
						case TYPE_TRAINING:
							// caso o jogador tenha quebrado o motor durante o treino, n�o exibe a mensagem de treino
							// completado com sucesso
							if ( player.getState() != RACE_STATE_WRECKED ) {
								setTutorialStage( TUTORIAL_STAGE_COMPLETED_OK );
								break;
							}
						
						case TYPE_CHAMPIONSHIP:
						case TYPE_SINGLE_RACE:
							switch ( player.getState() ) {
								case RACE_STATE_FINISHED:
									switch ( opponent.getState() ) {
										case RACE_STATE_FINISHED:
											if ( player.getTrackTime() > opponent.getTrackTime() ) {
												MediaPlayer.play( SOUND_RACE_LOST, 1 );
												showMessage( GameMIDlet.getText( TEXT_YOU ), GameMIDlet.getText( TEXT_LOSE ) );
												break;
											}
										default:
											// jogador venceu
											MediaPlayer.play( SOUND_RACE_WON, 1 );
											showMessage( GameMIDlet.getText( TEXT_YOU ), GameMIDlet.getText( TEXT_WIN ) );
									}
								break;

								case RACE_STATE_WRECKED:
									MediaPlayer.vibrate( VIBRATION_TIME_CAR_WRECKED );
									
									showMessage( GameMIDlet.getText( TEXT_CAR ), GameMIDlet.getText( TEXT_WRECKED ) );
								break;
								
								case RACE_STATE_RED_LIGHT:
									MediaPlayer.play( SOUND_RACE_LOST, 1 );
									showMessage( GameMIDlet.getText( TEXT_YOU ), GameMIDlet.getText( TEXT_LOSE ) );
								break;
								
								default:
									MediaPlayer.play( SOUND_RACE_WON, 1 );
									showMessage( GameMIDlet.getText( TEXT_YOU ), GameMIDlet.getText( TEXT_WIN ) );
							} // fim switch ( player.getState() )
						break;
					} // fim switch ( type )
				break; // fim case RACE_STATE_FINISHED

				case RACE_STATE_RACE_AGAIN:
					GameMIDlet.prepareRace( SCREEN_RACE_AGAIN, player.getIndex(), opponent.getIndex(), -1 );
				break;

				case RACE_STATE_PAUSED:
					lastState = this.state;
					GameMIDlet.setScreen( SCREEN_PAUSE );
				break;
				
				case RACE_STATE_RED_LIGHT:
					stopAcceleration();
					
					tree.setState( Tree.STATE_RED );
					
					switch ( redLightRacerIndex ) {
						case RED_LIGHT_PLAYER:
							player.setState( RACE_STATE_RED_LIGHT );
							
							if ( !playerFlags[ 0 ].isVisible() )
								playerFlags[ 0 ].setVisible( true );
							else
								playerFlags[ 1 ].setVisible( true );
						break;
						
						case RED_LIGHT_OPPONENT:
							opponent.setState( RACE_STATE_RED_LIGHT );
							
							if ( !opponentFlags[ 0 ].isVisible() )
								opponentFlags[ 0 ].setVisible( true );
							else
								opponentFlags[ 1 ].setVisible( true );							
						break;
					}
				break;
				
				case RACE_STATE_RED_LIGHT_MESSAGE:
					showMessage( GameMIDlet.getText( TEXT_RED_LIGHT_TOP ), GameMIDlet.getText( TEXT_RED_LIGHT_BOTTOM ) );
				break;
				
				//#if LOW_JAR == "false"
				case RACE_STATE_BACK_TO_PLAYOFFS:
					boolean playerWon = true;

					switch ( player.getState() ) {
						case RACE_STATE_RED_LIGHT:
						case RACE_STATE_WRECKED:
							playerWon = false;
						break;

						case RACE_STATE_FINISHED:
							switch ( opponent.getState() ) {
								case RACE_STATE_FINISHED:
									if ( player.getTrackTime() > opponent.getTrackTime() )
										playerWon = false;
								break;
							}
						break;
					} // fim switch ( player.getState() )
					GameMIDlet.playerRaceEnded( playerWon );					
				break;
				//#endif

				default:
					return false;
			}

			//#if DEBUG == "true"
			System.out.println( "PLAYSCREEN.SETSTATE->PREVIOUS: " + this.state + ", NEW: " + state );
			//#endif
			this.state = ( byte ) state;
			if ( state != RACE_STATE_PAUSED )
				lastState = -1;
			
			accTime = 0;
			
			return true;
		} // fim if ( state != this.state )
		
		return false;
	}
	
	
	public final void unpause() {
		if ( lastState >= 0 ) {
			state = lastState;
			lastState = -1;
		}
	}

	
	private final void drive( int key ) {
		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM1:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM3:
				player.engine.gearUp( true );
			break;
			
			case ScreenManager.FIRE:
			case ScreenManager.LEFT:
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				// a acelera��o � autom�tica no modo f�cil
				if ( !easyMode && state >= RACE_STATE_PRE_STAGING )
					player.engine.setAcceleration( Engine.ACCELERATION_ON );
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM7:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM9:
				player.engine.gearDown( true );
			break;
		} // fim switch ( key )		
	}
	
	
	public static final void setEasyMode( boolean easyMode ) {
		PlayScreen.easyMode = easyMode; 
	}
	
	
	public static final boolean isEasyMode() {
		return easyMode;
	}
	
	
	private final void stopAcceleration() {
		switch ( state ) {
			case RACE_STATE_FINISHED:
			case RACE_STATE_BRAKING:
			case RACE_STATE_WRECKED:
			case RACE_STATE_SHOW_RESULTS:
			case RACE_STATE_SHOW_SPEED_RECORD:
			case RACE_STATE_SHOW_TIME_RECORD:
			case RACE_STATE_RACE_AGAIN:
				player.engine.setAcceleration( Engine.ACCELERATION_BRAKING );
			break;
			
			case RACE_STATE_RED_LIGHT:
			case RACE_STATE_RED_LIGHT_MESSAGE:
				player.engine.setAcceleration( Engine.ACCELERATION_OFF );
			break;
			
			case RACE_STATE_RACING:
				// esse teste � para evitar que, no caso de o jogador vencer com boa vantagem, o carro de repente pare
				// de baixar marchas, ultrapassando o limite do int que armazena a dist�ncia percorrida da pista e com 
				// isso causando problemas de visualiza��o
				if ( player.getState() != RACE_STATE_RACING ) {
					player.engine.setAcceleration( Engine.ACCELERATION_BRAKING );
					break;
				}
			case RACE_STATE_STAGING:
			case RACE_STATE_COUNTDOWN:
				if ( !easyMode )
					player.engine.setAcceleration( Engine.ACCELERATION_OFF );
			break;
		}			
	}

	
	private static final class Indicator extends Drawable implements Updatable {
		
		private static final int RECT_COLOR = 0xff0000;
		
		private static final short BLINK_RATE = 200;
		private int accTime;
		
		private boolean active;
		
		
		private Indicator() {
			setVisible( false );
		}
		
		
		protected final void paint( Graphics g ) {
			g.setColor( RECT_COLOR );

			g.drawRect( translate.x, translate.y, size.x - 1, size.y - 1 );
			g.drawRect( translate.x + 1, translate.y + 1, size.x - 3, size.y - 3 );
		}

		
		public final void update( int delta ) {
			if ( active ) {
				accTime += delta;

				if ( accTime >= BLINK_RATE ) {
					accTime = 0;
					setVisible( !visible );
				}
			}
		}
		
		
		private final void setTarget( int x, int y, int width, int height ) {
			setPosition( x, y );
			setSize( width, height );
			
			setActive( true );
		}
		
		
		private final void setActive( boolean active ) {
			this.active = active;
			accTime = 0;
			
			setVisible( active );
		}
		
	}
	
	
	/**
	 * Trata eventos de tecla no modo tutorial.
	 * @param key �ndice da tecla pressionada.
	 * @return boolean indicando se o tutorial tratou completamente a tecla pressionada (true), ou se repassou ao bloco
	 * chamador (false).
	 */
	private final boolean keyPressedTutorial( int key ) {
		if ( type == TYPE_TRAINING ) {
			final byte previousGear = player.engine.getGear();
			
			final byte GEAR_CHANGE_NONE	= 0;
			final byte GEAR_CHANGE_UP	= 1;
			final byte GEAR_CHANGE_DOWN	= 2;
			
			byte gearChange = GEAR_CHANGE_NONE;

			switch ( key ) {
				case ScreenManager.KEY_NUM1:
				case ScreenManager.KEY_NUM2:
				case ScreenManager.KEY_NUM3:
				case ScreenManager.UP:
					gearChange = GEAR_CHANGE_UP;
				break;

				case ScreenManager.KEY_NUM7:
				case ScreenManager.KEY_NUM8:
				case ScreenManager.KEY_NUM9:
				case ScreenManager.DOWN:
					gearChange = GEAR_CHANGE_DOWN;
				break;
			}
			
			
			switch ( tutorialStage ) {
				case TUTORIAL_STAGE_CONTROLS_AUTO:
				case TUTORIAL_STAGE_CONTROLS_MANUAL:
					setTutorialStage( tutorialStage == TUTORIAL_STAGE_CONTROLS_AUTO ? TUTORIAL_STAGE_START_AUTO : TUTORIAL_STAGE_START_MANUAL );
				break;

				case TUTORIAL_STAGE_START_MANUAL:
				case TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT:
					switch ( gearChange ) {
						case GEAR_CHANGE_NONE:
						return false;
					}
				break;
				
				case TUTORIAL_STAGE_SHIFT_LIGHT_STOP_ONLY:
				case TUTORIAL_STAGE_GEAR_CHANGE_MANUAL:
				case TUTORIAL_STAGE_MESSAGE_SHIFT_LIGHT:
				case TUTORIAL_STAGE_MESSAGE_DAMAGE:				
				case TUTORIAL_STAGE_WAIT_START:
					switch ( gearChange ) {
						case GEAR_CHANGE_NONE:
						return false;
						
						case GEAR_CHANGE_UP:
							if ( setTutorialStage( TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT ) )
								player.engine.gearUp( true );
						break;
					}
				break;
					
				case TUTORIAL_STAGE_WAIT_GEAR_DOWN:
					switch ( gearChange ) {
						case GEAR_CHANGE_NONE:
						return false;
						
						case GEAR_CHANGE_DOWN:
							if ( setTutorialStage( TUTORIAL_STAGE_GEAR_DOWN_OK ) )
								player.engine.gearDown( true );
						break;
					}
				break;					
					
				case TUTORIAL_STAGE_GEAR_DOWN_MESSAGE:
					// engata a 4� marcha, para poder exemplificar o comando de redu��o de marcha
					while ( player.engine.getGear() < 4 )
						player.engine.gearUp( false );
					
					setTutorialStage( TUTORIAL_STAGE_WAIT_GEAR_DOWN );	
				break;
					
				case TUTORIAL_STAGE_GEAR_DOWN_OK:
					setTutorialStage( TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT );					
				break;
				
				case TUTORIAL_STAGE_COMPLETED_OK:
					tutorial.skip();
				break;				

			} // fim switch ( tutorialStage )
			
			return true;
		} // fim if ( type == TYPE_TRAINING )
		
		return false;
	}

	
	private final boolean setTutorialStage( byte stage ) {
		if ( tutorial.skip() ) {
			indicator1.setActive( false );
			indicator2.setActive( false );
			
			switch ( stage ) {
				case TUTORIAL_STAGE_CONTROLS_AUTO:
					tutorial.setText( TEXT_TRAINING_START_AUTO );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
					
					indicator1.setTarget( tree.getPosition().x, tree.getPosition().y, tree.getWidth(), tree.getHeight() );
				break;
				
				case TUTORIAL_STAGE_START_MANUAL:
					setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
					
					indicator1.setTarget( board.getPosition().x + DASHBOARD_SHIFT_LIGHT_AREA_X, board.getPosition().y + DASHBOARD_SHIFT_LIGHT_AREA_Y,
										 DASHBOARD_SHIFT_LIGHT_AREA_WIDTH, DASHBOARD_SHIFT_LIGHT_AREA_HEIGHT );
					
					indicator2.setTarget( board.getPosition().x + DASHBOARD_TACHOMETER_START_AREA_X, board.getPosition().y + DASHBOARD_TACHOMETER_START_AREA_Y,
										 DASHBOARD_TACHOMETER_START_AREA_WIDTH, DASHBOARD_TACHOMETER_START_AREA_HEIGHT );															
				break;

				case TUTORIAL_STAGE_WAIT_START:
					tutorial.setText( TEXT_TRAINING_1ST_GEAR_AUTO );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_UP );
					
					indicator1.setTarget( tree.getPosition().x, tree.getPosition().y, tree.getWidth(), tree.getHeight() );
				break;

				case TUTORIAL_STAGE_CONTROLS_MANUAL:
					tutorial.setText( TEXT_TRAINING_START_MANUAL );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
					
					indicator1.setTarget( tree.getPosition().x, tree.getPosition().y, tree.getWidth(), tree.getHeight() );
				break;

				case TUTORIAL_STAGE_MESSAGE_SHIFT_LIGHT:
					tutorial.setText( TEXT_TRAINING_GEAR_UP );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_UP );
					
					indicator1.setTarget( board.getPosition().x + DASHBOARD_SHIFT_LIGHT_AREA_X, board.getPosition().y + DASHBOARD_SHIFT_LIGHT_AREA_Y,
										 DASHBOARD_SHIFT_LIGHT_AREA_WIDTH, DASHBOARD_SHIFT_LIGHT_AREA_HEIGHT );
				break;

				case TUTORIAL_STAGE_MESSAGE_DAMAGE:
					tutorial.setText( TEXT_TRAINING_DAMAGE );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_UP );
					
					indicator1.setTarget( board.getPosition().x + DASHBOARD_DAMAGE_AREA_X, board.getPosition().y + DASHBOARD_DAMAGE_AREA_Y,
										 DASHBOARD_DAMAGE_AREA_WIDTH, DASHBOARD_DAMAGE_AREA_HEIGHT );					
					
					indicator2.setTarget( board.getPosition().x + DASHBOARD_TACHOMETER_DAMAGE_AREA_X, board.getPosition().y + DASHBOARD_TACHOMETER_DAMAGE_AREA_Y,
										 DASHBOARD_TACHOMETER_DAMAGE_AREA_WIDTH, DASHBOARD_TACHOMETER_DAMAGE_AREA_HEIGHT );										
				break;			

				case TUTORIAL_STAGE_GEAR_CHANGE_MANUAL:
					tutorial.setText( TEXT_TRAINING_START_AUTO );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
				break;

				case TUTORIAL_STAGE_GEAR_DOWN_MESSAGE:
					tutorial.setText( TEXT_TRAINING_GEAR_DOWN );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_DOWN );
				break;

				case TUTORIAL_STAGE_GEAR_DOWN_OK:
					tutorial.setText( TEXT_TRAINING_GEAR_DOWN_OK );
					
					setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
				break;			

				case TUTORIAL_STAGE_COMPLETED_OK:
					tutorial.setText( TEXT_TRAINING_COMPLETED );
				break;

				case TUTORIAL_STAGE_COMPLETED_ERROR:
					tutorial.setText( TEXT_TRAINING_START_AUTO );
				break;
				
				case TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT:
					setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
				break;
			} // fim switch ( stage )

			tutorialStage = stage;
			
			stopAcceleration();
			return true;
		}
		return false;
	}
	
	
	private final boolean checkUpdate() {
		switch ( tutorialStage ) {
			case TUTORIAL_STAGE_NONE:
			case TUTORIAL_STAGE_START_AUTO:
			case TUTORIAL_STAGE_START_MANUAL:				
			case TUTORIAL_STAGE_COMPLETED_OK:
			case TUTORIAL_STAGE_COMPLETED_ERROR:
			case TUTORIAL_STAGE_WAIT_GREEN_SHIFT_LIGHT:
			case TUTORIAL_STAGE_WAIT_GEAR_DOWN:
			return true;
			
			default:
			return false;
		} // fim switch ( stage )
	}
	
	
	public static final void removeOldDrawables() {
		if ( instance != null ) {
			if ( instance.drawablesToRemove > 0 ) {
				instance.player = null;
				instance.opponent = null;
				instance.board = null;			

				while ( instance.drawablesToRemove > 0 ) {
					instance.removeDrawable( instance.DRAWABLE_INDEX_PLAYER );
					--instance.drawablesToRemove;
				}
			}

			// os drawables utilizados no treinamento s�o garantidamente os �ltimos do grupo
			if ( instance.tutorialDrawablesToRemove > 0 ) {
				instance.tutorial = null;
				instance.indicator1 = null;
				instance.indicator2 = null;

				while ( instance.tutorialDrawablesToRemove > 0 ) {
					instance.removeDrawable( instance.getUsedSlots() - 1 );
					--instance.tutorialDrawablesToRemove;
				}
			}

			System.gc();
		}
	}
	
	
	public static final Message getMessage() throws Exception {
		if ( instance == null )
			instance = new PlayScreen();
		
		return instance.message;
	}
	
	
	private final void showMessage( String textTop, String textBottom ) {
		message.setRefPixelPosition( size.x >> 1, size.y >> 1 );
		message.setText( textTop, textBottom );
	}	
	
	
	public final void setIndicatorSequence( byte sequence ) {
		if ( sequence != indicatorSequence ) {
			switch ( state ) {
				case RACE_STATE_NONE:
				case RACE_STATE_STAGING:
				case RACE_STATE_WRECKED:
				case RACE_STATE_FINISHED:
				case RACE_STATE_SHOW_RESULTS:
				case RACE_STATE_BRAKING:
				case RACE_STATE_PRE_STAGING:
				case RACE_STATE_RACE_AGAIN:
				case RACE_STATE_RED_LIGHT:
				case RACE_STATE_SHOW_SPEED_RECORD:
				case RACE_STATE_SHOW_TIME_RECORD:
				case RACE_STATE_PAUSED:
				case RACE_STATE_BACK_TO_PLAYOFFS:
					if ( sequence != PLAYSCREEN_INDICATOR_NONE )
						return;

				default:
					switch ( sequence ) {
						case PLAYSCREEN_INDICATOR_NONE:
							//#if SCREEN_SIZE == "MEDIUM_BIG"
								if ( !ScreenManager.getInstance().hasPointerEvents() ) {
									indicatorPedal.setVisible( false );
	 								indicatorGearUp.setVisible( false );
	 								indicatorGearDown.setVisible( false );								
								}
							//#else
//# 							indicatorPedal.setVisible( false );
//# 							indicatorGearUp.setVisible( false );
//# 							indicatorGearDown.setVisible( false );
							//#endif

							indicatorPedal.setSequence( PLAYSCREEN_PEDAL_SEQUENCE_RELEASED );
							indicatorGearUp.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED );
							indicatorGearDown.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED );
						break;

						case PLAYSCREEN_INDICATOR_ACCELERATE:
							if ( !easyMode ) {
								indicatorPedal.setVisible( true );

								indicatorPedal.setSequence( PLAYSCREEN_PEDAL_SEQUENCE_PRESSING );
							}
							//#if SCREEN_SIZE == "MEDIUM_BIG"
								if ( !ScreenManager.getInstance().hasPointerEvents() ) {
 									indicatorGearUp.setVisible( false );
 									indicatorGearDown.setVisible( false );
								}
							//#else
//# 							indicatorGearUp.setVisible( false );
//# 							indicatorGearDown.setVisible( false );
							//#endif

							indicatorGearUp.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED );
							indicatorGearDown.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED );					
						break;

						case PLAYSCREEN_INDICATOR_GEAR_UP:
							if ( !easyMode ) {
								indicatorPedal.setVisible( true );
								indicatorPedal.setSequence( PLAYSCREEN_PEDAL_SEQUENCE_RELEASING );
							}
							indicatorGearUp.setVisible( true );
							
							//#if SCREEN_SIZE == "MEDIUM_BIG"
								if ( !ScreenManager.getInstance().hasPointerEvents() ) {
									indicatorGearDown.setVisible( false );		
								}
							//#else
//# 								indicatorGearDown.setVisible( false );				
							//#endif

							indicatorGearDown.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED );					
							indicatorGearUp.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_ANIMATING );
						break;

						case PLAYSCREEN_INDICATOR_GEAR_DOWN:
							if ( !easyMode ) {
								indicatorPedal.setVisible( true );
								indicatorPedal.setSequence( PLAYSCREEN_PEDAL_SEQUENCE_RELEASING );
							}
							//#if SCREEN_SIZE == "MEDIUM_BIG"
								if ( !ScreenManager.getInstance().hasPointerEvents() ) {
									indicatorGearUp.setVisible( false );
								}
							//#else
//# 								indicatorGearUp.setVisible( false );
							//#endif
							
							indicatorGearDown.setVisible( true );				

							indicatorGearUp.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED );
							indicatorGearDown.setSequence( PLAYSCREEN_GEAR_CHANGE_SEQUENCE_ANIMATING );
						break;

						default:
							return;
					} // fim switch ( sequence )
					indicatorSequence = sequence;
				break;
			}
		} // fim if ( sequence != indicatorSequence )
	}

	
	public final void onSequenceEnded( int id, int sequence ) {
		// n�o � necess�rio verificar o id, pois PlayScreen s� se registra como listener da anima��o do pedal
		switch ( sequence ) {
			case PLAYSCREEN_PEDAL_SEQUENCE_RELEASING:
				indicatorPedal.setSequence( PLAYSCREEN_PEDAL_SEQUENCE_RELEASED );
			break;
		}
	}

	
	public final void sizeChanged( int width, int height ) {
	}

	
	public final void resetRacersRedLights() {
		player.resetRedLights();
		opponent.resetRedLights();
	}
	
	
	public final void setOpponentSkillLevel( int skillLevel ) {
		( ( RacerAI ) opponent ).setSkillLevel( skillLevel );
	}

	
	//#if SCREEN_SIZE == "MEDIUM_BIG"

	public final void onPointerDragged( int x, int y ) {
		if ( !message.isVisible() ) {
			if ( x >= indicatorPedal.getPosX() - PLAYSCREEN_INDICATOR_X_SPACING_POINTER && x <= indicatorPedal.getPosX() + indicatorPedal.getWidth() + PLAYSCREEN_INDICATOR_X_SPACING_POINTER ) {
				// clique foi dentro da faixa horizontal dos indicadores de pedal e marchas
				if ( y >= indicatorGearUp.getPosY() - PLAYSCREEN_INDICATOR_Y_SPACING && y <= board.getPosY() ) {
					// clique foi num dos indicadores
					if ( y >= indicatorPedal.getPosY() - PLAYSCREEN_INDICATOR_Y_SPACING && y <= indicatorPedal.getPosY()
							+ indicatorPedal.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING ) {
						keyPressed( ScreenManager.FIRE );
					} else {
						keyReleased( ScreenManager.FIRE );
					}
				} else {
					keyReleased( ScreenManager.FIRE );
				}
			} else {
				keyReleased( ScreenManager.FIRE );
			}		
		}
	}


	public final void onPointerPressed( int x, int y ) {
		if ( x >= board.getPosX() + PAUSE_AREA_START_X && y >= size.y - PAUSE_AREA_HEIGHT ) {
			keyPressed( ScreenManager.KEY_BACK );
		} else if ( type == TYPE_TRAINING && message.isVisible() && message.contains( x, y ) ) {
			keyPressed( ScreenManager.FIRE );
		} else if ( message.isVisible() && message.contains( x, y ) ) {
			message.skip();
		} else if ( x >= indicatorPedal.getPosX() - PLAYSCREEN_INDICATOR_X_SPACING_POINTER && x <= indicatorPedal.getPosX() + indicatorPedal.getWidth() + PLAYSCREEN_INDICATOR_X_SPACING_POINTER ) {
			// clique foi dentro da faixa horizontal dos indicadores de pedal e marchas
			if ( y >= indicatorGearUp.getPosY() - PLAYSCREEN_INDICATOR_Y_SPACING && y <= board.getPosY() ) {
				// clique foi num dos indicadores
				if ( y <= indicatorGearUp.getPosY() + indicatorGearUp.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING ) {
					keyPressed( ScreenManager.UP );
				} else if ( y <= indicatorPedal.getPosY() + indicatorPedal.getHeight() + PLAYSCREEN_INDICATOR_Y_SPACING ) {
					keyPressed( ScreenManager.FIRE );
				} else {
					keyPressed( ScreenManager.DOWN );
				}
			} else {
				keyReleased( ScreenManager.FIRE );
			}
		} else {
			keyReleased( ScreenManager.FIRE );
		}
	}


	public final void onPointerReleased( int x, int y ) {
		keyReleased( ScreenManager.FIRE );
	}
	
	//#endif
	

	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}
	
	
}
