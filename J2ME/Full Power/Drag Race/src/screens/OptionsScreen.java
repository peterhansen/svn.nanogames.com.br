/*
 * OptionsScreen.java
 *
 * Created on June 4, 2007, 5:00 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class OptionsScreen extends Menu implements Constants {
	
	//#ifdef NO_SOUND
//# 		public static final byte INDEX_OPTIONS			= 0;
//# 		public static final byte INDEX_VIBRATION		= 1;
//# 		public static final byte INDEX_ERASE_RECORDS	= 2;
//# 		public static final byte INDEX_BACK				= 3;
//# 
//# 		private static final byte TOTAL_LABELS		= ( byte ) ( MediaPlayer.isVibrationSupported() ? 4 : 3 );	
	//#else
		public static final byte INDEX_OPTIONS			= 0;
		public static final byte INDEX_SOUND			= 1;
		public static final byte INDEX_VIBRATION		= 2;
		public static final byte INDEX_ERASE_RECORDS	= 3;
		public static final byte INDEX_BACK				= 4;

		private static final byte TOTAL_LABELS		= ( byte ) ( MediaPlayer.isVibrationSupported() ? 5 : 4 );
	//#endif
	
	private static final byte TOTAL_ITEMS		= ( byte ) ( TOTAL_LABELS + 1 );
	
	private final Label[] labels = new Label[ TOTAL_LABELS ];
	
	private final ImageFont font;
	private boolean textsLoaded;

	// intervalo m�nimo entre mudan�as de op��o no som, para evitar quebras em alguns aparelhos Samsung
	private final int MIN_TIME_INTERVAL = 500;
	private int timeSinceLastChange = MIN_TIME_INTERVAL;
	
	
	/** Creates a new instance of OptionsScreen */
	public OptionsScreen( MenuListener listener, ImageFont font, int id ) throws Exception {
		super( listener, id, TOTAL_ITEMS );
		
		this.font = font;
		
		setSize( ScreenManager.SCREEN_WIDTH, 0 );
		
		loadTexts();
		
		final Drawable cursorTemp = GameMIDlet.getCursor();
		setCursor( cursorTemp, Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );		
		
		setCircular( true );
		setCurrentIndex( 1 );
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			return;
			
			case ScreenManager.KEY_SOFT_LEFT:
				key = ScreenManager.FIRE;
			case ScreenManager.FIRE:
			case ScreenManager.RIGHT:
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				switch ( currentIndex ) {
					//#ifndef NO_SOUND
					case INDEX_SOUND:
						if ( timeSinceLastChange >= MIN_TIME_INTERVAL ) {
							timeSinceLastChange = 0;
							toggleSound();
						}
					break;
					//#endif

					case INDEX_VIBRATION:
						if ( MediaPlayer.isVibrationSupported() ) {
							toggleVibration();
							break;
						}
				}				
			
			default:
				super.keyPressed( key );
		} // fim switch ( key )
	} // fim do m�todo keyPressed( int )
	
	
	//#ifndef NO_SOUND
		private final void toggleSound() {
			MediaPlayer.setMute( !MediaPlayer.isMuted() );

			loadTexts();
		}
	//#endif
	
	
	private final void toggleVibration() {
		MediaPlayer.setVibration( !MediaPlayer.isVibration() );
		
		MediaPlayer.vibrate( VIBRATION_TIME_CAR_WRECKED );
		
		loadTexts();
	}
	
	
	private final void loadTexts() {
		try {
			String[] texts;
			//#ifdef NO_SOUND
//# 				if ( MediaPlayer.isVibrationSupported() ) {
//# 					texts = new String[] { GameMIDlet.getText( TEXT_OPTIONS ),
//# 										   GameMIDlet.getText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON ),
											//#if SCREEN_SIZE == "MEDIUM_BIG"
//# 										   GameMIDlet.getText( TEXT_ERASE_RECORDS ),
											//#else
//# 											GameMIDlet.getText( TEXT_ERASE_RECORDS_SHORT ),
											//#endif
//# 										   GameMIDlet.getText( TEXT_BACK ),
//# 										  };
//# 				} else {
//# 					// n�o mostra a op��o de vibra��o
//# 					texts = new String[] { GameMIDlet.getText( TEXT_OPTIONS ),
											//#if SCREEN_SIZE == "MEDIUM_BIG"
//# 										   GameMIDlet.getText( TEXT_ERASE_RECORDS ),
											//#else
//# 											GameMIDlet.getText( TEXT_ERASE_RECORDS_SHORT ),
											//#endif
//# 										   GameMIDlet.getText( TEXT_BACK ),
//# 										  };				
//# 				}			
			//#else
				if ( MediaPlayer.isVibrationSupported() ) {
					texts = new String[] { GameMIDlet.getText( TEXT_OPTIONS ),
										   GameMIDlet.getText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF ),
										   GameMIDlet.getText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON ),
											//#if SCREEN_SIZE == "MEDIUM_BIG"
										   GameMIDlet.getText( TEXT_ERASE_RECORDS ),
											//#else
//# 											GameMIDlet.getText( TEXT_ERASE_RECORDS_SHORT ),
											//#endif
										   GameMIDlet.getText( TEXT_BACK )
										  };
				} else {
					// n�o mostra a op��o de vibra��o
					texts = new String[] { GameMIDlet.getText( TEXT_OPTIONS ),
										   GameMIDlet.getText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF ),
											//#if SCREEN_SIZE == "MEDIUM_BIG"
										   GameMIDlet.getText( TEXT_ERASE_RECORDS ),
											//#else
//# 											GameMIDlet.getText( TEXT_ERASE_RECORDS_SHORT ),
											//#endif
										   GameMIDlet.getText( TEXT_BACK )
										  };				
				}
			//#endif

			if ( !textsLoaded ) {
				int y = 0;
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					final Label l = new Label( font, texts[ i ] );

					labels[ i ] = l;
					l.defineReferencePixel( l.getWidth() >> 1, 0 );
					l.setRefPixelPosition( size.x >> 1, y );
					insertDrawable( l );

					y += l.getHeight() + MENU_ITEMS_SPACING;
					
					//#if SCREEN_SIZE == "MEDIUM_BIG"
						if ( i == 0 )
							y += MENU_ITEMS_SPACING;
					//#else
//# 						if ( i == 0 )
//# 							y += l.getHeight();
					//#endif
				}
				
				setSize( size.x, y );
				defineReferencePixel( size.x >> 1, size.y >> 1 );
				setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );				
				
				textsLoaded = true;
			} else {
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					final Label l = ( Label ) getDrawable( i );

					l.setText( texts[ i ] );

					l.defineReferencePixel( l.getWidth() >> 1, 0 );
					l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
				}
			}
			
			GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
			
			// reposiciona o cursor (largura do texto pode mudar)
			updateCursorPosition();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo loadTexts()

	
	public final void setCurrentIndex( int index ) {
		final int previousIndex = currentIndex;
		
		super.setCurrentIndex( index );
		
		if ( currentIndex <= 1 ) {
			if ( previousIndex < 1 || previousIndex == activeDrawables - 1 )
				super.setCurrentIndex( 1 );
			else if ( previousIndex == 1 && currentIndex < 1 ) {
				super.setCurrentIndex( circular ? activeDrawables - 1 : 1 );
			}
		}
	}

	
	public final void update( int delta ) {
		super.update( delta );
		
		timeSinceLastChange += delta;
	}
	
}
