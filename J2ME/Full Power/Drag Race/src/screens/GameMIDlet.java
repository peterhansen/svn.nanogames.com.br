/*
 * GameMIDlet.java
 *
 * Created on October 3, 2007, 11:47 AM
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Serializable;
import core.Car;
import core.Constants;
import core.DashBoard;
import core.Engine;
import core.Parachutes;
import core.Racer;
import core.RearTires;
import java.io.DataInputStream;
import java.io.DataOutputStream;


import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDletStateChangeException;


/**
 *
 * @author  peter
 * @version
 */
public class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable {
	
	// tempo m�ximo considerado de um frame
	private static final int MAX_GAME_FRAME_TIME = 125;
	
	// vari�veis utilizadas para formatar as strings de tempo e velocidade
	private static final byte MAX_CHARS = 6;
	private static final byte MAX_CHARS_2 = MAX_CHARS + 2;
	private static final char[] tempChars = new char[ 16 ];
	
	private static final byte SPEED_DECIMAL_PLACES = 3;
	private static final byte TIME_DECIMAL_PLACES = 2;	
	
	// tipos de background que podem ser usados
	private static final byte BACKGROUND_NONE		= 0;
	private static final byte BACKGROUND_ASPHALT	= 1;
	private static final byte BACKGROUND_COLOR		= 2;
	private static final byte BACKGROUND_PLAYSCREEN	= 3;
	
	// fontes utilizadas
	private static ImageFont FONT_DEFAULT;
	
	private static PlayScreen playScreen;
	
	//#if LOW_JAR == "false"
	private static Championship championship;
	//#endif
	
	private static byte playerIndex;
	private static byte opponentIndex;
	private static byte opponentSkillLevel;
	
	private Drawable cursor;
	
	private Pattern patternMetal;
	
	private static boolean hasSavedGame;
	
	private boolean lowMemory;
	
	
	protected final void loadResources() {
		try {
			//#if LOW_JAR == "true"
//# 					( ( GameMIDlet ) instance ).lowMemory = true;
			//#else
				switch ( getVendor() ) {
					//#if SCREEN_SIZE != "SMALL"
				case VENDOR_NOKIA:
					//#endif

					case VENDOR_SONYERICSSON:
					case VENDOR_SIEMENS:
						// todos os SonyEricsson possuem mem�ria e desempenho suficientes para rodar o jogo completo,
						// por�m como a mem�ria � din�mica, o valor retornado de totalMemory pode corresponder a somente
						// parte da mem�ria que pode ser de fato utilizada no jogo.
					break;

					default:
						( ( GameMIDlet ) instance ).lowMemory = ScreenManager.SCREEN_WIDTH < 200 && Runtime.getRuntime().totalMemory() < MEMORY_LOW_LIMIT;
				}
			//#endif						
//			( ( GameMIDlet ) instance ).lowMemory = false; // teste

			try {
				createDatabase( DATABASE_NAME, DATABASE_TOTAL );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			} // fim catch ( Exception e )

			//#ifdef NO_SOUND
//# 					MediaPlayer.init( DATABASE_NAME, DATABASE_INDEX_MEDIA, null );
//# 					MediaPlayer.setMute( true );
			//#else
				final String[] sounds = new String[ SOUND_TOTAL ]; 
				for ( int i = 0; i < sounds.length; ++i )
					sounds[ i ] = PATH_SOUNDS + i + ".mid";

				MediaPlayer.init( DATABASE_NAME, DATABASE_INDEX_MEDIA, sounds );
			//#endif

			FONT_DEFAULT = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
			FONT_DEFAULT.setCharOffset( DEFAULT_FONT_CHAR_OFFSET );

			// carrega o cursor
			if ( isLowMemory() )
				cursor = new DrawableImage( PATH_CURSOR_IMG );
			else
				cursor = new Sprite( "/cursor.dat", "/cursor" );
			
			cursor.defineReferencePixel( cursor.getWidth(), cursor.getHeight() >> 1 );

			loadTexts( TEXT_TOTAL, PATH_TEXTS + "pt.dat" );

			currentScreen = -1;
			//#ifdef NO_SOUND
//# 					setScreen( SCREEN_SPLASH_NANO );
			//#else
				setScreen( SCREEN_CHOOSE_SOUND );
			//#endif
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif				

			exit();
		}
	} // fim do m�todo loadResources()
	
	
	protected final int changeScreen( int index ) {
		if ( index != currentScreen ) {
			Drawable nextScreen = null;
			
			final GameMIDlet midlet = ( GameMIDlet ) instance;
			
			byte backgroundType = BACKGROUND_ASPHALT;
			
			byte indexSoftRight = -1;
			byte indexSoftLeft = -1;
			
			int bgColor = COLOR_BACKGROUND;
			
			try {
				switch ( index ) {
					case SCREEN_RACE_AGAIN:
					case SCREEN_CHOOSE_SOUND:
					case SCREEN_CONFIRM_MENU:
					case SCREEN_CONFIRM_EXIT:
					case SCREEN_ERASE_RECORDS:
						int idTitle = 0;
						byte defaultOption = BasicConfirmScreen.INDEX_NO;
						switch ( index ) {
							case SCREEN_CHOOSE_SOUND:
								backgroundType = BACKGROUND_COLOR;
								idTitle = TEXT_DO_YOU_WANT_SOUND;
								if ( !MediaPlayer.isMuted() )
									defaultOption = BasicConfirmScreen.INDEX_YES;
								
								bgColor = 0x000000;
							break;
							
							case SCREEN_CONFIRM_MENU:
								idTitle = TEXT_CONFIRM_BACK_MENU;
							break;
							
							case SCREEN_CONFIRM_EXIT:
								idTitle = TEXT_CONFIRM_EXIT;
							break;

							case SCREEN_RACE_AGAIN:
								//#if SCREEN_SIZE == "MEDIUM_BIG"
								backgroundType = BACKGROUND_PLAYSCREEN;
								//#else
//# 								if ( ScreenManager.SCREEN_HEIGHT < RACE_AGAIN_MIN_HEIGHT )
//# 									backgroundType = BACKGROUND_ASPHALT;
//# 								else
//# 									backgroundType = BACKGROUND_PLAYSCREEN;
								//#endif

								idTitle = TEXT_RACE_AGAIN;
								defaultOption = BasicConfirmScreen.INDEX_YES;
							break;
							
							case SCREEN_ERASE_RECORDS:
								idTitle = TEXT_ERASE_RECORDS_CONFIRM;
								indexSoftRight = TEXT_BACK;
							break;
						} // fim switch ( index )
						
						nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, cursor, idTitle, TEXT_YES, TEXT_NO );
						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );						
						//#ifndef LG_C1100
							indexSoftLeft = TEXT_OK;
						//#endif
						
						( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( defaultOption );
					break;
					
					case SCREEN_SPLASH_NANO:
						//#if LOW_JAR == "false"
						if ( !isLowMemory() ) {
							// carrega o pattern de fundo
							patternMetal = new Pattern( new DrawableImage( PATH_IMAGES + "metal.png" ) );
							patternMetal.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
						}
						//#endif						
						
						//#if SCREEN_SIZE == "SMALL"
//# 							nextScreen = new BasicSplashNano( SCREEN_SPLASH_FULL_POWER, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_SPLASH );
						//#else
							nextScreen = new BasicSplashNano( SCREEN_SPLASH_FULL_POWER, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_SPLASH );
						//#endif
						backgroundType = BACKGROUND_COLOR;
						bgColor = 0x000000;
					break;
					
					case SCREEN_SPLASH_FULL_POWER:
						// verifica se h� campeonato salvo anteriormente
						try {
							loadData( DATABASE_NAME, DATABASE_INDEX_SAVED_GAME, this );
						} catch ( Exception e ) {
						}						
						
						bgColor = 0x000000;
						nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, bgColor, PATH_SPLASH, PATH_SPLASH + "fullpower.png", TEXT_SPLASH_FULL_POWER );
						backgroundType = BACKGROUND_COLOR;
					break;

					case SCREEN_SPLASH_GAME:
						HighScoresScreen.createInstance( 0 );
						
						nextScreen = new SplashGame();
						backgroundType = BACKGROUND_COLOR;
						bgColor = 0x000000;
					break;
					
					case SCREEN_OPTIONS:
						nextScreen = new OptionsScreen( midlet, FONT_DEFAULT, index );
						indexSoftRight = TEXT_BACK;
					break;

					case SCREEN_CREDITS:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, TEXT_CREDITS_TEXT, true );
						indexSoftRight = TEXT_BACK;
					break;

					case SCREEN_HELP:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, getText( TEXT_HELP_TEXT ) + getVersion(), false );
						indexSoftRight = TEXT_BACK;
					break;

					//#if LOW_JAR == "false"
					case SCREEN_CHAMPIONSHIP_NEW:
					case SCREEN_CHAMPIONSHIP_LOAD:
						nextScreen = new Loader( new LoadListener() {
								public final void load( final Loader loadScreen ) throws Exception {
									PlayScreen.removeOldDrawables();
									Thread.yield();
									Championship.resetKeys();
									Thread.yield();
									championship = Championship.createInstance( null, playerIndex );

									loadScreen.setActive( false );

									setScreen( SCREEN_CHAMPIONSHIP_CONTINUE );
								}
							}, FONT_DEFAULT, TEXT_LOADING_SHORT );
					break;
					
					case SCREEN_CHAMPIONSHIP_CONTINUE:
						nextScreen = championship;
						backgroundType = isLowMemory() ? BACKGROUND_COLOR : BACKGROUND_NONE;
						
						Championship.setActive( true );
						
						indexSoftLeft = TEXT_OK;
						indexSoftRight = TEXT_BACK;
					break;
					
					//#endif

					case SCREEN_RACE_SINGLE:
					case SCREEN_RACE_TRAINING:
					case SCREEN_RACE_CHAMPIONSHIP:
						final int INDEX = index;
						nextScreen = new Loader( new LoadListener() {
								public final void load( final Loader loadScreen ) throws Exception {
									byte type = -1;
									switch ( INDEX ) {
										case SCREEN_RACE_SINGLE:
											type = PlayScreen.TYPE_SINGLE_RACE;
										break;

										case SCREEN_RACE_TRAINING:
											type = PlayScreen.TYPE_TRAINING;
										break;

										case SCREEN_RACE_CHAMPIONSHIP:
											type = PlayScreen.TYPE_CHAMPIONSHIP;
										break;
									}
									PlayScreen.removeOldDrawables();
									Thread.yield();
									playScreen = PlayScreen.createInstance( type, DEFAULT_TRACK_LENGTH, playerIndex, opponentIndex, opponentSkillLevel );

									loadScreen.setActive( false );

									setScreen( SCREEN_CONTINUE_GAME );
								}
							}, FONT_DEFAULT, TEXT_LOADING_SHORT );						
					break;
					
					case SCREEN_CONTINUE_GAME:
						MediaPlayer.stop();
						
						nextScreen = playScreen;

						backgroundType = BACKGROUND_NONE;
						
						if ( currentScreen == SCREEN_PAUSE )
							playScreen.unpause();
						else {
							playScreen.resetRacersRedLights();
							playScreen.setState( RACE_STATE_PRE_STAGING );
						}
					break;
					
					case SCREEN_PAUSE:
						MediaPlayer.stop();
						
						nextScreen = new PauseScreen( midlet, FONT_DEFAULT, index );
						
						//#ifndef LG_C1100
							indexSoftLeft = TEXT_OK;
						//#endif
						
						//#if SCREEN_SIZE == "SMALL"
//# 						if ( ScreenManager.SCREEN_HEIGHT > SCREEN_HEIGHT_MIN )
						//#endif
						indexSoftRight = TEXT_CONTINUE;
					break;
					
					case SCREEN_HIGH_SCORES_TIMES:
					case SCREEN_HIGH_SCORES_SPEED:
						nextScreen = HighScoresScreen.createInstance( index );
						indexSoftRight = TEXT_BACK;
					break;
					
					case SCREEN_ERROR:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, TEXT_LAST_ERROR, false );
						indexSoftRight = TEXT_BACK;
					break;

					case SCREEN_LOADING:
						backgroundType = BACKGROUND_NONE;
						nextScreen = new LoadScreen( FONT_DEFAULT );
					break;

					case SCREEN_NEW_GAME_MENU:
						PlayScreen.removeOldDrawables();
						
					//#if LOW_JAR == "false"
						Championship.resetKeys();					
					//#endif	
					
					case SCREEN_MAIN_MENU:
						playScreen = null;
					
					//#if LOW_JAR == "false"
						championship = null;
					case SCREEN_CHAMPIONSHIP_MENU:
					//#endif
						
						playerIndex = Racer.DUMMY_RACER_INDEX;						
					case SCREEN_TRAINING:
					case SCREEN_SINGLE_RACE_MENU:
					case SCREEN_HIGH_SCORES_MENU:
						nextScreen = createBasicMenu( index );

						//#if SCREEN_SIZE == "MEDIUM_BIG"
						if ( index != SCREEN_CHAMPIONSHIP_SAVED || ScreenManager.SCREEN_HEIGHT >= SCREEN_HEIGHT_MIN )
							indexSoftRight = index == SCREEN_MAIN_MENU ? TEXT_EXIT : TEXT_BACK;
						//#else
//# 						indexSoftRight = index == SCREEN_MAIN_MENU ? TEXT_EXIT : TEXT_BACK;
						//#endif
					break;
					
					//#if LOW_JAR == "false"
					case SCREEN_CHAMPIONSHIP_SAVED:
						//#if SCREEN_SIZE == "MEDIUM_BIG"
						nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, cursor, TEXT_CHAMPIONSHIP_SAVED, TEXT_CONTINUE, TEXT_NEW, TEXT_CANCEL, true );
						//#else
//# 							nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, cursor, TEXT_CHAMPIONSHIP_SAVED_SHORT, TEXT_YES, TEXT_NO, TEXT_CANCEL, false );
						//#endif
						
						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
						( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( INDEX_SCREEN_CHAMPIONSHIP_SAVED_CONTINUE );
						
						//#if SCREEN_SIZE == "SMALL"
//# 						nextScreen.move( 0, -( ( BasicConfirmScreen ) nextScreen ).getDrawable( 0 ).getPosY() >> 1 );
//# 					
//# 						if ( ScreenManager.SCREEN_HEIGHT > SCREEN_HEIGHT_MIN ) {
//# 							indexSoftLeft = TEXT_OK;
//# 							indexSoftRight = TEXT_CANCEL;
//# 						}
						//#else
							indexSoftLeft = TEXT_OK;
							indexSoftRight = TEXT_CANCEL;
						//#endif
					break;
					//#endif
						
					case SCREEN_CHOOSE_CHARACTER_SINGLE:
					//#if LOW_JAR == "false"
					case SCREEN_CHOOSE_CHARACTER_CHAMPIONSHIP:
					//#endif
						nextScreen = new CharacterScreen( index == SCREEN_CHOOSE_CHARACTER_SINGLE );
								
						indexSoftLeft = TEXT_OK;
						indexSoftRight = TEXT_BACK;
					break;
				} // fim switch ( index )
				
				setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );
				setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );
				setBackground( backgroundType, bgColor );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif

				if ( index != SCREEN_ERROR ) {
					texts[ TEXT_LAST_ERROR ] += new String( e.toString() + "\n" + e.getClass() + "\n" + e.getMessage() ).toUpperCase();
					return changeScreen( SCREEN_ERROR );
				}
				
				// deu erro ao alocar tela de texto; sai do jogo
				exit();
			}
			if ( nextScreen != null ) {
				( ( GameMIDlet ) instance ).manager.setCurrentScreen( nextScreen );
			}
		} // fim if ( index != currentScreen )
		
		return index;
	} // fim do m�todo setScreen( byte )
	
	
	public final void onChoose( Menu menu, int id, int index ) {
		if ( id == currentScreen ) {
			switch ( id ) {
				//<editor-fold desc="tela inicial (ativar sons?)">
				case SCREEN_CHOOSE_SOUND:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
						case BasicConfirmScreen.INDEX_NO:
							MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
						default:
							setScreen( SCREEN_SPLASH_NANO );
					}				
				break;			
				//</editor-fold>
				
				case SCREEN_SPLASH_FULL_POWER:
					setScreen( SCREEN_SPLASH_GAME );
				break;

				//<editor-fold desc="menu principal">
				case SCREEN_MAIN_MENU:
					switch ( index ) {
						case INDEX_SCREEN_MAIN_MENU_NEW_GAME:
							setScreen( SCREEN_NEW_GAME_MENU );
						break;

						case INDEX_SCREEN_MAIN_MENU_OPTIONS:
							setScreen( SCREEN_OPTIONS );
						break;
						
						case INDEX_SCREEN_MAIN_MENU_HIGH_SCORES:
							setScreen( SCREEN_HIGH_SCORES_MENU );
						break;						

						case INDEX_SCREEN_MAIN_MENU_HELP:
							setScreen( SCREEN_HELP );
						break;
						
						case INDEX_SCREEN_MAIN_MENU_CREDITS:
							setScreen( SCREEN_CREDITS );
						break;

						case INDEX_SCREEN_MAIN_MENU_EXIT:
							exit();
						break;
					}
				break;
				//</editor-fold>

				//<editor-fold desc="op��es">
				case SCREEN_OPTIONS:
					if ( !MediaPlayer.isVibrationSupported() && index >= OptionsScreen.INDEX_VIBRATION )
						++index;
					
					switch ( index ) {
						case OptionsScreen.INDEX_ERASE_RECORDS:
							setScreen( SCREEN_ERASE_RECORDS );
						break;
						
						case OptionsScreen.INDEX_BACK:
							setScreen( SCREEN_MAIN_MENU );
						break;
					}				
				break;
				//</editor-fold>
				
				//<editor-fold desc="pausa">
				case SCREEN_PAUSE:
					if ( !MediaPlayer.isVibrationSupported() && index >= PauseScreen.INDEX_VIBRATION )
						++index;
					
					switch ( index ) {
						case PauseScreen.INDEX_CONTINUE:
							playScreen.unpause();
							setScreen( SCREEN_CONTINUE_GAME );
						break;
						
						case PauseScreen.INDEX_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case PauseScreen.INDEX_EXIT:
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}
				break;
				//</editor-fold>
				
				//<editor-fold desc="confirma��o de volta para o menu">
				case SCREEN_CONFIRM_MENU:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
							setScreen( SCREEN_MAIN_MENU );
						break;
						
						default:
							setScreen( SCREEN_PAUSE );
					}				
				break;
				//</editor-fold>
				
				//<editor-fold desc="confirma��o de sa�da do jogo">
				case SCREEN_CONFIRM_EXIT:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
							exit();
						break;
						
						default:
							setScreen( SCREEN_PAUSE );
					}				
				break;			
				//</editor-fold>	

				//<editor-fold desc="menu de novo jogo">
				case SCREEN_NEW_GAME_MENU:
					switch ( index ) {
						//#if LOW_JAR == "false"
							case INDEX_SCREEN_NEW_GAME_MENU_CHAMPIONSHIP:
								// verifica se h� campeonato salvo, e mostrar tela correspondente
								if ( hasSavedGame() )
									setScreen( SCREEN_CHAMPIONSHIP_SAVED );
								else
									setScreen( SCREEN_CHAMPIONSHIP_MENU );
							break;

							case INDEX_SCREEN_NEW_GAME_MENU_BACK:
								setScreen( SCREEN_MAIN_MENU );
							break;
						//#else
//# 							case INDEX_SCREEN_LOW_NEW_GAME_MENU_BACK:
//# 								setScreen( SCREEN_MAIN_MENU );
//# 							break;
						//#endif
						
						case INDEX_SCREEN_NEW_GAME_MENU_SINGLE_RACE:
							setScreen( SCREEN_SINGLE_RACE_MENU );
						break;
						
						case INDEX_SCREEN_NEW_GAME_MENU_TRAINING:
							setScreen( SCREEN_TRAINING );
						break;
					}
				break;
				//</editor-fold>
				
				//<editor-fold desc="menu de treino">
				case SCREEN_TRAINING:
					switch ( index ) {
						case INDEX_SCREEN_TRAINING_SIMPLE:
						case INDEX_SCREEN_TRAINING_NORMAL:
							PlayScreen.setEasyMode( index == INDEX_SCREEN_TRAINING_SIMPLE );
							prepareRace( SCREEN_RACE_TRAINING, -1, -1, 0 );
						break;
						
						case INDEX_SCREEN_TRAINING_BACK:
							setScreen( SCREEN_NEW_GAME_MENU );
						break;
					}					
				break;
				//</editor-fold>
				
				//<editor-fold desc="corrida �nica">
				case SCREEN_SINGLE_RACE_MENU:
					switch ( index ) {
						case INDEX_SCREEN_SINGLE_RACE_BACK:
							setScreen( SCREEN_NEW_GAME_MENU );
						break;
						
						case INDEX_SCREEN_SINGLE_RACE_NORMAL:
						case INDEX_SCREEN_SINGLE_RACE_SIMPLE:
							PlayScreen.setEasyMode( index == INDEX_SCREEN_TRAINING_SIMPLE );
							setScreen( SCREEN_CHOOSE_CHARACTER_SINGLE );
						break;
					}					
				break;
				//</editor-fold>
				
				//#if LOW_JAR == "false"
				//<editor-fold desc="menu de novo campeonato">
				case SCREEN_CHAMPIONSHIP_MENU:
					switch ( index ) {
						case INDEX_SCREEN_CHAMPIONSHIP_BACK:
							setScreen( SCREEN_NEW_GAME_MENU );
						break;
						
						case INDEX_SCREEN_CHAMPIONSHIP_SIMPLE:
						case INDEX_SCREEN_CHAMPIONSHIP_NORMAL:
							PlayScreen.setEasyMode( index == INDEX_SCREEN_CHAMPIONSHIP_SIMPLE );
							setScreen( SCREEN_CHOOSE_CHARACTER_CHAMPIONSHIP );
						break;
					}
				break;
				//</editor-fold>
				
				//<editor-fold desc="menu de campeonato salvo">
				case SCREEN_CHAMPIONSHIP_SAVED:
					switch ( index ) {
						case INDEX_SCREEN_CHAMPIONSHIP_SAVED_BACK:
							setScreen( SCREEN_NEW_GAME_MENU );
						break;
						
						case INDEX_SCREEN_CHAMPIONSHIP_SAVED_NEW:
							setScreen( SCREEN_CHAMPIONSHIP_MENU );
						break;
						
						case INDEX_SCREEN_CHAMPIONSHIP_SAVED_CONTINUE:
							setScreen( SCREEN_CHAMPIONSHIP_LOAD );
						break;
					}					
				break;	
				//</editor-fold>	
				
				//#endif
				
				//<editor-fold desc="menu de recordes">
				case SCREEN_HIGH_SCORES_MENU:
					switch ( index ) {
						case INDEX_SCREEN_HIGH_SCORES_BACK:
							setScreen( SCREEN_MAIN_MENU );
						break;
						
						case INDEX_SCREEN_HIGH_SCORES_BEST_TIMES:
							setScreen( SCREEN_HIGH_SCORES_TIMES );
						break;
						
						case INDEX_SCREEN_HIGH_SCORES_TOP_SPEEDS:
							setScreen( SCREEN_HIGH_SCORES_SPEED );
						break;						
					}
				break;
				//</editor-fold>
				
				//<editor-fold desc="deseja correr novamente?">
				case SCREEN_RACE_AGAIN:
					manager.setBackground( null, false );
					
					switch ( index ) {
						case BasicConfirmScreen.INDEX_NO:
							setScreen( SCREEN_MAIN_MENU );
						break;
						
						case BasicConfirmScreen.INDEX_YES:
							// sorteia novamente a dificuldade do oponente
							playScreen.setOpponentSkillLevel( -1 );
							setScreen( SCREEN_CONTINUE_GAME );
						break;
					}
				break;
				//</editor-fold>
				
				//<editor-fold desc="deseja apagar os recordes?">
				case SCREEN_ERASE_RECORDS:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
							HighScoresScreen.eraseRecords();
						case BasicConfirmScreen.INDEX_NO:
							setScreen( SCREEN_OPTIONS );
						break;
					}
				break;
				//</editor-fold>	
				
			} // fim switch ( id )
		} // fim if ( id == currentScreen )
	} // fim do m�todo onChoose( int, int )

	
	public final void onItemChanged( Menu menu, int id, int index ) {
	}
	
	
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		try {
			if ( textIndex < 0 )
				( ( GameMIDlet ) instance ).manager.setSoftKey( softKey, null );
			else
				( ( GameMIDlet ) instance ).manager.setSoftKey( softKey, new Label( FONT_DEFAULT, getText( textIndex ) ) );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable label ) {
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );
		
		if ( midlet != null && midlet.manager != null )
			midlet.manager.setSoftKey( softKey, label );
	} // fim do m�todo setSoftKesetSoftKeyyLabel( byte, Drawable )	
	
	
	public static final Drawable getCursor() {
		return ( ( GameMIDlet ) instance ).cursor;
	} // fim do m�todo getCursor()
	
	
	public static final void setBackground( byte backgroundType, int bkgColor ) {
		final GameMIDlet midlet = ( GameMIDlet ) getInstance();
		
		switch ( backgroundType ) {
			case BACKGROUND_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_ASPHALT:
				if ( !isLowMemory() ) {
					midlet.manager.setBackgroundColor( -1 );
					midlet.manager.setBackground( midlet.patternMetal, false );
					
					break;
				}
			
			case BACKGROUND_COLOR:
				midlet.manager.setBackgroundColor( bkgColor );
			break;
			
			case BACKGROUND_PLAYSCREEN:
				midlet.manager.setBackground( playScreen, true );
			break;
		}
	}
	
	
	public static final Drawable getPatternFill() {
		return isLowMemory() ? null : ( ( GameMIDlet ) instance ).patternMetal.getFill();
	}

	
	public static final ImageFont getDefaultFont() {
		return FONT_DEFAULT;
	}
	
	
	private static final BasicMenu createBasicMenu( int index ) {
		try {
			int[] entries = null;
			
			boolean title = false;
			byte firstEntryIndex = 1;
			
			switch ( index ) {
				case SCREEN_MAIN_MENU:
					//#if SCREEN_SIZE == "MEDIUM_BIG"
						if ( isLowMemory() || ScreenManager.SCREEN_HEIGHT <= 200 ) {
							entries = new int[] { TEXT_FULL_POWER, TEXT_NEW_GAME, TEXT_OPTIONS, TEXT_RECORDS, TEXT_HELP, TEXT_CREDITS, TEXT_EXIT };
						} else {
							entries = new int[] { TEXT_NEW_GAME, TEXT_OPTIONS, TEXT_RECORDS, TEXT_HELP, TEXT_CREDITS, TEXT_EXIT };
							title = true;
						}					
					//#else
//# 						firstEntryIndex = 0;
						//#if LOW_JAR == "true"
//# 							entries = new int[] { TEXT_NEW_GAME, TEXT_OPTIONS, TEXT_RECORDS, TEXT_HELP, TEXT_CREDITS, TEXT_EXIT };
						//#else
//# 							entries = new int[] { TEXT_NEW_GAME, TEXT_OPTIONS, TEXT_RECORDS, TEXT_HELP, TEXT_CREDITS, TEXT_EXIT };
						//#endif
					//#endif
				break;
				
				case SCREEN_NEW_GAME_MENU:
					//#if LOW_JAR == "true"
//# 						entries = new int[] { TEXT_NEW_GAME, TEXT_SINGLE_RACE, TEXT_TRAINING, TEXT_BACK };
					//#else
						entries = new int[] { TEXT_NEW_GAME, TEXT_SINGLE_RACE, TEXT_TRAINING, TEXT_CHAMPIONSHIP, TEXT_BACK };
					//#endif
				break;
				
				case SCREEN_TRAINING:
					entries = new int[] { TEXT_GEAR_TYPE, TEXT_SIMPLE, TEXT_NORMAL, TEXT_BACK  };
				break;
				
				case SCREEN_SINGLE_RACE_MENU:
					entries = new int[] { TEXT_GEAR_TYPE, TEXT_SIMPLE, TEXT_NORMAL, TEXT_BACK  };
				break;
				
				//#if LOW_JAR == "false"
				case SCREEN_CHAMPIONSHIP_MENU:
					entries = new int[] { TEXT_GEAR_TYPE, TEXT_SIMPLE, TEXT_NORMAL, TEXT_BACK };
				break;
				
				case SCREEN_CHAMPIONSHIP_SAVED:
					//#if SCREEN_SIZE == "MEDIUM_BIG"
						entries = new int[] { TEXT_CHAMPIONSHIP_SAVED, TEXT_CONTINUE, TEXT_NEW, TEXT_BACK };
					//#else
//# 						entries = new int[] { TEXT_CHAMPIONSHIP_SAVED_SHORT, TEXT_CONTINUE, TEXT_NEW, TEXT_BACK };
					//#endif
				break;
				//#endif
				
				case SCREEN_HIGH_SCORES_MENU:
					entries = new int[] { TEXT_RECORDS, TEXT_BEST_TIMES, TEXT_TOP_SPEEDS, TEXT_BACK };
				break;
			}

			BasicMenu menu;
			
			//#if SCREEN_SIZE == "MEDIUM_BIG"		
			if ( title ) {
				menu = new BasicMenu( ( GameMIDlet ) instance, index, FONT_DEFAULT, entries, MENU_ITEMS_SPACING, 1, entries.length, new DrawableImage( PATH_IMAGES + "menu_title.png" ) );
			} else
			//#endif
				menu = new BasicMenu( ( GameMIDlet ) instance, index, FONT_DEFAULT, entries, MENU_ITEMS_SPACING, firstEntryIndex, entries.length - 1 );
			
			if ( firstEntryIndex > 0 ) {
				menu.setSize( menu.getWidth(), menu.getHeight() + MENU_TITLE_SPACING );
				for ( byte i = firstEntryIndex; i < menu.getUsedSlots(); ++i )
					menu.getDrawable( i ).move( 0, MENU_TITLE_SPACING );								
			}
			
			menu.setCursor( getCursor(), Menu.CURSOR_DRAW_AFTER_MENU, Graphics.VCENTER | Graphics.LEFT );
			menu.setCircular( true );
			
			return menu;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return null;
		}
	}
	
	
	/**
	 * M�todo de convers�o de uma unidade em pixels para cent�metros.
	 * 
	 * @param pixels
	 * @return 
	 */
	public static final int convertToCm( long pixels ) {
		return ( int ) ( pixels * KILOMETER / PIXELS_PER_KILOMETER );
	}
	
	
	/**
	 * M�todo de convers�o de uma unidade em cent�metros para pixels .
	 * 
	 * @param centimeters
	 * @return 
	 */
	public static final int convertToPixel( long centimeters ) {
		return ( int ) ( centimeters * PIXELS_PER_KILOMETER / KILOMETER );
	}

	
	//#if LOW_JAR == "false"
	public static synchronized final void playerRaceEnded( boolean playerWon ) {
		championship.playerRaceEnded( playerWon );
		setScreen( SCREEN_CHAMPIONSHIP_CONTINUE );
	}
	//#endif

	
	public final void write( DataOutputStream output ) throws Exception {
	}

	
	public final void read( DataInputStream input ) throws Exception {
		// somente verifica a exist�ncia de jogo salvo
		setSavedGame( input.readBoolean() );
	}
	
	
	public static final void setSavedGame( boolean exists ) {
		hasSavedGame = exists;
	}
	
	
	public static final boolean hasSavedGame() {
		return hasSavedGame;
	}

	
	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		//#if LOW_JAR == "false"
		// caso um campeonato esteja em andamento e jogador n�o esteja na tela de jogo, grava o campeonato
		if ( championship != null && playScreen == null ) {
			championship.writeChampionship( Championship.WRITE_MODE_RECORD );
			championship = null;
		}
		//#endif
		
		super.destroyApp( unconditional );
	}
	
	
	//#if LOW_JAR == "false"
	public static final void startChampionship( int playerIndex ) {
		GameMIDlet.playerIndex = ( byte ) playerIndex;
		setScreen( SCREEN_CHAMPIONSHIP_NEW );
	}
	//#endif
	
	
	/**
	 * Prepara uma corrida.
	 * @param raceType tipo da corrida. Valores poss�veis s�o:
	 * <ul>
	 * <li>SCREEN_RACE_SINGLE</li>
	 * <li>SCREEN_RACE_CHAMPIONSHIP</li>
	 * <li>SCREEN_RACE_TRAINING</li>
	 * </ul>
	 * @param playerIndex �ndice do personagem do jogador. Valores menores ou iguais a zero sorteiam aleatoriamente o
	 * personagem.
	 * @param opponentIndex �ndice do personagem do oponente. Valores menores ou iguais a zero sorteiam aleatoriamente o
	 * personagem.
	 * @param opponentSkillLevel n�vel de habilidade do oponente. Valores v�lidos est�o na faixa 0-100 (inclusive). Para
	 * definir um n�vel de habilidade aleat�rio, basta passar um valor negativo.
	 */
	public static final void prepareRace( byte raceType, int playerIndex, int opponentIndex, int opponentSkillLevel ) {
		if ( playerIndex <= 0 )
			playerIndex = getRandomRacerIndex();
		
		if ( opponentIndex <= 0 || opponentIndex == playerIndex ) {
			do {
				opponentIndex = getRandomRacerIndex();			
			} while ( opponentIndex == playerIndex );
		}
		
		GameMIDlet.playerIndex = ( byte ) playerIndex;
		GameMIDlet.opponentIndex = ( byte ) opponentIndex;
		GameMIDlet.opponentSkillLevel = ( byte ) opponentSkillLevel;
		
		setScreen( raceType );
	}
	
	
	public static final String formatSpeed( int speed ) {
		String s = String.valueOf( speed );
		final int length = s.length();
		final int move = MAX_CHARS_2 - length;
		s.getChars( 0, length, tempChars, 0 );
		
		for ( int i = MAX_CHARS_2; i >= move; --i )
			tempChars[ i ] = tempChars[ i - move ];
		for ( int i = 0; i < move; ++i )
			tempChars[ i ] = '0';
		
		tempChars[ SPEED_DECIMAL_PLACES + 2 ] = tempChars[ SPEED_DECIMAL_PLACES + 1 ];
		tempChars[ SPEED_DECIMAL_PLACES + 1 ] = tempChars[ SPEED_DECIMAL_PLACES ];
		tempChars[ SPEED_DECIMAL_PLACES ] = '.';
		
		return new String( tempChars, 0, MAX_CHARS );
	}
	
	
	public static final String formatTime( int time ) {
		String s = String.valueOf( time );
		final int length = s.length();
		final int move = MAX_CHARS - length - 1;
		s.getChars( 0, length, tempChars, 0 );
		
		for ( int i = MAX_CHARS; i >= move; --i )
			tempChars[ i ] = tempChars[ i - move ];
		for ( int i = 0; i < move; ++i )
			tempChars[ i ] = '0';

		tempChars[ TIME_DECIMAL_PLACES + 3 ] = tempChars[ TIME_DECIMAL_PLACES + 2 ];
		tempChars[ TIME_DECIMAL_PLACES + 2 ] = tempChars[ TIME_DECIMAL_PLACES + 1 ];
		tempChars[ TIME_DECIMAL_PLACES + 1 ] = tempChars[ TIME_DECIMAL_PLACES ];
		tempChars[ TIME_DECIMAL_PLACES ] = '.';		
		
		return new String( tempChars, 0, MAX_CHARS );
	}
	
	
	public static final byte getRandomRacerIndex() {
		return ( byte ) ( 1 + ( NanoMath.randInt( CHARACTERS_CHOOSABLE ) ) );
	}
	
	
	public static final void load( LoadScreen load ) throws Exception {
		DashBoard.load( load );
		Parachutes.loadFrameSet( load );
		RearTires.loadFrameSet( load );
		Engine.loadFrameSets( load );
		
		Racer.loadRacers( load );
		Car.loadFrameSet( load );
		
		//#if LOW_JAR == "false"
		Championship.createInstance( load, -2 );
		//#else
//# 		load.changeProgress( 20 );
		//#endif
	}
	
	
 	public static final boolean isLowMemory() {
 		return ( ( GameMIDlet ) instance ).lowMemory;
 	}
	
	
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "1.0";

        return "<ALN_H>VERS�O " + version + "\n\n\n";        
    }	
	
	
	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	
	private static interface LoadListener {
		public void load( final Loader loadScreen ) throws Exception;
	}
	
	
	private static final class Loader extends Label implements Updatable {

		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 250;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;
		
		private static final byte MAX_DOTS_MODULE = MAX_DOTS -1 ;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;
		
		private final byte previousScreen;
		
		private boolean active = true;


		/**
		 * 
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private Loader( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );
			
			previousScreen = currentScreen;
			
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
			
			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo s�o carregados aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa, para evitar poss�veis atrasos em transi��es e
					// garantir que novos recursos s� ser�o carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );
						
						final Loader loadScreen = this;
						
						loadThread = new Thread() {
							public final void run() {
								try {
									System.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
									//#else
//# 										// volta � tela anterior
//# 										setScreen( previousScreen );
									//#endif
								}
							} // fim do m�todo run()
						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING_SHORT );
					for ( byte i = 0; i < dots; ++i )
						temp += '.';

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}					
				}
			}
		} // fim do m�todo update( int )
		
		
		public final void paint( Graphics g ) {
			super.paint( g );
			
			painted = true;
		}
		
		
		private final void setActive( boolean a ) {
			active = a;
		}

	} // fim da classe interna LoadScreen	
	
}
