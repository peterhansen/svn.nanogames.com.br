/*
 * LoadScreen.java
 *
 * Created on August 21, 2007, 4:47 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class LoadScreen extends DrawableGroup implements Constants, ScreenListener {
	
	private static final byte TOTAL_ITEMS = 4;
	
	private final RichLabel label;
	private final Pattern patternFill;
	private final Pattern patternTop;
	private final Pattern patternBottom;
	
	private byte progress;
	
	
	/** Creates a new instance of LoadScreen */
	public LoadScreen( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		label = new RichLabel( font, GameMIDlet.getText( TEXT_LOADING ) + "0%", size.x, null );
		label.setSize( label.getWidth(), label.getTextTotalHeight() );
		label.defineReferencePixel( 0, label.getHeight() >> 1 );
		label.setRefPixelPosition( 0, size.y >> 1 );
		
		// insere os patterns
		DrawableImage img = new DrawableImage( PATH_IMAGES + "pattern_top.png" );
		patternTop = new Pattern( img );
		patternTop.setSize( size.x, img.getHeight() );
		patternTop.defineReferencePixel( 0, patternTop.getHeight() );
		insertDrawable( patternTop );
		
		img = new DrawableImage( PATH_IMAGES + "pattern_bottom.png" );
		patternBottom = new Pattern( img );
		patternBottom.setSize( size.x, img.getHeight() );		
		insertDrawable( patternBottom );
		
		final Drawable fill = GameMIDlet.getPatternFill();
		if ( fill == null ) {
			patternFill = new Pattern( null );
			patternFill.setFillColor( COLOR_BACKGROUND );
		} else {
			patternFill = new Pattern( fill );
		}
		
		patternFill.setSize( size.x, label.getHeight() );
		patternFill.setPosition( 0, label.getPosition().y );
		insertDrawable( patternFill );
		
		patternTop.setRefPixelPosition( 0, patternFill.getPosition().y );
		patternBottom.setPosition( 0, patternFill.getPosition().y + patternFill.getHeight() );
		
		insertDrawable( label );
		
		changeProgress( 0 );
		
		final LoadScreen instance = this;
		final TimerTask task = new TimerTask() {
			
			public final void run() {
				try {
					GameMIDlet.load( instance );
					GameMIDlet.setScreen( SCREEN_MAIN_MENU );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
					
					GameMIDlet.exit();
				}
			}
		};
		final Timer timer = new Timer();
		timer.schedule( task, 200 );
	}
	
	
	public final void changeProgress( int percent ) {
		progress += percent;
		
		if ( progress < 0 )
			progress = 0;
		else if ( progress >= 100 )
			progress = 100;
		
		label.setText( GameMIDlet.getText( TEXT_LOADING ) + progress + "%" );
		
		setVisible( true );
	}
	
	
	public final void hideNotify() {
	}

	
	public final void showNotify() {
		patternFill.setSize( size );
		patternFill.setPosition( 0, 0 );
		
		patternTop.setVisible( false );
		patternBottom.setVisible( false );
		
		setVisible( true );
	}

	
	protected final void paint( Graphics g ) {
		super.paint( g );
		setVisible( false );

		try {
			Thread.sleep( 300 );
		} catch ( Exception e ) {
		}
	}

	
	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
		
		label.setRefPixelPosition( 0, size.y >> 1 );
		
		showNotify();
	}
	
}
