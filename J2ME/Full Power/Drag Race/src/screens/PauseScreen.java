/*
 * PauseScreen.java
 *
 * Created on June 4, 2007, 5:00 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class PauseScreen extends Menu implements Constants {
	
	//#ifdef NO_SOUND
//# 		public static final byte INDEX_PAUSE		= 0;
//# 		public static final byte INDEX_CONTINUE		= 1;
//# 		public static final byte INDEX_VIBRATION	= 2;
//# 		public static final byte INDEX_MENU			= 3;
//# 		public static final byte INDEX_EXIT			= 4;
//# 
//# 		private static final byte TOTAL_LABELS		= ( byte ) ( MediaPlayer.isVibrationSupported() ? 5 : 4 );	
	//#else
		public static final byte INDEX_PAUSE		= 0;
		public static final byte INDEX_CONTINUE		= 1;
		public static final byte INDEX_SOUND		= 2;
		public static final byte INDEX_VIBRATION	= 3;
		public static final byte INDEX_MENU			= 4;
		public static final byte INDEX_EXIT			= 5;

		private static final byte TOTAL_LABELS		= ( byte ) ( MediaPlayer.isVibrationSupported() ? 6 : 5 );
	//#endif
	
	private static final byte TOTAL_ITEMS		= ( byte ) ( TOTAL_LABELS + 2 );
	
	private final Label[] labels = new Label[ TOTAL_LABELS ];
	
	private final ImageFont font;
	private boolean textsLoaded;
	
	// intervalo m�nimo entre mudan�as de op��o no som, para evitar quebras em alguns aparelhos Samsung
	private final int MIN_TIME_INTERVAL = 500;
	private int timeSinceLastChange;
	
	
	
	/** Creates a new instance of OptionsScreen */
	public PauseScreen( MenuListener listener, ImageFont font, int id ) throws Exception {
		super( listener, id, TOTAL_ITEMS );
		
		this.font = font;
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		loadTexts();
		
		final Drawable pauseCursor = GameMIDlet.getCursor();
		setCursor( pauseCursor, Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );		
		
		setCircular( true );
		setCurrentIndex( 1 );
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				if ( timeSinceLastChange >= MIN_TIME_INTERVAL )
					GameMIDlet.setScreen( SCREEN_CONTINUE_GAME );
			return;

			case ScreenManager.KEY_SOFT_LEFT:
				key = ScreenManager.KEY_NUM5;
			break;

			case ScreenManager.FIRE:
				key = ScreenManager.KEY_NUM5;
			break;
			
			case ScreenManager.RIGHT:
				key = ScreenManager.KEY_NUM6;
			break;
			
			case ScreenManager.LEFT:
				key = ScreenManager.KEY_NUM4;
			break;
		} // fim switch ( key )
		
		switch ( key ) {
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				switch ( currentIndex ) {
					//#ifndef NO_SOUND
						case INDEX_SOUND:
							if ( timeSinceLastChange >= MIN_TIME_INTERVAL ) {
								timeSinceLastChange = 0;
								toggleSound();
							}
						return;
					//#endif
					
					case INDEX_VIBRATION:
						if ( MediaPlayer.isVibrationSupported() ) {
							toggleVibration();
							return;
						}
						
					case INDEX_CONTINUE:
						if ( timeSinceLastChange < MIN_TIME_INTERVAL )
							return;
				}
			
			default:
				super.keyPressed( key );				
		} // fim switch ( key )					
	}
	
	
	//#ifndef NO_SOUND
		private final void toggleSound() {
			MediaPlayer.setMute( !MediaPlayer.isMuted() );

			loadTexts();
		}
	//#endif
	
	
	private final void toggleVibration() {
		MediaPlayer.setVibration( !MediaPlayer.isVibration() );
		
		MediaPlayer.vibrate( VIBRATION_TIME_CAR_WRECKED );
		
		loadTexts();
	}
	
	
	private final void loadTexts() {
		try {
			String[] texts;
			//#ifdef NO_SOUND
//# 				if ( MediaPlayer.isVibrationSupported() ) {
//# 					texts = new String[] { GameMIDlet.getText( TEXT_PAUSE ),
//# 										   GameMIDlet.getText( TEXT_CONTINUE ),
//# 										   GameMIDlet.getText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON ),
//# 										   GameMIDlet.getText( TEXT_MAIN_MENU ),
//# 										   GameMIDlet.getText( TEXT_EXIT ),
//# 										};
//# 				} else {
//# 					texts = new String[] { GameMIDlet.getText( TEXT_PAUSE ),
//# 										   GameMIDlet.getText( TEXT_CONTINUE ),
//# 										   GameMIDlet.getText( TEXT_MAIN_MENU ),
//# 										   GameMIDlet.getText( TEXT_EXIT ),
//# 										 };				
//# 				}			
			//#else
				if ( MediaPlayer.isVibrationSupported() ) {
					texts = new String[] { GameMIDlet.getText( TEXT_PAUSE ),
										   GameMIDlet.getText( TEXT_CONTINUE ),
										   GameMIDlet.getText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF ),
										   GameMIDlet.getText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON ),
										   GameMIDlet.getText( TEXT_MAIN_MENU ),
										   GameMIDlet.getText( TEXT_EXIT ),
										};
				} else {
					texts = new String[] { GameMIDlet.getText( TEXT_PAUSE ),
										   GameMIDlet.getText( TEXT_CONTINUE ),
										   GameMIDlet.getText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF ),
										   GameMIDlet.getText( TEXT_MAIN_MENU ),
										   GameMIDlet.getText( TEXT_EXIT ),
										 };				
				}
			//#endif

			if ( !textsLoaded ) {
				int y = 0;
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					final Label l = new Label( font, texts[ i ] );

					labels[ i ] = l;
					l.defineReferencePixel( l.getWidth() >> 1, 0 );
					l.setRefPixelPosition( size.x >> 1, y );
					insertDrawable( l );

					y += l.getHeight() + MENU_ITEMS_SPACING;
					
					//#if SCREEN_SIZE == "MEDIUM_BIG"
						if ( i == 0 )
							y += MENU_ITEMS_SPACING;
					//#else
//# 						if ( i == 0 )
//# 							y += l.getHeight();
					//#endif
				}
				
				final int yOffset = ( size.y - y ) >> 1;
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					getDrawable( i ).move( 0, yOffset );
				}
				
				textsLoaded = true;
			} else {
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					final Label l = ( Label ) getDrawable( i );

					l.setText( texts[ i ] );

					l.defineReferencePixel( l.getWidth() >> 1, 0 );
					l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
				}
				
				//#ifndef LG_C1100
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
				//#endif
				
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_CONTINUE );							
			}
			
			// reposiciona o cursor (largura do texto pode mudar)
			updateCursorPosition();			
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}

	
	public final void setCurrentIndex( int index ) {
		final int previousIndex = currentIndex;
		
		super.setCurrentIndex( index );
		
		if ( currentIndex <= 1 ) {
			if ( previousIndex < 1 || previousIndex == activeDrawables - 1 )
				super.setCurrentIndex( 1 );
			else if ( previousIndex == 1 && currentIndex < 1 ) {
				super.setCurrentIndex( circular ? activeDrawables - 1 : 1 );
			}
		}
	}

	
	
	public final void update( int delta ) {
		super.update( delta );
		
		timeSinceLastChange += delta;
	}
	
	
}
