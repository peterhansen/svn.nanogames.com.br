/*
 * HighScoresScreen.java
 *
 * Created on October 3, 2007, 11:47 AM
 *
 */

package screens;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 *
 * @author peter
 */
public final class HighScoresScreen extends Menu implements Serializable, Constants {
	
	private static final byte TOTAL_SCORES = 5;
	
	private static final byte TOTAL_ITEMS = TOTAL_SCORES + 1;
	
	private static final int[] speeds = new int[ TOTAL_SCORES ];
	private static final int[] times = new int[ TOTAL_SCORES ];
	
	private final Label title;
	private final Label[] scoreLabels = new Label[ TOTAL_SCORES ];
	
	private static HighScoresScreen instance;
	
	private byte lastTopSpeedIndex = -1;
	private byte lastBestTimeIndex = -1;
	
	private final int BLINK_RATE = 388;
	
	private int accTime;
	
	private byte type;
	
	
	/** Creates a new instance of HighScoresScreen */
	private HighScoresScreen( ImageFont font ) throws Exception {
		super( null, 0, TOTAL_ITEMS );
		
		int yLabel = MENU_ITEMS_SPACING;
		
		title = new Label( font, null );
		insertDrawable( title );
		
		yLabel += title.getHeight() + ( MENU_ITEMS_SPACING << 1 );
		
		for ( int i = 0; i < scoreLabels.length; ++i ) {
			scoreLabels[ i ] = new Label( font, null );
			final Label label = scoreLabels[ i ];
			
			label.defineReferencePixel( label.getWidth() >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, yLabel );			
			
			yLabel += label.getHeight() + MENU_ITEMS_SPACING;
			
			insertDrawable( scoreLabels[ i ] );
		}
		
		setSize( ScreenManager.SCREEN_WIDTH, yLabel );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
	}
	
	
	/**
	 * Cria uma nova inst�ncia da tela de recordes, ou apenas uma refer�ncia para ela, caso j� tenha sido criada anteriormente.
	 *
	 * @param screen �ndice do tipo de recordes a serem exibidos: SCREEN_HIGH_SCORES_TIMES ou SCREEN_HIGH_SCORES_SCORES.
	 * @throws java.lang.Exception caso haja problemas ao alocar recursos.
	 * @return uma refer�ncia para a inst�ncia da tela de recordes.
	 */
	public static final HighScoresScreen createInstance( int screen ) throws Exception {
		if ( instance == null ) {
			instance = new HighScoresScreen( GameMIDlet.getDefaultFont() );
			
			loadRecords();
		} // fim if ( instance == null )
		
		instance.type = ( byte ) screen;
		switch ( screen ) {
			case SCREEN_HIGH_SCORES_TIMES:
			case SCREEN_HIGH_SCORES_SPEED:
				instance.updateLabels();
			break;
		}		
		
		return instance;
	}
	
	
	public static final boolean setTime( int time ) {
		for ( int i = 0; i < times.length; ++i ) {
			if ( time < times[ i ] ) {
				// �ltimo tempo � menor que um tempo anteriormente gravado
				for ( int j = times.length - 1; j > i; --j ) {
					times[ j ] = times[ j - 1 ];
				}
				
				instance.lastBestTimeIndex = ( byte ) i;
				instance.accTime = 0;
				
				times[ i ] = time;
				try {
					instance.type = SCREEN_HIGH_SCORES_TIMES;
					GameMIDlet.saveData( DATABASE_NAME, DATABASE_INDEX_TIMES, instance );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
				}
				
				return true;
			} // fim if ( time > times[ i ] )
		} // fim for ( int i = 0; i < times.length; ++i )
		
		return false;		
	}
	
	
	public static final boolean setSpeed( int speed ) {
		for ( int i = 0; i < speeds.length; ++i ) {
			if ( speed > speeds[ i ] ) {
				// �ltima pontua��o � maior que uma pontua��o anteriormente gravada
				for ( int j = speeds.length - 1; j > i; --j ) {
					speeds[ j ] = speeds[ j - 1 ];
				}
				
				instance.lastTopSpeedIndex = ( byte ) i;
				instance.accTime = 0;
				
				speeds[ i ] = speed;
				try {
					instance.type = SCREEN_HIGH_SCORES_SPEED;
					GameMIDlet.saveData( DATABASE_NAME, DATABASE_INDEX_SPEEDS, instance );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
				}
				
				return true;
			} // fim if ( speed > speeds[ i ] )
		} // fim for ( int i = 0; i < speeds.length; ++i )
		
		return false;		
	} // fim do m�todo setSpeed( int )
	
	
	public static final int isTopSpeed( int speed ) {
		for ( int i = 0; i < speeds.length; ++i ) {
			if ( speed > speeds[ i ] )
				return i;
		}
		
		return -1;		
	}
	
	
	public static final int isBestTime( int time ) {
		for ( int i = 0; i < times.length; ++i ) {
			if ( time < times[ i ] )
				return i;
		}
		
		return -1;		
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				stopBlink();

				GameMIDlet.setScreen( SCREEN_HIGH_SCORES_MENU );
			break;
			
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				stopBlink();

				GameMIDlet.setScreen( SCREEN_HIGH_SCORES_MENU );
			break;
		} // fim switch ( key )
	}

	
	public final void write( DataOutputStream output ) throws Exception {
		int[] values;
		
		switch ( type ) {
			case SCREEN_HIGH_SCORES_TIMES:
				values = times;
			break;
			
			case SCREEN_HIGH_SCORES_SPEED:
				values = speeds;
			break;
			
			default:
				return;
		} // fim switch ( type )
		
		for ( int i = 0; i < values.length; ++i )
			output.writeInt( values[ i ] );		
	}

	
	public final void read( DataInputStream input ) throws Exception {
		int[] values;
		
		switch ( type ) {
			case SCREEN_HIGH_SCORES_TIMES:
				values = times;
			break;
			
			case SCREEN_HIGH_SCORES_SPEED:
				values = speeds;
			break;
			
			default:
				return;
		} // fim switch ( type )
		
		for ( int i = 0; i < values.length; ++i )
			 values[ i ] = input.readInt();		
	}
	
	
	private final void updateLabels() {
		int textIndex;
		switch ( type ) {
			case SCREEN_HIGH_SCORES_TIMES:
				textIndex = TEXT_BEST_TIMES;
			break;
			
			case SCREEN_HIGH_SCORES_SPEED:
				textIndex = TEXT_TOP_SPEEDS;
			break;
			
			default:
				return;
		} // fim switch ( type )		
		
		title.setText( GameMIDlet.getText( textIndex ) );
		title.defineReferencePixel( title.getWidth() >> 1, 0 );
		title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, title.getRefPixelY() );		
		
		for ( int i = 0; i < scoreLabels.length; ++i ) {
			final Label label = scoreLabels[ i ];
			
			switch ( type ) {
				case SCREEN_HIGH_SCORES_TIMES:
					label.setText( GameMIDlet.formatTime( times[ i ] ) + GameMIDlet.getText( TEXT_SECONDS ) );
				break;

				case SCREEN_HIGH_SCORES_SPEED:
					label.setText( GameMIDlet.formatSpeed( speeds[ i ] ) + GameMIDlet.getText( TEXT_KMH ) );
				break;
			} // fim switch ( type )					
			
			label.defineReferencePixel( label.getWidth() >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, label.getRefPixelY() );
		} // fim for ( int i = 0; i < scoreLabels.length; ++i )
	} // fim do m�todo updateLabels()

	
	public final void update( int delta ) {
		super.update( delta );
		
		int index;
		switch ( type ) {
			case SCREEN_HIGH_SCORES_TIMES:
				index = lastBestTimeIndex;
			break;
			
			case SCREEN_HIGH_SCORES_SPEED:
				index = lastTopSpeedIndex;
			break;
			
			default:
				return;
		}
		
		if ( index >= 0 ) {
			accTime += delta;

			if ( accTime >= BLINK_RATE ) {
				accTime %= BLINK_RATE;
				scoreLabels[ index ].setVisible( !scoreLabels[ index ].isVisible() );
			}
		}						
	}
	
	
	private final void stopBlink() {
		switch ( type ) {
			case SCREEN_HIGH_SCORES_TIMES:
				if ( lastBestTimeIndex >= 0 )
					scoreLabels[ lastBestTimeIndex ].setVisible( true );
				
				lastBestTimeIndex = -1;				
			break;
			
			case SCREEN_HIGH_SCORES_SPEED:
				if ( lastTopSpeedIndex >= 0 )
					scoreLabels[ lastTopSpeedIndex ].setVisible( true );
				
				lastTopSpeedIndex = -1;				
			break;			
		}
	}

	
	public static final void eraseRecords() {
		try {
			GameMIDlet.eraseSlot( DATABASE_NAME, DATABASE_INDEX_SPEEDS );
			GameMIDlet.eraseSlot( DATABASE_NAME, DATABASE_INDEX_TIMES );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
		
		loadRecords();
	}
	
	
	private static final void loadRecords() {
		try {
			instance.type = SCREEN_HIGH_SCORES_TIMES;
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_INDEX_TIMES, instance );

			instance.type = SCREEN_HIGH_SCORES_SPEED;
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_INDEX_SPEEDS, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif

			// preenche a tabela de pontos e tempos com valores "dummy"
			for ( int i = 0; i < TOTAL_SCORES; ++i ) {
				speeds[ i ] = 200 * KILOMETER + ( ( TOTAL_SCORES - i ) * 35 * KILOMETER );
				times[ i ] = ( i + 1 ) * 8192;
			}

			try {
				instance.type = SCREEN_HIGH_SCORES_SPEED;
				GameMIDlet.saveData( DATABASE_NAME, DATABASE_INDEX_SPEEDS, instance );

				instance.type = SCREEN_HIGH_SCORES_TIMES;
				GameMIDlet.saveData( DATABASE_NAME, DATABASE_INDEX_TIMES, instance );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif					
			}
		}
	}
	
}
