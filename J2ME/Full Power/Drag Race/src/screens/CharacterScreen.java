/*
 * CharacterScreen.java
 *
 * Created on October 16, 2007, 8:31 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.Racer;

/**
 *
 * @author peter
 */
public final class CharacterScreen extends UpdatableGroup implements KeyListener, Constants
//#if SCREEN_SIZE == "MEDIUM_BIG"		
		, PointerListener 
//#endif
{
	
	private static final byte TOTAL_ITEMS = 5;
	
	// n�mero de personagens por linha
	private final byte CHARACTERS_PER_ROW = 4;
	private final byte CHARACTERS_PER_LINE = CHARACTERS_CHOOSABLE / CHARACTERS_PER_ROW;
	
	private final DrawableGroup characters;
	
	private final Label title;
	private final Label name;
	
	private byte indexPlayer;
	private byte indexOpponent;
	
	private final DrawableImage cursorPlayer;
	private final DrawableImage cursorOpponent;
	
	private DrawableImage currentCursor;
	
	private final boolean singleRace;
	
	private final byte STATE_CHOOSE_PLAYER		= 0;
	private final byte STATE_CHOOSE_OPPONENT	= 1;
	
	private byte state;
	
	private static final short BLINK_RATE = 300;
	private int accTime;
	
	
	/** Creates a new instance of CharacterScreen */
	public CharacterScreen( boolean singleRace ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.singleRace = singleRace;
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
		final String alignTag = RichLabel.TAG_START + RichLabel.TAG_ALIGN + RichLabel.ALIGN_HCENTER + RichLabel.TAG_END;
		title = new RichLabel( GameMIDlet.getDefaultFont(), alignTag + GameMIDlet.getText( TEXT_CHOOSE_CHARACTER ), 
								size.x * MESSAGE_TEXT_MAX_WIDTH_PERCENT / 100, null );
		title.setSize( title.getWidth(), ( ( RichLabel ) title ).getTextTotalHeight() );
		//#else
//# 		title = new MarqueeLabel( GameMIDlet.getDefaultFont(), GameMIDlet.getText( TEXT_CHOOSE_CHARACTER ), 
//# 									MarqueeLabel.DEFAULT_TEXT_SPEED, MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
//# 		title.setSize( size.x, title.getHeight() );
		//#endif
		
		insertDrawable( title );
		
		// adiciona-se 2 ao n�mero de itens do grupo devido aos cursores
		characters = new DrawableGroup( CHARACTERS_CHOOSABLE + 2 );
		final Point faceSize = Racer.getFaceAt( 0 ).getSize();
		faceSize.addEquals( CHARACTER_SCREEN_FACE_SPACING, CHARACTER_SCREEN_FACE_SPACING );
		
		for ( int i = 0; i < CHARACTERS_CHOOSABLE; ++i ) {
			// utiliza-se (+ 1) pois h� um personagem n�o-selecion�vel
			final DrawableImage face = Racer.getFaceAt( i + 1 );
			face.defineReferencePixel( face.getWidth() >> 1, face.getHeight() >> 1 );
			face.setPosition( CHARACTER_SCREEN_FACE_SPACING + faceSize.x * ( i % CHARACTERS_PER_ROW ), 
							  CHARACTER_SCREEN_FACE_SPACING + faceSize.y * ( i / CHARACTERS_PER_ROW ) );
			
			characters.insertDrawable( face );
		}
		characters.setSize( CHARACTER_SCREEN_FACE_SPACING + faceSize.x * CHARACTERS_PER_ROW, 
							CHARACTER_SCREEN_FACE_SPACING + faceSize.y * CHARACTERS_PER_LINE );
		insertDrawable( characters );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
		characters.defineReferencePixel( characters.getWidth() >> 1, characters.getHeight() >> 1 );
		characters.setRefPixelPosition( size.x >> 1, size.y >> 1 );		
		title.defineReferencePixel( title.getWidth() >> 1, title.getHeight() >> 1 );
		title.setRefPixelPosition( size.x >> 1, characters.getPosition().y >> 1 );
		
		if ( size.y > SCREEN_HEIGHT_MIN ) {
			name = new RichLabel( GameMIDlet.getDefaultFont(), null, 0, null );
			name.setSize( ScreenManager.SCREEN_WIDTH * 9 / 10, GameMIDlet.getDefaultFont().getImage().getHeight() << 1 );
			name.defineReferencePixel( name.getWidth() >> 1, 0 );
			name.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, characters.getPosition().y + characters.getHeight() + MENU_ITEMS_SPACING );					 
		} else {
			name = new MarqueeLabel( GameMIDlet.getDefaultFont(), null );
	 		name.setSize( size.x, name.getHeight() );
			name.setPosition( 0, characters.getPosition().y + characters.getHeight() + MENU_ITEMS_SPACING );					
		}
		
		//#else
//# 		name = new MarqueeLabel( GameMIDlet.getDefaultFont(), null );
//# 		name.setSize( size.x, name.getHeight() );
//# 		
//# 		if ( size.y > SCREEN_HEIGHT_MIN ) {
//# 			characters.defineReferencePixel( characters.getWidth() >> 1, characters.getHeight() >> 1 );
//# 			characters.setRefPixelPosition( size.x >> 1, size.y >> 1 );		
//# 			title.defineReferencePixel( title.getWidth() >> 1, title.getHeight() >> 1 );
//# 			title.setRefPixelPosition( size.x >> 1, characters.getPosition().y >> 1 );			
//# 			name.setPosition( 0, characters.getPosition().y + characters.getHeight() + MENU_ITEMS_SPACING );
//# 		} else {
//# 			title.defineReferencePixel( title.getWidth() >> 1, 0 );
//# 			title.setRefPixelPosition( size.x >> 1, 0 );
//# 			
//# 			characters.defineReferencePixel( characters.getWidth() >> 1, 0 );
//# 			characters.setRefPixelPosition( size.x >> 1, title.getPosition().y + title.getHeight() + CHARACTERS_SCREEN_ITEMS_OFFSET );
//# 			name.setPosition( 0, characters.getPosition().y + characters.getHeight() + CHARACTERS_SCREEN_ITEMS_OFFSET );
//# 		}
		//#endif
		
		insertDrawable( name );
		
		cursorPlayer = new DrawableImage( PATH_CHARACTERS + "cursor.png" );
		cursorPlayer.defineReferencePixel( cursorPlayer.getWidth() >> 1, cursorPlayer.getHeight() >> 1 );
		characters.insertDrawable( cursorPlayer );
		
		// no modo corrida simples, o jogador tamb�m escolhe seu oponente
		if ( singleRace ) {
			cursorOpponent = new DrawableImage( PATH_CHARACTERS + "cursor2.png" );
			cursorOpponent.defineReferencePixel( cursorOpponent.getWidth() >> 1, cursorOpponent.getHeight() >> 1 );
			characters.insertDrawable( cursorOpponent );
		} else {
			cursorOpponent = null;
		}

		setState( STATE_CHOOSE_PLAYER );
		
		setIndex( GameMIDlet.getRandomRacerIndex() );
	}

	
	public final void keyPressed( int key ) {
		int index;
		if ( !singleRace || state == STATE_CHOOSE_PLAYER )
			index = indexPlayer;
		else
			index = indexOpponent;
		
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				if ( singleRace ) {
					if ( state == STATE_CHOOSE_PLAYER )
						GameMIDlet.setScreen( SCREEN_SINGLE_RACE_MENU );
					else
						setState( STATE_CHOOSE_PLAYER );
				//#if LOW_JAR == "false"
				} else {
					GameMIDlet.setScreen( SCREEN_CHAMPIONSHIP_MENU );
				}
					//#else
//# 				}
				//#endif
			return;
			
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				if ( singleRace ) {
					if ( state == STATE_CHOOSE_PLAYER )
						setState( STATE_CHOOSE_OPPONENT );
					else
						GameMIDlet.prepareRace( SCREEN_RACE_SINGLE, indexPlayer + 1, indexOpponent + 1, -1 );
				//#if LOW_JAR == "false"
				} else {
					GameMIDlet.startChampionship( index + 1 );
				}
				//#else
//# 				}
				//#endif
			break;
			
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				if ( checkIndex( index - CHARACTERS_PER_LINE ) )
					setIndex( index - CHARACTERS_PER_LINE );
				else
					setIndex( index - ( CHARACTERS_PER_LINE << 1 ) );
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				if ( checkIndex( index + CHARACTERS_PER_LINE ) )
					setIndex( index + CHARACTERS_PER_LINE );
				else
					setIndex( index + ( CHARACTERS_PER_LINE << 1 ) );
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				if ( checkIndex( index - 1 ) )
					setIndex( index - 1 );
				else
					setIndex( index - 2 );
			break;
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				if ( checkIndex( index + 1 ) )
					setIndex( index + 1 );
				else
					setIndex( index + 2 );				
			break;
		}

	} // fim do m�todo keyPressed( int )

	
	public final void keyReleased( int key ) {
	}
	
	
	private final boolean checkIndex( int index ) {
		index = ( CHARACTERS_CHOOSABLE + index ) % CHARACTERS_CHOOSABLE;
		
		return !singleRace || state != STATE_CHOOSE_OPPONENT || index != indexPlayer;
	}
	
	
	private final void setIndex( int index ) {
		index = ( CHARACTERS_CHOOSABLE + index ) % CHARACTERS_CHOOSABLE;
		if ( state == STATE_CHOOSE_PLAYER )
			indexPlayer = ( byte ) index;
		else
			indexOpponent = ( byte ) index;

		//#if SCREEN_SIZE == "MEDIUM_BIG"
		if ( size.y > SCREEN_HEIGHT_MIN ) {
			final String alignTag = RichLabel.TAG_START + RichLabel.TAG_ALIGN + RichLabel.ALIGN_HCENTER + RichLabel.TAG_END;
			name.setText( alignTag + Racer.getNameAt( index + 1 ) );
		} else {
			name.setText( Racer.getNameAt( index + 1 ), false );
		}
		//#else
//# 		name.setText( Racer.getNameAt( index + 1 ), false );
		//#endif
		
		currentCursor.setRefPixelPosition( characters.getDrawable( index ).getRefPixelPosition() );
	}

	
	private final void setState( byte state ) {
		if ( singleRace ) {
			this.state = state;
			
			//#if SCREEN_SIZE == "MEDIUM_BIG"
			final String alignTag = RichLabel.TAG_START + RichLabel.TAG_ALIGN + RichLabel.ALIGN_HCENTER + RichLabel.TAG_END;
			//#endif
			
			switch ( state ) {
				case STATE_CHOOSE_PLAYER:
					//#if SCREEN_SIZE == "MEDIUM_BIG"
					title.setText( alignTag + GameMIDlet.getText( TEXT_CHOOSE_CHARACTER ) );
					//#else
//# 					title.setText( GameMIDlet.getText( TEXT_CHOOSE_CHARACTER ), false );
					//#endif
					currentCursor = cursorPlayer;
					
					cursorOpponent.setVisible( false );
					
					setIndex( indexPlayer );
				break;

				case STATE_CHOOSE_OPPONENT:
					//#if SCREEN_SIZE == "MEDIUM_BIG"
					title.setText( alignTag + GameMIDlet.getText( TEXT_CHOOSE_OPPONENT ) );
					//#else
//# 					title.setText( GameMIDlet.getText( TEXT_CHOOSE_OPPONENT ), false );
					//#endif
					
					currentCursor = cursorOpponent;
					cursorOpponent.setVisible( true );
					cursorPlayer.setVisible( true );
					
					do {
						indexOpponent = ( byte ) NanoMath.randInt( CHARACTERS_CHOOSABLE );
					} while ( indexOpponent == indexPlayer );
					
					setIndex( indexOpponent );
				break;
			}
		} else {
			currentCursor = cursorPlayer;
		}
	}

	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( currentCursor != null ) {
			accTime -= delta;
			
			if ( accTime <= 0 ) {
				accTime += BLINK_RATE;
				currentCursor.setVisible( !currentCursor.isVisible() );
			}
		}
	}


	//#if SCREEN_SIZE == "MEDIUM_BIG"
	
	public final void onPointerDragged( int x, int y ) {
		final byte pointerIndex = getCharacterAt( x, y );
		
		if ( pointerIndex >= 0 )
			setIndex( pointerIndex );
	}


	public final void onPointerPressed( int x, int y ) {
		final byte pointerIndex = getCharacterAt( x, y );
		
		if ( pointerIndex >= 0 ) {
			if ( ( state == STATE_CHOOSE_PLAYER && pointerIndex == indexPlayer ) || ( state == STATE_CHOOSE_OPPONENT && pointerIndex == indexOpponent ) ) {
				// jogador confirmou a sele��o atual
				keyPressed( ScreenManager.FIRE );
			} else {
				// jogador apenas marcou um personagem
				setIndex( pointerIndex );
			}
		}
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	
	//#endif
	
	
	/**
	 * Obt�m o �ndice do personagem presente na posi��o x, y da tela.
	 * 
	 * @param x posi��o x do ponteiro.
	 * @param y posi��o y do ponteiro.
	 * @return �ndice do personagem escolhido, ou -1 caso o ponteiro n�o esteja sobre nenhum personagem.
	 */
	private final byte getCharacterAt( int x, int y ) {
		x -= characters.getPosition().x;
		y -= characters.getPosition().y;
		
		for ( byte i = 0; i < CHARACTERS_CHOOSABLE; ++i ) {
			final Drawable character = characters.getDrawable( i );
			if ( character.contains( x, y ) )
				return i;
		}
		
		return -1;
	}
	
}
