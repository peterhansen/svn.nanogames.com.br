/*
 * Championship.java
 *
 * Created on October 9, 2007, 2:56 PM
 *
 */

//#if LOW_JAR == "false"

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.Message;
import core.Racer;
import core.RacerAI;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * @author peter
 */
public final class Championship extends UpdatableGroup implements Constants, KeyListener, Serializable
//#if SCREEN_SIZE == "MEDIUM_BIG"
		, PointerListener
//#endif

{
	
	private static final byte TOTAL_ITEMS = 30;
	
	private int keyHeld;
	
	// n�mero total de chaves
	private final byte TOTAL_KEYS = 15;
	
	private static final byte STAGE_EIGHTH_FINALS	= 0;
	private static final byte STAGE_QUARTER_FINALS	= 1;
	private static final byte STAGE_SEMI_FINALS		= 2;
	private static final byte STAGE_FINAL			= 3;
	private static final byte STAGE_FINISHED		= 4;
	
	private static final byte STAGE_TOTAL = 4;
	
	/** est�gio atual do campeonato */
	private byte currentStage;
	
	/** n�mero de corridas restantes no est�gio atual */
	private byte racesLeftOnCurrentStage = CHAMPIONSHIP_RACES_PER_LEVEL;
	
	private static final byte[] KEYS_PER_STAGE = { 8, 4, 2, 1 };
	
	private static final byte[] PLAYOFFS_KEYS_SPACING = { 2, -1, 16, 36 };
	
	private final Championship.Key[] keys = new Championship.Key[ TOTAL_KEYS ];
	
	private final Message message;
	
	private Championship.Key currentPlayerKey;
	private byte playerIndex;
	
	/** velocidade atual de movimenta��o da tela */
	private final Point moveSpeed = new Point();
	private final Point moveModule = new Point();
	private final Point moveDt = new Point();
	
	private final ImageFont font;
	
	private final Point limitMin = new Point();
	private final Point limitMax = new Point();

	public static final byte WRITE_MODE_RECORD = 0;
	public static final byte WRITE_MODE_ERASE	= 1;
	
	private byte writeMode = WRITE_MODE_RECORD;
	
	private final DrawableImage cursor;
	
	// poss�veis �ndices do jogador numa chave
	private static final byte INDEX_PLAYER_NOT_IN_KEY	= -1;
	private static final byte INDEX_PLAYER_FIRST_RACER	= 0;
	private static final byte INDEX_PLAYER_SECOND_RACER	= 1;
	
	private static Championship instance;
	
	private boolean active;
	
	/** imagens utilizadas para montar as chaves que indicam os cruzamentos */
	private static final byte KEY_IMAGES_TOTAL	= 4;
	
	private static final byte KEY_IMAGE_TOP		= 0;
	private static final byte KEY_IMAGE_FILL	= 1;
	private static final byte KEY_IMAGE_MIDDLE	= 2;
	private static final byte KEY_IMAGE_BOTTOM	= 3;
	private final DrawableImage[] keyImages = new DrawableImage[ KEY_IMAGES_TOTAL ];
	
	
	//#if SCREEN_SIZE == "MEDIUM_BIG"
	private final Point lastPointerPos = new Point();
	//#endif
	
	
	/**
	 * Cria uma nova inst�ncia de um campeonato.
	 * @throws java.lang.Exception 
	 */
	private Championship( LoadScreen load ) throws Exception {
		super( TOTAL_ITEMS );
		
		font = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_table.png", PATH_IMAGES + "font_table.dat" );
		load.changeProgress( 2 );
		
		Pattern pattern = null;
		Drawable fill = GameMIDlet.getPatternFill(); 
		if ( fill != null ) {
			pattern= new Pattern( fill );
			insertDrawable( pattern );
		}
		
		load.changeProgress( 1 );
		
		final String[] texts = new String[] {	GameMIDlet.getText( TEXT_EIGHTH_FINALS ),
												GameMIDlet.getText( TEXT_QUARTER_FINALS ),
												GameMIDlet.getText( TEXT_SEMI_FINALS ),
												GameMIDlet.getText( TEXT_FINAL ),
												};
		
		
		for ( int i = 0; i < keyImages.length; ++i ) {
			keyImages[ i ] = new DrawableImage( PATH_IMAGES + "key_" + i + ".png" );
			load.changeProgress( 1 );
		}
		
		// aloca e insere no grupo as chaves; a defini��o das posi��es e corredores de cada chave � feita a seguir
		for ( int i = 0; i < keys.length; ++i ) {
			keys[ i ] = new Championship.Key( this, font );
			insertDrawable( keys[ i ] );
			load.changeProgress( 1 );
		}
		
		int startY = PLAYOFFS_KEYS_INITIAL_POS;
		int currentYDist = keys[ 0 ].getHeight() + 1;
		int PLAYOFFS_KEY_DISTANCE_X = 0;		
		
		for ( int x = PLAYOFFS_KEYS_INITIAL_POS, stage = STAGE_EIGHTH_FINALS, k = 0; stage < STAGE_TOTAL; x += PLAYOFFS_KEY_DISTANCE_X, ++stage ) {
			if ( x > size.x )
				size.x = x;
			
			final Label label = new Label( GameMIDlet.getDefaultFont(), texts[ stage ] );
			label.setPosition( x, startY );
			insertDrawable( label );
			
			for ( int y = startY + label.getHeight(), keyIndex = 0; keyIndex < KEYS_PER_STAGE[ stage ]; y += currentYDist, ++keyIndex, ++k ) {
				if ( y > size.y )
					size.y = y;
				
				if ( keyIndex > 0 && ( k & 1 ) == 0 )
					y += PLAYOFFS_KEYS_SPACING[ stage ];
				
				final Championship.Key key = keys[ k ];
				key.setPosition( x, y );
				
				// insere as setas indicando o cruzamento das chaves
				if ( stage < STAGE_FINAL ) {
					if ( ( k & 1 ) == 0 ) {
						final Drawable imgKey = createKeyImage( stage );
						imgKey.setPosition( key.getPosition().x + key.getWidth(), key.getPosition().y + ( key.getHeight() >> 1 ) - 1 );

						if ( PLAYOFFS_KEY_DISTANCE_X == 0 )
							PLAYOFFS_KEY_DISTANCE_X = key.getWidth() + imgKey.getWidth();

						insertDrawable( imgKey );
					}
				} // fim if ( stage < STAGE_FINAL )				
			} // fim for ( int y = startY + label.getHeight(), ...; ... )
			startY += ( currentYDist >> 1 );
			currentYDist = ( currentYDist << 1 ) + PLAYOFFS_KEYS_SPACING[ stage ];
		} // fim for ( int x = PLAYOFFS_KEYS_INITIAL_X, stage = STAGE_EIGHTH_FINALS, k = 0; ...; ... )
		
		size.addEquals( keys[ 0 ].getWidth() + PLAYOFFS_KEYS_INITIAL_POS, keys[ 0 ].getHeight() + PLAYOFFS_KEYS_INITIAL_POS );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			if ( !GameMIDlet.isLowMemory() ) {
				final DrawableImage logo = new DrawableImage( PATH_IMAGES + "menu_title.png" );
				logo.defineReferencePixel( logo.getWidth() + 3, -3 );
				logo.setRefPixelPosition( size.x, 0 );

				insertDrawable( logo );
				load.changeProgress( 1 );
			}
		//#else
//# 				final DrawableImage logo = new DrawableImage( PATH_IMAGES + "title.png" );
//# 				logo.defineReferencePixel( logo.getWidth() + 3, -3 );
//# 				logo.setRefPixelPosition( size.x, 0 );
//# 
//# 				insertDrawable( logo );
//# 				load.changeProgress( 1 );
		//#endif
		
		load.changeProgress( 2 );
		
		setSize( NanoMath.max( size.x, ScreenManager.SCREEN_WIDTH ), NanoMath.max( size.y, ScreenManager.SCREEN_HEIGHT ) );
		
		limitMin.set( -size.x + ScreenManager.SCREEN_WIDTH, -size.y + ScreenManager.SCREEN_HEIGHT );
		
		if ( pattern != null )
			pattern.setSize( getSize() );
		
		cursor = new DrawableImage( PATH_IMAGES + "key_player.png" );
		insertDrawable( cursor );
		
		load.changeProgress( 1 );
		
		message = PlayScreen.getMessage();
		insertDrawable( message );
		
		load.changeProgress( 2 );
	}
	
	
	/**
	 * Cria uma nova inst�ncia de um campeonato, ou retorna a refer�ncia para um j� criado.
	 * @param load 
	 * @param index �ndice do corredor do jogador. Caso seja Racer.DUMMY_RACER_INDEX, tenta carregar um jogo anteriormente gravado.
	 * Caso seja -2, somente carrega as imagens utilizadas na tela de campeonato.
	 * @return 
	 * @throws java.lang.Exception
	 */	
	public static final Championship createInstance( LoadScreen load, int index ) throws Exception {
		if ( instance == null ) {
			try {
				instance = new Championship( load );
			} catch ( Exception e ) {
				instance = null;
				
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
				
				throw e;
			}
		}
		
		resetKeys();
		
		boolean newGame = true;
		switch ( index ) {
			case -2:
			break;
			
			case Racer.DUMMY_RACER_INDEX:
				// tenta carregar um jogo salvo anteriormente
				try {
					GameMIDlet.loadData( DATABASE_NAME, DATABASE_INDEX_SAVED_GAME, instance );

					// atualiza os rostos dos personagens at� a etapa atual
					byte endKey = 0;
					for ( int s = STAGE_EIGHTH_FINALS; s < instance.currentStage; ++s )
						endKey += KEYS_PER_STAGE[ s ];

					for ( byte key = 0; key < endKey; ++key ) {
						instance.keys[ key ].updateFaces();
					}

					newGame = false;
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif

					// apaga jogo salvo anteriormente
					instance.writeChampionship( WRITE_MODE_ERASE );
				}
			default:
				final Racer player = new Racer( index, false );

				if ( newGame ) {
					// sorteia os corredores

					final int[] characters = new int[ CHARACTERS_CHOOSABLE ];
					byte remaining = CHARACTERS_CHOOSABLE;
					for ( int i = 0; i < remaining; ++i ) {
						// soma 1 pois o primeiro personagem � "dummy"
						characters[ i ] = i + 1;
					}

					for ( int i = 0; i < KEYS_PER_STAGE[ STAGE_EIGHTH_FINALS ]; ++i ) {
						int randomIndex = NanoMath.randInt( remaining );
						final int racerIndex1 = characters[ randomIndex ];

						// remove o personagem sorteado da lista dos dispon�veis
						for ( int j = randomIndex; j < remaining - 1; ++j )
							characters[ j ] = characters[ j + 1 ];
						--remaining;

						int racerIndex2;
						do {
							randomIndex = NanoMath.randInt( remaining );
							racerIndex2 = characters[ randomIndex ];
						} while ( racerIndex1 == racerIndex2 );

						// remove o personagem sorteado da lista dos dispon�veis
						for ( int j = randomIndex; j < remaining - 1; ++j )
							characters[ j ] = characters[ j + 1 ];
						--remaining;

						// cria a chave, verificando se o jogador est� nela
						if ( racerIndex1 == index ) {
							instance.keys[ i ].setRacers( player, new RacerAI( racerIndex2, false ) );
						} else if ( racerIndex2 == index ) {
							instance.keys[ i ].setRacers( new RacerAI( racerIndex1, false ), player );
						} else {
							instance.keys[ i ].setRacers( new RacerAI( racerIndex1, false ), new RacerAI( racerIndex2, false ) );
						}
					} // fim for ( int i = 0; i < KEYS_PER_STAGE[ STAGE_EIGHTH_FINALS ]; ++i )
					
					// armazena o �ndice do jogador, para que o personagem do jogador possa ser identificado ao se carregar um jogo salvo.
					// No caso de carregar jogo salvo, o �ndice do jogador � lido do RMS.
					instance.playerIndex = ( byte ) index;
					instance.racesLeftOnCurrentStage = CHAMPIONSHIP_RACES_PER_LEVEL;
					instance.currentStage = STAGE_EIGHTH_FINALS;
				} // fim if ( newGame )

				instance.setCameraOnPlayer();				
			break;
		}
		
		return instance;
	}
	

	public final void keyPressed( int key ) {
		if ( active ) {
			if ( message.isVisible() ) {
				message.skip();
				return;
			}

			switch ( key ) {
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.FIRE:
				case ScreenManager.KEY_NUM5:
					if ( currentStage < STAGE_FINISHED )
						playRound();
					else
						backToMenu();
				break;

				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					backToMenu();
				break;

				case ScreenManager.UP:
				case ScreenManager.KEY_NUM2:
					keyHeld = ScreenManager.KEY_NUM2;
				break;

				case ScreenManager.DOWN:
				case ScreenManager.KEY_NUM8:
					keyHeld = ScreenManager.KEY_NUM8;
				break;

				case ScreenManager.LEFT:
				case ScreenManager.KEY_NUM4:
					keyHeld = ScreenManager.KEY_NUM4;
				break;

				case ScreenManager.RIGHT:
				case ScreenManager.KEY_NUM6:
					keyHeld = ScreenManager.KEY_NUM6;
				break;

				case ScreenManager.KEY_NUM1:
				case ScreenManager.KEY_NUM3:
				case ScreenManager.KEY_NUM7:
				case ScreenManager.KEY_NUM9:
					keyHeld = key;
				break;			
			} // fim switch ( key )
		} // fim if ( active )
	}

	
	public final void keyReleased( int key ) {
		keyHeld = 0;
		stop( true, true );
	}

	
	public final void update( int delta ) {
		if ( active ) {
			super.update( delta );

			switch ( keyHeld ) {
				case ScreenManager.KEY_NUM2:
					moveSpeed.set( 0, CHAMPIONSHIP_TABLE_MOVE_SPEED );
				break;

				case ScreenManager.KEY_NUM4:
					moveSpeed.set( CHAMPIONSHIP_TABLE_MOVE_SPEED, 0 );
				break;

				case ScreenManager.KEY_NUM6:
					moveSpeed.set( -CHAMPIONSHIP_TABLE_MOVE_SPEED, 0 );
				break;

				case ScreenManager.KEY_NUM8:
					moveSpeed.set( 0, -CHAMPIONSHIP_TABLE_MOVE_SPEED );
				break;

				case ScreenManager.KEY_NUM1:
					moveSpeed.set( CHAMPIONSHIP_TABLE_MOVE_SPEED, CHAMPIONSHIP_TABLE_MOVE_SPEED );
				break;

				case ScreenManager.KEY_NUM3:
					moveSpeed.set( -CHAMPIONSHIP_TABLE_MOVE_SPEED, CHAMPIONSHIP_TABLE_MOVE_SPEED );
				break;

				case ScreenManager.KEY_NUM7:
					moveSpeed.set( CHAMPIONSHIP_TABLE_MOVE_SPEED, -CHAMPIONSHIP_TABLE_MOVE_SPEED );
				break;

				case ScreenManager.KEY_NUM9:
					moveSpeed.set( -CHAMPIONSHIP_TABLE_MOVE_SPEED, -CHAMPIONSHIP_TABLE_MOVE_SPEED );
				break;			
			} // fim switch ( keyHeld )

			int temp;
			if ( moveSpeed.x != 0 ) {
				temp = moveModule.x + ( moveSpeed.x * delta );
				moveDt.x = temp / 1000;
				moveModule.x = temp % 1000;			
			}

			if ( moveSpeed.y != 0 ) {
				temp = moveModule.y + ( moveSpeed.y * delta );
				moveDt.y = temp / 1000;
				moveModule.y = temp % 1000;		
			}

			if ( moveDt.x != 0 || moveDt.y != 0 )
				move( moveDt );
		} // fim if ( active )
	} // fim do m�todo update( int )
	
	
	private synchronized final void playRound() {
		if ( active ) {
			// obt�m a primeira chave que deve ser jogada ou simulada
			byte startKey = 0;
			for ( int i = STAGE_EIGHTH_FINALS; i < currentStage; ++i )
				startKey += KEYS_PER_STAGE[ i ];

			final byte endKey = ( byte ) ( startKey + KEYS_PER_STAGE[ currentStage ] );

			currentPlayerKey = null;
			for ( byte key = startKey; key < endKey; ++key ) {
				// tenta simular a corrida
				if ( !keys[ key ].simulate() ) {
					// corrida n�o pode ser simulada pois um dos corredores � o jogador; armazena seu �ndice para que seja
					// iniciada uma corrida
					currentPlayerKey = keys[ key ];
				}
			}

			if ( currentPlayerKey != null ) {
				// jogador corre; atualiza��o das vari�veis de controle do n�mero de corridas e fase do campeonato � feita
				// ao voltar da corrida
				setActive( false );
				GameMIDlet.prepareRace( SCREEN_RACE_CHAMPIONSHIP, playerIndex, currentPlayerKey.getOpponentIndex(), calculateOpponentSkillLevel() );
			} else {
				racesCompleted();
			}
		}
	}
	
	
	private final byte calculateOpponentSkillLevel() {
		final byte SKILL_CHANGE_PER_LEVEL = 100 / STAGE_FINISHED;
		final byte SKILL_CHANGE_PER_RACE = SKILL_CHANGE_PER_LEVEL / CHAMPIONSHIP_RACES_PER_LEVEL;
		
		return ( byte ) ( currentStage * SKILL_CHANGE_PER_LEVEL + ( CHAMPIONSHIP_RACES_PER_LEVEL - racesLeftOnCurrentStage ) * SKILL_CHANGE_PER_RACE );
	}
	
	
	/**
	 * Atualiza o contador de corridas realizadas, e passa para o pr�ximo est�gio caso o est�gio atual do campeonato tenha 
	 * se encerrado.
	 * @return boolean indicando se a rodada terminou.
	 */
	private final boolean racesCompleted() {
		--racesLeftOnCurrentStage;
		
		if ( racesLeftOnCurrentStage <= 0 ) {
			setStage( currentStage + 1 );
			return true;
		}		
		return false;
	}
	
	
	public synchronized final void playerRaceEnded( boolean playerWon ) {
		setActive( true );

		currentPlayerKey.updatePlayerKey( playerWon );

		String top = null;
		String bottom = null;
		if ( racesCompleted() ) {
			if ( currentPlayerKey.getWinner().getIndex() == playerIndex ) {
				// jogador passou � pr�xima fase
				switch ( currentStage ) {
					case STAGE_FINISHED:
						top = GameMIDlet.getText( TEXT_CHAMPION_TOP );
						bottom = GameMIDlet.getText( TEXT_CONGRATULATIONS );

						MediaPlayer.play( SOUND_CHAMPIONSHIP_WON, 1 );
					break;

					default:
						top = GameMIDlet.getText( TEXT_NEXT_STAGE );
						bottom = "\n" + currentPlayerKey.getResultString();
				}
			} else {
				// armazena uma chave tempor�ria, pois currentPlayerKey pode ser anulado dentro de playRound
				final Key key = currentPlayerKey;

				// jogador foi eliminado do campeonato; simula as corridas restantes
				while ( currentStage != STAGE_FINISHED ) {
					playRound();
				}

				top = GameMIDlet.getText( TEXT_ELIMINATED );
				bottom = "\n" + key.getResultString();

				MediaPlayer.play( SOUND_CHAMPIONSHIP_LOST, 1 );
			}
		} else {
			// grava o estado atual do campeonato
			writeChampionship( WRITE_MODE_RECORD );
		}
		stop( true, true );

		// posiciona c�mera no jogador
		setCameraOnPlayer();

		if ( top != null && bottom != null )
			showMessage( top, bottom );
	} // fim do m�todo playerRaceEnded( boolean )
	
	
	private static final class Key extends UpdatableGroup implements Constants, Serializable {
		
		private static final byte INDEX_FIRST_RACER_NAME	= 1;
		private static final byte INDEX_FIRST_RACER_SCORE	= 2;
		private static final byte INDEX_SECOND_RACER_NAME	= 3;
		private static final byte INDEX_SECOND_RACER_SCORE	= 4;
		
		private static final byte TOTAL_ITEMS = 10;
		
		/** resultado da chave (n�mero de vit�rias de cada piloto) */
		private final byte[] result = new byte[ 2 ];
		
		private Racer firstRacer;
		private Racer secondRacer;
		
		private final DrawableGroup firstRacerFaceGroup;
		private final DrawableGroup secondRacerFaceGroup;
		
		private final Championship parent;
		
		
		public Key( Championship parent, ImageFont font ) throws Exception {
			super( TOTAL_ITEMS );
			
			this.parent = parent;
			
			final DrawableImage box = new DrawableImage( PATH_IMAGES + "box_players.png" );
			setSize( box.getSize() );
			
			defineReferencePixel( size.x >> 1, size.y >> 1 );
			
			// adiciona a caixa que armazena os labels
			insertDrawable( box );
			
			// adiciona as informa��es do primeiro corredor
			Label label = new MarqueeLabel( font, firstRacer == null ? Racer.getDummyName() : firstRacer.getName() );
			label.setPosition( CHAMPIONSHIP_BOX_RACER_NAME_X, CHAMPIONSHIP_BOX_FIRST_RACER_LABEL_Y );
			label.setSize( CHAMPIONSHIP_BOX_RACER_NAME_WIDTH, CHAMPIONSHIP_BOX_RACER_LABEL_HEIGHT );
			insertDrawable( label );
			
			label = new Label( font, null );
			label.setPosition( CHAMPIONSHIP_BOX_RACER_SCORE_X, CHAMPIONSHIP_BOX_FIRST_RACER_LABEL_Y );
			label.setSize( CHAMPIONSHIP_BOX_RACER_SCORE_WIDTH, CHAMPIONSHIP_BOX_RACER_LABEL_HEIGHT );
			insertDrawable( label );
			
			// adiciona as informa��es do segundo corredor
			label = new MarqueeLabel( font, secondRacer == null ? Racer.getDummyName() : secondRacer.getName() );
			label.setPosition( CHAMPIONSHIP_BOX_RACER_NAME_X, CHAMPIONSHIP_BOX_SECOND_RACER_LABEL_Y );
			label.setSize( CHAMPIONSHIP_BOX_RACER_NAME_WIDTH, CHAMPIONSHIP_BOX_RACER_LABEL_HEIGHT );
			insertDrawable( label );
			
			label = new Label( font, null );
			label.setPosition( CHAMPIONSHIP_BOX_RACER_SCORE_X, CHAMPIONSHIP_BOX_SECOND_RACER_LABEL_Y );
			label.setSize( CHAMPIONSHIP_BOX_RACER_SCORE_WIDTH, CHAMPIONSHIP_BOX_RACER_LABEL_HEIGHT );
			insertDrawable( label );
			
			firstRacerFaceGroup = new DrawableGroup( 1 );
			firstRacerFaceGroup.setPosition( CHAMPIONSHIP_BOX_RACER_FACE_X, CHAMPIONSHIP_BOX_FIRST_RACER_FACE_Y );
			insertDrawable( firstRacerFaceGroup );			
			
			// insere os rostos dos personagens
			DrawableImage face = firstRacer == null ? Racer.getDummyFace() : firstRacer.getFace();
			firstRacerFaceGroup.insertDrawable( face );			
			
			
			secondRacerFaceGroup = new DrawableGroup( 1 );
			insertDrawable( secondRacerFaceGroup );			
			
			face = secondRacer == null ? Racer.getDummyFace() : secondRacer.getFace();
			secondRacerFaceGroup.setPosition( CHAMPIONSHIP_BOX_RACER_FACE_X, CHAMPIONSHIP_BOX_SECOND_RACER_FACE_Y );
			secondRacerFaceGroup.insertDrawable( face );			

			setRacers( firstRacer, secondRacer );
		}
		

		private final void updateLabels() {
			// atualiza o resultado
			Label label = ( Label ) getDrawable( INDEX_FIRST_RACER_SCORE );
			label.setText( String.valueOf( result[ 0 ] ) );
			label = ( Label ) getDrawable( INDEX_SECOND_RACER_SCORE );
			label.setText( String.valueOf( result[ 1 ] ) );

			// atualiza os corredores
			MarqueeLabel marquee = ( MarqueeLabel ) getDrawable( INDEX_FIRST_RACER_NAME );
			DrawableImage face;
			if ( firstRacer != null ) {
				marquee.setText( firstRacer.getName(), false );
				marquee.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
				marquee.setVisible( true );
				
				face = firstRacer.getFace();
				firstRacerFaceGroup.setDrawable( face, 0 );
			} else {
				marquee.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
				marquee.setVisible( false );
				
				face = Racer.getDummyFace();
				firstRacerFaceGroup.setDrawable( face, 0 );
			}
			firstRacerFaceGroup.setSize( face.getSize() );
			
			marquee = ( MarqueeLabel ) getDrawable( INDEX_SECOND_RACER_NAME );
			if ( secondRacer != null ) {
				marquee.setText( secondRacer.getName(), false );
				marquee.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
				marquee.setVisible( true );
				
				face = secondRacer.getFace();
				secondRacerFaceGroup.setDrawable( face, 0 );		
			} else {
				marquee.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
				marquee.setVisible( false );
				
				face = Racer.getDummyFace();
				secondRacerFaceGroup.setDrawable( face, 0 );						
			}
			secondRacerFaceGroup.setSize( face.getSize() );
		}
		

		/**
		 * Tenta simular a corrida - no caso de um dos corredores ser o jogador, n�o pode ser simulada.
		 * @return boolean indicando se a corrida foi simulada ou n�o.
		 */
		private final boolean simulate() {
			if ( ( firstRacer instanceof RacerAI ) && ( secondRacer instanceof RacerAI ) ) {
				if ( NanoMath.randInt( 10000 ) < 5000 ) {
					++result[ 0 ];
				}
				else
					++result[ 1 ];

				updateLabels();

				return true;
			}
			
			// um dos corredores � o jogador
			return false;
		} // fim do m�todo simulate()
		
		
		private final void updatePlayerKey( boolean playerWon ) {
			// explica��o da sujeira: o �ndice a ser atualizado � um XOR dos testes booleanos: jogador venceu e jogador
			// est� na primeira posi��o. Caso ambos sejam o mesmo valor, incrementa-se o n�mero de vit�rias do primeiro
			// corredor; caso contr�rio, do segundo.
			++result[ playerWon ^ ( firstRacer instanceof RacerAI ) ? 0 : 1 ];
			
			// desaloca o carro do oponente (n�o precisa desalocar o do jogador, j� que vai ser usado em seguida)
			if ( firstRacer instanceof RacerAI )
				firstRacer.unloadCar();
			else
				secondRacer.unloadCar();
			
			updateLabels();
		}
		

		/**
		 * Remove o rosto do vencedor, que avan�a � pr�xima fase.
		 */
		private final void updateFaces() {
			if ( result[ 0 ] < result[ 1 ] )
				secondRacerFaceGroup.removeDrawable( 0 );
			else
				firstRacerFaceGroup.removeDrawable( 0 );
		}
		
		
		private final Racer getWinner() {
			return result[ 0 ] > result[ 1 ] ? firstRacer : secondRacer;
		}
		
		
		private final void setRacers( Racer firstRacer, Racer secondRacer ) {
			this.firstRacer = firstRacer;
			this.secondRacer = secondRacer;
			
			updateLabels();
		}

		
		public final void write( DataOutputStream output ) throws Exception {
			output.writeByte( result[ 0 ] );
			output.writeByte( result[ 1 ] );
			output.writeByte( firstRacer == null ? Racer.DUMMY_RACER_INDEX : firstRacer.getIndex() );
			output.writeByte( secondRacer == null ? Racer.DUMMY_RACER_INDEX : secondRacer.getIndex() );
		}

		
		public final void read( DataInputStream input ) throws Exception {
			result[ 0 ] = input.readByte();
			result[ 1 ] = input.readByte();

			// caso o �ndice do corredor seja Racer.DUMMY_RACER_INDEX, significa que ainda n�o se sabe quais s�o os corredores
			// da chave (ou seja, s�o chaves de etapas ainda n�o disputadas). Caso sejam valores v�lidos, cria um corredor
			// com o mesmo �ndice (levando em considera��o se � corredor da cpu ou jogador) e o adiciona � chave.
			byte racerIndex = input.readByte();
			if ( racerIndex != Racer.DUMMY_RACER_INDEX ) {
				if ( racerIndex == parent.playerIndex )
					firstRacer = new Racer( racerIndex, false );
				else
					firstRacer = new RacerAI( racerIndex, false );
			}
			
			racerIndex = input.readByte();
			if ( racerIndex != Racer.DUMMY_RACER_INDEX ) {
				if ( racerIndex == parent.playerIndex )
					secondRacer = new Racer( racerIndex, false );
				else
					secondRacer = new RacerAI( racerIndex, false );
			}
			
			updateLabels();
		}

		
		/**
		 * Obt�m o �ndice do jogador na chave.
		 * @return INDEX_PLAYER_NOT_IN_KEY, caso o jogador n�o esteja na chave, 
		 * INDEX_PLAYER_FIRST_RACER caso seja o primeiro corredor e INDEX_PLAYER_SECOND_RACER caso seja o segundo.
		 */
		private final byte getPlayerIndexInKey() {
			if ( firstRacer != null && firstRacer.getIndex() == parent.playerIndex )
				return INDEX_PLAYER_FIRST_RACER;
			
			if ( secondRacer != null && secondRacer.getIndex() == parent.playerIndex )
				return INDEX_PLAYER_SECOND_RACER;
			
			return INDEX_PLAYER_NOT_IN_KEY;
		}
		
		
		private final byte getOpponentIndex() {
			if ( firstRacer == null || secondRacer == null )
				return -1;
			
			if ( firstRacer.getIndex() == parent.playerIndex )
				return secondRacer.getIndex();
			
			return firstRacer.getIndex();
		}

		
		private final void reset() {
			result[ 0 ] = 0;
			result[ 1 ] = 0;
			
			setRacers( null, null );
		}

		
		private final String getResultString() {
			return String.valueOf( result[ 0 ] + GameMIDlet.getText( TEXT_VERSUS ) + result[ 1 ] );
		}
		
	} // fim da classe interna Key
	
	
	private final void stop( boolean x, boolean y ) {
		if ( x ) {
			moveSpeed.x = 0;
			moveModule.x = 0;
			moveDt.x = 0;		
		}
		
		if ( y ) {
			moveSpeed.y = 0;
			moveModule.y = 0;
			moveDt.y = 0;		
		}		
	}

	
	/**
	 * Prepara as chaves para um est�gio do campeonato.
	 * @param stage est�gio do campeonato.
	 */
	private final void setStage( int stage ) {
		switch ( stage ) {
			case STAGE_EIGHTH_FINALS:
			break;
			
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
			case STAGE_FINAL:
				// obt�m a primeira chave que deve ser jogada ou simulada
				byte startKey = 0;
				for ( int i = STAGE_EIGHTH_FINALS; i < currentStage; ++i )
					startKey += KEYS_PER_STAGE[ i ];

				final byte currentStageKeys = KEYS_PER_STAGE[ currentStage ];
				final byte endKey = ( byte ) ( startKey + currentStageKeys );

				// define as chaves da pr�xima etapa a partir dos vencedores das chaves atuais
				Racer temp = null;
				for ( byte key = startKey; key < endKey; ++key ) {
					keys[ key ].updateFaces();
					
					if ( key % 2 == 0 )
						temp = keys[ key ].getWinner();
					else 
						keys[ startKey + ( ( key - startKey ) >> 1 ) + currentStageKeys ].setRacers( temp, keys[ key ].getWinner() );
				}				
			break;
			
			case STAGE_FINISHED:
			break;

			default:
				return;
		}
		
		currentStage = ( byte ) stage;
		racesLeftOnCurrentStage = CHAMPIONSHIP_RACES_PER_LEVEL;
	}


	public final void setPosition( int x, int y ) {
		if ( x < limitMin.x ) {
			x = limitMin.x;
			stop( true, false );
		} else if ( x > limitMax.x ) {
			x = limitMax.x;
			stop( true, false );
		}
		
		if ( y < limitMin.y ) {
			y = limitMin.y;
			stop( false, true );
		} else if ( y > limitMax.y ) {
			y = limitMax.y;		
			stop( false, true );
		}
		
		super.setPosition( x, y );
	}

	
	/**
	 * Volta ao menu principal, ou � tela de recordes, caso haja um novo recorde e o campeonato j� esteja encerrado. Caso o
	 * jogador ainda esteja no campeonato, grava os dados no RMS para continua��o posterior.
	 */
	private final void backToMenu() {
		setActive( false );
		
		switch ( currentStage ) {
			case STAGE_FINISHED:
				// apaga campeonato gravado anteriormente
				writeChampionship( WRITE_MODE_ERASE );
				
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
			
			default:
				// grava o estado do campeonato e volta ao menu
				writeChampionship( WRITE_MODE_RECORD );
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			// fim default
		}
		GameMIDlet.setSavedGame( writeMode == WRITE_MODE_RECORD );
	}

	
	public final void write( DataOutputStream output ) throws Exception {
		switch ( writeMode ) {
			case WRITE_MODE_RECORD:
				output.writeByte( playerIndex );
				output.writeBoolean( PlayScreen.isEasyMode() );
				output.writeByte( currentStage );
				output.writeByte( racesLeftOnCurrentStage );

				for ( int i = 0; i < TOTAL_KEYS; ++i )
					keys[ i ].write( output );				
			break;
			
			case WRITE_MODE_ERASE:
				output.writeByte( Racer.DUMMY_RACER_INDEX );
			break;
		}

	}

	
	public final void read( DataInputStream input ) throws Exception {
		playerIndex = input.readByte();
		if ( playerIndex != Racer.DUMMY_RACER_INDEX ) {
			PlayScreen.setEasyMode( input.readBoolean() );
			currentStage = input.readByte();
			racesLeftOnCurrentStage = input.readByte();

			for ( int i = 0; i < TOTAL_KEYS; ++i )
				keys[ i ].read( input );
		} else {
			//#if DEBUG == "true"
				throw new Exception( "erro ao carregar jogo salvo" );
			//#else
//# 				throw new Exception();
			//#endif
		}
	}
	
	
	private final void setCameraOnPlayer() {
		for ( int i = TOTAL_KEYS - 1; i >= 0; --i ) {
			final byte indexInKey = keys[ i ].getPlayerIndexInKey();
			
			switch ( indexInKey ) {
				case INDEX_PLAYER_FIRST_RACER:
				case INDEX_PLAYER_SECOND_RACER:
					final Point pos = keys[ i ].getRefPixelPosition();
					setPosition( ScreenManager.SCREEN_HALF_WIDTH - pos.x, ScreenManager.SCREEN_HALF_HEIGHT - pos.y );

					cursor.setPosition( keys[ i ].getPosition().x + CHAMPIONSHIP_BOX_PLAYER_OFFSET_X, keys[ i ].getPosition().y +
							( indexInKey == INDEX_PLAYER_FIRST_RACER ? CHAMPIONSHIP_BOX_PLAYER_FIRST_OFFSET_Y : CHAMPIONSHIP_BOX_PLAYER_SECOND_OFFSET_Y ) );
				return;
			}
		}		
	}
	
	
	public final void writeChampionship( byte mode ) {
		switch ( mode ) {
			case WRITE_MODE_RECORD:
				switch ( currentStage ) {
					case STAGE_FINISHED:
						mode = WRITE_MODE_ERASE;
					break;
				}
			break;
		}
		
		writeMode = mode;
		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_INDEX_SAVED_GAME, this );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}		
	}
	
	
	private final void showMessage( String textTop, String textBottom ) {
		stop( true, true );
		message.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH - position.x, ScreenManager.SCREEN_HALF_HEIGHT - position.y );
		message.setText( textTop, textBottom );
	}
	
	
	/**
	 * Apaga as informa��es de todas as chaves do campeonato.
	 */
	public static final void resetKeys() {
		for ( int i = 0; i < instance.keys.length; ++i )
			instance.keys[ i ].reset();				
		
		instance.currentPlayerKey = null;
	}
	
	
	private final DrawableGroup createKeyImage( int stage ) throws Exception {
		final DrawableGroup key = new DrawableGroup( KEY_IMAGES_TOTAL );
		
		switch ( stage ) {
			case STAGE_EIGHTH_FINALS:
				key.setSize( CHAMPIONSHIP_KEY_WIDTH, CHAMPIONSHIP_KEY_SMALL_HEIGHT );
			break;
			
			case STAGE_QUARTER_FINALS:
				key.setSize( CHAMPIONSHIP_KEY_WIDTH, CHAMPIONSHIP_KEY_MEDIUM_HEIGHT );
			break;
			
			case STAGE_SEMI_FINALS:
				key.setSize( CHAMPIONSHIP_KEY_WIDTH, CHAMPIONSHIP_KEY_BIG_HEIGHT );
			break;			
		}

		DrawableImage img = new DrawableImage( keyImages[ KEY_IMAGE_TOP ] );
		key.insertDrawable( img );
		
		final Pattern fill = new Pattern( keyImages[ KEY_IMAGE_FILL ] );
		fill.setSize( keyImages[ KEY_IMAGE_FILL ].getWidth(), key.getHeight() - ( img.getHeight() << 1 ) );
		fill.setPosition( img.getWidth() - fill.getWidth(), img.getHeight() );
		
		key.insertDrawable( fill );
		
		final DrawableImage middle = new DrawableImage( keyImages[ KEY_IMAGE_MIDDLE ] );
		middle.defineReferencePixel( middle.getWidth(), middle.getHeight() >> 1 );
		middle.setRefPixelPosition( key.getWidth(), key.getHeight() >> 1 );
		
		key.insertDrawable( middle );
		
		img = new DrawableImage( keyImages[ KEY_IMAGE_BOTTOM ] );
		img.setPosition( 0, key.getHeight() - img.getHeight() );
		key.insertDrawable( img );
		
		return key;
	}
	
	
	public static final void setActive( boolean active ) {
		instance.setVisible( active );
		instance.active = active;
	}
	
	
	//#if SCREEN_SIZE == "MEDIUM_BIG"

	public final void onPointerDragged( int x, int y ) {
		if ( message.isVisible() ) {
			return;
		}
		
		move( x - lastPointerPos.x, y - lastPointerPos.y );
		lastPointerPos.set( x, y );
	}

	
	public final void onPointerPressed( int x, int y ) {
		if ( message.isVisible() ) {
			message.skip();
			return;
		}
		
		lastPointerPos.set( x, y );
	}

	
	public final void onPointerReleased( int x, int y ) {
	}

	//#endif
}

//#endif