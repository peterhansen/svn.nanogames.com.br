/*
 * DashBoard.java
 *
 * Created on October 4, 2007, 4:45 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
import screens.LoadScreen;

/**
 * Painel de informa��es do jogador, mostrado na parte inferior da tela.
 * @author peter
 */
public final class DashBoard extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 10;
	
	private static DrawableImage board;
	
	private final Racer player;
	
	private static Pattern minimapTop;
	private static Pattern minimapBottom;
	
	private static Label labelSpeed;
	private static Label labelTime;
	
	private final Point tachometerBase1 = new Point();
	private final Point tachometerBase2 = new Point();
	private final Point tachometerPoint = new Point();
	
	private static RichLabel gear;
	
	private final DrawableImage damageBar;
	private final short damageBarWidth;
	
	
	/** Creates a new instance of DashBoard */
	public DashBoard( Racer player ) throws Exception {
		super( TOTAL_ITEMS );
		
		if ( player == null ) {
			//#if DEBUG == "true"
				throw new Exception( "player n�o pode ser null" );
			//#else
//# 				throw new Exception();
			//#endif
		}
		
		this.player = player;
		
		insertDrawable( board );
		
		setSize( board.getSize() );
		defineReferencePixel( size.x >> 1, size.y );
		
		insertDrawable( labelSpeed );
		
		insertDrawable( labelTime );
		
		insertDrawable( gear );
		
		//#if LOW_JAR == "false"
		// adiciona o sprite de mudan�a de marcha
		final Sprite gearSprite = player.engine.getGearSprite();
		gearSprite.setPosition( DASHBOARD_GEAR_SPRITE_X, DASHBOARD_GEAR_SPRITE_Y );
		insertDrawable( gearSprite );
		//#endif
		
		// insere grupo dos pist�es (tamanho e posi��o do grupo j� definidos dentro de Engine)
		insertDrawable( player.engine.getPistonsGroup() );
		
		// insere a barra indicadora de dano no motor
		damageBar = new DrawableImage( PATH_BOARD + "damage.png" );
		damageBar.setPosition( DASHBOARD_DAMAGE_X, DASHBOARD_DAMAGE_Y );
		damageBarWidth = ( short ) damageBar.getWidth();
		insertDrawable( damageBar );
		
		// insere shift light (posi��o do sprite j� definida dentro de Engine)
		insertDrawable( player.engine.getShiftLight() );
		
		insertDrawable( minimapTop );
		
		insertDrawable( minimapBottom );
	}

	
	public final void update( int delta) {
		super.update( delta );
		
		// atualiza o label de velocidade
		labelSpeed.setText( GameMIDlet.formatSpeed( player.engine.getSpeed() ), false );
		
		// atualiza o label de tempo
		labelTime.setText( GameMIDlet.formatTime( player.getTrackTime() ) );
		
		gear.setText( GameMIDlet.getText( TEXT_GEAR_NEUTRAL + player.engine.getGear() ) );
		
		damageBar.setSize( damageBarWidth * player.engine.getDamagePercent() / 100, damageBar.getHeight() );
	}

	
	protected final void paint( Graphics g ) {
		super.paint( g );
		drawTachometer( g) ;
	}
	
	
	private final void drawTachometer( Graphics g ) {
		// calcula a posi��o da ponta do tac�metro, pois ela � usada como base para definir os outros 2 v�rtices do tri�ngulo
		final int tachometerAngle = TACHOMETER_MIN_ANGLE + ( player.engine.getRotation() * TACHOMETER_MAX_ANGLE_DIFF / Engine.ROTATION_MAX );
		tachometerPoint.setVector( tachometerAngle, DASHBOARD_TACHOMETER_POINTER_LENGTH );
		// inverte y, pois o eixo y do celular tem o sentido invertido em rela��o ao normal
		tachometerPoint.y = -tachometerPoint.y;
		tachometerPoint.addEquals( translate.x + DASHBOARD_TACHOMETER_CENTER_X, translate.y + DASHBOARD_TACHOMETER_CENTER_Y );
		
		// calcula a posi��o dos outros 2 v�rtices (pr�ximos ao centro do tac�metro)
		tachometerBase1.setVector( tachometerAngle - 180 + DASHBOARD_TACHOMETER_POINTER_ANGLE, DASHBOARD_TACHOMETER_POINTER_LENGTH_TOTAL );
		// inverte y, pois o eixo y do celular tem o sentido invertido em rela��o ao normal
		tachometerBase1.y = -tachometerBase1.y;
		tachometerBase1.addEquals( tachometerPoint );
		
		tachometerBase2.setVector( tachometerAngle - 180 - DASHBOARD_TACHOMETER_POINTER_ANGLE, DASHBOARD_TACHOMETER_POINTER_LENGTH_TOTAL );
		// inverte y, pois o eixo y do celular tem o sentido invertido em rela��o ao normal
		tachometerBase2.y = -tachometerBase2.y;
		tachometerBase2.addEquals( tachometerPoint );

		// desenha o tri�ngulo que indica a rota��o
		g.setColor( TACHOMETER_COLOR );
		g.fillTriangle( tachometerPoint.x, tachometerPoint.y, tachometerBase1.x, tachometerBase1.y, tachometerBase2.x, tachometerBase2.y  );
	}
	
	
	public final void setMinimapPercent( int firstRacerPercent, int secondRacerPercent ) {
		if ( firstRacerPercent > 100 )
			firstRacerPercent = 100;
		else if ( firstRacerPercent < 0 )
			firstRacerPercent = 0;
		
		minimapTop.setSize( DASHBOARD_MINIMAP_WIDTH * firstRacerPercent / 100, DASHBOARD_MINIMAP_HEIGHT );

		if ( secondRacerPercent > 100 )
			secondRacerPercent = 100;
		else if ( secondRacerPercent < 0 )
			secondRacerPercent = 0;
		
		minimapBottom.setSize( DASHBOARD_MINIMAP_WIDTH * secondRacerPercent / 100, DASHBOARD_MINIMAP_HEIGHT );		
	}
	
	
	public static final void load( LoadScreen load ) throws Exception {
		board = new DrawableImage( PATH_BOARD + "board.png" );		
		load.changeProgress( 3 );
		
		final ImageFont numberFont = ImageFont.createMultiSpacedFont( PATH_BOARD + "font_number.png", PATH_BOARD + "font_number.dat" );
		
		load.changeProgress( 2 );
		
		labelSpeed = new Label( numberFont, null );
		labelSpeed.setPosition( DASHBOARD_SPEED_LABEL_X, DASHBOARD_SPEED_LABEL_Y );
		labelSpeed.setSize( DASHBOARD_SPEED_LABEL_WIDTH, DASHBOARD_SPEED_LABEL_HEIGHT );
		
		load.changeProgress( 2 );
		
		labelTime = new Label( numberFont, null );
		labelTime.setPosition( DASHBOARD_TIME_LABEL_X, DASHBOARD_TIME_LABEL_Y );
		labelTime.setSize( DASHBOARD_TIME_LABEL_WIDTH, DASHBOARD_TIME_LABEL_HEIGHT );
		
		load.changeProgress( 1 );
		
		final ImageFont fontGear = ImageFont.createMultiSpacedFont( PATH_BOARD + "font_gear.png", PATH_BOARD + "font_gear.dat" );
		gear = new RichLabel( fontGear, null, 0, null );
		gear.setSize( DASHBOARD_GEAR_LABEL_WIDTH, DASHBOARD_GEAR_LABEL_HEIGHT );
		gear.setPosition( DASHBOARD_GEAR_LABEL_X, DASHBOARD_GEAR_LABEL_Y );
		
		load.changeProgress( 1 );
		
		minimapTop = new Pattern( null );
		minimapTop.setPosition( DASHBOARD_MINIMAP_X, DASHBOARD_MINIMAP_FIRST_RACER_Y );
		minimapTop.setFillColor( 0xff0000 );
				
		minimapBottom = new Pattern( null );
		minimapBottom.setPosition( DASHBOARD_MINIMAP_X, DASHBOARD_MINIMAP_SECOND_RACER_Y );
		minimapBottom.setFillColor( 0x0000ff );
				
		load.changeProgress( 1 );
	}
}
