/*
 * Asphalt.java
 *
 * Created on October 11, 2007, 4:44 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Asphalt extends UpdatableGroup implements Constants {
	
	// n�mero total de itens do grupo
	private static final byte TOTAL_ITEMS = 5;
	
	// pattern usado para desenhar o asfalto
	private final byte ASPHALT_TOTAL_FRAMES = 3;	
	
	private final Sprite asphaltSprite;
	private final int asphaltImageHeight;
	
	private final Pattern pattern;
	
	//#if LOW_JAR == "false"
		private final Pattern stallsLeft;
		private final Pattern stallsRight;
		private final int stallsImageHeight;

		private final Pattern crowd;
	//#endif
	
	private static Asphalt instance;
		
	
	/** Creates a new instance of Asphalt */
	private Asphalt() throws Exception {
		super( TOTAL_ITEMS );
		
		// aloca e insere o pattern do asfalto
		final Image[] asphaltFrames = new Image[ ASPHALT_TOTAL_FRAMES ];
		if ( GameMIDlet.isLowMemory() ) {
			asphaltFrames[ 0 ] = ImageLoader.loadImage( PATH_IMAGES + "asphalt_0.png" );
			asphaltFrames[ 1 ] = asphaltFrames[ 0 ];
			asphaltFrames[ 2 ] = asphaltFrames[ 0 ];
		} else {
			final String asphaltPath = PATH_IMAGES + "asphalt_";
			for ( int i = 0; i < asphaltFrames.length; ++i )
				asphaltFrames[ i ] = ImageLoader.loadImage( asphaltPath + i + ".png" );			
		}
		
		// o tempo de troca dos frames � zero pois a troca do frame do asfalto � controlado de acordo com a velocidade do jogador
		asphaltSprite = new Sprite( asphaltFrames, new byte[][] { { 0, 1, 2 } }, new short[] { 0 } );
		
		
		asphaltImageHeight = asphaltSprite.getHeight();
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		pattern = new Pattern( asphaltSprite );
		pattern.setSize( asphaltSprite.getWidth(), size.y + ( asphaltImageHeight << 1 ) );
		pattern.defineReferencePixel( pattern.getWidth() >> 1, 0 );
		pattern.setRefPixelPosition( size.x >> 1, 0 );
		insertDrawable( pattern );
		
		// caso a largura da tela seja maior que a largura do pattern de asfalto, preenche as laterais com imagens da
		// arquibancada (e torcida, caso esteja no campeonato)
		if ( ScreenManager.SCREEN_WIDTH > asphaltSprite.getWidth() ) {
			final int widthDiff = NanoMath.max( ( size.x - asphaltSprite.getWidth() ) >> 1, 1 );
			
			//#if LOW_JAR == "false"
				final DrawableImage stallsLeftImage = new DrawableImage( PATH_IMAGES + "stallsLeft.png" );
				stallsImageHeight = stallsLeftImage.getHeight();
				
				// no caso de telas com mais de 240 pixels de largura, utiliza mais de um n�vel de arquibancada
				final byte repeat = ( byte ) ( 1 + ( widthDiff / ( stallsLeftImage.getWidth() - 1 ) ) );
				
				stallsLeft = new Pattern( stallsLeftImage );
				stallsLeft.setSize( stallsLeftImage.getWidth() * repeat, size.y + stallsImageHeight );
				stallsLeft.setPosition( widthDiff - stallsLeft.getWidth(), 0 );

				insertDrawable( stallsLeft );

				stallsRight = new Pattern( new DrawableImage( stallsLeftImage, TRANS_MIRROR_H ) );
				stallsRight.setSize( widthDiff, size.y + stallsImageHeight );			
				stallsRight.defineReferencePixel( stallsRight.getWidth(), 0 );
				stallsRight.setRefPixelPosition( size.x, 0 );

				insertDrawable( stallsRight );
				
				final DrawableGroup crowdGroup = new DrawableGroup( 2 + ( repeat << 1 ) );
				crowdGroup.setSize( size.x, stallsImageHeight );

				// insere a torcida - � um pattern que reproduz um grupo de 2 drawables, j� com as torcidas dos 2 lados
				for ( byte i = 0; i < repeat; ++i ) {
					final DrawableImage imageCrowdLeft = new DrawableImage( PATH_IMAGES + "crowdLeft.png" );
					imageCrowdLeft.setPosition( widthDiff - ( stallsLeftImage.getWidth() * ( i + 1 ) ), 0 );

					final DrawableImage imageCrowdRight = new DrawableImage( imageCrowdLeft, TRANS_MIRROR_H );
					imageCrowdRight.defineReferencePixel( imageCrowdRight.getWidth(), 0 );
					imageCrowdRight.setRefPixelPosition( size.x - widthDiff + ( stallsLeftImage.getWidth() * ( i + 1 ) ), 0 );

					crowdGroup.insertDrawable( imageCrowdLeft );
					crowdGroup.insertDrawable( imageCrowdRight );
				}

				crowd = new Pattern( crowdGroup );
				crowd.setSize( size.x, stallsLeft.getHeight() );
				insertDrawable( crowd );				
			} else {
				stallsLeft = null;
				stallsRight = null;
				stallsImageHeight = 0;
				crowd = null;
			//#else
//# 			Pattern p = new Pattern( null );
//# 			p.setSize( widthDiff, size.y );
//# 			p.setFillColor( COLOR_BACKGROUND );
//# 			insertDrawable( p );
//# 
//# 			p = new Pattern( null );
//# 			p.setSize( widthDiff, size.y );
//# 			p.setPosition( size.x - p.getWidth(), 0 );
//# 			p.setFillColor( COLOR_BACKGROUND );
//# 			insertDrawable( p );				
			//#endif
		}
		
	}

	
	public static final Asphalt createInstance() throws Exception {
		return ( instance == null ? new Asphalt() : instance );
	}
			
	
	public final void setDistance( int distanceInPixels ) {
		pattern.setPosition( pattern.getPosition().x, -asphaltImageHeight + ( distanceInPixels % asphaltImageHeight ) );
		
		//#if LOW_JAR == "false"
			if ( stallsLeft != null ) {
				final int y = -stallsImageHeight + ( distanceInPixels % stallsImageHeight );

				stallsLeft.setPosition( stallsLeft.getPosition().x, y );
				stallsRight.setPosition( stallsRight.getPosition().x, y );

				crowd.setPosition( 0, -stallsImageHeight + ( ( distanceInPixels + CROWD_STALLS_Y_OFFSET ) % stallsImageHeight ) );
			}
		//#endif
	}
	

	public final void setSpeed( int speed ) {
		if ( speed < ASPHALT_FRAME_1_SPEED )
			asphaltSprite.setFrame( 0 );
		else if ( speed < ASPHALT_FRAME_2_SPEED )
			asphaltSprite.setFrame( 1 );
		else
			asphaltSprite.setFrame( 2 );
	}


	//#if LOW_JAR == "false"
		public final void setCrowdVisible( boolean crowdVisible ) {
			if ( crowd != null )
				crowd.setVisible( crowdVisible );
		}
	//#endif
	
}
