/*
 * Racer.java
 *
 * Created on October 4, 2007, 4:18 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.InputStream;
import screens.LoadScreen;

/**
 *
 * @author peter
 */
public class Racer extends UpdatableGroup implements Constants {
	
	// n�mero total de itens na cole��o
	protected static final byte TOTAL_ITEMS = 1;
	
	// nomes dos personagens
	protected static final String[] names = new String[ CHARACTERS_TOTAL ];
	
	// �ndice do corredor "dummy"
	public static final byte DUMMY_RACER_INDEX = 0;
	
	// lista de quais personagens j� est�o em uso
	protected static final boolean[] selectedIndexes = new boolean[ CHARACTERS_TOTAL ];
	
	// rostos dos personagens
	protected static final DrawableImage[] faces = new DrawableImage[ CHARACTERS_TOTAL ];
	
	protected static boolean charactersLoaded;
	
	// tempo acumulado do piloto na prova atual
	protected int trackTime;
	
	// tempo m�ximo de prova (evita que se estoure a �rea dispon�vel para o tempo no painel no caso de o jogador n�o largar)
	private static final int MAX_TRACK_TIME = 99999;
	
	// posi��o inicial dos carros antes de se posicionarem para a largada
	private static final int STAGING_MIN_INITIAL_REAL_POSITION = -10 * METER;
	private static final int STAGING_MAX_INITIAL_REAL_POSITION = -15 * METER;
	
	/** posi��o real do carro na corrida */
	protected int realPosition;
	
	protected final MUV realPositionMUV = new MUV();
	
	// motor do carro
	public final Engine engine;
	
	// partes do carro
	protected Car car;

	/** estado do corredor numa corrida */
	protected byte state;
	
	// n�mero atual de largadas queimadas. Caso se atinja o limite (RACE_MAX_RED_LIGHTS), o corredor � eliminado.
	protected byte redLights;
	
	// rosto do personagem
	protected final DrawableImage face;
	
	// �ndice do corredor (para que se possa recarregar seus dados ao continuar um campeonato)
	protected final byte index;
	
	private boolean carLoaded;
	
	
	/** Creates a new instance of Racer
	 * @param index
	 * @param loadCar 
	 * @throws java.lang.Exception
	 */
	public Racer( int index, boolean loadCar ) throws Exception {
		super( TOTAL_ITEMS );
		
		//#if DEBUG == "true"
			if ( index < 0 )
				throw new Exception( "invalid racer index: " + index );
		//#endif
		
		face = faces[ index ];
		
		this.index = ( byte ) index;
		selectedIndexes[ index ] = true;		
		
		if ( loadCar )
			loadCar();		
		
		engine = new Engine( this );
	}
	
	
	/**
	 * Aloca o carro.
	 * @throws java.lang.Exception 
	 */
	public final void loadCar() throws Exception {
		if ( !carLoaded ) {
			car = new Car( index );
			insertDrawable( car );
			setSize( car.getSize() );
			defineReferencePixel( size.x >> 1, 0 );		
			
			carLoaded = true;
		}
	}
	
	
	/**
	 * Desaloca o carro.
	 */
	public final void unloadCar() {
		removeDrawable( 0 );
		car = null;
		carLoaded = false;
	}
	

	public final int getRealPosition() {
		return realPosition;
	}
	
	
	public void update( int delta ) {
		// a divis�o � necess�ria pois a velocidade � em cm/h, por�m � atualizada segundo a segundo
		final long realDx = realPositionMUV.updateLong( delta ) / SECONDS_PER_HOUR;
		
		realPosition += realDx;
		realPositionMUV.setSpeed( engine.getSpeed(), false );
		
		super.update( delta );

		engine.update( delta );
		car.updateTires( ( int ) realDx );
		
		if ( engine.getSpeed() <= TIRES_REAR_MAX_SPEED_WIDE )
			car.rearTires.setSequence( RearTires.SEQUENCE_WIDE );
		else if ( engine.getSpeed() <= TIRES_REAR_MAX_SPEED_MEDIUM )
			car.rearTires.setSequence( RearTires.SEQUENCE_MEDIUM );
		else
			car.rearTires.setSequence( RearTires.SEQUENCE_NARROW );

		switch ( state ) {
			case RACE_STATE_PRE_STAGING:
				if ( realPosition >= 0 ) {
					realPosition = 0;
					setState( RACE_STATE_STAGING );
				}
			break;

			case RACE_STATE_RACING:
				trackTime += delta;
				if ( trackTime > MAX_TRACK_TIME )
					trackTime = MAX_TRACK_TIME;
			break; // fim case RACE_STATE_RACING

			case RACE_STATE_BRAKING:
				setState( RACE_STATE_FINISHED );
			case RACE_STATE_FINISHED:
			case RACE_STATE_WRECKED:
				// altera a velocidade de anima��o do p�ra-quedas de acordo com a velocidade do carro
				if ( engine.getSpeed() >= PARACHUTES_MIN_SPEED_FAST ) {
					if ( car.parachutes.getSequence() != Parachutes.SEQUENCE_OPENING )
						car.parachutes.setSequence( Parachutes.SEQUENCE_SPEED_FAST );
				}
				else if ( engine.getSpeed() >= PARACHUTES_MIN_SPEED_MEDIUM )
					car.parachutes.setSequence( Parachutes.SEQUENCE_SPEED_MEDIUM );
				else if ( engine.getSpeed() >= PARACHUTES_MIN_SPEED_SLOW )
					car.parachutes.setSequence( Parachutes.SEQUENCE_SPEED_SLOW );
				else
					car.parachutes.setSequence( Parachutes.SEQUENCE_CLOSING );
			break;
		} // fim switch ( state )
	}
	
	
	public void setState( int state ) {
		if ( this.state != state ) {
			switch ( state ) {
				case RACE_STATE_PRE_STAGING:
					reset();
					car.turnEngineOn( false );
				break;
				
				case RACE_STATE_STAGING:
					car.turnEngineOn( true );
					
					realPosition = 0;
					realPositionMUV.setSpeed( 0 );
					engine.reset();
					engine.setAcceleration( Engine.ACCELERATION_OFF );
				break;
					
				case RACE_STATE_RED_LIGHT:
					engine.increaseSpeed( Engine.RED_LIGHT_SPEED_INCREASE );
					engine.setAcceleration( Engine.ACCELERATION_OFF );
				break;
				
				case RACE_STATE_WRECKED:
					car.turnEngineOn( false );
				case RACE_STATE_BRAKING:
					car.parachutes.setSequence( Parachutes.SEQUENCE_OPENING );
					engine.setAcceleration( state == RACE_STATE_BRAKING ? Engine.ACCELERATION_BRAKING : Engine.ACCELERATION_WRECKED );
				break;
			}

			this.state = ( byte ) state;		
		}
	}
	
	
	public final int getTrackTime() {
		return trackTime;
	}
	
	
	public final byte getState() {
		return state;
	}
	
	
	/**
	 * Incrementa o contador de largadas queimadas.
	 * @return true, caso o corredor tenha sido eliminado por exceder o limite de largadas queimadas, e false caso contr�rio.
	 */
	public final boolean increaseRedLights() {
		++redLights;
		
		return redLights >= RACE_MAX_RED_LIGHTS;
	}
	
	
	public final void resetRedLights() {
		redLights = 0;
	}
	
	
	private final void reset() {
		car.turnEngineOn( false );
		engine.reset();
		engine.setAcceleration( Engine.ACCELERATION_STAGING );
		
		realPosition = CAR_PRE_STAGING_MIN_DISTANCE - NanoMath.randInt( CAR_PRE_STAGING_MAX_DISTANCE );
		realPositionMUV.setSpeed( 0 );
		trackTime = 0;
		car.reset();
		
		setVisible( true );
	}
	
	
	public final String getName() {
		return names[ index ];
	}
	
	
	public final byte getRedLights() {
		return redLights;
	}
	
	
	public final byte getIndex() {
		return index;
	}
	
	
	private static final void loadNames() throws Exception {
		InputStream inputStream = null;
		DataInputStream dataInputStream = null;
		
		final char[] buffer = new char[ 2500 ];
		int bufferIndex = 0;

		byte currentName = 0;
		
		try {
			inputStream = buffer.getClass().getResourceAsStream( PATH_TEXTS + "names.dat" );
			dataInputStream = new DataInputStream( inputStream );

			while ( currentName < CHARACTERS_TOTAL ) {
				char c = ( char ) dataInputStream.readUnsignedByte();

				switch ( c ) {
					case '\n':
						// chegou ao final de uma string
						names[ currentName++ ] = new String( buffer, 0, bufferIndex );
						bufferIndex = 0;
					break;

					case '\\':
						c = '\n';

					default:
						buffer[ bufferIndex++ ] = c;
				} // fim switch ( c )
			} // fim while ( currentName < TOTAL_NAMES )
		} catch ( EOFException e ) {
			// chegou ao final do arquivo; verifica se h� dados no buffer
			if ( currentName < CHARACTERS_TOTAL && bufferIndex > 0 )
				names[ currentName ] = new String( buffer, 0, bufferIndex );
			else {
				//#if DEBUG == "true"
					throw new Exception( "erro ao ler personagem #" + currentName );
				//#else
//# 					throw new Exception();
				//#endif
			}
		} finally {
			try {
				if ( dataInputStream != null ) {
					try {
						dataInputStream.close();
					} catch ( Exception ex ) {
					}
					dataInputStream = null;
				}

				if ( inputStream != null ) {
					try {
						inputStream.close();
					} catch ( Exception ex ) {
					}

					inputStream = null;
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif				
			}
		} // fim finally
	} // fim do m�todo loadNames()		
	
	
	public static final boolean isAvailable( int index ) {
		try {
			return index != DUMMY_RACER_INDEX && !selectedIndexes[ index ];
		} catch ( Exception e ) {
			return false;
		}
	}

	
	public static final DrawableImage getDummyFace() {
		try {
			return new DrawableImage( faces[ DUMMY_RACER_INDEX ] );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return null;
		}
	}
	
	
	public static final String getDummyName() {
		return names[ DUMMY_RACER_INDEX ];
	}
	
	
	public final DrawableImage getFace() {
		if ( index == DUMMY_RACER_INDEX )
			return getDummyFace();
		
		return face;
	}
	
	
	public static final void loadRacers( LoadScreen load ) throws Exception {
		if ( !charactersLoaded ) {
			loadNames();
			load.changeProgress( 6 );
			
			for ( int i = 0; i < faces.length; ++i ) {
				faces[ i ] = new DrawableImage( PATH_CHARACTERS + i + ".png" );
				
				load.changeProgress( 1 );
			}
			charactersLoaded = true;
		}
		
		for ( int i = 0; i < selectedIndexes.length; ++i )
			selectedIndexes[ i ] = false;		
	}
	
	
	public static final DrawableImage getFaceAt( int index ) throws Exception {
		return new DrawableImage( faces[ index ] );
	}
	
	
	public static final String getNameAt( int index ) {
		return names[ index ];
	}

	
	/**
	 * Faz ajuste no tempo de prova, pois a verifica��o do momento em que cruzou a linha de chegada pode ter sido feita
	 * instantes depois de ter cruzado efetivamente a linha.
	 */
	public final void adjustTrackTime() {
		// c�lculo utilizado: dT = dS / dV;
		// verifica-se qual a dist�ncia a mais foi percorrida no instante que foi detectado que passou da chegada; depois,
		// � calculado o tempo que o carro levaria para percorrer essa dist�ncia na velocidade atual (o c�lculo despreza
		// uma poss�vel varia��o de velocidade nesse per�odo), convertendo-se os valores de Km/h para m/s (na verdade,
		// convertendo cm/h para cm/ms).
		// logo: ( realPosition - DEFAULT_TRACK_LENGTH ) -> dist�ncia percorrida ap�s a chegada
		// 1000: quantidade de milisegundos em 1 segundo
		trackTime -= ( realPosition - DEFAULT_TRACK_LENGTH ) * 1000 * SECONDS_PER_HOUR / engine.getSpeed();
	}
	
	
	public final int getCarBodyHeight() {
		return car.getBodyHeight();
	}
}
