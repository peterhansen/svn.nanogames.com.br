/*
 * Engine.java
 *
 * Created on October 4, 2007, 4:21 PM
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import screens.GameMIDlet;
import screens.LoadScreen;
import screens.PlayScreen;

/**
 *
 * @author peter
 */
public final class Engine implements Constants, Updatable, SpriteListener {
	
	/** n�mero total de marchas (incluindo o ponto morto) */
	protected static final byte TOTAL_GEARS = 5;
	
	/** rela��o de cada marcha (utilizado para calcular a nova rota��o ap�s troca de marcha) */
	private static final short[] GEAR_RATIO = { 9999, 6894, 3587, 2241, 1255 };
	
	/** multiplicador da curva de "torque" dada pela curva de Bezier (quanto maior este valor, mais r�pido a rota��o varia) */
	private static final byte TORQUE_MULTIPLIER = 43;
	
	/** velocidade de altera��o do tac�metro no ponto morto */
	private static final short ROTATION_SPEED_NEUTRAL = TORQUE_MULTIPLIER * 200;
	
	/** rota��o do motor no ponto morto */
	private static final short ROTATION_NEUTRAL = 900;
	
	/** rota��o m�xima do motor */
	public static final short ROTATION_MAX = 10000;
	
	/** rota��o utilizada como ponto ideal de troca de marcha */
	public static final short ROTATION_MAX_TORQUE = 7900;	

	/** valores utilizados para descrever as curvas de Bezier de cada marcha, formando assim suas curvas de "torque" */
	private static final short[][] GEAR_TORQUE = {	{   0,   0,   0 },
													{   0,  40, 225 },
													{   0,  34, 175 },
													{   0,  28, 125 },
													{   0,  22,  79 },
	};
	
	private static final short[] GEAR_TORQUE_ROTATIONS = { 0, ROTATION_NEUTRAL, 7000, 9000 };
	
	private final BezierCurve bezier = new BezierCurve();
	
	/** �ndice do ponto morto */
	public static final byte GEAR_NEUTRAL = 0;
	
	/** �ndice da marcha atual */
	private byte gear;
	
	/** rota��o atual do motor em rpm */
	private short rotation;
	
	/** velocidade com que a rota��o � alterada (utilizado no c�lculo da varia��o de rota��o a cada frame) */
	private final MUV rotationSpeed	= new MUV();
	
	/** rota��o utilizada como base para redu��o autom�tica de marchas quando se est� freando. O c�lculo da rota��o � o seguinte:
	  <p>ROTATION_MAX_TORQUE * GEAR_RATIO[ gear ] / GEAR_RATION[ gear - 1 ] </p>
	 */
	private short rotationBrakingGearDown;
	
	/** porcentagem do poder de frenagem quando se est� no modo ACCELERATION_BRAKING */
	private static final short BRAKE_SPEED_CHANGE_PERCENT = 160;
	
	/** faixa de valores considerada para efeitos da troca da luz de marcha */
	private static final short ROTATION_LIGHT_RANGE = ROTATION_MAX * 10 / 100;
	
	private static final short ROTATION_START_YELLOW_BELOW	= ROTATION_MAX_TORQUE - ( ROTATION_LIGHT_RANGE << 1 );
	private static final short ROTATION_START_GREEN			= ROTATION_START_YELLOW_BELOW + ROTATION_LIGHT_RANGE;
	private static final short ROTATION_START_YELLOW_ABOVE	= ROTATION_START_GREEN + ( ROTATION_LIGHT_RANGE << 1 );
	private static final short ROTATION_START_RED			= ROTATION_START_YELLOW_ABOVE + ROTATION_LIGHT_RANGE;
	
	private static final short ROTATION_DIFF = ROTATION_MAX - ROTATION_NEUTRAL;
	
	/** velocidade de altera��o da rota��o ap�s motor estourar, em unidades por segundo (rota��o desce at� zerar) */
	private static final short ROTATION_SPEED_BLOWN = -ROTATION_MAX << 1;
	
	/** rota��o utilizada como base para indicar ao jogador que � melhor reduzir a marcha para conseguir uma melhor acelera��o */
	private static final short ROTATION_GEAR_DOWN = ROTATION_MAX * 35 / 100;
	
	/** rota��o de in�cio de dano do motor */
	private static final short ROTATION_START_DAMAGE = ROTATION_MAX * 77 / 100;
	
	/** rota��o m�xima da marcha mais baixa para que a marcha "entre" - caso seja superior a esse valor, "arranha" o c�mbio */
	private static final short ROTATION_LOW_GEAR_MAX = ROTATION_MAX * 95 / 100;
	
	/** rota��o ideal para a largada */
	private static final short ROTATION_NEUTRAL_MAX_TORQUE = ROTATION_MAX * 5 / 10;
	private static final short ROTATION_NEUTRAL_MAX_TORQUE_DIFF = ROTATION_NEUTRAL_MAX_TORQUE >> 2;
	private static final short ROTATION_NEUTRAL_MAX_TORQUE_BEGIN = ROTATION_NEUTRAL_MAX_TORQUE - ROTATION_NEUTRAL_MAX_TORQUE_DIFF;
	public static final short ROTATION_NEUTRAL_MAX_TORQUE_END = ROTATION_NEUTRAL_MAX_TORQUE + ROTATION_NEUTRAL_MAX_TORQUE_DIFF;
	
	/** rota��o m�nima do carro em cada troca de marcha para que os pneus traseiros soltem fuma�a */
	private static final short[] SMOKE_MIN_ROTATION = { ROTATION_NEUTRAL_MAX_TORQUE * 8 / 10, 
														ROTATION_MAX * 7 / 10,
														ROTATION_MAX * 7 / 10,
														ROTATION_MAX * 7 / 10 };
	
	/** velocidade m�xima para que o engate de uma marcha fa�a os pneus traseiros soltar fuma�a */	
	private static final int[] SMOKE_MAX_SPEED = { 70 * KILOMETER, 170 * KILOMETER, 300 * KILOMETER, 380 * KILOMETER };
	
	private static final short ROTATION_NEUTRAL_START_YELLOW_BELOW	= ROTATION_NEUTRAL_MAX_TORQUE - ( ROTATION_LIGHT_RANGE << 1 );
	private static final short ROTATION_NEUTRAL_START_GREEN			= ROTATION_NEUTRAL_START_YELLOW_BELOW + ROTATION_LIGHT_RANGE;
	private static final short ROTATION_NEUTRAL_START_YELLOW_ABOVE	= ROTATION_NEUTRAL_START_GREEN + ( ROTATION_LIGHT_RANGE << 1 );
	private static final short ROTATION_NEUTRAL_START_RED			= ROTATION_NEUTRAL_START_YELLOW_ABOVE + ROTATION_LIGHT_RANGE;	
	
	/** velocidade atual do motor em cent�metros por hora */
	private int speed;
	
	/** velocidade m�xima */
	private int topSpeed;
	
	/** acelera��o atual */
	private final MUV acceleration = new MUV();
	
	/** multiplicador da acelera��o - leva em conta a porcentagem atual da curva de torque, e multiplica este valor para obter
	 a varia��o da velocidade. Quanto maior este multiplicador, maior a acelera��o e a velocidade final dos carros. */
	private static final int ACCELERATION_MULTIPLIER = 1233 * METER;
	
	/** acelera��o do carro no ponto morto */
	private static final int ACCELERATION_NEUTRAL = ACCELERATION_MULTIPLIER * 10;
	
	/** acelera��o do carro quando o motor quebra */
	private static final int SPEED_CHANGE_WRECKED = 66 * KILOMETER;
	
	/** velocidade ganha com uma largada perfeita */
	private static final int MAX_SPEED_GAIN_START = 63 * KILOMETER;

	/** perda de velocidade ao realizar uma troca de marcha sem soltar o acelerador */
	private static final int BAD_GEAR_CHANGE_SPEED_LOSS = -20 * KILOMETER;
	
	/** perda de velocidade ao realizar uma troca de marcha autom�tica (essa perda � utilizada para n�o haver grande diferen�a
	 * nas velocidades entre os modos manual e autom�tico, e para reduzir a vantagem que a cpu tem por n�o ter de soltar
	 * o acelerador nas trocas de marcha)
	 */
	private static final int AUTOMATIC_GEAR_CHANGE_SPEED_LOSS = -17 * KILOMETER;	
	
	private static final int DAMAGE_MAX = 100000;
	private int damage;
	
	private final MUV damageSpeed = new MUV();
	
	/** incremento no dano para cada rota��o acima do limite */
	private static final short DAMAGE_INCREASE_PER_RPM = DAMAGE_MAX / ( ( ROTATION_MAX - ROTATION_START_DAMAGE ) * 3 );
	
	/** dano causado ao motor no caso de troca de marcha mal executada */
	private static final short DAMAGE_INCREASE_BAD_GEAR_CHANGE = DAMAGE_MAX / 5;
	
	//#if LOW_JAR == "false"
	private final Sprite spriteGear;
	//#endif
	
	private static final byte GEAR_SEQUENCE_STOPPED	= 0;
	private static final byte GEAR_SEQUENCE_DOWN	= 1;
	private static final byte GEAR_SEQUENCE_UP		= 2;
	
	public static final byte ACCELERATION_OFF		= 0;
	public static final byte ACCELERATION_ON		= 1;
	public static final byte ACCELERATION_AUTOMATIC	= 2;
	public static final byte ACCELERATION_BRAKING	= 3;
	public static final byte ACCELERATION_STAGING	= 4;
	public static final byte ACCELERATION_WRECKED	= 5;
	
	/** modo autom�tico, com rota��o abaixo da m�xima definida para o modo autom�tico */
	private static final byte ACCELERATION_AUTOMATIC_ON		= 6;
	/** modo autom�tico, por�m ultrapassou a rota��o m�xima definida para o autom�tico, logo p�ra de acelerar at� ficar abaixo do m�nimo */
	private static final byte ACCELERATION_AUTOMATIC_OFF	= 7;
	
	private byte accelerationMode;
	
	private final Racer racer;
	
	// <editor-fold desc="defini��es dos pist�es (sprites s�o inicializados nessa classe, por�m desenhados em DashBoard)">
	private static final byte PISTONS_TOTAL = 4;
	
	private static final byte PISTONS_SEQUENCE_STOPPED	= 0;
	private static final byte PISTONS_SEQUENCE_MOVING	= 1;
	private static final byte PISTONS_SEQUENCE_BROKEN	= 2;
	
	private final UpdatableGroup pistons;
	// </editor-fold>
	
	// <editor-fold desc="defini��es da luz de marcha">
	private final Sprite spriteShiftLight;
	
	public static final byte SHIFT_LIGHT_FRAME_YELLOW	= 0;
	public static final byte SHIFT_LIGHT_FRAME_GREEN	= 1;
	public static final byte SHIFT_LIGHT_FRAME_RED		= 2;
	// </editor-fold>
	
	private static Sprite frameSetGear;
	private static Sprite frameSetPistons;
	private static Sprite frameSetShiftLight;
	
	/** tela de jogo - a refer�ncia � usada para atualizar os sprites de indica��o de jogada para o jogador */
	private PlayScreen playScreen;
	
	private final boolean playSoundOnGearChange;

	/** incremento na velocidade quando jogador queima a largada (valor utilizado para evitar que se queime a largada,
	 * mas visualmente n�o haja indica��o de que jogador avan�ou a linha)
	 */
	public static final int RED_LIGHT_SPEED_INCREASE = 35 * KILOMETER;
	
	
	/** Creates a new instance of Engine
	 * @param racer 
	 */
	public Engine( Racer racer ) throws Exception {
		this.racer = racer;
		
		// s� toca sons ao trocar marcha caso o motor seja do jogador
		playSoundOnGearChange = !( racer instanceof RacerAI );
		
		//#if LOW_JAR == "false"
		// somente aloca o spriteGear - inser��o � feita em DashBoard (a refer�ncia � armazenada aqui para que altera��es do
		// c�mbio reflitam automaticamente no painel)
		spriteGear = new Sprite( frameSetGear );
		spriteGear.setListener( this, 0 );
		setGear( GEAR_NEUTRAL );
		//#endif
		
		pistons = new UpdatableGroup( PISTONS_TOTAL );
		pistons.setSize( DASHBOARD_PISTONS_WIDTH, DASHBOARD_PISTONS_HEIGHT );
		pistons.setPosition( DASHBOARD_PISTONS_X, DASHBOARD_PISTONS_Y );

		for ( int i = 0; i < PISTONS_TOTAL; ++i ) {
			final Sprite piston = new Sprite( frameSetPistons );
			piston.setSequence( PISTONS_SEQUENCE_MOVING );
			piston.setFrame( i & 1 );
			piston.setPosition( piston.getWidth() * i, 0 );
			pistons.insertDrawable( piston );
		}
		
		spriteShiftLight = new Sprite( frameSetShiftLight );
		spriteShiftLight.setPosition( DASHBOARD_SHIFT_LIGHT_X, DASHBOARD_SHIFT_LIGHT_Y );
	}
	
	
	public static final void loadFrameSets( LoadScreen load ) throws Exception {
		//#if LOW_JAR == "true"
//# 		load.changeProgress( 5 );
		//#else
		frameSetGear = new Sprite( PATH_BOARD + "gear.dat", PATH_BOARD + "gear" );
		//#endif
		load.changeProgress( 2 );
		
		// aloca as imagens dos pist�es
		frameSetPistons = new Sprite( PATH_BOARD + "piston.dat", PATH_BOARD + "piston" );
		load.changeProgress( 2 );
		
		// aloca as imagens da luz de troca de marcha
		frameSetShiftLight = new Sprite( PATH_BOARD + "shift.dat", PATH_BOARD + "shift" );
		load.changeProgress( 2 );
	}
	
	
	public final void gearUp( boolean playSound ) {
		if ( setGear( gear + 1 ) ) {
			//#if LOW_JAR == "false"
			spriteGear.setSequence( GEAR_SEQUENCE_UP );
			//#endif
			
			switch ( accelerationMode ) {
				case ACCELERATION_ON:
					gearChangeProblem();
				break;
				
				case ACCELERATION_AUTOMATIC:
				case ACCELERATION_AUTOMATIC_ON:
					// no modo autom�tico, reduz a velocidade dos carros na troca de marcha, para simular a perda causada
					// por soltar o acelerador durante a troca, no modo manual
					if ( gear > 1 )
						increaseSpeed( AUTOMATIC_GEAR_CHANGE_SPEED_LOSS );
					
				default:
					if ( playSoundOnGearChange && playSound )
						MediaPlayer.play( SOUND_GEAR_CHANGE_OK, 1 );
			}
		}
	}
	
	
	public final void gearDown( boolean playSound ) {
		if ( setGear( gear - 1 ) ) {
			//#if LOW_JAR == "false"
			spriteGear.setSequence( GEAR_SEQUENCE_DOWN );
			//#endif
			
			if ( accelerationMode == ACCELERATION_ON )
				gearChangeProblem();
			else if ( playSoundOnGearChange && playSound )
				MediaPlayer.play( SOUND_GEAR_CHANGE_OK, 1 );
		}
	}
	
	
	private final void gearChangeProblem() {
		increaseDamage( DAMAGE_INCREASE_BAD_GEAR_CHANGE );
		increaseSpeed( BAD_GEAR_CHANGE_SPEED_LOSS );
		
		if ( playSoundOnGearChange ) {
			if ( MediaPlayer.isMuted() )
				MediaPlayer.vibrate( VIBRATION_TIME_GEAR_CHANGE_ERROR );
			else
				MediaPlayer.play( SOUND_GEAR_CHANGE_ERROR, 1 );
		}
	}	
	
	
	public final byte getGear() {
		return gear;
	}
	
	
	public final boolean setGear( int gear ) {
		byte previousGear = this.gear;
		
		// caso o motor esteja estourado, n�o pode mais trocar de marcha
		if ( damage < DAMAGE_MAX && gear >= GEAR_NEUTRAL && gear < TOTAL_GEARS ) {
			
			if ( gear != GEAR_NEUTRAL ) {
				// mudou a marcha; caso seja o ponto morto, mant�m a rota��o anterior, sen�o altera a rota��o de acordo
				// com a rela��o entre a nova marcha e a anterior
				int newRotation = rotation * GEAR_RATIO[ gear ] / GEAR_RATIO[ previousGear ];
				
				// calcula o ganho de velocidade instant�nea obtido com a largada (quanto mais pr�ximo de ROTATION_NEUTRAL_MAX_TORQUE,
				// maior o ganho)
				if ( speed == 0 && previousGear == GEAR_NEUTRAL ) {
					final int diff = Math.abs( rotation - ROTATION_NEUTRAL_MAX_TORQUE );
					if ( diff <= ROTATION_NEUTRAL_MAX_TORQUE_DIFF )
						speed += ( 100 - ( diff * 100 / ROTATION_NEUTRAL_MAX_TORQUE_DIFF ) ) * MAX_SPEED_GAIN_START / 100;
				}

				//#if LOW_JAR == "false"
				if ( !GameMIDlet.isLowMemory() ) {
					if ( gear > previousGear && rotation >= SMOKE_MIN_ROTATION[ previousGear ] && speed <= SMOKE_MAX_SPEED[ previousGear ] )
						racer.car.smoke.setSequence( gear );
				}
				//#endif

				if ( newRotation >= ROTATION_START_DAMAGE ) {
					increaseDamage ( ( newRotation - ROTATION_START_DAMAGE ) * DAMAGE_INCREASE_BAD_GEAR_CHANGE / ROTATION_MAX );

					if ( newRotation >= ROTATION_LOW_GEAR_MAX ) {
						// nova marcha n�o entrou, pois a rota��o ficou acima do limite
						gearChangeProblem();

						return false;
					}
				} // fim if ( newRotation >= ROTATION_START_DAMAGE )
				rotation = ( short ) newRotation;
			}
			
			//#if LOW_JAR == "false"
			if ( racer != null && racer.car != null )
				racer.car.setFireSequence( Car.FIRE_SEQUENCE_EXPELL_THEN_IDLE );
			//#endif
			
			this.gear = ( byte ) gear;
			
			// chama o seguinte m�todo apenas para j� atualizar o shift light (o que � importante para o modo treinamento)
			changeRotation( 0 );			

			bezier.origin.set( GEAR_TORQUE_ROTATIONS[ 0 ], GEAR_TORQUE[ gear ][ 0 ] );
			bezier.control1.set( GEAR_TORQUE_ROTATIONS[ 1 ], GEAR_TORQUE[ gear ][ 1 ] );
			bezier.control2.set( GEAR_TORQUE_ROTATIONS[ 2 ], GEAR_TORQUE[ gear ][ 2 ] );
			bezier.destiny.set( GEAR_TORQUE_ROTATIONS[ 3 ], 0 );
		} // fim if ( gear >= GEAR_NEUTRAL && gear < TOTAL_GEARS )
		
		return previousGear != this.gear;
	} // fim do m�toto setGear( int )
	
	
	/**
	 * Obt�m o dano atual do motor, em porcentagem.
	 * @return inteiro indicando a porcentagem de dano do motor.
	 */
	public final int getDamagePercent() {
		return damage * 100 / DAMAGE_MAX;
	}

	
	public final void update( int delta ) {
		final long ds = acceleration.updateLong( delta );
		
		if ( damage < DAMAGE_MAX ) {
			final long dr = rotationSpeed.updateLong( delta );
			
			if ( gear == GEAR_NEUTRAL ) {
				rotationSpeed.setSpeed( ROTATION_SPEED_NEUTRAL, false );
				acceleration.setSpeed( ACCELERATION_NEUTRAL, false );
			} else {
				final int torquePercent = bezier.getY( rotation * 100 / ROTATION_MAX );
				rotationSpeed.setSpeed( TORQUE_MULTIPLIER * torquePercent, false );				

				acceleration.setSpeed( ACCELERATION_MULTIPLIER * torquePercent, false );
			}

			switch ( accelerationMode ) {
				case ACCELERATION_ON:
					changeRotation( dr );

					if ( gear > GEAR_NEUTRAL ) {
						increaseSpeed( ds );
					} else {
						// caso o jogador esteja acelerando em ponto morto, a rota��o do carro sobe, por�m ele perde velocidade
						increaseSpeed( -ds );
					}
				break;
				
				case ACCELERATION_BRAKING:
					changeRotation( -( dr << 1 ) );
					// poder de frenagem nesse modo de acelera��o � maior
					increaseSpeed( -ds * BRAKE_SPEED_CHANGE_PERCENT / 100 );

					if ( rotation <= rotationBrakingGearDown ) {
						if ( gear > GEAR_NEUTRAL ) {
							if ( gear > 1 ) {
								gearDown( false );

								rotationBrakingGearDown = ( short ) ( ROTATION_MAX_TORQUE * GEAR_RATIO[ gear ] / GEAR_RATIO[ gear - 1 ] );
							} else if ( speed <= 0 ) {
								setAcceleration( ACCELERATION_OFF );
							}
						}
					} // fim if ( rotation <= rotationBrakingGearDown )
				break;
				
				case ACCELERATION_AUTOMATIC_OFF:
					if ( gear > GEAR_NEUTRAL ) {
						if ( rotation <= ROTATION_START_GREEN )
							setAcceleration( ACCELERATION_AUTOMATIC_ON );
					} else {
						// volta a acelerar quando a rota��o saiu do ponto ideal para a largada
						if ( rotation <= ROTATION_NEUTRAL_MAX_TORQUE_BEGIN )
							setAcceleration( ACCELERATION_AUTOMATIC_ON );						
					}
				case ACCELERATION_OFF:
					changeRotation( -dr );

				case ACCELERATION_WRECKED:
					increaseSpeed( -ds );
				break;

				case ACCELERATION_AUTOMATIC:
				case ACCELERATION_AUTOMATIC_ON:
					changeRotation( dr );
					
					if ( gear > GEAR_NEUTRAL ) {
						increaseSpeed( ds );
						if ( rotation >= ROTATION_START_YELLOW_ABOVE )
							setAcceleration( ACCELERATION_AUTOMATIC_OFF );
					} else {
						// caso o jogador esteja acelerando em ponto morto, a rota��o do carro sobe, por�m ele perde velocidade
						increaseSpeed( -ds );
						
						// p�ra de acelerar quando a rota��o saiu do ponto ideal para a largada
						if ( rotation >= ROTATION_NEUTRAL_MAX_TORQUE_END )
							setAcceleration( ACCELERATION_AUTOMATIC_OFF );
					}
				break;
			} // fim switch ( mode )

			// caso rota��o esteja acima do limite, aumenta o dano do motor
			if ( rotation >= ROTATION_START_DAMAGE ) {
				increaseDamage( damageSpeed.updateInt( delta ) );
				damageSpeed.setSpeed( ( rotation - ROTATION_START_DAMAGE ) * DAMAGE_INCREASE_PER_RPM, false );
			}
		} else {
			// motor estourou; reduz rota��o at� zerar
			if ( rotation > 0 ) {
				rotation += rotationSpeed.updateLong( delta );
				
				if ( rotation < 0 )
					rotation = 0;
			}
			increaseSpeed( -ds );
		}
	}
	
	
	public final int getSpeed() {
		return speed;
	}

	
	public final void reset() {
		setGear( GEAR_NEUTRAL );
		
		damage = 0;
		resetPistons();
		
		speed = 0;
		topSpeed = 0;
		
		rotation = ROTATION_NEUTRAL;
		spriteShiftLight.setVisible( false );
		rotationSpeed.setSpeed( 0 );
		
		acceleration.setSpeed( 0 );
		damageSpeed.setSpeed( 0 );
	}
	
	
	//#if LOW_JAR == "false"
	public final Sprite getGearSprite() {
		return spriteGear;
	}
	//#endif
	
	
	public final UpdatableGroup getPistonsGroup() {
		return pistons;
	}
	
	
	public final Sprite getShiftLight() {
		return spriteShiftLight;
	}

	
	public final void onSequenceEnded( int id, int sequence ) {
		//#if LOW_JAR == "false"
		// n�o � necess�rio testar qual sequ�ncia foi encerrada, nem de que spriteGear, pois essa classe s� se registra como
		// listener de um spriteGear, e ele sempre p�ra ap�s encerrar uma sequ�ncia
		spriteGear.setSequence( GEAR_SEQUENCE_STOPPED );
		//#endif
	}

	
	/**
	 * Define o modo de acelera��o do motor.
	 * @param mode indica o modo de acelera��o do motor. Valores v�lidos:
	 * <ul>
	 * <li>ACCELERATION_ON: o motor est� sendo acelerado.</li>
	 * <li>ACCELERATION_OFF o motor n�o est� sendo acelerado.</li>
	 * <li>ACCELERATION_AUTOMATIC: o motor � acelerado automaticamente.</li>
	 * </ul>
	 */
	public final void setAcceleration( byte mode ) {
		switch ( mode ) {
			case ACCELERATION_STAGING:
				acceleration.setSpeed( 0 );
				speed = CAR_PRE_STAGING_SPEED;
			break;
			
			case ACCELERATION_BRAKING:
				if ( gear > GEAR_NEUTRAL )
					rotationBrakingGearDown = ( short ) ( ROTATION_MAX_TORQUE * GEAR_RATIO[ gear ] / GEAR_RATIO[ gear - 1 ] );			
			case ACCELERATION_ON:
			case ACCELERATION_OFF:
			case ACCELERATION_AUTOMATIC_ON:
			case ACCELERATION_AUTOMATIC_OFF:
			break;
			
			case ACCELERATION_WRECKED:
				acceleration.setSpeed( SPEED_CHANGE_WRECKED );
			break;
			
			case ACCELERATION_AUTOMATIC:
				mode = ACCELERATION_AUTOMATIC_ON;
			break;
			
			default:
				return;
		}
		accelerationMode = mode;
	}

	
	public final short getRotation() {
		return rotation;
	}
	
	
	private final void changeRotation( long diff ) {
		rotation += ( int ) diff;
		
		if ( gear == GEAR_NEUTRAL ) {
			if ( rotation > ROTATION_MAX )
				rotation = ROTATION_MAX;
			else if ( rotation < ROTATION_NEUTRAL )
				rotation = ROTATION_NEUTRAL;			
			else {
				if ( rotation < ROTATION_NEUTRAL_START_YELLOW_BELOW ) {
					spriteShiftLight.setVisible( false );
					
					if ( playScreen != null ) {
						if ( accelerationMode == ACCELERATION_OFF )
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
						else
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
					}
				} else {
					spriteShiftLight.setVisible( true );

					if ( rotation < ROTATION_NEUTRAL_START_GREEN )
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_YELLOW );
					else if ( rotation < ROTATION_NEUTRAL_START_YELLOW_ABOVE ) {
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_GREEN );
						
						if ( playScreen != null && accelerationMode != ACCELERATION_BRAKING )
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_UP );
					}
					else if ( rotation < ROTATION_NEUTRAL_START_RED )
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_YELLOW );
					else
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_RED );				
				}			
			}
		} else {
			if ( rotation > ROTATION_MAX )
				rotation = ROTATION_MAX;
			else if ( rotation < ROTATION_NEUTRAL )
				rotation = ROTATION_NEUTRAL;
			else if ( gear < TOTAL_GEARS - 1 ) {
				if ( rotation < ROTATION_START_YELLOW_BELOW ) {
					spriteShiftLight.setVisible( false );
					
					if ( playScreen != null ) {
						if ( accelerationMode == ACCELERATION_OFF )
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_ACCELERATE );
						else if ( gear > 1 && rotation <= ROTATION_GEAR_DOWN )
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_DOWN );
						else
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
					}
				} else {
					spriteShiftLight.setVisible( true );

					if ( rotation < ROTATION_START_GREEN )
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_YELLOW );
					else if ( rotation < ROTATION_START_YELLOW_ABOVE ) {
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_GREEN );
						
						if ( playScreen != null && accelerationMode != ACCELERATION_BRAKING )
							playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_UP );
					}
					else if ( rotation < ROTATION_START_RED )
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_YELLOW );
					else
						spriteShiftLight.setFrame( SHIFT_LIGHT_FRAME_RED );				
				}
			} else {
				spriteShiftLight.setVisible( false );
				
				if ( playScreen != null ) {
					 if ( accelerationMode != ACCELERATION_OFF && gear > 1 && rotation <= ROTATION_GEAR_DOWN )
						playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_GEAR_DOWN );
					 else
						playScreen.setIndicatorSequence( PLAYSCREEN_INDICATOR_NONE );
				}
			}
		}
	}

	
	private final void increaseDamage( int diff ) {
		damage += diff;
		
		int damagedPistons = 0;
		if ( damage >= DAMAGE_MAX ) {
			damage = DAMAGE_MAX;
			racer.setState( RACE_STATE_WRECKED );
			damagedPistons = PISTONS_TOTAL;
			rotationSpeed.setSpeed( ROTATION_SPEED_BLOWN, false );
		} else {
			damagedPistons = damage * PISTONS_TOTAL / DAMAGE_MAX;
		}
		
		for ( int i = 0; i < damagedPistons; ++i ) {
			( ( Sprite ) pistons.getDrawable( i ) ).setSequence( PISTONS_SEQUENCE_BROKEN );
		}			
	}
	
	
	public final void increaseSpeed( long diff ) {
		speed += ( int ) diff;

		if ( speed < 0 )
			speed = 0;
		else if ( speed > topSpeed )
			topSpeed = speed;
	}
	
	
	private final void resetPistons() {
		for ( int i = 0; i < PISTONS_TOTAL; ++i ) {
			final Sprite piston = ( Sprite ) pistons.getDrawable( i );
			piston.setSequence( PISTONS_SEQUENCE_MOVING );
			piston.setFrame( i & 1 );			
		}
	}

	
	public final int getTopSpeed() {
		return topSpeed;
	}

	
	public final void setPlayScreen( PlayScreen playScreen ) {
		this.playScreen = playScreen;
	}

	
	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}
}
