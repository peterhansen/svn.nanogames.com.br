/*
 * Message.java
 *
 * Created on October 18, 2007, 11:51 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Message extends DrawableGroup implements Constants, Updatable {
	
	private static final byte TOTAL_ITEMS = 5;
	
	// a mensagem � dividida em 2 partes
	private final RichLabel labelTop;
	private final RichLabel labelBottom;

	// patterns
	private final Pattern patternTop;
	private final Pattern patternFill;
	private final Pattern patternBottom;
	
	
	// estados de anima��o da mensagem
	private static final byte STATE_HIDDEN					= 0;
	private static final byte STATE_APPEARING				= 1;
	private static final byte STATE_OPENING					= 2;
	private static final byte STATE_TEXT_TOP_APPEARS_1		= 3;
	private static final byte STATE_TEXT_TOP_APPEARS_2		= 4;
	private static final byte STATE_TEXT_BOTTOM_APPEARS_1	= 5;
	private static final byte STATE_TEXT_BOTTOM_APPEARS_2	= 6;
	private static final byte STATE_SHOWN					= 7;
	private static final byte STATE_CLOSING					= 8;
	private static final byte STATE_HIDING					= 9;
	
	private byte state;
	
	private static final short TRANSITION_TIME = 333;
	
	private int accTime;
	
	private final MUV labelMUV = new MUV();
	
	private final int BORDERS_HEIGHT;
	
	private final int MESSAGE_TOTAL_HEIGHT;
	
	private final int MESSAGE_TEXT_SPEED_FAST;
	private final int MESSAGE_TEXT_SPEED_SLOW;
	
	private final int REF_PIXEL_Y;
	
	private final int DISTANCE_TO_SLOW = ScreenManager.SCREEN_WIDTH * MESSAGE_SCREEN_PERCENT_SLOW / 100;
	
	
	/** Creates a new instance of Message */
	public Message() throws Exception {
		super( TOTAL_ITEMS );
		
		// insere os patterns
		DrawableImage img = new DrawableImage( PATH_IMAGES + "pattern_top.png" );
		patternTop = new Pattern( img );
		patternTop.setSize( 0, img.getHeight() );
		
		img = new DrawableImage( PATH_IMAGES + "pattern_bottom.png" );
		patternBottom = new Pattern( img );
		patternBottom.setSize( 0, img.getHeight() );
		
		// calcula as dimens�es e velocidades de anima��o do texto
		MESSAGE_TOTAL_HEIGHT = GameMIDlet.getDefaultFont().getImage().getHeight() * MESSAGE_MAX_LINES;
		BORDERS_HEIGHT = patternTop.getHeight() + patternBottom.getHeight();
		REF_PIXEL_Y = ScreenManager.SCREEN_HALF_HEIGHT - ( MESSAGE_TOTAL_HEIGHT >> 1 );
		MESSAGE_TEXT_SPEED_FAST = MESSAGE_TEXT_SPEED_FAST_SCREEN_PERCENT * ScreenManager.SCREEN_WIDTH / 100;
		MESSAGE_TEXT_SPEED_SLOW = MESSAGE_TEXT_SPEED_SLOW_SCREEN_PERCENT * ScreenManager.SCREEN_WIDTH / 100;
		
		// insere a pattern de "transpar�ncia"
		img = new DrawableImage( PATH_IMAGES + "trans.png" );
		patternFill = new Pattern( img );
		patternFill.setPosition( 0, patternTop.getHeight() - ( patternTop.getHeight() & 1 ) );
		
		insertDrawable( patternFill );
		
		// insere os RichLabels que s�o preenchidos com o texto da mensagem no m�todo setText(String, String)
		labelTop = new RichLabel( GameMIDlet.getDefaultFont(), null, ScreenManager.SCREEN_WIDTH * MESSAGE_TEXT_MAX_WIDTH_PERCENT / 100, null );
		insertDrawable( labelTop );
		
		labelBottom = new RichLabel( GameMIDlet.getDefaultFont(), null, ScreenManager.SCREEN_WIDTH * MESSAGE_TEXT_MAX_WIDTH_PERCENT / 100, null );
		insertDrawable( labelBottom );		
		
		insertDrawable( patternTop );
		insertDrawable( patternBottom );		
		
		setState( STATE_HIDDEN );
	}

	
	public final void update( int delta ) {
		accTime += delta;
		
		int percent = accTime * 100 / TRANSITION_TIME;
		
		switch ( state ) {
			case STATE_HIDING:
				percent = 100 - percent;
			case STATE_APPEARING:
				final int width = ScreenManager.SCREEN_WIDTH * percent / 100;
				patternTop.setSize( width, patternTop.getHeight() );
				
				patternBottom.setSize( width, patternBottom.getHeight() );
				patternBottom.defineReferencePixel( patternBottom.getWidth(), patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );
				
				if ( accTime >= TRANSITION_TIME )
					setState( state == STATE_APPEARING ? STATE_OPENING : STATE_HIDDEN );
			break;
			
			case STATE_CLOSING:
				percent = 100 - percent;
			case STATE_OPENING:
				setSize( size.x, BORDERS_HEIGHT + ( MESSAGE_TOTAL_HEIGHT - BORDERS_HEIGHT ) * percent / 100 );
				
				if ( accTime >= TRANSITION_TIME )
					nextState();
			break;
			
			case STATE_TEXT_TOP_APPEARS_1:
				labelTop.move( labelMUV.updateInt( delta ), 0 );
				
				if ( labelTop.getRefPixelX() >= ScreenManager.SCREEN_HALF_WIDTH - DISTANCE_TO_SLOW )
					nextState();
			break;
			
			case STATE_TEXT_TOP_APPEARS_2:
				labelTop.move( labelMUV.updateInt( delta ), 0 );
				
				if ( labelTop.getRefPixelX() >= size.x >> 1 )
					nextState();
			break;			
			
			case STATE_TEXT_BOTTOM_APPEARS_1:
				labelBottom.move( labelMUV.updateInt( delta ), 0 );
				
				if ( labelBottom.getRefPixelX() <= ScreenManager.SCREEN_HALF_WIDTH + DISTANCE_TO_SLOW )
					nextState();				
			break;
			
			case STATE_TEXT_BOTTOM_APPEARS_2:
				labelBottom.move( labelMUV.updateInt( delta ), 0 );
				
				if ( labelBottom.getRefPixelX() <= size.x >> 1 )
					nextState();				
			break;			
			
			case STATE_SHOWN:
				labelTop.setRefPixelPosition( size.x >> 1, labelTop.getRefPixelY() );
				labelBottom.setRefPixelPosition( size.x >> 1, labelBottom.getRefPixelY() );
			break;
			
		}
	}
	
	
	private final void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
			break;
			
			case STATE_APPEARING:
				setSize( ScreenManager.SCREEN_WIDTH, BORDERS_HEIGHT );
				
				patternTop.setSize( 0, patternTop.getHeight() );
				
				patternBottom.setSize( 0, patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );
			break;
			
			case STATE_OPENING:
				setSize( size.x, BORDERS_HEIGHT );
				
				patternTop.setSize( size.x, patternTop.getHeight() );
				
				patternBottom.setSize( size.x, patternBottom.getHeight() );
				patternBottom.defineReferencePixel( patternBottom.getWidth(), patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );
			break;
			
			case STATE_CLOSING:
			break;
			
			case STATE_TEXT_TOP_APPEARS_1:
				setSize( size.x, MESSAGE_TOTAL_HEIGHT );
				
				labelTop.setPosition( -labelTop.getWidth(), labelTop.getPosition().y );
				
				labelMUV.setSpeed( MESSAGE_TEXT_SPEED_FAST );
			break;
			
			case STATE_TEXT_TOP_APPEARS_2:
				labelTop.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH -DISTANCE_TO_SLOW, labelTop.getRefPixelY() );
				
				labelMUV.setSpeed( MESSAGE_TEXT_SPEED_SLOW );
			break;			
			
			case STATE_TEXT_BOTTOM_APPEARS_1:
				labelTop.setRefPixelPosition( size.x >> 1, labelTop.getRefPixelY() );
				
				labelBottom.setRefPixelPosition( size.x + ( size.x >> 1 ), labelTop.getRefPixelY() );
				
				labelMUV.setSpeed( -MESSAGE_TEXT_SPEED_FAST );
			break;
			
			case STATE_TEXT_BOTTOM_APPEARS_2:
				labelBottom.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH + DISTANCE_TO_SLOW, labelTop.getRefPixelY() );
				
				labelMUV.setSpeed( -MESSAGE_TEXT_SPEED_SLOW );
			break;			
			
			case STATE_SHOWN:
				labelBottom.setRefPixelPosition( size.x >> 1, labelBottom.getRefPixelY() );
			break;
			
			case STATE_HIDING:
				setSize( size.x, BORDERS_HEIGHT );
				
				labelTop.setVisible( false );
				labelBottom.setVisible( false );
			break;
			
			default:
				return;
		
		}		

		setVisible( state != STATE_HIDDEN );
		accTime = 0;
		this.state = ( byte ) state;
	} // fim do m�todo setState( int )
	
	
	private final void nextState() {
		switch ( state ) {
			case STATE_HIDING:
				setState( STATE_HIDDEN );
			case STATE_HIDDEN:
			break;
				
			case STATE_APPEARING:
			case STATE_OPENING:
			case STATE_CLOSING:
			case STATE_TEXT_TOP_APPEARS_1:
			case STATE_TEXT_TOP_APPEARS_2:
			case STATE_TEXT_BOTTOM_APPEARS_1:
			case STATE_TEXT_BOTTOM_APPEARS_2:
			case STATE_SHOWN:
				setState( state + 1 );
		}
	}
	

	/**
	 * Avan�a para o pr�ximo est�gio de anima��o.
	 * @return boolean indicando se a mensagem foi exibida, ou seja, se 
	 */	
	public final boolean skip() {
		switch ( state ) {
			case STATE_CLOSING:
			case STATE_HIDING:
				setState( STATE_HIDDEN );
			case STATE_HIDDEN:
				return true;
				
			case STATE_APPEARING:
				setState( state + 1 );
			case STATE_OPENING:
				setState( state + 1 );
			case STATE_TEXT_TOP_APPEARS_1:
				setState( state + 1 );
			case STATE_TEXT_TOP_APPEARS_2:
				setState( state + 1 );
			case STATE_TEXT_BOTTOM_APPEARS_1:
				setState( state + 1 );
			case STATE_TEXT_BOTTOM_APPEARS_2:
				setState( state + 1 );
				return false;
			
			case STATE_SHOWN:
				setState( state + 1 );
			default:
				return false;
		}		
	}
	
	
	public final void setText( String textTop, String textBottom ) {
		final String tagHCenter = RichLabel.TAG_START + RichLabel.TAG_ALIGN + RichLabel.ALIGN_HCENTER + RichLabel.TAG_END;
		
		labelTop.setText( tagHCenter + textTop );
		labelTop.setVisible( true );
		labelTop.setSize( labelTop.getWidth(), labelTop.getTextTotalHeight() );
		labelTop.defineReferencePixel( labelTop.getWidth() >> 1, labelTop.getHeight() );
		labelTop.setRefPixelPosition( -labelTop.getWidth() >> 1, MESSAGE_TOTAL_HEIGHT >> 1 );
		
		labelBottom.setText( tagHCenter + textBottom );
		labelBottom.setVisible( true );
		labelBottom.setSize( labelBottom.getWidth(), labelBottom.getTextTotalHeight() );
		labelBottom.defineReferencePixel( labelBottom.getWidth() >> 1, 0 );
		labelBottom.setRefPixelPosition( -labelBottom.getWidth() >> 1, MESSAGE_TOTAL_HEIGHT >> 1 );
		
		setState( STATE_APPEARING );
	}

	
	public final void setSize( int width, int height ) {
		if ( height < BORDERS_HEIGHT )
			height = BORDERS_HEIGHT;
		
		super.setSize( width, height );
		
		patternFill.setSize( size.x, height - BORDERS_HEIGHT + ( ( height - BORDERS_HEIGHT ) & 1 ) );
		patternBottom.setRefPixelPosition( size );
		
		labelTop.setRefPixelPosition( labelTop.getRefPixelX(), size.y >> 1 );
		labelBottom.setRefPixelPosition( labelBottom.getRefPixelX(), size.y >> 1 );
		
		final Point previousRefPoint = new Point( getRefPixelPosition() );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( previousRefPoint );
	}
	
}
