/*
 * RearTires.java
 *
 * Created on October 29, 2007, 7:13 PM
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.ImageLoader;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;
import screens.LoadScreen;

/**
 *
 * @author peter
 */
public final class RearTires extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 2;
	
	//#if SCREEN_SIZE == "MEDIUM_BIG"
		private static final byte TOTAL_FRAMES = 10;
		private static final byte[] FRAMES_LOW_MEMORY = { 3, 5, 6, 8 };
	//#else
		//#if LOW_JAR == "false"
//# 		private static final byte TOTAL_FRAMES = 8;
		//#else
//# 		private static final byte TOTAL_FRAMES = 3;
		//#endif
	//#endif
	
	public static final byte SEQUENCE_WIDE		= 0;
	public static final byte SEQUENCE_MEDIUM	= 1;
	public static final byte SEQUENCE_NARROW	= 2;
	
	private static Sprite frameSet;
	
	private final RearTire left;
	private final RearTire right;
	
	private final byte offset;
	
	
	/** Creates a new instance of RearTires
	 * @param offset
	 * @throws java.lang.Exception 
	 */
	public RearTires( int offset ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.offset = ( byte ) offset;
		
		setSize( REAR_TIRES_WIDTH, REAR_TIRES_HEIGHT );
		
		left = new RearTire();
		left.defineReferencePixel( left.getWidth(), 0 );
		insertDrawable( left );
		
		right = new RearTire();
		insertDrawable( right );
		
		defineReferencePixel( size.x >> 1, 0 );
		setSequence( SEQUENCE_MEDIUM );
		setSequence( SEQUENCE_WIDE );
	}
	
	
	public static final void loadFrameSet( LoadScreen load ) throws Exception {
		if ( frameSet == null ) {
			final short[] frameTimes = new short[] { 0, 0, 0 };
			
			//#if SCREEN_SIZE == "MEDIUM_BIG"
			
				if ( GameMIDlet.isLowMemory() ) {
					final Image[] frames = new Image[ FRAMES_LOW_MEMORY.length ];
					for ( int i = 0; i < frames.length; ++i ) {
						frames[ i ] = ImageLoader.loadImage( PATH_TIRES + "rear_" + FRAMES_LOW_MEMORY[ i ] + ".png" );
						load.changeProgress( 1 );
					}
					
					final byte[][] sequences = new byte[][] {
						{ 0, 1 },
						{ 2 },
						{ 3 },
					};
					
					frameSet = new Sprite( frames, sequences, frameTimes );
					
					load.changeProgress( 1 );
				} else {
					final Image[] frames = new Image[ TOTAL_FRAMES ];
					for ( int i = 0; i < TOTAL_FRAMES; ++i ) {
						frames[ i ] = ImageLoader.loadImage( PATH_TIRES + "rear_" + i + ".png" );
						load.changeProgress( 1 );
					}
					
					final byte[][] sequences = new byte[][] {
						{ 0, 1, 2, 3, 4, 5 },
						{ 6, 7 },
						{ 8, 9 },
					};
					
					frameSet = new Sprite( frames, sequences, frameTimes );

					load.changeProgress( 1 );
				}
			
			//#else
				//#if LOW_JAR == "false"
//# 			
//# 					final Image[] frames = new Image[ TOTAL_FRAMES ];
//# 					for ( int i = 0; i < TOTAL_FRAMES; ++i ) {
//# 						frames[ i ] = ImageLoader.loadImage( PATH_TIRES + "rear_" + i + ".png" );
//# 						load.changeProgress( 1 );
//# 					}
//# 
//# 					final byte[][] sequences = new byte[][] {
//# 						{ 0, 1, 2, 3 },
//# 						{ 4, 5 },
//# 						{ 6, 7 }
//# 					};
//# 					
				//#else
//# 					
//# 					final Image[] frames = new Image[ TOTAL_FRAMES ];
//# 					for ( int i = 0; i < TOTAL_FRAMES; ++i ) {
//# 						frames[ i ] = ImageLoader.loadImage( PATH_TIRES + "rear_" + i + ".png" );
//# 						load.changeProgress( 1 );
//# 					}
//# 
//# 					final byte[][] sequences = new byte[][] {
//# 						{ 0 },
//# 						{ 1 },
//# 						{ 2 }
//# 					};
//# 					
				//#endif
//# 
//# 				frameSet = new Sprite( frames, sequences, frameTimes );
//# 
//# 				load.changeProgress( 1 );
			//#endif
		}
	}
	
	
	private static final class RearTire extends Sprite {
		
		private RearTire() {
			super( RearTires.frameSet );
		}


		public final void setSequence( int sequence ) {
			super.setSequence( sequence );
			defineReferencePixel( frame.getWidth(), 0 );
		}		
	}
	
	
	public final void setSequence( int sequence ) {
		if ( sequence != left.getSequence() ) {
			left.setSequence( sequence );
			left.setRefPixelPosition( ( size.x >> 1 ) + REAR_TIRE_LEFT_X_OFFSET + offset, 0 );
			
			right.setSequence( sequence );
			right.setPosition( ( size.x >> 1 ) + REAR_TIRE_RIGHT_X_OFFSET + offset, 0 );
		}
	}
	
	
	public final void nextFrame() {
		left.nextFrame();
		right.nextFrame();
	}
	
}
