/*
 * Message.java
 *
 * Created on October 18, 2007, 11:51 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import screens.GameMIDlet;


/**
 *
 * @author peter
 */
public final class TutorialMessage extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 5;
	
	private final RichLabel text;

	// patterns
	private final Pattern patternTop;
	private final Pattern patternFill;
	private final Pattern patternBottom;
	
	
	// estados de anima��o da mensagem
	private static final byte STATE_HIDDEN					= 0;
	private static final byte STATE_APPEARING				= 1;
	private static final byte STATE_OPENING					= 2;
	private static final byte STATE_SHOWN					= 3;
	private static final byte STATE_CLOSING					= 4;
	private static final byte STATE_HIDING					= 5;
	
	private byte state;
	
	private static final short TRANSITION_TIME = 333;
	
	private static final short MIN_SHOWN_TIME = TRANSITION_TIME << 1; // dem�in!
	
	private int accTime;
	
	private final int BORDERS_HEIGHT;
	
	private int textScrollLimit;
	
	private final MUV scrollMUV = new MUV();
	
	private final int MESSAGE_TOTAL_HEIGHT;

	/** tempo de espera em milisegundos quando o scroll do texto atinge o limite inferior ou superior */
	private static final short TEXT_WAIT_TIME = 800;
	
	/** tempo inicial de espera para que o scroll comece */
	private static final short TEXT_WAIT_TIME_INITIAL = TEXT_WAIT_TIME << 1;
	
	private int accWaitTime;
	
	
	/** Creates a new instance of Message */
	public TutorialMessage() throws Exception {
		super( TOTAL_ITEMS );
		
		// insere os patterns
		DrawableImage img = new DrawableImage( PATH_IMAGES + "pattern_top.png" );
		patternTop = new Pattern( img );
		patternTop.setSize( 0, img.getHeight() );
		
		img = new DrawableImage( PATH_IMAGES + "pattern_bottom.png" );
		patternBottom = new Pattern( img );
		patternBottom.setSize( 0, img.getHeight() );
		
		BORDERS_HEIGHT = patternTop.getHeight() + patternBottom.getHeight();
		MESSAGE_TOTAL_HEIGHT = GameMIDlet.getDefaultFont().getImage().getHeight() * MESSAGE_MAX_LINES;
		
		// insere a pattern de "transpar�ncia"
		img = new DrawableImage( PATH_IMAGES + "trans.png" );
		patternFill = new Pattern( img );
		patternFill.setPosition( 0, patternTop.getHeight() - ( patternTop.getHeight() & 1 ) );
		
		insertDrawable( patternFill );
		
		// insere o richLabel que formatar� o texto
		text = new RichLabel( GameMIDlet.getDefaultFont(), null, ScreenManager.SCREEN_WIDTH, null );
		text.setPosition( 0, BORDERS_HEIGHT >> 1 );
		text.setSize( ScreenManager.SCREEN_WIDTH, MESSAGE_TOTAL_HEIGHT - ( BORDERS_HEIGHT >> 1 ) );
		insertDrawable( text );
		
		insertDrawable( patternTop );
		insertDrawable( patternBottom );		
		
		setState( STATE_HIDDEN );
	}

	
	public final void update( int delta ) {
		accTime += delta;
		
		int percent = accTime * 100 / TRANSITION_TIME;
		
		switch ( state ) {
			case STATE_HIDING:
				percent = 100 - percent;
			case STATE_APPEARING:
				final int width = ScreenManager.SCREEN_WIDTH * percent / 100;
				patternTop.setSize( width, patternTop.getHeight() );
				
				patternBottom.setSize( width, patternBottom.getHeight() );
				patternBottom.defineReferencePixel( patternBottom.getWidth(), patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );
				
				if ( accTime >= TRANSITION_TIME )
					setState( state == STATE_APPEARING ? STATE_OPENING : STATE_HIDDEN );
			break;
			
			case STATE_CLOSING:
				percent = 100 - percent;
			case STATE_OPENING:
				setSize( size.x, BORDERS_HEIGHT + ( MESSAGE_TOTAL_HEIGHT - BORDERS_HEIGHT ) * percent / 100 );
				
				if ( accTime >= TRANSITION_TIME )
					nextState();
			break;
			
			case STATE_SHOWN:
				text.setTextOffset( text.getTextOffset() + scrollMUV.updateInt( delta ) );
				
				if ( text.getTextOffset() >= 0 || text.getTextOffset() <= textScrollLimit ) {
					if ( text.getTextOffset() >= 0 )
						text.setTextOffset( 0 );
					else
						text.setTextOffset( textScrollLimit );
					
					accWaitTime -= delta;
					if ( accWaitTime <= 0 ) {
						scrollMUV.setSpeed( -scrollMUV.getSpeed() );
						accWaitTime = TEXT_WAIT_TIME;
					}
				}
			break;
		}
	}
	
	
	private final void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
			break;
			
			case STATE_APPEARING:
				text.setVisible( true );
				
				patternFill.setVisible( false );
				text.setVisible( false );
				
				setSize( ScreenManager.SCREEN_WIDTH, BORDERS_HEIGHT );
				
				patternTop.setSize( 0, patternTop.getHeight() );
				
				patternBottom.setSize( 0, patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );
			break;
			
			case STATE_OPENING:
				setSize( size.x, BORDERS_HEIGHT );
				
				patternTop.setSize( size.x, patternTop.getHeight() );
				
				text.setVisible( true );
				patternFill.setVisible( true );
				
				patternBottom.setSize( size.x, patternBottom.getHeight() );
				patternBottom.defineReferencePixel( patternBottom.getWidth(), patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );
			break;
			
			case STATE_SHOWN:
				setSize( ScreenManager.SCREEN_WIDTH, MESSAGE_TOTAL_HEIGHT );
				
				patternTop.setSize( size.x, patternTop.getHeight() );
				
				patternBottom.setSize( size.x, patternBottom.getHeight() );
				patternBottom.defineReferencePixel( patternBottom.getWidth(), patternBottom.getHeight() );
				patternBottom.setRefPixelPosition( size );				
			break;
			
			case STATE_CLOSING:
			break;
			
			case STATE_HIDING:
				setSize( size.x, BORDERS_HEIGHT );
				text.setVisible( false );
			break;
			
			default:
				return;
		
		}		

		setVisible( state != STATE_HIDDEN );
		accTime = 0;
		this.state = ( byte ) state;
	} // fim do m�todo setState( int )
	
	
	private final void nextState() {
		switch ( state ) {
			case STATE_HIDING:
				setState( STATE_HIDDEN );
			case STATE_HIDDEN:
			break;
				
			case STATE_APPEARING:
			case STATE_OPENING:
			case STATE_CLOSING:
			case STATE_SHOWN:
				setState( state + 1 );
		}
	}
	

	/**
	 * Avan�a para o pr�ximo est�gio de anima��o.
	 * @return 
	 */	
	public final boolean skip() {
		switch ( state ) {
			case STATE_CLOSING:
			case STATE_HIDING:
				setState( STATE_HIDDEN );
			case STATE_HIDDEN:
				return true;
				
			case STATE_APPEARING:
				setState( state + 1 );
			case STATE_OPENING:
				setState( state + 1 );
				return false;
			
			case STATE_SHOWN:
				if ( accTime >= MIN_SHOWN_TIME )
					setState( state + 1 );
				else
					return false;
				
			default:
				return true;
		}		
	}
	
	
	public final void setText( byte textIndex ) {
		text.setText( GameMIDlet.getText( textIndex ) );
		
		final int textHeight = text.getTextTotalHeight();
		
		if ( textHeight <= ( MESSAGE_TEXT_LINES * text.getFont().getImage().getHeight() ) ) {
			scrollMUV.setSpeed( 0 );
			textScrollLimit = textHeight;
		} else {
			scrollMUV.setSpeed( MESSAGE_SCROLL_LINES_PERCENT_PER_SECOND * text.getFont().getImage().getHeight() / 100 );
			textScrollLimit = ( MESSAGE_TEXT_LINES * text.getFont().getImage().getHeight() ) - textHeight;
		}

		accWaitTime = TEXT_WAIT_TIME_INITIAL;
		text.setTextOffset( 0 );
		
		setState( STATE_APPEARING );
	}

	
	public final void setSize( int width, int height ) {
		if ( height < BORDERS_HEIGHT )
			height = BORDERS_HEIGHT;
		
		super.setSize( width, height );
		
		patternFill.setSize( size.x, height - BORDERS_HEIGHT + ( ( height - BORDERS_HEIGHT ) & 1 ) );
		patternBottom.setRefPixelPosition( size );
		
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			if ( ScreenManager.getInstance().hasPointerEvents() && ScreenManager.SCREEN_HEIGHT > 280 )
				setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT >> 1 );
			else
				setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		//#else
//# 			setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		//#endif
	}
	
}
