/*
 * Car.java
 *
 * Created on October 30, 2007, 10:27 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import screens.GameMIDlet;
import screens.LoadScreen;

/**
 *
 * @author peter
 */
public final class Car extends UpdatableGroup implements Constants, Serializable, SpriteListener {
	
	private static final byte TOTAL_ITEMS = 15;
	
	/** carroceria */
	protected DrawableImage body;
	
	/** p�ra-quedas */
	protected final Parachutes parachutes;
	
	protected final Sprite frontTires;
	
	protected final RearTires rearTires;
	
	protected final DrawableImage engine1;
	protected final DrawableImage engine2;

	//#if LOW_JAR == "false"
	// sprites do fogo dos motores
	protected final Sprite[] fireEngine1;
	protected final Sprite[] fireEngine2;
	//#endif
	
	private static final byte FIRE_LEFT		= 0;
	private static final byte FIRE_RIGHT	= 1;
	
	public static final byte FIRE_SEQUENCE_IDLE				= 0;
	public static final byte FIRE_SEQUENCE_INVISIBLE		= 1;
	public static final byte FIRE_SEQUENCE_EXPELL_THEN_IDLE	= 2;
	public static final byte FIRE_SEQUENCE_EXPELL_THEN_OFF	= 3;
	
	//#if LOW_JAR == "false"
	protected final Smoke smoke;
	//#endif
	
	/** informa��es dos carros */
	private static final int[][] information = new int[ CHARACTERS_TOTAL ][ CAR_TOTAL_VALUES ];
	private static boolean informationLoaded;
	
	private int frontTiresAccDx;
	private int rearTiresAccDx;
	
	// vari�veis utilizadas para realizar a anima��o do motor
	private boolean engineOn;
	private int engineAccTime;
	
	private static final byte ENGINE_ANIMATION_RATE = 12;
	private static final byte ENGINE_ANIMATION_1_LEFT = 0;
	private static final byte ENGINE_ANIMATION_2_LEFT = 12 >> 2;
	private static final byte ENGINE_ANIMATION_1_RIGHT = ENGINE_ANIMATION_RATE >> 1;
	private static final byte ENGINE_ANIMATION_2_RIGHT = ( ENGINE_ANIMATION_RATE * 3 ) >> 2;
	
	
	private final byte index;
	
	private static Sprite fireSet;
	private static Sprite frontTiresSet;
	
	
	/** Creates a new instance of Car
	 * @param index
	 * @throws java.lang.Exception 
	 */
	public Car( byte index ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.index = index;
		
		// carrega das informa��es do carro
		if ( !informationLoaded )
			GameMIDlet.openJarFile( PATH_CARS, this );
		
		final int[] info = information[ index ];

		// pneus dianteiros
		// insere os pneus dianteiros
		frontTires = new Sprite( frontTiresSet );
		frontTires.defineReferencePixel( frontTires.getWidth() >> 1, 0 );
		insertDrawable( frontTires );
		
		// carroceria
		body = new DrawableImage( PATH_CAR + info[ CAR_INFO_BODY_TYPE ] + ".png" );
		body.defineReferencePixel( body.getWidth() >> 1, 0 );
		insertDrawable( body );
		
		// define o tamanho do carro
		setSize( PLAYSCREEN_TRACK_WIDTH >> 1, body.getHeight() + PARACHUTES_HEIGHT );		
		
		frontTires.setRefPixelPosition( size.x >> 1, info[ CAR_INFO_FRONT_TIRES_POSITION ] );
		body.setRefPixelPosition( size.x >> 1, 0 );
		
		// pneus traseiros
		rearTires = new RearTires( ( body.getWidth() & 1 ) - 1 );
		rearTires.defineReferencePixel( rearTires.getWidth() >> 1, 0 );
		rearTires.setRefPixelPosition( ( size.x >> 1 ), info[ CAR_INFO_REAR_TIRES_POSITION ] );
		insertDrawable( rearTires );
		
		// p�ra-quedas
		parachutes = new Parachutes();
		parachutes.setRefPixelPosition( body.getRefPixelX(), body.getHeight() );
		insertDrawable( parachutes );
		
		//#if LOW_JAR == "false"
		if ( GameMIDlet.isLowMemory() ) {
			smoke = null;
		} else {
			// insere a fuma�a dos pneus traseiros
			smoke = new Smoke();
			smoke.setRefPixelPosition( rearTires.getRefPixelPosition() );
			insertDrawable( smoke );		
		} 
		//#endif
		
		// aerof�lio (caso exista)
		final Point aeroSize = new Point();
		DrawableImage aero = null;
		if ( info[ CAR_INFO_AERO_TYPE ] >= 0 ) {
			aero = new DrawableImage( PATH_CAR + info[ CAR_INFO_AERO_TYPE ] + ".png" );
			aero.defineReferencePixel( aero.getWidth() >> 1, 0 );
			aero.setRefPixelPosition( size.x >> 1, info[ CAR_INFO_AERO_POSITION ] );
			
			aeroSize.set( aero.getSize() );
			
			insertDrawable( aero );
		}
		
		// primeiro motor (obrigat�rio)
		engine1 = new DrawableImage( PATH_CAR + info[ CAR_INFO_ENGINE_1_TYPE ] + ".png" );
		engine1.defineReferencePixel( engine1.getWidth() >> 1, 0 );
		engine1.setRefPixelPosition( size.x >> 1, info[ CAR_INFO_ENGINE_1_POSITION ] );

		insertDrawable( engine1 );
		
		//#if LOW_JAR == "false"
		// insere a anima��o do fogo do primeiro motor
		Sprite fire;
		if ( !GameMIDlet.isLowMemory() ) {
			fire = new Sprite( fireSet );
			
			fire.setListener( this, 0 );
			fireEngine1 = new Sprite[ 2 ];
			fireEngine1[ FIRE_LEFT ] = fire;
			fire.defineReferencePixel( fire.getWidth(), 0 );
			fire.setRefPixelPosition( engine1.getPosition().x, engine1.getPosition().y + info[ CAR_INFO_ENGINE_1_FIRE_POSITION ] );
			fire.mirror( TRANS_MIRROR_H );
			if ( info[ CAR_INFO_ENGINE_1_FIRE_TYPE ] == FIRE_TYPE_DOWN )
				fire.mirror( TRANS_MIRROR_V );
			insertDrawable( fire );

			fire = new Sprite( fireSet );
			fire.setListener( this, 0 );
			fireEngine1[ FIRE_RIGHT ] = fire;
			fire.setPosition( engine1.getPosition().x + engine1.getWidth(), engine1.getPosition().y + info[ CAR_INFO_ENGINE_1_FIRE_POSITION ] );
			if ( info[ CAR_INFO_ENGINE_1_FIRE_TYPE ] == FIRE_TYPE_DOWN )
				fire.mirror( TRANS_MIRROR_V );		
			insertDrawable( fire );
		} else {
			fireEngine1 = null;
		}
		//#endif
		
		// segundo motor (pode n�o existir)
		if ( info[ CAR_INFO_ENGINE_2_TYPE ] >= 0 ) {
			engine2 = new DrawableImage( PATH_CAR + info[ CAR_INFO_ENGINE_2_TYPE ] + ".png" );
			engine2.defineReferencePixel( engine2.getWidth() >> 1, 0 );
			engine2.setRefPixelPosition( size.x >> 1, info[ CAR_INFO_ENGINE_2_POSITION ] );

			insertDrawable( engine2 );
			
			//#if LOW_JAR == "false"
			if ( !GameMIDlet.isLowMemory() ) {
				fire = new Sprite( fireSet );
				
				fire.setListener( this, 0 );
				fireEngine2 = new Sprite[ 2 ];
				fireEngine2[ FIRE_LEFT ] = fire;
				fire.defineReferencePixel( fire.getWidth(), 0 );
				fire.setRefPixelPosition( engine2.getPosition().x, engine2.getPosition().y + info[ CAR_INFO_ENGINE_2_FIRE_POSITION ] );
				if ( info[ CAR_INFO_ENGINE_2_FIRE_TYPE ] == FIRE_TYPE_DOWN )
					fire.mirror( TRANS_MIRROR_V );			
				fire.mirror( TRANS_MIRROR_H );
				insertDrawable( fire );

				fire = new Sprite( fireSet );
				fire.setListener( this, 0 );
				fireEngine2[ FIRE_RIGHT ] = fire;
				fire.setPosition( engine2.getPosition().x + engine2.getWidth(), engine2.getPosition().y + info[ CAR_INFO_ENGINE_2_FIRE_POSITION ] );
				if ( info[ CAR_INFO_ENGINE_2_FIRE_TYPE ] == FIRE_TYPE_DOWN )
					fire.mirror( TRANS_MIRROR_V );			
				insertDrawable( fire );
			} else {
				fireEngine2 = null;
			}
			//#endif
		} else {
			engine2 = null;
			
			//#if LOW_JAR == "false"
			fireEngine2 = null;
			//#endif
		}
	}
	

	public final void write( DataOutputStream output ) throws Exception {
	}

	
	public final void read( DataInputStream input ) throws Exception {
		// estado atual de leitura das informa��es do carro
		byte readState = CAR_INFO_BODY_TYPE;
		
		final StringBuffer buffer = new StringBuffer();
		
		// linha come�a em 1 pois h� o corredor "dummy"
		int currentIndex = 1;
		boolean negative = false;
		
		while ( currentIndex < CHARACTERS_TOTAL ) {
			final char c = ( char ) input.readUnsignedByte();
			
			switch ( c ) {
				case 'x':
				case '#':
					buffer.append( "-1" );
				break;
				
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					buffer.append( c );
				break;
				
				case '-':
					negative = true;
				break;
				
				default:
					if ( buffer.length() > 0 ) {
						switch ( readState ) {
							case CAR_INFO_BODY_TYPE:
							case CAR_INFO_AERO_TYPE:
							case CAR_INFO_AERO_POSITION:
							case CAR_INFO_ENGINE_1_TYPE:
							case CAR_INFO_ENGINE_1_POSITION:
							case CAR_INFO_ENGINE_1_FIRE_TYPE:
							case CAR_INFO_ENGINE_1_FIRE_POSITION:
							case CAR_INFO_ENGINE_2_TYPE:
							case CAR_INFO_ENGINE_2_POSITION:
							case CAR_INFO_ENGINE_2_FIRE_TYPE:
							case CAR_INFO_ENGINE_2_FIRE_POSITION:								
							case CAR_INFO_FRONT_TIRES_POSITION:
							case CAR_INFO_REAR_TIRES_POSITION:
								information[currentIndex ][ readState ] = negative ? -Integer.valueOf( buffer.toString() ).intValue() : Integer.valueOf( buffer.toString() ).intValue();
								++readState;
								
								if ( readState == CAR_INFO_DONE ) {
									++currentIndex;
									readState = CAR_INFO_BODY_TYPE;									
								}
							break;
						}
						
						buffer.delete( 0, buffer.length() );
						negative = false;
					}
				// fim default
			} // fim switch ( c )
		} // fim while ( currentIndex < CHARACTERS_TOTAL )
		informationLoaded = true;
	} // fim do m�todo read( DataInputStream )


	public final void updateTires( int realDx ) {
		frontTiresAccDx += realDx;
		if( frontTiresAccDx >= TIRES_FRONT_POSITION_CHANGE_FRAME ) {
			frontTiresAccDx %= TIRES_FRONT_POSITION_CHANGE_FRAME;
			frontTires.nextFrame();
		}		
		
		rearTiresAccDx += realDx;
		if( rearTiresAccDx >= TIRES_REAR_POSITION_CHANGE_FRAME ) {
			rearTiresAccDx %= TIRES_REAR_POSITION_CHANGE_FRAME;
			rearTires.nextFrame();
		}
	}

	
	protected final void reset() {
		frontTiresAccDx = 0;
		
		rearTires.setSequence( RearTires.SEQUENCE_WIDE );
		rearTiresAccDx = 0;
		
		parachutes.setSequence( Parachutes.SEQUENCE_NONE );
	}


	public final void update( int delta ) {
		super.update( delta );

		if ( engineOn ) {
			switch ( engineAccTime % ENGINE_ANIMATION_RATE ) {
				case ENGINE_ANIMATION_1_LEFT:
					engine1.setRefPixelPosition( ( size.x >> 1 ) - 1, engine1.getRefPixelY() );
				break;
				
				case ENGINE_ANIMATION_2_LEFT:
					if ( engine2 != null )
						engine2.setRefPixelPosition( ( size.x >> 1 ) - 1, engine2.getRefPixelY() );
				break;				
				
				case ENGINE_ANIMATION_1_RIGHT:
					engine1.setRefPixelPosition( ( size.x >> 1 ) + 1, engine1.getRefPixelY() );
				break;
				
				case ENGINE_ANIMATION_2_RIGHT:
					if ( engine2 != null )
						engine2.setRefPixelPosition( ( size.x >> 1 ) + 1, engine2.getRefPixelY() );
				break;
				
				default:
					engine1.setRefPixelPosition( ( size.x >> 1 ), engine1.getRefPixelY() );
					if ( engine2 != null )
						engine2.setRefPixelPosition( ( size.x >> 1 ), engine2.getRefPixelY() );
			}
			++engineAccTime;
			if ( engineAccTime < 0 )
				engineAccTime = 0;
		}
	}
	
	
	public final void turnEngineOn( boolean on ) {
		engineOn = on;
		
		if ( !on ) {
			//#if LOW_JAR == "false"
			setFireSequence( FIRE_SEQUENCE_EXPELL_THEN_OFF );
			//#endif
			
			engine1.setRefPixelPosition( size.x >> 1, engine1.getRefPixelY() );
			
			if ( engine2 != null )
				engine2.setRefPixelPosition( size.x >> 1, engine2.getRefPixelY() );
		}
	}
	
	
 	public final void onSequenceEnded( int id, int sequence ) {
	//#if LOW_JAR == "false"
		switch ( sequence ) {
			case FIRE_SEQUENCE_EXPELL_THEN_OFF:
				setFireSequence( FIRE_SEQUENCE_INVISIBLE );
			break;
				
			case FIRE_SEQUENCE_EXPELL_THEN_IDLE:
				setFireSequence( FIRE_SEQUENCE_IDLE );
			break;
		}
	//#endif
 	}


	//#if LOW_JAR == "false"

	public final void setFireSequence( byte sequence ) {
		if ( !GameMIDlet.isLowMemory() && fireEngine1[ FIRE_LEFT ].getSequence() != sequence ) {
			fireEngine1[ FIRE_LEFT ].setSequence( sequence );
			fireEngine1[ FIRE_LEFT ].setVisible( sequence != FIRE_SEQUENCE_INVISIBLE );
			fireEngine1[ FIRE_RIGHT ].setSequence( sequence );
			fireEngine1[ FIRE_RIGHT ].setVisible( sequence != FIRE_SEQUENCE_INVISIBLE );

			if ( fireEngine2 != null ) {
				fireEngine2[ FIRE_LEFT ].setSequence( sequence );
				fireEngine2[ FIRE_LEFT ].setVisible( sequence != FIRE_SEQUENCE_INVISIBLE );
				fireEngine2[ FIRE_RIGHT ].setSequence( sequence );			
				fireEngine2[ FIRE_RIGHT ].setVisible( sequence != FIRE_SEQUENCE_INVISIBLE );
			}
		}
	}
	//#endif
	
	
	public static final void loadFrameSet( LoadScreen load ) throws Exception {
		frontTiresSet = new Sprite( PATH_TIRES + "front.dat", PATH_TIRES + "front" );
		load.changeProgress( 1 );
		
		//#if LOW_JAR == "false"
		if ( !GameMIDlet.isLowMemory() ) {
			// aloca as imagens do fogo do motor
			fireSet = new Sprite( PATH_IMAGES + "fire.dat", PATH_IMAGES + "fire" );
			
			load.changeProgress( 3 );
		} else {
			load.changeProgress( 8 );
		}
		//#else
//# 		load.changeProgress( 8 );
		//#endif
	}

	
	public final int getBodyHeight() {
		return body.getHeight();
	}

	
	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}
	
}
