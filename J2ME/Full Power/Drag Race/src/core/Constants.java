/*
 * Constants.java
 *
 * Created on October 3, 2007, 11:47 AM
 *
 */

package core;

/**
 *
 * @author peter
 */
public interface Constants {
	
	//////////////////////////////////////////////
	// DEFINI��ES INDEPENDENTES DO TAMANHO DE TELA
    
    // caminhos dos recursos
	public static final String PATH_IMAGES				= "/";
	public static final String PATH_BOARD				= PATH_IMAGES + "board/";
	public static final String PATH_TREE_SMALL			= PATH_IMAGES + "tree/small_";
	public static final String PATH_TREE_BIG			= PATH_IMAGES + "tree/big_";
	public static final String PATH_CHARACTERS			= PATH_IMAGES + "characters/";
	public static final String PATH_TUTORIAL			= PATH_IMAGES + "tutorial/";
	public static final String PATH_SPLASH				= PATH_IMAGES + "splash/";
	public static final String PATH_PARACHUTES			= PATH_IMAGES + "parachutes/";
	public static final String PATH_CURSOR_IMG			= PATH_IMAGES + "cursor_0.png";
	public static final String PATH_TIRES				= PATH_IMAGES + "tires/";
	public static final String PATH_CAR					= PATH_IMAGES + "cars/";
    public static final String PATH_SOUNDS				= "/";
	public static final String PATH_TEXTS				= "/";
 	public static final String PATH_CARS				= "/cars.dat";
	
	// <editor-fold desc="�ndices dos sons">
	public static final byte SOUND_SPLASH				= 0;
	public static final byte SOUND_RACE_WON				= 1;
	public static final byte SOUND_RACE_LOST			= 2;
	public static final byte SOUND_CHAMPIONSHIP_WON		= 3;
	public static final byte SOUND_CHAMPIONSHIP_LOST	= 4;
	public static final byte SOUND_GEAR_CHANGE_OK		= 5;
	public static final byte SOUND_GEAR_CHANGE_ERROR	= 6;
	public static final byte SOUND_START				= 7;
	
	public static final byte SOUND_TOTAL				= 8;
	// </editor-fold>
	
	// cor padr�o de fundo da tela
	public static final int COLOR_BACKGROUND = 0x92918E;	
	
	// unidades reais
	public static final byte METER = 100;
	public static final int KILOMETER = 1000 * METER;
	public static final short SECONDS_PER_HOUR = 3600;
	
	// velocidades reais m�nimas do carro para utiliza��o de cada frame do asfalto
	public static final int ASPHALT_FRAME_1_SPEED = 200 * KILOMETER;
	public static final int ASPHALT_FRAME_2_SPEED = 300 * KILOMETER;
	
	/** velocidade dos carros no estado RACE_STATE_STAGING */
	public static final int CAR_PRE_STAGING_SPEED = 20 * KILOMETER;
	
	/** dist�ncia inicial m�nima do carro ao ponto de largada */
	public static final short CAR_PRE_STAGING_MIN_DISTANCE = -10 * METER;
	
	/** dist�ncia inicial m�xima do carro ao ponto de largada */
	public static final short CAR_PRE_STAGING_MAX_DISTANCE = 10 * METER;
	
	/** cor do ponteiro do tac�metro */
	public static final int TACHOMETER_COLOR = 0xddddff;
	
	/** �ngulo inicial do tac�metro (motor desligado) */
	public static final short TACHOMETER_MIN_ANGLE = -160;
	
	/** varia��o m�xima de �ngulo do tac�metro (motor na rota��o m�xima) */
	public static final short TACHOMETER_MAX_ANGLE_DIFF = -180 - ( ( 180 + TACHOMETER_MIN_ANGLE ) << 1 );
	
	/** dist�ncia m�nima do jogador para o topo da tela, em porcentagem */
	public static final byte PLAYSCREEN_MIN_PLAYER_Y_SCREEN_PERCENT = 5;

	
	public static final String DATABASE_NAME			= "N";
	public static final byte DATABASE_INDEX_MEDIA		= 1;
	public static final byte DATABASE_INDEX_TIMES		= 2;
	public static final byte DATABASE_INDEX_SPEEDS		= 3;
	public static final byte DATABASE_INDEX_SAVED_GAME	= 4;
	
	public static final byte DATABASE_TOTAL				= 4;
	
	
	// <editor-fold desc="estados da tela de jogo e carros">
	public static final byte RACE_STATE_NONE				= 0;
	public static final byte RACE_STATE_PRE_STAGING			= RACE_STATE_NONE + 1;
	public static final byte RACE_STATE_STAGING				= RACE_STATE_PRE_STAGING + 1;
	public static final byte RACE_STATE_COUNTDOWN			= RACE_STATE_STAGING + 1;
	public static final byte RACE_STATE_RED_LIGHT			= RACE_STATE_COUNTDOWN + 1;
	public static final byte RACE_STATE_RED_LIGHT_MESSAGE	= RACE_STATE_RED_LIGHT + 1;
	public static final byte RACE_STATE_RACING				= RACE_STATE_RED_LIGHT_MESSAGE + 1;
	public static final byte RACE_STATE_SHOW_RESULTS		= RACE_STATE_RACING + 1;
	public static final byte RACE_STATE_SHOW_TIME_RECORD	= RACE_STATE_SHOW_RESULTS + 1;
	public static final byte RACE_STATE_SHOW_SPEED_RECORD	= RACE_STATE_SHOW_TIME_RECORD + 1;
	public static final byte RACE_STATE_FINISHED			= RACE_STATE_SHOW_SPEED_RECORD + 1;
	public static final byte RACE_STATE_PAUSED				= RACE_STATE_FINISHED + 1;	
	public static final byte RACE_STATE_BRAKING				= RACE_STATE_PAUSED + 1;
	public static final byte RACE_STATE_WRECKED				= RACE_STATE_BRAKING + 1;	
	public static final byte RACE_STATE_RACE_AGAIN			= RACE_STATE_WRECKED + 1;	
	public static final byte RACE_STATE_BACK_TO_PLAYOFFS	= RACE_STATE_RACE_AGAIN + 1;	
	// </editor-fold>
	
	// n�mero m�ximo de largadas queimadas de um jogador
	public static final byte RACE_MAX_RED_LIGHTS = 2;
	
	/** n�mero de corridas de cada fase do campeonato */
	public static final byte CHAMPIONSHIP_RACES_PER_LEVEL = 5;
	
	// extens�o padr�o da pista em cm (equivalente a 1/4 de milha)
	public static final int DEFAULT_TRACK_LENGTH = 40233;	
	
	// tempo em milisegundos de cada luz amarela
	public static final short AMBER_LIGHT_TIME = 425;	
	
	/** tempo m�ximo em milisegundos que o jogador pode largar antes do tempo sem que se considere que queimou a largada */
	public static final byte RACER_RED_LIGHT_TIME = 20;
	
	// n�mero total de personagens
	public static final byte CHARACTERS_CHOOSABLE	= 16;
	public static final byte CHARACTERS_TOTAL		= CHARACTERS_CHOOSABLE + 1;
	
	// <editor-fold desc="estados da leitura do descritor de carros">
	/** quantidade total de informa��es a serem lidas de cada carro */
	public static final byte CAR_TOTAL_VALUES = 14;
	
	public static final byte CAR_INFO_BODY_TYPE					= 0;
	public static final byte CAR_INFO_AERO_TYPE					= 1;
	public static final byte CAR_INFO_AERO_POSITION				= 2;
	public static final byte CAR_INFO_ENGINE_1_TYPE				= 3;
	public static final byte CAR_INFO_ENGINE_1_POSITION			= 4;
	public static final byte CAR_INFO_ENGINE_1_FIRE_TYPE		= 5;
	public static final byte CAR_INFO_ENGINE_1_FIRE_POSITION	= 6;
	public static final byte CAR_INFO_ENGINE_2_TYPE				= 7;
	public static final byte CAR_INFO_ENGINE_2_POSITION			= 8;
	public static final byte CAR_INFO_ENGINE_2_FIRE_TYPE		= 9;
	public static final byte CAR_INFO_ENGINE_2_FIRE_POSITION	= 10;	
	public static final byte CAR_INFO_FRONT_TIRES_POSITION		= 11;
	public static final byte CAR_INFO_REAR_TIRES_POSITION		= 12;
	public static final byte CAR_INFO_DONE						= 13;
	
	public static final byte FIRE_TYPE_UP = 0;
	public static final byte FIRE_TYPE_DOWN = 1;
	
	// </editor-fold>	
	
	
	//<editor-fold desc="defini��es dos pneus traseiros">
	public static final short TIRES_FRONT_POSITION_CHANGE_FRAME = METER >> 1;
	public static final short TIRES_REAR_POSITION_CHANGE_FRAME = METER;
	/** velocidade m�xima para que os pneus traseiros mantenham-se largos (acima dessa velocidade, ficam na largura m�dia) */
	public static final int TIRES_REAR_MAX_SPEED_WIDE = 60 * KILOMETER;
	/** velocidade m�xima para que os pneus traseiros mantenham-se na sua largura m�dia (acima dessa velocidade, ficam estreitos) */
	public static final int TIRES_REAR_MAX_SPEED_MEDIUM = 110 * KILOMETER;
	//</editor-fold>
	
	
	//<editor-fold desc="defini��es dos p�ra-quedas">
	public static final int PARACHUTES_MIN_SPEED_FAST	= 290 * KILOMETER;
	public static final int PARACHUTES_MIN_SPEED_MEDIUM	= 160 * KILOMETER;
	public static final int PARACHUTES_MIN_SPEED_SLOW	= 30 * KILOMETER;
	//</editor-fold>
	
	

	
	// <editor-fold desc="�ndices das op��es de cada menu">
	
	public static final byte INDEX_SCREEN_NEW_GAME_MENU_SINGLE_RACE		= 1;
	public static final byte INDEX_SCREEN_NEW_GAME_MENU_TRAINING		= 2;
	//#if LOW_JAR == "true"
//# 		public static final byte INDEX_SCREEN_LOW_NEW_GAME_MENU_BACK		= 3;
	//#else
		public static final byte INDEX_SCREEN_NEW_GAME_MENU_CHAMPIONSHIP	= 3;
		public static final byte INDEX_SCREEN_NEW_GAME_MENU_BACK			= 4;
	//#endif

	public static final byte INDEX_SCREEN_TRAINING_SIMPLE	= 1;
	public static final byte INDEX_SCREEN_TRAINING_NORMAL	= 2;
	public static final byte INDEX_SCREEN_TRAINING_BACK		= 3;

	public static final byte INDEX_SCREEN_SINGLE_RACE_SIMPLE	= 1;
	public static final byte INDEX_SCREEN_SINGLE_RACE_NORMAL	= 2;
	public static final byte INDEX_SCREEN_SINGLE_RACE_BACK		= 3;
	
	public static final byte INDEX_SCREEN_CHAMPIONSHIP_SIMPLE	= 1;
	public static final byte INDEX_SCREEN_CHAMPIONSHIP_NORMAL	= 2;
	public static final byte INDEX_SCREEN_CHAMPIONSHIP_BACK		= 3;
	
	public static final byte INDEX_SCREEN_CHAMPIONSHIP_SAVED_CONTINUE	= 1;
	public static final byte INDEX_SCREEN_CHAMPIONSHIP_SAVED_NEW		= 2;
	public static final byte INDEX_SCREEN_CHAMPIONSHIP_SAVED_BACK		= 3;
	
	public static final byte INDEX_SCREEN_HIGH_SCORES_BEST_TIMES	= 1;
	public static final byte INDEX_SCREEN_HIGH_SCORES_TOP_SPEEDS	= 2;
	public static final byte INDEX_SCREEN_HIGH_SCORES_BACK			= 3;
	
	// </editor-fold>
	
	//<editor-fold desc="indicador de mensagens">
	/** porcentagem da largura da tela utilizada como medida para que as mensagens comecem a frear */
	public static final short MESSAGE_SCREEN_PERCENT_SLOW = 15;
	
	/** quantidade total de linhas de texto de uma tela de mensagem  */
	public static final byte MESSAGE_TEXT_LINES = 4;	
	
	/** quantidade de linhas de texto referentes � altura total do indicador de mensagens */
	public static final byte MESSAGE_MAX_LINES = MESSAGE_TEXT_LINES + 1;	
	
	/** velocidade do scroll autom�tico do texto do treinamento, em porcentagem de linhas de texto por segundo */
	public static final byte MESSAGE_SCROLL_LINES_PERCENT_PER_SECOND = 120;

	/** porcentagem da largura da tela percorridos por segundo pelos textos, no modo r�pido */
	public static final short MESSAGE_TEXT_SPEED_FAST_SCREEN_PERCENT = 150;
	/** porcentagem da largura da tela percorridos por segundo pelos textos, no modo lento */
	public static final byte MESSAGE_TEXT_SPEED_SLOW_SCREEN_PERCENT = 15;
	
	//</editor-fold>			
	
	
	//#ifdef NO_SOUND
//# 		// essa vers�o � utilizada no Samsung C420, que trata de maneira incorreta o tempo de vibra��o (� muito mais longo
//# 		// do que o tempo passado como par�metro)
//#  		public static final short VIBRATION_TIME_CAR_WRECKED		= 330;
//#  		public static final short VIBRATION_TIME_GEAR_CHANGE_ERROR	= 150;
	//#else
		public static final short VIBRATION_TIME_CAR_WRECKED		= 600;
		public static final short VIBRATION_TIME_GEAR_CHANGE_ERROR	= 330;
	//#endif	
	
	
	// <editor-fold defaultstate="collapsed" desc="telas do jogo">
	// telas do jogo
	public static final byte SCREEN_CHOOSE_SOUND					= 0;
	public static final byte SCREEN_SPLASH_NANO						= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_FULL_POWER				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME						= SCREEN_SPLASH_FULL_POWER + 1;
	public static final byte SCREEN_MAIN_MENU						= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_OPTIONS							= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CREDITS							= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP							= SCREEN_CREDITS + 1;
	public static final byte SCREEN_RACE_SINGLE						= SCREEN_HELP + 1;
	public static final byte SCREEN_RACE_CHAMPIONSHIP				= SCREEN_RACE_SINGLE + 1;
	public static final byte SCREEN_RACE_TRAINING					= SCREEN_RACE_CHAMPIONSHIP + 1;
	public static final byte SCREEN_CONTINUE_GAME					= SCREEN_RACE_TRAINING + 1;
	public static final byte SCREEN_PAUSE							= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_CONFIRM_MENU					= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT					= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_HIGH_SCORES_MENU				= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_HIGH_SCORES_TIMES				= SCREEN_HIGH_SCORES_MENU + 1;
	public static final byte SCREEN_HIGH_SCORES_SPEED				= SCREEN_HIGH_SCORES_TIMES + 1;
	public static final byte SCREEN_ERROR							= SCREEN_HIGH_SCORES_SPEED + 1;
	public static final byte SCREEN_LOADING							= SCREEN_ERROR + 1;	
	public static final byte SCREEN_TRAINING						= SCREEN_LOADING + 1;	
	public static final byte SCREEN_SINGLE_RACE_MENU				= SCREEN_TRAINING + 1;	
	
	//#if LOW_JAR == "false"
		public static final byte SCREEN_CHAMPIONSHIP_MENU				= SCREEN_SINGLE_RACE_MENU + 1;	
		public static final byte SCREEN_CHAMPIONSHIP_SAVED				= SCREEN_CHAMPIONSHIP_MENU + 1;	
		public static final byte SCREEN_NEW_GAME_MENU					= SCREEN_CHAMPIONSHIP_SAVED + 1;
	//#else
//# 		public static final byte SCREEN_NEW_GAME_MENU					= SCREEN_SINGLE_RACE_MENU + 1;
	//#endif
	
	public static final byte SCREEN_RACE_AGAIN						= SCREEN_NEW_GAME_MENU + 1;
	
	//#if LOW_JAR == "false"
		public static final byte SCREEN_CHAMPIONSHIP_NEW				= SCREEN_RACE_AGAIN + 1;
		public static final byte SCREEN_CHAMPIONSHIP_LOAD				= SCREEN_CHAMPIONSHIP_NEW + 1;
		public static final byte SCREEN_CHAMPIONSHIP_CONTINUE			= SCREEN_CHAMPIONSHIP_LOAD + 1;
		public static final byte SCREEN_CHOOSE_CHARACTER_SINGLE			= SCREEN_CHAMPIONSHIP_CONTINUE + 1;
		public static final byte SCREEN_CHOOSE_CHARACTER_CHAMPIONSHIP	= SCREEN_CHOOSE_CHARACTER_SINGLE + 1;
		public static final byte SCREEN_ERASE_RECORDS					= SCREEN_CHOOSE_CHARACTER_CHAMPIONSHIP + 1;
	//#else
//# 		public static final byte SCREEN_CHOOSE_CHARACTER_SINGLE			= SCREEN_RACE_AGAIN + 1;
//# 		public static final byte SCREEN_ERASE_RECORDS					= SCREEN_CHOOSE_CHARACTER_SINGLE + 1;
	//#endif
	
	// </editor-fold>
	
	
	// <editor-fold desc="textos">
	// defini��es dos textos
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= 1;
	public static final byte TEXT_NEW_GAME						= 2;
	public static final byte TEXT_EXIT							= 3;
	public static final byte TEXT_OPTIONS						= 4;
	public static final byte TEXT_PAUSE							= 5;
	public static final byte TEXT_CREDITS						= 6;
	public static final byte TEXT_CREDITS_TEXT					= 7;
	public static final byte TEXT_HELP							= 8;
	public static final byte TEXT_HELP_TEXT						= 9;
	public static final byte TEXT_ERASE_RECORDS_SHORT			= 10;
	public static final byte TEXT_LOADING_SHORT					= 11;
	public static final byte TEXT_CHAMPIONSHIP_SAVED_SHORT		= 12;
	public static final byte TEXT_FULL_POWER					= 13;
	public static final byte TEXT_CONGRATULATIONS				= 14;
	public static final byte TEXT_CHAMPION_TOP					= 15;
	public static final byte TEXT_TURN_SOUND_ON					= 16;
	public static final byte TEXT_TURN_SOUND_OFF				= 17;
	public static final byte TEXT_TURN_VIBRATION_ON				= 18;
	public static final byte TEXT_TURN_VIBRATION_OFF			= 19;
	public static final byte TEXT_CANCEL						= 20;
	public static final byte TEXT_MORE_GAMES					= 21;	
	public static final byte TEXT_CONTINUE						= 22;
	public static final byte TEXT_SPLASH_NANO					= 23;
	public static final byte TEXT_SPLASH_FULL_POWER				= 24;
	public static final byte TEXT_LOADING						= 25;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= 26;
	public static final byte TEXT_YES							= 27;
	public static final byte TEXT_NO							= 28;
	public static final byte TEXT_MAIN_MENU						= 29;
	public static final byte TEXT_CONFIRM_BACK_MENU				= 30;
	public static final byte TEXT_CONFIRM_EXIT					= 31;
	public static final byte TEXT_PRESS_ANY_KEY					= 32;
	public static final byte TEXT_LAST_ERROR					= 33;
	public static final byte TEXT_GAME_OVER						= 34;
	public static final byte TEXT_RECORDS						= 35;
	public static final byte TEXT_JUMP							= 36;
	public static final byte TEXT_NEW_RECORD_SPEED				= 37;
	public static final byte TEXT_TRY_AGAIN						= 38;
	public static final byte TEXT_TRAINING						= 39;
	public static final byte TEXT_SINGLE_RACE					= 40;
	public static final byte TEXT_CHAMPIONSHIP					= 41;
	public static final byte TEXT_SIMPLE						= 42;
	public static final byte TEXT_NORMAL						= 43;
	public static final byte TEXT_NEW							= 44;
	public static final byte TEXT_BEST_TIMES					= 45;
	public static final byte TEXT_HIGH_SCORES					= 46;
	public static final byte TEXT_TOP_SPEEDS					= 47;
	public static final byte TEXT_KMH							= 48;
	public static final byte TEXT_RACE_AGAIN					= 49;
	public static final byte TEXT_EIGHTH_FINALS					= 50;
	public static final byte TEXT_QUARTER_FINALS				= 51;
	public static final byte TEXT_SEMI_FINALS					= 52;
	public static final byte TEXT_FINAL							= 53;
	public static final byte TEXT_GEAR_NEUTRAL					= 54;
	public static final byte TEXT_GEAR_1						= 55;
	public static final byte TEXT_GEAR_2						= 56;
	public static final byte TEXT_GEAR_3						= 57;
	public static final byte TEXT_GEAR_4						= 58;
	public static final byte TEXT_CHAMPIONSHIP_SAVED			= 59;
	public static final byte TEXT_CHOOSE_CHARACTER				= 60;
	public static final byte TEXT_WIN							= 61;
	public static final byte TEXT_LOSE							= 62;
	public static final byte TEXT_WRECKED						= 63;
	public static final byte TEXT_RED_LIGHT_TOP					= 64;
	public static final byte TEXT_RED_LIGHT_BOTTOM				= 65;
	public static final byte TEXT_YOU							= 66;
	public static final byte TEXT_CAR							= 67;
	public static final byte TEXT_TIME							= 68;
	public static final byte TEXT_SPEED							= 69;
	public static final byte TEXT_VERSUS						= 70;
	public static final byte TEXT_NEW_RECORD_TIME				= 71;
	public static final byte TEXT_NEW_RECORD_SCORE				= 72;
	public static final byte TEXT_SECONDS						= 73;
	public static final byte TEXT_ERASE_RECORDS					= 74;
	public static final byte TEXT_ERASE_RECORDS_CONFIRM			= 75;
	public static final byte TEXT_GEAR_TYPE						= 76;
	public static final byte TEXT_CHOOSE_OPPONENT				= 77;
	public static final byte TEXT_TRAINING_START_AUTO			= 78;
	public static final byte TEXT_TRAINING_START_MANUAL			= 79;
	public static final byte TEXT_TRAINING_START_MANUAL_RETRY	= 80;
	public static final byte TEXT_TRAINING_1ST_GEAR_AUTO		= 81;
	public static final byte TEXT_TRAINING_GEAR_UP				= 82;
	public static final byte TEXT_TRAINING_GEAR_DOWN			= 83;
	public static final byte TEXT_TRAINING_DAMAGE				= 84;
	public static final byte TEXT_TRAINING_GEAR_DOWN_OK			= 85;
	public static final byte TEXT_TRAINING_COMPLETED			= 86;
	public static final byte TEXT_PRESS_5_TO_BUY				= 87;
	public static final byte TEXT_CONFIRM_BUY					= 88;
	public static final byte TEXT_SPONGEBOB_BUY_URL				= 89;
	public static final byte TEXT_BUY_ERROR						= 90;
	public static final byte TEXT_SPONGEBOB						= 91;
	public static final byte TEXT_NEXT_STAGE					= 92;
	public static final byte TEXT_ELIMINATED					= 93;
	
	public static final byte TEXT_TOTAL							= 94;	
	// </editor-fold>


	/** espa�amento extra entre os caracteres da fonte padr�o (os sobrep�e 1 pixel)*/
	public static final byte DEFAULT_FONT_CHAR_OFFSET = -1;	

	
	public static final int PLAYOFFS_KEYS_INITIAL_POS = 30;
	
	// caixa que cont�m as informa��es de cada chave (imagens e nomes dos corredores, al�m do resultado)
	public static final byte CHAMPIONSHIP_BOX_RACER_NAME_X			= 35;
	public static final byte CHAMPIONSHIP_BOX_RACER_NAME_WIDTH		= 55;
	public static final byte CHAMPIONSHIP_BOX_RACER_LABEL_HEIGHT	= 11;

	public static final byte CHAMPIONSHIP_BOX_FIRST_RACER_LABEL_Y	= 7;
	public static final byte CHAMPIONSHIP_BOX_SECOND_RACER_LABEL_Y	= 22;

	public static final byte CHAMPIONSHIP_BOX_RACER_SCORE_X			= 94;
	public static final byte CHAMPIONSHIP_BOX_RACER_SCORE_WIDTH		= 6;

	public static final byte CHAMPIONSHIP_BOX_RACER_FACE_X			= 2;
	public static final byte CHAMPIONSHIP_BOX_FIRST_RACER_FACE_Y	= 2;
	public static final byte CHAMPIONSHIP_BOX_SECOND_RACER_FACE_Y	= 19;

	public static final byte CHAMPIONSHIP_BOX_PLAYER_OFFSET_X			= -7;
	public static final byte CHAMPIONSHIP_BOX_PLAYER_FIRST_OFFSET_Y		= 1;
	public static final byte CHAMPIONSHIP_BOX_PLAYER_SECOND_OFFSET_Y	= 18;

	public static final byte CHAMPIONSHIP_KEY_WIDTH			= 20;
	public static final byte CHAMPIONSHIP_KEY_SMALL_HEIGHT	= 41;
	public static final byte CHAMPIONSHIP_KEY_MEDIUM_HEIGHT	= 83;
	public static final short CHAMPIONSHIP_KEY_BIG_HEIGHT	= 163;
 	// </editor-fold>			
	
	
	//<editor-fold desc="posicionamento das luzes de largada pequenas">
	public static final byte TREE_SMALL_LIGHTS_Y_INITIAL = 14;
	public static final byte TREE_SMALL_LIGHTS_Y_SPACING = 6;		

	//</editor-fold>	

	
	////////////////////////////////////////////
	// DEFINI��ES DEPENDENTES DO TAMANHO DE TELA
	
	//#if SCREEN_SIZE == "SMALL"
//# 	// <editor-fold desc="defini��es espec�ficas de telas pequenas">
//# 		// defini��es de telas pequenas
//# 	
//# 		// limite m�nimo de mem�ria considerado para que comece a haver "racionamento" de mem�ria
//# 		public static final int MEMORY_LOW_LIMIT = 700000;
//# 	
//# 		public static final byte INDEX_SCREEN_MAIN_MENU_NEW_GAME	= 0;
//# 		public static final byte INDEX_SCREEN_MAIN_MENU_OPTIONS		= 1;
//# 		public static final byte INDEX_SCREEN_MAIN_MENU_HIGH_SCORES	= 2;
//# 		public static final byte INDEX_SCREEN_MAIN_MENU_HELP		= 3;
//#  		public static final byte INDEX_SCREEN_MAIN_MENU_CREDITS		= 4;
//# 		public static final byte INDEX_SCREEN_MAIN_MENU_EXIT		= 5;
//# 			
//# 			
//# 		public static final byte SCREEN_HEIGHT_MIN = 120;
//# 		
//# 		public static final byte CHARACTERS_SCREEN_ITEMS_OFFSET = -2;
//# 	
//# 		/** altura m�nima da tela para que a tela de "Quer correr de novo?" exiba a tela de corrida no fundo */
//# 		public static final short RACE_AGAIN_MIN_HEIGHT = 140;
//# 	
//#  		/** n�mero de pixels da tela de jogo equivalente a um quil�metro real - valor utilizado para realizar o c�lculo
//#  		 de posicionamento de elementos do jogo baseados em posi��es reais da pista */
//#  		public static final short PIXELS_PER_KILOMETER = 10497;
//#  		
//#  		// espa�amento vertical entre os itens dos menus
//#  		public static final byte MENU_ITEMS_SPACING = 0;
//# 		
//# 		public static final byte MENU_TITLE_SPACING = 4;
//# 		
//# 		/** porcentagem m�xima da largura da tela que um label pode ter de largura */
//# 		public static final byte MESSAGE_TEXT_MAX_WIDTH_PERCENT = 96;
//# 		
//# 		//<editor-fold desc="posicionamento das luzes de largada">
//# 		public static final byte[] TREE_LIGHTS_X = { 5, 12, 25, 32 };
//# 		
//# 		public static final byte TREE_LIGHTS_Y_INITIAL = 14;
//# 		public static final byte TREE_LIGHTS_Y_SPACING = 6;
//# 		
//# 		//</editor-fold>	
//# 		
//#  		/** varia��o em pixels na posi��o vertical da torcida em rela��o � arquibancada */
//#  		public static final byte CROWD_STALLS_Y_OFFSET = 32;
//#  		
//#  		/** dist�ncia horizontal do centro dos carros ao centro da tela */
//#  		public static final byte PLAYSCREEN_CAR_OFFSET_X = 30;
//#  		
//#  		/** largura padr�o da pista, em pixels */
//#  		public static final short PLAYSCREEN_TRACK_WIDTH = 104;
//# 		
//#  		/** dist�ncia do cursor principal para os itens de menu */
//#  		public static final byte MENU_CURSOR_DISTANCE = 8;	
//# 		
//#  		/** velocidade de movimenta��o da tela em pixels por segundo */
//#  		public static final short CHAMPIONSHIP_TABLE_MOVE_SPEED = 110;		
//# 		
//#  		// <editor-fold desc="defini��es do painel do carro">
//#  		public static final byte DASHBOARD_TIME_LABEL_X = 9;
//#  		public static final byte DASHBOARD_TIME_LABEL_Y = 9;
//#  		public static final byte DASHBOARD_TIME_LABEL_WIDTH = 22;
//#  		public static final byte DASHBOARD_TIME_LABEL_HEIGHT = 5;
//#  		
//#  		public static final byte DASHBOARD_SPEED_LABEL_X = 9;
//#  		public static final byte DASHBOARD_SPEED_LABEL_Y = 21;
//#  		public static final byte DASHBOARD_SPEED_LABEL_WIDTH = 22;
//#  		public static final byte DASHBOARD_SPEED_LABEL_HEIGHT = 5;
//#  		
//#  		public static final byte DASHBOARD_GEAR_LABEL_X = 120;
//#  		public static final byte DASHBOARD_GEAR_LABEL_Y = 17;
//#  		public static final byte DASHBOARD_GEAR_LABEL_WIDTH = 9;
//#  		public static final byte DASHBOARD_GEAR_LABEL_HEIGHT = 10;
//#  		
//#  		public static final byte DASHBOARD_GEAR_SPRITE_X = 120;
//#  		public static final byte DASHBOARD_GEAR_SPRITE_Y = 5;
//#  		
//#  		public static final byte DASHBOARD_DAMAGE_X = 91;
//#  		public static final byte DASHBOARD_DAMAGE_Y = 17;
//#  
//#  		public static final byte DASHBOARD_DAMAGE_AREA_X		= 85;
//#  		public static final byte DASHBOARD_DAMAGE_AREA_Y		= 2;				
//#  		public static final byte DASHBOARD_DAMAGE_AREA_WIDTH	= 34;
//#  		public static final byte DASHBOARD_DAMAGE_AREA_HEIGHT	= 26;		
//#  		
//#  		public static final byte DASHBOARD_PISTONS_X		= 88;
//#  		public static final byte DASHBOARD_PISTONS_Y		= 3;
//#  		public static final byte DASHBOARD_PISTONS_WIDTH	= 29;
//#  		public static final byte DASHBOARD_PISTONS_HEIGHT	= 11;
//#  		
//#  		public static final byte DASHBOARD_SHIFT_LIGHT_X = 41;
//#  		public static final byte DASHBOARD_SHIFT_LIGHT_Y = 3;
//#  		
//#  		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_X		= 36;
//#  		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_Y		= -2;		
//#  		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_WIDTH	= 14;
//#  		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_HEIGHT	= 14;				
//#  		
//#  		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_X			= 74;
//#  		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_Y			= DASHBOARD_DAMAGE_AREA_Y;		
//#  		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_WIDTH		= 13;
//#  		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_HEIGHT	= DASHBOARD_DAMAGE_AREA_HEIGHT;
//#  		
//#  		public static final byte DASHBOARD_TACHOMETER_START_AREA_X		= DASHBOARD_SHIFT_LIGHT_AREA_X;
//#  		public static final byte DASHBOARD_TACHOMETER_START_AREA_Y		= DASHBOARD_SHIFT_LIGHT_AREA_Y;		
//#  		public static final byte DASHBOARD_TACHOMETER_START_AREA_WIDTH	= 53;
//#  		public static final byte DASHBOARD_TACHOMETER_START_AREA_HEIGHT	= 31;		
//#  		
//#  		
//#  		public static final byte DASHBOARD_TACHOMETER_CENTER_X = 65;
//#  		public static final byte DASHBOARD_TACHOMETER_CENTER_Y = 19;
//#  		
//#  		public static final byte DASHBOARD_TACHOMETER_POINTER_LENGTH		= 17;
//#  		public static final byte DASHBOARD_TACHOMETER_POINTER_LENGTH_TOTAL	= DASHBOARD_TACHOMETER_POINTER_LENGTH + 4;
//#  		public static final byte DASHBOARD_TACHOMETER_POINTER_ANGLE			= 7;
//#  		
//#  		public static final byte DASHBOARD_MINIMAP_X				= 9;
//#  		public static final byte DASHBOARD_MINIMAP_WIDTH			= 100;
//#  		public static final byte DASHBOARD_MINIMAP_HEIGHT			= 2;
//#  		public static final byte DASHBOARD_MINIMAP_FIRST_RACER_Y	= 28;
//#  		public static final byte DASHBOARD_MINIMAP_SECOND_RACER_Y	= 30;
//#  		// </editor-fold>		
//# 		
//#  		public static final byte PLAYSCREEN_INDICATOR_NONE			= 0;
//#  		public static final byte PLAYSCREEN_INDICATOR_ACCELERATE	= 1;
//#  		public static final byte PLAYSCREEN_INDICATOR_GEAR_UP		= 2;
//#  		public static final byte PLAYSCREEN_INDICATOR_GEAR_DOWN		= 3;
//#  		
//#  		public static final byte PLAYSCREEN_INDICATOR_Y_SPACING		= 4;
//#  		
//#  		public static final byte PLAYSCREEN_PEDAL_SEQUENCE_PRESSING		= 0;
//#  		public static final byte PLAYSCREEN_PEDAL_SEQUENCE_RELEASING	= 1;
//#  		public static final byte PLAYSCREEN_PEDAL_SEQUENCE_RELEASED		= 2;
//#  		
//#  		public static final byte PLAYSCREEN_PEDAL_TOTAL_SEQUENCES		= 3;
//#  		public static final byte PLAYSCREEN_GEAR_UP_TOTAL_SEQUENCES		= 2;
//#  		public static final byte PLAYSCREEN_GEAR_DOWN_TOTAL_SEQUENCES	= 2;
//#  		
//#  		public static final byte PLAYSCREEN_PEDAL_TOTAL_FRAMES		= 2;
//#  		public static final byte PLAYSCREEN_GEAR_UP_TOTAL_FRAMES	= 2;
//#  		public static final byte PLAYSCREEN_GEAR_DOWN_TOTAL_FRAMES	= 2;
//#  		
//#  		public static final short PLAYSCREEN_PEDAL_SEQUENCE_TIME		= 650;
//#  		public static final short PLAYSCREEN_GEAR_CHANGE_SEQUENCE_TIME	= 350;
//#  		
//#  		public static final byte PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED	= 0;
//#  		public static final byte PLAYSCREEN_GEAR_CHANGE_SEQUENCE_ANIMATING	= 1;		
//# 		
//# 		public static final byte SPONGEBOB_IMAGE_BORDER_SIZE = 3;
//# 		
//#  		/** n�mero total de frames dos pneus dianteiros */
//#  		public static final byte FRONT_TIRES_TOTAL_FRAMES		= 3;
//#  		public static final byte FRONT_TIRES_TOTAL_SEQUENCES	= 1;		
//# 		
//#  		/** dist�ncia horizontal de cada pneu traseiro ao centro do carro */
//#  		public static final byte REAR_TIRE_LEFT_X_OFFSET = -1;
//#  		public static final byte REAR_TIRE_RIGHT_X_OFFSET = 3;
//#  		public static final byte REAR_TIRES_WIDTH = 27;
//#  		public static final byte REAR_TIRES_HEIGHT = 20;		
//# 		
//# 		public static final byte PARACHUTES_HEIGHT = 62;
//# 		
//# 		//<editor-fold desc="tela de sele��o de personagens">
//# 		public static final byte CHARACTER_SCREEN_FACE_SPACING = 3;
//# 		//</editor-fold>
//# 	
//# 		
//# 	// </editor-fold>	
	//#else
		// <editor-fold desc="defini��es espec�ficas de telas m�dias e grandes">
		// defini��es de telas m�dias e grandes
		
		// limite m�nimo de mem�ria considerado para que comece a haver "racionamento" de mem�ria
		public static final int MEMORY_LOW_LIMIT = 999999;		
	public static final byte INDEX_SCREEN_MAIN_MENU_NEW_GAME	= 1;
	public static final byte INDEX_SCREEN_MAIN_MENU_OPTIONS		= 2;
	public static final byte INDEX_SCREEN_MAIN_MENU_HIGH_SCORES	= 3;
	public static final byte INDEX_SCREEN_MAIN_MENU_HELP		= 4;
	public static final byte INDEX_SCREEN_MAIN_MENU_CREDITS		= 5;
	public static final byte INDEX_SCREEN_MAIN_MENU_EXIT		= 6;
		
	
		/** n�mero de pixels da tela de jogo equivalente a um quil�metro real - valor utilizado para realizar o c�lculo
		 de posicionamento de elementos do jogo baseados em posi��es reais da pista */
		public static final short PIXELS_PER_KILOMETER = 17714;
		
		// espa�amento vertical entre os itens dos menus
		public static final short MENU_ITEMS_SPACING = 4;
		
		// espa�amento vertical entre os itens dos menus
		public static final short MENU_TITLE_SPACING = MENU_ITEMS_SPACING;
	
		/** porcentagem m�xima da largura da tela que um label pode ter de largura */
		public static final byte MESSAGE_TEXT_MAX_WIDTH_PERCENT = 90;		
		
		// <editor-fold desc="defini��es da tela das chaves do campeonato">
		/** velocidade de movimenta��o da tela em pixels por segundo */
		public static final short CHAMPIONSHIP_TABLE_MOVE_SPEED = 150;
	
		
		public static final short SCREEN_HEIGHT_MIN = 200;

		/** n�mero total de frames dos pneus dianteiros */
		public static final byte FRONT_TIRES_TOTAL_FRAMES		= 3;
		public static final byte FRONT_TIRES_TOTAL_SEQUENCES	= 1;
		
		/** dist�ncia horizontal de cada pneu traseiro ao centro do carro */
		public static final byte REAR_TIRE_LEFT_X_OFFSET = -3;
		public static final byte REAR_TIRE_RIGHT_X_OFFSET = 4;
		public static final byte REAR_TIRES_WIDTH = 49;
		public static final byte REAR_TIRES_HEIGHT = 32;
		
		// <editor-fold desc="defini��es do painel do carro">
		public static final short DASHBOARD_TOTAL_WIDTH = 176;
		
		public static final byte DASHBOARD_TIME_LABEL_X = 11;
		public static final byte DASHBOARD_TIME_LABEL_Y = 12;
		public static final byte DASHBOARD_TIME_LABEL_WIDTH = 27;
		public static final byte DASHBOARD_TIME_LABEL_HEIGHT = 8;
		
		public static final byte DASHBOARD_SPEED_LABEL_X = 11;
		public static final byte DASHBOARD_SPEED_LABEL_Y = 29;
		public static final byte DASHBOARD_SPEED_LABEL_WIDTH = 27;
		public static final byte DASHBOARD_SPEED_LABEL_HEIGHT = 8;
		
		public static final short DASHBOARD_GEAR_LABEL_X = 161;
		public static final byte DASHBOARD_GEAR_LABEL_Y = 23;
		public static final byte DASHBOARD_GEAR_LABEL_WIDTH = 13;
		public static final byte DASHBOARD_GEAR_LABEL_HEIGHT = 14;
		
		public static final short DASHBOARD_GEAR_SPRITE_X = 165;
		public static final byte DASHBOARD_GEAR_SPRITE_Y = 10;
		
		public static final byte DASHBOARD_DAMAGE_X = 122;
		public static final byte DASHBOARD_DAMAGE_Y = 23;

		public static final byte DASHBOARD_DAMAGE_AREA_X		= 117;
		public static final byte DASHBOARD_DAMAGE_AREA_Y		= 3;				
		public static final byte DASHBOARD_DAMAGE_AREA_WIDTH	= 42;
		public static final byte DASHBOARD_DAMAGE_AREA_HEIGHT	= 34;		
		
		public static final byte DASHBOARD_PISTONS_X		= 120;
		public static final byte DASHBOARD_PISTONS_Y		= 4;
		public static final byte DASHBOARD_PISTONS_WIDTH	= 36;
		public static final byte DASHBOARD_PISTONS_HEIGHT	= 15;
		
		public static final byte DASHBOARD_SHIFT_LIGHT_X = 53;
		public static final byte DASHBOARD_SHIFT_LIGHT_Y = 4;
		
		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_X		= 49;
		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_Y		= 0;		
		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_WIDTH	= 15;
		public static final byte DASHBOARD_SHIFT_LIGHT_AREA_HEIGHT	= 14;				
		
		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_X			= 99;
		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_Y			= 14;		
		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_WIDTH		= 15;
		public static final byte DASHBOARD_TACHOMETER_DAMAGE_AREA_HEIGHT	= 22;
		
		public static final byte DASHBOARD_TACHOMETER_START_AREA_X		= 63;
		public static final byte DASHBOARD_TACHOMETER_START_AREA_Y		= 4;		
		public static final byte DASHBOARD_TACHOMETER_START_AREA_WIDTH	= 51;
		public static final byte DASHBOARD_TACHOMETER_START_AREA_HEIGHT	= 33;		
		
		
		public static final byte DASHBOARD_TACHOMETER_CENTER_X = 88;
		public static final byte DASHBOARD_TACHOMETER_CENTER_Y = 26;
		
		public static final byte DASHBOARD_TACHOMETER_POINTER_LENGTH		= 22;
		public static final byte DASHBOARD_TACHOMETER_POINTER_LENGTH_TOTAL	= DASHBOARD_TACHOMETER_POINTER_LENGTH + 4;
		public static final byte DASHBOARD_TACHOMETER_POINTER_ANGLE			= 6;
		
		public static final byte DASHBOARD_MINIMAP_X				= 10;
		public static final short DASHBOARD_MINIMAP_WIDTH			= 144;
		public static final short DASHBOARD_MINIMAP_HEIGHT			= 3;
		public static final byte DASHBOARD_MINIMAP_FIRST_RACER_Y	= 38;
		public static final byte DASHBOARD_MINIMAP_SECOND_RACER_Y	= 41;
		// </editor-fold>		
		
		//<editor-fold desc="posicionamento das luzes de largada grandes">
		public static final byte TREE_BIG_LIGHTS_Y_INITIAL = 0;
		public static final byte TREE_BIG_LIGHTS_Y_SPACING = 17;
	
		//</editor-fold>		
		
		
		/** varia��o em pixels na posi��o vertical da torcida em rela��o � arquibancada */
		public static final byte CROWD_STALLS_Y_OFFSET = 32;
		
		/** dist�ncia horizontal do centro dos carros ao centro da tela */
		public static final byte PLAYSCREEN_CAR_OFFSET_X = 52;
		
		/** largura padr�o da pista, em pixels */
		public static final short PLAYSCREEN_TRACK_WIDTH = 176;
		
		
		/** dist�ncia do cursor principal para os itens de menu */
		public static final byte MENU_CURSOR_DISTANCE = 13;
		
		//<editor-fold desc="tela de splash">
		public static final short SPLASH_MAX_WIDTH	= 320;
		public static final short SPLASH_MAX_HEIGHT	= 320;
		
		public static final byte SPLASH_CAR_X = 57;
		public static final byte SPLASH_CAR_Y = 119;
		//</editor-fold>		
		
		public static final byte PLAYSCREEN_INDICATOR_NONE			= 0;
		public static final byte PLAYSCREEN_INDICATOR_ACCELERATE	= 1;
		public static final byte PLAYSCREEN_INDICATOR_GEAR_UP		= 2;
		public static final byte PLAYSCREEN_INDICATOR_GEAR_DOWN		= 3;
		
		public static final byte PLAYSCREEN_INDICATOR_Y_SPACING		= 4;
		
		public static final byte PLAYSCREEN_INDICATOR_Y_SPACING_POINTER		= 14;
		public static final byte PLAYSCREEN_INDICATOR_X_SPACING_POINTER		= 18;
		
		public static final byte PLAYSCREEN_PEDAL_SEQUENCE_PRESSING		= 0;
		public static final byte PLAYSCREEN_PEDAL_SEQUENCE_RELEASING	= 1;
		public static final byte PLAYSCREEN_PEDAL_SEQUENCE_RELEASED		= 2;
		
		public static final byte PLAYSCREEN_GEAR_CHANGE_SEQUENCE_STOPPED	= 0;
		public static final byte PLAYSCREEN_GEAR_CHANGE_SEQUENCE_ANIMATING	= 1;
		
		
		public static final byte SPONGEBOB_IMAGE_BORDER_SIZE = 3;
		
		public static final byte PARACHUTES_HEIGHT = 62;
		
		/** Altura da �rea de pausa, para aparelhos com touch screen. */
		public static final byte PAUSE_AREA_HEIGHT = 27;
		
		/** Posi��o x de in�cio da �rea de colis�o da pausa, para aparelhos com touch screen (contados a partir da posi��o do painel). */
		public static final short PAUSE_AREA_START_X = 154;
		
		//<editor-fold desc="tela de sele��o de personagens">
		public static final byte CHARACTER_SCREEN_FACE_SPACING = 6;
		//</editor-fold>
		

	//#endif
	
	
}
