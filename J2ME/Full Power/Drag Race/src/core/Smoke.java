//#if LOW_JAR == "false"

/*
 * Smoke.java
 *
 * Created on November 2, 2007, 11:45 AM
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;

/**
 *
 * @author peter
 */
public final class Smoke extends Sprite implements Constants, SpriteListener {
	
	public static final byte SEQUENCE_INVISIBLE	= 0;
	
	/** Creates a new instance of Smoke
	 * @throws java.lang.Exception 
	 */
	public Smoke() throws Exception {
		super( PATH_IMAGES + "smoke/smoke.dat", PATH_IMAGES + "smoke/smoke" );
		
		defineReferencePixel( size.x >> 1, 0 );
		
		setListener( this, 0 );
	}
	
	
	public final void setSequence( int sequence ) {
		if ( sequence != sequenceIndex )
			super.setSequence( sequence );
		
		setVisible( sequence != SEQUENCE_INVISIBLE );
	}

	
	public final void onSequenceEnded( int id, int sequence ) {
		setSequence( SEQUENCE_INVISIBLE );
	}

	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}
	
}

//#endif