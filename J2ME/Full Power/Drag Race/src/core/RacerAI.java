/*
 * RacerAI.java
 *
 * Created on October 5, 2007, 11:06 AM
 *
 */

package core;

import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author peter
 */
public final class RacerAI extends Racer {
	
	private byte skillLevel;
	
	// normalmente, os aparelhos que precisam utilizar a vers�o de jar pequeno possuem desempenho baixo, o que dificulta
	// o jogo. Logo, a dificuldade � reduzida para estes aparelhos, para que possa ficar equivalente � dos outros.
	//#if LOW_JAR == "true"
//# 	private static final short MIN_REACTION_GEAR_CHANGE = 350;
//# 	private static final short MAX_GEAR_CHANGE_DIFF		= 2100;
//# 	private static final short MAX_REACTION_START		= 850;	
	//#else
	private static final short MIN_REACTION_GEAR_CHANGE = 100;
	private static final short MAX_GEAR_CHANGE_DIFF		= 1800;
	private static final short MAX_REACTION_START		= 450;	
	//#endif
	
	private short reactionTime;
	

	public RacerAI( int index, boolean loadCar ) throws Exception {
		this( index, loadCar, ( byte ) 0 );
	}	
	
	
	/** Creates a new instance of RacerAI */
	public RacerAI( int index, boolean loadCar, byte skillLevel ) throws Exception {
		super( index, loadCar );
		engine.setAcceleration( Engine.ACCELERATION_AUTOMATIC );
		
		setSkillLevel( skillLevel );
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		switch ( state ) {
			case RACE_STATE_RACING:
				switch ( engine.getGear() ) {
					case 1:
					case 2:
					case 3:
						if ( engine.getRotation() < engine.ROTATION_MAX_TORQUE )
							break;

					case Engine.GEAR_NEUTRAL:
						reactionTime -= delta;
						if ( reactionTime <= 0 ) {
							engine.gearUp( false );
							reactionTime = ( short ) ( MIN_REACTION_GEAR_CHANGE + NanoMath.randInt( ( 120 - skillLevel ) * MAX_GEAR_CHANGE_DIFF / 100 ) );
						}
					break;
				}

			break;
		}
	}	
	
	
	/**
	 * Define o n�vel de habilidade do corredor da AI.
	 * @param level n�vel de habilidade em porcentagem. Valores v�lidos est�o na faixa 0-100 (inclusive).
	 */
	public final void setSkillLevel( int level ) {
		if ( level < 0 ) {
			level = ( byte ) NanoMath.randInt( 100 );
		}
		else if ( level > 100 )
			level = 100;
		
		skillLevel = ( byte ) level;
	}

	
	public final void setState( int state ) {
		super.setState( state );
		
		switch ( state ) {
			case RACE_STATE_STAGING:
				reactionTime = ( short ) NanoMath.randInt( ( 120 - skillLevel ) * MAX_REACTION_START / 100 );
			break;
		}
	}
	
}
