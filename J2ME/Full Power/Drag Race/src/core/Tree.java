/*
 * Tree.java
 *
 * Created on October 15, 2007, 12:40 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import screens.GameMIDlet;
import screens.PlayScreen;

/**
 *
 * @author peter
 */
public final class Tree extends UpdatableGroup implements Constants {
	
	private final byte LIGHT_TYPE_PRE_STAGE = 0;
	private final byte LIGHT_TYPE_AMBER		= 1;
	private final byte LIGHT_TYPE_GREEN		= 2;
	private final byte LIGHT_TYPE_RED		= 3;
	
	// quantidade de l�mpadas de cada est�gio, e tipo
	private final byte[][] LIGHTS_INFO = { 
										{},
										{},
										{ 4, LIGHT_TYPE_PRE_STAGE },
										{ 4, LIGHT_TYPE_PRE_STAGE },
										{ 2, LIGHT_TYPE_AMBER },
										{ 2, LIGHT_TYPE_AMBER },
										{ 2, LIGHT_TYPE_AMBER },
										{ 2, LIGHT_TYPE_GREEN },
										{ 2, LIGHT_TYPE_RED },
										{},
										{},
									};

	private static final byte TOTAL_LIGHTS = 18;
	
	private final DrawableImage[] lights = new DrawableImage[ TOTAL_LIGHTS ];
	
	public static final byte STATE_APPEARING	= 0;
	public static final byte STATE_SHOWN		= 1;
	public static final byte STATE_PRE_STAGE_1	= 2;
	public static final byte STATE_PRE_STAGE_2	= 3;
	public static final byte STATE_AMBER_1		= 4;
	public static final byte STATE_AMBER_2		= 5;
	public static final byte STATE_AMBER_3		= 6;
	public static final byte STATE_GREEN		= 7;
	public static final byte STATE_RED			= 8;
	public static final byte STATE_HIDING		= 9;
	public static final byte STATE_HIDDEN		= 10;
	
	private byte state;
	
	private static final byte TOTAL_ITEMS = TOTAL_LIGHTS + 3;
	
	// tempo de transi��o entre os estados em milisegundos
	private static final short TRANSITION_TIME = 600;
	
	private int accTime;
	
	private final PlayScreen playScreen;
	
	
	/** Creates a new instance of Tree */
	public Tree( PlayScreen playScreen ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.playScreen = playScreen;
		
		final byte[] TREE_SMALL_LIGHTS_X = { 5, 12, 25, 32 };
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			final String path = GameMIDlet.isLowMemory() ? PATH_TREE_SMALL : PATH_TREE_BIG;
		//#else
//# 			final String path = PATH_TREE_SMALL;
		//#endif
		final DrawableImage tree = new DrawableImage( path + "tree.png" );
		insertDrawable( tree );
		
		//#if SCREEN_SIZE == "MEDIUM_BIG"
			final int INITIAL_Y = GameMIDlet.isLowMemory() ? TREE_SMALL_LIGHTS_Y_INITIAL : TREE_BIG_LIGHTS_Y_INITIAL;
			final int SPACING_Y = GameMIDlet.isLowMemory() ? TREE_SMALL_LIGHTS_Y_SPACING : TREE_BIG_LIGHTS_Y_SPACING;
			
			final byte[] TREE_BIG_LIGHTS_X = { 0, 17, 34, 51 };
			
			final byte[] TREE_LIGHTS_X = GameMIDlet.isLowMemory() ? TREE_SMALL_LIGHTS_X : TREE_BIG_LIGHTS_X;
		//#else
//# 			final int INITIAL_Y = TREE_SMALL_LIGHTS_Y_INITIAL;
//# 			final int SPACING_Y = TREE_SMALL_LIGHTS_Y_SPACING;
//# 			final byte[] TREE_LIGHTS_X = TREE_SMALL_LIGHTS_X;		
		//#endif
		
		for ( int i = STATE_PRE_STAGE_1, y = INITIAL_Y, lightIndex = 0; i <= STATE_RED; ++i, y += SPACING_Y ) {
			for ( int j = 0, xIndex = ( LIGHTS_INFO[ i ][ 0 ] == 4 ? 0 : 1 ); j < LIGHTS_INFO[ i ][ 0 ]; ++j, ++xIndex, ++lightIndex ) {
				final DrawableImage light = new DrawableImage( path + LIGHTS_INFO[ i ][ 1 ] + ".png" );
				lights[ lightIndex ] = light;
				light.setPosition( TREE_LIGHTS_X[ xIndex ], y );
				light.setVisible( false );
				
				insertDrawable( light );
			}
		}
		
		setSize( tree.getSize() );
		defineReferencePixel( size.x >> 1, size.y );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 0 );
	}
	
	
	public final void setState( int state ) {
		if ( state != this.state ) {
			switch ( state ) {
				case STATE_APPEARING:
					switch ( this.state ) {
						case STATE_HIDDEN:
							setVisible( true );
						break;
						
						default:
							setState( STATE_SHOWN );
							return;
					}
					for ( int i = 0; i < lights.length; ++i )
						lights[ i ].setVisible( false );
				break;
				
				case STATE_SHOWN:
					setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, size.y );
					setVisible( true );
				break;

				case STATE_HIDDEN:
					setVisible( false );
				case STATE_PRE_STAGE_1:
				case STATE_PRE_STAGE_2:
				case STATE_AMBER_1:
				case STATE_AMBER_2:
				case STATE_AMBER_3:
				case STATE_GREEN:
					switch ( state ) {
						case STATE_AMBER_1:
							MediaPlayer.play( SOUND_START, 1 );
						break;
						
						case STATE_GREEN:
							if ( !playScreen.setState( RACE_STATE_RACING ) )
								return;
						break;
					}
				case STATE_RED:
					int lightIndex = state == STATE_RED ? TOTAL_LIGHTS - 2 : 0;
					for ( int i = ( state == STATE_RED ? STATE_RED : STATE_PRE_STAGE_1 ); i <= STATE_RED; ++i ) {
						for ( int j = 0; j < LIGHTS_INFO[ i ][ 0 ]; ++j, ++lightIndex ) {
							lights[ lightIndex ].setVisible( i <= state );
						}
					}
				break;
				
				case STATE_HIDING:
					setVisible( true );
				break;
			} // fim switch ( transitionState )
			
			accTime = 0;
			this.state = ( byte ) state;
		} // fim if ( transitionState != this.transitionState )
	} // fim do m�todo setState( int )

	
	public final void update( int delta ) {
		accTime += delta;
		
		switch ( state ) {
			case STATE_APPEARING:
				if ( accTime < TRANSITION_TIME )
					setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, size.y * accTime / TRANSITION_TIME );
				else
					setState( STATE_SHOWN );
			break;
			
			case STATE_AMBER_1:
			case STATE_AMBER_2:
			case STATE_AMBER_3:
				if ( accTime >= AMBER_LIGHT_TIME )
					setState( state + 1 );
			break;
			
			case STATE_GREEN:
				if ( accTime >= TRANSITION_TIME )
					setState( STATE_HIDING );
			break;
			
			case STATE_RED:
				if ( accTime >= TRANSITION_TIME )
					setState( STATE_HIDING );
			break;
			
			case STATE_HIDING:
				if ( accTime < TRANSITION_TIME )
					setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, size.y - ( size.y * accTime / TRANSITION_TIME ) );
				else
					setState( STATE_HIDDEN );				
			break;
		} // fim switch ( transitionState )
	}
	
	
	
	/**
	 * Indica se uma largada � v�lida, ou seja, se o corredor largou ap�s o sinal estar verde, ou ent�o at� RACER_RED_LIGHT_TIME
	 * antes da luz verde se acender.
	 * @return true, caso a largada seja v�lida, ou false caso contr�rio.
	 */
	public final boolean checkStart() {
		switch ( state ) {
			case STATE_AMBER_3:
				return ( AMBER_LIGHT_TIME - accTime ) <= RACER_RED_LIGHT_TIME;
			
			case STATE_GREEN:
				return true;
			
			default:
				return false;
		}
	}
	
}
