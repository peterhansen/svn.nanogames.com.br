/*
 * Parachutes.java
 *
 * Created on October 29, 2007, 6:49 PM
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;
import screens.LoadScreen;

/**
 *
 * @author peter
 */
public final class Parachutes extends Sprite implements Constants, SpriteListener {
	
	public static final byte SEQUENCE_NONE			= 0;
	public static final byte SEQUENCE_OPENING		= 1;
	public static final byte SEQUENCE_SPEED_FAST	= 2;
	public static final byte SEQUENCE_SPEED_MEDIUM	= 3;
	public static final byte SEQUENCE_SPEED_SLOW	= 4;
	public static final byte SEQUENCE_CLOSING		= 5;
	public static final byte SEQUENCE_CLOSED		= 6;
	
	private static final byte FRAME_TIME_OPENING	= 127;
	private static final byte FRAME_TIME_FAST		= 50;
	private static final byte FRAME_TIME_MEDIUM		= 85;
	private static final byte FRAME_TIME_SLOW		= 120;
	
	/** total de frames utilizados na vers�o completa */
	//#if SCREEN_SIZE == "MEDIUM_BIG"
	private static final byte TOTAL_WIDTH = 45;
	private static final byte TOTAL_HEIGHT = 61;
	
	private static final byte TOTAL_FRAMES = 11;
	/** frames utilizados na vers�o de baixa mem�ria */
	private static final byte[] FRAMES_LOW_MEMORY = { 1, 4, 9, 10 };	
	//#else
//# 	private static final byte TOTAL_WIDTH = 31;
//# 	private static final byte TOTAL_HEIGHT = 46;
//# 	
//# 	private static final byte TOTAL_FRAMES = 10;
//# 	/** frames utilizados na vers�o de baixa mem�ria */
//# 	private static final byte[] FRAMES_LOW_MEMORY = { 1, 4, 9, 9 };	
	//#endif
	
	private static Sprite frameSet;
	
	
	/** Creates a new instance of Parachutes
	 * @throws java.lang.Exception 
	 */
	public Parachutes() throws Exception {
		super( frameSet );
		
		setSize( TOTAL_WIDTH, TOTAL_HEIGHT );
		defineReferencePixel( size.x >> 1, 0 );
		
		setListener( this, 0 );
	}
	
	
	public static final void loadFrameSet( LoadScreen load ) throws Exception {
		if ( frameSet == null ) {
			//#if SCREEN_SIZE == "MEDIUM_BIG"
			final Point[] offsets = new Point[] {
				new Point( 14, 0 ),
				new Point( 15, 0 ),
				new Point( 7, 0 ),
				new Point( 5, 0 ),
				new Point( 2, 0 ),
				new Point( 0, 0 ),
				new Point( 1, 0 ),
				new Point( 5, 0 ),
				new Point( 7, 0 ),
				new Point( 15, 0 ),
				new Point( 14, 0 ),
			};
			final Point[] offsetsLow = new Point[] {
				new Point( 15, 0 ),
				new Point( 2, 0 ),
				new Point( 15, 0 ),
				new Point( 14, 0 ),
			};
			//#else
//# 			final Point[] offsets = new Point[] {
//# 				new Point( 11, 0 ),
//# 				new Point( 11, 0 ),
//# 				new Point( 6, 0 ),
//# 				new Point( 5, 0 ),
//# 				new Point( 3, 0 ),
//# 				new Point( 0, 0 ),
//# 				new Point( 0, 0 ),
//# 				new Point( 6, 0 ),
//# 				new Point( 11, 0 ),
//# 				new Point( 11, 0 ),
//# 			};				
//# 			
//# 			final Point[] offsetsLow = new Point[] {
//# 				new Point( 11, 0 ),
//# 				new Point( 3, 0 ),
//# 				new Point( 11, 0 ),
//# 				new Point( 11, 0 ),
//# 			};
			//#endif

			if ( GameMIDlet.isLowMemory() ) {
				final Image[] frames = new Image[ FRAMES_LOW_MEMORY.length ];
				for ( int i = 0; i < FRAMES_LOW_MEMORY.length; ++i ) {
					frames[ i ] = ImageLoader.loadImage( PATH_PARACHUTES + FRAMES_LOW_MEMORY[ i ] + ".png" );
					load.changeProgress( 1 );
				}
				
				final byte[][] sequences = new byte[][] {
					{ 0 },
					{ 0, 0 },
					{ 1 },
					{ 1 },
					{ 1 },
					{ 2, 2 },
					{ 3 }
				};
				
				final short[] frameTimes = new short[] { 0, FRAME_TIME_OPENING, 0, 0, 0, FRAME_TIME_OPENING, 0 };
				
				frameSet = new Sprite( frames, offsetsLow, sequences, frameTimes );
				
				load.changeProgress( 2 );
			} else {
				final Image[] frames = new Image[ TOTAL_FRAMES ];
				for ( int i = 0; i < frames.length; ++i ) {
					frames[ i ] = ImageLoader.loadImage( PATH_PARACHUTES + i + ".png" );
					load.changeProgress( 1 );
				}
				
				final byte[][] sequences = new byte[][] {
					{ 0 },
					{ 0, 1, 2, 3 },
					{ 4, 5, 6 },
					{ 4, 5, 6 },
					{ 4, 5, 6 },
					{ 7, 8, 9 },
					
					//#if SCREEN_SIZE == "MEDIUM_BIG"
					{ 10 },
					//#else
//# 					{ 9 },
					//#endif
				};
				
				final short[] frameTimes = new short[] { 0, FRAME_TIME_OPENING, FRAME_TIME_FAST, FRAME_TIME_MEDIUM, FRAME_TIME_SLOW, FRAME_TIME_OPENING, 0 };
				
				frameSet = new Sprite( frames, offsets, sequences, frameTimes );
				load.changeProgress( 2 );
			}
		} // fim if ( frameSet == null )
	}

	
	public final void setSequence( int sequence ) {
		if ( sequence != sequenceIndex ) {
			switch ( sequenceIndex ) {
				case SEQUENCE_CLOSED:
					if ( sequence == SEQUENCE_CLOSING ) {
						break;
					}
				
				default:
					super.setSequence( sequence );
					setFrame( 0 );
			}
		}
			
		setVisible( sequenceIndex != SEQUENCE_NONE );
	}
	
	
	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case SEQUENCE_OPENING:
				setSequence( SEQUENCE_SPEED_FAST );
			break;
			
			case SEQUENCE_CLOSING:
				setSequence( SEQUENCE_CLOSED );
			break;
		}
	}

	
	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}
	
}
