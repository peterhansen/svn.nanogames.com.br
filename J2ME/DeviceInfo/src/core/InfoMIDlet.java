package core;

/**
 * InfoMIDlet.java
 * �2008 Nano Games.
 *
 * Created on 21/01/2008 17:34:30.
 */

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import java.util.Hashtable;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.media.Manager;

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

//#if PUSH_REGISTRY == "true"
import javax.microedition.io.PushRegistry;
//#endif


/**
 * 
 * @author Peter
 */
public final class InfoMIDlet extends AppMIDlet implements Constants {

	private static ImageFont FONT_DEFAULT;
	
	private static InfoMIDlet midlet;

	private static MyMenuListener myMenuListener;
	
	private static Label CURSOR;
	
	
	protected final void loadResources() throws Exception {
		midlet = ( InfoMIDlet ) instance;
		
		FONT_DEFAULT = ImageFont.createMultiSpacedFont( PATH_RESOURCES + "font.png", PATH_RESOURCES + "font.dat" );

		loadTexts( TEXT_TOTAL, PATH_RESOURCES + "texts.dat" );

		CURSOR = new Label( FONT_DEFAULT, getText( TEXT_CURSOR ) );
		CURSOR.defineReferencePixel( CURSOR.getSize().x, CURSOR.getSize().y >> 1 );

		MediaPlayer.init( "N", 1, new String[] { "/0.mid" } );

		myMenuListener = new MyMenuListener( this );

		//#if BLACKBERRY_API == "true"
//# 
//# 			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
//# 			int[][] keys = null;
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 },
//# 						{ 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 },
//# 						{ 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 },
//# 						{ 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 },
//# 						{ 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 },
//# 						{ 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 },
//# 						{ 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 },
//# 						{ 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 },
//# 						{ 'N', ScreenManager.KEY_NUM8 },
//# 
//# 						{ 'e', ScreenManager.KEY_NUM1 },
//# 						{ 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 },
//# 						{ 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 },
//# 						{ 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 },
//# 						{ 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 },
//# 						{ 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 },
//# 						{ 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 },
//# 						{ 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR },
//# 						{ 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR },
//# 						{ 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR },
//# 						{ 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR },
//# 						{ 'S', ScreenManager.KEY_STAR },
//# 
//# 						{ '0', ScreenManager.KEY_NUM0 },
//# 						{ ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//# 
//# 				default:
//# 					keys = new int[][] {
//# 						{ 'w', ScreenManager.KEY_NUM1 },
//# 						{ 'W', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM3 },
//# 						{ 'R', ScreenManager.KEY_NUM3 },
//# 						{ 'z', ScreenManager.KEY_NUM7 },
//# 						{ 'Z', ScreenManager.KEY_NUM7 },
//# 						{ 'c', ScreenManager.KEY_NUM9 },
//# 						{ 'C', ScreenManager.KEY_NUM9 },
//# 						{ 'e', ScreenManager.KEY_NUM2 },
//# 						{ 'E', ScreenManager.KEY_NUM2 },
//# 						{ 's', ScreenManager.KEY_NUM4 },
//# 						{ 'S', ScreenManager.KEY_NUM4 },
//# 						{ 'd', ScreenManager.KEY_NUM5 },
//# 						{ 'D', ScreenManager.KEY_NUM5 },
//# 						{ 'f', ScreenManager.KEY_NUM6 },
//# 						{ 'F', ScreenManager.KEY_NUM6 },
//# 						{ 'x', ScreenManager.KEY_NUM8 },
//# 						{ 'X', ScreenManager.KEY_NUM8 },
//# 
//# 						{ 'y', ScreenManager.KEY_NUM1 },
//# 						{ 'Y', ScreenManager.KEY_NUM1 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'b', ScreenManager.KEY_NUM7 },
//# 						{ 'B', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'u', ScreenManager.UP },
//# 						{ 'U', ScreenManager.UP },
//# 						{ 'h', ScreenManager.LEFT },
//# 						{ 'H', ScreenManager.LEFT },
//# 						{ 'j', ScreenManager.FIRE },
//# 						{ 'J', ScreenManager.FIRE },
//# 						{ 'k', ScreenManager.RIGHT },
//# 						{ 'K', ScreenManager.RIGHT },
//# 						{ 'n', ScreenManager.DOWN },
//# 						{ 'N', ScreenManager.DOWN },
//# 					 };
//# 			}
//# 			for ( byte i = 0; i < keys.length; ++i )
//# 				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
//# 
		//#else

			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
			final int[][] keys = {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );

		//#endif


		setScreen( SCREEN_MAIN_MENU );
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
	}


	protected final void changeLanguage( byte language ) {
	}
	
	
	protected int changeScreen( int screen ) throws Exception {
		Drawable nextScreen = null;

		byte softIndexLeft = TEXT_OK;
		byte softIndexRight	= TEXT_BACK;

		try {
			switch ( screen ) {
				case SCREEN_MAIN_MENU:
					final BasicMenu menu = new BasicMenu( myMenuListener, screen, FONT_DEFAULT, new int[] {
						TEXT_DEVICE,
						TEXT_SCREEN,
						TEXT_CONTENT_TYPES,
						TEXT_PROTOCOLS,
						TEXT_KEYS,
						TEXT_KEYS2,
						TEXT_OPTIONAL,

						//#if PUSH_REGISTRY == "true"
							TEXT_ALARM,
						//#endif
							
						TEXT_EXIT
					} );
					menu.setCursor( CURSOR, Menu.CURSOR_DRAW_AFTER_MENU, Graphics.VCENTER | Graphics.LEFT );
					nextScreen = menu;

					softIndexRight	= TEXT_EXIT;
				break;

				case SCREEN_LOADING:
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							Thread.sleep( 400 ); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

							loadScreen.setActive( false );

							setScreen( SCREEN_PROPERTIES );
						}

					}, FONT_DEFAULT, TEXT_LOADING );

				break;


				case SCREEN_CONTENT_TYPES:
				case SCREEN_PROPERTIES:
				case SCREEN_PROTOCOLS:
				case SCREEN_SCREEN:
				case SCREEN_OPTIONAL:
					nextScreen = loadTextScreen( screen );
				break;

				case SCREEN_KEYS:
					nextScreen = new KeyScreen( FONT_DEFAULT );
				break;
				
				case SCREEN_KEYS2:
					nextScreen = new KeyScreen2( FONT_DEFAULT );
					softIndexLeft = -1;
				break;

				//#if PUSH_REGISTRY == "true"
//				case SCREEN_ALARM:
//					final long TIME_INTERVAL = 15 * 1000;
//					long time = new Date().getTime();
//					try {
//						PushRegistry.registerAlarm( instance.getClass().getName(), time + TIME_INTERVAL );
//					} catch ( Exception e ) {
//						texts[ TEXT_ALARM ] = e.getClass().getName();
//						return changeScreen( SCREEN_MAIN_MENU );
//					}
//					exit();
//				return screen;
				//#endif
			}

			if ( softIndexLeft >= 0 )
				manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, new Label( FONT_DEFAULT, getText( softIndexLeft ) ) );
			else
				manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null );
			
			manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, new Label( FONT_DEFAULT, getText( softIndexRight ) ) );

			manager.setCurrentScreen( nextScreen );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif

			exit();
		}
		return screen;
	}
	
	
	private static final BasicTextScreen loadTextScreen( int index ) throws Exception {
		final StringBuffer buffer = new StringBuffer();
		
		switch ( index ) {
			case SCREEN_PROPERTIES:
				final String[] PROPERTIES_LIST = new String[] {
					"microedition.platform",
					"microedition.encoding",
					"microedition.configuration",
					"microedition.profiles",
					"microedition.locale",
					"microedition.io.file.FileConnection.version",
					"file.separator",
					"microedition.pim.version",
					"microedition.commports",
					"microedition.hostname",
					"microedition.smartcardslots",
					"microedition.location.version",
					"microedition.sip.version",
					"microedition.m3g.version",
					"microedition.jtwi.version",
					"wireless.messaging.sms.smsc",
					"wireless.messaging.mms.mmsc",
				};

				buffer.append( getText( TEXT_VENDOR_DETECTED ) );
				buffer.append( getVendor() );
				buffer.append( "\n\n" );

				buffer.append( "-----\n" );
				buffer.append( getMIDletVersion() );
				buffer.append( "\n" );
				buffer.append( ( InfoMIDlet ) getInstance() );
				buffer.append( "\n" );
				buffer.append( Display.getDisplay( getInstance() ).numColors() );
				buffer.append( "-----\n" );
				
				buffer.append( getText( TEXT_TOTAL_MEMORY_1 ) );
				buffer.append( Runtime.getRuntime().totalMemory() );
				buffer.append( "\n\n" );
				
				// aloca mem�ria at� causar OutOfMemory, e ent�o obt�m o segundo valor de mem�ria total (alguns
				// aparelhos possuem aloca��o din�mica, n�o informando o total real na 1� chamada)
//				Vector v;
//				System.gc();
//				long memory = Runtime.getRuntime().freeMemory();
//				try {
//					v = new Vector();
//
//					while ( memory < MEMORY_TEST_MAX ) {
//						v.addElement( new byte[ MEMORY_TEST_INC ] );
//						memory += MEMORY_TEST_INC;
//					}
//				} catch ( Throwable t ) {
//					v = null;
//				}
//				buffer.append( getText( TEXT_TOTAL_MEMORY_2 ) );
//				buffer.append( Math.max( memory, Runtime.getRuntime().totalMemory() ) );
//				buffer.append( "\n\n" );
//
				for ( int i = 0; i < PROPERTIES_LIST.length; ++i ) {
					final String PROPERTY = PROPERTIES_LIST[ i ];
					buffer.append( PROPERTY );
					buffer.append( ": " );
					buffer.append( System.getProperty( PROPERTY ) );
					buffer.append( "\n\n" );
				}
			break;
			
			case SCREEN_SCREEN:
				buffer.append( getText( TEXT_DOUBLE_BUFFER ) );
				buffer.append( ScreenManager.getInstance().isDoubleBuffered() );
				buffer.append( "\n" );
				buffer.append( getText( TEXT_POINTER_EVENTS ) );
				buffer.append( ScreenManager.getInstance().hasPointerEvents() );
				buffer.append( "\n" );
				buffer.append( getText( TEXT_POINTER_MOTION_EVENTS ) );
				buffer.append( ScreenManager.getInstance().hasPointerMotionEvents() );
				buffer.append( "\n" );
				buffer.append( getText( TEXT_REPEAT_EVENTS ) );
				buffer.append( ScreenManager.getInstance().hasRepeatEvents() );
				buffer.append( "\n" );
				buffer.append( getText( TEXT_SCREEN_WIDTH ) );
				buffer.append( ScreenManager.SCREEN_WIDTH );
				buffer.append( "\n" );
				buffer.append( getText( TEXT_SCREEN_HEIGHT ) );
				buffer.append( ScreenManager.SCREEN_HEIGHT );
				buffer.append( "\n" );
//				buffer.append( getText( TEXT_NUM_COLORS ) );
//				buffer.append( Display.getDisplay( instance ).numColors() );
//				buffer.append( "\n" );
//				buffer.append( getText( TEXT_ALPHA_CHANNELS ) );
//				buffer.append( Display.getDisplay( instance ).numAlphaLevels() );
				buffer.append( "\n" );
				buffer.append( getText( TEXT_VIBRATION ) );
				buffer.append( MediaPlayer.isVibrationSupported() );
				buffer.append( "\n\n" );
			break;
			
			case SCREEN_CONTENT_TYPES:
				buffer.append( TEXT_CONTENT_TYPES );

				try {
					final String[] CONTENT_TYPES_LIST = Manager.getSupportedContentTypes( null );

					for ( int i = 0; i < CONTENT_TYPES_LIST.length; ++i ) {
						buffer.append( CONTENT_TYPES_LIST[ i ] );
						buffer.append( "\n" );
					}
				} catch ( Exception e ) {
					buffer.append( "ERRO" );
				}
				buffer.append( "\n\n" );
			break;
			
			case SCREEN_PROTOCOLS:
				buffer.append( TEXT_PROTOCOLS );

				try {
					final String[] PROTOCOLS_LIST = Manager.getSupportedContentTypes( null );

					for ( int i = 0; i < PROTOCOLS_LIST.length; ++i ) {
						buffer.append( PROTOCOLS_LIST[ i ] );
						buffer.append( "\n" );
					}
				} catch ( Exception e ) {
					buffer.append( "ERRO" );
				}
				buffer.append( "\n\n" );
			break;

			case SCREEN_OPTIONAL:
				final String[] CLASSES = new String[] {
					"com.motorola.phonebook.PhoneBookRecord",
					"com.motorola.phone.Dialer",
					"com.motorola.multimedia.Lighting",
					"com.motorola.extensions.ScalableImage",
					"com.motorola.funlight.FunLight",
					"com.motorola.pim.Contact",
					"com.motorola.graphics.j3d.ActionTable",
					"com.samsung.util.Acceleration",
					"com.samsung.util.AudioClip",
					"com.samsung.immersion.VibeTonz",
					"mmpp.media.BackLight",
					"mmpp.lang.MathFP",
					"mmpp.media.MediaPlayer",
					"mmpp.phone.ContentsManager",
					"mmpp.microedition.lcdui.TextFieldX",
					"com.sonyericsson.mmedia.AMRPlayer",
					"com.sonyericsson.util.TempFile",
					"com.siemens.mp.lcdui.Image",
					"com.siemens.mp.game.Light",
					"com.siemens.mp.io.Connection",
					"com.siemens.mp.wireless.messaging.MessageConnection",
					"com.vodafone.v10.system.device.DeviceControl",
				};

				int counter = 0;
				for ( int i = 0; i < CLASSES.length; ++i ) {
					final String CLASS = CLASSES[ i ];
					try {
						Class.forName( CLASS );

						++counter;
						buffer.append( CLASS );
						buffer.append( "\n\n" );
					} catch ( Exception e ) {
					}
				}

				if ( counter == 0 )
					buffer.append( "NENHUMA API OPCIONAL DETECTADA" );

				buffer.append( "\n\n" );
			break;
		}
		
		return new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, buffer.toString(), false );
	}
	
	
	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				switch ( index ) {
					case MAIN_MENU_ENTRY_PROPERTIES:
//						setScreen( SCREEN_PROPERTIES );
						setScreen( SCREEN_LOADING );
					break;
					
					case MAIN_MENU_ENTRY_CONTENT_TYPES:
						setScreen( SCREEN_CONTENT_TYPES );
					break;
					
					case MAIN_MENU_ENTRY_PROTOCOLS:
						setScreen( SCREEN_PROTOCOLS );
					break;
					
					case MAIN_MENU_ENTRY_SCREEN:
						setScreen( SCREEN_SCREEN );
					break;
					
					case MAIN_MENU_ENTRY_KEYS:
						setScreen( SCREEN_KEYS );
					break;
					
					case MAIN_MENU_ENTRY_KEYS2:
						setScreen( SCREEN_KEYS2 );
					break;
					
					case MAIN_MENU_ENTRY_OPTIONAL:
						setScreen( SCREEN_OPTIONAL );
					break;

					//#if PUSH_REGISTRY == "true"
						case MAIN_MENU_ENTRY_ALARM:
							setScreen( SCREEN_ALARM );
						break;
					//#endif
					
					case MAIN_MENU_ENTRY_EXIT:
						exit();
					break;
				}
			break;
		}
	}

	// </editor-fold>


}
