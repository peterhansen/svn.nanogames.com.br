/**
 * KeyScreen.java
 * �2008 Nano Games.
 *
 * Created on 21/01/2008 21:44:34.
 */

package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;


/**
 *
 * @author Peter
 */
public final class KeyScreen extends RichLabel implements PointerListener {

	private int lastKeyPressed;

	private int lastKeyReleased;

	private final Point positionDragged = new Point();

	private final Point positionPressed = new Point();

	private final Point positionRelased = new Point();



	public KeyScreen( ImageFont font ) throws Exception {
		super( font, null, ScreenManager.SCREEN_WIDTH, null );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void keyPressed( int key ) {
		if ( key == lastKeyPressed )
			InfoMIDlet.setScreen( Constants.SCREEN_MAIN_MENU );

		lastKeyPressed = key;

		updateLabel();
	}


	public final void keyReleased( int key ) {
		lastKeyReleased = key;

		updateLabel();
	}


	private final void updateLabel() {
		final StringBuffer buffer = new StringBuffer();

		buffer.append( InfoMIDlet.getText( Constants.TEXT_KEY_PRESSED ) );
		buffer.append( lastKeyPressed );

		buffer.append( InfoMIDlet.getText( Constants.TEXT_KEY_RELEASED ) );
		buffer.append( lastKeyReleased );

		buffer.append( InfoMIDlet.getText( Constants.TEXT_KEY_STATE ) );
		buffer.append( ScreenManager.getInstance().getKeyStates() );

		buffer.append( InfoMIDlet.getText( Constants.TEXT_DRAGGED ) );
		buffer.append( positionDragged.x );
		buffer.append( ", " );
		buffer.append( positionDragged.y );

		buffer.append( InfoMIDlet.getText( Constants.TEXT_PRESSED ) );
		buffer.append( positionPressed.x );
		buffer.append( ", " );
		buffer.append( positionPressed.y );

		buffer.append( InfoMIDlet.getText( Constants.TEXT_RELEASED ) );
		buffer.append( positionRelased.x );
		buffer.append( ", " );
		buffer.append( positionRelased.y );

		setText( buffer.toString() );
	}


	public void onPointerDragged( int x, int y ) {
		positionDragged.set( x, y );
	}


	public void onPointerPressed( int x, int y ) {
		positionPressed.set( x, y );
	}


	public void onPointerReleased( int x, int y ) {
		positionRelased.set( x, y );
	}


	public final void update( int delta ) {
		super.update( delta );

		updateLabel();
	}

}
