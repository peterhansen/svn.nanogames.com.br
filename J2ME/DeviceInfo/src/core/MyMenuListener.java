/**
 * MyMenuListener.java
 * 
 * Created on May 3, 2009, 11:15:30 PM
 *
 */

package core;

import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;

/**
 *
 * @author Peter
 */
public final class MyMenuListener implements MenuListener {

	private final InfoMIDlet midlet;

	
	public MyMenuListener( InfoMIDlet midlet ) {
		this.midlet = midlet;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		midlet.onChoose( menu, id, index );
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		midlet.onItemChanged( menu, id, index );
	}

}
