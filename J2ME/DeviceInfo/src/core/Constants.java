/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on 21/01/2008 17:38:08.
 */


package core;

/**
 *
 * @author Peter
 */
public interface Constants {

	/** Quantidade m�xima de mem�ria testada: 256Mb */
	public static final int MEMORY_TEST_MAX = 256 << 20;

	/** Incremento na aloca��o de mem�ria de cada passo do teste. */
	public static final int MEMORY_TEST_INC = 1 << 12;

	public static final String PATH_RESOURCES = "/";

	public static final byte SCREEN_MAIN_MENU		= 0;
	public static final byte SCREEN_PROPERTIES		= 1;
	public static final byte SCREEN_SCREEN			= 2;
	public static final byte SCREEN_CONTENT_TYPES	= 3;
	public static final byte SCREEN_PROTOCOLS		= 4;
	public static final byte SCREEN_KEYS			= 5;
	public static final byte SCREEN_KEYS2			= 6;
	public static final byte SCREEN_OPTIONAL		= 7;
	public static final byte SCREEN_LOADING			= 8;

	//#if PUSH_REGISTRY == "true"
		public static final byte SCREEN_ALARM			= 9;
	//#endif

	public static final byte MAIN_MENU_ENTRY_PROPERTIES		= 0;
	public static final byte MAIN_MENU_ENTRY_SCREEN			= 1;
	public static final byte MAIN_MENU_ENTRY_CONTENT_TYPES	= 2;
	public static final byte MAIN_MENU_ENTRY_PROTOCOLS		= 3;
	public static final byte MAIN_MENU_ENTRY_KEYS			= 4;
	public static final byte MAIN_MENU_ENTRY_KEYS2			= 5;
	public static final byte MAIN_MENU_ENTRY_OPTIONAL		= 6;

	//#if PUSH_REGISTRY == "true"
		public static final byte MAIN_MENU_ENTRY_ALARM			= 7;
		public static final byte MAIN_MENU_ENTRY_EXIT			= 8;
	//#else
//# 		public static final byte MAIN_MENU_ENTRY_EXIT			= 7;
	//#endif


	public static final byte TEXT_DEVICE				= 0;
	public static final byte TEXT_SCREEN				= TEXT_DEVICE + 1;
	public static final byte TEXT_YES					= TEXT_SCREEN + 1;
	public static final byte TEXT_NO					= TEXT_YES + 1;
	public static final byte TEXT_SOUND					= TEXT_NO + 1;
	public static final byte TEXT_KEYS					= TEXT_SOUND + 1;
	public static final byte TEXT_KEYS2					= TEXT_KEYS + 1;
	public static final byte TEXT_EXIT					= TEXT_KEYS2 + 1;
	public static final byte TEXT_DOUBLE_BUFFER			= TEXT_EXIT + 1;
	public static final byte TEXT_POINTER_EVENTS		= TEXT_DOUBLE_BUFFER + 1;
	public static final byte TEXT_POINTER_MOTION_EVENTS = TEXT_POINTER_EVENTS + 1;
	public static final byte TEXT_REPEAT_EVENTS			= TEXT_POINTER_MOTION_EVENTS + 1;
	public static final byte TEXT_SCREEN_WIDTH			= TEXT_REPEAT_EVENTS + 1;
	public static final byte TEXT_SCREEN_HEIGHT			= TEXT_SCREEN_WIDTH + 1;
	public static final byte TEXT_CONTENT_TYPES			= TEXT_SCREEN_HEIGHT + 1;
	public static final byte TEXT_PROTOCOLS				= TEXT_CONTENT_TYPES + 1;
	public static final byte TEXT_ALPHA_CHANNELS		= TEXT_PROTOCOLS + 1;
	public static final byte TEXT_VIBRATION				= TEXT_ALPHA_CHANNELS + 1;
	public static final byte TEXT_NUM_COLORS			= TEXT_VIBRATION + 1;
	public static final byte TEXT_TOTAL_MEMORY_1		= TEXT_NUM_COLORS + 1;
	public static final byte TEXT_TOTAL_MEMORY_2		= TEXT_TOTAL_MEMORY_1 + 1;
	public static final byte TEXT_KEY_PRESSED			= TEXT_TOTAL_MEMORY_2 + 1;
	public static final byte TEXT_KEY_RELEASED			= TEXT_KEY_PRESSED + 1;
	public static final byte TEXT_KEY_STATE				= TEXT_KEY_RELEASED + 1;
	public static final byte TEXT_OPTIONAL				= TEXT_KEY_STATE + 1;
	public static final byte TEXT_CURSOR				= TEXT_OPTIONAL + 1;
	public static final byte TEXT_DRAGGED				= TEXT_CURSOR + 1;
	public static final byte TEXT_PRESSED				= TEXT_DRAGGED + 1;
	public static final byte TEXT_RELEASED				= TEXT_PRESSED + 1;
	public static final byte TEXT_OK					= TEXT_RELEASED + 1;
	public static final byte TEXT_VENDOR_DETECTED		= TEXT_OK + 1;
	public static final byte TEXT_ALARM					= TEXT_VENDOR_DETECTED + 1;
	public static final byte TEXT_BACK					= TEXT_ALARM + 1;
	public static final byte TEXT_LOADING				= TEXT_BACK + 1;

	public static final byte TEXT_TOTAL		= TEXT_LOADING + 1;
}
