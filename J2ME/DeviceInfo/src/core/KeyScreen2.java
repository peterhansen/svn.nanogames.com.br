/**
 * KeyScreen.java
 * �2008 Nano Games.
 *
 * Created on 21/01/2008 21:44:34.
 */

package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.ScreenManager;


/**
 *
 * @author Peter
 */
public final class KeyScreen2 extends BasicTextScreen {


	public KeyScreen2( ImageFont font ) throws Exception {
		super( Constants.SCREEN_MAIN_MENU, font, null, false );
	}


	public final void keyPressed( int key ) {

		label.setText( label.getText() + "P: " + key + ", " + ( char ) key + "\n" );

		updateText();
	}


	public final void keyReleased( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				// tratamento deve ser feito em keyReleased pois teclas back dos BlackBerry s� enviam evento keyReleased
				InfoMIDlet.setScreen( Constants.SCREEN_MAIN_MENU );
			break;
		}
		label.setText( label.getText() + "R: " + key + ", " + ( char ) key + "\n" );

		updateText();
	}


	private final void updateText() {
		final int OFFSET = label.getTextTotalHeight();

		if ( OFFSET > size.y ) {
			label.setTextOffset( -OFFSET + size.y );
		}
	}

}
