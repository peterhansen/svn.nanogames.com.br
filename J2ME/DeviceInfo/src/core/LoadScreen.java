package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;


final class LoadScreen extends Label implements Updatable {

	/** Intervalo de atualiza��o do texto. */
	private static final short CHANGE_TEXT_INTERVAL = 600;

	private long lastUpdateTime;

	private static final byte MAX_DOTS = 4;

	private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

	private byte dots;

	private Thread loadThread;

	private final LoadListener listener;

	private boolean painted;

//	private final byte previousScreen;

	private boolean active = true;


	public LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
		super( font, AppMIDlet.getText( loadingTextIndex ) );
//		previousScreen = currentScreen;
		this.listener = listener;
		setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		ScreenManager.setKeyListener( null );
		ScreenManager.setPointerListener( null );
	}


	public final void update( int delta ) {
		final long interval = System.currentTimeMillis() - lastUpdateTime;
		if ( interval >= CHANGE_TEXT_INTERVAL ) {
			// os recursos do jogo s�o carregados aqui para evitar sobrecarga do m�todo loadResources, o que
			// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
			if ( loadThread == null ) {
				if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
					ScreenManager.setKeyListener( null );
					ScreenManager.setPointerListener( null );
					final LoadScreen loadScreen = this;
					loadThread = new Thread() {

						public final void run() {
							try {
								System.gc();
								listener.load( loadScreen );
							} catch ( Throwable e ) {
//								InfoMIDlet.setScreen( previousScreen );
							}
						}
					};
					loadThread.start();
				}
			} else if ( active ) {
				lastUpdateTime = System.currentTimeMillis();
				dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
				String temp = InfoMIDlet.getText( Constants.TEXT_LOADING );
//					System.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
				for ( byte i = 0; i < dots; ++i ) {
					temp += '.';
				}
				setText( temp );
				try {
					// permite que a thread de carregamento dos recursos continue sua execu��o
					Thread.sleep( CHANGE_TEXT_INTERVAL );
				} catch ( Exception e ) {
				}
			}
		}
	}


	// fim do m�todo update( int )
	public final void paint( Graphics g ) {
		super.paint( g );
		painted = true;
	}


	public final void setActive( boolean a ) {
		active = a;
	}
}
