package core;


interface LoadListener {

	public void load( final LoadScreen loadScreen ) throws Exception;
}
