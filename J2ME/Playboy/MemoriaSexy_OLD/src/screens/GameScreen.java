/**
 * GameScreen.java
 *
 * Created on Aug 24, 2010 10:58:29 AM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.BigCase;
import core.Case;
import core.ConfettiEmitter;
import core.Constants;
import core.Highlight;
import core.Justus;
import core.ParticleEmitter;
import core.SparkEmitter;
import core.Transition;
import core.ValuesBox;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import core.MenuLabel;
import core.Stage;
import java.util.Timer;
import java.util.TimerTask;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.Mutex;
import javax.microedition.lcdui.Graphics;
//#endif

/**
 *
 * @author peter
 */
public final class GameScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{

	/***/
	public static final int VALUE_MIN = REAL >> 1;
	/***/
	public static final int VALUE_MILLION = 1000000 * REAL;

	/** Valores possíveis de cada maleta, representados em centavos. */
	public static final int[] VALUES = {
		      REAL >> 1, // R$ 0,50
		      REAL, // R$ 1
		      5 * REAL,
		     10 * REAL, // R$ 10
		     25 * REAL,
		     50 * REAL,
		     75 * REAL,
		    100 * REAL, // R$ 100
		    200 * REAL,
		    300 * REAL,
		    400 * REAL,
		    500 * REAL,
		    750 * REAL,
		   1000 * REAL, // R$ 1.000
		   5000 * REAL,
		  10000 * REAL, // R$ 10.000
		  25000 * REAL,
		  50000 * REAL,
		  75000 * REAL,
		 100000 * REAL, // R$ 100.000
		 200000 * REAL,
		 300000 * REAL,
		 400000 * REAL,
		 500000 * REAL,
		 750000 * REAL,
		1000000 * REAL, // R$ 1 milhão
	};

	private static final short SORT_TRIES = CASES_TOTAL << 5;

	private static final byte[] CASES_PER_LINE = { 7, 6, 7, 6 };

	//#if MILLION_VERSION == "true"
//# 		/** Número de maletas a escolher na primeira rodada. */
//# 		private static final byte CASES_PER_TURN_INITIAL = 1;
//# 		/** Histórico de ofertas do negociador. */
//# 		private final int[] bankerOffers = new int[ 25 ];
	//#else
		/** Número de maletas a escolher na primeira rodada. */
		private static final byte CASES_PER_TURN_INITIAL = 6; // TODO 6
		/** Histórico de ofertas do negociador. */
		private final int[] bankerOffers = new int[ 11 ];
	//#endif

	private static final byte LINE_CHANGE = 6;

	private static final short TIME_LOOP_ANIMATION = 5000;
	private static final short TIME_CAMERA_MOVE = 1600;

	private static final byte STATE_NONE						= -1;
	private static final byte STATE_INTRO						= 0;
	private static final byte STATE_CHOOSE_FIRST_CASE			= STATE_INTRO + 1;
	private static final byte STATE_CHOOSE_NEXT_CASE			= STATE_CHOOSE_FIRST_CASE + 1;
	private static final byte STATE_HIGHLIGHT_CASE				= STATE_CHOOSE_NEXT_CASE + 1;
	private static final byte STATE_SHOW_CASE_CONTENT			= STATE_HIGHLIGHT_CASE + 1;
	private static final byte STATE_SHOW_LAST_CASE_CONTENT		= STATE_SHOW_CASE_CONTENT + 1;
	private static final byte STATE_SHOW_VALUES_BEFORE_BANKER	= STATE_SHOW_LAST_CASE_CONTENT + 1;
	private static final byte STATE_BANKER_CALLING				= STATE_SHOW_VALUES_BEFORE_BANKER + 1;
	private static final byte STATE_BANKER_OFFER				= STATE_BANKER_CALLING + 1;
	private static final byte STATE_DEAL_OR_NO_DEAL				= STATE_BANKER_OFFER + 1;
	private static final byte STATE_LAST_CASE					= STATE_DEAL_OR_NO_DEAL + 1;
	private static final byte STATE_GAME_OVER_DEAL				= STATE_LAST_CASE + 1;
	private static final byte STATE_GAME_OVER_LAST_CASE			= STATE_GAME_OVER_DEAL + 1;
	private static final byte STATE_SHOW_REMAINING_CASES		= STATE_GAME_OVER_LAST_CASE + 1;
	private static final byte STATE_SHOW_REMAINING_CASES_STOP	= STATE_SHOW_REMAINING_CASES + 1;
	private static final byte STATE_GAME_OVER_FINAL_COMMENTS	= STATE_SHOW_REMAINING_CASES_STOP + 1;

	private byte state;

	/** Tempo mínimo de disponibilidade do cursor para que se possa escolher uma maleta. */
	private static final short TIME_CURSOR_AVAILABLE_MIN = 1100;

	private boolean cursorAvailable;

	/***/
	private long cursorAvailableSince;

	private final Justus justus;

	private final Case[] cases = new Case[ CASES_TOTAL ];

	private byte lastCaseChosen;

	/** Maleta escolhida pelo jogador na 1ª rodada. */
	private byte playerCaseIndex = -1;

	private Drawable playerCase;

	private final BigCase bigCase;

	private final ValuesBox valuesBox;

	private final Highlight highlight;

	/** Mala selecionada atualmente. */
	private byte currentCase;

	private final byte playerCaseDrawableIndex;

	private byte currentTurn;

	private boolean firstCaseInTurn = true;

	private byte remainingCases = CASES_PER_TURN_INITIAL;

	private final Point cameraPosition = new Point();

	private final Point cameraLimit = new Point();

	private final UpdatableGroup stage;

	private BankerScreen banker;

	private final byte bankerDrawableIndex;

	private final Transition transition;

	//#if TOUCH == "true"
		private final Point firstPointerPos = new Point();
		private final Point lastPointerPos = new Point();

		// evita que a tela possa ser arrastada (scroll) durante o desenho de tela, o que causa problemas de
		// visualização em aparelhos que usam flushGraphics (ver descrição na classe ScreenManager)
		private final Mutex cameraMutex = new Mutex();
	//#endif

	private final BezierCurve cameraTweener = new BezierCurve();

	private static final byte CAMERA_MOVE_STATE_NONE			= 0;
	private static final byte CAMERA_MOVE_STATE_LOOP			= 1;
	private static final byte CAMERA_MOVE_STATE_MOVE_AND_STOP	= 2;

	private final static byte CASE_WORST = 0;
	private final static byte CASE_BAD = 1;
	private final static byte CASE_MEDIUM = 2;
	private final static byte CASE_GOOD = 3;
	private final static byte CASE_BEST = 4;

	private byte cameraState;

	private int cameraAccTime;
	private int cameraTotalTime;

	private int accTime;

	private final Stage stg;
	
	/***/
	private int score = -1;
	
	private boolean bankerCalling;

	private WinScreen winScreen;

	//#if TOUCH == "true"
		private final DrawableImage pauseIcon;
		private final DrawableImage valuesIcon;
	//#endif
		
	/** Intervalo entre as chamadas do telefone do negociador. */
	private static final short PHONE_RING_INTERVAL = 1900;

	//
	public GameScreen() throws Exception {
		super( 20 );

		stage = new UpdatableGroup( CASES_TOTAL + 10 );
		stage.setViewport( new Rectangle() );
		insertDrawable( stage );

		stg = new Stage();
		stg.setSize( STAGE_WIDTH, STAGE_HEIGHT );
		stage.insertDrawable( stg );

		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			cases[ i ] = new Case();
			stage.insertDrawable( cases[ i ] );
		}

		highlight = new Highlight( stage.getPosition(), null );
		highlight.setVisible( false );
		stage.insertDrawable( highlight );

		GameMIDlet.log( "highlight" );

		valuesBox = new ValuesBox( this, stage.getViewport() );
		playerCaseDrawableIndex = ( byte ) ( insertDrawable( valuesBox ) );
		bankerDrawableIndex = ( byte ) ( playerCaseDrawableIndex + 1 );

		//#if TOUCH == "true"
			// ícone de pausa para touch screen
			if ( ScreenManager.getInstance().hasPointerEvents() ) {
				pauseIcon = new DrawableImage( PATH_IMAGES + "pause.png" );
				insertDrawable( pauseIcon, playerCaseDrawableIndex );
			} else {
				pauseIcon = null;
			}
		//#endif

		//#if TOUCH == "true"
			// ícone da tela de valores para touch screen
			if ( ScreenManager.getInstance().hasPointerEvents() ) {
				valuesIcon = new DrawableImage( PATH_IMAGES + "values.png" );
				insertDrawable( valuesIcon, playerCaseDrawableIndex + 1 );
			} else {
				valuesIcon = null;
			}
		//#endif

		GameMIDlet.log( "valuesBox" );

		justus = new Justus( stage.getViewport() );
		
		GameMIDlet.log( "justus" );

		bigCase = new BigCase( this, stage.getPosition(), justus.getBoxHeight() );
		insertDrawable( bigCase );

		GameMIDlet.log( "bigCase" );

		transition = new Transition( this );
		insertDrawable( transition );

		insertDrawable( justus );

		/*final TimerTask task = new TimerTask() {

			public void run() {
				keyPressed( ScreenManager.FIRE );
			}

		};
		//task.
		final Timer t = new Timer();
		t.scheduleAtFixedRate(task, 1500, 50);*/
	}

	private static final int[] GetRandomValues() {
		final int[] values = new int[ VALUES.length ];
		System.arraycopy( VALUES, 0, values, 0, VALUES.length );

		for ( int i = 0; i < SORT_TRIES; ++i ) {
			final int randomIndex1 = NanoMath.randInt( CASES_TOTAL );
			int randomIndex2;
			do {
				randomIndex2 = NanoMath.randInt( CASES_TOTAL );
			} while ( randomIndex1 == randomIndex2 );
			
			final int temp = values[ randomIndex1 ];
			values[ randomIndex1 ] = values[ randomIndex2 ];
			values[ randomIndex2 ] = temp;
		}
		//#if DEBUG == "true"
			System.out.println( "CASES: " );
			for ( byte i = 0; i < CASES_TOTAL; ++i ) {
				System.out.println( ( i + 1 ) + " -> " + GameMIDlet.getCurrencyString( values[ i ] ) );
			}
		//#endif

		return values;
	}

	public final void setSize( int width, int height ) {
		try {
			super.setSize( width, height );
			defineReferencePixel( ANCHOR_CENTER );

			final Rectangle v = stage.getViewport();
			v.set( v.x, 0, v.width, height );

			bigCase.setRefPixelPosition( getRefPixelPosition() );

			stage.setSize( NanoMath.max ( STAGE_WIDTH, width ), NanoMath.max ( STAGE_HEIGHT, height ) );
			stg.setSize( width, height );

			cameraLimit.set( Math.min( 0, width - STAGE_WIDTH ), Math.min( 0, height - stage.getHeight() ) );

			highlight.setSize( stage.getSize() );

			final int LINE_HEIGHT = STAGE_STAIRS_TOTAL_HEIGHT / CASES_PER_LINE.length;
			for ( int line = 0, caseIndex = 0, y = STAGE_STAIR_BEGIN; line < CASES_PER_LINE.length; ++line, y += LINE_HEIGHT ) {
				final byte CASES_IN_LINE = CASES_PER_LINE[ line ];
				final int START_X = ( ( cases[ 0 ].getWidth() >> 1 ) +STAGE_WIDTH - ( CASES_IN_LINE * GIRL_WIDTH ) ) >> 1;

				for ( int j = 0, x = START_X; j < CASES_IN_LINE; ++j, ++caseIndex, x += GIRL_WIDTH ) {
					cases[ caseIndex ].setRefPixelPosition( x, y );
				}
			}

			valuesBox.setSize( size );
			justus.setSize( size );

			if ( banker != null )
				banker.setSize( size );
			if ( winScreen != null )
				winScreen.setSize( size );

			//#if TOUCH == "true"
				if ( pauseIcon != null ) {
					pauseIcon.setPosition( width - pauseIcon.getWidth(), 0 );
					valuesIcon.setPosition( 0, height - justus.getBoxHeight() - valuesIcon.getHeight() );
				}
			//#endif

			setCameraPosition( cameraPosition );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}


	private final void setCameraLoop() {
		cameraTotalTime = TIME_LOOP_ANIMATION;
		cameraState = CAMERA_MOVE_STATE_LOOP;
		cameraTweener.origin.set( NanoMath.min( 0, ( cameraLimit.x ) >> 1 ), cameraPosition.y );
		cameraTweener.destiny.set( cameraTweener.origin.x, cameraPosition.y );

		cameraTweener.control1.set( ( cameraLimit.x >> 1 ) + getWidth(), cameraPosition.y );
		cameraTweener.control2.set( ( cameraLimit.x >> 1 ) - getWidth(), cameraPosition.y );
	}


	private final void setCurrentCase( int c, boolean considerCursorAvailability ) {
		if ( c < 0 ) {
			if ( currentCase >= 0 )
				cases[ currentCase ].setActive( false );
		} else if ( !considerCursorAvailability || isCursorAvailable() ) {
			final boolean crescent = c > currentCase;

			int i = ( c + CASES_TOTAL ) % CASES_TOTAL;
			for ( ; i != currentCase; i = ( CASES_TOTAL + ( crescent ? i + 1 : i - 1 ) ) % CASES_TOTAL ) {
				if ( cases[ i ].isVisible() )
					break;
			}
			if ( currentCase >= 0 )
				cases[ currentCase ].setActive( false );

			currentCase = ( byte ) i;
			cases[ currentCase ].setActive( true );

			final Point caseCenter = cases[ i ].getCenterPosition();
			moveCameraTo( ( getWidth() >> 1 ) - caseCenter.x, ( getHeight() >> 1 ) - caseCenter.y );

			//#if DEBUG == "true"
				System.out.println( "Current case: " + ( currentCase + 1 ) + " -> " + GameMIDlet.getCurrencyString( cases[ i ].getValue() ) );
			//#endif
		}
		if ( considerCursorAvailability )
			playTheme();
	}


	private final void setCameraPosition( Point p ) {
		setCameraPosition( p.x, p.y );
	}


	private final void setCameraPosition( int x, int y ) {
		cameraPosition.set( NanoMath.clamp( x, cameraLimit.x, 0 ), NanoMath.clamp( y, cameraLimit.y, 0 ) );
		stage.setPosition( cameraPosition );
	}


	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_GAME_OVER_DEAL:
			case STATE_GAME_OVER_LAST_CASE:
				justus.changeExpression(NanoMath.randInt( 100 ) < 50 ? Justus.EXPRESSION_FELIZ : Justus.EXPRESSION_NORMAL );
			case STATE_INTRO:
			case STATE_LAST_CASE:
			case STATE_GAME_OVER_FINAL_COMMENTS:
				switch ( key ) {
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						if ( justus.isLastMessageShown( true ) )
							stateEnd();
						else {
							justus.showNextMessage();
						}
					break;
				}
			break;

			case STATE_SHOW_REMAINING_CASES:
				switch ( key ) {
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						if ( justus.isLastMessageShown( true ) )
							setState( STATE_SHOW_REMAINING_CASES_STOP );
						else
							justus.showNextMessage();
					break;
				}
			break;

			case STATE_CHOOSE_FIRST_CASE:
			case STATE_CHOOSE_NEXT_CASE:
				switch ( key ) {
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						setCurrentCase( ( currentCase + CASES_TOTAL - 1 ) % CASES_TOTAL, true );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						setCurrentCase( currentCase + 1, true );
					break;

					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						setCurrentCase( currentCase - LINE_CHANGE, true );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						setCurrentCase( currentCase + LINE_CHANGE, true );
					break;

					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						if ( justus.isLastMessageShown( true ) ) {
							if ( !valuesBox.isHiding() && currentTurn == 1 && !justus.isHiding() )
								justus.hide();
							else if ( isCursorAvailable() )
								stateEnd();
							else
								setCursorAvailable( true );
						} else {
							if ( currentTurn == 1 && !valuesBox.isHiding() ) {
								// se Justus estiver na penúltima mensagem (explicando a tabela de valores), o faz sumir
								if ( justus.getRemainingMessagesCount() > 1 ) {
									justus.showNextMessage();
								} else if ( justus.isHiding() ) {
									valuesBox.setState( ValuesBox.STATE_HIDING );
									justus.show();
									justus.showNextMessage();
								} else {
									justus.showNextMessage();
									justus.hide();
								}
							} else {
								justus.showNextMessage();
								if ( justus.isLastMessageShown( false ) ) {
									if ( state == STATE_CHOOSE_FIRST_CASE && currentCase < 0 )
										setCurrentCase( NanoMath.randInt( CASES_TOTAL ), false );

									setCursorAvailable( true );
								}
							}
						}
					break;

					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_NUM0:
						if ( valuesBox.toggle() )
							justus.hide();
						else
							justus.show();
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						if ( valuesBox.getState() == ValuesBox.STATE_HIDDEN ) {
							unloadBanker();
							GameMIDlet.setScreen( SCREEN_PAUSE );
						} else {
							valuesBox.setState( ValuesBox.STATE_HIDING );
							justus.show();
						}
					break;
				}
			break;

			case STATE_SHOW_VALUES_BEFORE_BANKER:
				if ( valuesBox.getState() == ValuesBox.STATE_SHOWN )
					valuesBox.setState( ValuesBox.STATE_HIDING );
			break;

			case STATE_SHOW_CASE_CONTENT:
			case STATE_SHOW_LAST_CASE_CONTENT:
				switch ( key ) {
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						if ( justus.isLastMessageShown( true ) ) {
							if ( bigCase.canBeClosed() )
								bigCase.setState( currentTurn == 0 ? BigCase.STATE_HIDE : BigCase.STATE_CLOSE_AND_HIDE );
						} else {
							justus.showNextMessage();
						}
					break;
				}
			break;

			case STATE_BANKER_CALLING:
				//if ( accTime >= TIME_BANKER_CALLING_MIN )
					stateEnd();
				//else
			break;

			case STATE_DEAL_OR_NO_DEAL:
				switch ( key ) {
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_NUM0:
						if ( valuesBox.toggle() )
							justus.hide();
						else
							justus.show();
					break;

/**//*TODO
					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						GameScreen.log("lalalalala");
					break;/**/

					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						if ( !justus.isHiding() ) {
							if ( justus.isLastMessageShown( true ) )
								justus.hide();
							else
								justus.showNextMessage();
							break;
						}

						// evita que o jogador possa confirmar a opção com a tela de valores na frente
						if ( !valuesBox.isHiding() ) {
							valuesBox.toggle();
							break;
						}
						// sem else e sem break mesmo
					default:
						banker.keyPressed( key );
				}
			break;
		}
	} // fim do método keyPressed( int )


	public final void keyReleased( int key ) {
		switch ( state ) {
			case STATE_DEAL_OR_NO_DEAL:
				banker.keyReleased( key );
			break;
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
		if ( deviceEvent ) {
			switch ( state ) {
				case STATE_BANKER_CALLING:
				case STATE_BANKER_OFFER:
				case STATE_DEAL_OR_NO_DEAL:
					bigCase.unload();
				break;

				case STATE_CHOOSE_FIRST_CASE:
				case STATE_CHOOSE_NEXT_CASE:
					if ( !GameMIDlet.isLowMemory() )
						GameMIDlet.setScreen( SCREEN_PAUSE );
				break;

//				default:
//					unloadBanker();
//				break;
			}
		}
		MediaPlayer.free();
	}


	public final void showNotify( boolean deviceEvent ) {
		sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		switch ( state ) {
			case STATE_CHOOSE_NEXT_CASE:
			case STATE_CHOOSE_FIRST_CASE:
				playTheme();
			break;

			case STATE_BANKER_OFFER:
			case STATE_DEAL_OR_NO_DEAL:
				MediaPlayer.play( SOUND_BANKER, MediaPlayer.LOOP_INFINITE );
			break;
		}
	}


	private final void stateEnd() {
		switch ( state ) {
			case STATE_INTRO:
				setState( STATE_CHOOSE_FIRST_CASE );
			break;

			case STATE_CHOOSE_FIRST_CASE:
				if(stg.colorIsBlue()) {
					if ( chooseCase( currentCase, false, false ) ) {
						setState( STATE_HIGHLIGHT_CASE );
					}
				}
			break;

			case STATE_CHOOSE_NEXT_CASE:
				if(stg.colorIsBlue()) {
					if ( chooseCase( currentCase, true, false ) ) {
						setState( STATE_HIGHLIGHT_CASE );
					}
				}
			break;

			case STATE_HIGHLIGHT_CASE:
				setState( STATE_SHOW_CASE_CONTENT );
			break;

			case STATE_SHOW_CASE_CONTENT:
				highlight.setVisible( false );
				if ( currentTurn > 0 ) {
					--remainingCases;
					if ( remainingCases > 0 )
						setState( STATE_CHOOSE_NEXT_CASE );
					else
						setState( STATE_SHOW_VALUES_BEFORE_BANKER );
				} else {
					++currentTurn;
					setState( STATE_CHOOSE_NEXT_CASE );
				}
			break;

			case STATE_SHOW_VALUES_BEFORE_BANKER:
				setState( STATE_BANKER_CALLING );
			break;

			case STATE_SHOW_LAST_CASE_CONTENT:
				setState( STATE_GAME_OVER_LAST_CASE );
			break;

			case STATE_BANKER_CALLING:
				if(justus.isLastMessageShown(true)) {
					if( bankerCalling )
						answerBankerCall();
					else if( !transition.isVisible() )
						setState( STATE_DEAL_OR_NO_DEAL );
				} else {
					justus.showNextMessage();
				}
			break;

			case STATE_DEAL_OR_NO_DEAL:
				if(!transition.isVisible()) {
					stage.setVisible( true );
					transition.doTransition( banker, stage );
					GameMIDlet.log( " [ begin transition (banker, stage)" );

					firstCaseInTurn = true;
					remainingCases = ( byte ) Math.max( CASES_PER_TURN_INITIAL - currentTurn, 1 );
					++currentTurn;

					if ( valuesBox.getValuesLeftCount() >= 2 )
						setState( STATE_CHOOSE_NEXT_CASE );
					else
						setState( STATE_LAST_CASE );
				}
			break;

			case STATE_LAST_CASE:
				setState( STATE_SHOW_LAST_CASE_CONTENT );
			break;

			case STATE_GAME_OVER_DEAL:
				setState( STATE_SHOW_REMAINING_CASES );
			break;

			case STATE_SHOW_REMAINING_CASES:
				removeCase( currentCase );
				if ( valuesBox.getValuesLeftCount() > 1 ) {
					setCurrentCase( currentCase + 1, false );
					chooseCase( currentCase, true, true );
				} else {
					setState( STATE_GAME_OVER_FINAL_COMMENTS );
				}
			break;

			case STATE_SHOW_REMAINING_CASES_STOP:
				setState( STATE_GAME_OVER_FINAL_COMMENTS );
			break;

			case STATE_GAME_OVER_LAST_CASE:
			case STATE_GAME_OVER_FINAL_COMMENTS:
				setState( STATE_NONE );
				unloadBanker();
				removeDrawable( winScreen );
				winScreen = null;
				GameMIDlet.onGameOver( getScore(), true );
			break;
		}
	}

	
	/**
	 *
	 * @param index
	 * @param eliminateValue
	 */
	private final boolean chooseCase( int index, boolean eliminateValue, boolean quickMode ) {
		if ( valuesBox.getState() == ValuesBox.STATE_HIDDEN ) {
			if ( !quickMode )
				MediaPlayer.play( SOUND_CASE_CHOOSE );
			
			if ( state == STATE_CHOOSE_FIRST_CASE ) {
				playerCaseIndex = ( byte ) index;
			} else {
				lastCaseChosen = ( byte ) index;
			}

			if ( eliminateValue ) {
				bigCase.setCase( index + 1, cases[ index ].getValueString(), getSoundIndex( cases[ index ].getValue() ) );
				valuesBox.eliminateValue( cases[ index ].getValue() );
				firstCaseInTurn = false;
				
				if ( !quickMode ) {
					// Justus fala com a modelo para abrir a maleta
					justus.appendMessage( replaceString( TEXT_JUSTUS_BEFORE_OPEN_CASE_1 + NanoMath.randInt( TEXT_JUSTUS_BEFORE_OPEN_CASE_TOTAL ),
											new String[] { cases[ index ].getGirlName(), String.valueOf( index + 1 ) } ) );
					justus.showNextMessage();
				}
			} else {
				bigCase.setCase( index + 1, null, -1 );
			}

			highlight.setTarget( cases[ index ].getCenterPosition(), quickMode ? Highlight.TIME_ANIMATION_QUICK : Highlight.TIME_ANIMATION_DEFAULT );

			return true;
		} else {
			valuesBox.setState( ValuesBox.STATE_HIDING );
			justus.show();
			return false;
		}
	}


	private final byte caseAnalysis(int value) {
		if ( value >= valuesBox.getBiggestValueLeft() )
			return CASE_BEST;
		if ( value <= valuesBox.getLowValueLeft() )
			return CASE_WORST;
		if ( value <= valuesBox.getNowBiggestLowValue() )
			return CASE_BAD;
		if ( value <= valuesBox.getNowBiggestMediumValue() )
			return CASE_MEDIUM;
		else
			return CASE_GOOD;
	}


	private final byte overallCaseAnalysis(int value) {
		if ( value == VALUES[ CASES_TOTAL-1 ] )
			return CASE_BEST;
		if ( value == VALUES[ 0 ] )
			return CASE_WORST;
		if ( value <= getBiggestOverallLowValue() )
			return CASE_BAD;
		if ( value <= getBiggestOverallMediumValue() )
			return CASE_MEDIUM;
		else
			return CASE_GOOD;
	}


	public static final int getBiggestOverallLowValue() {
		return VALUES[ 14 ];
	}


	public static final int getBiggestOverallMediumValue() {
		return VALUES[ 18 ];
	}

	
	/**
	 * Gera um comentário sobre a última maleta retirada.
	 * @return String com o comentário.
	 */
	private final void commentCase() {
		final int value = cases[ lastCaseChosen ].getValue();
		switch ( state ) {
			case STATE_SHOW_CASE_CONTENT:
				String comment = null;

				switch(caseAnalysis(value)) {
					case CASE_WORST:{
						comment = replaceString( TEXT_JUSTUS_GOOD_RESULT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_GOOD_TOTAL ), GameMIDlet.getCurrencyString( value ) );
						final int randomComment = TEXT_JUSTUS_BEST_RESULT_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_BEST_RESULT_COMMENT_TOTAL );
						comment += replaceString( randomComment, GameMIDlet.getCurrencyString( value ) );
						justus.changeExpression(Justus.EXPRESSION_FELIZ);
						break;
					}
					case CASE_BAD: {
						comment = replaceString( TEXT_JUSTUS_GOOD_RESULT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_GOOD_TOTAL ), GameMIDlet.getCurrencyString( value ) );
						final int randomComment = TEXT_JUSTUS_GOOD_RESULT_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_GOOD_COMMENTS_TOTAL );
						comment += replaceString( randomComment, GameMIDlet.getCurrencyString( valuesBox.getBiggestValueLeft() ) );
						justus.changeExpression(Justus.EXPRESSION_FELIZ);
						break;
					}
					case CASE_MEDIUM: {
						comment = replaceString( TEXT_JUSTUS_MEDIUM_RESULT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_MEDIUM_TOTAL ), GameMIDlet.getCurrencyString( value ) );
						justus.changeExpression(Justus.EXPRESSION_NORMAL);
						break;
					}
					case CASE_GOOD: {
						comment = replaceString( TEXT_JUSTUS_BAD_RESULT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_BAD_TOTAL ), GameMIDlet.getCurrencyString( value ) );
						final int randomComment = TEXT_JUSTUS_BAD_RESULT_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_BAD_COMMENTS_TOTAL );
						comment += replaceString( randomComment, GameMIDlet.getCurrencyString( value ) );
						justus.changeExpression(Justus.EXPRESSION_REZANDO);
						break;
					}
					case CASE_BEST: {
						comment = replaceString( TEXT_JUSTUS_BAD_RESULT_1 + NanoMath.randInt( TEXT_JUSTUS_PLAY_BAD_TOTAL ), GameMIDlet.getCurrencyString( value ) );
						final int randomComment = TEXT_JUSTUS_WORST_RESULT_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_WORST_RESULT_COMMENT_TOTAL );
						comment += replaceString( randomComment, GameMIDlet.getCurrencyString( value ) );
						justus.changeExpression(Justus.EXPRESSION_QUEQUEISSO);
						break;
					}
					//#if DEBUG == "true"
						default:
							throw new IllegalStateException();
					//#endif
				}

				justus.appendMessage( comment );

				final int extraCommentsChance = NanoMath.randInt( 100 );
				if ( extraCommentsChance < 20 && currentTurn < 9 ) {
					justus.appendMessage( replaceString( TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_1 + NanoMath.randInt( TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_TOTAL ), String.valueOf( valuesBox.getValuesLeftCount() ) ) );
				} else if ( extraCommentsChance < 50 ) {
					final Point biggest = valuesBox.getBigValuesLeft();
					if ( biggest.y == 0 )
						justus.appendMessage( replaceString( TEXT_JUSTUS_NO_MORE_GOOD_CASES, new String[] { String.valueOf( biggest.y ), GameMIDlet.getCurrencyString( biggest.x ) } ) );
					else if ( biggest.y == 100 )
						justus.appendMessage( replaceString( TEXT_JUSTUS_NO_MORE_BAD_CASES, new String[] { String.valueOf( biggest.y ), GameMIDlet.getCurrencyString( biggest.x ) } ) );
					else
						justus.appendMessage( replaceString( TEXT_JUSTUS_YOU_HAVE_CHANCE_1 + NanoMath.randInt( TEXT_JUSTUS_YOU_HAVE_CHANCE_TOTAL ), new String[] { String.valueOf( biggest.y ), GameMIDlet.getCurrencyString( biggest.x ) } ) );
				}
			break;

			case STATE_SHOW_LAST_CASE_CONTENT:
				final String scoreString = GameMIDlet.getCurrencyString( getScore() );
				if ( getScore() == VALUES[ 0 ] ) {
					// jogador ganhou o valor mínimo
					justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_MIN_VALUE_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_MIN_TOTAL ) ) );
				} else if ( getScore() == VALUES[ CASES_TOTAL - 1 ] ) {
					// jogador ganhou o prêmio máximo
					justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_MAX_VALUE_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_MAX_TOTAL ) ) );
				} else {
					justus.appendMessage( replaceString( TEXT_JUSTUS_GAME_OVER_START_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_START_TOTAL ), scoreString ) );
				}

				if ( getScore() < getBiggestOverallLowValue() ) {
					justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_TOTAL ) ) );
				} else if ( getScore() < getBiggestOverallMediumValue() ) {
					justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_TOTAL ) ) );
				} else {
					justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_TOTAL ) ) );
				}
			break;
		}
	} // fim do método commentLastCase()

	
	private final void playTheme() {
		if ( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_SUSPENSE, MediaPlayer.LOOP_INFINITE );
	}


	public final int getScore() {
		return score;
	}


	public final void update( int delta ) {
		// a câmera deve ser atualizada antes do restante dos elementos
		updateCamera( delta );

		switch(state) {
			case STATE_CHOOSE_FIRST_CASE:
			case STATE_CHOOSE_NEXT_CASE:
				if( isCursorAvailable() && ( justus.isLastMessageShown( true ) ) )
					stg.changeBackgroundColor(Stage.BACKGROUND_COLOR_BLUE);
				else if (stg.colorIsBlue())
					stg.changeBackgroundColor(Stage.BACKGROUND_COLOR_AMBAR);
			break;

			case STATE_NONE:
				return;
		}

		super.update( delta );
		accTime += delta;

		if ( currentCase >= 0 )
			cases[ currentCase ].update( delta );

		switch ( state ) {
			case STATE_HIGHLIGHT_CASE:
				if ( highlight.isAnimationOver() )
					stateEnd();
			break;

			case STATE_SHOW_REMAINING_CASES:
			case STATE_SHOW_REMAINING_CASES_STOP:
				if ( highlight.isAnimationOver() && !bigCase.isVisible() )
					bigCase.appear( cases[ currentCase ].getCaseCenter().add( stage.getPosition() ), BigCase.ANIMATION_BACK_TO_ORIGIN, true );
			break;

			case STATE_BANKER_CALLING:
				if( bankerCalling ) {
					if (justus.getRemainingMessagesCount() < 2) {
						if ( accTime >= PHONE_RING_INTERVAL ) {
							accTime %= PHONE_RING_INTERVAL;
							MediaPlayer.play( SOUND_PHONE );
						}
					}
				}
			break;
		}
	}


	private final void updateCamera( int delta ) {
		if ( cameraState != CAMERA_MOVE_STATE_NONE ) {
			cameraAccTime += delta;
			final Point p = new Point();
			cameraTweener.getPointAtFixed( p, NanoMath.divInt( cameraAccTime, cameraTotalTime ) );
			setCameraPosition( p );

			if ( cameraAccTime >= cameraTotalTime ) {
				cameraAccTime = 0;

				switch ( cameraState ) {
					case CAMERA_MOVE_STATE_MOVE_AND_STOP:
						cameraState = CAMERA_MOVE_STATE_NONE;
					break;

					case CAMERA_MOVE_STATE_LOOP:
						setCameraLoop();
					break;
				}
			}
		}
	}


	/**
	 * Prepara a tela de jogo para uma nova partida.
	 */
	public final void prepare() throws Exception {
		final int[] values = GetRandomValues();
		Case.resetGirlsNames();
		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			cases[ i ].set( i + 1, values[ i ] );
		}

		if ( playerCaseIndex >= 0 ) {
			playerCaseIndex = -1;
			removeDrawable( playerCase );
			playerCase = null;
		}

		bankerCalling = false;
		justus.clearMessages( true );

		if ( banker == null ) {
			banker = new BankerScreen( this );
			// usa o mesmo viewport da tela, para fazer uso das mesmas otimizações de desenho
			banker.setViewport( stage.getViewport() );
			insertDrawable( banker, bankerDrawableIndex );
			GameMIDlet.log( "banker" );
		}

		valuesBox.reset();

		for ( byte i = 0; i < bankerOffers.length; ++i )
			bankerOffers[ i ] = 0;

		currentTurn = 0;
		remainingCases = CASES_PER_TURN_INITIAL;
		score = -1;
		firstCaseInTurn = true;

		// só inicializa a tela de vitória quando for necessária
		removeDrawable( winScreen );
		winScreen = null;

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setState( STATE_INTRO );
	}


	private final void setIconsVisible( boolean pause, boolean values ) {
		//#if TOUCH == "true"
			if ( pauseIcon != null ) {
				pauseIcon.setVisible( pause );
				valuesIcon.setVisible( values );
			}
		//#endif
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;
		justus.clearMessages( false );
		boolean pauseVisible = false;
		boolean valuesVisible = false;

		switch ( state ) {
			case STATE_INTRO:
				stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_AMBAR );
				final Date d = new Date();
				final Calendar c = Calendar.getInstance();
				c.setTime( d );
				String intro;

				final int HOUR_OF_DAY = c.get( Calendar.HOUR_OF_DAY );
				if ( HOUR_OF_DAY >= 6 && HOUR_OF_DAY < 12 ) {
					intro = GameMIDlet.getText( TEXT_JUSTUS_GOOD_MORNING );
				} else if ( HOUR_OF_DAY >= 12 && HOUR_OF_DAY < 18 ) {
					intro = GameMIDlet.getText( TEXT_JUSTUS_GOOD_AFTERNOON );
				} else {
					intro = GameMIDlet.getText( TEXT_JUSTUS_GOOD_NIGHT );
				}

				final Customer customer = NanoOnline.getCurrentCustomer();

				if ( customer.isRegistered() ) {
					// adiciona saudação para jogador
					intro += ", " + customer.getNickname();
				}

				currentCase = -1;

				if ( banker != null )
					banker.setVisible( false );
				
				setCursorAvailable( false );

				highlight.setVisible( false );

				justus.appendMessage( replaceString( TEXT_JUSTUS_INTRO, intro ) );
				justus.changeExpression(Justus.EXPRESSION_FELIZ);
				justus.show();
				setCameraPosition( cameraLimit.x, cases[ CASES_TOTAL >> 1 ].getCenterPosition().y );
				setCameraLoop();
			
				bigCase.setState( BigCase.STATE_HIDDEN );
				valuesBox.setState( ValuesBox.STATE_HIDDEN );

				// gambiarra para evitar que primeira fala do Justus fique vazia
				justus.showNextMessage( true );
			break;

			case STATE_CHOOSE_FIRST_CASE:
				justus.appendMessage( TEXT_JUSTUS_CHOOSE_FIRST_CASE_1 );
				justus.appendMessage( TEXT_JUSTUS_CHOOSE_FIRST_CASE_2 );
				justus.appendMessage( TEXT_JUSTUS_CHOOSE_FIRST_CASE_3 );
				justus.show();
				justus.changeExpression(Justus.EXPRESSION_NORMAL);

				playTheme();
			break;

			case STATE_CHOOSE_NEXT_CASE:
				pauseVisible = true;
				valuesVisible = true;

				if ( banker != null )
					banker.setVisible( false );

				// adiciona comentário no começo do turno
				if ( currentTurn == 1 && remainingCases == CASES_PER_TURN_INITIAL ) {
					playTheme();
					removeCase( playerCaseIndex );
					setCurrentCase( currentCase + 1, false );
					setCursorAvailable( true );
					playerCase = Case.getCaseImage( playerCaseIndex + 1, true );
					playerCase.defineReferencePixel( ANCHOR_CENTER );
					insertDrawable( playerCase, playerCaseDrawableIndex );
					
					if ( ScreenManager.getInstance().hasPointerEvents() )
						justus.appendMessage( TEXT_JUSTUS_PAUSE_INFO_TOUCH );
					else
						justus.appendMessage( TEXT_JUSTUS_PAUSE_INFO_KEY );

					justus.changeExpression(Justus.EXPRESSION_NORMAL);
				} else if ( firstCaseInTurn ) {
					stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_AMBAR );
					playTheme();
					setCursorAvailable( false );
					final int randomStartMessage = NanoMath.randInt( TEXT_JUSTUS_TURN_START_TOTAL );
					justus.appendMessage( replaceString( TEXT_JUSTUS_TURN_START_1 + randomStartMessage, String.valueOf( currentTurn ) ) );

					if ( valuesBox.getValuesLeftCount() <= 3 ) {
						// adiciona comentário quando estiver chegando ao final do jogo
						justus.appendMessage( replaceString( (TEXT_JUSTUS_NEAR_END_1 + NanoMath.randInt( TEXT_JUSTUS_NEAR_END_TOTAL )), String.valueOf(valuesBox.getValuesLeftCount()) ) );
						justus.changeExpression(Justus.EXPRESSION_CALMA);
					} else {
						justus.changeExpression(Justus.EXPRESSION_NORMAL);
					}
				} else {
					valuesBox.setState( ValuesBox.STATE_APPEARING );
					removeCase( currentCase );
					setCurrentCase( currentCase + 1, false );
					setCursorAvailable( true );
					
					if ( currentTurn == 1 && remainingCases == CASES_PER_TURN_INITIAL - 1 ) {
						justus.show();
						justus.changeExpression(Justus.EXPRESSION_VOCE);
						justus.appendMessage( TEXT_JUSTUS_VALUES_BOX_INFO );
						if ( ScreenManager.getInstance().hasPointerEvents() )
							justus.appendMessage( TEXT_JUSTUS_VALUES_BOX_COMMAND_TOUCH );
						else
							justus.appendMessage( TEXT_JUSTUS_VALUES_BOX_COMMAND_KEY );
					} else {
						justus.hide();
					}
				}

				if ( remainingCases > 1 ) {
					justus.appendMessage( replaceString( TEXT_JUSTUS_CHOOSE_NEXT_CASE_1 + NanoMath.randInt( TEXT_JUSTUS_CHOOSE_NEXT_CASE_START_TOTAL ), String.valueOf( remainingCases ) ) );
				} else {
					justus.appendMessage( TEXT_JUSTUS_CHOOSE_ONE_MORE_CASE );
				}
			break;

			case STATE_LAST_CASE:
				pauseVisible = true;
				valuesVisible = true;
				setCursorAvailable( false );

				if ( banker != null )
					banker.setVisible( false );
				valuesBox.setState( ValuesBox.STATE_HIDING );
				justus.show();
				setCurrentCase( -1, false );

				justus.changeExpression(Justus.EXPRESSION_NORMAL);
				final int lastTurnCommentIndex = TEXT_JUSTUS_LAST_TURN_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_LAST_TURN_COMMENT_1_TOTAL );
				justus.appendMessage( TEXT_JUSTUS_LAST_TURN_1 + NanoMath.randInt( TEXT_JUSTUS_LAST_TURN_TOTAL ) );
				final int[] valuesLeft = valuesBox.getValuesLeft();
					justus.appendMessage( replaceString( lastTurnCommentIndex,
										new String[] { GameMIDlet.getCurrencyString( valuesLeft[ 0 ] ),
													   GameMIDlet.getCurrencyString( valuesLeft[ 1 ] ) } ) );
			break;

			case STATE_SHOW_CASE_CONTENT:
				justus.changeExpression(Justus.EXPRESSION_VOCE);
				
				if ( currentTurn < 1 )
					justus.appendMessage( replaceString( TEXT_JUSTUS_YOU_CHOSE_CASE, String.valueOf( playerCaseIndex + 1 ) ) );


				setCursorAvailable( false );
				valuesBox.setState( ValuesBox.STATE_HIDING );
				bigCase.appear( cases[ currentCase ].getCaseCenter().add( stage.getPosition() ), 
						currentTurn < 1 ? BigCase.ANIMATION_GOTO_TOP_LEFT : BigCase.ANIMATION_BACK_TO_ORIGIN, false );
			break;

			case STATE_SHOW_LAST_CASE_CONTENT:
				// nesse ponto, já sabemos a pontuação do jogador
				score = cases[ playerCaseIndex ].getValue();
				showPlayerCaseContent( false );

				setCursorAvailable( false );
				valuesBox.setState( ValuesBox.STATE_HIDING );
				bigCase.setState( BigCase.STATE_APPEARING );
			break;

			case STATE_SHOW_VALUES_BEFORE_BANKER:
				removeCase( currentCase );
				setCurrentCase( currentCase + 1, false );
				valuesBox.setState( ValuesBox.STATE_APPEARING );
				justus.hide();
			break;

			case STATE_BANKER_CALLING:
				stg.changeBackgroundColor( Stage.BACKGROUND_COLOR_RED );
				bankerCalling = true;
				pauseVisible = true;
				valuesVisible = true;
			
				if ( currentTurn <= 1 ) {
					// 1ª rodada - explica quem é o negociador (BRILHANTE em negociação, senão não seria um negociador!)
					for ( short i = TEXT_JUSTUS_BANKER_INTRO_1; i < TEXT_JUSTUS_BANKER_INTRO_FINAL; ++i )
						justus.appendMessage( i );
				}
				justus.appendMessage( TEXT_JUSTUS_BANKER_CALLING_1 + NanoMath.randInt( TEXT_JUSTUS_BANKER_CALLING_TOTAL ) );
				justus.changeExpression( Justus.EXPRESSION_NORMAL );
				justus.show();
			break;

			case STATE_DEAL_OR_NO_DEAL:
				valuesVisible = true;

				bankerOffers[ currentTurn ] = valuesBox.calculateBankerOffer( currentTurn );
				final String value = GameMIDlet.getCurrencyString( bankerOffers[ currentTurn ] );

				justus.appendMessage( replaceString( TEXT_JUSTUS_DEAL_OR_NO_DEAL_1 + NanoMath.randInt( TEXT_JUSTUS_DEAL_NO_DEAL_TOTAL ), value ) );

				banker.setOffer( GameMIDlet.getCurrencyString( bankerOffers[ currentTurn ], false ) );
				banker.setDealMode( true );

				stage.setVisible( false );
				transition.doTransition( null, banker );
				GameMIDlet.log( " [ begin transition (null, banker)" );
			break;

			case STATE_GAME_OVER_DEAL:
				score = valuesBox.calculateBankerOffer( currentTurn );
				final String scoreString = GameMIDlet.getCurrencyString( getScore() );

				justus.appendMessage( replaceString( TEXT_JUSTUS_DEAL_1 + NanoMath.randInt( TEXT_JUSTUS_DEAL_TOTAL ), scoreString ) );
				commentResult();
			break;

			case STATE_SHOW_REMAINING_CASES:
				justus.appendMessage( TEXT_JUSTUS_SEE_REMAINING_CASES_1 + NanoMath.randInt( TEXT_JUSTUS_SEE_REMAINING_CASES_TOTAL ) );
				justus.appendMessage( TEXT_JUSTUS_SKIP_REMAINING_CASES_1 + NanoMath.randInt( TEXT_JUSTUS_SKIP_REMAINING_CASES_TOTAL ) );
				
				showPlayerCaseContent( true );
				setCursorAvailable( false );
				
				if ( banker != null )
					banker.setVisible( false );
				justus.show();
			break;

			case STATE_SHOW_REMAINING_CASES_STOP:
				justus.appendMessage( TEXT_JUSTUS_SKIPPING_REMAINING_CASES_1 + NanoMath.randInt( TEXT_JUSTUS_SKIPPING_REMAINING_CASES_TOTAL ) );
			break;

			case STATE_GAME_OVER_LAST_CASE:
				commentResult();
			case STATE_GAME_OVER_FINAL_COMMENTS:
				// coloca a trama na tela
				highlight.setTarget( -1000, -1000, 0 );
				unloadBanker();

				if ( banker != null )
					banker.setVisible( false );
				justus.show();

				try {
					winScreen = new WinScreen( justus.getJustusWidth(), justus.getBoxHeight(), getScore() );
					insertDrawable( winScreen, bankerDrawableIndex );
					winScreen.show();
				} catch ( Exception e ) {
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif
				}

				justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_CHECK_ONLINE_RANKING_1 + NanoMath.randInt( TEXT_JUSTUS_CHECK_ONLINE_RANKING_TOTAL ) ) );
				justus.appendMessage( replaceString( TEXT_JUSTUS_GAME_OVER_END_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_END_TOTAL ), GameMIDlet.getCurrencyString( getScore() ) ) );

				switch(overallCaseAnalysis(getScore())) {
					case CASE_WORST:{
						justus.changeExpression(Justus.EXPRESSION_QUEQUEISSO);
						stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_RED );
						break;
					}
					case CASE_BAD:{
						justus.changeExpression(Justus.EXPRESSION_REZANDO);
						stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_RED );
						break;
					}
					case CASE_MEDIUM:{
						justus.changeExpression(Justus.EXPRESSION_NORMAL);
						stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_RED );
						break;
					}
					case CASE_GOOD:{
						justus.changeExpression(Justus.EXPRESSION_FELIZ);
						stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_AMBAR );
						break;
					}
					case CASE_BEST:{
						justus.changeExpression(Justus.EXPRESSION_FELIZ);
						stg.changeBackgroundColor ( Stage.BACKGROUND_COLOR_BLUE );
						break;
					}
				}
			break;
		}
		setIconsVisible( pauseVisible, valuesVisible );
		justus.showNextMessage();
	} // fim do método setState( int )


	/**
	 * Se for um aparelho com pouca memória, desaloca o negociador.
	 */
	private final void unloadBanker() {
		if ( GameMIDlet.isLowMemory() ) {
			removeDrawable( banker );
			banker = null;
			bigCase.unload();
		}
	}


	private final void answerBankerCall() {
		if(!transition.isVisible()) {
			bankerCalling = false;
			bigCase.unload();
			banker.setVisible( true );
			banker.setDealMode( false );
			transition.doTransition( stage, banker );
			GameMIDlet.log( " [ begin transition (stage, banker)" );

			if ( currentTurn > 1 ) {
				// verifica se a nova oferta é maior ou menor que a anterior
				final int currentOffer = valuesBox.calculateBankerOffer( currentTurn );
				final String[] subs = new String[] { GameMIDlet.getCurrencyString( bankerOffers[ currentTurn - 1 ], true )};
				justus.appendMessage( replaceString( TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_1 + NanoMath.randInt( TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_TOTAL ), subs ) );
				if ( currentOffer > bankerOffers[ currentTurn - 1 ] ) {
					justus.changeExpression( Justus.EXPRESSION_CELULAR );
					justus.appendMessage( replaceString( TEXT_JUSTUS_BANKER_RAISE_1 + NanoMath.randInt( TEXT_JUSTUS_BANKER_RAISE_TOTAL ), subs ) );
				}
				else if ( currentOffer < bankerOffers[ currentTurn - 1 ] ) {
					justus.changeExpression( Justus.EXPRESSION_CELULAR_ASSUSTADO );
					justus.appendMessage( replaceString( TEXT_JUSTUS_BANKER_LOWER_1 + NanoMath.randInt( TEXT_JUSTUS_BANKER_LOWER_TOTAL ), subs ) );
				}
			} else {
				justus.changeExpression( Justus.EXPRESSION_CELULAR );
				justus.appendMessage( TEXT_JUSTUS_BANKER_FIRST_OFFER_1 + NanoMath.randInt( TEXT_JUSTUS_BANKER_FIRST_OFFER_TOTAL ) );
			}

			justus.showNextMessage();

			MediaPlayer.play( SOUND_BANKER, MediaPlayer.LOOP_INFINITE );
			justus.show();
		}
	}


	private final void showPlayerCaseContent( boolean autoStep ) {
		bigCase.setCase( playerCaseIndex + 1, cases[ playerCaseIndex ].getValueString(), getSoundIndex( getScore() ) );
		bigCase.appear( playerCase.getRefPixelPosition(), BigCase.ANIMATION_GOTO_OFF_SCREEN, autoStep );

		removeDrawable( playerCase );
		playerCase = null;
	}


	private final void commentResult() {
		switch(overallCaseAnalysis(getScore())) {
			case CASE_WORST:{
				justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_MIN_VALUE_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_MIN_VALUE_TOTAL ) ) );
				justus.changeExpression(Justus.EXPRESSION_QUEQUEISSO);
				break;
			}
			case CASE_BAD:{
				justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_TOTAL ) ) );
				justus.changeExpression(Justus.EXPRESSION_REZANDO);
				break;
			}
			case CASE_MEDIUM:{
				justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_TOTAL ) ) );
				justus.changeExpression(Justus.EXPRESSION_NORMAL);
				break;
			}
			case CASE_GOOD:{
				justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_TOTAL ) ) );
				justus.changeExpression(Justus.EXPRESSION_FELIZ);
				break;
			}
			case CASE_BEST:{
				justus.appendMessage( GameMIDlet.getText( TEXT_JUSTUS_GAME_OVER_MAX_VALUE_1 + NanoMath.randInt( TEXT_JUSTUS_GAME_OVER_MAX_VALUE_TOTAL ) ) );
				justus.changeExpression(Justus.EXPRESSION_FELIZ);
				break;
			}
		}
	}


	/**
	 * Verifica qual o som de feedback deve ser tocado de acordo com o valor da maleta e o número do turno.
	 * @param value
	 * @return
	 */
	private final byte getSoundIndex( int value ) {
		if ( currentTurn < 10 )
			return value <= valuesBox.getNowBiggestMediumValue() ? SOUND_CASE_GOOD : SOUND_CASE_BAD;
		else
			return value > valuesBox.getNowBiggestMediumValue() ? SOUND_CASE_GOOD : SOUND_CASE_BAD;
	}


	private final void removeCase( int index ) {
		if ( cases[ index ].isVisible() ) {
			cases[ index ].setVisible( false );
			removeDrawable( cases[ index ] );
		}
	}


	private final void moveCameraTo( Point p ) {
		moveCameraTo( p.x, p.y );
	}


	private final void moveCameraTo( int x, int y ) {
		cameraAccTime = 0;
		cameraState = CAMERA_MOVE_STATE_MOVE_AND_STOP;
		cameraTweener.origin.set( cameraPosition );
		cameraTweener.destiny.set( x, y );
		cameraTotalTime = TIME_CAMERA_MOVE;

		cameraTweener.control1.set( ( x + cameraPosition.x ) >> 1, ( y + cameraPosition.y ) >> 1 );
		cameraTweener.control2.set( cameraTweener.destiny );
	}


	private final void setCursorAvailable( boolean a ) {
		if(a != cursorAvailable) {
			cursorAvailable = a;
			cursorAvailableSince = System.currentTimeMillis();
		}
	}


	private final boolean isCursorAvailable() {
		return cursorAvailable && ( System.currentTimeMillis() - cursorAvailableSince >= TIME_CURSOR_AVAILABLE_MIN );
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() )
			setSize( width, height );
	}


	private final String replaceString( int originalStringIndex, String sub ) {
		return replaceString( GameMIDlet.getText( originalStringIndex ), sub );
	}


	private final String replaceString( int originalStringIndex, String[] subs ) {
		return replaceString( GameMIDlet.getText( originalStringIndex ), subs );
	}


	private final String replaceString( String originalString, String sub ) {
		return replaceString( originalString, new String[] { sub } );
	}


	/**
	 * Substitui todos os caracteres arroba ('@') de uma string pelas strings de subs.
	 * @param originalString string original.
	 * @param subs strings que substituem cada caracter coringa. Caso haja menos substituições que coringas, a última substituição é repetida.
	 * @return uma nova string, com os caracteres coringa substituídos.
	 */
	private final String replaceString( String originalString, String[] subs ) {
		byte subIndex = 0;
		int index = originalString.indexOf( '@' );
		if ( index >= 0 ) {
			String newString = index == 0 ? "" : originalString.substring( 0, index );
			
			while ( index >= 0 ) {
				final int nextIndex = originalString.indexOf( '@', index + 1 );
				newString += subs[ subIndex ] + originalString.substring( index + 1, nextIndex >= 0 ? nextIndex : originalString.length() );

				if ( subIndex < subs.length -1 )
					++subIndex;

				index = nextIndex;
			}

			return newString;
		}

		return originalString;
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( id ) {
			case LISTENER_ID_BIG_CASE:
				switch ( sequence ) {
					case BigCase.STATE_OPENING:
						// comenta sobre o valor da mala
						commentCase();
						justus.showNextMessage();
					break;

					case BigCase.STATE_HIDE:
					case BigCase.STATE_CLOSE_AND_HIDE:
						stateEnd();
					break;

					case BigCase.STATE_SHOW_NUMBER:
						if ( currentTurn >= 1 )
							bigCase.setState( BigCase.STATE_OPENING );
					break;
				}
			break;

			case LISTENER_ID_TRANSITION:
				GameMIDlet.log( " ] end transition" );
				switch ( state ) {
					case STATE_GAME_OVER_DEAL:
						if ( banker != null )
							banker.setVisible( false );
						justus.show();
					break;

					case STATE_LAST_CASE:
						justus.show();
						justus.changeExpression(Justus.EXPRESSION_NORMAL);
					break;

					case STATE_CHOOSE_NEXT_CASE:
						// adiciona comentário no começo do turno
						justus.show();
						if ( firstCaseInTurn ) {
							if ( valuesBox.getValuesLeftCount() <= 3 ) {
								justus.changeExpression(Justus.EXPRESSION_CALMA);
							} else {
								justus.changeExpression(Justus.EXPRESSION_NORMAL);
							}
						}
					break;

					case STATE_DEAL_OR_NO_DEAL:
						justus.changeExpression(Justus.EXPRESSION_NORMAL);
					break;
				}
			break;

			case LISTENER_ID_VALUES_BOX:
				switch ( state ) {
					case STATE_SHOW_VALUES_BEFORE_BANKER:
						switch ( sequence ) {
							case ValuesBox.STATE_HIDDEN:
								stateEnd();
							break;
						}
					break;

					case STATE_CHOOSE_NEXT_CASE:
						playTheme();
						if( sequence == ValuesBox.STATE_HIDING ) justus.changeExpression(Justus.EXPRESSION_NORMAL);
					break;
				}
			break;

			case LISTENER_ID_BANKER:
				switch ( sequence ) {
					case BankerScreen.BANKER_SEQUENCE_DEAL:
						stage.setVisible( true );
						transition.doTransition( banker, stage );

						GameMIDlet.log( " [ begin transition (banker, stage)" );
						setState( STATE_GAME_OVER_DEAL );
					break;

					default:
						stateEnd();
				}
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	//#if TOUCH == "true"
		protected final void paint( Graphics g ) {
			cameraMutex.acquire();
			super.paint( g );
			cameraMutex.release();
		}


		public final void onPointerDragged( int x, int y ) {
			if ( valuesBox.isVisible() ) {
				valuesBox.onPointerDragged( x, y );
			} else {
				switch ( state ) {
					case STATE_CHOOSE_FIRST_CASE:
					case STATE_CHOOSE_NEXT_CASE:
						cameraMutex.acquire();

						cameraState = CAMERA_MOVE_STATE_NONE;
						setCameraPosition( cameraPosition.x + x - lastPointerPos.x, cameraPosition.y + y - lastPointerPos.y );
						lastPointerPos.set( x, y );

						cameraMutex.release();
					break;
				}
			}
		}


		public final void onPointerPressed( int x, int y ) {
/* Debug WinScreen
				try {
					winScreen = new WinScreen( justus.getJustusWidth(), justus.getBoxHeight(), 100000000 );
					insertDrawable( winScreen );
					winScreen.show();
				} catch ( Exception e ) {
//					//#if DEBUG == "true"
						e.printStackTrace();
//					//#endif
				}
*/

			if ( valuesBox.isVisible() ) {
				// pode acontecer no momento em que Justus explica o funcionamento da tabela
				if ( justus.contains( x, y ) )
					keyPressed( ScreenManager.FIRE );
				else
					valuesBox.onPointerPressed( x, y );
			} else {
				if ( pauseIcon.contains( x, y ) ) {
					keyPressed( ScreenManager.KEY_BACK );
				} else if ( valuesIcon.contains( x, y ) ) {
					keyPressed( ScreenManager.KEY_NUM0 );
				} else {
					switch ( state ) {
						case STATE_CHOOSE_FIRST_CASE:
						case STATE_CHOOSE_NEXT_CASE:
							if ( justus.contains( x, y ) ) {
								if ( !justus.isLastMessageShown( true ) ) {
									keyPressed( ScreenManager.FIRE );
								}
								else  {
									//if( !isCursorAvailable() )
									//	setCursorAvailable(true);
									justus.showNextMessage();
								}
							} else {
								firstPointerPos.set( x, y );
								lastPointerPos.set( x, y );
							}
						break;

						case STATE_DEAL_OR_NO_DEAL:
							if ( !justus.isHiding() ) {
								if ( justus.isLastMessageShown( true ) )
									justus.hide();
								else
									justus.showNextMessage();

								break;
							}

							banker.onPointerPressed( x, y );
						break;

						case STATE_INTRO:
						case STATE_GAME_OVER_DEAL:
						case STATE_GAME_OVER_LAST_CASE:
						case STATE_SHOW_VALUES_BEFORE_BANKER:
						case STATE_SHOW_CASE_CONTENT:
						case STATE_SHOW_LAST_CASE_CONTENT:
						default:
							keyPressed( ScreenManager.FIRE );
						break;
					}
				}
			}
		}


		public final void onPointerReleased( int x, int y ) {
			if ( valuesBox.isVisible() ) {
				valuesBox.onPointerReleased( x, y );
				if ( valuesBox.isHiding() )
					justus.show();
			} else {
				switch ( state ) {
					case STATE_CHOOSE_FIRST_CASE:
					case STATE_CHOOSE_NEXT_CASE:
						final Point p = new Point( x, y );
						if ( p.distanceTo( firstPointerPos ) <= TOUCH_TOLERANCE ) {
							final int caseIndex = getCaseAt( lastPointerPos.x, lastPointerPos.y );
							if ( caseIndex >= 0 ) {
								if ( currentCase != caseIndex )
									setCurrentCase( caseIndex, true );
								else
									keyPressed( ScreenManager.FIRE );
							}
						}
					break;

					case STATE_DEAL_OR_NO_DEAL:
						banker.onPointerReleased( x, y );
					break;
				}
			}
		}


		private final int getCaseAt( int x, int y ) {
			x -= stage.getPosX();
			y -= stage.getPosY();

			for ( byte i = 0; i < CASES_TOTAL; ++i ) {
				if ( cases[ i ].contains( x, y ) )
					return i;
			}

			return -1;
		}

	//#endif


}

