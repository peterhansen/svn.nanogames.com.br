/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author caiolima
 */
public final class ConfettiEmitter extends ParticleEmitter {

    private final int MIN_SPEED;
    private final int MAX_SPEED;

	public ConfettiEmitter( int confettisPerSecond, int minSpeed, int maxSpeed, int maxParticles) throws Exception {
        super(confettisPerSecond, false, new Sprite( PATH_IMAGES + "particle_paper" ), ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, 3, (short)maxParticles);

        MIN_SPEED = minSpeed;
        MAX_SPEED = maxSpeed;
	}


    public Point RandSpeed() {
        return new Point(0 , (NanoMath.randFixed(MAX_SPEED-MIN_SPEED) + MIN_SPEED));
    }


    public Point RandPosition() {
        return new Point(NanoMath.randInt( ScreenManager.SCREEN_WIDTH ), 0);
    }

	
	public int RandLifeTime() {
		return Integer.MAX_VALUE;
	}
}
