/**
 * Highlight.java
 *
 * Created on Sep 14, 2010 1:58:48 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

/**
 *
 * @author peter
 */
public final class Highlight extends DrawableGroup implements Constants, Updatable {

	private final Pattern top;
	private final Pattern left;
	private final Pattern right;
	private final Pattern bottom;
	private final DrawableGroup center;

	private short accTime;

	public static final short TIME_ANIMATION_DEFAULT	= 1800;
	public static final short TIME_ANIMATION_QUICK		= TIME_ANIMATION_DEFAULT >> 1;
	private static final short TIME_DONE				= TIME_ANIMATION_DEFAULT + 390;

	private short currentAnimationTime = TIME_ANIMATION_DEFAULT;

	private final BezierCurve tweener = new BezierCurve();

	private final Rectangle stageViewport;
	private final Point stagePosition;

	private static DrawableImage TRANSPARENCY;


	public Highlight( Point position, Rectangle viewport ) throws Exception {
		super( 6 );

		stageViewport = viewport;
		stagePosition = position;

		final boolean useTransparency = viewport == null;

		top = new Pattern( getFill( useTransparency ) );
		top.setViewport( new Rectangle() );
		top.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		insertDrawable( top );

		left = new Pattern( getFill( useTransparency ) );
		left.setViewport( new Rectangle() );
		left.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		insertDrawable( left );

		right = new Pattern( getFill( useTransparency ) );
		right.setViewport( new Rectangle() );
		right.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		insertDrawable( right );

		bottom = new Pattern( getFill( useTransparency ) );
		bottom.setViewport( new Rectangle() );
		bottom.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		insertDrawable( bottom );

		final short SIZE = useTransparency ? SPOT_SIZE_SMALL : SPOT_SIZE_BIG;

		center = new DrawableGroup( 4 );
		center.setSize( SIZE, SIZE );
		insertDrawable( center );
		
		DrawableImage centerImage = new DrawableImage( PATH_IMAGES + "spot_" + ( useTransparency ? 0 : 1 ) + ".png" );
		// top left
		centerImage.mirror( TRANS_MIRROR_H );
		center.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		center.insertDrawable( centerImage );

		// top right
		centerImage = new DrawableImage( centerImage );
		centerImage.setPosition( SIZE - centerImage.getWidth(), 0 );
		center.insertDrawable( centerImage );

		// bottom left
		centerImage = new DrawableImage( centerImage );
		centerImage.mirror( TRANS_MIRROR_H | TRANS_MIRROR_V );
		centerImage.setPosition( 0, SIZE - centerImage.getHeight() );
		center.insertDrawable( centerImage );

		// bottom right
		centerImage = new DrawableImage( centerImage );
		centerImage.mirror( TRANS_MIRROR_V );
		centerImage.setPosition( SIZE - centerImage.getWidth(), SIZE - centerImage.getHeight() );
		center.insertDrawable( centerImage );
	}


	private static final Drawable getFill( boolean transparency ) throws Exception {
		if ( transparency ) {
			if ( TRANSPARENCY == null )
				TRANSPARENCY = new DrawableImage( PATH_SPLASH + "transparency.png" );
			return new DrawableImage( TRANSPARENCY );
		} else {
			final Pattern p = new Pattern( 0x000000 );
			p.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
			return p;
		}
	}


	private final void refresh() {
		final int fp_progress = NanoMath.clamp( NanoMath.divInt( accTime, currentAnimationTime ), 0, NanoMath.ONE );
		final Point p = new Point();

		tweener.getPointAtFixed( p, fp_progress );
		// a posição do centro deve sempre ser ímpar para casar a trama com os outros patterns
		center.setRefPixelPosition( p.x + ( p.x & 1 ), p.y - 1 + ( p.y & 1 ) );

		top.getViewport().set( 0, 0, top.getWidth(), stagePosition.y + center.getPosY() );
		left.getViewport().set( 0, top.getViewport().height, stagePosition.x + center.getPosX(), center.getHeight() );
		right.getViewport().set( stagePosition.x + center.getPosX() + center.getWidth(), left.getViewport().y, right.getWidth(), center.getHeight() );
		bottom.getViewport().set( 0, stagePosition.y + center.getPosY() + center.getHeight(), bottom.getWidth(), bottom.getHeight() );

//		if ( stageViewport != null )
//			stageViewport.set( center.getPosition(), center.getSize() );
	}


	public final void setTarget( Point p, int time ) {
		setTarget( p.x, p.y, time );
	}


	public final Point getInternalSize() {
		return center.getSize();
	}


	public final void setTarget( int x, int y, int time ) {
		final Point randomPos = new Point();
		if ( NanoMath.randInt( 128 ) < 64 ) {
			// luz vem dos lados
			randomPos.x = NanoMath.randInt( 128 ) < 64 ? -getWidth() : getWidth();
			randomPos.y = NanoMath.randInt( getHeight() );
		} else {
			// luz vem de cima
			randomPos.x = NanoMath.randInt( getWidth() );
			randomPos.y = -getHeight();
		}
		accTime = 0;
		currentAnimationTime = ( short ) Math.max( time, 1 );
		
		tweener.origin.set( randomPos );
		tweener.control1.set( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) );
		tweener.control2.set( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) );
		tweener.destiny.set( x, y );

		refresh();
		setVisible( true );
	}


	public final void update( int delta ) {
		if ( isVisible() ) {
			if ( accTime < TIME_DONE ) {
				accTime += delta;
			}
			// é importante chamar refresh() mesmo após a movimentação pois o cenário pode ter se movido
			refresh();
		}
	}


	public final boolean isAnimationOver() {
		return accTime >= TIME_DONE;
	}

}
