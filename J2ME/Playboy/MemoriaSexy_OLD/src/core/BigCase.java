/**
 * BigCase.java
 *
 * Created on Aug 24, 2010 3:53:05 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;
import screens.GameScreen;

/**
 *
 * @author peter
 */
public final class BigCase extends UpdatableGroup implements Constants, SpriteListener {

	public static final byte STATE_HIDDEN			= 0;
	public static final byte STATE_APPEARING		= 1;
	public static final byte STATE_SHOW_NUMBER		= 2;
	public static final byte STATE_OPENING			= 3;
	public static final byte STATE_SHOW_VALUE		= 4;
	public static final byte STATE_CLOSE_AND_HIDE	= 5;
	public static final byte STATE_HIDE				= 6;

	private byte state;

	/** Duração da transição (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 850;

	/** Tempo mínimo de exibição do conteúdo de uma maleta, antes que o jogador possa avançar. */
	private static final short TIME_SHOW_CASE_CONTENT_MIN = 700;

	private static final short TIME_PRE_OPENING = 750;

	private static final byte SEQUENCE_SMALL		= 0;
	private static final byte SEQUENCE_GROWING		= 1;
	private static final byte SEQUENCE_CLOSED		= 2;
	private static final byte SEQUENCE_OPENING		= 3;
	private static final byte SEQUENCE_OPEN			= 4;
	private static final byte SEQUENCE_CLOSING		= 5;
	private static final byte SEQUENCE_SHRINKING	= 6;

	private int accTime;

	private DrawableGroup caseIn;
	private Sprite caseOut;
	
	//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 		private Sprite caseOutRight;
	//#endif

	private final byte caseOutDrawableIndex;

	private final RichLabel labelNumber;
	private final RichLabel labelValue;

	/** "Tweener" da animação dos elementos na tela. Os pontos x são usados para a posição do Justus, e os pontos
	 * y para a posição da caixa de texto.
	 */
	private final BezierCurve tweener = new BezierCurve();

	private final SpriteListener listener;

	private int ANCHOR_Y;

	private final int textBoxOffset;

	private final Point screenPosition;

	private final Point tweenerStart = new Point();

	private byte soundIndex = -1;

	public static final byte ANIMATION_BACK_TO_ORIGIN	= 0;
	public static final byte ANIMATION_GOTO_TOP_LEFT	= 1;
	public static final byte ANIMATION_GOTO_OFF_SCREEN	= 2;

	private byte animationType;
	private boolean autoStep;
	

	public BigCase( SpriteListener listener, Point screenPosition, int textBoxHeight ) throws Exception {
		super( 6 );

		this.listener = listener;
		this.screenPosition = screenPosition;
		textBoxOffset = -textBoxHeight >> 1;

		labelValue = new RichLabel( FONT_CASE_VALUE, null, getWidth() );
		labelValue.setPosition( 0, CASE_BIG_LABEL_VALUE_Y );
		labelValue.setVisible( false );
		caseOutDrawableIndex = ( byte ) ( insertDrawable( labelValue ) + 1 );

		labelNumber = new RichLabel( FONT_CASE_NUMBER, null, getWidth() );
		labelNumber.setVisible( false );
		insertDrawable( labelNumber );

		if ( !GameMIDlet.isLowMemory() )
			load();

		setState( STATE_HIDDEN );
	}


	public final void appear( Point start, byte animationType, boolean autoStep ) {
		tweenerStart.set( start );
		tweener.destiny.set( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT + textBoxOffset + ( ANCHOR_Y - ( getHeight() >> 1 ) ) );
		tweener.control1.set( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT );
		tweener.control2.set( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

		this.animationType = animationType;
		this.autoStep = autoStep;

		setState( STATE_APPEARING );
	}


	public final void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
				accTime = 0;
				refresh();
			break;

			case STATE_APPEARING:
				switch ( this.state ) {
					case STATE_OPENING:
					case STATE_SHOW_NUMBER:
					case STATE_SHOW_VALUE:
					return;
				}
				setVisible( true );
				labelValue.setVisible( false );
				load();
				caseIn.setVisible( false );
				caseOut.setSequence( SEQUENCE_GROWING );
				refresh();

				if ( !autoStep )
					MediaPlayer.play( SOUND_CAMERA_EFFECT );
			break;

			case STATE_OPENING:
				load();
				accTime = 0;
			break;

			case STATE_SHOW_NUMBER:
				setVisible( true );
				accTime = TRANSITION_TIME;
				refresh();
				accTime = 0;

				if ( !autoStep && soundIndex >= 0 )
					MediaPlayer.play( SOUND_OPEN_CASE );
			break;

			case STATE_SHOW_VALUE:
				accTime = 0;
				load();
				caseOut.setSequence( SEQUENCE_OPEN );
			break;

			case STATE_CLOSE_AND_HIDE:
			case STATE_HIDE:
				if ( this.state == STATE_HIDDEN )
					return;
				accTime = TRANSITION_TIME;

				load();
				switch ( animationType ) {
					case ANIMATION_BACK_TO_ORIGIN:
					break;

					case ANIMATION_GOTO_TOP_LEFT:
						final Rectangle frame = caseOut.getFrame( 0 );
						tweenerStart.set( frame.width * 2 / 3, frame.height * 2 / 3 );
					break;

					case ANIMATION_GOTO_OFF_SCREEN:
						tweenerStart.set( tweenerStart.x, -getHeight() );
					break;
				}

				caseOut.setSequence( state == STATE_CLOSE_AND_HIDE ? SEQUENCE_CLOSING : SEQUENCE_SHRINKING );
				setVisible( true );
			break;
		}

		this.state = ( byte ) state;
	}


	public final void load() {
		if ( caseOut == null ) {
			GameMIDlet.log( "BigCase load antes" );
			try {
				//#if SCREEN_SIZE == "SMALL"
//# 					if ( GameMIDlet.isLowMemory() ) {
//# 						caseOut = new Sprite( PATH_IMAGES + "case_big_low" );
//# 					}
//# 					else caseOut = new Sprite( PATH_IMAGES + "case_big" );
				//#else
					caseOut = new Sprite( PATH_IMAGES + "case_big" );
				//#endif

				caseOut.setListener( this, LISTENER_ID_BIG_CASE );
				insertDrawable( caseOut, caseOutDrawableIndex );

				final int firstFrameHeight = caseOut.getFrame( 0 ).height;
				final int firstFrameOffsetY = caseOut.getFrameOffset( 0 ).y;
				ANCHOR_Y = firstFrameOffsetY + ( firstFrameHeight >> 1 );

				setSize( caseOut.getSize() );
				defineReferencePixel( getWidth() >> 1, ANCHOR_Y );

				//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 					caseOutRight = new Sprite( caseOut );
//# 					caseOutRight.mirror( TRANS_MIRROR_H );
//# 					insertDrawable( caseOutRight, caseOutDrawableIndex );
				//#endif

				caseIn = new DrawableGroup( 2 );
				DrawableImage caseInImage = new DrawableImage( PATH_IMAGES + "case_in.png" );
				caseIn.setSize( caseInImage.getWidth() << 1, caseInImage.getHeight() );
				caseIn.insertDrawable( caseInImage );
				caseInImage = new DrawableImage( caseInImage );
				caseInImage.mirror( TRANS_MIRROR_H );
				caseInImage.setPosition( caseInImage.getWidth(), 0 );
				caseIn.insertDrawable( caseInImage );
				insertDrawable( caseIn, 0 );
				caseIn.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
				caseIn.setRefPixelPosition( getWidth() >> 1, 0 );

				labelValue.setSize( getWidth(), labelValue.getFont().getHeight() );
				labelNumber.setFont( FONT_CASE_NUMBER );
				labelNumber.setSize( getWidth(), GameMIDlet.GetFont( FONT_CASE_NUMBER ).getHeight() );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
			GameMIDlet.log( "BigCase load depois" );
		}
	}


	public final void unload() {
		if ( GameMIDlet.isLowMemory() ) {
			GameMIDlet.log( "BigCase UNload antes" );
			removeDrawable( caseOut );
			caseOut = null;

			removeDrawable( caseIn );
			caseIn = null;

			labelNumber.setFont( FONT_CASE_VALUE );
			GameMIDlet.unloadFontCaseValue();

			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 				removeDrawable( caseOutRight );
//# 				caseOutRight = null;
			//#endif
			GameMIDlet.log( "BigCase UNload depois" );
		}
	}


	public final void update( int delta ) {
		if ( visible ) {
			super.update( delta );

			switch ( state ) {
				case STATE_APPEARING:
					accTime += delta;
					refresh();

					if ( accTime >= TRANSITION_TIME ) {
						listener.onSequenceEnded( 0, state );
						setState( STATE_SHOW_NUMBER );
					}
				break;

				case STATE_SHOW_NUMBER:
					if ( accTime < TIME_SHOW_CASE_CONTENT_MIN ) {
						accTime += delta;
						if ( accTime >= TIME_SHOW_CASE_CONTENT_MIN ) {
							listener.onSequenceEnded( 0, state );
						}
					}
				break;

				case STATE_OPENING:
					if ( ( autoStep || accTime < TIME_PRE_OPENING ) && caseOut.getSequenceIndex() != SEQUENCE_OPENING ) {
						accTime += delta;

						if ( autoStep || accTime >= TIME_PRE_OPENING ) {
							labelValue.setVisible( true );
							caseOut.setSequence( SEQUENCE_OPENING );

							if ( !autoStep ) {
								MediaPlayer.play( soundIndex );
								if ( soundIndex == SOUND_CASE_BAD )
									MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							}
						}
					}
				break;

				case STATE_SHOW_VALUE:
					if ( accTime <= TIME_SHOW_CASE_CONTENT_MIN )
						accTime += delta;
					else if ( autoStep )
						setState( STATE_CLOSE_AND_HIDE );
				break;
				
				case STATE_HIDE:
					accTime -= delta;
					refresh();

					if ( accTime <= 0 ) {
						listener.onSequenceEnded( 0, state );
						setState( STATE_HIDDEN );
					}
				break;

				case STATE_HIDDEN:
					return;
			}

			if ( caseOut != null ) {
				//#if SCREEN_SIZE == "SMALL"
//# 				if(GameMIDlet.isLowMemory()) {
//# 					switch ( caseOut.getCurrFrameIndex() ) {
//# 						case 0:
//# 						case 1:
//# 							labelNumber.setPosition( 0, CASE_BIG_LABEL_NUMBER_Y + CASE_BIG_LABEL_NUMBER_SMALL_Y );
//# 							labelNumber.setFont( FONT_CASE_SMALL );
//# 							caseIn.setVisible( false );
//# 							labelNumber.setVisible( true );
//# 						break;
//# 
//# 						case 2:
//# 							labelNumber.setVisible( true );
//# 							labelNumber.setPosition( 0, CASE_BIG_LABEL_NUMBER_Y );
//# 							labelNumber.setFont( FONT_CASE_NUMBER );
//# 						break;
//# 
//# 						default:
//# 							caseIn.setVisible( true );
//# 							labelNumber.setVisible( false );
//# 					}
//# 				}
//# 				else {
//# 					switch ( caseOut.getCurrFrameIndex() ) {
//# 						case 0:
//# 						case 1:
//# 							labelNumber.setPosition( 0, CASE_BIG_LABEL_NUMBER_Y + CASE_BIG_LABEL_NUMBER_SMALL_Y );
//# 							labelNumber.setFont( FONT_CASE_SMALL );
//# 							caseIn.setVisible( false );
//# 							labelNumber.setVisible( true );
//# 						break;
//# 
//# 						case 2:
//# 						case 3:
//# 							caseIn.setVisible( caseOut.getCurrFrameIndex() != 2 );
//# 							labelNumber.setVisible( true );
//# 							labelNumber.setPosition( 0, CASE_BIG_LABEL_NUMBER_Y );
//# 							labelNumber.setFont( FONT_CASE_NUMBER );
//# 						break;
//# 
//# 						default:
//# 							labelNumber.setVisible( false );
//# 					}
//# 				}
				//#else
				switch ( caseOut.getCurrFrameIndex() ) {
					case 0:
					case 1:
						labelNumber.setPosition( 0, CASE_BIG_LABEL_NUMBER_Y + CASE_BIG_LABEL_NUMBER_SMALL_Y );
						labelNumber.setFont( FONT_CASE_SMALL );
						caseIn.setVisible( false );
						labelNumber.setVisible( true );
					break;

					//#if HYBRID != "true"
						case 4:
					//#endif

					case 2:
					case 3:
						caseIn.setVisible( caseOut.getCurrFrameIndex() != 2 );
						labelNumber.setVisible( true );
						labelNumber.setPosition( 0, CASE_BIG_LABEL_NUMBER_Y );
						labelNumber.setFont( FONT_CASE_NUMBER );
					break;

					default:
						labelNumber.setVisible( false );
				}
				//#endif
			}
			
			labelNumber.formatText( false );
		}

		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			if ( caseOutRight != null ) {
//# 				caseOutRight.setSequence( caseOut.getSequenceIndex() );
//# 				caseOutRight.setFrame( caseOut.getFrameSequenceIndex() );
//# 			}
		//#endif
	}


	public final boolean canBeClosed() {
		switch ( state ) {
			case STATE_SHOW_NUMBER:
				if ( labelValue.isVisible() )
					return false;
				
			case STATE_SHOW_VALUE:
				return accTime >= TIME_SHOW_CASE_CONTENT_MIN;

			default:
				return false;
		}
	}


	private final void refresh() {
		final Point p = new Point();
		tweener.origin.set( tweenerStart );
		tweener.getPointAtFixed( p, NanoMath.divInt( accTime, TRANSITION_TIME ) );
		setRefPixelPosition( p );
	}
	
	
	/**
	 *
	 * @param caseIndex
	 * @param value valor a ser exibido. Caso seja menor ou igual a zero, mostra apenas o número da maleta.
	 */
	public final void setCase( int caseIndex, String value, int soundIndex ) {
		labelNumber.setText( "<ALN_H>" + caseIndex );

		this.soundIndex = ( byte ) soundIndex;

		if ( value != null ) {
			labelValue.setText( "<ALN_H>" + value );
			labelValue.setVisible( true );
		} else {
			labelValue.setVisible( false );
		}
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case SEQUENCE_OPENING:
				caseOut.setSequence( SEQUENCE_OPEN );
				listener.onSequenceEnded( 0, state );
				setState( STATE_SHOW_VALUE );
			break;

			case SEQUENCE_GROWING:
					caseOut.setSequence( SEQUENCE_CLOSED );
			break;

			case SEQUENCE_CLOSING:
				labelValue.setVisible( false );
				setState( STATE_HIDE );
			break;

			case SEQUENCE_SHRINKING:
				caseOut.setSequence( SEQUENCE_SMALL );
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
}
