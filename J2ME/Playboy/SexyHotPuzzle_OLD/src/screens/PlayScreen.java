/**
 * PlayScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 2, 2008 6:50:28 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import core.ScoreLabel;
import core.PieceManager;
import core.Puzzle;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.userInterface.form.borders.ImageBorder;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.Rectangle;
import core.Clock;
import core.ParticleEmitter;
import core.Piece;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
	import br.com.nanogames.components.userInterface.form.TouchKeyPad;
//#endif


/**
 * 
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if TOUCH == "true"
		, PointerListener
//#endif

{
	public static final byte STATE_NONE						= 0;
	public static final byte STATE_BEGIN_LEVEL				= 1;
	public static final byte STATE_PLAYING					= 2;
	public static final byte STATE_LEVEL_CLEARED			= 3;
	public static final byte STATE_GAME_OVER				= 4;
	public static final byte STATE_PAUSED					= 5;
	public static final byte STATE_UNPAUSING				= 6;
	public static final byte STATE_NEW_RECORD				= 7;

	private byte state;

	/** Tempo m�nimo que uma mensagem � exibida na tela. */
	public static final short TIME_MESSAGE = 3000;

	private short timeToNextState;

	//#if SCREEN_SIZE == "SMALL"
//# 		private short timeInfoVisible;
	//#endif

	/** Pontua��o do jogador. */
	private int score;

	/***/
	private byte multiplier = 1;

	/** N�vel atual. */
	private byte level;

	/** pontua��o exibida atualmente */
	private int scoreShown;
	
	/** label que indica a pontua��o e o multiplicador */
	private final Label scoreLabel;

	/** Tempo restante. */
	private int timeLeft;

	/** Tempo m�ximo do n�vel atual. */
	private int timeMax;

	private final Clock clock;

	/** Tempo restante de combo. */
	private short comboTime;

	private final PieceManager pieceManager;

	private final Puzzle puzzle;

	private final ImageBorder puzzleBorder;

	private final Pattern bkgTopLight;
	private final Pattern bkgTopDark1;
	private final Pattern bkgTopDark2;
	private final Pattern bkgLeft;
	private final Pattern bkgRight;
	private final Pattern bkgBottom;
	
	/** Label utilizado para mostrar uma mensagem ao jogador (fim de jogo, n�vel completo, etc.).  */
	private final RichLabel labelMessage;
	
	/** pontua��o m�xima mostrada */
	//#if SCREEN_SIZE != "MEDIUM"
		private static final int SCORE_SHOWN_MAX = 999999;
	//#else
//# 		private static final int SCORE_SHOWN_MAX = 99999;
	//#endif

	/** Velocidade de atualiza��o da pontua��o. */
	private final MUV scoreSpeed = new MUV();

	private static final byte SCORE_LABELS_MAX = PieceManager.PIECES_MAX;

	private final ScoreLabel[] scoreLabels = new ScoreLabel[ SCORE_LABELS_MAX ];

	private static ImageFont FONT_SCORE;

	private int musicIndex = NanoMath.randInt( 128 ) < 64 ? 0 : 1;

	//<editor-fold defaultstate="collapsed" desc="mira do jogador e c�mera">

	/** mira do jogador */
	private final Sprite crosshair;

	// dire��es de movimenta��o da mira (podem ser combinadas)
	private static final byte DIRECTION_NONE	= 0;
	private static final byte DIRECTION_RIGHT	= 1;
	private static final byte DIRECTION_LEFT	= 2;
	private static final byte DIRECTION_UP		= 4;
	private static final byte DIRECTION_DOWN	= 8;

	private static final byte DIRECTION_H_MASK = DIRECTION_RIGHT | DIRECTION_LEFT;
	private static final byte DIRECTION_V_MASK = DIRECTION_UP | DIRECTION_DOWN;

	private static final byte DIRECTION_ALL_MASK = DIRECTION_H_MASK | DIRECTION_V_MASK;

	private byte crosshairDirection;

	/** velocidade de movimenta��o horizontal atual da mira */
	private final MUV crosshairSpeedX = new MUV();

	/** velocidade de movimenta��o vertical atual da mira */
	private final MUV crosshairSpeedY = new MUV();

	/** acelera��o da mira no eixo x */
	private final MUV crosshairAccX = new MUV();

	/** acelera��o da mira no eixo y */
	private final MUV crosshairAccY = new MUV();

	/** Limite de velocidade atual da mira (tamb�m usado para parar a mira). */
	private final Point crosshairSpeedLimit = new Point();

	/** Movimenta��o m�nima da mira num dos eixos, caso o jogador solte o bot�o antes de um movimento ter sido efetivamente realizado. */
	private final Point crosshairMinMove = new Point();

	private boolean crosshairMovedX;
	private boolean crosshairMovedY;

	private final ParticleEmitter emitter;

	/***/
	private final MUV emitRate = new MUV();

	/** Taxa m�xima de emiss�o de part�culas do brilho da mira por segundo. */
	private static final byte EMIT_RATE_MAX = 25;
	private static final byte EMIT_RATE_MAX_TOUCH = EMIT_RATE_MAX << 1;

	/** Imagem da mira sumindo e aparecendo a cada tiro. */
	private final Drawable[] crosshairFade;

	private final Rectangle crosshairViewport = new Rectangle();

	private static final byte CROSSHAIR_STATE_IDLE		= 0;
	private static final byte CROSSHAIR_STATE_APPEARING = 1;
	private static final byte CROSSHAIR_STATE_VANISHING = 2;

	private byte crosshairState;

	/** Dura��o em milisegundos da anima��o de aparecimento/desaparecimento da mira. */
	private static final short CROSSHAIR_FADE_TIME = 190;

	private short crosshairFadeTime;

	//#if TOUCH == "true"
		/** Indica o estado do ponteiro. */
		private byte pointerStatus;

		private static final byte POINTER_STATUS_RELEASED	= 0;
		private static final byte POINTER_STATUS_PRESSED	= 1;
		private static final byte POINTER_STATUS_DRAGGED	= 2;

		/** �ltima posi��o do ponteiro. */
		private final Point lastPointerPos = new Point();

		/** Momento do �ltimo evento onPointerPressed recebido. */
		private long lastPointerPressedTime;

		/** Momento do �ltimo evento onPointerDragged recebido. */
		private long lastPointerDraggedTime;

		/** Toler�ncia de tempo para que o evento onPointerReleased fa�a o jogador atirar, mesmo que o �ltimo evento de
		 * ponteiro n�o tenha sido onPointerPressed. Essa toler�ncia � utilizada para evitar que cliques n�o perfeitos,
		 * ou seja, quando a posi��o onde o ponteiro foi pressionado n�o � igual � posi��o onde foi liberado.
		 */
		private static final byte POINTER_CLICK_TIME_TOLERANCE = 120;
	//#endif

	/** Tempo em milisegundos que a mira leva para percorrer toda a tela na velocidade m�xima. */
	private static final short CROSSHAIR_SCREEN_TIME = 1050;

	/** Velocidade m�xima atual da mira. */
	private final Point crosshairMaxSpeed = new Point();

	/** velocidade m�nima considerada da mira antes de par�-la */
	private byte crossHairMinSpeed = 21;

	/** acelera��o da mira em pixels por segundo ao quadrado */
	private final Point crosshairAcc = new Point();

	//</editor-fold>

	private static Sprite CROSSHAIR;

	private static final short TIME_SHINE = 1000;
	private short timeShine;
	private final MUV specialEmitterRate = new MUV( ParticleEmitter.MAX_PARTICLES * 1000 / ParticleEmitter.PARTICLE_LIFE_TIME );


	public PlayScreen() throws Exception {
		super( SCORE_LABELS_MAX + 30 );

		if ( GameMIDlet.isLowMemory() ) {
			bkgTopLight = new Pattern( COLOR_BACKGROUND_LIGHT );
			bkgTopDark1 = new Pattern( COLOR_BACKGROUND );
			bkgTopDark2 = new Pattern( COLOR_BACKGROUND );
			bkgLeft = new Pattern( bkgTopDark1.getFillColor() );
			bkgRight = new Pattern( bkgLeft.getFillColor() );
			bkgBottom = new Pattern( bkgLeft.getFillColor() );
		} else {
			bkgTopLight = new Pattern( new DrawableImage( PATH_IMAGES + "p.png" ) );
			bkgTopDark1 = new Pattern( new DrawableImage( PATH_IMAGES + "bg.png" ) );
			bkgTopDark2 = new Pattern( bkgTopDark1.getFill() );
			bkgLeft = new Pattern( bkgTopDark1.getFill() );
			bkgRight = new Pattern( bkgLeft.getFill() );
			bkgBottom = new Pattern( bkgLeft.getFill() );
		}
		insertDrawable( bkgTopLight );
		insertDrawable( bkgTopDark1 );
		insertDrawable( bkgTopDark2 );
		insertDrawable( bkgLeft );
		insertDrawable( bkgRight );
		insertDrawable( bkgBottom );

		puzzle = new Puzzle();
		insertDrawable( puzzle );

		puzzleBorder = new ImageBorder( PATH_BORDER );
		puzzleBorder.getFill().setVisible( false );
		insertDrawable( puzzleBorder );

		pieceManager = new PieceManager( this );
		insertDrawable( pieceManager );

		emitter = new ParticleEmitter();
		insertDrawable( emitter );

		// insere a mira do jogador
		crosshair = new Sprite( CROSSHAIR ) {
			public Point getFrameOffset( int frameIndex ) {
				return new Point();
			}
		};
		crosshair.setClipTest( false );

		crosshairFade = new Drawable[ PIECE_TYPES ];
		for ( byte i = 0; i < PIECE_TYPES; ++i ) {
			Drawable image;
			if ( GameMIDlet.isLowMemory() ) {
				image = new Pattern( 0xffffff );
				image.setSize( crosshair.getWidth() >> 1, crosshair.getHeight() >> 1 );
			} else {
				image = new DrawableImage( new PaletteChanger( PATH_IMAGES + "p_" + i + ".png" ).createImage( 0xffffff, 0xffffff ) );
			}
			image.setViewport( crosshairViewport );
			image.setVisible( false );
			image.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			crosshairFade[ i ] = image;
			insertDrawable( image );
		}

		insertDrawable( crosshair );

		// rel�gio
		clock = new Clock();
		insertDrawable( clock ); 

		// insere o label indicando a pontua��o
		scoreLabel = new Label( FONT_SCORE, null );
		insertDrawable( scoreLabel );

		for ( byte i = 0; i < SCORE_LABELS_MAX; ++i ) {
			scoreLabels[ i ] = new ScoreLabel( this );
			insertDrawable( scoreLabels[ i ] );
		}
		labelMessage = new RichLabel( GameMIDlet.GetFont( FONT_MESSAGE ), "", getWidth() );
		insertDrawable( labelMessage );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public static final void load() throws Exception {
		//#if SCREEN_SIZE == "SMALL"
//# 			FONT_SCORE = GameMIDlet.GetFont( FONT_TEXT_WHITE );
		//#else
			FONT_SCORE = ImageFont.createMonoSpacedFont( PATH_IMAGES + "font_score.png", "0123456789" );
//			FONT_SCORE.setCharExtraOffset( 1 );
		//#endif
		CROSSHAIR = new Sprite( PATH_IMAGES + "c" );
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_PAUSED:
			return;

			case STATE_UNPAUSING:
			break;

			case STATE_NEW_RECORD:
				timeToNextState -= delta;
				super.update( delta );
			break;
			
			case STATE_PLAYING:
				updateTime( delta );

				// n�o � necess�rio emitir mais de um sprite por posi��o no mesmo instante, pois s� um deles estar� vis�vel
				emitter.emit( new Point( crosshair.getRefPixelX(), crosshair.getPosY() + crosshair.getHeight() ),
								emitRate.updateInt( delta ) > 0 ? 1 : 0 );

			default:
				updateCrosshair( delta );
				updateCrosshairFade( delta );
				super.update( delta );
		}

		//#if SCREEN_SIZE == "SMALL"
//# 		if ( timeInfoVisible > 0 ) {
//# 			timeInfoVisible -= delta;
//#
//# 			if ( timeInfoVisible <= 0 )
//# 				setInfoVisible( false );
//# 		}
		//#endif
		
		// caso o estado atual tenha uma dura��o m�xima definida, atualiza o contador
		if ( timeToNextState > 0 ) {
			timeToNextState -= delta;
			
			if ( timeToNextState <= 0 )
				stateEnded();
		}

		if ( timeShine > 0 ) {
			final Point p = new Point();
			timeShine = ( short ) NanoMath.clamp( timeShine - delta, 0, TIME_SHINE );

			final int particles = specialEmitterRate.updateInt( delta );
			for ( byte i = 0; i < particles; ++i ) {
				p.set( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) );
				emitter.emit( p, 1 );
			}
		}

		// atualiza a pontua��o
		updateScore( delta, false );
	}


	private final void updateCrosshairFade( int delta ) {
		final Drawable currentCrosshair = crosshairFade[ crosshair.getCurrFrameIndex() ];
		switch ( crosshairState ) {
			case CROSSHAIR_STATE_APPEARING:
				crosshairFadeTime += delta;
				if ( crosshairFadeTime >= CROSSHAIR_FADE_TIME ) {
					setCrosshairFadeState( CROSSHAIR_STATE_VANISHING );
				}
			break;

			case CROSSHAIR_STATE_VANISHING:
				crosshairFadeTime -= delta;
				if ( crosshairFadeTime <= 0 ) {
					setCrosshairFadeState( CROSSHAIR_STATE_IDLE );
				}
			break;

			default:
				return;
		}

		currentCrosshair.setRefPixelPosition( crosshair.getRefPixelPosition() );
		final Point p = currentCrosshair.getPosition();
		final Point s = currentCrosshair.getSize();

		crosshairFadeTime = ( short ) NanoMath.clamp( crosshairFadeTime, 0, CROSSHAIR_FADE_TIME );
		crosshairViewport.width = s.x * crosshairFadeTime / CROSSHAIR_FADE_TIME;
		crosshairViewport.x = p.x + ( ( s.x - crosshairViewport.width ) >> 1 );
		crosshairViewport.height = s.y * crosshairFadeTime / CROSSHAIR_FADE_TIME;
		crosshairViewport.y = p.y + ( ( s.y - crosshairViewport.height ) >> 1 );
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
			if ( key == ScreenManager.KEY_NUM0 ) {
				puzzle.printStatus();
				pieceManager.setSpecialPieceVisible( true );
			}
		//#endif

		switch ( state ) {
			case STATE_PLAYING:
				switch ( key ) {
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						addCrosshairDirection( DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						addCrosshairDirection( DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						addCrosshairDirection( DIRECTION_UP );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						addCrosshairDirection( DIRECTION_DOWN );
					break;

					case ScreenManager.KEY_NUM1:
						addCrosshairDirection( DIRECTION_UP | DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM3:
						addCrosshairDirection( DIRECTION_UP | DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM7:
						addCrosshairDirection( DIRECTION_DOWN | DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM9:
						addCrosshairDirection( DIRECTION_DOWN | DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
						if ( crosshairState == CROSSHAIR_STATE_IDLE ) {
							setCrosshairFadeState( CROSSHAIR_STATE_VANISHING );
							pieceManager.shot( getCrosshairArea() );
						}
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
						setState( STATE_PAUSED );
					break;
				}
			break;

			case STATE_GAME_OVER:
			case STATE_NEW_RECORD:
				if ( timeToNextState <= -TIME_MESSAGE )
					stateEnded();
			break;
		} // fim switch ( state )
	}


	public final void keyReleased( int key ) {
		if ( state == STATE_PLAYING ) {
			switch ( GameMIDlet.getVendor() ) {
				//#ifdef JAR_100_KB
	//# 			// a vers�o de 100kb � utilizada somente em alguns aparelhos Samsung. Nesses aparelhos, tamb�m ocorre o problema
	//# 			// da mira "presa", onde ela continua se movendo numa dire��o mesmo ap�s se soltar a tecla correspondente.
	//# 			case GameMIDlet.VENDOR_SAMSUNG:
				//#endif

				case GameMIDlet.VENDOR_HTC:
					// nos aparelhos HTC, o estado das teclas fica inv�lido ao se pressionar mais de uma tecla simultaneamente.
					// O evento keyReleased s� � chamado para uma das teclas, e mesmo o valor retornado por getKeyState �
					// inv�lido, armazenando "lixo" mesmo ap�s todas as teclas pressionadas serem soltas. Nesses aparelhos,
					// o jogo s� tratar� uma tecla por vez.
					removeCrosshairDirection( DIRECTION_ALL_MASK );
				break;

				default:
					switch ( key ) {
						case ScreenManager.KEY_NUM4:
						case ScreenManager.LEFT:
							removeCrosshairDirection( DIRECTION_LEFT );
						break;

						case ScreenManager.KEY_NUM6:
						case ScreenManager.RIGHT:
							removeCrosshairDirection( DIRECTION_RIGHT );
						break;

						case ScreenManager.KEY_NUM2:
						case ScreenManager.UP:
							removeCrosshairDirection( DIRECTION_UP );
						break;

						case ScreenManager.KEY_NUM8:
						case ScreenManager.DOWN:
							removeCrosshairDirection( DIRECTION_DOWN );
						break;

						case ScreenManager.KEY_NUM1:
							removeCrosshairDirection( DIRECTION_UP | DIRECTION_LEFT );
						break;

						case ScreenManager.KEY_NUM3:
							removeCrosshairDirection( DIRECTION_UP | DIRECTION_RIGHT );
						break;

						case ScreenManager.KEY_NUM7:
							removeCrosshairDirection( DIRECTION_DOWN | DIRECTION_LEFT );
						break;

						case ScreenManager.KEY_NUM9:
							removeCrosshairDirection( DIRECTION_DOWN | DIRECTION_RIGHT );
						break;
					} // fim switch ( key )
			}
		}
	}


	public final void onShot( byte shotResult, Point position ) {
		switch ( shotResult ) {
			case PieceManager.SHOT_HIT_NONE:
				MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
				changeTime( -TIME_PENALTY_MISS );
				setCombo( 1 );
			break;

			case PieceManager.SHOT_HIT_COMBO:
				// os pontos valem em dobro, mas n�o aumentam o contador de combo
			{
				final short scoreDiff = ( short ) ( ( SCORE_PIECE * multiplier ) << 1 );
				changeScore( scoreDiff );
				getNextScoreLabel().setScore( position, scoreDiff );

				// apenas reinicia o tempo de combo
				setCombo( multiplier );
			}
			break;

			case PieceManager.SHOT_HIT_REGULAR:
				final short scoreDiff = ( short ) ( SCORE_PIECE * multiplier );
				changeScore( scoreDiff );
				getNextScoreLabel().setScore( position, scoreDiff );
				changeTime( TIME_PIECE_INC );
				setCombo( multiplier + 1 );
			break;

			case PieceManager.SHOT_HIT_SPECIAL:
				changeScore( SCORE_SUPER_COMBO * multiplier );
				timeShine = TIME_SHINE;
			break;
		}
	}


	private final void setCrosshairFadeState( int state ) {
		final Drawable currentCrosshair = crosshairFade[ crosshair.getCurrFrameIndex() ];
		switch ( state ) {
			case CROSSHAIR_STATE_APPEARING:
				crosshairFadeTime = 0;
				currentCrosshair.setVisible( true );
				currentCrosshair.setTransform( crosshair.getTransform() );
			break;

			case CROSSHAIR_STATE_VANISHING:
				currentCrosshair.setVisible( true );
				currentCrosshair.setTransform( crosshair.getTransform() );
				crosshairFadeTime = CROSSHAIR_FADE_TIME;
			break;

			case CROSSHAIR_STATE_IDLE:
				currentCrosshair.setVisible( false );
			break;
		}
		crosshairState = ( byte ) state;
	}


	private final void setCombo( int combo ) {
		multiplier = ( byte ) combo;
		clock.setCombo( multiplier - 1 );
		comboTime = ( combo <= 1 ) ? 0 : ( ScreenManager.getInstance().hasPointerEvents() ? TIME_COMBO * TIME_PERCENT_TOUCHSCREEN / 100 : TIME_COMBO );

		if ( multiplier > 1 && ( multiplier % 10 == 1 ) ) {
			pieceManager.setSpecialPieceVisible( true );
		}
	}


	private final void updateCrosshairAcceleration() {
		switch ( crosshairDirection & DIRECTION_H_MASK ) {
			case DIRECTION_LEFT:
				crosshairAccX.setSpeed( -crosshairAcc.x );
				crosshairSpeedLimit.x = crosshairMaxSpeed.x;
			break;

			case DIRECTION_RIGHT:
				crosshairAccX.setSpeed( crosshairAcc.x );
				crosshairSpeedLimit.x = crosshairMaxSpeed.x;
			break;

			default:
				// jogador n�o est� apertando nem para direita nem para esquerda, ou ent�o est� apertando para direita e
				// esquerda ao mesmo tempo
				crosshairAccX.setSpeed( crosshairSpeedX.getSpeed() > 0 ? -Math.abs( crosshairAccX.getSpeed() ) :
										Math.abs( crosshairAccX.getSpeed() ) );
				crosshairSpeedLimit.x = 0;
			break;
		} // fim switch ( crosshairDirection & DIRECTION_H_MASK )

		switch ( crosshairDirection & DIRECTION_V_MASK ) {
			case DIRECTION_UP:
				crosshairAccY.setSpeed( -crosshairAcc.y );
				crosshairSpeedLimit.y = crosshairMaxSpeed.y;
			break;

			case DIRECTION_DOWN:
				crosshairAccY.setSpeed( crosshairAcc.y );
				crosshairSpeedLimit.y = crosshairMaxSpeed.y;
			break;

			default:
				// jogador n�o est� apertando nem para cima nem para baixo, ou ent�o est� apertando para cima e baixo
				// ao mesmo tempo
				crosshairAccY.setSpeed( crosshairSpeedY.getSpeed() > 0 ? -Math.abs( crosshairAccY.getSpeed() ) :
										Math.abs( crosshairAccY.getSpeed() ) );
				crosshairSpeedLimit.y = 0;
			break;
		} // fim switch ( crosshairDirection & DIRECTION_V_MASK )
	} // fim do m�todo updateCrosshairAcceleration()


	private final Rectangle getCrosshairArea() {
		return new Rectangle( crosshair.getRefPixelX() - ( crosshair.getWidth() >> 1 ), crosshair.getRefPixelY() - ( crosshair.getHeight() >> 1 ),
							  crosshair.getWidth(), crosshair.getHeight() );
//		return new Rectangle( crosshair.getRefPixelX() - CROSSHAIR_AREA_HALF, crosshair.getRefPixelY() - CROSSHAIR_AREA_HALF, CROSSHAIR_AREA, CROSSHAIR_AREA );
	}


	private final void updateCrosshair( int delta ) {
		//#if TOUCH == "true"
			switch ( pointerStatus ) {
				case POINTER_STATUS_DRAGGED:
//					final Point previousCrosshairPos = new Point( crosshair.getRefPixelPosition() );
//					crosshair.setRefPixelPosition( lastPointerPos );
//
//					crosshair.move( lastPointerPos.x - previousCrosshairPos.x, lastPointerPos.y - previousCrosshairPos.y );
				break;

				default:
		//#endif
				// atualiza a movimenta��o horizontal da mira
				int speed = crosshairSpeedX.getSpeed();
				// jogador n�o est� movendo a mira horizontalmente
				if ( ( crosshairDirection & DIRECTION_H_MASK ) == DIRECTION_NONE ) {
					crosshairSpeedLimit.x = 0;

					if ( speed > 0 ) {
						if ( crosshairAccX.getSpeed() > 0 )
							crosshairAccX.setSpeed( -crosshairAccX.getSpeed() >> 1 );
					} else {
						if ( crosshairAccX.getSpeed() < 0 )
							crosshairAccX.setSpeed( -crosshairAccX.getSpeed() >> 1 );
					}
				}
				speed += crosshairAccX.updateInt( delta );

				// o teste a seguir a princ�pio n�o seria necess�rio, mas soluciona o problema da mira que come�a a se
				// mover sozinha (quando o jogador p�ra de mover a mira, ela deveria desacelerar at� parar, mas segue
				// acelerando no sentido contr�rio)
				int speedTemp = Math.abs( speed );
				if ( speedTemp >= crosshairSpeedLimit.x ) {
					if ( crosshairSpeedLimit.x == 0 ) {
						// mira est� desacelerando; caso fique abaixo do m�nimo, p�ra de se mover
						if ( speedTemp <= crossHairMinSpeed ) {
							speed = 0;
							crosshairAccX.setSpeed( 0 );
						}
					} else {
						// mira chegou � velocidade m�xima
						speed = speed < 0 ? -crosshairSpeedLimit.x : crosshairSpeedLimit.x;
					}
				} // fim if ( speedTemp > crosshairSpeedLimit.x )
				crosshairSpeedX.setSpeed( speed, false );

				// atualiza a movimenta��o vertical da mira
				speed = crosshairSpeedY.getSpeed();

				// o teste a seguir a princ�pio n�o seria necess�rio, mas soluciona o problema da mira que come�a a se
				// mover sozinha (quando o jogador p�ra de mover a mira, ela deveria desacelerar at� parar, mas segue
				// acelerando no sentido contr�rio)
				if ( ( crosshairDirection & DIRECTION_V_MASK ) == DIRECTION_NONE ) {
					crosshairSpeedLimit.y = 0;

					if ( speed > 0 ) {
						if ( crosshairAccY.getSpeed() > 0 )
							crosshairAccY.setSpeed( -crosshairAccY.getSpeed() >> 1 );
					} else {
						if ( crosshairAccY.getSpeed() < 0 )
							crosshairAccY.setSpeed( -crosshairAccY.getSpeed() >> 1 );
					}
				}

				speed += crosshairAccY.updateInt( delta );
				speedTemp = Math.abs( speed );
				if ( speedTemp >= crosshairSpeedLimit.y ) {
					if ( crosshairSpeedLimit.y == 0 ) {
						// mira est� desacelerando; caso fique abaixo do m�nimo, p�ra de se mover
						if ( speedTemp <= crossHairMinSpeed ) {
							speed = 0;
							crosshairAccY.setSpeed( 0 );
						}
					} else {
						// mira chegou � velocidade m�xima
						speed = speed < 0 ? -crosshairSpeedLimit.y : crosshairSpeedLimit.y;
					}
				} // fim if ( speedTemp > crosshairSpeedLimit.y )
				crosshairSpeedY.setSpeed( speed, false );

				final Point p = crosshair.getRefPixelPosition();
				// move a mira de acordo com a �ltima velocidade registrada, respeitando os limites da tela e do n�vel
				final Point diff = new Point( crosshairSpeedX.updateInt( delta ), crosshairSpeedY.updateInt( delta ) );
				if ( diff.x != 0 ) {
					crosshairMovedX = true;
					p.x += diff.x;
				}
				if ( diff.y != 0 ) {
					crosshairMovedY = true;
					p.y += diff.y;
				}
				
				setCrosshairCenter( p );

				final int percent = Math.max( Math.abs( crosshairSpeedX.getSpeed() * 100 / crosshairMaxSpeed.x ), Math.abs( crosshairSpeedY.getSpeed() * 100 / crosshairMaxSpeed.y ) );
				emitRate.setSpeed( EMIT_RATE_MAX * percent / 100, false );
		//#if TOUCH == "true"
		} // fim switch ( pointerStatus )
		//#endif
	} // fim do m�todo updateCrosshair( int )


	private final void setCrosshairCenter( Point p ) {
		p.x = NanoMath.clamp( p.x, crosshair.getWidth() >> 1, getWidth() - ( crosshair.getWidth() >> 1 ) );
		p.y = NanoMath.clamp( p.y, ( bkgTopLight.getHeight() + CLOCK_SCORE_SPACING ) + ( crosshair.getHeight() >> 1 ), getHeight() - ( crosshair.getHeight() >> 1 ) );

		crosshair.setRefPixelPosition( p );
	}


	private final void resetCrosshairDirection() {
		crosshairDirection = DIRECTION_NONE;
		crosshairSpeedLimit.set( 0, 0 );
		crosshairAccX.setSpeed( 0 );
		crosshairAccY.setSpeed( 0 );
		crosshairSpeedX.setSpeed( 0 );
		crosshairSpeedY.setSpeed( 0 );
		crosshairMovedX = false;
		crosshairMovedY = false;
	}


	private final void addCrosshairDirection( int direction ) {
		final byte previousDirection = crosshairDirection;
		crosshairDirection |= direction;

		if ( ( previousDirection & DIRECTION_H_MASK ) == DIRECTION_NONE ) {
			// iniciou movimento em x
			crosshairMovedX = false;
		}
		if ( ( previousDirection & DIRECTION_V_MASK ) == DIRECTION_NONE ) {
			// iniciou movimento em y
			crosshairMovedY = false;
		}

		updateCrosshairAcceleration();
	}


	private final void removeCrosshairDirection( int direction ) {
		crosshairDirection ^= crosshairDirection & direction;

		// se o jogador parar de se mover numa dire��o antes de ter tido alguma altera��o na posi��o, move a
		// mira um pouco na dire��o indicada (para evitar problemas com aparelhos mais lentos)
		final byte DIRECTION_H = ( byte ) ( direction & DIRECTION_H_MASK );
		if ( DIRECTION_H != DIRECTION_NONE ) {
			if ( crosshairMovedX ) {
				updateCrosshairAcceleration();
			} else {
				crosshairSpeedX.setSpeed( 0 );
				crosshairAccX.setSpeed( 0 );

				switch ( DIRECTION_H ) {
					case DIRECTION_LEFT:
						crosshair.move( -crosshairMinMove.x, 0 );
					break;

					case DIRECTION_RIGHT:
						crosshair.move( crosshairMinMove.x, 0 );
					break;
				}
			}
		}

		final byte DIRECTION_V = ( byte ) ( direction & DIRECTION_V_MASK );
		if ( DIRECTION_V != DIRECTION_NONE ) {
			if ( crosshairMovedY ) {
				updateCrosshairAcceleration();
			} else {
				crosshairSpeedY.setSpeed( 0 );
				crosshairAccY.setSpeed( 0 );

				switch ( DIRECTION_V ) {
					case DIRECTION_UP:
						crosshair.move( 0, -crosshairMinMove.y );
					break;

					case DIRECTION_DOWN:
						crosshair.move( 0, crosshairMinMove.y );
					break;
				}
			}
		}

	}


//#if TOUCH == "true"

		public final void onPointerDragged( int x, int y ) {
			switch ( state ) {
				case STATE_PLAYING:
					if ( pointerStatus != POINTER_STATUS_RELEASED || crosshair.contains( x, y ) ) {
						final Point previousCrosshairPos = crosshair.getRefPixelPosition();
						final Point diff = new Point( lastPointerPos.x - previousCrosshairPos.x, lastPointerPos.y - previousCrosshairPos.y );
						crosshair.move( diff );

						setCrosshairCenter( crosshair.getRefPixelPosition() );

						final long time = System.currentTimeMillis() - lastPointerDraggedTime;
						lastPointerDraggedTime = System.currentTimeMillis();

                        
                        final int percent = NanoMath.clamp( ( int ) Math.max( ( Math.abs( diff.x ) * 1000 / time ) * 100 / crosshairMaxSpeed.x,
																			  ( Math.abs( diff.y ) * 1000 / time ) * 100 / crosshairMaxSpeed.y ), 0, 100 );

						emitRate.setSpeed( EMIT_RATE_MAX_TOUCH * percent / 100, false );

						pointerStatus = POINTER_STATUS_DRAGGED;
						lastPointerPos.set( x, y );
					}
				break;
			}
		}


		public final void onPointerPressed( int x, int y ) {
			switch ( state ) {
				case STATE_PLAYING:
					if ( crosshair.contains( x, y ) ) {
						lastPointerPressedTime = System.currentTimeMillis();
						lastPointerDraggedTime = lastPointerPressedTime;
						pointerStatus = POINTER_STATUS_PRESSED;

						lastPointerPos.set( x, y );
					}
				break;

				default:
					// confirma o fim de jogo
					keyPressed( ScreenManager.FIRE );
				break;
			}
		}


		public final void onPointerReleased( int x, int y ) {
			switch ( state ) {
				case STATE_PLAYING:
					final long interval = System.currentTimeMillis() - lastPointerPressedTime;
					if ( interval < POINTER_CLICK_TIME_TOLERANCE || ( pointerStatus == POINTER_STATUS_PRESSED && crosshair.contains( x, y ) ) ) {
						keyPressed( ScreenManager.FIRE );
					}
				break;
			}

			pointerStatus = POINTER_STATUS_RELEASED;
		}

	//#endif


	private final ScoreLabel getNextScoreLabel() {
		byte candidate = 0;
		for ( byte i = 0; i < SCORE_LABELS_MAX; ++i ) {
			switch ( scoreLabels[ i ].getState() ) {
				case ScoreLabel.STATE_IDLE:
					return scoreLabels[ i ];

				case ScoreLabel.STATE_VANISHING:
					candidate = i;
				break;
			}
		}

		return scoreLabels[ candidate ];
	}


	public final void setState( int state ) {
		final byte previousState = this.state;
		// evita que o fim de n�vel seja definido 2 vezes por PieceManager
		if ( previousState == state )
			return;

		resetCrosshairDirection();

		this.state = ( byte ) state;
		
		timeToNextState = 0;
		labelMessage.setVisible( false );

		//#if SCREEN_SIZE == "SMALL"
//# 			setInfoVisible( false );
//# 			timeInfoVisible = 0;
		//#endif
		
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				showMessage( GameMIDlet.getText( TEXT_LEVEL_CAPS ) + level );

				setCrosshairFadeState( CROSSHAIR_STATE_IDLE );
				setCombo( 1 );
				crosshair.setRefPixelPosition( getWidth() >> 1, getHeight() >> 1 );
				pieceManager.reset();

				puzzle.setState( Puzzle.STATE_CLOSING );

				timeToNextState = TIME_MESSAGE;
			break;
			
			case STATE_PLAYING:
				//#if SCREEN_SIZE == "SMALL"
//# 					timeInfoVisible = TIME_MESSAGE;
//# 					setInfoVisible( true );
				//#elif TOUCH == "true"
					onPointerReleased( 0, 0 );
				//#endif

				if ( !MediaPlayer.isPlaying() )
					MediaPlayer.play( SOUND_INDEX_AMBIENT_1 + Math.abs( musicIndex++ % SOUND_AMBIENT_TOTAL ), MediaPlayer.LOOP_INFINITE );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_PAUSE );
			break;

			case STATE_GAME_OVER:
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ) );
				MediaPlayer.play( SOUND_INDEX_GAME_OVER );

				timeToNextState = TIME_MESSAGE;

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;
			
			case STATE_LEVEL_CLEARED:
				showMessage( GameMIDlet.getText( TEXT_LEVEL_CAPS ) + level + GameMIDlet.getText( TEXT_COMPLETE ) );
				timeToNextState = TIME_MESSAGE;

				updateScore( 0, true );

				MediaPlayer.play( SOUND_INDEX_LEVEL_COMPLETE );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );

				puzzle.setState( Puzzle.STATE_OPENING );
			break;

			case STATE_PAUSED:
				GameMIDlet.setScreen( SCREEN_PAUSE );
			break;

			case STATE_UNPAUSING:
				//#if SCREEN_SIZE == "SMALL"
//# 					setInfoVisible( true );
				//#endif

				timeToNextState = 200;
			break;

			case STATE_NEW_RECORD:
				showMessage( GameMIDlet.getText( TEXT_NEW_RECORD ) );
			break;
		}
	} // fim do m�todo setState( int )
	
	
	private final void stateEnded() {
		switch ( state ) {
			case STATE_LEVEL_CLEARED:
				prepareNextLevel();
			break;
			
			case STATE_BEGIN_LEVEL:
			case STATE_UNPAUSING:
				setState( STATE_PLAYING );
			break;
			
			case STATE_GAME_OVER:
				if ( GameMIDlet.isHighScore( score ) )
					setState( STATE_NEW_RECORD );
				else
					GameMIDlet.gameOver( score );
			break;

			case STATE_NEW_RECORD:
				GameMIDlet.gameOver( score );
			break;
		}
	}


	public final void prepareNextLevel() {
		prepareLevel( level + 1 );
	}
	
	
	public final short getLevel() {
		return level;
	}


	private final void prepareLevel( int level ) {
		this.level = ( byte ) level;

		final byte DIFFICULTY_LEVEL = ( byte ) Math.min( level, LEVEL_HARD );

		setCombo( 1 );

		pieceManager.prepare( DIFFICULTY_LEVEL );

		timeMax = TIME_TOTAL_EASY - ( TIME_TOTAL_EASY - TIME_TOTAL_HARD ) * DIFFICULTY_LEVEL / LEVEL_HARD;
		if ( ScreenManager.getInstance().hasPointerEvents() ) {
			timeMax = timeMax * TIME_PERCENT_TOUCHSCREEN / 100;
		}
		timeLeft = timeMax;
		updateTime( 0 );

		//#if DEBUG == "true"
			System.out.println( "N�VEL" + level + " -> " + timeMax + " segundos." );
		//#endif

		setState( STATE_BEGIN_LEVEL );
	}


	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			refreshScoreLabel();
		}
	} // fim do m�todo updateScore( boolean )


	private final void refreshScoreLabel() {
//		AppMIDlet.gc();
//		scoreLabel.setText( String.valueOf( Runtime.getRuntime().freeMemory() ) );

		// enche de zeros � esquerda do placar
		final StringBuffer buffer = new StringBuffer();
		for ( int s = 1; s <= SCORE_SHOWN_MAX + 1; s *= 10 ) {
			if ( s - 1 > scoreShown )
				buffer.append( '0' );
		}
		buffer.append( scoreShown );
		scoreLabel.setText( buffer.toString() );
		scoreLabel.setPosition( getWidth() - scoreLabel.getWidth() - CLOCK_SCORE_SPACING, scoreLabel.getPosY() );
	}	

	
	private final void updateTime( int delta ) {
		changeTime( -delta );

		if ( timeLeft > 0 ) {
			// evita que o tempo de combo se reduza antes das pe�as entrarem na tela
			if ( comboTime > 0 && pieceManager.canDecreaseComboTime() ) {
				comboTime -= delta;
				if ( comboTime <= 0 )
					setCombo( 1 );
			}
		} else {
			setState( STATE_GAME_OVER );
		}
	}


	/**
	 * Incrementa a pontua��o e atualiza as vari�veis e labels correspondentes.
	 * @param diff varia��o na pontua��o (positiva ou negativa).
	 */
	private final void changeScore( int diff ) {
		score += diff;
		
		scoreSpeed.setSpeed( ( score - scoreShown ) >> 1, false );
	}


	private final void changeTime( int diff ) {
		timeLeft += diff;

		if ( timeLeft > timeMax )
			timeLeft = timeMax;
		else if ( timeLeft < 0 )
			timeLeft = 0;

		clock.setProgress( timeLeft, timeMax, comboTime );
	}


	private final void showMessage( String message ) {
		labelMessage.setText( "<ALN_H>" + message );
		labelMessage.setSize( ScreenManager.SCREEN_WIDTH, labelMessage.getTextTotalHeight() );
		labelMessage.setPosition( 0, ( size.y - labelMessage.getHeight() >> 1 ) );
		labelMessage.setVisible( true );

		//#if SCREEN_SIZE == "SMALL"
//# 			setInfoVisible( true );
		//#endif
	}


	//#if SCREEN_SIZE == "SMALL"
//# 	private final void setInfoVisible( boolean visible ) {
//# 		if ( size.y < HEIGHT_MIN ) {
//# 			scoreLabel.setVisible( visible );
//# 		}
//# 	}
	//#endif
	
	
	public final int getScore() {
		return score;
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		emitter.setSize( size );

		crosshairMaxSpeed.set( width * 1000 / CROSSHAIR_SCREEN_TIME, height * 1000 / CROSSHAIR_SCREEN_TIME );
		crosshairAcc.set( crosshairMaxSpeed.mul( 4 ) );

		// ajuste para jogar melhor com a "bolinha" do BlackBerry
		if ( GameMIDlet.getVendor() == GameMIDlet.VENDOR_BLACKBERRY )
			crosshairMinMove.set( width / 10, height / 10 );
		else
			crosshairMinMove.set( width >> 5, height >> 5 );

		final int CLOCK_HEIGHT = clock.getHeight() + ( CLOCK_SCORE_SPACING << 1 );
		puzzle.setSize( width, height - CLOCK_HEIGHT );
		puzzle.setPosition( ( width - puzzle.getWidth() ) >> 1, CLOCK_HEIGHT + ( ( height - CLOCK_HEIGHT - puzzle.getHeight() ) >> 1 ) );
		pieceManager.setSize( width, height - CLOCK_HEIGHT );
		Piece.setPuzzlePosition( puzzle.getPosition(), pieceManager.getPosition() );
		refreshScoreLabel();

		puzzleBorder.setSize( puzzle.getSize().add( puzzleBorder.getBorderSize() ) );
		puzzleBorder.setPosition( puzzle.getPosition().sub( puzzleBorder.getTopLeft() ) );

		clock.setPosition( CLOCK_SCORE_SPACING, CLOCK_SCORE_SPACING );
		scoreLabel.setPosition( scoreLabel.getPosX(), clock.getPosY() + ( ( clock.getHeight() - scoreLabel.getHeight() ) >> 1 ) );

		// ajusta os patterns de fundo
		bkgTopDark1.setSize( width, clock.getPosY() );
		bkgTopLight.setPosition( 0, bkgTopDark1.getHeight() );
		bkgTopLight.setSize( width, clock.getHeight() );

		bkgTopDark2.setPosition( 0, bkgTopLight.getPosY() + bkgTopLight.getHeight() );
		bkgTopDark2.setSize( width, puzzle.getPosY() - bkgTopDark2.getPosY() );

		bkgLeft.setPosition( 0, bkgTopDark2.getPosY() + bkgTopDark2.getHeight() );
		bkgLeft.setSize( puzzleBorder.getPosX() + 1, height - bkgLeft.getPosY() );
		
		bkgRight.setSize( width - puzzleBorder.getPosX() - puzzleBorder.getWidth() + 1, bkgLeft.getHeight() );
		bkgRight.setPosition( width - bkgRight.getWidth(), bkgLeft.getPosY() );

		bkgBottom.setSize( width, height - puzzleBorder.getPosY() - puzzleBorder.getHeight() + 1 );
		bkgBottom.setPosition( 0, height - bkgBottom.getHeight() );
	}


	public final void hideNotify( boolean deviceEvent) {
		resetCrosshairDirection();
		
		if ( state == STATE_PLAYING )
			setState( STATE_PAUSED );

		//#if TOUCH == "true"
			pointerStatus = POINTER_STATUS_RELEASED;
		//#endif
	}


	public final void showNotify( boolean deviceEvent) {
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != size.x || height != size.y )
			setSize( width, height );
	}


	public final Puzzle getPuzzle() {
		return puzzle;
	}


	public final Clock getClock() {
		return clock;
	}


	public final void setCrosshairTarget( Sprite target ) {
		if ( target != null ) {
			crosshair.setFrame( target.getFrameSequenceIndex() );
			crosshair.setSize( crosshair.getCurrentFrameImage().getWidth(), crosshair.getCurrentFrameImage().getHeight() );
			crosshair.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			crosshair.setTransform( target.getTransform() );
		}
	}


}
