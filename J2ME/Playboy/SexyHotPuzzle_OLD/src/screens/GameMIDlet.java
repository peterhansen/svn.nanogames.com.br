/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Clock;
import core.ParticleEmitter;
import core.Piece;
import java.util.Hashtable;
import core.SpecialPiece;

//#ifndef NO_RECOMMEND
	import br.com.nanogames.components.util.SmsSender;
	import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if NANO_RANKING == "true"
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
//#endif

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
	import br.com.nanogames.components.userInterface.form.TouchKeyPad;
//#endif

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener {

	private static final short GAME_MAX_FRAME_TIME = 135;

	private static final byte BACKGROUND_TYPE_SPECIFIC = 0;

	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;

	private static final byte BACKGROUND_TYPE_NONE = 2;

	private static int softKeyRightText;
	/** gerenciador da animação da soft key esquerda */
	private static BasicAnimatedSoftkey softkeyLeft;

	/** gerenciador da animação da soft key esquerda */
	private static BasicAnimatedSoftkey softkeyMid;

	/** gerenciador da animação da soft key direita */
	private static BasicAnimatedSoftkey softkeyRight;

	/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
	private static final int LOW_MEMORY_LIMIT = 1050000;

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif

	//#ifndef NO_RECOMMEND
		private BorderedScreen recommendScreen;
	//#endif

	/** Referência para a tela de jogo, para que seja possível retornar à tela de jogo após entrar na tela de pausa. */
	private static PlayScreen playScreen;

	private static boolean lowMemory;

	private static Pattern bkg;


	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#elif VENDOR == "MOTOROLA"
//# 			super( VENDOR_MOTOROLA, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
		//#endif
			
		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	}


	public static final void log( int i ) {
//		AppMIDlet.gc();
//		Logger.log( i + ": " + Runtime.getRuntime().freeMemory() );
	}


//	public static final void log( Throwable t ) {
//		AppMIDlet.gc();
//		Logger.log( t.getMessage() );
//	}


	protected final void loadResources() throws Exception {
		log( 1 );
		//#if JAR == "min"
//# 			lowMemory = true;
		//#else
			switch ( getVendor() ) {
				//#if SCREEN_SIZE == "BIG"
					case VENDOR_SAMSUNG:
					case VENDOR_NOKIA:
					case VENDOR_MOTOROLA:
				//#endif

				case VENDOR_SONYERICSSON:
				case VENDOR_SIEMENS:
				case VENDOR_BLACKBERRY:
				case VENDOR_HTC:
				break;

				default:
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
			}
		//#endif
//		lowMemory = true; // TODO teste

		//#if BLACKBERRY_API == "true"
//# 			// em aparelhos antigos (e/ou com versões de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms não recebe os eventos de teclado corretamente, sumindo somente após a aplicação ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
		//#endif

		for ( int i = isLowMemory() ? 1 : 0; i < FONT_TYPES_TO_LOAD; ++i ) {
			FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
			log( 6 );
		}

		if ( isLowMemory() )
			FONTS[ FONT_BIG ] = FONTS[ FONT_TEXT_WHITE ];
		else
			FONTS[ FONT_BIG ].setCharExtraOffset( 1 );

		FONTS[ FONT_TEXT_WHITE ].setCharExtraOffset( 1 );
		FONTS[ FONT_MESSAGE ].setCharExtraOffset( -1 );
		FONTS[ FONT_COMBO ].setCharExtraOffset( -2 );
		FONTS[ FONT_SCORE_SMALL ].setCharExtraOffset( -1 );

		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}

		// carrega os textos do jogo
		loadTexts( TEXT_TOTAL, PATH_IMAGES + "pt.dat" );

		log( 8 );

		if ( isLowMemory() ) {
			bkg = new Pattern( COLOR_BACKGROUND );
		} else {
			bkg = new Pattern( new DrawableImage( PATH_IMAGES + "bg.png" ) );
		}
		final int dim = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		bkg.setSize( dim, dim );

		setScreen( SCREEN_LOADING_1 );
	} // fim do método loadResources()


	public final void destroy() {
		if ( playScreen != null ) {
			//#if NANO_RANKING == "true"
				try {
					final Customer c = NanoOnline.getCurrentCustomer();
					RankingScreen.setHighScore( RANKING_TYPE_SCORE, c.getId(), playScreen.getScore(), true );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
				}
			//#else
//# 				HighScoresScreen.setScore( playScreen.getScore() );
			//#endif
				
			playScreen = null;
		}

		super.destroy();
	}


	public static final boolean isHighScore( int score ) {
		//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				return RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true ) > 0;
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
			return false;
		//#else
//# 			return HighScoresScreen.isHighScore( score ) >= 0;
		//#endif
	}


	public static final void gameOver( int score ) {
		//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				if ( score > 0 && RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true ) > 0 ) {
					setScreen( SCREEN_LOADING_HIGH_SCORES );
				} else {
					setScreen( SCREEN_MAIN_MENU );
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				setScreen( SCREEN_MAIN_MENU );
			}
		//#else
//# 			if ( HighScoresScreen.setScore( score ) ) {
//# 				setScreen( SCREEN_HIGH_SCORES );
//# 			} else {
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			}
		//#endif
	}


	protected final int changeScreen( int screen ) throws Exception {
		log( 10 );
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = BACKGROUND_TYPE_SPECIFIC;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;

		short visibleTime = ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME;

		switch ( screen ) {
//			case SCREEN_CHOOSE_LANGUAGE:
			case SCREEN_CHOOSE_SOUND:
				//#if DEBUG == "true"
					// evita a lentidão no emulador ao vibrar
					MediaPlayer.setVibration( false );
				//#endif

				nextScreen = new SedaMenu( midlet, screen, new int[] { TEXT_YES, TEXT_NO }, TEXT_DO_YOU_WANT_SOUND );
				( ( SedaMenu ) nextScreen ).setCurrentIndex( MediaPlayer.isMuted() ? CONFIRM_INDEX_NO : CONFIRM_INDEX_YES );
				( ( SedaMenu ) nextScreen ).setBackIndex( CONFIRM_INDEX_NO );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_SPLASH_NANO:
				//#if NANO_RANKING == "false"
//# 					HighScoresScreen.createInstance( DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
				//#endif

				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "SMALL"
//# 					nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, 1 );
				//#else
					nextScreen = new BasicSplashNano( SCREEN_LOADING_2, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, 1 );
				//#endif
			break;

//			case SCREEN_SPLASH_BRAND:
//				bkgType = BACKGROUND_TYPE_NONE;
//				nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_SPLASH + "nano.png", TEXT_SPLASH_BRAND  );
//			break;

			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame();
//				bkgType = BACKGROUND_TYPE_NONE;
			break;

			case SCREEN_MAIN_MENU:
				//#if NANO_RANKING == "true"
					if ( nanoOnlineForm != null ) {
						NanoOnline.unload();
						nanoOnlineForm = null;
					}
				//#endif

				//#ifndef NO_RECOMMEND
					recommendScreen = null;
				//#endif

				playScreen = null;
				int[] entries = null;
				//#if WEB_EMULATOR == "true"
//# 					entries = new int[] {
//# 							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
							//#if NANO_RANKING == "true"
//# 								TEXT_NANO_RANKING,
							//#else
//# 								TEXT_HIGH_SCORES,
							//#endif
//# 							TEXT_HELP,
//# 							TEXT_CREDITS,
//# 							};
//# 						nextScreen = new SedaMenu( midlet, screen, entries );
				//#else
					indexSoftRight = TEXT_EXIT;

					//#ifdef NO_RECOMMEND
//# 						final boolean smsSupported = false;
					//#elif BLACKBERRY_API == "true"
//# 						final boolean smsSupported = showRecommendScreen && SmsSender.isSupported();
					//#else
						final boolean smsSupported = SmsSender.isSupported();
					//#endif

					entries =  smsSupported ? new int[] {
						TEXT_NEW_GAME,
						TEXT_OPTIONS,
						//#if NANO_RANKING == "true"
							TEXT_NANO_RANKING,
						//#else
//# 						TEXT_HIGH_SCORES,
						//#endif
							TEXT_RECOMMEND_TITLE,
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,
						//#if DEBUG == "true"
					TEXT_LOG_TITLE,
						//#endif
							}
						: new int[] {
							TEXT_NEW_GAME,
							TEXT_OPTIONS,
						//#if NANO_RANKING == "true"
							TEXT_NANO_RANKING,
						//#else
//# 						TEXT_HIGH_SCORES,
						//#endif
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,
						//#if DEBUG == "true"
					TEXT_LOG_TITLE,
						//#endif
					};
						nextScreen = new SedaMenu( midlet, screen, entries );
				//#endif
				( ( SedaMenu ) nextScreen ).setBackIndex( entries.length - 1 );

				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_NEXT_LEVEL:
				playScreen.prepareNextLevel();
			case SCREEN_CONTINUE_GAME:
				nextScreen = playScreen;
				bkgType = BACKGROUND_TYPE_NONE;

				if ( screen == SCREEN_CONTINUE_GAME ) {
					playScreen.sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
					playScreen.setState( PlayScreen.STATE_UNPAUSING );
				}
			break;

			case SCREEN_OPTIONS:
				SedaMenu optionsScreen = null;

				if ( MediaPlayer.isVibrationSupported() ) {
					optionsScreen = new SedaMenu( midlet, screen, new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
					optionsScreen.setBackIndex( ENTRY_OPTIONS_MENU_VIB_BACK );
				} else {
					optionsScreen = new SedaMenu( midlet, screen, new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );
					optionsScreen.setBackIndex( ENTRY_OPTIONS_MENU_NO_VIB_BACK );
				}
 				nextScreen = optionsScreen;

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			//#if NANO_RANKING == "true"

				case SCREEN_HIGH_SCORES:
					setSpecialKeyMapping( false );

					nextScreen = nanoOnlineForm;
					bkgType = BACKGROUND_TYPE_NONE;
				break;

				case SCREEN_NANO_RANKING_MENU:
					nextScreen = nanoOnlineForm;
					bkgType = BACKGROUND_TYPE_NONE;
				break;

				case SCREEN_LOADING_NANO_ONLINE:
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							MediaPlayer.free();
							setSpecialKeyMapping( false );

							nanoOnlineForm = NanoOnline.load( APP_SHORT_NAME, SCREEN_MAIN_MENU );

							loadScreen.setActive( false );

							setScreen( SCREEN_NANO_RANKING_MENU );
						}

					}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

				break;

				case SCREEN_NANO_RANKING_PROFILES:
					nextScreen = nanoOnlineForm;
					bkgType = BACKGROUND_TYPE_NONE;
				break;

				case SCREEN_LOADING_PROFILES_SCREEN:
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							setSpecialKeyMapping( false );
							nanoOnlineForm = NanoOnline.load( APP_SHORT_NAME, SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT );

							loadScreen.setActive( false );

							setScreen( SCREEN_NANO_RANKING_PROFILES );
						}

					}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );
				break;

				case SCREEN_CHOOSE_PROFILE:
					setSpecialKeyMapping( false );

					final Customer c = NanoOnline.getCurrentCustomer();
					nextScreen = new SedaMenu( midlet, screen, new int[] { TEXT_CONFIRM, TEXT_CHOOSE_ANOTHER, TEXT_BACK },
											   getText( TEXT_CHOOSE_PROFILE ) + ( c.getId() == Customer.ID_NONE ? getText( TEXT_NO_PROFILE ) : c.getNickname() ) );
					( ( SedaMenu ) nextScreen ).setCurrentIndex( CONFIRM_INDEX_YES );
					( ( SedaMenu ) nextScreen ).setBackIndex( CONFIRM_INDEX_CANCEL );
					indexSoftLeft = TEXT_OK;
				break;

				case SCREEN_LOADING_HIGH_SCORES:
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							MediaPlayer.free();
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

							setSpecialKeyMapping( false );
							playScreen = null;
							nanoOnlineForm = NanoOnline.load( APP_SHORT_NAME, SCREEN_MAIN_MENU, NanoOnline.SCREEN_NEW_RECORD );

							loadScreen.setActive( false );

							setScreen( SCREEN_HIGH_SCORES );
						}

					}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );
				break;

			//#else
//# 
//# 				case SCREEN_HIGH_SCORES:
//# 					// lista de ranking local
//# 					nextScreen = HighScoresScreen.createInstance( DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
//# 					indexSoftRight = TEXT_BACK;
//# 				break;
//# 
			//#endif

			case SCREEN_HELP:
				final SpecialPiece specialPiece = new SpecialPiece( null );
				final Drawable[] pieces = Piece.getPieces();
				final Drawable[] specialChars = new Drawable[ pieces.length + 1 ];
				System.arraycopy( pieces, 0, specialChars, 0, pieces.length );
				specialChars[ pieces.length ] = specialPiece;

				nextScreen = new BorderedScreen( getText( TEXT_HELP_TEXT ) + getText( TEXT_VERSION ) + getMIDletVersion(), SCREEN_MAIN_MENU, false, specialChars );
				bkgType = BACKGROUND_TYPE_NONE;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_CREDITS:
				nextScreen = new BorderedScreen( getText( TEXT_CREDITS_TEXT ) + getText( TEXT_VERSION ) + getMIDletVersion(), SCREEN_MAIN_MENU, true );

				bkgType = BACKGROUND_TYPE_NONE;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_PAUSE:
				SedaMenu pauseScreen = null;
				if ( MediaPlayer.isVibrationSupported() ) {
					pauseScreen = new SedaMenu( midlet, screen, new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					pauseScreen = new SedaMenu( midlet, screen, new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_TOGGLE_SOUND, -1, -1 );
				}

				pauseScreen.setBackIndex( ENTRY_PAUSE_MENU_CONTINUE );
				nextScreen = pauseScreen;
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;
			break;

			case SCREEN_CONFIRM_MENU:
				nextScreen = new SedaMenu( midlet, screen, new int[] { TEXT_YES, TEXT_NO }, TEXT_CONFIRM_BACK_MENU );
				( ( SedaMenu ) nextScreen ).setCurrentIndex( CONFIRM_INDEX_NO );
				( ( SedaMenu ) nextScreen ).setBackIndex( CONFIRM_INDEX_NO );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT:
				nextScreen = new SedaMenu( midlet, screen, new int[] { TEXT_YES, TEXT_NO }, TEXT_CONFIRM_EXIT );
				( ( SedaMenu ) nextScreen ).setCurrentIndex( CONFIRM_INDEX_NO );
				( ( SedaMenu ) nextScreen ).setBackIndex( CONFIRM_INDEX_NO );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								// aloca os sons
								final String[] soundList = new String[ SOUND_TOTAL ];
								for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
									soundList[i] = PATH_SOUNDS + i + ".mid";
									log( 11 );
								}

								Piece.load();

								MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								//#if BLACKBERRY_API == "true"
//# 									MediaPlayer.setVolume( 60 );
								//#endif

								SedaMenu.load();

								// teste
//								ScreenManager.SCREEN_HEIGHT = 260;
//								ScreenManager.SCREEN_HALF_HEIGHT = ScreenManager.SCREEN_HEIGHT >> 1;

								log( 12 );

								log( 13 );

								softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
								softkeyMid = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_MID );
								softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );

								log( 21 );

								final ScreenManager manager = ScreenManager.getInstance();

								log( 22 );
								manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
								manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );

								loadScreen.setActive( false );

								setScreen( SCREEN_CHOOSE_SOUND );
							}


						}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						ParticleEmitter.load();

						SpecialPiece.load();

						log( 23 );

						// inicializa as tabelas de ranking online
						//#if NANO_RANKING == "true"
							RankingScreen.init( new int[] { TEXT_HIGH_SCORES }, new RankingFormatter() {
								public String format( int type, long score ) {
									return String.valueOf( score );
								}


								public final void initLocalEntry( int type, RankingEntry entry, int index ) {
									entry.setScore( ( 10 - index ) * SCORE_PIECE * 25 );
									entry.setNickname( HIGH_SCORE_DEFAULT_NICKNAME );
								}
						} );
						//#endif

						log( 24 );
						log( 25 );
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						
						PlayScreen.load();

						log( 26 );

						loadScreen.setActive( false );

						setScreen( SCREEN_MAIN_MENU );
					}

				}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_GAME:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						Clock.load();

						setSpecialKeyMapping( true );

						playScreen = new PlayScreen();

						loadScreen.setActive( false );

						setScreen( SCREEN_NEXT_LEVEL );
					}

				}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

				MediaPlayer.free();
//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			//#ifndef NO_RECOMMEND

				case SCREEN_RECOMMEND_SENT:
					nextScreen = new BorderedScreen( getText( TEXT_RECOMMEND_SENT ) + getRecommendURL() + "\n\n", SCREEN_MAIN_MENU, false );
					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_RECOMMEND:
					nextScreen = recommendScreen;

					indexSoftRight = TEXT_BACK;
					bkgType = BACKGROUND_TYPE_NONE;
				break;

				case SCREEN_LOADING_RECOMMEND_SCREEN:
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							setSpecialKeyMapping( false );
							final ScreenRecommend recommend = new ScreenRecommend( SCREEN_MAIN_MENU );
							final Form f = new Form( recommend );

							//#if SCREEN_SIZE == "BIG" && TOUCH == "true"
								f.setTouchKeyPad( new TouchKeyPad( getFont( FONT_TEXT_WHITE ), new DrawableImage( PATH_IMAGES + "clear.png" ), new DrawableImage( "/online/shift.png" ), getBorder(), 0x002831, 0x428a9c, 0x215963 ) );
							//#endif

							recommendScreen = new BorderedScreen( f, 0 );

							loadScreen.setActive( false );

							setScreen( SCREEN_RECOMMEND );
						}

					}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );
				break;

			//#endif

			//#if DEBUG == "true"
				case SCREEN_ERROR_LOG:
					nextScreen = new BorderedScreen( texts[ TEXT_LOG_TEXT ], SCREEN_MAIN_MENU, false );
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
					indexSoftRight = TEXT_BACK;
				break;
			//#endif
		} // fim switch ( screen )


		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE ) {
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft, visibleTime );
		}

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE ) {
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, visibleTime );
		}

		setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, -1 );

		ScreenManager.getInstance().setCurrentScreen( nextScreen );
		setBackground( bkgType );

		// reduz a chance de lentidão por excesso de objetos a desalocar (caso do BlackBerry)
		System.gc();

		return screen;
	} // fim do método changeScreen( int )


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SPECIFIC:
				midlet.manager.setBackground( bkg, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
			break;
		}
	} // fim do método setBackground( byte )


	public static final Pattern getBackground() {
		if ( isLowMemory() )
			return new Pattern( COLOR_BACKGROUND );

		return new Pattern( bkg.getFill() );
	}


	public static final ScrollBar getScrollBar() throws Exception {
		return new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT,
										COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Border getBorder() throws Exception {
		final LineBorder b = new LineBorder( 0xff7601, LineBorder.TYPE_ROUND_RAISED ) {
			public void setState( int state ) {
				super.setState( state );

				switch ( state ) {
					case STATE_PRESSED:
						setColor( 0xff4b00 );
						setFillColor( 0xff4b00 );
					break;

					case STATE_FOCUSED:
						setColor( 0xff7601 );
						setFillColor( 0xffab00 );
					break;

					default:
						setColor( 0xff7601 );
						setFillColor( 0xff7601 );
					break;
				}
			}
		};

		return b;
	}


	/**
	 * 
	 * @return
	 */
	public static final boolean isLowMemory() {
		return lowMemory;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif
					
				//#ifdef NO_RECOMMEND
//# 					if ( index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//# 						++index;
//# 					}
				//#endif

				//#ifndef NO_RECOMMEND
					//#if WEB_EMULATOR == "false"
						if ( !SmsSender.isSupported() && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
							++index;
						}
					//#endif
				//#endif

				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						//#if NANO_RANKING == "true"
							setScreen( SCREEN_CHOOSE_PROFILE );
						//#else
//# 							setScreen( SCREEN_LOADING_GAME );
						//#endif
						break;

					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
						break;


					//#if NANO_RANKING == "false"
//# 						case ENTRY_MAIN_MENU_LOCAL_RANKING:
//# 							setScreen( SCREEN_HIGH_SCORES );
//# 						break;
					//#else
						case ENTRY_MAIN_MENU_NANO_RANKING:
							setScreen( SCREEN_LOADING_NANO_ONLINE );
						break;
					//#endif

					//#if DEMO == "true"
//# 					case ENTRY_MAIN_MENU_BUY_FULL_GAME:
//# 						setScreen( SCREEN_BUY_GAME_RETURN );
//# 					break;
					//#endif
						
					case ENTRY_MAIN_MENU_RECOMMEND:
						setScreen( SCREEN_LOADING_RECOMMEND_SCREEN );
						break;

					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP );
						break;

					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
						break;

					//#if DEBUG == "true"
						case ENTRY_MAIN_MENU_ERROR_LOG:
							setScreen( SCREEN_ERROR_LOG );
						break;
					//#endif

					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
						break;
				} // fim switch ( index )
				break; // fim case SCREEN_MAIN_MENU


			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE

			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS

			//#if DEMO == "true"
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 							e.printStackTrace();
			//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif

			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case CONFIRM_INDEX_YES:
						//#if DEMO == "false"
							MediaPlayer.saveOptions();
							MediaPlayer.stop();
							gameOver( playScreen.getScore() );
							playScreen = null;
						//#else
//# 						gameOver( 0 );
						//#endif
						break;

					case CONFIRM_INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case CONFIRM_INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
						break;

					case CONFIRM_INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == CONFIRM_INDEX_NO );

				setScreen( SCREEN_SPLASH_NANO );
			break;

			//#if NANO_RANKING == "true"
				case SCREEN_CHOOSE_PROFILE:
					switch ( index ) {
						case CONFIRM_INDEX_YES:
							setScreen( SCREEN_LOADING_GAME );
						break;

						case CONFIRM_INDEX_NO:
							setScreen( SCREEN_LOADING_PROFILES_SCREEN );
						break;

						case CONFIRM_INDEX_CANCEL:
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				break;
			//#endif

//			case SCREEN_CHOOSE_LANGUAGE:
//				index -= BasicConfirmScreen.INDEX_YES;
//				if ( index != getLanguage() )
//					setLanguage( ( byte ) index);
//
//				setScreen( SCREEN_CHOOSE_SOUND );
//			break;
		} // fim switch ( id )		
	} // fim do método onChoose( Menu, int, int )


	public final void onItemChanged( Menu menu, int id, int index ) {
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
			softKeyRightText = textIndex;
		
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( FONT_TEXT_WHITE, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do método setSoftKeyLabel( byte, int )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_MID:
				if ( softkeyMid != null ) {
					softkeyMid.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	} // fim do método setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}

	
	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
		
		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//# 
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//# 
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//# 
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//# 
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//# 
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//# 
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//# 
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//# 		
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}

		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
		}
	}


	private static final class BorderedScreen extends UpdatableGroup implements KeyListener
			//#if SCREEN_SIZE != "SMALL" && TOUCH == "true"
			, PointerListener
			//#endif
	{

		private static final short TIME_TRANSITION = 600;

		private static final byte STATE_HIDDEN		= 0;
		private static final byte STATE_APPEARING	= 1;
		private static final byte STATE_SHOWN		= 2;
		private static final byte STATE_HIDING		= 3;

		private byte state;

		private short accTime;

		private final KeyListener keyListener;

		//#if SCREEN_SIZE != "SMALL" && TOUCH == "true"
			private final PointerListener pointerListener;
		//#endif

		private final int nextScreen;

		private final boolean handleKeys;

		private final Pattern bkgTop;
		private final Pattern bkgBottom;
		private final Pattern bkgLeft;
		private final Pattern bkgRight;
		private final Pattern fill;

		private final Rectangle contentViewport = new Rectangle();
		private final Rectangle topViewport = new Rectangle();
		private final Rectangle leftViewport = new Rectangle();
		private final Rectangle rightViewport = new Rectangle();
		private final Rectangle bottomViewport = new Rectangle();

		private final Drawable screen;


		public BorderedScreen( int textIndex, int nextScreen ) throws Exception {
			this( textIndex, nextScreen, false );
		}


		public BorderedScreen( int textIndex, int nextScreen, boolean autoScroll ) throws Exception {
			this( loadFormText( getText( textIndex ), autoScroll ), nextScreen, true );
		}


		public BorderedScreen( String text, int nextScreen, boolean autoScroll ) throws Exception {
			this( loadFormText( text, autoScroll ), nextScreen, true );
		}


		public BorderedScreen( String text, int nextScreen, boolean autoScroll, Drawable[] specialPieces ) throws Exception {
			this( loadFormText( text, autoScroll, specialPieces ), nextScreen, true );
		}


		public BorderedScreen( Container screen, int nextScreen ) throws Exception {
			this( screen, nextScreen, true );
		}

		
		public BorderedScreen( Container screen, int nextScreen, boolean setScreenSize ) throws Exception {
			super( 6 );

			this.screen = screen;
			this.nextScreen = nextScreen;

			//#ifndef NO_RECOMMEND
				if ( !( screen instanceof Form ) ) {
					final Form f = new Form( screen );
					screen = f;
				}
			//#endif

			//#if SCREEN_SIZE != "SMALL" && TOUCH == "true"
				if ( screen instanceof PointerListener )
					pointerListener = ( PointerListener ) screen;
				else
					pointerListener = null;
			//#endif

			if ( screen instanceof KeyListener )
				keyListener = ( KeyListener ) screen;
			else
				keyListener = null;

			if ( isLowMemory() ) {
				bkgTop = new Pattern( COLOR_BACKGROUND );
				bkgBottom = new Pattern( COLOR_BACKGROUND );
				bkgLeft = new Pattern( COLOR_BACKGROUND );
				bkgRight = new Pattern( COLOR_BACKGROUND );
			} else {
				bkgTop = new Pattern( bkg.getFill() );
				bkgBottom = new Pattern( bkg.getFill() );
				bkgLeft = new Pattern( bkg.getFill() );
				bkgRight = new Pattern( bkg.getFill() );
			}
			
			bkgTop.setViewport( topViewport );
			bkgBottom.setViewport( bottomViewport );
			bkgLeft.setViewport( leftViewport );
			bkgRight.setViewport( rightViewport );

			if ( isLowMemory() )
				fill = new Pattern( COLOR_BACKGROUND_LIGHT );
			else
				fill = new Pattern( new DrawableImage( PATH_IMAGES + "p.png" ) );
			fill.setViewport( contentViewport );

			insertDrawable( bkgTop );
			insertDrawable( bkgBottom );
			insertDrawable( bkgLeft );
			insertDrawable( bkgRight );
			insertDrawable( fill );

			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			fill.setSize( size );
			bkgLeft.setSize( size );
			bkgTop.setSize( size );
			bkgRight.setSize( size );
			bkgBottom.setSize( size );

			final Point borderSize = detectSize();
			final Point diff = size.sub( borderSize ).div( 2 );

			//#ifdef NO_RECOMMEND
//# 				handleKeys = true;
//# 				if ( setScreenSize )
//# 					screen.setSize( borderSize.x - ( BORDER_THICKNESS << 1 ) - BORDER_HALF_THICKNESS, borderSize.y - BORDER_THICKNESS );
//# 				screen.setPosition( diff.x + ( BORDER_THICKNESS << 1 ), diff.y + BORDER_HALF_THICKNESS );
			//#else
				if ( screen instanceof Form ) { // gambiarra para tratar corretamente o scroll
					handleKeys = false;
					if ( setScreenSize )
						screen.setSize( borderSize.x - ( BORDER_THICKNESS << 1 ), borderSize.y - BORDER_THICKNESS );
					screen.setPosition( diff.x + BORDER_THICKNESS, diff.y + BORDER_HALF_THICKNESS );
				} else {
					handleKeys = true;
					if ( setScreenSize )
						screen.setSize( borderSize.x - ( BORDER_THICKNESS << 1 ) - BORDER_HALF_THICKNESS, borderSize.y - BORDER_THICKNESS );
					screen.setPosition( diff.x + ( BORDER_THICKNESS << 1 ), diff.y + BORDER_HALF_THICKNESS );
				}
			//#endif

			screen.setViewport( contentViewport );
			insertDrawable( screen );

			setState( STATE_APPEARING );
		}


		private static final FormText loadFormText( String text, boolean autoScroll ) throws Exception {
			return loadFormText( text, autoScroll, null );
		}


		private static final FormText loadFormText( String text, boolean autoScroll, Drawable[] specialChars ) throws Exception {
			final Point size = detectSize();
			final FormText textScreen = new FormText( GetFont( FONT_TEXT_WHITE ), size.x, specialChars ) {
				public final void keyPressed( int key ) {
					switch ( key ) {
						case ScreenManager.KEY_NUM5:
						case ScreenManager.FIRE:
						case ScreenManager.KEY_CLEAR:
						case ScreenManager.KEY_BACK:
						case ScreenManager.KEY_SOFT_RIGHT:
						case ScreenManager.KEY_SOFT_LEFT:
						case ScreenManager.KEY_SOFT_MID:
							GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						break;

						default:
							super.keyPressed( key );
					}
				}
			};

			if ( autoScroll ) {
				textScreen.setAutoScroll( true );
				textScreen.setSize( size.x - ( BORDER_THICKNESS << 1 ), size.y - BORDER_THICKNESS );
				textScreen.setAutoScroll( true );
			} else {
				final ScrollBar scrollBar = getScrollBar();
				textScreen.setSize( size.x - Math.max( scrollBar.getWidth(), scrollBar.getWidth() ), size.y - BORDER_THICKNESS );
				textScreen.setScrollBarV( scrollBar );
			}
			textScreen.setText( text );

			return textScreen;
		}


		private static final Point detectSize() {
			//#if SCREEN_SIZE == "SMALL"
//# 				if ( ScreenManager.SCREEN_HEIGHT < HEIGHT_MIN )
//# 					return new Point( ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
//# 				else
//# 					return new Point( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#else
				return new Point( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#endif
		}


		public final void update( int delta ) {
			super.update( delta );

			switch ( state ) {
				case STATE_APPEARING:
					accTime += delta;
					setVisibleArea( accTime * 100 / TIME_TRANSITION );

					if ( accTime >= TIME_TRANSITION )
						setState( STATE_SHOWN );
				break;

				case STATE_HIDING:
					accTime -= delta;
					setVisibleArea( accTime * 100 / TIME_TRANSITION );

					if ( accTime <= 0 )
						setState( STATE_HIDDEN );
				break;
			}
		}


		public final void setState( int state ) {
			this.state = ( byte ) state;

			switch ( state ) {
				case STATE_HIDDEN:
					setVisibleArea( 0 );
					if ( handleKeys )
						GameMIDlet.setScreen( nextScreen );
				break;

				case STATE_APPEARING:
					accTime = 0;
					setVisibleArea( 0 );
				break;

				case STATE_SHOWN:
					accTime = TIME_TRANSITION;
					setVisibleArea( 100 );
				break;

				case STATE_HIDING:
				break;
			}
		}


		public final void setVisibleArea( int percent ) {
			percent = NanoMath.clamp( percent, 0, 100 );
			final int width = screen.getWidth() * percent / 100;
			final int height = screen.getHeight() * percent / 100;
			contentViewport.set( ( ScreenManager.SCREEN_WIDTH - width ) >> 1, ( ScreenManager.SCREEN_HEIGHT - height ) >> 1, width, height );

			topViewport.set( 0, 0, getWidth(), contentViewport.y );
			leftViewport.set( 0, topViewport.height, contentViewport.x, height );
			rightViewport.set( contentViewport.x + width, leftViewport.y, getWidth() - contentViewport.x + width, height );
			bottomViewport.set( 0, leftViewport.y + height, getWidth(), getHeight() - contentViewport.y + height );
		}


		public final void keyPressed( int key ) {
			switch ( state ) {
				case STATE_APPEARING:
					setState( STATE_SHOWN );
				break;

				case STATE_HIDING:
					setState( STATE_HIDDEN );
				break;

				case STATE_SHOWN:
					switch ( key ) {
						case ScreenManager.FIRE:
						case ScreenManager.KEY_NUM5:
						case ScreenManager.KEY_SOFT_LEFT:
						case ScreenManager.KEY_SOFT_RIGHT:
						case ScreenManager.KEY_CLEAR:
						case ScreenManager.KEY_BACK:
							if ( handleKeys ) {
								setState( STATE_HIDING );
								break;
							}

						default:
							if ( keyListener != null )
								keyListener.keyPressed( key );
					}
				break;
			}
		}


		public final void keyReleased( int key ) {
			if ( state == STATE_SHOWN && keyListener != null )
				keyListener.keyReleased( key );
		}


		//#if SCREEN_SIZE != "SMALL" && TOUCH == "true"
			public final void onPointerDragged( int x, int y ) {
				if ( state == STATE_SHOWN && pointerListener != null )
					pointerListener.onPointerDragged( x, y );
			}


			public final void onPointerPressed( int x, int y ) {
				if ( state == STATE_SHOWN && pointerListener != null )
					pointerListener.onPointerPressed( x, y );
			}


			public final void onPointerReleased( int x, int y ) {
				if ( state == STATE_SHOWN && pointerListener != null )
					pointerListener.onPointerReleased( x, y );
			}
		//#endif

	} // fim da classe interna BorderedTextScreen


	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;


	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualização do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private final byte previousScreen;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );

			previousScreen = currentScreen;

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );

			ScreenManager.setKeyListener( null );

			//#if TOUCH == "true"
				ScreenManager.setPointerListener( null );
			//#endif
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo são carregados aqui para evitar sobrecarga do método loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// só inicia a thread quando a tela atual for a ativa, para evitar possíveis atrasos em transições e
					// garantir que novos recursos só serão carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );

						//#if TOUCH == "true"
							ScreenManager.setPointerListener( null );
						//#endif

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
									texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();

									setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									//#else
//# 									// volta à tela anterior
//# 									setScreen( previousScreen );
								//#endif
								}
							} // fim do método run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execução
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do método update( int )


		public final void paint( Graphics g ) {
			super.paint( g );

			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	// </editor-fold>
}
