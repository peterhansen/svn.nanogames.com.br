/**
 * SedaMenu.java
 * 
 * Created on Jul 27, 2009, 7:32:40 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public final class SedaMenu extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if TOUCH == "true"
		, PointerListener
//#endif
{

	private static final byte STATE_HIDDEN		= 0;
	private static final byte STATE_APPEARING_1	= 1;
	private static final byte STATE_APPEARING_2	= 2;
	private static final byte STATE_APPEARING_3	= 3;
	private static final byte STATE_SHOWN		= 4;
	private static final byte STATE_HIDING_1	= 5;
	private static final byte STATE_HIDING_2	= 6;
	private static final byte STATE_HIDING_3	= 7;

	private byte state;

	/** Tempo em milisegundos da anima��o do c�rculo surgindo ou sumindo. */
	private static final short TIME_CIRCLE_TRANSITION = 666;

	/** Tempo em milisegundos da anima��o da barra de op��es surgindo ou sumindo. */
	private static final short TIME_BAR_TRANSITION = 330;

	/** Tempo em milisegundos da anima��o do conte�do central surgindo ou sumindo. */
	private static final short TIME_CONTENT_TRANSITION = 240;

	/***/
	private static final short TRANSITION_ANGULAR_SPEED = 300;

	private static final byte PIECES_TOTAL = ( byte ) ( CIRCLE_DEFAULT_DIAMETER / ( GameMIDlet.isLowMemory() ? 16 : 12 ) );

	/***/
	private short accTime;

	/** Total de imagens ao redor do c�rculo. */
	private static final byte IMAGES_TOTAL = 12;

	private final DrawableImage borderLeft;
	private final DrawableImage borderRight;
	private final Pattern fill;
	private final DrawableImage arrowLeft;
	private final DrawableImage arrowRight;
	private final DrawableImage arrowUp;
	private final DrawableImage arrowDown;

	private final Rectangle barLeftViewport = new Rectangle();
	private final Rectangle barRightViewport = new Rectangle();

	private final Label entry;

	private final int[] entries;

	private final byte soundIndex;

	private final byte vibrationIndex;

	private final int soundOffTextIndex;

	private final int vibrationOffTextIndex;

	private byte currentIndex;

	private final DrawableImage[] images;

	private final DrawableGroup imagesGroup;

	private final Rectangle imagesViewport = new Rectangle();

	private final DrawableGroup contentGroup;

	private final Rectangle contentViewport = new Rectangle();

	private final int id;

	private final MenuListener listener;

	private final MUV angularSpeed = new MUV( 30 );

	/** Indica se uma volta no c�rculo deve ser completada (ou seja, passar pelo �ngulo 0/360) antes de ser efetivamente
	 * feito o teste de chegada ao destino. */
	private boolean completeCircle;

	private short circleDiameter;

	private short angle;

	private short angleFinal;

	private byte backIndex = -1;

	private static final byte PIECES_TOTAL_TYPES = 7;

	private static DrawableImage[] PIECES;

	private static DrawableImage PRODUCTS;
	private static DrawableImage ARROW_LEFT;
	private static DrawableImage BORDER_LEFT;
	private static DrawableImage FILL;
	private static DrawableImage ARROW_UP;

	private final DrawableImage products;


	/** Estrutura que armazena a �ltima entrada selecionada de cada menu. A chave de cada entrada � o id do menu,
	 * e o valor da chave � o �ndice da �ltima entrada utilizada.
	 */
	protected static final Hashtable LAST_INDEX = new Hashtable();	


	public static final void load() throws Exception {
		if ( PIECES == null ) {

			//#if SCREEN_SIZE == "BIG"
				PIECES = new DrawableImage[ PIECES_TOTAL_TYPES ];
			//#else
//# 				PIECES = new DrawableImage[ GameMIDlet.isLowMemory() ? 2 : PIECES_TOTAL_TYPES ];
//# 				final PaletteMap[] paletteMap = new PaletteMap[] {
//# 					new PaletteMap( 0xb23b00, 0x0f292f ),
//# 					new PaletteMap( 0xffffff, 0xff6800 ),
//# 				};
			//#endif

			for ( byte i = 0; i < PIECES.length; ++i ) {
				//#if SCREEN_SIZE == "BIG"
					PIECES[ i ] = new DrawableImage( PATH_MENU + "p_" + i + ".png" );
				//#else
//# 					final PaletteChanger paletteChanger = new PaletteChanger( PATH_PIECES + "s_" + i + ".png" );
//# 					PIECES[ i ] = new DrawableImage( paletteChanger.createImage( paletteMap ) );
				//#endif
			}

			if ( !GameMIDlet.isLowMemory() )
				PRODUCTS = new DrawableImage( PATH_MENU + "products.png" );

			FILL = new DrawableImage( PATH_MENU + "bar.png" );
			ARROW_LEFT = new DrawableImage( PATH_MENU + "left.png" );
			ARROW_UP = new DrawableImage( PATH_MENU + "up.png" );
			BORDER_LEFT = new DrawableImage( PATH_MENU + "border.png" );
		}
	}


	public SedaMenu( MenuListener listener, int id, int[] entries ) throws Exception {
		this( listener, id, entries, null, -1, -1, -1, -1 );
	}


	public SedaMenu( MenuListener listener, int id, int[] entries, int titleIndex ) throws Exception {
		this( listener, id, entries, GameMIDlet.getText( titleIndex ), -1, -1, -1, -1 );
	}

	
	public SedaMenu( MenuListener listener, int id, int[] entries, int soundIndex, int soundOffText, int vibrationIndex, int vibrationOffText ) throws Exception {
		this( listener, id, entries, null, soundIndex, soundOffText, vibrationIndex, vibrationOffText );
	}


	public SedaMenu( MenuListener listener, int id, int[] entries, String title ) throws Exception {
		this( listener, id, entries, title, -1, -1, -1, -1 );
	}


	public SedaMenu( MenuListener listener, int id, int[] entries, String titleText, int soundIndex, int soundOffText, int vibrationIndex, int vibrationOffText ) throws Exception {
		super( 5 + IMAGES_TOTAL * 3 + entries.length );

		this.listener = listener;
		this.id = id;
		this.entries = entries;
		this.soundIndex = ( byte ) soundIndex;
		this.soundOffTextIndex = soundOffText;
		this.vibrationIndex = ( byte ) vibrationIndex;
		this.vibrationOffTextIndex = vibrationOffText;

		images = new DrawableImage[ PIECES_TOTAL ];
		imagesGroup = new DrawableGroup( images.length );
		imagesGroup.setViewport( imagesViewport );
		for ( byte i = 0; i < images.length; ++i ) {
			images[ i ] = new DrawableImage( PIECES[ i % PIECES.length ] );
			images[ i ].defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			imagesGroup.insertDrawable( images[ i ] );
		}

		contentGroup = new DrawableGroup( 2 );
		insertDrawable( contentGroup );

		if ( titleText != null ) {
			final RichLabel title = new RichLabel( FONT_TEXT_WHITE, titleText, CIRCLE_INTERNAL_DIAMETER * 9 / 10 );
			title.setSize( title.getWidth(), title.getTextTotalHeight() );
			contentGroup.insertDrawable( title );
			products = null;
		} else {
			if ( GameMIDlet.isLowMemory() ) {
				products = null;
			} else {
				products = new DrawableImage( PRODUCTS );
				products.setViewport( contentViewport );
				contentGroup.insertDrawable( products );
			}
		}
		contentGroup.setViewport( contentViewport );

		borderLeft = new DrawableImage( BORDER_LEFT );
		borderLeft.setViewport( barLeftViewport );
		insertDrawable( borderLeft );

		borderRight = new DrawableImage( borderLeft );
		borderRight.setViewport( barRightViewport );
		borderRight.mirror( TRANS_MIRROR_H );
		insertDrawable( borderRight );

		fill = new Pattern( FILL );
		insertDrawable( fill );

		arrowLeft = new DrawableImage( ARROW_LEFT );
		arrowLeft.defineReferencePixel( -arrowLeft.getWidth(), arrowLeft.getHeight() >> 1 );
		arrowLeft.setViewport( barLeftViewport );
		insertDrawable( arrowLeft );

		arrowRight = new DrawableImage( arrowLeft );
		arrowRight.defineReferencePixel( arrowRight.getWidth() << 1, arrowRight.getHeight() >> 1 );
		arrowRight.setViewport( barRightViewport );
		arrowRight.mirror( TRANS_MIRROR_H );
		insertDrawable( arrowRight );

		arrowUp = new DrawableImage( ARROW_UP );
		arrowUp.defineReferencePixel( arrowUp.getWidth() >> 1, arrowUp.getHeight() );
		insertDrawable( arrowUp );

		arrowDown = new DrawableImage( arrowUp );
		arrowDown.mirror( TRANS_MIRROR_V );
		arrowDown.defineReferencePixel( arrowDown.getWidth() >> 1, 0 );
		insertDrawable( arrowDown );

		entry = new Label( GameMIDlet.GetFont( FONT_TEXT_WHITE ), null );
		insertDrawable( entry );

		final Integer lastIndex = ( Integer ) LAST_INDEX.get( new Integer( id ) );
		if ( lastIndex == null )
			setCurrentIndex( 0 );
		else {
			final int index = lastIndex.intValue();
			if ( index == backIndex )
				setCurrentIndex( 0 );
			else
				setCurrentIndex( index );
		}
		
		positionImages();
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		setState( STATE_APPEARING_1 );
	}


	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;
		
		switch ( state ) {
			case STATE_APPEARING_1:
				setCircleDiameter( CIRCLE_DEFAULT_DIAMETER * accTime / TIME_CIRCLE_TRANSITION );
				updateAngle( delta, false );

				if ( accTime >= TIME_CIRCLE_TRANSITION ) {
					setState( STATE_APPEARING_2 );
				}
			break;

			case STATE_APPEARING_2:
				setBarWidth( MENU_BAR_WIDTH * accTime / TIME_BAR_TRANSITION );
				updateAngle( delta, true );

				if ( accTime >= TIME_BAR_TRANSITION ) {
					setState( STATE_APPEARING_3 );
				}
			break;

			case STATE_APPEARING_3:
				setContentPosY( accTime * 100 / TIME_CONTENT_TRANSITION );
				updateAngle( delta, true );

				if ( accTime >= TIME_CONTENT_TRANSITION ) {
					setState( STATE_SHOWN );
				}
			break;

			case STATE_HIDING_1:
				setContentPosY( ( TIME_CONTENT_TRANSITION - accTime ) * 100 / TIME_CONTENT_TRANSITION );

				if ( accTime >= TIME_CONTENT_TRANSITION ) {
					setState( STATE_HIDING_2 );
				}
			break;

			case STATE_HIDING_2:
				setBarWidth( MENU_BAR_WIDTH * ( TIME_BAR_TRANSITION - accTime ) / TIME_BAR_TRANSITION );

				if ( accTime >= TIME_BAR_TRANSITION ) {
					setState( STATE_HIDING_3 );
				}
			break;

			case STATE_HIDING_3:
				if ( circleDiameter <= 0 && accTime >= TIME_CIRCLE_TRANSITION ) {
					setState( STATE_HIDDEN );
				} else {
					setCircleDiameter( CIRCLE_DEFAULT_DIAMETER * ( TIME_CIRCLE_TRANSITION - accTime ) / TIME_CIRCLE_TRANSITION );
					updateAngle( delta, false );
				}
			break;

			case STATE_SHOWN:
				updateAngle( delta, true );
			break;
		}
	}


	private final void updateAngle( int delta, boolean checkLimit ) {
		final short previousAngle = angle;
		angle = ( short ) ( ( angle + angularSpeed.updateInt( delta ) ) );

		if ( NanoMath.sgn( previousAngle ) != NanoMath.sgn( angle ) )
			completeCircle = false;

		if ( checkLimit ) {
			if ( angularSpeed.getSpeed() > 0 ) {
				if ( !completeCircle && angle >= angleFinal ) {
					angle = ( short ) ( angleFinal % 360 );
					angularSpeed.setSpeed( 0 );
				}
			} else if ( angularSpeed.getSpeed() < 0 ) {
				if ( !completeCircle && angle <= angleFinal ) {
					angle = ( short ) ( angleFinal % 360 );
					angularSpeed.setSpeed( 0 );
				}
			}
		}
		positionImages();
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;
		
		setVisible( state != STATE_HIDDEN );

		switch ( state ) {
			case STATE_HIDDEN:
				if ( listener != null ) {
					// grava o �ndice atual para futuras inst�ncias de menu com o mesmo id
					LAST_INDEX.put( new Integer( id ), new Integer( currentIndex ) );
					listener.onChoose( null, id, currentIndex );
				}
			break;

			case STATE_APPEARING_1:
				setArrowUpDownVisible( false );
				arrowLeft.setVisible( false );
				arrowRight.setVisible( false );

				setCircleDiameter( 0 );
				setContentPosY( 0 );
				angularSpeed.setSpeed( TRANSITION_ANGULAR_SPEED );
				setBarWidth( 0 );
				setCurrentIndex( currentIndex );
			break;

			case STATE_APPEARING_2:
				setCircleDiameter( CIRCLE_DEFAULT_DIAMETER );
			break;

			case STATE_APPEARING_3:
				setBarWidth( MENU_BAR_WIDTH );
				updateAngle( 0, false );
			break;

			case STATE_HIDING_1:
			break;

			case STATE_HIDING_2:
			break;

			case STATE_HIDING_3:
				angularSpeed.setSpeed( -TRANSITION_ANGULAR_SPEED );
			break;

			case STATE_SHOWN:
				setCircleDiameter( CIRCLE_DEFAULT_DIAMETER );
				setBarWidth( MENU_BAR_WIDTH );
				setContentPosY( 100 );
				arrowLeft.setVisible( true );
				arrowRight.setVisible( true );
				setCurrentIndex( currentIndex );
			break;
		}
	}


	private final void setContentPosY( int percent ) {
		contentGroup.setPosition( ( getWidth() - contentGroup.getWidth() ) >> 1, 
								  fill.getPosY() + ( ( getHeight() - contentGroup.getHeight() >> 1 ) - fill.getPosY() ) * percent / 100 );
	}


	protected final void paint( Graphics g ) {
		int s = circleDiameter;

		// azul marinho
		fillCircle( g, 0x17373e, s );
		s -= CIRCLE_THICKNESS_1;

		// azul escuro (quase preto)
		fillCircle( g, 0x051d22, s );
		s -= CIRCLE_THICKNESS_2;

		imagesGroup.draw( g );

		// laranja escuro
		fillCircle( g, 0xd65200, s );
		s -= CIRCLE_THICKNESS_3;

		// laranja m�dio
		fillCircle( g, 0xe17205, s );
		s -= CIRCLE_THICKNESS_4;

		// laranja claro
		fillCircle( g, 0xf6891f, s );
		s -= CIRCLE_THICKNESS_5;

		// bege claro
		fillCircle( g, 0xffdd69, s );
		s -= CIRCLE_THICKNESS_6;

		// bege escuro
		fillCircle( g, 0xf7b348, s );
		s -= CIRCLE_THICKNESS_7;

		// azul interno
		fillCircle( g, 0x091316, s );

		super.paint( g );
	}


	private final void fillCircle( Graphics g, int color, int size ) {
		if ( size > 0 ) {
			g.setColor( color );
			g.fillArc( ( ScreenManager.SCREEN_WIDTH - size ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size ) >> 1, size, size, 0, 360 );
		}
	}


	public final void hideNotify( boolean deviceEvent) {
	}


	public final void showNotify( boolean deviceEvent) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			super.setSize( width, height );

			imagesGroup.setSize( size );

			final int y = ( height >> 1 ) + ( CIRCLE_DEFAULT_DIAMETER >> 2 );

			borderLeft.setPosition( ( width >> 1 ) - ( CIRCLE_DEFAULT_DIAMETER >> 1 ), y );
			fill.setPosition( borderLeft.getPosX() + borderLeft.getWidth(), borderLeft.getPosY() );
			borderRight.setPosition( ( width >> 1 ) + ( CIRCLE_DEFAULT_DIAMETER >> 1 ) - borderRight.getWidth(), y );
			fill.setSize( borderRight.getPosX() - fill.getPosX(), fill.getHeight() );
			arrowLeft.setPosition( fill.getPosX(), fill.getPosY() + ( ( fill.getHeight() - arrowLeft.getHeight() ) >> 1 ) );
			arrowRight.setPosition( borderRight.getPosX() - ( arrowRight.getWidth() ), arrowLeft.getPosY() );

			if ( contentGroup.getUsedSlots() > 0 ) {
				if ( products == null ) {
					contentGroup.setSize( contentGroup.getDrawable( 0 ).getWidth(), contentGroup.getDrawable( 0 ).getHeight() );
				} else {
					contentGroup.setSize( products.getWidth(), products.getPosY() + products.getHeight() );
				}
			}
			contentViewport.set( 0, 0, width, fill.getPosY() );
			contentGroup.setPosition( ( width - contentGroup.getWidth() ) >> 1, contentGroup.getPosY() );

			barLeftViewport.set( 0, 0, width >> 1, height );
			barRightViewport.set( width >> 1, 0, width >> 1, height );
		}
	}


	private final void setBarWidth( int width ) {
		borderLeft.setPosition( ( getWidth() - width ) >> 1, borderLeft.getPosY() );
		borderRight.setPosition( ( ( getWidth() + width ) >> 1 ) - borderRight.getWidth(), borderRight.getPosY() );

		fill.setPosition( borderLeft.getPosX() + borderLeft.getWidth(), borderLeft.getPosY() );
		fill.setSize( borderRight.getPosX() - fill.getPosX(), fill.getHeight() );

		arrowLeft.setPosition( fill.getPosX(), fill.getPosY() + ( ( fill.getHeight() - arrowLeft.getHeight() ) >> 1 ) );
		arrowRight.setPosition( borderRight.getPosX() - ( arrowRight.getWidth() ), arrowLeft.getPosY() );

		refreshLabel();
	}


	private final void setCircleDiameter( int diameter ) {
		circleDiameter = ( short ) Math.min( diameter, CIRCLE_DEFAULT_DIAMETER );

		if ( circleDiameter > CIRCLE_MIN_THICKNESS )
			imagesViewport.set( ScreenManager.SCREEN_HALF_WIDTH - ( circleDiameter >> 1 ), ScreenManager.SCREEN_HALF_HEIGHT - ( circleDiameter >> 1 ), circleDiameter, circleDiameter );
		else
			imagesViewport.set( 0, 0, 0, 0 );
	}

	private final void refreshLabel() {
		entry.setSize( Math.min( fill.getWidth(), entry.getFont().getTextWidth( entry.getText() ) ), entry.getHeight() );
		entry.setPosition( ( getWidth() - entry.getWidth() ) >> 1,
						   fill.getPosY() + ( ( fill.getHeight() - entry.getHeight() ) >> 1 ) );

		arrowUp.setRefPixelPosition( getWidth() >> 1, entry.getPosY() );
		arrowDown.setRefPixelPosition( getWidth() >> 1, entry.getPosY() + entry.getHeight() );
	}


	public final void setCurrentIndex( int index ) {
		currentIndex = ( byte ) ( ( entries.length + index ) % entries.length );

		if ( currentIndex == soundIndex ) {
			setArrowUpDownVisible( true );
			entry.setText( MediaPlayer.isMuted() ? soundOffTextIndex : entries[ currentIndex ] );
		} else if ( currentIndex == vibrationIndex ) {
			setArrowUpDownVisible( true );
			entry.setText( MediaPlayer.isVibration() ? entries[ currentIndex ] : vibrationOffTextIndex );
		} else {
			setArrowUpDownVisible( false );
			entry.setText( entries[ currentIndex ] );
		}

		refreshLabel();

		angleFinal = ( short ) ( ( 90 + ( currentIndex * 360 / entries.length ) ) % 360 );
		angularSpeed.setSpeed( ( angleFinal - angle ) << 1 );
		completeCircle = NanoMath.sgn( angularSpeed.getSpeed() ) != NanoMath.sgn( angleFinal - angle );
	}


	private final void setArrowUpDownVisible( boolean v ) {
		switch ( state ) {
			case STATE_APPEARING_2:
			case STATE_APPEARING_3:
			case STATE_SHOWN:
			break;

			default:
				v = false;
		}
		
		arrowUp.setVisible( v );
		arrowDown.setVisible( v );
	}


	private final void positionImages() {
		final Point p = new Point();
		final Point center = new Point( getWidth() >> 1, getHeight() >> 1 );
		final int PERCENT = Math.min( 15 + ( circleDiameter * 105 / CIRCLE_DEFAULT_DIAMETER ), 100 );
		final int DISTANCE = ( ( circleDiameter - ( CIRCLE_THICKNESS_1 >> 1 ) ) >> 1 ) * PERCENT / 100;
		for ( int i = 0; i < images.length; ++i ) {
			p.setVector( angle + ( i * 360 / images.length ), DISTANCE );
			images[ i ].setRefPixelPosition( center.add( p ) );
		}
	}


	public final void setBackIndex( int backIndex ) {
		this.backIndex = ( byte ) backIndex;
	}


	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_APPEARING_1:
			case STATE_APPEARING_2:
			case STATE_APPEARING_3:
				setState( STATE_SHOWN );
			break;

			case STATE_HIDING_1:
			case STATE_HIDING_2:
			case STATE_HIDING_3:
				setState( STATE_HIDDEN );
			break;

			case STATE_SHOWN:
				switch ( key ) {
					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						setCurrentIndex( currentIndex + 1 );
					break;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						setCurrentIndex( currentIndex - 1 );
					break;

					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						if ( currentIndex == soundIndex ) {
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							MediaPlayer.play( SOUND_INDEX_LEVEL_COMPLETE );
							setCurrentIndex( currentIndex );
						} else if ( currentIndex == vibrationIndex ) {
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							setCurrentIndex( currentIndex );
						}
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						if ( backIndex >= 0 ) {
							setCurrentIndex( backIndex );
							setState( STATE_HIDING_1 );
						}
					break;

					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_SOFT_LEFT:
						if ( currentIndex == soundIndex || currentIndex == vibrationIndex )
							keyPressed( ScreenManager.UP );
						else
							setState( STATE_HIDING_1 );
					break;

				} // fim switch ( key )
			break;
		}
	}


	public final void keyReleased( int key ) {
	}


	//#if SCREEN_SIZE == "BIG"

		public final void onPointerDragged( int x, int y ) {
		}


		public final void onPointerPressed( int x, int y ) {
			if ( y >= fill.getPosY() && y <= fill.getPosY() + fill.getHeight() ) {
				final int third = ( ( x - borderLeft.getPosX() ) * 3 ) / MENU_BAR_WIDTH;

				if ( third <= 0) {
					keyPressed( ScreenManager.LEFT );
				} else if ( third >= 2 ) {
					keyPressed( ScreenManager.RIGHT );
				} else {
					if ( arrowUp.isVisible() ) {
						keyPressed( y < fill.getPosY() + ( fill.getHeight() >> 1 ) ? ScreenManager.UP : ScreenManager.DOWN );
					} else {
						keyPressed( ScreenManager.FIRE );
					}
				}
			}
		}


		public final void onPointerReleased( int x, int y ) {
			keyReleased( 0 );
		}

	//#endif


}
