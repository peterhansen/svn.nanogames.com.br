/**
 * RegisterScreen2.java
 *
 * Created on 21/Dez/2008, 15:51:00
 *
 */

//#ifndef NO_RECOMMEND

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.ImageBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.SmsSender;
import br.com.nanogames.components.util.SmsSenderListener;
import core.Clock;
import core.Constants;

/**
 *
 * @author Peter
 */
public final class ScreenRecommend extends Container implements Constants, EventListener, SmsSenderListener {


	/***/
	private static final byte ENTRY_TEXT			= 0;
	private static final byte ENTRY_BOXES			= ENTRY_TEXT + 1;
	private static final byte ENTRY_KEYPAD			= ENTRY_BOXES + 1;

	/***/
	private static final byte ENTRY_BUTTON_OK			= ENTRY_KEYPAD + 1;
	/***/
	private static final byte ENTRY_BUTTON_BACK			= ENTRY_BUTTON_OK + 1;

	/***/
	private static final byte ENTRIES_TOTAL = ( ENTRY_BUTTON_BACK + 1 ) << 1;

	/***/
	private final TextBox textBox;

	/***/
	private final Button buttonOK;

	/***/
	private final Button buttonBack;


	public ScreenRecommend( int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );

		FlowLayout flowLayout = new FlowLayout( FlowLayout.AXIS_VERTICAL, ANCHOR_HCENTER );
		flowLayout.start.set( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS );
		flowLayout.gap.set( BORDER_THICKNESS, BORDER_THICKNESS );

		setLayout( flowLayout );

		setScrollableY( true );
		setScrollBarV( GameMIDlet.getScrollBar() );

		final ImageFont font = GameMIDlet.GetFont( FONT_TEXT_WHITE );

		final FormText text = new FormText( font, ScreenManager.SCREEN_WIDTH, ( ScreenManager.SCREEN_HEIGHT / font.getHeight() ) * 5 / 10 );
		text.setText( GameMIDlet.getText( TEXT_RECOMMEND_TEXT ) );
		text.setScrollableY( true );
		text.setScrollBarV( new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL ) );
		text.getScrollBarV().setSize( 0, 1 );
		text.addEventListener( this );
		insertDrawable( text );

		textBox = new TextBox( font, null, 12, TextBox.INPUT_MODE_NUMBERS, false );
		textBox.setCaret( new DrawableImage( PATH_IMAGES + "caret.png" ) );
		textBox.setId( ENTRY_BOXES );
		textBox.addEventListener( this );
		textBox.setBorder( loadBorder() );
		insertDrawable( textBox );

		buttonOK = new Button( font, ' ' + GameMIDlet.getText( TEXT_OK ) + ' ' );
		buttonOK.setId( ENTRY_BUTTON_OK );
		buttonOK.setBorder( GameMIDlet.getBorder() );
		buttonOK.setKeyListener( this );
		buttonOK.addEventListener( this );


		buttonBack = new Button( font, ' ' + GameMIDlet.getText( TEXT_BACK ) + ' ' );
		buttonBack.setId( ENTRY_BUTTON_BACK );
		buttonBack.addEventListener( this );
		buttonBack.setKeyListener( this );
		buttonBack.setBorder( GameMIDlet.getBorder() );


		flowLayout = new FlowLayout( FlowLayout.AXIS_HORIZONTAL );
		flowLayout.start.set( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS );
		flowLayout.gap.set( BORDER_THICKNESS, BORDER_THICKNESS );

		final Container container = new Container( 2, flowLayout );
		container.insertDrawable( buttonOK );
		container.insertDrawable( buttonBack );
		insertDrawable( container );


		final int SPACER_HEIGHT = font.getHeight();
		final Component spacer = new Component( 0 ) {

			public final Point calcPreferredSize( Point maximumSize ) {
				return new Point( 0, SPACER_HEIGHT );
			}


			public final String getUIID() {
				return "";
			}
		};
		spacer.setKeyListener( this );

		insertDrawable( spacer );
	}


	private static final ImageBorder loadBorder() throws Exception {
		return new ImageBorder( PATH_BORDER, Clock.COLOR_CLOCK_DARK, Clock.COLOR_CLOCK_LIGHT, Clock.COLOR_CLOCK_DARK );
	}


	public final Point calcPreferredSize( Point maximumSize ) {
		return new Point( ScreenManager.SCREEN_WIDTH, Integer.MAX_VALUE );
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		}
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_BACK:
						GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						evt.consume();
					break;

					case ENTRY_BUTTON_OK:
						final String number = textBox.getText();
						if ( number.length() > 7 ) {
							final SmsSender sender = new SmsSender();
							sender.send( textBox.getText(), GameMIDlet.getText( TEXT_RECOMMEND_SMS ) + GameMIDlet.getRecommendURL(), this );
							GameMIDlet.setScreen( SCREEN_RECOMMEND_SENT );
						} else {
							textBox.requestFocus();
						}
						evt.consume();
					break;
				}
			break;

			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
				evt.consume();
			break;

			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				switch ( sourceId ) {
					case ENTRY_BOXES:
						// se estiver no modo edit�vel, altera o label da soft key direita ao dar foco a um dos textBox
						if ( buttonBack.getId() == ENTRY_BUTTON_BACK && GameMIDlet.getSoftKeyRightTextIndex() != TEXT_CLEAR )
							GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_CLEAR );
					break;

					default:
						if ( GameMIDlet.getSoftKeyRightTextIndex() != TEXT_BACK )
							GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						evt.consume();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do m�todo eventPerformed( Event )


	public final void onSMSSent( int id, boolean smsSent ) {
		//#if DEBUG == "true"
			System.out.println( "onSMSSent( " + id + ", " + smsSent );
		//#endif
	}

	public void onSMSLog( int id, String log ) {
		//#if DEBUG == "true"
			System.out.println( "onSMSLog( " + id + ", " + log );
		//#endif
	}

}


//#endif