/**
 * SpecialPiece.java
 * 
 * Created on Sep 9, 2009, 5:48:05 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class SpecialPiece extends UpdatableGroup implements Constants, SpriteListener {

	private static final byte TOTAL_SHINES = 3;

	private final Sprite[] specialPieceShine = new Sprite[ TOTAL_SHINES ];

	private final DrawableImage image;

	private final DrawableImage border;

	private static DrawableImage IMAGE;

	private static DrawableImage BORDER;

	private short specialPieceTime;

	private short blinkTime;

	private final Rectangle specialPieceViewport = new Rectangle();

	private static final short TIME_SPECIAL_PIECE_BLINK = 186;
	private static final short TIME_SPECIAL_PIECE_APPEAR = 411;
	private static final short TIME_SPECIAL_PIECE_VISIBLE = 3300;

	public static final byte SPECIAL_PIECE_STATE_NONE		= 0;
	public static final byte SPECIAL_PIECE_STATE_APPEARING	= 1;
	public static final byte SPECIAL_PIECE_STATE_VISIBLE	= 2;
	public static final byte SPECIAL_PIECE_STATE_VANISHING	= 3;

	private byte specialPieceState;

	private final Drawable parent;
	
	
	public SpecialPiece( Drawable parent ) throws Exception {
		super( 3 + TOTAL_SHINES );

		this.parent = parent;

		if ( GameMIDlet.isLowMemory() ) {
			border = null;
		} else {
			border = new DrawableImage( BORDER );
			insertDrawable( border );
		}

		image = new DrawableImage( IMAGE );
		insertDrawable( image );

		if ( GameMIDlet.isLowMemory() ) {
			setSize( image.getSize() );
		} else {
			setSize( border.getSize() );
		}

		image.setPosition( ( size.x - image.getWidth() ) >> 1, ( size.y - image.getHeight() ) >> 1 );

		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );

		if ( parent != null ) {
			specialPieceViewport.x = -1000;
			specialPieceViewport.width = 2000;
			setViewport( specialPieceViewport );
		}

		Sprite s;
		//#if SCREEN_SIZE == "SMALL"
//# 			s = new Sprite( ParticleEmitter.getFade() );
		//#elif SCREEN_SIZE == "MEDIUM"
//# 			if ( GameMIDlet.isLowMemory() )
//# 				s = new Sprite( ParticleEmitter.getFade() );
//# 			else
//# 				s = new Sprite( PATH_IMAGES + "ss" );
		//#else
			s = new Sprite( PATH_IMAGES + "ss" );
		//#endif

		for ( byte i = 0; i < TOTAL_SHINES; ++i ) {
			final Sprite temp = new Sprite( s );
			specialPieceShine[ i ] = temp;

			//#if SCREEN_SIZE == "SMALL"
//# 				temp.setSequence( 1 );
			//#else
				if ( GameMIDlet.isLowMemory() )
					temp.setSequence( 1 );
			//#endif

			temp.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			temp.setListener( this, i );
			temp.update( NanoMath.randInt( 180 ) );
			
			insertDrawable( temp );
		}

		// se o pai for nulo, � sinal de que � a pe�a especial exibida na tela de ajuda
		if ( parent == null )
			setState( SPECIAL_PIECE_STATE_VISIBLE );
	}


	public static final void load() throws Exception {
		IMAGE = new DrawableImage( PATH_IMAGES + "shampoo.png" );
		BORDER = new DrawableImage( PATH_IMAGES + "b.png" );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if ( specialPieceState != SPECIAL_PIECE_STATE_NONE )
			setSpecialPieceShineRandomPos( id );
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	public final byte getState() {
		return specialPieceState;
	}


	private final void setSpecialPieceShineRandomPos( int index ) {
		specialPieceShine[ index ].setRefPixelPosition( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) );
		specialPieceShine[ index ].update( NanoMath.randInt( 80 ) );
	}


	public final void setState( int state ) {
		specialPieceState = ( byte ) state;

		switch ( state ) {
			case SPECIAL_PIECE_STATE_NONE:
				setVisible( false );
			break;

			case SPECIAL_PIECE_STATE_APPEARING:
				specialPieceTime = 0;
				setVisible( true );
				setPosition( NanoMath.randInt( parent.getWidth() - getWidth() ), NanoMath.randInt( parent.getHeight() - getHeight() ) );
				specialPieceViewport.height = 0;

				for ( byte i = 0; i < TOTAL_SHINES; ++i )
					setSpecialPieceShineRandomPos( i );
			break;

			case SPECIAL_PIECE_STATE_VISIBLE:
				specialPieceTime = 0;
				setVisible( true );
			break;

			case SPECIAL_PIECE_STATE_VANISHING:
				specialPieceTime = TIME_SPECIAL_PIECE_APPEAR;
				setVisible( true );
			break;
		}
	}


	public final void prepare() {
		switch ( specialPieceState ) {
			case SPECIAL_PIECE_STATE_APPEARING:
			case SPECIAL_PIECE_STATE_VISIBLE:
				setState( SPECIAL_PIECE_STATE_VANISHING );
			break;
		}
	}


	public final void update( int delta ) {
		super.update( delta );

		if ( border != null ) {
			blinkTime += delta;
			if ( blinkTime >= TIME_SPECIAL_PIECE_BLINK ) {
				blinkTime %= TIME_SPECIAL_PIECE_BLINK;
				border.setVisible( !border.isVisible() );
			}
		}

		switch ( specialPieceState ) {
			case SPECIAL_PIECE_STATE_APPEARING:
				specialPieceTime = ( short ) NanoMath.clamp( specialPieceTime + delta, 0, TIME_SPECIAL_PIECE_APPEAR );

				specialPieceViewport.height = getHeight() * specialPieceTime / TIME_SPECIAL_PIECE_APPEAR;
				specialPieceViewport.y = parent.getPosY() + getPosY() + ( ( getHeight() - specialPieceViewport.height ) >> 1 );

				if ( specialPieceTime >= TIME_SPECIAL_PIECE_APPEAR )
					setState( SPECIAL_PIECE_STATE_VISIBLE );
			break;

			case SPECIAL_PIECE_STATE_VISIBLE:
				if ( parent != null ) {
					specialPieceTime += delta;
					if ( specialPieceTime > TIME_SPECIAL_PIECE_VISIBLE )
						setState( SPECIAL_PIECE_STATE_VANISHING );
				}
			break;

			case SPECIAL_PIECE_STATE_VANISHING:
				specialPieceTime = ( short ) NanoMath.clamp( specialPieceTime - delta, 0, TIME_SPECIAL_PIECE_APPEAR );

				specialPieceViewport.height = getHeight() * specialPieceTime / TIME_SPECIAL_PIECE_APPEAR;
				specialPieceViewport.y = parent.getPosY() + getPosY() + ( ( getHeight() - specialPieceViewport.height ) >> 1 );

				if ( specialPieceTime <= 0 )
					setState( SPECIAL_PIECE_STATE_NONE );
			break;
		}
	}

}
