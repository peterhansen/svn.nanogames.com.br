/**
 * Piece.java
 * 
 * Created on Jul 20, 2009, 7:13:55 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Peter
 */
public final class Piece extends Sprite implements Constants {

	public static final byte MOVE_RIGHT				= 0;
	public static final byte MOVE_LEFT				= 1;
	public static final byte MOVE_UP				= 2;
	public static final byte MOVE_DOWN				= 3;
	public static final byte MOVE_UP_LEFT			= 4;
	public static final byte MOVE_UP_RIGHT			= 5;
	public static final byte MOVE_DOWN_LEFT			= 6;
	public static final byte MOVE_DOWN_RIGHT		= 7;
	public static final byte MOVE_BEZIER			= 8;
	public static final byte MOVE_LOOP				= 9;
	public static final byte MOVE_CLOCKWISE			= 10;
	public static final byte MOVE_COUNTER_CLOCKWISE	= 11;
	public static final byte MOVE_BOUNCE			= 12;

	/***/
	public static final byte TOTAL_MOVES = MOVE_BOUNCE + 1;

	/** �ndice do tipo de movimento atual. */
	private byte move;

	/** Tempo em milisegundos da anima��o de sumi�o da pe�a ao morrer. */
	public static final short TIME_VANISH = 290;

	/** Tempo em milisegundos do v�o da pe�a � posi��o de destino. */
	public static final short TIME_DIE = 600;

	public static final byte STATE_NONE				= 0;
	public static final byte STATE_PREPARING_MOVE	= 1;
	public static final byte STATE_MOVING			= 2;
	public static final byte STATE_DYING			= 3;
	public static final byte STATE_VANISHING		= 4;

	private byte state;

	private static final byte SEQUENCE_NORMAL = 0;
	private static final byte SEQUENCE_DYING = 1;
	
	/***/
	private static Sprite SPRITE;

	/***/
	private final MUV speedX = new MUV();

	/***/
	private final MUV speedY = new MUV();

	public static final byte DIRECTION_UP			= 0;
	public static final byte DIRECTION_DOWN			= 1;
	public static final byte DIRECTION_LEFT			= 2;
	public static final byte DIRECTION_RIGHT		= 3;
	public static final byte DIRECTION_LOOP			= 4;
	public static final byte DIRECTION_AFTER_LOOP	= 5;

	private byte direction;

	private byte initialDirection;

	/***/
	private final BezierCurve bezier = new BezierCurve();

	/***/
	private final Point startPosition = new Point();

	/***/
	private final PieceManager manager;

	private final PieceListener pieceListener;

	/***/
	private short stateTime;

	/***/
	private short totalTime;

	private static final Point offset = new Point();
	private static final Point managerPosition = new Point();
	private static Point managerTopLeft;
	private static Point managerBottomRight;

	/***/
	private final Point destination = new Point();

	/***/
	private final Point origin = new Point();

	private Sprite target;


	public Piece( PieceListener listener ) throws Exception {
		super( SPRITE );

		this.pieceListener = listener;

		if ( listener instanceof PieceManager )
			manager = ( PieceManager ) listener;
		else
			manager = null;

		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
	}
	

	public static final void load() throws Exception {
		final Image[] frames = new Image[ PIECE_TYPES << 1 ];
		final short[] frameTimes = new short[ frames.length ];
		final byte[][] sequences = new byte[ 2 ][ frames.length ];

		final String path = PATH_IMAGES + "p_";
		final PaletteMap[] paletteMap = new PaletteMap[] {
			new PaletteMap( 0x9E2800, 0xFFFFFF ),
			new PaletteMap( 0xF43E00, 0xFC9559 ),
			new PaletteMap( 0xFF5E00, 0xFFA36D ),
			new PaletteMap( 0xFF7400, 0xFFAF6D ),
			new PaletteMap( 0xFF9200, 0xFFC16D ),
			new PaletteMap( 0xFFAF00, 0xFFD16D ),
			new PaletteMap( 0xFFEF4C, 0xFFF699 )
		};
		
		for ( byte i = 0; i < PIECE_TYPES; ++i ) {
			final String p = path + i + ".png";
			frames[ i ] = Image.createImage( p );

			//#if SCREEN_SIZE != "BIG"
//# 				// corta as imagens das pe�as com cor especial ao serem atingidas
//# 				if ( GameMIDlet.isLowMemory() ) {
//# 					frames[ PIECE_TYPES + i ] = frames[ i ];
//# 				} else {
//# 					final PaletteChanger paletteChanger = new PaletteChanger( p );
//# 					frames[ PIECE_TYPES + i ] = paletteChanger.createImage( paletteMap );
//# 				}
			//#else
				final PaletteChanger paletteChanger = new PaletteChanger( p );
				frames[ PIECE_TYPES + i ] = paletteChanger.createImage( paletteMap );
			//#endif
			sequences[ 0 ][ i ] = i;
			sequences[ 1 ][ i ] = ( byte ) ( PIECE_TYPES + i );
		}

		//SPRITE = new Sprite( frames, sequences, frameTimes );
        SPRITE = new Sprite ("/case");
	}


	public static final Drawable[] getPieces() throws Exception {
		final Drawable[] pieces = new Drawable[ PIECE_TYPES ];
		for ( byte i = 0; i < PIECE_TYPES; ++i ) {
			pieces[ i ] = new Piece( null );
			( ( Piece ) pieces[ i ] ).setFrame( i );
		}

		return pieces;
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_PREPARING_MOVE:
				setState( STATE_MOVING );
			break;

			case STATE_MOVING:
				switch ( move ) {
					case MOVE_RIGHT:
						move( speedX.updateInt( delta ), 0 );

						if ( getPosX() >= manager.getWidth() )
							reset();
					break;

					case MOVE_LEFT:
						move( speedX.updateInt( delta ), 0 );

						if ( getPosX() <= -getWidth() )
							reset();
					break;

					case MOVE_UP:
						move( 0, speedY.updateInt( delta ) );

						if ( getPosY() <= -getHeight() )
							reset();
					break;

					case MOVE_DOWN:
						move( 0, speedY.updateInt( delta ) );

						if ( getPosY() >= manager.getHeight() )
							reset();
					break;

					case MOVE_UP_LEFT:
						move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

						if ( getPosX() <= -getWidth() || getPosY() <= -getHeight() )
							reset();
					break;

					case MOVE_UP_RIGHT:
						move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

						if ( getPosX() >= manager.getWidth() || getPosY() <= -getHeight() )
							reset();
					break;

					case MOVE_DOWN_LEFT:
						move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

						if ( getPosX() <= -getWidth() || getPosY() >= manager.getHeight() )
							reset();
					break;

					case MOVE_DOWN_RIGHT:
						move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

						if ( getPosX() >= manager.getWidth() || getPosY() >= manager.getHeight() )
							reset();
					break;

					case MOVE_BEZIER:
						stateTime += delta;
						final Point bezierPos = new Point();
						bezier.getPointAtFixed( bezierPos, NanoMath.divInt( stateTime, totalTime ) );
						setRefPixelPosition( bezierPos );

						if ( stateTime >= totalTime ) {
							reset();
						}
					break;

					case MOVE_BOUNCE:
						move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

						if ( speedX.getSpeed() < 0 ) {
							if ( getPosX() <= 0 )
								speedX.setSpeed( -speedX.getSpeed() );
						} else {
							if ( getPosX() >= manager.getWidth() - getWidth() )
								speedX.setSpeed( -speedX.getSpeed() );
						}

						if ( speedY.getSpeed() < 0 ) {
							if ( getPosY() <= 0 )
								speedY.setSpeed( -speedY.getSpeed() );
						} else {
							if ( getPosY() >= manager.getHeight() - getHeight() )
								speedY.setSpeed( -speedY.getSpeed() );
						}
					break;

					case MOVE_LOOP:
						switch ( direction ) {
							case DIRECTION_RIGHT:
								move( speedX.updateInt( delta ), 0 );

								if ( getRefPixelX() >= ( manager.getWidth() >> 2 ) ) {
									direction = DIRECTION_LOOP;
								}
							break;

							case DIRECTION_LEFT:
								move( speedX.updateInt( delta ), 0 );

								if ( getRefPixelX() <= ( ( manager.getWidth() * 3 ) >> 2 ) ) {
									direction = DIRECTION_LOOP;
								}
							break;

							case DIRECTION_UP:
								move( 0, speedY.updateInt( delta ) );

								if ( getRefPixelY() <= ( ( manager.getHeight() * 3 ) >> 2 ) ) {
									direction = DIRECTION_LOOP;
								}
							break;

							case DIRECTION_DOWN:
								move( 0, speedY.updateInt( delta ) );

								if ( getRefPixelY() >= ( manager.getHeight() >> 2 ) ) {
									direction = DIRECTION_LOOP;
								}
							break;

							case DIRECTION_LOOP:
								stateTime += delta;
								final Point refPos = new Point();
								bezier.getPointAtFixed( refPos, NanoMath.divInt( stateTime, totalTime ) );
								setRefPixelPosition( refPos );
								if ( stateTime >= totalTime )
									direction = DIRECTION_AFTER_LOOP;
							break;

							case DIRECTION_AFTER_LOOP:
								move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

								switch ( initialDirection ) {
									case DIRECTION_DOWN:
										if ( getPosY() >= manager.getHeight() )
											reset();
									break;

									case DIRECTION_UP:
										if ( getPosY() <= -PIECE_DEFAULT_HEIGHT )
											reset();
									break;

									case DIRECTION_LEFT:
										if ( getPosX() <= -PIECE_DEFAULT_WIDTH )
											reset();
									break;

									case DIRECTION_RIGHT:
										if ( getPosX() >= manager.getWidth() )
											reset();
									break;
								}
							break;
						}
					break;

					case MOVE_CLOCKWISE:
					case MOVE_COUNTER_CLOCKWISE:
						switch ( direction ) {
							case DIRECTION_DOWN:
								move( 0, speedY.updateInt( delta ) );
								if ( getRefPixelY() >= managerBottomRight.y ) {
									direction = move == MOVE_CLOCKWISE ? DIRECTION_LEFT : DIRECTION_RIGHT;
									setRefPixelPosition( getRefPixelX(), managerBottomRight.y );
								}
							break;

							case DIRECTION_UP:
								move( 0, -speedY.updateInt( delta ) );
								if ( getRefPixelY() <= managerTopLeft.y ) {
									direction = move == MOVE_CLOCKWISE ? DIRECTION_RIGHT : DIRECTION_LEFT;
									setRefPixelPosition( getRefPixelX(), managerTopLeft.y );
								}
							break;

							case DIRECTION_LEFT:
								move( -speedX.updateInt( delta ), 0 );
								if ( getRefPixelX() <= managerTopLeft.x ) {
									direction = move == MOVE_CLOCKWISE ? DIRECTION_UP : DIRECTION_DOWN;
									setRefPixelPosition( managerTopLeft.x, getRefPixelY() );
								}
							break;

							case DIRECTION_RIGHT:
								move( speedX.updateInt( delta ), 0 );
								if ( getRefPixelX() >= managerBottomRight.x ) {
									direction = move == MOVE_CLOCKWISE ? DIRECTION_DOWN : DIRECTION_UP;
									setRefPixelPosition( managerBottomRight.x, getRefPixelY() );
								}
							break;
						}
					break;
				}
			break;

			case STATE_DYING:
				stateTime += delta;

				if ( stateTime >= 0 ) {
					setPosition( origin.x + ( destination.x - origin.x ) * stateTime / TIME_DIE,
								 origin.y + ( destination.y - origin.y ) * stateTime / TIME_DIE );

					if ( stateTime >= TIME_DIE ) {
						setPosition( destination );
						setState( STATE_VANISHING );

						if ( pieceListener != null )
							pieceListener.onDie( this );
					}
				}
			break;

			case STATE_VANISHING:
				stateTime -= delta;

				if ( stateTime > 0 ) {
					viewport.height = size.y * stateTime / TIME_VANISH;
					viewport.y = position.y + managerPosition.y + ( ( size.y - viewport.height ) >> 1 );
				} else {
					setState( STATE_NONE );
					
					if ( pieceListener != null )
						pieceListener.onVanish( this );
				}
			break;
		}
	}

	
	public final void setMove( int move, Point startPos, int time ) {
		setMove( move, startPos, time, -1 );
	}
	

	public final void setMove( int move, Point startPos, int time, int direction ) {
		speedX.setSpeed( 0 );
		speedY.setSpeed( 0 );

		switch ( move ) {
			case MOVE_RIGHT:
				speedX.setSpeed( manager.getWidth() * 1000 / time );
			break;

			case MOVE_LEFT:
				speedX.setSpeed( -manager.getWidth() * 1000 / time );
			break;

			case MOVE_UP:
				speedY.setSpeed( -manager.getHeight() * 1000 / time );
			break;

			case MOVE_DOWN:
				speedY.setSpeed( manager.getHeight() * 1000 / time );
			break;

			case MOVE_BEZIER:
				// define a posi��o inicial para que as pe�as n�o "brotem" no meio da tela
				setRefPixelPosition( startPos );
			break;

			case MOVE_BOUNCE:
				// faz a m�dia das velocidades em x e y para obter o m�dulo
				final int SPEED_MODULE = ( manager.getWidth() + manager.getHeight() ) * 500 / time;
				final Point SPEED = new Point();
				int angle;
				final byte MIN_ANGLE = 21;
				do {
					angle = NanoMath.randInt( 360 );
				} while ( ( angle % 90 > 90 - MIN_ANGLE ) || ( angle % 90 < MIN_ANGLE ) );
				SPEED.setVector( angle, SPEED_MODULE );
				speedX.setSpeed( SPEED.x );
				speedY.setSpeed( SPEED.y );
			break;

			case MOVE_LOOP:
				// o tempo de loop � 1/2 do total
				time >>= 1;
			break;

			case MOVE_CLOCKWISE:
			case MOVE_COUNTER_CLOCKWISE:
				speedX.setSpeed( manager.getWidth() * 1000 / time );
				speedY.setSpeed( manager.getHeight() * 1000 / time );

				this.direction = ( byte ) direction;
			break;
		}

		initialDirection = ( byte ) direction;
		stateTime = 0;
		totalTime = ( short ) time;
		this.move = ( byte ) move;
		startPosition.set( startPos );
		setVisible( true );
		reset();

		setState( STATE_PREPARING_MOVE );
	}


	public final void setStateTime( int stateTime ) {
		this.stateTime = ( short ) stateTime;
	}


	public final void setFrame( int index ) {
		super.setFrame( index );
		setSize( getCurrentFrameImage().getWidth(), getCurrentFrameImage().getHeight() );
		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
	}


	public Point getFrameOffset( int frameIndex ) {
		return new Point();
	}


	public final Sprite getTarget() {
		return target;
	}


	private final void reset() {
		stateTime = 0;

		switch ( move ) {
			case MOVE_LOOP:
				switch ( initialDirection ) {
					case DIRECTION_LEFT:
						speedX.setSpeed( -manager.getWidth() * 1000 / totalTime );
						bezier.setValues( new Point( ( manager.getWidth() * 3 ) >> 2, startPosition.y ),
										 new Point( - manager.getWidth() >> 1, manager.getHeight() ),
										 new Point( ( manager.getWidth() * 3 ) >> 1, manager.getHeight() ),
										 new Point( manager.getWidth() >> 2, startPosition.y ) );
					break;

					case DIRECTION_RIGHT:
						speedX.setSpeed( manager.getWidth() * 1000 / totalTime );
						bezier.setValues( new Point( manager.getWidth() >> 2, startPosition.y ),
										 new Point( ( manager.getWidth() * 3 ) >> 1, 0 ),
										 new Point( - manager.getWidth() >> 1, 0 ),
										 new Point( ( manager.getWidth() * 3 ) >> 2, startPosition.y ) );
					break;

					case DIRECTION_DOWN:
						speedY.setSpeed( manager.getHeight() * 1000 / totalTime );
						bezier.setValues( new Point( startPosition.x, manager.getHeight() >> 2 ),
										 new Point( manager.getWidth(), - manager.getHeight() >> 1 ),
										 new Point( manager.getWidth(), ( manager.getHeight() * 3 ) >> 1 ),
										 new Point( startPosition.x, ( manager.getHeight() * 3 ) >> 2 ) );
					break;

					case DIRECTION_UP:
						speedY.setSpeed( -manager.getHeight() * 1000 / totalTime );
						bezier.setValues( new Point( startPosition.x, ( manager.getHeight() * 3 ) >> 2 ),
										 new Point( 0, ( manager.getHeight() * 3 ) >> 1 ),
										 new Point( 0, - manager.getHeight() >> 1 ),
										 new Point( startPosition.x, manager.getHeight() >> 2 ) );
					break;
				}

				setRefPixelPosition( startPosition );
				direction = initialDirection;
			break;

			case MOVE_BEZIER:
				final Point bezierOrigin = getRefPixelPosition();
				final Point[] points = new Point[ 3 ];

				final int MAX_OFFSET_X = ScreenManager.SCREEN_WIDTH / 5;
				final int MAX_OFFSET_Y = ScreenManager.SCREEN_HEIGHT / 5;

				final Point min = new Point( -MAX_OFFSET_X, -MAX_OFFSET_Y );
				final Point max = new Point( ScreenManager.SCREEN_WIDTH + MAX_OFFSET_X - PIECE_DEFAULT_WIDTH, ScreenManager.SCREEN_HEIGHT + MAX_OFFSET_Y - PIECE_DEFAULT_HEIGHT );

				for ( int i = 0; i < 2; ++i ) {
					points[ i ] = new Point( min.x + NanoMath.randInt( max.x - min.x ),
											 min.y + NanoMath.randInt( max.y - min.y ) );
				}
				points[ 2 ] = new Point( managerTopLeft.x + NanoMath.randInt( managerBottomRight.x - managerTopLeft.x ),
										 managerTopLeft.y + NanoMath.randInt( managerBottomRight.y - managerTopLeft.y ) );

				bezier.setValues( bezierOrigin, points[ 0 ], points[ 1 ], points[ 2 ] );
			break;

			default:
				setRefPixelPosition( startPosition );
			break;
		}
	}


	public final void setState( int state ) {
		setViewport( null );
		setVisible( true );

		switch ( state ) {
			case STATE_NONE:
				setVisible( false );
			break;

			case STATE_DYING:
				stateTime = 0;
				origin.set( getPosition().add( offset ) );
				final byte previousFrame = getFrameSequenceIndex();
				setSequence( SEQUENCE_DYING );
				setFrame( previousFrame );
			break;

			case STATE_VANISHING:
				stateTime = TIME_VANISH;
				setViewport( new Rectangle( position.add( managerPosition ), size ) );
			break;
		}

		this.state = ( byte ) state;
	}


	public final byte getState() {
		return state;
	}


	public final boolean isShot( Rectangle pos ) {
		return isVisible() && ( getState() == STATE_MOVING ) && new Rectangle( getPosition(), getSize() ).intersects( pos );
	}


	public final void setTarget( Sprite target ) {
		this.target = target;
		setSequence( SEQUENCE_NORMAL );

		if ( target != null ) {
			destination.set( target.getPosition().add( offset ) );
			setTransform( target.getTransform() );
			setFrame( target.getFrameSequenceIndex() );
		}
	}


	public static final void setPuzzlePosition( Point puzzlePosition, Point managerPosition ) {
		offset.set( puzzlePosition.sub( managerPosition ) );
		Piece.managerPosition.set( managerPosition );
	}


	public static final void setManagerLimits( Point topLeft, Point bottomRight ) {
		managerTopLeft = topLeft;
		managerBottomRight = bottomRight;
	}


}
