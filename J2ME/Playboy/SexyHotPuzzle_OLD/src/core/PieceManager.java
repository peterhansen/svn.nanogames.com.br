/**
 * PieceManager.java
 * 
 * Created on Jul 20, 2009, 7:18:22 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;
import screens.PlayScreen;

/**
 *
 * @author Peter
 */
public final class PieceManager extends UpdatableGroup implements Constants, SpriteListener, PieceListener {

	public static final byte PATTERN_HORIZONTAL				= 0;
	public static final byte PATTERN_VERTICAL				= 1;
	public static final byte PATTERN_BEZIER					= 2;
	public static final byte PATTERN_BOUNCE					= 3;
	public static final byte PATTERN_LOOP					= 4;
	public static final byte PATTERN_CLOCKWISE				= 5;
	public static final byte PATTERN_COUNTER_CLOCKWISE		= 6;

	public static final byte PATTERN_TOTAL					= 7;

	public static final byte SHOT_HIT_NONE		= 0;
	public static final byte SHOT_HIT_REGULAR	= 1;
	public static final byte SHOT_HIT_SPECIAL	= 2;
	public static final byte SHOT_HIT_COMBO		= 3;

	private static final short TIME_EASY = 15000;
	private static final short TIME_HARD = 5400;

	private static final short TIME_RANDOM_EASY = 1;
	private static final short TIME_RANDOM_HARD = TIME_HARD;

	private short currentTime;

	private short maxRandomTime;

	private final Sprite[] shines;

	private final Point topLeft = new Point();
	private final Point bottomRight = new Point();

	private byte currentShineIndex;

	private static final byte SHINES_MAX = 5;

	private static final byte SHINE_SEQUENCE_IDLE		= 0;
	private static final byte SHINE_SEQUENCE_ANIMATING	= 1;

	/** Quantidade m�xima de pe�as simult�neas na tela. */
	public static final byte PIECES_MAX = 8;
	/** Quantidade minima de pe�as simult�neas na tela (exceto no final de cada n�vel). */
	private static final byte PIECES_MIN = 3;

	private final Piece[] pieces;

	/** Quantidade atual de pe�as ativas. */
	private byte activePieces;

	/** Quantidade m�nima de pe�as a serem atingidas para passar de n�vel. */
	private static final byte PIECES_PER_LEVEL_MIN = 15;

	/** Quantidade m�xima de pe�as a serem atingidas para passar de n�vel. */
	private static final byte PIECES_PER_LEVEL_MAX = 54;

	/** Total de pe�as eliminadas. */
	private byte clearedPieces;

	private byte totalPieces;

	private final PlayScreen playScreen;

	private final Sprite[] pieceOrder = new Sprite[ PIECES_MAX ];

	private byte pieceOrderIndex;

	private final SpecialPiece specialPiece;

	private long lastResetTime;


	public PieceManager( PlayScreen playScreen ) throws Exception {
		super( PIECES_MAX + SHINES_MAX + 2 );
		pieces = new Piece[ PIECES_MAX ];

		this.playScreen = playScreen;

		for ( byte i = 0; i < PIECES_MAX; ++i ) {
			pieces[ i ] = new Piece( this );
			insertDrawable( pieces[ i ] );
		}

		shines = new Sprite[ SHINES_MAX ];
		final Sprite shine = new Sprite( PATH_IMAGES + "shine" );
		for ( byte i = 0; i < SHINES_MAX; ++i ) {
			final Sprite s = new Sprite( shine );
			shines[ i ] = s;

			s.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			s.setVisible( false );
			s.setListener( this, i );
			s.setClipTest( false );
			insertDrawable( s );
		}

		specialPiece = new SpecialPiece( this );
		insertDrawable( specialPiece );
	}


	public final void prepare( byte level ) {
		totalPieces = ( byte ) ( PIECES_PER_LEVEL_MIN + ( PIECES_PER_LEVEL_MAX - PIECES_PER_LEVEL_MIN ) * level / LEVEL_HARD );
		currentTime = ( short ) ( TIME_EASY + ( TIME_HARD - TIME_EASY ) * level / LEVEL_HARD );
		maxRandomTime = ( short ) ( TIME_RANDOM_EASY + ( TIME_RANDOM_HARD - TIME_RANDOM_EASY ) * level / LEVEL_HARD );
		clearedPieces = 0;
		playScreen.getPuzzle().reset( totalPieces );

		specialPiece.prepare();

		//#if DEBUG == "true"
			System.out.println( "PieceManager.prepare: " + totalPieces + ", " + currentTime );
		//#endif
	}


	public final void reset() {
		final byte pattern = ( byte ) NanoMath.randInt( PATTERN_TOTAL );
//		final byte pattern = PATTERN_COUNTER_CLOCKWISE;
		final boolean alternate = NanoMath.randInt( 128 ) < 64;
		final Point startPos = new Point();
		byte move = -1;

		pieceOrderIndex = 0;

		final byte PIECES_TO_INSERT = ( byte ) Math.min( PIECES_MIN + NanoMath.randInt( PIECES_MAX - PIECES_MIN ), totalPieces - clearedPieces );
//		final byte PIECES_TO_INSERT = 8;

		//#if DEBUG == "true"
			System.out.println( "RESET: " + playScreen.getPuzzle().getRemainingCount() + " + " + PIECES_TO_INSERT );
		//#endif

		final Sprite[] types = playScreen.getPuzzle().getPiecesRemaining( PIECES_TO_INSERT );

		final byte[] temp = new byte[ PIECES_TO_INSERT ];
		for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
			pieces[ i ].setTarget( types[ i ] );

			int t = -1;
			do {
				t = NanoMath.randInt( PIECES_TO_INSERT );
			} while ( temp[ t ] < 0 );

			pieceOrder[ i ] = pieces[ t ];
			temp[ t ] = -1;

			//#if DEBUG == "true"
				System.out.println( "ORDER[ " + i + " ]: " + pieceOrder[ i ] );
			//#endif
		}

		switch ( pattern ) {
			case PATTERN_HORIZONTAL:
				boolean right = NanoMath.randInt( 128 ) < 64;
				
				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Piece p = pieces[ i ];
					startPos.y = ( PIECE_DEFAULT_HEIGHT >> 1 ) + size.y * i / PIECES_TO_INSERT;

					if ( alternate )
						right = !right;

					if ( right ) {
						move = Piece.MOVE_RIGHT;
						startPos.x = -PIECE_DEFAULT_WIDTH >> 1;
					} else {
						move = Piece.MOVE_LEFT;
						startPos.x = size.x + ( PIECE_DEFAULT_WIDTH >> 1 );
					}
					p.setMove( move, startPos, currentTime );
				}
			break;

			case PATTERN_VERTICAL:
				boolean down = NanoMath.randInt( 128 ) < 64;

				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Piece p = pieces[ i ];
					startPos.x = ( PIECE_DEFAULT_WIDTH >> 1 ) + size.x * i / PIECES_TO_INSERT;

					if ( alternate )
						down = !down;

					if ( down ) {
						move = Piece.MOVE_DOWN;
						startPos.y = -PIECE_DEFAULT_HEIGHT >> 1;
					} else {
						move = Piece.MOVE_UP;
						startPos.y = size.y + ( PIECE_DEFAULT_HEIGHT >> 1 );
					}
					p.setMove( move, startPos, currentTime );
				}
			break;

			case PATTERN_BEZIER:
				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Piece p = pieces[ i ];
					startPos.set( getOffscreenPos( getSize() ) );
					p.setMove( Piece.MOVE_BEZIER, startPos, currentTime + NanoMath.randInt( maxRandomTime ) );
				}
			break;

			case PATTERN_BOUNCE:
				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Piece p = pieces[ i ];
					p.setMove( Piece.MOVE_BOUNCE, getOffscreenPos( getSize() ), currentTime + NanoMath.randInt( maxRandomTime ) );
				}
			break;

			case PATTERN_LOOP:
				byte direction = ( byte ) NanoMath.randInt( 4 );

				switch ( direction ) {
					case Piece.DIRECTION_UP:
						for ( int i = 0, y = getHeight(); i < PIECES_TO_INSERT; ++i ) {
							final Piece p = pieces[ i ];
							y += PIECE_DEFAULT_HEIGHT;
							startPos.set( bottomRight.x, y );
							p.setMove( Piece.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;
					
					case Piece.DIRECTION_DOWN:
						for ( int i = 0, y = 0; i < PIECES_TO_INSERT; ++i ) {
							final Piece p = pieces[ i ];
							y -= PIECE_DEFAULT_HEIGHT;
							startPos.set( topLeft.x, y );
							p.setMove( Piece.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;

					case Piece.DIRECTION_LEFT:
						for ( int i = 0, x = getWidth(); i < PIECES_TO_INSERT; ++i ) {
							final Piece p = pieces[ i ];
							x += PIECE_DEFAULT_WIDTH;
							startPos.set( x, topLeft.y );
							p.setMove( Piece.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;

					case Piece.DIRECTION_RIGHT:
						for ( int i = 0, x = 0; i < PIECES_TO_INSERT; ++i ) {
							final Piece p = pieces[ i ];
							x -= PIECE_DEFAULT_WIDTH;
							startPos.set( x, bottomRight.y );
							p.setMove( Piece.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;
				}
			break;

			case PATTERN_CLOCKWISE:
			case PATTERN_COUNTER_CLOCKWISE:
				byte orientation = pattern;

				for ( int i = 0, d = NanoMath.randInt( 8 ); i < PIECES_TO_INSERT; ++i, d = ( d + 1 ) % 8 ) {
					final Piece p = pieces[ i ];
					switch ( d ) {
						case 0:
							// top-left (vai pra direita)
							startPos.set( -PIECE_DEFAULT_WIDTH >> 1, topLeft.y );
							move = Piece.DIRECTION_RIGHT;
						break;

						case 1:
							// top-right (vai pra baixo)
							startPos.set( bottomRight.x, -PIECE_DEFAULT_HEIGHT >> 1 );
							move = Piece.DIRECTION_DOWN;
						break;

						case 2:
							// bottom-right (vai pra esquerda)
							startPos.set( getWidth() + ( PIECE_DEFAULT_WIDTH >> 1 ), bottomRight.y );
							move = Piece.DIRECTION_LEFT;
						break;
						
						case 3:
							// bottom-left (vai pra cima)
							startPos.set( topLeft.x, getHeight() + ( PIECE_DEFAULT_HEIGHT >> 1 ) );
							move = Piece.DIRECTION_UP;
						break;

						case 4:
							// top-right (vai pra esquerda)
							startPos.set( getWidth() + ( PIECE_DEFAULT_WIDTH >> 1 ), topLeft.y );
							move = Piece.DIRECTION_LEFT;
						break;

						case 5:
							// bottom-right (vai pra cima)
							startPos.set( bottomRight.x, getHeight() + ( PIECE_DEFAULT_HEIGHT >> 1 ) );
							move = Piece.DIRECTION_UP;
						break;

						case 6:
							// bottom-left (vai pra direita)
							startPos.set( -PIECE_DEFAULT_WIDTH >> 1, bottomRight.y );
							move = Piece.DIRECTION_RIGHT;
						break;

						case 7:
							// top-left (vai pra baixo)
							startPos.set( topLeft.x, -PIECE_DEFAULT_HEIGHT >> 1 );
							move = Piece.DIRECTION_DOWN;
						break;
					}
					
					orientation = d < 4 ? PATTERN_CLOCKWISE : PATTERN_COUNTER_CLOCKWISE;

					//#if DEBUG == "true"
						System.out.println( i + ": " + move + " -> " + startPos );
					//#endif

					p.setMove( orientation == PATTERN_CLOCKWISE ? Piece.MOVE_CLOCKWISE : Piece.MOVE_COUNTER_CLOCKWISE, startPos, currentTime, move );
				}
			break;
		}

		// p�ra as pe�as restantes
		for ( byte i = PIECES_TO_INSERT; i < PIECES_MAX; ++i )
			pieces[ i ].setState( Piece.STATE_NONE );

		setNextPiece( pieceOrder[ 0 ] );

		activePieces = PIECES_TO_INSERT;
		lastResetTime = System.currentTimeMillis();
	}


	public static final Point getOffscreenPos() {
		return getOffscreenPos( null );
	}


	public static final Point getOffscreenPos( Point size ) {
		if ( size == null )
			size = new Point( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final Point pos = new Point();

		if ( NanoMath.randInt( 128 ) < 64 ) {
			// pe�a acima ou abaixo da tela
			pos.x = ( -PIECE_DEFAULT_WIDTH >> 1 ) + NanoMath.randInt( size.x + PIECE_DEFAULT_WIDTH );
			pos.y = NanoMath.randInt( 128 ) < 64 ? ( -PIECE_DEFAULT_HEIGHT >> 1 ) : size.y + ( PIECE_DEFAULT_HEIGHT >> 1 );
		} else {
			// pe�a � esquerda ou direita da tela
			pos.x = NanoMath.randInt( 128 ) < 64 ? ( -PIECE_DEFAULT_WIDTH >> 1 ) : size.x + ( PIECE_DEFAULT_WIDTH >> 1 );
			pos.y = ( -PIECE_DEFAULT_HEIGHT >> 1 ) + NanoMath.randInt( size.y + PIECE_DEFAULT_HEIGHT );
		}

		return pos;
	}


	public final void shot( Rectangle pos ) {
		if ( pieceOrderIndex < pieceOrder.length ) {
			pos.x -= getPosX();
			pos.y -= getPosY();

			if ( specialPiece.getState() != SpecialPiece.SPECIAL_PIECE_STATE_NONE ) {
				final Rectangle r = new Rectangle( specialPiece.getPosition(), specialPiece.getSize() );

				if ( r.intersects( pos ) ) {
					shotAll();
					specialPiece.setState( SpecialPiece.SPECIAL_PIECE_STATE_VANISHING );
					playScreen.onShot( SHOT_HIT_SPECIAL, specialPiece.getRefPixelPosition() );
					return;
				}
			}

			for ( byte i = 0; i < PIECES_MAX; ++i ) {
				final Piece p = pieces[ i ];
				if ( p.isShot( pos ) && p.getFrameSequenceIndex() == pieceOrder[ pieceOrderIndex ].getFrameSequenceIndex() && p.getTransform() == pieceOrder[ pieceOrderIndex ].getTransform() ) {
					pieceShot( p, SHOT_HIT_REGULAR );
					return;
				}
			}
		}
		
		playScreen.onShot( SHOT_HIT_NONE, null );
	}


	private final void pieceShot( Piece p, byte shotType ) {
		p.setState( Piece.STATE_DYING );

		if ( clearedPieces + 1 >= totalPieces ) {
			// fim de n�vel
			setNextPiece( null );
		} else if ( pieceOrderIndex + 1 < pieceOrder.length ) {
			setNextPiece( pieceOrder[ pieceOrderIndex + 1 ] );
		}

		++clearedPieces;
		++pieceOrderIndex;

		playScreen.onShot( shotType, p.getRefPixelPosition() );

		//#if DEBUG == "true"
			System.out.println( "ATINGIU PE�A -> " + p.getState() + " PROGRESSO: " + clearedPieces + " / " + totalPieces );
		//#endif
	}


	private final void shotAll() {
		for ( byte i = 0; i < PIECES_MAX; ++i ) {
			if ( pieces[ i ] != null && pieces[ i ].getState() == Piece.STATE_MOVING )
				pieceShot( pieces[ i ], SHOT_HIT_COMBO );
		}
	}


	public final void setSpecialPieceVisible( boolean v ) {
		specialPiece.setState( v ? SpecialPiece.SPECIAL_PIECE_STATE_APPEARING : SpecialPiece.SPECIAL_PIECE_STATE_NONE );
	}


	public final void onDie( Piece p ) {
		playScreen.getPuzzle().clearPiece( p.getTarget(), true );

		final Sprite shine = shines[ currentShineIndex ];
		shine.setSequence( SHINE_SEQUENCE_ANIMATING );
		shine.setVisible( true );
		shine.setRefPixelPosition( p.getRefPixelPosition() );
		currentShineIndex = ( byte ) ( ( currentShineIndex + 1 ) % SHINES_MAX );
	}


	public final void onVanish( Piece p ) {
		//#if DEBUG == "true"
			// a quantidade de pe�as n�o pode ser menor ou igual a zero antes de decrementar
			if ( activePieces < 0 )
				throw new IllegalStateException( "activePieces < 0" );
		//#endif

		--activePieces;

		if ( clearedPieces >= totalPieces ) {
			// fim de n�vel
			playScreen.setState( PlayScreen.STATE_LEVEL_CLEARED );
		} else if ( activePieces <= 0 ) {
			reset();
		}
	}


	public final boolean canDecreaseComboTime() {
		return activePieces > 0 && ( System.currentTimeMillis() - lastResetTime ) > TIME_COMBO;
	}


	private final void setNextPiece( Sprite piece ) {
		playScreen.getClock().setNextPiece( piece );
		playScreen.setCrosshairTarget( piece );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		setPosition( 0, ScreenManager.SCREEN_HEIGHT - height );

		final int INTERNAL_WIDTH	= width * 76 / 100;
		final int INTERNAL_HEIGHT	= height * 76 / 100;

		topLeft.set( ( width - INTERNAL_WIDTH ) >> 1, ( ( height - INTERNAL_HEIGHT ) >> 1 ) );
		bottomRight.set( topLeft.add( INTERNAL_WIDTH, INTERNAL_HEIGHT ) );

		//#if DEBUG == "true"
			System.out.println( "MANAGER LIMITS: " + topLeft + " -> " + bottomRight );
		//#endif

		Piece.setManagerLimits( topLeft, bottomRight );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if ( id >= 0 ) {
			shines[ id ].setVisible( false );
			shines[ id ].setSequence( SHINE_SEQUENCE_IDLE );
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
		
	}


}
