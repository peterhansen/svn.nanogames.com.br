/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

import br.com.nanogames.components.UpdatableGroup;

/**
 *
 * @author hkajava
 */
public class GameController {
    private UpdatableGroup view;
    private GameModel model;

    public GameController(UpdatableGroup gameScreen) {
        view=gameScreen;
        model=GameModel.getModel();
    }

    public void startPlaying() {
        
    }

    public void pause() {

    }
    
    public void gameLogic(int delta) {
        view.update(delta);
        //handle user input
        model.gameSim();
    }
}
