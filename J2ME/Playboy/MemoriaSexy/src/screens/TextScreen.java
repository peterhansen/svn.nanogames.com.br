/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.MediaPlayer;
import core.BasicScreen;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
class TextScreen extends BasicScreen
{
	private static final byte SELECTED_NONE = 0;
	private static final byte SELECTED_EXIT_BUTTON = 1;

	private int selectionID = SELECTED_NONE;

	private final ScrollRichLabel text;
	private final Drawable ball;

	public TextScreen( int slots, GameMIDlet midlet, int screen, boolean hasTittle, String tittle, String textStr, boolean autoScroll ) throws Exception {
		this( slots, midlet, screen, hasTittle, tittle, textStr, autoScroll, null );
	}



	public TextScreen( int slots, GameMIDlet midlet, int screen, boolean hasTittle, String tittle, String textStr, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( slots + 2, midlet, screen, hasTittle, tittle, true );

		if( !autoScroll ) {
//			ball = new DrawableImage( PATH_IMAGES + "scroll_ball.png" );
			ball = new Pattern( 0xFFC000 );
			final Pattern bar = new Pattern( new DrawableImage( PATH_IMAGES + "scroll_bar_middle.png" ) );
			ball.setSize( bar.getFill().getSize() );
//			bar.setSize( ball.getSize() );
			text = new ScrollRichLabel( new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), textStr + "\n\n\n"/*, specialChars*/ ), bar, ball );
		} else {
			ball = null;
			text = new ScrollRichLabel( new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), textStr + "\n\n\n" ) );
		}
		insertDrawable( text );
		text.setAutoScroll( autoScroll );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public void setSize ( int width, int height ) {
		super.setSize( width, height );

		if( text.getScrollPage() != null ) {
			text.getScrollPage().setSize( ball.getSize() );
			if( text.getScrollFull() != null )
				text.getScrollFull().setSize( ball.getWidth(), height );
		}

		text.setSize( bkgArea.width - ( SAFE_MARGIN << 1 ), bkgArea.height );
		text.setPosition( bkgArea.x + SAFE_MARGIN, bkgArea.y );
	}


	public void exit() {
		midlet.onChoose( null, screenID, 0 );
	}


	public void keyReleased( int key ) {
		text.keyReleased( key );
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				exit();
			break;

			default:
				text.keyPressed( key );
		}
	}


	public void showNotify( boolean deviceEvent ) {
		if( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}




	//#if TOUCH == "true"
		public void onPointerPressed( int x, int y ) {
			text.onPointerPressed( x, y );
		}

		public void onPointerDragged( int x, int y ) {
			text.onPointerDragged( x, y );
		}

		public void onPointerReleased( int x, int y ) {
			text.onPointerReleased( x, y );
		}

	//#endif
	public void drawFrontLayer( Graphics g ) {
//		//#if TOUCH == "true"
//		exitButton.draw( g );
//		//#endif
	}
}