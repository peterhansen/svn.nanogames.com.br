/**
 * CardMenu.java
 *
 * Created on Dec 15, 2010 9:30:09 AM
 *
 */
package screens;

import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.AnimatedBorder;
import core.Constants;

/**
 *
 * @author peter
 */
public final class CardMenu extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if TOUCH == "true"
, PointerListener
//#endif
{

	/**
	 *
	 */
	private static final byte VISIBLE_CARDS = 5;
	/**
	 *
	 */
	private final AnimatedBorder[] cards;
	/**
	 *
	 */
	private short currentIndex;
	/**
	 *
	 */
	private final BezierCurve curve = new BezierCurve();
	/**
	 *
	 */
	private final BezierCurve sizeCurve = new BezierCurve();
	//#if TOUCH == "true"
	/**
	 *
	 */
	private final Point lastPointerPos = new Point();
	//#endif

//	public CardMenu( int screen, int[] entries, int title ) throws Exception {
//		this( screen, entries, title >= 0 ? GameMIDlet.getText( title ) : null );
//	}
//
//
//	public CardMenu( int screen, int[] entries, String titleText ) throws Exception {
//		this( screen, getEntries( entries ), titleText );
//	}
	/**
	 *
	 * @param screen
	 * @param entries
	 * @param titleText
	 * @throws Exception
	 */
	public CardMenu(int screen, String[] entries, String titleText) throws Exception {
		super(entries.length + 10);

		cards = new AnimatedBorder[VISIBLE_CARDS];
		for (byte i = 0; i < VISIBLE_CARDS; ++i) {
			final AnimatedBorder b = new AnimatedBorder();
			cards[i] = b;
//			b.setSize( CARD_WIDTH, CARD_HEIGHT );
			insertDrawable(b);
		}

//		final Drawable[] menuEntries = new Drawable[ entries.length ];
//		for ( byte i = 0; i < menuEntries.length; ++i ) {
//			menuEntries[i] = new MenuLabel( entries[ i ] );
//		}

//		title = new TitleLabel();
//		if ( titleText != null ) {
//			title.setText( titleText, TITLE_WAIT_TIME );
//		}
//		insertDrawable( title );

//		menu = new SubMenu( ( GameMIDlet ) GameMIDlet.getInstance(), screen, menuEntries, MENU_SPACING, 0, entries.length - 1, null );
//		for ( byte i = 0; i < menuEntries.length; ++i ) {
//			( ( MenuLabel ) menuEntries[ i ] ).setParent( menu );
//		}
//		insertDrawable( menu );

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
	}

	/**
	 *
	 * @param width
	 * @param height
	 */
	public final void setSize(int width, int height) {
		super.setSize(width, height);

		curve.origin.set(width / VISIBLE_CARDS, height - 200);
		curve.destiny.set(width - curve.origin.x, curve.origin.y);
		curve.control1.set(width >> 2, height - 100);
		curve.control2.set(width * 3 / 4, height - 100);
	}

	/**
	 *
	 * @param key
	 */
	public final void keyPressed(int key) {
		switch (key) {
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				setCurrentIndex(currentIndex - 1);
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				setCurrentIndex(currentIndex + 1);
				break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_NUM5:
				// TODO confirma opção atual
				break;

			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				// TODO volta do menu
				break;
		}
	}

	/**
	 *
	 * @param key
	 */
	public final void keyReleased(int key) {
	}

	/**
	 *
	 * @param index
	 */
	public final void setCurrentIndex(int index) {
		currentIndex = (short) ((index + cards.length) % cards.length);
//		cards[ currentIndex ].setAnimation( null, new Point( CARD_WIDTH, CARD_HEIGHT ) );

		final Point p = new Point();
		for (int i = 0; i < VISIBLE_CARDS; ++i) {
			final int fp_progress = NanoMath.divInt(i, VISIBLE_CARDS - 1);
			curve.getPointAtFixed(p, fp_progress);
			final int percent = 100 - Math.abs(((VISIBLE_CARDS >> 1) - i) * 100 / VISIBLE_CARDS);
//			cards[ i ].setSize( NanoMath.lerpInt( CARD_WIDTH >> 1, CARD_WIDTH << 1, percent ),
//								NanoMath.lerpInt( CARD_HEIGHT >> 1, CARD_HEIGHT << 1, percent ) );
			cards[i].setRefPixelPosition(p);
			System.out.println(i + " -> " + p);
		}
	}

	/**
	 *
	 * @param deviceEvent
	 */
	public final void hideNotify(boolean deviceEvent) {
	}

	/**
	 *
	 * @param deviceEvent
	 */
	public final void showNotify(boolean deviceEvent) {
	}

	/**
	 *
	 * @param width
	 * @param height
	 */
	public final void sizeChanged(int width, int height) {
		if (width != getWidth() || height != getHeight()) {
			setSize(width, height);
		}
	}

	//#if TOUCH == "true"
	/**
	 *
	 * @param x
	 * @param y
	 */
	public final void onPointerDragged(int x, int y) {
	}

	/**
	 *
	 * @param x
	 * @param y
	 */
	public final void onPointerPressed(int x, int y) {
		lastPointerPos.set(x, y);
	}

	/**
	 *
	 * @param x
	 * @param y
	 */
	public final void onPointerReleased(int x, int y) {
		final int dist = new Point(x, y).distanceTo(lastPointerPos);
		lastPointerPos.set(x, y);
	}
	//#endif
}
