 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.BasicScreen;
import core.SubMenu;
import core.MenuLabel;
import core.StateLabel;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
class OptionsMenu extends BasicScreen implements MenuListener
{
	public static final byte OPTIONS_CHOOSE_SOUND = 0;
	public static final byte OPTIONS_ON_MAIN = 1;
	public static final byte OPTIONS_LOGIN = 2;
	public static final byte OPTIONS_ON_GAME = 3;
	public static final byte OPTIONS_ON_ENDGAME = 4;
	public static final byte OPTIONS_NEW_RECORD = 5;
	public static final byte OPTIONS_GAME_TYPE = 6;

	private final SubMenu menu;
	private final MarqueeLabel labelProf;
	private final MenuLabel[] labels;
	private final int type;

	public OptionsMenu( GameMIDlet midlet, int screen, int titleId, int type, Customer c ) throws Exception {
		super( 2, midlet, screen, true, GameMIDlet.getText( titleId ), false );

		this.type = type;
		final int selection;
		final DrawableImage arrow = new DrawableImage( PATH_IMAGES + "menu_options_arrow.png" );

		switch ( type ) {
			case OPTIONS_CHOOSE_SOUND:
				MediaPlayer.stop();
				selection = 3;
				labelProf = null;
				labels = new MenuLabel[]{ new MenuLabel( TEXT_SOUND, arrow ),
											new MenuLabel( TEXT_VOLUME, arrow ),
											new MenuLabel( TEXT_VIBRATION, arrow ),
											new MenuLabel( TEXT_CONFIRM, arrow ) };
			break;

			case OPTIONS_ON_MAIN:
				MediaPlayer.stop();
				selection = 0;
				labelProf = null;
				labels = new MenuLabel[]{ new MenuLabel( TEXT_SOUND, arrow ),
											new MenuLabel( TEXT_VOLUME, arrow ),
											new MenuLabel( TEXT_VIBRATION, arrow ),
											new MenuLabel( TEXT_CONFIRM, arrow ) };
			break;

			case OPTIONS_LOGIN:
				selection = 0;
				setTitle( GameMIDlet.getText( TEXT_PROFILE_SELECTION ) );
				labelProf = new MarqueeLabel( FONT_TEXT,  ( ( c.getId() == Customer.ID_NONE ) ? ( GameMIDlet.getText( TEXT_NO_PROFILE ) ) : ( GameMIDlet.getText( TEXT_CURRENT_PROFILE ) + c.getNickname().toUpperCase() ) ) );
				labelProf.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				labelProf.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
				insertDrawable( labelProf );
				labels = new MenuLabel[]{ new MenuLabel( TEXT_CONFIRM, arrow ),
											new MenuLabel( TEXT_CHOOSE_ANOTHER, arrow ),
											new MenuLabel( TEXT_HELP, arrow ),
											new MenuLabel( TEXT_BACK, arrow ) };
			break;
			
			case OPTIONS_ON_GAME:
				MediaPlayer.stop();
				selection = 3;
				labelProf = null;
				labels = new MenuLabel[]{ new MenuLabel( TEXT_SOUND, arrow ),
											new MenuLabel( TEXT_VOLUME, arrow ),
											new MenuLabel( TEXT_VIBRATION, arrow ),
											new MenuLabel( TEXT_CONTINUE, arrow ),
											new MenuLabel( TEXT_BACK_MENU, arrow ),
											new MenuLabel( TEXT_EXIT_GAME, arrow ) };
			break;
			
			case OPTIONS_NEW_RECORD:
				selection = 0;
				labelProf = null;
				labels = new MenuLabel[]{ new MenuLabel( TEXT_HIGH_SCORES, arrow ),
											new MenuLabel( TEXT_RESTART, arrow ),
											new MenuLabel( TEXT_BACK_MENU, arrow ),
											new MenuLabel( TEXT_EXIT_GAME, arrow ) };
			break;

			case OPTIONS_ON_ENDGAME:
				selection = 0;
				labelProf = null;
				labels = new MenuLabel[]{ new MenuLabel( TEXT_RESTART, arrow ),
											new MenuLabel( TEXT_BACK_MENU, arrow ),
											new MenuLabel( TEXT_EXIT_GAME, arrow ) };
			break;

			case OPTIONS_GAME_TYPE:
				selection = 0;
				labelProf = null;
				labels = new MenuLabel[]{ new MenuLabel( TEXT_SINGLEPLAYER, arrow ),
											new MenuLabel( TEXT_MULTIPLAYER, arrow ),
											new MenuLabel( TEXT_BACK_MENU, arrow ) };
			break;
			
			default:
				selection = 0;
				labels = null;
				labelProf = null;
		}

		menu = new SubMenu( this, screen, labels );
		menu.setCurrentIndex( selection );
		insertDrawable( menu );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


//	public final void setBack() {
//		menu.setCurrentIndex( options.length - 1 );
//	}


	public final void setSize( int width, int height ) {
		//#if DEBUG == "true"
			System.out.println( "---O---> on OptionsMenu setSize( " + ScreenManager.SCREEN_WIDTH + ", " + ScreenManager.SCREEN_HEIGHT + " );" );
		//#endif
		super.setSize( width, height );

		if( type == OPTIONS_LOGIN ){
			labelProf.setSize( bkgArea.width, labelProf.getHeight() );
			labelProf.setPosition( bkgArea.x, bkgArea.y + SAFE_MARGIN );
			labelProf.setTextOffset( ( bkgArea.width - labelProf.getTextWidth() ) >> 1 );
			menu.setPosition( bkgArea.x , labelProf.getPosY() + labelProf.getHeight() );
			menu.setSize( bkgArea.width, bkgArea.height - ( SAFE_MARGIN + GameMIDlet.GetFont( FONT_TEXT ).getHeight() ) );
		} else {
			menu.setPosition( bkgArea.x, bkgArea.y );
			menu.setSize( bkgArea.width, bkgArea.height );
		}
	}
	
	
	public final void setBack() {
		
	}


	public final void keyPressed( int key ) {
		super.keyPressed( key );
		menu.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		super.keyReleased( key );
		menu.keyReleased( key );
	}


	public void hideNotify( boolean deviceEvent ) {
		if ( deviceEvent )
			MediaPlayer.stop();
	}


	public final void drawFrontLayer( Graphics g ) {
	}


	public void onChoose( Menu menu, int id, int index ) {
		if( labels[ index ].hasState() ) {
			labels[ index ].changeState();
		} else {
			midlet.onChoose( null, screenID, labels[index].id() );
		}
	}


	public void onItemChanged( Menu menu, int id, int index ) {
		for( int i = 0; i < labels.length; i++ ) {
			if( i == index ) {
				labels[i].select();
			} else {
				labels[i].unselect();
			}
		}
	}


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			//#if DEBUG == "true"
				try {
			//#endif
			menu.onPointerDragged( x , y );
			//#if DEBUG == "true"
				}
				catch ( Throwable t ) {
						System.out.println( t.getMessage() );
						t.printStackTrace();
				}
			//#endif
		}


		public void onPointerPressed( int x, int y ) {
			//#if DEBUG == "true"
				try {
			//#endif
			menu.onPointerPressed( x , y );
			//#if DEBUG == "true"
				}
				catch ( Throwable t ) {
						System.out.println( t.getMessage() );
						t.printStackTrace();
				}
			//#endif
		}


		public void onPointerReleased( int x, int y ) {
			//#if DEBUG == "true"
				try {
			//#endif
			menu.onPointerReleased( x , y );
			//#if DEBUG == "true"
				}
				catch ( Throwable t ) {
						System.out.println( t.getMessage() );
						t.printStackTrace();
				}
			//#endif
		}
	//#endif
}
