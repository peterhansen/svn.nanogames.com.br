/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.form.events.Event;
import core.Selectable;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import core.BasicScreen;
import core.Constants;
import core.Button;
import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.NumberLabel;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.SmsSender;
import br.com.nanogames.components.util.SmsSenderListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Caio
 */
public class ScreenRecommend extends BasicScreen implements Constants, KeyListener, ScreenListener, SpriteListener, Updatable, MenuListener, SmsSenderListener
		//#if TOUCH == "true"
			, PointerListener, EventListener
		//#endif
{
	//#if TOUCH == "true"
		public static final byte STATE_HIDDEN		= 0;
		public static final byte STATE_APPEARING	= 1;
		public static final byte STATE_SHOWN		= 2;
		public static final byte STATE_HIDING		= 3;

		/** tempo de animação para o thumb centralizar e a bara sumir e aparecer */
		private static final int ANIM_TOTAL_TIME = 1000;

		private TouchKeyPad keyPad;
		private byte keyPadState = STATE_HIDDEN;
		private int keyPadAccTime = ANIM_TOTAL_TIME;
		private final BezierCurve keyPadBezier = new BezierCurve();

		private byte firstSelection = -1;
	//#endif

	private final RichLabel text;
	private final NumberLabel[] labels;
	private final Button[] buttons;
	private byte selection;
	private byte SELECTION_LENGHT;

	private final Selectable[] itens;

	public ScreenRecommend( GameMIDlet midlet, int screenId ) throws Exception {
		super( 5, midlet, screenId, true, GameMIDlet.getText( TEXT_RECOMMEND_TITLE ), false );

		text = new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), GameMIDlet.getText( TEXT_RECOMMEND_TEXT ).toUpperCase() );
		insertDrawable( text );

		labels = new NumberLabel[ 2 ];

		labels[ 0 ] = new NumberLabel( "DDD", ( byte ) 3 );
		insertDrawable( labels[0] );
		labels[ 1 ] = new NumberLabel( "NÚMERO", ( byte ) 8 );
		insertDrawable( labels[1] );

		buttons = new Button[ 2 ];

		buttons[ 0 ] = new Button( TEXT_OK, this, screenId );
		insertDrawable( buttons[0] );
		buttons[ 1 ] = new Button( TEXT_BACK, midlet, screenId );
		insertDrawable( buttons[1] );

		//#if TOUCH == "true"
			final LineBorder lb = new LineBorder( 0xe8aa26, LineBorder.TYPE_ROUND_RAISED );
			lb.setFillColor( 0xe8aa26 );
			keyPad = new TouchKeyPad( GameMIDlet.GetFont( FONT_TEXT ), new DrawableImage( "/online/clear.png" ),
					new DrawableImage( "/online/shift.png" ), lb,0x680202, 0x8e2525, 0xb93131 );

			keyPad.addEventListener( this );
		//#endif

		SELECTION_LENGHT = ( byte ) ( labels.length + buttons.length );

		itens = new Selectable[ SELECTION_LENGHT ];
		itens[ 0 ] = labels[ 0 ];
		itens[ 1 ] = labels[ 1 ];
		itens[ 2 ] = buttons[ 0 ] ;
		itens[ 3 ] = buttons[ 1 ] ;

		select( selection );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	private final boolean sendSMS() {
		if ( labels[ 1 ].changed && labels[ 1 ].length() == 8 ) {
			final SmsSender sender = new SmsSender();
			sender.send( ( labels[ 0 ].changed && labels[ 0 ].length() > 1 ) ? labels[ 0 ].getText() + labels[ 1 ].getText() : labels[ 1 ].getText(), GameMIDlet.getText( TEXT_RECOMMEND_SMS ) + GameMIDlet.getRecommendURL(), this );
			GameMIDlet.setScreen( SCREEN_RECOMMEND_SENT );
			return true;
		}
		return false;
	}

	//#if TOUCH == "true"
	private void hideKeyPad() {
		if( ( keyPadState != STATE_HIDING ) && ( keyPadState != STATE_HIDDEN ) ) {
			keyPadState = STATE_HIDING;
			keyPadAccTime = 0;

			keyPadBezier.origin.set( keyPad.getPosX(), keyPad.getPosY() );
			keyPadBezier.destiny.set( keyPad.getPosX() , getHeight() );

			keyPadBezier.control1.set( keyPadBezier.destiny );
			keyPadBezier.control2.set( keyPadBezier.destiny );
		}
	}


	private void showKeyPad() {
		if( ( ScreenManager.getInstance().hasPointerEvents() ) && ( keyPadState != STATE_APPEARING ) && ( keyPadState != STATE_SHOWN ) ) {
			keyPadState = STATE_APPEARING;
			keyPadAccTime = 0;

			keyPadBezier.origin.set( keyPad.getPosX(), keyPad.getPosY() );
			keyPadBezier.destiny.set( keyPad.getPosX() , getHeight() - keyPad.getHeight() );

			keyPadBezier.control1.set( keyPadBezier.destiny );
			keyPadBezier.control2.set( keyPadBezier.destiny );
		}
	}


	private final void moveKeyPad( int dx, int dy ) {
		if ( ( dx != 0 ) || ( dy != 0 ) ) {
			keyPad.move( dx, dy );
		}
	}


	public void update( int delta ) {
		if( keyPadAccTime < ANIM_TOTAL_TIME ) {
			keyPadAccTime += delta;
			if ( keyPadAccTime >= ANIM_TOTAL_TIME ) {
				keyPadAccTime = ANIM_TOTAL_TIME;
			}
			final Point p = new Point();
			keyPadBezier.getPointAtFixed( p, NanoMath.divInt( keyPadAccTime, ANIM_TOTAL_TIME ) );
			moveKeyPad( p.x - keyPad.getPosX(), p.y - keyPad.getPosY() );
		}
	}
	//#endif


	public void setSize ( int width, int height ) {
		super.setSize( width, height );

		text.setSize( width - ( SAFE_MARGIN + SAFE_MARGIN ), height - ( titleGroup.getHeight() + ( SAFE_MARGIN << 1 ) ) );
		text.setPosition( SAFE_MARGIN, titleGroup.getPosY() + titleGroup.getHeight() + SAFE_MARGIN );
		text.formatText( false );

		//#if TOUCH == "true"
			keyPad.setSize( width, KEYPAD_MINIMUM_SIZE );
			final Point p = new Point();
			switch ( keyPadState ) {
				case STATE_SHOWN:
					keyPad.setPosition( 0, height - keyPad.getHeight() );
				break;

				case STATE_HIDDEN:
					keyPad.setPosition( 0, height );
				break;

				case STATE_HIDING:
					keyPadBezier.origin.set( 0, height - keyPad.getHeight() );
					keyPadBezier.destiny.set( 0, height );
					keyPadBezier.control1.set( keyPadBezier.destiny );
					keyPadBezier.control2.set( keyPadBezier.destiny );

					keyPadBezier.getPointAtFixed( p, NanoMath.divInt( keyPadAccTime, ANIM_TOTAL_TIME ) );
					keyPad.setPosition( p.x, p.y );
				break;

				case STATE_APPEARING:
					keyPadBezier.origin.set( 0, height );
					keyPadBezier.destiny.set( 0, height - keyPad.getHeight() );
					keyPadBezier.control1.set( keyPadBezier.destiny );
					keyPadBezier.control2.set( keyPadBezier.destiny );

					keyPadBezier.getPointAtFixed( p, NanoMath.divInt( keyPadAccTime, ANIM_TOTAL_TIME ) );
					keyPad.setPosition( p.x, p.y );
				break;
			}
		//#endif

		int TOTAL = ( labels.length * 1 ) + 1;
		for ( byte i = 0; i < labels.length; i++ ) {
			TOTAL += labels[ i ].MAX_NUMBERS;
		}
		int acc = 1;
		for ( byte i = 0; i < labels.length; i++ ) {
			labels[ i ].setSize( labels[ i ].MAX_NUMBERS * width / TOTAL , 0 );
			labels[ i ].setPosition( acc * width / TOTAL, text.getPosY() + text.getTextTotalHeight() );
			acc += 1 + labels[ i ].MAX_NUMBERS;
		}
		TOTAL = SAFE_MARGIN * ( buttons.length - 1 );
		for ( byte i = 0; i < buttons.length; i++ ) {
			TOTAL += buttons[ i ].getWidth();
		}
		acc = ( getWidth() - TOTAL ) >> 1 ;
		for ( byte i = 0; i < buttons.length; i++ ) {
			buttons[ i ].setPosition( acc, labels[ 0 ].getPosY() +  labels[ 0 ].getHeight() + 3 + SAFE_MARGIN ); // 3 = size of numberLabel border
			acc += buttons[ i ].getWidth() + SAFE_MARGIN;
		}
	}


//	public void exit() {
//		midlet.onChoose( null, screenID, 0 );
//	}


	public void drawFrontLayer( Graphics g ) {
		//#if TOUCH == "true"
			keyPad.draw( g );
		//#endif
	}


	private final void select( byte i ) {
		itens[ selection ].unselect();
		selection = i;
		if( selection >= SELECTION_LENGHT ) {
			selection = ( byte )(SELECTION_LENGHT - 1);
		} else if( selection < 0 ) {
			selection = 0;
		}

		//#if TOUCH == "true"

			if ( itens[ selection ].textBox() == null ) {
				hideKeyPad();
			} else {
				showKeyPad();
				keyPad.setTextBox( itens[ selection ].textBox() );
			}

//			if ( itens[ selection ].InputMode() == -1 ) {
//				hideKeyPad();
//			} else {
//				showKeyPad();
//				keyPad.setDefaultInputMode( itens[ selection ].InputMode() );
//			}
		//#endif

		itens[ selection ].select();
	}


	public void selectNext() {
		select( ( byte )( selection + 1 ) );
	}


	public void selectPrevious() {
		select( ( byte )( selection - 1 ) );
	}


	public void keyPressed( int key ) {
		super.keyReleased( key );
		switch ( key ) {
			case ScreenManager.UP:
				select( ( byte ) ( selection - 2 ) );
			break;
			case ScreenManager.LEFT:
				select( ( byte ) ( selection - 1 ) );
			break;
			case ScreenManager.RIGHT:
				select( ( byte ) ( selection + 1 ) );
			break;
			case ScreenManager.DOWN:
				select( ( byte ) ( selection + 2 ) );
			break;
			default:
				itens[ selection ].keyPressed( key );
		}
	}


	public void onSequenceEnded( int id, int sequence ) {
	}


	public void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	//#if TOUCH == "true"

		public void onPointerDragged( int x, int y ) {
			if ( keyPad.contains( x, y ) ) {
				keyPad.onPointerDragged( x, y );
			}
		}


		public void onPointerPressed( int x, int y ) {
			if ( keyPad.contains( x, y ) ) {
				keyPad.onPointerPressed( x, y );
			} else {
				for( byte i = 0; i < itens.length; i++ ) {
					if( itens[ i ].contains( x, y ) ) {
						select( i );
						firstSelection = i;
						return;
					}
				}
			}
		}


		public void onPointerReleased( int x, int y ) {
			if ( keyPad.contains( x, y ) ) {
				keyPad.onPointerReleased( x, y );
			} else {
				if( firstSelection >= 0 && firstSelection < itens.length ) {
					if( itens[ firstSelection ].contains( x, y ) ) {
						itens[ firstSelection ].keyPressed( ScreenManager.FIRE );
						return;
					} else {
						firstSelection = -1;
					}
				}
			}
		}


		public final void eventPerformed( Event evt ) {
			hideKeyPad();
		}
	//#endif


	public void onChoose( Menu menu, int id, int index ) {
		switch(index) {
			// Evento já é repassado direto para o GameMIDlet
//			case TEXT_BACK:
//			case TEXT_EXIT:
//				midlet.onChoose( menu, id, index );
//			break;
			case TEXT_OK:
				sendSMS();
//				if ( sendSMS() ) {
//					midlet.onChoose( menu, id, index );
//				}
			break;
		}
	}


	public void onItemChanged( Menu menu, int id, int index ) {
	}


	public void onSMSSent( int id, boolean smsSent ) {
		midlet.onChoose( null, screenID, TEXT_OK );
	}

	public void onSMSLog( int id, String s ){
		//#if DEBUG == "true"
			System.out.println( "sms: " + s );
		//#endif
	}

}