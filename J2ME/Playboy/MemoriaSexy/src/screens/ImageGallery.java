package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.SexyButton;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
class ImageGallery extends UpdatableGroup implements Constants, ScreenListener, KeyListener
//#if TOUCH == "true"
	, PointerListener
//#endif
{
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;

	private static final byte SELECTED_NONE = 0;
	private static final byte SELECTED_ARROW_LEFT = 1;
	private static final byte SELECTED_ARROW_RIGHT = 2;
	private static final byte SELECTED_IMAGE = 3;
	private static final byte SELECTED_THUMB = 4;
	private static final byte SELECTED_EXIT_BUTTON = 5;

	/** tempo de animação para o thumb centralizar e a bara sumir e aparecer */
	private static final int ANIM_TOTAL_TIME = 1000;
	private static final int MAX_ACCELERATION = NanoMath.toFixed( 10 );
	/** velocidade de arrasto da imagem */
	private static final int DRAG_VELOCITY = NanoMath.divInt( 1, 6 );
	/** Componentes decomposta da velocidade de arrasto da imagem */
	private static final int DRAG_VELOCITY_COS = NanoMath.mulFixed( DRAG_VELOCITY, NanoMath.cosInt( 45 ) );
	/** velocidade máxima de arrasto da imagem */
	private static final int MAX_DRAG_VELOCITY = NanoMath.toFixed( 200 );

	private final Label title;
	private final Pattern titlePattern;
	private DrawableImage image;
	private final DrawableImage[] thumbs;

	/** índice atualmente selecionado no menu */
	private int currentIndex = 0;
	/** indica se o menu é circular */
	private boolean circular;
	private boolean canSelectLocked;

	private final Pattern thumbsPattern;
	private final Sprite arrowRight;
	private final Sprite arrowLeft;

	private final DrawableGroup thumbsGroup;
	private final DrawableGroup barGroup;
	private final DrawableGroup imageGroup;
	private final DrawableGroup tittleGroup;
	private final DrawableGroup textGroup;

	private int selectionID;
	private int mediumThumbWidth;

	private final SexyButton exitButton;

	private final BezierCurve thumbBezier = new BezierCurve();
	private final BezierCurve barBezier = new BezierCurve();

	private int thumbAccTime = ANIM_TOTAL_TIME;
	private int barAccTime = ANIM_TOTAL_TIME;

	private byte barState = STATE_SHOWN;

	private final Point imageOffset = new Point();
	private final Point imageMaxOffset = new Point();

	private final Point cursorRealAcceleration = new Point();
	private final Point cursorAcceleration = new Point();
	private final Point cursorVelocity = new Point();


	private final Point pointerFirstPos = new Point();

	private final Point pointerLastPos = new Point();

	private final RichLabel infoText;

	private GameMIDlet midlet;
	private int screenID;

	private final DrawableImage logo;

	private boolean hasBkg = false;

	// private final MUV scoreSpeed = new MUV(); // TODO rever deslocamento da imagem
//	/** velocidade de movimentação horizontal atual da mira */
//	private final MUV cursorSpeedX = new MUV();
//	/** velocidade de movimentação vertical atual da mira */
//	private final MUV cursorSpeedY = new MUV();

	public ImageGallery( GameMIDlet midlet, int screen ) throws Exception {
		super( 5 );

		screenID = screen;
		this.midlet = midlet;

		imageGroup = new DrawableGroup( 2 );
		insertDrawable( imageGroup );

//		image = new DrawableImage( PATH_GIRLS + currentIndex + ".png" );
//		image.defineReferencePixel( ANCHOR_CENTER );
//		imageGroup.insertDrawable( image );

		tittleGroup = new DrawableGroup( 2 );
		insertDrawable( tittleGroup );

		final DrawableImage img = new DrawableImage( PATH_IMAGES + "menu_pattern.png" );
		final DrawableImage img2 = new DrawableImage( img );
		img2.setTransform( TRANS_MIRROR_V );
		img.setPosition( 0, img.getHeight() );
		final DrawableGroup bkg= new DrawableGroup( 2 );
		bkg.setSize( img.getWidth(), img.getHeight() << 1 );
		bkg.insertDrawable( img );
		bkg.insertDrawable( img2 );

		titlePattern = new Pattern( bkg );
		titlePattern.setSize( bkg.getWidth(), bkg.getHeight() );
		titlePattern.setPosition( 0 , - 1 );
		tittleGroup.insertDrawable( titlePattern );

		title = new Label( FONT_TITLE , TEXT_PHOTO_GALLERY );
		title.defineReferencePixel( ANCHOR_CENTER );
		tittleGroup.insertDrawable( title );

		logo = new DrawableImage( PATH_GIRLS + "logo.png" );
		insertDrawable( logo );

		infoText = new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), GameMIDlet.getText( TEXT_HELP_IMAGEGALLERY_TEXT ).toUpperCase() );
		textGroup = new DrawableGroup( 1 );
		textGroup.insertDrawable( infoText );
		imageGroup.insertDrawable( textGroup );

		barGroup = new DrawableGroup( 6 );
		insertDrawable( barGroup );

		thumbsPattern = new Pattern( bkg );
		barGroup.insertDrawable( thumbsPattern );

		thumbs = new DrawableImage[ TOTAL_GIRLS_PHOTOS ];

		mediumThumbWidth = 0;
		if( TOTAL_GIRLS_PHOTOS > 0 ) {
			thumbsGroup = new DrawableGroup( TOTAL_GIRLS_PHOTOS );
			for(byte i = 0; i < TOTAL_GIRLS_PHOTOS; i++) {

				if ( ( i + 1 ) < ( GameMIDlet.getMaxLevelReached() ) ) {
					thumbs[i] = new DrawableImage( PATH_GIRLS + "thumb" + i + ".png" );
				} else {
					thumbs[i] = new DrawableImage( PATH_GIRLS + "locked.png" );
				}

				thumbs[i].defineReferencePixel( ANCHOR_CENTER );
				thumbsGroup.insertDrawable( thumbs[i] );
				mediumThumbWidth += thumbs[i].getWidth();
			}
			barGroup.insertDrawable( thumbsGroup );
			mediumThumbWidth = ( mediumThumbWidth / TOTAL_GIRLS_PHOTOS ) + 6; // TODO passar variavel magica para constante
		}

		exitButton = new SexyButton( GameMIDlet.getText( TEXT_BACK ), true, FONT_TEXT );
		insertDrawable( exitButton );

		arrowRight = new Sprite( PATH_IMAGES + "menu_arrow" );
		arrowRight.mirror( TRANS_MIRROR_H );
		arrowRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		barGroup.insertDrawable( arrowRight );

		arrowLeft = new Sprite( arrowRight );
		arrowLeft.defineReferencePixel( ANCHOR_VCENTER );
		barGroup.insertDrawable( arrowLeft );

		setIndex( (byte)0 );

		updateImageToShow();
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		if( barState == STATE_HIDING || barState == STATE_HIDDEN ) {
			barGroup.setSize( width, thumbs[ 0 ].getHeight() + ( SAFE_MARGIN << 1 ) );
			barGroup.setPosition( 0, height );
			barState = STATE_HIDDEN;
		} else {
			barGroup.setSize( width, thumbs[ 0 ].getHeight() + ( SAFE_MARGIN << 1 ) );
			barGroup.setPosition( 0, height - ( barGroup.getHeight() ) );
			barState = STATE_SHOWN;
		}
		barAccTime = ANIM_TOTAL_TIME;

		thumbsPattern.setSize( barGroup.getSize() );
		arrowLeft.setRefPixelPosition( SAFE_MARGIN, barGroup.getHeight() >> 1 );
		arrowRight.setRefPixelPosition( barGroup.getWidth() - SAFE_MARGIN, barGroup.getHeight() >> 1 );

		updateImage();
		resizeThumbs();
	}


	private void updateImage() {

		if( getHeight() > getWidth() ) {
			tittleGroup.setSize( getWidth(), ( ( title.getHeight() + ( SAFE_MARGIN << 1 ) ) * ( getHeight() - barGroup.getPosY() ) ) / barGroup.getHeight() );
			titlePattern.setSize( tittleGroup.getWidth(), titlePattern.getHeight() );
			titlePattern.setPosition( 0, ( tittleGroup.getHeight() - titlePattern.getHeight() ) >> 1 );
			title.setRefPixelPosition( ( tittleGroup.getWidth() >> 1 ) , ( tittleGroup.getHeight() >> 1 ) - ( NanoMath.max( ( title.getHeight() - tittleGroup.getHeight() ) >> 1, 0 ) ) );
		} else {
			tittleGroup.setSize( 0, 0 );
		}

		logo.setPosition( SAFE_MARGIN ,
				barGroup.getPosY() - ( ( ( getHeight() - barGroup.getPosY() ) * ( ( 6 * logo.getHeight() ) / 7 ) ) / barGroup.getHeight() ) );

		exitButton.setPosition( getWidth() - ( exitButton.getWidth() + SAFE_MARGIN ) , logo.getPosY() + ( ( logo.getHeight() - exitButton.getHeight() ) >> 1 ) );

		imageGroup.setPosition( 0, tittleGroup.getPosY() + tittleGroup.getHeight() );
		imageGroup.setSize( getWidth() , barGroup.getPosY() - ( tittleGroup.getPosY() + tittleGroup.getHeight() ) );

		textGroup.setSize( imageGroup.getWidth(), imageGroup.getHeight() );

		infoText.setSize( textGroup.getWidth() - ( SAFE_MARGIN << 1 ) , 0 );
		infoText.formatText( false );
		infoText.setSize( infoText.getWidth(), infoText.getTextTotalHeight() );
		infoText.setPosition( ( ( textGroup.getWidth() - infoText.getWidth() ) >> 1 ), ( ( textGroup.getHeight() - infoText.getHeight() ) >> 1 )  );

		if ( image != null ) {
			imageMaxOffset.set( NanoMath.toFixed( NanoMath.max( ( image.getWidth() - imageGroup.getWidth() ) >> 1 , 0 ) ),
					NanoMath.toFixed( NanoMath.max( ( image.getHeight() - imageGroup.getHeight() ) >> 1, 0) ) );

			image.defineReferencePixel( ANCHOR_CENTER );
			updateImageCenter();

			GameMIDlet.setBkgBlocker( new Rectangle( imageGroup.getPosX() + NanoMath.max( image.getPosX(), 0 ),
										imageGroup.getPosY() + NanoMath.max( image.getPosY(), 0 ),
										NanoMath.min( imageGroup.getWidth(), image.getWidth() ) ,
										NanoMath.min( imageGroup.getHeight(), image.getHeight() ) ) );
		} else {
			GameMIDlet.setBkgBlocker( new Rectangle( barGroup.getPosX(), barGroup.getPosY(),
										barGroup.getWidth(), barGroup.getHeight() ) );
		}
	}


	public void resizeThumbs() {
		//#if DEBUG == "true"
			System.out.println( "" );
		//#endif
		thumbsGroup.setPosition( ( arrowLeft.getPosX() + arrowLeft.getWidth() + SAFE_MARGIN ) , SAFE_MARGIN );
		thumbsGroup.setSize( ( arrowRight.getPosX() - ( thumbsGroup.getPosX() + SAFE_MARGIN ) ) , thumbs[0].getHeight() );

		if( thumbAccTime < ANIM_TOTAL_TIME ) {
			thumbBezier.origin.set( thumbs[0].getRefPixelX(), thumbs[0].getRefPixelY() );
			thumbBezier.destiny.set( ( ( - currentIndex ) * mediumThumbWidth ) + ( thumbsGroup.getWidth() >> 1 ) , ( thumbsGroup.getHeight() >> 1 ) );

			thumbBezier.control1.set( thumbBezier.destiny );
			thumbBezier.control2.set( thumbBezier.destiny );
		} else {
			for( byte i = 0; i < thumbs.length; i++ ) {
				thumbs[ i ].setRefPixelPosition( ( ( i - currentIndex ) * mediumThumbWidth ) + ( thumbsGroup.getWidth() >> 1 ) , ( thumbsGroup.getHeight() >> 1 ) );
			}
		}
	}


	public void update( int delta ) {
		if( thumbAccTime < ANIM_TOTAL_TIME ) {
			thumbAccTime += delta;
			final Point p = new Point();
			thumbBezier.getPointAtFixed( p, NanoMath.divInt( thumbAccTime, ANIM_TOTAL_TIME ) );
			moveThumbs( p.x - thumbs[0].getRefPixelX(), p.y - thumbs[0].getRefPixelY() );

			if ( thumbAccTime > ANIM_TOTAL_TIME ) {
				thumbAccTime = ANIM_TOTAL_TIME;
				resizeThumbs();
			}
		}

		if( barAccTime < ANIM_TOTAL_TIME ) {
			barAccTime += delta;
			final Point p = new Point();
			barBezier.getPointAtFixed( p, NanoMath.divInt( barAccTime, ANIM_TOTAL_TIME ) );
			moveBar( p.x - barGroup.getPosX(), p.y - barGroup.getPosY() );
			updateImage();

			if ( barAccTime > ANIM_TOTAL_TIME ) {
				if( barState == STATE_APPEARING )
					barState = STATE_SHOWN;
				else if( barState == STATE_HIDING )
					barState = STATE_HIDDEN;
			}
		}

		boolean changed = false;
		if( cursorAcceleration.x != 0 ) {
			cursorRealAcceleration.x += cursorAcceleration.x * delta;
		}
		if( cursorRealAcceleration.x != 0 ) {
			if ( cursorRealAcceleration.x > MAX_ACCELERATION )
				cursorRealAcceleration.x = MAX_ACCELERATION;
			else if( cursorRealAcceleration.x < -MAX_ACCELERATION )
				cursorRealAcceleration.x = - MAX_ACCELERATION;
			cursorVelocity.x += cursorRealAcceleration.x * delta;
		} else {
			if ( cursorVelocity.x != 0 ) {
				cursorVelocity.x *= 9;
				cursorVelocity.x /= 10;
				changed = true;
			}
		}

		if( cursorAcceleration.y != 0 ) {
			cursorRealAcceleration.y += cursorAcceleration.y * delta;
		}
		if( cursorRealAcceleration.y != 0 ) {
			if ( cursorRealAcceleration.y > MAX_ACCELERATION )
				cursorRealAcceleration.y = MAX_ACCELERATION;
			else if( cursorRealAcceleration.y < -MAX_ACCELERATION )
				cursorRealAcceleration.y = - MAX_ACCELERATION;
			cursorVelocity.y += cursorRealAcceleration.y * delta;
		} else {
			if ( cursorVelocity.y != 0 ) {
				cursorVelocity.y *= 9;
				cursorVelocity.y /= 10;
				changed = true;
			}
		}

		if ( ( ( NanoMath.toInt( cursorVelocity.x ) * NanoMath.toInt( cursorVelocity.x ) ) +
				( NanoMath.toInt( cursorVelocity.y ) * NanoMath.toInt(cursorVelocity.y ) ) ) >
				 ( NanoMath.toInt( MAX_DRAG_VELOCITY ) * NanoMath.toInt( MAX_DRAG_VELOCITY ) ) ) {

			cursorVelocity.mulEquals( NanoMath.toInt( MAX_DRAG_VELOCITY ) * NanoMath.toInt( MAX_DRAG_VELOCITY ) );
			cursorVelocity.divEquals( ( NanoMath.toInt( cursorVelocity.x ) * NanoMath.toInt( cursorVelocity.x ) ) +
										( NanoMath.toInt( cursorVelocity.y ) * NanoMath.toInt( cursorVelocity.y ) ) );
		}

		imageOffset.x -= ( ( cursorVelocity.x * delta ) / 1000 );
		changed = true;

		imageOffset.y -= ( ( cursorVelocity.y * delta ) / 1000 );
		changed = true;

		if( changed ) updateImageCenter();

	}


	private final void moveThumbs( int dx, int dy ) {
		if ( ( dx != 0 ) || ( dy != 0 ) ) {
			for ( int i = 0; i < thumbs.length; ++i ) {
				thumbs[i].move( dx, dy );
			}
		}
	}


	private final void moveBar( int dx, int dy ) {
		if ( ( dx != 0 ) || ( dy != 0 ) ) {
			barGroup.move( dx, dy );
		}
	}


	private void changeBarState() {
		switch ( barState ) {
			case STATE_HIDING:
			case STATE_HIDDEN:
				showBar();
			break;

			case STATE_APPEARING:
			case STATE_SHOWN:
				hideBar();
			break;
		}
	}


	private void hideBar() {
		barState = STATE_HIDING;
		barAccTime = 0;

		barBezier.origin.set( barGroup.getPosX(), barGroup.getPosY() );
		barBezier.destiny.set( barGroup.getPosX() , getHeight() );

		barBezier.control1.set( barBezier.destiny );
		barBezier.control2.set( barBezier.destiny );
	}


	private void showBar() {
		barState = STATE_APPEARING;
		barAccTime = 0;

		barBezier.origin.set( barGroup.getPosX(), barGroup.getPosY() );
		barBezier.destiny.set( barGroup.getPosX() , getHeight() - barGroup.getHeight() );

		barBezier.control1.set( barBezier.destiny );
		barBezier.control2.set( barBezier.destiny );
	}


	private void updateImageCenter() {
		if ( imageOffset.x > imageMaxOffset.x ) {
			imageOffset.x = imageMaxOffset.x;
			cursorVelocity.x = 0;
			cursorAcceleration.x = 0;
			cursorRealAcceleration.x = 0;
		} else if ( imageOffset.x < -imageMaxOffset.x ) {
			imageOffset.x = -imageMaxOffset.x;
			cursorVelocity.x = 0;
			cursorAcceleration.x = 0;
			cursorRealAcceleration.x = 0;
		}

		if ( imageOffset.y > imageMaxOffset.y ) {
			imageOffset.y = imageMaxOffset.y;
			cursorVelocity.y = 0;
			cursorAcceleration.y = 0;
			cursorRealAcceleration.y = 0;
		} else if ( imageOffset.y < -imageMaxOffset.y ) {
			imageOffset.y = -imageMaxOffset.y;
			cursorVelocity.y = 0;
			cursorAcceleration.y = 0;
			cursorRealAcceleration.y = 0;
		}

		if( image!= null ) image.setRefPixelPosition( ( imageGroup.getWidth() >> 1 ) + NanoMath.toInt( imageOffset.x ) , ( imageGroup.getHeight() >> 1 ) + NanoMath.toInt( imageOffset.y ) );
	}


	public void draw( Graphics g ) {
		super.draw(g);

		/** Desenho do contorno do thumb selecionado*/
		if( ( ( ( thumbs[ currentIndex ].getPosX() + thumbs[ currentIndex ].getWidth() ) > 0 ) && ( thumbs[ currentIndex ].getPosX() < thumbsGroup.getWidth() ) )
				&&
				( ( ( thumbs[ currentIndex ].getPosY() + thumbs[ currentIndex ].getHeight() ) > 0 )	&& ( thumbs[ currentIndex ].getPosY() < thumbsGroup.getHeight() ) ) ) {

			final int minX = translate.x + barGroup.getPosX() + thumbsGroup.getPosX();
			final int maxX = minX + thumbsGroup.getWidth();
			final int minY = translate.y + barGroup.getPosY() + thumbsGroup.getPosY();
			final int maxY = minY + thumbsGroup.getHeight();

			int x = thumbs[ currentIndex ].getPosX() + translate.x + barGroup.getPosX() + thumbsGroup.getPosX() - 1;
			int y = thumbs[ currentIndex ].getPosY() + translate.y + barGroup.getPosY() + thumbsGroup.getPosY() - 1;
			int w = thumbs[ currentIndex ].getWidth() + 1;
			int h = thumbs[ currentIndex ].getHeight() + 1;

			int diff = ( x + w ) - maxX;
			if( diff > 0 ) { // Verifiacação se largura atual não cabe na area a ser desenhado
				w -= diff;  // faz com que a largura caiba
			}

			diff = ( y + h ) - maxY;
			if( diff > 0 ) { // Verifiacação se altura atual não cabe na area a ser desenhado
				h -= diff;  // faz com que a altura caiba
			}

			diff = x - minX;
			if( diff < 0 ) {
				x -= diff;
				w += diff;
			}

			diff = y - minY;
			if( diff < 0 ) {
				y -= diff;
				h += diff;
			}

			g.setColor( 0xFF2F03 );

			g.drawRect( x, y, w, h ); // Desenha a borda do retangle

			x--; y--; w+=2; h+=2; // Reposicionamento das coordenadas

			g.drawRect( x, y, w, h ); // Desenha a borda do retangle

			x--; y--; w+=2; h+=2; // Reposicionamento das coordenadas

			g.setColor( 0xFF750A );
			g.drawLine( x, y, x + w, y );
			g.drawLine( x, y + h, x + w, y + h );

			g.setColor( 0xFFB01F );
			g.drawLine( x, y, x, y + h );
			g.drawLine( x + w, y, x + w, y + h );
		} // Fim de desenho do contorno do thumb selecionado

		logo.draw( g );
	}


	public final void setIndex( byte id ) {
		if (id != currentIndex) {
			if ( circular ) {
				if ( canSelectLocked ) {
					if ( id >= TOTAL_GIRLS_PHOTOS ) {
						currentIndex = 0;
					} else if ( id < 0 ) {
						currentIndex = TOTAL_GIRLS_PHOTOS;
					} else {
						currentIndex = id;
					}
				} else {
					if ( id >= NanoMath.min( GameMIDlet.getMaxLevelReached(), TOTAL_GIRLS_PHOTOS ) ) {
						currentIndex = 0;
					} else if ( id < 0 ) {
						currentIndex = NanoMath.min( GameMIDlet.getMaxLevelReached(), TOTAL_GIRLS_PHOTOS );
					} else {
						currentIndex = id;
					}
				}
			} else {
				if ( canSelectLocked ) {
					if ( ( id < TOTAL_GIRLS_PHOTOS && id >= 0 ) ) {
						currentIndex = id;

						if( id == 0 ) {
							arrowLeft.setVisible( false );
						} else {
							arrowLeft.setVisible( true );
						}

						if( id == ( TOTAL_GIRLS_PHOTOS - 1 ) ) {
							arrowRight.setVisible( false );
						} else {
							arrowRight.setVisible( true );
						}
					}
				} else {
					if ( ( id < NanoMath.min( GameMIDlet.getMaxLevelReached(), TOTAL_GIRLS_PHOTOS ) && id >= 0 ) ) {
						currentIndex = id;

						if( id == 0 ) {
							arrowLeft.setVisible( false );
						} else {
							arrowLeft.setVisible( true );
						}

						if( id >= ( NanoMath.min( GameMIDlet.getMaxLevelReached(), TOTAL_GIRLS_PHOTOS ) - 1 ) ) {
							arrowRight.setVisible( false );
						} else {
							arrowRight.setVisible( true );
						}
					}
				}
			}

			updateImageToShow();
			updateImage();
			thumbAccTime = 0;
			resizeThumbs();
		}
	}


	public final void updateImageToShow() {
		try {
			/* TODO usar mutex ou load em outra thread com icone para info de carregando */
			imageOffset.set( 0, 0 ); // Reseta o offset da imagem
			imageGroup.removeDrawable( image );
			image = null;

			//#if DEBUG == "true"
				System.out.println( "-----------------------------------------" );
				System.out.println( "----------> updateImageToShow <----------" );
			//#endif

			if ( ( currentIndex + 1 ) < ( GameMIDlet.getMaxLevelReached() ) ) {

				//#if DEBUG == "true"
					System.out.println( "------------------> " + currentIndex + " <-----------------" );
				//#endif

				infoText.setVisible( false );
				hasBkg = false;

				image = new DrawableImage( PATH_GIRLS + currentIndex + ".jpg" );
				image.defineReferencePixel( ANCHOR_CENTER );
				imageGroup.insertDrawable( image );
			} else {
				hasBkg = true;
				infoText.setVisible( true );
			}
		}
		catch ( Throwable t ) {
			//#if DEBUG == "true"
				GameMIDlet.log( "setIndex exception -> " + t.getMessage() );
				t.printStackTrace();
			//#endif
		}
	}


	public void keyPressed(int key) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				changeBarState();
			break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				if ( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					changeBarState();
				} else {
					exit();
				}
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( 0, DRAG_VELOCITY );
				}
			break;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( 0, -DRAG_VELOCITY );
				}
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				arrowLeft.setFrame( 1 );
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( -DRAG_VELOCITY, 0 );
				}
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				arrowRight.setFrame( 1 );
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( DRAG_VELOCITY, 0 );
				}
			break;

			/** Tratamento das teclas representantes das diagonais*/
			case ScreenManager.KEY_NUM1: /** diagonal topLeft */
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( -DRAG_VELOCITY_COS, -DRAG_VELOCITY_COS );
				}
			break;

			case ScreenManager.KEY_NUM3: /** diagonal topRight */
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( DRAG_VELOCITY_COS, -DRAG_VELOCITY_COS );
				}
			break;

			case ScreenManager.KEY_NUM7:/** diagonal downLeft */
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( -DRAG_VELOCITY_COS, DRAG_VELOCITY_COS );
				}
			break;

			case ScreenManager.KEY_NUM9:/** diagonal downRight */
				if( barState == STATE_HIDDEN || barState == STATE_HIDING ) {
					cursorAcceleration.set( DRAG_VELOCITY_COS, DRAG_VELOCITY_COS );
				}
			break;
			/** Fim de tratamento das teclas representantes das diagonais*/
		}
	}


	public void keyReleased(int key) {
		cursorAcceleration.set( 0, 0 );
		cursorRealAcceleration.set( 0, 0 );

		switch ( key ) {
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				if( barState == STATE_SHOWN || barState == STATE_APPEARING ) {
					setIndex( ( byte ) ( currentIndex - 1 ) );
					arrowLeft.setFrame( 0 );
				}
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				if( barState == STATE_SHOWN || barState == STATE_APPEARING ) {
					setIndex( ( byte ) ( currentIndex + 1 ) );
					arrowRight.setFrame( 0 );
				}
			break;
		}
	}


	public void sizeChanged(int width, int height) {
		setSize( width, height );
	}


//#if TOUCH == "true"
	public void onPointerDragged(int x, int y) {
		switch ( selectionID ) {
			case SELECTED_IMAGE:
				if( imageGroup.contains( x, y ) ) {
					imageOffset.set( imageOffset.x + NanoMath.toFixed( x - pointerLastPos.x ), imageOffset.y + NanoMath.toFixed( y - pointerLastPos.y ) );
					updateImageCenter();
		//			selectionID = SELECTED_NONE;
				}
			break;

			case SELECTED_ARROW_LEFT:
				if( barState == STATE_SHOWN || barState == STATE_APPEARING ) {
					if( !arrowLeft.contains( x - barGroup.getPosX(), y - barGroup.getPosY()) ) {
						arrowLeft.setFrame( 0 );
					} else {
						arrowLeft.setFrame( 1 );
					}
				}
			break;

			case SELECTED_ARROW_RIGHT:
				if( barState == STATE_SHOWN || barState == STATE_APPEARING ) {
					if( !arrowRight.contains( x - barGroup.getPosX(), y - barGroup.getPosY()) ) {
						arrowRight.setFrame( 0 );
					} else {
						arrowRight.setFrame( 1 );
					}
				}
			break;
		}

		pointerLastPos.set( x, y );
	}


	public void onPointerPressed(int x, int y) {
		if( exitButton.contains( x, y ) ) {
			selectionID = SELECTED_EXIT_BUTTON;
		} else {
			if( barState == STATE_SHOWN || barState == STATE_APPEARING ) {
				if( arrowRight.contains( x - barGroup.getPosX(), y - barGroup.getPosY()) ) {
					arrowRight.setFrame( 1 );
					selectionID = SELECTED_ARROW_RIGHT;
				} else {
					arrowRight.setFrame( 0 );

					if( arrowLeft.contains( x - barGroup.getPosX(), y - barGroup.getPosY()) ) {
						arrowLeft.setFrame( 1 );
						selectionID = SELECTED_ARROW_LEFT;
					} else {
						arrowLeft.setFrame( 0 );

						if( thumbs[currentIndex].contains( x - ( barGroup.getPosX() + thumbsGroup.getPosX() ), y - ( barGroup.getPosY() + thumbsGroup.getPosY() ) ) ) {
							selectionID = SELECTED_THUMB;
						} else if( imageGroup.contains( x, y ) ) {
							selectionID = SELECTED_IMAGE;
						}
					}
				}
			} else if( imageGroup.contains( x, y ) ) {
				selectionID = SELECTED_IMAGE;
			}
		}
		pointerFirstPos.set( x, y );
		pointerLastPos.set( x, y );
	}


	public void onPointerReleased(int x, int y) {
		final Point click = new Point( x, y );
		
		if ( exitButton.contains( x, y ) ) {
			if ( selectionID == SELECTED_EXIT_BUTTON ) {
				exit();
			}
		}

		if( barState == STATE_SHOWN || barState == STATE_APPEARING ) {
			if( arrowRight.contains( x - barGroup.getPosX(), y - barGroup.getPosY()) ) {
				if( selectionID == SELECTED_ARROW_RIGHT ) {
					setIndex( ( byte ) ( currentIndex + 1 ) );
				}
				arrowRight.setFrame( 0 );
			}
			else if ( arrowLeft.contains( x - barGroup.getPosX(), y - barGroup.getPosY()) ) {
				if ( selectionID == SELECTED_ARROW_LEFT ) {
					setIndex( ( byte ) ( currentIndex - 1 ) );
				}
				arrowLeft.setFrame( 0 );
			}
			else if ( thumbs[currentIndex].contains( x - ( barGroup.getPosX() + thumbsGroup.getPosX() ), y - ( barGroup.getPosY() + thumbsGroup.getPosY() ) ) ) {
				if ( selectionID == SELECTED_THUMB )
					changeBarState();
			}
		}
		
		if ( selectionID == SELECTED_IMAGE && imageGroup.contains( x, y ) && click.distanceTo( pointerFirstPos ) <= 15 ) {
			changeBarState();
		}
		
		if ( thumbsGroup.contains( x - barGroup.getPosX(), y  - barGroup.getPosY()) ) {
			if ( selectionID == SELECTED_NONE ) {
				for ( byte i = 0; i < thumbs.length; i++ ) {
					if ( thumbs[i].contains( x - ( barGroup.getPosX() + thumbsGroup.getPosX() ), y - ( barGroup.getPosY() + thumbsGroup.getPosY() ) ) ) {
						setIndex( i );
					}
				}
			}
		}
		selectionID = SELECTED_NONE;
	}
//#endif


	public void exit() {
		midlet.onChoose( null, screenID, 0 );
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
		if( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}
}