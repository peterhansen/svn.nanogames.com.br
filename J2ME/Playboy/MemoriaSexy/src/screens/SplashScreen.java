/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Card;
import core.Constants;
import core.GameCard;
import core.ImageBuffer;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Caio
 */
public class SplashScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{
	private static final int ENTER_ANIM_TIME = 666;
	private static final int ANIM_TOTAL_TIME = 6000;
	private static final int ANIM_LIGHT_CICLE_TIME = ANIM_TOTAL_TIME / 6; // has to be divisor of ANIM_TOTAL_TIME
	private static final int ANIM_HIGHLIGHT_TIME = ( ANIM_LIGHT_CICLE_TIME << 1 ) / 3;
	private static final int EXIT_ANIM_TIME = 666;

	final int MAX_WIDTH = ( 3 * MENU_CARD_WIDTH ) >> 2;
	final int MAX_HEIGHT = ( 3 * MENU_CARD_HEIGHT ) >> 2;

	private final Pattern boardTop;
	private final Pattern boardBotton;
	private final MarqueeLabel label;

	private DrawableImage logo;
	private Card[][] cards;

	private final BezierCurve bezier = new BezierCurve();
	private int enterAccTime = 0;
	private int loopAccTime = 0;
	private int exitAccTime = 0;
	private boolean getOut = false;
	private static boolean isPortrait;

	private GameMIDlet midlet;
	private int screenID;

	public SplashScreen( GameMIDlet midlet, int screen ) throws Exception {
		super( 7 );

		this.midlet = midlet;
		screenID = screen;

		final DrawableImage img = new DrawableImage( PATH_IMAGES + "menu_pattern.png" );
		final DrawableImage img2 = new DrawableImage( img );
		img2.setTransform( TRANS_MIRROR_V );
		img.setPosition( 0, img.getHeight() );
		final DrawableGroup bkg= new DrawableGroup( 2 );
		bkg.setSize( img.getWidth(), img.getHeight() << 1 );
		bkg.insertDrawable( img );
		bkg.insertDrawable( img2 );

		boardTop = new Pattern( bkg );
		insertDrawable( boardTop );

		logo = new DrawableImage( PATH_IMAGES + "menu_logo.png" );
		isPortrait = true;
		logo.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( logo );

		boardBotton = new Pattern( bkg );
		insertDrawable( boardBotton );

		label = new MarqueeLabel( FONT_TITLE, TEXT_PRESS_ANY_KEY );
		insertDrawable( label );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		label.setVisible( false );

		resetCards();

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void resetCards() {

		final String[][] c = { {"J", "O", "G", "O" }, {"D", "A"}, {"M", "E", "M", "Ó", "R", "I", "A"} };
		final Label l = new Label( FONT_TITLE, "" );
		final Drawable d;
		Pattern p;

		try {
			d = new DrawableImage( PATH_IMAGES + "menu_pattern.png" );
			p = new Pattern( d );
		} catch ( Throwable t ) {
			p = new Pattern( 0x9C221C );
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
		}

		final int w = MENU_CARD_WIDTH >> 2;
		final int h = MENU_CARD_HEIGHT >> 2;
		p.setSize( w, h );

		cards = new Card[ c.length ][];
		for (int y = 0; y < cards.length; ++y) {
			cards[y] = new Card[c[y].length];
			for (int x = 0; x < cards[y].length; ++x) {
				try {
					cards[y][x] = new Card( );
					l.setText( c[y][x] );
					final Image img = Image.createImage( w, h );
					p.draw( img.getGraphics() );
					l.setPosition( ( w - l.getTextWidth() ) >> 1, ( h - l.getFont().getImage().getHeight() ) >> 1 );
					l.draw( img.getGraphics() );

					cards[y][x].setCardBackType( ImageBuffer.TYPE_BACK_CARD );
					cards[y][x].setCardFace( img );
					cards[y][x].flip();
				} catch ( Throwable t ) {
					//#if DEBUG == "true"
						t.printStackTrace();
					//#endif
				}
			}
		}
	}

//	public void paint( Graphics g ) {
//		super.paint( g );
//
//		for (int i = 0; i < cards.length; ++i) {
//			for (int j = 0; j < cards[i].length; ++j) {
//				if( cards[i][j] != null ) {
//					cards[i][j].paint( g );
//				}
//			}
//		}
//	}

	public void paint( Graphics g ) {
		super.paint( g );

		for (int i = 0; i < cards.length; ++i) {
			for (int j = 0; j < cards[i].length; ++j) {
				if( cards[i][j] != null ) {
					cards[i][j].draw( g );
				}
			}
		}
	}

//	public void draw( Graphics g ) {
//		super.draw( g );
//
//		for (int i = 0; i < cards.length; ++i) {
//			for (int j = 0; j < cards[i].length; ++j) {
//				if( cards[i][j] != null ) {
//					cards[i][j].draw( g );
//				}
//			}
//		}
//	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		if( !updateBoards() )
			UpdateCardsPosition();
	}


	public void UpdateCardsPosition() {
		int cardW = getWidth();
		int spacing = SAFE_MARGIN / 3;
		for (int i = 0; i < cards.length; ++i) {
			cardW = NanoMath.min( ( getWidth() - ( ( SAFE_MARGIN << 1 ) + ( spacing * (cards[i].length - 1) ) ) ) / cards[i].length, cardW );
		}

		int cardH = ( 3 * cardW ) >> 1;
		final int realH = ( getHeight() - ( boardBotton.getHeight() + boardTop.getHeight() ) );

		if( cardH * cards.length + ( ( cards.length + 1 ) * SAFE_MARGIN ) >= realH ) {
			cardH = ( realH - ( ( cards.length + 1 ) * SAFE_MARGIN ) ) / cards.length;
			cardW = ( cardH << 1 ) / 3;
		}

		final int h = ( ( cards.length - 1 ) * ( cardH + SAFE_MARGIN  ) );

		int startX;
		int posY = ( boardTop.getHeight() ) + ( ( realH - h ) >> 1 );

		for (int i = 0; i < cards.length; ++i) {
			startX = ( getWidth() - ( ( cardW + spacing ) * ( cards[i].length - 1 ) ) ) >> 1;
			for (int j = 0; j < cards[i].length; ++j) {
				cards[i][j].setSize( cardW, cardH );
				cards[i][j].setRefPixelPosition( startX + ( j * ( cardW + spacing ) ), posY );
				cards[i][j].setPreferedSize( cardW, cardH );
				cards[i][j].animate();
			}
			posY += cardH + SAFE_MARGIN;
		}
	}


	public boolean updateBoards() {
		boolean changed = false;

		if( getHeight() > getWidth() ) {
			if( !isPortrait ) {
				if( logo != null ) {
					removeDrawable( logo );
					logo = null;
				}
				try {
					logo = new DrawableImage( PATH_IMAGES + "menu_logo.png"  );
				} catch ( Throwable t ) {
					//#if DEBUG == "true"
						t.printStackTrace();
					//#endif
				}
				insertDrawable( logo );
				isPortrait = true;
			}
		} else {
			if( isPortrait ) {
				if( logo != null ) {
					removeDrawable( logo );
					logo = null;
				}
				try {
					logo = new DrawableImage( PATH_IMAGES + "menu_logo_H.png"  );
				} catch ( Throwable t ) {
					//#if DEBUG == "true"
						t.printStackTrace();
					//#endif
				}
				insertDrawable( logo );
				isPortrait = false;
			}
		}

		int acc;
		if( getOut ) {
			 acc = NanoMath.divInt( NanoMath.min( exitAccTime, EXIT_ANIM_TIME ), EXIT_ANIM_TIME );
		} else {
			 acc = NanoMath.divInt( NanoMath.min( enterAccTime, ENTER_ANIM_TIME ), ENTER_ANIM_TIME );
		}

		NanoMath.sqrtFixed( acc );

		if( getOut ) {
			acc = NanoMath.toFixed( 1 ) - acc;
		}

		if( !getOut ) {
			boardBotton.setSize( getWidth(), MENU_BOTTOM_BAR_HEIGHT );
			boardBotton.setPosition( 0, getHeight() - NanoMath.toInt( MENU_BOTTOM_BAR_HEIGHT * acc ) );

			label.setSize( boardBotton.getWidth(), label.getHeight() );
			label.setPosition( 0 , boardBotton.getPosY() + ( ( boardBotton.getHeight() - label.getHeight() ) >> 1 )  );
			label.setTextOffset( ( label.getWidth() - label.getTextWidth() ) >> 1 );

			boardTop.setSize( getWidth(), logo.getHeight() + ( SAFE_MARGIN << 1 ) );
			boardTop.setPosition( 0, - 1 - boardTop.getHeight() + NanoMath.toInt( boardTop.getHeight() * acc ) );

			if( isPortrait ) {
				logo.setPosition( boardTop.getPosX() + ( ( boardTop.getWidth() - logo.getWidth() ) >> 1 ) ,
						boardTop.getPosY() + ( ( boardTop.getHeight() - logo.getHeight() ) >> 1 ) );
			} else {
				logo.setPosition( boardTop.getPosX() + SAFE_MARGIN ,
						boardTop.getPosY() + ( ( boardTop.getHeight() - logo.getHeight() ) >> 1 ) );
			}

			final int y = getPosY() + boardTop.getPosY() + boardTop.getHeight();
			GameMIDlet.setBkgViewport( new Rectangle( getPosX(), y, getWidth(), ( getPosY() + boardBotton.getPosY() ) - y ) );
		}

		if( changed ) {
			UpdateCardsPosition();
			return true;
		}

		return false;
	}


	public void update( int delta ) {
		super.update( delta );

		for( int i = 0; i < cards.length; i ++ ) {
			for( int j = 0; ( j < ( cards[i].length ) ); j ++ ) {
				cards[i][j].update( delta );
			}
		}

		if( getOut ) {
			exitAccTime += delta;
			if( exitAccTime > ( EXIT_ANIM_TIME >> 1 ) ) {
				if( exitAccTime > EXIT_ANIM_TIME )
					midlet.onChoose( null, screenID, TEXT_EXIT);
			}
			updateBoards();
		} else {
			if( enterAccTime < ENTER_ANIM_TIME ) {
				enterAccTime += delta;
				updateBoards();
			} else {
				loopAccTime += delta;


				int current = 1;
				for( int i = cards.length - 1; i >= 0; i-- ) {
					for( int j = cards[i].length - 1; j >= 0; j-- ) {
						final int acc = ( ( loopAccTime + ( current * ANIM_TOTAL_TIME / 50 ) % ANIM_TOTAL_TIME ) + ANIM_TOTAL_TIME ) % ANIM_TOTAL_TIME;
						if( acc < ( ( ANIM_TOTAL_TIME ) >> 1 ) ) {
							if( !cards[i][j].isFlipped() ) {
								cards[i][j].flip();
							}
						} else {
							if( cards[i][j].isFlipped() ) {
								cards[i][j].flip();
							}
						}
						current++;
					}
				}

				if( loopAccTime % ANIM_LIGHT_CICLE_TIME < ANIM_HIGHLIGHT_TIME ) {
					label.setVisible( true );
				} else {
					label.setVisible( false );
				}

				if( loopAccTime > ANIM_TOTAL_TIME ) {
					loopAccTime -= ANIM_TOTAL_TIME;
				}
			}
		}
	}


	public void getOut() {
		if( enterAccTime >= ENTER_ANIM_TIME && !getOut ) {
			exitAccTime = 0;
			getOut = true;
			label.setVisible( false );
		}
	}


	public void keyPressed( int key ) {
		getOut();
	}


	public void keyReleased( int key ) {
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
		if( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}



	public void sizeChanged( int width, int height ) {
	}


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
		}


		public void onPointerPressed( int x, int y ) {
			getOut();
		}


		public void onPointerReleased( int x, int y ) {
		}
	//#endif
}
