/**
 * MainMenu.java
 *
 * Created on Sep 21, 2010 12:02:13 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Card;
import core.Constants;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
	
/**
 *
 * @author peter
 */
public class MainMenu extends UpdatableGroup implements Constants, KeyListener, ScreenListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{
	//private static final int ANIM_TOTAL_TIME = 500;
	private static final int ANIM_TOTAL_TIME = 666;

	private static final byte SELECTED_NONE = 0;
	private static final byte SELECTED_ARROW_LEFT = 1;
	private static final byte SELECTED_ARROW_RIGHT = 2;
	private static final byte SELECTED_TEXT = 3;
	private static final byte SELECTED_CARD = 4;

	private static Pattern boardTop;
	private static Pattern boardBotton;
	private static MarqueeLabel label;
	private static boolean isPortrait;

	private DrawableImage logo;
	private static Sprite arrowRight;
	private static Sprite arrowLeft;

	//#if TOUCH == "true"
	private static Rectangle arrowLeftArea;
	private static Rectangle arrowRightArea;
	//#endif

	private static Card[] cards;

	private static int[] entries;

	private static int currentIndex = -1;/** índice atualmente selecionado no menu */
	private static boolean circular;/** indica se o menu é circular */
	
	private static final BezierCurve bezier = new BezierCurve();
	private static int accTime = ANIM_TOTAL_TIME;
	private static Point bezierPoint = new Point();

	private GameMIDlet midlet;

	private static int width;
	private static int height;

	private int screenID;
	private int selectionID;
	private static Hashtable hashText = new Hashtable();

	private final Mutex cardMutex = new Mutex();

	public MainMenu( GameMIDlet midlet, int screen, int[] entries ) throws Exception {
		super( 6 /*+ icons.length*/ );
		

		screenID = screen;
		this.midlet = midlet;

		hashText.clear();
		for( int i = 0; i < entries.length; i++ ) {
			hashText.put( new Integer( entries[i] ), new Integer( i ) );
			//#if DEBUG == "true"
				System.out.println( "hashText.put( " + entries[i] + ", " + i + " )" );
			//#endif
		}

		if( cards == null )
			cards = new Card[ entries.length ];

		final DrawableImage img = new DrawableImage( PATH_IMAGES + "menu_pattern.png" );
		final DrawableImage img2 = new DrawableImage( img );
		img2.setTransform( TRANS_MIRROR_V );
		img.setPosition( 0, img.getHeight() );
		final DrawableGroup bkg= new DrawableGroup( 2 );
		bkg.setSize( img.getWidth(), img.getHeight() << 1 );
		bkg.insertDrawable( img );
		bkg.insertDrawable( img2 );

		if( boardTop == null ) {
			boardTop = new Pattern( bkg );
			insertDrawable( boardTop );
			boardTop.setPosition( 0, - 1);
		}

		if( boardBotton == null ) {
			boardBotton = new Pattern( bkg );
			insertDrawable( boardBotton );
		}

		logo = new DrawableImage( PATH_IMAGES + "menu_logo.png" );
		isPortrait = true;
		insertDrawable( logo );

		if( arrowRight == null ) {
			arrowRight = new Sprite( PATH_IMAGES + "menu_arrow" );
			arrowRight.mirror( TRANS_MIRROR_H );
			arrowRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
			insertDrawable( arrowRight );
		}

		if( arrowLeft == null ) {
			arrowLeft = new Sprite( arrowRight );
			arrowLeft.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
			insertDrawable( arrowLeft );
		}

		if( label == null ) {
			label = new MarqueeLabel( FONT_TITLE, "" );
			insertDrawable( label );
			label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		}

		circular = false;

		if( this.entries == null ) {
			this.entries = entries;
		}

		if( currentIndex == -1 ) {
			for( int i = 0; i < entries.length; i++ ){
				if( entries[i] == TEXT_NEW_GAME || i == entries.length - 1 ) {
					currentIndex = i;
					break;
				}
			}
		}

		setCurrentIndex( currentIndex );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public static void setEntry( int textId ) {

		//#if DEBUG == "true"
			System.out.println( "setEntry(" + textId + ")" );
		//#endif

		final Integer i = ( Integer ) hashText.get( new Integer( textId ) );
		if ( i != null ) {
			//#if DEBUG == "true"
				System.out.println( "setCurrentIndex(" + i + ")" );
			//#endif

			setCurrentIndex( i.intValue() );
		}
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );
		this.width = width;
		this.height = height;

		if( getHeight() > getWidth() ) {
			if( !isPortrait ) {
				if( logo != null ) {
					removeDrawable( logo );
					logo = null;
				}
				try {
					logo = new DrawableImage( PATH_IMAGES + "menu_logo.png"  );
				} catch ( Throwable t ) {
					//#if DEBUG == "true"
						t.printStackTrace();
					//#endif
				}
				insertDrawable( logo );
				isPortrait = true;
			}
		} else {
			if( isPortrait ) {
				if( logo != null ) {
					removeDrawable( logo );
					logo = null;
				}
				try {
					logo = new DrawableImage( PATH_IMAGES + "menu_logo_H.png"  );
				} catch ( Throwable t ) {
					//#if DEBUG == "true"
						t.printStackTrace();
					//#endif
				}
				insertDrawable( logo );
				isPortrait = false;
			}
		}

		boardBotton.setSize( width, MENU_BOTTOM_BAR_HEIGHT );
		boardBotton.setPosition( 0, height - MENU_BOTTOM_BAR_HEIGHT );

		arrowLeft.setRefPixelPosition( SAFE_MARGIN,  boardBotton.getPosY() + ( boardBotton.getHeight() >> 1 ) );
		arrowRight.setRefPixelPosition( width - SAFE_MARGIN,  boardBotton.getPosY() + ( boardBotton.getHeight() >> 1 ) );
		//#if TOUCH == "true"
			arrowLeftArea = new Rectangle();
			arrowRightArea = new Rectangle();
			arrowLeftArea.set( arrowLeft.getPosX(), arrowLeft.getPosY(), arrowLeft.getSize().x + 50, arrowLeft.getSize().y);
			arrowRightArea.set( arrowRight.getPosX() - 50, arrowRight.getPosY(), arrowRight.getSize().x + 50, arrowRight.getSize().y);
		//#endif

		label.setSize( boardBotton.getWidth() - ( ( SAFE_MARGIN  + arrowRight.getWidth() ) << 1 ), label.getHeight() );
		label.setPosition( SAFE_MARGIN + ( arrowRight.getWidth() ), boardBotton.getPosY() + ( ( boardBotton.getHeight() - label.getHeight() ) >> 1 )  );

		updateEntriesText();

		boardTop.setSize( width, logo.getHeight() + ( SAFE_MARGIN << 1 ) );

		if( isPortrait ) {
			logo.setPosition( ( ( boardTop.getWidth() - logo.getWidth() ) >> 1 ) ,  ( ( boardTop.getHeight() - logo.getHeight() ) >> 1 ) );
		} else {
			logo.setPosition( SAFE_MARGIN ,  ( ( boardTop.getHeight() - logo.getHeight() ) >> 1 ) );
		}

		setCurrentIndex( currentIndex );

		final int y = getPosY() + boardTop.getPosY() + boardTop.getHeight();
		GameMIDlet.setBkgViewport( new Rectangle( getPosX(), y, getWidth(), ( getPosY() + boardBotton.getPosY() + boardBotton.getHeight() ) - y ) );
	}
	
	
	public static DrawableImage getIcon( int TextID ) throws Exception {
		switch( TextID ) {
			case TEXT_NEW_GAME:
				return new DrawableImage( PATH_IMAGES + "menu_jogar.png" );

			case TEXT_OPTIONS:
				return new DrawableImage( PATH_IMAGES + "menu_options.png" );

			case TEXT_NANO_ONLINE:
				return new DrawableImage( PATH_IMAGES + "menu_ranking.png" );

			case TEXT_PHOTO_GALLERY:
				return new DrawableImage( PATH_IMAGES + "menu_galeria.png" );

			case TEXT_HELP:
				return new DrawableImage( PATH_IMAGES + "menu_help.png" );

			case TEXT_LOADING:
			case TEXT_RECOMMEND_TITLE:
				return new DrawableImage( PATH_IMAGES + "menu_indique.png" );

			case TEXT_CREDITS:
				return new DrawableImage( PATH_IMAGES + "menu_credits.png" );

			case TEXT_EXIT:
				return new DrawableImage( PATH_IMAGES + "menu_quit.png" );
		}
		return null;
	}


	public void keyPressed(int key) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				activateEntryAction();
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				arrowLeft.setFrame( 1 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				arrowRight.setFrame( 1 );
			break;
		}
	}


	public void keyReleased(int key) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				previousIndex();
				arrowLeft.setFrame( 0 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				nextIndex();
				arrowRight.setFrame( 0 );
			break;
		}
	}


	private void activateEntryAction() {
		midlet.onChoose( null, screenID, entries[currentIndex] );
	}


	private void nextIndex() {
		if ( currentIndex < ( entries.length - 1 ) )
			setCurrentIndex( currentIndex + 1 );
		else if ( circular )
			setCurrentIndex( 0 );
	}


	private void previousIndex() {
		if ( currentIndex > 0 )
			setCurrentIndex( currentIndex - 1 );
		else if ( circular )
			setCurrentIndex( entries.length - 1 );
	}
	
	
	public void load() {
		setCurrentIndex( currentIndex );
	}


	public void draw( Graphics g ) {
		super.draw( g );

		cardMutex.acquire(); {
			final int temp = NanoMath.toInt( ( currentIndexFP() / 100 ) + NanoMath.HALF ) ;
			for( int i = cards.length; i > 0; i-- ) {
				if( temp - i >= 0 )
					if( cards[ temp - i ] != null )
						cards[ temp - i ].draw( g );
				if( temp + i < cards.length )
					if( cards[ temp + i ] != null )
						cards[ temp + i ].draw( g );
			}

			if( temp >= 0 && temp < cards.length )
				if( cards[ temp ] != null )
					cards[ temp ].draw( g );
		}
		cardMutex.release();
	}
	
	
	private static int currentIndexFP() {
		Point p = new Point();
		bezier.getPointAtFixed( p, NanoMath.divInt( accTime, ANIM_TOTAL_TIME ) );
		return NanoMath.toFixed( p.x );
	}


	public void showNotify( boolean deviceEvent ) {
		if( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}


	public void hideNotify( boolean deviceEvent ) {
		if ( deviceEvent )
			MediaPlayer.stop();
	}
	
	
	public final void update( int delta ) {
		if( accTime < ANIM_TOTAL_TIME ) {
			accTime += delta;

			if( accTime > ANIM_TOTAL_TIME )
				accTime = ANIM_TOTAL_TIME;

			final int y = boardTop.getPosY() + boardTop.getHeight();
			final int x = NanoMath.toFixed( width ) >> 1;

			cardMutex.acquire();
			for( int i = 0; i < ( ( cards != null ) ? cards.length : 0 ); i++ ) {
				if( cards[i] != null ) {
					final int t = ( ( NanoMath.toFixed(i) - currentIndexFP()/ 100 ) );

					final int r;
					if( t > 0 ) {
						r = 2*NanoMath.sqrtFixed( t / 3 );
					} else {
						r = 2*-NanoMath.sqrtFixed( -t / 3 );
					}

					cards[i].setSize( NanoMath.toInt( ( MENU_CARD_WIDTH * ( NanoMath.toFixed( 3 ) - Math.abs( t ) ) ) / 3 ),
							NanoMath.toInt( ( MENU_CARD_HEIGHT * ( NanoMath.toFixed( 3 ) - Math.abs( t ) ) ) / 3 ), true );
					if( boardBotton != null ) {
						cards[i].setPosition( NanoMath.toInt( ( x + ( ( r * width ) / 4 ) ) - ( NanoMath.toFixed( cards[i].getWidth() ) >> 1 ) ) ,
								y + ( ( ( 95 * ( boardBotton.getPosY() - y ) ) - ( 100 * cards[i].getHeight() ) ) >> 1 ) / 100 );
					}
				}
			}
			cardMutex.release();
		}
	}


	private static void setCurrentIndex( int index ) {
		// TODO ciclar entre as cartas exibidas
		if ( entries != null && index >= 0 && index < entries.length ) {
			currentIndex = index;

			bezier.origin.set( NanoMath.toInt(currentIndexFP()), 0 );
			accTime = 0;
			bezier.destiny.set( currentIndex * 100 , 0 );

			bezier.control1.set( bezier.destiny );
			bezier.control2.set( bezier.destiny );

			for( int i = 0; i < cards.length; i ++ ) {
				if( GameMIDlet.isLowMemory() ) {
					if( i <= currentIndex + 2 && i >= currentIndex - 2 ) {
						try {
							if( cards[i] == null ) {
								cards[i] = new Card();
								cards[i].setCurrentDrawable( getIcon( entries[i] ) );
								cards[i].setPosition( Short.MAX_VALUE, Short.MAX_VALUE );
							}
						} catch( Throwable tr ) {
							//#if DEBUG == "true"
									tr.printStackTrace();
							//#endif
						}
					} else {
						cards[i] = null;
					}
				} else {
					try {
						if( cards[i] == null ) {
							cards[i] = new Card();
							cards[i].setCurrentDrawable( getIcon( entries[i] ) );
							cards[i].setPosition( Short.MAX_VALUE, Short.MAX_VALUE );
						}
					} catch( Throwable tr ) {
						//#if DEBUG == "true"
								tr.printStackTrace();
						//#endif
					}
				}
			}

			if( index == 0 ) {
				arrowLeft.setVisible( false );
			} else {
				arrowLeft.setVisible( true );

				if( index == ( entries.length - 1 ) ) {
					arrowRight.setVisible( false );
				} else {
					arrowRight.setVisible( true );
				}
			}
			updateEntriesText();
		} else {
			
		}
	}


	public static void unload() {
		GameMIDlet.log( "--Before MainMenu Unload--" );
		// TODO desalocar recursos da memória
		cards = null;
		entries = null;
		label = null;
		boardTop = null;
		boardBotton = null;
		arrowLeft = null;
		arrowRight = null;
		GameMIDlet.log( "--After MainMenu Unload--" );
	}


	/**
	 * Atualiza o texto e o posicionamento do label de texto do menu.
	 */
	private static void updateEntriesText() {
		if( label != null && entries != null && currentIndex < entries.length ) {
			final Point p = new Point( label.getSize() );
			label.setText( entries[currentIndex] );
			label.setSize( p );
			label.setTextOffset( ( p.x - label.getTextWidth() ) >> 1 );
		}
	}


	public void sizeChanged(int width, int height) {
		setSize( width, height );
	}


	//#if TOUCH == "true"
		public void onPointerDragged(int x, int y) {
			if( !arrowRight.contains( x, y ) ) {
				arrowRight.setFrame( 0 );
			}
			if( !arrowLeft.contains( x, y ) ) {
				arrowLeft.setFrame( 0 );
			}
		}


		public void onPointerPressed(int x, int y) {
			if( arrowRight.contains( x, y ) ) {
				arrowRight.setFrame( 1 );
				selectionID = SELECTED_ARROW_RIGHT;
				return;
			}
			arrowRight.setFrame( 0 );

			if( arrowLeft.contains( x, y ) ) {
				arrowLeft.setFrame( 1 );
				selectionID = SELECTED_ARROW_LEFT;
				return;
			}
			arrowLeft.setFrame( 0 );

			if( arrowRightArea.contains( x, y ) ) {
				arrowRight.setFrame( 1 );
				selectionID = SELECTED_ARROW_RIGHT;
				return;
			}

			if( arrowLeftArea.contains( x, y ) ) {
				arrowLeft.setFrame( 1 );
				selectionID = SELECTED_ARROW_LEFT;
				return;
			}

			if( label.contains( x, y ) ) {
				selectionID = SELECTED_TEXT;
				return;
			}

			if( currentIndex >= 0 && currentIndex < cards.length ) {
				if( cards[ currentIndex ] != null ) {
					if( cards[ currentIndex ].contains( x, y ) ) {
						selectionID = SELECTED_CARD;
						return;
					}
				}
			}

			for( int i = 0; i < cards.length; i++ ) {
				if( currentIndex - i >= 0 ) {
					if( cards[ currentIndex - i ] != null ){
						if( cards[ currentIndex - i ].contains( x, y ) ) {
							selectionID = SELECTED_CARD;
							return;
						}
					}
				}
				if( currentIndex + i < cards.length ) {
					if( cards[ currentIndex + i ] != null ){
						if( cards[ currentIndex + i ].contains( x, y ) ) {
							selectionID = SELECTED_CARD;
							return;
						}
					}
				}
			}
		}


		public void onPointerReleased(int x, int y) {
			if( arrowRight.contains( x, y ) ) {
				if(selectionID == SELECTED_ARROW_RIGHT) {
					nextIndex();
				}
				arrowRight.setFrame( 0 );
			}
			else if( arrowLeft.contains( x, y ) ) {
				if(selectionID == SELECTED_ARROW_LEFT) {
					previousIndex();
				}
				arrowLeft.setFrame( 0 );
			}
			else if( label.contains( x, y ) ) {
				if(selectionID == SELECTED_TEXT) {
					activateEntryAction();
				}
			}
			else {
				if( currentIndex >= 0 && currentIndex < cards.length ) {
					if( cards[ currentIndex ] != null ) {
						if( selectionID == SELECTED_CARD ) {
							if( cards[ currentIndex ].contains( x, y ) ) {
								activateEntryAction();
							}
						}
					}
				}

				for( int i = 0; i < cards.length; i++ ) {
					if( currentIndex - i >= 0 ) {
						if( cards[ currentIndex - i ] != null ){
							if( selectionID == SELECTED_CARD ) {
								if( cards[ currentIndex - i ].contains( x, y ) ) {
									setCurrentIndex( currentIndex - i );
								}
							}
						}
					}
					if( currentIndex + i < cards.length ) {
						if( cards[ currentIndex + i ] != null ){
							if( selectionID == SELECTED_CARD ) {
								if( cards[ currentIndex + i ].contains( x, y ) ) {
									setCurrentIndex( currentIndex + i );
								}
							}
						}
					}
				}
			}
			selectionID = SELECTED_NONE;
		}
	//#endif
}
