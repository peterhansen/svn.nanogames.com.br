/**
 * GameScreen.java
 *
 * Created on Dec 3, 2010 9:48:39 AM
 *
 */
package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.Cursor;
import core.GameCard;
import core.GameTextBox;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public final class GameScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if TOUCH == "true"
		, PointerListener
//#endif
{

	private static final byte MAX_LEVEL = 15;
	/***/
	private static final short SCORE_CARD = 100;
	/** Penalidade de tempo ao errar uma carta. */
	private static final short TIME_PENALTY_MISS = 10 * 1000;

	/** Duração de um round (2 jogadores), em milisegundos. */
	private static final int TIME_ROUND = 10 * 1000;

	/** Duração de uma partida (1 jogador), em milisegundos. */
//#if DEBUG == "true"
	private static final int TIME_GAME = 3 * 60 * 1000;

//#else
//# private static final int TIME_GAME = 3 * 60 * 1000;
//#endif
	/** Duração mínima da mensagem de fim de jogo. */
	private static final short TIME_GAME_OVER_MIN = 1000;
	/** Duração mínima da mensagem de fim de fase. */
	private static final short TIME_LEVEL_COMPLETE_MIN = 1000;
//#if DEBUG == "true"
	private static final short TIME_CARD_MIN = 1000;
//#else
//# private static final short TIME_CARD_MIN = 2000;
//#endif

	private static final int TIME_PULSATE_BONUS = 750;
	/***/
	private static final int SCORE_SHOWN_MAX = 999999;
	/***/
	private static final byte CARDS_PER_LEVEL_INITIAL = 8;
	/***/
	private static final byte CARDS_PER_LEVEL_FINAL = 25;
	/***/
	private static final int TIME_MESSAGE_TIME = 2500;

	private static final int TIME_SHOW_PICTURE = 2000;

	private static final int TIME_SHOW_OUTCOME = 1000;
	/**
	 *	Os estados e seus significados:
	 * 	STATE_NONE:
	 *  STATE_PLAYER_TURN_MESSAGE: "É sua vez!" ou então "Escolha sua carta"
	 *  STATE_CHOOSING_CARD_1: Escolhendo a primeira carta
	 *  STATE_CHOOSING_CARD_2: Escolhendo a segunda carta
	 *  STATE_INTRO: Começo de jogada
	 *  STATE_SHOW_PICTURE:
	 *  STATE_GAME_OVER: Fim de jogo
	 *  STATE_LEVEL_COMPLETE: Nível completo
	 *  STATE_NEW_RECORD: Novo recorde alcançado
	 */
	private static final byte STATE_NONE = -1;
	private static final byte STATE_PLAYER_TURN_MESSAGE = 0;
	private static final byte STATE_CHOOSING_CARD_1 = 1;
	private static final byte STATE_CHOOSING_CARD_2 = 2;
	private static final byte STATE_INTRO = 3;
	private static final byte STATE_SHOW_CARD_1 = 4;
	private static final byte STATE_SHOW_CARD_2 = 5;
	private static final byte STATE_SHOW_PICTURE = 6;
	private static final byte STATE_GAME_OVER = 7;
	private static final byte STATE_LEVEL_COMPLETE = 8;
	private static final byte STATE_NEW_RECORD = 9;
	private static final byte STATE_SHOWCARDS = 10;
	private static final byte STATE_NEXT_TURN = 12;
	private static final byte STATE_TEMPORARILY_SHOW_CARDS = 13;
	private static final byte STATE_SHOW_SPECIAL_CARD_MESSAGE = 15;


	private static final byte DIRECTION_ANGLE_0 = 0;
	private static final byte DIRECTION_ANGLE_45 = 1;
	private static final byte DIRECTION_ANGLE_90 = 2;
	private static final byte DIRECTION_ANGLE_135 = 3;
	private static final byte DIRECTION_ANGLE_180 = 4;
	private static final byte DIRECTION_ANGLE_225 = 5;
	private static final byte DIRECTION_ANGLE_270 = 6;
	private static final byte DIRECTION_ANGLE_315 = 7;

	private boolean touchScreenLocked;
	/***/
	private byte state;
	/***/
	private int stateTime;
	/***/
	private int[] scoreShown;
	/***/
	private int[] score;

	public static final byte MAX_CARD_FINDING_TRIES = 3;
	/***/
	private final Label scoreLabel;
	/** Velocidade de atualização da pontuação. */
	private MUV[] scoreSpeed;
	/***/
	private short level;
	/***/
	private int[] timeRemaining;
	private byte remainingCards;
	/***/
	private final Label timeLabel;
	/***/
	private short[] combo;
	/***/
	private final Label comboLabel;
	/**
	 *
	 */
	private final UpdatableGroup cardsGroup;
	/***/
	private GameCard[][] cards;
	private Point lastCard;
	/** Carta selecionada atualmente. É a que pulsa. */
	private final Point currentCard = new Point(-1, -1);
	/**
	 * Imagem do cursor.
	 */
	private final Cursor cursor;
	private DrawableImage girl;
	private Pattern girlCardPattern;
	private byte numIndexes;
	private byte numPlayers;
	private DrawableImage multiplierBall;
	private Pattern scoreBar;
	private Pattern timeBar;
	private GameTextBox gameMsg;
	private byte currentPlayer;
	private int turns;
	private boolean recordSent;

	private Rectangle[] collectedRegions;
	private byte bonusToAppear;
	private boolean showPieces;
	private final byte[] order;

	/**
	 * Construtor
	 * Inicializa fontes e campos de texto. Define também quantas cartas o jogo
	 * tem e incializa direto para o nível 1.
	 * @throws Exception
	 */
	public GameScreen() throws Exception {
		super(21);
		System.gc();
		girl = null;
		touchScreenLocked = false;

		int posX = 0;
		int posY = 0;
		cardsGroup = new UpdatableGroup(CARDS_PER_LEVEL_FINAL + 5);
		insertDrawable(cardsGroup);

		cursor = new Cursor();
		insertDrawable(cursor);

		scoreBar = new Pattern(0);
		DrawableImage menuPattern = new DrawableImage("/menu_pattern.png");
		scoreBar.setFill(menuPattern);
		scoreBar.setSize(new Point(ScreenManager.SCREEN_WIDTH, menuPattern.getHeight()));
		scoreBar.setPosition(0, ScreenManager.SCREEN_HEIGHT - menuPattern.getHeight());
		insertDrawable(scoreBar);

		if (ScreenManager.SCREEN_HEIGHT >=  ScreenManager.SCREEN_WIDTH)	{
			timeBar = new Pattern(0);
			menuPattern = new DrawableImage(menuPattern);
			timeBar.setFill(menuPattern);
			timeBar.setSize(new Point(ScreenManager.SCREEN_WIDTH, menuPattern.getHeight()));
			timeBar.setPosition(0, - 1);
			insertDrawable(timeBar);
		}
		else
			timeBar = null;

		multiplierBall = new DrawableImage("/argola_multiplicador.png");
		insertDrawable(multiplierBall);
		multiplierBall.setPosition((timeBar == null) ? 0 : COMBO_BALL_X, ScreenManager.SCREEN_HEIGHT - (scoreBar.getHeight() + multiplierBall.getHeight() ) / 2);

		comboLabel = new Label(FONT_MULTIPLIER);
		insertDrawable(comboLabel);
		comboLabel.setPosition((( multiplierBall.getWidth() - comboLabel.getWidth() ) >> 1) + multiplierBall.getPosX(),(( multiplierBall.getHeight() - comboLabel.getHeight() ) >> 1) + multiplierBall.getPosY());

		///box score
		DrawableImage boxScore = new DrawableImage("/box_pontos.png");

		if (timeBar == null){
			posX = (ScreenManager.SCREEN_HALF_WIDTH) - boxScore.getWidth() - DEFAULT_PADDING_WIDTH;
			posY = ScreenManager.SCREEN_HEIGHT - (boxScore.getHeight()) - DEFAULT_PADDING_HEIGHT;
		} else {			
			posY = ScreenManager.SCREEN_HEIGHT - (boxScore.getHeight() / 2) - (timeBar.getHeight() / 2);
//#if SCREEN_SIZE == "SMALL"
//# 			posX = ((ScreenManager.SCREEN_WIDTH - boxScore.getWidth()) >> 1) - DEFAULT_PADDING_WIDTH;
//#else
		posX = (ScreenManager.SCREEN_WIDTH - boxScore.getWidth()) >> 1;
//#endif
		}

		boxScore.setPosition( posX , posY );
		insertDrawable(boxScore);

		scoreLabel = new Label(FONT_NUMBERS);
		insertDrawable(scoreLabel);
		String tmp = "";
		for (int c = 0; c < MAX_NUMBER_CHARS; ++c)
			tmp += "0";
		scoreLabel.setText( tmp );
		scoreLabel.setPosition(boxScore.getPosX() + (boxScore.getWidth() >> 1) - (scoreLabel.getWidth() >> 1), boxScore.getPosY() + (boxScore.getHeight() / 2) - (scoreLabel.getHeight() / 2));

		///box time
		DrawableImage boxTime = new DrawableImage("/box_tempo.png");		

		if (timeBar == null){
			posX = (ScreenManager.SCREEN_HALF_WIDTH) + (( ScreenManager.SCREEN_HALF_WIDTH - PAUSE_BUTTON_WIDTH - boxTime.getWidth() - SAFE_MARGIN) >> 1);
			posY += (boxScore.getHeight() - boxTime.getHeight()) / 2;
		} else {
			posX = (ScreenManager.SCREEN_WIDTH - boxTime.getWidth()) >> 1;
			posY = (timeBar.getHeight() / 2) - (boxScore.getHeight() / 2);
		}		

		boxTime.setPosition( posX, posY);
		insertDrawable(boxTime);

		timeLabel = new Label(FONT_NUMBERS);
		insertDrawable(timeLabel);
		timeLabel.setText( "0:00" );
		timeLabel.setPosition(boxTime.getPosX() + (boxTime.getWidth() / 2) - (timeLabel.getWidth() / 2), boxTime.getPosY() + ( boxTime.getHeight() / 2 ) - (timeLabel.getHeight() / 2));

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		///caixa de mensagens
		gameMsg = new GameTextBox();
		insertDrawable(gameMsg);
		gameMsg.setPosition( 2 * BORDER_SIZE, 16 * (getHeight()/100));
		gameMsg.setSize(getWidth() - (4 * BORDER_SIZE), getHeight() - (4 * BORDER_SIZE) - (scoreBar.getHeight()) - ((timeBar != null) ? timeBar.getHeight() : 0 ));
		gameMsg.setVisible(false);

		order = new byte[ TOTAL_GIRLS_PHOTOS ];

		///preparacão para o jogo
		setNumPlayers((byte)1);
		prepareToLevel(1);
	}

	/**
	 *
	 * @return
	 */
	public final int getScore() {
		return score[currentPlayer];
	}

	/**
	 * Atualiza os campos de texto em relação ao score.
	 * Ocorre reposicionamento para acomodar o tamanho do texto do score.
	 */
	private final void refreshScoreLabel() {
		int oldWidth = scoreLabel.getWidth();
		String finalText = "";
		String shown = String.valueOf(scoreShown[currentPlayer]);
		for (int c = 0; c < MAX_NUMBER_CHARS - shown.length(); ++c) {
			finalText += "0";
		}
		finalText += shown;
		scoreLabel.setText(finalText);
		scoreLabel.setPosition(scoreLabel.getPosX() + oldWidth - scoreLabel.getWidth(), scoreLabel.getPosY());
	}

	/**
	 * Define qual é o jogador atual, ja atualizando tempo, combo e score do
	 * jogador
	 * @param n valor de 0 até numPlayers - 1 definindo o jogador
	 */
	public final void setCurrentPlayer(int n) {
		if (n >= numPlayers) {
			//#if DEBUG == "true"
				GameMIDlet.log("tentou-se setar o jogador atual com um valor inválido:" + n);
			//#endif
			return;
		}

		currentPlayer = (byte) n;
		refreshScoreLabel();
		setCombo(combo[currentPlayer]);
		if (numPlayers > 1)
			setRemainingTime(TIME_ROUND);
	}

	/**
	 * Incrementa a pontua��o e atualiza as vari�veis e labels correspondentes.
	 * @param diff varia��o na pontua��o (positiva ou negativa).
	 */
	private final void changeScore(int diff) {
		score[currentPlayer] += diff;
		scoreSpeed[currentPlayer].setSpeed((score[currentPlayer] - scoreShown[currentPlayer]) >> 1, false);
	}

	private final void changeTime(int diff) {
		timeRemaining[currentPlayer] = Math.max(timeRemaining[currentPlayer] - diff, 0);
		refreshTimeLabel();
	}

	/**
	 * Atualiza a pontuação exibida.
	 * @param equalize indica se a pontuação mostrada deve ser automaticamente igualada à pontuação real.
	 */
	private final void updateScore(int delta, boolean equalize) {
		if (scoreShown[currentPlayer] < score[currentPlayer]) {
			if (equalize) {
				scoreShown[currentPlayer] = score[currentPlayer];
			} else {
				scoreShown[currentPlayer] += scoreSpeed[currentPlayer].updateInt(delta);
				if (scoreShown[currentPlayer] >= score[currentPlayer]) {
					if (scoreShown[currentPlayer] > SCORE_SHOWN_MAX) {
						scoreShown[currentPlayer] = SCORE_SHOWN_MAX;
					}

					scoreShown[currentPlayer] = score[currentPlayer];
				}
			}
			refreshScoreLabel();
		}
	} // fim do método updateScore( boolean )

	/**
	 * Atualiza o jogo, dependendo do estado atual
	 * @param delta Quanto tempo desde o ultimo update.
	 */
	public final void update(int delta) {
		super.update(delta);

		stateTime += delta;
		updateScore(delta, false);
		switch (state) {
			case STATE_SHOW_SPECIAL_CARD_MESSAGE:
				if (stateTime - TIME_MESSAGE_TIME > 0)
					stateEnd();
				break;


			case STATE_PLAYER_TURN_MESSAGE:
				if (stateTime - TIME_MESSAGE_TIME > 0)
					setState(STATE_CHOOSING_CARD_1);
				break;

			case STATE_CHOOSING_CARD_1:
			case STATE_CHOOSING_CARD_2:
				if (cards[currentCard.x][currentCard.y] == null){
					//#if DEBUG == "true"
						GameMIDlet.log( "***cursor parou em carta nula!***" );
					//#endif
					placeCursorSafely();
				}
				updateTime(delta);
				if (stateTime - TIME_MESSAGE_TIME > 0)
					gameMsg.setVisible(false);
				break;

			case STATE_INTRO:
				setState(STATE_SHOWCARDS);

				break;

			case STATE_TEMPORARILY_SHOW_CARDS:
				if (stateTime > 3 * (TIME_CARD_FLIP * (remainingCards + 1)) / 2) {
					setState(STATE_CHOOSING_CARD_1);
				}
				break;


			case STATE_SHOWCARDS:
				if (stateTime > 3 * (TIME_CARD_FLIP * (remainingCards + 1)) / 2) {
					setState(STATE_PLAYER_TURN_MESSAGE);
				}
				break;

			case STATE_SHOW_PICTURE:
				break;

			case STATE_GAME_OVER:
				if ((stateTime / 2) > TIME_GAME_OVER_MIN) {
					stateEnd();
				}
				break;

			case STATE_LEVEL_COMPLETE:
				if (stateTime > TIME_LEVEL_COMPLETE_MIN) {
					stateEnd();
				}
				break;


			case STATE_SHOW_CARD_2:
				if (showPieces &&  (stateTime > TIME_CARD_FLIP * 4) && !girl.isVisible() ){
					girlCardPattern.setVisible( true );
					girl.setVisible( true );
				}
				break;
		}
	}

	/*
	 * Atualiza o indicador de tempo
	 */
	private final void refreshTimeLabel() {
		final int seconds = (timeRemaining[currentPlayer] / 1000) % 60;
		final int minutes = timeRemaining[currentPlayer] / 60000;

		final StringBuffer buffer = new StringBuffer();
		buffer.append(minutes);
		buffer.append(":");

		if (seconds < 10) {
			buffer.append('0');
		}
		buffer.append(seconds);

		timeLabel.setText(buffer.toString());
	}

	/**
	 * @param time O novo tempo restante a ser definido
	 */
	private final void setRemainingTime(int time) {
		timeRemaining[currentPlayer] = Math.max(0, time);
		refreshTimeLabel();
	}

	/**
	 * Atualiza quanto tempo o jogador ainda tem. Se o tempo acabar, troca o
	 * estado de jogo para fim de jogo.
	 * @param delta
	 */
	private final void updateTime(int delta) {
		setRemainingTime(timeRemaining[currentPlayer] - delta);
		if (timeRemaining[currentPlayer] <= 0) {
			setState( ( numPlayers > 1 ) ? STATE_NEXT_TURN : STATE_GAME_OVER );
		}
	}

/**
 *
 * @param angle
 * @param dx
 * @param dy
 * @return
 */
	private final boolean isInRangeOfAngle( byte angle, int dx, int dy ) {
		final int hip = NanoMath.sqrtInt( dx*dx + dy*dy );
		switch( angle ) {
			case DIRECTION_ANGLE_0:
				if( dx > 0 ) { //cosInt( 23 ); range > 1/4
					return NanoMath.divInt( dx, hip) > NanoMath.divInt( 70, 100 );
				}
			break;
			case DIRECTION_ANGLE_45:
				if( dx > 0 && dy < 0 ) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
					final int cos = NanoMath.divInt( dy, hip );
					return cos < NanoMath.divInt( -38, 100 ) && cos > NanoMath.divInt( -92, 100 ) ;
				}
			break;
			case DIRECTION_ANGLE_90:
				if( dy < 0 ) { //.sinInt( 67 ); range > 1/4
					return NanoMath.divInt( dy, hip) < NanoMath.divInt( -70, 100 );
				}
			break;
			case DIRECTION_ANGLE_135:
				if( dx < 0 && dy < 0 ) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
					final int cos = NanoMath.divInt( dy, hip);
					return cos < NanoMath.divInt( -38, 100 ) && cos > NanoMath.divInt( -92, 100 ) ;
				}
			break;
			case DIRECTION_ANGLE_180:
				if( dx < 0 ) { //NanoMath.cosInt( 157 ); range > 1/4
					return NanoMath.divInt( dx, hip) < NanoMath.divInt( -70, 100 );
				}
			break;
			case DIRECTION_ANGLE_225:
				if( dx < 0 && dy > 0 ) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
					final int cos = NanoMath.divInt( dy, hip);
					return cos > NanoMath.divInt( 38, 100 ) && cos < NanoMath.divInt( 92, 100 ) ;
				}
			break;
			case DIRECTION_ANGLE_270:
				if( dy > 0 ) { //NanoMath.sinInt( 247 ); range > 1/4
					return NanoMath.divInt( dy, hip) > NanoMath.divInt( 70, 100 ); 
				}
			break;
			case DIRECTION_ANGLE_315:
				if( dx > 0 && dy > 0 ) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
					final int cos = NanoMath.divInt( dy, hip);
					return cos > NanoMath.divInt( 38, 100 ) && cos < NanoMath.divInt( 92, 100 ) ;
				}
			break;
		}

		return false;
	}
	
	/**
	 *
	 * @param angle
	 * @return
	 */
	private final boolean moveCursorToDirection( byte angle ) {
		int bestDistance = Integer.MAX_VALUE;
		int x = -1;
		int y = -1;

		if( currentCard.x < cards.length && currentCard.y < cards[currentCard.x].length ) {
			for ( int r = 0; r < cards.length; r++ ) {
				for ( int c = 0; c < cards[r].length; c++ ) {
					if ( cards[r][c] != null && !( r == currentCard.x && c == currentCard.y ) ) {
						final int dx = cards[r][c].getPosX() - cards[currentCard.x][currentCard.y].getPosX();
						final int dy = cards[r][c].getPosY() - cards[currentCard.x][currentCard.y].getPosY();
						if( isInRangeOfAngle( angle, dx, dy ) ) {
							final int dist = cards[currentCard.x][currentCard.y].getPosition().distanceTo( cards[r][c].getPosition() );
							if( dist < bestDistance ) {
								bestDistance = dist;
								x = r;
								y = c;
							}
						}
					}
				}
			}
			if( x != -1 && y != -1 ) {
				setCurrentCard( x, y );
			}
		} else {
			selectRandomCard();
		}
		
		return false;
	}
	
	/**
	 *
	 * @return
	 */
	private final boolean moveCursorToCloserCard() {
		int bestDistance = Integer.MAX_VALUE;
		int x = -1;
		int y = -1;

		if( currentCard.x < cards.length && currentCard.y < cards[currentCard.x].length ) {
			for ( int r = 0; r < cards.length; r++ ) {
				for ( int c = 0; c < cards[r].length; c++ ) {
					if ( cards[r][c] != null && ( r != currentCard.x && c != currentCard.y ) ) {
						final int dist = cards[currentCard.x][currentCard.y].getPosition().distanceTo( cards[r][c].getPosition() );
						if( dist < bestDistance ) {
							bestDistance = dist;
							x = r;
							y = c;
						}
					}
				}
			}
			if( x != -1 && y != -1 ) {
				return setCurrentCard( x, y );
			}
		} else {
			return selectRandomCard();
		}

		return false;
	}
	
	/**
	 *
	 * @param p
	 * @return
	 */
	private final boolean moveCursorToCloserCard( Point p ) {
		int bestDistance = Integer.MAX_VALUE;
		int x = -1;
		int y = -1;

		for ( int r = 0; r < cards.length; r++ ) {
			for ( int c = 0; c < cards[r].length; c++ ) {
				if ( cards[r][c] != null ) {
					final int dist = p.distanceTo( cards[r][c].getPosition() );
					if( dist < bestDistance ) {
						bestDistance = dist;
						x = r;
						y = c;
					}
				}
			}
		}
		if( x != -1 && y != -1 ) {
			return setCurrentCard( x, y );
		}
		return placeCursorSafely();
	}

/**
 *
 * @return
 */
	private final boolean selectRandomCard() {
		int x = NanoMath.randInt( cards.length );
		int y = NanoMath.randInt( cards[x].length );
		int q = 0;
		
		while( cards[x][y] == null && q < 10 ) {
			x = NanoMath.randInt( cards.length );
			y = NanoMath.randInt( cards[x].length );
			q++;
		}
		
		if( cards[x][y] != null ) {
			return setCurrentCard( x, y );
		} else {
			return placeCursorSafely();
		}
	}

	/**
	 *
	 * @param row
	 * @param column
	 * @return
	 */
	private final boolean setCurrentCard( int row, int column ) {
		final int dstRow = row;
		final int dstColumn = column;
		try {
			// estou certo de que não é a primeira jogada OU a tentativa é diferente da seleção atual.
			if ( ( currentCard.x != -1 && currentCard.y != -1 ) || ( currentCard.x != row || currentCard.y != column ) ) {
				//faz com que a seleção faça um wrap, caso esteja indo além dos limites da mesa
				row = ( cards.length + row ) % cards.length;
				column = ( cards[row].length + column ) % cards[row].length;

				///estapa de busca?
				boolean searching = true;
				///numero de tentativas ja feitas
				int tries = 0;

				//se a carta buscada atual não estiver na mesa ou for igual a atual
				if ( cards[row][column] == null ) {
					if ( currentCard.x > dstRow ) {
						// antiga carta selecionada estava numa linha de baixo
						// se não houver carta na posição atual

						///percorrer todas as colunas até chegar na atual,
						///enquanto ainda estivermos procurando
						///decrementa de forma segura.
						//(adicionando o length -1 e usando modulo, de modo que
						///o modulo possa proteger contra numero negativo)
						for ( int r = row; r != currentCard.x && searching; r = ( r + cards.length - 1 ) % cards.length ) {
							///percorre a linha toda
							for ( int c = cards[r].length - 1; c >= 0; --c ) {
								if ( cards[r][c] != null ) {
									row = r;
									column = c;
									searching = false;
									break;
								}
							}
						}
					} else if ( currentCard.x < dstRow ) {
						// antiga carta selecionada estava numa linha de cima
						//percorre todas as linhas, buscando a mais proxima na direcao da carta desejada
						for ( int r = row; r != currentCard.x && searching; r = ( r + 1 ) % cards.length ) {
							//percorrendo todas as suas colunas em busca de uma carta válida
							for ( byte c = 0; c < cards[r].length; ++c ) {
								if ( cards[r][c] != null ) {
									row = r;
									column = c;
									searching = false;
									break;
								}
							}
						}
					} else {
						// antiga carta selecionada estava na mesma linha

						//numa coluna mais à esquerda
						if ( currentCard.y < dstColumn ) {
							//percorre todas as linhas, começando na atual
							//enquanto ainda estivermos procurando
							//e o numero de viradas não exceder o máximo
							for ( int r = currentCard.x; searching && ( r != currentCard.x || tries < MAX_CARD_FINDING_TRIES ); r = ( r + 1 ) % cards.length ) {

								//partindo da carta atual, percorre todas, dando voltas
								for ( int c = column; c != currentCard.y; c = ( c + 1 ) % cards[r].length ) {
									if ( cards[r][c] != null ) {
										row = r;
										column = c;
										searching = false;
										break;
									}
								}

								//demos mais uma virada na linha
								if ( r == currentCard.x ) {
									++tries;
								}

							}
						} else {
							//a carta selecionada anteriormente esta numa coluna à direita
							//decrementa de forma segura
							for ( int r = currentCard.x; searching && ( r != currentCard.x || tries < MAX_CARD_FINDING_TRIES ); r = ( r + cards.length - 1 ) % cards.length ) {
								if ( r == currentCard.x ) {
									++tries;
								}
								//decrementa as colunas de forma segura
								for ( int c = column; c != currentCard.y; c = ( c + cards[r].length - 1 ) % cards[r].length ) {
									if ( cards[r][c] != null ) {
										row = r;
										column = c;
										searching = false;
										break;
									}
								}
							}
						}
					}
				}
				currentCard.set( row, column );

				if ( getCurrentCard() == null ) {
					//#if DEBUG == "true"
					GameMIDlet.log( "carta nula inexperada (true )selecionada em r:" + row + ",c:" + column );
					//#endif
				} else {
					cursor.setTarget( getCurrentCard() );

					if ( ( state == STATE_CHOOSING_CARD_1 || state == STATE_CHOOSING_CARD_2 ) && !cards[currentCard.x][currentCard.y].isAnimating() ) {
						cards[currentCard.x][currentCard.y].animate();
					}
				}

				return true;
			}


			if ( cards[row][column] == null ) {
				//#if DEBUG == "true"
				GameMIDlet.log( "carta nula inexperada (false) em r:" + row + ",c:" + column );
				//#endif
			} else {
				//move o cursor
				cursor.setTarget( getCurrentCard() );

			}

			//Atualiza a carta atual
			currentCard.set( column, row );

			return false;
		} catch ( Exception e ) {
			return placeCursorSafely();
		}

	}

/**
 * Desenha a tela. Apenas necessário para quando vai exibir quais cartas ja foram descobertas
 * @param g
 */
	public final void paint( Graphics g ) {
		super.paint( g );

		switch ( state ) {
			case STATE_SHOW_CARD_2:
				if( showPieces && (stateTime > TIME_CARD_FLIP * 4) ) {
					for( int i = 0; i < collectedRegions.length && collectedRegions[i] != null; i++ ) {
						//girl.setViewport( collectedRegions[i] );

						girl.setViewport( collectedRegions[i] );

						if( (i + 1 >= collectedRegions.length ) || collectedRegions[i+1] == null ) {
							final int temp = (int) System.currentTimeMillis() % 1000;

							if( temp < 800 ) {
								final int mul = NanoMath.toInt( 153 * ( 1 - NanoMath.sinInt( (temp * 180) / 800 ) ) ) << 8;
								g.setColor( 0xff0000 | mul );
								g.drawRect( collectedRegions[i].x, collectedRegions[i].y, collectedRegions[i].width, collectedRegions[i].height );
								g.setColor( 0xff0000 | (mul + 0x3300) );
								g.drawRect( collectedRegions[i].x + 1, collectedRegions[i].y + 1, collectedRegions[i].width - 2 , collectedRegions[i].height - 2 );
								g.setColor( 0xff0000 | (mul + 0x6600)  );
								g.drawRect( collectedRegions[i].x + 2, collectedRegions[i].y + 2, collectedRegions[i].width - 4 , collectedRegions[i].height - 4 );
							}
						} else {
							girl.draw( g );
						}
					}
				}
			break;
		}
	}

	/**
	 * Toca musica de acordo com o estado do jogo
	 */
	public void playStateMusic() {
		switch( state ) {
			case STATE_LEVEL_COMPLETE:
				MediaPlayer.play( SOUND_MUSIC_LEVEL_COMPLETE, 1 );
			break;
			case STATE_GAME_OVER:
			case STATE_NEW_RECORD:
				MediaPlayer.play( SOUND_MUSIC_GAME_OVER, 1 );
			break;
			default:
				MediaPlayer.play( SOUND_MUSIC_GAME, MediaPlayer.LOOP_INFINITE );
		}
	}


	/***
	 * Troca o estado de jogo
	 * @param state Novo estado de jogo
	 */
	private final void setState(int state) {


		//#if DEBUG == "true"
			GameMIDlet.log("setState " + this.state + " -> " + state);
		//#endif

		this.state = (byte) state;
		stateTime = 0;
		switch (state) {
			case STATE_SHOW_SPECIAL_CARD_MESSAGE:
				//se for par, é boa. se for impar, é ruim
				boolean good = ( getCurrentCard().getKind() % 2 ) == BONUS_BONUS;
				gameMsg.setMessage( GameMIDlet.getText(good ? TEXT_BONUS : TEXT_PENALTY), GameMIDlet.getText(TEXT_SPECIAL_CARD_SWAP + (getCurrentCard().getKind() - 1)), good ? BONUS_BONUS : BONUS_PENALTY );
				gameMsg.setVisible( true );
				break;
				
			case STATE_NEXT_TURN:
				setCurrentPlayer((byte) (currentPlayer + 1) % numPlayers);
				//#if DEBUG == "true"
					GameMIDlet.log("jogador " + (currentPlayer + 1) + ", comece a jogar!");
				//#endif

				/// a cada 5 turnos, comecando pelo 4o
				if (turns % 5 == 3)
					createBonusCards();
				else if (turns % 5 == 4)
					///e no turno seguinte, limpa a situacao toda.
					makeAllCardsNormal();

				setState(STATE_PLAYER_TURN_MESSAGE);
				break;


			case STATE_SHOW_CARD_2: {
				showPieces = false;

				if ( getCurrentCard().equals( cards[lastCard.x][lastCard.y] ) ){
					showPieces = true;

					final Rectangle region = new Rectangle(getCurrentCard().getImageRegion());
					if( getWidth() > girl.getWidth() )
						region.x += girl.getPosX();
					if( getHeight() > girl.getHeight() )
						region.y += girl.getPosY();
					girl.setViewport( region );

					int i = 0;
					for ( ; i < collectedRegions.length && collectedRegions[i] != null; i++ ) {
						if ( region.x == collectedRegions[i].x && region.y == collectedRegions[i].y && region.width == collectedRegions[i].width && region.height == collectedRegions[i].height ) {
							break;
						}
					}

					if ( i < collectedRegions.length ) {
						collectedRegions[i] = region;
					}
				}

			}
			case STATE_SHOW_CARD_1: {

				if (!getCurrentCard().equals( cards[lastCard.x][lastCard.y] ) || (state == STATE_SHOW_CARD_1 ) )
					showPieces = false;

				cursor.setVisible( false );
				GameCard card = getCurrentCard();
				Point finalSize = new Point( card.getImageRegion().width, card.getImageRegion().height );
				Point finalPos = new Point( getWidth() >> 1, getHeight() >> 1 );
				cardsGroup.setDrawableIndex( card, cardsGroup.getUsedSlots() - 1 );
				card.setAnimation( finalPos, finalSize );
			}
			break;

			case STATE_PLAYER_TURN_MESSAGE:

				if (numPlayers > 1 || turns == 0){
					gameMsg.setMessage(GameMIDlet.getText( TEXT_PLAYER ) + (currentPlayer + 1), GameMIDlet.getText( TEXT_START_TURN ),2);
					gameMsg.setVisible(true);
				}

				break;

			case STATE_CHOOSING_CARD_1:
				lastCard = null;
				gameMsg.setVisible(false);
			case STATE_CHOOSING_CARD_2:
				cursor.setVisible( true );
				girlCardPattern.setVisible(false);
				girl.setVisible(false);
				break;

			case STATE_TEMPORARILY_SHOW_CARDS:
			case STATE_INTRO:
				girlCardPattern.setVisible(false);
				girl.setVisible(false);
				forceAllCardsShownProgressive();
				break;

			case STATE_SHOW_PICTURE:
				break;

			case STATE_GAME_OVER:
				playStateMusic();
//#if DEBUG == "true"
				GameMIDlet.log("GAME OVER man! game over!");
//#endif
				removeDrawable(cursor);
				for (int r = 0; r < cards.length; ++r) {
					for (int c = 0; c < cards[r].length; ++c) {
						if (cards[r][c] != null) {
							cards[r][c].setAnimation(new Point( ( getWidth() >> 1 ), ( getHeight() >> 1) ), new Point(0, 0));
						}
					}
				}

				gameMsg.setMessage( GameMIDlet.getText( TEXT_GAMEOVER_TITLE), GameMIDlet.getText( TEXT_GAMEOVER_MSG), 2);
				gameMsg.setVisible(true);
				break;

			case STATE_LEVEL_COMPLETE:
				playStateMusic();
				girl.setClipTest(false);
				girlCardPattern.setVisible( true );
				girl.setVisible(true);
				break;

//			case STATE_NEW_RECORD:
//				break;
		}
	}

	/**
	 * Algum estado foi encerrado.
	 */
	private final void stateEnd() {
		switch (state) {
			case STATE_SHOW_SPECIAL_CARD_MESSAGE:
						gameMsg.setVisible( false );
						GameCard selectedCard = getCurrentCard();
						switch ( selectedCard.getKind() ) {
							case GameCard.CARDTYPE_PULSATE_TWO:
								setState( STATE_CHOOSING_CARD_1);
								selectedCard.setKind( GameCard.CARDTYPE_NORMAL );
								forceAllCardsBackedAllAtOnce();
								pulsateTwoRandomCorrespondingCards();
								return;
							case GameCard.CARDTYPE_PULSATE_CORRESPONDING:
								forceAllCardsBackedAllAtOnce();
								setState( STATE_CHOOSING_CARD_1);
								selectedCard.setKind( GameCard.CARDTYPE_NORMAL );
								pulsateCorresponding();
								return;

							case GameCard.CARDTYPE_LOSE_TIME:
								setState( STATE_CHOOSING_CARD_1);
								selectedCard.setKind( GameCard.CARDTYPE_NORMAL );
								forceAllCardsBackedAllAtOnce();
								wrongCardsSelected();
								return;

							case GameCard.CARDTYPE_SHOW_CARDS:
								selectedCard.setKind( GameCard.CARDTYPE_NORMAL );
								setState(STATE_TEMPORARILY_SHOW_CARDS);
								return;

							case GameCard.CARDTYPE_SWAP: {
								int r1 = 0;
								int c1 = 0;
								int r2 = currentCard.x;
								int c2 = currentCard.y;
								int tries;

								tries = 20;
								forceAllCardsBackedAllAtOnce();

								do {
									r1 = NanoMath.randInt();
									if ( r1 < 0 ) {
										r1 = -r1;
									}
									r1 = r1 % cards.length;

									c1 = NanoMath.randInt();
									if ( c1 < 0 ) {
										c1 = -c1;
									}
									c1 = c1 % cards[r1].length;

									tries--;
									if ( tries == 0 ) {
										return;
									}

								} while ( cards[r1][c1] == getCurrentCard() || cards[r1][c1] == null );
								swapCards( r2, c2, r1, c1 );
								setCurrentCard( r1, c1 );
								printCards();
								selectedCard.setKind( GameCard.CARDTYPE_NORMAL );
								setState( STATE_CHOOSING_CARD_1);
								return;
							}
							case GameCard.CARDTYPE_LOSE_TURN:
								selectedCard.setKind( GameCard.CARDTYPE_NORMAL );
								forceAllCardsBackedAllAtOnce();								
								break;
						}
				setState( STATE_NEXT_TURN);
				break;

			case STATE_PLAYER_TURN_MESSAGE:
				gameMsg.setVisible(false);
				break;

			case STATE_CHOOSING_CARD_1:
				lastCard = null;
				break;

			case STATE_CHOOSING_CARD_2:
				break;

			case STATE_INTRO:
				break;

			case STATE_SHOW_CARD_1: {
				cursor.setVisible( true );
				girlCardPattern.setVisible( false );
				girl.setVisible( false );
				GameCard card = cards[lastCard.x][lastCard.y];
				card.setAnimation( card.getOriginalPosition(), card.getPreferedSize() );
				setState( STATE_CHOOSING_CARD_2 );

			}
			break;

			case STATE_SHOW_CARD_2:
				touchScreenLocked = true;
				cursor.setVisible( true );
				girlCardPattern.setVisible(false);
				girl.setVisible(false);
				GameCard card = cards[currentCard.x][currentCard.y];
				if (card != null && lastCard != null && card.equals(getCard(lastCard))) {

					//#if DEBUG == "true"
						GameMIDlet.log("ACERTOU");
					//#endif
						
					changeScore(SCORE_CARD * combo[currentPlayer]);
					setCombo(combo[currentPlayer] + 1);
					boolean changed = true;
					Point curr = new Point(currentCard);
					cards[lastCard.x][lastCard.y].shrinkVanish();
					cards[curr.x][curr.y].shrinkVanish();

					final Point p = cards[currentCard.x][currentCard.y].getPosition();
					
					cards[lastCard.x][lastCard.y] = null;
					cards[curr.x][curr.y] = null;
					remainingCards -= 2;
					changed = moveCursorToCloserCard( p );
				} else {
					card.flip();
					cards[lastCard.x][lastCard.y].flip();

					//#if DEBUG == "true"
						GameMIDlet.log("ERROU");
					//#endif

					card.setAnimation(card.getOriginalPosition(), card.getPreferedSize());
					wrongCardsSelected();
				}

				++turns;
				touchScreenLocked = false;
				if (getRemainingCards() == 0) {
					setState(STATE_LEVEL_COMPLETE);
				} else if (timeRemaining[currentPlayer] > 0) {
					setState(STATE_NEXT_TURN);
				} else {
					if (timeRemaining[currentPlayer] <= 0) {
						setState( ( numPlayers > 1 ) ? STATE_NEXT_TURN : STATE_GAME_OVER );
					}
				}

				break;

			case STATE_SHOW_PICTURE:
				if (getRemainingCards() > 0) {
					setState(STATE_PLAYER_TURN_MESSAGE);
				} else {
					setState(STATE_GAME_OVER);
				}
				break;


//			case STATE_NEW_RECORD:
			case STATE_GAME_OVER:
				GameMIDlet.setScreen( ( GameMIDlet.onGameOver( this ) ) ? SCREEN_NEW_RECORD_MENU : SCREEN_GAME_OVER_MENU );
				break;

			case STATE_LEVEL_COMPLETE:
				try {
					changeScore( timeRemaining[currentPlayer] / 100 );
					prepareToLevel(level + 1);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				break;
		}
	}

/**
 * Define qual é o valor de combo atual para o jogador atual
 * @param combo Novo combo
 */
	private final void setCombo(int combo) {

		if (combo > MAX_COMBO)
			return;
		
		this.combo[currentPlayer] = (short) combo;
		comboLabel.setText(String.valueOf(this.combo[currentPlayer]) + "x");

//#if SCREEN_SIZE == "SMALL"
//# 		comboLabel.setPosition( - 1 + ( - comboLabel.getWidth() >> 1 ) + multiplierBall.getPosX() + (multiplierBall.getWidth() >> 1), ( - comboLabel.getHeight() >> 1 ) + multiplierBall.getPosY() + (multiplierBall.getHeight() >> 1));
//#else
	comboLabel.setPosition(( - comboLabel.getWidth() >> 1 ) + multiplierBall.getPosX() + (multiplierBall.getWidth() >> 1), ( - comboLabel.getHeight() >> 1 ) + multiplierBall.getPosY() + (multiplierBall.getHeight() >> 1));
//#endif
		//#if DEBUG == "true"
			GameMIDlet.log("COMBO:" + combo);
		//#endif
	}

/**
 *
 * @return Se precisa que um record seja enviado
 */
	public final boolean isRecordSent() {
		return recordSent;
	}

/**
 * Define que um score ja esta sendo enviado
 */
	public final void setRecordSent() {
		recordSent = true;
	}


	/**
	 * Inicializa um novo nível.
	 * O estado de jogo antigo é todo jogado fora. Até porque o tamanho do
	 * tabuleiro é dependente do nível.
	 * Ao fim da inicialização, retorna ao jogo pro estado de "INTRO"
	 * @param level Nível a ser jogado
	 * @throws java.lang.Exception
	 */
	public final void prepareToLevel(int level) throws Exception {
		GameMIDlet.gc();

		if( level == 1 ) {
			recordSent = false;

			for( int i = 0; i < TOTAL_GIRLS_PHOTOS; i++ ) {
				order[ i ] = ( byte ) i;
			}
			//#if DEBUG == "true"
				GameMIDlet.log( "Created order array" );
			//#endif

			byte temp, temp2, max = ( byte ) NanoMath.min( GameMIDlet.getMaxLevelReached() - 1, TOTAL_GIRLS_PHOTOS );
			//#if DEBUG == "true"
				GameMIDlet.log( "Created temp variables" );
			//#endif

			for( byte i = 0; i < max; i++ ) {
				temp = ( byte ) ( ( NanoMath.randInt( max - i ) ) + i );
				temp2 = order[ i ];
				order[ i ] = order[ temp ];
				order[ temp ] = temp2;
			}
			//#if DEBUG == "true"
				GameMIDlet.log( "Sorted order array" );
			//#endif
		}

		this.level = (short) level;
		turns = 0;
		refreshScoreLabel();

		//no modo 2 jogadores, seria muito facil destravar todas as fotos
		if (numPlayers < 2) {
			GameMIDlet.proposeMaxLevel(level);
		}

		final int difficultyLevel = Math.min(level, MAX_LEVEL);

		String filename = String.valueOf( difficultyLevel <= TOTAL_GIRLS_PHOTOS ? order[ difficultyLevel - 1 ] : NanoMath.randInt( TOTAL_GIRLS_PHOTOS ) ) + ".jpg";
		GameMIDlet.log("girl:" + filename);

		if (girl != null) {
			girl.setVisible( false );
			removeDrawable(girl);
		}
		girl = null;
		GameMIDlet.gc();

		if (girlCardPattern != null) {
			girlCardPattern.setVisible( false );
			removeDrawable(girlCardPattern);
		}
		girlCardPattern = new Pattern(0);
		girl = new DrawableImage( PATH_GIRLS + filename );
		GameMIDlet.log( "girl image loaded");
//		girlCardPattern.setSize(girl.getSize());
		girlCardPattern.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
		girl.setClipTest(true);
		insertDrawable(girlCardPattern);
		insertDrawable(girl);
		girl.setVisible(false);
		girlCardPattern.setVisible(false);
		girl.setPosition(ScreenManager.SCREEN_HALF_WIDTH - ( girl.getWidth() >> 1 ), ScreenManager.SCREEN_HALF_HEIGHT - ( girl.getHeight() >> 1 ));
//		girlCardPattern.setPosition(girl.getPosition());
		GameMIDlet.log( "preparing to load the pattern");
		if (GameMIDlet.isLowMemory())
			girlCardPattern.setFillColor(0x660000);
		else{
			girlCardPattern.setFill(new DrawableImage(PATH_CARD_STATES + "red_card.png"));
			GameMIDlet.log( "background pattern loaded");
		}

		remainingCards = (byte) NanoMath.lerpInt(CARDS_PER_LEVEL_INITIAL, CARDS_PER_LEVEL_FINAL, difficultyLevel * 100 / MAX_LEVEL);

		///se o numero de cartas for impar, faz em par.
		if ((remainingCards & 1) != 0) {
			remainingCards--;
		}

		final int ROWS = NanoMath.sqrtInt(remainingCards);
		final int COLUMNS = ROWS + NanoMath.sqrtInt(remainingCards - (ROWS * ROWS));

		// remove as cartas antigas, caso existam
		cursor.setTarget(null);
		cardsGroup.removeAllDrawables();
		cards = new GameCard[ROWS][];
		GameMIDlet.log( "rows created");
		int remainingIndexes = remainingCards;

		int cardHeight = NanoMath.toInt(NanoMath.divInt(ScreenManager.SCREEN_HEIGHT - scoreBar.getHeight() - (timeBar != null ? timeBar.getHeight() : 0), ROWS + 1));
		int cardWidth = NanoMath.toInt(NanoMath.divInt(ScreenManager.SCREEN_WIDTH, COLUMNS + 1));

		int fp_ratio;

		if (CARD_DEFAULT_HEIGHT > CARD_DEFAULT_WIDTH) {
			fp_ratio = NanoMath.divInt(CARD_DEFAULT_WIDTH, CARD_DEFAULT_HEIGHT);
			cardWidth = NanoMath.min( NanoMath.mulFixed(fp_ratio, cardHeight) , cardWidth);
		} else {
			fp_ratio = NanoMath.divInt(CARD_DEFAULT_HEIGHT, CARD_DEFAULT_WIDTH);
			cardHeight = NanoMath.min( NanoMath.mulFixed(fp_ratio, cardWidth) , cardHeight);
		}
		
		for (byte r = 0; r < ROWS; ++r) {
			cards[r] = new GameCard[ NanoMath.min(COLUMNS, remainingIndexes) ];
			GameMIDlet.log( "column created");
			for (byte c = 0; c < COLUMNS && remainingIndexes > 0; ++c) {
				GameMIDlet.gc();
				--remainingIndexes;
				final GameCard card = new GameCard(girl.getImage());
				GameMIDlet.log( "card "+r+","+c+" created");
				cards[r][c] = card;
				cards[r][c].setPosition(getWidth() >> 1, getHeight() >> 1);
				card.setPreferedSize( cardWidth, cardHeight );
				cardsGroup.insertDrawable(card);
			}
		}
		GameMIDlet.log( "cards loaded");
		redistributeCardIndexes();
		GameMIDlet.log( "card indexes created");
		printCards();
		GameMIDlet.gc();
		dealCards();
		GameMIDlet.log( "cards dealt");
		setCurrentPlayer(0);

		if (numPlayers == 1)
			setRemainingTime(TIME_GAME);

		setState(STATE_INTRO);
		GameMIDlet.log( "intro started");
		playStateMusic();
		selectRandomCard();
	}

	/**
	 * Realiza a troca de posicão de duas cartas
	 * @param r1 linha 1
	 * @param c1 coluna 1
	 * @param r2 linha 2
	 * @param c2 coluna 2
	 */
	private void swapCards(int r1, int c1, int r2, int c2) {
		//#if DEBUG == "true"
			GameMIDlet.log("swapCards(" + r1 + "," + c1 + "," + r2 + "," + c2 + ");");
		//#endif

		GameCard card1;
		GameCard card2;

		card1 = cards[r1][c1];
		card2 = cards[r2][c2];

		if (!card1.isFlipped()) {
			card1.flip();
		}

		if (!card2.isFlipped()) {
			card2.flip();
		}

		card1.swapWith(card2);
		card2.swapWith(card1);
		cards[r1][c1] = card2;
		cards[r2][c2] = card1;
	}

	/**
	 * Redistribui indices de cartas, alem de cartas especiais
	 */
	private void redistributeCardIndexes() {
		byte r;
		byte c;
		byte remainingIndexes = remainingCards;

		numIndexes = (byte) (remainingIndexes >> 1);

		collectedRegions = new Rectangle[numIndexes];

		final byte[] indexes = new byte[remainingCards];
		for (byte i = 0; i < remainingCards; ++i) {
			indexes[i] = (byte) (i >> 1);
		}

		for (r = 0; r < cards.length; ++r) {
			for (c = 0; c < cards[r].length && remainingIndexes > 0; ++c) {
				final GameCard card;
				card = cards[r][c];

				if (card == null) {
					continue;
				}

				// sorteia um índice da carta
				final int rand = NanoMath.randInt(remainingIndexes);
				card.setIndex(indexes[rand]);
				--remainingIndexes;
				GameMIDlet.log("cards[" + r + "][" + c + "]:" + indexes[rand] + " rand:" + rand + " remainingIndexes:" + remainingIndexes);
				indexes[rand] = indexes[remainingIndexes];
			}
		}

		bonusToAppear = ( byte ) ( ( level / 5 ) + 1 );
	}

	private void makeAllCardsNormal() {
		for ( int r = 0; r < cards.length; ++r )
			for ( int c = 0; c < cards[r].length; ++c )
				if ( cards[r][c] != null)
					cards[r][c].setKind( GameCard.CARDTYPE_NORMAL);
	}


	private void createBonusCards(){
		byte r = - 1;
		byte c = - 1;

		///distribui bonus e penalidades...
		//primeiras 10 fases, so com uma, depois aumenta
		if (bonusToAppear-- > 0){
			//a cada 3 fases...uma tem carta especial
			if ((numIndexes % 3) == 0){
				do {
					r = (byte) NanoMath.randInt();
					if (r < 0) {
						r = (byte) -r;
					}
					r = (byte) (r % cards.length);

					c = (byte) NanoMath.randInt();
					if (c < 0) {
						c = (byte) -c;
					}
					c = (byte) (c % cards[r].length);
				} while (cards[r][c] == null);

				byte rnd = ( byte ) NanoMath.randInt();

				if (rnd < 0)
					rnd = ( byte ) -rnd;
				///E ainda assim, tem a possibilidade da carta especial ser
				///CARDTYPE_NORMAL, ou seja, carta comum, apesar da chance ser
				///de 1/5.
				rnd = ( byte ) ( rnd  % ( GameCard.CARDTYPE_PULSATE_CORRESPONDING + 1 ) );

				if (rnd == GameCard.CARDTYPE_LOSE_TURN && numPlayers < 2)
					return;				
				
				cards[r][c].setKind( rnd );

				//#if DEBUG == "true"
					GameMIDlet.log( "coloquei uma carta especial em "+r+","+c + " de tipo:"+cards[r][c].getKind() );
				//#endif
			}
		}

	}

	/**
	 * funçao de debug para imprimir as cartas
	 */
	private final void printCards() {
		//#if DEBUG == "true"
		GameMIDlet.log("Cartas (" + remainingCards + "):");
		for (byte r = 0; r < cards.length; ++r) {
			for (byte c = 0; c < cards[r].length; ++c) {
				if (cards[r][c] != null) {
					System.out.print(cards[r][c].getIndex() + " ");
				} else {
					System.out.print("x ");
				}
			}
			System.out.println();
		}
		//#endif
	}

	/**
	 * Re-ajusta a posição das cartas.
	 */
	private final void dealCards() {
		if (cards != null) {
			final int fpCardHeight = NanoMath.divInt(getHeight() - scoreBar.getHeight() - (timeBar != null ? timeBar.getHeight() : 0), cards.length + 1);
			final int fp_VerticalCardSpacing = fpCardHeight >> 3;
			final int cardHeight = NanoMath.toInt( fpCardHeight);
			final int utilHeight = (getHeight() - scoreBar.getHeight() - (timeBar != null ? timeBar.getHeight() : 0));
			int initialY = NanoMath.toFixed((7 * cardHeight / 16) + (timeBar != null ? timeBar.getHeight() : 0) + (utilHeight - cardHeight  * cards.length) / 2);

			for (int r = 0, fp_y = initialY; r < cards.length; ++r, fp_y += fpCardHeight + fp_VerticalCardSpacing) {

				int total = 1;

				for (int c = 0; c < cards[r].length; ++c) {
					if (cards[r][c] != null) {
						++total;
					}
				}

				final int fp_CardWidth = NanoMath.divInt(getWidth(), total);
				final int fp_HorizontalCardSpacing = fp_CardWidth >> 4;
				final int initialX = NanoMath.toFixed((getWidth() - (NanoMath.toInt(fp_CardWidth + fp_HorizontalCardSpacing ) * (cards[r].length-1))) >> 1);

				for (int c = 0, fp_x = initialX; c < cards[r].length; ++c, fp_x += fp_CardWidth + fp_HorizontalCardSpacing) {
					if (cards[r][c] != null) {
						cards[r][c].setOriginalPosition( new Point(NanoMath.toInt(fp_x), NanoMath.toInt(fp_y)) );
						cards[r][c].setAnimation(cards[r][c].getOriginalPosition(), cards[r][c].getSize());
					}
				}
			}

			Rectangle rect;
			int cardIndex = 0;
			int verticalSlices;
			int y;
			int x;
			int w;
			int h;

			verticalSlices = NanoMath.sqrtInt( numIndexes );
			int[] horizontalSlices = new int[verticalSlices];
			int q = numIndexes - ( verticalSlices * verticalSlices );
			for( int i = 0; i < horizontalSlices.length; i++ ) {
				horizontalSlices[i] = verticalSlices;
				if( q > 0 ) {
					q--;
					horizontalSlices[i]++;
				}
			}
			for( int i = 0; i < horizontalSlices.length; i++ ) {
				if( q > 0 ) {
					q--;
					horizontalSlices[i]++;
				} else {
					break;
				}
			}

			int offsetX = ( getWidth() - girl.getWidth() ) >> 1;
			int offsetY = ( getHeight() - girl.getHeight() ) >> 1;
			h = ( NanoMath.min( girl.getHeight(), getHeight() ) ) / verticalSlices;

			for (int r = 0; r < cards.length; ++r) {
				for (int c = 0; c < cards[r].length; ++c) {
					if (cards[r][c] == null) {
						continue;
					}
					
					rect = new Rectangle();
					cardIndex = cards[r][c].getIndex();

					y = 0;
					int accId = cardIndex;
					for( int i = 0; i < horizontalSlices.length; i++ ) {
						if( accId - horizontalSlices[i] >= 0) {
							accId -= horizontalSlices[i];
							y++;
						} else {
							break;
						}
					}
					x = accId;

					//tem que ajustar para que a ultima preencha todo o resto.
					w = ( NanoMath.min( girl.getWidth(), getWidth() ) ) / horizontalSlices[y];

					rect.x = (x * w);
					rect.y = (y * h);
					rect.width = w;
					if( horizontalSlices[y] < (x + 1) ) {
						rect.width += ( NanoMath.min( girl.getWidth(), getWidth() ) % horizontalSlices[y] );
					}
					rect.height = h;
					cards[r][c].setImageOffset( new Point( offsetX, offsetY ) );
					cards[r][c].setImageRegion(rect);
				}
			}
		}
	}

	/**
	 * Tratamento de pressionamento de teclas, de acordo com o estado de jogo
	 * @param key keycode da tecla
	 */
	public final void keyPressed(int key) {
		switch (state) {
			case STATE_PLAYER_TURN_MESSAGE:
				switch (key) {
					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
						break;
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						GameMIDlet.setScreen(SCREEN_PAUSE);
						break;
				}
				break;

			case STATE_CHOOSING_CARD_1:
			case STATE_CHOOSING_CARD_2:
				switch (key) {
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_RIGHT:
						moveCursorToDirection( DIRECTION_ANGLE_0 );
					break;
					
					case ScreenManager.KEY_NUM3:
						moveCursorToDirection( DIRECTION_ANGLE_45 );
					break;
						
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_UP:
						moveCursorToDirection( DIRECTION_ANGLE_90 );
					break;
					
					case ScreenManager.KEY_NUM1:
						moveCursorToDirection( DIRECTION_ANGLE_135 );
					break;

					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_LEFT:
						moveCursorToDirection( DIRECTION_ANGLE_180 );
					break;
					
					case ScreenManager.KEY_NUM7:
						moveCursorToDirection( DIRECTION_ANGLE_225 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_DOWN:
						moveCursorToDirection( DIRECTION_ANGLE_270 );
					break;
					
					case ScreenManager.KEY_NUM9:
						moveCursorToDirection( DIRECTION_ANGLE_315 );
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_FIRE:
						
						GameMIDlet.log("x:" + currentCard.x + "y:" + currentCard.y + "currentCard: " + currentCard + " -> " + getCurrentCard().getIndex() + " / " + (lastCard == null ? "null" : String.valueOf(getCard(lastCard).getIndex())));
						if (getCurrentCard().getKind() != GameCard.CARDTYPE_NORMAL)
						{
							setState( STATE_SHOW_SPECIAL_CARD_MESSAGE);
							return;
						}


						if (state == STATE_CHOOSING_CARD_1) {
							lastCard = new Point(currentCard);
							getCurrentCard().flip();
							setState(STATE_SHOW_CARD_1);
							final boolean changed = moveCursorToCloserCard();
						} else if (!currentCard.equals(lastCard)) {
								getCurrentCard().flip();
								setState(STATE_SHOW_CARD_2);
								return;
							}

						break;

					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						GameMIDlet.setScreen(SCREEN_PAUSE);
						break;
				}
				break;

			case STATE_SHOW_CARD_1:
			case STATE_SHOW_CARD_2:
				if (stateTime >= TIME_CARD_MIN) {
					stateEnd();
					return;
				}
				switch (key) {
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						GameMIDlet.setScreen(SCREEN_PAUSE);
						break;
				}
				break;

			case STATE_INTRO:
				break;

			case STATE_SHOW_PICTURE:
			case STATE_TEMPORARILY_SHOW_CARDS:
			case STATE_SHOWCARDS:
				switch (key) {
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_FIRE:
						break;
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						GameMIDlet.setScreen(SCREEN_PAUSE);
						break;
				}
				break;

			case STATE_LEVEL_COMPLETE:
				switch (key) {
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_SOFT_LEFT:
						break;
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						GameMIDlet.setScreen(SCREEN_PAUSE);
						break;
				}
				break;
		}
	}

	/**
	 * Obtem a referencia da carta na posicão passada
	 * @param index
	 * @return
	 */
	private final GameCard getCard(Point index) {
		if (index == null) {
			return null;
		} else {
			return cards[index.x][index.y];
		}
	}

	/**
	 * Carta atualmente marcada pelo cursor
	 * @return
	 */
	private final GameCard getCurrentCard() {
		try {
			return cards[currentCard.x][currentCard.y];
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Quantidade de cartas ainda faltando
	 * @return
	 */
	private final byte getRemainingCards() {
		return remainingCards;
	}

	/**
	 * Tratamento de soltura de tecla
	 * @param key keycode da tecla solta
	 */
	public final void keyReleased(int key) {
	}

	/**
	 * Notificação de ocultação do elemento gráfico
	 * @param deviceEvent
	 */
	public final void hideNotify(boolean deviceEvent) {
	}
	

	/**
	 * Notificação de exibição do elemento gráfico
	 * @param deviceEvent
	 */
	public void showNotify( boolean deviceEvent ) {
		if( !MediaPlayer.isPlaying() )
			playStateMusic();
	}
	

	/**
	 * Define um novo tamanho para o tabuleiro de jogo e re-ajusta as cartas.
	 * @param width
	 * @param height
	 */
	public final void setSize(int width, int height) {
		super.setSize(width, height);
		cardsGroup.setSize(size);
		dealCards();
	}

	/**
	 * Mudança de tamanho do elemento visual do tabuleiro. Se o tamanho for
	 * diferente do atual, ocorre uma re-organização dos elementos visuais
	 * @param width
	 * @param height
	 */
	public final void sizeChanged(int width, int height) {
		if (width != getWidth() || height != getHeight()) {
			setSize(width, height);
		}
	}

	//#if TOUCH == "true"
	/**
	 * Caso o ponteiro seja arrastado.
	 * @param x
	 * @param y
	 */
	public final void onPointerDragged(int x, int y) {
	}

	/**
	 * Caso o ponteiro seja pressionado.
	 * @param x
	 * @param y
	 */
	public final void onPointerPressed( int x, int y ) {
		if (touchScreenLocked)
			return;
				

		switch ( state ) {
			case STATE_CHOOSING_CARD_1:
			case STATE_CHOOSING_CARD_2:
				
					

				for ( int r = 0; r < cards.length; ++r ) {
					for ( int c = 0; c < cards[r].length; ++c ) {
						if ( cards[r][c] != null && !cards[r][c].isAnimating() && cards[r][c].contains( x, y ) ) {
							if ( cards[r][c] == getCurrentCard() ) {
								keyPressed( ScreenManager.KEY_FIRE );
							} else {
								setCurrentCard( r, c );
							}
						}
					}
				}
				break;

			default:
				keyPressed( ScreenManager.KEY_FIRE );
				break;
		}
	}

	/**
	 * Caso o ponteiro seja solto.
	 * @param x
	 * @param y
	 */
	public final void onPointerReleased(int x, int y) {
	}
	//#endif

	/**
	 * Faz com que todas as cartas se apresentem progressivamente
	 */
	private void forceAllCardsShownProgressive() {
		int amount = 0;
		int baseTime = 0;
		for (int r = 0; r < cards.length; ++r) {
			for (int c = 0; c < cards[r].length; ++c) {				
				if (cards[r][c] != null) {
					amount++;
					if (!cards[r][c].isFlipped())
						cards[r][c].flip();
					cards[r][c].presentate(baseTime + (TIME_CARD_FLIP * (amount)));
				}
			}
		}
	}

	/**
	 * Força todas as cartas viradas imediatamente
	 */
	private void forceAllCardsBackedAllAtOnce() {
		for (int r = 0; r < cards.length; ++r) {
			for (int c = 0; c < cards[r].length; ++c) {
				if (cards[r][c] != null && !cards[r][c].isFlipped()) {
					cards[r][c].flip();
				}
			}
		}
	}

	/**
	 * @return o numero de jogadores em jogo
	 */
	public byte getNumPlayers() {
		return numPlayers;
	}

	/**
	 * @param define o numero de jogadores
	 */
	public void setNumPlayers(byte numPlayers) {
		this.numPlayers = numPlayers;
		scoreSpeed = new MUV[numPlayers];
		score = new int[numPlayers];
		combo = new short[numPlayers];
		scoreShown = new int[numPlayers];
		timeRemaining = new int[numPlayers];

		for ( int c = 0; c < numPlayers; ++c){
			combo[c] = 1;
			timeRemaining[c] = (numPlayers > 1) ? TIME_ROUND : TIME_GAME;
			scoreSpeed[c] = new MUV();
		}
		
		refreshTimeLabel();
		setCombo(1);
	}

	/**
	 * Tenta recuperar de um cursor posicionado em local inválido
	 * @return se a recuperação deu certo ou não
	 */
	public boolean placeCursorSafely()	{
		for (int r = 0; r < cards.length; ++r) {
			for (int c = 0; c < cards[r].length; ++c) {
				if (cards[r][c] != null) {
					GameMIDlet.log( "> forcei cursor em ("+r+","+c+")" );
					setCurrentCard( r, c );
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * retorna o numero do nivel atual
	 * @return short do numero de nivel atual
	 */
	public int getLevel() {
		return this.level;
	}
	

	/**
	 * caso uma jogada errada tenha acontecido.
	 * também usado para carta de penalidade
	 */
	private void wrongCardsSelected() {
		setCombo( 1 );
		changeTime( TIME_PENALTY_MISS );
		MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
	}

	/**
	 * Faz com quem dois pares pulsem
	 */
	public void pulsateTwoRandomCorrespondingCards() {
		int r0	  = -1;
		int c0	  = -1;
		int r1	  = -1;
		int c1	  = -1;
		int tries =  0;
		
		for ( int n = 0; n < 2; ++n ) {
			do {
				r0 = ( byte ) NanoMath.randInt();
				if ( r0 < 0 ) {
					r0 = ( byte ) -r0;
				}
				r0 = ( byte ) ( r0 % cards.length );

				c0 = ( byte ) NanoMath.randInt();
				if ( c0 < 0 ) {
					c0 = ( byte ) -c0;
				}
				c0 = ( byte ) ( c0 % cards[r0].length );
			} while ( cards[r0][c0] == null );


			for ( int r = 0; r < cards.length; ++r ) {
				for ( int c = 0; c < cards[r].length; ++c ) {
					if ( cards[r][c] != null && cards[r][c].equals( cards[r0][c0] ) ) {
						c1 = c;
						r1 = r;
						c = cards[r].length;
						r = cards.length;
						break;
					}
				}
			}
			if (
					r0 == -1														||
					c0 == -1														||
					r1 == -1														||
					c1 == -1														||
					cards[r0][c0] == null											||
					cards[r1][c1] == null											||
					(r1 == r0 && c1 == c0)											||
					cards[r1][c1].isAnimating()										||
					(remainingCards > 4 && cards[r0][c0].equals( getCurrentCard())) ||
					(remainingCards > 4 && cards[r1][c1].equals( getCurrentCard()))
					) {

				///nao podemos continuar tentando para sempre...
				if (++tries <= MAX_CARD_FINDING_TRIES)
					--n;

				//#if DEBUG == "true"
					GameMIDlet.log( "tentando novamente" );
				//#endif
				continue;
			}

			cards[r1][c1].animate(- n * TIME_PULSATE_BONUS );
			cards[r0][c0].animate(- n * TIME_PULSATE_BONUS );
			//#if DEBUG == "true"
				GameMIDlet.log( "pulsando carta em "+r0+","+c0 );
				GameMIDlet.log( "pulsando carta em "+r1+","+c1 );
			//#endif
		}

	}
/**
 * pulsa a carta correspondente a carta atual
 */
	public void pulsateCorresponding()	{
		for (int r = 0; r < cards.length; ++r) {
			for (int c = 0; c < cards[r].length; ++c) {
				if (cards[r][c] != null && cards[r][c].equals( getCurrentCard())) {
					cards[r][c].animate();
				}
			}
		}
	}

}
