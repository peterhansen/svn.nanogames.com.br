/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;
import br.com.nanogames.components.userInterface.form.TextBox;

/**
 *
 * @author Caio
 */
public interface Selectable {
	public void select();
	public void unselect();
	public void keyPressed( int key );
	public TextBox textBox();
	public boolean contains( int x, int y );
}