/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Caio
 */
public abstract class BasicScreen  extends UpdatableGroup implements Constants, ScreenListener, KeyListener, Updatable
//#if TOUCH == "true"
	, PointerListener
//#endif
{
	protected final GameMIDlet midlet;
	protected final int screenID;

	private final MarqueeLabel screenTitle;
	private final Pattern titlePattern;
	protected final DrawableGroup titleGroup;

	private final Pattern bottomBarPattern;
	private final boolean hasBottomBar;
	private final boolean hasTitle;

	protected final Rectangle bkgArea = new Rectangle();

	public BasicScreen( int slots, GameMIDlet midlet, int screen, boolean hasTittle, String tittle, boolean hasBottomBar ) throws Exception {
		super( slots + 2 );

		screenID = screen;
		this.midlet = midlet;
		this.hasTitle = hasTittle;
		this.hasBottomBar = hasBottomBar;
		

		final DrawableImage img = new DrawableImage( PATH_IMAGES + "menu_pattern.png" );
		final DrawableImage img2 = new DrawableImage( img );
		img2.setTransform( TRANS_MIRROR_V );
		img.setPosition( 0, img.getHeight() );
		final DrawableGroup bkg= new DrawableGroup( 2 );
		bkg.setSize( img.getWidth(), img.getHeight() << 1 );
		bkg.insertDrawable( img );
		bkg.insertDrawable( img2 );


		if( hasTittle ) {
			titlePattern = new Pattern( bkg );
			insertDrawable( titlePattern );
			titleGroup = new DrawableGroup( 1 );

			screenTitle = new MarqueeLabel( FONT_TITLE, tittle );
			screenTitle.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			screenTitle.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			screenTitle.defineReferencePixel( ANCHOR_CENTER );
			titleGroup.insertDrawable( screenTitle );
		} else {
			titlePattern = null;
			titleGroup = null;
			screenTitle = null;
		}

		if( hasBottomBar ) {
			bottomBarPattern = new Pattern( bkg );
			bottomBarPattern.setSize( 0, MENU_BOTTOM_BAR_HEIGHT ); // TODO retirar constante mágica
			insertDrawable( bottomBarPattern );
		} else {
			bottomBarPattern = null;
		}
	}


	public final void setTitle( String text ) {
		screenTitle.setText( text );
		screenTitle.setTextOffset( ( screenTitle.getWidth() - screenTitle.getTextWidth() ) >> 1 );
	}


	public void setSize ( int width, int height ) {
		super.setSize( width, height );
		
		bkgArea.x = getPosX();
		bkgArea.width = width;

		if( hasTitle ) {
			titleGroup.setSize( width, screenTitle.getHeight() + ( SAFE_MARGIN << 1 ) );
			titlePattern.setSize( titleGroup.getSize() );
			bkgArea.y = titleGroup.getHeight();
			screenTitle.setPosition( SAFE_MARGIN, SAFE_MARGIN );
			screenTitle.setSize( titleGroup.getWidth() - ( SAFE_MARGIN << 1 ), screenTitle.getHeight() );
			screenTitle.setTextOffset( ( screenTitle.getWidth() - screenTitle.getTextWidth() ) >> 1 );
		} else {
			bkgArea.y = 0;
		}

		if( hasBottomBar ) {
			bottomBarPattern.setSize( width, bottomBarPattern.getHeight() );
			bottomBarPattern.setPosition( 0, height - bottomBarPattern.getHeight() );
			bkgArea.height = bottomBarPattern.getPosY() - bkgArea.y;
		} else {
			bkgArea.height = height - bkgArea.y;
		}

		GameMIDlet.setBkgViewport( new Rectangle( bkgArea.x, bkgArea.y, bkgArea.width, bkgArea.height ) );
	}

	public void exit() {
		midlet.onChoose( null, screenID, TEXT_EXIT );
	}


	public final void draw ( Graphics g ) {
		super.draw( g );

		titleGroup.draw( g );

		drawFrontLayer( g );
	}


	public abstract void drawFrontLayer( Graphics g );


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public void keyPressed( int key ) {
	}


	public void keyReleased( int key ) {
	}

//#if DEBUG == "true"
	public void onPointerDragged( int x, int y ) {
	}


	public void onPointerPressed( int x, int y ) {
	}


	public void onPointerReleased( int x, int y ) {
	}
//#endif
}