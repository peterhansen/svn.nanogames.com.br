/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author monteiro
 */
public class GameTextBox extends Drawable implements Constants {
	Pattern bkgRed;
	Pattern bkgYellow;
	DrawableImage iconGift;
	DrawableImage iconBomb;

	DrawableImage bkgRedImage;
	DrawableImage bkgYellowImage;

	RichLabel mainText;
	Label title;

	public GameTextBox(){
		super();
		try {
			if (GameMIDlet.isLowMemory()){
				///TODO: pegar os valores de cor certas (mas acredito que o vinho esteja certo)
				bkgRed = new Pattern(0x9C221C);
				bkgYellow = new Pattern(0xFFC20E);
			} else {
				bkgRedImage = new DrawableImage(PATH_CARD_STATES + "red_card.png");
				bkgRed = new Pattern(bkgRedImage);
				bkgYellowImage = new DrawableImage(PATH_IMAGES + "menu_pattern.png");
				bkgYellow = new Pattern(bkgYellowImage);
			}
			iconGift = new DrawableImage(PATH_IMAGES + "bonus_presente.png");
			iconBomb = new DrawableImage(PATH_IMAGES + "bonus_bomba.png");
			mainText = new RichLabel(FONT_TEXT, "");
			title = new Label(FONT_TITLE);
			mainText.setVisible(true);
			title.setVisible(true);

		} catch (Exception ex) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
			return;
		}
	}

	public final void paint(Graphics g){
		int ROUND_SIZE = 15;
		int currentWidth = getWidth();
		int currentHeight = getHeight();

		g.setColor(0xFFFFFF);
		g.fillRoundRect(translate.x, translate.y, currentWidth, currentHeight, ROUND_SIZE, ROUND_SIZE);
		bkgRed.draw(g);
		bkgYellow.draw(g);
		iconBomb.draw(g);
		iconGift.draw(g);
		mainText.draw(g);
		title.draw(g);
	}

	public void setSize(int sizeX, int sizeY){
//#if BLACKBERRY_API == "true"
//# 		if (ScreenManager.SCREEN_WIDTH < 300 && ScreenManager.SCREEN_HEIGHT < 300)
//# 			sizeY += 35;
//#endif
		super.setSize(sizeX, sizeY);

		bkgYellow.setSize(sizeX - BORDER_SIZE, (sizeY / 5) - (BORDER_SIZE / 2));

		if (bkgYellowImage != null && bkgYellowImage.getSize().y < bkgYellow.getSize().y)
			bkgYellow.setSize( bkgYellow.getSize().x, bkgYellowImage.getSize().y );

		bkgYellow.setPosition(BORDER_SIZE / 2,(BORDER_SIZE / 2));
		bkgRed.setPosition(BORDER_SIZE / 2, bkgYellow.getPosY() + bkgYellow.getSize().y);
		bkgRed.setSize(sizeX - BORDER_SIZE, sizeY - bkgYellow.getSize().y - BORDER_SIZE);
		iconBomb.setPosition(sizeX - iconBomb.getSize().x, 0);
		iconGift.setPosition(sizeX - iconGift.getSize().x, 0);

		title.setPosition(BORDER_SIZE,BORDER_SIZE / 2);
		title.setSize(bkgYellow.getSize().x - 2 * BORDER_SIZE,bkgYellow.getSize().y - BORDER_SIZE);
		int biggest = NanoMath.max( iconBomb.getHeight(), iconGift.getHeight()) - bkgYellow.getHeight() - (BORDER_SIZE / 2);
		mainText.setPosition(bkgRed.getPosX() + BORDER_SIZE,bkgRed.getPosY()+ DEFAULT_PADDING_TEXT_HEIGHT);
		mainText.setSize(bkgRed.getSize().x - 2 * BORDER_SIZE, bkgRed.getSize().y - 2 * BORDER_SIZE - biggest);
	}


	public void setMessage(String titleStr, String msg,int kind){
		mainText.setText(msg.toUpperCase(), true, false);
		title.setText(titleStr.toUpperCase());

		if (kind == BONUS_BONUS){
			iconGift.setVisible(true);
			iconBomb.setVisible(false);
		}else if (kind == BONUS_PENALTY){
			iconGift.setVisible(false);
			iconBomb.setVisible(true);
		}else{
			iconGift.setVisible(false);
			iconBomb.setVisible(false);
		}
	}


}
