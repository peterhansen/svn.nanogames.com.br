/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import java.util.Hashtable;
import screens.GameMIDlet;

/**
 *
 * @author caiolima
 */
public final class SubMenu extends Menu implements Constants
{
	private final static short TIME_GOTO_MENUITEM = 666;
	private final static byte TOUCH_TOLERANCE = 10;

	protected final byte backIndex;
	protected final byte firstEntry;
	/** Estrutura que armazena a última entrada selecionada de cada menu. A chave de cada entrada é o id do menu,
	 * e o valor da chave é o índice da última entrada utilizado
	 */
	protected static final Hashtable LAST_INDEX = new Hashtable();
	private final ImageFont font;
	private final Drawable[] labels;

    private final Point limit; // x = minY; y = maxY;

    /* Variaveis para movimentação do menu */
	private int AccTime;
	private int TotalTime;
    private int distanciaY = 0;
    private int antesY = 0;
	private boolean moveMenu;
	private final BezierCurve bezier = new BezierCurve();

	private boolean forceToShowSelected;
	private boolean fromTouch;
	private int lastY;
	private int firstY;


	public SubMenu( MenuListener listener, int id, Drawable[] entries ) throws Exception {
		this( listener, id, null, new int[ entries.length ], entries, null );
	}


	protected SubMenu( MenuListener listener, int id, ImageFont font, int[] entriesIndexes, Drawable[] entries, String[] entriesText ) throws Exception {
		super( listener, id, ( entriesIndexes == null ? entriesText.length : entriesIndexes.length ) );

		this.backIndex = ( byte ) entries.length;
		this.firstEntry = ( byte ) 0;

		this.font = font;
		final int LENGTH = ( entriesIndexes == null ? entriesText.length : entriesIndexes.length );
		labels = new Drawable[ LENGTH ];

		if ( entries == null ) {
			for ( int i = 0; i < LENGTH; ++i ) {
				final Label label = new Label( font, entriesText == null ? AppMIDlet.getText( entriesIndexes[ i ] ) : entriesText[ i ] );
				labels[ i ] = label;
				insertDrawable( label );
			}
		} else {
			for ( int i = 0; i < entries.length; ++i ) {
				insertDrawable( entries[ i ] );
				labels[ i ] = entries[ i ];
			}
		}

		setCircular( true );

		final Integer lastIndex = ( Integer ) LAST_INDEX.get( new Integer( id ) );
		if ( lastIndex == null ) {
			setCurrentIndex( firstEntry );
		} else {
			final int index = lastIndex.intValue();
			if ( index == backIndex )
				setCurrentIndex( 0 );
			else
				setCurrentIndex( index );
		}

        limit = new Point();
		forceToShowSelected = true;
		lastY = 0;
		fromTouch = false;

		setCurrentIndex( currentIndex );
	}


	public final void setSize( int width, int height ) {

		//#if DEBUG == "true"
			System.out.println( "SubMenu.setSize( " + width + ", " + height + " );" );
		//#endif

		final int CURSOR_HEIGHT_DIFF = getCursor() == null ? 0 : Math.max( getCursor().getHeight() - labels[ 0 ].getHeight(), 0 );
		int y = ( CURSOR_HEIGHT_DIFF >> 1 ) + SAFE_MARGIN;

        y += SAFE_MARGIN;
		for ( int i = 0; i < labels.length; ++i ) {
			labels[ i ].setSize( width, labels[ i ].getHeight() );
			labels[ i ].defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
			labels[ i ].setRefPixelPosition( width >> 1, y );

			y += labels[ i ].getHeight() + SAFE_MARGIN;
		}

        limit.set(NanoMath.min((height - y)+ SAFE_MARGIN, SAFE_MARGIN), SAFE_MARGIN );

		super.setSize( width, height );

		setCurrentIndex( currentIndex );
	}


	public final void setCursor( Drawable cursor, byte drawOrder, int alignment ) {
		super.setCursor( cursor, drawOrder, alignment );

		setSize( getSize() );
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if ( backIndex >= 0 ) {
					setCurrentIndex( backIndex );
					super.keyPressed( ScreenManager.KEY_NUM5 );
				}
			break;

			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
			break;

			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}


	public final void setCurrentIndex( int index ) {
		final int previousIndex = currentIndex;

		super.setCurrentIndex( index );

		if ( currentIndex <= firstEntry ) {
			if ( previousIndex < firstEntry || previousIndex == activeDrawables - 1 )
				super.setCurrentIndex( firstEntry );
			else if ( previousIndex == firstEntry && currentIndex < firstEntry ) {
				if ( firstEntry == 0 )
					super.setCurrentIndex( 0 );
				else
					super.setCurrentIndex( circular ? activeDrawables - 1 : firstEntry );
			}
		}

		// grava o índice atual para futuras instâncias de menu com o mesmo id
		LAST_INDEX.put( new Integer( menuId ), new Integer( currentIndex ) );

		if(!fromTouch)
			showSelected();
	}


	public final void showSelected() {
		forceToShowSelected = true;
		final Drawable entry = getDrawable( currentIndex );
		//final int relativeY = getPosY() + entry.getPosY();
		if ( entry.getPosY() < getSize().y/2 ) {
            moveMenuItems(((getSize().y/2)-entry.getPosY()) - entry.getHeight()/2/*+ itemSpacing*/);
			//setPosition( getPosX(), -entry.getPosY() );
		} else if ( (entry.getPosY() + entry.getHeight()) > (getSize().y/2) ) {
            moveMenuItems(((getSize().y/2) - (entry.getPosY() + entry.getHeight()))+entry.getHeight()/2);
			//setPosition( getPosX(), getPosY() + ScreenManager.SCREEN_HEIGHT - relativeY - entry.getHeight() );
		}
	}


    public final void update( int delta ) {
        for(int i = 0; i < labels.length; i++) {
            if(labels[i] instanceof Updatable) {
                ((Updatable)labels[i]).update(delta);
            }
        }

		if(forceToShowSelected) {
			if(moveMenu) {
				if ( AccTime >= TotalTime ) {
					moveMenu = false;
				} else {
					AccTime += delta;
					final Point p = new Point();
					bezier.getPointAtFixed( p, NanoMath.divInt( AccTime, TotalTime ) );
					setLabelsPosition( p.y );
				}
			}
		}
    }


    private final void setLabelsPosition(int y) {
        moveLabels(y - labels[0].getPosY());
    }


	private final void moveLabels( int y ) {
		if ( ( labels[0].getPosY() + y ) - limit.y > 0 ) {
			y = limit.y - labels[0].getPosY();
		}
		if ( ( labels[0].getPosY() + y ) - limit.x < 0 ) {
			y = labels[0].getPosY() - limit.x;
		}

		labels[0].getPosY();
		if ( y != 0 ) {
			for ( int i = 0; i < labels.length; ++i ) {
				labels[i].move( 0, y );
			}
		}
	}


    private final void moveMenuItems( int dy ) {
        if(labels.length>0 && getSize().y > 0) {
            if(labels[0].getPosY()+dy < limit.x) {
                dy = limit.x - labels[0].getPosY();
            }
            else if(labels[0].getPosY() + dy > limit.y) {
                dy = limit.y - labels[0].getPosY();
            }

            distanciaY = dy;
            antesY = labels[0].getPosY();
            AccTime = 0;
            TotalTime = TIME_GOTO_MENUITEM;
            moveMenu = true;

            bezier.origin.set( labels[0].getPosX(), antesY );
            bezier.destiny.set( labels[0].getPosX(), antesY + distanciaY );

            bezier.control1.set( labels[0].getPosX(), antesY );
            bezier.control2.set( labels[0].getPosX(), antesY + distanciaY );
        }
    }


	/**
	 * Obtém o índice da opção selecionada pelo ponteiro.
	 *
	 * @param x posição x do evento de ponteiro na tela.
	 * @param y posição y do evento de ponteiro na tela.
	 * @return índice da opção selecionada, ou -1 caso a posição não intercepte nenhuma entrada.
	 */
	protected final int getEntryAt( int x, int y ) {
		x -= position.x;
		y -= position.y;

		//#if DEBUG == "true"
			System.out.println( "firstEntry = " + firstEntry + ", activeDrawables = " + activeDrawables );
		//#endif

		for ( byte i = firstEntry; i < activeDrawables; ++i ) {

			//#if DEBUG == "true"
				System.out.println( "drawables[" + i + " ].contains ? " + drawables[ i ].contains( x, y ) );
			//#endif

			final Drawable entry = drawables[ i ];
			if ( entry.contains( x, y ) )
				return i;
		}
		return -1;
	}


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			/*final int index = getEntryAt( x, y );

			if ( index >= 0 )
				setCurrentIndex( index );
			else {
			}*/

			forceToShowSelected = false;
			moveLabels(y-lastY);
			lastY = y;
		}


		public void onPointerPressed( int x, int y ) {
			try {
			firstY = y;
			final int index = getEntryAt( x, y );
			if ( currentIndex == index ) {
				// o usuário confirmou a opção atual
				keyPressed( ScreenManager.FIRE );
			} else {
				// o usuário trocou a opção selecionada
				fromTouch = true;
				setCurrentIndex( index );
				fromTouch = false;
			}
			lastY = y;
			}
			catch ( Throwable t ) {
				//#if DEBUG == "true"
					System.out.println( t.getMessage() );
					t.printStackTrace();
				//#endif
			}
		} // fim do método onPointerPressed( int, int )


		public void onPointerReleased( int x, int y ) {
			if(NanoMath.abs(firstY-lastY)<=TOUCH_TOLERANCE) showSelected();
		}
	//#endif

}