/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author monteiro
 */
public class GameCard extends Card {
	public static final byte CARDTYPE_NORMAL = 0;
	public static final byte CARDTYPE_SWAP = 1;
	public static final byte CARDTYPE_SHOW_CARDS = 2;
	public static final byte CARDTYPE_LOSE_TURN = 3;
	public static final byte CARDTYPE_PULSATE_TWO = 4;
	public static final byte CARDTYPE_LOSE_TIME = 5;
	public static final byte CARDTYPE_PULSATE_CORRESPONDING = 6;
	private Rectangle imageRegion;
	private Image girlImage;
	private Point imageOffset;
	/**
	 *
	 */
	private byte index;
	private byte kind;

	/**
	 *
	 * @throws Exception
	 */
	public GameCard(Image GirlImage) throws Exception{
		super();
		kind = CARDTYPE_NORMAL;
		girlImage =  GirlImage;
		GameMIDlet.log( "gamecard created");
	}


	/**
	 *
	 * @param index
	 */
	public final void setIndex( int index ) {
		this.index = ( byte ) index;
	}


	public final byte getIndex() {
		return index;
	}


	public final boolean equals( GameCard c ) {
		return c != null && index == c.getIndex();
	}


	/**
	 * @return the kind
	 */ public byte getKind() {
		return kind;
	}


	/**
	 * @param kind the kind to set
	 */
	 public void setKind(byte kind) {
		setCardBackType( ( ( kind == CARDTYPE_NORMAL ) ? ImageBuffer.TYPE_BACK_CARD : ImageBuffer.TYPE_BACK_CARD_SPECIAL ) );
		setSize( getWidth(), getHeight(), false, true );
		this.kind = kind;
	}


	 public void swapWith(Card card2) {
		 super.swapWith(card2);
	 }


	public Rectangle getImageRegion() {
		return imageRegion;
	}


	public void destroyCentralFaceImage(){
//#if DEBUG == "true"
//		System.out.println("imagem de centro da carta destruida");
//#endif
		setCardFace( null );
		GameMIDlet.gc();
	}

	public void buildAndSetCentralFaceImage(){
		if ( imageRegion == null )
			return;

		if ( girlImage == null )
			return;
//#if DEBUG == "true"
//		System.out.println("imagem de centro da carta construida");
//#endif 

		Rectangle cut = new Rectangle(imageRegion);
		Image face;
		DrawableImage girl = new DrawableImage(girlImage);

		cut.x = ( - cut.x );
		cut.y = ( - cut.y );
		if( imageOffset.x < 0 )
			cut.x += ( imageOffset.x );
		if( imageOffset.y < 0 )
			cut.y += ( imageOffset.y );

		girl.setPosition( cut.x, cut.y );

		cut.x = 0;
		cut.y = 0;
		girl.setViewport(cut);
		face = createAndReturnCardface( imageRegion.width, imageRegion.height );

		girl.draw(face.getGraphics());
	}

	

	/** @param imageRegion the imageRegion to set */
	public void setImageRegion(Rectangle imageRegion) {
		this.imageRegion = imageRegion;		
	}

	/**
	 * @return the imageOffset
	 */
	public Point getImageOffset() {
		return imageOffset;
	}

	/**
	 * @param imageOffset the imageOffset to set
	 */
	public void setImageOffset( Point imageOffset ) {
		this.imageOffset = imageOffset;
	}


	public void unpresentate( int i ) {

	}


	public void presentate( int time ) {
		forceState( AnimatedBorder.STATE_FLIPPED_PRESENTING);
		startAnimating(-time);
	}
}
