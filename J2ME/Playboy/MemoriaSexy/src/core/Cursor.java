package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 * Cursor da mesa de jogo de memória, com quatro cantoneiras pulsantes
 * @author monteiro
 */
public class Cursor extends UpdatableGroup implements Constants {

	/**
	 *
	 */
	private int row;
	private int column;
	private GameCard target;
	private Sprite[] corner;

	public Cursor() throws Exception {
		super(4);

		row = 0;
		column = 0;

		//alocação do array
		corner = new Sprite[4];

		corner[0] = new Sprite(PATH_CURSOR + "cursor_carta");
		corner[1] = new Sprite(corner[0]);
		corner[2] = new Sprite(corner[0]);
		corner[3] = new Sprite(corner[0]);

		///todos no grupo...
		insertDrawable(corner[0]);
		insertDrawable(corner[1]);
		insertDrawable(corner[2]);
		insertDrawable(corner[3]);

		//posicionamentos:

		//canto superior direito
		corner[1].mirror( TRANS_MIRROR_H );

		//canto inferior direito
		corner[2].mirror( TRANS_MIRROR_H | TRANS_MIRROR_V );

		//canto inferior esquerdo
		corner[3].mirror( TRANS_MIRROR_V );

		//inicia a animação
		corner[0].pause(false);
		corner[1].pause(false);
		corner[2].pause(false);
		corner[3].pause(false);

		//torna os quatro cantos visiveis
		corner[0].setVisible(true);
		corner[1].setVisible(true);
		corner[2].setVisible(true);
		corner[3].setVisible(true);
		setVisible(true);

		GameMIDlet.log("allswell!");

	}


	/**
	 * Toca a animação pulsante do cursor
	 * @param delta
	 */
	public final void update(int delta) {

		if (target != null && target.getSize() != null && !getSize().equals(target.getSize())) {
			Point tmp = new Point(getSize());
			this.setPosition(getPosX() - (target.getSize().x - tmp.x) / 2, getPosY() - ((target.getSize().y - tmp.y) / 2));
			setSize(target.getSize());
		}

		if (target != null /*&& !target.isAnimating()*/)
			this.setPosition(target.getPosition());

		super.update(delta);
	}

	/**
	 * 
	 * @param g
	 */
	public void draw(Graphics g) {
		if (!isVisible())
			return;

		final int DX = getPosX();
		final int DY = getPosY();

		translate.addEquals( DX, DY );

		(getDrawable(0)).draw(g);
		(getDrawable(1)).draw(g);
		(getDrawable(2)).draw(g);
		(getDrawable(3)).draw(g);
		
		translate.subEquals( DX, DY );
	}

	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * @param column the column to set
	 */
	public void setColumn(int column) {
		this.column = column;
	}

	public void setSize(int width, int height) {
		super.setSize(width, height);
		corner[0].setPosition(- 5, - 5);
		//canto superior direito

		corner[1].setPosition(width - corner[1].getWidth() + 5, -5);

		//canto inferior direito
		corner[2].setPosition(width - corner[2].getWidth() + 5, height - corner[2].getHeight() + 5);

		//canto inferior esquerdo
		corner[3].setPosition(-5, height - corner[3].getHeight() + 5);
	}

	public void setTarget(GameCard currentCard) {
		target = currentCard;
		
		if (target == null)
			return;

		this.setPosition( currentCard.getPosX(), currentCard.getPosY() );
		update(0);
	}

	public final Card getTarget() {
		return target;
	}
}
