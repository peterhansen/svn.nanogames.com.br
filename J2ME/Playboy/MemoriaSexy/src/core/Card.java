/**
 * Card.java
 *
 * Created on Dec 16, 2010 5:11:34 PM
 *
 */
package core;

import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public class Card extends AnimatedBorder {
	private boolean flipped;
	private Point preferedSize;
	private Point originalPosition;


	/**
	 *
	 * @throws Exception
	 */
	public Card() throws Exception {
		super();
		setCardBackType( ImageBuffer.TYPE_BACK_CARD );
		setCardFace(ImageLoader.loadImage(PATH_CARD_STATES + "red_card.png"));
		preferedSize = new Point();
		setPreferedSize(CARD_DEFAULT_WIDTH, CARD_DEFAULT_HEIGHT);
		flipped = false;
		GameMIDlet.log( "card created");
	}


	public void setPreferedSize(int w, int h) {
		setSize(w, h);
		preferedSize.set(w, h);
	}


	public void animate( int time ) {
		setAnimation(getRefPixelPosition(), new Point(preferedSize.x, preferedSize.y), time);
	}


	public final void animate() {
		animate(0);
	}


	public final void shrinkVanish() {
		setAnimation(getRefPixelPosition(), new Point(0,0));
		destroyCentralFaceImage();
	}


	public void swapWith(Card card2) {
		Point targetPos = new Point(card2.getRefPixelPosition());
		setAnimation(targetPos, getSize());
		setOriginalPosition( targetPos);
	}

	
	/**
	 * @return the flipped
	 */ public final boolean isFlipped() {
		return flipped;
	}


	 public void flip(int time) {
		//expande a carta atual. retorna as outras ao tamanho normal
		flipped = !flipped;
		startAnimating(-time);
	}


	 public void flip() {
		 flip(0);
	}


	/**
	 * @return the originalPosition
	 */
	public Point getOriginalPosition() {
		return originalPosition;
	}


	/**
	 * @param originalPosition the originalPosition to set
	 */
	public void setOriginalPosition( Point originalPosition ) {
		this.originalPosition = originalPosition;
	}


	/**
	 * @return the preferedSize
	 */
	public Point getPreferedSize() {
		return preferedSize;
	}
}
