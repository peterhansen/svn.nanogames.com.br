/**
 * Border.java
 * 
 * Created on Mar 2, 2009, 2:33:55 PM
 *
 */
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public class AnimatedBorder extends UpdatableGroup implements Constants {

	/***/
	private final BezierCurve tweenSize = new BezierCurve();
	private short accSizeTime = -1;
	private static final short TIME_SIZE_ANIMATION = 666; // demôin!
	/***/
	private final BezierCurve tweenPosition = new BezierCurve();
	/***/
	private short accPosTime = -1;
	private static final short TIME_POS_ANIMATION = 1100;
	private Image centralImage;
	private Image centralScaleImage;
	private Drawable currentDrawable;
	private final static short ANIMATION_STOPPED_TIME = -0xFFF;
	private Image cardFace;
	private byte currentStateFrame;
	private boolean cardAnimating;
	private boolean cardIsFliped;
	private byte cardBackType;
	private int frameTime;
	public final static byte STATE_FLIPPED = 0;
	public final static byte STATE_UNFLIPPING = 1;
	public final static byte STATE_UNFLIPPED = 2;
	public final static byte STATE_FLIPPING = 3;
	public final static byte STATE_FLIPPED_PRESENTING = 4;
	public final static byte STATE_UNFLIPPING_PRESENTING = 5;
	public final static byte STATE_UNFLIPPED_PRESENTING = 6;
	public final static byte STATE_SHOWING_PRESENTING = 7;
	public final static byte STATE_FLIPPING_PRESENTING = 8;

	private boolean isShowingCardFace = true;

	private final static byte[] stateTransitions = {
		STATE_UNFLIPPING,
		STATE_UNFLIPPED,
		STATE_FLIPPING,
		STATE_FLIPPED,
		STATE_UNFLIPPING_PRESENTING,
		STATE_UNFLIPPED_PRESENTING,
		STATE_FLIPPING_PRESENTING,
		STATE_FLIPPING_PRESENTING,
		STATE_FLIPPED
	};
	private final static boolean[] willPerformTransitions = {
		false,
		true,
		false,
		true,
		true,
		true,
		true,
		true,
		true
	};

	public AnimatedBorder() throws Exception {
		super(13);
		centralImage = null;
		forceState(STATE_FLIPPED_PRESENTING);
		GameMIDlet.log( "animatedborder created");
	}

	public final void forceState(int newState){
		setCurrentState( ( byte ) newState);
		frameTime = 0;
		cardAnimating = false;
	}


	public final void startAnimating() {
		startAnimating(0);
	}


	public final void startAnimating( int time ) {
		cardAnimating = true;
		frameTime = time;
	}

	/**
	 *
	 * @param destination posição final da animação. Se for null, não muda a posição.
	 * @param finalSize tamanho final da animação. Se for null, não muda seu tamanho.
	 */
	public final void setAnimation( Point destination, Point finalSize ) {
		setAnimation(destination, finalSize, 0);
	}

	private final Point getCardBorderSize() {
		Point currentSize = new Point();
		switch (currentStateFrame) {
			case STATE_FLIPPING:
			case STATE_UNFLIPPING:
			case STATE_FLIPPING_PRESENTING:
			case STATE_UNFLIPPING_PRESENTING:
				if (frameTime < ( TIME_CARD_FLIP >> 1 ) ) {
					currentSize.x = getWidth() - 2 * NanoMath.toInt(NanoMath.mulFixed(NanoMath.toFixed(frameTime), NanoMath.divFixed(NanoMath.toFixed(getWidth()), NanoMath.toFixed(TIME_CARD_FLIP))));
				} else {
					currentSize.x = NanoMath.toInt(NanoMath.mulFixed(NanoMath.toFixed(frameTime), NanoMath.divFixed(NanoMath.toFixed(getWidth()), NanoMath.toFixed(TIME_CARD_FLIP))));
				}
				currentSize.y = getHeight();
				break;
			case STATE_FLIPPED:
			case STATE_UNFLIPPED:
			case STATE_FLIPPED_PRESENTING:
			case STATE_UNFLIPPED_PRESENTING:
			case STATE_SHOWING_PRESENTING:
			default: {
				currentSize.x = getWidth();
				currentSize.y = getHeight();
			}
			break;
		}
		return currentSize;
	}

	public final void setAnimation( Point destination, Point finalSize, int startDelay ) {

		if (startDelay == ANIMATION_STOPPED_TIME) {
			startDelay++;
		}

		if (destination != null) {
			getTweenPosition().origin.set(getRefPixelPosition());
			getTweenPosition().destiny.set(destination);
			getTweenPosition().control1.set(destination);
			getTweenPosition().control2.set(destination);
			accPosTime = (short) startDelay;
		}

		if (finalSize != null) {
			tweenSize.origin.set(size);
			tweenSize.destiny.set(finalSize);
			tweenSize.control1.set(size.x * 3 / 4, size.y * 3 / 4);
			tweenSize.control2.set(finalSize.x * 15 / 10, finalSize.y * 15 / 10);
			accSizeTime = (short) startDelay;
		}
	}

	public final void update( int delta ) {
		super.update(delta);

		if (accSizeTime < TIME_SIZE_ANIMATION && accSizeTime != ANIMATION_STOPPED_TIME) {
			accSizeTime += delta;
		}

		if (accSizeTime >= 0 && accSizeTime != ANIMATION_STOPPED_TIME) {

			final Point p = new Point();
			final Point previousRefPos = getRefPixelPosition();

			final int fp_progress = NanoMath.divInt(accSizeTime, TIME_SIZE_ANIMATION);
			tweenSize.getPointAtFixed(p, fp_progress);
			setSize(p);
			setRefPixelPosition(previousRefPos);

			if (accSizeTime >= TIME_SIZE_ANIMATION) {
				accSizeTime = ANIMATION_STOPPED_TIME;
			}

			if (centralImage != null && centralImage instanceof Updatable) {
				((Updatable) centralImage).update(delta);
			}

			if (centralScaleImage != null && centralScaleImage instanceof Updatable) {
				((Updatable) centralScaleImage).update(delta);
			}
		}

		if (accPosTime < TIME_POS_ANIMATION && accPosTime != ANIMATION_STOPPED_TIME) {
			accPosTime += delta;
		}

		if (accPosTime >= 0 && accPosTime != ANIMATION_STOPPED_TIME) {

			final Point p = new Point();

			final int fp_progress = NanoMath.divInt(accPosTime, TIME_POS_ANIMATION);
			getTweenPosition().getPointAtFixed(p, fp_progress);
			setRefPixelPosition(p);

			if (accPosTime >= TIME_POS_ANIMATION) {
				accPosTime = ANIMATION_STOPPED_TIME;
			}
		}

		if (cardAnimating) {
			frameTime += delta;			
			switch (currentStateFrame) {
				case STATE_UNFLIPPING: 
				case STATE_UNFLIPPING_PRESENTING: {
					if (frameTime < TIME_CARD_FLIP >> 1) {
						if (currentDrawable != null) {
							currentDrawable.setVisible(false);
						}
						isShowingCardFace = false;
						setCardBack();
						//setCurrentImage(cardBack);
					} else {
						if (currentDrawable != null) {
							currentDrawable.setVisible(true);
						}
						isShowingCardFace = true;
						setCurrentImage(cardFace);						
					}
					
				}
				break;
				case STATE_UNFLIPPED_PRESENTING:
				case STATE_SHOWING_PRESENTING:
				case STATE_UNFLIPPED: {
					if (currentDrawable != null) {
						currentDrawable.setVisible(true);
					}
					isShowingCardFace = true;
					setCurrentImage(cardFace);
				}
				break;
				case STATE_FLIPPING_PRESENTING:
				case STATE_FLIPPING: {
					if (frameTime < TIME_CARD_FLIP >> 1) {
						if (currentDrawable != null) {
							currentDrawable.setVisible(true);
						}
						isShowingCardFace = true;
						setCurrentImage(cardFace);
					} else {
						if (currentDrawable != null) {
							currentDrawable.setVisible(false);
						}
						isShowingCardFace = false;
						setCardBack();
						//setCurrentImage(cardBack);
					}					
				}
				break;
				case STATE_FLIPPED_PRESENTING:
				case STATE_FLIPPED: {
					if (currentDrawable != null) {
						currentDrawable.setVisible(false);
					}
					isShowingCardFace = false;
					setCardBack();
					//setCurrentImage(cardBack);
				}
				break;
			}			
			if (frameTime >= TIME_CARD_FLIP) {
				frameTime = 0;
				setCurrentState(stateTransitions[currentStateFrame]);
				cardAnimating = willPerformTransitions[currentStateFrame];
			}
		}
	}

	public boolean isShowingCardFace() {
		return isShowingCardFace;
	}

	public boolean isAnimating() {
		return accPosTime != ANIMATION_STOPPED_TIME;
	}

	public final void paint( Graphics g ) {
		//borda branca
		final int ROUND_SIZE = Math.max(getWidth() / 7, 3);
		int currentWidth;
		int currentHeight;

		g.setColor( 0xffffff );

		Point currentSize = getCardBorderSize();
		currentWidth = currentSize.x;
		currentHeight = currentSize.y;

		g.fillRoundRect(translate.x + ( ( getWidth() - currentWidth ) >> 1), translate.y, currentWidth, currentHeight, ROUND_SIZE, ROUND_SIZE);

		//preenchimento marrom (vinho?)
		g.setColor( 0x9C221C );

		currentWidth = getInternalWidth();
		currentHeight = getInternalHeight();

		if ( currentDrawable == null && ((currentWidth > 0 && currentHeight > 0) || centralScaleImage == null) && (centralScaleImage != null && (centralScaleImage.getWidth() != currentWidth || centralScaleImage.getHeight() != currentHeight))) {
//			if( currentStateFrame == STATE_FLIPPED ) {
//				
//			}
			if( cardIsFliped || centralImage == null ) {
				centralScaleImage = ImageBuffer.getImage( cardBackType, currentWidth, currentHeight, (GameMIDlet.isLowMemory() ? 1 : 0));
			} else {
				centralScaleImage = ImageLoader.scaleImage(centralImage, currentWidth, currentHeight);
			}
		}

		final int ROUND_SIZE_2 = Math.max(currentWidth / 7, 2);
		g.fillRoundRect(translate.x + ( (getWidth() - currentWidth) >> 1 ) + 1,
				translate.y + ( (getHeight() - currentHeight) >> 1 ) + 1, currentWidth - 1, currentHeight - 1, ROUND_SIZE_2, ROUND_SIZE_2);

		//imagem central
		if (centralScaleImage != null) {
			g.drawImage(centralScaleImage, translate.x + ( getWidth() >> 1 ), translate.y + ( getHeight() >> 1), Graphics.HCENTER | Graphics.VCENTER);
		}
	}


	/** Calcula a largura interna da carta mantendo a proporção da borda para uma carta (2:3), padrão usado no jogo.
	 * @return largura interna da carta */
	private final int getInternalWidth() {
		Point currentSize = getCardBorderSize();
		int ret = NanoMath.toInt( FP_CARD_INTERNAL_BORDER_PERCENT * currentSize.x );
		return ret - ( ( ( ( currentSize.x - ret ) * 3 ) / 2 ) - ( currentSize.x - ret ) );
	}


	/** Calcula a altura interna da carta mantendo a proporção da borda para uma carta (2:3), padrão usado no jogo.
	 * @return altura interna da carta */
	private final int getInternalHeight() {
		return NanoMath.toInt( getCardBorderSize().y * FP_CARD_INTERNAL_BORDER_PERCENT );
	}


	public final void setSize( int width, int height ) {
		setSize(width, height, false);
	}


	public final void setSize( int width, int height, boolean mantainAspectRatio ) {
		setSize(width, height, mantainAspectRatio, false);
	}


	public final void setSize( int width, int height, boolean mantainAspectRatio, boolean forceResize ) {
		boolean hasToResizeImg = ( ( width != getWidth() ) || ( height != getHeight() ) || forceResize );

		super.setSize( width, height );

		if ( hasToResizeImg ) {
			int HEIGHT = getInternalHeight();
			int WIDTH = getInternalWidth();
			if( cardIsFliped || centralImage == null ) {
				centralScaleImage = ImageBuffer.getImage( cardBackType, WIDTH, HEIGHT, 2 );
			} else {
				centralScaleImage = null;
				if (WIDTH > 0 && HEIGHT > 0 && centralImage.getHeight() > 0 && centralImage.getWidth() > 0) {
					if ( mantainAspectRatio ) {
						HEIGHT = Math.max( 1, ( HEIGHT * 90 ) / 100 );
						WIDTH = Math.max( 1, ( WIDTH * 90 ) / 100 );

						final int w;
						final int h;

						final int RATIO_FP = NanoMath.divInt(HEIGHT, WIDTH);
						final int IMG_RATIO = NanoMath.divInt(centralImage.getHeight(), centralImage.getWidth());

						if (IMG_RATIO < RATIO_FP) {
							w = WIDTH;
							h = NanoMath.toInt(WIDTH * IMG_RATIO);
						} else if (IMG_RATIO > RATIO_FP) {
							w = NanoMath.toInt(NanoMath.divFixed(NanoMath.toFixed(HEIGHT), IMG_RATIO));
							h = HEIGHT;
						} else {
							w = WIDTH;
							h = HEIGHT;
						}

						if (w > 0 && h > 0) {
							centralScaleImage = ImageLoader.scaleImage(centralImage, w, h);
						}
					} else {
						centralScaleImage = ImageLoader.scaleImage(centralImage, WIDTH, HEIGHT);
					}
				}
			}
			defineReferencePixel(ANCHOR_CENTER);
		}
	}


	/**
	 * @return the currentDrawable
	 */
	public Drawable getCurrentDrawable() {
		return currentDrawable;
	}


	/**
	 * @param currentDrawable the currentDrawable to set
	 */
	public void setCurrentDrawable( Drawable newDrawable ) {
		this.currentDrawable = newDrawable;

		if ((newDrawable != null) && (newDrawable instanceof DrawableImage)) {
			centralImage = ((DrawableImage) newDrawable).getImage();
		}
		Point sz = getCardBorderSize();
		setSize(sz.x, sz.y, true, true );
	}


	public void applyToCurrentImage( Drawable drawable ){
		if (drawable != null &&
			drawable.isVisible() &&
			centralImage != null &&
			centralImage.getGraphics() != null &&
			centralImage.isMutable()
			)
		drawable.draw( centralImage.getGraphics());
	}


	public void setCardBack() {
		centralImage = null;
		cardIsFliped = true;
		setSize( getWidth(), getHeight() );
	}


	public void setCurrentImage( Image currentFrameImage ) {
		cardIsFliped = false;
		centralImage = currentFrameImage;
		applyToCurrentImage(currentDrawable);
		if (centralImage != null && size.x > 0 && size.y > 0) {
			centralScaleImage = ImageLoader.scaleImage(centralImage, size.x, size.y);
		}
	}

	public void buildAndSetCentralFaceImage(){
	}

	public final void setCentralScaledImage(Image img){
		centralScaleImage = img;
	}

	/**
	 * @return the tweenPosition
	 */
	public final BezierCurve getTweenPosition() {
		return tweenPosition;
	}


	public void setCardBackType( byte type ) {
		cardBackType = type;
		setCardBack();
	}

	public Image getCardFace(){
		return cardFace;
	}

	public Image createAndReturnCardface(int width, int height){
		cardFace = Image.createImage(width, height);
		return cardFace;
	}

	public void setCardFace( Image img ) {
		
		if (img != null){
			cardFace = Image.createImage(img.getWidth(), img.getHeight());
			cardFace.getGraphics().drawImage(img, 0, 0, Graphics.LEFT | Graphics.TOP);
		}
		else
			cardFace = null;

		setCurrentImage(cardFace);
	}


	private void setCurrentState( byte b ) {
		currentStateFrame = b;
		switch (currentStateFrame){
			case STATE_FLIPPED:
				destroyCentralFaceImage();
				break;
			case STATE_UNFLIPPING:
				buildAndSetCentralFaceImage();
				break;
			case STATE_FLIPPED_PRESENTING:
				destroyCentralFaceImage();
				break;
			case STATE_UNFLIPPING_PRESENTING:
				buildAndSetCentralFaceImage();
				break;
		}
	}


	public void destroyCentralFaceImage() {

	}
}
