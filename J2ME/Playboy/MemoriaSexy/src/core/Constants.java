/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public interface Constants {

	public static final String APP_SHORT_NAME = "MEMS";

	public static final byte MAX_NUMBER_CHARS = 7;
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";

	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	public static final String PATH_ONLINE = PATH_IMAGES + "online/";
	public static final String PATH_CARD_BORDER = PATH_IMAGES + "card_border/";
	public static final String PATH_CARD_STATES = PATH_IMAGES + "card_states/";
	public static final String PATH_CURSOR = PATH_IMAGES + "cursor_states/";
	public static final String PATH_GIRLS = PATH_IMAGES + "ninfetas/";

	public static final byte RANKING_INDEX_ACC_SCORE = 0;
	public static final byte RANKING_INDEX_MAX_SCORE = 1;
	public static final byte RANKING_INDEX_MAX_LEVEL = 2;


	// <editor-fold defaultstate="collapsed" desc="DATABASE INFO">
		/** Nome da base de dados. */
		public static final String DATABASE_NAME = "N";

		/**Índice do slot de gravação das opções na base de dados. */
		public static final byte DATABASE_SLOT_OPTIONS = 1;
		public static final byte DATABASE_SLOT_GAME_DATA = 2;

		/** Quantidade total de slots da base de dados. */
		public static final byte DATABASE_TOTAL_SLOTS = 2;

		/** Id da categoria de notícias do Topa ou Não Topa, no Nano Online. */
		public static final byte NEWS_CATEGORY_ID = 6;
	// </editor-fold>


	//<editor-fold desc="CONSTANTES DO HARDWARE">
		// Duração da vibração ao atingir a trave, em milisegundos.
		public static final short VIBRATION_TIME_DEFAULT = 300;

		//#if SCREEN_SIZE == "SMALL"
//#  		public static final int LOW_MEMORY_LIMIT = 1000000;
		//#elif SCREEN_SIZE == "MEDIUM"
//# 			public static final int LOW_MEMORY_LIMIT = 1200000;
		//#elif SCREEN_SIZE == "BIG"
			public static final int LOW_MEMORY_LIMIT = 1600000;
		//#else
//#  		public static final int LOW_MEMORY_LIMIT = 2000000;
		//#endif
	//</editor-fold>


	// <editor-fold defaultstate="collapsed" desc="ÍNDICES">

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS LISTENERS">
			public static final byte LISTENER_ID_BIG_CASE	= 0;
			public static final byte LISTENER_ID_TRANSITION	= 1;
			public static final byte LISTENER_ID_VALUES_BOX	= 2;
			public static final byte LISTENER_ID_BANKER		= 3;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES E DADOS DAS FONTES">
			/** Offset da fonte padrão. */
			public static final byte DEFAULT_FONT_OFFSET = 1;

			/**Índice da fonte padrão do jogo. */
			public static final byte FONT_INDEX_DEFAULT	= 0;

			/**Índice da fonte utilizada para textos. */
			public static final byte FONT_TEXT			= 0;
			public static final byte FONT_TITLE			= 1;
			public static final byte FONT_MULTIPLIER	= 2;
			public static final byte FONT_NUMBERS		= 3;

			/** Total de tipos de fonte do jogo. */
			public static final byte FONT_TYPES_TOTAL	= 4;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES E DADOS DOS SONS">

			//índices dos sons do jogo
			public static final byte SOUND_DEFAULT				= 0;
			public static final byte SOUND_MUSIC_LEVEL_COMPLETE	= 1;
			public static final byte SOUND_MUSIC_GAME_OVER		= 2;
			public static final byte SOUND_MUSIC_GAME			= 3;
			public static final byte SOUND_MUSIC_MENU			= 4;

			/** Total de tipos de sons do jogo. */
			public static final byte SOUND_TOTAL = 5;

		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS TEXTOS">
			public static final short TEXT_OK							     	= 0;
			public static final short TEXT_BACK									= TEXT_OK + 1;
			public static final short TEXT_NEW_GAME								= TEXT_BACK + 1;
			public static final short TEXT_OPTIONS								= TEXT_NEW_GAME + 1;
			public static final short TEXT_NANO_ONLINE							= TEXT_OPTIONS + 1;
			public static final short TEXT_HELP									= TEXT_NANO_ONLINE + 1;
			public static final short TEXT_CREDITS								= TEXT_HELP + 1;
			public static final short TEXT_EXIT									= TEXT_CREDITS + 1;
			public static final short TEXT_PAUSE								= TEXT_EXIT + 1;
			public static final short TEXT_CREDITS_TEXT							= TEXT_PAUSE + 1;
			public static final short TEXT_CONTROLS								= TEXT_CREDITS_TEXT + 1;
			public static final short TEXT_RULES							    = TEXT_CONTROLS + 1;
			public static final short TEXT_HELP_PROFILE							= TEXT_RULES + 1;
			public static final short TEXT_HELP_CONTROLS						= TEXT_HELP_PROFILE + 1;
			public static final short TEXT_HELP_OBJECTIVES						= TEXT_HELP_CONTROLS + 1;
			public static final short TEXT_HELP_PROFILE_TEXT					= TEXT_HELP_OBJECTIVES + 1;
			public static final short TEXT_HELP_IMAGEGALLERY_TEXT				= TEXT_HELP_PROFILE_TEXT + 1;
			public static final short TEXT_TURN_OFF								= TEXT_HELP_IMAGEGALLERY_TEXT + 1;
			public static final short TEXT_TURN_ON								= TEXT_TURN_OFF + 1;
			public static final short TEXT_ON									= TEXT_TURN_ON + 1;
			public static final short TEXT_OFF									= TEXT_ON + 1;
			public static final short TEXT_SOUND								= TEXT_OFF + 1;
			public static final short TEXT_VIBRATION							= TEXT_SOUND + 1;
			public static final short TEXT_CANCEL								= TEXT_VIBRATION + 1;
			public static final short TEXT_CONTINUE								= TEXT_CANCEL + 1;
			public static final short TEXT_SPLASH_NANO							= TEXT_CONTINUE + 1;
			public static final short TEXT_SPLASH_BRAND							= TEXT_SPLASH_NANO + 1;
			public static final short TEXT_LOADING								= TEXT_SPLASH_BRAND + 1;
			public static final short TEXT_DO_YOU_WANT_SOUND					= TEXT_LOADING + 1;
			public static final short TEXT_YES									= TEXT_DO_YOU_WANT_SOUND + 1;
			public static final short TEXT_NO									= TEXT_YES + 1;
			public static final short TEXT_BACK_MENU							= TEXT_NO + 1;
			public static final short TEXT_CONFIRM_BACK_MENU					= TEXT_BACK_MENU + 1;
			public static final short TEXT_EXIT_GAME							= TEXT_CONFIRM_BACK_MENU + 1;
			public static final short TEXT_CONFIRM_EXIT_1						= TEXT_EXIT_GAME + 1;
			public static final short TEXT_PRESS_ANY_KEY						= TEXT_CONFIRM_EXIT_1 + 1;
			public static final short TEXT_GAME_OVER							= TEXT_PRESS_ANY_KEY + 1;
			public static final short TEXT_RECOMMEND_TITLE						= TEXT_GAME_OVER + 1;
			public static final short TEXT_PROFILE_SELECTION					= TEXT_RECOMMEND_TITLE + 1;
			public static final short TEXT_CHOOSE_ANOTHER					    = TEXT_PROFILE_SELECTION + 1;
			public static final short TEXT_CONFIRM						        = TEXT_CHOOSE_ANOTHER + 1;
			public static final short TEXT_NO_PROFILE							= TEXT_CONFIRM + 1;
			public static final short TEXT_TIME_FORMAT							= TEXT_NO_PROFILE + 1;
			public static final short TEXT_HIGH_SCORES					        = TEXT_TIME_FORMAT + 1;
			public static final short TEXT_VOLUME						        = TEXT_HIGH_SCORES + 1;
			public static final short TEXT_VERSION						        = TEXT_VOLUME + 1;
			public static final short TEXT_PHOTO_GALLERY			            = TEXT_VERSION + 1;
			public static final short TEXT_LEVEL_CLEARED			            = TEXT_PHOTO_GALLERY + 1;
			public static final short TEXT_LEVEL_SHOW				            = TEXT_LEVEL_CLEARED + 1;
			public static final short TEXT_NEW_RECORD					        = TEXT_LEVEL_SHOW + 1;
			public static final short TEXT_RESTART					            = TEXT_NEW_RECORD + 1;
			public static final short TEXT_RECOMMEND_SENT						= TEXT_RESTART + 1;
			public static final short TEXT_RECOMMEND_TEXT						= TEXT_RECOMMEND_SENT + 1;
			public static final short TEXT_RECOMMENDED							= TEXT_RECOMMEND_TEXT + 1;
			public static final short TEXT_RECOMMEND_SMS						= TEXT_RECOMMENDED + 1;
			public static final short TEXT_RANKING_ACCUMULATED_POINTS			= TEXT_RECOMMEND_SMS + 1;
			public static final short TEXT_RANKING_MAX_POINTS					= TEXT_RANKING_ACCUMULATED_POINTS + 1;
			public static final short TEXT_RANKING_MAX_LEVEL					= TEXT_RANKING_MAX_POINTS + 1;
			public static final short TEXT_CURRENT_PROFILE						= TEXT_RANKING_MAX_LEVEL + 1;
			public static final short TEXT_SINGLEPLAYER							= TEXT_CURRENT_PROFILE + 1;
			public static final short TEXT_MULTIPLAYER							= TEXT_SINGLEPLAYER + 1;
			public static final short TEXT_GAME_MODE							= TEXT_MULTIPLAYER + 1;
			public static final short TEXT_SPECIAL_CARD_SWAP					= TEXT_GAME_MODE + 1;
			public static final short TEXT_SPECIAL_CARD_SHOW_ALL				= TEXT_SPECIAL_CARD_SWAP + 1;
			public static final short TEXT_SPECIAL_CARD_LOSE_TURN				= TEXT_SPECIAL_CARD_SHOW_ALL + 1;
			public static final short TEXT_SPECIAL_CARD_PULSATE_TWO				= TEXT_SPECIAL_CARD_LOSE_TURN + 1;
			public static final short TEXT_SPECIAL_CARD_LOSE_TIME				= TEXT_SPECIAL_CARD_PULSATE_TWO + 1;
			public static final short TEXT_SPECIAL_CARD_PULSATE_CORRESPONDING	= TEXT_SPECIAL_CARD_LOSE_TIME + 1;
			public static final short TEXT_BONUS								= TEXT_SPECIAL_CARD_PULSATE_CORRESPONDING + 1;
			public static final short TEXT_PENALTY								= TEXT_BONUS + 1;
			public static final short TEXT_START_TURN							= TEXT_PENALTY + 1;
			public static final short TEXT_PLAYER								= TEXT_START_TURN + 1;
			public static final short TEXT_GAMEOVER_TITLE						= TEXT_PLAYER + 1;
			public static final short TEXT_GAMEOVER_MSG							= TEXT_GAMEOVER_TITLE + 1;

			/** número total de textos do jogo */
			public static final short TEXT_TOTAL = TEXT_GAMEOVER_MSG + 1;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS TELAS DO JOGO">
			public static final byte SCREEN_CHOOSE_SOUND				= 0;
			public static final byte SCREEN_SPLASH_NANO					= SCREEN_CHOOSE_SOUND + 1;
			public static final byte SCREEN_SPLASH_BRAND				= SCREEN_SPLASH_NANO + 1;
			public static final byte SCREEN_SPLASH_GAME					= SCREEN_SPLASH_BRAND + 1;
			public static final byte SCREEN_MAIN_MENU					= SCREEN_SPLASH_GAME + 1;
			public static final byte SCREEN_NEW_GAME					= SCREEN_MAIN_MENU + 1;
			public static final byte SCREEN_OPTIONS						= SCREEN_NEW_GAME + 1;
			public static final byte SCREEN_HELP						= SCREEN_OPTIONS + 1;
			public static final byte SCREEN_CREDITS						= SCREEN_HELP + 1;
			public static final byte SCREEN_PAUSE						= SCREEN_CREDITS + 1;
			public static final byte SCREEN_LOADING_1					= SCREEN_PAUSE + 1;
			public static final byte SCREEN_GAME_OVER					= SCREEN_LOADING_1 + 1;
			public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_GAME_OVER + 1;
			public static final byte SCREEN_LOADING_PHOTO_GALLERY		= SCREEN_LOADING_NANO_ONLINE + 1;
			public static final byte SCREEN_PHOTO_GALLERY				= SCREEN_LOADING_PHOTO_GALLERY + 1;
			public static final byte SCREEN_GAME_OVER_MENU				= SCREEN_PHOTO_GALLERY + 1;
			public static final byte SCREEN_LOG							= SCREEN_GAME_OVER_MENU + 1;
			public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_LOG + 1;
			public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
			public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
			public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_RECOMMEND + 1;
			public static final byte SCREEN_LOADING_GAME				= SCREEN_CHOOSE_PROFILE + 1;
			public static final byte SCREEN_NEXT_LEVEL					= SCREEN_LOADING_GAME + 1;
			public static final byte SCREEN_HELP_PROFILE				= SCREEN_NEXT_LEVEL + 1;
			public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_HELP_PROFILE + 1;
			public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
			public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_LOADING_HIGH_SCORES + 1;
			public static final byte SCREEN_NANO_RANKING_MENU			= SCREEN_NANO_RANKING_PROFILES + 1;
			public static final byte SCREEN_NEW_RECORD_MENU				= SCREEN_NANO_RANKING_MENU + 1;
			public static final byte SCREEN_LOADING_MAIN_MENU			= SCREEN_NEW_RECORD_MENU + 1;
			public static final byte SCREEN_CHOOSE_GAME_TYPE			= SCREEN_LOADING_MAIN_MENU + 1;
			public static final byte SCREEN_LOADING_MAIN_MENU_NO_UNLBOCK= SCREEN_CHOOSE_GAME_TYPE + 1;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU PRINCIPAL">
			// menu principal
			public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
			public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
			public static final byte ENTRY_MAIN_MENU_NEWS		    = 2;
			public static final byte ENTRY_MAIN_MENU_NANO_ONLINE    = 3;
			public static final byte ENTRY_MAIN_MENU_RECOMMEND		= 4;
			public static final byte ENTRY_MAIN_MENU_HELP			= 5;
			public static final byte ENTRY_MAIN_MENU_CREDITS		= 6;
			public static final byte ENTRY_MAIN_MENU_EXIT			= 7;

			//#if DEBUG == "true"
				public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
			//#endif
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE AJUDA">
			public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
			public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
			public static final byte ENTRY_HELP_MENU_BACK						= 2;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE PAUSA">
			public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
			public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
			public static final byte ENTRY_PAUSE_MENU_VOLUME				= 2;
			public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 3;
			public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 4;
			public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 5;

			public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 3;
			public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 4;

			public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
			public static final byte ENTRY_OPTIONS_MENU_VOLUME					= 1;
			public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 2;

			public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 2;
			public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 3;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE FIM DE JOGO">
			public static final byte ENTRY_GAME_OVER_HIGH_SCORES	= 0;
			public static final byte ENTRY_GAME_OVER_NEW_GAME		= 1;
			public static final byte ENTRY_GAME_OVER_BACK_MENU		= 2;
			public static final byte ENTRY_GAME_OVER_EXIT_GAME		= 3;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE SELEÇÃO DE PERFIL">
			public static final byte ENTRY_CHOOSE_PROFILE_USE_CURRENT	= 0;
			public static final byte ENTRY_CHOOSE_PROFILE_CHANGE		= 1;
			public static final byte ENTRY_CHOOSE_PROFILE_HELP			= 2;
			public static final byte ENTRY_CHOOSE_PROFILE_BACK			= 3;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE NOTÍCIAS">
			public static final byte ENTRY_NEWS_GENERAL		= 0;
			public static final byte ENTRY_NEWS_SCORERS		= 1;
			public static final byte ENTRY_NEWS_TABLE		= 2;
			public static final byte ENTRY_NEWS_BACK		= 3;
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU INICIAL DE SOM">
			public static final byte ENTRY_SOUND_ON_OFF		= 0;
			public static final byte ENTRY_SOUND_VOLUME		= 1;
			public static final byte ENTRY_SOUND_OK			= 2;
		// </editor-fold>

	// </editor-fold>


	//<editor-fold desc="CONSTANTES DO JOGO">
		/** Duração mínima da exibição da carta. */
		public static final short TIME_CARD_FLIP = 300;

		//multiplicador maximo de combos
		public static final byte MAX_COMBO = 99;

		//indice do tipo de mensagem: se for par, bonus
		public static final byte BONUS_BONUS = 0;

		//indice do tipo de mensagem: se for impar, penalidade
		public static final byte BONUS_PENALTY = 1;

		public static final int TOTAL_GIRLS_PHOTOS = 15;

		// Tempo de espera no splash
		public static final short TITLE_WAIT_TIME = 550;

		// Sprite do cursor pulsando
		public static final int CURSOR_SPRITE = 0;

		// Sprite da carta virando
		public static final int CARD_FLIP_SPRITE = 1;

		// Animação da carta virando
		public static final int CARD_FLIP_SEQUENCE = 0;

		public static final int FP_CARD_INTERNAL_BORDER_PERCENT = ( NanoMath.ONE * 925 ) / 1000;
	//</editor-fold>

	
	//#if SCREEN_SIZE == "SMALL"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA PEQUENA">
//#
//# 			//<editor-fold desc="K10 CONSTS">
//# 				public static final int SAFE_MARGIN = 3; // Margem de segurança para alinahmento de objetos nos limites da tela.
//# 				public static final int MENU_BOTTOM_BAR_HEIGHT = ( SAFE_MARGIN << 1 ) + 20; // ( SAFE_MARGIN * 2 ) + BUTTON_SIZE
//# 				public static final int MENU_CARD_HEIGHT = 70;
//# 				public static final int MENU_CARD_WIDTH = ( MENU_CARD_HEIGHT << 1 ) / 3 ;
//# 			//</editor-fold>
//#
//#
//# 			//<editor-fold desc="MONTY CONSTS">
//# 				public static final int BORDER_SIZE = 5;
//#
//# 				public static final int COMBO_BALL_X = 2;
//#
//# 				public static final int DEFAULT_PADDING_WIDTH = 5;
//#
//# 				public static final int DEFAULT_PADDING_HEIGHT = 0;
//#
//# 				public static final int DEFAULT_PADDING_TEXT_HEIGHT = 2;
//#
//# 				public static final int PAUSE_BUTTON_WIDTH = 42;
//#
//# 				/** Largura padrão das cartas dos menus. */
//# 				public static final int CARD_DEFAULT_WIDTH = 40;
//#
//# 				/** Altura padrão das cartas dos menus. */
//# 				public static final int CARD_DEFAULT_HEIGHT = 60;
//#
//# 				/** Espaçamento entre as cartas nos menus. */
//# 				public static final int CARDS_SPACING = 8;
//#
//# 				public static final int BASE_SCORE_UNIT = 10;
//#
//# 				//public static final int FP_CARD_INTERNAL_BORDER_PERCENT = NanoMath.ONE * 9 / 10;
//# 			//</editor-fold>
//#
//# 	//</editor-fold>
//#
	//#elif SCREEN_SIZE == "MEDIUM"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍ?FICAS PARA TELA MÉDIA">
//#
//# 			//<editor-fold desc="K10 CONSTS">
//# 				public static final int SAFE_MARGIN = 5; // Margem de segurança para alinahmento de objetos nos limites da tela.
//# 				public static final int MENU_BOTTOM_BAR_HEIGHT = ( SAFE_MARGIN << 1 ) + 20; // ( SAFE_MARGIN * 2 ) + BUTTON_SIZE
//# 			public static final int MENU_CARD_HEIGHT = 100;
//# 			public static final int MENU_CARD_WIDTH = ( MENU_CARD_HEIGHT << 1 ) / 3 ;
//# 			//</editor-fold>
//#
//#
//# 			//<editor-fold desc="MONTY CONSTS">
//#  				public static final int BORDER_SIZE = 8;
//#
//# 				public static final int COMBO_BALL_X = SAFE_MARGIN;
//#
//# 				public static final int DEFAULT_PADDING_WIDTH = - SAFE_MARGIN >> 1;
//#
//# 				public static final int DEFAULT_PADDING_HEIGHT = SAFE_MARGIN;
//#
//# 				public static final int PAUSE_BUTTON_WIDTH = 46;
//#
//# 				public static final int DEFAULT_PADDING_TEXT_HEIGHT = 2;
//#
//# 				/** Largura padrão das cartas dos menus. */
//# 				public static final int CARD_DEFAULT_WIDTH = 26;
//#
//# 				/** Altura padrão das cartas dos menus. */
//# 				public static final int CARD_DEFAULT_HEIGHT = 40;
//#
//# 				/** Espaçamento entre as cartas nos menus. */
//# 				public static final int CARDS_SPACING = 8;
//#
//# 				public static final int BASE_SCORE_UNIT = 10;
//#
//# 				//public static final int FP_CARD_INTERNAL_BORDER_PERCENT = NanoMath.ONE * 9 / 10;
//# 			//</editor-fold>
//#
//#  	//</editor-fold>
//#
	//#elif SCREEN_SIZE == "BIG"
	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE">

		//<editor-fold desc="K10 CONSTS">
			public static final int SAFE_MARGIN = 7; // Margem de segurança para alinahmento de objetos nos limites da tela.
			public static final int MENU_BOTTOM_BAR_HEIGHT = ( SAFE_MARGIN << 1 ) + 35; // ( SAFE_MARGIN * 2 ) + BUTTON_SIZE
			public static final int KEYPAD_MINIMUM_SIZE = 90;
			public static final int MENU_CARD_HEIGHT = 150;
			public static final int MENU_CARD_WIDTH = ( MENU_CARD_HEIGHT << 1 ) / 3 ;
		//</editor-fold>


		//<editor-fold desc="MONTY CONSTS">
 			public static final int BORDER_SIZE = 11;

			public static final int COMBO_BALL_X = SAFE_MARGIN;

			public static final int DEFAULT_PADDING_TEXT_WIDTH = SAFE_MARGIN;

			public static final int PAUSE_BUTTON_WIDTH = 64;

			public static final int DEFAULT_PADDING_WIDTH = SAFE_MARGIN + 1;

			public static final int DEFAULT_PADDING_HEIGHT = SAFE_MARGIN;

			public static final int DEFAULT_PADDING_TEXT_HEIGHT = 6;

			public static final int SCREEN_PREFERED_X = 320;

			public static final int SCREEN_PREFERED_Y = 240;

			/** Largura da borda das cartas.*/
			public static final int CARD_BORDER_WIDTH = 3;
			/** Largura padrão das cartas dos menus. */
			public static final int CARD_DEFAULT_WIDTH = 40;

			/** Altura padrão das cartas dos menus. */
			public static final int CARD_DEFAULT_HEIGHT = 60;

			/** Espaçamento entre as cartas nos menus. */
			public static final int CARDS_SPACING = 8;

			public static final int BASE_SCORE_UNIT = 10;

			//public static final int FP_CARD_INTERNAL_BORDER_PERCENT = NanoMath.ONE * 9 / 10;
		//</editor-fold>

	//</editor-fold>
	//#elif SCREEN_SIZE == "GIANT"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE">
//#
//# 		//<editor-fold desc="K10 CONSTS">
//# 			public static final int SAFE_MARGIN = 10; // Margem de segurança para alinahmento de objetos nos limites da tela.
//# 			public static final int MENU_BOTTOM_BAR_HEIGHT = ( SAFE_MARGIN << 1 ) + 35; // ( SAFE_MARGIN * 2 ) + BUTTON_SIZE
//# 			public static final int KEYPAD_MINIMUM_SIZE = 120;
//# 			public static final int MENU_CARD_HEIGHT = 220;
//# 			public static final int MENU_CARD_WIDTH = ( MENU_CARD_HEIGHT << 1 ) / 3 ;
//# 		//</editor-fold>
//#
//#
//# 		//<editor-fold desc="MONTY CONSTS">
//#  			public static final int BORDER_SIZE = 11;
//#
//# 			public static final int COMBO_BALL_X = SAFE_MARGIN;
//#
//#  			public static final int DEFAULT_PADDING_WIDTH = 5;
//#
//#  			public static final int DEFAULT_PADDING_HEIGHT = 5;
//# 			/***/
//# 			public static final int DEFAULT_PADDING_TEXT_HEIGHT = 0;
//# 			/***/
//#  			public static final int PAUSE_BUTTON_WIDTH = 64;
//#
//# 			/** Largura padrão das cartas dos menus. */
//# 			public static final int CARD_DEFAULT_WIDTH = 40;
//#
//# 			/** Altura padrão das cartas dos menus. */
//# 			public static final int CARD_DEFAULT_HEIGHT = 60;
//#
//# 			/** Espaçamento entre as cartas nos menus. */
//# 			public static final int CARDS_SPACING = 8;
//#
//# 			public static final int BASE_SCORE_UNIT = 10;
//#
//# 			//public static final int FP_CARD_INTERNAL_BORDER_PERCENT = ( NanoMath.ONE * 95 ) / 100;
//# 		//</editor-fold>
//#
//# 	//</editor-fold>
//#
//#
	//#endif

}
