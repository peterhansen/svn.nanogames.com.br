/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import java.lang.Short;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public class SexyButton extends UpdatableGroup implements Constants
{
	private static DrawableImage BOARD_LEFT;
	private static DrawableImage BOARD_CENTER;
	private static DrawableImage BOARD_RIGHT;

	private final DrawableImage boardLeft;
	private final Pattern boardCenter;
	private final DrawableImage boardRight;
	private final MarqueeLabel label;
	private boolean autoSize;

	private int maxWidth;

	public SexyButton( String text , boolean autoSize, int font ) throws Exception {
		super( 4 );

		if( BOARD_LEFT == null ) {
			BOARD_LEFT = new DrawableImage( PATH_IMAGES + "board_left.png" );
		}
		boardLeft = new DrawableImage( BOARD_LEFT );
		insertDrawable( boardLeft );

		if( BOARD_CENTER == null ) {
			BOARD_CENTER = new DrawableImage( PATH_IMAGES + "board_center.png" );
		}
		boardCenter = new Pattern( new DrawableImage( BOARD_CENTER ) );
		insertDrawable( boardCenter );

		if( BOARD_RIGHT == null ) {
			BOARD_RIGHT = new DrawableImage( PATH_IMAGES + "board_right.png" );
		}
		boardRight = new DrawableImage( BOARD_RIGHT );
		insertDrawable( boardRight );

		label = new MarqueeLabel( GameMIDlet.GetFont( font ), text );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		insertDrawable( label );

		maxWidth = Short.MAX_VALUE;
		this.autoSize = autoSize;

		setSize();
	}


	public void setFont( int fontId ) {
		label.setFont( fontId );

		if( autoSize ) {
			setPosition( getPosX() + ( ( getWidth() - ( label.getWidth() + ( boardLeft.getWidth() << 1 ) ) ) >> 1 ), getPosY() );
			setSize();
		} else {
			setSize( getWidth(), getHeight() );
		}
	}


	public void setText( int textId ){
		setText( GameMIDlet.getText( textId ) );
	}


	public void setText( String text ){
		label.setText( text );

		if( autoSize ) {
			setPosition( getPosX() + ( ( getWidth() - ( label.getWidth() + ( boardLeft.getWidth() << 1 ) ) ) >> 1 ), getPosY() );
			setSize();
		} else {
			setSize( getWidth(), getHeight() );
		}
	}


	public void setMaxWidth( int maxWidth ) {
		this.maxWidth = maxWidth;
		setSize( getWidth(), getHeight() );
	}


	public final void setSize() {
		setSize( label.getWidth() + ( boardLeft.getWidth() << 1 ), boardLeft.getHeight() );
	}


	public void setSize( int width, int height ) {
		//#if SCREEN_SIZE == "SMALL"
//# 			width -= 2;
		//#endif

		super.setSize( NanoMath.min( width, maxWidth ), height );
		defineReferencePixel( ANCHOR_CENTER );


//#if SCREEN_SIZE == "SMALL"
//# 		boardLeft.setPosition( 4 , ( height - boardLeft.getHeight() ) >> 1 );
//# 		boardCenter.setSize( width - ( boardLeft.getWidth() << 1 ) , boardLeft.getHeight() );
//# 		boardCenter.setPosition( -2 + boardLeft.getPosX() + boardLeft.getWidth(), boardLeft.getPosY() );
//# 		boardRight.setPosition( -2 + boardCenter.getPosX() + boardCenter.getWidth() , boardLeft.getPosY() );
//#else
	boardLeft.setPosition( 0 , ( height - boardLeft.getHeight() ) >> 1 );
	boardCenter.setSize( width - ( boardLeft.getWidth() << 1 ) , boardLeft.getHeight() );
	boardCenter.setPosition( boardLeft.getPosX() + boardLeft.getWidth(), boardLeft.getPosY() );
	boardRight.setPosition( boardCenter.getPosX() + boardCenter.getWidth() , boardLeft.getPosY() );
//#endif

		label.setText( label.getText(), true );

		//#if SCREEN_SIZE == "SMALL"
//# 			label.setSize( 2 +  NanoMath.min( boardCenter.getWidth(), label.getWidth() ), label.getHeight() );
//# 			label.setPosition( ( boardCenter.getPosX() + ( boardCenter.getWidth() >> 1 ) ) - ( label.getWidth() >> 1 ), ( ( boardCenter.getPosY() + ( boardCenter.getHeight() >> 1 ) ) - ( label.getHeight() >> 1 ) ) - 1 );
		//#else
		label.setSize( NanoMath.min( boardCenter.getWidth(), label.getWidth() ), label.getHeight() );
		label.setPosition( ( boardCenter.getPosX() + ( boardCenter.getWidth() >> 1 ) ) - ( label.getWidth() >> 1 ), ( ( boardCenter.getPosY() + ( boardCenter.getHeight() >> 1 ) ) - ( label.getHeight() >> 1 ) ) - 1 );
		//#endif
	}
}