/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public class ImageBuffer implements Constants {
	public final static byte TYPE_BACK_CARD = 0;
	public final static byte TYPE_BACK_CARD_SPECIAL = 1;

	private Image backCard;
	private Image backCardSpecial;
	private Image[] backCardScaleImage;
	private Image[] backCardScaleSpecialImage;
	private static ImageBuffer instance;
	
	private ImageBuffer() throws Exception {
		backCard = ImageLoader.loadImage( PATH_IMAGES + "carta_base.png" );
		backCardScaleImage = new Image[ 10 ];
		// TODO passar para constantes ou achar numero em funação da memória disponivel

		backCardSpecial = ImageLoader.loadImage( PATH_IMAGES + "carta_especial.png" );
		backCardScaleSpecialImage = new Image[ 4 ];
	}
	
	public static void init() {
		if( instance == null ) {
			try {
				instance = new ImageBuffer();
			} catch( Throwable t ) {
				//#if DEBUG == "true"
					t.printStackTrace();
				//#endif
			}
		}
	}
	
	
	public final static void cleanUp() {
		if( instance != null)
			instance.clean();
	}
	
	
	private void clean() {
		for( int i = 0; i < backCardScaleImage.length && backCardScaleImage[i] != null; i++ ) {
			backCardScaleImage[i] = null;
		}
	}
	
	
	public final static Image getImage( int type, int width, int height, int TOLERANCE ) {
		if( instance == null)
			init();
		
		return instance.getImg( type, width, height, TOLERANCE );
	}
	
	
	private final Image getImg( int type, int width, int height, int TOLERANCE ) { //TODO passar TOLERANCE para constantes
		if ( width <= 0 || height <= 0 )
			return null;

		int i = 0;
		Image last = null;
		final int sizeArea = width * height;

		switch ( type ) {
			case TYPE_BACK_CARD:
//				int best = -1;
//				int bestDiff = Short.MAX_VALUE;
//				int diff;
//
//				for( ; i < backCardScaleImage.length && backCardScaleImage[i] != null; i++ ) {
//					if( NanoMath.abs( backCardScaleImage[i].getWidth() - width ) <= TOLERANCE && NanoMath.abs( backCardScaleImage[i].getHeight() - height ) <= TOLERANCE ) {
//						diff = backCardScaleImage[i].getWidth() * backCardScaleImage[i].getHeight();
//						if( diff < bestDiff ) {
//							bestDiff = diff;
//							best = i + 1;
//						}
//					}
//					if( last != null ) {
//						final Image aux = backCardScaleImage[i];
//						backCardScaleImage[i] = last;
//						last = aux;
//					} else {
//						last = backCardScaleImage[i];
//						backCardScaleImage[i] = null;
//					}
//				}
//
//				if( best > -1 ) {
//					if( best < backCardScaleImage.length ) {
//						backCardScaleImage[0] = backCardScaleImage[best];
//						i = best;
//						for( ; i < ( backCardScaleImage.length - 1 ) && backCardScaleImage[ i + 1 ] != null; i++ ) {
//							backCardScaleImage[i] = backCardScaleImage[ i + 1 ];
//						}
//
//						if( i == ( backCardScaleImage.length - 1 ) ) {
//							backCardScaleImage[i] = last;
//						} else {
//							backCardScaleImage[i] = null;
//						}
//					} else {
//						backCardScaleImage[0] = last;
//					}
//				} else {
//					backCardScaleImage[0] = ImageLoader.scaleImage( backCard, width, height );
//				}
//
//				return backCardScaleImage[0];

				for( ; i < backCardScaleImage.length && backCardScaleImage[i] != null; i++ ) {
					if( NanoMath.abs( backCardScaleImage[i].getWidth() - width ) <= TOLERANCE && NanoMath.abs( backCardScaleImage[i].getHeight() - height ) <= TOLERANCE ) {
						backCardScaleImage[0] = backCardScaleImage[i];
						if( last != null ) {
							backCardScaleImage[i] = last;
						}
						//#if DEBUG == "true"
						//System.out.println( "*---> imagem obitida no buffer de " + backCardScaleImage[0].getWidth() +","+ backCardScaleImage[0].getHeight() );
						//#endif
						return backCardScaleImage[0];
					}
					if( last != null ) {
						final Image aux = backCardScaleImage[i];
						backCardScaleImage[i] = last;
						last = aux;
					} else {
						last = backCardScaleImage[i];
					}
				}

				if ( i < backCardScaleImage.length ) {
					backCardScaleImage[i] = last;
				}

				//#if DEBUG == "true"
				//System.out.println( "*-*-> imagem criada no buffer de " + width +","+ height );
				//#endif
				backCardScaleImage[0] = ImageLoader.scaleImage( backCard, width, height );

				return backCardScaleImage[0];

			case TYPE_BACK_CARD_SPECIAL:
				for( ; i < backCardScaleSpecialImage.length && backCardScaleSpecialImage[i] != null; i++ ) {
					if( NanoMath.abs( backCardScaleSpecialImage[i].getWidth() - width ) <= TOLERANCE && NanoMath.abs( backCardScaleSpecialImage[i].getHeight() - height ) <= TOLERANCE ) {
						backCardScaleSpecialImage[0] = backCardScaleSpecialImage[i];
						if( last != null ) {
							backCardScaleSpecialImage[i] = last;
						}
						//#if DEBUG == "true"
//						System.out.println( "*---> imagem obitida no buffer de " + backCardScaleImage[0].getWidth() +","+ backCardScaleImage[0].getHeight() );
						//#endif
						return backCardScaleSpecialImage[0];
					}
					if( last != null ) {
						final Image aux = backCardScaleSpecialImage[i];
						backCardScaleSpecialImage[i] = last;
						last = aux;
					} else {
						last = backCardScaleSpecialImage[i];
					}
				}

				if ( i < backCardScaleSpecialImage.length ) {
					backCardScaleSpecialImage[i] = last;
				}

				//#if DEBUG == "true"
//				System.out.println( "*-*-> imagem criada no buffer de " + width +","+ height );
				//#endif
				backCardScaleSpecialImage[0] = ImageLoader.scaleImage( backCardSpecial, width, height );

				return backCardScaleSpecialImage[0];
		}

		return null;
	}
}
