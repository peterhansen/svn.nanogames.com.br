/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MediaPlayer;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public class StateLabel  extends UpdatableGroup implements Constants {
	public static final byte OPTIONS_0_100_DEFAULT_STEP = 10;

	public static final byte OPTIONS_STATE_NONE = 0;
	public static final byte OPTIONS_STATE_ON_OF = 1;
	public static final byte OPTIONS_STATE_0_100 = 2;

	public static final byte OPTIONS_TYPES = 3;
	
	public final int text;
	public final int type;
	public byte value = 0;
	
	//Statet on off 
	public final MarqueeLabel on;
	public final MarqueeLabel off;
	public final DrawableImage selection;
	
	//Statet 0_100
	public final DrawableImage bkgVol;
	public final DrawableImage volume;

	public StateLabel( int stateType, int typeText ) throws Exception {
		super( 3 );

		this.text = typeText;
		this.type = stateType;

		switch( type ) {
			case OPTIONS_STATE_ON_OF:
				bkgVol = null;
				volume = null;
				on = new MarqueeLabel( FONT_TEXT, TEXT_ON );
				on.setSize( on.getTextWidth(), on.getHeight() );
				on.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				insertDrawable( on );
				off = new MarqueeLabel( FONT_TEXT, TEXT_OFF );
				off.setSize( off.getTextWidth(), off.getHeight() );
				off.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				insertDrawable( off );
				selection = new DrawableImage( PATH_IMAGES + "argola.png" );
				selection.defineReferencePixel( ANCHOR_CENTER );
				insertDrawable( selection );
			break;

			case OPTIONS_STATE_0_100:
				bkgVol = new DrawableImage( PATH_IMAGES + "volume_bar_empty.png" );
				insertDrawable( bkgVol );
				volume = new DrawableImage( PATH_IMAGES + "volume_bar_full.png" );
				insertDrawable( volume );
				on = null;
				off = null;
				selection = null;
			break;

			default:
				bkgVol = null;
				volume = null;
				on = null;
				off = null;
				selection = null;
			break;
		}

		updateValue();
		setSize();
	}


	public final void changeState() {
		switch ( text ) {
			case TEXT_VOLUME:
				//#if DEBUG == "true"
					System.out.println( "MediaPlayer.getVolume() was " + MediaPlayer.getVolume() );
				//#endif

				MediaPlayer.setVolume( ( OPTIONS_0_100_DEFAULT_STEP + ( ( MediaPlayer.getVolume() ) % 100 ) ) );

				//#if DEBUG == "true"
					System.out.println( "MediaPlayer.setVolume( " + OPTIONS_0_100_DEFAULT_STEP + " + ( ( "+ MediaPlayer.getVolume() + " ) % 100 ) ) " );
					System.out.println( "MediaPlayer.getVolume() is " + MediaPlayer.getVolume() + " but should be " + ( OPTIONS_0_100_DEFAULT_STEP + ( ( MediaPlayer.getVolume() ) % 100 ) ) );
				//#endif

				MediaPlayer.play( SOUND_DEFAULT );
			break;

			case TEXT_VIBRATION:
				MediaPlayer.setVibration( !MediaPlayer.isVibration() );
				MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
			break;

			case TEXT_SOUND:
				MediaPlayer.setMute( !MediaPlayer.isMuted() );
				MediaPlayer.play( SOUND_DEFAULT );
			break;
		}

		updateValue();
		updateEntry();
	}


	private final static byte booleanToValue( boolean bool ) {
		return (byte) ( bool ? 1 : 0 ) ;
	}


	private final boolean valueToBoolean() {
		return ( value > 0 ) ;
	}


	private final void updateValue() {
		switch ( text ) {
			case TEXT_VOLUME:
				value = MediaPlayer.getVolume();
			break;

			case TEXT_VIBRATION:
				value = booleanToValue( MediaPlayer.isVibration() );
			break;

			case TEXT_SOUND:
				value = booleanToValue( !MediaPlayer.isMuted() );
			break;
		}
	}


	private void updateEntry() {
		switch( type ) {
			case OPTIONS_STATE_ON_OF:
				if( valueToBoolean() ) {
					selection.setRefPixelPosition( on.getPosX() + ( on.getWidth() >> 1 ), on.getPosY() + ( on.getHeight() >> 1 ) );
				} else {
					selection.setRefPixelPosition( off.getPosX() + ( off.getWidth() >> 1 ), off.getPosY() + ( off.getHeight() >> 1 ) );
				}
			break;

			case OPTIONS_STATE_0_100:
			default:
				volume.setSize( ( bkgVol.getWidth() * value ) / 100, volume.getHeight() );
			break;
		}
	}

	public void setSize() {
		switch( type ) {
			case OPTIONS_STATE_ON_OF:
				setSize( on.getWidth() + ( selection.getWidth() >> 1 ) + off.getHeight() + selection.getWidth() , selection.getHeight() );
			break;

			case OPTIONS_STATE_0_100:
				setSize( bkgVol.getWidth(), bkgVol.getHeight() );
			break;
		}
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height);

		switch( type ) {
			case OPTIONS_STATE_ON_OF:
				final int w = on.getWidth() + ( selection.getWidth() >> 1 ) + off.getHeight();
				on.setPosition( ( width - w ) >> 1, ( ( selection.getHeight() - on.getHeight() ) >> 1 ) );
				off.setPosition( on.getPosX() + on.getWidth() + ( selection.getWidth() >> 1 ), ( ( selection.getHeight() - off.getHeight() ) >> 1 ));
			break;

			case OPTIONS_STATE_0_100:
			default:
				bkgVol.setPosition( ( width - bkgVol.getWidth() ) >> 1, ( height - bkgVol.getHeight() ) );
				volume.setPosition( bkgVol.getPosX(), bkgVol.getPosY() );
			break;
		}

		updateEntry();
	}
}
