/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public class Background extends UpdatableGroup implements Constants, ScreenListener, Updatable {
	
	private final int BKG_COLOR = 0xC50000;
	private final Pattern bkg;

	private final Rectangle[] viewports = new Rectangle[4];
	private boolean usingMultipleViewports = false;


	public Background() throws Exception {
		super( 1 );
		//#if SCREEN_SIZE != "SMALL"
			if (!GameMIDlet.isLowMemory()){
				bkg = new Pattern( new DrawableImage( PATH_IMAGES + "bkg_pattern.png" ) );
				insertDrawable( bkg );
			} else
		//#endif
				bkg = null;


		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void setSize( int width, int height ) {
		width = NanoMath.max( width, 10 );
		height = NanoMath.max( height, 10 );
		super.setSize( width, height );

		//#if SCREEN_SIZE != "SMALL"
		if (bkg != null)
			bkg.setSize( width, height );
		//#endif
	}


	public void setViewportBlocker( Rectangle r ) {
		viewports[0] =  new Rectangle( 0, 0, getWidth(), r.y );
		viewports[1] =  new Rectangle( 0, r.y, r.x, r.height );
		viewports[2] =  new Rectangle( 0, ( r.y + r.height ), getWidth(), ( getHeight() - ( r.y + r.height ) ) );
		viewports[3] =  new Rectangle( ( r.x + r.width ), r.y, getWidth() - ( r.x + r.width ) , r.height );
		usingMultipleViewports = true;
	}


	public void setViewport( Rectangle r, boolean b ) {
		usingMultipleViewports = false;
		super.setViewport( r );
	}


	public void setUnblockViewport( ) {
		setViewport( new Rectangle( getPosX(), getPosY(), getWidth(), getHeight() ), false );
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void update( int delta ) {
		if ( getWidth() != ScreenManager.SCREEN_WIDTH || getHeight() != ScreenManager.SCREEN_HEIGHT )
			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public void draw( Graphics g ) {
		if (bkg == null){
 			g.setColor( BKG_COLOR );
 			g.fillRect( translate.x, translate.y, getWidth(), getHeight() );
 			super.draw( g );
		} else {
			if( usingMultipleViewports && viewports != null ) {
				for( int i = 0; i < viewports.length; i++ ) {
					setViewport( viewports[i] );
					super.draw( g );
				}
			} else {
				super.draw( g );
			}
		}
	}


	public void paint( Graphics g ) {
		//#if SCREEN_SIZE != "SMALL"
		if ( bkg != null ) {

			if ( usingMultipleViewports && viewports != null ) {
				for ( int i = 0; i < viewports.length; i++ ) {
					setViewport( viewports[i] );
					super.paint( g );
				}
			} else {
				super.paint( g );
			}

		} else 
		//#endif
			super.paint( g );
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}
}