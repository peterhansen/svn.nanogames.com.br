/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class Button extends SexyButton implements Selectable{
	private Image img;
	private int[] data;
	private boolean selected;
	private boolean changed;
	private MenuListener listener;
	private int screenID;
	private int textID;

	public Button( int textID, MenuListener listener, int screenID ) throws Exception {
		super( GameMIDlet.getText( textID ), true, FONT_TITLE );

		this.listener = listener;
		this.screenID = screenID;
		this.textID = textID;

		changed = true;
	}

	public final void draw( Graphics g ) {
		if( changed ) {
			renewSelectedImage( g );
			changed = false;
		} else {
			g.drawRGB( data, 0, getWidth(), translate.x + getPosX(), translate.y + getPosY(), getWidth(), getHeight(), true );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( img == null || getWidth() != img.getWidth() || getHeight() != img.getHeight() ) {
			final int DATA_LENGTH = getWidth() * getHeight();
			data = null;
			img = null;
			img = Image.createImage( getWidth(), getHeight() );
			data = new int[ DATA_LENGTH ];
			changed = true;
		}
	}


	public final void renewSelectedImage( Graphics g ) {
		final Graphics g2 = img.getGraphics();
		g2.setColor( 0xff00ff );
		g2.fillRect( 0, 0, getWidth(), getHeight() );
		final Point previousTranslate = new Point( translate );

		translate.set( -getPosX(), -getPosY() );
		super.draw( g2 );
		translate.set( previousTranslate );
		img.getRGB( data, 0, getWidth(), 0, 0, getWidth(), getHeight() );
		final int magenta = g.getDisplayColor(0x00ff00ff) & 0x00ff00ff ;
		for ( int i = 0; i < data.length; ++i ) {
			final int color = g.getDisplayColor(data[i]);
			if ( ( color & 0x00ff00ff ) == magenta ) {
				data[ i ] = 0;
			} else {
				data[ i ] = 0xff000000 | Drawable.changeColorLightness( color, selected ? 0 : -85 );
//								data[ i ] &= ( ( NanoMath.clamp( i % getWidth(), 0, 0xff ) ) << 24 ) | 0x00ffffff;
			}
		}
		g.drawRGB( data, 0, getWidth(), translate.x + getPosX(), translate.y + getPosY(), getWidth(), getHeight(), true );
	}


	public final void select() {
		selected = true;
		changed = true;
	}


	public final void unselect() {
		selected = false;
		changed = true;
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				listener.onChoose( null, screenID, textID );
			break;
		}
	}


	public TextBox textBox() {
		return null;
	}
}