package util;


/**
 * Um objeto tem que ser comparavel com outros em diversas situações. Esta interface
 * Tenta reforçar esta habilidade
 * @author Daniel Monteiro
 */
public interface ComparableObject {


	/**
	 * Compara um objeto com outro
	 * @param other é o outro objeto ao qual se deseja comparar
	 * @return util.Constants.SMALLER( -1 ), util.Constants.SIMILAR( 0 ) ou util.Constants.BIGGER( 1 )	 *
	 */
	public byte compareWith( ComparableObject other );
}
