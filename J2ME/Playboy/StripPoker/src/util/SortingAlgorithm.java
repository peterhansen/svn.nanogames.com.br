package util;


/**
 * Indica um algoritmo de ordenação genérico.
 * @author Daniel Monteiro
 */
public class SortingAlgorithm {
//<editor-fold desc="Constantes de ordenação">


	/**
	 * 
	 */
	public static final byte SMALLER = -1;
	/**
	 * 
	 */
	public static final byte SIMILAR = 0;
	/**
	 * 
	 */
	public static final byte BIGGER = 1;
//</editor-fold>
//<editor-fold desc="Métodos da classe">


	/**
	 * Ordena os objetos
	 * @param bunch é um array de objetos que implementam a interface ComparableObject
	 */
	public static void sort( ComparableObject[] bunch ) {
		//#if DEBUG == "true"
		System.out.println( "SortingAlgorithm.sort chamada!" );
		//#endif
	}
//</editor-fold>
}
