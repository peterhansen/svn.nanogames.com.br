package util;
//<editor-fold desc="Imports">
import br.com.nanogames.components.util.NanoMath;
//</editor-fold>


/**
 * Heapsort genérico
 * @author Daniel Monteiro
 */
public class HeapSort extends SortingAlgorithm {
	//<editor-fold desc="Métodos da classe">


	/**
	 * Ordena os objetos. Assume que os objetos são agregadas todos juntos, sem buracos.
	 * @param bunch é um array de objetos que implementam a interface ComparableObject
	 */
	public static void sort( ComparableObject[] bunch ) {
		int size = 0;

		for ( byte d = 0; d < bunch.length; ++d ) {
			if ( bunch[d] != null ) {
				size++;
			}
		}

		buildHeap( bunch, size );

		for ( int c = size - 1; c > 0; --c ) {
			//O topo do heap sempre conterá o maior item.
			//Eu posso sempre pega-lo e jogar para o final,
			//desde que eu retire o ultimo elemento e o coloque em outro lugar.
			//coloca-lo no topo do heap resolve o problema
			ComparableObject tmp = bunch[ 0];
			bunch[ 0] = bunch[c];
			bunch[c] = tmp;
			//faz com que o maior elemento esteja além dos limites do heap
			size--;
			//e reconstroi, de modo que o topo novamente esteja ocupado pelo maior
			//item.
			heapfy( bunch, size, 0 );
		}
	}


	/**
	 * Monta o monte ( heap )
	 * @param bunch é o agregado homogeneo de objetos comparaveis que será
	 * transformado numa arvore binária especial, chamada Heap
	 * @param size é a extensão da arvore. Nem sempre ela é o vetor todo.
	 */
	private static void buildHeap( ComparableObject[] bunch, int size ) {
		for ( int c = NanoMath.floor( NanoMath.divFixed( NanoMath.toFixed( size ), NanoMath.toFixed( 2 ) ) ); c >= 0; c-- ) {
			heapfy( bunch, size, c );
		}
	}


	/**
	 *
	 * @param bunch
	 * @param size
	 * @param rootIndex
	 */
	private static void heapfy( ComparableObject[] bunch, int size, int rootIndex ) {
		int toChange = rootIndex;


		if ( ( ( 2 * rootIndex ) < size ) && bunch[toChange].compareWith( bunch[ 2 * rootIndex] ) == SortingAlgorithm.BIGGER ) {
			toChange = 2 * rootIndex;
		}

		if ( ( ( 2 * rootIndex + 1 ) < size ) && bunch[toChange].compareWith( bunch[ 2 * rootIndex + 1] ) == SortingAlgorithm.BIGGER ) {
			toChange = 2 * rootIndex + 1;
		}

		if ( toChange != rootIndex ) {

			ComparableObject tmp;

			tmp = bunch[rootIndex];
			bunch[rootIndex] = bunch[toChange];
			bunch[toChange] = tmp;

			heapfy( bunch, size, toChange );
		}
	}
	//</editor-fold>
}
