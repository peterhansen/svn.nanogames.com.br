package core;
//<editor-fold desc="Imports">
import br.com.nanogames.components.util.Serializable;
import core.UI.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;
//</editor-fold>


/**
 * Classe genérica de persistência de dados em jogos
 * @author Daniel "Monty" Monteiro
 */
public class PersistentGameInformation implements Constants, Serializable {
	//<editor-fold desc="Campos da classe">
	/**
	 * 
	 */
	private static byte maxLevel;
	/**
	 * Quantas "vidas" o jogador ainda tem
	 */
	private byte triesLeft;
	/**
	 * Em qual nível o jogador esta
	 */
	private byte level;
	/**
	 * Pontos do jogador (repare que eu nao chamei de score por ser algo mais
	 * generico).
	 */
	private long points;
	/**
	 * Jogador atual
	 */
	private byte currentPlayer;
	/**
	 * Média de pontos por partida
	 */
	private short average;
	/**
	 * Instancia do singleton
	 */
	private static PersistentGameInformation instance;
	public static byte DEFAULT_START_LEVEL;
	public static byte DEFAULT_TRIES_LEFT;
	public static long DEFAULT_POINTS;
	public static byte DEFAULT_PLAYER;
	public static short DEFAULT_AVERAGE;
	//</editor-fold>
	//<editor-fold desc="Métodos da classe">


	/**
	 * Construtor, por enquanto só define valores padrões
	 */
	private PersistentGameInformation() {
		//<editor-fold desc="inicializacao temporaria">
		triesLeft = Byte.MIN_VALUE;
		level = Byte.MIN_VALUE;
		points = Byte.MIN_VALUE;
		currentPlayer = Byte.MIN_VALUE;
		average = Short.MIN_VALUE;
		//</editor-fold>
	}


	/**
	 * Método de acesso do singleton
	 * @return a instância única
	 */
	public static final PersistentGameInformation getInstance() {

		if ( instance == null ) {
			instance = new PersistentGameInformation();
		}

		return instance;
	}


	/**
	 * Grava os dados num stream
	 * @param output O stream de saída
	 * @throws Exception
	 */
	public void write( DataOutputStream output ) throws Exception {
		try {
//			output.writeByte( level );
//			output.writeByte( triesLeft );
//			output.writeLong( points );
//			output.writeShort( average );
			output.writeByte( maxLevel );
		} catch ( Exception e ) {
		}
	}


	/**
	 * Lê dados de um stream de dados
	 * @param input O stream de entrada
	 * @throws Exception
	 */
	public void read( DataInputStream input ) throws Exception {
		try {
//			level = input.readByte();
//			triesLeft = input.readByte();
//			points = input.readLong();
//			average = input.readShort();
			maxLevel = input.readByte();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
//			level = DEFAULT_START_LEVEL;
//			triesLeft = DEFAULT_TRIES_LEFT;
//			points = DEFAULT_POINTS;
//			average = DEFAULT_AVERAGE;
			maxLevel = 0;
		}
	}


	/**
	 * @return the triesLeft
	 */
	public byte getTriesLeft() {
		byte toReturn = ( triesLeft != Byte.MIN_VALUE ) ? ( triesLeft ) : ( triesLeft = DEFAULT_TRIES_LEFT );
		return toReturn;
	}


	/**
	 * @param triesLeft the triesLeft to set
	 */
	public void setTriesLeft( byte TriesLeft ) {
		this.triesLeft = TriesLeft;
	}


	/**
	 * @return the level
	 */
	public byte getLevel() {
		return ( level != Byte.MIN_VALUE ) ? ( level ) : ( level = DEFAULT_START_LEVEL );
	}


	/**
	 * @param level the level to set
	 */
	public void setLevel( byte level ) {
		this.level = level;
	}


	/**
	 * @return the points
	 */
	public long getPoints() {
		return ( points != Byte.MIN_VALUE ) ? ( points ) : ( points = DEFAULT_POINTS );
	}


	/**
	 * @param points the points to set
	 */
	public void setPoints( long points ) {
		this.points = points;
	}


	/**
	 * @return the currentPlayer
	 */
	public byte getCurrentPlayer() {
		return ( currentPlayer != Byte.MIN_VALUE ) ? ( currentPlayer ) : ( currentPlayer = DEFAULT_PLAYER );
	}


	/**
	 * @param currentPlayer the currentPlayer to set
	 */
	public void setCurrentPlayer( byte currentPlayer ) {
		this.currentPlayer = currentPlayer;
	}

	/**
	 * Preferi assim, para que eu possa ter um controle mais fino sobre a média
	 * @param average define uma nova média
	 */
	public void setAverage( short average ) {
		this.average = average;
	}

	/**
	 *
	 * @return A média de pontos por partida
	 */
	public short getAverage() {
		return ( average != Short.MIN_VALUE ) ? ( average ) : ( average = DEFAULT_AVERAGE );
	}
	//</editor-fold>


	/**
	 *
	 * @return
	 */
	public static byte getMaxLevel() {
		return ( maxLevel != Byte.MIN_VALUE ) ? ( maxLevel ) : ( maxLevel = PokerPlayer.CLOTHING_BARBARA_FULL );
	}

	public static void setMaxLevel( byte candidate ) {

		if ( candidate > maxLevel )
			maxLevel = candidate;
		
	}
}
