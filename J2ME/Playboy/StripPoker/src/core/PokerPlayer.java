package core;
//<editor-fold desc="Import">
import core.UI.Constants;
import core.UI.GraphicalPokerCard;
import screens.GameMIDlet;
import util.HeapSort;
//</editor-fold>


/**
 * Jogador de Poker presente na mesa ( tanto Bob quanto Barbara )
 * @author Daniel Monteiro
 */
public class PokerPlayer implements Constants {
//<editor-fold desc="constantes da classe">
	//<editor-fold desc="constantes de roupagem">


	/**
	 * Barbara com todas as roupas
	 */
//#if	REDUCED == "true"
//# 	public static final byte CLOTHING_BARBARA_FULL = 6;
//#else
public static final byte CLOTHING_BARBARA_FULL = 16;
//#endif
	/**
	 * Bob com todas as roupas
	 */
	public static final byte CLOTHING_BOB_FULL = 5;
	//</editor-fold>
	//<editor-fold desc="constantes de ação do jogador">
	/**
	 * Depositando o blind ( o small ou o big )
	 */
	public static final byte ACTION_BLINDING = 0;
	/**
	 * Distribuindo as cartas
	 */
	public static final byte ACTION_DEALING = 1;
	/**
	 * Apostando
	 */
	public static final byte ACTION_BETTING = 2;
	/**
	 * Observando as cartas
	 */
	public static final byte ACTION_CHECKING = 3;
	/**
	 * Pagando pra ver
	 */
	public static final byte ACTION_CALLING = 4;
	/**
	 * Desistindo
	 */
	public static final byte ACTION_FOLDING = 5;
	/**
	 * Jogador ganhou
	 */
	public static final byte ACTION_WINNING = 6;
	/**
	 * Percebendo as consequencias ( boas ou ruins )
	 */
	public static final byte ACTION_TAKING_OUTCOME = 7;
	/**
	 * O jogador resolveu fazer um ALL IN
	 */
	public static final byte ACTION_ALLIN = 8;
	//</editor-fold>
//</editor-fold>
//<editor-fold desc="campos da classe">
	/**
	 * Mensagem do jogador na mesa de jogo
	 */
	private String say;
	/**
	 * Dinheiro ainda não apostado
	 */
	private long money;
	/**
	 * Aposta atual
	 */
	private long currentBet;
	/**
	 * Ultima ação
	 */
	private byte lastAction;
	/**
	 * define quem o jogador representa ( Bob ou Barbara )
	 */
	private byte personality;
	/**
	 * define o nível atual do jogador ( usado primariamente na Barbara )
	 */
	private byte level;
	/**
	 * A representação gráfica das cartas
	 */
	private GraphicalPokerCard[] graphicalCards;
	/**
	 * A mão em sí. É mais usada mesmo para calcular a vitória ou derrota do jogo
	 */
	private PokerHand hand;
//</editor-fold>
//<editor-fold desc="metodos">


	/**
	 * Inicializa o jogador com dados padrão para uma nova partida.
	 * O estado inicial do jogador é blinding.
	 * @param personality indica quem este jogador deve representar
	 */
	public PokerPlayer( byte personality, byte level ) {
		money = INITIAL_CASH_PER_PLAYER;
		currentBet = 0;
		lastAction = ACTION_BLINDING;
		this.personality = personality;
		this.level = level;
		graphicalCards = new GraphicalPokerCard[ CARD_ON_PLAYERS_HAND ];
		hand = new PokerHand();
	}


	/**
	 * Define qual vai ser a fala do jogador
	 * @param say
	 */
	public void setSay( String say ) {
		this.say = say;
	}


	/**
	 *
	 * @return A fala do jogador. Se ele não tem o que falar, retorna "".
	 */
	public String getSay() {
		String toReturn = say;
		say = "";
		return toReturn;
	}


	/**
	 * Realiza aposta.
	 * @param cash representa o montante da aposta
	 * @return O valor atual da aposta
	 */
	public final long bet( long cash, boolean calling ) {

		if ( cash > money ) {
			//#if DEBUG == "true"
			System.out.println( "tentou apostar mais dinheiro que o disponivel" );
			//#endif
		}

		if ( calling ) {
			lastAction = ACTION_CALLING;
		} else {
			lastAction = ACTION_BETTING;
		}

		setDesiredBet( cash );
		money -= cash;

		return currentBet;
	}


	/**
	 * Obtem capital do jogador
	 * @return O dinheiro ainda não apostado do jogador.
	 */
	public final long getMoney() {
		return money;
	}


	/**
	 * Sinaliza que o jogador desistiu e aceitou perder a rodada
	 */
	public final void fold() {
		lastAction = ACTION_FOLDING;
	}


	/**
	 * Decrementa o fator do blind ( primeira jogada )
	 * @param cash representa o valor de decremento.
	 */
	public final void blind( long cash ) {
		currentBet = cash;
		money -= cash;
	}


	/**
	 * Indica ao jogador que ele ganhou do monte ( pot )
	 * @param cash montante ganho
	 */
	public final void win( long cash ) {
		money += cash;
		lastAction = ACTION_WINNING;
	}


	/**
	 * Oferece carta ao jogador. Caso ele ja tenha o suficiente, o jogador rejeita
	 * @param pokerCard A carta a ser dado para o jogador. Esta é imediatamente virada.
	 */
	public void giveCard( GraphicalPokerCard pokerCard ) {

		if ( personality == PLAYER_BOB )
			pokerCard.flip();
		
		pokerCard.setPlayer( personality );

		hand.addCard( pokerCard.getCardData() );

		if ( graphicalCards[ 0] == null ) {
			graphicalCards[ 0] = pokerCard;
		} else if ( graphicalCards[ 1] == null ) {
			graphicalCards[ 1] = pokerCard;
		} else {
			//#if DEBUG == "true"
			System.out.println(
					"Erro de integridade. Um jogador esta recebendo uma carta a mais do que devia. (id=" + this.personality + ")" );
			//#endif
		}
	}


	/**
	 *
	 * @param i é o indice da carta desejada
	 * @return sua referencia
	 */
	public final GraphicalPokerCard getCard( int i ) {
		if ( i < 0 || i > CARD_ON_PLAYERS_HAND ) {
			//#if DEBUG == "true"
			System.out.println( "indice de carta inválido: " + i );
			//#endif
		}

		return graphicalCards[ i ];
	}


	/**
	 *
	 * @return o desejo de aposta atual do jogador
	 */
	public final long getBet() {
		return currentBet;
	}


	/**
	 *
	 * @return a mão do jogador
	 */
	public final PokerHand getHand() {
		return hand;
	}


	/**
	 * Faz com que o jogador aposte tudo
	 */
	public void allIn() {
		currentBet = getMoney();
		lastAction = ACTION_ALLIN;
		setSay( GameMIDlet.getText( TEXT_DO_ALL_IN ) );
	}


	/**
	 * Indica o desejo de aposta
	 * @param l é O valor da aposta
	 */
	public void setDesiredBet( long l ) {

		if ( l == money ) {
			allIn();
		} else {
			currentBet = l;
			lastAction = ACTION_BETTING;
		}
	}


	/**
	 *
	 * @param points indica o valor inicial da aposta
	 */
	public void setInitialCash( long points ) {
		money = points;
	}


	public byte getLastAction() {
		return lastAction;
	}


	public void sortCards() {
		hand.sort();
		HeapSort.sort( graphicalCards );
	}
//</editor-fold>
}
