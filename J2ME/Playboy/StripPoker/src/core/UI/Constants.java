/**
 * Constants.java
 * ©2011 Nano Games.
 * 
 */
package core.UI;


/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface Constants {
//<editor-fold desc="constantes de aparencia gerais">


	/**
	 * cor: branca
	 */
	public static final int WHITE = 0xFFFFFF;
	/**
	 * cor: cinza
	 */
	public static final int GREY = 0x888888;
	/**
	 * cor: vermelha
	 */
	public static final int RED = 0xFF0000;
	/**
	 * cor: preto
	 */
	public static final int BLACK = 0;
	/*
	 * cor do fundo do monitor do jogador
	 */
	public static final int COLOR_PLAYER_BKG = RED;
	/**
	 * comprimento da borda interior da carta
	 */
	public static final byte CARD_INNER_BORDER = 0;
	/**
	 * raio horizontal do round rect
	 */
	public static final byte CARD_BORDER_WIDTH = 4;
	/**
	 * raio vertical do round rect
	 */
	public static final byte CARD_BORDER_HEIGHT = 4;
	/**
	 * indice da informação da carta: valor
	 */
	public static final byte CARD_INFORMATION_RANK = 0;
	/**
	 * indice da informação da carta: naipe
	 */
	public static final byte CARD_INFORMATION_SUIT = 1;
//</editor-fold>
//<editor-fold desc="constantes de mecânica de jogo">
	//<editor-fold desc="Constantes das regras de jogo">
	/**
	 * Numero de cartas que são de fato consideradas
	 */
	public static final byte CARDS_WORTH = 5;
	/**
	 * Dinheiro inicial do jogador na partida
	 */
	public static final int INITIAL_CASH_PER_PLAYER = 1000;
	/**
	 * Numero de jogadores ( Bob + Barbara = 2 )
	 */
	public static final byte NUM_PLAYERS = 2;
	/**
	 * A carta não pertence a nenhum jogador
	 */
	public static final byte PLAYER_NO_PLAYER = -1;
	/**
	 * Barbara sempre é a jogadora 0
	 */
	public static final byte PLAYER_BARBARA = 0;
	/**
	 * Bob sempre é o jogador 1
	 */
	public static final byte PLAYER_BOB = 1;
	/**
	 * Por fins de praticidade, a mesa será considerada um jogador.
	 */
	public static final byte PLAYER_TABLE = NUM_PLAYERS;
	/**
	 * Blind inicial da partida
	 */
	public static final long INITIAL_BLIND = 10;
	/**
	 * Cartas ( rank ) por naipe ( suit )
	 */
	public static final byte RANKS_PER_SUIT = 13;
	/**
	 * Quantos naipes ( suits ) existem num baralho ( deck )
	 */
	public static final byte SUITS_PER_DECK = 4;
	/**
	 * Cartas por deck de  baralho
	 */
	public static final byte CARDS_PER_DECK = SUITS_PER_DECK * RANKS_PER_SUIT;
	/**
	 * Numero de cartas publicas na mesa ( Texas Hold'em = 5 )
	 */
	public static final byte PUBLIC_CARDS_ON_TABLE = 5;
	/**
	 * Numero de cartas na mão ( Texas Hold'em = 2 )
	 */
	public static final byte CARD_ON_PLAYERS_HAND = 2;
	/**
	 * Tempo mínimo entre cada jogada
	 */
	public static final int TIME_TURN = 1000;
	/**
	 * Tempo de piscada do joystick
	 */
	public static final int TIME_BLINK = 250;
	//</editor-fold>
	//<editor-fold desc="Estados de jogo">
	/**
	 * Estado de jogo: inicialização
	 */
	public static final byte GAMESTATE_PREGAME = 0;
	/**
	 * Estado de jogo: introdução
	 */
	public static final byte GAMESTATE_INTRO = 1;
	/**
	 * Estado de jogo: jogo começando
	 */
	public static final byte GAMESTATE_START = 2;
	/**
	 * Estado de jogo: jogo em andamento
	 */
	public static final byte GAMESTATE_PLAYING = 3;
	/**
	 * Estado de jogo: resultado de uma rodada
	 */
	public static final byte GAMESTATE_OUTCOME = 4;
	/**
	 * Estado de jogo: resultado de uma rodada
	 */
	public static final byte GAMESTATE_OUTCOME_2 = 5;
	/**
	 * Estado de jogo: Jogo todo vencido
	 */
	public static final byte GAMESTATE_WIN = 6;
	/**
	 * Estado de jogo: Fim de jogo
	 */
	public static final byte GAMESTATE_GAMEOVER = 7;
	//</editor-fold>
	public static final int TIME_OUTCOME = 6000;
//</editor-fold>
//<editor-fold desc="constantes necessárias para o funcionamento dos componentes">
	public static final int SCORE_SHOWN_MAX = 999999;
	public static final String APP_SHORT_NAME = "STPO";
	public static final byte MAX_NUMBER_CHARS = 7;
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_PLAYER_IMAGES = PATH_IMAGES + "player";
	public static final String PATH_ONLINE = PATH_IMAGES + "online/";
	public static final String PATH_CARD_BORDER = PATH_IMAGES + "card_border/";
	public static final String PATH_CARD_STATES = PATH_IMAGES + "card_states/";
	public static final String PATH_CURSOR = PATH_IMAGES + "cursor_states/";
	public static final String PATH_GIRLS = PATH_IMAGES + "ninfetas/";
	public static final String PATH_MENU = PATH_IMAGES + "menu/";

	public static final byte RANKING_INDEX_ACC_SCORE = 0;
	public static final byte RANKING_INDEX_AVG = 1;
	// <editor-fold defaultstate="collapsed" desc="DATABASE INFO">
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	/**Índice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	public static final byte DATABASE_SLOT_GAME_DATA = 2;
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 2;
	/** Id da categoria de notícias do Topa ou Não Topa, no Nano Online. */
	public static final byte NEWS_CATEGORY_ID = 6;
	// </editor-fold>
	//<editor-fold desc="CONSTANTES DO HARDWARE">
	// Duração da vibração ao atingir a trave, em milisegundos.
	public static final short VIBRATION_TIME_DEFAULT = 300;
	//#if SCREEN_SIZE == "SMALL"
//#  		public static final int LOW_MEMORY_LIMIT = 1000000;
	//#elif SCREEN_SIZE == "MEDIUM"
//# 			public static final int LOW_MEMORY_LIMIT = 1200000;
	//#elif SCREEN_SIZE == "BIG"
	public static final int LOW_MEMORY_LIMIT = 1600000;
	//#else
//#  		public static final int LOW_MEMORY_LIMIT = 2000000;
	//#endif
	//</editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES">
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES E DADOS DAS FONTES">
	/** Offset da fonte padrão. */
	public static final byte DEFAULT_FONT_OFFSET = 1;
	/**Índice da fonte padrão do jogo. */
	public static final byte FONT_INDEX_DEFAULT = 0;
	/**Índice da fonte utilizada para textos. */
	public static final byte FONT_TEXT = 3;
	public static final byte FONT_TITLE = 1;
	public static final byte FONT_SUIT_RED = 2;
	public static final byte FONT_SUIT_BLACK = 1;
	public static final byte FONT_NUMBERS = 0;
	public static final byte FONT_SUITS = 4;
	public static final byte FONT_MENU = 5;
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL = 6;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES E DADOS DOS SONS">

	//índices dos sons do jogo
	public static final byte SOUND_DEFAULT = 0;
	public static final byte SOUND_MUSIC_PLAYER_WINS = 1;
	public static final byte SOUND_MUSIC_PLAYER_LOOSES = 2;
	public static final byte SOUND_MUSIC_MENU = 3;
	public static final byte SOUND_MUSIC_GAMEOVER = 4;
	public static final byte SOUND_NEON = 5;
	public static final byte SOUND_CONFIRM_BET = 6;
	public static final byte SOUND_FOLD = 7;
	public static final byte SOUND_SHOWDOWN = 8;

	/** Total de tipos de sons do jogo. */
	public static final byte SOUND_TOTAL = SOUND_SHOWDOWN + 1;
	
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS TEXTOS">
	public static final short TEXT_OK = 0;
	public static final short TEXT_BACK = TEXT_OK + 1;
	public static final short TEXT_NEW_GAME = TEXT_BACK + 1;
	public static final short TEXT_OPTIONS = TEXT_NEW_GAME + 1;
	public static final short TEXT_NANO_ONLINE = TEXT_OPTIONS + 1;
	public static final short TEXT_HELP = TEXT_NANO_ONLINE + 1;
	public static final short TEXT_CREDITS = TEXT_HELP + 1;
	public static final short TEXT_EXIT = TEXT_CREDITS + 1;
	public static final short TEXT_PAUSE = TEXT_EXIT + 1;
	public static final short TEXT_CREDITS_TEXT = TEXT_PAUSE + 1;
	public static final short TEXT_CONTROLS = TEXT_CREDITS_TEXT + 1;
	public static final short TEXT_RULES = TEXT_CONTROLS + 1;
	public static final short TEXT_HELP_PROFILE = TEXT_RULES + 1;
	public static final short TEXT_HELP_CONTROLS = TEXT_HELP_PROFILE + 1;
	public static final short TEXT_HELP_OBJECTIVES = TEXT_HELP_CONTROLS + 1;
	public static final short TEXT_HELP_PROFILE_TEXT = TEXT_HELP_OBJECTIVES + 1;
	public static final short TEXT_HELP_IMAGEGALLERY_TEXT = TEXT_HELP_PROFILE_TEXT + 1;
	public static final short TEXT_TURN_OFF = TEXT_HELP_IMAGEGALLERY_TEXT + 1;
	public static final short TEXT_TURN_ON = TEXT_TURN_OFF + 1;
	public static final short TEXT_ON = TEXT_TURN_ON + 1;
	public static final short TEXT_OFF = TEXT_ON + 1;
	public static final short TEXT_SOUND = TEXT_OFF + 1;
	public static final short TEXT_VIBRATION = TEXT_SOUND + 1;
	public static final short TEXT_CANCEL = TEXT_VIBRATION + 1;
	public static final short TEXT_CONTINUE = TEXT_CANCEL + 1;
	public static final short TEXT_SPLASH_NANO = TEXT_CONTINUE + 1;
	public static final short TEXT_SPLASH_BRAND = TEXT_SPLASH_NANO + 1;
	public static final short TEXT_LOADING = TEXT_SPLASH_BRAND + 1;
	public static final short TEXT_DO_YOU_WANT_SOUND = TEXT_LOADING + 1;
	public static final short TEXT_YES = TEXT_DO_YOU_WANT_SOUND + 1;
	public static final short TEXT_NO = TEXT_YES + 1;
	public static final short TEXT_BACK_MENU = TEXT_NO + 1;
	public static final short TEXT_CONFIRM_BACK_MENU = TEXT_BACK_MENU + 1;
	public static final short TEXT_START_GAME = TEXT_CONFIRM_BACK_MENU + 1;
	public static final short TEXT_EXIT_GAME = TEXT_START_GAME + 1;
	public static final short TEXT_CONFIRM_EXIT_1 = TEXT_EXIT_GAME + 1;
	public static final short TEXT_PRESS_ANY_KEY = TEXT_CONFIRM_EXIT_1 + 1;
	public static final short TEXT_GAME_OVER = TEXT_PRESS_ANY_KEY + 1;
	public static final short TEXT_RECOMMEND_TITLE = TEXT_GAME_OVER + 1;
	public static final short TEXT_PROFILE_SELECTION = TEXT_RECOMMEND_TITLE + 1;
	public static final short TEXT_CHOOSE_ANOTHER = TEXT_PROFILE_SELECTION + 1;
	public static final short TEXT_CONFIRM = TEXT_CHOOSE_ANOTHER + 1;
	public static final short TEXT_NO_PROFILE = TEXT_CONFIRM + 1;
	public static final short TEXT_TIME_FORMAT = TEXT_NO_PROFILE + 1;
	public static final short TEXT_HIGH_SCORES = TEXT_TIME_FORMAT + 1;
	public static final short TEXT_VOLUME = TEXT_HIGH_SCORES + 1;
	public static final short TEXT_VERSION = TEXT_VOLUME + 1;
	public static final short TEXT_PHOTO_GALLERY = TEXT_VERSION + 1;
	public static final short TEXT_LEVEL_CLEARED = TEXT_PHOTO_GALLERY + 1;
	public static final short TEXT_LEVEL_SHOW = TEXT_LEVEL_CLEARED + 1;
	public static final short TEXT_NEW_RECORD = TEXT_LEVEL_SHOW + 1;
	public static final short TEXT_RESTART = TEXT_NEW_RECORD + 1;
	public static final short TEXT_RECOMMEND_SENT = TEXT_RESTART + 1;
	public static final short TEXT_RECOMMEND_NOTSENT = TEXT_RECOMMEND_SENT + 1;
	public static final short TEXT_RECOMMEND_TEXT = TEXT_RECOMMEND_NOTSENT + 1;
	public static final short TEXT_RECOMMENDED = TEXT_RECOMMEND_TEXT + 1;
	public static final short TEXT_RECOMMEND_SMS = TEXT_RECOMMENDED + 1;
	public static final short TEXT_RANKING_ACCUMULATED_POINTS = TEXT_RECOMMEND_SMS + 1;
	public static final short TEXT_RANKING_MAX_POINTS = TEXT_RANKING_ACCUMULATED_POINTS + 1;
	public static final short TEXT_RANKING_AVERAGE = TEXT_RANKING_MAX_POINTS + 1;
	public static final short TEXT_CURRENT_PROFILE = TEXT_RANKING_AVERAGE + 1;
	public static final short TEXT_SINGLEPLAYER = TEXT_CURRENT_PROFILE + 1;
	public static final short TEXT_MULTIPLAYER = TEXT_SINGLEPLAYER + 1;
	public static final short TEXT_GAME_MODE = TEXT_MULTIPLAYER + 1;
	public static final short TEXT_BONUS = TEXT_GAME_MODE + 1;
	public static final short TEXT_PENALTY = TEXT_BONUS + 1;
	public static final short TEXT_PLAYER = TEXT_PENALTY + 1;
	public static final short TEXT_GAMEOVER_TITLE = TEXT_PLAYER + 1;
	public static final short TEXT_GAMEOVER_MSG = TEXT_GAMEOVER_TITLE + 1;
	public static final short TEXT_TURN1 = TEXT_GAMEOVER_MSG + 1;
	public static final short TEXT_TURN2 = TEXT_TURN1 + 1;
	public static final short TEXT_TURN3 = TEXT_TURN2 + 1;
	public static final short TEXT_TURN4 = TEXT_TURN3 + 1;
	public static final short TEXT_TURN5 = TEXT_TURN4 + 1;
	public static final short TEXT_EVEN = TEXT_TURN5 + 1;
	public static final short TEXT_TURN6 = TEXT_EVEN + 1;
	public static final short TEXT_TURN_EXCEPTION = TEXT_TURN6 + 1;
	public static final short TEXT_AGAINST = TEXT_TURN_EXCEPTION + 1;
	public static final short TEXT_BARBARA_FOLD = TEXT_AGAINST + 1;
	public static final short TEXT_BARBARA_CALL = TEXT_BARBARA_FOLD + 1;
	public static final short TEXT_BARBARA_RAISE_1 = TEXT_BARBARA_CALL + 1;
	public static final short TEXT_BARBARA_RAISE_2 = TEXT_BARBARA_RAISE_1 + 1;
	public static final short TEXT_BARBARA_ALL_IN = TEXT_BARBARA_RAISE_2 + 1;
	public static final short TEXT_BARBARA_PREFLOP_GOOD_1 = TEXT_BARBARA_ALL_IN + 1;
	public static final short TEXT_BARBARA_PREFLOP_GOOD_2 = TEXT_BARBARA_PREFLOP_GOOD_1 + 1;
	public static final short TEXT_BARBARA_PREFLOP_GOOD_3 = TEXT_BARBARA_PREFLOP_GOOD_2 + 1;
	public static final short TEXT_BARBARA_PREFLOP_BAD = TEXT_BARBARA_PREFLOP_GOOD_3 + 1;
	public static final short TEXT_BARBARA_FLOP_GOOD_1 = TEXT_BARBARA_PREFLOP_BAD + 1;
	public static final short TEXT_BARBARA_FLOP_GOOD_2 = TEXT_BARBARA_FLOP_GOOD_1 + 1;
	public static final short TEXT_BARBARA_FLOP_GOOD_3 = TEXT_BARBARA_FLOP_GOOD_2 + 1;
	public static final short TEXT_BARBARA_FLOP_BAD = TEXT_BARBARA_FLOP_GOOD_3 + 1;
	public static final short TEXT_BARBARA_TURN_GOOD_1 = TEXT_BARBARA_FLOP_BAD + 1;
	public static final short TEXT_BARBARA_TURN_GOOD_2 = TEXT_BARBARA_TURN_GOOD_1 + 1;
	public static final short TEXT_BARBARA_TURN_GOOD_3 = TEXT_BARBARA_TURN_GOOD_2 + 1;
	public static final short TEXT_BARBARA_TURN_BAD = TEXT_BARBARA_TURN_GOOD_3 + 1;
	public static final short TEXT_BARBARA_RIVER_GOOD_1 = TEXT_BARBARA_TURN_BAD + 1;
	public static final short TEXT_BARBARA_RIVER_GOOD_2 = TEXT_BARBARA_RIVER_GOOD_1 + 1;
	public static final short TEXT_BARBARA_RIVER_GOOD_3 = TEXT_BARBARA_RIVER_GOOD_2 + 1;
	public static final short TEXT_BARBARA_RIVER_BAD = TEXT_BARBARA_RIVER_GOOD_3 + 1;
	public static final short TEXT_BARBARA_SPEECHLESS = TEXT_BARBARA_RIVER_BAD + 1;
	public static final short TEXT_WIN = TEXT_BARBARA_SPEECHLESS + 1;
	public static final short TEXT_LOSE = TEXT_WIN + 1;
	public static final short TEXT_GALLERY = TEXT_LOSE + 1;

	public static final short TEXT_PHOTO_1 = TEXT_GALLERY + 1;
	public static final short TEXT_PHOTO_2 = TEXT_PHOTO_1 + 1;
	public static final short TEXT_PHOTO_3 = TEXT_PHOTO_2 + 1;
	public static final short TEXT_PHOTO_4 = TEXT_PHOTO_3 + 1;
	public static final short TEXT_PHOTO_5 = TEXT_PHOTO_4 + 1;
	public static final short TEXT_PHOTO_6 = TEXT_PHOTO_5 + 1;
	public static final short TEXT_PHOTO_7 = TEXT_PHOTO_6 + 1;
	public static final short TEXT_PHOTO_8 = TEXT_PHOTO_7 + 1;
	public static final short TEXT_PHOTO_9 = TEXT_PHOTO_8 + 1;
	public static final short TEXT_PHOTO_10 = TEXT_PHOTO_9 + 1;
	public static final short TEXT_PHOTO_11 = TEXT_PHOTO_10 + 1;
	public static final short TEXT_PHOTO_12 = TEXT_PHOTO_11 + 1;
	public static final short TEXT_PHOTO_13 = TEXT_PHOTO_12 + 1;
	public static final short TEXT_PHOTO_14 = TEXT_PHOTO_13 + 1;
	public static final short TEXT_PHOTO_15 = TEXT_PHOTO_14 + 1;
	public static final short TEXT_PHOTO_16 = TEXT_PHOTO_15 + 1;
	public static final short TEXT_PHOTO_LOCKED = TEXT_PHOTO_16 + 1;
	public static final short TEXT_DO_FOLD = TEXT_PHOTO_LOCKED + 1;
	public static final short TEXT_DO_ALL_IN = TEXT_DO_FOLD + 1;
	public static final short TEXT_NAME_BARBARA = TEXT_DO_ALL_IN + 1;
	public static final short TEXT_NAME_BOB = TEXT_NAME_BARBARA + 1;
	public static final short TEXT_SAY_GAME_OVER = TEXT_NAME_BOB + 1;
	public static final short TEXT_SAY_GAME_WIN = TEXT_SAY_GAME_OVER + 1;



	/** número total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_SAY_GAME_WIN + 1;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS TELAS DO JOGO">
	public static final byte SCREEN_CHOOSE_SOUND = 0;
	public static final byte SCREEN_SPLASH_NANO = SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND = SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME = SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU = SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME = SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_OPTIONS = SCREEN_NEW_GAME + 1;
	public static final byte SCREEN_HELP = SCREEN_OPTIONS + 1;
	public static final byte SCREEN_CREDITS = SCREEN_HELP + 1;
	public static final byte SCREEN_PAUSE = SCREEN_CREDITS + 1;
	public static final byte SCREEN_LOADING_1 = SCREEN_PAUSE + 1;
	public static final byte SCREEN_GAME_OVER = SCREEN_LOADING_1 + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE = SCREEN_GAME_OVER + 1;
	public static final byte SCREEN_LOADING_PHOTO_GALLERY = SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_PHOTO_GALLERY = SCREEN_LOADING_PHOTO_GALLERY + 1;
	public static final byte SCREEN_GAME_OVER_MENU = SCREEN_PHOTO_GALLERY + 1;
	public static final byte SCREEN_LOG = SCREEN_GAME_OVER_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN = SCREEN_LOG + 1;
	public static final byte SCREEN_RECOMMEND_SENT = SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_RECOMMEND_NOTSENT = SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_RECOMMEND = SCREEN_RECOMMEND_NOTSENT + 1;
	public static final byte SCREEN_CHOOSE_PROFILE = SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_LOADING_GAME = SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_NEXT_LEVEL = SCREEN_LOADING_GAME + 1;
	public static final byte SCREEN_HELP_PROFILE = SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN = SCREEN_HELP_PROFILE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES = SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES = SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NANO_RANKING_MENU = SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_NEW_RECORD_MENU = SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_MAIN_MENU = SCREEN_NEW_RECORD_MENU + 1;
	public static final byte SCREEN_LOADING_MAIN_MENU_NO_UNLBOCK = SCREEN_LOADING_MAIN_MENU + 1;
	public static final byte SCREEN_WIN = SCREEN_LOADING_MAIN_MENU_NO_UNLBOCK + 1;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU PRINCIPAL">
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME = 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS = 1;
	public static final byte ENTRY_MAIN_MENU_NEWS = 2;
	public static final byte ENTRY_MAIN_MENU_NANO_ONLINE = 3;
	public static final byte ENTRY_MAIN_MENU_RECOMMEND = 4;
	public static final byte ENTRY_MAIN_MENU_HELP = 5;
	public static final byte ENTRY_MAIN_MENU_CREDITS = 6;
	public static final byte ENTRY_MAIN_MENU_EXIT = 7;
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG = ENTRY_MAIN_MENU_EXIT + 1;
	//#endif
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE AJUDA">
	public static final byte ENTRY_HELP_MENU_OBJETIVES = 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS = 1;
	public static final byte ENTRY_HELP_MENU_BACK = 2;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE PAUSA">
	public static final byte ENTRY_PAUSE_MENU_CONTINUE = 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND = 1;
	public static final byte ENTRY_PAUSE_MENU_VOLUME = 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION = 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU = 4;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME = 5;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU = 3;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME = 4;
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND = 0;
	public static final byte ENTRY_OPTIONS_MENU_VOLUME = 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION = 2;
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK = 2;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK = 3;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE FIM DE JOGO">
	public static final byte ENTRY_GAME_OVER_HIGH_SCORES = 0;
	public static final byte ENTRY_GAME_OVER_NEW_GAME = 1;
	public static final byte ENTRY_GAME_OVER_BACK_MENU = 2;
	public static final byte ENTRY_GAME_OVER_EXIT_GAME = 3;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE SELEÇÃO DE PERFIL">
	public static final byte ENTRY_CHOOSE_PROFILE_USE_CURRENT = 0;
	public static final byte ENTRY_CHOOSE_PROFILE_CHANGE = 1;
	public static final byte ENTRY_CHOOSE_PROFILE_HELP = 2;
	public static final byte ENTRY_CHOOSE_PROFILE_BACK = 3;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE NOTÍCIAS">
	public static final byte ENTRY_NEWS_GENERAL = 0;
	public static final byte ENTRY_NEWS_SCORERS = 1;
	public static final byte ENTRY_NEWS_TABLE = 2;
	public static final byte ENTRY_NEWS_BACK = 3;
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU INICIAL DE SOM">
	public static final byte ENTRY_SOUND_ON_OFF = 0;
	public static final byte ENTRY_SOUND_VOLUME = 1;
	public static final byte ENTRY_SOUND_OK = 2;
	// </editor-fold>
	// </editor-fold>
	//<editor-fold desc="CONSTANTES DO JOGO">
	/** Duração mínima da exibição da carta. */
	public static final short TIME_CARD_FLIP = 300;
	//</editor-fold>
	//#if SCREEN_SIZE == "SMALL"
//# //<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA PEQUENA">
//# 	/**
//#   	 * largura da carta
//#   	 */
//#   	public static final byte CARD_WIDTH = 15;
//#   	/**
//#   	 * altura da carta
//#   	 */
//#   	public static final byte CARD_HEIGHT = 22;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PHOTO_BORDER = 2;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PHOTO_WIDTH = 20;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_PAINEL_WIDTH = 41;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_PAINEL_HEIGHT = 35;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte SAFE_MARGIN = 11;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte CHIPS_X_LANDSCAPE = - 50;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte CHIPS_X_PORTRAIT = - 35;
//# 	public static final byte CARD_STRIDE_Y = 0;
//# 	public static final byte CARD_STRIDE_X = 1;
//#  	/**
//#  	 *
//#  	 */
//# // 	public static final byte PLAYER_CARDS_X_PORTRAIT = - ( CARD_ON_PLAYERS_HAND * CARD_WIDTH ) - ( CARD_WIDTH >> 1);
//#  	public static final byte PLAYER_CARDS_X_PORTRAIT = - PLAYER_PAINEL_WIDTH + PHOTO_WIDTH - CARD_WIDTH;
//#  	/**
//#  	 *
//#  	 */
//# // 	public static final byte PLAYER_CARDS_Y_PORTRAIT = - ( ( PLAYER_PAINEL_HEIGHT >> 1) + CARD_ON_PLAYERS_HAND * ( CARD_HEIGHT >> 1) ) ;
//#  	public static final byte PLAYER_CARDS_Y_PORTRAIT = - SAFE_MARGIN - PHOTO_BORDER - CARD_HEIGHT;
//#  	/*
//#  	 *
//#  	 */
//#  	public static final byte PLAYER_CARDS_X_LANDSCAPE = PLAYER_PAINEL_WIDTH;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PLAYER_CARDS_Y_LANDSCAPE = 0;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PUBLIC_CARDS_X_PORTRAIT = 0;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PUBLIC_CARDS_Y_PORTRAIT = -16;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte CHIPS_Y_PORTRAIT = PUBLIC_CARDS_Y_PORTRAIT + CARD_HEIGHT;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte CHIPS_Y_LANDSCAPE = PUBLIC_CARDS_Y_PORTRAIT + CARD_HEIGHT;
//#  	/*
//#  	 *
//#  	 */
//#  	public static final byte PUBLIC_CARDS_X_LANDSCAPE = 0;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PUBLIC_CARDS_Y_LANDSCAPE = 0;
//#  	/**
//#  	 *
//#  	 */
//#   	public static final byte SCORE_LABEL_Y_ADJUSTMENT = 0;
//#  	/**
//#  	 *
//#  	 */
//#   	public static final byte BET_LABEL_Y_ADJUSTMENT = 0;
//# 	public static final short MENU_BOTTOM_BAR_HEIGHT = 30;
//#
//#  	public static final short SEXY_HOT_MENU_BAR_OFFSET_Y = 5;
//# 	public static final byte OPTIONS_ITEMS_SPACING = 5;
//#
//#   	public static final byte RECOMMEND_LABEL_LEFT_X = 3;
//#   	public static final byte RECOMMEND_LABEL_LEFT_Y = 3;
//#   	public static final byte RECOMMEND_LABEL_LEFT_WIDTH = 33;
//#   	public static final byte RECOMMEND_LABEL_LEFT_HEIGHT = 7;
//#
//#   	public static final byte RECOMMEND_LABEL_RIGHT_X = 22
//# 			;
//#   	public static final byte RECOMMEND_LABEL_RIGHT_Y = 3;
//#   	public static final byte RECOMMEND_LABEL_RIGHT_WIDTH = 76;
//#   	public static final byte RECOMMEND_LABEL_RIGHT_HEIGHT = 7;
//#
//#  	public static final byte RESULT_CARDS_ROW1_Y = 2 * ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2);
//#  	public static final byte RESULT_CARDS_ROW2_Y = - ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2 ) / 2;
//#
//#   	public static final short KEYPAD_MINIMUM_SIZE = 100;
//#
//# 	public static final int LABELS_GROUP_BORDER = 20;
//#
//# 	public static final int TEXT_TITLE_MARGIN = 8;
//# //</editor-fold>
	//#elif SCREEN_SIZE == "MEDIUM"
//# //<editor-fold desc="DEFINIÇÕES ESPECÍ?FICAS PARA TELA MÉDIA">
//# 	/**
//#  	 * largura da carta
//#  	 */
//#  	public static final byte CARD_WIDTH = 24;
//#  	/**
//#  	 * altura da carta
//#  	 */
//#  	public static final byte CARD_HEIGHT = 34;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PHOTO_BORDER = 2;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PHOTO_WIDTH = 60;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PLAYER_PAINEL_WIDTH = 63;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PLAYER_PAINEL_HEIGHT = 67;
//#  	/**
//#  	 *
//#  	 */
//#  	//public static final byte SAFE_MARGIN = 23;
//#  	public static final byte SAFE_MARGIN = 5;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_X_LANDSCAPE = - 50;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_Y_LANDSCAPE = - 40;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_X_PORTRAIT = - 60;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_Y_PORTRAIT = - 50;
//#
//#
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PLAYER_CARDS_X_PORTRAIT = - ( CARD_ON_PLAYERS_HAND * CARD_WIDTH ) + ( CARD_WIDTH >> 1);
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PLAYER_CARDS_Y_PORTRAIT = - ( ( PLAYER_PAINEL_HEIGHT >> 1) + CARD_ON_PLAYERS_HAND * ( CARD_HEIGHT >> 2) );
//# 	/*
//# 	 *
//# 	 */
//# 	public static final byte PLAYER_CARDS_X_LANDSCAPE = 0;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PLAYER_CARDS_Y_LANDSCAPE = 0;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PUBLIC_CARDS_X_PORTRAIT = 0;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PUBLIC_CARDS_Y_PORTRAIT = -10;
//# 	/*
//# 	 *
//# 	 */
//# 	public static final byte PUBLIC_CARDS_X_LANDSCAPE = 0;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PUBLIC_CARDS_Y_LANDSCAPE = 0;
//# 	public static final byte RESULT_CARDS_ROW1_Y = 2 * ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2);
//# 	public static final byte RESULT_CARDS_ROW2_Y = 0;   // ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2);
//#
//#
//#
//#
//# 	/**
//# 	 *
//# 	 */
//#  	public static final byte SCORE_LABEL_Y_ADJUSTMENT = -1;
//# 	/**
//# 	 *
//# 	 */
//#  	public static final byte BET_LABEL_Y_ADJUSTMENT = 3;
//# 	public static final byte CARD_STRIDE_Y = CARD_HEIGHT >> 1;
//# 	public static final byte CARD_STRIDE_X = 3;
//# 	public static final short SEXY_HOT_MENU_BAR_OFFSET_Y = 5;
//# 	public static final short MENU_BOTTOM_BAR_HEIGHT = 30;
//# 	public static final byte OPTIONS_ITEMS_SPACING = 5;
//#
//#   	public static final byte RECOMMEND_LABEL_LEFT_X = 6;
//#   	public static final byte RECOMMEND_LABEL_LEFT_Y = 7;
//#   	public static final byte RECOMMEND_LABEL_LEFT_WIDTH = 33;
//#   	public static final byte RECOMMEND_LABEL_LEFT_HEIGHT = 11;
//#
//#   	public static final byte RECOMMEND_LABEL_RIGHT_X = 32;
//#   	public static final byte RECOMMEND_LABEL_RIGHT_Y = 7;
//#   	public static final byte RECOMMEND_LABEL_RIGHT_WIDTH = 76;
//#   	public static final byte RECOMMEND_LABEL_RIGHT_HEIGHT = 11;
//#
//#
//#   	public static final short KEYPAD_MINIMUM_SIZE = 100;
//#
//#     public static final int LABELS_GROUP_BORDER = 3;
//# 	public static final int TEXT_TITLE_MARGIN = 8;
//#
//# //</editor-fold>
	//#elif SCREEN_SIZE == "BIG"
	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE">
	/**
	 * largura da carta
	 */
	public static final byte CARD_WIDTH = 25;
	/**
	 * altura da carta
	 */
	public static final byte CARD_HEIGHT = 38;
	/**
	 *
	 */
	public static final byte PHOTO_BORDER = 5;
	/**
	 *
	 */
	public static final byte PHOTO_WIDTH = 60;
	/**
	 *
	 */
	public static final byte PLAYER_PAINEL_WIDTH = 95;
	/**
	 *
	 */
	public static final byte PLAYER_PAINEL_HEIGHT = 95;
	/**
	 *
	 */
	public static final byte SAFE_MARGIN = 5;
	/**
	 *
	 */
	public static final byte CHIPS_X_LANDSCAPE = - 50;
	/**
	 *
	 */
	public static final byte CHIPS_Y_LANDSCAPE = - 33;
	/**
	 *
	 */
	public static final byte CHIPS_X_PORTRAIT = - 70;
	/**
	 *
	 */
	public static final byte CHIPS_Y_PORTRAIT = - 70;
	public static final byte CARD_STRIDE_X = 3;
 	public static final byte RESULT_CARDS_ROW1_Y =   ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2);
 	public static final byte RESULT_CARDS_ROW2_Y = - ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2);

	public static final byte SCORE_LABEL_Y_ADJUSTMENT = -1;
	public static final byte BET_LABEL_Y_ADJUSTMENT = 3;
	/**
  	 *
  	 */
  	public static final byte PLAYER_CARDS_X_PORTRAIT = - ( CARD_WIDTH << 1);
  	/**
  	 *
  	 */
  	public static final byte PLAYER_CARDS_Y_PORTRAIT = - ( - ( PLAYER_PAINEL_HEIGHT >> 3 ) + ( PLAYER_PAINEL_HEIGHT >> 1 ) + ( CARD_ON_PLAYERS_HAND ) * ( CARD_HEIGHT >> 1) );
  	/*
  	 *
  	 */
  	public static final byte PLAYER_CARDS_X_LANDSCAPE = PLAYER_PAINEL_WIDTH;
  	/**
  	 *
  	 */
  	public static final byte PLAYER_CARDS_Y_LANDSCAPE = - CARD_HEIGHT >> 1;
  	/**
  	 *
  	 */
  	public static final byte PUBLIC_CARDS_X_PORTRAIT = 0;
  	/**
  	 *
  	 */
  	public static final byte PUBLIC_CARDS_Y_PORTRAIT = CARD_HEIGHT >> 1;
  	/*
  	 *
  	 */
  	public static final byte PUBLIC_CARDS_X_LANDSCAPE = 0;
  	/**
  	 *
  	 */
  	public static final byte PUBLIC_CARDS_Y_LANDSCAPE = - 2 * CARD_HEIGHT;
 	public static final byte CARD_STRIDE_Y = ( CARD_HEIGHT >> 1 );


	public static final short MENU_BOTTOM_BAR_HEIGHT = 50;

	public static final short SEXY_HOT_MENU_BAR_OFFSET_Y = 13;

	public static final byte OPTIONS_ITEMS_SPACING = 5;

	public static final byte RECOMMEND_LABEL_LEFT_X = 11;
	public static final byte RECOMMEND_LABEL_LEFT_Y = 9;
	public static final byte RECOMMEND_LABEL_LEFT_WIDTH = 33;
	public static final byte RECOMMEND_LABEL_LEFT_HEIGHT = 21;

	public static final byte RECOMMEND_LABEL_RIGHT_X = 49;
	public static final byte RECOMMEND_LABEL_RIGHT_Y = 9;
	public static final byte RECOMMEND_LABEL_RIGHT_WIDTH = 76;
	public static final byte RECOMMEND_LABEL_RIGHT_HEIGHT = 21;

	public static final short KEYPAD_MINIMUM_SIZE = 80;

	public static final int LABELS_GROUP_BORDER = -5;

	public static final int TEXT_TITLE_MARGIN = 8;

	//</editor-fold>
	//#elif SCREEN_SIZE == "GIANT"
//# //<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE">
//# 	public static final byte OPTIONS_ITEMS_SPACING = 5;
//#  	public static final short SEXY_HOT_MENU_BAR_OFFSET_Y = 13;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PHOTO_BORDER = 7;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PHOTO_WIDTH = 60;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte PLAYER_PAINEL_WIDTH = 118;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final short PLAYER_PAINEL_HEIGHT = 140;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte SAFE_MARGIN = 3;
//# 	/**
//# 	 * largura da carta
//# 	 */
//# 	public static final byte CARD_WIDTH = 35;
//# 	/**
//# 	 * altura da carta
//# 	 */
//# 	public static final byte CARD_HEIGHT = 54;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_X_LANDSCAPE = - 50;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_Y_LANDSCAPE = - 50;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_X_PORTRAIT = - 70;
//#  	/**
//#  	 *
//#  	 */
//#  	public static final byte CHIPS_Y_PORTRAIT = - 100;
//#  	public static final byte SCORE_LABEL_Y_ADJUSTMENT = -3;
//# 	public static final byte BET_LABEL_Y_ADJUSTMENT = 1;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_X_PORTRAIT = + ( CARD_WIDTH >> 1) - 2 * CARD_WIDTH;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_Y_PORTRAIT = - ( ( PLAYER_PAINEL_HEIGHT >> 1) + CARD_ON_PLAYERS_HAND * ( CARD_HEIGHT >> 1) );
//#   	/*
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_X_LANDSCAPE = PLAYER_PAINEL_WIDTH;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_Y_LANDSCAPE = - CARD_HEIGHT >> 1;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_X_PORTRAIT = 0;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_Y_PORTRAIT = CARD_HEIGHT >> 1;
//#   	/*
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_X_LANDSCAPE = 0;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_Y_LANDSCAPE = - CARD_HEIGHT << 1;
//# 	public static final byte CARD_STRIDE_Y = CARD_HEIGHT >> 1;
//#  	public static final byte CARD_STRIDE_X = 3;
//# 	public static final short MENU_BOTTOM_BAR_HEIGHT = 70;
//#
//#  	public static final byte RECOMMEND_LABEL_LEFT_X = 12;
//#  	public static final byte RECOMMEND_LABEL_LEFT_Y = 13;
//#  	public static final byte RECOMMEND_LABEL_LEFT_WIDTH = 37;
//#  	public static final byte RECOMMEND_LABEL_LEFT_HEIGHT = 25;
//#
//#  	public static final byte RECOMMEND_LABEL_RIGHT_X = 68;
//#  	public static final byte RECOMMEND_LABEL_RIGHT_Y = 13;
//#  	public static final byte RECOMMEND_LABEL_RIGHT_WIDTH = 76;
//#  	public static final byte RECOMMEND_LABEL_RIGHT_HEIGHT = 25;
//#
//#   	public static final byte RESULT_CARDS_ROW1_Y =   ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2 );
//#   	public static final byte RESULT_CARDS_ROW2_Y = - ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2 );
//#
//#  	public static final short KEYPAD_MINIMUM_SIZE = 120;
//#
//# 	public static final int LABELS_GROUP_BORDER = -20;
//#
//# 	public static final int TEXT_TITLE_MARGIN = 25;
//# //</editor-fold>
	//#elif SCREEN_SIZE == "BIG_BLACKBERRY"
//# 	public static final int TEXT_TITLE_MARGIN = 8;
//# 	public static final int LABELS_GROUP_BORDER = -5;
//# /**
//# 	 * largura da carta
//# 	 */
//# 	public static final byte CARD_WIDTH = 24;
//# 	/**
//# 	 * altura da carta
//# 	 */
//# 	public static final byte CARD_HEIGHT = 34;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PHOTO_BORDER = 5;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PHOTO_WIDTH = 60;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PLAYER_PAINEL_WIDTH = 95;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte PLAYER_PAINEL_HEIGHT = 95;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte SAFE_MARGIN = 5;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte CHIPS_X_LANDSCAPE = - 50;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte CHIPS_Y_LANDSCAPE = - 33;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte CHIPS_X_PORTRAIT = - 70;
//# 	/**
//# 	 *
//# 	 */
//# 	public static final byte CHIPS_Y_PORTRAIT = - 70;
//# 	public static final byte CARD_STRIDE_X = 3;
//#  	public static final byte RESULT_CARDS_ROW1_Y = -5 + 3 * ( - CARD_HEIGHT / 8 - CARD_HEIGHT / 2 ) / 2;
//#  	public static final byte RESULT_CARDS_ROW2_Y = 0;
//# 
//# 	public static final byte SCORE_LABEL_Y_ADJUSTMENT = -1;
//# 	public static final byte BET_LABEL_Y_ADJUSTMENT = 3;
//# 	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_X_PORTRAIT = - ( CARD_WIDTH << 1);
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_Y_PORTRAIT = - ( - ( PLAYER_PAINEL_HEIGHT >> 3 ) + ( PLAYER_PAINEL_HEIGHT >> 1 ) + ( CARD_ON_PLAYERS_HAND ) * ( CARD_HEIGHT >> 1) );
//#   	/*
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_X_LANDSCAPE = PLAYER_PAINEL_WIDTH;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PLAYER_CARDS_Y_LANDSCAPE = - CARD_HEIGHT >> 1;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_X_PORTRAIT = 0;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_Y_PORTRAIT =  - CARD_HEIGHT >> 1;
//#   	/*
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_X_LANDSCAPE = 0;
//#   	/**
//#   	 *
//#   	 */
//#   	public static final byte PUBLIC_CARDS_Y_LANDSCAPE = - 2 * CARD_HEIGHT;
//#  	public static final byte CARD_STRIDE_Y = ( CARD_HEIGHT >> 1 );
//# 
//# 
//# 	public static final short MENU_BOTTOM_BAR_HEIGHT = 50;
//# 
//# 	public static final short SEXY_HOT_MENU_BAR_OFFSET_Y = 13;
//# 
//# 	public static final byte OPTIONS_ITEMS_SPACING = 5;
//# 
//# 	public static final byte RECOMMEND_LABEL_LEFT_X = 11;
//# 	public static final byte RECOMMEND_LABEL_LEFT_Y = 10;
//# 	public static final byte RECOMMEND_LABEL_LEFT_WIDTH = 33;
//# 	public static final byte RECOMMEND_LABEL_LEFT_HEIGHT = 21;
//# 
//# 	public static final byte RECOMMEND_LABEL_RIGHT_X = 49;
//# 	public static final byte RECOMMEND_LABEL_RIGHT_Y = 8;
//# 	public static final byte RECOMMEND_LABEL_RIGHT_WIDTH = 76;
//# 	public static final byte RECOMMEND_LABEL_RIGHT_HEIGHT = 21;
//# 
//# 	public static final short KEYPAD_MINIMUM_SIZE = 100;
//# 
//# 	//</editor-fold>
	//#endif
//</editor-fold>
}
