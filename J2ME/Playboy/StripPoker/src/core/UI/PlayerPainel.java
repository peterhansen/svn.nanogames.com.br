package core.UI;
//<editor-fold desc="Imports">
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.Rectangle;
import core.PersistentGameInformation;
import screens.GameMIDlet;
//</editor-fold>


/**
 * Painel de dados do jogador
 * @author Daniel "Monty" Monteiro
 */
public class PlayerPainel extends DrawableGroup implements Updatable, Constants {
	//<editor-fold desc="Constantes da classe">


	/**
	 * Numero de caracteres no painel
	 */
	public static final byte MAX_NUM_CHARS = 6;
	//</editor-fold>
	//<editor-fold desc="Campos da classe">
	/**
	 * fundo do painel de jogador
	 */
	private DrawableImage playerPainel_bkg;
	/**
	 * placar
	 */
	private Label scoreLabel;
	/**
	 * Controla se o painel deve piscar ou não
	 */
	private boolean blink;
	private int accTime;
	private Pattern playerBkg;
	//</editor-fold>
	//<editor-fold desc="Métodos da classe">


	/**
	 * construtor: instancia e define um tamanho fixo para o painel, mas ainda nao
	 * define um jogador para o painel
	 * @throws Exception
	 */
	public PlayerPainel() throws Exception {
		super( 7 );

		playerPainel_bkg = new DrawableImage( PATH_IMAGES + "player_window.png" );

		insertDrawable( playerPainel_bkg );

		playerBkg = new Pattern( COLOR_PLAYER_BKG );

		setSize( playerPainel_bkg.getWidth(), playerPainel_bkg.getHeight() );

		scoreLabel = new Label( FONT_NUMBERS );
		insertDrawable( scoreLabel );
		setScoreLabel( PersistentGameInformation.getInstance().getPoints() );
	}


	/**
	 *
	 * @param score define o conteúdo do painel
	 */
	public void setScoreLabel( long score ) {
		String text = String.valueOf( score );
		String finalText = "";

		for ( int c = 0; c < MAX_NUM_CHARS - text.length() - 1; c++ ) {
			finalText += '0';
		}

		scoreLabel.setText( finalText + text );
	}


	/**
	 * Carrega e trata os ajustes de interface referentes a qual jogador este painel
	 * representa
	 * @param playerIndex define qual jogador este painel representara
	 */
	public void setPlayer( int playerIndex ) {

		scoreLabel.setVisible( true );

		if ( getUsedSlots() != 2 )
			return ;
		
		boolean flip = ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() );
		DrawableImage playerFace = null;
		DrawableImage moneySign = null;
		Rectangle faceArea = null;
		int playerOffset = 0;

		try {
			playerFace = new DrawableImage( PATH_IMAGES + "player_face.png" );
			moneySign = new DrawableImage( PATH_IMAGES + "cifrao.png" );
			insertDrawable( moneySign );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}

		faceArea = playerFace.getViewport();

		if ( playerIndex == PLAYER_BARBARA ) {

			playerFace.setPosition( getWidth() - ( playerFace.getWidth() >> 1 ) - PHOTO_BORDER , getHeight() - PHOTO_BORDER - playerFace.getHeight() );
			moneySign.setPosition( 0, PHOTO_BORDER);

		} else {

			playerOffset = ( playerIndex * ( playerFace.getWidth() >> 1 ) );

			if ( flip ) {

				playerPainel_bkg.setTransform( TRANS_MIRROR_H );
				moneySign.setPosition( 0, PHOTO_BORDER);
				playerFace.setPosition( PHOTO_BORDER + ( PHOTO_BORDER >> 1 ) + ( PHOTO_BORDER >> 2 ) - playerOffset, getHeight() - PHOTO_BORDER - playerFace.getHeight() );

			} else {

				playerPainel_bkg.setTransform( TRANS_MIRROR_V | TRANS_MIRROR_H );
				moneySign.setPosition( 0, getHeight() - moneySign.getHeight() );
				playerFace.setPosition( PHOTO_BORDER + ( PHOTO_BORDER >> 1 ) + ( PHOTO_BORDER >> 2 ) - playerOffset, PHOTO_BORDER + ( PHOTO_BORDER >> 1 ) + ( PHOTO_BORDER >> 2 ) );

			}

		}

		if ( faceArea == null ) {
			faceArea = new Rectangle();
		}

		faceArea.set( getPosX() + playerFace.getPosX() + playerOffset, getPosY() + playerFace.getPosY(),
					  playerFace.getWidth() >> 1, playerFace.getHeight() );
		playerFace.setViewport( faceArea );
		insertDrawable( playerBkg );
		playerBkg.setVisible( false );
		insertDrawable( playerFace );

		scoreLabel.setPosition( moneySign.getPosX() + moneySign.getWidth() + 3, moneySign.getPosY() + ( ( moneySign.getHeight() - scoreLabel.getHeight() ) >> 1 ) + SCORE_LABEL_Y_ADJUSTMENT );

		playerBkg.setPosition( playerFace.getPosX() + playerOffset - 4, playerFace.getPosY() - 1 );
		playerBkg.setSize( faceArea.width + 4, faceArea.height + 1 );
	}


	/**
	 * @return the blink
	 */
	public boolean isBlink() {
		return blink;
	}


	/**
	 * @param blink the blink to set
	 */
	public void setBlink( boolean blink ) {
		this.blink = blink;
		playerBkg.setVisible( blink );
	}


	public void update( int delta ) {
		if ( blink ) {
			accTime += delta;

			if ( accTime > TIME_BLINK ) {
				accTime = 0;
				playerBkg.setVisible( !playerBkg.isVisible() );
				scoreLabel.setVisible( !scoreLabel.isVisible() );
			}
		}
	}
	//</editor-fold>
}
