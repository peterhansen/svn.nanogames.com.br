package core.UI;
//<editor-fold desc="imports">
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.PersistentGameInformation;
import core.PokerCard;
import core.PokerPlayer;
import screens.GameMIDlet;
//</editor-fold>


/**
 * @author Daniel "Monty" Monteiro
 */
public class GameTable extends UpdatableGroup implements Constants, Updatable {
//<editor-fold desc="constantes da classe">
	//<editor-fold desc="turnos">


	/**
	 *
	 */
	public static final byte TURN_1_PREFLOP = 1;
	/**
	 *
	 */
	public static final byte TURN_2_FLOP = 2;
	/**
	 *
	 */
	public static final byte TURN_3_TURN = 3;
	/**
	 *
	 */
	public static final byte TURN_4_RIVER = 4;
	/**
	 *
	 */
	public static final byte TURN_5_SHOWDOWN = 5;
	/**
	 *
	 */
	public static final byte TURN_6_POST_SHOWDOWN = 6;
	/**
	 *
	 */
	public static final byte TURN_7 = 7;
	//</editor-fold>
//</editor-fold>
//<editor-fold desc="campos da classe">
	/**
	 * turno atual [1..7]
	 */
	private byte turn;
	/**
	 * jogador atual [0..1]
	 */
	private byte currentPlayer;
	/**
	 * Valor do montante atual
	 */
	private long pot;
	/**
	 * Valor da ultima aposta.
	 */
	private long currentBet;
	/**
	 * Qual jogador é o dealer
	 */
	private byte dealer;
	/**
	 * Informações sobre os jogadores
	 */
	private PokerPlayer[] player;
	/**
	 * Cartas na mesa ( grupo de drawables )
	 */
	private UpdatableGroup cardsOnTable;
	/**
	 * Cartas na mao do jogador
	 */
	private UpdatableGroup[] playerCards;
	/**
	 * Chips
	 */
	private UpdatableGroup chipsOnTable;
	/**
	 * Cartas na mesa ( referencias para objetos de mecânica de jogo )
	 */
	private GraphicalPokerCard[] publicCards;
	/**
	 * Quem venceu o round
	 */
	private byte winnerOfTheRound;
	/**
	 * Controla se a mesa esta piscando ou não
	 */
	private boolean blink;
	/**
	 * Tempo entre as piscadas
	 */
	private int accBlinkTime;
	/**
	 *
	 */
	private UpdatableGroup tableNormal;
	/**
	 * 
	 */
	private UpdatableGroup tableLit;
	private long startMoney;
//</editor-fold>
//<editor-fold desc="metodos da classe">


	/**
	 * Construtor: ja instancia (mas nao posiciona) os elementos graficos,
	 * exceto a aparencia basica da mesa
	 * @param level indica o nivel da partida atual
	 * @throws Exception é lançada caso algum carregamento não funcione
	 */
	public GameTable( byte level ) throws Exception {
		super( 15 );
		//<editor-fold desc="inicialização da mecânica da mesa de jogo">
		blink = false;
		winnerOfTheRound = PLAYER_NO_PLAYER;
		pot = 0;
		publicCards = new GraphicalPokerCard[ PUBLIC_CARDS_ON_TABLE ];
		player = new PokerPlayer[ 2 ];
		player[PLAYER_BARBARA] = new PokerPlayer( PLAYER_BARBARA, level );
		player[PLAYER_BOB] = new PokerPlayer( PLAYER_BOB, level );
		//as damas primeiro. Neste ponto, o currentPlayer é o dealer.
		dealer = currentPlayer = ( level % 2 == 0 ) ? PLAYER_BARBARA : PLAYER_BOB;
		//</editor-fold>
		//<editor-fold desc="inicialização dos elementos gráficos">
		DrawableImage tableCorner;

		tableNormal = new UpdatableGroup( 4 );
		insertDrawable( tableNormal );

		tableLit = new UpdatableGroup( 4 );
		insertDrawable( tableLit );

		//<editor-fold desc="Criando a mesa com aparência normal">

		tableCorner = new DrawableImage( PATH_IMAGES + "table_0.png" );
		final int imageWidth = tableCorner.getWidth();
		final int imageHeight = tableCorner.getHeight();
		tableCorner.setPosition( 0, 0 );
		tableNormal.insertDrawable( tableCorner );

		tableCorner = new DrawableImage( tableCorner );
		tableCorner.setPosition( -imageWidth, 0 );
		tableCorner.setTransform( TRANS_MIRROR_H );
		tableNormal.insertDrawable( tableCorner );

		tableCorner = new DrawableImage( tableCorner );
		tableCorner.setPosition( 0, -imageHeight );
		tableCorner.setTransform( TRANS_MIRROR_V );
		tableNormal.insertDrawable( tableCorner );


		tableCorner = new DrawableImage( tableCorner );
		tableCorner.setPosition( -imageWidth, -imageHeight );
		tableCorner.setTransform( TRANS_MIRROR_H | TRANS_MIRROR_V );
		tableNormal.insertDrawable( tableCorner );


		tableNormal.setVisible( !blink );
		tableNormal.setClipTest( false );

		//</editor-fold>
		//<editor-fold desc="Criando a mesa com as luzes acesas">
		tableCorner = new DrawableImage( PATH_IMAGES + "table_1.png" );
		tableCorner.setPosition( 0, 0 );
		tableLit.insertDrawable( tableCorner );

		tableCorner = new DrawableImage( tableCorner );
		tableCorner.setPosition( -imageWidth, 0 );
		tableCorner.setTransform( TRANS_MIRROR_H );
		tableLit.insertDrawable( tableCorner );

		tableCorner = new DrawableImage( tableCorner );
		tableCorner.setPosition( 0, -imageHeight );
		tableCorner.setTransform( TRANS_MIRROR_V );
		tableLit.insertDrawable( tableCorner );

		tableCorner = new DrawableImage( tableCorner );
		tableCorner.setPosition( -imageWidth, -imageHeight );
		tableCorner.setTransform( TRANS_MIRROR_H | TRANS_MIRROR_V );
		tableLit.insertDrawable( tableCorner );

		tableLit.setVisible( blink );
		tableLit.setClipTest( false );

		//</editor-fold>

		DrawableImage logo = new DrawableImage( PATH_IMAGES + "sexy_hot.png" );
		logo.setPosition( - logo.getWidth() >> 1, -logo.getHeight() >> 1 );
		insertDrawable( logo );

		/**
		 * Cada valor de chip, em media, so' aparece 4 vezes no maximo
		 * antes de dar vez ao seu irmao mais valioso.
		 * isso tambem significa que o valor maximo permitido e':
		 * 4 * 500 + 4 * 100 + 4 * 50 + 4 * 10 + 4 * 5 = 4 * 665 = 2660
		 */
		chipsOnTable = new UpdatableGroup( 3 * 4 * 5 );
		insertDrawable( chipsOnTable );

		cardsOnTable = new UpdatableGroup( PUBLIC_CARDS_ON_TABLE );
		cardsOnTable.setClipTest( false );
		insertDrawable( cardsOnTable );

		playerCards = new UpdatableGroup[ 2 ];
		playerCards[PLAYER_BARBARA] = new UpdatableGroup( 7 );
		playerCards[PLAYER_BOB] = new UpdatableGroup( 7 );
		playerCards[PLAYER_BARBARA].setClipTest( false );
		playerCards[PLAYER_BOB].setClipTest( false );
		insertDrawable( playerCards[PLAYER_BARBARA] );
		insertDrawable( playerCards[PLAYER_BOB] );
		playerCards[ PLAYER_BARBARA ].setVisible( false );
		setClipTest( false );
		//</editor-fold>
	}


	/**
	 * Uma pequena funçõa de facilidade. Não tem efeitos colaterais
	 * Ok, é um açucar sintático
	 * @param playerIndex é um jogador...
	 * @return ...cujo seguinte é retornado
	 */
	public final byte whosNext( byte playerIndex ) {
		return ( byte ) ( ( playerIndex + 1 ) % 2 );
	}


	/**
	 * Começa o jogo, ja iniciando o primeiro turno.
	 */
	public final void deal() {
		try {
			turn = TURN_1_PREFLOP;
			winnerOfTheRound = PLAYER_NO_PLAYER;
			long currentBlind = getBigBlindValue();
			//obtem o dinheiro do jogador
			long persistentCash = PersistentGameInformation.getInstance().getPoints();

			if ( persistentCash > currentBlind && persistentCash < ( ( 2 * PersistentGameInformation.DEFAULT_POINTS ) - currentBlind ) ) {
				getPlayerById( PLAYER_BOB ).setInitialCash( persistentCash );
			} else {
				getPlayerById( PLAYER_BOB ).setInitialCash( PersistentGameInformation.DEFAULT_POINTS );
			}


			//nada se cria, tudo se transforma.
			getPlayerById( PLAYER_BARBARA ).setInitialCash( ( 2 * PersistentGameInformation.DEFAULT_POINTS ) - getPlayerById(
					PLAYER_BOB ).getMoney() );

			//distribui as cartas
			dealCards();
			//small blind deve colocar dinheiro no pot
			getPlayerById( dealer ).blind( currentBlind );
			//big blind deve colocar o dobro de dinheiro no pot
			getPlayerById( whosNext( dealer ) ).blind( currentBlind >> 1 );

			pot += ( currentBlind >> 1 ) + currentBlind;

			currentBet = ( int ) ( currentBlind );

			getPlayerById( dealer ).setDesiredBet( currentBet );
			//big blind deve colocar o dobro de dinheiro no pot
			getPlayerById( whosNext( dealer ) ).setDesiredBet( currentBet );

			startTurn();
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
	}


	/**
	 * finaliza um turno, tomando todas a providencias
	 */
	public final void endTurn() {
		switch ( turn ) {
			case TURN_1_PREFLOP:
				break;
			case TURN_2_FLOP:
				break;
			case TURN_3_TURN:
				break;
			case TURN_4_RIVER:
				break;
			case TURN_5_SHOWDOWN:
				break;
			case TURN_6_POST_SHOWDOWN:
				break;
			case TURN_7:
				break;
			default:
				//#if DEBUG == "true"
				System.out.println( "valor de turno inválido:" + turn );
				//#endif
				break;
		}

		++turn;
		startTurn();
	}


	/**
	 * A ideia 'e oferecer o calculo sem efeitos colaterais, para que possa ser sempre consultado
	 * @return o vencedor do jogo
	 */
	public void simulateEnding() {
		getPlayerById( PLAYER_BARBARA ).getHand().backup();
		getPlayerById( PLAYER_BOB ).getHand().backup();

		for ( byte c = 0; c < 5; ++c ) {
			getPlayerById( PLAYER_BARBARA ).getHand().addCard( publicCards[c].getCardData() );
			getPlayerById( PLAYER_BOB ).getHand().addCard( publicCards[c].getCardData() );
		}

		justEvaluate();

		getPlayerById( PLAYER_BARBARA ).getHand().restore();
		getPlayerById( PLAYER_BOB ).getHand().restore();
	}


	/**
	 * Começo de turno
	 */
	private final void startTurn() {

		switch ( turn ) {
			case TURN_1_PREFLOP:
				setBlink( false );
				break;
			case TURN_2_FLOP:
			case TURN_3_TURN:
			case TURN_4_RIVER:
				for ( byte c = 0; c < turn + 1; ++c ) {
					publicCards[c].setFlipped( true );
				}
				break;
			case TURN_5_SHOWDOWN: {

				for ( byte e = 0; e < PUBLIC_CARDS_ON_TABLE; ++e ) {
					publicCards[e].setFlipped( true );
				}

				for ( byte d = 0; d < CARD_ON_PLAYERS_HAND; ++d ) {
					( ( GraphicalPokerCard ) playerCards[PLAYER_BARBARA].getDrawable( d ) ).flip();
				}

				if ( winnerOfTheRound == PLAYER_NO_PLAYER ) {

					getPlayerById( PLAYER_BARBARA ).getHand().backup();
					getPlayerById( PLAYER_BOB ).getHand().backup();

					for ( byte c = 0; c < turn; ++c ) {
						if ( turn > 3 && turn < 7 ) {
							getPlayerById( PLAYER_BARBARA ).getHand().addCard( publicCards[c].getCardData() );
							getPlayerById( PLAYER_BOB ).getHand().addCard( publicCards[c].getCardData() );
						}
					}
					winnerOfTheRound = evaluateOutcome();
				} else {
					getPlayerById( PLAYER_BARBARA ).getHand().restoreIfNeeded();
					getPlayerById( PLAYER_BOB ).getHand().restoreIfNeeded();

					simulateEnding();
				}

				byte level = PersistentGameInformation.getInstance().getLevel();
				int tries = PersistentGameInformation.getInstance().getTriesLeft();

				if ( winnerOfTheRound == PLAYER_BOB ) {
					PersistentGameInformation.getInstance().setLevel( ( byte ) ( level + 1 ) );
					getPlayerById( PLAYER_BOB ).win( pot );
				} else {
					PersistentGameInformation.getInstance().setTriesLeft( ( byte ) ( tries - 1 ) );
					getPlayerById( PLAYER_BARBARA ).win( pot );
				}

				getPlayerById( PLAYER_BARBARA ).getHand().restore();
				getPlayerById( PLAYER_BOB ).getHand().restore();

				try {
					Sprite anim = new Sprite( PATH_IMAGES + "carta_virando" );
					GraphicalPokerCard gpc;

					playerCards[PLAYER_BARBARA].removeAllDrawables();
					playerCards[PLAYER_BOB].removeAllDrawables();
					PokerCard[] hand;

//						if ( getPlayerById( PLAYER_BARBARA ).getHand().getCardsWorth() != null ) {
					hand = getPlayerById( PLAYER_BARBARA ).getHand().getCardsWorth();
					for ( byte d = 0; d < hand.length; ++d ) {
//								if ( hand[ hand.length - d - 1 ] != null ) {
						gpc = new GraphicalPokerCard( hand[ d ], anim );
						playerCards[PLAYER_BARBARA].insertDrawable( gpc );
						gpc.setPosition( d * ( CARD_WIDTH + CARD_STRIDE_X ), 0 );
						gpc.flip();
//							}
					}

//						if ( getPlayerById( PLAYER_BOB ).getHand().getCardsWorth() != null ) {
					hand = getPlayerById( PLAYER_BOB ).getHand().getCardsWorth();

					for ( byte d = 0; d < hand.length; ++d ) {
//								if ( hand[ hand.length - d - 1 ] != null ) {
						gpc = new GraphicalPokerCard( hand[ d ], anim );
						playerCards[PLAYER_BOB].insertDrawable( gpc );
						gpc.setPosition( d * ( CARD_WIDTH + CARD_STRIDE_X ), 0 );
						gpc.flip();
//							}
					}

					playerCards[ PLAYER_BARBARA ].setVisible( true );
					
					if ( !( ( GameMIDlet) GameMIDlet.getInstance() ).isLandscape() ) {
						playerCards[PLAYER_BARBARA].setPosition( -( ( CARD_WIDTH + CARD_STRIDE_X ) * CARDS_WORTH ) / 2,
																 RESULT_CARDS_ROW1_Y );
						playerCards[PLAYER_BOB].setPosition( -( ( CARD_WIDTH + CARD_STRIDE_X ) * CARDS_WORTH ) / 2,
															 RESULT_CARDS_ROW2_Y );
					} else {
						playerCards[PLAYER_BARBARA].setPosition( -( ( CARD_WIDTH + CARD_STRIDE_X ) * CARDS_WORTH ) / 2,
															 3 * RESULT_CARDS_ROW1_Y );
						playerCards[PLAYER_BOB].setPosition( -( ( CARD_WIDTH + CARD_STRIDE_X ) * CARDS_WORTH ) / 2,
															 - RESULT_CARDS_ROW2_Y );
					}

					cardsOnTable.removeAllDrawables();

				} catch ( Exception e ) {
					//#if DEBUG == "true"
					System.out.println( getPlayerById( PLAYER_BARBARA ).getHand().getCardsWorth() );
					e.printStackTrace();
					//#endif
				}

			}
			break;
			case TURN_6_POST_SHOWDOWN:
				break;
			default:
				//#if DEBUG == "true"
				System.out.println( "valor de turno inválido:" + turn );
				//#endif
				break;
		}
	}


	/**
	 * finaliza o jogo, ja contabilizando o resultado
	 */
	public final void endGame() {
	}


	/**
	 * Obtem jogador
	 * @param id do jogador
	 * @return referencia para o jogador do id
	 */
	public final PokerPlayer getPlayerById( byte id ) {
		if ( id < 0 || id > 1 ) {
			//#if DEBUG == "true"
			System.out.println( "O id não aponta para nenhum jogador válido:" + id );
			//#endif
		}
		return player[id];
	}


	/**
	 *
	 * @return referencia para o jogador que esta atuando como o dealer
	 */
	private final PokerPlayer getDealer() {
		return getPlayerById( dealer );
	}


	/**
	 * Distribui as cartas
	 */
	private void dealCards() throws Exception {
		//cria o baralho
		byte rank;
		byte suit;
		byte used = 0;
		GraphicalPokerCard[] deck;

		deck = new GraphicalPokerCard[ CARDS_PER_DECK ];
		Sprite cardFlipAnimation = new Sprite( PATH_IMAGES + "carta_virando" );

		for ( byte c = PokerCard.SUIT_SPADE; c < SUITS_PER_DECK; ++c ) {
			for ( byte d = PokerCard.RANK_2; d <= RANKS_PER_SUIT; ++d ) {
				deck[used] = new GraphicalPokerCard( c, d, cardFlipAnimation );
				++used;
			}
		}

		//para cada jogador:
		//	seleciona duas cartas
		int cardId;
		for ( byte d = 0; d < NUM_PLAYERS; ++d ) {

//#if DEBUG_SEQUENCES == "true"
//# 			getPlayerById( d ).giveCard( new GraphicalPokerCard( PokerCard.SUIT_DIAMOND, PokerCard.RANK_2, cardFlipAnimation) );
//# 			getPlayerById( d ).giveCard( new GraphicalPokerCard( PokerCard.SUIT_HEART, PokerCard.RANK_3, cardFlipAnimation) );
//#else
			for ( byte e = 0; e < CARD_ON_PLAYERS_HAND; ++e ) {
				do {
					//sorteia um numero
					cardId = NanoMath.randInt();

					//e garante que ele é um indice válido
					if ( cardId < 0 ) {
						cardId = -cardId;
					}

					cardId = ( cardId % CARDS_PER_DECK );

					//mas não adianta nada se ja tiver sido escolhido
				} while ( deck[cardId].getPlayer() != PLAYER_NO_PLAYER );

				//por fim, o jogador recebe a carta correspondente
				getPlayerById( d ).giveCard( deck[cardId] );

			}
//#endif
			getPlayerById( d ).sortCards();
		}

		GraphicalPokerCard graphCard;

		for ( byte h = 0; h < NUM_PLAYERS; ++h ) {
			for ( byte g = 0; g < CARD_ON_PLAYERS_HAND; ++g ) {
				graphCard = getPlayerById( h ).getCard( g );

				if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
					graphCard.setPosition( ( g * ( CARD_WIDTH + CARD_STRIDE_X ) ), -CARD_HEIGHT );
				} else {
					graphCard.setPosition( ( g * ( CARD_WIDTH + CARD_STRIDE_X ) ),
										   ( CARD_HEIGHT >> 4 ) + ( ( h == PLAYER_BOB ) ? -( g * CARD_STRIDE_Y ) - CARD_STRIDE_Y / 2 : +( g * CARD_STRIDE_Y ) + CARD_STRIDE_Y / 2 ) );
				}

				playerCards[h].insertDrawable( graphCard );
			}
		}


		//separa 5 cartas		
//#if DEBUG_SEQUENCES == "true"
//# 				publicCards[ 0 ] = new GraphicalPokerCard( PokerCard.SUIT_DIAMOND, PokerCard.RANK_4, cardFlipAnimation) ;
//# 				publicCards[ 0 ].setFlipped( false );
//# 				publicCards[ 0 ].setPlayer( PLAYER_TABLE );
//# 				publicCards[ 0 ].setPosition( ( cardsOnTable.getUsedSlots() * ( CARD_WIDTH + CARD_STRIDE_X ) ), 0 );
//# 				cardsOnTable.insertDrawable( publicCards[ 0 ] );
//# 				publicCards[ 1 ] = new GraphicalPokerCard( PokerCard.SUIT_DIAMOND, PokerCard.RANK_5, cardFlipAnimation) ;
//# 				publicCards[ 1 ].setFlipped( false );
//# 				publicCards[ 1 ].setPlayer( PLAYER_TABLE );
//# 				publicCards[ 1 ].setPosition( ( cardsOnTable.getUsedSlots() * ( CARD_WIDTH + CARD_STRIDE_X ) ), 0 );
//# 				cardsOnTable.insertDrawable( publicCards[ 1 ] );
//# 				publicCards[ 2 ] = new GraphicalPokerCard( PokerCard.SUIT_CLUB, PokerCard.RANK_Q, cardFlipAnimation) ;
//# 				publicCards[ 2 ].setFlipped( false );
//# 				publicCards[ 2 ].setPlayer( PLAYER_TABLE );
//# 				publicCards[ 2 ].setPosition( ( cardsOnTable.getUsedSlots() * ( CARD_WIDTH + CARD_STRIDE_X ) ), 0 );
//# 				cardsOnTable.insertDrawable( publicCards[ 2 ] );
//# 				publicCards[ 3 ] = new GraphicalPokerCard( PokerCard.SUIT_SPADE, PokerCard.RANK_5, cardFlipAnimation) ;
//# 				publicCards[ 3 ].setFlipped( false );
//# 				publicCards[ 3 ].setPlayer( PLAYER_TABLE );
//# 				publicCards[ 3 ].setPosition( ( cardsOnTable.getUsedSlots() * ( CARD_WIDTH + CARD_STRIDE_X ) ), 0 );
//# 				cardsOnTable.insertDrawable( publicCards[ 3 ] );
//# 				publicCards[ 4 ] = new GraphicalPokerCard( PokerCard.SUIT_DIAMOND, PokerCard.RANK_6, cardFlipAnimation) ;
//# 				publicCards[ 4 ].setFlipped( false );
//# 				publicCards[ 4 ].setPlayer( PLAYER_TABLE );
//# 				publicCards[ 4 ].setPosition( ( cardsOnTable.getUsedSlots() * ( CARD_WIDTH + CARD_STRIDE_X ) ), 0 );
//# 				cardsOnTable.insertDrawable( publicCards[ 4 ] );
//#else
		for ( byte f = 0; f < PUBLIC_CARDS_ON_TABLE; ++f ) {
			do {
				//sorteia um numero
				cardId = NanoMath.randInt();

				//e garante que ele é um indice válido
				if ( cardId < 0 ) {
					cardId = -cardId;
				}

				cardId = ( cardId % CARDS_PER_DECK );

				//mas não adianta nada se ja tiver sido escolhido
			} while ( deck[cardId].getPlayer() != PLAYER_NO_PLAYER );

			publicCards[f] = deck[cardId];
			publicCards[f].setFlipped( false );
			publicCards[f].setPlayer( PLAYER_TABLE );
			publicCards[f].setPosition( ( cardsOnTable.getUsedSlots() * ( CARD_WIDTH + CARD_STRIDE_X ) ), 0 );
			cardsOnTable.insertDrawable( publicCards[f] );
		}
//#endif
		System.gc();
	}


	/**
	 *
	 * @return O big blind do nível atual
	 */
	private long getBigBlindValue() {
		return INITIAL_BLIND * 2;
	}


	/**
	 * Re-ajuste espacial da mesa
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
			cardsOnTable.setPosition( ( - 5 * ( CARD_WIDTH + CARD_STRIDE_X ) ) >> 1, /*- ( height >> 1 ) +*/ PUBLIC_CARDS_Y_LANDSCAPE );
		} else {
			cardsOnTable.setPosition( ( - 5 * ( CARD_WIDTH + CARD_STRIDE_X ) ) >> 1, PUBLIC_CARDS_Y_PORTRAIT );
		}

		cardsOnTable.setSize( ( width * 6 ) / 10, CARD_HEIGHT );


		if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
			playerCards[PLAYER_BOB].setPosition( -( width >> 1 ) + PLAYER_CARDS_X_LANDSCAPE,
												 -( height >> 2 ) + PLAYER_CARDS_Y_LANDSCAPE );
		} else {
			playerCards[PLAYER_BOB].setPosition( PLAYER_CARDS_X_PORTRAIT, ( height >> 1 ) + PLAYER_CARDS_Y_PORTRAIT );
		}

		playerCards[PLAYER_BOB].setSize( CARD_WIDTH * 2, ( 3 * CARD_HEIGHT ) / 2 );

		if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
			playerCards[PLAYER_BARBARA].setPosition( -( width >> 1 ) + PLAYER_CARDS_X_LANDSCAPE,
													 -( height >> 2 ) + PLAYER_CARDS_Y_LANDSCAPE );
		} else {
			playerCards[PLAYER_BARBARA].setPosition(
					( width >> 1 ) - ( CARD_WIDTH + CARD_STRIDE_X ) * CARD_ON_PLAYERS_HAND,
					-( height >> 1 ) + PLAYER_PAINEL_HEIGHT );
		}

		playerCards[PLAYER_BARBARA].setSize( CARD_WIDTH * 2, ( 3 * CARD_HEIGHT ) / 2 );

		if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
			chipsOnTable.setPosition( CHIPS_X_LANDSCAPE, CHIPS_Y_LANDSCAPE );
		} else {
			chipsOnTable.setPosition( CHIPS_X_PORTRAIT, CHIPS_Y_PORTRAIT );
		}

		chipsOnTable.setSize( 200, 200 );
	}


	/**
	 * O jogador quer apostar. Avalia também se a aposta foi aumentada
	 */
	public void placeBet() {
		// TODO esse segundo parametro pra Bet me parece meio inutil
		boolean matchedBet = false;

		for ( byte f = 0; f < player.length; ++f ) {

			if ( getPlayerById( f ).getMoney() < /*getPlayerById( f ).getBet()*/ currentBet ) {
				getPlayerById( f ).fold();
				winnerOfTheRound = whosNext( f );

				if ( turn < TURN_4_RIVER ) {
					turn = TURN_4_RIVER;
				}

				endTurn();
				return;
			}

			if ( getPlayerById( f ).getMoney() == currentBet ) {
				getPlayerById( f ).allIn();

				if ( turn < TURN_4_RIVER ) {
					turn = TURN_4_RIVER;
				}

				endTurn();
				return;
			}


//			if ( getPlayerById( f ).getLastAction() == PokerPlayer.ACTION_ALLIN ) {
//				//#if DEBUG == "true"
//				System.out.println( "jogador " + f + " é ALL IN!" );
//				//#endif
//				matchedBet = false;
//				continue;
//			}

			pot += getPlayerById( f ).bet( getPlayerById( f ).getBet(), currentBet == getPlayerById( f ).getBet() );
			matchedBet = ( currentBet < getPlayerById( f ).getBet() );

			if ( matchedBet ) {
				currentBet = getPlayerById( f ).getBet();
			}
		}

		if ( !matchedBet ) {
			endTurn();
		}
	}


	/**
	 * Avalia o resultado do jogo
	 * @return retorna o ID do jogador vencedor
	 */
	private byte justEvaluate() {
		byte winner;
		int winnerScore;
		int currentScore;

		winner = 0;
		winnerScore = getPlayerById( ( byte ) winner ).getHand().scoreSequence();

		for ( byte f = 1; f < NUM_PLAYERS; ++f ) {

			currentScore = getPlayerById( f ).getHand().scoreSequence();

			if ( currentScore >= winnerScore ) {
				winner = f;
				winnerScore = currentScore;
			}
		}

		return winner;
	}


	/**
	 * Avalia o resultado do jogo
	 * @return retorna o ID do jogador vencedor
	 */
	private byte evaluateOutcome() {
		byte winner;
		int winnerScore;
		int currentScore;

		winner = 0;
		winnerScore = getPlayerById( ( byte ) winner ).getHand().scoreSequence();
		for ( byte f = 1; f < NUM_PLAYERS; ++f ) {

			if ( getPlayerById( f ).getMoney() < currentBet ) {
				getPlayerById( f ).fold();
			}


			if ( getPlayerById( f ).getLastAction() == PokerPlayer.ACTION_FOLDING ) {
				System.out.println( "jogador " + f + " deu fold!" );
				winner = whosNext( f );

				if ( turn < TURN_4_RIVER ) {
					turn = TURN_4_RIVER;
				}

				return winner;
			}


			currentScore = getPlayerById( f ).getHand().scoreSequence();

			if ( currentScore >= winnerScore ) {
				winner = f;
				winnerScore = currentScore;
			}
		}

		return winner;
	}


	/**
	 * 
	 * @return o numero de jogadores na mesa
	 */
	public byte getNumPlayers() {
		return ( byte ) player.length;
	}


	/**
	 *
	 * @return o numero do turno atual do jogo
	 */
	public byte getTurn() {
		return turn;
	}


	/**
	 *
	 * @param id da carta de jogo
	 * @return a carta na mesa desejada
	 */
	public PokerCard getPublicCard( byte id ) {
		return ( publicCards[id] != null ) ? publicCards[id].getCardData() : null;
	}


	/**
	 *
	 * @return o cacife atual
	 */
	public long getCurrentBet() {
		return currentBet;
	}


	/**
	 *
	 * @return o id do jogador vencedor do turno. PLAYER_NOPLAYER se não houver um vencedor ainda
	 */
	public byte getWinnerOfTheRound() {
		return winnerOfTheRound;
	}


	/**
	 * Força um vencedor da partida, caso o outro dê fold.
	 * @param winner é o ID do vencedor
	 */
	public void setWinner( byte winner ) {
		winnerOfTheRound = winner;
	}


	/**
	 * Organiza os chips do pot, de modo a usar o menor numero possível de chips.
	 */
	public void updatePotAndBet() {
		long[] chips = new long[ 5 ];
		int[] values = { 5, 10, 50, 100, 500 };
		long rest = pot;
		Chip chip;


		chipsOnTable.removeAllDrawables();
		System.gc();

		for ( int p = 0; p < NUM_PLAYERS; ++p ) {
			rest += getPlayerById( ( byte ) p ).getBet();
		}

		for ( int e = ( values.length - 1 ); e >= 0; --e ) {
			chips[e] += rest / values[e];
			rest = ( rest % values[e] );
		}

		try {
			for ( int c = 4; c >= 0; --c ) {
				for ( int d = 0; d < chips[c]; ++d ) {
					chip = new Chip( values[c] );
					chip.setPosition( d * chip.getWidth(), c * ( chip.getHeight() >> 1 ) );
					chipsOnTable.insertDrawable( chip );
				}
			}
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
	}


	public final void update( int delta ) {
		try {
			super.update( delta );

			if ( isBlink() ) {
				accBlinkTime += delta;

				if ( accBlinkTime > TIME_BLINK ) {
					accBlinkTime = 0;

					tableLit.setVisible( !tableLit.isVisible() );
					tableNormal.setVisible( !tableNormal.isVisible() );
				}

			}
		} catch ( Exception e ) {
			System.out.println( "this:" + this.toString() );
			System.out.println( "msg:" + e.getMessage() );
			e.printStackTrace();
		}
	}


	/**
	 * @return the blink
	 */
	public boolean isBlink() {
		return blink;
	}


	/**
	 * @param blink the blink to set
	 */
	public void setBlink( boolean blink ) {
		this.blink = blink;

		tableLit.setVisible( blink );
		tableNormal.setVisible( !blink );
	}


	/**
	 * Entre todos os chips ao vencedor
	 * @param position Posição do painel do jogador vencedor
	 */
	public void moveChipsTo( Point position ) {

		Point middle = position;

		for ( byte c = 0; c < chipsOnTable.getUsedSlots(); ++c ) {
			( ( Chip ) chipsOnTable.getDrawable( c ) ).setAnimation( middle, new Point( 0, 0 ) );
		}
	}


	public void forceWinner( byte winner ) {
		winnerOfTheRound = winner;
		turn = TURN_4_RIVER;
		endTurn();
	}
//</editor-fold>
}
