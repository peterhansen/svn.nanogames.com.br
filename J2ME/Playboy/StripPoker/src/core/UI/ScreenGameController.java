package core.UI;
//<editor-fold desc="Imports">
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Rectangle;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.MediaPlayer;
//#endif
//</editor-fold>


/**
 * Classe que implementa o joystick de tela do jogo de poker
 * @author Daniel "Monty" Monteiro
 */
public final class ScreenGameController extends UpdatableGroup implements Constants, KeyListener
//#if TOUCH == "true"
		, PointerListener
//#endif
{
	//<editor-fold desc="Constantes da classe">


	/**
	 * Numero máximo de caracteres possíveis no display de valor a ser apostado
	 */
	public static final byte MAX_NUM_CHARS = 4;
	/**
	 *
	 */
	public static final byte FRAME_NORMAL = 0;
	/**
	 * Botão de apostar pressionado
	 */
	public static final byte FRAME_UP = 1;
	/**
	 * Botão de aumentar a aposta pressionado
	 */
	public static final byte FRAME_RIGHT = 2;
	/*
	 * Botão de fold apertado
	 */
	public static final byte FRAME_DOWN = 3;
	/**
	 * Botão de diminuir a aposta apertado
	 */
	public static final byte FRAME_LEFT = 4;
	/**
	 * Botão piscando para indicar que a aposta foi aumentada
	 */
	public static final byte FRAME_BLINK = 5;
	//</editor-fold>
	//<editor-fold desc="Campos da classe">
	/**
	 * Botão para aumentar a aposta
	 */
	private Rectangle plusBtn;
	/**
	 * Botão para diminuir a aposta
	 */
	private Rectangle minusBtn;
	/**
	 * Botão para confirmar a aposta
	 */
	private Rectangle upBtn;
	/**
	 * Botão para desistir ( dar um fold )
	 */
	private Rectangle downBtn;
	/**
	 * A imagem do joystick em sí
	 */
	private Sprite bkg;
	/**
	 * O label que mostra o valor atual da aposta
	 */
	private Label label;
	/**
	 * Indica se tem que piscar
	 */
	private boolean blinking;
	/**
	 * Tempo passado desde a ultima troca de frame
	 */
	private int accTime;
	//</editor-fold>
	//<editor-fold desc="Métodos da classe">


	/**
	 * Construtor. Apenas instancia os objetos gráficos, sem posiciona-lo
	 * @throws Exception em caso de falha no carregamento do bitmap
	 */
	public ScreenGameController() throws Exception {
		super( 2 );

		bkg = new Sprite( PATH_IMAGES + "controles_tela" );
		insertDrawable( bkg );
		bkg.setSequence( 0 );
		bkg.pause( true );

		plusBtn = new Rectangle();

		minusBtn = new Rectangle();

		upBtn = new Rectangle();

		downBtn = new Rectangle();

		label = new Label( FONT_NUMBERS );
		insertDrawable( label );
		setCurrentBet( 0 );

		setSize( bkg.getCurrentFrame().width, bkg.getCurrentFrame().height );
	}


	/**
	 * Re-ajusta o layout do controle
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		label.setPosition( ( width - label.getWidth() ) >> 1, ( ( height - label.getHeight() ) >> 1 ) + ( BET_LABEL_Y_ADJUSTMENT ));

		plusBtn.set( ( 2 * width ) / 3, height / 3, width / 3, height / 3 );
		minusBtn.set( 0, height / 3, width / 3, height / 3 );
		upBtn.set( width / 3, 0, width / 3, height / 3 );
		downBtn.set( width / 3, ( 2 * height ) / 3, width / 3, height / 3 );
	}


	/**
	 * Define o valor de aposta que deve ser exibido, complementado de 0s a esquerda
	 * @param currentBet valor da aposta
	 */
	public void setCurrentBet( long currentBet ) {
		String text = String.valueOf( currentBet );
		String finalText = "";

		for ( int c = 0; c < MAX_NUM_CHARS - text.length(); c++ ) {
			finalText += '0';
		}

		label.setText( finalText + text );
	}


	/**
	 *
	 * @param key é uma tecla que foi pressionada
	 */
	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_DOWN:
				bkg.setFrame( FRAME_DOWN );
				break;

			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_FIRE:
				bkg.setFrame( FRAME_UP );
				break;

			case ScreenManager.KEY_LEFT:
				bkg.setFrame( FRAME_LEFT );
				break;

			case ScreenManager.KEY_RIGHT:
				bkg.setFrame( FRAME_RIGHT );
				break;
		}
	}


	/***
	 *
	 * @param blink define se o joystick deve piscar ou não
	 */
	public void setBlink( boolean blink ) {
		blinking = blink;
	}


	/**
	 *
	 * @param key indica a tecla que foi solta
	 */
	public void keyReleased( int key ) {
		if ( !blinking ) {
			bkg.setFrame( FRAME_NORMAL );
		}
	}


	/**
	 * Atualiza o estado interno da classe
	 * @param delta indica o tempo passado
	 */
	public void update( int delta ) {
		accTime += delta;

		if ( accTime >= TIME_BLINK ) {
			accTime = 0;

			if ( blinking ) {
				if ( bkg.getFrameSequenceIndex() == FRAME_BLINK ) {
					bkg.setFrame( FRAME_NORMAL );
				} else {
					bkg.setFrame( FRAME_BLINK );
				}
			}
		}

	}


//#if TOUCH == "true"
	/**
	 * Arraste na tela de toque - não utilizado
	 * @param x
	 * @param y
	 */
	public void onPointerDragged( int x, int y ) {
	}


	/**
	 * Tela de toque pressionda - traduz o evento em pressionamento de tecla
	 * @param x
	 * @param y
	 */
	public void onPointerPressed( int x, int y ) {

		x -= getPosX();
		y -= getPosY();

		if ( upBtn.contains( x, y ) ) {
			ScreenManager.getKeyListener().keyPressed( ScreenManager.KEY_UP );
		}
		if ( downBtn.contains( x, y ) ) {
			ScreenManager.getKeyListener().keyPressed( ScreenManager.KEY_DOWN );
		}
		if ( plusBtn.contains( x, y ) ) {
			ScreenManager.getKeyListener().keyPressed( ScreenManager.KEY_RIGHT );
		}
		if ( minusBtn.contains( x, y ) ) {
			ScreenManager.getKeyListener().keyPressed( ScreenManager.KEY_LEFT );
		}
	}


	/**
	 * Tela de toque liberada - traduz o evento em tecla solta
	 * @param x
	 * @param y
	 */
	public void onPointerReleased( int x, int y ) {
		ScreenManager.getKeyListener().keyReleased( 0 );
	}
//#endif
	//</editor-fold>
}
