//#if TOUCH == "true"
package core.UI;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class SlidingTabButton extends DrawableImage implements Updatable, Constants, PointerListener {

	public interface PauseListener {
		public void pause();
	}

	public final static int STATE_SHOWN   = 0;
	public final static int STATE_SHOWING = 1;
	public final static int STATE_HIDDING  = 2;
	public final static int STATE_HIDDEN  = 3;

	private int state;
	private int xHidden;
	private int xShown;
	private int timeMs;
	private int speed;
	private PauseListener listener = null;


	/**
	 *
	 * @param xHidden
	 * @param xShown
	 * @param timeMs
	 * @param y
	 * @throws Exception
	 */
	public SlidingTabButton( int xHidden, int xShown, int timeMs, int y, PauseListener listener ) throws Exception {
		super( PATH_IMAGES + "menu/pausebutton.png" );

		setSize( this.getImage().getWidth(), this.getImage().getHeight() );

		setClipTest( false );
		this.listener = listener;
		this.xHidden = xHidden;
		this.xShown = xShown;
		this.timeMs = timeMs;
		
		setPosition( xHidden - ( getWidth() >> 4 ), y - ( getHeight() >> 1 ) );
		speed = 0;
		state = STATE_HIDDEN;
	}


	/**
	 *
	 */
	public void show() {
		speed = ( ( xHidden - xShown ) / timeMs );
		state = STATE_SHOWING;
	}



	/**
	 *
	 */
	public void hide() {
		speed = ( ( xHidden - xShown ) / timeMs );
		state = STATE_HIDDING;
//		state = STATE_HIDDEN;
//		setPosition( xHidden - ( getWidth() >> 2 ), getPosY() );
	}


	/**
	 *
	 * @param delta
	 */
	public void update( int delta ) {

		switch ( state ) {
			case STATE_SHOWING:
				if ( speed != 0 ) {
					
					setPosition( getPosX() - speed, getPosY() );
					
					if ( getPosX() <= xShown - ( getWidth() >> 4 ) ) {
						speed = 0;
						state = STATE_SHOWN;
						setPosition( xShown - ( getWidth() >> 4 ), getPosY() );
					}
				}

				break;

			case STATE_HIDDING:

				if ( speed != 0 ) {
					setPosition( getPosX() + speed, getPosY() );
				}


				if ( getPosX() >= xHidden - ( getWidth() >> 4 ) ) {
					speed = 0;
					state = STATE_HIDDEN;
					setPosition( xHidden - ( getWidth() >> 4 ), getPosY() );
				}
				break;
		}

	}


	/**
	 *
	 * @return
	 */
	public boolean isHidden() {
		return state == STATE_HIDDEN;
	}


	/**
	 *
	 */
	public void performClick() {
		if ( state == STATE_SHOWN ) {
			hide();
			if ( listener != null )
				listener.pause();
		} else if ( state == STATE_HIDDEN ) {
			show();
		}
	}


	/**
	 *
	 * @param x
	 * @param y
	 */
	public void onPointerDragged( int x, int y ) {
		
	}


	/**
	 *
	 * @param x
	 * @param y
	 */
	public void onPointerPressed( int x, int y ) {
	}

	
	/**
	 *
	 * @param x
	 * @param y
	 */
	public void onPointerReleased( int x, int y ) {

		if ( contains( x, y ) )
			performClick();

	}

	public void setSize( int width, int height ) {
		super.setSize( width, height );

		setPosition( xHidden - ( getWidth() >> 4 ), ( ScreenManager.SCREEN_HALF_HEIGHT ) - ( getHeight() >> 1 ) );
		state = STATE_HIDDEN;
		speed = 0;
		xHidden = ScreenManager.SCREEN_WIDTH;
		xShown = ScreenManager.SCREEN_HALF_WIDTH;
	}
}
//#endif