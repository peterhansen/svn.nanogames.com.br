package core.UI;
//<editor-fold desc="Import">
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
//</editor-fold>


/**
 * Classe de chip, que representa o dinheiro na mesa
 * @author Daniel "Monty" Monteiro
 */
public class Chip extends UpdatableGroup implements Constants {


	private final static short ANIMATION_STOPPED_TIME = -0xFFF;
	private static final short TIME_SIZE_ANIMATION = 666; // demôin!
	//<editor-fold desc="Campos da classe">
	/**
	 *
	 */
	private int value;
	private Sprite chipSprite;
	private short accPosTime = -1;
	private short accSizeTime = -1;
	/***/
	private final BezierCurve tweenSize = new BezierCurve();
	private final BezierCurve tweenPosition = new BezierCurve();
	/***/
	private static final short TIME_POS_ANIMATION = 1100;

	//</editor-fold>
	//<editor-fold desc="Métodos da classe">

	/**
	 * Construtor padrão, que atribui ao chip o valor de 1
	 * @throws Exception caso o carregamento do sprite falhe
	 */
	public Chip() throws Exception {
		this( 1 );
	}


	/**
	 * Construtor que atribui um valor especifico ao chip
	 * @param value é o valor do chip
	 * @throws Exception caso o carregamento do sprite falhe
	 */
	public Chip( int value ) throws Exception {
		super( 1 );

		this.value = value;
		accPosTime = ANIMATION_STOPPED_TIME;
		accSizeTime = ANIMATION_STOPPED_TIME;

		chipSprite = new Sprite( PATH_IMAGES + "fichas" );
		chipSprite.setSequence( 0 );
		chipSprite.setFrame( getFrameForValue() );
		chipSprite.pause( true );
		insertDrawable( chipSprite );

		setSize( chipSprite.getCurrentFrame().width, chipSprite.getCurrentFrame().height );
	}


	/**
	 * Obtem o frame correspondente ao valor da ficha
	 * Valores possíveis são: 1000, 500, 100, 10 e 5
	 * Provavelmente deve ter alguma forma mais esperta de calcular isso
	 * mas esta forma não é ruim per se
	 * @return o valor da frame [ 0 .. 4 ]
	 */
	private final int getFrameForValue() {
		int frame = 0;

		switch ( value ) {
			case 500:
				frame = 4;
				break;
			case 100:
				frame = 3;
				break;
			case 50:
				frame = 2;
				break;
			case 10:
				frame = 1;
				break;
			case 5:
				frame = 0;
				break;
		}

		return frame;
	}


	public final void setAnimation( Point destination ) {
		setAnimation( destination, getSize() );
	}


	/**
	 *
	 * @param destination posição final da animação. Se for null, não muda a posição.
	 * @param finalSize tamanho final da animação. Se for null, não muda seu tamanho.
	 */
	public final void setAnimation( Point destination, Point finalSize ) {
		setAnimation( destination, finalSize, 0 );
	}


	public final void setAnimation( Point destination, Point finalSize, int startDelay ) {

		if ( startDelay == ANIMATION_STOPPED_TIME ) {
			startDelay++;
		}

		if ( destination != null ) {
			getTweenPosition().origin.set( getRefPixelPosition() );
			getTweenPosition().destiny.set( destination );
			getTweenPosition().control1.set( destination );
			getTweenPosition().control2.set( destination );
			accPosTime = ( short ) startDelay;
		}

		if ( finalSize != null ) {
			tweenSize.origin.set( size );
			tweenSize.destiny.set( finalSize );
			tweenSize.control1.set( size.x * 3 / 4, size.y * 3 / 4 );
			tweenSize.control2.set( finalSize.x * 15 / 10, finalSize.y * 15 / 10 );
			accSizeTime = ( short ) startDelay;
		}
	}


	public final void update( int delta ) {
		super.update( delta );

		if ( accSizeTime < TIME_SIZE_ANIMATION && accSizeTime != ANIMATION_STOPPED_TIME ) {
			accSizeTime += delta;
		}

		if ( accSizeTime >= 0 && accSizeTime != ANIMATION_STOPPED_TIME ) {

			final Point p = new Point();
			final Point previousRefPos = getRefPixelPosition();

			final int fp_progress = NanoMath.divInt( accSizeTime, TIME_SIZE_ANIMATION );
			tweenSize.getPointAtFixed( p, fp_progress );
			setSize( p );
			setRefPixelPosition( previousRefPos );

			if ( accSizeTime >= TIME_SIZE_ANIMATION ) {
				accSizeTime = ANIMATION_STOPPED_TIME;
			}
		}

		if ( accPosTime < TIME_POS_ANIMATION && accPosTime != ANIMATION_STOPPED_TIME ) {
			accPosTime += delta;
		}

		if ( accPosTime >= 0 && accPosTime != ANIMATION_STOPPED_TIME ) {

			final Point p = new Point();

			final int fp_progress = NanoMath.divInt( accPosTime, TIME_POS_ANIMATION );
			getTweenPosition().getPointAtFixed( p, fp_progress );
			setRefPixelPosition( p );

			if ( accPosTime >= TIME_POS_ANIMATION ) {
				accPosTime = ANIMATION_STOPPED_TIME;
			}
		}
	}


	/**
	 * @return the tweenPosition
	 */
	public final BezierCurve getTweenPosition() {
		return tweenPosition;
	}
	//</editor-fold>
}
