package core.UI;
//<editor-fold desc="Imports">
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
//</editor-fold>


/**
 *
 * @author Daniel "Monty" Monteiro e Caio
 */
public final class Background extends UpdatableGroup implements Constants, ScreenListener, Updatable {
//<editor-fold desc="Constantes da classe">


	/**
	 * Cor de fundo para quando o aparelho não tem memória suficiente para rodar a aplicação
	 */
	private final int BKG_COLOR = 0x430701;
//</editor-fold>
//<editor-fold desc="Inner classes">


	/**
	 * Pattern composto para criar o degradê do fundo
	 */
	private final class BackgroundElement extends DrawableGroup {


		/**
		 * Construtor que ja instancia elementos do pattern composto e ja define um
		 * tamanho fixo para cada unidade do pattern.
		 * @throws Exception
		 */
		public BackgroundElement() throws Exception {
			super( 2 );

			DrawableImage i1 = new DrawableImage( PATH_IMAGES + "bkg_pattern.png" );
			DrawableImage i2 = new DrawableImage( i1 );
			i2.setTransform( TRANS_MIRROR_V );

			insertDrawable( i1 );
			insertDrawable( i2 );

			i1.setPosition( 0, 0 );
			i2.setPosition( 0, i1.getHeight() );

			setSize( i1.getWidth(), i1.getHeight() << 1 );
		}
	}
//</editor-fold>
//<editor-fold desc="Campos da classe">
	/**
	 * O pattern de fundo em sí
	 */
	private final Pattern bkg;
	/**
	 * Sub-areas de superficie do background ( para quando há algum grande oclusor )
	 * @see usingMultipleViewports
	 */
	private final Rectangle[] viewports = new Rectangle[ 4 ];
	/**
	 * Indica se será usado o sistema de sub-areas de pattern
	 * @see viewports
	 */
	private boolean usingMultipleViewports = false;
//</editor-fold>
//<editor-fold desc="Métodos da classe">


	/**
	 * Construtor. Por padrão usa o pattern para a tela toda.
	 * Case a tela seja small, um padrão sólido será usado.
	 * @see BackgroundElement
	 * @throws Exception
	 */
	public Background() throws Exception {
		super( 1 );

		//#if REDUCED != "true"
 			if ( !GameMIDlet.isLowMemory() ) {
 				bkg = new Pattern( new BackgroundElement() );
 				insertDrawable( bkg );
 			} else
		//#endif
		{
			bkg = null;
		}

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	/**
	 * Re-ajusta o tamanho do fundo
	 * @param width
	 * @param height
	 */
	public final void setSize( int width, int height ) {
		width = NanoMath.max( width, 10 );
		height = NanoMath.max( height, 10 );
		super.setSize( width, height );

		//#if REDUCED != "true"
 		if ( bkg != null ) {
 			bkg.setSize( width, height );
 		}
		//#endif
	}


	/*
	 * Define o tamanho dos sub-patterns para o caso de um oclusor grande
	 */
	public void setViewportBlocker( Rectangle r ) {
		viewports[0] = new Rectangle( 0, 0, getWidth(), r.y );
		viewports[1] = new Rectangle( 0, r.y, r.x, r.height );
		viewports[2] = new Rectangle( 0, ( r.y + r.height ), getWidth(), ( getHeight() - ( r.y + r.height ) ) );
		viewports[3] = new Rectangle( ( r.x + r.width ), r.y, getWidth() - ( r.x + r.width ), r.height );
		usingMultipleViewports = true;
	}


	/**
	 * Define o tamanho  tamanho do pattern e retorna ao seu modo padrão de
	 * ocupar uma região inteira
	 * @param r
	 * @param b
	 */
	public void setViewport( Rectangle r, boolean b ) {
		usingMultipleViewports = false;
		super.setViewport( r );
	}


	/**
	 * Define o tamanho  tamanho do pattern e retorna ao seu modo padrão de
	 * ocupar uma região inteira
	 */
	public void setUnblockViewport() {
		setViewport( new Rectangle( getPosX(), getPosY(), getWidth(), getHeight() ), false );
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	/**
	 * Garante que o fundo sempre cobre a tela toda. Mesmo em casos de rotação
	 * do aparelho, etc.
	 * @param delta
	 */
	public void update( int delta ) {
		if ( getWidth() != ScreenManager.SCREEN_WIDTH || getHeight() != ScreenManager.SCREEN_HEIGHT ) {
			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}
	}


	/**
	 * Desenha o fundo, cobrindo 3 casos:
	 * 1 - pattern comum cobrindo a tela toda
	 * 2 - pattern liso, para aparelhos mais limitados
	 * 3 - sub-areas de visualização, no caso de haver algum grande oclusor.
	 * @see paint
	 * @param g Contexto gráfico
	 */
	public void draw( Graphics g ) {
		if ( bkg == null ) {
			g.setColor( BKG_COLOR );
			g.fillRect( translate.x, translate.y, getWidth(), getHeight() );
			super.draw( g );
		} else {
			if ( usingMultipleViewports && viewports != null ) {
				for ( int i = 0; i < viewports.length; i++ ) {
					setViewport( viewports[i] );
					super.draw( g );
				}
			} else {
				super.draw( g );
			}
		}
	}


	/**
	 * Pinta o fundo, cobrindo 3 casos:
	 * 1 - pattern comum cobrindo a tela toda
	 * 2 - pattern liso, para aparelhos mais limitados
	 * 3 - sub-areas de visualização, no caso de haver algum grande oclusor.
	 * @see draw
	 * @param g Contexto gráfico
	 */
	public void paint( Graphics g ) {
		//#if REDUCED != "true"
 			if ( bkg != null ) {
 
 				if ( usingMultipleViewports && viewports != null ) {
 					for ( int i = 0; i < viewports.length; i++ ) {
 						setViewport( viewports[i] );
 						super.paint( g );
 					}
 				} else {
 					super.paint( g );
 				}
 
 			} else
		//#endif
		{
			super.paint( g );
		}
	}


	/**
	 * Re-ajusta o tamanho do fundo.
	 * @param width
	 * @param height
	 */
	public void sizeChanged( int width, int height ) {
		setSize( width, height );
		bkg.setSize( width, height );
	}
//</editor-fold>
}
