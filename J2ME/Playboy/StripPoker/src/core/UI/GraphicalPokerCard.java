package core.UI;
//<editor-fold desc="Imports">
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import core.PokerCard;
import javax.microedition.lcdui.Graphics;
import util.ComparableObject;
//</editor-fold>


/**
 * Representação gráfica da classe de carta
 * @author Daniel "Monty" Monteiro
 */
public class GraphicalPokerCard extends Drawable implements Updatable, Constants, SpriteListener, ComparableObject {
//<editor-fold desc="Campos da classe">


	/**
	 * label da carta. usado no desenho
	 */
	private Label cardLabel;
	/**
	 *
	 */
	private PokerCard cardData;
	/**
	 *
	 */
	private Sprite cardFlipAnimation;
//</editor-fold>
//<editor-fold desc="Metodos da classe">


	/**
	 * Construtor, montando a sprite e preparando o estado para a carta virada
	 * @param suit Naipe
	 * @param rank Valor
	 * @throws Exception Se der algum erro no carregamento
	 */
	public GraphicalPokerCard( byte suit, byte rank, Sprite animation ) throws Exception {
		super();

		cardData = new PokerCard( suit, rank );
		cardFlipAnimation = new Sprite( animation );
		cardFlipAnimation.pause( true );
		cardFlipAnimation.setSequence( 0 );
		cardFlipAnimation.setFrame( 0 );
		cardFlipAnimation.setSize( CARD_WIDTH, CARD_HEIGHT );
		cardLabel = new Label( 1 );

		setSize( CARD_WIDTH, CARD_HEIGHT );
		setClipTest( false );
	}


	GraphicalPokerCard( PokerCard pokerCard, Sprite anim ) throws Exception {
		this( pokerCard.getSuit(), pokerCard.getRank(), anim );
	}


	/**
	 * Desenha a carta usando as funções de canvas.
	 * @param g contexto gráfico
	 */
	protected void paint( Graphics g ) {
		byte rank = cardData.getRank();
		byte suit = cardData.getSuit();
		boolean flipped = cardData.isFlipped();

		//HACK porque a fonte esta com um pequeno erro na tira e demoraria demais
		//pra pedir pra alguem corrigir
		if ( rank == PokerCard.RANK_A ) {
			rank = -1;
		}

		//posição adequada de desenho
		int x = translate.x;
		int y = translate.y;

		if ( flipped && cardFlipAnimation.isPaused() ) {
			//contorno da carta
			g.setColor( flipped ? WHITE : RED );
			g.fillRoundRect( x + CARD_INNER_BORDER, y + CARD_INNER_BORDER, CARD_WIDTH - ( 2 * CARD_INNER_BORDER ),
							 CARD_HEIGHT - ( 2 * CARD_INNER_BORDER ), CARD_BORDER_WIDTH, CARD_BORDER_HEIGHT );
			//texto da carta
			int offset;
			cardLabel.setFont( ( suit % 2 ) + 1 );
			cardLabel.setText( Integer.toHexString( rank + 1 ) );
			offset = ( CARD_WIDTH - cardLabel.getTextWidth() ) >> 1;
			cardLabel.drawString( g, Integer.toHexString( rank + 1 ), x + offset, y + ( CARD_BORDER_HEIGHT >> 1 ) );

			cardLabel.setFont( FONT_SUITS );
			cardLabel.setText( String.valueOf( suit ) );
			offset = ( CARD_WIDTH - cardLabel.getTextWidth() ) >> 1;
			cardLabel.drawString( g, String.valueOf( suit ), x + offset,
								  y + ( CARD_HEIGHT >> 1 ) - ( cardLabel.getHeight() >> 2 ) + ( CARD_BORDER_HEIGHT >> 2 ) );
		} else {
			cardFlipAnimation.draw( g );
		}
	}


	/**
	 * Atualiza o estado da carta
	 * @param delta é o tempo decorrido desde o ultimo update
	 */
	public void update( int delta ) {

		if ( !cardFlipAnimation.isPaused() ) {
			cardFlipAnimation.update( delta );
		}
	}


	/**
	 * Vira a carta. O estado interno da carta só é atualizado no fim da animação
	 */
	public void flip() {
		cardFlipAnimation.setSequence( cardData.isFlipped() ? 2 : 1 );
		cardFlipAnimation.pause( false );
		cardFlipAnimation.setListener( this, cardData.isFlipped() ? 2 : 1 );
	}


	/**
	 * Define quem é o dono da carta
	 * @param id do jogador
	 */
	public void setPlayer( byte id ) {
		cardData.setPlayer( id );
	}


	/**
	 * 
	 * @return o id do dono da carta
	 */
	public byte getPlayer() {
		return cardData.getPlayer();
	}


	/**
	 * Força um estado da carta
	 * @param flipping é se a carta esta com o valor à mostra ( true )
	 */
	public void setFlipped( boolean flipping ) {
		if ( flipping != cardData.isFlipped() ) {
			flip();
		}
	}


	/**
	 * 
	 * @return o valor da carta
	 */
	public int getRank() {
		return cardData.getRank();
	}


	/**
	 * 
	 * @return o naipe da carta
	 */
	public int getSuit() {
		return cardData.getSuit();
	}


	/**
	 * Callback do fim da animação
	 * @param id da animação
	 * @param sequence qual é a sequencia finalizada
	 */
	public void onSequenceEnded( int id, int sequence ) {
		cardFlipAnimation.pause( true );
		cardFlipAnimation.setSequence( 0 );
		cardFlipAnimation.setFrame( 0 );
		cardData.flip();
	}


	/**
	 * Callback de frame tickada
	 * @param id da animação
	 * @param sequence qual é a sequencia finalizada
	 */
	public void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	/**
	 * Dados brutos de estado da carta
	 * @return A carta bruta ( somente os dados dela )
	 */
	public PokerCard getCardData() {
		return cardData;
	}


	/**
	 * Compara a carta com outra carga
	 * @param pc A outra carta
	 * @return SortingAlgorithm.BIGGER, SortingAlgorithm.SMALLER ou SortingAlgorithm.SIMILAR
	 */
	public byte compareWith( GraphicalPokerCard pc ) {
		return cardData.compareWith( pc.getCardData() );
	}

	/**
	 * Comparação entre objetos comparaveis genéricos. Se o outro objeto for uma
	 * carta, faz a comparação mais apropriado.
	 * @param other é o outro objeto
	 * @return SortingAlgorithm.BIGGER, SortingAlgorithm.SMALLER ou SortingAlgorithm.SIMILAR
	 */
	public byte compareWith( ComparableObject other ) {
		if ( other instanceof GraphicalPokerCard ) {
			return compareWith( ( GraphicalPokerCard ) other );
		} else {
			// TODO
            return 0;
		}
	}
//</editor-fold>
}
