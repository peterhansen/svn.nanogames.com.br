package core.UI;
//<editor-fold desc="Imports">
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MUV;
//</editor-fold>


/**
 * Caixa de texto com movimentos de scroll vertical, para mostrar hora o começo,
 * hora o final do texto
 * Adaptado da classe core.Justus do jogo Topa ou não topa. A classe original é
 * é de autoria de Peter e Caio.
 * @author Daniel "Monty" Monteiro
 */
public class MessagePainel extends UpdatableGroup implements Constants {
	//<editor-fold desc="Constantes da classe">
	//<editor-fold desc="Estados de scrolling">


	/**
	 * Estado de scrolling: não há texto
	 */
	private static final byte SCROLL_STATE_NONE = 0;
	/**
	 * Estado de scrolling: rolando para cima
	 */
	private static final byte SCROLL_STATE_SCROLLING_UP = 1;
	/**
	 * Estado de scrolling: rolando para baixo
	 */
	private static final byte SCROLL_STATE_SCROLLING_DOWN = 2;
	/**
	 * Estado de scrolling: aguardando para o próximo movimento
	 */
	private static final byte SCROLL_STATE_WAITING = 3;
	//</editor-fold>
	/**
	 * tempo entre a impressão de um caractere e outro
	 */
	private static final byte TIME_PER_CHAR = 33;
	//</editor-fold>
	//<editor-fold desc="Campos da classe">
	/**
	 * pattern de meio
	 */
	private Pattern painelMiddle;
	/**
	 * cantoneira direita
	 */
	private DrawableImage painelRight;
	/**
	 * cantoneira esquerda
	 */
	private DrawableImage painelLeft;
	/**
	 * mensagens no painel
	 */
	private RichLabel label;
	/**
	 * Indica quanto tempo se passou entre cada troca de ação
	 */
	protected int accTime;
	/**
	 * Altura total do texto
	 */
	private int textTotalHeight;
	/**
	 * Guarda a representação visual da caixa de texto
	 * ( é a raiz da display list )
	 */
	private final DrawableGroup textBox;
	/**
	 * A moldura onde acontece o clipping do texto scrollado
	 */
	private final UpdatableGroup labelGroup;
	/**
	 * Limite inferior, onde acontece o corte do fundo da caixa de texto
	 */
	private short textLimitBottom;
	/**
	 * Limite superior, onde acontece o corte do topo da caixa de texto
	 */
	private short textLimitTop;
	/**
	 * Tempo sendo aguardado entre a impressão de um caractere e outro
	 */
	private int textAccTime;
	/**
	 * Mensagem que esta aguardando para ser exibida
	 */
	private String messageToShow;
	/**
	 * Quanto tempo o scrolling fica parado antes de começar um novo movimento
	 */
	//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
	//# 		private static final short SCROLL_WAIT_TIME = 1300;
	//#else
	private static final short SCROLL_WAIT_TIME = 1350;
	//#endif
	/**
	 * estado de scrolling
	 */
	private byte scrollState;
	/**
	 * controladora de timming de scroll
	 */
	private short scrollAccTime;
	/**
	 * Controle de movimento de scroll
	 */
	private final MUV scrollSpeed;
	/**
	 * Indica se há texto para aparecer ou não
	 */
	private boolean appearingMessage;
	private String pastBuffer;
	//</editor-fold>
	//<editor-fold desc="Métodos da classe">


	/**
	 * Construtor: Apenas instancia e monta a lista de exibição, mas ainda não posicioan nada.
	 * Depende de setSize para montar o layout
	 * @throws Exception caso alguma imagem falhe no carregamento
	 */
	public MessagePainel() throws Exception {
		super( 10 );

		textLimitBottom = 0;
		appearingMessage = false;
		textAccTime = 0;
		pastBuffer = "";

		//<editor-fold desc="Aparencia da caixa">
		textBox = new DrawableGroup( 5 );
		insertDrawable( textBox );

		//<editor-fold desc="A moldura da caixa de texto">
		painelMiddle = new Pattern( new DrawableImage( PATH_IMAGES + "monitor_barra_pattern.png" ) );
		painelRight = new DrawableImage( PATH_IMAGES + "monitor_canto_pattern.png" );
		painelRight.setTransform( TRANS_MIRROR_H );
		painelLeft = new DrawableImage( PATH_IMAGES + "monitor_canto_pattern.png" );
		textBox.insertDrawable( painelLeft );
		textBox.insertDrawable( painelMiddle );
		textBox.insertDrawable( painelRight );
		//</editor-fold>


		//<editor-fold desc="O label que contém o texto">
		labelGroup = new UpdatableGroup( 1 );
		textBox.insertDrawable( labelGroup );
		label = new RichLabel( FONT_TEXT );
		scrollSpeed = new MUV( ( label.getFont().getHeight() * 3 ) / 4 );
		labelGroup.insertDrawable( label );
		//</editor-fold>
		//</editor-fold>
	}


	/**
	 * Re-ajusta o layout da caixa de texto de acordo com o tamanho desejado
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		height = painelMiddle.getHeight();
		
		super.setSize( width, height );

		textBox.setSize( width, height );
		painelMiddle.setSize( width - ( painelLeft.getWidth() << 1 ), painelLeft.getHeight() );
		painelMiddle.setPosition( painelLeft.getWidth(), 0 );
		painelRight.setPosition( painelMiddle.getPosX() + painelMiddle.getWidth(), 0 );
		labelGroup.setPosition( painelMiddle.getPosition() );
		labelGroup.setPosition( painelMiddle.getPosX(), painelMiddle.getPosY() + painelLeft.getWidth() );
		labelGroup.setSize( painelMiddle.getWidth(), painelMiddle.getHeight() - 2 * painelLeft.getWidth() );
		label.setSize( labelGroup.getWidth(), 10000 );
		refresh();
	}


	/**
	 * Atualiza o estado interno da caixa de texto, executando o movimento de
	 * scrolling, se for o caso, ou mesmo inserindo caracteres bufferizados
	 * @param delta tempo passado desde o ultimo update
	 */
	public final void update( int delta ) {
		super.update( delta );
		//<editor-fold desc="Movimento de scrolling">
		switch ( scrollState ) {
			case SCROLL_STATE_SCROLLING_UP:
				label.move( 0, -scrollSpeed.updateInt( delta ) );
				if ( label.getPosY() <= -( textLimitBottom - labelGroup.getHeight() ) ) {
					label.setPosition( label.getPosX(), -( textLimitBottom - labelGroup.getHeight() ) );
					scrollState = SCROLL_STATE_WAITING;
				}
				break;

			case SCROLL_STATE_SCROLLING_DOWN:
				label.move( 0, scrollSpeed.updateInt( delta ) );
				if ( label.getPosY() >= textLimitTop ) {
					label.setPosition( label.getPosX(), textLimitTop );
					scrollState = SCROLL_STATE_WAITING;
				}
				break;

			case SCROLL_STATE_WAITING:
				scrollAccTime += delta;
				if ( scrollAccTime >= SCROLL_WAIT_TIME ) {
					scrollAccTime = 0;

					if ( label.getTextTotalHeight() > labelGroup.getHeight() ) {
						scrollState = ( label.getPosY() == textLimitTop ? SCROLL_STATE_SCROLLING_UP : SCROLL_STATE_SCROLLING_DOWN );
					}
				}
				break;
		}
		//</editor-fold>
		//<editor-fold desc="Impressão de novos caracteres">
		if ( appearingMessage && messageToShow != null ) {
			textAccTime += delta;
			if ( textAccTime / TIME_PER_CHAR >= messageToShow.length() ) {
				appearingMessage = false;
				pastBuffer /*+= " \n" +*/ = messageToShow;
				setLabelText( pastBuffer );
			} else {
				setLabelText( /*pastBuffer + "\n" +*/ messageToShow.substring( 0, textAccTime / TIME_PER_CHAR ) );
			}
		}
		//</editor-fold>
	}


	/**
	 *	Atualiza o buffer de texto
	 */
	private final void refresh() {
		appearingMessage = true;
		textAccTime = 0;
		label.setPosition( label.getPosX(), textLimitTop = 0 );
		
		if ( textLimitTop == textLimitBottom ) {
			scrollState = SCROLL_STATE_WAITING;
		} else {
			scrollState = SCROLL_STATE_SCROLLING_DOWN;
		}
	}


	/**
	 * Define o texto a ser exibido
	 * @param text é o texto em questão
	 */
	private final void setLabelText( String text ) {
		label.setText( text, true, false );
		textTotalHeight = label.getTextTotalHeight();
		textLimitTop = 0;//( short ) ( textBox.getHeight() - textTotalHeight - textLimitTop );
		textLimitBottom = ( short ) ( textLimitTop + textTotalHeight );
	}


	/**
	 * Adiciona uma mensagem no painel
	 * @param string a mensagem em sí
	 */
	public void addMessage( String string ) {
		messageToShow = string;
		refresh();
	}


	/**
	 *
	 * @return o texto ja impresso e o texto sendo impresso
	 */
	public String getText() {
		return pastBuffer + messageToShow;
	}


	public void clear() {
		pastBuffer = "";
		messageToShow = "";
		label.setPosition( label.getPosX(), textLimitTop = 0 );
		refresh();
	}
	//</editor-fold>
}
