package core;
//<editor-fold desc="imports">
import core.UI.Constants;
import util.ComparableObject;
import util.SortingAlgorithm;
//</editor-fold>


/**
 * Classe representando a carta de poker
 * @author Daniel "Monty" Monteiro
 */
public class PokerCard implements Constants, ComparableObject {
//<editor-fold desc="constantes">

	//<editor-fold desc="naipes">

	/**
	 * naipe: espadas
	 */
	public static final byte SUIT_SPADE = 0;
	/**
	 * naipe: copas
	 */
	public static final byte SUIT_HEART = 1;
	/**
	 * naipe: paus
	 */
	public static final byte SUIT_CLUB = 2;
	/**
	 * naipe: ouros
	 */
	public static final byte SUIT_DIAMOND = 3;
	//</editor-fold>
	//<editor-fold desc="cartas">
	///lembrando que sempre vai ser incrementado por 1
	public static final byte RANK_2 = 1;
	public static final byte RANK_3 = 2;
	public static final byte RANK_4 = 3;
	public static final byte RANK_5 = 4;
	public static final byte RANK_6 = 5;
	public static final byte RANK_7 = 6;
	public static final byte RANK_8 = 7;
	public static final byte RANK_9 = 8;
	public static final byte RANK_10 = 9;
	public static final byte RANK_J = 10;
	public static final byte RANK_Q = 11;
	public static final byte RANK_K = 12;
	public static final byte RANK_A = 13;
	//</editor-fold>
//</editor-fold>
//<editor-fold desc="campos da classe">
	/**
	 * Naipe da carta
	 */
	private byte suit;
	/**
	 * Nome da carta (2345678910JQK)
	 */
	private byte rank;
	/**
	 * Indica se a carta esta virada ( true = naipe e nome visiveis )
	 */
	private boolean flipped;
	/**
	 * ID do jogador que tem a carta. -1 caso seja uma carta da mesa.
	 */
	private byte player;
//</editor-fold>

//<editor-fold desc="metodos">

	/**
	 * Inicializa a carta ja definindo um tamanho, desligando o clip-test e deixando ela
	 * desvirada ( naipe e nome não visiveis )
	 * @param suit Naipe da carta ( paus, ouros, copas, espadas)
	 * @param rank Nome da carta ( 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K, A )
	 */
	public PokerCard( byte suit, byte rank ) {
		flipped = false;
		player = PLAYER_NO_PLAYER;
		this.suit = suit;
		this.rank = rank;
	}


	/**
	 *
	 * @return O nome da carta
	 */
	public final byte getRank() {
		return rank;
	}


	/**
	 *
	 * @return O naipe da carta
	 */
	public final byte getSuit() {
		return suit;
	}


	/**
	 *
	 * @param suit será definido como novo naipe da carta
	 */
	public final void setSuit( byte suit ) {
		this.suit = suit;
	}


	/**
	 *
	 * @param rank será definido como novo nome da carta
	 */
	public final void setRank( byte rank ) {
		this.rank = rank;
	}


	/**
	 * Vira a carta
	 */
	public final void flip() {
		flipped = !flipped;
	}


	/**
	 *
	 * @return indica se a carta esta virada ou não ( true = naipe e nome visiveis )
	 */
	public final boolean isFlipped() {
		return flipped;
	}


	/**
	 *
	 * @param flipped define se a carta esta ou não virada ( true = naipe e nome visiveis )
	 */
	public final void setFlipped( boolean flipped ) {
		this.flipped = flipped;
	}


	/**
	 *
	 * @param id define o jogador que possui a carta
	 */
	public final void setPlayer( byte id ) {
		this.player = id;
	}


	/**
	 *
	 * @return o jogador que possui a carta
	 */
	public final byte getPlayer() {
		return player;
	}


	/**
	 * Compara a carta com outra carga
	 * @param pc A outra carta
	 * @return SortingAlgorithm.BIGGER, SortingAlgorithm.SMALLER ou SortingAlgorithm.SIMILAR
	 */
	public byte compareWith( PokerCard pc ) {
		byte outcome;

		if ( pc.getRank() > getRank() ) {
			outcome = SortingAlgorithm.BIGGER;
		} else if ( pc.getRank() < getRank() ) {
			outcome = SortingAlgorithm.SMALLER;
		} else {
			outcome = SortingAlgorithm.SIMILAR;
		}

		return outcome;
	}


	//#if DEBUG == "true"
	/**
	 *
	 * @return informações da carta no formato "( indice naipe, indice valor )"
	 */
	public String toString() {
		String toReturn = "";
		String[] suits = {"SP", "HT", "CB", "DM"};

		toReturn += "(";

		toReturn += suits[ getSuit() ];
		
		toReturn += ",";

		toReturn += getPrettyRank();

		toReturn += ")";

		return toReturn;
	}
	//#endif


	/**
	 * Comparação entre objetos comparaveis genéricos. Se o outro objeto for uma
	 * carta, faz a comparação mais apropriado.
	 * @param other é o outro objeto
	 * @return SortingAlgorithm.BIGGER, SortingAlgorithm.SMALLER ou SortingAlgorithm.SIMILAR
	 */
	public byte compareWith( ComparableObject other ) {
		if ( other instanceof PokerCard ) {
			return compareWith( ( PokerCard ) other );
		} else {
			//#if SCREEN_SIZE == "BIG"
                return 0; // TODO
			//#else
//# 				return 0xF;
			//#endif
		}
	}


	String getPrettyRank() {
		String toReturn = "";
		
		switch( getRank() ) {
			case RANK_2:
			case RANK_3:
			case RANK_4:
			case RANK_5:
			case RANK_6:
			case RANK_7:
			case RANK_8:
			case RANK_9:
			case RANK_10:
				toReturn += getRank() + 1;
			break;
			case RANK_J:
				toReturn += "Valete";
			break;
			case RANK_K:
				toReturn += "Rei";
			break;
			case RANK_Q:
				toReturn += "Dama";
			break;
			case RANK_A:
				toReturn += "As";
			break;
		}
		
		return toReturn;
	}
//</editor-fold>
}
