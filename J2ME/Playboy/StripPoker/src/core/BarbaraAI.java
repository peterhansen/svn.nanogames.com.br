package core;
//<editor-fold desc="Imports">
import br.com.nanogames.components.util.NanoMath;
import core.UI.Constants;
import core.UI.GameTable;
import screens.GameMIDlet;
//</editor-fold>


/**
 * Classe de IA para jogo de poker
 * @author Daniel "Monty" Monteiro
 */
public class BarbaraAI implements Constants {
	//<editor-fold desc="Constantes da classe">


	/**
	 * Melhor mão com duas cartas
	 */
	private static final int HAND_2 = 4026368;
	/**
	 * Melhor mão com 5 cartas
	 */
	private static final int HAND_5 = 52195328;
	/**
	 * Melhor mão com 6 cartas
	 */
	private static final int HAND_6 = 52195328;
	/**
	 * Melhor mão com 7 cartas
	 */
	private static final int HAND_7 = 52195328;
	//</editor-fold>
	//<editor-fold desc="Campos da classe">
	/**
	 * A mesa de jogo ( o contexto de jogo )
	 */
	private GameTable gameTable;
	/**
	 * O jogador que será controlado pela IA
	 */
	private PokerPlayer player;
	//</editor-fold>
	//<editor-fold desc="Métodos da classe">


	/**
	 * Inicializa a IA do jogo
	 * @param gameTable é o Contexto de jogo
	 * @param playerId é o ID do jogador pelo qual a IA vai se manifestar
	 */
	public BarbaraAI( GameTable gameTable, byte playerId ) {
		this.gameTable = gameTable;
		this.player = gameTable.getPlayerById( playerId );
	}


	/**
	 * Atualiza o estado interno, tomando decisões
	 */
	public void update() {
		int myHandScore = 0;
		int bestPossibleHand = 0xFFFF;
		int percentHand = 0;
		int percentMoney = 0;
		int percentGame = 0;

		percentMoney = ( int ) ( ( 100 * player.getMoney() ) / ( 2 * INITIAL_CASH_PER_PLAYER ) );
		percentGame = ( 100 * gameTable.getTurn() ) / ( GameTable.TURN_7 );

		//Calcula o valor da minha mão
		player.getHand().backup();

		for ( byte c = 0; c < PUBLIC_CARDS_ON_TABLE; ++c ) {
			if ( gameTable.getPublicCard( c ) != null && gameTable.getPublicCard( c ).isFlipped() ) {
				player.getHand().addCard( gameTable.getPublicCard( c ) );
			}
		}
		myHandScore = player.getHand().scoreSequence();
		//Calcula o valor da melhor mão possível

		switch ( player.getHand().cardsInHand() ) {
			case 2:
				bestPossibleHand = HAND_2;
				break;
			case 5:
				bestPossibleHand = HAND_5;
				break;
			case 6:
				bestPossibleHand = HAND_6;
				break;
			case 7:
				bestPossibleHand = HAND_7;
				break;
		}
		player.getHand().restore();

		//Calcula o quão boa é a mão em relação a melhor mão possível
		percentHand = ( myHandScore * 100 ) / ( bestPossibleHand );

		//#if DEBUG == "true"
		try {
			System.out.println( "---------------------------------" );
			System.out.println( "tenho " + percentMoney + "% do dinheiro em jogo" );
			System.out.println( percentGame + "% do jogo completo" );
			System.out.println( "minha mão é " + percentHand + "% boa" );
			System.out.println( "---------------------------------" );
		} catch ( Throwable e ) {
			System.out.println( "msg:" + e.getMessage() );
			e.printStackTrace();
		}
		//#endif

		String say = getSayForTurnAndCondition( gameTable.getTurn(), percentMoney, percentHand );

		if ( percentHand < 5 && percentHand >= 0 && percentGame > 80 ) {
			say += ( GameMIDlet.getText( TEXT_BARBARA_FOLD ) + player.getHand().toString() );
			player.fold();
		} else if ( percentHand < 60 && percentHand >= 5 ) {
			// entre 30% e 60%
			// CALL
			say += GameMIDlet.getText( TEXT_BARBARA_CALL );
			player.setDesiredBet( gameTable.getCurrentBet() );
		} else if ( percentHand < 95 && percentHand >= 60 ) {
			// entre 60% e 90%
			// RAISE
			long raise = ( ( player.getMoney()  * ( percentHand - 55 ) ) / 100 );
			if ( gameTable.getTurn() < GameTable.TURN_6_POST_SHOWDOWN ) {
				say += ( GameMIDlet.getText( TEXT_BARBARA_RAISE_1 ) + raise + "." );
			} else {
				say += ( GameMIDlet.getText( TEXT_BARBARA_RAISE_2 ) );
			}
			player.setDesiredBet( raise + player.getBet() );
		} else if ( percentHand <= 100 && percentHand >= 95 && gameTable.getTurn() > GameTable.TURN_4_RIVER ) {
			// acima de 90%

			if ( gameTable.getTurn() >= GameTable.TURN_4_RIVER && percentGame > 50 ) {
				say +=  GameMIDlet.getText( TEXT_BARBARA_ALL_IN );
				player.allIn();
			} else {
				player.setDesiredBet( gameTable.getCurrentBet() );
			}



			if ( ( player.getMoney() - player.getBet() ) <= 0 ) {
				player.fold();
				return;
			}

		}
		player.setSay( say );
		
	}

	public static String getSayForTurnAndCondition( int turn, int percentMoney, int percentHand ) {
		String say = "...";
		int random5 = NanoMath.randInt( 4 );
		switch ( turn ) {
			case GameTable.TURN_1_PREFLOP:
				if ( percentMoney > 30 ) {

					switch ( random5 ) {
						case 4:
						case 0:
							say = (  GameMIDlet.getText( TEXT_BARBARA_PREFLOP_GOOD_1 ) );
							break;
						case 1:
							say = ( GameMIDlet.getText( TEXT_BARBARA_PREFLOP_GOOD_2 ) );
							break;
						case 3:
						case 2:
							say = ( GameMIDlet.getText( TEXT_BARBARA_PREFLOP_GOOD_3 ) );
							break;
					}
				} else {
					say = GameMIDlet.getText( TEXT_BARBARA_PREFLOP_BAD );
				}
				break;

			case GameTable.TURN_2_FLOP:
				if ( percentMoney > 30 ) {

					switch ( random5 ) {
						case 4:
						case 0:
							say = GameMIDlet.getText( TEXT_BARBARA_FLOP_GOOD_1 );
							break;
						case 1:
							//blefe!
							say = GameMIDlet.getText( TEXT_BARBARA_FLOP_GOOD_2 );
							break;
						case 3:
						case 2:
							say = GameMIDlet.getText( TEXT_BARBARA_FLOP_GOOD_3 );
							break;
					}
				} else {
					say = GameMIDlet.getText( TEXT_BARBARA_FLOP_BAD );
				}
				break;
			case GameTable.TURN_3_TURN:
				if ( percentMoney > 30 ) {

					switch ( random5 ) {
						case 4:
						case 0:
							say = GameMIDlet.getText( TEXT_BARBARA_TURN_GOOD_1 );
							break;
						case 1:
							say = GameMIDlet.getText( TEXT_BARBARA_TURN_GOOD_2 );
							break;
						case 3:
						case 2:
							say = GameMIDlet.getText( TEXT_BARBARA_TURN_GOOD_3 );
							break;
					}
				} else {
					say = GameMIDlet.getText( TEXT_BARBARA_TURN_BAD );
				}

				break;
			case GameTable.TURN_4_RIVER:
				if ( percentMoney > 30 ) {

					switch ( random5 ) {
						case 4:
						case 0:
							say = GameMIDlet.getText( TEXT_BARBARA_RIVER_GOOD_1 );
							break;
						case 1:
							say = GameMIDlet.getText( TEXT_BARBARA_RIVER_GOOD_2 );
							break;
						case 3:
						case 2:
							say = GameMIDlet.getText( TEXT_BARBARA_RIVER_GOOD_3 );
							break;
					}
				} else {
					say = GameMIDlet.getText( TEXT_BARBARA_RIVER_BAD );
				}
				break;

			default:
				say = GameMIDlet.getText( TEXT_BARBARA_SPEECHLESS );
		}

		return say + " ";
	}
	//</editor-fold>
}
