package core;
//<editor-fold desc="Import">
import br.com.nanogames.components.util.NanoMath;
import core.UI.Constants;
import java.util.Vector;
import util.HeapSort;
//</editor-fold>


/**
 *
 * @author Daniel Monteiro
 */
public class PokerHand implements Constants {


//<editor-fold desc="Inner classes">
	//<editor-fold desc="classe SequenceNodes">
	/**
	 * nó representando parte de uma mão candidata
	 */
	private class SequenceNode {


		/**
		 * quantas cartas ainda podem ser inseridas como filhas
		 */
		public int leftCards;
		/**
		 * mãos filhas, derivadas.
		 */
		public Vector next;
		/**
		 * score do meu nível da mão parcial
		 */
		public int partialScore;
		/**
		 * quantas cartas a mão usou
		 */
		public int usedCards;
		/**
		 * pontuação final da mão (contando com os filhos)
		 */
		public int finalScore;
		/**
		 * avaliação local da mão
		 */
		public int sequence;
		/**
		 * avaliação final da mão
		 */
		public int accSequence;
		/**
		 * nivel do nó (geração)
		 */
		public int level;
		/**
		 * referencias para as cartas
		 */
		public Vector pokerCards;
		/**
		 * indices para as cartas no vetor original
		 */
		public Vector cardIndexes;
		public String description;


		/**
		 *
		 * @param handSize
		 */
		public SequenceNode( int handSize ) {
			next = new Vector();
			pokerCards = new Vector();
			cardIndexes = new Vector();
			leftCards = handSize;
			partialScore = 0;
			accSequence = 0;
			level = 0;
			description = "";
			//#if DEBUG == "true"
			whoAmI = 0;
			parent = null;
			//#endif
		}
//<editor-fold desc="DEBUG">
		//#if DEBUG == "true"
		public int whoAmI;
		public String content;
		public SequenceNode parent;
//----------------------------------------------------------------------------


		public String toString() {
			String tmp = " ";
			content = " ";
			for ( byte c = 0; c < getTotalNextNodes(); ++c ) {
				tmp += "-> c = " + c;
				tmp += getNext( c ).toString();
			}

			String indexes = " ";

			for ( byte d = 0; d < cardIndexes.size(); ++d ) {
				indexes += ( ( Integer ) cardIndexes.elementAt( d ) ).intValue() + " ";
				content += ( ( PokerCard ) pokerCards.elementAt( d ) ).getPrettyRank() + " ";
			}

			tmp += "( score = " + finalScore + ", left cards = " + this.leftCards + ", whoAmI = " + this.whoAmI + ", level = " + this.level + ", content = " + this.content + ", acc sequence = " + accSequence + ", indexes [" + indexes + "] )\n";

			return tmp;
		}
//----------------------------------------------------------------------------


		public void dump() {
			String indexes = " ";
			content = " ";
			for ( byte d = 0; d < cardIndexes.size(); ++d ) {
				indexes += ( ( Integer ) cardIndexes.elementAt( d ) ).intValue() + " ";
				content += ( ( PokerCard ) pokerCards.elementAt( d ) ).getPrettyRank() + " ";
			}
			System.out.println(
					"( score = " + finalScore + ", left cards = " + this.leftCards + ", whoAmI = " + this.whoAmI + ", level = " + this.level + ", content = " + this.content + ", acc sequence = " + accSequence + ", indexes [" + indexes + "] )" );

			for ( byte c = 0; c < getTotalNextNodes(); ++c ) {
				getNext( c ).dump();
			}

		}
//----------------------------------------------------------------------------


		public int cardsToCome() {
			int toCome = usedCards;
			if ( parent != null ) {
				toCome += parent.cardsToCome();
			}
			return toCome;
		}
//----------------------------------------------------------------------------
		//#endif
//</editor-fold>


		/**
		 *
		 * @param n
		 * @return
		 */
		public SequenceNode getNext( int n ) {
			return ( SequenceNode ) next.elementAt( n );
		}


		/**
		 *
		 * @param handCards
		 * @return
		 */
		public int getPartialScore() {
			int candidatescore;
			int bestscore;
			int chosenScore = -1;


			if ( getTotalNextNodes() > 0 ) {
				chosenScore = 0;
			}


			//adiciona minhas cartas ao handCards
			for ( byte c = 0; c < getTotalNextNodes(); ++c ) {

				if ( c == chosenScore ) {
					continue;
				}

				bestscore = getNext( chosenScore ).getPartialScore();

				if ( ( getNext( c ).getEval() ) == POKERHAND_STRAIGHTFLUSH ) {
					System.out.println( "achei um straightflush" );
					bestscore += ( POKERHAND_STRAIGHTFLUSH << 20 );
				}

				candidatescore = getNext( c ).getPartialScore();
				
				if ( ( getNext( c ).getEval() ) == POKERHAND_STRAIGHTFLUSH ) {
					System.out.println( "achei um straightflush" );
					candidatescore += ( POKERHAND_STRAIGHTFLUSH << 20 );
				}

				if ( candidatescore > bestscore ) {
					chosenScore = c;
				}

			}
			if ( chosenScore != -1 ) {
				finalScore = ( partialScore << ( ( getNext( chosenScore ).getUsedCards() ) * 4 ) ) + getNext(
						chosenScore ).getPartialScore();

				if ( accSequence == 0 ) {
					for ( byte c = 0; c < getNext( chosenScore ).cardIndexes.size(); ++c ) {
						cardIndexes.addElement( getNext( chosenScore ).cardIndexes.elementAt( c ) );
						pokerCards.addElement( getNext( chosenScore ).pokerCards.elementAt( c ) );
					}
					accSequence += sequence + getNext( chosenScore ).accSequence;
					leftCards = getNext( chosenScore ).leftCards;
				}

				if ( level == 0 ) {
					//da uma empurrada marota, pra caber os kickers
					finalScore = finalScore << ( leftCards * 4 );
					finalScore += accSequence << 20;
				}

				return finalScore;
			} else {
				accSequence = sequence;
				return finalScore = partialScore;
			}
		}


		public int getUsedCards() {
			int totalUsedCards = usedCards;
			for ( byte c = 0; c < getTotalNextNodes(); ++c ) {
				totalUsedCards += getNext( c ).getUsedCards();
			}
			return totalUsedCards;
		}


		public int usefulCards( byte[] indexes ) {
			int toReturn = 0;
			for ( byte c = 0; c < indexes.length; ++c ) {
				if ( indexes[c] != -1 ) {
					++toReturn;
				}
			}
			return toReturn;
		}


		public int getNumCards() {
			return cardIndexes.size();
		}


		public int getIndex( int c ) {
			return ( ( Integer ) cardIndexes.elementAt( c ) ).intValue();
		}


		public PokerCard cardAt( int c ) {
			return ( ( PokerCard ) pokerCards.elementAt( c ) );
		}


		public boolean equivalent( SequenceNode node ) {

			System.out.println( "getNumCards() = " + getNumCards() );
			System.out.println( "node.getNumCards() = " + node.getNumCards() );

			if ( getNumCards() < CARDS_WORTH || node.getNumCards() < CARDS_WORTH ) {
				return false;
			}

			for ( byte c = 0; c < NanoMath.min( getNumCards(), node.getNumCards() ); ++c ) {

				System.out.println( "rank:" + cardAt( getIndex( c ) ).getRank() );
				System.out.println( "node.rank:" + node.cardAt( ( node.getIndex( c ) ) ).getRank() );

				if ( cardAt( getIndex( c ) ).getRank() != node.cardAt( ( node.getIndex( c ) ) ).getRank() ) {
					return false;
				}
			}

			return true;
		}


		private boolean intersect( SequenceNode node ) {

			for ( byte c = 0; c < this.cardIndexes.size(); ++c )  {
				for ( byte d = 0; d < node.getUsedCards(); ++d ) {
					if ( getIndex( c ) == node.getIndex( d ))
						return true;
				}
			}
			return false;
		}

		private void addNode( byte[] indexes, PokerCard[] cards, int sequence, String desc ) {
			int cardsInSeq = 0;

			int val = 0;
			for ( byte c = 0; c < indexes.length; ++c ) {

				if ( indexes[c] != -1 ) {
					++cardsInSeq;
				}

				if ( indexes[c] != -1 ) {
					val += cards[indexes[c]].getRank() << ( 4 * ( usefulCards( indexes ) - c - 1 ) );
				}
			}

			int taken = 0;
			for ( byte c = 0; c < getTotalNextNodes(); ++c ) {
				if ( cardsInSeq <= getNext( c ).leftCards ) {
					++taken;
					getNext( c ).addNode( indexes, cards, sequence, desc );
				}
			}

			if ( getTotalNextNodes() == 0 || cardsInSeq <= leftCards ) {
				SequenceNode node = new SequenceNode( leftCards - cardsInSeq );
				//#if DEBUG == "true"
				node.parent = this;
				//#endif
				node.partialScore = val;
				node.sequence = sequence;
				node.usedCards = cardsInSeq;
				node.description = desc;

				for ( byte d = 0; d < indexes.length; ++d ) {
					if ( indexes[d] != -1 ) {
						node.cardIndexes.addElement( new Integer( indexes[d] ) );
						node.pokerCards.addElement( cards[indexes[d]] );
					}
				}

				node.level = level + 1;

				//#if DEBUG == "true"
				node.whoAmI = next.size();
				//#endif

				if ( !intersect( node ) )
					next.addElement( node );
			}
		}


		public int getTotalNextNodes() {
			return next.size();
		}


		public int getEval() {
			return sequence;
		}
	}
	//</editor-fold>
//</editor-fold>
//<editor-fold desc="Constantes publicas">
	//<editor-fold desc="Mãos de Poker">
	// num byte, cada bit vai indicar uma possibilidade, sendo que duplas
	// ganham dois bits, porque pode indicar mais de uma dupla
	// [QUADRA][FLUSH][STRAIGHT][TRIPPLET][2DOUPLET][DOUPLET][HIGHCARD][não usado]
	// Um full house é caracterizado por 0001010
	// Além disso, a parte baixa de um short pode ser usada para indicar o valor
	// da combinação: ex. Quadra de ases, Royal Straight Flush, etc;
	/**
	 * Uma quadra ( três diffs juntos são 0 )
	 */
	public static final int POKERHAND_QUADRA = Integer.parseInt( "00000000000000000000000001000000", 2 );
	public static final String POKERHAND_NAME_QUADRA = "Quadra";
	/**
	 * Flush ( 5 incidências de um mesmo naipe )
	 */
	public static final int POKERHAND_FLUSH = Integer.parseInt( "00000000000000000000000000100000", 2 );
	public static final String POKERHAND_NAME_FLUSH = "Flush";
	/**
	 * Uma sequencia ( todos os diffs medem 1 )
	 */
	public static final int POKERHAND_STRAIGHT = Integer.parseInt( "00000000000000000000000000010000", 2 );
	public static final String POKERHAND_NAME_STRAIGHT = "Straight";
	/**
	 * Uma trinca (  dois diffs juntos são 0 )
	 */
	public static final int POKERHAND_TRIPPLET = Integer.parseInt( "00000000000000000000000000001000", 2 );
	public static final String POKERHAND_NAME_TRIPPLET = "Tripla";
	/**
	 * Duas duplas ( dois diffs em grupos distintos são zero )
	 */
	public static final int POKERHAND_DOUBLE_DOUPLET = Integer.parseInt( "00000000000000000000000000000100", 2 );
	public static final String POKERHAND_NAME_DOUBLE_DOUPLET = "Duas duplas";
	/**
	 * Uma dupla ( um diffs é 0 )
	 */
	public static final int POKERHAND_DOUPLET = Integer.parseInt( "00000000000000000000000000000010", 2 );
	public static final String POKERHAND_NAME_DOUPLET = "Dupla";
	/**
	 * Nenhuma combinação registrada
	 */
	public static final int POKERHAND_HIGH = Integer.parseInt( "00000000000000000000000000000001", 2 );
	public static final String POKERHAND_NAME_HIGH = "Uma mão comum";
	/**
	 * Straight-Flush
	 */
	public static final int POKERHAND_STRAIGHTFLUSH = Integer.parseInt( "00000000000000000000000001110001", 2 );
	public static final String POKERHAND_NAME_STRAIGHTFLUSH = "Straight Flush";
	/**
	 * FullHouse
	 */
	public static final int POKERHAND_FULLHOUSE = Integer.parseInt( "00000000000000000000000000111011", 2 );
	public static final String POKERHAND_NAME_FULLHOUSE = "Full House";
	//</editor-fold>
//</editor-fold>
//<editor-fold desc="Campos da classe">
	private SequenceNode root = null;
	/**
	 * cartas da mão
	 */
	private PokerCard[] cards;
	/**
	 * Backup da mão do jogador
	 */
	private PokerCard[] backup;
	private PokerCard[] cardsToShow;
	private String lastEval;
	private int lastScore;
//</editor-fold>
////<editor-fold desc="Métodos da classe">


	public String getLastEval() {
		return lastEval;
	}


	/**
	 * Construtor. Apenas reserva o espaço paras cartas.
	 */
	public PokerHand() {
		lastEval = POKERHAND_NAME_HIGH;
		cards = new PokerCard[ CARD_ON_PLAYERS_HAND + PUBLIC_CARDS_ON_TABLE ];
		backup = new PokerCard[ CARD_ON_PLAYERS_HAND ];
		cardsToShow = new PokerCard[ CARDS_WORTH ];
		lastScore = 0;
	}


	/**
	 *
	 * @return quantas cartas válidas estão na mão
	 */
	public final int cardsInHand() {
		int ret = 0;
		for ( byte c = 0; c < cards.length; ++c ) {
			if ( cards[c] != null ) {
				ret++;
			}
		}
		return ret;
	}


	/**
	 * Adiciona uma carta na mão. Assume que não existem buracos na mão
	 * @param card é a referencia da carta que vai ser atribuida a esta mão
	 */
	public final void addCard( PokerCard card ) {
		cards[cardsInHand()] = card;
	}


	/**
	 * Calcula o score de uma sequencia
	 * @param string contendo a sequencia codificada em hexadecimal
	 * @return o score obtido na sequencia, apenas considerando as 5 melhores cartas
	 */
	public int scoreSequence() {
		int returnVal = 0;

		HeapSort.sort( cards );

		returnVal = evaluateSpecialSequences();

		return returnVal;
	}


	private void shiftRight( byte[] vec, byte from ) {
		for ( byte c = from; c < vec.length - 1; ++c ) {
			vec[c] = vec[c + 1];
		}

		vec[vec.length - 1] = -1;
	}


	private void addUnique( byte[] vec, byte num ) {

		for ( byte d = 0; d < vec.length; ++d ) {
			if ( vec[d] == num ) {
				shiftRight( vec, d );
			}
		}

		for ( byte c = ( byte ) ( vec.length - 1 ); c > 0; --c ) {
			vec[c] = vec[c - 1];
		}

		vec[ 0] = num;
	}


	private void invalidateAll( byte[] vec ) {
		for ( byte c = ( byte ) ( vec.length - 1 ); c >= 0; --c ) {
			vec[c] = -1;
		}
	}

//#if DEBUG == "true"

	private void print( byte[] vec ) {
		String p = " vec:";

		for ( byte c = 0; c < vec.length; ++c ) {
			p += " " + vec[c];
		}

		System.out.println( p );
	}
//#endif


	/**
	 *
	 * @param vec
	 * @param val
	 */
	private void addStraight( byte[] vec, byte val ) {
		//#if DEBUG == "true"
		System.out.println( "STRAIGHT!" );
		//#endif
		invalidateAll( vec );

//		for ( byte c = 0; c < CARDS_WORTH; ++c ) {
//
//			if ( cards[ val ].getRank() != lastRank ) {
//				addUnique( vec, ( byte ) ( val ) );
//				System.out.println( "add " + cards[ val ] );
//				lastRank = cards[ val ].getRank();
//			} else {
//				--c;
//			}
//			--val;
//		}
		
		byte lastRank = cards[ val ].getRank();
		byte c = 1;
		System.out.println( " comecei com " + cards[ val ] );
		addUnique( vec, ( byte ) ( val ) );
		for ( ; val >= 0; --val ) {

			if ( cards[ val ].getRank() == lastRank - 1 ) {
				addUnique( vec, ( byte ) ( val ) );
				System.out.println( "add " + cards[ val ] );
				lastRank = cards[ val ].getRank();
				++c;

				if ( c == CARDS_WORTH )
					return;
			}
		}

	}

	private void addStraightFlush( byte[] vec, byte val ) {
		//#if DEBUG == "true"
		System.out.println( "STRAIGHT! ...flush" );
		//#endif
		int lastRank = -1;
		invalidateAll( vec );
		System.out.println( "cards:" );
		for ( byte c = 0; c < CARDS_WORTH; ++c ) {

			if ( cards[val].getRank() != lastRank ) {
				addUnique( vec, ( byte ) ( val ) );
				lastRank = cards[val].getRank();
				System.out.print( " " + cards[ val ] );
			} else {
				--c;
			}
			--val;
		}
		System.out.println( );
		makeSeq( vec, cards, POKERHAND_STRAIGHTFLUSH, POKERHAND_NAME_STRAIGHTFLUSH );
	}



	private void makeSeq( byte[] indexes, PokerCard[] cards, int sequence, String desc ) {
		root.addNode( indexes, cards, sequence, desc );
	}


	private void makeFlush( byte[] indexes, PokerCard[] cards ) {
//		invalidateAll( indexes );
		root.addNode( indexes, cards, POKERHAND_FLUSH, POKERHAND_NAME_FLUSH );
	}


	/**
	 * Avalia sequencias especiais ( duplas, trincas, flushes, etc )
	 * @return valor da mão codificada. bytes: [não usado][tipo da mão][high1][high2]
	 */
	private int evaluateSpecialSequences() {
		//#if DEBUG == "true"
			System.out.println( "avaliando sequencia" );
		//#endif
		int size = cardsInHand();
		int[] suits = new int[ SUITS_PER_DECK ];
		int[] diffs = new int[ size ];
		byte highs[] = new byte[ size ];
		int seqOnes = 0;
		int seqZeros = 0;
		int lastZero = -1;
		int eval = POKERHAND_HIGH;
		int higherSeq = eval;
		int lastSeq = eval;
		int equalSuits = 0;
		SequenceNode oldNode;
		boolean[] used = { false, false, false, false, false, false, false };

		root = new SequenceNode( 5 );

		lastEval = POKERHAND_NAME_HIGH;

		//<editor-fold desc="Obtem as diferenças de valores e os kickers">
		invalidateAll( highs );

		for ( byte e = 0; e < size; ++e ) {
			diffs[e] = cards[e].getRank();
			highs[e] = ( byte ) ( size - e - 1 );
		}

		for ( byte f = 0; f < size - 1; ++f ) {
			diffs[f] = diffs[f + 1] - diffs[f];
		}
		//</editor-fold>

//<editor-fold desc="Busca por sequencias simples: quadras, trincas, straights, etc">
		for ( byte g = 0; g < size - 1; ++g ) {

			if ( diffs[g] == 0 ) {
				//encontrado um 0. Importante quebrar a sequencia de 1s
				lastZero = g;
				seqZeros++;

				if ( g - 1 >= 0 && diffs[g - 1] != 0 && eval != POKERHAND_HIGH ) {
					makeSeq( highs, cards, eval, lastEval );
				}

			} else if ( diffs[g] == 1 ) {

				//encontrado um 1. Importante quebrar a sequencia de 0s
				seqOnes++;
				seqZeros = 0;
				
				if ( cards[ g ].getSuit() == cards[ g + 1 ].getSuit() )
					++equalSuits;
				else
					equalSuits = 0;

			} else {
				//nenhum dos dois. Quebra as duas sequencias
				seqZeros = 0;

				//CARDS_WORTH - 1 porque para 5 cartas sequenciais, voce tem 4 uns
				if ( seqOnes < ( CARDS_WORTH - 1 ) ) {
					seqOnes = 0;
				}
			}

			//busca um straight
			if ( seqOnes >= ( CARDS_WORTH - 1 ) ) {
				oldNode = root;
				root = new SequenceNode( 5 );

				if ( equalSuits >= CARDS_WORTH) {
					addStraightFlush( highs, ( byte ) ( g + 1 ) );
					eval = POKERHAND_STRAIGHTFLUSH;
					lastEval = POKERHAND_NAME_STRAIGHTFLUSH;
				} else if ( eval != POKERHAND_STRAIGHTFLUSH ) {
					addStraight( highs, ( byte ) ( g ) );
					eval = POKERHAND_STRAIGHT;
					lastEval = POKERHAND_NAME_STRAIGHT;
				}


				//finaliza o processamento (não era pra continuar?)
//				g = ( byte ) ( size - 1 );

			} else if ( seqZeros >= 1 && eval != POKERHAND_STRAIGHTFLUSH ) {
				//<editor-fold desc="Repetições">
				if ( seqZeros == 3 ) {

					addUnique( highs, ( byte ) ( g + 1 ) );
					used[g + 1] = true;
					//#if DEBUG == "true"
					System.out.println( "quadra" );
					//#endif
					eval = POKERHAND_QUADRA;
					lastEval = POKERHAND_NAME_QUADRA;
					makeSeq( highs, cards, eval, lastEval );
				} else if ( seqZeros == 2 ) {

					addUnique( highs, ( byte ) ( g + 1 ) );
					used[g + 1] = true;
					//#if DEBUG == "true"
					System.out.println( "tripla" );
					//#endif
					eval = POKERHAND_TRIPPLET;
					lastEval = POKERHAND_NAME_TRIPPLET;
					makeSeq( highs, cards, eval, lastEval );
				} else if ( seqZeros == 1 ) {
					//douplet ou double dupplet
					//tem que mandar as duas e ver em quais arvores cada uma se encaixa.
					invalidateAll( highs );
					addUnique( highs, ( byte ) ( g ) );
					addUnique( highs, ( byte ) ( g + 1 ) );
					used[g] = true;
					used[g + 1] = true;
					//#if DEBUG == "true"
					System.out.println( "dupla" );
					//#endif
					eval = POKERHAND_DOUPLET;
					lastEval = POKERHAND_NAME_DOUPLET;
					makeSeq( highs, cards, eval, lastEval );
				}
				//</editor-fold>
			}
		}

		if ( eval != POKERHAND_STRAIGHTFLUSH ) {

			if ( eval != POKERHAND_HIGH ) {
				makeSeq( highs, cards, eval, lastEval );
			}

			eval = root.getPartialScore();
		} else {
			root = new SequenceNode( 5 );
			root.accSequence = POKERHAND_STRAIGHTFLUSH;
			
			for ( byte c = 0; c < highs.length; ++c ) {
				if ( highs[ c ] != -1 ) {
					root.cardIndexes.addElement( new Integer( highs[ c ] ) );
					root.description = POKERHAND_NAME_STRAIGHTFLUSH;
					root.usedCards = 5;
					root.leftCards = 0;
				}
			}
		}
		//</editor-fold>
		//<editor-fold desc="Busca por um flush">
		// Essa checagem de qual é a maior carta para cada naipe é para
		//garantir que Straight+Flush < StraightFlush
		if ( eval != POKERHAND_STRAIGHTFLUSH ) {
			byte[] highFlushes = new byte[ SUITS_PER_DECK ];
			suits[PokerCard.SUIT_CLUB] = 0;
			suits[PokerCard.SUIT_SPADE] = 0;
			suits[PokerCard.SUIT_DIAMOND] = 0;
			suits[PokerCard.SUIT_HEART] = 0;

			higherSeq = PokerCard.SUIT_SPADE;

			byte[][] cardsPerSuit = new byte[ 4 ][ 10 ];
			cardsPerSuit[ 0] = new byte[ 10 ];
			cardsPerSuit[ 1] = new byte[ 10 ];
			cardsPerSuit[ 2] = new byte[ 10 ];
			cardsPerSuit[ 3] = new byte[ 10 ];

			for ( byte i = 0; i < 10; ++i ) {
				cardsPerSuit[ 0][i] = -1;
				cardsPerSuit[ 1][i] = -1;
				cardsPerSuit[ 2][i] = -1;
				cardsPerSuit[ 3][i] = -1;
			}

			for ( byte c = 0; c < size; ++c ) {
				cardsPerSuit[cards[c].getSuit()][suits[cards[c].getSuit()]] = c;
				suits[ cards[c].getSuit() ]++;

				if ( highFlushes[cards[c].getSuit()] < cards[c].getRank() ) {
					highFlushes[cards[c].getSuit()] = c;
				}
			}

			int flushCards;
			for ( byte d = 0; d < ( CARDS_WORTH - 1 ); ++d ) {

				if ( suits[ d ] >= ( CARDS_WORTH ) ) {
					root = new SequenceNode( 5 );

					eval = POKERHAND_FLUSH;
					lastEval = POKERHAND_NAME_FLUSH;

					invalidateAll( highs );
					flushCards = 0;
					for ( byte cd = ( byte ) ( cardsPerSuit[ d ].length - 1 ); cd >= 0 && flushCards < CARDS_WORTH ; --cd ) {
						
						if ( cardsPerSuit[ d ][ cd ] == -1 )
							continue;

						++flushCards;

						addUnique( highs, cardsPerSuit[ d ][ cd ] );
					}

					makeFlush( highs, cards );
					lastEval += " de " + getSuitPrettyName( d );
					higherSeq = lastSeq = eval;
					d = CARDS_WORTH;
					break;
				}
			}

			if ( ( eval ) == POKERHAND_FLUSH ) {
				eval = root.getPartialScore();
				root.leftCards = 0;
			}
		}
		//</editor-fold>

		for ( byte c = ( byte ) ( size - 1 ); c >= 0 && root.leftCards > 0; --c ) {
			if ( !used[c] ) {
				--root.leftCards;
				eval += ( cards[c].getRank() ) << root.leftCards * 4;
				root.pokerCards.addElement( cards[c] );
			}
		}

		if ( root.accSequence == 0 ) {
			eval = eval + ( POKERHAND_HIGH << 20 );
		}

		if ( ( ( eval >> 20 ) & POKERHAND_DOUBLE_DOUPLET ) == POKERHAND_DOUBLE_DOUPLET ) {
			lastEval = POKERHAND_NAME_DOUBLE_DOUPLET;
		}

		if ( ( ( eval >> 20 ) & ( POKERHAND_DOUPLET + POKERHAND_TRIPPLET ) ) == ( POKERHAND_DOUPLET + POKERHAND_TRIPPLET ) ) {
			eval = ( POKERHAND_FULLHOUSE << 20 ) + ( eval & 1048575 );
			lastEval = POKERHAND_NAME_FULLHOUSE;
		}

		cardsToShow = new PokerCard[ root.pokerCards.size() ];

		for ( byte d = 0; d < root.pokerCards.size(); ++d ) {
			cardsToShow[d] = ( ( PokerCard ) root.pokerCards.elementAt( d ) );
		}

		HeapSort.sort( cardsToShow );

		lastScore = eval;
		return eval;
	}

//	private int evaluateSpecialSequences() {
//		int[] suits = new int[ SUITS_PER_DECK ];
//		int eval = POKERHAND_HIGH;
//		int size = cardsInHand();
//		int[] diffs = new int[ size ];
//		int seqOnes = 0;
//		byte high1 = -1; //eu queria tirar essas variaveis, mas ainda nao posso..
//		byte high2 = -1;
//		byte highs[] = new byte[ size ];
//		int seqZeros = 0;
//		int lastZero = -1;
//		int higherSeq = eval;
//		int lastSeq = eval;
//		PokerHand cardsWorth = new PokerHand();
//
//		lastEval = POKERHAND_NAME_HIGH;
//		//<editor-fold desc="busca por straights e repetições">
//		//<editor-fold desc="Obtem as diferenças de valores e os kickers">
//		high1 = 0;
//		high2 = 0;
//
//		invalidateAll( highs );
//
//		for ( byte e = 0; e < size; ++e ) {
//			diffs[e] = cards[e].getRank();
//			if ( diffs[e] > diffs[high1] ) {
//
//
//				high2 = high1;
//				high1 = e;
//			}
//			highs[e] = ( byte ) ( size - e - 1 );
//		}
//
//		for ( byte f = 0; f < size - 1; ++f ) {
//			diffs[f] = diffs[f + 1] - diffs[f];
//		}
//		//</editor-fold>
//		//<editor-fold desc="Busca por sequencias simples: quadras, trincas, straights, etc">
//		for ( byte g = 0; g < size - 1; ++g ) {
//
//			if ( diffs[g] == 0 ) {
//
//				//encontrado um 0. Importante quebrar a sequencia de 1s
//				lastZero = g;
//				seqZeros++;
//
//			} else if ( diffs[g] == 1 ) {
//
//				//encontrado um 1. Importante quebrar a sequencia de 0s
//				seqOnes++;
//				seqZeros = 0;
//
//			} else {
//
//				//nenhum dos dois. Quebra as duas sequencias
//				seqZeros = 0;
//
//				if ( seqOnes < ( CARDS_WORTH - 1 ) ) {
//					seqOnes = 0;
//				}
//			}
//
//			//busca um straight
//			if ( seqOnes >= ( CARDS_WORTH - 1 ) ) {
//
//				eval = eval | POKERHAND_STRAIGHT;
//				lastEval = POKERHAND_NAME_STRAIGHT;
//				high1 = ( byte ) ( g + 1 ); //afinal o diff e' com a carta seguinte
//
//				addStraight( highs, high1 );
//
//				high2 = -1;
//				higherSeq = lastSeq = POKERHAND_STRAIGHT;
//				g = ( byte ) ( size - 1 );
//
//			} else if ( seqZeros >= 1 ) {
//				//<editor-fold desc="Repetições">
//				if ( seqZeros == 3 ) {
//
//					if ( ( eval & POKERHAND_TRIPPLET ) == POKERHAND_TRIPPLET && lastZero == g ) {
//						eval = eval - POKERHAND_TRIPPLET;
//					}
//
//					if ( ( eval & POKERHAND_DOUPLET ) == POKERHAND_DOUPLET && lastZero == g ) {
//						eval = eval - POKERHAND_DOUPLET;
//					}
//
//					eval = eval | POKERHAND_QUADRA;
//					lastEval = POKERHAND_NAME_QUADRA;
//					lastSeq = POKERHAND_QUADRA;
//
//					addUnique( highs, ( byte ) ( g + 1 ));
//
//					if ( higherSeq < lastSeq ) {
//						higherSeq = lastSeq;
//
//						if ( cards[high1].getRank() != cards[high2].getRank() && cards[high1].getRank() != cards[g].getRank() ) {
//							high2 = high1;
//							high1 = g;
//						}
//
//					} else {
//						if ( cards[high2].getRank() != cards[g].getRank() ) {
//							high2 = g;
//						}
//					}
//
//					seqZeros = 0;
//
//				} else if ( seqZeros == 2 ) {
//
//					if ( ( eval & POKERHAND_DOUPLET ) == POKERHAND_DOUPLET && lastZero == g ) {
//						eval = eval - POKERHAND_DOUPLET;
//					}
//
//					if ( ( eval & POKERHAND_DOUBLE_DOUPLET ) == POKERHAND_DOUBLE_DOUPLET && lastZero == g ) {
//						eval = eval - POKERHAND_DOUBLE_DOUPLET;
//						eval = eval | POKERHAND_DOUPLET;
//					}
//
//					eval = eval | POKERHAND_TRIPPLET;
//					lastEval = POKERHAND_NAME_TRIPPLET;
//					lastSeq = POKERHAND_TRIPPLET;
//
//					addUnique( highs, ( byte ) ( g + 1 ));
//
//					if ( higherSeq < lastSeq ) {
//						higherSeq = lastSeq;
//
//						if ( cards[high1].getRank() != cards[high2].getRank() && cards[high1].getRank() != cards[g].getRank() ) {
//
//							high2 = high1;
//							high1 = g;
//
//						}
//
//					} else {
//						if ( cards[high2].getRank() != cards[g].getRank() ) {
//							high2 = g;
//						}
//					}
//
//				} else if ( seqZeros == 1 ) {
//					if ( ( eval & POKERHAND_DOUPLET ) == POKERHAND_DOUPLET ) {
//						//apaga o bit anterior
//						eval = eval - POKERHAND_DOUPLET;
//						// e liga o seguinte.
//						eval = eval | POKERHAND_DOUBLE_DOUPLET;
//						//efetivamente, é como se fosse feito um
//						// eval = eval |  ( POKERHAND_DOUPLET + 1 );
//						// em primeiro lugar.
//
//						lastSeq = POKERHAND_DOUBLE_DOUPLET;
//						lastEval = POKERHAND_NAME_DOUBLE_DOUPLET;
//
//
//						addUnique( highs, ( byte ) ( g ));
//						addUnique( highs, ( byte ) ( g + 1) );
//
//						if ( higherSeq < lastSeq ) {
//							higherSeq = lastSeq;
//
//							if ( cards[high1].getRank() != cards[high2].getRank() && cards[high1].getRank() != cards[g].getRank() ) {
//								high2 = high1;
//								high1 = g;
//							}
//
//						} else {
//							if ( cards[high2].getRank() != cards[g].getRank() ) {
//								high2 = g;
//							}
//						}
//
//
//					} else {
//						eval = eval | POKERHAND_DOUPLET;
//						lastSeq = POKERHAND_DOUPLET;
//						lastEval = POKERHAND_NAME_DOUPLET;
//
//						addUnique( highs, ( byte ) ( g ) );
//						addUnique( highs, ( byte ) ( g + 1 ) );
//
//						if ( higherSeq < lastSeq ) {
//							higherSeq = lastSeq;
//
//							if ( cards[high1].getRank() != cards[high2].getRank() && cards[high1].getRank() != cards[g].getRank() ) {
//								high2 = high1;
//								high1 = g;
//							}
//
//						} else {
//							if ( cards[high2].getRank() != cards[g].getRank() ) {
//								high2 = g;
//							}
//						}
//					}
//				}
//				//</editor-fold>
//			}
//		}
//		//</editor-fold>
//		//</editor-fold>
//		//<editor-fold desc="Busca por um flush">
//		// Essa checagem de qual é a maior carta para cada naipe é para
//		//garantir que Straight+Flush < StraightFlush
//		byte[] highFlushes = new byte[ SUITS_PER_DECK ];
//
//		suits[PokerCard.SUIT_CLUB] = 0;
//		suits[PokerCard.SUIT_SPADE] = 0;
//		suits[PokerCard.SUIT_DIAMOND] = 0;
//		suits[PokerCard.SUIT_HEART] = 0;
//
//		higherSeq = PokerCard.SUIT_SPADE;
//
//		byte[][] cardsPerSuit = new byte[ 4 ][ 7 ];
//		cardsPerSuit[ 0] = new byte[ 7 ];
//		cardsPerSuit[ 1] = new byte[ 7 ];
//		cardsPerSuit[ 2] = new byte[ 7 ];
//		cardsPerSuit[ 3] = new byte[ 7 ];
//
//		for ( byte c = 0; c < size; ++c ) {
//			cardsPerSuit[cards[c].getSuit()][suits[cards[c].getSuit()]] = c;
//			suits[cards[c].getSuit()]++;
//
//			if ( highFlushes[cards[c].getSuit()] < cards[c].getRank() ) {
//				highFlushes[cards[c].getSuit()] = c;
//			}
//		}
//
//		for ( byte d = 0; d < ( CARDS_WORTH - 1 ); ++d ) {
//
//			if ( suits[d] >= ( CARDS_WORTH ) ) {
//
//				if ( ( eval &  POKERHAND_STRAIGHT ) == ( POKERHAND_STRAIGHT ) && highFlushes[ d ] == highs[ 0 ] ) {
//					eval = POKERHAND_STRAIGHTFLUSH;
//					lastEval = POKERHAND_NAME_STRAIGHTFLUSH;
//				} else {
//					eval = eval | POKERHAND_FLUSH | POKERHAND_HIGH;
//
//					lastEval = POKERHAND_NAME_FLUSH;
//
//					for ( byte cd = 0; cd < suits[d]; ++cd ) {
//						highs[cd] = cardsPerSuit[d][cd];
//					}
//
//					lastEval += " de " + getSuitPrettyName( d );
//					higherSeq = lastSeq = eval;
//					high2 = highFlushes[d];
//				}
//
//			}
//		}
//		//</editor-fold>
//		//<editor-fold desc="Busca por combinações especiais, cujo padrão binário sozinho não seria capaz de tornar maiores">
//		//você pode ter um straight e um flush que não são necessariamente um straight-flush
//		///cada if tem else porque os bits de cada um vão estar ligados e podem dar falsos sinais
//		 if ( ( eval & ( POKERHAND_DOUBLE_DOUPLET + POKERHAND_TRIPPLET ) ) == ( POKERHAND_DOUBLE_DOUPLET + POKERHAND_TRIPPLET ) ) {
//			eval = POKERHAND_FULLHOUSE;
//			lastEval = POKERHAND_NAME_FULLHOUSE;
//		} else if ( ( eval & ( POKERHAND_DOUPLET + POKERHAND_TRIPPLET ) ) == ( POKERHAND_DOUPLET + POKERHAND_TRIPPLET ) ) {
//			eval = POKERHAND_FULLHOUSE;
//			lastEval = POKERHAND_NAME_FULLHOUSE;
//		}
//
//		//</editor-fold>
//		//<editor-fold desc="Codifica as sequencias encontradas, assim como os kickers">
////		if ( high1 != -1 ) {
////			lastEval += " de " + cards[high1].getPrettyRank();
////		}
////
////		if ( high2 != -1 ) {
////
////			if (!( eval == POKERHAND_HIGH || ( eval & POKERHAND_FLUSH ) == POKERHAND_FLUSH )) {
////				lastEval += " com kicker de ";
////				lastEval += cards[high2].getPrettyRank();
////			}
////
////		}
//
//
//		if ( size > CARDS_WORTH ) {
//
//			cardsToShow = new PokerCard[ CARDS_WORTH ];
//			PokerCard pc;
//
//			for ( byte cd = 0; cd < CARDS_WORTH; ++cd ) {
//				pc = cards[ highs[ cd ] ];
//				cardsToShow[ cd ] = new PokerCard( pc.getSuit(), pc.getRank() );
//				cardsWorth.addCard( cardsToShow[ cd ] );
//			}
//
//			eval = cardsWorth.scoreSequence();
//			lastEval = cardsWorth.getLastEval();
//
//			lastScore = eval;
//			return eval;
//
//		} else {
//			//porque Straight flush de As deixa a desejar... ;-)
//			if ( eval == POKERHAND_STRAIGHTFLUSH && cards[ high1 ].getRank() == PokerCard.RANK_A ) {
//				lastEval = "Royal Straight Flush";
//			}
//
//			eval = eval << 20;
//
//			for ( byte cd = 0; cd < NanoMath.min( size, CARDS_WORTH ); ++cd ) {
//				if ( highs[ cd ] != -1 && cards[ highs[ cd ] ] != null ) {
//					eval += ( ( cards[ highs[ cd ] ].getRank() ) << ( 4 * ( CARDS_WORTH  - cd - 1 ) ) );
//				}
//			}
//		}
//
//		//</editor-fold>
//		lastScore = eval;
//		return eval;
//	}

	public int getLastScore() {
		return lastScore;
	}


	public PokerCard[] getCardsWorth() {
		return cardsToShow;
	}


	/**
	 * Ordena as cartas na mão do jogador
	 */
	public void sort() {
		HeapSort.sort( cards );
	}


	/**
	 * guarda as cartas que o jogador tem no começo do jogo
	 */
	public void backup() {
		//#if DEBUG == "true"
		if ( cardsInHand() > CARD_ON_PLAYERS_HAND ) {
			System.out.println( "algo de podre por aqui..." );
		}
		//#endif

		for ( byte d = 0; d < backup.length; ++d ) {
			backup[d] = cards[d];
		}
	}


	/**
	 * restaura a mão original do jogador
	 */
	public void restore() {
		for ( byte c = 0; c < cards.length; ++c ) {
			cards[c] = null;
		}

		for ( byte d = 0; d < backup.length; ++d ) {
			cards[d] = backup[d];
		}
	}


	/**
	 *
	 * @return Obtem a  representação em string da mão, usando a representação das cartas
	 */
	public String toString() {
		String toReturn = "";

		for ( byte c = 0; c < cardsInHand(); ++c ) {

			if ( cards[c] != null ) {
				toReturn += cards[c].toString();
			}
		}

		return toReturn;
	}


	public void restoreIfNeeded() {
		if ( cardsInHand() > CARD_ON_PLAYERS_HAND ) {
			restore();
		}
	}


	private String getSuitPrettyName( int s ) {
		switch ( s ) {
			case PokerCard.SUIT_CLUB:
				return "paus";
			case PokerCard.SUIT_HEART:
				return "copas";
			case PokerCard.SUIT_DIAMOND:
				return "ouros";
			case PokerCard.SUIT_SPADE:
				return "espadas";
		}
		return "";
	}
//</editor-fold>
}
