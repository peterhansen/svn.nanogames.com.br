package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.NanoMath;
import core.UI.Constants;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.util.Point;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif


/**
 *
 * @author Daniel "Monty" Monteiro
 */
class ImageGallery extends BasicScreen implements Constants, ScreenListener, KeyListener
//#if TOUCH == "true"
	, PointerListener
//#endif
{


	/** índice atualmente selecionado no menu */
	private int photoIndex = 0;
	/**
	 * 
	 */
	private Drawable content;
	/**
	 *
	 */
	int[] entries;
	private boolean browsing;
	private Point browsingPoint;
	private boolean dragging;
	private Point delta;


	/**
	 *
	 * @param entries
	 * @throws Exception
	 */
	public ImageGallery( int[] entries ) throws Exception {
		super( SCREEN_PHOTO_GALLERY, entries, GameMIDlet.getText( TEXT_GALLERY ) );
		browsingPoint = new Point( 0, 0 );
		this.entries = entries;
		browsing = false;
		setTitle( GameMIDlet.getText( TEXT_GALLERY ) );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		delta = new Point( 0,0 );
		
		setCurrentIndex( 0 );
	}


	/**
	 *
	 * @param index
	 */
	protected void setCurrentIndex( int index ) {
		super.setCurrentIndex( index );

		photoIndex = index;
		renewImage();

		//#if DEBUG == "true"
		System.out.println( "novo indice:" + photoIndex );
		//#endif
	}


	/**
	 * 
	 * @param key
	 */
	public final void keyReleased( int key ) {

		switch ( key ) {
			case ScreenManager.KEY_FIRE:				
			case ScreenManager.KEY_NUM5:

				if ( browsing ) {
					browsing = false;
				} else if ( photoIndex != entries.length - 1 ) {

					browsing = true;
					browsingPoint.set( 0, 0 );
				} else {
					super.keyReleased( key );
				}

				break;
			default:
				if ( !browsing ) {
					super.keyReleased( key );
				}
		}

		super.showBorders( !browsing );
		refreshContentSize();
	}


	/**
	 *
	 * @param key
	 */
	public final void keyPressed( int key ) {

		switch ( key ) {

			case ScreenManager.KEY_FIRE:
			case ScreenManager.KEY_NUM5:
				break;

			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_NUM2:
				if ( browsing ) {
//					if ( getHeight() < content.getHeight() ) {
					{
						if ( ( browsingPoint.y ) <= 0 ) {
							browsingPoint.y += 5;
							refreshContentSize();
						}
					}
				}
				break;

			case ScreenManager.KEY_DOWN:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM0:
				if ( browsing ) {
//					if ( getHeight() < content.getHeight() ) {
					{
						if ( getHeight() <= ( content.getHeight() + content.getPosY() ) ) {
							browsingPoint.y -= 5;
							refreshContentSize();
						}
					}
				}
				break;

			case ScreenManager.KEY_LEFT:
			case ScreenManager.KEY_NUM4:
				if ( browsing ) {
//					if ( getWidth() < content.getWidth() ) {
					{
						if ( ( browsingPoint.x ) < 0 ) {
							browsingPoint.x += 5;
							refreshContentSize();
						}
					}
					refreshContentSize();
				}
				break;

			case ScreenManager.KEY_RIGHT:
			case ScreenManager.KEY_NUM6:
				if ( browsing ) {
//					if ( getWidth() < content.getWidth() ) {
					{
						if ( browsingPoint.x > ( getWidth() - content.getWidth() ) ) {
							browsingPoint.x -= 5;
							refreshContentSize();
						}
					}
				}
				break;

			default:
				super.keyReleased( key );
		}
	}


	/**
	 *
	 */
	private void renewImage() {
		try {
			removeDrawable( content );
			content = null;
			System.gc();
			Drawable tmp;

			if ( entries.length == 1 ) {

				Label lbl = new Label( FONT_TEXT );
				lbl.setText( GameMIDlet.getText( TEXT_PHOTO_LOCKED ), false );
				lbl.setClipTest( false );
				lbl.setPosition( ( getWidth() - lbl.getTextWidth()) >> 1 , ( getHeight() - lbl.getHeight() ) >> 1 );
				tmp = lbl;

			} else if ( photoIndex >= 0 && entries[photoIndex] != TEXT_BACK ) {

				tmp = new DrawableImage( PATH_IMAGES + "player0/0/" + ( entries[photoIndex] - TEXT_PHOTO_1 ) + ".jpg" );
				tmp.setClipTest( true );
			} else {
				Label lbl = new Label( FONT_TEXT );				
				lbl.setText( GameMIDlet.getText( TEXT_BACK_MENU ), false );
				lbl.setClipTest( false );
				lbl.setPosition( ( getWidth() - lbl.getTextWidth()) >> 1 , ( getHeight() - lbl.getHeight() ) >> 1 );
				tmp = lbl;
			}

			content = tmp;
			refreshContentSize();

			if ( content != null )				
				insertDrawable( content );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
	}

	public void refreshContentSize() {

		if ( content instanceof DrawableImage ) {
			if ( !browsing ) {
				content.setSize( content.getWidth(), getHeight() - getArrowsGroupHeight() - getTitleHeight() - ( SEXY_HOT_MENU_BAR_OFFSET_Y << 1 ) );

				if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() )
					content.setPosition( ( getWidth() - content.getWidth() ) >> 1, getTitleHeight() );
				else
					content.setPosition( 0, ( getHeight() - content.getHeight() ) >> 1 );
			}
			else {

				if ( content.getWidth() < getWidth() )
					browsingPoint.x = ( getWidth() - content.getWidth() ) >> 1;

				if ( content.getHeight() < getHeight() )
					browsingPoint.y = ( getHeight() - content.getHeight() ) >> 1;

				content.setSize( ( ( DrawableImage ) content ).getImage().getWidth() , ( ( DrawableImage ) content ).getImage().getHeight() );
				content.setPosition( browsingPoint );
			}
		}
	}

//#if TOUCH == "true"
	public void onPointerDragged( int x, int y ) {
		if ( dragging ) {

					if ( getHeight() < content.getHeight() ) {
						browsingPoint.y += ( y - delta.y );

						if ( ( browsingPoint.y ) < 0 ) {
							browsingPoint.y  = 0;
						}

						if ( ( browsingPoint.y + content.getHeight() ) > getHeight() ) {
							browsingPoint.y = getHeight() - content.getHeight();
						}
					}

					if ( getWidth() < content.getWidth() ) {
						browsingPoint.x += ( x - delta.x );

						if ( ( browsingPoint.x ) > 0 ) {
							browsingPoint.x = 0;
						}

						if ( browsingPoint.x < ( getWidth() - content.getWidth() ) ) {
							browsingPoint.x = getWidth() - content.getWidth();
						}
					}

			delta.set( x, y );
			refreshContentSize();
		} else
			super.onPointerDragged( x, y );

	}


	public void onPointerPressed( int x, int y ) {
		if ( browsing ) {
			if ( content.contains( x, y ) ) {
				dragging = true;
				delta.set( x,y );
			} else {
				keyPressed( ScreenManager.KEY_FIRE );
				keyReleased( ScreenManager.KEY_FIRE );
			}
		} else {
			if ( content.contains( x, y ) ) {
				keyPressed( ScreenManager.KEY_FIRE );
				keyReleased( ScreenManager.KEY_FIRE );
			} else
				super.onPointerPressed( x, y );
		}
		refreshContentSize();
	}


	public void onPointerReleased( int x, int y ) {
		super.onPointerReleased( x,y );
		dragging = false;
		refreshContentSize();
	}
//#endif

	/**
	 *
	 * @param g
	 */
	public void drawFrontLayer( Graphics g ) {
	}
}
