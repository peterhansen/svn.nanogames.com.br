/**
 * MainMenu.java
 *
 * Created on Sep 21, 2010 12:02:13 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.ImageLoader;
import core.UI.Constants;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif
	
/**
 *
 * @author peter, ygor
 */
public final class MainMenu extends BasicScreen implements Constants, KeyListener, ScreenListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{
	private static final int ANIM_TOTAL_TIME = 666;

	private static boolean isPortrait;

	private DrawableImage logo;
	private final DrawableImage logoOff;

	private static DrawableImage[] CARDS;

	private static final byte CARDS_IMAGES_TOTAL = 7;

	private final DrawableImage[] cards;

	private static final BezierCurve bezier = new BezierCurve();
	private static int accTime = ANIM_TOTAL_TIME;
	private static int width;

	private final Mutex cardMutex = new Mutex();

	//#if DEBUG == "true"
 		private static short firstIndex = 4;
	//#elif BLACKBERRY_API == "true"
//# 		private static short firstIndex = 2;
	//#else
//# 	private static short firstIndex = 3;
	//#endif
	private static short lastIndex = -1;


	public MainMenu( int screen, int[] entries ) throws Exception {
		super( screen, entries, null );

		
		if ( CARDS == null && !GameMIDlet.isLowMemory() ) {
			CARDS = new DrawableImage[ CARDS_IMAGES_TOTAL ];

			final String path = PATH_MENU + "menu_";
			for ( byte i = 0; i < CARDS_IMAGES_TOTAL; ++i )
				CARDS[ i ] = new DrawableImage( path + i + ".png" );
		}

		isPortrait = false;

		// nao usa availableWidth ou Height pois isPortrait e falso
		if ( !GameMIDlet.isLowMemory() ) {
			cards = new DrawableImage[ entries.length ];
			loadCards( 0, 0 );
		} else
			cards = null;
			
		logo = new DrawableImage( getLogoOn() );
		insertDrawable( logo );

		logoOff = new DrawableImage( PATH_MENU + "logo_0.png" );
		logoOff.setVisible( false );
		insertDrawable( logoOff );

		setCurrentIndex( lastIndex < 0? firstIndex: lastIndex );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );
		this.width = width;

		logo.setPosition( ( ( width - logo.getWidth() ) >> 1 ), 0 );
		logoOff.setPosition( logo.getPosition() );

//#if SCREEN_SIZE == "BIG_BLACKBERRY"
//# 		if ( !( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
//# 			isPortrait = true;
//#else
		if( isPortrait ^ width > height ) {
			isPortrait = !isPortrait;
//#endif

			// recalculando largura e alturas para que a escala seja uniforme

			//#if SCREEN_SIZE == "GIANT"
//# 				final int scaledCardHeight = ( ( boardBottom.getPosY() - 40 - ( logo.getPosY() + ( logo.getHeight() >> 1 ) ) ) * 6 ) / 10;
			//#elif SCREEN_SIZE == "BIG_BLACKBERRY"
//#
//# 				final int scaledCardHeight = ( ( boardBottom.getPosY() - 40 - ( logo.getPosY() + ( logo.getHeight() >> 1 ) ) ) * 6 ) / 10;
//#
			//#else
				final int scaledCardHeight = ( ( boardBottom.getPosY() - ( logo.getPosY() + ( logo.getHeight() >> 1 ) ) ) * 6 ) / 10;
			//#endif

			if ( !GameMIDlet.isLowMemory() ) {
				final int scaledCardWidth = ( CARDS[ 0].getWidth() * scaledCardHeight ) / CARDS[ 0].getHeight();
				loadCards( scaledCardWidth, scaledCardHeight );
			}
		}
		setCurrentIndex( currentIndex );
	}
	
	
	public void load() {
		setCurrentIndex( currentIndex );
	}


	public void draw( Graphics g ) {
		super.draw( g );

		cardMutex.acquire();
		if ( cards != null ) {
			final int temp = NanoMath.toInt( ( currentIndexFP() / 100 ) + NanoMath.HALF ) ;
			for( int i = cards.length; i > 0; i-- ) {
				if( temp - i >= 0 )
					if( cards[ temp - i ] != null )
						cards[ temp - i ].draw( g );
				if( temp + i < cards.length )
					if( cards[ temp + i ] != null )
						cards[ temp + i ].draw( g );
			}

			if( temp >= 0 && temp < cards.length )
				if( cards[ temp ] != null )
					cards[ temp ].draw( g );
		}
		cardMutex.release();
	}
	
	
	private static int currentIndexFP() {
		Point p = new Point();
		bezier.getPointAtFixed( p, NanoMath.divInt( accTime, ANIM_TOTAL_TIME ) );
		return NanoMath.toFixed( p.x );
	}


	public void showNotify( boolean deviceEvent ) {
		MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}


	public void hideNotify( boolean deviceEvent ) {
		if ( deviceEvent )
			MediaPlayer.stop();
	}
	
	
	public final void update( int delta ) {
		super.update( delta );

		// atualizando logo
		logo.setVisible( sexyHot.getFrameSequenceIndex() == 1 );
		logoOff.setVisible( sexyHot.getFrameSequenceIndex() == 0 );
		
		if ( accTime < ANIM_TOTAL_TIME ) {
			accTime += delta;

			if( accTime > ANIM_TOTAL_TIME )
				accTime = ANIM_TOTAL_TIME;

			final int y = logo.getPosY() + logo.getHeight();
			final int x = NanoMath.toFixed( width ) >> 1;

			if( cards != null && cards.length > 0 ) {
				cardMutex.acquire();

				// assume que todas as cartas tem o mesmo tamanho
				final int cardMargin = cards[0].getWidth() >> 4;
				final int numberOfCards = width / ( cards[0].getWidth() + cardMargin );

				for( int i = 0; i < ( ( cards != null ) ? cards.length : 0 ); i++ ) {
					if( cards[i] != null ) {

						final int t = ( ( NanoMath.toFixed(i) - currentIndexFP()/ 100 ) );
						cards[i].setPosition( NanoMath.toInt( ( x + NanoMath.toFixed( cardMargin ) + ( t * width ) / numberOfCards ) - ( NanoMath.toFixed( cards[i].getWidth() ) >> 1 ) ),
								y + ( ( ( 95 * ( boardBottom.getPosY() - y ) ) - ( 100 * cards[i].getHeight() ) ) >> 1 ) / 100 );
					}
				}
				cardMutex.release();
			}
		}
	}


	protected final void setCurrentIndex( int index ) {
		super.setCurrentIndex( index );
		lastIndex = ( short ) index;

		bezier.origin.set( NanoMath.toInt(currentIndexFP()), 0 );
		accTime = 0;
		bezier.destiny.set( currentIndex * 100 , 0 );

		bezier.control1.set( bezier.destiny );
		bezier.control2.set( bezier.destiny );
	}

	private void loadCards( int availableWidth, int availableHeight ) {
		if ( cards != null ) {
			if( !isPortrait ) {
				for ( byte i = 0; i < cards.length; ++i ) {
					cards[ i ] = new DrawableImage( CARDS[ i % CARDS_IMAGES_TOTAL ] );
					cards[ i ].setPosition( -5000, -5000 );
				}
			} else {
				for ( byte i = 0; i < cards.length; ++i ) {
					cards[ i ] = new DrawableImage( ImageLoader.scaleImage( cards[ i ].getImage(), availableWidth, availableHeight) );
					cards[ i ].setSize( availableWidth, availableHeight);
					cards[ i ].setPosition( -5000, -5000 );
				}
			}
		}
		///faz o motokey quebrar
		if ( GameMIDlet.isLowMemory() ) {
			CARDS = null;
			System.gc();
		}
	}

	public static void unload() {
		GameMIDlet.log( "--Before MainMenu Unload--" );
		// TODO desalocar recursos da memória
		CARDS = null;
//		entries = null;
//		label = null;
//		boardBottom = null;
//		arrowLeft = null;
//		arrowRight = null;
		GameMIDlet.log( "--After MainMenu Unload--" );
	}


	public final void sizeChanged(int width, int height) {
		setSize( width, height );
	}

	
	public final void drawFrontLayer(Graphics g) {
	}

	//#if TOUCH == "true"
		public final void onPointerDragged(int x, int y) {
		}


		public final void onPointerPressed(int x, int y) {
			super.onPointerPressed( x, y );

			if ( selectionID == SELECTED_CONTENT ) {
				if( currentIndex >= 0 && currentIndex < cards.length ) {
					if( cards[ currentIndex ] != null ) {
						if( cards[ currentIndex ].contains( x, y ) ) {
							selectionID = SELECTED_CONTENT;
							return;
						}
					}
				}

				for( int i = 0; i < cards.length; i++ ) {
					if( currentIndex - i >= 0 ) {
						if( cards[ currentIndex - i ] != null ){
							if( cards[ currentIndex - i ].contains( x, y ) ) {
								selectionID = SELECTED_CONTENT;
								return;
							}
						}
					}
					if( currentIndex + i < cards.length ) {
						if( cards[ currentIndex + i ] != null ){
							if( cards[ currentIndex + i ].contains( x, y ) ) {
								selectionID = SELECTED_CONTENT;
								return;
							}
						}
					}
				}
			}
		}


		public final void onPointerReleased(int x, int y) {
			if ( selectionID == SELECTED_CONTENT ) {
				if( currentIndex >= 0 && currentIndex < cards.length ) {
					if( cards[ currentIndex ] != null ) {
						if( selectionID == SELECTED_CONTENT ) {
							if( cards[ currentIndex ].contains( x, y ) ) {
								activateEntryAction();
							}
						}
					}
				}

				for( int i = 0; i < cards.length; i++ ) {
					if( currentIndex - i >= 0 ) {
						if( cards[ currentIndex - i ] != null ){
							if( selectionID == SELECTED_CONTENT ) {
								if( cards[ currentIndex - i ].contains( x, y ) ) {
									setCurrentIndex( currentIndex - i );
								}
							}
						}
					}
					if( currentIndex + i < cards.length ) {
						if( cards[ currentIndex + i ] != null ){
							if( selectionID == SELECTED_CONTENT ) {
								if( cards[ currentIndex + i ].contains( x, y ) ) {
									setCurrentIndex( currentIndex + i );
								}
							}
						}
					}
				}
			}
			super.onPointerReleased( x, y );
			
		}
	//#endif
}
