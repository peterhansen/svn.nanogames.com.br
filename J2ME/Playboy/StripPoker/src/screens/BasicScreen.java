package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import core.UI.Constants;
import java.util.Hashtable;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif


/**
 *
 * @author Caio
 */
public abstract class BasicScreen extends UpdatableGroup implements Constants, ScreenListener, KeyListener
//#if TOUCH == "true"
		, PointerListener
//#endif
{


	protected final int screenID;
	protected final MarqueeLabel screenTitle;
	private final Pattern titlePattern;
	private final UpdatableGroup titleGroup;
	protected final boolean hasTitle;
	protected final Sprite sexyHot;
	protected final Pattern boardBottom;
	private final MarqueeLabel label;
	protected final Rectangle bkgArea = new Rectangle();
	private static DrawableImage LOGO;
	protected int selectionID;
	private final Hashtable hashText = new Hashtable();
	private final Sprite arrowRight;
	private final Sprite arrowLeft;
	protected static final byte SELECTED_NONE = 0;
	protected static final byte SELECTED_ARROW_LEFT = 1;
	protected static final byte SELECTED_ARROW_RIGHT = 2;
	protected static final byte SELECTED_TEXT = 3;
	protected static final byte SELECTED_CONTENT = 4;
	private int[] entries;
	protected short currentIndex = -1;
	/** índice atualmente selecionado no menu */
	private boolean circular;
	/** indica se o menu é circular */
	//#if TOUCH == "true"
	private Rectangle arrowLeftArea;
	private Rectangle arrowRightArea;
	//#endif


	public BasicScreen( int screen, int[] entries, String title ) throws Exception {
		super( 20 );

		screenID = screen;
		this.hasTitle = title != null;
		this.entries = entries;

		if ( entries != null ) {
			for ( int i = 0; i < entries.length; i++ ) {
				hashText.put( new Integer( entries[i] ), new Integer( i ) );
				//#if DEBUG == "true"
				System.out.println( "hashText.put( " + entries[i] + ", " + i + " )" );
				//#endif
			}
		}

		final DrawableImage img = new DrawableImage( PATH_IMAGES + "menu_pattern.png" );

		boardBottom = new Pattern( img );
		insertDrawable( boardBottom );

		sexyHot = new Sprite( PATH_MENU + "sexyhot" );
		sexyHot.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
		insertDrawable( sexyHot );

		arrowRight = new Sprite( PATH_MENU + "arrow_h" );
		arrowRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( arrowRight );

		arrowLeft = new Sprite( arrowRight );
		arrowLeft.mirror( TRANS_MIRROR_H );
		arrowLeft.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		insertDrawable( arrowLeft );

		label = new MarqueeLabel( FONT_MENU, null );
		insertDrawable( label );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );

		titleGroup = new UpdatableGroup( 1 );
		if ( hasTitle ) {
			final DrawableImage img2 = new DrawableImage( img );
			img2.mirror( TRANS_MIRROR_V );
			titlePattern = new Pattern( img2 );
			titlePattern.setSize( Short.MAX_VALUE, MENU_BOTTOM_BAR_HEIGHT );
			insertDrawable( titlePattern );

			screenTitle = new MarqueeLabel( FONT_MENU, title );
			screenTitle.setSize( 0, screenTitle.getFont().getHeight() );
			screenTitle.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			screenTitle.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			screenTitle.defineReferencePixel( ANCHOR_CENTER );
			titleGroup.insertDrawable( screenTitle );
		} else {
			titleGroup.insertDrawable( getLogoOn() );

			titlePattern = null;
			screenTitle = null;
		}

		insertDrawable( titleGroup );
	}


	public final void setTitle( String text ) {
		screenTitle.setText( text );
		screenTitle.setTextOffset( ( screenTitle.getWidth() - screenTitle.getTextWidth() ) >> 1 );
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		bkgArea.x = getPosX();
		bkgArea.width = width;

		if ( hasTitle ) {
			screenTitle.setPosition( SAFE_MARGIN, SAFE_MARGIN );
			screenTitle.setSize( width - ( SAFE_MARGIN << 1 ), screenTitle.getHeight() );
			screenTitle.setTextOffset( ( screenTitle.getWidth() - screenTitle.getTextWidth() ) >> 1 );

			titleGroup.setSize( width, screenTitle.getHeight() + TEXT_TITLE_MARGIN );
			bkgArea.y = titleGroup.getHeight();
		} else {
			bkgArea.y = 0;
		}

		boardBottom.setSize( width, MENU_BOTTOM_BAR_HEIGHT );
		boardBottom.setPosition( 0, height - boardBottom.getHeight() );

		bkgArea.height = boardBottom.getPosY() - bkgArea.y;

		sexyHot.setRefPixelPosition( width >> 1, boardBottom.getPosY() + SEXY_HOT_MENU_BAR_OFFSET_Y );

		arrowLeft.setRefPixelPosition( SAFE_MARGIN, boardBottom.getPosY() + ( boardBottom.getHeight() >> 1 ) );
		arrowRight.setRefPixelPosition( width - SAFE_MARGIN, boardBottom.getPosY() + ( boardBottom.getHeight() >> 1 ) );

		//#if TOUCH == "true"
		arrowLeftArea = new Rectangle();
		arrowRightArea = new Rectangle();

		arrowLeftArea.set(	arrowLeft.getPosX(),
							arrowLeft.getPosY() - 50,
							arrowLeft.getSize().x + 20,
							arrowLeft.getSize().y + 50 );


		arrowRightArea.set(  arrowRight.getPosX() - 20,
							arrowRight.getPosY() - 50,
							arrowRight.getSize().x + 20,
							arrowRight.getSize().y + 50 );
		//#endif

		label.setSize( boardBottom.getWidth() - ( ( SAFE_MARGIN + arrowRight.getWidth() ) << 1 ), label.getHeight() );
		label.setPosition( SAFE_MARGIN + ( arrowRight.getWidth() ),
						   boardBottom.getPosY() + ( ( boardBottom.getHeight() - label.getHeight() ) >> 1 ) );

		updateEntriesText();

//		GameMIDlet.setBkgViewport( new Rectangle( bkgArea.x, bkgArea.y, bkgArea.width, bkgArea.height ) );
	}


	public void exit() {
		( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( null, screenID, TEXT_EXIT );
	}


	public void draw( Graphics g ) {
		super.draw( g );

//		titleGroup.draw( g );

		drawFrontLayer( g );
	}


	public abstract void drawFrontLayer( Graphics g );


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public static final DrawableImage getLogoOn() throws Exception {
		if ( LOGO == null ) {
			LOGO = new DrawableImage( PATH_MENU + "logo_1.png" );
		}

		return LOGO;
	}


	public void setEntry( int textId ) {
		//#if DEBUG == "true"
		System.out.println( "setEntry(" + textId + ")" );
		//#endif

		final Integer i = ( Integer ) hashText.get( new Integer( textId ) );
		if ( i != null ) {
			//#if DEBUG == "true"
			System.out.println( "setCurrentIndex(" + i + ")" );
			//#endif

			setCurrentIndex( i.intValue() );
		}
	}


	protected void setCurrentIndex( int index ) {
		if ( entries != null && index >= 0 && index < entries.length ) {
			currentIndex = ( short ) index;
			arrowLeft.setVisible( index > 0 );
			arrowRight.setVisible( index < ( entries.length - 1 ) );
			updateEntriesText();
		}
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				arrowLeft.setFrame( 1 );
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				arrowRight.setFrame( 1 );
				break;
		}
	}


	public void keyReleased( int key ) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				activateEntryAction();
				break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				previousIndex();
				arrowLeft.setFrame( 0 );
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				nextIndex();
				arrowRight.setFrame( 0 );
				break;
		}
	}


	protected void activateEntryAction() {
		( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( null, screenID, entries[currentIndex] );
	}


	private void nextIndex() {
		if ( entries != null ) {
			if ( currentIndex < ( entries.length - 1 ) ) {
				setCurrentIndex( currentIndex + 1 );
			} else if ( circular ) {
				setCurrentIndex( 0 );
			}
		}
	}


	private void previousIndex() {
		if ( entries != null ) {
			if ( currentIndex > 0 ) {
				setCurrentIndex( currentIndex - 1 );
			} else if ( circular ) {
				setCurrentIndex( entries.length - 1 );
			}
		}
	}


	/**
	 * Atualiza o texto e o posicionamento do label de texto do menu.
	 */
	protected void updateEntriesText() {
		if ( label != null && entries != null && currentIndex >= 0 && currentIndex < entries.length ) {
			final Point p = new Point( label.getSize() );
			label.setText( entries[currentIndex] );
			label.setSize( p );
			label.setTextOffset( ( p.x - label.getTextWidth() ) >> 1 );
		}
	}

	//#if TOUCH == "true"

	public void onPointerDragged( int x, int y ) {
		if ( !arrowRightArea.contains( x, y ) ) {
			arrowRight.setFrame( 0 );
		}
		if ( !arrowLeftArea.contains( x, y ) ) {
			arrowLeft.setFrame( 0 );
		}
	}


	public void onPointerPressed( int x, int y ) {
		if ( arrowRightArea.contains( x, y ) ) {
			arrowRight.setFrame( 1 );
			selectionID = SELECTED_ARROW_RIGHT;
			return;
		}
		arrowRight.setFrame( 0 );

		if ( arrowLeftArea.contains( x, y ) ) {
			arrowLeft.setFrame( 1 );
			selectionID = SELECTED_ARROW_LEFT;
			return;
		}
		arrowLeft.setFrame( 0 );

		/*if ( arrowRightArea.contains( x, y ) ) {
			arrowRight.setFrame( 1 );
			selectionID = SELECTED_ARROW_RIGHT;
			return;
		}

		if ( arrowLeftArea.contains( x, y ) ) {
			arrowLeft.setFrame( 1 );
			selectionID = SELECTED_ARROW_LEFT;
			return;
		}*/

		if ( label.contains( x, y ) ) {
			selectionID = SELECTED_TEXT;
			return;
		}

		selectionID = SELECTED_CONTENT;
	}


	public void onPointerReleased( int x, int y ) {
		if ( arrowRightArea.contains( x, y ) ) {
			if ( selectionID == SELECTED_ARROW_RIGHT ) {
				nextIndex();
			}
			arrowRight.setFrame( 0 );
		} else if ( arrowLeftArea.contains( x, y ) ) {
			if ( selectionID == SELECTED_ARROW_LEFT ) {
				previousIndex();
			}
			arrowLeft.setFrame( 0 );
		} else if ( label.contains( x, y ) ) {
			if ( selectionID == SELECTED_TEXT ) {
				activateEntryAction();
			}
		}
		selectionID = SELECTED_NONE;
	}
	//#endif

	public int getTitleHeight() {
		return titleGroup.isVisible() ? titleGroup.getHeight() : 0;
	}

	public int getArrowsGroupHeight() {
		return boardBottom.isVisible() ? boardBottom.getHeight() : 0;
	}

	/**
	 * Determina se as bordas da view estarão visiveis ou não.
	 * @param b
	 */
	void showBorders( boolean b ) {
		boardBottom.setVisible( b );
		titleGroup.setVisible( b );
		LOGO.setVisible( b );
		sexyHot.setVisible( b );
		arrowLeft.setVisible( ( b ) ? ( currentIndex != 0 ) : false );
		arrowRight.setVisible( ( b ) ? ( currentIndex < entries.length - 1 ) : false );
	}
}
