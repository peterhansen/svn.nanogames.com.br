package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import core.UI.Constants;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class MenuLabel extends UpdatableGroup implements Constants {
	protected final StateLabel state;
	protected final int textID;
	protected final Drawable title;
	protected int stateType;

	protected final Sprite arrowLeft;
	protected final Sprite arrowRight;

	public MenuLabel( int entryTitle, Sprite arrow ) throws Exception {
		super( 4 );

		textID = entryTitle;
		stateType = StateLabel.OPTIONS_STATE_NONE;

		switch( entryTitle ) {
			case TEXT_SOUND:
			case TEXT_VIBRATION:
				stateType = StateLabel.OPTIONS_STATE_ON_OF;
			break;

			case TEXT_VOLUME:
				stateType = StateLabel.OPTIONS_STATE_0_100;
			break;
		}

		final String titleText = GameMIDlet.getText( entryTitle );
		final boolean stateExist = ( stateType != StateLabel.OPTIONS_STATE_NONE );
		
		arrowLeft = new Sprite( arrow );
		arrowLeft.setTransform( TRANS_MIRROR_H );
		arrowLeft.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_VCENTER );
		arrowRight = new Sprite( arrow );
		arrowRight.defineReferencePixel( ANCHOR_LEFT | ANCHOR_VCENTER );
		insertDrawable( arrowLeft );
		insertDrawable( arrowRight );

		title = new MarqueeLabel( GameMIDlet.GetFont( FONT_TITLE ), titleText );
		( (MarqueeLabel)title ).setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
		insertDrawable( title );

		if( stateExist ) {
			state = new StateLabel( stateType, entryTitle );
			insertDrawable( state );
		} else {
			state = null;
		}

		setSize( ( stateExist ?
			NanoMath.max( title.getWidth(), ( state.getWidth() + arrowLeft.getWidth() + arrowRight.getWidth() + ( SAFE_MARGIN << 1 ) )  ) :
			( title.getWidth() + arrowLeft.getWidth() + arrowRight.getWidth() + ( SAFE_MARGIN << 1 ) ) ) ,
					( title.getHeight() + ( stateExist ? SAFE_MARGIN + state.getHeight() : 0 ) ) );
	}
	
	
	public int id() {
		return textID;
	}


	public final void changeState() {
		if( state != null ) {
			state.changeState();
		}
	}
	
	
	public boolean hasState() {
		return state != null;
	}


	public void select() {
		arrowLeft.setVisible( true );
		arrowRight.setVisible( true );
		if( state != null ) {
			arrowLeft.setRefPixelPosition( state.getPosX() - SAFE_MARGIN, state.getPosY() + ( state.getHeight() >> 1 ) );
			arrowRight.setRefPixelPosition( state.getPosX() + SAFE_MARGIN + state.getWidth(), state.getPosY() + ( state.getHeight() >> 1 ) );
		} else {
			arrowLeft.setRefPixelPosition( title.getPosX() - SAFE_MARGIN, title.getPosY() + ( title.getHeight() >> 1 ) );
			arrowRight.setRefPixelPosition( title.getPosX() + SAFE_MARGIN + title.getWidth(), title.getPosY() + ( title.getHeight() >> 1 ) );
		}
	}


	public void unselect() {
		arrowLeft.setVisible( false );
		arrowRight.setVisible( false );
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		//#if DEBUG == "true"
			System.out.println( "MenuLabel.setSize( " + width + ", " + height + " );" );
		//#endif

		title.setPosition( ( getWidth() - ( title.getWidth() ) ) >> 1, 0 );

		if( state != null ) {
			state.setPosition( ( getWidth() - ( state.getWidth() ) ) >> 1 , ( getHeight() - state.getHeight() ) );
		}
	}
}