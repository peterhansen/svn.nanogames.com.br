/**
 * GameMIDlet.java
 * ©2011 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */
package screens;
//<editor-fold desc="Imports">
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Serializable;
import core.UI.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import br.com.nanogames.components.util.Rectangle;
import java.util.Hashtable;
import core.PersistentGameInformation;
import core.PokerPlayer;
import core.UI.Background;
//#if NANO_RANKING == "true"
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.userInterface.form.Form;
//#endif
//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif
//</editor-fold>


/**
 * 
 * @author Daniel "Monty" Monteiro
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable
//#if NANO_RANKING == "true"
		, ConnectionListener, RankingFormatter
//#endif
{


	private static final short GAME_MAX_FRAME_TIME = 180;
	private static final byte BACKGROUND_TYPE_CLOTH_TEXTURE = 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;
	private static final byte BACKGROUND_TYPE_NONE = 2;
	public static final byte OPTION_ENGLISH = 0;
	public static final byte OPTION_PORTUGUESE = 1;
	public static final byte OPTION_PLAY_SOUD = 0;
	public static final byte OPTION_NO_SOUND = 1;
	public static final byte CONFIRM_YES = 0;
	public static final byte CONFIRM_NO = 1;
	public static final byte CONFIRM_CANCEL = 2;
	//#if DEBUG == "true"
    	public static StringBuffer log = new StringBuffer();
	//#endif
        
	//#if NANO_RANKING == "true"
    	private Form nanoOnlineForm;
	//#endif
        
	// Referência para a tela de jogo, para que seja possível retornar à tela de jogo apÃ³s entrar na tela de pausa.
	private GameScreen gameScreen;

	// Variavel para armazenamento de tela a ser lida
	private Drawable screenToLoad;
	private static boolean lowMemory;
	private static Background bkg;

	static {
		PersistentGameInformation.DEFAULT_START_LEVEL = 0;
		PersistentGameInformation.DEFAULT_TRIES_LEFT = PokerPlayer.CLOTHING_BOB_FULL - 1;
		PersistentGameInformation.DEFAULT_POINTS = INITIAL_CASH_PER_PLAYER;
		PersistentGameInformation.DEFAULT_PLAYER = PLAYER_BARBARA;
	}


	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
		super( -1, GAME_MAX_FRAME_TIME );
		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
		//#endif

		log( "GameMIDlet antes" );
	}


	/***
	 * Cria o mapeamento de teclas para aparelhos com teclado QWERTY
	 * @param specialMapping
	 */
	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		try {
			ScreenManager.resetSpecialKeysTable();
			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

			int[][] keys = null;
			final int offset = 'a' - 'A';

			//#if BLACKBERRY_API == "true"
//# 						switch ( Keypad.getHardwareLayout() ) {
//# 							case ScreenManager.HW_LAYOUT_REDUCED:
//# 							case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 								keys = new int[][] {
//# 									{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 									{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 									{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 									{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 									{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 									{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 									{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 									{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//# 
//# 									{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 									{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 									{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 									{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 									{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 									{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 									{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 									{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 									{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 									{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 									{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 									{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 									{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 									{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 									{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 									{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 									{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 									{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//# 
//# 									{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 								 };
//# 							break;
//# 
//# 							default:
//# 								if ( specialMapping ) {
//# 									keys = new int[][] {
//# 										{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 										{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 										{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 										{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 										{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 										{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 										{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 										{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 										{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//# 
//# 										{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 										{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 										{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 										{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 										{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 										{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 										{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 										{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 										{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//# 
//# 										{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 										{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 									 };
//# 								} else {
//# 									for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 										table.put( new Integer( c ), new Integer( c ) );
//# 										table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 									}
//# 
//# 									final int[] chars = new int[]
//# 									{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 										'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 									};
//# 
//# 									for ( byte i = 0; i < chars.length; ++i )
//# 										table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 								}
//# 						}
//# 
			//#else

			if ( specialMapping ) {
				keys = new int[][]{
							{ 'q', ScreenManager.KEY_NUM1 },
							{ 'Q', ScreenManager.KEY_NUM1 },
							{ 'e', ScreenManager.KEY_NUM3 },
							{ 'E', ScreenManager.KEY_NUM3 },
							{ 'z', ScreenManager.KEY_NUM7 },
							{ 'Z', ScreenManager.KEY_NUM7 },
							{ 'c', ScreenManager.KEY_NUM9 },
							{ 'C', ScreenManager.KEY_NUM9 },
							{ 'w', ScreenManager.UP },
							{ 'W', ScreenManager.UP },
							{ 'a', ScreenManager.LEFT },
							{ 'A', ScreenManager.LEFT },
							{ 's', ScreenManager.FIRE },
							{ 'S', ScreenManager.FIRE },
							{ 'd', ScreenManager.RIGHT },
							{ 'D', ScreenManager.RIGHT },
							{ 'x', ScreenManager.DOWN },
							{ 'X', ScreenManager.DOWN },
							{ 'r', ScreenManager.KEY_NUM1 },
							{ 'R', ScreenManager.KEY_NUM1 },
							{ 'y', ScreenManager.KEY_NUM3 },
							{ 'Y', ScreenManager.KEY_NUM3 },
							{ 'v', ScreenManager.KEY_NUM7 },
							{ 'V', ScreenManager.KEY_NUM7 },
							{ 'n', ScreenManager.KEY_NUM9 },
							{ 'N', ScreenManager.KEY_NUM9 },
							{ 't', ScreenManager.KEY_NUM2 },
							{ 'T', ScreenManager.KEY_NUM2 },
							{ 'f', ScreenManager.KEY_NUM4 },
							{ 'F', ScreenManager.KEY_NUM4 },
							{ 'g', ScreenManager.KEY_NUM5 },
							{ 'G', ScreenManager.KEY_NUM5 },
							{ 'h', ScreenManager.KEY_NUM6 },
							{ 'H', ScreenManager.KEY_NUM6 },
							{ 'b', ScreenManager.KEY_NUM8 },
							{ 'B', ScreenManager.KEY_NUM8 },
							{ 10, ScreenManager.FIRE }, // ENTER
							{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

							{ 'u', ScreenManager.KEY_STAR },
							{ 'U', ScreenManager.KEY_STAR },
							{ 'j', ScreenManager.KEY_STAR },
							{ 'J', ScreenManager.KEY_STAR },
							{ '#', ScreenManager.KEY_STAR },
							{ '*', ScreenManager.KEY_STAR },
							{ 'm', ScreenManager.KEY_STAR },
							{ 'M', ScreenManager.KEY_STAR },
							{ 'p', ScreenManager.KEY_STAR },
							{ 'P', ScreenManager.KEY_STAR },
							{ ' ', ScreenManager.KEY_STAR },
							{ '$', ScreenManager.KEY_STAR }, };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]{ ' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
											   '_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i ) {
					table.put( new Integer( chars[i] ), new Integer( chars[i] ) );
				}
			}
			//#endif

			if ( keys != null ) {
				for ( byte i = 0; i < keys.length; ++i ) {
					table.put( new Integer( keys[i][ 0] ), new Integer( keys[i][ 1] ) );
				}
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			log( e.getClass() + e.getMessage() );
			e.printStackTrace();
			//#endif
		}
	}

	//#if DEBUG == "true"

	public final void start() {
		log( "start 1" );
		super.start();
		log( "start 2" );
	}
	//#endif


	/***
	 * Carrega os recursos do jogo
	 * @throws Exception
	 */
	protected final void loadResources() throws Exception {
		log( "loadResources início" );
		NanoOnline.init( NanoOnline.LANGUAGE_pt_BR, APP_SHORT_NAME );

		//<editor-fold desc="VERIFICAÇÃO DE MEMÓRIA TOTAL DISPONÍVEL">
		lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
//			lowMemory = true; // TODO teste
		//</editor-fold>


		//<editor-fold desc="LOADING FONTS">

		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
			switch ( i ) {
//				//#if SCREEN_SIZE == "SMALL"
//						case 3:
//							FONTS[ i ] = FONTS[ 2 ];
//						break;
//				//#endif
				default:
					FONTS[i] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
			}

			switch ( i ) {
				//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "SMALL"
				case FONT_TEXT:
					FONTS[i].setCharExtraOffset( 1 );
					break;
				//#endif

				case FONT_TITLE:
					FONTS[i].setCharExtraOffset( 0 );
					break;

				//#if SCREEN_SIZE == "SMALL"
//#  					case FONT_NUMBERS:
//#  						FONTS[i].setCharExtraOffset( 1 );
//#  					break;
				//#endif

				default:
					//#if SCREEN_SIZE == "SMALL"
//#  							FONTS[i].setCharExtraOffset( -1 );
					//#else
					FONTS[i].setCharExtraOffset( DEFAULT_FONT_OFFSET );
					//#endif
				}

			log( "FONTE " + i );
		}
		//</editor-fold>

		log( "database 0" );

		//#if NANO_RANKING == "true"
		NanoOnline.init( NanoOnline.LANGUAGE_pt_BR, APP_SHORT_NAME );
		NanoOnline.setRankingTypes( NanoOnline.RANKING_TYPES_GLOBAL_ONLY );
		RankingScreen.init( new int[]{
					TEXT_RANKING_ACCUMULATED_POINTS,
					TEXT_RANKING_AVERAGE,
				}, this );
		log( "NanoOnline" );
		//#endif

		//<editor-fold desc="DATABASE Creation">
		log( "creating database" );
		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Throwable t ) {
			//#if DEBUG == "true"
			t.printStackTrace();
			//#endif
		}
		log( "created database" );
		//</editor-fold>

		loadOptions();

		setLanguage( ( byte ) 2 ); // NanoOnline.LANGUAGE_pt_BR
		log( "textos" );

		bkg = new Background();
		log( "bkgPattern" );

		setSpecialKeyMapping( true );
		log( "loadResources fim" );

		// aloca os sons
		final String[] soundList = new String[SOUND_TOTAL];
		for (byte i = 0; i < SOUND_TOTAL; ++i) {
			soundList[i] = PATH_SOUNDS + i + ".mid";
		}

		MediaPlayer.init(DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList);

		setScreen( SCREEN_CHOOSE_SOUND );
	} // fim do método loadResources()


	/***
	 * Carrega um novo idioma
	 * @param language Código do idioma. Requer arquivo texts_[n].dat em res
	 */
	protected final void changeLanguage( byte language ) {
		GameMIDlet.log( "changeLanguage início" );
		language = 2;// NanoOnline.LANGUAGE_pt_BR;
		try {
			loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts_" + language + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
		GameMIDlet.log( "changeLanguage fim" );
	}


	/**
	 * Destrutor
	 */
	public final void destroy() {
//		saveOptions();
		//MainMenu.unload();

		if ( gameScreen != null ) {
			onGameOver( gameScreen );
			gameScreen = null;
		}

		super.destroy();
	}


	/**
	 * Salva o estado atual de opções e dados de jogo para proxima sessão
	 */
	public final void saveOptions() {
		try {
			AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA, this );
		} catch ( Exception e ) {
			//#if DEBUG_WRITE_ERROR == "true"
//# 			log( "save fail: " + e.getMessage() );
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do método saveOptions()


	public final void loadOptions() {
		try {
			AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA, this );
		} catch ( Exception e ) {
			//#if DEBUG_LOAD_ERROR == "true"
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do método loadOptions()


	/**
	 * Troca de tela do jogo
	 * @param screen Id da tela
	 * @return Id da tela
	 * @throws Exception
	 */
	protected final int changeScreen( int screen ) throws Exception {
		MediaPlayer.saveOptions();
		MediaPlayer.free();
		System.gc();
		boolean playMusic = true;

		final GameMIDlet midlet = ( GameMIDlet ) instance;

		Drawable nextScreen = null;
		log( "changeScreen " + screen );


		//#if DEBUG == "true"
		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				log( "changedScreen CHOOSE_SOUND" );
				break;
			case SCREEN_LOADING_1:
				log( "changedScreen LOADING_1" );
				break;
			case SCREEN_SPLASH_NANO:
				log( "changedScreen SPLASH_NANO" );
				break;
			case SCREEN_SPLASH_BRAND:
				log( "changedScreen SPLASH_BRAND" );
				break;
			case SCREEN_SPLASH_GAME:
				log( "changedScreen SPLASH_GAME" );
				break;
			case SCREEN_NEW_RECORD_MENU:
				log( "changedScreen NEW_RECORD_MENU" );
				break;
			case SCREEN_GAME_OVER_MENU:
				log( "changedScreen GAME_OVER_MENU" );
				break;
			case SCREEN_PAUSE:
				log( "changedScreen PAUSE" );
				break;
			case SCREEN_OPTIONS:
				log( "changedScreen OPTIONS" );
				break;
			case SCREEN_MAIN_MENU:
				log( "changedScreen MAIN_MENU" );
				break;
			case SCREEN_LOG:
				log( "changedScreen LOG" );
				break;
			case SCREEN_HELP:
				log( "changedScreen HELP" );
				break;
			case SCREEN_RECOMMEND_SENT:
				log( "changedScreen RECOMMEND_SENT" );
				break;
			case SCREEN_RECOMMEND:
				log( "changedScreen RECOMMEND" );
				break;
			case SCREEN_LOADING_RECOMMEND_SCREEN:
				log( "changedScreen LOADING_RECOMMEND_SCREEN" );
				break;
			case SCREEN_CHOOSE_PROFILE:
				log( "changedScreen CHOOSE_PROFILE" );
				break;
			case SCREEN_LOADING_GAME:
				log( "changedScreen LOADING_GAME" );
				break;
			case SCREEN_NEW_GAME:
				log( "changedScreen NEW_GAME" );
				break;
			case SCREEN_NEXT_LEVEL:
				log( "changedScreen NEXT_LEVEL" );
				break;
			case SCREEN_LOADING_PHOTO_GALLERY:
				log( "changedScreen LOADING_PHOTO_GALLERY" );
				break;
			case SCREEN_PHOTO_GALLERY:
				log( "changedScreen PHOTO_GALLERY" );
				break;
			case SCREEN_CREDITS:
				log( "changedScreen CREDITS" );
				break;
			case SCREEN_HELP_PROFILE:
				log( "changedScreen SCREEN_HELP_PROFILE" );
				break;
			case SCREEN_LOADING_PROFILES_SCREEN:
				log( "changedScreen LOADING_PROFILES_SCREEN" );
				break;
			case SCREEN_LOADING_NANO_ONLINE:
				log( "changedScreen LOADING_NANO_ONLINE" );
				break;
			case SCREEN_LOADING_HIGH_SCORES:
				log( "changedScreen LOADING_HIGH_SCORES" );
				break;
			case SCREEN_NANO_RANKING_PROFILES:
				log( "changedScreen NANO_RANKING_PROFILES" );
				break;
			case SCREEN_NANO_RANKING_MENU:
				log( "changedScreen NANO_RANKING_MENU" );
				break;

			default:
				log( "ERRO: nao foi possivel encontrar a tela " + screen );
		}
		//#endif



		byte bkgType = BACKGROUND_TYPE_CLOTH_TEXTURE;

		if ( bkg != null ) {
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}

		setSpecialKeyMapping( true );

		System.gc();
		switch ( screen ) {

			case SCREEN_NEW_GAME:
				invalidateRecord();
				System.gc();
				gameScreen = new GameScreen();
			case SCREEN_NEXT_LEVEL:
				playMusic = false;
				MediaPlayer.free();
				System.gc();
				nextScreen = gameScreen;
				break;

			case SCREEN_LOADING_1:
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				unblockBackground();
				nextScreen = new LoadScreen(
					new LoadListener() {
						public final void load(final LoadScreen loadScreen) throws Exception {
							setScreen( SCREEN_CHOOSE_SOUND );
						}
					}
				);
			break;

			case SCREEN_CHOOSE_SOUND:
				playMusic = false;
				nextScreen = new OptionsMenu( screen, TEXT_OPTIONS, OptionsMenu.OPTIONS_CHOOSE_SOUND );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;


			case SCREEN_NEW_RECORD_MENU:
				nextScreen = new OptionsMenu( screen, TEXT_NEW_RECORD, OptionsMenu.OPTIONS_NEW_RECORD );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;


			case SCREEN_GAME_OVER_MENU:
				playMusic = false;
				nextScreen = new OptionsMenu( screen, TEXT_GAME_OVER, OptionsMenu.OPTIONS_ON_ENDGAME );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;


			case SCREEN_PAUSE:
				playMusic = false;
				nextScreen = new OptionsMenu( screen, TEXT_PAUSE, OptionsMenu.OPTIONS_ON_GAME );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;


			case SCREEN_OPTIONS:
				playMusic = false;
				nextScreen = new OptionsMenu( screen, TEXT_OPTIONS, OptionsMenu.OPTIONS_ON_MAIN );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;


			case SCREEN_SPLASH_NANO:
				playMusic = false;
				//#if SCREEN_SIZE != "SMALL"
					nextScreen = new BasicSplashNano(SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM,
															PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#else
//# 				nextScreen = new BasicSplashNano(SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL,
//# 														PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#endif

				bkgType = BACKGROUND_TYPE_NONE;
			break;


			case SCREEN_LOADING_GAME:
				playMusic = false;
				MediaPlayer.free();
				unblockBackground();
				nextScreen = new LoadScreen(
						new LoadListener() {

							public final void load(final LoadScreen loadScreen) throws Exception {								
								loadScreen.setActive(false);
								setScreen( SCREEN_NEW_GAME );
							}
						}
				);
			break;

			case SCREEN_LOADING_PHOTO_GALLERY:
				playMusic = false;
				unblockBackground();
				screenToLoad = null;
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						
						int[] entries = new int[ PersistentGameInformation.getMaxLevel() + 1 ];
						entries[ entries.length - 1 ] = TEXT_BACK;
						for ( int c = 0; c < entries.length - 1; ++c ) {
							entries[c] = TEXT_PHOTO_1 + c;
						}
						

						screenToLoad = new ImageGallery( entries );
						loadScreen.setActive( false );
						setScreen( SCREEN_PHOTO_GALLERY );
					}

				} );
			break;


			case SCREEN_PHOTO_GALLERY:
				playMusic = false;
				nextScreen = screenToLoad;
//				cleanUpMemory(); TODO
//				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_SPLASH_GAME:
				playMusic = false;
				final String[] soundList = new String[ SOUND_TOTAL ];
				for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
					soundList[i] = PATH_SOUNDS + i + ".mid";
				}

				nextScreen = new SplashScreen();
			break;

//			case SCREEN_GAME_OVER: // forçando tela principal
//					screen = SCREEN_LOADING_MAIN_MENU;
			case SCREEN_LOADING_MAIN_MENU:
			case SCREEN_MAIN_MENU:
				nextScreen = new MainMenu( SCREEN_MAIN_MENU, new int[]{
							//#if DEBUG == "true"
								TEXT_LOADING,
							//#endif
								TEXT_CREDITS,
							//#if BLACKBERRY_API != "true"
								TEXT_RECOMMEND_TITLE,
							//#endif
								TEXT_NANO_ONLINE, TEXT_NEW_GAME,
							TEXT_OPTIONS , TEXT_PHOTO_GALLERY, TEXT_HELP , TEXT_EXIT  } );
			break;

			//#if DEBUG == "true"
				case SCREEN_LOG:
					nextScreen = new TextScreen( 0, this, screen, "LOG", log.toString(), false );

					if ( isLowMemory() )
						bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;
			//#endif

			case SCREEN_HELP:
				nextScreen = new TextScreen( 0, this, screen, GameMIDlet.getText( TEXT_HELP ), GameMIDlet.getText( TEXT_HELP_OBJECTIVES ) + GameMIDlet.getText( TEXT_HELP_CONTROLS ) + getVersion(), false );
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_CREDITS:
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				nextScreen = new TextScreen( 0, this, screen, GameMIDlet.getText( TEXT_CREDITS ), GameMIDlet.getText( TEXT_CREDITS_TEXT ), true );
			break;

			//#ifndef NO_RECOMMEND
				case SCREEN_RECOMMEND_SENT:
				case SCREEN_RECOMMEND_NOTSENT:
					final int contentTextId = screen == SCREEN_RECOMMEND_SENT? TEXT_RECOMMEND_SENT: TEXT_RECOMMEND_NOTSENT;
					nextScreen = new TextScreen( 0, this, screen, getText( TEXT_RECOMMENDED ), getText( contentTextId ) + getRecommendURL(), false );

					if ( isLowMemory() )
						bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;

				case SCREEN_RECOMMEND:
					nextScreen = screenToLoad;

					if ( isLowMemory() )
						bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;

				case SCREEN_LOADING_RECOMMEND_SCREEN:
					unblockBackground();
					nextScreen = new ScreenRecommend( SCREEN_RECOMMEND );
				break;
			//#endif


			case SCREEN_HELP_PROFILE:
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				nextScreen = new TextScreen( 0, this, screen, GameMIDlet.getText( TEXT_HELP_PROFILE ), GameMIDlet.getText( TEXT_HELP_PROFILE_TEXT ), false );
			break;

			
			case SCREEN_CHOOSE_PROFILE:
//				final Customer c = NanoOnline.getCurrentCustomer();
				nextScreen = new OptionsMenu( screen, TEXT_OPTIONS, OptionsMenu.OPTIONS_LOGIN );
			break;


			case SCREEN_LOADING_PROFILES_SCREEN:
				unblockBackground();
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						nanoOnlineForm = NanoOnline.load( SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_LOAD_PROFILE );

						loadScreen.setActive(false);

						setScreen( SCREEN_NANO_RANKING_PROFILES );
					}
				});
			break;


			case SCREEN_LOADING_HIGH_SCORES:
				playMusic = false;
				MediaPlayer.free();
				unblockBackground();
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						nanoOnlineForm = NanoOnline.load( SCREEN_NEW_RECORD_MENU, NanoOnline.SCREEN_NEW_RECORD );

						loadScreen.setActive(false);

						setScreen( SCREEN_NANO_RANKING_MENU );
					}
				});
			break;


			case SCREEN_LOADING_NANO_ONLINE:
				playMusic = false;
				MediaPlayer.free();
				unblockBackground();
				nextScreen = new LoadScreen(new LoadListener() {
					public final void load(final LoadScreen loadScreen) throws Exception {
						nanoOnlineForm = NanoOnline.load( SCREEN_LOADING_MAIN_MENU, NanoOnline.SCREEN_MAIN_MENU );

						loadScreen.setActive(false);

						setScreen( SCREEN_NANO_RANKING_MENU );
					}
				});
			break;


			case SCREEN_NANO_RANKING_PROFILES:
			case SCREEN_NANO_RANKING_MENU:
				playMusic = false;
				MediaPlayer.free();
				setSpecialKeyMapping(false);
				nextScreen = nanoOnlineForm;
				//#if DEBUG == "true"
					System.out.println( "setScreen( SCREEN_NANO_RANKING_PROFILES )" );
				//#endif
				bkgType = BACKGROUND_TYPE_NONE;
			break;
		} // fim switch ( screen )

		setBackground( bkgType );

		//#if DEBUG == "true"
		if ( nextScreen == null ) {
			throw new IllegalStateException( "Error: nextScreen null (" + screen + ")." );
		}
		//#endif

		midlet.manager.setCurrentScreen( nextScreen );

		//#if DEBUG == "true"
		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				log( "closedScreen CHOOSE_SOUND" );
				break;
			case SCREEN_LOADING_1:
				log( "closedScreen LOADING_1" );
				break;
			case SCREEN_SPLASH_NANO:
				log( "closedScreen SPLASH_NANO" );
				break;
			case SCREEN_SPLASH_BRAND:
				log( "closedScreen SPLASH_BRAND" );
				break;
			case SCREEN_SPLASH_GAME:
				log( "closedScreen SPLASH_GAME" );
				break;
			case SCREEN_GAME_OVER_MENU:
				log( "closedScreen GAME_OVER_MENU" );
				break;
			case SCREEN_PAUSE:
				log( "closedScreen PAUSE" );
				break;
			case SCREEN_OPTIONS:
				log( "closedScreen OPTIONS" );
				break;
			case SCREEN_MAIN_MENU:
				log( "closedScreen MAIN_MENU" );
				break;
			case SCREEN_LOG:
				log( "closedScreen LOG" );
				break;
			case SCREEN_HELP:
				log( "closedScreen HELP" );
				break;
			case SCREEN_RECOMMEND_SENT:
				log( "closedScreen RECOMMEND_SENT" );
				break;
			case SCREEN_RECOMMEND:
				log( "closedScreen RECOMMEND" );
				break;
			case SCREEN_LOADING_RECOMMEND_SCREEN:
				log( "closedScreen LOADING_RECOMMEND_SCREEN" );
				break;
			case SCREEN_CHOOSE_PROFILE:
				log( "closedScreen CHOOSE_PROFILE" );
				break;
			case SCREEN_LOADING_GAME:
				log( "closedScreen LOADING_GAME" );
				break;
			case SCREEN_NEW_GAME:
				log( "closedScreen NEW_GAME" );
				break;
			case SCREEN_NEXT_LEVEL:
				log( "closedScreen NEXT_LEVEL" );
				break;
			case SCREEN_LOADING_PHOTO_GALLERY:
				log( "closedScreen LOADING_PHOTO_GALLERY" );
				break;
			case SCREEN_PHOTO_GALLERY:
				log( "closedScreen PHOTO_GALLERY" );
				break;
			case SCREEN_CREDITS:
				log( "closedScreen CREDITS" );
				break;
			case SCREEN_HELP_PROFILE:
				log( "closedScreen SCREEN_HELP_PROFILE" );
				break;
			case SCREEN_LOADING_PROFILES_SCREEN:
				log( "closedScreen LOADING_PROFILES_SCREEN" );
				break;
			case SCREEN_LOADING_NANO_ONLINE:
				log( "closedScreen LOADING_NANO_ONLINE" );
				break;
			case SCREEN_LOADING_HIGH_SCORES:
				log( "closedScreen LOADING_HIGH_SCORES" );
				break;
			case SCREEN_NANO_RANKING_PROFILES:
				log( "closedScreen NANO_RANKING_PROFILES" );
				break;
			case SCREEN_NANO_RANKING_MENU:
				log( "closedScreen NANO_RANKING_MENU" );
				break;

			default:
				log( "closedScreen " + screen );
		}
		//#endif

		if ( playMusic )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
		
		return screen;
	} // fim do método changeScreen( int )


	private static final String getVersion() {
		String version = instance.getAppProperty("MIDlet-Version");
		if (version == null) {
			version = "";
		}

		return "<ALN_H>" + getText(TEXT_VERSION) + version + "\n\n";
	}


	/**
	 * Define qual vai ser o padrão de fundo de tela
	 * @param type Id do padrão
	 */
	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
				break;

			case BACKGROUND_TYPE_CLOTH_TEXTURE:
				if ( bkg != null ) {
					midlet.manager.setBackground( bkg, true );
				}
				break;

			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( 0x430701 );
				break;
		}
	} // fim do método setBackground( byte )


	public static final boolean isLowMemory() {
		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
		return false;
		//#else
//# 			return lowMemory;
		//#endif
	}


	public final void onChoose( Menu menu, int id, int index ) {
		System.gc();
		switch ( id ) {
			case SCREEN_MAIN_MENU: {
				switch ( index ) {
					case TEXT_NEW_GAME:
						setScreen( SCREEN_CHOOSE_PROFILE );
						break;
					//#if DEBUG == "true"
					case TEXT_LOADING:
						setScreen( SCREEN_LOG );
						break;
					//#endif

					case TEXT_OPTIONS:
						setScreen( SCREEN_OPTIONS );
						break;

					case TEXT_NANO_ONLINE:
						setScreen( SCREEN_LOADING_NANO_ONLINE );
						break;

					case TEXT_PHOTO_GALLERY:
						setScreen( SCREEN_LOADING_PHOTO_GALLERY );
						break;

					case TEXT_HELP:
						setScreen( SCREEN_HELP );
						break;

					case TEXT_RECOMMEND_TITLE:
						setScreen( SCREEN_LOADING_RECOMMEND_SCREEN );
						break;

					case TEXT_CREDITS:
						setScreen( SCREEN_CREDITS );
						break;

					case TEXT_EXIT:
						exit();
						break;
				}

//				MainMenu.unload();
			}
			break;

			//#if NANO_RANKING == "true"
			case SCREEN_CHOOSE_PROFILE:
				switch ( index ) {
					case TEXT_CONFIRM:
						if ( nanoOnlineForm != null ) {
							NanoOnline.unload();
							nanoOnlineForm = null;
						}
						setScreen( SCREEN_LOADING_GAME );
						break;

					case TEXT_CHOOSE_ANOTHER:
						setScreen( SCREEN_LOADING_PROFILES_SCREEN );
						break;

					case TEXT_HELP:
						setScreen( SCREEN_HELP_PROFILE );
						break;

					case TEXT_BACK:
					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						if ( nanoOnlineForm != null ) {
							NanoOnline.unload();
							nanoOnlineForm = null;
						}
						setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;
			//#endif

			case SCREEN_OPTIONS:
				MediaPlayer.free();
				MediaPlayer.saveOptions();
				setScreen( SCREEN_LOADING_MAIN_MENU );
				break;

			case SCREEN_SPLASH_GAME:
				setScreen( SCREEN_LOADING_MAIN_MENU );
				break;

			case SCREEN_GAME_OVER_MENU:
				switch ( index ) {
					case TEXT_RESTART:
						setScreen( SCREEN_LOADING_GAME );
						break;


					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						gameScreen = null;
						setScreen( SCREEN_LOADING_MAIN_MENU );
						break;

					case TEXT_EXIT_GAME:
						exit();
						break;
				}
				break;

			case SCREEN_NEW_RECORD_MENU:
				switch ( index ) {
					case TEXT_HIGH_SCORES:
						setScreen( SCREEN_LOADING_HIGH_SCORES );
						break;

					case TEXT_RESTART:
						setScreen( SCREEN_LOADING_GAME );
						break;

					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						gameScreen = null;
						setScreen( SCREEN_LOADING_MAIN_MENU );
						break;

					case TEXT_EXIT_GAME:
						exit();
						break;
				}
				break;

			case SCREEN_PAUSE:
				switch ( index ) {
					case TEXT_CONTINUE:
						MediaPlayer.saveOptions();
						MediaPlayer.free();
						System.gc();
						setScreen( SCREEN_NEXT_LEVEL );
//						gameScreen.setState( GameScreen.STATE_UNPAUSING );
						break;

					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						MediaPlayer.saveOptions();
						onGameOver( gameScreen );
//						if ( gameScreen != null )
//							onGameOver( gameScreen );
						gameScreen = null;

						setScreen( SCREEN_LOADING_MAIN_MENU );
						break;

					case TEXT_EXIT_GAME:
						onGameOver( gameScreen );
						gameScreen = null;
						exit();
						break;
				}
				break;

			case SCREEN_HELP_PROFILE:
				setScreen( SCREEN_CHOOSE_PROFILE );
				break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.saveOptions();
				setScreen( SCREEN_SPLASH_NANO );
				break;

			case SCREEN_PHOTO_GALLERY:
				setScreen( SCREEN_LOADING_MAIN_MENU );
				break;

			case SCREEN_CREDITS:
				setScreen( SCREEN_LOADING_MAIN_MENU );
				break;

			case SCREEN_HELP:
				setScreen( SCREEN_LOADING_MAIN_MENU );
				break;

			case SCREEN_RECOMMEND: {
				switch ( index ) {
					case TEXT_BACK:
					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						setScreen( SCREEN_LOADING_MAIN_MENU );
						break;
					case TEXT_OK:
						setScreen( SCREEN_RECOMMEND_SENT );
						break;
				}
			}
			break;

			case SCREEN_RECOMMEND_SENT:
			case SCREEN_RECOMMEND_NOTSENT:  {
				setScreen( SCREEN_LOADING_MAIN_MENU );
			}
			break;

			//#if DEBUG == "true"
			case SCREEN_LOG:
				setScreen( SCREEN_LOADING_MAIN_MENU );
				break;
			//#endif
		} // fim switch ( id )
	}


	public static final void unblockBackground() {
		if ( bkg != null ) {
			bkg.setUnblockViewport();
		}
	}


	public static final void setBkgViewport( Rectangle r ) {
		if ( bkg != null ) {
			bkg.setViewport( r, false );
		}
	}


	public static final void setBkgBlocker( Rectangle r ) {
		if ( bkg != null ) {
			bkg.setViewportBlocker( r );
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
	}


	protected final ImageFont getFont( int index ) {
		try {
			switch ( index ) {
				default:
					return FONTS[index];
			}
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
			return null;
		}
	}


//	private static final String getVersion() {
//		String version = instance.getAppProperty("MIDlet-Version");
//		if (version == null) {
//			version = "";
//		}
//
//		return "<ALN_H>" + getText(TEXT_VERSION) + version + "\n\n";
//	}
	public final void write( DataOutputStream output ) throws Exception {
		output.writeByte( language );
		PersistentGameInformation.getInstance().write( output );
	}


	public final void read( DataInputStream input ) throws Exception {
		setLanguage( input.readByte() );
		PersistentGameInformation.getInstance().read( input );
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null ) {
			return URL_DEFAULT;
		}

		return url;
	}


	//#if NANO_RANKING == "true"
	public final String format( int type, long score ) {
		return String.valueOf( score );
	}


	public final int getRankingType( int rankingIndex ) {
		switch ( rankingIndex ) {

			case RANKING_INDEX_ACC_SCORE:
				return RankingFormatter.RANKING_TYPE_CUMULATIVE_DECRESCENT;

			case RANKING_INDEX_AVG:
			default:
				return RankingFormatter.RANKING_TYPE_AVERAGE_DESCRESCENT;
		}
	}
	//#endif


/**
	 *
	 * @param gs A tela de jogo que esta em game over
	 * @return se deve ou não exibir informações de game over ( neste caso, sempre será false )
	 */
	public static final boolean onGameOver( GameScreen gs ) {
		try {
			if( gs != null ) {
				boolean some = false;
				//#if NANO_RANKING == "true"
					boolean record = false;
					final Customer c = NanoOnline.getCurrentCustomer();
					if ( gs.getScore() > 0 ) {
						try {
							if ( c.isRegistered() ) {
								record = RankingScreen.setHighScore( RANKING_INDEX_ACC_SCORE, c.getId(), gs.getScore() ) >= 0;
							}
						} catch ( Exception e ) {
							//#if DEBUG == "true"
								e.printStackTrace();
							//#endif
						}
					}

					if( record )
						some = true;
					record = false;
					if ( gs.getAverage() > 0 ) {
						try {
							if ( c.isRegistered() ) {
								record = RankingScreen.setHighScore( RANKING_INDEX_AVG, c.getId(), gs.getAverage() ) >= 0;
							}
						} catch ( Exception e ) {
							//#if DEBUG == "true"
								e.printStackTrace();
							//#endif
						}
					}
					if( record )
						some = true;
				//#endif
				return some;
			}
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
		}


		return false;
	}

	public void processData( int id, byte[] data ) {
	}


	public void onInfo( int id, int infoIndex, Object extraData ) {
	}


	public void onError( int id, int errorIndex, Object extraData ) {
	}


	public void initLocalEntry( int rankingIndex, RankingEntry entry, int entryIndex ) {
	}


	void invalidateRecord() {
//				PersistentGameInformation.DEFAULT_START_LEVEL = 0;
//		PersistentGameInformation.DEFAULT_TRIES_LEFT = PokerPlayer.CLOTHING_BOB_FULL - 1;
//		PersistentGameInformation.DEFAULT_POINTS = INITIAL_CASH_PER_PLAYER;
//		PersistentGameInformation.DEFAULT_PLAYER = PLAYER_BARBARA;
		PersistentGameInformation.getInstance().setLevel( ( byte ) 0 );
		///dealer
		PersistentGameInformation.getInstance().setCurrentPlayer( PLAYER_BARBARA );
		PersistentGameInformation.getInstance().setPoints( INITIAL_CASH_PER_PLAYER );
		PersistentGameInformation.getInstance().setTriesLeft( ( byte ) ( PokerPlayer.CLOTHING_BOB_FULL - 1) );
		saveOptions();
//		try {
//			AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA,
//								new Serializable() {
//
//
//				public void write( DataOutputStream output ) throws Exception {
//					output.writeByte( GameMIDlet.getInstance().getLanguage() );
//
//				}
//
//
//				public void read( DataInputStream input ) throws Exception {
//				}
//			} );
//		} catch ( Exception ex ) {
//			ex.printStackTrace();
//		}
	}

	public boolean isLandscape() {
		return ( ScreenManager.SCREEN_WIDTH > ScreenManager.SCREEN_HEIGHT );
	}
}
