package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio, Ygor
 */
public final class OptionsMenu extends BasicScreen {
	
	public static final byte OPTIONS_CHOOSE_SOUND = 0;
	public static final byte OPTIONS_ON_MAIN = 1;
	public static final byte OPTIONS_LOGIN = 2;
	public static final byte OPTIONS_ON_GAME = 3;
	public static final byte OPTIONS_ON_ENDGAME = 4;
	public static final byte OPTIONS_NEW_RECORD = 5;
	public static final byte OPTIONS_GAME_TYPE = 6;

	private static final byte OPTIONS_MAIN_SOUND = 0;
	private static final byte OPTIONS_MAIN_VOLUME = 1;
	private static final byte OPTIONS_MAIN_VIBRATION = 2;

	private static final byte OPTIONS_GAME_SOUND		= 1;
	private static final byte OPTIONS_GAME_VOLUME		= 2;
	private static final byte OPTIONS_GAME_VIBRATION	= 3;

	private static final byte ARROW_DOWN = 0;
	private static final byte ARROW_UP = 1;

	private final int type;

	private final Sprite[] arrows = new Sprite[ 2 ];

	//#if TOUCH == "true"
		private final Rectangle[] arrowAreas = new Rectangle[ 2 ];
	//#endif

	private Drawable content;

	
	public OptionsMenu( int screen, int titleId, int type ) throws Exception {
		super( screen, getEntries( type ), GameMIDlet.getText( titleId ) );

		this.type = type;
		final Sprite arrow = new Sprite( PATH_MENU + "arrow_v" );
		for ( byte i = 0; i < arrows.length; ++i ) {
			arrows[ i ] = new Sprite( arrow );
			arrows[ i ].defineReferencePixel( ANCHOR_CENTER );

			//#if TOUCH == "true"
				arrowAreas[ i ] = new Rectangle();
			//#endif
		}
		arrows[ ARROW_DOWN ].mirror( TRANS_MIRROR_V );

		

		switch ( type ) {
			case OPTIONS_CHOOSE_SOUND:
				MediaPlayer.stop();
			break;

			case OPTIONS_ON_MAIN:
				MediaPlayer.stop();
			break;

			case OPTIONS_LOGIN:
				setTitle( GameMIDlet.getText( TEXT_PROFILE_SELECTION ) );
			break;

			case OPTIONS_ON_GAME:
				MediaPlayer.stop();
			break;
		}

		int index = 0;

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setCurrentIndex( index );
	}


	private static final int[] getEntries( int type ) {
		switch ( type ) {
			case OPTIONS_CHOOSE_SOUND:
				return new int[]{	TEXT_SOUND,
									TEXT_VOLUME,
									TEXT_VIBRATION,
									TEXT_START_GAME };
				
			case OPTIONS_ON_MAIN:
				return new int[]{	TEXT_SOUND,
									TEXT_VOLUME,
									TEXT_VIBRATION,
									TEXT_CONFIRM };

			case OPTIONS_LOGIN:
				return new int[]{	TEXT_CONFIRM,
									TEXT_CHOOSE_ANOTHER,
									TEXT_HELP,
									TEXT_BACK };

			case OPTIONS_ON_GAME:
				return new int[]{	TEXT_CONTINUE,
									TEXT_SOUND,
									TEXT_VOLUME,
									TEXT_VIBRATION,
									TEXT_BACK_MENU,
									TEXT_EXIT_GAME };

			case OPTIONS_ON_ENDGAME:
				return new int[]{	TEXT_RESTART,
									TEXT_HIGH_SCORES,
									TEXT_BACK_MENU,
									TEXT_EXIT_GAME };

			default:
				return null;
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		refreshContent();
	}


	public final void setBack() {

	}


	public final void keyPressed( int key ) {
		super.keyPressed( key );
		
		if( key == ScreenManager.KEY_FIRE ) {
			// o return ignora o evento de fire e deixa que
			// o keyReleased lide com essa tecla (deixando a tela)
			return;
		}
		
		switch ( type ) {
			case OPTIONS_CHOOSE_SOUND:
			case OPTIONS_ON_MAIN:
				switch ( currentIndex ) {
					case OPTIONS_MAIN_SOUND:
						keyPressedSound( key );
					break;

					case OPTIONS_MAIN_VOLUME:
						keyPressedVolume( key );
					break;

					case OPTIONS_MAIN_VIBRATION:
						keyPressedVibration( key );
					break;
				}
			break;

			case OPTIONS_ON_GAME:
				switch ( currentIndex ) {
					case OPTIONS_GAME_SOUND:
						keyPressedSound( key );
					break;

					case OPTIONS_GAME_VOLUME:
						keyPressedVolume( key );
					break;

					case OPTIONS_GAME_VIBRATION:
						keyPressedVibration( key );
					break;
				}
			break;
		}
	}


	private final void keyPressedSound( int key ) {
		boolean soundUp = true;
		switch ( key ) {
			case ScreenManager.KEY_DOWN:
			case ScreenManager.KEY_NUM8:
				soundUp = false;
			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_FIRE:
			case ScreenManager.KEY_NUM5:
				MediaPlayer.setMute( !MediaPlayer.isMuted() );
				refreshSound();
				pressArrow( soundUp ? ARROW_UP : ARROW_DOWN );
			break;
		}
	}


	private final void keyPressedVolume( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_NUM2:
				pressArrow( ARROW_UP );
				MediaPlayer.setVolume( NanoMath.min( MediaPlayer.getVolume() + 10, 100 ) );
			break;

			case ScreenManager.KEY_DOWN:
			case ScreenManager.KEY_NUM8:
				pressArrow( ARROW_DOWN );
				MediaPlayer.setVolume(  NanoMath.max( MediaPlayer.getVolume() - 10, 0 ) );
			break;
		}
		refreshVolume();
	}

	private final void keyPressedVibration( int key ) {
		boolean up = true;
		switch ( key ) {
			case ScreenManager.KEY_DOWN:
			case ScreenManager.KEY_NUM8:
				up = false;
			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_FIRE:
			case ScreenManager.KEY_NUM5:
				pressArrow( up ? ARROW_UP : ARROW_DOWN );
				MediaPlayer.setVibration( !MediaPlayer.isVibration() );
				refreshVibration();
			break;
		}
	}


	public final void keyReleased( int key ) {
		super.keyReleased( key );

		for ( byte i = 0; i < arrows.length; ++i )
			arrows[ i ].setFrame( 0 );
	}


	private final void pressArrow( byte arrow ) {
		arrows[ arrow ].setFrame( 1 );
	}


	public void hideNotify( boolean deviceEvent ) {
		if ( deviceEvent )
			MediaPlayer.stop();
	}


	public final void drawFrontLayer( Graphics g ) {
	}


	protected final void setCurrentIndex( int index ) {
		super.setCurrentIndex( index );

		if ( content != null ) {
			removeDrawable( content );
			content = null;
		}

		try {
			switch ( type ) {
				case OPTIONS_ON_MAIN:
				case OPTIONS_CHOOSE_SOUND:
					switch ( index ) {
						case OPTIONS_MAIN_SOUND:
							content = getOnOffGroup();
							refreshSound();
							break;

						case OPTIONS_MAIN_VIBRATION:
							content = getOnOffGroup();
							refreshVibration();
							break;

						case OPTIONS_MAIN_VOLUME:
							content = getVolumeGroup();
							refreshVolume();
							break;
					}
				break;

				case OPTIONS_ON_GAME:
					switch ( index ) {
						case OPTIONS_GAME_SOUND:
							content = getOnOffGroup();
							refreshSound();
							break;
							
						case OPTIONS_GAME_VIBRATION:
							content = getOnOffGroup();
							refreshVibration();
							break;

						case OPTIONS_GAME_VOLUME:
							content = getVolumeGroup();
							refreshVolume();
							break;
					}
				break;

				case OPTIONS_LOGIN:
					final Customer c = NanoOnline.getCurrentCustomer();
					content = getLabel( ( c.isRegistered() ? ( GameMIDlet.getText( TEXT_CURRENT_PROFILE ) + c.getNickname() ) : ( GameMIDlet.getText( TEXT_NO_PROFILE ) ) ) );
				break;
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}

		if ( content != null ) {
			insertDrawable( content );
			refreshContent();
		}
	}


	private final MarqueeLabel getLabel( String text ) {
		final MarqueeLabel label = new MarqueeLabel( FONT_TEXT, text );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setSize( bkgArea.width, label.getHeight() );
		label.setPosition( bkgArea.x, bkgArea.y + SAFE_MARGIN );
		label.setTextOffset( ( bkgArea.width - label.getTextWidth() ) >> 1 );

		return label;
	}


	private final Drawable getOnOffGroup() throws Exception {
		final UpdatableGroup onOffGroup = new UpdatableGroup( 5 );
		final Sprite onOff = new Sprite( PATH_MENU + "onoff" );
		onOffGroup.insertDrawable( onOff );
		for ( byte i = 0; i < arrows.length; ++i )
			onOffGroup.insertDrawable( arrows[ i ] );
		final int WIDTH = Math.max( onOff.getWidth(), arrows[ 0 ].getWidth() );
		onOffGroup.setSize( WIDTH,
							( ( arrows[ 0 ].getHeight() + OPTIONS_ITEMS_SPACING ) << 1 ) + onOff.getHeight() );
		arrows[ ARROW_UP ].setPosition( ( WIDTH - arrows[ ARROW_UP ].getWidth() ) >> 1, 0 );
		onOff.setPosition( ( WIDTH - onOff.getWidth() ) >> 1, arrows[ ARROW_UP ].getPosY() + arrows[ ARROW_UP ].getHeight() + OPTIONS_ITEMS_SPACING );
		arrows[ ARROW_DOWN ].setPosition( arrows[ ARROW_UP ].getPosX(), onOff.getPosY() + onOff.getHeight() + OPTIONS_ITEMS_SPACING );

		//#if TOUCH == "true"
			final int deltaWidth = arrows[ ARROW_DOWN ].getWidth() >> 2;
			final int deltaHeight = arrows[ ARROW_DOWN ].getHeight() >> 2;
			arrowAreas[ ARROW_DOWN ].x = arrows[ ARROW_DOWN ].getPosX() - deltaWidth;
			arrowAreas[ ARROW_DOWN ].y = arrows[ ARROW_DOWN ].getPosY() - deltaHeight;
			arrowAreas[ ARROW_DOWN ].width = arrows[ ARROW_DOWN ].getWidth() + 2 * deltaWidth;
			arrowAreas[ ARROW_DOWN ].height = arrows[ ARROW_DOWN ].getHeight() + 2 * deltaHeight;
			arrowAreas[ ARROW_UP ].x = arrows[ ARROW_UP ].getPosX() - deltaWidth;
			arrowAreas[ ARROW_UP ].y = arrows[ ARROW_UP ].getPosY() - deltaHeight;
			arrowAreas[ ARROW_UP ].width = arrows[ ARROW_UP ].getWidth() + 2 * deltaWidth;
			arrowAreas[ ARROW_UP ].height = arrows[ ARROW_UP ].getHeight() + 2 * deltaHeight;
		//#endif

		return onOffGroup;
	}


	private final Drawable getVolumeGroup() throws Exception {
		final UpdatableGroup volumeGroup = new UpdatableGroup( 5 );
		final DrawableImage[] volume = new DrawableImage[ 2 ];
		for ( byte i = 0; i < 2; ++i ) {
			volume[ i ] = new DrawableImage( PATH_IMAGES + "volume_bar_" + i + ".png" );
			volume[ i ].setPosition( arrows[ 0 ].getWidth() + OPTIONS_ITEMS_SPACING, 0 );
			volumeGroup.insertDrawable( volume[ i ] );
			volumeGroup.insertDrawable( arrows[ i ] );
		}
		volumeGroup.setSize( ( ( arrows[ 0 ].getWidth() + OPTIONS_ITEMS_SPACING ) << 1 ) + volume[ 0 ].getWidth(), volume[ 0 ].getHeight() );
		arrows[ ARROW_DOWN ].setPosition( 0, ( volumeGroup.getHeight() - arrows[ ARROW_DOWN ].getHeight() ) >> 1 );
		arrows[ ARROW_UP ].setPosition( volumeGroup.getWidth() - arrows[ ARROW_UP ].getWidth(), arrows[ ARROW_DOWN ].getPosY() );

		//#if TOUCH == "true"
			final int deltaWidth = arrows[ ARROW_DOWN ].getWidth() >> 2;
			final int deltaHeight = arrows[ ARROW_DOWN ].getHeight() >> 2;
			arrowAreas[ ARROW_DOWN ].x = arrows[ ARROW_DOWN ].getPosX() - deltaWidth;
			arrowAreas[ ARROW_DOWN ].y = arrows[ ARROW_DOWN ].getPosY() - deltaHeight;
			arrowAreas[ ARROW_DOWN ].width = arrows[ ARROW_DOWN ].getWidth() + 2 * deltaWidth;
			arrowAreas[ ARROW_DOWN ].height = arrows[ ARROW_DOWN ].getHeight() + 2 * deltaHeight;
			arrowAreas[ ARROW_UP ].x = arrows[ ARROW_UP ].getPosX() - deltaWidth;
			arrowAreas[ ARROW_UP ].y = arrows[ ARROW_UP ].getPosY() - deltaHeight;
			arrowAreas[ ARROW_UP ].width = arrows[ ARROW_UP ].getWidth() + 2 * deltaWidth;
			arrowAreas[ ARROW_UP ].height = arrows[ ARROW_UP ].getHeight() + 2 * deltaHeight;
		//#endif

		return volumeGroup;
	}


	private final void refreshSound() {
		final Sprite onOff = ( Sprite ) ( ( DrawableGroup ) content ).getDrawable( 0 );
		onOff.setFrame( MediaPlayer.isMuted() ? 0 : 1 );
	}


	private final void refreshVibration() {
		final Sprite onOff = ( Sprite ) ( ( DrawableGroup ) content ).getDrawable( 0 );
		onOff.setFrame( MediaPlayer.isVibration() ? 1 : 0 );
	}


	private final void refreshContent() {
		if ( content != null ) {
			content.setPosition( bkgArea.x + ( ( bkgArea.width - content.getWidth() ) >> 1 ),
								 bkgArea.y + ( ( bkgArea.height - content.getHeight() ) >> 1 ) ); // TODO centralizar
		}
	}

	private final void refreshVolume() {
		final Drawable volumeOff = ( ( DrawableGroup ) content ).getDrawable( 0 );
		final Drawable volumeOn = ( ( DrawableGroup ) content ).getDrawable( 2 );
		volumeOn.setSize( volumeOff.getWidth() * MediaPlayer.getVolume() / 100, volumeOn.getHeight() );
	}

	public void showNotify( boolean deviceEvent ) {
		if( type == OPTIONS_LOGIN && deviceEvent )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}

	//#if TOUCH == "true"
		public void onPointerPressed( int x, int y ) {
			super.onPointerPressed( x, y );
			if( content == null )	return;

			x -= content.getPosX();
			y -= content.getPosY();
			
			if ( arrowAreas[ 0 ].contains( x, y ) ) {
				keyPressed( ScreenManager.KEY_DOWN );
			} else if ( arrowAreas[ 1 ].contains( x, y ) ) {
				keyPressed( ScreenManager.KEY_UP );
			}
		}

		public void onPointerReleased( int x, int y ) {
			super.onPointerReleased( x, y );
			arrows[ 0 ].setFrame( 0 );
			arrows[ 1 ].setFrame( 0 );
		}
	//#endif
}
