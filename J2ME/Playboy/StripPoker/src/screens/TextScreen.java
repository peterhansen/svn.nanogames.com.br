package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
public final class TextScreen extends BasicScreen {

	private static final int SCROLLBAR_BKG_COLOR = 0xFF0000;
	private static final int SCROLLBAR_CURSOR_COLOR = 0x990000;

	private final ScrollRichLabel text;
	private final Drawable scrollCursor;
	private final Pattern bkg;

	public TextScreen( int slots, GameMIDlet midlet, int screen, /*boolean hasTittle,*/ String tittle, String textStr, boolean autoScroll ) throws Exception {
		this( slots, midlet, screen, /*hasTittle,*/ tittle, textStr, autoScroll, null );
	}


	public TextScreen( int slots, GameMIDlet midlet, int screen, /*boolean hasTittle,*/ String tittle, String textStr, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( screen, new int[] { TEXT_BACK }, tittle );

		bkg = new Pattern( 0x000000 );
		insertDrawable( bkg, 0 );

		if( !autoScroll ) {
			scrollCursor = new Pattern( SCROLLBAR_BKG_COLOR );
			final Pattern bar = new Pattern( SCROLLBAR_CURSOR_COLOR ); // TODO new DrawableImage( PATH_IMAGES + "scroll_bar_middle.png" ) );
			scrollCursor.setSize( 10, 10 );
			text = new ScrollRichLabel( new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), textStr + "\n\n\n" ), bar, scrollCursor );
		} else {
			scrollCursor = null;
			text = new ScrollRichLabel( new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), textStr + "\n\n\n" ) );
		}
		insertDrawable( text, 1 );
		text.setAutoScroll( autoScroll );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		setCurrentIndex( 0 );
	}


	public void setSize ( int width, int height ) {
		super.setSize( width, height );

		if( text.getScrollPage() != null ) {
			text.getScrollPage().setSize( scrollCursor.getSize() );
			if( text.getScrollFull() != null )
				text.getScrollFull().setSize( scrollCursor.getWidth(), height );
		}

		// TODO: melhorar esse calculo marretado. Nao quis mexer agora
		// nas bkg area e etc da Basic Screen
		final int contentPosY = hasTitle? ( screenTitle.getPosY() + screenTitle.getHeight() ) >> 1: 0;
		final int contentHeight = boardBottom.getPosY() - 11 - ( boardBottom.getHeight() >> 1 ) - contentPosY;
		
		text.setSize( bkgArea.width - ( SAFE_MARGIN >> 1 ), contentHeight );
		text.setPosition( bkgArea.x + ( SAFE_MARGIN >> 1 ), contentPosY + bkgArea.y );

		bkg.setSize( bkgArea.width, contentHeight );
		bkg.setPosition( bkgArea.x, contentPosY + bkgArea.y );
	}


	public void exit() {
		( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( null, screenID, 0 );
	}


	public void keyReleased( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				exit();
			break;

			default:
				text.keyReleased( key );
		}
	}


	public void keyPressed( int key ) {
//		switch ( key ) {
//			case ScreenManager.KEY_SOFT_RIGHT:
//			case ScreenManager.KEY_CLEAR:
//			case ScreenManager.KEY_BACK:
//			case ScreenManager.KEY_NUM5:
//			case ScreenManager.FIRE:
//				exit();
//			break;
//
//			default:
		text.keyPressed( key );
//		}
	}


	public void showNotify( boolean deviceEvent ) {
		if( deviceEvent )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}


	//#if TOUCH == "true"
		public void onPointerPressed( int x, int y ) {
			super.onPointerPressed( x, y );
			text.onPointerPressed( x, y );
		}

		public void onPointerDragged( int x, int y ) {
			super.onPointerDragged( x, y );
			text.onPointerDragged( x, y );
		}

		public void onPointerReleased( int x, int y ) {
			super.onPointerReleased( x, y );
			text.onPointerReleased( x, y );
		}

	//#endif

		
	public void drawFrontLayer( Graphics g ) {
//		//#if TOUCH == "true"
//		exitButton.draw( g );
//		//#endif
	}
}