package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import core.UI.Constants;

/**
 *
 * @author peter, ygor
 */
public final class SplashScreen extends UpdatableGroup implements Constants, ScreenListener
	//#if DEBUG == "true"
		, KeyListener
	//#endif
{

	private static final byte STATE_BEFORE_BLINK = 0;
	private static final byte STATE_LOGO_BLINKS = 1;
	private static final byte STATE_AFTER_BLINK = 2;
	private static final byte STATE_SCREEN_TRANSITION_WAIT = 3;
	private static int currentState;
	
	private static final int TIME_BEFORE = 3000;
	private static final int TIME_LOGO = 3500;
	private static final int TIME_AFTER = 1000;
	private static final int TIME_WAITING = 100;
	
	private static final int LOGO_BLINK_RATE = TIME_LOGO / 5;
	
	private final DrawableImage logoOn;
	private final DrawableImage logoOff;
	private short accTime;

	private static final BezierCurve bezier = new BezierCurve();
	private static final Point currentLogoPos = new Point();

	private short logoTime;

	public SplashScreen() throws Exception {
		super(3);

		logoOff = new DrawableImage( PATH_MENU + "logo_0.png" );
		logoOff.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
		logoOff.setVisible( false );
		insertDrawable( logoOff );

		logoOn = new DrawableImage( MainMenu.getLogoOn() );
		logoOn.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
		insertDrawable( logoOn );

		setState( STATE_BEFORE_BLINK );
		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
	}
	

	public final void update(int delta) {
		super.update(delta);

		final int percentage;
		accTime += delta;
		switch ( currentState ) {
			case STATE_BEFORE_BLINK:
				if( accTime - delta == 0 )	logoOff.setVisible( true );
				percentage = ( accTime * 100 ) / TIME_BEFORE;
				bezier.getPointAt( currentLogoPos, percentage );
				logoOff.setRefPixelPosition( currentLogoPos );
				break;

			case STATE_LOGO_BLINKS:
				percentage = ( accTime * 100 ) / TIME_LOGO;
				logoTime += delta;
				if ( logoTime >= LOGO_BLINK_RATE ) {
					logoTime = 0;
					logoOn.setVisible( !logoOn.isVisible() );
					logoOff.setVisible( !logoOn.isVisible() );
					if( logoOn.isVisible() )
						MediaPlayer.play( SOUND_NEON );
				}
				break;

			case STATE_AFTER_BLINK:
				percentage = ( accTime * 100 ) / TIME_AFTER;
				bezier.getPointAt( currentLogoPos, percentage );
				logoOn.setRefPixelPosition( currentLogoPos );
				break;

			case STATE_SCREEN_TRANSITION_WAIT:
			default:
				percentage = ( accTime * 100 ) / TIME_WAITING;
				break;
		}

		if ( percentage >= 100 && currentState <= STATE_SCREEN_TRANSITION_WAIT ) {
			setState( currentState + 1 );
			if( currentState > STATE_SCREEN_TRANSITION_WAIT )
				goToNextScreen();
		}
	}

	private final void goToNextScreen() {
		GameMIDlet.setScreen( SCREEN_MAIN_MENU );
	}

	private final void setState( int state ) {
		currentState = state;
		accTime = 0;

		switch( state ) {
			case STATE_BEFORE_BLINK:
				logoOn.setVisible( false );
				break;

			case STATE_AFTER_BLINK:
				logoOn.setVisible( true );
				logoOff.setVisible( false );
				MediaPlayer.play( SOUND_NEON );
				break;

			case STATE_LOGO_BLINKS:
				logoOn.setRefPixelPosition( logoOff.getRefPixelPosition() );
				break;

			case STATE_SCREEN_TRANSITION_WAIT:
				logoOn.setRefPixelPosition( ScreenManager.SCREEN_WIDTH >> 1 , 0 );
				break;
		}

		setBezierPoints( state, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		setBezierPoints( currentState, width, height );
	}

	
	public void hideNotify(boolean deviceEvent) {
	}

	
	public void showNotify(boolean deviceEvent) {
	}


	public final void sizeChanged(int width, int height) {
		setSize( width, height );
	}

	/**
	 * Posiciona os pontos da curva de bezier que descreve o movimento
	 * do logo.
	 * @param state Estado atual da tela
	 * @param width Largura atual da tela
	 * @param height Altura atual da tela
	 */
	private final void setBezierPoints( int state, int width, int height ) {
		final int controlY;
		final int halfWidth = width >> 1;
		switch( state ) {
			case STATE_BEFORE_BLINK:
				bezier.origin.set( halfWidth, height + ( ( logoOff.getHeight() * 3 ) / 2 ) );
				bezier.destiny.set( halfWidth, ( height >> 1 ) - ( logoOff.getHeight() >> 1 ) );
				controlY = height >> 2;
				bezier.control1.set( halfWidth, controlY );
				bezier.control2.set( halfWidth, controlY );
				break;

			case STATE_AFTER_BLINK:
				bezier.destiny.set( halfWidth, 0 );
				bezier.origin.set( halfWidth, ( height >> 1 ) - ( logoOn.getHeight() >> 1 ) );
				controlY = ( height * 3 ) >> 3;
				bezier.control1.set( halfWidth, controlY );
				bezier.control2.set( halfWidth, controlY );
				break;
		}
	}


	//#if DEBUG == "true"
		public void keyPressed( int key ) {
		}


		public void keyReleased( int key ) {
			goToNextScreen();
		}
	//#endif
}
