/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.SmsSender;
import br.com.nanogames.components.util.SmsSenderListener;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
//#endif

/**
 *
 * @author Caio, Ygor
 */
public final class ScreenRecommend extends BasicScreen implements MenuListener, SmsSenderListener, EventListener, ScreenListener
		//#if TOUCH == "true"
			,PointerListener
		//#endif
{
	//#if TOUCH == "true"
		public static final byte STATE_HIDDEN		= 0;
		public static final byte STATE_APPEARING	= 1;
		public static final byte STATE_SHOWN		= 2;
		public static final byte STATE_HIDING		= 3;

		/** tempo de animação para o thumb centralizar e a bara sumir e aparecer */
		private static final int ANIM_TOTAL_TIME = 1000;

		private TouchKeyPad keyPad;
		private byte keyPadState = STATE_HIDDEN;
		private int keyPadAccTime = ANIM_TOTAL_TIME;
		private final BezierCurve keyPadBezier = new BezierCurve();

		//private byte firstSelection = -1;
	//#endif

	private final RichLabel text;
	private final TextBox[] textBoxes;

	private final Sprite arrowUp;

	//#if TOUCH == "true"
		private final Rectangle arrowArea;
	//#endif

	private static final byte SELECTION_ON_LEFT		= -1;
	private static final byte SELECTION_DDD			= 0;
	private static final byte SELECTION_NUMBER		= 1;
	private static final byte SELECTION_ON_RIGHT	= 2;

	private byte selection = -1;

	private final UpdatableGroup labelsGroup = new UpdatableGroup( 4 );


	public ScreenRecommend( int screenId ) throws Exception {
		super( screenId, new int[] { TEXT_OK, TEXT_BACK }, GameMIDlet.getText( TEXT_RECOMMEND_TITLE ) );

		text = new RichLabel( GameMIDlet.GetFont( FONT_TEXT ), GameMIDlet.getText( TEXT_RECOMMEND_TEXT ) );
		insertDrawable( text );

		arrowUp = new Sprite( PATH_MENU + "arrow_v" );
		arrowUp.defineReferencePixel( ANCHOR_CENTER );
		//#if TOUCH == "true"
			arrowArea = new Rectangle();
		//#endif

		labelsGroup.insertDrawable( new DrawableImage( PATH_MENU + "barra_cadastro_indique.png" ) );
		Point labelsGroupSize = labelsGroup.getDrawable( 0 ).getSize();
		labelsGroupSize.x += ( ( arrowUp.getWidth() * 14 ) / 10 );
		labelsGroup.setSize( labelsGroupSize );
		insertDrawable( labelsGroup );

		textBoxes = new TextBox[ 2 ];
		for ( byte i = 0; i < 2; ++i ) {
			textBoxes[ i ] = new TextBox( GameMIDlet.GetFont( FONT_NUMBERS ), null, i == 0 ? 3 : 8, TextBox.INPUT_MODE_NUMBERS );
			textBoxes[ i ].setCaret( getCaret() );
			textBoxes[ i ].addEventListener( this );
			labelsGroup.insertDrawable( textBoxes[ i ] );
		}
		
		labelsGroup.insertDrawable( arrowUp );
		
		//#if TOUCH == "true"
			final LineBorder lb = new LineBorder( 0xe8aa26, LineBorder.TYPE_ROUND_RAISED );
			lb.setFillColor( 0xe8aa26 );
			keyPad = new TouchKeyPad( GameMIDlet.GetFont( FONT_TEXT ), new DrawableImage( "/online/clear.png" ),
					new DrawableImage( "/online/shift.png" ), lb, 0x680202, 0x8e2525, 0xb93131 );

			keyPad.addEventListener( this );
		//#endif

		setEntry( 0 );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	private final Drawable getCaret() {
		final Pattern caret = new Pattern(0xff0000);
		caret.setSize( 2, textBoxes[0].getFont().getHeight() );
		return caret;
	}

	private void sendSMS() {
		if ( textBoxes[ 1 ].getText().length() >= 7 ) {
			final SmsSender sender = new SmsSender();
			sender.send( ( textBoxes[ 0 ].getText().length() > 1 ) ? textBoxes[ 0 ].getText() + textBoxes[ 1 ].getText() : textBoxes[ 1 ].getText(), GameMIDlet.getText( TEXT_RECOMMEND_SMS ) + GameMIDlet.getRecommendURL(), this );
		} else {
			select( textBoxes[ 0 ].getText().length() == 0? 0: 1);
		}
	}
	
	//#if TOUCH == "true"
		private void hideKeyPad() {
			if( ( keyPadState != STATE_HIDING ) && ( keyPadState != STATE_HIDDEN ) ) {
				keyPadState = STATE_HIDING;
				keyPadAccTime = 0;

				keyPadBezier.origin.set( keyPad.getPosX(), keyPad.getPosY() );
				keyPadBezier.destiny.set( keyPad.getPosX() , getHeight() );

				keyPadBezier.control1.set( keyPadBezier.destiny );
				keyPadBezier.control2.set( keyPadBezier.destiny );
			}
		}


		private void showKeyPad() {
			if( ( ScreenManager.getInstance().hasPointerEvents() ) && ( keyPadState != STATE_APPEARING ) && ( keyPadState != STATE_SHOWN ) ) {
				keyPadState = STATE_APPEARING;
				keyPadAccTime = 0;

				keyPadBezier.origin.set( keyPad.getPosX(), keyPad.getPosY() );
				keyPadBezier.destiny.set( keyPad.getPosX() , getHeight() - keyPad.getHeight() );

				keyPadBezier.control1.set( keyPadBezier.destiny );
				keyPadBezier.control2.set( keyPadBezier.destiny );
			}
		}


		private final void moveKeyPad( int dx, int dy ) {
			if ( ( dx != 0 ) || ( dy != 0 ) ) {
				keyPad.move( dx, dy );
			}
		}


		public final void update( int delta ) {
			super.update( delta );

			if( keyPadAccTime < ANIM_TOTAL_TIME ) {
				keyPadAccTime += delta;
				if ( keyPadAccTime >= ANIM_TOTAL_TIME ) {
					keyPadAccTime = ANIM_TOTAL_TIME;
				}
				final Point p = new Point();
				keyPadBezier.getPointAtFixed( p, NanoMath.divInt( keyPadAccTime, ANIM_TOTAL_TIME ) );
				moveKeyPad( p.x - keyPad.getPosX(), p.y - keyPad.getPosY() );
			}
		}
	//#endif


	public void setSize ( int width, int height ) {
		super.setSize( width, height );

		text.setSize( width - ( SAFE_MARGIN + SAFE_MARGIN ), 0 );
		text.setSize( text.getWidth(), Math.min( bkgArea.height - labelsGroup.getHeight() - ( SAFE_MARGIN * 3 ), text.getTextTotalHeight() ) );

		//#if SCREEN_SIZE == "GIANT"
//# 		text.setPosition( SAFE_MARGIN, bkgArea.y + SAFE_MARGIN + 30 );
		//#else
				text.setPosition( SAFE_MARGIN, bkgArea.y + SAFE_MARGIN );
		//#endif
			
		text.formatText( false );

		labelsGroup.setPosition( ( width - labelsGroup.getWidth() ) >> 1, text.getPosY() + text.getHeight() + SAFE_MARGIN );

		//#if TOUCH == "true"
			keyPad.setSize( width, NanoMath.max( KEYPAD_MINIMUM_SIZE, ( height - ( ( boardBottom.getPosY() - ( labelsGroup.getPosY() + labelsGroup.getHeight() ) ) >> 1 ) ) * 5 ) / 10 );

			final Point p = new Point();
			switch ( keyPadState ) {
				case STATE_SHOWN:
					keyPad.setPosition( 0, height - keyPad.getHeight() );
				break;

				case STATE_HIDDEN:
					keyPad.setPosition( 0, height );
				break;

				case STATE_HIDING:
					keyPadBezier.origin.set( 0, height - keyPad.getHeight() );
					keyPadBezier.destiny.set( 0, height );
					keyPadBezier.control1.set( keyPadBezier.destiny );
					keyPadBezier.control2.set( keyPadBezier.destiny );

					keyPadBezier.getPointAtFixed( p, NanoMath.divInt( keyPadAccTime, ANIM_TOTAL_TIME ) );
					keyPad.setPosition( p.x, p.y );
				break;

				case STATE_APPEARING:
					keyPadBezier.origin.set( 0, height );
					keyPadBezier.destiny.set( 0, height - keyPad.getHeight() );
					keyPadBezier.control1.set( keyPadBezier.destiny );
					keyPadBezier.control2.set( keyPadBezier.destiny );

					keyPadBezier.getPointAtFixed( p, NanoMath.divInt( keyPadAccTime, ANIM_TOTAL_TIME ) );
					keyPad.setPosition( p.x, p.y );
				break;
			}
		//#endif

		textBoxes[ 0 ].setPosition( RECOMMEND_LABEL_LEFT_X, RECOMMEND_LABEL_LEFT_Y );
		textBoxes[ 0 ].setSize( RECOMMEND_LABEL_LEFT_WIDTH, RECOMMEND_LABEL_LEFT_HEIGHT );

		textBoxes[ 1 ].setPosition( RECOMMEND_LABEL_RIGHT_X, RECOMMEND_LABEL_RIGHT_Y );
		textBoxes[ 1 ].setSize( RECOMMEND_LABEL_RIGHT_WIDTH, RECOMMEND_LABEL_RIGHT_HEIGHT );

		final int arrowUpShiftX = ( labelsGroup.getWidth() - ( textBoxes[ 1 ].getPosX() + textBoxes[ 1 ].getWidth() ) ) >> 1;
		arrowUp.setRefPixelPosition( textBoxes[ 1 ].getPosX() + textBoxes[ 1 ].getWidth() + arrowUpShiftX - LABELS_GROUP_BORDER, labelsGroup.getHeight() >> 1 );

		//#if TOUCH == "true"
			final int deltaWidth = arrowUp.getWidth() >> 1;
			final int deltaHeight = arrowUp.getHeight() >> 1;
			arrowArea.x = arrowUp.getPosX() - deltaWidth;
			arrowArea.y = arrowUp.getPosY() - deltaHeight;
			arrowArea.width = arrowUp.getWidth() + 2 * deltaWidth;
			arrowArea.height = arrowUp.getHeight() + 2 * deltaHeight;
		//#endif
	}

	public void drawFrontLayer( Graphics g ) {
		//#if TOUCH == "true"
			keyPad.draw( g );
		//#endif
	}

	private final void select( int i ) {

		// desejamos que sempre que o usuario saia (no menu da basic screen)
		// da opcao "voltar", a opcao "ok" esteja visivel
		if( selection == 2 && i != 2) setCurrentIndex(0);
		
		selection = ( byte ) NanoMath.clamp( i, -1, 2 );

		//#if TOUCH == "true"
			switch ( selection ) {
				case SELECTION_DDD:
				case SELECTION_NUMBER:
					showKeyPad();
					keyPad.setTextBox( textBoxes[ selection ] );
				break;

				default:
					hideKeyPad();
			}
		//#endif

		for ( int j = 0; j < textBoxes.length; ++j )
			textBoxes[ j ].setFocus( j == selection );
	}


	public final void selectNext() {
		select( ( byte )( selection + 1 ) );
	}


	public final void selectPrevious() {
		select( ( byte )( selection - 1 ) );
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_LEFT:
			case ScreenManager.KEY_RIGHT:
			case ScreenManager.KEY_NUM0: case ScreenManager.KEY_NUM1: case ScreenManager.KEY_NUM2: case ScreenManager.KEY_NUM3:
			case ScreenManager.KEY_NUM4: case ScreenManager.KEY_NUM5: case ScreenManager.KEY_NUM6: case ScreenManager.KEY_NUM7:
			case ScreenManager.KEY_NUM8: case ScreenManager.KEY_NUM9: case ScreenManager.KEY_CLEAR: case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				switch ( selection ) {
					case SELECTION_DDD:
					case SELECTION_NUMBER:
						textBoxes[ selection ].keyPressed( key );
					break;

					case SELECTION_ON_LEFT:
						switch ( key ) {
							case ScreenManager.KEY_RIGHT:
								select( selection + 1 );
							break;

							case ScreenManager.KEY_LEFT:
								super.keyPressed( key );
							break;
						}
					break;

					case SELECTION_ON_RIGHT:
						switch ( key ) {
							case ScreenManager.KEY_LEFT:
								select( selection - 1 );
							break;

							case ScreenManager.KEY_RIGHT:
								super.keyPressed( key );
							break;
						}
					break;

					default:
						super.keyPressed( key );
				}
			break;

			default:
				switch( key ) {
					case ScreenManager.KEY_UP:
						arrowUp.setFrame( 1 );
						break;
						
					default:
						super.keyPressed( key );
						break;
				}
				break;
		}
	}


	public void keyReleased( int key ) {
		switch ( selection ) {
			case SELECTION_DDD:
			case SELECTION_NUMBER:
				textBoxes[ selection ].keyReleased( key );
				// note a falta do break!
				
			case SELECTION_ON_LEFT:
				if( key == ScreenManager.KEY_UP || key == ScreenManager.KEY_FIRE ) {
					arrowUp.setFrame( 0 );
					sendSMS();
				}
				break;

			default:
				super.keyReleased( key );
		}
	}


	public void onSequenceEnded( int id, int sequence ) {
	}


	public void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			if( keyPad.contains( x, y ) ) {
				keyPad.onPointerDragged( x, y );
			} else if ( labelsGroup.contains( x, y ) ) {
				for( int i = 0; i < textBoxes.length; i++ ) {
					if( textBoxes[ i ].contains( x, y ) ) {
						textBoxes[ i ].onPointerDragged( x, y );
					}
				}
			} else {
				super.onPointerDragged( x, y );
			}
		}


		public final void onPointerPressed( int x, int y ) {
			if ( keyPad.contains( x, y ) ) {
				keyPad.onPointerPressed( x, y );
			} else if ( labelsGroup.contains( x, y ) ) {
				x -= labelsGroup.getPosX();
				y -= labelsGroup.getPosY();
				for( int i = 0; i < textBoxes.length; i++ ) {
					if( textBoxes[ i ].contains( x, y ) ) {
						select( i );
						textBoxes[ i ].onPointerPressed( x, y );
					}
				}

				if( arrowArea.contains( x, y ) ) {
					keyPressed( ScreenManager.KEY_UP );
				}
			} else {
				super.onPointerPressed( x, y );
			}
		}


		public final void onPointerReleased( int x, int y ) {
			if ( keyPad.contains( x, y ) ) {
				keyPad.onPointerReleased( x, y );
			} else if ( labelsGroup.contains( x, y ) ) {
				x -= labelsGroup.getPosX();
				y -= labelsGroup.getPosY();

				for( int i = 0; i < textBoxes.length; i++ ) {
					if( textBoxes[ i ].contains( x, y ) ) {
						textBoxes[ i ].onPointerReleased( x, y );
					}
				}

				if( arrowArea.contains( x, y ) )
					keyReleased( ScreenManager.KEY_UP );
			} else {
				select( -1 );
				super.onPointerReleased( x, y );
			}
		}
	//#endif

	protected void activateEntryAction() {
		switch( currentIndex ) {
			case 0: sendSMS(); break;
			case 1: ( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( null, screenID, TEXT_BACK ); break;
		}
	}
		
	public final void eventPerformed( Event evt ) {
		switch ( evt.eventType ) {
			case Event.EVT_TEXTBOX_LEFT_ON_START:
				select( selection - 1 );
			break;

			case Event.EVT_TEXTBOX_RIGHT_ON_END:
				select( selection + 1 );
			break;

			case Event.EVT_BUTTON_CONFIRMED:
				select( SELECTION_ON_LEFT );
		}
	}

	public void hideNotify( boolean deviceEvent ) {
	}

	public void showNotify( boolean deviceEvent ) {
		if( deviceEvent )
			MediaPlayer.play( SOUND_MUSIC_MENU, MediaPlayer.LOOP_INFINITE );
	}

	public void onChoose( Menu menu, int id, int index ) {
	}

	public void onItemChanged( Menu menu, int id, int index ) {
	}

	public void onSMSSent( int id, boolean smsSent ) {
		GameMIDlet.setScreen( smsSent? SCREEN_RECOMMEND_SENT: SCREEN_RECOMMEND_NOTSENT );
	}

	public void onSMSLog( int id, String s ) {
		//#if DEBUG == "true"
			System.out.println( "sms: " + s );
		//#endif
	}

}