package screens;
//<editor-fold desc="Import">
import br.com.nanogames.components.DrawableImage;
import core.UI.MessagePainel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.BarbaraAI;
import core.PersistentGameInformation;
import core.PokerPlayer;
import core.UI.Constants;
import core.UI.GameTable;
import core.UI.PlayerPainel;
import core.UI.ScreenGameController;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
import core.UI.SlidingTabButton;
//#endif
//</editor-fold>


/**
 * Tela de jogo - controla a interface do jogador com a mesa de jogo
 * @author Daniel "Monty" Monteiro
 */
public final class GameScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if TOUCH == "true"
		, SlidingTabButton.PauseListener, PointerListener
//#endif
{
//<editor-fold desc="Campos da classe">
	/* Elementos visuais */
	/**
	 * mesa de jogo
	 */
	private GameTable gameTable;
	/**
	 * "fichas de personagem"
	 */
	private PlayerPainel[] playerPainel;
	/**
	 * painel de mensagens
	 */
	private MessagePainel messagePainel;
	/**
	 * controle de tela
	 */
	private ScreenGameController screenGameController;

	/**
	 * estado atual do jogo
	 */
	private byte currentState;

	/** velocidade do pan para a imagem de strip */
	private int panTime = 0;
	private final int PAN_SLOWNESS = 20;

	// estados de strip pan
	private static final byte PANSTATE_GOING_UP = 0;
	private static final byte PANSTATE_GOING_RIGHT = 1;
	private static final byte PANSTATE_GOING_DOWN = 2;
	private static final byte PANSTATE_GOING_LEFT = 3;

	/** bezier usado no pan */
	private static final BezierCurve bezier = new BezierCurve();

	/** estado do shift para imagens do strip */
	private byte currentStripPanState = PANSTATE_GOING_LEFT;

	/**
	 * Candidata a aposta atual DO JOGADOR
	 */
	private long currentBet;
	/**
	 * Qual é a rodada de jogo? ( Partida )
	 */
	private byte round;
	/**
	 * Tempo em um estado
	 */
	private long stateTime;
	/**
	 * Tempo desde a ultima jogada
	 */
	private long turnTime;
	/**
	 *
	 */
	private DrawableImage stripTeaseImage;
	/**
	 *
	 */
	private BarbaraAI barbaraAI;
	/**
	 *
	 */
	private PersistentGameInformation persistentGameState;
	//#if TOUCH == "true"
		private SlidingTabButton pauseButton;
	//#endif
	private boolean playerAllIn;
//</editor-fold>
//<editor-fold desc="Métodos da classe">


	/**
	 * Construtor
	 * @throws Exception
	 */
	public GameScreen() throws Exception {
		super( 15 );
		round = 0;
		persistentGameState = PersistentGameInformation.getInstance();

		//#if TOUCH == "true"
			pauseButton = new SlidingTabButton( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_WIDTH, 50, ScreenManager.SCREEN_HALF_HEIGHT, this );
		//#endif

		setSize(  ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		playerAllIn = false;
		final Point p = new Point( 0, 0 );
		bezier.setValues( p, p, p, p );
		System.gc();
	}


	/**
	 * Inicia nova partida
	 * @throws Exception caso alguma imagem falhe para ser carregada
	 */
	private void initNewGame() throws Exception {
		removeAllDrawables();
		
		//mesa de jogo
		gameTable = new GameTable( persistentGameState.getLevel() );		
		insertDrawable( gameTable );
		barbaraAI = new BarbaraAI( gameTable, PLAYER_BARBARA );

		//painel de mensagens.
		messagePainel = new MessagePainel();
		insertDrawable( messagePainel );

		//janelas de jogadores
		playerPainel = new PlayerPainel[ 2 ];
		playerPainel[PLAYER_BARBARA] = new PlayerPainel();
		insertDrawable( playerPainel[PLAYER_BARBARA] );
		playerPainel[PLAYER_BOB] = new PlayerPainel();
		insertDrawable( playerPainel[PLAYER_BOB] );

		screenGameController = new ScreenGameController();
		insertDrawable( screenGameController );

		turnTime = 0;
	}

	/**
	 * Toca musica de acordo com o estado do jogo
	 */
	public void playStateMusic() {
		switch( currentState ) {
			case GAMESTATE_OUTCOME_2:
				if( gameTable.getWinnerOfTheRound() == PLAYER_BOB )
					// player ganhou
					MediaPlayer.play( SOUND_MUSIC_PLAYER_WINS, 1 );
				else
					if( persistentGameState.getTriesLeft() == -1 ) {
						MediaPlayer.play( SOUND_MUSIC_GAMEOVER );
					} else {
						MediaPlayer.play( SOUND_MUSIC_PLAYER_LOOSES, 1 );
					}
			break;
		}
	}


	/**
	 * Atualiza o jogo, dependendo do estado atual
	 * @param delta Quanto tempo desde o ultimo update.
	 */
	public final void update( int delta ) {
		super.update( delta );
		stateTime += delta;
		turnTime += delta;

		if( currentState == GAMESTATE_OUTCOME_2 && stripTeaseImage != null ) {
			panTime += delta;
			final int percentage = NanoMath.max( 0, panTime / PAN_SLOWNESS );
			if( percentage > 100 ) {
				increasePanState();
			} else {
				stripTeaseImage.setPosition( bezier.getX( percentage ), bezier.getY( percentage ) );
			}
		}
	}

	/** Seta estado do pan das imagens de strip
		TODO: refatorar... */
	private void increasePanState() {
		panTime = -30 * PAN_SLOWNESS;
		Point origin = bezier.destiny;
		Point destiny = new Point( 0, 0 );
		int shift;

		byte newState = (byte) ( ( currentStripPanState + 1 ) % ( PANSTATE_GOING_LEFT + 1 ) );

		while( newState != currentStripPanState ) {
			if( newState == PANSTATE_GOING_UP ) {
				shift = ScreenManager.SCREEN_HEIGHT - stripTeaseImage.getHeight();
				if( shift >= 0 ) {
					newState = (byte) ( ( newState + 1 ) % PANSTATE_GOING_LEFT );
					continue;
				}
				destiny = new Point( origin.x, shift );
				currentStripPanState = newState;
				bezier.setValues( origin, origin, destiny, destiny );
				stripTeaseImage.setPosition( bezier.getX( 0 ), bezier.getY( 0 ) );
				break;
			} else if( newState == PANSTATE_GOING_RIGHT ) {
				shift = ScreenManager.SCREEN_WIDTH - stripTeaseImage.getWidth();
				if( shift >= 0 ) {
					newState = (byte) ( ( newState + 1 ) % ( PANSTATE_GOING_LEFT + 1 ) );
					continue;
				}
				destiny = new Point( shift, origin.y );
				currentStripPanState = newState;
				bezier.setValues( origin, origin, destiny, destiny );
				stripTeaseImage.setPosition( bezier.getX( 0 ), bezier.getY( 0 ) );
				break;
			} else if ( newState == PANSTATE_GOING_DOWN ) {
				shift = ScreenManager.SCREEN_HEIGHT - stripTeaseImage.getHeight();
				if( shift >= 0 ) {
					newState = (byte) ( ( newState + 1 ) % ( PANSTATE_GOING_LEFT + 1 ) );
					continue;
				}
				destiny = new Point( origin.x, 0 );
				currentStripPanState = newState;
				bezier.setValues( origin, origin, destiny, destiny );
				stripTeaseImage.setPosition( bezier.getX( 0 ), bezier.getY( 0 ) );
				break;
			} else {
				shift = ScreenManager.SCREEN_WIDTH - stripTeaseImage.getWidth();
				if( shift >= 0 ) {
					newState = (byte) ( ( newState + 1 ) % ( PANSTATE_GOING_LEFT + 1 ) );
					continue;
				}
				destiny = new Point( 0, origin.y );
				currentStripPanState = newState;
				bezier.setValues( origin, origin, destiny, destiny );
				stripTeaseImage.setPosition( bezier.getX( 0 ), bezier.getY( 0 ) );
				break;
			}
		}
		
	}


	/**
	 *
	 */
	public void pause() {
		MediaPlayer.stop();
		GameMIDlet.setScreen( SCREEN_PAUSE );
	}

	/**
	 * 
	 */
	private void exitToMenu() {
		MediaPlayer.stop();
		GameMIDlet.onGameOver( this );
		GameMIDlet.setScreen( SCREEN_MAIN_MENU );
	}

	/***
	 * Troca o estado de jogo
	 * @param state Novo estado de jogo
	 */
		
	private final void setState( byte state ) {
		//#if DEBUG == "true"
		System.out.println( "ajustando estado do jogo para:" + state );
		//#endif

		currentState = state;
		stateTime = 0;
		playStateMusic();
		switch ( state ) {
			case GAMESTATE_INTRO:
				setState( GAMESTATE_START );
				break;
			case GAMESTATE_START:
				try {
					initNewGame();
				} catch ( Exception ex ) {
					//#if DEBUG == "true"
					ex.printStackTrace();
					//#endif
				}

				gameTable.deal();
				playerPainel[PLAYER_BARBARA].setScoreLabel( gameTable.getPlayerById( PLAYER_BARBARA ).getMoney() );
				playerPainel[PLAYER_BOB].setScoreLabel( gameTable.getPlayerById( PLAYER_BOB ).getMoney() );

				setCurrentBet( gameTable.getPlayerById( PLAYER_BOB ).getBet() );

				messagePainel.addMessage( GameMIDlet.getText( TEXT_TURN1 ) );
				setState( GAMESTATE_PLAYING );
				gameTable.updatePotAndBet();
				break;
			case GAMESTATE_PLAYING:
				break;
			case GAMESTATE_OUTCOME:
				
				if ( gameTable.getWinnerOfTheRound() != PLAYER_NO_PLAYER ) {
					if ( persistentGameState.getTriesLeft() == -1 ) {
						if( gameTable.getWinnerOfTheRound() == PLAYER_BOB ) {
							messagePainel.addMessage( GameMIDlet.getText( TEXT_WIN ) );
						} else {
							messagePainel.addMessage( GameMIDlet.getText( TEXT_SAY_GAME_OVER ) );
						}
					} else if ( persistentGameState.getLevel() == PokerPlayer.CLOTHING_BARBARA_FULL ) {
						if( gameTable.getWinnerOfTheRound() == PLAYER_BOB ) {
							messagePainel.addMessage( GameMIDlet.getText( TEXT_SAY_GAME_WIN ) );
						} else {
							messagePainel.addMessage( GameMIDlet.getText( TEXT_LOSE ) );
						}
					} else {
						messagePainel.addMessage( GameMIDlet.getText( gameTable.getWinnerOfTheRound() == PLAYER_BOB? TEXT_WIN: TEXT_LOSE ) );
					}

					gameTable.setBlink( true );
					playerPainel[gameTable.getWinnerOfTheRound()].setBlink( true );
					gameTable.moveChipsTo( playerPainel[gameTable.getWinnerOfTheRound()].getPosition() );
				}


				break;
			case GAMESTATE_OUTCOME_2:
				persistentGameState.setAverage(  ( short ) ( ( persistentGameState.getAverage() + persistentGameState.getPoints() ) >> 1 ));
				removeAllDrawables();
				System.gc();
				showStripTeaseStep( gameTable.whosNext( gameTable.getWinnerOfTheRound() ), round );
				++round;
				break;
			case GAMESTATE_WIN:
				break;
			case GAMESTATE_GAMEOVER:
				GameMIDlet.onGameOver( this );
				break;
			default:
				//#if DEBUG == "true"
				System.out.println( "Estado de jogo indefinido:" + state );
			//#endif
		}

	}


	/**
	 * Algum estado foi encerrado.
	 */
	private final void stateEnd() {
		switch ( currentState ) {
			case GAMESTATE_OUTCOME:
				setState( GAMESTATE_OUTCOME_2);
				break;
			case GAMESTATE_OUTCOME_2:
				stripTeaseImage = null;
				removeAllDrawables();
				System.gc();
				if ( gameTable.getWinnerOfTheRound() == PLAYER_BARBARA && persistentGameState.getTriesLeft() < 0 ) {
					exitToMenu();
					return;
				} else if ( gameTable.getWinnerOfTheRound() == PLAYER_BOB && persistentGameState.getLevel() >= PokerPlayer.CLOTHING_BARBARA_FULL ) {
					exitToMenu();
					return;
				} else {
					persistentGameState.setMaxLevel( persistentGameState.getLevel() );
					persistentGameState.setPoints( gameTable.getPlayerById( PLAYER_BOB ).getMoney() );
					( ( GameMIDlet ) GameMIDlet.getInstance() ).saveOptions();
					setVisible( false );
					setState( GAMESTATE_START );
					MediaPlayer.free();
					refreshLayout();
					setVisible( true );
				}
				break;
		}
	}


	/**
	 * Tratamento de pressionamento de teclas, de acordo com o estado de jogo
	 * @param key keycode da tecla
	 */
	public final void keyPressed( int key ) {
		String say;

		screenGameController.keyPressed( key );

		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_SOFT_MID:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_STAR:
			case ScreenManager.KEY_POUND:
				pause();
				break;

			case ScreenManager.KEY_UP:
			case ScreenManager.KEY_FIRE:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM5:
				if ( currentState == GAMESTATE_PLAYING ) {

					if( turnTime < TIME_TURN ) return;
					turnTime = 0;

					if( gameTable.getTurn() + 1 != GameTable.TURN_5_SHOWDOWN ) {
						MediaPlayer.play( SOUND_CONFIRM_BET );
					} else {
						MediaPlayer.play( SOUND_SHOWDOWN );
					}

					if ( !playerAllIn && ( gameTable.getPlayerById( PLAYER_BOB ).getMoney() < currentBet || gameTable.getCurrentBet() > currentBet ) ) {
						return;
					}

					messagePainel.clear();
					// +1 porque isso só vai ser visivel no turno seguinte ao do case
					//#if DEBUG == "true"
						System.out.println( "turno " + ( gameTable.getTurn() + 1 ) );
					//#endif


					if ( ( gameTable.getTurn() + 1 ) != GameTable.TURN_5_SHOWDOWN && gameTable.getPlayerById( PLAYER_BOB ).getMoney() < currentBet ) {
							messagePainel.clear();
							messagePainel.addMessage( GameMIDlet.getText( TEXT_DO_FOLD ) );
					}

					switch ( gameTable.getTurn() + 1 ) {
						case GameTable.TURN_1_PREFLOP:
							say = GameMIDlet.getText( TEXT_TURN1 );
							break;
						case GameTable.TURN_2_FLOP:
							say = GameMIDlet.getText( TEXT_TURN2 );
							break;
						case GameTable.TURN_3_TURN:
							say = GameMIDlet.getText( TEXT_TURN3 );
							break;
						case GameTable.TURN_4_RIVER:
							say = GameMIDlet.getText( TEXT_TURN4 );
							break;
						case GameTable.TURN_5_SHOWDOWN:
							//Preciso atualizar para exibir corretament no painel
							gameTable.simulateEnding();

							say = ( GameMIDlet.getText( TEXT_TURN5 ) + gameTable.getPlayerById( PLAYER_BARBARA ).getHand().getLastEval() + GameMIDlet.getText( TEXT_AGAINST ) + gameTable.getPlayerById(
								   PLAYER_BOB ).getHand().getLastEval() + "." );

							if ( gameTable.getPlayerById( PLAYER_BARBARA ).getHand().getLastScore() == ( gameTable.getPlayerById(
									PLAYER_BOB ).getHand().getLastScore() ) ) {
								say += GameMIDlet.getText( TEXT_EVEN );
							}

							break;
						case GameTable.TURN_6_POST_SHOWDOWN:
							say = GameMIDlet.getText( TEXT_TURN6 );
							break;
						default:
							say = ( GameMIDlet.getText( TEXT_TURN_EXCEPTION ) );
					}

					if ( ( gameTable.getTurn() ) <= GameTable.TURN_4_RIVER ) {

						gameTable.getPlayerById( PLAYER_BOB ).setDesiredBet( currentBet );

						if ( gameTable.getPlayerById( PLAYER_BOB ).getMoney() >= currentBet && gameTable.getCurrentBet() <= currentBet || playerAllIn ) {

							//lembrando que placebet avança com o turno
							gameTable.placeBet();
							barbaraAI.update();

							if ( ( gameTable.getTurn()  ) <= GameTable.TURN_4_RIVER ) {
								say += gameTable.getPlayerById( PLAYER_BARBARA ).getSay();
							}

							messagePainel.addMessage( say );

							if ( gameTable.getPlayerById( PLAYER_BOB ).getLastAction() == PokerPlayer.ACTION_ALLIN ) {
								playerAllIn = true;
								messagePainel.addMessage( GameMIDlet.getText( TEXT_DO_ALL_IN ) );
							}

							for ( byte c = 0; c < gameTable.getNumPlayers(); ++c ) {
								playerPainel[c].setScoreLabel( gameTable.getPlayerById( c ).getMoney() );
							}
							gameTable.updatePotAndBet();

							if ( gameTable.getPlayerById( PLAYER_BARBARA ).getLastAction() == PokerPlayer.ACTION_FOLDING ) {
								gameTable.forceWinner( PLAYER_BOB );
							}

						} else if ( gameTable.getCurrentBet() <= currentBet ) {

							if ( currentState != GAMESTATE_OUTCOME ) {
								setState( GAMESTATE_OUTCOME );
							}
							return;
						}

						currentBet = gameTable.getCurrentBet();

						screenGameController.setCurrentBet( currentBet );


					} else {
						if ( currentState != GAMESTATE_OUTCOME ) {
							setState( GAMESTATE_OUTCOME );
						}
						return;
					}

					// No entanto, se ja estiver exibindo o resultado do jogo...
				} else if ( currentState == GAMESTATE_OUTCOME ) {
					if ( stateTime >= TIME_TURN ) {
						stateEnd();
					}

				} else if ( currentState == GAMESTATE_OUTCOME_2 ) {
					if ( stateTime >= TIME_TURN ) {
						stateEnd();
					}

				}
				break;

			case ScreenManager.KEY_DOWN:
			case ScreenManager.KEY_NUM8:
				//#if DEBUG == "true"
					System.out.println( "currentState:" + currentState );
				//#endif
					
				if ( gameTable != null && gameTable.getWinnerOfTheRound() == PLAYER_NO_PLAYER && ( currentState != GAMESTATE_OUTCOME || currentState != GAMESTATE_OUTCOME_2 ) ) {
					if ( gameTable.getPlayerById( PLAYER_BOB ).getLastAction() != PokerPlayer.ACTION_FOLDING ) {
						MediaPlayer.play( SOUND_FOLD );
						fold();
					}
				}
				break;
			case ScreenManager.KEY_LEFT:
			case ScreenManager.KEY_NUM4:
				if ( gameTable.getPlayerById( PLAYER_BOB ).getLastAction() != PokerPlayer.ACTION_ALLIN  && gameTable.getPlayerById( PLAYER_BOB ).getLastAction() != PokerPlayer.ACTION_FOLDING ) {
					if ( ( gameTable.getTurn() < GameTable.TURN_4_RIVER ) && currentBet > gameTable.getCurrentBet() ) {
						currentBet -= 5;
						screenGameController.setCurrentBet( currentBet );
						screenGameController.setBlink( ( currentBet > gameTable.getCurrentBet() ) );
						gameTable.getPlayerById( PLAYER_BOB ).setDesiredBet( currentBet );
						gameTable.updatePotAndBet();
					}
				}
				break;
			case ScreenManager.KEY_RIGHT:
			case ScreenManager.KEY_NUM6:
				if ( gameTable.getPlayerById( PLAYER_BOB ).getLastAction() != PokerPlayer.ACTION_ALLIN && gameTable.getPlayerById( PLAYER_BOB ).getLastAction() != PokerPlayer.ACTION_FOLDING ) {
					if ( ( gameTable.getTurn() < GameTable.TURN_4_RIVER ) && currentBet < gameTable.getPlayerById(
							PLAYER_BOB ).getMoney() ) {

						currentBet += 5;

						screenGameController.setCurrentBet( currentBet );
						screenGameController.setBlink( ( currentBet > gameTable.getCurrentBet() ) );
						gameTable.getPlayerById( PLAYER_BOB ).setDesiredBet( currentBet );
						gameTable.updatePotAndBet();
					}
				}
				break;
		}
	}


	/**
	 * Tratamento de soltura de tecla
	 * @param key keycode da tecla solta
	 */
	public final void keyReleased( int key ) {
		screenGameController.keyReleased( key );
		screenGameController.setBlink( ( currentBet > gameTable.getCurrentBet() ) );
	}


	/**
	 * Notificação de ocultação do elemento gráfico
	 * @param deviceEvent
	 */
	public final void hideNotify( boolean deviceEvent ) {
	}


	/**
	 * Notificação de exibição do elemento gráfico
	 * @param deviceEvent
	 */
	public void showNotify( boolean deviceEvent ) {
		gameTable.setSize( getWidth() , getHeight() );
	}


	/**
	 * Define um novo tamanho para o tabuleiro de jogo e re-ajusta as cartas.
	 * @param width
	 * @param height
	 */
	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( currentState == GAMESTATE_PREGAME ) {
			setState( GAMESTATE_INTRO );
		}

		//posiciona o pivot da mesa no centro dela, afim de mante-la sempre centralizada
		gameTable.setPosition( width >> 1, height >> 1 );
		gameTable.setSize( width, height );

		//posiciona as fichas de jogador no canto superior direito...
		playerPainel[PLAYER_BARBARA].setPosition( width - playerPainel[PLAYER_BARBARA].getWidth(), 0 );
		//...e no canto inferior esquerdo

		if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
			playerPainel[PLAYER_BOB].setPosition( 0, 0 );
		} else {
			playerPainel[PLAYER_BOB].setPosition( 0, height - playerPainel[PLAYER_BOB].getHeight() );
		}


		playerPainel[PLAYER_BARBARA].setPlayer( PLAYER_BARBARA );
		playerPainel[PLAYER_BOB].setPlayer( PLAYER_BOB );

		//finalmente dimensiona o painel de mensagens.

		if ( ( ( GameMIDlet ) GameMIDlet.getInstance() ).isLandscape() ) {
			messagePainel.setSize( ( width - screenGameController.getWidth() - SAFE_MARGIN ), ( height << 1 ) / 7 );
			messagePainel.setPosition( 0, height - messagePainel.getHeight() );
		} else {
			messagePainel.setSize( width - playerPainel[PLAYER_BARBARA].getWidth(), messagePainel.getHeight() );
			messagePainel.setPosition( 0, 0 );
		}

		//e tambem define a posicao do controle de tela
		screenGameController.setPosition( width - screenGameController.getWidth(),
										  height - screenGameController.getHeight() );
//#if TOUCH == "true"
	insertDrawable( pauseButton );
//#endif

	}


	/**
	 * @param width
	 * @param height
	 */
	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			setSize( width, height );
		}
	}


	//#if TOUCH == "true"
	/**
	 * Caso o ponteiro seja arrastado.
	 * @param x
	 * @param y
	 */
	public final void onPointerDragged( int x, int y ) {
	}


	/**
	 * Caso o ponteiro seja pressionado.
	 * @param x
	 * @param y
	 */
	public final void onPointerPressed( int x, int y ) {
		pauseButton.onPointerPressed( x, y );
		screenGameController.onPointerPressed( x, y );

		if ( currentState == GAMESTATE_OUTCOME || currentState == GAMESTATE_OUTCOME_2 ) {
			keyPressed( ScreenManager.KEY_FIRE );
		}
	}


	/**
	 * Caso o ponteiro seja solto.
	 * @param x
	 * @param y
	 */
	public final void onPointerReleased( int x, int y ) {
		screenGameController.onPointerReleased( x, y );

		if ( pauseButton.contains( x,y) )
			pauseButton.onPointerReleased( x, y );
		else
			pauseButton.hide();
	}
	//#endif


	/*
	 * Mostra uma etapa do strip-tease
	 */
	private void showStripTeaseStep( byte playerId, byte round ) {
		try {
			int clothingLevel = ( ( playerId == PLAYER_BOB ) ? persistentGameState.getTriesLeft() + 1 : persistentGameState.getLevel() - 1 );
			MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );

			//#if DEBUG == "true"
			System.out.println( "carregando:" + PATH_PLAYER_IMAGES + playerId + "/0/" + String.valueOf(
					clothingLevel ) + ( ( playerId == PLAYER_BARBARA ) ? ".jpg" : ".png" ) );
			//#endif
			stripTeaseImage = new DrawableImage( PATH_PLAYER_IMAGES + playerId + "/0/" + String.valueOf(
						clothingLevel ) + ( ( playerId == PLAYER_BARBARA ) ? ".jpg" : ".png" ) );

			stripTeaseImage.setVisible( true );
			stripTeaseImage.setPosition(
										-( stripTeaseImage.getWidth() -  getWidth()  ) >> 1,
										-( stripTeaseImage.getHeight() - getHeight() ) >> 1
										);

			final Point origin = new Point(
				ScreenManager.SCREEN_WIDTH > stripTeaseImage.getWidth()? stripTeaseImage.getPosX(): 0,
				ScreenManager.SCREEN_HEIGHT > stripTeaseImage.getHeight()? stripTeaseImage.getPosY(): 0
			);
			bezier.setValues( origin, origin, origin, origin );
			currentStripPanState = PANSTATE_GOING_LEFT;
			increasePanState();

			insertDrawable( stripTeaseImage );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
	}


	/**
	 * Re-ajusta o layout do jogo
	 */
	private void refreshLayout() {
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	/**
	 *
	 * @param bet define a aposta atual do jogador
	 */
	private void setCurrentBet( long bet ) {
		currentBet = bet;
		screenGameController.setCurrentBet( bet );
	}


	private void fold() {
		gameTable.getPlayerById( PLAYER_BOB ).fold();
		persistentGameState.setTriesLeft( ( byte ) ( persistentGameState.getTriesLeft() - 1 ) );
		gameTable.setWinner( PLAYER_BARBARA );
		setState( GAMESTATE_OUTCOME );
	}

	/**
	 * Açucar sintático ao resgate!!!
	 * @return o placar da partida
	 */
	short getScore() {
		return ( short ) PersistentGameInformation.getInstance().getPoints();
	}

	/**
	 * Preciso dizer mais?
	 * @return
	 */
	int getAverage() {
		return PersistentGameInformation.getInstance().getAverage();
	}
//</editor-fold>
}
