/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.SexyBackground;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.form.Form;

//#if NANO_RANKING == "true"
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
//#endif

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
//#endif

import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.BasicScreen;
import core.Key;
import core.Puzzle;
import core.SpecialKey;

import core.SpecialSexyHot;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener ,Serializable
//#if NANO_RANKING == "true"
		, ConnectionListener, RankingFormatter
//#endif
{
	
	private static final short GAME_MAX_FRAME_TIME = 180;
	
	private static final byte BACKGROUND_TYPE_DEFAULT_PATTERN	= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;

	public static final byte OPTION_ENGLISH = 0;
	public static final byte OPTION_PORTUGUESE = 1;

	public static final byte OPTION_PLAY_SOUD = 0;
	public static final byte OPTION_NO_SOUND = 1;

	public static final byte CONFIRM_YES = 0;
	public static final byte CONFIRM_NO = 1;
	public static final byte CONFIRM_CANCEL = 2;

	public static short maxReachedLevel;//TODO

	/* Imagens Estáticas */
	public static DrawableImage SEXY_LOGO;
	public static DrawableImage BAR_LEFT;
	public static DrawableImage BAR_CENTER;


	//#if DEBUG == "true"
		public static StringBuffer log = new StringBuffer();
	//#endif

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif
	
	/** Referência para a tela de jogo, para que seja possível retornar à tela de jogo apÃ³s entrar na tela de pausa. */
	private GameScreen gameScreen;
	private Drawable screenToLoad;
	
	private static boolean lowMemory;
	
	private static LoadListener loader;
	private static SexyBackground bkg;

	
	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
			FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
		//#endif

		log("GameMIDlet antes");

//		System.out.println(Runtime.getRuntime().freeMemory());
	}

	
	public static final void setSpecialKeyMapping(boolean specialMapping) {
		try {
			ScreenManager.resetSpecialKeysTable();
			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

			int[][] keys = null;
			final int offset = 'a' - 'A';

			//#if BLACKBERRY_API == "true"
//# 				switch ( Keypad.getHardwareLayout() ) {
//# 					case ScreenManager.HW_LAYOUT_REDUCED:
//# 					case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 						keys = new int[][] {
//# 							{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 							{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 							{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 							{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 							{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 							{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 							{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 							{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 							{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 							{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 							{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 							{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 							{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 							{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 							{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 							{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 							{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 							{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 						 };
//# 					break;
//#
//# 					default:
//# 						if ( specialMapping ) {
//# 							keys = new int[][] {
//# 								{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 								{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 								{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 								{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 								{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 								{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 								{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 								{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 								{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 								{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 								{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 								{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 								{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 								{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 								{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 								{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 								{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 								{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 								{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 								{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 							 };
//# 						} else {
//# 							for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 								table.put( new Integer( c ), new Integer( c ) );
//# 								table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 							}
//#
//# 							final int[] chars = new int[]
//# 							{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 								'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 							};
//#
//# 							for ( byte i = 0; i < chars.length; ++i )
//# 								table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 						}
//# 				}
//#
			//#else

				if ( specialMapping ) {
					keys = new int[][] {
						{ 'q', ScreenManager.KEY_NUM1 },
						{ 'Q', ScreenManager.KEY_NUM1 },
						{ 'e', ScreenManager.KEY_NUM3 },
						{ 'E', ScreenManager.KEY_NUM3 },
						{ 'z', ScreenManager.KEY_NUM7 },
						{ 'Z', ScreenManager.KEY_NUM7 },
						{ 'c', ScreenManager.KEY_NUM9 },
						{ 'C', ScreenManager.KEY_NUM9 },
						{ 'w', ScreenManager.UP },
						{ 'W', ScreenManager.UP },
						{ 'a', ScreenManager.LEFT },
						{ 'A', ScreenManager.LEFT },
						{ 's', ScreenManager.FIRE },
						{ 'S', ScreenManager.FIRE },
						{ 'd', ScreenManager.RIGHT },
						{ 'D', ScreenManager.RIGHT },
						{ 'x', ScreenManager.DOWN },
						{ 'X', ScreenManager.DOWN },

						{ 'r', ScreenManager.KEY_NUM1 },
						{ 'R', ScreenManager.KEY_NUM1 },
						{ 'y', ScreenManager.KEY_NUM3 },
						{ 'Y', ScreenManager.KEY_NUM3 },
						{ 'v', ScreenManager.KEY_NUM7 },
						{ 'V', ScreenManager.KEY_NUM7 },
						{ 'n', ScreenManager.KEY_NUM9 },
						{ 'N', ScreenManager.KEY_NUM9 },
						{ 't', ScreenManager.KEY_NUM2 },
						{ 'T', ScreenManager.KEY_NUM2 },
						{ 'f', ScreenManager.KEY_NUM4 },
						{ 'F', ScreenManager.KEY_NUM4 },
						{ 'g', ScreenManager.KEY_NUM5 },
						{ 'G', ScreenManager.KEY_NUM5 },
						{ 'h', ScreenManager.KEY_NUM6 },
						{ 'H', ScreenManager.KEY_NUM6 },
						{ 'b', ScreenManager.KEY_NUM8 },
						{ 'B', ScreenManager.KEY_NUM8 },

						{ 10, ScreenManager.FIRE }, // ENTER
						{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

						{ 'u', ScreenManager.KEY_STAR },
						{ 'U', ScreenManager.KEY_STAR },
						{ 'j', ScreenManager.KEY_STAR },
						{ 'J', ScreenManager.KEY_STAR },
						{ '#', ScreenManager.KEY_STAR },
						{ '*', ScreenManager.KEY_STAR },
						{ 'm', ScreenManager.KEY_STAR },
						{ 'M', ScreenManager.KEY_STAR },
						{ 'p', ScreenManager.KEY_STAR },
						{ 'P', ScreenManager.KEY_STAR },
						{ ' ', ScreenManager.KEY_STAR },
						{ '$', ScreenManager.KEY_STAR },
					 };
				} else {
					for ( char c = 'A'; c <= 'Z'; ++c ) {
						table.put( new Integer( c ), new Integer( c ) );
						table.put( new Integer( c + offset ), new Integer( c + offset ) );
					}

					final int[] chars = new int[]
					{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
						'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
					};

					for ( byte i = 0; i < chars.length; ++i )
						table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
				}
			//#endif

			if ( keys != null ) {
				for ( byte i = 0; i < keys.length; ++i )
					table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				log( e.getClass() + e.getMessage() );
				e.printStackTrace();
			//#endif
		}
	}


	public static DrawableImage getLogo() {
		return SEXY_LOGO;
	}


	public static DrawableImage getBarLeft() {
		return  BAR_LEFT;
	}


	public static DrawableImage getBarCenter() {
		return  BAR_CENTER;
	}


	public static int calculateLogoPosY( int height, int posY ) {
		if( posY + SEXY_LOGO.getHeight() < height - SEXY_LOGO_MIN_MARGIN )
			return posY;
		else
			return height - ( SEXY_LOGO.getHeight() + SEXY_LOGO_MIN_MARGIN );
	}


	/**
	 * Adiciona uma entrada no log. Não é necessário remover chamadas a esse método em versões de release, pois elas
	 * são descartadas pelo obfuscator.
	 * @param s
	 */
	public static final void log(String s) {
		//#if DEBUG == "true"
 			System.gc();

			log.append(s);
			log.append(": ");
			final long freeMem = Runtime.getRuntime().freeMemory();
 			log.append( freeMem );
 			log.append( ':' );
 			log.append( freeMem * 100 / Runtime.getRuntime().totalMemory() );
 			log.append( "%\n" );

 			System.out.println( s + ": " + freeMem + ": " + ( freeMem * 100 / Runtime.getRuntime().totalMemory() ) + "%" );
		//#endif
	}


	//#if DEBUG == "true"
		public final void start() {
			log( "start 1" );
			super.start();
			log( "start 2" );
		}
	//#endif


	protected final void loadResources() throws Exception {
		log("loadResources início");

		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//#  			Vector v = new Vector();
//#  			final int MEMORY_INCREASE = 170 * 1000; // size: 170kb
//#  			int total = 0;
//#  		    try {
//#  				while( Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT ) {
//#  					v.addElement(new byte[ MEMORY_INCREASE ]);
//#  					total += MEMORY_INCREASE;
//#  				}
//#  				lowMemory = false;
//#  			} catch( Throwable e ) {
//#  				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT && total < LOW_MEMORY_LIMIT;
//#  			} finally {
//#  				v = null;
//#  				System.gc();
//#  			}
		//#endif
//	   lowMemory = true; // TODO forçar modo LowMemmory

		for (byte i = 0; i < FONT_TYPES_TOTAL; ++i) {
			log( "FONTE " + i );

			switch ( i ) {
				//#if SCREEN_SIZE == "BIG"
					case FONT_MENU:
					case FONT_MENU_2:
						FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + ( i + 1 ) );
					break;
				//#elif SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//#  					case FONT_VIB_VOL:
//# 						FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + ( FONT_TEXT_WHITE ) );
//#  					break;
//#
//# 					case FONT_MENU:
//#  					case FONT_MENU_2:
//# 						if ( isLowMemory() )
//# 							FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + ( i + 1 ) );
//# 						else
//# 							FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
//# 					break;
//#
//# 					case FONT_TEXT_WHITE:
//# 						if ( isLowMemory() )
//# 							break;
				//#endif
				default:
					FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
			}

			if( isLowMemory() ) {
				FONTS[ FONT_VIB_VOL ] = FONTS[ FONT_MENU ];
			}

			switch ( i ) {
				//#if SCREEN_SIZE == "GIANT"
//# 					//case FONT_MENU:
//# 					case FONT_MENU_2:
//# 					break;
				//#endif
 				case FONT_SOFTKEY:
				break;

				default:
					if ( FONTS[ i ] != null ) {
						FONTS[ i ].setCharExtraOffset( DEFAULT_FONT_OFFSET );

					}
			}
		}

//		if( !isLowMemory() ) { // TODO Desabilitar o Ranking
			NanoOnline.init( NanoOnline.LANGUAGE_pt_BR , APP_SHORT_NAME );
			NanoOnline.setRankingTypes( NanoOnline.RANKING_TYPES_GLOBAL_ONLY );
			RankingScreen.init( new int[] {
				TEXT_RANKING_ACCUMULATED_POINTS,
				TEXT_RANKING_MAX_POINTS,
				TEXT_RANKING_MAX_LEVEL
			}, this );
			log( "NanoOnline" );
//		}

		SEXY_LOGO = new DrawableImage( PATH_IMAGES + "menu_logo.png" );
		log( "Sexy logo" );
		BAR_LEFT = new DrawableImage( PATH_IMAGES + "menu_little_board_left.png" );
		log( "Bar Left" );
		BAR_CENTER = new DrawableImage( PATH_IMAGES + "menu_little_board_center.png" );
		log( "Bar Center" );

		//setLanguage( ( byte ) 2 ); // NanoOnline.LANGUAGE_pt_BR
		language = NanoOnline.LANGUAGE_pt_BR;
		setLanguage( language );
		log( "textos" );

		log( "SoftButton" );

//		bkg = new SexyBackground();
//		log( "SexyBackground" );

		setSpecialKeyMapping(true);
		log("loadResources fim");
		
		log( "create database" );
		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}

		final String[] soundList = new String[SOUND_TOTAL];
		for (byte i = 0; i < SOUND_TOTAL; ++i) {
			soundList[i] = PATH_SOUNDS + i + ".mid";
		}
		MediaPlayer.init(DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList);
		loadOptions();

		setScreen( SCREEN_CHOOSE_SOUND );
	} // fim do método loadResources()

	
	protected final void changeLanguage( byte language ) {
		GameMIDlet.log( "changeLanguage início" );
		//language = NanoOnline.LANGUAGE_pt_BR;
		try {
			loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts_" + language + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
		}
		GameMIDlet.log( "changeLanguage fim" );
	}

	
	public final void destroy(){
		saveOptions();

		if( gameScreen != null )
			onGameOver( gameScreen );

		super.destroy();
		if ( gameScreen != null ) {
			unloadGameScreen();
		}
	}
	

	public static final void saveOptions() {
		try {
			AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA, ( ( GameMIDlet ) instance ) );
			MediaPlayer.saveOptions();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	} // fim do método saveOptions()



	public final void loadOptions() {
		try {
			AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA, this );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	} // fim do método loadOptions()


	protected final int changeScreen(int screen) throws Exception {
		final GameMIDlet midlet = (GameMIDlet) instance;

		Drawable nextScreen = null;

		//#if DEBUG == "true"
		switch( screen ) {
			case SCREEN_CHOOSE_SOUND:
				log( "changedScreen CHOOSE_SOUND" );
			break;
			case SCREEN_LOADING_1:
				log( "changedScreen LOADING_1" );
			break;
			case SCREEN_SPLASH_NANO:
				log( "changedScreen SPLASH_NANO" );
			break;
			case SCREEN_SPLASH_BRAND:
				log( "changedScreen SPLASH_BRAND" );
			break;
			case SCREEN_SPLASH_GAME:
				log( "changedScreen SPLASH_GAME" );
			break;
			case SCREEN_NEW_RECORD_MENU:
				log( "changedScreen NEW_RECORD_MENU" );
			break;
			case SCREEN_GAME_OVER_MENU:
				log( "changedScreen GAME_OVER_MENU" );
			break;
			case SCREEN_PAUSE:
				log( "changedScreen PAUSE" );
			break;
			case SCREEN_OPTIONS:
				log( "changedScreen OPTIONS" );
			break;
			case SCREEN_MAIN_MENU:
				log( "changedScreen MAIN_MENU" );
			break;
			case SCREEN_LOG:
				log( "changedScreen LOG" );
			break;
			case SCREEN_HELP:
				log( "changedScreen HELP" );
			break;
			case SCREEN_RECOMMEND_SENT:
				log( "changedScreen RECOMMEND_SENT" );
			break;
			case SCREEN_RECOMMEND:
				log( "changedScreen RECOMMEND" );
			break;
			case SCREEN_LOADING_RECOMMEND_SCREEN:
				log( "changedScreen LOADING_RECOMMEND_SCREEN" );
			break;
			case SCREEN_CHOOSE_PROFILE:
				log( "changedScreen CHOOSE_PROFILE" );
			break;
			case SCREEN_LOADING_GAME:
				log( "changedScreen LOADING_GAME" );
			break;
			case SCREEN_NEW_GAME:
				log( "changedScreen NEW_GAME" );
			break;
			case SCREEN_NEXT_LEVEL:
				log( "changedScreen NEXT_LEVEL" );
			break;
			case SCREEN_LOADING_PHOTO_GALLERY:
				log( "changedScreen LOADING_PHOTO_GALLERY" );
			break;
			case SCREEN_PHOTO_GALLERY:
				log( "changedScreen PHOTO_GALLERY" );
			break;
			case SCREEN_CREDITS:
				log( "changedScreen CREDITS" );
			break;
			case SCREEN_HELP_PROFILE:
				log( "changedScreen SCREEN_HELP_PROFILE" );
			break;
			case SCREEN_LOADING_PROFILES_SCREEN:
				log( "changedScreen LOADING_PROFILES_SCREEN" );
			break;
			case SCREEN_LOADING_NANO_ONLINE:
				log( "changedScreen LOADING_NANO_ONLINE" );
			break;
			case SCREEN_LOADING_HIGH_SCORES:
				log( "changedScreen LOADING_HIGH_SCORES" );
			break;
			case SCREEN_NANO_RANKING_PROFILES:
				log( "changedScreen NANO_RANKING_PROFILES" );
			break;
			case SCREEN_NANO_RANKING_MENU:
				log( "changedScreen NANO_RANKING_MENU" );
			break;

			default:
				log( "changedScreen " + screen );
		}
		//#endif

		byte bkgType = BACKGROUND_TYPE_DEFAULT_PATTERN; // pattern é padrão de fundo, se não for tem que especificar dependendo do caso no switch
		if( bkg != null && ( ( ScreenManager.SCREEN_WIDTH != bkg.getWidth() ) || ( ScreenManager.SCREEN_HEIGHT != bkg.getHeight() ) ) ) {
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}

		setSpecialKeyMapping(true);

		int indexSoftRight = -1;
		int indexSoftLeft = -1;
		int indexSoftCenter = -1;

		switch (screen) {
			case SCREEN_CHOOSE_SOUND:
				nextScreen = new OptionsMenu( this, screen, TEXT_OPTIONS, OptionsMenu.OPTIONS_ON_MAIN, null );
				( (OptionsMenu) nextScreen ).setBack();

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;


			case SCREEN_LOADING_1:
				nextScreen = new Drawable() {
					protected void paint( Graphics g ) { }
				};
			break;

			case SCREEN_SPLASH_NANO:
				//#if SCREEN_SIZE != "SMALL"
					nextScreen = new BasicSplashNano(SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_MEDIUM,
															PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#else
//# 				nextScreen = new BasicSplashNano(SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_SMALL,
//# 														PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#endif
				bkgType = BACKGROUND_TYPE_NONE;
			break;

			case SCREEN_SPLASH_BRAND:
			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame();
			break;

			case SCREEN_NEW_RECORD_MENU:
				nextScreen = new OptionsMenu( this, screen, TEXT_NEW_RECORD, OptionsMenu.OPTIONS_NEW_RECORD, null );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;

			case SCREEN_GAME_OVER_MENU:
				nextScreen = new OptionsMenu( this, screen, TEXT_GAME_OVER, OptionsMenu.OPTIONS_ON_ENDGAME, null );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;

			case SCREEN_PAUSE:
				nextScreen = new OptionsMenu( this, screen, TEXT_PAUSE, OptionsMenu.OPTIONS_ON_GAME, null );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;

			case SCREEN_OPTIONS:
				nextScreen = new OptionsMenu( this, screen, TEXT_OPTIONS, OptionsMenu.OPTIONS_ON_MAIN, null );

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;

			case SCREEN_MAIN_MENU:
				if ( true || !isLowMemory() ) {  // TODO Desabilitar o Ranking
					nextScreen = new SexyMenu(this, screen, new int[]{ TEXT_NEW_GAME ,
																		//#if DEBUG == "true"
																			TEXT_LOADING,
																		//#endif
																		TEXT_OPTIONS , TEXT_NANO_ONLINE , TEXT_PHOTO_GALLERY,
																		TEXT_HELP , TEXT_RECOMMEND_TITLE , TEXT_CREDITS , TEXT_EXIT  });
				} else {
					nextScreen = new SexyMenu(this, screen, new int[]{ TEXT_NEW_GAME ,
																		//#if DEBUG == "true"
																			TEXT_LOADING,
																		//#endif
																		TEXT_OPTIONS , TEXT_PHOTO_GALLERY,
																		TEXT_HELP , TEXT_RECOMMEND_TITLE , TEXT_CREDITS , TEXT_EXIT  });
				}
				
				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;

			//#if DEBUG == "true"
				case SCREEN_LOG:
					nextScreen = new TextScreen( 0, this, screen, true, "LOG", true, true, true, log.toString(), false );

					if ( isLowMemory() )
						bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;
			//#endif

			case SCREEN_HELP:
				if ( isLowMemory() ) {
					ScreenManager.getInstance().setCurrentScreen( null );
					SexyMenu.unload();
				}
//				} else {
					final Drawable s = SpecialSexyHot.creatDemo();
					SpecialKey.load();
					final SpecialKey ski = new SpecialKey( null );
					ski.demo( SpecialKey.SPICY_ICE);
					final SpecialKey skn = new SpecialKey( null );
					skn.demo( SpecialKey.SPICY_NORMAL);
					final SpecialKey skr = new SpecialKey( null );
					skr.demo( SpecialKey.SPICY_RADIATION);
					Key.load();
					final Key k = new Key( null );
					final Drawable[] specialChars = new Drawable[] {
						s, ski, skr, skn, k
					};
					nextScreen = new TextScreen( 0, this, screen, true, GameMIDlet.getText( TEXT_HELP ), true, true, true, GameMIDlet.getText( TEXT_HELP_OBJECTIVES ) + getVersion(), false, specialChars );
//					final Label sx = new Label( FONT_TEXT_WHITE, TEXT_HELP_IMG1 );
//					final Label sc = new Label( FONT_TEXT_WHITE, TEXT_HELP_IMG2 );
//					final Label sh = new Label( FONT_TEXT_WHITE, TEXT_HELP_IMG3 );
//					final Label sr = new Label( FONT_TEXT_WHITE, TEXT_HELP_IMG4 );
//					final Drawable[] specialChars = new Drawable[] {
//						sx, sc, sr, sh, sx
//					};
//					nextScreen = new TextScreen( 0, this, screen, true, GameMIDlet.getText( TEXT_HELP ), true, true, true, GameMIDlet.getText( TEXT_HELP_OBJECTIVES ), false, specialChars );
//				}
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				indexSoftRight = TEXT_BACK;
			break;

			//#ifndef NO_RECOMMEND

				case SCREEN_RECOMMEND_SENT:
					nextScreen = new TextScreen( 0, this, screen, true, getText( TEXT_RECOMMENDED ), true, true, true, getText( TEXT_RECOMMEND_SENT ) + getRecommendURL(), false );
					indexSoftRight = TEXT_BACK;

					if ( isLowMemory() )
						bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;

				case SCREEN_RECOMMEND:
					nextScreen = screenToLoad;

					if ( isLowMemory() )
						bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;

				case SCREEN_LOADING_RECOMMEND_SCREEN:
					unblockBackground();
					screenToLoad = null;
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							screenToLoad = new ScreenRecommend( midlet, SCREEN_RECOMMEND );

							loadScreen.setActive( false );

							setScreen( SCREEN_RECOMMEND );
						}

					} );
				break;

			//#endif

			case SCREEN_CHOOSE_PROFILE:
				if( true || (! isLowMemory()) ) {  // TODO Desabilitar o Ranking
					final Customer c = NanoOnline.getCurrentCustomer();
					// a fonte sÃ³ possui caracteres maiÃºsculos
					final String name = (getText(TEXT_CURRENT_PROFILE) + (c.getId() == Customer.ID_NONE ? getText(TEXT_NO_PROFILE) : c.getNickname())).toUpperCase();
					nextScreen = new OptionsMenu( this, screen, TEXT_OPTIONS, OptionsMenu.OPTIONS_LOGIN, c );
			break;
				} else {
//				if ( isLowMemory() ) {
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				}


			case SCREEN_LOADING_GAME:
				unblockBackground();

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;

				nextScreen = new LoadScreen( new LoadListener() {
					public final void load(final LoadScreen loadScreen) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						Key.load();
						GameMIDlet.log( "Key.load()" );
						SpecialKey.load();
						GameMIDlet.log( "SpecialKey.load()" );
						GameScreen.load();
						GameMIDlet.log( "GameScreen.load()" );
//						Puzzle.loadBackground();

						if ( gameScreen == null ) {
							gameScreen = new GameScreen( midlet );
							GameMIDlet.log( "gameScreen" );
						} else {
							gameScreen.reset();
						}

						loadScreen.setActive(false);
						setScreen( SCREEN_NEW_GAME );
					}
				} );
			break;

			case SCREEN_NEW_GAME:
			case SCREEN_NEXT_LEVEL:
				indexSoftRight = TEXT_PAUSE;
				nextScreen = gameScreen;
				gameScreen.updateBkgBlocker();
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_PHOTO_GALLERY:
				unblockBackground();
				screenToLoad = null;
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						screenToLoad = new ImageGallery( midlet, SCREEN_PHOTO_GALLERY );
						loadScreen.setActive( false );
						setScreen( SCREEN_PHOTO_GALLERY );
					}

				} );
			break;

			case SCREEN_PHOTO_GALLERY:
				nextScreen = screenToLoad;

				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_CREDITS:
				indexSoftRight = TEXT_BACK;
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				nextScreen = new TextScreen( 0, this, screen, true, GameMIDlet.getText( TEXT_CREDITS ), true, true, true, GameMIDlet.getText( TEXT_CREDITS_TEXT ), true );
			break;

			case SCREEN_HELP_PROFILE:
				indexSoftRight = TEXT_BACK;
				if ( isLowMemory() )
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				nextScreen = new TextScreen( 0, this, screen, true, GameMIDlet.getText( TEXT_HELP_PROFILE ), true, true, true, GameMIDlet.getText( TEXT_HELP_PROFILE_TEXT ), false );
			break;

			case SCREEN_LOADING_PROFILES_SCREEN:
				MediaPlayer.free();
				unblockBackground();
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						nanoOnlineForm = NanoOnline.load( SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT );

						loadScreen.setActive(false);

						setScreen( SCREEN_NANO_RANKING_PROFILES );
					}
				});
			break;

			case SCREEN_LOADING_NANO_ONLINE:
				MediaPlayer.free();
				unblockBackground();
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						nanoOnlineForm = NanoOnline.load( SCREEN_MAIN_MENU, NanoOnline.SCREEN_MAIN_MENU );

						loadScreen.setActive(false);

						setScreen( SCREEN_NANO_RANKING_MENU );
					}
				});
			break;

			case SCREEN_LOADING_HIGH_SCORES:
				MediaPlayer.free();
				unblockBackground();
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						nanoOnlineForm = NanoOnline.load( SCREEN_NEW_RECORD_MENU, NanoOnline.SCREEN_RECORDS );

						loadScreen.setActive(false);

						setScreen( SCREEN_NANO_RANKING_MENU );
					}
				});
			break;

			case SCREEN_NANO_RANKING_PROFILES:
			case SCREEN_NANO_RANKING_MENU:
				MediaPlayer.free();
				setSpecialKeyMapping(false);
				nextScreen = nanoOnlineForm;
				//#if DEBUG == "true"
					System.out.println( "setScreen( SCREEN_NANO_RANKING_PROFILES )" );
				//#endif
				bkgType = BACKGROUND_TYPE_NONE;
			break;
		} // fim switch ( screen )

		setBackground( bkgType );

		final Pattern p = new Pattern( 0xff0000 );
		p.setSize( 50, 20 );
		if( indexSoftRight != -1 ) { 
			final Label d = new Label( GetFont( FONT_SOFTKEY ), getText( indexSoftRight ) );
			d.setSize( d.getWidth() + SAFE_MARGIN, d.getHeight() + SAFE_MARGIN );
			ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_RIGHT, d );
		} else {
			ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
		}
		if( indexSoftLeft != -1 ) { 
			final Label d = new Label( GetFont( FONT_SOFTKEY ), getText( indexSoftLeft ) );
			final DrawableGroup g = new DrawableGroup( 1 );
			g.insertDrawable( d );
			g.setSize( d.getSize().add( new Point( SAFE_MARGIN, SAFE_MARGIN ) ) );
			d.setPosition( SAFE_MARGIN, 0 );
			ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_LEFT, g );
		} else {
			ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_LEFT, null );
		}
		if( indexSoftCenter != -1 ) {
			final Label d = new Label( GetFont( FONT_SOFTKEY ), getText( indexSoftCenter ) );
			d.setSize( d.getWidth(), d.getHeight() + SAFE_MARGIN );
			ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_MID, d );
		} else {
			ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_MID, null );
		}

		//#if DEBUG == "true"
			if ( nextScreen == null )
				throw new IllegalStateException( "Warning: nextScreen null (" + screen + ")." );
		//#endif

//		ScreenManager.getInstance().setCurrentScreen( nextScreen );
		midlet.manager.setCurrentScreen( nextScreen );

		//#if DEBUG == "true"
		switch( screen ) {
			case SCREEN_CHOOSE_SOUND:
				log( "changedScreen CHOOSE_SOUND" );
			break;
			case SCREEN_LOADING_1:
				log( "changedScreen LOADING_1" );
			break;
			case SCREEN_SPLASH_NANO:
				log( "changedScreen SPLASH_NANO" );
			break;
			case SCREEN_SPLASH_BRAND:
				log( "changedScreen SPLASH_BRAND" );
			break;
			case SCREEN_SPLASH_GAME:
				log( "changedScreen SPLASH_GAME" );
			break;
			case SCREEN_NEW_RECORD_MENU:
				log( "changedScreen NEW_RECORD_MENU" );
			break;
			case SCREEN_GAME_OVER_MENU:
				log( "changedScreen GAME_OVER_MENU" );
			break;
			case SCREEN_PAUSE:
				log( "changedScreen PAUSE" );
			break;
			case SCREEN_OPTIONS:
				log( "changedScreen OPTIONS" );
			break;
			case SCREEN_MAIN_MENU:
				log( "changedScreen MAIN_MENU" );
			break;
			case SCREEN_LOG:
				log( "changedScreen LOG" );
			break;
			case SCREEN_HELP:
				log( "changedScreen HELP" );
			break;
			case SCREEN_RECOMMEND_SENT:
				log( "changedScreen RECOMMEND_SENT" );
			break;
			case SCREEN_RECOMMEND:
				log( "changedScreen RECOMMEND" );
			break;
			case SCREEN_LOADING_RECOMMEND_SCREEN:
				log( "changedScreen LOADING_RECOMMEND_SCREEN" );
			break;
			case SCREEN_CHOOSE_PROFILE:
				log( "changedScreen CHOOSE_PROFILE" );
			break;
			case SCREEN_LOADING_GAME:
				log( "changedScreen LOADING_GAME" );
			break;
			case SCREEN_NEW_GAME:
				log( "changedScreen NEW_GAME" );
			break;
			case SCREEN_NEXT_LEVEL:
				log( "changedScreen NEXT_LEVEL" );
			break;
			case SCREEN_LOADING_PHOTO_GALLERY:
				log( "changedScreen LOADING_PHOTO_GALLERY" );
			break;
			case SCREEN_PHOTO_GALLERY:
				log( "changedScreen PHOTO_GALLERY" );
			break;
			case SCREEN_CREDITS:
				log( "changedScreen CREDITS" );
			break;
			case SCREEN_HELP_PROFILE:
				log( "changedScreen SCREEN_HELP_PROFILE" );
			break;
			case SCREEN_LOADING_PROFILES_SCREEN:
				log( "changedScreen LOADING_PROFILES_SCREEN" );
			break;
			case SCREEN_LOADING_NANO_ONLINE:
				log( "changedScreen LOADING_NANO_ONLINE" );
			break;
			case SCREEN_LOADING_HIGH_SCORES:
				log( "changedScreen LOADING_HIGH_SCORES" );
			break;
			case SCREEN_NANO_RANKING_PROFILES:
				log( "changedScreen NANO_RANKING_PROFILES" );
			break;
			case SCREEN_NANO_RANKING_MENU:
				log( "changedScreen NANO_RANKING_MENU" );
			break;

			default:
				log( "changedScreen " + screen );
		}
		//#endif
		return screen;
	} // fim do mÃ©todo changeScreen( int )


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	private static final void setBackground( byte type ) {
//		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_WIDTH);

		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				if ( bkg != null ) {
					log( "UNLOAD BKG ANTES" );
					bkg = null;
					log( "UNLOAD BKG DEPOIS" );
				}

				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_DEFAULT_PATTERN:
				if ( bkg == null ) {
					try {
						bkg = new SexyBackground();
						bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
					} catch ( Exception ex ) {
						//#if DEBUG == "true"
							ex.printStackTrace();
						//#endif
					}
					log( "SexyBackground" );
				}
				midlet.manager.setBackground( bkg, true );
			break;
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				if ( bkg != null ) {
					log( "UNLOAD BKG ANTES" );
					bkg = null;
					log( "UNLOAD BKG DEPOIS" );
				}
				
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( 0x760303 );
			break;
		}
	} // fim do mÃ©todo setBackground( byte )


	public static final void unblockBackground() {
		setBkgBlocker( new Rectangle( 0, 0, 0, 0 ) );
	}


	public static final void setBkgBlocker( Rectangle r ) {
		if( bkg != null )
			bkg.setViewportBlocker( r );
	}


	public static final boolean isLowMemory() {
		return lowMemory;
	}

	protected final ImageFont getFont( int index ) {
		try {
			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 				switch ( index ) {
//# 					case FONT_TEXT_WHITE:
//# 						if ( isLowMemory() ) {
//# 							final ImageFont font = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + index );
//# 							font.setCharExtraOffset( DEFAULT_FONT_OFFSET );
//# 							return font;
//# 						}
//# 					break;
//# 				}
			//#endif
				
			return FONTS[ index ];
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
			return null;
		}
	}

    
    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "";

        return "<ALN_H>" + getText( TEXT_VERSION ) + version + "\n\n";
    }
	
	
	public final void write(DataOutputStream output) throws Exception {
		output.writeShort( maxReachedLevel );
//		output.writeBoolean( sound );
//		output.writeBoolean( vibration );

		//#if DEBUG == "true"
			System.out.println("------------write--------------");
			System.out.println("maxReachedLevel: " + maxReachedLevel);
//			System.out.println("sound: " + sound);
//			System.out.println("vibration: " + vibration);
			System.out.println("----------end-write------------");
		//#endif
	}


	public final void read( DataInputStream input ) throws Exception {
		maxReachedLevel = input.readShort();
//		sound = input.readBoolean();
//		vibration = input.readBoolean();
		
		//#if DEBUG == "true"
			System.out.println("-------------read--------------");
			System.out.println("maxReachedLevel: " + maxReachedLevel);
//			System.out.println("sound: " + sound);
//			System.out.println("vibration: " + vibration);
			System.out.println("-----------end-read------------");
		//#endif
	}


	private static final void unloadGameScreen() {
		( ( GameMIDlet ) instance ).gameScreen = null;
		Puzzle.clearBuffer();
	}


	public void onChoose(Menu menu, int id, int index) {
		//#if DEBUG == "true"
			System.out.println( "---> onChoose( null, " + id + ", " + index + " )"  );
		//#endif
		switch(id) {
			case SCREEN_MAIN_MENU: {
				switch(index) {
					case TEXT_NEW_GAME:
						//#if NANO_RANKING == "true"
							setScreen(SCREEN_CHOOSE_PROFILE);
						//#else
//#							setScreen( SCREEN_LOADING_GAME );
						//#endif
					break;
					//#if DEBUG == "true"
						case TEXT_LOADING:
							setScreen( SCREEN_LOG );
						break;
					//#endif

					case TEXT_OPTIONS:
						setScreen( SCREEN_OPTIONS );
					break;

					case TEXT_NANO_ONLINE:
						setScreen( SCREEN_LOADING_NANO_ONLINE );
					break;

					case TEXT_PHOTO_GALLERY:
						setScreen( SCREEN_LOADING_PHOTO_GALLERY );
					break;

					case TEXT_HELP:
						setScreen( SCREEN_HELP );
					break;

					case TEXT_RECOMMEND_TITLE:
						setScreen( SCREEN_LOADING_RECOMMEND_SCREEN );
					break;

					case TEXT_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;

					case TEXT_EXIT:
						exit();
					break;
				}
				if( isLowMemory() )
					SexyMenu.unload();
			}
			break;

			//#if NANO_RANKING == "true"
				case SCREEN_CHOOSE_PROFILE:
					switch (index ) {
						case TEXT_CONFIRM:
							if (nanoOnlineForm != null) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
							setScreen( SCREEN_LOADING_GAME );
						break;

						case TEXT_CHOOSE_ANOTHER:
							setScreen( SCREEN_LOADING_PROFILES_SCREEN );
						break;

						case TEXT_HELP:
							setScreen( SCREEN_HELP_PROFILE );
						break;

						case TEXT_BACK:
						case TEXT_EXIT:
						case TEXT_BACK_MENU:
							if (nanoOnlineForm != null) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				break;
			//#endif

			case SCREEN_PHOTO_GALLERY:
				setScreen( SCREEN_MAIN_MENU );
				SexyMenu.setEntry( TEXT_PHOTO_GALLERY );
			break;

			case SCREEN_CREDITS:
				setScreen( SCREEN_MAIN_MENU );
				SexyMenu.setEntry( TEXT_CREDITS );
			break;

			case SCREEN_HELP_PROFILE:
				setScreen( SCREEN_CHOOSE_PROFILE );
			break;

			case SCREEN_HELP:
				setScreen( SCREEN_MAIN_MENU );
				SexyMenu.setEntry( TEXT_HELP );
			break;

			case SCREEN_RECOMMEND: {
				switch(index) {
					case TEXT_BACK:
					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						setScreen( SCREEN_MAIN_MENU );
						SexyMenu.setEntry( TEXT_RECOMMEND_TITLE );
					break;
					case TEXT_OK:
						setScreen( SCREEN_RECOMMEND_SENT );
					break;
				}
			}
			break;

			case SCREEN_RECOMMEND_SENT: {
				setScreen( SCREEN_MAIN_MENU );
				SexyMenu.setEntry( TEXT_RECOMMEND_TITLE );
			}
			break;

			//#if DEBUG == "true"
				case SCREEN_LOG:
					MediaPlayer.stop();
					setScreen( SCREEN_MAIN_MENU );
					SexyMenu.setEntry( TEXT_LOADING );
				break;
			//#endif

			case SCREEN_OPTIONS:
//				if( index == TEXT_CONFIRM ) { // se for multifunção
					MediaPlayer.stop();
					MediaPlayer.saveOptions();
					setScreen( SCREEN_MAIN_MENU );
					SexyMenu.setEntry( TEXT_OPTIONS );
//				}
			break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.saveOptions();
				setScreen( SCREEN_SPLASH_NANO );
			break;

//			case SCREEN_NEXT_LEVEL:
//				int a = 1 / 0;
////				switch(index) {
////					case TEXT_GAME_OVER:
////						setScreen( SCREEN_GAME_OVER_MENU );
////					break;
////				}
//			break;

			case SCREEN_GAME_OVER_MENU:
				switch(index) {
					case TEXT_RESTART:
						setScreen( SCREEN_LOADING_GAME );
					break;


					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						unloadGameScreen();
						setScreen( SCREEN_MAIN_MENU );
					break;

					case TEXT_EXIT_GAME:
						exit();
					break;
				}
			break;

			case SCREEN_NEW_RECORD_MENU:
				switch(index) {
					case TEXT_HIGH_SCORES:
						setScreen( SCREEN_LOADING_HIGH_SCORES );
					break;
					
					case TEXT_RESTART:
						setScreen( SCREEN_LOADING_GAME );
					break;

					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						unloadGameScreen();
						setScreen( SCREEN_MAIN_MENU );
					break;

					case TEXT_EXIT_GAME:
						exit();
					break;
				}
			break;

			case SCREEN_PAUSE:
				switch(index) {
					case TEXT_CONTINUE:
						MediaPlayer.saveOptions();
						setScreen( SCREEN_NEXT_LEVEL );
						gameScreen.setState( GameScreen.STATE_UNPAUSING );
					break;

					case TEXT_EXIT:
					case TEXT_BACK_MENU:
						MediaPlayer.saveOptions();
						if ( gameScreen != null )
							onGameOver( gameScreen );
						unloadGameScreen();

						setScreen( SCREEN_MAIN_MENU );
					break;

					case TEXT_EXIT_GAME:
						exit();
					break;
				}
			break;
		}
	}

	public void onItemChanged(Menu menu, int id, int index) {
	}


	/**
	 * 
	 * @param score
	 * @param index tipo do ranking (conforme definido em Constants)
	 * @param showGameOverScreen
	 */
	public static final boolean onGameOver( GameScreen gs ) {
		if ( !isLowMemory() ) {
			boolean some = false;
			//#if NANO_RANKING == "true"
				boolean record = false;
				if ( gs.getScore() > 0 ) {
					final Customer c = NanoOnline.getCurrentCustomer();
					try {
						if ( c.isRegistered() ) {
							record = RankingScreen.setHighScore( RANKING_INDEX_ACC_SCORE, c.getId(), gs.getScore() ) >= 0;
						}
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
				if( record )
					some = true;
				record = false;
				if ( gs.getScore() > 0 ) {
					final Customer c = NanoOnline.getCurrentCustomer();
					try {
						if ( c.isRegistered() ) {
							record = RankingScreen.setHighScore( RANKING_INDEX_MAX_SCORE, c.getId(), gs.getScore() ) >= 0;
						}
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
				if( record )
					some = true;
				record = false;
				if ( gs.getLevel() > 0 ) {
					final Customer c = NanoOnline.getCurrentCustomer();
					try {
						if ( c.isRegistered() ) {
							record = RankingScreen.setHighScore( RANKING_INDEX_MAX_LEVEL, c.getId(), gs.getLevel() ) >= 0;
						}
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
				if( record )
					some = true;
			//#endif
			return some;
		} else {
			return false;
		}
	}


	public void processData(int id, byte[] data) {
	}


	public void onInfo(int id, int infoIndex, Object extraData) {
	}


	public void onError(int id, int errorIndex, Object extraData) {
	}


	public void initLocalEntry(int rankingIndex, RankingEntry entry, int entryIndex) {
	}


	//#if NANO_RANKING == "true"
		public final String format( int type, long score ) {
			return String.valueOf( score );
		}


		public final int getRankingType( int rankingIndex ) {
			switch ( rankingIndex ) {
				case RANKING_INDEX_ACC_SCORE:
					return RankingFormatter.RANKING_TYPE_CUMULATIVE_DECRESCENT;

				case RANKING_INDEX_MAX_LEVEL:
				case RANKING_INDEX_MAX_SCORE:
				default:
					return RankingFormatter.RANKING_TYPE_BEST_SCORE_DECRESCENT;
			}
		}
	//#endif

}
