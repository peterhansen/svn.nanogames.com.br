/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Rectangle;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public final class SplashGame extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{
	private final short TIME_WAIT_LOGO = 1000;

	private final Pattern board;
	private final MarqueeLabel label;
	private final DrawableImage keyChainL;
	private final DrawableImage keyChainR;
	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
	private final DrawableImage chainHorizontal;
	private final DrawableImage chainVerical;
	//#endif
	private final DrawableImage extraKey;
	private final Sprite logo;

	private int accTime;
	
	public SplashGame() throws Exception {
		super( 8 );

		board = new Pattern( new DrawableImage( PATH_IMAGES + "menu_board_center.png" ) );
		insertDrawable( board );

		label = new MarqueeLabel( GameMIDlet.GetFont( FONT_MENU ), TEXT_PRESS_ANY_KEY );
		insertDrawable( label );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );

		keyChainL = new DrawableImage( PATH_SPLASH + "keys.png" );
		keyChainR = new DrawableImage( keyChainL );
		keyChainR.mirror( TRANS_MIRROR_H );
		insertDrawable( keyChainL );
		insertDrawable( keyChainR );

		logo = new Sprite( PATH_SPLASH + "sexy_logo" );
		insertDrawable( logo );

		extraKey = new DrawableImage( PATH_SPLASH + "extra_key.png" );
		insertDrawable( extraKey );

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
			chainHorizontal = new DrawableImage( PATH_SPLASH + "horizontal_chain.png" );
			chainVerical = new DrawableImage( PATH_SPLASH + "vertical_chain.png" );
			insertDrawable( chainHorizontal );
			insertDrawable( chainVerical );
		//#endif

		accTime = 0;

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	private final void setState( int state ) {
		
	}


	public final void setSize(int width, int height) {
		super.setSize( width, height );

		board.setSize( width, board.getHeight() );
		board.setPosition( 0, height - ( 3 * board.getHeight() >> 1 ) );

		label.setSize( NanoMath.min( board.getWidth(), label.getWidth() ), label.getHeight() );
		label.setPosition( ( board.getPosX() + ( board.getWidth() >> 1 ) ) - ( label.getWidth() >> 1 ), ( board.getPosY() + ( board.getHeight() >> 1 ) ) - ( label.getHeight() >> 1 ) );
		
		GameMIDlet.setBkgBlocker( new Rectangle( 0, board.getPosY(), width, board.getHeight() ) );

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
			if( height > width ) {
				chainVerical.setVisible( true );
				chainHorizontal.setVisible( false );

				final int restY = ( board.getPosY() - ( keyChainL.getHeight() + logo.getHeight() + chainVerical.getHeight() - ( SPLASH_EXTRA_KEY_THICKNESS << 1 )  ) ) >> 1 ;
				final int restX = ( width - ( keyChainL.getWidth() + logo.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS * 3 ) ) ) >> 1 ;

				keyChainL.setPosition( restX, restY + logo.getHeight() + chainVerical.getHeight() - ( ( 3 * SPLASH_EXTRA_KEY_THICKNESS ) >> 1 ) );
				keyChainR.setPosition( restX + keyChainL.getWidth() , keyChainL.getPosY() );

				extraKey.setPosition( keyChainR.getPosX() - ( extraKey.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS >> 1 ) ) , keyChainL.getPosY() + SPLASH_EXTRA_KEY_TO_CHAIN );
				logo.setPosition( keyChainR.getPosX() - ( SPLASH_EXTRA_KEY_THICKNESS * 3 ), restY );

				chainVerical.setPosition( keyChainR.getPosX() - SPLASH_EXTRA_KEY_THICKNESS, restY + logo.getHeight() - ( SPLASH_EXTRA_KEY_THICKNESS >> 1 ) );
			} else {
				chainVerical.setVisible( false );
				chainHorizontal.setVisible( true );

				final int restY = ( board.getPosY() - ( keyChainL.getHeight() + ( SPLASH_EXTRA_KEY_THICKNESS << 1 ) ) ) >> 1 ;
				final int restX = ( width - ( keyChainL.getWidth() + SPLASH_CHAIN_KEY_HORIZONTAL_POSITION_X + chainHorizontal.getWidth() + logo.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS ) ) ) >> 1 ;

				keyChainL.setPosition( restX, restY + ( SPLASH_EXTRA_KEY_THICKNESS << 1 ) );
				keyChainR.setPosition( restX + keyChainL.getWidth() , keyChainL.getPosY() );

				extraKey.setPosition( keyChainR.getPosX() - ( extraKey.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS >> 1 ) ) , keyChainL.getPosY() + SPLASH_EXTRA_KEY_TO_CHAIN );

				chainHorizontal.setPosition( keyChainR.getPosX() + SPLASH_CHAIN_KEY_HORIZONTAL_POSITION_X, keyChainR.getPosY() + ( ( ( 5 * SPLASH_EXTRA_KEY_THICKNESS ) >> 1 ) - chainHorizontal.getHeight() ) );
				logo.setPosition( chainHorizontal.getPosX() + chainHorizontal.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS ), restY );
			}
		//#else
//# 			final int restY = ( board.getPosY() - ( keyChainL.getHeight() + logo.getHeight() - SPLASH_EXTRA_KEY_THICKNESS ) ) >> 1 ;
//# 			final int restX = ( width - ( keyChainL.getWidth() + logo.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS * 3 ) ) ) >> 1 ;
//# 
//# 			keyChainL.setPosition( restX, restY + logo.getHeight() - SPLASH_EXTRA_KEY_THICKNESS );
//# 			keyChainR.setPosition( restX + keyChainL.getWidth() , keyChainL.getPosY() );
//# 
//# 			extraKey.setPosition( keyChainR.getPosX() - ( extraKey.getWidth() - ( SPLASH_EXTRA_KEY_THICKNESS >> 1 ) ) , keyChainL.getPosY() + SPLASH_EXTRA_KEY_TO_CHAIN );
//# 			logo.setPosition( keyChainR.getPosX() - ( SPLASH_EXTRA_KEY_THICKNESS * 3 ), restY );
		//#endif
	}

		
	public final void sizeChanged(int width, int height) {
		if (width != size.x || height != size.y) {
			setSize(width, height);
		}
	}


	public final void update(int delta) {
		super.update( delta );
		accTime += delta;
	}


	public final void keyPressed(int key) {
	}
	

	public final void keyReleased( int key ) {
		//#if DEBUG == "true"
			// permite pular a tela de splash
			if ( true ) {
				GameMIDlet.setScreen(SCREEN_MAIN_MENU);
			}
		//#endif

		if ( accTime >= TIME_WAIT_LOGO ) {
			GameMIDlet.setScreen( SCREEN_MAIN_MENU );
		}
	}


	public final void onSequenceEnded( int id, int sequence ) {
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
		MediaPlayer.play( SOUND_FIRST_MUSIC, MediaPlayer.LOOP_INFINITE );
	}


	//#if SCREEN_SIZE != "SMALL"
		public final void onPointerDragged( int x, int y ) {
		}


		public final void onPointerPressed( int x, int y ) {
			keyPressed( 0 );
		}


		public final void onPointerReleased( int x, int y ) {
			keyReleased( 0 );
		}
	//#endif

}
