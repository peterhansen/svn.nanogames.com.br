 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.BasicScreen;
import core.SubMenu;
import core.MenuLabel;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
class OptionsMenu extends BasicScreen implements MenuListener
{
	public static final byte OPTIONS_0_100_DEFAULT_STEP = 25;

	public static final byte OPTIONS_TYPES = 3;

	public static final byte OPTIONS_ON_MAIN = 0;
	public static final byte OPTIONS_ON_GAME = 1;
	public static final byte OPTIONS_ON_ENDGAME = 2;
	public static final byte OPTIONS_LOGIN = 3;
	public static final byte OPTIONS_NEW_RECORD = 4;

	private final SubMenu menu;
	private final MarqueeLabel labelProf;
	private final MenuLabel[] labels;
	private final byte[] valueType;
	private final byte[] value;
	private final int[] options;
	private final int type;

	public OptionsMenu( GameMIDlet midlet, int screen, int titleId, int type, Customer c ) throws Exception {
		super( 2, midlet, screen, true, GameMIDlet.getText( titleId ), true, true, true );

		this.type = type;
		final int selection;
		switch ( type ) {
			case OPTIONS_ON_GAME:
				selection = 3;
				options = new int[]{ TEXT_SOUND, TEXT_VOLUME, TEXT_VIBRATION, TEXT_CONTINUE, TEXT_BACK_MENU, TEXT_EXIT_GAME };
				valueType = new byte[]{ OPTIONS_STATE_ON_OF, OPTIONS_STATE_0_100, OPTIONS_STATE_ON_OF };
				labelProf = null;
			break;

			case OPTIONS_ON_MAIN:
				selection = 0;
				options = new int[]{ TEXT_SOUND, TEXT_VOLUME, TEXT_VIBRATION, TEXT_CONFIRM};
				valueType = new byte[]{ OPTIONS_STATE_ON_OF, OPTIONS_STATE_0_100, OPTIONS_STATE_ON_OF };
				labelProf = null;
			break;

			case OPTIONS_LOGIN:
				selection = 0;
				setTitle( GameMIDlet.getText( TEXT_PROFILE_SELECTION ) );
				labelProf = new MarqueeLabel( FONT_TEXT_WHITE, GameMIDlet.getText( TEXT_CURRENT_PROFILE ) + ( ( c.getId() == Customer.ID_NONE ) ? GameMIDlet.getText( TEXT_NO_PROFILE ) : c.getNickname() ).toLowerCase() );
				labelProf.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				labelProf.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
				insertDrawable( labelProf );
				options = new int[]{ TEXT_CONFIRM, TEXT_CHOOSE_ANOTHER, TEXT_HELP, TEXT_BACK };
				valueType = new byte[]{ };
			break;

			case OPTIONS_NEW_RECORD:
				selection = 0;
				options = new int[]{ TEXT_HIGH_SCORES, TEXT_RESTART, TEXT_BACK_MENU, TEXT_EXIT_GAME };
				valueType = new byte[]{ };
				labelProf = null;
			break;

			default:
				selection = 0;
				options = new int[]{ TEXT_RESTART, TEXT_BACK_MENU, TEXT_EXIT_GAME };
				valueType = new byte[]{ };
				labelProf = null;
			break;
		}

		value = new byte[ valueType.length ];
		
		labels = new MenuLabel[ options.length ];
		for( int i = 0; i < options.length; i++ ) {
			updateValue( i );
			labels[i] = new MenuLabel( ( i < valueType.length ) ? ( GameMIDlet.getText( options[i] ) + ":" ) : "" , ( i < valueType.length ) ? getText( valueType[i], value[i] ) : GameMIDlet.getText( options[i] ) );
			refreshText( i );
		}

		menu = new SubMenu( this, screen, labels );
		menu.setCurrentIndex( selection );
		insertDrawable( menu );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void setBack() {
		menu.setCurrentIndex( options.length - 1 );
	}


	public final void updateValue( int id ) {
		switch ( options[id] ) {
			case TEXT_VOLUME:
				value[id] = MediaPlayer.getVolume();
			break;
			case TEXT_VIBRATION:
				value[id] = booleanToValue( MediaPlayer.isVibration() );
			break;
			case TEXT_SOUND:
				value[id] = booleanToValue( !MediaPlayer.isMuted() );
			break;
		}
	}


	public final static String getText( byte valueType, byte value ) {
		switch ( valueType ) {
			case OPTIONS_STATE_ON_OF:
				if( ( value % 2 ) == 1 ) {
					return GameMIDlet.getText( TEXT_TURN_OFF );
				} else {
					return GameMIDlet.getText( TEXT_TURN_ON );
				}
			case OPTIONS_STATE_0_100:
				return value + "%";
		}
		return "";
	}


	public final void setSize( int width, int height ) {
		//#if DEBUG == "true"
			System.out.println( "---O---> on OptionsMenu setSize( " + ScreenManager.SCREEN_WIDTH + ", " + ScreenManager.SCREEN_HEIGHT + " );" );
		//#endif
		super.setSize( width, height );

		if( type == OPTIONS_LOGIN ){
			labelProf.setSize( bkgArea.width, FONT_WHITE_HEIGHT );
			labelProf.setPosition( bkgArea.x, bkgArea.y + ITEM_SPACING );
			labelProf.setTextOffset( ( bkgArea.width - labelProf.getTextWidth() ) >> 1 );
			menu.setPosition( bkgArea.x , labelProf.getPosY() + labelProf.getHeight() );
			menu.setSize( bkgArea.width, bkgArea.height - ( ITEM_SPACING + FONT_WHITE_HEIGHT ) );
		} else {
			menu.setPosition( bkgArea.x, bkgArea.y );
			menu.setSize( bkgArea.width, bkgArea.height );
		}
	}


	public final void keyPressed( int key ) {
		super.keyPressed( key );
		menu.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		super.keyReleased( key );
		menu.keyReleased( key );
	}


	public final void drawFrontLayer( Graphics g ) {
	}
	
	
	public final void refreshText( int id ) {
		if( id < value.length )
			labels[id].setValueText( getText( valueType[id], value[id] ) );
	}


	private final static byte booleanToValue( boolean bool ) {
		return (byte) ( bool ? 1 : 0 ) ;
	}


	public void onChoose( Menu menu, int id, int index ) {
		if( index < value.length ) {

			switch ( options[index] ) {
				case TEXT_VOLUME:
					//#if DEBUG == "true"
						System.out.println( "MediaPlayer.getVolume() was " + MediaPlayer.getVolume() );
					//#endif

					MediaPlayer.setVolume( ( OPTIONS_0_100_DEFAULT_STEP + ( ( MediaPlayer.getVolume() ) % 100 ) ) );

					//#if DEBUG == "true"
						System.out.println( "MediaPlayer.setVolume( " + OPTIONS_0_100_DEFAULT_STEP + " + ( ( "+ MediaPlayer.getVolume() + " ) % 100 ) ) " );
						System.out.println( "MediaPlayer.getVolume() is " + MediaPlayer.getVolume() + " but should be " + ( OPTIONS_0_100_DEFAULT_STEP + ( ( MediaPlayer.getVolume() ) % 100 ) ) );
					//#endif
						
					MediaPlayer.play( SOUND_LOCK_OPEN );
				break;

				case TEXT_VIBRATION:
					MediaPlayer.setVibration( !MediaPlayer.isVibration() );
					MediaPlayer.vibrate( 500 );
				break;

				case TEXT_SOUND:
					MediaPlayer.setMute( !MediaPlayer.isMuted() );
					MediaPlayer.play( NanoMath.randInt( SOUND_COLD_SPICY ) );
				break;
			}

			updateValue( index );
			refreshText( index );
		} else {
			midlet.onChoose( null, screenID, options[index] );
		}
	}


	public void onItemChanged( Menu menu, int id, int index ) {
		for( int i = 0; i < labels.length; i++ ) {
			if( i == index )
				labels[i].setFont( FONT_NIVEL_2 );
			else
				labels[i].setFont( FONT_NIVEL );
		}
	}


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			//#if DEBUG == "true"
				try {
			//#endif
			menu.onPointerDragged( x , y );
			//#if DEBUG == "true"
				}
				catch ( Throwable t ) {
						System.out.println( t.getMessage() );
						t.printStackTrace();
				}
			//#endif
		}


		public void onPointerPressed( int x, int y ) {
			//#if DEBUG == "true"
				try {
			//#endif
			menu.onPointerPressed( x , y );
			//#if DEBUG == "true"
				}
				catch ( Throwable t ) {
						System.out.println( t.getMessage() );
						t.printStackTrace();
				}
			//#endif
		}


		public void onPointerReleased( int x, int y ) {
			//#if DEBUG == "true"
				try {
			//#endif
			menu.onPointerReleased( x , y );
			//#if DEBUG == "true"
				}
				catch ( Throwable t ) {
						System.out.println( t.getMessage() );
						t.printStackTrace();
				}
			//#endif
		}
	//#endif
}
