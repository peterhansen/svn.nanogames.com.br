/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.MediaPlayer;
import core.BasicScreen;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
class TextScreen extends BasicScreen
{
	private static final byte SELECTED_NONE = 0;
	private static final byte SELECTED_EXIT_BUTTON = 1;

	private int selectionID = SELECTED_NONE;
	
	private final ScrollRichLabel text;

	public TextScreen( int slots, GameMIDlet midlet, int screen, boolean hasTittle, String tittle , boolean hasLogo, boolean hasBkg, boolean hasBorder, String textStr, boolean autoScroll ) throws Exception {
		this( slots, midlet, screen, hasTittle, tittle, hasLogo, hasBkg, hasBorder, textStr, autoScroll, null );
	}



	public TextScreen( int slots, GameMIDlet midlet, int screen, boolean hasTittle, String tittle , boolean hasLogo, boolean hasBkg, boolean hasBorder, String textStr, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( slots + 2, midlet, screen, hasTittle, tittle, hasLogo, true, true );

		if( !autoScroll ) {
			final LineBorder lb = new LineBorder( 0x975507, LineBorder.TYPE_ROUND_SIMPLE );
			lb.setFillColor( 0x975507 );
			lb.setSize( SCROLL_BAR_WIDTH, 50 );
			final Pattern dummy = new Pattern( COLOR_BACKGROUND );
			dummy.setSize( SCROLL_BAR_WIDTH, 50 );
			RichLabel label = new RichLabel( GameMIDlet.GetFont( FONT_TEXT_WHITE ), textStr + "\n\n\n" );
			label.setSpecialChars( specialChars );
			text = new ScrollRichLabel( label, dummy, lb );

		} else {
			text = new ScrollRichLabel( new RichLabel( GameMIDlet.GetFont( FONT_TEXT_WHITE ), textStr + "\n\n\n" ) );
		}
		insertDrawable( text );
		text.setAutoScroll( autoScroll );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public void setSize ( int width, int height ) {
		super.setSize( width, height );

		if( text.getScrollPage() != null ) {
			text.getScrollPage().setSize( SCROLL_BAR_WIDTH, height );
			if( text.getScrollFull() != null )
				text.getScrollFull().setSize( text.getScrollPage().getSize() );
		}

		text.setSize( width - ( TEXT_ABSOLUTE_MARGIN + TEXT_ABSOLUTE_MARGIN ), height - ( tittleGroup.getHeight() + ( TEXT_ABSOLUTE_MARGIN << 1 ) ) );
		text.setPosition( TEXT_ABSOLUTE_MARGIN, tittleGroup.getPosY() + tittleGroup.getHeight() + TEXT_BKG_BORDER );
	}


	public void exit() {
		midlet.onChoose( null, screenID, 0 );
	}


	public void keyReleased( int key ) {
		text.keyReleased( key );
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				exit();
			break;

			default:
				text.keyPressed( key );
		}
	}


	public void showNotify( boolean deviceEvent ) {
		super.showNotify( deviceEvent );
		if( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_FIRST_MUSIC, MediaPlayer.LOOP_INFINITE );
	}




	//#if TOUCH == "true"
		public void onPointerPressed( int x, int y ) {
			text.onPointerPressed( x, y );
		}

		public void onPointerDragged( int x, int y ) {
			text.onPointerDragged( x, y );
		}

		public void onPointerReleased( int x, int y ) {
			text.onPointerReleased( x, y );
		}

	//#endif
	public void drawFrontLayer( Graphics g ) {
//		//#if TOUCH == "true"
//		exitButton.draw( g );
//		//#endif
	}
}
