/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import java.lang.Short;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public class SexyLabel extends UpdatableGroup implements Constants
{
	private final DrawableImage boardLeft;
	private final Pattern boardCenter;
	private final DrawableImage boardRight;
	private final MarqueeLabel label;
	private boolean autoSize;

	private int maxWidth;

	public SexyLabel( String text , boolean autoSize, DrawableImage boardL, DrawableImage boardC, int font ) throws Exception {
		super( 4 );

		boardLeft = new DrawableImage( boardL );
		insertDrawable( boardLeft );
		boardCenter = new Pattern( new DrawableImage( boardC ) );
		insertDrawable( boardCenter );
		boardRight = new DrawableImage( boardL );
		boardRight.mirror( TRANS_MIRROR_H );
		insertDrawable( boardRight );
		label = new MarqueeLabel( GameMIDlet.GetFont( font ), text );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		insertDrawable( label );

		maxWidth = Short.MAX_VALUE;
		this.autoSize = autoSize;

		setSize();
	}


	public void setFont( int fontId ) {
		label.setFont( fontId );
	}


	public void setText( int textId ){
		setText( GameMIDlet.getText( textId ) );
	}


	public void setText( String text ){
		label.setText( text, true );

		if( autoSize ) {
			setPosition( getPosX() + ( ( getWidth() - ( label.getWidth() + ( boardLeft.getWidth() << 1 ) ) ) >> 1 ), getPosY() );
			setSize();
		} else {
			setSize( getWidth(), getHeight() );
		}
	}


	public void setMaxWidth( int maxWidth ) {
		this.maxWidth = maxWidth;
		setSize( getWidth(), getHeight() );
	}


	public final void setSize() {
		setSize( label.getWidth() + ( boardLeft.getWidth() << 1 ), boardLeft.getHeight() );
	}

	
	public void setSize( int width, int height ) {
		super.setSize( NanoMath.min( width, maxWidth ), height );
		defineReferencePixel( ANCHOR_CENTER );

		//#if DEBUG == "true"
			System.out.println( " \""+ label.getText() +"\" SexyLabel.setSize( " + width + ", " + height + " );" );
		//#endif
		
		boardLeft.setPosition( 0 , ( height - boardLeft.getHeight() ) >> 1 );
		boardCenter.setSize( width - ( boardLeft.getWidth() << 1 ) , boardLeft.getHeight() );
		boardCenter.setPosition( boardLeft.getPosX() + boardLeft.getWidth(), boardLeft.getPosY() );
		boardRight.setPosition( boardCenter.getPosX() + boardCenter.getWidth() , boardLeft.getPosY() );
		label.setText( label.getText(), true );
		label.setSize( NanoMath.min( boardCenter.getWidth(), label.getWidth() ), label.getHeight() );

		//#if DEBUG == "true"
			System.out.println( "--> label.getWidth = "+ label.getWidth() +", label.getHeight = " + label.getHeight() + " boardCenter.getWidth = " + boardCenter.getWidth() );
		//#endif

		label.setPosition( ( boardCenter.getPosX() + ( boardCenter.getWidth() >> 1 ) ) - ( label.getWidth() >> 1 ), ( boardCenter.getPosY() + ( boardCenter.getHeight() >> 1 ) ) - ( label.getHeight() >> 1 ) );
	}
}
