/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import javax.microedition.media.MediaException;
import screens.GameScreen;

/**
 *
 * @author Caio
 */
public final class SpecialSexyHot extends UpdatableGroup implements Constants, SpriteListener {

	/** tempo de animação para o thumb centralizar e a bara sumir e aparecer */
	private static final int ANIM_TOTAL_TIME = 1000;

	public static final byte PART_TYPES = 5;

	public static final byte PART_S = 0;
	public static final byte PART_E = 1;
	public static final byte PART_X = 2;
	public static final byte PART_Y = 3;
	public static final byte PART_HOT = 4;
	
	public static final byte PART_STATE_HIDE			= 0;
	public static final byte PART_STATE_SHOWING			= 1;
	public static final byte PART_STATE_BEING_COLLECTED	= 2;
	public static final byte PART_STATE_COLLECTED		= 3;

	public static final byte SEQUENCE_IDLE = 0;
	public static final byte SEQUENCE_INCINERATION = 1;

	private final byte[] partState = new byte[ PART_TYPES ];
	private final Point captivePosition = new Point();
	private byte captiveType = 0;
	private boolean captiveVisible = false;

	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;

	private final DrawableImage[] parts = new DrawableImage[ PART_TYPES ];

	private final short TOTAL_WIDTH;
	private final short MAX_HEIGHT;
	private final short MAX_WIDTH;

	private final Sprite incineration;

	private final Rectangle sexyShowArea = new Rectangle();
	private final Rectangle sexyHideArea = new Rectangle();

	private final BezierCurve bezier = new BezierCurve();
	private final BezierCurve collectedBezier = new BezierCurve();

	private int accTime = ANIM_TOTAL_TIME;
	private int collectAccTime = ANIM_TOTAL_TIME;

	private final UpdatableGroup collection;
	private byte collectionState;
	private final short[] collectionPosX = new short[ PART_TYPES ];

	/***/
	private final MUV speedX = new MUV();
	private final MUV speedY = new MUV();

	/** índice do tipo de movimento atual. */
	private byte move;

	private final Point bottomRight = new Point();
	private final Point topLeft = new Point();

	private final GameScreen gameScreen;


	public SpecialSexyHot( GameScreen gameScreen ) throws Exception {
		super( 2 );

		this.gameScreen = gameScreen;

		collection = new UpdatableGroup( 6 );
		insertDrawable( collection );

		parts[ PART_S ] = new DrawableImage( PATH_IMAGES + "game_powerup_s.png" );
		parts[ PART_S ].defineReferencePixel( ANCHOR_CENTER );
		collectionPosX[ PART_S ] = 0;

		parts[ PART_E ] = new DrawableImage( PATH_IMAGES + "game_powerup_e.png" );
		parts[ PART_E ].defineReferencePixel( ANCHOR_CENTER );
		collectionPosX[ PART_E ] = ( short ) ( collectionPosX[ PART_S ] + parts[ PART_S ].getWidth() );

		parts[ PART_X ] = new DrawableImage( PATH_IMAGES + "game_powerup_x.png" );
		parts[ PART_X ].defineReferencePixel( ANCHOR_CENTER );
		collectionPosX[ PART_X ] = ( short ) ( collectionPosX[ PART_E ] + parts[ PART_E ].getWidth() );

		parts[ PART_Y ] = new DrawableImage( PATH_IMAGES + "game_powerup_y.png" );
		parts[ PART_Y ].defineReferencePixel( ANCHOR_CENTER );
		collectionPosX[ PART_Y ] = ( short ) ( collectionPosX[ PART_X ] + parts[ PART_X ].getWidth() );

		parts[ PART_HOT ] = new DrawableImage( PATH_IMAGES + "game_powerup_hot.png" );
		parts[ PART_HOT ].defineReferencePixel( ANCHOR_CENTER );
		collectionPosX[ PART_HOT ] = ( short ) ( collectionPosX[ PART_Y ] + parts[ PART_Y ].getWidth() );

		MAX_HEIGHT = (short) NanoMath.max( parts[ PART_S ].getHeight(),
				NanoMath.max( parts[ PART_E ].getHeight(),
				NanoMath.max( parts[ PART_X ].getHeight(),
				NanoMath.max( parts[ PART_Y ].getHeight(),
				parts[ PART_HOT ].getHeight() ) ) ) );
		MAX_WIDTH = (short) NanoMath.max( parts[ PART_S ].getWidth(),
				NanoMath.max( parts[ PART_E ].getWidth(),
				NanoMath.max( parts[ PART_X ].getWidth(),
				NanoMath.max( parts[ PART_Y ].getWidth(),
				parts[ PART_HOT ].getWidth() ) ) ) );
		TOTAL_WIDTH = (short) ( collectionPosX[ PART_HOT ] + parts[ PART_HOT ].getWidth() );

		incineration = new Sprite( PATH_IMAGES + "game_powerup_incineration" );

//		int width = 0, height = 0;
//		for( byte i = 0; i < PART_TYPES; i++ ) {
//			width += parts[ i ].getWidth();
//		}

		collection.setSize( TOTAL_WIDTH + ( COLLECTION_OFFSET_X << 1 ), ( COLLECTION_OFFSET_Y << 1 ) + MAX_HEIGHT );
		incineration.setListener( this, 0 );
		incineration.setVisible( false );


		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		reset();
	}


	public void draw( Graphics g ) {
		if ( captiveVisible ) {
			parts[ captiveType ].setRefPixelPosition( captivePosition );
		}

		super.draw( g );
	}


	public final void reset() {
		incineration.setSequence( SEQUENCE_IDLE );
		collection.removeAllDrawables();
		removeAllDrawables();
		insertDrawable( collection );
		incineration.setVisible( false );
		captivePosition.set( 0 , 0 );
		captiveType = 0;
		captiveVisible = false;
		accTime = ANIM_TOTAL_TIME;
		collectAccTime = ANIM_TOTAL_TIME;
		hideCollection();


		for( byte i = 0; i < partState.length; i++ ) {
			partState[ i ] = PART_STATE_HIDE;
		}
		captiveVisible = false;
	}
	
	
	public final boolean tryToCollect( Rectangle rect ) {
		final Rectangle r = new Rectangle( parts[ captiveType ].getPosition(), parts[ captiveType ].getSize() );

		if( rect.intersects( r ) && partState[ captiveType ] == PART_STATE_SHOWING ) {
			collect();
			return true;
		}
		return false;
	}
	
	
	private final void collect() {
		partState[ captiveType ] = PART_STATE_BEING_COLLECTED;

		MediaPlayer.play( SOUND_KEY_COLLECTED );

		showCollection();
		collectAccTime = 0;

		collectedBezier.origin.set( captivePosition );
		collectedBezier.destiny.set( COLLECTION_OFFSET_X + sexyShowArea.x + collectionPosX[ captiveType ] + ( parts[ captiveType ].getWidth() >> 1 ),
				COLLECTION_OFFSET_Y + sexyShowArea.y + ( parts[ captiveType ].getHeight() >> 1 ) );

		collectedBezier.control1.set( collectedBezier.destiny );
		collectedBezier.control2.set( collectedBezier.destiny );
	}


	public final void throwSomeCollectable() {
		if ( !captiveVisible ) {
			for( byte i = 0; i < PART_TYPES; i++ ) {
				if( partState[i] == PART_STATE_HIDE ) {
					throwCollectable( i, 5000 );
					return; // se adicionar mais codigo embaixo lembrar de tirar essa linha
				}
			}
		}
	}


	private final void throwCollectable( byte partType, int time ) {
		if( !captiveVisible ) {
			captiveVisible = true;
			captiveType = partType;

			speedX.setSpeed( 0 );
			speedY.setSpeed( 0 );

			move = ( byte ) NanoMath.randInt( 4 );

			switch ( move ) {
				case Key.MOVE_RIGHT:
					speedX.setSpeed( getWidth() * 1000 / time );
					captivePosition.set( new Point( -( MAX_WIDTH >> 1 ) , topLeft.y + NanoMath.randInt( bottomRight.y - topLeft.y ) ) );
				break;

				case Key.MOVE_LEFT:
					speedX.setSpeed( -getWidth() * 1000 / time );
					captivePosition.set( new Point( getWidth() + ( MAX_WIDTH >> 1 ) , topLeft.y + NanoMath.randInt( bottomRight.y - topLeft.y ) ) );
				break;

				case Key.MOVE_UP:
					speedY.setSpeed( -getHeight() * 1000 / time );
					captivePosition.set( new Point( topLeft.x + NanoMath.randInt( bottomRight.x - topLeft.x ), getHeight() + ( MAX_HEIGHT >> 1 ) ) );
				break;

				case Key.MOVE_DOWN:
					speedY.setSpeed( getHeight() * 1000 / time );
					captivePosition.set( new Point( topLeft.x + NanoMath.randInt( bottomRight.x - topLeft.x ), -( MAX_HEIGHT >> 1 ) ) );
				break;
			}

			partState[ captiveType ] = PART_STATE_SHOWING;
			insertDrawable( parts[ captiveType ] );
		}
	}


	private final void collectionRevision( boolean incinerate ) {
		for( byte i = 0; i < PART_TYPES; i++ ) {
			if( partState[i] != PART_STATE_COLLECTED ) {
				hideCollection();
				return;
			}
		}
		if ( incinerate )
			incinerate();
	}


	private final void incinerate() {
		reset();
		collection.insertDrawable( incineration );
		incineration.setVisible( true );
		incineration.setPosition( 0, 0 );
		incineration.setSequence( SEQUENCE_INCINERATION );
		gameScreen.executeSexyHotSpecial();
		MediaPlayer.play( SOUND_SEXY_SPECIAL );
	}


	public final void addToCollection( boolean incinerate ) {
		removeDrawable( parts[ captiveType ] );
		parts[ captiveType ].setPosition( COLLECTION_OFFSET_X + collectionPosX[ captiveType ] , COLLECTION_OFFSET_Y );
		collection.insertDrawable( parts[ captiveType ] );
		partState[ captiveType ] = PART_STATE_COLLECTED;
		collectionRevision( incinerate );
		captiveVisible = false;
	}


	public final void demo() {
		for( byte i = 0; i < partState.length - 1; i++ ) {
			throwCollectable( i, 1 );
			addToCollection( false );
		}
		setSize( TOTAL_WIDTH, MAX_HEIGHT );
	}


	public static final DrawableGroup creatDemo() throws Exception  {
		final DrawableGroup ret = new DrawableGroup( 5 );

		final DrawableImage s = new DrawableImage( PATH_IMAGES + "game_powerup_s.png" );
		ret.insertDrawable( s );

		final DrawableImage e = new DrawableImage( PATH_IMAGES + "game_powerup_e.png" );
		e.defineReferencePixel( ANCHOR_CENTER );
		e.setPosition( s.getPosX() + s.getWidth(), 0 );
		ret.insertDrawable( e );

		final DrawableImage x = new DrawableImage( PATH_IMAGES + "game_powerup_x.png" );
		x.defineReferencePixel( ANCHOR_CENTER );
		x.setPosition( e.getPosX() + e.getWidth(), 0 );
		ret.insertDrawable( x );

		final DrawableImage y = new DrawableImage( PATH_IMAGES + "game_powerup_y.png" );
		y.defineReferencePixel( ANCHOR_CENTER );
		y.setPosition( x.getPosX() + x.getWidth(), 0 );
		ret.insertDrawable( y );

		final DrawableImage hot = new DrawableImage( PATH_IMAGES + "game_powerup_hot.png" );
		hot.defineReferencePixel( ANCHOR_CENTER );
		hot.setPosition( y.getPosX() + y.getWidth(), 0 );
		ret.insertDrawable( hot );
		
		final int max = NanoMath.max( s.getHeight(),
				NanoMath.max( e.getHeight(),
				NanoMath.max( x.getHeight(),
				NanoMath.max( y.getHeight(),
				hot.getHeight() ) ) ) );
		
		ret.setSize( hot.getPosX() + hot.getWidth(), max );

		return ret;
	}


	public final void removeCaptive() {
		removeDrawable( parts[ captiveType ] );
		partState[ captiveType ] = PART_STATE_HIDE;
		captiveVisible = false;
	}


	private final void hideCollection() {
		collectionState = STATE_HIDING;
		accTime = 0;

		bezier.origin.set( collection.getPosX(), collection.getPosY() );
		bezier.destiny.set( sexyHideArea.x , sexyHideArea.y );

		bezier.control1.set( bezier.destiny );
		bezier.control2.set( bezier.destiny );
	}


	private final void showCollection() {
		collectionState = STATE_APPEARING;
		accTime = 0;

		bezier.origin.set( collection.getPosX(), collection.getPosY() );
		bezier.destiny.set( sexyShowArea.x , sexyShowArea.y );

		bezier.control1.set( bezier.destiny );
		bezier.control2.set( bezier.destiny );
	}
	
	
	public final void update( int delta ) {
		super.update( delta );

		if( accTime < ANIM_TOTAL_TIME ) {
			accTime += delta;
			final Point p = new Point();
			bezier.getPointAtFixed( p, NanoMath.divInt( accTime, ANIM_TOTAL_TIME ) );
			collection.move( p.x - collection.getPosX(), p.y - collection.getPosY() );

			if ( accTime >= ANIM_TOTAL_TIME ) {
				if( collectionState == STATE_APPEARING ) {
					collectionState = STATE_SHOWN;
				} else if( collectionState == STATE_HIDING ) {
					collectionState = STATE_HIDDEN;
				}
			}
		}

		if( collectAccTime < ANIM_TOTAL_TIME ) {
			collectAccTime += delta;
			final Point p = new Point();
			collectedBezier.getPointAtFixed( p, NanoMath.divInt( collectAccTime, ANIM_TOTAL_TIME ) );
			captivePosition.addEquals( p.sub( captivePosition ) );

			if ( collectAccTime >= ANIM_TOTAL_TIME ) {
				addToCollection( true );
			}
		}


		if ( partState[ captiveType ] == PART_STATE_SHOWING ) {
			captivePosition.addEquals( speedX.updateInt( delta ), speedY.updateInt( delta ) );

			switch ( move ) {
				case Key.MOVE_LEFT:
					if ( captivePosition.x <= -( MAX_WIDTH >> 1 ) )
						removeCaptive();
				break;

				case Key.MOVE_RIGHT:
					if ( captivePosition.x >= getWidth() + ( MAX_WIDTH >> 1 ) )
						removeCaptive();
				break;

				case Key.MOVE_UP:
					if ( captivePosition.y <= -( MAX_HEIGHT >> 1 ) )
						removeCaptive();
				break;

				case Key.MOVE_DOWN:
					if ( captivePosition.y >= getHeight() + ( MAX_HEIGHT >> 1 ) )
						removeCaptive();
				break;
			}
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		final int INTERNAL_WIDTH	= width * 76 / 100;
		final int INTERNAL_HEIGHT	= height * 76 / 100;

		topLeft.set( ( width - INTERNAL_WIDTH ) >> 1, ( ( height - INTERNAL_HEIGHT ) >> 1 ) );
		bottomRight.set( topLeft.add( INTERNAL_WIDTH, INTERNAL_HEIGHT ) );

		sexyShowArea.set( ( width - collection.getWidth() ) >> 1, SAFE_MARGIN, collection.getWidth(), collection.getHeight() );
		sexyHideArea.set( ( width - collection.getWidth() ) >> 1, - ( SAFE_MARGIN + collection.getHeight() ), collection.getWidth(), collection.getHeight() );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if ( sequence == SEQUENCE_INCINERATION ) {
			if ( incineration.isVisible() ) {
				reset();
				hideCollection();
			}
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
}
