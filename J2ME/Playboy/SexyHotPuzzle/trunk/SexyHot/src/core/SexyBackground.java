/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
public class SexyBackground extends UpdatableGroup implements Constants, ScreenListener {
	//#if SCREEN_SIZE == "SMALL"
//# 		private final int BKG_COLOR = 0x760303;
	//#else
		private final Pattern top;
		private final Pattern bottom;
	//#endif
	private final DrawableImage decalLT; // decal from the left top of screen
	private final DrawableImage decalRT; // decal from the right top of screen
	private final DrawableImage decalLM; // decal from the left middle of screen
	private final DrawableImage decalRB; // decal from the right bottom of screen
	private final DrawableImage decalLB; // decal from the left bottom of screen

	private final Rectangle[] viewports = new Rectangle[4];
	private boolean usingMultipleViewports = false;


	public SexyBackground() throws Exception {
		super( 7 );
		//#if SCREEN_SIZE != "SMALL"
			top = new Pattern( new DrawableImage( PATH_IMAGES + "bkg_top.png" ) );
			insertDrawable( top );
			bottom = new Pattern( new DrawableImage( PATH_IMAGES + "bkg_bottom.png" ) );
			insertDrawable( bottom );
		//#endif
		decalLT = new DrawableImage( PATH_IMAGES + "bkg_decal_0.png" );
		insertDrawable( decalLT );
		decalRT = new DrawableImage( PATH_IMAGES + "bkg_decal_1.png" );
		insertDrawable( decalRT );
		decalLM = new DrawableImage(decalLT);
		insertDrawable( decalLM );
		decalRB = new DrawableImage(decalLT);
		insertDrawable( decalRB );
		decalLB = new DrawableImage(decalRT);
		decalLB.mirror( TRANS_MIRROR_H );
		insertDrawable( decalLB );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void setSize( int width, int height ) {
		width = NanoMath.max( width, 10 );
		height = NanoMath.max( height, 10 );
		super.setSize( width, height );

		//#if SCREEN_SIZE != "SMALL"
			//setViewport( new Rectangle( 0, 0, ( width >> 1 ) , ( height >> 1 ) ) );
			//#if DEBUG == "true"
				System.out.println( "setSize( " + width + ", " + height + " )"  );
			//#endif
			bottom.setSize(width, bottom.getFill().getHeight());
			bottom.setPosition( 0, (height - bottom.getHeight()) + 1 );
			top.setSize( width, bottom.getPosY() );
			top.setPosition( 0, 0 );
		//#endif
		
		decalLT.setPosition( NanoMath.randInt( width >> 2 ) , NanoMath.randInt( 20 ) );
		decalRT.setPosition( width - decalRT.getWidth() , 0);
		decalLM.setPosition( NanoMath.randInt( width >> 2 ) , decalLT.getPosY() + decalLT.getHeight() + ( ( ( 16 + NanoMath.randInt( 32 ) ) * ( height - ( decalLT.getPosY() + decalLT.getHeight() + decalLB.getHeight() + decalLM.getHeight() ) ) ) >> 6 ) );
		decalRB.setPosition( width - ( decalRB.getWidth() + NanoMath.randInt( width >> 2 ) ) , decalRT.getPosY() + decalRT.getHeight() + ( ( ( 16 + NanoMath.randInt( 32 ) ) * ( height - ( decalRT.getPosY() + decalRT.getHeight() + decalRB.getHeight() ) ) ) >> 6 ) );
		decalLB.setPosition( 0, height - decalLB.getHeight() );
	}


	public void setViewportBlocker( Rectangle r ) {
		viewports[0] =  new Rectangle( 0, 0, getWidth(), r.y );
		viewports[1] =  new Rectangle( 0, r.y, r.x, r.height );
		viewports[2] =  new Rectangle( 0, ( r.y + r.height ), getWidth(), ( getHeight() - ( r.y + r.height ) ) );
		viewports[3] =  new Rectangle( ( r.x + r.width ), r.y, getWidth() - ( r.x + r.width ) , r.height );
		usingMultipleViewports = true;
	}


	public void setUnblockViewport( ) {
		usingMultipleViewports = false;
		setViewport( new Rectangle( getPosX(), getPosY(), getWidth(), getHeight() ) );
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void draw( Graphics g ) {
		//#if SCREEN_SIZE == "SMALL"
//# 			g.setColor( BKG_COLOR );
//# 			g.fillRect( translate.x, translate.y, getWidth(), getHeight() );
//# 			super.draw( g );
		//#else
			if( usingMultipleViewports && viewports != null ) {
				for( int i = 0; i < viewports.length; i++ ) {
					setViewport( viewports[i] );
					super.draw( g );
				}
			} else {
				super.draw( g );
			}
		//#endif
	}


	public void paint( Graphics g ) {
		//#if SCREEN_SIZE == "SMALL"
//# 			super.paint( g );
		//#else
			if( usingMultipleViewports && viewports != null ) {
				for( int i = 0; i < viewports.length; i++ ) {
					setViewport( viewports[i] );
					super.paint( g );
				}
			} else {
				super.paint( g );
			}
		//#endif
	}

	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}
}