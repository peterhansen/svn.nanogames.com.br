/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.SexyLabel;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Caio
 */
public abstract class BasicScreen  extends UpdatableGroup implements Constants, ScreenListener, KeyListener, Updatable
//#if TOUCH == "true"
	, PointerListener
//#endif
{
	protected final GameMIDlet midlet;
	protected final int screenID;
	
	private final SexyLabel screenTittle;
	protected final DrawableGroup tittleGroup;
	private static DrawableImage sexyLogo;
	
	private final boolean hasBorder;
	private final boolean hasTittle;
	private final boolean hasLogo;
	private final boolean hasBkg;
	
	protected final Rectangle bkgArea;
	
	public BasicScreen( int slots, GameMIDlet midlet, int screen, boolean hasTittle, String tittle , boolean hasLogo, boolean hasBkg, boolean hasBorder ) throws Exception {
		super( slots + 1 );

		screenID = screen;
		this.hasBkg = hasBkg;
		this.midlet = midlet;
		this.hasTittle = hasTittle;
		this.hasLogo = hasLogo;
		this.hasBorder = hasBorder;
		
		if( hasTittle ) {
			tittleGroup = new DrawableGroup( 1 );

			screenTittle = new TittleLabel( tittle, true );
			screenTittle.defineReferencePixel( ANCHOR_CENTER );
			tittleGroup.insertDrawable( screenTittle );			
		} else {
			tittleGroup = null;
			screenTittle = null;
		}
		
		if( hasLogo ) {
			sexyLogo = new DrawableImage( GameMIDlet.getLogo() );
			sexyLogo.defineReferencePixel( ANCHOR_HCENTER );
		} else {
			sexyLogo = null;
		}
		
		if( hasBkg ) {
			bkgArea = new Rectangle();
		} else {
			bkgArea = null;
		}
	}


	public final void setTitle( String text ) {
		screenTittle.setText( text );
	}
	
	
	public void setSize ( int width, int height ) {
		super.setSize( width, height );
		if( hasTittle ) {
			tittleGroup.setSize( width, screenTittle.getHeight() << 1 );
			screenTittle.setRefPixelPosition( tittleGroup.getWidth() >> 1, tittleGroup.getHeight() >> 1 );
		}
		
		if( hasBkg ) {
			bkgArea.width = width - ( SAFE_MARGIN << 1 );
			if( hasTittle ) {
				bkgArea.height = height - ( tittleGroup.getHeight() + ( SAFE_MARGIN << 1 ) );
				bkgArea.y = tittleGroup.getHeight();
			} else {
				bkgArea.height = height - ( SAFE_MARGIN << 1 );
				bkgArea.y = SAFE_MARGIN;
			}
			bkgArea.x = SAFE_MARGIN;

			GameMIDlet.setBkgBlocker( new Rectangle( bkgArea.x, bkgArea.y, bkgArea.width, bkgArea.height ) );
		}
		
		if( hasLogo ) {
			sexyLogo.setRefPixelPosition( width >> 1, height - ( SEXY_LOGO_MIN_MARGIN + sexyLogo.getHeight() ) );
		}
	}

	public void exit() {
		midlet.onChoose( null, screenID, TEXT_EXIT );
	}


	public final void draw ( Graphics g ) {
		/* Draw of Back Layer */
		if( hasBkg ) {

			g.setColor( COLOR_BACKGROUND );
			g.fillRect( bkgArea.x , bkgArea.y , bkgArea.width , bkgArea.height );

			if( hasBorder ) {
				int x = bkgArea.x;
				int y = bkgArea.y;
				int w = bkgArea.width;
				int h = bkgArea.height;

				x--; y--; w++; h++; // Reposicionamento das coordenadas

				g.setColor( COLOR_BACKGROUND_BORDER_HORIZONTAL_DECAL );
				g.drawLine( x, y, x + w, y );
				g.drawLine( x, y + h, x + w, y + h );

				g.setColor( COLOR_BACKGROUND_BORDER_VERTICAL_DECAL );
				g.drawLine( x, y, x, y + h );
				g.drawLine( x + w, y, x + w, y + h );

				x--; y--; w+=2; h+=2; // Reposicionamento das coordenadas

				g.setColor( COLOR_BACKGROUND_BORDER );

				g.drawRect( x, y, w, h ); // Desenha a borda do retangle

				x--; y--; w+=2; h+=2; // Reposicionamento das coordenadas

				g.drawRect( x, y, w, h ); // Desenha a borda do retangle
			}
		} /* end of draw of Back Layer */

		super.draw( g );

		/* Draw of Front Layer */
		tittleGroup.draw( g );
		sexyLogo.draw( g );
		/* end of draw of Front Layer */

		drawFrontLayer( g );
	}


	public abstract void drawFrontLayer( Graphics g );


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public void keyPressed( int key ) {
	}


	public void keyReleased( int key ) {
	}

//#if DEBUG == "true"
	public void onPointerDragged( int x, int y ) {
	}


	public void onPointerPressed( int x, int y ) {
	}


	public void onPointerReleased( int x, int y ) {
	}
//#endif
}
