/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.UpdatableGroup;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public class MenuLabel extends UpdatableGroup implements Constants
{
	private static final byte TEXT_SPACING = 10;

	protected final SexyLabel state;
	protected final Drawable tittle;
	protected boolean tittleExist;

//	private int minWidth;
//	private int maxWidth;

	public MenuLabel( int tittleText, int stateText ) throws Exception {
		this( GameMIDlet.getText( tittleText ), GameMIDlet.getText( stateText ) );
	}

	public MenuLabel( String tittleText, String stateText ) throws Exception {
		super( 2 );

		state = new SexyLabel( stateText , false, GameMIDlet.getBarLeft(), GameMIDlet.getBarCenter(), FONT_NIVEL );
		insertDrawable( state );

		tittleExist = tittleText != "";
		if( GameMIDlet.isLowMemory() ) {
			if( tittleExist )
				tittle = new TittleLabel( tittleText, false );
			else
				tittle = null;
		} else {
			tittle = new MarqueeLabel( GameMIDlet.GetFont( FONT_VIB_VOL ), tittleText );
			( (MarqueeLabel)tittle ).setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
		}

		if( tittleExist )
			insertDrawable( tittle );
		
		setSize( state.getWidth() + ( tittleExist ? tittle.getWidth() : 0 ) + ( ( tittleExist ? 3 : 2 ) * TEXT_SPACING ) , state.getHeight() );
	}


	public void setFont( int fontId ) {
		state.setFont( fontId );
	}


	public void setValueText( String text ) {
		state.setText( text );
	}
	

	public void setSize( int width, int height ) {		
		super.setSize( width, height );

		//#if DEBUG == "true"
			System.out.println( "MenuLabel.setSize( " + width + ", " + height + " );" );
		//#endif

		if( tittleExist )
			tittle.setPosition( TEXT_SPACING, ( height - tittle.getHeight() ) >> 1 );
		
		state.setMaxWidth( getWidth() - ( ( tittleExist ? tittle.getWidth() : 0 ) + ( ( tittleExist ? 3 : 2 ) * TEXT_SPACING ) ) );
		state.setSize( getWidth() - ( ( tittleExist ? tittle.getWidth() : 0 ) + ( ( tittleExist ? 3 : 2 ) * TEXT_SPACING ) ), height );
		state.setPosition( getWidth() - ( state.getWidth() + TEXT_SPACING ), ( height - state.getHeight() ) >> 1 );
	}
}
