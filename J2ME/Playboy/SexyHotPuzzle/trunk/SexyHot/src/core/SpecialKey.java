/**
 * SpecialKey.java
 * 
 * Created on Sep 9, 2009, 5:48:05 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class SpecialKey extends UpdatableGroup implements Constants, SpriteListener {

	private static final byte TOTAL_SHINES = 3;

	public static final byte SPICY_TYPES = 3;
	public static final byte SPICY_NORMAL = 0;
	public static final byte SPICY_ICE = 1;
	public static final byte SPICY_RADIATION = 2;

	private final Sprite[] specialPieceShine;

	private final Sprite spicy;

	private static Sprite SPICY;

	private short specialPieceTime;

	private short blinkTime;

	private static final short TIME_SPECIAL_PIECE_BLINK = 186;
	private static final short TIME_SPECIAL_PIECE_APPEAR = 411;
	private static final short TIME_SPECIAL_PIECE_VISIBLE = 3300;

	public static final byte SPECIAL_PIECE_STATE_NONE		= 0;
	public static final byte SPECIAL_PIECE_STATE_VISIBLE	= 1;

	private byte specialPieceState;

	private final Drawable parent;
	public int type;

	private final MUV speedX = new MUV();
	private final MUV speedY = new MUV();

	private final KeyManager manager;

	/** índice do tipo de movimento atual. */
	private byte move;

	private short stateTime;

	private final BezierCurve bezier = new BezierCurve();

	private byte direction;

	private byte initialDirection;

	private short totalTime = 10000;

	private Rectangle area= new Rectangle();

	/***/
	private final Point startPosition = new Point();
	
	
	public SpecialKey( KeyManager parent ) throws Exception {
		super( 2 + TOTAL_SHINES );

		this.parent = parent;

		manager = parent;

		spicy = new Sprite( SPICY );
		insertDrawable( spicy );

		setSize( spicy.getSize() );
		area.set( new Point(), spicy.getSize() );

		spicy.setPosition( ( size.x - spicy.getWidth() ) >> 1, ( size.y - spicy.getHeight() ) >> 1 );

		type = 0;
		spicy.setFrame( type );

		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );

		if ( !GameMIDlet.isLowMemory() ) {
			Sprite s;
			s = new Sprite( PATH_IMAGES + "ss" );
			GameMIDlet.log( "s" );

			specialPieceShine = new Sprite[ TOTAL_SHINES ];
			for ( byte i = 0; i < TOTAL_SHINES; ++i ) {
				final Sprite temp = new Sprite( s );
				specialPieceShine[ i ] = temp;

				temp.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
				temp.setListener( this, i );
				temp.update( NanoMath.randInt( 180 ) );

				insertDrawable( temp );
				GameMIDlet.log( "shine " + i );
			}
		} else {
			specialPieceShine = null;
		}

		setState( SPECIAL_PIECE_STATE_NONE );
	}


	public static final void load() throws Exception {
		SPICY = new Sprite( PATH_IMAGES + "game_spicy" );
	}


	public final void onSequenceEnded( int id, int sequence ) {
//		if ( specialPieceState != SPECIAL_PIECE_STATE_NONE )
			setSpecialPieceShineRandomPos( id );
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	public final byte getState() {
		return specialPieceState;
	}


	private final void setSpecialPieceShineRandomPos( int index ) {
		if ( !GameMIDlet.isLowMemory() ) {
			specialPieceShine[ index ].setRefPixelPosition( ( specialPieceShine[0].getWidth() >> 1 ) + NanoMath.randInt( getWidth() - specialPieceShine[0].getWidth() ),
					( specialPieceShine[0].getHeight() >> 1 ) + NanoMath.randInt( getHeight() - specialPieceShine[0].getHeight() ) );
			specialPieceShine[ index ].update( NanoMath.randInt( 80 ) );
		}
	}


	public boolean intersects( Rectangle rect ) {
		return area.intersects( rect );
	}


	public final void resetMove( int time ) {
		speedX.setSpeed( 0 );
		speedY.setSpeed( 0 );

		move = ( byte ) NanoMath.randInt( 4 );

		switch ( move ) {
			case Key.MOVE_RIGHT:
				speedX.setSpeed( manager.getWidth() * 1000 / time );
				startPosition.set( new Point( -getWidth() , Key.managerTopLeft.y + NanoMath.randInt( Key.managerBottomRight.y - Key.managerTopLeft.y ) ) );
			break;

			case Key.MOVE_LEFT:
				speedX.setSpeed( -manager.getWidth() * 1000 / time );
				startPosition.set( new Point( manager.getWidth() , Key.managerTopLeft.y + NanoMath.randInt( Key.managerBottomRight.y - Key.managerTopLeft.y ) ) );
			break;

			case Key.MOVE_UP:
				speedY.setSpeed( -manager.getHeight() * 1000 / time );
				startPosition.set( new Point( Key.managerTopLeft.x + NanoMath.randInt( Key.managerBottomRight.x - Key.managerTopLeft.x ), manager.getHeight() ) );
			break;

			case Key.MOVE_DOWN:
				speedY.setSpeed( manager.getHeight() * 1000 / time );
				startPosition.set( new Point( Key.managerTopLeft.x + NanoMath.randInt( Key.managerBottomRight.x - Key.managerTopLeft.x ), -getHeight() ) );
			break;
		}

		//#if DEBUG == "true"
			System.out.println( " ------------------------------------------------------- " );
			System.out.println( " *** Spicy " + type + " -> resetMove: " + move + " -> " + startPosition );
			System.out.println( " ------------------------------------------------------- " );
		//#endif
		setPosition( startPosition );

		stateTime = 0;
		totalTime = ( short ) time;
		setVisible( true );
	}


	public final void demo( int Type ) {
		type = Type;
		spicy.setFrame( type );
		spicy.setPosition( 0, 0 );
		setVisible( true );
		for ( byte i = 0; i < TOTAL_SHINES; ++i )
			setSpecialPieceShineRandomPos( i );
	}


	public final void setState( int state ) {
		setState( state, NanoMath.randInt( SPICY_TYPES ) );
	}


	public final void setState( int state, int Type ) {
		switch ( state ) {
			case SPECIAL_PIECE_STATE_NONE:
				setVisible( false );
			break;

			case SPECIAL_PIECE_STATE_VISIBLE:
				if ( state != specialPieceState ) {
					type = Type;
					spicy.setFrame( type );
					resetMove( 4000 );
					for ( byte i = 0; i < TOTAL_SHINES; ++i )
						setSpecialPieceShineRandomPos( i );
				}

				specialPieceTime = 0;
				setVisible( true );

			break;
		}

		specialPieceState = ( byte ) state;
	}


	public final void prepare() {
		setState( SPECIAL_PIECE_STATE_NONE );
	}


	private final void movementEnd() {
		setState( SPECIAL_PIECE_STATE_NONE );
	}


	public final void update( int delta ) {
		super.update( delta );

		// TODO fazer mover como as chaves se movem

		switch ( specialPieceState ) {
			case SPECIAL_PIECE_STATE_VISIBLE:
				move( speedX.updateInt( delta ), speedY.updateInt( delta ) );
				area.set( getPosition(), spicy.getSize() );

				switch ( move ) {
					case Key.MOVE_LEFT:
						if ( getPosX() <= -KEY_DEFAULT_WIDTH )
							movementEnd();
					break;

					case Key.MOVE_RIGHT:
						if ( getPosX() >= manager.getWidth() )
							movementEnd();
					break;

					case Key.MOVE_UP:
						if ( getPosY() <= -KEY_DEFAULT_HEIGHT )
							movementEnd();
					break;

					case Key.MOVE_DOWN:
						if ( getPosY() >= manager.getHeight() )
							movementEnd();
					break;
				}
			break;
		}
	}

}
