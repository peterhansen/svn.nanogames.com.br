/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class NumberLabel extends TextBox implements Constants, Selectable {
	public boolean changed = false;
	public final byte MAX_NUMBERS;
	private boolean selected = false;
	private final String initialText;

	public NumberLabel( String initialText, byte maxNumbers ) throws Exception {
		super( GameMIDlet.GetFont( FONT_TEXT_WHITE ), null, maxNumbers, TextBox.INPUT_MODE_NUMBERS );
		this.initialText = initialText;
		clearNumbers();
		MAX_NUMBERS = maxNumbers;
	}

	public final void setSize( int width, int height ) {
		super.setSize( width, NUMBER_LABEL_HEIGHT );
		resizeText();
	}


	public final void setText( String s, boolean b ) {
		super.setText( s, true );
	}


	private final void resizeText() {
		label.setSize( NanoMath.min( getWidth(), label.getTextWidth() + 2 ), getHeight() );
//		label.setSize( getSize() );
		label.setPosition( ( ( getWidth() - label.getWidth() ) >> 1 ), ( ( getHeight() >> 1 ) ) - ( FONT_WHITE_HEIGHT >> 1 ) );
	}


	public final void draw( Graphics g ) {
		if ( selected )
			g.setColor( 0x870808 ); // TODO tirar constante mágica
		else
			g.setColor( COLOR_BACKGROUND );

		g.fillRect( getPosX(), getPosY(), getWidth(), getHeight());

		if ( selected )
			g.setColor( 0xfff0ce ); // TODO tirar constante mágica
		else
			g.setColor( COLOR_BACKGROUND_BORDER_HORIZONTAL_DECAL );

		g.drawRect( getPosX() - 1, getPosY() - 1, getWidth() + 1, getHeight() + 1);

		if ( selected )
			g.setColor( 0xe8aa26 ); // TODO tirar constante mágica
		else
			g.setColor( COLOR_BACKGROUND_BORDER );
		
		g.drawRect( getPosX() - 2, getPosY() - 2, getWidth() + 3, getHeight() + 3);
		g.drawRect( getPosX() - 3, getPosY() - 3, getWidth() + 5, getHeight() + 5);
		super.draw( g );
	}


	public int length() {
		return getText().length();
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM0:
			case ScreenManager.KEY_NUM1:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM3:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.KEY_NUM7:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM9:
				insertChar( ( char )( '0' + ( key - ScreenManager.KEY_NUM0 ) ) );
			break;
			
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				backspace();
			break;
		}
	}


	public final void insertChar( char c ) {
		if( changed ) {
			if( label.getText().length() < MAX_NUMBERS ) {
				super.insertChar( c );
			}
		} else {
			label.setText( String.valueOf( c ) );
			changed = true;
		}
		resizeText();
	}


	public final void backSpace() {
		if( changed ) {
			if( label.getText().length() > 0 ) {
				super.backSpace();
//				label.setText( label.getText().substring( 0, label.getText().length() - 1 ) );
			}
			if( label.getText().length() > 0 ) {
				resizeText();
			} else {
				clearNumbers();
			}
		}
	}


	public final void clearNumbers() {
		changed = false;
		label.setText( initialText );
		resizeText();
	}


	public final void select() {
		selected = true;
	}


	public final void unselect() {
		selected = false;
	}

	public final boolean contains( int x, int y ) {
		return super.contains( x, y );
	}


	public TextBox textBox() {
		return this;
	}
}
