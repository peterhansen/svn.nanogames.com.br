/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class HUD extends UpdatableGroup implements Constants {

	public static final int COLOR_CLOCK_FILL = 0xC1C1C0;
	public static final int COLOR_BAR_FILL = 0x975507;

	private short degrees;

	private final Pattern comboBarFull;

	private final Label labelCombo;

	private final DrawableImage clock;
	private final DrawableImage bonusBar;

	private static final short TIME_BLINK = 250;
	private static final short TIME_COMBO_BLINK = 140;

	private int colorDark;

	private long lastColorTime;
	private long lastComboColorTime;


	public HUD()  throws Exception {
		super( 6 );

		bonusBar = new DrawableImage( PATH_IMAGES + "game_bonus_bar.png" );
		insertDrawable( bonusBar );

		comboBarFull = new Pattern( COLOR_BAR_FILL );
		comboBarFull.setSize( 0, BONUS_BAR_HEIGHT );
		insertDrawable( comboBarFull );

		labelCombo = new Label( FONT_BONUS, null );
		insertDrawable( labelCombo );
		
		clock = new DrawableImage( PATH_IMAGES + "game_clock.png" );
		clock.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( clock );

		setSize();
	}
	
	
	public final void setSize() {
		setSize( ScreenManager.SCREEN_WIDTH , clock.getHeight() + ( CLOCK_SAFE_MARGIN << 1 ) );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		bonusBar.setPosition( SAFE_MARGIN, height - ( HUD_PUZZLE_DISTANCE + bonusBar.getHeight() ) );
		comboBarFull.setPosition( bonusBar.getPosX() + BONUS_BAR_X ,  bonusBar.getPosY() + BONUS_BAR_Y );
		clock.setRefPixelPosition( width >> 1, height >> 1 );
	}


	public final void setScore( int score ) {

	}


	protected final void paint( Graphics g ) {
		final int x = translate.x + clock.getRefPixelX() - ( CLOCK_DIAMETER >> 1 );
		final int y = translate.y + clock.getRefPixelY() - ( CLOCK_DIAMETER >> 1 );
		if(colorDark != COLOR_CLOCK_FILL) {
			g.setColor( colorDark );
			g.fillArc( x, y, CLOCK_DIAMETER, CLOCK_DIAMETER, 0, 360 );
		}

		if ( degrees > 0 ) {
			g.setColor( COLOR_CLOCK_FILL );
			g.fillArc( x, y, CLOCK_DIAMETER, CLOCK_DIAMETER, 90, -degrees );
		}

		super.paint( g );
	}


	public final void setProgress( int timeLeft, int timeMax, int comboTime ) {
		degrees = ( short ) ( NanoMath.clamp( 360 * timeLeft / timeMax, 0, 360 ) );
		comboBarFull.setSize( comboTime * BONUS_BAR_WIDTH / TIME_COMBO, comboBarFull.getHeight() );
//		comboBarEmpty.setSize( BONUS_BAR_WIDTH - comboBarFull.getWidth(), comboBarEmpty.getHeight() );
//		comboBarEmpty.setPosition( comboBarFull.getPosX() + comboBarFull.getWidth(), comboBarEmpty.getPosY() );

		if ( timeLeft <= 0 ) {
			colorDark = COLOR_CLOCK_FILL;
		} else if ( timeLeft <= ( timeMax >> 2 ) ) {
			final long diff = System.currentTimeMillis() - lastColorTime;
			if ( diff > TIME_BLINK ) {
				colorDark = colorDark == COLOR_CLOCK_FILL ? COLOR_BAR_FILL : COLOR_CLOCK_FILL;
				lastColorTime = System.currentTimeMillis();
			}
		} else {
			colorDark = COLOR_CLOCK_FILL;
		}

		if ( comboTime <= TIME_COMBO_START_BLINK ) {
			final long diff = System.currentTimeMillis() - lastComboColorTime;
			if ( diff > TIME_COMBO_BLINK ) {
				comboBarFull.setFillColor( comboBarFull.getFillColor() == COLOR_BAR_FILL ? COLOR_CLOCK_FILL : COLOR_BAR_FILL );
				lastComboColorTime = System.currentTimeMillis();
			}
		} else {
			comboBarFull.setFillColor( COLOR_BAR_FILL );
		}
	}


	public final void setCombo( int combo ) {
		if ( combo > 1 ) {
			labelCombo.setVisible( true );
			//#if  SCREEN_SIZE == "BIG" ||  SCREEN_SIZE == "GIANT"
				labelCombo.setText( "" + combo + "x" ); //TEMP
			//#else
//# 				labelCombo.setText( combo + "x" );
			//#endif
			labelCombo.setPosition( comboBarFull.getPosX() + BONUS_BAR_X,
									comboBarFull.getPosY() + ( ( comboBarFull.getHeight() - labelCombo.getHeight() ) >> 1 ) );
		} else {
			labelCombo.setVisible( false );
		}
	}
	
	
	
//	public final void update( int delta ) {
//		// atualiza a pontua��o
//		updateScore( delta, false );
//	}
//
//
//	/**
//	 * Atualiza o label da pontua��o.
//	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
//	 */
//	private final void updateScore( int delta, boolean equalize ) {
//		if ( scoreShown < score ) {
//			if ( equalize ) {
//				scoreShown = score;
//			} else  {
//				scoreShown += scoreSpeed.updateInt( delta );
//				if ( scoreShown >= score ) {
//					if ( scoreShown > SCORE_SHOWN_MAX )
//						scoreShown = SCORE_SHOWN_MAX;
//
//					scoreShown = score;
//				}
//			}
//			refreshScoreLabel();
//		}
//	} // fim do m�todo updateScore( boolean )
//
//
//	private final void refreshScoreLabel() {
////		AppMIDlet.gc();
////		scoreLabel.setText( String.valueOf( Runtime.getRuntime().freeMemory() ) );
//
//		// enche de zeros � esquerda do placar
//		final StringBuffer buffer = new StringBuffer();
//		for ( int s = 1; s <= SCORE_SHOWN_MAX + 1; s *= 10 ) {
//			if ( s - 1 > scoreShown )
//				buffer.append( '0' );
//		}
//		buffer.append( scoreShown );
//		scoreLabel.setText( buffer.toString() );
//		scoreLabel.setPosition( getWidth() - scoreLabel.getWidth() - CLOCK_SCORE_SPACING, scoreLabel.getPosY() );
//	}
}
