/**
 * Puzzle.java
 * 
 * Created on Jul 21, 2009, 3:40:08 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Puzzle extends DrawableGroup implements Constants, SpriteListener, Updatable {

	public static final short PUZZLE_MAX_PIECES = (PUZZLE_MAX_SIZE / BLOCK_SIZE) * (PUZZLE_MAX_SIZE / BLOCK_SIZE);
	public static short PUZZLE_USED_PIECES = 0;

	private static final byte SEQUENCE_CLEARED		= 0;
	private static final byte SEQUENCE_CLEARING		= 1;
	private static final byte SEQUENCE_REMAINING	= 2;
	private static final byte SEQUENCE_CHOSEN		= 3;

	public static final byte STATE_CLOSED		= 0;
	public static final byte STATE_OPENING		= 1;
	public static final byte STATE_OPEN			= 2;
	public static final byte STATE_CLOSING		= 3;

	private byte state = -1;

	public static final short TIME_ANIMATION = 1800;

	private short accTime;
	private short previousVisiblePieces = -1;

	private final Drawable seda;

//	private static Drawable background;

	private final Mutex mutex = new Mutex();

	private DrawableGroup bkgGroup;
	private Drawable bkg;

	/** �ndice das pe�as. */
	private byte[][] pieces;

	public short clearedPieces;

	private short initialPieces;

	private short totalPieces;

	private final byte DRAWABLE_OFFSET = 3;

	/** Imagem pr�-renderizada do background e das pe�as, para acelerar (MUITO) o desenho do quebra-cabe�as. */
	private static Image buffer;
	private final Sprite chain;
	private final Sprite locks;
	private final DrawableImage sexyLogo;

	public static int RealAreaWidth;
	public static int RealAreaHeight;

	private long lastTime = 0;
	public final Point realSize = new Point();

	private int  columns;
	private int  rows;

	private byte currentExplosionIndex;

	private static final byte MAX_EXPLOSIONS = 9;
	private static final short EXPLOSION_TIME = 400;

	private final Sprite[] explosions;
	private final int[] accExplosionTime;
	private final Point[] explosionPosition;
	public final byte[] isExploding;
	public final int[] lockIndex;
	public final boolean hasExplosions;

	private static final byte SEQUENCE_IDLE			= 0;
	private static final byte SEQUENCE_ANIMATING	= 1;


	public Puzzle() throws Exception {
		super( PUZZLE_MAX_PIECES + MAX_EXPLOSIONS + 3 );

		locks = new Sprite( PATH_IMAGES + "game_lock" );

		chain = new Sprite( PATH_IMAGES + "game_chain" );
		chain.setSequence( 1 );

		seda = GameMIDlet.getLogo();
		insertDrawable( seda );

		sexyLogo = new DrawableImage( GameMIDlet.getLogo() );

		bkgGroup = new DrawableGroup( 1 );
 		insertDrawable( bkgGroup );
		loadBackground( ( short ) 0 );
		
		for ( short i = 0; i <= PUZZLE_MAX_PIECES; ++i ) {
			final Sprite temp = new Sprite( locks );
			temp.setVisible( false );
			insertDrawable( temp );
		}

		if( !GameMIDlet.isLowMemory() ) {
			explosions = new Sprite[ MAX_EXPLOSIONS ];
			explosionPosition = new Point[ MAX_EXPLOSIONS ];
			accExplosionTime = new int[ MAX_EXPLOSIONS ];
			isExploding = new byte[ PUZZLE_MAX_PIECES ];
			lockIndex = new int[ MAX_EXPLOSIONS ];
			hasExplosions = true;

			final Sprite shine = new Sprite( PATH_IMAGES + "game_sexyfire" );
			for ( byte i = 0; i < MAX_EXPLOSIONS; ++i ) {
				explosionPosition[i] = new Point();
				final Sprite s = new Sprite( shine );
				explosions[ i ] = s;

				s.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				s.setVisible( false );
				s.setListener( this, i );
				insertDrawable( s );
			}
		} else {
			explosions = null;
			explosionPosition = null;
			accExplosionTime = null;
			isExploding = null;
			lockIndex = null;
			hasExplosions = false;
		}

		setState( STATE_CLOSED );
	}


	public final void loadBackground( short photoNumber ) {
		bkgGroup.removeAllDrawables();
		bkg = null;

		//#if DEBUG == "true"
			System.out.println( "------------> loadBackground <-----------" );
			System.out.println( "-----------------> "+ photoNumber +" <------------------" );
		//#endif

		try {
			
//			if( GameMIDlet.isLowMemory() ) {
//				bkg = new Pattern( 0x00ff00 );
//				bkg.setSize( PUZZLE_MAX_SIZE, PUZZLE_MAX_SIZE );
//			} else {
				bkg = new DrawableImage( PATH_GIRLS + ( NanoMath.min( photoNumber, TOTAL_GIRLS_PHOTOS - 1 ) ) + ".jpg" );
//			}
			bkgGroup.insertDrawable( bkg );
			bkgGroup.setSize( bkg.getSize() );
		} catch ( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
		}
	}


	public final void clearPiece( Sprite p, boolean refreshBuffer ) {
		if( setSpriteSequence( p, SEQUENCE_CLEARED ) ) {
			++clearedPieces;
		}

		if ( refreshBuffer ) {
			//#if DEBUG == "true"
//				System.out.println( "clearPiece() and refreshBuffer()" );
			//#endif
			refreshBuffer();
		} else {
			//#if DEBUG == "true"
//				System.out.println( "clearPiece( )" );
			//#endif
		}

		//#if DEBUG == "true"
			//System.out.println( "PE�AS VIS�VEIS: " + getRemainingCount() + " / " + totalPieces );
		//#endif
	}


	public final void clearAll() {
		clearedPieces = 0;
		for ( short i = 0; i < totalPieces; ++i ) {
			final Sprite s = getPiece( i );
			setSpriteSequence( s, SEQUENCE_REMAINING );
		}
	}


	public final short getTotalPieces() {
		return totalPieces;
	}


	public final void reset( int piecesShown , short photoNumber ) {
		loadBackground( photoNumber );
		reset( piecesShown );
	}


	public final void reset( int piecesShown ) {
		clearAll();

		//#if DEBUG == "true"
			System.out.println( "----------------> RESET <----------------" );
			System.out.println( "-----------------------------------------" );
		//#endif
		
		initialPieces = ( short ) ( totalPieces - piecesShown );
		fillPieces( initialPieces, false );

		refreshBuffer();

		setState( STATE_CLOSING );
	}


	protected final void paint( Graphics g ) {
		mutex.acquire();

//		//#if SCREEN_SIZE == "BIG" ||  SCREEN_SIZE == "GIANT"
			if ( buffer != null )
				g.drawImage( buffer, translate.x + PUZZLE_BORDER_THICKNESS, translate.y + PUZZLE_BORDER_THICKNESS, 0 );


			// na vers�o de baixa mem�ria, o buffer j� possui a imagem de background, e as pe�as vis�veis (escuras, que faltam
			// ser encaixadas) devem ser desenhadas sobre ele
//			if ( GameMIDlet.isLowMemory() ) {
//				final int TOTAL = totalPieces + DRAWABLE_OFFSET;
//				for ( int i = 0; i < TOTAL; ++i )
//					drawables[ i ].draw( g );
//			}

			if( buffer != null ) {
				g.setColor( COLOR_BACKGROUND_BORDER );
				g.drawRect( translate.x, translate.y, buffer.getWidth() + 7, buffer.getHeight() + 7 );
				g.drawRect( translate.x + 1, translate.y + 1, buffer.getWidth() + 5, buffer.getHeight() + 5 );
				g.drawRect( translate.x + 2, translate.y + 2, buffer.getWidth() + 3, buffer.getHeight() + 3 );
				g.setColor( COLOR_BACKGROUND_BORDER_VERTICAL_DECAL );
				g.drawLine( translate.x + 3, translate.y + 3, translate.x + 3, translate.y + buffer.getHeight() + 4 );
				g.drawLine( translate.x + buffer.getWidth() + 4, translate.y + 3, translate.x + buffer.getWidth() + 4, translate.y + buffer.getHeight() + 4 );
				g.setColor( COLOR_BACKGROUND_BORDER_HORIZONTAL_DECAL );
				g.drawLine( translate.x + 3, translate.y + 3, translate.x + buffer.getWidth() + 4, translate.y + 3 );
				g.drawLine( translate.x + 3, translate.y + buffer.getHeight() + 4, translate.x + buffer.getWidth() + 4, translate.y + buffer.getHeight() + 4 );
			}

			if( hasExplosions ) {
				for( int i = 0; i < explosions.length; i++ ) {
					if ( explosions[ i ].getSequenceIndex() != SEQUENCE_IDLE ) {
						final Sprite s = getPiece( lockIndex[ i ] );
						final int percentage = ( accExplosionTime[ i ] * BLOCK_SIZE ) / EXPLOSION_TIME; // TODO tirar constante mágica
						Rectangle area = new Rectangle( translate.x + s.getPosX(), translate.y + s.getPosY() + ( percentage ),
												s.getWidth(), s.getHeight() - percentage );
						drawBLock( s, g, area, true );
						explosions[ i ].setRefPixelPosition( s.getPosX() + ( BLOCK_SIZE >> 1 ) , s.getPosY() + ( percentage ) );
						explosions[ i ].draw( g );
					}
				}
			}
			
			sexyLogo.draw( g );
//		//#else
//			if ( buffer != null )
//				g.drawImage( buffer, translate.x, translate.y, 0 );
//		//#endif

		mutex.release();
	}


	private final void fillPieces( int totalCleared, boolean reset ) {
		if ( reset ) {
			reset( 0 );
			//#if DEBUG == "true"
				System.out.println( "----(-0-)----FROM FILLPIECES------------" );
			//#endif
		}

		final int TOTAL_TRIES = 30;
		while ( clearedPieces < totalCleared ) {
			// sorteia uma das pe�as restantes
			Sprite p = null;
			byte tries = 0;

			do {
				p = getPiece( NanoMath.randInt( totalPieces ) );
				++tries;
			} while ( p.getSequenceIndex() == SEQUENCE_CLEARED && tries < TOTAL_TRIES );

			// percorre um a um, caso o sorteio n�o tenha dado resultado (evita loops infinitos ou muito longos)
			if ( tries >= TOTAL_TRIES ) {
				final short index = ( short ) NanoMath.randInt( totalPieces - 1 );
				int i = index + 1;

				for ( ; i != index && p.getSequenceIndex() == SEQUENCE_CLEARED; i = ( i + 1 ) % totalPieces ) {
					p = getPiece( i );
				}

				//#if DEBUG == "true"
//					if ( i == index ) {
//						throw new IllegalStateException( "n�o achou pe�a dispon�vel" );
//					}
				//#endif
			}

			clearPiece( p, false );
		}

		//#if DEBUG == "true"
			System.out.println( "fillPieces FIM: " + getRemainingCount() + " -> " + totalCleared + " / " + totalPieces );
		//#endif
	}


	public final Key getKeyWithTarget( Key[] keys, Sprite target ) {
		for ( byte i = 0; i < keys.length; ++i ) {
			if ( keys[ i ] != null && keys[ i ].getTarget() == target )
				return keys[ i ];
		}

		return null;
	}
	
	
	public final void sexyHotSpecial( KeyManager keyManager ) {
		final byte[] countC = new byte[ columns ];
		final byte[] countR = new byte[ rows ];

		for( int i = 0; i < PUZZLE_MAX_PIECES; i++ ) {
			final Sprite s = getPiece( i );
			if ( s.isVisible() ) {
				countC[ s.getPosX() / BLOCK_SIZE ]++;
				countR[ s.getPosY() / BLOCK_SIZE ]++;
			}
		}

		byte max = 0;
		byte biggestColumn = 0;
		byte biggestRow = 0;
		//#if DEBUG == "true"
			System.out.println( "----------------> COUNT <-----------------" );
		//#endif
		for( byte i = 0; i < rows; i++ ) {
			if ( countR[i] > max ) {
				max = countR[i];
				biggestRow = i;
			}
			//#if DEBUG == "true"
				System.out.println( "row"+ ( i + 1 ) + " = " + countR[i] );
			//#endif
		}

		for( byte i = 0; i < columns; i++ ) {
			if ( countC[i] > max ) {
				max = countC[i];
				biggestColumn = i;
			}
			//#if DEBUG == "true"
				System.out.println( "column"+ ( i + 1 ) + " = " + countC[i] );
			//#endif
		}

		if ( max > 0 ) {
			if ( countR[biggestRow] >= countC[biggestColumn] ) {
				removeRow( biggestRow, keyManager );
			} else {
				removeColumn( biggestColumn, keyManager );
			}
			refreshBuffer();
		}
	}


	private final void removeColumn( byte column, KeyManager keyManager ) {
		for( int i = 0; i < PUZZLE_MAX_PIECES; i++ ) {
			final Sprite s = getPiece( i );
			if ( s.isVisible() && ( ( s.getPosX() / BLOCK_SIZE ) == column ) ) {
				final Key k = getKeyWithTarget( keyManager.getKeys(), s );

				if ( k == null ) {
					clearPiece( s, false );
					createExplosion( s.getPosition().add( BLOCK_SIZE >> 1, 0 ), i );
					//#if DEBUG == "true"
						System.out.println( "EXPLODIU NA MARRA: " + i + ", " + column );
					//#endif
				} else {
					keyManager.pieceShot( k, KeyManager.SHOT_HIT_REGULAR, false );
					//#if DEBUG == "true"
						System.out.println( "EXPLODIU NO MACETAO: " + i + ", " + column );
						System.out.println( "Key: " + k );
					//#endif
				}
			}
		}
	}


	private final void removeRow( byte row, KeyManager keyManager ) {
		for( int i = 0; i < PUZZLE_MAX_PIECES; i++ ) {
			final Sprite s = getPiece( i );
			if ( s.isVisible() && ( ( s.getPosY() / BLOCK_SIZE ) == row ) ) {
				final Key k = getKeyWithTarget( keyManager.getKeys(), s );
				
				if ( k == null ) {
					clearPiece( s, false );
					createExplosion( s.getPosition().add( BLOCK_SIZE >> 1, 0 ), i );
					//#if DEBUG == "true"
						System.out.println( "EXPLODIU NA MARRA: " + row + ", " + i );
					//#endif
				} else {
					keyManager.pieceShot( k, KeyManager.SHOT_HIT_REGULAR, false );
					//#if DEBUG == "true"
						System.out.println( "EXPLODIU NO MACETAO: " + row + ", " + i );
					//#endif
				}
			}
		}
	}


	public final void setSize( int width, int height ) {
//		//#if  SCREEN_SIZE == "SMALL" ||  SCREEN_SIZE == "MEDIUM"
//			if ( GameMIDlet.isLowMemory() ) {
//				width = width * 94 / 100;
//				height = height * 92 / 100;
//			}
//		//#endif

		final Point previousSize = new Point( size );
		
		columns = NanoMath.min( NanoMath.min( bkgGroup.getWidth(), PUZZLE_MAX_SIZE ) , width ) / BLOCK_SIZE;
		rows = (  NanoMath.min( NanoMath.min( bkgGroup.getHeight(), PUZZLE_MAX_SIZE ) , height ) - ( sexyLogo.getHeight() >> 1 ) ) / BLOCK_SIZE;

		final int realWidth = columns * BLOCK_SIZE;
		final int realHeight = rows * BLOCK_SIZE;

		super.setSize( realWidth + ( PUZZLE_BORDER_THICKNESS << 1 ) , realHeight + sexyLogo.getHeight() + PUZZLE_BORDER_THICKNESS );

		sexyLogo.setPosition( ( getWidth() >> 1 ) - ( sexyLogo.getWidth() >> 1 ), realHeight - ( sexyLogo.getHeight() >> 1 ) );

		//#if DEBUG == "true"
			System.out.println( "columns: " + columns + ", rows = " + rows );
		//#endif

		totalPieces = ( short ) ( columns * rows );
		PUZZLE_USED_PIECES = totalPieces;

		pieces = new byte[ columns ][ rows ];
		int count = 0;
		for ( int r = 0, y = 0; r < rows; ++r ) {
			for ( int c = 0, x = 0; c < columns; ++c, ++count ) {
				pieces[ c ][ r ] = ( byte ) NanoMath.randInt( KEY_TYPES );
				final Sprite s = getPiece( count );
				setSpriteSequence( s, SEQUENCE_REMAINING );
				setFrame( s, pieces[ c ][ r ] );

//				System.out.println( "piece: " + c + ", " + r + " = " + pieces[ c ][ r ] + " on position ( " + x + ", " + y + " )");

				s.setPosition( x, y );

				s.setVisible( true );

				if ( c == columns - 1 && r == rows - 1 ) {

					if ( bkgGroup != null )
						bkgGroup.setPosition( ( size.x - bkgGroup.getWidth() ) >> 1, 0 );
					seda.setPosition( 0, 0 );

					//#if DEBUG == "true"
						System.out.println( "PUZZLE SIZE: " + getSize() );
					//#endif
				}

				x += BLOCK_SIZE;
			}
			y += BLOCK_SIZE;
		}

		//#if DEBUG == "true"
			System.out.println( "PEÇAS( " + count + "):" );
			for ( byte r = 0; r < rows; ++r ) {
				for ( byte c = 0; c < columns; ++c ) {
					System.out.print( ( pieces[ c ][ r ] < 10 ? " " + pieces[ c ][ r ] : pieces[ c ][ r ] + "" ) + " " );
				}
				System.out.println();
			}
		//#endif

		for ( ; count < PUZZLE_MAX_PIECES; ++count ) // esconde não visiveis
			getPiece( count ).setVisible( false );

		fillPieces( clearedPieces, true );

		mutex.acquire();
//		if ( buffer == null || !getSize().equals( previousSize ) ) {
		//#if  SCREEN_SIZE == "BIG" ||  SCREEN_SIZE == "GIANT"
			if ( buffer == null || !getSize().equals( previousSize ) ) {
		//#else
//# 			if ( buffer == null || ( /*!GameMIDlet.isLowMemory() &&*/ !getSize().equals( previousSize ) ) ) {
		//#endif

		//#if DEBUG == "true"
			System.out.println( "NEW BUFFER CREATED" );
		//#endif

			buffer = null;
			buffer = Image.createImage( realWidth, realHeight );
		}
		mutex.release();

		RealAreaHeight = ( realHeight + ( PUZZLE_BORDER_THICKNESS >> 1 ) );
		RealAreaWidth = ( realWidth + ( PUZZLE_BORDER_THICKNESS >> 1 ) );
		
		refreshBuffer();
	}


	public final Point getBoardSize() {
		return new Point( RealAreaWidth, RealAreaHeight );
	}


	public static final void clearBuffer() {
		buffer = null;
		System.gc();
	}


	public final void refreshBuffer() {
		//#if  SCREEN_SIZE == "SMALL" ||  SCREEN_SIZE == "MEDIUM"
//# 			// se o background for nulo, � sinal de que o buffer j� est� correto
//# //			if ( bkg == null ) return;
		//#endif

		mutex.acquire();

		if ( buffer != null ) {
			final Graphics g = buffer.getGraphics();

			final Point previousTranslate = new Point( translate );
			translate.set( 0, 0 );

			g.setClip( 0, 0, buffer.getWidth(), buffer.getHeight() );
//			pushClip( g );
			final int TOTAL = totalPieces + DRAWABLE_OFFSET;

//			//#if  SCREEN_SIZE == "SMALL" ||  SCREEN_SIZE == "MEDIUM"
//				if ( GameMIDlet.isLowMemory() ) {
//					// em baixa mem�ria, esses drawables n�o est�o inseridos no grupo
//					//#if DEBUG == "true"
//						System.out.println( "PAINTED LOW MEMORY BUFFER" );
//					//#endif
//
//					pattern.draw( g );
//					bkg.draw( g );
//				}
//			//#endif

			for ( int i = 0; i < TOTAL; ++i ) {
				if( drawables[ i ] instanceof Sprite ) {
					if( ( (Sprite) drawables[ i ] ).getSequenceIndex() > SEQUENCE_CLEARING ) {
						drawBLock( ( (Sprite) drawables[ i ] ), g, new Rectangle( drawables[ i ].getPosX(),
								drawables[ i ].getPosY(), drawables[ i ].getWidth(), drawables[ i ].getHeight() ), false );
					}
				} else {
					drawables[ i ].draw( g );
				}
			}
			
//			popClip( g );

			translate.set( previousTranslate );

//			//#if SCREEN_SIZE == "SMALL" ||  SCREEN_SIZE == "MEDIUM"
//				// libera a mem�ria usada pela imagem de fundo, pois n�o � mais necess�ria
//				if ( GameMIDlet.isLowMemory() ) {
////					System.gc();
////					final long before = Runtime.getRuntime().freeMemory();
//
//					//removeDrawable( bkg );
//					//bkg = null;
//					background = null;
//
//					System.gc();
////					System.out.println( "RELEASED " + ( before - Runtime.getRuntime().freeMemory() ) + " bytes." );
//				}
//			//#endif
		}

		mutex.release();
	}


	/**
	 * Desenha o bloco com o cadeado.
	 * @param total
	 * @return
	 */
	private final void drawBLock( Sprite s, Graphics g, Rectangle area, boolean fromPaint ) {
		g.setColor( 0x2F2E2E ); // TODO tirar constantes mágicas
		g.fillRect( area.x, area.y, area.width, area.height );
		chain.setViewport( area );
		if ( fromPaint ) {
			chain.setPosition( area.x - getPosX(), area.y - ( getPosY() + BLOCK_SIZE - area.height ) );
		} else {
			chain.setPosition( area.x, area.y );
		}
		chain.mirror( TRANS_NONE );
		chain.draw( g );
		final int frameN = chain.getCurrFrameIndex();
		chain.setFrame( ( frameN + chain.getCurrentSequence().length >> 1 ) % chain.getCurrentSequence().length );
		chain.mirror( TRANS_MIRROR_H );
		chain.draw( g );
		chain.setFrame( frameN );
		g.setColor( 0x1A1A1A ); // TODO tirar constantes mágicas
		g.drawRect( area.x, area.y, area.width - 1, area.height - 1 );
		s.setViewport( area );
		s.draw( g );
	}


	/**
	 * Obt�m "total" pe�as dispon�veis do tabuleiro.
	 * @param total
	 * @return
	 */
	public final Sprite[] getPiecesRemaining( int total ) {
		//#if DEBUG == "true"
			System.out.println( "total: " + total + ", totalPieces = " + totalPieces + ", clearedPieces: " + clearedPieces );
		//#endif
			
		total = Math.min( total, totalPieces - clearedPieces );
		final Sprite[] ret = new Sprite[ total ];


		int start = NanoMath.randInt( totalPieces - 1 );
		for ( int i = start + 1, index = 0; index < total; i = ( i + 1 ) % totalPieces ) {
			final Sprite p = getPiece( i );

			if ( p.getSequenceIndex() == SEQUENCE_REMAINING ) {
				//#if DEBUG == "true"
//					System.out.println( "SORTEADO: " + i + " -> " + p );
				//#endif

				setSpriteSequence( p, SEQUENCE_CHOSEN );

				ret[ index++ ] = p;
			}
		}

		return ret;
	}


	//#if DEBUG == "true"
		public final int getRemainingCount() {
			int visiblePieces = 0;
			int count = 0;
			for ( short i = 0; i < totalPieces; ++i ) {
				if ( getPiece( i ).getSequenceIndex() == SEQUENCE_REMAINING )
					++count;

				if ( getPiece( i ).isVisible() )
					visiblePieces++;
			}

			System.out.println( "-------> GET REMAINING COUNT <------" );
			System.out.println( "VISIBLE PIECES: " + visiblePieces );

			return count;
		}


		public final void printStatus() {
			int visiblePieces = 0;
			for ( short i = 0; i < totalPieces; ++i ) {
				if ( getPiece( i ).isVisible() )
					visiblePieces++;
			}

			System.out.println( "----------> PRINT STATUS <----------" );
			System.out.println( "VISIBLE PIECES: " + visiblePieces );
		}
	//#endif


	private final boolean setSpriteSequence( Sprite s, int sequence ) {
		if ( s.getSequenceIndex() != sequence ) {
			if ( s.getSequenceIndex() == SEQUENCE_CLEARING ) {
				s.move( -PUZZLE_BORDER_THICKNESS, -PUZZLE_BORDER_THICKNESS );
				s.setViewport( new Rectangle( s.getPosX(), s.getPosY(), s.getHeight(), s.getWidth() ) );
			} else if ( sequence  == SEQUENCE_CLEARING ) {
				s.move( PUZZLE_BORDER_THICKNESS, PUZZLE_BORDER_THICKNESS );
			}
			
			final byte frame = s.getFrameSequenceIndex();
			s.setSequence( sequence );
			s.setFrame( frame );
			if ( sequence == SEQUENCE_CLEARED ) {
				s.setVisible( false );
				return true;
			} else {
				s.setVisible( true );
				return false;
			}
		}
		return false;

//		//#if DEBUG == "true"
////			System.out.println( "setSpriteSequence( " + sequence + " )" );
//		//#endif
//
//		//#if  SCREEN_SIZE == "SMALL" ||  SCREEN_SIZE == "MEDIUM"
////# 			if ( GameMIDlet.isLowMemory() ) {
////# 				//s.setVisible( bkg != null || sequence != SEQUENCE_CLEARED );
////# 			}
//		//#endif
	}


	private final Sprite getPiece( int index ) {
		return ( Sprite ) getDrawable( index + DRAWABLE_OFFSET );
	}


	public final void setState( int state ) {
		if ( this.state != state ) {
			this.state = ( byte ) state;

			switch ( state ) {
				case STATE_CLOSED:
					accTime = 0;
					refreshAnimation();
				break;

				case STATE_OPENING:
					accTime = 0;
				break;

				case STATE_OPEN:
					accTime = TIME_ANIMATION;
					refreshAnimation();
				break;

				case STATE_CLOSING:
					if ( previousVisiblePieces > 0 )
						accTime = TIME_ANIMATION;
				break;
			}
		}
	}


	public final void update( int delta ) {
		if(  hasExplosions ) {
			for ( int i = 0; i < explosions.length; i++ ) {
				if ( explosions[i].isVisible() ) {
					accExplosionTime[i] += delta;

	//				explosions[i].setRefPixelPosition( explosionPosition[ i ].x ,
	//						explosionPosition[ i ].y + ( ( BLOCK_SIZE * accExplosionTime[i] ) / EXPLOSION_TIME ) );

					explosions[i].update( delta );
				}
			}
		}

		switch ( state ) {
			case STATE_OPENING:
				accTime += delta;

				refreshAnimation();

				if ( accTime >= TIME_ANIMATION )
					setState( STATE_OPEN );
			break;

			case STATE_CLOSING:
				accTime -= delta;

				refreshAnimation();

				if ( accTime <= 0 )
					setState( STATE_CLOSED );
			break;
		}

		if( !GameMIDlet.isLowMemory() ) {
			final long diff = System.currentTimeMillis() - lastTime;
			if ( diff > 200 ) {
				chain.setFrame( ( chain.getFrameSequenceIndex() + 1 ) % chain.getCurrentSequence().length );
				refreshBuffer();
				lastTime = System.currentTimeMillis();
			}
		}
	}


	private final void refreshAnimation() {
		final int visiblePieces = ( accTime * totalPieces / TIME_ANIMATION );

//		//#if  SCREEN_SIZE == "BIG" ||  SCREEN_SIZE == "GIANT"
////# //			for ( short i = 0; i < totalPieces; ++i ) {
////# //				getPiece( i ).setVisible( true /*i > visiblePieces*/ );
////# //			}
//		//#else
//			for ( short i = 0; i < totalPieces; ++i ) {
//				final Sprite p = getPiece( i );
//				p.setVisible( i > visiblePieces && ( !GameMIDlet.isLowMemory() || p.getSequenceIndex() != SEQUENCE_CLEARED ) );
//			}
//		//#endif
		if ( visiblePieces != previousVisiblePieces ) {
			refreshBuffer();
			previousVisiblePieces = ( short ) visiblePieces;
		}
	}


	private final void setFrame( Sprite s, byte pieceType ) {		
		s.setFrame( pieceType );
	}


	private final void createExplosion( Point pos, int id ) {
		if( hasExplosions ) {
			pos.addEquals( PUZZLE_BORDER_THICKNESS, PUZZLE_BORDER_THICKNESS );
			if ( explosions[ currentExplosionIndex ].getSequenceIndex() != SEQUENCE_IDLE ) {
				setSpriteSequence( getPiece( lockIndex[ currentExplosionIndex ] ), SEQUENCE_CLEARED );
			}
			setSpriteSequence( getPiece( id ), SEQUENCE_CLEARING );
			lockIndex[ currentExplosionIndex ] = id;
			isExploding[ id ] = ( byte ) ( currentExplosionIndex + 1 );
			final Sprite explosion = explosions[ currentExplosionIndex ];
			explosion.setSequence( SEQUENCE_ANIMATING );
			explosion.setVisible( true );
			explosionPosition[ currentExplosionIndex ].set( pos );
			accExplosionTime[ currentExplosionIndex ] = 0;
			explosion.setRefPixelPosition( pos );
			currentExplosionIndex = ( byte ) ( ( currentExplosionIndex + 1 ) % MAX_EXPLOSIONS );
		}
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if( hasExplosions ) {
			if ( id >= 0 ) {
				explosions[ id ].setVisible( false );
				explosions[ id ].setSequence( SEQUENCE_IDLE );
				isExploding[ lockIndex[ id ] ] = 0;
				setSpriteSequence( getPiece( lockIndex[ id ] ), SEQUENCE_CLEARED );
			}
		}
	}


	public void onFrameChanged( int id, int frameSequenceIndex ) {
	}

}
