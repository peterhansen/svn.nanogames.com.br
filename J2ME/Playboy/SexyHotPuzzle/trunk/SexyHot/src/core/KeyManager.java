/**
 * KeyManager.java
 * 
 * Created on Jul 20, 2009, 7:18:22 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;
import screens.GameScreen;

/**
 *
 * @author Peter
 */
public final class KeyManager extends UpdatableGroup implements Constants, SpriteListener, GameKeyListener {

	public static final byte PATTERN_HORIZONTAL				= 0;
	public static final byte PATTERN_VERTICAL				= 1;
	public static final byte PATTERN_BEZIER					= 2;
	public static final byte PATTERN_BOUNCE					= 3;
	public static final byte PATTERN_LOOP					= 4;
	public static final byte PATTERN_CLOCKWISE				= 5;
	public static final byte PATTERN_COUNTER_CLOCKWISE		= 6;

	public static final byte PATTERN_TOTAL					= 7;

	public static final byte SHOT_HIT_NONE		= 0;
	public static final byte SHOT_HIT_REGULAR	= 1;
	public static final byte SHOT_HIT_SPECIAL	= 2;
	public static final byte SHOT_HIT_COMBO		= 3;

	private static final short TIME_EASY = 10000;
	private static final short TIME_HARD = 3000;
//
	private static final short TIME_RANDOM_EASY = 1;
	private static final short TIME_RANDOM_HARD = TIME_HARD;

	private short currentTime;
	private short maxRandomTime;

	private final Sprite[] explosions;

	private final Point topLeft = new Point();
	private final Point bottomRight = new Point();

	private byte currentShineIndex;

	private static final byte MAX_EXPLOSIONS = 8;

	private static final byte SHINE_SEQUENCE_IDLE		= 0;
	private static final byte SHINE_SEQUENCE_ANIMATING	= 1;

	/** Quantidade m�xima de pe�as simult�neas na tela. */
	public static final byte PIECES_MAX = 8;
	/** Quantidade minima de pe�as simult�neas na tela (exceto no final de cada n�vel). */
	private static final byte PIECES_MIN = 3;

	private final Key[] keys;

	/** Quantidade atual de pe�as ativas. */
	private byte activePieces;

	/** Total de pe�as eliminadas. */
//	private byte clearedPieces;

	//private byte totalPieces;

	private final GameScreen gameScreen;

	private final Key[] pieceOrder = new Key[ PIECES_MAX ];

	private byte pieceOrderIndex;

	private final SpecialKey specialPiece;

	private long lastResetTime;

	private final SpecialSexyHot specialHot;


	public KeyManager( GameScreen gameScreen ) throws Exception {
		super( PIECES_MAX + MAX_EXPLOSIONS + 2 );
		keys = new Key[ PIECES_MAX ];

		this.gameScreen = gameScreen;

		for ( byte i = 0; i < PIECES_MAX; ++i ) {
			keys[ i ] = new Key( this );
			insertDrawable( keys[ i ] );
			GameMIDlet.log( "key " + i );
		}
		GameMIDlet.log( "keys" );

		if( !GameMIDlet.isLowMemory() ) {
			explosions = new Sprite[ MAX_EXPLOSIONS ];
			final Sprite shine = new Sprite( PATH_IMAGES + "game_explosion" );
			GameMIDlet.log( "game_explosion" );
			for ( byte i = 0; i < MAX_EXPLOSIONS; ++i ) {
				final Sprite s = new Sprite( shine );
				explosions[ i ] = s;

				s.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
				s.setVisible( false );
				s.setListener( this, i );
				insertDrawable( s );
				GameMIDlet.log( "explosion " + i );
			}
			GameMIDlet.log( "explosions" );
		} else {
			explosions = null;
		}

		specialPiece = new SpecialKey( this );
		GameMIDlet.log( "specialPiece" );
		insertDrawable( specialPiece );

		specialHot = new SpecialSexyHot( gameScreen );
		GameMIDlet.log( "specialHot" );
		insertDrawable( specialHot );
	}


	public final void prepare( byte level ) {
		currentTime = ( short ) ( TIME_EASY + ( TIME_HARD - TIME_EASY ) * level / LEVEL_HARD );
		maxRandomTime = ( short ) ( TIME_RANDOM_EASY + ( TIME_RANDOM_HARD - TIME_RANDOM_EASY ) * level / LEVEL_HARD );

		specialHot.reset();
		specialPiece.prepare();

		//#if DEBUG == "true"
			System.out.println( "PieceManager.prepare: " + Puzzle.PUZZLE_USED_PIECES + ", " + currentTime );
		//#endif
	}


	public final void reset() {
		final byte pattern = ( byte ) NanoMath.randInt( PATTERN_TOTAL );
		final boolean alternate = NanoMath.randInt( 128 ) < 64;
		final Point startPos = new Point();
		byte move = -1;

		pieceOrderIndex = 0;

		final byte PIECES_TO_INSERT = ( byte ) Math.min( PIECES_MIN + NanoMath.randInt( PIECES_MAX - PIECES_MIN ), Puzzle.PUZZLE_USED_PIECES - gameScreen.getPuzzle().clearedPieces );

		//#if DEBUG == "true"
			System.out.println( "RESET: " + gameScreen.getPuzzle().getRemainingCount() + " + " + PIECES_TO_INSERT );
		//#endif

		final Sprite[] types = gameScreen.getPuzzle().getPiecesRemaining( PIECES_TO_INSERT );

		final byte[] temp = new byte[ PIECES_TO_INSERT ];
		for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
			keys[ i ].setTarget( types[ i ] );

			int t = -1;
			do {
				t = NanoMath.randInt( PIECES_TO_INSERT );
			} while ( temp[ t ] < 0 );

			pieceOrder[ i ] = keys[ t ];
			temp[ t ] = -1;

			//#if DEBUG == "true"
				System.out.println( "ORDER[ " + i + " ]: " + pieceOrder[ i ] );
			//#endif
		}

		switch ( pattern ) {
			case PATTERN_HORIZONTAL:
				boolean right = NanoMath.randInt( 128 ) < 64;
				
				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Key p = keys[ i ];
					startPos.y = ( size.y * ( i + 1 ) )  / ( PIECES_TO_INSERT + 1);

					if ( alternate )
						right = !right;

					if ( right ) {
						move = Key.MOVE_RIGHT;
						startPos.x = -KEY_DEFAULT_WIDTH  >> 1;
					} else {
						move = Key.MOVE_LEFT;
						startPos.x = size.x + ( KEY_DEFAULT_WIDTH  >> 1 );
					}
					p.setMove( move, startPos, currentTime );
				}
			break;

			case PATTERN_VERTICAL:
				boolean down = NanoMath.randInt( 128 ) < 64;

				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Key p = keys[ i ];
					startPos.x = ( size.x * ( i + 1 ) )  / ( PIECES_TO_INSERT + 1 );

					if ( alternate )
						down = !down;

					if ( down ) {
						move = Key.MOVE_DOWN;
						startPos.y = -KEY_DEFAULT_HEIGHT >> 1;
					} else {
						move = Key.MOVE_UP;
						startPos.y = size.y + ( KEY_DEFAULT_HEIGHT >> 1 );
					}
					p.setMove( move, startPos, currentTime );
				}
			break;

			case PATTERN_BEZIER:
				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Key p = keys[ i ];
					startPos.set( getOffscreenPos( getSize(), getPosition() ) );
					p.setMove( Key.MOVE_BEZIER, startPos, currentTime + NanoMath.randInt( maxRandomTime ) );
				}
			break;

			case PATTERN_BOUNCE:
				for ( byte i = 0; i < PIECES_TO_INSERT; ++i ) {
					final Key p = keys[ i ];
					p.setMove( Key.MOVE_BOUNCE, getOffscreenPos( getSize(), getPosition() ), currentTime + NanoMath.randInt( maxRandomTime ) );
				}
			break;

			case PATTERN_LOOP:
				byte direction = ( byte ) NanoMath.randInt( 4 );

				switch ( direction ) {
					case Key.DIRECTION_UP:
						for ( int i = 0, y = getHeight(); i < PIECES_TO_INSERT; ++i ) {
							final Key p = keys[ i ];
							y += KEY_DEFAULT_HEIGHT;
							startPos.set( bottomRight.x, y );
							p.setMove( Key.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;
					
					case Key.DIRECTION_DOWN:
						for ( int i = 0, y = 0; i < PIECES_TO_INSERT; ++i ) {
							final Key p = keys[ i ];
							y -= KEY_DEFAULT_HEIGHT;
							startPos.set( topLeft.x, y );
							p.setMove( Key.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;

					case Key.DIRECTION_LEFT:
						for ( int i = 0, x = getWidth(); i < PIECES_TO_INSERT; ++i ) {
							final Key p = keys[ i ];
							x += KEY_DEFAULT_WIDTH;
							startPos.set( x, topLeft.y );
							p.setMove( Key.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;

					case Key.DIRECTION_RIGHT:
						for ( int i = 0, x = 0; i < PIECES_TO_INSERT; ++i ) {
							final Key p = keys[ i ];
							x -= KEY_DEFAULT_WIDTH;
							startPos.set( x, bottomRight.y );
							p.setMove( Key.MOVE_LOOP, startPos, currentTime, direction );
						}
					break;
				}
			break;

			case PATTERN_CLOCKWISE:
			case PATTERN_COUNTER_CLOCKWISE:
				byte orientation = pattern;

				for ( int i = 0, d = NanoMath.randInt( 8 ); i < PIECES_TO_INSERT; ++i, d = ( d + 1 ) % 8 ) {
					final Key p = keys[ i ];
					switch ( d ) {
						case 0:
							// top-left (vai pra direita)
							startPos.set( -KEY_DEFAULT_WIDTH >> 1, topLeft.y );
							move = Key.DIRECTION_RIGHT;
						break;

						case 1:
							// top-right (vai pra baixo)
							startPos.set( bottomRight.x, -KEY_DEFAULT_HEIGHT >> 1 );
							move = Key.DIRECTION_DOWN;
						break;

						case 2:
							// bottom-right (vai pra esquerda)
							startPos.set( getWidth() + ( KEY_DEFAULT_WIDTH >> 1 ), bottomRight.y );
							move = Key.DIRECTION_LEFT;
						break;
						
						case 3:
							// bottom-left (vai pra cima)
							startPos.set( topLeft.x, getHeight() + ( KEY_DEFAULT_HEIGHT >> 1 ) );
							move = Key.DIRECTION_UP;
						break;

						case 4:
							// top-right (vai pra esquerda)
							startPos.set( getWidth() + ( KEY_DEFAULT_WIDTH >> 1 ), topLeft.y );
							move = Key.DIRECTION_LEFT;
						break;

						case 5:
							// bottom-right (vai pra cima)
							startPos.set( bottomRight.x, getHeight() + ( KEY_DEFAULT_HEIGHT >> 1 ) );
							move = Key.DIRECTION_UP;
						break;

						case 6:
							// bottom-left (vai pra direita)
							startPos.set( - ( KEY_DEFAULT_WIDTH >> 1 ), bottomRight.y );
							move = Key.DIRECTION_RIGHT;
						break;

						case 7:
							// top-left (vai pra baixo)
							startPos.set( topLeft.x, -KEY_DEFAULT_HEIGHT >> 1 );
							move = Key.DIRECTION_DOWN;
						break;
					}
					
					orientation = d < 4 ? PATTERN_CLOCKWISE : PATTERN_COUNTER_CLOCKWISE;

					//#if DEBUG == "true"
						System.out.println( i + ": " + move + " -> " + startPos );
					//#endif

					p.setMove( orientation == PATTERN_CLOCKWISE ? Key.MOVE_CLOCKWISE : Key.MOVE_COUNTER_CLOCKWISE, startPos, currentTime, move );
				}
			break;
		}

		// p�ra as pe�as restantes
		for ( byte i = PIECES_TO_INSERT; i < PIECES_MAX; ++i )
			keys[ i ].setState( Key.STATE_NONE );

		setNextPiece( pieceOrder[ 0 ] );

		Key.ResetDyingCount();

		activePieces = PIECES_TO_INSERT;
		lastResetTime = System.currentTimeMillis();
	}


//	public static final Point getOffscreenPos() {
//		return getOffscreenPos( null );
//	}


	public static final Point getOffscreenPos( Point size, Point position ) {
//		if ( size == null )
//			size = new Point( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final Point pos = new Point();

		if ( NanoMath.randInt( 128 ) < 64 ) {
			// pe�a acima ou abaixo da tela
			pos.x = + NanoMath.randInt( size.x + KEY_DEFAULT_WIDTH ) - ( KEY_DEFAULT_WIDTH );
			pos.y = NanoMath.randInt( 128 ) < 64 ? ( -KEY_DEFAULT_HEIGHT ) : size.y + ( KEY_DEFAULT_HEIGHT );
		} else {
			// pe�a � esquerda ou direita da tela
			pos.x = NanoMath.randInt( 128 ) < 64 ? - ( KEY_DEFAULT_WIDTH ) : size.x + ( KEY_DEFAULT_WIDTH );
			pos.y =  + NanoMath.randInt( size.y + KEY_DEFAULT_HEIGHT ) - ( KEY_DEFAULT_HEIGHT );
		}

		//#if DEBUG == "true"
			System.out.println( "keyRandomPosition: " + pos );
		//#endif

		return pos;
	}


	public final void shot( Rectangle rect ) {
		if ( pieceOrderIndex < pieceOrder.length ) {
			rect.x -= getPosX();
			rect.y -= getPosY();

			if ( specialHot.tryToCollect( rect ) )
				return;

			if ( specialPiece.getState() != SpecialKey.SPECIAL_PIECE_STATE_NONE ) {
				if ( specialPiece.intersects( rect ) ) {
					switch ( specialPiece.type ) {
						case SpecialKey.SPICY_NORMAL:
							specialPiece.setState( SpecialKey.SPECIAL_PIECE_STATE_NONE );
							shotAll();
							gameScreen.onShot( SHOT_HIT_SPECIAL, specialPiece.getRefPixelPosition() );
						return;
						case SpecialKey.SPICY_ICE:
							specialPiece.setState( SpecialKey.SPECIAL_PIECE_STATE_NONE );
							frostAll();
							gameScreen.onShot( SHOT_HIT_SPECIAL, specialPiece.getRefPixelPosition() );
						return;
						case SpecialKey.SPICY_RADIATION:
							specialPiece.setState( SpecialKey.SPECIAL_PIECE_STATE_NONE );
							try {
								radiateAll( pieceOrder[ pieceOrderIndex ].getFrameSequenceIndex() );
							} catch ( Exception e ) {
								e.printStackTrace();
							}
							gameScreen.onShot( SHOT_HIT_SPECIAL, specialPiece.getRefPixelPosition() );
						return;
					}
				}
			}

			for ( byte i = 0; i < PIECES_MAX; ++i ) {
				final Key p = keys[ i ];
				if ( p.isShot( rect ) && p.getFrameSequenceIndex() == pieceOrder[ pieceOrderIndex ].getFrameSequenceIndex() && p.getTransform() == pieceOrder[ pieceOrderIndex ].getTransform() ) {
					pieceShot( p, SHOT_HIT_REGULAR, true );
					return;
				}
			}
		}
		
		gameScreen.onShot( SHOT_HIT_NONE, null );
	}


	public final Key[] getKeys() {
		return pieceOrder;
	}


	public final void checkConsistency() {
//		for( int i = pieceOrderIndex; i + 1 < pieceOrder.length; ) {
//			//#if DEBUG == "true"
//				System.out.println( "----------------checkConsistency-----------------" );
//				System.out.println( "pieceOrderIndex = " + pieceOrderIndex );
//				System.out.println( "pieceOrder.length = " + pieceOrder.length );
//				System.out.println( "i = " + i );
//			//#endif
//			if ( !pieceOrder[ i ].getTarget().isVisible() )
//				pieceOrder[ i ].setState( Key.STATE_VANISHING );
//			i++;
//		}
//		reset();
	}


	protected final void pieceShot( Key p, byte shotType, boolean playSound ) {
		p.setState( Key.STATE_DYING );
		if( playSound )
			MediaPlayer.play( SOUND_KEY_COLLECTED );

		gameScreen.onShot( shotType, p.getRefPixelPosition() );

		if ( gameScreen.getPuzzle().clearedPieces + 1 >= Puzzle.PUZZLE_USED_PIECES ) {
			// fim de nível
			setNextPiece( null );
			pieceOrderIndex++;
		} else if ( pieceOrderIndex + 1 < pieceOrder.length ) {
			int count = 0;
			do {
				pieceOrderIndex = ( byte ) ( ( pieceOrderIndex + 1 ) % pieceOrder.length );

				//#if DEBUG == "true"
				System.out.println( "_/`> JUMP" );
 				System.out.println( "activePieces: " + activePieces );
				//#endif
				
				if ( count++ > pieceOrder.length ) {
					//#if DEBUG == "true"
					for ( byte i = 0 ; i < pieceOrder.length; ++i )
						System.out.println( i + " -> " + ( pieceOrder[ i ] == null ? "null" : String.valueOf( pieceOrder[ i ].getState() ) ) );
					//#endif
					return;
					//throw new RuntimeException( "LOOP INFINITO! " );
				}

			} while ( pieceOrder[ pieceOrderIndex ] == null || ( ( pieceOrder[ pieceOrderIndex ].getState() != Key.STATE_MOVING ) && ( pieceOrder[ pieceOrderIndex ].getState() != Key.STATE_PREPARING_MOVE ) ) );
			setNextPiece( pieceOrder[ pieceOrderIndex ] );
		}

		//#if DEBUG == "true"
			System.out.println( "ATINGIU PE�A -> " + p.getState() + " PROGRESSO: " + gameScreen.getPuzzle().clearedPieces + " / " + Puzzle.PUZZLE_USED_PIECES );
		//#endif
	}


	private final void shotAll() {
		for ( byte i = 0; i < PIECES_MAX; ++i ) {
			if ( keys[ i ] != null && keys[ i ].getState() == Key.STATE_MOVING ) {
				pieceShot( keys[ i ], SHOT_HIT_COMBO, false );
				keys[ i ].heat();

				createExplosion( keys[ i ], true );
			}
		}
		MediaPlayer.play( SOUND_HOT_SPICY );
	}


	private final void frostAll() {
		MediaPlayer.play( SOUND_COLD_SPICY );
		for ( byte i = 0; i < PIECES_MAX; ++i ) {
			//#if DEBUG == "true"
				try {
			//#endif
				if ( keys[ i ] != null && keys[ i ].getState() == Key.STATE_MOVING ) {
					if ( ( ( keys[ i ].getPosX() > 0 ) && ( ( keys[ i ].getPosX() + ( KEY_DEFAULT_WIDTH ) ) < getWidth() ) ) &&
							( ( keys[ i ].getPosY() > 0 ) && ( ( keys[ i ].getPosY() + ( KEY_DEFAULT_HEIGHT ) ) < getHeight() ) ) ) {
						keys[ i ].frost();
						createExplosion( keys[ i ], true );
					}
				}
			//#if DEBUG == "true"
				} catch ( Throwable t ) {
					t.printStackTrace();
					System.out.println( "--------------- FROST ALL --------------" );
					System.out.println( " | i = " + i );
					System.out.println(" | pieceOrder.length = " + pieceOrder.length );
					System.out.println( " | pieces.length: " + keys.length );
					System.out.println( " | pieceOrderIndex = " + pieceOrderIndex );
					System.out.println( " | i + pieceOrderIndex = " + (i + pieceOrderIndex) );
					System.out.println( " | pieceOrder[ i + pieceOrderIndex ] = " + pieceOrder[ i + pieceOrderIndex ]  );
					System.out.println( "----------------------------------------" );
				}
			//#endif
		}
	}


	private final void radiateAll( int type ) {
		MediaPlayer.play( SOUND_RADIATED_SPICY );
		for ( byte i = 0; i < PIECES_MAX; ++i ) {
			//#if DEBUG == "true"
				try {
			//#endif
				if ( pieceOrder[ i ] != null && ( pieceOrder[ i ].getState() != Key.STATE_NONE /*|| pieceOrder[ i ].getState() == Key.STATE_PREPARING_MOVE*/ ) ) {
					pieceOrder[ i ].radiate( type );
					pieceOrder[ i ].setFrame( type );
					createExplosion( pieceOrder[ i ], true );
				}
			//#if DEBUG == "true"
				} catch ( Throwable t ) {
					t.printStackTrace();
					System.out.println( "-------------- RADIATE ALL -------------" );
					System.out.println( " | i = " + i );
					System.out.println(" | pieceOrder.length = " + pieceOrder.length );
					System.out.println( " | pieces.length: " + keys.length );
					System.out.println( " | type = " + type );
					System.out.println( " | pieceOrderIndex = " + pieceOrderIndex );
					System.out.println( " | i + pieceOrderIndex = " + (i + pieceOrderIndex) );
					System.out.println( " | pieceOrder[ i + pieceOrderIndex ] = " + pieceOrder[ i + pieceOrderIndex ]  );
					System.out.println( "----------------------------------------" );
				}
			//#endif
		}
	}


	private final void createExplosion( Key k, boolean useCustomType ) {
		if( !GameMIDlet.isLowMemory() ) {
			final Sprite explosion = explosions[ currentShineIndex ];
			explosion.setSequence( SHINE_SEQUENCE_ANIMATING + ( useCustomType ? k.specialState : 0 ) );
			explosion.setVisible( true );
			explosion.setRefPixelPosition( k.getRefPixelPosition() );
			currentShineIndex = ( byte ) ( ( currentShineIndex + 1 ) % MAX_EXPLOSIONS );
		}
	}


	public final void throwSexyHot() {
		specialHot.throwSomeCollectable();
	}


	public final void setSpecialPieceVisible( boolean v ) {
		specialPiece.setState( v ? SpecialKey.SPECIAL_PIECE_STATE_VISIBLE : SpecialKey.SPECIAL_PIECE_STATE_NONE );
	}


	public final void onDie( Key k ) {
		//#if DEBUG == "true"
			System.out.println( "onDie( )" );
		//#endif

		gameScreen.getPuzzle().clearPiece( k.getTarget(), true );
		Key.RemoveDyingCount();

		createExplosion( k, false );
//		final Sprite shine = explosions[ currentShineIndex ];
//		shine.setSequence( SHINE_SEQUENCE_ANIMATING /*+ p.specialState*/ );
//		shine.setVisible( true );
//		shine.setRefPixelPosition( ( p.getPosX() + ( KEY_DEFAULT_WIDTH >> 1 ) ), p.getPosY() + ( KEY_DEFAULT_HEIGHT >> 1 ) );
//		currentShineIndex = ( byte ) ( ( currentShineIndex + 1 ) % MAX_EXPLOSIONS );
	}


	public final void onVanish( Key p ) {
		//#if DEBUG == "true"
			// a quantidade de pe�as n�o pode ser menor ou igual a zero antes de decrementar
			if ( activePieces < 0 )
				throw new IllegalStateException( "activePieces < 0" );
		//#endif

		--activePieces;

		if ( gameScreen.getPuzzle().clearedPieces >= Puzzle.PUZZLE_USED_PIECES ) {
			// fim de n�vel
			gameScreen.setState( GameScreen.STATE_LEVEL_CLEARED );
		} else if ( activePieces <= 0 ) {
			reset();
		}
	}


	public final boolean canDecreaseComboTime() {
		return activePieces > 0 && ( System.currentTimeMillis() - lastResetTime ) > TIME_COMBO;
	}


	private final void setNextPiece( Key piece ) {
		gameScreen.setCrosshairTarget( piece );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		specialHot.setSize( width, height );

		//#if DEBUG == "true"
			System.out.println( "MANAGER setSize( " + width + ", " + height + " )" );
		//#endif

		//setPosition( 0, ScreenManager.SCREEN_HEIGHT - height );

		final int INTERNAL_WIDTH	= width * 76 / 100;
		final int INTERNAL_HEIGHT	= height * 76 / 100;

		topLeft.set( ( width - INTERNAL_WIDTH ) >> 1, ( ( height - INTERNAL_HEIGHT ) >> 1 ) );
		bottomRight.set( topLeft.add( INTERNAL_WIDTH, INTERNAL_HEIGHT ) );

		//#if DEBUG == "true"
			System.out.println( "MANAGER LIMITS: " + topLeft + " -> " + bottomRight );
		//#endif

//		puzzleOffset = ( PUZZLE_SIZE - width ) >> 1;

		Key.setManagerLimits( topLeft, bottomRight );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if( !GameMIDlet.isLowMemory() ) {
			if ( id >= 0 ) {
				explosions[ id ].setVisible( false );
				explosions[ id ].setSequence( SHINE_SEQUENCE_IDLE );
			}
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
		
	}


}
