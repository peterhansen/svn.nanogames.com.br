/**
 * GameKeyListener.java
 * 
 * Created on Aug 17, 2009, 7:16:11 PM
 *
 */

package core;

/**
 *
 * @author Peter
 */
public interface GameKeyListener {

	public void onVanish( Key p );

	public void onDie( Key aThis );

}
