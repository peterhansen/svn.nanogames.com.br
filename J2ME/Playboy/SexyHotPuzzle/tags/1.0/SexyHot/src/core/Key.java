/**
 * Piece.java
 * 
 * Created on Jul 20, 2009, 7:13:55 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Peter
 */
public final class Key extends Sprite implements Constants {

	public static final byte MOVE_RIGHT				= 0;
	public static final byte MOVE_LEFT				= 1;
	public static final byte MOVE_UP				= 2;
	public static final byte MOVE_DOWN				= 3;
	public static final byte MOVE_UP_LEFT			= 4;
	public static final byte MOVE_UP_RIGHT			= 5;
	public static final byte MOVE_DOWN_LEFT			= 6;
	public static final byte MOVE_DOWN_RIGHT		= 7;
	public static final byte MOVE_BEZIER			= 8;
	public static final byte MOVE_LOOP				= 9;
	public static final byte MOVE_CLOCKWISE			= 10;
	public static final byte MOVE_COUNTER_CLOCKWISE	= 11;
	public static final byte MOVE_BOUNCE			= 12;

	/***/
	public static final byte TOTAL_MOVES = MOVE_BOUNCE + 1;

	/** �ndice do tipo de movimento atual. */
	private byte move;

	/** Tempo em milisegundos da anima��o de sumi�o da pe�a ao morrer. */
	public static final short TIME_VANISH = 290;

	/** Tempo em milisegundos do v�o da pe�a � posi��o de destino. */
	public static final short TIME_DIE = 600;

	public static final byte STATE_NONE				= 0;
	public static final byte STATE_PREPARING_MOVE	= 1;
	public static final byte STATE_MOVING			= 2;
	public static final byte STATE_DYING			= 3;
	public static final byte STATE_VANISHING		= 4;

	private byte state;

	private static final byte SEQUENCE_NORMAL = 0;
	private static final byte SEQUENCE_DYING = 1;
	
	/***/
	private static Sprite SPRITE;
	private static Sprite FROSTSPRITE;
	private static Sprite RADIATIONSPRITE;

	/***/
	private final MUV speedX = new MUV();

	/***/
	private final MUV speedY = new MUV();

	public static final byte DIRECTION_UP			= 0;
	public static final byte DIRECTION_DOWN			= 1;
	public static final byte DIRECTION_LEFT			= 2;
	public static final byte DIRECTION_RIGHT		= 3;
	public static final byte DIRECTION_LOOP			= 4;
	public static final byte DIRECTION_AFTER_LOOP	= 5;

	private byte direction;

	private byte initialDirection;

	/***/
	private final BezierCurve bezier = new BezierCurve();

	/***/
	private final Point startPosition = new Point();

	/***/
	public final KeyManager manager;

	private final GameKeyListener pieceListener;

	/***/
	private short stateTime;

	/***/
	private short totalTime;

	private static final Point offset = new Point();
	public static final Point managerPosition = new Point();
	public static Point managerTopLeft;
	public static Point managerBottomRight;

	/***/
	private final Point destination = new Point();

	/***/
	private final Point origin = new Point();

	private Sprite target;
	
	private final static byte KEY_SPECIAL_STATE_NONE = 0;
	private final static byte KEY_SPECIAL_STATE_HOT = 1;
	private final static byte KEY_SPECIAL_STATE_FROST = 2;
	private final static byte KEY_SPECIAL_STATE_RADIATED = 3;
	
	public byte specialState = KEY_SPECIAL_STATE_NONE;

	public static int DYING_KEYS = 0;
	public static boolean CAN_PLAY = true;


	public Key( GameKeyListener listener ) throws Exception {
		super( SPRITE );

		this.pieceListener = listener;

		if ( listener instanceof KeyManager )
			manager = ( KeyManager ) listener;
		else
			manager = null;

//		setSize( KEY_DEFAULT_WIDTH, KEY_DEFAULT_HEIGHT );
//		defineReferencePixel( ( KEY_DEFAULT_WIDTH ) >> 1, SPRITE.getHeight() >> 1 /* ANCHOR_HCENTER | ANCHOR_VCENTER */ );
		defineReferencePixel( ANCHOR_CENTER );
	}
	

	public static final void load() throws Exception {
		if ( SPRITE == null ) {
			SPRITE = new Sprite( PATH_IMAGES + "game_keys" );
			FROSTSPRITE = new Sprite( PATH_IMAGES + "game_keyfrost" );
			RADIATIONSPRITE = new Sprite( PATH_IMAGES + "game_keyradiation" );
		}
	}


	public static final Drawable[] getPieces() throws Exception {
		final Drawable[] pieces = new Drawable[ KEY_TYPES ];
		for ( byte i = 0; i < KEY_TYPES; ++i ) {
			pieces[ i ] = new Key( null );
			( ( Key ) pieces[ i ] ).setFrame( i );
		}

		return pieces;
	}


	public void heat() {
		specialState = KEY_SPECIAL_STATE_HOT;
	}


	public void deheat() {
		if( specialState == KEY_SPECIAL_STATE_HOT )
			specialState = KEY_SPECIAL_STATE_NONE;
	}


	public void resetSpecialState() {
		specialState = KEY_SPECIAL_STATE_NONE;
	}


	public void frost() {
		specialState = KEY_SPECIAL_STATE_FROST;
	}


	public void defrost() {
		if( specialState == KEY_SPECIAL_STATE_FROST )
			specialState = KEY_SPECIAL_STATE_NONE;
	}


	public void radiate( int i ) {
		setFrame( i );
		specialState = KEY_SPECIAL_STATE_RADIATED;
	}


	public void deradiate() {
		if( specialState == KEY_SPECIAL_STATE_RADIATED )
			specialState = KEY_SPECIAL_STATE_NONE;
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_PREPARING_MOVE:
				setState( STATE_MOVING );
			break;

			case STATE_MOVING:
				if ( specialState != KEY_SPECIAL_STATE_FROST ) {
					switch ( move ) {
						case MOVE_RIGHT:
							move( speedX.updateInt( delta ), 0 );

							if ( getPosX() >= manager.getWidth() )
								reset();
						break;

						case MOVE_LEFT:
							move( speedX.updateInt( delta ), 0 );

							if ( getPosX() <= -KEY_DEFAULT_WIDTH )
								reset();
						break;

						case MOVE_UP:
							move( 0, speedY.updateInt( delta ) );

							if ( getPosY() <= -KEY_DEFAULT_HEIGHT )
								reset();
						break;

						case MOVE_DOWN:
							move( 0, speedY.updateInt( delta ) );

							if ( getPosY() >= manager.getHeight() )
								reset();
						break;

						case MOVE_UP_LEFT:
							move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

							if ( getPosX() <= -KEY_DEFAULT_WIDTH || getPosY() <= -KEY_DEFAULT_HEIGHT )
								reset();
						break;

						case MOVE_UP_RIGHT:
							move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

							if ( getPosX() >= manager.getWidth() || getPosY() <= -KEY_DEFAULT_HEIGHT )
								reset();
						break;

						case MOVE_DOWN_LEFT:
							move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

							if ( getPosX() <= -KEY_DEFAULT_WIDTH || getPosY() >= manager.getHeight() )
								reset();
						break;

						case MOVE_DOWN_RIGHT:
							move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

							if ( getPosX() >= manager.getWidth() || getPosY() >= manager.getHeight() )
								reset();
						break;

						case MOVE_BEZIER:
							stateTime += delta;
							final Point bezierPos = new Point();
							bezier.getPointAtFixed( bezierPos, NanoMath.divInt( stateTime, totalTime ) );
							setRefPixelPosition( bezierPos );

							if ( stateTime >= totalTime ) {
								reset();
							}
						break;

						case MOVE_BOUNCE:
							move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

							if ( speedX.getSpeed() < 0 ) {
								if ( getPosX() <= 0 )
									speedX.setSpeed( -speedX.getSpeed() );
							} else {
								if ( getPosX() >= manager.getWidth() - KEY_DEFAULT_WIDTH )
									speedX.setSpeed( -speedX.getSpeed() );
							}

							if ( speedY.getSpeed() < 0 ) {
								if ( getPosY() <= 0 )
									speedY.setSpeed( -speedY.getSpeed() );
							} else {
								if ( getPosY() >= manager.getHeight() - KEY_DEFAULT_WIDTH )
									speedY.setSpeed( -speedY.getSpeed() );
							}
						break;

						case MOVE_LOOP:
							switch ( direction ) {
								case DIRECTION_RIGHT:
									move( speedX.updateInt( delta ), 0 );

									if ( getRefPixelX() >= ( manager.getWidth() >> 2 ) ) {
										direction = DIRECTION_LOOP;
									}
								break;

								case DIRECTION_LEFT:
									move( speedX.updateInt( delta ), 0 );

									if ( getRefPixelX() <= ( ( manager.getWidth() * 3 ) >> 2 ) ) {
										direction = DIRECTION_LOOP;
									}
								break;

								case DIRECTION_UP:
									move( 0, speedY.updateInt( delta ) );

									if ( getRefPixelY() <= ( ( manager.getHeight() * 3 ) >> 2 ) ) {
										direction = DIRECTION_LOOP;
									}
								break;

								case DIRECTION_DOWN:
									move( 0, speedY.updateInt( delta ) );

									if ( getRefPixelY() >= ( manager.getHeight() >> 2 ) ) {
										direction = DIRECTION_LOOP;
									}
								break;

								case DIRECTION_LOOP:
									stateTime += delta;
									final Point refPos = new Point();
									bezier.getPointAtFixed( refPos, NanoMath.divInt( stateTime, totalTime ) );
									setRefPixelPosition( refPos );
									if ( stateTime >= totalTime )
										direction = DIRECTION_AFTER_LOOP;
								break;

								case DIRECTION_AFTER_LOOP:
									move( speedX.updateInt( delta ), speedY.updateInt( delta ) );

									switch ( initialDirection ) {
										case DIRECTION_DOWN:
											if ( getPosY() >= manager.getHeight() )
												reset();
										break;

										case DIRECTION_UP:
											if ( getPosY() <= -KEY_DEFAULT_HEIGHT )
												reset();
										break;

										case DIRECTION_LEFT:
											if ( getPosX() <= -KEY_DEFAULT_WIDTH )
												reset();
										break;

										case DIRECTION_RIGHT:
											if ( getPosX() >= manager.getWidth() )
												reset();
										break;
									}
								break;
							}
						break;

						case MOVE_CLOCKWISE:
						case MOVE_COUNTER_CLOCKWISE:
							switch ( direction ) {
								case DIRECTION_DOWN:
									move( 0, speedY.updateInt( delta ) );
									if ( getRefPixelY() >= managerBottomRight.y ) {
										direction = move == MOVE_CLOCKWISE ? DIRECTION_LEFT : DIRECTION_RIGHT;
										setRefPixelPosition( getRefPixelX(), managerBottomRight.y );
									}
								break;

								case DIRECTION_UP:
									move( 0, -speedY.updateInt( delta ) );
									if ( getRefPixelY() <= managerTopLeft.y ) {
										direction = move == MOVE_CLOCKWISE ? DIRECTION_RIGHT : DIRECTION_LEFT;
										setRefPixelPosition( getRefPixelX(), managerTopLeft.y );
									}
								break;

								case DIRECTION_LEFT:
									move( -speedX.updateInt( delta ), 0 );
									if ( getRefPixelX() <= managerTopLeft.x ) {
										direction = move == MOVE_CLOCKWISE ? DIRECTION_UP : DIRECTION_DOWN;
										setRefPixelPosition( managerTopLeft.x, getRefPixelY() );
									}
								break;

								case DIRECTION_RIGHT:
									move( speedX.updateInt( delta ), 0 );
									if ( getRefPixelX() >= managerBottomRight.x ) {
										direction = move == MOVE_CLOCKWISE ? DIRECTION_DOWN : DIRECTION_UP;
										setRefPixelPosition( managerBottomRight.x, getRefPixelY() );
									}
								break;
							}
						break;
					}
				}
			break;

			case STATE_DYING:
				stateTime += delta;

				if ( stateTime >= 0 ) {
					setRefPixelPosition( origin.x + ( destination.x - origin.x ) * stateTime / TIME_DIE,
								 origin.y + ( destination.y - origin.y ) * stateTime / TIME_DIE );

					if ( stateTime >= TIME_DIE ) {
						setRefPixelPosition( destination );
						setState( STATE_VANISHING );

						if ( pieceListener != null )
							pieceListener.onDie( this );
					}
				}
			break;

			case STATE_VANISHING:
				stateTime -= delta;

				if ( stateTime > 0 ) {
					viewport.height = size.y * stateTime / TIME_VANISH;
					viewport.y = position.y + managerPosition.y + ( ( size.y - viewport.height ) >> 1 );
				} else {
					setState( STATE_NONE );
					
					if ( pieceListener != null )
						pieceListener.onVanish( this );
				}
			break;
		}
	}


	public void draw( Graphics g ) { 
//		if( isVisible() ) { // Test Draw
//			g.setColor( 0x00AA00 );
//			g.fillRect( translate.x + getRefPixelX() - (getWidth() >> 1), translate.y + getRefPixelY() - (getHeight() >> 1), getWidth(), getHeight() );
//			g.setColor( 0x0000AA );
//			g.fillRect( translate.x + getPosX(), translate.y + getPosY() , KEY_DEFAULT_WIDTH, KEY_DEFAULT_HEIGHT );
//			g.setColor( 0xAA0000 );
//			g.fillRect( translate.x + getRefPixelX() - ( KEY_DEFAULT_WIDTH >> 1 ), translate.y + getRefPixelY()  - ( KEY_DEFAULT_HEIGHT >> 1 ), KEY_DEFAULT_WIDTH, KEY_DEFAULT_HEIGHT );
//			g.setColor( 0xAAAA00 );
//			g.fillRect( translate.x + getRefPixelX() - ( 5 ), translate.y + getRefPixelY()  - ( 5 ), 10, 10 );
//		}
		super.draw( g );
		
		if( isVisible() && sequenceIndex == SEQUENCE_NORMAL ) {
			switch ( specialState ) {
				case KEY_SPECIAL_STATE_FROST:
					FROSTSPRITE.setPosition( getRefPixelX() - ( FROSTSPRITE.getWidth() >> 1 ), getRefPixelY() - ( FROSTSPRITE.getHeight() >> 1 ) );
					FROSTSPRITE.setFrame( frameSequenceIndex );
					FROSTSPRITE.draw( g );
				break;
				case KEY_SPECIAL_STATE_RADIATED:
					RADIATIONSPRITE.setPosition( getRefPixelX() - ( FROSTSPRITE.getWidth() >> 1 ), getRefPixelY() - ( FROSTSPRITE.getHeight() >> 1 ) );
					RADIATIONSPRITE.setFrame( frameSequenceIndex );
					RADIATIONSPRITE.draw( g );
				break;
			}
		}
	}

	
	public final void setMove( int move, Point startPos, int time ) {
		setMove( move, startPos, time, -1 );
	}
	

	public final void setMove( int move, Point startPos, int time, int direction ) {
		speedX.setSpeed( 0 );
		speedY.setSpeed( 0 );
		resetSpecialState();

		//#if DEBUG == "true"
			System.out.println( " -> setMove: " + move + " -> " + startPos );
		//#endif

		switch ( move ) {
			case MOVE_RIGHT:
				speedX.setSpeed( manager.getWidth() * 1000 / time );
			break;

			case MOVE_LEFT:
				speedX.setSpeed( -manager.getWidth() * 1000 / time );
			break;

			case MOVE_UP:
				speedY.setSpeed( -manager.getHeight() * 1000 / time );
			break;

			case MOVE_DOWN:
				speedY.setSpeed( manager.getHeight() * 1000 / time );
			break;

			case MOVE_BEZIER:
				// define a posição inicial para que as peças não "brotem" no meio da tela
				setRefPixelPosition( startPos );
			break;

			case MOVE_BOUNCE:
				// faz a média das velocidades em x e y para obter o m�dulo
				final int SPEED_MODULE = ( manager.getWidth() + manager.getHeight() ) * 500 / time;
				final Point SPEED = new Point();
				int angle;
				final byte MIN_ANGLE = 21;
				do {
					angle = NanoMath.randInt( 360 );
				} while ( ( angle % 90 > 90 - MIN_ANGLE ) || ( angle % 90 < MIN_ANGLE ) );
				SPEED.setVector( angle, SPEED_MODULE );
				speedX.setSpeed( SPEED.x );
				speedY.setSpeed( SPEED.y );
			break;

			case MOVE_LOOP:
				// o tempo de loop � 1/2 do total
				//time >>= 1;
			break;

			case MOVE_CLOCKWISE:
			case MOVE_COUNTER_CLOCKWISE:
				speedX.setSpeed( manager.getWidth() * 1000 / time );
				speedY.setSpeed( manager.getHeight() * 1000 / time );

				this.direction = ( byte ) direction;
			break;
		}

		initialDirection = ( byte ) direction;
		stateTime = 0;
		totalTime = ( short ) time;
		this.move = ( byte ) move;
		startPosition.set( startPos );
		setVisible( true );
		reset();

		setState( STATE_PREPARING_MOVE );
	}


	public final void setStateTime( int stateTime ) {
		this.stateTime = ( short ) stateTime;
	}


	public final void setFrame( int index ) {
		super.setFrame( index );
		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
	}


	public final Sprite getTarget() {
		return target;
	}


	private final void reset() {
		stateTime = 0;

		switch ( move ) {
			case MOVE_LOOP:
				switch ( initialDirection ) {
					case DIRECTION_LEFT:
						speedX.setSpeed( -manager.getWidth() * 1000 / totalTime );
						bezier.setValues( new Point( ( manager.getWidth() * 3 ) >> 2, startPosition.y ),
										 new Point( - manager.getWidth() >> 1, manager.getHeight() ),
										 new Point( ( manager.getWidth() * 3 ) >> 1, manager.getHeight() ),
										 new Point( manager.getWidth() >> 2, startPosition.y ) );
					break;

					case DIRECTION_RIGHT:
						speedX.setSpeed( manager.getWidth() * 1000 / totalTime );
						bezier.setValues( new Point( manager.getWidth() >> 2, startPosition.y ),
										 new Point( ( manager.getWidth() * 3 ) >> 1, 0 ),
										 new Point( - manager.getWidth() >> 1, 0 ),
										 new Point( ( manager.getWidth() * 3 ) >> 2, startPosition.y ) );
					break;

					case DIRECTION_DOWN:
						speedY.setSpeed( manager.getHeight() * 1000 / totalTime );
						bezier.setValues( new Point( startPosition.x, manager.getHeight() >> 2 ),
										 new Point( manager.getWidth(), - manager.getHeight() >> 1 ),
										 new Point( manager.getWidth(), ( manager.getHeight() * 3 ) >> 1 ),
										 new Point( startPosition.x, ( manager.getHeight() * 3 ) >> 2 ) );
					break;

					case DIRECTION_UP:
						speedY.setSpeed( -manager.getHeight() * 1000 / totalTime );
						bezier.setValues( new Point( startPosition.x, ( manager.getHeight() * 3 ) >> 2 ),
										 new Point( 0, ( manager.getHeight() * 3 ) >> 1 ),
										 new Point( 0, - manager.getHeight() >> 1 ),
										 new Point( startPosition.x, manager.getHeight() >> 2 ) );
					break;
				}

				setRefPixelPosition( startPosition );
				direction = initialDirection;
			break;

			case MOVE_BEZIER:
				final Point bezierOrigin = getRefPixelPosition();
				final Point[] points = new Point[ 3 ];

				final int MAX_OFFSET_X = ScreenManager.SCREEN_WIDTH / 5;
				final int MAX_OFFSET_Y = ScreenManager.SCREEN_HEIGHT / 5;

				final Point min = new Point( -MAX_OFFSET_X, -MAX_OFFSET_Y );
				final Point max = new Point( ScreenManager.SCREEN_WIDTH + MAX_OFFSET_X - KEY_DEFAULT_WIDTH, ScreenManager.SCREEN_HEIGHT + MAX_OFFSET_Y - KEY_DEFAULT_HEIGHT );

				for ( int i = 0; i < 2; ++i ) {
					points[ i ] = new Point( min.x + NanoMath.randInt( max.x - min.x ),
											 min.y + NanoMath.randInt( max.y - min.y ) );
				}
				points[ 2 ] = new Point( managerTopLeft.x + NanoMath.randInt( managerBottomRight.x - managerTopLeft.x ),
										 managerTopLeft.y + NanoMath.randInt( managerBottomRight.y - managerTopLeft.y ) );

				bezier.setValues( bezierOrigin, points[ 0 ], points[ 1 ], points[ 2 ] );
			break;

			default:
				setRefPixelPosition( startPosition );
			break;
		}
	}
	
	
	public static final void ResetDyingCount() {
		CAN_PLAY = true;
		DYING_KEYS = 0;
	}


	public static final void RemoveDyingCount() {
		//#if DEBUG == "true"
			System.out.println( "---------from---DYING_KEYS: "+ DYING_KEYS +"-------------------" );
		//#endif
		DYING_KEYS--;
		//#if DEBUG == "true"
			System.out.println( "----------to----DYING_KEYS: "+ DYING_KEYS +"-------------------" );
		//#endif
		if( DYING_KEYS > 0 ) {
			if ( CAN_PLAY ) {
				MediaPlayer.play( SOUND_MULTIPLE_LOCKS );
				//#if DEBUG == "true"
					System.out.println( "-> Played SOUND_MULTIPLE_LOCKS _/\\_/`-´\\_/\\_/\\_/`-´\\_/\\_" );
				//#endif
				CAN_PLAY = false;
			}
		} else {
			if ( CAN_PLAY ) {
				MediaPlayer.play( SOUND_LOCK_OPEN );
				//#if DEBUG == "true"
					System.out.println( "-> Played SOUND_LOCK_OPEN _/`-´\\_" );
				//#endif
			}
			DYING_KEYS = 0;
			CAN_PLAY = true;
		}
	}


	public final void setState( int state ) {
		setViewport( null );
		setVisible( true );

		switch ( state ) {
			case STATE_NONE:
				setVisible( false );
			break;

			case STATE_DYING:
				DYING_KEYS++;
				stateTime = 0;
				origin.set( getRefPixelPosition() );
//				final byte previousFrame = getFrameSequenceIndex();
//				setSequence( SEQUENCE_DYING );
//				setFrame( previousFrame );
			break;

			case STATE_VANISHING:
				final byte previousFrame = getFrameSequenceIndex();
				setSequence( SEQUENCE_DYING );
				setFrame( previousFrame );
				stateTime = TIME_VANISH;
				setViewport( new Rectangle( position.add( managerPosition ), size ) );
			break;
		}

		this.state = ( byte ) state;
	}


	public final byte getState() {
		return state;
	}


	public final boolean isShot( Rectangle rect ) {
		return isVisible() && 
				( getState() == STATE_MOVING ) && 
				new Rectangle( getRefPixelX() - ( KEY_DEFAULT_WIDTH >> 1 ),
				getRefPixelY() - ( KEY_DEFAULT_HEIGHT >> 1 ) , KEY_DEFAULT_WIDTH,
				KEY_DEFAULT_HEIGHT ).intersects( rect );
	}


	public final void setTarget( Sprite target ) {
		this.target = target;
		setSequence( SEQUENCE_NORMAL );

		if ( target != null ) {
			destination.set( target.getPosition().add( offset ) );
			destination.x += target.getWidth() >> 1;
			destination.y += target.getHeight() >> 1;
			setTransform( target.getTransform() );
			setFrame( target.getFrameSequenceIndex() );
		}
	}


	public static final void setPuzzlePosition( Point puzzlePosition, Point managerPosition ) {
		offset.set( managerPosition.sub( puzzlePosition ) );
		Key.managerPosition.set( managerPosition );
	}


	public static final void setManagerLimits( Point topLeft, Point bottomRight ) {
		managerTopLeft = topLeft;
		managerBottomRight = bottomRight;
	}
}