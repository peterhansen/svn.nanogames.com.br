/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public interface Constants {

	/** Nome curto do jogo, para submissão ao Nano Online. */
	public static final String APP_SHORT_NAME = "SEXP";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";

	public static final byte RANKING_INDEX_ACC_SCORE = 0;
	public static final byte RANKING_INDEX_MAX_SCORE = 1;
	public static final byte RANKING_INDEX_MAX_LEVEL = 2;
	
	// <editor-fold defaultstate="collapsed" desc="PATHS DOS ARQUIVOS">
	
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_GIRLS = PATH_IMAGES + "girls/";

	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	/** Caminho das imagens usadas no Nano Online. */
	public static final String PATH_ONLINE = PATH_IMAGES + "online/";

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="DATABASE INFO">
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";	
	
	/**Índice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;

	public static final byte DATABASE_SLOT_GAME_DATA = 2;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 2;

	/** Id da categoria de notícias do Topa ou Não Topa, no Nano Online. */
	public static final byte NEWS_CATEGORY_ID = 6;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="CONSTANTES DE JOGO">

	// <editor-fold defaultstate="collapsed" desc="Seda Copy">
	/** Penalidade de tempo ao errar um tiro. */
	public static final short TIME_PENALTY_MISS = 2000;

	/** Incremento no tempo restante a cada pe�a atingida, em milisegundos. */
	public static final short TIME_PIECE_INC = 2180;

	/** Pontos ganhos por cada pe�a atingida. */
	public static final short SCORE_PIECE = 10;

	public static final short SCORE_SUPER_COMBO = SCORE_PIECE * 10;
	
	/** Nível de dificuldade máximo. */
	public static final byte LEVEL_HARD = 15;
	
	/** Duração total do combo. */
	public static final short TIME_COMBO = 2710;
	public static final short TIME_COMBO_START_BLINK = TIME_COMBO / 3;

	/** Tempo m�ximo restante na dificuldade m�nima. */
	public static final int TIME_TOTAL_EASY = 60000;

	/** Tempo m�ximo restante na dificuldade m�xima. */
	public static final short TIME_TOTAL_HARD = 11000;

	/** Porcentagem do tempo padr�o caso o aparelho suporte touch screen. */
	public static final byte TIME_PERCENT_TOUCHSCREEN = 75;
	// </editor-fold>

	public static final int COLOR_BACKGROUND = 0x511617;
	public static final int COLOR_BACKGROUND_BORDER_HORIZONTAL_DECAL = 0xCF5D5E;
	public static final int COLOR_BACKGROUND_BORDER_VERTICAL_DECAL = 0x6E0203;
	public static final int COLOR_BACKGROUND_BORDER = 0x8F2425;

	/**Tipos de chaves encontradas no jogo*/
	public static final byte KEY_TYPE_HEART			= 0;
	public static final byte KEY_TYPE_TRIANGLE		= 1;
	public static final byte KEY_TYPE_SQUARE		= 2;
	public static final byte KEY_TYPE_ARCH			= 3;
	public static final byte KEY_TYPE_PENTAGON		= 4;
	public static final byte KEY_TYPE_CIRCLE		= 5;
	public static final byte KEY_TYPE_HEXAGON		= 6;

	/**Quantidade de diferentes tipos de chaves*/
	public static final byte KEY_TYPES = 7;

	/**Quantidade de diferentes tipos de chaves*/
	public static final byte MENU_GIRLS_TYPES = 8;

	/**Quantidade de diferentes tipos de chaves*/
	public static final byte TOTAL_GIRLS_PHOTOS = 15;


	public static final byte OPTIONS_STATE_NONE = 0;
	public static final byte OPTIONS_STATE_ON_OF = 1;
	public static final byte OPTIONS_STATE_0_100 = 2;
	
	// </editor-fold>
		
	// <editor-fold defaultstate="collapsed" desc="HARDWARE CONFIGURATION">
	
	/** Duração da vibração ao atingir a trave, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 300;

//#if SCREEN_SIZE == "SMALL"
//# 		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#elif SCREEN_SIZE == "BIG"
		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
		public static final int LOW_MEMORY_LIMIT = 1600000;
    //#else
//# 		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif
		
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES">

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS RANKINGS">
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES E DADOS DAS FONTES DE TEXTO">
	
	/** Offset da fonte padrão. */
	public static final byte DEFAULT_FONT_OFFSET = 1;
	
	/**Índice da fonte utilizada para textos. */
	public static final byte FONT_MENU			= 0;
	public static final byte FONT_NIVEL			= 1;
	public static final byte FONT_TEXT_WHITE	= 2;
	public static final byte FONT_BONUS			= 3;
	public static final byte FONT_PONTOS		= 4;
	public static final byte FONT_MENU_2		= 5;
	public static final byte FONT_NIVEL_2		= 6;
	public static final byte FONT_VIB_VOL		= 7;
	public static final byte FONT_SOFTKEY		= 8;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= FONT_SOFTKEY + 1;
	
	/**Índice da fonte padrão do jogo. */
	public static final byte FONT_INDEX_DEFAULT	= FONT_TEXT_WHITE;
	
	// </editor-fold>
		
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS SONS">
	
	//índices dos sons do jogo
	public static final byte SOUND_KEY_COLLECTED		= 0;
	public static final byte SOUND_LOCK_OPEN			= SOUND_KEY_COLLECTED + 1;
	public static final byte SOUND_SEXY_SPECIAL			= SOUND_LOCK_OPEN + 1;
	public static final byte SOUND_COLD_SPICY			= SOUND_SEXY_SPECIAL + 1;
	public static final byte SOUND_HOT_SPICY			= SOUND_COLD_SPICY + 1;
	public static final byte SOUND_RADIATED_SPICY		= SOUND_HOT_SPICY + 1;
	public static final byte SOUND_MULTIPLE_LOCKS		= SOUND_RADIATED_SPICY + 1;
	public static final byte SOUND_FIRST_MUSIC			= SOUND_MULTIPLE_LOCKS + 1;
	public static final byte SOUND_GAME_OVER_MUSIC		= SOUND_FIRST_MUSIC + 1;
	public static final byte SOUND_LAST_MUSIC			= SOUND_GAME_OVER_MUSIC + 1;

	//total de sons
	public static final byte SOUND_TOTAL = SOUND_LAST_MUSIC + 1;

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS LISTENERS">

	public static final byte LISTENER_ID_BIG_CASE	= 0;
	public static final byte LISTENER_ID_TRANSITION	= 1;
	public static final byte LISTENER_ID_VALUES_BOX	= 2;
	public static final byte LISTENER_ID_BANKER		= 3;

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS TEXTOS">

	public static final short TEXT_OK							     	= 0;
	public static final short TEXT_BACK									= TEXT_OK + 1;
	public static final short TEXT_NEW_GAME								= TEXT_BACK + 1;
	public static final short TEXT_OPTIONS								= TEXT_NEW_GAME + 1;
	public static final short TEXT_NANO_ONLINE							= TEXT_OPTIONS + 1;
	public static final short TEXT_HELP									= TEXT_NANO_ONLINE + 1;
	public static final short TEXT_CREDITS								= TEXT_HELP + 1;
	public static final short TEXT_EXIT									= TEXT_CREDITS + 1;
	public static final short TEXT_PAUSE								= TEXT_EXIT + 1;
	public static final short TEXT_CREDITS_TEXT							= TEXT_PAUSE + 1;
	public static final short TEXT_CONTROLS								= TEXT_CREDITS_TEXT + 1;
	public static final short TEXT_RULES							    = TEXT_CONTROLS + 1;
	public static final short TEXT_HELP_PROFILE							= TEXT_RULES + 1;
	public static final short TEXT_HELP_CONTROLS						= TEXT_HELP_PROFILE + 1;
	public static final short TEXT_HELP_OBJECTIVES						= TEXT_HELP_CONTROLS + 1;
	public static final short TEXT_HELP_PROFILE_TEXT					= TEXT_HELP_OBJECTIVES + 1;
	public static final short TEXT_HELP_IMAGEGALLERY_TEXT				= TEXT_HELP_PROFILE_TEXT + 1;
	public static final short TEXT_TURN_OFF								= TEXT_HELP_IMAGEGALLERY_TEXT + 1;
	public static final short TEXT_TURN_ON								= TEXT_TURN_OFF + 1;
	public static final short TEXT_ON									= TEXT_TURN_ON + 1;
	public static final short TEXT_OFF									= TEXT_ON + 1;
	public static final short TEXT_SOUND								= TEXT_OFF + 1;
	public static final short TEXT_VIBRATION							= TEXT_SOUND + 1;
	public static final short TEXT_CANCEL								= TEXT_VIBRATION + 1;
	public static final short TEXT_CONTINUE								= TEXT_CANCEL + 1;
	public static final short TEXT_SPLASH_NANO							= TEXT_CONTINUE + 1;
	public static final short TEXT_SPLASH_BRAND							= TEXT_SPLASH_NANO + 1;
	public static final short TEXT_LOADING								= TEXT_SPLASH_BRAND + 1;
	public static final short TEXT_DO_YOU_WANT_SOUND					= TEXT_LOADING + 1;
	public static final short TEXT_YES									= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final short TEXT_NO									= TEXT_YES + 1;
	public static final short TEXT_BACK_MENU							= TEXT_NO + 1;
	public static final short TEXT_CONFIRM_BACK_MENU					= TEXT_BACK_MENU + 1;
	public static final short TEXT_EXIT_GAME							= TEXT_CONFIRM_BACK_MENU + 1;
	public static final short TEXT_CONFIRM_EXIT_1						= TEXT_EXIT_GAME + 1;
	public static final short TEXT_PRESS_ANY_KEY						= TEXT_CONFIRM_EXIT_1 + 1;
	public static final short TEXT_GAME_OVER							= TEXT_PRESS_ANY_KEY + 1;
	public static final short TEXT_RECOMMEND_TITLE						= TEXT_GAME_OVER + 1;
	public static final short TEXT_PROFILE_SELECTION					= TEXT_RECOMMEND_TITLE + 1;
    public static final short TEXT_CHOOSE_ANOTHER					    = TEXT_PROFILE_SELECTION + 1;
    public static final short TEXT_CONFIRM						        = TEXT_CHOOSE_ANOTHER + 1;
    public static final short TEXT_NO_PROFILE							= TEXT_CONFIRM + 1;
	public static final short TEXT_TIME_FORMAT							= TEXT_NO_PROFILE + 1;
	public static final short TEXT_HIGH_SCORES					        = TEXT_TIME_FORMAT + 1;
	public static final short TEXT_VOLUME						        = TEXT_HIGH_SCORES + 1;
	public static final short TEXT_VERSION						        = TEXT_VOLUME + 1;
	public static final short TEXT_PHOTO_GALLERY			            = TEXT_VERSION + 1;
	public static final short TEXT_LEVEL_CLEARED			            = TEXT_PHOTO_GALLERY + 1;
	public static final short TEXT_LEVEL_SHOW				            = TEXT_LEVEL_CLEARED + 1;
	public static final short TEXT_NEW_RECORD					        = TEXT_LEVEL_SHOW + 1;
	public static final short TEXT_RESTART					            = TEXT_NEW_RECORD + 1;
	public static final short TEXT_RECOMMEND_SENT						= TEXT_RESTART + 1;
	public static final short TEXT_RECOMMEND_TEXT						= TEXT_RECOMMEND_SENT + 1;
	public static final short TEXT_RECOMMENDED							= TEXT_RECOMMEND_TEXT + 1;
	public static final short TEXT_RECOMMEND_SMS						= TEXT_RECOMMENDED + 1;
	public static final short TEXT_RANKING_ACCUMULATED_POINTS			= TEXT_RECOMMEND_SMS + 1;
	public static final short TEXT_RANKING_MAX_POINTS					= TEXT_RANKING_ACCUMULATED_POINTS + 1;
	public static final short TEXT_RANKING_MAX_LEVEL					= TEXT_RANKING_MAX_POINTS + 1;
	public static final short TEXT_CURRENT_PROFILE						= TEXT_RANKING_MAX_LEVEL + 1;


	/** número total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_CURRENT_PROFILE + 1;

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS TELAS DO JOGO">

//	public static final byte SCREEN_CHOOSE_LANGUAGE				= 0;
	public static final byte SCREEN_CHOOSE_SOUND				= 0;
	public static final byte SCREEN_SPLASH_NANO					= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME					= SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU					= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME					= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CONTINUE_GAME				= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_NEXT_LEVEL					= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_OPTIONS						= SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_HIGH_SCORES					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_NANO_RANKING_MENU			= SCREEN_HIGH_SCORES + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_HELP						= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_HELP_PROFILE				= SCREEN_HELP + 1;
	public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_HELP_PROFILE + 1;
	public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_CREDITS						= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_PAUSE						= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU				= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT				= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1					= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_LOADING_2					= SCREEN_LOADING_1 + 1;
	public static final byte SCREEN_LOADING_GAME				= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_LOADING_GAME + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_LOADING_PLAY_SCREEN			= SCREEN_NO_PROFILE + 1;
	public static final byte SCREEN_PHOTO_GALLERY				= SCREEN_LOADING_PLAY_SCREEN + 1;
	public static final byte SCREEN_GAME_OVER_MENU				= SCREEN_PHOTO_GALLERY + 1;
	public static final byte SCREEN_NEW_RECORD_MENU				= SCREEN_GAME_OVER_MENU + 1;
	public static final byte SCREEN_LOADING_PHOTO_GALLERY		= SCREEN_NEW_RECORD_MENU + 1;
	public static final byte SCREEN_LOG							= SCREEN_LOADING_PHOTO_GALLERY + 1;

	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG				= SCREEN_LOG + 1;
	//#endif

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_NEWS		    = 2;
	public static final byte ENTRY_MAIN_MENU_NANO_ONLINE    = 3;
	public static final byte ENTRY_MAIN_MENU_RECOMMEND		= 4;
	public static final byte ENTRY_MAIN_MENU_HELP			= 5;
	public static final byte ENTRY_MAIN_MENU_CREDITS		= 6;
	public static final byte ENTRY_MAIN_MENU_EXIT			= 7;
	
	//#if DEBUG == "true"
		public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE PAUSA">

	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VOLUME				= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 4;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 5;

	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 3;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 4;

	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VOLUME					= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 2;

	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 2;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 3;

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE FIM DE JOGO">

	public static final byte ENTRY_GAME_OVER_HIGH_SCORES	= 0;
	public static final byte ENTRY_GAME_OVER_NEW_GAME		= 1;
	public static final byte ENTRY_GAME_OVER_BACK_MENU		= 2;
	public static final byte ENTRY_GAME_OVER_EXIT_GAME		= 3;

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DA TELA DE SELEÇÃO DE PERFIL">

	public static final byte ENTRY_CHOOSE_PROFILE_USE_CURRENT	= 0;
	public static final byte ENTRY_CHOOSE_PROFILE_CHANGE		= 1;
	public static final byte ENTRY_CHOOSE_PROFILE_HELP			= 2;
	public static final byte ENTRY_CHOOSE_PROFILE_BACK			= 3;

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE NOTÍCIAS">
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU INICIAL DE SOM">

	public static final byte ENTRY_SOUND_ON_OFF		= 0;
	public static final byte ENTRY_SOUND_VOLUME		= 1;
	public static final byte ENTRY_SOUND_OK			= 2;

	// </editor-fold>
	
	// </editor-fold>

	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA CADA TELA">
	
	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA PEQUENA">
	//#if SCREEN_SIZE == "SMALL"
//#   	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//#   	public static final byte SAFE_MARGIN = 5;
//#
//#  	public static final byte COLLECTION_OFFSET_X = 1;
//#  	public static final byte COLLECTION_OFFSET_Y = 4;
//#
//#   	/** Altura padrão das chaves. */
//#   	public static final byte KEY_DEFAULT_HEIGHT = 20;
//#   	/** Largura padrão das chaves. */
//#   	public static final byte KEY_DEFAULT_WIDTH = 12;
//#
//#   	/** Largura total interna da barra de bonus. */
//#   	public static final byte BONUS_BAR_WIDTH = 40;
//#   	/** Altura total interna da barra de bonus. */
//#   	public static final byte BONUS_BAR_HEIGHT = 7;
//#
//#   	/** Posição x da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
//#   	public static final byte BONUS_BAR_X = 2;
//#   	/** Posição y da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
//#   	public static final byte BONUS_BAR_Y = 2;
//#
//#   	/** espaçamento entre o puzzle e a barra de bonus e o label de pontos */
//#   	public static final byte HUD_PUZZLE_DISTANCE = 2;
//#
//#   	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//#   	public static final byte CLOCK_SAFE_MARGIN = 1;
//#   	/** Di�metro do indicador de tempo da tela de jogo. */
//#   	public static final byte CLOCK_DIAMETER = 11;
//#   	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
//#   	public static final byte CLOCK_SCORE_SPACING = 3;
//#
//#  	/** Tamanho de cada bloco do puzzle. */
//#  	public static final byte BLOCK_SIZE = 21;
//#  	public static final short PUZZLE_MAX_SIZE = 147;
//#
//#  	public static final short SCROLL_BAR_WIDTH = 5;
//#
//#  	/** Grossura da chave da imagem extra_key.png */
//#  	public static final byte SPLASH_EXTRA_KEY_THICKNESS = 4;
//#  	public static final byte SPLASH_EXTRA_KEY_TO_CHAIN = 18;
//#
//#  	/** Grossura da borda que ronda o puzzle */
//#  	public static final byte PUZZLE_BORDER_THICKNESS = 4;
//#
//#  	/** Grossura da borda que ronda o puzzle */
//#  	public final byte TEXT_BKG_BORDER = SAFE_MARGIN;
//#  	/** Distância entre a borda da tela e a caixa texto */
//#  	public final static int TEXT_ABSOLUTE_MARGIN = TEXT_BKG_BORDER + SAFE_MARGIN;
//#
//#  	/** Distância mínima entre a borda da tela e o logo da SexyHot */
//#  	public final static int SEXY_LOGO_MIN_MARGIN = 3;
//#
//#  	public final static int ITEM_SPACING = 8;
//#  	public final static int FONT_WHITE_HEIGHT = 14;
//#  	public final static int NUMBER_LABEL_HEIGHT = 12;
//# 	//</editor-fold>
//#
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA MÉDIA">
	//#elif SCREEN_SIZE == "MEDIUM"
//#   	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//#   	public static final byte SAFE_MARGIN = 5;
//# 
//#  	public static final byte COLLECTION_OFFSET_X = 2;
//#  	public static final byte COLLECTION_OFFSET_Y = 4;
//# 
//#   	/** Altura padrão das chaves. */
//#   	public static final byte KEY_DEFAULT_HEIGHT = 26;
//#   	/** Largura padrão das chaves. */
//#   	public static final byte KEY_DEFAULT_WIDTH = 15;
//# 
//#   	/** Largura total interna da barra de bonus. */
//#   	public static final byte BONUS_BAR_WIDTH = 42;
//#   	/** Altura total interna da barra de bonus. */
//#   	public static final byte BONUS_BAR_HEIGHT = 9;
//# 
//#   	/** Posição x da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
//#   	public static final byte BONUS_BAR_X = 2;
//#   	/** Posição y da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
//#   	public static final byte BONUS_BAR_Y = 3;
//# 
//#   	/** espaçamento entre o puzzle e a barra de bonus e o label de pontos */
//#   	public static final byte HUD_PUZZLE_DISTANCE = 4;
//# 
//#   	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//#   	public static final byte CLOCK_SAFE_MARGIN = 1;
//#   	/** Di�metro do indicador de tempo da tela de jogo. */
//#   	public static final byte CLOCK_DIAMETER = 15;
//#   	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
//#   	public static final byte CLOCK_SCORE_SPACING = 3;
//# 
	//#if HYBRID == "true"
//# 		/** Tamanho de cada bloco do puzzle. */
//# 		public static final byte BLOCK_SIZE = 21;
//# 		public static final short PUZZLE_MAX_SIZE = 147;
	//#else
//# 		/** Tamanho de cada bloco do puzzle. */
//# 		public static final byte BLOCK_SIZE = 25;
//# 		public static final short PUZZLE_MAX_SIZE = 175;
	//#endif
//# 
//#  	public static final short SCROLL_BAR_WIDTH = 5;
//# 
//#  	/** Grossura da chave da imagem extra_key.png */
//#  	public static final byte SPLASH_EXTRA_KEY_THICKNESS = 9;
//#  	public static final byte SPLASH_EXTRA_KEY_TO_CHAIN = 25;
//# 
//#  	/** Grossura da borda que ronda o puzzle */
//#  	public static final byte PUZZLE_BORDER_THICKNESS = 4;
//# 
//#  	/** Grossura da borda que ronda o puzzle */
//#  	public final byte TEXT_BKG_BORDER = 5;
//#  	/** Distância entre a borda da tela e a caixa texto */
//#  	public final static int TEXT_ABSOLUTE_MARGIN = TEXT_BKG_BORDER + SAFE_MARGIN;
//# 
//#  	/** Distância mínima entre a borda da tela e o logo da SexyHot */
//#  	public final static int SEXY_LOGO_MIN_MARGIN = 4;
//# 
//#  	public final static int ITEM_SPACING = 12;
//#  	public final static int FONT_WHITE_HEIGHT = 14;
//# 	public final static int NUMBER_LABEL_HEIGHT = 17;
//# 
//# 	//</editor-fold>
//# 
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE">
	//#elif SCREEN_SIZE == "BIG"
 	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
 	public static final byte SAFE_MARGIN = 7;

	public static final byte COLLECTION_OFFSET_X = 5;
	public static final byte COLLECTION_OFFSET_Y = 9;

 	/** Altura padrão das chaves. */
 	public static final byte KEY_DEFAULT_HEIGHT = 36;
 	/** Largura padrão das chaves. */
 	public static final byte KEY_DEFAULT_WIDTH = 20;

 	/** Largura total interna da barra de bonus. */
 	public static final byte BONUS_BAR_WIDTH = 61;
 	/** Altura total interna da barra de bonus. */
 	public static final byte BONUS_BAR_HEIGHT = 13;

 	/** Posição x da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
 	public static final byte BONUS_BAR_X = 2;
 	/** Posição y da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
 	public static final byte BONUS_BAR_Y = 3;

 	/** espaçamento entre o puzzle e a barra de bonus e o label de pontos */
 	public static final byte HUD_PUZZLE_DISTANCE = 5;

 	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
 	public static final byte CLOCK_SAFE_MARGIN = 1;
 	/** Di�metro do indicador de tempo da tela de jogo. */
 	public static final byte CLOCK_DIAMETER = 26;
 	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
 	public static final byte CLOCK_SCORE_SPACING = 3;

	/** Tamanho de cada bloco do puzzle. */
	public static final byte BLOCK_SIZE = 35;
	public static final short PUZZLE_MAX_SIZE = 315;

	public static final short SCROLL_BAR_WIDTH = 5;

	public static final short SPLASH_CHAIN_KEY_HORIZONTAL_POSITION_X = 39; // 152 - 100 - 13
	/** Grossura da chave da imagem extra_key.png */
	public static final byte SPLASH_EXTRA_KEY_THICKNESS = 9;
	public static final byte SPLASH_EXTRA_KEY_TO_CHAIN = 39;

	/** Grossura da borda que ronda o puzzle */
	public static final byte PUZZLE_BORDER_THICKNESS = 4;

	/** Grossura da borda que ronda o puzzle */
	public final byte TEXT_BKG_BORDER = 5;
	/** Distância entre a borda da tela e a caixa texto */
	public final static int TEXT_ABSOLUTE_MARGIN = TEXT_BKG_BORDER + SAFE_MARGIN;

	/** Distância mínima entre a borda da tela e o logo da SexyHot */
	public final static int SEXY_LOGO_MIN_MARGIN = 5;

	public final static int ITEM_SPACING = 20;
	public final static int FONT_WHITE_HEIGHT = 14;
	public final static int NUMBER_LABEL_HEIGHT = 22;
	public final static int KEYPAD_MINIMUM_SIZE = 90;

	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GIGANTE">
	//#elif SCREEN_SIZE == "GIANT"
//#  	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//#  	public static final byte SAFE_MARGIN = 10;
//# 
//# 	public static final byte COLLECTION_OFFSET_X = 5;
//# 	public static final byte COLLECTION_OFFSET_Y = 11;
//# 
//#  	/** Altura padrão das chaves. */
//#  	public static final byte KEY_DEFAULT_HEIGHT = 52;
//#  	/** Largura padrão das chaves. */
//#  	public static final byte KEY_DEFAULT_WIDTH = 31;
//# 
//#  	/** Largura total interna da barra de bonus. */
//#  	public static final byte BONUS_BAR_WIDTH = 89;
//#  	/** Altura total interna da barra de bonus. */
//#  	public static final byte BONUS_BAR_HEIGHT = 20;
//# 
//#  	/** Posição x da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
//#  	public static final byte BONUS_BAR_X = 3;
//#  	/** Posição y da barra de bonus, relativa a imagem da barra (relógio, proxima chave e barra de bonus). */
//#  	public static final byte BONUS_BAR_Y = 4;
//# 
//#  	/** espaçamento entre o puzzle e a barra de bonus e o label de pontos */
//#  	public static final byte HUD_PUZZLE_DISTANCE = 5;
//# 
//#  	/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//#  	public static final byte CLOCK_SAFE_MARGIN = 1;
//#  	/** Di�metro do indicador de tempo da tela de jogo. */
//#  	public static final byte CLOCK_DIAMETER = 35;
//#  	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
//#  	public static final byte CLOCK_SCORE_SPACING = 3;
//# 
//# 	/** Tamanho de cada bloco do puzzle. */
//# 	public static final byte BLOCK_SIZE = 50;
//# 
//# 	/** Tamanho do puzzle. */
//# 	public static final short PUZZLE_MAX_SIZE = 450;
//#     public static final short SCROLL_BAR_WIDTH = 6;
//# 
//# 	/** Grossura da chave da imagem extra_key.png */
//# 	public static final byte SPLASH_EXTRA_KEY_THICKNESS = 14;
//# 
//# 	public static final byte SPLASH_CHAIN_KEY_HORIZONTAL_POSITION_X = 56; // 227 - 22 - 149
//# 	/** Grossura da chave da imagem extra_key.png */
//# 	public static final byte SPLASH_EXTRA_KEY_TO_CHAIN = 58;
//# 
//# 	/** Grossura da borda que ronda o puzzle */
//# 	public static final byte PUZZLE_BORDER_THICKNESS = 4;
//# 
//# 	/** Grossura da borda que ronda o puzzle */
//# 	public final byte TEXT_BKG_BORDER = 5;
//# 	/** Distância entre a borda da tela e a caixa texto */
//# 	public final static int TEXT_ABSOLUTE_MARGIN = TEXT_BKG_BORDER + SAFE_MARGIN;
//# 
//# 	/** Distância mínima entre a borda da tela e o logo da SexyHot */
//# 	public final static int SEXY_LOGO_MIN_MARGIN = 5;
//# 
//# 	public final static int ITEM_SPACING = 25;
//# 	public final static int FONT_WHITE_HEIGHT = 12;
//# 	public final static int NUMBER_LABEL_HEIGHT = 27;
//# 	public final static int KEYPAD_MINIMUM_SIZE = 120;
//# 
//#  	//</editor-fold>
//# 
//# 
	//#endif
	//</editor-fold>
	
	//</editor-fold>
}
