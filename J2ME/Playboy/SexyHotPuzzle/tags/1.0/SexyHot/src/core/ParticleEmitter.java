/**
 * SplashEmitter.java
 * �2008 Nano Games.
 *
 * Created on May 1, 2008 10:53:37 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class ParticleEmitter extends Drawable implements Updatable, Constants {
	
	public static byte EMIT_PARTICLES;
	public static short MAX_PARTICLES;
	
	public static final short PARTICLE_LIFE_TIME = 400;
	
	private static Particle[] particles;
	
	private short activeParticles;

	private static Sprite FADE;
	
	
	public ParticleEmitter() throws Exception {
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		Particle.parent = this;
		setClipTest( false );
	}


	public static final void load() throws Exception {
		FADE = new Sprite( new Sprite( PATH_IMAGES + "fade" ) );
		FADE.setClipTest( false );

		// normalmente aparelhos com pouca mem�ria tamb�m possuem baixo desempenho
		if ( GameMIDlet.isLowMemory() ) {
			EMIT_PARTICLES = 5;
		} else {
			//#if  SCREEN_SIZE == "BIG" ||  SCREEN_SIZE == "GIANT"
				EMIT_PARTICLES = 15;
			//#else
//# 				EMIT_PARTICLES = 7;
			//#endif
		}
		MAX_PARTICLES = ( short ) ( ( 11 * EMIT_PARTICLES ) );

		particles = new Particle[ MAX_PARTICLES ];
		for ( short i = 0; i < MAX_PARTICLES; ++i ) {
			particles[ i ] = new Particle();
		}
	}


	public static final Sprite getFade() {
		return FADE;
	}


	protected final void paint( Graphics g ) {
		for ( short i = 0; i < activeParticles; ++i ) {
			particles[ i ].draw( g );
		}
	}
	
	
	public final void update( int delta ) {
		final short previousActiveParticles = activeParticles;
		for ( short i = 0; i < previousActiveParticles; ++i )
			particles[ i ].update( delta );
		
		if ( previousActiveParticles != activeParticles )
			sort();
	}	
	
	
	public final void reset() {
		activeParticles = 0;
	}	
	
	
	private final void sort() {
		short i = 0;
		short j = ( short ) ( MAX_PARTICLES - 1 );
		for ( ; i < MAX_PARTICLES; ++i ) {
			if ( !particles[ i ].visible ) {
				for ( ; j > i; --j ) {
					if ( particles[ j ].visible ) {
						final Particle temp = particles[ i ];
						particles[ i ] = particles[ j ];
						particles[ j ] = temp;
						break;
					}
				}
			}
		}
		
		activeParticles = j;
	}
	
	
	public final void emit( Point pos, int quantity ) {
		final int limit = NanoMath.min( MAX_PARTICLES, quantity + activeParticles );
		for ( short i = activeParticles; i < limit; ++i ) {
			particles[ i ].born( pos );
		}
		activeParticles = ( short ) limit;
	}
	
	
	private final void onDie() {
		--activeParticles;
		
		//#if DEBUG == "true"
		if ( activeParticles < 0 )
			throw new RuntimeException( "N�mero de part�culas negativo." );
		//#endif
	}
	
	
	/**
	 * Classe interna que descreve uma part�cula de fogos de artif�cio. 
	 */
	private static final class Particle {
		
		/** Tempo acumulado da part�cula. */
		private short accTime;
		
		private final Point position = new Point();
		
		private static ParticleEmitter parent;

		private boolean visible;

		private byte frame;

		
		private final void born( Point bornPosition ) {
			accTime = 0;
			position.set( bornPosition.sub( FADE.getWidth() >> 1, FADE.getHeight() >> 1 ) );
			visible = true;
		}

		
		public final void update( int delta ) {
			if ( visible ) {
				accTime += delta;

				if ( accTime < PARTICLE_LIFE_TIME ) {
					frame = ( byte ) ( accTime * FADE.getSequence( 0 ).length / PARTICLE_LIFE_TIME );
				} else {
					visible = false;
					parent.onDie();
				}
			}
		}


		public final void draw( Graphics g ) {
			if ( visible ) {
				FADE.setFrame( frame );
				FADE.setPosition( position.add( translate ) );
				FADE.draw( g );
			} // fim if ( visible )
		}


	}

}
