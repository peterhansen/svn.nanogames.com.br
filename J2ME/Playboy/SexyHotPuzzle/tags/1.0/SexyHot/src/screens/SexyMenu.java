/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import java.util.Hashtable;

/**
 *
 * @author Caio
 */
public final class SexyMenu  extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if TOUCH == "true"
	, PointerListener
//#endif
{
	private static final byte SELECTED_NONE = 0;
	private static final byte SELECTED_ARROW_LEFT = 1;
	private static final byte SELECTED_ARROW_RIGHT = 2;
	private static final byte SELECTED_TEXT = 3;

	private static Pattern boardCenter;
	private static MarqueeLabel label;
	private static Sprite girl;

	private final DrawableImage lock;
	private final DrawableImage lockBackground;
	private final DrawableImage sexyHotLogo;
	private final DrawableImage boardLeft;
	private final DrawableImage boardRight;
	private final Sprite arrowRight;
	private final Sprite arrowLeft;
	//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 		private final DrawableImage lockR;
	//#endif
	
	private static int[] entries;
		
	private static int currentIndex;/** índice atualmente selecionado no menu */
	private static boolean circular;/** indica se o menu é circular */

	private GameMIDlet midlet;

	private int screenID;
	private int selectionID;
	private static Hashtable hashText = new Hashtable();
	
	public SexyMenu(GameMIDlet midlet, int screen, int[] entries) throws Exception {
		super( 11 );

		screenID = screen;
		this.midlet = midlet;

		hashText.clear();
		for( int i = 0; i < entries.length; i++ ) {
			hashText.put( new Integer( entries[i] ), new Integer( i ) );
			//#if DEBUG == "true"
				System.out.println( "hashText.put( " + entries[i] + ", " + i + " )" );
			//#endif
		}

		lockBackground = new DrawableImage( PATH_IMAGES + "menu_lock_bkg.png" );
		lockBackground.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( lockBackground );

		girl = new Sprite( PATH_IMAGES + "menu_girls" );		
		girl.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable(girl);

		lock = new DrawableImage( PATH_IMAGES + "menu_lock.png" );
		insertDrawable( lock );
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			lock.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
//# 			lockR = new DrawableImage( lock );
//# 			lockR.mirror( TRANS_MIRROR_H );
//# 			lockR.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
//# 			insertDrawable( lockR );
		//#else
			lock.defineReferencePixel( ANCHOR_CENTER );
		//#endif

		boardCenter = new Pattern( new DrawableImage( PATH_IMAGES + "menu_board_center.png" ) );

		boardLeft = new DrawableImage( PATH_IMAGES + "menu_board_side.png" );
		boardLeft.defineReferencePixel( ANCHOR_LEFT | ANCHOR_VCENTER );

		boardRight = new DrawableImage( boardLeft );
		boardRight.mirror( TRANS_MIRROR_H );
		boardRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_VCENTER );

		insertDrawable( boardLeft );
		insertDrawable( boardCenter );
		insertDrawable( boardRight );
		
		sexyHotLogo = new DrawableImage( PATH_IMAGES + "menu_logo.png" );
		sexyHotLogo.defineReferencePixel( ANCHOR_HCENTER );
		insertDrawable( sexyHotLogo );

		arrowRight = new Sprite( PATH_IMAGES + "menu_arrow" );
		arrowRight.mirror( TRANS_MIRROR_H );
		arrowRight.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( arrowRight );

		arrowLeft = new Sprite( arrowRight );
		arrowLeft.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( arrowLeft );

		label = new MarqueeLabel( GameMIDlet.GetFont( FONT_MENU ), "" );
		insertDrawable( label );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		
		circular = false;
		currentIndex = entries[0];
		this.entries = entries;
		setCurrentIndex( 0 );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public static void setEntry( int textId ) {

		//#if DEBUG == "true"
			System.out.println( "setEntry(" + textId + ")" );
		//#endif

		final Integer i = ( Integer ) hashText.get( new Integer( textId ) );
		if ( i != null ) {
			//#if DEBUG == "true"
				System.out.println( "setCurrentIndex(" + i + ")" );
			//#endif

			setCurrentIndex( i.intValue() );
		}
	}
	

	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		sexyHotLogo.setRefPixelPosition( width >> 1 , ( sexyHotLogo.getHeight() >> 1 ) );
		
		boardLeft.setRefPixelPosition( 0 , height - ( boardLeft.getHeight() ) );
		boardRight.setRefPixelPosition( width , boardLeft.getRefPixelY() );
		boardCenter.setSize( width - ( boardLeft.getWidth() << 1 ), boardLeft.getHeight() );
		boardCenter.setPosition( ( boardLeft.getWidth() ), boardLeft.getPosY() );
		label.setSize( boardCenter.getWidth(), label.getHeight() );
		label.setPosition( boardCenter.getPosX(), boardCenter.getPosY() + ( ( boardCenter.getHeight() - label.getHeight() ) >> 1 )  );
		updateEntriesText();
		arrowRight.setRefPixelPosition( ( boardRight.getPosX() + ( boardRight.getWidth() >> 1 ) ), ( boardRight.getPosY() + ( boardRight.getHeight() >> 1 ) ) );
		arrowLeft.setRefPixelPosition( ( boardLeft.getPosX() + ( boardLeft.getWidth() >> 1 ) ), ( boardLeft.getPosY() + ( boardLeft.getHeight() >> 1 ) ) );

		final short distance = (short)(sexyHotLogo.getPosY() + sexyHotLogo.getHeight());
		lock.setRefPixelPosition(  width >> 1, distance + ( ( boardLeft.getPosY() - distance ) >> 1 ) );
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			lockR.setRefPixelPosition( lock.getRefPixelPosition() );
		//#endif

			
		lockBackground.setRefPixelPosition(  lock.getRefPixelX(), lock.getRefPixelY() );
		girl.setRefPixelPosition(  lock.getRefPixelX(), lock.getRefPixelY() );

		GameMIDlet.setBkgBlocker( new Rectangle( boardLeft.getPosX() + getPosX(),
				boardLeft.getPosY() + getPosY(), width, boardLeft.getHeight() ) );
	}


	public void keyPressed(int key) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				activateEntryAction();
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				arrowLeft.setFrame( 1 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				arrowRight.setFrame( 1 );
			break;
		}
	}


	public void keyReleased(int key) {
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				previousIndex();
				arrowLeft.setFrame( 0 );
			break;
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				nextIndex();
				arrowRight.setFrame( 0 );
			break;
		}
	}


	private void activateEntryAction() {
		midlet.onChoose( null, screenID, entries[currentIndex] );
	}
	
	
	private void nextIndex() {
		if ( currentIndex < ( entries.length - 1 ) )
			setCurrentIndex( currentIndex + 1 );
		else if ( circular )
			setCurrentIndex( 0 );
	}
	
	 
	private void previousIndex() {
		if ( currentIndex > 0 )
			setCurrentIndex( currentIndex - 1 );
		else if ( circular )
			setCurrentIndex( entries.length - 1 );
	}
	 
	
	private static void setCurrentIndex( int index ) {
		if ( girl != null ) {
			if ( index >= 0 && index < entries.length ) {
				currentIndex = index;
				final int i = NanoMath.randInt( MENU_GIRLS_TYPES );
				if(i!=girl.getCurrFrameIndex())
					girl.setFrame( i );
				else
					girl.setFrame( ( i + 1 ) % MENU_GIRLS_TYPES );
				updateEntriesText();
			}
		}
	}


	public static void unload() {
		GameMIDlet.log( "Before SexyMenu Unload" );
		label = null;
		girl = null;
		boardCenter = null;
		GameMIDlet.log( "After SexyMenu Unload" );
	}
	

	/**
	 * Atualiza o texto e o posicionamento do label de texto do menu.
	 */
	private static void updateEntriesText() {
		if( label != null && boardCenter != null && currentIndex < entries.length ) {
			final Point p = new Point( label.getSize() );
			label.setText( entries[currentIndex] );
			label.setSize( p );
			label.setTextOffset( ( p.x - label.getTextWidth() ) >> 1 );
//			label.setText( entries[currentIndex] );
//			label.setSize( NanoMath.min( boardCenter.getWidth(), label.getWidth() ), label.getHeight() );
//			label.setPosition( ( boardCenter.getPosX() + ( boardCenter.getWidth() >> 1 ) ) - ( label.getWidth() >> 1 ), ( boardCenter.getPosY() + ( boardCenter.getHeight() >> 1 ) ) - ( label.getHeight() >> 1 ) );
		}
	}


	public void hideNotify(boolean deviceEvent) {
	}


	public void showNotify(boolean deviceEvent) {
		if( !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_FIRST_MUSIC, MediaPlayer.LOOP_INFINITE );
	}


	public void sizeChanged(int width, int height) {
		setSize( width, height );
	}


//#if TOUCH == "true"
	public void onPointerDragged(int x, int y) {
		if( !boardRight.contains( x, y ) ) {
			arrowRight.setFrame( 0 );
		}
		if( !boardLeft.contains( x, y ) ) {
			arrowLeft.setFrame( 0 );
		}
	}


	public void onPointerPressed(int x, int y) {
		if( boardRight.contains( x, y ) ) {
			arrowRight.setFrame( 1 );
			selectionID = SELECTED_ARROW_RIGHT;
		} else {
			arrowRight.setFrame( 0 );

			if( boardLeft.contains( x, y ) ) {
				arrowLeft.setFrame( 1 );
				selectionID = SELECTED_ARROW_LEFT;
			} else {
				arrowLeft.setFrame( 0 );
				if( boardCenter.contains( x, y ) || lock.contains( x, y ) ) {
					selectionID = SELECTED_TEXT;
				}
			}
		}
	}


	public void onPointerReleased(int x, int y) {
		if( boardRight.contains( x, y ) ) {
			if(selectionID == SELECTED_ARROW_RIGHT) {
				nextIndex();
			}
			arrowRight.setFrame( 0 );
		}
		else if( boardLeft.contains( x, y ) ) {
			if(selectionID == SELECTED_ARROW_LEFT) {
				previousIndex();
			}
			arrowLeft.setFrame( 0 );
		}
		else if( boardCenter.contains( x, y ) || lock.contains( x, y ) ) {
			if(selectionID == SELECTED_TEXT) {
				activateEntryAction();
			}
		}
		selectionID = SELECTED_NONE;
	}
//#endif
}
