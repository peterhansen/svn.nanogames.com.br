/**
 * GameMIDlet.java
 */
package core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.MediaPlayer;
import java.util.Hashtable;
import br.com.nanogames.components.util.Serializable;
import com.qualcomm.qis.form.Connecting;
import com.qualcomm.qis.form.Purchase;
import com.qualcomm.qis.form.Warning;
import com.qualcomm.qis.helper.LicenseString;
import license.DecrementUseCallback;
import license.License;
import license.LicenseAgent;
import license.LicenseCallback;
import license.NullCallbackException;
import license.PurchaseResponse;

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif

/**
 * ©2007 Nano Games
 * @author Daniel L. Alves
 */

public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable,
		LicenseCallback, DecrementUseCallback
{
	/** Fonte padrão do jogo */
	private static final byte BACKGROUND_TYPE_DEFAULT = 0;

	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;

	private static final byte BACKGROUND_TYPE_NONE = 2;

	private static int softKeyRightText;

	/** gerenciador da animação da soft key esquerda */
	private static BasicAnimatedSoftkey softkeyLeft;

	/** gerenciador da animação da soft key direita */
	private static BasicAnimatedSoftkey softkeyRight;

	public static final byte DEFAULT_FONT_OFFSET = -1;

	//#if LOG == "true"
//# 	public static final byte SCREEN_LOG				= 21;
	//#endif

	public static StringBuffer log = new StringBuffer();
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu principal">

	public static final byte MAIN_MENU_NEW_GAME		= 0;
	public static final byte MAIN_MENU_OPTIONS		= 1;
	public static final byte MAIN_MENU_NANO_ONLINE	= 2;
	public static final byte MAIN_MENU_RECOMMEND	= 3;
	public static final byte MAIN_MENU_HELP			= 4;
	public static final byte MAIN_MENU_CREDITS		= 5;
	public static final byte MAIN_MENU_EXIT			= 6;
	public static final byte MAIN_MENU_LOG			= 7;

	//</editor-fold>

    public static final byte OPTION_ENGLISH= 1;
    public static final byte OPTION_PORTUGUESE= 2;


	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu de opções">

	//#ifdef NO_SOUND
//# 		public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 0;
//# 		public static final byte OPTIONS_MENU_ERASE_RECORDS		= 1;
//# 		public static final byte OPTIONS_MENU_BACK				= 2;
//#
//# 		public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 0;
//# 		public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;
	//#else
	public static final byte OPTIONS_MENU_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 1;
	public static final byte OPTIONS_MENU_BACK				= 2;

	public static final byte OPTIONS_MENU_NO_VIB_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;

	//#endif

	//</editor-fold>

	/** Indica se o device possui pouca memória */
	private boolean lowMemory;

	private License license;

	private final DrawableImage[] SIGN_ICONS = new DrawableImage[ 12 ];

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	private Form form;

	/** Barra de progresso (inferior). */
	private ProgressBar progressBar;

	private TitleBar titleBar;



	public GameMIDlet() throws Exception {
		titleBar = null;
		form = null;
		progressBar = null;
		license = null;
		softkeyLeft = null;
		softkeyRight = null;
		currentScreen = -1;
		instance = null;
		FONTS = new ImageFont[FONT_TYPES_TOTAL];
	}


	public static final void log(String s) {
		//#if DEBUG == "true"
			System.gc();

			log.append(s);
			log.append(": ");
			log.append(Runtime.getRuntime().freeMemory());

			log.append("\n");
		//#endif
	}


	public static final Drawable[] getSignIcons() {
		return ( ( GameMIDlet ) instance ).SIGN_ICONS;
	}


	protected final void loadResources() throws Exception
	{
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}


		for ( byte i = 0; i < 12; ++i ) {
			SIGN_ICONS[ i ] = new DrawableImage( PATH_SIGNS + i + ".png" );
		}

		// Carrega a fonte da aplicação
		final String PATH_FONTS = ROOT_DIR + "fonts/";
		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
			FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_FONTS + i );
			if ( i >= FONT_TEXT_WHITE )
				FONTS[ i ].setCharExtraOffset( 1 );
		}

		softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
		softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );

		manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
		manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );

		// Determina uma cor de fundo
		manager.setBackgroundColor( DEFAULT_LIGHT_COLOR );

		//#if LOG == "true"
//# 		Logger.setRMS( DATABASE_NAME, DATABASE_SLOT_LOG );
//# 		Logger.loadLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif

		try{
			AppMIDlet.loadData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
		} catch (Exception e) {
			language = NanoOnline.LANGUAGE_pt_BR;
		}
		language = NanoOnline.LANGUAGE_pt_BR; // TODO teste

		setLanguage( language );

		form = new Form();
		progressBar = new ProgressBar();
		form.setStatusBar( progressBar, false );

		//#if TOUCH == "true"
			form.setTouchKeyPad( new TouchKeyPad( getFont( FONT_BLACK ), new DrawableImage( PATH_NANO_ONLINE_IMAGES + "clear.png" ), new DrawableImage( PATH_NANO_ONLINE_IMAGES + "shift.png" ) ) );
		//#endif

		titleBar = new TitleBar();
		form.setTitleBar( titleBar );

		form.refreshLayout();
		manager.setCurrentScreen( form );

		progressBar.showMessage( ProgressBar.MSG_TYPE_INFO, LicenseString.LICENSEAGENT_CONNECTING );
		setScreen( SCREEN_CHOOSE_LANGUAGE );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Drawable getCaret() throws Exception {
		return new DrawableImage( PATH_NANO_ONLINE_IMAGES + "d.png" );
	}


	public static final Border getSimpleBorder( boolean white ) throws Exception {
		return new NanoOnlineSimpleBorder( white );
	}


	private static final class NanoOnlineSimpleBorder extends LineBorder {

		private static final int COLOR_PRESSED_BORDER	= 0x003f46;
		private static final int COLOR_PRESSED_FILL		= 0x038392;
		private static final int COLOR_FOCUSED_BORDER	= 0x034362;
		private static final int COLOR_FOCUSED_FILL		= 0x7dbfc7;
		private static final int COLOR_ERROR			= 0xff0000;
		private static final int COLOR_WARNING			= 0xaaaa00;
		private static final int COLOR_UNFOCUSED_BORDER	= 0x818181;
		private static final int COLOR_UNFOCUSED_FILL	= 0xbebebe;

		private final boolean white;


		public NanoOnlineSimpleBorder() throws Exception {
			this( false );
		}


		public NanoOnlineSimpleBorder( boolean white ) throws Exception {
			super( white ? 0xffffff : COLOR_UNFOCUSED_FILL, LineBorder.TYPE_ROUND_SIMPLE );

			this.white = white;
		}


		public final void setState( int state ) {
			super.setState( state );

			switch ( state ) {
				case STATE_PRESSED:
					setColor( COLOR_PRESSED_BORDER );
					setFillColor( COLOR_PRESSED_FILL );
				break;

				case STATE_ERROR:
					setColor( COLOR_ERROR );
				break;

				case STATE_WARNING:
					setColor( COLOR_WARNING );
				break;

				case STATE_FOCUSED:
					setColor( COLOR_FOCUSED_BORDER );
					setFillColor( COLOR_FOCUSED_FILL );
				break;

				case STATE_UNFOCUSED:
				default:
					setColor( COLOR_UNFOCUSED_BORDER );
					setFillColor( white ? 0xffffff : COLOR_UNFOCUSED_FILL );
				break;
			}
		}


		public final Border getCopy() throws Exception {
			return new NanoOnlineSimpleBorder( white );
		}

	}


	public static final Button getButton( int textIndex, int id, EventListener listener ) throws Exception {
		return getButton( getText( textIndex ), id, listener );
	}


	public static final Button getButton( String text, int id, EventListener listener ) throws Exception {
		final Button button = new Button( getFont( FONT_TEXT_WHITE ), text ) {
			protected final void setState( int state ) {
				super.setState( state );
				switch ( state ) {
					case STATE_PRESSED:
						setFont( getFont( FONT_BLACK ) );
					break;

					default:
						setFont( getFont( FONT_TEXT_WHITE ) );
				}
			}


			public final void setFocus( boolean focus ) {
				super.setFocus( focus );

				if ( focus ) {
					label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				} else {
					label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_NONE );
					label.setTextOffset( 0 );
				}
			}


//			public final void setSize( int width, int height ) {
//				if ( label != null ) {
//					super.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ),
//								   label.getHeight() + ( getBorder() == null ? 0 : getBorder().getBorderHeight() ) );
//				}
//			}

		};
		button.setId( id );

		if ( listener != null )
			button.addEventListener( listener );
		button.setBorder( getBorder() );
//		button.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ), button.getHeight() + button.getBorder().getBorderHeight() );
		button.setFocus( false );

		return button;
	}


	/**
	 *
	 * @param titleIndex
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final TitledBorder getTitledBorder( int titleIndex ) throws Exception {
		return getTitledBorder( getText( titleIndex ) );
	}


	public static final TitledBorder getTitledBorder( int titleIndex, Border b ) throws Exception {
		return getTitledBorder( getText( titleIndex ), b );
	}


	public static final TitledBorder getTitledBorder( String title ) throws Exception {
		return getTitledBorder( title, getBorder() );
	}


	public static final TitledBorder getTitledBorder( String title, Border b ) throws Exception {
		return new TitledBorder( new Label( getFont( FONT_BLACK ), title ), b );
	}


	protected void changeLanguage( byte language ) {
		try {
			loadTexts( TEXT_TOTAL, "/" + language + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
		}
	}


	public static final void setSoftKey( byte softKey, Drawable d, int visibleTime )
	{
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );
		midlet.manager.setSoftKey( softKey, d );
	}


	public synchronized final void onChoose( Menu menu, int id, int index )
	{
		switch( id )
		{
				case SCREEN_CHOOSE_LANGUAGE:
					try {
						switch (index) {

							case OPTION_ENGLISH:
								language = NanoOnline.LANGUAGE_en_US;
								loadTexts(TEXT_TOTAL, ROOT_DIR + "en.dat");
								setScreen(SCREEN_ENABLE_SOUNDS);
								break;
							case OPTION_PORTUGUESE:
								language = NanoOnline.LANGUAGE_pt_BR;
								loadTexts(TEXT_TOTAL, ROOT_DIR + "pt.dat");
								setScreen(SCREEN_ENABLE_SOUNDS);
								break;
						}
						AppMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif

						exit();
					}

                   break;

			//#if LOG == "true"
//# 			case SCREEN_LOG:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 				break;
			//#endif

			case SCREEN_SPLASH_BRAND:
				setScreen( SCREEN_SPLASH_GAME );
				break;

			case SCREEN_ENABLE_SOUNDS:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				MediaPlayer.saveOptions();
				setScreen( SCREEN_SPLASH_NANO );
                break;// fim switch ( id )
		}
	}

	public void onItemChanged( Menu menu, int id, int index )
	{
	}


	public static final boolean isLowMemory() {
		return (( GameMIDlet )getInstance() ).lowMemory;
	}


	public final void write(DataOutputStream output) throws Exception {
		output.writeByte(language);
		output.writeBoolean(MediaPlayer.isMuted());
	}

	public final void read(DataInputStream input) throws Exception {
		language = input.readByte();
		input.readBoolean();
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final ScrollBar getScrollBarV() throws Exception  {
		return new NanoOnlineScrollBar( ScrollBar.TYPE_VERTICAL );
	}


	/**
	 * Obtém a referência para a barra de progresso do Nano Online.
	 * @return referência para a barra de progresso do Nano Online.
	 */
	public static final ProgressBar getProgressBar() {
		return ( ( GameMIDlet ) instance ).progressBar;
	}


	public static final Form getForm() {
		return ( ( GameMIDlet ) instance ).form;
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 *
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 *
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
			softKeyRightText = textIndex;

		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( FONT_TEXT_WHITE, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do método setSoftKeyLabel( byte, int )


	protected final int changeScreen( int screen ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		byte bkgType = BACKGROUND_TYPE_NONE;

		if ( screen != currentScreen ) {
			Drawable nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;

			int softKeyVisibleTime = 0;

			try {
				switch (screen) {
					case SCREEN_SPLASH_NANO:
                        bkgType = BACKGROUND_TYPE_NONE;

						//#if SCREEN_SIZE == "SMALL"
//# 							nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
						//#elif SCREEN_SIZE != "SUPER_BIG"
							nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
						//#else
//#
//# 							nextScreen = new SplashNano();
						//#endif
						break;

					case SCREEN_SPLASH_GAME:
//					TODO	nextScreen = new SplashGame( getFont( FONT_MENU ) );
                        bkgType = BACKGROUND_TYPE_NONE;
						break;

					case SCREEN_MAIN_MENU:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new BasicMenu( midlet, screen, getFont( FONT_MENU ), new int[] {
									TEXT_OPTIONS,
									TEXT_HELP,
									TEXT_CREDITS,
									TEXT_EXIT,
								}, MENU_ITEMS_SPACING, MAIN_MENU_NEW_GAME, MAIN_MENU_EXIT );
						( ( BasicMenu ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						bkgType = BACKGROUND_TYPE_DEFAULT;
						break;

					case SCREEN_HELP:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						( ( BasicMenu ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_BIRTHDAY:
						indexSoftRight = TEXT_BACK;
						form.setContentPane( new ProfileScreen( true, 0 ) );
						nextScreen = form;
					break;

					case SCREEN_HOROSCOPE:
						indexSoftRight = TEXT_BACK;
						form.setContentPane( new HoroscopeScreen() );
						nextScreen = form;
					break;

					case SCREEN_CHOOSE_SIGN:
						indexSoftRight = TEXT_BACK;
						form.setContentPane( new SignScreen() );
						nextScreen = form;
					break;

					case SCREEN_OPTIONS:
						indexSoftRight = TEXT_EXIT;
						form.setContentPane( new OptionsScreen() );
						nextScreen = form;
					break;

					case SCREEN_PURCHASE_INFO:
						indexSoftRight = TEXT_EXIT;
						form.setContentPane( new PurchaseInfoScreen() );
						nextScreen = form;
					break;

					case SCREEN_CHOOSE_LANGUAGE:
						indexSoftRight = TEXT_EXIT;
						form.setContentPane( new LanguageScreen() );
						nextScreen = form;
					break;
				} // fim switch ( screen )

				setBackground( bkgType );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, então sai do jogo
				exit();
			}

			getProgressBar().setSoftKey( getText( indexSoftRight ) );

			if ( nextScreen != null ) {
				midlet.manager.setCurrentScreen( nextScreen );
				if ( screen == SCREEN_CHOOSE_SIGN )
					ScreenManager.setKeyListener( ( ( Form ) nextScreen ).getContentPane() );
				
				return screen;
			} else {
				return -1;
			}
		} // fim if ( screen != currentScreen )
		return screen;
	}


	public static final Drawable getCursor() throws Exception {
		return null; 
	}


	public static final ScrollBar getScrollBar() throws Exception {
		return new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT,
										COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Border getBorder() throws Exception {
		final LineBorder b = new LineBorder( 0xff7601, LineBorder.TYPE_ROUND_RAISED ) {
			public void setState( int state ) {
				super.setState( state );

				switch ( state ) {
					case STATE_PRESSED:
						setColor( 0xff8b00 );
						setFillColor( 0xff8b00 );
					break;

					case STATE_FOCUSED:
						setColor( 0xff7601 );
						setFillColor( 0xffab00 );
					break;

					default:
						setColor( 0xfd9600 );
						setFillColor( 0xff7601 );
					break;
				}
			}
		};

		return b;
	}


	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}

		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
		}
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );

		return url;
	}


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_DEFAULT:
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
			break;
		}
	} // fim do método setBackground( byte )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	} // fim do método setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}


	public void decrementUseCallback( License license ) {
		this.license = license;

//		TODO try {
//			appDevelopersStartApp();
//		} catch ( Throwable th ) {
//			// all exceptions from application code should be caught in the
//			// callback
//			th.printStackTrace();
//		}
	}


	private void decrementUseAsync( License validLicense ) {
		try {
			validLicense.decrementUseAsync( 1, ( DecrementUseCallback ) this, false );
		} catch ( NullCallbackException nullcallbackexception ) {
			// This will only happen if DecrementUseCallback is null.
			progressBar.showMessage( ProgressBar.MSG_TYPE_ERROR, LicenseString.LICENSEAGENT_INTERNAL_ERROR );
//			display.setCurrent( new Error( "Error Display", LicenseString.LICENSEAGENT_INTERNAL_ERROR, this ) );
		}
	}


	public final void licenseCallback( License license ) {
		this.license = license;
		progressBar.showInfo( LicenseString.LICENSEAGENT_CONNECTING );
		
		try {
			Thread.sleep( 1 );
			//#if CHECK_SIGNATURE == "true"
//# 				System.out.println( license.getExpirationDate() + " / "  +System.currentTimeMillis() + " : " + ( license.getExpirationDate() >= System.currentTimeMillis() ) );
//# 				if ( this.license.isValid() && license.getExpirationDate() >= System.currentTimeMillis() ) {
//# 					validLicense( this.license );
//# 				} else {
//# 					invalidLicense( this.license );
//# 				}
			//#else
				validLicense( license );
			//#endif
		} catch ( Throwable th ) {
			// all exceptions from application code should be caught in the
			// callback
			th.printStackTrace();
		}
	}


	public final void pauseApp() {
		//#if CHECK_SIGNATURE == "true"
//# 			LicenseAgent.pause();
		//#endif
	}


	public void destroy( boolean flag ) {
		if ( form != null ) {
			try {
				form.destroy();
			} catch ( Exception e ) {};
			form = null;
		}
		if ( progressBar != null ) {
			try {
				progressBar.destroy();
			} catch ( Exception e ) {};
			progressBar = null;
		}

		//#if CHECK_SIGNATURE == "true"
//# 			LicenseAgent.cancel();
		//#endif
		
		super.destroy();
	}


	public void start() {
		super.start();

//		TODO progressBar.showMessage( ProgressBar.MSG_TYPE_INFO, LicenseString.LICENSEAGENT_CONNECTING );
////		display.setCurrent( new Connecting( "Information", LicenseString.LICENSEAGENT_CONNECTING ) );
//		try {
//			LicenseAgent.getLicenseAsync( this, ( LicenseCallback ) this );
//		} catch ( NullCallbackException nullcallbackexception ) {
//			exit();
//		}
	}


	private final void validLicense( License validLicense ) {
		//#if CHECK_SIGNATURE == "true"
//# 			if ( validLicense.isUseBased() ) {
//# 				decrementUseAsync( validLicense );
//# 			} else {
//# 				if ( validLicense.getErrorCode() != License.NOERROR ) {
//# 					progressBar.showMessage( ProgressBar.MSG_TYPE_WARNING, validLicense.getErrorString() );
//# 	//				display.setCurrent( new Warning( "Warning", validLicense.getErrorString(), this ) );
//# 				/**
//# 				 * Following this call , the user will be presented with warning
//# 				 * message, which after dismissing will go ahead and launch the
//# 				 * appDevelopers code.This is to make sure that the user sees
//# 				 * the warning message.
//# 				 */
//# 				} else {
//# 					progressBar.showMessage( ProgressBar.MSG_TYPE_INFO, getText( TEXT_LICENSE_OK ) + formatTime( validLicense.getExpirationDate(), getText( TEXT_TIME_FORMAT ) ) );
//# 					setScreen( SCREEN_OPTIONS );
//# 				}
//# 			}
		//#else
			progressBar.showMessage( ProgressBar.MSG_TYPE_INFO, getText( TEXT_LICENSE_OK ) + formatTime( System.currentTimeMillis() + 1000000, getText( TEXT_TIME_FORMAT ) ) );
			setScreen( SCREEN_OPTIONS );
		//#endif
	}


    public final void purchase() {
        /**
         * The API purchaseNewLicense() will initiate a platform request, to
         * either launch a storefront or a web browser. Following this call
         * there are two possibilities , depending on the device. [1] Some
         * devices will launch the browser/storefront immediately, placing the
         * application in the background. [2] Other devices will do nothing,
         * until the application exits. This is the case on "lower-end" devices
         * that cannot multi-task.Based on the return of shouldAppClose() its
         * recommended for the application developers to close the application
         * if it returns true as shown below.
         */
        PurchaseResponse purchaseresponse = this.license.purchaseNewLicense();
        if (!purchaseresponse.isSuccess()) {
            String errorString = purchaseresponse.getErrorCode() + " : " + purchaseresponse.getErrorString();
            //display.setCurrent(new Error("Error Display", errorString, this));
			progressBar.showMessage( ProgressBar.MSG_TYPE_INFO, errorString );
        } else if (purchaseresponse.shouldAppClose()) {
            exit();
        } else {
			
		}
    }


    private String getPurchaseStringFromJAD(int state) {
        return LicenseString.getLicensePurchaseString(state, this);
    }


	private final void invalidLicense( License invalidLicense ) {
		if ( invalidLicense.getErrorCode() == License.NOERROR ) {
			PurchaseInfoScreen.setText( getPurchaseStringFromJAD( invalidLicense.getState() ), formatTime( invalidLicense.getExpirationDate(), getText( TEXT_TIME_FORMAT ) ) );
			setScreen( SCREEN_PURCHASE_INFO );
			//progressBar.showMessage( ProgressBar.MSG_TYPE_INFO, getPurchaseStringFromJAD( invalidLicense.getState() ) );
//			display.setCurrent( new Purchase( "Purchase information", getPurchaseStringFromJAD( invalidLicense.getState() ), this ) );
		} else {
			progressBar.showMessage( ProgressBar.MSG_TYPE_ERROR, invalidLicense.getErrorString() + ":" + invalidLicense.getErrorCode() );
//			display.setCurrent( new Error( "Error Display", invalidLicense.getErrorString() + ":" + invalidLicense.getErrorCode(), this ) );
		}
	}

}
