/**
 * ProfileScreen.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */
package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.RadioButton;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import license.LicenseAgent;
import license.LicenseCallback;
import license.NullCallbackException;


/**
 *
 * @author Peter
 */
public final class LanguageScreen extends SimpleContainer implements ConnectionListener, Constants {

	/***/
	private static final byte ENTRY_BUTTON_ENGLISH		= 0;
	private static final byte ENTRY_BUTTON_PORTUGUESE	= 1;
	private static final byte ENTRY_BUTTON_EXIT			= 2;

	/** Quantidade total de entradas da tela. */
	private static final byte ENTRIES_TOTAL						= ENTRY_BUTTON_PORTUGUESE + 3;

	private final Button buttonEnglish;
	/***/
	private final Button buttonPortuguese;
	

	/**
	 * 
	 * @param profileIndex
	 * @throws java.lang.Exception
	 */
	public LanguageScreen() throws Exception {
		super( ENTRIES_TOTAL );

		final FormText formText = new FormText( GameMIDlet.getFont( FONT_BLACK ), 0, 0 );
		formText.setText( "Escolha seu idioma\nElija su idioma\n\n" );
		formText.addEventListener( this );
		formText.setFocusable( true );
		insertDrawable( formText );

		buttonEnglish = GameMIDlet.getButton( "Español", ENTRY_BUTTON_ENGLISH, this );
		insertDrawable( buttonEnglish );

		buttonPortuguese = GameMIDlet.getButton( "Português", ENTRY_BUTTON_PORTUGUESE, this );
		insertDrawable( buttonPortuguese );

		final Button buttonExit = GameMIDlet.getButton( "Sair / Salir", ENTRY_BUTTON_EXIT, this );
		insertDrawable( buttonExit );

		GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_INFO, "Horoscope DEMO" );
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_ENGLISH:
					case ENTRY_BUTTON_PORTUGUESE:
						GameMIDlet.setLanguage( sourceId == ENTRY_BUTTON_PORTUGUESE ? NanoOnline.LANGUAGE_pt_BR : NanoOnline.LANGUAGE_en_US );

						try {
							//#if CHECK_SIGNATURE == "true"
//# 								LicenseAgent.getLicenseAsync((GameMIDlet) GameMIDlet.getInstance(), (LicenseCallback) GameMIDlet.getInstance());
							//#else
								( ( GameMIDlet ) GameMIDlet.getInstance() ).licenseCallback( null );
							//#endif
						} catch ( Exception ex) {
							GameMIDlet.exit();
						}
					break;

					case ENTRY_BUTTON_EXIT:
					case ProgressBar.ID_SOFT_RIGHT:
						GameMIDlet.exit();
					break;
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						GameMIDlet.exit();
						break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )

	public void processData(int id, byte[] data) {
	}

	public void onInfo(int id, int infoIndex, Object extraData) {
	}

	public void onError(int id, int errorIndex, Object extraData) {
	}


}
