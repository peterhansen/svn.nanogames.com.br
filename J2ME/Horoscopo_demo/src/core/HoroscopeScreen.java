/**
 * HoroscopeScreen.java
 *
 * Created on 5:15:17 PM.
 *
 */
package core;

import br.com.nanogames.components.online.TextScreen;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;


/**
 *
 * @author Peter
 */
public final class HoroscopeScreen extends SimpleContainer {

	private static String text = "";


	public HoroscopeScreen() throws Exception {
		super( 5 );

		final FormText formText = new FormText( GameMIDlet.getFont( FONT_BLACK ), 0, 0, GameMIDlet.getSignIcons() );
		formText.setText( text );
		formText.addEventListener( this );
		formText.setFocusable( true );
		insertDrawable( formText );

		setBackIndex( SCREEN_OPTIONS );
	}


	public static final void setText( String text ) {
		//final int dividerIndex = text.indexOf( '|' );

		//String signText = text.substring( 0, dividerIndex );

//		for ( byte i = 0; i < 12; ++i ) {
//			if ( signText.equalsIgnoreCase( GameMIDlet.getText( TEXT_SIGN_ARIES + i ) ) ) {
//				signText = "<ALN_H><IMG_" + i + ">\n<ALN_L>";
//				break;
//			}
//		}

//		HoroscopeScreen.text = signText + text.substring( dividerIndex + 1 );
		HoroscopeScreen.text = text;
	}

	public void eventPerformed(Event evt) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
						onBack();
					break;
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
						break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	}


}
