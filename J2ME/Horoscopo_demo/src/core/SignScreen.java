/**
 * SignScreen.java
 *
 * Created on 6:04:59 PM.
 *
 */
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.userInterface.form.layouts.GridLayout;
import br.com.nanogames.components.util.NanoMath;


/**
 *
 * @author Peter
 */
public final class SignScreen extends SimpleContainer {

    private final Drawable[] signs = new Drawable[ 12 ];
    private final Label[] labels = new Label[ 12 ];

	private final Pattern cursor;

	private byte currentIcon;

	private boolean connecting;

	
	public SignScreen() throws Exception {
		super( 30 );

		final FlowLayout l = new FlowLayout( FlowLayout.AXIS_VERTICAL, ANCHOR_LEFT | ANCHOR_VCENTER );
		setLayout( l );

		cursor = new Pattern( 0x000000 );
		insertDrawable( cursor );

		final Drawable[] signIcons = GameMIDlet.getSignIcons();
		for ( byte i = 0; i < 12; ++i ) {
			final DrawableImage icon = new DrawableImage( ( DrawableImage ) signIcons[ i ] );
            signs[ i ] = icon;
			icon.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
			insertDrawable( icon );

            final Label label = new Label( FONT_BLACK, TEXT_SIGN_ARIES + i );
            insertDrawable( label );
            labels[ i ] = label;
		}
		
		cursor.setSize( signs[ 0 ].getSize().add( 4, 4 ) );
		cursor.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		final DrawableComponent dc = new DrawableComponent( cursor );
		dc.addEventListener( this );
		dc.setVisible( false );
		insertDrawable( dc );

		setCurrentIcon( NanoMath.randInt( 12 ) );
	}

    public final void refreshLayout() {
		ScreenManager.setKeyListener( this );
        for ( int i = 0, y = 32; i < 12; ++i ) {
            signs[ i ].setPosition( ( ( ( i % 3 ) ) * getWidth() / 3 ) + ( getWidth() / 6 ) - ( signs[ i ].getWidth() >> 1 ), y );
            labels[ i ].setPosition( signs[ i ].getPosX() + ( ( signs[ i ].getWidth() - labels[ i ].getWidth() ) >> 1 ),
                                     signs[ i ].getPosY() + signs[ i ].getHeight() + 2 );

            if ( i % 3 == 2 )
                y += signs[ i ].getHeight() + labels[ i ].getHeight() + 5;
        }
		setCurrentIcon( currentIcon );
    }
	

    public void eventPerformed(Event evt) {
        final int type = evt.eventType;
        switch ( type ) {
            case Event.EVT_KEY_PRESSED:
				keyPressed( ( ( Integer ) evt.data ).intValue() );
            break;
        }
    }


	public final void processData( int id, byte[] data ) {
		if ( data != null ) {
			try {
				HoroscopeScreen.setText( new String( data ) );
				GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_INFO, TEXT_HOROSCOPE_DOWNLOADED );
				GameMIDlet.setScreen( SCREEN_HOROSCOPE );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif

				GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_ERROR, ex.getMessage() );
			} finally {
			}
		}
		GameMIDlet.getProgressBar().processData( id, data );
	}

	public void keyPressed(int key) {
		if ( connecting )
			return;
		
		switch ( key ) {
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				setInConnection( true );
				new Thread() {
					public final void run() {
						final byte[] textData = GameMIDlet.getText( TEXT_HOROSCOPE_PEIXES + currentIcon ).getBytes();
						try {
							GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONNECTION_OPENED, null );
							Thread.sleep( 200 );
							GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_TOTAL, new Integer( textData.length ) );
							Thread.sleep( 200 );
							GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_READ, new Integer( textData.length / 10 ) );
							Thread.sleep( 200 );
							GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_READ, new Integer( textData.length / 5 ) );
							Thread.sleep( 500 );
							GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_READ, new Integer( textData.length ) );
							Thread.sleep( 200 );
							GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONNECTION_ENDED, null );
						} catch ( Exception e ) {
						}
						processData( 0, textData );
					}
				}.start();
			break;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setCurrentIcon( currentIcon - 3 );
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setCurrentIcon( currentIcon + 3 );
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				setCurrentIcon( currentIcon - 1 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				setCurrentIcon( currentIcon + 1 );
			break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.setScreen( SCREEN_OPTIONS );
			break;
		}
	}

	
	private final void setCurrentIcon( int index ) {
		currentIcon = (byte) ( ( index + 12 ) % 12 );
		cursor.setRefPixelPosition( signs[ currentIcon ].getRefPixelPosition() );
		GameMIDlet.getProgressBar().showInfo( GameMIDlet.getText( ( TEXT_SIGN_ARIES + currentIcon ) ) );
	}


	/**
	 *
	 * @param inConnection
	 */
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		connecting = inConnection;

		if ( inConnection ) {
			GameMIDlet.getProgressBar().setSoftKey( GameMIDlet.getText( TEXT_CANCEL ) );
		} else {
			GameMIDlet.getProgressBar().setSoftKey( GameMIDlet.getText( TEXT_BACK ) );
		}
	}


}
