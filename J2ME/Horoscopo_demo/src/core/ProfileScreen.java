/**
 * ProfileScreen.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */
package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.RadioButton;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;


/**
 *
 * @author Peter
 */
public final class ProfileScreen extends SimpleContainer implements ConnectionListener, Constants {

	// <editor-fold defaultstate="collapsed" desc="CÓDIGOS DE RETORNO DO REGISTRO DO USUÁRIO">

	// ATENÇÃO: ESSES CÓDIGOS DEVEM *SEMPRE* ESTAR DE ACORDO COM OS CÓDIGOS RETORNADOS PELO SERVIDOR!

	// obs.: codigo de retorno zero é OK (RC_OK)
	/** Código de retorno do registro do usuário: erro: apelido já em uso. */
	private static final byte RC_ERROR_NICKNAME_IN_USE = 1;
	/** Código de retorno do registro do usuário: erro: formato do apelido inválido. */
	private static final byte RC_ERROR_NICKNAME_FORMAT = 2;
	/** Código de retorno do registro do usuário: erro: comprimento do apelido inválido. */
	private static final byte RC_ERROR_NICKNAME_LENGTH = 3;
	/** Código de retorno do registro do usuário: erro: formato do e-mail inválido. */
	private static final byte RC_ERROR_EMAIL_FORMAT = 4;
	/** Código de retorno do registro do usuário: erro: comprimento do e-mail inválido. */
	private static final byte RC_ERROR_EMAIL_LENGTH = 5;
	/** Código de retorno do registro do usuário: erro: password muito fraca. */
	private static final byte RC_ERROR_PASSWORD_WEAK = 6;
	/** Código de retorno do registro do usuário: erro: password e confirmação diferentes. */
	private static final byte RC_ERROR_PASSWORD_CONFIRMATION = 7;
	/** Código de retorno do registro do usuário: erro: comprimento do 1º nome inválido. */
	private static final byte RC_ERROR_FIRST_NAME_LENGTH = 8;
	/** Código de retorno do registro do usuário: erro: comprimento do sobrenome inválido. */
	private static final byte RC_ERROR_LAST_NAME_LENGTH = 9;
	/** Código de retorno do registro do usuário: erro: gênero inválido. */
	private static final byte RC_ERROR_GENRE = 10;
	/** Código de retorno do registro do usuário: erro: comprimento do gênero inválido. */
	private static final byte RC_ERROR_GENRE_LENGTH = 11;
	/** Código de retorno do registro do usuário: erro: formato do número de telefone inválido. */
	private static final byte RC_ERROR_PHONE_NUMBER_FORMAT = 12;
	/** Código de retorno do registro do usuário: erro: comprimento do número de telefone inválido. */
	private static final byte RC_ERROR_PHONE_NUMBER_LENGTH = 13;
	/** Código de retorno do registro do usuário: erro: CPF inválido. */
	private static final byte RC_ERROR_CPF_FORMAT = 14;
	/**
	 * Código de retorno do registro do usuário: erro: e-mail já em uso.
	 * @since Nano Online 0.0.31
	 */
	private static final byte RC_ERROR_EMAIL_ALREADY_IN_USE = 15;
	// </editor-fold>

	// índices das entradas de dados (não necessariamente correspondem aos ids dos parâmetros de conexão
	private static final byte ENTRY_BIRTHDAY_FIELD_1 = 0;
	private static final byte ENTRY_BIRTHDAY_FIELD_2 = ENTRY_BIRTHDAY_FIELD_1 + 1;
	private static final byte ENTRY_BIRTHDAY_FIELD_3 = ENTRY_BIRTHDAY_FIELD_2 + 1;
	private final byte ENTRY_BIRTHDAY_DAY;
	private final byte ENTRY_BIRTHDAY_MONTH;
	private final byte ENTRY_BIRTHDAY_YEAR;

	/***/
	private static final byte ENTRY_BUTTON_OK = 0;
	private static final byte ENTRY_BUTTON_CANCEL				= ENTRY_BUTTON_OK + 1;
	private static final byte ENTRY_BUTTON_BACK					= ENTRY_BUTTON_CANCEL + 1;

	/** Quantidade total de entradas da tela. */
	private static final byte ENTRIES_TOTAL						= ENTRY_BUTTON_BACK + 3;

	/***/
	private static final byte EDIT_LABELS_TOTAL = ENTRY_BIRTHDAY_FIELD_3 + 1;

	/***/
	private final TextBox[] textBoxes = new TextBox[ EDIT_LABELS_TOTAL ];
	/***/
	private final Button buttonEditOK;
	/***/
	private final Button buttonCancel;
	/***/
	private int currentConnection = -1;
	

	/**
	 * 
	 * @param profileIndex
	 * @throws java.lang.Exception
	 */
	public ProfileScreen( boolean editable, int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );

		final FormText text = new FormText( GameMIDlet.getFont( FONT_BLACK ), 0, 0 );
		text.setText( GameMIDlet.getText( TEXT_TYPE_YOUR_BIRTHDAY ) );
		text.addEventListener( this );
		text.setFocusable( true );
		insertDrawable( text );

		final String timeFormat = GameMIDlet.getText( TEXT_TIME_FORMAT ).toLowerCase();
		final int indexDay = timeFormat.indexOf( "d" );
		final int indexMonth = timeFormat.indexOf( "m" );
		final int indexYear = timeFormat.indexOf( "y" );

		if ( indexDay < indexMonth ) {
			// DMY, DYM, YDM
			if ( indexMonth < indexYear ) {
				// DMY
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_3;
			} else if ( indexDay < indexYear ) {
				// DYM
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_2;
			} else {
				// YDM
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_1;
			}
		} else {
			// MDY, MYD, YMD
			if ( indexMonth < indexYear ) {
				// MDY
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_3;
			} else if ( indexDay < indexYear ) {
				// MYD
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_2;
			} else {
				// YMD
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_1;
			}
		}

		final byte[] length = {
			( byte ) ( ENTRY_BIRTHDAY_YEAR == ENTRY_BIRTHDAY_FIELD_1 ? 4 : 2 ),
			( byte ) ( ENTRY_BIRTHDAY_YEAR == ENTRY_BIRTHDAY_FIELD_2 ? 4 : 2 ),
			( byte ) ( ENTRY_BIRTHDAY_YEAR == ENTRY_BIRTHDAY_FIELD_3 ? 4 : 2 ),
		};

		// adiciona os campos para entrada do aniversário
		final Container containerBirthday = new Container( 5 ) {

			public Point calcPreferredSize( Point maximumSize ) {
				return new Point( 131, 34 ); // TODO corrigir erro de cálculo automático de tamanho
			}

		};
		final FlowLayout birthdayLayout = new FlowLayout( FlowLayout.AXIS_HORIZONTAL, ANCHOR_BOTTOM );
		birthdayLayout.start.set( 0, LAYOUT_GAP_Y );
		birthdayLayout.gap.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );
		containerBirthday.setLayout( birthdayLayout );
		containerBirthday.setBorder( GameMIDlet.getTitledBorder( TEXT_BIRTHDAY, null ) );

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			TextBox textBox = null;

			if ( i == ENTRY_BIRTHDAY_FIELD_1 ) {
				insertDrawable( containerBirthday );
			}

			// caixas de texto da data de nascimento
			textBox = new TextBox( GameMIDlet.getFont( FONT_BLACK ), null, length[i], TextBox.INPUT_MODE_NUMBERS, true );
			textBoxes[i] = textBox;
			textBox.setCaret( GameMIDlet.getCaret() );
			textBox.setBorder( GameMIDlet.getSimpleBorder( false ) );
			containerBirthday.insertDrawable( textBox );

			if ( i < ENTRY_BIRTHDAY_FIELD_3 ) {
				containerBirthday.insertDrawable( new FormLabel( GameMIDlet.getFont( FONT_BLACK ), "/" ) );
			}

			textBox.addEventListener( this );
			textBox.setPasswordMask( PASSWORD_MASK_DEFAULT );
		}

		buttonEditOK = GameMIDlet.getButton( TEXT_OK, ENTRY_BUTTON_OK, this );
		insertDrawable( buttonEditOK );

		buttonCancel = GameMIDlet.getButton( TEXT_BACK, ENTRY_BUTTON_BACK, this );
		insertDrawable( buttonCancel );

		setBackIndex( SCREEN_OPTIONS );

		if ( editable ) {
			setEditable();
		}
	}


	public final void processData( int id, byte[] data ) {
		onConnectionEnded( false );

		//#if DEBUG == "true"
			System.out.println( "processData: " + ( data == null ? -1 : data.length ) + "\n" + new String( data ) );
		//#endif


		if ( data != null ) {
			try {
				HoroscopeScreen.setText( new String( data ) );
				GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_INFO, TEXT_HOROSCOPE_DOWNLOADED );
				GameMIDlet.setScreen( SCREEN_HOROSCOPE );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif

				GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_ERROR, ex.getMessage() );
			} finally {
			}
		}
		GameMIDlet.getProgressBar().processData( id, data );
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		if ( id == currentConnection ) {
			GameMIDlet.getProgressBar().onInfo( id, infoIndex, extraData );
			switch ( infoIndex ) {
				case ConnectionListener.INFO_CONNECTION_ENDED:
					onConnectionEnded( false );
					break;
			}
		}
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		if ( id == currentConnection ) {
			GameMIDlet.getProgressBar().onError( id, errorIndex, extraData );
			onConnectionEnded( false );
		}
	}


	/**
	 * Valida os campos de texto antes de enviar ao servidor.
	 * 
	 * @return <code>true</code>, caso estejam todos preenchidos da maneira correta, e <code>false</code> caso contrário.
	 */
	private final boolean validate() {
		byte firstError = -1;
		byte errorMessage = -1;

		// remove caracteres vazios no começo e no final de cada textbox
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			String text = textBoxes[i].getText().trim();
			// remove espaços duplos nos textos
			int doubleSpaceIndex = text.indexOf( "  " );
			while ( doubleSpaceIndex >= 0 ) {
				// não é necessário verificar o limite da string em doubleSpaceIndex + 1 porque é garantido que isso
				// seja válido (afinal, é um espaço duplo)
				text = text.substring( 0, doubleSpaceIndex ) + text.substring( doubleSpaceIndex + 1 );
				doubleSpaceIndex = text.indexOf( "  " );
			}
			textBoxes[i].setText( text, false );
		}

		// verifica se a data de nascimento é válida
		if ( textBoxes[ENTRY_BIRTHDAY_DAY].getText().length() > 0 &&
				textBoxes[ENTRY_BIRTHDAY_MONTH].getText().length() > 0 &&
				textBoxes[ENTRY_BIRTHDAY_YEAR].getText().length() > 0 ) {
			int year = Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_YEAR].getText() );
			if ( textBoxes[ENTRY_BIRTHDAY_YEAR].getText().length() < 4 ) {
				// trata casos onde o jogador não insere todos os dígitos do ano
				final Date date = new Date( System.currentTimeMillis() );
				final Calendar calendar = Calendar.getInstance();
				calendar.setTime( date );

				if ( year >= calendar.get( Calendar.YEAR ) - 2000 ) {
					year = 1900 + year;
				} else {
					// considera que é mais provável jogador ter menos de 10 anos de idade do que mais de 100
					year = 2000 + year;
				}
				textBoxes[ENTRY_BIRTHDAY_YEAR].setText( String.valueOf( year ), false );
			}

			switch ( AppMIDlet.isValidBirthday( Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_DAY].getText() ),
					Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() ),
					year ) ) {
				case 1:
					if ( firstError < 0 ) {
						firstError = ENTRY_BIRTHDAY_DAY;
						errorMessage = TEXT_REGISTER_ERROR_INVALID_DAY;
					}
					textBoxes[ENTRY_BIRTHDAY_DAY].getBorder().setState( Border.STATE_ERROR );
					break;

				case 2:
					if ( firstError < 0 ) {
						firstError = ENTRY_BIRTHDAY_MONTH;
						errorMessage = TEXT_REGISTER_ERROR_INVALID_MONTH;
					}
					textBoxes[ENTRY_BIRTHDAY_MONTH].getBorder().setState( Border.STATE_ERROR );
					break;
			}
		} else {
			if ( firstError < 0 ) {
				if ( textBoxes[ENTRY_BIRTHDAY_DAY].getText().length() <= 0 ) {
					firstError = ENTRY_BIRTHDAY_DAY;
					errorMessage = TEXT_REGISTER_ERROR_INVALID_DAY;
					textBoxes[ENTRY_BIRTHDAY_DAY].getBorder().setState( Border.STATE_ERROR );
				} else if ( textBoxes[ENTRY_BIRTHDAY_MONTH].getText().length() <= 0 ) {
					firstError = ENTRY_BIRTHDAY_MONTH;
					errorMessage = TEXT_REGISTER_ERROR_INVALID_MONTH;
					textBoxes[ENTRY_BIRTHDAY_MONTH].getBorder().setState( Border.STATE_ERROR );
				} else {
					firstError = ENTRY_BIRTHDAY_YEAR;
					errorMessage = TEXT_REGISTER_ERROR_INVALID_YEAR;
					textBoxes[ENTRY_BIRTHDAY_YEAR].getBorder().setState( Border.STATE_ERROR );
				}
			}
		}

		if ( firstError >= 0 ) {
			textBoxes[firstError].getBorder().setState( Border.STATE_ERROR );
			GameMIDlet.getProgressBar().showError( errorMessage );
			GameMIDlet.getForm().requestFocus( textBoxes[firstError] );
		}

		// se o índice ainda for negativo, é porque não houve erro na validação
		return firstError < 0;
	}


	/**
	 * 
	 */
	private final void onConnectionEnded( boolean forceCancel ) {
		if ( forceCancel ) {
			NanoConnection.cancel( currentConnection );
		}
		currentConnection = -1;

		setInConnection( false );
	}


	public final void setFocus( boolean focus ) {
		super.setFocus( focus );

		if ( currentConnection < 0 && focus ) {
			onConnectionEnded( false );
		}
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
						onBack();
					break;

					case ENTRY_BUTTON_OK:
						if ( validate() ) {
							try {
								//#if HTTP == "true"
//# 									final StringBuffer buffer = new StringBuffer();
//#
//# 									buffer.append( "language=" );
//# 									buffer.append( "pt" );
//# 									buffer.append( "&myDate=" );
//# 									buffer.append( textBoxes[ENTRY_BIRTHDAY_DAY].getText() );
//# 									buffer.append( "/" );
//# 									buffer.append( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() );
//# 									buffer.append( "/" );
//# 									buffer.append( "2010" );
//# 									buffer.append( "&day=" );
//# 									buffer.append( textBoxes[ENTRY_BIRTHDAY_DAY].getText() );
//# 									buffer.append( "&month=" );
//# 									buffer.append( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() );
//# 									buffer.append( "&year=" );
//# 									buffer.append( textBoxes[ENTRY_BIRTHDAY_YEAR].getText() );
//#
//# 									currentConnection = NanoConnection.post( URL_HOROSCOPE, buffer.toString().getBytes(), this, true );
								//#else
									new Thread() {
										public final void run() {
											final byte[] textData = GameMIDlet.getText( TEXT_HOROSCOPE_PEIXES + ( ( 8 + Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() ) ) % 12 ) ).getBytes();
											try {
												GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONNECTION_OPENED, null );
												Thread.sleep( 200 );
												GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_TOTAL, new Integer( textData.length ) );
												Thread.sleep( 200 );
												GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_READ, new Integer( textData.length / 10 ) );
												Thread.sleep( 200 );
												GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_READ, new Integer( textData.length / 5 ) );
												Thread.sleep( 500 );
												GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONTENT_LENGTH_READ, new Integer( textData.length ) );
												Thread.sleep( 200 );
												GameMIDlet.getProgressBar().onInfo( 0, ProgressBar.INFO_CONNECTION_ENDED, null );
											} catch ( Exception e ) {
											}
											processData( 0, textData );
										}
									}.start();
								//#endif
								setInConnection( true );
							} catch ( Exception ex ) {
								//#if DEBUG == "true"
									ex.printStackTrace();
								//#endif
							}
						}
						break;

					case ENTRY_BUTTON_CANCEL:
					case ENTRY_BUTTON_BACK:
						onBack();
					break;
				}
			break;

			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
						break;
				} // fim switch ( key )
			break;

			case Event.EVT_TEXTBOX_LEFT_ON_START:
				evt.consume();
				evt.source.keyReleased( 0 );
				evt.source.keyPressed( ScreenManager.UP );
			break;

			case Event.EVT_TEXTBOX_RIGHT_ON_END:
				evt.consume();
				evt.source.keyReleased( 0 );
				evt.source.keyPressed( ScreenManager.DOWN );
			break;

			case Event.EVT_TEXTBOX_FILLED:
				evt.consume();
				evt.source.keyReleased( 0 );
				getComponentForm().keyPressed( ScreenManager.DOWN );
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	protected final void onBack() {
		if ( currentConnection >= 0 ) {
			onConnectionEnded( true );
		} else {
			super.onBack();
		}
	}


	/**
	 * 
	 * @param inConnection
	 */
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[i].setEnabled( !inConnection );
		}
		buttonEditOK.setEnabled( !inConnection );
		buttonCancel.setEnabled( !inConnection );

		if ( inConnection ) {
			GameMIDlet.getProgressBar().setSoftKey( GameMIDlet.getText( TEXT_CANCEL ) );
		} else {
			GameMIDlet.getProgressBar().setSoftKey( GameMIDlet.getText( TEXT_BACK ) );
		}
	}


	/**
	 * 
	 */
	private final void setEditable() {
		buttonEditOK.setText( GameMIDlet.getText( TEXT_OK ), false );
		buttonEditOK.setId( ENTRY_BUTTON_OK );

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[i].setEnabled( true );
		}

		buttonCancel.setText( GameMIDlet.getText( TEXT_BACK ), false );
		buttonCancel.setId( ENTRY_BUTTON_BACK );
	}

}
