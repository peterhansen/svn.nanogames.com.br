/**
 * ProfileScreen.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */
package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.RadioButton;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import license.LicenseAgent;
import license.LicenseCallback;
import license.NullCallbackException;


/**
 *
 * @author Peter
 */
public final class OptionsScreen extends SimpleContainer implements ConnectionListener, Constants {

	/***/
	private static final byte ENTRY_BUTTON_BIRTHDAY		= 0;
	private static final byte ENTRY_BUTTON_CHOOSE_SIGN	= 1;
	private static final byte ENTRY_BUTTON_EXIT			= 2;

	/** Quantidade total de entradas da tela. */
	private static final byte ENTRIES_TOTAL						= ENTRY_BUTTON_EXIT + 3;

	private final Button buttonEnglish;
	/***/
	private final Button buttonPortuguese;
	

	/**
	 * 
	 * @param profileIndex
	 * @throws java.lang.Exception
	 */
	public OptionsScreen() throws Exception {
		super( ENTRIES_TOTAL );

		final FormText formText = new FormText( GameMIDlet.getFont( FONT_BLACK ), 0, 0 );
		formText.setText( GameMIDlet.getText( TEXT_CHOOSE_AN_OPTION ) );
		formText.addEventListener( this );
		formText.setFocusable( true );
		insertDrawable( formText );

		buttonEnglish = GameMIDlet.getButton( TEXT_BIRTHDAY_TITLE, ENTRY_BUTTON_BIRTHDAY, this );
		insertDrawable( buttonEnglish );

		buttonPortuguese = GameMIDlet.getButton( TEXT_CHOOSE_SIGN, ENTRY_BUTTON_CHOOSE_SIGN, this );
		insertDrawable( buttonPortuguese );

		final Button buttonExit = GameMIDlet.getButton( TEXT_EXIT, ENTRY_BUTTON_EXIT, this );
		insertDrawable( buttonExit );
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_BIRTHDAY:
						GameMIDlet.setScreen( SCREEN_BIRTHDAY );
					break;

					case ENTRY_BUTTON_CHOOSE_SIGN:
						GameMIDlet.setScreen( SCREEN_CHOOSE_SIGN );
					break;

					case ENTRY_BUTTON_EXIT:
					case ProgressBar.ID_SOFT_RIGHT:
						GameMIDlet.exit();
					break;
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						GameMIDlet.exit();
						break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )

	public void processData(int id, byte[] data) {
	}

	public void onInfo(int id, int infoIndex, Object extraData) {
	}

	public void onError(int id, int errorIndex, Object extraData) {
	}


}
