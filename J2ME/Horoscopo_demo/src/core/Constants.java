/**
 * Constants.java
 */

package core;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;

/**
 * ©2007 Nano Games
 * @author Daniel L. Alves
 */
public interface Constants {
	/***/
	public static final byte RANKING_TYPE_SCORE = 0;

	public static final int COLOR_BACKGROUND = 0xffffff;
	public static final int COLOR_BACKGROUND_LIGHT = 0xffffff;

	public static final String URL_HOROSCOPE = "http://horplaza.wiz.com.br/zodiac/daily.txt";

	/***/
	public static final String HIGH_SCORE_DEFAULT_NICKNAME = "----";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";


	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	public static final String PATH_SIGNS = "/signs/";

	/** Caminho das imagens do Nano Online no .jar */
	public static final String PATH_NANO_ONLINE_IMAGES = "/online/";

	/** Caminho das imagens das bordas. */
	public static final String PATH_BORDER = PATH_IMAGES + "border/";

	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	public static final String PATH_MENU = PATH_IMAGES + "menu/";

	public static final byte FONT_BLACK			= 2;
	public static final byte FONT_MENU			= 0;
	public static final byte FONT_TEXT_WHITE	= 1;
	public static final byte FONT_SMALL			= 0;
	public static final byte FONT_WHITE			= 0;
	public static final byte FONT_DEFAULT		= 0;
	
	public static final byte FONT_TYPES_TOTAL = 3;

	/** Tempo que as soft keys ficam visíveis na tela */
	public static final short SOFT_KEY_VISIBLE_TIME = 1000;

	/** Tempo que o background leva para mudar completamente de cor nas telas de carregamento */
	public static final short LOAD_SCREEN_BKG_COLOR_CHANGE_TIME = 200;
			
	/** Quantidade mínima de memória para que o jogo rode com sua capacidade máxima */
	//#if SCREEN_SIZE == "SMALL"
//# 		public static final int LOW_MEMORY_LIMIT = 900000;
	//#elif SCREEN_SIZE == "BIG"
		public static final int LOW_MEMORY_LIMIT = 1600000;
    //#else
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif
	
	/** Tempo de duração do splash do jogo */
	public static final short SPLASH_GAME_DUR = 3000;
	
	/** Nome da base de dados */
	public static final String DATABASE_NAME = "n";
	
	/** Índice do slot de opções na base de dados */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	public static final byte DATABASE_SLOT_LANGUAGE = 2;

	public static final byte DATABASE_TOTAL_SLOTS = 2;
		
	/** Strings compartilhadas pelo jogo inteiro */
	public static final String SPRITE_DESC_EXT = ".bin";
	public static final String FONT_DESC_EXT = ".dat";
	public static final String IMG_EXT = ".png";
	public static final String ROOT_DIR = "/";
	public static final String PATH_BKG = ROOT_DIR + "Bkg/";
			
	/** Caminho das imagens da tela de splash */
	public static final String PATH_SPLASH = "/splash/";
	
	
	//<editor-fold defaultstate="collapsed" desc="TEXTOS DO JOGO">

	public static final byte TEXT_BACK								= 0;
	public static final byte TEXT_EXIT								= 1;
	public static final byte TEXT_OPTIONS							= 2;
	public static final byte TEXT_CREDITS							= 3;
	public static final byte TEXT_CREDITS_TEXT						= 4;
	public static final byte TEXT_HELP								= 5;
	public static final byte TEXT_TURN_SOUND_ON						= TEXT_HELP + 1;
	public static final byte TEXT_TURN_SOUND_OFF					= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON					= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF				= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_SPLASH_NANO						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_LOADING							= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_YES								= TEXT_LOADING + 1;
	public static final byte TEXT_NO								= TEXT_YES + 1;
    public static final byte TEXT_CONFIRM							= TEXT_NO + 1;
    public static final byte TEXT_OK								= TEXT_CONFIRM + 1;
    public static final byte TEXT_CLEAR								= TEXT_OK + 1;
    public static final byte TEXT_CHOOSE_AN_OPTION					= TEXT_CLEAR + 1;
    public static final byte TEXT_CHOOSE_SIGN						= TEXT_CHOOSE_AN_OPTION + 1;
    public static final byte TEXT_BIRTHDAY							= TEXT_CHOOSE_SIGN + 1;
    public static final byte TEXT_LICENSE_OK						= TEXT_BIRTHDAY + 1;
	public static final byte TEXT_TYPE_YOUR_BIRTHDAY				= TEXT_LICENSE_OK + 1;
	public static final byte TEXT_BIRTHDAY_TITLE					= TEXT_TYPE_YOUR_BIRTHDAY + 1;
	public static final byte TEXT_PURCHASE_INFO_1					= TEXT_BIRTHDAY_TITLE + 1;
	public static final byte TEXT_PURCHASE_INFO_2					= TEXT_PURCHASE_INFO_1 + 1;
	public static final byte TEXT_ERROR								= TEXT_PURCHASE_INFO_2 + 1;
	public static final byte TEXT_REGISTER_ERROR_INVALID_DAY		= TEXT_ERROR + 1;
	public static final byte TEXT_REGISTER_ERROR_INVALID_MONTH		= TEXT_REGISTER_ERROR_INVALID_DAY + 1;
	public static final byte TEXT_REGISTER_ERROR_INVALID_YEAR		= TEXT_REGISTER_ERROR_INVALID_MONTH + 1;
	public static final byte TEXT_CANCEL							= TEXT_REGISTER_ERROR_INVALID_YEAR + 1;
	public static final byte TEXT_HOROSCOPE_DOWNLOADED				= TEXT_CANCEL + 1;
	public static final byte TEXT_CONNECTION_OPENED					= TEXT_HOROSCOPE_DOWNLOADED + 1;
	public static final byte TEXT_RESPONSE_CODE						= TEXT_CONNECTION_OPENED + 1;
	public static final byte TEXT_SENDING_DATA						= TEXT_RESPONSE_CODE + 1;
	public static final byte TEXT_RECEIVING_DATA					= TEXT_SENDING_DATA + 1;
	public static final byte TEXT_TIME_FORMAT						= TEXT_RECEIVING_DATA + 1;
	public static final byte TEXT_SIGN_ARIES						= TEXT_TIME_FORMAT + 1;
	public static final byte TEXT_SIGN_TOURO						= TEXT_SIGN_ARIES + 1;
	public static final byte TEXT_SIGN_GEMEOS						= TEXT_SIGN_TOURO + 1;
	public static final byte TEXT_SIGN_CANCER						= TEXT_SIGN_GEMEOS + 1;
	public static final byte TEXT_SIGN_LEAO							= TEXT_SIGN_CANCER + 1;
	public static final byte TEXT_SIGN_VIRGEM						= TEXT_SIGN_LEAO + 1;
	public static final byte TEXT_SIGN_LIBRA						= TEXT_SIGN_VIRGEM + 1;
	public static final byte TEXT_SIGN_ESCORPIAO					= TEXT_SIGN_LIBRA + 1;
	public static final byte TEXT_SIGN_SAGITARIO					= TEXT_SIGN_ESCORPIAO + 1;
	public static final byte TEXT_SIGN_CAPRICORNIO					= TEXT_SIGN_SAGITARIO + 1;
	public static final byte TEXT_SIGN_AQUARIO						= TEXT_SIGN_CAPRICORNIO + 1;
	public static final byte TEXT_SIGN_PEIXES						= TEXT_SIGN_AQUARIO + 1;
	public static final byte TEXT_HOROSCOPE_PEIXES					= TEXT_SIGN_PEIXES + 1;

	//#if LOG == "true"
//# 	public static final byte TEXT_LOG					= 64;
	//#endif
		
	/** Número total de textos do jogo */
	public static final byte TEXT_TOTAL					        = TEXT_HOROSCOPE_PEIXES + 14;

	//</editor-fold>

    /*Índices das telas do jogo"*/

	public static final byte SCREEN_LOAD_RESOURCES				= 0;
	public static final byte SCREEN_ENABLE_SOUNDS				= 1;
	public static final byte SCREEN_CHANGE_BKG_COLOR			= 2;
	public static final byte SCREEN_SPLASH_NANO					= 3;
	public static final byte SCREEN_SPLASH_BRAND				= 4;
	public static final byte SCREEN_SPLASH_GAME					= 5;
	public static final byte SCREEN_MAIN_MENU					= 6;
	public static final byte SCREEN_LOAD_GAME					= 7;
	public static final byte PURCHASE_INFO						= 8;
	public static final byte SCREEN_LICENSE_ERROR				= 9;
	public static final byte SCREEN_HELP						= 10;
	public static final byte SCREEN_HELP_INTRO					= 11;
	public static final byte SCREEN_HELP_RULES					= 12;
	public static final byte SCREEN_HELP_SPECIAL_ICONS			= 13;
	public static final byte SCREEN_HELP_CONTROLS				= 14;
	public static final byte SCREEN_RECORDS						= 15;
    public static final byte SCREEN_CHOOSE_LANGUAGE			    = 27;
	public static final byte SCREEN_NANO_RANKING_MENU			= 28;
	public static final byte SCREEN_CHOOSE_SIGN					= 29;
	public static final byte SCREEN_HOROSCOPE					= 30;
	public static final byte SCREEN_BIRTHDAY					= 31;
	public static final byte SCREEN_OPTIONS						= 32;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_PURCHASE_INFO				= SCREEN_LOADING_PROFILES_SCREEN + 1;

	// cores da barra de scroll
	public static final int COLOR_FULL_LEFT_OUT	= 0x000b0d;
	public static final int COLOR_FULL_FILL		= 0x32abca;//fundo barra
	public static final int COLOR_FULL_RIGHT	= 0x032026;
	public static final int COLOR_FULL_LEFT		= 0x245865;
	public static final int COLOR_PAGE_OUT		= 0x002b30;
	public static final int COLOR_PAGE_FILL		= 0x0F6981;//barra
	public static final int COLOR_PAGE_LEFT_1	= 0x32abca;
	public static final int COLOR_PAGE_LEFT_2	= 0x63a3ad;//sombra barra scroll

	public static final int DEFAULT_SCROLL_LIGHT_COLOR = COLOR_FULL_FILL;
	public static final int DEFAULT_LIGHT_COLOR = COLOR_FULL_FILL;
	public static final int DEFAULT_DARK_COLOR = COLOR_PAGE_FILL;	

	/** Espessura da borda do tabuleiro. */
	
 	public static final byte BORDER_THICKNESS = 5;

	/** Espessura da borda do tabuleiro. */
	public static final int BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;
	
	public static final byte MENU_ITEMS_SPACING = 10;

	public static final int TIME_SOFT_KEY_VISIBLE = 4000;

	/** Espaçamento horizontal entre os componentes. */
	public static final byte LAYOUT_GAP_X = 3;
	/** Espaçamento vertical entre os componentes. */
	public static final byte LAYOUT_GAP_Y = 4;
	/** Posição horizontal de início dos componentes. */
	public static final byte LAYOUT_START_X = 3;
	/** Posição vertical de início dos componentes. */
	public static final byte LAYOUT_START_Y = 13;

	/** Largura mínima dos botões do Nano Online. */
	public static final byte BUTTON_MIN_WIDTH = 112 - LAYOUT_GAP_X;

	public static final char PASSWORD_MASK_DEFAULT = '*';
}