/**
 * ProfileScreen.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */
package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.RadioButton;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;


/**
 *
 * @author Peter
 */
public final class PurchaseInfoScreen extends SimpleContainer implements ConnectionListener, Constants {

	/***/
	private static final byte ENTRY_BUTTON_BUY = 0;
	private static final byte ENTRY_BUTTON_EXIT					= ENTRY_BUTTON_BUY + 1;

	/** Quantidade total de entradas da tela. */
	private static final byte ENTRIES_TOTAL						= ENTRY_BUTTON_EXIT + 3;

	private final Button buttonEditOK;
	/***/
	private final Button buttonCancel;
	/***/
	private int currentConnection = -1;

	private static String text = "";

	private static String expirationDate;
	

	/**
	 * 
	 * @param profileIndex
	 * @throws java.lang.Exception
	 */
	public PurchaseInfoScreen() throws Exception {
		super( ENTRIES_TOTAL );

		final FormText formText = new FormText( GameMIDlet.getFont( FONT_BLACK ), 0, 0 );
		formText.setText( GameMIDlet.getText( TEXT_PURCHASE_INFO_1 ) + expirationDate + GameMIDlet.getText( TEXT_PURCHASE_INFO_2 ) );
		formText.addEventListener( this );
		formText.setFocusable( true );
		insertDrawable( formText );

		buttonEditOK = GameMIDlet.getButton( TEXT_OK, ENTRY_BUTTON_BUY, this );
		insertDrawable( buttonEditOK );

		buttonCancel = GameMIDlet.getButton( TEXT_EXIT, ENTRY_BUTTON_EXIT, this );
		insertDrawable( buttonCancel );

		GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_INFO, text );
	}


	public static final void setText( String text, String date ) {
		PurchaseInfoScreen.text = text;
		expirationDate = date;
	}


	public final void processData( int id, byte[] data ) {
		onConnectionEnded( false );

		//#if DEBUG == "true"
			System.out.println( "processData: " + ( data == null ? -1 : data.length ) + "\n" + new String( data ) );
		//#endif


		if ( data != null ) {
			try {
				HoroscopeScreen.setText( new String( data ) );
				GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_INFO, TEXT_HOROSCOPE_DOWNLOADED );
				GameMIDlet.setScreen( SCREEN_HOROSCOPE );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif

				GameMIDlet.getProgressBar().showMessage( ProgressBar.MSG_TYPE_ERROR, ex.getMessage() );
			} finally {
			}
		}
		GameMIDlet.getProgressBar().processData( id, data );
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		if ( id == currentConnection ) {
			GameMIDlet.getProgressBar().onInfo( id, infoIndex, extraData );
			switch ( infoIndex ) {
				case ConnectionListener.INFO_CONNECTION_ENDED:
					onConnectionEnded( false );
					break;
			}
		}
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		if ( id == currentConnection ) {
			GameMIDlet.getProgressBar().onError( id, errorIndex, extraData );
			onConnectionEnded( false );
		}
	}


	/**
	 * 
	 */
	private final void onConnectionEnded( boolean forceCancel ) {
		if ( forceCancel ) {
			NanoConnection.cancel( currentConnection );
		}
		currentConnection = -1;

		setInConnection( false );
	}


	public final void setFocus( boolean focus ) {
		super.setFocus( focus );

		if ( currentConnection < 0 && focus ) {
			onConnectionEnded( false );
		}
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_BUY:
						( ( GameMIDlet ) GameMIDlet.getInstance() ).purchase();
					break;

					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_BUTTON_EXIT:
						GameMIDlet.exit();
					break;
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						GameMIDlet.exit();
						break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	protected final void onBack() {
		if ( currentConnection >= 0 ) {
			onConnectionEnded( true );
		} else {
			super.onBack();
		}
	}


	/**
	 * 
	 * @param inConnection
	 */
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		buttonEditOK.setEnabled( !inConnection );
		buttonCancel.setEnabled( !inConnection );

		if ( inConnection ) {
			GameMIDlet.getProgressBar().setSoftKey( GameMIDlet.getText( TEXT_CANCEL ) );
		} else {
			GameMIDlet.getProgressBar().setSoftKey( GameMIDlet.getText( TEXT_BACK ) );
		}
	}

}
