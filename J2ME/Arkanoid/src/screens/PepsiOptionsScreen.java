/**
 * OptionsScreen.java
 * 
 * Created on Mar 3, 2009, 10:02:11 AM
 *
 */

//#if JAR != "min"

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import core.Tag;

/**
 *
 * @author Peter
 */
public class PepsiOptionsScreen extends BasicOptionsScreen implements Constants {


	private PepsiOptionsScreen( MenuListener listener, int id, Drawable[] entries, int spacing, int backIndex, int soundIndex, int vibrationIndex ) throws Exception {
		super( listener, id, entries, spacing, backIndex, soundIndex, TEXT_TURN_SOUND_OFF, TEXT_TURN_SOUND_ON, vibrationIndex, TEXT_TURN_VIBRATION_OFF, TEXT_TURN_VIBRATION_ON, SOUND_INDEX_LEVEL_COMPLETE, 1 );
	}
	

	public static final PepsiOptionsScreen createInstance( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, int soundIndex, int vibrationIndex ) throws Exception {
		final Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			menuEntries[ i ] = new Tag( i % Tag.COLORS_TOTAL, MENU_TAG_WIDTH, FONT_TEXT_WHITE, entries[ i ] );
		}

		return new PepsiOptionsScreen( listener, id, menuEntries, MENU_ENTRY_Y_OFFSET, backIndex < 0 ? entries.length - 1 : backIndex, soundIndex, vibrationIndex );
	}
	

	protected void updateText( byte index ) {
		final Tag tag = ( Tag ) getDrawable( index );

		if ( index == INDEX_SOUND ) {
			tag.setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
		} else if ( index == INDEX_VIBRATION ) {
			tag.setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
		}
	}


}

//#endif