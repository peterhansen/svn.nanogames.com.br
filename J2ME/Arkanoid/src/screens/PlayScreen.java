/**
 * PlayScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 2, 2008 6:50:28 PM.
 */
package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Border;
import core.Constants;
import core.Tag;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.Serializable;
import core.Ball;
import core.Block;
import core.Element;
import core.HelpBar;
import core.HelpBar;
import core.PlayerBar;
import core.ArkanoidListener;
import core.PowerUp;
import core.Shield;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Random;
import java.lang.Math;
import javax.microedition.lcdui.Graphics;
import javax.microedition.sensor.LimitCondition;
import br.com.nanogames.components.userInterface.PointerListener;
//#if SCREEN_SIZE == "MEDIUM" || WEB_EMULATOR == "true"
//#
//#endif

//#if DEBUG == "true"

//#endif
/**
 *
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener, Serializable, ArkanoidListener
        //#if SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"
        , PointerListener
        //#endif
{


    /*barra do jogador*/
    final PlayerBar playerBar;

    /*barra de ajuda*/
    final HelpBar helpBar;

    /* estado atual do jogo*/
    protected byte stateGame = JOGANDO;
    /* fase atual do jogo*/
    protected int stageGame = FASE_1;

    /* array de blocos inseridos na fase*/
    private final Block[][] blocks;
    final Ball bola;

    /* icone possibilita ganhar mais uma vida*/
    final PowerUp extraLifeUpIcon;

    /* icine q possibilita ganhar um escudo temporario*/
    final PowerUp shieldIcon;

    /*grupo onde todos os elementos que perticipam do jogo s�o inseridos*/
    final UpdatableGroup telaDeJogo;
    final UpdatableGroup telaAuxiliar;
    //final Shield shield;
    private final short LARGURA_BLOCO;
    private final short ALTURA_BLOCO;
    private final short NUM_MAX_LINHAS;

    /*largura da tela de acordo com o numero de blocos*/
    public int LARGURA_LIMITE_TELA;

    /* altuta limite que os blocos ocupam*/
    public int ALTURA_LIMITE_BLOCOS;

    /* altura limite da tela*/
    public int ALTURA_LIMITE_TELA;
    public int ALTURA_LIMITE_TELA_AUXILIAR;
    public int LARGURA_LIMITE_TELA_AUXILIAR;

    /* labels de indica��o de pontos , vidas e etc*/
    final Label ponto;
    final Label over;
    final Label valPont;
    final Label pause;
    final Label vidas;
    final Label contVidas;
    final Label textStage;
    final Label stage;
    final Label extraLife;
    final Label extraShield;
    /* variavel que indica o tempo que um componente pode piscar na tela*/
    protected short timeblink;

    /* indica o tempo de mudan�a do estado ESPERANDO para o estado JOGANDO*/
    protected short timeState;

    /* tempo em milisegundos */
    final int secondsBlink = 200;
    final int secondsState = 3000;
    final ImageFont font;

    /* variavel que acumula os pontos*/
    private int ACUM_PONTO;

    /* contador de vidas*/
    private int lives = 3;

    /* plano de fundo*/
    final Pattern bgkTelaDeJogo;
    final Pattern bgkTelaDeAuxiliar;
    final Pattern bgk;
    private byte pointerStatus;
    private long lastPointerPressedTime;
    /** �ltima posi��o do ponteiro. */
    private final Point lastPointerPos = new Point();
    private static final byte POINTER_STATUS_RELEASED = 0;
    private static final byte POINTER_STATUS_PRESSED = 1;
    private static final byte POINTER_STATUS_DRAGGED = 2;

    public PlayScreen() throws Exception {
        super(NUM_ELEMENTS + (NUM_BLOCKS_LINHA * NUM_BLOCKS_COLUNA) + 30);
        font = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat");
        bgk = new Pattern(new DrawableImage(PATH_IMAGES + "bkg.png"));
        bgkTelaDeJogo = new Pattern(new DrawableImage(PATH_IMAGES + "bkg_0.png"));
        bgk.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
        insertDrawable(bgk);

        final Block block = new Block();
        LARGURA_LIMITE_TELA = NUM_BLOCKS_LINHA * block.getWidth();
        ALTURA_LIMITE_BLOCOS = NUM_BLOCKS_COLUNA * block.getHeight();
        ALTURA_LIMITE_TELA = ALTURA_LIMITE_BLOCOS * 3;


        telaDeJogo = new UpdatableGroup(NUM_ELEMENTS + (NUM_BLOCKS_LINHA * NUM_BLOCKS_COLUNA) + 30);
        insertDrawable(telaDeJogo);

        telaAuxiliar = new UpdatableGroup(NUM_ELEMENTS);


        //#if SCREEN_SIZE == "BIG"
        telaDeJogo.setSize(LARGURA_LIMITE_TELA, 230);
        Element.setLimits(LARGURA_LIMITE_TELA, 230);
        //#elif SCREEN_SIZE =="MEDIUM"
//#         telaDeJogo.setSize(LARGURA_LIMITE_TELA, 170);
//#         Element.setLimits(LARGURA_LIMITE_TELA, 170);
        //#elif SCREEN_SIZE =="SMALL"
//#         telaDeJogo.setSize(LARGURA_LIMITE_TELA, 102);
//#         Element.setLimits(LARGURA_LIMITE_TELA, 102);
//#
        //#endif

        bgkTelaDeJogo.setSize(telaDeJogo.getWidth(), telaDeJogo.getHeight());
        telaDeJogo.insertDrawable(bgkTelaDeJogo);

        bola = new Ball(this);
        bola.setPosition((telaDeJogo.getWidth() - bola.getWidth()) / 2, ALTURA_LIMITE_BLOCOS + bola.getHeight());
        telaDeJogo.insertDrawable(bola);

        LARGURA_BLOCO = (short) block.getWidth();
        ALTURA_BLOCO = (short) block.getHeight();
        blocks = new Block[(ALTURA_LIMITE_BLOCOS / ALTURA_BLOCO)][(LARGURA_LIMITE_TELA / LARGURA_BLOCO)];


        /*......DEFININDO NUMERO MAXIMO DE LINHAS Q O BLOCO PODE OCUPAR.......*/
        NUM_MAX_LINHAS = (short) ((ALTURA_LIMITE_BLOCOS / ALTURA_BLOCO) / 2);

        playerBar = new PlayerBar();
        helpBar = new HelpBar();
        extraLifeUpIcon = new PowerUp(this);
        // shield = new Shield();
        telaDeJogo.insertDrawable(playerBar);
        telaDeJogo.insertDrawable(helpBar);
        shieldIcon = new PowerUp(this);
        helpBar.setState(MORTO);


        textStage = new Label(font, TEXT_LEVEL);
        textStage.setPosition(0, 0);


        stage = new Label(font, String.valueOf(getCurrentStage()));
        stage.setPosition(textStage.getPosX() + textStage.getWidth(), textStage.getPosY());


        over = new Label(font, TEXT_GAME_OVER);
        telaDeJogo.insertDrawable(over);

        vidas = new Label(font, TEXT_LIFE);
        vidas.setPosition(textStage.getPosX(), textStage.getPosY() + textStage.getHeight());


        contVidas = new Label(font, String.valueOf(lives));
        contVidas.setPosition(vidas.getPosX() + vidas.getWidth(), vidas.getPosY());


        ponto = new Label(font, TEXT_SCORE_GAME);
        ponto.setPosition(vidas.getPosX(), vidas.getPosY() + vidas.getHeight());


        valPont = new Label(font, String.valueOf(ACUM_PONTO));
        valPont.setPosition(ponto.getPosX() + ponto.getWidth(), ponto.getPosY());


        pause = new Label(font, TEXT_PAUSE);
        pause.setPosition(LARGURA_LIMITE_TELA / 2 - bola.getWidth(), telaDeJogo.getHeight() / 2);
        telaDeJogo.insertDrawable(pause);


        extraLife = new Label(font, TEXT_EXTRA_LIFE);
        telaDeJogo.insertDrawable(extraLife);

        extraShield = new Label(font, TEXT_EXTRA_SHIELD);
        telaDeJogo.insertDrawable(extraShield);


        ALTURA_LIMITE_TELA_AUXILIAR = textStage.getHeight() + vidas.getHeight() + ponto.getHeight();
        LARGURA_LIMITE_TELA_AUXILIAR = ponto.getWidth() + 6 * valPont.getWidth();

        bgkTelaDeAuxiliar = new Pattern(0x000000);



        //#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM"
        telaAuxiliar.setSize(LARGURA_LIMITE_TELA_AUXILIAR, ALTURA_LIMITE_TELA_AUXILIAR);
        bgkTelaDeAuxiliar.setSize(telaDeJogo.getWidth(), ALTURA_LIMITE_TELA_AUXILIAR);

        //#elif SCREEN_SIZE =="SMALL"
//#         if (ScreenManager.SCREEN_WIDTH == ScreenManager.SCREEN_HEIGHT) {
//#             telaAuxiliar.setSize(ScreenManager.SCREEN_WIDTH, ALTURA_LIMITE_TELA_AUXILIAR);
//#             bgkTelaDeAuxiliar.setSize(ScreenManager.SCREEN_WIDTH, ALTURA_LIMITE_TELA_AUXILIAR);
//#         } else {
//#             telaAuxiliar.setSize(LARGURA_LIMITE_TELA_AUXILIAR, ALTURA_LIMITE_TELA_AUXILIAR);
//#             bgkTelaDeAuxiliar.setSize(telaDeJogo.getWidth(), ALTURA_LIMITE_TELA_AUXILIAR);
//#         }
        //#endif





        insertDrawable(telaAuxiliar);




        telaAuxiliar.insertDrawable(bgkTelaDeAuxiliar);
        telaAuxiliar.insertDrawable(textStage);
        telaAuxiliar.insertDrawable(stage);
        telaAuxiliar.insertDrawable(vidas);
        telaAuxiliar.insertDrawable(contVidas);
        telaAuxiliar.insertDrawable(ponto);
        telaAuxiliar.insertDrawable(valPont);

        telaDeJogo.insertDrawable(extraLifeUpIcon);
        extraLifeUpIcon.setState(MORTO);
        telaDeJogo.insertDrawable(shieldIcon);
        shieldIcon.setState(MORTO);

        // insertDrawable(shield);
        //  shield.setPosition(00, ScreenManager.SCREEN_HEIGHT - shield.getHeight());
        //  shield.setState(MORTO); 

        for (int linha = 0; linha < NUM_BLOCKS_COLUNA; linha++) {
            for (int coluna = 0; coluna < NUM_BLOCKS_LINHA; coluna++) {
                Block blocks = new Block();
                blocks.setPosition(getPosCell(linha, coluna));
                telaDeJogo.insertDrawable(blocks);
                this.blocks[linha][coluna] = blocks;
            }
        }

        setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
        setStage(FASE_1);
        setCurrentState(ESPERANDO);


    }

    public void read(DataInputStream input) throws Exception {
        for (int linha = 0; linha < NUM_BLOCKS_COLUNA; linha++) {
            for (int coluna = 0; coluna < NUM_BLOCKS_LINHA; coluna++) {
                final byte stateBlock = (byte) (input.readByte() - (byte) '0');
                switch (stateBlock) {
                    case 0:
                        blocks[linha][coluna].setState(MORTO);
                        break;
                    case 1:
                        blocks[linha][coluna].setState(STATE_3);
                        break;
                    case 2:
                        blocks[linha][coluna].setState(STATE_2);
                        break;
                    case 3:
                        blocks[linha][coluna].setState(STATE_1);
                        break;
                    case 4:
                        blocks[linha][coluna].setState(STATE_EXTRA_LIFE);
                        break;
                    case 5:
                        blocks[linha][coluna].setState(STATE_EXTRA_BALL);
                        break;
                    case 6:
                        blocks[linha][coluna].setState(STATE_EXTRA_SHIELD);
                        break;
                }
            }
        }
    }

    public boolean passou() {
        for (int linha = 0; linha < NUM_BLOCKS_COLUNA; linha++) {
            for (int coluna = 0; coluna < NUM_BLOCKS_LINHA; coluna++) {
                if (blocks[linha][coluna].getState() != MORTO) {
                    return false;
                }
            }
        }

        return true;
    }

    public void setStage(int stage) throws Exception {
        stageGame = stage;
        extraLifeUpIcon.setState(MORTO);
        shieldIcon.setState(MORTO);
        for (int linha = 0; linha < NUM_BLOCKS_COLUNA; linha++) {
            for (int coluna = 0; coluna < NUM_BLOCKS_LINHA; coluna++) {
                blocks[linha][coluna].setState(STATE_3);
                blocks[linha][coluna].setVisible(true);
            }
        }
        switch (stage) {

            case FASE_1:
                GameMIDlet.openJarFile(PATH_LEVEL_1, this);
                helpBar.setState(MORTO);
                break;
            case FASE_2:
                GameMIDlet.openJarFile(PATH_LEVEL_2, this);
                ACUM_PONTO += 25;
                valPont.setText(String.valueOf(ACUM_PONTO));
                helpBar.setState(MORTO);
                contVidas.setText(String.valueOf(lives));
                break;
            case FASE_3:
                GameMIDlet.openJarFile(PATH_LEVEL_3, this);
                ACUM_PONTO += 50;
                valPont.setText(String.valueOf(ACUM_PONTO));
                helpBar.setState(MORTO);
                contVidas.setText(String.valueOf(lives));
                break;
            case FASE_4:
                GameMIDlet.openJarFile(PATH_LEVEL_4, this);
                ACUM_PONTO += 75;
                valPont.setText(String.valueOf(ACUM_PONTO));
                bola.speedX.setSpeed(0);
                lives += 1;
                //helpBar.setState(MORTO);
                break;
            case FASE_5:
                GameMIDlet.openJarFile(PATH_LEVEL_5, this);
                ACUM_PONTO += 100;
                valPont.setText(String.valueOf(ACUM_PONTO));
                lives += 1;
                helpBar.setState(MORTO);
                contVidas.setText(String.valueOf(lives));
                break;
            case FASE_6:
                GameMIDlet.openJarFile(PATH_LEVEL_6, this);
                ACUM_PONTO += 125;
                valPont.setText(String.valueOf(ACUM_PONTO));
                helpBar.setVisible(true);
                helpBar.setState(STATE_3);
                helpBar.setPosition(LARGURA_LIMITE_TELA, ALTURA_BLOCO + helpBar.getHeight());
                lives += 1;
                contVidas.setText(String.valueOf(lives));
                break;
            case FASE_7:
                GameMIDlet.openJarFile(PATH_LEVEL_7, this);
                ACUM_PONTO += 150;
                valPont.setText(String.valueOf(ACUM_PONTO));
                helpBar.setState(MORTO);
                lives += 1;
                contVidas.setText(String.valueOf(lives));
                break;
        }
        setCurrentState(ESPERANDO);
        bola.speedX.setSpeed(0);
    }

    public int getScore() {
        return ACUM_PONTO;
    }

    public final Point getPosCell(int y, int x) {
        return new Point(x * ALTURA_BLOCO, y * LARGURA_BLOCO);
    }

    public final Point getCell(Point pos) {
        return getCell(pos.x,
                pos.y);
    }

    public final Point getCell(int x, int y) {
        return new Point(y / ALTURA_BLOCO, x / LARGURA_BLOCO);
    }

    public final Point getCell(Element e) {
        return getCell(e.getPosition());
    }

    public int getCurrentStage() {
        return stageGame;
    }

    public int getCurrentState() {
        return stateGame;
    }

    public void manageLives(boolean cont) {
        if (cont) {
            if (bola.getState() == MORTO) {
                lives -= 1;
                ACUM_PONTO -= 15;
                if (getScore() < 0) {
                    ACUM_PONTO = 0;
                }
                setCurrentState(ESPERANDO);
            }
            valPont.setText(String.valueOf(ACUM_PONTO));
            contVidas.setText(String.valueOf(lives));
        }


    }

    public void manageStages() {
    }

    public void testCollision() throws Exception {
        int posBaixEsq = (bola.getPosY() + bola.getHeight()) / ALTURA_BLOCO;
        int posCimaEsq = bola.getPosY() / ALTURA_BLOCO;
        //  int posBaixoEsq = (bola.getPosX() + bola.getWidth()) / LARGURA_BLOCO;
        // int posBaixoDir = ((bola.getPosX() + bola.getWidth()) + bola.getHeight()) / LARGURA_BLOCO;

        if (getCell(bola).x < ALTURA_LIMITE_BLOCOS / ALTURA_BLOCO &&
                getCell(bola).y < LARGURA_LIMITE_TELA / LARGURA_BLOCO) {
            if (blocks[posCimaEsq][getCell(bola).y].getState() == STATE_EXTRA_LIFE) {
                extraLifeUpIcon.setState(STATE_3);
                extraLifeUpIcon.setPosition(blocks[posCimaEsq][getCell(bola).y].getPosX(), blocks[posCimaEsq][getCell(bola).y].getPosY());
            }

            if (blocks[posCimaEsq][getCell(bola).y].getState() == STATE_EXTRA_SHIELD) {
                shieldIcon.setState(STATE_3);
                shieldIcon.setPosition(blocks[posCimaEsq][getCell(bola).y].getPosX(), blocks[posCimaEsq][getCell(bola).y].getPosY());
            }

            if (blocks[posCimaEsq][getCell(bola).y].getState() != MORTO) {

                blocks[(getCell(bola).x)][getCell(bola).y].treatCollision();
                bola.treatCollision(blocks[(getCell(bola).x)][getCell(bola).y]);

            }
        }
    }

    public void capturaPonto(boolean ponto) {
        if (ponto) {
            ACUM_PONTO += 10;
            valPont.setText(String.valueOf(ACUM_PONTO));
        }

    }

    public void manageSpecialElements(int delta) {
        if (extraLifeUpIcon.getState() == STATE_3) {
            extraLifeUpIcon.testCollision(playerBar);
            extraLife.setPosition(extraLifeUpIcon.getPosX(),
                    extraLifeUpIcon.getPosY() - extraLifeUpIcon.getHeight());
            timeblink +=
                    delta;
            if (timeblink >= secondsBlink) {
                timeblink = 0;
                extraLife.setVisible(!extraLife.isVisible());
            }
            if (extraLifeUpIcon.testCollision(playerBar) == true) {
                lives += 1;
                valPont.setText(String.valueOf(ACUM_PONTO));
                contVidas.setText(String.valueOf(lives));

            }

        }
        if (extraLifeUpIcon.getState() == MORTO) {
            extraLife.setVisible(false);
        }

        if (shieldIcon.getState() == STATE_3) {
            shieldIcon.testCollision(playerBar);
            extraShield.setPosition(shieldIcon.getPosX() - extraShield.getWidth() / 2,
                    shieldIcon.getPosY() - extraShield.getHeight());
            timeblink +=
                    delta;
            if (timeblink >= secondsBlink) {
                timeblink = 0;
                extraShield.setVisible(!extraShield.isVisible());
            }
            if (shieldIcon.testCollision(playerBar) == true) {
                //    shield.setState(STATE_3);
            }

        }
        if (shieldIcon.getState() == MORTO) {
            extraShield.setVisible(false);
        }
    // if (shield.getState() != MORTO) {
    // bola.testCollision(shield);
    // }
    //if (shield.testCollision(bola) == true) {
    // }

    }

    public void update(int delta) {

        switch (stateGame) {
            case ESPERANDO:
                playerBar.speedX.setSpeed(0);
                timeblink +=
                        delta;
                if (timeblink >= secondsBlink) {
                    timeblink = 0;
                    bola.setPosition((telaDeJogo.getWidth() - bola.getWidth()) / 2, ALTURA_LIMITE_BLOCOS + bola.getHeight());
                    bola.setVisible(!bola.isVisible());
                    playerBar.setPosition((telaDeJogo.getWidth() / 2 - playerBar.getWidth() / 2), (telaDeJogo.getPosY() + telaDeJogo.getHeight()) - ALTURA_LIMITE_TELA / 10);

                }

                timeState += delta;
                if (timeState >= secondsState) {
                    setCurrentState(JOGANDO);
                    timeState = 0;

                }

                break;
            case JOGANDO:
                super.update(delta);

                bola.testCollision(playerBar);
                manageSpecialElements(delta);
                if (lives == 0) {
                    setCurrentState(GAME_OVER);
                }

                try {
                    testCollision();
                    if (passou()) {
                        setCurrentState(ESPERANDO);
                        bola.speedX.setSpeed(0);
                        setStage(getCurrentStage() + 1);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                /*TESTAR  COLIS�O COM A BARRA EXTRA*/
                if (helpBar.getState() != MORTO) {
                    bola.testCollision(helpBar);
                }

                break;
            case PAUSAD0:
                timeblink += delta;
                if (timeblink >= secondsBlink) {
                    timeblink = 0;
                    bola.setVisible(!bola.isVisible());
                    // pause.setVisible(!pause.isVisible());
                    pause.setVisible(true);
                }

                playerBar.speedX.setSpeed(0);
                break;

            case DESPAUSANDO:
                bola.setVisible(true);
                setCurrentState(JOGANDO);
                break;

            case GAME_OVER:
                timeblink += delta;
                if (timeblink >= secondsBlink) {
                    timeblink = 0;
                    over.setVisible(!over.isVisible());
                }


                break;
        }

    }

    public void setCurrentState(int state) {
        //.........DEFININDO ESTADOS DO JOGO.............
        stateGame = (byte) state;
        switch (state) {
            case JOGANDO:
                stage.setText(String.valueOf(getCurrentStage()));
                pause.setVisible(false);
                bola.setVisible(true);
                over.setVisible(false);
                break;

            case PAUSAD0:
                break;
            case ESPERANDO:
                over.setVisible(false);
                pause.setVisible(false);
                extraLifeUpIcon.setState(MORTO);
                shieldIcon.setState(MORTO);
                extraLife.setVisible(false);
                extraShield.setVisible(false);
                break;

            case GAME_OVER:
                pause.setVisible(false);
                bola.setVisible(false);
                extraLife.setVisible(false);
                extraShield.setVisible(false);
                over.setText(TEXT_GAME_OVER);
                over.setPosition((LARGURA_LIMITE_TELA / 2) - over.getWidth() / 2, ALTURA_LIMITE_BLOCOS);

                break;

        }


    }

    public void keyPressed(int key) {

        switch (key) {
            case ScreenManager.KEY_NUM0:
                System.out.println(getCurrentStage());
                if (getCurrentState() == GAME_OVER) {
                    lives = 3;
                    ACUM_PONTO = 0;
                    contVidas.setText(String.valueOf(lives));
                    valPont.setText(String.valueOf(ACUM_PONTO));
                    setCurrentState(ESPERANDO);
                    try {
                        setStage(FASE_1);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                break;

            case ScreenManager.KEY_NUM4:
                telaDeJogo.move(-5, 0);
                break;
            case ScreenManager.KEY_NUM6:
                telaDeJogo.move(5, 0);
                break;
            case ScreenManager.KEY_NUM8:
                telaDeJogo.move(0, -5);
                break;
            case ScreenManager.KEY_NUM2:
                telaDeJogo.move(0, 5);
                break;
            case ScreenManager.KEY_NUM1:
                if (stateGame == JOGANDO) {
                    setCurrentState(PAUSAD0);
                } else if (stateGame == PAUSAD0) {
                    setCurrentState(DESPAUSANDO);
                }

                break;
        }
        switch (stateGame) {

            case JOGANDO:
                playerBar.keyPressed(key);
                break;
        }

    }

    public void keyReleased(int key) {
        playerBar.keyReleased(key);
    }

    public final void setSize(int width, int height) {
        super.setSize(width, height);

        //#if SCREEN_SIZE == "BIG"
        if (width > height) {
            telaDeJogo.setPosition(0, 0);
            telaAuxiliar.setPosition(telaDeJogo.getPosX() + telaDeJogo.getWidth(), ALTURA_TOTAL_TELA / 2);
            telaAuxiliar.setSize(telaAuxiliar.getWidth(), 320);
        } else {
            telaDeJogo.setPosition(getPosX() + 20, getPosY());
            telaAuxiliar.setPosition(telaDeJogo.getPosX(), telaDeJogo.getPosY() + telaDeJogo.getHeight() + 10);
        }
//#elif SCREEN_SIZE =="MEDIUM"
//#         if (width > height) {
//#             telaDeJogo.setPosition(0, 0);
//#             telaAuxiliar.setPosition(telaDeJogo.getPosX() + telaDeJogo.getWidth() ,getHeight()-telaAuxiliar.getHeight());
//#         } else {
//#             telaDeJogo.setPosition(0, 0);
//#              telaAuxiliar.setPosition(telaDeJogo.getPosX(),telaDeJogo.getPosY()+ telaDeJogo.getHeight());}
//#
//#
//#elif SCREEN_SIZE =="SMALL"
//#         if (width > height) {
//#             telaAuxiliar.setPosition(telaDeJogo.getPosX() + telaDeJogo.getWidth(), ALTURA_TOTAL_TELA / 2);
//#             ponto.setVisible(false);
//#             valPont.setPosition(ponto.getPosX(), ponto.getPosY());
//#         } else {
//#             telaDeJogo.setPosition(0, 0);
//#             telaAuxiliar.setPosition(telaDeJogo.getPosX(), telaDeJogo.getPosY() + telaDeJogo.getHeight());
//#             if (width == height) {
//#
//#                 ponto.setPosition(stage.getPosX() + stage.getWidth() * 3, stage.getPosY());
//#                 valPont.setPosition(ponto.getPosX() + ponto.getWidth(), ponto.getPosY());
//#             }
//#         }
//#endif

    }

    public void managePointer() {
    }

    public void hideNotify() {
    }

    public void showNotify() {
    }

    public void sizeChanged(int width, int height) {
    }
//#if SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"
    int x1;
    int x2;
    int y2;
    int x3;
    int y3;

    public void onPointerPressed(int x, int y) {
        if (getCurrentState() == GAME_OVER) {
            lives = 3;
            ACUM_PONTO = 0;
            contVidas.setText(String.valueOf(lives));
            valPont.setText(String.valueOf(ACUM_PONTO));
            setCurrentState(ESPERANDO);
            try {
                setStage(FASE_1);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        x -= telaDeJogo.getPosX();
        y -= telaDeJogo.getPosY();
        x1 = x;
        lastPointerPos.set( x, y );
        if (playerBar.contains(x, y)) {
            pointerStatus = POINTER_STATUS_PRESSED;
        }
    }

    public void onPointerDragged(int x, int y) {

        switch (stateGame) {
            case JOGANDO:
                if (telaDeJogo.contains(x, y)) {
                    x -= telaDeJogo.getPosX();
                    y -= telaDeJogo.getPosY();

                    if (pointerStatus != POINTER_STATUS_RELEASED || playerBar.contains(x, y)) {
                        pointerStatus = POINTER_STATUS_DRAGGED;
                        playerBar.move( x - lastPointerPos.x, 0 );
                        lastPointerPos.set(x, y);
                    }
                }
        }
    }

    public void onPointerReleased(int x, int y) {
        x -= telaDeJogo.getPosX();
        y -= telaDeJogo.getPosY();
        x2 = x;
        y2 = y;

        pointerStatus = POINTER_STATUS_RELEASED;


        playerBar.speedX.setSpeed(0);

    }

//#endif


    public void write(DataOutputStream output) throws Exception {
    }

    public void manageBlocks(boolean cont) {
    }
}
