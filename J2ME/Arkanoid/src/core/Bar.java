/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;

/**
 *
 * @author Edson
 */
public class Bar extends Element {

    protected int velBar;
    protected byte stateBar;

    public Bar() {
        super();

        velBar = LARGURA_LIMITE_TELA / 2;
        speedX.setSpeed(0);
        speedY.setSpeed(0);
    }

    public void treatCollision(Element e) {
    }
     public void treatCollision() {
        if (getState() == STATE_1) {
            setState(STATE_2);
        } else if (getState() == STATE_2) {
            setState(STATE_3);
        } else if (getState() == STATE_3) {
            setState(MORTO);
        }
    }

    public boolean treatCollisionWall() {
        boolean collide = false;
        if (getPosX() + getSize().x >= (LARGURA_LIMITE_TELA)) {
            collide = true;
            setPosition(LARGURA_LIMITE_TELA - getSize().x, getPosY());

        }

        if (getPosX() <= (00)) {
            collide = true;
            setPosition(0, getPosY());
        }
        return collide;
    }

    private byte getState() {
      return stateBar;
    }

    public void setState(int state) {

    }
}

