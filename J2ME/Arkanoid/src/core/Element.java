/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import screens.PlayScreen;

/**
 *
 * @author Edson
 */
public abstract class Element extends UpdatableGroup implements Updatable, Constants {

    // variaveis limites de tela
    public static int LARGURA_LIMITE_TELA;
    public static int ALTURA_LIMITE_TELA;
    public static int INICIO_TELA_DE_JOGO_X;
    public static int INICIO_TELA_DE_JOGO_Y;
    public final MUV speedX;
    public final MUV speedY;
    protected int velX;
    protected int velY;

    public Element() {
        super(NUM_ELEMENTS + 50);
        velY = (int) (ALTURA_LIMITE_TELA/ 2);
        velX = (int) (LARGURA_LIMITE_TELA / 2);
        speedX = new MUV(velX);
        speedY = new MUV(velY);
    }

    public void update(int delta) {
        super.update(delta);

        final int diffX = speedX.updateInt(delta);
        final int diffY = speedY.updateInt(delta);
        move(diffX, diffY);
        testColisionWall();
    }

    public boolean testCollision(Element e) {
        boolean collide = false;
        if ((getPosX() + getWidth() > e.getPosX() &&
                getPosX() < e.getPosX() + e.getSize().x &&
                getPosY() + getHeight() > e.getPosY()) &&
                getPosY() < e.getPosY() + e.getSize().y) {

            collide = true;


            if (collide == true) {
                treatCollision(e);
                e.treatCollision(this);

            }
        }

        return collide;
    }

    public boolean testColisionWall() {
        boolean collide = false;
        if (getPosX() + getSize().x > LARGURA_LIMITE_TELA || getPosX() < INICIO_TELA_DE_JOGO_X) {
            collide = true;
        }
        if (getPosY() + getSize().y > ALTURA_LIMITE_TELA || getPosY() < INICIO_TELA_DE_JOGO_Y) {
            collide = true;
        }
        if (collide) {
            treatCollisionWall();
        }
        return collide;
    }

    public static void setLimits(int largura, int altura) {
        LARGURA_LIMITE_TELA = largura;
        ALTURA_LIMITE_TELA = altura;
       

    }

    public abstract void setState(int state);

    public abstract void treatCollision(Element e);

    public abstract boolean treatCollisionWall();
}
