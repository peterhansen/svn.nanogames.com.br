/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author Edson
 */
public class Shield extends Element {

    final DrawableImage shield;
    int stateShield;

    public Shield() throws Exception {
        super();
        speedX.setSpeed(0);
        speedY.setSpeed(0);
        shield = new DrawableImage(PATH_IMAGES +"small/splash/shield.png");
        setSize(shield.getSize());
        setPosition(00, ScreenManager.SCREEN_HEIGHT - shield.getHeight());
        //insertDrawable(shield);
        //setState(STATE_3);

    }

    public void setState(int state) {
        stateShield = state;
        switch (state) {

            case STATE_1:
                setVisible(true);
                break;
            case STATE_2:
                setVisible(true);
                break;
            case STATE_3:
                setVisible(true);
                break;
            case MORTO:
                setVisible(false);
                break;
        }
    }

    public void treatCollision(Element e) {
     speedY.setSpeed(-speedY.getSpeed());
        if (getState() == STATE_1) {
            setState(STATE_2);
        } else if (getState() == STATE_2) {
            setState(STATE_3);
        } else if (getState() == STATE_3) {
            setState(MORTO);
        }
    }

    public int getState() {
        return stateShield;
    }

    public boolean treatCollisionWall() {
        boolean collid = false;
        return collid;
    }
}
