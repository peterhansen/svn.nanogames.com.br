/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Edson
 */
public class Ball extends Element implements Constants {

    public Sprite ball;
    protected byte directionBola;
    public final byte direitaB = 1;
    public final byte esquerdaB = 2;
    public final byte vertB = 3;
    ArkanoidListener p1;
    ArkanoidListener liv;
    int stateBall;

    public Ball(ArkanoidListener p) throws Exception {
        super();

        ball = new Sprite(PATH_IMAGES + "ball");
        setSize(ball.getWidth(), ball.getHeight());
        insertDrawable(ball);
        p1 = p;
        liv = p;


    }

    public void treatCollision(Element e) {
        int posCentroE = e.getPosX() + e.getWidth() / 2;
        int posCentroBola = getPosX() + getSize().x / 2;
        int DistacCentroE;
        //#if SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"
        int dMax = 20;
        //#else
//#          int dMax = 30;
        //#endif
        int velXMax = ScreenManager.SCREEN_WIDTH;
        int novaVelX;
        //........REGRA DE 3
        DistacCentroE = (posCentroBola - posCentroE);
        novaVelX = (DistacCentroE * velXMax) / dMax;

        if (speedY.getSpeed() < 0) {
            setPosition(getPosX(), e.getPosY() + e.getSize().y);
        } else {
            setPosition(getPosX(), e.getPosY() - getSize().y);
        }

        speedY.setSpeed(-speedY.getSpeed());
        speedX.setSpeed(novaVelX);

    }

    public boolean treatCollisionWall() {
        boolean collide = false;



        if (getPosX() < 0 || getPosX() + getSize().x > LARGURA_LIMITE_TELA) {
            setPosition(NanoMath.clamp(getPosX(), 0, LARGURA_LIMITE_TELA - getWidth()),
                    NanoMath.clamp(getPosY(), 0, ALTURA_LIMITE_TELA - getHeight()));
            speedX.setSpeed(-speedX.getSpeed());
            collide = true;
        }
        if (getPosY() < 0) {
            setPosition(getPosX(), 0);
            speedY.setSpeed(-speedY.getSpeed());
            collide = true;
        }
        if (getPosY() + getSize().y > ALTURA_LIMITE_TELA) {
            setPosition(getPosX(), ALTURA_LIMITE_TELA - getSize().y);
            speedY.setSpeed(-speedY.getSpeed());
            if (getPosY() + getHeight() >= ALTURA_LIMITE_TELA) {
                liv.manageLives(true);
                speedX.setSpeed(0);
                speedY.setSpeed(-speedY.getSpeed());
                setState(MORTO);
            }
            collide = true;
        }
        return collide;
    }

    public void treatCollision(Block b) {


        if (speedY.getSpeed() < 0) {
            setPosition(getPosX(), b.getPosY() + b.getSize().y);
        } else {
            setPosition(getPosX(), b.getPosY());
        }

        speedY.setSpeed(-speedY.getSpeed());
        p1.capturaPonto(true);
    }

    public void setState(int state) {

        stateBall = state;
        switch (state) {
            case STATE_3:
                setVisible(true);
                break;

            case MORTO:
                setVisible(false);
                break;
        }
    }

    public int getState() {
        return stateBall;
    }
}
