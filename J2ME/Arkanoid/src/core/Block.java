/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import screens.PlayScreen;

/**
 *
 * @author Edson
 */
public class Block extends Element {

    DrawableImage block;
    public int stateBlock;

    public Block() throws Exception {


        //#if SCREEN_SIZE == "BIG"
        block = new DrawableImage(PATH_IMAGES + "block.png");
        setSize((int) 15, 15);
        //#elif SCREEN_SIZE =="MEDIUM"
//#
//#          block = new DrawableImage(PATH_IMAGES + "block.png");
//#           setSize((int) 12, 12);
        //#elif SCREEN_SIZE =="SMALL"
//#          block = new DrawableImage(PATH_IMAGES + "block.png");
//#            setSize((int) 8, 8);
        //#endif

        speedX.setSpeed(0);
        speedY.setSpeed(0);
        insertDrawable(block);
        setState(STATE_3);
    }

    public void treatCollision(Element e) {
    }

    public void treatCollision() {
        if (getState() == STATE_1) {
            setState(STATE_2);
        } else if (getState() == STATE_2) {
            setState(STATE_3);
        } else if (getState() == STATE_3) {
            setState(MORTO);
        } else if (getState() == STATE_EXTRA_LIFE) {
            setState(MORTO);
        } else if (getState() == STATE_EXTRA_SHIELD) {
            setState(MORTO);
        } else if (getState() == STATE_EXTRA_BALL) {
            setState(MORTO);
        }
    }

    public boolean treatCollisionWall() {
        boolean collide = false;
        return collide;
    }

    public int getState() {
        return stateBlock;
    }

    public void setState(int state) {
        stateBlock = state;
        switch (state) {

            case STATE_1:
                setVisible(true);
                break;
            case STATE_2:
                setVisible(true);
                break;
            case STATE_3:
                setVisible(true);
                break;
            case STATE_EXTRA_LIFE:
                setVisible(true);
                break;
            case MORTO:
                setVisible(false);
                break;
            case INATIVO:
                setVisible(false);
                break;
            case STATE_EXTRA_SHIELD:
                setVisible(true);
                break;
            case STATE_EXTRA_BALL:
                setVisible(true);
                break;
        }
    }
}

