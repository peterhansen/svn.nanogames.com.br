/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

/**
 *
 * @author Edson
 */
public interface ArkanoidListener {
      public abstract void capturaPonto( boolean ponto );
      public abstract void manageLives(boolean cont);
      public abstract void manageBlocks(boolean cont);
}
