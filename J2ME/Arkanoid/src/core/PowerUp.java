/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Edson
 */
public class PowerUp extends Element {

    final DrawableImage powerUp;
    ArkanoidListener l1;
    ArkanoidListener p;
    int stateSpecial;

    public PowerUp(ArkanoidListener l) throws Exception {
        super();
        powerUp = new DrawableImage(PATH_IMAGES + "bonus.png");
        setSize(powerUp.getSize());
        setPosition(LARGURA_LIMITE_TELA / 2, 0);
        insertDrawable(powerUp);
        l1 = l;
        p = l;
        speedY.setSpeed(speedY.getSpeed() / 2);
        speedX.setSpeed(speedX.getSpeed() / 2);

    }

    public void setState(int state) {

        stateSpecial = state;
        switch (state) {
            case STATE_3:
                setVisible(true);
                break;
            case MORTO:
                setVisible(false);
                break;
        }
    }

    public int getState() {
        return stateSpecial;
    }

    public boolean treatCollisionWall() {
        boolean collide = false;

        if (getPosX() < 0 || getPosX() + getSize().x > LARGURA_LIMITE_TELA) {
            setPosition(NanoMath.clamp(getPosX(), 0, LARGURA_LIMITE_TELA - getWidth()),
                    NanoMath.clamp(getPosY(), 0, ALTURA_LIMITE_TELA - getHeight()));

            speedX.setSpeed(-speedX.getSpeed());
            collide = true;
        }

        if (getPosY() < 0) {
            setPosition(getPosX(), INICIO_TELA_DE_JOGO_Y);
            speedY.setSpeed(-speedY.getSpeed());
            collide = true;
        }

        if (getPosY() + getSize().y > ALTURA_LIMITE_TELA) {
            setPosition(getPosX(), ALTURA_LIMITE_TELA - getSize().y);
            speedY.setSpeed(-speedY.getSpeed());
            if (getPosY() + getHeight() >= ALTURA_LIMITE_TELA) {
                // speedX.setSpeed(0);
                speedY.setSpeed(-speedY.getSpeed());
                setState(MORTO);
            }
            collide = true;
        }

        return collide;
    }

    public void treatCollision(Element e) {
        setState(MORTO);
    }
}
