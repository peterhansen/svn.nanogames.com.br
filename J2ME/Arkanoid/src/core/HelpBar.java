/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author Edson
 */
public class HelpBar extends Bar {

    private final int SPEED_2;
    public DrawableImage helpBar;


    //Pattern helpBar;
    public HelpBar() throws Exception {
        super();
        SPEED_2 = velBar;

        helpBar = new DrawableImage(PATH_IMAGES + "bar.png");

        setPosition((LARGURA_LIMITE_TELA / 2) - getSize().x / 2,
                (ScreenManager.SCREEN_HEIGHT) - getSize().y - (ScreenManager.SCREEN_HEIGHT / 20));
        insertDrawable(helpBar);
        setSize(helpBar.getSize());
        move(SPEED_2, 0);
        setState(STATE_3);

    }

    public int getState() {
        return super.stateBar;
    }

    public boolean treatCollisionWall() {
        boolean collide = false;
        if (getPosX() + getSize().x >= (LARGURA_LIMITE_TELA)) {
            collide = true;
            setPosition(LARGURA_LIMITE_TELA - getSize().x, getPosY());
            speedX.setSpeed(-SPEED_2);

        }

        if (getPosX() <= (00)) {
            collide = true;
            setPosition(0, getPosY());
            speedX.setSpeed(SPEED_2);
        }
        return collide;
    }

    public void treatClollisionBlock(Block b) {
        if (getPosX() + getSize().x >= b.getPosX()) {
            setPosition(b.getWidth() - getSize().x, getPosY());
            speedX.setSpeed(-SPEED_2);

        }
        if (getPosX() <= b.getPosX() + b.getWidth()) {
            setPosition(b.getPosX() + b.getWidth(), getPosY());
            speedX.setSpeed(SPEED_2);
        }



    }

    public void setState(int state) {

        stateBar = (byte) state;
        switch (state) {

            case STATE_1:
                setVisible(true);
                break;
            case STATE_2:
                setVisible(true);
                break;
            case STATE_3:
                setVisible(true);
                break;
            case MORTO:
                setVisible(false);
                break;
        }

    }

    public void treatCollision() {
        if (getState() == STATE_1) {
            setState(STATE_2);
        } else if (getState() == STATE_2) {
            setState(STATE_3);
        } else if (getState() == STATE_3) {
            setState(MORTO);
        }
    }

    public void keyPressed(int key) {
    }

    public void keyReleased(int key) {
    }
}
