/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author Edson
 */
public class PlayerBar extends Bar {

    private final int SPEED;
    public DrawableImage playerBar;


    //Pattern playerBar;
    public PlayerBar() throws Exception {
        super();
        SPEED = velBar;
        playerBar = new DrawableImage(PATH_IMAGES + "bar.png");
        insertDrawable(playerBar);
        setSize(playerBar.getSize());

    }

    public void keyPressed(int key) {
        switch (key) {

            case ScreenManager.LEFT:
            case ScreenManager.KEY_NUM4:
                speedX.setSpeed(-SPEED);
                break;

            case ScreenManager.RIGHT:
            case ScreenManager.KEY_NUM6:
                speedX.setSpeed(SPEED);

                break;
        }
    }

    public void keyReleased(int key) {
        speedX.setSpeed(0);
    }
}
