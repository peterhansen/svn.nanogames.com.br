/**
 * Tag.java
 * 
 * Created on Apr 23, 2009, 10:27:24 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Tag extends DrawableGroup implements Constants {

	public static final byte COLOR_BLUE		= 0;
	public static final byte COLOR_ORANGE	= 1;
	public static final byte COLOR_PINK		= 2;

	public static final byte COLORS_TOTAL = 3;

	private static DrawableImage[] LEFT			= new DrawableImage[ COLORS_TOTAL ];
	private static DrawableImage[] FILL_BIG		= new DrawableImage[ COLORS_TOTAL ];
	private static DrawableImage[] DOWN			= new DrawableImage[ COLORS_TOTAL ];
	private static DrawableImage[] FILL_SMALL	= new DrawableImage[ COLORS_TOTAL ];

	private final DrawableImage left;
	private final Pattern fillBig;
	private final DrawableImage down;
	private final Pattern fillSmall;

	private final Label label;

	private int limitRight = -1;


	public Tag( int color, int width, byte fontIndex, int textIndex ) throws Exception {
		this( color, width, fontIndex );

		setText( textIndex );
	}


	public Tag( int color, int width, byte fontIndex ) throws Exception {
		super( 5 );

		left = new DrawableImage( LEFT[ color ] );
		down = new DrawableImage( DOWN[ color ] );

		fillBig = new Pattern( new DrawableImage( FILL_BIG[ color ] ) );
		fillBig.setPosition( left.getWidth(), 0 );
		fillBig.setSize( width, fillBig.getFill().getHeight() );
		
		fillSmall = new Pattern( new DrawableImage( FILL_SMALL[ color ] ) );

		label = new Label( GameMIDlet.getFont( fontIndex ), null );
		label.setPosition( fillBig.getPosition() );
		label.setSize( fillBig.getSize() );

		insertDrawable( left );
		insertDrawable( down );
		insertDrawable( fillBig );
		insertDrawable( fillSmall );
		insertDrawable( label );

		setSize( 0, down.getHeight() );
	}


	public final void setRightLimit( int limit ) {
		limitRight = limit;
	}


	public static final void load() throws Exception {
		final String[] PATH_TAGS = { PATH_IMAGES + "tags/b_", PATH_IMAGES + "tags/o_", PATH_IMAGES + "tags/r_" };

		for ( byte i = 0; i < COLORS_TOTAL; ++i ) {
			LEFT[ i ] = new DrawableImage( PATH_TAGS[ i ] + 0 + ".png" );
			FILL_BIG[ i ] = new DrawableImage( PATH_TAGS[ i ] + 1 + ".png" );
			DOWN[ i ] = new DrawableImage( PATH_TAGS[ i ] + 2 + ".png" );
			FILL_SMALL[ i ] = new DrawableImage( PATH_TAGS[ i ] + 3 + ".png" );
		}
	}


	public final void setPosition( int x, int y ) {
		super.setPosition( x, y );
		
		setSize( ( limitRight < 0 ? ScreenManager.SCREEN_WIDTH : limitRight ) - x, down.getHeight() );

		down.setPosition( fillBig.getPosX() + fillBig.getWidth(), 0 );

		fillSmall.setSize( size.x - ( down.getPosX() + down.getWidth() ), fillSmall.getFill().getHeight() );
		fillSmall.setPosition( size.x - fillSmall.getWidth(), size.y - fillSmall.getHeight() );
	}


	public final void setText( String text ) {
		label.setText( text );
	}


	public final void setText( int textIndex ) {
		label.setText( textIndex );
	}


	public final void setInternalWidth( int width ) {
		fillBig.setSize( width, fillBig.getHeight() );
	}


	public final void setSelected( boolean selected ) {
		final int POS_X = ( ScreenManager.SCREEN_WIDTH - fillBig.getWidth() ) >> 1;
		setPosition( selected ? POS_X + TAG_SELECTED_X_OFFSET : POS_X, getPosY() );
	}


	public final ImageFont getFont() {
		return label.getFont();
	}


	public final int getInternalHeight() {
		return fillSmall.getHeight();
	}

}
