package screens;


import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import core.AnimatedSoftkey;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * GameMIDlet.java
 * �2008 Nano Games.
 *
 * Created on May 23, 2008 11:47:20 PM.
 */

/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener {
	
	private static final byte BACKGROUND_TYPE_PATTERN		= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;	
	
	private static final int BKG_COLOR = 0x333333;
	
	private static Pattern patternBkg;
	
	/** gerenciador da anima��o da soft key esquerda */
	private static AnimatedSoftkey softkeyLeft;	
	
	/** gerenciador da anima��o da soft key direita */
	private static AnimatedSoftkey softkeyRight;		
	
	private static PlayScreen playScreen;
	
	private static ImageFont font;


	protected final int changeScreen( int screen ) throws Exception {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = BACKGROUND_TYPE_PATTERN;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;	
		
		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				final BasicConfirmScreen confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, false );
				if ( !MediaPlayer.isMuted() )
					confirm.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
				
				setMenuCursor( confirm );

				nextScreen = confirm;
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_SPLASH_NANO:
				bkgType = BACKGROUND_TYPE_NONE;
				nextScreen = new BasicSplashNano( midlet, screen, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, ( byte ) -1 );				
			break;
			
			case SCREEN_SPLASH_GAME:
//				nextScreen = new SplashGame( getFont( FONT_INDEX_DEFAULT ) ); TODO
				bkgType = BACKGROUND_TYPE_NONE;
			break;
			
			case SCREEN_MAIN_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_NEW_GAME,
					TEXT_OPTIONS,
					TEXT_HIGH_SCORES,
					TEXT_HELP,
					TEXT_CREDITS,
					TEXT_EXIT,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_EXIT;				
			break;
			
			case SCREEN_NEW_GAME:
				playScreen = new PlayScreen();
			case SCREEN_GAME:
				nextScreen = playScreen;
				bkgType = BACKGROUND_TYPE_NONE;
				indexSoftRight = TEXT_PAUSE;
			break;
			
			case SCREEN_OPTIONS:
				BasicOptionsScreen optionsScreen = null;
				
				if ( MediaPlayer.isVibrationSupported() ) {
					optionsScreen = new BasicOptionsScreen( midlet, getFont( FONT_INDEX_DEFAULT ), screen, new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					optionsScreen = new BasicOptionsScreen( midlet, getFont( FONT_INDEX_DEFAULT ), screen, new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
				}
				nextScreen = optionsScreen;
				
				setMenuCursor( optionsScreen );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_HIGH_SCORES:
				nextScreen = HighScoresScreen.createInstance( getFont( FONT_INDEX_DEFAULT ) );
				
				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_HELP_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_OBJECTIVES,
					TEXT_CONTROLS,
					TEXT_TIPS,
					TEXT_BACK,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
			case SCREEN_HELP_TIPS:
				nextScreen = new BasicTextScreen( midlet, screen, getFont( FONT_INDEX_TEXT ), TEXT_HELP_OBJECTIVES + ( screen - SCREEN_HELP_OBJECTIVES ), false, null );

				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), TEXT_CREDITS_TEXT, true );

				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_PAUSE:
				BasicOptionsScreen pauseScreen = null;
				
				if ( MediaPlayer.isVibrationSupported() ) {
					pauseScreen = new BasicOptionsScreen( midlet, getFont( FONT_INDEX_DEFAULT ), screen, new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					pauseScreen = new BasicOptionsScreen( midlet, getFont( FONT_INDEX_DEFAULT ), screen, new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
					
				}
				nextScreen = pauseScreen;

				setMenuCursor( pauseScreen );
				
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;				
			break;
			
			case SCREEN_CONFIRM_MENU:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO, false );
				setMenuCursor( ( Menu ) nextScreen );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT:
				final int TEXT_EXIT_INDEX = TEXT_CONFIRM_EXIT;
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_EXIT_INDEX, TEXT_YES, TEXT_NO, false );
				setMenuCursor( ( Menu ) nextScreen );
				indexSoftLeft = TEXT_OK;
			break;
			
			case SCREEN_DISCLAIMER:
				HighScoresScreen.createInstance( getFont( FONT_INDEX_DEFAULT ) );
				
				nextScreen = new BasicTextScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), "<ALN_H>ATEN��O\n\nESTE JOGO FOI DESENVOLVIDO PELA NANO GAMES PARA O CURSO \"DESIGN DE JOGOS PARA CELULAR\", MINISTRADO POR FABIO DARCI.\n\nA NANO GAMES N�O SE RESPONSABILIZA POR ALTERA��ES FEITAS AO JOGO POR TERCEIROS.", true ) {
					public final void update( int delta ) {
						final int previousTextOffset = textOffset;
						super.update( delta );
						if ( textOffset != previousTextOffset && textOffset == textLimitBottom ) {
							GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						}
					}
				};
				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			break;
		} // fim switch ( screen )
		
		
		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );	
		
		setBackground( bkgType );
		
		midlet.manager.setCurrentScreen( nextScreen, ScreenManager.TRANSITION_TYPE_IMMEDIATE );
		
		return screen;
	} // fim do m�todo changeScreen( int )


	protected final void loadResources() throws Exception {
		loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts.dat" );
		font = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
		
		patternBkg = new Pattern( new DrawableImage( PATH_IMAGES + "bkg.png" ) );
		patternBkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		softkeyLeft = new AnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
		softkeyRight = new AnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );
		
		try {
			createDatabase( DATABASE_NAME, DATABASE_INDEX_TOTAL );
		} catch ( Exception e ) {
			
		}
		
		MediaPlayer.init( DATABASE_NAME, DATABASE_INDEX_OPTIONS, new String[] {
			PATH_SOUNDS + "shot.mid"
		} );
		
		setScreen( SCREEN_CHOOSE_SOUND );
	}
	
	
	private static final BasicMenu createBasicMenu( int index, int[] entries ) throws Exception {
		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, getFont( FONT_INDEX_DEFAULT ), entries );		
		setMenuCursor( menu );
		
		return menu;
	}		
	
	
	public static final void setMenuCursor( Menu menu ) {
		try {
			final DrawableImage cursor = new DrawableImage( PATH_IMAGES + "cursor.png" );
			cursor.defineReferencePixel( Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_RIGHT );
			menu.setCursor( cursor, Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_LEFT | Drawable.ANCHOR_VCENTER );
			menu.setCurrentIndex( menu.getCurrentIndex() );
		} catch ( Exception e ) {
			//#if DEBUG ==" true"
//# 			e.printStackTrace();
			//#endif
			
			exit();
		}
	}	
	
	
	public static final ImageFont getFont( int index ) {
		return font;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_SPLASH_NANO:
//				setScreen( SCREEN_SPLASH_GAME );
				setScreen( SCREEN_DISCLAIMER );
			break;
			
			case SCREEN_MAIN_MENU:
				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						setScreen( SCREEN_NEW_GAME ); 
					break;
					
					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
					break;
					
					case ENTRY_MAIN_MENU_HIGH_SCORES:
						setScreen( SCREEN_HIGH_SCORES );
					break;
					
					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU
			
			case SCREEN_CREDITS:
				setScreen( SCREEN_MAIN_MENU );
			break;
			
			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_TIPS:
						setScreen( SCREEN_HELP_TIPS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_HELP_CONTROLS:
			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_TIPS:
				setScreen( SCREEN_HELP_MENU );
			break;
			
			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_GAME );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:	
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:	
							setScreen( SCREEN_GAME );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:	
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:			
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}					
				}
			break; // fim case SCREEN_PAUSE
			
			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_BACK:	
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
						break;		
					}
				}
			break; // fim case SCREEN_OPTIONS
			
			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						HighScoresScreen.setScore( playScreen.getScore() );
						playScreen.resetScore();
						setScreen( SCREEN_MAIN_MENU );
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;
			
			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;			
			
			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				
				setScreen( SCREEN_SPLASH_NANO );
			break;
		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
	}

	
	/**
	 * Define uma soft key a partir de um texto. Equivalente � chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece vis�vel. Para o label estar sempre vis�vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( getFont( FONT_INDEX_TEXT ), getText( textIndex ) ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}				
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null )
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
			break;
			
			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) 
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
			break;
		}
	} // fim do m�todo setSoftKey( byte, Drawable, boolean, int )		
	
	
	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_PATTERN:
				midlet.manager.setBackground( patternBkg, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( BKG_COLOR );
			break;
		}
	} // fim do m�todo setBackground( byte )	


	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( playScreen != null ) {
			HighScoresScreen.setScore( playScreen.getScore() );
			playScreen = null;
		}
		
		super.destroyApp( unconditional );
	}


	public static final void gameOver( int score ) {
		if ( HighScoresScreen.setScore( score ) ) {
			playScreen.resetScore();
			setScreen( SCREEN_HIGH_SCORES );
		} else {
			setScreen( SCREEN_MAIN_MENU );
		}
	}		
	
}
