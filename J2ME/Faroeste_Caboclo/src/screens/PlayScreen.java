package screens;

import core.Guy;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GuyGroup;

/**
 * PlayScreen.java
 * �2008 Nano Games.
 *
 * Created on May 30, 2008 10:51:17 AM.
 */

/**
 * 
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, PointerListener {

	public static final byte STATE_NONE				= 0;
	public static final byte STATE_BEGIN_LEVEL		= 1;
	public static final byte STATE_PLAYING			= 2;
	public static final byte STATE_LEVEL_CLEARED	= 3;
	public static final byte STATE_GAME_OVER		= 4;
	
	private byte state;
	
	public static final short TIME_MESSAGE = 3000;
	
	private short timeToNextState;
	
	private static final byte MAX_LIVES = 3;
	
	/** N�mero de vidas restantes do jogador. */
	private byte lives = MAX_LIVES;
	
	private final DrawableImage[] livesImages;
	
	private static final byte MAX_SHOTS = 7;
	
	private byte shotsLeft = MAX_SHOTS;
	
	private static final byte TOTAL_GUYS = 5;
	
	private final GuyGroup guyGroup;
	
	private final DrawableImage[] ammo;
	
	/** pontua��o do jogador */
	private int score;
	
	private short level;
	
	/** pontua��o exibida atualmente */
	private int scoreShown;
	
	/** label que indica a pontua��o e o multiplicador */
	private final Label scoreLabel;	
	
	/** pontua��o m�xima mostrada */
	private static final int SCORE_SHOWN_MAX = 999999;

	/** velocidade de atualiza��o da pontua��o */
	private final MUV scoreSpeed = new MUV();	
	
	private final Label labelMessage;
	
	private final Point[] positions = new Point[] { 
		new Point( 38, 47 ),
		new Point( 104, 47 ),
		new Point( 168, 47 ),
		new Point( 38, 126 ),
		new Point( 168, 127 ),
	};	
	
	
	public PlayScreen() throws Exception {
		super( 30 );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final DrawableImage bkg = new DrawableImage( PATH_IMAGES + "saloon.png" );
		insertDrawable( bkg );
		
		final Pattern pattern = new Pattern( 0x000000 );
		pattern.setPosition( 0, bkg.getHeight() );
		pattern.setSize( size.x, size.y - bkg.getHeight() );
		insertDrawable( pattern );
		
		guyGroup = new GuyGroup( this );
		guyGroup.setSize( size );
		insertDrawable( guyGroup );
		
		ammo = new DrawableImage[ MAX_SHOTS ];
		DrawableImage temp = new DrawableImage( PATH_IMAGES + "ammo.png" );
		for ( int i = 0, x = 0; i < MAX_SHOTS; ++i, x += temp.getWidth() ) {
			ammo[ i ] = new DrawableImage( temp );
			ammo[ i ].setPosition( x, size.y - ammo[ i ].getHeight() );
			insertDrawable( ammo[ i ] );
		}
		
		livesImages = new DrawableImage[ MAX_LIVES ];
		temp = new DrawableImage( PATH_IMAGES + "heart.png" );
		for ( int i = 0, x = 0; i < MAX_LIVES; ++i, x += temp.getWidth() ) {
			livesImages[ i ] = new DrawableImage( temp );
			livesImages[ i ].setPosition( x, 0 );
			insertDrawable( livesImages[ i ] );
		}
		
		scoreLabel = new Label( GameMIDlet.getFont( FONT_INDEX_DEFAULT ), "0" );
		scoreLabel.setPosition( 0, ammo[ 0 ].getPosY() - scoreLabel.getHeight() );
		insertDrawable( scoreLabel );
		
		labelMessage = new Label( GameMIDlet.getFont( FONT_INDEX_DEFAULT ), "" );
		insertDrawable( labelMessage );
		
		prepareLevel( 1 );
	}
	

	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM1:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM3:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
				switch ( state ) {
					case STATE_PLAYING:
						if ( shotsLeft > 0 ) {
							MediaPlayer.play( SOUND_SHOT );
							setShotsLeft( shotsLeft - 1 );

							switch ( guyGroup.hit( key - ScreenManager.KEY_NUM1 ) ) {
								case SHOT_RESULT_HIT_BAD_GUY:
									// adiciona pontos
									changeScore( SCORE_PER_ENEMY );
								break;

								case SHOT_RESULT_HIT_GOOD_GUY:
									// jogador perdeu uma vida
									playerHit();
								break;
							}
						} else {
							// tocar som? TODO
						}
					break;
					
					case STATE_GAME_OVER:
						stateEnded();
					break;
				}
			break;
			
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				switch ( state ) {
					case STATE_PLAYING:
						GameMIDlet.setScreen( SCREEN_PAUSE );
					break;
					
					case STATE_GAME_OVER:
						stateEnded();
					break;
				}
			break;
			
			case ScreenManager.KEY_NUM0:
			case ScreenManager.KEY_POUND:
			case ScreenManager.KEY_STAR:
				if ( state == STATE_PLAYING ) {
					reload();
					break;
				}
			
			default:
				switch ( state ) {
					case STATE_GAME_OVER:
						stateEnded();
					break;
				}
		}
	}


	public final void playerHit() {
		if ( state == STATE_PLAYING ) {
			MediaPlayer.vibrate( VIBRATION_TIME_LOST_LIFE );

			setLives( lives - 1 );
			if ( lives == 0 ) {
				setState( STATE_GAME_OVER );
			}
		}
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( timeToNextState > 0 ) {
			timeToNextState -= delta;
			
			if ( timeToNextState <= 0 )
				stateEnded();
		}
		
		updateScore( delta, false );
	}


	public final void keyReleased( int key ) {
	}


	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
	}


	public final void onPointerReleased( int x, int y ) {
	}


	private final void reload() {
		setShotsLeft( MAX_SHOTS );
	}
	
	
	private final void setLives( int lives ) {
		this.lives = ( byte ) lives;
		
		for ( byte i = 0; i < MAX_LIVES; ++i ) {
			livesImages[ i ].setVisible( i < lives );
		}
	}
	
	
	private final void setShotsLeft( int shotsLeft ) {
		this.shotsLeft = ( byte ) shotsLeft;
		
		for ( byte i = 0; i < MAX_SHOTS; ++i ) {
			ammo[ i ].setVisible( i < shotsLeft );
		}
	}
	
	
	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			updateScoreLabel();
		}
	} // fim do m�todo updateScore( boolean )		

	
	private final void updateScoreLabel() {
		scoreLabel.setText( String.valueOf( scoreShown ) );
	}	
	
	
	private final void changeScore( int diff ) {
		score += diff;
		
		scoreSpeed.setSpeed( score - scoreShown, false );
	}	
	
	
	private final void prepareLevel( int level ) {
		this.level = ( short ) level;
		
		guyGroup.prepare( level );
		
		setState( STATE_BEGIN_LEVEL );
	}
	
	
	public final void setState( int state ) {
		this.state = ( byte ) state;
		
		timeToNextState = 0;
		labelMessage.setVisible( false );
		
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				showMessage( GameMIDlet.getText( TEXT_LEVEL ) + level );
				
				timeToNextState = TIME_MESSAGE;
			break;
			
			case STATE_PLAYING:
				labelMessage.setVisible( false );
			break;
			
			case STATE_GAME_OVER:
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ) );
			break;
			
			case STATE_LEVEL_CLEARED:
				showMessage( GameMIDlet.getText( TEXT_LEVEL_CLEARED ) );
				timeToNextState = TIME_MESSAGE;
			break;
		}
	}
	
	
	private final void showMessage( String message ) {
		labelMessage.setText( message );
		labelMessage.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		labelMessage.setRefPixelPosition( size.x >> 1, size.y >> 1 );		
		labelMessage.setVisible( true );
	}
	
	
	private final void stateEnded() {
		switch ( state ) {
			case STATE_LEVEL_CLEARED:
				prepareLevel( level + 1 );
			break;
			
			case STATE_BEGIN_LEVEL:
				setState( STATE_PLAYING );
			break;
			
			case STATE_GAME_OVER:
				GameMIDlet.gameOver( score );
			break;
		}
	}
	
	
	public final Point getSlot( int index ) {
		return positions[ index ];
	}
	
	
	public final void resetScore() {
		score = 0;
		scoreShown = 0;
	}	

	
	public final int getScore() {
		return score;
	}
	
}
