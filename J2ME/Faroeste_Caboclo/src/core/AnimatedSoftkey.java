package core;

/*
 * AnimatedSoftkey.java
 * 
 * Created on December 6, 2007, 8:48 PM
 */



import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;

/**
 *
 * @author peter
 */
public final class AnimatedSoftkey extends UpdatableGroup {
	

	private static final byte STATE_HIDDEN		= 0;
	private static final byte STATE_APPEARING	= 1;
	private static final byte STATE_SHOWN		= 2;
	private static final byte STATE_HIDING		= 3;

	private byte state;

	private Drawable nextDrawable;
	
	private short nextVisibleTime;
	
	/** tempo que o label permanece no estado STATE_SHOWN (caso seja negativo, indica que � pra permanecer vis�vel */
	private short visibleTime;
	
	/** tempo padr�o de visibilidade do softkey */
	public static final short DEFAULT_VISIBLE_TIME = 3000;
	
	private final MUV speed = new MUV();
	
	private final byte softkeyIndex;
	
	private boolean inTransition;


	/**
	 * 
	 * @param softkeyIndex �ndice da softkey a ser definida. Os valores v�lidos s�o:
	 * <ul>
	 * <li>ScreenManager.SOFT_KEY_LEFT</li>
	 * <li>ScreenManager.SOFT_KEY_MID</li>
	 * <li>ScreenManager.SOFT_KEY_RIGHT</li>
	 * </ul>
	 * @throws java.lang.Exception
	 */
	public AnimatedSoftkey( byte softkeyIndex ) throws Exception {
		super( 1 );
		
		this.softkeyIndex = softkeyIndex;
	}


	/**
	 * Define o pr�ximo drawable a ser utilizado como softkey.
	 * @param d pr�ximo drawable a ser utilizado como softkey. Passar <i>null</i> remove o softkey atual.
	 * @param visibleTime tempo m�ximo que o softkey permanece vis�vel. Utilize zero ou valores negativos para 
	 * que ele fique permanentemente vis�vel.
	 * @param changeNow indica se a troca deve ser imediata, ou somente ap�s o softkey atual expirar seu tempo
	 * de visibilidade.
	 */
	public synchronized final void setNextSoftkey( Drawable d, int visibleTime, boolean changeNow ) {
		nextDrawable = d;
		nextVisibleTime = ( short ) visibleTime;

		switch ( state ) {
			case STATE_HIDDEN:
				changeDrawables();
			break;
			
			case STATE_HIDING:
			case STATE_APPEARING:
			case STATE_SHOWN:
				if ( changeNow )
					setState( STATE_HIDING );
				else { 
					// se a troca n�o for imediata e o softkey anterior n�o tiver tempo limite, define o tempo
					// limite (caso contr�rio, a troca jamais ocorreria)
					if ( this.visibleTime <= 0 )
						this.visibleTime = DEFAULT_VISIBLE_TIME;
				}
					
			break;
		} // fim switch ( state )
	}


	public final void update( int delta ) {
		super.update( delta );

		final Drawable d = getDrawable( 0 );
		switch ( state ) {
			case STATE_APPEARING:
				if ( !inTransition ) {
					d.move( 0, speed.updateInt( delta ) );
					if ( d.getPosition().y <= 0 )
						setState( STATE_SHOWN );
				}
			break;

			case STATE_SHOWN:
				if ( visibleTime > 0 ) {
					visibleTime -= delta;
					if ( visibleTime <= 0 )
						setState( STATE_HIDING );
				}
			break;

			case STATE_HIDING:
				d.move( 0, speed.updateInt( delta ) );
				if ( d.getPosition().y >= size.y )
					setState( STATE_HIDDEN );
			break;
		} // fim switch ( state )
	}


	private final void changeDrawables() {
		removeDrawable( 0 );

		if ( nextDrawable == null ) {
			setState( STATE_HIDDEN );
		} else {
			visibleTime = nextVisibleTime;
			
			setSize( nextDrawable.getSize() );
			ScreenManager.getInstance().setSoftKey( softkeyIndex, this );

			insertDrawable( nextDrawable );
			nextDrawable = null;
			
			setState( STATE_APPEARING );
		}
	}


	private final void setState( byte newState ) {
		if ( activeDrawables > 0 ) {
			final Drawable d = getDrawable( 0 );

			switch ( newState ) {
				case STATE_HIDDEN:
					if ( nextDrawable != null ) {
						changeDrawables();
						return;
					}
					setVisible( false );
				break;

				case STATE_APPEARING:
					setVisible( !inTransition );
					speed.setSpeed( ( -size.y * 3 ) >> 1 );
					d.setPosition( 0, size.y );
				break;

				case STATE_SHOWN:
					d.setPosition( 0, 0 );
				break;

				case STATE_HIDING:
					speed.setSpeed( ( size.y * 3 ) >> 1 );
				break;
			} // fim switch ( state )			
		}

		state = newState;
	}
	
	
	public final void setInTransition( boolean inTransition ) {
		this.inTransition = inTransition;
		
		setVisible( !inTransition && state != STATE_HIDDEN );
	}


	//#if SCREEN_SIZE != "SMALL"
	public final boolean contains( int x, int y ) {
		return drawables[ 0 ] != null && super.contains( x, y );
	}
	//#endif
	

}
