package core;

/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on May 30, 2008 11:25:21 AM.
 */

/**
 *
 * @author Peter
 */
public interface Constants {
	
	public static final short SLOT_WIDTH = 32;
	
	public static final short SLOT_HEIGHT = 48;
	
	public static final String DATABASE_NAME = "N";
	
	public static final byte DATABASE_INDEX_OPTIONS = 1;
	public static final byte DATABASE_SLOT_SCORES = 2;
	
	public static final byte DATABASE_INDEX_TOTAL = 2;
	
	public static final String PATH_IMAGES = "/";
	public static final String PATH_SOUNDS = "/";
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";

	public static final String PATH_CHARACTERS = PATH_IMAGES + "characters/";
	
	public static final byte SOUND_SHOT = 0;
	
	public static final byte SHOT_RESULT_NO_HIT			= 0;
	public static final byte SHOT_RESULT_HIT_GOOD_GUY	= 1;
	public static final byte SHOT_RESULT_HIT_BAD_GUY	= 2;
	
	public static final short VIBRATION_TIME_LOST_LIFE = 200;
	
	public static final byte DIFFICULTY_LEVEL_MAX = 15;
	
	public static final byte SCORE_PER_ENEMY = 100;
	
	/** �ndice da fonte padr�o do jogo. */
	public static final byte FONT_INDEX_DEFAULT	= 0;
	
	/** �ndice da fonte do placar. */
	public static final byte FONT_INDEX_BOARD	= 1;
	
	/** �ndice da fonte utilizada para textos. */
	public static final byte FONT_INDEX_TEXT	= 2;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 3;	
	
	public static final byte SCREEN_CHOOSE_SOUND			= 0;
	public static final byte SCREEN_SPLASH_NANO				= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME				= SCREEN_MAIN_MENU + 1;	
	public static final byte SCREEN_GAME					= SCREEN_NEW_GAME + 1;	
	public static final byte SCREEN_OPTIONS					= SCREEN_GAME + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_OBJECTIVES			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS			= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_HELP_TIPS				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_HELP_TIPS + 1;
	public static final byte SCREEN_PAUSE					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU			= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT			= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_DISCLAIMER				= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_HIGH_SCORES				= SCREEN_DISCLAIMER + 1;
	
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_HIGH_SCORES	= 2;
	public static final byte ENTRY_MAIN_MENU_HELP			= 3;
	public static final byte ENTRY_MAIN_MENU_CREDITS		= 4;
	public static final byte ENTRY_MAIN_MENU_EXIT			= 5;
	
	// </editor-fold>
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DO CAMPEONATO">
	
	public static final byte ENTRY_CHAMPIONSHIP_MENU_3RD_DIVISION	= 0;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_2ND_DIVISION	= 1;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_1ST_DIVISION	= 2;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_BACK			= 3;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_TIPS						= 2;
	public static final byte ENTRY_HELP_MENU_BACK						= 3;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>
		
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_PAUSE							= TEXT_OPTIONS + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_TIPS							= TEXT_CONTROLS + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_HELP_TIPS						= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_HELP_TIPS + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_NANO + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_NO							= TEXT_YES + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_NO + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT					= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_LEVEL							= TEXT_CONFIRM_EXIT + 1;
	public static final byte TEXT_LEVEL_CLEARED					= TEXT_LEVEL + 1;
	public static final byte TEXT_HIGH_SCORES					= TEXT_LEVEL_CLEARED + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_HIGH_SCORES + 1;
	
	/** n�mero total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_GAME_OVER + 1;
	
	public static final byte TEXT_CONFIRM_EXIT_TOTAL = 5;
	// </editor-fold>	
	
		
	
}
