package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.PlayScreen;

/**
 * Guy.java
 * �2008 Nano Games.
 *
 * Created on May 23, 2008 11:49:41 PM.
 */

/**
 * 
 * @author Peter
 */
public class Guy extends Sprite implements Constants {
	
	public static final byte INDEX_BAD_GUY_1	= 0;
	public static final byte INDEX_BAD_GUY_2	= 1;
	public static final byte INDEX_BAD_GUY_3	= 2;
	public static final byte INDEX_GOOD_GUY_1	= 3;
	public static final byte INDEX_GOOD_GUY_2	= 4;
	public static final byte INDEX_GOOD_GUY_3	= 5;
	
	public static final byte GUYS_TOTAL = 6;
	
	private static final byte PERCENT_SHOOT_INITIAL	= 30;
	private static final byte PERCENT_SHOOT_FINAL	= 70;
	
	private static byte currentShootPercent;
	
	private static final short MIN_INTERVAL = 0;
	private static final short MAX_INTERVAL_EASY = 3400;
	private static final short MAX_INTERVAL_HARD = 1000;
	
	private static short currentMaxInterval;
	
	private static final byte SPEED = SLOT_HEIGHT << 1;
	
	private short timeToNextMove;
	
	public static final byte DIRECTION_HORIZONTAL	= 0;
	public static final byte DIRECTION_VERTICAL		= 1;
	
	private final byte direction;
	
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;
	public static final byte STATE_DYING		= 4;
	public static final byte STATE_SHOOTING		= 5;
	
	private byte state;
	
	public static final byte SEQUENCE_MOVING	= 0;
	public static final byte SEQUENCE_DYING		= 1;
	public static final byte SEQUENCE_SHOOTING	= 2;
	
	private final MUV speed = new MUV();
	
	private int limit;
	
	private final byte index;
	
	private static final byte MAX_APPEARANCES = 3;
	
	private byte appearances;
	
	private final GuyGroup group;
	
	
	public Guy( GuyGroup group, int index ) throws Exception {
		super( PATH_CHARACTERS + ( index / 3 ) + ".bin", PATH_CHARACTERS + ( index / 3 ) );
		
		this.index = ( byte ) index;
		this.group = group;
		
		appearances = ( byte ) ( 1 + NanoMath.randInt( MAX_APPEARANCES ) );
		
		this.direction = NanoMath.randInt( 100 ) < 50 ? DIRECTION_HORIZONTAL : DIRECTION_VERTICAL;
		
		viewport = new Rectangle();
		
		timeToNextMove = ( short ) ( PlayScreen.TIME_MESSAGE + NanoMath.randInt( MAX_INTERVAL_EASY ) );
		setState( STATE_HIDDEN );
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		timeToNextMove -= delta;
		if ( timeToNextMove > 0 ) {
			boolean ended = false;
			
			switch ( direction ) {
				case DIRECTION_HORIZONTAL:
					move( speed.updateInt( delta ), 0 );
					
					if ( speed.getSpeed() < 0 ) {
						if ( position.x <= limit ) {
							ended = true;
							position.x = limit;
						}
					} else {
						if ( position.x >= limit ) {
							ended = true;
							position.x = limit;
						}						
					}
				break;

				case DIRECTION_VERTICAL:
					move( 0, speed.updateInt( delta ) );
					
					if ( speed.getSpeed() < 0 ) {
						if ( position.y <= limit ) {
							ended = true;
							position.y = limit;
						}
					} else {
						if ( position.y >= limit ) {
							ended = true;
							position.y = limit;
						}						
					}
				break;
			} // fim switch ( direction )
			
			if ( ended ) {
				// chegou ao destino
				switch ( state ) {
					case STATE_HIDING:
						setState( STATE_HIDDEN );
					break;
					
					case STATE_APPEARING:
						setState( STATE_SHOWN );
					break;
				}
			}
		} else {
			nextMove();
		}
	}
	
	
	private final void nextMove() {
		timeToNextMove = ( short ) ( MIN_INTERVAL + NanoMath.randInt( currentMaxInterval ) );
		
		switch ( state ) {
			case STATE_HIDDEN:
				if ( NanoMath.randInt( 100 ) < 80 )
					setState( STATE_APPEARING );
			break;
			
			case STATE_APPEARING:
				if ( NanoMath.randInt( 100 ) < 20 )
					setState( STATE_HIDING );
			break;
			
			case STATE_SHOWN:
				if ( isBadGuy() && NanoMath.randInt( 100 ) < currentShootPercent ) {
					setState( STATE_SHOOTING );
					break;
				}
			
			case STATE_SHOOTING:
				if ( NanoMath.randInt( 100 ) < 80 )
					setState( STATE_HIDING );				
			break;
			
			case STATE_HIDING:
				if ( NanoMath.randInt( 100 ) < 20 )
					setState( STATE_APPEARING );
			break;
			
		}
	}
	
	
	public final void setState( byte state ) {
		final byte previousState = this.state;
		this.state = state;
		
		switch ( state ) {
			case STATE_DYING:
				setSequence( SEQUENCE_DYING );
			break;
			
			case STATE_SHOOTING:
				setSequence( SEQUENCE_SHOOTING );
				position.set( viewport.x, viewport.y );
				speed.setSpeed( 0 );
			break;
			
			case STATE_APPEARING:
				if ( getSequence() != SEQUENCE_MOVING )
					setSequence( SEQUENCE_MOVING );
				
				switch ( direction ) {
					case DIRECTION_HORIZONTAL:
						if ( previousState == STATE_HIDDEN ) {
							if ( NanoMath.randInt( 100 ) < 50 )
								setPosition( viewport.x - size.x, viewport.y );
							else
								setPosition( viewport.x + viewport.width, viewport.y );	
						}
						
						speed.setSpeed( position.x > viewport.x ? -SPEED : SPEED );
						limit = viewport.x;
					break;

					case DIRECTION_VERTICAL:
						if ( previousState == STATE_HIDDEN )
							setPosition( viewport.x, viewport.y + viewport.height );
						
						speed.setSpeed( -SPEED );
						limit = viewport.y;
					break;
				}				
			break;
			
			case STATE_HIDING:
				if ( getSequence() != SEQUENCE_MOVING )
					setSequence( SEQUENCE_MOVING );
				
				switch ( direction ) {
					case DIRECTION_HORIZONTAL:
						speed.setSpeed( position.x > viewport.x ? -SPEED : SPEED );
						if ( speed.getSpeed() < 0 )
							limit = viewport.x - size.x;
						else
							limit = viewport.x + viewport.width;
					break;

					case DIRECTION_VERTICAL:
						speed.setSpeed( SPEED );
						limit = viewport.y + viewport.height;						
					break;
				}				
			break;
			
			case STATE_SHOWN:
				position.set( viewport.x, viewport.y );
				speed.setSpeed( 0 );
				
				if ( getSequence() != SEQUENCE_MOVING )
					setSequence( SEQUENCE_MOVING );
			break;
			
			case STATE_HIDDEN:
				--appearances;
				
				if ( appearances >= 0 ) {
					speed.setSpeed( 0 );

					if ( getSequence() != SEQUENCE_MOVING )
						setSequence( SEQUENCE_MOVING );
				} else {
					// entra um novo personagem na posi��o
					group.changeGuy( this );
				}
			break;
		}
	}
	
	
	public final boolean isBadGuy() {
		switch ( index ) {
			case INDEX_BAD_GUY_1:
			case INDEX_BAD_GUY_2:
			case INDEX_BAD_GUY_3:
				return true;
				
			default:
				return false;
		}
	}
	
	
	/**
	 * Jogador atirou nessa posi��o; verifica se o personagem foi atingido.
	 */
	public final byte hit() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_DYING:
				return SHOT_RESULT_NO_HIT;
			
			default:
				setState( STATE_DYING );
				return isBadGuy() ? SHOT_RESULT_HIT_BAD_GUY : SHOT_RESULT_HIT_GOOD_GUY;
		}
	}
	
	
	public static final void prepare( int level ) {
		currentShootPercent = ( byte ) ( PERCENT_SHOOT_INITIAL + ( PERCENT_SHOOT_FINAL - PERCENT_SHOOT_INITIAL ) * level / DIFFICULTY_LEVEL_MAX );
		currentMaxInterval = ( short ) ( MAX_INTERVAL_EASY + ( MAX_INTERVAL_HARD - MAX_INTERVAL_EASY ) * level / DIFFICULTY_LEVEL_MAX );
	}
	
	
	public static final byte getRandomIndex( boolean badGuy ) {
		if ( badGuy ) {
			return ( byte ) NanoMath.randInt( 3 );
		}
		
		return ( byte ) ( NanoMath.randInt( 3 ) + INDEX_GOOD_GUY_1 );
	}
	
	
}
