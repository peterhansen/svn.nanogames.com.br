/**
 * GuyGroup.java
 * �2008 Nano Games.
 *
 * Created on Jun 1, 2008 9:01:50 PM.
 */

package core;

import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.PlayScreen;


/**
 * 
 * @author Peter
 */
public final class GuyGroup extends UpdatableGroup implements Constants, SpriteListener {

	private static final byte ACTIVE_GUYS_MAX = 5;
	
	private static final byte BAD_GUYS_INITIAL	= 5;
	private static final byte BAD_GUYS_FINAL	= 20;
	
	private static byte badGuysRemaining;
	
	private static byte activeBadGuys;	
	
	private final Guy[] sprites;
	
	private final Mutex mutex = new Mutex();
	
	private final PlayScreen playScreen;
	
	
	public GuyGroup( PlayScreen screen ) throws Exception {
		super( ACTIVE_GUYS_MAX );
		
		playScreen = screen;
		
		sprites = new Guy[ Guy.GUYS_TOTAL ];

		for ( byte i = 0; i < Guy.GUYS_TOTAL; ++i ) {
			sprites[ i ] = new Guy( this, i );
		}		
	}
	
	
	public final void changeGuy( Guy g ) {
		changeGuy( g, true );
	}
	
	
	public final void changeGuy( Guy g, final boolean useMutex ) {
		if ( useMutex )
			mutex.acquire();
		
		for ( byte i = 0; i < activeDrawables; ++i ) {
			if ( drawables[ i ] == g ) {
				try {
					removeDrawable( i );
				
					final Guy newGuy = new Guy( this, Guy.getRandomIndex( badGuysRemaining > 0 && activeBadGuys < badGuysRemaining && NanoMath.randInt( 100 ) < 70 ) );
					if ( newGuy.isBadGuy() )
						++activeBadGuys;					
					
					newGuy.setListener( this, i );
					final Point viewportTopLeft = playScreen.getSlot( i );
					newGuy.setViewport( new Rectangle( viewportTopLeft.x, viewportTopLeft.y, SLOT_WIDTH, SLOT_HEIGHT ) );
					
					insertDrawable( newGuy, i );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
				}
				
				break;
			}
		}
		
		if ( useMutex )
			mutex.release();
	}
	
	
	public final void insertGuy( int slot ) {
		mutex.acquire();
		
		try {
			final Guy newGuy = new Guy( this, Guy.getRandomIndex( badGuysRemaining > 0 && activeBadGuys < badGuysRemaining && NanoMath.randInt( 100 ) < 30 ) );
			if ( newGuy.isBadGuy() )
				++activeBadGuys;

			newGuy.setListener( this, slot );
			final Point viewportTopLeft = playScreen.getSlot( slot );
			newGuy.setViewport( new Rectangle( viewportTopLeft.x, viewportTopLeft.y, SLOT_WIDTH, SLOT_HEIGHT ) );

			insertDrawable( newGuy, slot );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}		
		
		mutex.release();
	}
	
	
	public final void prepare( int level ) {
		level = Math.min( level, DIFFICULTY_LEVEL_MAX );
		
		do {
			removeDrawable( 0 );
		} while ( activeDrawables > 0 );
		
		activeBadGuys = 0;
		badGuysRemaining = ( byte ) ( BAD_GUYS_INITIAL + ( BAD_GUYS_FINAL - BAD_GUYS_INITIAL ) * level / DIFFICULTY_LEVEL_MAX );
		
		Guy.prepare( level );
		for ( byte i = 0; i < ACTIVE_GUYS_MAX; ++i )
			insertGuy( i );
	}
	
	
	public final byte hit( int index ) {
		return ( ( Guy ) drawables[ index ] ).hit();
	}


	public final void onSequenceEnded( int id, int sequence ) {
		mutex.acquire();
		
		try {
			switch ( sequence ) {
				case Guy.SEQUENCE_DYING:
					// adiciona novo personagem no local
					final Guy guy = ( ( Guy ) drawables[ id ] );
					if ( guy.isBadGuy() ) {
						--activeBadGuys;
						--badGuysRemaining;
					}
					
					if ( badGuysRemaining > 0 ) {
						changeGuy( guy, false );
					} else {
						removeDrawable( id );
						playScreen.setState( PlayScreen.STATE_LEVEL_CLEARED );
					}
				break;
				
				case Guy.SEQUENCE_SHOOTING:
					// jogador perdeu uma vida
					playScreen.playerHit();
					( ( Guy ) drawables[ id ] ).setState( Guy.STATE_HIDING );
				break;
			}
		} catch ( Exception ex ) {
			ex.printStackTrace();
		}
		
		mutex.release();
	}
}
