/**
 * MenuLabel.java
 * 
 * Created on Mar 2, 2009, 3:14:44 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public class MenuLabel extends UpdatableGroup implements Constants, SpriteListener {

	private static final byte SEQUENCE_UNSELECTED	= 0;
	private static final byte SEQUENCE_SELECTING	= 1;
	private static final byte SEQUENCE_SELECTED		= 2;
	private static final byte SEQUENCE_UNSELECTING	= 3;

	private static final byte ID_BAR	= 0;
	private static final byte ID_SHINE	= 1;

	private static Sprite BAR;
	private static Sprite SHINE;
	private static Sprite SHINE2;

	private final MarqueeLabel label;

	private boolean current;

	private final Sprite bar;

	//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 		private final Sprite barRight;
	//#endif

	private final Sprite shine;
	private final Sprite shine2;

	private final Pattern bkg;

	private Drawable parent;

	private boolean selectable = true;


	public MenuLabel( int textIndex ) throws Exception {
		this( GameMIDlet.getText( textIndex ) );
	}


	public MenuLabel( String text ) throws Exception {
		super( 7 );

		if ( BAR == null )
			BAR = new Sprite( PATH_IMAGES + "menubox" );

		bkg = new Pattern( 0xf5f647 );
		insertDrawable( bkg );

		bar = new Sprite( BAR );
		bar.setListener( this, ID_BAR );
		insertDrawable( bar );

		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# //			bar.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
//# 			bar.defineReferencePixel( ANCHOR_CENTER );
//# 			barRight = new Sprite( bar );
//# 			barRight.mirror( TRANS_MIRROR_H );
//# //			barRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
//# 			barRight.defineReferencePixel( ANCHOR_CENTER );
//# 			insertDrawable( barRight );
		//#else
			bar.defineReferencePixel( ANCHOR_CENTER );
		//#endif

		final Rectangle r = bar.getFrame( 0 );
		bkg.setSize( bar.getWidth() + 2, r.height + 2 );
		bkg.defineReferencePixel( ANCHOR_CENTER );

		label = new MarqueeLabel( FONT_TITLE, text );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		insertDrawable( label );

		if ( SHINE == null ) {
			//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
			SHINE = new Sprite( PATH_IMAGES + "menu_shine" );
			//#else
//# 				SHINE = new Sprite( PATH_IMAGES + "shine" );
			//#endif
		}

		if ( SHINE2 == null )
			SHINE2 = new Sprite( PATH_IMAGES + "menu_shine_2" );
		
		shine = new Sprite( SHINE );
		shine.setListener( this, ID_SHINE );
		shine.setVisible( false );
		shine.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( shine );

		shine2 = new Sprite( SHINE2 );
		shine2.setVisible( true );
		shine2.setSequence( 1 );
		insertDrawable( shine2 );

		setSize( shine2.getSize() );
		bar.setRefPixelPosition( getWidth() >> 1, getHeight() >> 1 );
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			barRight.setRefPixelPosition( bar.getRefPixelPosition() );
		//#endif
			
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		shine.setRefPixelPosition( bar.getRefPixelPosition() );
		bkg.setRefPixelPosition( bar.getRefPixelPosition() );

		label.setPosition( bar.getPosX() + MENU_LABEL_OFFSET_X, bar.getPosY() + ( ( bar.getHeight() - label.getHeight() ) >> 1 ) );
		label.setSize( bar.getWidth() - ( MENU_LABEL_OFFSET_X << 1 ), label.getHeight() );

		setText( text );
		setCurrent( false );
	}


	public static final void unload() {
		//#if DEBUG == "true"
			System.gc();
			final long memory = Runtime.getRuntime().freeMemory();
		//#endif

		SHINE = null;
		SHINE2 = null;
		BAR = null;

		//#if DEBUG == "true"
			System.gc();
			System.out.println( "MenuLabel.unload -> memória liberada: " + ( Runtime.getRuntime().freeMemory() - memory ) + " bytes." );
		//#endif
	}
	

	public final void setSelectable( boolean s ) {
		selectable = s;

		if ( !selectable ) {
			setBarSequence( SEQUENCE_SELECTED );
			refreshLabel();
		}
		shine.setVisible( selectable );
	}


	public final void setCurrent( boolean c ) {
		if ( selectable ) {
			bkg.setVisible( false );
			this.current = c;
			switch ( bar.getSequenceIndex() ) {
				case SEQUENCE_SELECTED:
				case SEQUENCE_SELECTING:
					if ( !c ) {
						setBarSequence( SEQUENCE_UNSELECTING );
						shine.setSequence( SHINE_SEQUENCE_RIGHT );
						shine.setTransform( TRANS_MIRROR_H );
						shine.setVisible( true );
					}
				break;

				default:
					if ( c ) {
						setBarSequence( SEQUENCE_SELECTING );
						shine.setSequence( SHINE_SEQUENCE_RIGHT );
						shine.setTransform( TRANS_NONE );
						shine.setVisible( true );
					}
			}
			refreshLabel();
		}
		shine2.setVisible( false );
		bkg.setVisible( false );
		shine2.setSequence( 0 );
	}


	private final void refreshLabel() {
		label.setViewport( new Rectangle( 0, 0, Short.MAX_VALUE, Short.MAX_VALUE ) );
		label.setScrollFrequency( ( current || !selectable ) ? MarqueeLabel.SCROLL_FREQ_IF_BIGGER : MarqueeLabel.SCROLL_FREQ_NONE );
		label.setTextOffset( Math.max( 0, ( label.getWidth() - label.getFont().getTextWidth( label.getText() ) ) >> 1 ) );
	}


	public final void setText( int textIndex ) {
		setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setText( String text ) {
		label.setText( text, false );
		refreshLabel();
	}


	public void update( int delta ) {
		super.update( delta );

		switch ( bar.getSequenceIndex() ) {
			case SEQUENCE_SELECTING:
			case SEQUENCE_UNSELECTING:
				final int percent;
				switch ( bar.getCurrFrameIndex() ) {
					case 1:
					case 5:
						percent = 50;
					break;

					case 2:
					case 4:
						percent = 20;
					break;

					case 3:
						percent = 0;
					break;

					case 0:
					case 6:
					default:
						percent = 100;
				}
				final Rectangle v = label.getViewport();

				if ( v != null ) {
					final int height = percent * label.getHeight() / 100;
					label.getViewport().height = height;
					final int PARENT_Y = parent == null ? 0 : parent.getPosY();
					label.getViewport().y = PARENT_Y + getPosY() + label.getPosY() + ( label.getHeight() >> 1 ) - ( height >> 1 );
				}
			break;
		}
	}


	public final void setParent( Drawable p ) {
		parent = p;
	}


	private final void setBarSequence( int sequence ) {
		bar.setSequence( sequence );
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			barRight.setSequence( sequence );
		//#endif
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( id ) {
			case ID_BAR:
				switch ( sequence ) {
					case SEQUENCE_SELECTING:
						setBarSequence( SEQUENCE_SELECTED );
						label.setViewport( null );
						shine2.setVisible( true );
						shine2.setSequence( 1 );
						bkg.setVisible( true );
					break;

					case SEQUENCE_UNSELECTING:
						setBarSequence( SEQUENCE_UNSELECTED );
						label.setViewport( null );
					break;
				}
			break;

			case ID_SHINE:
				shine.setVisible( false );
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}



}
