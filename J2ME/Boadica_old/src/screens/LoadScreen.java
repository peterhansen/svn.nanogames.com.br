package screens;

import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.MenuLabel;
import javax.microedition.lcdui.Graphics;


public final class LoadScreen extends MenuLabel implements Updatable, ScreenListener {

	/** Intervalo de atualizaÃ§Ã£o do texto. */
	private static final short CHANGE_TEXT_INTERVAL = 600;

	private long lastUpdateTime;

	private static final byte MAX_DOTS = 4;

	private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

	private byte dots;

	private Thread loadThread;

	private final LoadListener listener;

	private boolean painted;

//	private final byte previousScreen;

	private boolean active = true;


	public LoadScreen( LoadListener listener ) throws Exception {
		super( TEXT_LOADING );
//		previousScreen = currentScreen;
		this.listener = listener;
		ScreenManager.setKeyListener( null );
		//#if TOUCH == "true"
			ScreenManager.setPointerListener( null );
		//#endif
		setSelectable( false );
	}


	public final void update( int delta ) {
		final long interval = System.currentTimeMillis() - lastUpdateTime;
		if ( interval >= CHANGE_TEXT_INTERVAL ) {
			// os recursos do jogo sÃ£o carregados aqui para evitar sobrecarga do mÃ©todo loadResources, o que
			// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
			if ( loadThread == null ) {
				if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
					ScreenManager.setKeyListener( null );
					//#if TOUCH == "true"
						ScreenManager.setPointerListener( null );
					//#endif
					final LoadScreen loadScreen = this;
					loadThread = new Thread() {

						public final void run() {
							try {
								MediaPlayer.stop();
								AppMIDlet.gc();
								listener.load( loadScreen );
							} catch ( Throwable e ) {
								//#if DEBUG == "true"
									e.printStackTrace();
									GameMIDlet.log( e.getMessage() );
								//#endif
								GameMIDlet.setScreen( SCREEN_MAIN_MENU );
							}
						}
					};
					loadThread.start();
				}
			} else if ( active ) {
				lastUpdateTime = System.currentTimeMillis();
				dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
				String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
				for ( byte i = 0; i < dots; ++i ) {
					temp += '.';
				}
				setText( temp );
				try {
					// permite que a thread de carregamento dos recursos continue sua execuÃ§Ã£o
					Thread.sleep( CHANGE_TEXT_INTERVAL );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif
				}
			}
		}
	}


	// fim do mÃ©todo update( int )
	public final void paint( Graphics g ) {
		super.paint( g );
		painted = true;
	}


	public final void setActive( boolean a ) {
		active = a;
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( size );
	}
}
