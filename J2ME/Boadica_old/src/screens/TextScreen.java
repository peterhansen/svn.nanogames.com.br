/**
 * TextScreen.java
 *
 * Created on Sep 21, 2010 5:26:46 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.MenuLabel;
import core.TitleLabel;

//#if SCREEN_SIZE != "SMALL"
	import core.ColoredBorder;
//#endif

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public final class TextScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
	//#if TOUCH == "true"
		, PointerListener
	//#endif
{

	private final BasicTextScreen text;

	//#if SCREEN_SIZE != "SMALL"
		private final ColoredBorder border;
	//#endif

	private final TitleLabel logo;
	private final MenuLabel labelBack;
	

	public TextScreen( int backIndex, String t, String title, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( 5 );

		//#if SCREEN_SIZE != "SMALL"
			border = new ColoredBorder();
			insertDrawable( border );

			border.setFill( new Pattern( 0x020929 ) );
		//#endif

		logo = new TitleLabel();
		logo.setText( title, TITLE_WAIT_TIME );
		logo.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( logo );

		text = new BasicTextScreen( backIndex, GameMIDlet.GetFont( FONT_TEXT_WHITE ), t, autoScroll, specialChars );
		if ( !autoScroll ) {
			text.setScrollFull( GameMIDlet.getScrollFull() );
			text.setScrollPage( GameMIDlet.getScrollPage() );
		}

		insertDrawable( text );

		labelBack = new MenuLabel( TEXT_BACK );
		labelBack.setCurrent( true );
		insertDrawable( labelBack );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void keyPressed( int key ) {
		text.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		text.keyReleased( key );
	}


	public final void hideNotify( boolean deviceEvent ) {
		text.hideNotify( deviceEvent );
	}


	public final void showNotify( boolean deviceEvent ) {
		text.showNotify( deviceEvent );
	}

	
	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		logo.setRefPixelPosition( width >> 1, 0 );

		//#if SCREEN_SIZE == "SMALL"
//# 			text.setPosition( 0, logo.getPosY() + logo.getHeight() + TITLE_Y_SPACING );
//# 			text.setSize( width, height - logo.getHeight() - labelBack.getHeight() );
//# 			labelBack.setPosition( ( width - labelBack.getWidth() ) >> 1, height - labelBack.getHeight() );
		//#else
			border.setPosition( 0, logo.getPosY() + logo.getHeight() + TITLE_Y_SPACING);
			border.setSize( width, height - border.getPosY() - labelBack.getHeight() );

			labelBack.setPosition( ( width - labelBack.getWidth() ) >> 1, height - labelBack.getHeight() );

			final Rectangle r = border.getInternalRect();

			text.setPosition( border.getPosX() + r.x, border.getPosY() + r.y );
			text.setSize( r.width, r.height );
		//#endif
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			if ( text.contains( x, y ) )
				text.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			if ( text.contains( x, y ) )
				text.onPointerPressed( x, y );
			else if ( labelBack.contains( x, y ) )
				keyPressed( ScreenManager.KEY_BACK );
		}


		public final void onPointerReleased( int x, int y ) {
			if ( text.contains( x, y ) )
				text.onPointerReleased( x, y );
		}
	//#endif
	
}
