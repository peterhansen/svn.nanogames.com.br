
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.AppMIDlet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author peter
 */
public class FavelaMIDlet extends AppMIDlet {
    
    private static final byte SCREEN_TEST = 0;


    protected int changeScreen( int screen ) throws Exception {
		final FavelaMIDlet midlet = (FavelaMIDlet) instance;
		
		Drawable nextScreen = null;
		
        switch ( screen ) {
            case SCREEN_TEST:
				nextScreen = new TestScreen();
			break;
        }
		
		
		midlet.manager.setBackgroundColor( 0x000000 );
		midlet.manager.setCurrentScreen( nextScreen );
		
		return screen;
    }


    protected void loadResources() throws Exception {
        setScreen( SCREEN_TEST );
    }
    
}
