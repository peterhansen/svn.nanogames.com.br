
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author peter
 */
public final class TestScreen extends UpdatableGroup implements PointerListener {

	private final NodeManager manager = new NodeManager();

	public TestScreen() {
		super( 10 );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		final Node root = new Node( 0xffffff );
		
		final Node fight = new Node( 0xff0000 );
		final Node fight1 = new Node( 0xff9900 );
		final Node fight2 = new Node( 0xff0099 );
		fight.addChild( fight1 );
		fight.addChild( fight2 );
		
		final Node fight11 = new Node( 0xffff00 );
		final Node fight12 = new Node( 0xff00ff );
		fight1.addChild( fight11 );
		fight1.addChild( fight12 );
		
		final Node defense = new Node( 0x00ff00 );
		final Node defense1 = new Node( 0x99ff00 );
		final Node defense2 = new Node( 0x00ff99 );
		defense.addChild( defense1 );
		defense.addChild( defense2 );
		
		final Node move = new Node( 0x0000ff );
		final Node move1 = new Node( 0x9900ff );
		final Node move2 = new Node( 0x0099ff );
		move.addChild( move1 );
		move.addChild( move2 );
		
		root.addChild( fight );
		root.addChild( defense );
		root.addChild( move );
		
		manager.setRoot( root );
		insertDrawable( manager );
	}


	public void onPointerDragged( int x, int y ) {
		manager.onPointerDragged( x, y );
	}


	public void onPointerPressed( int x, int y ) {
		manager.onPointerPressed( x, y );
	}


	public void onPointerReleased( int x, int y ) {
		manager.onPointerReleased( x, y );
	}
	
}
