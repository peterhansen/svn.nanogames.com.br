
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author peter
 */
public final class Node extends UpdatableGroup {
	
	private final byte SIZE = 52;
	
	private final byte SIZE_HALF = SIZE >> 1;

	private final byte SIZE_MAX = SIZE * 3 / 2;
	
	private static final short TIME_ANIMATION = 170;

	private final int color;
	
	private Node parent;
	
	private final Vector children = new Vector();

	private short accTime;
	
	private final BezierCurve bezierSize = new BezierCurve();
	
	private final BezierCurve bezierPosition = new BezierCurve();
	
	public static final byte STATE_HIDDEN = 0;
	
	public static final byte STATE_APPEARING = 1;
	
	public static final byte STATE_SHOWN = 2;
	
	public static final byte STATE_HIDING = 3;
	
	private byte state;
	
	private boolean showChildren;
	
	private short angle;
	
	
	public Node( int color ) {
		super( 20 );
		
		this.color = color;
		
		setState( STATE_HIDDEN );
		setClipTest( false );
	}
	
	
	public final void setParent( Node parent ) {
		this.parent = parent;
	}
	
	
	public final Node getParent() {
		return parent;
	}
	
	
	public final boolean isChildOf( Node n ) {
		Node p = getParent();
		
		while ( p != null && p != n )
			p = p.getParent();
			
		return p != null;
	}
	

	protected void paint( Graphics g ) {
		super.paint( g );
		
		g.setColor( color );
		g.fillArc( translate.x - ( getWidth() >> 1 ), translate.y - ( getHeight() >> 1 ), getWidth(), getHeight(), 0, 360 );
	}
	
	
	public final void addChild( Node child ) {
		child.setParent( this );
		children.addElement( child );
		insertDrawable( child );
		
		System.out.println( "ADD CHILD( " + this + " ) -> " + child + " -> " + children.size() );
	}
	
	
	public final void showChildren() {
		System.out.println( "SHOW CHILDREN( " + this + " ) -> " + children.size() );
		
		if ( children.size() > 0 ) {
			final int ANGLE_TOTAL;
			final int ANGLE_DIFF;
			final int ANGLE_START;
			
			if ( parent == null ) {
				ANGLE_TOTAL = 360;
				ANGLE_DIFF = ANGLE_TOTAL / children.size();
				ANGLE_START = -90;
			} else {
				ANGLE_TOTAL = 180;
				ANGLE_DIFF = ANGLE_TOTAL / ( children.size() + 1 );
				ANGLE_START = angle - ( ANGLE_TOTAL / children.size() + 1 ) + ANGLE_DIFF;
			}
			
			final Point p = new Point();

			for ( int i = 0; i < children.size(); ++i ) {  
				final Node c = ( Node ) children.elementAt( i );
				System.out.println( "CHILD #" + i + " -> " + p );
				
				final short ANGLE = ( short ) ( ANGLE_START + ( i * ANGLE_DIFF ) );
				p.setVector( ANGLE, SIZE * 11 / 10 );
				c.angle = ANGLE;
				c.show( p, false );
			}
		}
	}
	
	
	public final void hideChildren() {
		hideChildren( null );
	}
	
	
	public final void hideChildren( Node exception ) {
		System.out.println( "HIDE CHILDREN( " + this + " )" + children.size() );
		
		for ( int i = 0; i < children.size(); ++i ) {  
			final Node c = ( Node ) children.elementAt( i );
			if ( c != exception )
				c.hide();
		}
	}
	
	
	public final void show( Point target, boolean showChildren ) {
		switch ( state ) {
			case STATE_APPEARING:
			case STATE_SHOWN:
			return;
		}
		
		this.showChildren = showChildren;
		
		System.out.println( "SHOW( " + this + " ): " + target );
		
		bezierPosition.setValues( new Point( target ), new Point( target ), new Point( target ), new Point( target ) );
		setState( STATE_APPEARING );
	}
	
	
	public final void hide() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_HIDING:
			return;
		}
		
		bezierPosition.setValues( new Point( position ), new Point( position ), new Point( position ), new Point( bezierPosition.origin ) );
		setState( STATE_HIDING );
		
		hideChildren();
	}
	
	
	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;
		
		System.out.println( "STATE( " + this + " ): " + state );
		
		switch ( state ) {
			case STATE_HIDDEN:
				hideChildren();
			break;
			
			case STATE_APPEARING:
				setVisible( true );
				bezierSize.setValues( new Point( size ), new Point( SIZE, SIZE ), new Point( SIZE_MAX, SIZE_MAX ), new Point( SIZE, SIZE ) );
			break;
			
			case STATE_HIDING:
				setVisible( true );
				bezierSize.setValues( new Point( size ), new Point( SIZE_MAX, SIZE_MAX ), new Point( SIZE, SIZE ), new Point() );
			break;
			
			case STATE_SHOWN:
				setVisible( true );
				setSize( SIZE, SIZE );
				
				if ( showChildren )
					showChildren();
			break;
		}
	}
	
	
	public final byte getState() {
		return state;
	}


	public final void update( int delta ) {
		super.update( delta );
		
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_SHOWN:
			return;

			case STATE_APPEARING:
			case STATE_HIDING:
				accTime += delta;
				final int FP_PERCENT = NanoMath.divInt( accTime, TIME_ANIMATION );
				bezierSize.getPointAtFixed( size, FP_PERCENT );
				bezierPosition.getPointAtFixed( position, FP_PERCENT );
				
				//System.out.println( NanoMath.toString( FP_PERCENT * 100 ) + "% -> " + position + " -> " + size );

				if ( accTime >= TIME_ANIMATION ) {
					switch ( state ) {
						case STATE_APPEARING:
							setState( STATE_SHOWN );
						break;
							
						case STATE_HIDING:
							setState( STATE_HIDDEN );
						break;
					}
				}
			break;
		}
	}


	public boolean contains( int x, int y ) {
		final int bla = position.distanceTo( new Point( x, y ) );
		return bla <= SIZE_HALF;
	}
	
	
	public Node getNodeAt( int x, int y ) {
		for ( int i = 0; i < children.size(); ++i ) {
			final Node n = ( Node ) children.elementAt( i );
			final Node nChild = n.getNodeAt( x - position.x, y - position.y );
			
			if ( nChild != null ) {
				switch ( nChild.getState() ) {
					case STATE_APPEARING:
					case STATE_SHOWN:
					return nChild;
				}
			}
		}
		
		if ( contains( x, y ) ) {
			System.out.println( "----> " + this );
			return this;
		}
		
		return null;
	}
	
	
	public final String toString() {
		return "Node (" + ( ( color & 0xff0000 ) >> 16 ) + ", " + ( ( color & 0x00ff00 ) >> 8 ) + ", " + ( color & 0x0000ff ) + ")";
	}
	
}
