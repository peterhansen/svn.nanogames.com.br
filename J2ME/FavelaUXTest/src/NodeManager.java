
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.Point;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author peter
 */
public final class NodeManager extends UpdatableGroup implements PointerListener {

	private Node root;
	
	private Node currentNode;
	
	
	public NodeManager() {
		super( 20 );
		
		setSize( Short.MAX_VALUE, Short.MAX_VALUE );
	}
	
	
	public final void setRoot( Node r ) {
		if ( root != null ) {
			removeDrawable( root );
		}
		
		root = r;
		insertDrawable( root );
	}


	public final void onPointerDragged( int x, int y ) {
		if ( root != null ) {
			final Node n = root.getNodeAt( x, y );
			
			if ( n != currentNode && n != null ) {
				if ( currentNode != null ) {
					currentNode.hideChildren( n );
					
					Node p = n.getParent();
					Node temp = n;
					while ( p != null ) {
						p.hideChildren( temp );
						temp = p;
						p = p.getParent();
					}
					
					if ( currentNode.isChildOf( n ) )
						n.hideChildren();
				}
				
				currentNode = n;
				
				n.showChildren();
			}
		}
	}


	public final void onPointerPressed( int x, int y ) {
		if ( root != null ) {
			if ( root.getState() == Node.STATE_HIDDEN ) {
				root.show( new Point( x, y ), true );
			}
		}
	}


	public final void onPointerReleased( int x, int y ) {
		if ( root != null ) {
			if ( root.getState() != Node.STATE_HIDDEN ) {
				root.hide();
			}
		}
	}
	
}
