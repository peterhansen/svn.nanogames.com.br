/**
 * ConnectionTestMidlet.java
 * �2008 Nano Games.
 *
 * Created on 07/08/2008 10:55:00.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.DynamicByteArray;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.SocketConnection;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * @author Daniel L. Alves
 */

public final class ConnectionTestMidlet extends AppMIDlet implements Constants
{
	public ImageFont FONT_DEFAULT;
	public final Drawable[] icons = new Drawable[ 2 ];

	public ConnectionTestMidlet() throws Exception
	{
		loadTexts( TEXT_TOTAL, "/pt.dat" );
	}
	
	protected void destroyApp( boolean unconditional ) throws MIDletStateChangeException
	{
		super.destroyApp( unconditional );
	}
	
	protected void loadResources() throws Exception
	{
		icons[ ICON_BACK ] = new DrawableImage( "/btBack.png" );
		Thread.yield();

		icons[ ICON_OK ] = new DrawableImage( "/btOk.png" );
		Thread.yield();
		
		// Carrega a fonte da aplica��o
		final String path = "/fontText";
		FONT_DEFAULT = ImageFont.createMultiSpacedFont( path + ".png", path + ".dat" );
		FONT_DEFAULT.setCharOffset( 1 );
		Thread.yield();
		
		// Determina uma cor de fundo
		manager.setBackgroundColor( 0 );
		
		// Vai para a tela onde iremos carregar os outros recursos do midlet
		setScreen( SCREEN_MAIN_SCREEN  );
	}

	protected int changeScreen( int screen ) throws Exception
	{
		final ConnectionTestMidlet midlet = ( ConnectionTestMidlet ) instance;

		if( screen != currentScreen )
		{
			Drawable nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;
			
			int softKeyVisibleTime = 0;
			
			try
			{
				switch( screen )
				{
					case SCREEN_MAIN_SCREEN:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;

						nextScreen = new MainScreen( FONT_DEFAULT );
						
						final Pattern full = new Pattern( 0x444444 );
						full.setSize( 4, 0 );
						(( MainScreen )nextScreen ).setScrollFull( full );
						
						final Pattern page = new Pattern( 0xBBBBBB );
						page.setSize( 4, 0 );
						(( MainScreen )nextScreen ).setScrollPage( page );

						break;
				}

				if( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft == SOFT_KEY_REMOVE ? null : icons[ indexSoftLeft ] );

				if( indexSoftRight != SOFT_KEY_DONT_CHANGE )
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight == SOFT_KEY_REMOVE ? null : icons[ indexSoftRight ] );
			}
			catch( Exception e )
			{
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, ent�o sai do jogo
				exit();
			}

			if( nextScreen != null )
			{
				midlet.manager.setCurrentScreen( nextScreen );
				return screen;
			}
			else
			{
				return -1;
			}
		} // fim if ( screen != currentScreen )
		return screen;
	}
	
	private final class MainScreen extends BasicTextScreen implements ConnectionThreadListener
	{
		boolean done;
		ConnectionThread t;
		public MainScreen( ImageFont font ) throws Exception
		{
			super( SCREEN_MAIN_SCREEN, font, "", false );
		}
		
		public void keyPressed( int key )
		{
			switch( key )
			{
				case ScreenManager.KEY_BACK:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
					exit();
					break;

				case ScreenManager.KEY_NUM1:
					if( !done )
					{
						done = true;
						t = new ConnectionThread( this, ConnectionThread.RUN_METHOD_SOCKET );
						t.start();
					}
					break;
					
				case ScreenManager.KEY_NUM2:
					if( !done )
					{
						done = true;
						t = new ConnectionThread( this, ConnectionThread.RUN_METHOD_HTTP_GET );
						t.start();
					}
					break;
					
				case ScreenManager.KEY_NUM3:
					if( !done )
					{
						done = true;
						t = new ConnectionThread( this, ConnectionThread.RUN_METHOD_HTTP_POST );
						t.start();
					}
					break;
					
				case ScreenManager.KEY_NUM4:
					if( !done )
					{
						done = true;
						t = new ConnectionThread( this, ConnectionThread.RUN_METHOD_HTTP_DIRECT );
						t.start();
					}
					break;
					
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.FIRE:
					return;
//					if( !done )
//					{
//						done = true;
//						t = new ConnectionThread( this );
//						t.start();
//					}
//					break;
					
				default:
					super.keyPressed( key );
					break;
			}
		}

		public synchronized void onError( String error )
		{
			onMessage( error );
		}
		
		public synchronized void onEnd( String message )
		{
			onMessage( message );
			done = false;
		}

		public synchronized void onMessage( String message )
		{
			final RichLabel l = ( RichLabel )drawables[0];
			l.setText( l.getText() + "- " + message + "\n", true );
			
			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			updateScroll();
			
			Thread.yield();
		}
	}
	
	private final static class ConnectionThread extends Thread
	{
		private final byte RUN_METHOD;
		private final ConnectionThreadListener listener;
		
		public static final byte RUN_METHOD_SOCKET		=	0;
		public static final byte RUN_METHOD_HTTP_GET	=	1;
		public static final byte RUN_METHOD_HTTP_POST	=	2;
		public static final byte RUN_METHOD_HTTP_DIRECT	 =  3;
		
		public ConnectionThread( ConnectionThreadListener listener, byte runMethod )
		{
			this.listener = listener;
			RUN_METHOD = runMethod;
		}
		
		public void run()
		{
			switch( RUN_METHOD )
			{
				case RUN_METHOD_SOCKET:
					runSocket();
					break;
					
				case RUN_METHOD_HTTP_GET:
					runHttpGet();
					break;
					
				case RUN_METHOD_HTTP_POST:
					runHttpPost();
					break;
					
				case RUN_METHOD_HTTP_DIRECT:
					runHttpDirect();
					break;
			}
		}
		
		public void runSocket()
		{
			SocketConnection socket = null;
			InputStream is = null;
			OutputStream os = null;
			
			try
			{
				Class.forName( "javax.microedition.io.SocketConnection" );
			}
			catch( Exception ex )
			{
				listener.onMessage( "N�o suporta socket" );
				return;
			}
			
			listener.onMessage( "Utilizando socket" );

			try
			{
				socket = ( SocketConnection ) Connector.open( "socket://200.201.187.54:8800", Connector.READ_WRITE, false );
				//socket = ( SocketConnection ) Connector.open( "socket://localhost:8800", Connector.READ_WRITE, false );
				
				listener.onMessage( "Abriu a conex�o" );
				
				socket.setSocketOption( SocketConnection.LINGER, 5 );

				listener.onMessage( "Setou linger time" );

				final String data = "target=TOMMY";
				final String pack =	"GET / HTTP/1.0\r\n" +
//									"From: test@nanogames.com.br\r\n" +
									"User-Agent: Profile/MIDP-2.0 Configuration/CLDC-1.0\r\n" +
									"Connection: close\r\n" +
									"Content-Type: application/x-www-form-urlencoded\r\n" +
									"Content-Language: en-US" +
//									"Content-Length: " + data.length() + //"\r\n" +
									"\r\n\r\n" +
									data +
									"\r\n\r\n";

				os = socket.openDataOutputStream();

				listener.onMessage( "Abriu a outputstream" );

				os.write( pack.getBytes() );
				os.flush();
				os.close();
				
				listener.onMessage( "Enviou a mensagem" );

				is = socket.openDataInputStream();
				
				listener.onMessage( "Abriu a input stream" );

				DynamicByteArray array = new DynamicByteArray();
				array.readInputStream( is );
					
				process( array.getData() );
				
				listener.onMessage( "Leu os dados" );
			}
			catch( ClassCastException ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "A url utilizada n�o segue o padr�o HTTP" );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
				listener.onError( "Exce��o: " + ex.getClass() + " - "  + ex.getMessage() );
			}
			finally
			{
				if( is != null )
				{
					try
					{
						is.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a input stream" );
					}
				}

				if( os != null )
				{
					try
					{
						os.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a output stream" );
					}
				}
				
				if( socket != null )
				{
					try
					{
						socket.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a conex�o" );
					}
				}
			}
			listener.onEnd( "Conex�o terminada" );
		}
		
		public void runHttpDirect()
		{
			InputStream is = null;
			
			try
			{
				Class.forName( "javax.microedition.io.HttpConnection" );
			}
			catch( Exception ex )
			{
				listener.onMessage( "N�o suporta HttpConnection" );
				return;
			}
			
			listener.onMessage( "Usando HTTP Direct" );

			try
			{
				is = Connector.openDataInputStream( "http://jad.pitecohansen.com/t/redir.php?target=TOMMY" );
				
				listener.onMessage( "Abriu a input stream" );

				// Get the length and process the data
				listener.onMessage( "Length Available: " + is.available() );
				
				DynamicByteArray array = new DynamicByteArray();
				array.readInputStream( is );
					
				process( array.getData() );
				
				listener.onMessage( "Leu os dados" );
			}
			catch( ClassCastException ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "A url utilizada n�o segue o padr�o HTTP" );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "Exce��o: " + ex.getClass() + " - " + ex.getMessage() );
			}
			finally
			{
				if( is != null )
				{
					try
					{
						is.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a input stream" );
					}
				}
			}
			listener.onEnd( "Conex�o terminada" );
		}
		
		public void runHttpGet()
		{
			HttpConnection c = null;
			InputStream is = null;
			
			final int RC_DEFAULT_VALUE = -9378; // qualquer coisa
			int rc = RC_DEFAULT_VALUE;
			
			try
			{
				Class.forName( "javax.microedition.io.HttpConnection" );
			}
			catch( Exception ex )
			{
				listener.onMessage( "N�o suporta HttpConnection" );
				return;
			}
			
			listener.onMessage( "Usando HTTP Get" );

			try
			{
				String params = "target=TOMMY";
				c = ( HttpConnection ) Connector.open( "http://jad.pitecohansen.com/t/redir.php?" + params, Connector.READ_WRITE, false );
				
				listener.onMessage( "Abriu a conex�o" );

				// Set the request method and headers
				c.setRequestMethod( HttpConnection.GET );
				c.setRequestProperty( "User-Agent", "Profile/MIDP-2.0 Configuration/CLDC-1.0" );
				c.setRequestProperty( "Content-Language", "en-US" );
				c.setRequestProperty( "Connection", "close" );

				// Getting the response code will open the connection,
				// send the request, and read the HTTP response headers.
				// The headers are stored until requested.
				try
				{
					rc = c.getResponseCode();
				}
				catch( Exception bla )
				{
					if( rc == RC_DEFAULT_VALUE )
						throw new IOException( "N�o conseguiu obter o c�digo de resposta: " + bla.getClass() + " - " + bla.getMessage() );
					
					else if( rc != HttpConnection.HTTP_OK )
						throw new IOException( "HTTP response code: " + rc );
				}
				
				listener.onMessage( "Recebeu a resposta" );

				is = c.openDataInputStream();
				
				listener.onMessage( "Abriu a input stream" );

//				String type = c.getType();
//				processType( type );

				// Get the length and process the data
				final int len = ( int ) c.getLength();
				if( len > 0 )
				{
					int actual = 0;
					int bytesread = 0;
					final byte[] data = new byte[len];
					while( ( bytesread != len ) && ( actual != -1 ) )
					{
						actual = is.read( data, bytesread, len - bytesread );
						bytesread += actual;
					}
					process( data );
				}
				else
				{
					DynamicByteArray array = new DynamicByteArray();
					array.readInputStream( is );
					
					process( array.getData() );
				}
				
				is.read();
				
				listener.onMessage( "Leu os dados" );
			}
			catch( ClassCastException ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "A url utilizada n�o segue o padr�o HTTP" );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "Exce��o: " + ex.getClass() + " - " + ex.getMessage() );
			}
			finally
			{
				if( is != null )
				{
					try
					{
						is.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a input stream" );
					}
				}

				if( c != null )
				{
					try
					{
						c.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a conex�o" );
					}
				}
			}
			listener.onEnd( "Conex�o terminada" );
		}
		
		public void runHttpPost()
		{
			HttpConnection c = null;
			InputStream is = null;
			OutputStream os = null;
			
			final int RC_DEFAULT_VALUE = -9378; // qualquer coisa
			int rc = RC_DEFAULT_VALUE;
			
			try
			{
				Class.forName( "javax.microedition.io.HttpConnection" );
			}
			catch( Exception ex )
			{
				listener.onMessage( "N�o suporta HttpConnection" );
				return;
			}
			
			listener.onMessage( "Usando HTTP Post" );

			try
			{
				String params = "target=TOMMY";
				c = ( HttpConnection ) Connector.open( "http://jad.pitecohansen.com/t/redir.php", Connector.READ_WRITE, false );
				
				listener.onMessage( "Abriu a conex�o" );

				// Set the request method and headers
				c.setRequestMethod( HttpConnection.POST );
				c.setRequestProperty( "User-Agent", "Profile/MIDP-2.0 Configuration/CLDC-1.0" );
				c.setRequestProperty( "Content-Language", "en-US" );
				c.setRequestProperty( "Connection", "close" );
				c.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded" );
				c.setRequestProperty( "Content-Length", String.valueOf( params.length() ) );
				
				os = c.openDataOutputStream();
				os.write( params.getBytes() );
				
				// N�o deve-se chamar output.flush() para evitar problemas com alguns aparelhos
				// http.getResponseCode() implicitamente chama flush()

				// Getting the response code will open the connection,
				// send the request, and read the HTTP response headers.
				// The headers are stored until requested.
				try
				{
					rc = c.getResponseCode();
				}
				catch( Exception bla )
				{
					if( rc == RC_DEFAULT_VALUE )
						throw new IOException( "N�o conseguiu obter o c�digo de resposta: " + bla.getClass() + " - " + bla.getMessage() );
					
					else if( rc != HttpConnection.HTTP_OK )
						throw new IOException( "HTTP response code: " + rc );
				}
				
				listener.onMessage( "Recebeu a resposta" );

				is = c.openDataInputStream();
				
				listener.onMessage( "Abriu a input stream" );

//				String type = c.getType();
//				processType( type );

				// Get the length and process the data
				final int len = ( int ) c.getLength();
				if( len > 0 )
				{
					int actual = 0;
					int bytesread = 0;
					final byte[] data = new byte[len];
					while( ( bytesread != len ) && ( actual != -1 ) )
					{
						actual = is.read( data, bytesread, len - bytesread );
						bytesread += actual;
					}
					process( data );
				}
				else
				{
					DynamicByteArray array = new DynamicByteArray();
					array.readInputStream( is );
					
					process( array.getData() );
				}
				
				is.read();
				
				listener.onMessage( "Leu os dados" );
			}
			catch( ClassCastException ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "A url utilizada n�o segue o padr�o HTTP" );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif

				listener.onError( "Exce��o: " + ex.getClass() + " - " + ex.getMessage() );
			}
			finally
			{
				if( is != null )
				{
					try
					{
						is.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a input stream" );
					}
				}

				if( os != null )
				{
					try
					{
						os.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a output stream" );
					}
				}

				if( c != null )
				{
					try
					{
						c.close();
					}
					catch( Exception ex )
					{
						listener.onMessage( "N�o conseguiu fechar a conex�o" );
					}
				}
			}
			listener.onEnd( "Conex�o terminada" );
		}
			
		private void process( byte[] data )
		{
			listener.onMessage( new String( data ) );
		}

		private void processType( String type )
		{
			// Faz alguma coisa com os dados recebidos...
		}
	}
	
	private interface ConnectionThreadListener
	{
		public void onError( String error );
		
		public void onEnd( String message );
		
		public void onMessage( String message );
	}
}