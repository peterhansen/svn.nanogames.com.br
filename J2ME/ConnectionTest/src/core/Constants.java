package core;

/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on 22/07/2008 18:48:07.
 */

/**
 *
 * @author Daniel L. Alves
 */

public interface Constants
{
	public static byte SCREEN_MAIN_SCREEN	= 0;
		
	public static byte TEXT_TEST_SMS		= 0;
	public static byte TEXT_EXIT			= 1;

	public static byte TEXT_TOTAL			= 2;
	
	public static byte ICON_BACK = 0;
	public static byte ICON_OK = 1;
}