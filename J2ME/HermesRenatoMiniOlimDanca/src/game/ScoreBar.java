/**
 * ScoreBar.java
 * �2008 Nano Games.
 *
 * Created on 13/06/2008 12:53:08.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import core.Constants;
import core.GameMIDlet;
import core.LongMUV;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class ScoreBar extends UpdatableGroup implements Constants
{
	/** Label que exibe os pontos do jogador */
	private final Label scoreLabel;
	
	/** Pontua��o atual */
	private long score;
	
	/** Pontua��o que est� sendo exibida no placar */
	private long scoreShown;

	/** Objeto para controlar a velocidade de atualiza��o da pontua��o */
	private final LongMUV scoreSpeed = new LongMUV();
	
	// CANCELED: Nesse jogo a pontua��o n�o diminui
	/** Indica se os pontos est�o subindo ou descendo. Auxiliar de anima��o */
	//private boolean scoreGoingUp;
	
	/** Dist�ncia do label dos pontos para as bordas da barra */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte LABEL_DIST_FROM_BORDER = 2;
	//#else
//# 		private final byte LABEL_DIST_FROM_BORDER = 1;
	//#endif
	
	/** Sombra que fica abaixo do label dos pontos */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte LABEL_SHADOW_HEIGHT = 1;
	//#else
//# 		private final byte LABEL_SHADOW_HEIGHT = 1;
	//#endif
	
	/** Espa�amento entre os caracteres da fonte de pontos */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte FONT_POINTS_CHAR_OFFSET = 1;
	//#else
//# 		private final byte FONT_POINTS_CHAR_OFFSET = 1;
	//#endif
	
	/** Posi��o Y e largura da sombra que fica abaixo do label dos pontos */
	private final short shadowY, shadowWidth;
	
	/** Quantos n�meros possu�mos na pontua��o m�xima */
	private final byte nNumbers = ( byte ) String.valueOf( MAX_SCORE ).length();
	
	//<editor-fold defaultstate="collapsed" desc="�ndice dos elementos do grupo">
	
	public static final byte INDEX_BKG			= 0;
	public static final byte INDEX_TOP_BAR		= 1;
	public static final byte INDEX_RIGHT_BAR	= 2;
	public static final byte INDEX_SCORE_LABEL	= 3;
	
	//</editor-fold>
	
	/** Construtor */
	public ScoreBar() throws Exception
	{
		super( 4 );
		
		// Aloca os elementos do grupo
		final Pattern bkg = new Pattern( 0 );
		insertDrawable( bkg );
		Thread.yield();

		final DrawableImage barImg = new DrawableImage( ROOT_DIR + "bar" + IMG_EXT );
		Thread.yield();
		
		final Pattern topBar = new Pattern( barImg );
		insertDrawable( topBar );
		Thread.yield();
		
		final DrawableImage rightBar = new DrawableImage( ROOT_DIR + "numberBarStroke" + IMG_EXT );
		insertDrawable( rightBar );
		Thread.yield();

		final String path = "fontPoints";
		final ImageFont fontPoints = ImageFont.createMultiSpacedFont( ROOT_DIR + path + IMG_EXT, ROOT_DIR + path + FONT_DESC_EXT );
		fontPoints.setCharOffset( FONT_POINTS_CHAR_OFFSET );
		Thread.yield();
		
		scoreLabel = new Label( fontPoints, GameMIDlet.formatNumStr( 0, nNumbers, '0', '.' ) );
		insertDrawable( scoreLabel );
		
		// Determina o tamanho do grupo e das patterns
		final int rightBarWidth = rightBar.getWidth();
		final int groupWidth = scoreLabel.getWidth() + ( LABEL_DIST_FROM_BORDER << 1 ) + rightBarWidth;
		final int bkgHeight = scoreLabel.getHeight() + ( LABEL_DIST_FROM_BORDER << 1 );
		final int topBarHeight = barImg.getHeight();
		setSize( groupWidth, bkgHeight + topBarHeight + LABEL_SHADOW_HEIGHT );

		shadowWidth = ( short )( groupWidth - rightBarWidth );
		topBar.setSize( shadowWidth, topBarHeight );
		bkg.setSize( shadowWidth, bkgHeight );
		
		// Posiciona os elementos da cole��o
		bkg.setPosition( 0, topBarHeight );
		shadowY = ( short )( topBarHeight + bkgHeight );
		rightBar.setPosition( groupWidth - rightBarWidth, 0 );
		scoreLabel.setPosition( LABEL_DIST_FROM_BORDER, topBarHeight + LABEL_DIST_FROM_BORDER );
	}
	
	public void changeScore( long points )
	{
		// Estourou a precis�o, pois nesse jogo a pontua��o nunca diminui
		if( points < 0 )
			return;
		
		// Estourou a precis�o, pois nesse jogo a pontua��o nunca diminui
		final long nextScore = score + points;
		if( nextScore < score )
		{
			score = MAX_SCORE;
		}
		else
		{
			score = nextScore;

			if( score > MAX_SCORE )
				score = MAX_SCORE;
			else if( score < 0 )
				score = 0;
		}

		final long diff = score - scoreShown;
		
		// Opa! Houve um erro...
		if( diff < 0 )
			return;
		
		// CANCELED: Nesse jogo a pontua��o n�o diminui
		//scoreGoingUp = diff >= 0 ? true : false;
		
		scoreSpeed.setSpeed( diff, false );
	}

	public void update( int delta )
	{
		// Atualiza a pontua��o
		updateScore( delta, false );
	}

	protected void paint( Graphics g )
	{
		super.paint( g );

		// Desenha a sombra do label
		g.setColor( 0xFF7D8582 );
		g.fillRect( translate.x + 0, translate.y + shadowY, shadowWidth, LABEL_SHADOW_HEIGHT );
	}
	
	public long getScore()
	{
		return score;
	}
	
	/**
	 * Atualiza o label da pontua��o
	 * @param delta Tempo decorrido desde a �ltima chama ao m�todo
	 * @param changeNow Indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real
	 */
	public final void updateScore( int delta, boolean changeNow )
	{
		if( scoreShown != score )
		{
			if( changeNow )
			{
				scoreShown = score;
			}
			else
			{
				final long dp = scoreSpeed.update( delta );
				
				// Opa! Houve um erro...
				if( dp < 0 )
				{
					scoreShown = score;
				}
				else
				{
					final long nextScoreShown = scoreShown + dp;
					
					// CANCELED: Nesse jogo a pontua��o n�o diminui
					//if( ( scoreGoingUp && ( scoreShown > score ) ) || ( !scoreGoingUp && ( scoreShown < score ) ) )
					if( ( nextScoreShown < scoreShown ) || ( nextScoreShown >= score ) )
						scoreShown = score;
					else
						scoreShown = nextScoreShown;
				}
			}
			scoreLabel.setText( GameMIDlet.formatNumStr( scoreShown, nNumbers, '0', '.' ) );
		}
	}
}
