/**
 * PushItBar.java
 * �2008 Nano Games.
 *
 * Created on 04/06/2008 16:57:07.
 */

package game;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class PushItBar extends UpdatableGroup implements KeyListener, Constants, SpriteListener
{
	/** Caixas que informam o que o usu�rio deve apertar */
	public final DrawableImage pushIt2, pushIt4, pushIt6, pushIt8, pushItQm, pushItStar, pushItX;
	
	/** Rastros das caixas */
	public final DrawableImage trail2, trail4, trail6, trail8;
	
	/** Imagens em escala de cinza */
	// OBS: O certo seria termos um grayTrail para cada bot�o (2,4,6,8), mas para economizarmos espa�o,
	// utilizamos somente um
	public final DrawableImage gray2, gray4, gray6, gray8, grayQm, grayStar, grayX, grayTrail;
	
	/** Quanto do rastro fica por de tr�s da caixa */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		public static final byte PUSHIT_TRAIL_OFFSET = 3;
		private static final byte PUSHIT_DIST_FROM_BAR = 6;
	//#else
//# 		public static final byte PUSHIT_TRAIL_OFFSET = 2;
//# 		private static final byte PUSHIT_DIST_FROM_BAR = 6;
	//#endif
	
	/** �rea da mira por onde os pushIts passam e ganham cor */
	public final Rectangle COLORED_PUSHITS_VIEWPORT = new Rectangle();
	
	/** N�mero de pushIts na fase */
	private short nLevelPushIts;
	
	/** N�mero de pushIts acert�veis ao longo da fase */
	private short nHitablePushIts;
	
	/** N�mero de pushIts que j� sa�ram da tela*/
	private short nInactivePushIts;

	/** Controla a anima��o dos pushIts */
	private final MUV pushItSpeedControl = new MUV();
	
	/** Mira */
	private final Sprite aim;
	
	/** Brilho exibido quando o usu�rio tenta acertar um pushIt */
	private final Sprite shine;
	
	/** Objeto que receber� informa��es sobre a barra ao longo do jogo */
	private PushItBarListener listener;
	
	/** Tempo j� decorrido do b�nus da estrela */
	private int starBonusCounter;
	
	/** Tempo de dura��o do b�nus da estrela */
	private int starBonusTime;
	
	/** Velocidade de movimenta��o dos pushIts com o b�nus da estrela */
	private int starBonusSpeed;
	
	/** Velocidade de movimenta��o dos pushIts antes do b�nus da estrela */
	private int speedBeforeStarBonus;
	
	/** PushIt que est� sendo consumido no momento */
	private int currPushIt = -1;
	
	/** Controla de quanto em quanto tempo fornecemos pontos quando o jogador est� acertando um rastro */
	private short trailCounter;
	
	/** �rea de colis�o da mira com os pushIts */
	private final Rectangle aimCollisionArea;
	
	//<editor-fold defaultstate="collapsed" desc="�ndice dos elementos do grupo">
	
	public static final byte INDEX_BAR_TOP		= 0;
	public static final byte INDEX_BAR_BOTTOM	= 1;
	public static final byte INDEX_BKG			= 2;
	public static final byte INDEX_FIRST_PUSHIT	= 3;
	public static final short INDEX_SHINE		= INDEX_FIRST_PUSHIT + MAX_PUSHITS;
	public static final short INDEX_AIM			= INDEX_SHINE + 1;
	public static final short INDEX_ARROW		= INDEX_AIM + 1;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Tipos de acerto que podem ocorrer">
	
	public static final byte PUSHIT_HIT_KEEP_TRAIL	= -2;
	public static final byte PUSHIT_HIT_MISS		= -1;
	public static final byte PUSHIT_HIT_PERFECT		=  0;
	public static final byte PUSHIT_HIT_GREAT		=  1;
	public static final byte PUSHIT_HIT_GOOD		=  2;
	public static final byte PUSHIT_HIT_AVERAGE		=  3;
	public static final byte PUSHIT_HIT_BAD			=  4;
	
	public static final byte PUSHIT_N_TRUE_HIT_TYPES =  5;
	
	public static final byte PUSHIT_N_HIT_TYPES =  6;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Vers�es da barra de pushIts">
	
	public static final byte PUSHIT_BAR_VERSION_FULL	= 0;
	public static final byte PUSHIT_BAR_VERSION_HELP	= 1;
	
	//</editor-fold>
	
	/** Construtor */
	public PushItBar( Drawable borderPatternFill, PushItBarListener listener ) throws Exception
	{
		super( MAX_PUSHITS + 6 );
		
		this.listener = listener;
		
		// Imagens originais
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final boolean hasPointerEvents = ScreenManager.getInstance().hasPointerEvents();
			final String PATH_2 = ROOT_DIR + ( hasPointerEvents ? "ts2" : "2" ) + IMG_EXT;
		//#else
//# 		final String PATH_2 = ROOT_DIR + "2" + IMG_EXT;
		//#endif
		pushIt2 = new DrawableImage( PATH_2 );
		Thread.yield();

		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final String PATH_4 = ROOT_DIR + ( hasPointerEvents ? "ts4" : "4" ) + IMG_EXT;
		//#else
//# 		final String PATH_4 = ROOT_DIR + "4" + IMG_EXT;
		//#endif
		pushIt4 = new DrawableImage( PATH_4 );
		Thread.yield();
		
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final String PATH_6 = ROOT_DIR + ( hasPointerEvents ? "ts6" : "6" ) + IMG_EXT;
		//#else
//# 		final String PATH_6 = ROOT_DIR + "6" + IMG_EXT;
		//#endif
		pushIt6 = new DrawableImage( PATH_6 );
		Thread.yield();
		
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final String PATH_8 = ROOT_DIR + ( hasPointerEvents ? "ts8" : "8" ) + IMG_EXT;
		//#else
//# 		final String PATH_8 = ROOT_DIR + "8" + IMG_EXT;
		//#endif
		pushIt8 = new DrawableImage( PATH_8 );
		Thread.yield();
		
		final String PATH_QUESTION_MARK = ROOT_DIR + "q" + IMG_EXT;
		pushItQm = new DrawableImage( PATH_QUESTION_MARK );
		Thread.yield();
		
		final String PATH_STAR = ROOT_DIR + "s" + IMG_EXT;
		pushItStar = new DrawableImage( PATH_STAR );
		Thread.yield();
		
		final String PATH_X = ROOT_DIR + "x" + IMG_EXT;
		pushItX = new DrawableImage( PATH_X );
		Thread.yield();
		
		// Rastros
		final String PATH_TRAIL = ROOT_DIR + "t" + IMG_EXT;
		trail8 = new DrawableImage( PATH_TRAIL );

		PaletteChanger aux = new PaletteChanger( PATH_TRAIL );
		PaletteMap[] map = { new PaletteMap(  20, 190,  16, 255, 195,   5 ),
							 new PaletteMap(  67, 218,  63, 255, 219,   5 ),
							 new PaletteMap( 102, 225,  99, 255, 234,  52 ),
							 new PaletteMap( 101, 212,  99, 255, 234,  56 ),
							 new PaletteMap( 133, 237, 138, 255, 251, 162 ),
							 new PaletteMap( 144, 233, 142, 254, 255,  90 ),
							 new PaletteMap( 166, 242, 170, 255, 253, 205 ),
							 new PaletteMap( 159, 236, 161, 254, 251, 183 ),
							 new PaletteMap( 114, 235, 119, 255, 251, 149 ),
							 new PaletteMap(  85, 230,  92, 255, 250, 132 ),
							 new PaletteMap(  67, 226,  73, 255, 249,  94 ),
							 new PaletteMap(  58, 224,  64, 255, 246,   5 ),
							 new PaletteMap( 168, 255, 175, 255, 253, 205 ),
							 new PaletteMap( 201, 255, 205, 255, 253, 219 ),
							 new PaletteMap(  12, 207,   7, 255, 206,  52 ) };

		Thread.yield();
		trail2 = new DrawableImage( aux.createImage( map ) );
		Thread.yield();
		
		map[0] = new PaletteMap(  20, 190,  16,   0, 153, 255 );
		map[1] = new PaletteMap(  67, 218,  63,   0, 189, 255 );
		map[2] = new PaletteMap( 102, 225,  99,  49, 199, 255 );
		map[3] = new PaletteMap( 101, 212,  99,  55, 199, 253 );
		map[4] = new PaletteMap( 133, 237, 138, 136, 233, 255 );
		map[5] = new PaletteMap( 144, 233, 142, 115, 229, 255 );
		map[6] = new PaletteMap( 166, 242, 170, 171, 239, 255 );
		map[7] = new PaletteMap( 159, 236, 161, 154, 236, 255 );
		map[8] = new PaletteMap( 114, 235, 119, 111, 228, 255 );
		map[9] = new PaletteMap(  85, 230,  92,  84, 223, 255 );
		map[10] = new PaletteMap(  67, 226,  73,  48, 216, 255 );
		map[11] = new PaletteMap(  58, 224,  64,   0, 207, 255 );
		map[12] = new PaletteMap( 168, 255, 175, 197, 241, 255 );
		map[13] = new PaletteMap( 201, 255, 205, 215, 245, 254 );
		map[14] = new PaletteMap(  12, 207,   7,  42, 170, 255 );
		
		Thread.yield();
		trail4 = new DrawableImage( aux.createImage( map ) );
		Thread.yield();
		
		map[0] = new PaletteMap(  20, 190,  16, 255, 108,   0 );
		map[1] = new PaletteMap(  67, 218,  63, 251, 180,  55 );
		map[2] = new PaletteMap( 102, 225,  99, 255, 185,  63 );
		map[3] = new PaletteMap( 101, 212,  99, 255, 185,  62 );
		map[4] = new PaletteMap( 133, 237, 138, 255, 208, 124 );
		map[5] = new PaletteMap( 144, 233, 142, 255, 197,  97 );
		map[6] = new PaletteMap( 166, 242, 170, 252, 228, 152 );
		map[7] = new PaletteMap( 159, 236, 161, 255, 210, 132 );
		map[8] = new PaletteMap( 114, 235, 119, 255, 196,  92 );
		map[9] = new PaletteMap(  85, 230,  92,  255, 187,  69 );
		map[10] = new PaletteMap(  67, 226,  73, 255, 173,  29 );
		map[11] = new PaletteMap(  58, 224,  64, 255, 162,   0 );
		map[12] = new PaletteMap( 168, 255, 175, 252, 228, 152 );
		map[13] = new PaletteMap( 201, 255, 205, 254, 237, 182 );
		map[14] = new PaletteMap(  12, 207,   7, 255, 173,  29 );

		Thread.yield();
		trail6 = new DrawableImage( aux.createImage( map ) );
		Thread.yield();
		
		map = null;
		aux = null;
		System.gc();

		// Imagens em tons de cinza
		aux = new PaletteChanger( PATH_2 );
		gray2 = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_4 );
		gray4 = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_6 );
		gray6 = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_8 );
		gray8 = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_QUESTION_MARK );
		grayQm = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_STAR );
		grayStar = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_X );
		grayX = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = new PaletteChanger( PATH_TRAIL );
		grayTrail = new DrawableImage( aux.createImage( 0xFFFFFFFF, 0xFF000000 ) );
		Thread.yield();
		
		aux = null;
		System.gc();
		
		// Aloca e posiciona os elementos
		final int groupHeight = build( this, borderPatternFill, pushIt2, PUSHIT_BAR_VERSION_FULL );
		
		shine = ( Sprite )drawables[ INDEX_SHINE ];
		aim = ( Sprite )drawables[ INDEX_AIM ];
		
		// Determina o viewport da mira
		COLORED_PUSHITS_VIEWPORT.x = 0;
		COLORED_PUSHITS_VIEWPORT.y = 0;
		COLORED_PUSHITS_VIEWPORT.width = ScreenManager.SCREEN_WIDTH - pushIt2.getWidth() - 1;
		COLORED_PUSHITS_VIEWPORT.height = groupHeight;
		
		final int pushItWidth = pushIt2.getWidth();
		final int aimAreaX = aim.getPosX() + ( ( aim.getWidth() - pushItWidth ) >> 1 );
		aimCollisionArea = new Rectangle( aimAreaX, pushIt2.getPosY(), pushItWidth, pushIt2.getHeight() );
	}
	
	public Rectangle getColoredPushItsViewport()
	{
		return COLORED_PUSHITS_VIEWPORT;
	}
	
	private static int build( UpdatableGroup group, Drawable borderPatternFill, Drawable basePushIt, byte version ) throws Exception
	{
		final int groupFinalWidth = version == PUSHIT_BAR_VERSION_FULL ? ScreenManager.SCREEN_WIDTH : ScreenManager.SCREEN_HALF_WIDTH;
			
		// Cria as bordas da barra
		final int barFrameHeight = borderPatternFill.getHeight();
		final Pattern topBorder = new Pattern( borderPatternFill );
		group.insertDrawable( topBorder );
		topBorder.setSize( groupFinalWidth, barFrameHeight );
		Thread.yield();

		final DrawableImage mirroredBorder = new DrawableImage( ROOT_DIR + "bar2" + IMG_EXT );
		final Pattern bottomBorder = new Pattern( mirroredBorder );
		group.insertDrawable( bottomBorder );
		bottomBorder.setSize( groupFinalWidth, barFrameHeight );
		Thread.yield();
		
		// Cria o fundo da barra
		final int barBkgHeight = basePushIt.getHeight() + ( PUSHIT_DIST_FROM_BAR << 1 ); 
		final Pattern barBkg = new Pattern( 0 );
		group.insertDrawable( barBkg );
		barBkg.setSize( groupFinalWidth, barBkgHeight );
		
		// Cria todos os pushIts
		if( version == PUSHIT_BAR_VERSION_FULL )
		{
			for( int i = 0 ; i < MAX_PUSHITS ; ++i )
			{
				final PushIt pushIt = new PushIt();
				group.insertDrawable( pushIt );
				pushIt.setVisible( false );
				Thread.yield();
			}
		}
		else
		{
			group.insertDrawable( basePushIt );
		}
		
		// Cria o brilho
		final String PATH_SHINE = ROOT_DIR + "shine";
		final Sprite shineAux = new Sprite( PATH_SHINE + SPRITE_DESC_EXT, PATH_SHINE );
		group.insertDrawable( shineAux );
		shineAux.setVisible( false );

		if( group instanceof SpriteListener )
			shineAux.setListener( ( SpriteListener )group, INDEX_SHINE );

		Thread.yield();
		
		// Cria a mira
		final String PATH_AIM = ROOT_DIR + "aim";
		final Sprite aimAux = new Sprite( PATH_AIM + SPRITE_DESC_EXT, PATH_AIM );
		group.insertDrawable( aimAux );
		Thread.yield();
		
		// Cria a seta que indica a mira
		final Drawable arrow = createArrow();
		group.insertDrawable( arrow );
		
		// Determina o tamanho da cole��o
		final int arrowHeight = arrow.getHeight();
		final int groupHeight = barBkgHeight + barFrameHeight + arrowHeight;
		group.setSize( groupFinalWidth, groupHeight );

		// Posiciona os elementos da cole��o
		barBkg.setPosition( 0, arrowHeight );
		topBorder.setPosition( 0, arrowHeight - barFrameHeight );
		bottomBorder.setPosition( 0, groupHeight - barFrameHeight );
		if( version == PUSHIT_BAR_VERSION_FULL )
		{
			aimAux.setPosition( groupFinalWidth - aimAux.getWidth() - basePushIt.getWidth(), arrowHeight + (( barBkgHeight - aimAux.getHeight() ) >> 1 ) );
			shineAux.setPosition( aimAux.getPosX() + (( aimAux.getWidth() - shineAux.getWidth() ) >> 1 ), aimAux.getPosY() + (( aimAux.getHeight() - shineAux.getHeight() ) >> 1 ));
			arrow.setPosition( aimAux.getPosX() + (( aimAux.getWidth() - arrow.getWidth()) >> 1 ), 0 );
		}
		else
		{
			arrow.setPosition( ( groupFinalWidth - arrow.getWidth() ) >> 1, 0 );
			aimAux.setPosition( ( groupFinalWidth - aimAux.getWidth() ) >> 1, arrowHeight + (( barBkgHeight - aimAux.getHeight() ) >> 1 ) );
			shineAux.setPosition( ( groupFinalWidth - shineAux.getWidth() ) >> 1, aimAux.getPosY() + (( aimAux.getHeight() - shineAux.getHeight() ) >> 1 ));
			basePushIt.setPosition( -basePushIt.getWidth(), aimAux.getPosY() + (( aimAux.getHeight() - shineAux.getHeight() ) >> 1 ) );
		}
		return groupHeight;
	}

	/** Deve ser passado um grupo com no m�nimo 7 slots */
	public static void createHelpVersion( UpdatableGroup group ) throws Exception
	{
		final DrawableImage barImg = new DrawableImage( ROOT_DIR + "bar" + IMG_EXT );
		Thread.yield();
		
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final String path = ROOT_DIR + ( ScreenManager.getInstance().hasPointerEvents() ? "ts" : "" ) + "2";
		//#else
//# 		final String path = ROOT_DIR + "2";
		//#endif
								
		final DrawableImage basePushIt = new DrawableImage( path + IMG_EXT );
		Thread.yield();
		
		build( group, barImg, basePushIt, PUSHIT_BAR_VERSION_HELP );
		Thread.yield();
	}
	
	/** Libera mem�ria */
	public void destruct()
	{	
		shine.setListener( null, 0 );
		currPushIt = -1;
		listener = null;
		System.gc();
		Thread.yield();
	}
	
	private static UpdatableGroup createArrow() throws Exception
	{
		final UpdatableGroup group = new UpdatableGroup( 2 );
		
		final DrawableImage arrowBkg = new DrawableImage( ROOT_DIR + "arrowBkg" + IMG_EXT );
		group.insertDrawable( arrowBkg );
		Thread.yield();
		
		final String path = ROOT_DIR + "arrow";
		final Sprite arrow = new Sprite( path + SPRITE_DESC_EXT, path );
		group.insertDrawable( arrow );
		Thread.yield();

		final int groupWidth = arrowBkg.getWidth(), groupHeight = arrowBkg.getHeight();
		group.setSize( groupWidth, groupHeight );
		arrow.setPosition( ( groupWidth - arrow.getWidth() ) >> 1, groupHeight - arrow.getHeight() - 1 );
		
		return group;
	}

	public void setPosition( int x, int y )
	{
		COLORED_PUSHITS_VIEWPORT.x += x - getPosX();
		COLORED_PUSHITS_VIEWPORT.y += y - getPosY();
		super.setPosition( x, y );
	}

	/** Configura a nova fase */
	public void onNewLevel( int level ) throws Exception
	{
		// A dificuldade p�ra de crescer depois de MAX_DIFFICULT_LEVEL
		if( level > MAX_DIFFICULT_LEVEL )
			level = MAX_DIFFICULT_LEVEL;

		// Calcula a velocidade da fase
		final int percentage = ( level * 100 ) / MAX_DIFFICULT_LEVEL;
		final int currSpeed = NanoMath.lerpInt( MIN_PUSHITS_SPEED, MAX_PUSHITS_SPEED, percentage );
		pushItSpeedControl.setSpeed( currSpeed, true );
		
		// Calcula quantos pushIts teremos nesta fase
		nLevelPushIts = ( short )NanoMath.lerpInt( MIN_PUSHITS, MAX_PUSHITS, percentage );		
		byte nStars = ( byte )NanoMath.lerpInt( MIN_STAR_PUSHITS, MAX_STAR_PUSHITS, percentage );
		byte nQuestionMarks = ( byte )NanoMath.lerpInt( MIN_QM_PUSHITS, MAX_QM_PUSHITS, percentage );
		
		// Esse � invertido porque queremos ir diminuindo com o tempo
		byte nNones = ( byte )NanoMath.lerpInt( MAX_NONE_PUSHITS, MIN_QM_PUSHITS, percentage );
		
		// Sorteia os pushIts
		nHitablePushIts = 0;
		int currX = -( currSpeed - DEFAULT_PUSHIT_SPACEMENT ), nNonesTogether = 0;
		final int max = nLevelPushIts + INDEX_FIRST_PUSHIT, pushItsY = aim.getPosY() + (( aim.getHeight() - pushIt2.getHeight() ) >> 1);
		for( int i = INDEX_FIRST_PUSHIT ; i < max ; ++i )
		{
			int code = PushIt.PUSHIT_HALF_NONE + NanoMath.randInt( PushIt.PUSHIT_STAR - PushIt.PUSHIT_HALF_NONE + 1 );
			switch( code )
			{
				case PushIt.PUSHIT_QUESTION_MARK:
					if( nQuestionMarks > 0 )
					{
						--nQuestionMarks;
					}
					else
					{
						++nHitablePushIts;
						code = PushIt.PUSHIT_2 + NanoMath.randInt( PushIt.PUSHIT_TRIPLE_8 - PushIt.PUSHIT_2 + 1 );
					}
					break;
				
				case PushIt.PUSHIT_STAR:
					// N�o faz sentido termos uma estrela como �ltimo pushIt da fase
					if( ( nStars > 0 ) && ( i < max - 1 ) )
					{
						--nStars;
					}
					else
					{
						++nHitablePushIts;
						code = PushIt.PUSHIT_2 + NanoMath.randInt( PushIt.PUSHIT_TRIPLE_8 - PushIt.PUSHIT_2 + 1 );
					}
					break;
				
				case PushIt.PUSHIT_TRIPLE_NONE:
				case PushIt.PUSHIT_DOUBLE_N_HALF_NONE:
				case PushIt.PUSHIT_DOUBLE_NONE:
				case PushIt.PUSHIT_ONE_N_HALF_NONE:
				case PushIt.PUSHIT_NONE:
				case PushIt.PUSHIT_HALF_NONE:
					if(( nNones > 0 ) && ( nNonesTogether < ( MAX_NONE_PUSHITS_TOGETHER * 10 ) ) && ( i > INDEX_FIRST_PUSHIT ) && ( i < max - 1 ) )
					{
						--nNones;
						// Finjo que 5 vale 0.5
						nNonesTogether += (( code + 2 ) * 5 );
					}
					else
					{
						++nHitablePushIts;
						nNonesTogether = 0;
						code = PushIt.PUSHIT_2 + NanoMath.randInt( PushIt.PUSHIT_TRIPLE_8 - PushIt.PUSHIT_2 + 1 );
					}
					break;
					
				default:
					++nHitablePushIts;
					// Sem break mesmo
			}
			
			// 70% de chance de n�o possuir rastro
			if( ( code > PushIt.PUSHIT_TRIPLE_NONE ) && ( NanoMath.randInt( 100 ) < 70 ) )
				code = PushIt.untrailCode( code );
				
			(( PushIt )drawables[i]).setPushIt( code );
			currX -= ( drawables[i].getWidth() + DEFAULT_PUSHIT_SPACEMENT );
			drawables[i].setPosition( currX, pushItsY );
		}
		
		// Reinicia as vari�veis de controle
		currPushIt = -1;
		trailCounter = 0;
			
		nInactivePushIts = 0;
		
		starBonusTime = 0;
		starBonusCounter = 0;
		starBonusSpeed = 0;
		speedBeforeStarBonus = -1;
		
		// Reseta alguns estados
		shine.setVisible( false );
	}
	
	public synchronized void keyPressed( int key )
	{
		currPushIt = -1;
		
		shine.setSequence( 0 );
		shine.setVisible( true );
		shine.pause( false );
		
		checkPushItHit( key );
	}
	
	private synchronized void checkPushItHit( int keyPressed )
	{
		// Calcula a �rea da mira
		final int pushItWidth = pushIt2.getWidth();

		// Um pushIt ser� considerado sob a mira apenas se estiver no intervalo [-AIM_DIST_ACCEPTANCE, AIM_DIST_ACCEPTANCE]
		// em rela��o a aimArea. Logo, sempre teremos apenas um �nico pushIt sob a mira
		final int max = NanoMath.min( INDEX_FIRST_PUSHIT + nInactivePushIts + 4, INDEX_FIRST_PUSHIT + nLevelPushIts );
		
		int i;
		final int minIntersection = pushItWidth - AIM_DIST_ACCEPTANCE;
		Rectangle pushItArea = null;
		
		for( i = INDEX_FIRST_PUSHIT + nInactivePushIts ; i < max ; ++i )
		{
			// Trata os pushIts vazios e j� acertados
			if( drawables[i].isVisible() && !(( PushIt )drawables[i] ).getUsed() )
			{
				pushItArea = (( PushIt )drawables[i] ).getCollisionArea();
				final Rectangle intersection = new Rectangle();
				intersection.setIntersection( aimCollisionArea, pushItArea );

				// Se houve interse��o, armazena
				if( intersection.width >= minIntersection )
					break;
			}
		}
		
		// Informa se acertou algu�m
		if( i < max )
		{
			final PushIt aux = ( PushIt )drawables[i];

			// Acertou o pushIt e seu c�digo
			if( aux.hit( keyPressed ) )
			{
				if( aux.hasTrail() )
				{
					trailCounter = 0;
					currPushIt = i;
				}
				else if( aux.getCode() == PushIt.PUSHIT_STAR )
				{
					setStarBonus();
					
					// N�o chama o listener, pois n�o iremos mudar o estado
					//listener.onHit();
					
					return;
				}

				// + 1 => O zero tb conta
				final byte hitTypeInterval = ( byte )( ( pushItWidth - minIntersection + 1 )/ PUSHIT_N_TRUE_HIT_TYPES );

				int errorMargin = aimCollisionArea.x - pushItArea.x;
				if( errorMargin < 0 )
					errorMargin = -errorMargin;

				listener.onHit( aux.getCode() == PushIt.PUSHIT_QUESTION_MARK ? PushItBarListener.ON_HIT_TRUE_QM : PushItBarListener.ON_HIT_TRUE, ( byte )( errorMargin / hitTypeInterval ) );
			}
			// Acertou algu�m, mas errou o c�digo
			else
			{
				listener.onHit( aux.getCode() == PushIt.PUSHIT_QUESTION_MARK ? PushItBarListener.ON_HIT_FALSE_QM : PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS );
			}
		}
		// N�o acertou ningu�m
		else
		{
			// Verifica se o �ltimo j� passou
			final PushIt lastPushIt = ( PushIt )drawables[ INDEX_FIRST_PUSHIT + nLevelPushIts - 1 ];
			if( lastPushIt.isVisible() && ( !lastPushIt.getUsed() || ( lastPushIt.getPosX() < ScreenManager.SCREEN_WIDTH ) ) )
				listener.onHit( PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS );
		}
	}
	
	public synchronized void keyReleased( int key )
	{
		currPushIt = -1;
	}
	
	/** Configura o b�nus da estrela */
	private void setStarBonus()
	{
		starBonusCounter = 0;

		// Soma pois pode estar acumulando duas estrelas
		starBonusTime += MIN_STAR_TIME + NanoMath.randInt( MAX_STAR_TIME - MIN_STAR_TIME + 1 );
		
		// Se estamos acumulando, n�o sobrescreve a velocidade original
		if( speedBeforeStarBonus < 0 )
			speedBeforeStarBonus = pushItSpeedControl.getSpeed();
		
		// Verifica a velocidade a ser aplicada
		starBonusSpeed = pushItSpeedControl.getSpeed() >> 1;

		final int minSpeed = pushIt2.getWidth() >> 2;
		if( starBonusSpeed < minSpeed )
			starBonusSpeed = minSpeed;
		
		final int speedDiff = speedBeforeStarBonus - starBonusSpeed;
		if( speedDiff > 0 )
			pushItSpeedControl.setSpeed( starBonusSpeed, false );
	}

	/** Atualiza o componente */
	public synchronized void update( int delta )
	{
		//#if SCREEN_SIZE == "SMALL"
//# 		cancelCurrPushIt();
		//#endif
		
		// Atualiza os componentes inseridos na cole��o
		superDotUpdate( delta );
		
		// Verifica se est� pressionando um pushIt de rastro
		if( currPushIt >= 0 )
		{
			trailCounter += delta;
			if( trailCounter >= TRAIL_POINTS_TIME )
			{
				final int max = trailCounter / TRAIL_POINTS_TIME;
				for( int i = 0 ; i < max ; ++i )
					listener.onHit( PushItBarListener.ON_HIT_TRUE_TRAIL, PUSHIT_HIT_KEEP_TRAIL );
				trailCounter %= TRAIL_POINTS_TIME;
			}
		}
		
		// Aplica o b�nus da estrela
		if( starBonusTime > 0 )
		{
			starBonusCounter += delta;
			
			int timePercentage = ( starBonusCounter * 100 ) / starBonusTime;
			if( timePercentage >= 100 )
			{
				pushItSpeedControl.setSpeed( speedBeforeStarBonus, false );

				starBonusTime = 0;
				starBonusCounter = 0;
				starBonusSpeed = 0;
				speedBeforeStarBonus = -1;
			}
			else
			{
				int speed = starBonusSpeed + ((( speedBeforeStarBonus - starBonusSpeed ) * timePercentage ) / 100 );
				if( speed > speedBeforeStarBonus )
					speed = speedBeforeStarBonus;

				pushItSpeedControl.setSpeed( speed, false );
			}
		}

		// Move os pushIts
		final int dx = pushItSpeedControl.updateInt( delta );
		if( dx == 0 )
			return;

		final int max = nLevelPushIts + INDEX_FIRST_PUSHIT;
		for( int i = INDEX_FIRST_PUSHIT + nInactivePushIts; i < max ; ++i )
		{
			if( currPushIt == i )
			{
				if( !(( PushIt )drawables[i] ).reduceTrail( dx, aimCollisionArea.x ) )
				{
					currPushIt = -1;
					setNoShine();
				}
			}
			else
			{
				final PushIt aux = ( PushIt )drawables[i];
				aux.move( dx, 0 );
				
				// Verifica se o jogador perdeu um pushIt
				if( aux.isVisible()
					&& !aux.getUsed()
					&& ( aux.getCode() >= PushIt.PUSHIT_2 )
					&& ( aux.getCode() < PushIt.PUSHIT_QUESTION_MARK )
					&& ( aux.getCollisionArea().x >= aimCollisionArea.x + aimCollisionArea.width ) )
				{
					(( PushIt )drawables[i] ).setUsed();
					listener.onHit( PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS );
					
					currPushIt = -1;
				}
			
				if( aux.getPosX() >= ScreenManager.SCREEN_WIDTH )
				{
					aux.setVisible( false );
					++nInactivePushIts;

					if( nInactivePushIts == nLevelPushIts )
					{
						listener.onLevelEnded();
						return;
					}
				}
			}
		}
	}
	
	public short getNHitablePushIts()
	{
		return nHitablePushIts;
	}
	
	/** Otimiza o processo de atualiza��o dos pushIts */
	private void superDotUpdate( int delta )
	{
		// CANCELED : PushIts n�o s�o updatables
		// Calcula quantos pushIts cabem na tela e acrescenta uma folga de 3
//		final short nPushItsOnScreen = ( short )( ( ScreenManager.SCREEN_WIDTH / pushIt2.getWidth() ) + 3 );
//		final short min = ( short )( INDEX_FIRST_PUSHIT + nInactivePushIts );
//		final short max = ( short )NanoMath.min( min + nPushItsOnScreen, INDEX_SHINE );
//		for( int i = min; i < max ; ++i )
//			(( PushIt )drawables[i] ).update( delta );
		
		// Atualiza os outros updatables inseridos no grupo
		shine.update( delta );
		aim.update( delta );
		(( UpdatableGroup )drawables[ INDEX_ARROW ] ).update( delta );
	}

	public synchronized void onSequenceEnded( int id, int sequence )
	{
		//#if SCREEN_SIZE == "SMALL"
//# 		// Tenta corrigir bug que s� ocorre no Nokia 6822
//# 		if( currPushIt >= 0 )
//# 		{
//# 			cancelCurrPushIt();
//# 		}
//# 		else
//# 		{
//# 			currPushIt = -1;
//# 			setNoShine();
//# 		}
		//#else
		if( currPushIt < 0 )
			setNoShine();
		//#endif
	}
	
	//#if SCREEN_SIZE == "SMALL"
//# 	/** Tenta corrigir um bug que s� ocorre no Nokia 6822 */
//# 	private synchronized void cancelCurrPushIt()
//# 	{
//# 		if( currPushIt < 0 )
//# 			return;
//# 
//# 		if( ( currPushIt < INDEX_FIRST_PUSHIT + nInactivePushIts )
//# 				|| ( currPushIt >= INDEX_FIRST_PUSHIT + nLevelPushIts )
//# 				|| ( !drawables[ currPushIt ].isVisible() )
//# 				|| ( drawables[ currPushIt ].getPosX() > aimCollisionArea.x + aimCollisionArea.width )
//# 				|| ( drawables[ currPushIt ].getPosX() + drawables[ currPushIt ].getWidth() < aimCollisionArea.x )
//# 				// N�o pode fazer este teste, pq "used" � setado no 1o hit!!!
//# 				//|| ( (( PushIt )drawables[ currPushIt ]).getUsed() )
//# 				|| ( !(( PushIt )drawables[ currPushIt ]).hasTrail() )
//# 			  )
//# 		{
//# 			currPushIt = -1;
//# 			setNoShine();
//# 		}
//# 	}
	//#endif

	private void setNoShine()
	{
		shine.pause( true );
		shine.setVisible( false );
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}

	/** Otimiza o m�tdo de desenho */
	protected void paint( Graphics g )
	{
		for( int i = INDEX_BAR_TOP ; i <= INDEX_BKG ; ++i )
			drawables[i].draw( g );
				
		/** Otimiza o redesenho dos pushIts */
		int firstOnScreen = INDEX_FIRST_PUSHIT + nInactivePushIts;
		final int lastOnScreen = NanoMath.min( firstOnScreen + 1 + ( ScreenManager.SCREEN_WIDTH / ( pushIt2.getWidth() + DEFAULT_PUSHIT_SPACEMENT )), INDEX_FIRST_PUSHIT + nLevelPushIts - 1 );
		for( ; firstOnScreen <= lastOnScreen ; ++firstOnScreen )
		{
			if( drawables[ firstOnScreen ].isVisible() ) // Impede o desvio de chamada de fun��o
				drawables[ firstOnScreen ].draw( g );
		}
		
		for( int i = INDEX_SHINE ; i <= INDEX_ARROW ; ++i )
			drawables[i].draw( g );
	}
}
