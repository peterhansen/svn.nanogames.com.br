/**
 * PushItBarListener.java
 * �2008 Nano Games.
 *
 * Created on 11/06/2008 14:38:22.
 */

package game;

/**
 *
 * @author Daniel L. Alves
 */

public interface PushItBarListener
{
	/** Valores poss�veis para o par�metro "hit" de onHit() */
	public static final byte ON_HIT_FALSE		= 0;
	public static final byte ON_HIT_FALSE_QM	= 1;
	public static final byte ON_HIT_TRUE		= 2;
	public static final byte ON_HIT_TRUE_QM		= 3;
	public static final byte ON_HIT_TRUE_TRAIL	= 4;
		
	/** Indica se o usu�rio acertou ou n�o o pushIt */
	public void onHit( byte hit, byte hitStatus );
	
	/** Indica que o fase atual terminou */
	public void onLevelEnded();
}
