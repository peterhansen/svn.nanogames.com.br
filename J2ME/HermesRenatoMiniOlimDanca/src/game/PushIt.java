/**
 * PushIt.java
 * �2008 Nano Games.
 *
 * Created on 05/06/2008 17:41:27.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.GameMIDlet;

/**
 * @author Daniel L. Alves
 */
public final class PushIt extends DrawableGroup
{
	//<editor-fold defaultstate="collapsed" desc="C�digos que indicam o que o jogador deve apertar">

	public static final byte  PUSHIT_NOT_SET				= -2;
	
	public static final byte  PUSHIT_HALF_NONE				= -1;
	public static final byte  PUSHIT_NONE					= 0;
	public static final byte  PUSHIT_ONE_N_HALF_NONE		= 1;
	public static final byte  PUSHIT_DOUBLE_NONE			= 2;
	public static final byte  PUSHIT_DOUBLE_N_HALF_NONE		= 3;
	public static final byte  PUSHIT_TRIPLE_NONE			= 4;
	
	public static final byte  PUSHIT_2						= 5;
	public static final byte  PUSHIT_ONE_N_HALF_2			= 6;
	public static final byte  PUSHIT_DOUBLE_2				= 7;
	public static final byte  PUSHIT_DOUBLE_N_HALF_2		= 8;
	public static final byte  PUSHIT_TRIPLE_2				= 9;
	
	public static final byte  PUSHIT_4						= 10;
	public static final byte  PUSHIT_ONE_N_HALF_4			= 11;
	public static final byte  PUSHIT_DOUBLE_4				= 12;
	public static final byte  PUSHIT_DOUBLE_N_HALF_4		= 13;
	public static final byte  PUSHIT_TRIPLE_4				= 14;
	
	public static final byte  PUSHIT_6						= 15;
	public static final byte  PUSHIT_ONE_N_HALF_6			= 16;
	public static final byte  PUSHIT_DOUBLE_6				= 17;
	public static final byte  PUSHIT_DOUBLE_N_HALF_6		= 18;
	public static final byte  PUSHIT_TRIPLE_6				= 19;
	
	public static final byte  PUSHIT_8						= 20;
	public static final byte  PUSHIT_ONE_N_HALF_8			= 21;
	public static final byte  PUSHIT_DOUBLE_8				= 22;
	public static final byte  PUSHIT_DOUBLE_N_HALF_8		= 23;
	public static final byte  PUSHIT_TRIPLE_8				= 24;

	public static final byte  PUSHIT_QUESTION_MARK			= 25;

	public static final byte  PUSHIT_STAR					= 26;
	
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="�ndices das imagens da cole��o">
	
	private final byte PUSHIT_GRAYSCALE_TRAIL		= 0;
	private final byte PUSHIT_GRAYSCALE_BOX			= 1;
	private final byte PUSHIT_HIGHLIGHTED_TRAIL		= 2;
	private final byte PUSHIT_HIGHLIGHTED_BOX		= 3;
	private final byte PUSHIT_HIGHLIGHTED_BOX_QM	= 4;
	
	//</editor-fold>
	
	/** Chance que o jogador possui de acertar o c�digo escondido pela interroga��o */
	private static final byte PUSHIT_QUESTION_MARK_EXTRA_CHANCE_TO_HIT = 33;

	/** C�digo atual deste pushit */
	private byte currCode;
	
	/** C�digo escondido pelo ponto de interroga��o */
	private byte questionMarkRealCode;
	
	/** Indica se o pushIt j� sofreu uma tentativa de acerto*/
	private boolean used;
	
	/** C�digo atual deste pushit */
	public PushIt() throws Exception
	{
		super( 5 );
		currCode = PUSHIT_NOT_SET;
		
		questionMarkRealCode = PUSHIT_NOT_SET;
		
		insertDrawable( null );
		insertDrawable( null );
		insertDrawable( null );
		insertDrawable( null );
		insertDrawable( null );
	}
	
	/** Retorna o c�digo atual do pushIt */
	public byte getCode()
	{
		return currCode;
	}
	
	/** Verifica se o pushIt j� sofreu uma tentativa de acerto */
	public boolean getUsed()
	{
		return used;
	}
	
	/** Indica que o pushIt j� foi utilizado */
	public void setUsed()
	{
		used = true;
	}
	
	/** Indica que o pushIt sofreu uma tentativa de acerto */
	public boolean hit( int keyPressed )
	{
		setUsed();
		
		final boolean isQuestionMark = currCode == PUSHIT_QUESTION_MARK;
		final int codeToCheck = isQuestionMark ? questionMarkRealCode : currCode;
		
		boolean ret;
		switch( keyPressed )
		{
			case ScreenManager.KEY_STAR:
				ret = currCode == PUSHIT_STAR;
				break;
		
			// Eu sei, t� bem divertido =P
			default:
//			case ScreenManager.KEY_NUM2:
//			case ScreenManager.KEY_NUM4:
//			case ScreenManager.KEY_NUM6:
//			case ScreenManager.KEY_NUM8:
				final int motherCode = ((( keyPressed - ScreenManager.KEY_NUM2 ) >> 1 ) + 1 ) * 5;
				ret = prevMulOf5( codeToCheck ) == motherCode;

				// Rouba a favor do jogador
				if( isQuestionMark && !ret )
					ret = NanoMath.randInt( 75 ) < PUSHIT_QUESTION_MARK_EXTRA_CHANCE_TO_HIT;

				break;
		}
		
		// Se acertou, n�o aparece o cinza depois da mira
		if( ret )
		{
			if( !hasTrail() )
				setVisible( false );
		}
		else if( isQuestionMark )
		{
			drawables[ PUSHIT_HIGHLIGHTED_BOX_QM ].setVisible( true );
		}
		return ret;
	}
	
	/** Obt�m o primeiro m�ltiplo de 5 anterior a n */
	private static int prevMulOf5( int n )
	{
		final int mod = n % 5;
		if( n == 0 )
			return n;
		return n - mod;
	}
	
	/** Indica se o psuhIt */
	public boolean hasTrail()
	{
		return currCode >= PUSHIT_2 && currCode <= PUSHIT_TRIPLE_8 && (( currCode % 5 ) != 0 );
	}
	
	/** Determina como deve ser a apar�ncia deste pushit */
	public void setPushIt( int code ) throws Exception
	{
		used = false;
		currCode = ( byte )code;
		
		questionMarkRealCode = PUSHIT_NOT_SET;

		final PushItBar pushItbar = GameMIDlet.getPushItBar();
		final int pushItWidth = pushItbar.pushIt2.getWidth();
		final int pushItHalfWidth = pushItWidth >> 1;
		
		int extraWidth = 0;

		drawables[ PUSHIT_HIGHLIGHTED_BOX_QM ] = new DrawableImage( pushItbar.pushItX );
		drawables[ PUSHIT_HIGHLIGHTED_BOX_QM ].setVisible( false );
		
		// Aloca qualquer coisa s� para driblar os componentes
		drawables[ PUSHIT_GRAYSCALE_TRAIL ] = new DrawableImage( pushItbar.grayTrail );
		drawables[ PUSHIT_GRAYSCALE_TRAIL ].setVisible( false );
		drawables[ PUSHIT_HIGHLIGHTED_TRAIL ] = new DrawableImage( pushItbar.trail2 );
		drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setVisible( false );

		setVisible( true );
		
		switch( currCode )
		{
			case PUSHIT_TRIPLE_2:
			case PUSHIT_DOUBLE_N_HALF_2:
			case PUSHIT_DOUBLE_2:
			case PUSHIT_ONE_N_HALF_2:
				drawables[ PUSHIT_GRAYSCALE_TRAIL   ] = new DrawableImage( pushItbar.grayTrail );
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ] = new DrawableImage( pushItbar.trail2 );
			case PUSHIT_2:
				extraWidth = ( currCode - PUSHIT_2 ) * pushItHalfWidth;
				drawables[ PUSHIT_GRAYSCALE_BOX   ] = new DrawableImage( pushItbar.gray2 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt2 );
				break;
	
			case PUSHIT_TRIPLE_4:
			case PUSHIT_DOUBLE_N_HALF_4:
			case PUSHIT_DOUBLE_4:
			case PUSHIT_ONE_N_HALF_4:
				drawables[ PUSHIT_GRAYSCALE_TRAIL   ] = new DrawableImage( pushItbar.grayTrail );
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ] = new DrawableImage( pushItbar.trail4 );
			case PUSHIT_4:
				extraWidth = ( currCode - PUSHIT_4 ) * pushItHalfWidth;
				drawables[ PUSHIT_GRAYSCALE_BOX   ] = new DrawableImage( pushItbar.gray4 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt4 );
				break;

			case PUSHIT_TRIPLE_6:
			case PUSHIT_DOUBLE_N_HALF_6:
			case PUSHIT_DOUBLE_6:
			case PUSHIT_ONE_N_HALF_6:
				drawables[ PUSHIT_GRAYSCALE_TRAIL   ] = new DrawableImage( pushItbar.grayTrail );
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ] = new DrawableImage( pushItbar.trail6 );
			case PUSHIT_6:
				extraWidth = ( currCode - PUSHIT_6 ) * pushItHalfWidth;
				drawables[ PUSHIT_GRAYSCALE_BOX   ] = new DrawableImage( pushItbar.gray6 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt6 );
				break;

			case PUSHIT_TRIPLE_8:
			case PUSHIT_DOUBLE_N_HALF_8:
			case PUSHIT_DOUBLE_8:
			case PUSHIT_ONE_N_HALF_8:
				drawables[ PUSHIT_GRAYSCALE_TRAIL   ] = new DrawableImage( pushItbar.grayTrail );
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ] = new DrawableImage( pushItbar.trail8 );
			case PUSHIT_8:
				extraWidth = ( currCode - PUSHIT_8 ) * pushItHalfWidth;
				drawables[ PUSHIT_GRAYSCALE_BOX   ] = new DrawableImage( pushItbar.gray8 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt8 );
				break;

			case PUSHIT_QUESTION_MARK:
				// Sorteia um desses valores: PUSHIT_2, PUSHIT_4, PUSHIT_6 e PUSHIT_8
				questionMarkRealCode = ( byte )( ( 1 + NanoMath.randInt( 4 ) ) * 5 );

				drawables[ PUSHIT_GRAYSCALE_BOX   ] = new DrawableImage( pushItbar.grayQm );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushItQm );
				break;
				
			case PUSHIT_STAR:
				drawables[ PUSHIT_GRAYSCALE_BOX   ] = new DrawableImage( pushItbar.grayStar );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushItStar );
				break;
			
			case PUSHIT_HALF_NONE:
			case PUSHIT_TRIPLE_NONE:
			case PUSHIT_DOUBLE_N_HALF_NONE:
			case PUSHIT_DOUBLE_NONE:
			case PUSHIT_ONE_N_HALF_NONE:
			case PUSHIT_NONE:
				extraWidth = ( currCode - PUSHIT_NONE ) * pushItHalfWidth;
				setVisible( false );
				break;
		}
		// Como estamos trocando v�rias imagens, deixamos nossa consci�ncia limpa chamando gc()
		System.gc();
		
		// Se possui rastro...
		final int pushItHeight = pushItbar.pushIt2.getHeight();
		if( isVisible() )
		{
			if( extraWidth > 0 )
			{
				final int trailWidth = extraWidth + PushItBar.PUSHIT_TRAIL_OFFSET;
				
				drawables[ PUSHIT_GRAYSCALE_TRAIL ].setVisible( true );
				drawables[ PUSHIT_GRAYSCALE_TRAIL ].setSize( trailWidth, pushItHeight );
				
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setVisible( true );
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setSize( trailWidth, pushItHeight );
				
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setViewport( pushItbar.getColoredPushItsViewport() );
			}
			
			drawables[ PUSHIT_GRAYSCALE_BOX   ].setPosition( extraWidth, 0 );
			drawables[ PUSHIT_HIGHLIGHTED_BOX ].setPosition( extraWidth, 0 );
			
			drawables[ PUSHIT_HIGHLIGHTED_BOX ].setViewport( pushItbar.getColoredPushItsViewport() );
		}
		
		// Determina o novo tamanho da cole��o
		setSize( pushItWidth + extraWidth, pushItHeight );
	}
	
	/** Retorna o c�digo do pushIt sem rastro */
	public static int untrailCode( int code )
	{
		switch( code )
		{
			case PUSHIT_HALF_NONE:
				return PUSHIT_NONE;
				
			case PUSHIT_STAR:
				return PUSHIT_STAR;
				
			default:
				return prevMulOf5( code );
		}
	}
	
	/** Obt�m a �rea do cole��o que pode sofrer colis�o */
	public Rectangle getCollisionArea()
	{
		return new Rectangle( position.x + drawables[ PUSHIT_HIGHLIGHTED_BOX ].getPosX(), position.y + drawables[ PUSHIT_HIGHLIGHTED_BOX ].getPosY(), drawables[ PUSHIT_HIGHLIGHTED_BOX ].getWidth(), drawables[ PUSHIT_HIGHLIGHTED_BOX ].getHeight() );
	}

	/** Reduz o tamanho do rastro do pushIt. Retorna se ainda h� ratro para ser reduzido */
	public boolean reduceTrail( int dx, int aimCollisionAreaX )
	{
		for( int i = 0 ; i < 3 ; i += 2 )
		{
			drawables[i].setSize( drawables[ PUSHIT_GRAYSCALE_TRAIL ].getWidth() - dx, drawables[ PUSHIT_GRAYSCALE_TRAIL ].getHeight() );
			drawables[i].move( dx, 0 );
		}
		
		final boolean ret = ( position.x + drawables[ PUSHIT_GRAYSCALE_TRAIL ].getPosX() < aimCollisionAreaX ) && ( drawables[ PUSHIT_GRAYSCALE_TRAIL ].getPosX() < drawables[ PUSHIT_GRAYSCALE_BOX ].getPosX() );
		setVisible( ret );
		return ret;
	}
}
