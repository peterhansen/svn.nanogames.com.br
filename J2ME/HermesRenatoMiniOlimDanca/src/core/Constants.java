/**
 * Constants.java
 */

package core;

/**
 * �2007 Nano Games
 * @author Daniel L. Alves
 */

public interface Constants
{
	/** Cores utilizadas para o fundo durante o jogo */
	public static final int DEFAULT_SCROLL_LIGHT_COLOR = 0xFAAAFF;
	public static final int DEFAULT_LIGHT_COLOR = 0xF66CFF;
	public static final int DEFAULT_DARK_COLOR = 0xBD00D7;
	
	/** N�mero m�ximo de uma fase */
	public static final short MAX_LEVEL = 999;
		
	/** Fase na qual o jogo chega ao seu m�ximo de dificuldade */
	public static final byte MAX_DIFFICULT_LEVEL = 20;
	
	/** M�xima pontua��o poss�vel */
	public static final long MAX_SCORE = 999999999999L;
		
	/** N�mero m�ximo de teclas que o jogador pode ter que apertar durante uma fase
	 * @see #MIN_PUSHITS
	 */
	public static final short MAX_PUSHITS = 180;
	
	/** N�mero m�nimo de teclas que o jogador pode ter que apertar durante uma fase
	 * @see #MAX_PUSHITS
	 */
	public static final short MIN_PUSHITS = 10;
		
	/** Menor velocidade que uma fase pode atingir
	 * @see #MAX_PUSHITS_SPEED
	 */
	//#ifdef SmallC65
//# 	public static final short MIN_PUSHITS_SPEED = 17; // Metade da velocidade abaixo
	//#else
		//#if SCREEN_SIZE == "SMALL"
//# 		public static final short MIN_PUSHITS_SPEED = 23; // Obtido com regra de 3 a partir do valor abaixo
		//#else
			public static final short MIN_PUSHITS_SPEED = 33; // Largura padr�o dos pushIts m�dios ( 24 ) + 9 para n�o come�ar t�o devagar
		//#endif
	//#endif
	
	/** Maior velocidade que uma fase pode atingir
	 * @see #MIN_PUSHITS_SPEED
	 */
	//#ifdef SmallC65
//# 	public static final short MAX_PUSHITS_SPEED = 60; // Metade da velocidade abaixo
	//#else
		//#if SCREEN_SIZE == "SMALL"
//# 		public static final short MAX_PUSHITS_SPEED = 85; // Obtido com regra de 3 a partir do valor abaixo
		//#else
			public static final short MAX_PUSHITS_SPEED = 120; // 5 * Largura padr�o dos pushIts m�dios ( 24 )
		//#endif
	//#endif
	
	/** Tempo m�ximo de dura��o do b�nus da estrela (em mil�simos)
	 * @see #MIN_STAR_TIME
	 */
	public static final short MAX_STAR_TIME = 15000;
	
	/** Tempo m�nimo de dura��o do b�nus da estrela (em mil�simos)
	 * @see #MAX_STAR_TIME
	 */
	public static final short MIN_STAR_TIME = 10000;
	
	/** N�mero m�nimo de pushIts de estrela que podem aparecer em uma fase
	 * @see #MAX_STAR_PUSHITS
	 */
	public static final byte MIN_STAR_PUSHITS = 0;
	
	/** N�mero m�ximo de pushIts de estrela que podem aparecer em uma fase
	 * @see #MIN_STAR_PUSHITS
	 */
	public static final byte MAX_STAR_PUSHITS = 12;
	
	/** N�mero m�nimo de pushIts de interroga��o que podem aparecer em uma fase
	 * @see #MAX_QM_PUSHITS
	 */
	public static final byte MIN_QM_PUSHITS = 0;
	
	/** N�mero m�ximo de pushIts de interroga��o que podem aparecer em uma fase
	 * @see #MIN_QM_PUSHITS
	 */
	public static final byte MAX_QM_PUSHITS = 20;
	
	/** N�mero m�nimo de pushIts vazios que podem aparecer em uma fase
	 * @see #MAX_NONE_PUSHITS
	 */
	public static final byte MIN_NONE_PUSHITS = 0;
	
	/** N�mero m�ximo de pushIts vazios que podem aparecer em uma fase
	 * @see #MIN_NONE_PUSHITS
	 */
	public static final byte MAX_NONE_PUSHITS = 10;
	
	/** Tempo m�ximo de dura��o de um ch�o
	 * @see #MIN_FLOOR_DUR
	 */
	public static final short MAX_FLOOR_DUR = 700;
	
	/** Tempo m�nimo de dura��o de um ch�o
	 * @see #MAX_FLOOR_DUR
	 */
	public static final short MIN_FLOOR_DUR = 300;
	
	/** Tempo m�ximo de dura��o de um background
	 * @see #MIN_BKG_DUR
	 */
	public static final short MAX_BKG_DUR = 10000;
	
	/** Tempo m�nimo de dura��o de um background
	 * @see #MAX_BKG_DUR
	 */
	public static final short MIN_BKG_DUR = 5000;
	
	/** Velocidade m�xima do background
	 * @see #MIN_BKG_SPEED
	 */
	//#if SCREEN_SIZE == "SMALL"
//# 	public static final short MAX_BKG_SPEED = 20;
	//#else
	public static final short MAX_BKG_SPEED = 30;
	//#endif
	
	/** Velocidade m�nima do background
	 * @see #MAX_BKG_SPEED
	 */
	//#if SCREEN_SIZE == "SMALL"
//# 	public static final short MIN_BKG_SPEED = 5;
	//#else
	public static final short MIN_BKG_SPEED = 10;
	//#endif
	
	/** N�mero m�ximo de pushIts vazios que podem aparecer juntos em uma fase */
	public static final byte MAX_NONE_PUSHITS_TOGETHER = 3;
	
	/** Folga que damos na mira para que o usu�rio n�o precise acertar o pushIt 100% */
	public static final byte AIM_DIST_ACCEPTANCE = 9;
	
	/** Espa�amento m�nimo entre os pushIts */
	public static final byte DEFAULT_PUSHIT_SPACEMENT = AIM_DIST_ACCEPTANCE;

	/** De quanto em quanto tempo fornecemos pontos quando o jogador est� acertando um rastro */
	public static final byte TRAIL_POINTS_TIME = 50;
	
	/** At� quando contabilizamos os hits */
	public static final short MAX_HIT_COUNT = 1000;
	
	/** Maior multiplicador b�nus poss�vel */
	public static final short MAX_BONUS_MULIPLIER = 999;
	
	/** Quanto o contador de b�nus cresce ao receber um acerto PUSHIT_HIT_PERFECT */
	public static final byte PERFECT_BONUS_COUNTER_INC = 4;
	
	/** Termo inicial da PA que regula o crescimento do multiplicador b�nus */
	public static final byte BONUS_PA_A1 = 4;
	
	/** Raz�o da PA que regula o crescimento do multiplicador b�nus */
	public static final byte BONUS_PA_R = 1;
	
	/** Tempo de visibilidade das imagens que indicam o tipo acerto */
	public static final short FEEDBACK_DUR = 500;
	
	/** Pontua��o fornecida quando o jogador faz um acerto perfeito */
	public static final short PERFECT_HIT_N_POINTS = 5137;
	
	/** Porcentagem de PERFECT_HIT_N_POINTS decrescida a cada categoria de acerto abaixo de PUSHIT_HIT_PERFECT */
	public static final byte UNDER_PERFECT_HIT_PERCENTAGE = 20;
	
	/** N�mero de competidores que aparecer�o na tela das medalhas (incluindo o personagem principal)*/
	public static final byte N_COMPETITORS = 6;
	
	/** Nota m�xima obtida pelo melhor advers�rio
	 * @see #BEST_COMPETITOR_MIN_PERCENTAGE */
	public static final byte BEST_COMPETITOR_MAX_PERCENTAGE = 95;
	
	/** Nota m�nima obtida pelo melhor advers�rio
	 * @see #BEST_COMPETITOR_MAX_PERCENTAGE */
	public static final byte BEST_COMPETITOR_MIN_PERCENTAGE = 80;
	
	/** Diferen�a entre os intervalos das notas dos competidores */
	public static final byte COMPETITORS_PERCENTAGE_STEP = 5;
	
	// N�mero de medalhas ol�mpicas existentes
	public static final byte N_MEDALS = 3;
	
	/** Tempo que o background leva para mudar completamente de cor nas telas de carregamento */
	public static final short LOAD_SCREEN_BKG_COLOR_CHANGE_TIME = 200;
			
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		public static final byte MENU_ITEMS_SPACING = 5;
		public static final byte MENU_AWAY_DIST_FROM_TITLE = 5;
		public static final byte MENU_CURSOR_DIST_FROM_OPTS = 3;
		public static final byte MEDAL_SCREEN_LABELS_X_SPACEMENT = 5;
	//#else
//# 		public static final byte MENU_ITEMS_SPACING = 2;
//# 		public static final byte MENU_AWAY_DIST_FROM_TITLE = 5;
//# 		public static final byte MENU_CURSOR_DIST_FROM_OPTS = 1;
//# 		public static final byte MEDAL_SCREEN_LABELS_X_SPACEMENT = 1;
	//#endif
	
	/** Quantidade m�nima de mem�ria para que o jogo rode com sua capacidade m�xima */
	//#if SCREEN_SIZE == "SMALL"
//# 		public static final int LOW_MEMORY_LIMIT = 600000;
	//#else
	public static final int LOW_MEMORY_LIMIT = 900000;
	//#endif
	
	/** Tempo de dura��o do splash do jogo */
	public static final short SPLASH_GAME_DUR = 3000;
	
	/** Tempo de dura��o do splash da marca */
	public static final short SPLASH_BRAND_DUR = 3000;
	
	/** Nome da base de dados */
	public static final String DATABASE_NAME = "n";
	
	/** �ndice do slot de op��es na base de dados */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �ndice do slot de recordes de pontos na base de dados */
	public static final byte DATABASE_SLOT_SCORES = 2;
	
	//#if LOG == "true"
//# 	/** �ndice do slot do log de erros */
//# 	public static final byte DATABASE_SLOT_LOG = 3;
	//#endif
	
	/** N�mero total de slots na base de dados */
	//#if LOG == "true"
//# 	public static final byte DATABASE_TOTAL_SLOTS = 3;
	//#else
	public static final byte DATABASE_TOTAL_SLOTS = 2;
	//#endif

	//<editor-fold defaultstate="collapsed" desc="�ndices das m�sicas/sons do jogo">
	
	//#ifndef NO_SOUND

		//#ifdef SmallS40
//#  
//# 			/** Quantidade total de m�sicas / sons do jogo */
//# 			public static final byte SOUND_TOTAL = 4;
//# 
//# 			public static final byte SOUND_INDEX_THEME				= 0;
//# 			public static final byte SOUND_INDEX_LEVEL_COMPLETED	= 1;
//# 			public static final byte SOUND_INDEX_LIFE_LOST			= 2;
//# 			public static final byte SOUND_INDEX_LEVEL_MUSIC_0		= 3;
//# 
//# 			/** N�mero de m�sicas que podem ser tocadas durante uma fase */
//# 			public static final byte SOUND_N_LEVEL_SOUNDS = 1;
//# 			
		//#elif SmallGF500
//# 			
//# 			/** Quantidade total de m�sicas / sons do jogo */
//# 			public static final byte SOUND_TOTAL = 3;
//# 
//# 			public static final byte SOUND_INDEX_THEME				= 0;
//# 			public static final byte SOUND_INDEX_LEVEL_COMPLETED	= 1;
//# 			public static final byte SOUND_INDEX_LIFE_LOST			= 2;
//#  
		//#else

			/** Quantidade total de m�sicas / sons do jogo */
			public static final byte SOUND_TOTAL = 6;

			public static final byte SOUND_INDEX_THEME				= 0;
			public static final byte SOUND_INDEX_LEVEL_COMPLETED	= 1;
			public static final byte SOUND_INDEX_LIFE_LOST			= 2;
			public static final byte SOUND_INDEX_LEVEL_MUSIC_0		= 3;
			public static final byte SOUND_INDEX_LEVEL_MUSIC_1		= 4;
			public static final byte SOUND_INDEX_LEVEL_MUSIC_2		= 5;

			/** N�mero de m�sicas que podem ser tocadas durante uma fase */
			public static final byte SOUND_N_LEVEL_SOUNDS = 3;

		//#endif

	//#endif
	
	//</editor-fold>
		
	//<editor-fold defaultstate="collapsed" desc="Sequ�ncias do Away">

	public static final byte AWAY_SEQ_STATIC			= 0;
	public static final byte AWAY_SEQ_OPENING_LEGS		= 1;
	public static final byte AWAY_SEQ_TRAVOLTA_STYLE	= 2;
	public static final byte AWAY_SEQ_BRAND_NEW_THING	= 3;
	public static final byte AWAY_SEQ_VUPT				= 4;
	public static final byte AWAY_SEQ_BRAKE				= 5;
	public static final byte AWAY_SEQ_VANISH			= 6;
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	public static final byte AWAY_SEQ_APPEAR			= 7;
	//#endif
	
	/** N�mero de sequ�ncias utilizadas durante a fase */
	public static final byte AWAY_SEQ_N_LEVEL_SEQS		= 5;

	//</editor-fold>
	
	/** Strings compartilhadas pelo jogo inteiro */
	public static final String SPRITE_DESC_EXT = ".bin";
	public static final String FONT_DESC_EXT = ".dat";
	public static final String IMG_EXT = ".png";
	public static final String ROOT_DIR = "/";
			
	/** Caminho das imagens da tela de splash */
	public static final String PATH_SPLASH = "/Splash/";
	
	/** Dura��o da vibra��o padr�o */
	//#if SAMSUNG_BAD_SOUND == "true"
//#			public static final byte DEFAULT_VIBRATION_TIME = 80;
	//#else
		public static final short DEFAULT_VIBRATION_TIME = 200;
	//#endif
	
	//<editor-fold defaultstate="collapsed" desc="Textos do Jogo">

	public static final byte TEXT_BACK						= 0;
	public static final byte TEXT_NEW_GAME					= 1;
	public static final byte TEXT_EXIT						= 2;
	public static final byte TEXT_OPTIONS					= 3;
	public static final byte TEXT_CREDITS					= 4;
	public static final byte TEXT_CREDITS_TEXT				= 5;
	public static final byte TEXT_HELP						= 6;
	public static final byte TEXT_HELP_TEXT_INTRO			= 7;
	public static final byte TEXT_HELP_TEXT_RULES			= 8;
	public static final byte TEXT_HELP_TEXT_SPECIAL_ICONS	= 9;
	public static final byte TEXT_HELP_TEXT_CONTROLS		= 10;
	public static final byte TEXT_TURN_SOUND_ON				= 11;
	public static final byte TEXT_TURN_SOUND_OFF			= 12;
	public static final byte TEXT_TURN_VIBRATION_ON			= 13;
	public static final byte TEXT_TURN_VIBRATION_OFF		= 14;
	public static final byte TEXT_CONTINUE					= 15;
	public static final byte TEXT_SPLASH_NANO				= 16;
	public static final byte TEXT_SPLASH_MTV				= 17;
	public static final byte TEXT_LOADING					= 18;
	public static final byte TEXT_DO_YOU_WANT_SOUND			= 19;
	public static final byte TEXT_YES						= 20;
	public static final byte TEXT_NO						= 21;
	public static final byte TEXT_EXIT_GAME					= 22;
	public static final byte TEXT_PRESS_ANY_KEY				= 23;
	public static final byte TEXT_RECORDS					= 24;
	public static final byte TEXT_ERASE_RECORDS				= 25;
	public static final byte TEXT_BACK_MENU					= 26;
	public static final byte TEXT_YOURE_DA_MAN				= 27;
	public static final byte TEXT_HITS						= 28;
	public static final byte TEXT_BONUS						= 29;
	public static final byte TEXT_COMPETITOR_0				= 30;
	public static final byte TEXT_COMPETITOR_1				= 31;
	public static final byte TEXT_COMPETITOR_2				= 32;
	public static final byte TEXT_COMPETITOR_3				= 33;
	public static final byte TEXT_COMPETITOR_4				= 34;
	public static final byte TEXT_COMPETITOR_5				= 35;
	public static final byte TEXT_RANKING					= 36;
	public static final byte TEXT_GAMEOVER					= 37;
	public static final byte TEXT_GET_READY					= 38;
	public static final byte TEXT_HELP_INTRO				= 39;
	public static final byte TEXT_HELP_RULES				= 40;
	public static final byte TEXT_HELP_SPECIAL_ICONS		= 41;
	public static final byte TEXT_HELP_CONTROLS				= 42;
	public static final byte TEXT_BACK_MENU_2				= 43;
	public static final byte TEXT_TS_TUTORIAL_TITLE			= 44;
	public static final byte TEXT_TS_TUTORIAL_PRESS			= 45;
	public static final byte TEXT_TS_TUTORIAL_AREA			= 46;
	public static final byte TEXT_TS_TUTORIAL_TO_HIT		= 47;
	
	//#if LOG == "true"
//# 	public static final byte TEXT_LOG						= 48;
	//#endif
		
	/** N�mero total de textos do jogo */
	//#if LOG == "true"
//# 	public static final byte TEXT_TOTAL						= 49;
	//#else
	public static final byte TEXT_TOTAL					= 48;
	//#endif

	//</editor-fold>
}