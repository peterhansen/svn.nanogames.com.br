/**
 * GameMIDlet.java
 */
package core;

import screens.*;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import game.PushItBar;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDletStateChangeException;

//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif

/**
 * �2007 Nano Games
 * @author Daniel L. Alves
 */

public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener
{	
	/** Refer�ncia para a tela de jogo */
	private PlayScreen playScreen;
	
	/** Fonte padr�o do jogo */
	public ImageFont FONT_DEFAULT;
	public static final byte DEFAULT_FONT_OFFSET = -1;
	
	//#if SCREEN_SIZE != "SMALL"
		private static final byte BIG_FONT_OFFSET = -2;
	//#endif
	
	/** N�mero de �cones que a aplica��o possui */
	private final byte N_GAME_ICONS = 3;
	
	/** �cones da aplica��o */
	public final Drawable[] icons = new Drawable[ N_GAME_ICONS ];
	
	/** Imagens utilizadas em quase todas as telas do jogo. As deixa pr�-alocadas para melhorar o tempo de
	 *  carregamento
	 */
	public Sprite MENU_CURSOR;
	public DrawableImage TITLE;

	/** Tempo que as soft keys ficam vis�veis na tela */
	private static final short SOFT_KEY_VISIBLE_TIME = 1000;
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos �cones da aplica��o">
	
	public static final byte ICON_BACK	= 0;
	public static final byte ICON_OK	= 1;
	public static final byte ICON_PAUSE	= 2;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das telas do jogo">
	
	public static final byte SCREEN_LOAD_RESOURCES		= 0;
	public static final byte SCREEN_ENABLE_SOUNDS		= 1;
	public static final byte SCREEN_CHANGE_BKG_COLOR	= 2;
	public static final byte SCREEN_SPLASH_NANO			= 3;
	public static final byte SCREEN_SPLASH_BRAND		= 4;
	public static final byte SCREEN_SPLASH_GAME			= 5;
	public static final byte SCREEN_MAIN_MENU			= 6;
	public static final byte SCREEN_LOAD_GAME			= 7;
	public static final byte SCREEN_OPTIONS				= 8;
	public static final byte SCREEN_CREDITS				= 9;
	public static final byte SCREEN_HELP				= 10;
	public static final byte SCREEN_HELP_INTRO			= 11;
	public static final byte SCREEN_HELP_RULES			= 12;
	public static final byte SCREEN_HELP_SPECIAL_ICONS	= 13;
	public static final byte SCREEN_HELP_CONTROLS		= 14;
	public static final byte SCREEN_RECORDS				= 15;
	public static final byte SCREEN_GAME				= 16;
	public static final byte SCREEN_PAUSE				= 17;
	public static final byte SCREEN_UNPAUSE				= 18;
	public static final byte SCREEN_CONTINUE			= 19;
	public static final byte SCREEN_GAMEOVER			= 20;
	public static final byte SCREEN_ENDING				= 21;
	public static final byte SCREEN_YN_ERASE_RECORDS	= 22;
	public static final byte SCREEN_YN_EXIT_GAME		= 23;
	public static final byte SCREEN_YN_BACK_TO_MENU		= 24;
	
	//#if LOG == "true"
//# 	public static final byte SCREEN_LOG				= 21;
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu principal">
	
	public static final byte MAIN_MENU_NEW_GAME		= 0;
	public static final byte MAIN_MENU_OPTIONS		= 1;
	public static final byte MAIN_MENU_RECORDS		= 2;
	public static final byte MAIN_MENU_HELP			= 3;
	public static final byte MAIN_MENU_CREDITS		= 4;
	public static final byte MAIN_MENU_EXIT			= 5;
	public static final byte MAIN_MENU_LOG			= 6;
	
	public static final byte MAIN_MENU_CONTINUING_NEW_GAME	= 0;
	public static final byte MAIN_MENU_CONTINUING_CONTINUE	= 1;
	public static final byte MAIN_MENU_CONTINUING_OPTIONS	= 2;
	public static final byte MAIN_MENU_CONTINUING_RECORDS	= 3;
	public static final byte MAIN_MENU_CONTINUING_HELP		= 4;
	public static final byte MAIN_MENU_CONTINUING_CREDITS	= 5;
	public static final byte MAIN_MENU_CONTINUING_EXIT		= 6;
	public static final byte MAIN_MENU_CONTINUING_LOG		= 7;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu de pausa">
	
	//#ifdef NO_SOUND
//# 		public static final byte PAUSE_MENU_CONTINUE			= 0;
//# 		public static final byte PAUSE_MENU_TOGGLE_VIBRATION	= 1;
//# 		public static final byte PAUSE_MENU_BACK_TO_MENU		= 2;	
//# 		public static final byte PAUSE_MENU_EXIT_GAME			= 3;
//# 
//# 		public static final byte PAUSE_MENU_NO_VIB_CONTINUE			= 0;
//# 		public static final byte PAUSE_MENU_NO_VIB_BACK_TO_MENU		= 1;	
//# 		public static final byte PAUSE_MENU_NO_VIB_EXIT_GAME		= 2;
	//#else
	public static final byte PAUSE_MENU_CONTINUE			= 0;
	public static final byte PAUSE_MENU_TOGGLE_SOUND		= 1;
	public static final byte PAUSE_MENU_TOGGLE_VIBRATION	= 2;
	public static final byte PAUSE_MENU_BACK_TO_MENU		= 3;	
	public static final byte PAUSE_MENU_EXIT_GAME			= 4;

	public static final byte PAUSE_MENU_NO_VIB_CONTINUE			= 0;
	public static final byte PAUSE_MENU_NO_VIB_TOGGLE_SOUND		= 1;
	public static final byte PAUSE_MENU_NO_VIB_BACK_TO_MENU		= 2;	
	public static final byte PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu de op��es">
	
	//#ifdef NO_SOUND
//# 		public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 0;
//# 		public static final byte OPTIONS_MENU_ERASE_RECORDS		= 1;
//# 		public static final byte OPTIONS_MENU_BACK				= 2;
//# 
//# 		public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 0;
//# 		public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;
	//#else
	public static final byte OPTIONS_MENU_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 1;
	public static final byte OPTIONS_MENU_ERASE_RECORDS		= 2;
	public static final byte OPTIONS_MENU_BACK				= 3;

	public static final byte OPTIONS_MENU_NO_VIB_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 1;
	public static final byte OPTIONS_MENU_NO_VIB_BACK				= 2;
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu de ajuda">

	// CANCELED : Retiramos por falta de resposta da MTV
	//public static final byte HELP_MENU_INTRO			= 0;
	public static final byte HELP_MENU_RULES			= 0;
	public static final byte HELP_MENU_SPECIAL_ICONS	= 1;
	public static final byte HELP_MENU_CONTROLS			= 2;
	public static final byte HELP_MENU_BACK				= 3;
	
	//</editor-fold>

	//#if ( LOW_JAR == "false" )
		/** Indica se o device possui pouca mem�ria */
		private boolean lowMemory;
	//#endif
	
	/** Indica se os recursos do jogo j� foram alocados */
	private boolean resourcesLoaded;
	
	/** Tempo m�ximo entre duas renderiza��es */
	private static final short LOW_FPS_FRAME_TIME = 100;
	
	/** Indica se devemos bloquear a mudan�a de telas. Com isso conseguiremos esperar os fades das telas
	 * de carregamento, por exemplo */ 
	private boolean blockScreenChange;
	
	public GameMIDlet() throws Exception
	{
		//#ifdef SmallSagem
//# 		super( VENDOR_SAGEM_GRADIENTE, LOW_FPS_FRAME_TIME );
		//#else
			super( -1, LOW_FPS_FRAME_TIME );
		//#endif

		loadTexts( TEXT_TOTAL, ROOT_DIR + "pt" + FONT_DESC_EXT );
	}
	
	protected void destroyApp( boolean unconditional ) throws MIDletStateChangeException
	{
		if( playScreen != null )
		{
			// Grava a pontua��o mesmo que esteja no meio do jogo
			GameRecordsScreen.setScore( playScreen.getScore() );
			resourcesLoaded = false;
			playScreen = null;
		}
		
		//#if LOG == "true"
//# 		Logger.saveLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif
		
		super.destroyApp( unconditional );
	}
	
	protected void loadResources() throws Exception
	{	
		//#if ( LOW_JAR == "false" )
			switch( getVendor() )
			{
				case VENDOR_SONYERICSSON:
				case VENDOR_NOKIA:
				case VENDOR_SIEMENS:
					break;

				default:
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
			}
		//#endif

		try
		{
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}

		// Carrega a fonte da aplica��o
		//#if SCREEN_SIZE != "SMALL"
		if( isBig() )
		{
			final String path = ROOT_DIR + "fontMainBig";
			FONT_DEFAULT = ImageFont.createMultiSpacedFont( path + IMG_EXT, path + FONT_DESC_EXT );
			FONT_DEFAULT.setCharOffset( BIG_FONT_OFFSET );
		}
		else
		{
		//#endif
			final String path = ROOT_DIR + "fontMain";
			FONT_DEFAULT = ImageFont.createMultiSpacedFont( path + IMG_EXT, path + FONT_DESC_EXT );
			FONT_DEFAULT.setCharOffset( DEFAULT_FONT_OFFSET );
		//#if SCREEN_SIZE != "SMALL"
		}
		//#endif
		
		// Determina uma cor de fundo
		manager.setBackgroundColor( DEFAULT_LIGHT_COLOR );
		
		//#if LOG == "true"
//# 		Logger.setRMS( DATABASE_NAME, DATABASE_SLOT_LOG );
//# 		Logger.loadLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif
		
		// Vai para a tela onde iremos carregar os outros recursos do midlet
		setScreen( SCREEN_LOAD_RESOURCES );
	}
	
	private void loadImagesAndSounds() throws Exception
	{
		final String path = ROOT_DIR + "cursorMenu";
		MENU_CURSOR = new Sprite( path + SPRITE_DESC_EXT, path );
		MENU_CURSOR.defineReferencePixel( MENU_CURSOR.getSize().x + MENU_CURSOR_DIST_FROM_OPTS, MENU_CURSOR.getSize().y >> 1 );
		Thread.yield();
		
		// Inicializa as vari�veis est�ticas da classe GameRecordsScreen
		final byte fontOffset = FONT_DEFAULT.getCharOffset();
		
		GameRecordsScreen aux = GameRecordsScreen.createInstance( FONT_DEFAULT );
		aux = null;
		System.gc();
		
		FONT_DEFAULT.setCharOffset( fontOffset );
		
		icons[ ICON_BACK ] = new DrawableImage( ROOT_DIR + "btBack" + IMG_EXT );
		Thread.yield();

		icons[ ICON_OK ] = new DrawableImage( ROOT_DIR + "btOk" + IMG_EXT );
		Thread.yield();
		
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
		icons[ ICON_PAUSE ] = new DrawableImage( ROOT_DIR + ( ScreenManager.getInstance().hasPointerEvents() ? "btPauseBig" : "btPause" ) + IMG_EXT );
		//#else
//# 		icons[ ICON_PAUSE ] = new DrawableImage( ROOT_DIR + "btPause" + IMG_EXT );
		//#endif
		Thread.yield();

		//#ifdef NO_SOUND
//# 		MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, null );
//# 		MediaPlayer.setMute( true );
		//#else
			final String[] SOUNDS = new String[SOUND_TOTAL];

			for( byte i = 0; i < SOUND_TOTAL; ++i )
				SOUNDS[i] = ROOT_DIR + i + ".mid";

			MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, SOUNDS );
		//#endif

		manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null );
		manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
		
		resourcesLoaded = true;
	}
	
	//#if SCREEN_SIZE != "SMALL"
	/** Retorna se o device e considerado como tela grande */
	public static boolean isBig()
	{
		return ( ScreenManager.SCREEN_HEIGHT >= 260 ) && ( ScreenManager.SCREEN_WIDTH > 220 );
	}
	//#endif
	
	public static synchronized void blockScreenChange( boolean block )
	{
		(( GameMIDlet )instance ).blockScreenChange = block;
	}
	
	/** Cria o background dos menus */
	private Drawable createMenuBkg( boolean withAwayDancing ) throws Exception
	{
		final MenuBkg bkg = withAwayDancing ? new MenuBkg( 3 ) : new MenuBkg( 2 );
		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		Thread.yield();
		
		// Determina a cor de fundo
		final Pattern bkgColor = new Pattern( DEFAULT_LIGHT_COLOR );
		bkgColor.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		bkg.insertDrawable( bkgColor );
		Thread.yield();
		
		// A partir de agora, TITLE n�o mais precisa ser colorido
		if( TITLE == null )
		{
			PaletteChanger aux = new PaletteChanger( ROOT_DIR + "SplashGame/title" + IMG_EXT );
			TITLE = new DrawableImage( aux.createImage( DEFAULT_LIGHT_COLOR, DEFAULT_DARK_COLOR ) );
			aux = null;
			System.gc();
			Thread.yield();
		}
		bkg.insertDrawable( TITLE );

		// Cria a sombra do personagem
		if( withAwayDancing )
		{
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final String path = ROOT_DIR + ( GameMIDlet.isLowMemory() ? "AwaySml" : "Away" ) + "/away";
			//#else
//# 			final String path = ROOT_DIR + "Away/away";
			//#endif
			final Sprite away = new Sprite( path + "_menu" + SPRITE_DESC_EXT, path, DEFAULT_DARK_COLOR, DEFAULT_DARK_COLOR );
			bkg.insertDrawable( away );
			away.setSequence( 0 );
			Thread.yield();
			
			// Posiciona os elementos na tela
			final int titleHeight = TITLE.getHeight();
			final int totalHeight = titleHeight + away.getHeight() + MENU_AWAY_DIST_FROM_TITLE;
			final int y = ( ScreenManager.SCREEN_HEIGHT - totalHeight ) >> 1;

			TITLE.setPosition( ( ScreenManager.SCREEN_WIDTH - TITLE.getWidth() ) >> 1, y );
			away.setPosition( ( ScreenManager.SCREEN_WIDTH - away.getWidth() ) >> 1, y + titleHeight + MENU_AWAY_DIST_FROM_TITLE );
			away.setListener( bkg, 2 );
		}
		else
		{
			TITLE.setPosition( ( ScreenManager.SCREEN_WIDTH - TITLE.getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - TITLE.getHeight() ) >> 1 );
		}
		return bkg;
	}

	public static void setSoftKey( byte softKey, Drawable d, int visibleTime )
	{
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );
		midlet.manager.setSoftKey( softKey, d );
	}

	public synchronized void onChoose( Menu menu, int id, int index )
	{
		switch( id )
		{
			case SCREEN_MAIN_MENU:
				if( playScreen != null )
				{
					switch( index )
					{
						case MAIN_MENU_CONTINUING_NEW_GAME:
							setScreen( SCREEN_LOAD_GAME  );
							break;
							
						case MAIN_MENU_CONTINUING_CONTINUE:
							setScreen( SCREEN_CONTINUE );
							break;

						case MAIN_MENU_CONTINUING_OPTIONS:
							setScreen( SCREEN_OPTIONS );
							break;

						case MAIN_MENU_CONTINUING_RECORDS:
							setScreen( SCREEN_RECORDS );
							break;

						case MAIN_MENU_CONTINUING_HELP:
							setScreen( SCREEN_HELP );
							break;

						case MAIN_MENU_CONTINUING_CREDITS:
							setScreen( SCREEN_CREDITS );
							break;

						//#if LOG == "true"
//# 				case MAIN_MENU_CONTINUING_LOG:
//# 					setScreen( SCREEN_LOG );
//# 					break;
						//#endif

						case MAIN_MENU_CONTINUING_EXIT:
							exit();
							break;
					}
				}
				else
				{
					switch( index )
					{
						case MAIN_MENU_NEW_GAME:
							setScreen( SCREEN_LOAD_GAME  );
							break;

						case MAIN_MENU_OPTIONS:
							setScreen( SCREEN_OPTIONS );
							break;

						case MAIN_MENU_RECORDS:
							setScreen( SCREEN_RECORDS );
							break;

						case MAIN_MENU_HELP:
							setScreen( SCREEN_HELP );
							break;

						case MAIN_MENU_CREDITS:
							setScreen( SCREEN_CREDITS );
							break;

						//#if LOG == "true"
//# 					case MAIN_MENU_LOG:
//# 						setScreen( SCREEN_LOG );
//# 						break;
						//#endif

						case MAIN_MENU_EXIT:
							exit();
							break;
					}
				}
				break; // fim case SCREEN_MAIN_MENU


			//#if LOG == "true"
//# 			case SCREEN_LOG:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 				break;
			//#endif

			case SCREEN_SPLASH_BRAND:
				setScreen( SCREEN_SPLASH_GAME );
				break;

			case SCREEN_CREDITS:
				setScreen( SCREEN_MAIN_MENU );
				break;

			case SCREEN_HELP:
				switch( index )
				{
					// CANCELED : Retiramos por falta de resposta da MTV
//					case HELP_MENU_INTRO:
//						setScreen( SCREEN_HELP_INTRO );
//						break;
						
					case HELP_MENU_RULES:
						setScreen( SCREEN_HELP_RULES );
						break;
						
					case HELP_MENU_SPECIAL_ICONS:
						setScreen( SCREEN_HELP_SPECIAL_ICONS );
						break;
						
					case HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
						break;
						
					case HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;

			case SCREEN_PAUSE:
				if( MediaPlayer.isVibrationSupported() )
				{
					switch( index )
					{
						case PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_UNPAUSE );
							break;

						//#ifndef NO_SOUND
						case PAUSE_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
						break;
						//#endif

						case PAUSE_MENU_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
							break;

						case PAUSE_MENU_BACK_TO_MENU:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_BACK_TO_MENU );
							break;

						case PAUSE_MENU_EXIT_GAME:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_EXIT_GAME );
							break;
					}
				}
				else
				{
					switch( index )
					{
						case PAUSE_MENU_NO_VIB_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_UNPAUSE );
							break;

						//#ifndef NO_SOUND
					case PAUSE_MENU_NO_VIB_TOGGLE_SOUND:
						MediaPlayer.setMute( !MediaPlayer.isMuted() );
						break;
						//#endif

						case PAUSE_MENU_NO_VIB_BACK_TO_MENU:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_BACK_TO_MENU );
							break;

						case PAUSE_MENU_NO_VIB_EXIT_GAME:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_EXIT_GAME );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE												 

			case SCREEN_OPTIONS:
				if( MediaPlayer.isVibrationSupported() )
				{
					switch( index )
					{
						//#ifndef NO_SOUND
						case OPTIONS_MENU_TOGGLE_SOUND:
							break;
						//#endif
							
						case OPTIONS_MENU_TOGGLE_VIBRATION:
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
							break;
							
						case OPTIONS_MENU_ERASE_RECORDS:
							setScreen( SCREEN_YN_ERASE_RECORDS );
							break;
							
						case OPTIONS_MENU_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				else
				{
					switch( index )
					{
						//#ifndef NO_SOUND
						case OPTIONS_MENU_NO_VIB_TOGGLE_SOUND:
							break;
						//#endif
							
						case OPTIONS_MENU_NO_VIB_ERASE_RECORDS:
							setScreen( SCREEN_YN_ERASE_RECORDS );
							break;
							
						case OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS


 			case SCREEN_YN_ERASE_RECORDS:
 				switch( index )
				{
 					case BasicConfirmScreen.INDEX_YES:
 						GameRecordsScreen.eraseRecords();

						// Evita alguns bugs como entrar na tela de recordes e estar piscando uma pontua��o previamente apagada
						GameRecordsScreen.stopBlink();
 						
 					case BasicConfirmScreen.INDEX_NO:
 						setScreen( SCREEN_OPTIONS );
						break;
 				}
				break;
				
			case SCREEN_YN_BACK_TO_MENU:
				switch( index )
				{
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						
						//#ifndef NO_SOUND
							//#if SAMSUNG_BAD_SOUND == "true"
//#									MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
							//#else
								MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
							//#endif
						//#endif

 						setScreen( SCREEN_MAIN_MENU );
						break;
 						
 					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_YN_EXIT_GAME:
				switch( index )
				{
					case BasicConfirmScreen.INDEX_YES:
 						GameMIDlet.exit();
						break;
 						
 					case BasicConfirmScreen.INDEX_NO:
 						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_ENABLE_SOUNDS:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				setScreen( SCREEN_CHANGE_BKG_COLOR );
				break;

		} // fim switch ( id )
	}

	public void onItemChanged( Menu menu, int id, int index )
	{
	}

	public static void gameOver( long score ) throws Exception
	{
		final GameMIDlet midlet = ( GameMIDlet )instance;
		
		// Precisamos desalocar o away do jogo e alocar o away do fundo dos menus
		midlet.freeBkgMemory();
		midlet.freePlayscreenMemory();
		
		// Aloca o background do menu
		midlet.manager.setBackground( midlet.createMenuBkg( true ), true );
		
		//#ifndef SIEMENS
			//#ifndef NO_SOUND
				//#if SAMSUNG_BAD_SOUND == "true"
//# 				MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
				//#else
					MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
				//#endif
			//#endif
		//#endif
		
		if( GameRecordsScreen.setScore( score ) )
			setScreen( SCREEN_RECORDS );
		else
			setScreen( SCREEN_MAIN_MENU );
	}

	/** Desaloca a tela de jogo e reseta as vari�veis est�ticas da classe PushItBar */
	private void freePlayscreenMemory()
	{
		if( playScreen != null )
		{
			playScreen.killAway();

			// - 1 => retiramos o Away da cole��o do jogo
			(( PushItBar )playScreen.getDrawable( PlayScreen.PLAYSCREEN_PUSHITBAR_INDEX - 1 )).destruct();

			playScreen = null;
		}
		System.gc();
		Thread.yield();
	}
	
	public static PushItBar getPushItBar()
	{
		final PlayScreen aux = (( GameMIDlet )getInstance()).playScreen;
		return aux != null ? ( PushItBar )aux.getDrawable( PlayScreen.PLAYSCREEN_PUSHITBAR_INDEX ) : null;
	}
	
	public static boolean isLowMemory()
	{
		//#if ( LOW_JAR == "true" )
//# 		return true;
		//#else
			return (( GameMIDlet )getInstance() ).lowMemory;
		//#endif
	}
	
	/** Obt�m o pr�ximo inteiro m�ltiplo de m */
	public static int nextMulOf( int n, int m )
	{
		final int aux = n % m;
		if( aux == 0 )
			return n;
		return n + ( m - aux );
	}
	
	public static String formatNumStr( long value, int nChars, char filler, char separator )
	{
		final char[] valueStr = String.valueOf( value ).toCharArray();

		final boolean hasSeparator = ( separator != '\0' );
		final char[] s = new char[ !hasSeparator ? nChars : nChars + ( nextMulOf( nChars, 3 ) / 3 ) - 1 ];

		int i, sCounter = s.length - 1;
		for( i = 0 ; i < nChars ; ++i )
		{
			if( hasSeparator && ( i > 0 ) && ( i % 3 == 0 ) )
				s[ sCounter-- ] = separator;
			
			if( i < valueStr.length )
				s[ sCounter-- ] = valueStr[ valueStr.length - i - 1 ];
			else
				s[ sCounter-- ] = filler;
		}

		return new String( s );
	}

	private final class LoadScreen extends Label implements Updatable
	{
		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;
		public static final byte SLEEP_TIME = 100;
		private long lastUpdateTime;
		private static final byte MAX_DOTS = 4;
		private byte dots;
		private final GameMIDlet midlet;
		private Thread loadThread;
		private final int nextScreen;
		private final int bkgInitColor, bkgFinalColor;
		private int currColor;
		private int colorChangeCounter;

		private LoadScreen( GameMIDlet midlet, int nextScreen, int bkgInitColor, int bkgFinalColor ) throws Exception
		{
			super( midlet.FONT_DEFAULT, GameMIDlet.getText( TEXT_LOADING ) );
			this.midlet = midlet;
			this.nextScreen = nextScreen;
			this.bkgInitColor = bkgInitColor;
			this.bkgFinalColor = bkgFinalColor;
			currColor = bkgInitColor;
			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
			lastUpdateTime = System.currentTimeMillis();
		}

		protected void paint( Graphics g )
		{
			// Coloca uma cor de fundo
			pushClip( g );
			g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

			g.setColor( currColor );

			g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			popClip( g );
		
			// Desenha o label
			super.paint( g );
		}

		public void update( int delta )
		{
			final long interval = System.currentTimeMillis() - lastUpdateTime;
			
			// Modifica a cor do background
			if( ( currColor & 0x00FFFFFF ) != ( bkgFinalColor & 0x00FFFFFF  ) )
			{
				colorChangeCounter += delta;
				
				int percentage = ( colorChangeCounter * 100 ) / GameMIDlet.LOAD_SCREEN_BKG_COLOR_CHANGE_TIME;
				if( percentage > 100 )
					percentage = 100;

				final int r = NanoMath.lerpInt( ( bkgInitColor & 0x00FF0000 ) >> 16, ( bkgFinalColor & 0x00FF0000 ) >> 16, percentage );
				final int g = NanoMath.lerpInt( ( bkgInitColor & 0x0000FF00 ) >>  8, ( bkgFinalColor & 0x0000FF00 ) >>  8, percentage );
				final int b = NanoMath.lerpInt( bkgInitColor & 0x000000FF, bkgFinalColor & 0x000000FF, percentage );
				currColor = 0xFF000000 | ( r << 16 ) | ( g << 8 ) | b;
			}
			else
			{
				GameMIDlet.blockScreenChange( false );
			}

			if( interval >= CHANGE_TEXT_INTERVAL )
			{
				// as imagens do jogo s�o carregadas aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if( loadThread == null )
				{
					// s� inicia a thread quando a tela atual for a ativa (ou seja, a transi��o da TV estiver encerrada)
					if( ScreenManager.getInstance().getCurrentScreen() == this )
					{
						loadThread = new Thread()
						{
							public final void run()
							{
								try
								{
									MediaPlayer.free();
									GameMIDlet.blockScreenChange( true );
									GameMIDlet.setScreen( nextScreen );
								}
								catch( Exception e )
								{
									// Sai do jogo
									exit();
								}
							}
						};
						loadThread.start();
					}
				}
				else
				{
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) % MAX_DOTS );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for( byte i = 0; i < dots; ++i )
					{
						temp += '.';
					}

					setText( temp );

					try
					{
						// Permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( SLEEP_TIME );
					}
					catch( Exception e )
					{
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
			}
		}
	}
	
	private	final class HelpPushItBar extends UpdatableGroup implements SpriteListener
	{
		private MUV animControl = new MUV( MIN_PUSHITS_SPEED );

		public HelpPushItBar() throws Exception
		{
			super( 7 );
			PushItBar.createHelpVersion( this );
		}

		public void update( int delta )
		{
			super.update( delta );
			
			if( !drawables[ PushItBar.INDEX_FIRST_PUSHIT ].isVisible() )
				return;
			
			final int dx = animControl.updateInt( delta );
			if( dx == 0 )
				return;
			
			if( drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getPosX() + dx >= ( ( getWidth() - drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getWidth() ) >> 1 ) )
			{
				drawables[ PushItBar.INDEX_FIRST_PUSHIT ].setVisible( false );
				drawables[ PushItBar.INDEX_FIRST_PUSHIT ].setPosition( -drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getWidth(), drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getPosY() );
				
				// Faz a anima��o do brilho
				final Sprite shine = ( Sprite )drawables[ PushItBar.INDEX_FIRST_PUSHIT + 1 ];
				shine.pause( false );
				shine.setVisible( true );
				shine.setSequence( 0 );
			}
			else
			{
				drawables[ PushItBar.INDEX_FIRST_PUSHIT ].move( dx, 0 );
			}
		}

		public void onSequenceEnded( int id, int sequence )
		{
			final Sprite shine = ( Sprite )drawables[ PushItBar.INDEX_FIRST_PUSHIT + 1 ];
			shine.pause( true );
			shine.setVisible( false );
			drawables[ PushItBar.INDEX_FIRST_PUSHIT ].setVisible( true );
		}

		public void onFrameChanged( int id, int frameSequenceIndex )
		{
		}
	}

	private final class MenuBkg extends UpdatableGroup implements SpriteListener
	{
		public MenuBkg( int slots ) throws Exception
		{
			super( slots );
		}

		public void onSequenceEnded( int id, int sequence )
		{
			// Possui 50% de chance de fazer o movimento para um lado diferente do atual
			if( NanoMath.randInt( 100 ) >= 50 )
				(( Sprite )drawables[ id ] ).mirror( Sprite.TRANS_MIRROR_H );
		}

		public void onFrameChanged( int id, int frameSequenceIndex )
		{
		}
	}
	
	private	final class ColorTransitionBkg extends Drawable implements Updatable
	{
		int currColor, counter;
		final int initColor, finalColor, totalTime, nextScreen;

		public ColorTransitionBkg( int colorFrom, int colorTo, int colorTransitionDur, int nextScreen )
		{
			initColor = colorFrom & 0x00FFFFFF;
			finalColor = colorTo & 0x00FFFFFF;
			currColor = initColor;

			totalTime = colorTransitionDur;
			this.nextScreen = nextScreen;

			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}

		public void update( int delta )
		{
			if( currColor != finalColor )
			{
				counter += delta;

				int percentage = ( counter * 100 ) / totalTime;
				if( percentage > 100 )
					percentage = 100;

				final int r = NanoMath.lerpInt( ( initColor & 0x00FF0000 ) >> 16, ( finalColor & 0x00FF0000 ) >> 16, percentage );
				final int g = NanoMath.lerpInt( ( initColor & 0x0000FF00 ) >>  8, ( finalColor & 0x0000FF00 ) >>  8, percentage );
				final int b = NanoMath.lerpInt( initColor & 0x000000FF, finalColor & 0x000000FF, percentage );
				currColor = ( r << 16 ) | ( g << 8 ) | b;
			}
			else
			{
				AppMIDlet.setScreen( nextScreen );
			}
		}

		protected void paint( Graphics g )
		{
			g.setColor( currColor );
			g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}
	}

	/** Retira o away e a pattern de fundo da mem�ria. Mantemos TITLE pois utilizamos esta imagem tamb�m
	 * durante a tela de pause */
	private void freeBkgMemory()
	{
		// Cancela a refer�ncia para o background dos menus
		manager.setBackground( null, false );
		
		// Pede para liberar mem�ria
		System.gc();
		
		// Libera o processador para tentar deixar o garbage collector agir
		Thread.yield();
	}
	
	public ImageFont getLongTextfont() throws Exception
	{
		final String path = ROOT_DIR + "fontText";
		return ImageFont.createMultiSpacedFont( path + IMG_EXT, path + FONT_DESC_EXT );
	}
	
	private String getAppVersion()
	{
		String version = getAppProperty( "MIDlet-Version" );
		if ( version == null )
			version = "1.0.0";
		return "<ALN_H>Vers�o " + version;
	}
	
	protected int changeScreen( int screen )
	{
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		//#if SCREEN_SIZE != "SMALL"

			// Algu�m pode ter mudado o espa�amento da fonte por fora
			if( !GameMIDlet.isBig() )
				midlet.FONT_DEFAULT.setCharOffset( GameMIDlet.DEFAULT_FONT_OFFSET );

		//#endif

		if( screen != currentScreen )
		{
			Drawable nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;
			
			int softKeyVisibleTime = 0;
			
			try
			{
				switch( screen )
				{
					case SCREEN_LOAD_RESOURCES:
						//#ifdef NO_SOUND
//#								nextScreen = new LoadScreen( midlet, SCREEN_SPLASH_NANO );
						//#else
							nextScreen = new LoadScreen( midlet, SCREEN_ENABLE_SOUNDS, DEFAULT_LIGHT_COLOR, DEFAULT_LIGHT_COLOR );
						//#endif
						break;

					case SCREEN_ENABLE_SOUNDS:
						if( !resourcesLoaded )
							loadImagesAndSounds();

						nextScreen = new MyBasicConfirmScreen( midlet, screen, FONT_DEFAULT, MENU_CURSOR, TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, -1, true, true );
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						break;
						
					case SCREEN_CHANGE_BKG_COLOR:
						manager.setBackgroundColor( -1 );
						nextScreen = new ColorTransitionBkg( DEFAULT_LIGHT_COLOR, 0, 300, SCREEN_SPLASH_NANO );
						break;

					case SCREEN_SPLASH_NANO:
						if( !resourcesLoaded )
							loadImagesAndSounds();

						nextScreen = new SplashNano( PATH_SPLASH, getText( TEXT_SPLASH_NANO ), SCREEN_SPLASH_BRAND );
						
						//#ifndef NO_SOUND
							//#if SAMSUNG_BAD_SOUND == "true"
//#									MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
							//#else
								MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
							//#endif
						//#endif
						break;

					case SCREEN_SPLASH_BRAND:
						nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, ROOT_DIR + "SplashMTV/mtv" + IMG_EXT, TEXT_SPLASH_MTV );
						break;

					case SCREEN_SPLASH_GAME:
						nextScreen = new SplashGame( FONT_DEFAULT );
						break;

					case SCREEN_MAIN_MENU:
						// Se est� vindo do splash do jogo...
						if( currentScreen == SCREEN_SPLASH_GAME )
						{
							// S� vai entrar aqui uma vez. Ser� a vez na qual TITLE ser� alocado
							manager.setBackground( createMenuBkg( true ), true );
						}
						// Se est� vindo da tela de pause -> voltar ao menu principal ? -> sim
						else if( currentScreen == SCREEN_YN_BACK_TO_MENU )
						{
							// Retira as imagens do background do menu de pausa da mem�ria
							freeBkgMemory();
							
							// Precisamos alocar o Away do menu e desalocar o Away do jogo
							playScreen.killAway();
							manager.setBackground( createMenuBkg( true ), true );
						}
						Thread.yield();
						
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;

						if( playScreen != null )
						{
							nextScreen = new BasicMenu( midlet, screen, FONT_DEFAULT, new int[]{
														  TEXT_NEW_GAME,
														  TEXT_CONTINUE,
														  TEXT_OPTIONS,
														  TEXT_RECORDS,
														  TEXT_HELP,
														  TEXT_CREDITS,
														  TEXT_EXIT,
														  //#if LOG == "true"
//# 												  TEXT_LOG,
														  //#endif							
													  }, MENU_ITEMS_SPACING, MAIN_MENU_CONTINUING_NEW_GAME, MAIN_MENU_CONTINUING_EXIT );
						}
						else
						{
							nextScreen = new BasicMenu( midlet, screen, FONT_DEFAULT, new int[]{
														  TEXT_NEW_GAME,
														  TEXT_OPTIONS,
														  TEXT_RECORDS,
														  TEXT_HELP,
														  TEXT_CREDITS,
														  TEXT_EXIT,
														  //#if LOG == "true"
//# 												  TEXT_LOG,
														  //#endif							
													  }, MENU_ITEMS_SPACING, MAIN_MENU_NEW_GAME, MAIN_MENU_EXIT );
						}
						(( BasicMenu )nextScreen ).setCursor( MENU_CURSOR, BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_LOAD_GAME:
						nextScreen = new LoadScreen( midlet, SCREEN_GAME, DEFAULT_LIGHT_COLOR, 0 );
						break;

					case SCREEN_OPTIONS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						if( MediaPlayer.isVibrationSupported() )
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_TURN_VIBRATION_OFF,
//# 																 TEXT_RECORDS,
//# 																 TEXT_BACK,
//# 															 }, OPTIONS_MENU_BACK, -1, -1, OPTIONS_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
							//#else
								nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
																	 TEXT_TURN_SOUND_OFF,
																	 TEXT_TURN_VIBRATION_OFF,
																	 TEXT_RECORDS,
																	 TEXT_BACK,
																 }, OPTIONS_MENU_BACK, OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, OPTIONS_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON, SOUND_INDEX_THEME,
																 //#if SAMSUNG_BAD_SOUND == "true"
//#																	SAMSUNG_LOOP_INFINITE
																 //#else
																	MediaPlayer.LOOP_INFINITE
																 //#endif
																 );
							//#endif
						}
						else
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_RECORDS,
//# 																 TEXT_BACK,
//# 															 }, OPTIONS_MENU_NO_VIB_BACK, -1, -1, -1, -1 );
							//#else
								nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
																	 TEXT_TURN_SOUND_OFF,
																	 TEXT_RECORDS,
																	 TEXT_BACK,
																 }, OPTIONS_MENU_NO_VIB_BACK, OPTIONS_MENU_NO_VIB_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1, SOUND_INDEX_THEME,
																 //#if SAMSUNG_BAD_SOUND == "true"
//#																	SAMSUNG_LOOP_INFINITE
																 //#else
																	MediaPlayer.LOOP_INFINITE
																 //#endif
																 );
							//#endif
						}
						(( BasicOptionsScreen )nextScreen ).setCursor( MENU_CURSOR, BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_CREDITS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getLongTextfont(), TEXT_CREDITS_TEXT, true );
						break;

					case SCREEN_HELP:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new BasicMenu( midlet, screen, FONT_DEFAULT, new int[]{
														  // CANCELED : Retiramos por falta de resposta da MTV
														  //TEXT_HELP_INTRO,
														  TEXT_HELP_RULES,
														  TEXT_HELP_SPECIAL_ICONS,
														  TEXT_HELP_CONTROLS,
														  TEXT_BACK
													  }, MENU_ITEMS_SPACING, HELP_MENU_RULES, HELP_MENU_BACK );
						
						(( BasicMenu )nextScreen ).setCursor( MENU_CURSOR, BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;
						
					// CANCELED : Retiramos por falta de resposta da MTV
//					case SCREEN_HELP_INTRO:
//						indexSoftRight = ICON_BACK;
//						indexSoftLeft = ICON_OK;
//						
//						nextScreen = new BasicTextScreen( SCREEN_HELP, getLongTextfont(), TEXT_HELP_TEXT_INTRO, false );
//						(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
//						(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
//						break;

					case SCREEN_HELP_RULES:
						{
							indexSoftRight = ICON_BACK;
							indexSoftLeft = ICON_OK;

							final Drawable[] specialCharsA = new Drawable[7];

							specialCharsA[0] = new HelpPushItBar();

							//#if SCREEN_SIZE != "SMALL"
							final String path = ROOT_DIR + ( GameMIDlet.isBig() ? "fbb" : "fb" );
							//#else
//# 								final String path = ROOT_DIR + "fb";
							//#endif
							for( int i = 1 ; i < specialCharsA.length ; ++i )
							{
								specialCharsA[i] = new DrawableImage( path + ( i - 1 ) + IMG_EXT );
								Thread.yield();
							}

							nextScreen = new BasicTextScreen( SCREEN_HELP, getLongTextfont(), getText( TEXT_HELP_TEXT_RULES ) + getAppVersion(), false, specialCharsA );
							(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
							(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
							nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - icons[ ICON_BACK ].getHeight() );
						}
						break;

					case SCREEN_HELP_SPECIAL_ICONS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;

						final Drawable[] specialCharsB = new Drawable[2];
						specialCharsB[0] = new DrawableImage( ROOT_DIR + "s" + IMG_EXT );
						Thread.yield();
						
						specialCharsB[1] = new DrawableImage( ROOT_DIR + "q" + IMG_EXT );
						Thread.yield();

						nextScreen = new BasicTextScreen( SCREEN_HELP, getLongTextfont(), getText( TEXT_HELP_TEXT_SPECIAL_ICONS ) + getAppVersion(), false, specialCharsB );
						(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
						(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
						nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - icons[ ICON_BACK ].getHeight() );
						break;
						
					case SCREEN_HELP_CONTROLS:
						{
							indexSoftRight = ICON_BACK;
							indexSoftLeft = ICON_OK;

							final Drawable[] specialCharsC = new Drawable[5];

							//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
								final String path = ROOT_DIR + ( manager.hasPointerEvents() ? "ts" : "" );
							//#else
//# 								final String path = ROOT_DIR;
							//#endif
							for( int i = 2 ; i <= 8 ; i += 2 )
							{
								specialCharsC[ ( i >> 1 ) - 1 ] = new DrawableImage( path + i + IMG_EXT );
								Thread.yield();
							}

							specialCharsC[4] = new DrawableImage( ROOT_DIR + "s" + IMG_EXT );
							Thread.yield();

							nextScreen = new BasicTextScreen( SCREEN_HELP, getLongTextfont(), getText( TEXT_HELP_TEXT_CONTROLS ) + getAppVersion(), false, specialCharsC );
							(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
							(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
							nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - icons[ ICON_BACK ].getHeight() );
						}
						break;

					case SCREEN_PAUSE:
						if( manager.getBackground() == null )
							manager.setBackground( createMenuBkg( false ), false );

						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						if( MediaPlayer.isVibrationSupported() )
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_CONTINUE,
//# 																 TEXT_TURN_VIBRATION_OFF,
//# 																 TEXT_BACK_MENU,
//# 																 TEXT_EXIT_GAME,
//# 															 }, PAUSE_MENU_CONTINUE, -1, -1, PAUSE_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
							//#else
								nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
																	 TEXT_CONTINUE,
																	 TEXT_TURN_SOUND_OFF,
																	 TEXT_TURN_VIBRATION_OFF,
																	 TEXT_BACK_MENU,
																	 TEXT_EXIT_GAME,
																 }, PAUSE_MENU_CONTINUE, PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, PAUSE_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON, -1, -1 );
							//#endif
						}
						else
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_CONTINUE,
//# 																 TEXT_BACK_MENU,
//# 																 TEXT_EXIT_GAME,
//# 															 }, PAUSE_MENU_NO_VIB_CONTINUE, -1, -1, -1, -1 );
							//#else
								nextScreen = new BasicOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
																	 TEXT_CONTINUE,
																	 TEXT_TURN_SOUND_OFF,
																	 TEXT_BACK_MENU,
																	 TEXT_EXIT_GAME,
																 }, PAUSE_MENU_NO_VIB_CONTINUE, PAUSE_MENU_NO_VIB_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1, -1, -1 );
							//#endif
						}
						(( BasicOptionsScreen )nextScreen ).setCursor( MENU_CURSOR, BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_YN_BACK_TO_MENU:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new MyBasicConfirmScreen( midlet, screen, FONT_DEFAULT, MENU_CURSOR, TEXT_BACK_MENU_2, TEXT_YES, TEXT_NO, -1, false, false );
						break;
						
					case SCREEN_YN_EXIT_GAME:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new MyBasicConfirmScreen( midlet, screen, FONT_DEFAULT, MENU_CURSOR, TEXT_EXIT_GAME, TEXT_YES, TEXT_NO, -1, false, false );
						break;
						
					case SCREEN_YN_ERASE_RECORDS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new MyBasicConfirmScreen( midlet, screen, FONT_DEFAULT, MENU_CURSOR, TEXT_ERASE_RECORDS, TEXT_YES, TEXT_NO, -1, false, false );
						break;
 
 					case SCREEN_RECORDS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = GameRecordsScreen.createInstance( FONT_DEFAULT );
						break;

					//#if LOG == "true"
//# 					case SCREEN_LOG:
//# 						Logger.log( log );
//# 						nextScreen = Logger.getLogScreen( SCREEN_MAIN_MENU, getLongTextfont() );
//# 						(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
//# 						(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
//# 						break;
					//#endif

					// Vai do menu principal para o jogo
					case SCREEN_GAME:
						// Precisamos desalocar o away do menu, pois iremos alocar o away do jogo
						freeBkgMemory();
						freePlayscreenMemory();
						
						playScreen = new PlayScreen();
						nextScreen = playScreen;

						// � poss�vel que o bot�o de pause tenha ficado invis�vel por n�o caber em alguma tela
						//#if SCREEN_SIZE == "SMALL"
//# 						if( PlayScreen.cancelBtPause() )
//# 							icons[ GameMIDlet.ICON_PAUSE ].setVisible( true );
						//#endif

						indexSoftRight = ICON_PAUSE;
						break;
					
					// Volta do menu principal para o jogo, logo precisa alocar o away do jogo novamente
					case SCREEN_CONTINUE:
						MediaPlayer.stop();

						// Precisamos desalocar o away do menu...
						freeBkgMemory();

						// ... pois iremos alocar o away do jogo
						playScreen.reviveAway();
						
						// Sem break mesmo

					// Volta do menu de pause para o jogo
					case SCREEN_UNPAUSE:

						nextScreen = playScreen;
						playScreen.unpause();

						indexSoftRight = ICON_PAUSE;
						break;

				} // fim switch ( screen )

				if( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft == SOFT_KEY_REMOVE ? null : icons[ indexSoftLeft ] );

				if( indexSoftRight != SOFT_KEY_DONT_CHANGE )
					midlet.manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight == SOFT_KEY_REMOVE ? null : icons[ indexSoftRight ] );
			}
			catch( Exception e )
			{
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, ent�o sai do jogo
				exit();
			}

			if( nextScreen != null )
			{
				while( blockScreenChange )
				{
					try
					{
						Thread.sleep( LoadScreen.SLEEP_TIME );
					}
					catch( Exception ex )
					{
						ex.printStackTrace();
					}
				}
					
				midlet.manager.setCurrentScreen( nextScreen );
				return screen;
			}
			else
			{
				return -1;
			}
		} // fim if ( screen != currentScreen )
		return screen;
	}
}
