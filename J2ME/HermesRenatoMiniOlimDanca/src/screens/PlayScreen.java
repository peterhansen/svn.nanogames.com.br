/**
 * PlayScreen.java
 */
package screens;

//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import core.GameMIDlet;
import game.PushItBar;
import game.PushItBarListener;
import game.ScoreBar;
import javax.microedition.lcdui.Graphics;

/**
 * �2007 Nano Games
 * @author Daniel L. Alves
 */
public final class PlayScreen extends UpdatableGroup implements KeyListener, Constants, PushItBarListener, SpriteListener
//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	// N�o h� aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do c�digo)
, PointerListener
//#endif
{
	//<editor-fold defaultstate="collapsed" desc="Posicionamento dos elementos da tela de jogo">
	
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
	
		/** Dist�ncia em pixels da barra dos pushIts para o topo da tela */
		private final byte PUSHIT_BAR_DIST_FROM_TOP = 12;

		/** Dist�ncia em pixels do label dos hits para o canto direito da tela */
		private final byte LABEL_HITS_DIST_FROM_RIGHT = 3;
		
		/** Dist�ncia em pixels das imagens de feedback de acerto para o label dos hits */
		private final byte FEEDBACK_IMGS_DIST_FROM_HITS_LB = 3;

	//#else
//# 	
//# 		/** Dist�ncia em pixels da barra dos pushIts para o topo da tela */
//# 		private final byte PUSHIT_BAR_DIST_FROM_TOP = 8;
//# 
//# 		/** Dist�ncia em pixels do label dos hits para o canto direito da tela */
//# 		private final byte LABEL_HITS_DIST_FROM_RIGHT = 3;
//# 	
//# 		/** Dist�ncia em pixels das imagens de feedback de acerto para o label dos hits */
//# 		private final byte FEEDBACK_IMGS_DIST_FROM_HITS_LB = 3;
//# 
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Estados do jogo">

	// Estados do jogo
	private final byte STATE_RUNNING				= 10;
	private final byte STATE_PAUSED					= 1;
	private final byte STATE_LEVEL_ENDED			= 2; // Retira alguns elementos da tela de jogo
	private final byte STATE_MEDALS_SCREEN			= 3; // Exibe o ranking informando se o jogador passou de fase
	private final byte STATE_MEDALS_SCREEN_WAITING	= 4; // Espera o jogador fornecer qualquer input para passarmos para o pr�ximo estado
	private final byte STATE_TRANSITION_1			= 5; // Torna a tela preta
	private final byte STATE_SHOWING_RESULT			= 6; // Informa se o jogador passou ou n�o de fase
	private final byte STATE_TRANSITION_2			= 7; // Levanta a cortina preta
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	private final byte STATE_TS_TUTORIAL			= 8; // Explica como jogar em aparelhos touch screen
	private final byte STATE_TS_TUTORIAL_EXIT		= 9; // Explica como jogar em aparelhos touch screen
	//#endif
	
	/** Estado atual do jogo */
	private byte currState;
	
	/** Estado anterior ao atual. Necess�rio para voltarmos do estado STATE_PAUSED */
	private byte prevState;
	
	//</editor-fold>

	/** Fase atual do jogo */
	private short level = -1;
	
	/** Espa�amento da fonte dos hits */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte FONT_HITS_CHAR_OFFSET = -1;
	//#else
//# 	private final byte FONT_HITS_CHAR_OFFSET = -2;
	//#endif
	
	//<editor-fold defaultstate="collapsed" desc="Transi��es">
	
	/** Controla a transi��o de fases */
	private final MUV transitionControl = new MUV();
	
	/** �ltimo valor gerado por <code>transitionControl</code> */
	private int lastTransitionDx;
	
	/** Em quantas fatias a tela ser� cortada na transi��o de fases */
	private final byte TRANSITION_N_SLICES = 10;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos Drawables do Grupo">

	private final byte PLAYSCREEN_BACKGROUND_INDEX				= 0;
	private final byte PLAYSCREEN_FLOOR_INDEX					= 1;
	private final byte PLAYSCREEN_CROWD_INDEX					= 2;
	private final byte PLAYSCREEN_AWAY_INDEX					= 3;
	private final byte PLAYSCREEN_SCOREBAR_INDEX				= 4;
	public static final byte PLAYSCREEN_PUSHITBAR_INDEX			= 5;
	private final byte PLAYSCREEN_LABEL_HITS_INDEX				= 6;
	private final byte PLAYSCREEN_LABEL_BONUS_INDEX				= 7;
	private final byte PLAYSCREEN_FEEDBACK_PERFECT_INDEX		= 8;
	private final byte PLAYSCREEN_FEEDBACK_GREAT_INDEX			= 9;
	private final byte PLAYSCREEN_FEEDBACK_GOOD_INDEX			= 10;
	private final byte PLAYSCREEN_FEEDBACK_AVERAGE_INDEX		= 11;
	private final byte PLAYSCREEN_FEEDBACK_BAD_INDEX			= 12;
	private final byte PLAYSCREEN_FEEDBACK_MISS_INDEX			= 13;
	private final byte PLAYSCREEN_RESULT_LABEL_INDEX			= 14;
	private final byte PLAYSCREEN_MEDAL_SCREEN_BKG				= 15;
	private final byte PLAYSCREEN_MEDAL_SCREEN_TITLE			= 16;
	private final byte PLAYSCREEN_MEDAL_SCREEN_FIRST_NAME_LB	= 17;
	private final byte PLAYSCREEN_MEDAL_SCREEN_LAST_NAME_LB		= PLAYSCREEN_MEDAL_SCREEN_FIRST_NAME_LB + N_COMPETITORS - 1;
	private final byte PLAYSCREEN_MEDAL_SCREEN_FIRST_GRADE_LB	= PLAYSCREEN_MEDAL_SCREEN_LAST_NAME_LB + 1;
	private final byte PLAYSCREEN_MEDAL_SCREEN_LAST_GRADE_LB	= PLAYSCREEN_MEDAL_SCREEN_FIRST_GRADE_LB + N_COMPETITORS - 1;
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	private final byte PLAYSCREEN_TS_STAR_INDEX					= PLAYSCREEN_MEDAL_SCREEN_LAST_GRADE_LB + 1;
	private final byte PLAYSCREEN_TS_TUTORIAL_LABEL_HINT		= PLAYSCREEN_TS_STAR_INDEX + 1;
	//#endif
	
	//</editor-fold>
	
	/** Personagem principal do jogo */
	private Sprite away;
	
	/** Barra que cont�m os bot�es a serem a pertados */
	private final PushItBar pushItBar;
	
	/** Conta quantos hits ( acertos consecutivos ) o jogador est� fazendo */
	private short hitCount;
	
	/** Conta o n�mero de aceertos totais da fase */
	private short levelHits;
	
	/** Multiplicador de pontos atual */
	private short bonusMultiplier;
	
	/** Indica quando devemos aumentar o multiplicador b�nus */
	private short bonusMultiplierCounter;
	
	/** Label que exibe o n�mero de hits atual */
	private final Label labelHits;
	
	/** Label que exibe o b�nus multiplicador de pontos */
	private final RichLabel labelBonusMultiplier;
	
	/** Barra que exibe os pontos do jogador */
	private final ScoreBar scoreBar;
	
	/** Controla a anima��o do ch�o */
	private int floorAnimCounter;
	
	/** Controla a anima��o do background */
	private int bkgAnimCounter;
	
	/** N�mero m�ximo de backgrounds que podemos sortear */
	private final byte N_BKGS = 5;
	
	/** Poss�veis backgrounds para a tela de jogo */
	private Drawable[] bkgs;
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos Backgrounds">
	
	private final byte BKG_LINES		= 0;
	private final byte BKG_FLOWER		= 1;
	private final byte BKG_RECTANGLE	= 2;
	private final byte BKG_GREENBALL	= 3;
	private final byte BKG_WEATHERVANE	= 4;
	
	//</editor-fold>
	
	/** Parte do label de b�nus que nunca muda */
	private final String bonusStr;
	
	/** N�mero de bot�es exibidos na tela na vers�o touch screen */
	private final byte N_TOUCH_SCREEN_BUTTONS = 5;
	
	/** Armazena o tipo do �ltimo acerto. Auxilia na contagem de pontos dos rastros */
	private byte lastHitStatus = PushItBar.PUSHIT_HIT_MISS;
	
	/** Controla o tempo de visibilidade das imagens que indicam o tipo acerto */
	private short feedBackCounter;
	
	/** Largura da tela de medalhas */
	private final int medalsScreenTotalWidth;
		
	/** Posi��o dos labels em x na tela de medalhas */
	private final int medalsScreenLabelsPosX;
	
	/** De quanto em quanto tempo o label do jogador pisca */
	private final short MEDALS_SCREEN_BLINK_ANIM_TIME = 250;
	
	/** Tempo de dura��o da tela que indica se o jogador perdeu ou passou para a pr�xima fase */
	private final short RESULT_SCREEN_DUR = 3000;
	
	/** Coloca��o obtida pelo jogador na fase atual */
	private byte playerPos;
	
	/** Gravam as informa��es sobre a anima��o atual do personagem. Assim, quando chamarmos reviveAway(),
	 * conseguiremos voltar do mesmo ponto em que paramos */
	private short awayFrameTimeBeforeGoingToMainMenu;
	private int awaySeqBeforeGoingToMainMenu, awayFrameBeforeGoingToMainMenu;
	private boolean awayPausedBeforeGoingToMainMenu, awayVisibleBeforeGoingToMainMenu, awayMirroredBeforeGoingToMainMenu;
	
	//#ifndef NO_SOUND
	//#ifndef SmallGF500
	/** Som que est� sendo tocado na fase atual */
	private byte currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_0 - 1;
	//#endif
	//#endif
	
	// Pr�xima sequ�ncia a ser executada pelo Away
	private byte awayNextSeq = -1;
	
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	
	/** Poss�veis retornos de peterTriangleMethod() */
	private final byte TRIANGLE_NONE	= -1; // Inicializa��o
	private final byte TRIANGLE_TOP		=  0;
	private final byte TRIANGLE_LEFT	=  1;
	private final byte TRIANGLE_RIGHT	=  2;
	private final byte TRIANGLE_BOTTOM	=  3;
	private final byte TRIANGLE_STAR	=  4; // Nome feio mas que server ao seu prop�sito
	
	private byte lastPressed = TRIANGLE_NONE;
	
	// Tratam a anima��o das �reas da tela de toque
	private final short TOUCH_SCREEN_AREA_COLOR_TIME = 100;
	private final byte TOUCH_SCREEN_AREA_N_COLORS = 6;

	private final short[] counterTsAreaColor = { -1, -1, -1, -1 };
		
	final int tsAreaColors[][] = new int[][]{
												{ 0xFFFFDF05, 0xFFFFE90E, 0xFFFFF605, 0xFFFFFD33, 0xFFFFFB86, 0xFFFFFDC6 },
												{ 0xFF0072FF, 0xFF0099FF, 0xFF00BDFF, 0xFF00CFFF, 0xFF8BE2FF, 0xFFC5F1FF },
												{ 0xFFDE5E00, 0xFFFF6C00, 0xFFFF8A00, 0xFFFFA200, 0xFFFFC869, 0xFFFCE498 },
												{ 0xFF049A01, 0xFF04B900, 0xFF05CE00, 0xFF37E23F, 0xFF84FF8A, 0xFFCBFFD2 }
											};
	
	private final boolean hasPointerEvents = ScreenManager.getInstance().hasPointerEvents();
	
	/** Conserta um problema de sincronia de eventos */
	private boolean receivedPointerPressed;
		
	//#endif
	
	/** Construtor */
	public PlayScreen() throws Exception
	{
		// Inicializa o grupo
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			super( 19 + ( N_COMPETITORS << 1 ) );
		//#else
//# 			super( 17 + ( N_COMPETITORS << 1 ) );
		//#endif
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		floorAnimCounter = MIN_FLOOR_DUR + NanoMath.randInt( MAX_FLOOR_DUR - MIN_FLOOR_DUR + 1 );
		bonusStr = GameMIDlet.getText( TEXT_BONUS );
		
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		if( GameMIDlet.isLowMemory() )
//# 			bkgs = new Pattern[ 1 ];
//# 		else
//# 			bkgs = new BasicAnimatedPattern[ N_BKGS ];
		//#else
			bkgs = new BasicAnimatedPattern[ N_BKGS ];
		//#endif

		// Aloca os backgrounds
		createBkgs();
		
		// Aloca o ch�o
		String path = ROOT_DIR + "floor";
		Drawable floorImg;
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		if( GameMIDlet.isLowMemory() )
//# 			floorImg = new DrawableImage( path + "_0" + IMG_EXT );
//# 		else
//# 			floorImg = new Sprite( path + SPRITE_DESC_EXT, path );
		//#else
		floorImg = new Sprite( path + SPRITE_DESC_EXT, path );
		//#endif
			
		Thread.yield();
		final Pattern floor = new Pattern( floorImg );
		insertDrawable( floor );
		// +1 => Evita erros de precis�o de ScreenManager.SCREEN_HALF_HEIGHT
		floor.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT + 1 );
		floor.setPosition( 0, ScreenManager.SCREEN_HALF_HEIGHT );
		
		// Aloca a plat�ia
		Drawable crowdImg;
		path = ROOT_DIR + "dancers";
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		if( GameMIDlet.isLowMemory() )
//# 		{
//# 			crowdImg = new DrawableImage( path + "_0" + IMG_EXT );
//# 		}
//# 		else
//# 		{
		//#endif
			crowdImg = new Sprite( path + SPRITE_DESC_EXT, path );
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		}
		//#endif
		Thread.yield();
		final Pattern crowd = new Pattern( crowdImg );
		insertDrawable( crowd );
		crowd.setSize( ScreenManager.SCREEN_WIDTH, crowdImg.getHeight() );
		crowd.setPosition( 0, ( ScreenManager.SCREEN_HEIGHT - crowd.getHeight() ) >> 1 );
	
		// Aloca o personagem principal do jogo
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
		path = ROOT_DIR + ( GameMIDlet.isLowMemory() ? "AwaySml" : "Away" ) + "/away";
		//#else
//# 		path = ROOT_DIR + "Away/away";
		//#endif
		away = new Sprite( path + SPRITE_DESC_EXT, path );
		Thread.yield();
		insertDrawable( away );
		
		final int awayPosY = ScreenManager.SCREEN_HEIGHT < 127 ? ScreenManager.SCREEN_HEIGHT - away.getHeight() : crowd.getPosY() + (( ScreenManager.SCREEN_HEIGHT - crowd.getPosY() - away.getHeight() ) >> 1 );
		away.setPosition( ( ScreenManager.SCREEN_WIDTH - away.getWidth() ) >> 1, awayPosY );
		away.setListener( this, PLAYSCREEN_AWAY_INDEX );
		
		// Aloca a barra dos pontos
		scoreBar = new ScoreBar();
		insertDrawable( scoreBar );
		
		Thread.yield();
		
		// Aloca a barra dos pushIts
		final Drawable borderImg = (( Pattern )scoreBar.getDrawable( ScoreBar.INDEX_TOP_BAR )).getFill();
		pushItBar = new PushItBar( borderImg, this );
		insertDrawable( pushItBar );
		
		Thread.yield();

		// Aloca o label dos hits
		//#if SCREEN_SIZE != "SMALL"
		if( GameMIDlet.isBig() )
			path = ROOT_DIR + "fontHitsBig";
		else
		//#endif
			path = ROOT_DIR + "fontHits";
		final ImageFont fontHits = ImageFont.createMultiSpacedFont( path + IMG_EXT, path + FONT_DESC_EXT );
		Thread.yield();

		fontHits.setCharOffset( FONT_HITS_CHAR_OFFSET );
		labelHits = new Label( fontHits, "" );
		insertDrawable( labelHits );
		labelHits.setVisible( false );
		Thread.yield();
	
		// Aloca o label do b�nus
		//#if SCREEN_SIZE != "SMALL"
		if( GameMIDlet.isBig() )
			path = ROOT_DIR + "fontBonusBig";
		else
		//#endif
			path = ROOT_DIR + "fontBonus";

		labelBonusMultiplier = new RichLabel( ImageFont.createMultiSpacedFont( path + IMG_EXT, path + FONT_DESC_EXT ), bonusStr + String.valueOf( MAX_BONUS_MULIPLIER ), 0, getBonusLabelSpecialChars() );
		insertDrawable( labelBonusMultiplier );
		labelBonusMultiplier.setVisible( false );
		setBonusMultiplier( 1 );
		Thread.yield();
		
		// Posiciona alguns elementos
		pushItBar.setPosition( 0, PUSHIT_BAR_DIST_FROM_TOP );
		scoreBar.setPosition( 0 , pushItBar.getPosY() - scoreBar.getHeight() + pushItBar.getDrawable( PushItBar.INDEX_BAR_TOP ).getPosY() );
		
		final int pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();
		labelHits.setPosition( ScreenManager.SCREEN_WIDTH - labelHits.getWidth() - LABEL_HITS_DIST_FROM_RIGHT, pushItBarEnd );
		labelBonusMultiplier.setPosition( 0, pushItBarEnd );
		Thread.yield();
		
		// Aloca as imagens de feedback do tipo de acerto
		final int feedBackY = labelHits.getPosY() + labelHits.getHeight() + FEEDBACK_IMGS_DIST_FROM_HITS_LB;
		
		//#if SCREEN_SIZE != "SMALL"
			path = ROOT_DIR + ( GameMIDlet.isBig() ? "fbb" : "fb" );
		//#else
//# 			path = ROOT_DIR + "fb";
		//#endif

		for( byte i = 0 ; i < PushItBar.PUSHIT_N_HIT_TYPES ; ++i )
		{
			final DrawableImage img = new DrawableImage( path + i + IMG_EXT );
			insertDrawable( img );
			img.setVisible( false );
			img.setPosition( ScreenManager.SCREEN_WIDTH - img.getWidth() - LABEL_HITS_DIST_FROM_RIGHT, feedBackY );
			Thread.yield();
		}
		
		// Cria o label que ir� exibir a mensagem informando se o jogador passou ou n�o de fase
		final ImageFont mainFont = (( GameMIDlet )GameMIDlet.getInstance() ).FONT_DEFAULT;
		final RichLabel resultLabel = new RichLabel( mainFont, "", ScreenManager.SCREEN_WIDTH, null );
		insertDrawable( resultLabel );
		resultLabel.setVisible( false );
		Thread.yield();
		
		// Aloca a trama de preto e magenta
		final Pattern falseAlpha = new Pattern( new DrawableImage( ROOT_DIR + "Splash/transparency" + IMG_EXT ) );
		insertDrawable( falseAlpha );
		falseAlpha.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		falseAlpha.setVisible( false );
		Thread.yield();
		
		// Aloca as medalhas
		final DrawableImage[] medals = new DrawableImage[ N_MEDALS ];
		path = ROOT_DIR + "md";
		for( byte i = 0 ; i < N_MEDALS ; ++i )
		{
			medals[i] = new DrawableImage( path + i + IMG_EXT );
			Thread.yield();
		}
		
		// Aloca o t�tulo da tela de medalhas
		final Label medalsScreenTitle = new Label( mainFont, GameMIDlet.getText( TEXT_RANKING ) );
		insertDrawable( medalsScreenTitle );
		medalsScreenTitle.setVisible( false );
		Thread.yield();
		
		// Utiliza medals[0].getHeight() ao inv�s de mainFont.getHeight() pois a medalha ser� o "caraceter"
		// mais alto. ( getNCompetitorsInUse() + 1 ) => Competidores + T�tulo
		final int inc = ( medals[0].getHeight() + MENU_ITEMS_SPACING );
		final int totalSize = ( ( getNCompetitorsInUse() + 1 ) * ( medals[0].getHeight() + MENU_ITEMS_SPACING ) ) - MENU_ITEMS_SPACING;
		medalsScreenTitle.setPosition( ( ScreenManager.SCREEN_WIDTH - medalsScreenTitle.getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - totalSize ) >> 1 );

		// Aloca os labels dos competidores
		for( byte j = 0 ; j < 2 ; ++j ) // (1)Labels dos nomes ; (2)Labels das notas
		{
			int y = medalsScreenTitle.getPosY() + inc;
			for( byte i = 0 ; i < N_COMPETITORS ; ++i )
			{
				final RichLabel l = new RichLabel( mainFont, "", 0, medals );
				insertDrawable( l );
				l.setPosition( 0, y );
				l.setVisible( false );
				l.setSize( ScreenManager.SCREEN_WIDTH, medals[0].getHeight() );
				y += inc;
				Thread.yield();
			}
		}
		
		// Verifica o nome mais largo
		int largestName = 0;
		for( byte i = 0 ; i < getNCompetitorsInUse() ; ++i )
		{
			final int nameWidth = mainFont.getTextWidth( GameMIDlet.getText( TEXT_COMPETITOR_0 + i ) );
			if( nameWidth > largestName )
				largestName = nameWidth;
		}

		// Obt�m a soma mais larga de labels
		medalsScreenTotalWidth = medals[0].getWidth() + mainFont.getCharWidth( ' ' ) + largestName + MEDAL_SCREEN_LABELS_X_SPACEMENT + mainFont.getTextWidth( "100" );
		
		// Verifica onde os labels estar�o posicionados em X
		medalsScreenLabelsPosX = ( ScreenManager.SCREEN_WIDTH - medalsScreenTotalWidth ) >> 1;
		
		Thread.yield();
		
		// Se possui touch screen aloca imagens para representar os bot�es na tela
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )

		if( hasPointerEvents )
		{
			// Estrela touchscreen
			path = ROOT_DIR + "tss";
			final Sprite tsStarButton = new Sprite( path + SPRITE_DESC_EXT, path );
			insertDrawable( tsStarButton );
			tsStarButton.setPosition( 0, ScreenManager.SCREEN_HEIGHT - tsStarButton.getHeight() );
			Thread.yield();
			
			// Label utilizado na tela de tutorial
			final RichLabel tsTutorialHint = new RichLabel( mainFont, "", ScreenManager.SCREEN_WIDTH - 8, new Drawable[]{ pushItBar.pushIt2, pushItBar.pushIt4, pushItBar.pushIt6, pushItBar.pushIt8, pushItBar.pushItStar, new DrawableImage( path + "s" + IMG_EXT ) } );
			insertDrawable( tsTutorialHint );
			tsTutorialHint.setVisible( false );
			Thread.yield();
		}

		//#endif
		
		setState( STATE_SHOWING_RESULT );
	}
	
	/** Aloca os backgrounds do jogo. Ficaremos alternando por eles aleatoriamente */
	private void createBkgs() throws Exception
	{
		final String dir = ROOT_DIR + "Bkg/";
		
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		if( GameMIDlet.isLowMemory() )
//# 		{
			//#ifdef SmallS40
//# 			bkgs[0] = new Pattern( 0 );
			//#else
//# 			bkgs[0] = new Pattern( new DrawableImage( dir + "0" + IMG_EXT ));
			//#endif
//# 			Thread.yield();
//# 			insertDrawable( bkgs[0] );
//# 			bkgs[0].setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
//# 		}
//# 		else
//# 		{
		//#endif
			for( byte i = 0 ; i < N_BKGS ; ++i )
			{
				Drawable bkgImg;
				if( i < BKG_GREENBALL )
				{
					bkgImg = new DrawableImage( dir + i + IMG_EXT );
				}
				else
				{
					final String path = dir + i;
					bkgImg = new Sprite( path + SPRITE_DESC_EXT, path );
				}
				Thread.yield();

				bkgs[ i ] = new BasicAnimatedPattern( bkgImg, MIN_BKG_SPEED + NanoMath.randInt( MAX_BKG_SPEED - MIN_BKG_SPEED + 1 ), MIN_BKG_SPEED + NanoMath.randInt( MAX_BKG_SPEED - MIN_BKG_SPEED + 1 ) );
				(( BasicAnimatedPattern )bkgs[ i ] ).setAnimation( NanoMath.randInt( BasicAnimatedPattern.TOTAL_ANIMATIONS - 1 ) + 1 );
				bkgs[ i ].setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
			}

			insertDrawable( bkgs[ NanoMath.randInt( N_BKGS ) ] );
			bkgAnimCounter = MIN_BKG_DUR + NanoMath.randInt( MAX_BKG_DUR - MIN_BKG_DUR + 1 );
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		}
		//#endif
	}
	
	/** Aloca os caracteres especiais do label de b�nus */
	private Drawable[] getBonusLabelSpecialChars() throws Exception
	{
		final Drawable[] ret = new Drawable[ 2 ];
		
		//#if SCREEN_SIZE != "SMALL"
		if( GameMIDlet.isBig() )
		{
			ret[ 0 ] = new DrawableImage( ROOT_DIR + "bonusBig" + IMG_EXT );
			Thread.yield();

			ret[ 1 ] = new DrawableImage( ROOT_DIR + "xBonusBig" + IMG_EXT );
			Thread.yield();
		}
		else
		{
		//#endif
			ret[ 0 ] = new DrawableImage( ROOT_DIR + "bonus" + IMG_EXT );
			Thread.yield();
 
			ret[ 1 ] = new DrawableImage( ROOT_DIR + "xBonus" + IMG_EXT );
			Thread.yield();
		//#if SCREEN_SIZE != "SMALL"
		}
		//#endif

		return ret;
	}
	
	/** Inicializa os membros da classe para o in�cio da nova fase */
	private void onNewLevel() throws Exception
	{
		// Atualiza as vari�veis de controle
		levelHits = 0;
		playerPos = -1;
		
		// Atualiza a fase atual
		level = ( short )( level + 1 );
		if( level > MAX_LEVEL )
			level = MAX_LEVEL;

		// Apaga os labels das medalhas, a trama de "alpha" e o label do resultado da fase
		for( byte i = PLAYSCREEN_RESULT_LABEL_INDEX ; i <= PLAYSCREEN_MEDAL_SCREEN_LAST_GRADE_LB ; ++i )
			drawables[ i ].setVisible( false );
		
		// Exibe os labels que devem estar vis�veis
		if( hitCount > 0 )
			labelHits.setVisible( true );

		if( bonusMultiplier > 1 )
			labelBonusMultiplier.setVisible( true );
		
		// Deixa o personagem vis�vel novamente
		awayFrameBeforeGoingToMainMenu = 0;
		awayFrameTimeBeforeGoingToMainMenu = 0;
		awayPausedBeforeGoingToMainMenu = false; 
		awayVisibleBeforeGoingToMainMenu = true;
		awayMirroredBeforeGoingToMainMenu = ( away.getTransform() & Sprite.TRANS_MIRROR_H ) != 0;
		awaySeqBeforeGoingToMainMenu = AWAY_SEQ_STATIC;
		
		away.setVisible( true );
		away.setSequence( AWAY_SEQ_STATIC );
		away.pause( false );
		
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
		if( hasPointerEvents )
		{
			lastPressed = TRIANGLE_NONE;
			for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
				counterTsAreaColor[i] = -1;
			
			if( level == 0 )
			{
				away.setVisible( false );
				away.pause( true );

				pushItBar.setVisible( false );
				scoreBar.setVisible( false );
			}
		}
		//#endif
		
		// Posiciona as barras dos pushIts e dos pontos corretamente
		// OBS: Vai acontecer um reposicionamento desnecess�rio apenas na primeira fase, quando estivermos vindo
		// diretamente do construtor. No entanto, n�o causa perda de desempenho no decorrer do jogo
		pushItBar.setPosition( 0, PUSHIT_BAR_DIST_FROM_TOP );
		scoreBar.setPosition( 0 , PUSHIT_BAR_DIST_FROM_TOP - scoreBar.getHeight() + pushItBar.getDrawable( PushItBar.INDEX_BAR_TOP ).getPosY() );
		
		// Carrega a nova fase
		pushItBar.onNewLevel( level );
	}
	
	private void skipState()
	{
		//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
		if( hasPointerEvents )
		{
			// N�o deixa acelerar o tutorial touchscreen
			if( ( ( currState == STATE_TRANSITION_2 ) && ( level < 0 ) ) || ( currState == STATE_TS_TUTORIAL ) )
				return;
		}
		//#endif
		setState( currState + 1 );
	}
	
	public void keyPressed( int key )
	{
		//#if DEBUG == "true"
		try
		{
		//#endif

		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		if( ( key == ScreenManager.KEY_BACK ) || ( key == ScreenManager.KEY_CLEAR ) || ( key == ScreenManager.KEY_SOFT_RIGHT ) )
		{
			pause();
			return;
		}
		
		if( ( currState >= STATE_MEDALS_SCREEN_WAITING ) && ( currState <= STATE_SHOWING_RESULT ) )
		{
			skipState();
			return;
		}
		
		if( currState == STATE_RUNNING )
		{
			switch( key )
			{
				case ScreenManager.UP:
					pushItBar.keyPressed( ScreenManager.KEY_NUM2 );
					break;
					
				case ScreenManager.DOWN:
					pushItBar.keyPressed( ScreenManager.KEY_NUM8 );
					break;
					
				case ScreenManager.LEFT:
					pushItBar.keyPressed( ScreenManager.KEY_NUM4 );
					break;
					
				case ScreenManager.RIGHT:
					pushItBar.keyPressed( ScreenManager.KEY_NUM6 );
					break;
					
				case ScreenManager.FIRE:
					pushItBar.keyPressed( ScreenManager.KEY_STAR );
					break;
					
				case ScreenManager.KEY_NUM2:
				case ScreenManager.KEY_NUM4:
				case ScreenManager.KEY_NUM6:
				case ScreenManager.KEY_NUM8:
				case ScreenManager.KEY_STAR:
					pushItBar.keyPressed( key );
					break;
			}
		}

		//#if DEBUG == "true"
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		//#endif
	}

	public void keyReleased( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;

		if( currState != STATE_RUNNING )
			return;

		switch( key )
		{
			// N�o convertemos os direcionais, como em keyPressed, pois pushItBar.keyReleased() n�o faz uso
			// de key
			case ScreenManager.UP:
			case ScreenManager.DOWN:
			case ScreenManager.LEFT:
			case ScreenManager.RIGHT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_STAR:
				pushItBar.keyReleased( key );
				break;
		}
	}
	
	/** Chama a tela do menu de pausa */
	private synchronized void pause()
	{
		if( currState != STATE_PAUSED )
		{
			awayFrameBeforeGoingToMainMenu = away.getFrameSequenceIndex();
			awayPausedBeforeGoingToMainMenu = away.isPaused(); 
			awayVisibleBeforeGoingToMainMenu = away.isVisible();
			awaySeqBeforeGoingToMainMenu = away.getSequence();
			awayMirroredBeforeGoingToMainMenu = ( away.getTransform() & Sprite.TRANS_MIRROR_H ) != 0;
			awayFrameTimeBeforeGoingToMainMenu = away.getCurrFrameAccTime();
		
			setState( STATE_PAUSED );
			GameMIDlet.setScreen( GameMIDlet.SCREEN_PAUSE );
			
			//#ifndef NO_SOUND
			MediaPlayer.stop();
			//#endif
		}
	}
	
	public boolean isAwayAlive()
	{
		return away != null;
	}
	
	/** Libera o espa�o alocado na mem�ria para as imagens do Away */
	public void killAway()
	{
		if( away != null )
		{
			removeDrawable( PLAYSCREEN_AWAY_INDEX );
			away = null;
			System.gc();
			Thread.yield();
		}
	}
	
	/** Realoca as imagens do Away */
	public void reviveAway() throws Exception
	{
		if( away == null )
		{
			// Aloca as imagens
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			final String path = ROOT_DIR + ( GameMIDlet.isLowMemory() ? "AwaySml" : "Away" ) + "/away";
			//#else
//# 			final String path = ROOT_DIR + "Away/away";
			//#endif
			away = new Sprite( path + SPRITE_DESC_EXT, path );
			Thread.yield();
			insertDrawable( away, PLAYSCREEN_AWAY_INDEX );

			// Coloca a tela de jogo para receber informa��es sobre as anima��es do sprite
			away.setListener( this, PLAYSCREEN_AWAY_INDEX );

			// Posiciona novamente o personagem
			away.setPosition( ( ScreenManager.SCREEN_WIDTH - away.getWidth() ) >> 1, drawables[ PLAYSCREEN_CROWD_INDEX ].getPosY() + (( ScreenManager.SCREEN_HEIGHT - drawables[ PLAYSCREEN_CROWD_INDEX ].getPosY() - away.getHeight() ) >> 1 ) );

			// Coloca o personagem na sequ�ncia que estava antes
			away.setSequence( awaySeqBeforeGoingToMainMenu );

			// Coloca o personagem no frame que estava antes
			away.setFrame( awayFrameBeforeGoingToMainMenu );

			// Informa o tempo j� decorrido do frame
			away.setCurrFrameAccTime( awayFrameTimeBeforeGoingToMainMenu );

			// Verifica se a anima��o do personagem estava pausada
			away.pause( awayPausedBeforeGoingToMainMenu );

			// Verifica se o personagem estava vis�vel
			away.setVisible( awayVisibleBeforeGoingToMainMenu );

			// Verifica se o personagem estava espelhado
			if( awayMirroredBeforeGoingToMainMenu )
				away.mirror( TRANS_MIRROR_H );
		}
	}
	
	/** Retoma o jogo */
	public synchronized void unpause()
	{
		// N�o chama setState( prevState ) pois repetir�amos anima��es
		if( currState == STATE_PAUSED )
		{
			currState = prevState;
			
			//#ifndef NO_SOUND
			//#ifndef SmallGF500
			if( currState == STATE_RUNNING )
				//#if SAMSUNG_BAD_SOUND == "true"
//#					MediaPlayer.play( currSoundIndex, SAMSUNG_LOOP_INFINITE );
				//#else
				MediaPlayer.play( currSoundIndex, MediaPlayer.LOOP_INFINITE );
				//#endif
			//#endif
			//#endif
			
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			receivedPointerPressed = false;
			//#endif
		}
	}
	
	/** Obt�m a apontua��o atual do jogo */
	public long getScore()
	{
		return scoreBar.getScore();
	}

	public void update( int delta )
	{
		//#if DEBUG == "true"
		try
		{
		//#endif

		switch( currState )
		{
			case STATE_MEDALS_SCREEN_WAITING:
				// Pisca o label do correspondente ao jogador
				lastTransitionDx += delta;
				if( lastTransitionDx >= MEDALS_SCREEN_BLINK_ANIM_TIME )
				{
					lastTransitionDx = 0;
					
					// Nome
					final byte labelNameIndex = ( byte )( PLAYSCREEN_MEDAL_SCREEN_FIRST_GRADE_LB + playerPos );
					drawables[ labelNameIndex ].setVisible( !drawables[ labelNameIndex ].isVisible() );

					// Nota
					final byte labelGradeIndex = ( byte )( PLAYSCREEN_MEDAL_SCREEN_FIRST_NAME_LB + playerPos );
					drawables[ labelGradeIndex ].setVisible( !drawables[ labelGradeIndex ].isVisible() );
				}
				updateLevel( delta );
				break;
				
			case STATE_RUNNING:
				
				//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
				if( hasPointerEvents )
					updateTouchScreenAreaColor( delta );
				//#endif

				updateLevel( delta );
				break;

			case STATE_TRANSITION_2:
				lastTransitionDx += transitionControl.updateInt( delta );
				break;
				
			case STATE_SHOWING_RESULT:
				lastTransitionDx += delta;
				
				if( floorAnimCounter != DEFAULT_LIGHT_COLOR )
				{
					int percentage = ( lastTransitionDx * 100 ) / LOAD_SCREEN_BKG_COLOR_CHANGE_TIME;
					if( percentage > 100 )
						percentage = 100;

					final int r = NanoMath.lerpInt( 0, ( DEFAULT_LIGHT_COLOR & 0x00FF0000 ) >> 16, percentage );
					final int g = NanoMath.lerpInt( 0, ( DEFAULT_LIGHT_COLOR & 0x0000FF00 ) >>  8, percentage );
					final int b = NanoMath.lerpInt( 0, DEFAULT_LIGHT_COLOR & 0x000000FF, percentage );
					floorAnimCounter = 0xFF000000 | ( r << 16 ) | ( g << 8 ) | b;
				}

				if( lastTransitionDx >= RESULT_SCREEN_DUR )
				{
					floorAnimCounter = 0;
					setState( STATE_TRANSITION_2 );
				}

				break;

			case STATE_LEVEL_ENDED:
				{
					updateLevel( delta );

					final int dy = transitionControl.updateInt( delta );
					if( dy == 0 )
						return;

					final int pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();
					if( pushItBarEnd - dy < 0 )
					{
						pushItBar.setPosition( 0 , -pushItBar.getHeight() );
						if( !away.isVisible() )
							setState( STATE_MEDALS_SCREEN );
					}
					else
					{
						pushItBar.move( 0, -dy );
						scoreBar.move( 0, -dy );
					}
				}
				break;

			case STATE_MEDALS_SCREEN:
				updateLevel( delta );

				byte nCompleted = 0;
				final int dx = transitionControl.updateInt( delta );
				if( dx == 0 )
					return;
			
				final byte maxLbNameIndex = ( byte )( PLAYSCREEN_MEDAL_SCREEN_FIRST_NAME_LB + getNCompetitorsInUse() );
				for( byte i = PLAYSCREEN_MEDAL_SCREEN_FIRST_NAME_LB ; i < maxLbNameIndex ; ++i )
				{
					final int currPosX = drawables[ i ].getPosX();
					if( currPosX + dx > medalsScreenLabelsPosX )
					{
						++nCompleted;
						drawables[ i ].setPosition( medalsScreenLabelsPosX, drawables[ i ].getPosY() );
					}
					else
					{
						drawables[ i ].move( dx, 0 );
					}
				}
				
				final byte maxLbGradeIndex = ( byte )( PLAYSCREEN_MEDAL_SCREEN_FIRST_GRADE_LB + getNCompetitorsInUse() );
				for( byte i = PLAYSCREEN_MEDAL_SCREEN_FIRST_GRADE_LB ; i < maxLbGradeIndex ; ++i )
				{
					final int currPosX = drawables[ i ].getPosX();
					final int finalPosX = medalsScreenLabelsPosX + medalsScreenTotalWidth - drawables[ i ].getWidth();
					if( currPosX - dx < finalPosX )
					{
						++nCompleted;
						drawables[ i ].setPosition( finalPosX, drawables[ i ].getPosY() );
					}
					else
					{
						drawables[ i ].move( -dx, 0 );
					}
				}
				if( nCompleted == ( getNCompetitorsInUse() << 1 ))
					setState( STATE_MEDALS_SCREEN_WAITING );

				break;

			case STATE_TRANSITION_1:
				// � poss�vel que o bot�o de pause tenha ficado invis�vel por n�o caber em alguma tela
				//#if SCREEN_SIZE == "SMALL"
//# 					if( cancelBtPause() )
//# 						(( GameMIDlet )GameMIDlet.getInstance()).icons[ GameMIDlet.ICON_PAUSE ].setVisible( true );
				//#endif
			
				lastTransitionDx += transitionControl.updateInt( delta );
				updateLevel( delta );
				break;
				
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			case STATE_TS_TUTORIAL:
				// Atualiza os componentes da tela
				(( Updatable )drawables[ PLAYSCREEN_BACKGROUND_INDEX ]).update( delta );
				(( Updatable )drawables[ PLAYSCREEN_FLOOR_INDEX ]).update( delta );
				(( Updatable )drawables[ PLAYSCREEN_CROWD_INDEX ]).update( delta );
				(( Sprite )drawables[ PLAYSCREEN_TS_STAR_INDEX ]).update( delta );
				
				updateFloor( delta );
				updateBkg( delta );
				updateTouchScreenAreaColor( delta );
				break;

			case STATE_TS_TUTORIAL_EXIT:
				{
					// Atualiza os componentes da tela
					(( Updatable )drawables[ PLAYSCREEN_BACKGROUND_INDEX ]).update( delta );
					(( Updatable )drawables[ PLAYSCREEN_FLOOR_INDEX ]).update( delta );
					(( Updatable )drawables[ PLAYSCREEN_CROWD_INDEX ]).update( delta );
					(( Updatable )drawables[ PLAYSCREEN_AWAY_INDEX ]).update( delta );

					updateFloor( delta );
					updateBkg( delta );

					final int dy = transitionControl.updateInt( delta );
					if( dy == 0 )
						return;

					if( pushItBar.getPosY() + dy >= PUSHIT_BAR_DIST_FROM_TOP )
					{
						scoreBar.setPosition( 0, PUSHIT_BAR_DIST_FROM_TOP - scoreBar.getHeight() + pushItBar.getDrawable( PushItBar.INDEX_BAR_TOP ).getPosY() );
						pushItBar.setPosition( 0 , PUSHIT_BAR_DIST_FROM_TOP );

						if( away.getSequence() == AWAY_SEQ_STATIC )
							setState( STATE_RUNNING );
					}
					else
					{
						pushItBar.move( 0, dy );
						scoreBar.move( 0, dy );
					}
				}
				break;
			//#endif
		}

		//#if DEBUG == "true"
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		//#endif
	}
	
	//#if SCREEN_SIZE == "SMALL"
//# 	public static boolean cancelBtPause()
//# 	{
//# 		return ScreenManager.SCREEN_HEIGHT < 148;
//# 	}
	//#endif
	
	private byte getNCompetitorsInUse()
	{
		//#if SCREEN_SIZE != "SMALL"
			return N_COMPETITORS;
		//#else
//# 			return ScreenManager.SCREEN_HEIGHT < 126 ? N_COMPETITORS - 1 : N_COMPETITORS;
		//#endif
	}
	
	/** Verifica se deve mudar a sequ�ncia de anima��o do ch�o */
	private void updateFloor( int delta )
	{
		floorAnimCounter -= delta;
		if( floorAnimCounter <= 0 )
		{
			final Sprite floor = ( Sprite )(( Pattern )drawables[ PLAYSCREEN_FLOOR_INDEX ]).getFill();

			final int nSequences = floor.getNSequences();
			int nextSequence = NanoMath.randInt( nSequences );
			if( nextSequence == floor.getSequence() )
				nextSequence = ( nextSequence + 1 ) % nSequences;
			floor.setSequence( nextSequence );

			floorAnimCounter = MIN_FLOOR_DUR + NanoMath.randInt( MAX_FLOOR_DUR - MIN_FLOOR_DUR + 1 );
		}
	}
	
	/** Verifica se deve mudar a sequ�ncia de anima��o do fundo */
	private void updateBkg( int delta )
	{
		bkgAnimCounter -= delta;
		if( bkgAnimCounter <= 0 )
		{
			int nextScreen = NanoMath.randInt( N_BKGS );
			if( bkgs[ nextScreen ] == drawables[ PLAYSCREEN_BACKGROUND_INDEX ] )
				nextScreen = ( nextScreen + 1 ) % N_BKGS;

			(( BasicAnimatedPattern )bkgs[ nextScreen ]).setAnimation( NanoMath.randInt( BasicAnimatedPattern.TOTAL_ANIMATIONS - 1 ) + 1 );
			(( BasicAnimatedPattern )bkgs[ nextScreen ]).setSpeed( MIN_BKG_SPEED + NanoMath.randInt( MAX_BKG_SPEED - MIN_BKG_SPEED + 1 ), MIN_BKG_SPEED + NanoMath.randInt( MAX_BKG_SPEED - MIN_BKG_SPEED + 1 ) );
			setDrawable( bkgs[ nextScreen ], PLAYSCREEN_BACKGROUND_INDEX );

			bkgAnimCounter = MIN_BKG_DUR + NanoMath.randInt( MAX_BKG_DUR - MIN_BKG_DUR + 1 );
		}
	}
	
	private void updateLevel( int delta )
	{
		// Atualiza os componentes do grupo
		super.update( delta );

		//#if ( SCREEN_SIZE == "SMALL" )
//# 		if( !GameMIDlet.isLowMemory() )
//# 		{
		//#endif
			
			// Verifica se deve mudar a sequ�ncia de anima��o do ch�o
			updateFloor( delta );

			// Verifica se deve mudar a sequ�ncia de anima��o do fundo
			updateBkg( delta );
		
		//#if ( SCREEN_SIZE == "SMALL" )
//# 		}
		//#endif

		// Verifica se deve apagar a palavra de feedback
		if( feedBackCounter > 0 )
		{
			feedBackCounter -= delta;
			if( feedBackCounter <= 0 )
				noFeedback();
		}
	}
	
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	private void updateTouchScreenAreaColor( int delta )
	{
		if(( lastPressed >= TRIANGLE_TOP ) && ( lastPressed <= TRIANGLE_BOTTOM ))
		{
			counterTsAreaColor[ lastPressed ] -= delta;
			if( counterTsAreaColor[ lastPressed ] <= 0 )
			{
				counterTsAreaColor[ lastPressed ] = TOUCH_SCREEN_AREA_COLOR_TIME;
				lastTransitionDx = ( lastTransitionDx + 1 ) % TOUCH_SCREEN_AREA_N_COLORS;
			}
		}
	}
	//#endif
	
	private synchronized void setState( int state )
	{
		final byte stateBeforeChange = currState;
		currState = ( byte )state;

		switch( state )
		{
			case STATE_SHOWING_RESULT:
				// Impede que a m�sica da tela anterior seja prolongada para esta tela
				MediaPlayer.stop();

				// Utiliza floorAnimCounter como vari�vel auxiliar na transi��o de cores do background
				floorAnimCounter = 0;
				
				final RichLabel aux = ( RichLabel )drawables[ PLAYSCREEN_RESULT_LABEL_INDEX ];
				
				if( playerPos != 0 )
				{
					aux.setText( GameMIDlet.getText( TEXT_GAMEOVER ), true, false );
					
					// Retira o bot�o de pause da tela
					ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
				}
				else
				{
					aux.setText( GameMIDlet.getText( TEXT_GET_READY ) + String.valueOf( level + 2 ), true, false );
				}
				aux.setSize( aux.getWidth(), aux.getTextTotalHeight() );
				aux.setPosition( ( ScreenManager.SCREEN_WIDTH - aux.getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - aux.getHeight() ) >> 1 );
				aux.setVisible( true );
				// Sem break mesmo

			case STATE_RUNNING:
			case STATE_LEVEL_ENDED:
			case STATE_MEDALS_SCREEN_WAITING:
				lastTransitionDx = 0;
				break;

			case STATE_PAUSED:
				prevState = stateBeforeChange;
				break;
				
			case STATE_MEDALS_SCREEN:
				onShowMedalsScreen();
				break;

			case STATE_TRANSITION_1:
				lastTransitionDx = 0;
				transitionControl.setSpeed( ( ScreenManager.SCREEN_WIDTH / TRANSITION_N_SLICES ) << 1, true );
				break;
		
			case STATE_TRANSITION_2:
				try
				{
					if( playerPos == 0 )
					{
						onNewLevel();
						
						lastTransitionDx = 0;
						transitionControl.setSpeed( ScreenManager.SCREEN_HEIGHT, true );
						
						//#ifndef NO_SOUND
						//#ifndef SmallGF500
							++currSoundIndex;
							if( currSoundIndex >= SOUND_INDEX_LEVEL_MUSIC_0 + SOUND_N_LEVEL_SOUNDS )
								currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_0;
						
							//#if SAMSUNG_BAD_SOUND == "true"
//#									MediaPlayer.play( currSoundIndex, SAMSUNG_LOOP_INFINITE );
							//#else
							MediaPlayer.play( currSoundIndex, MediaPlayer.LOOP_INFINITE );
							//#endif
						//#endif
						//#endif
					}
					else
					{
						GameMIDlet.gameOver( getScore() );
					}
				}
				catch( Exception ex )
				{
					//#if DEBUG == "true"
						ex.printStackTrace();
					//#endif
						
					// N�o � para acontecer, mas...
					GameMIDlet.exit();
					return;
				}
				
				break;
				
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			case STATE_TS_TUTORIAL:
				for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
					counterTsAreaColor[i] = TOUCH_SCREEN_AREA_COLOR_TIME;

				lastTransitionDx = 0;
			
				drawables[ PLAYSCREEN_MEDAL_SCREEN_BKG ].setVisible( true );
				
				final RichLabel labelHint = ( RichLabel )drawables[ PLAYSCREEN_TS_TUTORIAL_LABEL_HINT ];
				labelHint.setText( AppMIDlet.getText( TEXT_TS_TUTORIAL_PRESS ) + AppMIDlet.getText( TEXT_TS_TUTORIAL_AREA ) + AppMIDlet.getText( TEXT_TS_TUTORIAL_TO_HIT ) + "0>", true, false );
				labelHint.setSize( labelHint.getWidth(), labelHint.getTextTotalHeight() );
				
				labelHint.setPosition( ( ScreenManager.SCREEN_WIDTH - labelHint.getWidth() ) >> 1, 2 /* S� para n�o ficar grudado na parte superior da tela */ );
				labelHint.setVisible( true );
				
				lastPressed = TRIANGLE_TOP;
				break;
				
			case STATE_TS_TUTORIAL_EXIT:
				lastTransitionDx = 0;
				transitionControl.setSpeed( ScreenManager.SCREEN_HEIGHT, true );
				
				final int movement = -( pushItBar.getHeight() + PUSHIT_BAR_DIST_FROM_TOP );
				scoreBar.move( 0, movement );
				scoreBar.setVisible( true );
				
				pushItBar.move( 0, movement );
				pushItBar.setVisible( true );
				
				away.setVisible( true );
				away.setSequence( AWAY_SEQ_APPEAR );
				away.pause( false );
				break;
			//#endif
		} // fim switch ( currState )
	}

	protected void paint( Graphics g )
	{
		switch( currState )
		{
			case STATE_TRANSITION_1:
				super.paint( g );
				paintTransition( g );
				break;
				
			case STATE_SHOWING_RESULT:

				if( playerPos == 0 )
					g.setColor( 0xFF000000 );
				else
					g.setColor( floorAnimCounter );

				g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				drawables[ PLAYSCREEN_RESULT_LABEL_INDEX ].draw( g );
				break;
			
			case STATE_TRANSITION_2:
				if( lastTransitionDx > 0 )
					super.paint( g );
				
				paintTransition( g );
				break;
			
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			case STATE_TS_TUTORIAL:
				for( int i = 0 ; i < PLAYSCREEN_TS_STAR_INDEX ; ++i )
					drawables[ i ] .draw( g );
				
				drawTouchScreenArea( g, lastPressed, true );
				
				for( int i = PLAYSCREEN_TS_STAR_INDEX ; i < activeDrawables ; ++i )
					drawables[ i ] .draw( g );
				
				break;
			//#endif
				
			default:
				drawables[ PLAYSCREEN_BACKGROUND_INDEX ].draw( g );
				drawables[ PLAYSCREEN_FLOOR_INDEX ].draw( g );
				
				//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
				paintTouchScreenAreas( g );
				//#endif
				
				for ( int i = PLAYSCREEN_CROWD_INDEX; i < activeDrawables; ++i )
					drawables[ i ].draw( g );
		}
	}
	
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	
	private void paintTouchScreenAreas( Graphics g )
	{
		if( hasPointerEvents )
			drawTouchScreenArea( g, lastPressed, false );
	}
	
	//#endif
	
	private void paintTransition( Graphics g )
	{
		g.setColor( 0xFF000000 );
		
		if( currState == STATE_TRANSITION_1 )
		{
			final short sliceWidth = ( short )( ScreenManager.SCREEN_WIDTH / TRANSITION_N_SLICES );
			int x = 0;
			
			// +1 => Evita imprecis�es na divis�o da largura da tela
			for( byte i = 0 ; i < TRANSITION_N_SLICES + 1 ; ++i )
			{
				g.fillRect( x, 0, lastTransitionDx, ScreenManager.SCREEN_HEIGHT );
				x += sliceWidth;
			}
			
			// Garante que fechou a cortina
			final byte JUST_TO_GET_SURE = 5;
			if( lastTransitionDx > sliceWidth + JUST_TO_GET_SURE )
				setState( STATE_SHOWING_RESULT );
		}
		else // if( currState == STATE_TRANSITION_2 )
		{
			final int height = ScreenManager.SCREEN_HEIGHT - lastTransitionDx;
			if( height > 0 )
			{
				g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, height );
			}
			else
			{
				//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
				if( hasPointerEvents && ( level == 0 ))
					setState( STATE_TS_TUTORIAL );
				else
				//#endif
					setState( STATE_RUNNING );
			}
		}
	}

	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )

	public void onPointerDragged( int x, int y )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		if( ( currState != STATE_RUNNING ) || !receivedPointerPressed )
			return;
		
		final byte currPressed = getPressedPointIndex( x, y );

		if( currPressed != lastPressed )
		{
			lastPressed = TRIANGLE_NONE;
			for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
				counterTsAreaColor[i] = -1;

			touchPointToButton( currPressed );
		}
	}
	
	// Verifica em qual tri�ngulo da tela o ponto recebido como par�metro est�
	private byte peterTriangleMethod( int x, int y )
	{
		final int pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();
		final int halfAreaHeight = pushItBarEnd + (( ScreenManager.SCREEN_HEIGHT - pushItBarEnd ) >> 1 );

		// whatTheHell:
		// x / y = H / W
		// Com regra de 3:
		// y <= ( x * H ) / W
		final boolean whatTheHell = NanoMath.min( y, ScreenManager.SCREEN_HEIGHT - y ) <= ( NanoMath.min( x, ScreenManager.SCREEN_WIDTH - x ) * ScreenManager.SCREEN_HEIGHT ) / ScreenManager.SCREEN_WIDTH;
		
		if( x < ScreenManager.SCREEN_HALF_WIDTH )
		{
			if( y > halfAreaHeight )
				return whatTheHell ? TRIANGLE_BOTTOM : TRIANGLE_LEFT;
			else
				return whatTheHell ? TRIANGLE_TOP : TRIANGLE_LEFT;
		}
		else
		{
			if( y > halfAreaHeight )
				return whatTheHell ? TRIANGLE_BOTTOM : TRIANGLE_RIGHT;
			else
				return whatTheHell ? TRIANGLE_TOP : TRIANGLE_RIGHT;
		}
	}

	private byte getPressedPointIndex( int x, int y )
	{
		if( drawables[ PLAYSCREEN_TS_STAR_INDEX ].contains( x, y ) )
			return TRIANGLE_STAR;
		return peterTriangleMethod( x, y );
	}
		
	public void onPointerPressed( int x, int y )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		if( ( currState >= STATE_MEDALS_SCREEN_WAITING ) && ( currState <= STATE_SHOWING_RESULT ) )
		{
			skipState();
			return;
		}
		
		if( currState == STATE_RUNNING )
		{
			receivedPointerPressed = true;
			touchPointToButton( getPressedPointIndex( x, y ) );
		}
		else if( currState == STATE_TS_TUTORIAL )
		{
			if( getPressedPointIndex( x, y ) == lastPressed )
			{
				++lastPressed;
				if( lastPressed > TRIANGLE_STAR )
				{
					lastPressed = TRIANGLE_NONE;
					for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
						counterTsAreaColor[i] = -1;
					
					removeDrawable( PLAYSCREEN_TS_TUTORIAL_LABEL_HINT );

					(( Sprite )drawables[ PLAYSCREEN_TS_STAR_INDEX ]).setSequence( 0 );
					drawables[ PLAYSCREEN_MEDAL_SCREEN_BKG ].setVisible( false );
					setState( STATE_TS_TUTORIAL_EXIT );
				}
				else
				{
					final RichLabel labelHint = ( RichLabel )drawables[ PLAYSCREEN_TS_TUTORIAL_LABEL_HINT ];

					if( lastPressed == TRIANGLE_STAR )
					{
						(( Sprite )drawables[ PLAYSCREEN_TS_STAR_INDEX ]).setSequence( 1 );
						labelHint.setText( AppMIDlet.getText( TEXT_TS_TUTORIAL_PRESS ) + "<IMG_5>" + AppMIDlet.getText( TEXT_TS_TUTORIAL_TO_HIT ) + lastPressed + ">", true, false );
						labelHint.setPosition( labelHint.getPosX(), ( ScreenManager.SCREEN_HEIGHT - labelHint.getHeight() ) >> 1 );
					}
					else
					{
						lastTransitionDx = 0;
						counterTsAreaColor[ lastPressed ] = TOUCH_SCREEN_AREA_COLOR_TIME;
						
						labelHint.setText( AppMIDlet.getText( TEXT_TS_TUTORIAL_PRESS ) + AppMIDlet.getText( TEXT_TS_TUTORIAL_AREA ) + AppMIDlet.getText( TEXT_TS_TUTORIAL_TO_HIT ) + lastPressed + ">", true, false );
					}
				}
			}
		}
	}
	
	private void touchPointToButton( byte touchScreenAreaIndex )
	{
		lastPressed = touchScreenAreaIndex;
		
		for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
			counterTsAreaColor[i] = -1;
		
		switch( lastPressed )
		{
			case TRIANGLE_TOP:
			case TRIANGLE_LEFT:
			case TRIANGLE_RIGHT:
			case TRIANGLE_BOTTOM:
				lastTransitionDx = 0;
				counterTsAreaColor[ lastPressed ] = TOUCH_SCREEN_AREA_N_COLORS;
				pushItBar.keyPressed( ScreenManager.KEY_NUM2 + (( lastPressed - TRIANGLE_TOP ) << 1 ) );
				break;

			case TRIANGLE_STAR:
				pushItBar.keyPressed( ScreenManager.KEY_STAR );
				break;
		}
	}
	
	private void drawTouchScreenArea( Graphics g, byte touchScreenAreaIndex, boolean fill )
	{
		final byte POINT_SIZE = 3;
		final int pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();
		final int halfAreaHeight = pushItBarEnd + (( ScreenManager.SCREEN_HEIGHT - pushItBarEnd ) >> 1 );
		
		switch( touchScreenAreaIndex )
		{
			case TRIANGLE_TOP:
				if( counterTsAreaColor[ TRIANGLE_TOP ] >= 0 )
				{
					g.setColor( tsAreaColors[ TRIANGLE_TOP ][ lastTransitionDx ] );
					
					if( !fill )
						drawTriangle( g, 0, pushItBarEnd, ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE );
					else
						g.fillTriangle(  0, pushItBarEnd, ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight );
				}
				break;

			case TRIANGLE_LEFT:
				if( counterTsAreaColor[ TRIANGLE_LEFT ] >= 0 )
				{
					g.setColor( tsAreaColors[ TRIANGLE_LEFT ][ lastTransitionDx ] );
						
					if( !fill )
						drawTriangle( g, 0, pushItBarEnd, 0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE );
					else
						g.fillTriangle(  0, pushItBarEnd, 0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight );
				}
				break;

			case TRIANGLE_RIGHT:
				if( counterTsAreaColor[ TRIANGLE_RIGHT ] >= 0 )
				{
					g.setColor( tsAreaColors[ TRIANGLE_RIGHT ][ lastTransitionDx ] );
					
					if( !fill )
						drawTriangle( g, ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE );
					else
						g.fillTriangle(  ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight );
				}
				break;

			case TRIANGLE_BOTTOM:
				if( counterTsAreaColor[ TRIANGLE_BOTTOM ] >= 0 )
				{
					g.setColor( tsAreaColors[ TRIANGLE_BOTTOM ][ lastTransitionDx ] );
					
					if( !fill )
						drawTriangle( g, 0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE );
					else
						g.fillTriangle(  0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight );
				}
				break;
		}
	}
	
	private void drawTriangle( Graphics g, int x0, int y0, int x1, int y1, int x2, int y2, int pointSize )
	{
		drawLine( g, x0, y0, x1, y1, pointSize );
		drawLine( g, x1, y1, x2, y2, pointSize );
		drawLine( g, x2, y2, x0, y0, pointSize );
	}
	
	private void drawLine( Graphics g, int x0, int y0, int x1, int y1, int pointSize )
	{
		int change;
		for( int i = 0 ; i < pointSize ; ++i )
		{
			change = ( ( i + 1 ) >> 1 ) * ( - ( i & 1 ) );
			g.drawLine( x0 + change, y0, x1 + change, y1 );
		}
	}

	public void onPointerReleased( int x, int y )
	{
		if( currState == STATE_RUNNING )
		{
			// �, querido leitor, por mais incr�vel que pare�a, acontece...
			if( this != ScreenManager.getInstance().getCurrentScreen() )
				return;
			
			lastPressed = TRIANGLE_NONE;
			for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
				counterTsAreaColor[i] = -1;
			
			pushItBar.keyReleased( 0 /* Nesse caso, tanto faz o valor passado */ );
		}
	}
	
	//#endif
	
	public synchronized void onHit( byte hit, byte hitStatus )
	{
		if( currState != STATE_RUNNING )
			return;

		switch( hit )
		{
			case ON_HIT_FALSE:
				// Cancela o multiplicador b�nus
				bonusMultiplierCounter = 0;
				setBonusMultiplier( 1 );

				// Cancela a contagem de hits
				hitCount = 0;
				labelHits.setVisible( false );

				// Exibe a frase indicando o que aconteceu
				showFeedback( PLAYSCREEN_FEEDBACK_MISS_INDEX );
				
				// Vibra
				MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
				break;
				
			case ON_HIT_FALSE_QM:
				// Se errou o ponto de interroga��o, divide o multiplicador b�nus atual por 2
				setBonusMultiplier( bonusMultiplier >> 1 );
				
				// Vibra
				MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
				break;
				
			case ON_HIT_TRUE:
				// Incrementa o contador de acertos totais
				++levelHits;

				// Armazena o tipo de acerto
				lastHitStatus = hitStatus;
				
				// Atualiza o label dos hits
				if( hitCount + 1 < MAX_HIT_COUNT )
				{
					++hitCount;
					
					labelHits.setText( hitCount + GameMIDlet.getText( TEXT_HITS ) );
					labelHits.setPosition( ScreenManager.SCREEN_WIDTH - labelHits.getWidth() - LABEL_HITS_DIST_FROM_RIGHT, labelHits.getPosY() );
					labelHits.setVisible( true );
				}
				else if( hitCount + 1 == MAX_HIT_COUNT )
				{
					++hitCount;
					labelHits.setText( GameMIDlet.getText( TEXT_YOURE_DA_MAN ) );
					labelHits.setPosition( ScreenManager.SCREEN_WIDTH - labelHits.getWidth() - LABEL_HITS_DIST_FROM_RIGHT, labelHits.getPosY() );
				}

				// Atualiza a pontua��o
				scoreBar.changeScore((((( 100L - ( ( long )hitStatus * ( long )PushItBar.UNDER_PERFECT_HIT_PERCENTAGE ) ) * ( long )PushItBar.PERFECT_HIT_N_POINTS ) / 100L ) * ( long )bonusMultiplier ) + ( long )hitCount );
			
				// Exibe a frase indicando o que aconteceu
				showFeedback( ( byte )( PLAYSCREEN_FEEDBACK_PERFECT_INDEX + hitStatus ) );

				// Verifica se deve aumentar o multiplicador b�nus
				incBonusCounter( hitStatus );

				break;
				
			case ON_HIT_TRUE_QM:
				// Se acertou o ponto de interroga��o, dobra o multiplicador b�nus
				bonusMultiplierCounter = 0;
				setBonusMultiplier( bonusMultiplier << 1 );
				break;
				
			case ON_HIT_TRUE_TRAIL:
				// Atualiza a pontua��o
				scoreBar.changeScore((((( 100L - ( ( long )lastHitStatus * ( long )PushItBar.UNDER_PERFECT_HIT_PERCENTAGE )) * ( long )PushItBar.PERFECT_HIT_N_POINTS ) / 100L ) * ( long )bonusMultiplier ) + ( long )hitCount );
				
				// Verifica se deve aumentar o multiplicador b�nus
				incBonusCounter( lastHitStatus );
				
				// Fica exibindo a frase indicando o que aconteceu enquanto o rastro durar
				showFeedback( ( byte )( PLAYSCREEN_FEEDBACK_PERFECT_INDEX + lastHitStatus ) );
				
				break;
		}
	}
	
	/** Indica que o fase atual terminou */
	public synchronized void onLevelEnded()
	{
		if( currState == STATE_RUNNING )
		{
			// Retira alguns elementos da tela
			noFeedback();
			labelHits.setVisible( false );
			labelBonusMultiplier.setVisible( false );
			
			// Ver onSequenceEnded()
			awayNextSeq = AWAY_SEQ_VANISH;

			// Determina a anima��o para a retirada das barras de pushIts e pontos da tela
			lastTransitionDx = 0;
			transitionControl.setSpeed( ScreenManager.SCREEN_HEIGHT, true );
			
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			if( hasPointerEvents )
			{
				// P�ra de desenhar as �reas do touchscreen
				for( int i = 0 ; i < counterTsAreaColor.length ; ++i )
					counterTsAreaColor[i] = -1;
			}
			//#endif

			setState( STATE_LEVEL_ENDED );
		}
	}
	
	private void onShowMedalsScreen()
	{
		// Deixa a trama e o t�tulo vis�veis
		drawables[ PLAYSCREEN_MEDAL_SCREEN_BKG ].setVisible( true );
		drawables[ PLAYSCREEN_MEDAL_SCREEN_TITLE ].setVisible( true );

		// Sorteia as notas dos 5 adivers�rios
		final int bestCompMaxPercentage = NanoMath.lerpInt( BEST_COMPETITOR_MIN_PERCENTAGE, BEST_COMPETITOR_MAX_PERCENTAGE, (( level > MAX_DIFFICULT_LEVEL ? MAX_DIFFICULT_LEVEL : level ) * 100 ) / MAX_DIFFICULT_LEVEL );
		final byte[] grades = new byte[ getNCompetitorsInUse() ];
		int intervalMin = bestCompMaxPercentage - COMPETITORS_PERCENTAGE_STEP;

		// "getNCompetitorsInUse() - 1" pois o �ltimo � o jogador
		for( byte i = 0 ; i < getNCompetitorsInUse() - 1 ; ++i ) 
		{
			grades[ i ] = ( byte )( intervalMin + NanoMath.randInt( COMPETITORS_PERCENTAGE_STEP + 1 ) );
			intervalMin -= COMPETITORS_PERCENTAGE_STEP;
		}
		
		// Insere a nota do jogador no array de notas
		final byte playerGrade = ( byte )( ( levelHits * 100 ) / pushItBar.getNHitablePushIts() );
		for( playerPos = 0 ; playerPos < getNCompetitorsInUse() ; ++playerPos ) 
		{
			if( playerGrade >= grades[ playerPos ] )
				break;
		}

		for( byte i = ( byte )( getNCompetitorsInUse() - 2 ) ; i >= playerPos ; --i )
			grades[ i + 1 ] = grades[ i ];
		
		grades[ playerPos ] = playerGrade;
		
		// Prefixo para comportar a medalha
		//#if SCREEN_SIZE != "SMALL"
			final String prefix = GameMIDlet.isBig() ? "   " : "     ";
		//#else
//# 			final String prefix = "     ";
		//#endif

		// Distribui as notas pelos oponentes
		final boolean usedCompetitors[] = new boolean[ getNCompetitorsInUse() - 1 ];
		for( byte i = 0 ; i < getNCompetitorsInUse() ; ++i )
		{
			int competitorNameIndex = 0;

			if( i != playerPos )
			{
				// Sorteia um advers�rio para possuir a nota
				competitorNameIndex = NanoMath.randInt( getNCompetitorsInUse() - 1 );

				if( usedCompetitors[ competitorNameIndex ] == true )
				{
					// Procura o primeiro competidor ainda n�o utilizado
					for( byte j = 0 ; j < usedCompetitors.length ; ++j )
					{
						if( usedCompetitors[ j ] == false )
						{
							competitorNameIndex = j;
							break;
						}
					}
				}
				usedCompetitors[ competitorNameIndex ] = true;

				// Pois afinal o intervalo que queremos fica de 1 a 5, j� que o 0 � o jogador
				++competitorNameIndex;
			}
				
			// Nome
			final RichLabel labelName = ( RichLabel )drawables[ PLAYSCREEN_MEDAL_SCREEN_FIRST_NAME_LB + i ];
			final int neededHeight = labelName.getHeight();
			labelName.setText( "<ALN_L><ALN_V>" + ( i < N_MEDALS ? "<IMG_" + i + ">" : prefix ) + " " + GameMIDlet.getText( TEXT_COMPETITOR_0 + competitorNameIndex ), true, true );
			labelName.setTextOffset( ( neededHeight - labelName.getHeight() ) >> 1 );
			labelName.setSize( labelName.getWidth(), neededHeight );
			labelName.setPosition( -medalsScreenTotalWidth, labelName.getPosY() );
			labelName.setVisible( true );
			
			// Nota
			final RichLabel labelGrade = ( RichLabel )drawables[ PLAYSCREEN_MEDAL_SCREEN_FIRST_GRADE_LB + i ];
			labelGrade.setText( "<ALN_R><ALN_V>" + String.valueOf( grades[ i ] ), true, true );
			labelGrade.setTextOffset( ( neededHeight - labelGrade.getHeight() ) >> 1 );
			labelGrade.setSize( labelGrade.getWidth(), neededHeight );
			labelGrade.setPosition( ScreenManager.SCREEN_WIDTH, labelGrade.getPosY() );
			labelGrade.setVisible( true );

		} // fim for( byte i = 0 ; i < getNCompetitorsInUse() ; ++i )
		
		// Se fez menos pontos que o melhor, perdeu
		//#ifndef NO_SOUND
		if( playerPos != 0 )
			MediaPlayer.play( SOUND_INDEX_LIFE_LOST );
		else
			MediaPlayer.play( SOUND_INDEX_LEVEL_COMPLETED );
		//#endif
		
		// Determina a anima��o para a entrada dos labels com as notas
		lastTransitionDx = 0;
		transitionControl.setSpeed( ScreenManager.SCREEN_WIDTH, true );
		
		// O bot�o de pausa n�o cabe em algumas telas pequenas
		//#if SCREEN_SIZE == "SMALL"
//# 		if( cancelBtPause() )
//# 			(( GameMIDlet )GameMIDlet.getInstance()).icons[ GameMIDlet.ICON_PAUSE ].setVisible( false );
		//#endif
	}
	
	private void incBonusCounter( byte hitStatus )
	{
		if( bonusMultiplier < MAX_BONUS_MULIPLIER )
		{
			final byte inc = ( byte )( PERFECT_BONUS_COUNTER_INC - hitStatus );
			if( inc > 0 )
			{
				bonusMultiplierCounter += inc;
				final int nextBonusMultiplier = getPATerm( BONUS_PA_A1, BONUS_PA_R, bonusMultiplier );
				if( bonusMultiplierCounter > nextBonusMultiplier )
				{
					bonusMultiplierCounter -= nextBonusMultiplier;
					setBonusMultiplier( bonusMultiplier + 1 );
				}
			}
		}
	}
	
	private int getPATerm( int a1, int r, int n )
	{
		return a1 + ( ( n - 1 ) * r );
	}
	
	/** Determina um novo multiplicador b�nus */
	private void setBonusMultiplier( int newBonusMultiplier )
	{
		if( newBonusMultiplier <= 1 )
		{
			bonusMultiplier = 1;
			labelBonusMultiplier.setVisible( false );
		}
		else
		{
			if( newBonusMultiplier > MAX_BONUS_MULIPLIER )
				newBonusMultiplier = MAX_BONUS_MULIPLIER;
			
			if( bonusMultiplier != newBonusMultiplier )
			{
				bonusMultiplier = ( short )newBonusMultiplier;
				labelBonusMultiplier.setVisible( true );
				labelBonusMultiplier.setText( GameMIDlet.getText( TEXT_BONUS ) + bonusMultiplier );
			}
		}
	}
	
	/** Exibe a frase de feedback recebida como par�metro */
	private void showFeedback( byte index )
	{
		noFeedback();
		feedBackCounter = FEEDBACK_DUR;
		drawables[ index ].setVisible( true );
	}
	
	/** Apaga o feedback vis�vel no momento */
	private void noFeedback()
	{
		for( byte i = PLAYSCREEN_FEEDBACK_PERFECT_INDEX ; i <= PLAYSCREEN_FEEDBACK_MISS_INDEX ; ++i )
			drawables[i].setVisible( false );
	}
	
	public void onSequenceEnded( int id, int sequence )
	{
		if( id == PLAYSCREEN_AWAY_INDEX )
		{
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			if( currState == STATE_TS_TUTORIAL_EXIT )
			{
				if( sequence == AWAY_SEQ_APPEAR )
					away.setSequence( AWAY_SEQ_STATIC );
			}
			else
			{
			//#endif
				if( sequence == AWAY_SEQ_VANISH )
				{
					away.setVisible( false );
					away.pause( true );
				}
				else
				{
					int newSeq;
					if( awayNextSeq < 0 )
					{
						// Possui 50% de chance de fazer o movimento para um lado diferente do atual
						if( NanoMath.randInt( 100 ) >= 50 )
							away.mirror( Sprite.TRANS_MIRROR_H );

						// Sorteia a nova sequ�ncia
						newSeq = AWAY_SEQ_OPENING_LEGS + NanoMath.randInt( AWAY_SEQ_N_LEVEL_SEQS );
						if( newSeq == away.getSequence() )
						{
							++newSeq;
							if( newSeq > AWAY_SEQ_N_LEVEL_SEQS )
								newSeq = AWAY_SEQ_OPENING_LEGS;
						}
					}
					else
					{
						newSeq = awayNextSeq;
						awayNextSeq = -1;
					}
					away.setSequence( newSeq );
				}
			//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
			}
			//#endif
		}
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
}