/**
 * GameRecordsScreen.java
 * �2008 Nano Games.
 *
 * Created on 28/04/2008 12:34:46.
 */

package screens;

//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.GameMIDlet;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * @author Daniel L. Alves
 */

public final class GameRecordsScreen extends UpdatableGroup implements Serializable, Constants, KeyListener
//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
, PointerListener
//#endif
{
	private static final byte TOTAL_SCORES = 5;
	
	private static final long[] scores = new long[TOTAL_SCORES];
	
	private final short BLINK_RATE = 388;
	
	private byte lastHighScoreIndex = -1;
	
	/** Pontua��o base para os recordes dummy */
	private static final byte DUMMY_BASE = 0;
	
	private static GameRecordsScreen instance;
	
	private int accTime;
	
	/** �ndice do primeiro label no menu */
	protected final byte FIRST_LABEL_INDEX = 1;
	
	/** Construtor */
	private GameRecordsScreen( ImageFont font ) throws Exception
	{
		super( 1 + TOTAL_SCORES );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final int fontHeight = font.getHeight();
		final int totalHeight = ( fontHeight + MENU_ITEMS_SPACING ) * ( TOTAL_SCORES + 1 );
		
		//#if SCREEN_SIZE == "SMALL"
//# 			final DrawableImage btBack = new DrawableImage( ROOT_DIR + "btBack" + IMG_EXT );
//# 			int yLabel = ScreenManager.SCREEN_HEIGHT >= 128 ? ( ScreenManager.SCREEN_HEIGHT - totalHeight ) >> 1 : ( ScreenManager.SCREEN_HEIGHT - btBack.getHeight() - totalHeight ) >> 1;
		//#else
		int yLabel = ( ScreenManager.SCREEN_HEIGHT - totalHeight ) >> 1;
		//#endif
		
		final Label title = new Label( font, AppMIDlet.getText( TEXT_RECORDS ) );
		insertDrawable( title );
		title.setPosition( ( ScreenManager.SCREEN_WIDTH - title.getWidth() ) >> 1, yLabel );

		yLabel += fontHeight + ( MENU_ITEMS_SPACING << 1 );
		
		for( int i = 0; i < scores.length; ++i )
		{
			final Label aux = new Label( font, "0. " + String.valueOf( MAX_SCORE ) );
			insertDrawable( aux );
			
			aux.setPosition( ( ScreenManager.SCREEN_WIDTH - aux.getWidth() ) >> 1, yLabel );

			yLabel += fontHeight + MENU_ITEMS_SPACING;
		}
	}

	/**
	 * Cria uma nova inst�ncia da tela de recordes, ou apenas uma refer�ncia para ela, caso j� tenha sido criada anteriormente.
	 *
	 * @param font fonte utilizada para criar os labels.
	 * @throws java.lang.Exception caso haja problemas ao alocar recursos.
	 * @return uma refer�ncia para a inst�ncia da tela de recordes.
	 */
	public static final GameRecordsScreen createInstance( ImageFont font ) throws Exception
	{
		if( instance == null )
		{
			instance = new GameRecordsScreen( font );
			loadRecords();
		}
		
		//#if SCREEN_SIZE != "SMALL"

		// Tenta conseguir mais espa�o
		if( !GameMIDlet.isBig() )
		{
			final ImageFont usedFont = (( Label )instance.drawables[0]).getFont();
			usedFont.setCharOffset( GameMIDlet.DEFAULT_FONT_OFFSET - 1 );
		}
		
		//#endif
		
		// Atualiza o texto dos labels
		instance.updateLabels();
		
		return instance;
	}

	public static final boolean setScore( long score )
	{
		for( int i = 0; i < scores.length; ++i )
		{
			if( score > scores[i] )
			{
				// �ltima pontua��o � maior que uma pontua��o anteriormente gravada
				for( int j = scores.length - 1; j > i; --j )
					scores[j] = scores[j - 1];

				instance.lastHighScoreIndex = ( byte ) i;
				instance.accTime = 0;

				scores[i] = score;
				try
				{
					GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_SCORES, instance );
				}
				catch( Exception e )
				{
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif
				}
				return true;
			}
		}
		return false;
	}

	public final void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		stopBlink();

		GameMIDlet.setScreen( GameMIDlet.SCREEN_MAIN_MENU );
	}

	public final void write( DataOutputStream output ) throws Exception
	{
		for( int i = 0; i < TOTAL_SCORES; ++i )
			output.writeLong( scores[i] );
	}

	public final void read( DataInputStream input ) throws Exception
	{
		for( int i = 0; i < TOTAL_SCORES; ++i )
			scores[i] = input.readLong();
	}

	private final void updateLabels()
	{
		// Verifica quantos d�gitos possui o score mais alto
		final int nDigits = String.valueOf( scores[0] ).length();
		
		for( int i = 0; i < scores.length; ++i )
		{
			final Label label = ( Label )getDrawable( FIRST_LABEL_INDEX + i );

			//#if SCREEN_SIZE != "SMALL"
			if( GameMIDlet.isBig() )
				label.setText( ( i + 1 ) + ". " + GameMIDlet.formatNumStr( scores[i], nDigits, '�', '\0' ) );
			else
				label.setText( ( i + 1 ) + ".   " + GameMIDlet.formatNumStr( scores[i], nDigits, '�', '\0' ) );
			//#else
//# 				label.setText( ( i + 1 ) + ". " + GameMIDlet.formatNumStr( scores[i], nDigits, '�', '\0' ) );
			//#endif

			label.defineReferencePixel( label.getWidth() >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, label.getRefPixelY() );
		}
	}
	
	public final void update( int delta )
	{
		super.update( delta );

		if( lastHighScoreIndex >= 0 )
		{
			accTime += delta;

			if( accTime >= BLINK_RATE )
			{
				accTime %= BLINK_RATE;

				final Drawable aux = getDrawable( FIRST_LABEL_INDEX + lastHighScoreIndex );
				aux.setVisible( !aux.isVisible() );
			}
		}
	}

	public static final void stopBlink()
	{
		if( instance.lastHighScoreIndex >= 0 )
			instance.getDrawable( instance.FIRST_LABEL_INDEX + instance.lastHighScoreIndex ).setVisible( true );

		instance.lastHighScoreIndex = -1;
	}

	public static final void eraseRecords()
	{
		try
		{
			GameMIDlet.eraseSlot( DATABASE_NAME, DATABASE_SLOT_SCORES );
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}

		loadRecords();
	}

	private static final void loadRecords()
	{
		try
		{
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_SCORES, instance );
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif

			// Preenche a tabela de pontos com valores "dummy"
			for( int i = 0; i < TOTAL_SCORES; ++i )
				scores[i] = ( TOTAL_SCORES - i ) * DUMMY_BASE;

			try
			{
				GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_SCORES, instance );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	}

	public void keyReleased( int key )
	{
	}

	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	
	public void onPointerDragged( int x, int y )
	{
		onPointerPressed( x, y );
	}

	public void onPointerPressed( int x, int y )
	{
		keyPressed( ScreenManager.KEY_NUM5 );
	}

	public void onPointerReleased( int x, int y )
	{
	}
	
	//#endif
}
