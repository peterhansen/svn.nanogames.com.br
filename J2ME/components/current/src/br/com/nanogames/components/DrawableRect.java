/**
 * DrawableRect.java
 * ©2008 Nano Games.
 *
 * Created on Aug 6, 2008 11:25:13 AM.
 */

package br.com.nanogames.components;

//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Color;
//# import java.awt.Graphics;
//#endif

/**
 * Essa classe encapsula a funcionalidade de desenho de um retângulo não preenchido. Para desenhar um retângulo
 * preenchido, utilize a classe <code>Pattern</code>.
 * @author Peter
 */
public class DrawableRect extends Drawable {
	
	private int color;
	
	
	public DrawableRect() {
	}
	
	
	public DrawableRect( int color ) {
		setColor( color );
	}
	
	
	public final int getColor() {
		return color;
	}
	
	
	public final void setColor( int color ) {
		this.color = color;
	}

	
	protected void paint( Graphics g ) {
		//#if J2SE == "false"
			g.setColor( color );
		//#else
//# 			g.setColor( new Color( color ) );
		//#endif
			
		g.drawRect( translate.x, translate.y, size.x - 1, size.y - 1 );
	}

}
