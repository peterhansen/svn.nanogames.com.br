//#if TOUCH == "true"
	/**
	 * TouchKeyPad.java
	 *
	 * Created on Aug 6, 2009, 8:11:53 PM
	 *
	 */

	package br.com.nanogames.components.userInterface.form;

	import br.com.nanogames.components.DrawableImage;
	import br.com.nanogames.components.ImageFont;
	import br.com.nanogames.components.Pattern;
	import br.com.nanogames.components.util.Point;
	import br.com.nanogames.components.DrawableGroup;
	import br.com.nanogames.components.userInterface.form.borders.Border;
	import br.com.nanogames.components.Label;
	import br.com.nanogames.components.online.NanoOnline;
	import br.com.nanogames.components.userInterface.PointerListener;
	import br.com.nanogames.components.userInterface.form.TextBox;
	
	//#if J2SE == "false"
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
	//#else
//# 	import java.awt.Color;
//# 	import java.awt.Graphics;
//# 	import java.awt.Image;
//# 	import java.awt.image.BufferedImage;
	//#endif

	/**
	 *
	 * @author Peter
	 */
	public class TouchKeyPad extends Component implements PointerListener {

		/** Largura mínima interna do teclado. */
		private static final short MIN_WIDTH = 240;
		/** Altura mínima interna do teclado. */
		private final short MIN_HEIGHT;

		private static final int FP_MAX_SCREEN_HEIGHT_PERCENT = NanoMath.divInt( 4, 10 );

		private static final int FP_CHAR_BOX_WIDTH_PERCENT	= NanoMath.divInt( 20, MIN_WIDTH );
		private static final int FP_CHAR_BOX_HEIGHT_PERCENT	= NanoMath.divInt( 20, 100 );

		private short CHAR_BOX_WIDTH;
		private short CHAR_BOX_HEIGHT;

		private static final int FP_CHANGE_PAGE_WIDTH_PERCENT = NanoMath.divInt( 56, MIN_WIDTH );
		private static final int FP_SPACEBAR_WIDTH_PERCENT = NanoMath.divInt( 116, MIN_WIDTH );
		private static final int FP_SHIFT_WIDTH_PERCENT = NanoMath.divInt( 32, MIN_WIDTH );

		private short CHANGE_PAGE_WIDTH;
		private short SPACEBAR_WIDTH;
		private short SHIFT_WIDTH;

		private static final byte START_Y = 9;

		private static final byte CHAR_SHIFT		= -1;
		private static final byte CHAR_OK			= -2;
		private static final byte CHAR_CHANGE_PAGE	= -3;
		private static final byte CHAR_CLEAR		= -4;
		private static final byte CHAR_NONE			= -5;

		/***/
		private static final int FP_CHAR_BOX_SPACING_X_PERCENT = NanoMath.divInt( 4, MIN_WIDTH );
		private static final int FP_CHAR_BOX_SPACING_Y_PERCENT = NanoMath.divInt( 4, 100 );

		private short CHAR_BOX_SPACING_X;
		private short CHAR_BOX_SPACING_Y;

		private byte CHAR_BOX_TOTAL_WIDTH;
		private byte CHAR_BOX_TOTAL_HEIGHT;

		private static final byte ROW_MAX_CHARS = 10;
		private static final byte ROW_MAX_CHARS_SHIFT = ROW_MAX_CHARS - 3;

		private static final int COLOR_FILL_DARK = 0x7dbfc7;
		private static final int COLOR_FILL_LIGHT = 0xc3e2e5;
		private static final int COLOR_BORDER_TOP = 0x038392;

		private final int colorFillDark;
		private final int colorFillLight;
		private final int colorBorderTop;

		private static final byte BORDER_TOP_HEIGHT = 1;
		private static final byte FILL_LIGHT_HEIGHT = 3;

		private final char[][] DEFAULT_ROWS = { { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p' },
												{ 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ç' },
												{ 'z', 'x', 'c', 'v', 'b', 'n', 'm' },
												{ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' } };

		/***/
		private static final String TEXT_CHANGE_PAGE = ".,!123";

		/** Linhas finais de caracteres. */
		private final char[][] rows;

		/***/
		protected final ImageFont font;

		/** Imagem pré-renderizada do teclado. */
		protected Image imgBuffer;

		private DrawableImage image;

		private final DrawableImage imgClear;

		private final DrawableImage imgShift;

		private static final byte ROWS_PER_PAGE = 3;

		private final byte ROWS_TOTAL;

		private byte initialRow;

		private short SPACEBAR_X;

		/** Estado do caps lock do teclado. */
		private boolean capsLock;

		private final Label dummyLabel;

		private final Border dummyBorder;

		private short charPressed = CHAR_NONE;

		private TextBox textBox;

		/***/
		private final Pattern patternLeft;

		/***/
		private final Pattern patternRight;

		private byte defaultInputMode = TextBox.INPUT_MODE_ANY;


		public TouchKeyPad( ImageFont font, DrawableImage imgClear, DrawableImage imgShift ) throws Exception {
			this( font, imgClear, imgShift, NanoOnline.getBorder(), COLOR_FILL_DARK, COLOR_FILL_LIGHT, COLOR_BORDER_TOP );
		}


		public TouchKeyPad( ImageFont font, DrawableImage imgClear, DrawableImage imgShift, Border border, int colorFillDark, int colorFillLight, int colorBorderTop ) throws Exception {
			super( 3 );

			this.font = font;

			dummyLabel = new Label( font, null );
			dummyBorder = border;

			this.colorBorderTop = colorBorderTop;
			this.colorFillDark = colorFillDark;
			this.colorFillLight = colorFillLight;

			this.imgClear = imgClear;
			this.imgShift = imgShift;

			final char[][] temp = new char[ 50 ][];
			byte rowIndex = ( byte ) DEFAULT_ROWS.length;

			MIN_HEIGHT = ( short ) ( ( START_Y << 1 ) + NanoMath.toInt( NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( font.getHeight() ), NanoMath.ONE + FP_CHAR_BOX_SPACING_Y_PERCENT ), NanoMath.toFixed( rowIndex ) ) ) );
			
			final char[] specialChars = font.getAllChars( ImageFont.CHAR_TYPE_SPECIAL );
			final char[] accentChars = font.getAllChars( isCapsLock() ? ImageFont.CHAR_TYPE_ACCENT_UPPER_CASE : ImageFont.CHAR_TYPE_ACCENT_LOWER_CASE );

			final char[] allChars = new char[ specialChars.length + accentChars.length ];
			System.arraycopy( specialChars, 0, allChars, 0, specialChars.length );
			System.arraycopy( accentChars, 0, allChars, specialChars.length, accentChars.length );
			// copia os caracteres especiais e acentuados
			short total = 0;
			while ( total < allChars.length ) {
				final byte rowTotal = ( byte ) Math.min( allChars.length - total, ( rowIndex % ROWS_PER_PAGE ) == ( ROWS_PER_PAGE - 1 ) ? ROW_MAX_CHARS_SHIFT : ROW_MAX_CHARS );
				temp[ rowIndex ] = new char[ rowTotal ];
				System.arraycopy( allChars, total, temp[ rowIndex++ ], 0, rowTotal );

				total += rowTotal;
			}
			ROWS_TOTAL = rowIndex;

			rows = new char[ ROWS_TOTAL ][];
			System.arraycopy( DEFAULT_ROWS, 0, rows, 0, DEFAULT_ROWS.length );
			System.arraycopy( temp, DEFAULT_ROWS.length, rows, DEFAULT_ROWS.length, rowIndex - DEFAULT_ROWS.length );

			patternLeft = new Pattern( colorFillLight );
			insertDrawable( patternLeft );

			patternRight = new Pattern( colorFillLight );
			insertDrawable( patternRight );

			refreshSize( MIN_WIDTH, MIN_HEIGHT );
		}


		public byte getDefaultInputMode() {
			return defaultInputMode;
		}


		public void setDefaultInputMode( byte defaultInputMode ) {
			this.defaultInputMode = defaultInputMode;
		}


		protected final void refreshSize( int width, int height ) {
			setSize( Math.max( width, MIN_WIDTH ), 
					 Math.max( NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( height ), FP_MAX_SCREEN_HEIGHT_PERCENT ) ), MIN_HEIGHT ) );
		}


		private final void prepareRows() {
			final Graphics g = imgBuffer.getGraphics();

			//#if J2SE == "false"
				g.setColor( colorBorderTop );
				g.drawRect( 0, 0, imgBuffer.getWidth(), BORDER_TOP_HEIGHT - 1 );
				g.setColor( colorFillLight );
				g.fillRect( 0, BORDER_TOP_HEIGHT, imgBuffer.getWidth(), FILL_LIGHT_HEIGHT );
				g.setColor( colorFillDark );
				g.fillRect( 0, BORDER_TOP_HEIGHT + FILL_LIGHT_HEIGHT, imgBuffer.getWidth(), imgBuffer.getHeight() - ( BORDER_TOP_HEIGHT + FILL_LIGHT_HEIGHT ) );
			//#else
//# 				g.setColor( new Color( colorBorderTop ) );
//# 				g.drawRect( 0, 0, imgBuffer.getWidth( null ), BORDER_TOP_HEIGHT - 1 );
//# 				g.setColor( new Color( colorFillLight ) );
//# 				g.fillRect( 0, BORDER_TOP_HEIGHT, imgBuffer.getWidth( null ), FILL_LIGHT_HEIGHT );
//# 				g.setColor( new Color( colorFillDark ) );
//# 				g.fillRect( 0, BORDER_TOP_HEIGHT + FILL_LIGHT_HEIGHT, imgBuffer.getWidth( null ), imgBuffer.getHeight( null ) - ( BORDER_TOP_HEIGHT + FILL_LIGHT_HEIGHT ) );
			//#endif

			pushClip( g );

			int y = START_Y;
			final byte inputMode = ( textBox == null ) ? defaultInputMode : textBox.getInputMode();
			switch ( inputMode ) {
				case TextBox.INPUT_MODE_NUMBERS:
					final short ROW_NUMERIC_WIDTH = ( short ) ( ( CHAR_BOX_TOTAL_WIDTH << 1 ) + CHAR_BOX_WIDTH );
					final short NUMERIC_CLEAR_WIDTH = ( short ) ( CHAR_BOX_TOTAL_WIDTH + CHAR_BOX_WIDTH );
					final short START_X = ( short ) ( ( getWidth() - ROW_NUMERIC_WIDTH ) >> 1 );

					for ( byte i = 0; i < 10; ++i ) {
						final char number = ( char ) ( '0' + ( ( i + 1 ) % 10 ) );
						dummyBorder.setState( charPressed == number ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
						drawChar( g, ( short ) number, START_X + ( CHAR_BOX_TOTAL_WIDTH * ( i % 3 ) ), START_Y + ( CHAR_BOX_TOTAL_HEIGHT * ( i / 3 ) ), CHAR_BOX_WIDTH, CHAR_BOX_HEIGHT );
					}

					// desenha a tecla backspace
					dummyBorder.setState( charPressed == CHAR_CLEAR ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
					drawImage( g, imgClear, START_X + CHAR_BOX_TOTAL_WIDTH, START_Y + ( CHAR_BOX_TOTAL_HEIGHT * 3 ), NUMERIC_CLEAR_WIDTH, CHAR_BOX_HEIGHT );
				break;

				default:
					final short ROW_SHIFT_WIDTH = ( short ) ( ROW_MAX_CHARS_SHIFT * ( CHAR_BOX_WIDTH + CHAR_BOX_SPACING_X ) - CHAR_BOX_SPACING_X );

					for ( int rowIndex = initialRow, count = 0; rowIndex < rows.length && count < ROWS_PER_PAGE; ++rowIndex, ++count, y += CHAR_BOX_TOTAL_HEIGHT ) {
						final char[] row = rows[ rowIndex ];
						final short ROW_WIDTH = ( short ) ( row.length * ( CHAR_BOX_WIDTH + CHAR_BOX_SPACING_X ) - CHAR_BOX_SPACING_X );

						for ( int i = 0, x = ( getWidth() - ROW_WIDTH ) >> 1; i < row.length; ++i, x += CHAR_BOX_TOTAL_WIDTH ) {
							dummyBorder.setState( row[ i ] == charPressed ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
							final char c = isCapsLock() ? Character.toUpperCase( row[ i ] ) : row[ i ];
							drawChar( g, ( short ) c, x, y, CHAR_BOX_WIDTH, CHAR_BOX_HEIGHT );
						}
					}
					y = START_Y + ( ( ROWS_PER_PAGE - 1 ) * CHAR_BOX_TOTAL_HEIGHT );
					// desenha a tecla shift
					dummyBorder.setState( charPressed == CHAR_SHIFT ? Border.STATE_PRESSED : ( isCapsLock() ? Border.STATE_FOCUSED : Border.STATE_UNFOCUSED ) );
					drawImage( g, imgShift, ( ( getWidth() - ROW_SHIFT_WIDTH ) >> 1 ) - CHAR_BOX_SPACING_X - SHIFT_WIDTH, y, SHIFT_WIDTH, CHAR_BOX_HEIGHT );

					// desenha a tecla backspace
					dummyBorder.setState( charPressed == CHAR_CLEAR ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
					drawImage( g, imgClear, ( ( getWidth() - ROW_SHIFT_WIDTH ) >> 1 ) + ROW_SHIFT_WIDTH + CHAR_BOX_SPACING_X, y, SHIFT_WIDTH, CHAR_BOX_HEIGHT );

					y += CHAR_BOX_TOTAL_HEIGHT;

					// desenha a barra de espaço
					dummyBorder.setState( charPressed == ' ' ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
					drawBox( g, SPACEBAR_X, y, SPACEBAR_WIDTH, CHAR_BOX_HEIGHT );

					// desenha o botão de troca de página de caracteres
					dummyBorder.setState( charPressed == CHAR_CHANGE_PAGE ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
					drawString( g, TEXT_CHANGE_PAGE, SPACEBAR_X - CHAR_BOX_SPACING_X - CHANGE_PAGE_WIDTH, y, CHANGE_PAGE_WIDTH, CHAR_BOX_HEIGHT );
			}


			// desenha o botão OK
			dummyBorder.setState( charPressed == CHAR_OK ? Border.STATE_PRESSED : Border.STATE_UNFOCUSED );
			drawString( g, "OK", SPACEBAR_X + SPACEBAR_WIDTH + CHAR_BOX_SPACING_X, START_Y + CHAR_BOX_TOTAL_HEIGHT * ROWS_PER_PAGE, CHANGE_PAGE_WIDTH, CHAR_BOX_HEIGHT );

			popClip( g );
		}


		private final void drawBox( Graphics g, int x, int y, int width, int height ) {
			dummyBorder.setSize( width, height );
			dummyBorder.setPosition( x, y );
			dummyBorder.draw( g );
		}


		private final void drawString( Graphics g, String text, int x, int y, int width, int height ) {
			drawBox( g, x, y, width, height );
			dummyLabel.drawString( g, text, dummyBorder.getPosX() + ( ( width - font.getTextWidth( TEXT_CHANGE_PAGE ) ) >> 1 ), y + ( ( height - font.getHeight() ) >> 1 ) );
		}


		private final void drawChar( Graphics g, short c, int x, int y, int width, int height ) {
			drawBox( g, x, y, width, height );
			dummyLabel.drawChar( g, ( char ) c, dummyBorder.getPosX() + ( ( width - font.getCharWidth( ( char ) c ) ) >> 1 ), y + ( ( height - font.getHeight() ) >> 1 ) );
		}


		private final void drawImage( Graphics g, DrawableImage img, int x, int y, int width, int height ) {
			drawBox( g, x, y, width, height );
			img.setPosition( x + ( ( width - img.getWidth() ) >> 1 ), y + ( ( height - imgClear.getHeight() ) >> 1 ) );
			img.draw( g );
		}


		public boolean isCapsLock() {
			return capsLock;
		}


		public void setCapsLock( boolean capsLock ) {
			this.capsLock = capsLock;
			prepareRows();
		}


		public void onPointerDragged( int x, int y ) {
		}


		public void onPointerPressed( int x, int y ) {
			x -= getPosX();
			y -= getPosY();

			final short c = getCharAt( x, y );
			charPressed = c;
			switch ( c ) {
				case CHAR_OK:
					final Form f = getComponentForm();
					if ( f == null )
						dispatchActionEvent( new Event( this, Event.EVT_BUTTON_CONFIRMED ) );
					else
						f.hideTouchKeyPad();
				break;

				case CHAR_CLEAR:
					if ( textBox != null )
						textBox.backSpace();
				break;

				case CHAR_SHIFT:
					setCapsLock( !isCapsLock() );
				return;

				case CHAR_CHANGE_PAGE:
					setInitialRow( initialRow + ROWS_PER_PAGE );
				break;

				case CHAR_NONE:
				break;

				default:
					charPressed = ( short ) Character.toLowerCase( ( char ) c );
					if ( textBox != null )
						textBox.insertChar( ( char ) c);
			}
			prepareRows();
		}


		public void onPointerReleased( int x, int y ) {
			// TODO funcionamento de botão nas teclas OK, clear, troca de página e repasse de eventos ao textbox
	//		switch ( charPressed ) {
	//			case CHAR_OK:
	//				getComponentForm().hideTouchKeyPad();
	//			break;
	//
	//			case CHAR_CLEAR:
	//				if ( textBox != null )
	//					textBox.backSpace();
	//			break;
	//
	//			case CHAR_SHIFT:
	//				setCapsLock( !isCapsLock() );
	//			return;
	//
	//			case CHAR_CHANGE_PAGE:
	//				setInitialRow( initialRow + ROWS_PER_PAGE );
	//			break;
	//
	//			case CHAR_NONE:
	//			break;
	//
	//			default:
	//				charPressed = ( short ) Character.toLowerCase( ( char ) c );
	//				if ( textBox != null )
	//					textBox.insertChar( ( char ) c);
	//		}

			if ( charPressed != CHAR_NONE ) {
				charPressed = CHAR_NONE;
				prepareRows();
			}
		}


		private final short getCharAt( int x, int y ) {
			y -= START_Y;
			final byte row = ( byte ) ( initialRow + ( y / CHAR_BOX_TOTAL_HEIGHT ) );
			final byte rowInPage = ( byte ) ( row % ROWS_PER_PAGE );

			final byte inputMode = ( textBox == null ) ? TextBox.INPUT_MODE_ANY : textBox.getInputMode();
			switch ( inputMode ) {
				case TextBox.INPUT_MODE_NUMBERS:
					final short numericWidth = ( short ) ( 3 * ( CHAR_BOX_TOTAL_WIDTH ) - CHAR_BOX_SPACING_X );
					final short numericStartX = ( short ) ( ( getWidth() - numericWidth ) >> 1 );
					final short column = ( short ) ( ( x - numericStartX ) / CHAR_BOX_TOTAL_WIDTH );

					if ( row - initialRow < ROWS_PER_PAGE ) {
						if ( x >= numericStartX && column < 3 ) {
							return ( short ) ( '1' + ( 3 * rowInPage ) + column );
						}
					} else {
						if ( x >= numericStartX ) {
							switch ( column ) {
								case 0:
									return ( short ) '0';

								case 1:
								case 2:
									return CHAR_CLEAR;

								default:
									if ( x >= SPACEBAR_X + SPACEBAR_WIDTH )
										return CHAR_OK;
							}
						}
					}
				break;

				case TextBox.INPUT_MODE_ANY:
				case TextBox.INPUT_MODE_EMAIL:
				case TextBox.INPUT_MODE_PASSWORD:
				case TextBox.INPUT_MODE_URL:
					if ( row - initialRow < ROWS_PER_PAGE ) {
						final short rowWidth = ( short ) ( ( rowInPage >= ROWS_TOTAL - initialRow ? ROW_MAX_CHARS_SHIFT : rows[ row ].length ) * ( CHAR_BOX_WIDTH + CHAR_BOX_SPACING_X ) - CHAR_BOX_SPACING_X );
						final short initialX = ( short ) ( ( getWidth() - rowWidth ) >> 1 );

						if ( rowInPage == ROWS_PER_PAGE - 1 ) {
							if ( x < initialX )
								return CHAR_SHIFT;
							else if ( x >= initialX + rowWidth )
								return CHAR_CLEAR;
							else if ( rowInPage < ROWS_TOTAL - initialRow ) {
								final short index = ( short ) ( ( x - initialX ) / CHAR_BOX_TOTAL_WIDTH );
								return ( short ) getChar( row, index );
							}
						} else {
							if ( rowInPage < ROWS_TOTAL - initialRow && x >= initialX && x <= initialX + rowWidth ) {
								final short index = ( short ) ( ( x - initialX ) / CHAR_BOX_TOTAL_WIDTH );
								return ( short ) getChar( row, index );
							}
						}
					} else {
						if ( x < SPACEBAR_X ) {
							return CHAR_CHANGE_PAGE;
						} else if ( x >= SPACEBAR_X + SPACEBAR_WIDTH ) {
							return CHAR_OK;
						} else {
							return ' ';
						}
					}
				break;
			}


			return CHAR_NONE;
		}


		private final char getChar( int row, int index ) {
			return isCapsLock() ? Character.toUpperCase( rows[ row ][ index ] ) : rows[ row ][ index ];
		}


		private final void setInitialRow( int row ) {
			if ( row >= ROWS_TOTAL )
				row = 0;

			initialRow = ( byte ) row;
			prepareRows();
		}


		public final TextBox getTextBox() {
			return textBox;
		}


		public void setTextBox( TextBox textBox ) {
			this.textBox = textBox;
			prepareRows();
		}


		public String getUIID() {
			return "TouchKeyPad";
		}


		public void setSize( int width, int height ) {
			final Point previousSize = new Point( size );

			super.setSize( width, height );

			if ( !size.equals( previousSize ) ) {
				if ( image != null ) {
					removeDrawable( image );
					image = null;
				}
				
				//#if J2SE == "false"
					imgBuffer = Image.createImage( getWidth(), getHeight() );
				//#else
	//# 				imgBuffer = new BufferedImage( getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB );
				//#endif
				image = new DrawableImage( imgBuffer );
				insertDrawable( image );
				image.setPosition( ( width - image.getWidth() ) >> 1, 0 );
				patternLeft.setSize( image.getPosX(), height );

				final int FP_WIDTH = NanoMath.toFixed( width );
				final int FP_HEIGHT = NanoMath.toFixed( height );

				CHANGE_PAGE_WIDTH = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_WIDTH, FP_CHANGE_PAGE_WIDTH_PERCENT ) );
				CHAR_BOX_WIDTH = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_WIDTH, FP_CHAR_BOX_WIDTH_PERCENT ) );
				CHAR_BOX_HEIGHT = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_HEIGHT, FP_CHAR_BOX_HEIGHT_PERCENT ) );
				CHAR_BOX_SPACING_X = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_WIDTH, FP_CHAR_BOX_SPACING_X_PERCENT ) );
				CHAR_BOX_SPACING_Y = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_HEIGHT, FP_CHAR_BOX_SPACING_Y_PERCENT ) );
				SHIFT_WIDTH = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_WIDTH, FP_SHIFT_WIDTH_PERCENT ) );
				SPACEBAR_WIDTH = ( short ) NanoMath.toInt( NanoMath.mulFixed( FP_WIDTH, FP_SPACEBAR_WIDTH_PERCENT ) );
				SPACEBAR_X = ( short ) ( ( getWidth() - SPACEBAR_WIDTH ) >> 1 );

				CHAR_BOX_TOTAL_WIDTH = ( byte ) ( CHAR_BOX_WIDTH + CHAR_BOX_SPACING_X );
				CHAR_BOX_TOTAL_HEIGHT = ( byte ) ( CHAR_BOX_HEIGHT + CHAR_BOX_SPACING_Y );

				patternRight.setPosition( image.getPosX() + image.getWidth(), 0 );
				patternRight.setSize( width - patternRight.getPosX(), height );

				prepareRows();
			}
		}


	}

//#endif