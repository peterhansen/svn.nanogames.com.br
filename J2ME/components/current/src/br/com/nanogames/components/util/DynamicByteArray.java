/**
 * DinamicByteArray.java
 * ©2008 Nano Games.
 *
 * Created on 04/04/2008 16:29:10.
 */

package br.com.nanogames.components.util;

import br.com.nanogames.components.userInterface.AppMIDlet;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Daniel L. Alves
 */

public final class DynamicByteArray
{
	/** Tamanho padrão do buffer utilizado para a leitura de dados em readInputStream() */
	public static final short DEFAULT_READ_BUFFER_SIZE = 1024;

	/** Dados contidos no objeto */
	private byte[] byteArray;

	
	/** Obtém o tamanho do array */
	public final int length()
	{
		return byteArray.length;
	}
	
	/** Insere novos dados no array dinâmico
	 * @param data Array de onde iremos copiar os dados
	 * @param offset Índice do array a partir do qual iremos copiar os dados
	 * @param nBytes Quantos bytes iremos copiar do array fornecido
	 * @throws java.lang.IllegalArgumentException
	 */
	public final void insertData( byte[] data, int offset, int nBytes ) throws IllegalArgumentException
	{
		if( ( offset < 0 ) || ( nBytes < 0 ) )
		{
			//#if DEBUG == "true"
//# 				throw new IllegalArgumentException( "Os parâmetros offset e nBytes devem ser maiores ou iguais a zero" );
			//#else
			throw new IllegalArgumentException();
			//#endif
		}
		
		if( nBytes == 0 )
			return;
		
		int currLength = 0;
			
		// Incrementa o tamanho do array
		if( byteArray != null )
		{
			currLength = byteArray.length;
			final byte[] aux = new byte[ currLength + nBytes ];
			System.arraycopy( byteArray, 0, aux, 0, currLength );
			
			byteArray = aux;
			AppMIDlet.gc();
		}
		else
		{
			byteArray = new byte[ nBytes ];
		}
		
		// Armazena os novos dados
		System.arraycopy( data, offset, byteArray, currLength, nBytes );
	}
	
	/** Obtém o conteúdo do array dinâmico de bytes */
	public final byte[] getData()
	{
		return byteArray;
	}


	public final void readInputStream( InputStream input ) throws IOException {
		readInputStream( input, -1 );
	}

	
	/** Preenche o array dinâmico com os dados lidos de um InputStream.
	 *
	 * @param input stream usado para leitura dos dados.
	 * @param dataLimit limite de bytes a serem lidos. Caso seja menor ou igual a zero, lê todos os dados do stream.
	 */
	public final void readInputStream( InputStream input, int dataLimit ) throws IOException {
		if ( dataLimit <= 0 )
			dataLimit = Integer.MAX_VALUE;

		// Cria o buffer que irá armazenar os dados lidos
		final byte[] buffer = new byte[ Math.min( DEFAULT_READ_BUFFER_SIZE, dataLimit ) ];

		// Lê os dados da stream de entrada
		int nBytes;
		int diff = dataLimit - ( byteArray == null ? 0 : byteArray.length );
		while ( ( nBytes = input.read( buffer, 0, Math.min( buffer.length, diff ) ) ) != -1 && ( diff > 0 ) ) {
			insertData( buffer, 0, nBytes );
			diff = dataLimit - byteArray.length;
		}
	}

	
}