/*
 * MUV.java
 *
 * Created on October 18, 2007, 6:47 PM
 *
 */

package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
public final class MUV {
	
	private int speed;
	
	private int lastSpeedModule;
	
	
	public MUV() {
	}
	
	
	/**
	 * Creates a new instance of MUV
	 */
	public MUV( int speed ) {
		setSpeed( speed );
	}
	
	
	/**
	 * Define a velocidade do movimento, e zera o resto da divisão do último cálculo. Equivalente à chamada de <i>setSpeed(speed, true)</i>.
	 * @param speed velocidade do movimento em unidades por segundo.
	 * @see #setSpeed(int,boolean)
	 */
	public final void setSpeed( int speed ) {
		setSpeed( speed, true );
	}
	
	
	/**
	 * Define a velocidade do movimento.
	 * @param speed velocidade do movimento em unidades por segundo.
	 * @param resetModule indica se o resto da divisão do último cálculo realizado deve ser zerado.
	 * @see #setSpeed(int)
	 */
	public final void setSpeed( int speed, boolean resetModule ) {
		this.speed = speed;
		if ( resetModule )
			lastSpeedModule = 0;		
	}
	
	
	public final long updateLong( long delta ) {
		final long ds = lastSpeedModule + ( speed * delta );
		final long dx = ds / 1000;
		
		lastSpeedModule = ( int ) ( ds % 1000 );
		
		return dx;
	}
	
	
	public final int updateInt( int delta ) {
		final int ds = lastSpeedModule + ( speed * delta );
		final int dx = ds / 1000;
		
		lastSpeedModule = ds % 1000;
		
		return dx;
	}	
	
	
	public final int getSpeed() {
		return speed;
	}
	
}
