/**
 * FormLabel.java
 * 
 * Created on 10/Nov/2008, 12:11:07
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;

/**
 *
 * @author Peter
 */
public class FormMarqueeLabel extends DrawableComponent {

	protected final MarqueeLabel label;
			
	
	public FormMarqueeLabel( ImageFont font, String text ) throws Exception {
		super( new MarqueeLabel( font, text ) );
		
		label = ( MarqueeLabel ) d;

		if ( text != null )
			setSize( font.getTextWidth( text ), getHeight() );
		
		setFocusable( false );
	}
	
	
	public String getUIID() {
		return "marqueelabel";
	}


	public final void setText( String text ) {
		setText( text, true );
	}
	
	
	public void setText( String text, boolean setSize ) {
		label.setText( text, setSize );
		
		if ( setSize )
			resizeToFit();
	}


	public final void setText( int textIndex, boolean setSize ) {
		label.setText( textIndex, setSize );
		
		if ( setSize )
			resizeToFit();
	}


	public final void setText( int textIndex ) {
		setText( textIndex, false );
	}


	public void setFont( ImageFont font ) throws NullPointerException {
		label.setFont( font );
	}


	public String getText() {
		return label.getText();
	}


	public void setWaitTime( int waitTime ) {
		label.setWaitTime( waitTime );
	}


	public void setTextOffset( int textOffset ) {
		label.setTextOffset( textOffset );
	}


	public void setSpeed( int speed ) {
		label.setSpeed( speed );
	}


//	public void setSize( int width, int height ) {
//		label.setSize( width, height );
//	}


	public void setScrollMode( byte mode ) {
		label.setScrollMode( mode );
	}


	public void setScrollFrequency( byte frequency ) {
		label.setScrollFrequency( frequency );
	}


	public int getWaitTime() {
		return label.getWaitTime();
	}


	public final int getTextOffset() {
		return label.getTextOffset();
	}


	public int getSpeed() {
		return label.getSpeed();
	}


	public byte getScrollMode() {
		return label.getScrollMode();
	}


	public final byte getScrollFrequency() {
		return label.getScrollFrequency();
	}


	public void setSize( int width, int height ) {
		setSize( width, height, true );
	}

}
