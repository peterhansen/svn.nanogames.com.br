/**
 * MainScreen.java
 * 
 * Created on Dec 16, 2008, 3:00:17 PM
 *
 */

package br.com.nanogames.components.online.newsfeeder;

import br.com.nanogames.components.online.*;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
/**
 *
 * @author Peter
 */
public final class NewsFeederScreen extends NanoOnlineContainer implements ConnectionListener {

	/***/
	private static final byte TOTAL_ENTRIES = NewsFeederManager.MAX_NEWS_ENTRIES + 5;

	/** Botão para atualizar a lista de noticias. */
	private static final byte ID_BUTTON_UPDATE = TOTAL_ENTRIES;

	/** Botão para voltar (ou cancelar a conexão, no modo online). */
	private static final byte ID_BUTTON_BACK = ID_BUTTON_UPDATE + 1;

	private static final byte ID_FIRST_ENTRY = ID_BUTTON_BACK + 1;
	
	private static final byte ID_UPDATE_ENTRY = ID_FIRST_ENTRY + TOTAL_ENTRIES;
	
	/** Id do botão para acesso à URL de uma notícia. */
	private static final byte ID_BUTTON_URL = ID_UPDATE_ENTRY + 1;

	/** Contâiner das entradas das notícias. */
	private final Container entriesContainer;

	private final Button updateButton;
	private final Button backButton;

	/***/
	private NewsFeederEntry[] newsFeederEntries = new NewsFeederEntry[ 0 ];
	
	private byte currentNewsFeederEntryIndex = -1;

	private static final byte VIEW_MODE_ENTRIES		= 0;
	private static final byte VIEW_MODE_READ_ENTRY	= 1;

	private byte viewMode;


	/**
	 *
	 * @param online
	 * @param type
	 * @param backIndex
	 * @throws java.lang.Exception
	 */
	public NewsFeederScreen( int backIndex ) throws Exception {
		super( TOTAL_ENTRIES );

		setBackIndex( backIndex );

		final FlowLayout flowLayout = new FlowLayout( FlowLayout.AXIS_VERTICAL, ANCHOR_HCENTER );
		flowLayout.gap.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );
		entriesContainer = new Container( TOTAL_ENTRIES, flowLayout );

		updateButton = NanoOnline.getButton( TEXT_UPDATE, ID_BUTTON_UPDATE, this );
		backButton = NanoOnline.getButton( TEXT_BACK, ID_BUTTON_BACK, this );

		final NewsFeederManager manager = new NewsFeederManager();
		manager.load();
		newsFeederEntries = manager.getEntries();

		setViewMode( VIEW_MODE_ENTRIES, false );
	}
	
	
	/**
	 * Indica se há entrada(s) de notícias não lida(s).
	 * @return true, caso haja pelo menos uma entrada não lida, e false caso contrário.
	 */
	public static final boolean hasUnreadEntries() {
		final NewsFeederManager manager = new NewsFeederManager();
		manager.load();
		
		for ( byte i = 0; i < manager.getEntries().length; ++i ) {
			if ( !manager.getEntries()[ i ].isRead() )
				return true;
		}
				
		return false;
	}


	/**
	 * 
	 * @param evt
	 */
	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ID_BUTTON_UPDATE:
						// envia os recordes de cada perfil para o servidor
						try {
							final ByteArrayOutputStream b = new ByteArrayOutputStream();
							final DataOutputStream out = new DataOutputStream( b );

							// escreve os dados globais - inclusive as informações do news feeder
							NanoOnline.writeGlobalData( out );
							out.flush();

							NanoConnection.post( NANO_ONLINE_URL + "news_feeds/refresh", b.toByteArray(), this, false );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								AppMIDlet.log( e , "47");
							//#endif
						}
					break;

					case ProgressBar.ID_SOFT_RIGHT:
					case ID_BUTTON_BACK:
						onBack();
					break;
					
					case ID_BUTTON_URL:
						AppMIDlet.browseURL( ( currentNewsFeederEntryIndex == ID_UPDATE_ENTRY ) ? NewsFeederManager.getUpdateEntry().getURL() : newsFeederEntries[ currentNewsFeederEntryIndex ].getURL(), true );
					break;
					
					case ID_UPDATE_ENTRY:
					default:
						try {
							setViewMode( VIEW_MODE_READ_ENTRY, true, sourceId == ID_UPDATE_ENTRY ? ID_UPDATE_ENTRY : sourceId - ID_FIRST_ENTRY );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								AppMIDlet.log( e , "48");
							//#endif

							try {
								setViewMode( VIEW_MODE_ENTRIES, true );
							} catch ( Exception e1 ) {
								//#if DEBUG == "true"
	//# 								AppMIDlet.log( e1 , "49");
								//#endif
							}
						}
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( viewMode ) {
					case VIEW_MODE_ENTRIES:
						switch ( key ) {
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_BACK:
								onBack();
							break;
						} // fim switch ( key )
					break;

					case VIEW_MODE_READ_ENTRY:
						switch ( key ) {
							case ScreenManager.KEY_BACK:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.FIRE:
							case ScreenManager.KEY_NUM5:
							case ScreenManager.KEY_SOFT_LEFT:
								try {
									setViewMode( VIEW_MODE_ENTRIES, true );
								} catch ( Exception e ) {
									//#if DEBUG == "true"
//# 										AppMIDlet.log( e , "50");
									//#endif
								}
							break;
						}
					break;
				}

			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	protected final void onBack() {
		NewsFeederManager.save( newsFeederEntries );
		super.onBack();
	}


	/**
	 *
	 */
	private final void refreshEntries( boolean refreshLayout ) throws Exception {
		entriesContainer.removeAllDrawables();

		// indica a hora da última atualização
		final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 0 );
		if ( NewsFeederManager.getLastUpdated() > 0 )
			text.setText( NanoOnline.getText( TEXT_UPDATED_AT ) + AppMIDlet.formatTime( NewsFeederManager.getLastUpdated(), NanoOnline.getText( TEXT_TIME_FORMAT ) ) );
		else
			text.setText( NanoOnline.getText( TEXT_NO_NEWS_FOUND ) );

		entriesContainer.insertDrawable( text );

		final NewsFeederEntry updateEntry = NewsFeederManager.getUpdateEntry();
		NewsFeederEntry[] entries;
		if ( updateEntry == null ) {
			entries = newsFeederEntries;
		} else {
			entries = new NewsFeederEntry[ newsFeederEntries.length + 1 ];
			entries[ 0 ] = updateEntry;
			System.arraycopy( newsFeederEntries, 0, entries, 1, newsFeederEntries.length );
		}

		if ( entries.length > 0 ) {
			byte total = 0;
			for ( ; total < entries.length; ++total ) {
				final NewsFeederEntry entry = entries[ total ];
				int buttonId;
				if ( updateEntry == null ) {
					buttonId = ID_FIRST_ENTRY + total;
				} else {
					buttonId = ( total == 0 ) ? ID_UPDATE_ENTRY : ( ID_FIRST_ENTRY + total - 1 );
				}
				final Button button = NanoOnline.getButton( getEntryTitleDate( entry ), buttonId, null );
				button.setBorder( NanoOnline.getBorder( entry.isRead() ? BORDER_COLOR_TYPE_DEFAULT : BORDER_COLOR_TYPE_HIGHLIGHT ) );
				button.addEventListener( this );
				entriesContainer.insertDrawable( button );
			}
		}

		if ( refreshLayout ) {
			refreshLayout();
			entriesContainer.requestFocus();
		}
	}


	/**
	 * 
	 * @param entry
	 * @return
	 */
	private final String getEntryTitleDate( NewsFeederEntry entry ) {
		final StringBuffer buffer = new StringBuffer();

		buffer.append( AppMIDlet.formatTime( entry.getTime(), NanoOnline.getText( TEXT_TIME_FORMAT ) ) );
		buffer.append( " - " );

		buffer.append( entry.getTitle() );

		return buffer.toString();
	}


	/**
	 * 
	 * @param id
	 * @param data
	 */
	public final void processData( int id, byte[] data ) {
		if ( data != null ) {
			DataInputStream input = null;
			try {
				final Hashtable table = NanoOnline.readGlobalData( data );

				// o código de retorno é obrigatório
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				final StringBuffer buffer = new StringBuffer();

				switch ( returnCode ) {
					case RC_OK:
						// quando o retorno do servidor é ok, é sinal de que novas entradas do feeder já foram lidas
						// e gravadas no RMS. Nesse ponto, é necessário somente recarregá-las do RMS e exibir a nova lista.
						final NewsFeederManager manager = new NewsFeederManager();
						manager.load();
						newsFeederEntries = manager.getEntries();
						refreshEntries( true );

						NanoOnline.getProgressBar().clearProgressBar();
						NanoOnline.getProgressBar().showInfo( buffer.toString() ); // TODO exibir mensagem 
					break;

					case RC_APP_NOT_FOUND:
						NanoOnline.getProgressBar().showError( TEXT_ERROR_APP_NOT_FOUND );
					break;

					case RC_SERVER_INTERNAL_ERROR:
					default:
						// TODO deixar tratamento de erros genéricos (aparelho não suportado, erro interno do servidor, etc. em NanoOnlineContainer)
						NanoOnline.getProgressBar().showError( TEXT_ERROR_SERVER_INTERNAL );
					break;
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					AppMIDlet.log( e , "51");
				//#endif
			}
		}
	}


	/**
	 *
	 * @param viewMode
	 * @param refreshLayout
	 * @throws java.lang.Exception
	 */
	private final void setViewMode( int viewMode, boolean refreshLayout ) throws Exception {
		setViewMode( viewMode, refreshLayout, -1 );
	}


	/**
	 * 
	 * @param viewMode
	 * @param refreshLayout
	 * @param newsFeederEntryIndex
	 * @throws java.lang.Exception
	 */
	private final void setViewMode( int viewMode, boolean refreshLayout, int newsFeederEntryIndex ) throws Exception {
		while ( getUsedSlots() > MIN_COMPONENTS )
			removeDrawable( MIN_COMPONENTS );
		
		currentNewsFeederEntryIndex = ( byte ) newsFeederEntryIndex;
		
		switch ( viewMode ) {
			case VIEW_MODE_ENTRIES:
				setLayout( new FlowLayout( FlowLayout.AXIS_VERTICAL ) );
				
				NanoOnline.setTitle( TEXT_NEWS );
				insertDrawable( entriesContainer );
				insertDrawable( updateButton );
				insertDrawable( backButton );
				refreshEntries( false );
			break;

			case VIEW_MODE_READ_ENTRY:
				setLayout( new BorderLayout() );
				setScrollable( false );
				
				final FormText formText = new FormText( NanoOnline.getFont( FONT_BLACK ) );
				formText.addEventListener( this );
				formText.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
				formText.setScrollBarV( NanoOnline.getScrollBarV() );
				
				final NewsFeederEntry entry = newsFeederEntryIndex == ID_UPDATE_ENTRY ? NewsFeederManager.getUpdateEntry() : newsFeederEntries[ newsFeederEntryIndex ];
				
				formText.setText( entry.getContent() );

				formText.setMaxLines( 0 );
				insertDrawable( formText, BorderLayout.CENTER );
				
				// se houver URL específica, adiciona o botão para acesso
				if ( entry.getURL().length() > 0 ) {
					final Button button = NanoOnline.getButton( TEXT_ACCESS_URL, ID_BUTTON_URL, this );
					insertDrawable( button, BorderLayout.SOUTH );
				}

				NanoOnline.setTitle( getEntryTitleDate( entry ) );

				// marca a notícia como lida (ao sair dessa tela as informações são gravadas no RMS)
				entry.setRead( true );
			break;

			//#if DEBUG == "true"
//# 			default:
//# 				throw new IllegalArgumentException( "Invalid viewMode: " + viewMode );
			//#endif
		}

		this.viewMode = ( byte ) viewMode;

		if ( refreshLayout )
			refreshLayout();
	}


	/**
	 *
	 * @param id
	 * @param infoIndex
	 * @param extraData
	 */
	public final void onInfo( int id, int infoIndex, Object extraData ) {
		NanoOnline.getProgressBar().onInfo( id, infoIndex, extraData );
	}


	/**
	 * 
	 * @param id
	 * @param errorIndex
	 * @param extraData
	 */
	public final void onError( int id, int errorIndex, Object extraData ) {
		NanoOnline.getProgressBar().onError( id, errorIndex, extraData );
	}

}
