
/*
 * Quad.java
 *
 * Created on 21 de Agosto de 2007, 22:53
 *
 */
package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
public final class Quad {
	
	/** Tolerância na comparação de igualdade de 2 valores. */
	private static final int FP_MIN_TOLERANCE = NanoMath.divInt( 1, 100 );
	
	// âncoras do quad
	public static final byte ANCHOR_TOP_LEFT		= 0;
	public static final byte ANCHOR_TOP				= 1;
	public static final byte ANCHOR_TOP_RIGHT		= 2;
	public static final byte ANCHOR_RIGHT			= 3;
	public static final byte ANCHOR_BOTTOM_RIGHT	= 4;
	public static final byte ANCHOR_BOTTOM			= 5;
	public static final byte ANCHOR_BOTTOM_LEFT		= 6;
	public static final byte ANCHOR_LEFT			= 7;
	public static final byte ANCHOR_CENTER			= 8;
	
	// vetores unitários do plano
	public final Point3f top		= new Point3f();
	public final Point3f right		= new Point3f();
	public final Point3f normal		= new Point3f();
	public final Point3f position	= new Point3f();	// a orientação considerada é sempre top-left
	public final Point3f center		= new Point3f();	// armazena a posição central do quad, de forma a agilizar cálculos de colisão
	public int fp_d;							// coordenada d da equação do plano: p = ax + by + cz + d
	public int fp_halfDiagonal;					// metade do comprimento da diagonal do quad (acelera futuros cálculos)
	
	// módulos dos vetores (definem a largura e altura do Quad)
	public int fp_width;
	public int fp_height;	
	
	
	public Quad( Point3f pos, byte anchor, Point3f top, Point3f right, int fp_width, int fp_height  ) {
		top.normalize();
		right.normalize();
		normal.set( right.cross( top ) );
		normal.normalize();

		calculateHalfDiagonal();

		setPosByAnchor( pos, anchor  );

		// para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) é um ponto qualquer pertencente a esse plano.
		fp_d = ( normal.mul( -1 ) ).dot( position );
	}
	
	
	public Quad( Point3f bottomLeft, Point3f topLeft, Point3f bottomRight ) {
		setValues( bottomLeft, topLeft, bottomRight );
	}
	
	
	public Quad() {
	}
	
	
	public final void setValues( Point3f bottomLeft, Point3f topLeft, Point3f bottomRight ) {
		top.set( topLeft.sub( bottomLeft ) );
		fp_height = top.getModule();
		top.normalize();

		right.set( bottomRight.sub(bottomLeft ) );
		fp_width = right.getModule();
		right.normalize();

		normal.set( right.cross( top ) );
		normal.normalize();

		position.set( topLeft );
		final int FP_2 = NanoMath.toFixed( 2 );
		center.set( topLeft.add( right.mul( NanoMath.divFixed( fp_width, FP_2 ) ) ).sub( top.mul( NanoMath.divFixed( fp_height, FP_2 ) ) ) );

		calculateHalfDiagonal();

		// para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) é um ponto qualquer pertencente a esse plano.
		fp_d = ( normal.mul( -1 ) ).dot( position );
	}
	
	
	public final void setValues( Point3f pos, byte anchor, Point3f t, Point3f r, int fp_width, int fp_height  ) {
		top.set( t );
		right.set( r );
		this.fp_width = fp_width;
		this.fp_height = fp_height;

		top.normalize();
		right.normalize();
		normal.set( right.cross( top ) );
		normal.normalize();

		calculateHalfDiagonal();

		setPosByAnchor( pos, anchor  );

		// para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) é um ponto qualquer pertencente a esse plano.
		fp_d = ( normal.mul( -1 ) ).dot( position );
	}
	
	
	/**
	 * Retorna a distância do ponto ao plano do quad em notação de ponto fixo.
	 * http://www-math.mit.edu/~djk/18_022/chapter02/example01.html
	 * 
	 * @param point
	 * @return
	 */
	public final int distanceTo( Point3f point ) {
		return normal.dot( point.sub( center ) );
	}
	
	
	/**
	 * Indica se o ponto está dentro dos limites de altura/largura do quad (não testa se o ponto pertence ao plano).
	 * 
	 * @param point 
	 * @param tolerance 
	 * @return 
	 */
	public final boolean isInsideQuad( Point3f point, int tolerance ) {
		if ( tolerance < FP_MIN_TOLERANCE )
			tolerance = FP_MIN_TOLERANCE;

		// primeiro teste: se ponto estiver numa distância em relação ao centro do quad maior que a
		// metade da diagonal do mesmo, não pode estar colidindo
		final Point3f distance = point.sub( center );
//		final int teste1 = distance.getModule();
		if ( distance.getModule() > ( fp_halfDiagonal + tolerance ) )
			return false;

		// colide com a esfera ao redor do centro do Quad; agora verifica-se se o ponto colide com o
		// Quad em si. Para isso, projeta-se o ponto nos vetores top e right do Quad. Caso o módulo
		// das projeções sejam menores que a altura e largura do Quad, há colisão. Como os módulos
		// de top e right são, respectivamente, height e width, não há necessidade de calcular seus
		// módulos. TODO testando: vetores TOP e RIGHT são unitários, logo o módulo é 1
		// cálculo da projeção de um vetor sobre outro: proj(B,A) = ( (A.B)/|A|² ) * A
		final int fp_height_half = NanoMath.mulFixed( fp_height, NanoMath.HALF );
//		final Point3f projTop = top.mul( NanoMath.divFixed( top.dot( distance ), NanoMath.mulFixed( fp_height_half, fp_height_half ) ) );
		final Point3f projTop = top.mul( top.dot( distance ) );

//		final int t = top.dot( distance );
//		final Point3f t2 = top.mul( t );
//		final int teste = projTop.getModule();
		if ( projTop.getModule() < fp_height_half + tolerance ) {
			final int fp_width_half = NanoMath.mulFixed( fp_width, NanoMath.HALF );
//			final Point3f projRight = right.mul( NanoMath.divFixed( right.dot( distance ), NanoMath.mulFixed( fp_width_half, fp_width_half ) ) );
			final Point3f projRight = right.mul( right.dot( distance ) );

//			final int teste2 = projRight.getModule();
			if ( projRight.getModule() < fp_width_half + tolerance )
				return true;
		}

		return false;		
	}
	

	private final void setPosByAnchor( Point3f pos, byte anchor  ) {
		final Point3f r = right.mul( fp_width );
		final Point3f t = top.mul( fp_height );
		
		switch ( anchor ) {
			case ANCHOR_TOP_LEFT:
				center.set( pos.add( r.mul( NanoMath.HALF ) ).sub( t.mul( NanoMath.HALF ) ) );
				position.set( pos );
			break;
		
			case ANCHOR_TOP:
				center.set( pos.sub( t.mul( NanoMath.HALF ) ) );
				position.set( pos.sub( r.mul( NanoMath.HALF ) ) );
			break;
			
			case ANCHOR_TOP_RIGHT:
				center.set( pos.sub( r.mul( NanoMath.HALF ) ).sub( t.mul( NanoMath.HALF ) ) );
				position.set( pos.sub( r ) );
			break;
			
			case ANCHOR_RIGHT:
				center.set( pos.sub( r.mul( NanoMath.HALF ) ) );
				position.set( pos.sub( r ).add( t.mul( NanoMath.HALF ) ) );
			break;
			
			case ANCHOR_BOTTOM_RIGHT:
				center.set( pos.sub( r.mul( NanoMath.HALF ) ).add( t.mul( NanoMath.HALF ) ) );
				position.set( pos.sub( r ).add( t ) );
			break;
			
			case ANCHOR_BOTTOM:
				center.set( pos.add( t.mul( NanoMath.HALF ) ) );
				position.set( pos.sub( r.mul( NanoMath.HALF ) ).add( t ) );
			break;
			
			case ANCHOR_BOTTOM_LEFT:
				center.set( pos.add( r.mul( NanoMath.HALF ) ).add( t.mul( NanoMath.HALF ) ) );
				position.set( pos.add( t ) );
			break;
			
			case ANCHOR_LEFT:
				center.set( pos.add( r.mul( NanoMath.HALF ) ) );
				position.set( pos.add( t.mul( NanoMath.HALF ) ) );
			break;
		
			case ANCHOR_CENTER:
				center.set( pos );
				position.set( pos.sub( r.mul( NanoMath.HALF ) ).add( t.mul( NanoMath.HALF ) ) );
			break;
		}		
	}
	
	
	private final void calculateHalfDiagonal() {
		fp_halfDiagonal = NanoMath.mulFixed( NanoMath.sqrtFixed( NanoMath.mulFixed( fp_width, fp_width ) + NanoMath.mulFixed( fp_height, fp_height ) ), NanoMath.HALF );		
	}
	
}
