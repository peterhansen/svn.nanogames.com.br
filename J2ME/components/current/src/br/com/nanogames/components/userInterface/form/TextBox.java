/**
 * TextBox.java
 * 
 * Created on 7/Nov/2008, 15:54:30
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import java.util.Hashtable;
import java.util.Vector;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//#endif

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public class TextBox extends Component {
	
	/** Sempre considera a tecla # (jogo-da-velha) como tecla de troca de entrada de texto */
	public static final char KEY_INPUT_CHANGE = ScreenManager.KEY_POUND;

	// modos de entrada de texto
	/** Modo de entrada de texto: todos os caracteres. */
	public static final byte INPUT_MODE_ANY			= 0;

	/** Modo de entrada de texto: somente números. */
	public static final byte INPUT_MODE_NUMBERS		= 1;

	/** Modo de entrada de texto: e-mail. */
	public static final byte INPUT_MODE_EMAIL		= 2;

	/** Modo de entrada de texto: password. */
	public static final byte INPUT_MODE_PASSWORD	= 3;

	/** Modo de entrada de texto: URL. */
	public static final byte INPUT_MODE_URL			= 4;	
	
	/** Tipo de caixa de texto: caracteres minúsculos. */
	public static final byte CASE_TYPE_LOWER = 0;

	/** Tipo de caixa de texto: caracteres maiúsculos. */
	public static final byte CASE_TYPE_UPPER = 1;

	/** Tipo de caixa de texto: caracteres iniciais de cada palavra maiúsculos. */
	public static final byte CASE_TYPE_CAPITALIZE_WORDS = 2;

	/** Tipo de caixa de texto: somente primeiro caracter de cada frase é maiúsculo. */
	public static final byte CASE_TYPE_CAPITALIZE_SENTENCES = 3;

	/** Quantidade total de caixas de texto diferentes. */
	protected static final byte CASE_TYPES_TOTAL = CASE_TYPE_CAPITALIZE_SENTENCES + 1;	
	
	/** Tempo em milisegundos que uma tecla deve ser mantida pressionada para que seja utilizada a ação de tecla segurada. */
	protected static final short TIME_KEY_PRESS = 400;

	/** Tempo em milisegundos após uma tecla ser pressionada para que o caracter atual seja inserido no texto. */
	protected static final short TIME_KEY_WAIT_BEFORE_INSERT = 800;

	/** Tempo em milisegundos que a tecla clear deve ser mantida pressionada para apagar todo o texto anterior à posição do cursor. */
	protected static final short TIME_KEY_CLEAR_ALL = 1000; // TODO apagar aos poucos?

	/** Intervalo em milisegundos entre 2 repetições de tecla esquerda/direita sendo mantidas pressionadas. */
	protected static final short TIME_NAVIGATE_KEY_REPEAT = 180;
	
	/** Tempo padrão em milisegundos que o cursor permanece visível. */
	public static final short TIME_DEFAULT_CURSOR_VISIBLE = 500;

	/** Tempo padrão em milisegundos que o cursor permanece invisível. */
	public static final short TIME_DEFAULT_CURSOR_INVISIBLE = 250;
	
	/** Tipo de máscara para password: caracter (somente INPUT_MODE_PASSWORD). */
	protected static final byte PASSWORD_MASK_TYPE_CHAR		= 0;
	
	/** Tipo de máscara para password: drawable (somente INPUT_MODE_PASSWORD). */
	protected static final byte PASSWORD_MASK_TYPE_DRAWABLE	= 1;
	
	/** Caracter padrão utilizado como máscara para password. */
	public static final char PASSWORD_CHAR_MASK_DEFAULT = '*';
	
	/** Referência para o EditLabel. */
	protected final EditLabel label;
	
	
	public TextBox( ImageFont font, String initialText, int maxChars, byte inputMode ) throws Exception {
		this( font, initialText, maxChars, inputMode, false );
	}
	
	
	public TextBox( ImageFont font, String initialText, int maxChars, byte inputMode, boolean useBorderAndFill ) throws Exception {
		super( 3 ); // TODO verificar tamanho do grupo
		
		label = new EditLabel( this, font, initialText, maxChars, inputMode, useBorderAndFill );
		insertDrawable( label );
		setKeyListener( label );
		//#if TOUCH == "true"
			setPointerListener( label );
		//#endif
		
		setPreferredSize( label.getSize() );
		
		setSize( label.getWidth(), label.getHeight(), false );
		setInputMode( inputMode );
	}


	public Point calcPreferredSize( Point maximumSize ) {
		final Point borderSize = border == null ? new Point() : border.getBorderSize();
		
		int charWidth = label.getFont().getCharWidth( 'm' );
		if ( charWidth == 0 )
			charWidth = label.getFont().getCharWidth( '0' );
		if ( charWidth == 0 )
			charWidth = label.getFont().getCharWidth( 'A' );
		
		if ( maximumSize == null ) {
			return new Point( borderSize.x + charWidth * getMaxChars(),
							  borderSize.y + label.getFont().getHeight() );
		} else {
			return new Point( borderSize.x + Math.min( charWidth * getMaxChars(), maximumSize.x ),
							  borderSize.y + Math.min( label.getFont().getHeight(), maximumSize.y ) );
		}
//		return super.calcPreferredSize( maximumSize );
	}
	
	
	public final int getMaxChars() {
		return label.getMaxChars();
	}


	public void setFocus( boolean focus ) {
		super.setFocus( focus );

		if ( border != null )
			border.setState( focus ? Border.STATE_FOCUSED : Border.STATE_UNFOCUSED );

		if ( focus ) {
			setInputMode( getInputMode() );
			setCaseType( getCaseType() );
		}
		
		// faz o & para que o textBox possa ter foco, mas não possa ser editado caso não esteja ativo
		focus &= enabled;
		label.setActive( focus );
		setHandlesInput( focus );
	}


	protected final void updateTextX() {
		label.updateTextX();
	}


	public void setPasswordMask( char mask ) {
		label.setPasswordMask( mask );
	}


	public void setPasswordMask( Drawable mask ) {
		label.setPasswordMask( mask );
	}


	public void setMaxChars( int maxChars ) {
		label.setMaxChars( maxChars );
	}


	public void setInputMode( byte inputMode ) {
		label.setInputMode( inputMode );
		dispatchActionEvent( new Event( this, Event.EVT_TEXTBOX_INPUT_MODE_CHANGED, new Byte( label.getInputMode() ) ) );
	}


	public final void setCircular( boolean circular ) {
		label.setCircular( circular );
	}


	public final void setCaseType( int caseType ) {
		label.setCaseType( caseType );
		dispatchActionEvent( new Event( this, Event.EVT_TEXTBOX_CASE_TYPE_CHANGED, new Byte( label.getCaseType() ) ) );
	}


	public void setCaretPosition( int position ) {
		label.setCaretPosition( position );
	}


	public void setCaretBlinkRate( int caretTimeVisible, int caretTimeInvisible ) {
		label.setCaretBlinkRate( caretTimeVisible, caretTimeInvisible );
	}


	public void setCaret( Drawable caret ) {
		label.setCaret( caret );
	}


	public final void setActive( boolean active ) {
		label.setActive( active );
	}
	
	
	public String getUIID() {
		return "textbox";
	}


	public void insertChar( char c ) {
		label.insertChar( c );
	}


	public final void backspace() {
		backSpace();
	}
	
	
	protected void setSize( int width, int height, boolean setLabelSize ) {
		super.setSize( width, height );
		
		if ( setLabelSize )
			label.setSize( getLayoutWidth(), getLayoutHeight() );
	}
	
	
	public void setSize( int width, int height ) {
		setSize( width, height, true );
	}


	protected void nextChar() {
		label.nextChar();
	}


	public final boolean isCircular() {
		return label.isCircular();
	}


	public final byte getCaseType() {
		return label.getCaseType();
	}


	public final byte getInputMode() {
		return label.getInputMode();
	}


	public Drawable getCaret() {
		return label.getCaret();
	}


	public void backSpace() { // TODO wtf?
		label.backSpace();
	}


	public void setText( String text, boolean setSize ) {
		label.setText( text, setSize );
	}


	public final void setText( String text ) {
		label.setText( text );
	}


	public void setFont( ImageFont font ) throws NullPointerException {
		label.setFont( font );
	}


	public String getText() {
		return label.getText();
	}


	public ImageFont getFont() {
		return label.getFont();
	}


	public final char[] getCharBuffer() {
		return label.getCharBuffer();
	}


	public String getCaseTypeString() {
		switch ( label.getInputMode() ) {
			case INPUT_MODE_NUMBERS:
				return "123";

			default:
				switch ( label.getCaseType() ) {
					case CASE_TYPE_LOWER:
						return "abc";

					case CASE_TYPE_UPPER:
						return "ABC";

					case CASE_TYPE_CAPITALIZE_SENTENCES:
						return "Abc abc";

					case CASE_TYPE_CAPITALIZE_WORDS:
						return "Abc Abc";

					default:
						return "abc";
				}
		}
	}


	/*******************************************************************************************************************
	 * Classe interna EditLabel, que contém a lógica de tratamento de entrada de texto.							       *
	 *******************************************************************************************************************/
	protected static class EditLabel extends Label implements Updatable, KeyListener 
			//#if TOUCH == "true"
				, PointerListener
			//#endif

	{

		/** Hashtable que associa uma tecla a um array de caracteres. */
		protected final Hashtable characters = new Hashtable();

		/** Referência para o char[] da tecla atual, para evitar acessos desnecessários à HashTable. */
		protected char[] currentChars;

		/** Código da última tecla pressionada. */
		protected int lastKeyPressed;

		/** Código da tecla sendo mantida pressionada (caso haja uma). */
		protected int keyHeld;

		/** Indica se há alguma alteração pendente. */
		protected boolean pendingCommit;

		/** Tempo decorrido em milisegundos desde a última tecla pressionada (ela pode ter sido solta ou não durante esse período). */
		protected short timeSinceLastKeyPress;

		/** Índice do caracter atual do array utilizado. */
		protected byte currentCharIndex;

		/** Modo de entrada atual. */
		protected byte inputMode = INPUT_MODE_ANY;

		/** Tipo de caixa de texto atual. Valores válidos: 
		 * <ul>
		 * <li>CASE_TYPE_LOWER</li>
		 * <li>CASE_TYPE_UPPER</li>
		 * <li>CASE_TYPE_CAPITALIZE_WORDS</li>
		 * </ul>
		 */
		protected byte caseType;

		/** Posição do cursor. */
		protected short caretPosition;

		/** Indica se a navegação do cursor pelo texto é circular. */
		protected boolean circular;

		/** Drawable utilizado como cursor. */
		protected Drawable caret;

		/** Tempo em milisegundos que o cursor permanece visível. */
		protected short caretTimeVisible = TIME_DEFAULT_CURSOR_VISIBLE;

		/** Tempo em milisegundos que o cursor permanece invisível. */
		protected short caretTimeInvisible = TIME_DEFAULT_CURSOR_INVISIBLE;	

		/** Tempo acumulado da animação de "pisca-pisca" do cursor. */
		protected short accTime;

		/** Contador de caracteres. */
		protected short charCount;

		protected byte passwordMaskType = PASSWORD_MASK_TYPE_CHAR;

		/** Caracter utilizado como máscara para password. */
		protected char passwordCharMask = PASSWORD_CHAR_MASK_DEFAULT;

		/** Drawable utilizado como máscara para password. */
		protected Drawable passwordDrawableMask;

		/** Indica se o label está ativo, ou seja, pode ser editado (quando não está ativo, não ocorre também a animação do cursor). */
		protected boolean active;

		/** Identificador do label. */
		protected short id;

		/** Posição x inicial do texto (utilizado para garantir que o cursor sempre estará visível). */
		protected short textX;

		/***/
		protected final Pattern fill;

		/***/
		protected int fillColorSelected = 0xffffff;

		/***/
		protected int fillColorUnselected = 0xd0dfee;	
		
		/***/
		protected final TextBox textBox;


		/**
		 * Cria um label editável.
		 * @param font fonte utilizada para desenhar o texto.
		 * @param initialText texto inicial (pode ser vazio).
		 * @param maxChars número máximo de caracteres.
		 * @param inputMode modo de entrada dos caracteres. Valores válidos:
		 * <ul>
		 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
		 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
		 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endereços de e-mail.</li>
		 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, porém não são exibidos. A máscara que substitui
		 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padrão, é um caracter '*' (asterisco)</li>
		 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endereços de internet.</li>
		 * </ul>
		 * @throws java.lang.Exception caso a fonte seja nula.
		 */
		public EditLabel( TextBox textBox, ImageFont font, String initialText, int maxChars, byte inputMode ) throws Exception {
			this( textBox, font, initialText, maxChars, inputMode, false );
		}


		/**
		 * Cria um label editável.
		 * @param font fonte utilizada para desenhar o texto.
		 * @param initialText texto inicial (pode ser vazio).
		 * @param maxChars número máximo de caracteres.
		 * @param inputMode modo de entrada dos caracteres. Valores válidos:
		 * <ul>
		 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
		 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
		 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endereços de e-mail.</li>
		 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, porém não são exibidos. A máscara que substitui
		 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padrão, é um caracter '*' (asterisco)</li>
		 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endereços de internet.</li>
		 * </ul>
		 * @throws java.lang.Exception caso a fonte seja nula.
		 */
		public EditLabel( TextBox textBox, ImageFont font, String initialText, int maxChars, byte inputMode, boolean useBorderAndFill ) throws Exception {
			super( font, initialText );
			
			//  TODO tirar borda da classe EditLabel
			
			this.textBox = textBox;
			setMaxChars( maxChars );

			final char[][] charTable = {
					{ ' ', '0', '\n', '+', '-', '=', '%', ':', ';', '$', '*', '#' },	// NUM0
					{ '.', ',', '?', '!', '1', '@', '_', '/', '\'', '\"' },				// NUM1
					{ 'a', 'b', 'c', '2', 'á', 'ã', 'à', 'ç' },							// NUM2
					{ 'd', 'e', 'f', '3', 'é', 'ê' },									// NUM3
					{ 'g', 'h', 'i', '4', 'í' },										// NUM4
					{ 'j', 'k', 'l', '5' },												// NUM5
					{ 'm', 'n', 'o', '6', 'ñ' },										// NUM6
					{ 'p', 'q', 'r', 's', '7' },										// NUM7
					{ 't', 'u', 'v', '8', 'ú' },										// NUM8
					{ 'w', 'x', 'y', 'z', '9' },										// NUM9
					{ ' ', '(', ')', '[', ']', '{', '}', '<', '>', '\\' },				// asterisco (*)
				};		

			Vector temp = new Vector();
			for ( int i = 0; i < charTable.length; ++i ) {
				final char[] iChars = charTable[ i ];
				for ( int j = 0; j < iChars.length; ++j ) {
					final char c = iChars[ j ];
					if ( font.getCharWidth( c ) > 0 ) {
						// existe o caracter na fonte; o adiciona ao vector temporário.
						temp.addElement( new Character( c ) );
					}
				}

				final char[] chars = new char[ temp.size() ];
				for ( int j = 0; j < chars.length; ++j )
					chars[ j ] = ( ( Character ) temp.elementAt( j ) ).charValue();
				// associa o código das teclas numéricas 0-9 ou * à sequência de caracteres detectada.
				final int KEY = i < charTable.length - 1 ? ( ScreenManager.KEY_NUM0 + i ) : ScreenManager.KEY_STAR;
				characters.put( new Integer( KEY ), chars );
				temp.removeAllElements();
			}

			if ( useBorderAndFill ) {
				fill = new Pattern( fillColorUnselected );

				setSize( size.x, size.y + 2 );
			} else {
				fill = null;
			}


			if ( initialText != null && initialText.length() > 0 ) {
				charCount = (short) initialText.length();
				setCaretPosition( charCount );
			}
		} // fim do construtor EditLabel( ImageFont, String, int, byte )
		
		
		/**
		 * 
		 * @return
		 */
		public String getText() {
			return new String( charBuffer, 0, charCount );
		}


		/**
		 * 
		 * @param caret 
		 */
		public void setCaret( Drawable caret ) {
			this.caret = caret;
			updateTextX();
		}


		/**
		 * 
		 * @return 
		 */
		public Drawable getCaret() {
			return caret;
		}


		/**
		 *@see Updatable#update(int)
		 */
		public void update( int delta ) {
			if ( active ) {
				timeSinceLastKeyPress += delta;

				switch ( keyHeld ) {
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						if ( pendingCommit && timeSinceLastKeyPress >= TIME_KEY_PRESS ) {
							// segurou uma tecla de 0 a 9 pelo tempo mínimo - troca o caracter atual pelo número correspondente à tecla
							setCurrentChar( ( char ) ( '0' + lastKeyPressed - ScreenManager.KEY_NUM0 ) );

							// não avança diretamente para o próximo caracter, para que o usuário ainda possa escolher outro caracter
							// e também para que, no caso de uso de máscaras (password, por exemplo), o usuário possa ver o número
							keyHeld = 0;
							timeSinceLastKeyPress = 0;
						}
					break;

					case ScreenManager.LEFT:
					case ScreenManager.RIGHT:
						// navega automaticamente com o cursor ao segurar teclas esquerda/direita
						if ( timeSinceLastKeyPress >= TIME_NAVIGATE_KEY_REPEAT ) {
							keyPressed( keyHeld );
						}
					break;

					case ScreenManager.KEY_CLEAR:
						if ( timeSinceLastKeyPress >= TIME_KEY_CLEAR_ALL ) {
							while ( caretPosition > 0 )
								backSpace();
						}
					break;

					case 0:
						// não há tecla pressionada
						if ( pendingCommit && timeSinceLastKeyPress >= TIME_KEY_WAIT_BEFORE_INSERT ) {
							commitChange( true );
						}			
					break;
				} // fim switch ( keyHeld )

				if ( caret != null && caretTimeInvisible > 0 ) {
					accTime += delta;

					if ( caret.isVisible() ) {
						if ( accTime >= caretTimeVisible ) {
							accTime -= caretTimeVisible;
							caret.setVisible( false );
						}
					} else {
						if ( accTime >= caretTimeInvisible ) {
							accTime -= caretTimeInvisible;
							caret.setVisible( true );
						}
					} // fim else ==> !caret.isVisible()
				} // fim if ( caret != null )
			} // fim if ( active )
		} // fim do método update( int )


		private final void commitChange( boolean clearLastKeyPressed ) {
			commitChange( clearLastKeyPressed, isActive() );
		}


		/**
		 * Realiza a alteração pendente, caso haja uma.
		 */
		private final void commitChange( boolean clearLastKeyPressed, boolean dispatchEvent ) {
			pendingCommit = false;
			currentCharIndex = 0;

			if ( clearLastKeyPressed )
				lastKeyPressed = 0;

			if ( dispatchEvent && charBuffer != null && caretPosition >= charBuffer.length ) {
				switch ( inputMode ) {
					case INPUT_MODE_NUMBERS:
					break;

					default:
						textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_FILLED ) );
				}
			}
		}


		protected void insertChar( char c ) {
			if ( charCount < charBuffer.length ) {
				// faz o "chega-pra-lá" no array, para substituir eventuais lixos deixados anteriormente
				for ( short i = charCount; i > caretPosition; --i )
					charBuffer[ i ] = charBuffer[ i - 1 ];

				setCurrentChar( c );
				++charCount;
				++caretPosition;

				updateTextX();

				if ( isActive() && !pendingCommit && charBuffer != null ) {
					if ( caretPosition >= charBuffer.length )
						textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_FILLED ) );
					else
						textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_CHAR_COMMITED ) );
				}
			}
		}


		public void setText( String text, boolean setSize ) {
			if ( text == null ) {
				text = "";
				charCount = 0;
			} else {
				charCount = ( short ) text.length();
			}
			
			// copia os caracteres para o buffer, caso já tenha sido inicializado
			if ( charBuffer != null )
				text.getChars( 0, Math.min( charCount, charBuffer.length ), charBuffer, 0 );

			if ( setSize )
				setSize( font.getTextWidth( text ), font.getHeight() );		
			
			setCaretPosition( charCount );			
		}
		
		
		/**
		 * 
		 * @return
		 */
		public final int getMaxChars() {
			return charBuffer.length;
		}
		
		
		/**
		 * Apaga o caracter anterior à posição do cursor.
		 */
		public void backSpace() {
			if ( charCount > 0 ) {
				if ( caretPosition > 0 ) {
					commitChange( false, false );

					for ( int i = caretPosition - 1; i < charCount - 1; ++i ) {
						charBuffer[ i ] = charBuffer[ i + 1 ];
					}

					--charCount;
					--caretPosition;
					updateTextX();

					textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_CHAR_COMMITED ) );
				}
			} else {
				// clear pressionado com textBox vazio
				textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_BACK ) );
			}
		}


		/**
		 * Define a velocidade do "pisca-pisca" do cursor.
		 * 
		 * @param caretTimeVisible tempo em milisegundos que o cursor permanece visível.
		 * @param caretTimeInvisible tempo em milisegundos que o cursor permanece invisível. Valores menores ou iguais a
		 * zero significam que o cursor estará sempre visível.
		 */
		public void setCaretBlinkRate( int caretTimeVisible, int caretTimeInvisible ) {
			this.caretTimeVisible = ( short ) caretTimeVisible;

			if ( caretTimeInvisible <= 0 && caret != null )
				caret.setVisible( true );

			this.caretTimeInvisible = ( short ) caretTimeInvisible;
		} // fim do método setCaretBlinkRate( int, int )


		/**
		 * 
		 * @see userInterface.KeyListener#keyReleased(int)
		 */
		public void keyReleased( int key ) {
			if ( active ) {
				if ( key != lastKeyPressed ) {
					switch ( key ) {
						case ScreenManager.KEY_NUM0:
						case ScreenManager.KEY_NUM1:
						case ScreenManager.KEY_NUM2:
						case ScreenManager.KEY_NUM3:
						case ScreenManager.KEY_NUM4:
						case ScreenManager.KEY_NUM5:
						case ScreenManager.KEY_NUM6:
						case ScreenManager.KEY_NUM7:
						case ScreenManager.KEY_NUM8:
						case ScreenManager.KEY_NUM9:
						case ScreenManager.KEY_STAR:
							switch ( inputMode ) {
								case INPUT_MODE_NUMBERS:
								break;

								default:
									commitChange( true );
								break;
							}
						break;
					} // fim switch ( key )
				}

				keyHeld = 0;
			}
		} // fim do método keyReleased( int )


		/**
		 * 
		 * 
		 * @see userInterface.KeyListener#keyPressed(int)
		 */
		public void keyPressed( int key ) {
			if ( active ) {
				// não permite 2 teclas pressionadas simultaneamente
				if ( keyHeld != 0 )
					keyReleased( keyHeld );

				final int previousKeyPressed = lastKeyPressed;

				keyHeld = key;
				lastKeyPressed = key;
				timeSinceLastKeyPress = 0;

				switch ( key ) {
					case ScreenManager.RIGHT:
						if ( caretPosition >= charCount || ( !setCaretPosition( caretPosition + 1 ) && isActive() ) )
							textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_RIGHT_ON_END ) );
					break;

					case ScreenManager.LEFT:
						if ( caretPosition <= 0 || ( !setCaretPosition( caretPosition - 1 ) && isActive() ) )
							textBox.dispatchActionEvent( new Event( textBox, Event.EVT_TEXTBOX_LEFT_ON_START ) );
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						// como não é possível detectar exatamente se o aparelho possui tecla específica para CLEAR, sempre utiliza também
						// a soft key direita com essa função.
						keyPressed( ScreenManager.KEY_CLEAR );
					break;

					case ScreenManager.KEY_CLEAR:
						backSpace();
					break;

					case ScreenManager.KEY_POUND:
						// avisa o textBox antes para que possa ser disparado o evento indicando mudança do modo de entrada de texto
						textBox.setCaseType( ( caseType + 1 ) % CASE_TYPES_TOTAL );
					break;

					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
					case ScreenManager.KEY_STAR:
						switch ( inputMode ) {
							case INPUT_MODE_NUMBERS:
								// apenas insere o número correspondente
								insertChar( ( char ) ( '0' + lastKeyPressed - ScreenManager.KEY_NUM0 ) );
								commitChange( true );
							break;

							default:
								currentChars = ( char[] ) characters.get( new Integer( key ) );

								switch ( previousKeyPressed ) {
									case ScreenManager.KEY_NUM0:
									case ScreenManager.KEY_NUM1:
									case ScreenManager.KEY_NUM2:
									case ScreenManager.KEY_NUM3:
									case ScreenManager.KEY_NUM4:
									case ScreenManager.KEY_NUM5:
									case ScreenManager.KEY_NUM6:
									case ScreenManager.KEY_NUM7:
									case ScreenManager.KEY_NUM8:
									case ScreenManager.KEY_NUM9:
									case ScreenManager.KEY_STAR:
										if ( key != previousKeyPressed ) {
											if ( pendingCommit ) {
												commitChange( false );
												insertChar( currentChars[ 0 ] );
												currentCharIndex = 0;
												pendingCommit = true;
											}
										} else {
											nextChar();
											pendingCommit = true;
										}
									break;

									default:
										// primeira tecla pressionada - reinicia o contador do array de caracteres atual
										insertChar( currentChars[ 0 ] );
										currentCharIndex = 0;
										pendingCommit = true;
									// fim default
								} // fim switch ( previousKeyPressed )
							// fim default
						}
					break;
					
					case ScreenManager.UP:
					case ScreenManager.DOWN:
						// apenas faz o TextBox parar de tratar entradas de tecla momentaneamente - automaticamente
						// o Form irá fazer o tratamento adequado
						textBox.setHandlesInput( false );
					break;

					default:
//						switch ( ImageFont.getCharType( ( char ) key ) ) {
//							case ImageFont.CHAR_TYPE_ACCENT_LOWER_CASE:
//							case ImageFont.CHAR_TYPE_ACCENT_UPPER_CASE:
//							case ImageFont.CHAR_TYPE_NUMERIC:
//							case ImageFont.CHAR_TYPE_REGULAR:
								if ( font.getCharWidth( ( char ) key ) > 0 ) {
									textBox.insertChar( ( char ) key );
									commitChange( true );
								} else {
									textBox.dispatchActionEvent( new Event( textBox, Event.EVT_KEY_PRESSED, new Integer( key ) ) );
								}
//							break;
//						}
				} // fim switch ( key )		
			} else {
				// caso não esteja ativo, envia de volta o evento para ser tratado
				textBox.dispatchActionEvent( new Event( textBox, Event.EVT_KEY_PRESSED, new Integer( key ) ) );
			}
		} // fim do método keyPressed( int )


		/**
		 * Define a quantidade máxima de caracteres do label.
		 * @param maxChars número máximo de caracteres do label. Qualquer valor igual ou maior que zero é válido; valores 
		 * negativos são ignorados.
		 */
		public void setMaxChars( int maxChars ) {
			if ( maxChars > 0 ) {
				try {
					final char[] oldCharBuffer = charBuffer;
					charBuffer = new char[ maxChars ];

					charCount = ( short ) Math.min( charCount, maxChars );
					if ( caretPosition > charCount )
						setCaretPosition( caretPosition );

					if ( oldCharBuffer != null )
						System.arraycopy( oldCharBuffer, 0, charBuffer, 0, Math.min( oldCharBuffer.length, maxChars ) );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
		//# 			AppMIDlet.log( e , "12");
					//#endif
				}
			}
		} // fim do método setMaxChars( int )


		/**
		 * Posiciona o cursor no texto.
		 * 
		 * @param position índice do cursor no texto.
		 * @return boolean indicando se houve mudança na posição do cursor.
		 */
		protected boolean setCaretPosition( int position ) {
			final short previousPosition = caretPosition;
			
			if ( circular )
				position = ( position + charCount + 1 ) % ( charCount + 1 );

			if ( pendingCommit )
				commitChange( true );
			caretPosition = ( short ) NanoMath.clamp( position, 0, charCount );
			updateTextX();

			return caretPosition != previousPosition;
		} // fim setCaretPosition( int )


		/**
		 * Define o modo de entrada de caracteres no label.
		 * 
		 * @param inputMode modo de entrada dos caracteres. Valores válidos:
		 * <ul>
		 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
		 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
		 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endereços de e-mail.</li>
		 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, porém não são exibidos. A máscara que substitui
		 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padrão, é um caracter '*' (asterisco)</li>
		 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endereços de internet.</li>
		 * </ul>
		 */
		public void setInputMode( byte inputMode ) {
			switch ( inputMode ) {
				case INPUT_MODE_ANY:
				case INPUT_MODE_NUMBERS:
				case INPUT_MODE_EMAIL:
				case INPUT_MODE_PASSWORD:
				case INPUT_MODE_URL:
					this.inputMode = inputMode;
				break;
			} // fim switch ( inputMode )
		} // fim do método setInputMode( byte )


		public final byte getInputMode() {
			return inputMode;
		}


		public void setPasswordMask( Drawable mask ) {
			passwordDrawableMask = mask;

			if ( mask != null )
				passwordMaskType = PASSWORD_MASK_TYPE_DRAWABLE;
		}


		public void setPasswordMask( char mask ) {
			passwordMaskType = PASSWORD_MASK_TYPE_CHAR;
			passwordCharMask = mask;
		}


		public final void setActive( boolean active ) {
			this.active = active;

			if ( fill != null ) {
				if ( active ) {
					fill.setFillColor( fillColorSelected );
				} else {
					fill.setFillColor( fillColorUnselected );
				}
			}

			if ( !active )
				commitChange( true );
		}


		public final boolean isActive() {
			return active;
		}


		public void setSize( int width, int height ) {
			super.setSize( width, height );

			if ( fill != null ) {
//				fill.setSize( width - 2, height - 2 );
				fill.setSize( size );
			}

			updateTextX();
		}		


		protected void paint( Graphics g ) {
			int x = translate.x + textX;
			final int y = 0;
			int index = 0;

			if ( fill != null ) {
//				++x;
				fill.draw( g );
			}

			//#if J2SE == "false"
				final int endX = translate.x + g.getClipWidth();
			//#else
//# 				final int endX = translate.x + g.getClipBounds().width;
			//#endif

			// desenha o texto antes do cursor
			switch ( inputMode ) {
				case INPUT_MODE_PASSWORD:
					final int maxIndex = Math.min( caretPosition, charBuffer.length ) - ( pendingCommit ? 1 : 0 );

					for ( ; index < maxIndex && x < endX; ++index ) {
						switch ( passwordMaskType ) {
							case PASSWORD_MASK_TYPE_CHAR:
								drawChar( g, passwordCharMask, x, translate.y + y );
								x += font.getCharWidth( passwordCharMask );
							break;

							case PASSWORD_MASK_TYPE_DRAWABLE:
								passwordDrawableMask.setPosition( x, y );
								passwordDrawableMask.draw( g );
								x += font.getCharWidth( charBuffer[ index ] );
							break;
						}
					}

					// no caso de haver alteração a ser confirmada, exibe o caracter sendo escolhido sem a máscara
					if ( !pendingCommit )
						break;

				default:
					for ( ; index < caretPosition && index < charBuffer.length && x < endX; ++index ) {
						drawChar( g, charBuffer[ index ], x, translate.y + y );

						x += font.getCharWidth( charBuffer[ index ] );
					}
				// fim default
			} // fim switch ( inputMode )

			// desenha o cursor
			if ( active && caret != null ) {
				caret.setPosition( x - translate.x, ( getHeight() - caret.getHeight() ) >> 1 );

				caret.draw( g );

				x += caret.getWidth();
			}


			// desenha o texto após o cursor
			switch ( inputMode ) {
				case INPUT_MODE_PASSWORD:
					final int maxIndex = Math.min( charCount, charBuffer.length );

					for ( ; index < maxIndex && x < endX; ++index ) {
						switch ( passwordMaskType ) {
							case PASSWORD_MASK_TYPE_CHAR:
								drawChar( g, passwordCharMask, x, translate.y + y );
								x += font.getCharWidth( passwordCharMask );
							break;

							case PASSWORD_MASK_TYPE_DRAWABLE:
								passwordDrawableMask.setPosition( x, y );
								passwordDrawableMask.draw( g );
								x += font.getCharWidth( charBuffer[ index ] );
							break;
						}
					}
				break;

				default:
					for ( ; index < charCount && index < charBuffer.length && x < endX; ++index ) {
						drawChar( g, charBuffer[ index ], x, translate.y + y );

						x += font.getCharWidth( charBuffer[ index ] );
					}
				// fim default
			} // fim switch ( inputMode )		

		}


		/**
		 * 
		 * @param circular
		 */
		public final void setCircular( boolean circular ) {
			this.circular = circular;
		}


		/**
		 * 
		 * @return
		 */
		public final boolean isCircular() {
			return circular;
		}


		/**
		 * 
		 * @param caseType
		 */
		public final void setCaseType( int caseType ) {
			this.caseType = ( byte ) caseType;
		}


		/**
		 * 
		 * @return
		 */
		public final byte getCaseType() {
			return caseType;
		}


		/**
		 * Troca o caracter atual pelo próximo caracter existente na fonte.
		 */
		protected void nextChar() {
			final byte previousCharIndex = currentCharIndex;
			do {
				currentCharIndex = ( byte ) ( ( currentCharIndex + 1 ) % currentChars.length );
			} while ( font.getCharWidth( currentChars[ currentCharIndex ] ) == 0 && currentCharIndex != previousCharIndex );

			setCurrentChar( currentChars[ currentCharIndex ] );
		}


		/**
		 * Atualiza a posição de início do texto, de acordo com o tamanho do texto, do label e a posição do cursor.
		 */
		protected final void updateTextX() {
			if ( charBuffer != null ) {
				int index = 0;
				int x = caret == null ? 0 : caret.getWidth();
				for ( ; index < caretPosition && index < charBuffer.length; ++index ) {
					x += font.getCharWidth( charBuffer[ index ] );
				}

				// x agora armazena a posição à direita do cursor
				textX = ( short ) Math.min( 0, size.x - x );
			}
		}


		/**
		 * 
		 * @param c
		 */
		private final void setCurrentChar( char c ) {
			final int index = pendingCommit ? Math.max( caretPosition - 1, 0 ) : caretPosition;

			switch ( caseType ) {
				case CASE_TYPE_LOWER:
					charBuffer[ index ] = c;
				break;

				case CASE_TYPE_UPPER:
					charBuffer[ index ] = Character.toUpperCase( c );
				break;

				case CASE_TYPE_CAPITALIZE_WORDS:
				case CASE_TYPE_CAPITALIZE_SENTENCES:
					boolean upperCase = index == 0;
					if ( !upperCase ) {
						switch ( charBuffer[ index - 1 ] ) {
							case ' ':
							case '\t':
							case ',':
							case '-':
							case '_':
							case '\\':
							case '/':
							case ';':
							case '+':
							case '*':
							case '=':
							case '(':
							case ')':
							case ':':
							case '[':
							case ']':
							case '{':
							case '}':
								if ( caseType == CASE_TYPE_CAPITALIZE_SENTENCES )
									break;
							case '\n':
							case '\r':
							case '?':
							case '.':
							case '!':
								upperCase = true;
							break;
						}
					}
					charBuffer[ index ] = upperCase ? Character.toUpperCase( c ) : c;
				break;
			}

			updateTextX();
		}

		//#if TOUCH == "true"
			/**
			 *
			 * @param x
			 * @param y
			 */
			public void onPointerDragged( int x, int y ) {
				onPointerPressed( x, y );
			}


			/**
			 *
			 * @param x
			 * @param y
			 */
			public void onPointerPressed( int x, int y ) {
				// corrige a posição do clique de acordo com a altura e largura da borda do TextBox
				y -= position.y + ( textBox.getBorder() == null ? 0 : textBox.getBorder().getTop() );
				if ( ( y >= 0 && y <= size.y ) ) {
					x -= position.x + ( textBox.getBorder() == null ? 0 : textBox.getBorder().getLeft() );

					if ( x >= 0 && x <= size.x ) {
						int cx = -textX;

						for ( int i = 0; i < caretPosition; ++i ) {
							switch ( inputMode ) {
								case INPUT_MODE_PASSWORD:
									switch ( passwordMaskType ) {
										case PASSWORD_MASK_TYPE_CHAR:
											cx += font.getCharWidth( passwordCharMask );
										break;

										case PASSWORD_MASK_TYPE_DRAWABLE:
											cx += font.getCharWidth( charBuffer[ i ] );
										break;
									}
								break;

								default:
									cx += font.getCharWidth( charBuffer[ i ] );
							}

							if ( cx >= x ) {
								setCaretPosition( i );
								return;
							}
						}


						if ( caret != null )
							cx += caret.getWidth();

						for ( int i = caretPosition; i < charBuffer.length; ++i ) {
							switch ( inputMode ) {
								case INPUT_MODE_PASSWORD:
									switch ( passwordMaskType ) {
										case PASSWORD_MASK_TYPE_CHAR:
											cx += font.getCharWidth( passwordCharMask );
										break;

										case PASSWORD_MASK_TYPE_DRAWABLE:
											cx += font.getCharWidth( charBuffer[ i ] );
										break;
									}
								break;

								default:
									cx += font.getCharWidth( charBuffer[ i ] );
							}

							if ( cx >= x ) {
								setCaretPosition( i );
								return;
							}
						}

						// clicou após o final do texto - posiciona o cursor na última posição disponível
						setCaretPosition( charCount );
					}
				} // fim if ( y >= 0 && y <= size.y )
			} // fim do método onPointerPressed( int, int )


			/**
			 *
			 * @param x
			 * @param y
			 */
			public void onPointerReleased( int x, int y ) {
			}

		//#endif
	}


}
