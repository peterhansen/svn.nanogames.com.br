//#if JAVA_VERSION == "ANDROID"
//# package br.com.nanogames.MIDP;
//# 
//# import android.app.Activity;
//# import android.content.res.Configuration;
//# import android.os.Bundle;
//# import android.view.MotionEvent;
//# import android.view.Window;
//# import android.view.WindowManager;
//# import java.util.logging.Level;
//# import java.util.logging.Logger;
//# import javax.microedition.io.ResourceDictionary;
//# import javax.microedition.lcdui.Display;
//# import javax.microedition.media.Manager;
//# import javax.microedition.midlet.MIDlet;
//# 
//# public class MainActivity extends Activity
//# {
//#     private static MainActivity instance;
//#     private MIDlet midlet;
//#     private static String midletName;
//#     public static final String TAG = "MainActivity";
//#     
//#     @Override
//#     protected void onDestroy() {
//#         super.onDestroy();
//#         midlet.notifyDestroyed();       
//#         Manager.Destroy();
//#     }
//#     
//#     @Override 
//#     public void onConfigurationChanged(Configuration newConfig) { 
//#       super.onConfigurationChanged(newConfig); 
//#     } 
//#     
//#     public int getWidth() {
//#         if ( midlet != null && Display.getDisplay( midlet ).getCurrent() != null && Display.getDisplay( midlet ).getCurrent().getWidth() != 0 )
//#             return Display.getDisplay( midlet ).getCurrent().getWidth();
//#         else 
//#         if ( getWindowManager().getDefaultDisplay().getWidth() != 0 )
//#             return getWindowManager().getDefaultDisplay().getWidth();
//#         else //morra!
//#             return 0;
//#     }
//#     
//#     public int getHeight() {
//#         if ( midlet != null && Display.getDisplay( midlet ).getCurrent() != null && Display.getDisplay( midlet ).getCurrent().getHeight() != 0 )        
//#             return Display.getDisplay( midlet ).getCurrent().getHeight();
//#         else 
//#         if ( getWindowManager().getDefaultDisplay().getHeight() != 0 )
//#             return getWindowManager().getDefaultDisplay().getHeight();
//#         else //morra!
//#             return 0;
//#     }
//#     
//#     public static MainActivity getActivity() {
//#         return instance;
//#     }
//# 
//#     @Override
//#     public boolean onTouchEvent(MotionEvent event) {
//#         return super.onTouchEvent(event);
//#     }
//# 
//#     @Override
//#     protected void onResume() {
//#         super.onResume();
//#         
//#         try {
//#             midlet = (MIDlet) Class.forName( midletName ).newInstance();
//#             midlet.start();
//#            } catch (Exception ex) {           
//#             Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
//#         }        
//#         
//#     }
//# 
//#     
//#     
//#     @Override
//#     protected void onStart() {
//#         super.onStart();
//#     }
//#     
//#     
//#     /** Called when the activity is first created. */
//#     @Override
//#     public void onCreate(Bundle savedInstanceState)
//#     {
//#         instance = this;
//#         super.onCreate(savedInstanceState);
//#         
//#         requestWindowFeature( Window.FEATURE_NO_TITLE );
//#         this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN ); 
//#         
//# //        setContentView( R.layout.main );   
//#         
//#         ResourceDictionary.setGroup( this.getResources() );
//#         loadResources( savedInstanceState );
//#     }
//#     
//#     public static void startMIDlet( String midletName ) {
//#         MainActivity.midletName = midletName;
//#     }
//# 
//#     public void loadResources( Bundle mainBundle ) {
//#         
//#     }
//#     
//# 
//# }
//#endif