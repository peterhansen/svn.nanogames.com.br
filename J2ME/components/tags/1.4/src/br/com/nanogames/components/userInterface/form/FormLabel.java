/**
 * FormLabel.java
 * 
 * Created on 10/Nov/2008, 12:11:07
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;

/**
 *
 * @author Peter
 */
public class FormLabel extends DrawableComponent {

	protected final Label label;
			
	
	public FormLabel( ImageFont font, String text ) throws Exception {
		super( new Label( font, text ) );
		
		label = ( Label ) d;
		
		setFocusable( false );
	}
	
	
	public String getUIID() {
		return "label";
	}


	public final void setText( String text ) {
		setText( text, true );
	}
	
	
	public final void setText( String text, boolean setSize ) {
		label.setText( text, setSize );
		
		if ( setSize )
			resizeToFit();
	}


	public final void setText( int textIndex, boolean setSize ) {
		label.setText( textIndex, setSize );
		
		if ( setSize )
			resizeToFit();
	}


	public final void setText( int textIndex ) {
		setText( textIndex, true );
	}


	public void setFont( ImageFont font ) throws NullPointerException {
		label.setFont( font );
	}


	public String getText() {
		return label.getText();
	}


//	public void setText( String text, boolean formatText, boolean autoSize ) {
//		label.setText( text, formatText, autoSize );
//		
//		if ( autoSize )
//			resizeToFit();
//	}

	
	
}
