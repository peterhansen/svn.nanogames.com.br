/*
 * ImageLoader.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.userInterface.AppMIDlet;
//#if J2SE == "false"
import javax.microedition.lcdui.Image;
//#else
//# import java.awt.image.BufferedImage;
//# import java.awt.Image;
//# import java.net.URL;
//# import javax.imageio.ImageIO;
//#endif

public final class ImageLoader {
 
	public static final byte FILTER_NONE		= 0;
	public static final byte FILTER_GRAY_SCALE	= 1;
	public static final byte FILTER_SEPIA		= 2;
	public static final byte FILTER_NEGATIVE	= 3;
	
	// cor utilizada para que todas as cores exceto magenta sejam trocadas
	public static final int CONVERT_ALL_COLOR = -1;
	
	
	private ImageLoader() {
	}
	
	
	/**
	 * 
	 * @param path
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Image loadImage( String path ) throws Exception {
		//#if DEBUG == "true"
//# 		// verifica se há a substring "//" no endereço (reinicia o aparelho em alguns aparelhos SonyEricsson)
//# 		if ( path.indexOf( "//" ) >= 0 )
//# 			throw new IllegalArgumentException( "Endereço da imagem contém \"//\"" );
//# 		try {
		//#endif
		
		AppMIDlet.gc();
		//#if J2SE == "false"
			final Image ret = Image.createImage( path );
		//#else
//# 			final BufferedImage ret = ImageIO.read( path.getClass().getResourceAsStream( path ) );
		//#endif
		// permite que uma outra Thread execute, evitando tempos de resposta altos ou até mesmo travamentos ao
		// suspender a execução durante o carregamento de muitas imagens (especialmente em aparelhos Motorola)
		Thread.yield();
		return ret;
		
		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 			
//# 			throw new Exception( e + " ao alocar imagem no caminho \"" + path + "\"." );
//# 		}
		//#endif
	}
	
	
	public static final Image loadImage( Image source, byte filterType ) throws Exception {
		//#if J2SE == "false"
			final int width  = source.getWidth();
			final int height = source.getHeight();
			
			final int[] data = new int[ width * height ]; 
			source.getRGB( data, 0, width, 0, 0, width, height );
		//#else
//# 			final int width  = source.getWidth( null );
//# 			final int height = source.getHeight( null );
//# 			
//# 			final int[] data = ( ( BufferedImage ) source ).getRGB( 0, 0, width, height, null, 0, width );
		//#endif
        
		
		switch ( filterType ) {
			case FILTER_GRAY_SCALE:
				toGray( data );
			break;

			case FILTER_SEPIA:
				toSepia( data );
			break;
        } // fim switch ( filterType )
		
		//#if J2SE == "false"
			return Image.createRGBImage( data, width, height, true );
		//#else
//# 			final BufferedImage ret = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );
//# 			ret.setRGB( 0, 0, width, height, data, 0, width );
//# 			
//# 			return ret;
		//#endif
        
	} // fim do método loadImage( Image, byte )
	 
	
	public static final Image loadImage( String filename, byte filterType ) throws Exception {
		return loadImage( ImageLoader.loadImage( filename ), filterType );
	}
	
	
	private static final void toGray( int[] data ) {
		for ( int i = 0; i < data.length; ++i ) {
			if ( ( data[ i ] & 0xff000000 ) == 0 ) {
				data[ i ] = 0x00ff00ff;
				continue;
			} else {
				final int color = data[ i ] & 0x00ffffff;
				final int media = ( ( ( color & 0xff0000 ) >> 16 ) +
									( ( color & 0x00ff00 ) >> 8 ) + 
									  ( color & 0x0000ff ) ) / 3;	
				
				data[ i ] = 0xff000000 | ( ( media << 16 ) | ( media << 8 ) | media );
			}
		}
	}
	
	
	private static final void toSepia( int[] data ) {
		// TODO sépia sepia
		for ( int i = 0; i < data.length; ++i ) {
			if ( ( data[ i ] & 0xff000000 ) == 0 ) {
				data[ i ] = 0x00ff00ff;
				continue;
			} else {
				final int color = data[ i ] & 0x00ffffff;
				
				final int r = ( color & 0xff0000 ) >> 16;
				final int g = ( color & 0x00ff00 ) >> 8;
				final int b = color & 0x0000ff;
				
				final int r2 = ( r * 393 + g * 769 + b * 189 ) / 1351;
				final int g2 = ( r * 349 + g * 586 + b * 168 ) / 1103;
				final int b2 = ( r * 272 + g * 534 + b * 131 ) / 2140;
				
//				original:
//				final int r2 = ( r * 393 + g * 769 + b * 189 ) / 1351;
//				final int g2 = ( r * 349 + g * 686 + b * 168 ) / 1203;
//				final int b2 = ( r * 272 + g * 534 + b * 131 ) / 2140;				

				data[ i ] = 0xff000000 | ( ( r2 << 16 ) | ( g2 << 8 ) | b2 );
			}
		}		
		
//		// We take the RGB colour and we map it to YIQ
//		vec3 colour = texture2D(OGL2Texture, gl_TexCoord[0].st).rgb;
//
//		float Y = dot(vec3( 0.299, 0.587, 0.114),colour);
//
//		// these values blatently ripped off from Pete's GPU forums
//		float red = Y + 0.1912;
//		float green = Y - 0.0544;
//		float blue  = Y - 0.2210;
//
//		// 0.0 alpha unless you want some sort of transparency
//		gl_FragColor = vec4(red,green,blue,0.0); 
//		return color;
	}
	
}
 
