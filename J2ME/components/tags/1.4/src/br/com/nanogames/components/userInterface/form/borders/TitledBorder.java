/**
 * TitledBorder.java
 * 
 * Created on 8/Dez/2008, 20:25:11
 *
 */

package br.com.nanogames.components.userInterface.form.borders;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.form.Component;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public class TitledBorder extends Border {
	
	public static final byte ABOVE_BOTTOM = 4;
	public static final byte ABOVE_TOP = 1;
	public static final byte BELOW_BOTTOM = 6;
	public static final byte BELOW_TOP = 3;
	public static final byte BOTTOM = 5;
	public static final byte CENTER = 2;
	public static final byte DEFAULT_JUSTIFICATION = 0;
	public static final byte DEFAULT_POSITION = 0;
	protected static final byte EDGE_SPACING = 2;
	public static final byte LEADING = 4;
	public static final byte LEFT = 1;
	public static final byte RIGHT = 3;
	protected static final byte TEXT_INSET_H = 5;
	protected static final byte TEXT_SPACING = 2;
	public static final byte TOP = 2;
	public static final byte TRAILING = 5;

	/** Label da borda. */
	protected final Label label;

	/**
	 *
	 */
	protected int labelAnchor;
	
	protected Border border;
	
	
	public TitledBorder( Label label, Border border ) throws Exception {
		super( border == null ? 1 : 2 );
		
		setTop( label.getHeight() );
		setBottom( 0 );
		setLeft( 0 );
		setRight( 0 );
		
		if ( border != null ) {
			this.border = border;
			insertDrawable( border );
			setTop( getTop() + border.getTop() );
		}

		this.label = label;
		insertDrawable( label );
		
		refreshTitle();
	}
	
	
	public TitledBorder( Label label ) throws Exception {
		this( label, null );
	}


	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	public Border getCopy() throws Exception {
		final TitledBorder copy = new TitledBorder( label, border );
		copy.setSize( size );

		return copy;
	}


	/**
	 * 
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		refreshTitle();
		updateBorder();
	}
	
	
	public void refreshTitle() {
//		// reposiciona o label dentro da borda
//		final Point pos = new Point();
//		switch ( labelAnchor & ANCHOR_VERTICAL_MASK ) {
//			case ANCHOR_BOTTOM:
//				setBottom( Math.max( bottom, label.getHeight() ) );
//				pos.y = getHeight() - bottom;
//			break;
//			
//			default:
//				labelAnchor = ( labelAnchor & ANCHOR_HORIZONTAL_MASK ) | ANCHOR_TOP;
//			case ANCHOR_TOP:
//				setTop( Math.max( top, label.getHeight() ) );
//			break;
//		}
//		
//		
//		switch ( labelAnchor & ANCHOR_HORIZONTAL_MASK ) {
//			case ANCHOR_HCENTER:
//				pos.x = getWidth() >> 1;
//			break;
//			
//			case ANCHOR_RIGHT:
//				pos.x = getWidth() - right;
//			break;
//			
//			default:
//				labelAnchor = ( labelAnchor & ANCHOR_VERTICAL_MASK ) | ANCHOR_LEFT;
//			case ANCHOR_LEFT:
//				pos.x = left;
//			break;
//		}
//		
//		label.defineReferencePixel( labelAnchor );
//		label.setRefPixelPosition( pos );
	}


	/**
	 *
	 * @return
	 */
	public int getLabelAnchor() {
		return labelAnchor;
	}


	/**
	 * Define a posição relativa do label.
	 * @param labelAnchor indica que área da borda deve ser tomada como referência para posicionamento do label.
	 * A posição final do label depende dessa área e do pixel de referência definido no label. Os valores válidos são
	 * combinações formadas pelo operador binário OU ('|') de um tipo de alinhamento horizontal e outro vertical,
	 * definidos na classe Drawable:
	 * <ul>
	 * <li>ANCHOR_LEFT: alinhado à esquerda.</li>
	 * <li>ANCHOR_HCENTER: centralizado horizontalmente.</li>
	 * <li>ANCHOR_RIGHT: horizontal à direita.</li>
	 * <br></br>
	 * <li>ANCHOR_TOP: alinhamento vertical no topo da linha.</li>
	 * <li>ANCHOR_BOTTOM:	alinhamento vertical na parte inferior da linha.</li>
	 * </ul>
	 * 
	 * @see br.com.nanogames.components.Drawable#ANCHOR_LEFT
	 * @see br.com.nanogames.components.Drawable#ANCHOR_HCENTER
	 * @see br.com.nanogames.components.Drawable#ANCHOR_RIGHT
	 * @see br.com.nanogames.components.Drawable#ANCHOR_TOP
	 * @see br.com.nanogames.components.Drawable#ANCHOR_BOTTOM
	 */
	public void setLabelAnchor( int labelAnchor ) {
		this.labelAnchor = labelAnchor;
		
		refreshTitle();
	}


	/**
	 * 
	 * @return
	 */
	public final Label getLabel() {
		return label;
	}


	public void setState( int state ) {
		if ( border == null )
			super.setState( state );
		else 
			border.setState( state );
	}


	public byte getState() {
		return border == null ? super.getState() : border.getState();
	}


	public void setComponent( Component c, boolean fitSize ) {
		if ( border == null ) {
			super.setComponent( c, fitSize );
		} else {
			setSize( border.getSize().add( 0, label.getHeight() ) );
			
			border.setComponent( c, fitSize );
		}
	}


	public byte getBottom() {
		return ( byte ) ( super.getBottom() + ( border == null ? 0 : border.getBottom() ) );
	}


	public byte getLeft() {
		return ( border == null ? super.getLeft() : border.getLeft() );
	}


	public byte getRight() {
		return ( border == null ? super.getRight() : border.getRight() );
	}


	public byte getTop() {
		return ( byte ) ( super.getTop() + ( border == null ? 0 : border.getTop() ) );
	}
	
	
	private final void updateBorder() {
		if ( border != null ) {
			final int verticalAlignment = labelAnchor & ANCHOR_VERTICAL_MASK;

			switch ( verticalAlignment ) {
				case ANCHOR_BOTTOM:
					setBottom( label.getHeight() );
					setTop( 0 );
					border.setPosition( super.getLeft(), 0 );
				break;

				case ANCHOR_TOP:
				default:
					setBottom( 0 );
					setTop( label.getHeight() );
					border.setPosition( super.getLeft(), super.getTop() );
				break;
			}

			border.setSize( getWidth(), getHeight() - border.getPosY() );		
		}
	}

}
