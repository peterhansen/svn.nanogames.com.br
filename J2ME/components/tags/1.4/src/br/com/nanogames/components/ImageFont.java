/*
 * ImageFont.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.ImageLoader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Vector;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
//#else
//# import java.awt.Image;
//#endif

/**
 *
 * @author peter
 */
public class ImageFont {

	/** Tipo de caracter: qualquer tipo. */
	public static final byte CHAR_TYPE_ALL					= 0;
	/** Tipo de caracter: acentuados com caixa baixa. */
	public static final byte CHAR_TYPE_ACCENT_LOWER_CASE	= 1;
	/** Tipo de caracter: acentuados com caixa alta. */
	public static final byte CHAR_TYPE_ACCENT_UPPER_CASE	= 2;
	/** Tipo de caracter: especiais @#$%¨&*()/-+="',.;:[]{}!? etc. */
	public static final byte CHAR_TYPE_SPECIAL				= 3;
	/** Tipo de caracter: comum (A-Z, a-z) e ç. */
	public static final byte CHAR_TYPE_REGULAR				= 4;
	/** Tipo de caracter: numérico (0-9). */
	public static final byte CHAR_TYPE_NUMERIC				= 5;

 
	/** Número de caracteres disponíveis. */
	protected static final short CHAR_TABLE_SIZE = 256;
	
	/** Imagem contendo os caracteres da fonte. */
	protected final Image image;
	
	/** Largura de cada caracter. */
	protected final byte[] charsWidths = new byte[ CHAR_TABLE_SIZE ];
	
	/** Posição inicial (x) de cada caracter na imagem. */
    protected final short[] charsOffsets = new short[ CHAR_TABLE_SIZE ];
	
	/** Offset extra entre os caracteres (utilizado para se aumentar ou reduzir o espaçamento padrão entre eles). */
	protected byte charExtraOffset;
	 
	
	protected ImageFont( Image image ) {
		this.image = image;
	}
	

	/**
	 * Cria uma nova instância de fonte monoespaçada.
	 * @param imagePath endereço da imagem que contém os caracteres da fonte.
	 * @param characters caracteres presentes na imagem.
	 * @return referência para a fonte criada, ou null caso haja erro ao alocar recursos ou parâmetros inválidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMonoSpacedFont( String imagePath, String characters ) throws Exception {
		return createMonoSpacedFont( ImageLoader.loadImage( imagePath ), characters );
	}	
	
	
	/**
	 * Cria uma nova instância de fonte monoespaçada.
	 * @param image imagem que contém os caracteres da fonte.
	 * @param characters caracteres presentes na imagem.
	 * @return referência para a fonte criada, ou null caso haja erro ao alocar recursos ou parâmetros inválidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMonoSpacedFont( Image image, String characters ) throws Exception {
		ImageFont font = new ImageFont( image );

		// se a divisão da largura da imagem pelo número de caracteres não for exata, retorna erro
		//#if J2SE == "false"
			final int IMAGE_WIDTH = image.getWidth();
		//#else
//# 			final int IMAGE_WIDTH = image.getWidth( null );
		//#endif

		if ( IMAGE_WIDTH % characters.length() != 0 ) {
			//#if DEBUG == "true"
//# 					throw new Exception( "Erro ao criar fonte monoespaçada: largura da imagem( " + image.getWidth() + ") e o número de caracteres (" + characters.length() + ") não resultam numa divisão exata." );
			//#else
				throw new Exception();
			//#endif
		}

		// pré-calcula a largura dos caracteres
		final byte charWidth = ( byte ) ( IMAGE_WIDTH / characters.length() );
		final char[] chars = new char[ characters.length() ];
		characters.getChars( 0, characters.length(), chars, 0 );

		// preenche o array de tamanhos de caracteres
		for ( int i = 0; i < chars.length; ++i ) {
			font.charsWidths[ chars[ i ] ] = charWidth;
		}
		font.calculateOffsets( chars );

		return font;
	} // fim do método createMonoSpacedFont()


	/**
	 * Cria uma nova instância de fonte multiespaçada.
	 * @param pathPrefix prefixo do caminho da imagem (.png) e do arquivo descritor (.bin) da fonte.
	 * @return referência para a fonte criada, ou null caso haja erro ao alocar recursos ou parâmetros inválidos.
	 * @throws java.lang.Exception
	 */
	public static final ImageFont createMultiSpacedFont( String pathPrefix ) throws Exception {
		return createMultiSpacedFont( pathPrefix + ".png", pathPrefix + ".bin" );
	}

	
	/**
	 * Cria uma nova instância de fonte multiespaçada.
	 * @param imagePath caminho da imagem que contém os caracteres da fonte.
	 * @param fontDataFile endereço do arquivo binário que descreve a tira de fonte.
	 * @return referência para a fonte criada, ou null caso haja erro ao alocar recursos ou parâmetros inválidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMultiSpacedFont( String imagePath, String fontDataFile ) throws Exception {
		return createMultiSpacedFont( ImageLoader.loadImage( imagePath ), fontDataFile );
	}	
	
	
	/**
	 * Cria uma nova instância de fonte multiespaçada.
	 * @param image imagem que contém os caracteres da fonte.
	 * @param fontDataFile endereço do arquivo binário que descreve a tira de fonte.
	 * @return referência para a fonte criada, ou null caso haja erro ao alocar recursos ou parâmetros inválidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMultiSpacedFont( Image image, String fontDataFile ) throws Exception {
		InputStream input = null;
		DataInputStream dataInput = null;
			
		try {
			ImageFont font = new ImageFont( image );
			
			input = font.getClass().getResourceAsStream( fontDataFile );
			dataInput = new DataInputStream( input );
			
			// obtém o número total de caracteres da fonte
			final short numberOfChars = ( short ) dataInput.readUnsignedByte();
			final byte[] tempCharsWidth = new byte[ numberOfChars ];
			for ( int i = 0; i < numberOfChars; ++i ) {
				tempCharsWidth[ i ] = ( byte ) dataInput.readUnsignedByte();
			}
			
			
			char c;
			// esse array temporário é utilizado para cálculo posterior dos offsets de cada caracter, 
			// passando-o para o método calculateOffsets()
			final char[] chars = new char[ numberOfChars ];
			// armazena as larguras de cada caracter
			for ( int i = 0; i < numberOfChars; ++i ) {
				c = ( char ) ( dataInput.readUnsignedByte() );
				chars[ i ] = c;
				font.charsWidths[ c ] = tempCharsWidth[ i ];
			}
			
			font.calculateOffsets( chars );

			AppMIDlet.gc();
			return font;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			throw e;
		} finally {
			try {
//				if ( input != null )
//					input.close();
				if ( dataInput != null )
					dataInput.close();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}
		}
	} // fim do método createMultiSpacedFont()
	
	
	/**
	 *	Método auxiliar para calcular e atualizar os offsets de cada caracter na fonte.
	 * 
	 * @param characters 
	 */
	protected final void calculateOffsets( char[] characters ) {
		short offset = 0;
		for ( int i = 0; i < characters.length; ++i ) {
			charsOffsets[ characters[ i ] ] = offset;
			offset += charsWidths[ characters[ i ] ];
		}

		// TODO remapear maiúsculas/minúsculas (caso só tenha um)
	} // fim do método calculateOffsets( char[] )


	/**
	 * Retorna a largura em pixels de um texto.
	 *
	 * @param text índice do texto cuja largura será calculada.
	 * @return largura do texto em pixels.
	 * @throws NullPointerException caso textIndex seja inválido.
	 * @see #getTextWidth(char[])
	 * @see #getTextWidth(String)
	 */
	public final int getTextWidth( int textIndex ) {
		return getTextWidth( AppMIDlet.getText( textIndex ) );
	}
	
	
	/**
	 * Retorna a largura em pixels de um texto.
	 * 
	 * @param text texto cuja largura será calculada.
	 * @return largura do texto em pixels.
	 * @throws NullPointerException caso text seja null.
	 * @see #getTextWidth(char[])
	 * @see #getTextWidth(int)
	 */
	public final int getTextWidth( String text ) {
		return getTextWidth( text.toCharArray() );
	} // fim do método getTextWidth( String )
	
	
	/**
	 * Retorna a largura em pixels de um texto.
	 * 
	 * @param text texto cuja largura será calculada.
	 * @return largura do texto em pixels.
	 * @throws NullPointerException caso text seja null.
	 * @see #getTextWidth(String)
	 * @see #getTextWidth(int)
	 */	
	public int getTextWidth( char[] text ) {
		int width = 0;

		for ( int i = 0; i < text.length; ++i )
			width += getCharWidth( text[ i ] );

		// a largura é corrigida para que o último caracter seja exibido corretamente
		return width > 0 ? width - charExtraOffset : 0;
	}
	
	
	/**
	 * Retorna a largura de um caracter da fonte.
	 * @param c caracter cuja largura será retornada.
	 * @return largura do caracter em pixels.
	 */
	public int getCharWidth( char c ) {
		// não é necessário verificar se é menor que zero pois char é unsigned
		return ( ( c < CHAR_TABLE_SIZE && charsWidths[c] > 0 ) ? charsWidths[ c ] + charExtraOffset : 0 );
	} // fim do método getCharWidth( char )	 
	
	
	public final Image getImage() {
		return image;
	}
	
	
	/**
	 * Obtém a altura da fonte.
	 * 
	 * @return inteiro indicando a altura dos caracteres da fonte.
	 */
	public final int getHeight() {
		//#if J2SE == "false"
			return image.getHeight();
		//#else
//# 			return image.getHeight( null );
		//#endif
	}
	

	/**
	 *
	 * @param offset
	 */
	public final void setCharExtraOffset( int extraOffset ) {
		charExtraOffset = ( byte ) extraOffset;
	}
	

	/**
	 * 
	 * @return
	 */
	public final byte getCharExtraOffset() {
		return charExtraOffset;
	}


	/**
	 * 
	 * @param c
	 * @return
	 */
	public final short getCharOffset( char c ) {
		return c < CHAR_TABLE_SIZE ? charsOffsets[ c ] : 0;
	}


	/**
	 * Indica o tipo de um caracter.
	 * @param c caracter cujo tipo será obtido.
	 * @return tipo do caracter. Possíveis valores retornados:
	 * <ul>
	 * <li>CHAR_TYPE_ACCENT_LOWER_CASE</li>
	 * <li>CHAR_TYPE_ACCENT_UPPER_CASE</li>
	 * <li>CHAR_TYPE_REGULAR</li>
	 * <li>CHAR_TYPE_SPECIAL</li>
	 * <li>CHAR_TYPE_NUMERIC</li>
	 * </ul>
	 */
	public static final byte getCharType( char c ) {
		switch ( c ) {
			case 'á': case 'é': case 'í': case 'ó': case 'ú':
			case 'ã': case 'õ':
			case 'à': case 'è':
			case 'ñ':
			case 'â': case 'ê': case 'ô':
			case 'ä': case 'ë': case 'ö': case 'ü':
				return CHAR_TYPE_ACCENT_LOWER_CASE;

			case 'Á': case 'É': case 'Í': case 'Ó': case 'Ú':
			case 'Ã': case 'Õ':
			case 'À': case 'È':
			case 'Ñ':
			case 'Â': case 'Ê': case 'Ô':
			case 'Ä': case 'Ë': case 'Ö': case 'Ü':
				return CHAR_TYPE_ACCENT_UPPER_CASE;

			case 'ç':
			case 'Ç':
				return CHAR_TYPE_REGULAR;

			case ' ':
				return CHAR_TYPE_ALL;

			default:
				if ( c >= '0' && c <= '9' )
					return CHAR_TYPE_NUMERIC;
				else if ( ( c < 'A' || c > 'Z' ) && ( c < 'a' || c > 'z' ) )
					return CHAR_TYPE_SPECIAL;
				else
					return CHAR_TYPE_REGULAR;
		} // fim switch ( c )
	} // fim do método getCharType( byte )


	/**
	 * Retorna um array com todos os caracteres de um determinado tipo existentes na fonte.
	 * @param type tipo de caracter. Valores válidos:
	 * <ul>
	 * <li>CHAR_TYPE_ACCENT_LOWER_CASE</li>
	 * <li>CHAR_TYPE_ACCENT_UPPER_CASE</li>
	 * <li>CHAR_TYPE_REGULAR</li>
	 * <li>CHAR_TYPE_SPECIAL</li>
	 * <li>CHAR_TYPE_NUMERIC</li>
	 * <li>CHAR_TYPE_ALL</li>
	 * </ul>
	 * @return array com todos os caracteres de um determinado tipo existentes na fonte.
	 */
	public final char[] getAllChars( byte type ) {
		final char[] temp = new char[ 256 ];

		short count = 0;
		for ( char c = 0; c < 256; ++c ) {
			if ( charsWidths[ c ] > 0 && ( type == CHAR_TYPE_ALL || getCharType( c ) == type ) )
				temp[ count++ ] = c;
		}

		final char[] ret = new char[ count ];
		for ( short i = 0 ; i < ret.length; ++i )
			ret[ i ] = temp[ i ];

		return ret;
	}


}
 
