/**
 * Tokenizer.java
 * ©2008 Nano Games.
 *
 * Created on 24/01/2008 16:30:43.
 */

package br.com.nanogames.components.util;

import java.util.Vector;


/**
 * 
 * @author Peter
 */
public final class Tokenizer {
	
	/** Delimitador espaço: ' ' */
	public static final byte DELIMITER_SPACE			= 1;
	
	/** Delimitador tabulação: '\t'. */
	public static final byte DELIMITER_TAB				= 2;
	
	/** Delimitador quebra de linha: '\n'. */
	public static final byte DELIMITER_NEW_LINE			= 4;
	
	/** Delimitador retorno de carro: '\r'. */
	public static final byte DELIMITER_CARRIAGE_RETURN	= 8;
	
	/** Delimitador form feed: '\f'. */
	public static final byte DELIMITER_FORM_FEED		= 16;
	
	/** Delimitador padrão do Tokenizer, compreendendo todos os delimitadores: DELIMITER_SPACE, DELIMITER_TAB, 
	 * DELIMITER_NEW_LINE, DELIMITER_CARRIAGE_RETURN e DELIMITER_FORM_FEED. */
	public static final byte DELIMITER_DEFAULT			= DELIMITER_SPACE | DELIMITER_TAB | DELIMITER_NEW_LINE | 
														  DELIMITER_CARRIAGE_RETURN | DELIMITER_FORM_FEED;
	
	private static final char CHAR_SPACE			= ' ';
	private static final char CHAR_TAB				= '\t';
	private static final char CHAR_NEW_LINE			= '\n';
	private static final char CHAR_CARRIAGE_RETURN	= '\r';
	private static final char CHAR_FORM_FEED		= '\f';
	
	private static final char[] CHAR_ALL = { CHAR_SPACE, CHAR_TAB, CHAR_NEW_LINE, CHAR_CARRIAGE_RETURN, CHAR_FORM_FEED };
	

	
	private Tokenizer() {
	}
	
	
	/**
	 * Separa um texto em tokens, utilizando o delimitador padrão (DELIMITER_DEFAULT).
	 * 
	 * @param s texto a ser dividido em tokens.
	 * @return
	 */	
	public static final String[] tokenize( String s ) {
		return tokenize( s, DELIMITER_DEFAULT );
	}
	
	
	/**
	 * Separa um texto em tokens, utilizando o delimitador recebido como parâmetro.
	 * 
	 * @param s texto a ser dividido em tokens.
	 * @param delimiter 
	 * @return
	 */
	public static final String[] tokenize( String s, int delimiter ) {
		final char[] delimiters = new char[ CHAR_ALL.length ];
		for ( int i = 0; i < delimiters.length; ++i ) {
			if ( ( delimiter & ( 1 << i ) ) != 0 )
				delimiters[ i ] = CHAR_ALL[ i ];
		}
		
		return tokenize( s, delimiters );
	}
	
	
	public static final String[] tokenize( String s, char[] delimiters ) {
		final Vector vector = new Vector();
		final char[] array = s.toCharArray();
		
		final boolean[] delimiterChars = new boolean[ 256 ];
		for ( int i = 0; i < delimiters.length; ++i )
			delimiterChars[ delimiters[ i ] ] = true;
		
		final StringBuffer token = new StringBuffer();
		
		for ( int i = 0; i < array.length; ++i ) {
			final char c = array[ i ];
			
			if ( delimiterChars[ c ] ) {
				// TODO adicionar boolean para definir se o caracter delimitador também será inserido
				// achou um delimitador; verifica se há um token
				if ( token.length() > 0 ) {
					// insere o token
					vector.addElement( token.toString() );
					token.delete( 0, token.length() ); // TODO melhor usar delete() ou setLength()?
				}
			} else {
				token.append( c );
			}
		}
		
		final String[] tokens = new String[ vector.size() ];
		for ( int i = 0; i < tokens.length; ++i )
			tokens[ i ] = ( String ) vector.elementAt( i );
		
		return tokens;		
	}
	
	
}
