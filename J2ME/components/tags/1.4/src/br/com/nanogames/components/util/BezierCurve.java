/*
 * BezierCurve.java
 *
 * Created on 5 de Julho de 2006, 22:16
 */

package br.com.nanogames.components.util;

/**
 *	Classe utilizada para calcular uma curva de Bezier a partir de 4 pontos
 *	dados. Código adaptado de: http://en.wikipedia.org/wiki/B%C3%A9zier_curve
 * @author peter
 */
public final class BezierCurve {

	private static final int FP_3 = NanoMath.toFixed( 3 );
	
	public final Point origin = new Point();
	public final Point control1 = new Point();
	public final Point control2 = new Point();
	public final Point destiny = new Point();
	
	
	public BezierCurve() {
	}

	
	/** Creates a new instance of BezierCurve.
	 * 
	 * @param origin
	 * @param control1 
	 * @param control2
	 * @param destiny 
	 */
	public BezierCurve( Point origin, Point control1, Point control2, Point destiny ) {
		setValues( origin, control1, control2, destiny );
	}
	
	
	/**
	 * 
	 * @param origin
	 * @param control1
	 * @param control2
	 * @param destiny
	 */
	public final void setValues( Point origin, Point control1, Point control2, Point destiny ) {
		this.origin.set( origin );
		this.control1.set( control1 );
		this.control2.set( control2 );
		this.destiny.set( destiny );
	}
	
	
	/**
	 * Obtém o ponto da curva referente a determinada porcentagem desta curva.
	 * @param fill referência para o ponto que será preenchido com a posição (x,y) referentes à porcentagem definida da curva.
	 * @param percent porcentagem do andamento da curva. Valores menores que 0 (zero) ou maiores que 100 (cem) serão truncados.
	 * @see #getX(int)
	 * @see #getY(int)
	 */
	public final void getPointAt( Point fill, int percent ) {
		getPointAtFixed( fill, NanoMath.divInt( percent, 100 ) );
	}


	/**
	 *
	 * @param fill
	 * @param fp_percent
	 */
	public final void getPointAtFixed( Point fill, int fp_percent ) {
		fill.set( calculateXFP( fp_percent ), calculateYFP( fp_percent ) );
	}
	
	
	/**
	 * Retorna apenas a componente x de um ponto da curva.
	 * @param percent porcentagem de andamento da curva.
	 * @return valor do ponto x na posição determinada em <i>percent</i> porcento da curva.
	 * @see #getPointAt(Point,int)
	 * @see #getY(int)
	 */
	public final int getX( int percent ) {
		return calculateXFP( NanoMath.divInt( percent, 100 ) );
	}
	
	
	/**
	 * Retorna apenas a componente y de um ponto da curva.
	 * @param percent porcentagem de andamento da curva.
	 * @return valor do ponto y na posição determinada em <i>percent</i> porcento da curva.
	 * @see #getPointAt(Point,int)
	 * @see #getX(int)
	 */
	public final int getY( int percent ) {
		return calculateYFP( NanoMath.divInt( percent, 100 ) );
	}


	public final int getXFixed( int fp_percent ) {
		return calculateXFP( fp_percent );
	}
	
	
	public final int getYFixed( int fp_percent ) {
		return calculateYFP( fp_percent );
	}


	private final int calculateXFP( int fp_percent ) {
		fp_percent = NanoMath.clamp( fp_percent, 0, NanoMath.ONE );

		final int fp_square = NanoMath.mulFixed( fp_percent, fp_percent );
		
		final int fp_cx = NanoMath.mulFixed( FP_3, NanoMath.toFixed( control1.x - origin.x ) );
		final int fp_bx = NanoMath.mulFixed( FP_3, NanoMath.toFixed( control2.x - control1.x ) ) - fp_cx;
		final int fp_ax = NanoMath.toFixed( destiny.x - origin.x ) - fp_cx - fp_bx;

		return NanoMath.toInt( NanoMath.mulFixed( fp_ax, NanoMath.mulFixed( fp_percent, fp_square ) ) +
							   NanoMath.mulFixed( fp_bx, fp_square ) +
							   NanoMath.mulFixed( fp_cx, fp_percent ) + NanoMath.toFixed( origin.x ) );
	}
	
	
	private final int calculateYFP( int fp_percent ) {
		fp_percent = NanoMath.clamp( fp_percent, 0, NanoMath.ONE );

		final int fp_square = NanoMath.mulFixed( fp_percent, fp_percent );
		
		final int fp_cy = NanoMath.mulFixed( FP_3, NanoMath.toFixed( control1.y - origin.y ) );
		final int fp_by = NanoMath.mulFixed( FP_3, NanoMath.toFixed( control2.y - control1.y ) ) - fp_cy;
		final int fp_ay = NanoMath.toFixed( destiny.y - origin.y ) - fp_cy - fp_by;

		return NanoMath.toInt( NanoMath.mulFixed( fp_ay, NanoMath.mulFixed( fp_percent, fp_square ) ) +
							   NanoMath.mulFixed( fp_by, fp_square ) +
							   NanoMath.mulFixed( fp_cy, fp_percent ) + NanoMath.toFixed( origin.y ) );
	}
	
}
