//#if TOUCH == "true"
	/**
	 * PointerListener.java
	 * ©2008 Nano Games.
	 *
	 * Created on 14/02/2008 01:54:40.
	 */

	package br.com.nanogames.components.userInterface;

	/**
	 *
	 * @author Peter
	 */
	public interface PointerListener {

		public void onPointerDragged( int x, int y );


		public void onPointerPressed( int x, int y );


		public void onPointerReleased( int x, int y );

	}

//#endif