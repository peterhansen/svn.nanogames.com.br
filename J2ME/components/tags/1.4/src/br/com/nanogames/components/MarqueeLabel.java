/*
 * MarqueeLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.MUV;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

/**
 *
 * @author peter
 */
public class MarqueeLabel extends Label implements Updatable {
	
	/** velocidade padrão de animação do texto em pixels por segundo */
	public static final byte DEFAULT_TEXT_SPEED = 30;
 
	/** Velocidade absoluta da animação do texto, em pixels por segundo. */
	protected final MUV speed = new MUV( DEFAULT_TEXT_SPEED );
	
	/** tempo padrão de espera em milisegundos, no final de cada movimento (modo SCROLL_MODE_LEFT_RIGHT) */
	public static final short DEFAULT_WAIT_TIME = 800;
	
	/** Tempo atual total de espera. */
	protected int waitTime = DEFAULT_WAIT_TIME;
	
	/** Tempo restante de espera. */
	protected int remainingWaitTime;
	
	/** posição x atual do texto */
	protected short textX;
	
	/** largura do texto */
	protected short textWidth;
	
	protected static final byte DIR_LEFT		= 0;
	protected static final byte DIR_RIGHT		= 1;
	protected static final byte DIR_STOP_LEFT	= 2;
	protected static final byte DIR_STOP_RIGHT	= 3;
	
	protected byte direction;

	// modos de scroll do texto
	/** pára a rolagem do texto */
	public static final byte SCROLL_MODE_NONE		= 0;
	/** texto rola para a esquerda até o fim, e então reaparece na direita */
	public static final byte SCROLL_MODE_LEFT		= 1;
	/** texto rola para a direita até o fim, e então reaparece na esquerda */
	public static final byte SCROLL_MODE_RIGHT		= 2;
	/** texto rola da direita para a esquerda; ao chegar no início, inverte sentido, e segue assim indefinidamente */
	public static final byte SCROLL_MODE_LEFT_RIGHT	= 4;
	
	protected byte scrollMode = SCROLL_MODE_LEFT;
	
	/** Frequência da animação do texto: sempre animado. */
	public static final byte SCROLL_FREQ_ALWAYS			= 0;
	/** Frequência da animação do texto: sem animação. */
	public static final byte SCROLL_FREQ_NONE			= 1;
	/** Frequência da animação do texto: somente se o texto for maior que a largura do label. */
	public static final byte SCROLL_FREQ_IF_BIGGER		= 2;
	/** Frequência da animação do texto: somente se o texto for menor que a largura do label. */
	public static final byte SCROLL_FREQ_IF_SMALLER		= 3;
	
	/** Frequência de animação atual. */
	protected byte scrollFrequency = SCROLL_FREQ_ALWAYS;
	
	/** Indica se o texto está sendo animado atualmente. */
	protected boolean scrolling;
	
	protected short limitLeft;
	protected short limitRight;
	
	
	/**
	 * Cria um novo MarqueeLabel.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto do label.
	 * @param speed velocidade do label em pixels por segundo.
	 * @param scrollMode modo de movimentação do texto. Valores válidos:
	 * <ul>
	 * <li>SCROLL_MODE_LEFT: texto rola para a esquerda até o fim, e então reaparece na direita.</li>
	 * <li>SCROLL_MODE_RIGHT: texto rola para a direita até o fim, e então reaparece na esquerda.</li>
	 * <li>SCROLL_MODE_LEFT_RIGHT: texto rola da direita para a esquerda; ao chegar no início, inverte sentido, e 
	 * segue assim indefinidamente.</li>
	 * </ul>
	 * @throws java.lang.Exception 
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String)
	 */
	public MarqueeLabel( ImageFont font, String text, int speed, byte scrollMode ) throws Exception {
		super( font, text );
		
		setSpeed( speed );
		setScrollMode( scrollMode );
	} // fim do construtor ( ImageFont, String, int, byte )


	/**
	 * Cria um novo MarqueeLabel a partir da fonte e do texto recebidos, movendo o texto da direita para a esquerda com
	 * velocidade padrão.
	 *
	 * @param fontIndex índice da fonte utilizada para desenhar os caracteres do label.
	 * @param textIndex índice do texto do label.
	 * @throws java.lang.Exception
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String, int, byte)
	 */
	public MarqueeLabel( int fontIndex, int textIndex ) throws Exception {
		this( fontIndex, AppMIDlet.getText( textIndex ) );
	} // fim do construtor ( ImageFont, String )


	/**
	 * Cria um novo MarqueeLabel a partir da fonte e do texto recebidos, movendo o texto da direita para a esquerda com
	 * velocidade padrão.
	 *
	 * @param fontIndex índice da fonte utilizada para desenhar os caracteres do label.
	 * @param text texto do label.
	 * @throws java.lang.Exception
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String, int, byte)
	 */
	public MarqueeLabel( int fontIndex, String text ) throws Exception {
		this( AppMIDlet.GetFont( fontIndex ), text );
	} // fim do construtor ( ImageFont, String )
	
	 
	/**
	 * Cria um novo MarqueeLabel a partir da fonte e do texto recebidos, movendo o texto da direita para a esquerda com
	 * velocidade padrão.
	 *
	 * @param fontIndex fonte utilizada para desenhar os caracteres do label.
	 * @param text índice do texto do label.
	 * @throws java.lang.Exception
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String, int, byte)
	 */
	public MarqueeLabel( ImageFont font, int textIndex ) throws Exception {
		this( font, AppMIDlet.getText( textIndex ) );
	} // fim do construtor ( ImageFont, String )


	/**
	 * Cria um novo MarqueeLabel a partir da fonte e do texto recebidos, movendo o texto da direita para a esquerda com 
	 * velocidade padrão.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto do label.
	 * @throws java.lang.Exception 
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String, int, byte)
	 */
	public MarqueeLabel( ImageFont font, String text ) throws Exception {
		super( font, text );
	} // fim do construtor ( ImageFont, String )	
	 
	
	/**
	 * Define a velocidade do scroll do texto.
	 * @param speed velodidade do scroll do texto, em pixels por segundo.
	 */
	public void setSpeed( int speed ) {
		this.speed.setSpeed( Math.abs( speed ) );
	} // fim do método setSpeed( int )
	
	
	/**
	 * Define o modo de scroll do texto.
	 * @param mode modo de scroll do texto. Valores válidos:
	 * <ul>
	 * <li>SCROLL_MODE_LEFT: texto rola para a esquerda até o fim, e então reaparece na direita.</li>
	 * <li>SCROLL_MODE_RIGHT: texto rola para a direita até o fim, e então reaparece na esquerda.</li>
	 * <li>SCROLL_MODE_LEFT_RIGHT: texto rola da direita para a esquerda; ao chegar no início, inverte sentido, e segue assim indefinidamente.</li>
	 * </ul>
	 */
	public void setScrollMode( byte mode ) {
		switch ( mode ) {
			case SCROLL_MODE_LEFT_RIGHT:
				direction = DIR_LEFT;
				
				if ( textWidth > size.x ) {
					limitLeft = ( short ) ( size.x - textWidth );
					limitRight = 0;
				} else {
					limitLeft = 0;
					limitRight = ( short ) ( size.x - textWidth );
				}
			break;
			
			case SCROLL_MODE_LEFT:
			case SCROLL_MODE_RIGHT:
			case SCROLL_MODE_NONE:
			break;
			
			default:
				//#if DEBUG == "true"
//# 					throw new IllegalArgumentException( "invalid scroll mode: " + mode );
				//#else
					return;
				//#endif
		} // fim switch ( mode )
		
		scrollMode = mode;
	} // fim do método setScrollMode( byte )
	
	
	public void setScrollFrequency( byte frequency ) {
		switch ( frequency ) {
			case SCROLL_FREQ_ALWAYS:
				scrolling = true;
			break;
			
			case SCROLL_FREQ_IF_BIGGER:
				scrolling = textWidth > getWidth();
			break;
			
			case SCROLL_FREQ_IF_SMALLER:
				scrolling = textWidth < getWidth();
			break;
			
			case SCROLL_FREQ_NONE:
				scrolling = false;
			break;
			
			default:
				//#if DEBUG == "true"
//# 					throw new IllegalArgumentException( "invalid scroll frequency: " + frequency );
				//#else
					return;
				//#endif
		}
		
		scrollFrequency = frequency;
	}
	
	
	public final byte getScrollFrequency() {
		return scrollFrequency;
	}
	
	
	public byte getScrollMode() {
		return scrollMode;
	}
	 
	
	public void update( int delta ) {
		if ( scrolling ) {
			switch ( scrollMode ) {
				case SCROLL_MODE_LEFT:
					textX -= speed.updateInt( delta );

					if ( textX < -textWidth ) {
						textX = ( short ) size.x;
					}
				break;

				case SCROLL_MODE_LEFT_RIGHT:
					switch ( direction ) {
						case DIR_LEFT:
							textX -= speed.updateInt( delta );

							if ( textX <= limitLeft ) {
								textX = limitLeft;
								remainingWaitTime = waitTime;
								direction = DIR_STOP_RIGHT;
							}
						break;

						case DIR_RIGHT:
							textX += speed.updateInt( delta );

							if ( textX >= limitRight ) {
								textX = limitRight;
								remainingWaitTime = waitTime;
								direction = DIR_STOP_LEFT;
							}
						break;

						case DIR_STOP_LEFT:
							remainingWaitTime -= delta;
							if ( remainingWaitTime <= 0 ) {
								direction = DIR_LEFT;
							}
						break;

						case DIR_STOP_RIGHT:
							remainingWaitTime -= delta;
							if ( remainingWaitTime <= 0 ) {
								direction = DIR_RIGHT;
							}						
						break;
					} // fim switch ( direction )
				break;

				case SCROLL_MODE_RIGHT:
					textX += speed.updateInt( delta );

					if ( textX >= size.x ) {
						textX = ( short ) -textWidth;
					}
				break;
			} // fim switch ( scrollMode )
		}
	} // fim do método update( int )
	
	
	public void setText( String text, boolean setSize ) {
		super.setText( text, setSize );
		
		if ( setSize )
			setSize( size.x, font.getHeight() );
		
		// pré-calcula a largura do texto, para otimizar a animação
		textWidth = ( short ) font.getTextWidth( charBuffer );
		
		// recalcula a posição de início e fim do scroll, caso necessário
		setScrollMode( scrollMode );
		
		setScrollFrequency( scrollFrequency );
	}	
	
	
	/**
	 * Define o tempo de espera do texto ao terminar de rolar para um lado.
	 * @param waitTime tempo em milisegundos que o texto pára até voltar a se mover.
	 */
	public void setWaitTime( int waitTime ) {
		if ( waitTime < 0 )
			waitTime = 0;
		
		this.waitTime = waitTime;
	} // fim do método setWaitTime( int )
	
	
	public int getWaitTime() {
		return waitTime;
	}

	
	protected void paint( Graphics g ) {
		drawString( g, charBuffer, translate.x + textX, translate.y );
	}

	
	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		// recalcula a posição de início e fim do scroll, caso necessário
		setScrollMode( scrollMode );
		
		setScrollFrequency( scrollFrequency );
	} // fim do método setSize( int, int )


	public void setTextOffset( int textOffset ) {
		textX = ( short ) textOffset;
	}
	
	
	/**
	 * Obtém a posição atual do texto (esquerda).
	 * 
	 * @return posição x atual do texto, em pixels.
	 */
	public final int getTextOffset() {
		return textX;
	}
	
	
	/**
	 * Retorna a velocidade de animação do texto.
	 * @return velocidade de animação do texto, em pixels por segundo.
	 */
	public int getSpeed() {
		return speed.getSpeed();
	}
	
}
 
