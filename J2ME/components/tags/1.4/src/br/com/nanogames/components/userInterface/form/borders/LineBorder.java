/**
 * LineBorder.java
 * 
 * Created on 8/Dez/2008, 22:27:35
 *
 */

package br.com.nanogames.components.userInterface.form.borders;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 * Referência para os tipos de borda: http://java.sun.com/docs/books/tutorial/uiswing/components/border.html
 */
public class LineBorder extends Border {

	/***/
	private static final byte COLOR_DARK_FACTOR = -40;

	private static final short COLOR_DARKER_FACTOR = ( COLOR_DARK_FACTOR ) << 1;

	/** Tipo de borda: simples (única cor). */
	public static final byte TYPE_SIMPLE			= 0;
	/** Tipo de borda: */
	public static final byte TYPE_ETCHED_RAISED		= 1;
	/** Tipo de borda: */
	public static final byte TYPE_ETCHED_LOWERED	= 2;
	/** Tipo de borda: */
	public static final byte TYPE_BEVEL_RAISED		= 3;
	/** Tipo de borda: */
	public static final byte TYPE_BEVEL_LOWERED		= 4;
	/** Tipo de borda: */
	public static final byte TYPE_ROUND_RAISED		= 5;
	/** Tipo de borda: */
	public static final byte TYPE_ROUND_LOWERED		= 6;
	/** Tipo de borda: arredondada (simples). */
	public static final byte TYPE_ROUND_SIMPLE		= 7;


	/** Tipo da borda de linha. */
	protected byte type;

	/** Cor da borda. */
	protected int color;

	/** Cor de preenchimento. */
	protected int fillColor = -1;
	

	/**
	 * 
	 * @throws java.lang.Exception
	 */
	public LineBorder() throws Exception {
		super( 0 );
	}
	

	/**
	 *
	 * @param color
	 * @throws java.lang.Exception
	 */
	public LineBorder( int color ) throws Exception {
		this();
		setColor( color );
	}


	public LineBorder( int color, int type ) throws Exception {
		this( color );

		setType( type );
	}


	/**
	 * 
	 * @param g
	 */
	public void paint( Graphics g ) {
		g.setColor( color );

		switch ( type ) {
			case TYPE_SIMPLE:
				g.drawRect( translate.x, translate.y, size.x - 1, size.y - 1 );

				if ( fillColor >= 0 ) {
					g.setColor( fillColor );
					g.fillRect( translate.x + 1, translate.y + 1, size.x - 2, size.y - 2 );
				}
			break;

			case TYPE_ETCHED_RAISED:
				g.drawRect( translate.x, translate.y, size.x - 2, size.y - 2 );

				g.setColor( changeColorLightness( color, COLOR_DARK_FACTOR ) );
				paintEtchedAuxBorders( g );
			break;

			case TYPE_ETCHED_LOWERED:
				paintEtchedAuxBorders( g );

				g.setColor( changeColorLightness( color, COLOR_DARK_FACTOR ) );
				g.drawRect( translate.x, translate.y, size.x - 2, size.y - 2 );
			break;

			case TYPE_BEVEL_RAISED:
				// borda superior
				g.drawRect( translate.x, translate.y, size.x - 2, 1 );

				// borda esquerda
				g.drawRect( translate.x, translate.y + 2, 1, size.y - 2 );

				// escurece a cor
				g.setColor( changeColorLightness( color, COLOR_DARK_FACTOR ) );
				// borda interna direita
				g.drawRect( translate.x + size.x - 2, translate.y + 1, 0, size.y - 4 );
				// borda interna inferior
				g.drawRect( translate.x + 1, translate.y + size.y - 2, size.x - 3, 0 );

				// escurece novamente a cor
				g.setColor( changeColorLightness( color, COLOR_DARKER_FACTOR ) );
				// borda externa direita
				g.drawRect( translate.x + size.x - 1, translate.y, 0, size.y );
				// borda externa inferior
				g.drawRect( translate.x, translate.y + size.y - 1, size.x - 1, 0 );
			break;

			case TYPE_BEVEL_LOWERED:
				// borda inferior
				g.drawRect( translate.x, translate.y + size.y - 2, size.x - 2, 1 );

				// borda direita
				g.drawRect( translate.x + size.x - 2, translate.y + 1, 1, size.y - 2 );

				// escurece a cor
				g.setColor( changeColorLightness( color, COLOR_DARK_FACTOR ) );
				// borda externa esquerda
				g.drawRect( translate.x, translate.y, 0, size.y );
				// borda externa superior
				g.drawRect( translate.x + 1, translate.y, size.x, 0 );

				// escurece novamente a cor
				g.setColor( changeColorLightness( color, COLOR_DARKER_FACTOR ) );
				// borda interna esquerda
				g.drawRect( translate.x + 1, translate.y + 1, 0, size.y - 3 );
				// borda interna superior
				g.drawRect( translate.x + 2, translate.y + 1, size.x - 4, 0 );
			break;

			case TYPE_ROUND_RAISED:
			case TYPE_ROUND_LOWERED:
				if ( fillColor >= 0 ) {
					g.setColor( fillColor );
					g.fillRect( translate.x + 3, translate.y + 3, size.x - 4, size.y - 5 );

					g.setColor( changeColorLightness( color, -COLOR_DARK_FACTOR ) );
					g.fillRect( translate.x + 1, translate.y + 1, 3, 3 );
					g.fillRect( translate.x + 1, translate.y + 3, 2, size.y - 4 );
					g.fillRect( translate.x + 3, translate.y + 1, size.x - 4, 2 );
				}
				g.setColor( color );

				// borda superior
				g.drawRect( translate.x + 2, translate.y, size.x - 5, 0 );

				// ponto superior esquerdo
				g.drawRect( translate.x + 1, translate.y + 1, 0, 0 );

				// ponto superior direito
				g.drawRect( translate.x + size.x - 2, translate.y + 1, 0, 0 );

				// borda esquerda
				g.drawRect( translate.x, translate.y + 2, 0, size.y - 5 );

				// borda inferior (clara)
				g.drawRect( translate.x + 1, translate.y + size.y - 2, size.x - 3, 0 );

				// escurece a cor
				g.setColor( changeColorLightness( color, COLOR_DARK_FACTOR ) );

				// borda direita
				g.drawRect( translate.x + size.x - 1, translate.y + 2, 0, size.y - 5 );

				// borda inferior (escura)
				g.drawRect( translate.x + 2, translate.y + size.y - 1, size.x - 5, 0 );

				// ponto inferior direito
				g.drawRect( translate.x + size.x - 2, translate.y + size.y - 2, 0, 0 );
			break;

			case TYPE_ROUND_SIMPLE:
				if ( fillColor >= 0 ) {
					g.setColor( fillColor );
					g.fillRect( translate.x + 1, translate.y + 1, size.x - 2, size.y - 2 );
				}
				
				g.setColor( color );

				// borda superior
				g.drawRect( translate.x + 1, translate.y, size.x - 3, 0 );

				// borda esquerda
				g.drawRect( translate.x, translate.y + 1, 0, size.y - 3 );

				// borda direita
				g.drawRect( translate.x + size.x - 1, translate.y + 1, 0, size.y - 3 );

				// borda inferior
				g.drawRect( translate.x + 1, translate.y + size.y - 1, size.x - 3, 0 );
			break;
		}
	}


	private final void paintEtchedAuxBorders( Graphics g ) {
		// horizontal de cima
		g.drawRect( translate.x + 1, translate.y + 1, size.x - 4, 0 );
		// vertical à esquerda
		g.drawRect( translate.x + 1, translate.y + 1, 0, size.y - 4 );
		// horizontal de baixo
		g.drawRect( translate.x, translate.y + size.y - 1, size.x - 2, 0 );
		// vertical à direita
		g.drawRect( translate.x + size.x - 1, translate.y, 0, size.y - 1 );
	}


	/**
	 * Define a cor da borda. No caso de bordas do tipo <i>ETCHED</i> e <i>BEVEL</i>, essa é a cor definida para a sua borda
	 * de cor mais clara (as outras são calculadas automaticamente).
	 * @param color cor da borda, no padrão 0xRRGGBB
	 */
	public void setColor( int color ) {
		this.color = color;
	}


	/**
	 * 
	 * @return
	 */
	public final int getColor() {
		return color;
	}


	/**
	 * Define a cor da borda. No caso de bordas do tipo <i>ETCHED</i> e <i>BEVEL</i>, essa é a cor definida para a sua borda
	 * de cor mais clara (as outras são calculadas automaticamente).
	 * @param color cor da borda, no padrão 0xRRGGBB
	 */
	public void setFillColor( int fillColor ) {
		this.fillColor = fillColor;
	}


	/**
	 *
	 * @return
	 */
	public final int getFillColor() {
		return fillColor;
	}


	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	public Border getCopy() throws Exception {
		final LineBorder border = new LineBorder( color );
		border.set( top, left, bottom, right );
		border.setType( type, false );
		
		return border;
	}


	/**
	 * 
	 * @return
	 */
	public final byte getType() {
		return type;
	}


	/**
	 * 
	 * @param type
	 */
	public final void setType( int type ) {
		setType( type, true );
	}


	/**
	 * 
	 * @param type
	 * @param setBorderThickness
	 */
	public void setType( int type, boolean setBorderThickness ) {
		this.type = ( byte ) type;

		if ( setBorderThickness ) {
			switch ( type ) {
				case TYPE_SIMPLE:
					set( 1, 1, 1, 1 );
				break;

				case TYPE_ETCHED_RAISED:
				case TYPE_ETCHED_LOWERED:
				case TYPE_BEVEL_RAISED:
				case TYPE_BEVEL_LOWERED:
					set( 2, 2, 2, 2 );
				break;

				case TYPE_ROUND_LOWERED:
				case TYPE_ROUND_RAISED:
					set( 4, 5, 3, 3 );
				break;
			}
		}
	}


	public void setState( int state ) {
		super.setState( state );
		
		switch ( type ) {
			case TYPE_SIMPLE:
				switch ( state ) {

				}
			break;

			case TYPE_ETCHED_RAISED:
				switch ( state ) {
					case STATE_PRESSED:
						setType( TYPE_ETCHED_LOWERED );
					break;
				}
			break;
			
			case TYPE_ETCHED_LOWERED:
				switch ( state ) {
					case STATE_FOCUSED:
					case STATE_UNFOCUSED:
						setType( TYPE_ETCHED_RAISED );
					break;
				}
			break;
			
			case TYPE_BEVEL_RAISED:
				switch ( state ) {
					case STATE_PRESSED:
						setType( TYPE_BEVEL_LOWERED );
					break;
				}
			break;
			
			case TYPE_BEVEL_LOWERED:
				switch ( state ) {
					case STATE_FOCUSED:
					case STATE_UNFOCUSED:
						setType( TYPE_BEVEL_RAISED );
					break;
				}
			break;

			case TYPE_ROUND_RAISED:
				switch ( state ) {
					case STATE_PRESSED:
						setType( TYPE_ROUND_LOWERED );
					break;
				}
			break;

			case TYPE_ROUND_LOWERED:
				switch ( state ) {
					case STATE_FOCUSED:
					case STATE_UNFOCUSED:
						setType( TYPE_ROUND_RAISED );
					break;
				}
			break;
		}
	}
	
}
