/*
 * Point.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

/**
 *
 * @author peter
 */
package br.com.nanogames.components.util;

public class Point {
 
	/** Coordenada x do ponto. */
	public int x;
	
	/** Coordenada y do ponto. */
	public int y;
	
	
	public Point() {
	}
	 
	
	public Point( int x, int y ) {
		set( x, y );
	}
    
	
	public Point( Point point ) {
		set( point );
	}	
	
	
	public void set( int x, int y ) {
        this.x = x;
        this.y = y;
	}
	
	
	public final void set( Point point ) {
		set( point.x, point.y );
	}
	 
    
	public final int distanceTo( Point point ) {
		return distanceTo( point.x, point. y );
	}
	
	
	public int distanceTo( int x, int y ) {
		return NanoMath.sqrtInt( ( x - this.x ) * ( x - this.x ) + ( y - this.y ) * ( y - this.y ) );
	}
    
	
	public void normalize() {
        divEquals( getModule() );
	}
	 
    
	public final int dotProduct( Point p ) {
		return ( x * p.x ) + ( y * p.y );
	}
    
    
	public int getModule() {
		return NanoMath.sqrtInt( x * x + y * y );
	}
	 
    
	public void addEquals( Point p ) {
       addEquals( p.x, p.y );
	}
	
	
	public void addEquals( int x, int y ) {
        this.x += x;
        this.y += y;
	}
	
	
	public final Point add( Point p ) {
		return add( p.x, p.y );
	}
	
	
	public Point add( int x, int y ) {
        return new Point( this.x + x, this.y + y );
	}
	 
    
	public final void subEquals( Point p ) {
        subEquals( p.x, p.y );
	}
    
    
	public void subEquals( int x, int y ) {
        this.x -= x;
        this.y -= y;
	}
	
	
	public final Point sub( Point p ) {
        return sub( p.x, p.y );
	}
    
    
	public Point sub( int x, int y ) {
        return new Point( this.x - x, this.y - y );
	}        
	 
    
	public void mulEquals( int m ) {
        x *= m;
        y *= m;
	}
	
	
	public Point mul( int m ) {
        return new Point( x * m, y * m );
	}
	 
    
	public void divEquals( int d ) {
        x /= d;
        y /= d;
	}
	 
    
	public Point div( int d ) {
		//#if DEBUG == "true"
//# 			if ( d == 0 )
//# 				throw new IllegalArgumentException( "Division by zero." );
		//#endif

        return new Point( x / d, y / d );
	}
	
	
	public final boolean equals( Point p ) {
		return equals( p.x, p.y );
	}
    
    
	public boolean equals( int x, int y ) {
		return this.x == x && this.y == y;
	}    
	
	
	/**
	 * Define um vetor com origem em (0,0) a partir do ângulo e módulo recebidos.
	 * 
	 * @param angle ângulo do vetor em graus (sistema de ângulos como no círculo trigonométrico, no sentido anti-horário).
	 * @param module módulo (comprimento) do vetor.
	 */
	public final void setVector( int angle, int module ) {
		module = NanoMath.toFixed( module );
		
		set( NanoMath.toInt( NanoMath.mulFixed( module, NanoMath.cosInt( angle ) ) ),
			 NanoMath.toInt( NanoMath.mulFixed( module, NanoMath.sinInt( angle ) ) ) );
	}
	
	
	//#if DEBUG == "true"
//# 	public String toString() {
//# 		return "(" + x + ", " + y + ")";
//# 	}
	//#endif

}
 
