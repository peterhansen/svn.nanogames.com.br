/*
 * DrawableGroup.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.Point;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

/**
 *
 * @author peter
 */
public class DrawableGroup extends Drawable {
 
	/** Drawables inseridos no grupo. */
	protected final Drawable[] drawables;
	
	/** Quantidade de drawables inseridos no grupo atualmente. */
	protected short activeDrawables;
	 	 
	
    /**
	 * Construtor de DrawableGroup.
	 * @param slots número máximo de drawables.
	 */
	public DrawableGroup( int slots ) {
		drawables = new Drawable[ slots ];
	}
	
	
    /**
     * Insere um drawable no último slot disponível, ou seja, o drawable inserido ocupa inicialmente a posição de desenho
	 * mais à frente de todos do grupo. Equivalente à chamada de <i>insertDrawable(drawable,getUsedSlots()</i>
	 * 
     * @param drawable drawable a ser inserido no grupo.
     * @return o índice onde o drawable foi inserido, ou -1 caso não haja mais slots disponíveis.
	 * 
	 * @see #insertDrawable(Drawable,int)
     */
	public short insertDrawable( Drawable drawable ) {
		//#if DEBUG == "true"
//# 		if ( drawable == null || drawable == this )
//# 			throw new IllegalArgumentException( "can't insert null drawable or self in a group." );
		//#endif
		
		if ( activeDrawables < drawables.length ) {
			drawables[ activeDrawables ] = drawable;
			return activeDrawables++;
		}
		
		//#if DEBUG == "true"
//# 			throw new RuntimeException( "can't insert drawable: group already full." );
		//#else
			return -1;
		//#endif
		
	} // fim do método insertDrawable( Drawable )
	
	
    /**
     * Insere um drawable no índice recebido.
	 * 
     * @param drawable drawable a ser inserido no grupo.
	 * @param index índice onde o drawable será inserido. Caso o índice seja menor que zero, o drawable será inserido
	 * no índice 0. Caso seja maior ou igual à quantidade de drawables já inseridos, ele será inserido após a última
	 * posição ocupada inicialmente. Todos os drawables de índices maior ou igual a <i>index</i> (caso existam) são
	 * deslocados um índice acima.
	 * @return o índice onde o drawable foi efetivamente inserido, ou -1 caso não haja mais slots disponíveis.
	 * 
	 * @see #insertDrawable(Drawable)
	 * @see #setDrawableIndex(int, int)
     */
	public final short insertDrawable( Drawable drawable, int index ) {
		final short oldIndex = insertDrawable( drawable );
		final short newIndex = ( short ) Math.min( oldIndex, index );
		
		if ( newIndex >= 0 && newIndex != oldIndex )
			setDrawableIndex( oldIndex, newIndex );
		
		return newIndex;
	}
	
	
	/**
	 * 
	 * @param d
	 * @return
	 */
	public Drawable removeDrawable( Drawable d ) {
		for ( short i = 0; i < activeDrawables; ++i ) {
			if ( drawables[ i ] == d )
				return removeDrawable( i );
		}
		
		return null;
	}
	 
	
    /**
     * Remove um drawable do grupo.
     * @param index índice do drawable a ser removido no grupo.
     * @return uma referência para o drawable removido, ou null caso o índice seja inválido.
     */
	public Drawable removeDrawable( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
			
			final Drawable removed = drawables[ index ];
			
			if ( removed != null ) {
				final int LIMIT = drawables.length - 1;
				for ( int i = index; i < LIMIT; ++i )
					drawables[ i ] = drawables[ i + 1 ];
				
				--activeDrawables;
				
				// anula a referência para o último item do array, para evitar vazamento de memória
				drawables[ LIMIT ] = null;
			}
			
			return removed;
			
		//#if DEBUG == "true"
//# 		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 			
//# 			return null;
//# 		}
		//#endif
	} // fim do método removeDrawable( int )


	/**
	 * Remove todos os drawables do grupo.
	 */
	public void removeAllDrawables() {
		while ( getUsedSlots() > 0 )
			removeDrawable( 0 );
	}

	
	/** 
	 * Substitui o drawable existente no índice passado como parâmetro
     * @param drawable Drawable a ser inserido no grupo
	 * @param index Índice onde o drawable será inserido
	 * 
	 * @see #insertDrawable(Drawable, int)
	 * @see #removeDrawable(int)
	 */
	public final void setDrawable( Drawable drawable, int index ) {
		removeDrawable( index );
		AppMIDlet.gc();
		insertDrawable( drawable, index );
	}

	
    /**
	 * Altera a ordem de desenho de um drawable. Todos os drawables de índice <i>newIndex</i> e superior (caso existam)
	 * são deslocados um índice acima.
	 * 
	 * @param oldIndex índice atual do drawable. Caso seja menor que zero, o valor considerado é zero.
	 * @return <i>true</i>, caso a troca seja feita com sucesso, e <i>false</i> caso contrário. A faixa de valores válidos 
	 * para ambos os argumentos é de 0 (zero) ao número de elementos inseridos menos um. Exemplo: caso haja 5 drawables 
	 * no grupo, os valores válidos são 0, 1, 2, 3 e 4. É importante notar que o número de elementos inseridos não é
	 * necessariamente igual à capacidade do grupo - pode haver apenas um elemento inserido num grupo com capacidade para
	 * 10 drawables, por exemplo.
	 * @param newIndex novo índice do drawable. Caso seja menor que zero, o valor considerado é zero.
	 */
	public boolean setDrawableIndex( int oldIndex, int newIndex ) {
		//#if DEBUG == "true"
//# 		oldIndex = Math.max( oldIndex, 0 );
//# 		newIndex = Math.max( newIndex, 0 );
//# 		
//# 		if ( oldIndex < activeDrawables && newIndex < activeDrawables ) {
//# 			try {
		//#endif
				
				final Drawable old = drawables[ oldIndex ];

				if ( oldIndex < newIndex ) {
					for ( int i = oldIndex; i < newIndex; ++i )
						drawables[ i ] = drawables[ i + 1 ];
				} else {
					for ( int i = oldIndex; i > newIndex; --i )
						drawables[ i ] = drawables[ i - 1 ];
				}

				drawables[ newIndex ] = old;
				return true;
				
		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
//# 
//# 				return false;
//# 			}
//# 		}
//# 		return false;
//# 		
		//#endif
	}
	
	 
    /**
     * 
     * @param index 
     * @return 
     */
	public Drawable getDrawable( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
			return drawables[ index ];
		
		//#if DEBUG == "true"
//# 		} catch ( ArrayIndexOutOfBoundsException e ) {
//# 			e.printStackTrace();
//# 			
//# 			return null;
//# 		}
		//#endif
	} // fim do método getDrawable( int )
	
	
	/**
	 * Obtém o drawable presente numa posição x,y do grupo.
	 * 
	 * @param x posição x do ponteiro.
	 * @param y posição y do ponteiro.
	 * @return o drawable presente na posição x,y (pode ser o próprio grupo), ou <code>null</code> caso o ponto não
	 * esteja dentro da área do grupo.
	 */
	public Drawable getDrawableAt( int x, int y ) {
		if ( contains( x, y ) ) {
			x -= position.x;
			y -= position.y;
			
			for ( int i = activeDrawables - 1; i >= 0; --i ) {
				return drawables[ i ].getDrawableAt( x, y );
			}
			
			return this;
		}
		
		return null;
	}	


	protected void paint( Graphics g ) {
		for ( int i = 0; i < activeDrawables; ++i )
			drawables[ i ].draw( g );
	} // fim do método paint( Graphics )
	 
	
	/**
	 * 
	 * @return capacidade máxima do grupo.
	 */
	public int getTotalSlots() {
		return drawables.length;
	}
	
	
	/**
	 * 
	 * @return quantidade de drawables inseridos no grupo.
	 */
	public short getUsedSlots() {
		return activeDrawables;
	}

	
	public boolean mirror( int mirrorType ) {
		super.mirror( mirrorType );
		
		if ( ( mirrorType & TRANS_MIRROR_H ) != TRANS_NONE ) {
			for ( int i = 0; i < activeDrawables; ++i ) {
				final Drawable d = drawables[ i ];
				final int previousX = d.getRefPixelX();
				
				d.mirror( mirrorType );
				d.setRefPixelPosition( size.x - previousX, d.getRefPixelY() );
			} // fim for ( int i = 0; i < activeDrawables; ++i )
		}
		
		if ( ( mirrorType & TRANS_MIRROR_V ) != TRANS_NONE ) {
			for ( int i = 0; i < activeDrawables; ++i ) {
				final Drawable d = drawables[ i ];
				final int previousY = d.getRefPixelY();
				
				d.mirror( mirrorType );
				d.setRefPixelPosition( d.getRefPixelX(), size.y - previousY );
			} // fim for ( int i = 0; i < activeDrawables; ++i )				
		}
		
		return true;
	} // fim do método mirror( int )

	
	public boolean rotate( int rotationType ) {
		super.rotate( rotationType );
		
		switch ( rotationType ) {
			case TRANS_ROT90:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final Point previousRefPos = new Point( d.getRefPixelPosition() );
					
					d.rotate( rotationType );
					d.setRefPixelPosition( size.y - previousRefPos.y + ( size.x - size.y ), previousRefPos.x );
				}
			break;
			
			case TRANS_ROT180:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final int previousX = d.getRefPixelX();
					final int previousY = d.getRefPixelY();
					
					d.rotate( rotationType );
					d.setRefPixelPosition( size.x - previousX, size.y - previousY );	
				}				
			break;
			
			case TRANS_ROT270:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final Point previousRefPos = new Point( d.getRefPixelPosition() );
					
					d.rotate( rotationType );
					d.setRefPixelPosition( previousRefPos.y, size.x - previousRefPos.x + ( size.y - size.x ) );
				}				
			break;
		}
		
		return true;
	} // fim do método rotate( int )
	
}

