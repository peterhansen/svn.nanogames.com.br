/*
 * Copyright 2008 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
package br.com.nanogames.components.userInterface.form.layouts;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.util.Point;


/**
 * Flows elements in a row so they can spill over when reaching line end
 *
 * @author Nir Shabi
 */
public class FlowLayout extends Layout {
	
	public static final byte AXIS_HORIZONTAL	= 0;
	
	public static final byte AXIS_VERTICAL		= 1;
	
	private byte axis;

	/** Alinhamento dos componentes em cada linha. Valores válidos:
	 * <ul>
	 * <li>Drawable.ANCHOR_LEFT</li>
	 * <li>Drawable.ANCHOR_HCENTER</li>
	 * <li>Drawable.ANCHOR_RIGHT</li>
	 * <li>Drawable.ANCHOR_TOP</li>
	 * <li>Drawable.ANCHOR_VCENTER</li>
	 * <li>Drawable.ANCHOR_BOTTOM</li>
	 * </ul>
	 */
	private byte alignment = Drawable.ANCHOR_LEFT;

	/** Espaçamento em pixels entre componentes. */
	public final Point gap = new Point();

	/** Espaçamento inicial dos componentes de cada linha ou coluna. */
	public final Point start = new Point();

	
	/**
	 * Cria um novo FlowLayout, com eixo horizontal.
	 */
	public FlowLayout() {
		this( AXIS_HORIZONTAL );
	}
	

	/** 
	 * Creates a new instance of FlowLayout with left alignment
	 * @see #FlowLayout(int)
	 */
	public FlowLayout( byte axis ) {
		//#if DEBUG == "true"
//# 			if ( axis != AXIS_HORIZONTAL && axis != AXIS_VERTICAL )
//# 				throw new IllegalArgumentException( "invalid axis value: " + axis );
		//#endif
		
		this.axis = axis;
	}


	/** 
	 * Cria um novo FlowLayout.
	 *
	 * @param axis
	 * @param alignment Alinhamento dos componentes em cada linha. Valores válidos:
	 * <ul>
	 * <li>Drawable.ANCHOR_LEFT</li>
	 * <li>Drawable.ANCHOR_HCENTER</li>
	 * <li>Drawable.ANCHOR_RIGHT</li>
	 * <li>Drawable.ANCHOR_TOP</li>
	 * <li>Drawable.ANCHOR_VCENTER</li>
	 * <li>Drawable.ANCHOR_BOTTOM</li>
	 * </ul>
	 * @see #alignment
	 * @see #FlowLayout()
	 */
	public FlowLayout( byte axis, int alignment ) {
		this( axis );
		this.alignment = ( byte ) alignment;
	}


	/**
	 * @inheritDoc
	 */
	public void layoutContainer( Container container ) {
		int width = 0;
		int height = 0;
		int rowWidth = 0;
		int rowHeight = 0;
		int x = start.x;
		int y = start.y;

		int maxWidth = container.getLayoutWidth() - start.x;
		int maxHeight = container.getLayoutHeight() - start.y;

		boolean canScrollX = container.isScrollableX();
		boolean canScrollY = container.isScrollableY();
		boolean scrollXActive = false;
		boolean scrollYActive = false;
		
		final int axisAlignment = ( axis == AXIS_HORIZONTAL ? alignment & Drawable.ANCHOR_VERTICAL_MASK : alignment & Drawable.ANCHOR_HORIZONTAL_MASK );

		final short numOfcomponents = container.getComponentCount();
		for ( int i = 0; i < numOfcomponents; ++i ) {
			final Component c = container.getComponentAt( i );
			final Point preferredSize = new Point( c.getPreferredSize( new Point( maxWidth, maxHeight ) ) );
			
			if ( preferredSize.x > maxWidth )
				preferredSize.x = maxWidth;
			
			if ( preferredSize.y > maxHeight )
				preferredSize.y = maxHeight;

			c.setSize( preferredSize.x, preferredSize.y );

			if ( axis == AXIS_HORIZONTAL ) {
				if ( !canScrollX && x + preferredSize.x > maxWidth ) {
					// ultrapassou a largura máxima; pula para próxima linha
					x = preferredSize.x + gap.x;

					// tratamento especial para evitar que o 1º componente pule linha
					if ( i > 0 ) {
						y += rowHeight + gap.y;
						rowHeight = 0;
					}

					switch ( axisAlignment ) {
						case Drawable.ANCHOR_VCENTER:
							c.setPosition( start.x, ( maxHeight - c.getHeight() ) >> 1 );
						break;

						case Drawable.ANCHOR_BOTTOM:
							c.setPosition( start.x, maxHeight - c.getHeight() );
						break;

						default:
							c.setPosition( start.x, y );
					}
				} else {
					// componente ainda cabe na mesma linha
					switch ( axisAlignment ) {
						case Drawable.ANCHOR_VCENTER:
							c.setPosition( x, ( maxHeight - c.getHeight() ) >> 1 );
						break;

						case Drawable.ANCHOR_BOTTOM:
							c.setPosition( x, maxHeight - c.getHeight() );
						break;

						default:
							c.setPosition( x, y );
					}

					x += preferredSize.x + gap.x;
				}
				
				if ( preferredSize.y > rowHeight )
					rowHeight = preferredSize.y;

				if ( x > width )
					width = x;		
				
				final int Y_TEMP = y + rowHeight + gap.y;
				if ( Y_TEMP > height )
					height = Y_TEMP;				
			} else {
				// eixo vertical
				if ( !canScrollY && y + preferredSize.y > maxHeight ) {
					// ultrapassou a altura máxima; pula para próxima coluna
					y = preferredSize.y + gap.y;

					// tratamento especial para evitar que o 1º componente pule coluna
					if ( i > 0 ) {
						x += rowWidth + gap.x;
						rowWidth = 0;
					}

					switch ( axisAlignment ) {
						case Drawable.ANCHOR_HCENTER:
							c.setPosition( ( maxWidth - c.getWidth() ) >> 1, start.y );
						break;

						case Drawable.ANCHOR_RIGHT:
							c.setPosition( maxWidth - c.getWidth(), start.y );
						break;

						default:
							c.setPosition( x, start.y );
					}

				} else {
					// componente ainda cabe na mesma coluna
					switch ( axisAlignment ) {
						case Drawable.ANCHOR_HCENTER:
							c.setPosition( ( maxWidth - c.getWidth() ) >> 1, y );
						break;

						case Drawable.ANCHOR_RIGHT:
							c.setPosition( maxWidth - c.getWidth(), y );
						break;

						default:
							c.setPosition( x, y );
					}

					y += preferredSize.y + gap.y;
					if ( preferredSize.x > width )
						width = preferredSize.x;
				}				
				
				if ( canScrollY && !scrollYActive && ( y + c.getHeight() > container.getHeight() ) ) {
					// adiciona barra de rolagem vertical - refaz os cálculos com nova largura
					scrollYActive = true;

					if ( container.getScrollBarV() != null )
						maxWidth -= container.getScrollBarV().getWidth() - gap.x;
//					maxHeight = Integer.MAX_VALUE; // TODO teste

					// i será incrementado para zero automaticamente após continue
					i = -1;
					width = 0;
					height = 0;
					rowHeight = 0;
					rowWidth = 0;
					x = start.x;
					y = start.y;

					// reinicia o loop
					continue;
				}	

				if ( canScrollX && !scrollXActive && x + c.getWidth() > container.getWidth() ) {
					// adiciona barra de rolagem horizontal - refaz os cálculos com nova altura
					scrollXActive = true;

//					maxWidth = Integer.MAX_VALUE; // TODO teste
					maxHeight -= container.getScrollBarH().getHeight();

					// i será incrementado para zero automaticamente após continue
					i = -1;
					width = 0;
					height = 0;
					rowHeight = 0;
					rowWidth = 0;
					x = start.x;
					y = start.y;

					// reinicia o loop
					continue;
				}				
				
				if ( preferredSize.x > rowWidth )
					rowWidth = preferredSize.x;

				if ( y > height )
					height = y;				
				
				final int X_TEMP = x + rowWidth + gap.x;
				if ( X_TEMP > width )
					width = X_TEMP;
			}
		} // fim for ( int i = 0; i < numOfcomponents; ++i )
		
		container.setMaximumSize( new Point( width, height ) );
	}


	public Point calcPreferredSize( final Container container, Point maxSize ) {
		int width = 0;
		int height = 0;
		int rowWidth = 0;
		int rowHeight = 0;
		int x = start.x;
		int y = start.y;

		boolean canScrollX = container.isScrollableX();
		boolean canScrollY = container.isScrollableY();
		boolean scrollXActive = false;
		boolean scrollYActive = false;
		
		
		if ( maxSize == null )
			maxSize = new Point( Integer.MAX_VALUE, Integer.MAX_VALUE );		

		final short numOfcomponents = container.getComponentCount();
		for ( int i = 0; i < numOfcomponents; ++i ) {
			final Component c = container.getComponentAt( i );
			final Point preferredSize = c.getPreferredSize( maxSize );
			
			if ( axis == AXIS_HORIZONTAL ) {
				if ( x + preferredSize.x > maxSize.x ) {
					// ultrapassou a largura máxima; pula para próxima linha
					x = preferredSize.x + gap.x;

					// tratamento especial para evitar que o 1º componente pule linha
					if ( i > 0 ) {
						y += rowHeight + gap.y;
						rowHeight = 0;
					}
				} else {
					// componente ainda cabe na mesma linha
					x += preferredSize.x + gap.x;
				}
				
				if ( preferredSize.y > rowHeight )
					rowHeight = preferredSize.y;

				if ( x > width )
					width = x;		
				
				final int Y_TEMP = y + rowHeight + gap.y;
				if ( Y_TEMP > height )
					height = Y_TEMP;				
			} else {
				// eixo vertical
				if ( y + preferredSize.y > maxSize.y ) {
					// ultrapassou a altura máxima; pula para próxima coluna
					y = preferredSize.y + gap.y;

					// tratamento especial para evitar que o 1º componente pule coluna
					if ( i > 0 ) {
						x += rowWidth + gap.x;
						rowWidth = 0;
					}
				} else {
					// componente ainda cabe na mesma linha
					y += preferredSize.y + gap.y;
					if ( preferredSize.x > width )
						width = preferredSize.x;
				}
				
				if ( canScrollX && !scrollXActive && x + c.getWidth() > container.getWidth() ) {
					// adiciona barra de rolagem horizontal - refaz os cálculos com nova altura
					scrollXActive = true;

					maxSize.y -= container.getScrollBarH().getHeight();

					// i será incrementado para zero automaticamente após continue
					i = -1;
					width = 0;
					height = 0;
					rowHeight = 0;
					rowWidth = 0;
					x = start.x;
					y = start.y;

					// reinicia o loop
					continue;
				}				
				
				if ( canScrollY && !scrollYActive && y + c.getHeight() > container.getHeight() ) {
					// adiciona barra de rolagem vertical - refaz os cálculos com nova largura
					scrollYActive = true;

					if ( container.getScrollBarV() != null )
						maxSize.x -= container.getScrollBarV().getWidth();

					// i será incrementado para zero automaticamente após continue
					i = -1;
					width = 0;
					height = 0;
					rowHeight = 0;
					rowWidth = 0;
					x = start.x;
					y = start.y;

					// reinicia o loop
					continue;
				}				
				
				if ( preferredSize.x > rowWidth )
					rowWidth = preferredSize.x;

				if ( y > height )
					height = y;				
				
				final int X_TEMP = x + rowWidth + gap.x;
				if ( X_TEMP > width )
					width = X_TEMP;
			}
		}
		
		return new Point( width, height );
	}
	
}
