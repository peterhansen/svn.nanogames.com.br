/*
 * AppMIDlet.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */
package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

//#if J2SE == "false"
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;
//#else
//#endif

//#if QUALCOMM_API == "true"
//# import com.qualcomm.qis.helper.LicenseString;
//# import license.DecrementUseCallback;
//# import license.License;
//# import license.LicenseAgent;
//# import license.LicenseCallback;
//# import license.NullCallbackException;
//# import license.PurchaseResponse;
//#endif


/**
 *
 * @author peter
 */
public abstract class AppMIDlet 
		//#if J2SE == "false"
			extends MIDlet
		//#endif

		//#if QUALCOMM_API == "true"
//# 			implements LicenseCallback, DecrementUseCallback
		//#endif
{

	/** Duração máxima padrão de um frame, em milisegundos */
	protected static final byte MAX_FRAME_TIME_DEFAULT = 100;
	
	protected final short MAX_FRAME_TIME;
	
	protected static AppMIDlet instance;

	/** Fontes utilizadas no aplicativo. */
	protected ImageFont[] FONTS;
	
	/** textos utilizados no aplicativo */
	protected static String[] texts;
	
	// perfis de fabricantes
	public static final byte VENDOR_GENERIC = 0;
	public static final byte VENDOR_NOKIA = 1;
	public static final byte VENDOR_MOTOROLA = 2;
	public static final byte VENDOR_SAMSUNG = 3;
	public static final byte VENDOR_LG = 4;
	public static final byte VENDOR_SONYERICSSON = 5;
	public static final byte VENDOR_SIEMENS = 6;
	public static final byte VENDOR_HTC = 7;
	public static final byte VENDOR_BLACKBERRY = 8;
	public static final byte VENDOR_VODAFONE = 9;
	public static final byte VENDOR_SAGEM_GRADIENTE = 10;
	public static final byte VENDOR_INTELBRAS = 11;
	
	/** perfil do fabricante detectado */
	private static byte VENDOR;
	
	protected ScreenManager manager;
	
	/** Mutex utilizado para sincronizar o método setScreen(int). */
	protected static final Mutex mutexScreen = new Mutex();
	
	/** Índice da tela ativa atualmente. */
	protected static byte currentScreen = -1;

	protected static byte language;


	//#if QUALCOMM_API == "true"
//# 		/** Licença do Plaza Retail License Agent Kit. */
//# 		protected License license;
//# 
//# 		/** Tipo de mensagem do License Agent Kit: informação. */
//# 		protected static final byte MSG_TYPE_INFO			= 0;
//# 
//# 		/** Tipo de mensagem do License Agent Kit: aviso. */
//# 		protected static final byte MSG_TYPE_WARNING		= 1;
//# 
//# 		/** Tipo de mensagem do License Agent Kit: confirmação. */
//# 		protected static final byte MSG_TYPE_CONFIRMATION	= 2;
//# 
//# 		/** Tipo de mensagem do License Agent Kit: erro. */
//# 		protected static final byte MSG_TYPE_ERROR			= 3;
//# 
//# 		/** Tipo de ação após exibição de mensagem do License Agent Kit: nenhuma. */
//# 		protected static final byte MSG_ACTION_NONE			= 0;
//# 
//# 		/** Tipo de ação após exibição de mensagem do License Agent Kit: inicia a aplicação. */
//# 		protected static final byte MSG_ACTION_START		= 1;
//# 
//# 		/** Tipo de ação após exibição de mensagem do License Agent Kit: sai da aplicação. */
//# 		protected static final byte MSG_ACTION_EXIT			= 2;
//# 
//# 		/** Tipo de ação após exibição de mensagem do License Agent Kit: compra mais créditos. */
//# 		protected static final byte MSG_ACTION_PURCHASE		= 3;
	//#endif



	/**
	 * Instancia o MIDlet, forçando um fabricante específico e dessa forma evitando os testes de auto-detecção.
	 * @param vendor índice do fabricante. Utilizar valores negativos para auto-detectar.
	 * @param maxFrameTime 
	 * @see AppMIDlet()
	 */
	protected AppMIDlet( int vendor, int maxFrameTime ) {
		MAX_FRAME_TIME = ( short ) maxFrameTime;

		byte deviceVendor = VENDOR_GENERIC;

		//#if TRY_CATCH_ALL == "true"
//#  		try {
		//#endif				

		if ( vendor < 0 ) {
			//#if BLACKBERRY_API == "true"
//#  				deviceVendor = VENDOR_BLACKBERRY;
			//#else
				String platform = System.getProperty( "microedition.platform" );

				if ( platform != null ) {
					/** strings que indicam cada fabricante (em algumas marcas, pode haver mais de uma possível string, dependendo do modelo) */
					final String[][] VENDOR_AGENT = new String[][] {
						{}, // genérico
						{ "nokia" }, // Nokia
						{ "mot-", "motorola" }, // Motorola
						{ "samsung", "sec-" }, // Samsung
						{ "lg-" }, // LG
						{ "sonyericsson" }, // SonyEricsson
						{ "sie-", "benq-" }, // Siemens
						{ "intent", "htc" }, // HTC, Qtek
						{ "rim" }, // Blackberry
						{ "vodafone" }, // Vodafone
						{}, // Sagem/Gradiente
					};

					platform = platform.toLowerCase();
					byte i, j;

					for ( i = 0; i < VENDOR_AGENT.length && deviceVendor == VENDOR_GENERIC; ++i ) {
						for ( j = 0; j < VENDOR_AGENT[i].length; ++j ) {
							if ( platform.indexOf( VENDOR_AGENT[i][j] ) >= 0 ) {
								// encontrou o fabricante (valor da variável não é atribuída aqui porque variáveis "final"
								// não podem ser atribuídas em loops, mesmo com break)
								deviceVendor = i;
								break;
							}
						} // fim for ( j = 0; j < VENDOR_AGENT[ i ].length; ++j )
					} // fim for ( i = 0; i < TOTAL_VENDORS; ++i )
				} // fim if ( platform != null )

				if ( deviceVendor == VENDOR_GENERIC ) {
					// aparelho não informou corretamente o seu fabricante; tenta a detecção através da detecção de APIs
					// específicas de cada fabricante. Várias classes para o mesmo fabricante podem ser testados até se encontrar
					// um válido, pois as APIs podem variar entre modelos e séries de modelos de cada fabricante.
					final byte[] vendors = new byte[]{
						VENDOR_LG,
						VENDOR_SAMSUNG,
						VENDOR_SIEMENS,
						VENDOR_MOTOROLA,
						VENDOR_SONYERICSSON,
						VENDOR_VODAFONE,
					};

					for ( int i = 0; i < vendors.length; ++i ) {
						if ( checkVendor( vendors[i] ) ) {
							deviceVendor = vendors[i];
							break;
						}
					}
				} // fim if ( vendor == VENDOR_GENERIC )
			//#endif
		} else {
			// força a utilização de um fabricante específico
			deviceVendor = ( byte ) vendor;
		}

		//#if TRY_CATCH_ALL == "true"
//#  		} catch ( Throwable e ) {
//#  			e.printStackTrace();
//#  		}
		//#endif			

		VENDOR = deviceVendor;
	} // fim do construtor AppMIDlet( int )
	
	
	/**
	 * Carrega textos a partir de um arquivo de recurso para um String[]. Cada linha de texto do arquivo corresponde a 
	 * um texto a ser carregado. Para utilizar quebras de linha no texto, deve-se utilizar o caracter barra invertida (\).
	 * 
	 * @param texts array de strings onde serão carregados os textos. A quantidade de textos a serem carregados corresponde
	 * ao tamanho do array.
	 * @param resourceFilename caminho do arquivo que contém os textos do jogo.
	 * @throws java.lang.Exception caso texts seja null, o caminho do arquivo seja inválido, detectar o fim do arquivo antes de
	 * se ler todos os textos, ou ocorra erro na leitura do arquivo.
	 */	
	public static final void loadTexts( final String[] texts, String resourceFilename ) throws Exception {
		//#if TRY_CATCH_ALL == "true"
//#  		try {
		//#endif				

		// para se utilizar essa variável dentro da classe interna descrita abaixo, é necessário que ela seja final
		final int TOTAL_TEXTS = texts.length;

		openJarFile( resourceFilename, new Serializable() {

			public final void write( DataOutputStream output ) throws Exception {
			}


			public final void read( DataInputStream input ) throws Exception {
				final char[] buffer = new char[ 2048 ];
				int bufferIndex = 0;

				short currentString = 0;

				try {
					while ( currentString < TOTAL_TEXTS ) {
						// se o texto estiver em UTF8, o converte
						byte b = input.readByte(); // TODO não ler de byte em byte para
						if ( b < 0 )
							b = ( byte ) ( b - 0x100 );

						char c = ( char ) b;

						if ( c >= 0xC0 ) {
							c = ( char ) ( ( ( c << 8 ) | input.readUnsignedByte() ) - 0xc2c0 );
						} else if ( c >= 0x80 ) {
							c = ( char ) ( c - 0xc200 );
						}

						switch ( c ) {
							case '\r':
								break;

							case '\n':
								// chegou ao final de uma string. Caso a linha esteja vazia, utiliza uma string vazia (não
								// fazer esse teste lança uma exceção)
								texts[currentString++] = ( bufferIndex > 0 ) ? new String( buffer, 0, bufferIndex ) : "";

								bufferIndex = 0;
								break;

							case '\\':
								buffer[bufferIndex++] = '\n';
								break;

							default:
								buffer[bufferIndex++] = c;
						} // fim switch ( c )
					} // fim while ( currentString < TEXT_TOTAL )				
				} catch ( EOFException eof ) {
					if ( currentString < TOTAL_TEXTS ) {
						// chegou ao final do arquivo antes de ler todos os textos
						//#if DEBUG == "true"
//#  						throw new Exception( "erro ao ler textos: fim de arquivo detectado antes da leitura de todos os textos." );
						//#else
						throw eof;
					//#endif
					} // fim if ( currentString < TOTAL_TEXTS )
				} // fim catch ( EOFException )
			} // fim do método read( DataInputStream )
		} );

	//#if TRY_CATCH_ALL == "true"
//#  		} catch ( Throwable e ) {
//#  			e.printStackTrace();
//#  		}
	//#endif				
	}

	
	// TODO verificar uso de notifyPaused e etc. Exemplo: Motorola A1200 não está suspendendo ao receber SMS - aparentemente, não está recebendo eventos. Jogos da Gameloft, porém, conseguem suspender ao receber SMS nesse aparelho.
	/**
	 * Carrega os textos do aplicativo a partir de um arquivo de recurso. Cada linha de texto do arquivo corresponde a 
	 * um texto do aplicativo. Para utilizar quebras de linha no texto, deve-se utilizar o caracter barra invertida (\).
	 * 
	 * @param totalTexts quantidade total de textos a serem carregados.
	 * @param resourceFilename caminho do arquivo que contém os textos do jogo.
	 * @throws java.lang.Exception caso o caminho do arquivo seja inválido, detectar o fim do arquivo antes de
	 * se ler todos os textos, ou ocorra erro na leitura do arquivo.
	 */
	protected final void loadTexts( int totalTexts, String resourceFilename ) throws Exception {
		texts = null;
		texts = new String[ totalTexts ];
		loadTexts( texts, resourceFilename );
	} // fim do método loadTexts( String )


	/**
	 * Instancia o AppMIDlet, detectando automaticamente o fabricante do aparelho. Equivalente à chamada do construtor
	 * AppMIDLet( -1 )
	 * @see #AppMIDlet(int)
	 */
	public AppMIDlet() {
		this( -1, MAX_FRAME_TIME_DEFAULT );
	} // fim do construtor AppMIDlet()


	//#if BLACKBERRY_API == "false"
	/**
	 * Verifica qual o fabricante do aparelho, através da tentativa de detecção de APIs específicas.
	 * @param vendor índice do fabricante a ser verificado.
	 * @return true, se o fabricante pôde ser determinado através da detecção de API específica, ou false caso contrário.
	 */
	private final boolean checkVendor( byte vendor ) {
		String[] classes = null;
		switch ( vendor ) {
			case VENDOR_GENERIC:
			case VENDOR_NOKIA:
				return false;

			case VENDOR_MOTOROLA:
				classes = new String[]{
					"com.motorola.phonebook.PhoneBookRecord",
					"com.motorola.phone.Dialer",
					"com.motorola.multimedia.Lighting",
					"com.motorola.extensions.ScalableImage",
					"com.motorola.funlight.FunLight",
					"com.motorola.pim.Contact",
					"com.motorola.graphics.j3d.ActionTable",
				};
				break;

			case VENDOR_SAMSUNG:
				classes = new String[]{
					"com.samsung.util.Acceleration",
					"com.samsung.util.AudioClip",
					"com.samsung.immersion.VibeTonz"
				};
				break;

			case VENDOR_LG:
				classes = new String[]{
					"mmpp.media.BackLight",
					"mmpp.lang.MathFP",
					"mmpp.media.MediaPlayer",
					"mmpp.phone.ContentsManager",
					"mmpp.microedition.lcdui.TextFieldX",
				};
				break;

			case VENDOR_SONYERICSSON:
				classes = new String[]{
					"com.sonyericsson.mmedia.AMRPlayer",
					"com.sonyericsson.util.TempFile"
				};
				break;

			case VENDOR_SIEMENS:
				classes = new String[]{
					"com.siemens.mp.lcdui.Image",
					"com.siemens.mp.game.Light",
					"com.siemens.mp.io.Connection",
					"com.siemens.mp.wireless.messaging.MessageConnection",
				};
				break;

			case VENDOR_VODAFONE:
				classes = new String[]{
					"com.vodafone.v10.system.device.DeviceControl",
				};
				break;
		}

		for ( int i = 0; i < classes.length; ++i ) {
			try {
				Class.forName( classes[i] );

				// se chegou nessa linha (ou seja, não lançou exceção), é sinal de que encontrou a String
				return true;
			} catch ( Exception e ) {
			}
		} // fim for ( int i = 0; i < classes.length; ++i )

		return false;
	}
	//#endif


	/**
	 * Indica se uma base de dados já existe.
	 * 
	 * @param databaseName nome da base de dados.
	 * @param expectedSlots quantidade de slots esperada da base. Valores menores ou iguais a zero ignoram este teste, ou
	 * seja, apenas indica se a base existe.
	 * 
	 * @return true, caso a base já exista e tenha a quantidade esperada de slots, e false caso contrário.
	 * 
	 * @see #createDatabase(String, int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final boolean hasDatabase( String databaseName, int expectedSlots ) {
		//#if J2SE == "false"
			final String[] names = RecordStore.listRecordStores();
			RecordStore rs = null;

			if ( names != null ) {
				for ( int i = 0; i < names.length; ++i ) {
					if ( names[ i ].equals( databaseName ) ) {
						try {
							if ( expectedSlots > 0 ) {
								rs = RecordStore.openRecordStore( databaseName, false );
								return ( rs.getNumRecords() == expectedSlots );
							}

							// se chegou nessa linha, significa que não importa saber a quantidade de slots na base de dados,
							// somente saber se ela existe.
							return true;
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//#  						e.printStackTrace();
							//#endif

							return false;
						} finally {
							if ( rs != null ) {
								try {
									rs.closeRecordStore();
								} catch ( Exception e ) {
								//#if DEBUG == "true"
//#  								e.printStackTrace();
								//#endif
								}
							} // fim if ( rs != null )
						} // fim finally
					} // fim if ( names[ i ].equals( databaseName ) )
				} // fim for ( int i = 0; i < names.length; ++i )
			} // fim if ( names != null )
		//#else
		//#endif

		return false;
	} // fim do método hasDatabase( String, int )


	/**
	 * Cria uma base de dados com uma quantidade pré-determinada de slots para gravação. Equivalente à chamada de <code>
	 * createDatabase( dataBaseName, numSlots, false )</code>.
	 *
	 * @param dataBaseName nome da base de dados.
	 * @param numSlots número de slots disponíveis para gravação. Caso a base já exista, mas a quantidade de slots seja
	 * diferente, a base é recriada com a nova quantidade de slots.
	 *
	 * @throws javax.microedition.rms.RecordStoreFullException caso não haja espaço livre disponível para criar a base de dados.
	 * @throws java.lang.Exception caso ocorra outro tipo de erro na criação da base de dados.
	 *
	 * @return boolean indicando se a base de dados foi criada. Caso ela já existisse antes, retorna false.
	 *
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final boolean createDatabase( String dataBaseName, int numSlots ) throws Exception {
		return createDatabase( dataBaseName, numSlots, false );
	}


	/**
	 * Cria uma base de dados com uma quantidade pré-determinada de slots para gravação.
	 * 
	 * @param dataBaseName nome da base de dados.
	 * @param numSlots número de slots disponíveis para gravação. Caso a base já exista, mas a quantidade de slots seja 
	 * diferente, a base é recriada com a nova quantidade de slots.
	 * @param eraseIfGreater indica se a base deve ser recriada se ela existir, mas tiver MAIS que <code>numSlots</code> slots.
	 * 
	 * @throws javax.microedition.rms.RecordStoreFullException caso não haja espaço livre disponível para criar a base de dados.
	 * @throws java.lang.Exception caso ocorra outro tipo de erro na criação da base de dados.
	 * 
	 * @return boolean indicando se a base de dados foi criada. Caso ela já existisse antes, retorna false.
	 * 
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final boolean createDatabase( String dataBaseName, int numSlots, boolean eraseIfGreater ) throws Exception {
		//#if J2SE == "false"
			RecordStore rs = null;

			try {
				if ( hasDatabase( dataBaseName, 0 ) ) {
					rs = RecordStore.openRecordStore( dataBaseName, false );
					if ( numSlots == rs.getNumRecords() ) {
						// base já existe, com a mesma quantidade de slots
						return false;
					} else if ( numSlots > rs.getNumRecords() ) {
						// há menos slots salvos do que o desejado; apenas insere novos slots
						for ( int i = rs.getNumRecords(); i < numSlots; ++i )
							rs.addRecord( null, 0, 0 );

						return false;
					} else if ( eraseIfGreater ) {
						// quantidade de slots é maior; apaga a base antiga e a recria com a nova quantidade
						rs.closeRecordStore();

						RecordStore.deleteRecordStore( dataBaseName );
					} else {
						return false;
					}
				} // fim if ( hasDatabase( dataBaseName, 0 ) )

				rs = RecordStore.openRecordStore( dataBaseName, true );

				for ( int i = 0; i < numSlots; ++i ) {
					rs.addRecord( null, 0, 0 );
				}

				return true;
			} catch ( RecordStoreFullException rsfe ) {
				//#if DEBUG == "true"
//#  				throw new RecordStoreFullException( "Erro ao criar base de dados: não há espaço disponível." );
				//#else
				throw rsfe;
				//#endif
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//#  			e.printStackTrace();
				//#endif

				throw e;
			} finally {
				if ( rs != null ) {
					try {
						rs.closeRecordStore();
					} catch ( Exception e ) {
					//#if DEBUG == "true"
//#  					e.printStackTrace();
					//#endif
					}
				} // fim if ( rs != null )
			} // fim finally
		//#else
//#  			return false; // TODO gravação no "RMS" em J2SE
		//#endif
	} // fim do método createDatabase( String, int )


	/**
	 * Grava dados numa base de dados.
	 * @param dataBaseName nome da base de dados (precisa já existir)
	 * @param slot índice do slot onde os dados serão gravados. A faixa de valores válidos vai de 1 (um) ao número 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite gravação nos slots
	 * 1, 2 e 3.
	 * 
	 * @param serializable referência para o objeto Serializable que gravará seus dados no slot, através do seu método
	 * <i>write( DataOutputStream )</i>. Não é necessário chamar o método flush() do DataOutputStream.
	 * 
	 * @throws java.lang.NullPointerException caso <code>serializable</code> seja null.
	 * @throws javax.microedition.rms.RecordStoreNotFoundException caso <code>dataBaseName</code> seja inválido ou nulo.
	 * @throws javax.microedition.rms.InvalidRecordIDException caso <code>slot</code> seja inválido.
	 * @throws javax.microedition.rms.RecordStoreFullException caso não haja espaço livre disponível para gravar os dados.
	 * @throws java.lang.Exception caso ocorra outro tipo de erro ao abrir ou gravar os dados na base.
	 * 
	 * @see #createDatabase(String,int)
	 * @see #hasDatabase(String,int)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final void saveData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		if ( serializable == null ) {
			//#if DEBUG == "true"
//#  				throw new NullPointerException( "Erro ao gravar na base de dados: serializable não pode ser null." );
			//#else
			throw new NullPointerException();
		//#endif
		}

		//#if J2SE == "false"
			if ( !hasDatabase( dataBaseName, 0 ) ) {
				//#if DEBUG == "true"
//#  				throw new RecordStoreNotFoundException( "Erro ao gravar dados: base de dados \"" + dataBaseName + "\" não existe." );
				//#else
					throw new RecordStoreNotFoundException();
				//#endif
			}

			ByteArrayOutputStream byteOutput = null;
			DataOutputStream dataOutput = null;
			RecordStore rs = null;

			try {
				byteOutput = new ByteArrayOutputStream();
				dataOutput = new DataOutputStream( byteOutput );

				serializable.write( dataOutput );

				dataOutput.flush();

				final byte[] data = byteOutput.toByteArray();

				rs = RecordStore.openRecordStore( dataBaseName, false );
				rs.setRecord( slot, data, 0, data.length );
			} catch ( RecordStoreFullException rsfe ) {
				//#if DEBUG == "true"
//#  				throw new RecordStoreFullException( "Erro ao gravar na base de dados: não há espaço disponível." );
				//#else
					throw rsfe;
				//#endif
			} catch ( InvalidRecordIDException ire ) {
				//#if DEBUG == "true"
//#  				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inválido: " + slot );
				//#else
					throw ire;
				//#endif
			} finally {
				if ( rs != null ) {
					try {
						rs.closeRecordStore();
					} catch ( Exception e ) {
					//#if DEBUG == "true"
//#  					e.printStackTrace();
					//#endif
					}
				} // fim if ( rs != null )

				if ( byteOutput != null ) {
					try {
						byteOutput.close();
					} catch ( Exception e ) {
					}
					byteOutput = null;
				} // fim if ( byteOutput != null )

				if ( dataOutput != null ) {
					try {
						dataOutput.close();
					} catch ( Exception e ) {
					}
					dataOutput = null;
				} // fim if ( dataOutput != null )
			} // fim finally
		//#else
		//#endif
	} // fim do método saveData( String, int, Serializable )


	/**
	 * Apaga o conteúdo de um slot de uma base de dados, substituindo seu conteúdo por um array de 0 bytes.
	 * 
	 * @param dataBaseName nome da base de dados (precisa já existir).
	 * @param slot índice do slot cujos dados serão apagados. A faixa de valores válidos vai de 1 (um) ao número 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite gravação nos slots
	 * 1, 2 e 3.
	 * 
	 * @throw javax.microedition.rms.RecordStoreNotFoundException caso <code>dataBaseName</code> seja inválido ou nulo.
	 * @throws javax.microedition.rms.InvalidRecordIDException caso <code>slot</code> seja inválido.
	 * @throws java.lang.Exception caso ocorra erro ao apagar os dados da base.
	 * dados.
	 * 
	 * @see #createDatabase(String,int)
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final void eraseSlot( String dataBaseName, int slot ) throws Exception {
		//#if J2SE == "false"
			if ( hasDatabase( dataBaseName, 0 ) ) {
				RecordStore rs = null;

				try {
					rs = RecordStore.openRecordStore( dataBaseName, false );

					rs.setRecord( slot, new byte[ 0 ], 0, 0 );
				} catch ( InvalidRecordIDException ire ) {
					//#if DEBUG == "true"
//#  				throw new InvalidRecordIDException( "Erro ao ler da base de dados: slot inválido: " + slot );
					//#else
					throw ire;
				//#endif
				} finally {
					if ( rs != null ) {
						try {
							rs.closeRecordStore();
						} catch ( Exception e ) {
						//#if DEBUG == "true"
//#  					e.printStackTrace();
						//#endif
						} // fim catch( Exception e )
					} // fim if ( rs != null )
				} // fim finally
			} else {
				//#if DEBUG == "true"
//#  				throw new RecordStoreNotFoundException( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" não existe." );
				//#else
				throw new RecordStoreNotFoundException();
			//#endif

			}
		//#else
		//#endif
	} // fim do método eraseSlot( String, int )


	/**
	 * Lê os dados disponíveis num slot de uma base de dados.
	 * 
	 * @param dataBaseName nome da base de dados.
	 * @param slot índice do slot de onde os dados serão lidos. A faixa de valores válidos vai de 1 (um) ao número 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots possui como índices válidos
	 * apenas 1, 2 e 3.
	 * @param serializable referência para o objeto Serializable que interpretará o array de bytes lido da base de dados,
	 * através do seu método <i>read( DataInputStream )</i>.
	 * @throws java.lang.NullPointerException caso <code>serializable</code> seja null.
	 * @throws javax.microedition.rms.RecordStoreNotFoundException caso <code>dataBaseName</code> seja inválido ou nulo.
	 * @throws javax.microedition.rms.InvalidRecordIDException caso <code>slot</code> seja inválido.
	 * @throws java.lang.Exception caso ocorram outros erros relacionados à abertura da base ou leitura dos dados.
	 * 
	 * @see #createDatabase(String,int)
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final void loadData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		if ( serializable == null ) {
			//#if DEBUG == "true"
//#  				throw new NullPointerException( "Erro ao ler da base de dados: serializable não pode ser null." );
			//#else
			throw new NullPointerException();
		//#endif
		}

		//#if J2SE == "false"
			if ( !hasDatabase( dataBaseName, 0 ) ) {
				//#if DEBUG == "true"
//#  				throw new Exception( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" não existe." );
				//#else
					throw new RecordStoreNotFoundException();
				//#endif
			}

			ByteArrayInputStream byteInput = null;
			DataInputStream dataInput = null;
			RecordStore rs = null;

			try {
				rs = RecordStore.openRecordStore( dataBaseName, false );

				final byte[] data = rs.getRecord( slot );

				byteInput = new ByteArrayInputStream( data );
				dataInput = new DataInputStream( byteInput );

				serializable.read( dataInput );
			} catch ( InvalidRecordIDException ire ) {
				//#if DEBUG == "true"
//#  					throw new InvalidRecordIDException( "Erro ao ler da base de dados: slot inválido: " + slot );
				//#else
					throw ire;
				//#endif
			} finally {
				if ( rs != null ) {
					try {
						rs.closeRecordStore();
					} catch ( Exception e ) {
					//#if DEBUG == "true"
//#  					e.printStackTrace();
					//#endif
					} // fim catch( Exception e )
				} // fim if ( rs != null )

				if ( byteInput != null ) {
					try {
						byteInput.close();
					} catch ( Exception e ) {
					}
					byteInput = null;
				} // fim if ( byteInput != null )

				if ( dataInput != null ) {
					try {
						dataInput.close();
					} catch ( Exception e ) {
					}
					dataInput = null;
				} // fim if ( dataInput != null )
			} // fim finally
		//#else
		//#endif
	} // fim do método loadData( String, int, Serializable )


	/**
	 * Abre um arquivo presente no Jar para leitura.
	 * 
	 * @param filename endereço do arquivo a ser lido.
	 * @param serializable referência para o objeto Serializable que fará a leitura dos dados do arquivo, através do seu 
	 * método <i>read( DataInputStream )</i>.
	 * 
	 * @throws java.lang.NullPointerException caso <code>filename</code> seja inválido ou ocorra erro na abertura do arquivo.
	 * @throws java.lang.Exception caso ocorra erro na leitura dos dados do arquivo.
	 */
	public static synchronized final void openJarFile( String filename, Serializable serializable ) throws Exception {
		InputStream inputStream = null;
		DataInputStream dataInputStream = null;

		try {
			inputStream = filename.getClass().getResourceAsStream( filename );

 			// a chamada do método getResourceAsStream não lança exceção no caso de erro na abertura do arquivo. Em vez
 			// disso, apenas retorna null.
 			if ( inputStream == null ) {
				//#if DEBUG == "true"
//#   				throw new NullPointerException( "Erro ao abrir arquivo: " + filename );
				//#else
					throw new NullPointerException();
				//#endif
			}

			dataInputStream = new DataInputStream( inputStream );

			serializable.read( dataInputStream );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//#  			e.printStackTrace();
			//#endif
			throw e;
		} finally {
			try {
				if ( dataInputStream != null ) {
					try {
						dataInputStream.close();
					} catch ( Exception ex ) {
					}
					dataInputStream = null;
				}

				if ( inputStream != null ) {
					try {
						inputStream.close();
					} catch ( Exception ex ) {
					}

					inputStream = null;
				}
			} catch ( Exception e ) {
			//#if DEBUG == "true"
//#  				e.printStackTrace();
			//#endif				
			}
		} // fim finally				
	} // fim do método openJarFile( String, Serializable )


	public static AppMIDlet getInstance() {
		return instance;
	}
	
	
	/**
	 * Retorna a versão deste MIDlet, conforme informado por <code>instance.getAppProperty( "MIDlet-Version" )</code>.
	 * @return versão do MIDlet, no formato <code>xx.yy</code> ou <code>xx.yy.zz</code>. Caso a versão do aplicativo
	 * seja nula (valor retornado por getAppProperty()), retorna a string <code>"0.0.0"</code>
	 */
    public static final String getMIDletVersion() {
		//#if J2SE == "false"
			String version = instance.getAppProperty( "MIDlet-Version" );
			if ( version == null )
				version = "0.0.0";

			return version;
		//#else
//#  			return "J2SE BETA"; // TODO
		//#endif
    }


	/**
	 * Obtém uma propriedade definida no aplicativo (dentro do arquivo jad).
	 *
	 * @param key chave cujo valor será retornado.
	 * @return o valor da chave, ou <code>null</code> caso a chave não seja encontrada.
	 */
	public static final String getProperty( String key ) {
		return instance.getAppProperty( key );
	}


	public static final byte getLanguage() {
		return language;
	}


	/**
	 * Chama o garbage collector, levando em consideração o fabricante - a chamada é ignorada em fabricantes cujos aparelhos
	 * normalmente possuem bastante memória livre.
	 *
	 */
	public static final void gc() {
		switch( getVendor() ) {
			case VENDOR_BLACKBERRY:
			case VENDOR_HTC:
			case VENDOR_SONYERICSSON:
			break;

			default:
				System.gc();
		}
	}


	/**
	 * 
	 * @param language
	 */
	public static final void setLanguage( byte language ) {
		AppMIDlet.language = language;
		instance.changeLanguage( language );
	}


	/**
	 * 
	 * @param language
	 */
	protected void changeLanguage( byte language ) {
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static final String getPlatform() {
		//#if J2SE == "false"
			return System.getProperty( "microedition.platform" );
		//#else
//#  			return "PLATFORM J2SE"; // TODO
		//#endif
	}


	/**
	 * Realiza uma ligação para um número de telefone. 
	 * @param phoneNumber número de telefone a ser chamado.
	 * @return boolean indicando se a ligação foi feita com sucesso.
	 */
	public static final boolean callPhoneNumber( String phoneNumber ) {
		//#if J2SE == "false"
			try {
				getInstance().platformRequest( "tel:" + phoneNumber );

				return true;
			} catch ( ConnectionNotFoundException ex ) {
				//#if DEBUG == "true"
//#  				ex.printStackTrace();
				//#endif

				return false;
			}
		//#else
//#  			return false; // TODO
		//#endif
	}


	/**
	 * Acessa uma URL com o navegador do aparelho. ATENÇÃO: em alguns modelos isso exige que a execução seja encerrada.
	 *
	 * @param url caminho completo a ser acessado, inclusive com a indicação do protocolo, como "http://", "ftp://", 
	 * "https://", etc. Se não houver qualquer protocolo definido (ou seja, "*://"), é utilizado o HTTP.
	 * @param exitIfNecessary indica se a execução do aplicativo deve ser encerrada para que o pedido seja atendido, caso
	 * o aparelho indique que essa ação seja necessária.
	 * @return boolean indicando se o pedido foi atendido com sucesso.
	 */
	public static final boolean browseURL( String url, boolean exitIfNecessary ) {
		//#if J2SE == "false"
			try {
				if ( url.indexOf( "://" ) < 0 )
					url = "http://" + url;

				if ( getInstance().platformRequest( url ) ) {
					if ( exitIfNecessary )
						exit();
					else
						return false;
				}

				return true;
			} catch ( ConnectionNotFoundException ex ) {
				//#if DEBUG == "true"
//#  				ex.printStackTrace();
				//#endif

				return false;
			}
		//#else
//#  			return false; // TODO
		//#endif
	}
	
	
	//#if J2SE == "false"
		/**
		 *
		 * @throws javax.microedition.midlet.MIDletStateChangeException
		 * @deprecated É preferível chamar AppMIDlet.start() para melhorar compatibilidade com J2SE.
		 * @see #start()
		 */
		protected final void startApp() throws MIDletStateChangeException {
			start();
		}
	//#endif
	

	/**
	 * 
	 * @throws javax.microedition.midlet.MIDletStateChangeException
	 */
	public void start() {
		if ( instance != this ) {
			instance = this;

			manager = ScreenManager.createInstance( MAX_FRAME_TIME );

			//#if J2SE == "false"
				// se chegar nesse ponto sem detectar o fabricante, tenta outros métodos de detecção
				if ( getVendor() == VENDOR_GENERIC )
					VENDOR = ScreenManager.checkVendorByKeys();
			//#endif

			try {
				// carrega os recursos do aplicativo
				loadResources();

				// após carregar os recursos, inicia o gerenciador de tela (não é iniciado antes para que todo o
				// processamento seja dedicado ao carregamento dos recursos)
				manager.start();

				//#if QUALCOMM_API == "true"
//# 					// This asynchronous call fetches the application's license information of from the remote server.
//# 					// The results are returned in the LicenseCallback.
//# 					LicenseAgent.getLicenseAsync( this, ( LicenseCallback ) this );
				//#endif
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//#  				e.printStackTrace();
				//#endif

				exit();
			}
		} else {
			manager.showNotify();
		}
	}


	/**
	 * Troca a tela ativa atualmente, tratando a sincronização entre 2 possíveis chamadas concorrentes do método. Este
	 * método <b>não</b> troca efetivamente a tela ativa no <code>ScreenManager</code> - essa troca deve ser  feita dentro 
	 * do método <code>changeScreen</code>, que deve ser estendido pelas aplicações.
	 * 
	 * @param index índice da nova tela ativa.
	 * @see #changeScreen(int)
	 */
	public static synchronized final void setScreen( int index ) {
		if ( mutexScreen.acquire() ) {
			try {
				ScreenManager.setKeyListener( null );
//				ScreenManager.setScreenListener( null ); TODO teste... essa linha impede que o método hideNotify() da tela antiga seja chamado automaticamente

				//#if TOUCH == "true"
					ScreenManager.setPointerListener( null );
				//#endif
				
				currentScreen = ( byte ) instance.changeScreen( index );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//#  				e.printStackTrace();
				//#endif

				return;
			} finally {
				mutexScreen.release();
			}
		}
	//#if DEBUG == "true"
//#  		// TODO o que fazer no caso de não conseguir o acesso ao mutex?
//#  		else {
//#  			System.out.println( "AppMIDLet.setScreen: mutexScreen.acquire() retornou false." );
//#  		}
	//#endif
	} // fim do método setScreen( int )


	/** 
	 * Realiza efetivamente a troca da tela ativa. Não é necessário tratar sincronização da troca de tela, pois isto é
	 * feito no método AppMIDlet.setScreen(int). 
	 * 
	 * @param screen índice da nova tela ativa.
	 * @return índice da tela definida. Este índice pode ser diferente do recebido como parâmetro. Por exemplo, pode-se
	 * receber como parâmetro o índice de uma tela de jogo, mas a tela efetivamente exibida ser uma tela de log, indicando
	 * um erro que ocorreu ao tentar alocar a tela de jogo. Nesse caso, a implementação do método <code>changeScreen</code>
	 * <b>não</b> deve chamar novamente <code>setScreen</code>, pois isso causará deadlock da aplicação.
	 * 
	 * @see #setScreen(int)
	 */
	protected abstract int changeScreen( int screen ) throws Exception;


	/**
	 * Este método deve ser estendido pelos MIDlets, e será o responsável por realizar o carregamento de recursos
	 * como sons, imagens, fontes e quaisquer outros recursos utilizados inicialmente nos aplicativos. O método é
	 * chamado automaticamente na primeira vez que startApp() é executado, numa Thread própria para que o método
	 * startApp() retorne mais rapidamente, evitando que alguns aparelhos informem que o MIDlet não está respondendo.
	 * 
	 * @throws java.lang.Exception 
	 */
	protected abstract void loadResources() throws Exception;


	/**
	 * Obtém uma das fontes do aplicativo.
	 */
	public static ImageFont GetFont( int fontIndex ) {
		return getInstance().getFont( fontIndex );
	}


	protected ImageFont getFont( int fontIndex ) {
		return FONTS[ fontIndex ];
	}


	protected void pauseApp() {
		//#if QUALCOMM_API == "true"
//# 			// Call this API when the MIDlet enters pauseApp(). This cancels any outstanding operations and allows
//# 			// License Agent save the state correctly.
//#  			LicenseAgent.pause();
		//#endif

		if ( instance != null && manager != null ) {
			manager.hideNotify();
		}
	}


	//#if J2SE == "false"
		/**
		 *
		 * @param unconditional
		 * @throws javax.microedition.midlet.MIDletStateChangeException
		 * @deprecated É preferível chamar AppMIDlet.destroy() para melhorar compatibilidade com J2SE.
		 * @see #destroy() 
		 */
		protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
			destroy();
		}
	//#endif


	/**
	 * Método chamado pela aplicação para liberar recursos e sair.
	 */
	public void destroy() {
		// aparelhos BlackBerry não param de tocar sons automaticamente após a saída
		MediaPlayer.free();

		//#if QUALCOMM_API == "true"
//# 			// Call this API when the MIDlet enters destroyApp(). This cancels any outstanding network operations. 
//#  			LicenseAgent.cancel();
		//#endif

		if ( instance != null && instance.manager != null ) {
			instance.manager.setCurrentScreen( null );
			instance.manager.stop();
			instance.manager = null;
		}
		instance = null;

		//#if J2SE == "false"
			notifyDestroyed();
		//#endif
	}


	public static final void exit() {
		try {
			instance.destroy();
		} catch ( Exception e ) {
		//#if DEBUG == "true"
//#  			e.printStackTrace();
		//#endif
		}
	}


	/**
	 * Retorna o índice do fabricante do aparelho.
	 */
	public static byte getVendor() {
		return VENDOR;
	}


	//#if QUALCOMM_API == "true"
//# 		public void decrementUseCallback( License license ) {
			//#if DEBUG == "true"
//# 				System.out.println( "decrementUseCallback( " + license + " )" );
			//#endif
//# 			this.license = license;
//# 
//# 			try {
//# 				onValidLicense( license );
//# 			} catch ( Throwable t ) {
				//#if DEBUG == "true"
//# 					t.printStackTrace();
				//#endif
//# 					
//# 				exit();
//# 			}
//# 		}
//# 
//# 
//# 		protected void decrementUseAsync( License validLicense ) {
			//#if DEBUG == "true"
//# 				System.out.println( "decrementUseAsync( " + validLicense + " )" );
			//#endif
//# 
//# 			try {
//# 				validLicense.decrementUseAsync( 1, ( DecrementUseCallback ) this, false );
//# 			} catch ( NullCallbackException nullcallbackexception ) {
//# 				// This will only happen if DecrementUseCallback is null.
//# 				onLicenseMessage( MSG_TYPE_ERROR, MSG_ACTION_EXIT, LicenseString.LICENSEAGENT_INTERNAL_ERROR );
//# 			}
//# 		}
//# 
//# 
//# 		public void licenseCallback( License license ) {
			//#if DEBUG == "true"
//# 				System.out.println( "licenseCallback( " + license + " )" );
			//#endif
//# 
//# 			this.license = license;
//# 
//# 			onLicenseMessage( MSG_TYPE_INFO, MSG_ACTION_NONE, LicenseString.LICENSEAGENT_CONNECTING );
//# 
//# 			try {
//# 				Thread.sleep( 1 );
				//#if DEBUG == "true"
//# 					System.out.println( license.getExpirationDate() + " / "  +System.currentTimeMillis() + " : " + ( license.getExpirationDate() >= System.currentTimeMillis() ) );
				//#endif
//# 				// TODO é necessário verificar a data de expiração? Aparentemente licenças expiradas por tempo continuam
//# 				// retornando true para license.isValid()
//# //				if ( license.isValid() && license.getExpirationDate() >= System.currentTimeMillis() ) {
//# 				if ( license.isValid() ) {
//# 					validLicense( license );
//# 				} else {
//# 					invalidLicense( license );
//# 				}
//# 			} catch ( Throwable th ) {
//# 				// all exceptions from application code should be caught in the callback
				//#if DEBUG == "true"
//# 					th.printStackTrace();
				//#endif
//# 			}
//# 		}
//# 
//# 
//# 		public void purchase() {
			//#if DEBUG == "true"
//# 				System.out.println( "purchase()" );
			//#endif
//# 
//# 			/**
//# 			 * The API purchaseNewLicense() will initiate a platform request, to either launch a storefront or a web
//# 			 * browser. Following this call there are two possibilities , depending on the device.
//# 			 * [1] Some devices will launch the browser/storefront immediately, placing the application in the background.
//# 			 * [2] Other devices will do nothing, until the application exits. This is the case on "lower-end" devices
//# 			 * that cannot multi-task. Based on the return of shouldAppClose() its recommended for the application
//# 			 * developers to close the application if it returns true as shown below.
//# 			 */
//# 			final AppMIDlet midlet = instance;
//# 			new Thread() {
//# 				public final void run() {
//# 					try {
//# 						final PurchaseResponse purchaseResponse = midlet.license.purchaseNewLicense();
//# 						if (!purchaseResponse.isSuccess()) {
//# 							final String errorString = purchaseResponse.getErrorCode() + " : " + purchaseResponse.getErrorString();
//# 							onLicenseMessage( MSG_TYPE_ERROR, MSG_ACTION_EXIT, errorString );
//# 						} else if (purchaseResponse.shouldAppClose()) {
							//#if DEBUG == "true"
			//# 					System.out.println( "Exitting before purchase." );
							//#endif
//# 							exit();
//# 						} else {
//# 							// reinicia o processo, mas agora com uma licença renovada
//# 							LicenseAgent.getLicenseAsync( midlet, ( LicenseCallback ) midlet );
//# 						}
//# 					} catch ( Exception ex ) {
						//#if DEBUG == "true"
		//# 					ex.printStackTrace();
						//#endif
//# 
//# 						exit();
//# 					}
//# 
//# 				}
//# 			}.start();
//# 		}
//# 
//# 
//# 		protected String getPurchaseStringFromJAD( int state ) {
//# 			return LicenseString.getLicensePurchaseString(state, this);
//# 		}
//# 
//# 
//# 		protected void validLicense( final License validLicense ) {
			//#if DEBUG == "true"
//# 				System.out.println( "validLicense( " + validLicense + " )" );
			//#endif
//# 
//# 			if ( validLicense.isUseBased() ) {
//# 				decrementUseAsync( validLicense );
//# 			} else {
//# 				if ( validLicense.getErrorCode() != License.NOERROR ) {
//# 					// Following this call , the user will be presented with warning message, which after dismissing will
//# 					// go ahead and launch the appDevelopers code. This is to make sure that the user sees the warning message.
//# 					onLicenseMessage( MSG_TYPE_WARNING, MSG_ACTION_START, validLicense.getErrorString() );
//# 				} else {
//# 					onValidLicense( validLicense );
//# 				}
//# 			}
//# 		}
//# 
//# 
//# 		protected void invalidLicense( License invalidLicense ) {
			//#if DEBUG == "true"
//# 				System.out.println( "invalidLicense( " + invalidLicense + " )" );
			//#endif
//# 
//# 			if ( invalidLicense.getErrorCode() == License.NOERROR ) {
//# 				onLicenseMessage( MSG_TYPE_INFO, MSG_ACTION_PURCHASE, getPurchaseStringFromJAD( invalidLicense.getState() ) );
//# 			} else {
//# 				onLicenseMessage( MSG_TYPE_ERROR, MSG_ACTION_EXIT, invalidLicense.getErrorString() + ":" + invalidLicense.getErrorCode() );
//# 			}
//# 		}
//# 
//# 
//# 		/**
//# 		 * 
//# 		 * @param type tipo da mensagem. Valores válidos:
//# 		 * <ul>
//# 		 * <li>MSG_TYPE_CONFIRMATION</li>
//# 		 * <li>MSG_TYPE_ERROR</li>
//# 		 * <li>MSG_TYPE_INFO</li>
//# 		 * <li>MSG_TYPE_WARNING</li>
//# 		 * </ul>
//# 		 * @param action tipo da ação. Valores válidos:
//# 		 * <ul>
//# 		 * <li>MSG_ACTION_EXIT</li>
//# 		 * <li>MSG_ACTION_NONE</li>
//# 		 * <li>MSG_ACTION_PURCHASE</li>
//# 		 * <li>MSG_ACTION_START</li>
//# 		 * </ul>
//# 		 * @param message mensagem do Licese Agent Kit.
//# 		 *
//# 		 * @see #MSG_TYPE_CONFIRMATION
//# 		 * @see #MSG_TYPE_ERROR
//# 		 * @see #MSG_TYPE_INFO
//# 		 * @see #MSG_TYPE_WARNING
//# 		 * @see #MSG_ACTION_EXIT
//# 		 * @see #MSG_ACTION_NONE
//# 		 * @see #MSG_ACTION_PURCHASE
//# 		 * @see #MSG_ACTION_START
//# 		 */
//# 		protected abstract void onLicenseMessage( final byte type, final byte action, final String message );
//# 
//# 
//# 		/**
//# 		 *
//# 		 * @param validLicense
//# 		 */
//# 		protected abstract void onValidLicense( License validLicense );
//# 
	//#endif


	/**
	 * Obtém um dos textos utilizados no aplicativo.
	 * @param index índice do texto.
	 * @return texto de índice <i>index</i>.
	 */
	public static final String getText( int index ) {
		//#if DEBUG == "true"
//#  		try {
		//#endif
		
		return texts[ index ];
		
		//#if DEBUG == "true"
//#  		} catch ( Exception e ) {
//#  			System.out.println( "AppMIDlet.getText: índice inválido: " + index + " / " + ( texts == null ? 0 : texts.length ) );
//#  			e.printStackTrace();
//#
//#  			return null;
//#  		}
		//#endif
	}
	
	
	public static final String DATE_FORMAT_DDMMYY			= "d/m/y";
	public static final String DATE_FORMAT_DDMMYYYY			= "d/m/Y";
	public static final String DATE_FORMAT_YYYYMMDD			= "Y/m/d";
	public static final String DATE_FORMAT_YYMMDD			= "y/m/d";
	public static final String DATE_FORMAT_DDMMYYYY_HHMM	= "d/m/Y h:n";
	public static final String DATE_FORMAT_DDMMYY_HHMM		= "d/m/y h:n";
	public static final String DATE_FORMAT_YYYYMMDD_HHMM	= "Y/m/d h:n";
	public static final String DATE_FORMAT_YYMMDD_HHMM		= "y/m/d h:n";
	
	
	/**
	 * 
	 * @param time
	 * @param format string que indica o formato da data. Cada caracter da string simboliza um valor:
	 * <ul>
	 * <li>'d': dia (01 a 31)</li>
	 * <li>'m': mês (01 a 12)</li>
	 * <li>'y': ano (00 a 99)(</li>
	 * <li>'Y': ano (completo, como 2009, 1973, etc.)</li>
	 * <li>'h': hora (00 a 23)</li>
	 * <li>'n': minuto (00 a 59)</li>
	 * <li>outros caracteres: adicionados sem conversão</li>
	 * </ul>
	 * @return
	 */
	public static final String formatTime( long time, String format ) {
		final Date date = new Date( time );
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime( date );

		final StringBuffer text = new StringBuffer();
		
		final char[] chars = format.toCharArray();
		for ( byte i = 0; i < chars.length; ++i ) {
			switch ( chars[ i ] ) {
				case 'd':
					final int day = calendar.get( Calendar.DAY_OF_MONTH );
					if ( day < 10 )
						text.append( '0' );
					text.append( day );					
				break;
				
				case 'm':
					final int month = calendar.get( Calendar.MONTH ) + 1;
					if ( month < 10 )
						text.append( '0' );			
					text.append( month );					
				break;
				
				case 'Y':
					text.append( calendar.get( Calendar.YEAR ) );
				break;
				
				case 'y':
					final int year = calendar.get( Calendar.YEAR ) % 100;
					if ( year < 10 )
						text.append( '0' );
					text.append( year );
				break;
				
				case 'h':
					final int hour = calendar.get( Calendar.HOUR_OF_DAY );
					if ( hour < 10 )
						text.append( '0' );			
					text.append( hour );
				break;
				
				case 'n':
					final int minute = calendar.get( Calendar.MINUTE );
					if ( minute < 10 )
						text.append( '0' );			
					text.append( minute );							
				break;
				
				default:
					text.append( chars[ i ] );
			}
		}
		
		return text.toString();
	}


	/**
	 * 
	 * @param day dia do mês (1-31)
	 * @param month mês do ano (1-12)
	 * @param year ano
	 * @return
	 */
	public static final long getTime( int day, int month, int year ) {
		// falta de critério nas constantes do Java: mês é contado de 0 a 11, porém dias e anos são contagem natural
		final Calendar calendar = Calendar.getInstance();
		calendar.set( Calendar.DAY_OF_MONTH, day );
		calendar.set( Calendar.MONTH, month - 1 );
		calendar.set( Calendar.YEAR, year );

		return calendar.getTime().getTime();
	}
	
	
	/**
	 *
	 * @param day dia do mês. Valores válidos: 1 a 31 (depende do mês, podendo ser 28, 29, 30 ou 31).
	 * @param month mês do ano. Valores válidos: 1 a 12.
	 * @param year ano no calendário ocidental.
	 * @return 0, caso a data seja válida; 1 para erro no dia e 2 para erro no mês.
	 */
	public static final byte isValidBirthday( int day, int month, int year ) {
		if ( month <= 0 || month > 12 ) {
			return 2;
		}
		
		if ( day <= 0 )
			return 1;

		if ( month == 2 ) {
			if ( isLeapYear( year ) ) {
				if ( day > 29 ) {
					return 1;
				}
			} else {
				if ( day > 28 ) {
					return 1;
				}
			}
		} else if ( ( month == 1 ) || ( month == 3 ) || ( month == 5 ) || ( month == 7 ) || ( month == 8 ) || ( month == 10 ) || ( month == 12 ) ) {
			if ( day > 31 ) {
				return 1;
			}
		} else {
			if ( day > 30 ) {
				return 1;
			}
		}
		
		return 0;
	}
	

	/**
	 * Verifica se o ano é bissexto. Para isso, utilizamos as regras:
	 *
	 *		1. De 4 em 4 anos é ano bissexto.
	 *		2. De 100 em 100 anos não é ano bissexto.
	 *		3. De 400 em 400 anos é ano bissexto.
	 *		4. Prevalece as últimas regras sobre as primeiras.
	 *
	 *	Para melhor entender
	 *
	 *	   * São bissextos todos os anos múltiplos de 400, p.ex: 1600, 2000, 2400, 2800
	 *	   * Não são bissextos todos os múltiplos de 100 e não de 400, p.ex: 1700, 1800, 1900, 2100, 2200, 2300, 2500...
	 *	   * São bissextos todos os múltiplos de 4 e não múltiplos de 100, p.ex: 1996, 2004, 2008, 2012, 2016...
	 *	   * Não são bissextos todos os demais anos.
	 * @param year
	 * @return
	 */
	public static final boolean isLeapYear( int year ) {
		if ( year % 100 == 0 ) {
			year /= 100;
		}

		// &3 == %4
		return ( year & 3 ) == 0;
	}
	
}
 
