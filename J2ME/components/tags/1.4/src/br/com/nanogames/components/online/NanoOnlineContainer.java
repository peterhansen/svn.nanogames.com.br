/**
 * NanoOnlineContainer.java
 * 
 * Created on 29/Jan/2009, 12:25:03
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.userInterface.form.layouts.Layout;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

/**
 * Classe que encapsula funcionamentos básicos de telas do Nano Online.
 * @author Peter
 */
public class NanoOnlineContainer extends Container implements NanoOnlineConstants, EventListener {

	protected static final byte MIN_COMPONENTS = 0;


	/***/
	private final Pattern patternTop;

	/***/
	private final Pattern patternFill;
	
	protected int backScreenIndex = -1;
	
	
	/**
	 * Cria um novo contâiner do Nano Online, utilizando um FlowLayout com eixo vertical.
	 * @param nSlots
	 * @throws java.lang.Exception
	 * @see #NanoOnlineContainer( int, Layout )
	 */
	protected NanoOnlineContainer( int nSlots ) throws Exception {
		this( nSlots, new FlowLayout( FlowLayout.AXIS_VERTICAL ) );
	}
	
	
	/**
	 * Cria um novo contâiner do Nano Online, utilizando o layout escolhido.
	 * @param nSlots
	 * @param layout
	 * @throws java.lang.Exception
	 * @see #NanoOnlineContainer( int )
	 */
	protected NanoOnlineContainer( int nSlots, Layout layout ) throws Exception {
		super( nSlots + MIN_COMPONENTS, layout );

		patternTop = new Pattern( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "t.png" ) );
		patternTop.setSize( 0, patternTop.getFill().getHeight() );

		patternFill = new Pattern( 0xffffff );
		patternFill.setPosition( 0, patternTop.getHeight() );
		
		setScrollBarV( NanoOnline.getScrollBarV() );
		
		NanoOnline.getProgressBar().softkeyRight.addEventListener( this );
	}


	public final void setLayout( Layout layout ) {
		super.setLayout( layout );

		if ( layout instanceof FlowLayout ) {
			setScrollableY( true );
			( ( FlowLayout ) layout ).gap.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );
			( ( FlowLayout ) layout ).start.set( LAYOUT_START_X, LAYOUT_START_Y );
		}
	}


	protected final void removeAllComponents() {
		while ( getUsedSlots() > MIN_COMPONENTS )
			removeDrawable( MIN_COMPONENTS, false );
	}


	/**
	 * Destrói o contâiner e também o remove da lista de listeners da barra de progresso.
	 */
	public void destroy() {
		super.destroy();
		
		NanoOnline.getProgressBar().softkeyRight.removeActionListener( this );
	}
	
	
	public final void setBackIndex( int backIndex ) {
		this.backScreenIndex = backIndex;
	}
	
	
	protected void onBack() {
		if ( backScreenIndex >= 0 ) {
			NanoOnline.setScreen( backScreenIndex );
		}
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		patternTop.setSize( width, patternTop.getHeight() );
		patternFill.setSize( width, height - patternTop.getHeight() );
	}


	protected void paint( Graphics g ) {
		// desenha manualmente o fundo para garantir que ele sempre estará visível na mesma posição
		patternTop.draw( g );
		patternFill.draw( g );
		
		super.paint( g );
	}


	public void eventPerformed( Event evt ) {
	}

	
}
