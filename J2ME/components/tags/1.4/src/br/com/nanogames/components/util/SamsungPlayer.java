/**
 * SamsungPlayer.java
 * ©2008 Nano Games.
 *
 * Created on 18/01/2008 13:22:34.
 */

// TODO implementar métodos para detectar tempo total do som, indicar o estado do player, etc.

//#if SAMSUNG_API == "true"
//# 
//# package br.com.nanogames.components.util;
//# 
//# import javax.microedition.media.Control;
//# import javax.microedition.media.MediaException;
//# import javax.microedition.media.Player;
//# import javax.microedition.media.PlayerListener;
//# import javax.microedition.media.TimeBase;
//# import com.samsung.util.AudioClip;
//# 
//# /**
//#  * Essa classe implementa um tocador de sons que utiliza a API específica de sons da Samsung (AudioClip) para tocar sons.
//#  * @author Peter
//#  */
//# public final class SamsungPlayer implements Player {
//# 
//# 	/** Volume padrão do AudioClip. TODO confirmar valor */
//# 	private static final byte VOLUME = 100;
//# 	
//# 	/** AudioClip chamado para tocar os sons. */
//# 	private final AudioClip audioClip;
//# 	
//# 	/** Contador de loops definido atualmente. */
//# 	private byte loopCount = 1;
//# 	
//# 	
//# 	public SamsungPlayer( String filename ) throws Exception {
//# 		audioClip = new AudioClip( 0, filename );
//# 	}
//# 	
//# 
//# 	public final void realize() throws MediaException {
//# 	}
//# 
//# 
//# 	public final void prefetch() throws MediaException {
//# 	}
//# 
//# 
//# 	public final void start() throws MediaException {
//# 		audioClip.play( loopCount, VOLUME );
//# 	}
//# 
//# 
//# 	public final void stop() throws MediaException {
//# 		audioClip.stop();
//# 	}
//# 
//# 
//# 	public final void deallocate() {
//# 		try {
//# 			stop();
//# 		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 		}
//# 	}
//# 
//# 
//# 	public final void close() {
//# 	}
//# 
//# 
//# 	public final void setTimeBase( TimeBase t ) throws MediaException {
//# 	}
//# 
//# 
//# 	public final TimeBase getTimeBase() {
//# 		return null;
//# 	}
//# 
//# 
//# 	public final long setMediaTime( long mediaTime ) throws MediaException {
//# 		return 0;
//# 	}
//# 
//# 
//# 	public final long getMediaTime() {
//# 		return 0;
//# 	}
//# 
//# 
//# 	public final int getState() {
//# 		return 0;
//# 	}
//# 
//# 
//# 	public final long getDuration() {
//# 		return 0;
//# 	}
//# 
//# 
//# 	public final String getContentType() {
//# 		return "";
//# 	}
//# 
//# 
//# 	public final void setLoopCount( int loopCount ) {
//# 		this.loopCount = ( byte ) ( loopCount == MediaPlayer.LOOP_INFINITE ? 100 : loopCount );
//# 	}
//# 
//# 
//# 	public final void addPlayerListener( PlayerListener arg0 ) {
//# 	}
//# 
//# 
//# 	public final void removePlayerListener( PlayerListener arg0 ) {
//# 	}
//# 
//# 
//# 	public final Control[] getControls() {
//# 		return null;
//# 	}
//# 
//# 
//# 	public Control getControl( String arg0 ) {
//# 		return null;
//# 	}
//# 	
//# }
//# 
//#
//# 
//# // TODO melhor API de som Samsung:
//# 
//# // private int volume;
//# //   private AudioClip clip;
//# //
//# //   public oTune(byte[] audioData, int audioOffset, int audioLength, String message) {
//# //       volume = 3;
//# //       try {
//# //           clip = new AudioClip(1, audioData, audioOffset, audioLength);
//# //           clip.play(1, volume);
//# //       } catch (Exception e) {
//# //           message = e.toString();
//# //       }
//# //   }
//# //
//# //   public oTune(String fName) {
//# //       volume = 3;
//# //       playSong(fName);
//# //   }
//# //
//# //   public void playSong(String fName) {
//# //       try {
//# //           clip = new AudioClip(1, fName);
//# //           clip.play(1, volume);
//# //       } catch (Exception e) {
//# //       }
//# //   }
//# //
//# //   public boolean readyForPlay() {
//# //       if (clip != null) {
//# //           return true;
//# //       }
//# //       return false;
//# //   }
//# //
//# //   public void play(int LoopCount) {
//# //       if (LoopCount == -1) {
//# //           LoopCount = 255;
//# //       }
//# //       if (clip != null) {
//# //           clip.stop(); // this needs to be done if not playing for the first time
//# //           clip.play(LoopCount, volume);
//# //       }
//# //   }
//# //
//# //   public void stop() {
//# //       if (clip != null) {
//# //           clip.stop();
//# //       }
//# //   }
//# //
//# //   public void pause() {
//# //       if (clip != null) {
//# //           clip.pause();
//# //       }
//# //   }
//# //
//# //   public void resume() {
//# //       if (clip != null) {
//# //           clip.resume();
//# //       }
//# //   }
//# //
//# //   public void setVolume(int newVolume) {
//# //       volume  = newVolume;
//# //   }
//# //
//# //   public void destroy() {
//# //       if (clip != null) {
//# //           clip.stop();
//# //           clip = null;
//# //       }
//# //   }
//# //}
//# 
//# 
//#endif