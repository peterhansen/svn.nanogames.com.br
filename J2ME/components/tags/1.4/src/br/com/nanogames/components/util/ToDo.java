/**
 * ToDo.java
 * ©2008 Nano Games.
 *
 * Created on Jun 13, 2008 6:40:33 PM.
 */

package br.com.nanogames.components.util;

/**
 *
 * @author Peter
 */
public interface ToDo {

//TODO 1. Filhos apontarem o pai nos grupos: Possíveis problemas:
//	1.1. não há destrutor, logo deveria haver controle mais rígido sobre desalocação de todos os grupos, varrendo os filhos e anulando a referência para o pai (senão memória jamais será desalocada)
//	
//TODO 2. Spline
//TODO 3. NURBS
//TODO 5. Aprimorar o arquivo descritor dos sprites:
//TODO        5.1 Poder indicar transformações (espelhamento e rotação)
//TODO        5.2 Poder indicar o número de vezes que uma sequência deve ser executada
//TODO        5.3 Indicar o número total de frames para otimizar a alocação dos arrays
//TODO        5.4 Indicar o número total de sequências para otimizar a alocação dos arrays
//TODO        5.5 Poder indicar que não deve desenhar nada durante tanto tempo (frame "invisível")
//TODO 6. Opções invisíveis no menu
//TODO 7. Opções não-selecionáveis no menu
//TODO 8. tilemaps
//TODO 9. engine isométrica
//TODO 12. gravação e leitura de arquivos genéricos
//TODO 14: No RichLabel, retirar os espaços nos finais e inícios de linhas que foram geradas por quebra automática
//
//
//**************************************************************************************************
//Caso vc queira acrescentar estes métodos em NanoMath. Já utilizei-os inúmeras vezes na minha vida.
//[]s
//
///** Obtém o próximo inteiro múltiplo de m */
//    private int nextMulOf( int n, int m )
//    {
//        final int aux = n % m;
//        if( aux == 0 )
//            return n;
//        return n + ( m - aux );
//    }
//
///** Obtém o próximo inteiro múltiplo de m. Otimizado para potências de 2 */
//private int nextMulOfPot2( int n, int m )
//{
//    --m;
//    return ( n + m ) & ~m;
//}
//
///** Retorna uma valor interpolado linearmente entre v1 e v2, tal que o valor de retorno seja v1 quando
//     * percentage é 0 e v2 quando percentage é 100
//     */
//    public final int lerp( int v1, int v2, int percentage )
//    {
//        // O parâmetro percentage deve estar entre 0 e 100
//        clamp( percentage, 0, 100 );
//       
//        return v1 + ( ( percentage * ( v2 - v1 ) ) / 100 );
//    }
//
//    /** Coloca o valor de i dentro do intervalo [min, max] */
//    public final int clamp( int i, int min, int max )
//    {
//        return i <= min ? min : i >= max ? max : i;
//    }
//
//	TODO classe/método para criação automática de bordas (contêiner?), que possam ser aumentadas/diminuídas automaticamente
	
}
