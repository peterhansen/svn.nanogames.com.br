/**
 * DrawableBorder.java
 * 
 * Created on Jul 16, 2009, 12:41:39 PM
 *
 */

package br.com.nanogames.components.userInterface.form.borders;

import br.com.nanogames.components.Drawable;

/**
 *
 * @author Peter
 */
public class DrawableBorder extends Border {


	public DrawableBorder( Drawable d ) throws Exception {
		super( 1 );
		insertDrawable( d );
	}


	public Border getCopy() throws Exception {
		final DrawableBorder b = new DrawableBorder( getDrawable( 0 ) );
		b.set( getTop(), getLeft(), getBottom(), getRight() );
		return b;
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		getDrawable( 0 ).setSize( size );
	}

}
