/**
 * NewRecordScreen.java
 * 
 * Created on 5/Fev/2009, 19:36:50
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;

/**
 *
 * @author Peter
 */
public class NewRecordScreen extends NanoOnlineContainer {

	private static final byte TOTAL_SLOTS = 10;
	
	private static final byte ENTRY_BUTTON_USE_PROFILE		= 0;
	private static final byte ENTRY_BUTTON_CHOOSE_PROFILE	= 1;
	private static final byte ENTRY_BUTTON_SAVE_LOCAL		= 2;
	private static final byte ENTRY_BUTTON_CREATE_PROFILE	= 3;
	private static final byte ENTRY_BUTTON_LOAD_EXISTING	= 4;


	public NewRecordScreen() throws Exception {
		super( TOTAL_SLOTS );
		
		final ImageFont font = NanoOnline.getFont( FONT_BLACK );
		
		final Customer[] customers = NanoOnline.getCustomers();
		
		final FormText text = new FormText( font );
		if ( customers == null ) {
			// não há jogadores salvos no aparelho
			text.setText( NanoOnline.getText( TEXT_NO_PROFILES_FOUND ) );
			insertDrawable( text );
			
			final Button buttonCreate = NanoOnline.getButton( TEXT_CREATE_PROFILE, ENTRY_BUTTON_CREATE_PROFILE, this );
			insertDrawable( buttonCreate );
			
			final Button buttonLoad = NanoOnline.getButton( TEXT_IMPORT_PROFILE, ENTRY_BUTTON_LOAD_EXISTING, this );
			insertDrawable( buttonLoad );
		} else {
			// há perfis salvos no aparelho - mostra o perfil atual e opções para escolher/registrar outro
			text.setText( NanoOnline.getText( TEXT_CURRENT_PROFILE ) + ": " + NanoOnline.getCurrentCustomer().getNickname() );
			insertDrawable( text );
			
			final Button buttonUse = NanoOnline.getButton( TEXT_USE_THIS_PROFILE, ENTRY_BUTTON_USE_PROFILE, this );
			insertDrawable( buttonUse );
			
			final Button buttonChoose = NanoOnline.getButton( TEXT_CHOOSE_PROFILE, ENTRY_BUTTON_CHOOSE_PROFILE, this );
			insertDrawable( buttonChoose );

			final Button buttonLoad = NanoOnline.getButton( TEXT_IMPORT_PROFILE, ENTRY_BUTTON_LOAD_EXISTING, this );
			insertDrawable( buttonLoad );	
		}
		
		text.setFocusable( false );

		if ( NanoOnline.getRankingTypes() != NanoOnline.RANKING_TYPES_GLOBAL_ONLY ) {
			final Button buttonSaveLocal = NanoOnline.getButton( TEXT_SAVE_LOCAL_RECORD, ENTRY_BUTTON_SAVE_LOCAL, this );
			insertDrawable( buttonSaveLocal );
		}
	}
	

	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_USE_PROFILE:
						try {
							RankingScreen.setHighScore( RankingScreen.getLastType(), NanoOnline.getCurrentCustomer().getId(), RankingScreen.getLastScore(), RankingScreen.getLastDecrescent(), RankingScreen.getLastCumulative() );
							NanoOnline.setScreen( SCREEN_RECORDS, backScreenIndex );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
							NanoOnline.setScreen( backScreenIndex );
						}
					break;
					
					case ENTRY_BUTTON_CHOOSE_PROFILE:
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT, getId() );
					break;
					
					case ENTRY_BUTTON_SAVE_LOCAL:
						NanoOnline.setScreen( SCREEN_ENTER_LOCAL_NAME, getId() );
					break;
					
					case ENTRY_BUTTON_CREATE_PROFILE:
						NanoOnline.setScreen( SCREEN_REGISTER_PROFILE, getId() );
					break;
					
					case ENTRY_BUTTON_LOAD_EXISTING:
						NanoOnline.setScreen( SCREEN_LOAD_PROFILE, getId() );
					break;
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						keyPressed( ScreenManager.FIRE );
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )			
	}

}
