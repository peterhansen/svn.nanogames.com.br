/**
 * ScrollRichLabel.java
 * ©2008 Nano Games.
 *
 * Created on Jul 8, 2008 10:59:48 PM.
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.*;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//#endif


/**
 * Classe que engloba um RichLabel para ser utilizado em forms, com barras de scroll.
 * @author Peter
 */
public class FormText extends Container {
	
	public static final byte MAX_LINES_DEFAULT = 3;
	
	protected final RichLabel label;
	
	/** Velocidade de movimentação do texto. */
	protected final MUV textSpeed = new MUV();
	
	/** Velocidade máxima de movimentação do texto. No caso de movimentação automática ou através de teclas, esse valor
	 * é sempre utilizado. No caso de movimentação através de ponteiro, a velocidade é gradual.
	 */
	protected short TEXT_SPEED;
	
	/** Limite superior do texto. */
	protected short textLimitTop;
	
	/** Limite inferior do texto. */
	protected short textLimitBottom;
	
	/** Indica se o scroll do texto está no modo automático. */
	protected boolean autoScroll;	
	
	/** Última posição do ponteiro (valor utilizado para fazer scroll). */
	protected short lastPointerY;	
	
	/** Indica se o ponteiro está sendo arrastado. */
	protected boolean dragging;
	
	/** Posição de início do arrasto da barra de scroll da página. */
	protected short dragYStart;
	
	/** Offset do texto no momento em que a barra de scroll da página começou a ser arrastada. */
	protected short textOffsetStart;
	
	/** Armazena a última tecla apertada. Ver keyPressed() e keyReleased() */
	private int lastKeyPressed;
	
	/***/
	private byte maxLines;

	/** Indica se houve alteração no scroll do texto (necessário em aparelhos BlackBerry com rolagem, pois não é possível
	 * manter pressionado para baixo, por exemplo). */
	private boolean scrollChanged;
	
	
	
	public FormText( ImageFont font ) throws Exception {
		this( font, 0 );
	}
	
	
	public FormText( ImageFont font, int maxLineWidth ) throws Exception {
		this( font, maxLineWidth, MAX_LINES_DEFAULT );
	}
	
	
	public FormText( ImageFont font, int maxLineWidth, int maxLines ) throws Exception {
		this( font, maxLineWidth, maxLines, null );
	}
	
	
	public FormText( ImageFont font, int maxLineWidth, Drawable[] specialChars ) throws Exception {
		this( font, maxLineWidth, MAX_LINES_DEFAULT, specialChars );
	}
	
	
	public FormText( ImageFont font, int maxLineWidth, int maxLines, Drawable[] specialChars ) throws Exception {
		super( 1 );
		
		this.label = new RichLabel( font, null, maxLineWidth, specialChars );
		insertDrawable( label );
		
		setScrollableY( true );
		
		// marca o componente como focusable, pois apesar de estender um container possui comportamentos próprios
		focusable = true;
		
		setMaxLines( maxLines );
		
		setAutoScroll( false );
	}


	public void update( int delta ) {
		super.update( delta );
		
		if ( textSpeed.getSpeed() != 0 ) {
			final int dy = textSpeed.updateInt( delta );

			if ( dy != 0 ) {
				scrollChanged = true;
				setScrollY( getScrollY() + dy );
			}
		}
	}
	
	
	public void setScrollY( int offset ) {
		if ( offset > textLimitBottom ) {
			offset = textLimitBottom;
		} else if ( offset < textLimitTop ) {
			if ( autoScroll ) {
				offset = textLimitBottom;
			} else {
				offset = textLimitTop;
			}
		}
		
		label.setTextOffset( offset );
		refreshScrollV();
	}	
	
	
	protected final boolean isAtTop() {
		return getScrollY() <= textLimitTop;
	}
	
	
	protected final boolean isAtBottom() {
		return getScrollY() >= textLimitBottom;
	}
	
	
	protected void setTextSpeed( int speed ) {
		if ( autoScroll ) {
			textSpeed.setSpeed( -Math.abs( TEXT_SPEED ) );
		} else {
			textSpeed.setSpeed( speed );
			scrollChanged = false;
		}
	}	
	
	
	public void setMaxLines( int maxLines ) {
		this.maxLines = ( byte ) maxLines;
		setAutoCalcPreferredSize( true );
	}
	

	public void onPointerDragged( int x, int y ) {
		if ( !autoScroll && dragging ) {
			x -= position.x - getScrollX();
			y -= position.y - getScrollY();
			
			setScrollY( getScrollY() + y - lastPointerY );
			lastPointerY = ( short ) y;
		}		
	}


	public void onPointerPressed( int x, int y ) {
		if ( !autoScroll ) {
			x -= position.x;
			y -= position.y;

			// usuário clicou no texto
			dragging = true;
			lastPointerY = ( short ) y;
		}
	}


	public void onPointerReleased( int x, int y ) {
		dragging = false;
	}
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		final int layoutHeight = getLayoutHeight();
		int scrollVWidth = ( isScrollableY() && getScrollBarV() != null && getScrollBarV().isVisible() ) ? getScrollBarV().getWidth() : 0;
		int scrollHHeight = ( isScrollableX() && getScrollBarH() != null && getScrollBarH().isVisible() ) ? getScrollBarH().getHeight() : 0;

		label.setSize( width - scrollVWidth, layoutHeight );
		label.formatText( false );

		setMaximumHeight( label.getTextTotalHeight() );

		// se o texto não couber mesmo sem a barra de scroll, refaz o cálculo
		if ( getMaximumHeight() > layoutHeight ) {
			refreshScrollBars();

			scrollVWidth = ( isScrollableY() && getScrollBarV() != null && getScrollBarV().isVisible() ) ? getScrollBarV().getWidth() : 0;
			scrollHHeight = ( isScrollableX() && getScrollBarH() != null && getScrollBarH().isVisible() ) ? getScrollBarH().getHeight() : 0;

			label.setSize( width - scrollVWidth, layoutHeight );
			label.formatText( false );
			setMaximumHeight( label.getTextTotalHeight() );
		}

		if ( autoScroll ) {
			// FIXME erro na altura de fim de scroll automático
			textLimitBottom = ( short ) size.y;
			
			setScrollY( textLimitBottom );
		} else {
			textLimitTop = ( short ) NanoMath.min( -label.getTextTotalHeight() + size.y, 0 );
		}			
		
		setAutoScroll( autoScroll );
		refreshScrollBars();
	}

	
	public void keyPressed( int key ) {
		if ( autoScroll ) {
			setHandlesInput( false );
			dispatchActionEvent( new Event( this, Event.EVT_KEY_PRESSED, new Integer( key ) ) );
		} else {
			// Armazena a tecla apertada para posterior utilização em keyReleased()
			lastKeyPressed = key;
			
			switch ( key ) {
				case ScreenManager.UP:
				case ScreenManager.KEY_NUM2:
					if ( isAtBottom() )
						setHandlesInput( false );
					else
						setTextSpeed( Math.abs( TEXT_SPEED ) );
				break;

				case ScreenManager.DOWN:
				case ScreenManager.KEY_NUM8:
					if ( isAtTop() )
						setHandlesInput( false );
					else
						setTextSpeed( -Math.abs( TEXT_SPEED ) );
				break;

				case ScreenManager.LEFT:
				case ScreenManager.KEY_NUM4:
					if ( isAtBottom() )
						setHandlesInput( false );
					else
						setScrollY( label.getTextOffset() + size.y );
				break;			

				case ScreenManager.RIGHT:
				case ScreenManager.KEY_NUM6:
					if ( isAtTop() )
						setHandlesInput( false );
					else
						setScrollY( label.getTextOffset() - size.y );
				break;

				default:
					setTextSpeed( 0 );
					dispatchActionEvent( new Event( this, Event.EVT_KEY_PRESSED, new Integer( key ) ) );
			} // fim switch ( key )
		}
	}


	public void keyReleased( int key ) {
		// Esse if faz com que os aparelhos não travem a rolagem ao mudarmos sua direção repentinamente. No
		// entanto, alguns aparelhos podem não mandar o evento de release necessário se duas teclas forem
		// apertadas simultaneamente.
		if ( key == lastKeyPressed ) {
			// se o usuário soltar a tecla antes de ter feito algum scroll, faz o scroll mínimo (altura da fonte)
			if ( !scrollChanged ) {
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						setScrollY( label.getTextOffset() + label.getFont().getHeight() );
					break;
					
					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						setScrollY( label.getTextOffset() - label.getFont().getHeight() );
					break;
				} // fim switch ( key )
			}

			setTextSpeed( 0 );
		}
	}
	
	
	public void setAutoScroll( boolean autoScroll ) {
		this.autoScroll = autoScroll;
		
		if ( autoScroll ) {
			TEXT_SPEED = ( short ) ( ( label.getFont().getHeight() * 3 ) >> 1 );
			textLimitTop = ( short ) -label.getTextTotalHeight();
			
			setTextSpeed( TEXT_SPEED );
		} else {
			TEXT_SPEED = ( short ) ( ( label.getFont().getHeight() * 13 ) >> 1 );
			textLimitBottom = 0;
		}			
	}
	
	
	protected void paint( Graphics g ) {
		// pula o método paint da classe Container, pois ele utilizava o offset do scroll aqui, o que causava erro de desenho
		// do texto e problemas no tratamento de ponteiros
		for ( int i = 0; i < activeDrawables; ++i )
			drawables[ i ].draw( g );

		
		if ( scrollBarH != null )
			scrollBarH.draw( g );
		if ( scrollBarV != null )
			scrollBarV.draw( g );
	}
	
	
	public final void setText( String text, boolean formatText, boolean autoSize ) {
		label.setText( text, formatText, autoSize );
		setSize( size );
	}

	
	public final void setText( String text ) {
		label.setText( text );
		setSize( size );
	}
	
	
	public final String getText() {
		return label.getText();
	}


	public String getUIID() {
		return "text";
	}


	protected void refreshScrollH() {
		// se a barra de scroll não existir, não há nada a atualizar
		if ( scrollBarH != null ) {
			// só mostra a barra de rolagem se a largura máxima for maior que a largura atual
			if ( isScrollableX() && getMaximumWidth() > getWidth() ) {
				final boolean scrollV = isScrollableY() && scrollBarV != null;
				scrollBarH.setSize( getWidth() - ( scrollV ? scrollBarV.getWidth() : 0 ), scrollBarV.getHeight() );
				scrollBarH.setPosition( 0, getLayoutHeight() - scrollBarH.getHeight() );

				scrollBarH.refreshScroll( this );

				scrollBarH.setVisible( true );
			} else {
				scrollBarH.setVisible( false );
			}
		}
	}


	protected void refreshScrollV() {
		// se a barra de scroll não existir, não há nada a atualizar
		if ( scrollBarV != null ) {
			// só mostra a barra de rolagem se a altura máxima for maior que a altura atual
			if ( isScrollableY() && getMaximumHeight() > getHeight() ) {
				final boolean scrollH = isScrollableX() && scrollBarH != null;
				scrollBarV.setSize( scrollBarV.getWidth(), getHeight() - ( scrollH ? scrollBarH.getHeight() : 0 ) );
				scrollBarV.setPosition( getLayoutWidth() - scrollBarV.getWidth(), 0 );

				scrollBarV.refreshScroll( this );
				
				scrollBarV.setVisible( true );
			} else {
				scrollBarV.setVisible( false );
			}
		}
	}


	public Point getScroll() {
		return new Point( getScrollX(), getScrollY() );
	}


	public int getScrollX() {
		return 0;
	}


	/**
	 * Utiliza o offset do texto como referência para o offset do scroll.
	 * @return
	 */
	public int getScrollY() {
		return label.getTextOffset();
	}


	public Point calcPreferredSize( Point maximumSize ) {
		// TODO levar em consideração o preferredSize
		// TODO rever cálculo de tamanho para inserção em containers com outros conteúdos (ex.: não pode ser maior que a área útil do container pai)
		if ( maximumSize != null ) {
			label.setSize( maximumSize.x, label.getHeight() );
			label.formatText( false );
		}
		final int textHeight = label.getTextTotalHeight();
		final int maxHeight = maxLines > 0 ? ( textHeight > 0 ? Math.min( textHeight, maxLines * label.getFont().getHeight() ) : maxLines * label.getFont().getHeight() ) : label.getTextTotalHeight();
		
		if ( maximumSize != null ) {
			return new Point( maximumSize.x, Math.min( maxHeight, maximumSize.y ) );
//			final Point previousSize = new Point( getSize() );
//			setSize( maximumSize.x, label.getHeight() );
//			
//			final Point prefSize = new Point( maximumSize.x, label.getTextTotalHeight() );
//			setSize( previousSize );
//			
//			return prefSize;
		}
		
		final Point borderSize = border == null ? new Point() : border.getBorderSize();
		return new Point( label.getWidth(), maxHeight ).add( borderSize );
	}


	public void setFocus( boolean focus ) {
		super.setFocus( focus );
		setHandlesInput( focus );
		
		if ( !focus ) {
			keyReleased( lastKeyPressed );
			onPointerReleased( 0, 0 );
		}
	}
	
	
}
