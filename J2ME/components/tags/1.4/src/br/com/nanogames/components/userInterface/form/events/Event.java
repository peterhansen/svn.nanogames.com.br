/**
 * Event.java
 * 
 * Created on Dec 13, 2008, 1:27:03 PM
 *
 */
package br.com.nanogames.components.userInterface.form.events;

import br.com.nanogames.components.userInterface.form.Component;


/**
 *
 * @author Peter
 */
public class Event {

	/** Evento disparado quando o componente ganha foco. */
	public static final byte EVT_FOCUS_GAINED			= 0;

	/** Evento disparado quando o componente perde o foco. */
	public static final byte EVT_FOCUS_LOST				= 1;

	/**
	 * Evento disparado quando o componente recebe um evento de tecla pressionada.
	 * <p><code>data</code>: <code>Integer</code> contendo o código da tecla pressionada.</p>
	 */
	public static final byte EVT_KEY_PRESSED			= 2;

	/**
	 * Evento disparado quando o componente recebe um evento de tecla solta.
	 * <p><code>data</code>: <code>Integer</code> contendo o código da tecla solta.</p>
	 */
	public static final byte EVT_KEY_RELEASED			= 3;

	//#if TOUCH == "true"
		/** Evento disparado quando o componente recebe um evento de ponteiro pressionado. */
		public static final byte EVT_POINTER_PRESSED		= 4;

		/** Evento disparado */
		public static final byte EVT_POINTER_RELEASED		= 5;

		/** Evento disparado */
		public static final byte EVT_POINTER_DRAGGED		= 6;
	//#endif

	/** Evento disparado quando um botão é confirmado. */
	public static final byte EVT_BUTTON_CONFIRMED		= 7;

	/** Evento disparado por um TextBox ao se pressionar as teclas CLEAR, BACK ou soft key direita com o texto vazio. */
	public static final byte EVT_TEXTBOX_BACK				= 8;

	/** Evento disparado por um TextBox ao mudar o tipo de entrada de texto (todas as palavras, números, password, etc.). */
	public static final byte EVT_TEXTBOX_INPUT_MODE_CHANGED	= 9;
	
	/** Evento disparado por um TextBox ao mudar o tipo de caixa (alta, baixa, etc.). */
	public static final byte EVT_TEXTBOX_CASE_TYPE_CHANGED	= 10;

	/** Evento disparado por um checkbox ao mudar de estado (selecionado/não selecionado/parcialmente selecionado).
	 */
	public static final byte EVT_CHECKBOX_STATE_CHANGED		= 11;

	/**
	 * Evento disparado por um TextBox ao pressionar a tecla esquerda no início do texto.
	 */
	public static final byte EVT_TEXTBOX_LEFT_ON_START		= 12;

	/**
	 * Evento disparado por um TextBox ao pressionar a tecla direita no final do texto.
	 */
	public static final byte EVT_TEXTBOX_RIGHT_ON_END		= 13;

	/**
	 * Evento disparado por um TextBox ao inserir um caracter e chegar ao limite de caracteres.
	 */
	public static final byte EVT_TEXTBOX_FILLED				= 14;


	/**
	 * Valor utilizado no caso de um evento com tipo não definido.
	 * @see #Event(Object)
	 */
	public static final int EVENT_TYPE_NONE = Integer.MIN_VALUE;

	/** Indica se o evento já foi consumido. */
	private boolean consumed;

	/** Objeto de origem do evento. */
	public final Component source;

	/** Identificador do tipo de evento. */
	public final int eventType;

	/** Dados extras do evento (opcional). */
	public final Object data;


	/**
	 * Cria uma nova instância de Event. Equivalente ao construtor <code>Event( source, EVENT_TYPE_NONE, null )</code>.
	 * @param source element for the action event
	 * @see #Event(Object, int)
	 * @see #Event(Object, int, Object)
	 * @see #EVENT_TYPE_NONE
	 */
	public Event( Component source ) {
		this( source, EVENT_TYPE_NONE, null );
	}


	/**
	 * Cria uma nova instância de Event. Equivalente ao construtor <code>Event( source, eventType, null )</code>.
	 * @param source element for the action event
	 * @param eventType identificador do tipo de evento.
	 * @see #Event(Object)
	 * @see #Event(Object, int, Object)
	 */
	public Event( Component source, int eventType ) {
		this( source, eventType, null );
	}


	/**
	 * Cria uma nova instância de Event.
	 * @param source objeto de origem do evento.
	 * @param eventType identificador do tipo de evento.
	 * @param data dados extras do evento (opcional).
	 * @see #Event(Object)
	 * @see #Event(Object, int)
	 */
	public Event( Component source, int eventType, Object data ) {
		this.source = source;
		this.eventType = eventType;
		this.data = data;
	}

	
	/**
	 * Consume the event indicating that it was handled thus preventing other action
	 * listeners from handling/receiving the event
	 */
	public void consume() {
		consumed = true;
	}


	/**
	 * Returns true if the event was consumed thus indicating that it was handled.
	 * This prevents other action listeners from handling/receiving the event.
	 */
	public boolean isConsumed() {
		return consumed;
	}


}
