/**
 * PaletteChanger.java
 * ©2008 Nano Games.
 *
 * Created on 14/03/2008 16:43:13.
 */
package br.com.nanogames.components.util;

import br.com.nanogames.components.userInterface.AppMIDlet;
import java.io.DataInputStream;
import java.io.DataOutputStream;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
//#else
//# import java.awt.image.BufferedImage;
//# import java.awt.Image;
//# import java.io.ByteArrayInputStream;
//# import javax.imageio.ImageIO;
//#endif

/**
 * @author Daniel L. Alves
 */

/** Modifica a paleta de cores de uma imagem PNG
 * Para mais informações, ver: http://www.libpng.org/pub/png/spec/1.2/png-1.2-pdg.html
 */
public final class PaletteChanger implements Serializable
{
	/** Início do chunk da paleta de cores na imagem PNG 
	 * Os primeiros oito bytes são a assinatura do arquivo PNG e possuem os valores decimais:
	 * 137 80 78 71 13 10 26 10
	 * O primeiro chunk, IHDR, vem logo em seguida e possui 25 bytes.
	 * O próximo chunk é o PLTE
	 */
	private final byte CHUNK_PLTE_START = 8 + 25;
	
	/** Início dos dados do chunk PLTE. Este csegue o formato geral dos chunks das imagens PNG:
	 * Length: 4 bytes
	 * Type: 4 bytes
	 * Data: Cores da paleta na ordem RGB (1 byte para cada componente). Deve ser divisível por 3
	 * CRC: 4 bytes
	 */
	private final byte PALETTE_START = CHUNK_PLTE_START + 8;
	
	/** Imagem que o objeto está manipulando na forma de um array de bytes */
	private byte[] rawImage;

	/** Tamanho em bytes da paleta de cores da imagem PNG */
	private int paletteSize;
	
	/** Número de cores na paleta da imagem PNG */
	private int nColors;
		
	/** Tabela para cálculo do CRC dos chunks da imagem PNG */
	private long[] crcTable;

	
	/**
	 * Construtor
	 * 
	 * @param imagePath path da imagem que será manipulada pelo objeto
	 * @throws java.lang.Exception
	 */
	public PaletteChanger( String imagePath ) throws Exception
	{
		// Inicia a leitura da imagem ( ver método read( DataInputStream ) )
		AppMIDlet.openJarFile( imagePath, this );
	}
	
	
	/** 
	 * Preenche a tabela para cálculo do CRC dos chunks da imagem PNG.
	 * 
	 * @see emptyCRCTable
	 */
	private final void fillCRCTable()
	{
		crcTable = new long[256];
		
		long c;
		for( int i = 0; i < crcTable.length ; ++i )
		{
			c = i;
			for( int k = 0; k < 8; ++k )
			{
				if( ( c & 1 ) == 1 )
					c = 0xedb88320L ^ ( c >> 1 );
				else
					c >>= 1;
			}
			crcTable[i] = c;
		}
	}
	
	
	/** 
	 * Esvazia a tabela para cálculo do CRC dos chunks da imagem PNG.
	 * 
	 * @see fillCRCTable
	 */
	public final void emptyCRCTable()
	{
		crcTable = null;
		AppMIDlet.gc();
	}

	
	/**
	 * Cria uma nova imagem, a partir da recebida no construtor, alterando as cores de sua paleta.
	 * ATENÇÃO: foi detectado um problema onde imagens de 256 cores geradas diretamente no Photoshop não têm
	 * suas cores trocadas. Utilizar sempre o PNGout ou outro programa de conversão de PNG nas imagens parece resolver.
	 * 
	 * @param map Mapeamento que indica como a paleta de cores deve ser modificada.
	 * @return Uma imagem com a paleta de cores modificada.
	 * @see #createImage(int, int)
	 */
	public final Image createImage( PaletteMap[] map )
	{
		// Inicializa a tabela de CRCs
		if( crcTable == null )
			fillCRCTable();
		
		// Não precisa armazenar o CRC original, pois a cada mudança teremos que calcular um novo CRC
		//final byte[] oldCRC = new byte[4];
		//System.arraycopy( rawImage, paletteStart + paletteSize, oldCRC, 0, 4 );

		// Armazena a paleta original da imagem e o CRC do chunk
		final byte[] palette = new byte[paletteSize];
		System.arraycopy( rawImage, PALETTE_START, palette, 0, palette.length );

		// Modifica a paleta original de acordo com o parâmetro map
		for( int i = 0; i < map.length ; ++i )
		{
			int paletteEntryIndex;
			
			if( ( paletteEntryIndex = getPaletteEntryIndex( map[i] ) ) != -1 )
			{
				// R
				rawImage[paletteEntryIndex    ] = map[i].newR;
				// G
				rawImage[paletteEntryIndex + 1] = map[i].newG;
				// B
				rawImage[paletteEntryIndex + 2] = map[i].newB;
			}
		}
		
		// Na forma antiga, percorríamos a paleta de cores procurando um mapeamento para cada uma delas.
		// Invertemos a lógica para tentar diminuir o processamento
//		for( int i = 0; i < nColors ; ++i )
//		{
//			int mapIndex, aux = paletteStart + ( i * 3 );
//
//			if( ( mapIndex = getMappedColor( rawImage[aux], rawImage[aux + 1], rawImage[aux + 2], map ) ) != -1 )
//			{
//				// R
//				rawImage[aux + 0] = map[mapIndex].newR;
//				// G
//				rawImage[aux + 1] = map[mapIndex].newG;
//				// B
//				rawImage[aux + 2] = map[mapIndex].newB;
//			}
//		}

		// Calcula o novo CRC do chunk
		// chunkPLTEStart + 4 => não inclui o campo length no cálculo do CRC
		// paletteSize + 4 => paleta + nome do chunk ( "PLTE" )
		final byte[] crc = NanoMath.longToByteArray ( calculateCRC( rawImage, CHUNK_PLTE_START + 4, paletteSize + 4 ) );
		
		// Escreve o novo CRC do chunk
		// 4 + i => Um long possui 8 bytes. Só estamos interessados nos últimos 4 bytes
		for( int i = 0 ; i < 4 ; ++i )
			rawImage[PALETTE_START + paletteSize + i] = crc[4 + i];

		// Cria a nova imagem
		//#if J2SE == "false"
			final Image newImage = Image.createImage( rawImage, 0, rawImage.length );
		//#else
//# 			BufferedImage newImage = null;
//# 			try {
//# 				final ByteArrayInputStream input = new ByteArrayInputStream( rawImage );
//# 				newImage = ImageIO.read( input );
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
//# 			}
		//#endif

		// Volta à paleta original
		System.arraycopy( palette, 0, rawImage, PALETTE_START, palette.length );

		// Não precisa voltar com o CRC original, pois na próxima mudança teremos que calcular um novo CRC
		// Escreve o CRC antigo
		//System.arraycopy( oldCRC, 0, rawImage, paletteStart + paletteSize, 4 );

		// Retorna a imagem criada
		return newImage;
	}

	
	/**
	 * Cria uma nova imagem, a partir da recebida no construtor, interpolando as cores de sua paleta entre
	 * lightColor e darkColor.
	 * ATENÇÃO: foi detectado um problema onde imagens de 256 cores geradas diretamente no Photoshop não têm
	 * suas cores trocadas. Utilizar sempre o PNGout ou outro programa de conversão de PNG nas imagens parece resolver.
	 * 
	 * @param lightColor Cor clara utilizada na interpolação.
	 * @param darkColor Cor escura utilizada na interpolação.
	 * @return Uma imagem com a paleta de cores modificada.
	 * @see #createImage(PaletteMap[])
	 */
	public final Image createImage( int lightColor, int darkColor )
	{
		//#if DEBUG == "true"
//# 			try {
		//#endif
		// Inicializa a tabela de CRCs
		if( crcTable == null )
			fillCRCTable();
		
		// Não precisa armazenar o CRC original, pois a cada mudança teremos que calcular um novo CRC
		//final byte[] oldCRC = new byte[4];
		//System.arraycopy( rawImage, paletteStart + paletteSize, oldCRC, 0, 4 );

		// Armazena a paleta original da imagem e o CRC do chunk
		final byte[] palette = new byte[paletteSize];
		System.arraycopy( rawImage, PALETTE_START, palette, 0, palette.length );
		
		final int lightR = ( lightColor & 0x00FF0000 ) >> 16;
		final int lightG = ( lightColor & 0x0000FF00 ) >>  8;
		final int lightB = lightColor & 0x000000FF;
		final int darkR = ( darkColor & 0x00FF0000 ) >> 16;
		final int darkG = ( darkColor & 0x0000FF00 ) >>  8;
		final int darkB = darkColor & 0x000000FF;
		
		// Modifica a paleta original de acordo com o parâmetro map
		int paletteEntryIndex = PALETTE_START;
		for( int i = 0; i < nColors ; ++i )
		{
			// Otimiza a divisão por 255 com >> 8, ignorando a falta de precisão, já que esta será muito pequena
			final int r = /*((*/ 0x000000FF & rawImage[ paletteEntryIndex     ];// ) * lightR ) >> 8;
			final int g = /*((*/ 0x000000FF & rawImage[ paletteEntryIndex + 1 ];// ) * lightG ) >> 8;
			final int b = /*((*/ 0x000000FF & rawImage[ paletteEntryIndex + 2 ];// ) * lightB ) >> 8;
			final int luminance = ( ( r * 30 ) + ( g * 59 ) + ( b * 11 ) ) >> 8;

			// R
			rawImage[paletteEntryIndex    ] = ( byte )NanoMath.lerpInt( darkR, lightR, luminance );
			// G
			rawImage[paletteEntryIndex + 1] = ( byte )NanoMath.lerpInt( darkG, lightG, luminance );
			// B
			rawImage[paletteEntryIndex + 2] = ( byte )NanoMath.lerpInt( darkB, lightB, luminance );
			
			// Vai para a próxima cor da paleta
			paletteEntryIndex += 3;
		}

		// Calcula o novo CRC do chunk
		// chunkPLTEStart + 4 => não inclui o campo length no cálculo do CRC
		// paletteSize + 4 => paleta + nome do chunk ( "PLTE" )
		final byte[] crc = NanoMath.longToByteArray ( calculateCRC( rawImage, CHUNK_PLTE_START + 4, paletteSize + 4 ) );
		
		// Escreve o novo CRC do chunk
		// 4 + i => Um long possui 8 bytes. Só estamos interessados nos últimos 4 bytes
		for( int i = 0 ; i < 4 ; ++i )
			rawImage[PALETTE_START + paletteSize + i] = crc[4 + i];

		// Cria a nova imagem
		//#if J2SE == "false"
			final Image newImage = Image.createImage( rawImage, 0, rawImage.length );
		//#else
//# 			BufferedImage newImage = null;
//# 			try {
//# 				final ByteArrayInputStream input = new ByteArrayInputStream( rawImage );
//# 				newImage = ImageIO.read( input );
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
//# 			}			
		//#endif

		// Volta à paleta original
		System.arraycopy( palette, 0, rawImage, PALETTE_START, palette.length );

		// Não precisa voltar com o CRC original, pois na próxima mudança teremos que calcular um novo CRC
		// Escreve o CRC antigo
		//System.arraycopy( oldCRC, 0, rawImage, paletteStart + paletteSize, 4 );

		// Retorna a imagem criada
		return newImage;
		
		//#if DEBUG == "true"
//# 		}
//# 		catch( Exception ex ) {
//# 			ex.printStackTrace();
//# 			return null;
//# 		}
		//#endif
	}


	public static final int getRed( int color ) {
		return ( color & 0x00ff0000 ) >> 16;
	}


	public static final int getGreen( int color ) {
		return ( color & 0x0000ff00 ) >> 8;
	}


	public static final int getBlue( int color ) {
		return ( color & 0x000000ff );
	}


	/**
	 * 
	 * @param color
	 * @return
	 */
	public static final int getLuminance( int color ) {
		return ( ( getRed( color )   * 30 ) +
			     ( getGreen( color ) * 59 ) +
				 ( getBlue( color )  * 11 ) ) >> 8;
	}


//	/**
//	 *
//	 * @param color
//	 * @param luminance
//	 * @return
//	 */
//	public static final int changeLuminance( int color, int luminance ) {
//		final int r = getRed( color );
//		final int g = getGreen( color );
//		final int b = getBlue( color );
//
//		final int y = ( (  66 * r + 129 * g +  25 * b + 128) >> 8) +  16;
//		final int u = ( ( -38 * r -  74 * g + 112 * b + 128) >> 8) + 128;
//		final int v = ( ( 112 * r -  94 * g -  18 * b + 128) >> 8) + 128;
//
//		final int fp_1164 = NanoMath.divInt( 1164, 1000 );
//		final int fp_2018 = NanoMath.divInt( 2018, 1000 );
//		final int fp_0391 = NanoMath.divInt( 391, 1000 );
//		final int fp_0813 = NanoMath.divInt( 813, 1000 );
//		final int fp_1596 = NanoMath.divInt( 1596, 1000 );
//
//		final int fp_y = NanoMath.mulFixed( fp_1164, NanoMath.toFixed( luminance - 16 ) );
//		final int fp_u = NanoMath.toFixed( u - 128 );
//		final int fp_v = NanoMath.toFixed( v - 128 );
//
//		final int b2 = NanoMath.clamp( NanoMath.toInt( fp_y + NanoMath.mulFixed( fp_2018, fp_u ) ), 0, 0xff );
//		final int g2 = NanoMath.clamp( NanoMath.toInt( fp_y - NanoMath.mulFixed( fp_0813, fp_v ) - NanoMath.mulFixed( fp_0391, fp_u ) ), 0, 0xff );
//		final int r2 = NanoMath.clamp( NanoMath.toInt( fp_y + NanoMath.mulFixed( fp_1596, fp_v ) ), 0, 0xff);
//
//		return ( r2 << 16 ) | ( g2 << 8 ) | b2;
//	}
	
	
	/** Retorna o índice do início da cor da paleta em rawImage que está mapeada em map.
	 * 
	 * @param map O mapeamento de cores que contém a cor procurada na paleta
	 * @return O índice do início da cor da paleta em rawImage ou -1 caso a cor não tenha sido encontrada
	 */
	private final int getPaletteEntryIndex( PaletteMap map )
	{
		// Percorre a paleta de cores da imagem
		for( int i = 0 ; i < nColors ; ++i )
		{
			final int aux = PALETTE_START + ( i * 3 );
			
			// Verifica se encontramos a cor mapeada
			if( ( rawImage[aux] == map.srcR ) && ( rawImage[aux + 1] == map.srcG ) && ( rawImage[aux + 2] == map.srcB ) )
				return aux;
		}
		// A cor mapeada não existe na paleta de cores da imagem
		return -1;
	}
	
	
	// Na forma antiga, percorríamos a paleta de cores procurando um mapeamento para cada uma delas.
	// Invertemos a lógica para tentar diminuir o processamento
//	private final int getMappedColor( byte r, byte g, byte b, PaletteMap[] map )
//	{
//		for( int i = 0; i < map.length; ++i )
//		{
//			if( ( map[i].srcR == r ) && ( map[i].srcG == g ) && ( map[i].srcB == b ) )
//				return i;
//		}
//		return -1;
//	}

	
	public final void write( DataOutputStream output ) throws Exception
	{
	}

	
	public final void read( DataInputStream input ) throws Exception
	{
		// Preenche o array dinâmico
		final DynamicByteArray array = new DynamicByteArray();
		array.readInputStream( input );

		// Obtém os dados formatados
		rawImage = array.getData();
		
		// Armazena o tamanho da paleta e o número de cores contidas nela
		paletteSize = NanoMath.byteArrayToIntArray( rawImage, CHUNK_PLTE_START, 4 )[0];
		nColors = paletteSize / 3;
	}
	
	
	/** Calcula o CRC do chunk passado como parâmetro.
	 * 
	 * @param array Array de bytes que contém o chunk para o qual deseja-se calcular o CRC
	 * @param offset Índice do byte onde começam os dados que devem ser levados em conta no cálculo do CRC
	 * @param length Quantos bytes a partir de offset devemos utilizar no cálculo do CRC
	 * @return O CRC calculado para o chunk
	 */
	private final long calculateCRC( byte[] array, int offset, int length )
	{
		long crc = 0xFFFFFFFFL;
		
		for( int i = 0 ; i < length ; ++i )
			crc = crcTable[ ( int )( ( crc ^ array[ offset + i ] ) & 0xFF ) ] ^ ( crc >> 8 );

		return crc ^ 0xFFFFFFFFL;
	}
	
	
}
