/**
 * RankingFormatter.java
 * 
 * Created on 1/Fev/2009, 11:59:33
 *
 */

package br.com.nanogames.components.online;

/**
 * Essa interface é utilizada para inicializar tabelas de ranking local, e também para formatar a exibição da pontuação
 * na tabela de ranking. É importante que os valores iniciais da tabela respeitem a ordem (crescente ou decrescente) do
 * ranking, para evitar tabelas inconsistentes.
 * @author Peter
 */
public interface RankingFormatter {

	/**
	 * Formata uma pontuação.
	 * @param type tipo de ranking.
	 * @param score pontuação a ser formatada.
	 * @return string representando a pontuação de acordo com a formatação especificada.
	 */
	public String format( int type, long score );
	
	
	/**
	 * Inicializa uma entrada de ranking local.
	 * @param type tipo de ranking.
	 * @param entry entrada a ser formatada.
	 * @param index índice da entrada no ranking, sendo o índice zero a melhor pontuação.
	 */
	public void initLocalEntry( int type, RankingEntry entry, int index );
}
