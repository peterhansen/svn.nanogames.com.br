/**
 * AdManager.java
 *
 * Created on 11:12:53 AM.
 *
 */
package br.com.nanogames.components.online.ad;

import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreFullException;


/**
 *
 * @author Peter
 */
public final class AdManager implements NanoOnlineConstants {

	/** Versão do gerenciador de anúncios. */
	protected static final String AD_MANAGER_VERSION = "0.0.1";

	/** Nome da base de dados do gerenciador de recursos. */
	public static final String AD_MANAGER_DATABASE_NAME = "am";

	/***/
	private static final byte SLOT_AD_INDEXES	= 1;

	/***/
	private static final byte SLOT_AD_VIEWS		= 2;

	/** Quantidade inicial de slots no record store. */
	private static final byte SLOTS_INITIAL		= 2;

	private static final byte TABLE_INDEX_ADS		= 0;
	private static final byte TABLE_INDEX_VIEWS		= 1;
	private static final byte TABLE_INDEX_BOTH		= 2;

	/** Id de parâmetro do gerenciador de anúncios: versão do gerenciador. Formato dos dados: string */
	protected static final byte PARAM_AD_MANAGER_VERSION		= 0;

	/** Id de parâmetro do gerenciador de anúncios: quantidade de anúncios. Formato dos dados: short */
	protected static final byte PARAM_N_ADS						= PARAM_AD_MANAGER_VERSION + 1;

	/** Id de parâmetro do gerenciador de anúncios: id do anúncio. Formato dos dados: int */
	protected static final byte PARAM_AD_VERSION_ID						= PARAM_N_ADS + 1;

	/** Id de parâmetro do gerenciador de anúncios: id da versão do canal do anúncio. Formato dos dados: int */
	protected static final byte PARAM_AD_CHANNEL_VERSION_ID				= PARAM_AD_VERSION_ID + 1;

	/** Id de parâmetro do gerenciador de anúncios: quantidade de informações de visualização. Formato dos dados:
	 * int indicando o id do anúncio
	 * int indicando o id do usuário
	 * short indicando o número de entradas, e para cada entrada:
	 *	 long indicando a hora de início da visualização
	 *	 int indicando a duração da visualização
	 */
	protected static final byte PARAM_AD_VIEWS				= PARAM_AD_CHANNEL_VERSION_ID + 1;

	/** Id de parâmetro do gerenciador de anúncios: quantidade de anúncios inativos (que devem ser apagados do aparelho).
	 * Formato dos dados: byte indicando a quantidade, e então N x int ids de anúncios. */
	protected static final byte PARAM_N_INACTIVE_ADS			= PARAM_AD_VIEWS + 1;

	/** Id de parâmetro do gerenciador de anúncios: data/hora de expiração do anúncio. Formato dos dados: long */
	protected static final byte PARAM_AD_EXPIRATION_TIME		= PARAM_N_INACTIVE_ADS + 1;

	/** Id de parâmetro do gerenciador de anúncios: quantidade de recursos de um anúncio. 
	 * Formato dos dados: short indicando a quantidade, e então N x int ids de recursos (baixados separadamente) */
	protected static final byte PARAM_AD_N_RESOURCES			= PARAM_AD_EXPIRATION_TIME + 1;

	/** Id de parâmetro do gerenciador de anúncios: fim da leitura/gravação de dados do gerenciador de anúncios.
	 * Formato dos dados: nenhum */
	protected static final byte PARAM_AD_MANAGER_END			= PARAM_AD_N_RESOURCES + 1;

	/** Id de parâmetro do gerenciador de anúncios: fim da leitura/gravação de um anúncio.
	 * Formato dos dados: nenhum */
	protected static final byte PARAM_AD_END					= PARAM_AD_MANAGER_END + 1;

	/** Mapeamento de slots de anúncios. A chave é Integer com o id do ad, valor é Integer indicando o slot onde está salvo. */
	private final Hashtable tableAds = new Hashtable();

	/***/
	private final Vector adViewsSlots = new Vector();

	/***/
	private static AdManager instance;

	/***/
	private final RecordStore rsAds;
	

	private AdManager() throws Exception {
		final boolean newDatabase = AppMIDlet.createDatabase( AD_MANAGER_DATABASE_NAME, SLOTS_INITIAL );
		rsAds = RecordStore.openRecordStore( AD_MANAGER_DATABASE_NAME, false );

		if ( newDatabase ) {
			// acabou de criar a base de dados; preenche com valores iniciais
			instance = this;
			save( TABLE_INDEX_BOTH );
		}
	}


	/**
	 *
	 * @throws java.lang.Exception
	 */
	private static final void save( byte tablesToSave ) throws Exception {
		ByteArrayOutputStream byteOutput = null;
		DataOutputStream dataOutput = null;

		byte start;
		byte end;
		switch ( tablesToSave ) {
			case TABLE_INDEX_BOTH:
				start = SLOT_AD_INDEXES;
				end = SLOT_AD_VIEWS;
			break;

			default:
				start = end = tablesToSave;
		}

		for ( byte slot = start, tableIndex = 0; slot <= end; ++slot, ++tableIndex ) {
			try {
				byteOutput = new ByteArrayOutputStream();
				dataOutput = new DataOutputStream( byteOutput );

				dataOutput.writeUTF( AD_MANAGER_VERSION );

				switch ( slot ) {
					case SLOT_AD_VIEWS:
						//#if DEBUG == "true"
//# 							System.out.println( "GRAVANDO " + instance.adViewsSlots.size() + " AD VIEWS SLOTS" );
						//#endif
						dataOutput.writeShort( instance.adViewsSlots.size() );
						for ( Enumeration e = instance.adViewsSlots.elements(); e.hasMoreElements(); ) {
							final int value = ( ( Integer ) e.nextElement() ).intValue();
							//#if DEBUG == "true"
//# 								System.out.println( "\t->SLOT: " + value );
							//#endif
							dataOutput.writeInt( value );
						}
					break;

					case SLOT_AD_INDEXES:
						final Hashtable t = instance.tableAds;
						dataOutput.writeShort( t.size() );
						//#if DEBUG == "true"
//# 							System.out.println( "GRAVANDO " + t.size() );
						//#endif
						for ( Enumeration e = instance.tableAds.keys(); e.hasMoreElements();  ) {
							final Integer key = ( Integer ) e.nextElement();
							//#if DEBUG == "true"
//# 								System.out.println( "\t->AD INDEX: " + key + ", " + instance.tableAds.get( key ) );
							//#endif
							dataOutput.writeInt( key.intValue() );
							dataOutput.writeInt( ( ( Integer ) instance.tableAds.get( key ) ).intValue() );
						}
					break;

					//#if DEBUG == "true"
//# 						default:
//# 							throw new IllegalStateException( "Erro ao gravar RMS do AdManager - slot inválido: " + slot );
					//#endif
				}

				dataOutput.flush();

				final byte[] data = byteOutput.toByteArray();
				instance.rsAds.setRecord( slot, data, 0, data.length );
			} catch ( RecordStoreFullException rsfe ) {
				//#if DEBUG == "true"
	//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados: não há espaço disponível." );
				//#else
					throw rsfe;
				//#endif
			} catch ( InvalidRecordIDException ire ) {
				//#if DEBUG == "true"
	//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inválido: " + slot );
				//#else
					throw ire;
				//#endif
			} finally {
				if ( byteOutput != null ) {
					try {
						byteOutput.close();
					} catch ( Exception e ) {}
				} // fim if ( byteOutput != null )

				if ( dataOutput != null ) {
					try {
						dataOutput.close();
					} catch ( Exception e ) {}
				} // fim if ( dataOutput != null )
			} // fim finally
		} // fim for ( byte slot = start, tableIndex = 0; slot <= end; ++slot, ++tableIndex )
	}


	/**
	 * Carrega o gerenciador de anúncios. Esse método deve ser chamado antes de qualquer operação de leitura ou escrita de
	 * dados de anúncios.
	 * @throws java.lang.Exception
	 * @see #unload()
	 */
	public static final void load() throws Exception {
		if ( instance == null ) {
			instance = new AdManager();

			// preenche a tabela de associação recurso x slot
			ByteArrayInputStream byteInput = null;
			DataInputStream dataInput = null;

			for ( byte slot = SLOT_AD_INDEXES, tableIndex = 0; slot <= SLOT_AD_VIEWS; ++slot, ++tableIndex ) {
				try {
					final byte[] data = instance.rsAds.getRecord( slot );
					byteInput = new ByteArrayInputStream( data );
					dataInput = new DataInputStream( byteInput );

					// lê a versão do gerenciador de anúncios
					final String version = dataInput.readUTF();
					//#if DEBUG == "true"
//# 						System.out.println( "AdManager version read: " + version );
					//#endif

					// TODO se a versão lida do AdManager no RMS for diferente, recria a base?
					final short total = dataInput.readShort();
					//#if DEBUG == "true"
//# 						System.out.println( "TOTAL ENTRIES READ: " + total );
					//#endif

					switch ( slot ) {
						case SLOT_AD_VIEWS:
							for ( short i = 0; i < total; ++i ) {
								final int value = dataInput.readInt();
								//#if DEBUG == "true"
//# 									System.out.println( "\t->ad view slot read: " + value );
								//#endif
								instance.adViewsSlots.addElement( new Integer( value ) );
							}
						break;

						case SLOT_AD_INDEXES:
							final Hashtable t = instance.tableAds;
							for ( short i = 0; i < total; ++i ) {
								final int key = dataInput.readInt();
								final int value = dataInput.readInt();
								//#if DEBUG == "true"
//# 									System.out.println( "\t->key, value read: " + key + ", " + value );
								//#endif
								t.put( new Integer( key ), new Integer( value ) );
							}
						break;
					}
				} finally {
					// libera os recursos dos streams
					if ( byteInput != null ) {
						try {
							byteInput.close();
						} catch ( Exception e ) {}
						byteInput = null;
					}

					if ( dataInput != null ) {
						try {
							dataInput.close();
						} catch ( Exception e ) {}
						dataInput = null;
					}
				}
			}
		}
	}


	/**
	 * Descarrega o gerenciador de anúncios da memória. Métodos chamados posteriormente sem uma chamada a <code>load</code>
	 * lançarão exceções.
	 * 
	 * @see #load()
	 */
	public static final void unload() {
		if ( instance != null ) {
			try {
				instance.rsAds.closeRecordStore();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
			instance = null;
		}
	}


	/**
	 * Retorna o anúncio atual para um determinado canal.
	 *
	 * @param adChannelId id do canal.
	 * @return referência para o Ad atual, ou null caso não seja encontrado o canal ou não haja anúncios para o canal.
	 */
	public static final Ad getAdByChannelId( int adChannelId ) {
		final int[] ids = getAllAdIds();
		// TODO melhorar o algoritmo de seleção de ad a partir do seu ad channel (atualmente carrega todas as informações dos ads do RMS)
		final Vector candidates = new Vector();
		for ( short i = 0; i < ids.length; ++i ) {
			final Ad ad = getAd( ids[ i ] );
			//#if DEBUG == "true"
//# 				System.out.println( "AD #" + ad.getVersionId() + ", CHANNEL VERSION #" + ad.getAdChannelVersionId() );
			//#endif
//			TODO teste if ( ad.getAdChannelVersionId() == adChannelId )
				candidates.addElement( ad );
		}

		//#if DEBUG == "true"
//# 			System.out.println( "AD CANDIDATES: " + candidates.size() );
		//#endif
		return ( Ad ) ( candidates.size() == 0 ? null : candidates.elementAt( NanoMath.randInt( candidates.size() ) ) );
	}


	/**
	 *
	 * @param adId
	 * @return
	 */
	public static final Ad getAd( int adId ) {
		try {
			final int slot = ( ( Integer ) instance.tableAds.get( new Integer( adId ) ) ).intValue();
			return new Ad( instance.rsAds.getRecord( slot ) );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif
				
			return null;
		}
	}


	/**
	 * 
	 * @return
	 */
	private static final int[] getAllAdIds() {
		final Enumeration keys = instance.tableAds.keys();
		final Vector adsVector = new Vector();
		while ( keys.hasMoreElements() ) {
			adsVector.addElement( keys.nextElement() );
		}

		final int[] adIds = new int[ adsVector.size() ];
		for ( short i = 0; i < adIds.length; ++i )
			adIds[ i ] = ( ( Integer ) adsVector.elementAt( i ) ).intValue();

		return adIds;
	}


	/**
	 *
	 * @param adId
	 * @return
	 */
	public static final boolean hasAd( final int adId ) {
		return instance.tableAds.containsKey( new Integer( adId ) );
	}


	/**
	 *
	 * @param ad
	 * @param saveTables
	 * @throws java.lang.Exception
	 */
	private static final void addAd( final Ad ad, final boolean saveTables ) throws Exception {
		ByteArrayOutputStream byteOutput = null;
		DataOutputStream dataOutput = null;

		try {
			byteOutput = new ByteArrayOutputStream();
			dataOutput = new DataOutputStream( byteOutput );

			//#if DEBUG == "true"
//# 				System.out.println( "AddAd: " + ad.getVersionId() + ", " + saveTables );
			//#endif

			ad.write( dataOutput );
			dataOutput.flush();

			final byte[] data = byteOutput.toByteArray();
			final int newSlotIndex = instance.rsAds.addRecord( data, 0, data.length );
			instance.tableAds.put( new Integer( ad.getVersionId() ), new Integer( newSlotIndex ) );

			//#if DEBUG == "true"
//# 				System.out.println( "AD #" + ad.getVersionId() + " SALVO NO SLOT #" + newSlotIndex );
			//#endif

			if ( saveTables )
				save( TABLE_INDEX_ADS );

            //#if DEBUG == "true"
//#                 printAds();
            //#endif
		}
		//#if DEBUG == "true"
//# 			catch ( RecordStoreFullException rsfe ) {
//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados: não há espaço disponível." );
//# 			} catch ( InvalidRecordIDException ire ) {
//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inválido: " );
//# 			}
		//#endif
		finally {
			if ( byteOutput != null ) {
				try {
					byteOutput.close();
				} catch ( Exception e ) {}
			} // fim if ( byteOutput != null )

			if ( dataOutput != null ) {
				try {
					dataOutput.close();
				} catch ( Exception e ) {}
			} // fim if ( dataOutput != null )
		}
	}


	/**
	 *
	 * @param resourceId
	 * @param tableIndex
	 */
	private static final void deleteAd( final int adId ) {
		try {
			final int slot = ( ( Integer ) instance.tableAds.get( new Integer( adId ) ) ).intValue();
			final Ad ad = getAd( adId );

			instance.rsAds.deleteRecord( slot );
			instance.tableAds.remove( new Integer( adId ) );

			if ( ad != null ) {
				final int[] resourceIds = ad.getResourceIds();
				for ( short i = 0; i < resourceIds.length; ++i ) {
					final Resource res = ResourceManager.getResource( resourceIds[ i ] );
					if ( res.decrementUse() ) {
						// salva o recurso no RMS
						ResourceManager.saveResource( res );
					} else {
						// recurso não tem mais uso; apaga do RMS
						ResourceManager.delete( res );
					}
				}
			}
		} catch ( InvalidRecordIDException ex ) {
			//#if DEBUG == "true"
//# 				ex.printStackTrace();
			//#endif
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif
		}
	}
	

	/**
	 * Grava as visualizações de um canal publicitário.
	 *
	 * @param visualizations Visualizações desse canal. A chave do Hashtable é o id do usuário, e a chave é um Vector
	 * contendo vários objetos Point, cujos x indicam a hora de início da visualização e y a duração.
	 * @param ad anúncio cujos dados de visualização serão gravados.
	 */
	protected static final void saveVisualizations( Hashtable visualizations, Ad ad ) throws Exception {
		ByteArrayOutputStream byteOutput = null;
		DataOutputStream dataOutput = null;

		for ( Enumeration customers = visualizations.keys(); customers.hasMoreElements(); ) {
			try {
				byteOutput = new ByteArrayOutputStream();
				dataOutput = new DataOutputStream( byteOutput );

				final Integer key = ( Integer ) customers.nextElement();
				final Vector values = ( Vector ) visualizations.get( key );

				int customerId = key.intValue();
				final int slot = createVisualizationsSlot();

				dataOutput.writeInt( ad.getVersionId() );
				dataOutput.writeInt( customerId );
				dataOutput.writeShort( values.size() );
				//#if DEBUG == "true"
//# 					System.out.println( "SAVING VIEWS: " + ad.getVersionId() + ", " + customerId + ", " + values.size() );
				//#endif
				for ( Enumeration times = values.elements(); times.hasMoreElements(); ) {
					final Point p = ( Point ) times.nextElement();
					//#if DEBUG == "true"
//# 						System.out.println( "\t->VIEW: " + ( AdChannel.START_TIME + p.x ) + ", " + p.y );
					//#endif
					dataOutput.writeLong( AdChannel.START_TIME + p.x );
					dataOutput.writeInt( p.y );
				}
				dataOutput.flush();

				final byte[] data = byteOutput.toByteArray();
				instance.rsAds.setRecord( slot, data, 0, data.length );
			}
			//#if DEBUG == "true"
//# 				catch ( RecordStoreFullException rsfe ) {
	//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados do ad manager: não há espaço disponível." );
//# 				} catch ( InvalidRecordIDException ire ) {
	//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados do ad manager: slot inválido: " );
//# 				}
			//#endif
			finally {
				if ( byteOutput != null ) {
					try {
						byteOutput.close();
					} catch ( Exception e ) {}
				} // fim if ( byteOutput != null )

				if ( dataOutput != null ) {
					try {
						dataOutput.close();
					} catch ( Exception e ) {}
				} // fim if ( dataOutput != null )
			}
		}
		
		save( TABLE_INDEX_VIEWS );
	}


	/**
	 * 
	 * @param adId
	 * @param customerId
	 * @param createIfNecessary
	 * @return
	 */
	private static final int createVisualizationsSlot() {
		try {
			final int newSlotIndex = instance.rsAds.addRecord( null, 0, 0 );
			instance.adViewsSlots.addElement( new Integer( newSlotIndex ) );
			return newSlotIndex;
		} catch ( Exception e ) {
			return -1;
		}
	}


	/**
	 *
	 * @param input
	 * @throws java.lang.Exception
	 */
	public static final void readGlobalData( DataInputStream input ) throws Exception {
		load();
		ResourceManager.load();
		
		byte paramId;
		boolean readingData = true;

		do {
			paramId = input.readByte();

			switch ( paramId ) {
				case PARAM_N_INACTIVE_ADS:
					// lê os ids de anúncios inativos (que devem ser apagados)
					final byte totalInactiveAds = input.readByte();
					for ( byte i = 0; i < totalInactiveAds; ++i ) {
						deleteAd( input.readInt() );
					}
				break;

				case PARAM_AD_MANAGER_END:
					readingData = false;
				break;

				case PARAM_N_ADS:
					final short totalAds = input.readShort();
					//#if DEBUG == "true"
//# 						System.out.println( "LENDO " + totalAds + " ADS" );
					//#endif
					for ( short i = 0; i < totalAds; ++i ) {
						// aloca um anúncio "dummy" para leitura dos dados
						final Ad dummy = new Ad();
						dummy.parseParams( input );

						// verifica se o anúncio já existe
						final boolean exists = hasAd( dummy.getVersionId() );
						//#if DEBUG == "true"
//# 							System.out.println( "EXISTE(ANTES)? " + exists );
						//#endif
						if ( !exists ) {
							// recurso não existe - cria um novo
							addAd( dummy, false );
						}
						//#if DEBUG == "true"
//# 							System.out.println( "EXISTE(DEPOIS)? " + hasAd( dummy.getVersionId() ) );
						//#endif

						// adiciona os filhos (recursos), incrementando seus contadores de uso
						final int[] adResources = dummy.getResourceIds();
						for ( short childIndex = 0; childIndex < adResources.length; ++childIndex ) {
							Resource r = ResourceManager.getResource( adResources[ childIndex ] );
							if ( r == null ) {
								r = new Resource();
								r.setId( adResources[ childIndex ] );
								r.setType( ResourceTypes.RESOURCE_TYPE_NONE );
								r.incrementUse();
								ResourceManager.addResource( r, false );
							} else {
								r.incrementUse();
								ResourceManager.saveResource( r );
							}
						}
					}
					//#if DEBUG == "true"
//# 						System.out.println( "FIM DA LEITURA DOS ADS" );
					//#endif
				break;

				//#if DEBUG == "true"
//# 					default:
//# 						throw new IllegalArgumentException( "Lido paramId inválido do ad manager: " + paramId );
				//#endif
			}
		} while ( readingData );

		// apaga os dados de visualizações de anúncios depois de receber os dados do servidor
		try {
			for ( Enumeration e = instance.adViewsSlots.elements(); e.hasMoreElements(); ) {
				final int slot = ( ( Integer ) e.nextElement() ).intValue();
				instance.rsAds.deleteRecord( slot );
			}
			instance.adViewsSlots.removeAllElements();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif
		}

		save( TABLE_INDEX_BOTH );
		ResourceManager.save();
	}


	/**
	 * Envia as informações globais de anúncios para o servidor.
	 * @param output
	 * @throws java.lang.Exception
	 */
	public static final void writeGlobalData( DataOutputStream output ) throws Exception {
		load();
		
		output.writeByte( ID_AD_SERVER_DATA );

		// informa a versão do news feeder, para evitar que o servidor retorne informações num formato
		// mais novo (e possivelmente não suportado)
		output.writeByte( PARAM_AD_MANAGER_VERSION );
		output.writeUTF( AD_MANAGER_VERSION );

		// envia a lista atual de anúncios no aparelho - o servidor usa essa lista para selecionar quais anúncios enviar
		// de volta, e também informar ao aparelho quais anúncios antigos deve apagar
		output.writeByte( PARAM_N_ADS );
		output.writeShort( instance.tableAds.size() );
		for ( Enumeration e = instance.tableAds.keys(); e.hasMoreElements(); ) {
			output.writeInt( ( ( Integer ) e.nextElement() ).intValue() );
		}

		// envia informações sobre a visualização de ads no aparelho
		//#if DEBUG == "true"
//# 			System.out.println( "->SENDING VIEW SLOTS: " + instance.adViewsSlots.size() );
		//#endif
		if ( instance.adViewsSlots.size() > 0 ) {
			for ( Enumeration slots = instance.adViewsSlots.elements(); slots.hasMoreElements(); ) {
				final int slot = ( ( Integer) slots.nextElement() ).intValue();

				// preenche a tabela de associação recurso x slot
				ByteArrayInputStream byteInput = null;
				DataInputStream dataInput = null;
				final ByteArrayOutputStream tempOutput = new ByteArrayOutputStream();
				final DataOutputStream tempDataOutput = new DataOutputStream( tempOutput );

				try {
					// escreve os dados primeiro num stream temporário, para que eventuais problemas de leitura
					// dos dados não resultem em envio de "lixo" para o servidor
					final byte[] data = instance.rsAds.getRecord( slot );
					byteInput = new ByteArrayInputStream( data );
					dataInput = new DataInputStream( byteInput );

					// id do anúncio
					final int adId = dataInput.readInt();
					tempDataOutput.writeInt( adId );
					// id do usuário
					final int customerId = dataInput.readInt();
					tempDataOutput.writeInt( customerId );
					final short entries = dataInput.readShort();
					tempDataOutput.writeShort( entries );

					//#if DEBUG == "true"
//# 						System.out.println( "\tAD VIEW-> AD #" + adId + ", CUSTOMER #" + customerId + " -> " + entries + " SLOTS" );
					//#endif
					for ( short i = 0; i < entries; ++i ) {
						final long start = dataInput.readLong();
						// hora de início da visualização
						tempDataOutput.writeLong( start );
						// duração da visualização
						final int duration = dataInput.readInt();
						tempDataOutput.writeInt( duration );
						
						//#if DEBUG == "true"
//# 							System.out.println( "\t\tAD VIEW: " + new Date( start ) + " -> " + new Date( start + duration ) );
						//#endif
					}

					// escreve todos os dados de uma só vez
					output.writeByte( PARAM_AD_VIEWS );
					output.write( tempOutput.toByteArray() );
				} finally {
					// libera os recursos dos streams
					if ( byteInput != null ) {
						try {
							byteInput.close();
						} catch ( Exception e ) {}
					}

					if ( dataInput != null ) {
						try {
							dataInput.close();
						} catch ( Exception e ) {}
					}
				}
			}
		}

		output.writeByte( PARAM_AD_MANAGER_END );
	}


    //#if DEBUG == "true"
//#         public static final void printAds() {
//#             System.out.println( "\n----> LISTA DE ANÚNCIOS: <----" );
//#                 final Enumeration keys = instance.tableAds.keys();
//#                 while ( keys.hasMoreElements() ) {
//#                     final Integer key = ( Integer ) keys.nextElement();
//#                     System.out.println("AD ID " + key + " -> SLOT " + instance.tableAds.get( key ) );
//#                 }
//#             }
    //#endif

}
