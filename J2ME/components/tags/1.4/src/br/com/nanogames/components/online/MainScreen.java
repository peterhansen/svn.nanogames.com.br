/**
 * MainScreen.java
 * 
 * Created on Dec 16, 2008, 3:00:17 PM
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.online.newsfeeder.NewsFeederScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;

/**
 *
 * @author Peter
 */
public class MainScreen extends NanoOnlineContainer {

	/** Quantidade total de slots. */
	private static final byte TOTAL_SLOTS = 10;

	/***/
	private static final byte ENTRY_PROFILES	= 0;
	/***/
	private static final byte ENTRY_RECORDS		= ENTRY_PROFILES + 1;
	/***/
	private static final byte ENTRY_NEWS		= ENTRY_RECORDS + 1;
	/***/
	private static final byte ENTRY_HELP		= ENTRY_NEWS + 1;
	/***/
	private static final byte ENTRY_EXIT		= ENTRY_HELP + 1;

	private static final byte ENTRY_TOTAL		= ENTRY_EXIT + 1;

	
	public MainScreen() throws Exception {
		super( TOTAL_SLOTS, new FlowLayout( FlowLayout.AXIS_VERTICAL ) );
		
		final byte[] TEXTS = { TEXT_PROFILES, TEXT_RECORDS, TEXT_NEWS, TEXT_HELP_TITLE, TEXT_EXIT };

		for ( byte i = 0; i < ENTRY_TOTAL; ++i ) {
			final Component c = NanoOnline.getButton( TEXTS[ i ], i, this );
			insertDrawable( c );

			// permite uma melhor navegação no caso de um GridLayout
//			if ( i >= 2 ) {
//				getComponentAt( i - 2 ).setNextFocusDown( c );
//				c.setNextFocusUp( getComponentAt( ( i - 2 ) % ENTRY_TOTAL ) );
//			}
		}
		
		if ( NewsFeederScreen.hasUnreadEntries() ) {
			getComponentAt( ENTRY_NEWS ).setBorder( NanoOnline.getBorder( BORDER_COLOR_TYPE_HIGHLIGHT ) );
		}
	}
	
	
	public void eventPerformed( Event evt ) {
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( evt.source.getId() ) {
					case ENTRY_PROFILES:
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT, NanoOnline.isSubmittingNewRecord() ? SCREEN_NEW_RECORD : getId() );
					break;
					
					case ENTRY_RECORDS:
						NanoOnline.setScreen( SCREEN_RECORDS, getId() );
					break;
					
					case ENTRY_NEWS:
						NanoOnline.setScreen( SCREEN_NEWS, getId() );
					break;

					case ENTRY_HELP:
						NanoOnline.setScreen( SCREEN_HELP_MENU, getId() );
					break;
					
					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_EXIT:
						NanoOnline.exit();
					break;
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				switch ( ( ( Integer ) evt.data ).intValue() ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						NanoOnline.exit();
					break;
				}
			break;
		}
	}
	

}
