/**
 * TextScreen.java
 * 
 * Created on Oct 21, 2009, 12:06:51 PM
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;

/**
 *
 * @author Peter
 */
public final class TextScreen extends NanoOnlineContainer {

	public TextScreen( String text, final int backIndex ) throws Exception {
		super( 1, new BorderLayout() );

		final FormText formText = new FormText( NanoOnline.getFont( FONT_BLACK ) );
		formText.addEventListener( new EventListener() {
			public final void eventPerformed( Event evt ) {
				switch ( evt.eventType ) {
					case Event.EVT_KEY_PRESSED:
						final int key = ( ( Integer ) evt.data ).intValue();

						switch ( key ) {
							case ScreenManager.KEY_BACK:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.FIRE:
							case ScreenManager.KEY_NUM5:
							case ScreenManager.KEY_SOFT_LEFT:
								NanoOnline.setScreen( backIndex );
							break;
						}
					break;
				}
			}
		} );
		formText.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		formText.setScrollBarV( NanoOnline.getScrollBarV() );
		formText.setText( text );

		formText.setMaxLines( 0 );
		insertDrawable( formText, BorderLayout.CENTER );
	}

}
