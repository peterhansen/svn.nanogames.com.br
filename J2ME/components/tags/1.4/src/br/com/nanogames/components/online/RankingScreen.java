/**
 * RankingScreen.java
 * 
 * Created on 1/Fev/2009, 11:55:59
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 *
 * @author Peter
 */
public final class RankingScreen extends NanoOnlineContainer implements ConnectionListener {

	/** Índice de parâmetro de submissão de ranking online: quantidade de entradas sendo enviadas. Tipo do dado: byte */
	public static final byte PARAM_N_ENTRIES	= 0;
	/** Índice de parâmetro de submissão de ranking online: subtipo do ranking. Tipo do dado: byte */
	public static final byte PARAM_SUB_TYPE		= 1;
	/** Índice de parâmetro de submissão de ranking online: quantidade de perfis extras. Tipo do dado: byte. Para cada entrada, 
	 * é lido um inteiro, indicando o id do perfil.
	 */
	public static final byte PARAM_N_OTHER_PROFILES = 2;
	/** Índice de parâmetro de submissão de ranking: fim dos dados. Tipo do dado: nenhum. */
	public static final byte PARAM_END = 3;
 
	/** índice de parâmetro de submissão de ranking online: quantidade de recordes sendo requisitados. Tipo do dado: short */
	public static final byte PARAM_LIMIT = 4;
	/** índice de parâmetro de submissão de ranking online: offset dos recordes sendo requisitados. Tipo do dado: int */
	public static final byte PARAM_OFFSET = 5;	

	/***/
	public static final String RANKING_DATABASE = "ro";
	
	/** Resultado de submissão de pontos: não é melhor pontuação local nem recorde pessoal. */
	public static final byte SCORE_STATUS_NONE							= 0;
	/** Resultado de submissão de pontos: novo recorde local. */
	public static final byte SCORE_STATUS_NEW_LOCAL_RECORD				= 1;
	/** Resultado de submissão de pontos: novo recorde pessoal (no aparelho atual - jogador pode ter submetido pontuação maior em outro aparelho). */
	public static final byte SCORE_STATUS_NEW_PERSONAL_RECORD			= 2;
	/** Resultado de submissão de pontos: novo recorde local E pessoal (no aparelho atual - jogador pode ter submetido pontuação maior em outro aparelho). */
	public static final byte SCORE_STATUS_NEW_PERSONAL_AND_LOCAL_RECORD	= SCORE_STATUS_NEW_LOCAL_RECORD | SCORE_STATUS_NEW_PERSONAL_RECORD;
	
	/** Máximo de entradas de ranking. */
	public static final byte LOCAL_RANKING_ENTRIES_TOTAL = 20;
	
	/***/
	private static final byte TOTAL_ENTRIES = 6;
	
	/** Botão para atualizar o ranking (somente ranking online). */
	private static final byte ID_BUTTON_UPDATE = TOTAL_ENTRIES;
	
	/** Botão para voltar (ou cancelar a conexão, no modo online). */
	private static final byte ID_BUTTON_BACK = ID_BUTTON_UPDATE + 1;
	
	/** Botão para apagar a lista de recordes. */
	private static final byte ID_BUTTON_ERASE = ID_BUTTON_BACK + 1;

	/** CheckBox para enviar os recordes pendentes no aparelho. */
	private static final byte ID_CHECKBOX_SUBMIT_RECORDS = ID_BUTTON_ERASE + 1;
	
	private final byte type;

	private final CheckBox checkBoxSubmit;
	
	/***/
	private static long lastScore;
	
	private static byte lastType;
	
	private static boolean lastDecrescent;

	private static boolean lastCumulative;

	/** Indica se o ranking é online ou local. */
	private final boolean online;
	
	/***/
	private static byte totalTypes;
	
	private static RankingFormatter formatter;
	
	private static int[] typesEntries;
	
	private final Button buttonUpdate;
	
	private final Button buttonBack;

	/** Contâiner das entradas de recordes. */
	private Container entriesContainer;
	
	/** Categoria de slot: entradas não submetidas. */
	private static final byte SLOT_CATEGORY_UNSUBMITTED_ENTRIES	= 0;
	/** Categoria de slot: recordes pessoais de cada jogador. */
	private static final byte SLOT_CATEGORY_PERSONAL_RECORDS	= SLOT_CATEGORY_UNSUBMITTED_ENTRIES + 1;
	/** Categoria de slot: recordes locais do jogo. */
	private static final byte SLOT_CATEGORY_LOCAL_RECORDS		= SLOT_CATEGORY_PERSONAL_RECORDS + 1;
	/** Categoria de slot: recordes globais (baixados do servidor). */
	private static final byte SLOT_CATEGORY_GLOBAL_RECORDS		= SLOT_CATEGORY_LOCAL_RECORDS + 1;
	/** Quantidade total de categorias de slots por tipo de ranking, ou seja, é a diferença entre uma mesma categoria de 
	 * slot de tipos diferentes. Por exemplo: entradas não submetidas tipo 0: índice no RMS = 1; entradas não submetidas 
	 * tipo 1: índice no RMS = 1 + SLOT_TOTAL.
	 */
	private static final byte SLOT_CATEGORIES_TOTAL = SLOT_CATEGORY_GLOBAL_RECORDS + 1;
	

	/**
	 * 
	 * @param online
	 * @param type
	 * @param backIndex
	 * @throws java.lang.Exception
	 */
	public RankingScreen( boolean online, int type, int backIndex ) throws Exception {
		super( TOTAL_ENTRIES );

		setBackIndex( backIndex );
		
		this.online = online;
		this.type = ( byte ) type;

		// tenta carregar as entradas de ranking salvas (se existirem)
		RankingEntry[] rankingEntries;
		final RankingLoader loader = new RankingLoader( type, online ? SLOT_CATEGORY_GLOBAL_RECORDS : SLOT_CATEGORY_LOCAL_RECORDS );
		loader.load();
		rankingEntries = loader.rankingEntries;

		if ( online ) {
			checkBoxSubmit = new CheckBox( NanoOnline.getCheckBoxButton(), NanoOnline.getFont( FONT_BLACK ), NanoOnline.getText( TEXT_SUBMIT_RECORDS ), true );
			buttonUpdate = NanoOnline.getButton( TEXT_UPDATE, ID_BUTTON_UPDATE, this );
		} else {
			buttonUpdate = null;
			checkBoxSubmit = null;
		}

		// não é necessário adicionar o botão de apagar caso a lista esteja vazia
		// TODO adicionar botão pra apagar na tela de recordes?
//		if ( entriesContainer.getUsedSlots() > 0 ) {
//			final Button erase = new Button( font, NanoOnline.getText( TEXT_ERASE_RECORDS ) );
//			erase.setId( ID_BUTTON_ERASE );
//			erase.setBorder( NanoOnline.getBorder() );
//			erase.addActionListener( this );
//
//			insertDrawable( erase );
//		}
		
		buttonBack = NanoOnline.getButton( TEXT_BACK, ID_BUTTON_BACK, this );
		refreshEntries( rankingEntries, loader.lastUpdated, false );
	}
	
	
	private final FlowLayout getFlowLayout() {
		final FlowLayout flowLayout = new FlowLayout( FlowLayout.AXIS_VERTICAL, ANCHOR_HCENTER );
		flowLayout.gap.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );
//		flowLayout.start.set( LAYOUT_START_X, LAYOUT_START_Y );		
		
		return flowLayout;
	}
	
	
	/**
	 * Inicializa as tabelas de ranking.
	 * 
	 * @param typesEntries índices dos textos (em AppMIDlet) correspondentes a cada tipo de ranking. Esses serão os títulos
	 * das entradas mostradas no menu de ranking. Cada tipo possui sua própria tabela local e global.
	 * @param rankingFormatter formatador do ranking. Caso seja null, todos as entradas de ranking local são inicializadas
	 * com pontuação zero, e com apelidos vazios, e as pontuações são exibidas como inteiros.
	 * @throws java.lang.Exception
	 */
	public static final void init( int[] typesEntries, RankingFormatter rankingFormatter ) throws Exception {
		formatter = rankingFormatter;
		RankingScreen.typesEntries = typesEntries;
		RankingScreen.totalTypes = ( byte ) typesEntries.length;
		
		// a quantidade total de slots é dada pelo total de tipos de ranking, sendo que cada uma precisa de SLOT_CATEGORIES_TOTAL
		// para armazenar as diferentes categorias de informações. Não é necessário verificar o valor de retorno desse método,
		// pois no caso de os slots não existirem antes, eles serão inicializados corretamente em RankingLoader, reduzindo
		// o overhead de inicialização do jogo/aplicativo como um todo.
		AppMIDlet.createDatabase( RANKING_DATABASE, totalTypes * SLOT_CATEGORIES_TOTAL );
	}
	
	
	/**
	 * Verifica se uma pontuação é melhor marca pessoal de um jogador ou está entre as melhores marcas locais.
	 * 
	 * @param type índice da tabela de ranking (ex.: 1 por pista).
	 * @param profileId índice do perfil de usuário que obteve a pontuação testada. Utilizar valores negativos faz somente 
	 * a verificação de recordes locais.
	 * @param score pontuação a ser verificada.
	 * @param decrescent indica se a tabela de recorde é descrescente, ou seja, maiores pontuações no topo da tabela.
	 * @return status da pontuação:
	 * <ul>
	 * <li>SCORE_STATUS_NONE</li>
	 * <li>SCORE_STATUS_NEW_PERSONAL_RECORD</li>
	 * <li>SCORE_STATUS_NEW_LOCAL_RECORD</li>
	 * <li>SCORE_STATUS_NEW_PERSONAL_AND_LOCAL_RECORD</li>
	 * </ul>
	 */
	public static final byte isHighScore( int type, int profileId, long score, boolean decrescent, boolean cumulative ) throws Exception {
		byte scoreStatus = SCORE_STATUS_NONE;
		
		setLastInfo( type, score, decrescent, cumulative );
		
		// verifica a pontuação local
		if ( getScoreIndex( type, SLOT_CATEGORY_LOCAL_RECORDS, profileId, score, decrescent, cumulative ) >= 0 )
			scoreStatus |= SCORE_STATUS_NEW_LOCAL_RECORD;
				
		// verifica se é um novo recorde pessoal
		if ( getScoreIndex( type, SLOT_CATEGORY_PERSONAL_RECORDS, profileId, score, decrescent, cumulative ) >= 0 ) {
			// se for recorde pessoal, não precisa armazenar agora nos recordes a submeter, pois isso será feito automaticamente
			// ao gravar a melhor marca pessoal (se for armazenado aqui o recorde a submeter e o jogador trocar o perfil,
			// o recorde será armazenado duas vezes, com o agravante de ter 2 perfis diferentes)
			scoreStatus |= SCORE_STATUS_NEW_PERSONAL_RECORD;
		} else {
			// se não for um recorde pessoal, verifica se é pelo menos uma nova melhor marca não submetida, e a armazena
			if ( profileId >= 0 ) {
				final Customer customer = NanoOnline.getCustomerById( profileId );
				final int index = getScoreIndex( type, SLOT_CATEGORY_UNSUBMITTED_ENTRIES, profileId, score, decrescent, cumulative );

				if ( index >= 0 ) {
					// armazena a pontuação na lista de recordes a submeter
					final RankingLoader loader = new RankingLoader( type, SLOT_CATEGORY_UNSUBMITTED_ENTRIES );
					loader.load();

					if ( index >= loader.rankingEntries.length ) {
						// adiciona uma entrada
						loader.rankingEntries = increaseEntriesArray( loader.rankingEntries );
					}

					loader.rankingEntries[ index ].setProfileId( profileId );
					loader.rankingEntries[ index ].setNickname( customer.getNickname() );
					loader.rankingEntries[ index ].setScore( score );
					loader.save();
				}
			}
		}
		
		return scoreStatus;
	}
	

	/**
	 *
	 * @param type
	 * @param nickname
	 * @param score
	 * @param decrescent
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final byte setHighScore( int type, String nickname, long score, boolean decrescent, boolean cumulative ) throws Exception {
		return setHighScore( type, Customer.ID_NONE, nickname, score, decrescent, cumulative );
	}
		

	/**
	 * 
	 * @param type
	 * @param profileId
	 * @param score
	 * @param decrescent
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final byte setHighScore( int type, int profileId, long score, boolean decrescent, boolean cumulative ) throws Exception {
		return setHighScore( type, profileId, null, score, decrescent, cumulative );
	}
	

	/**
	 * 
	 * @param type
	 * @param profileId
	 * @param nickname
	 * @param score
	 * @param decrescent
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final byte setHighScore( int type, int profileId, String nickname, long score, boolean decrescent, boolean cumulative ) throws Exception {
		byte scoreStatus = SCORE_STATUS_NONE;
		
		final Customer customer = NanoOnline.getCustomerById( profileId );
		
		setLastInfo( type, score, decrescent, cumulative );
		
		// primeiro verifica o ranking local
		int index = getScoreIndex( type, SLOT_CATEGORY_LOCAL_RECORDS, profileId, score, decrescent, cumulative );
		if ( index >= 0 ) {
			scoreStatus |= SCORE_STATUS_NEW_LOCAL_RECORD;
			
			final RankingLoader loader = new RankingLoader( type, SLOT_CATEGORY_LOCAL_RECORDS );
			loader.load();
			final RankingEntry[] entries = loader.rankingEntries;
			
			moveOlderEntries( entries, index );
			entries[ index ].setProfileId( profileId );
			if ( customer == null )
				entries[ index ].setNickname( nickname == null ? "----" : nickname );
			else
				entries[ index ].setNickname( customer.getNickname() );
			
			entries[ index ].setScore( cumulative ? entries[ index ].getScore() + score : score );
			entries[ index ].setTimeStamp( System.currentTimeMillis() );
			
			loader.save();
		}
		
		// verifica as entradas não submetidas; caso tenha um recorde a submeter melhor que a pontuação recebida, NÃO é
		// novo recorde pessoal. Caso não seja encontrada uma entrada desse perfil, ou seja encontrada uma com pontuação
		// menor que a recebida, atualiza a melhor pontuação a ser enviada e verifica se é novo recorde pessoal.
		if ( profileId >= 0 ) {
			index = getScoreIndex( type, SLOT_CATEGORY_UNSUBMITTED_ENTRIES, profileId, score, decrescent, cumulative );
			
			if ( index >= 0 ) {
				// armazena a pontuação na lista de recordes a submeter
				RankingLoader loader = new RankingLoader( type, SLOT_CATEGORY_UNSUBMITTED_ENTRIES );
				loader.load();
				
				if ( index >= loader.rankingEntries.length ) {
					// adiciona uma entrada
					loader.rankingEntries = increaseEntriesArray( loader.rankingEntries );
				}
				
				loader.rankingEntries[ index ].setProfileId( profileId );
				loader.rankingEntries[ index ].setNickname( customer.getNickname() );
				loader.rankingEntries[ index ].setScore( cumulative ? loader.rankingEntries[ index ].getScore() + score : score );
				loader.rankingEntries[ index ].setTimeStamp( System.currentTimeMillis() );
				loader.save();

				// agora verifica se é melhor recorde pessoal
				index = getScoreIndex( type, SLOT_CATEGORY_PERSONAL_RECORDS, profileId, score, decrescent, cumulative );
				if ( index >= 0 ) {
					loader = new RankingLoader( type, SLOT_CATEGORY_PERSONAL_RECORDS );
					loader.load();
					if ( index >= loader.rankingEntries.length ) {
						// adiciona uma entrada
						loader.rankingEntries = increaseEntriesArray( loader.rankingEntries );
					}
					
					loader.rankingEntries[ index ].setProfileId( profileId );
					loader.rankingEntries[ index ].setNickname( customer.getNickname() );
					loader.rankingEntries[ index ].setScore( cumulative ? loader.rankingEntries[ index ].getScore() + score : score );
					loader.save();
					
					scoreStatus |= SCORE_STATUS_NEW_PERSONAL_RECORD;
				}
			}
		}
		
		return scoreStatus;
	}
	
	
	/**
	 * Faz o "chega-pra-lá" nas entradas do ranking, para inserção de novo recorde.
	 * @param entries
	 * @param initialIndex
	 */
	private static final void moveOlderEntries( RankingEntry[] entries, int initialIndex ) {
		for ( int i = entries.length - 1; i > initialIndex; --i ) {
			entries[ i ].copyFrom( entries[ i - 1 ] );
		}
	}
	
	
	private static final RankingEntry[] increaseEntriesArray( RankingEntry[] entries ) {
		final RankingEntry[] ret = new RankingEntry[ entries.length + 1 ];
		System.arraycopy( entries, 0, ret, 0, entries.length );
		ret[ entries.length ] = new RankingEntry();
		
		return ret;
	}
	

    /**
     * Une dois arrays de entradas de ranking, eliminando as duplicatas.
     * 
     * @param e1
     * @param e2
     * @return
     */
	private static final RankingEntry[] mergeEntries( RankingEntry[] e1, RankingEntry[] e2 ) {
		if ( e1.length <= 0 )
			return e2;
		else if ( e2.length <= 0 )
			return e1;

        final Vector v = new Vector( e1.length + e2.length );
        for ( byte i = 0; i < e1.length; ++i ) {
            v.addElement( e1[ i ] );
        }
        for ( byte index2 = 0; index2 < e2.length; ++index2 ) {
            boolean exists = false;
            for ( int index1 = index2 + 1; index1 < e1.length; ++index1 ) {
                if ( e1[ index1 ].getProfileId() == e2[ index2 ].getProfileId() ) {
                    exists = true;
                    break;
                }
            }

            if ( !exists )
                v.addElement( e2[ index2 ] );
        }

		final RankingEntry[] ret = new RankingEntry[ v.size() ];

		for ( int i = 0; i < ret.length; ++i ) {
            ret[ i ] = ( RankingEntry ) v.elementAt( i );

            int temp = i - 1;
            while ( temp > 0 && ret[ temp -1 ].getPosition() > ret[ temp ].getPosition() ) {
                final RankingEntry e = ret[ temp ];
                ret[ temp ] = ret[ temp -1 ];
                ret[ temp -1 ] = e;
                --temp;
            }
        }
		
		return ret;
	}
	
	
	private static final void setLastInfo( int type, long score, boolean decrescent, boolean cumulative ) {
		lastType = ( byte ) type;
		lastScore = score;
		lastDecrescent = decrescent;
		lastCumulative = cumulative;
	}
	
	
	/**
	 * Obtém a posição da pontuação de um jogador numa determinada tabela.
	 * 
	 * @param type índice da tabela de ranking (ex.: 1 por pista).
	 * @param category categoria do slot cuja posição do recorde será pesquisada.
	 * @param profileId índice do perfil de usuário que obteve a pontuação testada. Utilizar valores negativos faz somente 
	 * a verificação de recordes locais.
	 * @param score pontuação a ser verificada.
	 * @param decrescent indica se a tabela de recorde é descrescente, ou seja, maiores pontuações no topo da tabela.
	 * @return
	 */
	private static final int getScoreIndex( int type, int category, int profileId, long score, boolean decrescent, boolean cumulative ) {
		try {
			final boolean online = category != SLOT_CATEGORY_LOCAL_RECORDS;
			
			if ( online ) {
				// se for ranking online e o perfil do usuário for inválido, não há recorde a verificar
				if ( profileId != Customer.ID_NONE ) {
					final RankingLoader loader = new RankingLoader( type, category );
					loader.load();
					final RankingEntry[] entries = loader.rankingEntries;

					for ( byte i = 0; i < entries.length; ++i ) {
						if ( profileId == entries[ i ].getProfileId() ) {
							if ( cumulative || ( decrescent && score > entries[ i ].getScore() ) || ( !decrescent && score < entries[ i ].getScore() ) ) {
								// é um novo recorde do jogador
								return i;
							}

							// achou o jogador na lista, mas não é novo recorde
							return -1;
						}
					}

					// não achou o jogador na lista - ranking está cheio. Retorna o tamanho do array, para que a entrada
					// seja inserida no final do array posteriormente, pois garantidamente é uma melhor marca pessoal no
					// aparelho
					return entries.length;
				}
			} else if ( NanoOnline.getRankingTypes() != NanoOnline.RANKING_TYPES_GLOBAL_ONLY ) {
				final RankingLoader loader = new RankingLoader( type, category );
				loader.load();
				final RankingEntry[] entries = loader.rankingEntries;

				// verifica a pontuação local
				for ( byte i = 0; i < entries.length; ++i ) {
					if ( cumulative || ( decrescent && score > entries[ i ].getScore() ) || ( !decrescent && score < entries[ i ].getScore() ) ) {
						return i;
					}
				}
			}
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 				ex.printStackTrace();
			//#endif
		}
		
		return -1;
	}
	
	
	protected static final long getLastScore() {
		return lastScore;
	}
	
	
	protected static final byte getLastType() {
		return lastType;
	}
	
	
	protected static final boolean getLastDecrescent() {
		return lastDecrescent;
	}


	protected static final boolean getLastCumulative() {
		return lastCumulative;
	}
	
	
	protected static final byte getTotalTypes() {
		return totalTypes;
	}
	
	
	protected static final int[] getTypesEntries() {
		return typesEntries;
	}


	/**
	 * 
	 * @param evt
	 */
	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ID_BUTTON_UPDATE:
						// envia os recordes de cada perfil para o servidor
						try {
							final ByteArrayOutputStream b = new ByteArrayOutputStream();
							final DataOutputStream out = new DataOutputStream( b );

							// escreve os dados globais antes de adicionar as informações específicas para registro do usuário
							NanoOnline.writeGlobalData( out );
							
							// envia somente as entradas que não foram submetidas
							final RankingLoader loader = new RankingLoader( type, SLOT_CATEGORY_UNSUBMITTED_ENTRIES );
							loader.load();
							final RankingEntry[] entries = loader.rankingEntries;
							
							out.writeByte( PARAM_N_ENTRIES );
							if ( checkBoxSubmit.isChecked() ) {
								out.writeShort( entries.length );
								out.writeByte( PARAM_SUB_TYPE );
								// caso seja um ranking crescente, envia o valor negativo do tipo para informar isso ao servidor
								out.writeByte( getLastDecrescent() ? type : -type );

								//#if DEBUG == "true"
	//# 								System.out.println( "SUBMETENDO " + entries.length + " RECORDE(S):" );
								//#endif

								for ( byte i = 0; i < entries.length; ++i ) {
									out.writeInt( entries[ i ].getProfileId() );
									out.writeLong( entries[ i ].getScore() );
									out.writeLong( entries[ i ].getTimeStamp() );

									//#if DEBUG == "true"
	//# 									System.out.println( "RECORDE #" + i + " -> " + entries[ i ] );
									//#endif
								}
								
								// depois de submeter as entradas de ranking, envia também os ids dos outros jogadores,
								// para o servidor retornar suas posições mesmo que estejam fora do top 10
								final Customer[] allCustomers = NanoOnline.getCustomers();
								// se o jogador ainda não tiver criado ou importado nenhum perfil, o array pode ser nulo
								if ( allCustomers != null ) {
									final Vector v = new Vector();
									for ( byte i = 0; i < allCustomers.length; ++i ) {
										boolean found = false;
										for ( int j = 0; j < entries.length; ++j ) {
											if ( entries[ j ].getProfileId() == allCustomers[ i ].getId() ) {
												j = entries.length;
												found = true;
											}
										}
										if ( !found ) {
											// se chegou aqui, o perfil não fazia parte dos recordes enviados (não é necessário enviar de novo)
											v.addElement( new Integer( allCustomers[ i ].getId() ) );
										}
									}
									final int N_OTHER_CUSTOMERS = v.size();
									if ( N_OTHER_CUSTOMERS > 0 ) {
										out.writeByte( PARAM_N_OTHER_PROFILES );
										out.writeShort( N_OTHER_CUSTOMERS );
										for ( byte i = 0; i < N_OTHER_CUSTOMERS; ++i ) {
											out.writeInt( ( ( Integer ) v.elementAt( i ) ).intValue() );
										}
									}
								}
							} else {
								// jogador não quer enviar recordes no momento, apenas visualizar a tabela
								out.writeShort( 0 );
							}
							
							out.flush();
							
							NanoConnection.post( NANO_ONLINE_URL + "ranking_entries/submit", b.toByteArray(), this, false );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
						}
					break;
					
//					case ID_BUTTON_ERASE:
//						// TODO mostrar confirmação antes de efetivamante apagar os recordes
//						try {
//							eraseRecords( type, online );
//
//							NanoOnline.getProgressBar().showInfo( TEXT_RECORDS_ERASED );
//						} catch ( Exception e ) {
//							//#if DEBUG == "true"
////# 								e.printStackTrace();
//							//#endif
//						}
//						NanoOnline.setScreen( SCREEN_RECORDS );
//					break;
					
					case ProgressBar.ID_SOFT_RIGHT:
					case ID_BUTTON_BACK:
						onBack();
					break;
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )		
	} // fim do método eventPerformed( Event )


	/**
	 * 
	 */
	private final void refreshEntries( RankingEntry[] rankingEntries, long lastUpdated, boolean refreshLayout ) throws Exception {
		// TODO corrigir bug em insertDrawable( Component, int )
		removeDrawable( buttonBack );
		removeDrawable( buttonUpdate );
		removeDrawable( checkBoxSubmit );
		if ( entriesContainer != null ) {
			removeDrawable( entriesContainer, true );
			entriesContainer = null;
		}
		entriesContainer = new Container( rankingEntries.length + 2, getFlowLayout() );
		
		insertDrawable( entriesContainer );
		if ( checkBoxSubmit != null )
			insertDrawable( checkBoxSubmit );
		if ( buttonUpdate != null )
			insertDrawable( buttonUpdate );
		insertDrawable( buttonBack );

		// se for ranking online, indica a hora da última atualização
		if ( online && lastUpdated > 0 ) {
			final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 0, 0 );
			text.setText( NanoOnline.getText( TEXT_UPDATED_AT ) + AppMIDlet.formatTime( lastUpdated, NanoOnline.getText( TEXT_TIME_FORMAT ) ) );
			entriesContainer.insertDrawable( text );
		}
		
		if ( rankingEntries.length > 0 ) {
			for ( short total = 0; total < rankingEntries.length; ++total ) {
				final RankingEntry entry = rankingEntries[ total ];
				final StringBuffer buffer = new StringBuffer();

				buffer.append( entry.getPosition() > 0 ? entry.getPosition() : ( total + 1 ) );
				buffer.append( ". " );

				if ( formatter == null )
					buffer.append( entry.getScore() );
				else
					buffer.append( formatter.format( type, entry.getScore() ) );

				buffer.append( ' ' );
				buffer.append( entry.getNickname() );

				final Button button = NanoOnline.getButton( buffer.toString(), -1, null );
				button.setBorder( NanoOnline.getRankingBorder( total & 1 ) );
				button.setEnabled( false );
				button.addEventListener( this );
//				button.setBorder( NanoOnline.isCustomerSaved( entry.getProfileId() ) ? NanoOnline.getBorder() : NanoOnline.getBorder2() );
				entriesContainer.insertDrawable( button );
			}
		} else {
			final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 0, 0 );
			text.setText( NanoOnline.getText( TEXT_NO_RECORDS_FOUND ) );
			text.setFocusable( false );
			entriesContainer.insertDrawable( text );
		}

		if ( refreshLayout ) {
			refreshLayout();
			entriesContainer.requestFocus();
		}
	}
	

	/**
	 * 
	 * @param id
	 * @param data
	 */
	public final void processData( int id, byte[] data ) {
		if ( data != null ) {
			DataInputStream input = null;
			try {
				final Hashtable table = NanoOnline.readGlobalData( data );
				
				// o código de retorno é obrigatório
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				byte errorEntryIndex = -1;
				byte errorText = -1;

				final StringBuffer buffer = new StringBuffer();

				switch ( returnCode ) {
					case RC_OK:
						// grava as informações do usuário no RMS e mostra a tela de confirmação
						final byte[] specificData = ( byte[] ) table.get( new Byte( ID_SPECIFIC_DATA ) );
						input = new DataInputStream( new ByteArrayInputStream( specificData ) );
						
						final RankingLoader globalRecords = new RankingLoader( getLastType(), SLOT_CATEGORY_GLOBAL_RECORDS );
						byte paramId = -1;
						RankingEntry[] entries = new RankingEntry[ 0 ];
						RankingEntry[] extraEntries = new RankingEntry[ 0 ];
						
						do {
							paramId = input.readByte();
							
							switch ( paramId ) {
								case PARAM_N_ENTRIES:
									// primeiro lê um short indicando quantas entradas de ranking foram recebidas
									final short totalEntries = input.readShort();
									if ( totalEntries > 0 ) {
										entries = new RankingEntry[ totalEntries ];

										for ( short i = 0; i < entries.length; ++i ) {
											entries[ i ] = new RankingEntry();
											final RankingEntry e = entries[ i ];
											
											e.setPosition( i + 1 );
											// ordem de leitura: id do usuário (int), pontuação (long), nick (UTF)
											e.setProfileId( input.readInt() );
											e.setScore( input.readLong() );
											e.setNickname( input.readUTF() );
										}
									}
								break;
								
								case PARAM_N_OTHER_PROFILES:
									final short totalExtraEntries = input.readShort();
									extraEntries = new RankingEntry[ totalExtraEntries ];
									for ( byte i = 0; i < totalExtraEntries; ++i ) {
										extraEntries[ i ] = new RankingEntry();
										final RankingEntry e = extraEntries[ i ];
										
										e.setProfileId( input.readInt() );
										e.setScore( input.readLong() );
										e.setNickname( input.readUTF() );
										
										// por último, é lida a colocação do jogador
										e.setPosition( input.readInt() );
									}
								break;
							}
						} while ( paramId != PARAM_END );

						// apaga a lista de recordes não submetidos
						final RankingLoader submittedEntries = new RankingLoader( getLastType(), SLOT_CATEGORY_UNSUBMITTED_ENTRIES );
						submittedEntries.erase( true );
						
						entries = mergeEntries( entries, extraEntries );									
						
						// armazena o momento da atualização
						globalRecords.lastUpdated = System.currentTimeMillis();

						// salva as alterações no ranking
						globalRecords.rankingEntries = entries;
						globalRecords.save();
						refreshEntries( entries, globalRecords.lastUpdated, true );

						NanoOnline.getProgressBar().clearProgressBar();
						NanoOnline.getProgressBar().showInfo( buffer.toString() );
					break;

					case RC_APP_NOT_FOUND:
						NanoOnline.getProgressBar().showError( TEXT_ERROR_APP_NOT_FOUND );
					break;

					case RC_SERVER_INTERNAL_ERROR:
					default:
						// TODO deixar tratamento de erros genéricos (aparelho não suportado, erro interno do servidor, etc. em NanoOnlineContainer)
						NanoOnline.getProgressBar().showError( TEXT_ERROR_SERVER_INTERNAL );
					break;
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
		}
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		//#if DEBUG == "true"
//# 			System.out.println( "RankingScreen.onInfo: " + id + ", " + infoIndex + ", " + extraData );
		//#endif

		NanoOnline.getProgressBar().onInfo( id, infoIndex, extraData );
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		//#if DEBUG == "true"
//# 			System.out.println( "RankingScreen.onError: " + id + ", " + errorIndex + ", " + extraData );
		//#endif

		NanoOnline.getProgressBar().onError( id, errorIndex, extraData );
	}	

	
	/**
	 * 
	 */
	private static final class RankingLoader implements Serializable {

		/***/
		private RankingEntry[] rankingEntries = new RankingEntry[ 0 ];
		
		/***/
		private final byte type;
		
		/** Momento da última atualização do ranking. */
		private long lastUpdated;
		
		/***/
		private final byte category;

		
		private RankingLoader( int type, int category ) {
			this.category = ( byte ) category;
			this.type = ( byte ) type;
		}
		
		
		private RankingLoader( int type, int category, RankingEntry[] entries ) {
			this( type, category );
			this.rankingEntries = entries;
		}
		
		
		private final void save() throws Exception {
			AppMIDlet.saveData( RANKING_DATABASE, getSlotIndex(), this );

			//#if DEBUG == "true"
//# 				System.out.println( "\n--->RANKING GRAVADO - CATEGORIA " + category + ", TIPO " + type + ", ATUALIZADO EM " + lastUpdated );
//# 				for ( short i = 0; i < rankingEntries.length; ++i ) {
//# 					System.out.println( "ENTRADA " + i + "/" + rankingEntries.length + ":" );
//# 					System.out.println( rankingEntries[ i ] );
//# 				}
			//#endif
		}
		
		
		private final void load() {
			try {
				AppMIDlet.loadData( RANKING_DATABASE, getSlotIndex(), this );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
				
				erase( true );
			}

			//#if DEBUG == "true"
//# 				System.out.println( "--->\nRANKING LIDO - CATEGORIA " + category + ", TIPO " + type + ", ATUALIZADO EM " + lastUpdated );
//# 				for ( short i = 0; i < rankingEntries.length; ++i ) {
//# 					System.out.println( "ENTRADA " + i + "/" + rankingEntries.length + ":" );
//# 					System.out.println( rankingEntries[ i ] );
//# 				}
			//#endif
		}
		
		
		/**
		 * Apaga os dados do slot.
		 */
		private final void erase( boolean save ) {
			// inicializa o slot
			switch ( category ) {
				case SLOT_CATEGORY_LOCAL_RECORDS:
					rankingEntries = new RankingEntry[ LOCAL_RANKING_ENTRIES_TOTAL ];
					for ( byte i = 0; i < rankingEntries.length; ++i ) {
						rankingEntries[ i ] = new RankingEntry();
						if ( formatter != null )
							formatter.initLocalEntry( type, rankingEntries[ i ], i );
					}
				break;

				case SLOT_CATEGORY_GLOBAL_RECORDS:
				case SLOT_CATEGORY_PERSONAL_RECORDS:
				case SLOT_CATEGORY_UNSUBMITTED_ENTRIES:
				default:
					rankingEntries = new RankingEntry[ 0 ];
				break;
			}				

			if ( save ) {
				try {
					save();
				} catch ( Exception e2 ) {
					//#if DEBUG == "true"
		//# 						e2.printStackTrace();
					//#endif
				}
			}
		}


		public final void write( DataOutputStream output ) throws Exception {
			//#if DEBUG == "true"
//# 				System.out.println( "Gravando " + rankingEntries.length + " entradas de ranking" );
			//#endif
			
			switch ( category ) {
				case SLOT_CATEGORY_GLOBAL_RECORDS:
					output.writeLong( lastUpdated );
					// sem break
				case SLOT_CATEGORY_PERSONAL_RECORDS:
				case SLOT_CATEGORY_UNSUBMITTED_ENTRIES:
					output.writeShort( rankingEntries.length );
				break;
			}
			
			for ( byte i = 0; i < rankingEntries.length; ++i ) {
				rankingEntries[ i ].write( output );
			}
		}


		public final void read( DataInputStream input ) throws Exception {
			switch ( category ) {
				case SLOT_CATEGORY_GLOBAL_RECORDS:
					lastUpdated = input.readLong();
					// sem break
				case SLOT_CATEGORY_PERSONAL_RECORDS:
				case SLOT_CATEGORY_UNSUBMITTED_ENTRIES:
					rankingEntries = new RankingEntry[ input.readShort() ];
				break;
				
				case SLOT_CATEGORY_LOCAL_RECORDS:
					rankingEntries = new RankingEntry[ LOCAL_RANKING_ENTRIES_TOTAL ];
				break;
			}
			
			for ( byte i = 0; i < rankingEntries.length; ++i ) {
				rankingEntries[ i ] = new RankingEntry();
				rankingEntries[ i ].read( input );
			}
		}
		
		
		/**
		 * 
		 * @return
		 */
		private final int getSlotIndex() {
			// os índices do RMS começam em 1 em vez de zero...
			return 1 + ( type * SLOT_CATEGORIES_TOTAL ) + category;
		}
	} // fim da classe interna RankingLoader

}
