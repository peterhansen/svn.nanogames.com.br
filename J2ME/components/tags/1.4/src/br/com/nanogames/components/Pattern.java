/*
 * Pattern.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

/**
 *
 * @author peter
 */
public class Pattern extends Drawable implements Updatable {
	
	/** Cor de preenchimento do background. Pode-se utilizar valores negativos para indicar não-preenchimento com cor sólida do fundo. */
	protected int fillColor = -1;
	
	/** Drawable utilizado para preenchimento. */
	protected Drawable fill;
	
	/** Referência para o fill, caso ele seja atualizável (acelera o teste durante as chamadas de <code>update</code>. */
	protected Updatable updatableFill;
	
	/** Indica se o drawable de preenchimento utiliza transparência. Caso não utilize, é possível otimizar o desenho do pattern, copiando
	 * áreas pintadas em vez de chamar o método <code>draw(Graphics)</code> várias vezes.
	 */
//	protected boolean usesTransparency;
	
	/** Modo de desenho do pattern: não desenha nada (fillColor e fill não estão definidos). */
	protected static final byte DRAW_MODE_NOTHING				= 0;
	
	/** 
	 * Modo de desenho do pattern: preenche uma região top-left do tamanho do drawable com a cor sólida, desenha o drawable
	 * nessa região, e então a copia no restante da área (fillColor e fill definidos, com fill utilizando transparência). 
	 */
	protected static final byte DRAW_MODE_FILL_COPY_AREA		= 1;
	
	/** 
	 * Modo de desenho do pattern: preenche uma região top-left do tamanho do drawable com o drawable, depois a copia no 
	 * restante da área (fillColor e fill definidos, com fill não utilizando transparência, o que elimina a necessidade 
	 * de preencher a área com a cor sólida, uma vez que o drawable irá substituir toda a região). 
	 */
	protected static final byte DRAW_MODE_DONT_FILL_COPY_AREA	= 2;
	
	/** 
	 * Modo de desenho do pattern: desenha o drawable por toda a área (fillColor não definido, e fill definido utilizando
	 * transparência, o que impede de fazer cópias, uma vez que o fundo sob o drawable pode não ser uniforme). */
	protected static final byte DRAW_MODE_DONT_FILL_DRAW		= 3;
	
	/** 
	 * Modo de desenho do pattern: apenas preenche a área do pattern com a cor sólida definida (fillColor definido, fill
	 * não definido). 
	 */
	protected static final byte DRAW_MODE_FILL_ONLY				= 4;
	
//	/** Indica se o método <code>Graphics.copyArea()</code> é suportado pelo aparelho. */
//	public static final boolean COPY_AREA_SUPPORTED;
	
	/** Modo de desenho atual do pattern. */
	protected byte drawMode;
	
	
//	static {
//		boolean supported = false;
//		
//		// FIXME IllegalState Exception durante o desenho do pattern utilizando copyArea ao receber eventos de suspend
//		try {
//			ScreenManager.getScreenBuffer().copyArea( 0, 0, 2, 2, 5, 5, 0 );
//			supported = true;
//		} catch ( Throwable t ) {
//		}
//		
//		COPY_AREA_SUPPORTED = supported;
//	}
	
	
	/**
	 * Cria um novo pattern que utiliza um drawable como preenchimento. Por padrão, considera-se que este drawable não
	 * possui áreas com transparência, para que possam ser feitas otimizações de desenho. Caso ele utilize áreas 
	 * transparentes, deve-se utilizar o método <code>setUsesTransparency(boolean)</code>. Pode-se ainda utilizar um 
	 * drawable com transparência em conjunto com uma cor sólida de preenchimento, bastando definir uma cor no método
	 * <code>setFill(int)</code>.
	 * 
	 * @param fill drawable utilizado como preenchimento.
	 * @see #Pattern(int)
	 * @see #setUsesTransparency(boolean)
	 * @see #setFill(Drawable)
	 * @see #setFill(int)
	 */
	public Pattern( Drawable fill ) {
		setFill( fill );

		if ( fill != null )
			setSize( fill.getSize() );
	}
	
	
	/**
	 * Cria um novo pattern que utiliza uma cor sólida como preenchimento. 
	 * 
	 * @param color cor sólida utilizada como preenchimento. Caso seja um valor negativo, não há preenchimento de cor sólida.
	 * @see #Pattern(Drawable)
	 * @see #setFillColor(int)
	 * @see setFill(Drawable)
	 */
	public Pattern( int color ) {
		setFillColor( color );
	}

	
	protected void paint( Graphics g ) {
		//#if DEBUG == "true"
//# 			if ( fill != null && ( fill.getWidth() == 0 || fill.getHeight() == 0 ) )
//# 				throw new RuntimeException( "Fill size can't be zero." );
		//#endif

		switch ( drawMode ) {
			case DRAW_MODE_FILL_ONLY:
				// apenas preenche toda a área com a cor sólida definida
				//#if J2SE == "false"
					g.setColor( fillColor );
				//#else
//# 					g.setColor( new Color( fillColor ) );
				//#endif
				g.fillRect( translate.x, translate.y, size.x, size.y );
			break;
			
			case DRAW_MODE_DONT_FILL_DRAW:
				// não há cor de preenchimento definida e drawable usa transparência; logo, é necessário desenhar o drawable
				// por toda a área do pattern
				final Point fillSize = fill.getSize();
				final Rectangle clip = clipStack[ currentStackSize ];

				final int START_X = ( clip.x - translate.x ) / fillSize.x * fillSize.x;
				final int START_Y = ( clip.y - translate.y ) / fillSize.y * fillSize.y;
				final int END_X = START_X + clip.width + fillSize.x - 1;
				final int END_Y = START_Y + clip.height + fillSize.y - 1;

				for ( int y = START_Y; y < END_Y; y += fillSize.y ) {
					for ( int x = START_X; x < END_X; x += fillSize.x ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					}
				}

				// antigo modo de desenho
//				for ( int y = 0; y < size.y; y += fillSize.y ) {
//					for ( int x = 0; x < size.x; x += fillSize.x ) {
//						fill.setRefPixelPosition( x, y );
//						fill.draw( g );
//					} // fim for ( int x = 0; x < size.x; x += fillSize.x )
//				} // fim for ( int y = 0; y < size.y; y += fillSize.y )
			break;
			
//			case DRAW_MODE_FILL_COPY_AREA:
//			{
//				// há cor de preenchimento definida e drawable utiliza transparência; preenche cor a cor de preenchimento
//				// uma região top-left das dimensões do drawable, depois o desenha nessa região, e então copia este padrão
//				// por toda a região do pattern
//				
//				// preenche uma região top-left das dimensões do drawable com a cor de fundo
//				final Point fillSize = fill.getSize();
//				g.setColor( fillColor );
//				g.fillRect( translate.x, translate.y, fillSize.x, fillSize.y );			
//				
//				// não há break
//				
//			}
//			case DRAW_MODE_DONT_FILL_COPY_AREA:
//			{
//				// TODO verificar desempenho de copyArea
//				// FIXME algoritmo de copyArea não copia área completa no caso de viewports diferentes da tela
//				final Rectangle clip = clipStack[ currentStackSize ];
//				
//				final Point fillSize = fill.getSize();
//				// desenha o drawable pela primeira vez
//				fill.setRefPixelPosition( 0, 0 );
//				fill.draw( g );
//				
//				// se pelo menos uma das coordenadas de translação acumulada for negativa, significa que o drawable será
//				// desenhado parcialmente horizontalmente e/ou verticalmente. Nesse caso, ele é desenhado 1 ou 2 vezes a
//				// mais, de forma que a área copiada abranja toda a dimensão do drawable.
//				if ( translate.x < 0 || fill.referencePixel.x != 0 ) {
//					fill.setRefPixelPosition( fillSize.x, 0 );
//					fill.draw( g );
//				}
//				if ( translate.y < 0 || fill.referencePixel.y != 0 ) {
//					fill.setRefPixelPosition( 0, fillSize.y );
//					fill.draw( g );
//				}
//				
//				final int LIMIT_X = clip.x + clip.width;
//				final int LIMIT_Y = clip.y + clip.height;
//
//				final int MOD_X = clip.width % fillSize.x;
//				final int MOD_Y = clip.height % fillSize.y;
//
//				final int END_X = LIMIT_X - MOD_X;
//				final int END_Y = LIMIT_Y - MOD_Y;
//				// copia toda a primeira linha horizontal
//				final int FILL_HEIGHT = NanoMath.min( clip.height, fillSize.y );
//				for ( int x = clip.x + fillSize.x; x < END_X; x += fillSize.x ) {				
//					g.copyArea( clip.x, clip.y, fillSize.x, FILL_HEIGHT, x, clip.y, 0 );
//				}
//				if ( MOD_X > 0 )
//					g.copyArea( clip.x, clip.y, MOD_X, FILL_HEIGHT, END_X, clip.y, 0 );
//
//				if ( FILL_HEIGHT == fillSize.y ) {
//					// copia a linha horizontal até preencher toda a área do pattern
//					for ( int y = clip.y + fillSize.y; y < END_Y; y += fillSize.y ) {
//						g.copyArea( clip.x, clip.y, size.x, fillSize.y, clip.x, y, 0 );
//					}			
//					if ( MOD_Y > 0 )
//						g.copyArea( clip.x, clip.y, size.x, MOD_Y, clip.x, END_Y, 0 );
//				}
//			}
//			break;
		} // fim switch ( drawMode )
	} // fim do método paint( Graphics )
	
	
	public void setFill( Drawable fill ) {
		this.fill = fill;
		
		if ( fill instanceof Updatable )
			updatableFill = ( Updatable ) fill;
		else
			updatableFill = null;
		
		updateDrawMode();
	}
	
	
	public Drawable getFill() {
		return fill;
	}
	
	
	/**
	 * Define a cor de fundo pintada sob o drawable de preenchimento. No caso de não haver drawable de preenchimento,
	 * pinta toda a área do pattern com a cor definida.
	 * 
	 * @param fillColor cor de fundo. Valores negativos indicam ausência de cor de preenchimento.
	 * @see setFill(Drawable)
	 */
	public void setFillColor( int fillColor ) {
		this.fillColor = fillColor;
		
		updateDrawMode();
	}
	
	
	public int getFillColor() {
		return fillColor;
	}
	
	
//	public final void setUsesTransparency( boolean usesTransparency ) {
//		this.usesTransparency = usesTransparency;
//		
//		updateDrawMode();
//	}
	
	
	/**
	 * Indica se o drawable atual utiliza transparência.
	 * @return
	 */
//	public final boolean usesTransparency() {
//		return usesTransparency;
//	}
	 
	
	protected final void updateDrawMode() {
		if ( fillColor >= 0 ) {
			// há uma cor sólida de preenchimento definida
			if ( fill == null ) {
				drawMode = DRAW_MODE_FILL_ONLY;
			} else {
//				if ( usesTransparency ) {
//					if ( COPY_AREA_SUPPORTED )
//						drawMode = DRAW_MODE_FILL_COPY_AREA;
//					else
//						drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				} else {
//					if ( COPY_AREA_SUPPORTED )
//						drawMode = DRAW_MODE_DONT_FILL_COPY_AREA;
//					else
						drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				}
			}
		} else {
			// não há cor sólida de preenchimento definida
			if ( fill == null ) {
				drawMode = DRAW_MODE_NOTHING;
			} else {
//				if ( usesTransparency ) {
//					drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				} else {
//					if ( COPY_AREA_SUPPORTED )
//						drawMode = DRAW_MODE_DONT_FILL_COPY_AREA;
//					else
						drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				}
			}
		}
	}


	public void update( int delta ) {
		if ( updatableFill != null )
			updatableFill.update( delta );
	}
}
 
