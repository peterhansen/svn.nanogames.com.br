/*
 * Label.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;

//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif


/**
 *
 * @author peter
 */
public class Label extends Drawable {
	 
	/** fonte utilizada para desenhar o texto */
	protected ImageFont font;
	
	/** array de caracteres que representa o texto do label */
	protected char[] charBuffer;


	public Label( int fontIndex ) {
		this( fontIndex, null );
	}

	
	public Label( ImageFont font ) {
		this( font, null );
	}


	public Label( int fontIndex, int textIndex ) {
		this( fontIndex, AppMIDlet.getText( textIndex ) );
	}

	
	public Label( int fontIndex, String text ) {
		this( AppMIDlet.GetFont( fontIndex ), text );
	}


	public Label( ImageFont font, int textIndex ) {
		this( font, AppMIDlet.getText( textIndex ) );
	}


	/**
	 * Cria um novo label.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	public Label( ImageFont font, String text ) {
		this( font, text, true );
	}
	
	
	/**
	 * Cria um novo label.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
	 * @param setTextSize indica se a largura do label deve ser redefinida de acordo com o texto. <i>true</i> redimensiona
	 * o label de forma que tenha a mesma largura que o texto, e <i>false</i> mantém as dimensões atuais do label, 
	 * independentemente do texto definido.
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	protected Label( ImageFont font, String text, boolean setTextSize ) {
		setFont( font );
		setText( text, setTextSize );		
	}
	
	
	/**
	 * Define o texto do texto a partir do índice do texto nos recursos do aplicativo. Equivalente à chamada de 
	 * <code>setText( AppMIDlet.getText( textIndex )</code>.
	 * 
	 * @param textIndex índice do texto definido no AppMIDlet.
	 * @see #setText(String)
	 * @see #setText(String,boolean)
	 */
	public final void setText( int textIndex ) {
		setText( AppMIDlet.getText( textIndex ) );
	}
	
	
	/**
	 * Define o texto do texto a partir do índice do texto nos recursos do aplicativo. Equivalente à chamada de 
	 * <code>setText( AppMIDlet.getText( textIndex, setSize )</code>.
	 * 
	 * @param textIndex índice do texto definido no AppMIDlet.
	 * @param setSize indica se as dimensões do label devem ser atualizadas de forma a conter todo o texto.
	 * @see #setText(String)
	 * @see #setText(String,boolean)
	 */
	public final void setText( int textIndex, boolean setSize ) {
		setText( AppMIDlet.getText( textIndex ), setSize );
	}
	
	
	/**
	 * Define o texto do label. Equivalente à chamada de <code>setText( text, true )</code>.
	 * 
	 * @param text texto do label.
	 * @see #setText(String, boolean)
	 * @see #setText(int)
	 */
	public final void setText( String text ) {
		setText( text, true );
	}
	
	
	/**
	 * Define o texto do label.
	 * 
	 * @param text texto do label.
	 * @param setSize indica se as dimensões do label devem ser atualizadas de forma a conter todo o texto.
	 * @see #setText(String)
	 * @see #setText(int)
	 */
	public void setText( String text, boolean setSize ) {
		if ( text == null )
			text = "";

		charBuffer = text.toCharArray();
		
		if ( setSize )
			setSize( font.getTextWidth( text ), font.getHeight() );		
	}
	 
	
	/**
	 * 
	 * @return
	 */
	public final char[] getCharBuffer() {
		return charBuffer;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getText() {
		return new String( charBuffer );
	}
	 
	
	public ImageFont getFont() {
		return font;
	}


	public void setFont( ImageFont font ) throws NullPointerException {
		if ( font == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao definir fonte do label: fonte nula." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		this.font = font;
	}
	

	protected void paint( Graphics g ) {
		drawString( g, charBuffer, translate.x, translate.y );
	}
	
	
	/**
	 * Desenha um texto num Graphics. 
	 * 
	 * @param g Graphics onde será desenhado o texto.
	 * @param text texto a ser desenhado.
	 * @param x posição x (esquerda) onde será desenhado o texto.
	 * @param y posição y (topo) onde será desenhado o texto.
	 */
	public final void drawString( Graphics g, String text, int x, int y ) {
		drawString( g, text.toCharArray(), x, y );
	}
	
	
	/**
	 * Desenha um texto num Graphics. 
	 * 
	 * @param g Graphics onde será desenhado o texto.
	 * @param text texto a ser desenhado.
	 * @param x posição x (esquerda) onde será desenhado o texto.
	 * @param y posição y (topo) onde será desenhado o texto.
	 */
	public void drawString( Graphics g, char[] text, int x, int y ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif
		
		// não desenha caracteres que estejam fora da área de clip horizontal
		final int limitLeft = getClip().x;
		final int limitRight = limitLeft + getClip().width;

		final int textLength = text.length;

		int i = 0;

		// TODO : Desmembrar este for em 3:
		// 1) Desenhar o 1o caracter da linha com teste completo de clip
		// 2) Desenhar todos os caracteres internos sem testes
		// 3) Desenhar o último caracter da linha com teste completo de clip
		for ( ; i < textLength && x < limitRight; ++i ) {
			// o teste da largura do caracter é usado para evitar chamadas desnecessárias de funções no caso
			// de caracteres não presentes na fonte (são ignorados)
			final char c = text[ i ];
			// aqui a largura tem que ser a do caracter somente (sem o offset extra)
			final int charWidth = font.charsWidths[ c ];
			
			if ( charWidth > 0 && ( x + charWidth >= limitLeft ) ) {
				// faz a interseção da área de clip do caracter com a área de clip do texto todo
				pushClip( g );
				
				g.clipRect( x, y, charWidth, font.getHeight() );
				//#if J2SE == "false"
					g.drawImage( font.image, x - font.getCharOffset( c ), y, 0 );
				//#else
//# 					g.drawImage( font.image, x - font.getCharOffset( c ), y, null );
				//#endif
				
				// restaura a área de clip total
				popClip( g );
			} // fim if ( charWidth > 0 && ( x + charWidth >= limitLeft ) )
			
			x += font.getCharWidth( c );
		} // fim for ( int i = 0; i < textArray.length && x < limitRight; ++i )
		
		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
//# 				System.out.println( "Erro ao desenhar string: " + new String( text ) );
//# 			}
		//#endif
	} // fim do método drawString( Graphics, String, int, int )
	
	
	/**
	 * Desenha um caracter num Graphics. 
	 * 
	 * @param g Graphics onde será desenhado o texto.
	 * @param c caracter a ser desenhado.
	 * @param x posição x (esquerda) onde será desenhado o texto.
	 * @param y posição y (topo) onde será desenhado o texto.
	 */
	public void drawChar( Graphics g, char c, int x, int y ) {
		// não desenha se o caracter estiver fora da área de clip horizontal
		final int limitLeft = getClip().x;
		final int limitRight = limitLeft + getClip().width;
		
		final int charWidth = font.getCharWidth( c );
		if ( charWidth > 0 && x < limitRight && x + charWidth >= limitLeft ) {
			pushClip( g );

			// faz a interseção da área de clip do caracter com a área de clip do texto todo
			g.clipRect( x, y, font.charsWidths[ c ], font.getHeight() );
			//#if J2SE == "false"
				g.drawImage( font.image, x - font.getCharOffset( c ), y, 0 );
			//#else
//# 				g.drawImage( font.image, x - font.getCharOffset( c ), y, null );
			//#endif

			// restaura a área de clip total
			popClip( g );
		} // fim if ( charsWidths[ c ] > 0 )
	} // fim do método drawChar( Graphics, char, int, int )	

}
 
