/*
 * KeysConstants.java
 *
 * Created on March 26, 2007, 9:15 PM
 *
 */

package br.com.nanogames.components.userInterface;

import javax.microedition.lcdui.Canvas;

/**
 *
 * @author peter
 */
public interface KeysConstants {
	
	// TODO confirmar teclas dos aparelhos LG
	
	// softkey esquerdo (todos os aparelhos possuem)
	public static final int KEY_SOFT_LEFT_GENERIC		= -6;
	public static final int KEY_SOFT_LEFT_LG			= KEY_SOFT_LEFT_GENERIC;
	public static final int KEY_SOFT_LEFT_LG_2			= -202;
	public static final int KEY_SOFT_LEFT_NOKIA			= KEY_SOFT_LEFT_GENERIC;
	public static final int KEY_SOFT_LEFT_MOTOROLA		= -21;
	public static final int KEY_SOFT_LEFT_MOTOROLA_2	= 21;
	public static final int KEY_SOFT_LEFT_SAMSUNG		= KEY_SOFT_LEFT_GENERIC;
	public static final int KEY_SOFT_LEFT_SONYERICSSON	= KEY_SOFT_LEFT_GENERIC;
	public static final int KEY_SOFT_LEFT_SIEMENS		= -1;
	public static final int KEY_SOFT_LEFT_SIEMENS_2		= 105;
	public static final int KEY_SOFT_LEFT_SAGEM			= -7; // igual � soft key direita dos SonyEricsson

	// softkey direito (todos os aparelhos possuem)
	public static final int KEY_SOFT_RIGHT_GENERIC		= -7;
	public static final int KEY_SOFT_RIGHT_LG			= KEY_SOFT_RIGHT_GENERIC;
	public static final int KEY_SOFT_RIGHT_LG_2			= -203;
	public static final int KEY_SOFT_RIGHT_NOKIA		= KEY_SOFT_RIGHT_GENERIC;
	public static final int KEY_SOFT_RIGHT_MOTOROLA		= -22;
	public static final int KEY_SOFT_RIGHT_MOTOROLA_2	= 22;
	public static final int KEY_SOFT_RIGHT_SAMSUNG		= KEY_SOFT_RIGHT_GENERIC;
	public static final int KEY_SOFT_RIGHT_SONYERICSSON	= KEY_SOFT_RIGHT_GENERIC;
	public static final int KEY_SOFT_RIGHT_SIEMENS		= -4;
	public static final int KEY_SOFT_RIGHT_SIEMENS_2	= 106;
	public static final int KEY_SOFT_RIGHT_SAGEM		= KEY_SOFT_LEFT_SONYERICSSON; // igual � soft key esquerda dos SonyEricsson
	
	// softkey central (somente alguns aparelhos possuem)
	public static final int KEY_SOFT_MID_MOTOROLA		= -23;
	public static final int KEY_SOFT_MID_MOTOROLA_2		= 23;
	
	// tecla de clear (somente alguns aparelhos possuem)
	public static final int KEY_CLEAR_GENERIC			= -8;
	public static final int KEY_CLEAR_LG				= -16;
	public static final int KEY_CLEAR_LG_2				= -204;
	public static final int KEY_CLEAR_NOKIA				= KEY_CLEAR_GENERIC;
	public static final int KEY_CLEAR_MOTOROLA			= KEY_CLEAR_GENERIC;
	public static final int KEY_CLEAR_SAMSUNG			= KEY_CLEAR_GENERIC;
	public static final int KEY_CLEAR_SONYERICSSON		= KEY_CLEAR_GENERIC;
	public static final int KEY_CLEAR_SIEMENS			= -12;
	public static final int KEY_CLEAR_SIEMENS_2			= -12345;
	public static final int KEY_CLEAR_SAGEM				= KEY_CLEAR_SONYERICSSON;
	
	// tecla de troca de modo de entrada de texto (alguns n�o possuem tecla espec�fica para essa fun��o; a troca
	// do modo de entrada � feita atrav�s de um menu mostrado ao se pressionar uma softkey)
	public static final int KEY_CHANGE_INPUT_GENERIC		= Canvas.KEY_STAR;
	public static final int KEY_CHANGE_INPUT_LG				= KEY_CHANGE_INPUT_GENERIC; // TODO confirmar KEY_CHANGE_INPUT_LG
	public static final int KEY_CHANGE_INPUT_NOKIA			= KEY_CHANGE_INPUT_GENERIC; // utiliza Canvas.KEY_POUND, mas � considerado o padr�o para facilitar os testes de EditLabelCanvas.KEY_POUND;
	public static final int KEY_CHANGE_INPUT_MOTOROLA		= KEY_CHANGE_INPUT_GENERIC; // utiliza Canvas.KEY_NUM0, mas � considerado o padr�o para facilitar os testes de EditLabel
	public static final int KEY_CHANGE_INPUT_SAMSUNG		= KEY_CHANGE_INPUT_GENERIC; // TODO confirmar KEY_CHANGE_INPUT_SAMSUNG
	public static final int KEY_CHANGE_INPUT_SONYERICSSON	= KEY_CHANGE_INPUT_GENERIC;
	public static final int KEY_CHANGE_INPUT_SIEMENS		= KEY_CHANGE_INPUT_GENERIC; // TODO confirmar KEY_CHANGE_INPUT_SIEMENS
	public static final int KEY_CHANGE_INPUT_SAGEM			= KEY_CHANGE_INPUT_SONYERICSSON;
	
	
	// tecla espec�fica para voltar (somente alguns aparelhos possuem)
	public static final int KEY_BACK_SONYERICSSON		= -11;
	public static final int KEY_BACK_SAGEM				= -KEY_BACK_SONYERICSSON;

}
