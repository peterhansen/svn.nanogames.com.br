/*
 * Label.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.KeyListener;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public class Label extends Drawable implements KeyListener {
 
	// texto exibido no label
	protected String text;
	 
	// fonte utilizada para desenhar o texto
	protected ImageFont font;
	
	// buffer utilizado para acelerar o desenho de strings
	protected static final short CHAR_BUFFER_SIZE = 256;
	protected static final char[] charBuffer = new char[ CHAR_BUFFER_SIZE ];
	 

	public Label( ImageFont font, String text ) throws Exception {
		this( font, text, true );
	}
	
	
	protected Label( ImageFont font, String text, boolean setTextSize ) throws Exception {
		if ( font == null ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao criar Label: fonte nula." );
			//#else
				throw new Exception();
			//#endif
		}
		
		setFont( font );
		setText( text, setTextSize );		
	}
	
	
	/**
	 * Define o texto do label. Equivalente � chamada de <i>setText( text, true )</i>.
	 * @param text texto do label.
	 * @see #setText(String, boolean)
	 */
	public final void setText( String text ) {
		setText( text, true );
	}
	
	
	/**
	 * Define o texto do label.
	 * @param text texto do label.
	 * @param setSize indica se as dimens�es do label devem ser atualizadas de forma a conter todo o texto.
	 * @see #setText(String)
	 */
	public void setText( String text, boolean setSize ) {
		if ( text == null )
			text = "";

		this.text = text;
		
		if ( setSize )
			setSize( font.getTextWidth( text ), font.getImage().getHeight() );		
	}
	 
	
	public String getText() {
		return text;
	}
	 
	
	public ImageFont getFont() {
		return font;
	}


	public void setFont( ImageFont font ) throws NullPointerException {
		if ( font == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao definir fonte do label: fonte nula." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		this.font = font;
	}
	 
	
	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
	}
	 
	
	/**
	 *@see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
	}
	

	protected void paint( Graphics g ) {
		drawString( g, text, translate.x, translate.y );
	}
	
	
	/**
	 * Desenha um texto num Graphics. 
	 * @param g Graphics onde ser� desenhado o texto.
	 * @param text texto a ser desenhado.
	 * @param x posi��o x (esquerda) onde ser� desenhado o texto.
	 * @param y posi��o y (topo) onde ser� desenhado o texto.
	 */
	public void drawString( Graphics g, String text, int x, int y ) {
		// n�o desenha caracteres que estejam fora da �rea de clip horizontal
		final int limitLeft = getClip().position.x;
		final int limitRight = limitLeft + getClip().size.x;
		
		final int textLength = text.length();
		text.getChars( 0, textLength, charBuffer, 0 );
		
		for ( int i = 0; i < textLength && x < limitRight; ++i ) {
			// o teste da largura do caracter � usado para evitar chamadas desnecess�rias de fun��es no caso
			// de caracteres n�o presentes na fonte (s�o ignorados)
			final char c = charBuffer[ i ];
			final byte charWidth = font.charsWidths[ c ];
			
			if ( charWidth > 0 && ( x + charWidth >= limitLeft ) ) {
				// faz a interse��o da �rea de clip do caracter com a �rea de clip do texto todo
				pushClip( g );
				
				g.clipRect( x, y, charWidth, font.image.getHeight() );
				g.drawImage( font.image, x - font.charsOffsets[ c ], y, 0 );
				
				// restaura a �rea de clip total
				popClip( g );
			} // fim if ( charWidth > 0 && ( x + charWidth >= limitLeft ) )
			
			x += font.charsWidths[ c ] + font.getCharOffset();
		} // fim for ( int i = 0; i < textArray.length && x < limitRight; ++i )
	} // fim do m�todo drawString( Graphics, String, int, int )
	
	
	/**
	 * Desenha um caracter num Graphics. 
	 * @param g Graphics onde ser� desenhado o texto.
	 * @param text texto a ser desenhado.
	 * @param x posi��o x (esquerda) onde ser� desenhado o texto.
	 * @param y posi��o y (topo) onde ser� desenhado o texto.
	 */
	public void drawChar( Graphics g, char c, int x, int y ) {
		// n�o desenha se o caracter estiver fora da �rea de clip horizontal
		final int limitLeft = getClip().position.x;
		final int limitRight = limitLeft + getClip().size.x;
		
		final int charWidth = font.charsWidths[ c ];
		if ( charWidth > 0 && x < limitRight && x + charWidth >= limitLeft ) {
			pushClip( g );

			// faz a interse��o da �rea de clip do caracter com a �rea de clip do texto todo
			g.clipRect( x, y, font.charsWidths[ c ], font.image.getHeight() );
			g.drawImage( font.image, x - font.charsOffsets[ c ], y, 0 );

			// restaura a �rea de clip total
			popClip( g );
		} // fim if ( charsWidths[ c ] > 0 )
	} // fim do m�todo drawChar( Graphics, char, int, int )	

}
 
