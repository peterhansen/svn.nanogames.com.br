/*
 * NanoMath.java
 *
 * Created on October 26, 2007, 11:48 AM
 *
 */

package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
public final class NanoMath {

	/** o construtor � privado pois a classe n�o pode ser instanciada (todos os seus m�todos s�o est�ticos) */
	private NanoMath() {
	}
	
	// <editor-fold desc="tabela utilizada para c�lculos da raiz quadrada">
	/** tabela utilizada para c�lculos da raiz quadrada */
	private static final short[] squareRootTable = {
	      0,  16,  22,  27,  32,  35,  39,  42,  45,  48,  50,  53,  55,  57,
	     59,  61,  64,  65,  67,  69,  71,  73,  75,  76,  78,  80,  81,  83,
	     84,  86,  87,  89,  90,  91,  93,  94,  96,  97,  98,  99, 101, 102,
	    103, 104, 106, 107, 108, 109, 110, 112, 113, 114, 115, 116, 117, 118,
	    119, 120, 121, 122, 123, 124, 125, 126, 128, 128, 129, 130, 131, 132,
	    133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 144, 145,
	    146, 147, 148, 149, 150, 150, 151, 152, 153, 154, 155, 155, 156, 157,
	    158, 159, 160, 160, 161, 162, 163, 163, 164, 165, 166, 167, 167, 168,
	    169, 170, 170, 171, 172, 173, 173, 174, 175, 176, 176, 177, 178, 178,
	    179, 180, 181, 181, 182, 183, 183, 184, 185, 185, 186, 187, 187, 188,
	    189, 189, 190, 191, 192, 192, 193, 193, 194, 195, 195, 196, 197, 197,
	    198, 199, 199, 200, 201, 201, 202, 203, 203, 204, 204, 205, 206, 206,
	    207, 208, 208, 209, 209, 210, 211, 211, 212, 212, 213, 214, 214, 215,
	    215, 216, 217, 217, 218, 218, 219, 219, 220, 221, 221, 222, 222, 223,
	    224, 224, 225, 225, 226, 226, 227, 227, 228, 229, 229, 230, 230, 231,
	    231, 232, 232, 233, 234, 234, 235, 235, 236, 236, 237, 237, 238, 238,
	    239, 240, 240, 241, 241, 242, 242, 243, 243, 244, 244, 245, 245, 246,
	    246, 247, 247, 248, 248, 249, 249, 250, 250, 251, 251, 252, 252, 253,
	    253, 254, 254, 255
	};
	// </editor-fold>
	
	
	/**
	 * Calcula a raiz quadrada inteira de um valor.
	 * @param x valor cuja raiz quadrada ser� calculada.
	 * @return a raiz quadrada inteira de x (valor arredondado para mais ou para menos, o que for mais pr�ximo)
	 */
	public static final long sqrt( int x ) {
		long xn;

		if (x >= 0x10000)
			if (x >= 0x1000000)
				if (x >= 0x10000000)
					if (x >= 0x40000000) {
						if (x >= Integer.MAX_VALUE)
							return Integer.MAX_VALUE;
						xn = squareRootTable[x>>24] << 8;
					} else
						xn = squareRootTable[x>>22] << 7;
				else
					if (x >= 0x4000000)
						xn = squareRootTable[x>>20] << 6;
					else
						xn = squareRootTable[x>>18] << 5;
			else {
				if (x >= 0x100000)
					if (x >= 0x400000)
						xn = squareRootTable[x>>16] << 4;
					else
						xn = squareRootTable[x>>14] << 3;
				else
					if (x >= 0x40000)
						xn = squareRootTable[x>>12] << 2;
					else
						xn = squareRootTable[x>>10] << 1;

				xn = (xn + 1 + x / xn) >> 1;

				if (xn * xn > x) /* Correct rounding if necessary */
					--xn;

				return xn;
			}
		else
			if (x >= 0x100) {
				if (x >= 0x1000)
					if (x >= 0x4000)
						xn = (squareRootTable[x>>8] >> 0) + 1;
					else
						xn = (squareRootTable[x>>6] >> 1) + 1;
				else
					if (x >= 0x400)
						xn = (squareRootTable[x>>4] >> 2) + 1;
					else
						xn = (squareRootTable[x>>2] >> 3) + 1;

				if (xn * xn > x) /* Correct rounding if necessary */
					--xn;

				return xn;
			} else
				return squareRootTable[x] >> 4;

		if (xn * xn > x) /* Correct rounding if necessary */
		    --xn;
	
		return xn;
	} // fim do m�todo sqrt( int )	
	
	/** constante multiplicadora dos valores de seno e coseno para melhorar a precis�o */
	public static final short SIN_COS_MULTIPLIER = 10000;
	
	/** tabela de seno de 0 a 90 graus (valores multiplicados por 10000) */
	private static final short[] TABLE_SIN =  { 0, 175, 349, 523, 698, 872, 1045, 1219, 1392, 1564, 1736, 1908, 2079, 2250, 
												2419, 2588, 2756, 2924, 3090, 3256, 3420, 3584, 3746, 3907, 4067, 4226, 4384,
												4540, 4695, 4848, 5000, 5150, 5299, 5446, 5592, 5736, 5878, 6018, 6157, 6293, 
												6428, 6561, 6691, 6820, 6947, 7071, 7193, 7314, 7431, 7547, 7660, 7771, 7880, 
												7986, 8090, 8192, 8290, 8387, 8480, 8572, 8660, 8746, 8829, 8910, 8988, 9063, 
												9135, 9205, 9272, 9336, 9397, 9455, 9511, 9563, 9613, 9659, 9703, 9744, 9781, 
												9816, 9848, 9877, 9903, 9925, 9945, 9962, 9976, 9986, 9994, 9998, 10000 };
	
	/**
	 * Obt�m o valor do seno de um determinado �ngulo.
	 * @param angle �ngulo (em graus) cujo seno ser� obtido. Valores negativos ou maiores que 360 s�o v�lidos.
	 * @return o valor do seno de <i>angle</i> multiplicado por SIN_COS_MULTIPLIER.
	 */
	public static final short sin( int angle ) {
		angle %= 360;
		if ( angle < 0 )
			angle += 360;

		if ( angle <= 90 )
			return TABLE_SIN[ angle ];
		else if ( angle <= 180 )
			return TABLE_SIN[ 180 - angle ];
		else if ( angle <= 270 )
			return ( short ) -TABLE_SIN[ angle - 180 ];
		else
			return ( short ) -TABLE_SIN[ 360 - angle ];
			
	} // fim do m�todo sin( int )
	
	
	/**
	 * Obt�m o valor do cosseno de um determinado �ngulo.
	 * @param angle �ngulo (em graus) cujo cosseno ser� obtido. Valores negativos ou maiores que 360 s�o v�lidos.
	 * @return o valor do cosseno de <i>angle</i> multiplicado por SIN_COS_MULTIPLIER.
	 */	
	public static final short cos( int angle ) {
		return sin( 90 - angle );
	}
	
	
	/**
	 * Obt�m o valor da tangente de um determinado �ngulo.
	 * @param angle �ngulo (em graus) cuja tangente ser� obtido. Valores negativos ou maiores que 360 s�o v�lidos.
	 * @return o valor da tangente de <i>angle</i> multiplicada por SIN_COS_MULTIPLIER, Integer.MAX_VALUE caso o �ngulo
	 * seja de 90� (ou -270�), e -Integer.MAX_VALUE caso o �ngulo seja de 270� (ou -90�).
	 */	
	public static final int tan( int angle ) {
		switch ( angle % 360 ) {
			case 90:
			case -270:
				return Integer.MAX_VALUE;
			
			case 270:
			case -90:
				return -Integer.MAX_VALUE;
				
			default:
				return sin( angle ) / cos( angle );
		} // fim switch ( angle % 360 )
	} // fim do m�todo tan( int )
	
}
