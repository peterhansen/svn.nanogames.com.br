package br.com.nanogames.components;

/*
 * Updatable.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

/**
 *
 * @author peter
 */
public interface Updatable {
 
	/**
	 * Atualiza o estado de um objeto ap�s <i>delta</i> milisegundos.
	 * @param delta intervalo de tempo considerado para a atualiza��o.
	 */
	public abstract void update( int delta );
}
 
