/*
 * SpriteListener.java
 *
 * Created on October 5, 2007, 7:32 PM
 *
 */

package br.com.nanogames.components;

/**
 *
 * @author peter
 */
public interface SpriteListener {
	
	/**
	 * M�todo chamado por um sprite, indicando o fim de uma sequ�ncia de anima��o.
	 * @param id identifica��o do sprite.
	 * @param sequence �ndice da sequ�ncia encerrada.
	 */
	public void sequenceEnded( int id, int sequence );
	
}
