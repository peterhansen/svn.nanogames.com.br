/*
 * MediaPlayer.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

import br.com.nanogames.components.userInterface.AppMIDlet;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import javax.microedition.lcdui.Display;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;

/**
 *
 * @author peter
 */
public final class MediaPlayer implements Serializable {
	
	/** indica se o tocador de sons est� mudo. Definir o tocador como mudo n�o altera o volume. */
	private static boolean muted;
	 
	/** indica se a vibra��o est� ativada */
	private static boolean vibration = true;
	 
	/** valor a ser passado para o tocador de som no caso de se tocar um som infinitas vezes seguidas */
	public static final byte LOOP_INFINITE = -1;
	
	/** �ndice do �ltimo som tocado */
	private static byte lastPlayedIndex = -1;
	
	/** intervalo m�nimo em milisegundos entre 2 chamadas de play() - caso o intervalo seja menor que esse valor, a
	 * chamada do m�todo � ignorada. Esse intervalo � utilizado para evitar que v�rias chamadas seguidas do m�todo
	 * "quebrem" a aplica��o na maioria dos aparelhos, ou que haja perda de desempenho devido �s chamadas seguidas, j�
	 * que a grande maioria dos aparelhos n�o � capaz de tocar 2 sons num intervalo muito curto de tempo.
	 */
	private static final short MIN_INTERVAL_BETWEEN_PLAY = 256;
	private static long lastPlayTime;
	
	/** refer�ncia para o Display (utilizado para vibrar) */
	private static Display display;
	 
	/** tocador de som. S� h� um tocador ativo a cada momento, devido a problemas em v�rios aparelhos ao se pr�-alocar
	 * v�rios sons. */
	protected static Player player;
	
	/** endere�os dos sons a serem tocados e seus respectivos tipos de m�dia */
	protected static String[][] sounds;
	
	protected static String databaseName;
	protected static int databaseIndex;
	
	public static final String[] EXTENSION_WAV	= { "wav" };
	public static final String[] EXTENSION_AMR	= { "amr" };
	public static final String[] EXTENSION_MP3	= { "mp3", "mp4" };
	public static final String[] EXTENSION_MIDI	= { "mid", "midi" };
	
	public static final String MEDIA_TYPE_WAV			= "audio/x-wav";
	public static final String MEDIA_TYPE_AU			= "audio/basic";
	public static final String MEDIA_TYPE_MP3			= "audio/mpeg";
	public static final String MEDIA_TYPE_MID			= "audio/midi";
	public static final String MEDIA_TYPE_AMR			= "audio/amr";
	public static final String MEDIA_TYPE_TONE_SEQUENCE	= "audio/x-tone-seq";
	
	private static final String[] SUPPORTED_TYPES = Manager.getSupportedContentTypes( null );
	
	private static final Hashtable extensionTable = new Hashtable();
	
	private static MediaPlayer instance;
	
	private static boolean vibrationSupported = true;
	
	
	private MediaPlayer() {
		for ( int i = 0; i < EXTENSION_WAV.length; ++i )
			extensionTable.put( EXTENSION_WAV[ i ], MEDIA_TYPE_WAV );
		
		for ( int i = 0; i < EXTENSION_AMR.length; ++i )
			extensionTable.put( EXTENSION_AMR[ i ], MEDIA_TYPE_AMR );		
		
		for ( int i = 0; i < EXTENSION_MP3.length; ++i )
			extensionTable.put( EXTENSION_MP3[ i ], MEDIA_TYPE_MP3 );
		
		for ( int i = 0; i < EXTENSION_MIDI.length; ++i )
			extensionTable.put( EXTENSION_MIDI[ i ], MEDIA_TYPE_MID );
	}
	
	
	public static final boolean support( String mediaType ) {
		return extensionTable.get( mediaType ) != null;
	}
	
	
	/**
	 * Indica se o aparelho � capaz de vibrar. A chamada deste m�todo n�o interrompe uma vibra��o ativa do aparelho.
	 * @return <i>true</i>, caso o aparelho seja capaz de vibrar, e <i>false</i> caso contr�rio.
	 * @see MediaPlayer#checkVibrationSupport
	 */
	public static final boolean isVibrationSupported() {
		return vibrationSupported;
	}

	
	/**
	 * Verifica se o aparelho possui recurso de vibra��o. Normalmente, esse m�todo s� � chamado por ScreenManager quando 
	 * a tela fica ativa, pois em alguns aparelhos se esse teste for feito quando n�o h� tela ativa o aparelho indica 
	 * incorretamente que n�o possui o recurso. A chamada deste m�todo interrompe a vibra��o do aparelho, caso ela esteja 
	 * ocorrendo. Para evitar isso, deve-se utilizar o m�todo isVibrationSupported.
	 * @see MediaPlayer#isVibrationSupported
	 * @return <i>true</i>, caso o aparelho seja capaz de vibrar, e <i>false</i> caso contr�rio.
	 */
	public static final boolean checkVibrationSupport() {
		switch ( AppMIDlet.getVendor() ) {
			case AppMIDlet.VENDOR_MOTOROLA:
			case AppMIDlet.VENDOR_SAMSUNG:
			case AppMIDlet.VENDOR_SONYERICSSON:
			case AppMIDlet.VENDOR_SIEMENS:
				// considera que todos possuem vibra��o - o teste � diferenciado para alguns fabricantes pois nos casos 
				// da Nokia (e possivelmente outros fabricantes), h� aparelhos sem suporte a vibra��o (todos os S60 2nd 
				// Edition, al�m de alguns S40 2nd Edition), enquanto alguns aparelhos Samsung indicam que n�o suportam
				// vibra��o, quando na verdade possuem o recurso (D820, por exemplo)
			return true;
			
			default:
				final boolean previousVibration = vibration;
				vibration = true;
				vibrationSupported = vibrate( 0 );
				
				vibration = vibrationSupported && previousVibration;

				return vibrationSupported;
		}
	} // fim do m�todo checkVibrationSupport()

	
	/**
	 * Ativa a vibra��o.
	 * @param duration dura��o em milisegundos da vibra��o. O tempo exato e a forma como a vibra��o ocorre (cont�nua ou
	 * intermitente) s�o dependentes da implementa��o de cada aparelho. Para interromper a vibra��o, deve-se passar como
	 * argumento o valor 0 (zero).
	 * @return boolean indicando se o aparelho suporta vibra��o.
	 */
	public static final boolean vibrate( int duration ) {
		if ( vibration ) {
			// normalmente n�o � necess�rio que a vibra��o esteja dentro de um bloco try/catch, mas alguns aparelhos
			// lan�am exce��o ou travam quando a vibra��o � ativada enquanto est�o carregando, por exemplo.
			try {
				return display.vibrate( duration );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
				
				return false;
			}			
		} // fim if ( vibration )
		return false;
	} // fim do m�todo vibrate( int )
	 
	
	/**
	 * Define o estado da vibra��o.
	 * @param active estado da vibra��o: ligada (true) ou desligada (false).
	 */
	public static final void setVibration( boolean active ) {
		vibration = active;

		// p�ra alguma vibra��o que possa estar ativa no momento caso a vibra��o seja desativada
		if ( !active )
			vibrate( 0 );

		saveOptions();
	} // fim do m�todo setVibration( boolean )
	 
	
	/**
	 * Indica se a vibra��o est� ligada.
	 * @return <i>true</i>, caso esteja ligada, e <i>false<i> caso contr�rio.
	 */
	public static final boolean isVibration() {
		return vibration;
	} // fim do m�todo isVibration()
	 
	
	/**
	 * Inicializa o tocador de sons.
	 * @param soundsPaths endere�os dos arquivos de som. A ordem dos arquivos define o �ndice a ser passado posteriormente
	 * ao m�todo play. Por exemplo: para se tocar o arquivo "menu.mid" presente na lista de arquivos { "splash.mid", 
	 * "shot.wav", "menu.mid", "ending.mp3" }, deve-se passar o �ndice 2 (3� item da lista). A extens�o do arquivo define
	 * o tipo de som a ser tocado.
	 * @throws java.lang.Exception caso haja erro ao carregar os sons
	 */
	public static final void init( String databaseName, int databaseIndex, String[] soundsPaths ) throws Exception {
		// TODO carregar arquivos em byte[], mantendo os arquivos na mem�ria e depois carregando-os atrav�s de inputStream
		// dos arrays de bytes
		if ( instance == null )
			instance = new MediaPlayer();		
		
		display = Display.getDisplay( AppMIDlet.getInstance() );
		
		if ( soundsPaths != null && soundsPaths.length > 0 ) {
			sounds = new String[ soundsPaths.length ][ 2 ];
			for ( int i = 0; i < sounds.length; ++i ) {
				sounds[ i ][ 0 ] = soundsPaths[ i ];
				final char[] chars = soundsPaths[ i ].toCharArray();

				// obt�m a extens�o do som a ser tocado
				int begin = chars.length - 1;
				for ( ; begin >= 0; --begin ) {
				   if ( chars[ begin ] == '.' )
					   break;
				} 
				final String extension = soundsPaths[ i ].substring( begin + 1 ).toLowerCase();
				sounds[ i ][ 1 ] = ( String ) extensionTable.get( extension );
			}
		} // fim if ( soundsPaths != null && soundsPaths.length > 0 )
		
		MediaPlayer.databaseName = databaseName;
		MediaPlayer.databaseIndex = databaseIndex;
		
		// tenta carregar as op��es
		loadOptions();
	} // fim do m�todo init( String[] )
	 
	
	public static final void free() {
		if ( player != null ) {
			try {
				player.deallocate();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}			
			
			try {
				player.close();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}						
			
			player = null;
		}

		System.gc();
		lastPlayedIndex = -1;
	} // fim do m�todo free()
	 
	
	public static final void stop() {
		if ( player != null ) {
			try {
				player.stop();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
		} // fim if ( player != null )
	}
	 
	
	/**
	 * Toca um som.
	 * @param index �ndice do som a ser tocado. O �ndice � determinado pela ordem dos endere�os passados ao m�todo init.
	 * @param loopCount n�mero de repeti��es do som a ser tocado.
	 */
	public static final void play( int index, int loopCount ) {
		if ( !muted ) {
			// verifica se o intervalo entre a chamada atual de play e a anterior � maior ou igual ao intervalo m�nimo
			final long interval = System.currentTimeMillis() - lastPlayTime;
			
			if ( interval >= MIN_INTERVAL_BETWEEN_PLAY ) {
				try {
					stop();

					if ( index != lastPlayedIndex ) {
						// caso o �ndice seja diferente do �ltimo som tocado, o Player anterior � desalocado para melhorar
						// a portabilidade
						free();

						final InputStream input = display.getClass().getResourceAsStream( sounds[ index ][ 0 ] );
						player = Manager.createPlayer( input, sounds[ index ][ 1 ] );
						try { 
							player.realize(); 
						} catch ( Exception e ) {
							//#if DEBUG == "true"
	//# 						e.printStackTrace();
							//#endif
						}

						try { 
							player.prefetch(); 
						} catch ( Exception e ) {
							//#if DEBUG == "true"
	//# 						e.printStackTrace();
							//#endif
						}
					} // fim if ( index != lastPlayedIndex )

					player.setLoopCount( loopCount );
	//				switch ( loopCount ) { TODO testar
	//					case 0:
	//					case 1:
	//					break;
	//					
	//					default:
	//						player.setLoopCount( loopCount );
	//				}

					player.start();

					lastPlayedIndex = ( byte ) index;
					lastPlayTime = System.currentTimeMillis();
				} catch ( Exception e ) {
					//#if DEBUG == "true"
		//# 			e.printStackTrace();
					//#endif
				} // fim catch ( Exception e )				
			} // fim if ( interval >= MIN_INTERVAL_BETWEEN_PLAY )
		} // fim if ( !muted )
	} // fim do m�todo play( int, int )

	
	public static final void setMute( boolean mute ) {
		MediaPlayer.muted = mute;
		
		// p�ra de tocar sons caso o tocador seja definido como mudo
		if ( mute )
			stop();
		
		saveOptions();
	} // fim do m�todo setMute( boolean )

	
	public static final boolean isMuted() {
		return muted;
	}
	
	
	public static final boolean flashBacklights( int duration ) {
		// TODO testar flashBacklights
		try {
			return display.flashBacklight( duration );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif

			return false;
		}			
	}
	
	
	/**
	 * Play back a tone as specified by a note and its duration. A note is given in the range of 0 to 127 inclusive. The 
	 * frequency of the note can be calculated from the following formula:
	 * <p>SEMITONE_CONST = 17.31234049066755 = 1/(ln(2^(1/12)))
	 * <br></br>note = ln(freq/8.176)*SEMITONE_CONST
	 * <br></br>The musical note A = MIDI note 69 (0x45) = 440 Hz.</p>
	 * <p>This call is a non-blocking call. Notice that this method may utilize CPU resources significantly on devices that 
	 * don't have hardware support for tone generation.</p>
	 *
	 * @param note Defines the tone of the note as specified by the above formula.
	 * @param duration The duration of the tone in milli-seconds. Duration must be positive.
	 * @param volume Audio volume range from 0 to 100. 100 represents the maximum volume at the current hardware level. 
	 * Setting the volume to a value less than 0 will set the volume to 0. Setting the volume to greater than 100 will 
	 * set the volume to 100.
	 */
	public static final void playTone( int note, int duration, int volume ) {
		try {
			Manager.playTone( note, duration, volume );
		} catch ( Exception e ) {
			e.printStackTrace();
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	}


	private static final void saveOptions() {
		try {
			AppMIDlet.saveData( databaseName, databaseIndex, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo saveOptions()
	
	
	private static final void loadOptions() {
		try {
			AppMIDlet.loadData( databaseName, databaseIndex, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo loadOptions()

	
	public final void write( DataOutputStream output ) throws Exception {
		output.writeBoolean( muted );
		output.writeBoolean( vibration );
	}

	
	public final void read( DataInputStream input ) throws Exception {
		muted = input.readBoolean();
		vibration = input.readBoolean();		
	}
	
}
 
