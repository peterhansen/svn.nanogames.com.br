/*
 * Pattern.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class Pattern extends Drawable {
	
	// cor de preenchimento do background. Pode-se utilizar valores negativos para indicar n�o-preenchimento com cor
	// s�lida do fundo. 
	protected int fillColor = -1;
	
	// drawable utilizado para preenchimento
	protected Drawable fill;
	
	public Pattern( Drawable fill ) {
		setFill( fill );
	}

	
	protected void paint( Graphics g ) {
		if ( fillColor >= 0 ) {
			g.setColor( fillColor );
			g.fillRect( translate.x, translate.y, size.x, size.y );
		}
		
		if ( fill != null ) {
			final Point fillSize = fill.getSize();
			
			// TODO otimizar desenho do pattern
			for ( int y = 0; y < size.y; y += fillSize.y ) {
				for ( int x = 0; x < size.x; x += fillSize.x ) {
					fill.setRefPixelPosition( x, y );
					fill.draw( g );
				} // fim for ( int x = 0; x < size.x; x += fillSize.x )
			} // fim for ( int y = 0; y < size.y; y += fillSize.y )
		} // fim if ( fill != null )
	} // fim do m�todo paint( Graphics )
	
	
	public void setFill( Drawable fill ) {
		this.fill = fill;
	}
	
	
	public Drawable getFill() {
		return fill;
	}
	
	
	/**
	 * Define a cor de fundo pintada sob o drawable de preenchimento. No caso de n�o haver drawable de preenchimento,
	 * pinta toda a �rea do pattern com a cor definida.
	 * @param fillColor cor de fundo. Valores negativos indicam aus�ncia de cor de preenchimento.
	 */
	public void setFillColor( int fillColor ) {
		this.fillColor = fillColor;
	}
	
	
	public int getFillColor() {
		return fillColor;
	}
	 
}
 
