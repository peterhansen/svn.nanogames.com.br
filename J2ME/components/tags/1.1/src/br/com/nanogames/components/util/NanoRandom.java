/*
 * NanoRandom.java
 *
 * Created on 3 de Julho de 2007, 11:58
 *
 */

package br.com.nanogames.components.util;

import java.util.Random;


/**
 *
 * @author peter
 */
public final class NanoRandom extends Random {
    
    /** n�mero primo utilizado para incrementar o seed a cada nova inst�ncia */
    private static final int SEED_CHANGE = 999983;
    
    /** valor utilizado para reduzir a probabilidade de objetos Random alocados consecutivamente utilizarem o mesmo seed 
	 * e assim gerarem muitos padr�es de repeti��es */
    private static long SEED = System.currentTimeMillis();
    
	
    /**
	 * Creates a new instance of NanoRandom
	 */
    public NanoRandom() {
        super( SEED += SEED_CHANGE );
    }
    
    
	/**
	 * Retorna um n�mero aleat�rio positivo, entre 0 (inclusive) e n (exclusivo).
	 */
    public final int nextInt( int n ) {
        return Math.abs( nextInt() % n );
    }
}
