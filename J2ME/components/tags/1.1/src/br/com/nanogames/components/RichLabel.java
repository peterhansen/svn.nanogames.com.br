/*
 * RichLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 * Implementa um label com formata��o especial, possibilitando quebras de linha, v�rias formas de alinhamento 
 * de texto e inser��o de Drawables no texto (animados ou n�o). Para ter acesso a esses recursos, utiliza-se uma 
 * formata��o similar ao padr�o HTML.
 *
 * <h4>Alinhamento</h4>
 * Por padr�o, o alinhamento � top-left. O alinhamento considerado ao escrever uma linha � a combina��o do �ltimo alinhamento
 * horizontal com o �ltimo alinhamento vertical definidos. Por exemplo: ao encontrar uma tag &lt;ALN_R&gt;, todo o texto
 * subsequente � alinhado � direita, at� se chegar ao fim do texto ou ser lida uma tag indicando outro tipo de alinhamento
 * horizontal.
 * <p>Valores v�lidos:</p>
 * <ul>
 * <li>&lt;ALN_L&gt; alinhado � esquerda.</li>
 * <li>&lt;ALN_H&gt; centralizado horizontalmente.</li>
 * <li>&lt;ALN_R&gt; horizontal � direita.</li>
 * <li>&lt;ALN_T&gt; alinhamento vertical no topo da linha.</li>
 * <li>&lt;ALN_V&gt; centralizado verticalmente.</li>
 * <li>&lt;ALN_B&gt; alinhamento vertical na parte inferior da linha.</li>
 * </ul>
 *
 * <h4>Inser��o de Drawables</h4>
 * Drawables podem ser inseridos no texto, atrav�s do uso da tag <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" � o �ndice do
 * Drawable no array passado ao contrutor do label. Os Drawables seguem tamb�m as regras de alinhamento v�lidas para o 
 * texto. � importante observar que o alinhamento vertical define a forma como o texto e os Drawables de uma linha ir�o
 * se posicionar, no caso de a altura do Drawable n�o ser igual � altura da fonte utilizada no texto. Esse alinhamento 
 * toma como base o elemento de maior altura encontrado numa linha.
 * <p>� poss�vel tamb�m inserir Drawables animados no texto. Para isso, basta que o Drawable implemente a interface
 * <i>Updatable</i>, e fa�a o gerenciamento da anima��o atrav�s do m�todo <i>update</i> implementado.</p>
 *
 * <h4>Exemplo</h4>
 * A seguinte String:<br></br> <b>&lt;ALN_H&gt;T�tulo\n&lt;ALN_R&gt;Texto � direita\n&lt;ALN_V&gt;Texto � direita e centralizado verticalmente&lt;IMG_0&gt;</b>
 *
 * <p>resulta num texto com a primeira linha centralizada horizontalmente, a segunda alinhada horizontalmente � direita,
 * e a terceira alinhada � direita, por�m com alinhamento vertical do texto no centro do Drawable colocado no fim do texto.
 * Para se utilizar Drawables no texto, deve-se passar um array contendo refer�ncias a eles, e depois utilizar a tag no padr�o
 * <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" � o �ndice do Drawable no array.</p>
 *
 *
 * @author peter
 */
public class RichLabel extends Label implements Updatable {
 
	// tags de alinhamento do texto
	public static final char ALIGN_LEFT		= 'L';
	public static final char ALIGN_RIGHT	= 'R';
	public static final char ALIGN_HCENTER	= 'H';
	public static final char ALIGN_TOP		= 'T';
	public static final char ALIGN_VCENTER	= 'V';
	public static final char ALIGN_BOTTOM	= 'B';
	public static final char TAG_START		= '<';
	public static final char TAG_SEPARATOR	= '_';
	public static final char TAG_END		= '>';
	public static final String TAG_IMAGE	= "IMG_";
	public static final String TAG_ALIGN	= "ALN_";
	 
	// array de Drawables que podem ser inseridos no texto
	protected final Drawable[] drawables;
	
	// array dos drawables atualiz�veis. Esse � um subconjunto gerado automaticamente a partir do array de drawables, e
	// tem como fun��o acelerar o m�todo update, varrendo somente os elementos que podem ser atualizados.
	protected final Updatable[] updatables;
	
	// �ndices de cada informa��o na matriz de informa��es das linhas
	protected static final byte INFO_LINE_START			= 0;
	protected static final byte INFO_LINE_END			= INFO_LINE_START	+ 1;
	protected static final byte INFO_LINE_WIDTH			= INFO_LINE_END		+ 1;
	protected static final byte INFO_LINE_HEIGHT		= INFO_LINE_WIDTH	+ 1;
	protected static final byte INFO_LINE_ALIGN			= INFO_LINE_HEIGHT	+ 1;
	
	protected static final byte LINE_INFO_TOTAL_INDEXES	= INFO_LINE_ALIGN	+ 1;	
	
	protected static final byte TAG_TYPE_NONE	= 0;
	protected static final byte TAG_TYPE_ALIGN	= 1;
	protected static final byte TAG_TYPE_IMAGE	= 2;
	
	protected static final int DEFAULT_TEXT_ALIGNMENT = Graphics.TOP | Graphics.LEFT;
	
	// n�mero de espa�os de cada '\t' 
	protected static final byte TAB_SPACES = 3;
	
	// matriz de informa��es das linhas. O primeiro �ndice refere-se � linha, e o segundo a cada tipo de informa��o
	// conforme descrito acima.
	protected short[][] linesInfo;
	
	// offset na posi��o de desenho do texto (utilizado para fazer scroll do texto)
	protected int textOffset;
	
	// linha inicial a partir da qual � feita a varredura para desenho
	protected int initialLine;
	
	// texto em formato de char[], para acelerar futuras varreduras
	protected char[] textArray;
	
	// StringBuffers usados no m�todo paint( Graphics )
	protected final StringBuffer bufferType = new StringBuffer( 3 );
	protected final StringBuffer bufferNumber = new StringBuffer();
	
	// evita que o texto seja desenhado e/ou alterado enquanto o texto est� sendo reformatado, evitando-se assim problemas 
	// de inconsist�ncia que podem gerar travamentos
	protected boolean textInUse;
	 
	
	/**
	 * 
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @param maxLineWidth largura m�xima em pixels de cada linha, para fins de realizar a quebra do texto automaticamente
	 * de acordo com essa largura. Caso seja menor ou igual a zero, cada linha s� ser� quebrada explicitamente, ou seja,
	 * caso encontre caracteres '\n'.
	 * @param specialChars array de drawables que podem ser utilizados no texto. Para utiliz�-los, deve-se usar a tag 
	 * <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" � o �ndice do Drawable no array passado ao contrutor do label. Os Drawables 
	 * seguem tamb�m as regras de alinhamento v�lidas para o texto. � importante observar que o alinhamento vertical 
	 * define a forma como o texto e os Drawables de uma linha ir�o se posicionar, no caso de a altura do Drawable n�o 
	 * ser igual � altura da fonte utilizada no texto. Esse alinhamento toma como base o elemento de maior altura encontrado 
	 * numa linha.
	 * <p>� poss�vel tamb�m inserir Drawables animados no texto. Para isso, basta que o Drawable implemente a interface
	 * <i>Updatable</i>, e fa�a o gerenciamento da anima��o atrav�s do m�todo <i>update</i> implementado.</p>
	 * @throws java.lang.Exception 
	 */
	public RichLabel( ImageFont font, String text, int maxLineWidth, Drawable[] specialChars ) throws Exception {
		// formata o texto apenas no final do construtor, para evitar que, no caso de haver caracteres especiais,
		// sejam necess�rias 2 chamadas a formatText (pois na 1� chamada drawables n�o estaria inicializado)
		super( font, text, false );
		
		if ( specialChars != null && specialChars.length > 0 ) {
			this.drawables = specialChars;
			
			Vector updatableVector = new Vector();
			
			// varre o array de caracteres especiais, procurando por caracteres animados; essa varredura tem por
			// objetivo otimizar a varredura do m�todo update(), varrendo somente os objetos que podem ser atualizados
			for ( int i = 0; i < specialChars.length; ++i ) {
				if ( specialChars[ i ] instanceof Updatable )
					updatableVector.addElement( specialChars[ i ] );
			}
			
			// cria o array de Updatables somente com a quantidade necess�ria de objetos
			if ( updatableVector.size() > 0 ) {
				updatables = new Updatable[ updatableVector.size() ];
				
				for ( int i = 0; i < updatables.length; ++i ) {
					updatables[ i ] = ( Updatable ) updatableVector.elementAt( i );
				}
			} else {
				// n�o foram encontrados elementos atualiz�veis no array de caracteres especiais; n�o � necess�rio
				// criar o array de atualiz�veis
				updatables = null;
			}
			
		} else {
			this.drawables = null;
			updatables = null;
		}
		
		size.x = maxLineWidth;
		formatText();
	}
	
	
	protected void formatText() {
        Vector lines = new Vector( 1 );
        final short[] lineTemp = new short[ LINE_INFO_TOTAL_INDEXES ];
        
		int lineStart = 0;
		// �ndices na string que delimitam uma linha a ser desenhada
		int lineEnd = 0;
		// largura da linha de texto atual
		int lineWidth = 0;
		// altura da linha de texto atual
		int lineHeight = 0;
		int verticalAlignment = Graphics.TOP;
		int horizontalAlignment = Graphics.LEFT;
		
		// largura em pixels do espa�o e da tabula��o s�o pr�-calculados para acelerar o loop
		final int spaceWidth = font.getCharWidth( ' ' );
		final int tabWidth = spaceWidth * TAB_SPACES;
        
		// linha atual
        int currentLine = 0;
		// posi��o x atual
		int x = 0;
		// caracteres lidos
        int read = 0;
        
        boolean lineIncomplete = true; // indica se a linha ainda n�o foi totalmente preenchida
        
        // constantes calculadas previamente de forma a acelerar o loop
        final int toRead = textArray.length;
		
		if ( size.x <= 0 )
			size.x = font.getTextWidth( new String ( textArray ) );
		
        while ( read < toRead ) {
            int i = read;
            lineIncomplete = true;
			
			// posi��o no textArray de char onde a �ltima palavra iniciou
            int lastWordStart = 0;
            
            lineStart = read;
            lineWidth = x = 0;

			// indica se uma palavra j� foi iniciada, ou se o caracter lido � o primeiro de uma palavra (no caso de caracteres imprim�veis)
            boolean word = false; 
			
            lineHeight = font.getImage().getHeight();
            
        	// enquanto n�o completa uma linha                
            while ( lineIncomplete && ( i < toRead ) ) {
                final char c = textArray[ i ];
				// indica o tipo do caracter lido 
                boolean printableRead = false;
                switch ( c ) {
                	case 0: // n�o desenha caso o caracter n�o esteja definido.
					break;
						
                	case ' ':
                	    if ( ( x += spaceWidth ) > size.x ) {
                	        lineIncomplete = false;
                	        i++;
                	    }
               	        lineWidth = x;
					break; // fim case ' '
						
                	case '\n':
            	        lineWidth = x;
                	    lineIncomplete = false;
                	    read = i++;
					break; // fim case '\n'
						
                	case '\t':
                	    if ( ( x += tabWidth ) > size.x ) {
                	        lineIncomplete = false;
                	        ++i;
                	    }
                	    lineWidth = x;
					break; // fim case '\t'
						
                	case TAG_START:
                	    StringBuffer bufferType = new StringBuffer( 3 );
                	    StringBuffer bufferNumber = new StringBuffer();
                	    char alignType = 0;

                	    byte type = TAG_TYPE_NONE; 
                	    int j = i + 1;
                	    while ( textArray[ j ] != TAG_END ) {
                	        switch ( type ) {
                	        	case TAG_TYPE_NONE:
                       	            bufferType.append( textArray[ j ] );
                        	        if ( textArray[ j ] == TAG_SEPARATOR ) {
                        	            final String tagType = bufferType.toString();
                        	            if (tagType.equals( TAG_ALIGN ) )
                        	                type = TAG_TYPE_ALIGN;
                        	            else if ( tagType.equals( TAG_IMAGE ) )
                        	                type = TAG_TYPE_IMAGE;
                        	        }
                	        	    break;
                	        	case TAG_TYPE_ALIGN:
                	        	    alignType = textArray[ j ];
                	        	    break;
                	        	case TAG_TYPE_IMAGE:
                	        	    bufferNumber.append( textArray[ j ] );
                	        	    break;
                	        } // fim switch ( type )
                	        ++j;
                	    } // fim while ( textArray[ j ] != TAG_END )
						
						switch ( type ) {
							case TAG_TYPE_ALIGN:
								switch ( alignType ) {
									case ALIGN_HCENTER:
										horizontalAlignment = Graphics.HCENTER;
									break;
									case ALIGN_LEFT:
										horizontalAlignment = Graphics.LEFT;
									break;
									case ALIGN_RIGHT:
										horizontalAlignment = Graphics.RIGHT;
									break;
									case ALIGN_BOTTOM:
										verticalAlignment = Graphics.BOTTOM;
									break;
									case ALIGN_VCENTER:
										verticalAlignment = Graphics.VCENTER;
									break;
									case ALIGN_TOP:
										verticalAlignment = Graphics.TOP;
									break;
								} // fim switch ( alignType )
							break;
							case TAG_TYPE_IMAGE:
								if ( drawables != null ) {
									final int index = Integer.parseInt( bufferNumber.toString() );
									printableRead = true;
									if (!word) {
										lineEnd = lastWordStart = i;
										lineWidth = x;
									} // testa se ultrapassa limite da margem direita. caso ultrapasse, a palavra ser� impressa na linha seguinte. caso a palavra seja grande demais, � quebrada por caracter.
									word = true;
									final int h = drawables[ index ].getSize().y;
									if ( lastWordStart == lineStart ) {
										// palavra � maior que a largura da linha (palavra � quebrada por caracter)
										read = lineEnd = i;
										lineWidth = x;
									}
									else // palavra � menor que a largura da linha - � impressa por completo na linha seguinte
										read = lineEnd = lastWordStart;

									if ( ( x += drawables[ index ].getSize().x ) > size.x )
										lineIncomplete = false;
									if ( lineIncomplete && ( h > font.getImage().getHeight() ) && ( h > lineHeight ) )
										lineHeight = h;
								} // fim if ( drawables != null )
							break;
						} // fim switch ( type )

            	        if ( lineIncomplete )
            	            i = j;
					break; // fim case TAG_START
						
            		default:
                		printableRead = true;            			
						if ( !word ) {
							lineEnd = lastWordStart = i;
							lineWidth = x;
						} 
						
						// testa se ultrapassa limite da margem direita. caso ultrapasse, a palavra ser� impressa na linha seguinte. caso a palavra seja grande demais, � quebrada por caracter.
						if ( lastWordStart == lineStart ) {
							// palavra � maior que a largura da linha (palavra � quebrada por caracter).
							read = lineEnd = i;
							lineWidth = x;
						} else {
							// palavra � menor que a largura da linha - � impressa por completo na linha seguinte
							read = lineEnd = lastWordStart;
						}
						
						if ( ( x += font.getCharWidth( c ) ) > size.x )
							lineIncomplete = false;
						word = true;
    	                break;
                } // fim switch
				if ( !lineIncomplete ) {
					if ( !printableRead ) {
						//i++; // com essa linha n�o comentada, os espa�os no in�cio de cada linha n�o s�o mostrados
						read = i;
						lineEnd = i;
					}
				} else { // if (lineIncomplete)
					++i;
					if ( !printableRead ) {
						lineEnd = i;
						word = false;
					}
					read = i;
				}
            } // fim while (lineIncomplete)
            if ( i >= toRead ) {
    	        read = lineEnd = i;
    	        lineWidth = x;
            } else
                read = lineEnd;
            
            lineTemp[ INFO_LINE_START ] = ( short ) lineStart;
            lineTemp[ INFO_LINE_END ] = ( short ) lineEnd;
			// espa�os e tabula��es podem ultrapassar a margem da imagem, por�m a largura da linha n�o pode ser maior 
			// que a largura do label, sen�o ocorrem problemas com alinhamentos horizontais.
			lineTemp[ INFO_LINE_WIDTH ] = ( short ) Math.min( lineWidth, size.x );
            lineTemp[ INFO_LINE_HEIGHT ] = ( short ) lineHeight;
            lineTemp[ INFO_LINE_ALIGN ] = ( short ) ( horizontalAlignment | verticalAlignment );
            final short[] t = new short[ lineTemp.length ];
            System.arraycopy( lineTemp, 0, t, 0, lineTemp.length );
            lines.addElement( t );
            
            ++currentLine;
        } // fim while ( read < toRead )
        
        linesInfo = new short[ lines.size() ][ LINE_INFO_TOTAL_INDEXES ];
        for ( int index = 0; index < linesInfo.length; ++index ) {
            final short[] t = ( short[] ) lines.elementAt( index );
            for ( int i = 0; i < LINE_INFO_TOTAL_INDEXES; ++i )
                linesInfo[ index ][ i ] = t[ i ];
        }
		
		// volta a exibir o texto a partir do in�cio
		setTextOffset( 0 );
	} // fim do m�todo formatText()	
	
	
	/**
	 * 
	 * @param g 
	 */
    public void paint( Graphics g ) {
		lockText();
		
		final int totalLines = linesInfo.length;
		int x;
		int	y = translate.y + textOffset;
		int	verticalAlignment;
		int	currentLine;
		boolean imageEnd = false;

		// altura da fonte
		final int fontHeight = font.getImage().getHeight();

		pushClip( g );
		final Rectangle clipArea = getClip();
		final int yLimit = translate.y + clipArea.size.y;

		for ( currentLine = initialLine; currentLine < totalLines && !imageEnd; ++currentLine ) {
			// se o topo da linha estiver al�m da �rea de clip, p�ra a varredura, pois garantidamente todos os caracteres
			// e/ou drawables das linhas seguintes estar�o fora da �rea de clip.
			if ( y >= yLimit )
				break;

			// ajusta a posi��o do texto de acordo com o alinhamento horizontal
			if ( ( linesInfo[ currentLine ][ INFO_LINE_ALIGN ] & Graphics.RIGHT ) != 0 )
				x = translate.x + size.x - linesInfo[ currentLine ][ INFO_LINE_WIDTH ];        
			else if ( ( linesInfo[ currentLine ][ INFO_LINE_ALIGN ] & Graphics.HCENTER ) != 0 )
				x = translate.x + ( ( size.x - linesInfo[ currentLine ][ INFO_LINE_WIDTH ] ) >> 1 );
			else 
				x = translate.x;

			// ajusta a posi��o do texto de acordo com o alinhamento vertical
			if ( ( linesInfo[ currentLine ][ INFO_LINE_ALIGN ] & Graphics.BOTTOM ) != 0 ) {
				verticalAlignment = Graphics.BOTTOM;

				y += linesInfo[ currentLine ][ INFO_LINE_HEIGHT ] - fontHeight;
			} else if ( ( linesInfo[ currentLine ][ INFO_LINE_ALIGN ] & Graphics.VCENTER ) != 0 ) {
				verticalAlignment = Graphics.VCENTER;

				y += ( linesInfo[ currentLine ][ INFO_LINE_HEIGHT ] - fontHeight ) >> 1;
			} else {
				verticalAlignment = Graphics.TOP;
			}

			// o seguinte teste � utilizado para que s� as linhas a partir da �rea vis�vel sejam desenhadas
			if ( y + linesInfo[ currentLine ][ INFO_LINE_HEIGHT ] >= translate.y ) {
				for ( int charIndex = linesInfo[ currentLine ][ INFO_LINE_START ]; ( charIndex < linesInfo[ currentLine ][ INFO_LINE_END ] ); ++charIndex ) {
					final char c = textArray[ charIndex ];
					switch ( c ) {
						case 0: // n�o desenha caso o caracter n�o esteja definido.
						break;

						case TAG_START:
							byte type = TAG_TYPE_NONE;
							int tagEndIndex = charIndex + 1;
							while ( textArray[ tagEndIndex ] != TAG_END ) {
								switch ( type ) {
								case TAG_TYPE_NONE:
									bufferType.append( textArray[ tagEndIndex ] );
									if ( textArray[ tagEndIndex ] == TAG_SEPARATOR ) {
										final String tagType = bufferType.toString();
										if ( tagType.equals( TAG_IMAGE ) )
											type = TAG_TYPE_IMAGE;
									}
									break;
								case TAG_TYPE_IMAGE:
									bufferNumber.append( textArray[ tagEndIndex ] );
									break;
								}
								++tagEndIndex;
							} // fim while ( textArray[ tagEndIndex ] != TAG_END )
							charIndex = tagEndIndex;		          	    

							if ( type == TAG_TYPE_IMAGE ) {
								final int index = Integer.parseInt( bufferNumber.toString() );
								
								// verifica se o �ndice � v�lido
								try {
									final int drawableWidth = drawables[ index ].getSize().x;
									int drawableHeight = drawables[ index ].getSize().y;
									if ( y + drawableHeight > yLimit ) {
										drawableHeight = yLimit - y;
										imageEnd = true;
									}
									else
										drawableHeight += y - position.y;

									if ( verticalAlignment == Graphics.TOP ) {
										drawables[ index ].setPosition( x - translate.x, y - translate.y );
									} else if ( verticalAlignment == Graphics.VCENTER ) {
										drawables[ index ].setPosition( x - translate.x, y - translate.y + ( ( fontHeight - drawables[ index ].getSize().y ) >> 1 ) );
									} else {
										drawables[ index ].setPosition( x - translate.x, y - translate.y + fontHeight - drawables[ index ].getSize().y );
									}
									drawables[ index ].draw( g );
									x += drawableWidth;
								} catch ( Exception e ) {
									//#if DEBUG == "true"
//# 									// desenha uma �rea vermelha e escreve o �ndice do drawable n�o encontrado
//# 									final String text = "<IMG_" + index + ">";
//# 									final int textWidth = font.getTextWidth( text );
//# 									g.setColor( 0xff0000 );
//# 									g.fillRect( x, y, textWidth, fontHeight );
//# 
//# 									drawString( g, text, x, y );
//# 									x += textWidth;
									//#endif									
								}
							} // fim if ( type == TAG_TYPE_IMAGE )
							bufferNumber.delete( 0, bufferNumber.length() );
							bufferType.delete( 0, bufferType.length() );
						break; // fim case TAG_START

						default:
							drawChar( g, c, x, y );
							x += font.getCharWidth( c );
						break;

					} // fim switch ( c ) 
				} // fim for ( int charIndex = linesInfo[ currentLine ][ INFO_LINE_START ]; ( charIndex < linesInfo[ currentLine ][ INFO_LINE_END ] ); ++charIndex )
			} // fim if ( y + linesInfo[ currentLine ][ INFO_LINE_HEIGHT ] >= translate.y )

			// posiciona a vari�vel y no topo da pr�xima linha (o c�lculo para cada tipo de alinhamento vertical varia)
			switch ( verticalAlignment ) {
				case Graphics.TOP:
					y += linesInfo[ currentLine ][ INFO_LINE_HEIGHT ];
				break;

				case Graphics.VCENTER:
					y += linesInfo[ currentLine ][ INFO_LINE_HEIGHT ] - ( ( linesInfo[ currentLine ][ INFO_LINE_HEIGHT ] - fontHeight ) >> 1 );
				break;

				case Graphics.BOTTOM:
					y += fontHeight;
				break;
			} // fim switch ( verticalAlignment )
		} // fim for ( currentLine = initialLine; currentLine < totalLines && !imageEnd; ++currentLine )
		popClip( g );

		unlockText();
    } // fim do m�todo paint( Graphics )
	
	 
	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		if ( updatables != null ) {
			for ( int i = 0; i < updatables.length; ++i ) {
				updatables[ i ].update( delta );
			}
		}
	} // fim do m�todo update( int )
	

	/**
	 * 
	 * @return 
	 */
	public final short[][] getLinesInfo() {
		return linesInfo;
	} // fim do m�todo getLinesInfo()

	 
	/**
	 * Obt�m a altura total do texto.
	 * @return altura total do texto, em pixels (pode exceder a altura definida do label).
	 */
	public int getTextTotalHeight() {
		int height = 0;
		for ( int i = 0; i < linesInfo.length; ++i )
			height += linesInfo[ i ][ INFO_LINE_HEIGHT ];
		
		return height;
	} // fim do m�todo getTextTotalHeight()
	
	
	/**
	 * Define o offset na posi��o inicial de desenho do texto.
	 * @param offset offset na posi��o inicial de desenho do texto, em pixels.
	 */
	public void setTextOffset( int offset ) {
		textOffset = offset;
		
		int h = 0;
		int i = 0;
		for ( ; i < linesInfo.length; ++i ) {
			if ( h - linesInfo[ i ][ INFO_LINE_HEIGHT ] <= textOffset )
				break;
			
			h -= linesInfo[ i ][ INFO_LINE_HEIGHT ];
		}
//		initialLine = i;// TODO ajustar initialLine e tratamento de desenho - conforme initialLine aumenta, a posi��o de desenho do texto torna-se incorreta
		initialLine = 0;
	} // fim do m�todo setTextOffset( int )
	
	
	/**
	 * Define o texto do label.
	 * @param text texto do label.
	 * @param formatText indica se o texto deve ser reformatado, de forma a preencher de forma �tima a nova �rea de texto.
	 */
	public void setText( String text, boolean formatText ) {
		// evita que o texto seja desenhado e/ou alterado enquanto o texto est� sendo reformatado, evitando-se assim
		// problemas de inconsist�ncia que podem gerar travamentos
		lockText();
		
		if ( text == null )
			this.text = "";
		else
			this.text = text;

		final char[] newTextArray = this.text.toCharArray();
		textArray = newTextArray;
		
		if ( formatText )
			formatText();

		// notifica todas as poss�veis threads esperando para alterar o texto, e volta a permitir desenhos do texto
		unlockText();
	} // fim do m�todo setText( String, boolean )

	
	/**
	 * Define o tamanho do label. Caso a largura mude, o texto � reformatado automaticamente.
	 * 
	 * @param x largura em pixels do drawable.
	 * @param y altura em pixels do drawable.
	 */
	public void setSize( int width, int height ) {
		// armazena a antiga largura do label. Caso ela mude, � necess�rio reformatar o texto.
		final int oldWidth = size.x;
		super.setSize( width, height );
		
		if ( oldWidth != width )
			formatText();
	}

	
	public int getTextOffset() {
		return textOffset;
	}
	
	
	private synchronized final void lockText() {
		while ( textInUse ) {
			try {
				wait();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}
		} // fim while ( textInUse )
		textInUse = true;		
	}
	

	private synchronized final void unlockText() {
		textInUse = false;
		notifyAll();		
	}
	
}
 
