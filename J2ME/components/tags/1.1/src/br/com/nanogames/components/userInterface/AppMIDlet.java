/*
 * AppMIDlet.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotOpenException;


/**
 *
 * @author peter
 */
public class AppMIDlet extends MIDlet {
 
	protected static AppMIDlet instance;
	
	// perfis de fabricantes
	public static final byte VENDOR_GENERIC			= 0;
	public static final byte VENDOR_NOKIA			= 1;
	public static final byte VENDOR_MOTOROLA		= 2;
	public static final byte VENDOR_SAMSUNG			= 3;
	public static final byte VENDOR_LG				= 4;
	public static final byte VENDOR_SONYERICSSON	= 5;
	public static final byte VENDOR_SIEMENS			= 6;
	public static final byte VENDOR_VODAFONE		= 7;
	
	// perfil do fabricante detectado
	public final byte VENDOR;
	
	private static final byte TOTAL_VENDORS = VENDOR_SIEMENS + 1;
		
	// strings que indicam cada fabricante (em algumas marcas, pode haver mais de uma poss�vel string, dependendo do modelo)
	protected final String[][] VENDOR_AGENT = {
		{},						// gen�rico
		{ "nokia" },			// Nokia
		{ "mot-", "motorola" },	// Motorola
		{ "samsung", "sec-" },	// Samsung
		{ "lg-" },				// LG
		{ "sonyericsson" },		// SonyEricsson
		{ "sie-" },				// Siemens
		{ "vodafone" },			// Vodafone TODO confirmar String
	};
	
	protected ScreenManager manager;
	
	// TODO: compartilhar informa��es no RecordStore a respeito dos jogos/aplicativos Nano instalados no aparelho
	
	
	protected AppMIDlet() {
		String platform = System.getProperty( "microedition.platform" );

		byte vendor = VENDOR_GENERIC;		
		if ( platform != null ) {
			platform = platform.toLowerCase();
			byte i, j;
			
			for ( i = 0; i < TOTAL_VENDORS && vendor == VENDOR_GENERIC; ++i ) {
				for ( j = 0; j < VENDOR_AGENT[ i ].length; ++j ) {
					if ( platform.indexOf( VENDOR_AGENT[ i ][ j ] ) >= 0 ) {
						// encontrou o fabricante (valor da vari�vel n�o � atribu�da aqui porque vari�veis "final"
						// n�o podem ser atribu�das em loops)
						vendor = i;
						break;
					}
				} // fim for ( j = 0; j < VENDOR_AGENT[ i ].length; ++j )
			} // fim for ( i = 0; i < TOTAL_VENDORS; ++i )
		} // fim if ( platform != null )
		
		if ( vendor == VENDOR_GENERIC ) {
			// aparelho n�o informou corretamente o seu fabricante; tenta a detec��o atrav�s da detec��o de APIs 
			// espec�ficas de cada fabricante. V�rias classes para o mesmo fabricante podem ser testados at� se encontrar
			// um v�lido, pois as APIs podem variar entre modelos e s�ries de modelos de cada fabricante.
			final byte[] vendors = new byte[] { 
				VENDOR_LG,
				VENDOR_SAMSUNG,
				VENDOR_SIEMENS,
				VENDOR_MOTOROLA,
				VENDOR_SONYERICSSON,
				VENDOR_VODAFONE,
			};
			
			for ( int i = 0; i < vendors.length; ++i ) {
				if ( checkVendor( vendors[ i ] ) ) {
					vendor = vendors[ i ];
					break;
				}
			}
		} // fim if ( vendor == VENDOR_GENERIC )
		
		VENDOR = vendor;		
	} // fim do construtor AppMIDlet()
	
	
	/**
	 * Verifica qual o fabricante do aparelho, atrav�s da tentativa de detec��o de APIs espec�ficas.
	 * @param vendor �ndice do fabricante a ser verificado.
	 * @return true, se o fabricante p�de ser determinado atrav�s da detec��o de API espec�fica, ou false caso contr�rio.
	 */
	private final boolean checkVendor( byte vendor ) {
		String[] classes = null;
		switch ( vendor ) {
			case VENDOR_GENERIC:
			case VENDOR_NOKIA:
			return false;
			
			case VENDOR_MOTOROLA:
				classes = new String[] { 
					"com.motorola.phonebook.PhoneBookRecord",
					"com.motorola.phone.Dialer",
					"com.motorola.multimedia.Lighting",
					"com.motorola.extensions.ScalableImage",
					"com.motorola.funlight.FunLight",
					"com.motorola.pim.Contact",
					"com.motorola.graphics.j3d.ActionTable",
				};
			break;
			
			case VENDOR_SAMSUNG:
				classes = new String[] { 
					"com.samsung.util.Acceleration",
					"com.samsung.util.AudioClip",
					"com.samsung.immersion.VibeTonz"
				};
			break;
			
			case VENDOR_LG:
				classes = new String[] { 
					"mmpp.media.BackLight",
					"mmpp.lang.MathFP",
					"mmpp.media.MediaPlayer",
					"mmpp.phone.ContentsManager",
					"mmpp.microedition.lcdui.TextFieldX",
				};
			break;
			
			case VENDOR_SONYERICSSON:
				classes = new String[] { 
					"com.sonyericsson.mmedia.AMRPlayer",
					"com.sonyericsson.util.TempFile" 
				};
			break;
			
			case VENDOR_SIEMENS:
				classes = new String[] { 
					"com.siemens.mp.lcdui.Image",
					"com.siemens.mp.game.Light",
					"com.siemens.mp.io.Connection",
					"com.siemens.mp.wireless.messaging.MessageConnection",
				};
			break;
			
			case VENDOR_VODAFONE:
				classes = new String[] {
					"com.vodafone.v10.system.device.DeviceControl",
				};
			break;
		}
		
		for ( int i = 0; i < classes.length; ++i ) {
			try {
				Class.forName( classes[ i ] );

				// se chegou nessa linha (ou seja, n�o lan�ou exce��o), � sinal de que encontrou a String
				vendor = ( byte ) i;
				return true;
			} catch ( Exception e ) {
			}
		} // fim for ( int i = 0; i < classes.length; ++i )
		
		return false;
	}
	
	
	/**
	 * Indica se uma base de dados j� existe.
	 * 
	 * @param databaseName nome da base de dados.
	 * @param expectedSlots quantidade de slots esperada da base. Valores menores ou iguais a zero ignoram este teste, ou
	 * seja, apenas indica se a base existe.
	 * @return true, caso a base j� exista e tenha a quantidade esperada de slots, e false caso contr�rio.
	 */	
	public static boolean hasDatabase( String databaseName, int expectedSlots ) {
        final String[] names = RecordStore.listRecordStores();
        
        if ( names != null ) {
            for ( int i = 0; i < names.length; ++i ) {
                if ( names[ i ].equals( databaseName ) ) {
					if ( expectedSlots > 0 ) {
						RecordStore rs = null;
						try {
							rs = RecordStore.openRecordStore( databaseName, false );
							return ( rs.getNumRecords() == expectedSlots );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 							e.printStackTrace();
							//#endif
							
							return false;
						} finally {
							if ( rs != null ) {
								try {
									rs.closeRecordStore();
								} catch ( Exception e ) {
									//#if DEBUG == "true"
//# 									e.printStackTrace();
									//#endif
								}
							}
						}
					}
					return true;
				}
            } // fim for ( int i = 0; i < names.length; ++i )
        } // fim if ( names != null )
        
        return false;		
	} // fim do m�todo hasDatabase( String, int )
    
	
	/**
	 * Cria uma base de dados com uma quantidade pr�-determinada de slots para grava��o.
	 * 
	 * @param dataBaseName nome da base de dados.
	 * @param numSlots n�mero de slots dispon�veis para grava��o. Caso a base de dados j� exista, e tenha a mesma quantidade
	 * de slots, apenas retorna. Caso a quantidade de slots seja diferente, a base � recriada com a nova quantidade de 
	 * slots.
	 * @throws java.lang.Exception caso haja erro ao criar a base de dados.
	 */
    public static synchronized void createDatabase( String dataBaseName, int numSlots ) throws Exception {
		RecordStore rs = null;
		
        try {		
			if ( hasDatabase( dataBaseName, 0 ) ) {
				rs = RecordStore.openRecordStore( dataBaseName, false );
				if ( numSlots == rs.getNumRecords() ) {
					// base j� existe, com a mesma quantidade de slots 
					return;
				} else {
					// quantidade de slots � diferente; apaga a base antiga e a recria com a nova quantidade
					rs.closeRecordStore();
					
					RecordStore.deleteRecordStore( dataBaseName );
				}
			}

            rs = RecordStore.openRecordStore( dataBaseName, true );
            
            for ( int i = 0; i < numSlots; ++i ) {
                rs.addRecord( null, 0, 0 );
            }
        } catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao criar base de dados: n�o h� espa�o dispon�vel." );
			//#else
				throw new Exception();
			//#endif
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif			
        } finally {
            if ( rs != null ) {
				try { 
					rs.closeRecordStore();
				} catch( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
				}
			} // fim if ( rs != null )
        } // fim finally
    } // fim do m�todo createDatabase( String, int )
	
	
	/**
	 * Grava dados numa base de dados.
	 * @param dataBaseName nome da base de dados (precisa j� existir)
	 * @param slot �ndice do slot onde os dados ser�o gravados. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite grava��o nos slots
	 * 1, 2 e 3.
	 * @param serializable refer�ncia para o objeto Serializable que gravar� seus dados no slot, atrav�s do seu m�todo
	 * <i>write( DataOutputStream )</i>. N�o � necess�rio chamar o m�todo flush() do DataOutputStream.
	 * @throws java.lang.Exception caso haja erro ao abrir ou gravar os dados na base.
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */	
	public static synchronized final void saveData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		// TODO criar classe espec�fica para leitura e grava��o de arquivos (RMS, arquivos no JAR, arquivos no celular)
		if ( serializable == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao gravar na base de dados: serializable n�o pode ser null." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		if ( !hasDatabase( dataBaseName, 0 ) ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao gravar dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
				throw new Exception();
			//#endif
		}
		
		ByteArrayOutputStream byteOutput = null;
		DataOutputStream dataOutput = null;
		RecordStore rs = null;		
		
		try {
			byteOutput = new ByteArrayOutputStream();
			dataOutput = new DataOutputStream( byteOutput );
			
			serializable.write( dataOutput );
			
			dataOutput.flush();
			
			final byte[] data = byteOutput.toByteArray();
        
            rs = RecordStore.openRecordStore( dataBaseName, false );
            rs.setRecord( slot, data, 0, data.length );
		} catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao gravar na base de dados: n�o h� espa�o dispon�vel." );
			//#else
				throw new Exception();
			//#endif
        } catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao gravar na base de dados: slot inv�lido: " + slot );
			//#else
				throw new Exception();
			//#endif
		} finally {
            if ( rs != null ) {
				try { 
					rs.closeRecordStore(); 
				} catch( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif					
				}
			} // fim if ( rs != null )			
			
			if ( byteOutput != null ) {
				try {
					byteOutput.close();
				} catch ( Exception e ) {
				}
				byteOutput = null;
			} // fim if ( byteOutput != null )
			
			if ( dataOutput != null ) {
				try {
					dataOutput.close();
				} catch ( Exception e ) {
				}
				dataOutput = null;
			} // fim if ( dataOutput != null )
		} // fim finally
	} // fim do m�todo saveData( String, int, Serializable )
	
	
	/**
	 * Apaga o conte�do de um slot de uma base de dados, substituindo seu conte�do por um array de 0 bytes.
	 * @param dataBaseName nome da base de dados (precisa j� existir).
	 * @param slot �ndice do slot cujos dados ser�o apagados. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite grava��o nos slots
	 * 1, 2 e 3.
	 * @throws java.lang.Exception caso a base de dados ou o slot n�o existam, ou haja erros na abertura ou grava��o dos
	 * dados.
	 */
	public static synchronized final void eraseSlot( String dataBaseName, int slot ) throws Exception {
		if ( hasDatabase( dataBaseName, 0 ) ) {
			RecordStore rs = null;

			try {
				rs = RecordStore.openRecordStore( dataBaseName, false );

				rs.setRecord( slot, new byte[ 0 ], 0, 0 );
			} catch ( InvalidRecordIDException ire ) {
				//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler da base de dados: slot inv�lido: " + slot );
				//#else
					throw new Exception();
				//#endif
			} finally {
				if ( rs != null ) {
					try { 
						rs.closeRecordStore(); 
					} catch( Exception e ) {
						//#if DEBUG == "true"
//# 					e.printStackTrace();
						//#endif
					} // fim catch( Exception e )
				} // fim if ( rs != null )			
			} // fim finally					
		} else {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
				throw new Exception();
			//#endif
			
		}
	} // fim do m�todo eraseSlot( String, int )
	
	
	/**
	 * L� os dados dispon�veis num slot de uma base de dados.
	 * @param dataBaseName nome da base de dados.
	 * @param slot �ndice do slot de onde os dados ser�o lidos. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots possui como �ndices v�lidos
	 * apenas 1, 2 e 3.
	 * @param serializable refer�ncia para o objeto Serializable que interpretar� o array de bytes lido da base de dados,
	 * atrav�s do seu m�todo <i>read( DataInputStream )</i>.
	 * @throws java.lang.Exception
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 */	
	public static synchronized final void loadData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		if ( serializable == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao ler da base de dados: serializable n�o pode ser null." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		if ( !hasDatabase( dataBaseName, 0 ) ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
				throw new Exception();
			//#endif
		}
		
		ByteArrayInputStream byteInput = null;
		DataInputStream dataInput = null;
		RecordStore rs = null;
		
		try {
			rs = RecordStore.openRecordStore( dataBaseName, false );

			final byte[] data = rs.getRecord( slot );
			
			byteInput = new ByteArrayInputStream( data );
			dataInput = new DataInputStream( byteInput );
				
			serializable.read( dataInput );
		} catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler da base de dados: slot inv�lido: " + slot );
			//#else
				throw new Exception();
			//#endif
		} finally {
            if ( rs != null ) {
				try { 
					rs.closeRecordStore(); 
				} catch( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
				} // fim catch( Exception e )
			} // fim if ( rs != null )			
			
			if ( byteInput != null ) {
				try {
					byteInput.close();
				} catch ( Exception e ) {
				}
				byteInput = null;
			} // fim if ( byteInput != null )
			
			if ( dataInput != null ) {
				try {
					dataInput.close();
				} catch ( Exception e ) {
				}
				dataInput = null;
			} // fim if ( dataInput != null )
		} // fim finally		
	} // fim do m�todo loadData( String, int, Serializable )
	
	
	/**
	 * Abre um arquivo para leitura.
	 * @param filename endere�o do arquivo a ser lido.
	 * @param serializable refer�ncia para o objeto Serializable que far� a leitura dos dados do arquivo, atrav�s do seu 
	 * m�todo <i>read( DataInputStream )</i>.
	 * @throws java.lang.Exception
	 */		
	public static final void openFile( String filename, Serializable serializable ) throws Exception {
		InputStream inputStream = null;
		DataInputStream dataInputStream = null;
		
		try {
			inputStream = ( InputStream ) instance.getClass().getResourceAsStream( filename );
			dataInputStream = new DataInputStream( inputStream );
			
			serializable.read( dataInputStream );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		} finally {
			try {
				if ( dataInputStream != null ) {
					try {
						dataInputStream.close();
					} catch ( Exception ex ) {
					}
					dataInputStream = null;
				}

				if ( inputStream != null ) {
					try {
						inputStream.close();
					} catch ( Exception ex ) {
					}

					inputStream = null;
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif				
			}
		} // fim finally				
	} // fim do m�todo openFile( String, Serializable )
	

	public static AppMIDlet getInstance() {
		return instance;
	}

	
	protected void startApp() throws MIDletStateChangeException {
		if ( instance == null )
			instance = this;
		
		if ( manager != null )
			manager.showNotify();
	}

	
	protected void pauseApp() {
		if ( instance != null && manager != null )
			manager.hideNotify();		
	}

	
	protected void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( instance != null && instance.manager != null ) {
			instance.manager.hideNotify();
			instance.manager.stop();
			instance.manager = null;
		}
		instance = null;
		
		notifyDestroyed();
	}
	
	
	public static final void exit() {
		try {
			instance.destroyApp( true );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	}
	 

	public static byte getVendor() {
		return instance.VENDOR;
	}
	
	
}
 
