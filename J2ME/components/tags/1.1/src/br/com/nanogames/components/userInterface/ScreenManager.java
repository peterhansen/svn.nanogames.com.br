/*
 * ScreenManager.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import java.util.Hashtable;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;


/**
 *
 * @author peter
 */
public class ScreenManager extends GameCanvas implements KeysConstants, Runnable {
	
	protected boolean paused;
	
	protected boolean running;
	 
	protected Thread thread;
	 
	// tempo em milisegundos decorridos no �ltimo frame
	protected int lastFrameTime;	
 
	protected int accTime;
	 
	protected Updatable updatable;
	 
	// objeto que recebe os eventos de teclas (pode ser null)
	protected KeyListener keyListener;
	
	// objeto que recebe os eventos showNotify/hideNotify (pode ser null)
	protected ScreenListener screenListener;
	 
	protected final byte GET_SIZE_REPEAT_TIMES = 10;
	protected byte getSizeTimes = GET_SIZE_REPEAT_TIMES;
	 
	// cor padr�o de preenchimento do background
	public static final int DEFAULT_BG_COLOR = 0x000000;
	
	// cor de preenchimento do background
	protected int bgColor = DEFAULT_BG_COLOR;
	
	// Drawable de fundo (caso seja nulo, o fundo � preenchido com a cor de fundo definida)
	protected Drawable background;
	// refer�ncia para o drawable de fundo, caso ele seja atualiz�vel
	protected Updatable updatableBackground;
	
	// tipos poss�veis de transi��o de tela
	// n�o h� transi��o; tela nova substitui a anterior instantaneamente
	public static final byte TRANSITION_TYPE_IMMEDIATE	= 0;
	// transi��o em 1 est�gio: tela anterior sai ao mesmo tempo que a nova entra
	public static final byte TRANSITION_TYPE_1_STAGE	= 1;
	// transi��o em 2 est�gios: tela anterior sai, e ent�o a nova entra
	public static final byte TRANSITION_TYPE_2_STAGES	= 2;
	
	// tipo da transi��o atual
	protected byte transitionType;
	
	// est�gios da transi��o (a��es executadas em cada est�gio dependem do tipo de transi��o)
	protected static final byte TRANSITION_STAGE_IDLE	= 0;
	protected static final byte TRANSITION_STAGE_1		= 1;
	protected static final byte TRANSITION_STAGE_2		= 2;
	
	// est�gio atual da transi��o
	protected byte transitionStage;
	
	// tela atual e pr�xima tela, caso esteja em transi��o
	protected Drawable currentScreen;
	protected Drawable nextScreen;
	
	// transi��es da tela anterior e da pr�xima, caso existam (vari�veis utilizadas para evitar chamadas excessivas de
	// "instanceof" e typecasts a fim de descobrir se os Drawables atual e final implementam a transi��o animada). A
	// refer�ncia do tipo "Updatable" � utilizada para chamar o m�todo update da nova tela, caso esta implemente a
	// interface "Updatable".
	protected AnimatedTransition currentScreenTransition;
	protected AnimatedTransition nextScreenTransition;
	protected Updatable nextScreenUpdatable;
	
	// "labels" dos softkeys (drawables que indicam as a��es dos softkeys esquerdo, central e direito)
	protected final Drawable[] softKeys = new Drawable[ 3 ];
	protected final Updatable[] softKeysUpdatables = new Updatable[ 3 ];
	
	// �ndices dos softkeys
	public static final byte SOFT_KEY_LEFT	= 0;
	public static final byte SOFT_KEY_MID	= 1;
	public static final byte SOFT_KEY_RIGHT	= 2;
	 
	// tempo m�ximo de um frame. Caso o intervalo de atualiza��o seja maior, ele � reduzido a esse valor, impedindo
	// que intervalos muito grandes de atualiza��o causem inconsist�ncia (como n�o detec��o de colis�o entre objetos
	// cujas trajet�rias se cruzaram nesse intervalo, por exemplo).
	protected final int MAX_FRAME_TIME;
	 
	protected static int keyState;
	
	// teclas especiais
	public static final int KEY_SOFT_LEFT		= 0xff0001;
	public static final int KEY_SOFT_RIGHT		= 0xff0002;
	public static final int KEY_CLEAR			= 0xff0003;
	public static final int KEY_SOFT_MID		= 0xff0004;
	public static final int KEY_CHANGE_INPUT	= 0xff0005;
	public static final int KEY_BACK			= 0xff0006;
	
	protected static final Hashtable specialKeysTable = new Hashtable( 5 );
	
	protected static ScreenManager instance;
	
	// dimens�es da tela. Os valores referentes � tela cheia s� s�o preenchidos no momento da primeira visualiza��o da 
	// tela, ou seja, ap�s a primeira chamada ao m�todo paint().
	public static int SCREEN_WIDTH;
	public static int SCREEN_HALF_WIDTH;
	public static int SCREEN_HEIGHT;
	public static int SCREEN_HALF_HEIGHT;
	
	// vari�veis utilizadas somente no modo SHOW_FREE_MEM, que mostra na tela a mem�ria heap livre
	//#if SHOW_FREE_MEM == "true"
//# 	// intervalo de atualiza��o da mem�ria livre em milisegundos
//# 	private static final short SHOW_FREE_MEM_UPDATE_INTERVAL = 150;
//# 	// tempo restante at� a pr�xima atualiza��o da mem�ria livre
//# 	private int timeToNextUpdate;
//# 	// mem�ria livre
//# 	private String freeMem = "";
	//#endif
	
	
	// vari�veis utilizadas somente no modo SHOW_FPS, que mostra a taxa de quadros por segundo
	//#if SHOW_FPS == "true"
//# 	// intervalo em milisegundos entre as atualiza��es do indicador de fps
//# 	private static final short SHOW_FPS_UPDATE_INTERVAL = 150;
//# 	private static final short SHOW_FPS_FRAME_MULTIPLIER = 1000 / SHOW_FPS_UPDATE_INTERVAL;
//# 	private short timeToNextFpsUpdate;
//# 	
//# 	private short frameCounter;
//# 	
//# 	private String fps = "";
	//#endif
	
	
	private boolean painting;
	
	
	/** indica se os eventos de teclas repassados ao KeyListener (caso haja um) j� receber�o o valor da tecla pressionada
	 * considerando-se as fun��es especiais (soft keys, BACK, CLEAR, FIRE, RIGHT, etc)
	 */
	private boolean handleSpecialKey = true;
	
	
	protected ScreenManager( boolean suppressKeyEvents, int maxFrameTime ) {
		super( suppressKeyEvents );
		
		MAX_FRAME_TIME = maxFrameTime;
		
		// obt�m as dimens�es da tela - nesse ponto, os valores podem n�o referir-se � tela cheia; para se obter os valores
		// referentes � tela cheia, somente ap�s a primeira chamada ao m�todo paint().
		setFullScreenMode( true );
		SCREEN_WIDTH = getWidth();
		SCREEN_HALF_WIDTH = SCREEN_WIDTH >> 1;
		SCREEN_HEIGHT = getHeight();
		SCREEN_HALF_HEIGHT = SCREEN_HEIGHT >> 1;
	}
	

	public static ScreenManager createInstance( int maxFrameTime ) {
		instance = null;
		
		instance = new ScreenManager( false, maxFrameTime );
		
		if ( instance != null ) {
			// adiciona o 0 para evitar quebra de alguns aparelhos caso seja chamado getGameAction( 0 ), dentro de getSpecialKey(int)
			instance.specialKeysTable.put( new Integer( 0 ), new Integer( 0 ) );
			
			// adiciona as teclas de 0 a 9, para evitar que sejam capturadas por getGameAction(), dentro de getSpecialKey(),
			// em especial evitando que sejam associadas aos valores GAME_A, GAME_B, GAME_C e GAME_D (como ocorre nos
			// aparelhos Motorola, por exemplo)
			for ( int i = KEY_NUM0; i <= KEY_NUM9; ++i )
				instance.specialKeysTable.put( new Integer( i ), new Integer( i ) );
			
			switch ( AppMIDlet.getVendor() ) {
				case AppMIDlet.VENDOR_LG:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_LG ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_LG_2 ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_LG ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_LG_2 ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_LG ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_LG ), new Integer( KEY_CLEAR ) );	
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_LG_2 ), new Integer( KEY_CLEAR ) );	
				break;
				
				case AppMIDlet.VENDOR_NOKIA:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_NOKIA ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_NOKIA ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_NOKIA ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_NOKIA ), new Integer( KEY_CLEAR ) );					
				break;

				case AppMIDlet.VENDOR_MOTOROLA:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_MOTOROLA ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_MOTOROLA_2 ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_MID_MOTOROLA ), new Integer( KEY_SOFT_MID ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_MID_MOTOROLA_2 ), new Integer( KEY_SOFT_MID ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_MOTOROLA ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_MOTOROLA_2 ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_MOTOROLA ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_MOTOROLA ), new Integer( KEY_CLEAR ) );
				break;

				case AppMIDlet.VENDOR_SIEMENS:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SIEMENS ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SIEMENS_2 ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SIEMENS ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SIEMENS_2 ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_SIEMENS ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_SIEMENS ), new Integer( KEY_CLEAR ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_SIEMENS_2 ), new Integer( KEY_CLEAR ) );
				break;
				
				case AppMIDlet.VENDOR_SAMSUNG:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SAMSUNG ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SAMSUNG ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_SAMSUNG ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_SAMSUNG ), new Integer( KEY_CLEAR ) );					
				break;
				
				case AppMIDlet.VENDOR_SONYERICSSON:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SONYERICSSON ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SONYERICSSON ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_SONYERICSSON ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_SONYERICSSON ), new Integer( KEY_CLEAR ) );
					instance.specialKeysTable.put( new Integer( KEY_BACK_SONYERICSSON ), new Integer( KEY_BACK ) );
				break;				
				
				default:
					instance.specialKeysTable.put( new Integer( KEY_SOFT_LEFT_GENERIC ), new Integer( KEY_SOFT_LEFT ) );
					instance.specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_GENERIC ), new Integer( KEY_SOFT_RIGHT ) );
					instance.specialKeysTable.put( new Integer( KEY_CHANGE_INPUT_GENERIC ), new Integer( KEY_CHANGE_INPUT ) );
					instance.specialKeysTable.put( new Integer( KEY_CLEAR_GENERIC ), new Integer( KEY_CLEAR ) );
			} // fim switch ( AppMIDlet.getVendor() )
		} // fim if ( instance != null )
		
		return instance;
	} // fim do m�todo createInstance( boolean, int )
	 
	
	/**
	 * Define o Drawable a ser utilizado como indicador de um softkey.
	 * @param softKey �ndice do softkey a ser definido. Valores v�lidos: KEY_SOFT_LEFT, KEY_SOFT_MID e KEY_SOFT_RIGHT.
	 * @param d Drawable a ser utilizado como indicador do softkey. Caso seja nulo, o softkey anterior (caso exista) �
	 * removido.
	 */
	public void setSoftKey( int softKey, Drawable d ) {
		try {
			softKeys[ softKey ] = d;
			if ( d instanceof Updatable )
				softKeysUpdatables[ softKey ] = ( Updatable ) d;
			else
				softKeysUpdatables[ softKey ] = null;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return;
		}
		
		if ( d != null ) {
			// posiciona o softKey
			final Point size = d.getSize();

			switch ( softKey ) {
				case SOFT_KEY_LEFT:
					d.defineReferencePixel( 0, size.y );
					d.setRefPixelPosition( 0, SCREEN_HEIGHT );
				break;
				case SOFT_KEY_MID:
					d.defineReferencePixel( size.x >> 1, size.y );
					d.setRefPixelPosition( SCREEN_HALF_WIDTH, SCREEN_HEIGHT );
				break;
				case SOFT_KEY_RIGHT:
					d.defineReferencePixel( size.x, size.y );
					d.setRefPixelPosition( SCREEN_WIDTH, SCREEN_HEIGHT );
				break;
			} // fim switch ( softKey )
		} // fim if ( d != null )		
	} // fim do m�todo setSoftKey( int, Drawable )
	 
	
	/**
	 * Troca a tela atual, podendo realizar uma anima��o de transi��o.
	 * @param screen nova tela a ser exibida. M�todos ser�o repassados a essa nova tela caso ela implemente pelo menos uma
	 * das seguintes interfaces:
	 * <ul>
	 * <li>AnimatedTransition</li>
	 * <li>KeyListener</li>
	 * <li>Updatable</li>
	 * <li>ScreenListener</li>
	 * </ul>
	 * @param transitionType tipo de transi��o de tela. Para que possa haver transi��o, o Drawable deve implementar a interface
	 * <i>AnimatedTransition</i>; caso contr�rio, ser� feita a transi��o do tipo TRANSITION_TYPE_IMMEDIATE. Tipos dispon�veis:
	 * <ul>
	 * <li>TRANSITION_TYPE_IMMEDIATE: n�o h� transi��o; tela nova substitui a anterior instantaneamente.</li>
	 * <li>TRANSITION_TYPE_1_STAGE: transi��o em 1 est�gio: tela anterior sai ao mesmo tempo que a nova entra.</li>
	 * <li>TRANSITION_TYPE_2_STAGES: transi��o em 2 est�gios: tela anterior sai, e ent�o a nova entra.</li>
	 * </ul>
	 *
	 * @see AnimatedTransition
	 * @see KeyListener
	 * @see Updatable
	 * @see ScreenListener
	 */
	public void setCurrentScreen( Drawable screen, byte transitionType ) {
		if ( screen instanceof AnimatedTransition ) {
			nextScreenTransition = ( AnimatedTransition ) screen;
		}
		else {
			nextScreenTransition = null;
			transitionType = TRANSITION_TYPE_IMMEDIATE;
		}

		this.transitionType = transitionType;
		nextScreen = screen;
		if ( screen instanceof Updatable )
			nextScreenUpdatable = ( Updatable ) screen;		
		
		switch ( transitionType ) {
			case TRANSITION_TYPE_IMMEDIATE:
				if ( screen instanceof AnimatedTransition )
					currentScreenTransition = ( AnimatedTransition ) screen;
				
				// j� encerrou a transi��o
				switchScreens();
				transitionStage = TRANSITION_STAGE_IDLE;
			break;
			
			case TRANSITION_TYPE_1_STAGE:
				if ( nextScreenTransition != null )
					nextScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_APPEARING );				
				
			// sem break
				
			case TRANSITION_TYPE_2_STAGES:
				if ( currentScreenTransition != null )
					currentScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_HIDING );
				
				transitionStage = TRANSITION_STAGE_1;
			break;
		} // fim switch ( transitionType )
	} // fim do m�todo setCurrentScreen( Drawable, byte )
	 
	
	/**
	 * Define a cor de preenchimento do fundo, utilizada quando n�o h� Drawable definido como background.
	 * @param bgColor cor s�lida usada para preenchimento do background. Valores negativos indicam para n�o preencher 
	 * o fundo da tela com uma cor s�lida, mesmo que n�o haja drawable definido como background.
	 */
	public void setBackgroundColor( int bgColor ) {
		this.bgColor = bgColor;
	}
	
	
	/**
	 * Define o comportamento dos eventos keyPressed(int) e keyReleased(int), repassados ao KeyListener corrente (caso
	 * haja um definido).
	 * <p>O valor padr�o do ScreenManager � <i>true</i>, e normalmente s� � necess�rio utilizar <i>false</i>
	 * caso haja necessidade de tratar separadamente a tecla e sua fun��o, como <i>KEY_NUM4</i> e <i>LEFT</i> e 
	 * <i>KEY_NUM5</i> e <i>FIRE</i>, por exemplo.</p>
	 *
	 * @param handleSpecialKey caso seja <i>true</i>, os eventos keyPressed e keyReleased j� verificam a exist�ncia de
	 * fun��es especiais ligadas � tecla, ou seja, n�o � necess�rio que o KeyListener chame novamente ScreenManager.getSpecialKey(key)
	 * para detectar a fun��o da tecla pressionada. Caso seja <i>false</i>, � repassado o mesmo c�digo de tecla recebido
	 * por ScreenManager.
	 */
	public static final void setKeyListenerBehavior( boolean handleSpecialKey ) {
		instance.handleSpecialKey = handleSpecialKey;
	}
	 
	
	/**
	 * Obt�m o estado das teclas do aparelho.
	 * @return int indicando qual a tecla mantida pressionada no momento, ou 0 (zero) caso n�o haja teclas pressionadas.
	 */
	public static int getKeyState() {
		// TODO: m�ltiplas teclas?
		return keyState;
	}
	
	 
	/**
	 * Obt�m a tecla especial associada a um c�digo de tecla pressionada.
	 * @param keyCode �ndice da tecla pressionada.
	 * @return tipo de tecla especial associada, ou 0 (zero) caso n�o haja fun��o especial associada a esse c�digo.
	 */
	public static int getSpecialKey( int keyCode ) {
		final Integer value = ( Integer ) specialKeysTable.get( new Integer( keyCode ) );

		if ( value != null )
			return value.intValue();

		// n�o foi encontrada tecla especial (soft keys, back, etc) associada � tecla. Retorna a a��o de jogo associada 
		// � tecla, se houver uma. Sen�o, retorna o c�digo da pr�pria tecla
		final int ret = instance.getGameAction( keyCode );
		return ret == 0 ? keyCode : ret;
	} // fim do m�todo getSpecialKey( int )
	

	protected synchronized void showNotify() {
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		try {
		//#endif
		
		paused = false;
		notifyAll();
			
		start();
		
		getSizeTimes = GET_SIZE_REPEAT_TIMES;
		
		if ( screenListener != null )
			screenListener.showNotify();
		
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif				
	}

	
	protected synchronized void hideNotify() {
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		try {
		//#endif
		
		paused = true;

		keyReleased( 0 );
		keyState = 0;
		
		// p�ra de tocar um som, caso necess�rio
		MediaPlayer.stop();
		
		if ( screenListener != null )
			screenListener.hideNotify();
		
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif		
	}

	
	public void paint( Graphics g ) {
		lockPaint();

		// Zera a transla��o acumulada, para evitar que eventuais "lixos" na deixados na �ltima chamada de paint causem 
		// erro de posicionamento nas futuras chamadas de desenho.		
		Drawable.translate.set( 0, 0 );

		setFullScreenMode( true );

		if ( getSizeTimes > 0 ) {
			--getSizeTimes;

			// verifica se o aparelho � capaz de vibrar. Essa chamada � feita aqui para que se tenha uma garantia maior
			// de a chamada ser realizada com uma tela ativa; caso contr�rio, o aparelho pode retornar incorretamente que
			// n�o pode vibrar.
			if ( getSizeTimes <= 0 )
				MediaPlayer.checkVibrationSupport();

			SCREEN_WIDTH = getWidth();
			SCREEN_HALF_WIDTH = SCREEN_WIDTH >> 1;
			SCREEN_HEIGHT = getHeight();
			SCREEN_HALF_HEIGHT = SCREEN_HEIGHT >> 1;
		} // fim if ( firstTime )

		g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );

		if ( background == null ) {
			if ( bgColor >= 0 ) {
				g.setColor( bgColor );
				g.fillRect( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
			}
		} else {
			background.draw( g );
		}

		switch ( transitionStage ) {
			case TRANSITION_STAGE_IDLE:
				if ( currentScreen != null )
					currentScreen.draw( g );
			break; // fim case TRANSITION_STAGE_IDLE

			case TRANSITION_STAGE_1:
				switch ( transitionType ) {
					case TRANSITION_TYPE_1_STAGE:
						if ( currentScreen != null )
							currentScreen.draw( g );

						if ( nextScreen != null )
							nextScreen.draw( g );
					break;

					case TRANSITION_TYPE_2_STAGES:
						if ( currentScreen != null )
							currentScreen.draw( g );
					break;
				} // fim switch ( transitionType )
			break; // fim case TRANSITION_STAGE_1

			case TRANSITION_STAGE_2:
				if ( currentScreen != null )
					currentScreen.draw( g );

				if ( nextScreen != null )
					nextScreen.draw( g );
			break; // fim case TRANSITION_STAGE_2
		} // fim switch ( transitionStage )

		// desenha os indicadores de softkeys (caso existam)
		for ( int i = 0; i < softKeys.length; ++i ) {
			if ( softKeys[ i ] != null )
				softKeys[ i ].draw( g );
		} // fim for ( int i = 0; i < softKeys.length; ++i )

		//#if SHOW_FREE_MEM == "true"
//# 		g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
//# 		g.setColor( 0xff0000 );
//# 		g.setFont( Font.getFont( Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_MEDIUM ) );
//# 		g.drawString( freeMem, SCREEN_WIDTH, 0, Graphics.TOP | Graphics.RIGHT );
		//#endif

		//#if SHOW_FPS == "true"
//# 			g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
//# 			g.setColor( 0x00ff00 );
//# 			final Font font = Font.getFont( Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_MEDIUM );
//# 			g.setFont( font );
//# 			g.drawString( fps, SCREEN_WIDTH, font.getHeight(), Graphics.TOP | Graphics.RIGHT );
		//#endif

		unlockPaint();
	} // fim do m�todo paint( Graphics )


	protected synchronized void keyReleased( int keyCode ) {
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		try {
		//#endif
			
		if ( keyListener != null )
			keyListener.keyReleased( handleSpecialKey ? getSpecialKey( keyCode ) : keyCode );
		
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif	
	}

	
	protected synchronized void keyPressed( int keyCode ) {
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		try {
		//#endif
			
		// TODO tratar m�ltiplas teclas pressionadas (verificar m�todos getKeyState, keyPressed, keyReleased)
		switch ( transitionStage ) {
			case TRANSITION_STAGE_IDLE:
				if ( keyListener != null )
					keyListener.keyPressed( handleSpecialKey ? getSpecialKey( keyCode ) : keyCode );
			break;
		}

		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif			
	}
	

	public void run() {
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		try {
		//#endif
		
        lastFrameTime = 0;
        running = true;
		
		Display.getDisplay( AppMIDlet.getInstance() ).setCurrent( this );		

		long time;
		long lastFrameMS = System.currentTimeMillis();
				
		while ( running ) {
			if ( paused ) {
				synchronized ( this ) {
					try {
						wait();
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif
					}
				}
				continue;
			} // fim if ( paused )
			
			time = System.currentTimeMillis();

			lastFrameTime = ( int ) ( time - lastFrameMS );
			// limita o tempo m�ximo do frame
			if ( lastFrameTime > MAX_FRAME_TIME )
				lastFrameTime = MAX_FRAME_TIME;

			accTime += lastFrameTime;

			if ( lastFrameTime > 0 ) {
				switch ( transitionStage ) {
					case TRANSITION_STAGE_1:
						switch ( transitionType ) {
							case TRANSITION_TYPE_1_STAGE:
								// a tela atual pode ser nula (normalmente, isso � v�lido na primeira transi��o de tela
								// feita no aplicativo)
								if ( currentScreenTransition != null )
									currentScreenTransition.updateTransition( lastFrameTime );

								// a pr�xima tela pode vir a ser nula, no caso de se desejar "esvaziar" a tela, por exemplo
								if ( nextScreenTransition != null )
									nextScreenTransition.updateTransition( lastFrameTime );

								// verifica se as telas atual e pr�xima (caso existam) terminaram suas transi��es (caso
								// uma tela seja nula, considera-se que sua transi��o est� terminada)
								if ( ( currentScreenTransition == null || currentScreenTransition.getTransitionState() == AnimatedTransition.TRANSITION_INACTIVE )
									&& ( nextScreenTransition == null || nextScreenTransition.getTransitionState() == AnimatedTransition.TRANSITION_SHOWN ) ) 
								{
									// terminou a transi��o
									transitionStage = TRANSITION_STAGE_IDLE;

									if ( currentScreenTransition != null )
										currentScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_INACTIVE );

									if ( nextScreenTransition != null )
										nextScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_SHOWN );

									switchScreens();
								}
							break; // fim case TRANSITION_TYPE_1_STAGE

							case TRANSITION_TYPE_2_STAGES:
								if ( currentScreenTransition != null )
									currentScreenTransition.updateTransition( lastFrameTime );

								// verifica se a tela atual terminou sua transi��o - a pr�xima tela s� iniciar� sua transi��o
								// quando esta terminar
								if ( currentScreenTransition == null || currentScreenTransition.getTransitionState() == AnimatedTransition.TRANSITION_INACTIVE )
								{
									// terminou a primeira etapa da transi��o
									transitionStage = TRANSITION_STAGE_2;

									if ( currentScreenTransition != null )
										currentScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_INACTIVE );

									if ( nextScreenTransition != null )
										nextScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_APPEARING );
								} // fim if ( currentScreenTransition == null || currentScreenTransition.getTransitionState() == AnimatedTransition.TRANSITION_INACTIVE )
							break; // fim case TRANSITION_TYPE_2_STAGES
						} // fim switch ( transitionType )

					break; // fim case TRANSITION_STAGE_1

					case TRANSITION_STAGE_2:
						// para estar neste est�gio, s� pode ser a transi��o do tipo TRANSITION_TYPE_2_STAGES; s� � necess�rio
						// atualizar a pr�xima tela, pois a primeira j� encerrou sua anima��o de sa�da
						if ( nextScreenTransition != null )
							nextScreenTransition.updateTransition( lastFrameTime );

						if ( nextScreenTransition == null || nextScreenTransition.getTransitionState() == AnimatedTransition.TRANSITION_SHOWN )
						{
							// terminou a transi��o
							transitionStage = TRANSITION_STAGE_IDLE;

							if ( nextScreenTransition != null )
								nextScreenTransition.setTransitionState( AnimatedTransition.TRANSITION_SHOWN );

							switchScreens();
						}
					break; // fim case TRANSITION_STAGE_2
				} // fim switch ( transitionType )

				if ( updatableBackground != null )
					updatableBackground.update( lastFrameTime );

				// se a tela atual for atualiz�vel, chama seu m�todo update
				if ( updatable != null )
					updatable.update( lastFrameTime );

				// caso a pr�xima tela seja atualiz�vel, tamb�m chama seu m�todo update
				if ( nextScreenUpdatable != null )
					nextScreenUpdatable.update( lastFrameTime );

				// atualiza os softkeys, caso sejam atualiz�veis
				for ( int i = 0; i < softKeysUpdatables.length; ++i ) {
					if ( softKeysUpdatables[ i ] != null )
						softKeysUpdatables[ i ].update( lastFrameTime );
				} // fim for ( int i = 0; i < softKeysUpdatables.length; ++i )
			} // fim if ( lastFrameTime > 0 )

			lastFrameMS = System.currentTimeMillis();

			//#if SHOW_FREE_MEM == "true"
//# 			timeToNextUpdate -= lastFrameTime;
//# 			if ( timeToNextUpdate <= 0 ) {
//# 				timeToNextUpdate = SHOW_FREE_MEM_UPDATE_INTERVAL;
//# 				
//# 				System.gc();
//# 				freeMem = String.valueOf( Runtime.getRuntime().freeMemory() );
//# 			}
			//#endif

			//#if SHOW_FPS == "true"
//# 			++frameCounter;
//# 			
//# 			timeToNextFpsUpdate -= lastFrameTime;
//# 			if ( timeToNextFpsUpdate <= 0 ) {
//# 				timeToNextFpsUpdate = SHOW_FPS_UPDATE_INTERVAL;
//# 				fps = String.valueOf( frameCounter * SHOW_FPS_FRAME_MULTIPLIER );
//# 				
//# 				frameCounter = 0;
//# 			}
			//#endif

			repaint();
			serviceRepaints();
		} // fim while ( running )
		//#if TRY_CATCH_MAIN_LOOP == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif
	} // fim do m�todo run()
	
			
	/**
	 * M�todo auxiliar utilizado para realizar a troca das refer�ncias da tela antiga e da nova.
	 */
	private final void switchScreens() {
		currentScreen = null;
		currentScreenTransition = null;
		updatable = null;
		System.gc();
		
		currentScreen = nextScreen;
		currentScreenTransition = nextScreenTransition;
		updatable = nextScreenUpdatable;
		
		nextScreenUpdatable = null;		
		nextScreen = null;
		nextScreenTransition = null;		
		
		
		// registra a tela atual como listener de teclas e/ou eventos de suspend
		if ( currentScreen instanceof KeyListener )
			keyListener = ( KeyListener ) currentScreen;
		else
			keyListener = null;
		
		if ( currentScreen instanceof ScreenListener )
			screenListener = ( ScreenListener ) currentScreen;		
		else
			screenListener = null;
	} // fim do m�todo switchScreens()
	
	
	/**
	 * Define o fundo.
	 * 
	 * @param background Drawable a ser usado como fundo de tela. Caso seja null, o fundo � preenchido com a cor definida.
	 * @param update indica se o background deve ser atualizado juntamente com a tela ativa. Para isso, o drawable deve
	 * implementar a interface <i>Updatable</i>. Para manter o background atual e somente alterar a op��o de atualiz�-lo
	 * ou n�o, basta chamar esse m�todo passando o mesmo drawable utilizado como background (pode-se utilizar o m�todo
	 * <i>getBackground</i> dessa classe, por exemplo), e alterar somente o par�mentro <i>update</i>.
	 */
	public void setBackground( Drawable background, boolean update ) {
		this.background = background;
		
		if ( update && ( background instanceof Updatable ) )
			updatableBackground = ( Updatable ) background;
		else
			updatableBackground = null;
	} // fim do m�todo setBackground( Drawable, boolean )
	
	
	/**
	 * Obt�m o Drawable sendo atualmente utilizado como fundo de tela.
	 * @return refer�ncia para o Drawable utilizado como fundo de tela, ou null caso o fundo esteja sendo preenchido
	 * com uma cor s�lida definida anteriormente.
	 */
	public final Drawable getBackground() {
		return background;
	}
	
	
	public static final ScreenManager getInstance() {
		return instance;
	}
	
	
    public final void start() {
        if ( running == false ) {
            thread = new Thread( this );
            thread.start();
        }
    }
    	
    
    public final void stop() {
		running = false;
		thread = null;
    }	 	
	

	private synchronized final void lockPaint() {
		while ( painting ) {
			try {
				wait();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}
		} // fim while ( textInUse )
		painting = true;		
	}
	

	private synchronized final void unlockPaint() {
		painting = false;
		notifyAll();		
	}	

	
	/**
	 * @param w 
	 * @param h 
	 */
	protected final void sizeChanged( int w, int h ) {
		if ( w != 0 && h != 0 ) {
			super.sizeChanged( w, h );
			
			SCREEN_WIDTH = w;
			SCREEN_HALF_WIDTH = w >> 1;
			
			SCREEN_HEIGHT = h;
			SCREEN_HALF_HEIGHT = h >> 1;

			// atualiza a posi��o dos labels dos softkeys (caso existam)
			for ( int i = SOFT_KEY_LEFT; i <= SOFT_KEY_RIGHT; ++i )
				setSoftKey( i, softKeys[ i ] );

			if ( screenListener != null )
				screenListener.sizeChanged( w, h );
		}
	}
	
	
	/**
	 * Retorna a refer�ncia para a tela corrente.
	 * @return refer�ncia para a tela corrente.
	 */
	public final Drawable getCurrentScreen() {
		return currentScreen;
	}	
	 
}
 
