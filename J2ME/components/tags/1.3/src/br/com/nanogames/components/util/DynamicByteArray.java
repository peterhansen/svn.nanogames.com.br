/**
 * DinamicByteArray.java
 * �2008 Nano Games.
 *
 * Created on 04/04/2008 16:29:10.
 */

package br.com.nanogames.components.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Daniel L. Alves
 */

public final class DynamicByteArray
{
	/** Tamanho padr�o do buffer utilizado para a leitura de dados em readInputStream() */
	public static final short DEFAULT_READ_BUFFER_SIZE = 1024;

	/** Dados contidos no objeto */
	private byte[] byteArray;
	
	/** Obt�m o tamanho do array */
	public final int length()
	{
		return byteArray.length;
	}
	
	/** Insere novos dados no array din�mico
	 * @param data Array de onde iremos copiar os dados
	 * @param offset �ndice do array a partir do qual iremos copiar os dados
	 * @param nBytes Quantos bytes iremos copiar do array fornecido
	 * @throws java.lang.IllegalArgumentException
	 */
	public final void insertData( byte[] data, int offset, int nBytes ) throws IllegalArgumentException
	{
		if( ( offset < 0 ) || ( nBytes < 0 ) )
		{
			//#if DEBUG == "true"
//# 				throw new IllegalArgumentException( "Os par�metros offset e nBytes devem ser maiores ou iguais a zero" );
			//#else
			throw new IllegalArgumentException();
			//#endif
		}
		
		if( nBytes == 0 )
			return;
		
		int currLength = 0;
			
		// Incrementa o tamanho do array
		if( byteArray != null )
		{
			currLength = byteArray.length;
			final byte[] aux = new byte[ currLength + nBytes ];
			System.arraycopy( byteArray, 0, aux, 0, currLength );
			
			byteArray = aux;
			System.gc();
		}
		else
		{
			byteArray = new byte[ nBytes ];
		}
		
		// Armazena os novos dados
		System.arraycopy( data, offset, byteArray, currLength, nBytes );
	}
	
	/** Obt�m o conte�do do array din�mico de bytes */
	public final byte[] getData()
	{
		return byteArray;
	}
	
	/** Preenche o array din�mico com os dados lidos de um InputStream */
	public final void readInputStream( InputStream input ) throws IOException
	{
		// Cria o buffer que ir� armazenar os dados lidos
		final byte[] buffer = new byte[ DEFAULT_READ_BUFFER_SIZE ];

		// L� os dados da stream de entrada
		int nBytes;
		while( ( nBytes = input.read( buffer, 0, buffer.length ) ) != -1 )
			insertData( buffer, 0, nBytes );
	}
}