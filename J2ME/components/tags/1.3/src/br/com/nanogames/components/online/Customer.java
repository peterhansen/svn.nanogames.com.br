package br.com.nanogames.components.online;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;


public final class Customer implements Serializable {

	/** Id do usu�rio "dummy", ou seja, n�o h� usu�rio registrado ainda. */
	public static final int ID_NONE = -1;
	/***/
	private int id = ID_NONE;
	//#if DEBUG == "true"
//# 	private String nickname = "chuchu";
//# 	private String email = "abc@nanogames.com.br";
//# 	private String password = ".adgj6";
//# 	private String firstName = "peter";
//# 	private String lastName = "hansen";
	//#else
	private String nickname = "";
	private String email = "@.com.br";
	private String password = "";
	private String firstName = "";
	private String lastName = "";
	//#endif
	private boolean rememberPassword = true;
	private char genre = 'n';


	public final void write( DataOutputStream output ) throws Exception {
		//#if DEBUG == "true"
//# 			System.out.println( "id: " + id + ", nickname: "+ nickname + ", email: " + email + ", nome: " + firstName );
		//#endif		
		output.writeInt( id );
		output.writeUTF( nickname );
		output.writeUTF( email );
		output.writeUTF( firstName );
		output.writeUTF( lastName );
		output.writeBoolean( rememberPassword );
		output.writeChar( genre );
		if ( rememberPassword ) {
			output.writeUTF( password );
		}
	}


	public final void read( DataInputStream input ) throws Exception {
		id = input.readInt();
		nickname = input.readUTF();
		email = input.readUTF();
		firstName = input.readUTF();
		lastName = input.readUTF();
		genre = input.readChar();
		rememberPassword = input.readBoolean();
		if ( rememberPassword ) {
			password = input.readUTF();
		}
	}


	/**
	 * Indica se o jogador j� est� registrado, ou seja, com um id >= 0.
	 * @return
	 */
	public final boolean isRegistered() {
		return id != ID_NONE;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail( String email ) {
		this.email = email;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName( String firstName ) {
		this.firstName = firstName;
	}


	public char getGenre() {
		return genre;
	}


	public void setGenre( char genre ) {
		this.genre = genre;
	}


	public int getId() {
		return id;
	}


	public void setId( int id ) {
		this.id = id;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName( String lastName ) {
		this.lastName = lastName;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname( String nickname ) {
		this.nickname = nickname;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword( String password ) {
		this.password = password;
	}


	public boolean isRememberPassword() {
		return rememberPassword;
	}


	public void setRememberPassword( boolean rememberPassword ) {
		this.rememberPassword = rememberPassword;
	}
	
	
	
}
