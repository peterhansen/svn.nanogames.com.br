/**
 * ToDo.java
 * �2008 Nano Games.
 *
 * Created on Jun 13, 2008 6:40:33 PM.
 */

package br.com.nanogames.components.util;

/**
 *
 * @author Peter
 */
public interface ToDo {

//TODO 1. Filhos apontarem o pai nos grupos: Poss�veis problemas:
//	1.1. n�o h� destrutor, logo deveria haver controle mais r�gido sobre desaloca��o de todos os grupos, varrendo os filhos e anulando a refer�ncia para o pai (sen�o mem�ria jamais ser� desalocada)
//	
//TODO 2. Spline
//TODO 3. NURBS
//TODO 5. Aprimorar o arquivo descritor dos sprites:
//TODO        5.1 Poder indicar transforma��es (espelhamento e rota��o)
//TODO        5.2 Poder indicar o n�mero de vezes que uma sequ�ncia deve ser executada
//TODO        5.3 Indicar o n�mero total de frames para otimizar a aloca��o dos arrays
//TODO        5.4 Indicar o n�mero total de sequ�ncias para otimizar a aloca��o dos arrays
//TODO        5.5 Poder indicar que n�o deve desenhar nada durante tanto tempo (frame "invis�vel")
//TODO 6. Op��es invis�veis no menu
//TODO 7. Op��es n�o-selecion�veis no menu
//TODO 8. tilemaps
//TODO 9. engine isom�trica
//TODO 12. grava��o e leitura de arquivos gen�ricos
//TODO 14: No RichLabel, retirar os espa�os nos finais e in�cios de linhas que foram geradas por quebra autom�tica
//
//
//**************************************************************************************************
//Caso vc queira acrescentar estes m�todos em NanoMath. J� utilizei-os in�meras vezes na minha vida.
//[]s
//
///** Obt�m o pr�ximo inteiro m�ltiplo de m */
//    private int nextMulOf( int n, int m )
//    {
//        final int aux = n % m;
//        if( aux == 0 )
//            return n;
//        return n + ( m - aux );
//    }
//
///** Obt�m o pr�ximo inteiro m�ltiplo de m. Otimizado para pot�ncias de 2 */
//private int nextMulOfPot2( int n, int m )
//{
//    --m;
//    return ( n + m ) & ~m;
//}
//
///** Retorna uma valor interpolado linearmente entre v1 e v2, tal que o valor de retorno seja v1 quando
//     * percentage � 0 e v2 quando percentage � 100
//     */
//    public final int lerp( int v1, int v2, int percentage )
//    {
//        // O par�metro percentage deve estar entre 0 e 100
//        clamp( percentage, 0, 100 );
//       
//        return v1 + ( ( percentage * ( v2 - v1 ) ) / 100 );
//    }
//
//    /** Coloca o valor de i dentro do intervalo [min, max] */
//    public final int clamp( int i, int min, int max )
//    {
//        return i <= min ? min : i >= max ? max : i;
//    }
//
//	TODO classe/m�todo para cria��o autom�tica de bordas (cont�iner?), que possam ser aumentadas/diminu�das automaticamente
	
}
