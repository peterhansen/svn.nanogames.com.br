/**
 * RankingScreen.java
 * 
 * Created on 1/Fev/2009, 11:55:59
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;
import java.util.Vector;

/**
 *
 * @author Peter
 */
public class RankingScreen extends NanoOnlineContainer implements ConnectionListener {
	
	/** �ndice de par�metro de submiss�o de ranking online: quantidade de entradas sendo enviadas. Tipo do dado: byte */
	public static final byte PARAM_N_ENTRIES	= 0;
	/** �ndice de par�metro de submiss�o de ranking online: subtipo do ranking. Tipo do dado: byte */
	public static final byte PARAM_SUB_TYPE		= 1;
	
	/***/
	public static final String RANKING_DATABASE = "ro";
	
	/** Resultado de submiss�o de pontos: n�o � melhor pontua��o local nem recorde pessoal. */
	public static final byte SCORE_STATUS_NONE							= 0;
	/** Resultado de submiss�o de pontos: novo recorde local. */
	public static final byte SCORE_STATUS_NEW_LOCAL_RECORD				= 1;
	/** Resultado de submiss�o de pontos: novo recorde pessoal (no aparelho atual - jogador pode ter submetido pontua��o maior em outro aparelho). */
	public static final byte SCORE_STATUS_NEW_PERSONAL_RECORD			= 2;
	/** Resultado de submiss�o de pontos: novo recorde local E pessoal (no aparelho atual - jogador pode ter submetido pontua��o maior em outro aparelho). */
	public static final byte SCORE_STATUS_NEW_PERSONAL_AND_LOCAL_RECORD	= SCORE_STATUS_NEW_LOCAL_RECORD | SCORE_STATUS_NEW_PERSONAL_RECORD;
	
	/** M�ximo de entradas de ranking. */
	public static final byte MAX_RANKING_ENTRIES = 10;
	
	/***/
	private static final byte TOTAL_ENTRIES = MAX_RANKING_ENTRIES + 4;
	
	/** Bot�o para atualizar o ranking (somente ranking online). */
	private static final byte ID_BUTTON_UPDATE = TOTAL_ENTRIES;
	
	/** Bot�o para voltar (ou cancelar a conex�o, no modo online). */
	private static final byte ID_BUTTON_BACK = ID_BUTTON_UPDATE + 1;
	
	/** Bot�o para apagar a lista de recordes. */
	private static final byte ID_BUTTON_ERASE = ID_BUTTON_BACK + 1;
	
	private final byte type;
	
	/***/
	private static long lastScore;
	
	private static byte lastType;
	
	private static boolean lastDecrescent;
	
	private final boolean online;
	
	/***/
	private static byte totalTypes;
	
	private static RankingFormatter formatter;
	
	private static int[] typesEntries;
	
	
	public RankingScreen( boolean online, int type, int backIndex ) throws Exception {
		super( TOTAL_ENTRIES );
		
		setBackIndex( backIndex );
		
		this.online = online;
		this.type = ( byte ) type;
		
		final ImageFont font = NanoOnline.getFont( FONT_DEFAULT );
		
		// tenta carregar as entradas de ranking salvas (se existirem)
		RankingEntry[] rankingEntries;
		try {
			final RankingLoader loader = new RankingLoader( type, online );
			loader.load();
			rankingEntries = loader.rankingEntries;
		} catch ( Exception e ) {
			rankingEntries = new RankingEntry[ MAX_RANKING_ENTRIES ];

			for ( byte i = 0; i < MAX_RANKING_ENTRIES; ++i ) {
				rankingEntries[ i ] = new RankingEntry();
				if ( formatter != null )
					formatter.initLocalEntry( type, rankingEntries[ i ], i );
			}

			try {
				new RankingLoader( this.type, online, rankingEntries ).save();
			} catch ( Exception e2 ) {
				//#if DEBUG == "true"
	//# 						e2.printStackTrace();
				//#endif
			}
		}

		final byte offset = online ? MAX_PROFILES : 0;
		for ( byte i = 0; i < MAX_RANKING_ENTRIES; ++i ) {
			final RankingEntry entry = rankingEntries[ i + offset ];
			if ( online && entry.getProfileId() < 0 )
				break;
			
			final StringBuffer buffer = new StringBuffer();
			
			buffer.append( ( i + 1 ) );
			buffer.append( ". " );
			
			if ( formatter == null )
				buffer.append( entry.getScore() );
			else
				buffer.append( formatter.format( type, entry.getScore() ) );
			
			buffer.append( ' ' );
			buffer.append( entry.getNickname() );
			
			final Button button = new Button( font, buffer.toString() );
			button.setEnabled( false );
			button.setBorder( NanoOnline.getBorder() );
			insertDrawable( button );
		}
		
		if ( online ) {
			final Button update = new Button( font, NanoOnline.getText( TEXT_UPDATE ) );
			update.setId( ID_BUTTON_UPDATE );
			update.setBorder( NanoOnline.getBorder() );
			update.addActionListener( this );

			insertDrawable( update );
		}
		
		final Button erase = new Button( font, NanoOnline.getText( TEXT_ERASE_RECORDS ) );
		erase.setId( ID_BUTTON_ERASE );
		erase.setBorder( NanoOnline.getBorder() );
		erase.addActionListener( this );

		insertDrawable( erase );
		
		final Button back = new Button( font, NanoOnline.getText( TEXT_BACK ) );
		back.setId( ID_BUTTON_BACK );
		back.setBorder( NanoOnline.getBorder() );
		back.addActionListener( this );

		insertDrawable( back );
	}
	
	
	/**
	 * Inicializa as tabelas de ranking.
	 * @param typesEntries �ndices dos textos (em AppMIDlet) correspondentes a cada tipo de ranking. Esses ser�o os t�tulos
	 * das entradas mostradas no menu de ranking. Cada tipo possui sua pr�pria tabela local e global.
	 * @param rankingFormatter formatador do ranking. Caso seja null, todos as entradas de ranking local s�o inicializadas
	 * com pontua��o zero, e com apelidos vazios, e as pontua��es s�o exibidas como inteiros.
	 * @throws java.lang.Exception
	 */
	public static final void init( int[] typesEntries, RankingFormatter rankingFormatter ) throws Exception {
		formatter = rankingFormatter;
		RankingScreen.typesEntries = typesEntries;
		RankingScreen.totalTypes = ( byte ) typesEntries.length;
		
		final int TOTAL_SLOTS = totalTypes << 1;
		if ( AppMIDlet.createDatabase( RANKING_DATABASE, TOTAL_SLOTS ) ) {
			// base de dados n�o existia; preenche a tabela local com valores iniciais
			try {
				for ( byte t = 0; t < TOTAL_SLOTS; ++t ) {
					final RankingEntry[] rankingEntries = new RankingEntry[ MAX_RANKING_ENTRIES  + ( t < totalTypes ? 0 : MAX_PROFILES ) ];
					for ( byte i = 0; i < rankingEntries.length; ++i ) {
						rankingEntries[ i ] = new RankingEntry();
						if ( t < totalTypes && formatter != null )
							formatter.initLocalEntry( t, rankingEntries[ i ], i );
					}
					
					new RankingLoader( t >> 1, t >= totalTypes, rankingEntries ).save();
				}			
			} catch ( Exception e2 ) {
				//#if DEBUG == "true"
//# 						e2.printStackTrace();
				//#endif
			}
		}
	}
	
	
	/**
	 * Verifica se uma pontua��o � melhor marca pessoal de um jogador ou est� entre as melhores marcas locais.
	 * 
	 * @param type �ndice da tabela de ranking (ex.: 1 por pista).
	 * @param profileId �ndice do perfil de usu�rio que obteve a pontua��o testada. Utilizar valores negativos faz somente 
	 * a verifica��o de recordes locais.
	 * @param score pontua��o a ser verificada.
	 * @param decrescent indica se a tabela de recorde � descrescente, ou seja, maiores pontua��es no topo da tabela.
	 * @return status da pontua��o:
	 * <ul>
	 * <li>SCORE_STATUS_NONE</li>
	 * <li>SCORE_STATUS_NEW_PERSONAL_RECORD</li>
	 * <li>SCORE_STATUS_NEW_LOCAL_RECORD</li>
	 * <li>SCORE_STATUS_NEW_PERSONAL_AND_LOCAL_RECORD</li>
	 * </ul>
	 */
	public static final byte isHighScore( int type, int profileId, long score, boolean decrescent ) throws Exception {
		byte scoreStatus = SCORE_STATUS_NONE;
		
		setLastInfo( type, score, decrescent );
		
		// verifica a pontua��o local
		if ( getScoreIndex( type, false, profileId, score, decrescent ) >= 0 )
			scoreStatus |= SCORE_STATUS_NEW_LOCAL_RECORD;
		
		// verifica a pontua��o global do jogador
		if ( getScoreIndex( type, true, profileId, score, decrescent ) >= 0 )
			scoreStatus |= SCORE_STATUS_NEW_PERSONAL_RECORD;
		
		return scoreStatus;
	}
	
	
	public static final byte setHighScore( int type, String nickname, long score, boolean decrescent ) throws Exception {
		return setHighScore( type, Customer.ID_NONE, nickname, score, decrescent );
	}
		
	
	public static final byte setHighScore( int type, int profileId, long score, boolean decrescent ) throws Exception {
		return setHighScore( type, profileId, null, score, decrescent );
	}
	
	
	private static final byte setHighScore( int type, int profileId, String nickname, long score, boolean decrescent ) throws Exception {
		byte scoreStatus = SCORE_STATUS_NONE;
		
		setLastInfo( type, score, decrescent );
		
		byte index = getScoreIndex( type, false, profileId, score, decrescent );
		
		// primeiro verifica o ranking local
		if ( index >= 0 ) {
			scoreStatus |= SCORE_STATUS_NEW_LOCAL_RECORD;
			
			final RankingLoader loader = new RankingLoader( type, false );
			loader.load();
			final RankingEntry[] entries = loader.rankingEntries;
			
			moveOlderEntries( entries, index );

			entries[ index ].setProfileId( profileId );
			if ( NanoOnline.getCustomerById( profileId ) == null )
				entries[ index ].setNickname( nickname == null ? "----" : nickname );
			else
				entries[ index ].setNickname( NanoOnline.getCustomerById( profileId ).getNickname() );
			entries[ index ].setSubmitted( false );
			entries[ index ].setScore( score );
			
			loader.save();
		}
		
		// verifica a pontua��o global do jogador
		if ( profileId >= 0 ) {
			index = getScoreIndex( type, true, profileId, score, decrescent );
			if ( index >= 0 ) {
				scoreStatus |= SCORE_STATUS_NEW_PERSONAL_RECORD;

				final RankingLoader loader = new RankingLoader( type, true );
				loader.load();
				final RankingEntry[] entries = loader.rankingEntries;
				
				moveOlderEntries( entries, index );

				entries[ index ].setProfileId( profileId );
				entries[ index ].setNickname( NanoOnline.getCustomerById( profileId ).getNickname() );
				entries[ index ].setSubmitted( false );
				entries[ index ].setScore( score );

				loader.save();
			}
		}
		
		return scoreStatus;
	}
	
	
	/**
	 * Faz o "chega-pra-l�" nas entradas do ranking, para inser��o de novo recorde.
	 * @param entries
	 * @param initialIndex
	 */
	private static final void moveOlderEntries( RankingEntry[] entries, int initialIndex ) {
		for ( int i = entries.length - 1; i > initialIndex; --i ) {
			final RankingEntry oldEntry = entries[ i ];
			final RankingEntry newEntry = entries[ i - 1 ];
			
			oldEntry.setProfileId( newEntry.getProfileId() );
			oldEntry.setNickname( newEntry.getNickname() );
			oldEntry.setSubmitted( newEntry.isSubmitted() );
			oldEntry.setScore( newEntry.getScore() );
		}
	}
	
	
	/**
	 * 
	 * @param type
	 * @param online
	 * @throws java.lang.Exception
	 */
	private static final void eraseRecords( int type, boolean online ) throws Exception {
		if ( online ) {
			final RankingEntry[] entries = new RankingEntry[ 0 ];
			final RankingLoader loader = new RankingLoader( type, online, entries );
			loader.save();
		} else {
			final RankingLoader loader = new RankingLoader( type, online );
			loader.load();
			final RankingEntry[] entries = loader.rankingEntries;

			for ( byte i = 0; i < entries.length; ++i ) {
				entries[ i ] = new RankingEntry();
				if ( formatter != null )
					formatter.initLocalEntry( type, entries[ i ], i );
			}

			loader.save();
		}		
	}
	
	
	private static final void setLastInfo( int type, long score, boolean decrescent ) {
		lastType = ( byte ) type;
		lastScore = score;
		lastDecrescent = decrescent;
	}
	
	
	/**
	 * 
	 * @param type �ndice da tabela de ranking (ex.: 1 por pista).
	 * @param online
	 * @param profileId �ndice do perfil de usu�rio que obteve a pontua��o testada. Utilizar valores negativos faz somente 
	 * a verifica��o de recordes locais.
	 * @param score pontua��o a ser verificada.
	 * @param decrescent indica se a tabela de recorde � descrescente, ou seja, maiores pontua��es no topo da tabela.
	 * @return
	 */
	private static final byte getScoreIndex( int type, boolean online, int profileId, long score, boolean decrescent ) {
		try {
			if ( online ) {
				final RankingLoader loader = new RankingLoader( type, true );
				loader.load();
				final RankingEntry[] entries = loader.rankingEntries;
				
				for ( byte i = 0; i < MAX_PROFILES; ++i ) {
					if ( profileId == entries[ i ].getProfileId() ) {
						if ( ( decrescent && score > entries[ i ].getScore() ) || ( !decrescent && score < entries[ i ].getScore() )) {
							return i;
						}
					}
				}		
				
				// n�o achou o jogador na lista - ranking est� cheio. Varre a lista procurando um perfil n�o mais usado,
				// e retorna esse �ndice (garantidamente � a melhor pontua��o local do jogador, pois n�o havia nenhuma antes)
				for ( byte i = 0; i < MAX_PROFILES; ++i ) {
					if ( NanoOnline.getCustomerById( entries[ i ].getProfileId() ) == null ) {
						return i;
					}
				}
				
				//#if DEBUG == "true"
//# 					// n�o deveria chegar aqui!
//# 					throw new Exception( "algoritmo de busca de recordes globais dos perfis com erro!" );
				//#endif
			} else {
				final RankingLoader loader = new RankingLoader( type, false );
				loader.load();
				final RankingEntry[] entries = loader.rankingEntries;

				// verifica a pontua��o local
				for ( byte i = 0; i < entries.length; ++i ) {
					if ( ( decrescent && score > entries[ i ].getScore() ) || ( !decrescent && score < entries[ i ].getScore() ) ) {
						return i;
					}
				}
			}
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 				ex.printStackTrace();
			//#endif
		}
		
		return -1;
	}
	
	
	protected static final long getLastScore() {
		return lastScore;
	}
	
	
	protected static final byte getLastType() {
		return lastType;
	}
	
	
	protected static final boolean getLastDecrescent() {
		return lastDecrescent;
	}
	
	
	protected static final byte getTotalTypes() {
		return totalTypes;
	}
	
	
	protected static final int[] getTypesEntries() {
		return typesEntries;
	}


	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ID_BUTTON_UPDATE:
						// envia os recordes de cada perfil para o servidor
						try {
							final ByteArrayOutputStream b = new ByteArrayOutputStream();
							final DataOutputStream out = new DataOutputStream( b );

							// escreve os dados globais antes de adicionar as informa��es espec�ficas para registro do usu�rio
							NanoOnline.writeGlobalData( out );
							
							// envia somente as entradas que n�o foram submetidas
							// TODO alterar o estado de submiss�o das entradas ap�s confirma��o da grava��o dos recordes
							final RankingEntry[] entries = getEntriesToSubmit( type );
							
							out.writeByte( PARAM_N_ENTRIES );
							out.writeByte( entries.length );
							out.writeByte( PARAM_SUB_TYPE );
							// caso seja um ranking crescente, envia o valor negativo do tipo para informar isso ao servidor
							out.writeByte( getLastDecrescent() ? type : -type );
							
							for ( byte i = 0; i < entries.length; ++i ) {
//								out.writeByte( PARAM_PROFILE_ID );
								out.writeInt( entries[ i ].getProfileId() );
//								out.writeByte( PARAM_SCORE );
								out.writeLong( entries[ i ].getScore() );
							}
							
							out.flush();
							
							NanoConnection.post( NANO_ONLINE_ADDRESS + "/ranking_entries", b.toByteArray(), this, false );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
						}
					break;
					
					case ID_BUTTON_ERASE:
						// TODO mostrar confirma��o antes de efetivamante apagar os recordes
						try {
							eraseRecords( type, online );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
						}
						NanoOnline.setScreen( SCREEN_RECORDS );
					break;
					
					case ProgressBar.ID_SOFT_RIGHT:
					case ID_BUTTON_BACK:
						onBack();
					break;
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )		
	} // fim do m�todo eventPerformed( Event )
	
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	private final RankingEntry[] getEntriesToSubmit( int type ) {
		final RankingLoader loader = new RankingLoader( type, true );
		loader.load();		
		final Vector v = new Vector();
		for ( byte i = 0; i < MAX_PROFILES; ++i ) {
			if ( loader.rankingEntries[ i ].getProfileId() >= 0 && !loader.rankingEntries[ i ].isSubmitted() )
				v.addElement( loader.rankingEntries[ i ] );
		}
		final RankingEntry[] entries = new RankingEntry[ v.size() ];
		for ( byte i = 0 ; i < entries.length; ++i ) {
			entries[ i ] = ( RankingEntry ) v.elementAt( i );
		}
		
		return entries;
	}
	
	
	public final void processData( int id, byte[] data ) {
		if ( data != null ) {
			DataInputStream input = null;
			try {
				final Hashtable table = NanoOnline.readGlobalData( data );
				
				// o c�digo de retorno � obrigat�rio
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				byte errorEntryIndex = -1;
				byte errorText = -1;
				
				switch ( returnCode ) {
					case RC_OK:
						// grava as informa��es do usu�rio no RMS e mostra a tela de confirma��o
						final byte[] specificData = ( byte[] ) table.get( new Byte( ID_SPECIFIC_DATA ) );
						input = new DataInputStream( new ByteArrayInputStream( specificData ) );
						
						// primeiro l� um byte indicando quantas entradas de ranking foram recebidas
						final byte totalEntries = input.readByte();
						
						// marca os recordes como enviados
						final RankingLoader loader = new RankingLoader( getLastType(), true );
						loader.load();
						final RankingEntry[] entries = loader.rankingEntries;
						for ( byte i = 0; i < MAX_PROFILES; ++i ) {
							// todos os perfis do subtipo de ranking atual foram atualizados
							if ( entries[ i ].getProfileId() >= 0 )
								entries[ i ].setSubmitted( true );
						}
						
						for ( byte i = MAX_PROFILES, j = 0; i < entries.length; ++i, ++j ) {
							final RankingEntry e = entries[ i ];
							if ( j < totalEntries ) {
								// ordem de leitura: id do usu�rio (int), pontua��o (long), nick (UTF)
								e.setProfileId( input.readInt() );
								e.setScore( input.readLong() );
								e.setNickname( input.readUTF() );
							} else {
								entries[ i ].setProfileId( Customer.ID_NONE );
							}
						}
						
						// salva as altera��es no ranking
						loader.save();
					break;
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
		}
	}


	public void onInfo( int id, int infoIndex, Object extraData ) {
	}


	public void onError( int id, int errorIndex, Object extraData ) {
	}	

	
	private static final class RankingLoader implements Serializable {

		private RankingEntry[] rankingEntries;
		
		private final byte type;
		
		private final boolean online;

		
		private RankingLoader( int type, boolean online ) {
			this( type, online, null );
		}
		
		
		private RankingLoader( int type, boolean online, RankingEntry[] entries ) {
			this.rankingEntries = entries;
			this.online = online;
			
			this.type = ( byte ) ( online ? totalTypes + type : type );
		}
		
		
		private final void save() throws Exception {
			// os �ndices do RMS come�am em 1 em vez de zero...
			AppMIDlet.saveData( RANKING_DATABASE, type + 1, this );
		}
		
		
		private final void load() {
			// os �ndices do RMS come�am em 1 em vez de zero...
			try {
				AppMIDlet.loadData( RANKING_DATABASE, type + 1, this );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
		}
		

		public void write( DataOutputStream output ) throws Exception {
			//#if DEBUG == "true"
//# 				System.out.println( "Gravando " + rankingEntries.length + " entradas de ranking" );
			//#endif
			
			if ( online ) {
//				output.writeLong(  ); TODO data de atualiza��o do ranking
				
			}
			
			for ( byte i = 0; i < rankingEntries.length; ++i ) {
				rankingEntries[ i ].write( output );
			}
		}


		public void read( DataInputStream input ) throws Exception {
			if ( online ) {
				rankingEntries = new RankingEntry[ MAX_RANKING_ENTRIES + MAX_PROFILES ];
				
			} else {
				rankingEntries = new RankingEntry[ MAX_RANKING_ENTRIES ];
				
			}
			
			//#if DEBUG == "true"
//# 				System.out.println( "Lendo " + rankingEntries.length + " entradas de ranking" );
			//#endif			
			
			for ( byte i = 0; i < rankingEntries.length; ++i ) {
				rankingEntries[ i ] = new RankingEntry();
				rankingEntries[ i ].read( input );
			}
		}
	} // fim da classe interna RankingLoader

}
