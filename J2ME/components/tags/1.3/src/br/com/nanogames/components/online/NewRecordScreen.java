/**
 * NewRecordScreen.java
 * 
 * Created on 5/Fev/2009, 19:36:50
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;

/**
 *
 * @author Peter
 */
public class NewRecordScreen extends NanoOnlineContainer {

	private static final byte TOTAL_SLOTS = 10;
	
	private static final byte ENTRY_BUTTON_USE_PROFILE		= 0;
	private static final byte ENTRY_BUTTON_CHOOSE_PROFILE	= 1;
	private static final byte ENTRY_BUTTON_SAVE_LOCAL		= 2;
	private static final byte ENTRY_BUTTON_CREATE_PROFILE	= 3;
	private static final byte ENTRY_BUTTON_LOAD_EXISTING	= 4;
	
	
	public NewRecordScreen() throws Exception {
		super( TOTAL_SLOTS );
		
		final ImageFont font = NanoOnline.getFont( FONT_DEFAULT );
		
		final Customer[] customers = NanoOnline.getCustomers();
		
		final FormText text = new FormText( font );
		if ( customers == null ) {
			// n�o h� jogadores salvos no aparelho
			text.setText( NanoOnline.getText( TEXT_NO_PROFILES_FOUND ) );
			insertDrawable( text );
			
			final Button buttonCreate = new Button( font, NanoOnline.getText( TEXT_REGISTER ) );
			buttonCreate.setId( ENTRY_BUTTON_CREATE_PROFILE );
			buttonCreate.setBorder( NanoOnline.getBorder() );
			buttonCreate.addActionListener( this );
			insertDrawable( buttonCreate );
			
			final Button buttonLoad = new Button( font, NanoOnline.getText( TEXT_USE_EXISTING_PROFILE ) );
			buttonLoad.setId( ENTRY_BUTTON_LOAD_EXISTING );
			buttonLoad.setBorder( NanoOnline.getBorder() );
			buttonLoad.addActionListener( this );
			insertDrawable( buttonLoad );			
		} else {
			// h� perfis salvos no aparelho - mostra o perfil atual e op��es para escolher/registrar outro
			text.setText( NanoOnline.getText( TEXT_CURRENT_PROFILE ) + ": " + NanoOnline.getCurrentCustomer().getNickname() );
			insertDrawable( text );
			
			final Button buttonUse = new Button( font, NanoOnline.getText( TEXT_USE_PROFILE ) );
			buttonUse.setId( ENTRY_BUTTON_USE_PROFILE );
			buttonUse.setBorder( NanoOnline.getBorder() );
			buttonUse.addActionListener( this );
			insertDrawable( buttonUse );
			
			final Button buttonChoose = new Button( font, NanoOnline.getText( TEXT_USE_EXISTING_PROFILE ) );
			buttonChoose.setId( ENTRY_BUTTON_CHOOSE_PROFILE );
			buttonChoose.setBorder( NanoOnline.getBorder() );
			buttonChoose.addActionListener( this );
			insertDrawable( buttonChoose );
		}
		
		text.setFocusable( false );

		final Button buttonSaveLocal = new Button( font, NanoOnline.getText( TEXT_SAVE_LOCAL_RECORD ) );
		buttonSaveLocal.setId( ENTRY_BUTTON_SAVE_LOCAL );
		buttonSaveLocal.addActionListener( this );
		buttonSaveLocal.setBorder( NanoOnline.getBorder() );
		insertDrawable( buttonSaveLocal );		
	}
	

	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_USE_PROFILE:
						try {
							RankingScreen.setHighScore( RankingScreen.getLastType(), NanoOnline.getCurrentCustomer().getId(), RankingScreen.getLastScore(), RankingScreen.getLastDecrescent() );
							NanoOnline.setScreen( SCREEN_RECORDS, backScreenIndex );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
							NanoOnline.setScreen( backScreenIndex );
						}
					break;
					
					case ENTRY_BUTTON_CHOOSE_PROFILE:
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT, getId() );
					break;
					
					case ENTRY_BUTTON_SAVE_LOCAL:
						NanoOnline.setScreen( SCREEN_ENTER_LOCAL_NAME, getId() );
					break;
					
					case ENTRY_BUTTON_CREATE_PROFILE:
						NanoOnline.setScreen( SCREEN_REGISTER_PROFILE, getId() );
					break;
					
					case ENTRY_BUTTON_LOAD_EXISTING:
						NanoOnline.setScreen( SCREEN_LOAD_PROFILE, getId() );
					break;
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						keyPressed( ScreenManager.FIRE );
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )			
	}

}
