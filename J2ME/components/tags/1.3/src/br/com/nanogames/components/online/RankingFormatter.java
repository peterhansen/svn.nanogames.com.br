/**
 * RankingFormatter.java
 * 
 * Created on 1/Fev/2009, 11:59:33
 *
 */

package br.com.nanogames.components.online;

/**
 * Essa interface � utilizada para inicializar tabelas de ranking local, e tamb�m para formatar a exibi��o da pontua��o
 * na tabela de ranking. � importante que os valores iniciais da tabela respeitem a ordem (crescente ou decrescente) do
 * ranking, para evitar tabelas inconsistentes.
 * @author Peter
 */
public interface RankingFormatter {

	/**
	 * Formata uma pontua��o.
	 * @param type tipo de ranking.
	 * @param score pontua��o a ser formatada.
	 * @return string representando a pontua��o de acordo com a formata��o especificada.
	 */
	public String format( int type, long score );
	
	
	/**
	 * Inicializa uma entrada de ranking local.
	 * @param type tipo de ranking.
	 * @param entry entrada a ser formatada.
	 * @param index �ndice da entrada no ranking, sendo o �ndice zero a melhor pontua��o.
	 */
	public void initLocalEntry( int type, RankingEntry entry, int index );
}
