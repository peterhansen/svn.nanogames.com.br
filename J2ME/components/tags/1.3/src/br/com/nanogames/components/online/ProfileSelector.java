/**
 * ProfileSelector.java
 * 
 * Created on 28/Jan/2009, 21:49:27
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.events.Event;

/**
 *
 * @author Peter
 */
public final class ProfileSelector extends NanoOnlineContainer {

	/***/
	private static final byte ENTRY_FIRST_PROFILE		= 0;
	/***/
	private static final byte ENTRY_BUTTON_NEW			= ENTRY_FIRST_PROFILE + MAX_PROFILES + 1;
	/***/
	private static final byte ENTRY_BUTTON_USE_EXISTING	= ENTRY_BUTTON_NEW + 1;
	/***/
	private static final byte ENTRY_BUTTON_BACK			= ENTRY_BUTTON_USE_EXISTING + 1;
	/***/
	private static final byte TOTAL_ITEMS = ENTRY_BUTTON_BACK + 1;
	
	
	public ProfileSelector( int backIndex ) throws Exception {
		super( TOTAL_ITEMS );
		
		setBackIndex( backIndex >= 0 ? backIndex : SCREEN_MAIN_MENU );
		
		final ImageFont font = NanoOnline.getFont( FONT_DEFAULT );
		
		// insere os perfis j� cadastrados no aparelho
		final Customer[] customers = NanoOnline.getCustomers();
		if ( customers != null ) {
			for ( byte i = 0; i < customers.length; ++i ) {
				final Button b = new Button( font, customers[ i ].getNickname() );
				b.setId( i );
				b.setBorder( NanoOnline.getBorder() );
				b.addActionListener( this );
				
				insertDrawable( b );
			}
		}
		
		// insere os bot�es
		
		final Button buttonNew = new Button( font, NanoOnline.getText( TEXT_REGISTER ) );
		buttonNew.setId( ENTRY_BUTTON_NEW );
		buttonNew.setBorder( NanoOnline.getBorder() );
		buttonNew.addActionListener( this );
		
		insertDrawable( buttonNew );
		
		final Button buttonUse = new Button( font, NanoOnline.getText( TEXT_USE_EXISTING_PROFILE ) );
		buttonUse.setId( ENTRY_BUTTON_USE_EXISTING );
		buttonUse.addActionListener( this );
		buttonUse.setBorder( NanoOnline.getBorder() );
		insertDrawable( buttonUse );
		
		final Button buttonBack = new Button( font, NanoOnline.getText( TEXT_BACK ) );
		buttonBack.setId( ENTRY_BUTTON_BACK );
		buttonBack.addActionListener( this );
		buttonBack.setBorder( NanoOnline.getBorder() );
		insertDrawable( buttonBack );
	}
	

	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_BUTTON_BACK:
						onBack();
					break;
					
					case ENTRY_BUTTON_USE_EXISTING:
						NanoOnline.setScreen( SCREEN_LOAD_PROFILE, getId() );
					break;
					
					case ENTRY_BUTTON_NEW:
						NanoOnline.setScreen( SCREEN_REGISTER_PROFILE, getId() );
					break;
					
					default:
						// selecionou um dos perfis
						ProfileScreen.setProfileIndex( sourceId - ENTRY_FIRST_PROFILE );
						NanoOnline.setScreen( SCREEN_PROFILE_EDIT );
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do m�todo eventPerformed( Event )

}
