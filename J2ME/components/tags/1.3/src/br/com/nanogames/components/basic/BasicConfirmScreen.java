/*
 * YesNoScreen.java
 *
 * Created on May 7, 2007, 12:36 PM
 *
 */
package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author peter
 */
public final class BasicConfirmScreen extends Menu {
	
	/** �ndice da op��o "sim" da tela de confirma��o. */
	public static final byte INDEX_YES = 1;

	/** �ndice da op��o "n�o" da tela de confirma��o. */
	public static final byte INDEX_NO = 2;

	/** �ndice da op��o "cancelar" da tela de confirma��o. */
	public static final byte INDEX_CANCEL = 3;	
	
	/** Tipo de tela b�sica de confirma��o com 2 op��es dispon�veis: sim/n�o. */
	public static final byte TYPE_YES_NO		= 0;
	
	/** Tipo de tela b�sica de confirma��o com 3 op��es dispon�veis: sim/n�o/cancelar. */
	public static final byte TYPE_YES_NO_CANCEL	= 1;
	
	/** Alinhamento das op��es: lado a lado horizontalmente. */
	public static final byte ALIGNMENT_HORIZONTAL = 0;
	
	/** Alinhamento das op��es: verticalmente. */
	public static final byte ALIGNMENT_VERTICAL = 1;
	
	private byte alignment;
	
	private final Drawable labelTitle;

	private final Drawable labelYes;

	private final Drawable labelNo;
	
	private final Drawable labelCancel;

	private static final byte TOTAL_ITEMS = 5;
	
	/** Indica se a tela de confirma��o deve utilizar a tela inteira. */
	private boolean useFullScreen;

	private short titleSpacing;

	private short entriesSpacing;

	
	/**
	 * Cria uma nova tela de confirma��o. Equivalente ao construtor <code>BasicConfirmScreen( listener, id, font, cursor, 
	 * idTitle, idYes, idNo, <b>true</b> )</code>.
	 * 
	 * @param listener listener da tela de confirma��o.
	 * @param id identificador da tela de confirma��o, a ser passado para o listener.
	 * @param font fonte a ser utilizada.
	 * @param cursor cursor do menu.
	 * @param idTitle �ndice do texto utilizado como t�tulo.
	 * @param idYes �ndice do texto correspondente � op��o <i>sim</i>.
	 * @param idNo �ndice do texto correspondente � op��o <i>n�o</i>.
	 * 
	 * @throws java.lang.Exception
	 * 
	 * @see #BasicConfirmScreen(MenuListener, int, ImageFont, Drawable, int, int, int, boolean)
	 */
	public BasicConfirmScreen( MenuListener listener, int id, ImageFont font, Drawable cursor, int idTitle, int idYes, int idNo ) throws Exception {
		this( listener, id, font, cursor, idTitle, idYes, idNo, true );
	}
	
	
	public BasicConfirmScreen( MenuListener listener, int id, ImageFont font, Drawable cursor, int idTitle, int idYes, int idNo, boolean useFullScreen ) throws Exception {
		this( listener, id, font, cursor, idTitle, idYes, idNo, -1, useFullScreen );
	}


	/**
	 * Cria uma nova tela de confirma��o.
	 *
	 * @param listener listener da tela de confirma��o.
	 * @param id identificador da tela de confirma��o, a ser passado para o listener.
	 * @param font fonte a ser utilizada.
	 * @param cursor cursor do menu.
	 * @param idTitle �ndice do texto utilizado como t�tulo.
	 * @param idYes �ndice do texto correspondente � op��o <i>sim</i>.
	 * @param idNo �ndice do texto correspondente � op��o <i>n�o</i>.
	 * @param useFullScreen indica se a tela de confirma��o deve ocupar sempre as dimens�es totais da tela do aparelho,
	 * e fazer redimensionamento autom�tico sempre que necess�rio.
	 *
	 * @throws java.lang.Exception
	 *
	 * @see #BasicConfirmScreen(MenuListener, int, ImageFont, Drawable, int, int, int)
	 */
	public BasicConfirmScreen( MenuListener listener, int id, ImageFont font, Drawable cursor, int idTitle, int idYes, int idNo, int idCancel, boolean useFullScreen ) throws Exception {
		this( listener, id, font, cursor, 
			  new RichLabel( font, AppMIDlet.getText( idTitle ), ScreenManager.SCREEN_WIDTH * 9 / 10, null ),
			  new Label( font, idYes ), new Label( font, idNo ), idCancel >= 0 ? new Label( font, AppMIDlet.getText( idCancel ) ) : null, useFullScreen );
	}
	
		
	public BasicConfirmScreen( MenuListener listener, int id, ImageFont font, Drawable cursor, Drawable title, Drawable yes, Drawable no, Drawable cancel, boolean useFullScreen ) throws Exception {
		super( listener, id, TOTAL_ITEMS );

		labelTitle = title;
		if ( title instanceof RichLabel )
			( ( RichLabel ) title ).setSize( title.getWidth(), ( ( RichLabel ) title ).getTextTotalHeight() );
		insertDrawable( title );

		titleSpacing = ( short ) font.getHeight();

		labelYes = yes;
		insertDrawable( labelYes );

		labelNo = no;
		insertDrawable( labelNo );

		labelCancel = cancel;
		if ( cancel != null )
			insertDrawable( labelCancel );

		if ( cursor != null )
			setCursor( cursor, CURSOR_DRAW_BEFORE_MENU, ANCHOR_VCENTER | ANCHOR_LEFT );

		this.useFullScreen = useFullScreen;
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		setCurrentIndex( INDEX_NO );
	}

	
	public final void setCurrentIndex( int index ) {
		if( index == 0 ) {
			index = 1;
		}

		super.setCurrentIndex( index );
	}

	
	public final void keyPressed( int key )	{
		switch( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				setCurrentIndex( labelCancel == null ? INDEX_NO : INDEX_CANCEL );

			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				super.keyPressed( ScreenManager.KEY_NUM8 );
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				super.keyPressed( ScreenManager.KEY_NUM2 );
			break;

			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}
	
	
	/**
	 * Define o modo tela cheia. Esse modo � utilizado para que, no caso dessa tela ser a primeira a ser exibida no
	 * aplicativo, atualize seu tamanho automaticamente caso as dimens�es da tela se alterem.
	 * @param fullScreen
	 */
	public final void setFullScreenMode( boolean fullScreen ) {
		useFullScreen = fullScreen;
	}

	
	public final void update( int delta ) {
		if ( useFullScreen && ( size.x != ScreenManager.SCREEN_WIDTH || size.y != ScreenManager.SCREEN_HEIGHT ) )
			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		super.update( delta );
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		defineReferencePixel( size.x >> 1, size.y >> 1 );

		// deixa uma margem de cada lado da tela, para distribuir melhor o texto do t�tulo
		labelTitle.setSize( labelTitle.getWidth(), labelTitle.getHeight() );

		labelTitle.defineReferencePixel( labelTitle.getWidth() >> 1, 0 );
		
		int y = labelTitle.getHeight() + titleSpacing;
		
		switch ( alignment ) {
			case ALIGNMENT_HORIZONTAL:
//				if ( y + labelTitle.getFont().getHeight() + ( labelCancel == null ? 0 : labelCancel.getHeight() ) > size.y ) TODO alinhamento horizontal
//					y = size.y - ( ( labelTitle.getFont().getHeight() * ( labelCancel == null ? 3 : 5 ) ) >> 1 );
//
//				labelYes.defineReferencePixel( labelYes.getWidth(), 0 );
//
//				labelYes.setRefPixelPosition( ( size.x << 2 ) / 10, y );
//				labelNo.setPosition( size.x * 6 / 10, y );
			break;
			
			case ALIGNMENT_VERTICAL:
//				final int labelsHeight = labelYes.getHeight() + labelNo.getHeight() + ( labelCancel == null ? 0 : labelCancel.getHeight() ) + entriesSpacing;
//				if ( y + labelsHeight > size.y )
//					y = size.y - labelsHeight;

				labelYes.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
				labelYes.setRefPixelPosition( size.x >> 1, y );
				
				y += labelYes.getHeight() + entriesSpacing;
				
				labelNo.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
				labelNo.setRefPixelPosition( size.x >> 1, y );
				
				y += labelNo.getHeight() + entriesSpacing;
				
			break;
		} // fim switch ( alignment )
		
		if ( labelCancel != null ) {
			labelCancel.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
			labelCancel.setRefPixelPosition( size.x >> 1, y );
			y += labelCancel.getHeight();
		}

		final int diffY = ( size.y - y ) >> 1;
		labelTitle.setRefPixelPosition( size.x >> 1, diffY );
		labelYes.move( 0, diffY );
		labelNo.move( 0, diffY );

		if ( labelCancel != null )
			labelCancel.move( 0, diffY );

		updateCursorPosition();		
	}
	
	
	public final void setEntriesAlignment( byte alignment ) {
		this.alignment = alignment;
		
		setSize( size );
	}


	public final void setSpacing( int titleSpacing, int entriesSpacing ) {
		this.titleSpacing = ( short ) titleSpacing;
		this.entriesSpacing = ( short ) entriesSpacing;

		setSize( size );
	}
	
}
