/**
 * Form.java
 * 
 * Created on 5/Nov/2008, 14:31:47
 *
 */
package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.util.Point;
import java.util.Vector;

/**
 *
 * *<pre>
 *
 *       **************************
 *       *        TitleBar        *
 *       **************************
 *       *                        *
 *       *                        *
 *       *      ContentPane       *
 *       *                        *
 *       *                        *
 *       **************************
 *       *         MenuBar        *
 *       **************************
 * </pre> 
 * 
 * @author Peter
 */
public class Form extends Container implements ScreenListener {
	
	private static final byte TOTAL_ITEMS = 3;

	/** Refer�ncia para o componente que est� com o foco de arrasto do ponteiro (caso haja um). */
	protected Component dragged;
	
	/** Barra de t�tulo (sempre na parte superior da tela). */
	protected Component titleBar;

	/** Barra de status (sempre na parte inferior da tela). */
	protected Component statusBar;
	
	/** Conte�do central do form. */
	protected Container contentPane;
	
	/** Componente com foco atualmente. */
	protected Component focused;
	
    /**
     * Allows us to cache the next focus component ordered from top to down, this
     * vector is guaranteed to have all focusable children in it.
     */
    protected Component[] focusDownSequence;

	protected short focusDownIndex;
	protected short focusRightIndex;
	
    /**
     * Allows us to cache the next focus component ordered from left to right, this
     * vector is guaranteed to have all focusable children in it.
     */
    protected Component[] focusRightSequence;

	/***/
	protected boolean focusVectorInitialized;

	
	/**
	 * 
	 * @throws java.lang.Exception
	 */
	public Form() throws Exception {
		this( null );
	}
	
	
	/**
	 * 
	 * @param contentPane
	 * @throws java.lang.Exception
	 */
	public Form( Container contentPane ) throws Exception {
		super( TOTAL_ITEMS, new BorderLayout() );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		setContentPane( contentPane );
	}
	
	
	/**
	 * 
	 */
	public void destroy() {
		super.destroy();
		
		clearFocusVectors();
		setDraggedComponent( null );
		setFocused( null );
		setContentPane( null );
	}
	
	
    /**
     * Resets the cache focus vectors, this is a good idea when we remove
     * or add an element to the layout.
     */
    protected void clearFocusVectors() {
		focusDownSequence = null;
		focusRightSequence = null;
		focusVectorInitialized = false;
		focusDownIndex = -1;
		focusRightIndex = -1;
    }	
	
	
	/**
	 * Chamada equivalente a <code>setContentPane( contentPane, true )</code>.
	 * @param contentPane
	 * @return
	 */
	public final Container setContentPane( Container contentPane ) {
		return setContentPane( contentPane, true );
	}
	
	
	/**
	 * 
	 * @param contentPane
	 */
	public Container setContentPane( Container contentPane, boolean destroyPrevious ) {
		final Container previous = this.contentPane;
		if ( previous != null ) {
			removeDrawable( previous, destroyPrevious );
			layout.removeLayoutComponent( previous );
		}
		
		if ( contentPane != null ) {
			this.contentPane = contentPane;
			insertDrawable( contentPane, BorderLayout.CENTER );
		}
		
		setDraggedComponent( null );
		setFocused( null );
		
		refreshLayout();
		updateFocusVectors();
//		initFocus(); TODO
		
		return previous;
	}	
	
	
	/**
	 * 
	 * @return
	 */
	public Container getContentPane() {
		return contentPane;
	}
	
	
	/**
	 * 
	 * @param contentPane
	 */
	public void setTitleBar( Component titleBar ) {
		if ( this.titleBar != null ) {
			removeDrawable( this.titleBar );
			layout.removeLayoutComponent( this.titleBar );
		}
		
		this.titleBar = titleBar;
		insertDrawable( titleBar, BorderLayout.NORTH );
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Component getTitleBar() {
		return titleBar;
	}		
	
	
	/**
	 * 
	 * @param contentPane
	 */
	public void setStatusBar( Component statusBar ) {
		if ( this.statusBar != null ) {
			removeDrawable( this.statusBar );
			layout.removeLayoutComponent( this.statusBar );
		}
		
		this.statusBar = statusBar;
		insertDrawable( statusBar, BorderLayout.SOUTH );
	}


	/**
	 * 
	 * @return
	 */
	public Component getStatusBar() {
		return statusBar;
	}


	/**
     * @inheritDoc
     */
    public final Form getComponentForm() {
        return this;
    }	
	
	
	/**
	 * 
	 * @return
	 */
	public String getUIID() {
		return "form";
	}
	
	
    /**
     * Sets the current dragged Component
     */
    protected void setDraggedComponent( Component c ) {
        dragged = c;
    }	
	
	
    /**
     * Sets the focused component and fires the appropriate events to make it so
     * 
     * @param focused the newly focused component or null for no focus
     */
    public void setFocused( Component focused ) {
		if ( this.focused == focused && focused != null ) {
			return;
		}
		final Component oldFocus = this.focused;
		this.focused = focused;

		if ( oldFocus != null ) {
			oldFocus.setFocus( false );
		}
		// a listener might trigger a focus change event essentially
		// invalidating focus so we shouldn't break that 
		if ( focused != null && this.focused == focused ) {
			focused.setFocus( true );
		}

		if ( focusDownSequence != null ) {
			for ( short i = 0; i < focusDownSequence.length; ++i ) {
				if ( focusDownSequence[ i ] == focused ) {
					focusDownIndex = i;
					break;
				}
			}
		} else {
			focusDownIndex = -1;
		}

		if ( focusRightSequence != null ) {
			for ( short i = 0; i < focusRightSequence.length; ++i ) {
				if ( focusRightSequence[ i ] == focused ) {
					focusRightIndex = i;
					break;
				}
			}
		} else {
			focusRightIndex = -1;
		}
		
		scrollToComponent( focused );
	}


    /**
     * Makes sure the component is visible in the scroll if this container 
     * is scrollable
     * 
     * @param c the component to be visible
 	 */
	public void scrollToComponent( Component c ) {
		if ( c != null ) {
			Container p = c.getParent();
			while ( p != null ) {
				if ( p.isScrollable() ) {
					p.scrollToComponent( c );
					return;
				}
				p = p.getParent();
			}
		}
	}
	
	
    /**
     * Request focus for a form child component
     * 
     * @param cmp the form child component
     */
    public void requestFocus( Component cmp ) {
        if ( cmp.isFocusable() && contains( cmp ) ) {
            setFocused( cmp );
        }
    }	
	

    /**
     * Returns the current focus component for this form
     * 
     * @return the current focus component for this form
     */
    public Component getFocused() {
        return focused;
    }	
	
	
	protected void initFocus() {
		if ( !focusVectorInitialized )
			updateFocusVectors();		
		
		// FIXME ao trocar de tela, o novo ContentPane � definido, e um novo componente ganha o foco. Por�m, esse
		// novo componente em foco recebe o evento keyReleased, mesmo n�o tendo sido ele que originou o keyPressed
		if ( contentPane != null && focusDownSequence != null )
			setFocused( focusDownSequence[ 0 ] );
	}
	
	
	public void updateFocus( int key ) {
		Component currentFocus = getFocused();
		if ( !focusVectorInitialized )
			updateFocusVectors();
		
		switch ( key ) {
			case ScreenManager.DOWN: {
				if ( currentFocus.getNextFocusDown() != null ) {
					currentFocus = currentFocus.getNextFocusDown();
				} else {
					if ( focusDownSequence != null ) {
						int i = focusDownIndex;
						
						do {
							++i;
							if ( i == focusDownSequence.length )
								i = 0;
							
							final Component nextFocus = focusDownSequence[ i ];
							if ( nextFocus.isFocusable() ) {
								currentFocus = nextFocus;
								break;
							}
						} while ( i > focusDownIndex );
					}
				}
				break;
			}
			
			case ScreenManager.UP: {
				if ( currentFocus.getNextFocusUp() != null ) {
					currentFocus = currentFocus.getNextFocusUp();
				} else {
					if ( focusDownSequence != null ) {
						int i = focusDownIndex;
						
						do {
							--i;
							if ( i < 0 )
								i = focusDownSequence.length - 1;
							
							final Component nextFocus = focusDownSequence[ i ];
							if ( nextFocus.isFocusable() ) {
								currentFocus = nextFocus;
								break;
							}
						} while ( i < focusDownIndex );
					}
				}
				break;
			}
			
			case ScreenManager.RIGHT: {
				if ( currentFocus.getNextFocusRight() != null ) {
					currentFocus = currentFocus.getNextFocusRight();
				} else {
					if ( focusRightSequence != null ) {
						int i = focusRightIndex + 1;
						if ( i == focusRightSequence.length )
							i = 0;
						
						currentFocus = focusRightSequence[ i ];
					}
				}
				break;
			}
			
			case ScreenManager.LEFT: {
				if ( currentFocus.getNextFocusLeft() != null ) {
					currentFocus = currentFocus.getNextFocusLeft();
				} else {
					if ( focusRightSequence != null ) {
						int i = focusRightIndex - 1;
						if ( i < 0 )
							i = focusRightSequence.length - 1;

						currentFocus = focusRightSequence[ i ];
					}
				}
				break;
			}
			
			default:
				return;
		}
		setFocused( currentFocus );
	}
	

	public void updateFocusVectors() {
//		setFocused( null ); TODO necess�rio?
		clearFocusVectors();

		if ( contentPane != null ) {
			focusDownSequence = findAllFocusable( contentPane, false );
			focusRightSequence = findAllFocusable( contentPane, true );
		
			focusVectorInitialized = true;
		}
	}


	private final Component[] findAllFocusable( Container c, boolean toTheRight ) {
		final Vector v = new Vector();

		findAllFocusable( c, v, toTheRight );

		final Component[] ret = new Component[ v.size() ];
		for ( short i = 0; i < ret.length; ++i )
			ret[ i ] = ( Component ) v.elementAt( i );

		return ret;
	}


	/**
	 * Finds all focusable components in the hierarchy 
	 */
	private final void findAllFocusable( Container c, Vector v, boolean toTheRight ) {
		if ( c.isFocusable() ) { // TODO teste
			v.addElement( c );
			//#if DEBUG == "true"
//# 			System.out.println( "ADICIONOU! " + c + ", " + c.getUIID() + " -> " + v.size() );
			//#endif
		}
		
		final int count = c.getComponentCount();

		for ( short i = 0; i < count; ++i ) {
			Component current = c.getComponentAt( i );
			if ( current instanceof Container ) {
				findAllFocusable( ( Container ) current, v, toTheRight );
			} else if ( current.isFocusable() ) {
				// TODO somente n�o-containers podem ter foco?
				v.addElement( current );
				//#if DEBUG == "true"
//# 				System.out.println( "ADICIONOU " + current + ", " + current.getUIID() + " -> " + v.size() );
				//#endif
//				addSortedComponent( v, current, toTheRight );
			}
		}
	}


	/**
	 * @inheritDoc
	 */
	public void keyPressed( int key ) {
		if ( focused != null ) {
			// o evento � repassado antes de se testar focused.handlesInput() para que uma poss�vel altera��o
			// em setHandlesInput() j� seja refletida aqui. Ex.: pressionar para baixo num campo de texto
			final Component f = focused;
			f.keyPressed( key );
			if ( !f.handlesInput() ) {
				// atualiza o foco
				updateFocus( key );
			}
		} else {
			initFocus();
		}
	}

	
	/**
	 * @inheritDoc
	 */
	public void keyReleased( int key ) {
		// TODO � necess�rio tratar casos onde o form pai do componente n�o � o form que recebe eventos?
		if ( focused != null && focused.getComponentForm() == this ) {
			focused.keyReleased( key );
		}
	}
	
	
	/**
	 * @inheritDoc
	 */
	public void onPointerPressed( int x, int y ) {
		final Point diff = new Point();
		final Component c = getComponentAt( x, y, diff );

		if ( c != null ) {
			x -= diff.x;
			y -= diff.y;			

			if ( c.isFocusable() )
				setFocused( c );

			c.onPointerPressed( x, y );
			setDraggedComponent( c );
		}
	}	


	/**
	 * @inheritDoc
	 */
	public void onPointerDragged( int x, int y ) {
		if ( dragged == null ) {
			final Point diff = new Point();
			final Component c = getComponentAt( x, y, diff );
			
			if ( c != null ) {
				x -= diff.x;
				y -= diff.y;			

				if ( c.isFocusable() )
					setFocused( c );
				
				c.onPointerDragged( x, y );
			}
		} else {
			final Point diff = new Point();
			dragged.getAbsolutePos( diff );
			// gambiarra: como a posi��o absoluta soma a posi��o do drawable e ess
			diff.subEquals( dragged.getPosition() );
			
			dragged.onPointerDragged( x - diff.x, y - diff.y );
		}
	}

	
	/**
	 * @inheritDoc
	 */
	public void onPointerReleased( int x, int y ) {
		if ( dragged == null ) {
			final Point diff = new Point();
			final Component c = getComponentAt( x, y, diff );
			
			if ( c != null ) {
				x -= diff.x;
				y -= diff.y;			

				if ( c.isFocusable() )
					setFocused( c );
				
				c.onPointerReleased( x, y );
				setDraggedComponent( c );
			}			
		} else {
			final Point diff = new Point();
			dragged.getAbsolutePos( diff );
			diff.subEquals( dragged.getPosition() );
			
			dragged.onPointerReleased( x - diff.x, y - diff.y );
			setDraggedComponent( null );
		}
	}

	
	public void hideNotify() {
	}

	
	public void showNotify() {
	}

	
	public void sizeChanged( int width, int height ) {
		setAutoCalcPreferredSize( true );

		setSize( width, height );
	}


}
