/*
 * Gauge.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.KeyListener;
import javax.microedition.lcdui.Graphics;

public class Gauge extends Drawable implements Updatable, KeyListener {
 
	protected int value;
	 
	protected final int maxValue;
	
	protected final boolean interactive;
	
	protected final Drawable fill;
	 
	public Gauge( Drawable fill, boolean interactive, int maxValue, int initialValue ) {
		this.maxValue = maxValue;
		this.interactive = interactive;
		this.fill = fill;
	}
	 
	
	public boolean isInteractive() {
		return false;
	}
	 
	
	public int getValue() {
		return value;
	}
	 
	
	public int getMaxValue() {
		return maxValue;
	}
	 
	
	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
	}
	
	 
	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
	}
	
	 
	/**
	 *@see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
	}

	
	protected void paint( Graphics g ) {
	}
	 
}
 
