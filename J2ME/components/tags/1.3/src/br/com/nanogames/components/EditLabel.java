/*
 * EditLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.EditLabelListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class EditLabel extends Label implements Updatable, PointerListener {
	
	/** Hashtable que associa uma tecla a um array de caracteres. */
	protected final Hashtable characters = new Hashtable();
	
	protected char[] currentChars;
	
	/** C�digo da �ltima tecla pressionada */
	protected int lastKeyPressed;
	
	/** C�digo da tecla sendo mantida pressionada (caso haja uma). */
	protected int keyHeld;
	
	/** Indica se h� alguma altera��o pendente. */
	protected boolean pendingCommit;
	
	/** Tempo em milisegundos que uma tecla deve ser mantida pressionada para que seja utilizada a a��o de tecla segurada. */
	protected static final short TIME_KEY_PRESS = 400;
	
	/** Tempo em milisegundos ap�s uma tecla ser pressionada para que o caracter atual seja inserido no texto. */
	protected static final short TIME_KEY_WAIT_BEFORE_INSERT = 800;
	
	/** Tempo em milisegundos que a tecla clear deve ser mantida pressionada para apagar todo o texto anterior � posi��o do cursor. */
	protected static final short TIME_KEY_CLEAR_ALL = 1000; // TODO apagar aos poucos?
	
	/** Intervalo em milisegundos entre 2 repeti��es de tecla esquerda/direita sendo mantidas pressionadas. */
	protected static final short TIME_NAVIGATE_KEY_REPEAT = 240;
	
	/** Tempo decorrido em milisegundos desde a �ltima tecla pressionada (ela pode ter sido solta ou n�o durante esse per�odo). */
	protected int timeSinceLastKeyPress;
	
	/** �ndice do caracter atual do array utilizado. */
	protected byte currentCharIndex;
	
	/** Sempre considera a tecla # (jogo-da-velha) como tecla de troca de entrada de texto */
	public static final char KEY_INPUT_CHANGE = ScreenManager.KEY_POUND;
	
	// modos de entrada de texto
	/** Modo de entrada de texto: todos os caracteres. */
	public static final byte INPUT_MODE_ANY			= 0;
	
	/** Modo de entrada de texto: somente n�meros. */
	public static final byte INPUT_MODE_NUMBERS		= 1;
	
	/** Modo de entrada de texto: e-mail. */
	public static final byte INPUT_MODE_EMAIL		= 2;
	
	/** Modo de entrada de texto: password. */
	public static final byte INPUT_MODE_PASSWORD	= 3;
	
	/** Modo de entrada de texto: URL. */
	public static final byte INPUT_MODE_URL			= 4;

	/** Modo de entrada atual. */
	protected byte inputMode = INPUT_MODE_ANY;
	
	/** Tipo de caixa de texto: caracteres min�sculos. */
	public static final byte CASE_TYPE_LOWER = 0;
	
	/** Tipo de caixa de texto: caracteres mai�sculos. */
	public static final byte CASE_TYPE_UPPER = 1;
	
	/** Tipo de caixa de texto: caracteres iniciais de cada palavra mai�sculos. */
	public static final byte CASE_TYPE_CAPITALIZE_WORDS = 2;
	
	// TODO tipo de caixa de texto: somente primeiro caracter de cada frase � mai�sculo.
	
	/** Quantidade total de caixas de texto diferentes. */
	protected static final byte CASE_TYPES_TOTAL = CASE_TYPE_CAPITALIZE_WORDS + 1;
	
	/** Tipo de caixa de texto atual. Valores v�lidos: 
	 * <ul>
	 * <li>CASE_TYPE_LOWER</li>
	 * <li>CASE_TYPE_UPPER</li>
	 * <li>CASE_TYPE_CAPITALIZE_WORDS</li>
	 * </ul>
	 */
	protected byte caseType;
	 
	/** Posi��o do cursor. */
	protected int caretPosition;
	
	/** Indica se a navega��o do cursor pelo texto � circular. */
	protected boolean circular;
	
	/** Drawable utilizado como cursor. */
	protected Drawable caret;
	
	/** Tempo padr�o em milisegundos que o cursor permanece vis�vel. */
	public static final int TIME_DEFAULT_CURSOR_VISIBLE = 500;
	
	/** Tempo padr�o em milisegundos que o cursor permanece invis�vel. */
	public static final int TIME_DEFAULT_CURSOR_INVISIBLE = 250;
	
	/** Tempo em milisegundos que o cursor permanece vis�vel. */
	protected int caretTimeVisible = TIME_DEFAULT_CURSOR_VISIBLE;
	
	/** Tempo em milisegundos que o cursor permanece invis�vel. */
	protected int caretTimeInvisible = TIME_DEFAULT_CURSOR_INVISIBLE;	
	
	/** Tempo acumulado da anima��o de "pisca-pisca" do cursor. */
	protected int accTime;
	
	/** Contador de caracteres. */
	protected short charCount;
	
	/** Tipos dispon�veis de m�scara para password (INPUT_MODE_PASSWORD). */
	protected static final byte PASSWORD_MASK_TYPE_CHAR		= 0;
	protected static final byte PASSWORD_MASK_TYPE_DRAWABLE	= 1;
	
	protected byte passwordMaskType = PASSWORD_MASK_TYPE_CHAR;
	
	/** Caracter padr�o utilizado como m�scara para password. */
	public static final char PASSWORD_CHAR_MASK_DEFAULT = '*';

	/** Caracter utilizado como m�scara para password. */
	protected char passwordCharMask = PASSWORD_CHAR_MASK_DEFAULT;
	
	/** Drawable utilizado como m�scara para password. */
	protected Drawable passwordDrawableMask;
	
	/** Indica se o label est� ativo, ou seja, pode ser editado (quando n�o est� ativo, n�o ocorre tamb�m a anima��o do cursor). */
	protected boolean active;
	
	/** Listener dos eventos desse label. */
	protected EditLabelListener listener;
	
	/** Identificador do label. */
	protected short id;
	
	/** Posi��o x inicial do texto (utilizado para garantir que o cursor sempre estar� vis�vel). */
	protected short textX;
	
	protected final Pattern fill;
	
	protected final DrawableRect border;

	protected int borderColorSelected = 0xabadb3;
	
	protected int borderColorUnselected = 0xabadb3;
	
	protected int fillColorSelected = 0xffffff;
	
	protected int fillColorUnselected = 0xd0dfee;	
	
	
    /**
	 * Cria um label edit�vel.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param initialText texto inicial (pode ser vazio).
	 * @param maxChars n�mero m�ximo de caracteres.
	 * @param inputMode modo de entrada dos caracteres. Valores v�lidos:
	 * <ul>
	 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
	 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
	 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endere�os de e-mail.</li>
	 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, por�m n�o s�o exibidos. A m�scara que substitui
	 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padr�o, � um caracter '*' (asterisco)</li>
	 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endere�os de internet.</li>
	 * </ul>
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	public EditLabel( ImageFont font, String initialText, int maxChars, byte inputMode ) throws Exception {
		this( font, initialText, maxChars, inputMode, false );
	}
	
	
    /**
	 * Cria um label edit�vel.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param initialText texto inicial (pode ser vazio).
	 * @param maxChars n�mero m�ximo de caracteres.
	 * @param inputMode modo de entrada dos caracteres. Valores v�lidos:
	 * <ul>
	 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
	 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
	 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endere�os de e-mail.</li>
	 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, por�m n�o s�o exibidos. A m�scara que substitui
	 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padr�o, � um caracter '*' (asterisco)</li>
	 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endere�os de internet.</li>
	 * </ul>
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	public EditLabel( ImageFont font, String initialText, int maxChars, byte inputMode, boolean useBorderAndFill ) throws Exception {
		super( font, initialText );
		setMaxChars( maxChars );
		setInputMode( inputMode );
		
		final char[][] charTable = {
				{ ' ', '0', '\n', '+', '-', '=', '%', ':', ';', '$', '*', '#' },	// NUM0
				{ '.', ',', '?', '!', '1', '_', '@', '/', '\'', '\"' },				// NUM1
				{ 'a', 'b', 'c', '2', '�', '�', '�', '�' },							// NUM2
				{ 'd', 'e', 'f', '3', '�', '�' },									// NUM3
				{ 'g', 'h', 'i', '4', '�' },										// NUM4
				{ 'j', 'k', 'l', '5' },												// NUM5
				{ 'm', 'n', 'o', '6', '�' },										// NUM6
				{ 'p', 'q', 'r', 's', '7' },										// NUM7
				{ 't', 'u', 'v', '8', '�' },										// NUM8
				{ 'w', 'x', 'y', 'z', '9' },										// NUM9
				{ ' ', '(', ')', '[', ']', '{', '}', '<', '>', '\\' },				// asterisco (*)
			};		
		
		Vector temp = new Vector();
		for ( int i = 0; i < charTable.length; ++i ) {
			final char[] iChars = charTable[ i ];
			for ( int j = 0; j < iChars.length; ++j ) {
				final char c = iChars[ j ];
				if ( font.getCharWidth( c ) > 0 ) {
					// existe o caracter na fonte; o adiciona ao vector tempor�rio.
					temp.addElement( new Character( c ) );
				}
			}
			
			final char[] chars = new char[ temp.size() ];
			for ( int j = 0; j < chars.length; ++j )
				chars[ j ] = ( ( Character ) temp.elementAt( j ) ).charValue();
			// associa o c�digo das teclas num�ricas 0-9 ou * � sequ�ncia de caracteres detectada.
			final int KEY = i < charTable.length - 1 ? ( ScreenManager.KEY_NUM0 + i ) : ScreenManager.KEY_STAR;
			characters.put( new Integer( KEY ), chars );
			temp.removeAllElements();
		}
		
		if ( useBorderAndFill ) {
			fill = new Pattern( fillColorUnselected );
			border = new DrawableRect( borderColorUnselected );
			
			setSize( size.x, size.y + 2 );
		} else {
			fill = null;
			border = null;
		}
		
		if ( initialText != null && initialText.length() > 0 ) {
			charCount = (short) initialText.length();
			setCaretPosition( charCount );
		}
	} // fim do construtor EditLabel( ImageFont, String, int, byte )
	 
	
    /**
     * 
     * @param caret 
     */
	public void setCaret( Drawable caret ) {
		this.caret = caret;
		updateTextX();
	}
	 
	
    /**
     * 
     * @return 
     */
	public Drawable getCaret() {
		return caret;
	}
	
	
	public final void setListener( EditLabelListener listener, int id ) {
		this.listener = listener;
		this.id = ( short ) id;
	}
	
	
	public final EditLabelListener getListener() {
		return listener;
	}
	 
	
	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		if ( active ) {
			timeSinceLastKeyPress += delta;

			switch ( keyHeld ) {
				case ScreenManager.KEY_NUM0:
				case ScreenManager.KEY_NUM1:
				case ScreenManager.KEY_NUM2:
				case ScreenManager.KEY_NUM3:
				case ScreenManager.KEY_NUM4:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_NUM6:
				case ScreenManager.KEY_NUM7:
				case ScreenManager.KEY_NUM8:
				case ScreenManager.KEY_NUM9:
					if ( pendingCommit && timeSinceLastKeyPress >= TIME_KEY_PRESS ) {
						// segurou uma tecla de 0 a 9 pelo tempo m�nimo - troca o caracter atual pelo n�mero correspondente � tecla
						setCurrentChar( ( char ) ( '0' + lastKeyPressed - ScreenManager.KEY_NUM0 ) );

						// n�o avan�a diretamente para o pr�ximo caracter, para que o usu�rio ainda possa escolher outro caracter
						// e tamb�m para que, no caso de uso de m�scaras (password, por exemplo), o usu�rio possa ver o n�mero
						keyHeld = 0;
						timeSinceLastKeyPress = 0;
					}
				break;

				case ScreenManager.LEFT:
				case ScreenManager.RIGHT:
					// navega automaticamente com o cursor ao segurar teclas esquerda/direita
					if ( timeSinceLastKeyPress >= TIME_NAVIGATE_KEY_REPEAT ) {
						keyPressed( keyHeld );
					}
				break;

				case ScreenManager.KEY_CLEAR:
					if ( timeSinceLastKeyPress >= TIME_KEY_CLEAR_ALL ) {
						while ( caretPosition > 0 )
							backSpace();
					}
				break;

				case 0:
					// n�o h� tecla pressionada
					if ( pendingCommit && timeSinceLastKeyPress >= TIME_KEY_WAIT_BEFORE_INSERT ) {
						commitChange( true );
					}			
				break;
			} // fim switch ( keyHeld )

			if ( caret != null && caretTimeInvisible > 0 ) {
				accTime += delta;

				if ( caret.isVisible() ) {
					if ( accTime >= caretTimeVisible ) {
						accTime -= caretTimeVisible;
						caret.setVisible( false );
					}
				} else {
					if ( accTime >= caretTimeInvisible ) {
						accTime -= caretTimeInvisible;
						caret.setVisible( true );
					}
				} // fim else ==> !caret.isVisible()
			} // fim if ( caret != null )
		} // fim if ( active )
	} // fim do m�todo update( int )
	
	
	/**
	 * Realiza a altera��o pendente, caso haja uma.
	 */
	private final void commitChange( boolean clearLastKeyPressed ) {
		pendingCommit = false;
		currentCharIndex = 0;
		
		if ( clearLastKeyPressed )
			lastKeyPressed = 0;
	}
	
	
	protected void insertChar( char c ) {
		if ( charCount < charBuffer.length ) {
			// faz o "chega-pra-l�" no array, para substituir eventuais lixos deixados anteriormente
			for ( short i = charCount; i > caretPosition; --i )
				charBuffer[ i ] = charBuffer[ i - 1 ];
				
			setCurrentChar( c );
			++charCount;
			++caretPosition;
			
			updateTextX();
		}
	}
	
	
	/**
	 * Apaga o caracter anterior � posi��o do cursor.
	 */
	public void backSpace() {
		if ( charCount > 0 ) {
			if ( caretPosition > 0 ) {
				commitChange( false );

				for ( int i = caretPosition - 1; i < charCount - 1; ++i ) {
					charBuffer[ i ] = charBuffer[ i + 1 ];
				}

				--charCount;
				--caretPosition;
				updateTextX();
			}
		} else if ( listener != null ) {
			listener.onBack( this, id );
		}
	}
	
	
	/**
	 * Define a velocidade do "pisca-pisca" do cursor.
	 * 
	 * @param caretTimeVisible tempo em milisegundos que o cursor permanece vis�vel.
	 * @param caretTimeInvisible tempo em milisegundos que o cursor permanece invis�vel. Valores menores ou iguais a
	 * zero significam que o cursor estar� sempre vis�vel.
	 */
	public void setCaretBlinkRate( int caretTimeVisible, int caretTimeInvisible ) {
		this.caretTimeVisible = caretTimeVisible;
		
		if ( caretTimeInvisible <= 0 && caret != null )
			caret.setVisible( true );
		
		this.caretTimeInvisible = caretTimeInvisible;
	} // fim do m�todo setCaretBlinkRate( int, int )

	
	/**
	 * 
	 * @see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
		if ( active ) {
			if ( key != lastKeyPressed ) {
				switch ( key ) {
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
					case ScreenManager.KEY_STAR:
						commitChange( true );
					break;
				} // fim switch ( key )
			}

			keyHeld = 0;
		}
	} // fim do m�todo keyReleased( int )

	
	/**
	 * 
	 * 
	 * @see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
		if ( active ) {
			// n�o permite 2 teclas pressionadas simultaneamente
			if ( keyHeld != 0 )
				keyReleased( keyHeld );

			final int previousKeyPressed = lastKeyPressed;

			keyHeld = key;
			lastKeyPressed = key;
			timeSinceLastKeyPress = 0;

			switch ( key ) {
				case ScreenManager.RIGHT:
					setCaretPosition( caretPosition + 1 );
				break;

				case ScreenManager.LEFT:
					setCaretPosition( caretPosition - 1 );				
				break;
				
				case ScreenManager.FIRE:
				case ScreenManager.KEY_SOFT_LEFT:
					if ( listener != null ) {
						listener.onConfirm( this, id );
					}
				break;

				case ScreenManager.KEY_SOFT_RIGHT:
					// como n�o � poss�vel detectar exatamente se o aparelho possui tecla espec�fica para CLEAR, sempre utiliza tamb�m
					// a soft key direita com essa fun��o.
					keyPressed( ScreenManager.KEY_CLEAR );
				break;

				case ScreenManager.KEY_CLEAR:
					backSpace();
				break;

				case ScreenManager.KEY_POUND:
					setCaseType( ( caseType + 1 ) % CASE_TYPES_TOTAL );
				break;

				case ScreenManager.KEY_NUM0:
				case ScreenManager.KEY_NUM1:
				case ScreenManager.KEY_NUM2:
				case ScreenManager.KEY_NUM3:
				case ScreenManager.KEY_NUM4:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_NUM6:
				case ScreenManager.KEY_NUM7:
				case ScreenManager.KEY_NUM8:
				case ScreenManager.KEY_NUM9:
				case ScreenManager.KEY_STAR:
					switch ( inputMode ) {
						case INPUT_MODE_NUMBERS:
							// apenas insere o n�mero correspondente
							insertChar( ( char ) ( '0' + lastKeyPressed - ScreenManager.KEY_NUM0 ) );
							commitChange( true );
						break;

						default:
							currentChars = ( char[] ) characters.get( new Integer( key ) );

							switch ( previousKeyPressed ) {
								case ScreenManager.KEY_NUM0:
								case ScreenManager.KEY_NUM1:
								case ScreenManager.KEY_NUM2:
								case ScreenManager.KEY_NUM3:
								case ScreenManager.KEY_NUM4:
								case ScreenManager.KEY_NUM5:
								case ScreenManager.KEY_NUM6:
								case ScreenManager.KEY_NUM7:
								case ScreenManager.KEY_NUM8:
								case ScreenManager.KEY_NUM9:
								case ScreenManager.KEY_STAR:
									if ( key != previousKeyPressed ) {
										if ( pendingCommit ) {
											commitChange( false );
											insertChar( currentChars[ 0 ] );
											currentCharIndex = 0;
											pendingCommit = true;
										}
									} else {
										nextChar();
										pendingCommit = true;
									}
								break;

								default:
									// primeira tecla pressionada - reinicia o contador do array de caracteres atual
									insertChar( currentChars[ 0 ] );
									currentCharIndex = 0;
									pendingCommit = true;
								// fim default
							} // fim switch ( previousKeyPressed )
						// fim default
					}
				break;
				
				default:
					if ( listener != null )
						listener.onUnhandledKey( this, id, key );
			} // fim switch ( key )		
		} // fim if ( active )
	} // fim do m�todo keyPressed( int )
	
	
	/**
	 * Define a quantidade m�xima de caracteres do label.
	 * @param maxChars n�mero m�ximo de caracteres do label. Qualquer valor igual ou maior que zero � v�lido; valores 
	 * negativos s�o ignorados.
	 */
	public void setMaxChars( int maxChars ) {
		if ( maxChars > 0 ) {
			try {
				final char[] oldCharBuffer = charBuffer;
				charBuffer = new char[ maxChars ];
				
				charCount = ( short ) Math.min( charCount, maxChars );
				if ( caretPosition > charCount )
					setCaretPosition( caretPosition );

				System.arraycopy( oldCharBuffer, 0, charBuffer, 0, Math.min( oldCharBuffer.length, maxChars ) );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
	//# 			e.printStackTrace();
				//#endif
			}
		}
	} // fim do m�todo setMaxChars( int )
	
	
	/**
	 * Posiciona o cursor no texto.
	 * @param position �ndice do cursor no texto.
	 */
	protected void setCaretPosition( int position ) {
		if ( circular )
			position = ( position + charCount + 1 ) % ( charCount + 1 );
		
		commitChange( true );
		caretPosition = NanoMath.clamp( position, 0, charCount );
		updateTextX();
	} // fim setCaretPosition( int )
	
	
	/**
	 * Define o modo de entrada de caracteres no label.
	 * 
	 * @param inputMode modo de entrada dos caracteres. Valores v�lidos:
	 * <ul>
	 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
	 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
	 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endere�os de e-mail.</li>
	 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, por�m n�o s�o exibidos. A m�scara que substitui
	 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padr�o, � um caracter '*' (asterisco)</li>
	 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endere�os de internet.</li>
	 * </ul>
	 */
	public void setInputMode( byte inputMode ) {
		switch ( inputMode ) {
			case INPUT_MODE_ANY:
			case INPUT_MODE_NUMBERS:
			case INPUT_MODE_EMAIL:
			case INPUT_MODE_PASSWORD:
			case INPUT_MODE_URL:
				this.inputMode = inputMode;
			break;
		} // fim switch ( inputMode )
	} // fim do m�todo setInputMode( byte )
	 
	
	public void setPasswordMask( Drawable mask ) {
		passwordDrawableMask = mask;
		
		if ( mask != null )
			passwordMaskType = PASSWORD_MASK_TYPE_DRAWABLE;
	}
	
	
	public void setPasswordMask( char mask ) {
		passwordMaskType = PASSWORD_MASK_TYPE_CHAR;
		passwordCharMask = mask;
	}
	
	
	public final void setActive( boolean active ) {
		this.active = active;
		
		if ( fill != null ) {
			if ( active ) {
				border.setColor( borderColorSelected );
				fill.setFillColor( fillColorSelected );
			} else {
				border.setColor( borderColorUnselected );
				fill.setFillColor( fillColorUnselected );
			}
		}
		
		if ( !active )
			commitChange( true );
	}
	
	
	public final boolean isActive() {
		return active;
	}
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( fill != null ) {
			border.setSize( width, height );
			fill.setSize( width - 2, height - 2 );
		}
		
		updateTextX();
	}		
	

	protected void paint( Graphics g ) {
		int x = translate.x + textX;
		final int y = fill == null ? 0 : 1;
		int index = 0;
		
		if ( fill != null ) {
			++x;
			
			fill.setPosition( 1, 1 );
			fill.draw( g );
			
			border.draw( g );
		}
		
		final int endX = translate.x + g.getClipWidth();
		
		// desenha o texto antes do cursor
		switch ( inputMode ) {
			case INPUT_MODE_PASSWORD:
				final int maxIndex = Math.min( caretPosition, charBuffer.length ) - ( pendingCommit ? 1 : 0 );
				
				for ( ; index < maxIndex && x < endX; ++index ) {
					switch ( passwordMaskType ) {
						case PASSWORD_MASK_TYPE_CHAR:
							drawChar( g, passwordCharMask, x, translate.y + y );
							x += font.getCharWidth( passwordCharMask );
						break;
						
						case PASSWORD_MASK_TYPE_DRAWABLE:
							passwordDrawableMask.setPosition( x, y );
							passwordDrawableMask.draw( g );
							x += font.getCharWidth( charBuffer[ index ] );
						break;
					}
				}
				
				// no caso de haver altera��o a ser confirmada, exibe o caracter sendo escolhido sem a m�scara
				if ( !pendingCommit )
					break;
			
			default:
				for ( ; index < caretPosition && index < charBuffer.length && x < endX; ++index ) {
					drawChar( g, charBuffer[ index ], x, translate.y + y );

					x += font.getCharWidth( charBuffer[ index ] );
				}
			// fim default
		} // fim switch ( inputMode )
		
		// desenha o cursor
		if ( active && caret != null ) {
			caret.setPosition( x - translate.x, y );
			
			caret.draw( g );
			
			x += caret.getWidth();
		}
			
		
		// desenha o texto ap�s o cursor
		switch ( inputMode ) {
			case INPUT_MODE_PASSWORD:
				final int maxIndex = Math.min( charCount, charBuffer.length );
				
				for ( ; index < maxIndex && x < endX; ++index ) {
					switch ( passwordMaskType ) {
						case PASSWORD_MASK_TYPE_CHAR:
							drawChar( g, passwordCharMask, x, translate.y + y );
							x += font.getCharWidth( passwordCharMask );
						break;
						
						case PASSWORD_MASK_TYPE_DRAWABLE:
							passwordDrawableMask.setPosition( x, y );
							passwordDrawableMask.draw( g );
							x += font.getCharWidth( charBuffer[ index ] );
						break;
					}
				}
			break;
			
			default:
				for ( ; index < charCount && index < charBuffer.length && x < endX; ++index ) {
					drawChar( g, charBuffer[ index ], x, translate.y + y );

					x += font.getCharWidth( charBuffer[ index ] );
				}
			// fim default
		} // fim switch ( inputMode )		

	}
	
	
	public final void setCircular( boolean circular ) {
		this.circular = circular;
	}
	
	
	public final boolean isCircular() {
		return circular;
	}
	
	
	public final void setCaseType( int caseType ) {
		this.caseType = ( byte ) caseType;
	}
	
	
	public final byte getCaseType() {
		return caseType;
	}
	
	
	/**
	 * Troca o caracter atual pelo pr�ximo caracter existente na fonte.
	 */
	protected void nextChar() {
		final byte previousCharIndex = currentCharIndex;
		do {
			currentCharIndex = ( byte ) ( ( currentCharIndex + 1 ) % currentChars.length );
		} while ( font.getCharWidth( currentChars[ currentCharIndex ] ) == 0 && currentCharIndex != previousCharIndex );
		
		setCurrentChar( currentChars[ currentCharIndex ] );
	}
	
	
	/**
	 * Atualiza a posi��o de in�cio do texto, de acordo com o tamanho do texto, do label e a posi��o do cursor.
	 */
	protected final void updateTextX() {
		int index = 0;
		int x = caret == null ? 0 : caret.getWidth();
		for ( ; index < caretPosition && index < charBuffer.length; ++index ) {
			x += font.getCharWidth( charBuffer[ index ] );
		}
		
		// x agora armazena a posi��o � direita do cursor
		textX = ( short ) Math.min( 0, size.x - x );
	}
	
	
	private final void setCurrentChar( char c ) {
		final int index = pendingCommit ? Math.max( caretPosition - 1, 0 ) : caretPosition;
		
		switch ( caseType ) {
			case CASE_TYPE_LOWER:
				charBuffer[ index ] = c;
			break;
			
			case CASE_TYPE_UPPER:
				charBuffer[ index ] = Character.toUpperCase( c );
			break;
			
			case CASE_TYPE_CAPITALIZE_WORDS:
				boolean upperCase = index == 0;
				if ( !upperCase ) {
					switch ( charBuffer[ index - 1 ] ) {
						case ' ':
						case '\n':
						case '\r':
						case '\t':
						case '.':
						case ',':
						case '-':
						case '_':
						case '\\':
						case '/':
						case ';':
						case '?':
						case '!':
						case '+':
						case '*':
						case '=':
						case '(':
						case ')':
						case ':':
						case '[':
						case ']':
						case '{':
						case '}':
							upperCase = true;
						break;
					}
				}
				charBuffer[ index ] = upperCase ? Character.toUpperCase( c ) : c;
			break;
		}
		
		updateTextX();
	}


	public void onPointerDragged(int x, int y) { // TODO
	}

	
	public void onPointerPressed(int x, int y) {// TODO
	}

	
	public void onPointerReleased(int x, int y) {// TODO
	}
}
 
