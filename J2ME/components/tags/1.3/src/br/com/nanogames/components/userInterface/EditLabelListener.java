/**
 * EditLabelListener.java
 * �2008 Nano Games.
 *
 * Created on Oct 28, 2008 11:07:28 AM.
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.EditLabel;

/**
 *
 * @author Peter
 */
public interface EditLabelListener {

	
	/**
	 * M�todo chamado por um EditLabel ao se pressionar as teclas CLEAR, BACK ou soft key direita com o texto vazio.
	 * @param label 
	 */
	public void onBack( EditLabel label, int id );
	
	
	/**
	 * M�todo chamado por um EditLabel ao se pressionar as teclas FIRE ou soft key esquerda.
	 * @param label
	 */
	public void onConfirm( EditLabel label, int id );
	
	
	/**
	 * M�todo chamado por um EditLabel caso ele receba um evento de tecla que n�o seja tratado.
	 * 
	 * @param label
	 * @param id
	 * @param key
	 */
	public void onUnhandledKey( EditLabel label, int id, int key );
	
}
