/**
 * NanoOnline.java
 * 
 * Created on 30/Out/2008, 16:49:16
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.SimpleScrollBar;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.DynamicByteArray;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 *
 * @author Peter
 */
public class NanoOnline implements NanoOnlineConstants {
	
	/** Vers�o do cliente Nano Online. */
	public static final String VERSION = "0.0.1";
	
	
	// <editor-fold defaultstate="collapsed" desc="IDIOMAS DO NANO ONLINE">
	
	/** Idioma do Nano Online: Ingl�s (EUA) */
	public static final byte LANGUAGE_en_US	= 0;
	
	/** Idioma do Nano Online: Espanhol (Espanha) */
	public static final byte LANGUAGE_es_ES	= 1;
	
	/** Idioma do Nano Online: Portugu�s (Brasil) */
	public static final byte LANGUAGE_pt_BR	= 2;
	
	/** Idioma do Nano Online: Alem�o (Alemanha) */
	public static final byte LANGUAGE_de_DE	= 3;
	
	/** Idioma do Nano Online: Portugu�s (Portugal) */
	public static final byte LANGUAGE_pt_PT	= 4;
	
	/** Idioma do Nano Online: Italiano (It�lia) */
	public static final byte LANGUAGE_it_IT	= 5;
	
	/** Idioma do Nano Online: Franc�s (Fran�a) */
	public static final byte LANGUAGE_fr_FR	= 6;
	
	/** Idioma do Nano Online: Japones (Jap�o) */
	public static final byte LANGUAGE_jp_JP	= 7;
	
	/** Idioma do Nano Online: Russo (R�ssia) */
	public static final byte LANGUAGE_ru_RU	= 8;
	
	/** Idioma do Nano Online: Coreano (Cor�ia) */
	public static final byte LANGUAGE_ko_KR	= 9;
	
	/** Idioma do Nano Online: Holand�s (Holanda) */
	public static final byte LANGUAGE_nl_NL	= 10;	
	
	/** Idioma padr�o do Nano Online: Ingl�s (EUA) */
	public static final byte LANGUAGE_DEFAULT = LANGUAGE_en_US;
	
	
	// </editor-fold>

	/** Mutex utilizado para sincronizar o m�todo setScreen(int). */
	protected final Mutex mutexScreen = new Mutex();
	
	/** �ndice da tela ativa atualmente. */
	protected int currentScreen = -1;
	
	/***/
	private final String[] texts;
	
	/***/
	private static NanoOnline instance;
	
	/***/
	private final ImageFont[] FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	
	/** Perfil de usu�rio atualmente ativo. */
	private static Customer currentCustomer = new Customer();
	
	private static Customer[] customers;
	
	/***/
	private byte language = LANGUAGE_DEFAULT;
	
	/** Form utilizado para mostrar as p�ginas (cont�m ainda uma barra de t�tulo e outra de status). */
	private final Form form;

	/***/
	private final ProgressBar progressBar;
	
	private int nextScreenIndex;
	
	
	static {
		//#if DEBUG == "true"
//# 			// apaga as refer�ncias est�ticas (emulador n�o reinicia de fato a execu��o...)
//# 			customers = null;
//# 			instance = null;
		//#endif
		
		// cria a base de dados do Nano Online
		try {
			AppMIDlet.createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif
		}				
	}
	
	
	private NanoOnline() throws Exception {
		if ( instance != null )
			unload();
		instance = this;
		
		loadProfiles();
		
		FONTS[ FONT_TEXT ] = ImageFont.createMultiSpacedFont( PATH_NANO_ONLINE_IMAGES + "font_0.png", PATH_NANO_ONLINE_IMAGES + "font_0.dat" );
		FONTS[ FONT_DEFAULT ] = FONTS[ FONT_TEXT ];
		
		texts = new String[ TEXTS_TOTAL ];
		AppMIDlet.loadTexts( texts, PATH_NANO_ONLINE_IMAGES + "pt.dat" );
		
		form = new Form();
		progressBar = new ProgressBar();
		form.setStatusBar( progressBar );
	}
	
	
	/**
	 * Equivalente � chamada de <code>load( backScreenIndex, SCREEN_MAIN_MENU )</code>.
	 * @param backScreenIndex
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Form load( int backScreenIndex ) throws Exception {
		return load( backScreenIndex, SCREEN_MAIN_MENU );
	}
	
	
	/**
	 * 
	 * @param backScreenIndex
	 * @param screenIndex
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Form load( int backScreenIndex, int screenIndex ) throws Exception {
		if ( instance == null )
			instance = new NanoOnline();
		
		// armazena a tela chamada ao sair do Nano Online
		instance.nextScreenIndex = backScreenIndex;
		
		setScreen( screenIndex );
		
		return instance.form;
	}
	
	
	public static final void unload() {
		if ( instance != null ) {
			// necess�rio para n�o receber mais eventos ap�s anular as refer�ncias internas
			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
			ScreenManager.setScreenListener( null );
			
			// chamando destroy() explicitamente reduz o risco de vazamento de mem�ria
			instance.form.destroy();
			instance.progressBar.destroy();
				
			instance = null;
			
			System.gc();			
		}
	}
	
	
	protected static final void setScreen( int index ) {
		setScreen( index, -1 );
	}
	
	
	/**
	 * Troca a tela ativa atualmente, tratando a sincroniza��o entre 2 poss�veis chamadas concorrentes do m�todo. Este
	 * m�todo <b>n�o</b> troca efetivamente a tela ativa no <code>ScreenManager</code> - essa troca deve ser feita dentro 
	 * do m�todo <code>changeScreen</code>, que deve ser estendido pelas aplica��es.
	 * 
	 * @param index �ndice da nova tela ativa.
	 * @see #changeScreen(int)
	 */
	protected static final void setScreen( int index, int backIndex ) {
		if ( instance.mutexScreen.acquire() ) {
			instance.changeScreen( index, backIndex );
		}
		//#if DEBUG == "true"
//# 		// TODO o que fazer no caso de n�o conseguir o acesso ao mutex?
//# 		else {
//# 			System.out.println( "AppMIDLet.setScreen: mutexScreen.acquire() retornou false." );
//# 		}
		//#endif	
	} // fim do m�todo setScreen( int )
	
	
	private synchronized final void changeScreen( int index, final int backIndex ) {
		try {
			Container nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;

			switch ( index ) {
				case SCREEN_MAIN_MENU:
					nextScreen = new MainScreen();
					indexSoftRight = TEXT_EXIT;
				break;

				case SCREEN_LOAD_PROFILE:
					nextScreen = new LoginScreen( backIndex );
					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_PROFILE_SELECT:
					nextScreen = new ProfileSelector( backIndex );
					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_REGISTER_PROFILE:
					nextScreen = new ProfileScreen( true, backIndex );
					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_PROFILE_EDIT:
					nextScreen = new ProfileScreen( false, backIndex );
					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_HELP_MENU:
					RankingScreen.isHighScore( 0, -1, NanoMath.randInt( 100000 ), true );
					changeScreen( SCREEN_NEW_RECORD, index );
					
					// TODO
//					indexSoftRight = TEXT_BACK;
//
//					nextScreen = new BasicMenu( 
//							new int[] { 
//										TEXT_HELP_TITLE_COMMANDS,
//										TEXT_HELP_TITLE_RANKING,
//										TEXT_HELP_TITLE_SUPPORT,
//										TEXT_HELP_TITLE_TAXES,
//										TEXT_BACK,
//							}, 4 ) {
//						protected void buttonPressed( int index ) {
//							switch ( index ) {
//								case 0:
//								case 1:
//								case 2:
//								case 3:
//									NanoOnline.setScreen( SCREEN_HELP_COMMANDS + index, backIndex );
//								break;
//
//								case 4:
//									NanoOnline.setScreen( backIndex >= 0 ? backIndex : SCREEN_MAIN_MENU );
//								break;
//							}
//						}
//					};
				break;

				case SCREEN_HELP_COMMANDS:
				case SCREEN_HELP_RANKING:
				case SCREEN_HELP_SUPPORT:
				case SCREEN_HELP_TAXES:
					final FormText text = new FormText( getFont( FONT_DEFAULT ) );
					text.addActionListener( new EventListener() {
						public final void eventPerformed( Event evt ) {
							switch ( evt.eventType ) {
								case Event.EVT_KEY_PRESSED:
									final int key = ( ( Integer ) evt.data ).intValue();

									switch ( key ) {
										case ScreenManager.KEY_BACK:
										case ScreenManager.KEY_CLEAR:
										case ScreenManager.KEY_SOFT_RIGHT:
										case ScreenManager.FIRE:
										case ScreenManager.KEY_NUM5:
										case ScreenManager.KEY_SOFT_LEFT:
											setScreen( backIndex >= 0 ? backIndex : SCREEN_HELP_MENU );
										break;
									}
								break;
							}
						}
					} );
					text.setText( getText( TEXT_HELP_TEXT_COMMANDS + index - SCREEN_HELP_COMMANDS ) );
					text.setScrollBarV( getScrollBarV() );
					nextScreen = text;

					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_NEW_RECORD:
					nextScreen = new NewRecordScreen();
					indexSoftRight = TEXT_OK;
				break;
				
				case SCREEN_RECORDS:
					nextScreen = new RankingMenu();
//					nextScreen = new RankingScreen( false, 0, currentScreen );
					indexSoftRight = TEXT_BACK;
				break;
				
				case SCREEN_ENTER_LOCAL_NAME:
					nextScreen = new LocalRankingScreen();
					indexSoftRight = TEXT_BACK;
				break;
				
				//#if DEBUG == "true"
//# 				default:
//# 					throw new IllegalArgumentException( "Invalid screen index: " + index );
				//#endif
			} // fim switch ( index )

			switch ( indexSoftRight ) {
				case SOFT_KEY_REMOVE:
					getProgressBar().setSoftKey( null );
				case SOFT_KEY_DONT_CHANGE:
				break;
				
				default:
					getProgressBar().setSoftKey( getText( indexSoftRight ) );
			}
			
//				setBackground( bkgType );
			nextScreen.setId( index );
			form.setContentPane( nextScreen );

			currentScreen = index;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			//# 				e.printStackTrace();
			//#endif

			return;
		} finally {
			mutexScreen.release();
		}
	}
	
	
	/**
	 * Descarrega o Nano Online e volta � tela anterior ao Nano Online.
	 */
	public static void exit() {
		// armazena a refer�ncia antes de unload(), pois instance � anulado
		final int nextScreen = instance.nextScreenIndex;
		unload();
		AppMIDlet.setScreen( nextScreen );
	}
	
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	protected static final String getText( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
		return instance.texts[ index ];
		
		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			System.out.println( "invalid text index: " + index );
//# 			e.printStackTrace();
//# 			return null;
//# 		}
		//#endif
	}
	
	
	protected static final ImageFont getFont( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
		return instance.FONTS[ index ];

		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 			return null;
//# 		}
		//#endif		
	}


	public static final ScrollBar getScrollBarV() throws Exception  {
		return new SimpleScrollBar( ScrollBar.TYPE_VERTICAL );
	}


	public static final ScrollBar getScrollBarH() throws Exception  {
		return new SimpleScrollBar( ScrollBar.TYPE_HORIZONTAL );
	}
	
	
	public static final LineBorder getBorder() throws Exception {
		final LineBorder border = new LineBorder( 0xff0000, LineBorder.TYPE_ETCHED_RAISED ) {
			public void setState( int state ) {
				switch ( state ) {
					case STATE_PRESSED:
						setColor( 0xffffff );
					break;
					
					case STATE_ERROR:
						setColor( 0xff0000 );
					break;
					
					case STATE_WARNING:
						setColor( 0xaaaa00 );
					break;
					
					case STATE_UNFOCUSED:
					break;
					
					default:
						setColor( 0xaaaaaa );
				}
				
				setVisible( state != STATE_UNFOCUSED );
			}
		};
		
		return border;
	}
	
	
	public static final TitledBorder getTitledBorder( int titleIndex ) throws Exception {
		return new TitledBorder( new Label( getFont( FONT_DEFAULT ), getText( titleIndex ) ), getBorder() );
	}
	
	
	public static final Drawable getCaret() throws Exception {
		return new DrawableImage( PATH_NANO_ONLINE_IMAGES + "cursor.png" );
	}
	
	
	public static final ProgressBar getProgressBar() {
		return instance.progressBar;
	}
	
	
	public static final Form getForm() {
		return instance.form;
	}	
	
	
	public static final Customer getCurrentCustomer() {
		return currentCustomer;
	}
	
	
	public static final Customer getCustomer( int index ) {
		loadProfiles();
		
		try {
			return customers[ index ];
		} catch ( Exception e ) {
			return null;
		}
	}
	
	
	public static final Customer getCustomerById( int id ) {
		loadProfiles();
		
		if ( customers != null ) {
			for ( byte i = 0; i < customers.length; ++i ) {
				if ( customers[ i ].getId() == id )
					return customers[ i ];
			}
		}
		
		return null;
	}
	
	
	public static final Customer[] getCustomers() {
		loadProfiles();
		return customers;
	}
	
	
	public static final void setCurrentCustomerIndex( int index ) {
		currentCustomer = customers[ index ];
	}
	
	
	/**
	 * 
	 * @param output
	 * @throws java.io.IOException
	 */
	public static final void writeGlobalData( DataOutputStream output ) throws IOException {
		output.writeByte( ID_APP );
		output.writeUTF( ID_APP_DUMMY );
		output.writeByte( ID_CUSTOMER_ID );
		output.writeInt( currentCustomer == null ? Customer.ID_NONE : currentCustomer.getId() );
		output.writeByte( ID_RETURN_CODE );
		output.writeShort( 30000 );
		output.writeByte( ID_NANO_ONLINE_VERSION );
		output.writeUTF( VERSION );
		output.writeByte( ID_LANGUAGE );
		output.writeByte( instance.language );
		output.writeByte( ID_ERROR_MESSAGE );
		output.writeUTF( "mensagem de erro... bla bla bla!" );
		output.writeByte( ID_APP_VERSION );
		output.writeUTF( AppMIDlet.getMIDletVersion() );
		output.writeByte( ID_SPECIFIC_DATA );
	}
	
	
	public static final Hashtable readGlobalData( byte[] data ) throws IOException {
		final DataInputStream input = new DataInputStream( new ByteArrayInputStream( data ) );
		final Hashtable t = new Hashtable();
		
		try {
			while ( true ) {
				final byte id = input.readByte(); 
				System.out.println ( "LEU ID: " + id );
				switch ( id ) {
					case ID_APP:
					case ID_RETURN_CODE:
						t.put( new Byte( id ), new Short( input.readShort() ) );
					break;

					case ID_CUSTOMER_ID:
						t.put( new Byte( id ), new Integer( input.readInt() ) );
					break;

					case ID_LANGUAGE:
						t.put( new Byte( id ), new Byte( input.readByte() ) );
					break;

					case ID_APP_VERSION:
					case ID_NANO_ONLINE_VERSION:
					case ID_ERROR_MESSAGE:
						t.put( new Byte( id ), input.readUTF() );
					break;

					case ID_SPECIFIC_DATA:
						final DynamicByteArray d = new DynamicByteArray();
						d.readInputStream( input );
						t.put( new Byte( id ), d.getData() );
					break;
				}
			} 
		} catch ( EOFException e ) {
		}
		
		final Enumeration e = t.keys();
		while ( e.hasMoreElements() ) {
			Object next = e.nextElement();
			System.out.println( "BLA: " + next + " -> " + t.get( next ) );
		}
		
		return t;
	}
	
	
	private static final void loadProfiles() {
		if ( customers == null ) {
			try {
				AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_PROFILES, new Serializable() {

					public final void write( DataOutputStream output ) throws Exception {
					}


					public final void read( DataInputStream input ) throws Exception {
						// l� a quantidade de perfis ativos
						final byte TOTAL_PROFILES = input.readByte();
						if ( TOTAL_PROFILES > 0 ) {
							// l� o perfil ativo atualmente
							final byte CURRENT_PROFILE_INDEX = input.readByte();
							
							customers = new Customer[ TOTAL_PROFILES ];
							
							for ( byte i = 0; i < customers.length; ++i ) {
								customers[ i ] = new Customer();
								customers[ i ].read( input );
							}
							
							currentCustomer = customers[ CURRENT_PROFILE_INDEX ];
						}
					}
				} );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
				
				saveProfiles();
			}
		}
	}	
	
	
	/**
	 * Insere um novo perfil de usu�rio no final do array de perfis, e salva as altera��es no RMS. Caso o perfil
	 * j� esteja no aparelho, apenas grava os perfis j� existentes.
	 * @param c
	 * @throws ArrayStoreException caso j� tenha atingido o limite de perfis gravados no celular.
	 */
	public static final void addProfile( Customer c ) {
		//#if DEBUG == "true"
//# 		if ( c == null )
//# 			throw new IllegalArgumentException( "customer can't be null" );
//# 			System.out.println( "add Customer: " + c.getId() + ", " + c.getNickname() );
		//#endif
		
		loadProfiles();
		
		if ( customers == null ) {
			customers = new Customer[ 1 ];
		} else {
			// verifica se o perfil j� est� gravado no aparelho para evitar entradas duplicadas
			for ( byte i = 0; i < customers.length; ++i ) {
				if ( customers[ i ].getId() == c.getId() ) {
					saveProfiles();
					return;
				}
			}
			
			final Customer[] temp = customers;
			if ( temp.length == MAX_PROFILES ) {
				throw new ArrayStoreException();
			}
			customers = new Customer[ temp.length + 1 ];
			System.arraycopy( temp, 0, customers, 0, temp.length );
		}
		
		customers[ customers.length - 1 ] = c;
		saveProfiles();
	}
	
	
	public static final void removeProfile( Customer c ) {
		for ( byte i = 0; i < customers.length; ++i ) {
			if ( customers[ i ].getId() == c.getId() ) {
				removeProfile( i );
				return;
			}
		}		
	}
	
	
	public static final void removeProfile( int index ) {
		for ( int j = index; j < customers.length - 1; ++j ) {
			customers[ j ] = customers[ j + 1 ];
		}
		final Customer[] temp = customers;
		customers = new Customer[ temp.length - 1 ];
		System.arraycopy( temp, 0, customers, 0, temp.length );

		saveProfiles();
	}
	
	
	public static final void saveProfiles() {
		try {
			AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_PROFILES, new Serializable() {

				public void write( DataOutputStream output ) throws Exception {
					//#if DEBUG == "true"
//# 						System.out.println( "Gravando " + ( customers == null ? 0 : customers.length ) + "perfil(s) de usu�rios" );
					//#endif
					
					// quantidade de perfis ativos
					if ( customers == null ) {
						output.writeByte( 0 );
					} else {
						output.writeByte( customers.length );
						// �ndice do perfil ativo
						byte currentProfileIndex = 0;
						for ( byte i = 0; i < customers.length; ++i ) {
							if ( customers[ i ].getId() == currentCustomer.getId() )
								currentProfileIndex = i;
							break;
						}
						output.writeByte( currentProfileIndex );
						
						for ( byte i = 0; i < customers.length; ++i ) {
							//#if DEBUG == "true"
//# 								System.out.println( "Perfil #" + i + ":" );
							//#endif
							customers[ i ].write( output );
						}
						
					}
				}


				public final void read( DataInputStream input ) throws Exception {
				}
			} );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif				
		}
	}
	
}
