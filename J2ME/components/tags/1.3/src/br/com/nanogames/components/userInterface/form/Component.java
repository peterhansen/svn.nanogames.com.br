/**
 * Component.java
 * 
 * Created on 5/Nov/2008, 13:52:04
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import java.util.Enumeration;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public abstract class Component extends UpdatableGroup implements KeyListener, PointerListener {
	
	/** Container onde o componente est� inserido. */
	protected Container parent;

	/** Refer�ncia para o keyListener do componente (normalmente o principal Drawable do grupo). */
	protected KeyListener keyListener;

	/** Refer�ncia para o pointerListener do componente (normalmente o principal Drawable do grupo). */
	protected PointerListener pointerListener;

	/** Permite alterar a ordem do percurso de foco ao utilizar a tecla baixo. */
    private Component nextFocusDown;
	
	/** Permite alterar a ordem do percurso de foco ao utilizar a tecla cima. */
    private Component nextFocusUp;
	
	/** Permite alterar a ordem do percurso de foco ao utilizar a tecla direita. */
    private Component nextFocusRight;

	/** Permite alterar a ordem do percurso de foco ao utilizar a tecla esquerda. */
    private Component nextFocusLeft;	
	
	/** Indica se o componente est� com o foco. */
	protected boolean hasFocus;
	
	/** Indica se o componente est� ativo. Para fins de navega��o, um componente inativo continua podendo receber foco. */
	protected boolean enabled = true;
	
	/** Indica se o componente pode receber o foco. */
	protected boolean focusable = true;
	
	/***/
	protected Border border;
	
	/** Indica se o componente pode tratar eventos de entrada, como teclas e ponteiros. */
	protected boolean handlesInput;
	
	/** Dimens�es preferidas pelo componente. Essas dimens�es n�o s�o necessariamente respeitadas, ou seja,
	 * seu tamanho pode ser maior ou menor que as dimens�es preferidas.
	 */
	protected Point preferredSize;
	
	/***/
	private boolean autoCalcPreferredSize = true;

	/** Dimens�es m�ximas do componente. Essas dimens�es s�o utilizadas como restri��es ao calcular automaticamente o
	 * tamanho preferido do componente. Se tiver um dos eixos (x e/ou y) menores ou iguais a zero, considera-se que n�o 
	 * h� restri��es nesse eixo.
	 */
	protected final Point maximumSize = new Point( Integer.MAX_VALUE, Integer.MAX_VALUE );

	/***/
	protected Vector eventListeners;

	/** Identificador do componente, para ser passado ao listener. Seu valor inicial � igual a <code>Integer.MIN_VALUE</code>. */
	protected int id = Integer.MIN_VALUE;
			
	
	public Component( int nSlots ) {
		this( nSlots, new Point() ); // TODO teste
	}
	
	
	/**
	 * Cria um novo componente.
	 * @param nSlots quantidade m�xima de drawables que podem ser inseridos.
	 * @param preferredSize tamanho desejado do componente. Pode-se passar a refer�ncia para o <code>size</code> do
	 * drawable principal do componente, assim toda altera��o no tamanho do drawable ser� automaticamente refletida no
	 * componente. Caso o par�metro seja <code>null</code>, o pr�prio tamanho do componente � considerado o seu tamanho
	 * desejado.
	 */
	protected Component( int nSlots, Point preferredSize ) {
		super( nSlots );
		
		this.preferredSize = preferredSize;
	}
	
	
	/**
	 * Destr�i o componente. Esse m�todo deve sempre ser chamado antes de se anular a refer�ncia para o componente,
	 * para anular poss�veis refer�ncias para outros componentes e assim evitar vazamentos de mem�ria.
	 */
	public void destroy() {
		setParent( null );
		setPointerListener( null );
		setKeyListener( null );
		setNextFocusDown( null );
		setNextFocusUp( null );
		setNextFocusLeft( null );
		setNextFocusRight( null );

		if ( eventListeners != null ) {
			eventListeners.removeAllElements();
			eventListeners = null;
		}
	}


	/**
	 *
	 * @param listener
	 */
	public void addActionListener( EventListener listener ) {
		// TODO quando um componente � destru�do, pode haver listeners de eventos registrados, causando vazamento de mem�ria. Solu��o: listeners an�nimos?
		if ( eventListeners == null )
			eventListeners = new Vector();

		eventListeners.addElement( listener );
	}


	/**
	 * 
	 * @param listener
	 */
	public void removeActionListener( EventListener listener ) {
		if ( eventListeners != null )
			eventListeners.removeElement( listener );
	}


	/**
	 * Dispara um evento para todos os listeners registrados, ou at� que o evento seja consumido.
	 * @param evt evento a ser disparado para os listeners.
	 */
	protected void dispatchActionEvent( Event evt ) {
		if ( eventListeners != null ) {
			for ( Enumeration e = eventListeners.elements(); e.hasMoreElements() && !evt.isConsumed(); ) {
				( ( EventListener ) e.nextElement() ).eventPerformed( evt );
			}
		}
	}
	
	
    /**
     * Identificador �nico de um componente (ou grupo de componentes). Deve ser implementado pelas classes que
	 * derivam de Component para que um determinado estilo possa ser aplicado ao componente (similar ao funcionamento
	 * de CSS).
     * 
     * @return string �nica identificando este componente para a folha de estilos.
     */
    public abstract String getUIID();
	
	
    /**
     * This flag doesn't really give focus, its a state that determines
     * what colors from the Style should be used when painting the component.
     * Actual focus is determined by the parent form.
     * 
     * @param focused sets the state that determines what colors from the 
     * Style should be used when painting a focused component
     * 
     * @see #requestFocus
     */
    public void setFocus( boolean focus ) {
        this.hasFocus = focus;
		
		dispatchActionEvent( new Event( this, focus ? Event.EVT_FOCUS_GAINED : Event.EVT_FOCUS_LOST ) );
    }
	
	
    /**
     * Changes the current component to the focused component, will work only
     * for a component that belongs to a parent form.
     */
    public void requestFocus() {
        Form rootForm = getComponentForm();
        if ( rootForm != null ) {
            rootForm.requestFocus( this );
        }
    }	
	
	
    /**
     * Returns the Component Form or null if this Component is not added yet to a form.
     * 
     * @return the Component Form
     */
    public Form getComponentForm() {
        if ( parent != null ) {
            return parent.getComponentForm();
        }
		
        return null;
    }	
	
	
    /**
     * Returns true if this component can receive focus.
     * 
     * @return true if this component can receive focus; otherwise false
     */
    public boolean isFocusable() {
        return focusable && isVisible();
    }


    /**
     * A simple setter to determine if this Component can get focused
     * 
     * @param focusable indicate whether this component can get focused
     */
    public void setFocusable( boolean focusable ) {
        this.focusable = focusable;
        final Form p = getComponentForm();
        if ( p != null ) {
            p.clearFocusVectors();
        }
    }	
	
	
	public final boolean isEnabled() {
		return enabled;
	}


	public void setEnabled( boolean enabled ) {
		this.enabled = enabled;
	}	
	
	
    /**
     * Prevents key events from being grabbed for focus traversal. E.g. a list component
     * might use the arrow keys for internal navigation so it will switch this flag to
     * true in order to prevent the focus manager from moving to the next component.
     * 
     * @return true if key events are being used for focus traversal
     * ; otherwise false
     */
    public boolean handlesInput() {
        return handlesInput;
    }

	
    /**
     * Prevents key events from being grabbed for focus traversal. E.g. a list component
     * might use the arrow keys for internal navigation so it will switch this flag to
     * true in order to prevent the focus manager from moving to the next component.
     * 
     * @param handlesInput indicates whether key events can be grabbed for 
     * focus traversal
     */
    public void setHandlesInput(boolean handlesInput) {
        this.handlesInput = handlesInput;
    }	
	
	
	public void keyPressed( int key ) {
		// TODO testar tratamento de eventos - s� dispara um Event caso n�o haja listener
		if ( keyListener == null )
			dispatchActionEvent( new Event( this, Event.EVT_KEY_PRESSED, new Integer( key ) ) );
		else
			keyListener.keyPressed( key );
	}

	
	public void keyReleased( int key ) {
		if ( keyListener == null )
			dispatchActionEvent( new Event( this, Event.EVT_KEY_RELEASED, new Integer( key ) ) );
		else
			keyListener.keyReleased( key );
	}

	
	public void onPointerPressed( int x, int y ) {
		if ( pointerListener == null )
			dispatchActionEvent( new Event( this, Event.EVT_POINTER_PRESSED, new Point( x, y ) ) );
		else
			pointerListener.onPointerPressed( x - position.x, y - position.y );
	}

	
	public void onPointerDragged( int x, int y ) {
		if ( pointerListener == null )
			dispatchActionEvent( new Event( this, Event.EVT_POINTER_DRAGGED, new Point( x, y ) ) );
		else
			pointerListener.onPointerDragged( x - position.x, y - position.y );
	}

	
	public void onPointerReleased( int x, int y ) {
		if ( pointerListener == null )
			dispatchActionEvent( new Event( this, Event.EVT_POINTER_RELEASED, new Point( x, y ) ) );
		else	
			pointerListener.onPointerReleased( x - position.x, y - position.y );
	}
	
	
	public final Container getParent() {
		return parent;
	}

	
	public void setParent( Container parent ) {
		this.parent = parent;
	}	
	
	
    /**
     * Returns the absolute X location based on the component hierarchy, this method
     * calculates a location on the screen for the component rather than a relative
     * location as returned by getX()
     * 
     * @return the absolute x location of the component
     * @see #getPosX
     */
    public int getAbsoluteX() {
		if ( parent != null )
			return position.x + parent.getAbsoluteX();
		
		return position.x;
    }	
	

	/**
	 * Returns the absolute Y location based on the component hierarchy, this method
	 * calculates a location on the screen for the component rather than a relative
	 * location as returned by getY()
	 * 
	 * @return the absolute y location of the component
	 * @see #getPosY
	 */
	public int getAbsoluteY() {
		if ( parent != null ) {
			return position.y + parent.getAbsoluteY();
		}
		return position.y;
	}


	/**
	 * Returns the absolute position location based on the component hierarchy, this method
	 * calculates a location on the screen for the component rather than a relative
	 * location as returned by getPosition()
	 * 
	 * @see #getPosition
	 */
	public void getAbsolutePos( Point p ) {
		p.addEquals( position );

		if ( parent != null ) {
			parent.getAbsolutePos( p );
		}
	}


	public final KeyListener getKeyListener() {
		return keyListener;
	}


	public void setKeyListener( KeyListener keyListener ) {
		this.keyListener = keyListener;
	}


	public final PointerListener getPointerListener() {
		return pointerListener;
	}


	public void setPointerListener( PointerListener pointerListener ) {
		this.pointerListener = pointerListener;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the down key
	 */
	public Component getNextFocusDown() {
		return nextFocusDown;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the down key
	 */
	public void setNextFocusDown( Component nextFocusDown ) {
		this.nextFocusDown = nextFocusDown;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the up key. 
	 */
	public Component getNextFocusUp() {
		return nextFocusUp;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the up key, this method doesn't affect the general focus behavior.
	 */
	public void setNextFocusUp( Component nextFocusUp ) {
		this.nextFocusUp = nextFocusUp;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the left key. 
	 */
	public Component getNextFocusLeft() {
		return nextFocusLeft;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the left key, this method doesn't affect the general focus behavior.
	 */
	public void setNextFocusLeft( Component nextFocusLeft ) {
		this.nextFocusLeft = nextFocusLeft;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the right key
	 */
	public Component getNextFocusRight() {
		return nextFocusRight;
	}


	/**
	 * Allows us to determine which component will receive focus next when traversing 
	 * with the right key
	 */
	public void setNextFocusRight( Component nextFocusRight ) {
		this.nextFocusRight = nextFocusRight;
	}
	
	
	/**
	 * @return 
	 * @see #getLayoutHeight()
	 * @see #getLayoutSize()
	 */	
	public int getLayoutWidth() {
		return ( border == null ? getWidth() : getWidth() - border.getBorderWidth() );
	}
	
	
	/**
	 * @return 
	 * @see #getLayoutWidth()
	 * @see #getLayoutSize()
	 */	
	public int getLayoutHeight() {
		return ( border == null ? getHeight() : getHeight() - border.getBorderHeight() );
	}
	
	
	/**
	 * Retorna um <code>Point</code> informando as dimens�es internas do componente. Equivalente � chamada de
	 * <code>new Point( getLayoutWidth(), getLayoutHeight() )</code>.
	 * @return 
	 * @see #getLayoutWidth()
	 * @see #getLayoutHeight()
	 */
	public final Point getLayoutSize() {
		return new Point( getLayoutWidth(), getLayoutHeight() );
	}
	
	
    /**
     * Returns the Component Preferred Size, there is no guarantee the Component will 
     * be sized at its Preferred Size. The final size of the component may be
     * smaller than its preferred size or even larger than the size.<br>
     * The Layout manager can take this value into consideration, but there is
     * no guarantee or requirement.
     * 
     * @return the component preferred size
     */
    public Point getPreferredSize() {
//		if ( autoCalcPreferredSize && preferredSize != null ) { TODO teste
		if ( autoCalcPreferredSize || preferredSize == null ) {
			setPreferredSize( calcPreferredSize() );
			autoCalcPreferredSize = false;
		}
		
        return preferredSize;
    }


	public final Point getPreferredSize( Point maxSize ) {
		if ( maxSize != null && ( preferredSize == null || !preferredSize.equals( maxSize ) ) ) {
			return calcPreferredSize( maxSize );
		}

		return getPreferredSize();
	}


	/**
	 * Helper method to retrieve the preferred width of the component.
	 * 
	 * @return preferred width of the component
	 * @see #getPreferredSize
	 */
	public final int getPreferredWidth() {
		return getPreferredSize() == null ? size.x : preferredSize.x;
	}


	/**
	 * Helper method to retrieve the preferred height of the component.
	 * 
	 * @return preferred height of the component
	 * @see #getPreferredSize
	 */
	public final int getPreferredHeight() {
		return getPreferredSize() == null ? size.y : preferredSize.y;
	}
	
	
    /**
     * Sets the Component Preferred Size, there is no guarantee the Component will 
  	 * be sized at its Preferred Size. The final size of the component may be
	 * smaller than its preferred size or even larger than the size.<br>
	 * The Layout manager can take this value into consideration, but there is
	 * no guarantee or requirement.
	 * 
	 * @param p the component dimension
	 */
	public void setPreferredSize( Point p ) {
		preferredSize = p;
	}
	
	
    /**
     * Calculates the preferred size based on component content. This method is
     * invoked lazily by getPreferred size.
     * 
     * @return the calculated preferred size based on component content
     */
    public final Point calcPreferredSize() {
		return calcPreferredSize( null );
    }


	public Point calcPreferredSize( Point maximumSize ) {
		// TODO leva em conta o maximumSize aqui?
        return new Point( size );
	}
	
	
	/**
	 * Indicates the values within the component have changed and preferred 
	 * size should be recalculated
	 * 
	 * @param autoCalcPreferredSize indicate whether this component need to 
	 * recalculate his preferred size
	 */
	protected void setAutoCalcPreferredSize( boolean autoCalcPreferredSize ) {
		if ( autoCalcPreferredSize != this.autoCalcPreferredSize ) {
			this.autoCalcPreferredSize = autoCalcPreferredSize;
			if ( autoCalcPreferredSize && getParent() != null ) {
				getParent().setAutoCalcPreferredSize( autoCalcPreferredSize );
			}
		}
	}


	public final Point getMaximumSize( Point maxSize ) {
		if ( !maxSize.equals( this.maximumSize ) )
			return calcPreferredSize( maxSize );

		return getMaximumSize();
	}


	/**
	 * 
	 * @return
	 */
	public final Point getMaximumSize() {
		return maximumSize;
	}


	/**
	 * 
	 */
	public final int getMaximumWidth() {
		return maximumSize.x;
	}

	
	/**
	 *
	 */
	public final int getMaximumHeight() {
		return maximumSize.y;
	}


	public final void setMaximumSize( Point maximumSize ) {
		setMaximumWidth( maximumSize.x );
		setMaximumHeight( maximumSize.y );
	}


	public final void setMaximumWidth( int maxWidth ) {
		if ( maxWidth <= 0 )
			maximumSize.x = Integer.MAX_VALUE;
		else
			maximumSize.x = maxWidth;
	}


	public final void setMaximumHeight( int maxHeight ) {
		if ( maxHeight <= 0 )
			maximumSize.y = Integer.MAX_VALUE;
		else
			maximumSize.y = maxHeight;
	}
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		if ( border != null )
			border.setSize( size );
	}
	
	
	public void draw( Graphics g ) {
		if ( border == null ) {
			super.draw( g );
		} else {
			if ( visible ) {
				pushClip( g );
				translate.addEquals( position );

				// TODO modos de desenho da borda: antes ou depois do conte�do
				// desenha a borda
				border.draw( g );

				final Point topLeft = border.getTopLeft();
				
				translate.addEquals( topLeft );

				// desenha primeiro a parte interna (conte�do). A dupla pushClip/popClip � necess�ria para garantir a corretude
				// no c�lculo de interse��o das �reas de clip.
				pushClip( g );
				
				Rectangle clip = clipStack[ currentStackSize ];
				clip.setIntersection( translate.x, translate.y, size.x - border.getBorderWidth(), size.y - border.getBorderHeight() );

				if ( clip.width > 0 && clip.height > 0 ) {
					g.setClip( clip.x, clip.y, clip.width, clip.height );

					paint( g );
				}
				translate.subEquals( topLeft );
				
				popClip( g );
				

				translate.subEquals( position );
				popClip( g );
			} // fim if ( visible )  			
		}
	}
	
	
	/**
	 * 
	 * @param border
	 */
	public final void setBorder( Border border ) {
		setBorder( border, true );
	}


	/**
	 * 
	 * @param border
	 * @param fitSize
	 */
	public void setBorder( Border border, boolean fitSize ) {
		final Border previousBorder = this.border;
		this.border = border;
		
		if ( border != null ) {
			border.setComponent( this, fitSize );
			
			if ( fitSize ) {
				//#if DEBUG == "true"
//# 					System.out.println( this );
//# 					System.out.println( "FIT (1): " + size.x + ", " + size.y );
				//#endif

//				setSize( getWidth() + border.getBorderWidth(), getHeight() + border.getBorderHeight() ); // TODO necess�rio?

				//#if DEBUG == "true"
//# 					System.out.println( "FIT (2): " + size.x + ", " + size.y );
//# 					System.out.println();
				//#endif
			}
				
		}
		
		if ( border != previousBorder ) // TODO testar
			setAutoCalcPreferredSize( true );
	}


	/**
	 * 
	 * @return
	 */
	public final Border getBorder() {
		return border;
	}


	/**
	 * Obt�m o id desse componente. O id pode ser utilizado para identificar de forma mais f�cil a origem de um evento
	 * recebido por um ActionListener, por exemplo (dentro de um switch, em vez de sequ�ncias de if/else comparando objetos).
	 * @return inteiro identificador do objeto.
	 * @see #setId(int)
	 */
	public int getId() {
		return id;
	}


	/**
	 * Define o id desse componente. O id pode ser utilizado para identificar de forma mais f�cil a origem de um evento
	 * recebido por um ActionListener, por exemplo (dentro de um switch, em vez de sequ�ncias de if/else comparando objetos).
	 * @param id identificador do objeto.
	 * @see #getId()
	 */
	public void setId( int id ) {
		this.id = id;
	}

}
