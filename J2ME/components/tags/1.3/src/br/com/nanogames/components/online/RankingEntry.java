/**
 * RankingEntry.java
 * 
 * Created on 1/Fev/2009, 12:12:34
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Peter
 */
public class RankingEntry implements Serializable {
	
	private long score = 0;
	
	private int profileId = -1;
	
	private String nickname = "";
	
	private boolean submitted;


	public String getNickname() {
		return nickname;
	}


	public void setNickname( String nickname ) {
		this.nickname = nickname;
	}


	public int getProfileId() {
		return profileId;
	}


	public void setProfileId( int profileId ) {
		this.profileId = profileId;
	}


	public long getScore() {
		return score;
	}


	public void setScore( long score ) {
		this.score = score;
	}


	public boolean isSubmitted() {
		return submitted;
	}


	public void setSubmitted( boolean submitted ) {
		this.submitted = submitted;
	}


	public void write( DataOutputStream output ) throws Exception {
		//#if DEBUG == "true"
//# 			//System.out.println( "Gravando entrada de ranking:\n\tscore: " + getScore() + "\n\tnickname: " + getNickname() + "\n\tprofile id: " + getProfileId() + "\n\tsubmitted: " + isSubmitted() );
		//#endif		
		output.writeLong( getScore() );
		output.writeUTF( getNickname() );
		output.writeInt( getProfileId() );
		output.writeBoolean( isSubmitted() );
	}


	public void read( DataInputStream input ) throws Exception {
		//#if DEBUG == "true"
//# 			//System.out.println( "Lendo entrada de ranking..." );
		//#endif
		
		setScore( input.readLong() );
		setNickname( input.readUTF() );
		setProfileId( input.readInt() );
		setSubmitted( input.readBoolean() );
		
		//#if DEBUG == "true"
//# 			//System.out.println( "Leu entrada de ranking:\n\tscore: " + getScore() + "\n\tnickname: " + getNickname() + "\n\tprofile id: " + getProfileId() + "\n\tsubmitted: " + isSubmitted() );
		//#endif			
	}
	
	
}
