/**
 * NanoOnlineContainer.java
 * 
 * Created on 29/Jan/2009, 12:25:03
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.userInterface.form.layouts.Layout;

/**
 * Classe que encapsula funcionamentos b�sicos de telas do Nano Online.
 * @author Peter
 */
public abstract class NanoOnlineContainer extends Container implements NanoOnlineConstants, EventListener {

	
	protected int backScreenIndex = -1;
	
	
	/**
	 * Cria um novo cont�iner do Nano Online, utilizando um FlowLayout com eixo vertical.
	 * @param nSlots
	 * @throws java.lang.Exception
	 * @see #NanoOnlineContainer( int, Layout )
	 */
	protected NanoOnlineContainer( int nSlots ) throws Exception {
		this( nSlots, new FlowLayout( FlowLayout.AXIS_VERTICAL ) );
	}
	
	
	/**
	 * Cria um novo cont�iner do Nano Online, utilizando o layout escolhido.
	 * @param nSlots
	 * @param layout
	 * @throws java.lang.Exception
	 * @see #NanoOnlineContainer( int )
	 */
	protected NanoOnlineContainer( int nSlots, Layout layout ) throws Exception {
		super( nSlots, layout );
		
		setScrollBarV( NanoOnline.getScrollBarV() );
		
		if ( layout instanceof FlowLayout ) {
			setScrollableY( true );
			( ( FlowLayout ) layout ).gap.set( 3, 3 );		
		}
		
		NanoOnline.getProgressBar().softkeyRight.addActionListener( this );
		//#if DEBUG == "true"
//# 		System.out.println( "-->CONSTRUIU CONTAINER: " + this );
		//#endif
	}


	/**
	 * Destr�i o cont�iner e tamb�m o remove da lista de listeners da barra de progresso.
	 */
	public void destroy() {
		super.destroy();
		
		NanoOnline.getProgressBar().softkeyRight.removeActionListener( this );
		
		//#if DEBUG == "true"
//# 		System.out.println( "-->DESTRUIU CONTAINER: " + this );
		//#endif
	}
	
	
	public final void setBackIndex( int backIndex ) {
		this.backScreenIndex = backIndex;
	}
	
	
	protected void onBack() {
		if ( backScreenIndex >= 0 ) {
			NanoOnline.setScreen( backScreenIndex  );
		}
	}
	
	
}
