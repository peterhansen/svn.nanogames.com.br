/**
 * Border.java
 * 
 * Created on 8/Dez/2008, 20:19:18
 *
 */

package br.com.nanogames.components.userInterface.form.borders;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author Peter
 */
public abstract class Border extends DrawableGroup {

	/** Estado da borda: sem foco. */
	public static final byte STATE_UNFOCUSED	= 0;

	/** Estado da borda: com foco. */
	public static final byte STATE_FOCUSED		= 1;

	/** Estado da borda: componente pressionado (ex.: bot�o). */
	public static final byte STATE_PRESSED		= 2;

	/** Estado da borda: aviso. */
	public static final byte STATE_WARNING		= 3;

	/** Estado da borda: erro. */
	public static final byte STATE_ERROR		= 4;

	/** Estado atual da borda. */
	protected byte state;

	/**
	 * Espessura da borda esquerda.
	 */
	protected byte left = 1;
	
	/**
	 * Espessura da borda direita.
	 */
	protected byte right = 1;
	
	/**
	 * Espessura da borda superior.
	 */
	protected byte top = 1;
	
	/**
	 * Espessura da borda inferior.
	 */
	protected byte bottom = 1;
	
	
	protected Border( int nSlots ) throws Exception {
		super( nSlots );
		
		// inicia as bordas no modo sem foco (chamada necess�ria para o caso de bordas com l�gicas pr�prias do
		// m�todo setState() )
		setState( STATE_UNFOCUSED );
	}


	/**
	 * 
	 * @return c�pia da borda.
	 */
	public abstract Border getCopy() throws Exception;
	
	
	public final void set( int top, int left, int bottom, int right ) {
		setTop( top );
		setLeft( left );
		setBottom( bottom );
		setRight( right );
	}


	/**
	 * Retorna a espessura da borda inferior.
	 */
	public byte getBottom() {
		return bottom;
	}


	/**
	 * Define a espessura da borda inferior.
	 */
	public void setBottom( int bottom ) {
		this.bottom = ( byte ) bottom;
	}


	/**
	 * Retorna a espessura da borda esquerda.
	 */
	public byte getLeft() {
		return left;
	}


	/**
	 * Define a espessura da borda esquerda.
	 */
	public void setLeft( int left ) {
		this.left = ( byte ) left;
	}


	/**
	 * Retorna a espessura da borda direita.
	 */
	public byte getRight() {
		return right;
	}


	/**
	 * Define a espessura da borda direita.
	 */
	public void setRight( int right ) {
		this.right = ( byte ) right;
	}


	/**
	 * Retorna a espessura da borda superior.
	 */
	public byte getTop() {
		return top;
	}


	/**
	 * Define a espessura da borda superior.
	 */
	public void setTop( int top ) {
		this.top = ( byte ) top;
	}
	
	
	/**
	 * Retorna um Point contendo a espessura da borda esquerda (x) e superior (y).
	 */
	public final Point getTopLeft() {
		return new Point( getLeft(), getTop() );
	}
	
	
	/**
	 * Retorna um Point contendo a espessura da borda direita (x) e inferior (y).
	 */
	public final Point getBottomRight() {
		return new Point( getRight(), getBottom() );
	}
	
	
	/**
	 * Obt�m a espessura total das bordas esquerda e direita.
	 */
	public final int getBorderWidth() {
		return getLeft() + getRight();
	}
	
	
	/**
	 * Obt�m a espessura total das bordas superior e inferior.
	 */
	public final int getBorderHeight() {
		return getTop() + getBottom();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public final Point getBorderSize() {
		return new Point( getBorderWidth(), getBorderHeight() );
	}	


	/**
	 * Ajusta a borda de modo a adaptar-se a uma mudan�a de estado.
	 * @param state novo estado da borda. A classe Border define alguns poss�veis estados:
	 * <ul>
	 * <li>STATE_UNFOCUSED</li>
	 * <li>STATE_FOCUSED</li>
	 * <li>STATE_PRESSED</li>
	 * <li>STATE_WARNING</li>
	 * <li>STATE_ERROR</li>
	 * </ul>
	 * 
	 * Nem todos os estados se aplicam ao componente que possui a borda. Outros estados podem ser utilizados, de acordo
	 * com a funcionalidade desejada. Nesse caso, recomenda-se a utiliza��o de �ndices negativos, para evitar confus�o com
	 * os valores dos estados padr�o aqui definidos (e utilizados por classes como Component e Button).
	 */
	public void setState( int state ) {
		this.state = ( byte ) state;
	}
	
	
	public byte getState() {
		return state;
	}
	
	
	/**
	 * 
	 * @param c
	 * @param fitSize
	 */
	public void setComponent( Component c, boolean fitSize ) {
		if ( fitSize ) {
			setSize( c.getSize().add( getBorderSize() ) );
//			setSize( c.getSize() );
		} else {
			setSize( c.getSize() );
		}
	}
	
}
