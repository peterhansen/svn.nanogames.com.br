/*
 * ScreenListener.java
 *
 * Created on August 23, 2007, 3:02 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 * Interface que recebe eventos relativos � tela, repassados por ScreenManager.
 *
 * @author peter
 * @see ScreenManager
 * @see #hideNotify()
 * @see #showNotify()
 * @see #sizeChanged(int,int)
 */
public interface ScreenListener {

	/**
	 * Evento chamado quando o aparelho recebe um evento de suspend. � importante notar que, dependendo do aparelho,
	 * esse m�todo n�o � chamado para todos os poss�veis eventos (liga��o, SMS, fechar flip, etc).
	 */	
	public void hideNotify();
	
	
	/**
	 * Evento chamado quando o aparelho volta de um evento de suspend. � importante notar que, dependendo do aparelho,
	 * esse m�todo n�o � chamado para todos os poss�veis eventos (liga��o, SMS, abrir flip, etc).
	 */	
	public void showNotify();
	
	
	/**
	 * Evento chamado quando as dimens�es da tela sofrem altera��o.
	 */
	public void sizeChanged( int width, int height );
	
}
