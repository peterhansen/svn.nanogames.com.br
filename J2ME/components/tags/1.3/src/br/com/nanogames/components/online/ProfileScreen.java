/**
 * ProfileScreen.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.EditLabel;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 *
 * @author Peter
 */
public class ProfileScreen extends NanoOnlineContainer implements ConnectionListener {

	// <editor-fold defaultstate="collapsed" desc="C�DIGOS DE RETORNO DO REGISTRO DO USU�RIO">

	// ATEN��O: ESSES C�DIGOS DEVEM *SEMPRE* ESTAR DE ACORDO COM OS C�DIGOS RETORNADOS PELO SERVIDOR!

	// obs.: codigo de retorno zero � OK (RC_OK)
	
	/** C�digo de retorno do registro do usu�rio: erro: apelido j� em uso. */
	private static final byte RC_ERROR_NICKNAME_IN_USE			= 1;
	
	/** C�digo de retorno do registro do usu�rio: erro: formato do apelido inv�lido. */
	private static final byte RC_ERROR_NICKNAME_FORMAT			= 2;
	
	/** C�digo de retorno do registro do usu�rio: erro: comprimento do apelido inv�lido. */
	private static final byte RC_ERROR_NICKNAME_LENGTH			= 3;
	
	/** C�digo de retorno do registro do usu�rio: erro: formato do e-mail inv�lido. */
	private static final byte RC_ERROR_EMAIL_FORMAT				= 4;
	
	/** C�digo de retorno do registro do usu�rio: erro: comprimento do e-mail inv�lido. */
	private static final byte RC_ERROR_EMAIL_LENGTH				= 5;
	
	/** C�digo de retorno do registro do usu�rio: erro: password muito fraca. */
	private static final byte RC_ERROR_PASSWORD_WEAK			= 6;
	
	/** C�digo de retorno do registro do usu�rio: erro: password e confirma��o diferentes. */
	private static final byte RC_ERROR_PASSWORD_CONFIRMATION	= 7;
	
	/** C�digo de retorno do registro do usu�rio: erro: comprimento do 1� nome inv�lido. */
	private static final byte RC_ERROR_FIRST_NAME_LENGTH		= 8;
	
	/** C�digo de retorno do registro do usu�rio: erro: comprimento do sobrenome inv�lido. */
	private static final byte RC_ERROR_LAST_NAME_LENGTH			= 9;
	
	/** C�digo de retorno do registro do usu�rio: erro: g�nero inv�lido. */
	private static final byte RC_ERROR_GENRE                    = 10;

	/** C�digo de retorno do registro do usu�rio: erro: comprimento do g�nero inv�lido. */
	private static final byte RC_ERROR_GENRE_LENGTH             = 11;

	/** C�digo de retorno do registro do usu�rio: erro: formato do n�mero de telefone inv�lido. */
	private static final byte RC_ERROR_PHONE_NUMBER_FORMAT      = 12;

	/** C�digo de retorno do registro do usu�rio: erro: comprimento do n�mero de telefone inv�lido. */
	private static final byte RC_ERROR_PHONE_NUMBER_LENGTH      = 13;
	
	// </editor-fold>
	
	/***/
	private static final byte ENTRY_NICKNAME				= 0;
	/***/
	private static final byte ENTRY_EMAIL					= ENTRY_NICKNAME + 1;
	/***/
	private static final byte ENTRY_PASSWORD				= ENTRY_EMAIL + 1;
	/***/
	private static final byte ENTRY_PASSWORD_CONFIRM		= ENTRY_PASSWORD + 1;
	
//	private static final byte ENTRY_FIRST_NAME;
//	private static final byte ENTRY_LAST_NAME;
//	private static final byte ENTRY_REMEMBER_ME;
//	private static final byte ENTRY_REMEMBER_PASSWORD;
	
	/***/
	private static final byte ENTRY_BUTTON_OK				= ENTRY_PASSWORD_CONFIRM + 1;
	private static final byte ENTRY_BUTTON_EDIT				= ENTRY_BUTTON_OK + 1;
	private static final byte ENTRY_BUTTON_USE				= ENTRY_BUTTON_EDIT + 1;
	/***/
	private static final byte ENTRY_BUTTON_CANCEL			= ENTRY_BUTTON_USE + 1;
	private static final byte ENTRY_BUTTON_ERASE			= ENTRY_BUTTON_CANCEL + 1;
	private static final byte ENTRY_BUTTON_BACK				= ENTRY_BUTTON_ERASE + 1;
	
	/** Bot�o para apagar o perfil (do celular). Vis�vel somente no modo de edi��o. */
	
	/***/
	private static final byte ENTRIES_TOTAL = ENTRY_BUTTON_BACK + 1;
	
	/** Quantidade m�nima de caracteres do apelido. */
	private static final byte NICKNAME_MIN_CHARS = 4;
	/** Quantidade m�nima de caracteres do e-mail. */
	private static final byte EMAIL_MIN_CHARS = 4;
	/** Quantidade m�nima de caracteres da password. */
	private static final byte PASSWORD_MIN_CHARS = 6;
	
	/***/
	private static final byte EDIT_LABELS_TOTAL = 4;
	
	/***/
	private final TextBox[] textBoxes = new TextBox[ EDIT_LABELS_TOTAL ];	
	
	/***/
	private final Button buttonEditOK;

	/***/
	private final Button buttonUse;
	
	/***/
	private final Button buttonErase;
	
	/***/
	private final Button buttonCancel;

	/***/
	private int currentConnection = -1;
	
	private static byte profileIndex = -1;
	
	private final boolean newProfile;
	
	
	/**
	 * 
	 * @param profileIndex
	 * @throws java.lang.Exception
	 */
	public ProfileScreen( boolean newProfile, int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );
		
		this.newProfile = newProfile;
		setBackIndex( backIndex >= 0 ? backIndex : SCREEN_PROFILE_SELECT );
		
		final ImageFont font = NanoOnline.getFont( FONT_DEFAULT );
		
		final byte[] MAX_CHARS = { 20, 30, 20, 20 };
		final byte[] INPUT_MODE = { EditLabel.INPUT_MODE_ANY, EditLabel.INPUT_MODE_EMAIL, EditLabel.INPUT_MODE_PASSWORD, EditLabel.INPUT_MODE_PASSWORD };
		final byte[] titles = { TEXT_NICKNAME, TEXT_EMAIL, TEXT_PASSWORD, TEXT_PASSWORD_CONFIRM };
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			final TextBox textBox = new TextBox( font, null, MAX_CHARS[ i ], INPUT_MODE[ i ], true );
			textBoxes[ i ] = textBox;
			
			textBox.setCaret( NanoOnline.getCaret() );
			textBox.setId( i );
			textBox.addActionListener( this );
			textBox.setPasswordMask( '@' );
			textBox.setBorder( NanoOnline.getTitledBorder( titles[ i ] ) );
			textBox.setEnabled( newProfile );
			
			if ( !newProfile && profileIndex >= 0 ) {
				final Customer customer = NanoOnline.getCustomer( profileIndex );
				switch ( i ) {
					case ENTRY_NICKNAME:
						textBox.setText( customer.getNickname(), false );
					break;

					case ENTRY_EMAIL:
						textBox.setText( customer.getEmail(), false );
						textBox.setCaretPosition( 0 );
					break;

					case ENTRY_PASSWORD_CONFIRM:
						if ( !customer.isRememberPassword() )
							break;

					case ENTRY_PASSWORD:
						textBox.setText( customer.getPassword(), false );
					break;
				}
			}
			
			insertDrawable( textBox );
		}
		
		
		// se o perfil j� existir, mostra a op��o "usar este perfil"
		if ( newProfile ) {
			buttonUse = null;
		} else {
			buttonUse = new Button( font, NanoOnline.getText( TEXT_USE_PROFILE ) );
			buttonUse.setId( ENTRY_BUTTON_USE );
			buttonUse.setBorder( NanoOnline.getBorder() );
			buttonUse.addActionListener( this );

			insertDrawable( buttonUse );
		} 
		
		
		buttonEditOK = new Button( font, NanoOnline.getText( newProfile ? TEXT_OK : TEXT_EDIT ) );
		buttonEditOK.setId( newProfile ? ENTRY_BUTTON_OK : ENTRY_BUTTON_EDIT );
		buttonEditOK.setBorder( NanoOnline.getBorder() );
		buttonEditOK.addActionListener( this );
		
		insertDrawable( buttonEditOK );
		
		// se o perfil j� existir, mostra a op��o "apagar"
		if ( newProfile ) {
			buttonErase = null;
		} else {
			buttonErase = new Button( font, NanoOnline.getText( TEXT_ERASE_PROFILE ) );
			buttonErase.setId( ENTRY_BUTTON_ERASE );
			buttonErase.setBorder( NanoOnline.getBorder() );
			buttonErase.addActionListener( this );

			insertDrawable( buttonErase );
		} 
		
		buttonCancel = new Button( font, NanoOnline.getText( newProfile ? TEXT_CANCEL : TEXT_BACK ) );
		buttonCancel.setId( newProfile ? ENTRY_BUTTON_CANCEL : ENTRY_BUTTON_BACK );
		buttonCancel.addActionListener( this );
		buttonCancel.setBorder( NanoOnline.getBorder() );
		insertDrawable( buttonCancel );
		
		if ( newProfile )
			setEditable();
	}
	

	public void processData( int id, byte[] data ) {
		onConnectionEnded( false );
		System.out.println( "processData: " + ( data == null ? -1 : data.length ) );
		if ( data != null ) {
			DataInputStream input = null;
			try {
				final Hashtable table = NanoOnline.readGlobalData( data );
				
				// o c�digo de retorno � obrigat�rio
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				byte errorEntryIndex = -1;
				byte errorText = -1;
				
				switch ( returnCode ) {
					case RC_OK:
						// grava as informa��es do usu�rio no RMS e mostra a tela de confirma��o
						final byte[] specificData = ( byte[] ) table.get( new Byte( ID_SPECIFIC_DATA ) );
						input = new DataInputStream( new ByteArrayInputStream( specificData ) );
						
						final Customer customer = newProfile ? new Customer() : NanoOnline.getCustomer( profileIndex );
						customer.setEmail( textBoxes[ ENTRY_EMAIL ].getText() );
//						customer.firstName = ;
//						customer.genre = ;
						if ( input.readByte() == ID_CUSTOMER_ID ) {
							customer.setId( input.readInt() );
							
	//						customer.lastName = ;
							customer.setNickname( textBoxes[ ENTRY_NICKNAME ].getText() );
							customer.setPassword( textBoxes[ ENTRY_PASSWORD ].getText() );
	//						customer.rememberPassword = ;

							if ( newProfile )
								NanoOnline.addProfile( customer );
							else
								NanoOnline.saveProfiles();
							
							NanoOnline.getProgressBar().showConfirmation( TEXT_CUSTOMER_REGISTERED );
							NanoOnline.setScreen( SCREEN_MAIN_MENU );
						} else {
							// erro na formata��o da resposta do servidor
							NanoOnline.getProgressBar().showError( TEXT_REGISTER_ERROR_REPLY_FORMAT );
							setInConnection( false );
						}
					break;
					
					case RC_SERVER_INTERNAL_ERROR:
						errorText = TEXT_ERROR_SERVER_INTERNAL;
					break;
					
					case RC_APP_NOT_FOUND:
						errorText = TEXT_ERROR_APP_NOT_FOUND;
					break;
					
					case RC_DEVICE_NOT_SUPPORTED:
						errorText = TEXT_ERROR_DEVICE_NOT_SUPPORTED;
					break;
					
					case RC_ERROR_NICKNAME_IN_USE:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_REGISTER_ERROR_NICKNAME_IN_USE;
					break;

					case RC_ERROR_NICKNAME_FORMAT:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_REGISTER_ERROR_NICKNAME_FORMAT;
					break;

					case RC_ERROR_NICKNAME_LENGTH:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_REGISTER_ERROR_NICKNAME_LENGTH;
					break;

					case RC_ERROR_EMAIL_FORMAT:
						errorEntryIndex = ENTRY_EMAIL;
						errorText = TEXT_REGISTER_ERROR_EMAIL_FORMAT;
					break;

					case RC_ERROR_EMAIL_LENGTH:
						errorEntryIndex = ENTRY_EMAIL;
						errorText = TEXT_REGISTER_ERROR_EMAIL_LENGTH;
					break;

					case RC_ERROR_PASSWORD_WEAK:
						errorEntryIndex = ENTRY_PASSWORD;
						errorText = TEXT_REGISTER_ERROR_PASSWORD_WEAK;
					break;

					case RC_ERROR_PASSWORD_CONFIRMATION:
						errorEntryIndex = ENTRY_PASSWORD_CONFIRM;
						errorText = TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION;
					break;

					case RC_ERROR_FIRST_NAME_LENGTH:
					break;

					case RC_ERROR_LAST_NAME_LENGTH:					
					break;
				}
				
				if ( errorText < 0 ) {
					// usu�rio registrado com sucesso
				} else {
					if ( errorEntryIndex >= 0 ) {
						textBoxes[ errorEntryIndex ].getBorder().setState( Border.STATE_ERROR );
						NanoOnline.getForm().requestFocus( textBoxes[ errorEntryIndex ] );
					}
					
					NanoOnline.getProgressBar().showError( errorText );
				}

				System.out.print( "data(" + data.length + "): " );
				for ( int i = 0; i < data.length; ++i ) {
					System.out.print( ( int ) data[ i ] );
					System.out.print( ", " );
				}
				System.out.println();
				System.out.println( new String( data ) );
			} catch ( IOException ex ) {
				//#if DEBUG == "true"
//# 				ex.printStackTrace();
				//#endif
			} finally {
				if ( input != null ) {
					try {
						input.close();
					} catch ( IOException ex ) {
					}
				}
			}
		}
		NanoOnline.getProgressBar().processData( id, data );
	}


	public void onInfo( int id, int infoIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onInfo( id, infoIndex, extraData );
			switch ( infoIndex ) {
				case ConnectionListener.INFO_CONNECTION_ENDED:
					onConnectionEnded( false );
				break;
			}
		}
	}


	public void onError( int id, int errorIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onError( id, errorIndex, extraData );
			onConnectionEnded( false );
		}
	}
	
	
	/**
	 * Valida os campos de texto antes de enviar ao servidor.
	 * 
	 * @return <code>true</code>, caso estejam todos preenchidos da maneira correta, e <code>false</code> caso contr�rio.
	 */
	private final boolean validate() {
		byte firstError = -1;
		byte errorMessage = -1;
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setText( textBoxes[ i ].getText().trim(), false );
		}
		
		String text = textBoxes[ ENTRY_NICKNAME ].getText();
		if ( text.length() < NICKNAME_MIN_CHARS ) {
			firstError = ENTRY_NICKNAME;
			errorMessage = TEXT_REGISTER_ERROR_NICKNAME_LENGTH;
			textBoxes[ ENTRY_NICKNAME ].getBorder().setState( Border.STATE_ERROR );
		} else {
			// garante que primeiro caracter � v�lido (a-z, A-Z)
			if ( !isAlpha( text.charAt( 0 ) ) ) {
				firstError = ENTRY_NICKNAME;
				errorMessage = TEXT_REGISTER_ERROR_NICKNAME_FORMAT;
				textBoxes[ ENTRY_NICKNAME ].getBorder().setState( Border.STATE_ERROR );
			}
		}
		
		text = textBoxes[ ENTRY_EMAIL ].getText();
		if ( text.length() >= EMAIL_MIN_CHARS ) {
			// garante que primeiro caracter seja v�lido (a-z, A-Z)
			if ( !isAlpha( text.charAt( 0 ) ) ) {
				if ( firstError < 0 ) {
					firstError = ENTRY_EMAIL;
					errorMessage = TEXT_REGISTER_ERROR_EMAIL_FORMAT;
				}
				textBoxes[ ENTRY_EMAIL ].getBorder().setState( Border.STATE_ERROR );
			}
			
			if ( text.indexOf( '@' ) < 0 ) {
				if ( firstError < 0 ) {
					firstError = ENTRY_EMAIL;				
					errorMessage = TEXT_REGISTER_ERROR_EMAIL_FORMAT;
				}
				textBoxes[ ENTRY_EMAIL ].getBorder().setState( Border.STATE_ERROR );
			}
		}
		
		text = textBoxes[ ENTRY_PASSWORD ].getText();
		final String confirm = textBoxes[ ENTRY_PASSWORD_CONFIRM ].getText();
		if ( text.length() >= PASSWORD_MIN_CHARS ) {
			boolean hasAlpha = false;
			boolean hasDigit = false;
			boolean hasSpecial = false;
			
			final char[] textChars = text.toCharArray();
			for ( short i = 0; i < textChars.length; ++ i ) {
				if ( !hasAlpha && isAlpha( textChars[ i ] ) ) {
					hasAlpha = true;
					if ( hasDigit )
						break;
				} else if ( !hasDigit && textChars[ i ] >= '0' && textChars[ i ] <= '9' ) {
					hasDigit = true;
					if ( hasAlpha )
						break;
				}
//				else if ( !hasSpecial ) {
//					switch ( textChars[ i ] ) {
//						case '_':
//						break;
//					}
//				}
			}
			
			if ( hasAlpha && hasDigit ) {
				if ( text.compareTo( confirm ) != 0 ) {
					// passwords n�o conferem
					if ( firstError < 0 ) {
						firstError = ENTRY_PASSWORD_CONFIRM;
						errorMessage = TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION;
					}
					textBoxes[ ENTRY_PASSWORD_CONFIRM ].getBorder().setState( Border.STATE_ERROR );
				}
			} else {
				// password muito fraca
				if ( firstError < 0 ) {
					firstError = ENTRY_PASSWORD;				
					errorMessage = TEXT_REGISTER_ERROR_PASSWORD_WEAK;
				}
				textBoxes[ ENTRY_PASSWORD ].getBorder().setState( Border.STATE_ERROR );
			}
		} else {
			// password muito curta
			if ( firstError < 0 ) {
				firstError = ENTRY_PASSWORD;			
				errorMessage = TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT;
			}
			textBoxes[ ENTRY_PASSWORD ].getBorder().setState( Border.STATE_ERROR );
		}
		
		if ( firstError >= 0 ) {
			textBoxes[ firstError ].getBorder().setState( Border.STATE_ERROR );
			NanoOnline.getProgressBar().showError( errorMessage );
			NanoOnline.getForm().requestFocus( textBoxes[ firstError ] );
		}
		
		// se o �ndice ainda for negativo, � porque n�o houve erro na valida��o
		return firstError < 0;
	}
	
	
	private final boolean isAlpha( char c ) {
		// TODO criar hashtable que mapeia '�', '�', '�', '�' para 'a', etc.
		return ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' );
	}
	
	
	/**
	 * 
	 */
	private final void onConnectionEnded( boolean forceCancel ) {
		if ( forceCancel ) {
			NanoConnection.cancel( currentConnection );
		}
		currentConnection = -1;
		
		setInConnection( false );
	}
	
	
	public void setFocus( boolean focus ) {
		super.setFocus( focus );
		
		if ( currentConnection < 0 && focus ) {
			onConnectionEnded( false );
		}
	}


	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
						onBack();
					break;
					
					case ENTRY_BUTTON_EDIT:
						setEditable();
					break;
					
					case ENTRY_BUTTON_ERASE:
						NanoOnline.removeProfile( profileIndex );
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT );
					break;
					
					case ENTRY_BUTTON_USE:
						NanoOnline.setCurrentCustomerIndex( profileIndex );
						NanoOnline.setScreen( SCREEN_MAIN_MENU );
					break;
					
					case ENTRY_BUTTON_OK:
						if ( validate() ) {
							try {
								final ByteArrayOutputStream b = new ByteArrayOutputStream();
								final DataOutputStream out = new DataOutputStream( b );

								// escreve os dados globais antes de adicionar as informa��es espec�ficas para registro do usu�rio
								NanoOnline.writeGlobalData( out );

								out.writeByte( ENTRY_NICKNAME );
								out.writeUTF( textBoxes[ENTRY_NICKNAME].getText() );

								out.writeByte( ENTRY_EMAIL );
								out.writeUTF( textBoxes[ENTRY_EMAIL].getText() );

								out.writeByte( ENTRY_PASSWORD );
								out.writeUTF( textBoxes[ENTRY_PASSWORD].getText() );

								out.writeByte( ENTRY_PASSWORD_CONFIRM );
								out.writeUTF( textBoxes[ ENTRY_PASSWORD_CONFIRM ].getText() );


								// se o usu�rio j� estiver registrado, apenas atualiza suas informa��es
								if ( NanoOnline.getCurrentCustomer().isRegistered() ) {
									out.writeByte( ID_CUSTOMER_ID );
									out.writeInt( NanoOnline.getCurrentCustomer().getId() );
									out.flush();

									currentConnection = NanoConnection.put( NANO_ONLINE_ADDRESS + "customers", b.toByteArray(), this, false );
								} else {
									out.flush();
									currentConnection = NanoConnection.post( NANO_ONLINE_ADDRESS + "customers", b.toByteArray(), this, false );
								}
								setInConnection( true );
							} catch ( IOException ex ) {
								//#if DEBUG == "true"
//# 									ex.printStackTrace();
								//#endif
							}
						}
					break;
					
					case ENTRY_BUTTON_CANCEL:
					case ENTRY_BUTTON_BACK:
						onBack();
					break;
				}
			break;
			
			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
			break;
			
			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				switch ( sourceId ) {
					case ENTRY_NICKNAME:
					case ENTRY_EMAIL:
					case ENTRY_PASSWORD:
					case ENTRY_PASSWORD_CONFIRM:
						if ( evt.source.getBorder().getState() != Border.STATE_ERROR )
							evt.source.getBorder().setState( evt.eventType == Event.EVT_FOCUS_GAINED ? Border.STATE_FOCUSED : Border.STATE_UNFOCUSED );

						// se estiver no modo edit�vel, altera o label da soft key direita ao dar foco a um dos textBox
						if ( buttonCancel.getId() == ENTRY_BUTTON_CANCEL && currentConnection < 0 )
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CLEAR ) );
					break;
					
					default:
						if ( currentConnection < 0 )
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
//						switch ( sourceId ) {
//							case ENTRY_BUTTON_CANCEL:
//							case ENTRY_BUTTON_OK:
//								keyPressed( key );
//							break;
//							
//							default:
								onBack();
//							break;
//						} // fim switch ( sourceId )
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do m�todo eventPerformed( Event )
	
	
	protected final void onBack() {
		if ( currentConnection >= 0 )
			onConnectionEnded( true );
		else
			super.onBack();
	}
	
	
	/**
	 * 
	 * @param inConnection
	 */
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setEnabled( !inConnection );
		}
		buttonEditOK.setEnabled( !inConnection );
		buttonCancel.setEnabled( !inConnection );
		
		if ( inConnection ) {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CANCEL ) );
		} else {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
			NanoOnline.getProgressBar().clearProgressBar();
		}
	}
	
	
	/**
	 * 
	 */
	private final void setEditable() {
		buttonEditOK.setText( NanoOnline.getText( TEXT_OK ) );
		buttonEditOK.setId( ENTRY_BUTTON_OK );
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setEnabled( true );
		}		
		
		buttonCancel.setText( NanoOnline.getText( TEXT_CANCEL ) );
		buttonCancel.setId( ENTRY_BUTTON_CANCEL );
		
		textBoxes[ ENTRY_NICKNAME ].requestFocus();
		
		if ( !newProfile ) {
			removeDrawable( buttonUse );
			removeDrawable( buttonErase );
			refreshLayout();
		}
	}
	
	
	/**
	 * 
	 * @param index
	 */
	public static final void setProfileIndex( int index ) {
		profileIndex = ( byte ) index;
	}

}
