/**
 * ProgressBar.java
 * 
 * Created on 16/Dez/2008, 17:06:42
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author Peter
 */
public class ProgressBar extends Container implements NanoOnlineConstants, ConnectionListener {

	/** Tipo de mensagem exibida pela barra de progresso: informa��o (cinza). */
	public static final byte MSG_TYPE_INFO			= 0;

	/** Tipo de mensagem exibida pela barra de progresso: aviso (amarelo). */
	public static final byte MSG_TYPE_WARNING		= 1;

	/** Tipo de mensagem exibida pela barra de progresso: confirma��o (verde). */
	public static final byte MSG_TYPE_CONFIRMATION	= 2;

	/** Tipo de mensagem exibida pela barra de progresso: erro (vermelho). */
	public static final byte MSG_TYPE_ERROR			= 3;

	private static final int[] COLORS = { 0x9999bb,
										  0xffff00,
										  0x00ff00,
										  0xff0000 };
	
	/** �ndice da tecla da soft key direita. */
	public static final byte ID_SOFT_RIGHT = -123;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private static final short KILOBYTE = 1 << 10;
	
	/** Label que indica o status atual. */
	private final MarqueeLabel label;
	
	/** Cont�iner do label que indica o status atual. */
	private final DrawableComponent labelComponent;
	
	/***/
	private final Container barContainer;
	
	/***/
	private final DrawableComponent barRead;
	
	/***/
	private final DrawableComponent barTotal;
	
	/***/
	public final Button softkeyRight;
	
	/** Total de bytes a baixar. */
	private int contentLengthTotal;
	
	/** bytes baixados at� o momento. */
	private int contentLengthRead;
	
	
	public ProgressBar() throws Exception {
		super( TOTAL_ITEMS, new BorderLayout() );
		
		setPreferredSize( size );
		
		// container da barra de progresso do download
		barContainer = new Container( 3 );
		insertDrawable( barContainer, BorderLayout.CENTER );
		
		barRead = new DrawableComponent( new Pattern( 0xff0000 ) );
		barContainer.insertDrawable( barRead );
		
		barTotal = new DrawableComponent( new Pattern( COLORS[ MSG_TYPE_INFO ] ) );
		barContainer.insertDrawable( barTotal );
		
		
		label = new MarqueeLabel( NanoOnline.getFont( FONT_DEFAULT ), null );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		labelComponent = new DrawableComponent( label );
		barContainer.insertDrawable( labelComponent );
		
		softkeyRight = new Button( NanoOnline.getFont( FONT_DEFAULT ), null );
		softkeyRight.setFocusable( false );
		softkeyRight.addActionListener( new EventListener() {
			public void eventPerformed( Event evt ) {
				switch ( evt.eventType ) {
					case Event.EVT_POINTER_PRESSED:
						NanoOnline.getForm().keyPressed( ScreenManager.KEY_SOFT_RIGHT );
					break;
					
					case Event.EVT_POINTER_RELEASED:
					case Event.EVT_BUTTON_CONFIRMED:
						NanoOnline.getForm().keyReleased( ScreenManager.KEY_SOFT_RIGHT );
					break;
				}
			}
		} );
		insertDrawable( softkeyRight, BorderLayout.EAST );

		final LineBorder b = new LineBorder( 0x559955 );
		b.setType( LineBorder.TYPE_ETCHED_RAISED );
		softkeyRight.setBorder( b );
		
		barContainer.setBorder( b.getCopy() );
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		refreshProgress();
	}


	public String getUIID() {
		return "progressbar";
	}


	public void processData( int id, byte[] data ) {
	}


	public void onInfo( int id, int infoIndex, Object extraData ) {
		System.out.println( "onInfo( " + id + ", " + infoIndex + ", " + ( extraData == null ? "" : extraData.toString() ) + ")");
		final StringBuffer buffer = new StringBuffer();
		switch ( infoIndex ) {
			case INFO_CONNECTION_OPENED:
				buffer.append( NanoOnline.getText( TEXT_CONNECTION_OPENED ) );
			break;

			case INFO_OUTPUT_STREAM_OPENED:
			case INFO_DATA_WRITTEN:
				buffer.append( NanoOnline.getText( TEXT_SENDING_DATA ) );
			break;

			case INFO_RESPONSE_CODE:
				buffer.append( NanoOnline.getText( TEXT_RESPONSE_CODE ) );
				buffer.append( ( ( Integer ) extraData ).intValue() );
			break;

			case INFO_INPUT_STREAM_OPENED:
				buffer.append( NanoOnline.getText( TEXT_RECEIVING_DATA ) );
			break;

			case INFO_CONTENT_LENGTH_TOTAL:
				contentLengthTotal = ( ( Integer ) extraData ).intValue();
				refreshProgress();
				refreshLabel( buffer );
			break;

			case INFO_CONTENT_LENGTH_READ:
				contentLengthRead = ( ( Integer ) extraData ).intValue();
				refreshProgress();

				buffer.append( NanoOnline.getText( TEXT_RECEIVING_DATA ) );
				refreshLabel( buffer );
			break;

			case INFO_CONNECTION_ENDED:
				buffer.append( "BLA" );
			break;
		}
		
		label.setText( buffer.toString(), false );
		refreshLayout();
	}


	public void onError( int id, int errorIndex, Object extraData ) {
		switch ( errorIndex ) {
			case ERROR_HTTP_CONNECTION_NOT_SUPPORTED:
			case ERROR_CANT_GET_RESPONSE_CODE:
			case ERROR_URL_BAD_FORMAT:
			case ERROR_CONNECTION_EXCEPTION:
			case ERROR_CANT_CLOSE_INPUT_STREAM:
			case ERROR_CANT_CLOSE_CONNECTION:
			case ERROR_CANT_CLOSE_OUTPUT_STREAM:
				label.setText( "ERROR INDEX: " +  errorIndex + " - " + ( ( Exception ) extraData ).getMessage(), false );
			break;
		}
	}
	
	
	private final void refreshProgress() {
		if ( contentLengthTotal > 0 ) {
			barRead.setSize( barContainer.getWidth() * contentLengthRead / contentLengthTotal, barContainer.getHeight() );
		} else {
			barRead.setSize( 0, barContainer.getHeight() );
		}
		barTotal.setSize( barContainer.getWidth() - barRead.getWidth(), barContainer.getHeight() );
		barTotal.setPosition( barRead.getWidth(), 0 );
	}
	
	
	private final void refreshLabel( StringBuffer buffer ) {
		buffer.append( ' ' );
		if ( contentLengthTotal < NanoMath.MAX_INTEGER_VALUE ) {
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthRead, KILOBYTE ) ) );
			buffer.append( '/' );
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthTotal, KILOBYTE ) ) );

			buffer.append( " kB" );
		} else {
			// ultrapassa o valor m�ximo permitido no fixedPoint, ent�o arredonda o valor
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthRead / KILOBYTE, KILOBYTE ), 4 ) );
			buffer.append( '/' );
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthTotal / KILOBYTE, KILOBYTE ), 4 ) );

			buffer.append( "MB" );		
		}
		
		// TODO adicionar "..." indicando progresso
//		try {
//			Thread.sleep( 40 ); // TODO reduz velocidade para poder acompanhar barra de download no emulador
//		} catch ( InterruptedException ex ) {
//			ex.printStackTrace();
//		}
	}
	
	
	public final void setSoftKey( String text ) {
		// TODO otimizar chamadas - defini��o de setSoftKey pode resultar em at� 4 chamadas de setSize
		softkeyRight.setText( text );
		refreshLayout();
	} // fim do m�todo setSoftKey( byte, Drawable, boolean, int )	


	public Point calcPreferredSize( Point maximumSize ) {
		return super.calcPreferredSize( maximumSize );
	}


	public final void showError( int errorIndex ) {
		showMessage( MSG_TYPE_ERROR, errorIndex );
	}

	
	public final void showInfo( int errorIndex ) {
		showMessage( MSG_TYPE_INFO, errorIndex );
	}


	public final void showWarning( int errorIndex ) {
		showMessage( MSG_TYPE_WARNING, errorIndex );
	}


	public final void showConfirmation( int errorIndex ) {
		showMessage( MSG_TYPE_CONFIRMATION, errorIndex );
	}


	public final void showMessage( byte type, int msgIndex ) {
		label.setText( NanoOnline.getText( msgIndex ), false );
		( ( Pattern ) barTotal.d ).setFillColor( COLORS[ type ] );
		refreshProgress();
	}


	public void refreshLayout() {
		super.refreshLayout();
		
		labelComponent.setSize( barContainer.getLayoutSize() );
	}
	
	
	public final void clearProgressBar() {
		contentLengthRead = 0;
		contentLengthTotal = 0;
		refreshProgress();
	}
	
}
