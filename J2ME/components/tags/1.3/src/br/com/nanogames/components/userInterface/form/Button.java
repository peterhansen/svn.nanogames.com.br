/**
 * Button.java
 * 
 * Created on 6/Nov/2008, 17:15:41
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;

/**
 *
 * @author Peter
 */
public class Button extends FormLabel {

	/***/
	public static final byte STATE_DEFAULT	= 0;

	/***/
	public static final byte STATE_PRESSED	= 1;

	/***/
	public static final byte STATE_ROLLOVER = 2;

	/***/
	protected byte state;


	public Button( ImageFont font, String text ) throws Exception {
		super( font, text );

		setFocusable( true );
	}

	
	public String getUIID() {
		return "button";
	}


	public void keyPressed( int key ) {
		super.keyPressed( key );

		if ( isEnabled() ) {
			switch ( key ) {
				case ScreenManager.KEY_NUM5:
				case ScreenManager.FIRE:
					setState( STATE_PRESSED );
				break;
			}
		}
	}


	public void keyReleased( int key ) {
		super.keyReleased( key );

		if ( isEnabled() ) {
			switch ( key ) {
				case ScreenManager.KEY_NUM5:
				case ScreenManager.FIRE:
					setState( STATE_ROLLOVER );
					dispatchActionEvent( new Event( this, Event.EVT_BUTTON_CONFIRMED ) );
				break;
			}
		}
	}


	public void onPointerDragged( int x, int y ) {
		super.onPointerDragged( x, y );

		if ( isEnabled() ) {
			if ( contains( x, y ) ) {
				setState( STATE_PRESSED );
			} else {
				setState( STATE_ROLLOVER );
			}
		}
	}


	public void onPointerPressed( int x, int y ) {
		super.onPointerPressed( x, y );

		if ( isEnabled() && contains( x, y ) ) {
			setState( STATE_PRESSED );
		}
	}


	public void onPointerReleased( int x, int y ) {
		super.onPointerReleased( x, y );

		if ( isEnabled() ) {
			switch ( state ) {
				case STATE_PRESSED:
					setState( STATE_ROLLOVER );
					if ( contains( x, y ) ) {
						dispatchActionEvent( new Event( this, Event.EVT_BUTTON_CONFIRMED ) );
					}
				break;
			}
		}
	}


	public void setFocus( boolean focus ) {
		super.setFocus( focus );

		if ( focus ) {
			setState( STATE_ROLLOVER );
		} else {
			setState( STATE_DEFAULT );
		}

	}


	protected void setState( int state ) {
		this.state = ( byte ) state;

		if ( border != null ) {
			switch ( state ) {
				case STATE_DEFAULT:
					border.setState( Border.STATE_UNFOCUSED );
				break;

				case STATE_PRESSED:
					border.setState( Border.STATE_PRESSED );
				break;

				case STATE_ROLLOVER:
					border.setState( Border.STATE_FOCUSED );
				break;
			}
		}
	}


}
