/**
 * Event.java
 * 
 * Created on Dec 13, 2008, 1:27:03 PM
 *
 */
package br.com.nanogames.components.userInterface.form.events;

import br.com.nanogames.components.userInterface.form.Component;


/**
 *
 * @author Peter
 */
public class Event {

	/** Evento disparado quando o componente ganha foco. */
	public static final byte EVT_FOCUS_GAINED			= 0;

	/** Evento disparado quando o componente perde o foco. */
	public static final byte EVT_FOCUS_LOST				= 1;

	/**
	 * Evento disparado quando o componente recebe um evento de tecla pressionada.
	 * <p><code>data</code>: <code>Integer</code> contendo o c�digo da tecla pressionada.</p>
	 */
	public static final byte EVT_KEY_PRESSED			= 2;

	/**
	 * Evento disparado quando o componente recebe um evento de tecla solta.
	 * <p><code>data</code>: <code>Integer</code> contendo o c�digo da tecla solta.</p>
	 */
	public static final byte EVT_KEY_RELEASED			= 3;

	/** Evento disparado quando o componente recebe um evento de ponteiro pressionado. */
	public static final byte EVT_POINTER_PRESSED		= 4;

	/** Evento disparado */
	public static final byte EVT_POINTER_RELEASED		= 5;

	/** Evento disparado */
	public static final byte EVT_POINTER_DRAGGED		= 6;

	/** Evento disparado quando um bot�o � confirmado. */
	public static final byte EVT_BUTTON_CONFIRMED		= 7;

	/** Evento disparado por um TextBox ao se pressionar as teclas CLEAR, BACK ou soft key direita com o texto vazio. */
	public static final byte EVT_TEXTBOX_BACK			= 8;
	

	/**
	 * Valor utilizado no caso de um evento com tipo n�o definido.
	 * @see #Event(Object)
	 */
	public static final int EVENT_TYPE_NONE = Integer.MIN_VALUE;

	/** Indica se o evento j� foi consumido. */
	private boolean consumed;

	/** Objeto de origem do evento. */
	public final Component source;

	/** Identificador do tipo de evento. */
	public final int eventType;

	/** Dados extras do evento (opcional). */
	public final Object data;


	/**
	 * Cria uma nova inst�ncia de Event. Equivalente ao construtor <code>Event( source, EVENT_TYPE_NONE, null )</code>.
	 * @param source element for the action event
	 * @see #Event(Object, int)
	 * @see #Event(Object, int, Object)
	 * @see #EVENT_TYPE_NONE
	 */
	public Event( Component source ) {
		this( source, EVENT_TYPE_NONE, null );
	}


	/**
	 * Cria uma nova inst�ncia de Event. Equivalente ao construtor <code>Event( source, eventType, null )</code>.
	 * @param source element for the action event
	 * @param eventType identificador do tipo de evento.
	 * @see #Event(Object)
	 * @see #Event(Object, int, Object)
	 */
	public Event( Component source, int eventType ) {
		this( source, eventType, null );
	}


	/**
	 * Cria uma nova inst�ncia de Event.
	 * @param source objeto de origem do evento.
	 * @param eventType identificador do tipo de evento.
	 * @param data dados extras do evento (opcional).
	 * @see #Event(Object)
	 * @see #Event(Object, int)
	 */
	public Event( Component source, int eventType, Object data ) {
		this.source = source;
		this.eventType = eventType;
		this.data = data;
	}

	
	/**
	 * Consume the event indicating that it was handled thus preventing other action
	 * listeners from handling/receiving the event
	 */
	public void consume() {
		consumed = true;
	}


	/**
	 * Returns true if the event was consumed thus indicating that it was handled.
	 * This prevents other action listeners from handling/receiving the event.
	 */
	public boolean isConsumed() {
		return consumed;
	}


}
