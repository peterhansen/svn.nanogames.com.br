/*
 * ImageFont.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.ImageLoader;
import java.io.DataInputStream;
import java.io.InputStream;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class ImageFont {
 
	/** N�mero de caracteres dispon�veis. */
	protected static final short CHAR_TABLE_SIZE = 256;
	
	/** Imagem contendo os caracteres da fonte. */
	protected final Image image;
	
	/** Largura de cada caracter. */
	protected final byte[] charsWidths = new byte[ CHAR_TABLE_SIZE ];
	
	/** Posi��o inicial (x) de cada caracter na imagem. */
    protected final short[] charsOffsets = new short[ CHAR_TABLE_SIZE ];
	
	/** Offset extra entre os caracteres (utilizado para se aumentar ou reduzir o espa�amento padr�o entre eles). */
	protected byte charOffset;
	 
	
	protected ImageFont( Image image ) {
		this.image = image;
	}
	

	/**
	 * Cria uma nova inst�ncia de fonte monoespa�ada.
	 * @param imagePath endere�o da imagem que cont�m os caracteres da fonte.
	 * @param characters caracteres presentes na imagem.
	 * @return refer�ncia para a fonte criada, ou null caso haja erro ao alocar recursos ou par�metros inv�lidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMonoSpacedFont( String imagePath, String characters ) throws Exception {
		return createMonoSpacedFont( ImageLoader.loadImage( imagePath ), characters );
	}	
	
	
	/**
	 * Cria uma nova inst�ncia de fonte monoespa�ada.
	 * @param image imagem que cont�m os caracteres da fonte.
	 * @param characters caracteres presentes na imagem.
	 * @return refer�ncia para a fonte criada, ou null caso haja erro ao alocar recursos ou par�metros inv�lidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMonoSpacedFont( Image image, String characters ) throws Exception {
		ImageFont font = new ImageFont( image );

		// se a divis�o da largura da imagem pelo n�mero de caracteres n�o for exata, retorna erro
		if ( image.getWidth() % characters.length() != 0 ) {
			//#if DEBUG == "true"
//# 					throw new Exception( "Erro ao criar fonte monoespa�ada: largura da imagem( " + image.getWidth() + ") e o n�mero de caracteres (" + characters.length() + ") n�o resultam numa divis�o exata." );
			//#else
				throw new Exception();
			//#endif
		}

		// pr�-calcula a largura dos caracteres
		final byte charWidth = ( byte ) ( image.getWidth() / characters.length() );
		final char[] chars = new char[ characters.length() ];
		characters.getChars( 0, characters.length(), chars, 0 );

		// preenche o array de tamanhos de caracteres
		for ( int i = 0; i < chars.length; ++i ) {
			font.charsWidths[ chars[ i ] ] = charWidth;
		}
		font.calculateOffsets( chars );

		return font;
	} // fim do m�todo createMonoSpacedFont()

	
	/**
	 * Cria uma nova inst�ncia de fonte multiespa�ada.
	 * @param imagePath caminho da imagem que cont�m os caracteres da fonte.
	 * @param fontDataFile endere�o do arquivo bin�rio que descreve a tira de fonte.
	 * @return refer�ncia para a fonte criada, ou null caso haja erro ao alocar recursos ou par�metros inv�lidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMultiSpacedFont( String imagePath, String fontDataFile ) throws Exception {
		return createMultiSpacedFont( ImageLoader.loadImage( imagePath ), fontDataFile );
	}	
	
	
	/**
	 * Cria uma nova inst�ncia de fonte multiespa�ada.
	 * @param image imagem que cont�m os caracteres da fonte.
	 * @param fontDataFile endere�o do arquivo bin�rio que descreve a tira de fonte.
	 * @return refer�ncia para a fonte criada, ou null caso haja erro ao alocar recursos ou par�metros inv�lidos.
	 * @throws java.lang.Exception 
	 */
	public static final ImageFont createMultiSpacedFont( Image image, String fontDataFile ) throws Exception {
		InputStream input = null;
		DataInputStream dataInput = null;
			
		try {
			ImageFont font = new ImageFont( image );
			
			input = font.getClass().getResourceAsStream( fontDataFile );
			dataInput = new DataInputStream( input );
			
			// obt�m o n�mero total de caracteres da fonte
			final short numberOfChars = ( short ) dataInput.readUnsignedByte();
			final byte[] tempCharsWidth = new byte[ numberOfChars ];
			for ( int i = 0; i < numberOfChars; ++i ) {
				tempCharsWidth[ i ] = ( byte ) dataInput.readUnsignedByte();
			}
			
			
			char c;
			// esse array tempor�rio � utilizado para c�lculo posterior dos offsets de cada caracter, 
			// passando-o para o m�todo calculateOffsets()
			final char[] chars = new char[ numberOfChars ];
			// armazena as larguras de cada caracter
			for ( int i = 0; i < numberOfChars; ++i ) {
				c = ( char ) ( dataInput.readUnsignedByte() );
				chars[ i ] = c;
				font.charsWidths[ c ] = tempCharsWidth[ i ];
			}
			
			font.calculateOffsets( chars );

			System.gc();
			return font;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			throw e;
		} finally {
			try {
//				if ( input != null )
//					input.close();
				if ( dataInput != null )
					dataInput.close();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}
		}
	} // fim do m�todo createMultiSpacedFont()
	
	
	/**
	 *	M�todo auxiliar para calcular e atualizar os offsets de cada caracter na fonte.
	 * 
	 * @param characters 
	 */
	protected final void calculateOffsets( char[] characters ) {
		short offset = 0;
		for ( int i = 0; i < characters.length; ++i ) {
			charsOffsets[ characters[ i ] ] = offset;
			offset += charsWidths[ characters[ i ] ];
		}
	} // fim do m�todo calculateOffsets( char[] )


	/**
	 * Retorna a largura em pixels de um texto.
	 *
	 * @param text �ndice do texto cuja largura ser� calculada.
	 * @return largura do texto em pixels.
	 * @throws NullPointerException caso textIndex seja inv�lido.
	 * @see #getTextWidth(char[])
	 * @see #getTextWidth(String)
	 */
	public final int getTextWidth( int textIndex ) {
		return getTextWidth( AppMIDlet.getText( textIndex ) );
	}
	
	
	/**
	 * Retorna a largura em pixels de um texto.
	 * 
	 * @param text texto cuja largura ser� calculada.
	 * @return largura do texto em pixels.
	 * @throws NullPointerException caso text seja null.
	 * @see #getTextWidth(char[])
	 * @see #getTextWidth(int)
	 */
	public final int getTextWidth( String text ) {
		return getTextWidth( text.toCharArray() );
	} // fim do m�todo getTextWidth( String )
	
	
	/**
	 * Retorna a largura em pixels de um texto.
	 * 
	 * @param text texto cuja largura ser� calculada.
	 * @return largura do texto em pixels.
	 * @throws NullPointerException caso text seja null.
	 * @see #getTextWidth(String)
	 * @see #getTextWidth(int)
	 */	
	public int getTextWidth( char[] text ) {
		int width = 0;

		for ( int i = 0; i < text.length; ++i )
			width += getCharWidth( text[ i ] );

		// a largura � corrigida para que o �ltimo caracter seja exibido corretamente
		return width > 0 ? width - charOffset : 0;
	}
	
	
	/**
	 * Retorna a largura de um caracter da fonte.
	 * @param c caracter cuja largura ser� retornada.
	 * @return largura do caracter em pixels.
	 */
	public int getCharWidth( char c ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif
				
		return charsWidths[ c ] > 0 ? charsWidths[ c ] + charOffset : 0;
			
		//#if DEBUG == "true"	
//# 			} catch ( Exception e ) {
//# 			System.err.println( "Caracter inv�lido: " + c + ", " + ( int ) c );
//# 			e.printStackTrace();
//# 				return 0;
//# 			}
		//#endif
	} // fim do m�todo getCharWidth( char )	 
	
	
	public final Image getImage() {
		return image;
	}
	
	
	/**
	 * Obt�m a altura da fonte.
	 * 
	 * @return inteiro indicando a altura dos caracteres da fonte.
	 */
	public final int getHeight() {
		return image.getHeight();
	}
	
	
	public final void setCharOffset( int offset ) {
		charOffset = ( byte ) offset;
	}
	
	
	public final byte getCharOffset() {
		return charOffset;
	}
}
 
