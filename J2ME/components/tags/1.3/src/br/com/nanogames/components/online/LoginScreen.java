/**
 * RegisterScreen2.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.EditLabel;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 *
 * @author Peter
 */
public class LoginScreen extends NanoOnlineContainer implements ConnectionListener {

	// <editor-fold defaultstate="collapsed" desc="C�DIGOS DE RETORNO DO LOGIN DE USU�RIO J� EXISTENTE">

	// ATEN��O: ESSES C�DIGOS DEVEM *SEMPRE* ESTAR DE ACORDO COM OS C�DIGOS RETORNADOS PELO SERVIDOR!

	// obs.: c�digo de retorno zero � OK (RC_OK)
	
	/** C�digo de retorno do login de usu�rio: erro: usu�rio n�o encontrado. */
	private static final byte RC_ERROR_NICKNAME_NOT_FOUND		= 1;
	
	/** C�digo de retorno do login de usu�rio: erro: password inv�lida. */
	private static final byte RC_ERROR_PASSWORD_INVALID			= 2;
	
	// </editor-fold>
	
	/***/
	private static final byte ENTRY_NICKNAME				= 0;
	/***/
	private static final byte ENTRY_PASSWORD				= ENTRY_NICKNAME + 1;
	
	// TODO adicionar checkbox "lembrar de mim"
	
	/***/
	private static final byte ENTRY_BUTTON_OK				= ENTRY_PASSWORD + 1;
	/***/
	private static final byte ENTRY_BUTTON_BACK			= ENTRY_BUTTON_OK + 1;
	
	/***/
	private static final byte ENTRIES_TOTAL = ( ENTRY_BUTTON_BACK + 1 ) << 1;
	
	/** Quantidade m�nima de caracteres do apelido. */
	private static final byte NICKNAME_MIN_CHARS = 4;
	/** Quantidade m�nima de caracteres da password. */
	private static final byte PASSWORD_MIN_CHARS = 6;
	
	/***/
	private static final byte EDIT_LABELS_TOTAL = 2;
	
	/***/
	private final TextBox[] textBoxes = new TextBox[ EDIT_LABELS_TOTAL ];	
	
	/***/
	private final Button buttonOK;
	
	/***/
	private final Button buttonBack;

	/***/
	private int currentConnection = -1;
	
	
	public LoginScreen( int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );
		
		setBackIndex( backIndex >= 0 ? backIndex : SCREEN_PROFILE_SELECT );
		
		final ImageFont font = NanoOnline.getFont( FONT_DEFAULT );
		
		final byte[] MAX_CHARS = { 20, 20 };
		final byte[] INPUT_MODE = { EditLabel.INPUT_MODE_ANY, EditLabel.INPUT_MODE_PASSWORD };
		final byte[] titles = { TEXT_NICKNAME, TEXT_PASSWORD };
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			final TextBox textBox = new TextBox( font, null, MAX_CHARS[ i ], INPUT_MODE[ i ], true );
			textBoxes[ i ] = textBox;
			
			textBox.setCaret( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "cursor.png" ) );
			textBox.setId( i );
			textBox.addActionListener( this );
			textBox.setPasswordMask( '@' );
			textBox.setBorder( NanoOnline.getTitledBorder( titles[ i ] ) );
			
			final Customer customer = NanoOnline.getCurrentCustomer();
			if ( customer != null ) {
				switch ( i ) {
					case ENTRY_NICKNAME:
						textBox.setText( customer.getNickname(), false );
					break;

					case ENTRY_PASSWORD:
						textBox.setText( customer.getPassword(), false );
					break;
				}
			}
			
			insertDrawable( textBox );
		}
		
		buttonOK = new Button( font, NanoOnline.getText( TEXT_OK ) );
		buttonOK.setId( ENTRY_BUTTON_OK );
		buttonOK.setBorder( NanoOnline.getBorder() );
		buttonOK.addActionListener( this );
		
		insertDrawable( buttonOK );
		
		buttonBack = new Button( font, NanoOnline.getText( TEXT_BACK ) );
		buttonBack.setId( ENTRY_BUTTON_BACK );
		buttonBack.addActionListener( this );
		buttonBack.setBorder( NanoOnline.getBorder() );
		insertDrawable( buttonBack );
	}
	

	public void processData( int id, byte[] data ) {
		onConnectionEnded( false );
		System.out.println( "processData: " + ( data == null ? -1 : data.length ) );
		if ( data != null ) {
			try {
				final Hashtable table = NanoOnline.readGlobalData( data );
				
				// o c�digo de retorno � obrigat�rio
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				byte errorEntryIndex = -1;
				byte errorText = -1;
				
				switch ( returnCode ) {
					case RC_OK:
						// grava as informa��es do usu�rio no RMS e mostra a tela de confirma��o
						final Customer customer = new Customer();
//						customer.email = textBoxes[ ENTRY_EMAIL ].getText();
//						customer.firstName = ;
//						customer.genre = ;
						// TODO ler da tabela o id do usu�rio!
						customer.setId( ( ( Integer ) table.get( new Byte( ID_CUSTOMER_ID ) ) ).intValue() );
//						customer.lastName = ;
						customer.setNickname( textBoxes[ ENTRY_NICKNAME ].getText() );
						customer.setPassword( textBoxes[ ENTRY_PASSWORD ].getText() );
//						customer.rememberPassword = ;
						
						NanoOnline.addProfile( customer );
						
						NanoOnline.getProgressBar().showConfirmation( TEXT_CUSTOMER_REGISTERED );
						onBack();
					break;
					
					case RC_SERVER_INTERNAL_ERROR:
						errorText = TEXT_ERROR_SERVER_INTERNAL;
					break;
					
					case RC_APP_NOT_FOUND:
						errorText = TEXT_ERROR_APP_NOT_FOUND;
					break;
					
					case RC_DEVICE_NOT_SUPPORTED:
						errorText = TEXT_ERROR_DEVICE_NOT_SUPPORTED;
					break;
					
					case RC_ERROR_NICKNAME_NOT_FOUND:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_LOGIN_ERROR_NICKNAME_NOT_FOUND;
					break;
					
					case RC_ERROR_PASSWORD_INVALID:
						errorEntryIndex = ENTRY_PASSWORD;
						errorText = TEXT_LOGIN_ERROR_PASSWORD_INVALID;
					break;
				}
				
				if ( errorText >= 0 ) {
					if ( errorEntryIndex >= 0 ) {
						textBoxes[ errorEntryIndex ].getBorder().setState( Border.STATE_ERROR );
						NanoOnline.getForm().requestFocus( textBoxes[ errorEntryIndex ] );
					}
					
					NanoOnline.getProgressBar().showError( errorText );
				}

				System.out.print( "data(" + data.length + "): " );
				for ( int i = 0; i < data.length; ++i ) {
					System.out.print( ( int ) data[ i ] );
					System.out.print( ", " );
				}
				System.out.println();
				System.out.println( new String( data ) );
			} catch ( IOException ex ) {
				//#if DEBUG == "true"
//# 				ex.printStackTrace();
				//#endif
			}
		}
		NanoOnline.getProgressBar().processData( id, data );
	}


	public void onInfo( int id, int infoIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onInfo( id, infoIndex, extraData );
			switch ( infoIndex ) {
				case ConnectionListener.INFO_CONNECTION_ENDED:
					onConnectionEnded( false );
				break;
			}
		}
	}


	public void onError( int id, int errorIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onError( id, errorIndex, extraData );
			onConnectionEnded( false );
		}
	}
	
	
	/**
	 * Valida os campos de texto antes de enviar ao servidor.
	 * 
	 * @return <code>true</code>, caso estejam todos preenchidos da maneira correta, e <code>false</code> caso contr�rio.
	 */
	private final boolean validate() {
		byte firstError = -1;
		byte errorMessage = -1;
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setText( textBoxes[ i ].getText().trim(), false );
		}
		
		String text = textBoxes[ ENTRY_NICKNAME ].getText();
		if ( text.length() < NICKNAME_MIN_CHARS ) {
			firstError = ENTRY_NICKNAME;
			errorMessage = TEXT_REGISTER_ERROR_NICKNAME_LENGTH;
			textBoxes[ ENTRY_NICKNAME ].getBorder().setState( Border.STATE_ERROR );
		} else {
			// garante que primeiro caracter � v�lido (a-z, A-Z)
			if ( !isAlpha( text.charAt( 0 ) ) ) {
				firstError = ENTRY_NICKNAME;
				errorMessage = TEXT_REGISTER_ERROR_NICKNAME_FORMAT;
				textBoxes[ ENTRY_NICKNAME ].getBorder().setState( Border.STATE_ERROR );
			}
		}
		
		text = textBoxes[ ENTRY_PASSWORD ].getText();
		if ( text.length() < PASSWORD_MIN_CHARS ) {
			// password muito curta
			if ( firstError < 0 ) {
				firstError = ENTRY_PASSWORD;			
				errorMessage = TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT;
			}
			textBoxes[ ENTRY_PASSWORD ].getBorder().setState( Border.STATE_ERROR );
		}
		
		if ( firstError >= 0 ) {
			textBoxes[ firstError ].getBorder().setState( Border.STATE_ERROR );
			NanoOnline.getProgressBar().showError( errorMessage );
			NanoOnline.getForm().requestFocus( textBoxes[ firstError ] );
		}
		
		// se o �ndice ainda for negativo, � porque n�o houve erro na valida��o
		return firstError < 0;
	}
	
	
	private final boolean isAlpha( char c ) {
		// TODO criar hashtable que mapeia '�', '�', '�', '�' para 'a', etc.
		return ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' );
	}
	
	
	/**
	 * 
	 */
	private final void onConnectionEnded( boolean forceCancel ) {
		if ( forceCancel ) {
			NanoConnection.cancel( currentConnection );
		}
		currentConnection = -1;
		
		setInConnection( false );
	}
	
	
	public void setFocus( boolean focus ) {
		super.setFocus( focus );
		
		if ( currentConnection < 0 && focus ) {
			onConnectionEnded( false );
		}
	}


	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		System.out.println( evt.eventType + ", " + sourceId );
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_BUTTON_BACK:
						checkBackButton();
					break;
					
					case ENTRY_BUTTON_OK:
						if ( validate() ) {
							try {
								final ByteArrayOutputStream b = new ByteArrayOutputStream();
								final DataOutputStream out = new DataOutputStream( b );

								// escreve os dados globais antes de adicionar as informa��es espec�ficas para registro do usu�rio
								NanoOnline.writeGlobalData( out );

								out.writeByte( ENTRY_NICKNAME );
								out.writeUTF( textBoxes[ ENTRY_NICKNAME ].getText() );

								out.writeByte( ENTRY_PASSWORD );
								out.writeUTF( textBoxes[ENTRY_PASSWORD].getText() );

								out.flush();
								currentConnection = NanoConnection.post( NANO_ONLINE_ADDRESS + "customers/login/", b.toByteArray(), this, false );
								
								setInConnection( true );
							} catch ( IOException ex ) {
								//#if DEBUG == "true"
//# 								ex.printStackTrace();
								//#endif
							}
						}
					break;
				}
			break;
			
			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
			break;
			
			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				switch ( sourceId ) {
					case ENTRY_NICKNAME:
					case ENTRY_PASSWORD:
						if ( evt.source.getBorder().getState() != Border.STATE_ERROR )
							evt.source.getBorder().setState( evt.eventType == Event.EVT_FOCUS_GAINED ? Border.STATE_FOCUSED : Border.STATE_UNFOCUSED );

						// se estiver no modo edit�vel, altera o label da soft key direita ao dar foco a um dos textBox
						if ( buttonBack.getId() == ENTRY_BUTTON_BACK && currentConnection < 0 )
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CLEAR ) );
					break;
					
					default:
						if ( currentConnection < 0 )
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
						checkBackButton();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do m�todo eventPerformed( Event )
	
	
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setEnabled( !inConnection );
		}
		buttonOK.setEnabled( !inConnection );
		buttonBack.setEnabled( !inConnection );
		
		if ( inConnection ) {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CANCEL ) );
		} else {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
			NanoOnline.getProgressBar().clearProgressBar();
		}
	}
	
	
	private final void checkBackButton() {
		if ( currentConnection >= 0 )
			onConnectionEnded( true );
		else
			onBack();	
	}	
	

}
