/*
 * SplashNano.java
 *
 * Created on June 15, 2007, 11:32 AM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Rectangle;

//#if DEBUG == "true"
//# import br.com.nanogames.components.userInterface.KeyListener;
//#endif


/**
 *
 * @author peter
 */
public final class BasicSplashNano extends UpdatableGroup implements ScreenListener
	//#if DEBUG == "true"
//# 		, KeyListener
	//#endif
{
	
	public static final byte SCREEN_SIZE_SMALL	= 0;
	public static final byte SCREEN_SIZE_MEDIUM	= 1;
	
	private final int BACKLIGHT_COLOR = 0x1A9DBA;
	
	private final MUV lightSpeed;
	
	// velocidade do efeito da luz em pixels por segundo
	private final int LIGHT_MOVE_SPEED;

	private final int LIGHT_X_OFFSET;
	private int LIGHT_X_LEFT;
	private int LIGHT_X_RIGHT;	

	// offsets na posi��o dos logos em rela��o ao texto "Nano"
	private final short IMG_GAMES_OFFSET_X;
	private final short IMG_GAMES_OFFSET_Y;
	private final short IMG_TURTLE_OFFSET_X;
	private final short IMG_TURTLE_OFFSET_Y;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private final DrawableImage imgNano;
	private final DrawableImage imgNanoLight;
	private final DrawableImage imgGames;
	private final DrawableImage imgTurtle;

	private final Rectangle lightViewport = new Rectangle();
	
	private final ImageFont font;
	
	private final Pattern transparency;
	private final Pattern backColor;
	
	// estados da anima��o do splash
	private final byte STATE_NANO_APPEARS	= 0;
	private final byte STATE_LIGHT_RIGHT	= STATE_NANO_APPEARS + 1;
	private final byte STATE_LIGHT_LEFT		= STATE_LIGHT_RIGHT + 1;
	private final byte STATE_WAIT			= STATE_LIGHT_LEFT + 1;
	private final byte STATE_NONE			= STATE_WAIT + 1;
	
	// dura��o em milisegundos de cada estado de anima��o
	private final short DURATION_NANO_APPEARS	= 600;
	private final short DURATION_WAIT			= 2100;
	
	/** tema de abertura a ser tocado */
	private final byte SOUND_INDEX;

	private final byte SOUND_LOOPS;
	
	/** Valor que identifica esta tela ao listener. */
	private final int nextScreenIndex;
	
	private byte currentState;
	
	private int accTime;

	private final RichLabel label;

	private final byte screenSize;

	private final Pattern bkg;


	public BasicSplashNano( int nextScreenIndex, byte screenSize, String imagesPath, int textIndex, int soundIndex ) throws Exception {
		this( nextScreenIndex, screenSize, imagesPath, textIndex, soundIndex, 1 );
	}

	
	/** Creates a new instance of SplashNano
	 * @param nextScreenIndex �ndice da tela a ser exibida ap�s o splash da Nano - o valor � passado para o m�todo 
	 * <code>AppMIDlet.setScreen(int)</code>.
	 * @param screenSize tamanho da tela. Valores v�lidos: SCREEN_SIZE_SMALL (telas com largura at� 132 pixels) ou 
	 * SCREEN_SIZE_MEDIUM (telas com 176 pixels ou mais de largura).
	 * @param imagesPath
	 * @param textIndex 
	 * @param soundIndex
	 * @throws java.lang.Exception 
	 */
	public BasicSplashNano( int nextScreenIndex, byte screenSize, String imagesPath, int textIndex, int soundIndex, int soundLoops ) throws Exception {
		super( TOTAL_ITEMS );
		
		bkg = new Pattern( null );
		bkg.setSize( size );
		bkg.setFillColor( 0x000000 );
		insertDrawable( bkg );
		
		this.nextScreenIndex = nextScreenIndex;
		SOUND_INDEX = ( byte ) soundIndex;
		SOUND_LOOPS = ( byte ) soundLoops;
		
		// logo da Nano
		imgNano = new DrawableImage( imagesPath + "nano.png" );
		imgNano.defineReferencePixel( imgNano.getSize().x >> 1, 0 );

		this.screenSize = screenSize;
		
		if ( screenSize == SCREEN_SIZE_SMALL ) {
			// velocidade do efeito da luz em pixels por segundo
			LIGHT_MOVE_SPEED = 115;

			LIGHT_X_OFFSET = 0;

			// offsets na posi��o dos logos em rela��o ao texto "Nano"
			IMG_GAMES_OFFSET_X = 26;
			IMG_GAMES_OFFSET_Y = 10;
			IMG_TURTLE_OFFSET_X = 24;
			IMG_TURTLE_OFFSET_Y = -32;
		} else {
			// velocidade do efeito da luz em pixels por segundo
			LIGHT_MOVE_SPEED = 157;

			LIGHT_X_OFFSET = 8;

			// offsets na posi��o dos logos em rela��o ao texto "Nano"
			IMG_GAMES_OFFSET_X = 65;
			IMG_GAMES_OFFSET_Y = 25;
			IMG_TURTLE_OFFSET_X = 59;
			IMG_TURTLE_OFFSET_Y = -69;
		}
		
		lightSpeed = new MUV( LIGHT_MOVE_SPEED );
		
		// efeito da luz passando
		imgNanoLight = new DrawableImage( imagesPath + "light.png" );
		imgNanoLight.setViewport( lightViewport );
		
		// cor de fundo do logo
		backColor = new Pattern( null );
		backColor.setFillColor( BACKLIGHT_COLOR );
		backColor.setSize( imgNano.getSize() );
		
		insertDrawable( backColor );
		insertDrawable( imgNanoLight );
		insertDrawable( imgNano );
		
		imgGames = new DrawableImage( imagesPath + "games.png" );
		imgGames.setPosition( imgNano.getPosition().x + IMG_GAMES_OFFSET_X, imgNano.getPosition().y + IMG_GAMES_OFFSET_Y );
		insertDrawable( imgGames );
		
		imgTurtle = new DrawableImage( imagesPath + "turtle.png" );
		imgTurtle.setPosition( imgNano.getPosition().x + IMG_TURTLE_OFFSET_X, imgNano.getPosition().y + IMG_TURTLE_OFFSET_Y );
		insertDrawable( imgTurtle );
		
		font = ImageFont.createMultiSpacedFont( imagesPath + "font_credits.png", imagesPath + "font_credits.dat" );
		label = new RichLabel( font, AppMIDlet.getText( textIndex ), ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2, null );
		insertDrawable( label );
		
		transparency = new Pattern( new DrawableImage( imagesPath + "transparency.png" ) );
//		transparency.setUsesTransparency( true ); o coment�rio deve ser removido no caso de Pattern utilizar copyArea
		insertDrawable( transparency );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		setState( STATE_NANO_APPEARS );
	} // fim do construtor SplashNano( String )
	
	
	private final void setState( int state ) {
		switch ( state ) {
			case STATE_NANO_APPEARS:
			break;
			
			case STATE_LIGHT_RIGHT:
				if ( SOUND_INDEX >= 0 ) {
					MediaPlayer.play( SOUND_INDEX, SOUND_LOOPS );
				}
				
				transparency.setVisible( false );
			case STATE_LIGHT_LEFT:
				if ( state == STATE_LIGHT_LEFT )
					lightSpeed.setSpeed( -LIGHT_MOVE_SPEED );
			case STATE_WAIT:
			case STATE_NONE:
			break;
			
			default:
				return;
		} // fim switch ( state )
		
		currentState = ( byte ) state;
		accTime = 0;
	} // fim do m�todo setState( byte )

	
	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;
		switch ( currentState ) {
			case STATE_NANO_APPEARS:
				if ( accTime >= DURATION_NANO_APPEARS )
					setState( currentState + 1 );
			break;
			
			case STATE_LIGHT_RIGHT:
				imgNanoLight.move( lightSpeed.updateInt( delta ), 0 );
				
				if ( imgNanoLight.getPosition().x >= LIGHT_X_RIGHT ) {
					imgNanoLight.setPosition( LIGHT_X_RIGHT, imgNanoLight.getPosition().y );
					setState( currentState + 1 );
				}
			break;
			
			case STATE_LIGHT_LEFT:
				imgNanoLight.move( lightSpeed.updateInt( delta ), 0 );

				if ( imgNanoLight.getPosition().x <= LIGHT_X_LEFT ) {
					imgNanoLight.setPosition( LIGHT_X_LEFT, imgNanoLight.getPosition().y );
					setState( currentState + 1 );
				}
			break;

			case STATE_WAIT:
				if ( accTime >= DURATION_WAIT ) {
					setState( STATE_NONE );
					AppMIDlet.setScreen( nextScreenIndex );
				}
			break;
		} // fim switch ( state )		
	} // fim do m�todo update( int )


	public final void hideNotify() {
	}


	public final void showNotify() {
	}


	public final void sizeChanged( int width, int height ) {
		// evita erros de desenho e/ou posicionamento no caso de mudan�a de dimens�es da tela durante o splash
		if ( width != size.x || height != size.y ) {
			setSize( width, height );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		final int HALF_WIDTH = width >> 1;
		final int HALF_HEIGHT = height >> 1;

		bkg.setSize( size );

		if ( screenSize == SCREEN_SIZE_SMALL ) {
			imgNano.setRefPixelPosition( HALF_WIDTH, HALF_HEIGHT - 6 );
		} else {
			imgNano.setRefPixelPosition( HALF_WIDTH, HALF_HEIGHT );
		}

		lightViewport.set( imgNano.getPosX(), imgNano.getPosY(), imgNano.getWidth(), imgNano.getHeight() );

		LIGHT_X_LEFT = imgNano.getPosition().x + LIGHT_X_OFFSET;
		LIGHT_X_RIGHT = imgNano.getPosition().x + imgNano.getSize().x;

		// efeito da luz passando
		imgNanoLight.setPosition( imgNano.getPosition().x - imgNanoLight.getSize().x, imgNano.getPosition().y );

		// cor de fundo do logo
		backColor.setPosition( imgNano.getPosition() );

		imgGames.setPosition( imgNano.getPosition().x + IMG_GAMES_OFFSET_X, imgNano.getPosition().y + IMG_GAMES_OFFSET_Y );
		imgTurtle.setPosition( imgNano.getPosition().x + IMG_TURTLE_OFFSET_X, imgNano.getPosition().y + IMG_TURTLE_OFFSET_Y );

		label.setSize( ( size.x * 3 ) >> 2, 0 );
		label.formatText( false );
		label.setSize( label.getWidth(), label.getTextTotalHeight() );
		label.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		label.setRefPixelPosition( size.x >> 1, size.y );

		transparency.setSize( width, height );
	}


	//#if DEBUG == "true"
//# 	public final void keyPressed( int key ) {
//# 		AppMIDlet.setScreen( nextScreenIndex );
//# 	}
//# 
//# 
//# 	public final void keyReleased( int key ) {
//# 	}
	//#endif
	
	
}
