/*
 * ImageLoader.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
import javax.microedition.lcdui.Image;

public final class ImageLoader {
 
	public static final byte FILTER_NONE		= 0;
	public static final byte FILTER_GRAY_SCALE	= 1;
	public static final byte FILTER_SEPIA		= 2;
	public static final byte FILTER_NEGATIVE	= 3;
	
	// cor utilizada para que todas as cores exceto magenta sejam trocadas
	public static final int CONVERT_ALL_COLOR = -1;
	
	
	private ImageLoader() {
	}
	
	
	/**
	 * 
	 * @param path
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Image loadImage( String path ) throws Exception {
		//#if DEBUG == "true"
//# 		// verifica se h� a substring "//" no endere�o (reinicia o aparelho em alguns aparelhos SonyEricsson)
//# 		if ( path.indexOf( "//" ) >= 0 )
//# 			throw new IllegalArgumentException( "Endere�o da imagem cont�m \"//\"" );
//# 		try {
		//#endif
		
		System.gc();
		final Image ret = Image.createImage( path );
		// permite que uma outra Thread execute, evitando tempos de resposta altos ou at� mesmo travamentos ao
		// suspender a execu��o durante o carregamento de muitas imagens (especialmente em aparelhos Motorola)
		Thread.yield();
		return ret;
		
		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			e.printStackTrace();
//# 			
//# 			throw new Exception( e + " ao alocar imagem no caminho \"" + path + "\"." );
//# 		}
		//#endif
	}
	
	
	/**
	 * Carrega uma nova imagem a partir de uma imagem base, convertendo as cores de acordo com a tabela recebida como par�metro.
	 * @param source imagem utilizada como base para a convers�o.
	 * @param conversionTable tabela de convers�o de cores. Essa tabela deve ser composta de pares de inteiros, cujo primeiro
	 * �ndice (zero) � a cor original, e o segundo �ndice (um) a cor de destino. Caso a cor de origem seja igual a -1,
	 * todas as cores exceto o magenta s�o trocadas (nesse caso, s� faz sentido utilizar uma tabela com um �nico par de
	 * convers�o).
	 * @throws java.lang.Exception Caso a imagem seja inv�lida ou nula, ou a tabela seja inv�lida.
	 * @return a imagem carregada e com as cores trocadas de acordo com a tabela.
	 * @see ImageLoader#loadImage(String, byte)
	 */
	public static final Image loadImage( Image source, int[][] conversionTable ) throws Exception {
        final int width  = source.getWidth();
        final int height = source.getHeight();
        
        final int[] data = new int[ width * height ]; 
        source.getRGB( data, 0, width, 0, 0, width, height );
        
        for ( int i = 0; i < data.length; ++i ) {
            final int pixel = data[ i ] & 0x00FFFFFF; // ignora alfa
            for ( int j = 0; j < conversionTable.length; ++j ) {
                final int from = conversionTable[ j ][ 0 ];
				if ( from == -1 ) {
					final int alpha = data[ i ] & 0xFF000000;

					if ( alpha == 0 )
						data[ i ] = 0x00FF00FF;
					else
						data[ i ] = 0xFF000000 | ( conversionTable[ j ][ 1 ] );  // ignora alfa
				} else {
					if ( pixel == from ) {
						if ( conversionTable[ j ][ 1 ] == 0xFF00FF )
							data[ i ] = 0x00FF00FF;
						else
							data[ i ] = 0xFF000000 | ( conversionTable[ j ][ 1 ] );  // ignora alfa
					}
				}
            } // fim for ( int j = 0; j < conversionTable.length; ++j )
        } // fim for ( int i = 0; i < data.length; ++i )
        
		final Image retValue = Image.createRGBImage( data, width, height, true );
		System.gc();
        return retValue;
	} // fim do m�todo loadImage( Image, int[][] )
	
	
	/**
	 * Carrega uma imagem a partir de um endere�o, convertendo as cores de acordo com a tabela recebida como par�metro.
	 * @param filename endere�o da imagem a ser carregada.
	 * @param conversionTable tabela de convers�o de cores. Essa tabela deve ser composta de pares de inteiros, cujo primeiro
	 * �ndice (zero) � a cor original, e o segundo �ndice (um) a cor de destino. Caso a cor de origem seja igual a -1,
	 * todas as cores exceto o magenta s�o trocadas (nesse caso, s� faz sentido utilizar uma tabela com um �nico par de
	 * convers�o).
	 * @throws java.lang.Exception Caso o endere�o seja inv�lido, a imagem esteja corrompida, ou a tabela seja inv�lida.
	 * @return a imagem carregada e com as cores trocadas de acordo com a tabela.
	 * @see ImageLoader#loadImage(Image, Object)
	 */
	public static final Image loadImage( String filename, int[][] conversionTable ) throws Exception {
		return loadImage( ImageLoader.loadImage( filename ), conversionTable );
	} // fim do m�todo loadImage( String, int[][] )
	 
	
	public static final Image loadImage( Image source, byte filterType ) throws Exception {
        final int width  = source.getWidth();
        final int height = source.getHeight();
        
        final int[] data = new int[ width * height ]; 
        source.getRGB( data, 0, width, 0, 0, width, height );
		
		switch ( filterType ) {
			case FILTER_GRAY_SCALE:
				toGray( data );
			break;

			case FILTER_SEPIA:
				toSepia( data );
			break;
        } // fim switch ( filterType )
        
        return Image.createRGBImage( data, width, height, true );
	} // fim do m�todo loadImage( Image, byte )
	 
	
	public static final Image loadImage( String filename, byte filterType ) throws Exception {
		return loadImage( ImageLoader.loadImage( filename ), filterType );
	}
	
	
	private static final void toGray( int[] data ) {
		for ( int i = 0; i < data.length; ++i ) {
			if ( ( data[ i ] & 0xff000000 ) == 0 ) {
				data[ i ] = 0x00ff00ff;
				continue;
			} else {
				final int color = data[ i ] & 0x00ffffff;
				final int media = ( ( ( color & 0xff0000 ) >> 16 ) +
									( ( color & 0x00ff00 ) >> 8 ) + 
									  ( color & 0x0000ff ) ) / 3;	
				
				data[ i ] = 0xff000000 | ( ( media << 16 ) | ( media << 8 ) | media );
			}
		}
	}
	
	
	private static final void toSepia( int[] data ) {
		// TODO s�pia sepia
		for ( int i = 0; i < data.length; ++i ) {
			if ( ( data[ i ] & 0xff000000 ) == 0 ) {
				data[ i ] = 0x00ff00ff;
				continue;
			} else {
				final int color = data[ i ] & 0x00ffffff;
				
				final int r = ( color & 0xff0000 ) >> 16;
				final int g = ( color & 0x00ff00 ) >> 8;
				final int b = color & 0x0000ff;
				
				final int r2 = ( r * 393 + g * 769 + b * 189 ) / 1351;
				final int g2 = ( r * 349 + g * 586 + b * 168 ) / 1103;
				final int b2 = ( r * 272 + g * 534 + b * 131 ) / 2140;
				
//				original:
//				final int r2 = ( r * 393 + g * 769 + b * 189 ) / 1351;
//				final int g2 = ( r * 349 + g * 686 + b * 168 ) / 1203;
//				final int b2 = ( r * 272 + g * 534 + b * 131 ) / 2140;				

				data[ i ] = 0xff000000 | ( ( r2 << 16 ) | ( g2 << 8 ) | b2 );
			}
		}		
		
//		// We take the RGB colour and we map it to YIQ
//		vec3 colour = texture2D(OGL2Texture, gl_TexCoord[0].st).rgb;
//
//		float Y = dot(vec3( 0.299, 0.587, 0.114),colour);
//
//		// these values blatently ripped off from Pete's GPU forums
//		float red = Y + 0.1912;
//		float green = Y - 0.0544;
//		float blue  = Y - 0.2210;
//
//		// 0.0 alpha unless you want some sort of transparency
//		gl_FragColor = vec4(red,green,blue,0.0); 
//		return color;
	}
	
//	TODO: http://www.j2meforums.com/forum/index.php?topic=14818.0
///*
//* Minimal PNG encoder to create MIDP images from RGBA arrays.
//*
//* Copyright 2006 Christian Froeschlin
//*
//* www.chrfr.de
//*
//*/
//
//import java.io.*;
//import javax.microedition.lcdui.Image;
//
//public class PNG
//{
// 
//  public static Image toImage(int width, int height, byte[] alpha, byte[] red, byte[] green, byte[] blue)
//  {
//    try   
//    {
//      byte[] png = toPNG(width, height, alpha, red, green, blue);
//      return Image.createImage(png, 0, png.length);
//    }
//    catch (IOException e)
//    {
//      return null;
//    }
//  }
// 
//  public static byte[] toPNG(int width, int height, byte[] alpha, byte[] red, byte[] green, byte[] blue) throws IOException
//  {
//    byte[] signature = new byte[] {(byte) 137, (byte) 80, (byte) 78, (byte) 71, (byte) 13, (byte) 10, (byte) 26, (byte) 10};
//    byte[] header = createHeaderChunk(width, height);
//    byte[] data = createDataChunk(width, height, alpha, red, green, blue);
//    byte[] trailer = createTrailerChunk();
//   
//    ByteArrayOutputStream png = new ByteArrayOutputStream(signature.length + header.length + data.length + trailer.length);
//    png.write(signature);
//    png.write(header);
//    png.write(data);
//    png.write(trailer);
//    return png.toByteArray();
//  }
//
//  public static byte[] createHeaderChunk(int width, int height) throws IOException
//  {
//    ByteArrayOutputStream baos = new ByteArrayOutputStream(13);
//    DataOutputStream chunk = new DataOutputStream(baos);
//    chunk.writeInt(width);
//    chunk.writeInt(height);
//    chunk.writeByte(8); // Bitdepth
//    chunk.writeByte(6); // Colortype ARGB
//    chunk.writeByte(0); // Compression
//    chunk.writeByte(0); // Filter
//    chunk.writeByte(0); // Interlace   
//    return toChunk("IHDR", baos.toByteArray());
//  }
//
//  public static byte[] createDataChunk(int width, int height, byte[] alpha, byte[] red, byte[] green, byte[] blue) throws IOException
//  {
//    int source = 0;
//    int dest = 0;
//    byte[] raw = new byte[4*(width*height) + height];
//    for (int y = 0; y < height; y++)
//    {
//      raw[dest++] = 0; // No filter
//      for (int x = 0; x < width; x++)
//      {
//        raw[dest++] = red[source];
//        raw[dest++] = green[source];
//        raw[dest++] = blue[source];
//        raw[dest++] = alpha[source++];
//      }
//    }
//    return toChunk("IDAT", toZLIB(raw));
//  }
//
//  public static byte[] createTrailerChunk() throws IOException
//  {
//    return toChunk("IEND", new byte[] {});
//  }
// 
//  public static byte[] toChunk(String id, byte[] raw) throws IOException
//  {
//    ByteArrayOutputStream baos = new ByteArrayOutputStream(raw.length + 12);
//    DataOutputStream chunk = new DataOutputStream(baos);
//   
//    chunk.writeInt(raw.length);
//   
//    byte[] bid = new byte[4];
//    for (int i = 0; i < 4; i++)
//    {
//      bid[i] = (byte) id.charAt(i);
//    }
//   
//    chunk.write(bid);
//       
//    chunk.write(raw);
//   
//    int crc = 0xFFFFFFFF;
//    crc = updateCRC(crc, bid); 
//    crc = updateCRC(crc, raw);   
//    chunk.writeInt(~crc);
//   
//    return baos.toByteArray();
//  }
//
//  static int[] crcTable = null;
// 
//  public static void createCRCTable()
//  {
//    crcTable = new int[256];
//   
//    for (int i = 0; i < 256; i++)
//    {
//      int c = i;
//      for (int k = 0; k < 8; k++)
//      {
//        c = ((c & 1) > 0) ? 0xedb88320 ^ (c >>> 1) : c >>> 1;
//      }
//      crcTable[i] = c;
//    }
//  }
// 
//  public static int updateCRC(int crc, byte[] raw)
//  {
//    if (crcTable == null)
//    {
//      createCRCTable();
//    }
//   
//    for (int i = 0; i < raw.length; i++)
//    {
//      crc = crcTable[(crc ^ raw[i]) & 0xFF] ^ (crc >>> 8);     
//    }
//   
//    return crc;
//  }
// 
//  // Creates a single zlib block contain a single
//  // uncompressed deflate block. Must be < 64K!
//  public static byte[] toZLIB(byte[] raw) throws IOException
//  {   
//    byte tmp;
//    ByteArrayOutputStream baos = new ByteArrayOutputStream(raw.length + 5 + 6);
//    DataOutputStream zlib = new DataOutputStream(baos);
//    tmp = (byte) (8 + (7 << 16));
//    zlib.writeByte(tmp);      // Compression + Flags
//    zlib.writeByte((31 - ((tmp << 8) % 31)) % 31); // FCHECK (compr/dict 0)
//   
//    //Uncompressed deflate block
//    zlib.writeByte((byte) 1);    //Final flag set, Compression type 0
//    char length = (char) raw.length;
//    zlib.writeChar(length);  //Length
//    zlib.writeChar(~length); //Length 1st complement
//    zlib.write(raw);         //Data
//   
//    // zlib block check sum
//    zlib.writeInt(calcADLER32(raw));
//    return baos.toByteArray();
//  }
//
//  // Unverified (the Java PNG loader did not complain at any value)
//  public static int calcADLER32(byte[] raw)
//  {
//    int s1 = 1;
//    int s2 = 0;
//    for (int i = 0; i < raw.length; i++)
//    {
//      s1 = (s1 + raw[i]) % 65521;
//      s2 = (s2 + s1) % 65521;     
//    }
//    return (s2 << 16) + s1;
//  }
//}
	
//	TODO: http://www.j2meforums.com/forum/index.php?topic=5589.0
//private long calculateCRC(byte[] array)
//{
//       long[] crcTable = new long[256];
//       long c;
//
//       for(int n=0; n<256; n++)
//       {
//           c = n;
//           for(int k=0; k<8; k++)
//           {
//               if((c & 1) == 1)
//               {
//                   c = 0xedb88320L ^ (c >> 1);
//               }
//               else
//               {
//                   c >>= 1;
//               }
//           }
//           crcTable[n] = c;
//       }
//
//       long temp1, temp2;
//       long crc = 0xFFFFFFFFL;
//       for(int n=0; n<array.length; n++)
//       {
//           temp1 = (crc >> 8);
//           temp2 = crcTable[(int)((crc ^ array[n]) & 0xFF)];
//           crc = temp1 ^ temp2;
//       }
//
//       return ~crc;
//}	
	 
}
 
