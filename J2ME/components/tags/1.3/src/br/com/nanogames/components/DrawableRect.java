/**
 * DrawableRect.java
 * �2008 Nano Games.
 *
 * Created on Aug 6, 2008 11:25:13 AM.
 */

package br.com.nanogames.components;

import javax.microedition.lcdui.Graphics;


/**
 * Essa classe encapsula a funcionalidade de desenho de um ret�ngulo n�o preenchido. Para desenhar um ret�ngulo
 * preenchido, utilize a classe <code>Pattern</code>.
 * @author Peter
 */
public class DrawableRect extends Drawable {
	
	private int color;
	
	
	public DrawableRect() {
	}
	
	
	public DrawableRect( int color ) {
		setColor( color );
	}
	
	
	public final int getColor() {
		return color;
	}
	
	
	public final void setColor( int color ) {
		this.color = color;
	}

	
	protected void paint( Graphics g ) {
		g.setColor( color );
		g.drawRect( translate.x, translate.y, size.x - 1, size.y - 1 );
	}

}
