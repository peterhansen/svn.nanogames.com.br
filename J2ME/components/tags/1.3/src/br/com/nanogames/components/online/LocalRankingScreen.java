/**
 * LocalRankingScreen.java
 * 
 * Created on 6/Fev/2009, 8:48:53
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;

/**
 *
 * @author Peter
 */
public class LocalRankingScreen extends NanoOnlineContainer {
	
	private static final byte ENTRY_NICKNAME	= 0;
	private static final byte ENTRY_BUTTON_OK	= 1;
	private static final byte ENTRY_BUTTON_BACK	= 2;
	
	private final TextBox textBox;

	
	public LocalRankingScreen() throws Exception {
		super( 3 );
		
		setBackIndex( SCREEN_NEW_RECORD );
		
		final ImageFont font = NanoOnline.getFont( FONT_DEFAULT );
		
		textBox = new TextBox( font, null, 20, TextBox.INPUT_MODE_ANY, true );
		textBox.setCaret( NanoOnline.getCaret() );
		textBox.setId( ENTRY_NICKNAME );
		textBox.addActionListener( this );
		textBox.setBorder( NanoOnline.getTitledBorder( TEXT_NICKNAME ) );
		insertDrawable( textBox );
		
		final Button buttonOK = new Button( font, NanoOnline.getText( TEXT_OK ) );
		buttonOK.setId( ENTRY_BUTTON_OK );
		buttonOK.setBorder( NanoOnline.getBorder() );
		buttonOK.addActionListener( this );		
		
		insertDrawable( buttonOK );
		
		final Button buttonBack = new Button( font, NanoOnline.getText( TEXT_BACK ) );
		buttonBack.setId( ENTRY_BUTTON_BACK );
		buttonBack.setBorder( NanoOnline.getBorder() );
		buttonBack.addActionListener( this );		
		
		insertDrawable( buttonBack );
	}
	

	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_OK:
						onButtonConfirm();
					break;
					
					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_BUTTON_BACK:
						onBack();
					break;
				}
			break;
			
			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
			break;
			
			case Event.EVT_FOCUS_GAINED:
				if ( sourceId == ENTRY_NICKNAME ) {
					evt.source.getBorder().setState( Border.STATE_FOCUSED );
					NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CLEAR ) );
				} else {
					evt.source.getBorder().setState( Border.STATE_UNFOCUSED );
					NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do m�todo eventPerformed( Event )
	
	
	private final void onButtonConfirm() {
		try {
			RankingScreen.setHighScore( RankingScreen.getLastType(), textBox.getText(), RankingScreen.getLastScore(), RankingScreen.getLastDecrescent() );
			NanoOnline.setScreen( SCREEN_RECORDS );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif
				
			NanoOnline.setScreen( SCREEN_MAIN_MENU );
		}
	}

}
