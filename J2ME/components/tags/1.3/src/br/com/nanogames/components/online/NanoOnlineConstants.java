/**
 * NanoOnlineConstants.java
 * 
 * Created on 30/Out/2008, 15:54:55
 *
 */

package br.com.nanogames.components.online;

/**
 *
 * @author Peter
 */
public interface NanoOnlineConstants {
	
	/** Endere�o raiz do Nano Online. */
	public static final String NANO_ONLINE_ADDRESS = "http://online.nanogames.com.br/";
//	public static final String NANO_ONLINE_ADDRESS = "http://localhost:3000/";
	
	public static final String PATH_NANO_ONLINE_IMAGES = "/online/";
	
	/** Nome da base de dados onde ser�o armazenadas as informa��es do Nano Online. */
	public static final String DATABASE_NAME = "no";
	
	/** �ndice do slot de grava��o das informa��es dos usu�rios na base de dados. */
	public static final byte DATABASE_SLOT_PROFILES = 1;
	
	/** Quantidade total de slots na base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 1;
	
	/** �ndice da fonte padr�o utilizada no Nano Online. */
	public static final byte FONT_DEFAULT = 0;
	
	/** �ndice da fonte padr�o utilizada no Nano Online. */
	public static final byte FONT_TEXT = 1;
	
	/** Quantidade total de fontes utilizadas no Nano Online. */
	public static final byte FONT_TYPES_TOTAL = 2;
	
	/** M�ximo de perfis gravados. */
	public static final byte MAX_PROFILES = 10;
	
	
	// <editor-fold desc="TEXTOS UTILIZADOS NO NANO ONLINE" >
	
	public static final byte TEXT_YES									= 0;
	public static final byte TEXT_NO									= TEXT_YES + 1;
	public static final byte TEXT_BACK									= TEXT_NO + 1;
	public static final byte TEXT_CANCEL								= TEXT_BACK + 1;
	public static final byte TEXT_CONFIRM								= TEXT_CANCEL + 1;
	public static final byte TEXT_OK									= TEXT_CONFIRM + 1;
	public static final byte TEXT_EDIT									= TEXT_OK + 1;
	public static final byte TEXT_EXIT									= TEXT_EDIT + 1;
	public static final byte TEXT_CLEAR									= TEXT_EXIT + 1;
	public static final byte TEXT_NICKNAME								= TEXT_CLEAR + 1;
	public static final byte TEXT_EMAIL									= TEXT_NICKNAME + 1;
	public static final byte TEXT_PASSWORD								= TEXT_EMAIL + 1;
	public static final byte TEXT_PASSWORD_CONFIRM						= TEXT_PASSWORD + 1;
	public static final byte TEXT_CONNECTING							= TEXT_PASSWORD_CONFIRM + 1;
	public static final byte TEXT_REGISTER								= TEXT_CONNECTING + 1;
	public static final byte TEXT_LOGIN									= TEXT_REGISTER + 1;
	public static final byte TEXT_RECORDS								= TEXT_LOGIN + 1;
	public static final byte TEXT_OPTIONS								= TEXT_RECORDS + 1;
	public static final byte TEXT_UPDATE								= TEXT_OPTIONS + 1;
	public static final byte TEXT_ERASE_RECORDS							= TEXT_UPDATE + 1;
	public static final byte TEXT_HELP_TITLE							= TEXT_ERASE_RECORDS + 1;
	public static final byte TEXT_HELP_TITLE_COMMANDS					= TEXT_HELP_TITLE + 1;
	public static final byte TEXT_HELP_TITLE_RANKING					= TEXT_HELP_TITLE_COMMANDS + 1;
	public static final byte TEXT_HELP_TITLE_TAXES						= TEXT_HELP_TITLE_RANKING + 1;
	public static final byte TEXT_HELP_TITLE_SUPPORT					= TEXT_HELP_TITLE_TAXES + 1;
	public static final byte TEXT_HELP_TEXT_COMMANDS					= TEXT_HELP_TITLE_SUPPORT + 1;
	public static final byte TEXT_HELP_TEXT_RANKING						= TEXT_HELP_TEXT_COMMANDS + 1;
	public static final byte TEXT_HELP_TEXT_TAXES						= TEXT_HELP_TEXT_RANKING + 1;
	public static final byte TEXT_HELP_TEXT_SUPPORT						= TEXT_HELP_TEXT_TAXES + 1;
	public static final byte TEXT_CONNECTION_OPENED						= TEXT_HELP_TEXT_SUPPORT + 1;
	public static final byte TEXT_RESPONSE_CODE							= TEXT_CONNECTION_OPENED + 1;
	public static final byte TEXT_SENDING_DATA							= TEXT_RESPONSE_CODE + 1;
	public static final byte TEXT_RECEIVING_DATA						= TEXT_SENDING_DATA + 1;
	public static final byte TEXT_REMEMBER_ME							= TEXT_RECEIVING_DATA + 1;
	public static final byte TEXT_REMEMBER_PASSWORD						= TEXT_REMEMBER_ME + 1;
	public static final byte TEXT_USE_EXISTING_PROFILE					= TEXT_REMEMBER_PASSWORD + 1;
	public static final byte TEXT_USE_PROFILE							= TEXT_USE_EXISTING_PROFILE + 1;
	public static final byte TEXT_ERASE_PROFILE							= TEXT_USE_PROFILE + 1;
	public static final byte TEXT_NO_PROFILES_FOUND						= TEXT_ERASE_PROFILE + 1;
	public static final byte TEXT_CURRENT_PROFILE						= TEXT_NO_PROFILES_FOUND + 1;
	public static final byte TEXT_SAVE_LOCAL_RECORD						= TEXT_CURRENT_PROFILE + 1;
	public static final byte TEXT_LOCAL_RANKING							= TEXT_SAVE_LOCAL_RECORD + 1;
	public static final byte TEXT_GLOBAL_RANKING						= TEXT_LOCAL_RANKING + 1;
	public static final byte TEXT_CUSTOMER_REGISTERED					= TEXT_GLOBAL_RANKING + 1;
	public static final byte TEXT_REGISTER_ERROR_NICKNAME_IN_USE		= TEXT_CUSTOMER_REGISTERED + 1;
	public static final byte TEXT_REGISTER_ERROR_NICKNAME_FORMAT		= TEXT_REGISTER_ERROR_NICKNAME_IN_USE + 1;
	public static final byte TEXT_REGISTER_ERROR_NICKNAME_LENGTH		= TEXT_REGISTER_ERROR_NICKNAME_FORMAT + 1;
	public static final byte TEXT_REGISTER_ERROR_EMAIL_FORMAT			= TEXT_REGISTER_ERROR_NICKNAME_LENGTH + 1;
	public static final byte TEXT_REGISTER_ERROR_EMAIL_LENGTH			= TEXT_REGISTER_ERROR_EMAIL_FORMAT + 1;
	public static final byte TEXT_REGISTER_ERROR_PASSWORD_WEAK			= TEXT_REGISTER_ERROR_EMAIL_LENGTH + 1;
	public static final byte TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT		= TEXT_REGISTER_ERROR_PASSWORD_WEAK + 1;
	public static final byte TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION	= TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT + 1;
	public static final byte TEXT_REGISTER_ERROR_REPLY_FORMAT			= TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION + 1;
	public static final byte TEXT_LOGIN_ERROR_NICKNAME_NOT_FOUND		= TEXT_REGISTER_ERROR_REPLY_FORMAT + 1;
	public static final byte TEXT_LOGIN_ERROR_PASSWORD_INVALID			= TEXT_LOGIN_ERROR_NICKNAME_NOT_FOUND + 1;
	public static final byte TEXT_ERROR_SERVER_INTERNAL					= TEXT_LOGIN_ERROR_PASSWORD_INVALID + 1;
	public static final byte TEXT_ERROR_APP_NOT_FOUND					= TEXT_ERROR_SERVER_INTERNAL + 1;
	public static final byte TEXT_ERROR_DEVICE_NOT_SUPPORTED			= TEXT_ERROR_APP_NOT_FOUND + 1;
	
	
	public static final byte TEXTS_TOTAL = TEXT_ERROR_DEVICE_NOT_SUPPORTED + 1;
	
	// </editor-fold>
	
	
	// <editor-fold desc="TELAS USADAS NO NANO ONLINE" >
	
	public static final byte SCREEN_MAIN_MENU				= 100;
	public static final byte SCREEN_REGISTER_PROFILE		= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_PROFILE_SELECT			= SCREEN_REGISTER_PROFILE + 1;
	public static final byte SCREEN_PROFILE_EDIT			= SCREEN_PROFILE_SELECT + 1;
	public static final byte SCREEN_LOAD_PROFILE			= SCREEN_PROFILE_EDIT + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_LOAD_PROFILE + 1;
	public static final byte SCREEN_HELP_COMMANDS			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_RANKING			= SCREEN_HELP_COMMANDS + 1;
	public static final byte SCREEN_HELP_SUPPORT			= SCREEN_HELP_RANKING + 1;
	public static final byte SCREEN_HELP_TAXES				= SCREEN_HELP_SUPPORT + 1;
	public static final byte SCREEN_ERROR					= SCREEN_HELP_TAXES + 1;
	public static final byte SCREEN_OPTIONS					= SCREEN_ERROR + 1;
	public static final byte SCREEN_RECORDS					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_NEW_RECORD				= SCREEN_RECORDS + 1;
	public static final byte SCREEN_ENTER_LOCAL_NAME		= SCREEN_NEW_RECORD + 1;
	
	// </editor-fold>
	
	
	// <editor-fold desc="IDS GLOBAIS UTILIZADOS COMO CHAVES NO PROTOCOLO NANO ONLINE" >
	
	// por padr�o, os ids de chaves globais s�o negativos, para evitar poss�veis confus�es com ids espec�ficos de
	// um servi�o (registro de usu�rio, ranking online, multiplayer, propagandas, etc.), que devem utilizar ids
	// positivos.
	
	/** Id de chave global: aplicativo/jogo. Tipo do valor: String */
	public static final byte ID_APP					= Byte.MIN_VALUE;
	
	/** Id de chave global: vers�o do aplicativo/jogo. Tipo do valor: string */
	public static final byte ID_APP_VERSION			= ID_APP + 1;
	
	/** Id de chave global: idioma atual do aplicativo. Tipo do valor: byte */
	public static final byte ID_LANGUAGE			= ID_APP_VERSION + 1;
	
	/** Id de chave global: vers�o do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string */
	public static final byte ID_NANO_ONLINE_VERSION	= ID_LANGUAGE + 1;
	
	/** Id de chave global: c�digo de retorno. Tipo do valor: short */
	public static final byte ID_RETURN_CODE			= ID_NANO_ONLINE_VERSION + 1;
	
	/** Id de chave global: usu�rio. Tipo do valor: int */
	public static final byte ID_CUSTOMER_ID				= ID_RETURN_CODE + 1;
	
	/** Id de chave global: mensagem de erro. Tipo do valor: string */
	public static final byte ID_ERROR_MESSAGE		= ID_CUSTOMER_ID + 1;
	
	/** Id de chave global: in�cio de dados espec�ficos. Tipo do valor: byte[] (tamanho vari�vel) */
	public static final byte ID_SPECIFIC_DATA		= ID_ERROR_MESSAGE + 1;
	
	// </editor-fold>
	
	
	// <editor-fold desc="Valores de retorno globais" >

	// assim como as chaves, os valores de retorno globais possuem valores negativos para que n�o haja confus�o com os
	// valores utilizados internamente em cada servi�o. A chave 0 (zero) � considerada OK (RC_OK)

	/** Valor de retorno global: sem erros. */
	public static final byte RC_OK						= 0;
	
	/** Valor de retorno global: erro interno do servidor. */
	public static final byte RC_SERVER_INTERNAL_ERROR	= RC_OK - 1;
	
	/** Valor de retorno global: aparelho n�o suportado/detectado. */
	public static final byte RC_DEVICE_NOT_SUPPORTED	= RC_SERVER_INTERNAL_ERROR - 1;
	
	/** Valor de retorno global: aplicativo n�o encontrado (id de aplicativo inv�lido). */
	public static final byte RC_APP_NOT_FOUND			= RC_DEVICE_NOT_SUPPORTED - 1;
	
	// </editor-fold>	
	
	
	// <editor-fold desc="IDS DE JOGOS E APLICATIVOS" >
	
	/** Id de jogo/aplicativo: dummy (usado para testes e debug) */
	public static final String ID_APP_DUMMY									= "DMMY";
	
	// </editor-fold>
	
}
