/**
 * NanoConnection.java
 * 
 * Created on 31/Out/2008, 14:40:36
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.DynamicByteArray;
import br.com.nanogames.components.util.Mutex;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

/**
 *
 * @author Peter
 */
public class NanoConnection {
	
	/** M�todo HTTP: GET */
	public static final byte METHOD_GET		= 0;
	/** M�todo HTTP: POST */
	public static final byte METHOD_POST	= 1;
	/** M�todo HTTP: PUT (emulado, pois n�o h� suporte nativo a PUT em J2ME) */
	public static final byte METHOD_PUT		= 2;
	/** M�todo HTTP: DELETE (emulado, pois n�o h� suporte nativo a DELETE em J2ME) */
	public static final byte METHOD_DELETE	= 3;
	/** M�todo HTTP: HEAD */
	public static final byte METHOD_HEAD	= 4;
	
	/** Identificador da conex�o atual. */
	protected static int currentId;
	
	/***/
	protected static final Mutex mutex = new Mutex();
	
	/** Armazena os ids das conex�es ativas. */
	protected static final Hashtable activeConnections = new Hashtable();
	
	
	/**
	 * Chamada equivalente a <code>open( url, METHOD_GET, null, listener, false )</code>.
	 * 
	 * @param url
	 * @param listener
	 * @return
	 * @see post(String, byte[], ConnectionListener, boolean)
	 * @see open(String, byte, byte[], ConnectionListener, boolean)
	 * @see #cancel(int)
	 */
	public static final int get( final String url, final ConnectionListener listener ) {
		return open( url, METHOD_GET, null, listener, false );
	}
	

	/**
	 * Equivalente � chamada de <code>open( url, METHOD_POST, data, listener, useKeyValuePairs )</code>.
	 * 
	 * @param url
	 * @param data
	 * @param listener
	 * @param useKeyValuePairs
	 * @return
	 * @see get(String, ConnectionListener)
	 * @see open(String, byte, byte[], ConnectionListener, boolean)
	 * @see #cancel(int)
	 */
	public static final int post( final String url, final byte[] data, final ConnectionListener listener, final boolean useKeyValuePairs ) {
		return open( url, METHOD_POST, data, listener, useKeyValuePairs );
	}
	
	
	/**
	 * Equivalente � chamada de <code>open( url, METHOD_PUT, data, listener, useKeyValuePairs )</code>.
	 *
	 * @param url
	 * @param data
	 * @param listener
	 * @param useKeyValuePairs
	 * @return
	 * @see get(String, ConnectionListener)
	 * @see open(String, byte, byte[], ConnectionListener, boolean)
	 * @see #cancel(int)
	 */
	public static final int put( final String url, final byte[] data, final ConnectionListener listener, final boolean useKeyValuePairs ) {
		return open( url, METHOD_PUT, data, listener, useKeyValuePairs );
	}


	/**
	 * Abre uma conex�o HTTP.
	 * 
	 * @param url endere�o a ser conectado.
	 * @param method m�todo de conex�o HTTP. Valores v�lidos:
	 * <ul>
	 * <li>METHOD_GET</li>
	 * <li>METHOD_POST</li>
	 * <li>METHOD_PUT</li>
	 * <li>METHOD_DELETE</li>
	 * <li>METHOD_HEAD</li>
	 * </ul>
	 * @param data dados da conex�o. Esse par�metro � desconsiderado no caso de conex�o GET, ou caso seja nulo.
	 * @param listener listener que receber� os eventos indicando o progresso da conex�o, eventuais erros e que receber� os
	 * dados retornados pelo servidor no caso de uma conex�o bem-sucedida.
	 * @param useKeyValuePairs TODO comentar
	 * @return o id da conex�o atual, que ser� o mesmo repassado ao listener para identificar de que pedido de conex�o o
	 * evento foi gerado.
	 * @throws NullPointerException caso <code>url</code>, <code>method</code> ou <code>listener</code> sejam nulos.
	 * @see #cancel(int)
	 * @see get(String, ConnectionListener)
	 * @see post(String, byte[], ConnectionListener, boolean)
	 */
	public static final int open( final String url, final byte method, final byte[] data, final ConnectionListener listener, final boolean useKeyValuePairs ) throws NullPointerException {
		// TODO modo de leitura dos dados atrav�s de streams
		try {
			mutex.acquire();
			
			//#if DEBUG == "true"
//# 			System.out.println( "NanoConnection.open:\n\turl: " + ( url == null ? "null" : url ) + "\n\tmethod: " + method + "\n\tdata: " + ( data == null ? "null" : new String( data ) ) );
			//#endif
			
			final int ID = currentId++;
			activeConnections.put( new Integer( ID ), new Object() );
			final Thread connectThread = new Thread() {
				public final void run() {
					// d� chance � thread chamadora de continuar a execu��o, e assim poder armazenar o ID
					checkYield( ID );

					HttpConnection c = null;
					InputStream is = null;
					OutputStream os = null;

					//#if BLACKBERRY_API == "false"
					try {
						Class.forName( "javax.microedition.io.HttpConnection" );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 							e.printStackTrace();
						//#endif
							
						listener.onError( ID, ConnectionListener.ERROR_HTTP_CONNECTION_NOT_SUPPORTED, e );
						return;
					}
					//#endif

					try {
						// os m�todos PUT e DELETE s�o emulados, por isso precisam de altera��o na URL
						String finalURL;
						switch ( method ) {
							case METHOD_PUT:
								finalURL = url + "?_method=PUT";
							break;

							case METHOD_DELETE:
								finalURL = url + "?_method=DELETE";
							break;
							
							default:
								finalURL = url;
						}
						
						c = ( HttpConnection ) Connector.open( finalURL, Connector.READ_WRITE, false );
						listener.onInfo( ID, ConnectionListener.INFO_CONNECTION_OPENED, null );
						checkYield( ID );
						
						// Set the request method and headers
						boolean post = false;
						switch ( method ) {
							case METHOD_GET:
								c.setRequestMethod( HttpConnection.GET );
							break;
							
							case METHOD_HEAD:
								c.setRequestMethod( HttpConnection.HEAD );
							break;
							
							case METHOD_PUT:
							case METHOD_DELETE:
							case METHOD_POST:
								c.setRequestMethod( HttpConnection.POST );
								post = true;
							break;
							
							//#if DEBUG == "true"
//# 							default:
//# 								throw new IllegalArgumentException( "Invalid HTTP method: " + method );
							//#endif
						}

						final String platform = AppMIDlet.getPlatform();
						// alguns aparelhos, como o Nokia N76, n�o enviam o user-agent ao conectar pelo J2ME. Nesse caso, envia
						// a plataforma para tentar a detec��o.
						c.setRequestProperty( "User-Agent", platform == null ? "Profile/MIDP-2.0 Configuration/CLDC-1.0" : platform );
						c.setRequestProperty( "Content-Language", "en-US" ); // TODO pt-BR?
						c.setRequestProperty( "Connection", "close" );

						if ( post && data != null ) {
							// content-type � diferente para enviar arquivos bin�rios
							c.setRequestProperty( "Content-Type", useKeyValuePairs ? "application/x-www-form-urlencoded" : "application/octet-stream" ); 
							c.setRequestProperty( "Content-Length", String.valueOf( data.length ) );
							
							os = c.openDataOutputStream();
							listener.onInfo( ID, ConnectionListener.INFO_OUTPUT_STREAM_OPENED, null );
							checkYield( ID );
							
							os.write( data );
							listener.onInfo( ID, ConnectionListener.INFO_DATA_WRITTEN, null );
							checkYield( ID );
						}

						// N�o deve-se chamar output.flush() para evitar problemas com alguns aparelhos
						// http.getResponseCode() implicitamente chama flush()

						// Getting the response code will open the connection, send the request, and read 
						// the HTTP response headers. The headers are stored until requested.
						try {
							final int rc = c.getResponseCode();
							listener.onInfo( ID, ConnectionListener.INFO_RESPONSE_CODE, new Integer( rc ) );
							checkYield( ID );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
								
							listener.onError( ID, ConnectionListener.ERROR_CANT_GET_RESPONSE_CODE, e );
						}

						is = c.openDataInputStream();
						listener.onInfo( ID, ConnectionListener.INFO_INPUT_STREAM_OPENED, null );
						checkYield( ID );
						
						// Get the length and process the data
						final int len = ( int ) c.getLength();
						listener.onInfo( ID, ConnectionListener.INFO_CONTENT_LENGTH_TOTAL, new Integer( len ) );
						checkYield( ID );

						final DynamicByteArray array = new DynamicByteArray();
						// Cria o buffer tempor�rio que ir� armazenar os dados lidos
						// n�o utiliza o m�todo array.readInputStream() porque ele n�o permite acompanhamento do progresso
						final byte[] buffer = new byte[ 1024 ];

						// L� os dados da stream de entrada
						int nBytes;
						int bytesRead = 0;
						while( ( nBytes = is.read( buffer, 0, buffer.length ) ) != -1 ) {
							array.insertData( buffer, 0, nBytes );

							bytesRead += nBytes;
							listener.onInfo( ID, ConnectionListener.INFO_CONTENT_LENGTH_READ, new Integer( bytesRead ) );
							checkYield( ID );
						}

						final byte[] readData = array.getData();
						listener.processData( ID, readData );
						
//						is.read(); // TODO necess�rio? (aparentemente � necess�rio para fechar corretamente a conex�o)
						listener.onInfo( ID, ConnectionListener.INFO_CONNECTION_ENDED, null );
					} catch ( ClassCastException e ) {
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif

						listener.onError( ID, ConnectionListener.ERROR_URL_BAD_FORMAT, e );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif

						listener.onError( ID, ConnectionListener.ERROR_CONNECTION_EXCEPTION, e );
					} finally {
						cancel( ID );
						
						if ( is != null ) {
							try {
								is.close();
							} catch ( Exception e ) {
								listener.onError( ID, ConnectionListener.ERROR_CANT_CLOSE_INPUT_STREAM, e );
							}
						}

						if ( os != null ) {
							try {
								os.close();
							} catch ( Exception e ) {
								listener.onError( ID, ConnectionListener.ERROR_CANT_CLOSE_OUTPUT_STREAM, e );
							}
						}

						if ( c != null ) {
							try {
								c.close();
							} catch ( Exception e ) {
								listener.onError( ID, ConnectionListener.ERROR_CANT_CLOSE_CONNECTION, e );
							}
						}
					} // fim finally
				}
			};
			connectThread.setPriority( Thread.NORM_PRIORITY + 1 ); //TODO testar prioridade ideal para Thread de conex�o
			connectThread.start();

			return ID;
		} finally {
			mutex.release();
		}
	}
	
	
	/**
	 * 
	 * @param id
	 * @throws java.lang.RuntimeException
	 */
	protected static final void checkYield( int id ) throws RuntimeException {
		if ( !activeConnections.containsKey( new Integer( id ) ) ) {
			//#if DEBUG == "true"
//# 				throw new RuntimeException( "Conex�o cancelada: " + id );
			//#else
				throw new RuntimeException();
			//#endif
		}
		
		Thread.yield();
	}
	
	
	/**
	 * Cancela uma conex�o. Como a conex�o � feita numa Thread pr�pria, o cancelamento pode n�o ser imediato.
	 * @param id id da conex�o, conforme retornado pelo m�todo <code>open(String, String, byte[], ConnectionListener)</code>.
	 * @see get(String, ConnectionListener)
	 * @see post(String, byte[], ConnectionListener, boolean)
	 * @see #open(String, String, byte[], ConnectionListener)
	 */
	public static final void cancel( int id ) {
		activeConnections.remove( new Integer( id ) );
	}
	
}
