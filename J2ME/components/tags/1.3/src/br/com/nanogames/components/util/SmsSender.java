package br.com.nanogames.components.util;

/**
 * SmsSender.java
 * �2008 Nano Games.
 *
 * Created on 22/07/2008 18:06:46.
 */

/**
 * @author Daniel L. Alves
 */

import javax.microedition.io.Connector;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

//#if SAMSUNG_API == "true"
//# import com.samsung.util.SM;
//# import com.samsung.util.SMS;
//#endif


/** Classe respons�vel pelo envio de mensagens SMS */
public final class SmsSender extends Thread
{
	/** Tempo m�ximo pelo qual esperamos a thread de envio de SMS terminar suas tarefas */
	public static final short SMS_SENDER_MAX_WAIT_TIME = 25000;
	
	/** Thread de envio de SMS deste objeto */
	private SmsThread smsThread;
	
	/** Objeto que receber� informa��es sobre o andamento do envio do SMS. Esta refer�ncia tamb�m � armazenada
	 * nesta classe (e n�o apenas em SmsThread) para que o mesmo objeto possa ser chamado pela thread de envio
	 * de SMS e pela thread de timeout da conex�o
	 */
	private SmsSenderListener listener;
	
	/** Controla o acesso �s regi�es cr�ticas da opera��o de envio de SMS */
	private final Mutex mutex = new Mutex();
	
	/** Retorna se o device suporta o envio de SMS */
	public static final boolean isSupported()
	{
		//#if SAMSUNG_API == "true"
//# 			// � importante lembrar que nem todos aparelhos da Samsung suportam a pr�pria API da Samsung!
//# 			// A prefer�ncia � pela API WMA do J2ME, j� que na bateria de testes mostrou ser implementada por
//# 			// um maior n�mero de devices desta fabricante
//# 			if( !supportsDefault() )
//# 			{
//# 				try
//# 				{
//# 					return SMS.isSupported();
//# 				}
//# 				catch( Throwable t )
//# 				{
//# 					return false;
//# 				}
//# 			}
//# 			return true;
		//#else
			return supportsDefault();
		//#endif
	}
	
	/** Retorna se o device suporta o envio de SMS atrav�s das APIs opcionais do J2ME ( WMA - Wireless Messaging API )*/
	private static final boolean supportsDefault()
	{
		// TODO teste para resolver verification error em alguns aparelhos BlackBerry
		//#if BLACKBERRY_API == "true"
//# 			return true;
		//#else
			try
			{
				Class.forName( "javax.wireless.messaging.MessageConnection" );
				Class.forName( "javax.wireless.messaging.TextMessage" );
				return true;
			}
			catch( Throwable t )
			{
				return false;
			}
		//#endif
	}

	
	/** Envia uma mensagem SMS
	 * @param phoneNumber Telefone para o qual a mensagem ser� enviada
	 * @param smsText Texto da mensagem SMS
	 * @param listener Objeto que receber� informa��es sobre o andamento do envio do SMS
	 */
	public final void send( String phoneNumber, String smsText, SmsSenderListener listener )
	{
		mutex.acquire();

		// Termina alguma thread de envio de SMS que possa estar ativa
		killSmsThread();

		// Troca o listener apenas depois de chamar killSmsThread(), j� que tal m�todo pode querer invocar
		// o listener anterior
		this.listener = listener;
		
		// Cria uma thread para o envio do SMS. Al�m de ser uma melhor pr�tica de programa��o, se torna 
		// OBRIGAT�RIO, pois alguns devices (i.e A1200) disparam uma exce��o quando tentamos executar uma
		// opera��o bloqueante na thread principal da aplica��o
		smsThread = new SmsThread( phoneNumber, smsText, listener, this, mutex );
		smsThread.setPriority( Thread.MAX_PRIORITY );
		smsThread.start();
		
		mutex.release();
	}
	
	/** Controla o timeout da conex�o de envio de SMS. Se faz necess�rio pois em alguns devices (i.e. LG KG800,
	 * LG MG320c) a exce��o de timeout da conex�o n�o � disparada. Uma das raz�es para isso acontecer � que nem
	 * mesmo o padr�o J2ME obriga o fabricante a faz�-lo (vide documenta��o da classe MessageConnection)
	 */
	public final void run()
	{
		//#if DEBUG == "true"
//# 		if( listener != null )
//# 			listener.onSMSLog( smsThread.getID(), "\n\nChamou o timer" );
		//#endif

		try
		{
			sleep( SMS_SENDER_MAX_WAIT_TIME );
		}
		catch( Exception ex )
		{
			// Pode acontecer de sleep() disparar uma exce��o e acabarmos chamando killSmsThread() sem esperarmos
			// um tempo razo�vel para o envio do SMS. Como na pr�tica o disparo de uma exce��o por sleep() se
			// mostrou inexistente, ignoramos a possibilidade
		}

		mutex.acquire();
		
		if( killSmsThread() && ( listener != null ) )
		{
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "Erro: For�ou o fim da execu��o" );
			//#endif

			listener.onSMSSent( smsThread.getID(), false );
		}
		
		mutex.release();
	}
	
	/** TODO : Come�ar a utilizar Thread.interrupt() em killSmsThread() a partir do momento que o 1o se tornar
	 * dispon�vel em toda base dos devices com que trabalhamos */

	/** Tenta for�ar o t�rmino da thread de envio de SMS. O ideal seria se pud�ssemos chamar Thread.interrupt()
	 * em todos os devices. Mas como tal m�todo s� est� presente a partir do CLDC 1.1 e n�o conseguimos pegar
	 * a vers�o do CLDC em tempo de execu��o, tentamos contornar o problema for�ando o fim da conex�o. Nos
	 * aparelhos LG KG800 e MG320c foi descoberta a possibilidade de um erro: caso o usu�rio tente enviar um SMS
	 * e n�o possua cr�ditos para faz�-lo, a thread de envio ficar� eternamente na mem�ria, j� que o m�todo
	 * MessageConnection.send() n�o dispara exce��es por timeout e/ou conex�o fechada.
	 * 
	 * O mutex � sempre utilizado por fora desse m�todo
	 * 
	 * @return Se teve ou n�o que matar a thread
	 */
	private final boolean killSmsThread()
	{
		//#if DEBUG == "true"
//# 		if( listener != null )
//# 			listener.onSMSLog( smsThread.getID(), "\n\nkillSmsThread():" );
		//#endif

		if( ( smsThread != null ) && ( smsThread.isAlive() ) )
		{
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "\n- For�ou o fechamento da conex�o" );
			//#endif

			smsThread.closeSmsConnection();
			return true;
		}
		//#if DEBUG == "true"
//# 		else
//# 		{
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "\n- Conex�o j� estava fechada" );
//# 		}
		//#endif
		return false;
	}
	
		/** Thread respons�vel pelo envio do SMS */
	private static final class SmsThread extends Thread
	{
		/** Telefone para o qual a mensagem ser� enviada */
		private final String phoneNumber;
			
		/** Texto da mensagem SMS */
		private final String smsText;

		/** Objeto que receber� informa��es sobre o andamento do envio do SMS */
		private final SmsSenderListener listener;
		
		/** Conex�o atrav�s da qual o SMS ser� enviado. Mant�m esta refer�ncia na classe para que seja poss�vel
		 * fechar a conex�o por fora do m�todo que a criou. S� � utilizada em conjunto com a API WMA. A API
		 * da Samsung n�o faz uso deste atributo
		 */
		private MessageConnection smsConn;

		/** Timer que ir� cancelar a conex�o de envio de SMS caso o tempo limite seja atingido. Se faz necess�rio
		 * pois em alguns devices (i.e. LG KG800, LG MG320c) a exce��o de timeout da conex�o n�o � disparada. Uma
		 * das raz�es para isso acontecer � que nem mesmo o padr�o J2ME obriga o fabricante a faz�-lo (vide
		 * documenta��o da classe MessageConnection). No entanto, s� � utilizado em conjunto com a API WMA.
		 * A API da Samsung n�o faz uso deste atributo
		 */
		private final Thread timeoutThread;

		/** Controla o acesso �s regi�es cr�ticas da opera��o de envio de SMS */
		private final Mutex mutex;
		
		/** Vari�vel respons�vel por gerar os ids das threads de envio de SMS */
		private static int idCounter = Integer.MIN_VALUE;
		
		/** ID �nico desta thread */
		private final int id;
		
		/** Cria uma thread de envio de SMS
		 * @param phoneNumber Telefone para o qual a mensagem ser� enviada
		 * @param smsText Texto da mensagem SMS
		 * @param listener Objeto que receber� informa��es sobre o andamento do envio do SMS
		 * @param timeoutThread Thread que controlar� o timeout da conex�o
		 * @param mutex Controlador de acesso �s regi�es cr�ticas da opera��o de envio de SMS
		 */
		public SmsThread( String phoneNumber, String smsText, SmsSenderListener listener, Thread timeoutThread, Mutex mutex )
		{
			this.phoneNumber = phoneNumber;
			this.smsText = smsText;
			this.listener = listener;
			this.timeoutThread = timeoutThread;
			this.mutex = mutex;
			id = newID();
		}
		
		/** Retorna um ID �nico para o objeto chamador */
		private static synchronized int newID()
		{
			return idCounter++;
		}
		
		/** Obt�m o ID �nico da thread */
		public int getID()
		{
			return id;
		}
		
		//#if SAMSUNG_API == "true"
//# 			public void run()
//# 			{
//# 				// Verifica qual m�todo de execu��o deve ser chamado de acordo com as APIs suportadas pelo device
//# 				// � importante lembrar que nem todos aparelhos da Samsung suportam a pr�pria API da Samsung!
//# 				// Ver m�todo SmsSender.isSupported()
//# 				if( supportsDefault() )
//# 					runDefault();
//# 				else
//# 					runSamsung();
//# 			}
//# 
//# 			/** Envia um SMS utilizando a API da Samsung. N�o precisa utilizar mutex!!! */
//# 			private void runSamsung()
//# 			{
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n\nrunSamsung():" );
				//#endif
//# 			
//# 				boolean ret = true;
//# 
//# 				try
//# 				{
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Criando a mensagem" );
					//#endif
//# 
//# 					final SM message = new SM();
//# 					message.setData( smsText );
//# 					message.setDestAddress( phoneNumber );
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Enviando sms" );
					//#endif
//# 
//# 					SMS.send( message );
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- SMS enviado" );
					//#endif
//# 				}
//# 				catch( Throwable t )
//# 				{
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Disparou uma excecao ao tentar enviar o sms:" + t.getMessage() );
					//#endif
//# 					
//# 					ret = false;
//# 				}
//# 
//# 				if( listener != null )
//# 					listener.onSMSSent( getID(), ret );
//# 			}
		//#else
		public void run()
		{
			runDefault();
		}
		//#endif
		
		/** Envia um SMS utilizando a API opcional WMA do J2ME */
		private void runDefault()
		{
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( getID(), "\n\nrunDefault():" );
			//#endif
			
			boolean ret = true;

			try
			{
				mutex.acquire();

				smsConn = null;

				// S� queremos enviar
				//final String smsConnection = "sms://:" + midlet.getAppProperty( "SMS-Port" );
				final String smsConnection = "sms://" + phoneNumber;

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Abrindo a conex�o" );
				//#endif

				smsConn = ( MessageConnection )Connector.open( smsConnection, Connector.WRITE, true );

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Criando a mensagem" );
				//#endif

				final TextMessage sms = ( TextMessage )smsConn.newMessage( MessageConnection.TEXT_MESSAGE );

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Determinando o texto da mensagem" );
				//#endif

				sms.setPayloadText( smsText  );

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Setando o timer" );
				//#endif

				timeoutThread.start();

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Enviando o sms" );
				//#endif
				
				mutex.release();

				// O m�todo MessageConnection.send() pode causar alguns problemas:
				// - Samsung: Alguns Samsungs (i.e. D820) n�o disparam exece��es quando o usu�rio tenta enviar 
				// um SMS sem possuir cr�ditos. Logo, retornaremos que o SMS foi enviado sem isso ter ocorrido
				// de verdade
				// - LG: Caso o usu�rio tente enviar um SMS e n�o possua cr�ditos para faz�-lo, a execu��o desta
				// thread ficar� eternamente bloqueada dentro de send() em alguns devices (i.e. KG800, MG320c).
				// Ver SmsSender.killSmsThread() para maiores informa��es sobre o workaround utilizado
				smsConn.send( sms );
			}
			catch( Throwable t )
			{
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Erro:" + t.getMessage() );
				//#endif
				
				ret = false;
			}
			finally
			{
				mutex.acquire();
				
				closeSmsConnection();
			}

			if( listener != null )
				listener.onSMSSent( getID(), ret );
			
			mutex.release();
		}
		
		/** Fecha a conex�o de envio de SMS desta thread. Este m�todo � p�blico para que seja poss�vel
		 * for�ar o fechamento da conex�o por fora do objeto
		 * 
		 * O mutex � sempre utilizado por fora desse m�todo
		 */
		public void closeSmsConnection()
		{
			if( smsConn != null )
			{
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Fechando a conex�o" );
				//#endif

				try
				{
					smsConn.close();

					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Conex�o fechada" );
					//#endif

					smsConn = null;
					System.gc();
					yield();
				}
				catch( Throwable t )
				{
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Erro ao fechar a conex�o:" + t.getMessage() );
					//#endif
					
					// Pode n�o conseguir fechar a conex�o e esssa ficar eternamente aberta, no entanto isso
					// n�o deveria acontecer
				}
			}
		}
	}
}