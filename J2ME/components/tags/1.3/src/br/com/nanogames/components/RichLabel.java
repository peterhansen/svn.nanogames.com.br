/*
 * RichLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 * Implementa um label com formata��o especial, possibilitando quebras de linha, v�rias formas de alinhamento 
 * de texto e inser��o de Drawables no texto (animados ou n�o). Para ter acesso a esses recursos, utiliza-se uma 
 * formata��o similar ao padr�o HTML.
 *
 * <h4>Alinhamento</h4>
 * Por padr�o, o alinhamento � top-left. O alinhamento considerado ao escrever uma linha � a combina��o do �ltimo alinhamento
 * horizontal com o �ltimo alinhamento vertical definidos. Por exemplo: ao encontrar uma tag &lt;ALN_R&gt;, todo o texto
 * subsequente � alinhado � direita, at� se chegar ao fim do texto ou ser lida uma tag indicando outro tipo de alinhamento
 * horizontal.
 * <p>Valores v�lidos:</p>
 * <ul>
 * <li>&lt;ALN_L&gt; alinhado � esquerda.</li>
 * <li>&lt;ALN_H&gt; centralizado horizontalmente.</li>
 * <li>&lt;ALN_R&gt; horizontal � direita.</li>
 * <li>&lt;ALN_T&gt; alinhamento vertical no topo da linha.</li>
 * <li>&lt;ALN_V&gt; centralizado verticalmente.</li>
 * <li>&lt;ALN_B&gt; alinhamento vertical na parte inferior da linha.</li>
 * </ul>
 *
 * <h4>Inser��o de Drawables</h4>
 * Drawables podem ser inseridos no texto, atrav�s do uso da tag <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" � o �ndice do
 * Drawable no array passado ao contrutor do label. Os Drawables seguem tamb�m as regras de alinhamento v�lidas para o 
 * texto. � importante observar que o alinhamento vertical define a forma como o texto e os Drawables de uma linha ir�o
 * se posicionar, no caso de a altura do Drawable n�o ser igual � altura da fonte utilizada no texto. Esse alinhamento 
 * toma como base o elemento de maior altura encontrado numa linha.
 * <p>� poss�vel tamb�m inserir Drawables animados no texto. Para isso, basta que o Drawable implemente a interface
 * <i>Updatable</i>, e fa�a o gerenciamento da anima��o atrav�s do m�todo <i>update</i> implementado.</p>
 *
 * <h4>Exemplo</h4>
 * A seguinte String:<br></br> <b>&lt;ALN_H&gt;T�tulo\n&lt;ALN_R&gt;Texto � direita\n&lt;ALN_V&gt;Texto � direita e centralizado verticalmente&lt;IMG_0&gt;</b>
 *
 * <p>resulta num texto com a primeira linha centralizada horizontalmente, a segunda alinhada horizontalmente � direita,
 * e a terceira alinhada � direita, por�m com alinhamento vertical do texto no centro do Drawable colocado no fim do texto.
 * Para se utilizar Drawables no texto, deve-se passar um array contendo refer�ncias a eles, e depois utilizar a tag no padr�o
 * <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" � o �ndice do Drawable no array.</p>
 *
 *
 * @author peter
 */
public class RichLabel extends Label implements Updatable {
 
	// tags de alinhamento do texto
	public static final char ALIGN_LEFT		= 'L';
	public static final char ALIGN_RIGHT	= 'R';
	public static final char ALIGN_HCENTER	= 'H';
	public static final char ALIGN_TOP		= 'T';
	public static final char ALIGN_VCENTER	= 'V';
	public static final char ALIGN_BOTTOM	= 'B';
	public static final char TAG_START		= '<';
	public static final char TAG_SEPARATOR	= '_';
	public static final char TAG_END		= '>';
	public static final String TAG_IMAGE	= "IMG_";
	public static final String TAG_ALIGN	= "ALN_";
	 
	// array de Drawables que podem ser inseridos no texto
	protected final Drawable[] drawables;
	
	// array dos drawables atualiz�veis. Esse � um subconjunto gerado automaticamente a partir do array de drawables, e
	// tem como fun��o acelerar o m�todo update, varrendo somente os elementos que podem ser atualizados.
	protected final Updatable[] updatables;
	
	// �ndices de cada informa��o na matriz de informa��es das linhas
	/** �ndice da informa��o das linhas referente ao �ndice do caracter de in�cio da linha. */
	public static final byte INFO_LINE_START			= 0;
	/** �ndice da informa��o das linhas referente ao �ndice do caracter de fim da linha. */
	public static final byte INFO_LINE_END				= INFO_LINE_START				+ 1;
	/** �ndice da informa��o das linhas referente � largura da linha. */
	public static final byte INFO_LINE_WIDTH			= INFO_LINE_END					+ 1;
	/** �ndice da informa��o das linhas referente � altura da linha. */
	public static final byte INFO_LINE_HEIGHT			= INFO_LINE_WIDTH				+ 1;
	/** �ndice da informa��o das linhas referente ao tipo de alinhamento horizontal da linha. */
	public static final byte INFO_LINE_ALIGN_HORIZONTAL	= INFO_LINE_HEIGHT				+ 1;
	/** �ndice da informa��o das linhas referente ao tipo de alinhamento vertical da linha. */
	public static final byte INFO_LINE_ALIGN_VERTICAL	= INFO_LINE_ALIGN_HORIZONTAL	+ 1;
	
	protected static final byte LINE_INFO_TOTAL_INDEXES	= INFO_LINE_ALIGN_VERTICAL	+ 1;	
	
	protected static final byte TAG_TYPE_NONE	= 0;
	protected static final byte TAG_TYPE_ALIGN	= 1;
	protected static final byte TAG_TYPE_IMAGE	= 2;
	
	protected static final int DEFAULT_TEXT_ALIGNMENT = ANCHOR_TOP | ANCHOR_LEFT;
	
	// n�mero de espa�os de cada '\t' 
	protected static final byte TAB_SPACES = 3;
	
	/** Matriz de informa��es das linhas. O primeiro �ndice refere-se � linha, e o segundo a cada tipo de informa��o
	 * conforme descrito em INFO_LINE_START, INFO_LINE_END, etc.
	 */
	protected short[][] linesInfo;
	
	/** Offset na posi��o de desenho do texto (utilizado para fazer scroll do texto) */
	protected int textOffset;
	
	/** linha inicial a partir da qual � feita a varredura para desenho */
	protected short initialLine;
	
	/** Varia��o na posi��o inicial de desenho causada pela posi��o atual do texto - valor utilizado para evitar varreduras
	 * desnecess�rias em linhas que n�o est�o dentro da �rea vis�vel.
	 */
	protected int initialLineOffset;
	
	// StringBuffers usados no m�todo paint( Graphics )
	protected final StringBuffer bufferType = new StringBuffer( 3 );
	protected final StringBuffer bufferNumber = new StringBuffer();
	
	/** evita que o texto seja desenhado e/ou alterado enquanto o texto est� sendo reformatado, evitando-se assim 
	 * problemas de inconsist�ncia que podem gerar travamentos
	 */
	protected final Mutex textMutex = new Mutex();
	
	
	/**
	 * Cria um novo label formatado. Ap�s a cria��o, seu tamanho estar� definido de forma a comportar todo o texto. 
	 * Equivalente � chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @param maxLineWidth largura m�xima em pixels de cada linha, para fins de realizar a quebra do texto automaticamente
	 * de acordo com essa largura. Caso seja menor ou igual a zero, cada linha s� ser� quebrada explicitamente, ou seja,
	 * caso encontre caracteres '\n'.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 */
	public RichLabel( ImageFont font, String text, int maxLineWidth ) throws Exception {
		this( font, text, maxLineWidth, null );
	}
	 
	
	/**
	 * Cria um novo label formatado. Ap�s a cria��o, seu tamanho estar� definido de forma a comportar todo o texto.
	 * 
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @param maxLineWidth largura m�xima em pixels de cada linha, para fins de realizar a quebra do texto automaticamente
	 * de acordo com essa largura. Caso seja menor ou igual a zero, cada linha s� ser� quebrada explicitamente, ou seja,
	 * caso encontre caracteres '\n'.
	 * @param specialChars array de drawables que podem ser utilizados no texto. Para utiliz�-los, deve-se usar a tag 
	 * <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" � o �ndice do Drawable no array passado ao contrutor do label. Os Drawables 
	 * seguem tamb�m as regras de alinhamento v�lidas para o texto. � importante observar que o alinhamento vertical 
	 * define a forma como o texto e os Drawables de uma linha ir�o se posicionar, no caso de a altura do Drawable n�o 
	 * ser igual � altura da fonte utilizada no texto. Esse alinhamento toma como base o elemento de maior altura encontrado 
	 * numa linha.
	 * <p>� poss�vel tamb�m inserir Drawables animados no texto. Para isso, basta que o Drawable implemente a interface
	 * <i>Updatable</i>, e fa�a o gerenciamento da anima��o atrav�s do m�todo <i>update</i> implementado.</p>
	 * @throws java.lang.Exception 
	 * @see #RichLabel(ImageFont, String, int)
	 */
	public RichLabel( ImageFont font, String text, int maxLineWidth, Drawable[] specialChars ) throws Exception {
		// formata o texto apenas no final do construtor, para evitar que, no caso de haver caracteres especiais,
		// sejam necess�rias 2 chamadas a formatText (pois na 1� chamada drawables n�o estaria inicializado)
		super( font, text, false );
		
		if ( specialChars != null && specialChars.length > 0 ) {
			this.drawables = specialChars;
			
			Vector updatableVector = new Vector();
			
			// varre o array de caracteres especiais, procurando por caracteres animados; essa varredura tem por
			// objetivo otimizar a varredura do m�todo update(), varrendo somente os objetos que podem ser atualizados
			for ( int i = 0; i < specialChars.length; ++i ) {
				if ( specialChars[ i ] instanceof Updatable )
					updatableVector.addElement( specialChars[ i ] );
			}
			
			// cria o array de Updatables somente com a quantidade necess�ria de objetos
			if ( updatableVector.size() > 0 ) {
				updatables = new Updatable[ updatableVector.size() ];
				
				for ( int i = 0; i < updatables.length; ++i ) {
					updatables[ i ] = ( Updatable ) updatableVector.elementAt( i );
				}
			} else {
				// n�o foram encontrados elementos atualiz�veis no array de caracteres especiais; n�o � necess�rio
				// criar o array de atualiz�veis
				updatables = null;
			}
			
		} else {
			this.drawables = null;
			updatables = null;
		}
		
		if ( maxLineWidth > 0 )
			size.x = maxLineWidth;
		
		formatText( maxLineWidth <= 0 );
		
		// define a altura inicial de forma a comportar todo o texto
		size.y = getTextTotalHeight();
	}
	
	
	/**
	 * 
	 * @param autoSize
	 */
	public void formatText( boolean autoSize ) {
        final Vector lines = new Vector( 1 );
        final short[] lineTemp = new short[ LINE_INFO_TOTAL_INDEXES ];
        
		int lineStart = 0;
		// �ndices na string que delimitam uma linha a ser desenhada
		int lineEnd = 0;
		// largura da linha de texto atual
		int lineWidth = 0;
		// altura da linha de texto atual
		int lineHeight = 0;
		int verticalAlignment = ANCHOR_TOP;
		int horizontalAlignment = ANCHOR_LEFT;
		
		// largura em pixels do espa�o � armazenado para acelerar o loop
		final int spaceWidth = font.getCharWidth( ' ' );
        
		// linha atual
        int currentLine = 0;
		// posi��o x atual
		int x = 0;
		// caracteres lidos
        int read = 0;
        
		// indica se a linha ainda n�o foi totalmente preenchida
        boolean lineIncomplete = true; 
        
        // constantes calculadas previamente de forma a acelerar o loop
        final int toRead = charBuffer.length;
		
		if ( size.x <= 0 )
			autoSize = true;
		
        while ( read < toRead ) {
            int i = read;
            lineIncomplete = true;
			
			// posi��o no textArray de char onde a �ltima palavra iniciou
            int lastWordStart = 0;
            
            lineStart = read;
            lineWidth = x = 0;

			// indica se uma palavra j� foi iniciada, ou se o caracter lido � o primeiro de uma palavra (no caso de caracteres imprim�veis)
            boolean word = false; 
			
            lineHeight = 0;// TODO testar se n�o deu cagada! font.getImage().getHeight();
            
        	// enquanto n�o completa uma linha                
            while ( lineIncomplete && ( i < toRead ) ) {
                final char c = charBuffer[ i ];
				// indica o tipo do caracter lido 
                boolean printableRead = false;
                switch ( c ) {
                	case 0: // n�o desenha caso o caracter n�o esteja definido.
					break;
						
                	case ' ':
						if( lineHeight == 0 )
							lineHeight = font.getHeight();
						x += spaceWidth;
                	    if ( !autoSize && x > size.x ) {
                	        lineIncomplete = false;
                	        i++;
                	    }
               	        lineWidth = x;
					break; // fim case ' '
						
                	case '\n':
						if( lineHeight == 0 )
							lineHeight = font.getHeight();
            	        lineWidth = x;
                	    lineIncomplete = false;
                	    read = i++;
					break; // fim case '\n'
						
                	case TAG_START:
                	    final StringBuffer strBufferType = new StringBuffer( 3 );
                	    final StringBuffer strBufferNumber = new StringBuffer();
                	    char alignType = 0;

                	    byte type = TAG_TYPE_NONE; 
                	    int j = i + 1;
                	    while ( charBuffer[ j ] != TAG_END ) {
                	        switch ( type ) {
                	        	case TAG_TYPE_NONE:
                       	            strBufferType.append( charBuffer[ j ] );
                        	        if ( charBuffer[ j ] == TAG_SEPARATOR ) {
                        	            final String tagType = strBufferType.toString();
                        	            if ( tagType.equals( TAG_ALIGN ) )
                        	                type = TAG_TYPE_ALIGN;
                        	            else if ( tagType.equals( TAG_IMAGE ) )
                        	                type = TAG_TYPE_IMAGE;
                        	        }
                	        	break;
                	        	case TAG_TYPE_ALIGN:
                	        	    alignType = charBuffer[ j ];
                	        	break;
                	        	case TAG_TYPE_IMAGE:
                	        	    strBufferNumber.append( charBuffer[ j ] );
                	        	break;
                	        } // fim switch ( type )
                	        ++j;
                	    } // fim while ( textArray[ j ] != TAG_END )
						
						switch ( type ) {
							case TAG_TYPE_ALIGN:
								switch ( alignType ) {
									case ALIGN_HCENTER:
										horizontalAlignment = ANCHOR_HCENTER;
									break;
									
									case ALIGN_LEFT:
										horizontalAlignment = ANCHOR_LEFT;
									break;
									
									case ALIGN_RIGHT:
										horizontalAlignment = ANCHOR_RIGHT;
									break;
									
									case ALIGN_BOTTOM:
										verticalAlignment = ANCHOR_BOTTOM;
									break;
									
									case ALIGN_VCENTER:
										verticalAlignment = ANCHOR_VCENTER;
									break;
									
									case ALIGN_TOP:
										verticalAlignment = ANCHOR_TOP;
									break;
								} // fim switch ( alignType )
							break;
							
							case TAG_TYPE_IMAGE:
								if ( drawables != null ) {
									final int index = Integer.parseInt( strBufferNumber.toString() );
									printableRead = true;
//									if ( !word ) { //TODO testar
										// Iniciou uma nova "palavra" (um drawable). Marca a posi��o de in�cio como a
										// �ltima posi��o de fim de linha conhecida.
//										word = true;
										lastWordStart = i;
										lineEnd = lastWordStart;
										lineWidth = x;

										// se uma tag de imagem for o primeiro caracter imprim�vel da linha, marca o in�cio da linha
//										if ( lineStart == 0 ) TODO teste
										if ( lastWordStart == 0 )
											lineStart = i;
//									} 

									final int drawableWidth = drawables[ index ].getWidth();
									final int drawableHeight = drawables[ index ].getHeight();
									
									final int tempWidth = x + drawableWidth;
									
									if ( !autoSize && tempWidth > size.x ) {
										// terminou uma linha
										lineIncomplete = false;
										
										if ( lastWordStart == lineStart ) {
											// drawable � o primeiro caracter da linha - inicia uma nova linha
											
											// o marcador de fim de linha � posicionado logo ap�s o fim da tag
											lineEnd = j;

											// o drawable tem largura maior ou igual � largura do label, logo considera-se
											// que a linha tem tamb�m a largura do label.
											lineWidth = size.x;
											
											// atualiza a altura da linha, caso necess�rio
//											if ( ( drawableHeight > font.getImage().getHeight() ) && ( drawableHeight > lineHeight ) ) TODO verificar
											if ( drawableHeight > lineHeight )
												lineHeight = drawableHeight;
										} else {
											// h� caracteres na linha antes do drawable - o imprime por completo na linha seguinte.
											lineEnd = lastWordStart;
											lineWidth = x;
										}
										
										read = lineEnd;
									} else {
										x = tempWidth;
										
										// atualiza a altura da linha, caso necess�rio
										if ( /* Testar se ficou tudo OK sem isso ( drawableHeight > font.getImage().getHeight() ) &&*/ ( drawableHeight > lineHeight ) )
											lineHeight = drawableHeight;
									}
									
								} // fim if ( drawables != null )
							break;
						} // fim switch ( type )

            	        if ( lineIncomplete )
            	            i = j;
					break; // fim case TAG_START
						
            		default:
                		printableRead = true;            			
						if ( !word ) {
							lineEnd = lastWordStart = i;
							lineWidth = x;
						} 
						
						// TODO verificar se � poss�vel otimizar teste de altura da linha
						
						lineHeight = NanoMath.max( font.getHeight(), lineHeight );
						
						// Testa se ultrapassa limite da margem direita. caso ultrapasse, a palavra ser� impressa na 
						// linha seguinte. caso a palavra seja grande demais, � quebrada por caracter.
						if ( lastWordStart == lineStart ) {
							// palavra � maior que a largura da linha (palavra � quebrada por caracter).
							read = lineEnd = i;
							lineWidth = x;
						} else {
							// palavra � menor que a largura da linha - � impressa por completo na linha seguinte
							read = lineEnd = lastWordStart;
						}
						
						x += font.getCharWidth( c );
						if ( !autoSize && x > size.x )
							lineIncomplete = false;
						word = true;
    	                break;
                } // fim switch ( c )
				
				if ( !lineIncomplete ) {
					if ( !printableRead ) {
						//i++; // com essa linha n�o comentada, os espa�os no in�cio de cada linha n�o s�o mostrados
						read = i;
						lineEnd = i;
					}
				} else { // if (lineIncomplete)
					++i;
					if ( !printableRead ) {
						lineEnd = i;
						word = false;
					}
					read = i;
				}
            } // fim while (lineIncomplete)
			
            if ( i >= toRead ) {
    	        read = lineEnd = i;
    	        lineWidth = x;
            } else
                read = lineEnd;
            
            lineTemp[ INFO_LINE_START ] = ( short ) lineStart;
            lineTemp[ INFO_LINE_END ] = ( short ) lineEnd;
			// espa�os e tabula��es podem ultrapassar a margem da imagem, por�m a largura da linha n�o pode ser maior 
			// que a largura do label, sen�o ocorrem problemas com alinhamentos horizontais.
			if ( autoSize ) {
				// define suas dimens�es de forma a comportar todos os caracteres numa �nica linha.
				lineTemp[ INFO_LINE_WIDTH ] = ( short ) lineWidth;
				setSize( lineWidth, lineHeight );
			} else {
				lineTemp[ INFO_LINE_WIDTH ] = ( short ) Math.min( lineWidth, size.x );
			}
			
            lineTemp[ INFO_LINE_HEIGHT ] = ( short ) lineHeight;
            lineTemp[ INFO_LINE_ALIGN_HORIZONTAL ] = ( short ) horizontalAlignment;
			lineTemp[ INFO_LINE_ALIGN_VERTICAL ] = ( short ) verticalAlignment;
            final short[] t = new short[ lineTemp.length ];
            System.arraycopy( lineTemp, 0, t, 0, lineTemp.length );
            lines.addElement( t );
            
            ++currentLine;
        } // fim while ( read < toRead )
        
        linesInfo = new short[ lines.size() ][ LINE_INFO_TOTAL_INDEXES ];
        for ( int index = 0; index < linesInfo.length; ++index ) {
            final short[] t = ( short[] ) lines.elementAt( index );
            for ( int i = 0; i < LINE_INFO_TOTAL_INDEXES; ++i )
                linesInfo[ index ][ i ] = t[ i ];
        }
		
		// volta a exibir o texto a partir do in�cio
		setTextOffset( 0 );
	} // fim do m�todo formatText()	
	
	
	/**
	 * 
	 * @param g 
	 */
    public void paint( Graphics g ) {
		// FIXME pode ocorrer erro no desenho de partes do texto ap�s drawable - n�o � exibido texto ap�s drawable, por�m, ap�s o scroll descer um pouco, � exibido um bloco inteiro de texto que deveria estar vis�vel antes
		textMutex.acquire();
		
		final int totalLines = linesInfo.length;
		int x;
		int	y = translate.y + textOffset - initialLineOffset;
		int	verticalAlignment;
		int	currentLine;
		boolean imageEnd = false;

		// altura da fonte
		final int fontHeight = font.getImage().getHeight();

		pushClip( g );
		final int yLimit = g.getClipY() + g.getClipHeight();

		// se o topo da linha estiver al�m da �rea de clip, p�ra a varredura, pois garantidamente todos os caracteres
		// e/ou drawables das linhas seguintes estar�o fora da �rea de clip.		
		for ( currentLine = initialLine; currentLine < totalLines && !imageEnd && y < yLimit; ++currentLine ) {
			// armazena uma refer�ncia para as informa��es atuais, de forma a evitar acessos desnecess�rios ao array
			final short[] LINE_INFO = linesInfo[ currentLine ];
			
			// ajusta a posi��o do texto de acordo com o alinhamento horizontal
			switch ( LINE_INFO[ INFO_LINE_ALIGN_HORIZONTAL ] ) {
				case ANCHOR_RIGHT:
					x = translate.x + size.x - LINE_INFO[ INFO_LINE_WIDTH ];
				break;
				
				case ANCHOR_HCENTER:
					x = translate.x + ( ( size.x - LINE_INFO[ INFO_LINE_WIDTH ] ) >> 1 );
				break;
				
				default:
					x = translate.x;
			}

			// ajusta a posi��o do texto de acordo com o alinhamento vertical
			verticalAlignment = LINE_INFO[ INFO_LINE_ALIGN_VERTICAL ];
			switch ( verticalAlignment ) {
				case ANCHOR_BOTTOM:
					y += LINE_INFO[ INFO_LINE_HEIGHT ] - fontHeight;
				break;
				
				case ANCHOR_VCENTER:
					y += ( LINE_INFO[ INFO_LINE_HEIGHT ] - fontHeight ) >> 1;
				break;
			}

			// o seguinte teste � utilizado para que s� as linhas a partir da �rea vis�vel sejam desenhadas
			if ( y + LINE_INFO[ INFO_LINE_HEIGHT ] >= translate.y ) {
				for ( int charIndex = LINE_INFO[ INFO_LINE_START ]; ( charIndex < LINE_INFO[ INFO_LINE_END ] ); ++charIndex ) {
					final char c = charBuffer[ charIndex ];
					
					switch ( c ) {
						case 0: // n�o desenha caso o caracter n�o esteja definido.
						break;

						case TAG_START:
							byte type = TAG_TYPE_NONE;
							int tagEndIndex = charIndex + 1;
							while ( charBuffer[ tagEndIndex ] != TAG_END ) {
								switch ( type ) {
									case TAG_TYPE_NONE:
										bufferType.append( charBuffer[ tagEndIndex ] );
										if ( charBuffer[ tagEndIndex ] == TAG_SEPARATOR ) {
											final String tagType = bufferType.toString();
											if ( tagType.equals( TAG_IMAGE ) )
												type = TAG_TYPE_IMAGE;
										}
										break;
									case TAG_TYPE_IMAGE:
										bufferNumber.append( charBuffer[ tagEndIndex ] );
										break;
								}
								++tagEndIndex;
							} // fim while ( textArray[ tagEndIndex ] != TAG_END )
							charIndex = tagEndIndex;		          	    

							if ( type == TAG_TYPE_IMAGE ) {
								final int index = Integer.parseInt( bufferNumber.toString() );
								
								// verifica se o �ndice � v�lido
								try {
									final int drawableWidth = drawables[ index ].getWidth();
									int drawableHeight = drawables[ index ].getHeight();
									if ( y + drawableHeight > yLimit ) {
										drawableHeight = yLimit - y;
										imageEnd = true;
									} else {
										drawableHeight += y - position.y;
									}

									if ( verticalAlignment == ANCHOR_TOP ) {
										drawables[ index ].setPosition( x - translate.x, y - translate.y );
									} else if ( verticalAlignment == ANCHOR_VCENTER ) {
										drawables[ index ].setPosition( x - translate.x, y - translate.y + ( ( fontHeight - drawables[ index ].getHeight() ) >> 1 ) );
									} else {
										drawables[ index ].setPosition( x - translate.x, y - translate.y + fontHeight - drawables[ index ].getHeight() );
									}
									drawables[ index ].draw( g );
									x += drawableWidth;
								} catch ( Exception e ) {
									//#if DEBUG == "true"
//# 									// desenha uma �rea vermelha e escreve o �ndice do drawable n�o encontrado
//# 									final String text = "<IMG_" + index + ">";
//# 									final int textWidth = font.getTextWidth( text );
//# 									g.setColor( 0xff0000 );
//# 									g.fillRect( x, y, textWidth, fontHeight );
//# 
//# 									drawString( g, text, x, y );
//# 									x += textWidth;
									//#endif									
								}
							} // fim if ( type == TAG_TYPE_IMAGE )
							bufferNumber.delete( 0, bufferNumber.length() );
							bufferType.delete( 0, bufferType.length() );
						break; // fim case TAG_START

						default:
							drawChar( g, c, x, y );
							x += font.getCharWidth( c );
						break;

					} // fim switch ( c ) 
				} // fim for ( int charIndex = LINE_INFO[ INFO_LINE_START ]; ( charIndex < LINE_INFO[ INFO_LINE_END ] ); ++charIndex )
			} // fim if ( y + LINE_INFO[ INFO_LINE_HEIGHT ] >= translate.y )

			// posiciona a vari�vel y no topo da pr�xima linha (o c�lculo para cada tipo de alinhamento vertical varia)
			switch ( verticalAlignment ) {
				case ANCHOR_TOP:
					y += LINE_INFO[ INFO_LINE_HEIGHT ];
				break;

				case ANCHOR_VCENTER:
					y += LINE_INFO[ INFO_LINE_HEIGHT ] - ( ( LINE_INFO[ INFO_LINE_HEIGHT ] - fontHeight ) >> 1 );
				break;

				case ANCHOR_BOTTOM:
					y += fontHeight;
				break;
			} // fim switch ( verticalAlignment )
		} // fim for ( currentLine = initialLine; currentLine < totalLines && !imageEnd; ++currentLine )
		popClip( g );

		textMutex.release();
    } // fim do m�todo paint( Graphics )
	
	 
	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		if ( updatables != null ) {
			for ( int i = 0; i < updatables.length; ++i ) {
				updatables[ i ].update( delta );
			}
		}
	} // fim do m�todo update( int )
	

	/**
	 * 
	 * @return 
	 */
	public final short[][] getLinesInfo() {
		return linesInfo;
	} // fim do m�todo getLinesInfo()

	 
	/**
	 * Obt�m a altura total do texto.
	 * @return altura total do texto, em pixels (pode exceder a altura definida do label).
	 */
	public int getTextTotalHeight() {
		int height = 0;
		for ( int i = 0; i < linesInfo.length; ++i )
			height += linesInfo[ i ][ INFO_LINE_HEIGHT ];
		
		return height;
	} // fim do m�todo getTextTotalHeight()
	
	
	/**
	 * Define o offset na posi��o inicial de desenho do texto.
	 * @param offset offset na posi��o inicial de desenho do texto, em pixels.
	 */
	public void setTextOffset( int offset ) {
		textOffset = offset;
		
		initialLineOffset = 0;
		int i = 0;
		for ( ; i < linesInfo.length; ++i ) {
			if ( initialLineOffset - linesInfo[ i ][ INFO_LINE_HEIGHT ] <= textOffset )
				break;
			
			initialLineOffset -= linesInfo[ i ][ INFO_LINE_HEIGHT ];
		}
		
		// armazena a primeira linha vis�vel, de forma a otimizar o desenho do RichLabel
		initialLine = ( short ) i;
	} // fim do m�todo setTextOffset( int )
	
	
	/** 
	 * Retorna a linha a partir da qual o texto come�a a ser desenhado.
	 * @return 
	 * @see #setInitialLine(int)
	 */
	public final short getInitialLine() {
		return initialLine;
	}
	
	
	/** 
	 * Determina a linha a partir da qual o texto come�a a ser desenhado.
	 * @param i 
	 * @see #getInitialLine
	 */
	public final void setInitialLine( int i ) {
		initialLine = ( short ) i;
		
		initialLineOffset = 0;
		for( int j = 0 ; j < i ; ++j )
			initialLineOffset -= linesInfo[ j ][ INFO_LINE_HEIGHT ];
	}
	
	
	/**
	 * Define o texto do label, com op��o para reformatar o texto ou n�o. Caso o texto deva ser reformatado, suas
	 * dimens�es n�o ser�o alteradas - equivalente � chamada de <code>setText( text, formatText, false )</code>.
	 * 
	 * @param text texto do label.
	 * @param formatText indica se o texto deve ser reformatado, de forma a preencher de forma �tima a nova �rea de texto.
	 * @see #setText(String)
	 * @see #setText(String, boolean, boolean)
	 */
	public final void setText( String text, boolean formatText ) {
		setText( text, formatText, false );
	}
	
	
	/**
	 * Define o texto do label.
	 * @param text texto do label.
	 * @param formatText indica se o texto deve ser reformatado, de forma a preencher de forma �tima a nova �rea de texto.
	 * @param autoSize indica se as dimens�es do texto devem ser redefinidas automaticamente. Esse par�metro � desprezado
	 * caso <code>formatText</code> seja false.
	 * @see #setText(String)
	 * @see #setText(String, boolean)
	 */
	public void setText( String text, boolean formatText, boolean autoSize ) {
		// evita que o texto seja desenhado e/ou alterado enquanto o texto est� sendo reformatado, evitando-se assim
		// problemas de inconsist�ncia que podem gerar travamentos. Durante o construtor, textMutex � null, por isso
		// a verifica��o do objeto.
		if ( textMutex != null )
			textMutex.acquire();
		
		if ( text == null )
			text = "";

		charBuffer = text.toCharArray();
		
		if ( formatText )
			formatText( autoSize );

		// notifica todas as poss�veis threads esperando para alterar o texto, e volta a permitir desenhos do texto
		if ( textMutex != null )
			textMutex.release();
	} // fim do m�todo setText( String, boolean )

	
	public int getTextOffset() {
		return textOffset;
	}
	
}
 
