/*
 * FocusListener.java
 *
 * Created on October 26, 2007, 09:23 PM
 *
 */

package br.com.nanogames.components.userInterface.form;

/**
 *
 * @author Peter
 */
public interface FocusListener {

	public void focusGained();
	
	public void focusLost();
}
