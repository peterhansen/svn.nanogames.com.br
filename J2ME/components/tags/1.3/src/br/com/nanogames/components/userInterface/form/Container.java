/**
 * Container.java
 * 
 * Created on 5/Nov/2008, 13:59:22
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.layouts.Layout;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public class Container extends Component {
	
	/** Tipo de adi��o autom�tica da barra de scroll: n�o adiciona barra de scroll. */
	public static final byte AUTO_SCROLLBAR_NONE			= 0; 
	public static final byte AUTO_SCROLLBAR_HORIZONTAL		= 1;
	public static final byte AUTO_SCROLLBAR_VERTICAL		= 2;
	public static final byte AUTO_SCROLLBAR_BOTH			= AUTO_SCROLLBAR_HORIZONTAL | AUTO_SCROLLBAR_VERTICAL;
	
	/***/
    protected boolean scrollableX;
	
	/***/
    protected boolean scrollableY;	
	
	/** Barra de rolagem horizontal. */
	protected ScrollBar scrollBarH;

	/** Barra de rolagem vertical. */
	protected ScrollBar scrollBarV;

	/** Offset na posi��o do(s) componente(s) interno(s) devido ao scroll. */
	protected final Point scroll = new Point();

	/** Array que cont�m refer�ncias para todos os componentes, para otimizar a atualiza��o do grupo. */
	protected final Component[] components;
	
	/** Quantidade de componentes no array */
	protected short activeComponents;
	
	protected Layout layout;
	
	
	public Container( int nComponents ) {
		this( nComponents, null );
	}
	
	
	public Container( int nComponents, Layout layout ) {
		super( nComponents );
		
		// por padr�o, os containers n�o podem ter foco direto (somente seus filhos). Esse tratamento � utilizado
		// devido a classes que derivam de container (como FormText), mas que possuem tratamentos espec�ficos de foco,
		// por exemplo
		focusable = false;
		
		components = new Component[ nComponents ];
		
		setLayout( layout );
	}
	
	
	public void destroy() {
		super.destroy();
		
		if ( scrollBarH != null ) {
			scrollBarH.destroy();
			scrollBarH = null;
		}

		if ( scrollBarV != null ) {
			scrollBarV.destroy();
			scrollBarV = null;
		}

		while ( activeDrawables > 0 ) {
			removeDrawable( 0, true );
		}
	}
	
	
	/**
	 * Insere um componente no pr�ximo slot dispon�vel.
	 * 
	 * @param c componente a ser inserido no grupo.
	 * @return o �ndice onde o componente foi inserido <b>no array de drawables</b>, ou -1 caso n�o haja mais slots dispon�veis.
	 */
	public short insertDrawable( Component c ) {
		//#if DEBUG == "true"
//# 			if ( c.getParent() != null ) {
//# 				throw new IllegalArgumentException( "Component is already contained in Container: " + c.getParent() );
//# 			}
//# 			if ( c instanceof Form ) {
//# 				throw new IllegalArgumentException( "A form cannot be added to a container" );
//# 			}
		//#endif
		
		final short retValue = super.insertDrawable( c );
		
		// caso o componente tenha sido inserido com sucesso e seja atualiz�vel, o insere tamb�m no array de atualiz�veis
		if ( retValue >= 0 ) {
			c.setParent( this );
			Form f = getComponentForm();
			if ( f != null ) {
				f.clearFocusVectors();
			}
			
			components[ activeComponents++ ] = c;
			
			setAutoCalcPreferredSize( true );
		}
			
		return retValue;
	} // fim do m�todo insertDrawable( Component )	
	
	
	/**
	 * Insere um componente no pr�ximo slot dispon�vel.
	 * 
	 * @param c componente a ser inserido no grupo.
	 * @param constraints 
	 * @return o �ndice onde o componente foi inserido <b>no array de drawables</b>, ou -1 caso n�o haja mais slots dispon�veis.
	 */
	public short insertDrawable( Component c, Object constraints ) {
		final short ret = insertDrawable( c );
		
		if ( ret >= 0 && layout != null )
			layout.addLayoutComponent( this, c, constraints );
		
		return ret;
	}
	
	
	public Drawable removeDrawable( Drawable d, boolean destroyComponent ) {
		final Drawable removed = removeDrawable( d );
		
		if ( destroyComponent && ( removed instanceof Component ) )
			( ( Component ) removed ).destroy();
		
		return removed;
	}
	
	
    /**
     * Remove um drawable do grupo. Equivalente � chamada de <code>removeDrawable( index, false )</code>, ou seja
	 * mesmo que o drawable removido seja um componente seu m�todo <code>destroy()</code> <b>n�o</b> � chamado.
     * @param index �ndice do drawable a ser removido no grupo.
     * @return uma refer�ncia para o drawable removido, ou null caso o �ndice seja inv�lido.
	 * @see #removeDrawable(int, boolean)
     */
	public Drawable removeDrawable( int index ) {
		return removeDrawable( index, false );
	} // fim do m�todo removeDrawable( int )	
	
	
    /**
     * Remove um drawable do grupo.
     * @param index �ndice do drawable a ser removido no grupo.
	 * @param destroyComponent caso seja <code>true</code> e o drawable removido seja um Component, chama automaticamente
	 * seu m�todo <code>destroy()</code> (ou seja, o Component j� � retornado no estado destru�do.
     * @return uma refer�ncia para o drawable removido, ou null caso o �ndice seja inv�lido.
	 * @see #removeDrawable(int)
     */
	public Drawable removeDrawable( int index, boolean destroyComponent ) {
		final Drawable removed = super.removeDrawable( index );
		
		// caso o drawable removido seja atualiz�vel, o remove tamb�m da lista de atualiz�veis
		if ( removed instanceof Component ) {
			( ( Component ) removed ).setAutoCalcPreferredSize( true );
			
			// � necess�rio varrer a lista pois os �ndices dos drawables n�o necessariamente s�o os mesmos �ndices
			// dos componentes
			for ( short i = 0; i < activeComponents; ++i ) {
				if ( components[ i ] == removed ) {
					final int LIMIT = components.length - 1;
					for ( int j = i; j < LIMIT; ++j )
						components[ j ] = components[ j + 1 ];

					// reduz o contador de componentes
					--activeComponents;
					// anula a refer�ncia no array para o �ltimo elemento (caso contr�rio, haveria entradas
					// duplicadas no array, podendo causar futuros vazamentos de mem�ria)
					components[ LIMIT ] = null;
					
					break;
				} // fim if ( components[ i ] == removed )
			} // fim for ( short i = 0; i < activeComponents; ++i )
			
			if ( destroyComponent )
				( ( Component ) removed ).destroy();
			
			final Form form = getComponentForm();
			if ( form != null ) {
				form.clearFocusVectors();
				// TODO passar foco para pai, ou para anterior/pr�ximo?
			}			
		} // fim if ( removed instanceof Component )
		
		setAutoCalcPreferredSize( true );
		
		return removed;		
	}


	public boolean setDrawableIndex( int oldIndex, int newIndex ) {
		final Drawable oldDrawable = drawables[ oldIndex ];
		final Drawable newDrawable = drawables[ newIndex ];
		
		// faz a poss�vel altera��o na ordem do array de componentes tamb�m, para garantir que o m�todo getComponentAt(int, int)
		// sempre retorne o componente mais � frente no eixo z na posi��o clicada
		final boolean swapped = super.setDrawableIndex( oldIndex, newIndex );
		if ( swapped ) {
			if ( ( oldDrawable instanceof Component ) && ( newDrawable instanceof Component ) ) {
				short oldComponentIndex = -1;
				short newComponentIndex = -1;
				
				for ( short i = 0; i < activeComponents; ++i ) {
					if ( components[ i ] == oldDrawable ) {
						oldComponentIndex = i;
						if ( newComponentIndex >= 0 )
							break;
					} else if ( components[ i ] == newDrawable ) {
						newComponentIndex = i;
						if ( oldComponentIndex >= 0 )
							break;
					}
				}
				final Component old = components[ oldComponentIndex ];

				if ( oldComponentIndex < newComponentIndex ) {
					for ( int i = oldComponentIndex; i < newComponentIndex; ++i )
						components[ i ] = components[ i + 1 ];
				} else {
					for ( int i = oldComponentIndex; i > newComponentIndex; --i )
						components[ i ] = components[ i - 1 ];
				}

				components[ newComponentIndex ] = old;				
			}
		} // fim if ( swapped )
		return swapped;
	}
	
	
    /**
     * Returns true if the given component is within the hierarchy of this container
     *
     * @param cmp a Component to check
     * @return true if this Component contains in this Container
     */
    public boolean contains( Component cmp ) {
		for ( int i = 0; i < components.length; ++i ) {
			final Component c = components[ i ];
			if ( c == cmp )
				return true;

			if ( c instanceof Container ) {
				if ( ( ( Container ) c ).contains( cmp ) )
					return true;
			}
		}
		return false;
	}
	
	
    /**
     * Returns the Component at a given index
     * 
     * @param index of the Component you wish to get
     * @return a Component
     * @throws ArrayIndexOutOfBoundsException if an invalid index was given.
     */
    public Component getComponentAt( int index ) {
        return components[ index ];
    }	
	
	
    /**
     * Returns the number of components
     * 
     * @return the Component count
     */
    public final short getComponentCount() {
        return activeComponents;
    }	

	
	/**
	 * Obt�m o componente presente numa posi��o x,y do cont�iner. Observa��o: somente Components podem ser retornados
	 * nessa busca, ou seja, caso haja um Drawable na posi��o x,y recebida que n�o seja um Component, ele <b>n�o</b> � retornado.
	 * 
	 * @param x posi��o x do ponteiro.
	 * @param y posi��o y do ponteiro.
	 * @return o componente presente na posi��o x,y (pode ser o pr�prio cont�iner), ou <code>null</code> caso o ponto n�o
	 * esteja dentro da �rea do cont�iner.
	 */
	public Component getComponentAt( int x, int y ) {
		return getComponentAt( x, y, null );
	}		
	
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param diff
	 * @return
	 */
	protected Component getComponentAt( int x, int y, Point diff ) {
		if ( contains( x, y ) ) {
			x -= position.x;
			y -= position.y;
			
			if ( scrollBarV != null && scrollBarV.contains( x, y ) ) {
				updateDiff( diff, false );
				return scrollBarV;
			}			
			
			if ( scrollBarH != null && scrollBarH.contains( x, y ) ) {
				updateDiff( diff, false );
				return scrollBarH;
			}			
			
			x -= getScrollX();
			y -= getScrollY();
			
			for ( int i = activeComponents - 1; i >= 0; --i ) {
				Component c = components[ i ];
			
				if ( c instanceof Container ) {
					// pode haver mais de 1 cont�iner dentro de outro - n�o interrompe a busca caso n�o ache um componente
					// na posi��o x,y recebida dentro de um deles
					c = ( ( Container ) c ).getComponentAt( x, y, diff );
					if ( c != null ) {
						updateDiff( diff, true );
						return c;
					}
				} else {
					if ( c.contains( x, y ) ) {
						updateDiff( diff, true );
						
						return c;
					}
				}
			}
			
			return this;
		}
		
		return null;		
	}
	
	
	private final void updateDiff( Point diff, boolean addScroll ) {
		if ( diff != null ) {
			if ( addScroll ) {
				diff.x += position.x + getScrollX();
				diff.y += position.y + getScrollY();
			} else {
				diff.x += position.x;
				diff.y += position.y;
			}
		}		
	}
	

	/**
	 * @inheritDoc
	 */
	public boolean isScrollableX() {
		return scrollableX;
//		return scrollableX && getPreferredWidth() > getWidth(); // TODO testar
//		return scrollableX && getMaximumWidth() > getWidth();
	}


	/**
	 * @inheritDoc
	 */
	public boolean isScrollableY() {
		return scrollableY; // TODO testar
//		return scrollableY && getPreferredHeight() > getHeight(); // TODO testar
//		return scrollableY && getMaximumHeight() > getHeight();
	}


	/**
	 * Indicates whether the component should/could scroll by default a component
	 * is not scrollable.
	 * 
	 * @return whether the component is scrollable
	 */
	protected boolean isScrollable() {
		return isScrollableX() || isScrollableY();
	}


	/**
	 * Sets whether the component should/could scroll on the X axis
	 * 
	 * @param scrollableX whether the component should/could scroll on the X axis
	 */
	public void setScrollableX( boolean scrollableX ) {
		this.scrollableX = scrollableX;
	}


	/**
	 * Sets whether the component should/could scroll on the Y axis
	 * 
	 * @param scrollableY whether the component should/could scroll on the Y axis
	 */
	public void setScrollableY( boolean scrollableY ) {
		this.scrollableY = scrollableY;
	}


	/**
	 * The equivalent of calling both setScrollableY and setScrollableX
	 * 
	 * @param scrollable whether the component should/could scroll on the 
	 * X and Y axis
	 */
	public final void setScrollable( boolean scrollable ) {
		setScrollableX( scrollable );
		setScrollableY( scrollable );
	}	
	
	
	public void scrollToComponent( Component c ) {
		if ( c != null /*&& c.getParent() == this */ ) {
			if ( isScrollableX() ) { // TODO
				
			}
			
			if ( isScrollableY() ) {
				int absY = getScrollY();
				// caso o componente a ser exibido n�o seja filho direto deste cont�iner, deve-se adicionar a posi��o
				// relativa de cada pai, at� chegar a este cont�iner
				Container p = c.getParent();
				while ( p != this && p != null ) {
					absY += c.getParent().getPosY();
					p = p.getParent();
				}
		
				final int RELATIVE_Y = absY + c.getPosY();
				if ( RELATIVE_Y + c.getHeight() > getHeight() ) {
					setScrollY( getScrollY() + getHeight() - RELATIVE_Y - c.getHeight() );
				} else if ( RELATIVE_Y < 0 ) {
					setScrollY( -c.getPosY() );
				}
			}
			
		}
	}

	
	/**
	 * Returns the width for layout manager purposes, this takes scrolling
	 * into consideration unlike the getWidth method.
	 * 
	 * @return the layout width
	 */
	public int getLayoutWidth() {
		final int WIDTH = super.getLayoutWidth();
		
		if ( isScrollableX() ) {
			final int scrollHeight = scrollBarH == null ? 0 : scrollBarH.getHeight();
			return Math.max( WIDTH + scrollHeight, getPreferredWidth() + scrollHeight );
		} else {
			final Container scrollableParent = getScrollableParent();
			if ( scrollableParent != null && scrollableParent.isScrollableX() ) {
				return Math.max( WIDTH, getPreferredWidth() );
			}
			if ( WIDTH <= 0 ) {
				if ( parent != null ) {
					return Math.min( getPreferredWidth(), getPreferredSize( new Point( parent.getLayoutWidth(), parent.getLayoutHeight() ) ).x );
				}
				return getPreferredWidth();
			}
			return WIDTH;
		}
	}


	/**
	 * Returns the height for layout manager purposes, this takes scrolling
	 * into consideration unlike the getWidth method.
	 * 
	 * @return the layout height
	 */
	public int getLayoutHeight() {
		final int HEIGHT = super.getLayoutHeight();
		
		if ( isScrollableX() ) {
			final int scrollHeight = scrollBarH == null ? 0 : scrollBarH.getHeight();
			return Math.max( HEIGHT, getPreferredHeight() ) + scrollHeight;
		} else {
//			final Container scrollableParent = getScrollableParent();
//			if ( scrollableParent != null && scrollableParent.isScrollableY() ) {
			if ( isScrollableY() ) {
				return Math.max( HEIGHT, getPreferredHeight() );
			}
			if ( HEIGHT <= 1 ) {
				return getPreferredHeight();
			}
			return HEIGHT;
		}
	}



	public void setBorder( Border border, boolean fitSize ) {
		super.setBorder( border, fitSize );

		// atualiza as barras de scroll pois pode ter ocorrido mudan�a do tamanho interno, e com isso � necess�rio
		// reposicionar as barras de scroll (caso existem) de acordo.
		refreshScrollH();
		refreshScrollV();
	}
	
	
    /**
	 * Returns a parent container that is scrollable or null if no parent is 
	 * scrollable.
	 * 
	 * @return a parent container that is scrollable or null if no parent is 
	 * scrollable.
	 */
	private Container getScrollableParent() {
		Container scrollableParent = getParent();
		while ( scrollableParent != null ) {
			if ( scrollableParent.isScrollable() ) {
				return scrollableParent;
			}
			scrollableParent = scrollableParent.getParent();
		}
		return null;
	}	


	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		if ( width < getPreferredWidth() || height < getPreferredHeight() ) {
			refreshLayout();
			
			final Form form = getComponentForm();
			if ( form != null )
				form.scrollToComponent( form.getFocused() );
		}
	}


	public final ScrollBar getScrollBarH() {
		return scrollBarH;
	}


	public void setScrollBarH( ScrollBar scrollBarH ) {
		// anula a refer�ncia anterior da barra de rolagem para o pai (no caso, este cont�iner)
		if ( this.scrollBarH != null )
			this.scrollBarH.setParent( null );

		this.scrollBarH = scrollBarH;
		
		if ( scrollBarH != null ) {
			scrollBarH.setParent( this );
		}		
	}


	public final ScrollBar getScrollBarV() {
		return scrollBarV;
	}


	public void setScrollBarV( ScrollBar scrollBarV ) {
		// anula a refer�ncia anterior da barra de rolagem para o pai (no caso, este cont�iner)
		if ( this.scrollBarV != null )
			this.scrollBarV.setParent( null );

		this.scrollBarV = scrollBarV;
		
		if ( scrollBarV != null ) {
			scrollBarV.setParent( this );
		}
	}
	
	
    /**
     * @inheritDoc
     */
    public Point calcPreferredSize( Point maximumSize ) {
		if ( layout == null )
			return super.calcPreferredSize( maximumSize );
		
        return layout.calcPreferredSize( this, maximumSize );
    }	
	
	
	/**
	 * Atualiza o layout do cont�iner e de seus filhos, levando em considera��o seu tamanho (com as barras de scroll,
	 * se existentes).
	 */
	public void refreshLayout() {
		if ( layout != null )
			layout.layoutContainer( this );

		refreshScrollBars();
		
		for ( short i = 0; i < activeComponents; ++i ) {
			if ( components[ i ] instanceof Container ) {
				( ( Container ) components[ i ] ).refreshLayout();
			}
		}
	}
	
	
	/**
	 * Atualiza ambas as barras de scroll (horizontal e vertical), caso existam.
	 */
	public final void refreshScrollBars() {
		refreshScrollH();
		refreshScrollV();
	}
	
	
	protected void refreshScrollH() {
		// se a barra de scroll n�o existir, n�o h� nada a atualizar
		if ( scrollBarH != null ) {
			if ( isScrollableX() ) {
				final boolean scrollV = isScrollableY() && scrollBarV != null;
				scrollBarH.setSize( getWidth() - ( scrollV ? scrollBarV.getWidth() : 0 ), scrollBarH.getHeight() );
				scrollBarH.setPosition( 0, getLayoutHeight() - scrollBarH.getHeight() );

				scrollBarH.refreshScroll( this );
				scrollBarH.setVisible( true );
			} else {
				scrollBarH.setVisible( true );
			}		
		}
	}


	protected void refreshScrollV() {
		// se a barra de scroll n�o existir, n�o h� nada a atualizar
		if ( scrollBarV != null ) {
//			if ( isScrollableY() ) { // TODO teste
			if ( isScrollableY() && getMaximumHeight() > getHeight() ) {
				final boolean scrollH = isScrollableX() && scrollBarH != null;
				scrollBarV.setSize( scrollBarV.getWidth(), getHeight() - ( scrollH ? scrollBarH.getHeight() : 0 ) );
				scrollBarV.setPosition( getLayoutWidth() - scrollBarV.getWidth(), 0 );

				scrollBarV.refreshScroll( this );
				
				scrollBarV.setVisible( true );
			} else {
				scrollBarV.setVisible( false );
			}
		}
	}


	protected void paint( Graphics g ) {
		translate.addEquals( getScroll() );
	
		super.paint( g );
		
		translate.subEquals( getScroll() );
		
		if ( scrollBarH != null )
			scrollBarH.draw( g );
		if ( scrollBarV != null )
			scrollBarV.draw( g );
	}
	
	
	/**
	 * @inheritDoc
	 */
	public void onPointerPressed( int x, int y ) {
		x -= position.x;
		y -= position.y;

		final Point diff = new Point();
		final Component cmp = getComponentAt( x, y, diff );

		x -= diff.x;
		y -= diff.y;

		if ( cmp == this ) {
			super.onPointerPressed( x, y );
		} else if ( cmp != null ) {
			cmp.onPointerPressed( x, y );
		}
	}
	
	
	/**
	 * Returns the layout manager responsible for arranging this container
	 * 
	 * @return the container layout manager
	 */
	public final Layout getLayout() {
		return layout;
	}


	/**
	 * Sets the layout manager responsible for arranging this container
	 * 
	 * @param layout the specified layout manager
	 */
	public void setLayout( Layout layout ) {
		this.layout = layout;
	}
	
	
	public int getScrollX() {
		return scroll.x;
	}
	
	
	public int getScrollY() {
		return scroll.y;
	}
	
	
	public Point getScroll() {
		return scroll;
	}
	
	
	public final void setScrollX( int x ) {
		scroll.x = NanoMath.clamp( x, getWidth() - getMaximumWidth(), 0 );
		refreshScrollH();
	}
	
	
	public void setScrollY( int y ) {
		scroll.y = NanoMath.clamp( y, getHeight() - getMaximumHeight(), 0 );
		refreshScrollV();
	}
	
	
	public final void setScroll( int x, int y ) {
		scroll.set( x, y );
		refreshScrollBars();
	}
	
	
	public final void setScroll( Point scroll ) {
		setScroll( scroll.x, scroll.y );
	}
	
	
	/**
	 * Returns the absolute position location based on the component hierarchy, this method
	 * calculates a location on the screen for the component rather than a relative
	 * location as returned by getPosition()
	 * 
	 * @see #getPosition
	 */
	public void getAbsolutePos( Point p ) {
		p.addEquals( getScroll() );
		super.getAbsolutePos( p );
	}
	
	
	
	public String getUIID() {
		return "container";
	}
	
}
