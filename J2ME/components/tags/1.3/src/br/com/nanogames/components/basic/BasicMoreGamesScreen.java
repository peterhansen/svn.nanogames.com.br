/**
 * BasicMoreGamesScreen.java
 * �2008 Nano Games.
 *
 * Created on 01/02/2008 16:31:23.
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;


/**
 * 
 * @author Peter
 */
public class BasicMoreGamesScreen extends UpdatableGroup {
	
	private static final byte TOTAL_ITEMS = 4;
	
	public static final byte INDEX_BACK = -1;

	private final MenuListener listener;
	
	private final int id;
	
	private byte currentIndex;
	private final byte TOTAL_GAMES;
	
	
	/**
	 * Aloca uma nova tela de compra de jogos.
	 * 
	 * @param listener listener que receber� os eventos indicando o jogo selecionado para compra, ou volta ao menu
	 * anterior.
	 * @param id �ndice de identifica��o da tela de compra (valor repassado ao listener).
	 * @param path diret�rio que cont�m os arquivos da tela de compra. Os seguintes arquivos devem existir no diret�rio:
	 * <ul>
	 * <li>pattern.png: imagem utilizada como pattern de fundo.</li>
	 * <li>left.png: imagem utilizada para indicar ao jogador a troca para o jogo anterior. Opcional no caso de tela
	 * com apenas um jogo.</li>
	 * <li>right.png: imagem utilizada para indicar ao jogador a troca para o jogo anterior. Opcional no caso de tela
	 * com apenas um jogo.</li>
	 * <li>img_#.png: imagem do jogo - substituir '#' pelo �ndice de cada jogo, iniciando em 0 (zero).</li>
	 * </ul>
	 * @param totalGames quantidade total de jogos.
	 * @throws java.lang.Exception
	 */
	public BasicMoreGamesScreen( MenuListener listener, int id, String path, int totalGames ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		this.listener = listener;
		this.id = id;
		TOTAL_GAMES = ( byte ) totalGames;
	}
		
		
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				listener.onChoose( null, id, INDEX_BACK );
			break;
			
			case ScreenManager.FIRE:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_NUM5:
				listener.onChoose( null, id, currentIndex );
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				setCurrentIndex( ( TOTAL_GAMES + currentIndex - 1 ) % TOTAL_GAMES );
			break;
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				setCurrentIndex( ( currentIndex + 1 ) % TOTAL_GAMES );
			break;
		}
	}


	private final void setCurrentIndex( int index ) {
	}
	
	
}
