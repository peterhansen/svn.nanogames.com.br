/**
 * BasicOptionsScreen.java
 * �2007 Nano Games
 * 
 * Created on 11/12/2007 14:38:40 
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;

/**
 *
 * @author peter
 */
public class BasicOptionsScreen extends BasicMenu {

	public static final byte HAS_TITLE		= 1;
	public static final byte HAS_SOUND		= 2;
	public static final byte HAS_VIBRATION	= 4;
	public static final byte HAS_LANGUAGE	= 8;
	
	/** Dura��o padr�o da vibra��o ao ativ�-la. */
	protected static final short VIBRATION_DEFAULT_TIME = 300;
	
	protected final byte INDEX_SOUND;
	protected final byte INDEX_VIBRATION;
	protected final byte INDEX_LANGUAGE;

	// TODO adicionar controle de volume
	
	protected final short textIndexSoundOn;
	protected final short textIndexSoundOff;
	protected final short textIndexVibrationOn;
	protected final short textIndexVibrationOff;

	protected final short textIndexLanguage;

	protected byte currentLanguageIndex;
	
	protected final int soundToPlayIndex;
	protected final int soundToPlayLoopCount;


	public BasicOptionsScreen( MenuListener listener, int id, Drawable[] entries, int spacing, int backIndex, int soundIndex, int soundOnText, int soundOffText, int vibrationIndex, int vibrationOnText, int vibrationOffText, int soundToPlayIndex, int soundToPlayLoopCount ) throws Exception {
		super( listener, id, entries, spacing, 0, backIndex, null );

		this.soundToPlayIndex = soundToPlayIndex;
		this.soundToPlayLoopCount = soundToPlayLoopCount;

		INDEX_LANGUAGE = -1;
		textIndexLanguage = -1;

		INDEX_SOUND = ( byte ) soundIndex;
		if ( INDEX_SOUND >= 0 ) {
			textIndexSoundOn = ( short ) soundOnText;
			textIndexSoundOff = ( short ) soundOffText;

			updateText( INDEX_SOUND );
		} else {
			textIndexSoundOff = -1;
			textIndexSoundOn = -1;
		}

		INDEX_VIBRATION = ( byte ) vibrationIndex;
		if ( INDEX_VIBRATION >= 0 ) {
			textIndexVibrationOn = ( short ) vibrationOnText;
			textIndexVibrationOff = ( short ) vibrationOffText;

			updateText( INDEX_VIBRATION );
		} else {
			textIndexVibrationOn = -1;
			textIndexVibrationOff = -1;
		}
	}
	
	
	/**
	 * @see #BasicOptionsScreen(MenuListener, int, ImageFont, int[], int, int, int, int, int, int, int)
	 * @throws java.lang.Exception
	 */
	public BasicOptionsScreen( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, int soundIndex, int soundOffText, int vibrationIndex, int vibrationOffTex ) throws Exception {
		this( listener, id, font, entries, backIndex, soundIndex, soundOffText, vibrationIndex, vibrationOffTex, -1, -1 );
	}
	
	
	/**
	 * Cria uma nova inst�ncia de um menu b�sico de op��es.
	 * @param listener
	 * @param id
	 * @param font
	 * @param entries
	 * @param backIndex 
	 * @param soundIndex �ndice da entrada relativa � op��o ligar/desligar som. Caso essa op��o n�o exista, basta
	 * passar valores negativos como argumento.
	 * @param soundOffText �ndice do texto de som desligado. Caso n�o exista a op��o de ligar/desligar som, esse
	 * valor � ignorado.
	 * @param vibrationIndex �ndice da entrada relativa � op��o ligar/desligar vibra��o. Caso essa op��o n�o 
	 * exista, basta passar valores negativos como argumento.
	 * @param vibrationOffText �ndice do texto de vibra��o desligada. Caso n�o exista a op��o de ligar/desligar 
	 * som, esse valor � ignorado.
	 * @param soundToPlayIndex �ndice do som que queremos tocar quando o jogador ativar o som
	 * @throws java.lang.Exception
	 */
	public BasicOptionsScreen( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, int soundIndex, int soundOffText, int vibrationIndex, int vibrationOffText, int soundToPlayIndex, int soundToPlayLoopCount ) throws Exception {
		super( listener, id, font, entries, 0, 0, backIndex );
		
		this.soundToPlayIndex = soundToPlayIndex;
		this.soundToPlayLoopCount = soundToPlayLoopCount;

		INDEX_LANGUAGE = -1;
		textIndexLanguage = -1;

		INDEX_SOUND = ( byte ) soundIndex;
		if ( INDEX_SOUND >= 0 ) {
			textIndexSoundOn = ( short ) entries[ INDEX_SOUND ];
			textIndexSoundOff = ( short ) soundOffText;
			
			updateText( INDEX_SOUND );
		} else {
			textIndexSoundOff = -1;
			textIndexSoundOn = -1;
		}
		
		INDEX_VIBRATION = ( byte ) vibrationIndex;
		if ( INDEX_VIBRATION >= 0 ) {
			textIndexVibrationOn = ( short ) entries[ INDEX_VIBRATION ];
			textIndexVibrationOff = ( short ) vibrationOffText;
			
			updateText( INDEX_VIBRATION );
		} else {
			textIndexVibrationOn = -1;
			textIndexVibrationOff = -1;
		}
	}


	public BasicOptionsScreen( MenuListener listener, int id, ImageFont font, int[] entries, int[] languages, int entriesTypes, int soundOffText, int vibrationOffText, int soundToPlayIndex, int languageTextIndex, int currentLanguageIndex ) throws Exception {
		super( listener, id, font, entries, 0, 0, entries.length - 1 );

		this.soundToPlayIndex = soundToPlayIndex;
		this.soundToPlayLoopCount = 1;

		int index = ( entriesTypes & HAS_TITLE ) == 0 ? 0 : 1;

		if ( ( entriesTypes & HAS_SOUND ) != 0 ) {
			INDEX_SOUND = ( byte ) index++;

			textIndexSoundOn = ( short ) entries[ INDEX_SOUND ];
			textIndexSoundOff = ( short ) soundOffText;

			updateText( INDEX_SOUND );
 		} else {
			INDEX_SOUND = -1;
			textIndexSoundOff = -1;
			textIndexSoundOn = -1;
		}

		if ( ( entriesTypes & HAS_VIBRATION ) != 0 ) {
			INDEX_VIBRATION = ( byte ) index++;

			textIndexVibrationOn = ( short ) entries[ INDEX_VIBRATION ];
			textIndexVibrationOff = ( short ) vibrationOffText;

			updateText( INDEX_VIBRATION );
		} else {
			INDEX_VIBRATION = -1;
			textIndexVibrationOn = -1;
			textIndexVibrationOff = -1;
		}

		if ( ( entriesTypes & HAS_LANGUAGE ) != 0 ) {
			INDEX_LANGUAGE = ( byte ) index;
			textIndexLanguage = ( short ) languageTextIndex;
		} else {
			INDEX_LANGUAGE = -1;
			textIndexLanguage = -1;
		}
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.RIGHT:
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				if ( currentIndex == INDEX_SOUND ) {
					MediaPlayer.setMute( !MediaPlayer.isMuted() );
					MediaPlayer.play( soundToPlayIndex, soundToPlayLoopCount );
					updateText( INDEX_SOUND );
					
					break;
				} else if ( currentIndex == INDEX_VIBRATION ) {
					MediaPlayer.setVibration( !MediaPlayer.isVibration() );
					MediaPlayer.vibrate( VIBRATION_DEFAULT_TIME );
					updateText( INDEX_VIBRATION );
					
					break;
				} else if ( currentIndex == INDEX_LANGUAGE ) {
					
				}

			default:
				super.keyPressed( key );				
		} // fim switch ( key )					
	}
	

	protected void updateText( byte index ) {
		final Label l = ( Label ) getDrawable( index );
		
		if ( index == INDEX_SOUND ) {
			l.setText( MediaPlayer.isMuted() ? textIndexSoundOff : textIndexSoundOn );
		} else if ( index == INDEX_VIBRATION ) {
			l.setText( MediaPlayer.isVibration() ? textIndexVibrationOn : textIndexVibrationOff );
		}
		l.defineReferencePixel( l.getWidth() >> 1, 0 );
		l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
		
		// como a largura do texto pode mudar, atualiza o cursor
		setCurrentIndex( currentIndex );
	}
	
}
