/*
 * DrawableGroup.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class DrawableGroup extends Drawable {
 
	/** Drawables inseridos no grupo. */
	protected final Drawable[] drawables;
	
	/** Quantidade de drawables inseridos no grupo atualmente. */
	protected short activeDrawables;
	 	 
	
    /**
	 * Construtor de DrawableGroup.
	 * @param slots n�mero m�ximo de drawables.
	 */
	public DrawableGroup( int slots ) {
		drawables = new Drawable[ slots ];
	}
	
	
    /**
     * Insere um drawable no �ltimo slot dispon�vel, ou seja, o drawable inserido ocupa inicialmente a posi��o de desenho
	 * mais � frente de todos do grupo. Equivalente � chamada de <i>insertDrawable(drawable,getUsedSlots()</i>
	 * 
     * @param drawable drawable a ser inserido no grupo.
     * @return o �ndice onde o drawable foi inserido, ou -1 caso n�o haja mais slots dispon�veis.
	 * 
	 * @see #insertDrawable(Drawable,int)
     */
	public short insertDrawable( Drawable drawable ) {
		//#if DEBUG == "true"
//# 		if ( drawable == null || drawable == this )
//# 			throw new IllegalArgumentException( "can't insert null drawable or self in a group." );
		//#endif
		
		if ( activeDrawables < drawables.length ) {
			drawables[ activeDrawables ] = drawable;
			return activeDrawables++;
		}
		
		//#if DEBUG == "true"
//# 			throw new RuntimeException( "can't insert drawable: group already full." );
		//#else
			return -1;
		//#endif
		
	} // fim do m�todo insertDrawable( Drawable )
	
	
    /**
     * Insere um drawable no �ndice recebido.
	 * 
     * @param drawable drawable a ser inserido no grupo.
	 * @param index �ndice onde o drawable ser� inserido. Caso o �ndice seja menor que zero, o drawable ser� inserido
	 * no �ndice 0. Caso seja maior ou igual � quantidade de drawables j� inseridos, ele ser� inserido ap�s a �ltima
	 * posi��o ocupada inicialmente. Todos os drawables de �ndices maior ou igual a <i>index</i> (caso existam) s�o
	 * deslocados um �ndice acima.
	 * @return o �ndice onde o drawable foi efetivamente inserido, ou -1 caso n�o haja mais slots dispon�veis.
	 * 
	 * @see #insertDrawable(Drawable)
	 * @see #setDrawableIndex(int, int)
     */
	public final short insertDrawable( Drawable drawable, int index ) {
		final short oldIndex = insertDrawable( drawable );
		
		final short newIndex = ( short ) Math.min( oldIndex, index );
		
		if ( newIndex >= 0 )
			setDrawableIndex( oldIndex, newIndex );
		
		return newIndex;
	}
	
	
	/**
	 * 
	 * @param d
	 * @return
	 */
	public Drawable removeDrawable( Drawable d ) {
		for ( short i = 0; i < activeDrawables; ++i ) {
			if ( drawables[ i ] == d )
				return removeDrawable( i );
		}
		
		return null;
	}
	 
	
    /**
     * Remove um drawable do grupo.
     * @param index �ndice do drawable a ser removido no grupo.
     * @return uma refer�ncia para o drawable removido, ou null caso o �ndice seja inv�lido.
     */
	public Drawable removeDrawable( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
			
			final Drawable removed = drawables[ index ];
			
			if ( removed != null ) {
				final int LIMIT = drawables.length - 1;
				for ( int i = index; i < LIMIT; ++i )
					drawables[ i ] = drawables[ i + 1 ];
				
				--activeDrawables;
				
				// anula a refer�ncia para o �ltimo item do array, para evitar vazamento de mem�ria
				drawables[ LIMIT ] = null;
			}
			
			return removed;
			
		//#if DEBUG == "true"
//# 		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 			
//# 			return null;
//# 		}
		//#endif
	} // fim do m�todo removeDrawable( int )

	
	/** 
	 * Substitui o drawable existente no �ndice passado como par�metro
     * @param drawable Drawable a ser inserido no grupo
	 * @param index �ndice onde o drawable ser� inserido
	 * 
	 * @see #insertDrawable(Drawable, int)
	 * @see #removeDrawable(int)
	 */
	public final void setDrawable( Drawable drawable, int index ) {
		removeDrawable( index );
		System.gc();
		insertDrawable( drawable, index );
	}

	
    /**
	 * Altera a ordem de desenho de um drawable. Todos os drawables de �ndice <i>newIndex</i> e superior (caso existam)
	 * s�o deslocados um �ndice acima.
	 * 
	 * @param oldIndex �ndice atual do drawable. Caso seja menor que zero, o valor considerado � zero.
	 * @return <i>true</i>, caso a troca seja feita com sucesso, e <i>false</i> caso contr�rio. A faixa de valores v�lidos 
	 * para ambos os argumentos � de 0 (zero) ao n�mero de elementos inseridos menos um. Exemplo: caso haja 5 drawables 
	 * no grupo, os valores v�lidos s�o 0, 1, 2, 3 e 4. � importante notar que o n�mero de elementos inseridos n�o �
	 * necessariamente igual � capacidade do grupo - pode haver apenas um elemento inserido num grupo com capacidade para
	 * 10 drawables, por exemplo.
	 * @param newIndex novo �ndice do drawable. Caso seja menor que zero, o valor considerado � zero.
	 */
	public boolean setDrawableIndex( int oldIndex, int newIndex ) {
		//#if DEBUG == "true"
//# 		oldIndex = Math.max( oldIndex, 0 );
//# 		newIndex = Math.max( newIndex, 0 );
//# 		
//# 		if ( oldIndex < activeDrawables && newIndex < activeDrawables ) {
//# 			try {
		//#endif
				
				final Drawable old = drawables[ oldIndex ];

				if ( oldIndex < newIndex ) {
					for ( int i = oldIndex; i < newIndex; ++i )
						drawables[ i ] = drawables[ i + 1 ];
				} else {
					for ( int i = oldIndex; i > newIndex; --i )
						drawables[ i ] = drawables[ i - 1 ];
				}

				drawables[ newIndex ] = old;
				return true;
				
		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
//# 
//# 				return false;
//# 			}
//# 		}
//# 		return false;
//# 		
		//#endif
	}
	
	 
    /**
     * 
     * @param index 
     * @return 
     */
	public Drawable getDrawable( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
			return drawables[ index ];
		
		//#if DEBUG == "true"
//# 		} catch ( ArrayIndexOutOfBoundsException e ) {
//# 			e.printStackTrace();
//# 			
//# 			return null;
//# 		}
		//#endif
	} // fim do m�todo getDrawable( int )
	
	
	/**
	 * Obt�m o drawable presente numa posi��o x,y do grupo.
	 * 
	 * @param x posi��o x do ponteiro.
	 * @param y posi��o y do ponteiro.
	 * @return o drawable presente na posi��o x,y (pode ser o pr�prio grupo), ou <code>null</code> caso o ponto n�o
	 * esteja dentro da �rea do grupo.
	 */
	public Drawable getDrawableAt( int x, int y ) {
		if ( contains( x, y ) ) {
			x -= position.x;
			y -= position.y;
			
			for ( int i = activeDrawables - 1; i >= 0; --i ) {
				return drawables[ i ].getDrawableAt( x, y );
			}
			
			return this;
		}
		
		return null;
	}	


	protected void paint( Graphics g ) {
		for ( int i = 0; i < activeDrawables; ++i )
			drawables[ i ].draw( g );
	} // fim do m�todo paint( Graphics )
	 
	
	/**
	 * 
	 * @return capacidade m�xima do grupo.
	 */
	public int getTotalSlots() {
		return drawables.length;
	}
	
	
	/**
	 * 
	 * @return quantidade de drawables inseridos no grupo.
	 */
	public short getUsedSlots() {
		return activeDrawables;
	}

	
	public boolean mirror( int mirrorType ) {
		super.mirror( mirrorType );
		
		if ( ( mirrorType & TRANS_MIRROR_H ) != TRANS_NONE ) {
			for ( int i = 0; i < activeDrawables; ++i ) {
				final Drawable d = drawables[ i ];
				final int previousX = d.getRefPixelX();
				
				d.mirror( mirrorType );
				d.setRefPixelPosition( size.x - previousX, d.getRefPixelY() );
			} // fim for ( int i = 0; i < activeDrawables; ++i )
		}
		
		if ( ( mirrorType & TRANS_MIRROR_V ) != TRANS_NONE ) {
			for ( int i = 0; i < activeDrawables; ++i ) {
				final Drawable d = drawables[ i ];
				final int previousY = d.getRefPixelY();
				
				d.mirror( mirrorType );
				d.setRefPixelPosition( d.getRefPixelX(), size.y - previousY );
			} // fim for ( int i = 0; i < activeDrawables; ++i )				
		}
		
		return true;
	} // fim do m�todo mirror( int )

	
	public boolean rotate( int rotationType ) {
		super.rotate( rotationType );
		
		switch ( rotationType ) {
			case TRANS_ROT90:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final Point previousRefPos = new Point( d.getRefPixelPosition() );
					
					d.rotate( rotationType );
					d.setRefPixelPosition( size.y - previousRefPos.y + ( size.x - size.y ), previousRefPos.x );
				}
			break;
			
			case TRANS_ROT180:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final int previousX = d.getRefPixelX();
					final int previousY = d.getRefPixelY();
					
					d.rotate( rotationType );
					d.setRefPixelPosition( size.x - previousX, size.y - previousY );	
				}				
			break;
			
			case TRANS_ROT270:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final Point previousRefPos = new Point( d.getRefPixelPosition() );
					
					d.rotate( rotationType );
					d.setRefPixelPosition( previousRefPos.y, size.x - previousRefPos.x + ( size.y - size.x ) );
				}				
			break;
		}
		
		return true;
	} // fim do m�todo rotate( int )
	
}

