/**
 * PaletteChanger.java
 * �2008 Nano Games.
 *
 * Created on 14/03/2008 16:43:13.
 */
package br.com.nanogames.components.util;

import br.com.nanogames.components.userInterface.AppMIDlet;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.lcdui.Image;

/**
 * @author Daniel L. Alves
 */

/** Modifica a paleta de cores de uma imagem PNG
 * Para mais informa��es, ver: http://www.libpng.org/pub/png/spec/1.2/png-1.2-pdg.html
 */
public final class PaletteChanger implements Serializable
{
	/** In�cio do chunk da paleta de cores na imagem PNG 
	 * Os primeiros oito bytes s�o a assinatura do arquivo PNG e possuem os valores decimais:
	 * 137 80 78 71 13 10 26 10
	 * O primeiro chunk, IHDR, vem logo em seguida e possui 25 bytes.
	 * O pr�ximo chunk � o PLTE
	 */
	private final byte CHUNK_PLTE_START = 8 + 25;
	
	/** In�cio dos dados do chunk PLTE. Este csegue o formato geral dos chunks das imagens PNG:
	 * Length: 4 bytes
	 * Type: 4 bytes
	 * Data: Cores da paleta na ordem RGB (1 byte para cada componente). Deve ser divis�vel por 3
	 * CRC: 4 bytes
	 */
	private final byte PALETTE_START = CHUNK_PLTE_START + 8;
	
	/** Imagem que o objeto est� manipulando na forma de um array de bytes */
	private byte[] rawImage;

	/** Tamanho em bytes da paleta de cores da imagem PNG */
	private int paletteSize;
	
	/** N�mero de cores na paleta da imagem PNG */
	private int nColors;
		
	/** Tabela para c�lculo do CRC dos chunks da imagem PNG */
	private long[] crcTable;

	
	/**
	 * Construtor
	 * 
	 * @param imagePath path da imagem que ser� manipulada pelo objeto
	 * @throws java.lang.Exception
	 */
	public PaletteChanger( String imagePath ) throws Exception
	{
		// Inicia a leitura da imagem ( ver m�todo read( DataInputStream ) )
		AppMIDlet.openJarFile( imagePath, this );
	}
	
	
	/** 
	 * Preenche a tabela para c�lculo do CRC dos chunks da imagem PNG.
	 * 
	 * @see emptyCRCTable
	 */
	private final void fillCRCTable()
	{
		crcTable = new long[256];
		
		long c;
		for( int i = 0; i < crcTable.length ; ++i )
		{
			c = i;
			for( int k = 0; k < 8; ++k )
			{
				if( ( c & 1 ) == 1 )
					c = 0xedb88320L ^ ( c >> 1 );
				else
					c >>= 1;
			}
			crcTable[i] = c;
		}
	}
	
	
	/** 
	 * Esvazia a tabela para c�lculo do CRC dos chunks da imagem PNG.
	 * 
	 * @see fillCRCTable
	 */
	public final void emptyCRCTable()
	{
		crcTable = null;
		System.gc();
	}

	
	/** Cria uma nova imagem, a partir da recebida no construtor, alterando as cores de sua paleta.
	 * 
	 * @param map Mapeamento que indica como a paleta de cores deve ser modificada.
	 * @return Uma imagem com a paleta de cores modificada.
	 * @see #createImage(int, int)
	 */
	public final Image createImage( PaletteMap[] map )
	{
		// Inicializa a tabela de CRCs
		if( crcTable == null )
			fillCRCTable();
		
		// N�o precisa armazenar o CRC original, pois a cada mudan�a teremos que calcular um novo CRC
		//final byte[] oldCRC = new byte[4];
		//System.arraycopy( rawImage, paletteStart + paletteSize, oldCRC, 0, 4 );

		// Armazena a paleta original da imagem e o CRC do chunk
		final byte[] palette = new byte[paletteSize];
		System.arraycopy( rawImage, PALETTE_START, palette, 0, palette.length );

		// Modifica a paleta original de acordo com o par�metro map
		for( int i = 0; i < map.length ; ++i )
		{
			int paletteEntryIndex;
			
			if( ( paletteEntryIndex = getPaletteEntryIndex( map[i] ) ) != -1 )
			{
				// R
				rawImage[paletteEntryIndex    ] = map[i].newR;
				// G
				rawImage[paletteEntryIndex + 1] = map[i].newG;
				// B
				rawImage[paletteEntryIndex + 2] = map[i].newB;
			}
		}
		
		// Na forma antiga, percorr�amos a paleta de cores procurando um mapeamento para cada uma delas.
		// Invertemos a l�gica para tentar diminuir o processamento
//		for( int i = 0; i < nColors ; ++i )
//		{
//			int mapIndex, aux = paletteStart + ( i * 3 );
//
//			if( ( mapIndex = getMappedColor( rawImage[aux], rawImage[aux + 1], rawImage[aux + 2], map ) ) != -1 )
//			{
//				// R
//				rawImage[aux + 0] = map[mapIndex].newR;
//				// G
//				rawImage[aux + 1] = map[mapIndex].newG;
//				// B
//				rawImage[aux + 2] = map[mapIndex].newB;
//			}
//		}

		// Calcula o novo CRC do chunk
		// chunkPLTEStart + 4 => n�o inclui o campo length no c�lculo do CRC
		// paletteSize + 4 => paleta + nome do chunk ( "PLTE" )
		final byte[] crc = longToByteArray ( calculateCRC( rawImage, CHUNK_PLTE_START + 4, paletteSize + 4 ) );
		
		// Escreve o novo CRC do chunk
		// 4 + i => Um long possui 8 bytes. S� estamos interessados nos �ltimos 4 bytes
		for( int i = 0 ; i < 4 ; ++i )
			rawImage[PALETTE_START + paletteSize + i] = crc[4 + i];

		// Cria a nova imagem
		final Image newImage = Image.createImage( rawImage, 0, rawImage.length );

		// Volta � paleta original
		System.arraycopy( palette, 0, rawImage, PALETTE_START, palette.length );

		// N�o precisa voltar com o CRC original, pois na pr�xima mudan�a teremos que calcular um novo CRC
		// Escreve o CRC antigo
		//System.arraycopy( oldCRC, 0, rawImage, paletteStart + paletteSize, 4 );

		// Retorna a imagem criada
		return newImage;
	}

	
	/** Cria uma nova imagem, a partir da recebida no construtor, interpolando as cores de sua paleta entre
	 * lightColor e darkColor.
	 * 
	 * @param lightColor Cor clara utilizada na interpola��o.
	 * @param darkColor Cor escura utilizada na interpola��o.
	 * @return Uma imagem com a paleta de cores modificada.
	 * @see #createImage(PaletteMap[])
	 */
	public final Image createImage( int lightColor, int darkColor )
	{
		//#ifdef DEBUG
		try {
		//#endif
		// Inicializa a tabela de CRCs
		if( crcTable == null )
			fillCRCTable();
		
		// N�o precisa armazenar o CRC original, pois a cada mudan�a teremos que calcular um novo CRC
		//final byte[] oldCRC = new byte[4];
		//System.arraycopy( rawImage, paletteStart + paletteSize, oldCRC, 0, 4 );

		// Armazena a paleta original da imagem e o CRC do chunk
		final byte[] palette = new byte[paletteSize];
		System.arraycopy( rawImage, PALETTE_START, palette, 0, palette.length );
		
		final int lightR = ( lightColor & 0x00FF0000 ) >> 16;
		final int lightG = ( lightColor & 0x0000FF00 ) >>  8;
		final int lightB = lightColor & 0x000000FF;
		final int darkR = ( darkColor & 0x00FF0000 ) >> 16;
		final int darkG = ( darkColor & 0x0000FF00 ) >>  8;
		final int darkB = darkColor & 0x000000FF;
		
		// Modifica a paleta original de acordo com o par�metro map
		int paletteEntryIndex = PALETTE_START;
		for( int i = 0; i < nColors ; ++i )
		{
			// Otimiza a divis�o por 255 com >> 8, ignorando a falta de precis�o, j� que esta ser� muito pequena
			final int r = /*((*/ 0x000000FF & rawImage[ paletteEntryIndex     ];// ) * lightR ) >> 8;
			final int g = /*((*/ 0x000000FF & rawImage[ paletteEntryIndex + 1 ];// ) * lightG ) >> 8;
			final int b = /*((*/ 0x000000FF & rawImage[ paletteEntryIndex + 2 ];// ) * lightB ) >> 8;
			final int luminance = ( ( r * 30 ) + ( g * 59 ) + ( b * 11 ) ) >> 8;

			// R
			rawImage[paletteEntryIndex    ] = ( byte )NanoMath.lerpInt( darkR, lightR, luminance );
			// G
			rawImage[paletteEntryIndex + 1] = ( byte )NanoMath.lerpInt( darkG, lightG, luminance );
			// B
			rawImage[paletteEntryIndex + 2] = ( byte )NanoMath.lerpInt( darkB, lightB, luminance );
			
			// Vai para a pr�xima cor da paleta
			paletteEntryIndex += 3;
		}

		// Calcula o novo CRC do chunk
		// chunkPLTEStart + 4 => n�o inclui o campo length no c�lculo do CRC
		// paletteSize + 4 => paleta + nome do chunk ( "PLTE" )
		final byte[] crc = longToByteArray ( calculateCRC( rawImage, CHUNK_PLTE_START + 4, paletteSize + 4 ) );
		
		// Escreve o novo CRC do chunk
		// 4 + i => Um long possui 8 bytes. S� estamos interessados nos �ltimos 4 bytes
		for( int i = 0 ; i < 4 ; ++i )
			rawImage[PALETTE_START + paletteSize + i] = crc[4 + i];

		// Cria a nova imagem
		final Image newImage = Image.createImage( rawImage, 0, rawImage.length );

		// Volta � paleta original
		System.arraycopy( palette, 0, rawImage, PALETTE_START, palette.length );

		// N�o precisa voltar com o CRC original, pois na pr�xima mudan�a teremos que calcular um novo CRC
		// Escreve o CRC antigo
		//System.arraycopy( oldCRC, 0, rawImage, paletteStart + paletteSize, 4 );

		// Retorna a imagem criada
		return newImage;
		
		//#ifdef DEBUG
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			return null;
		}
		//#endif
	}
	
	
	/** Retorna o �ndice do in�cio da cor da paleta em rawImage que est� mapeada em map.
	 * 
	 * @param map O mapeamento de cores que cont�m a cor procurada na paleta
	 * @return O �ndice do in�cio da cor da paleta em rawImage ou -1 caso a cor n�o tenha sido encontrada
	 */
	private final int getPaletteEntryIndex( PaletteMap map )
	{
		// Percorre a paleta de cores da imagem
		for( int i = 0 ; i < nColors ; ++i )
		{
			final int aux = PALETTE_START + ( i * 3 );
			
			// Verifica se encontramos a cor mapeada
			if( ( rawImage[aux] == map.srcR ) && ( rawImage[aux + 1] == map.srcG ) && ( rawImage[aux + 2] == map.srcB ) )
				return aux;
		}
		// A cor mapeada n�o existe na paleta de cores da imagem
		return -1;
	}
	
	
	// Na forma antiga, percorr�amos a paleta de cores procurando um mapeamento para cada uma delas.
	// Invertemos a l�gica para tentar diminuir o processamento
//	private final int getMappedColor( byte r, byte g, byte b, PaletteMap[] map )
//	{
//		for( int i = 0; i < map.length; ++i )
//		{
//			if( ( map[i].srcR == r ) && ( map[i].srcG == g ) && ( map[i].srcB == b ) )
//				return i;
//		}
//		return -1;
//	}

	
	public final void write( DataOutputStream output ) throws Exception
	{
	}

	
	public final void read( DataInputStream input ) throws Exception
	{
		// Preenche o array din�mico
		final DynamicByteArray array = new DynamicByteArray();
		array.readInputStream( input );

		// Obt�m os dados formatados
		rawImage = array.getData();
		
		// Armazena o tamanho da paleta e o n�mero de cores contidas nela
		paletteSize = byteArrayToIntArray( rawImage, CHUNK_PLTE_START, 4 )[0];
		nColors = paletteSize / 3;
	}
	
	
	/** Converte um array de bytes em um array de n�meros inteiros.
	 * 
	 * @param bytes Array de bytes que ser� convertido
	 * @param offset �ndice do array bytes a partir do qual a convers�o deve come�ar
	 * @param lenght Quantos bytes ser�o convertidos. Deve ser m�ltiplo de 4
	 * @return Um array de inteiros contendo a convers�o dos bytes recebidos como par�metro
	 * @throws IllegalArgumentException Caso length n�o seja m�ltiplo de 4
	 */
	private final int[] byteArrayToIntArray( byte[] bytes, int offset, int length ) throws IllegalArgumentException
	{
		// Lembrando que "& 2" == "% 4"
		if( ( length & 2 ) != 0 )
		{
			//#if DEBUG == "true"
//# 				throw new IllegalArgumentException( "O par�metro length deve ser m�ltiplo de 4" );
			//#else
			throw new IllegalArgumentException();
			//#endif
		}
				
		int[] ints = new int[length >> 2];

		int intcount = 0;
		for( int bytecount = offset ; bytecount < offset + length ; bytecount += 4 )
		{
			ints[intcount++] =
				( ( ( ( int ) ( bytes[bytecount    ] ) ) << 24 ) ) |
				( ( ( ( int ) ( bytes[bytecount + 1] ) ) << 16 ) ) |
				( ( ( ( int ) ( bytes[bytecount + 2] ) ) <<  8 ) ) |
				( ( ( ( int ) ( bytes[bytecount + 3] ) ) ) );
		}
		return ints;
	}

	
	/** Calcula o CRC do chunk passado como par�metro.
	 * 
	 * @param array Array de bytes que cont�m o chunk para o qual deseja-se calcular o CRC
	 * @param offset �ndice do byte onde come�am os dados que devem ser levados em conta no c�lculo do CRC
	 * @param length Quantos bytes a partir de offset devemos utilizar no c�lculo do CRC
	 * @return O CRC calculado para o chunk
	 */
	private final long calculateCRC( byte[] array, int offset, int length )
	{
		long crc = 0xFFFFFFFFL;
		
		for( int i = 0 ; i < length ; ++i )
			crc = crcTable[ ( int )( ( crc ^ array[ offset + i ] ) & 0xFF ) ] ^ ( crc >> 8 );

		return crc ^ 0xFFFFFFFFL;
	}
	
	
	/** Converte um n�mero long em um array de bytes 
	 * @param l N�mero a ser convertido
	 * @return N�mero recebido como par�metro convertido em um array de bytes
	 */
	private final byte[] longToByteArray( long l )
	{
		final byte[] bytes = new byte[8];

		bytes[0] = ( byte )( l >> 56 );
		bytes[1] = ( byte )( ( l & 0x00FF000000000000L ) >> 48 );
		bytes[2] = ( byte )( ( l & 0x0000FF0000000000L ) >> 40 );
		bytes[3] = ( byte )( ( l & 0x000000FF00000000L ) >> 32 );
		bytes[4] = ( byte )( ( l & 0x00000000FF000000L ) >> 24 );
		bytes[5] = ( byte )( ( l & 0x0000000000FF0000L ) >> 16 );
		bytes[6] = ( byte )( ( l & 0x000000000000FF00L ) >>  8 );
		bytes[7] = ( byte )( l & 0x00000000000000FFL );
		
		return bytes;
	}
}
