/*
 * DrawableImage.java
 *
 * Created on May 4, 2007, 10:02 AM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.ImageLoader;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class DrawableImage extends Drawable {
	
	protected final Image image;

	/** 
	 * Largura real da imagem - esse valor � independente do tamanho do drawable, utilizado para realizar as opera��es 
	 * de desenho no caso de transforma��es.
	 */
	protected final short IMAGE_WIDTH;

	/** 
	 * Altura real da imagem - esse valor � independente do tamanho do drawable, utilizado para realizar as opera��es 
	 * de desenho no caso de transforma��es.
	 */
	protected final short IMAGE_HEIGHT;
	
	
	//#if USE_MIDP2_SPRITE == "true"
//# 	
//# 	/** Refer�ncia para um objeto da classe sprite, que serve para desenhar de forma otimizada no caso de serem aplicadas
//# 	 * transforma��es na imagem. Utilizando-se apenas a imagem, ocorrem erros de desenho e/ou travamentos ao rotacionar 
//# 	 * imagens em aparelhos Samsung e Siemens.
//# 	 */
//# 	protected final javax.microedition.lcdui.game.Sprite sprite;
//# 
	//#endif
	
	
	/** Cria uma nova inst�ncia de DrawableImage.
	 * @param image imagem a partir da qual o DrawableImage ser� criado.
	 * @throws java.lang.Exception 
	 * @see #DrawableImage(String)
	 * @see #DrawableImage(DrawableImage)	 
	 */
	public DrawableImage( Image image ) throws Exception {
		this.image = image;
		
		//#if USE_MIDP2_SPRITE == "true"
//# 		sprite = new javax.microedition.lcdui.game.Sprite( image );
		//#endif
		
		IMAGE_WIDTH = ( short ) image.getWidth();
		IMAGE_HEIGHT = ( short ) image.getHeight();
		
		setSize( IMAGE_WIDTH, IMAGE_HEIGHT );
	} // fim do construtor DrawableImage( Image )
	
	
	/**
	 * Cria uma nova inst�ncia de DrawableImage a partir de uma imagem presente no caminho indicado.
	 * @param imagePath caminho da imagem.
	 * @throws java.lang.Exception caso haja erro ao alocar a imagem, ou o endere�o seja inv�lido.
	 * @see #DrawableImage(Image)
	 * @see #DrawableImage(DrawableImage)
	 */
	public DrawableImage( String imagePath ) throws Exception {
		this( ImageLoader.loadImage( imagePath ) );
		System.gc();
	}
	
	
	/**
	 * Cria uma c�pia de um DrawableImage. Atributos como posi��o, tamanho, visibilidade e etc N�O n�o copiados.
	 * @param dImage DrawableImage a ser copiado.
	 * @throws java.lang.Exception caso dImage seja inv�lido.
	 * @see #DrawableImage(String)
	 * @see #DrawableImage(Image)
	 */
	public DrawableImage( DrawableImage dImage ) throws Exception {
		this( dImage.image );
	}

	
	protected void paint( Graphics g ) {
		// FIXME erro no posicionamento interno da imagem em algumas combina��es de rota��o e espelhamento
		// FIXME rota��o de imagens e sprites n�o-quadrados apresenta erro de clip em aparelhos Samsung (mesmo utilizando Sprite MIDP2)
		//#if USE_MIDP2_SPRITE == "true"
//# 			sprite.setTransform( transformMIDP );
//# 			sprite.setPosition( translate.x, translate.y );
//# 			sprite.paint( g );
		//#else
		if ( transform == TRANS_NONE )
			g.drawImage( image, translate.x, translate.y, 0 );
		else
			g.drawRegion( image, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, transformMIDP, translate.x, translate.y, 0 );
		//#endif
	}


	//#if USE_MIDP2_SPRITE == "true"
//# 	public void defineReferencePixel( int refPixelX, int refPixelY ) {
//# 		super.defineReferencePixel( refPixelX, refPixelY );
//# 		
//# 		sprite.defineReferencePixel( refPixelX, refPixelY );
//# 	}
	//#endif
	
}
