/*
 * AppMIDlet.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */
package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.InputStream;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;


/**
 *
 * @author peter
 */
public abstract class AppMIDlet extends MIDlet {

	/** Dura��o m�xima padr�o de um frame, em milisegundos */
	protected static final byte MAX_FRAME_TIME_DEFAULT = 100;
	
	protected final short MAX_FRAME_TIME;
	
	protected static AppMIDlet instance;
	
	/** textos utilizados no aplicativo */
	protected static String[] texts;
	
	// perfis de fabricantes
	public static final byte VENDOR_GENERIC = 0;
	public static final byte VENDOR_NOKIA = 1;
	public static final byte VENDOR_MOTOROLA = 2;
	public static final byte VENDOR_SAMSUNG = 3;
	public static final byte VENDOR_LG = 4;
	public static final byte VENDOR_SONYERICSSON = 5;
	public static final byte VENDOR_SIEMENS = 6;
	public static final byte VENDOR_HTC = 7;
	public static final byte VENDOR_BLACKBERRY = 8;
	public static final byte VENDOR_VODAFONE = 9;
	public static final byte VENDOR_SAGEM_GRADIENTE = 10;
	public static final byte VENDOR_INTELBRAS = 11;
	
	/** perfil do fabricante detectado */
	public final byte VENDOR;
	
	protected ScreenManager manager;
	
	/** Mutex utilizado para sincronizar o m�todo setScreen(int). */
	protected static final Mutex mutexScreen = new Mutex();
	
	/** �ndice da tela ativa atualmente. */
	protected static byte currentScreen = -1;


	// TODO: compartilhar informa��es no RecordStore a respeito dos jogos/aplicativos Nano instalados no aparelho
	/**
	 * Instancia o MIDLet, for�ando um fabricante espec�fico e dessa forma evitando os testes de auto-detec��o.
	 * @param vendor �ndice do fabricante. Utilizar valores negativos para auto-detectar.
	 * @param maxFrameTime 
	 * @see AppMIDlet()
	 */
	protected AppMIDlet( int vendor, int maxFrameTime ) {
		MAX_FRAME_TIME = ( short ) maxFrameTime;

		byte deviceVendor = VENDOR_GENERIC;

		//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif				

		if ( vendor < 0 ) {
			String platform = System.getProperty( "microedition.platform" );

			if ( platform != null ) {
				/** strings que indicam cada fabricante (em algumas marcas, pode haver mais de uma poss�vel string, dependendo do modelo) */
				final String[][] VENDOR_AGENT = new String[][] {
					{}, // gen�rico
					{ "nokia" }, // Nokia
					{ "mot-", "motorola" }, // Motorola
					{ "samsung", "sec-" }, // Samsung
					{ "lg-" }, // LG
					{ "sonyericsson" }, // SonyEricsson
					{ "sie-", "benq-" }, // Siemens
					{ "intent", "htc" }, // HTC, Qtek
					{ "rim" }, // Blackberry
					{ "vodafone" }, // Vodafone TODO confirmar String
					{}, // Sagem/Gradiente
				};

				platform = platform.toLowerCase();
				byte i, j;

				for ( i = 0; i < VENDOR_AGENT.length && deviceVendor == VENDOR_GENERIC; ++i ) {
					for ( j = 0; j < VENDOR_AGENT[i].length; ++j ) {
						if ( platform.indexOf( VENDOR_AGENT[i][j] ) >= 0 ) {
							// encontrou o fabricante (valor da vari�vel n�o � atribu�da aqui porque vari�veis "final"
							// n�o podem ser atribu�das em loops, mesmo com break)
							deviceVendor = i;
							break;
						}
					} // fim for ( j = 0; j < VENDOR_AGENT[ i ].length; ++j )
				} // fim for ( i = 0; i < TOTAL_VENDORS; ++i )
			} // fim if ( platform != null )

			if ( deviceVendor == VENDOR_GENERIC ) {
				// aparelho n�o informou corretamente o seu fabricante; tenta a detec��o atrav�s da detec��o de APIs 
				// espec�ficas de cada fabricante. V�rias classes para o mesmo fabricante podem ser testados at� se encontrar
				// um v�lido, pois as APIs podem variar entre modelos e s�ries de modelos de cada fabricante.
				final byte[] vendors = new byte[]{
					VENDOR_LG,
					VENDOR_SAMSUNG,
					VENDOR_SIEMENS,
					VENDOR_MOTOROLA,
					VENDOR_SONYERICSSON,
					VENDOR_VODAFONE,
				};

				for ( int i = 0; i < vendors.length; ++i ) {
					if ( checkVendor( vendors[i] ) ) {
						deviceVendor = vendors[i];
						break;
					}
				}
			} // fim if ( vendor == VENDOR_GENERIC )
		} else {
			// for�a a utiliza��o de um fabricante espec�fico
			deviceVendor = ( byte ) vendor;
		}

		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif			

		VENDOR = deviceVendor;
	} // fim do construtor AppMIDlet( int )

	
	// TODO verificar uso de notifyPaused e etc. Exemplo: Motorola A1200 n�o est� suspendendo ao receber SMS - aparentemente, n�o est� recebendo eventos. Jogos da Gameloft, por�m, conseguem suspender ao receber SMS nesse aparelho.
	/**
	 * Carrega os textos do aplicativo a partir de um arquivo de recurso. Cada linha de texto do arquivo corresponde a 
	 * um texto do aplicativo. Para utilizar quebras de linha no texto, deve-se utilizar o caracter barra invertida (\).
	 * 
	 * @param totalTexts quantidade total de textos a serem carregados.
	 * @param resourceFilename caminho do arquivo que cont�m os textos do jogo.
	 * @throws java.lang.Exception caso o caminho do arquivo seja inv�lido, detectar o fim do arquivo antes de
	 * se ler todos os textos, ou ocorra erro na leitura do arquivo.
	 */
	protected final void loadTexts( int totalTexts, String resourceFilename ) throws Exception {
		//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif				

		// para se utilizar essa vari�vel dentro da classe interna descrita abaixo, � necess�rio que ela seja final
		final int TOTAL_TEXTS = totalTexts;

		openJarFile( resourceFilename, new Serializable() {

			public final void write( DataOutputStream output ) throws Exception {
			}


			public final void read( DataInputStream input ) throws Exception {
				final char[] buffer = new char[ 2048 ];
				int bufferIndex = 0;

				short currentString = 0;

				texts = null;
				texts = new String[ TOTAL_TEXTS ];

				try {
					while ( currentString < TOTAL_TEXTS ) {
						char c = ( char ) input.readUnsignedByte();

						switch ( c ) {
							case '\r':
								break;

							case '\n':
								// chegou ao final de uma string. Caso a linha esteja vazia, utiliza uma string vazia (n�o
								// fazer esse teste lan�a uma exce��o)
								texts[currentString++] = ( bufferIndex > 0 ) ? new String( buffer, 0, bufferIndex ) : "";

								bufferIndex = 0;
								break;

							case '\\':
								buffer[bufferIndex++] = '\n';
								break;

							default:
								buffer[bufferIndex++] = c;
						} // fim switch ( c )
					} // fim while ( currentString < TEXT_TOTAL )				
				} catch ( EOFException eof ) {
					if ( currentString < TOTAL_TEXTS ) {
						// chegou ao final do arquivo antes de ler todos os textos
						//#if DEBUG == "true"
//# 						throw new Exception( "erro ao ler textos: fim de arquivo detectado antes da leitura de todos os textos." );
						//#else
						throw eof;
					//#endif
					} // fim if ( currentString < TOTAL_TEXTS )
				} // fim catch ( EOFException )
			} // fim do m�todo read( DataInputStream )
		} );

	//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		}
	//#endif			

	} // fim do m�todo loadTexts( String )


	/**
	 * Instancia o AppMIDlet, detectando automaticamente o fabricante do aparelho. Equivalente � chamada do construtor
	 * AppMIDLet( -1 )
	 * @see #AppMIDlet(int)
	 */
	public AppMIDlet() {
		this( -1, MAX_FRAME_TIME_DEFAULT );
	} // fim do construtor AppMIDlet()


	/**
	 * Verifica qual o fabricante do aparelho, atrav�s da tentativa de detec��o de APIs espec�ficas.
	 * @param vendor �ndice do fabricante a ser verificado.
	 * @return true, se o fabricante p�de ser determinado atrav�s da detec��o de API espec�fica, ou false caso contr�rio.
	 */
	private final boolean checkVendor( byte vendor ) {
		String[] classes = null;
		switch ( vendor ) {
			case VENDOR_GENERIC:
			case VENDOR_NOKIA:
				return false;

			case VENDOR_MOTOROLA:
				classes = new String[]{
					"com.motorola.phonebook.PhoneBookRecord",
					"com.motorola.phone.Dialer",
					"com.motorola.multimedia.Lighting",
					"com.motorola.extensions.ScalableImage",
					"com.motorola.funlight.FunLight",
					"com.motorola.pim.Contact",
					"com.motorola.graphics.j3d.ActionTable",
				};
				break;

			case VENDOR_SAMSUNG:
				classes = new String[]{
					"com.samsung.util.Acceleration",
					"com.samsung.util.AudioClip",
					"com.samsung.immersion.VibeTonz"
				};
				break;

			case VENDOR_LG:
				classes = new String[]{
					"mmpp.media.BackLight",
					"mmpp.lang.MathFP",
					"mmpp.media.MediaPlayer",
					"mmpp.phone.ContentsManager",
					"mmpp.microedition.lcdui.TextFieldX",
				};
				break;

			case VENDOR_SONYERICSSON:
				classes = new String[]{
					"com.sonyericsson.mmedia.AMRPlayer",
					"com.sonyericsson.util.TempFile"
				};
				break;

			case VENDOR_SIEMENS:
				classes = new String[]{
					"com.siemens.mp.lcdui.Image",
					"com.siemens.mp.game.Light",
					"com.siemens.mp.io.Connection",
					"com.siemens.mp.wireless.messaging.MessageConnection",
				};
				break;

			case VENDOR_VODAFONE:
				classes = new String[]{
					"com.vodafone.v10.system.device.DeviceControl",
				};
				break;
		}

		for ( int i = 0; i < classes.length; ++i ) {
			try {
				Class.forName( classes[i] );

				// se chegou nessa linha (ou seja, n�o lan�ou exce��o), � sinal de que encontrou a String
				return true;
			} catch ( Exception e ) {
			}
		} // fim for ( int i = 0; i < classes.length; ++i )

		return false;
	}


	/**
	 * Indica se uma base de dados j� existe.
	 * 
	 * @param databaseName nome da base de dados.
	 * @param expectedSlots quantidade de slots esperada da base. Valores menores ou iguais a zero ignoram este teste, ou
	 * seja, apenas indica se a base existe.
	 * 
	 * @return true, caso a base j� exista e tenha a quantidade esperada de slots, e false caso contr�rio.
	 * 
	 * @see #createDatabase(String, int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final boolean hasDatabase( String databaseName, int expectedSlots ) {
		final String[] names = RecordStore.listRecordStores();
		RecordStore rs = null;

		if ( names != null ) {
			for ( int i = 0; i < names.length; ++i ) {
				if ( names[ i ].equals( databaseName ) ) {
					try {
						if ( expectedSlots > 0 ) {
							rs = RecordStore.openRecordStore( databaseName, false );
							return ( rs.getNumRecords() == expectedSlots );
						}

						// se chegou nessa linha, significa que n�o importa saber a quantidade de slots na base de dados,
						// somente saber se ela existe.
						return true;
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif

						return false;
					} finally {
						if ( rs != null ) {
							try {
								rs.closeRecordStore();
							} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								e.printStackTrace();
							//#endif
							}
						} // fim if ( rs != null )
					} // fim finally
				} // fim if ( names[ i ].equals( databaseName ) )
			} // fim for ( int i = 0; i < names.length; ++i )
		} // fim if ( names != null )

		return false;
	} // fim do m�todo hasDatabase( String, int )


	/**
	 * Cria uma base de dados com uma quantidade pr�-determinada de slots para grava��o.
	 * 
	 * @param dataBaseName nome da base de dados.
	 * @param numSlots n�mero de slots dispon�veis para grava��o. Caso a base j� exista, mas a quantidade de slots seja 
	 * diferente, a base � recriada com a nova quantidade de slots.
	 * 
	 * @throws javax.microedition.rms.RecordStoreFullException caso n�o haja espa�o livre dispon�vel para criar a base de dados.
	 * @throws java.lang.Exception caso ocorra outro tipo de erro na cria��o da base de dados.
	 * 
	 * @return boolean indicando se a base de dados foi criada. Caso ela j� existisse antes, retorna false.
	 * 
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final boolean createDatabase( String dataBaseName, int numSlots ) throws Exception {
		RecordStore rs = null;

		try {
			if ( hasDatabase( dataBaseName, 0 ) ) {
				rs = RecordStore.openRecordStore( dataBaseName, false );
				if ( numSlots == rs.getNumRecords() ) {
					// base j� existe, com a mesma quantidade de slots 
					return false;
				} else {
					// quantidade de slots � diferente; apaga a base antiga e a recria com a nova quantidade
					rs.closeRecordStore();

					RecordStore.deleteRecordStore( dataBaseName );
				}
			} // fim if ( hasDatabase( dataBaseName, 0 ) )

			rs = RecordStore.openRecordStore( dataBaseName, true );

			for ( int i = 0; i < numSlots; ++i ) {
				rs.addRecord( null, 0, 0 );
			}

			return true;
		} catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new RecordStoreFullException( "Erro ao criar base de dados: n�o h� espa�o dispon�vel." );
			//#else
			throw rsfe;
		//#endif
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif

			throw e;
		} finally {
			if ( rs != null ) {
				try {
					rs.closeRecordStore();
				} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
				}
			} // fim if ( rs != null )
		} // fim finally
	} // fim do m�todo createDatabase( String, int )


	/**
	 * Grava dados numa base de dados.
	 * @param dataBaseName nome da base de dados (precisa j� existir)
	 * @param slot �ndice do slot onde os dados ser�o gravados. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite grava��o nos slots
	 * 1, 2 e 3.
	 * 
	 * @param serializable refer�ncia para o objeto Serializable que gravar� seus dados no slot, atrav�s do seu m�todo
	 * <i>write( DataOutputStream )</i>. N�o � necess�rio chamar o m�todo flush() do DataOutputStream.
	 * 
	 * @throws java.lang.NullPointerException caso <code>serializable</code> seja null.
	 * @throws javax.microedition.rms.RecordStoreNotFoundException caso <code>dataBaseName</code> seja inv�lido ou nulo.
	 * @throws javax.microedition.rms.InvalidRecordIDException caso <code>slot</code> seja inv�lido.
	 * @throws javax.microedition.rms.RecordStoreFullException caso n�o haja espa�o livre dispon�vel para gravar os dados.
	 * @throws java.lang.Exception caso ocorra outro tipo de erro ao abrir ou gravar os dados na base.
	 * 
	 * @see #createDatabase(String,int)
	 * @see #hasDatabase(String,int)
	 * @see #loadData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final void saveData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		if ( serializable == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao gravar na base de dados: serializable n�o pode ser null." );
			//#else
			throw new NullPointerException();
		//#endif
		}

		if ( !hasDatabase( dataBaseName, 0 ) ) {
			//#if DEBUG == "true"
//# 				throw new RecordStoreNotFoundException( "Erro ao gravar dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
			throw new RecordStoreNotFoundException();
		//#endif
		}

		ByteArrayOutputStream byteOutput = null;
		DataOutputStream dataOutput = null;
		RecordStore rs = null;

		try {
			byteOutput = new ByteArrayOutputStream();
			dataOutput = new DataOutputStream( byteOutput );

			serializable.write( dataOutput );

			dataOutput.flush();

			final byte[] data = byteOutput.toByteArray();

			rs = RecordStore.openRecordStore( dataBaseName, false );
			rs.setRecord( slot, data, 0, data.length );
		} catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados: n�o h� espa�o dispon�vel." );
			//#else
			throw new RecordStoreFullException();
		//#endif
		} catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inv�lido: " + slot );
			//#else
			throw new InvalidRecordIDException();
		//#endif
		} finally {
			if ( rs != null ) {
				try {
					rs.closeRecordStore();
				} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif					
				}
			} // fim if ( rs != null )			

			if ( byteOutput != null ) {
				try {
					byteOutput.close();
				} catch ( Exception e ) {
				}
				byteOutput = null;
			} // fim if ( byteOutput != null )

			if ( dataOutput != null ) {
				try {
					dataOutput.close();
				} catch ( Exception e ) {
				}
				dataOutput = null;
			} // fim if ( dataOutput != null )
		} // fim finally
	} // fim do m�todo saveData( String, int, Serializable )


	/**
	 * Apaga o conte�do de um slot de uma base de dados, substituindo seu conte�do por um array de 0 bytes.
	 * 
	 * @param dataBaseName nome da base de dados (precisa j� existir).
	 * @param slot �ndice do slot cujos dados ser�o apagados. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite grava��o nos slots
	 * 1, 2 e 3.
	 * 
	 * @throw javax.microedition.rms.RecordStoreNotFoundException caso <code>dataBaseName</code> seja inv�lido ou nulo.
	 * @throws javax.microedition.rms.InvalidRecordIDException caso <code>slot</code> seja inv�lido.
	 * @throws java.lang.Exception caso ocorra erro ao apagar os dados da base.
	 * dados.
	 * 
	 * @see #createDatabase(String,int)
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #loadData(String,int,Serializable)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final void eraseSlot( String dataBaseName, int slot ) throws Exception {
		if ( hasDatabase( dataBaseName, 0 ) ) {
			RecordStore rs = null;

			try {
				rs = RecordStore.openRecordStore( dataBaseName, false );

				rs.setRecord( slot, new byte[ 0 ], 0, 0 );
			} catch ( InvalidRecordIDException ire ) {
				//#if DEBUG == "true"
//# 				throw new InvalidRecordIDException( "Erro ao ler da base de dados: slot inv�lido: " + slot );
				//#else
				throw ire;
			//#endif
			} finally {
				if ( rs != null ) {
					try {
						rs.closeRecordStore();
					} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
					} // fim catch( Exception e )
				} // fim if ( rs != null )			
			} // fim finally					
		} else {
			//#if DEBUG == "true"
//# 				throw new RecordStoreNotFoundException( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
			throw new RecordStoreNotFoundException();
		//#endif

		}
	} // fim do m�todo eraseSlot( String, int )


	/**
	 * L� os dados dispon�veis num slot de uma base de dados.
	 * 
	 * @param dataBaseName nome da base de dados.
	 * @param slot �ndice do slot de onde os dados ser�o lidos. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots possui como �ndices v�lidos
	 * apenas 1, 2 e 3.
	 * @param serializable refer�ncia para o objeto Serializable que interpretar� o array de bytes lido da base de dados,
	 * atrav�s do seu m�todo <i>read( DataInputStream )</i>.
	 * @throws java.lang.NullPointerException caso <code>serializable</code> seja null.
	 * @throws javax.microedition.rms.RecordStoreNotFoundException caso <code>dataBaseName</code> seja inv�lido ou nulo.
	 * @throws javax.microedition.rms.InvalidRecordIDException caso <code>slot</code> seja inv�lido.
	 * @throws java.lang.Exception caso ocorram outros erros relacionados � abertura da base ou leitura dos dados.
	 * 
	 * @see #createDatabase(String,int)
	 * @see #hasDatabase(String,int)
	 * @see #saveData(String,int,Serializable)
	 * @see #eraseSlot(String,int)
	 * @see #openJarFile(String,Serializable)
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */
	public static synchronized final void loadData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		if ( serializable == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao ler da base de dados: serializable n�o pode ser null." );
			//#else
			throw new NullPointerException();
		//#endif
		}

		if ( !hasDatabase( dataBaseName, 0 ) ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
			throw new RecordStoreNotFoundException();
		//#endif
		}

		ByteArrayInputStream byteInput = null;
		DataInputStream dataInput = null;
		RecordStore rs = null;

		try {
			rs = RecordStore.openRecordStore( dataBaseName, false );

			final byte[] data = rs.getRecord( slot );

			byteInput = new ByteArrayInputStream( data );
			dataInput = new DataInputStream( byteInput );

			serializable.read( dataInput );
		} catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new InvalidRecordIDException( "Erro ao ler da base de dados: slot inv�lido: " + slot );
			//#else
			throw ire;
		//#endif
		} finally {
			if ( rs != null ) {
				try {
					rs.closeRecordStore();
				} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
				} // fim catch( Exception e )
			} // fim if ( rs != null )			

			if ( byteInput != null ) {
				try {
					byteInput.close();
				} catch ( Exception e ) {
				}
				byteInput = null;
			} // fim if ( byteInput != null )

			if ( dataInput != null ) {
				try {
					dataInput.close();
				} catch ( Exception e ) {
				}
				dataInput = null;
			} // fim if ( dataInput != null )
		} // fim finally		
	} // fim do m�todo loadData( String, int, Serializable )


	/**
	 * Abre um arquivo presente no Jar para leitura.
	 * 
	 * @param filename endere�o do arquivo a ser lido.
	 * @param serializable refer�ncia para o objeto Serializable que far� a leitura dos dados do arquivo, atrav�s do seu 
	 * m�todo <i>read( DataInputStream )</i>.
	 * 
	 * @throws java.lang.NullPointerException caso <code>filename</code> seja inv�lido ou ocorra erro na abertura do arquivo.
	 * @throws java.lang.Exception caso ocorra erro na leitura dos dados do arquivo.
	 */
	public static synchronized final void openJarFile( String filename, Serializable serializable ) throws Exception {
		InputStream inputStream = null;
		DataInputStream dataInputStream = null;

		try {
			inputStream = filename.getClass().getResourceAsStream( filename );

 			// a chamada do m�todo getResourceAsStream n�o lan�a exce��o no caso de erro na abertura do arquivo. Em vez
 			// disso, apenas retorna null.
 			if ( inputStream == null ) {
				//#if DEBUG == "true"
//#  				throw new NullPointerException( "Erro ao abrir arquivo: " + filename );
				//#else
				throw new NullPointerException();
				//#endif
			}

			dataInputStream = new DataInputStream( inputStream );

			serializable.read( dataInputStream );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			throw e;
		} finally {
			try {
				if ( dataInputStream != null ) {
					try {
						dataInputStream.close();
					} catch ( Exception ex ) {
					}
					dataInputStream = null;
				}

				if ( inputStream != null ) {
					try {
						inputStream.close();
					} catch ( Exception ex ) {
					}

					inputStream = null;
				}
			} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif				
			}
		} // fim finally				
	} // fim do m�todo openJarFile( String, Serializable )


	public static AppMIDlet getInstance() {
		return instance;
	}


	protected void startApp() throws MIDletStateChangeException {
		if ( instance != this ) {
			instance = this;

			manager = ScreenManager.createInstance( MAX_FRAME_TIME );

			try {
				// carrega os recursos do aplicativo
				loadResources();

				// ap�s carregar os recursos, inicia o gerenciador de tela (n�o � iniciado antes para que todo o
				// processamento seja dedicado ao carregamento dos recursos)
				manager.start();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif

				exit();
			}
		} else {
			manager.showNotify();
		}
	}


	/**
	 * Troca a tela ativa atualmente, tratando a sincroniza��o entre 2 poss�veis chamadas concorrentes do m�todo. Este
	 * m�todo <b>n�o</b> troca efetivamente a tela ativa no <code>ScreenManager</code> - essa troca deve ser  feita dentro 
	 * do m�todo <code>changeScreen</code>, que deve ser estendido pelas aplica��es.
	 * 
	 * @param index �ndice da nova tela ativa.
	 * @see #changeScreen(int)
	 */
	public static synchronized final void setScreen( int index ) {
		if ( mutexScreen.acquire() ) {
			try {
				ScreenManager.setKeyListener( null ); // TODO teste
				ScreenManager.setPointerListener( null );
				ScreenManager.setScreenListener( null );
				
				currentScreen = ( byte ) instance.changeScreen( index );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				//# 				e.printStackTrace();
				//#endif

				return;
			} finally {
				mutexScreen.release();
			}
		}
	//#if DEBUG == "true"
//# 		// TODO o que fazer no caso de n�o conseguir o acesso ao mutex?
//# 		else {
//# 			System.out.println( "AppMIDLet.setScreen: mutexScreen.acquire() retornou false." );
//# 		}
	//#endif
	} // fim do m�todo setScreen( int )


	/** 
	 * Realiza efetivamente a troca da tela ativa. N�o � necess�rio tratar sincroniza��o da troca de tela, pois isto �
	 * feito no m�todo AppMIDlet.setScreen(int). 
	 * 
	 * @param screen �ndice da nova tela ativa.
	 * @return �ndice da tela definida. Este �ndice pode ser diferente do recebido como par�metro. Por exemplo, pode-se
	 * receber como par�metro o �ndice de uma tela de jogo, mas a tela efetivamente exibida ser uma tela de log, indicando
	 * um erro que ocorreu ao tentar alocar a tela de jogo. Nesse caso, a implementa��o do m�todo <code>changeScreen</code>
	 * <b>n�o</b> deve chamar novamente <code>setScreen</code>, pois isso causar� deadlock da aplica��o.
	 * 
	 * @see #setScreen(int)
	 */
	protected abstract int changeScreen( int screen ) throws Exception;


	/**
	 * Este m�todo deve ser estendido pelos MIDlets, e ser� o respons�vel por realizar o carregamento de recursos
	 * como sons, imagens, fontes e quaisquer outros recursos utilizados inicialmente nos aplicativos. O m�todo �
	 * chamado automaticamente na primeira vez que startApp() � executado, numa Thread pr�pria para que o m�todo
	 * startApp() retorne mais rapidamente, evitando que alguns aparelhos informem que o MIDlet n�o est� respondendo.
	 * 
	 * @throws java.lang.Exception 
	 */
	protected abstract void loadResources() throws Exception;


	protected void pauseApp() {
		if ( instance != null && manager != null ) {
			manager.hideNotify();
		}
	}


	protected void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( instance != null && instance.manager != null ) {
			instance.manager.stop();
			instance.manager = null;
		}
		instance = null;

		notifyDestroyed();
	}


	public static final void exit() {
		try {
			instance.destroyApp( true );
		} catch ( Exception e ) {
		//#if DEBUG == "true"
//# 			e.printStackTrace();
		//#endif
		}
	}


	public static byte getVendor() {
		return instance.VENDOR;
	}


	/**
	 * Obt�m um dos textos utilizados no aplicativo.
	 * @param index �ndice do texto.
	 * @return texto de �ndice <i>index</i>.
	 */
	public static final String getText( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
		return texts[ index ];
		
		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			System.out.println( "AppMIDlet.getText: �ndice inv�lido: " + index + " / " + ( texts == null ? 0 : texts.length ) );
//# 			e.printStackTrace();
//# 			
//# 			return null;
//# 		}
		//#endif
	}
}
 
