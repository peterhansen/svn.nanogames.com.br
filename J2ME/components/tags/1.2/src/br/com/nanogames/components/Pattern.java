/*
 * Pattern.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class Pattern extends Drawable implements Updatable {
	
	/** Cor de preenchimento do background. Pode-se utilizar valores negativos para indicar n�o-preenchimento com cor s�lida do fundo. */
	protected int fillColor = -1;
	
	/** Drawable utilizado para preenchimento. */
	protected Drawable fill;
	
	/** Refer�ncia para o fill, caso ele seja atualiz�vel (acelera o teste durante as chamadas de <code>update</code>. */
	protected Updatable updatableFill;
	
	/** Indica se o drawable de preenchimento utiliza transpar�ncia. Caso n�o utilize, � poss�vel otimizar o desenho do pattern, copiando
	 * �reas pintadas em vez de chamar o m�todo <code>draw(Graphics)</code> v�rias vezes.
	 */
//	protected boolean usesTransparency;
	
	/** Modo de desenho do pattern: n�o desenha nada (fillColor e fill n�o est�o definidos). */
	protected static final byte DRAW_MODE_NOTHING				= 0;
	
	/** 
	 * Modo de desenho do pattern: preenche uma regi�o top-left do tamanho do drawable com a cor s�lida, desenha o drawable
	 * nessa regi�o, e ent�o a copia no restante da �rea (fillColor e fill definidos, com fill utilizando transpar�ncia). 
	 */
	protected static final byte DRAW_MODE_FILL_COPY_AREA		= 1;
	
	/** 
	 * Modo de desenho do pattern: preenche uma regi�o top-left do tamanho do drawable com o drawable, depois a copia no 
	 * restante da �rea (fillColor e fill definidos, com fill n�o utilizando transpar�ncia, o que elimina a necessidade 
	 * de preencher a �rea com a cor s�lida, uma vez que o drawable ir� substituir toda a regi�o). 
	 */
	protected static final byte DRAW_MODE_DONT_FILL_COPY_AREA	= 2;
	
	/** 
	 * Modo de desenho do pattern: desenha o drawable por toda a �rea (fillColor n�o definido, e fill definido utilizando
	 * transpar�ncia, o que impede de fazer c�pias, uma vez que o fundo sob o drawable pode n�o ser uniforme). */
	protected static final byte DRAW_MODE_DONT_FILL_DRAW		= 3;
	
	/** 
	 * Modo de desenho do pattern: apenas preenche a �rea do pattern com a cor s�lida definida (fillColor definido, fill
	 * n�o definido). 
	 */
	protected static final byte DRAW_MODE_FILL_ONLY				= 4;
	
//	/** Indica se o m�todo <code>Graphics.copyArea()</code> � suportado pelo aparelho. */
//	public static final boolean COPY_AREA_SUPPORTED;
	
	/** Modo de desenho atual do pattern. */
	protected byte drawMode;
	
	
//	static {
//		boolean supported = false;
//		
//		// FIXME IllegalState Exception durante o desenho do pattern utilizando copyArea ao receber eventos de suspend
//		try {
//			ScreenManager.getScreenBuffer().copyArea( 0, 0, 2, 2, 5, 5, 0 );
//			supported = true;
//		} catch ( Throwable t ) {
//		}
//		
//		COPY_AREA_SUPPORTED = supported;
//	}
	
	
	/**
	 * Cria um novo pattern que utiliza um drawable como preenchimento. Por padr�o, considera-se que este drawable n�o
	 * possui �reas com transpar�ncia, para que possam ser feitas otimiza��es de desenho. Caso ele utilize �reas 
	 * transparentes, deve-se utilizar o m�todo <code>setUsesTransparency(boolean)</code>. Pode-se ainda utilizar um 
	 * drawable com transpar�ncia em conjunto com uma cor s�lida de preenchimento, bastando definir uma cor no m�todo
	 * <code>setFill(int)</code>.
	 * 
	 * @param fill drawable utilizado como preenchimento.
	 * @see #Pattern(int)
	 * @see #setUsesTransparency(boolean)
	 * @see #setFill(Drawable)
	 * @see #setFill(int)
	 */
	public Pattern( Drawable fill ) {
		setFill( fill );
	}
	
	
	/**
	 * Cria um novo pattern que utiliza uma cor s�lida como preenchimento. 
	 * 
	 * @param color cor s�lida utilizada como preenchimento. Caso seja um valor negativo, n�o h� preenchimento de cor s�lida.
	 * @see #Pattern(Drawable)
	 * @see #setFillColor(int)
	 * @see setFill(Drawable)
	 */
	public Pattern( int color ) {
		setFillColor( color );
	}

	
	protected void paint( Graphics g ) {
		switch ( drawMode ) {
			case DRAW_MODE_FILL_ONLY:
				// apenas preenche toda a �rea com a cor s�lida definida
				g.setColor( fillColor );
				g.fillRect( translate.x, translate.y, size.x, size.y );
			break;
			
			case DRAW_MODE_DONT_FILL_DRAW:
			{
				// n�o h� cor de preenchimento definida e drawable usa transpar�ncia; logo, � necess�rio desenhar o drawable
				// por toda a �rea do pattern
				final Point fillSize = fill.getSize();
				
				for ( int y = 0; y < size.y; y += fillSize.y ) {
					for ( int x = 0; x < size.x; x += fillSize.x ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = 0; x < size.x; x += fillSize.x )
				} // fim for ( int y = 0; y < size.y; y += fillSize.y )
			}
			break;
			
//			case DRAW_MODE_FILL_COPY_AREA:
//			{
//				// h� cor de preenchimento definida e drawable utiliza transpar�ncia; preenche cor a cor de preenchimento
//				// uma regi�o top-left das dimens�es do drawable, depois o desenha nessa regi�o, e ent�o copia este padr�o
//				// por toda a regi�o do pattern
//				
//				// preenche uma regi�o top-left das dimens�es do drawable com a cor de fundo
//				final Point fillSize = fill.getSize();
//				g.setColor( fillColor );
//				g.fillRect( translate.x, translate.y, fillSize.x, fillSize.y );			
//				
//				// n�o h� break
//				
//			}
//			case DRAW_MODE_DONT_FILL_COPY_AREA:
//			{
//				// TODO verificar desempenho de copyArea
//				// FIXME algoritmo de copyArea n�o copia �rea completa no caso de viewports diferentes da tela
//				final Rectangle clip = clipStack[ currentStackSize ];
//				
//				final Point fillSize = fill.getSize();
//				// desenha o drawable pela primeira vez
//				fill.setRefPixelPosition( 0, 0 );
//				fill.draw( g );
//				
//				// se pelo menos uma das coordenadas de tranla��o acumulada for negativa, significa que o drawable ser�
//				// desenhado parcialmente horizontalmente e/ou verticalmente. Nesse caso, ele � desenhado 1 ou 2 vezes a
//				// mais, de forma que a �rea copiada abranja toda a dimens�o do drawable.
//				if ( translate.x < 0 || fill.referencePixel.x != 0 ) {
//					fill.setRefPixelPosition( fillSize.x, 0 );
//					fill.draw( g );
//				}
//				if ( translate.y < 0 || fill.referencePixel.y != 0 ) {
//					fill.setRefPixelPosition( 0, fillSize.y );
//					fill.draw( g );
//				}
//				
//				final int LIMIT_X = clip.x + clip.width;
//				final int LIMIT_Y = clip.y + clip.height;
//
//				final int MOD_X = clip.width % fillSize.x;
//				final int MOD_Y = clip.height % fillSize.y;
//
//				final int END_X = LIMIT_X - MOD_X;
//				final int END_Y = LIMIT_Y - MOD_Y;
//				// copia toda a primeira linha horizontal
//				final int FILL_HEIGHT = NanoMath.min( clip.height, fillSize.y );
//				for ( int x = clip.x + fillSize.x; x < END_X; x += fillSize.x ) {				
//					g.copyArea( clip.x, clip.y, fillSize.x, FILL_HEIGHT, x, clip.y, 0 );
//				}
//				if ( MOD_X > 0 )
//					g.copyArea( clip.x, clip.y, MOD_X, FILL_HEIGHT, END_X, clip.y, 0 );
//
//				if ( FILL_HEIGHT == fillSize.y ) {
//					// copia a linha horizontal at� preencher toda a �rea do pattern
//					for ( int y = clip.y + fillSize.y; y < END_Y; y += fillSize.y ) {
//						g.copyArea( clip.x, clip.y, size.x, fillSize.y, clip.x, y, 0 );
//					}			
//					if ( MOD_Y > 0 )
//						g.copyArea( clip.x, clip.y, size.x, MOD_Y, clip.x, END_Y, 0 );
//				}
//			}
//			break;
		} // fim switch ( drawMode )
	} // fim do m�todo paint( Graphics )
	
	
	public void setFill( Drawable fill ) {
		this.fill = fill;
		
		if ( fill instanceof Updatable )
			updatableFill = ( Updatable ) fill;
		else
			updatableFill = null;
		
		updateDrawMode();
	}
	
	
	public Drawable getFill() {
		return fill;
	}
	
	
	/**
	 * Define a cor de fundo pintada sob o drawable de preenchimento. No caso de n�o haver drawable de preenchimento,
	 * pinta toda a �rea do pattern com a cor definida.
	 * 
	 * @param fillColor cor de fundo. Valores negativos indicam aus�ncia de cor de preenchimento.
	 * @see setFill(Drawable)
	 */
	public void setFillColor( int fillColor ) {
		this.fillColor = fillColor;
		
		updateDrawMode();
	}
	
	
	public int getFillColor() {
		return fillColor;
	}
	
	
//	public final void setUsesTransparency( boolean usesTransparency ) {
//		this.usesTransparency = usesTransparency;
//		
//		updateDrawMode();
//	}
	
	
	/**
	 * Indica se o drawable atual utiliza transpar�ncia.
	 * @return
	 */
//	public final boolean usesTransparency() {
//		return usesTransparency;
//	}
	 
	
	protected final void updateDrawMode() {
		if ( fillColor >= 0 ) {
			// h� uma cor s�lida de preenchimento definida
			if ( fill == null ) {
				drawMode = DRAW_MODE_FILL_ONLY;
			} else {
//				if ( usesTransparency ) {
//					if ( COPY_AREA_SUPPORTED )
//						drawMode = DRAW_MODE_FILL_COPY_AREA;
//					else
//						drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				} else {
//					if ( COPY_AREA_SUPPORTED )
//						drawMode = DRAW_MODE_DONT_FILL_COPY_AREA;
//					else
						drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				}
			}
		} else {
			// n�o h� cor s�lida de preenchimento definida
			if ( fill == null ) {
				drawMode = DRAW_MODE_NOTHING;
			} else {
//				if ( usesTransparency ) {
//					drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				} else {
//					if ( COPY_AREA_SUPPORTED )
//						drawMode = DRAW_MODE_DONT_FILL_COPY_AREA;
//					else
						drawMode = DRAW_MODE_DONT_FILL_DRAW;
//				}
			}
		}
	}


	public void update( int delta ) {
		if ( updatableFill != null )
			updatableFill.update( delta );
	}
}
 
