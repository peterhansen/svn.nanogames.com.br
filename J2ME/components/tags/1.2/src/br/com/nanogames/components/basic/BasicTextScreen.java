/*
 * TextScreen.java
 *
 * Created on June 4, 2007, 3:53 PM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;


/**
 *
 * @author peter
 */
public class BasicTextScreen extends ScrollRichLabel {
	
	/** �ndice da pr�xima tela a ser exibida. */
	protected final int nextScreenIndex;
	
	
	/**
	 * @param nextScreenIndex 
	 * @param font
	 * @param textIndex
	 * @param autoScroll
	 * @throws java.lang.Exception
	 */
	public BasicTextScreen( int nextScreenIndex, ImageFont font, int textIndex, boolean autoScroll ) throws Exception {
		this( nextScreenIndex, font, textIndex, autoScroll, null );
	}	
	
	
	/**
	 * 
	 * @param nextScreenIndex 
	 * @param font
	 * @param textIndex
	 * @param autoScroll
	 * @param specialChars
	 * @throws java.lang.Exception
	 */
	public BasicTextScreen( int nextScreenIndex, ImageFont font, int textIndex, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		this( nextScreenIndex, font, AppMIDlet.getText( textIndex ), autoScroll, specialChars );
	}
	
	
	/**
	 * 
	 * @param nextScreenIndex 
	 * @param font
	 * @param text
	 * @param autoScroll
	 * @throws java.lang.Exception
	 */
	public BasicTextScreen( int nextScreenIndex, ImageFont font, String text, boolean autoScroll ) throws Exception {
		this( nextScreenIndex, font, text, autoScroll, null  );
	}	
	
	
	/**
	 * Cria uma nova tela b�sica de texto.
	 * 
	 * @param nextScreenIndex �ndice da tela exibida ap�s a tela b�sica de texto.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto da tela.
	 * @param autoScroll indica se a tela far� rolagem do texto automaticamente. Nesse modo, o texto come�a no extremo
	 * inferior da tela, sobe at� desaparecer na parte superior, e ent�o volta � posi��o inferior. Caso esteja no modo
	 * de scroll manual, barras de rolagem padr�o s�o inseridas automaticamente.
	 * @param specialChars caracteres especiais utilizados no texto.
	 * @throws java.lang.Exception
	 */
	public BasicTextScreen( int nextScreenIndex, ImageFont font, String text, boolean autoScroll, Drawable[] specialChars ) throws Exception {	
		super( new RichLabel( font, text, ScreenManager.SCREEN_WIDTH, specialChars ) );
		
		this.nextScreenIndex = nextScreenIndex;
		
		// esse parece um teste bobo, mas evita que setAutoScroll( false ) seja chamado 2 vezes (� chamado implicitamente no construtor da classe-m�e)
		if ( autoScroll ) {
			setAutoScroll( true );
		} else {
			// insere barras de texto padr�o
			final Pattern scrollF = new Pattern( 0xececec );
			scrollF.setSize( 8, 0 );
			setScrollFull( scrollF );
			
			final Pattern scrollP = new Pattern( 0xb3b3b3 );
			scrollP.setSize( 8, 0 );
			setScrollPage( scrollP );
		}
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		setTextSpeed( 0 );
	}

	
	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:			
				setTextSpeed( 0 );
				
				AppMIDlet.setScreen( nextScreenIndex );
			return;
			
			default:
				super.keyPressed( key );
		} // fim switch ( key )
	} // fim do m�todo keyPressed( int )
	
	
	public void onPointerPressed( int x, int y ) {
		if ( autoScroll ) {
			keyPressed( ScreenManager.FIRE );
		} else {
			super.onPointerPressed( x, y );
		}
	}

}
