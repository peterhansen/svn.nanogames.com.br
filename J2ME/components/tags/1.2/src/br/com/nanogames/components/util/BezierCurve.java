/*
 * BezierCurve.java
 *
 * Created on 5 de Julho de 2006, 22:16
 */

package br.com.nanogames.components.util;

/**
 *	Classe utilizada para calcular uma curva de Bezier a partir de 4 pontos
 *	dados. C�digo adaptado de: http://en.wikipedia.org/wiki/B%C3%A9zier_curve
 * @author peter
 */
public final class BezierCurve {

	// valores pr�-calculados das porcentagens ao quadrado, para otimizar getPointAt()
	private static final short[] squares = {	0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100,
											121, 144, 169, 196, 225, 256, 289, 324, 361, 400,
											441, 484, 529, 576, 625, 676, 729, 784, 841, 900,
											961, 1024, 1089, 1156, 1225, 1296, 1369, 1444, 1521, 1600, 
											1681, 1764, 1849, 1936, 2025, 2116, 2209, 2304, 2401, 2500, 
											2601, 2704, 2809, 2916, 3025, 3136, 3249, 3364, 3481, 3600, 
											3721, 3844, 3969, 4096, 4225, 4356, 4489, 4624, 4761, 4900, 
											5041, 5184, 5329, 5476, 5625, 5776, 5929, 6084, 6241, 6400, 
											6561, 6724, 6889, 7056, 7225, 7396, 7569, 7744, 7921, 8100, 
											8281, 8464, 8649, 8836, 9025, 9216, 9409, 9604, 9801, 10000 };
	
	// valores pr�-calculados das porcentagens ao cubo, para otimizar getPointAt()
	private static final int[] cubes = { 	0, 1, 8, 27, 64, 125, 216, 343, 512, 729, 1000, 
											1331, 1728, 2197, 2744, 3375, 4096, 4913, 5832, 6859, 8000, 
											9261, 10648, 12167, 13824, 15625, 17576, 19683, 21952, 24389, 27000, 
											29791, 32768, 35937, 39304, 42875, 46656, 50653, 54872, 59319, 64000, 
											68921, 74088, 79507, 85184, 91125, 97336, 103823, 110592, 117649, 125000, 
											132651, 140608, 148877, 157464, 166375, 175616, 185193, 195112, 205379, 216000, 
											226981, 238328, 250047, 262144, 274625, 287496, 300763, 314432, 328509, 343000, 
											357911, 373248, 389017, 405224, 421875, 438976, 456533, 474552, 493039, 512000, 
											531441, 551368, 571787, 592704, 614125, 636056, 658503, 681472, 704969, 729000, 
											753571, 778688, 804357, 830584, 857375, 884736, 912673, 941192, 970299, 1000000	};
	
	public final Point origin = new Point();
	public final Point control1 = new Point();
	public final Point control2 = new Point();
	public final Point destiny = new Point();
	
	
	public BezierCurve() {
	}

	
	/** Creates a new instance of BezierCurve.
	 * 
	 * @param origin
	 * @param control1 
	 * @param control2
	 * @param destiny 
	 */
	public BezierCurve( Point origin, Point control1, Point control2, Point destiny ) {
		setValues( origin, control1, control2, destiny );
	}
	
	
	/**
	 * 
	 * @param origin
	 * @param control1
	 * @param control2
	 * @param destiny
	 */
	public final void setValues( Point origin, Point control1, Point control2, Point destiny ) {
		this.origin.set( origin );
		this.control1.set( control1 );
		this.control2.set( control2 );
		this.destiny.set( destiny );
	}
	
	
	/**
	 * Obt�m o ponto da curva referente a determinada porcentagem desta curva.
	 * @param fill refer�ncia para o ponto que ser� preenchido com a posi��o (x,y) referentes � porcentagem definida da curva.
	 * @param percent porcentagem do andamento da curva. Valores menores que 0 (zero) ou maiores que 100 (cem) ser�o truncados.
	 * @see #getX(int)
	 * @see #getY(int)
	 */
	public final void getPointAt( Point fill, int percent ) {
		if ( percent < 0 )
			percent = 0;
		else if ( percent > 100 )
			percent = 100;
			
		fill.set( calculateX( percent ), calculateY( percent ) );
	}
	
	
	/**
	 * Retorna apenas a componente x de um ponto da curva.
	 * @param percent porcentagem de andamento da curva.
	 * @return valor do ponto x na posi��o determinada em <i>percent</i> porcento da curva.
	 * @see #getPointAt(Point,int)
	 * @see #getY(int)
	 */
	public final int getX( int percent ) {
		if ( percent < 0 )
			percent = 0;
		else if ( percent > 100 )
			percent = 100;
		
		return calculateX( percent );
	}
	
	
	/**
	 * Retorna apenas a componente y de um ponto da curva.
	 * @param percent porcentagem de andamento da curva.
	 * @return valor do ponto y na posi��o determinada em <i>percent</i> porcento da curva.
	 * @see #getPointAt(Point,int)
	 * @see #getX(int)
	 */
	public final int getY( int percent ) {
		if ( percent < 0 )
			percent = 0;
		else if ( percent > 100 )
			percent = 100;
		
		return calculateY( percent );		
	}
	
	
	private final int calculateX( int percent ) {
		final int cx = 3 * ( control1.x - origin.x );
		final int bx = 3 * ( control2.x - control1.x ) - cx;
		final int ax = destiny.x - origin.x - cx - bx;

		return ( ax * cubes[ percent ] ) / 1000000 + ( bx * squares[ percent ] ) / 10000 + ( cx * percent ) / 100 + origin.x;
	}
	
	
	private final int calculateY( int percent ) {
		final int cy = 3 * ( control1.y - origin.y );
		final int by = 3 * ( control2.y - control1.y ) - cy;
		final int ay = destiny.y - origin.y - cy - by;

		return ( ay * cubes[ percent ] ) / 1000000 + ( by * squares[ percent ] ) / 10000 + ( cy * percent ) / 100 + origin.y;
	}
	
}
