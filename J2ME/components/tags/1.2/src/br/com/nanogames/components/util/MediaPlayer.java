/*
 * MediaPlayer.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.util.Hashtable;
import javax.microedition.lcdui.Display;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;

/**
 *
 * @author peter
 */
public final class MediaPlayer implements Serializable {
	
	/** Tempo em que ocorreu a �ltima altera��o nas op��es de som. Este valor � utilizado para garantir um intervalo
	 * m�nimo entre 2 mudan�as de op��o no som, evitando assim a quebra do aplicativo em alguns aparelhos (em especial
	 * aparelhos Samsung).
	 */
	private static long lastMuteToggledTime;
	
	/** intervalo m�nimo em milisegundos entre 2 mudan�as de op��o do som */
	private static final short MIN_MUTE_TOGGLE_INTERVAL = 500;
	
	/** indica se o tocador de sons est� mudo. Definir o tocador como mudo n�o altera o volume. */
	private static boolean muted;
	
	/** indica se a vibra��o est� ativada */
	private static boolean vibration = true;
	 
	/** valor a ser passado para o tocador de som no caso de se tocar um som infinitas vezes seguidas */
	//#if SAMSUNG_API == "true"
//#		public static final byte LOOP_INFINITE = 100;
	//#else
		public static final byte LOOP_INFINITE = -1;
	//#endif
	
	/** �ndice do �ltimo som tocado */
	private static byte lastPlayedIndex = -1;
	
	/** refer�ncia para o Display (utilizado para vibrar) */
	private static Display display;
	 
	/** tocador de som. S� h� um tocador ativo a cada momento, devido a problemas em v�rios aparelhos ao se pr�-alocar
	 * v�rios sons. */
	protected static Player player;
	
	/** endere�os dos sons a serem tocados e seus respectivos tipos de m�dia */
	protected static String[][] sounds;
	
	protected static String databaseName;
	protected static int databaseIndex;
	
	public static final String[] EXTENSION_WAV	= { "wav" };
	public static final String[] EXTENSION_AMR	= { "amr" };
	public static final String[] EXTENSION_MP3	= { "mp3", "mp4" };
	public static final String[] EXTENSION_MIDI	= { "mid", "midi" };
	
	public static final String MEDIA_TYPE_WAV			= "audio/x-wav";
	public static final String MEDIA_TYPE_AU			= "audio/basic";
	public static final String MEDIA_TYPE_MP3			= "audio/mpeg";
	public static final String MEDIA_TYPE_MID			= "audio/midi";
	public static final String MEDIA_TYPE_AMR			= "audio/amr";
	public static final String MEDIA_TYPE_TONE_SEQUENCE	= "audio/x-tone-seq";
	
	private static final String[] SUPPORTED_TYPES = Manager.getSupportedContentTypes( null );
	
	private static final Hashtable extensionTable = new Hashtable();
	
	private static MediaPlayer instance;
	
	private static boolean vibrationSupported = true;
	
	/** Altura m�nima da tela a partir da qual considera-se que todos os aparelhos suportam vibra��o. */
	private static final short VIBRATION_ON_MIN_HEIGHT = 240;
	
	/** Mutex utilizado para fazer a exclus�o m�tua de eventos de som. */
	private static final Mutex mutex = new Mutex();
	
	
	private MediaPlayer() {
		for ( int i = 0; i < EXTENSION_WAV.length; ++i )
			extensionTable.put( EXTENSION_WAV[ i ], MEDIA_TYPE_WAV );
		
		for ( int i = 0; i < EXTENSION_AMR.length; ++i )
			extensionTable.put( EXTENSION_AMR[ i ], MEDIA_TYPE_AMR );		
		
		for ( int i = 0; i < EXTENSION_MP3.length; ++i )
			extensionTable.put( EXTENSION_MP3[ i ], MEDIA_TYPE_MP3 );
		
		for ( int i = 0; i < EXTENSION_MIDI.length; ++i )
			extensionTable.put( EXTENSION_MIDI[ i ], MEDIA_TYPE_MID );
	}
	
	
	/**
	 * Indica se o aparelho suporta determinado tipo de m�dia.
	 * 
	 * @param mediaType tipo de m�dia, no padr�o TODO (que padr�o?). Tipos comuns:
	 * <ul>
	 * <li>MEDIA_TYPE_WAV: "audio/x-wav"</li>
	 * <li>MEDIA_TYPE_AU: "audio/basic"</li>
	 * <li>MEDIA_TYPE_MP3: "audio/mpeg"</li>
	 * <li>MEDIA_TYPE_MID: "audio/midi"</li>
	 * <li>MEDIA_TYPE_AMR: "audio/amr"</li>
	 * <li>MEDIA_TYPE_TONE_SEQUENCE: "audio/x-tone-seq"</li>
	 * </ul>
	 * @return <i>true</i>, caso o tipo de m�dia seja suportado, e <i>false</i>caso contr�rio.
	 */
	public static final boolean support( String mediaType ) {
		if ( SUPPORTED_TYPES != null ) {
			for ( int i = 0; i < SUPPORTED_TYPES.length; ++i ) {
				if ( SUPPORTED_TYPES[ i ].indexOf( mediaType ) >= 0 )
					return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Indica se o aparelho � capaz de vibrar. A chamada deste m�todo n�o interrompe uma vibra��o ativa do aparelho.
	 * @return <i>true</i>, caso o aparelho seja capaz de vibrar, e <i>false</i> caso contr�rio.
	 * @see MediaPlayer#checkVibrationSupport
	 */
	public static final boolean isVibrationSupported() {
		return vibrationSupported;
	}

	
	/**
	 * Ativa a vibra��o.
	 * @param duration dura��o em milisegundos da vibra��o. O tempo exato e a forma como a vibra��o ocorre (cont�nua ou
	 * intermitente) s�o dependentes da implementa��o de cada aparelho. Para interromper a vibra��o, deve-se passar como
	 * argumento o valor 0 (zero).
	 * @return boolean indicando se o aparelho suporta vibra��o.
	 */
	public static final boolean vibrate( int duration ) {
		if ( vibration ) {
			// normalmente n�o � necess�rio que a vibra��o esteja dentro de um bloco try/catch, mas alguns aparelhos
			// lan�am exce��o ou travam quando a vibra��o � ativada enquanto est�o carregando, por exemplo.
			try {
				return display.vibrate( duration );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
				
				return false;
			}			
		} // fim if ( vibration )
		return false;
	} // fim do m�todo vibrate( int )
	 
	
	/**
	 * Define o estado da vibra��o.
	 * @param active estado da vibra��o: ligada (true) ou desligada (false).
	 */
	public static final void setVibration( boolean active ) {
		vibration = active;

		// p�ra alguma vibra��o que possa estar ativa no momento caso a vibra��o seja desativada
		if ( !active )
			vibrate( 0 );
	} // fim do m�todo setVibration( boolean )

	
	/**
	 * Indica se a vibra��o est� ligada.
	 * @return <i>true</i>, caso esteja ligada, e <i>false<i> caso contr�rio.
	 */
	public static final boolean isVibration() {
		return vibration;
	} // fim do m�todo isVibration()
	 
	
	/**
	 * Inicializa o tocador de sons.
	 * @param databaseName 
	 * @param databaseIndex 
	 * @param soundsPaths endere�os dos arquivos de som. A ordem dos arquivos define o �ndice a ser passado posteriormente
	 * ao m�todo play. Por exemplo: para se tocar o arquivo "menu.mid" presente na lista de arquivos { "splash.mid", 
	 * "shot.wav", "menu.mid", "ending.mp3" }, deve-se passar o �ndice 2 (3� item da lista). A extens�o do arquivo define
	 * o tipo de som a ser tocado.
	 * @throws java.lang.Exception caso haja erro ao carregar os sons
	 */
	public static final void init( String databaseName, int databaseIndex, String[] soundsPaths ) throws Exception {
		mutex.acquire();
		
		// TODO carregar arquivos em byte[], mantendo os arquivos na mem�ria e depois carregando-os atrav�s de inputStream
		// dos arrays de bytes
		// TODO utilizar um �nico arquivo de som, e fazer chamadas de setMediaTime para tocar os diferentes sons? (registrando um Listener para saber quando parar)
		if ( instance == null )
			instance = new MediaPlayer();		
		
		display = Display.getDisplay( AppMIDlet.getInstance() );
		
		// verifica se o aparelho possui suporte a vibra��o
		switch ( AppMIDlet.getVendor() ) {
			case AppMIDlet.VENDOR_MOTOROLA:
			case AppMIDlet.VENDOR_SAMSUNG:
			case AppMIDlet.VENDOR_SONYERICSSON:
			case AppMIDlet.VENDOR_HTC:
			case AppMIDlet.VENDOR_SIEMENS:
			case AppMIDlet.VENDOR_SAGEM_GRADIENTE:
				// considera que todos possuem vibra��o - o teste � diferenciado para alguns fabricantes pois nos casos 
				// da Nokia (e possivelmente outros fabricantes), h� aparelhos sem suporte a vibra��o (todos os S60 2nd 
				// Edition, al�m de alguns S40 2nd Edition), enquanto alguns aparelhos Samsung indicam que n�o suportam
				// vibra��o, quando na verdade possuem o recurso (D820, por exemplo)
				vibrationSupported = true;
			break;
			
			default:
				vibrationSupported = ScreenManager.SCREEN_HEIGHT >= VIBRATION_ON_MIN_HEIGHT || vibrate( 1 );
		}
		
		if ( soundsPaths != null && soundsPaths.length > 0 ) {
			sounds = new String[ soundsPaths.length ][ 2 ];
			for ( int i = 0; i < sounds.length; ++i ) {
				sounds[ i ][ 0 ] = soundsPaths[ i ];
				final char[] chars = soundsPaths[ i ].toCharArray();

				// obt�m a extens�o do som a ser tocado
				int begin = chars.length - 1;
				for ( ; begin >= 0; --begin ) {
				   if ( chars[ begin ] == '.' )
					   break;
				} 
				final String extension = soundsPaths[ i ].substring( begin + 1 ).toLowerCase();
				sounds[ i ][ 1 ] = ( String ) extensionTable.get( extension );
			}
		} // fim if ( soundsPaths != null && soundsPaths.length > 0 )
		
		MediaPlayer.databaseName = databaseName;
		MediaPlayer.databaseIndex = databaseIndex;
		
		// tenta carregar as op��es
		loadOptions();
		
		mutex.release();
	} // fim do m�todo init( String[] )
	
	
	/**
	 * Libera os recursos utilizados pelo tocador de sons.
	 * 
	 * @param useMutex indica se deve-se utilizar a exclus�o m�tua (utilizar <code>false</code> caso esse m�todo seja chamado
	 * dentro um bloco de c�digo onde o mutex j� est� em uso, como no caso do m�todo <code>play</code>).
	 * @see free()
	 */
	private static final void free( final boolean useMutex ) {
		if ( useMutex )
			mutex.acquire();		

		if ( player != null ) {
			try {
				player.deallocate();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}			

			try {
				player.close();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}						

			player = null;
		}

		System.gc();
		lastPlayedIndex = -1;

		if ( useMutex )
			mutex.release();		
	}
	 
	
	/**
	 * Libera os recursos utilizados pelo tocador de sons.
	 */
	public static final void free() {
		free( true );
	} // fim do m�todo free()
	
	
	/**
	 * P�ra de tocar o som atual, sem desaloc�-lo.
	 * @param useMutex indica se deve ser utilizada a exclus�o m�tua no acesso ao tocador de sons. Normalmente s� <b>n�o</b>
	 * � utilizada a exclus�o m�tua no caso de chamada deste m�todo ao suspender a aplica��o, para evitar demora na resposta
	 * ao m�todo <code>hideNotify</code> caso o evento ocorra dentro da regi�o cr�tica do m�todo <code>play</code>.
	 * @see #stop()
	 */
	public static final void stop( final boolean useMutex ) {
		if ( useMutex )
			mutex.acquire();

		if ( player != null ) {
			try {
				player.stop();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
		} // fim if ( player != null )

		if ( useMutex )
			mutex.release();		
	}
	 
	
	/**
	 * P�ra de tocar o som atual, sem desaloc�-lo, e utilizando a exclus�o m�tua. Equivalente � chamada de <code>stop(true)</code>.
	 * @see #stop(boolean)
	 */
	public static final void stop() {
		stop( true );
	} // fim do m�todo stop()
	
	
	/**
	 * Toca um som uma vez. Equivalente � chamada de <code>play(index, 1)</code>.
	 * 
	 * @param index �ndice do som a ser tocado. O �ndice � determinado pela ordem dos endere�os passados ao m�todo init.
	 */
	public static final void play( int index ) {
		play( index, 1 );
	}
	 
	
	/**
	 * Toca um som.
	 * @param index �ndice do som a ser tocado. O �ndice � determinado pela ordem dos endere�os passados ao m�todo init.
	 * @param loopCount n�mero de repeti��es do som a ser tocado.
	 */
	public static final void play( final int index, final int loopCount ) {
		final Thread thread = new Thread() {
			public final void run() {
				mutex.acquire();
				
				if ( !muted ) {
					// n�o pode utilizar mutex aqui, sen�o causa deadlock
					stop( false );

					try {
						if ( index != lastPlayedIndex ) {
							// caso o �ndice seja diferente do �ltimo som tocado, o Player anterior � desalocado para melhorar
							// a portabilidade
							// utilizar mutex na chamada deste m�todo causa deadlock
							free( false );
						
							//#if SAMSUNG_API == "true"
	//# 						switch ( AppMIDlet.getVendor() ) {
	//# 							
	//# 							// Alguns aparelhos Samsung n�o tocam sons corretamente ao utilizar a MMAPI. Nesses casos,
	//# 							// utiliza-se a API pr�pria da Samsung. A compila��o deve ser espec�fica pois, apesar da
	//# 							// identifica��o durante a execu��o garantir que somente aparelhos Samsung executar�o c�digos
	//# 							// que utilizem a API espec�fica, ocorre erro na instala��o (etapa de compila��o) em aparelhos
	//# 							// LG caso a classe SamsungPlayer esteja presente.
	//# 							case AppMIDlet.VENDOR_SAMSUNG:
	//# 								player = new SamsungPlayer( sounds[ index ][ 0 ] );
	//# 							break;
	//# 							
	//# 							default:
							//#endif

							final InputStream input = display.getClass().getResourceAsStream( sounds[ index ][ 0 ] );
							player = Manager.createPlayer( input, sounds[ index ][ 1 ] );			

							//#if SAMSUNG_API == "true"
	//# 						}
							//#endif

							try { 
								player.realize(); 
							} catch ( Exception e ) {
								//#if DEBUG == "true"
//# 							e.printStackTrace();
								//#endif
							}

							try { 
								player.prefetch(); 
							} catch ( Exception e ) {
								//#if DEBUG == "true"
//# 							e.printStackTrace();
								//#endif
							}

			//				switch ( loopCount ) { TODO testar
			//					case 0:
			//					case 1:
			//					break;
			//					
			//					default:
			//						player.setLoopCount( loopCount );
			//				}

						} else {
							// setMediaTime() pode causar problemas em alguns aparelhos. No Benq EL71, por exemplo, faz com
							// que sons n�o sejam mais tocados ap�s algumas repeti��es, causando congelamentos ao tentar
							// tocar outro som ou desligar o tocador. Em outros aparelhos, por outro lado, n�o chamar
							// este m�todo ao repetir o �ltimo som tocado (ou seja, sem realocar o som) pode fazer com que
							// o som comece a ser tocado do ponto onde parou na �ltima vez, o que normalmente significa que
							// o som come�a a tocar a partir do final (n�o � ouvido).
							switch ( AppMIDlet.getVendor() ) {
								case AppMIDlet.VENDOR_SIEMENS:
								break;
								
								default:
									try { 
										player.setMediaTime( 0 );
									} catch ( Exception e ) {
										//#if DEBUG == "true"
//# 							e.printStackTrace();
										//#endif
									}						
								// fim default
							}
						}
						
						player.setLoopCount( loopCount );
						player.start();
						lastPlayedIndex = ( byte ) index;
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif
					} // fim catch ( Exception e )				
				} // fim if ( !muted )
				
				mutex.release();
			}
		};
		thread.setPriority( Thread.MAX_PRIORITY );
		thread.start();
		Thread.yield();
	} // fim do m�todo play( int, int )

	
	public static final void setMute( boolean mute ) {
		mutex.acquire();
		
		// verifica se foi decorrido o tempo m�nimo entre 2 altera��es do estado do tocador de som
		final long time = System.currentTimeMillis();
		if ( time - lastMuteToggledTime >= MIN_MUTE_TOGGLE_INTERVAL ) {
			lastMuteToggledTime = time;
			
			MediaPlayer.muted = mute;

			// p�ra de tocar sons caso o tocador seja definido como mudo
			if ( mute )
				stop( false );
		}
		
		mutex.release();
	} // fim do m�todo setMute( boolean )

	
	public static final boolean isMuted() {
		return muted;
	}
	
	
	/**
	 * Play back a tone as specified by a note and its duration. A note is given in the range of 0 to 127 inclusive. The 
	 * frequency of the note can be calculated from the following formula:
	 * <p>SEMITONE_CONST = 17.31234049066755 = 1/(ln(2^(1/12)))
	 * <br></br>note = ln(freq/8.176)*SEMITONE_CONST
	 * <br></br>The musical note A = MIDI note 69 (0x45) = 440 Hz.</p>
	 * <p>This call is a non-blocking call. Notice that this method may utilize CPU resources significantly on devices that 
	 * don't have hardware support for tone generation.</p>
	 *
	 * @param note Defines the tone of the note as specified by the above formula.
	 * @param duration The duration of the tone in milli-seconds. Duration must be positive.
	 * @param volume Audio volume range from 0 to 100. 100 represents the maximum volume at the current hardware level. 
	 * Setting the volume to a value less than 0 will set the volume to 0. Setting the volume to greater than 100 will 
	 * set the volume to 100.
	 */
	public static final void playTone( int note, int duration, int volume ) {
		if ( !muted ) {
			try {
				Manager.playTone( note, duration, volume );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}
		}
	}


	public static final void saveOptions() {
		try {
			AppMIDlet.saveData( databaseName, databaseIndex, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo saveOptions()
	
	
	/**
	 * Carrega as op��es de som e vibra��o armazenadas no RMS. Este m�todo � chamado automaticamente no m�todo <code>init()</code>.
	 * @see #init(String, int, String[])
	 */
	public static final void loadOptions() {
		try {
			AppMIDlet.loadData( databaseName, databaseIndex, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo loadOptions()

	
	public final void write( DataOutputStream output ) throws Exception {
		output.writeBoolean( muted );
		output.writeBoolean( vibration );
	}

	
	public final void read( DataInputStream input ) throws Exception {
		muted = input.readBoolean();
		vibration = input.readBoolean();		
	}
	
	
	/**
	 * Indica se o tocador de som est� ativo, ou seja, se � n�o-nulo e est� no estado Player.STARTED.
	 * @return <i>true</i>, caso o tocador esteja ativo, e <i>false</i> caso contr�rio.
	 */
	public static final boolean isPlaying() {
		try {
			mutex.acquire();
		
			return player != null && player.getState() == Player.STARTED;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return false;
		} finally {
			mutex.release();
		}
	}
	
}
 
