/*
 * Sprite.java
 *
 * Created on September 28, 2007, 5:11 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class Sprite extends Drawable implements Updatable {
	
	/** Extens�o de arquivo utilizada nos descritores de frameSets. */
	public static final String FRAMESET_DESCRIPTOR_EXTENSION = ".dat";
	
	/** Caracter utilizado para indicar para o leitor de descritores o in�cio de uma sequ�ncia de frames com dura��es
	 * fixas (o primeiro valor lido � a dura��o dos frames, e em seguida os �ndices da sequ�ncia). */
	public static final char FRAMESET_DESCRIPTOR_SEQUENCE_FIXED = 'f';
	
	/** Caracter utilizado para indicar para o leitor de descritores o in�cio de uma sequ�ncia de frames com dura��es
	 * vari�veis. Os valores presentes na sequ�ncia devem sempre obedecer a ordem �ndice - dura��o. */
	public static final char FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE = 'v';	
 
	/** Sequ�ncias dos �ndices dos frames que comp�em a anima��o. */
	protected final byte[][] sequences;
	
	/** Dura��o em milisegundos de cada frame. */
	protected final short[][] frameTimes;
	 
	/** Conjunto de frames. */
	protected final Image[] frames;
	
	/** Offset na posi��o de desenho de cada frame, causado pelas varia��es de tamanho de cada imagem. */
	protected final Point[] offsets;	
	
	/** �ndice da sequ�ncia de frames atual */
	protected byte sequenceIndex;
	 
	/** �ndice do frame atual na sequ�ncia. */
	protected byte frameSequenceIndex;
	
	/** �ndice real do frame atual (�ndice da imagem no array de frames). */
	protected byte frameRealIndex;
	
	/** tempo acumulado no frame atual */
	protected short accTime;
	
	/** Refer�ncia para o frame atual, para acelerar acessos futuros (em vez de referenciar o array o tempo todo) */
	protected Image frame;
	
	
	//#if USE_MIDP2_SPRITE == "true"
//# 	
//# 	/** Refer�ncia para um objeto da classe sprite, que serve para desenhar de forma otimizada no caso de serem aplicadas
//# 	 * transforma��es na imagem. Utilizando-se apenas a imagem, ocorrem erros e/ou travamentos ao rotacionar imagens em
//# 	 * aparelhos Samsung e Siemens.
//# 	 */
//# 	protected javax.microedition.lcdui.game.Sprite frameSprite;	
//# 	
	//#endif
	
	
	/** Dura��o do frame atual em milisegundos. */
	protected short frameTime;
	
	/** Refer�ncia para a sequ�ncia atual, para acelerar acessos futuros (em vez de referenciar o array o tempo todo) */
	protected byte[] sequence;	
	
	/** Listener que receber� os eventos de fim de sequ�ncia (pode ser nulo) */
	protected SpriteListener listener;
	
	/** Id do sprite, a ser passado para o listener poder identificar qual sprite o chamou */
	protected int id;
	
	/** Indica se a anima��o do sprite est� pausada no momento */
	protected boolean paused;
	
	
	// TODO tratar offset nos casos de rota��o
	
	
	// FIXME carregar sprite a partir de descritor utilizando otimiza��o do obfuscator causa reboot do aparelho em
	// alguns SonyEricsson (detectado no W600)
	/**
	 * Aloca um novo sprite, a partir das informa��es presentes no descritor.
	 * 
	 * @param descriptorFilename 
	 * @param filenamePrefix
	 * @throws java.lang.Exception
	 */
	public Sprite( String descriptorFilename, String filenamePrefix ) throws Exception {
		this( descriptorFilename, filenamePrefix, null );
	}
	
	
	// FIXME carregar sprite a partir de descritor utilizando otimiza��o do obfuscator causa reboot do aparelho em
	// alguns SonyEricsson (detectado no W600)
	/**
	 * Aloca um novo sprite, a partir das informa��es presentes no descritor.
	 * 
	 * @param descriptorFilename 
	 * @param filenamePrefix
	 * @param map Mapa para convers�o de cores de paleta ou null caso deseje utilizar as cores originais
	 * @throws java.lang.Exception
	 */
	public Sprite( String descriptorFilename, String filenamePrefix, PaletteMap[] map ) throws Exception {
		this( new FrameReader( descriptorFilename, filenamePrefix, map ) );
	}
	
	
	// FIXME carregar sprite a partir de descritor utilizando otimiza��o do obfuscator causa reboot do aparelho em
	// alguns SonyEricsson (detectado no W600)
	/**
	 * Aloca um novo sprite, a partir das informa��es presentes no descritor.
	 * 
	 * @param descriptorFilename 
	 * @param filenamePrefix
	 * @param lightColor Cor mais clara utilizada na interpola��o de cores
	 * @param darkColor Cor mais escura utilizada na interpola��o de cores
	 * @throws java.lang.Exception
	 */
	public Sprite( String descriptorFilename, String filenamePrefix, int lightColor, int darkColor ) throws Exception {
		this( new FrameReader( descriptorFilename, filenamePrefix, lightColor, darkColor ) );
	}
	
	/**
	 * 
	 * @param reader
	 * @throws java.lang.Exception
	 */
	private Sprite( FrameReader reader ) throws Exception {
		this( reader.frames, reader.offsets, reader.getSequences(), reader.getFrameTimes() );
		
		setSize( reader.totalSize );
	}
	
	
	/**
	 * Cria uma c�pia de um sprite.
	 * 
	 * @param s sprite a ser copiado. O novo sprite possuir� os mesmos frames, offsets de frames, sequ�ncias e dura��o
	 * de frames do original. Atributos como posi��o, pixel de refer�ncia, visibilidade e etc N�O s�o copiados.
	 */
	public Sprite( Sprite s ) {
		this( s.frames, s.sequences, s.frameTimes );
		
		setSize( s.getSize() );
		
		// Faz uma c�pia dos offsets (n�o utiliza os mesmos objetos sen�o altera��es causadas por transforma��es ser�o
		// refletidas em todos os sprites que utilizam a mesma origem). A transforma��o atual do sprite utilizado como
		// base da c�pia � necess�ria para evitar que os valores copiados dos offsets estejam modificados em fun��o
		// da transforma��o.
		final int previousTransform = s.getTransform();
		s.setTransform( TRANS_NONE );
		
		for ( int i = 0; i < offsets.length; ++i )
			offsets[ i ] = new Point( s.offsets[ i ] );
		
		s.setTransform( previousTransform );
	}
	
	
	public Sprite( Image[] frames, Point[] offsets, byte[][] sequences, short[] frameTimes ) {
		this( frames, offsets, sequences, new short[ sequences.length ][ 1 ] );
		
		setFrameTimes( frameTimes );
	}
	
	
	public Sprite( Image[] frames, byte[][] sequences, short[] frameTimes ) {
		this( frames, new Point[ frames.length ], sequences, new short[ sequences.length ][ 1 ] );
		
		for ( int i = 0; i < offsets.length; ++i )
			offsets[ i ] = new Point();

		setFrameTimes( frameTimes );
		
		setSequence( 0 );
	}
	
	
	/**
	 * 
	 * @param frames
	 * @param sequences
	 * @param frameTimes
	 */
	public Sprite( Image[] frames, byte[][] sequences, short[][] frameTimes ) {
		this( frames, new Point[ frames.length ], sequences, frameTimes );
	}	
	
	
	/**
	 * 
	 * @param frames
	 * @param offsets
	 * @param sequences
	 * @param frameTimes
	 */
	public Sprite( Image[] frames, Point[] offsets, byte[][] sequences, short[][] frameTimes ) {
		this.frames = frames;
		this.offsets = offsets;
		this.sequences = sequences;
		this.frameTimes = frameTimes;
		
		setSequence( 0 );
		for ( byte i = 0; i < frames.length; ++i ) {
			final Image image = frames[ i ];
			
			if ( image.getWidth() > size.x )
				size.x = image.getWidth();
			
			if ( image.getHeight() > size.y )
				size.y = image.getHeight();
		}
		// TODO definir tamanho automaticamente (chamada do m�todo abaixo � utilizada para que classes que estendam
		// setSize() tenham seu tamanho definido corretamente
		setSize( size );
	}
	

	protected void paint( Graphics g ) {
		// FIXME erro no posicionamento interno da imagem em algumas combina��es de rota��o e espelhamento
		//#if USE_MIDP2_SPRITE == "true"
//# 			frameSprite.setTransform( transformMIDP );
//# 			frameSprite.setPosition( translate.x + offsets[ frameRealIndex ].x, translate.y + offsets[ frameRealIndex ].y );
//# 			frameSprite.paint( g );
		//#else
		if ( transform == TRANS_NONE )
			g.drawImage( frame, translate.x + offsets[ frameRealIndex ].x, translate.y + offsets[ frameRealIndex ].y, 0 );
		else
			g.drawRegion( frame, 0, 0, frame.getWidth(), frame.getHeight(), transformMIDP, translate.x + offsets[ frameRealIndex ].x, translate.y + offsets[ frameRealIndex ].y, 0 );
		//#endif		
	}
	
	
	public final short getCurrFrameAccTime() {
		return accTime;
	}
	
	
	public final void setCurrFrameAccTime( int time ) {
		accTime = ( short )time;
	}
	
	
	public final void pause( boolean b ) {
		paused = b;
	}
	
	
	public final boolean isPaused() {
		return paused;
	}

	
	public SpriteListener getListener() {
		return listener;
	}

	
	public void update( int delta ) {
		// se o frame tiver dura��o 0 (zero), o controle da anima��o � feito externamente, atrav�s de chamadas expl�citas
		// a nextFrame, previousFrame e etc.
		if ( frameTime > 0 && !paused ) {
			accTime += delta;
			if ( accTime >= frameTime ) {
				// trocou de frame
				accTime -= frameTime;

				nextFrame();

				// Se houver um listener registrado ...
				if ( listener != null )
				{
					// Avisa que mudou de frame
					listener.onFrameChanged( id, frameSequenceIndex  );
					
					// Avisa que a sequ�ncia acabou
					if ( frameSequenceIndex == 0 )
						listener.onSequenceEnded( id, sequenceIndex );
				}
			}
		}
	}

	
	/**
	 * Avan�a um frame na sequ�ncia atual.
	 * 
	 * @see #previousFrame()
	 * @see #setFrame(int)
	 */	
	public void nextFrame() {
		if ( frameSequenceIndex < sequence.length - 1 )
			setFrame( frameSequenceIndex + 1 );
		else
			setFrame( 0 );
	}
	 
	
	/**
	 * Retrocede um frame na sequ�ncia atual.
	 * 
	 * @see #nextFrame()
	 * @see #setFrame(int)
	 */
	public void previousFrame() {
		if ( frameSequenceIndex > 0 )
			setFrame( frameSequenceIndex - 1 );
		else
			setFrame( sequence.length - 1 );
	}
	 
	
	/**
	 * Define o frame atual do sprite.
	 * 
	 * @param index �ndice do frame do sprite na sequ�ncia de anima��o atual.
	 * @see #nextFrame()
	 * @see #previousFrame()
	 */
	public void setFrame( int index ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif
		
		frameSequenceIndex = ( byte ) index;
		frameRealIndex = sequence[ frameSequenceIndex ];
		frame = frames[ frameRealIndex ];
		frameTime = frameTimes[ sequenceIndex ][ frameSequenceIndex ];
		
		//#if USE_MIDP2_SPRITE == "true"
//# 			// TODO verificar impacto no desempenho - � melhor pr�-alocar todos os frames de sprites midp2?
//# 		frameSprite = new javax.microedition.lcdui.game.Sprite( frame );
		//#endif
		
		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
//# 				System.err.println( "Exce��o em Sprite.setFrame. frameSequenceIndex: " + frameSequenceIndex + ", frameRealIndex: " + frameRealIndex + ", sequenceIndex: " + sequenceIndex );
//# 				e.printStackTrace();
//# 			}
		//#endif
	}	
	
	
	/**
	 * Define a sequ�ncia atual de anima��o do sprite. A sequ�ncia ser� iniciada a partir do frame de �ndice 0 (zero), e
	 * as dimens�es do sprite ser�o atualizadas de acordo com as dimens�es dos frames da nova sequ�ncia. Aten��o: o ponto 
	 * de refer�ncia n�o � alterado.
	 * @param sequence �ndice da sequ�ncia de anima��o.
	 */
	public void setSequence( int sequence ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif		
		sequenceIndex = ( byte ) sequence;
		this.sequence = sequences[ sequenceIndex ];
		setFrame( 0 );
		accTime = 0;
		
		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
//# 				System.err.println( "Exce��o em Sprite.setSequence. sequence: " + sequence + ", sequences.length: " + ( sequences == null ? -1 : sequences.length ) );
//# 				e.printStackTrace();
//# 			}
		//#endif		
	} // fim do m�todo setSequence( int )
	
	
	/**
	 * Obt�m o �ndice da sequ�ncia atual.
	 * 
	 * @return �ndice da sequ�ncia atual.
	 */
	public final byte getSequence() {
		return sequenceIndex;
	}
	
	
	/**
	 * Retorna o n�mero de sequ�ncias que o sprite possui.
	 * 
	 * @return n�mero total de sequ�ncias.
	 */
	public final int getNSequences() {
		return sequences.length;
	}
	
	
	/**
	 * Obt�m o �ndice do frame atual na sequ�ncia.
	 * 
	 * @return �ndice do frame atual na sequ�ncia.
	 */
	public final byte getFrameSequenceIndex() {
		return frameSequenceIndex;
	}

	
	/**
	 * Obt�m a refer�ncia para a imagem do frame atual.
	 * 
	 * @return refer�ncia para a imagem do frame atual.
	 */
	public final Image getCurrentFrameImage() {
		return frame;
	}
	
	
	/**
	 * Obt�m a refer�ncia para a imagem de um frame.
	 * 
	 * @param sequenceIndex �ndice da sequ�ncia cuja imagem ser� obtida.
	 * @param frameIndex �ndice do frame na sequ�ncia cuja imagem ser� obtida.
	 * @return refer�ncia para a imagem do frame.
	 */
	public final Image getFrameImage( int sequenceIndex, int frameIndex ) {
		return frames[ sequences[ sequenceIndex][ frameIndex ] ];
	}
	
	
	/**
	 * Obt�m o offset do frame passado como par�metro
	 * 
	 * @param frameIndex �ndice do frame
	 * @return Offset do frame.
	 */
	public final Point getFrameOffset( int frameIndex ) {
		return offsets[ frameIndex ];
	}
	
	
	/**
	 * Retorna o �ndice do frame atual no array de frames do sprite
	 * 
	 * @return �ndice do frame atual
	 */
	public final byte getCurrFrameIndex() {
		return frameRealIndex;
	}

	
	/**
	 * Define o listener que ter� seu m�todo <i>sequenceEnded</i> chamado quando uma sequ�ncia do sprite terminar.
	 *
	 * @param listener refer�ncia para o listener do sprite. Passar <i>null</i> remove o listener anterior.
	 * @param id identifica��o do sprite, que ser� passada para o listener identificar qual sprite teve uma sequ�ncia encerrada.
	 */
	public final void setListener( SpriteListener listener, int id ) {
		this.listener = listener;
		this.id = id;
	}


	public boolean mirror( int mirrorType ) {
		mirrorType &= TRANS_MASK_MIRROR;
		final int newMirrorStatus = ( transform & TRANS_MASK_MIRROR ) ^ mirrorType;
		
		// FIXME erro no posicionamento interno da imagem em algumas combina��es de rota��o e espelhamento
		// se houver mudan�a no espelhamento horizontal, atualiza o pixel de refer�ncia e os offsets dos frames
		if ( ( newMirrorStatus & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) ) {
			defineReferencePixel( size.x - referencePixel.x, referencePixel.y );
			
			for ( int i = 0; i < offsets.length; ++i )
				offsets[ i ].x = size.x - offsets[ i ].x - frames[ i ].getWidth();
		}
		
		// se houver mudan�a no espelhamento vertical, atualiza o pixel de refer�ncia e os offsets dos frames
		if ( ( newMirrorStatus & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) ) {
			defineReferencePixel( referencePixel.x, size.y - referencePixel.y );	
			
			for ( int i = 0; i < offsets.length; ++i )
				offsets[ i ].y = size.y - offsets[ i ].y - frames[ i ].getHeight();			
		}

		// a transforma��o passa a ser a uni�o da rota��o acumulada com o novo estado de espelhamento
		transform = ( transform & TRANS_MASK_ROTATE ) | newMirrorStatus;
		updateTransform();
		
		return true;		
	} // fim do m�todo mirror( int )
	
	
	public boolean rotate( int rotationType ) {
		super.rotate( rotationType );
		
		 // atualiza os offsets do sprite ap�s rota��es
		switch ( rotationType ) {
			case TRANS_ROT90:
			case TRANS_ROT270:
				for ( byte i = 0; i < offsets.length; ++i )
					offsets[ i ].set( offsets[ i ].y, offsets[ i ].x );
			break;
		}
		
		return true;
	}


	//#if USE_MIDP2_SPRITE == "true"
//# 	public void defineReferencePixel( int refPixelX, int refPixelY ) {
//# 		super.defineReferencePixel( refPixelX, refPixelY );
//# 		
//# 		frameSprite.defineReferencePixel( refPixelX, refPixelY );
//# 	}
	//#endif
	
	
	/**
	 * 
	 * @param sequencesFrameTime
	 */
	private final void setFrameTimes( short[] sequencesFrameTime ) {
		// preenche o array da dura��o de frames
		for ( int i = 0; i < frameTimes.length; ++i ) {
			frameTimes[ i ] = new short[ sequences[ i ].length ];
			
			final short currentSequenceFrameTime = sequencesFrameTime[ i ];
			
			for ( int j = 0; j < frameTimes[ i ].length; ++j )
				frameTimes[ i ][ j ] = currentSequenceFrameTime;
		}		
	}
	
	
	/**
	 * Obt�m o array de dura��o dos frames de cada sequ�ncia.
	 * @return array com a dura��o dos frames de cada sequ�ncia.
	 */
	public final short[][] getFrameTimes() {
		return frameTimes;
	}
	

	//<editor-fold defaultstate="collapsed" desc="Classe interna FrameReader">
	
	private static final class FrameReader implements Serializable {
		
		private static final byte READ_STATE_FRAMES_WIDTH			= 0;
		private static final byte READ_STATE_FRAMES_HEIGHT			= 1;
		private static final byte READ_STATE_FRAMES_OFFSETS			= 2;
		private static final byte READ_STATE_FIXED_READ_TIME		= 3;
		private static final byte READ_STATE_FIXED_READ_INDEXES		= 4;
		private static final byte READ_STATE_VARIABLE_READ_TIME		= 5;
		private static final byte READ_STATE_VARIABLE_READ_INDEX	= 6;
		
		private byte readState = READ_STATE_FRAMES_WIDTH;
		
		/** Offset na posi��o de cada frame (valor tempor�rio). */
		private Point[] offsets;
		
		private final Vector indexes = new Vector();
		private final Vector times = new Vector();
		private final Vector sequenceIndexVector = new Vector();		
		private final Vector sequenceTimeVector = new Vector();		
		private final StringBuffer buffer = new StringBuffer();
		
		private final Image[] frames;
		
		private final Point totalSize = new Point();
		
		
		private FrameReader( String descritorFilename, String framesFilenamePrefix, PaletteMap[] map ) throws Exception
		{
			AppMIDlet.openJarFile( descritorFilename, this );

			final int TOTAL_FRAMES = offsets.length;
			frames = new Image[offsets.length];
			final String PREFIX = framesFilenamePrefix + '_';

			// aloca os frames
			if( map == null )
			{
				for( int i = 0; i < TOTAL_FRAMES; ++i )
					frames[i] = ImageLoader.loadImage( PREFIX + i + ".png" );
			}
			else
			{
				for( int i = 0; i < TOTAL_FRAMES; ++i )
				{
					PaletteChanger pc = new PaletteChanger( PREFIX + i + ".png" );
					frames[i] = pc.createImage( map );
					pc = null;
					System.gc();
				}
			}
		}

		private FrameReader( String descritorFilename, String framesFilenamePrefix, int lightColot, int darkColor ) throws Exception
		{
			AppMIDlet.openJarFile( descritorFilename, this );

			final int TOTAL_FRAMES = offsets.length;
			frames = new Image[offsets.length];
			final String PREFIX = framesFilenamePrefix + '_';

			// aloca os frames
			if(( lightColot == 0 ) || ( darkColor == 0 ))
			{
				for( int i = 0; i < TOTAL_FRAMES; ++i )
					frames[i] = ImageLoader.loadImage( PREFIX + i + ".png" );
			}
			else
			{
				for( int i = 0; i < TOTAL_FRAMES; ++i )
				{
					PaletteChanger pc = new PaletteChanger( PREFIX + i + ".png" );
					frames[i] = pc.createImage( lightColot, darkColor );
					pc = null;
					System.gc();
				}
			}
		}
		
		public final void write( DataOutputStream output ) throws Exception {
		}
		
		public final void read( DataInputStream input ) throws Exception {
			try {
				while ( true ) {
					final char c = ( char ) input.readUnsignedByte();
					
					switch ( c ) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							buffer.append( c );
						break;
						
						case FRAMESET_DESCRIPTOR_SEQUENCE_FIXED:
							stateEnd();
							setReadState( READ_STATE_FIXED_READ_TIME );
						break;
						
						case FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE:
							stateEnd();
							times.removeAllElements();
							indexes.removeAllElements();
							
							setReadState( READ_STATE_VARIABLE_READ_TIME );
						break;						
						
						default:
							if ( buffer.length() > 0 ) {
								// terminou de ler um valor
								switch ( readState ) {
									case READ_STATE_FRAMES_WIDTH:
										totalSize.x = Integer.valueOf( buffer.toString() ).intValue();
										buffer.delete( 0, buffer.length() );
										
										setReadState( READ_STATE_FRAMES_HEIGHT );
									break;
									
									case READ_STATE_FRAMES_HEIGHT:
										totalSize.y = Integer.valueOf( buffer.toString() ).intValue();
										buffer.delete( 0, buffer.length() );
										
										setReadState( READ_STATE_FRAMES_OFFSETS );
									break;
									
									case READ_STATE_FRAMES_OFFSETS:
										insertValueFromBuffer( indexes );
									break;
									
									case READ_STATE_FIXED_READ_TIME:
										insertValueFromBuffer( times  );
										setReadState( READ_STATE_FIXED_READ_INDEXES );
									break;
									
									case READ_STATE_FIXED_READ_INDEXES:
										insertValueFromBuffer( indexes );
									break;
									
									case READ_STATE_VARIABLE_READ_TIME:
										insertValueFromBuffer( times  );
										setReadState( READ_STATE_VARIABLE_READ_INDEX );
									break;

									case READ_STATE_VARIABLE_READ_INDEX:
										insertValueFromBuffer( indexes );
										setReadState( READ_STATE_VARIABLE_READ_TIME );
									break;
								}
							} // fim if ( buffer.length() > 0 )
						// fim default
					} // fim switch ( c )
				} // fim while ( true )
			} catch ( EOFException eof ) {
				// leu todos os valores presentes no arquivo
				stateEnd();
				
				//#if DEBUG == "true"
//# 				// no modo debug, lan�a exce��o caso o leitor termine num estado intermedi�rio, o que indica erro no 
//# 				// arquivo descritor de frames.
//# 				switch ( readState ) {
//# 					case READ_STATE_FIXED_READ_TIME:
//# 					case READ_STATE_VARIABLE_READ_INDEX:
//# 					throw new Exception( "Erro no descritor de sprites: fim detectado no estado de leitura #" + readState + "." );
//# 				}
//# 				
//# 				if ( sequenceIndexVector.size() == 0 )
//# 					throw new Exception( "Erro no descritor de sprites: nenhuma sequ�ncia definida." );
				//#endif
			}
		}
		
		
		private final void setReadState( int readState ) {
			switch ( readState ) {
				case READ_STATE_FIXED_READ_TIME:
					times.removeAllElements();
				case READ_STATE_FIXED_READ_INDEXES:
					indexes.removeAllElements();
				break;
			}
			
			this.readState = ( byte ) readState;
		}
		
		
		private final void stateEnd() {
			switch ( readState ) {
				case READ_STATE_FRAMES_OFFSETS:
					offsets = new Point[ indexes.size() >> 1 ];
					for ( int i = 0; i < offsets.length; ++i ) {
						offsets[ i ] = new Point( ( ( Integer ) indexes.elementAt( i << 1 ) ).intValue(), 
												( ( Integer ) indexes.elementAt( ( i << 1 ) + 1 ) ).intValue() );
					}					
				break;
				
				case READ_STATE_FIXED_READ_INDEXES:
				case READ_STATE_VARIABLE_READ_TIME:
					final byte[] sequence = new byte[ indexes.size() ];
					for ( byte i = 0; i < sequence.length; ++i ) {
						sequence[ i ] = ( ( Integer ) indexes.elementAt( i ) ).byteValue();
					}
					sequenceIndexVector.addElement( sequence );
					
					final short[] time = new short[ sequence.length ];
					if ( times.size() == time.length ) {
						for ( byte i = 0; i < sequence.length; ++i ) {
							time[ i ] = ( ( Integer ) times.elementAt( i ) ).shortValue();
						}
					} else {
						final short DEFAULT_TIME = ( ( Integer ) times.elementAt( 0 ) ).shortValue();
						for ( byte i = 0; i < sequence.length; ++i ) {
							time[ i ] = DEFAULT_TIME;
						}						
					}
					sequenceTimeVector.addElement( time );					
				break;
			}
			buffer.delete( 0, buffer.length() );
		} // fim do m�todo stateEnd()
		
		
		private final void insertValueFromBuffer( Vector vector ) {
			vector.addElement( Integer.valueOf( buffer.toString() ) );
			buffer.delete( 0, buffer.length() );
		}
		
		
		private final byte[][] getSequences() {
			final byte[][] sequences = new byte[ sequenceIndexVector.size() ][];

			for ( byte i = 0; i < sequences.length; ++i ) {
				sequences[ i ] = ( byte[] ) ( sequenceIndexVector.elementAt( i ) );
			}			
			
			return sequences;
		}
		
		
		private final short[][] getFrameTimes() {
			final short[][] frameTimes = new short[ sequenceIndexVector.size() ][];	
			
			for ( byte i = 0; i < frameTimes.length; ++i ) {
				frameTimes[ i ] = ( short[] ) ( sequenceTimeVector.elementAt( i ) );
			}

			return frameTimes;
		}
		
	}; // fim da classe interna FileHandler	
	
	//</editor-fold>	
	
}
