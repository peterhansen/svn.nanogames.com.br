/**
 * Mutex.java
 * �2008 Nano Games.
 *
 * Created on 10/03/2008 14:42:30.
 */

package br.com.nanogames.components.util;


/**
 * 
 * @author Daniel L. Alves
 */
public final class Mutex
{
	/** Refer�ncia para a thread que possui o controle da exclus�o m�tua */
	private Thread mutexOwner;
	
	/** Tempo que uma thread fica aguardando antes de tentar obter o mutex novamente */
	private final short DEFAULT_WAIT_TIME = 1000;
	
	/** 
	 * Obt�m o recurso caso este esteja livre. Caso contr�rio, fica esperando o recurso ser liberado.
	 * @return boolean indicando se conseguiu obter acesso ao recurso.
	 * @see #release()
	 */
	public synchronized final boolean acquire()
	{
		while( mutexOwner != null && mutexOwner.isAlive() )
		{
			try
			{
				wait( DEFAULT_WAIT_TIME );
			}
			catch( Throwable ex )
			{
				return false;
			}
		}
		mutexOwner = Thread.currentThread();
		return true;
	}
	
	
	/** 
	 * Libera o recurso, avisando todas as Threads que possam estar esperando por ele.
	 * @see #acquire()
	 */
	public synchronized final void release()
	{
		if( Thread.currentThread() == mutexOwner )
		{
			mutexOwner = null;
			notifyAll();
		}
	}
}
