/*
 * UpdatableGroup.java
 *
 * Created on April 11, 2007, 3:07 PM
 *
 */

package br.com.nanogames.components;

/**
 *
 * @author peter
 */
public class UpdatableGroup extends DrawableGroup implements Updatable {
	
	// array que cont�m refer�ncias para todos os drawables atualiz�veis, para otimizar a atualiza��o do grupo
	protected final Updatable[] updatables;
	// quantidade de atualiz�veis ativos no array
	protected short activeUpdatables;
	
	
	/** 
	 * Creates a new instance of UpdatableGroup
	 * @param slots 
	 */
	public UpdatableGroup( int slots ) {
		super( slots );
		
		updatables = new Updatable[ slots ];		
	}
	

	public void update( int delta ) {
		for ( short i = 0; i < activeUpdatables; ++i )
			updatables[ i ].update( delta );		
	}
	
	
	/**
	 * Insere um drawable no pr�ximo slot dispon�vel.
	 * 
	 * @param drawable drawable a ser inserido no grupo.
	 * @return o �ndice onde o drawable foi inserido, ou -1 caso n�o hajam mais slots dispon�veis.
	 */
	public short insertDrawable( Drawable drawable ) {
		final short retValue = super.insertDrawable( drawable );
		
		// caso o drawable tenha sido inserido com sucesso e seja atualiz�vel, o insere tamb�m no array de atualiz�veis
		if ( retValue >= 0 && drawable instanceof Updatable )
			updatables[ activeUpdatables++ ] = ( Updatable ) drawable;
			
		return retValue;
	} // fim do m�todo insertDrawable( Drawable )
	

    /**
     * Remove um drawable do grupo.
     * @param index �ndice do drawable a ser removido no grupo.
     * @return uma refer�ncia para o drawable removido, ou null caso o �ndice seja inv�lido.
     */
	public Drawable removeDrawable( int index ) {
		final Drawable removed = super.removeDrawable( index );
		
		// caso o drawable removido seja atualiz�vel, o remove tamb�m da lista de atualiz�veis
		if ( removed instanceof Updatable ) {
			// � necess�rio varrer a lista pois os �ndices dos drawables n�o necessariamente s�o os mesmos �ndices
			// dos atualiz�veis
			for ( short i = 0; i < activeUpdatables; ++i ) {
				if ( updatables[ i ] == removed ) {
					for ( int j = i; j < activeUpdatables - 1; ++j )
						updatables[ j ] = updatables[ j + 1 ];

					// reduz o contador de atualiz�veis
					--activeUpdatables;
					// anula a refer�ncia no array para o antigo �ltimo elemento (caso contr�rio, haveria entradas
					// duplicadas no array, podendo causar futuros vazamentos de mem�ria)
					updatables[ activeUpdatables ] = null;
					
					break;
				} // fim if ( updatables[ i ] == removed )
			} // fim for ( short i = 0; i < activeUpdatables; ++i )
		} // fim if ( removed instanceof Updatable )
		
		return removed;
	} // fim do m�todo removeDrawable( int )	
	
}
