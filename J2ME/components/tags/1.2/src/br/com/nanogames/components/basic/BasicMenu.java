/*
 * BasicMenu.java
 *
 * Created on October 4, 2007, 10:24 AM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import java.util.Hashtable;

/**
 *
 * @author peter
 */
public class BasicMenu extends Menu {
	
	protected final byte backIndex;
	
	protected final byte firstEntry;
	
	/** Estrutura que armazena a �ltima entrada selecionada de cada menu. A chave de cada entrada � o id do menu,
	 * e o valor da chave � o �ndice da �ltima entrada utilizada.
	 */
	protected static final Hashtable LAST_INDEX = new Hashtable();	
	
	

	/** 
	 * Aloca um novo menu b�sico, com as seguintes caracter�sticas: centralizado na tela, circular, textos centralizados
	 * horizontalmente.
	 * 
	 * @param listener listener do menu, que receber� os eventos de troca de item do menu e confirma��o de um item.
	 * @param id identifica��o deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entries �ndices dos textos que comp�em cada entrada de menu.
	 * @param itemSpacing espa�amento vertical entre os itens. Por padr�o, o espa�amento � a altura de cada entrada
	 * de menu. Este valor pode ser usado tanto para aumentar essa dist�ncia (valores positivos), quanto para reduzi-la
	 * (valores negativos).
	 * @param firstEntryIndex �ndice da primeira op��o selecion�vel do menu. Caso o menu use a primeira entrada como
	 * t�tulo, por exemplo, basta passar o valor 1. Caso todas as entradas sejam selecion�veis, basta passar 0 (zero).
	 * @param backIndex indica se a �ltima entrada de menu possui a mesma fun��o das teclas de retorno, como 
	 * ScreenManager.KEY_BACK, ScreenManager.KEY_SOFT_RIGHT e ScreenManager.KEY_CLEAR.
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */	
	public BasicMenu( MenuListener listener, int id, ImageFont font, int[] entries, int itemSpacing, int firstEntryIndex, int backIndex ) throws Exception {
		this( listener, id, font, entries, itemSpacing, firstEntryIndex, backIndex, null );
	}
	
	
	/** 
	 * Aloca um novo menu b�sico, com as seguintes caracter�sticas: centralizado na tela, circular, textos centralizados
	 * horizontalmente.
	 * 
	 * @param listener listener do menu, que receber� os eventos de troca de item do menu e confirma��o de um item.
	 * @param id identifica��o deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entries �ndices dos textos que comp�em cada entrada de menu.
	 * @param itemSpacing espa�amento vertical entre os itens. Por padr�o, o espa�amento � a altura de cada entrada
	 * de menu. Este valor pode ser usado tanto para aumentar essa dist�ncia (valores positivos), quanto para reduzi-la
	 * (valores negativos).
	 * @param firstEntryIndex �ndice da primeira op��o selecion�vel do menu. Caso o menu use a primeira entrada como
	 * t�tulo, por exemplo, basta passar o valor 1. Caso todas as entradas sejam selecion�veis, basta passar 0 (zero).
	 * @param backIndex indica se a �ltima entrada de menu possui a mesma fun��o das teclas de retorno, como 
	 * ScreenManager.KEY_BACK, ScreenManager.KEY_SOFT_RIGHT e ScreenManager.KEY_CLEAR.
	 * @param title 
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */
	public BasicMenu( MenuListener listener, int id, ImageFont font, int[] entries, int itemSpacing, int firstEntryIndex, int backIndex, Drawable title ) throws Exception {
		super( listener, id, entries.length + ( title == null ? 0 : 1 ) );
		
		this.backIndex = ( byte ) backIndex;
		this.firstEntry = ( byte ) firstEntryIndex;
		
		final int FONT_HEIGHT = font.getImage().getHeight();
		int y = FONT_HEIGHT >> 1;
		
		if ( title != null ) {
			insertDrawable( title );
			title.defineReferencePixel( title.getWidth() >> 1, 0 );
			title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 0 );
			y += title.getHeight();
		}
		
		for ( int i = 0; i < entries.length; ++i ) {
			final Label label = new Label( font, AppMIDlet.getText( entries[ i ] ) );
			insertDrawable( label );

			label.defineReferencePixel( label.getWidth() >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, y );
			
			y += FONT_HEIGHT + itemSpacing;
		}

		setCircular( true );
		
		final Integer lastIndex = ( Integer ) LAST_INDEX.get( new Integer( id ) );
		if ( lastIndex == null )
			setCurrentIndex( firstEntryIndex );
		else {
			final int index = lastIndex.intValue();
			if ( index == backIndex )
				setCurrentIndex( 0 );
			else
				setCurrentIndex( index );		
		}

		setSize( ScreenManager.SCREEN_WIDTH, y + ( FONT_HEIGHT >> 1 ) );
		defineReferencePixel( ScreenManager.SCREEN_HALF_WIDTH, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );	

		// atualiza o cursor (largura do texto pode mudar)
		setCurrentIndex( currentIndex );	
	}
	

	/** 
	 * Aloca um novo menu b�sico, com as seguintes caracter�sticas: centralizado na tela, circular, textos centralizados
	 * horizontalmente. Equivalente � chamada do construtor BasicMenu( listener, id, font, entries, 0, 0, entries.length - 1 ).
	 * 
	 * @param listener listener do menu, que receber� os eventos de troca de item do menu e confirma��o de um item.
	 * @param id identifica��o deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entries �ndices dos textos que comp�em cada entrada de menu.
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */
	public BasicMenu( MenuListener listener, int id, ImageFont font, int[] entries ) throws Exception {
		this( listener, id, font, entries, 0, 0, entries.length - 1 );
	}	
	
	
	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if ( backIndex >= 0 ) {
					setCurrentIndex( backIndex );
					super.keyPressed( ScreenManager.KEY_NUM5 );
				}
			break;
			
			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
			break;

			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}

	
	public final void setCurrentIndex( int index ) {
		final int previousIndex = currentIndex;
		
		super.setCurrentIndex( index );
		
		if ( currentIndex <= firstEntry ) {
			if ( previousIndex < firstEntry || previousIndex == activeDrawables - 1 )
				super.setCurrentIndex( firstEntry );
			else if ( previousIndex == firstEntry ) {
				if ( firstEntry == 0 )
					super.setCurrentIndex( 0 );
				else 
					super.setCurrentIndex( circular ? activeDrawables - 1 : firstEntry );
			}
		}
		
		// grava o �ndice atual para futuras inst�ncias de menu com o mesmo id
		LAST_INDEX.put( new Integer( menuId ), new Integer( currentIndex ) );
	}
	
	
	/**
	 * Obt�m o �ndice da op��o selecionada pelo ponteiro.
	 * 
	 * @param x posi��o x do evento de ponteiro na tela.
	 * @param y posi��o y do evento de ponteiro na tela.
	 * @return �ndice da op��o selecionada, ou -1 caso a posi��o n�o intercepte nenhuma entrada.
	 */
	protected final int getEntryAt( int x, int y ) {
		// TODO caso o menu esteja inserido em outra cole��o em vez de ser a tela ativa, o c�lculo da posi��o pressionada estar� incorreto.
		x -= position.x;
		y -= position.y;
		
		for ( byte i = firstEntry; i < activeDrawables; ++i ) {
			final Drawable entry = drawables[ i ];
			if ( entry.contains( x, y ) )
				return i;
		}
		return -1;
	}	
	

}
