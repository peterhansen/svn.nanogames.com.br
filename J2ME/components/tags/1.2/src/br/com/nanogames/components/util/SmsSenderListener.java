/**
 * SmsSenderListener.java
 * ©2008 Nano Games.
 *
 * Created on 29/07/2008 12:41:09.
 */

package br.com.nanogames.components.util;

/**
 * @author Daniel L. Alves
 */

/** Interface que permite o recebimento de informações sobre o andamento do envio de SMSs através da classe
 * SmsSender
 */
public interface SmsSenderListener
{
	/** Informa se a tentativa de envio do SMS foi bem sucedida */
	public void onSMSSent( int id, boolean smsSent );

	//#if DEBUG == "true"
//# 	/** Fornece informações passo-a-passo das rotinas de envio de SMS */
//# 	public void onSMSLog( int id, String log );
	//#endif
}
