/**
 * ScrollRichLabel.java
 * �2008 Nano Games.
 *
 * Created on Jul 8, 2008 10:59:48 PM.
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;


/**
 * 
 * @author Peter
 */
public class ScrollRichLabel extends UpdatableGroup implements KeyListener, PointerListener {
	
	protected static final byte INDEX_LABEL = 0;
	protected static final byte INDEX_SCROLL_FULL = 1;
	protected static final byte INDEX_SCROLL_PAGE = 2;
	
	/** Modo de arrasto do ponteiro: sem arrasto. */
	protected static final byte DRAG_MODE_NONE			= 0;
	/** Modo de arrasto do ponteiro: arrastando barra de scroll da p�gina atual. */
	protected static final byte DRAG_MODE_SCROLL_PAGE	= 1;
	/** Modo de arrasto do ponteiro: arrastando texto. */
	protected static final byte DRAG_MODE_TEXT			= 2;

	protected final RichLabel label;
	
	protected Drawable scrollFull;
	
	protected Drawable scrollPage;
	
	/** Velocidade de movimenta��o do texto. */
	protected final MUV textSpeed = new MUV();
	
	/** Velocidade m�xima de movimenta��o do texto. No caso de movimenta��o autom�tica ou atrav�s de teclas, esse valor
	 * � sempre utilizado. No caso de movimenta��o atrav�s de ponteiro, a velocidade � gradual.
	 */
	protected short TEXT_SPEED;
	
	/** Limite superior do texto. */
	protected short textLimitTop;
	
	/** Limite inferior do texto. */
	protected short textLimitBottom;
	
	/** Indica se o scroll do texto est� no modo autom�tico. */
	protected boolean autoScroll;	
	
	/** �ltima posi��o do ponteiro (valor utilizado para fazer scroll). */
	protected int lastPointerY;	
	
	/** Indica o tipo de arrasto do ponteiro. */
	protected byte dragMode;
	
	/** Posi��o de in�cio do arrasto da barra de scroll da p�gina. */
	protected short dragYStart;
	
	/** Offset do texto no momento em que a barra de scroll da p�gina come�ou a ser arrastada. */
	protected short textOffsetStart;
	
	/** Armazena a �ltima tecla apertada. Ver keyPressed() e keyReleased() */
	private int lastKeyPressed;
	
	public ScrollRichLabel( RichLabel label ) {
		this( label, null, null );
	}
	
	
	public ScrollRichLabel( RichLabel label, Drawable scrollFull, Drawable scrollPage ) {
		super( 3 );
		
		//#if DEBUG == "true"
//# 		if ( label == null )
//# 			throw new IllegalArgumentException( "ScrollRichLabel: label n�o pode ser nulo." );
		//#endif
		
		this.label = label;
		insertDrawable( label );
		
		setScrollFull( scrollFull );
		setScrollPage( scrollPage );
		
		setAutoScroll( false );
	}


	public void update( int delta ) {
		super.update( delta );
		
		if ( textSpeed.getSpeed() != 0 ) {
			final int dy = textSpeed.updateInt( delta );

			if ( dy != 0 )
				setTextOffset( label.getTextOffset() + dy );
		}
	}
	
	
	public void setTextOffset( int offset ) {
		if ( offset > textLimitBottom ) {
			offset = textLimitBottom;
		} else if ( offset < textLimitTop ) {
			if ( autoScroll ) {
				offset = textLimitBottom;
			} else {
				offset = textLimitTop;
				setTextSpeed( 0 );
			}
		}
		
		label.setTextOffset( offset );
		updateScroll();
	}	
	
	
	protected void setTextSpeed( int speed ) {
		if ( autoScroll )
			textSpeed.setSpeed( -Math.abs( TEXT_SPEED ) );
		else
			textSpeed.setSpeed( speed );
	}	
	

	public void onPointerDragged( int x, int y ) {
		if ( !autoScroll ) {
			y -= position.y;
			
			switch ( dragMode ) {
				case DRAG_MODE_SCROLL_PAGE:
					// posiciona o texto de acordo com a posi��o relativa da barra de scroll
					final int FP_SCROLL_TO_TEXT = NanoMath.divInt( size.y, scrollPage.getHeight() );
					final int FP_TEMP = NanoMath.toFixed( y - dragYStart );
					setTextOffset( textOffsetStart - NanoMath.toInt( NanoMath.mulFixed( FP_SCROLL_TO_TEXT, FP_TEMP ) ) );
				break;
				
				case DRAG_MODE_TEXT:
					setTextOffset( label.getTextOffset() + y - lastPointerY );
					lastPointerY = y;
				break;
			}
		}		
	}


	public void onPointerPressed( int x, int y ) {
		if ( !autoScroll ) {
			x -= position.x;
			y -= position.y;

			if ( scrollPage != null && scrollPage.contains( x, y ) ) {
				// usu�rio clicou na barra de scroll da p�gina atual
				dragMode = DRAG_MODE_SCROLL_PAGE;
				lastPointerY = y;
				dragYStart = ( short ) y;
				textOffsetStart = ( short ) label.getTextOffset();
			} else if ( scrollFull != null && scrollFull.contains( x, y ) ) {
				// usu�rio clicou na barra de scroll total
				dragMode = DRAG_MODE_NONE;
				
				if ( scrollPage != null ) {
					// avan�a ou retrocede uma p�gina, de acordo com a posi��o clicada na barra
					keyPressed( y < scrollPage.getPosY() ? ScreenManager.LEFT : ScreenManager.RIGHT );
				}
			} else {
				// usu�rio clicou no texto
				dragMode = DRAG_MODE_TEXT;
				lastPointerY = y;
			}
		}
	}


	public void onPointerReleased( int x, int y ) {
		dragMode = DRAG_MODE_NONE;
	}
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		final int TEXT_TOTAL_HEIGHT = label.getTextTotalHeight();
		
		if ( scrollFull != null ) {
			if ( TEXT_TOTAL_HEIGHT <= height ) {
				scrollFull.setVisible( false );
				
				label.setSize( size );
			} else {
				scrollFull.setVisible( true );
				
				scrollFull.setPosition( width - scrollFull.getWidth(), 0 );
				scrollFull.setSize( scrollFull.getWidth(), size.y );

				label.setSize( scrollFull.getPosX(), height );
			}
		} else {
			label.setSize( size );
		}
		
		if ( scrollPage != null ) {
			scrollPage.setVisible( TEXT_TOTAL_HEIGHT > height );
			scrollPage.setPosition( width - scrollPage.getWidth(), 0 );
		}

		label.formatText( false );
		
		if ( autoScroll ) {
			// FIXME erro na altura de fim de scroll autom�tico
			textLimitBottom = ( short ) size.y;
			
			setTextOffset( textLimitBottom );
		} else {
			textLimitTop = ( short ) NanoMath.min( -label.getTextTotalHeight() + size.y, 0 );
		}			
		
		setAutoScroll( autoScroll );
		updateScroll();
	}

	
	protected void updateScroll() {
		if ( scrollPage != null ) {
			final int TEXT_TOTAL_HEIGHT = label.getTextTotalHeight();
			
			if( TEXT_TOTAL_HEIGHT > 0 ) {
				scrollPage.setSize( scrollPage.getWidth(), Math.min( ( size.y * size.y / TEXT_TOTAL_HEIGHT ) + 1, size.y ) );
				scrollPage.setPosition( scrollPage.getPosX(), size.y * -label.getTextOffset() / TEXT_TOTAL_HEIGHT );
			}
		}
	}


	public void keyPressed( int key ) {
		
		// Armazena a tecla apertada para posterior utiliza��o em keyReleased()
		lastKeyPressed = key;

		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setTextSpeed( Math.abs( TEXT_SPEED ) );
			break;
			
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setTextSpeed( -Math.abs( TEXT_SPEED ) );
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				if ( !autoScroll )
					setTextOffset( label.getTextOffset() + size.y );
			break;			
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				if ( !autoScroll )
					setTextOffset( label.getTextOffset() - size.y );
			break;
			
			default:
				setTextSpeed( 0 );
		} // fim switch ( key )
	}


	public void keyReleased( int key ) {
		// Esse if faz com que os aparelhos n�o travem a rolgem ao mudarmos sua dire��o repentinamente. No
		// entanto, alguns aparelhos podem n�o mandar o evento de release necess�rio se duas teclas forem
		// apertadas simultaneamente.
		if( key == lastKeyPressed )
			setTextSpeed( 0 );
	}
	
	
	public void setAutoScroll( boolean autoScroll ) {
		this.autoScroll = autoScroll;
		
		if ( autoScroll ) {
			TEXT_SPEED = ( short ) ( ( label.getFont().getHeight() * 3 ) >> 1 );
			textLimitTop = ( short ) -label.getTextTotalHeight();
		} else {
			TEXT_SPEED = ( short ) ( ( label.getFont().getHeight() * 13 ) >> 1 );
			textLimitBottom = 0;
		}			
	}
	
	
	public final Drawable getScrollFull() {
		return scrollFull;
	}
	
	
	public final Drawable getScrollPage() {
		return scrollPage;
	}
	
	
	public void setScrollFull( Drawable scrollFull ) {
		if ( this.scrollFull != null )
			removeDrawable( INDEX_SCROLL_FULL );
		
		if ( scrollFull != null )
			insertDrawable( scrollFull, INDEX_SCROLL_FULL );
		
		this.scrollFull = scrollFull;
		
		setSize( size );
	}
	
	
	public void setScrollPage( Drawable scrollPage ) {
		if ( this.scrollPage != null )
			removeDrawable( scrollFull == null ? INDEX_SCROLL_FULL : INDEX_SCROLL_PAGE );
		
		if ( scrollFull != null )
			insertDrawable( scrollPage, INDEX_SCROLL_PAGE );
		
		this.scrollPage = scrollPage;

		setSize( size );
	}
}
