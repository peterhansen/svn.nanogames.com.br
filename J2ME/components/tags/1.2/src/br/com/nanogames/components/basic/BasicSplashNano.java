/*
 * SplashNano.java
 *
 * Created on June 15, 2007, 11:32 AM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;

//#if DEBUG == "true"
//# import br.com.nanogames.components.userInterface.KeyListener;
//#endif


/**
 *
 * @author peter
 */
public final class BasicSplashNano extends UpdatableGroup 
	//#if DEBUG == "true"
//# 		implements KeyListener
	//#endif
{
	
	public static final byte SCREEN_SIZE_SMALL	= 0;
	public static final byte SCREEN_SIZE_MEDIUM	= 1;
	
	private final int BACKLIGHT_COLOR = 0x1A9DBA;
	
	private final MUV lightSpeed;
	
	// velocidade do efeito da luz em pixels por segundo
	private final int LIGHT_MOVE_SPEED;

	private final int LIGHT_X_OFFSET;
	private final int LIGHT_X_LEFT;
	private final int LIGHT_X_RIGHT;	

	// offsets na posi��o dos logos em rela��o ao texto "Nano"
	private final short IMG_GAMES_OFFSET_X;
	private final short IMG_GAMES_OFFSET_Y;
	private final short IMG_TURTLE_OFFSET_X;
	private final short IMG_TURTLE_OFFSET_Y;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private final DrawableImage imgNano;
	private final DrawableImage imgNanoLight;
	private final DrawableImage imgGames;
	private final DrawableImage imgTurtle;
	
	private final ImageFont font;
	
	private final Pattern transparency;
	private final Pattern backColor;
	private final Pattern blackLeft;
	private final Pattern blackRight;
	
	// estados da anima��o do splash
	private final byte STATE_NANO_APPEARS	= 0;
	private final byte STATE_LIGHT_RIGHT	= STATE_NANO_APPEARS + 1;
	private final byte STATE_LIGHT_LEFT		= STATE_LIGHT_RIGHT + 1;
	private final byte STATE_WAIT			= STATE_LIGHT_LEFT + 1;
	private final byte STATE_NONE			= STATE_WAIT + 1;
	
	// dura��o em milisegundos de cada estado de anima��o
	private final short DURATION_NANO_APPEARS	= 600;
	private final short DURATION_WAIT			= 2100;
	
	/** tema de abertura a ser tocado */
	private final byte SOUND_INDEX;
	
	/** Valor que identifica esta tela ao listener. */
	private final int nextScreenIndex;
	
	private byte currentState;
	
	private int accTime;
	
	
	/** Creates a new instance of SplashNano
	 * @param nextScreenIndex �ndice da tela a ser exibida ap�s o splash da Nano - o valor � passado para o m�todo 
	 * <code>AppMIDlet.setScreen(int)</code>.
	 * @param screenSize tamanho da tela. Valores v�lidos: SCREEN_SIZE_SMALL (telas com largura at� 132 pixels) ou 
	 * SCREEN_SIZE_MEDIUM (telas com 176 pixels ou mais de largura).
	 * @param imagesPath
	 * @param textIndex 
	 * @param soundIndex
	 * @throws java.lang.Exception 
	 */
	public BasicSplashNano( int nextScreenIndex, byte screenSize, String imagesPath, int textIndex, int soundIndex ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final Pattern bkg = new Pattern( null );
		bkg.setSize( size );
		bkg.setFillColor( 0x000000 );
		insertDrawable( bkg );
		
		this.nextScreenIndex = nextScreenIndex;
		SOUND_INDEX = ( byte ) soundIndex;
		
		// logo da Nano
		imgNano = new DrawableImage( imagesPath + "nano.png" );
		imgNano.defineReferencePixel( imgNano.getSize().x >> 1, 0 );
		
		if ( screenSize == SCREEN_SIZE_SMALL ) {
			// velocidade do efeito da luz em pixels por segundo
			LIGHT_MOVE_SPEED = 115;

			LIGHT_X_OFFSET = 0;

			// offsets na posi��o dos logos em rela��o ao texto "Nano"
			IMG_GAMES_OFFSET_X = 26;
			IMG_GAMES_OFFSET_Y = 10;
			IMG_TURTLE_OFFSET_X = 24;
			IMG_TURTLE_OFFSET_Y = -32;

			imgNano.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT - 6 );
		} else {
			// velocidade do efeito da luz em pixels por segundo
			LIGHT_MOVE_SPEED = 157;

			LIGHT_X_OFFSET = 8;

			// offsets na posi��o dos logos em rela��o ao texto "Nano"
			IMG_GAMES_OFFSET_X = 65;
			IMG_GAMES_OFFSET_Y = 25;
			IMG_TURTLE_OFFSET_X = 59;
			IMG_TURTLE_OFFSET_Y = -69;

			imgNano.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		}
		
		lightSpeed = new MUV( LIGHT_MOVE_SPEED );
		
		LIGHT_X_LEFT = imgNano.getPosition().x + LIGHT_X_OFFSET;
		LIGHT_X_RIGHT = imgNano.getPosition().x + imgNano.getSize().x;

		// efeito da luz passando
		imgNanoLight = new DrawableImage( imagesPath + "light.png" );
		imgNanoLight.setPosition( imgNano.getPosition().x - imgNanoLight.getSize().x, imgNano.getPosition().y );
		
		// cor de fundo do logo
		backColor = new Pattern( null );
		backColor.setFillColor( BACKLIGHT_COLOR );
		backColor.setSize( imgNano.getSize() );
		backColor.setPosition( imgNano.getPosition() );
		
		// adiciona as �reas pretas � direita e esquerda do texto "Nano", para evitar que o efeito da luz passando fique errado
		blackLeft = new Pattern( null );
		blackLeft.setFillColor( 0x000000 );
		blackLeft.setSize( imgNano.getPosition().x, imgNano.getSize().y );
		blackLeft.setPosition( 0, imgNano.getPosition().y );		
		
		blackRight = new Pattern( null );
		blackRight.setFillColor( 0x000000 );
		blackRight.setSize( ScreenManager.SCREEN_WIDTH - LIGHT_X_RIGHT, imgNano.getSize().y );
		blackRight.defineReferencePixel( blackRight.getSize().x, 0 );
		blackRight.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, imgNano.getPosition().y );
		
		insertDrawable( backColor );
		insertDrawable( imgNanoLight );
		insertDrawable( blackLeft );
		insertDrawable( blackRight );		
		insertDrawable( imgNano );
		
		imgGames = new DrawableImage( imagesPath + "games.png" );
		imgGames.setPosition( imgNano.getPosition().x + IMG_GAMES_OFFSET_X, imgNano.getPosition().y + IMG_GAMES_OFFSET_Y );
		insertDrawable( imgGames );
		
		imgTurtle = new DrawableImage( imagesPath + "turtle.png" );
		imgTurtle.setPosition( imgNano.getPosition().x + IMG_TURTLE_OFFSET_X, imgNano.getPosition().y + IMG_TURTLE_OFFSET_Y );
		insertDrawable( imgTurtle );
		
		font = ImageFont.createMultiSpacedFont( imagesPath + "font_credits.png", imagesPath + "font_credits.dat" );
		
		final RichLabel label = new RichLabel( font, AppMIDlet.getText( textIndex ), ( size.x * 3 ) >> 2, null );
		label.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		label.setRefPixelPosition( size.x >> 1, size.y );
		insertDrawable( label );
		
		transparency = new Pattern( new DrawableImage( imagesPath + "transparency.png" ) );
//		transparency.setUsesTransparency( true ); o coment�rio deve ser removido no caso de Pattern utilizar copyArea
		transparency.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		transparency.defineReferencePixel( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		transparency.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( transparency );
		
		setState( STATE_NANO_APPEARS );
	} // fim do construtor SplashNano( String )
	
	
	private final void setState( int state ) {
		switch ( state ) {
			case STATE_NANO_APPEARS:
			break;
			
			case STATE_LIGHT_RIGHT:
				if ( SOUND_INDEX >= 0 )
					MediaPlayer.play( SOUND_INDEX );
				
				transparency.setVisible( false );
			case STATE_LIGHT_LEFT:
				if ( state == STATE_LIGHT_LEFT )
					lightSpeed.setSpeed( -LIGHT_MOVE_SPEED );
			case STATE_WAIT:
			case STATE_NONE:
			break;
			
			default:
				return;
		} // fim switch ( state )
		
		currentState = ( byte ) state;
		accTime = 0;
	} // fim do m�todo setState( byte )

	
	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;
		switch ( currentState ) {
			case STATE_NANO_APPEARS:
				if ( accTime >= DURATION_NANO_APPEARS )
					setState( currentState + 1 );
			break;
			
			case STATE_LIGHT_RIGHT:
				imgNanoLight.move( lightSpeed.updateInt( delta ), 0 );
				
				if ( imgNanoLight.getPosition().x >= LIGHT_X_RIGHT ) {
					imgNanoLight.setPosition( LIGHT_X_RIGHT, imgNanoLight.getPosition().y );
					setState( currentState + 1 );
				}
			break;
			
			case STATE_LIGHT_LEFT:
				imgNanoLight.move( lightSpeed.updateInt( delta ), 0 );

				if ( imgNanoLight.getPosition().x <= LIGHT_X_LEFT ) {
					imgNanoLight.setPosition( LIGHT_X_LEFT, imgNanoLight.getPosition().y );
					setState( currentState + 1 );
				}
			break;

			case STATE_WAIT:
				if ( accTime >= DURATION_WAIT ) {
					setState( STATE_NONE );
					AppMIDlet.setScreen( nextScreenIndex );
				}
			break;
		} // fim switch ( state )		
	} // fim do m�todo update( int )


	//#if DEBUG == "true"
//# 	public final void keyPressed( int key ) {
//# 		AppMIDlet.setScreen( nextScreenIndex );
//# 	}
//# 
//# 
//# 	public final void keyReleased( int key ) {
//# 	}
	//#endif
	
	
}
