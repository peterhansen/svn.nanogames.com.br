/*
 * Drawable.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

/**
 *
 * @author peter
 */
public abstract class Drawable {
 
    /** posi��o do drawable */
	protected final Point position = new Point();
	 
    /** indica se o drawable est� vis�vel */
	protected boolean visible = true;
	 
    /** dimens�es do drawable (utilizado para fins de detec��o de colis�o e para definir �rea de clip inicial) */
	protected final Point size = new Point();
    
    /** pixel de refer�ncia (em rela��o ao tamanho do drawable, e n�o � sua posi��o) */
	protected Point referencePixel = new Point();
	 
    /** transforma��o aplicada ao drawable (padr�o: TRANS_NONE). Pode haver m�ltiplas transforma��es (espelhamento
	 * horizontal e rota��o de 90�, por exemplo), atrav�s do uso do operador matem�tico OU: |
	 */
	protected int transform;
	
	/** transforma��o passada �s classes MIDP que utilizam transforma��o (valores para as mesmas transforma��es s�o diferentes) */
	protected int transformMIDP;
	
	/** 
	 * �rea vis�vel do drawable <b>nas coordenadas da tela</b>. Caso seja <code>null</code>, � utilizada a �rea total da tela.
	 */
	protected Rectangle viewport;
	
    /** nenhuma transforma��o */
	public static final byte TRANS_NONE = 0;    
    /** espelhamento vertical */
	public static final byte TRANS_MIRROR_V = 1;	
    /** espelhamento horizontal */
	public static final byte TRANS_MIRROR_H = 2;
    /** rota��o de 90� no sentido hor�rio */
	public static final byte TRANS_ROT90 = 4;
    /** rota��o de 180� */
	public static final byte TRANS_ROT180 = 8;
    /** rota��o de 270� no sentido hor�rio */
	public static final byte TRANS_ROT270 = 16;
	
	protected static final byte TRANS_MASK_MIRROR = ( byte ) ( TRANS_MIRROR_H | TRANS_MIRROR_V );
	protected static final byte TRANS_MASK_ROTATE = ( byte ) ( TRANS_ROT90 | TRANS_ROT180 | TRANS_ROT270 );
	protected static final byte TRANS_MASK_ALL = ( byte ) ( TRANS_MASK_MIRROR | TRANS_MASK_ROTATE );
	
	/** Constante para definir a �ncora do drawable relativa � sua posi��o vertical do topo (y = 0). */
	public static final byte ANCHOR_TOP		= Graphics.TOP;
	/** Constante para definir a �ncora do drawable relativa � sua posi��o horizontal esquerda (x = 0). */
	public static final byte ANCHOR_LEFT	= Graphics.LEFT;
	/** Constante para definir a �ncora do drawable relativa � sua posi��o vertical inferior (y = size.y). */
	public static final byte ANCHOR_BOTTOM	= Graphics.BOTTOM;
	/** Constante para definir a �ncora do drawable relativa � sua posi��o horizontal da direita (x = size.x). */
	public static final byte ANCHOR_RIGHT	= Graphics.RIGHT;
	/** Constante para definir a �ncora do drawable relativa � sua posi��o vertical central (y = size.y / 2). */
	public static final byte ANCHOR_VCENTER	= Graphics.VCENTER;
	/** Constante para definir a �ncora do drawable relativa � sua posi��o horizontal central (x = size.x / 2). */
	public static final byte ANCHOR_HCENTER	= Graphics.HCENTER;
	
	private static final byte ANCHOR_HORIZONTAL_MASK = ANCHOR_LEFT | ANCHOR_HCENTER | ANCHOR_RIGHT;
    private static final byte ANCHOR_VERTICAL_MASK = ANCHOR_TOP | ANCHOR_VCENTER | ANCHOR_BOTTOM;
    
	/** Altura m�xima da pilha de clip. */
	private static final byte MAX_CLIP_STACK_SIZE = 32;
	/** Pilha de �reas de clip usada pelos drawables no momento do desenho. */
    protected static final Rectangle[] clipStack = new Rectangle[ MAX_CLIP_STACK_SIZE ];
	
	protected static byte currentStackSize;
	
	/** Transla��o acumulada no desenho atual. */
	public static final Point translate = new Point();
	
	
	static {
		for ( int i = 0; i < MAX_CLIP_STACK_SIZE; ++i )
			clipStack[ i ] = new Rectangle();
	}

	 
    /**
     * Define o estado de visibilidade do drawable.
	 * 
     * @param visible estado de visibilidade do drawable.
     */
	public void setVisible( boolean visible ) {
        this.visible = visible;
	}
	
    
    /**
     * Obt�m o estado de visibilidade atual do drawable.
	 * 
     * @return boolean indicando o estado de visibilidade do drawable.
     */
	public boolean isVisible() {
		return visible;
	}
	
    
    /**
     * Posiciona o ponto top-left (0,0) do drawable na posi��o (x,y). Esse m�todo n�o leva em considera��o o ponto de 
	 * refer�ncia do drawable; para posicionar o ponto de refer�ncia em (x,y), deve-se utilizar o m�todo <b>setRefPixelPosition</b>.
     * @param x posi��o horizontal onde o ponto mais � esquerda do drawable ser� posicionado.
     * @param y posi��o vertical onde o ponto mais superior do drawable ser� posicionado.
	 * @see #setPosition(Point)
	 * @see #setRefPixelPosition(int, int)
	 * @see #setRefPixelPosition(Point)
     */
	public void setPosition( int x, int y ) {
        position.set( x, y );
	}
	
	
	/**
	 * Posiciona o ponto top-left (0,0) do drawable na posi��o do Point recebido como par�metro. Esse m�todo n�o leva em 
	 * considera��o o ponto de refer�ncia do drawable; para posicionar o ponto de refer�ncia em (x,y), deve-se utilizar 
	 * o m�todo <b>setRefPixelPosition</b>. A chamada desse m�todo equivale a <b>setPosition( p.x, p.y )</b>.
	 * @param p ponto nova posi��o top-left do drawable.
	 * @see #setPosition(int,int)
	 * @see #setRefPixelPosition(int, int)
	 * @see #setRefPixelPosition(Point)
	 */
	public final void setPosition( Point p ) {
		setPosition( p.x, p.y );
	}
	
    
    /**
     * Retorna a posi��o top-left do drawable.
	 * 
     * @return refer�ncia para a posi��o do drawable.
	 * @see #getPosX()
	 * @see #getPosY()
     */
	public final Point getPosition() {
		return position;
	}
	
	
	/**
	 * Retorna a posi��o x (esquerda) do drawable.
	 * 
	 * @return inteiro indicando a posi��o horizontal (esquerda) do drawable.
	 * @see #getPosition()
	 * @see #getPosY()
	 */
	public final int getPosX() {
		return position.x;
	}
	
	
	/**
	 * Retorna a posi��o y (topo) do drawable.
	 * 
	 * @return inteiro indicando a posi��o vertical (topo) do drawable.
	 * @see #getPosition()
	 * @see #getPosX()
	 */	
	public final int getPosY() {
		return position.y;
	}
	
    
    /**
     * Realiza as opera��es de clip e transla��o do drawable antes das chamadas efetivas de desenho no Graphics, levando
	 * em considera��o a �rea de clip e posi��o do grupo "pai" do drawable, de forma que a �rea de clip n�o seja ultrapassada. 
	 * <p>Esse m�todo n�o executa chamadas diretas de desenho no Graphics; elas devem ser feitas no m�todo <b>paint</b>. Ap�s a
	 * chamada de paint, a �rea anterior de clip e a posi��o de desenho do grupo s�o restauradas.</p>
	 * 
     * @param g Graphics onde ser� desenhado o drawable.
	 * @see #paint(Graphics)
     */
	public void draw( Graphics g ) {
		if ( visible ) {
			pushClip( g );
			translate.addEquals( position );
			
			final Rectangle clip = clipStack[ currentStackSize ];
			clip.setIntersection( translate.x, translate.y, size.x, size.y );
			
			if ( clip.width > 0 && clip.height > 0 ) {
				g.setClip( clip.x, clip.y, clip.width, clip.height );

				paint( g );
			}

			translate.subEquals( position );
			popClip( g );
		} // fim if ( visible )        
	} // fim do m�todo draw( Graphics )
	
    
    /**
     * Move o drawable.
	 * 
     * @param dx varia��o horizontal na posi��o do drawable.
     * @param dy varia��o vertical na posi��o do drawable.
	 * @see #move(Point)
     */
	public void move( int dx, int dy ) {
        setPosition( position.x + dx, position.y + dy );
	}
    
	 
    /**
     * Move o drawable. A chamada deste m�todo equivale a <b>move( distance.x, distance.y )</b>.
	 * 
     * @param distance dist�ncia (x,y) a ser percorrida pelo drawable. 
	 * @see #move(int, int)
     */
	public final void move( Point distance ) {
        move( distance.x, distance.y );
	}
    
	 
    /**
     * Obt�m o tamanho do drawable.
	 * 
     * @return refer�ncia para o Point que define o tamanho do drawable.
	 * @see #getWidth()
	 * @see #getHeight()
     */
	public final Point getSize() {
		return size;
	}
	
	
	/**
	 * Retorna a largura do drawable.
	 * 
	 * @return inteiro indicando a largura do drawable.
	 * @see #getSize()
	 * @see #getHeight()
	 */
	public final int getWidth() {
		return size.x;
	}
	
	
	/**
	 * Retorna a altura do drawable.
	 * 
	 * @return inteiro indicando a altura do drawable.
	 * @see #getSize()
	 * @see #getWidth()
	 */	
	public final int getHeight() {
		return size.y;
	}
	 
    
    /**
     * Define o tamanho do drawable. Chamada equivalente a <b>setSize( size.x, size.y )</b>.
	 * 
     * @param size tamanho (largura, altura) do drawable.
	 * @see #setSize(int, int)
     */
	public final void setSize( Point size ) {
        setSize( size.x, size.y );
	}
	
	
	/**
	 * Define o tamanho do drawable.
	 * 
	 * @param width largura em pixels do drawable.
	 * @param height altura em pixels do drawable.
	 * @see #setSize(Point)
	 */
	public void setSize( int width, int height ) {
		size.set( width, height );
	}
	
	
	/**
	 * Obt�m o ret�ngulo da �rea vis�vel do drawable.
	 * 
	 * @return refer�ncia para o Rectangle que representa a �rea vis�vel do drawable na tela. Caso seja <code>null</code>,
	 * significa que a �rea vis�vel do drawable na tela � a tela inteira (conforme valor retornado por 
	 * <code>ScreenManager.getViewport()</code>.
	 * 
	 * @see #setViewport(Rectangle)
	 */
	public final Rectangle getViewport() {
		return viewport;
	}
	
	
	/**
	 * Define a �rea de visualiza��o efetiva do drawable na tela.
	 * 
	 * @param viewport Rectangle que define a �rea vis�vel do drawable na tela. A refer�ncia � armazenada internamente,
	 * ou seja, altera��es subsequentes ao Rectangle refletir�o automaticamente no viewport do drawable.
	 * 
	 * <p>Caso seja passado <code>null</code>, o drawable realiza a interse��o com a �rea total da tela, conforme definido
	 * na classe <code>ScreenManager</code>.
	 * 
	 * @see #getViewport()
	 */
	public final void setViewport( Rectangle viewport ) {
		this.viewport = viewport;
	}
	 
    
	/**
	 * Trata apenas do desenho em si no Graphics, ou seja, chamadas de m�todos como g.drawImage(), g.drawRect() e etc. 
	 * Todas as opera��es de clip e/ou transla��o s�o executadas antes no m�todo draw( Graphics ).
	 * 
	 * @param g 
	 * @see #draw(Graphics)
	 */
	protected abstract void paint( Graphics g );
    
    
    /**
     * Empilha a �rea de clip atual.
	 * 
     * @param g Graphics cuja �rea de clip definida ser� empilhada.
     */
	public final void pushClip( Graphics g ) {
		clipStack[ currentStackSize++ ].set( g.getClipX(), g.getClipY(), g.getClipWidth(), g.getClipHeight() );
		
		if ( viewport == null )
			clipStack[ currentStackSize ].set( clipStack[ currentStackSize - 1 ] );
		else
			clipStack[ currentStackSize ].setIntersection( clipStack[ currentStackSize - 1 ], viewport );
	}
	
    
    /**
     * Remove o topo da pilha de �reas de clip, e define a �rea de clip atual a partir dela.
	 * 
     * @param g Graphics cuja �rea de clip ser� restaurada.
     */
	public final void popClip( Graphics g ) {
		final Rectangle clip = clipStack[ --currentStackSize ];
		g.setClip( clip.x, clip.y, clip.width, clip.height );
	}
	
    
    /**
     * Define o pixel de refer�ncia do drawable. Esse m�todo apenas define o ponto a ser utilizado como refer�ncia; ele n�o
	 * altera a posi��o atual do drawable.
	 * 
     * @param refPixelX ponto x de refer�ncia do drawable.
     * @param refPixelY ponto y de refer�ncia do drawable.
	 * @see #defineReferencePixel(Point)
	 * @see #setRefPixelPosition(int,int)
	 * @see #defineReferencePixel(int)
	 * @see #setRefPixelPosition(Point)
     */
	public void defineReferencePixel( int refPixelX, int refPixelY ) {
        referencePixel.set( refPixelX, refPixelY );
	}
	 
    
    /**
     * Define o pixel de refer�ncia do drawable. Esse m�todo apenas define o ponto a ser utilizado como refer�ncia; ele n�o
	 * altera a posi��o atual do drawable. M�todo equivalente � chamada de <b>defineReferencePixel( refPixel.x, refPixel.y )</b>.
	 * 
     * @param refPixel ponto de refer�ncia (x,y) do drawable.
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(int)
	 * @see #setRefPixelPosition(int,int)
	 * @see #setRefPixelPosition(Point)
     */
	public final void defineReferencePixel( Point refPixel ) {
        defineReferencePixel( refPixel.x, refPixel.y );
	}
	
	
	/**
	 * Define o pixel de refer�ncia do drawable a partir de uma de suas �ncoras. Esse m�todo apenas define o ponto a ser 
	 * utilizado como refer�ncia; ele n�o altera a posi��o atual do drawable.
	 * 
	 * @param anchor inteiro indicando a �ncora utilizada para definir o pixel de refer�ncia. Os valores v�lidos s�o
	 * formados a partir da combina��o de indicadores verticais (ANCHOR_TOP, ANCHOR_VCENTER, ANCHOR_BOTTOM) e horizontais
	 * (ANCHOR_LEFT, ANCHOR_HCENTER, ANCHOR_RIGHT). Passar o valor 0 (zero) resulta na utiliza��o da �ncora TOP-LEFT.
	 * 
	 * @see #defineReferencePixel(Point)
	 * @see #defineReferencePixel(int,int)
	 * @see #setRefPixelPosition(int,int)
	 * @see #setRefPixelPosition(Point)
	 * @see #ANCHOR_TOP
	 * @see #ANCHOR_LEFT
	 * @see #ANCHOR_BOTTOM
	 * @see #ANCHOR_RIGHT
	 * @see #ANCHOR_VCENTER
	 * @see #ANCHOR_HCENTER
	 */
	public final void defineReferencePixel( int anchor ) {
		final Point anchorPoint = new Point();
		
		switch ( anchor & ANCHOR_VERTICAL_MASK ) {
			case ANCHOR_VCENTER:
				anchorPoint.y = size.y >> 1;
			break;
			
			case ANCHOR_BOTTOM:
				anchorPoint.y = size.y;
			break;
		}
		
		switch ( anchor & ANCHOR_HORIZONTAL_MASK ) {
			case ANCHOR_HCENTER:
				anchorPoint.x = size.x >> 1;
			break;
			
			case ANCHOR_RIGHT:
				anchorPoint.x = size.x;
			break;			
		}
		
		defineReferencePixel( anchorPoint );
	} // fim do m�todo defineReferencePixel( int )
	 
    
    /**
     * Define a posi��o do ponto de refer�ncia a partir de um Point. M�todo equivalente � chamada de 
	 * <b>setRefPixelPosition( refPosition.x, refPosition.y )</b>.
	 * 
     * @param refPosition posi��o (x,y) do ponto de refer�ncia.
	 * @see #setRefPixelPosition(int,int)
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(Point)
     */
	public final void setRefPixelPosition( Point refPosition ) {
        setRefPixelPosition( refPosition.x, refPosition.y );
	}
	
    
    /**
     * Define a posi��o do ponto do refer�ncia.
	 * 
     * @param x posi��o x do ponto de refer�ncia.
     * @param y posi��o y do ponto de refer�ncia.
	 * @see #setRefPixelPosition(Point)
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(Point)
     */
	public void setRefPixelPosition( int x, int y ) {
        position.set( x - referencePixel.x, y - referencePixel.y );        
	}
	
    
    /**
     * Obt�m a posi��o do ponto de refer�ncia.
     * @return 
     */
	public Point getRefPixelPosition() {
		return new Point( position.x + referencePixel.x, position.y + referencePixel.y );
	}
	
    
    /**
     * Obt�m a posi��o x do ponto de refer�ncia.
     * @return 
     */
	public int getRefPixelX() {
		return position.x + referencePixel.x;
	}
	 
    
    /**
     * Obt�m a posi��o y do ponto de refer�ncia.
     * @return 
     */
	public int getRefPixelY() {
		return position.y + referencePixel.y;
	}
	 
    
	/**
	 * Verifica se um ponto est� dentro da �rea do drawable.
	 * 
	 * @param p ponto cuja interse��o com a �rea do drawable ser� testada.
	 * @return boolean indicando se o ponto est� dentro dos limites do drawable.
	 * @throws NullPointerException caso p seja nulo.
	 */
	public final boolean contains( Point p ) {
		return contains( p.x, p.y );
	}
	
	
	/**
	 * Verifica se um ponto est� dentro da �rea do drawable.
	 * 
	 * @param x posi��o x do ponto cuja interse��o ser� verificada.
	 * @param y posi��o y do ponto cuja interse��o ser� verificada.
	 * @return boolean indicando se o ponto est� dentro dos limites do drawable.
	 * @throws NullPointerException caso p seja nulo.
	 */	
	public boolean contains( int x, int y ) {
		return x >= position.x && y >= position.y && x <= ( position.x + size.x ) && y <= ( position.y + size.y );
	}
	
	
	/**
	 * Obt�m o drawable presente no ponto <code>p</code>. Caso o drawable seja um grupo, � feita uma varredura interna para
	 * que seja retornado o drawable mais � frente cuja �rea englobe o ponto <code>p</code>.
	 * @param p ponto cuja interse��o com o drawable ser� verificada.
	 * @return refer�ncia para o drawable presente na posi��o (x,y), ou <code>null</code> caso o ponto n�o esteja
	 * dentro da �rea de colis�o do drawable.
	 */
	public final Drawable getDrawableAt( Point p ) {
		return getDrawableAt( p.x, p.y );
	}
	
	
	/**
	 * Obt�m o drawable presente na posi��o (x,y). Caso o drawable seja um grupo, � feita uma varredura interna para
	 * que seja retornado o drawable mais � frente cuja �rea englobe o ponto (x,y).
	 * 
	 * @param x posi��o x do ponteiro.
	 * @param y posi��o y do ponteiro.
	 * @return refer�ncia para o drawable presente na posi��o (x,y), ou <code>null</code> caso o ponto n�o esteja
	 * dentro da �rea de colis�o do drawable.
	 */
	public Drawable getDrawableAt( int x, int y ) {
		return contains( x, y ) ? this : null;
	}
    
	 
    /**
     * Define a transforma��o atual do drawable, realizando opera��es de rota��o e/ou espelhamento, conforme necess�rio.
	 * Caso a transforma��o envolva rota��o E espelhamento, primeiro � feito o espelhamento, e ent�o a rota��o.
     * @param transform 
     * @return 
	 * @see #rotate(int)
	 * @see #mirror(int)
     */
	public boolean setTransform( int transform ) {
		final int mirrorState = getMirror();
		if ( ( mirrorState & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) )
			mirror( TRANS_MIRROR_H );

		if ( ( mirrorState & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) )
			mirror( TRANS_MIRROR_V );
		
		final int newRotation = transform & TRANS_MASK_ROTATE;
		if ( getRotation() != newRotation ) {
			switch ( getRotation() ) {
				case TRANS_NONE:
					rotate( newRotation );
				break;
				
				case TRANS_ROT90:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT270 );
						break;
						
						case TRANS_ROT180:
							rotate( TRANS_ROT90 );
						break;

						case TRANS_ROT270:
							rotate( TRANS_ROT180 );
						break;
					} // fim switch ( newRotation )
				break;

				case TRANS_ROT180:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT180 );
						break;
						
						case TRANS_ROT90:
							rotate( TRANS_ROT270 );
						break;

						case TRANS_ROT270:
							rotate( TRANS_ROT90 );
						break;
					} // fim switch ( newRotation )
				break;

				case TRANS_ROT270:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT90 );
						break;
						
						case TRANS_ROT90:
							rotate( TRANS_ROT180 );
						break;

						case TRANS_ROT180:
							rotate( TRANS_ROT270 );
						break;
					} // fim switch ( newRotation )
				break;
			} // fim switch ( getRotation() )
		} // fim if ( getRotation() != newRotation )

		return false;
	}
	 
    
    /**
     * Obt�m a transforma��o acumulada.
	 * 
     * @return inteiro indicando a transforma��o acumulada.
     */
	public int getTransform() {
		return transform;
	}
	
	
	/**
	 * Espelha o drawable em torno de um de seus eixos.
	 * 
	 * @param mirrorType tipo do espelhamento. Os valores v�lidos s�o:
	 * <ul>
	 * <li>TRANS_MIRROR_H: espelha em torno do eixo vertical, ou seja, esquerda e direita s�o invertidos.</li>
	 * <li>TRANS_MIRROR_V: espelha em torno do eixo horizontal, ou seja, cima e baixo s�o invertidos.</li>
	 * <li>TRANS_MIRROR_H | TRANS_MIRROR_V: espelha em ambos os eixos (opera��o equivalente a uma rota��o de 180�)</li>
	 * </ul>
	 * @return boolean indicando se a opera��o � suportada pelo drawable.
	 * @see #setTransform(int)
	 */
	public boolean mirror( int mirrorType ) {
		mirrorType &= TRANS_MASK_MIRROR;
		final int newMirrorStatus = ( transform & TRANS_MASK_MIRROR ) ^ mirrorType;
		
		// se houver mudan�a no espelhamento horizontal, atualiza o pixel de refer�ncia
		if ( ( newMirrorStatus & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) )
			defineReferencePixel( size.x - referencePixel.x, referencePixel.y );
		
		// se houver mudan�a no espelhamento vertical, atualiza o pixel de refer�ncia
		if ( ( newMirrorStatus & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) )
			defineReferencePixel( referencePixel.x, size.y - referencePixel.y );						

		// a transforma��o passa a ser a uni�o da rota��o acumulada com o novo estado de espelhamento
		transform = ( transform & TRANS_MASK_ROTATE ) | newMirrorStatus;
		updateTransform();
		
		return false;
	} // fim do m�todo mirror( int )
	
	
	/**
	 * Rotaciona o drawable, utilizando como piv� da rota��o o seu pixel de refer�ncia.
	 * @param rotationType �ngulo da rota��o. Valores v�lidos:
	 * <ul>
	 * <li>TRANS_ROT90: rotaciona 90� no sentido hor�rio (270� no sentido anti-hor�rio).</li>
	 * <li>TRANS_ROT180: rotaciona o drawable 180�.</li>
	 * <li>TRANS_ROT270: rotaciona 270� no sentido hor�rio (90� no sentido anti-hor�rio).</li>
	 * </ul>
	 * @return boolean indicando se a opera��o de rota��o � suportada pelo drawable.
	 * @see #setTransform(int)
	 */
	public boolean rotate( int rotationType ) {
		rotationType &= TRANS_MASK_ROTATE;
		
		final Point previousRef = new Point( getRefPixelPosition() );
		
		switch ( rotationType ) {
			case TRANS_ROT90:
				defineReferencePixel( size.y - referencePixel.y, referencePixel.x );
				setSize( size.y, size.x );
			break;
			
			case TRANS_ROT180:
				defineReferencePixel( size.x - referencePixel.x, size.y - referencePixel.y );				
			break;
			
			case TRANS_ROT270:
				defineReferencePixel( referencePixel.y, size.x - referencePixel.x );				
				setSize( size.y, size.x );
			break;
			
			default:
				return true;
		} // fim switch ( rotationType )
		
		// TODO atualizar �rea de colis�o ap�s rota��o de 90� ou 270�
		int newRotation = transform & TRANS_MASK_ROTATE;		
		
		switch ( newRotation ) {
			case TRANS_NONE:
				newRotation = rotationType;
			break;
			
			case TRANS_ROT90:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_ROT180;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_ROT270;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_NONE;
					break;
				} // fim switch ( rotationType )
			break;

			case TRANS_ROT180:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_ROT270;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_NONE;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_ROT90;
					break;
				} // fim switch ( rotationType )				
			break;

			case TRANS_ROT270:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_NONE;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_ROT90;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_ROT180;
					break;
				} // fim switch ( rotationType )				
			break;
			
			default:
				return false;
		} // fim switch ( newRotation )
		
		setPosition( previousRef.x - referencePixel.x, previousRef.y - referencePixel.y );
		
		transform = ( transform & TRANS_MASK_MIRROR ) | newRotation;
		updateTransform();
		
		return false;
	} // fim do m�todo rotate( int )
	
	
	/**
	 * Obt�m o espelhamento atual.
	 * 
	 * @return int indicando o espelhamento.
	 * @see #mirror(int)
	 */
	public int getMirror() {
		return transform & TRANS_MASK_MIRROR;
	}
	
	
	/**
	 * Obt�m a rota��o atual.
	 * 
	 * @return int indicando a rota��o atual.
	 * @see #rotate(int)
	 */
	public int getRotation() {
		return transform & TRANS_MASK_ROTATE;
	}
	
	
	protected final void updateTransform() {
		switch ( transform ) {
			case TRANS_NONE:
			case TRANS_ROT180 | TRANS_MIRROR_H | TRANS_MIRROR_V:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_NONE;
			break;
			
			case TRANS_MIRROR_H:
			case TRANS_MIRROR_V | TRANS_ROT180:	
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR;
			break;
			
			case TRANS_MIRROR_V:
			case TRANS_MIRROR_H | TRANS_ROT180:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT180;
			break;			
			
			case TRANS_ROT180:
			case TRANS_MIRROR_H | TRANS_MIRROR_V:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_ROT180;
			break;			

			case TRANS_ROT90:
			case TRANS_MIRROR_H | TRANS_MIRROR_V | TRANS_ROT270:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_ROT90;
			break;			
			
			case TRANS_ROT270:
			case TRANS_MIRROR_H | TRANS_MIRROR_V | TRANS_ROT90:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_ROT270;
			break;			
			
			case TRANS_MIRROR_H | TRANS_ROT270:
			case TRANS_MIRROR_V | TRANS_ROT90:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT90;
			break;			

			case TRANS_MIRROR_H | TRANS_ROT90:			
			case TRANS_MIRROR_V | TRANS_ROT270:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT270;
			break;			
		}
	} // fim do m�todo updateTransform()

	
	/**
	 * Obt�m a �rea de clip definida atual.
	 * @return refer�ncia para o topo da pilha de clip, ou null, caso a pilha esteja vazia.
	 * @see #pushClip(Graphics)
	 * @see #popClip(Graphics)
	 */
	public static final Rectangle getClip() {
		if ( currentStackSize > 0 )
			return clipStack[ currentStackSize - 1 ];
		
		return null;
	} // fim do m�todo getClip()

	
}
 
