/*
 * ScreenManager.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import java.util.Hashtable;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;


/**
 *
 * @author peter
 */
public final class ScreenManager extends GameCanvas implements Runnable {
	
	private boolean paused;
	
	private boolean running;
	 
	private Thread thread;
	 
	/** tempo em milisegundos decorridos no �ltimo frame */
	private int lastFrameTime;	
 
	/** Se a tela atual for atualiz�vel, armazena uma refer�ncia j� com o typecast, para acelerar futuros acessos. */
	private Updatable currentScreenUpdatable;
	 
	/** Listener dos eventos de teclas (pode ser null). */
	private static KeyListener keyListener;
	
	/** Listener dos eventos de ponteiro (caneta stylus) (pode ser null). */
	private static PointerListener pointerListener;
	
	/** objeto que recebe os eventos showNotify/hideNotify (pode ser null) */
	private static ScreenListener screenListener;
	 
	/** cor padr�o de preenchimento do background */
	public static final int DEFAULT_BG_COLOR = 0x000000;
	
	/** cor de preenchimento do background */
	private int bgColor = DEFAULT_BG_COLOR;
	
	/** Drawable de fundo (caso seja nulo, o fundo � preenchido com a cor de fundo definida) */
	private Drawable background;
	/** refer�ncia para o drawable de fundo, caso ele seja atualiz�vel */
	private Updatable updatableBackground;
	
	/** Refer�ncia para a tela atual. */
	private Drawable currentScreen;
	
	// "labels" dos softkeys (drawables que indicam as a��es dos softkeys esquerdo, central e direito)
	private final Drawable[] softKeys = new Drawable[ 3 ];
	private final Updatable[] softKeysUpdatables = new Updatable[ 3 ];
	
	// �ndices dos softkeys
	/** �ndice do soft key esquerdo (valor utilizado para definir o drawable do soft key, n�o o �ndice da tecla). */
	public static final byte SOFT_KEY_LEFT	= 0;
	/** �ndice do soft key central (valor utilizado para definir o drawable do soft key, n�o o �ndice da tecla). */
	public static final byte SOFT_KEY_MID	= 1;
	/** �ndice do soft key direito (valor utilizado para definir o drawable do soft key, n�o o �ndice da tecla). */
	public static final byte SOFT_KEY_RIGHT	= 2;
	 
	/** Tempo m�ximo de um frame. Caso o intervalo de atualiza��o seja maior, ele � reduzido a esse valor, impedindo
	 * que intervalos muito grandes de atualiza��o causem inconsist�ncia (como n�o detec��o de colis�o entre objetos
	 * cujas trajet�rias se cruzaram nesse intervalo, por exemplo).
	 */
	private final int MAX_FRAME_TIME;
	 
	// teclas especiais
	public static final int KEY_SOFT_LEFT		= 0xff0001;
	public static final int KEY_SOFT_RIGHT		= 0xff0002;
	public static final int KEY_CLEAR			= 0xff0003;
	public static final int KEY_SOFT_MID		= 0xff0004;
	public static final int KEY_BACK			= 0xff0005;
	
	/** Hashtable que cont�m o mapeamento de c�digos de teclas para suas fun��es, como soft keys, apagar, voltar, etc. */
	private static final Hashtable specialKeysTable = new Hashtable();
	
	
	//<editor-fold defaultstate="collapsed" desc="CONSTANTES DAS TECLAS DE CADA FABRICANTE">
	// TODO adicionar c�digos de teclas dos BlackBerry
	
	// softkey esquerdo (todos os aparelhos possuem)
	private static final byte KEY_SOFT_LEFT_GENERIC			= -6;
	private static final byte KEY_SOFT_LEFT_LG				= KEY_SOFT_LEFT_GENERIC;
	private static final short KEY_SOFT_LEFT_LG_2			= -202;
	private static final byte KEY_SOFT_LEFT_NOKIA			= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_MOTOROLA		= -21;
	private static final byte KEY_SOFT_LEFT_MOTOROLA_2		= 21;
	private static final byte KEY_SOFT_LEFT_SAMSUNG			= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_SONYERICSSON	= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_SIEMENS			= -1;
	private static final byte KEY_SOFT_LEFT_SIEMENS_2		= 105;
	private static final byte KEY_SOFT_LEFT_SAGEM_GRADIENTE	= -7; // igual � soft key direita dos SonyEricsson
	private static final int  KEY_SOFT_LEFT_INTENT_JTE		= 57345;
	private static final int  KEY_SOFT_LEFT_INTENT_JTE_2	= -KEY_SOFT_LEFT_INTENT_JTE;
	private static final byte KEY_SOFT_LEFT_INTELBRAS		= KEY_SOFT_LEFT_GENERIC;

	// softkey direito (todos os aparelhos possuem)
	private static final byte KEY_SOFT_RIGHT_GENERIC		= -7;
	private static final byte KEY_SOFT_RIGHT_LG				= KEY_SOFT_RIGHT_GENERIC;
	private static final short KEY_SOFT_RIGHT_LG_2			= -203;
	private static final byte KEY_SOFT_RIGHT_NOKIA			= KEY_SOFT_RIGHT_GENERIC;
	private static final byte KEY_SOFT_RIGHT_MOTOROLA		= -22;
	private static final byte KEY_SOFT_RIGHT_MOTOROLA_2		= 22;
	private static final byte KEY_SOFT_RIGHT_SAMSUNG		= KEY_SOFT_RIGHT_GENERIC;
	private static final byte KEY_SOFT_RIGHT_SONYERICSSON	= KEY_SOFT_RIGHT_GENERIC;
	private static final byte KEY_SOFT_RIGHT_SIEMENS		= -4;
	private static final byte KEY_SOFT_RIGHT_SIEMENS_2		= 106;
	private static final byte KEY_SOFT_RIGHT_SAGEM_GRADIENTE= KEY_SOFT_LEFT_SONYERICSSON; // igual � soft key esquerda dos SonyEricsson
	private static final int  KEY_SOFT_RIGHT_INTENT_JTE		= 57346;
	private static final int  KEY_SOFT_RIGHT_INTENT_JTE_2	= -KEY_SOFT_RIGHT_INTENT_JTE;
	private static final byte KEY_SOFT_RIGHT_INTELBRAS		= KEY_SOFT_RIGHT_GENERIC;
	
	// softkey central (somente alguns aparelhos possuem)
	private static final byte KEY_SOFT_MID_MOTOROLA			= -23;
	private static final byte KEY_SOFT_MID_MOTOROLA_2		= 23;
	
	// tecla de clear (somente alguns aparelhos possuem)
	private static final byte KEY_CLEAR_GENERIC				= -8;
	private static final byte KEY_CLEAR_LG					= -16;
	private static final short KEY_CLEAR_LG_2				= -204;
	private static final byte KEY_CLEAR_LG_3				= -11; // TODO verificar tecla CLEAR no ME970 (Shine)
	private static final byte KEY_CLEAR_NOKIA				= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_MOTOROLA			= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_SAMSUNG				= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_SONYERICSSON		= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_SIEMENS				= -12;
	private static final short KEY_CLEAR_SIEMENS_2			= -12345;
	private static final byte KEY_CLEAR_SAGEM				= KEY_CLEAR_SONYERICSSON;
	private static final byte KEY_CLEAR_INTENT_JTE			= KEY_CLEAR_GENERIC;
	
	// tecla espec�fica para voltar (somente alguns aparelhos possuem)
	private static final byte KEY_BACK_SONYERICSSON			= -11;
	private static final byte KEY_BACK_SAGEM				= -KEY_BACK_SONYERICSSON;
	private static final byte KEY_BACK_INTENT_JTE			= 27;
	private static final byte KEY_BACK_BLACKBERRY			= 27; // TODO confirmar: n�o envia evento keyPressed (somente keyReleased)?
	
	// outras teclas
	private static final byte KEY_FIRE_BLACKBERRY			= 8;
	private static final byte KEY_ENTER_BLACKBERRY			= 10;
	
	//</editor-fold>
	
	/** Inst�ncia do gerenciador de telas. */
	private static ScreenManager instance;
	
	// dimens�es da tela. Os valores referentes � tela cheia s� s�o preenchidos no momento da primeira visualiza��o da 
	// tela, ou seja, ap�s a primeira chamada ao m�todo paint().
	/** Largura da tela. */
	public static int SCREEN_WIDTH;
	/** Posi��o horizontal central da tela. */
	public static int SCREEN_HALF_WIDTH;
	/** Altura da tela. */
	public static int SCREEN_HEIGHT;
	/** Posi��o vertical central da tela. */
	public static int SCREEN_HALF_HEIGHT;
	
	/** �rea da tela efetivamente vis�vel. */
	public static final Rectangle viewport = new Rectangle();
	
	// vari�veis utilizadas somente no modo SHOW_FREE_MEM, que mostra na tela a mem�ria heap livre
	//#if SHOW_FREE_MEM == "true"
//# 	// intervalo de atualiza��o da mem�ria livre em milisegundos
//# 	private static final short SHOW_FREE_MEM_UPDATE_INTERVAL = 150;
//# 	// tempo restante at� a pr�xima atualiza��o da mem�ria livre
//# 	private int timeToNextUpdate;
//# 	// mem�ria livre
//# 	private String freeMem = "";
	//#endif
	
	
	// vari�veis utilizadas somente no modo SHOW_FPS, que mostra a taxa de quadros por segundo
	//#if SHOW_FPS == "true"
//# 	private static final int FP_FPS_UPDATE_INTERVAL = NanoMath.ONE;
//# 	
//# 	private int fp_accFpsTime;
//# 	
//# 	private short frameCounter;
//# 	
//# 	private String strFps = "";
	//#endif
	
	
	/** 
	 * Vari�vel utilizada para fazer o controle de teclas pressionadas, evitando-se que os m�todos keyPressed e 
	 * keyReleased sejam chamados concorrentemente.
	 */
	private boolean usingKeys;
	
	/** 
	 * Indica se os eventos de teclas repassados ao KeyListener (caso haja um) j� receber�o o valor da tecla pressionada
	 * considerando-se as fun��es especiais (soft keys, BACK, CLEAR, FIRE, RIGHT, etc)
	 */
	private boolean handleSpecialKey = true;
	
	/** Back buffer de desenho da tela. */
	private final Graphics screenBuffer;
	

	/**
	 * Cria uma nova inst�ncia do gerenciador de telas. Ele somente deve ser definido como a tela ativa do aparelho
	 * posteriormente, ao iniciar sua thread. Defini-lo como tela ativa no construtor pode causar problemas em alguns
	 * aparelhos, como o Gradiente GF690, que eventualmente exibia somente uma tela preta ao iniciar a aplica��o.
	 * 
	 * @param maxFrameTime
	 */
	protected ScreenManager( int maxFrameTime ) {
		super( false );
		
		// essa chamada � necess�ria aqui para garantir que o buffer utilize a tela cheia (sem a chamada, Nokia S60 3rd Edition
		// utilizam tela parcial, por exemplo)
		setFullScreenMode( true );
		
		MAX_FRAME_TIME = maxFrameTime;
		
		screenBuffer = getGraphics();
	}
	

	/**
	 * Cria uma nova inst�ncia do gerenciador de telas.
	 * 
	 * @param maxFrameTime
	 * @return
	 */
	public static final ScreenManager createInstance( int maxFrameTime ) {
		instance = new ScreenManager( maxFrameTime );
		
		// obt�m as dimens�es da tela - nesse ponto, os valores podem n�o referir-se � tela cheia; para se obter os valores
		// referentes � tela cheia, somente ap�s a primeira chamada ao m�todo paint().
		instance.setFullScreenMode( true );
		
		instance.sizeChanged( instance.screenBuffer.getClipWidth(), instance.screenBuffer.getClipHeight() );
		
		// TODO teste -> chamando sizeChanged() em createInstance, para centralizar testes e atribui��es
//		SCREEN_WIDTH = instance.screenBuffer.getClipWidth();
//		SCREEN_HALF_WIDTH = SCREEN_WIDTH >> 1;
//		SCREEN_HEIGHT = instance.screenBuffer.getClipHeight();
//		SCREEN_HALF_HEIGHT = SCREEN_HEIGHT >> 1;
//		
//		viewport.set( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
		
		// adiciona o 0 para evitar quebra de alguns aparelhos caso seja chamado getGameAction( 0 ), dentro de getSpecialKey(int)
		specialKeysTable.put( new Integer( 0 ), new Integer( 0 ) );

		// adiciona as teclas de 0 a 9, para evitar que sejam capturadas por getGameAction(), dentro de getSpecialKey(),
		// em especial evitando que sejam associadas aos valores GAME_A, GAME_B, GAME_C e GAME_D (como ocorre nos
		// aparelhos Motorola, por exemplo)
		for ( int i = KEY_NUM0; i <= KEY_NUM9; ++i ) // TODO necess�rio??
			specialKeysTable.put( new Integer( i ), new Integer( i ) );

		// adiciona tamb�m os soft keys, para que n�o haja erro no repasse de eventos em pointerPressed (caso contr�rio,
		// o valor repassado nao seria reconhecido como o de um soft key)
		specialKeysTable.put( new Integer( KEY_SOFT_LEFT ), new Integer( KEY_SOFT_LEFT ) );
		specialKeysTable.put( new Integer( KEY_SOFT_MID ), new Integer( KEY_SOFT_MID ) );
		specialKeysTable.put( new Integer( KEY_SOFT_RIGHT ), new Integer( KEY_SOFT_RIGHT ) );
		
		specialKeysTable.put( new Integer( KEY_STAR ), new Integer( KEY_STAR ) );
		specialKeysTable.put( new Integer( KEY_POUND ), new Integer( KEY_POUND ) );

		// preenche a tabela com os �ndices espec�ficos de cada fabricante
		switch ( AppMIDlet.getVendor() ) {
			case AppMIDlet.VENDOR_LG:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_LG ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_LG_2 ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_LG ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_LG_2 ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_LG ), new Integer( KEY_CLEAR ) );	
				specialKeysTable.put( new Integer( KEY_CLEAR_LG_2 ), new Integer( KEY_CLEAR ) );	
				specialKeysTable.put( new Integer( KEY_CLEAR_LG_3 ), new Integer( KEY_CLEAR ) );	
			break;

			case AppMIDlet.VENDOR_NOKIA:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_NOKIA ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_NOKIA ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_NOKIA ), new Integer( KEY_CLEAR ) );					
			break;

			case AppMIDlet.VENDOR_MOTOROLA:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_MOTOROLA ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_MOTOROLA_2 ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_MID_MOTOROLA ), new Integer( KEY_SOFT_MID ) );
				specialKeysTable.put( new Integer( KEY_SOFT_MID_MOTOROLA_2 ), new Integer( KEY_SOFT_MID ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_MOTOROLA ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_MOTOROLA_2 ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_MOTOROLA ), new Integer( KEY_CLEAR ) );
			break;

			case AppMIDlet.VENDOR_SIEMENS:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SIEMENS ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SIEMENS_2 ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SIEMENS ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SIEMENS_2 ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_SIEMENS ), new Integer( KEY_CLEAR ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_SIEMENS_2 ), new Integer( KEY_CLEAR ) );
			break;

			case AppMIDlet.VENDOR_SAMSUNG:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SAMSUNG ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SAMSUNG ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_SAMSUNG ), new Integer( KEY_CLEAR ) );					
			break;

			case AppMIDlet.VENDOR_SONYERICSSON:
				/**
				 * Teclas ignoradas do SonyEricsson. Algumas das fun��es destas teclas s�o: controle de volume, browser, 
				 * walkman, c�mera, etc. Estas teclas s�o ignoradas pois podem causar congelamento em alguns aparelhos, 
				 * uma vez que algumas n�o enviam eventos keyReleased ou ent�o causam problemas em getSpecialKey. 
				 * Exemplos: a tecla de browser do K700 e de volume no W600.
				 */
				final byte[] KEY_IGNORED_SONYERICSSON = { -10, -12, -13, -14, -20, -21, -22, -23, -24, -25, -26, -27, -28, -34, -35, -36, -37 };				
				for ( byte i = 0; i < KEY_IGNORED_SONYERICSSON.length; ++i ) {
					specialKeysTable.put( new Integer( KEY_IGNORED_SONYERICSSON[ i ] ), new Integer( 0 ) );
				}

				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SONYERICSSON ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SONYERICSSON ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_SONYERICSSON ), new Integer( KEY_CLEAR ) );
				specialKeysTable.put( new Integer( KEY_BACK_SONYERICSSON ), new Integer( KEY_BACK ) );
			break;			

			case AppMIDlet.VENDOR_HTC:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_INTENT_JTE ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_INTENT_JTE_2 ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_INTENT_JTE ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_INTENT_JTE_2 ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_INTENT_JTE ), new Integer( KEY_CLEAR ) );
				specialKeysTable.put( new Integer( KEY_BACK_INTENT_JTE ), new Integer( KEY_BACK ) );
			break;

			case AppMIDlet.VENDOR_BLACKBERRY:
				specialKeysTable.put( new Integer( KEY_BACK_BLACKBERRY ), new Integer( KEY_BACK ) );
				specialKeysTable.put( new Integer( KEY_FIRE_BLACKBERRY ), new Integer( FIRE ) );
				specialKeysTable.put( new Integer( KEY_ENTER_BLACKBERRY ), new Integer( FIRE ) );
			break;

			case AppMIDlet.VENDOR_SAGEM_GRADIENTE:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_SAGEM_GRADIENTE ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_SAGEM_GRADIENTE ), new Integer( KEY_SOFT_RIGHT ) );					
			break;
			
			case AppMIDlet.VENDOR_INTELBRAS:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_INTELBRAS ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_INTELBRAS ), new Integer( KEY_SOFT_RIGHT ) );
			break;

			default:
				specialKeysTable.put( new Integer( KEY_SOFT_LEFT_GENERIC ), new Integer( KEY_SOFT_LEFT ) );
				specialKeysTable.put( new Integer( KEY_SOFT_RIGHT_GENERIC ), new Integer( KEY_SOFT_RIGHT ) );
				specialKeysTable.put( new Integer( KEY_CLEAR_GENERIC ), new Integer( KEY_CLEAR ) );
		} // fim switch ( AppMIDlet.getVendor() )
		
		return instance;
	} // fim do m�todo createInstance( boolean, int )
	 
	
	/**
	 * Define o Drawable a ser utilizado como indicador de um softkey.
	 * @param softKey �ndice do softkey a ser definido. Valores v�lidos: KEY_SOFT_LEFT, KEY_SOFT_MID e KEY_SOFT_RIGHT.
	 * @param d Drawable a ser utilizado como indicador do softkey. Caso seja nulo, o softkey anterior (caso exista) �
	 * removido.
	 */
	public final void setSoftKey( int softKey, Drawable d ) {
		try {
			softKeys[ softKey ] = d;
			if ( d instanceof Updatable )
				softKeysUpdatables[ softKey ] = ( Updatable ) d;
			else
				softKeysUpdatables[ softKey ] = null;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return;
		}
		
		if ( d != null ) {
			// posiciona o softKey
			final Point size = d.getSize();

			switch ( softKey ) {
				case SOFT_KEY_LEFT:
					d.defineReferencePixel( 0, size.y );
					d.setRefPixelPosition( 0, SCREEN_HEIGHT );
				break;
				case SOFT_KEY_MID:
					d.defineReferencePixel( size.x >> 1, size.y );
					d.setRefPixelPosition( SCREEN_HALF_WIDTH, SCREEN_HEIGHT );
				break;
				case SOFT_KEY_RIGHT:
					d.defineReferencePixel( size.x, size.y );
					d.setRefPixelPosition( SCREEN_WIDTH, SCREEN_HEIGHT );
				break;
			} // fim switch ( softKey )
		} // fim if ( d != null )		
	} // fim do m�todo setSoftKey( int, Drawable )
	 
	
	/**
	 * Troca a tela atual, podendo realizar uma anima��o de transi��o.
	 * @param screen nova tela a ser exibida. M�todos ser�o repassados a essa nova tela caso ela implemente pelo menos uma
	 * das seguintes interfaces:
	 * <ul>
	 * <li>KeyListener</li>
	 * <li>Updatable</li>
	 * <li>ScreenListener</li>
	 * </ul>
	 *
	 * @see KeyListener
	 * @see Updatable
	 */
	public final void setCurrentScreen( Drawable screen ) {
		currentScreen = screen;
		System.gc();
		
		// registra a tela atual como listener de atualiza��o, teclas, ponteiro e/ou eventos de suspend
		if ( currentScreen instanceof Updatable )
			currentScreenUpdatable = ( Updatable ) currentScreen;
		else
			currentScreenUpdatable = null;

		if ( currentScreen instanceof KeyListener )
			keyListener = ( KeyListener ) currentScreen;
		else
			keyListener = null;

		if ( currentScreen instanceof PointerListener )
			pointerListener = ( PointerListener ) currentScreen;
		else
			pointerListener = null;

		if ( currentScreen instanceof ScreenListener )
			screenListener = ( ScreenListener ) currentScreen;		
		else
			screenListener = null;
	} // fim do m�todo setCurrentScreen( Drawable, byte )
	 
	
	/**
	 * Define a cor de preenchimento do fundo, utilizada quando n�o h� Drawable definido como background.
	 * @param bgColor cor s�lida usada para preenchimento do background. Valores negativos indicam para n�o preencher 
	 * o fundo da tela com uma cor s�lida, mesmo que n�o haja drawable definido como background.
	 */
	public final void setBackgroundColor( int bgColor ) {
		this.bgColor = bgColor;
	}
	
	
	/**
	 * Define o comportamento dos eventos keyPressed(int) e keyReleased(int), repassados ao KeyListener corrente (caso
	 * haja um definido).
	 * <p>O valor padr�o do ScreenManager � <i>true</i>, e normalmente s� � necess�rio utilizar <i>false</i>
	 * caso haja necessidade de tratar separadamente a tecla e sua fun��o, como <i>KEY_NUM4</i> e <i>LEFT</i> e 
	 * <i>KEY_NUM5</i> e <i>FIRE</i>, por exemplo.</p>
	 *
	 * @param handleSpecialKey caso seja <i>true</i>, os eventos keyPressed e keyReleased j� verificam a exist�ncia de
	 * fun��es especiais ligadas � tecla, ou seja, n�o � necess�rio que o KeyListener chame novamente ScreenManager.getSpecialKey(key)
	 * para detectar a fun��o da tecla pressionada. Caso seja <i>false</i>, � repassado o mesmo c�digo de tecla recebido
	 * por ScreenManager.
	 * 
	 * @see #keyPressed(int)
	 * @see #keyReleased(int)
	 */
	public static final void setKeyListenerBehavior( boolean handleSpecialKey ) {
		instance.handleSpecialKey = handleSpecialKey;
	}
	 
	
	/**
	 * Obt�m a tecla especial associada a um c�digo de tecla pressionada.
	 * @param keyCode �ndice da tecla pressionada.
	 * @return tipo de tecla especial associada, ou 0 (zero) caso n�o haja fun��o especial associada a esse c�digo.
	 */
	public static final int getSpecialKey( int keyCode ) {
		// TODO implementar hashtable pr�pria, pois hashtable do java realiza sincroniza��o (lento)
		final Integer value = ( Integer ) specialKeysTable.get( new Integer( keyCode ) );

		if ( value != null )
			return value.intValue();

		// n�o foi encontrada tecla especial (soft keys, back, etc) associada � tecla. Retorna a a��o de jogo associada 
		// � tecla, se houver uma. Sen�o, retorna o c�digo da pr�pria tecla. O bloco try/catch � necess�rio pois evita
		// problemas com teclas especiais em alguns aparelhos (como as teclas de Web e SMS no Motorola)
		try {
			final int ret = instance.getGameAction( keyCode );
			return ret == 0 ? keyCode : ret;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return 0;
		}
	} // fim do m�todo getSpecialKey( int )
	

	/**
	 * Evento enviado pelo aparelho, informando que o gerenciador de telas passou a ser a tela exibida pelo aparelho.
	 */
	protected synchronized final void showNotify() {
		//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif
		
		paused = false;
		notifyAll();
			
		start();
		
		if ( screenListener != null )
			screenListener.showNotify();
		
		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif				
	}

	
	/**
	 * Evento enviado pelo aparelho, informando que a tela ativa (no caso, o gerenciador de telas) n�o � mais a
	 * tela sendo exibida.
	 */
	protected synchronized final void hideNotify() {
	 	//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif
		
		paused = true;

		// � utilizado o bloco try/catch aqui pois pode ocorrer de, ao encerrar a aplica��o, hideNotify ser chamado ap�s
		// destroyApp, o que faz com que refer�ncias a AppMIDlet.instance causem NullPointerException
		try {
			keyReleased( 0 );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
		
		// p�ra de tocar um som, caso necess�rio. A exclus�o m�tua n�o � utilizada aqui para evitar atrasos na resposta da
		// chamada caso o m�todo MediaPlayer.play() esteja na sua regi�o cr�tica.
		MediaPlayer.stop( false );
		
		if ( screenListener != null )
			screenListener.hideNotify();
		
		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif		
	}

	
	/**
	 * Callback chamada pelo aparelho. Aqui s�o desenhados o fundo de tela, tela ativa, tela anterior (caso esteja em 
	 * transi��o) e soft keys, caso estejam definidos.
	 * 
	 * @param g refer�ncia para o Graphics onde as opera��es de desenho ser�o realizadas.
	 */
	public final void paint( Graphics g ) {
		// a sincroniza��o deste m�todo com as teclas � necess�ria para evitar erros de desenho em v�rios aparelhos, notadamente
		// Siemens, Samsung e LG (drawables podem ser desenhados com offset incorreto por 1 frame durante pressionamento de teclas)
 		lockKeys();
		
		//#if USE_FLUSH_GRAPHICS == "true"
//# 		g = screenBuffer;
		//#endif
		
	 	//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif

		// Zera a transla��o acumulada, para evitar que eventuais "lixos" na deixados na �ltima chamada de paint causem 
		// erro de posicionamento nas futuras chamadas de desenho.		
		Drawable.translate.set( 0, 0 );
		
		setFullScreenMode( true );

		g.setClip( viewport.x, viewport.y, viewport.width, viewport.height );

		if ( background == null ) {
			if ( bgColor >= 0 ) {
				g.setColor( bgColor );
				g.fillRect( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
			}
		} else {
			background.draw( g );
		}

		if ( currentScreen != null )
			currentScreen.draw( g );		
		
		// desenha os indicadores de softkeys (caso existam)
		for ( int i = 0; i < softKeys.length; ++i ) {
			if ( softKeys[ i ] != null )
				softKeys[ i ].draw( g );
		} // fim for ( int i = 0; i < softKeys.length; ++i )

		//#if SHOW_FREE_MEM == "true"
//# 		g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
//# 		g.setColor( 0xff0000 );
//# 		g.setFont( Font.getFont( Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_MEDIUM ) );
//# 		g.drawString( freeMem, SCREEN_WIDTH, 0, Graphics.TOP | Graphics.RIGHT );
		//#endif

		//#if SHOW_FPS == "true"
//# 			g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
//# 			g.setColor( 0x00ff00 );
//# 			final Font font = Font.getFont( Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_MEDIUM );
//# 			g.setFont( font );
//# 			g.drawString( strFps, SCREEN_WIDTH, font.getHeight(), Graphics.TOP | Graphics.RIGHT );
		//#endif
		
		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable t ) {
//# 			t.printStackTrace();
//# 		}
		//#endif				
		
		//#if USE_FLUSH_GRAPHICS == "true"
//# 		flushGraphics();
		//#endif

		// a sincroniza��o deste m�todo com as teclas � necess�ria para evitar erros de desenho em v�rios aparelhos, notadamente
		// Siemens, Samsung e LG (drawables podem ser desenhados com offset incorreto por 1 frame durante pressionamento de teclas)
 		unlockKeys();
	} // fim do m�todo paint( Graphics )
	
	
	/**
	 * Trata o evento de tecla pressionada e o repassa ao keyListener, caso esteja registrado.
	 * 
	 * @param keyCode �ndice da tecla pressionada, recebido pelo aparelho. Caso haja keyListener registrado, seu
	 * m�todo keyPressed ser� chamado, recebendo o mesmo valor de keyCode (no caso de <i>handleSpecialKey</i> ser false),
	 * ou com as fun��es especiais tratadas (caso <i>handleSpecialKey</i> seja true).
	 * 
	 * @see #keyReleased(int)
	 * @see #setKeyListenerBehavior(boolean)
	 * @see KeyListener
	 */
	protected final void keyPressed( int keyCode ) {
		//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif
			
//		switch ( transitionStage ) { TODO teste de teclas
//			case TRANSITION_STAGE_IDLE:
//				lockKeys();
//					if ( keyListener != null ) {
//						keyListener.keyPressed( handleSpecialKey ? getSpecialKey( keyCode ) : keyCode );
//					}
//				unlockKeys();
//			break;
//		}
		lockKeys();
		if ( keyListener != null ) {
			switch ( keyCode ) {
				case KEY_NUM0:
				case KEY_NUM1:
				case KEY_NUM2:
				case KEY_NUM3:
				case KEY_NUM4:
				case KEY_NUM5:
				case KEY_NUM6:
				case KEY_NUM7:
				case KEY_NUM8:
				case KEY_NUM9:
				case KEY_STAR:
				case KEY_POUND:
					keyListener.keyPressed( keyCode );
				break;
				
				default:
					keyListener.keyPressed( handleSpecialKey ? getSpecialKey( keyCode ) : keyCode );
			}
		}
		unlockKeys();

		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		} finally {
//# 				unlockKeys();
//# 			}
		//#endif	
		
	} // fim do m�todo keyPressed( int )	


	/**
	 * Evento recebido ao arrastar um ponteiro numa tela sens�vel ao toque. Caso haja um <code>pointerListener</code>
	 * registrado, o evento ser� repassado para ele.
	 * 
	 * @param x posi��o x do ponteiro.
	 * @param y posi��o y do ponteiro.
	 * @see #pointerPressed(int, int)
	 * @see #pointerReleased(int, int)
	 * @see PointerListener
	 */
	protected final void pointerDragged( int x, int y ) {
		if ( pointerListener != null )
			pointerListener.onPointerDragged( x, y );
	}


	/**
	 * Evento recebido quando o usu�rio pressiona a tela com um ponteiro. Caso o usu�rio clique sobre um soft key e 
	 * haja um keyListener definido, o keyListener ir� receber o evento do soft key pressionado. Caso haja um PointerListener
	 * definido, este ir� receber o evento.
	 * 
	 * @param x posi��o x do ponteiro.
	 * @param y posi��o y do ponteiro.
	 * @see #pointerDragged(int, int)
	 * @see #pointerReleased(int, int)
	 * @see PointerListener
	 */
	protected final void pointerPressed( int x, int y ) {
		// verifica colis�o com os softkeys
		for ( byte i = 0; i < softKeys.length; ++i ) {
			if ( softKeys[ i ] != null && softKeys[ i ].contains( x, y ) ) {
				switch ( i ) {
					case SOFT_KEY_LEFT:
						keyPressed( KEY_SOFT_LEFT );
					return;

					case SOFT_KEY_MID:
						keyPressed( KEY_SOFT_MID );
					return;

					case SOFT_KEY_RIGHT:
						keyPressed( KEY_SOFT_RIGHT );
					return;
				}
			} //  fim if ( softKeys[ i ] != null && softKeys[ i ].contains( x, y ) )
		} // fim for ( byte i = 0; i < softKeys.length; ++i )
		
		if ( pointerListener != null )
			pointerListener.onPointerPressed( x, y );		
	} // fim do m�todo pointerPressed( int, int )


	/**
	 * Evento recebido quando o usu�rio p�ra de pressionar a tela utilizando um ponteiro. Caso haja um <code>PointerListener</code>
	 * definido, este evento ser� repassado para ele.
	 * 
	 * @param x posi��o x onde o ponteiro foi solto.
	 * @param y posi��o y onde o ponteiro foi solto.
	 * @see #pointerPressed(int, int)
	 * @see #pointerDragged(int, int)
	 * @see PointerListener
	 */
	protected final void pointerReleased( int x, int y ) {
		if ( pointerListener != null )
			pointerListener.onPointerReleased( x, y );		
	}


	/**
	 * Trata o evento de tecla liberada e o repassa ao keyListener, caso esteja registrado.
	 * 
	 * @param keyCode �ndice da tecla liberada, recebido pelo aparelho. Caso haja keyListener registrado, seu
	 * m�todo keyReleased ser� chamado, recebendo o mesmo valor de keyCode (no caso de <i>handleSpecialKey</i> ser false),
	 * ou com as fun��es especiais tratadas (caso <i>handleSpecialKey</i> seja true).
	 * 
	 * @see #keyPressed(int)
	 * @see #setKeyListenerBehavior(boolean)
	 * @see KeyListener
	 */
	protected final void keyReleased( int keyCode ) {
		lockKeys();
		
		//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif
			
		if ( keyListener != null )
			keyListener.keyReleased( handleSpecialKey ? getSpecialKey( keyCode ) : keyCode );
		
		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif	
		
		unlockKeys();
	} // fim do m�todo keyReleased( int )

	
	/**
	 * 
	 */
	public final void run() {
		//#if TRY_CATCH_ALL == "true"
//# 		try {
		//#endif
			
		Display.getDisplay( AppMIDlet.getInstance() ).setCurrent( this );
		
        lastFrameTime = 0;
		
		long time;
		long lastFrameMS = System.currentTimeMillis();
		
		while ( running ) {
			synchronized ( this ) {
				while ( paused ) {
					try {
						wait();
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif
					}
				} // fim while ( paused )
			} // fim synchronized( this )
			
			time = System.currentTimeMillis();

			lastFrameTime = ( int ) ( time - lastFrameMS );
			// limita o tempo m�ximo do frame
			if ( lastFrameTime > MAX_FRAME_TIME )
				lastFrameTime = MAX_FRAME_TIME;

			if ( lastFrameTime > 0 ) {
				if ( updatableBackground != null )
					updatableBackground.update( lastFrameTime );

				// se a tela atual for atualiz�vel, chama seu m�todo update
				if ( currentScreenUpdatable != null )
					currentScreenUpdatable.update( lastFrameTime );

				// atualiza os softkeys, caso sejam atualiz�veis
				for ( int i = 0; i < softKeysUpdatables.length; ++i ) {
					if ( softKeysUpdatables[ i ] != null )
						softKeysUpdatables[ i ].update( lastFrameTime );
				} // fim for ( int i = 0; i < softKeysUpdatables.length; ++i )
			} // fim if ( lastFrameTime > 0 )

			lastFrameMS = System.currentTimeMillis();

			//#if SHOW_FREE_MEM == "true"
//# 			timeToNextUpdate -= lastFrameTime;
//# 			if ( timeToNextUpdate <= 0 ) {
//# 				timeToNextUpdate = SHOW_FREE_MEM_UPDATE_INTERVAL;
//# 				
//# 				System.gc();
//# 				freeMem = String.valueOf( Runtime.getRuntime().freeMemory() );
//# 			}
			//#endif

			//#if SHOW_FPS == "true"
//# 			++frameCounter;
//# 			
//# 			fp_accFpsTime += NanoMath.divInt( lastFrameTime, 1000 );
//# 			if ( fp_accFpsTime > 0 ) {
//# 				strFps = NanoMath.toString( NanoMath.divFixed( NanoMath.toFixed( frameCounter ), fp_accFpsTime ) );
//# 				
//# 				if ( fp_accFpsTime >= FP_FPS_UPDATE_INTERVAL ) {
//# 					frameCounter = 0;
//# 					fp_accFpsTime %= FP_FPS_UPDATE_INTERVAL;
//# 				}
//# 			}
			//#endif

			// Redesenha a tela. Importante: a dupla repaint()/serviceRepaints() � uma op��o melhor para
			// fazer o redesenho da tela do que paint(offScreenBuffer)/flushGraphics(). A utiliza��o da 2� op��o causa
			// muita lentid�o no tratamento de teclas em alguns aparelhos, como SonyEricsson Z550 e K500, independentemente
			// do uso ou n�o de mutex no m�todo paint(Graphics).
			repaint();
			serviceRepaints(); 
		} // fim while ( running )
		
		//#if TRY_CATCH_ALL == "true"
//# 		} catch ( Throwable e ) {
//# 			e.printStackTrace();
//# 		}
		//#endif
	} // fim do m�todo run()
	
			
	/**
	 * Define o fundo.
	 * 
	 * @param background Drawable a ser usado como fundo de tela. Caso seja null, o fundo � preenchido com a cor definida.
	 * @param update indica se o background deve ser atualizado juntamente com a tela ativa. Para isso, o drawable deve
	 * implementar a interface <i>Updatable</i>. Para manter o background atual e somente alterar a op��o de atualiz�-lo
	 * ou n�o, basta chamar esse m�todo passando o mesmo drawable utilizado como background (pode-se utilizar o m�todo
	 * <i>getBackground</i> dessa classe, por exemplo), e alterar somente o par�mentro <i>update</i>.
	 */
	public final void setBackground( Drawable background, boolean update ) {
		this.background = background;
		
		if ( update && ( background instanceof Updatable ) )
			updatableBackground = ( Updatable ) background;
		else
			updatableBackground = null;
	} // fim do m�todo setBackground( Drawable, boolean )
	
	
	/**
	 * Obt�m o Drawable sendo atualmente utilizado como fundo de tela.
	 * @return refer�ncia para o Drawable utilizado como fundo de tela, ou null caso o fundo esteja sendo preenchido
	 * com uma cor s�lida definida anteriormente.
	 */
	public final Drawable getBackground() {
		return background;
	}
	
	
	/**
	 * Obt�m uma refer�ncia para a inst�ncia do gerenciador de telas.
	 * @return refer�ncia para o gerenciador de telas.
	 */
	public static final ScreenManager getInstance() {
		return instance;
	}
	
	
	/**
	 * Obt�m a refer�ncia para o back buffer da tela.
	 * 
	 * @return refer�ncia para o Graphics onde a tela � desenhada.
	 */
	public static final Graphics getScreenBuffer() {
		return instance.screenBuffer;
	}
	
	
	/**
	 * Inicia a execu��o do ScreenManager, definindo-o como a tela ativa do aparelho e executando o loop principal de 
	 * atualiza��o e desenho de tela.
	 */
    public synchronized final void start() {
        if ( !running ) {
			running = true;
            thread = new Thread( this );
            thread.start();
        }
    }
    	
	
    /**
	 * Interrompe a execu��o da thread do gerenciador de telas.
	 */
    public final void stop() {
		running = false;
		thread = null;
    }	 	
	

	/**
	 * Obt�m o bloqueio de acesso �s teclas.
	 * @see #unlockKeys()
	 */
	private synchronized final void lockKeys() {
		while ( usingKeys ) {
			try {
				wait();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif
			}
		} // fim while ( usingKeys )
		usingKeys = true;		
	} // fim do m�todo lockKeys()
	

	/**
	 * Libera o bloqueio de acesso �s teclas.
	 * @see #lockKeys()
	 */
	private synchronized final void unlockKeys() {
		usingKeys = false;
		notifyAll();		
	} // fim do m�todo unlockKeys()

	
	/**
	 * M�todo repassado pelo aparelho, informando uma altera��o nas dimens�es da tela.
	 * 
	 * @param w nova largura da tela, em pixels.
	 * @param h nova altura da tela, em pixels.
	 */
	protected final void sizeChanged( int w, int h ) {
		if ( w > 0 && h > 0 ) {
			// gambiarra nojenta causada pelo LG ME970, que indica que tem 307 de altura quando na verdade tem 304
			if ( AppMIDlet.getVendor() == AppMIDlet.VENDOR_LG && h == 307 ) {
				h = 304;
			}
			
			super.sizeChanged( w, h );
			
			SCREEN_WIDTH = w;
			SCREEN_HALF_WIDTH = w >> 1;
			
			SCREEN_HEIGHT = h;
			SCREEN_HALF_HEIGHT = h >> 1;
			
			viewport.set( 0, 0, w, h );

			// atualiza a posi��o dos labels dos softkeys (caso existam)
			for ( int i = SOFT_KEY_LEFT; i <= SOFT_KEY_RIGHT; ++i )
				setSoftKey( i, softKeys[ i ] );

			if ( screenListener != null )
				screenListener.sizeChanged( w, h );
		}
	} // fim do m�todo sizeChanged( int, int )
	
	
	/**
	 * Retorna a refer�ncia para a tela corrente.
	 * @return refer�ncia para a tela corrente.
	 */
	public final Drawable getCurrentScreen() {
		return currentScreen;
	}	
	
	
	public static final KeyListener getKeyListener() {
		return keyListener;
	}


	public static final void setKeyListener( KeyListener keyListener ) {
		ScreenManager.keyListener = keyListener;
	}


	public static final PointerListener getPointerListener() {
		return pointerListener;
	}


	public static final void setPointerListener( PointerListener pointerListener ) {
		ScreenManager.pointerListener = pointerListener;
	}


	public static final ScreenListener getScreenListener() {
		return screenListener;
	}


	public static final void setScreenListener( ScreenListener screenListener ) {
		ScreenManager.screenListener = screenListener;
	}
	 
}
 
