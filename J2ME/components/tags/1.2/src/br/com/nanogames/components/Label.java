/*
 * Label.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public class Label extends Drawable implements KeyListener {
	 
	/** fonte utilizada para desenhar o texto */
	protected ImageFont font;
	
	/** array de caracteres que representa o texto do label */
	protected char[] charBuffer;
	 

	/**
	 * Cria um novo label.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	public Label( ImageFont font, String text ) throws Exception {
		this( font, text, true );
	}
	
	
	/**
	 * Cria um novo label.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
	 * @param setTextSize indica se a largura do label deve ser redefinida de acordo com o texto. <i>true</i> redimensiona
	 * o label de forma que tenha a mesma largura que o texto, e <i>false</i> mant�m as dimens�es atuais do label, 
	 * independentemente do texto definido.
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	protected Label( ImageFont font, String text, boolean setTextSize ) throws Exception {
		if ( font == null ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao criar Label: fonte nula." );
			//#else
				throw new Exception();
			//#endif
		}
		
		setFont( font );
		setText( text, setTextSize );		
	}
	
	
	/**
	 * Define o texto do texto a partir do �ndice do texto nos recursos do aplicativo. Equivalente � chamada de 
	 * <code>setText( AppMIDlet.getText( textIndex )</code>.
	 * 
	 * @param textIndex �ndice do texto definido no AppMIDlet.
	 * @see #setText(String)
	 * @see #setText(String,boolean)
	 */
	public final void setText( int textIndex ) {
		setText( AppMIDlet.getText( textIndex ) );
	}
	
	
	/**
	 * Define o texto do texto a partir do �ndice do texto nos recursos do aplicativo. Equivalente � chamada de 
	 * <code>setText( AppMIDlet.getText( textIndex, setSize )</code>.
	 * 
	 * @param textIndex �ndice do texto definido no AppMIDlet.
	 * @param setSize indica se as dimens�es do label devem ser atualizadas de forma a conter todo o texto.
	 * @see #setText(String)
	 * @see #setText(String,boolean)
	 */
	public final void setText( int textIndex, boolean setSize ) {
		setText( AppMIDlet.getText( textIndex ), setSize );
	}
	
	
	/**
	 * Define o texto do label. Equivalente � chamada de <code>setText( text, true )</code>.
	 * 
	 * @param text texto do label.
	 * @see #setText(String, boolean)
	 * @see #setText(int)
	 */
	public final void setText( String text ) {
		setText( text, true );
	}
	
	
	/**
	 * Define o texto do label.
	 * 
	 * @param text texto do label.
	 * @param setSize indica se as dimens�es do label devem ser atualizadas de forma a conter todo o texto.
	 * @see #setText(String)
	 * @see #setText(int)
	 */
	public void setText( String text, boolean setSize ) {
		if ( text == null )
			text = "";

		charBuffer = text.toCharArray();
		
		if ( setSize )
			setSize( font.getTextWidth( text ), font.getHeight() );		
	}
	 
	
	public String getText() {
		return new String( charBuffer );
	}
	 
	
	public ImageFont getFont() {
		return font;
	}


	public void setFont( ImageFont font ) throws NullPointerException {
		if ( font == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao definir fonte do label: fonte nula." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		this.font = font;
	}
	 
	
	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
	}
	 
	
	/**
	 *@see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
	}
	

	protected void paint( Graphics g ) {
		drawString( g, charBuffer, translate.x, translate.y );
	}
	
	
	/**
	 * Desenha um texto num Graphics. 
	 * 
	 * @param g Graphics onde ser� desenhado o texto.
	 * @param text texto a ser desenhado.
	 * @param x posi��o x (esquerda) onde ser� desenhado o texto.
	 * @param y posi��o y (topo) onde ser� desenhado o texto.
	 */
	public final void drawString( Graphics g, String text, int x, int y ) {
		drawString( g, text.toCharArray(), x, y );
	}
	
	
	/**
	 * Desenha um texto num Graphics. 
	 * 
	 * @param g Graphics onde ser� desenhado o texto.
	 * @param text texto a ser desenhado.
	 * @param x posi��o x (esquerda) onde ser� desenhado o texto.
	 * @param y posi��o y (topo) onde ser� desenhado o texto.
	 */
	public void drawString( Graphics g, char[] text, int x, int y ) {
		// n�o desenha caracteres que estejam fora da �rea de clip horizontal
		final int limitLeft = getClip().x;
		final int limitRight = limitLeft + getClip().width;
		
		final int textLength = text.length;
		
		// TODO : Desmembrar este for em 3:
		// 1) Desenhar o 1o caracter da linha com teste completo de clip
		// 2) Desenhar todos os caracteres internos sem testes
		// 3) Desenhar o �ltimo caracter da linha com teste completo de clip
		for ( int i = 0; i < textLength && x < limitRight; ++i ) {
			// o teste da largura do caracter � usado para evitar chamadas desnecess�rias de fun��es no caso
			// de caracteres n�o presentes na fonte (s�o ignorados)
			final char c = charBuffer[ i ];
			final byte charWidth = font.charsWidths[ c ];
			
			if ( charWidth > 0 && ( x + charWidth >= limitLeft ) ) {
				// faz a interse��o da �rea de clip do caracter com a �rea de clip do texto todo
				pushClip( g );
				
				g.clipRect( x, y, charWidth, font.image.getHeight() );
				g.drawImage( font.image, x - font.charsOffsets[ c ], y, 0 );
				
				// restaura a �rea de clip total
				popClip( g );
			} // fim if ( charWidth > 0 && ( x + charWidth >= limitLeft ) )
			
			x += font.charsWidths[ c ] + font.getCharOffset();
		} // fim for ( int i = 0; i < textArray.length && x < limitRight; ++i )
	} // fim do m�todo drawString( Graphics, String, int, int )
	
	
	/**
	 * Desenha um caracter num Graphics. 
	 * 
	 * @param g Graphics onde ser� desenhado o texto.
	 * @param c caracter a ser desenhado.
	 * @param x posi��o x (esquerda) onde ser� desenhado o texto.
	 * @param y posi��o y (topo) onde ser� desenhado o texto.
	 */
	public void drawChar( Graphics g, char c, int x, int y ) {
		// n�o desenha se o caracter estiver fora da �rea de clip horizontal
		final int limitLeft = getClip().x;
		final int limitRight = limitLeft + getClip().width;
		
		final int charWidth = font.charsWidths[ c ];
		if ( charWidth > 0 && x < limitRight && x + charWidth >= limitLeft ) {
			pushClip( g );

			// faz a interse��o da �rea de clip do caracter com a �rea de clip do texto todo
			g.clipRect( x, y, font.charsWidths[ c ], font.image.getHeight() );
			g.drawImage( font.image, x - font.charsOffsets[ c ], y, 0 );

			// restaura a �rea de clip total
			popClip( g );
		} // fim if ( charsWidths[ c ] > 0 )
	} // fim do m�todo drawChar( Graphics, char, int, int )	

}
 
