/*
 * BasicAnimatedPattern.java
 *
 * Created on 7 de Julho de 2007, 11:48
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class BasicAnimatedPattern extends Pattern {
	
	public static final byte ANIMATION_RANDOM				= -1;
	public static final byte ANIMATION_NONE					= 0;
	public static final byte ANIMATION_HORIZONTAL			= 1;
	public static final byte ANIMATION_ALTERNATE_HORIZONTAL	= 2;
	public static final byte ANIMATION_VERTICAL				= 3;
	public static final byte ANIMATION_ALTERNATE_VERTICAL	= 4;
	public static final byte ANIMATION_DIAGONAL				= 5;
	
	public static final byte TOTAL_ANIMATIONS				= 6;
	
	private byte currentAnimation;
	
	private final Point offset = new Point();
	
	private final MUV xMUV = new MUV();
	private final MUV yMUV = new MUV();
	
	
	/** 
	 * 
	 * @param fill drawable utilizado como preenchimento do pattern.
	 * @param speedX velocidade horizontal de animação, em pixels por segundo.
	 * @param speedY velocidade vertical de animação, em pixels por segundo.
	 */
	public BasicAnimatedPattern( Drawable fill, int speedX, int speedY ) {
		super( fill );

		setSpeed( speedX, speedY );
	}

	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( currentAnimation != ANIMATION_NONE ) {
			final int dx = xMUV.updateInt( delta );
			final int dy = yMUV.updateInt( delta );

			switch ( currentAnimation ) {
				case ANIMATION_HORIZONTAL:
				case ANIMATION_ALTERNATE_HORIZONTAL:
					offset.x = ( offset.x + dx ) % fill.getWidth();
				break;
				
				case ANIMATION_VERTICAL:
				case ANIMATION_ALTERNATE_VERTICAL:
					offset.y = ( offset.y + dy ) % fill.getHeight();					
				break;
				
				case ANIMATION_DIAGONAL:
					offset.x = ( offset.x + dx ) % fill.getWidth();
					offset.y = ( offset.y + dy ) % fill.getHeight();					
				break;
			}
		} // fim if ( animation != ANIMATION_NONE )
	}
	
	
	public final void setAnimation( int animation ) {
		if ( animation == ANIMATION_RANDOM )
			animation = ( byte ) ( NanoMath.randInt( TOTAL_ANIMATIONS ) );
				
		switch ( animation ) {
			case ANIMATION_NONE:
			case ANIMATION_HORIZONTAL:
			case ANIMATION_VERTICAL:
			case ANIMATION_DIAGONAL:
			case ANIMATION_ALTERNATE_HORIZONTAL:
			case ANIMATION_ALTERNATE_VERTICAL:
			break;
			
			default:
				return;
		}
		
		if ( NanoMath.randInt( 100 ) < 50 )
			xMUV.setSpeed( -xMUV.getSpeed() );
		
		if ( NanoMath.randInt( 100 ) < 50 )
			yMUV.setSpeed( -yMUV.getSpeed() );
		
		currentAnimation = ( byte )animation;
	}
	
	
	/**
	 * Define a velocidade de movimentação dos patterns.
	 * @param speedX velocidade horizontal em pixels por segundo.
	 * @param speedY velocidade vertical em pixels por segundo.
	 */
	public final void setSpeed( int speedX, int speedY ) {
		xMUV.setSpeed( speedX );
		yMUV.setSpeed( speedY );
	}

	
	protected final void paint( Graphics g ) {
		// TODO otimizar desenho de BasicAnimatedPattern (conforme otimizações feitas na classe Pattern)
		final Point fillSize = fill.getSize();		
		boolean alternate = false;
		
		switch ( currentAnimation ) {
			case ANIMATION_ALTERNATE_HORIZONTAL:
				for ( int y = -Math.abs( offset.y ); y < size.y; y += fillSize.y ) {
					alternate = !alternate;
					for ( int x = -Math.abs( alternate ? -offset.x : ( offset.x - fillSize.x ) ); x < size.x; x += fillSize.x ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = -offset.x; x < size.x; x += fillSize.x )
				} // fim for ( int y = -offset.y; y < size.y; y += fillSize.y )				
			break;
			
			case ANIMATION_ALTERNATE_VERTICAL:
				for ( int x = -Math.abs( offset.x ); x < size.x; x += fillSize.x ) {
					alternate = !alternate;
					for ( int y = -Math.abs( alternate ? -offset.y : ( offset.y - fillSize.y ) ); y < size.y; y += fillSize.y ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = -offset.x; x < size.x; x += fillSize.x )
				} // fim for ( int y = -offset.y; y < size.y; y += fillSize.y )				
			break;
		
			default:
				for ( int y = -Math.abs( offset.y ); y < size.y; y += fillSize.y ) {
					for ( int x = -Math.abs( offset.x ); x < size.x; x += fillSize.x ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = -offset.x; x < size.x; x += fillSize.x )
				} // fim for ( int y = -offset.y; y < size.y; y += fillSize.y )
			break;
		}
	}

}
