/*
 * AnimatedTransition.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 * Essa interface deve ser implementada pelos Drawables utilizados como telas principais, quando se deseja que elas
 * realizem algum tipo de anima��o no momento da troca de tela feita por um <i>ScreenManager</i>.
 * @author peter
 */
public interface AnimatedTransition {
 
	// estados da transi��o
	public static final byte TRANSITION_INACTIVE	= 0;
	public static final byte TRANSITION_APPEARING	= 1;
	public static final byte TRANSITION_SHOWN		= 2;
	public static final byte TRANSITION_HIDING		= 3;
	
	
	/**
	 * Define o estado atual da transi��o.
	 * @param state novo estado da transi��o.
	 */
	public abstract void setTransitionState( byte state );
	
	
	/**
	 * Atualiza a anima��o de transi��o ap�s "delta" milisegundos. O fim da transi��o � determinado pela classe que
	 * implementa esta interface, ou ent�o for�ada por uma chamada do m�todo setTransitionState pelo gerenciador de telas.
	 * @param delta intervalo de tempo considerado para a atualiza��o.
	 */
	public abstract void updateTransition( int delta );
	
	
	/**
	 * Indica o estado atual da transi��o.
	 * @return byte indicando o estado atual da transi��o.
	 */
	public abstract byte getTransitionState();
}
 
