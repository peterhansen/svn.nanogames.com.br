/*
 * UpdatablePattern.java
 *
 * Created on April 11, 2007, 3:14 PM
 *
 */

package br.com.nanogames.components;

/**
 *
 * @author peter
 */
public class UpdatablePattern extends Pattern implements Updatable {
	
	protected Updatable updatableFill;
	
	/** Creates a new instance of UpdatablePattern */
	public UpdatablePattern( Drawable fill ) {
		super( fill );
	}

	
	public void update( int delta ) {
		if ( updatableFill != null )
			updatableFill.update( delta );
	}

	
	public void setFill( Drawable fill ) {
		super.setFill( fill );
		
		if ( fill instanceof Updatable )
			updatableFill = ( Updatable ) fill;
		else
			updatableFill = null;
	}
	
}
