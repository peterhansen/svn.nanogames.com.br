/*
 * KeyListener.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 *
 * @author peter
 */
public interface KeyListener {
 
	
	/**
	 * M�todo chamado quando uma tecla � pressionada.
	 * @param key �ndice da tecla pressionada.
	 */
	public abstract void keyPressed( int key );
	
	
	/**
	 * M�todo chamado quando uma tecla que estava pressionada � liberada.
	 * @param key �ndice da tecla liberada.
	 */
	public abstract void keyReleased( int key );
}
 
