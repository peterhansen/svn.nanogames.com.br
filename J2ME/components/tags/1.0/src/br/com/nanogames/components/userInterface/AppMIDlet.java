/*
 * AppMIDlet.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotOpenException;


/**
 *
 * @author peter
 */
public class AppMIDlet extends MIDlet {
 
	protected static AppMIDlet instance;
	
	// perfis de fabricantes
	public static final byte VENDOR_GENERIC			= 0;
	public static final byte VENDOR_NOKIA			= 1;
	public static final byte VENDOR_MOTOROLA		= 2;
	public static final byte VENDOR_SAMSUNG			= 3;
	public static final byte VENDOR_LG				= 4;
	public static final byte VENDOR_SONYERICSSON	= 5;
	public static final byte VENDOR_SIEMENS			= 6;
	public static final byte VENDOR_VODAFONE		= 7;
	
	// perfil do fabricante detectado
	public final byte VENDOR;
	
	private static final byte TOTAL_VENDORS = VENDOR_SIEMENS + 1;
		
	// strings que indicam cada fabricante (em algumas marcas, pode haver mais de uma poss�vel string, dependendo do modelo)
	protected final String[][] VENDOR_AGENT = {
		{},						// gen�rico
		{ "nokia" },			// Nokia
		{ "mot-", "motorola" },	// Motorola
		{ "samsung", "sec-" },	// Samsung
		{ "lg-" },				// LG
		{ "sonyericsson" },		// SonyEricsson
		{ "sie-" },				// Siemens
		{ "vodafone" },			// Vodafone TODO confirmar String
	};
	
	protected ScreenManager manager;
	
	// TODO: compartilhar informa��es no RecordStore a respeito dos jogos/aplicativos Nano instalados no aparelho
	
	
	protected AppMIDlet() {
		String platform = System.getProperty( "microedition.platform" );

		byte vendor = VENDOR_GENERIC;		
		if ( platform != null ) {
			platform = platform.toLowerCase();
			byte i, j;
			
			for ( i = 0; i < TOTAL_VENDORS && vendor == VENDOR_GENERIC; ++i ) {
				for ( j = 0; j < VENDOR_AGENT[ i ].length; ++j ) {
					if ( platform.indexOf( VENDOR_AGENT[ i ][ j ] ) >= 0 ) {
						// encontrou o fabricante (valor da vari�vel n�o � atribu�da aqui porque vari�veis "final"
						// n�o podem ser atribu�das em loops)
						vendor = i;
						break;
					}
				} // fim for ( j = 0; j < VENDOR_AGENT[ i ].length; ++j )
			} // fim for ( i = 0; i < TOTAL_VENDORS; ++i )
		} // fim if ( platform != null )
		
		if ( vendor == VENDOR_GENERIC ) {
			// aparelho n�o informou corretamente o seu fabricante; tenta a detec��o atrav�s da detec��o de APIs 
			// espec�ficas de cada fabricante. V�rias classes para o mesmo fabricante podem ser testados at� se encontrar
			// um v�lido, pois as APIs podem variar entre modelos e s�ries de modelos de cada fabricante.
			final String[][] classes = new String[][] {
				// gen�rico
				{},				
				
				// Nokia
				{},						
				
				// Motorola
				{ "com.motorola.phonebook.PhoneBookRecord",
				  "com.motorola.phone.Dialer",
				  "com.motorola.multimedia.Lighting",
				  "com.motorola.multimedia.Vibrator",
				  "com.motorola.extensions.ScalableImage",
				  "com.motorola.extensions.ScalableJPGImage",
				  "com.motorola.funlight.FunLight",
				  "com.motorola.funlight.FunLightException",
				  "com.motorola.pim.Contact",
				  "com.motorola.pim.ContactList",						  
				  "com.motorola.graphics.j3d.ActionTable",
				  "com.motorola.graphics.j3d.AffineTrans",
				  "com.motorola.graphics.j3d.Effect3D",
				  "com.motorola.graphics.j3d.Figure",
				  "com.motorola.graphics.j3d.FigureLayout",
				  "com.motorola.graphics.j3d.Light",
				  "com.motorola.graphics.j3d.Texture",
				  "com.motorola.graphics.j3d.Util3D",
				  "com.motorola.graphics.j3d.Vector3D", 
				},	
				
				// Samsung
				{ "com.samsung.util.Acceleration",
				  "com.samsung.util.AudioClip",
				  "com.samsung.util.LCDLight",
				  "com.samsung.util.SM",
				  "com.samsung.util.SMS",
				  "com.samsung.util.SMSListener",
				  "com.samsung.util.Vibration",
				  "com.samsung.immersion.VibeTonz" },
				  
				// LG
				{ "mmpp.media.BackLight",
				  "mmpp.media.Beep",
				  "mmpp.lang.MathFP",
				  "mmpp.media.MediaPlayer",
				  "mmpp.media.LED",
				  "mmpp.phone.ContentsManager",
				  "mmpp.phone.Phone",
				  "mmpp.microedition.lcdui.GraphicsX",
				  "mmpp.microedition.lcdui.TextFieldX" },
				  
				// SonyEricsson
				{ "com.sonyericsson.mmedia.AMRPlayer",
				  "com.sonyericsson.mmedia.VAMediaBridge",
				  "com.sonyericsson.mmedia.iMelodyPlayer",
				  "com.sonyericsson.util.TempFile" },
				  
				// Siemens
				{ "com.siemens.mp.lcdui.Image",
				  "com.siemens.mp.game.Light",
				  "com.siemens.mp.game.Sound",
				  "com.siemens.mp.game.Vibrator",
				  "com.siemens.mp.io.Connection",
				  "com.siemens.mp.io.File",	
				  "com.siemens.mp.NotAllowedException",
				  "com.siemens.mp.wireless.messaging.MessageConnection",
				  "com.siemens.mp.wireless.messaging.MessagePart",
				  "com.siemens.mp.wireless.messaging.MultipartMessage", },
				
				// Vodafone
				{ "com.vodafone.v10.system.device.DeviceControl", },			
			};
			
			// TODO evitar carregar array com todas as classes poss�veis (ler de arquivo?), de forma a reduzir tempo de
			// carregamento dos midlets (muito longo)
			for ( int i = 0; i < classes.length; ++i ) {
				for ( int  j = 0; j < classes[ i ].length; ++j ) {
					try {
						Class.forName( classes[ i ][ j ] );
						
						// se chegou nessa linha (ou seja, n�o lan�ou exce��o), � sinal de que encontrou a String
						vendor = ( byte ) i;
						i = classes.length;
						break;
					} catch ( Exception e ) {
					}					
				} // fim for ( int j = 0; j < classes[ i ].length; ++j )
			} // fim for ( int i = 0; i < classes.length; ++i )
		} // fim if ( vendor == VENDOR_GENERIC ) 
		
		VENDOR = vendor;		
	} // fim do construtor AppMIDlet()
	
	
	/**
	 * Indica se uma base de dados j� existe.
	 * @param dataBaseName nome da base de dados.
	 * @return true, caso a base j� exista, e false caso contr�rio.
	 */
    public static boolean hasDatabase( String dataBaseName ) {
        final String[] names = RecordStore.listRecordStores();
        
        if ( names != null ) {
            for ( int i = 0; i < names.length; ++i ) {
                if ( names[ i ].equals( dataBaseName ) )
					return true;
            } // fim for ( int i = 0; i < names.length; ++i )
        } // fim if ( names != null )
        
        return false;
    } // fim do m�todo hasDataBase( String )
    
	
	/**
	 * Cria uma base de dados com uma quantidade pr�-determinada de slots para grava��o.
	 * 
	 * @param dataBaseName nome da base de dados.
	 * @param numSlots n�mero de slots dispon�veis para grava��o.
	 * @throws java.lang.Exception caso haja erro ao criar a base de dados.
	 */
    public static synchronized void createDatabase( String dataBaseName, int numSlots ) throws Exception {
        if ( hasDatabase( dataBaseName ) ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao criar base de dados: base de dados \"" + dataBaseName + "\" j� existente." );
			//#else
				throw new Exception();
			//#endif
		}
        
        RecordStore rs = null;
        
        try {
            rs = RecordStore.openRecordStore( dataBaseName, true );
            
            for ( int i = 0; i < numSlots; ++i ) {
                rs.addRecord( null, 0, 0 );
            }
        } catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao criar base de dados: n�o h� espa�o dispon�vel." );
			//#else
				throw new Exception();
			//#endif
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif			
        } finally {
            if ( rs != null ) {
				try { 
					rs.closeRecordStore();
				} catch( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
				}
			} // fim if ( rs != null )
        } // fim finally
    } // fim do m�todo createDatabase( String, int )
	
	
	/**
	 * Grava dados numa base de dados.
	 * @param dataBaseName nome da base de dados (precisa j� existir)
	 * @param slot �ndice do slot onde os dados ser�o gravados. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots permite grava��o nos slots
	 * 1, 2 e 3.
	 * @param serializable refer�ncia para o objeto Serializable que gravar� seus dados no slot, atrav�s do seu m�todo
	 * <i>write( DataOutputStream )</i>. N�o � necess�rio chamar o m�todo flush() do DataOutputStream.
	 * @throws java.lang.Exception caso haja erro ao abrir ou gravar os dados na base.
	 * @see br.com.nanogames.components.util.Serializable#write(DataOutputStream)
	 */	
	public static synchronized final void saveData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		// TODO criar classe espec�fica para leitura e grava��o de arquivos (RMS, arquivos no JAR, arquivos no celular)
		if ( serializable == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao gravar na base de dados: serializable n�o pode ser null." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		if ( !hasDatabase( dataBaseName ) ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao gravar dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
				throw new Exception();
			//#endif
		}
		
		ByteArrayOutputStream byteOutput = null;
		DataOutputStream dataOutput = null;
		RecordStore rs = null;		
		
		try {
			byteOutput = new ByteArrayOutputStream();
			dataOutput = new DataOutputStream( byteOutput );
			
			serializable.write( dataOutput );
			
			dataOutput.flush();
			
			final byte[] data = byteOutput.toByteArray();
        
            rs = RecordStore.openRecordStore( dataBaseName, false );
            rs.setRecord( slot, data, 0, data.length );
		} catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao gravar na base de dados: n�o h� espa�o dispon�vel." );
			//#else
				throw new Exception();
			//#endif
        } catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao gravar na base de dados: slot inv�lido: " + slot );
			//#else
				throw new Exception();
			//#endif
		} finally {
            if ( rs != null ) {
				try { 
					rs.closeRecordStore(); 
				} catch( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif					
				}
			} // fim if ( rs != null )			
			
			if ( byteOutput != null ) {
				try {
					byteOutput.close();
				} catch ( Exception e ) {
				}
				byteOutput = null;
			} // fim if ( byteOutput != null )
			
			if ( dataOutput != null ) {
				try {
					dataOutput.close();
				} catch ( Exception e ) {
				}
				dataOutput = null;
			} // fim if ( dataOutput != null )
		} // fim finally
	} // fim do m�todo saveData( String, int, Serializable )
	
	
	/**
	 * L� os dados dispon�veis num slot de uma base de dados.
	 * @param dataBaseName nome da base de dados.
	 * @param slot �ndice do slot de onde os dados ser�o lidos. A faixa de valores v�lidos vai de 1 (um) ao n�mero 
	 * total de slots na base de dados. Por exemplo: uma base de dados com 3 slots possui como �ndices v�lidos
	 * apenas 1, 2 e 3.
	 * @param serializable refer�ncia para o objeto Serializable que interpretar� o array de bytes lido da base de dados,
	 * atrav�s do seu m�todo <i>read( DataInputStream )</i>.
	 * @throws java.lang.Exception
	 * @see br.com.nanogames.components.util.Serializable#read(DataInputStream)
	 */	
	public static synchronized final void loadData( String dataBaseName, int slot, Serializable serializable ) throws Exception {
		if ( serializable == null ) {
			//#if DEBUG == "true"
//# 				throw new NullPointerException( "Erro ao ler da base de dados: serializable n�o pode ser null." );
			//#else
				throw new NullPointerException();
			//#endif
		}
		
		if ( !hasDatabase( dataBaseName ) ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler dados: base de dados \"" + dataBaseName + "\" n�o existe." );
			//#else
				throw new Exception();
			//#endif
		}
		
		ByteArrayInputStream byteInput = null;
		DataInputStream dataInput = null;
		RecordStore rs = null;
		
		try {
			rs = RecordStore.openRecordStore( dataBaseName, false );

			final byte[] data = rs.getRecord( slot );
			
			byteInput = new ByteArrayInputStream( data );
			dataInput = new DataInputStream( byteInput );
				
			serializable.read( dataInput );
		} catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new Exception( "Erro ao ler da base de dados: slot inv�lido: " + slot );
			//#else
				throw new Exception();
			//#endif
		} finally {
            if ( rs != null ) {
				try { 
					rs.closeRecordStore(); 
				} catch( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
				} // fim catch( Exception e )
			} // fim if ( rs != null )			
			
			if ( byteInput != null ) {
				try {
					byteInput.close();
				} catch ( Exception e ) {
				}
				byteInput = null;
			} // fim if ( byteInput != null )
			
			if ( dataInput != null ) {
				try {
					dataInput.close();
				} catch ( Exception e ) {
				}
				dataInput = null;
			} // fim if ( dataInput != null )
		} // fim finally		
	} // fim do m�todo loadData( String, int, Serializable )
	

	public static AppMIDlet getInstance() {
		return instance;
	}

	
	protected void startApp() throws MIDletStateChangeException {
		if ( instance == null )
			instance = this;
		
		if ( manager != null )
			manager.showNotify();
	}

	
	protected void pauseApp() {
		if ( instance != null && manager != null )
			manager.hideNotify();		
	}

	
	protected void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( instance.manager != null ) {
			instance.manager.hideNotify();
			instance.manager.stop();
			instance.manager = null;
		}		
		
		notifyDestroyed();
	}
	
	
	public static final void exit() {
		try {
			instance.destroyApp( true );
			instance = null;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	}
	 

	public static byte getVendor() {
		return instance.VENDOR;
	}
	
	
}
 
