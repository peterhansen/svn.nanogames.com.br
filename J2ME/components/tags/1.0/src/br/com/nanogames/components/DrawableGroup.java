/*
 * DrawableGroup.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class DrawableGroup extends Drawable {
 
	protected final Drawable[] drawables;
	
	protected short activeDrawables;
	 
	
    /**
     * Insere um drawable no pr�ximo slot dispon�vel.
     * @param drawable drawable a ser inserido no grupo.
     * @return o �ndice onde o drawable foi inserido, ou -1 caso n�o haja mais slots dispon�veis.
     */
	public int insertDrawable( Drawable drawable ) {
		if ( activeDrawables < drawables.length ) {
			drawables[ activeDrawables ] = drawable;
			return activeDrawables++;
		}
		
		return -1;
	} // fim do m�todo insertDrawable( Drawable )
	 
	
    /**
     * Remove um drawable do grupo.
     * @param index �ndice do drawable a ser removido no grupo.
     * @return uma refer�ncia para o drawable removido, ou null caso o �ndice seja inv�lido.
     */
	public Drawable removeDrawable( int index ) {
		try {
			final Drawable removed = drawables[ index ];
			
			if ( removed != null ) {
				for ( int i = index; i < activeDrawables - 1; ++i )
					drawables[ i ] = drawables[ i + 1 ];
				
				--activeDrawables;
				
				// anula a refer�ncia para o antigo �ltimo item do array, para evitar vazamento de mem�ria
				drawables[ activeDrawables ] = null;
			}
			
			return removed;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return null;
		}
	} // fim do m�todo removeDrawable( int )


    /**
	 * Altera a ordem de desenho de um drawable.
	 * @param oldIndex �ndice atual do drawable.
	 * @return <i>true</i>, caso a troca seja feita com sucesso, e <i>false</i> caso contr�rio. A faixa de valores v�lidos 
	 * para ambos os argumentos � de 0 (zero) ao n�mero de elementos inseridos menos um. Exemplo: caso haja 5 drawables 
	 * no grupo, os valores v�lidos s�o 0, 1, 2, 3 e 4. � importante notar que o n�mero de elementos inseridos n�o �
	 * necessariamente igual � capacidade do grupo - pode haver apenas um elemento inserido num grupo com capacidade para
	 * 10 drawables, por exemplo.
	 * @param newIndex novo �ndice do drawable. 
	 */
	public boolean setDrawableIndex( int oldIndex, int newIndex ) {
		if ( oldIndex < activeDrawables && newIndex < activeDrawables ) {
			try {
				final Drawable old = drawables[ oldIndex ];

				if ( oldIndex < newIndex ) {
					for ( int i = newIndex; i < oldIndex; ++i )
						drawables[ i ] = drawables[ i + 1 ];
				} else {
					for ( int i = oldIndex; i > newIndex; --i )
						drawables[ i ] = drawables[ i - 1 ];
				}

				drawables[ newIndex ] = old;
				return true;
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif

				return false;
			}
		}
		
		return false;
	}
	 
	
    /**
	 * Construtor de DrawableGroup.
	 * @param slots n�mero m�ximo de drawables.
	 * @throws java.lang.Exception caso o valor de <i>slots</i> seja menor que zero.
	 */
	public DrawableGroup( int slots ) throws Exception {
		drawables = new Drawable[ slots ];
	}
	
	 
    /**
     * 
     * @param index 
     * @return 
     */
	public Drawable getDrawable( int index ) {
		try {
			return drawables[ index ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return null;
		}
	} // fim do m�todo getDrawable( int )


	protected void paint( Graphics g ) {
		for ( int i = 0; i < activeDrawables; ++i )
			drawables[ i ].draw( g );
	} // fim do m�todo paint( Graphics )
	 
	
	public int getTotalSlots() {
		return drawables.length;
	}
	
	
	public int getUsedSlots() {
		return activeDrawables;
	}

	
	public boolean mirror( int mirrorType ) {
		super.mirror( mirrorType );
		
		if ( ( mirrorType & TRANS_MIRROR_H ) != TRANS_NONE ) {
			for ( int i = 0; i < activeDrawables; ++i ) {
				final Drawable d = drawables[ i ];
				final int previousX = d.getRefPixelX();
				
				d.mirror( mirrorType );
				d.setRefPixelPosition( size.x - previousX, d.getRefPixelY() );
			} // fim for ( int i = 0; i < activeDrawables; ++i )
		}
		
		if ( ( mirrorType & TRANS_MIRROR_V ) != TRANS_NONE ) {
			for ( int i = 0; i < activeDrawables; ++i ) {
				final Drawable d = drawables[ i ];
				final int previousY = d.getRefPixelY();
				
				d.mirror( mirrorType );
				d.setRefPixelPosition( d.getRefPixelX(), size.y - previousY );
			} // fim for ( int i = 0; i < activeDrawables; ++i )				
		}
		
		return true;
	} // fim do m�todo mirror( int )

	
	public boolean rotate( int rotationType ) {
		super.rotate( rotationType );
		
		switch ( rotationType ) {
			case TRANS_ROT90:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final Point previousRefPos = new Point( d.getRefPixelPosition() );
					
					d.rotate( rotationType );
					d.setRefPixelPosition( size.y - previousRefPos.y + ( size.x - size.y ), previousRefPos.x );
				}
			break;
			
			case TRANS_ROT180:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final int previousX = d.getRefPixelX();
					final int previousY = d.getRefPixelY();
					
					d.rotate( rotationType );
					d.setRefPixelPosition( size.x - previousX, size.y - previousY );	
				}				
			break;
			
			case TRANS_ROT270:
				for ( int i = 0; i < activeDrawables; ++i ) {
					final Drawable d = drawables[ i ];
					final Point previousRefPos = new Point( d.getRefPixelPosition() );
					
					d.rotate( rotationType );
					d.setRefPixelPosition( previousRefPos.y, size.x - previousRefPos.x + ( size.y - size.x ) );
				}				
			break;
		}
		
		return true;
	} // fim do m�todo rotate( int )
	
}

