/*
 * DrawableImage.java
 *
 * Created on May 4, 2007, 10:02 AM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class DrawableImage extends Drawable {
	
	protected final Image image;
	protected final Point imageSize = new Point();
	
	
	/** Creates a new instance of DrawableImage */
	public DrawableImage( Image image ) throws Exception {
		this.image = image;
		setSize( image.getWidth(), image.getHeight() );
	} // fim do construtor DrawableImage( Image )
	
	
	public DrawableImage( String imagePath ) throws Exception {
		this( Image.createImage( imagePath ) );
		System.gc();
	}

	
	protected void paint( Graphics g ) {
		if ( transform == TRANS_NONE )
			g.drawImage( image, translate.x, translate.y, 0 );
		else
			g.drawRegion( image, translate.x, translate.y, imageSize.x, imageSize.y, transformMIDP, 0, 0, 0 );
	}

	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		// as dimens�es da imagem s�o armazenadas para serem passadas ao m�todo drawRegion (as dimens�es passadas ao 
		// m�todo n�o podem exceder as dimens�es da imagem)		
		if ( width <= image.getWidth() )
			imageSize.x = width;
		if ( height <= image.getHeight() )
			imageSize.y = height;
	} // fim do m�todo setSize( int, int )

	
}
