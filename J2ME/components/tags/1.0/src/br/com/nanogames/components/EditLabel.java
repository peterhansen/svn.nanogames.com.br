/*
 * EditLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class EditLabel extends Label implements Updatable {
	
	protected static final char[][] charTable = {
		// TODO NUM0 deve variar, pois Motorola o utiliza como padr�o para mudan�a de entrada de caracteres
		{ '0', ' ', '\n', '+', '-', '=', '%', ':', ';', '$', '*', '#' },	// NUM0
		{ '.', ',', '?', '!', '1', '_', '@', '/', '\'', '\"' },				// NUM1
		{ 'a', 'b', 'c', '2', '�', '�', '�', '�' },							// NUM2
		{ 'd', 'e', 'f', '3', '�', '�' },									// NUM3
		{ 'g', 'h', 'i', '4', '�' },										// NUM4
		{ 'j', 'k', 'l', '5' },												// NUM5
		{ 'm', 'n', 'o', '6', '�' },										// NUM6
		{ 'p', 'q', 'r', 's', '7' },										// NUM7
		{ 't', 'u', 'v', '8', '�' },										// NUM8
		{ 'w', 'x', 'y', 'z', '9' },										// NUM9
		{ ' ', '(', ')', '[', ']', '{', '}', '<', '>', '\\' },				// asterisco (*) (varia de acordo com aparelho)
		{ ' ', '(', ')', '[', ']', '{', '}', '<', '>', '\\' },				// jogo-da-velha (#) (varia de acordo com aparelho)
	};
	
	// c�digo da �ltima tecla pressionada
	protected int lastKeyPressed;
	
	// tempo em milisegundos que uma tecla deve ser mantida pressionada para que seja utilizada a a��o de tecla segurada
	protected static final short KEY_PRESS_TIME = 400;
	
	// tempo em milisegundos ap�s uma tecla ser pressionada para que o caracter atual seja inserido no texto
	protected static final short KEY_WAIT_BEFORE_INSERT_TIME = 800;
	
	// tempo decorrido em milisegundos desde a �ltima tecla pressionada
	protected int timeSinceLastKeyPress;
	
	// �ndice do array de caracteres atualmente utilizado
	protected byte currentArrayIndex = -1;
	
	// �ndice do caracter atual do array utilizado
	protected byte currentCharIndex;
	
	// tecla utilizada para mudan�a de entrada de caracteres
	protected final char keyInputChange;
	
	protected static final byte KEY_INDEX_NUM0	= 0;
	protected static final byte KEY_INDEX_NUM1	= 1;
	protected static final byte KEY_INDEX_NUM2	= 2;
	protected static final byte KEY_INDEX_NUM3	= 3;
	protected static final byte KEY_INDEX_NUM4	= 4;
	protected static final byte KEY_INDEX_NUM5	= 5;
	protected static final byte KEY_INDEX_NUM6	= 6;
	protected static final byte KEY_INDEX_NUM7	= 7;
	protected static final byte KEY_INDEX_NUM8	= 8;
	protected static final byte KEY_INDEX_NUM9	= 9;
	protected static final byte KEY_INDEX_STAR	= 10;
	protected static final byte KEY_INDEX_POUND	= 11;
	
	protected static final byte TOTAL_KEYS = 12;
 
	// modos de entrada de texto
	public static final byte INPUT_MODE_ANY			= 0;
	public static final byte INPUT_MODE_DECIMAL		= 1;
	public static final byte INPUT_MODE_EMAIL		= 2;
	public static final byte INPUT_MODE_PASSWORD	= 3;
	public static final byte INPUT_MODE_URL			= 4;

	// modo de entrada atual
	protected byte inputMode = INPUT_MODE_ANY;
	 
	// posi��o do cursor
	protected int caretPosition;
	
	protected Drawable caret;
	
	// tempo padr�o em milisegundos que o cursor permanece vis�vel
	public static final int DEFAULT_CURSOR_TIME_VISIBLE = 500;
	
	// tempo padr�o em milisegundos que o cursor permanece invis�vel
	public static final int DEFAULT_CURSOR_TIME_INVISIBLE = 250;
	
	// tempo em milisegundos que o cursor permanece vis�vel
	protected int caretTimeVisible = DEFAULT_CURSOR_TIME_VISIBLE;
	
	// tempo em milisegundos que o cursor permanece invis�vel
	protected int caretTimeInvisible = DEFAULT_CURSOR_TIME_INVISIBLE;	
	
	// tempo acumulado da anima��o de "pisca-pisca" do cursor
	protected int accTime;
	
	// n�mero m�ximo de caracteres do label
	protected char[] textChars;
	
	// contador de caracteres
	protected short charCount;
	
	// tipos dispon�veis de m�scara para password (INPUT_MODE_PASSWORD)
	protected static final byte PASSWORD_MASK_TYPE_CHAR		= 0;
	protected static final byte PASSWORD_MASK_TYPE_DRAWABLE	= 1;
	
	protected byte passwordMaskType = PASSWORD_MASK_TYPE_CHAR;
	
	// caracter padr�o utilizado como m�scara para password
	public static final char PASSWORD_CHAR_MASK_DEFAULT = '*';

	// caracter utilizado como m�scara para password
	protected char passwordCharMask = PASSWORD_CHAR_MASK_DEFAULT;
	
	// Drawable utilizado como m�scara para password
	protected Drawable passwordDrawableMask;
	 
	
    /**
	 * Cria um label edit�vel.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param initialText texto inicial (pode ser vazio).
	 * @param maxChars n�mero m�ximo de caracteres.
	 * @param inputMode modo de entrada dos caracteres. Valores v�lidos:
	 * <ul>
	 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
	 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
	 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endere�os de e-mail.</li>
	 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, por�m n�o s�o exibidos. A m�scara que substitui
	 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padr�o, � um caracter '*' (asterisco)</li>
	 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endere�os de internet.</li>
	 * </ul>
	 * @throws java.lang.Exception caso a fonte seja nula.
	 */
	public EditLabel( ImageFont font, String initialText, int maxChars, byte inputMode ) throws Exception {
		super( font, initialText );
		setMaxChars( maxChars );
		setInputMode( inputMode );
		
		// verifica qual a tecla utilizada para trocar o modo de entrada de texto (mai�sculo/min�sculo, t9 ou entrada 
		// normal) no aparelho.
		if ( ScreenManager.getSpecialKey( ScreenManager.KEY_STAR ) == ScreenManager.KEY_CHANGE_INPUT ) {
			keyInputChange = ScreenManager.KEY_STAR;
		} else {
			// considera a tecla # (jogo-da-velha) como tecla de troca de entrada de texto
			keyInputChange = ScreenManager.KEY_POUND;
		}
	} // fim do construtor EditLabel( ImageFont, String, int, byte )
	 
	
    /**
     * 
     * @param caret 
     */
	public void setCaret( Drawable caret ) {
		this.caret = caret;
	}
	 
	
    /**
     * 
     * @return 
     */
	public Drawable getCaret() {
		return caret;
	}
	 
	
	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		timeSinceLastKeyPress += delta;
		
		if ( lastKeyPressed == 0 ) {
			if ( currentArrayIndex >= 0 && timeSinceLastKeyPress >= KEY_WAIT_BEFORE_INSERT_TIME ) {
				insertChar( charTable[ currentArrayIndex ][ currentCharIndex ] );
				currentArrayIndex = -1;
//				keyReleased( 0 );
			}
		} else {
			if ( timeSinceLastKeyPress >= KEY_PRESS_TIME ) {
				switch ( lastKeyPressed ) {
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						// adiciona o algarismo num�rico correspondente � tecla
						insertChar( ( char ) ( '0' + lastKeyPressed - ScreenManager.KEY_NUM0 ) );
						
						keyReleased( -1 );
					break;
				} // fim switch ( lastKeyPressed )
			} // fim if ( timeSinceLastKeyPress >= KEY_PRESS_TIME )
		} // fim if ( lastKeyPressed != 0 )
		
		if ( caret != null && caretTimeInvisible > 0 ) {
			accTime += delta;
			
			if ( caret.isVisible() ) {
				if ( accTime >= caretTimeVisible ) {
					accTime -= caretTimeVisible;
					caret.setVisible( false );
				}
			} else {
				if ( accTime >= caretTimeInvisible ) {
					accTime -= caretTimeInvisible;
					caret.setVisible( true );
				}
			} // fim else ==> !caret.isVisible()
		} // fim if ( caret != null )
	} // fim do m�todo update( int )
	
	
	protected void insertChar( char c ) {
		if ( charCount < textChars.length ) {
			textChars[ caretPosition ] = c;
			
			++charCount;
			++caretPosition;
		}
	}
	
	
	public void backSpace() {
		if ( caretPosition > 0 ) {
			for ( int i = caretPosition - 1; i < charCount - 1; ++i ) {
				textChars[ i ] = textChars[ i + 1 ];
			}
			
			--charCount;
			--caretPosition;
		}
	}
	
	
	/**
	 * Define a velocidade do "pisca-pisca" do cursor.
	 * 
	 * @param caretTimeVisible tempo em milisegundos que o cursor permanece vis�vel.
	 * @param caretTimeInvisible tempo em milisegundos que o cursor permanece invis�vel. Valores menores ou iguais a
	 * zero significam que o cursor estar� sempre vis�vel.
	 */
	public void setCaretBlinkRate( int caretTimeVisible, int caretTimeInvisible ) {
		this.caretTimeVisible = caretTimeVisible;
		
		if ( caretTimeInvisible <= 0 && caret != null )
			caret.setVisible( true );
		
		this.caretTimeInvisible = caretTimeInvisible;
	} // fim do m�todo setCaretBlinkRate( int, int )

	
	/**
	 * 
	 * 
	 * @see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
		System.out.println( "keyReleased: "+ key );
		if ( lastKeyPressed > 0 ) {
			if ( key == lastKeyPressed ) {
				nextChar();
			} else {
				switch ( key ) {
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
						if ( key == keyInputChange )
							// TODO muda modo de entrada
							break;

					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						insertChar( charTable[ currentArrayIndex ][ currentCharIndex ] );
					break;

				} // fim switch ( key )
				lastKeyPressed = 0;
			}
		} // fim if ( lastKeyPressed > 0 )
	} // fim do m�todo keyReleased( 0 )

	
	/**
	 * 
	 * 
	 * @see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.RIGHT:
				if ( key != ScreenManager.KEY_NUM6 )
					setCaretPosition( caretPosition + 1 );
			return;
			
			case ScreenManager.LEFT:
				if ( key != ScreenManager.KEY_NUM4 )
					setCaretPosition( caretPosition - 1 );				
			return;
			
			case ScreenManager.KEY_CLEAR:
				backSpace();
			return;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )
		
		switch ( key ) {
			case ScreenManager.KEY_STAR:
				if ( key == keyInputChange )
					return;
				
				setCurrentArrayIndex( KEY_INDEX_STAR );
			break;
			
			case ScreenManager.KEY_POUND:
				if ( key == keyInputChange )
					return;
				
				setCurrentArrayIndex( KEY_INDEX_POUND );
			break;
			
			case ScreenManager.KEY_NUM0:
			case ScreenManager.KEY_NUM1:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM3:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.KEY_NUM7:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM9:
				if ( lastKeyPressed > 0 && key != lastKeyPressed )
					insertChar( charTable[ currentArrayIndex ][ currentCharIndex ] );
				
				setCurrentArrayIndex( KEY_INDEX_NUM0 + key - ScreenManager.KEY_NUM0 );
			break;

		} // fim switch ( key )		
		
		lastKeyPressed = key;
		timeSinceLastKeyPress = 0;		
	} // fim do m�todo keyPressed( int )
	
	
	/**
	 * Define a quantidade m�xima de caracteres do label.
	 * @param maxChars n�mero m�ximo de caracteres do label. Qualquer valor igual ou maior que zero � v�lido; valores 
	 * negativos s�o ignorados.
	 */
	public void setMaxChars( int maxChars ) {
		try {
			textChars = new char[ maxChars ];
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo setMaxChars( int )
	
	
	/**
	 * Posiciona o cursor no texto.
	 * @param position �ndice do cursor no texto.
	 */
	protected void setCaretPosition( int position ) {
		if ( position >= 0 && position <= charCount ) {
			// TODO posicionar cursor no texto
			caretPosition = position;
		}
	} // fim setCaretPosition( int )
	
	
	/**
	 * Define o modo de entrada de caracteres no label.
	 * 
	 * @param inputMode modo de entrada dos caracteres. Valores v�lidos:
	 * <ul>
	 * <li>INPUT_MODE_ANY: qualquer caracter pode ser digitado.</li>
	 * <li>INPUT_MODE_DECIMAL: somente caracteres de 0 a 9 podem ser digitados.</li>
	 * <li>INPUT_MODE_EMAIL: restringe os caracteres de forma a otimizar a escrita de endere�os de e-mail.</li>
	 * <li>INPUT_MODE_PASSWORD: qualquer caracter pode ser digitado, por�m n�o s�o exibidos. A m�scara que substitui
	 * os caracteres pode ser um caracter da fonte ou um Drawable. Por padr�o, � um caracter '*' (asterisco)</li>
	 * <li>INPUT_MODE_URL: restringe os caracteres de forma a otimizar a escrita de endere�os de internet.</li>
	 * </ul>*
	 */
	public void setInputMode( byte inputMode ) {
		switch ( inputMode ) {
			case INPUT_MODE_ANY:
			case INPUT_MODE_DECIMAL:
			case INPUT_MODE_EMAIL:
			case INPUT_MODE_PASSWORD:
			case INPUT_MODE_URL:
				this.inputMode = inputMode;
			break;
		} // fim switch ( inputMode )
	} // fim do m�todo setInputMode( byte )
	 
	
	public void setPasswordMask( Drawable mask ) {
		passwordDrawableMask = mask;
		
		if ( mask != null )
			passwordMaskType = PASSWORD_MASK_TYPE_DRAWABLE;
	}
	
	
	public void setPasswordMask( char mask ) {
		passwordMaskType = PASSWORD_MASK_TYPE_CHAR;
		passwordCharMask = mask;
	}
	

	protected void paint( Graphics g ) {
		int x = 0;
		int index = 0;
		
		g.setColor( 0x9999ff );
		g.fillRect( 0, 0, size.x, size.y );
		
		final int endX = g.getClipWidth();
		
		// desenha o texto antes do cursor
		for ( ; index < caretPosition && index < textChars.length && x < endX; ++index ) {
			drawChar( g, textChars[ index ], x, 0 );
			
			x += font.getCharWidth( textChars[ index ] );
		}
		
		// desenha o cursor
		if ( caret != null ) {
			caret.setPosition( x, 0 );
			
			caret.draw( g );
			
			x += caret.getSize().x;
		}
			
		
		// desenha o texto ap�s o cursor
		for ( ; index < charCount && index < textChars.length && x < endX; ++index ) {
			drawChar( g, textChars[ index ], x, 0 );
			
			x += font.getCharWidth( textChars[ index ] );
		}
		
		if ( currentArrayIndex >= 0 ) {
			g.setClip( 0, 0, 2000, 2000 );
			g.setFont( Font.getFont( Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM ) );
			g.setColor( 0xffffff );
			g.drawString( "" + charTable[ currentArrayIndex ][ currentCharIndex ], 0, 30, 0 );
		}
	}
	
	
	protected void setCurrentArrayIndex( int arrayIndex ) {
		if ( arrayIndex != currentArrayIndex && arrayIndex >= 0 && arrayIndex < TOTAL_KEYS ) {
			currentArrayIndex = ( byte ) arrayIndex;
			currentCharIndex = 0;
		}
	}
	
	
	protected void nextChar() {
		++currentCharIndex;
		if ( currentCharIndex >= charTable[ currentArrayIndex ].length )
			currentCharIndex = 0;
	}
	
}
 
