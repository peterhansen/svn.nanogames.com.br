/*
 * MenuListener.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 *
 * @author peter
 */
public interface MenuListener {
 
	
	/**
	 * M�todo chamado quando uma op��o de um menu � selecionada.
	 * @param id identifica��o do menu.
	 * @param index �ndice da op��o selecionada.
	 */
	public abstract void onChoose( int id, int index );
	
	
	/**
	 * M�todo chamado quando o item selecionado atualmente no menu � trocado.
	 * @param id identifica��o do menu.
	 * @param index �ndice do novo item.
	 */
	public abstract void itemChanged( int id, int index );
	
	
}
 
