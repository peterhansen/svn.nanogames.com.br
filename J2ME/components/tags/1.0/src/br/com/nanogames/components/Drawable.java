/*
 * Drawable.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

/**
 *
 * @author peter
 */
public abstract class Drawable {
 
    // posi��o do drawable
	protected final Point position = new Point();
	 
    // indica se o drawable est� vis�vel
	protected boolean visible = true;
	 
    // dimens�es do drawable (utilizado para fins de detec��o de colis�o e para definir �rea de clip inicial)
	protected final Point size = new Point();
    
    // pixel de refer�ncia (em rela��o ao tamanho do drawable, e n�o � sua posi��o)
	protected Point referencePixel = new Point();
	 
    // bounding box considerado para a colis�o (padr�o: 0, 0, size.x, size.y)
	protected Rectangle collisionArea = new Rectangle();
	 
    // transforma��o aplicada ao drawable (padr�o: TRANS_NONE). Pode haver m�ltiplas transforma��es (espelhamento
    // horizontal e rota��o de 90�, por exemplo), atrav�s do uso do operador matem�tico OU: |
	protected int transform;
	
	// transforma��o passada �s classes MIDP que utilizam transforma��o (valores para as mesmas transforma��es s�o diferentes)
	protected int transformMIDP;
    
    // nenhuma transforma��o
	public static final byte TRANS_NONE = 0;    
    // espelhamento vertical
	public static final byte TRANS_MIRROR_V = 1;	
    // espelhamento horizontal
	public static final byte TRANS_MIRROR_H = 2;
    // rota��o de 90� no sentido hor�rio
	public static final byte TRANS_ROT90 = 4;
    // rota��o de 180�
	public static final byte TRANS_ROT180 = 8;
    // rota��o de 270� no sentido hor�rio
	public static final byte TRANS_ROT270 = 16;
	
	protected static final byte TRANS_MASK_MIRROR = ( byte ) ( TRANS_MIRROR_H | TRANS_MIRROR_V );
	protected static final byte TRANS_MASK_ROTATE = ( byte ) ( TRANS_ROT90 | TRANS_ROT180 | TRANS_ROT270 );
	protected static final byte TRANS_MASK_ALL = ( byte ) ( TRANS_MASK_MIRROR | TRANS_MASK_ROTATE );
    
    // pilha de �reas de clip usada pelos drawables no momento do desenho
	private static final byte MAX_CLIP_STACK_SIZE = 32;
    protected static final Rectangle[] clipStack = new Rectangle[ MAX_CLIP_STACK_SIZE ];
	
	protected static byte currentStackSize;
	
	
	public static final Point translate = new Point();
	
	
	static {
		for ( int i = 0; i < MAX_CLIP_STACK_SIZE; ++i )
			clipStack[ i ] = new Rectangle();
	}

	 
    /**
     * 
     * @param visible 
     */
	public void setVisible( boolean visible ) {
        this.visible = visible;
	}
	
    
    /**
     * 
     * @return 
     */
	public boolean isVisible() {
		return visible;
	}
	
    
    /**
     * Posiciona o ponto top-left (0,0) do drawable na posi��o (x,y). Esse m�todo n�o leva em considera��o o ponto de 
	 * refer�ncia do drawable; para posicionar o ponto de refer�ncia em (x,y), deve-se utilizar o m�todo <b>setRefPixelPosition</b>.
     * @param x posi��o horizontal onde o ponto mais � esquerda do drawable ser� posicionado.
     * @param y posi��o vertical onde o ponto mais superior do drawable ser� posicionado.
	 * @see #setPosition(Point)
	 * @see #setRefPixelPosition(int, int)
	 * @see #setRefPixelPosition(Point)
     */
	public void setPosition( int x, int y ) {
        position.set( x, y );
	}
	
	
	/**
	 * Posiciona o ponto top-left (0,0) do drawable na posi��o do Point recebido como par�metro. Esse m�todo n�o leva em 
	 * considera��o o ponto de refer�ncia do drawable; para posicionar o ponto de refer�ncia em (x,y), deve-se utilizar 
	 * o m�todo <b>setRefPixelPosition</b>. A chamada desse m�todo equivale a <b>setPosition( p.x, p.y )</b>.
	 * @param p ponto nova posi��o top-left do drawable.
	 * @see #setPosition(int,int)
	 * @see #setRefPixelPosition(int, int)
	 * @see #setRefPixelPosition(Point)
	 */
	public final void setPosition( Point p ) {
		setPosition( p.x, p.y );
	}
	
    
    /**
     * Retorna a posi��o top-left do drawable.
     * @return refer�ncia para a posi��o do drawable.
     */
	public Point getPosition() {
		return position;
	}
	
    
    /**
     * Realiza as opera��es de clip e transla��o do drawable antes das chamadas efetivas de desenho no Graphics, levando
	 * em considera��o a �rea de clip e posi��o do grupo "pai" do drawable, de forma que a �rea de clip n�o seja ultrapassada. 
	 * <p>Esse m�todo n�o executa chamadas diretas de desenho no Graphics; elas devem ser feitas no m�todo <b>paint</b>. Ap�s a
	 * chamada de paint, a �rea anterior de clip e a posi��o de desenho do grupo s�o restauradas.</p>
     * @param g Graphics onde ser� desenhado o drawable.
	 * @see #paint(Graphics)
     */
	public void draw( Graphics g ) {
		if ( visible ) {
			pushClip( g );
			translate.add( position );
			
			g.clipRect( translate.x, translate.y, size.x, size.y );
			paint( g );

			translate.sub( position );
			popClip( g );
		} // fim if ( visible )        
	} // fim do m�todo draw( Graphics )
	
    
    /**
     * Move o drawable.
     * @param dx varia��o horizontal na posi��o do drawable.
     * @param dy varia��o vertical na posi��o do drawable.
	 * @see #move(Point)
     */
	public void move( int dx, int dy ) {
        position.add( dx, dy );
	}
    
	 
    /**
     * Move o drawable. A chamada deste m�todo equivale a <b>move( distance.x, distance.y )</b>.
     * @param distance dist�ncia (x,y) a ser percorrida pelo drawable. 
	 * @see #move(int, int)
     */
	public final void move( Point distance ) {
        move( distance.x, distance.y );
	}
    
	 
    /**
     * Obt�m o tamanho do drawable.
     * @return refer�ncia para o Point que define o tamanho do drawable.
     */
	public Point getSize() {
		return size;
	}
	 
    
    /**
     * Define o tamanho do drawable. Chamada equivalente a <b>setSize( size.x, size.y )</b>.
     * @param size tamanho (largura, altura) do drawable.
	 * @see #setSize(int, int)
     */
	public final void setSize( Point size ) {
        setSize( size.x, size.y );
	}
	
	
	/**
	 * Define o tamanho do drawable.
	 * 
	 * @param width largura em pixels do drawable.
	 * @param height altura em pixels do drawable.
	 * @see #setSize(Point)
	 */
	public void setSize( int width, int height ) {
		size.set( width, height );
	}
	 
    
	/**
	 * Trata apenas do desenho em si no Graphics, ou seja, chamadas de m�todos como g.drawImage(), g.drawRect() e etc. 
	 * Todas as opera��es de clip e/ou transla��o s�o executadas antes no m�todo draw( Graphics ).
	 * @see #draw(Graphics)
	 */
	protected abstract void paint( Graphics g );
    
    
    /**
     * Empilha a �rea de clip atual.
     * @param g Graphics cuja �rea de clip definida ser� empilhada.
     */
	public final void pushClip( Graphics g ) {
		clipStack[ currentStackSize++ ].set( g.getClipX(), g.getClipY(), g.getClipWidth(), g.getClipHeight() );
	}
	
    
    /**
     * Remove o topo da pilha de �reas de clip, e define a �rea de clip atual a partir dela.
     * @param g Graphics cuja �rea de clip ser� restaurada.
     */
	public final void popClip( Graphics g ) {
		final Rectangle clip = clipStack[ --currentStackSize ];
		g.setClip( clip.position.x, clip.position.y, clip.size.x, clip.size.y );
	}
	
    
    /**
     * Define o pixel de refer�ncia do drawable. Esse m�todo apenas define o ponto a ser utilizado como refer�ncia; ele n�o
	 * altera a posi��o atual do drawable.
     * @param refPixelX ponto x de refer�ncia do drawable.
     * @param refPixelY ponto y de refer�ncia do drawable.
	 * @see #defineReferencePixel(Point)
	 * @see #setRefPixelPosition(int,int)
	 * @see #setRefPixelPosition(Point)
     */
	public void defineReferencePixel( int refPixelX, int refPixelY ) {
        referencePixel.set( refPixelX, refPixelY );
	}
	 
    
    /**
     * Define o pixel de refer�ncia do drawable. Esse m�todo apenas define o ponto a ser utilizado como refer�ncia; ele n�o
	 * altera a posi��o atual do drawable. M�todo equivalente � chamada de <b>defineReferencePixel( refPixel.x, refPixel.y )</b>.
     * @param refPixel ponto de refer�ncia (x,y) do drawable.
	 * @see #defineReferencePixel(int,int)
	 * @see #setRefPixelPosition(int,int)
	 * @see #setRefPixelPosition(Point)
     */
	public final void defineReferencePixel( Point refPixel ) {
        defineReferencePixel( refPixel.x, refPixel.y );
	}
	 
    
    /**
     * Define a posi��o do ponto de refer�ncia a partir de um Point. M�todo equivalente � chamada de 
	 * <b>setRefPixelPosition( refPosition.x, refPosition.y )</b>.
     * @param refPosition posi��o (x,y) do ponto de refer�ncia.
	 * @see #setRefPixelPosition(int,int)
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(Point)
     */
	public final void setRefPixelPosition( Point refPosition ) {
        setRefPixelPosition( refPosition.x, refPosition.y );
	}
	
    
    /**
     * Define a posi��o do ponto do refer�ncia.
     * @param x posi��o x do ponto de refer�ncia.
     * @param y posi��o y do ponto de refer�ncia.
	 * @see #setRefPixelPosition(Point)
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(Point)
     */
	public void setRefPixelPosition( int x, int y ) {
        position.set( x - referencePixel.x, y - referencePixel.y );        
	}
	
    
    /**
     * Obt�m a posi��o do ponto de refer�ncia.
     * @return 
     */
	public Point getRefPixelPosition() {
		return new Point( position.x + referencePixel.x, position.y + referencePixel.y );
	}
	
    
    /**
     * Obt�m a posi��o x do ponto de refer�ncia.
     * @return 
     */
	public int getRefPixelX() {
		return position.x + referencePixel.x;
	}
	 
    
    /**
     * Obt�m a posi��o y do ponto de refer�ncia.
     * @return 
     */
	public int getRefPixelY() {
		return position.y + referencePixel.y;
	}
	 
    
    /**
     * 
     * @param d 
     * @return 
     */
	public boolean intersects( Drawable d ) {
		return collisionArea.intersects( d.collisionArea );
	}
    
	 
    /**
     * <b>N�O IMPLEMENTADO</b><br></br>
	 * Define a �rea de colis�o do drawable. Por padr�o, essa �rea corresponde ao tamanho do drawable.
     * @param x 
     * @param y 
     * @param width 
     * @param height 
	 * @see #defineCollisionArea(Rectangle)
     */
	public void defineCollisionArea( int x, int y, int width, int height ) { // TODO
        collisionArea.set( x, y, width, height );
	}
	 
    
    /**
     * <b>N�O IMPLEMENTADO</b><br></br>
	 * Define a �rea de colis�o do drawable. Por padr�o, essa �rea corresponde ao tamanho do drawable.
	 * Chamada equivalente a <b>defineCollisionArea( area.position.x, area.position.y, area.size.x, area.size.y )</b>.
     * @param area 
	 * @see #defineCollisionArea(int, int, int, int)
	 */
	public final void defineCollisionArea( Rectangle area ) {
        defineCollisionArea( area.position.x, area.position.y, area.size.x, area.size.y );
	}
    
	 
    /**
     * Define a transforma��o atual do drawable, realizando opera��es de rota��o e/ou espelhamento, conforme necess�rio.
     * @param transform 
     * @return 
	 * @see #rotate(int)
	 * @see #mirror(int)
     */
	public boolean setTransform( int transform ) {
		final int mirrorState = getMirror();
		if ( ( mirrorState & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) )
			mirror( TRANS_MIRROR_H );

		if ( ( mirrorState & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) )
			mirror( TRANS_MIRROR_V );
		
		final int newRotation = transform & TRANS_MASK_ROTATE;
		if ( getRotation() != newRotation ) {
			switch ( getRotation() ) {
				case TRANS_NONE:
					rotate( newRotation );
				break;
				
				case TRANS_ROT90:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT270 );
						break;
						
						case TRANS_ROT180:
							rotate( TRANS_ROT90 );
						break;

						case TRANS_ROT270:
							rotate( TRANS_ROT180 );
						break;
					} // fim switch ( newRotation )
				break;

				case TRANS_ROT180:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT180 );
						break;
						
						case TRANS_ROT90:
							rotate( TRANS_ROT270 );
						break;

						case TRANS_ROT270:
							rotate( TRANS_ROT90 );
						break;
					} // fim switch ( newRotation )
				break;

				case TRANS_ROT270:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT90 );
						break;
						
						case TRANS_ROT90:
							rotate( TRANS_ROT180 );
						break;

						case TRANS_ROT180:
							rotate( TRANS_ROT270 );
						break;
					} // fim switch ( newRotation )
				break;
			} // fim switch ( getRotation() )
		} // fim if ( getRotation() != newRotation )

		return false;
	}
	 
    
    /**
     * 
     * @return 
     */
	public int getTransform() {
		return transform;
	}
	
	
	/**
	 * Espelha o drawable em torno de um de seus eixos.
	 * @param mirrorType tipo do espelhamento. Os valores v�lidos s�o:
	 * <ul>
	 * <li>TRANS_MIRROR_H: espelha em torno do eixo vertical, ou seja, esquerda e direita s�o invertidos.</li>
	 * <li>TRANS_MIRROR_V: espelha em torno do eixo horizontal, ou seja, cima e baixo s�o invertidos.</li>
	 * <li>TRANS_MIRROR_H | TRANS_MIRROR_V: espelha em ambos os eixos (opera��o equivalente a uma rota��o de 180�)</li>
	 * </ul>
	 * @return boolean indicando se a opera��o � suportada pelo drawable.
	 * @see #setTransform(int)
	 */
	public boolean mirror( int mirrorType ) {
		mirrorType &= TRANS_MASK_MIRROR;
		final int newMirrorStatus = ( transform & TRANS_MASK_MIRROR ) ^ mirrorType;
		
		// se houver mudan�a no espelhamento horizontal, atualiza o pixel de refer�ncia
		if ( ( newMirrorStatus ^ ( transform & TRANS_MIRROR_H ) ) != TRANS_NONE )
			defineReferencePixel( size.x - referencePixel.x, referencePixel.y );
		
		// se houver mudan�a no espelhamento vertical, atualiza o pixel de refer�ncia
		if ( ( newMirrorStatus ^ ( transform & TRANS_MIRROR_V ) ) != TRANS_NONE )
			defineReferencePixel( referencePixel.x, size.y - referencePixel.y );						

		// a transforma��o passa a ser a uni�o da rota��o acumulada com o novo estado de espelhamento
		transform = ( transform & TRANS_MASK_ROTATE ) | newMirrorStatus;
		updateTransform();
		
		return false;
	} // fim do m�todo mirror( int )
	
	
	/**
	 * Rotaciona o drawable, utilizando como piv� da rota��o o seu pixel de refer�ncia.
	 * @param rotationType �ngulo da rota��o. Valores v�lidos:
	 * <ul>
	 * <li>TRANS_ROT90: rotaciona 90� no sentido hor�rio (270� no sentido anti-hor�rio).</li>
	 * <li>TRANS_ROT180: rotaciona o drawable 180�.</li>
	 * <li>TRANS_ROT270: rotaciona 270� no sentido hor�rio (90� no sentido anti-hor�rio).</li>
	 * </ul>
	 * @return boolean indicando se a opera��o de rota��o � suportada pelo drawable.
	 * @see #setTransform(int)
	 */
	public boolean rotate( int rotationType ) {
		rotationType &= TRANS_MASK_ROTATE;
		
		final Point previousRef = new Point( getRefPixelPosition() );
		
		switch ( rotationType ) {
			case TRANS_ROT90:
				defineReferencePixel( size.y - referencePixel.y, referencePixel.x );
				setSize( size.y, size.x );
			break;
			
			case TRANS_ROT180:
				defineReferencePixel( size.x - referencePixel.x, size.y - referencePixel.y );				
			break;
			
			case TRANS_ROT270:
				defineReferencePixel( referencePixel.y, size.x - referencePixel.x );				
				setSize( size.y, size.x );
			break;
			
			default:
				return true;
		} // fim switch ( rotationType )
		
		// TODO atualizar �rea de colis�o ap�s rota��o de 90� ou 270�
		int newRotation = transform & TRANS_MASK_ROTATE;		
		
		switch ( newRotation ) {
			case TRANS_NONE:
				newRotation = rotationType;
			break;
			
			case TRANS_ROT90:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_ROT180;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_ROT270;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_NONE;
					break;
				} // fim switch ( rotationType )
			break;

			case TRANS_ROT180:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_ROT270;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_NONE;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_ROT90;
					break;
				} // fim switch ( rotationType )				
			break;

			case TRANS_ROT270:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_NONE;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_ROT90;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_ROT180;
					break;
				} // fim switch ( rotationType )				
			break;
			
			default:
				return false;
		} // fim switch ( newRotation )
		
		setPosition( previousRef.x - referencePixel.x, previousRef.y - referencePixel.y );
		
		transform = ( transform & TRANS_MASK_MIRROR ) | newRotation;
		updateTransform();
		
		return false;
	} // fim do m�todo rotate( int )
	
	
	public int getMirror() {
		return transform & TRANS_MASK_MIRROR;
	}
	
	
	public int getRotation() {
		return transform & TRANS_MASK_ROTATE;
	}
	
	
	protected final void updateTransform() {
		switch ( transform ) {
			case TRANS_NONE:
			case TRANS_ROT180 | TRANS_MIRROR_H | TRANS_MIRROR_V:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_NONE;
			break;
			
			case TRANS_MIRROR_H:
			case TRANS_MIRROR_V | TRANS_ROT180:	
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR;
			break;
			
			case TRANS_MIRROR_V:
			case TRANS_MIRROR_H | TRANS_ROT180:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT180;
			break;			
			
			case TRANS_ROT180:
			case TRANS_MIRROR_H | TRANS_MIRROR_V:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_ROT180;
			break;			

			case TRANS_ROT90:
			case TRANS_MIRROR_H | TRANS_MIRROR_V | TRANS_ROT270:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_ROT90;
			break;			
			
			case TRANS_ROT270:
			case TRANS_MIRROR_H | TRANS_MIRROR_V | TRANS_ROT90:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_ROT270;
			break;			
			
			case TRANS_MIRROR_H | TRANS_ROT270:
			case TRANS_MIRROR_V | TRANS_ROT90:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT90;
			break;			

			case TRANS_MIRROR_H | TRANS_ROT90:			
			case TRANS_MIRROR_V | TRANS_ROT270:
				transformMIDP = javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT270;
			break;			
		}
	} // fim do m�todo updateTransform()

	
	/**
	 * Obt�m a �rea de clip definida atual.
	 * @return refer�ncia para o topo da pilha de clip, ou null, caso a pilha esteja vazia.
	 * @see #pushClip(Graphics)
	 * @see #popClip(Graphics)
	 */
	public static final Rectangle getClip() {
		if ( currentStackSize > 0 )
			return clipStack[ currentStackSize - 1 ];
		
		return null;
	} // fim do m�todo getClip()

	
}
 
