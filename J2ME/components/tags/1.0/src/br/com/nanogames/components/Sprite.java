/*
 * Sprite.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;
import br.com.nanogames.components.Updatable;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class Sprite extends Drawable implements Updatable {
 
	// �ndice da sequ�ncia de frames atual
	protected int currentSequence;
	 
	// �ndice do frame atual
	protected int currentFrame;
	
	// conjunto de frames utilizados para realizar a anima��o
	protected final FrameSet frameSet;

	// tempo acumulado no frame atual
	protected int accTime;
	

    /**
     * 
     * @param set 
     * @return 
     */
	public Sprite( FrameSet set ) {
		frameSet = set;
		setSequence( 0 );
	}
	 

	/**
	 * Avan�a um frame na sequ�ncia atual.
	 */	
	public void nextFrame() {
		if ( currentFrame < frameSet.getTotalFrames( currentSequence ) - 1 )
			setFrame( currentFrame + 1 );
		else
			setFrame( 0 );
	}
	 
	
	/**
	 * Retrocede um frame na sequ�ncia atual.
	 */
	public void previousFrame() {
		if ( currentFrame > 0 )
			setFrame( currentFrame - 1 );
		else
			setFrame( frameSet.getTotalFrames( currentSequence ) - 1 );
	}
	 
	
	/**
	 * Define o frame atual do sprite.
	 * @param frame �ndice do frame do sprite.
	 */
	public void setFrame( int frame ) {
		currentFrame = frame;
	}
	
	 
	/**
	 * Testa colis�o com um sprite.
	 * @param s sprite com o qual ser� testada a colis�o
	 * @param pixelLevel <i>true</i> para realizar o teste pixel a pixel, e <i>false</i> para testar utilizando
	 * somente a bounding box do sprite.
	 * @return <i>true</i> caso haja colis�o, e <i>false</i> caso contr�rio.
	 */
	public boolean intersects( Sprite s, boolean pixelLevel ) {
		// TODO colis�o pixel a pixel
		return collisionArea.intersects( s.collisionArea );
	}
	 
	
	/**
	 * Atualiza a anima��o do sprite.
	 *@param delta intervalo em milisegundos considerado para a atualiza��o da anima��o.
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		accTime += delta;
		
		if ( frameSet.getTotalFrames( currentSequence ) > 1 && frameSet.getAnimTimePerFrame( currentSequence ) > 0 ) {
			if ( accTime >= frameSet.getAnimTimePerFrame( currentSequence ) ) {
				accTime -= frameSet.getAnimTimePerFrame( currentSequence );
				
				nextFrame();
				
				if ( currentFrame == 0 ) {
					// terminou uma sequ�ncia
					sequenceEnded( currentSequence );
				}
			} // fim if ( accTime >= frameSet.getAnimTimePerFrame( currentSequence ) )
		} // fim if ( frameSet.getTotalFrames( currentSequence ) > 1 && frameSet.getAnimTimePerFrame( currentSequence ) > 0 )
	} // fim do m�todo update( int )

	
	/**
	 * Desenha o sprite.
	 * @param g refer�ncia para o Graphics onde o sprite ser� desenhado.
	 */
	protected void paint( Graphics g ) {
		frameSet.draw( g, currentSequence, currentFrame, transformMIDP );
	}
	
	
	/**
	 * Define a sequ�ncia atual de anima��o do sprite. A sequ�ncia ser� iniciada a partir do frame de �ndice 0 (zero), e
	 * as dimens�es do sprite ser�o atualizadas de acordo com as dimens�es dos frames da nova sequ�ncia. Aten��o: o ponto 
	 * de refer�ncia n�o � alterado.
	 * @param sequence �ndice da sequ�ncia de anima��o.
	 */
	public void setSequence( int sequence ) {
		currentSequence = sequence;
		currentFrame = 0;
		accTime = 0;
		
		// quando o sprite est� rotacionado em 90� ou 270�, os valores de altura e largura s�o invertidos
		switch ( getRotation() ) {
			case TRANS_ROT90:
			case TRANS_ROT270:
				setSize( frameSet.getFrameHeight( sequence ), frameSet.getFrameWidth( sequence ) );
			break;
			
			default:
				setSize( frameSet.getFrameWidth( sequence ), frameSet.getFrameHeight( sequence ) );
		}
	} // fim do m�todo setSequence( int )
	 
	
	public int getSequenceFrames() {
		return frameSet.getTotalFrames( currentSequence );
	}
	
	
	public int getCurrentFrame() {
		return currentFrame;
	}
	
	
	public int getCurrentSequence() {
		return currentSequence;
	}

	
	/**
	 * M�todo chamado quando uma sequ�ncia de anima��o se encerrou. Por padr�o, nenhuma a��o � tomada. Sprites que possuem
	 * comportamento espec�fico ao terminar uma sequ�ncia (controlar o n�mero de vezes que a sequ�ncia � feita, por exemplo)
	 * devem estender esse m�todo.
	 * @param currentSequence �ndice da sequ�ncia encerrada.
	 */
	protected void sequenceEnded( int currentSequence ) {
	}
	
	
	public FrameSet getFrameSet() {
		return frameSet;
	}

	
}
 
