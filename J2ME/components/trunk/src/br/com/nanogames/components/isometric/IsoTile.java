package br.com.nanogames.components.isometric;

import br.com.nanogames.components.Drawable;
import javax.microedition.lcdui.Graphics;

public class IsoTile extends Drawable {

	public static final byte TILE_CLIP_FULL = 4;
	 
	public static final byte TILE_CLIP_LEFT_ONLY = 8;
	 
	private static final byte TILE_CLIP_RIGHT_ONLY = 16;
	 
	public static final int TILE_CLIP_MASK = TILE_CLIP_FULL | TILE_CLIP_LEFT_ONLY | TILE_CLIP_RIGHT_ONLY;
	 
	public static final byte DRAW_NORMAL = 1;
	 
	public static final byte DRAW_PARTIAL = 2;
	 
	public static final byte DRAW_MASK = DRAW_NORMAL | DRAW_PARTIAL;

	protected void paint(Graphics g) {
	}	
	
}
 
