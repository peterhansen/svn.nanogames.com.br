/*
 * Sprite_New.java
 *
 * Created on September 28, 2007, 5:11 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class Sprite_New extends Drawable implements Updatable {
	
	/** Extens�o de arquivo utilizada nos descritores de frameSets. */
	public static final String FRAMESET_DESCRIPTOR_EXTENSION = ".dat";
	
	/** Caracter utilizado para indicar para o leitor de descritores o in�cio de uma sequ�ncia de frames com dura��es
	 * fixas (o primeiro valor lido � a dura��o dos frames, e em seguida os �ndices da sequ�ncia). */
	public static final char FRAMESET_DESCRIPTOR_SEQUENCE_FIXED = 'f';
	
	/** Caracter utilizado para indicar para o leitor de descritores o in�cio de uma sequ�ncia de frames com dura��es
	 * vari�veis. Os valores presentes na sequ�ncia devem sempre obedecer a ordem �ndice - dura��o. */
	public static final char FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE = 'v';	
 
	/** Sequ�ncias dos �ndices dos frames que comp�em a anima��o. */
	protected final byte[][] sequences;
	
	/** Dura��o em milisegundos de cada frame. */
	protected final short[][] frameTimes;
	 
	/** Conjunto de frames. */
	protected final Image[] frames;
	
	/** Offset na posi��o de desenho de cada frame, causado pelas varia��es de tamanho de cada imagem. */
	protected final Point[] offsets;	
	
	/** �ndice da sequ�ncia de frames atual */
	protected byte sequenceIndex;
	 
	/** �ndice do frame atual na sequ�ncia. */
	protected byte frameSequenceIndex;
	
	/** �ndice real do frame atual (�ndice da imagem no array de frames). */
	protected byte frameRealIndex;
	
	/** tempo acumulado no frame atual */
	protected short accTime;
	
	/** Refer�ncia para o frame atual, para acelerar acessos futuros (em vez de referenciar o array o tempo todo) */
	protected Image frame;
	
	/** Dura��o do frame atual em milisegundos. */
	protected short frameTime;
	
	/** Refer�ncia para a sequ�ncia atual, para acelerar acessos futuros (em vez de referenciar o array o tempo todo) */
	protected byte[] sequence;	
	
	/** Listener que receber� os eventos de fim de sequ�ncia (pode ser nulo) */
	protected SpriteListener listener;
	
	/** Id do sprite, a ser passado para o listener poder identificar qual sprite o chamou */
	protected int id;
	
	
	// TODO tratar offset nos casos de rota��o e espelhamento
	
	
	/**
	 * 
	 * @param filenamePrefix 
	 * @throws java.lang.Exception
	 */
	public Sprite_New( String filenamePrefix ) throws Exception {
		this( new FrameReader( filenamePrefix ) );
	}
	
	
	/**
	 * 
	 * @param reader
	 * @throws java.lang.Exception
	 */
	private Sprite_New( FrameReader reader ) throws Exception {
		this( reader.frames, reader.offsets, reader.getSequences(), reader.getFrameTimes() );
	}
	
	
	/**
	 * Cria uma c�pia de um sprite.
	 * 
	 * @param s sprite a ser copiado. O novo sprite possuir� os mesmos frames, offsets de frames, sequ�ncias e dura��o
	 * de frames do original. Atributos como posi��o, pixel de refer�ncia, visibilidade e etc N�O s�o copiados.
	 */
	public Sprite_New( Sprite_New s ) {
		this( s.frames, s.offsets, s.sequences, s.frameTimes );
	}
	
	
	/**
	 * 
	 * @param frames
	 * @param offsets
	 * @param sequences
	 * @param frameTimes
	 * @throws java.lang.Exception
	 */
	private Sprite_New( Image[] frames, Point[] offsets, byte[][] sequences, short[][] frameTimes ) {
		this.frames = frames;
		this.offsets = offsets;
		this.sequences = sequences;
		this.frameTimes = frameTimes;
		
		setSequence( 0 );
		setSize( frame.getWidth() << 1, frame.getHeight() << 1 ); // TODO definir tamanho corretamente
	}
	

	protected void paint( Graphics g ) {
		if ( transform == TRANS_NONE )
			g.drawImage( frame, translate.x + offsets[ frameRealIndex ].x, translate.y + offsets[ frameRealIndex ].y, 0  );
//		else
//			g.drawRegion( currentFrame, translate.x, translate.y, imageSize.x, imageSize.y, transformMIDP, 0, 0, 0 ); TODO
	}
	

	public void update( int delta ) {
		// se o frame tiver dura��o 0 (zero), o controle da anima��o � feito externamente, atrav�s de chamadas expl�citas
		// a nextFrame, previousFrame e etc.
		if ( frameTime > 0 ) {
			accTime += delta;
			if ( accTime >= frameTime ) {
				// trocou de frame
				accTime -= frameTime;

				nextFrame();

				// se tiver finalizado uma sequ�ncia e houver um listener registrado para este sprite, o avisa que a sequ�ncia acabou
				if ( frameSequenceIndex == 0 && listener != null )
					listener.onSequenceEnded( id, sequenceIndex  );
			}
		}
	}

	
//	public boolean intersects( Sprite_New s ) {
//		return currentFrame.collidesWith( s.currentFrame ); // TODO getCurrentFrame
//	}
	
	
	/**
	 * Avan�a um frame na sequ�ncia atual.
	 * 
	 * @see #previousFrame()
	 * @see #setFrame(int)
	 */	
	public void nextFrame() {
		if ( frameSequenceIndex < sequence.length - 1 )
			setFrame( frameSequenceIndex + 1 );
		else
			setFrame( 0 );
	}
	 
	
	/**
	 * Retrocede um frame na sequ�ncia atual.
	 * 
	 * @see #nextFrame()
	 * @see #setFrame(int)
	 */
	public void previousFrame() {
		if ( frameSequenceIndex > 0 )
			setFrame( frameSequenceIndex - 1 );
		else
			setFrame( sequence.length - 1 );
	}
	 
	
	/**
	 * Define o frame atual do sprite.
	 * 
	 * @param index �ndice do frame do sprite na sequ�ncia de anima��o atual.
	 * @see #nextFrame()
	 * @see #previousFrame()
	 */
	public void setFrame( int index  ) {
		frameSequenceIndex = ( byte ) index;
		frameRealIndex = sequence[ frameSequenceIndex ];
		frame = frames[ frameRealIndex ];
		frameTime = frameTimes[ sequenceIndex ][ frameSequenceIndex ];
	}	
	
	
	/**
	 * Define a sequ�ncia atual de anima��o do sprite. A sequ�ncia ser� iniciada a partir do frame de �ndice 0 (zero), e
	 * as dimens�es do sprite ser�o atualizadas de acordo com as dimens�es dos frames da nova sequ�ncia. Aten��o: o ponto 
	 * de refer�ncia n�o � alterado.
	 * @param sequence �ndice da sequ�ncia de anima��o.
	 */
	public void setSequence( int sequence ) {
		sequenceIndex = ( byte ) sequence;
		this.sequence = sequences[ sequenceIndex ];
		setFrame( 0 );
		accTime = 0;
	} // fim do m�todo setSequence( int )
	
	
	public final byte getSequence() {
		return sequenceIndex;
	}
	
	
	/**
	 * Obt�m o �ndice do frame atual na sequ�ncia.
	 * 
	 * @return �ndice do frame atual na sequ�ncia.
	 */
	public final byte getFrameSequenceIndex() {
		return frameSequenceIndex;
	}

	
	public final Image getFrame() {
		return frame;
	}
	
	
	/**
	 * Define o listener que ter� seu m�todo <i>sequenceEnded</i> chamado quando uma sequ�ncia do sprite terminar.
	 *
	 * @param listener refer�ncia para o listener do sprite. Passar <i>null</i> remove o listener anterior.
	 * @param id identifica��o do sprite, que ser� passada para o listener identificar qual sprite teve uma sequ�ncia encerrada.
	 */
	public final void setListener( SpriteListener listener, int id ) {
		this.listener = listener;
		this.id = id;
	}	
	

	//<editor-fold defaultstate="collapsed" desc="Classe interna FrameReader">
	
	private static final class FrameReader implements Serializable {
		
		private static final byte READ_STATE_FRAMES_OFFSETS			= 0;
		private static final byte READ_STATE_FIXED_READ_TIME		= 1;
		private static final byte READ_STATE_FIXED_READ_INDEXES		= 2;
		private static final byte READ_STATE_VARIABLE_READ_INDEX	= 3;
		private static final byte READ_STATE_VARIABLE_READ_TIME		= 4;
		
		private byte readState = READ_STATE_FRAMES_OFFSETS;
		
		/** Offset na posi��o de cada frame (valor tempor�rio). */
		private Point[] offsets;
		
		private final Vector indexes = new Vector();
		private final Vector times = new Vector();
		private final Vector sequenceIndexVector = new Vector();		
		private final Vector sequenceTimeVector = new Vector();		
		private final StringBuffer buffer = new StringBuffer();
		
		private final Image[] frames;
		
		
		private FrameReader( String framesFilenamePrefix ) throws Exception {
			AppMIDlet.openJarFile( framesFilenamePrefix + ".dat", this  );
			
			final int TOTAL_FRAMES = offsets.length;
			frames = new Image[ offsets.length ];
			final String PREFIX = framesFilenamePrefix + '_';
			for ( int i = 0; i < TOTAL_FRAMES; ++i ) {
				// aloca os frames
				frames[ i ] = Image.createImage( PREFIX + i + ".png" );
			}
		}
		
		
		public final void write( DataOutputStream output ) throws Exception {
		}

		
		public final void read( DataInputStream input ) throws Exception {
			try {
				while ( true ) {
					final char c = ( char ) input.readUnsignedByte();
					
					switch ( c ) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							buffer.append( c );
						break;
						
						case FRAMESET_DESCRIPTOR_SEQUENCE_FIXED:
							stateEnd();
							setReadState( READ_STATE_FIXED_READ_TIME );
						break;
						
						case FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE:
							stateEnd();
							times.removeAllElements();
							indexes.removeAllElements();
							
							setReadState( READ_STATE_VARIABLE_READ_INDEX );
						break;						
						
						default:
							if ( buffer.length() > 0 ) {
								// terminou de ler um valor
								switch ( readState ) {
									case READ_STATE_FRAMES_OFFSETS:
										insertValueFromBuffer( indexes );
									break;
									
									case READ_STATE_FIXED_READ_TIME:
										insertValueFromBuffer( times  );
										setReadState( READ_STATE_FIXED_READ_INDEXES );
									break;
									
									case READ_STATE_FIXED_READ_INDEXES:
										insertValueFromBuffer( indexes );
									break;
									
									case READ_STATE_VARIABLE_READ_INDEX:
										insertValueFromBuffer( indexes );
										setReadState( READ_STATE_VARIABLE_READ_TIME );
									break;
									
									case READ_STATE_VARIABLE_READ_TIME:
										insertValueFromBuffer( times  );
										setReadState( READ_STATE_VARIABLE_READ_INDEX );
									break;
								}
							} // fim if ( buffer.length() > 0 )
						// fim default
					} // fim switch ( c )
				} // fim while ( true )
			} catch ( EOFException eof ) {
				// leu todos os valores presentes no arquivo
				stateEnd();
				
				//#if DEBUG == "true"
				// no modo debug, lan�a exce��o caso o leitor termine num estado intermedi�rio, o que indica erro no 
				// arquivo descritor de frames.
				switch ( readState ) {
					case READ_STATE_FIXED_READ_TIME:
					case READ_STATE_VARIABLE_READ_TIME:
					throw new Exception( "Erro no descritor de arquivos: fim detectado no estado de leitura #" + readState + "." );
				}
				//#endif
			}
		}
		
		
		private final void setReadState( int readState ) {
			switch ( readState ) {
				case READ_STATE_FIXED_READ_TIME:
					times.removeAllElements();
				case READ_STATE_FIXED_READ_INDEXES:
					indexes.removeAllElements();
				break;
			}
			
			this.readState = ( byte ) readState;
		}
		
		
		private final void stateEnd() {
			switch ( readState ) {
				case READ_STATE_FRAMES_OFFSETS:
					offsets = new Point[ indexes.size() >> 1 ];
					for ( int i = 0; i < offsets.length; ++i ) {
						offsets[ i ] = new Point( ( ( Integer ) indexes.elementAt( i << 1 ) ).intValue(), 
												( ( Integer ) indexes.elementAt( ( i << 1 ) + 1 ) ).intValue() );
					}					
				break;
				
				case READ_STATE_FIXED_READ_INDEXES:
				case READ_STATE_VARIABLE_READ_INDEX:
					final byte[] sequence = new byte[ indexes.size() ];
					for ( byte i = 0; i < sequence.length; ++i ) {
						sequence[ i ] = ( ( Integer ) indexes.elementAt( i ) ).byteValue();
					}
					sequenceIndexVector.addElement( sequence );
					
					final short[] time = new short[ sequence.length ];
					if ( times.size() == time.length ) {
						for ( byte i = 0; i < sequence.length; ++i ) {
							time[ i ] = ( ( Integer ) times.elementAt( i ) ).shortValue();
						}
					} else {
						final short DEFAULT_TIME = ( ( Integer ) times.elementAt( 0 ) ).shortValue();
						for ( byte i = 0; i < sequence.length; ++i ) {
							time[ i ] = DEFAULT_TIME;
						}						
					}
					sequenceTimeVector.addElement( time );					
				break;
			}
			buffer.delete( 0, buffer.length() );
		} // fim do m�todo stateEnd()
		
		
		private final void insertValueFromBuffer( Vector vector ) {
			vector.addElement( Integer.valueOf( buffer.toString() ) );
			buffer.delete( 0, buffer.length() );
		}
		
		
		private final byte[][] getSequences() {
			final byte[][] sequences = new byte[ sequenceIndexVector.size() ][];

			for ( byte i = 0; i < sequences.length; ++i ) {
				sequences[ i ] = ( byte[] ) ( sequenceIndexVector.elementAt( i ) );
			}			
			
			return sequences;
		}
		
		
		private final short[][] getFrameTimes() {
			final short[][] frameTimes = new short[ sequenceIndexVector.size() ][];	
			
			for ( byte i = 0; i < frameTimes.length; ++i ) {
				frameTimes[ i ] = ( short[] ) ( sequenceTimeVector.elementAt( i ) );
			}

			return frameTimes;
		}
		
	}; // fim da classe interna FileHandler	
	
	//</editor-fold>	
	
}
