/*
 * YesNoScreen.java
 *
 * Created on May 7, 2007, 12:36 PM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class BasicConfirmScreen extends Menu {
	
	public static final byte INDEX_YES = 1;
	public static final byte INDEX_NO  = 2;
	
	private final RichLabel labelTitle;
	
	private final Label labelYes;
	private final Label labelNo;
	
	private final String title;
	
	private static final byte TOTAL_ITEMS = 5;
	
	
	/**
	 * Cria uma nova tela de confirma��o.
	 * 
	 * @param listener
	 * @param id
	 * @param font
	 * @param cursor 
	 * @param idTitle
	 * @param idYes 
	 * @param idNo 
	 * @throws java.lang.Exception
	 */
	public BasicConfirmScreen( MenuListener listener, int id, ImageFont font, Drawable cursor, int idTitle, int idYes, int idNo ) throws Exception {
        super( listener, id, TOTAL_ITEMS );
		
		// TODO adicionar novo tipo de tela de confirma��o: Sim/N�o/Cancelar (ou talvez estender funcionalidade,
		// de forma que a classe tamb�m possa ser usada como Alert)
		
		labelTitle = new RichLabel( font, "", ScreenManager.SCREEN_WIDTH * 9 / 10, null );
		insertDrawable( labelTitle );
		
		title = AppMIDlet.getText( idTitle );
		
		labelYes = new Label( font, AppMIDlet.getText( idYes ) );
		insertDrawable( labelYes );
		
		labelNo = new Label( font, AppMIDlet.getText( idNo ) );
		insertDrawable( labelNo );
		
		setCursor( cursor, CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );
		
		positionItems();
		
		setCurrentIndex( INDEX_NO );
	}
	
	
	public final void setCurrentIndex( int index ) {
		if ( index == 0 )
			index = 1;
		
		super.setCurrentIndex( index );
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				super.keyPressed( ScreenManager.KEY_NUM8 );
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				super.keyPressed( ScreenManager.KEY_NUM2 );
			break;
			
			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}
	
	
	public final void update( int delta ) {
		positionItems();
		
		super.update( delta );
	}
	
	
	private final void positionItems() {
		if ( size.x != ScreenManager.SCREEN_WIDTH || size.y != ScreenManager.SCREEN_HEIGHT ) {
			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			defineReferencePixel( size.x >> 1, size.y >> 1 );
			
			// deixa uma margem de cada lado da tela, para distribuir melhor o texto do t�tulo
			labelTitle.setText( title );
			labelTitle.setSize( labelTitle.getSize().x, labelTitle.getTextTotalHeight() );
			
			int y = ScreenManager.SCREEN_HALF_HEIGHT + ( labelNo.getSize().y << 1 );

			labelTitle.defineReferencePixel( labelTitle.getSize().x >> 1, labelTitle.getSize().y );
			labelTitle.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
			if ( labelTitle.getPosition().y < 0 ) {
				y -= labelTitle.getPosition().y;
				labelTitle.setPosition( labelTitle.getPosition().x, 0 );
			}

			labelYes.defineReferencePixel( labelYes.getSize().x, 0 );
			
			labelYes.setRefPixelPosition( ( size.x << 2 ) / 10, y );			
			labelNo.setPosition( size.x * 6 / 10, y );

			updateCursorPosition();
		}
	}
	
}
