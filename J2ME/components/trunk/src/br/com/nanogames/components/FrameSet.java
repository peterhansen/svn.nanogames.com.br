/*
 * FrameSet.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class FrameSet {
 
	/** N�mero total de sequ�ncias de frames. */
	protected int totalSequences;
	
	/** N�mero de sequ�ncias j� inseridas. */
	protected int activeSequences;
	 
	/** Dura��o em milisegundos de um frame de cada sequ�ncia. */
	protected final int[] frameTime;
	 
	/** Dimens�es em pixels de cada sequ�ncia de frames (a maior largura e altura dos frames de cada sequ�ncia � considerada). */
	protected final Point[] frameSize;
	 
	/** Frames de cada sequ�ncia. Cada frame corresponde a uma imagem. */
	protected final Image[][] frames;
	
	
	/**
	 * Cria uma nova inst�ncia de FrameSet.
	 * @param numSequences n�mero m�ximo de sequ�ncias de anima��o do conjunto de frames.
	 * @throws java.lang.Exception 
	 */
	public FrameSet( int numSequences ) throws Exception {
		totalSequences = numSequences;
		
		frameTime = new int[ numSequences ];
		
		frameSize = new Point[ numSequences ];
		for ( int i = 0; i < frameSize.length; ++i )
			frameSize[ i ] = new Point();
		
		frames = new Image[ numSequences ][];
	}
	
	
	/**
	 * Define a dura��o em milisegundos de uma sequ�ncia.
	 * @param sequence �ndice da sequ�ncia cuja dura��o dos frames ser� definida. Os valores v�lidos compreendem a 
	 * faixa de 0 (zero) ao n�mero total de sequ�ncias menos um.
	 * @param time dura��o em milisegundos da sequ�ncia de anima��o.
	 */
	public void setSequenceTime( int sequence, int time ) {
		frameTime[ sequence ] = time / getTotalFrames( sequence );
	}
	 
	
	/**
	 * Adiciona uma nova sequ�ncia de frames ao conjunto.
	 * @param frames conjunto de frames que comp�em uma sequ�ncia de anima��o.
	 * @param animTime dura��o total em milisegundos da sequ�ncia.
	 */
	public void addSequence( Image[] frames, int animTime ) {
		this.frames[ activeSequences ] = frames;
		frameTime[ activeSequences ] = animTime;
		
		// as dimens�es consideradas de cada sequ�ncia s�o formadas a partir da maior largura e maior altura dos frames
		// dessa sequ�ncia
		Point maxSize = new Point();
		for ( int i = 0; i < frames.length; ++i ) {
			if ( frames[ i ].getWidth() > maxSize.x )
				maxSize.x = frames[ i ].getWidth();
			
			if ( frames[ i ].getHeight() > maxSize.y )
				maxSize.y = frames[ i ].getHeight();
		} // fim for ( int i = 0; i < frames.length; ++i )
		
		frameSize[ activeSequences ].set( maxSize );
		
		++activeSequences;
	} // fim do m�todo addSequence( Image[], int )
	 
	
	/**
	 * Desenha um frame de uma sequ�ncia.
	 * @param g refer�ncia para o Graphics onde ser� desenhado o frame.
	 * @param sequence �ndice da sequ�ncia cujo frame ser� desenhado.
	 * @param frame �ndice do frame a ser desenhado.
	 */
	public void draw( Graphics g, int sequence, int frame ) {
		g.drawImage( frames[ sequence ][ frame ], 0, 0, 0 );
	}
	
	
	/**
	 * Desenha um frame de uma sequ�ncia.
	 * @param g refer�ncia para o Graphics onde ser� desenhado o frame.
	 * @param sequence �ndice da sequ�ncia cujo frame ser� desenhado.
	 * @param frame �ndice do frame a ser desenhado.
	 * @param transform 
	 */
	public void draw( Graphics g, int sequence, int frame, int transform ) {
		if ( transform == 0 )
			g.drawImage( frames[ sequence ][ frame ], Drawable.translate.x, Drawable.translate.y, 0 );
		else
			g.drawRegion( frames[ sequence ][ frame ], 0, 0, frames[ sequence ][ frame ].getWidth(), frames[ sequence ][ frame ].getHeight(), transform, Drawable.translate.x, Drawable.translate.y, 0 );
	}	
	 
	
	/**
	 * Obt�m a largura dos frames de uma sequ�ncia.
	 * @param sequence largura em pixels da sequ�ncia. O valor considerado � o da imagem com a maior largura da sequ�ncia,
	 * ou seja, o valor retornado � sempre maior ou igual � largura de cada frame da sequ�ncia.
	 * @return a largura em pixels da sequ�ncia, ou -1 caso o �ndice da sequ�ncia passado seja inv�lido.
	 */
	public int getFrameWidth( int sequence ) {
		try {
			return frameSize[ sequence ].x;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return -1;
		}
	}
	 
	
	/*
	 * Obt�m a altura dos frames de uma sequ�ncia.
	 * @param sequence altura em pixels da sequ�ncia. O valor considerado � o da imagem com a maior altura da sequ�ncia,
	 * ou seja, o valor retornado � sempre maior ou igual � altura de cada frame da sequ�ncia.
	 * @return a altura em pixels da sequ�ncia, ou -1 caso o �ndice da sequ�ncia passado seja inv�lido.
	 */
	public int getFrameHeight( int sequence ) {
		try {
			return frameSize[ sequence ].y;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return -1;
		}
	}
	 
	
	/**
	 * Obt�m a dura��o de cada frame de uma sequ�ncia em milisegundos.
	 * @param sequence �ndice da sequ�ncia cuja dura��o dos frames ser� obtida. Os valores v�lidos compreendem a 
	 * faixa de 0 (zero) ao n�mero total de sequ�ncias menos um.
	 * @return dura��o de cada frame, ou -1 caso o �ndice da sequ�ncia seja inv�lido.
	 */
	public int getAnimTimePerFrame( int sequence ) {
		try {
			return frameTime[ sequence ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return -1;
		}
	}
	 
	
	/**
	 * Obt�m o n�mero total de frames de uma sequ�ncia.
	 * @param sequence �ndice da sequ�ncia cujo n�mero total de frames ser� obtido. Os valores v�lidos compreendem a 
	 * faixa de 0 (zero) ao n�mero total de sequ�ncias menos um.
	 * @return quantidade de frames da sequ�ncia, ou -1 caso o �ndice da sequ�ncia seja inv�lido.
	 */
	public int getTotalFrames( int sequence ) {
		try {
			return frames[ sequence ].length;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return -1;
		}
	}
	 
	
	/**
	 * Obt�m a imagem que representa um frame de uma sequ�ncia.
	 * @param sequence �ndice da sequ�ncia cuja imagem ser� obtida. Os valores v�lidos compreendem a faixa de 0 (zero) ao
	 * n�mero total de sequ�ncias menos um.
	 * @param frame �ndice do frame cuja imagem ser� obtida. Valores v�lidos v�o de 0 (zero) ao n�mero total de frames da
	 * sequ�ncia menos um.
	 * @return imagem do frame, ou null caso o �ndice da sequ�ncia ou do frame estejam incorretos.
	 */
	public Image getFrame( int sequence, int frame ) {
		try {
			return frames[ sequence ][ frame ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
			
			return null;
		}
	}
	 
}
 
