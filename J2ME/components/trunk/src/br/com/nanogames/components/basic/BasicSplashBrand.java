/*
 * BasicSplashBrand.java
 * �2007 Nano Games
 *
 * Created on October 3, 2007, 12:01 PM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author peter
 */
public final class BasicSplashBrand extends DrawableGroup implements Updatable {
	
	private static final byte TOTAL_ITEMS = 3;
	
	/** tempo total que a tela da marca � exibida */
	private int transitionTime = 3000;

	private final MenuListener listener;
	
	private final int id;
	

	/**
	 * Aloca uma nova tela b�sica de splash de marca.
	 * 
	 * @param listener listener de menu que receber� o evento indicando o fim do tempo de exposi��o desta tela.
	 * @param id valor passado ao listener para indicar esta tela.
	 * @param bkgColor cor s�lida de preenchimento do fundo. Para n�o utilizar cor de preenchimento, basta passar valores
	 * negativos.
	 * @param splashPath caminho (diret�rio) da imagem e descritor da fonte utilizada nos cr�ditos da marca. Nesse diret�rio
	 * � necess�rio que existam os seguintes arquivos:
	 * <ul>
	 * <li>font_credits.png</li>
	 * <li>font_credits.dat</li>
	 * </ul>
	 * @param logoPath caminho completo da imagem utilizada como logo da marca.
	 * @param creditsTextIndex �ndice do texto de cr�ditos da marca.
	 * @throws java.lang.Exception
	 */
	public BasicSplashBrand( MenuListener listener, int id, int bkgColor, String splashPath, String logoPath, int creditsTextIndex ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		//#if DEBUG == "true"
//# 		if ( listener == null )
//# 			throw new Exception( "listener n�o pode ser null" );
		//#endif
		
		this.listener = listener;
		this.id = id;
		
		if ( bkgColor >= 0 ) {
			final Pattern bkg = new Pattern( null );
			bkg.setSize( size );
			bkg.setFillColor( bkgColor );
			insertDrawable( bkg );
		}
		
		final ImageFont font = ImageFont.createMultiSpacedFont( splashPath + "font_credits.png", splashPath + "font_credits.dat" );
		
		final DrawableImage logo = new DrawableImage( logoPath );
		logo.setPosition( ( size.x - logo.getSize().x ) >> 1, ( size.y - logo.getSize().y ) >> 1 );
		insertDrawable( logo );
		
		final RichLabel label = new RichLabel( font, AppMIDlet.getText( creditsTextIndex ), size.x * 9 / 10, null );
		label.setSize( label.getSize().x, label.getTextTotalHeight() );
		label.defineReferencePixel( label.getSize().x >> 1, label.getSize().y );
		label.setRefPixelPosition( size.x >> 1, size.y );
		
		insertDrawable( label );
		
		// ajusta a posi��o da logo, caso ela esteja sendo coberta pelo texto
		if ( logo.getPosition().y + logo.getSize().y > label.getPosition().y ) {
			// se a logo for maior que o espa�o livre, apenas a posiciona no topo da tela
			if ( logo.getSize().y >= label.getPosition().y )
				logo.setPosition( logo.getPosition().x, 0 );
			else
				logo.setPosition( logo.getPosition().x, ( label.getPosition().y - logo.getSize().y ) >> 1 );
		}		
		
	}

	
	public final void update( int delta ) {
		transitionTime -= delta;

		if ( transitionTime <= 0 )
			listener.onChoose( id, 0 );
	}
	
}
