/*
 * Menu.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class Menu extends UpdatableGroup implements KeyListener {
 
	/** �ndice atualmente selecionado no menu */
	protected int currentIndex;
	 
	/** indica se o menu � circular */
	protected boolean circular;
	 
	/** listener do menu. Esse listener receber� eventos cada vez que houver mudan�a no item atualmente selecionado ou
	 * quando o usu�rio confirmar uma sele��o no menu.
	 */
	protected final MenuListener listener;
	 
	/** id do menu. Esse valor � recebido como par�metro pelo construtor, e passado ao listener (caso exista) para que 
	 * este determine qual o menu o est� chamando, no caso do listener estar escutando v�rios menus simultaneamente.
	 */
	protected final int menuId;
	 
	/** drawable utilizado como cursor */
	protected Drawable cursor;
	
	/** caso o cursor seja atualiz�vel, mant�m uma segunda refer�ncia para ele, de forma a evitar chamadas desnecess�rias
	 * de instanceof e typecasts durante a atualiza��o do menu
	 */
	protected Updatable updatableCursor;
	
	
	// ordens de desenho dispon�veis para o cursor
	public static final byte CURSOR_DRAW_AFTER_MENU		= 0;
	public static final byte CURSOR_DRAW_BEFORE_MENU	= 1;
	
	protected byte cursorDrawOrder;

	/** regi�o do item atualmente selecionado utilizado como refer�ncia para o cursor se posicionar. Os valores v�lidos s�o 
	 * combina��es formadas pelo operador bin�rio OU ('|') de um tipo de alinhamento horizontal e outro vertical, 
	 * definidos na classe Graphics: LEFT, HCENTER, RIGHT e TOP, VCENTER, BOTTOM.
	 */
	protected int cursorAlignment;
	
	
	/**
	 * 
	 * @param listener 
	 * @param id 
	 * @param entries 
	 * @throws java.lang.Exception 
	 */
	public Menu( MenuListener listener, int id, int entries ) throws Exception {
		super( entries );
	
		this.listener = listener;
		this.menuId = id;
	} // fim do construtor Menu( MenuListener, int, int )	
	 
	
	public void nextIndex() {
		if ( currentIndex < activeDrawables - 1 )
			setCurrentIndex( currentIndex + 1 );
		else if ( circular )
			setCurrentIndex( 0 );
	}
	
	 
	public void previousIndex() {
		if ( currentIndex > 0 )
			setCurrentIndex( currentIndex - 1 );
		else if ( circular )
			setCurrentIndex( activeDrawables - 1 );
	}
	 
	
	public int getCurrentIndex() {
		return currentIndex;
	}
	 
	
	public void setCurrentIndex( int index ) {
		if ( index != currentIndex && index >= 0 && index < activeDrawables ) {
			currentIndex = index;
			
			if ( listener != null )
				listener.onItemChanged( menuId, index );
			
			updateCursorPosition();
		} // fim if ( index != currentIndex && index >= 0 && index < activeDrawables )
	} // fim do m�todo setCurrentIndex( int )
	 
	
	public boolean isCircular() {
		return circular;
	}
	 
	
	public void setCircular( boolean circular ) {
		this.circular = circular;
	}
	 
	
	/**
	 * Define qual � o drawable do grupo utilizado como cursor.
	 * 
	 * @param cursor drawable a ser utilizado como cursor.
	 * @param drawOrder ordem de desenho do cursor em rela��o ao menu. Valores v�lidos:
	 * <ul>
	 * <li>CURSOR_DRAW_AFTER_MENU: cursor � desenhado ap�s o menu.</li>
	 * <li>CURSOR_DRAW_BEFORE_MENU: cursor � desenhado antes do menu.</li>
	 * </ul>
	 * @param alignment indica que �rea da op��o selecionada deve ser tomada como refer�ncia para posicionamento do cursor.
	 * A posi��o final do cursor depende dessa �rea e do pixel de refer�ncia definido no cursor. Os valores v�lidos s�o 
	 * combina��es formadas pelo operador bin�rio OU ('|') de um tipo de alinhamento horizontal e outro vertical, 
	 * definidos na classe Graphics:
	 * <ul>
	 * <li>Graphics.LEFT: alinhado � esquerda.</li>
	 * <li>Graphics.HCENTER: centralizado horizontalmente.</li>
	 * <li>Graphics.RIGHT: horizontal � direita.</li>
	 * <br></br>
	 * <li>Graphics.TOP: alinhamento vertical no topo da linha.</li>
	 * <li>Graphics.VCENTER: centralizado verticalmente.</li>
	 * <li>Graphics.BOTTOM:	alinhamento vertical na parte inferior da linha.</li>
	 * </ul>
	 */
	public void setCursor( Drawable cursor, byte drawOrder, int alignment ) {
		this.cursor = cursor;
		if ( cursor instanceof Updatable )
			updatableCursor = ( Updatable ) cursor;
		else
			updatableCursor = null;
		
		switch ( drawOrder ) {
			case CURSOR_DRAW_BEFORE_MENU:
			case CURSOR_DRAW_AFTER_MENU:
				cursorDrawOrder = drawOrder;
			break;
			
			default:
				cursorDrawOrder = CURSOR_DRAW_AFTER_MENU;
		} // fim switch ( drawOrder )
		
		cursorDrawOrder = drawOrder;
		cursorAlignment = alignment;
		updateCursorPosition();
	} // fim do m�todo setCursor( Drawable, byte, int )
	
	
	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
		switch ( key ) {
			case Canvas.UP:
			case Canvas.KEY_NUM2:
				previousIndex();
			break;
			
			case Canvas.DOWN:
			case Canvas.KEY_NUM8:
				nextIndex();
			break;
			
			case Canvas.FIRE:
			case Canvas.KEY_NUM5:
				if ( listener != null )
					listener.onChoose( menuId, currentIndex );
			break;
		} // fim switch ( key )
	} // fim do m�todo keyPressed( int )


	/**
	 *@see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
	}


	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		super.update( delta );
		
		if ( updatableCursor != null )
			updatableCursor.update( delta );
	}

	
	protected void updateCursorPosition() {
		if ( cursor != null ) {
			final Drawable current = drawables[ currentIndex ];
			final Point refPoint = new Point();

            if ( ( cursorAlignment & Graphics.TOP ) != 0 ) {
				refPoint.y = current.getPosition().y;
			} else if ( ( cursorAlignment & Graphics.VCENTER ) != 0 ) {
				refPoint.y = current.getPosition().y + ( current.getSize().y >> 1 );
			} else {
				refPoint.y = current.getPosition().y + current.getSize().y;
			}

            if ( ( cursorAlignment & Graphics.LEFT ) != 0 ) {
				refPoint.x = current.getPosition().x;
			} else if ( ( cursorAlignment & Graphics.HCENTER ) != 0) {
				refPoint.x = current.getPosition().x + ( current.getSize().x >> 1 );
			} else {
				refPoint.x = current.getPosition().x + current.getSize().x;
			}

			cursor.setRefPixelPosition( refPoint );
		} // fim if ( cursor != null )
	} // fim do m�todo updateCursorPosition()

	
	protected void paint( Graphics g ) {
		if ( cursor != null && visible ) {
			switch ( cursorDrawOrder ) {
				case CURSOR_DRAW_AFTER_MENU:
					super.paint( g );
					cursor.draw( g );
				return;
				
				case CURSOR_DRAW_BEFORE_MENU:
					cursor.draw( g );
			} // fim switch ( cursorDrawOrder )
		} // fim if ( cursor != null && visible )
		super.paint( g );
	} // fim do m�todo paint( Graphics )

	
}
 
