/*
 * MarqueeLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class MarqueeLabel extends Label implements Updatable {
	
	/** velocidade padr�o de anima��o do texto */
	public static final byte DEFAULT_TEXT_SPEED = 40;
 
	/** velocidade da anima��o do texto, em pixels por segundo */
	protected int speed = DEFAULT_TEXT_SPEED;
	
	/** tempo padr�o de espera em milisegundos, no final de cada movimento (modo SCROLL_MODE_LEFT_RIGHT) */
	public static final short DEFAULT_WAIT_TIME = 600;
	
	/** tempo atual de espera */
	protected int waitTime = DEFAULT_WAIT_TIME;
	
	/** posi��o x atual do texto */
	protected int textX;
	
	/** tempo acumulado da anima��o do texto */
	protected int accTime;
	
	/** largura do texto */
	protected int textWidth;
	
	protected static final byte DIR_LEFT		= 0;
	protected static final byte DIR_RIGHT		= 1;
	protected static final byte DIR_STOP_LEFT	= 2;
	protected static final byte DIR_STOP_RIGHT	= 3;
	
	protected byte direction;

	// modos de scroll do texto
	/** texto rola para a esquerda at� o fim, e ent�o reaparece na direita */
	public static final byte SCROLL_MODE_LEFT = 0;
	/** texto rola para a direita at� o fim, e ent�o reaparece na esquerda */
	public static final byte SCROLL_MODE_RIGHT = 1;
	/** texto rola da direita para a esquerda; ao chegar no in�cio, inverte sentido, e segue assim indefinidamente */
	public static final byte SCROLL_MODE_LEFT_RIGHT = 2;
	/** p�ra a rolagem do texto */
	public static final byte SCROLL_MODE_NONE = 3;
	
	
	protected byte scrollMode = SCROLL_MODE_LEFT;
	
	protected int limitLeft;
	protected int limitRight;
	
	
	/**
	 * Cria um novo MarqueeLabel.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto do label.
	 * @param speed velocidade do label em pixels por segundo.
	 * @param scrollMode modo de movimenta��o do texto. Valores v�lidos:
	 * <ul>
	 * <li>SCROLL_MODE_LEFT: texto rola para a esquerda at� o fim, e ent�o reaparece na direita.</li>
	 * <li>SCROLL_MODE_RIGHT: texto rola para a direita at� o fim, e ent�o reaparece na esquerda.</li>
	 * <li>SCROLL_MODE_LEFT_RIGHT: texto rola da direita para a esquerda; ao chegar no in�cio, inverte sentido, e 
	 * segue assim indefinidamente.</li>
	 * </ul>
	 * @throws java.lang.Exception 
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String)
	 */
	public MarqueeLabel( ImageFont font, String text, int speed, byte scrollMode ) throws Exception {
		super( font, text );
		
		setSpeed( speed );
		setScrollMode( scrollMode );
	} // fim do construtor ( ImageFont, String, int, byte )
	
	 
	/**
	 * Cria um novo MarqueeLabel a partir da fonte e do texto recebidos, movendo o texto da direita para a esquerda com 
	 * velocidade padr�o.
	 * 
	 * @param font fonte utilizada para desenhar os caracteres do label.
	 * @param text texto do label.
	 * @throws java.lang.Exception 
	 * @see MarqueeLabel#MarqueeLabel(ImageFont, String, int, byte)
	 */
	public MarqueeLabel( ImageFont font, String text ) throws Exception {
		super( font, text );
	} // fim do construtor ( ImageFont, String )	
	 
	
	/**
	 * Define a velocidade do scroll do texto.
	 * @param speed velodidade do scroll do texto, em pixels por segundo.
	 */
	public void setSpeed( int speed ) {
		this.speed = Math.abs( speed );
		accTime = 0;
	} // fim do m�todo setSpeed( int )
	
	
	/**
	 * Define o modo de scroll do texto.
	 * @param mode modo de scroll do texto. Valores v�lidos:
	 * <ul>
	 * <li>SCROLL_MODE_LEFT: texto rola para a esquerda at� o fim, e ent�o reaparece na direita.</li>
	 * <li>SCROLL_MODE_RIGHT: texto rola para a direita at� o fim, e ent�o reaparece na esquerda.</li>
	 * <li>SCROLL_MODE_LEFT_RIGHT: texto rola da direita para a esquerda; ao chegar no in�cio, inverte sentido, e segue assim indefinidamente.</li>
	 * </ul>
	 */
	public void setScrollMode( byte mode ) {
		switch ( mode ) {
			case SCROLL_MODE_LEFT_RIGHT:
				direction = DIR_LEFT;
				textX = size.x;
				
				if ( textWidth > size.x ) {
					limitLeft = size.x - textWidth;
					limitRight = 0;
				} else {
					limitLeft = 0;
					limitRight = size.x - textWidth;
				}
			break;
			
			case SCROLL_MODE_LEFT:
				textX = -textWidth;
			break;
			
			case SCROLL_MODE_RIGHT:
				textX = size.x;
			break;
			
			case SCROLL_MODE_NONE:
				textX = 0;
			break;
			
			default:
				return;
		} // fim switch ( mode )
		
		scrollMode = mode;
	} // fim do m�todo setScrollMode( byte )
	
	
	public byte getScrollMode() {
		return scrollMode;
	}
	 
	
	public void update( int delta ) {
		switch ( scrollMode ) {
			case SCROLL_MODE_LEFT:
				accTime += delta;
				
				textX = size.x - ( accTime * speed ) / 1000;
				
				if ( textX < -textWidth ) {
					textX = size.x;
					accTime = 0;
				}
			break;
			
			case SCROLL_MODE_LEFT_RIGHT:
				accTime += delta;
				
				switch ( direction ) {
					case DIR_LEFT:
						textX = limitRight - ( accTime * speed ) / 1000;

						if ( textX <= limitLeft ) {
							textX = limitLeft;
							accTime = 0;
							direction = DIR_STOP_RIGHT;
						}
					break;
					
					case DIR_RIGHT:
						textX = limitLeft + ( accTime * speed ) / 1000;

						if ( textX >= limitRight ) {
							textX = limitRight;
							accTime = 0;
							direction = DIR_STOP_LEFT;
						}
					break;
					
					case DIR_STOP_LEFT:
						if ( accTime >= waitTime ) {
							accTime = 0;
							direction = DIR_LEFT;
						}
					break;
					
					case DIR_STOP_RIGHT:
						if ( accTime >= waitTime ) {
							accTime = 0;
							direction = DIR_RIGHT;
						}						
					break;
				} // fim switch ( direction )
			break;
			
			case SCROLL_MODE_RIGHT:
				accTime += delta;
				
				textX = -textWidth + ( ( accTime * speed ) / 1000 );
				
				if ( textX > size.x ) {
					accTime = 0;
					textX = -textWidth;
				}
			break;
		} // fim switch ( scrollMode )
	} // fim do m�todo update( int )
	
	
	public void setText( String text, boolean setSize ) {
		super.setText( text, setSize );
		
		// pr�-calcula a largura do texto, para otimizar a anima��o
		textWidth = font.getTextWidth( charBuffer );
		
		// recalcula a posi��o de in�cio e fim do scroll, caso necess�rio
		setScrollMode( scrollMode );
	}	
	
	
	/**
	 * Define o tempo de espera do texto ao terminar de rolar para um lado.
	 * @param waitTime tempo em milisegundos que o texto p�ra at� voltar a se mover.
	 */
	public void setWaitTime( int waitTime ) {
		if ( waitTime < 0 )
			waitTime = 0;
		
		this.waitTime = waitTime;
	} // fim do m�todo setWaitTime( int )
	
	
	public int getWaitTime() {
		return waitTime;
	}

	
	protected void paint( Graphics g ) {
		drawString( g, charBuffer, translate.x + textX, translate.y );
	}

	
	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		// recalcula a posi��o de in�cio e fim do scroll, caso necess�rio
		setScrollMode( scrollMode );
	} // fim do m�todo setSize( int, int )


	public void setTextOffset( int textOffset ) {
		textX = textOffset;
	}
	
	
	public int getSpeed() {
		return speed;
	}
	
}
 
