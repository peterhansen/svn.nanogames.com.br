/**
 * BasicOptionsScreen.java
 * �2007 Nano Games
 * 
 * Created on 11/12/2007 14:38:40 
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;

/**
 *
 * @author peter
 */
public final class BasicOptionsScreen extends BasicMenu {

	private final ImageFont font;
	
	private final byte INDEX_SOUND;
	private final byte INDEX_VIBRATION;
	
	private final String textSoundOn;
	private final String textSoundOff;
	private final String textVibrationOn;
	private final String textVibrationOff;
	
	
	/**
	 * Cria uma nova inst�ncia de um menu b�sico de op��es.
	 * @param listener
	 * @param font
	 * @param id
	 * @param entries
	 * @param backIndex 
	 * @param soundIndex �ndice da entrada relativa � op��o ligar/desligar som. Caso essa op��o n�o exista, basta
	 * passar valores negativos como argumento.
	 * @param soundOffText �ndice do texto de som desligado. Caso n�o exista a op��o de ligar/desligar som, esse
	 * valor � ignorado.
	 * @param vibrationIndex �ndice da entrada relativa � op��o ligar/desligar vibra��o. Caso essa op��o n�o 
	 * exista, basta passar valores negativos como argumento.
	 * @param vibrationOffText �ndice do texto de vibra��o desligada. Caso n�o exista a op��o de ligar/desligar 
	 * som, esse valor � ignorado.
	 * @throws java.lang.Exception
	 */
	public BasicOptionsScreen( MenuListener listener, ImageFont font, int id, int[] entries, int backIndex, int soundIndex, int soundOffText, int vibrationIndex, int vibrationOffText ) throws Exception {
		super( listener, id, font, entries, 0, 0, backIndex );
		
		this.font = font;

		INDEX_SOUND = ( byte ) soundIndex;
		if ( INDEX_SOUND >= 0 ) {
			textSoundOn = AppMIDlet.getText( entries[ INDEX_SOUND ] );
			textSoundOff = AppMIDlet.getText( soundOffText );
			
			updateText( INDEX_SOUND );
		} else {
			textSoundOff = null;
			textSoundOn = null;
		}
		
		INDEX_VIBRATION = ( byte ) vibrationIndex;
		if ( INDEX_VIBRATION >= 0 ) {
			textVibrationOn = AppMIDlet.getText( entries[ INDEX_VIBRATION] );		
			textVibrationOff = AppMIDlet.getText( vibrationOffText );
			
			updateText( INDEX_VIBRATION );
		} else {
			textVibrationOn = null;
			textVibrationOff = null;
		}
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.RIGHT:
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:				
				if ( currentIndex == INDEX_SOUND ) {
					toggleSound();
					return;
				} else if ( currentIndex == INDEX_VIBRATION ) {
					toggleVibration();
					return;
				}
				
			default:
				super.keyPressed( key );				
		} // fim switch ( key )					
	}
	
	
	private final void toggleSound() {
		final boolean previousOption = MediaPlayer.isMuted();
		MediaPlayer.setMute( !MediaPlayer.isMuted() );
		
		// caso a op��o tenha sido alterada, atualiza o texto de forma correspondente
		if ( previousOption != MediaPlayer.isMuted() )		
			updateText( INDEX_SOUND );
	}
	
	
	private final void toggleVibration() {
		final boolean previousOption = MediaPlayer.isVibration();
		MediaPlayer.setVibration( !MediaPlayer.isVibration() );
		
//		MediaPlayer.vibrate( VIBRATION_TIME_CAR_WRECKED ); TODO
		
		// caso a op��o tenha sido alterada, atualiza o texto de forma correspondente
		if ( previousOption != MediaPlayer.isVibration() )
			updateText( INDEX_VIBRATION );
	}


	private final void updateText( byte index ) {
		final Label l = ( Label ) getDrawable( index );
		
		if ( index == INDEX_SOUND ) {
			l.setText( MediaPlayer.isMuted() ? textSoundOff : textSoundOn );
		} else if ( index == INDEX_VIBRATION ) {
			l.setText( MediaPlayer.isVibration() ? textVibrationOn : textVibrationOff );
		}
		l.defineReferencePixel( l.getSize().x >> 1, 0 );
		l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
		
		// como a largura do texto pode mudar, atualiza a posi��o do cursor
		updateCursorPosition();		
	}
	
}
