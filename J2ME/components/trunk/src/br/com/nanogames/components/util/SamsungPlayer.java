/**
 * SamsungPlayer.java
 * �2008 Nano Games.
 *
 * Created on 18/01/2008 13:22:34.
 */

package br.com.nanogames.components.util;

import javax.microedition.media.Control;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.TimeBase;
import com.samsung.util.AudioClip;

/**
 * Essa classe implementa um tocador de sons que utiliza a API espec�fica de sons da Samsung (AudioClip) para tocar sons.
 * @author Peter
 */
public final class SamsungPlayer implements Player {

	/** Volume padr�o do AudioClip. TODO confirmar valor */
	private static final byte VOLUME = 100;
	
	/** AudioClip chamado para tocar os sons. */
	private final AudioClip audioClip;
	
	/** Contador de loops definido atualmente. */
	private byte loopCount = 1;
	
	
	public SamsungPlayer( String filename ) throws Exception {
		audioClip = new AudioClip( 0, filename );
	}
	

	public final void realize() throws MediaException {
	}


	public final void prefetch() throws MediaException {
	}


	public final void start() throws MediaException {
		audioClip.play( loopCount, VOLUME );
	}


	public final void stop() throws MediaException {
		audioClip.stop();
	}


	public final void deallocate() {
	}


	public final void close() {
	}


	public final void setTimeBase( TimeBase t ) throws MediaException {
	}


	public final TimeBase getTimeBase() {
		return null;
	}


	public final long setMediaTime( long mediaTime ) throws MediaException {
		return 0;
	}


	public final long getMediaTime() {
		return 0;
	}


	public final int getState() {
		return 0;
	}


	public final long getDuration() {
		return 0;
	}


	public final String getContentType() {
		return "";
	}


	public final void setLoopCount( int loopCount ) {
		this.loopCount = ( byte ) loopCount;
	}


	public final void addPlayerListener( PlayerListener arg0 ) {
	}


	public final void removePlayerListener( PlayerListener arg0 ) {
	}


	public final Control[] getControls() {
		return null;
	}


	public Control getControl( String arg0 ) {
		return null;
	}
	
}
