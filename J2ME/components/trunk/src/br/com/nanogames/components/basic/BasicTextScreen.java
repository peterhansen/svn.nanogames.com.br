/*
 * TextScreen.java
 *
 * Created on June 4, 2007, 3:53 PM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;


/**
 *
 * @author peter
 */
public final class BasicTextScreen extends RichLabel {
	
	private final short TEXT_MOVE_SPEED;
	
	// dire��es do scroll do texto
	private static final byte DIRECTION_NONE		= 0;
	private static final byte DIRECTION_FORWARD		= 1;
	private static final byte DIRECTION_BACKWARD	= 2;
	
	private byte direction = DIRECTION_NONE;	
	
	private final int textLimitTop;
	private final int textLimitBottom;
	
	private int lastSpeedModule;
	
	private final boolean autoScroll;
	
	private final MenuListener listener;
	
	private final int id;
	
	
	public BasicTextScreen( MenuListener listener, int id, ImageFont font, int textIndex, boolean autoScroll ) throws Exception {
		this( listener, id, font, textIndex, autoScroll, null );
	}	
	
	
	public BasicTextScreen( MenuListener listener, int id, ImageFont font, int textIndex, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( font, AppMIDlet.getText( textIndex ), ScreenManager.SCREEN_WIDTH, specialChars );
		
		// TODO adicionar barra de scroll
		
		this.listener = listener;
		this.id = id;
		
		setSize( size.x, ScreenManager.SCREEN_HEIGHT - font.getImage().getHeight() );
		
		this.autoScroll = autoScroll;
		if ( autoScroll ) {
			setDirection( DIRECTION_FORWARD );
			TEXT_MOVE_SPEED = ( short ) ( ( font.getImage().getHeight() * 3 ) >> 1 );
			textLimitTop = -getTextTotalHeight();
			textLimitBottom = size.y;
			
			achievedLimitTop();
		} else {
			TEXT_MOVE_SPEED = ( short ) ( ( font.getImage().getHeight() * 11 ) >> 1 );
			textLimitTop = ( -getTextTotalHeight() + size.y ) >= 0 ? 0 : ( -getTextTotalHeight() + size.y );
			textLimitBottom = 0;
		}
		
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:				
				setDirection( DIRECTION_NONE );
				
				listener.onChoose( id, 0 );
			return;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setDirection( DIRECTION_BACKWARD );
			break;
			
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setDirection( DIRECTION_FORWARD );				
			break;
			
			default:
				setDirection( DIRECTION_NONE );			
		} // fim switch ( key )
	} // fim do m�todo keyPressed( int )
	

	public final void keyReleased( int key ) {
		setDirection( DIRECTION_NONE );
	}	
	
	
	private final void setDirection( byte direction ) {
		if ( autoScroll )
			this.direction = DIRECTION_FORWARD;
		else
			this.direction = direction;
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		switch ( direction ) {
			case DIRECTION_BACKWARD:
			case DIRECTION_FORWARD:
				// TODO utilizar classe MUV
				final int ds = lastSpeedModule + ( ( direction == DIRECTION_BACKWARD ? TEXT_MOVE_SPEED : -TEXT_MOVE_SPEED ) * delta );
				final int dy = ds / 1000;
				lastSpeedModule = ds % 1000;
				
				if ( dy != 0 ) {
					setTextOffset( getTextOffset() + dy );
					
					if ( getTextOffset() >= textLimitBottom ) {
						setTextOffset( textLimitBottom );
						setDirection( DIRECTION_NONE );
					} else {
						if ( getTextOffset() <=  textLimitTop ) {
							achievedLimitTop();
						}
					}
				} // fim if ( dy != 0 )
			break;
			
			default:
				return;
		} // fim switch ( direction )		
	}
	
	
	private final void achievedLimitTop() {
		if ( autoScroll ) {
			setTextOffset( textLimitBottom );
		} else {
			setTextOffset( autoScroll ? textLimitBottom : textLimitTop );
			setDirection( DIRECTION_NONE );
		}
	}

	
}
