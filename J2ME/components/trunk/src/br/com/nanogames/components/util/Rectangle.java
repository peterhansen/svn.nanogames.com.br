/*
 * Rectangle.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
public class Rectangle {
 
    // posi��o (top-left) do ret�ngulo
	public final Point position = new Point();

    // dimens�es do ret�ngulo (largura, altura)
	public final Point size = new Point();
	 
    
	public Rectangle( Rectangle r ) {
		position.set( r.position );
        size.set( r.size );
	}
    
	 
	public Rectangle() {
	}
	 
    
	public Rectangle( int x, int y, int width, int height ) {
        set( x, y, width, height );
	}
    
	 
	public boolean contains( int x, int y ) {
		return x >= position.x && y >= position.y && ( x <= position.x + size.x ) && ( y <= position.y + size.y );
	}
	 
    
	public boolean contains( Point point ) {
		return contains( point.x, point.y );
	}
    
	 
	public void set( int x, int y, int width, int height ) {
        position.set( x, y );
        size.set( width, height );
	}
	 
    
	public void set( Rectangle r ) {
        position.set( r.position );
        size.set( r.size );
	}
    
	 
	public boolean intersects( Rectangle r ) {
		return    position.x <= ( r.position.x + r.size.x ) 
               && ( position.x + size.x ) >= r.position.x
			   && position.y <= ( r.position.y + r.size.y )
			   && ( position.y + size.y ) >= r.position.y;
	}
    
	 
	public Rectangle union( Rectangle r ) {
		return new Rectangle( Math.min( position.x, r.position.x ), Math.min( position.y, r.position.y ),
                              Math.max( position.x + size.x, r.position.x + r.size.x ), Math.max( position.y + size.y, r.position.y + r.size.y )  );
	}
	 
}
 
