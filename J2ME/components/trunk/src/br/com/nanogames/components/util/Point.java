/*
 * Point.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

/**
 *
 * @author peter
 */
package br.com.nanogames.components.util;

public class Point {
 
	/** Coordenada x do ponto. */
	public int x;
	
	/** Coordenada y do ponto. */
	public int y;
	 
	
	/**
	 * 
	 */
	public void set( int x, int y ) {
        this.x = x;
        this.y = y;
	}
	 
    
	public int distanceTo( Point point ) {
		return distanceTo( point.x, point. y );
	}
	
	
	public int distanceTo( int x, int y ) {
		return ( int ) NanoMath.sqrt( ( x - this.x ) * ( x - this.x ) + ( y - this.y ) * ( y - this.y ) );
	}
	 
    
	public final void set( Point point ) {
		set( point.x, point.y );
	}
	 
    
	public Point() {
	}
    
	 
	public Point( int x, int y ) {
		set( x, y );
	}
	 
    
	public Point( Point point ) {
		set( point );
	}
	 
    
	public void normalize() {
        divide( getModule() );
	}
	 
    
	public int dotProduct( Point p ) {
		return ( x * p.x ) + ( y * p.y );
	}
	 
    
    
	public int getModule() {
		return ( int ) NanoMath.sqrt( x * x + y * y );
	}
	 
    
	public void add( Point p ) {
        x += p.x;
        y += p.y;
	}
    
    
	public void add( int x, int y ) {
        this.x += x;
        this.y += y;
	}    
	 
    
	public void sub( Point p ) {
        x -= p.x;
        y -= p.y;
	}
    
    
	public void sub( int x, int y ) {
        this.x -= x;
        this.y -= y;
	}        
	 
    
	public void multiply( int m ) {
        x *= m;
        y *= m;
	}
	 
    
	public void divide( int d ) {
        x /= d;
        y /= d;
	}
	 
    
	public boolean equals( Point p ) {
		return x == p.x && y == p.y;
	}
    
    
	public boolean equals( int x, int y ) {
		return this.x == x && this.y == y;
	}    
	
	
	/**
	 * Define um vetor com origem em (0,0) a partir do �ngulo e m�dulo recebidos.
	 * @param angle �ngulo do vetor em graus (sistema de �ngulos como no c�rculo trigonom�trico, no sentido anti-hor�rio).
	 * @param module m�dulo (comprimento) do vetor.
	 */
	public final void setVector( int angle, int module ) {
		set( module * NanoMath.cos( angle ) / NanoMath.SIN_COS_MULTIPLIER, module * NanoMath.sin( angle ) / NanoMath.SIN_COS_MULTIPLIER );
	}

}
 
