/*
 * Frame.java
 *
 * Created on September 28, 2007, 5:55 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public class Frame extends Drawable {
	
	/** imagem que representa o frame. */
	protected final Image image;
	
	/** dura��o em milisegundos do frame. */
	protected short duration;
	
	/** offset da posi��o top-left em rela��o */
	protected final Point offset = new Point();
	
	
	/** Creates a new instance of Frame */
	public Frame( String imagePath ) throws Exception {
		this( Image.createImage( imagePath ) );
		System.gc();
	}
	
	
	public Frame( Image image ) throws Exception {
		this.image = image;
		setSize( image.getWidth(), image.getHeight() );
	}

	
	/**
	 * Desenha o frame, sem realizar opera��es de transforma��o. Equivalente � chamada de <i>paint(g, 0)</i>.
	 * @param g refer�ncia para o Graphics onde ser� desenhado o frame.
	 * @see #paint(Graphics,int)
	 */
	public final void paint( Graphics g ) {
		paint( g, 0 );
	}
	
	
	/**
	 * Desenha um frame de uma sequ�ncia.
	 * @param g refer�ncia para o Graphics onde ser� desenhado o frame.
	 * @param transform transforma��o acumulada, de acordo com a defini��o em <i>javax.microedition.lcdui.game.Sprite</i>.
	 * @see #paint(Graphics)
	 */
	protected void paint( Graphics g, int transform ) {
		if ( transform == 0 )
			g.drawImage( image, Drawable.translate.x + offset.x, Drawable.translate.y + offset.y, 0 );
		else
			g.drawRegion( image, 0, 0, image.getWidth(), image.getHeight(), transform, Drawable.translate.x + offset.x, Drawable.translate.y + offset.y, 0 );
	}	

	
	public final boolean collidesWith( Frame f ) {
		return false; //		TODO collidesWith
	}
	
	
	public final short getDuration() {
		return duration;
	}
	
}
