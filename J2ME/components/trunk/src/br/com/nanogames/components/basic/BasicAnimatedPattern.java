/*
 * BasicAnimatedPattern.java
 *
 * Created on 7 de Julho de 2007, 11:48
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoRandom;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class BasicAnimatedPattern extends Pattern implements Updatable {
	
	public static final byte ANIMATION_RANDOM				= -1;
	public static final byte ANIMATION_NONE					= 0;
	public static final byte ANIMATION_HORIZONTAL			= 1;
	public static final byte ANIMATION_ALTERNATE_HORIZONTAL	= 2;
	public static final byte ANIMATION_VERTICAL				= 3;
	public static final byte ANIMATION_ALTERNATE_VERTICAL	= 4;
	public static final byte ANIMATION_DIAGONAL				= 5;
	
	private static final byte TOTAL_ANIMATIONS				= 6;
	
	private byte currentAnimation;
	
	private final Point module = new Point();
	
	private final Point offset = new Point();
	
	private final Point speed;
	
	
	/** Creates a new instance of Background */
	public BasicAnimatedPattern( Drawable d, int speedX, int speedY ) throws Exception {
		super( d );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		speed = new Point( speedX, speedY );
	}

	
	public final void update( int delta ) {
		if ( currentAnimation != ANIMATION_NONE ) {
			final int dsX = module.x + ( speed.x * delta );
			final int dx = dsX / 1000;
			module.x = dsX % 1000;
			
			final int dsY = module.y + ( speed.y * delta );
			final int dy = dsY / 1000;
			module.y = dsY % 1000;			

			switch ( currentAnimation ) {
				case ANIMATION_HORIZONTAL:
				case ANIMATION_ALTERNATE_HORIZONTAL:
					offset.x = ( offset.x + dx ) % fill.getSize().x;
				break;
				
				case ANIMATION_VERTICAL:
				case ANIMATION_ALTERNATE_VERTICAL:
					offset.y = ( offset.y + dy ) % fill.getSize().y;					
				break;
				
				case ANIMATION_DIAGONAL:
					offset.x = ( offset.x + dx ) % fill.getSize().x;
					offset.y = ( offset.y + dy ) % fill.getSize().y;					
				break;
			}
		} // fim if ( animation != ANIMATION_NONE )
	}
	
	
	public final void setAnimation( byte animation ) {
		final NanoRandom random = new NanoRandom();
		
		if ( animation == ANIMATION_RANDOM )
			animation = ( byte ) ( random.nextInt( TOTAL_ANIMATIONS ) );
				
		switch ( animation ) {
			case ANIMATION_NONE:
			case ANIMATION_HORIZONTAL:
			case ANIMATION_VERTICAL:
			case ANIMATION_DIAGONAL:
			case ANIMATION_ALTERNATE_HORIZONTAL:
			case ANIMATION_ALTERNATE_VERTICAL:
			break;
			
			default:
				return;
		}
		
		if ( random.nextInt( 100 ) < 50 )
			speed.x = -speed.x;
		
		if ( random.nextInt( 100 ) < 50 )
			speed.y = -speed.y;		
		
		currentAnimation = animation;
	}

	
	protected final void paint( Graphics g ) {
		final Point fillSize = fill.getSize();		
		boolean alternate = false;
		
		switch ( currentAnimation ) {
			case ANIMATION_ALTERNATE_HORIZONTAL:
				for ( int y = -Math.abs( offset.y ); y < size.y; y += fillSize.y ) {
					alternate = !alternate;
					for ( int x = -Math.abs( alternate ? -offset.x : ( offset.x - fillSize.x ) ); x < size.x; x += fillSize.x ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = -offset.x; x < size.x; x += fillSize.x )
				} // fim for ( int y = -offset.y; y < size.y; y += fillSize.y )				
			break;
			
			case ANIMATION_ALTERNATE_VERTICAL:
				for ( int x = -Math.abs( offset.x ); x < size.x; x += fillSize.x ) {
					alternate = !alternate;
					for ( int y = -Math.abs( alternate ? -offset.y : ( offset.y - fillSize.y ) ); y < size.y; y += fillSize.y ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = -offset.x; x < size.x; x += fillSize.x )
				} // fim for ( int y = -offset.y; y < size.y; y += fillSize.y )				
			break;
		
			default:
				for ( int y = -Math.abs( offset.y ); y < size.y; y += fillSize.y ) {
					for ( int x = -Math.abs( offset.x ); x < size.x; x += fillSize.x ) {
						fill.setRefPixelPosition( x, y );
						fill.draw( g );
					} // fim for ( int x = -offset.x; x < size.x; x += fillSize.x )
				} // fim for ( int y = -offset.y; y < size.y; y += fillSize.y )
			break;
		}
	}

	
	public final void setSize(int width, int height) {
		if ( fill == null )
			super.setSize( width, height );
		else
			super.setSize( width + fill.getSize().x, height + fill.getSize().y );
	}	
	
}
