/**
 * GameMIDlet.java
 */
package core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.MediaPlayer;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import br.com.nanogames.components.util.Serializable;
import screens.AxeMenu;
import screens.PlayScreen;
import screens.ScreenRecommend;
import screens.SplashGame;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import screens.AxeMenu;
import screens.AxeTextScreen;
import screens.Bkg;
import screens.PlayScreen;
import screens.ScreenRecommend;
import screens.SplashGame;
//#endif

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif
/**
 * ©2007 Nano Games
 * @author Daniel L. Alves
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable {

	/** Referência para a tela de jogo */
	private PlayScreen playScreen;
	/** Fonte padrão do jogo */
	private static final byte BACKGROUND_TYPE_DEFAULT = 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;
	private static final byte BACKGROUND_TYPE_NONE = 2;
	private static final byte BACKGROUND_TYPE_WALL = 3;
	private static int softKeyRightText;
	public static final byte DEFAULT_FONT_OFFSET = -1;
	//#if LOG == "true"
//# 	public static final byte SCREEN_LOG				= 21;
	//#endif
	public static StringBuffer log = new StringBuffer();
	//</editor-fold>
	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu principal">
	public static final byte MAIN_MENU_NEW_GAME = 0;
	public static final byte MAIN_MENU_OPTIONS = 1;
	public static final byte MAIN_MENU_RECOMMEND = 2;
	public static final byte MAIN_MENU_HELP = 3;
	public static final byte MAIN_MENU_CREDITS = 4;
	public static final byte MAIN_MENU_EXIT = 5;
	public static final byte MAIN_MENU_LOG = 6;
	//</editor-fold>
	//</editor-fold>
	/* indices de opções menu escolha de personagem */
	public static final byte OPTION_ENGLISH = 0;
	public static final byte OPTION_PORTUGUESE = 1;
	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu de opções">
	//#ifdef NO_SOUND
//# 		public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 0;
//# 		public static final byte OPTIONS_MENU_ERASE_RECORDS		= 1;
//# 		public static final byte OPTIONS_MENU_BACK				= 2;
//# 
//# 		public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 0;
//# 		public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;
	//#else
	public static final byte OPTIONS_MENU_TOGGLE_SOUND = 0;
	public static final byte OPTIONS_MENU_TOGGLE_VIBRATION = 1;
	public static final byte OPTIONS_MENU_BACK = 2;
	public static final byte OPTIONS_MENU_NO_VIB_TOGGLE_SOUND = 0;
	public static final byte OPTIONS_MENU_NO_VIB_BACK = 1;
	//#endif
	//</editor-fold>
	/** Indica se o device possui pouca memória */
	private boolean lowMemory;

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	//#ifndef NO_RECOMMEND
		private Drawable recommendScreen;
	//#endif
		
	/** Tempo máximo entre duas renderizações */
	private static final short LOW_FPS_FRAME_TIME = 107;
	public static long score;

	private MenuBkg background;

	private static byte screenSize = -1;

	private static final String[] PREFIXES = { "/small/", "/medium/", "/big/", "/giant/", "/small_medium/", "/big_giant/" };


	public GameMIDlet() throws Exception {
		super( -1, LOW_FPS_FRAME_TIME );

		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	}


	public static final void log( String s ) {
		//#if DEBUG == "true"
			System.gc();

			log.append( s );
			log.append( ": " );
			log.append( Runtime.getRuntime().freeMemory() );

			log.append( "\n" );
		//#endif
	}


	public final void destroy() {
		//#if LOG == "true"
//# 		Logger.saveLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif

		super.destroy();
	}


	protected final void loadResources() throws Exception {
		//#if SCREEN_SIZE == "GENERIC"
			if ( ScreenManager.SCREEN_WIDTH < 176 ) {
				screenSize = SCREEN_SIZE_SMALL;
			} else if ( ScreenManager.SCREEN_WIDTH < 240 ) {
				screenSize = SCREEN_SIZE_MEDIUM;
			} else if ( ScreenManager.SCREEN_WIDTH > 400 || ScreenManager.SCREEN_HEIGHT > 400 ) {
				screenSize = SCREEN_SIZE_GIANT;
			} else {
				screenSize = SCREEN_SIZE_BIG;
			}
		//#elif SCREEN_SIZE == "GIANT"
//# 			screenSize = SCREEN_SIZE_GIANT;
		//#elif SCREEN_SIZE == "BIG"
//# 			screenSize = SCREEN_SIZE_BIG;
		//#elif SCREEN_SIZE == "MEDIUM"
//# 			screenSize = SCREEN_SIZE_MEDIUM;
		//#elif SCREEN_SIZE == "SMALL"
//# 			screenSize = SCREEN_SIZE_SMALL;
		//#endif
		
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}

		// Carrega a fonte da aplicação
		final String PATH_FONTS = getPath( PATH_IMAGES ) + "fonts/";
		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
			if ( getScreenSize() == SCREEN_SIZE_BIG && i >= FONT_DICE_GREEN && i <= FONT_DICE_YELLOW )
				FONTS[i] = ImageFont.createMultiSpacedFont( getPath( PATH_IMAGES, SCREEN_SIZE_MEDIUM ) + "fonts/" + i );
			else
				FONTS[i] = ImageFont.createMultiSpacedFont( PATH_FONTS + i );
		}

		FONTS[FONT_MENU_OVER].setCharExtraOffset( 2 );
		FONTS[FONT_MENU].setCharExtraOffset( 2 );
		FONTS[FONT_DICE_GREEN].setCharExtraOffset(-1);
		FONTS[FONT_DICE_YELLOW].setCharExtraOffset(-1);
		FONTS[FONT_DICE_RED].setCharExtraOffset(-1);
		FONTS[FONT_TEXT].setCharExtraOffset(1);
		FONTS[FONT_TITLE].setCharExtraOffset(1);

		switch ( getVendor() ) {
			case VENDOR_MOTOROLA:
			case VENDOR_NOKIA:
			case VENDOR_SAMSUNG:
				if ( getScreenSize() < SCREEN_SIZE_BIG ) {
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
					break;
				}
			// sem break

			case VENDOR_SONYERICSSON:
			case VENDOR_SIEMENS:
			case VENDOR_BLACKBERRY:
			case VENDOR_HTC:
			break;

			default:
				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
//		lowMemory = true; // TODO teste

		//#if BLACKBERRY_API == "true"
//# 			// em aparelhos antigos (e/ou com versões de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms não recebe os eventos de teclado corretamente, sumindo somente após a aplicação ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
		//#endif
		
		//#ifdef NO_SOUND
//# 		MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, null );
//# 		MediaPlayer.setMute( true );
		//#else
			final String[] SOUNDS = new String[ SOUNDS_TOTAL ];
			final boolean[] MP3 = new boolean[] {
				true,
				true,
				false,
				true,
				true,
				true,
				false,
				true,
				false,
			};

			for ( byte i = 0; i < SOUNDS_TOTAL; ++i ) {
				SOUNDS[i] = getPath( ROOT_DIR ) + i + ( MP3[ i ] ? ".mp3" : ".mid" );
			}

			MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, SOUNDS );
		//#endif

		// Determina uma cor de fundo
		manager.setBackgroundColor( DEFAULT_LIGHT_COLOR );

		//#if LOG == "true"
//# 		Logger.setRMS( DATABASE_NAME, DATABASE_SLOT_LOG );
//# 		Logger.loadLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif

		Tag.load();

		try {
			AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this );
		} catch ( Exception e ) {
			language = NanoOnline.LANGUAGE_en_US;
		}

		setScreen( SCREEN_CHOOSE_LANGUAGE );
	}


	public static final byte getScreenSize() {
		return screenSize;
	}

	public static final String getPath( byte identifier ) {
		return getPath( identifier, false );
	}


	public static final byte getSpacing() {
		switch ( getScreenSize() ) {
			case SCREEN_SIZE_MEDIUM:
				return 4;

			case SCREEN_SIZE_BIG:
				return 8;

			case SCREEN_SIZE_GIANT:
				return 14;

			case SCREEN_SIZE_SMALL:
			default:
				return 2;
		}
	}

	
	public static final String getPath( byte identifier, boolean generic ) {
		return getPath( identifier, generic? ( getScreenSize() >> 1 ) + 4 : getScreenSize() );
	}


	public static final String getPath( byte identifier, int screenSize ) {
		final String prefix = PREFIXES[ screenSize ];
		
		switch ( identifier ) {
			case PATH_BKG:
			return prefix + "Bkg/";

			case PATH_BORDER:
				return prefix + "border/";

			case PATH_MENU:
				return prefix + "menu/";

			case PATH_SCROLL:
				return prefix + "scroll/";

			case ROOT_DIR:
				return "/";

			case PATH_SPLASH:
				return prefix + "splash/";

			case PATH_IMAGES:
			default:
				return prefix;
		}
	}


	public static final boolean isAnimationEnded() {
		final MenuBkg bkg = ( ( GameMIDlet ) instance ).background;
		return bkg == null || !bkg.isLogoVisible();
	}


	public synchronized final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif

				//#ifndef RECOMMEND_SCREEN
//# 				if ( index >= MAIN_MENU_RECOMMEND ) {
//# 					++index;
//# 				}
				//#endif

				//#ifndef NO_RECOMMEND
					//#if WEB_EMULATOR == "false"
	//# 							if ( !SmsSender.isSupported() && index >= MAIN_MENU_RECOMMEND ) {
	//# 								++index;
	//# 							}
					//#endif
				//#endif
				switch ( index ) {
					case MAIN_MENU_NEW_GAME:
						animateMenu( menu, SCREEN_LOAD_GAME );
						break;

					case MAIN_MENU_OPTIONS:
						animateMenu( menu, SCREEN_OPTIONS );
						break;

					case MAIN_MENU_RECOMMEND:
						animateMenu( menu, SCREEN_LOADING_RECOMMEND_SCREEN );
						break;

					case MAIN_MENU_HELP:
						animateMenu( menu, SCREEN_HELP );
						break;

					case MAIN_MENU_CREDITS:
						//#if CREDITS == "true"
//# 							animateMenu( menu, SCREEN_CREDITS );
//# 							break;
//# 							// quando é false, deixa passar direto pois o índice passa a ser
//# 							// o de saída
						//#endif

					case MAIN_MENU_EXIT:
						animateMenu( menu, SCREEN_EXIT );
						break;
				}
				break; // fim case SCREEN_MAIN_MENU


			case SCREEN_CHOOSE_LANGUAGE:
				try {
					switch ( index ) {
						case OPTION_ENGLISH:
							language = NanoOnline.LANGUAGE_en_US;
						break;

						case OPTION_PORTUGUESE:
						default:
							language = NanoOnline.LANGUAGE_pt_BR;
						break;
					}
					AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this );
					
					loadTexts( TEXT_TOTAL, getPath( ROOT_DIR ) + language + ".dat" );
					animateMenu( menu, SCREEN_ENABLE_SOUNDS );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif

					exit();
				}
				break;

			case SCREEN_SPLASH_BRAND:
				animateMenu( menu, SCREEN_SPLASH_GAME );
				break;

			case SCREEN_CREDITS:
				animateMenu( menu, SCREEN_MAIN_MENU );
				break;

			case SCREEN_OPTIONS:
				final Tag tag = ( Tag ) menu.getDrawable( index );

				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case OPTIONS_MENU_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );

							tag.setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
							break;

						case OPTIONS_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							MediaPlayer.play( MediaPlayer.support( MediaPlayer.MEDIA_TYPE_MP3 ) ? SOUND_DICE_MP3 : SOUND_DICE_MIDI );
							tag.setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case OPTIONS_MENU_BACK:
							MediaPlayer.saveOptions();
							animateMenu( menu, SCREEN_MAIN_MENU );
							break;
					}
				} else {
					switch ( index ) {
						case OPTIONS_MENU_NO_VIB_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							MediaPlayer.play( MediaPlayer.support( MediaPlayer.MEDIA_TYPE_MP3 ) ? SOUND_DICE_MP3 : SOUND_DICE_MIDI );
							tag.setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							animateMenu( menu, SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS

			case SCREEN_ENABLE_SOUNDS:
				MediaPlayer.setMute( index == 1 );
				MediaPlayer.saveOptions();
				animateMenu( menu, SCREEN_SPLASH_GAME );
				break;// fim switch ( id )
		}
	}


	private final void animateMenu( Menu m, int nextScreen ) {
		switch ( nextScreen ) {
			case SCREEN_MAIN_MENU:
			case SCREEN_OPTIONS:
				getBkg().setLogoVisible( true );
			break;

			default:
				getBkg().setLogoVisible( false );
		}
		
		for ( byte i = 0; i < m.getUsedSlots(); ++i ) {
			final Drawable d = m.getDrawable( i );
			if ( d instanceof Tag ) {
				( ( Tag ) d ).exit( i < m.getUsedSlots() - 1 ? -1 : nextScreen );
			}
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		for ( byte i = 0; i < menu.getUsedSlots(); ++i ) {
			if ( menu.getDrawable( i ) instanceof Tag ) {
				final Tag tag = ( Tag ) menu.getDrawable( i );
				tag.setSelected( i == index );
			}
		}
	}

	
	public static final boolean isLowMemory() {
		return ( ( GameMIDlet ) getInstance() ).lowMemory;
	}


	public final void write( DataOutputStream output ) throws Exception {

		output.writeByte( language );
		output.writeBoolean( MediaPlayer.isMuted() );
	}


	public final void read( DataInputStream input ) throws Exception {

		language = input.readByte();

		input.readBoolean();
	}


	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;


	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualização do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;
		private long lastUpdateTime;
		private static final byte MAX_DOTS = 4;
		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;
		private byte dots;
		private Thread loadThread;
		private final LoadListener listener;
		private boolean painted;
		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener ) throws Exception {
			super( AppMIDlet.getFont( FONT_MENU ), AppMIDlet.getText( TEXT_LOADING ) );

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo são carregados aqui para evitar sobrecarga do método loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// só inicia a thread quando a tela atual for a ativa, para evitar possíveis atrasos em transições e
					// garantir que novos recursos só serão carregados quando a tela anterior puder ser desalocada por completo
					if ( painted ) {
						//#if TOUCH == "true"
						//	ScreenManager.setPointerListener( null );
						//#endif

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
//										texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();
//
//										setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									GameMIDlet.exit();
								//#endif
								}
							} // fim do método run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execução
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
					//#endif
					}
				}
			}
		} // fim do método update( int )


		public final void paint( Graphics g ) {
			super.paint( g );
			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT ) {
			softKeyRightText = textIndex;
		}
	} // fim do método setSoftKeyLabel( byte, int )


	protected final int changeScreen( int screen ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		byte bkgType = BACKGROUND_TYPE_DEFAULT;

		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		boolean playTheme = false;
		int softKeyVisibleTime = 0;

		switch ( screen ) {
			case SCREEN_MAIN_MENU:
			case SCREEN_OPTIONS:
				getBkg().setLogoVisible( true );
			break;

			default:
				getBkg().setLogoVisible( false );
		}

		try {
			switch ( screen ) {
				case SCREEN_ENABLE_SOUNDS:
					nextScreen = AxeMenu.createInstance( screen, new int[] { TEXT_TURN_SOUND_ON, TEXT_TURN_SOUND_OFF } );
					( ( AxeMenu ) nextScreen ).setCurrentIndex( MediaPlayer.isMuted() ? 1 : 0 );
					break;

				case SCREEN_SPLASH_GAME:
					nextScreen = new SplashGame( getFont( FONT_MENU ) );
					bkgType = BACKGROUND_TYPE_WALL;
					break;

				case SCREEN_MAIN_MENU:
					playScreen = null;
					
					playTheme = true;
					//#ifndef NO_RECOMMEND
						recommendScreen = null;
					//#endif

					nextScreen = AxeMenu.createInstance( screen, new int[] {
								TEXT_NEW_GAME,
								TEXT_OPTIONS,
								//#if RECOMMEND_SCREEN == "true"
									TEXT_RECOMMEND_TITLE,
								//#endif
								TEXT_HELP,
								//#if CREDITS == "true"
//# 									TEXT_CREDITS,
								//#endif
								TEXT_EXIT,
							} );
					break;

				case SCREEN_LOAD_GAME:
					bkgType = BACKGROUND_TYPE_NONE;
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( LoadScreen loadScreen ) throws Exception {
							setBackground( BACKGROUND_TYPE_NONE );
							background = null;
							
							MediaPlayer.free();
							setSpecialKeyMapping( true );
							playScreen = new PlayScreen();
							loadScreen.setActive( false );
							setScreen( SCREEN_GAME );
						}
					} );
					break;

				case SCREEN_OPTIONS:
					playTheme = true;
					if ( MediaPlayer.isVibrationSupported() ) {
						nextScreen = AxeMenu.createInstance( screen, new int[]{
									TEXT_TURN_SOUND_OFF,
									TEXT_TURN_VIBRATION_OFF,
									TEXT_BACK } );
					} else {
						nextScreen = AxeMenu.createInstance( screen, new int[]{
									TEXT_TURN_SOUND_OFF,
									TEXT_BACK } );
					}
					break;

				//#if CREDITS == "true"
//# 				case SCREEN_CREDITS:
//# 					playTheme = true;
//# 					indexSoftRight = TEXT_BACK;
//# 
//# 					final Drawable[] logos = new Drawable[] {
//# 						new DrawableImage( getPath( PATH_SPLASH, true ) + "nano.png" ),
//# 						new DrawableImage( getPath( PATH_SPLASH, true ) + "emporio.png" ),
//# 						new DrawableImage( getPath( PATH_SPLASH, true ) + "kana.png" ),
//# 						new DrawableImage( getPath( PATH_SPLASH, true ) + "cubo.png" ),
//# 					};
//# 
//# 					nextScreen = new AxeTextScreen( getText( TEXT_CREDITS_TEXT ) + getMIDletVersion() + "\n\n", SCREEN_MAIN_MENU, true, logos );
//# 					break;
				//#endif

				case SCREEN_HELP:
					playTheme = true;
					indexSoftRight = TEXT_BACK;

					final ImageFont font = ImageFont.createMultiSpacedFont( getPath( PATH_IMAGES, true ) + "fonts/7" );
					font.setCharExtraOffset( 1 );
					final RichLabel title = new RichLabel( font, TEXT_NAO_DEIXE_GATA_ENTEDIADA, getScreenSize() == SCREEN_SIZE_SMALL ? ScreenManager.SCREEN_WIDTH : ScreenManager.SCREEN_WIDTH * 3 / 4 );

					final AxeTextScreen s = new AxeTextScreen( getText( TEXT_HELP_TEXT ) + getMIDletVersion() + "\n\n", SCREEN_MAIN_MENU, false, new Drawable[] { title } );
					nextScreen = s;
					break;

				// Vai do menu principal para o jogo
				case SCREEN_NEXT_LEVEL:
					playScreen.reset();
				case SCREEN_GAME:
					//#if TOUCH == "true"
					if ( !ScreenManager.getInstance().hasPointerEvents() ) {
						softKeyVisibleTime = TIME_SOFT_KEY_VISIBLE;
					}
					//#else
//# 							softKeyVisibleTime = TIME_SOFT_KEY_VISIBLE;
					//#endif
					bkgType = BACKGROUND_TYPE_NONE;

					nextScreen = playScreen;
					break;

				case SCREEN_CHOOSE_LANGUAGE:
					nextScreen = AxeMenu.createInstance( screen, new String[] { "ENGLISH", "PORTUGUÊS" } );
					( ( AxeMenu ) nextScreen ).setCurrentIndex( getLanguage() == NanoOnline.LANGUAGE_en_US ? 0 : 1 );
					break;

				//#if RECOMMEND_SCREEN == "true"

				case SCREEN_RECOMMEND_SENT:
					playTheme = true;
					nextScreen = new AxeTextScreen( getText( TEXT_RECOMMEND_SENT ) + getRecommendURL().toUpperCase() + "\n\n", SCREEN_MAIN_MENU, false, null );
					//( ( Pattern ) ( ( AxeTextScreen ) nextScreen ).getScrollFull() ).setFillColor( DEFAULT_DARK_COLOR );
					//( ( Pattern ) ( ( AxeTextScreen ) nextScreen ).getScrollPage() ).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
					//nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

					indexSoftRight = TEXT_BACK;
					break;

				case SCREEN_RECOMMEND:
					playTheme = true;
					nextScreen = recommendScreen;

					indexSoftRight = TEXT_BACK;
					break;

				case SCREEN_LOADING_RECOMMEND_SCREEN:
					nextScreen = new LoadScreen( new LoadListener() {

						public final void load( final LoadScreen loadScreen ) throws Exception {
							setSpecialKeyMapping( true );
							final ScreenRecommend recommend = new ScreenRecommend( SCREEN_MAIN_MENU );
							final Form f = new Form( recommend );

							f.setTouchKeyPad( new TouchKeyPad( getFont( FONT_TEXT ), new DrawableImage( getPath( PATH_IMAGES ) + "clear.png" ),
												new DrawableImage( "/online/shift.png" ), getBorder(), 0x002831, 0x428a9c, 0x215963 ) );

							recommendScreen = f;

							loadScreen.setActive( false );

							setScreen( SCREEN_RECOMMEND );
						}
					} );
					break;

				//#endif

				case SCREEN_EXIT:
					exit();
				break;
			} // fim switch ( screen )

			if ( indexSoftRight != SOFT_KEY_DONT_CHANGE ) {
				setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, softKeyVisibleTime );
			}

			setBackground( bkgType );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif

			// Ocorreu um erro ao tentar trocar de tela, então sai do jogo
			exit();
		}

		if ( nextScreen != null ) {
			if ( nextScreen instanceof AxeMenu )
				( ( AxeMenu ) nextScreen ).setPlayTheme( playTheme );

			midlet.manager.setCurrentScreen( nextScreen );
			return screen;
		} else {
			return -1;
		}
	}


	public static final ScrollBar getScrollBar() throws Exception {
		return new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT,
				COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Border getBorder() throws Exception {
		final LineBorder b = new LineBorder( 0xff7601, LineBorder.TYPE_ROUND_RAISED ) {

			public void setState( int state ) {
				super.setState( state );

				switch ( state ) {
					case STATE_PRESSED:
						setColor( 0x2d6c00 );
						setFillColor( 0x266800 );
						break;

					case STATE_FOCUSED:
						setColor( 0x2d6c00 );
						setFillColor( 0x469300 );
						break;

					default:
						setColor( 0x2d6c00 );
						setFillColor( 0x314a19 );
						break;
				}
			}
		};
		b.setState( Border.STATE_UNFOCUSED );

		return b;
	}


	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

		if ( specialMapping ) {
			keys = new int[][]{
						{ 'q', ScreenManager.KEY_NUM1 },
						{ 'Q', ScreenManager.KEY_NUM1 },
						{ 'e', ScreenManager.KEY_NUM3 },
						{ 'E', ScreenManager.KEY_NUM3 },
						{ 'z', ScreenManager.KEY_NUM7 },
						{ 'Z', ScreenManager.KEY_NUM7 },
						{ 'c', ScreenManager.KEY_NUM9 },
						{ 'C', ScreenManager.KEY_NUM9 },
						{ 'w', ScreenManager.UP },
						{ 'W', ScreenManager.UP },
						{ 'a', ScreenManager.LEFT },
						{ 'A', ScreenManager.LEFT },
						{ 's', ScreenManager.FIRE },
						{ 'S', ScreenManager.FIRE },
						{ 'd', ScreenManager.RIGHT },
						{ 'D', ScreenManager.RIGHT },
						{ 'x', ScreenManager.DOWN },
						{ 'X', ScreenManager.DOWN },
						{ 'r', ScreenManager.KEY_NUM1 },
						{ 'R', ScreenManager.KEY_NUM1 },
						{ 'y', ScreenManager.KEY_NUM3 },
						{ 'Y', ScreenManager.KEY_NUM3 },
						{ 'v', ScreenManager.KEY_NUM7 },
						{ 'V', ScreenManager.KEY_NUM7 },
						{ 'n', ScreenManager.KEY_NUM9 },
						{ 'N', ScreenManager.KEY_NUM9 },
						{ 't', ScreenManager.KEY_NUM2 },
						{ 'T', ScreenManager.KEY_NUM2 },
						{ 'f', ScreenManager.KEY_NUM4 },
						{ 'F', ScreenManager.KEY_NUM4 },
						{ 'g', ScreenManager.KEY_NUM5 },
						{ 'G', ScreenManager.KEY_NUM5 },
						{ 'h', ScreenManager.KEY_NUM6 },
						{ 'H', ScreenManager.KEY_NUM6 },
						{ 'b', ScreenManager.KEY_NUM8 },
						{ 'B', ScreenManager.KEY_NUM8 },
						{ 10, ScreenManager.FIRE }, // ENTER
						{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

						{ 'u', ScreenManager.KEY_STAR },
						{ 'U', ScreenManager.KEY_STAR },
						{ 'j', ScreenManager.KEY_STAR },
						{ 'J', ScreenManager.KEY_STAR },
						{ '#', ScreenManager.KEY_STAR },
						{ '*', ScreenManager.KEY_STAR },
						{ 'm', ScreenManager.KEY_STAR },
						{ 'M', ScreenManager.KEY_STAR },
						{ 'p', ScreenManager.KEY_STAR },
						{ 'P', ScreenManager.KEY_STAR },
						{ ' ', ScreenManager.KEY_STAR },
						{ '$', ScreenManager.KEY_STAR }, };
		} else {
			for ( char c = 'A'; c <= 'Z'; ++c ) {
				table.put( new Integer( c ), new Integer( c ) );
				table.put( new Integer( c + offset ), new Integer( c + offset ) );
			}

			final int[] chars = new int[]{ ' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
				'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
			};

			for ( byte i = 0; i < chars.length; ++i ) {
				table.put( new Integer( chars[i] ), new Integer( chars[i] ) );
			}
		}

		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i ) {
				table.put( new Integer( keys[i][ 0] ), new Integer( keys[i][ 1] ) );
			}
		}
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null ) {
			return URL_DEFAULT;
		}

		return url;
	}


	public static final MenuBkg getBkg() {
		final GameMIDlet g = ( GameMIDlet ) instance;
		if ( g.background == null )
			try {
			g.background = new MenuBkg();
		} catch (Exception ex) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
		}
		
		return g.background;
	}


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_DEFAULT:
			case BACKGROUND_TYPE_WALL:
				getBkg().setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				midlet.manager.setBackground( ( ( GameMIDlet ) instance ).background, true );
			break;
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
				break;
		}
	} // fim do método setBackground( byte )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}


}
