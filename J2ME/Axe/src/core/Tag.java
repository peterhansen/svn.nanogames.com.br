/**
 * Tag.java
 * 
 * Created on Apr 23, 2009, 10:27:24 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public final class Tag extends DrawableGroup implements Constants, Updatable {

	public static short DEFAULT_WIDTH;

	public static final byte COLOR_LIGHT		= 0;
	public static final byte COLOR_DARK			= 1;

	public static final byte COLORS_TOTAL		= 2;

	private static final byte FRAME_LEFT = 0;
	private static final byte FRAME_FILL = 1;
	private static final byte FRAME_RIGHT = 2;

	private static Sprite BASE;

	private final Sprite left;
	private final Pattern fill;
	private final Sprite right;

	private final Label label;

	private final MUV speed = new MUV();

	private boolean moving = true;

	public static final byte MODE_CENTER				= 0;
	public static final byte MODE_FIXED_WIDTH_RIGHT		= 1;
	public static final byte MODE_SOFT_KEY_RIGHT		= 2;
	public static final byte MODE_SOFT_KEY_LEFT			= 3;

	private final byte mode;

	private short waitTime;

	private int nextScreen = -1;


	public Tag( byte mode ) throws Exception {
		this( mode, null );
	}


	public Tag( byte mode, int diff ) throws Exception {
		this( mode );

		waitTime = ( short ) ( diff * getWidth() >> 1 );

		final int diffX = diff * getWidth() >> 1;
		move( diffX, 0 );
	}


	public Tag( byte mode, String text ) throws Exception {
		super( 5 );

		this.mode = mode;

		left = new Sprite( BASE );
		right = new Sprite( BASE );

		fill = new Pattern( new Sprite( BASE ) );
		
		label = new Label( GameMIDlet.getFont( FONT_MENU ), text );

		insertDrawable( left );
		insertDrawable( right );
		insertDrawable( fill );
		insertDrawable( label );

		switch ( mode ) {
			case MODE_FIXED_WIDTH_RIGHT:
				setSize( DEFAULT_WIDTH, right.getHeight() );
			break;

			case MODE_CENTER:
			case MODE_SOFT_KEY_RIGHT:
			case MODE_SOFT_KEY_LEFT:
				setSize( label.getWidth() + right.getWidth() + left.getCurrentFrameImage().getWidth(), right.getHeight() );
			break;
		}

		setSelected( false );
		reset();
	}


	public final void reset() {
		switch ( mode ) {
			case MODE_FIXED_WIDTH_RIGHT:
			case MODE_CENTER:
			case MODE_SOFT_KEY_RIGHT:
				speed.setSpeed( -getWidth() << 2 );
				setPosition( ScreenManager.SCREEN_WIDTH, getPosY() );
			break;

			case MODE_SOFT_KEY_LEFT:
				speed.setSpeed( getWidth() << 2 );
				setPosition( -getWidth(), getPosY() );
			break;
		}
		setVisible( true );
		setMoving( true );
	}


	public final void exit( int nextScreen ) {
		speed.setSpeed( mode == MODE_SOFT_KEY_LEFT ? -Math.abs( speed.getSpeed() ) : Math.abs( speed.getSpeed() ) );
		moving = true;
		this.nextScreen = nextScreen;
	}


	public static final void load() throws Exception {
		switch ( GameMIDlet.getScreenSize() ) {
			case SCREEN_SIZE_SMALL:
				DEFAULT_WIDTH = 100;
			break;

			case SCREEN_SIZE_MEDIUM:
				DEFAULT_WIDTH = 120;
			break;

			case SCREEN_SIZE_BIG:
				DEFAULT_WIDTH = 150;
			break;

			case SCREEN_SIZE_GIANT:
				DEFAULT_WIDTH = 170;
			break;
		}
		BASE = new Sprite( GameMIDlet.getPath( PATH_IMAGES, true ) + "tags/t" );
	}


	public final void setText( String text ) {
		label.setText( text );
		setSize( getSize() );
	}


	public final void setText( int textIndex ) {
		if ( textIndex < 0 )
			exit( -1 );
		else
			setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setSelected( boolean selected ) {
		final byte sequence = selected ? COLOR_LIGHT : COLOR_DARK;

		right.setSequence( sequence );
		right.setFrame( FRAME_RIGHT );

		left.setSequence( sequence );
		left.setFrame( FRAME_LEFT );

		( ( Sprite ) ( fill.getFill() ) ).setSequence( sequence );
		( ( Sprite ) ( fill.getFill() ) ).setFrame( FRAME_FILL );

		label.setFont( GameMIDlet.getFont( selected ? FONT_MENU_OVER : FONT_MENU ) );
	}


	public final ImageFont getFont() {
		return label.getFont();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		right.setPosition( width - right.getCurrentFrameImage().getWidth(), 0 );
		fill.setPosition( left.getCurrentFrameImage().getWidth(), 0 );
		fill.setSize( right.getPosX() - fill.getPosX(), fill.getHeight() );

		label.setPosition( right.getPosX() - label.getWidth() - left.getCurrentFrameImage().getWidth(), ( getHeight() - label.getHeight() ) >> 1 );
	}


	public final void setMoving( boolean moving ) {
		this.moving = moving;
	}


	public final void update( int delta ) {
		if ( moving ) {
			if ( ( mode == MODE_SOFT_KEY_LEFT && speed.getSpeed() < 0 ) || ( mode != MODE_SOFT_KEY_LEFT && speed.getSpeed() > 0 ) ) {
				waitTime -= delta;
				if ( waitTime <= delta ) {
					final int FINAL_X = mode == MODE_SOFT_KEY_LEFT ? -getWidth() : ScreenManager.SCREEN_WIDTH;

					move( speed.updateInt( delta ), 0 );
					if ( ( mode != MODE_SOFT_KEY_LEFT && getPosX() >= FINAL_X ) || ( mode == MODE_SOFT_KEY_LEFT && getPosX() <= FINAL_X ) ) {
						if ( GameMIDlet.isAnimationEnded() ) {
							moving = false;
							if ( nextScreen >= 0 )
								GameMIDlet.setScreen( nextScreen );
						}
					}
				}
			} else {
				final int FINAL_X;
				switch ( mode ) {
					case MODE_CENTER:
						FINAL_X = ( ScreenManager.SCREEN_HALF_WIDTH - getWidth() ) >> 1;
					break;

					case MODE_SOFT_KEY_LEFT:
						FINAL_X = GameMIDlet.getScreenSize() == SCREEN_SIZE_SMALL ? 0 : 10;
					break;

					default:
						FINAL_X = ScreenManager.SCREEN_WIDTH - getWidth() - ( GameMIDlet.getScreenSize() == SCREEN_SIZE_SMALL ? 0 : GameMIDlet.getSpacing() );
				}

				move( speed.updateInt( delta ), 0 );
				if ( ( mode != MODE_SOFT_KEY_LEFT && getPosX() <= FINAL_X ) || ( mode == MODE_SOFT_KEY_LEFT && getPosX() >= FINAL_X ) ) {
					setPosition( FINAL_X, getPosY() );
					moving = false;
				}
			}
		}
	}


}
