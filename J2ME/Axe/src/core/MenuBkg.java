/**
 * MenuBkg.java
 *
 * Created on 10:42:03 AM.
 *
 */
package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;


/**
 *
 * @author Peter
 */
public final class MenuBkg extends UpdatableGroup implements Constants {

	private final DrawableImage floorCenter;
	private final DrawableImage floorMirror;
	private final Pattern wallBase;
	private final Pattern wallTop;

	private final DrawableImage logo;

	private static final short TIME_HIDE = 600;

	private final BezierCurve titleSpeed = new BezierCurve();
	private final BezierCurve canSpeed = new BezierCurve();

	private short accTime;

	public static final byte LOGO_STATE_HIDDEN		= 0;
	public static final byte LOGO_STATE_APPEARING	= 1;
	public static final byte LOGO_STATE_SHOWN		= 2;
	public static final byte LOGO_STATE_HIDING		= 3;

	private DrawableImage floorBorder;
	private DrawableImage floorBorderMirror;

	private byte logoState;

	private final DrawableImage can;

	private boolean logoMustBeVisible;
	
	
	public MenuBkg() throws Exception {
		super( 20 );

		floorCenter = new DrawableImage( GameMIDlet.getPath( PATH_IMAGES, true ) + "c.png" );
		insertDrawable( floorCenter );
		floorMirror = new DrawableImage( floorCenter );
		floorMirror.mirror( TRANS_MIRROR_H );
		insertDrawable( floorMirror );

		wallBase = new Pattern( new DrawableImage( GameMIDlet.getPath( PATH_IMAGES, true ) + "wall_base.png" ) );
		insertDrawable( wallBase );
		wallTop = new Pattern( new DrawableImage( GameMIDlet.getPath( PATH_IMAGES, true ) + "wall_top.png" ) );
		insertDrawable( wallTop );

		can = new DrawableImage( GameMIDlet.getPath( PATH_IMAGES ) + "axe.png" );
		can.setPosition( -can.getWidth(), 0 );
		can.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
		insertDrawable( can );

		logo = new DrawableImage( GameMIDlet.getPath( PATH_SPLASH, true ) + "title.png" );
		logo.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( logo );
		logo.setVisible( false );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final DrawableImage getCan() {
		return can;
	}


	public final void setLogoVisible( boolean visible ) {
		this.logoMustBeVisible = visible;
		switch ( logoState ) {
			case LOGO_STATE_APPEARING:
			case LOGO_STATE_SHOWN:
				if ( !visible )
					setLogoState( LOGO_STATE_HIDING );
			break;

			case LOGO_STATE_HIDDEN:
			case LOGO_STATE_HIDING:
				if ( visible )
					setLogoState( LOGO_STATE_APPEARING );
			break;
		}
	}


	private final void setLogoState( byte state ) {
		this.logoState = state;
		accTime = 0;
		
		switch ( state ) {
			case LOGO_STATE_HIDDEN:
				logo.setVisible( false );
				logo.setPosition( logo.getPosX(), -logo.getHeight() );
			break;

			case LOGO_STATE_APPEARING:
				titleSpeed.setValues(	new Point( getWidth() >> 1, logo.getRefPixelY() ),
										new Point( getWidth() >> 1, logo.getHeight() >> 3 ),
										new Point( getWidth() >> 1, logo.getHeight() / 3 ),
										new Point( getWidth() >> 1, 0 ) );

				canSpeed.setValues(	new Point( can.getRefPixelX(), can.getRefPixelY() ),
									new Point( 0, can.getRefPixelY() ),
									new Point( can.getWidth() >> 1, getCanY() ),
									new Point( Math.max( ( getWidth() - Tag.DEFAULT_WIDTH ) >> 1, can.getWidth() >> 1 ), getCanY() ) );

				logo.setVisible( true );
			break;

			case LOGO_STATE_SHOWN:
				logo.setVisible( true );
				logo.setPosition( logo.getPosX(), 0 );
			break;

			case LOGO_STATE_HIDING:
				titleSpeed.setValues(	new Point( getWidth() >> 1, logo.getRefPixelY() ),
										new Point( getWidth() >> 1, logo.getHeight() / 3 ),
										new Point( getWidth() >> 1, logo.getHeight() >> 3 ),
										new Point( getWidth() >> 1, -logo.getHeight() ) );

				canSpeed.setValues(	new Point( can.getRefPixelX(), can.getRefPixelY() ),
									new Point( can.getWidth() >> 1, getCanY() ),
									new Point( 0, getCanY() ),
									new Point( -can.getWidth(), getCanY() ) );
				
				logo.setVisible( true );
			break;
		}
	}


	public final int getLogoY() {
		switch ( logoState ) {
			case LOGO_STATE_APPEARING:
			case LOGO_STATE_SHOWN:
				return logo.getHeight();

			case LOGO_STATE_HIDDEN:
			case LOGO_STATE_HIDING:
			default:
				return 0;
		}
	}


	public final boolean isLogoVisible() {
		return !logoMustBeVisible && logo.isVisible();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		floorCenter.setPosition( ( getWidth() >> 1 ) - floorCenter.getWidth(), getHeight() - Math.min( getHeight() >> 2, floorCenter.getHeight() ) );
		floorMirror.setPosition( getWidth() >> 1, floorCenter.getPosY() );
		
		if ( GameMIDlet.getScreenSize() >= SCREEN_SIZE_BIG && getWidth() > ( floorCenter.getWidth() << 1 )  ) {
			// adiciona imagens complementares
			try {
				if ( floorBorder == null ) {
					floorBorder = new DrawableImage( GameMIDlet.getPath( PATH_IMAGES, true ) + "b.png" );
					insertDrawable( floorBorder, 0 );
					
					floorBorderMirror = new DrawableImage( floorBorder );
					floorBorderMirror.mirror( TRANS_MIRROR_H );
					insertDrawable( floorBorderMirror, 0 );
				}
				floorBorder.setPosition( floorCenter.getPosX() - floorBorder.getWidth(), floorCenter.getPosY() );
				floorBorderMirror.setPosition( floorMirror.getPosX() + floorMirror.getWidth(), floorCenter.getPosY() );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}

		wallBase.setSize( getWidth(), wallBase.getHeight() );
		wallBase.setPosition( 0, floorCenter.getPosY() - wallBase.getHeight() );

		logo.setPosition( ( getWidth() - logo.getWidth() ) >> 1, logo.getPosY() );

		final int remainder = wallBase.getPosY() % wallTop.getFill().getHeight();
		final int blocks = ( ( wallBase.getPosY() + remainder ) / wallTop.getFill().getHeight() ) + 1;

		wallTop.setSize( getWidth(), blocks * wallTop.getFill().getHeight() );
		wallTop.setPosition( 0, wallBase.getPosY() - wallTop.getHeight() );
	}


	public final DrawableImage getLogo() {
		return logo;
	}


	public final int getCanY() {
		return Math.min( logo.getHeight() + ( ( getHeight() - can.getHeight() - logo.getHeight() ) >> 1 ), getHeight() - can.getHeight() );
	}


	public final void update( int delta ) {
		super.update( delta );


		switch ( logoState ) {
			case LOGO_STATE_APPEARING:
			case LOGO_STATE_HIDING:
				// evita pequenas falhas de desenho
				boolean finished = accTime > TIME_HIDE;
				accTime += delta;
				final int FP_PERCENT = NanoMath.divInt( accTime, TIME_HIDE );
				final Point p = new Point();
				
				titleSpeed.getPointAtFixed( p, FP_PERCENT );
				logo.setRefPixelPosition( p );

				canSpeed.getPointAtFixed( p, FP_PERCENT );
				can.setRefPixelPosition( p );

				if ( FP_PERCENT >= NanoMath.ONE && finished ) {
					setLogoState( logoState == LOGO_STATE_APPEARING ? LOGO_STATE_SHOWN : LOGO_STATE_HIDDEN );
				}
			break;
		}
	}

}
