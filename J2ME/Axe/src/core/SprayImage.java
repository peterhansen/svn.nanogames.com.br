/**
 * SprayImage.java
 *
 * Created on 11:49:05 PM.
 *
 */
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;


/**
 *
 * @author Peter
 */
public final class SprayImage extends Drawable implements Updatable {

	private final short TIME_SPRAY;

	final int[] logoData;

	private short accTime;

	private int visiblePixels;

	private final int w;
	private final int h;

	private boolean active;

	private int maxX;

	private final int COLOR_MAGENTA;

	private final Mutex mutex = new Mutex();
	

	public SprayImage( DrawableImage d, int time ) {
		w = d.getImage().getWidth();
		h = d.getImage().getHeight();

		TIME_SPRAY = ( short ) time;

		setSize( w, h );

		logoData = new int[ w * h ];
		d.getImage().getRGB( logoData, 0, w, 0, 0, w, h );
		for ( int i = 0; i < logoData.length; ++i )
			logoData[ i ] = logoData[ i ] & 0x00ffffff;

		final Image lixao = Image.createImage( 1, 1 );
		COLOR_MAGENTA = lixao.getGraphics().getDisplayColor( 0x00ff00ff );
	}
	

	protected final void paint( Graphics g ) {
		mutex.acquire();
		try {
			final Image i = Image.createRGBImage( logoData, w, h, true );
			g.drawImage( i, translate.x, translate.y, 0 );
		} catch ( Exception e ) {
		} finally {
			mutex.release();
		}
	}


	public final void setActive( boolean a ) {
		active = a;
	}


	public final void update( int delta ) {
		if ( active && visiblePixels < logoData.length ) {
			accTime += delta;

			mutex.acquire();

			try {
				final int expectedVisible = Math.min( accTime * logoData.length / TIME_SPRAY, logoData.length );
				final int FP_PERCENT = NanoMath.divInt( accTime, TIME_SPRAY );
				maxX = Math.max( 1, NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( getWidth() ), FP_PERCENT ) ) );

				while ( visiblePixels < expectedVisible ) {
					int tries = 10;
					int temp;

					do {
						temp = NanoMath.randInt( maxX ) + NanoMath.randInt( getHeight() ) * getWidth();
						--tries;
					} while ( ( logoData[ temp ] & 0xff000000 ) != 0 && tries > 0 );

					if ( tries == 0 ) {
						// varre até próximo pixel
						for ( int i = ( temp + 1 ) % logoData.length; i != temp; ) {
							if ( ( logoData[ temp ] & 0xff000000 ) != 0 ) {
								temp = i;
								break;
							}
							++i;
							if ( i >= logoData.length )
								i = 0;
						}
					}

					if ( logoData[ temp ] != COLOR_MAGENTA )
						logoData[ temp ] |= 0xff000000;

					++visiblePixels;
				}
			} catch ( Exception e ) {
			} finally {
				mutex.release();
			}
		}
	}


	public final int getMaxX() {
		return maxX;
	}

}
