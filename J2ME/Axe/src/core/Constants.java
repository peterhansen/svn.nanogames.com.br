/**
 * Constants.java
 */

package core;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;

/**
 * ©2007 Nano Games
 * @author Edson
 */
public interface Constants {
	/** Nome curto do jogo, para submissão ao Nano Online. */
	public static final String APP_SHORT_NAME = "AXET";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static final String URL_DEFAULT = "http://game.axe.com.br";

	/***/
	public static final byte RANKING_TYPE_SCORE = 0;

	public static final int COLOR_BACKGROUND = 0x000000;
	public static final int COLOR_BACKGROUND_LIGHT = 0x174950;

	public static final byte FONT_MENU_OVER		= 1;
	public static final byte FONT_MENU			= 0;
	/** Índice da fonte utilizada para textos. */
	public static final byte FONT_TEXT			= 2;

	public static final byte FONT_DICE_GREEN	= 3;
	public static final byte FONT_DICE_RED	= 4;
	public static final byte FONT_DICE_YELLOW	= 5;
	public static final byte FONT_TITLE	= 6;
	


	public static final byte FONT_TYPES_TOTAL = 7;

	/** Tempo que as soft keys ficam visíveis na tela */
	public static final short SOFT_KEY_VISIBLE_TIME = 1000;

	public static final byte FACE_1_RESULT = 0;
	public static final byte FACE_2_RESULT = 1;
	public static final byte FACE_3_RESULT = 2;
	public static final byte FACE_4_RESULT = 3;
	public static final byte FACE_5_RESULT = 4;
	public static final byte FACE_6_RESULT = 5;

	public static final byte RESULT_TYPES_TOTAL = 6;
	
	/** Tempo que o background leva para mudar completamente de cor nas telas de carregamento */
	public static final short LOAD_SCREEN_BKG_COLOR_CHANGE_TIME = 200;
			
	/** Quantidade mínima de memória para que o jogo rode com sua capacidade máxima */
	public static final int LOW_MEMORY_LIMIT = 1200000;
	
	/** Tempo de duração do splash do jogo */
	public static final short SPLASH_GAME_DUR = 3000;
	
	/** Nome da base de dados */
	public static final String DATABASE_NAME = "n";
	
	/** Índice do slot de opções na base de dados */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** Índice do slot de recordes de pontos na base de dados */
	public static final byte DATABASE_SLOT_SCORES = 2;

	public static final byte DATABASE_SLOT_LANGUAGE = 3;

	
	//#if LOG == "true"
//# 	/** Índice do slot do log de erros */
//# 	public static final byte DATABASE_SLOT_LOG = 4;
	//#endif
	
	/** Número total de slots na base de dados */
	//#if LOG == "true"
//# 	public static final byte DATABASE_TOTAL_SLOTS = 4;
	//#else
	public static final byte DATABASE_TOTAL_SLOTS = 3;
	//#endif

	//<editor-fold defaultstate="collapsed" desc="Índices das músicas/sons do jogo">
	

	/** Quantidade total de músicas / sons do jogo */
	public static final byte SOUNDS_TOTAL = 9;

	public static final byte SOUND_DICE_MP3				= 0;
	public static final byte SOUND_SPRAY_MP3			= 1;
	public static final byte SOUND_DICE_MIDI			= 2;
	public static final byte SOUND_RESULT_LIGHT			= 3;
	public static final byte SOUND_RESULT_MEDIUM		= 4;
	public static final byte SOUND_RESULT_HOT			= 5;
	public static final byte SOUND_SPRAY_MIDI			= 6;
	public static final byte SOUND_SHAKE_DICE			= 7;
	public static final byte SOUND_INTRO				= 8;

	public static final short VIBRATION_TIME_SHAKE_DICE = 400;

	
	//</editor-fold>
		
	/** Strings compartilhadas pelo jogo inteiro */
	public static final String SPRITE_DESC_EXT = ".bin";
	public static final String FONT_DESC_EXT = ".dat";
	public static final String IMG_EXT = ".png";

	public static final byte PATH_BKG		= 0;
	public static final byte PATH_IMAGES	= 1;
	public static final byte PATH_BORDER	= 2;
	public static final byte PATH_SCROLL	= 3;
	public static final byte PATH_MENU		= 4;
	public static final byte PATH_SPLASH	= 5;
	public static final byte ROOT_DIR		= 6;

	public static final byte SCREEN_SIZE_SMALL	= 0;
	public static final byte SCREEN_SIZE_MEDIUM	= 1;
	public static final byte SCREEN_SIZE_BIG	= 2;
	public static final byte SCREEN_SIZE_GIANT	= 3;
			
	/** Duração da vibração padrão */
	//#if SAMSUNG_BAD_SOUND == "true"
//#			public static final byte DEFAULT_VIBRATION_TIME = 80;
	//#else
		public static final short DEFAULT_VIBRATION_TIME = 300;
	//#endif
	
	//<editor-fold defaultstate="collapsed" desc="TEXTOS DO JOGO">

	public static final byte TEXT_BACK						= 0;
	public static final byte TEXT_NEW_GAME					= 1;
	public static final byte TEXT_EXIT						= 2;
	public static final byte TEXT_OPTIONS					= 3;
	public static final byte TEXT_CREDITS					= 4;
	public static final byte TEXT_CREDITS_TEXT				= 5;
	public static final byte TEXT_HELP						= 6;
	public static final byte TEXT_HELP_TEXT					= TEXT_HELP + 1;
	public static final byte TEXT_TURN_SOUND_ON				= TEXT_HELP_TEXT + 1;
	public static final byte TEXT_TURN_SOUND_OFF			= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON			= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF		= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_SPLASH_NANO				= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_LOADING					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_DO_YOU_WANT_SOUND			= TEXT_LOADING + 1;
	public static final byte TEXT_YES						= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_NO						= TEXT_YES + 1;
    public static final byte TEXT_NAO_DEIXE_GATA_ENTEDIADA	   = TEXT_NO + 1;
    public static final byte TEXT_RECOMMEND_TITLE			 = TEXT_NAO_DEIXE_GATA_ENTEDIADA + 1;
    public static final byte TEXT_RECOMMEND_TEXT			 = TEXT_RECOMMEND_TITLE + 1;
    public static final byte TEXT_RECOMMEND_SMS				 = TEXT_RECOMMEND_TEXT + 1;
    public static final byte TEXT_RECOMMEND_SENT			 = TEXT_RECOMMEND_SMS + 1;
    public static final byte TEXT_OK							= TEXT_RECOMMEND_SENT + 1;
    public static final byte TEXT_CLEAR							= TEXT_OK + 1;
	public static final byte TEXT_MESSAGE_DICE_RED_1				= TEXT_CLEAR + 1;
    public static final byte TEXT_MESSAGE_DICE_RED_2				= TEXT_MESSAGE_DICE_RED_1 + 1;
    public static final byte TEXT_MESSAGE_DICE_RED_3					= TEXT_MESSAGE_DICE_RED_2 + 1;
    public static final byte TEXT_MESSAGE_DICE_RED_4				= TEXT_MESSAGE_DICE_RED_3 + 1;
    public static final byte TEXT_MESSAGE_DICE_RED_5					= TEXT_MESSAGE_DICE_RED_4 + 1;
    public static final byte TEXT_MESSAGE_DICE_RED_6				= TEXT_MESSAGE_DICE_RED_5 + 1;
	public static final byte TEXT_MESSAGE_DICE_GREEN_1				= TEXT_MESSAGE_DICE_RED_6  + 1;
	public static final byte TEXT_MESSAGE_DICE_GREEN_2				= TEXT_MESSAGE_DICE_GREEN_1	  + 1;
	public static final byte TEXT_MESSAGE_DICE_GREEN_3				= TEXT_MESSAGE_DICE_GREEN_2  + 1;
	public static final byte TEXT_MESSAGE_DICE_GREEN_4				= TEXT_MESSAGE_DICE_GREEN_3  + 1;
	public static final byte TEXT_MESSAGE_DICE_GREEN_5				= TEXT_MESSAGE_DICE_GREEN_4  + 1;
	public static final byte TEXT_MESSAGE_DICE_GREEN_6				= TEXT_MESSAGE_DICE_GREEN_5  + 1;
	public static final byte TEXT_MESSAGE_DICE_YELLOW_1				= TEXT_MESSAGE_DICE_GREEN_6  + 1;
	public static final byte TEXT_MESSAGE_DICE_YELLOW_2				= TEXT_MESSAGE_DICE_YELLOW_1 + 1;
	public static final byte TEXT_MESSAGE_DICE_YELLOW_3				= TEXT_MESSAGE_DICE_YELLOW_2  + 1;
	public static final byte TEXT_MESSAGE_DICE_YELLOW_4				= TEXT_MESSAGE_DICE_YELLOW_3  + 1;
	public static final byte TEXT_MESSAGE_DICE_YELLOW_5				= TEXT_MESSAGE_DICE_YELLOW_4  + 1;
	public static final byte TEXT_MESSAGE_DICE_YELLOW_6				= TEXT_MESSAGE_DICE_YELLOW_5  + 1;
	public static final byte TEXT_MESSAGE_NA			     	= TEXT_MESSAGE_DICE_YELLOW_6  + 1;
	public static final byte TEXT_MESSAGE_NAS				    = TEXT_MESSAGE_NA  + 1;
	public static final byte TEXT_MESSAGE_NOS				    = TEXT_MESSAGE_NAS  + 1;
	public static final byte TEXT_MESSAGE_NO				    = TEXT_MESSAGE_NOS  + 1;
	public static final byte TEXT_MESSAGE_KISS				    = TEXT_MESSAGE_NO  + 1;
	public static final byte TEXT_MESSAGE_BITE				    = TEXT_MESSAGE_KISS  + 1;
	public static final byte TEXT_MESSAGE_LICK				    = TEXT_MESSAGE_BITE  + 1;
	public static final byte TEXT_MESSAGE_BLOW				    = TEXT_MESSAGE_LICK  + 1;
	public static final byte TEXT_MESSAGE_MASSAGE			    = TEXT_MESSAGE_BLOW + 1;
	public static final byte TEXT_MESSAGE_ROMANTICA			    = TEXT_MESSAGE_MASSAGE + 1;
	public static final byte TEXT_MESSAGE_OUSADA		        = TEXT_MESSAGE_ROMANTICA + 1;
	public static final byte TEXT_MESSAGE_TIMIDA		        = TEXT_MESSAGE_OUSADA + 1;
	public static final byte TEXT_MESSAGE_MEIGA		            = TEXT_MESSAGE_TIMIDA + 1;
	public static final byte TEXT_MESSAGE		                = TEXT_MESSAGE_MEIGA + 1;
	public static final byte TEXT_AGAIN		                    = TEXT_MESSAGE + 1;
	public static final byte TEXT_AXE_PRESENTS	                = TEXT_AGAIN + 1;
	public static final byte TEXT_START	                        = TEXT_AXE_PRESENTS + 1;
	public static final byte TEXT_MESSAGE_QUESTION              = TEXT_START + 1;


	//#if LOG == "true"
//# 	public static final byte TEXT_LOG					= 64;
	//#endif
		
	/** Número total de textos do jogo */
	public static final byte TEXT_TOTAL					        = TEXT_MESSAGE_QUESTION + 2;

	//</editor-fold>


    /*Índices das telas do jogo"*/

	public static final byte SCREEN_LOAD_RESOURCES				= 0;
	public static final byte SCREEN_ENABLE_SOUNDS				= 1;
	public static final byte SCREEN_CHANGE_BKG_COLOR			= 2;
	public static final byte SCREEN_SPLASH_NANO					= 3;
	public static final byte SCREEN_SPLASH_BRAND				= 4;
	public static final byte SCREEN_SPLASH_GAME					= 5;
	public static final byte SCREEN_MAIN_MENU					= 6;
	public static final byte SCREEN_LOAD_GAME					= 7;
	public static final byte SCREEN_OPTIONS						= 8;
	public static final byte SCREEN_CREDITS						= 9;
	public static final byte SCREEN_HELP						= 10;
	public static final byte SCREEN_HELP_INTRO					= 11;
	public static final byte SCREEN_HELP_RULES					= 12;
	public static final byte SCREEN_HELP_SPECIAL_ICONS			= 13;
	public static final byte SCREEN_HELP_CONTROLS				= 14;
	public static final byte SCREEN_RECORDS						= 15;
	public static final byte SCREEN_GAME						= 16;
	public static final byte SCREEN_PAUSE						= 17;
	public static final byte SCREEN_UNPAUSE						= 18;
	public static final byte SCREEN_CONTINUE					= 19;
	public static final byte SCREEN_GAMEOVER					= 20;
	public static final byte SCREEN_ENDING						= 21;
	public static final byte SCREEN_YN_EXIT_GAME				= 22;
	public static final byte SCREEN_YN_BACK_TO_MENU				= 23;
    public static final byte SCREEN_CHOOSE_LANGUAGE			    = 24;
	public static final byte SCREEN_NANO_RANKING_MENU			= 25;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_NO_PROFILE + 1;
	public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_RESULT						= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_NEXT_LEVEL					= SCREEN_RESULT + 1;
	public static final byte SCREEN_EXIT						= SCREEN_NEXT_LEVEL + 1;

	// cores da barra de scroll
	public static final int COLOR_FULL_LEFT_OUT	= 0x000b0d;
	public static final int COLOR_FULL_FILL		= 0x32abca;//fundo barra
	public static final int COLOR_FULL_RIGHT	= 0x032026;
	public static final int COLOR_FULL_LEFT		= 0x245865;
	public static final int COLOR_PAGE_OUT		= 0x002b30;
	public static final int COLOR_PAGE_FILL		= 0x0F6981;//barra
	public static final int COLOR_PAGE_LEFT_1	= 0x32abca;
	public static final int COLOR_PAGE_LEFT_2	= 0x63a3ad;//sombra barra scroll

	
	
	public static final int DEFAULT_SCROLL_LIGHT_COLOR = COLOR_FULL_FILL;
	public static final int DEFAULT_LIGHT_COLOR = COLOR_FULL_FILL;
	public static final int DEFAULT_DARK_COLOR = COLOR_PAGE_FILL;	


	/** Espessura da borda do tabuleiro. */
	
 	public static final byte BORDER_THICKNESS = 5;

	// tipo dos dados

	public static  final byte DICE_GREEN = 0;
	public static  final byte DICE_RED = 1;
	public static  final byte DICE_YELLOW = 2;
	public static final byte DADOS_TOTAL = 3;

	

	/** Espessura da borda do tabuleiro. */
	public static final int BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;
	
	public static final int TIME_SOFT_KEY_VISIBLE = 4000;
	public static final byte MENU_ITEMS_SPACING = 10;
    
	public static final byte DICE_MESSAGE_1        =0;
	public static final byte DICE_MESSAGE_2        =1;
	public static final byte DICE_MESSAGE_3        =2;
	public static final byte DICE_MESSAGE_4        =3;
	public static final byte DICE_MESSAGE_5        =4;
	public static final byte DICE_MESSAGE_6        =5;

	public static final byte STATE_UP       =0;
	public static final byte STATE_DOWN     =1;


}