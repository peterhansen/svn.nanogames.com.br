/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.GameMIDlet;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Edson
 */
public final class Dice extends UpdatableGroup implements Constants, SpriteListener {

	private static final byte SEQUENCE_RIGHT		= 0;
	private static final byte SEQUENCE_LEFT			= 1;
	private static final byte SEQUENCE_UP			= 2;
	private static final byte SEQUENCE_DOWN			= 3;
	private static final byte SEQUENCE_DOWN_LEFT	= 4;
	private static final byte SEQUENCE_UP_RIGHT		= 5;
	private static final byte SEQUENCE_FLASH		= 6;
	private static final byte SEQUENCE_BLANK		= 7;

	private static final byte TOTAL_SEQUENCES = 8;

	private static final byte TOTAL_SHINES = 6;

	private static final byte SHINE_APPEARANCES = TOTAL_SHINES << 2;

	private byte remainingAppearances;

	private static final byte ID_SHINE = 100;

	/** Porcentagem da nova velocidade a cada segundo. */
	private static final byte BRAKE_RATE = 65;

	private short brakeAccTime;

	private final Sprite[] shines = new Sprite[ TOTAL_SHINES ];

	private final int MAX_SPEED_MODULE;
	private final int MIN_SPEED_MODULE;
	private final int MIN_SPEED;

	private static final short MIN_FRAME_TIME = 20;
	private static final short MAX_FRAME_TIME = 130;

	public static final byte TWIST_NONE		= 0;
	public static final byte TWIST_LIGHT	= 1;
	public static final byte TWIST_MEDIUM	= 2;
	public static final byte TWIST_HOT		= 3;

	private static byte twist;

	/** Proporção da velocidade após uma colisão, em notação de ponto fixo. */
	private static final int FP_FRICTION = -21000;
	
	/** Raiz quadrada de 3, dividido por 2 (em ponto fixo). */
	private static final int FP_SQRT3_2 = 56754;

	private byte type;
	protected byte state;
	public static final int STATE_WAITING = 0;
	public static final int STATE_RUNNING = 1;
	public static final int STATE_RESULT = 2;
	
	private final RichLabel labelsMessage;
	private final ImageFont fonts;
	protected final MUV speedX = new MUV();
	protected final MUV speedY = new MUV();
	private final Sprite dice;
	//private final Pattern dice;
	private final Point lastPosition;

	private static Sprite[] DICES;
	private static Sprite SHINE;

	private byte faceIndex;

	
	public Dice(byte tipo ) throws Exception {
		super( 5 + TOTAL_SHINES );

		MAX_SPEED_MODULE = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		MIN_SPEED_MODULE = MAX_SPEED_MODULE >> 3;
		MIN_SPEED = MAX_SPEED_MODULE / 9;

		load();
		
		this.type = tipo;
		dice = new Sprite( DICES[ tipo ] );
		switch (tipo) {
			case DICE_GREEN:
				fonts = GameMIDlet.getFont(FONT_DICE_GREEN);
			break;
				
			case DICE_RED:
				fonts = GameMIDlet.getFont(FONT_DICE_RED);
			break;

			case DICE_YELLOW:
				fonts = GameMIDlet.getFont(FONT_DICE_YELLOW);
			break;

			default:
				throw new IllegalArgumentException();
		}

		dice.setListener( this, tipo );

		insertDrawable(dice);
		lastPosition = new Point();
		
		setSize(dice.getWidth(), dice.getHeight());
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		labelsMessage = new RichLabel(fonts, " " , getWidth());
		insertDrawable(labelsMessage);

		for ( byte i = 0; i < TOTAL_SHINES; ++i ) {
			shines[ i ] = new Sprite( SHINE );
			shines[ i ].setListener( this, ID_SHINE + i );
			shines[ i ].defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			insertDrawable( shines[ i ] );
		}

		setState(STATE_WAITING);
	}


	public static final void load() throws Exception {
		if ( DICES == null ) {
			DICES = new Sprite[ DADOS_TOTAL ];

			byte screenSize = GameMIDlet.getScreenSize();
			switch ( screenSize ) {
				case SCREEN_SIZE_BIG:
					screenSize = SCREEN_SIZE_MEDIUM;
				break;

				case SCREEN_SIZE_MEDIUM:
					if ( GameMIDlet.isLowMemory() )
						screenSize = SCREEN_SIZE_SMALL;
				break;
			}

			final String base = GameMIDlet.getPath( PATH_IMAGES, screenSize );
			final String path = base + ( GameMIDlet.isLowMemory() ? "dl" : "d" ) + ".bin";

			for ( byte i = 0; i < DADOS_TOTAL; ++i ) {
				switch ( i ) {
					case DICE_GREEN:
						DICES[ i ] = new Sprite( path, base + "d" );
					break;

					case DICE_RED:
						DICES[ i ] = new Sprite( path, base + "d", 0xff0000, 0x1f0000 );
					break;

					case DICE_YELLOW:
						DICES[ i ] = new Sprite( path, base + "d", 0xffff00, 0x1f1000 );
					break;
				}			
			}

			SHINE = new Sprite( GameMIDlet.getPath( ROOT_DIR ) + "s" );
		}
	}


	public static final void unload() {
		DICES = null;
		System.gc();
	}


	public final byte getNum() {
		int index = -1;

		if ( type == DICE_YELLOW ) {
			switch ( twist ) {
				case TWIST_LIGHT:
					index = ( NanoMath.randInt( 128 ) < 64 ? TEXT_MESSAGE_DICE_YELLOW_4 : TEXT_MESSAGE_DICE_YELLOW_5 ) - TEXT_MESSAGE_DICE_YELLOW_1;
				break;

				case TWIST_MEDIUM:
					index = ( NanoMath.randInt( 128 ) < 64 ? TEXT_MESSAGE_DICE_YELLOW_6 : TEXT_MESSAGE_DICE_YELLOW_2 ) - TEXT_MESSAGE_DICE_YELLOW_1;
				break;

				case TWIST_HOT:
					index = ( NanoMath.randInt( 128 ) < 64 ? TEXT_MESSAGE_DICE_YELLOW_1 : TEXT_MESSAGE_DICE_YELLOW_3 ) - TEXT_MESSAGE_DICE_YELLOW_1;
				break;

				case TWIST_NONE:
				default:
					index = NanoMath.randInt(6);
			}
		} else {
			index = NanoMath.randInt(6);
		}

		faceIndex = ( byte ) index;

		return faceIndex;
	}

	
	public final void setLastposition(final Point p) {
		lastPosition.set(p);
	}


	private final void setShinesVisible( boolean v ) {
		for ( byte i = 0; i < TOTAL_SHINES; ++i ) {
			shines[ i ].setVisible( v );
			if ( v ) {
				shines[ i ].setRefPixelPosition( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) );
				shines[ i ].setFrame( NanoMath.randInt( shines[ i ].getSequence( 0 ).length ) );
			}
		}

		remainingAppearances = SHINE_APPEARANCES;
	}


	public final void onSequenceEnded(int id, int sequence) {
		if ( id < ID_SHINE ) {
			switch ( sequence ) {
				case SEQUENCE_FLASH:
					dice.setSequence( SEQUENCE_BLANK );
				break;
			}

			switch ( state ) {
				case STATE_RUNNING:
					final Point speed = new Point( speedX.getSpeed(), speedY.getSpeed() );
					//#if DEBUG == "true"
						System.out.println( type + " -> " + speed.getModule() + " / " + MIN_SPEED );
					//#endif
					if ( speed.getModule() <= MIN_SPEED ) {
						setState( STATE_RESULT );
					}
				break;
			}
		} else {
			--remainingAppearances;
			if ( remainingAppearances > 0 )
				shines[ id - ID_SHINE ].setRefPixelPosition( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) );
			else
				shines[ id - ID_SHINE ].setVisible( false );
		}
	}


	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}

	///<DM>
	private static final class Vec2
	{
		public int x;
		public int y;
		private int umpontoum;

		public int Length()
		{
			return NanoMath.sqrtFixed(NanoMath.mulFixed(x, x)+NanoMath.mulFixed(y, y));
		}


		public int ModLength()
		{
		int length=Length();
		if (length<0)
			return -length;
		else return length;
		}

		public void Normalize()
		{
			int module=Length();
			x=NanoMath.divFixed(x, module);
			y=NanoMath.divFixed(y, module);
			if (Length()>umpontoum)
				Normalize();
		}


		public Vec2()
		{
			x=0;
			y=0;
			umpontoum=NanoMath.toFixed(101);
			umpontoum=NanoMath.divFixed(umpontoum, NanoMath.toFixed(100));
		}
	}


	public final void treatCollision(Dice d, int delta) {
		final Rectangle intersection = new Rectangle();
		intersection.setIntersection( getCollisionArea(), d.getCollisionArea() );

		final int diffX = ( intersection.width >> 1 );
		final int diffY = ( intersection.height >> 1 );

		if ( diffX < diffY ) {
			if ( getPosX() < d.getPosX() ) {
				move( -diffX, 0 );
				d.move( diffX, 0 );
			} else {
				move( diffX, 0 );
				d.move( -diffX, 0 );
			}
		} else {
			if ( getPosY() < d.getPosY() ) {
				move( 0, -diffY );
				d.move( 0, diffY );
			} else {
				move( 0, diffY );
				d.move( 0, -diffY );
			}
		}

		if ( getSpeedModule() < MIN_SPEED_MODULE && d.getSpeedModule() < MIN_SPEED_MODULE ) {
			return;
		}

		///vetorizar as velocidades originais:
		Vec2 v1=new Vec2();		
		Vec2 v2=new Vec2();

		v1.x=NanoMath.toFixed(speedX.getSpeed());
		v1.y=NanoMath.toFixed(speedY.getSpeed());
		v2.x=NanoMath.toFixed(d.speedX.getSpeed());
		v2.y=NanoMath.toFixed(d.speedY.getSpeed());

		int factor=NanoMath.toFixed(100);
		///obter vetor normal
		Vec2 normalUn = new Vec2();
		normalUn.x = NanoMath.toFixed(d.getRefPixelX() - getRefPixelX() );
		normalUn.y = NanoMath.toFixed(d.getRefPixelY() - getRefPixelY() );
		
		if (normalUn.Length()!=0)
			normalUn.Normalize();
		///obter vetor tangente

		Vec2 tangUn = new Vec2();
		tangUn.x = -normalUn.y;
		tangUn.y = normalUn.x;
		/// componentes normais e tangenciais:
		int v1t;
		int v1n;
		int v2t;
		int v2n;
		///e apos a colisao (a tangencial fica igual):
		int v2nP;
		int v1nP;
		///preciso projetar o v1 no normal unitario e tangente unitario. mas estes nao podem existir. vou projetar no
		///nao unitario e depois dividir pelo modulo do normal.
		v1n=(NanoMath.mulFixed(v1.x,normalUn.x)+ NanoMath.mulFixed(v1.y,normalUn.y));
		v1t=(NanoMath.mulFixed(v1.x,tangUn.x)+NanoMath.mulFixed(v1.y,tangUn.y));
		///mesmo para v2:
		v2n=(NanoMath.mulFixed(v2.x,normalUn.x)+ NanoMath.mulFixed(v2.y,normalUn.y));
		v2t=(NanoMath.mulFixed(v2.x,tangUn.x)+NanoMath.mulFixed(v2.y,tangUn.y));
		///pelas formulas e considerando que ambos os dados tem a mesma massa, v2nP==v1n e v1nP==v2n
		int lossfactor=NanoMath.toFixed(70);

		v1nP=v2n;
		v2nP=v1n;

		v1nP=NanoMath.mulFixed(v1nP, lossfactor);
		v1nP=NanoMath.divFixed(v1nP, factor);
		v2nP=NanoMath.mulFixed(v2nP, lossfactor);
		v2nP=NanoMath.divFixed(v2nP, factor);

		v1.x=NanoMath.mulFixed(v1nP,normalUn.x)+NanoMath.mulFixed(v1t,tangUn.x);
		v1.y=NanoMath.mulFixed(v1nP,normalUn.y)+NanoMath.mulFixed(v1t,tangUn.y);
		v2.x=NanoMath.mulFixed(v2nP,normalUn.x)+NanoMath.mulFixed(v2t,tangUn.x);
		v2.y=NanoMath.mulFixed(v2nP,normalUn.y)+NanoMath.mulFixed(v2t,tangUn.y);

		v1nP=NanoMath.toInt(v1nP);
		v2nP=NanoMath.toInt(v2nP);

		setSpeed( NanoMath.toInt(v1.x), NanoMath.toInt( v1.y ) );
		d.setSpeed( NanoMath.toInt(v2.x), NanoMath.toInt( v2.y ) );

		if ( getState() != STATE_RUNNING )
			setState( STATE_RUNNING );
		if ( d.getState() != STATE_RUNNING )
			d.setState( STATE_RUNNING );
	}


	private final int getSpeedModule() {
		return new Point( speedX.getSpeed(), speedY.getSpeed() ).getModule();
	}

	
	public final void updateMove(int delta) {
		final int diffX;
		final int diffY;
		diffX = speedX.updateInt(delta);
		diffY = speedY.updateInt(delta);
		move(diffX, diffY);
	}

	public final void update(int delta) {
		setLastposition( getPosition() );
		
		updateMove(delta);
		testColisionWall( true );

		switch (state) {
			case STATE_RUNNING:
				super.update(delta);

				brakeAccTime += delta;
				if ( brakeAccTime >= 1000 ) {
					brakeAccTime %= 1000;
					setSpeed( speedX.getSpeed() * BRAKE_RATE / 100, speedY.getSpeed() * BRAKE_RATE / 100 );
				}
			break;

			case STATE_RESULT:
				super.update( delta );
				for ( byte i = 0; i < TOTAL_SHINES; ++i )
					shines[ i ].update( delta );
			break;
		}
	}
	

	public final void message() {
		final byte indexFace = getNum();
		switch (indexFace) {
			case DICE_MESSAGE_1:
				switch (type) {
					case DICE_GREEN:
						labelsMessage.setText(TEXT_MESSAGE_DICE_GREEN_1);
						break;
					case DICE_RED:
						labelsMessage.setText(TEXT_MESSAGE_DICE_RED_1);
						break;
					case DICE_YELLOW:
						labelsMessage.setText(TEXT_MESSAGE_DICE_YELLOW_1);
						break;
				}

				break;
			case DICE_MESSAGE_2:
				switch (type) {
					case DICE_GREEN:
						labelsMessage.setText(TEXT_MESSAGE_DICE_GREEN_2);
						break;
					case DICE_RED:
						labelsMessage.setText(TEXT_MESSAGE_DICE_RED_2);
						break;
					case DICE_YELLOW:
						labelsMessage.setText(TEXT_MESSAGE_DICE_YELLOW_2);
						break;
				}
				break;
			case DICE_MESSAGE_3:
				switch (type) {
					case DICE_GREEN:
						labelsMessage.setText(TEXT_MESSAGE_DICE_GREEN_3);
						break;
					case DICE_RED:
						labelsMessage.setText(TEXT_MESSAGE_DICE_RED_3);
						break;
					case DICE_YELLOW:
						labelsMessage.setText(TEXT_MESSAGE_DICE_YELLOW_3);
						break;
				}
				break;
			case DICE_MESSAGE_4:
				switch (type) {
					case DICE_GREEN:
						labelsMessage.setText(TEXT_MESSAGE_DICE_GREEN_4);
						break;
					case DICE_RED:
						labelsMessage.setText(TEXT_MESSAGE_DICE_RED_4);
						break;
					case DICE_YELLOW:
						labelsMessage.setText(TEXT_MESSAGE_DICE_YELLOW_4);
						break;
				}

				break;
			case DICE_MESSAGE_5:
				switch (type) {
					case DICE_GREEN:
						labelsMessage.setText(TEXT_MESSAGE_DICE_GREEN_5);
						break;
					case DICE_RED:
						labelsMessage.setText(TEXT_MESSAGE_DICE_RED_5);
						break;
					case DICE_YELLOW:
						labelsMessage.setText(TEXT_MESSAGE_DICE_YELLOW_5);
						break;
				}
			break;

			case DICE_MESSAGE_6:
				switch (type) {
					case DICE_GREEN:
						labelsMessage.setText(TEXT_MESSAGE_DICE_GREEN_6);
					break;
					case DICE_RED:
						labelsMessage.setText(TEXT_MESSAGE_DICE_RED_6);
					break;
					case DICE_YELLOW:
						labelsMessage.setText(TEXT_MESSAGE_DICE_YELLOW_6);
					break;
				}
			break;
		}
		labelsMessage.setSize( labelsMessage.getWidth(), labelsMessage.getTextTotalHeight() );
	}
	

	public final byte getState() {
		return state;
	}


	public final void setState(int state) {
		this.state = ( byte ) state;

		switch (state) {
			case STATE_WAITING:
				faceIndex = -1;
				setVisible(false);
				setShinesVisible( false );
				labelsMessage.setVisible(false);
			break;
				
			case STATE_RUNNING:
				setShinesVisible( false );
				setVisible(true);
				labelsMessage.setVisible(false);				
			break;

			case STATE_RESULT:
				setShinesVisible( true );
				setSpeed( 0, 0 );
				dice.setFrame( 0 );
				message();
				labelsMessage.setPosition( ( getWidth() - labelsMessage.getWidth() ) >> 1, ( getHeight() - labelsMessage.getHeight() ) >> 1 );
				labelsMessage.setVisible(true);
				setVisible(true);
				dice.setSequence( SEQUENCE_FLASH );
			break;
		}
	}


	public final void blankDice() {
		dice.setSequence( SEQUENCE_BLANK );
	}


	public final void born( Point pos, int position ) {
		final int START = MIN_SPEED_MODULE + ( MAX_SPEED_MODULE - MIN_SPEED_MODULE ) * position / DADOS_TOTAL;
		final int vel = START + NanoMath.randInt( MAX_SPEED_MODULE - START );

//		final Point destination = new Point( NanoMath.randInt( ScreenManager.SCREEN_WIDTH ), NanoMath.randInt( ScreenManager.SCREEN_HEIGHT ) );
		final Point destination = new Point( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		final Point direction = destination.sub( pos );
//		direction.normalize();
//		direction.mulEquals( vel );
		setSpeed( direction.x, direction.y );

		setRefPixelPosition( pos );
		
		setState( STATE_RUNNING );
	}


	public Image getDiceSprite()
	{
		return dice.getCurrentFrameImage();
	}


	public final boolean testCollision(Dice d) {
		return state != STATE_WAITING && !testColisionWall( false ) && getCollisionArea().intersects( d.getCollisionArea() );
	}


	public final boolean testColisionWall( boolean handleCollision ) {
		final Rectangle r = getCollisionArea();
		boolean collides = false;
		
		if ( r.x + r.width > ScreenManager.SCREEN_WIDTH || r.x < 0 )
			collides = true;
		else if ( r.y + r.height > ScreenManager.SCREEN_HEIGHT || r.y < 0)
			collides = true;

		if ( collides && handleCollision ) {
			treatCollisionWall();
		}
		return collides;
	}


	public final boolean treatCollisionWall() {
		final Rectangle r = getCollisionArea();
		boolean collides = false;
		final Point newSpeed = new Point( speedX.getSpeed(), speedY.getSpeed() );

		if ( r.x < 0 && speedX.getSpeed() < 0 ) {
			setPosition( -r.x, getPosY() );
			newSpeed.x = NanoMath.toInt(NanoMath.mulFixed(FP_FRICTION, NanoMath.toFixed( speedX.getSpeed() ) ) );
			collides = true;
		} else if ( r.x + r.width > ScreenManager.SCREEN_WIDTH && speedX.getSpeed() > 0 ) {
			move( ScreenManager.SCREEN_WIDTH - ( r.x + r.width ), 0 );
			newSpeed.x = NanoMath.toInt(NanoMath.mulFixed(FP_FRICTION, NanoMath.toFixed(speedX.getSpeed() ) ) );
			collides = true;
		}

		if ( r.y < 0 && speedY.getSpeed() < 0 ) {
			setPosition(getPosX(), -r.y );
			newSpeed.y = NanoMath.toInt(NanoMath.mulFixed(FP_FRICTION, NanoMath.toFixed(speedY.getSpeed() ) ) );
			collides = true;
		} else if ( r.y + r.height > ScreenManager.SCREEN_HEIGHT && speedY.getSpeed() > 0 ) {
			move( 0, ScreenManager.SCREEN_HEIGHT - ( r.y + r.height ) );
			newSpeed.y = NanoMath.toInt( NanoMath.mulFixed( FP_FRICTION, NanoMath.toFixed( speedY.getSpeed() ) ) );
			collides = true;
		}

		// TODO teste!
		if ( testColisionWall( false ) ) {
			int a = 0;
		}

		if ( collides ) {
			// atualiza a animação de acordo
			setSpeed( newSpeed.x, newSpeed.y );
		}

		return collides;
	}


	private final void setSpeed( int x, int y ) {
		speedX.setSpeed( x );
		speedY.setSpeed( y );

		final int MODULE = NanoMath.sqrtInt( x * x + y * y );
		
		final short FRAME_TIME = ( short ) NanoMath.lerpInt( MAX_FRAME_TIME, MIN_FRAME_TIME, MODULE * 100 / MAX_SPEED_MODULE );
		final short[][] frameTimes = dice.getFrameTimes();
		for ( byte i = 0; i < frameTimes.length; ++i ) {
			for ( byte j = 0; j < frameTimes[ i ].length; ++j ) {
				frameTimes[ i ][ j ] = FRAME_TIME;
			}
		}

		// o +1 é só para evitar divisão por zero
		final int FP_SIN = Math.abs( NanoMath.divInt( y, 1 + MODULE ) );
		final boolean RIGHT = x >= 0;
		final boolean UP = y <= 0;

		int trans = TRANS_NONE;
		byte sequence = dice.getSequenceIndex();
		
		if ( FP_SIN < NanoMath.HALF ) {
			// ângulo entre 0 e 30 graus
			if ( RIGHT )
				sequence = SEQUENCE_RIGHT;
			else
				sequence = SEQUENCE_LEFT;
		} else if ( FP_SIN < FP_SQRT3_2 ) {
			// ângulo entre 30 e 60 graus
			if ( UP ) {
				sequence = SEQUENCE_UP_RIGHT;
				if ( !RIGHT )
					trans = TRANS_MIRROR_H;
			} else {
				sequence = SEQUENCE_DOWN_LEFT;
				if ( RIGHT )
					trans = TRANS_MIRROR_H;
			}
		} else {
			// ângulo entre 60 e 90 graus
			if ( UP )
				sequence = SEQUENCE_UP;
			else
				sequence = SEQUENCE_DOWN;
		}
		
		if ( sequence != dice.getSequenceIndex() )
			dice.setSequence( sequence );
		
		dice.setTransform( trans );
	}


	public static final void setTwist( int twist, boolean vibrate ) {
		if ( twist != Dice.twist ) {
			Dice.twist = ( byte ) twist;
			if ( vibrate )
				MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
		}
	}


	public final byte getFaceIndex() {
		return faceIndex;
	}


	public final void setFaceIndex( int i ) {
		faceIndex = (byte) i;
	}


	private final Rectangle getCollisionArea() {
		final int frameIndex = dice.getCurrFrameIndex();
		return new Rectangle( getPosX() + dice.getFrameOffset( frameIndex ).x,
							  getPosY() + dice.getFrameOffset( frameIndex ).y,
							  getDiceSprite().getWidth(),
							  getDiceSprite().getHeight() );
	}


}
