///**
// * ResultScreen.java
// *
// * Created on 23/Nov/2009, 11:24:09
// *
// */
//package screens;
//
//import br.com.nanogames.components.DrawableGroup;
//import br.com.nanogames.components.DrawableImage;
//import br.com.nanogames.components.ImageFont;
//import br.com.nanogames.components.Label;
//import br.com.nanogames.components.Pattern;
//import br.com.nanogames.components.RichLabel;
//import br.com.nanogames.components.UpdatableGroup;
//import br.com.nanogames.components.online.NanoOnline;
//import br.com.nanogames.components.userInterface.KeyListener;
//import br.com.nanogames.components.userInterface.ScreenListener;
//import br.com.nanogames.components.userInterface.ScreenManager;
//import core.Constants;
//import core.GameMIDlet;
//import br.com.nanogames.components.userInterface.PointerListener;
//import br.com.nanogames.components.util.MediaPlayer;
//import core.Tag;
//
///**
// *
// * @author Peter
// */
//public final class ResultScreen extends DrawableGroup implements Constants {
//
//	private byte indexGreen;
//	private byte indexRed;
//	private byte indexYellow;
//	private final RichLabel labelsMessageYellow;
//	private final Label finalMessage;
//	private final Tag tagAgain;
//	private final RichLabel sentence;
//	private final Label conectivo;
//	private final Label sentencePref;
//	private final ImageFont fontSentence;
//	private final ImageFont fontButon;
//
//
//	public ResultScreen() throws Exception {
//		super(15);
//
//		fontButon = GameMIDlet.getFont(FONT_MENU);
//		fontSentence = GameMIDlet.getFont(FONT_TITLE);
//		sentence = new RichLabel(fontSentence, "   ", ScreenManager.SCREEN_WIDTH);
//		sentencePref = new Label(fontSentence, "");
//		conectivo = new Label(fontButon, "  ");
//		finalMessage = new Label(fontSentence, TEXT_MESSAGE);
//		tagAgain = new Tag(Tag.MODE_SOFT_KEY_LEFT, GameMIDlet.getText( TEXT_AGAIN) );
//		insertDrawable(sentence);
//		insertDrawable(finalMessage);
//		insertDrawable(tagAgain);
//
//		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
//	}
//
//	public final void setIndexFaceA(final byte index) {
//		indexGreen = index;
//	}
//
//	public final void setIndexFaceB(final byte index) {
//		indexRed = index;
//	}
//
//	public final void setIndexFaceC(final byte index) {
//		indexYellow = index;
//	}
//
//
//	public final void messages() {
//		byte tipo = -1;
//		byte a = 0;
//		switch (indexGreen) {
//			case DICE_MESSAGE_1:
//				labelsMessageGreen.setText(TEXT_MESSAGE_DICE_GREEN_1);
//				conectivo.setText(TEXT_MESSAGE_NOS);
//				break;
//			case DICE_MESSAGE_2:
//				labelsMessageGreen.setText(TEXT_MESSAGE_DICE_GREEN_2);
//				conectivo.setText(TEXT_MESSAGE_NAS);
//				tipo = a;
//				break;
//			case DICE_MESSAGE_3:
//				labelsMessageGreen.setText(TEXT_MESSAGE_DICE_GREEN_3);
//				sentencePref.setText(TEXT_MESSAGE_LICK);
//				conectivo.setText(TEXT_MESSAGE_NO);
//				break;
//			case DICE_MESSAGE_4:
//				labelsMessageGreen.setText(TEXT_MESSAGE_DICE_GREEN_4);
//				conectivo.setText(TEXT_MESSAGE_NA);
//				tipo = a;
//				break;
//			case DICE_MESSAGE_5:
//				labelsMessageGreen.setText(TEXT_MESSAGE_DICE_GREEN_5);
//				conectivo.setText(TEXT_MESSAGE_NA);
//				break;
//			case DICE_MESSAGE_6:
//				labelsMessageGreen.setText(TEXT_MESSAGE_DICE_GREEN_6);
//				conectivo.setText(TEXT_MESSAGE_NA);
//				tipo = a;
//				break;
//		}
//		switch (indexRed) {
//			case DICE_MESSAGE_1:
//				labelsMessageRed.setText(TEXT_MESSAGE_DICE_RED_1);
//				sentencePref.setText(TEXT_MESSAGE_KISS);
//				break;
//			case DICE_MESSAGE_2:
//				labelsMessageRed.setText(TEXT_MESSAGE_DICE_RED_2);
//				sentencePref.setText(TEXT_MESSAGE_QUESTION);
//				break;
//			case DICE_MESSAGE_3:
//				labelsMessageRed.setText(TEXT_MESSAGE_DICE_RED_3);
//				sentencePref.setText(TEXT_MESSAGE_BITE);
//				tipo = a;
//				break;
//			case DICE_MESSAGE_4:
//				labelsMessageRed.setText(TEXT_MESSAGE_DICE_RED_4);
//				sentencePref.setText(TEXT_MESSAGE_LICK);
//				tipo = a;
//				break;
//			case DICE_MESSAGE_5:
//				labelsMessageRed.setText(TEXT_MESSAGE_DICE_RED_5);
//				sentencePref.setText(TEXT_MESSAGE_BLOW);
//				break;
//			case DICE_MESSAGE_6:
//				labelsMessageRed.setText(TEXT_MESSAGE_DICE_RED_6);
//				sentencePref.setText(TEXT_MESSAGE_MASSAGE);
//				break;
//		}
//
//		byte soundIndex = -1;
//		switch (indexYellow) {
//			case DICE_MESSAGE_1:
//				labelsMessageYellow.setText(TEXT_MESSAGE_DICE_YELLOW_1);
//				soundIndex = SOUND_RESULT_HOT;
//				break;
//			case DICE_MESSAGE_2:
//				labelsMessageYellow.setText(TEXT_MESSAGE_DICE_YELLOW_2);
//				if (tipo == a) {
//					labelsMessageYellow.setText(TEXT_MESSAGE_ROMANTICA);
//				}
//				soundIndex = SOUND_RESULT_MEDIUM;
//				break;
//			case DICE_MESSAGE_3:
//				labelsMessageYellow.setText(TEXT_MESSAGE_DICE_YELLOW_3);
//				if (tipo == a) {
//					labelsMessageYellow.setText(TEXT_MESSAGE_OUSADA);
//				}
//				soundIndex = SOUND_RESULT_HOT;
//				break;
//			case DICE_MESSAGE_4:
//				labelsMessageYellow.setText(TEXT_MESSAGE_DICE_YELLOW_4);
//				soundIndex = SOUND_RESULT_LIGHT;
//				break;
//			case DICE_MESSAGE_5:
//				labelsMessageYellow.setText(TEXT_MESSAGE_DICE_YELLOW_5);
//				if (tipo == a) {
//					labelsMessageYellow.setText(TEXT_MESSAGE_TIMIDA);
//				}
//				soundIndex = SOUND_RESULT_LIGHT;
//				break;
//			case DICE_MESSAGE_6:
//				labelsMessageYellow.setText(TEXT_MESSAGE_DICE_YELLOW_6);
//				if (tipo == a) {
//					labelsMessageYellow.setText(TEXT_MESSAGE_MEIGA);
//				}
//				soundIndex = SOUND_RESULT_MEDIUM;
//				break;
//		}
//		if ( soundIndex >= 0 )
//			MediaPlayer.play( soundIndex );
//
//		labelsMessageGreen.setPosition(dicegreen.getPosX() + dicegreen.getWidth() / 2 - labelsMessageGreen.getWidth() / 2, dicegreen.getPosY() + dicegreen.getHeight() / 2 - labelsMessageGreen.getHeight() / 2);
//		labelsMessageRed.setPosition(diceRed.getPosX() + diceRed.getWidth() / 2 - labelsMessageRed.getWidth() / 2, diceRed.getPosY() + diceRed.getHeight() / 2 - labelsMessageRed.getHeight() / 2);
//		labelsMessageYellow.setPosition(diceYellow.getPosX() + diceYellow.getWidth() / 2 - labelsMessageYellow.getWidth() / 2, diceYellow.getPosY() + diceYellow.getHeight() / 2 - labelsMessageYellow.getHeight() / 2);
//
//     if (GameMIDlet.getLanguage() == NanoOnline.LANGUAGE_pt_BR) {
//			sentence.setText(sentencePref.getText() + labelsMessageYellow.getText() + conectivo.getText() + labelsMessageGreen.getText());
//		}else if (GameMIDlet.getLanguage() == NanoOnline.LANGUAGE_en_US) {
//		  sentence.setText( labelsMessageYellow.getText() + sentencePref.getText()  + conectivo.getText() + labelsMessageGreen.getText());
//	 }
//		a = -1;
//	}
//
//
//	public final void reset() {
//		indexGreen = -1;
//		indexRed = -1;
//		indexYellow = -1;
//	}
//
//
//	public final void setSize(int width, int height) {
//		super.setSize(width, height);
//
//		final int toalSpace = width - diceRed.getWidth() * 3;
//		final int space = toalSpace / 4;
//
//		diceRed.setPosition(space, height / 2 - dicegreen.getHeight() / 2);
//		diceYellow.setPosition(diceRed.getPosX() + diceRed.getWidth() + space, diceRed.getPosY());
//		dicegreen.setPosition(diceYellow.getPosX() + diceYellow.getWidth() + space, diceYellow.getPosY());
//		messages();
//		sentence.setPosition(width / 2 - sentence.getWidth() / 2, dicegreen.getPosY() - 2 * sentence.getHeight());
//		sentence.setSize(sentence.getWidth(), height / 3);
//		finalMessage.setPosition(width / 2 - finalMessage.getWidth() / 2, diceYellow.getPosY() + diceYellow.getHeight() + 2 * finalMessage.getHeight());
//
//		tagAgain.setPosition( tagAgain.getPosX(), getHeight() - tagAgain.getHeight() - GameMIDlet.getSpacing() );
//	}
//
//
//}
