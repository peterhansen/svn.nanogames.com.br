/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import core.GameMIDlet;

/**
 *
 * @author Edson
 */
public final class Bkg extends UpdatableGroup implements Constants, ScreenListener {

	private final Pattern centro;
	private final Pattern vertical;
	private final Pattern horizontal;
	private final Pattern verticalD;
	private final Pattern horizontalB;
	private final DrawableImage cantoDInf;
	private final DrawableImage cantoDSup;
	private final DrawableImage cantoESup;
	private final DrawableImage cantoEInf;
	private final DrawableImage logo;

	
	public Bkg() throws Exception {
		super(10);
		
		centro = new Pattern(new DrawableImage(GameMIDlet.getPath( PATH_IMAGES, true ) + "wall_top.png"));
		vertical = new Pattern(new DrawableImage(GameMIDlet.getPath( PATH_BKG, true ) + "vertical.png"));
		horizontal = new Pattern(new DrawableImage(GameMIDlet.getPath( PATH_BKG, true ) + "horizontal.png"));

		logo = new DrawableImage ( GameMIDlet.getPath( PATH_SPLASH, true ) + "title_pink.png" );

		verticalD = new Pattern(new DrawableImage( ( ( DrawableImage ) vertical.getFill() ) ) );
		horizontalB = new Pattern(new DrawableImage( ( ( DrawableImage ) horizontal.getFill() ) ) );
		verticalD.getFill().mirror(TRANS_MIRROR_H);
		horizontalB.getFill().mirror(TRANS_MIRROR_V);

		cantoESup = new DrawableImage(GameMIDlet.getPath( PATH_BKG, true ) + "canto.png");
		cantoDSup = new DrawableImage( cantoESup );
		cantoDSup.mirror(TRANS_MIRROR_H);

		cantoDInf = new DrawableImage( cantoESup );
		cantoDInf.setTransform(TRANS_ROT180);

		cantoEInf = new DrawableImage( cantoESup );
		cantoEInf.setTransform(TRANS_ROT270);

		insertDrawable(centro);
		insertDrawable(vertical);
		insertDrawable(horizontal);
		insertDrawable(horizontalB);
		insertDrawable(cantoDInf);
		insertDrawable(cantoDSup);
		insertDrawable(verticalD);
		insertDrawable(cantoEInf);
		insertDrawable(cantoESup);
		insertDrawable(logo);

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
	}

	public final void hideNotify() {
	}

	public final void showNotify() {
	}


	public final void setLogoVisible( boolean v ) {
		logo.setVisible( v );
	}
	

	public final void sizeChanged(int width, int height) {
		if ( width != getWidth() || height != getHeight() )
			setSize( width, height );
	}

	public final void setSize(int width, int height) {
		super.setSize(width, height);
		cantoDSup.setPosition(width - cantoDSup.getWidth(), 0);
		cantoDInf.setPosition(width - cantoDInf.getWidth(), height - cantoDInf.getHeight());
		cantoEInf.setPosition(0, height - cantoEInf.getHeight());

		horizontal.setSize(width - 2 * cantoESup.getWidth(), horizontal.getHeight());
		horizontal.setPosition(cantoESup.getPosX() + cantoESup.getWidth(), 0);

		vertical.setSize(vertical.getWidth(), height - 2 * cantoDInf.getHeight());
		vertical.setPosition(0, cantoESup.getPosY() + cantoESup.getHeight());

		horizontalB.setSize(width - 2 * cantoEInf.getWidth(), horizontal.getHeight());
		horizontalB.setPosition(cantoEInf.getPosX() + cantoEInf.getWidth(), height - horizontal.getHeight());

		verticalD.setSize(vertical.getWidth(), height - 2 * cantoDInf.getHeight());

		verticalD.setPosition(width - verticalD.getWidth(), cantoDSup.getPosY() + cantoDSup.getHeight());

		centro.setSize(width - 2 * vertical.getWidth(), height - 2 * horizontal.getHeight());
		centro.setPosition(vertical.getPosX() + vertical.getWidth(), horizontal.getPosY() + horizontal.getHeight());
		logo.setPosition(width/2-logo.getWidth()/2, logo.getHeight() - logo.getHeight()/2);
	}
}
