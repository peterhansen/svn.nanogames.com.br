/**
 * SplashGame.java
 * ©2007 Nano Games
 */
package screens;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GameMIDlet;
import core.SprayImage;
import javax.microedition.lcdui.Graphics;

/**
 * @author Edson
 */
public final class SplashGame extends UpdatableGroup implements Constants, KeyListener, SpriteListener
		//#if TOUCH == "true"
		// Não há aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do código)
		, PointerListener
		//#endif
{
	private static final byte STATE_INITIAL			= 0;
	private static final byte STATE_TEXT_APPEARS	= 1;
	private static final byte STATE_CAN_APPEARS		= 2;
	private static final byte STATE_SPRAY			= 3;
	private static final byte STATE_CAN_HIDE		= 4;
	private static final byte STATE_TITLE_ANIMATE	= 5;
	private static final byte STATE_END				= 6;

	private static final short TIME_TITLE_ANIMATE	= 500;
	private static final short TIME_SPRAY_ANIMATE	= 1000;
	private static final short TIME_CAN_ANIMATE		= 1500;

	private final DrawableImage logo;

	private final SprayImage logoSpray;

	private final Sprite shine;

	private byte state;

	private short accTime;

	private final RichLabel title;

	private int LOGO_FINAL_Y;

	private final BezierCurve sprayMovement = new BezierCurve();

	private final DrawableImage can;
	

	/** Construtor */
	public SplashGame(ImageFont font) throws Exception {
		super(10);

		can = GameMIDlet.getBkg().getCan();

		logo = GameMIDlet.getBkg().getLogo();
		insertDrawable(logo);

		logoSpray = new SprayImage( new DrawableImage( logo ), TIME_SPRAY_ANIMATE );
		insertDrawable( logoSpray );

		title = new RichLabel( FONT_TEXT, TEXT_AXE_PRESENTS, ScreenManager.SCREEN_WIDTH );
		insertDrawable(title);

		shine = new Sprite( GameMIDlet.getPath( ROOT_DIR ) + "ts" );
		shine.setListener( this, 0 );
		shine.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		shine.setVisible( false );
		insertDrawable( shine );

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		setState( STATE_INITIAL );
	}

	
	private final void setState(int state) {
		this.state = ( byte ) state;
		accTime = 0;

		switch ( state ) {
			case STATE_INITIAL:
				logo.setVisible( false );
			break;

			case STATE_TEXT_APPEARS:
				emit();
				title.setVisible( true );
			break;

			case STATE_CAN_APPEARS:
				sprayMovement.setValues( new Point( -can.getWidth(), getHeight() ),
										 new Point( logo.getPosX() + logo.getWidth(), logo.getPosY() + logo.getHeight() ),
										 new Point( logo.getPosX(), logo.getPosY() + logo.getHeight() ),
										 new Point( logo.getPosX(), logo.getPosY() + ( logo.getHeight() >> 1 ) ) );
			break;

			case STATE_SPRAY:
				MediaPlayer.play( MediaPlayer.support( MediaPlayer.MEDIA_TYPE_MP3 ) ? SOUND_SPRAY_MP3 : SOUND_SPRAY_MIDI );
				logoSpray.setActive( true );
				title.setVisible( false );
			break;

			case STATE_CAN_HIDE:
				MediaPlayer.stop();
				
				sprayMovement.setValues( new Point( can.getRefPixelPosition() ),
										 new Point( getSize() ), 
										 new Point( 0, getHeight() ),
										 new Point( logo.getPosX(), GameMIDlet.getBkg().getCanY() ) );

				logo.setVisible( true );
				logoSpray.setVisible( false );
				logoSpray.setActive( false );
			break;

			case STATE_TITLE_ANIMATE:
			break;

			case STATE_END:
				logo.setPosition( ( getWidth() - logo.getWidth() ) >> 1, LOGO_FINAL_Y );
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		}
	}


	private final void emit() {
		shine.setSequence( 1 );
		shine.setVisible( true );
		shine.setRefPixelPosition( title.getPosX() + NanoMath.randInt( title.getWidth() ),
								   title.getPosY() + NanoMath.randInt( title.getHeight() ) );
	}

	public final void update(int delta) {
		super.update(delta);

		accTime += delta;
		final Point p = new Point();

		switch ( state ) {
			case STATE_INITIAL:
				setState( STATE_TEXT_APPEARS );
			break;

			case STATE_TEXT_APPEARS:
				if ( accTime >= 700 )
					setState( STATE_CAN_APPEARS );
			break;

			case STATE_CAN_APPEARS:
				sprayMovement.getPointAtFixed( p, NanoMath.divInt( accTime, TIME_CAN_ANIMATE ) );
				can.setRefPixelPosition( p );

				if ( accTime >= TIME_CAN_ANIMATE )
					setState( STATE_SPRAY );
			break;

			case STATE_SPRAY:
				can.setRefPixelPosition( logoSpray.getPosX() + logoSpray.getMaxX(), can.getPosY() );

				if ( accTime >= TIME_SPRAY_ANIMATE )
					setState( STATE_CAN_HIDE );
			break;

			case STATE_CAN_HIDE:
				sprayMovement.getPointAtFixed( p, NanoMath.divInt( accTime, TIME_CAN_ANIMATE ) );
				can.setRefPixelPosition( p );

				if ( accTime >= TIME_CAN_ANIMATE )
					setState( STATE_TITLE_ANIMATE );
			break;

			case STATE_TITLE_ANIMATE:
				if ( accTime < TIME_TITLE_ANIMATE ) {
					logo.setPosition( ( getWidth() - logo.getWidth() ) >> 1,
								NanoMath.lerpInt( getHeight() >> 2, LOGO_FINAL_Y, accTime * 100 / TIME_TITLE_ANIMATE ) );
				} else {
					setState( STATE_END );
				}
			break;
		}
	}


	public final void keyPressed(int key) {
		switch (key) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.exit();
				break;

			default:
				//#if DEBUG == "true"
					callNextScreen();
				//#endif
		}
	}


	public final void keyReleased(int key) {
	}


	private final void callNextScreen() {
		AppMIDlet.setScreen(GameMIDlet.SCREEN_MAIN_MENU);
	}


	//#if TOUCH == "true"
		public final void onPointerDragged(int x, int y) {
		}

		public final void onPointerPressed(int x, int y) {
			keyPressed(0);
		}

		public final void onPointerReleased(int x, int y) {
		}
	//#endif

		
	public final void setSize(int width, int height) {
		super.setSize(width, height);
		
		logo.setPosition( ( getWidth() - logo.getWidth() ) >> 1, getHeight() >> 2 );
		logoSpray.setPosition( logo.getPosition() );

		title.setSize( getWidth(), title.getTextTotalHeight() );
		title.setPosition( 0, logo.getPosY() - title.getHeight() );

		LOGO_FINAL_Y = 0;
	}


	public final void onSequenceEnded(int id, int sequence) {
		if ( title.isVisible() ) {
			emit();
		} else {
			shine.setSequence( 0 );
			shine.setVisible( false );
		}
	}


	public final void onFrameChanged(int id, int frameSequenceIndex) {
	}
}
