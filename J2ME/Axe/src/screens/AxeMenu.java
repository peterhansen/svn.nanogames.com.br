/**
 * AxeMenu.java
 *
 * Created on 3:09:08 PM.
 *
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.GameMIDlet;
import core.Tag;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author Peter
 */
public final class AxeMenu extends BasicMenu implements ScreenListener, Constants {

	private final Drawable[] entries;

	private short scrollY;

	private final byte ENTRY_HEIGHT;

	private boolean playTheme;

	private final DrawableImage arrowDown;
	private final DrawableImage arrowUp;


	private AxeMenu( MenuListener listener, int index, Drawable[] entries, int spacing ) throws Exception {
		super( listener, index, entries, spacing, 0, entries.length - 1, null );
		this.entries = entries;

		arrowDown = new DrawableImage( GameMIDlet.getPath( PATH_IMAGES, true ) + "arrow.png" );
		arrowUp = new DrawableImage( arrowDown );

		arrowUp.mirror( TRANS_MIRROR_V );
		arrowDown.setClipTest( false );
		arrowUp.setClipTest( false );

		ENTRY_HEIGHT = ( byte ) ( entries[ 0 ].getHeight() + GameMIDlet.getSpacing() );
		autoSize();
		showNotify();
	}


	public static final AxeMenu createInstance( int index, int[] entries ) throws Exception {
		final String[] stringEntries = new String[ entries.length ];
		for ( byte i = 0; i < stringEntries.length; ++i )
			stringEntries[ i ] = GameMIDlet.getText( entries[ i ] );

		return createInstance( index, stringEntries );
	}


	public final void setPlayTheme( boolean play ) {
		playTheme = play;
		showNotify();
	}


	public static final AxeMenu createInstance( int index, String[] entries ) throws Exception {
		final Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			final Tag t = new Tag( Tag.MODE_FIXED_WIDTH_RIGHT, i );
			menuEntries[ i ] = t;
			t.setText( entries[ i ] );
		}

		final AxeMenu menu = new AxeMenu( ( GameMIDlet ) GameMIDlet.getInstance(), index, menuEntries, GameMIDlet.getSpacing() );
		return menu;
	}


	public final void setSize( int width, int height ) {
		autoSize();
	}


	public final void keyPressed(int key) {
		super.keyPressed(key);
		
		switch ( key ) {
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_SOFT_LEFT:
				arrowDown.setVisible( false );
				arrowUp.setVisible( false );
			break;
		}
	}

	public final void onPointerPressed(int x, int y) {
		if ( y < getPosY() )
			keyPressed( ScreenManager.UP );
		else if ( y >= getPosY() + getHeight() )
			keyPressed( ScreenManager.DOWN );
		else
			super.onPointerPressed(x, y);
	}


	public final void autoSize() {
		if ( entries != null ) {
			int y = 0;

			for ( int i = 0; i < entries.length; ++i ) {
				entries[ i ].setRefPixelPosition( entries[ i ].getPosX(), y );

				y += ENTRY_HEIGHT;
			}

			final int LOGO_Y = GameMIDlet.getBkg().getLogoY();
			final int HEIGHT = Math.min( ScreenManager.SCREEN_HEIGHT - LOGO_Y - 20, y );

			size.set( ScreenManager.SCREEN_WIDTH, ( HEIGHT / ENTRY_HEIGHT * ENTRY_HEIGHT ) );

			arrowDown.setVisible( HEIGHT < y );
			arrowUp.setVisible( HEIGHT < y );

			setPosition( 0, ScreenManager.SCREEN_HEIGHT - getHeight() - GameMIDlet.getSpacing() - ( arrowUp.isVisible() ? arrowUp.getHeight() : 0 ) );

			arrowUp.setPosition( getWidth() - entries[ 0 ].getWidth() - GameMIDlet.getSpacing() + ( ( entries[ 0 ].getWidth() - arrowDown.getWidth() ) >> 1 ), -arrowUp.getHeight() << 1 );
			arrowDown.setPosition( arrowUp.getPosX(), getHeight() );

			if ( arrowDown.isVisible() )
				move( 0, -arrowDown.getHeight() );

			// atualiza o cursor (largura do texto pode mudar)
			setCurrentIndex( currentIndex );
		}
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() )
			GameMIDlet.setScreen( menuId );
	}


	public final void showNotify() {
		if ( playTheme && !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_INTRO, MediaPlayer.LOOP_INFINITE );
	}


	public final void setCurrentIndex( int index ) {
		final int previousIndex = currentIndex;

		if ( index >= 0 && index < activeDrawables ) {
			currentIndex = index;

			if ( listener != null )
				listener.onItemChanged( this, menuId, index );

			updateCursorPosition();
		} // fim if ( index != currentIndex && index >= 0 && index < activeDrawables )

		if ( currentIndex <= firstEntry ) {
			if ( previousIndex < firstEntry || previousIndex == activeDrawables - 1 )
				super.setCurrentIndex( firstEntry );
			else if ( previousIndex == firstEntry && currentIndex < firstEntry ) {
				if ( firstEntry == 0 )
					super.setCurrentIndex( 0 );
				else
					super.setCurrentIndex( circular ? activeDrawables - 1 : firstEntry );
			}
		}

		// grava o índice atual para futuras instâncias de menu com o mesmo id
		LAST_INDEX.put( new Integer( menuId ), new Integer( currentIndex ) );

		if ( ENTRY_HEIGHT > 0 ) {
			final Drawable entry = getDrawable( currentIndex );
			final int relativeY = entry.getPosY();
			if ( relativeY < 0 ) {
				scrollY = ( short ) -entry.getPosY();
			} else if ( relativeY + ENTRY_HEIGHT > getHeight() ) {
				scrollY = ( short ) ( getHeight() - relativeY - ENTRY_HEIGHT );
			} else {
				scrollY = 0;
			}
			scrollY = ( short ) ( scrollY * ENTRY_HEIGHT / ENTRY_HEIGHT );
		}
	}


	public final void draw( Graphics g ) {
		if ( visible ) {
			if ( clipTest ) {
				pushClip( g );
				translate.addEquals( position );

				final Rectangle clip = clipStack[ currentStackSize ];
				clip.setIntersection( translate.x, translate.y, size.x, size.y );

				if ( clip.width > 0 && clip.height > 0 ) {
					g.setClip( clip.x, clip.y, clip.width, clip.height );

					translate.addEquals( 0, scrollY );
					paint( g );
					translate.subEquals( 0, scrollY );
				}
				
				g.setClip( -1000, -1000, 2000, 2000 );
				arrowUp.draw( g );
				arrowDown.draw( g );

				translate.subEquals( position );
				popClip( g );
			} else {
				// desenha direto, ignorando o teste de interseção
				translate.addEquals( position );
				paint( g );
				translate.subEquals( position );
			}
		} // fim if ( visible )        
	}

}
