/**
 * PlayScreen.java
 */
package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GameMIDlet;
import core.Tag;
import game.Dice;

/**
 * ©2007 Nano Games
 * @author Edson
 */
public final class PlayScreen extends UpdatableGroup implements KeyListener, Constants, ScreenListener, PointerListener {

	private final Bkg bkg;
	private final BezierCurve messageSpeed = new BezierCurve();
	private final UpdatableGroup messageGroup;
	private final RichLabel message;
	protected int stateGame;
	private final ImageFont fonts;

	private static final short TIME_ANIMATION = 600;
	
	protected static final byte STATE_WAITING	= 0;
	protected static final byte STATE_SHAKING	= 1;
	protected static final byte STATE_RUNNING	= 2;
	protected static final byte STATE_RESULT_1	= 3;
	protected static final byte STATE_RESULT_2	= 4;

	private byte state;

	public static final byte MESSAGE_STATE_HIDDEN		= 0;
	public static final byte MESSAGE_STATE_APPEARING	= 1;
	public static final byte MESSAGE_STATE_SHOWN		= 2;
	public static final byte MESSAGE_STATE_HIDING		= 3;

	private byte messageState;

	private short accMessageTime;

	private short accResultTime;

	private static final short TIME_RESULT_ANIM = 1200;
	private static final short TIME_RESULT = 2000;

	private final Dice[] dices = new Dice[DADOS_TOTAL];
	private final BezierCurve[] diceCurves = new BezierCurve[ DADOS_TOTAL ];
	private final Tag softKeyRight;
	private final Tag tagAgain;

	private final UpdatableGroup gameGroup;

	private final byte SHAKE_OFFSET = 3;

	private final Label finalMessage;

	private final RichLabel sentence;

	private final Pattern trama;

	
	public PlayScreen() throws Exception {
		super( 15 );

		gameGroup = new UpdatableGroup( 15 );
		insertDrawable( gameGroup );
		
		bkg = new Bkg();
		fonts = GameMIDlet.getFont(FONT_TITLE);
		message = new RichLabel( fonts, TEXT_START, ScreenManager.SCREEN_WIDTH );
		
		messageGroup = new UpdatableGroup( 2 );
		final Pattern black = new Pattern( 0x000000 );
		black.setSize( 5000, 5000 );
		messageGroup.insertDrawable(black);
		messageGroup.insertDrawable(message);
		messageGroup.setSize( 0, message.getHeight() );

		gameGroup.insertDrawable(bkg);

		trama = new Pattern(new DrawableImage(GameMIDlet.getPath(ROOT_DIR) + "trama.png"));
		insertDrawable( trama );

		for (byte i = 0; i < DADOS_TOTAL; ++i) {
			dices[i] = new Dice((byte) (i % 3) );
			insertDrawable(dices[i]);
			diceCurves[ i ] = new BezierCurve();
		}

		finalMessage = new Label(FONT_TITLE, TEXT_MESSAGE);
		insertDrawable( finalMessage );

		sentence = new RichLabel( FONT_TITLE, null, 0 );
		insertDrawable( sentence );

		tagAgain = new Tag(Tag.MODE_SOFT_KEY_LEFT, GameMIDlet.getText( TEXT_AGAIN) );
		tagAgain.setVisible( false );
		insertDrawable(tagAgain);

		softKeyRight = new Tag(Tag.MODE_SOFT_KEY_RIGHT, GameMIDlet.getText(TEXT_EXIT));
		insertDrawable(softKeyRight);

		insertDrawable( messageGroup );
		
		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
		setMessageState( MESSAGE_STATE_HIDDEN );

		reset();
	}
	

	public final void reset() {
		setState( STATE_WAITING );
	}


	private final void setMessageState( byte state ) {
		this.messageState = state;
		accMessageTime = 0;

		switch ( state ) {
			case MESSAGE_STATE_HIDDEN:
				messageGroup.setPosition( 0, getHeight() );
				messageGroup.setVisible( false );
			break;

			case MESSAGE_STATE_APPEARING:
				messageGroup.setVisible( true );
				messageSpeed.setValues(	new Point( 0, messageGroup.getRefPixelY() ),
										new Point( 0, getHeight() - ( messageGroup.getHeight() >> 2 ) ),
										new Point( 0, getHeight() - ( messageGroup.getHeight() >> 1 ) ),
										new Point( 0, getHeight() - messageGroup.getHeight() ) );
			break;

			case MESSAGE_STATE_SHOWN:
				messageGroup.setVisible( true );
				messageGroup.setPosition( 0, getHeight() - messageGroup.getHeight() );
			break;

			case MESSAGE_STATE_HIDING:
				messageGroup.setVisible( true );
				messageSpeed.setValues(	new Point( 0, messageGroup.getRefPixelY() ),
										new Point( 0, getHeight() - ( messageGroup.getHeight() >> 1 ) ),
										new Point( 0, getHeight() - ( messageGroup.getHeight() >> 2 ) ),
										new Point( 0, getHeight() ) );
			break;
		}
	}
	

	public final void update(int delta) {
		super.update(delta);

		for (int i = 0; i < DADOS_TOTAL; i++) {
			for (int j = i - 1; j >= 0; --j) {
				if (dices[i].testCollision(dices[j])) {
					dices[i].treatCollision(dices[j], delta);
				}
			}
		}

		switch ( state ) {
			case STATE_SHAKING:
				gameGroup.setPosition( -SHAKE_OFFSET + NanoMath.randInt( SHAKE_OFFSET << 1 ), -SHAKE_OFFSET + NanoMath.randInt( SHAKE_OFFSET << 1 ) );
			break;

			case STATE_RESULT_1:
				accResultTime += delta;
				
				final int FP_PROGRESS = NanoMath.divInt( accResultTime, TIME_RESULT_ANIM );
				final Point p = new Point();
				for ( byte i = 0; i < DADOS_TOTAL; ++i ) {
					diceCurves[ i ].getPointAtFixed( p, FP_PROGRESS );
					dices[ i ].setRefPixelPosition( p );
				}

				if ( FP_PROGRESS >= NanoMath.ONE )
					setState( STATE_RESULT_2 );
			break;

			case STATE_RUNNING:
				byte stopped = 0;
				for (int i = 0; i < DADOS_TOTAL; i++) {
					if ( dices[ i ].getState() == Dice.STATE_RESULT )
						++stopped;
				}
				if ( stopped == DADOS_TOTAL ) {
					accResultTime += delta;
					if ( accResultTime >= TIME_RESULT_ANIM )
						setState( STATE_RESULT_1 );
				}
			break;

			case STATE_RESULT_2:
				accResultTime = (short) Math.min( accResultTime + delta, TIME_RESULT );
			break;
		}

		switch ( messageState ) {
			case MESSAGE_STATE_APPEARING:
			case MESSAGE_STATE_HIDING:
				accMessageTime += delta;
				final int FP_PERCENT = NanoMath.divInt( accMessageTime, TIME_ANIMATION );

				messageGroup.setPosition( 0, messageSpeed.getYFixed( FP_PERCENT ) );

				if ( FP_PERCENT >= NanoMath.ONE ) {
					setMessageState( messageState == MESSAGE_STATE_APPEARING ? MESSAGE_STATE_SHOWN : MESSAGE_STATE_HIDDEN );
				}
			break;
		}
	}


	public final void setState( byte state ) {
		this.state = state;

		switch ( state ) {
			case STATE_WAITING:
				gameGroup.setPosition( 0, 0 );
				
				Dice.setTwist( Dice.TWIST_NONE, false );
				setMessageState( MESSAGE_STATE_APPEARING );
				for (byte i = 0; i < DADOS_TOTAL; ++i) {
					dices[i].setState(Dice.STATE_WAITING);
				}
				trama.setVisible( false );
				sentence.setVisible( false );
				finalMessage.setVisible( false );
				tagAgain.exit( -1 );

				bkg.setLogoVisible( true );

				softKeyRight.reset();
			break;

			case STATE_SHAKING:
				setMessageState( MESSAGE_STATE_HIDING );
				MediaPlayer.play( SOUND_SHAKE_DICE, MediaPlayer.LOOP_INFINITE );
				MediaPlayer.vibrate( VIBRATION_TIME_SHAKE_DICE );
			break;

			case STATE_RUNNING:
				accResultTime = 0;
				gameGroup.setPosition( 0, 0 );
				if (!ScreenManager.getInstance().hasPointerEvents()) {
					softKeyRight.exit(-1);
				}

				dices[ 0 ].born( new Point( -ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT ), 0 );
				dices[ 1 ].born( new Point( ScreenManager.SCREEN_HALF_WIDTH * 3, ScreenManager.SCREEN_HALF_HEIGHT ), 1 );
				dices[ 2 ].born( new Point( ScreenManager.SCREEN_HALF_WIDTH, -ScreenManager.SCREEN_HALF_HEIGHT ), 2 );

				MediaPlayer.play( MediaPlayer.support( MediaPlayer.MEDIA_TYPE_MP3 ) ? SOUND_DICE_MP3 : SOUND_DICE_MIDI );
			break;

			case STATE_RESULT_1:
				accResultTime = 0;
				softKeyRight.exit( -1 );

				for ( byte i = 0; i < DADOS_TOTAL; ++i ) {
					dices[ i ].blankDice();
					diceCurves[ i ].setValues( dices[ i ].getRefPixelPosition(),
											   new Point( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) ),
											   new Point( NanoMath.randInt( getWidth() ), NanoMath.randInt( getHeight() ) ),
											   new Point( getWidth() * ( i + 1 ) / ( DADOS_TOTAL + 1 ), ScreenManager.SCREEN_HALF_HEIGHT ) );
				}
			break;

			case STATE_RESULT_2:
				bkg.setLogoVisible( false );
				trama.setVisible( true );
				sentence.setVisible( true );
				finalMessage.setVisible( true );
				tagAgain.reset();
				if (!ScreenManager.getInstance().hasPointerEvents()) {
					softKeyRight.reset();
				}

				accResultTime = 0;
				refreshSentence();

				MediaPlayer.play( SOUND_INTRO, MediaPlayer.LOOP_INFINITE );
			break;
		}
	}


	public final void keyPressed(int key) {
		switch (key) {
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				softKeyRight.exit(SCREEN_MAIN_MENU);
			break;

			default:
				switch ( state ) {
					case STATE_WAITING:
						setState( STATE_SHAKING );
					case STATE_RUNNING:
						switch ( key ) {
							case ScreenManager.KEY_NUM1:
							case ScreenManager.KEY_NUM2:
							case ScreenManager.KEY_NUM3:
								Dice.setTwist( Dice.TWIST_LIGHT + ( key - ScreenManager.KEY_NUM1 ), state == STATE_RUNNING );
							break;
						}
					break;

					case STATE_RESULT_2:
						if ( accResultTime >= TIME_RESULT ) {
							reset();
						}
					break;
				}
			break;
		}
	}


	public final void keyReleased(int key) {
		switch ( state ) {
			case STATE_SHAKING:
				setState( STATE_RUNNING );
			break;
		}
	}


	private final void refreshSentence() {
		int conectivo = -1;
		int sentencePref = -1;
		int twistString = -1;
		byte soundIndex = -1;
		boolean masculine = true;

		switch ( dices[ DICE_GREEN ].getFaceIndex() ) {
			case DICE_MESSAGE_1:
				conectivo = TEXT_MESSAGE_NOS;
			break;

			case DICE_MESSAGE_2:
				conectivo = TEXT_MESSAGE_NAS;
			break;

			case DICE_MESSAGE_3:
				conectivo = TEXT_MESSAGE_NO;
			break;

			case DICE_MESSAGE_4:
			case DICE_MESSAGE_6:
			case DICE_MESSAGE_5:
				conectivo = TEXT_MESSAGE_NA;
			break;
		}


		switch ( dices[ DICE_RED ].getFaceIndex() ) {
			case DICE_MESSAGE_1:
				sentencePref = TEXT_MESSAGE_KISS;
			break;

			case DICE_MESSAGE_2:
				sentencePref = TEXT_MESSAGE_QUESTION;
			break;

			case DICE_MESSAGE_3:
				sentencePref = TEXT_MESSAGE_BITE;
				masculine = false;
			break;

			case DICE_MESSAGE_4:
				sentencePref = TEXT_MESSAGE_LICK;
				masculine = false;
			break;

			case DICE_MESSAGE_5:
				sentencePref = TEXT_MESSAGE_BLOW;
			break;

			case DICE_MESSAGE_6:
				sentencePref = TEXT_MESSAGE_MASSAGE;
				masculine = false;
			break;
		}

		switch ( dices[ DICE_YELLOW ].getFaceIndex() ) {
			case DICE_MESSAGE_1:
				twistString = TEXT_MESSAGE_DICE_YELLOW_1;
				soundIndex = SOUND_RESULT_HOT;
			break;

			case DICE_MESSAGE_2:
				twistString = masculine ? TEXT_MESSAGE_DICE_YELLOW_2 : TEXT_MESSAGE_ROMANTICA;
				soundIndex = SOUND_RESULT_MEDIUM;
			break;

			case DICE_MESSAGE_3:
				twistString = masculine ? TEXT_MESSAGE_DICE_YELLOW_3 : TEXT_MESSAGE_OUSADA;
				soundIndex = SOUND_RESULT_HOT;
			break;

			case DICE_MESSAGE_4:
				twistString = TEXT_MESSAGE_DICE_YELLOW_4;
				soundIndex = SOUND_RESULT_MEDIUM;
			break;

			case DICE_MESSAGE_5:
				twistString = masculine ? TEXT_MESSAGE_DICE_YELLOW_5 : TEXT_MESSAGE_TIMIDA;
				soundIndex = SOUND_RESULT_LIGHT;
			break;

			case DICE_MESSAGE_6:
				twistString = masculine ? TEXT_MESSAGE_DICE_YELLOW_6 : TEXT_MESSAGE_MEIGA;
				soundIndex = SOUND_RESULT_LIGHT;
			break;
		}

		final String greenString = GameMIDlet.getText( TEXT_MESSAGE_DICE_GREEN_1 + dices[ DICE_GREEN ].getFaceIndex() );

		switch ( GameMIDlet.getLanguage() ) {
			case NanoOnline.LANGUAGE_en_US:
				sentence.setText( GameMIDlet.getText( twistString ) + GameMIDlet.getText( sentencePref ) + GameMIDlet.getText( conectivo ) + greenString );
			break;

			case NanoOnline.LANGUAGE_pt_BR:
			default:
				sentence.setText( GameMIDlet.getText( sentencePref ) + GameMIDlet.getText( twistString ) + GameMIDlet.getText( conectivo ) + greenString );
		}
		refreshSentencePos();

		//#if DEBUG == "true"
			System.out.println( "SENTENCE: " + sentence.getText() );
		//#endif

//		if ( soundIndex >= 0 )
//			MediaPlayer.play( soundIndex ); // TODO sons removidos temporariamente
	}


	public final void hideNotify() {
		keyReleased( ScreenManager.FIRE );
	}


	public final void showNotify() {
	}


	public final void sizeChanged(int width, int height) {
		if ( getWidth() != width || getHeight() != height )
			setSize( width, height );
	}
	

	public final void setSize(int width, int height) {
		super.setSize(width, height);

		bkg.setSize(width, height);
		gameGroup.setSize(width, height);
		message.setSize( width, message.getTextTotalHeight() );
		messageGroup.setSize( width, message.getHeight() );
		trama.setSize( getSize() );

		switch ( state ) {
			case STATE_WAITING:
				setMessageState( MESSAGE_STATE_APPEARING );
			break;

			case STATE_RESULT_1:
			case STATE_RESULT_2:
				setState( state );
			break;

			default:
				setMessageState( MESSAGE_STATE_HIDDEN );
		}

		softKeyRight.setPosition(softKeyRight.getPosX(), getHeight() - softKeyRight.getHeight() - GameMIDlet.getSpacing() );
		softKeyRight.setMoving( true );
		tagAgain.setPosition( tagAgain.getPosX(), softKeyRight.getPosY() );
		tagAgain.setMoving( true );

		refreshSentencePos();
		finalMessage.setPosition( ( width - finalMessage.getWidth() ) >> 1, ( getHeight() * 3 / 4 ) - ( finalMessage.getHeight() >> 1 ) );

		switch ( state ) {
			case STATE_RESULT_1:
			case STATE_RESULT_2:
				setState( state );
			break;
		}
	}


	private final void refreshSentencePos() {
		sentence.setSize( getWidth() * 9 / 10, sentence.getTextTotalHeight() );
		sentence.setPosition( ( getWidth() - sentence.getWidth() ) >> 1, Math.max( 0, ( getHeight() >> 2 ) - ( sentence.getHeight() >> 1 ) ) );
	}
	

	public final void onPointerDragged(int x, int y) {
	}


	public final void onPointerPressed(int x, int y) {
		if ( x >= softKeyRight.getPosX() && y >= softKeyRight.getPosY() ) {
			keyPressed(ScreenManager.KEY_BACK);
		} else {
			final int area = y * 6 / getHeight();
			
			switch ( area ) {
				case 0:
				case 1:
				case 2:
					keyPressed( ScreenManager.KEY_NUM3 - area );
				break;

				default:
					keyPressed( ScreenManager.FIRE );
			}

		}
	}

	public final void onPointerReleased(int x, int y) {
		keyReleased( ScreenManager.FIRE );
	}
}

	