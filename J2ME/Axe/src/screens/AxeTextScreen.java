/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import core.GameMIDlet;
import core.Tag;

/**
 *
 * @author Peter
 */
public final class AxeTextScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener, PointerListener {

	private final ScrollRichLabel label;

	private final int nextScreenIndex;

	private final Tag tag;

	private boolean autoScroll;


	public AxeTextScreen( int textIndex, int nextScreenIndex, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		this( GameMIDlet.getText( textIndex ), nextScreenIndex, autoScroll, specialChars );
	}


	public AxeTextScreen( String text, int nextScreenIndex, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		super( 5 );

		this.nextScreenIndex = nextScreenIndex;
		this.autoScroll = autoScroll;

		label = new ScrollRichLabel( new RichLabel( GameMIDlet.getFont( FONT_TEXT ), text, 
									GameMIDlet.getScreenSize() == SCREEN_SIZE_SMALL ? ScreenManager.SCREEN_WIDTH : ScreenManager.SCREEN_WIDTH * 8 / 10,
									specialChars ) );
		insertDrawable( label );

		label.setAutoScroll( autoScroll );

		tag = new Tag( Tag.MODE_SOFT_KEY_RIGHT, GameMIDlet.getText( TEXT_BACK ) );
		insertDrawable( tag );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final ScrollRichLabel getLabel() {
		return label;
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				tag.exit( nextScreenIndex );
			return;

			default:
				label.keyPressed( key );
		} // fim switch ( key )
	} // fim do método keyPressed( int )


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() )
			setSize( width, height );
	}


	public final void hideNotify() {
	}


	public final void showNotify() {
	}


	public final void onPointerDragged(int x, int y) {
		label.onPointerDragged( x - label.getPosX(), y - label.getPosY() );
	}


	public final void onPointerPressed(int x, int y) {
		if ( x >= tag.getPosX() && y >= tag.getPosY() )
			keyPressed( ScreenManager.KEY_CLEAR );
		else
			label.onPointerPressed( x - label.getPosX(), y - label.getPosY() );
	}


	public final void onPointerReleased(int x, int y) {
		label.onPointerReleased( x - label.getPosX(), y - label.getPosY() );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		label.setSize( GameMIDlet.getScreenSize() == SCREEN_SIZE_SMALL ? width : width * 8 / 10, autoScroll ? height : height * 8 / 10 );
		label.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		label.setRefPixelPosition( getWidth() >> 1, getHeight() >> 1 );

		tag.setPosition( tag.getPosX(), getHeight() - tag.getHeight() - GameMIDlet.getSpacing() );
		tag.reset();
	}


	public final void keyReleased(int key) {
		label.keyReleased( key );
	}

}
