/**
 * Player.java
 * �2008 Nano Games.
 *
 * Created on Mar 28, 2008 10:32:03 AM.
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.PaletteMap;
import screens.Match;


/**
 * 
 * @author Peter
 */
public abstract class Player extends Sprite implements Constants {
	
	protected static Match match;
	
	
	protected Player( String pathDat, String pathImages, int index, PaletteMap[] map ) throws Exception {
		super( pathDat + index + ".dat", pathImages + index, map );
	}
	
	
	protected Player( Sprite player ) {
		super( player );
		setVisible( false );
	}
	
	
	protected Player( String pathDat, String pathImages, int index ) throws Exception {
		super( pathDat + index + ".dat", pathImages + index );
		
		setVisible( false );
	}
	
	
	public void update( int delta ) {
		delta = match.getDelta();
		
		super.update( delta );
	}	
	
	
	public static final void setMatch( Match match ) {
		Player.match = match;
	}
	
	
}
