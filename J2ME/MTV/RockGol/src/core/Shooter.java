/**
 * Shooter.java
 * �2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:15:41 AM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class Shooter extends Player {
	
	
	public static final byte SEQUENCE_STOPPED	= 0;
	public static final byte SEQUENCE_SHOOTING	= 1;
	public static final byte SEQUENCE_SHOT		= 2;
	
	/** �ndice de batedor: Cleston original */
	public static final byte INDEX_CLESTON_ORIGINAL = 0;
	/** �ndice de batedor: Cleston com cores alternativas 1 */
	public static final byte INDEX_CLESTON_1 = 4;
 	/** �ndice de batedor: Cleston com cores alternativas 2 */
 	public static final byte INDEX_CLESTON_2 = 8;	
	
	//#if JAR == "min"
//# 	/** �ndice de batedor: "Bob Marley original" */
//# 	public static final byte INDEX_REGGAE_1 = 1;
//# 	/** �ndice de batedor: "Bob Marley alternativo 2" */
//# 	public static final byte INDEX_REGGAE_2 = 2;
//# 	/** �ndice de batedor: "Bob Marley alternativo 3" */
//# 	public static final byte INDEX_REGGAE_3 = 3;
//# 	/** �ndice de batedor: "Bob Marley alternativo 4" */
//# 	public static final byte INDEX_REGGAE_4 = 5;
//# 	/** �ndice de batedor: Bob Marley com cores alternativas 5 */
//# 	public static final byte INDEX_REGGAE_5 = 6;
//# 	/** �ndice de batedor: "Bob Marley alternativo 6" */
//# 	public static final byte INDEX_REGGAE_6 = 7;
//# 	/** �ndice de batedor: "Bob Marley alternativo 7" */
//# 	public static final byte INDEX_REGGAE_7 = 9;
//# 	/** �ndice de batedor: Bob Marley com cores alternativas 8 */
//# 	public static final byte INDEX_REGGAE_8 = 10;
//# 	/** �ndice de batedor: "Bob Marley alternativo 9" */
//# 	public static final byte INDEX_REGGAE_9 = 11;
	//#else
	/** �ndice de batedor: "Jordan" no Lakers */
	public static final byte INDEX_NBA_LAKERS = 1;
	/** �ndice de batedor: "Michael Jackson" da cor que veio ao mundo */
	public static final byte INDEX_MICHAEL_BLACK = 2;
	/** �ndice de batedor: "Bob Marley original" */
	public static final byte INDEX_REGGAE_1 = 3;
	/** �ndice de batedor: "Jordan" no Knicks */
	public static final byte INDEX_NBA_KNICKS = 5;
	/** �ndice de batedor: "Michael Jackson" nem t�o preto assim */
	public static final byte INDEX_MICHAEL_MEDIUM = 6;
	/** �ndice de batedor: "Bob Marley alternativo 1" */
	public static final byte INDEX_REGGAE_2 = 7;
	/** �ndice de batedor: "Jordan" no Bulls */
	public static final byte INDEX_NBA_BULLS = 9;
	/** �ndice de batedor: "Michael Jackson" atual */
	public static final byte INDEX_MICHAEL_WHITE = 10;
	/** �ndice de batedor: "Bob Marley alternativo 2" */
	public static final byte INDEX_REGGAE_3 = 11;
	//#endif
	
	
	public Shooter( Sprite cleston ) {
		super( cleston );
	}
	
	
	/**
	 * 
	 * @param index
	 * @throws java.lang.Exception
	 */
	private Shooter( int index ) throws Exception {
		super( GameMIDlet.isLowMemory() ? PATH_SHOOTERS_LOW : PATH_SHOOTERS, PATH_SHOOTERS, index );
	}	
	
	
	/**
	 * 
	 * @param index
	 * @throws java.lang.Exception
	 */
	private Shooter( int index, PaletteMap[] map ) throws Exception {
		super( GameMIDlet.isLowMemory() ? PATH_SHOOTERS_LOW : PATH_SHOOTERS, PATH_SHOOTERS, index, map );
	}
	
	
	private static final byte getSpriteIndex( int index ) {
		//#if JAR == "min"
//# 		return ( byte ) ( ( index & 3 ) == 0 ? 0 : 3 );
		//#else
		return ( byte ) ( index & 3 );
		//#endif
	}
	
	
	private static final PaletteMap[] getPaletteMap( int index ) {
		switch ( index ) {
			case INDEX_CLESTON_1:
				return new PaletteMap[] {
					new PaletteMap( 0xff0000, 0xffdd00 ), // camisa e mei�o claros
					new PaletteMap( 0xce0000, 0xbb9900 ), // camisa e mei�o escuros
					new PaletteMap( 0x001152, 0x4a0052 ), // short claro
					new PaletteMap( 0x000a30, 0x2b0030 ), // short escuro
				};				
			
			case INDEX_CLESTON_2:
				return new PaletteMap[] {
					new PaletteMap( 0xff0000, 0xffff00 ), // camisa e mei�o claros
					new PaletteMap( 0xce0000, 0xdddd00 ), // camisa e mei�o escuros
					new PaletteMap( 0x001152, 0x0033bb ), // short claro
					new PaletteMap( 0x000a30, 0x000799 ), // short escuro
				};			
				
			case INDEX_REGGAE_2:
				return new PaletteMap[] {
					new PaletteMap( 0xffcc00, 0x108b00 ), // amarelo claro
					new PaletteMap( 0xdf9300, 0x0b5f00 ), // amarelo escuro
					new PaletteMap( 0xe40000, 0x602b00 ), // cal�a clara
					new PaletteMap( 0xa70000, 0x3f1c00 ), // cal�a escura
				//#if JAR == "min"
//# 					new PaletteMap( 0x3a3939, 0x909090 ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0x666666 ), // cabelo escuro
				//#else
					new PaletteMap( 0x393939, 0x909090 ), // cabelo claro
					new PaletteMap( 0x1d1d1d, 0x666666 ), // cabelo escuro
				//#endif
					new PaletteMap( 0xffa04c, 0xdfb04c ), // pele clara
					new PaletteMap( 0xce782c, 0xae652c ), // pele escura
				};				
				
			case INDEX_REGGAE_3:
				return new PaletteMap[] {
					new PaletteMap( 0xffcc00, 0xfeffff ), // amarelo claro
					new PaletteMap( 0xdf9300, 0xc9eeee ), // amarelo escuro
					new PaletteMap( 0x108b00, 0xfeffff ), // verde claro
					new PaletteMap( 0x0b5f00, 0xc9eeee ), // verde escuro
					new PaletteMap( 0xe40000, 0xfeffff ), // cal�a clara
					new PaletteMap( 0xa70000, 0xc9eeee ), // cal�a escura
					new PaletteMap( 0x3a3939, 0xdedddd ), // cabelo claro
					new PaletteMap( 0x1e1d1d, 0xa9cccc ), // cabelo escuro
					new PaletteMap( 0x393939, 0xfeffff ), // sapato claro
					new PaletteMap( 0x1d1d1d, 0xc9eeee ), // sapato escuro
					new PaletteMap( 0xffa04c, 0xb44c00 ), // pele clara
					new PaletteMap( 0xce782c, 0x702f00 ), // pele escura
				};

			//#if JAR == "min"
//# 				
//# 			case INDEX_REGGAE_4:
//# 				return new PaletteMap[] {
//# 					new PaletteMap( 0xffcc00, 0x363636 ), // amarelo claro
//# 					new PaletteMap( 0xdf9300, 0x1f1f1f ), // amarelo escuro
//# 					new PaletteMap( 0x108b00, 0x363636 ), // verde claro
//# 					new PaletteMap( 0x0b5f00, 0x1f1f1f ), // verde escuro
//# 					new PaletteMap( 0xe40000, 0xc500b0 ), // cal�a clara
//# 					new PaletteMap( 0xa70000, 0x88007a ), // cal�a escura
//# 					new PaletteMap( 0x3a3939, 0xda2c00 ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0xff5700 ), // cabelo escuro
//# 					new PaletteMap( 0x393939, 0x602b00 ), // sapato claro
//# 					new PaletteMap( 0x1d1d1d, 0x3f1c00 ), // sapato escuro
//# 				};
//# 				
//# 			case INDEX_REGGAE_5:
//# 				return new PaletteMap[] {
//# 					new PaletteMap( 0xffcc00, 0xf80000 ), // amarelo claro
//# 					new PaletteMap( 0xdf9300, 0xc20000 ), // amarelo escuro
//# 					new PaletteMap( 0x108b00, 0xf80000 ), // verde claro
//# 					new PaletteMap( 0x0b5f00, 0xc20000 ), // verde escuro
//# 					new PaletteMap( 0xe40000, 0x0028d0 ), // cal�a clara
//# 					new PaletteMap( 0xa70000, 0x001983 ), // cal�a escura
//# 					new PaletteMap( 0x3a3939, 0xc9eeee ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0xfeffff ), // cabelo escuro
//# 					new PaletteMap( 0x393939, 0x393939 ), // sapato claro
//# 					new PaletteMap( 0x1d1d1d, 0x1d1d1d ), // sapato escuro
//# 				};				
//# 
//# 			case INDEX_REGGAE_6:
//# 				return new PaletteMap[] {
//# 					new PaletteMap( 0xffcc00, 0xe40001 ), // amarelo claro
//# 					new PaletteMap( 0xdf9300, 0xa70001 ), // amarelo escuro
//# 					new PaletteMap( 0x108b00, 0xe40001 ), // verde claro
//# 					new PaletteMap( 0x0b5f00, 0xa70001 ), // verde escuro
//# 					new PaletteMap( 0xe40000, 0xfeffff ), // cal�a clara
//# 					new PaletteMap( 0xa70000, 0xc9eeee ), // cal�a escura
//# 					new PaletteMap( 0x3a3939, 0xc9eeee ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0xfeffff ), // cabelo escuro
//# 				};
//# 				
//# 			case INDEX_REGGAE_7:
//# 				return new PaletteMap[] {
//# 					new PaletteMap( 0xffcc00, 0xfeffff ), // amarelo claro
//# 					new PaletteMap( 0xdf9300, 0xc9eeee ), // amarelo escuro
//# 					new PaletteMap( 0xe40000, 0x108b00 ), // cal�a clara
//# 					new PaletteMap( 0xa70000, 0x0b5f00 ), // cal�a escura
//# 					new PaletteMap( 0x3a3939, 0xc9eeee ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0xfeffff ), // cabelo escuro
//# 					new PaletteMap( 0x393939, 0xc9eeee ), // sapato claro
//# 					new PaletteMap( 0x1d1d1d, 0xfeffff ), // sapato escuro
//# 				};
//# 				
//# 			case INDEX_REGGAE_8:
//# 				return new PaletteMap[] {
//# 					new PaletteMap( 0xffcc00, 0xfcffff ), // amarelo claro
//# 					new PaletteMap( 0xdf9300, 0xe2e2e2 ), // amarelo escuro
//# 					new PaletteMap( 0x108b00, 0xfcffff ), // verde claro
//# 					new PaletteMap( 0x0b5f00, 0xe2e2e2 ), // verde escuro
//# 					new PaletteMap( 0xe40000, 0x00e039 ), // cal�a clara
//# 					new PaletteMap( 0xa70000, 0x00a42a ), // cal�a escura
//# 					new PaletteMap( 0x3a3939, 0xffcc00 ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0xfff000 ), // cabelo escuro
//# 					new PaletteMap( 0x393939, 0x602b00 ), // sapato claro
//# 					new PaletteMap( 0x1d1d1d, 0x3f1c00 ), // sapato escuro
//# 
//# 				};							
//# 				
//# 			case INDEX_REGGAE_9:
//# 				return new PaletteMap[] {
//# 					new PaletteMap( 0xffffff, 0xffffcc ), // olhos e dentes
//# 					new PaletteMap( 0xdf9300, 0x108b00 ), // amarelo escuro
//# 					new PaletteMap( 0x108b00, 0xff5700 ), // verde claro
//# 					new PaletteMap( 0x0b5f00, 0xa70001 ), // verde escuro
//# 					new PaletteMap( 0xe40000, 0xc500b0 ), // cal�a clara
//# 					new PaletteMap( 0xa70000, 0x88007a ), // cal�a escura
//# 					new PaletteMap( 0x1e1d1d, 0x1e1d1d ), // cabelo escuro
//# 					new PaletteMap( 0x393939, 0x0028d0 ), // sapato claro
//# 					new PaletteMap( 0x1d1d1d, 0x001983 ), // sapato escuro
//# 					new PaletteMap( 0x3a3939, 0xe0e0dd ), // cabelo claro
//# 					new PaletteMap( 0x1e1d1d, 0xaaaa9a ), // cabelo escuro
//# 				};
//# 				
			//#else
			case INDEX_MICHAEL_BLACK:
				return new PaletteMap[] {
					new PaletteMap( 0xffa04c, 0x7d3500 ), // pele clara
					new PaletteMap( 0xf48a2d, 0x5e2800 ), // pele escura
				};
			
			case INDEX_MICHAEL_MEDIUM:
				return new PaletteMap[] {
					new PaletteMap( 0xffa04c, 0xce782c ), // pele clara
					new PaletteMap( 0xf48a2d, 0xab570c ), // pele escura
					new PaletteMap( 0x393939, 0xae0000 ), // casaco claro
					new PaletteMap( 0x151515, 0xff0000 ), // casaco escuro
					new PaletteMap( 0xffffff, 0x000000 ), // camisa clara					
					new PaletteMap( 0xc7d5ff, 0x151515 ), // camisa escura		
				};
			
			case INDEX_MICHAEL_WHITE:
				return new PaletteMap[] {
					new PaletteMap( 0xc7d5ff, 0x151516 ), // camisa escura		
					new PaletteMap( 0xffffff, 0x000000 ), // camisa clara
					new PaletteMap( 0x393939, 0xc7d5ff ), // casaco claro
					new PaletteMap( 0x151515, 0xffffff ), // casaco escuro
				};
			
			case INDEX_NBA_KNICKS:
				return new PaletteMap[] {
					new PaletteMap( 0xffcc00, 0x005b90 ), // camisa clara
					new PaletteMap( 0xdf9300, 0x003a5e ), // camisa escura
					new PaletteMap( 0x6c0090, 0xf47820 ), // parte originalmente roxa
					new PaletteMap( 0xfefefe, 0xf47820 ), // faixa
				};

			case INDEX_NBA_BULLS:
				return new PaletteMap[] {
					new PaletteMap( 0xffcc00, 0xd30000 ), // camisa clara
					new PaletteMap( 0xdf9300, 0x990000 ), // camisa escura
					new PaletteMap( 0x6c0090, 0x000000 ), // parte originalmente roxa
				};
			//#endif
				
			default:
				return null;
		}		
	}
	
	
	public static final Shooter createInstance( int index ) throws Exception {
		final byte playerIndex = getSpriteIndex( index );
		boolean mirror = false;
		switch ( index ) {
			//#if JAR != "min"
			case INDEX_NBA_LAKERS:
			//#endif
				
			case INDEX_CLESTON_ORIGINAL:
			case INDEX_REGGAE_1:
				return new Shooter( playerIndex );
				
			//#if JAR == "min"
//# 			case INDEX_REGGAE_6:
//# 			case INDEX_REGGAE_8:
			//#else
			case INDEX_MICHAEL_MEDIUM:
			case INDEX_NBA_KNICKS:
			//#endif
				
			case INDEX_REGGAE_3:
				mirror = true;
			default:
				final Shooter shooter = new Shooter( playerIndex, getPaletteMap( index ) );
				if ( mirror )
					shooter.setTransform( TRANS_MIRROR_H );
				
				return shooter;
		}
	}
	
	
	public static final DrawableImage getImage( int index ) throws Exception {
		final String path = PATH_SHOOTERS + getSpriteIndex( index ) + "_0.png";
		
		switch ( index ) {
			//#if JAR != "min"
			case INDEX_NBA_LAKERS:
			//#endif
				
			case INDEX_CLESTON_ORIGINAL:
			case INDEX_REGGAE_1:
				return new DrawableImage( path );
				
			default:
				PaletteChanger pc = new PaletteChanger( path );
				final DrawableImage image = new DrawableImage( pc.createImage( getPaletteMap( index ) ) );
				pc = null;
				
				return image;
		}		
	}


}
