package core;

import br.com.nanogames.components.util.NanoMath;

/*
 * Point3f.java
 *
 * Created on 21 de Agosto de 2007, 22:27
 *
 */

/**
 *
 * @author peter
 */
public final class XYZ {
	
	private static final byte BUFFER_SIZE = 32;
	private static final byte BUFFER_SIZE_MOD = BUFFER_SIZE - 1;
	
	private static final XYZ[] buffer = new XYZ[ BUFFER_SIZE ];
	private static int bufferIndex;
	
	/** Coordenada x do ponto em nota��o de ponto fixo. */
	public int x;
	/** Coordenada y do ponto em nota��o de ponto fixo. */
	public int y;
	/** Coordenada z do ponto em nota��o de ponto fixo. */
	public int z;
	
	
	static {
		for ( int i = 0; i < buffer.length; ++i )
			buffer[ i ] = new XYZ();
	}
	
	
	public XYZ() {
	}
	
	
	public XYZ( XYZ xyz ) {
		this( xyz.x, xyz.y, xyz.z );
	}
	
	
	public XYZ( int x ) {
		set( x, 0, 0 );
	}

	
	public XYZ( int x, int y ) {
		set( x, y, 0 );
	}

	
	public XYZ( int x, int y, int z ) {
		set( x, y, z );
	}
	
	
	public final void set() {
		set( 0, 0, 0 );
	}
	
	
	public final void set( int x ) {
		set( x, 0, 0 );
	}
	
	
	public final void set( int x, int y ) {
		set( x, y, 0 );
	}	

	
	public final void set( int x, int y, int z ) {
		this.x = x;
		this.y = y; 
		this.z = z;
	}
	
	
	public final void set( XYZ xyz ) {
		set( xyz.x, xyz.y, xyz.z );
	}

	
	public final XYZ sub( XYZ p ) {
		final XYZ value = buffer[ bufferIndex ];
		value.set( x - p.x, y - p.y, z - p.z );
		
		bufferIndex = ( bufferIndex + 1 ) & BUFFER_SIZE_MOD;
		return value;
	}
	
	
	public final XYZ add( XYZ p ) {
		final XYZ value = buffer[ bufferIndex ];
		value.set( x + p.x, y + p.y, z + p.z );
		
		bufferIndex = ( bufferIndex + 1 ) & BUFFER_SIZE_MOD;
		return value;
	}

	
	public XYZ div( int d ) {
		final XYZ value = buffer[ bufferIndex ];
		if ( d != 0 )
			value.set( NanoMath.divFixed( x, d ), NanoMath.divFixed( y, d ), NanoMath.divFixed( z, d ) );
		else
			value.set( x, y, z );
		
		bufferIndex = ( bufferIndex + 1 ) & BUFFER_SIZE_MOD;
		return value;
	} // fim do operador /

	
	 public final XYZ mult( int m ) {
		 final XYZ value = buffer[ bufferIndex ];
		 value.set( NanoMath.mulFixed( x, m ), NanoMath.mulFixed( y, m ), NanoMath.mulFixed( z, m ) );

		bufferIndex = ( bufferIndex + 1 ) & BUFFER_SIZE_MOD;
		return value;
	 }

	 
	 public final void subEquals( XYZ p ) { 
		 x -= p.x; 
		 y -= p.y; 
		 z -= p.z; 
	 }
	 
	 
	 public final void addEquals( XYZ p ) { 
		 x += p.x; 
		 y += p.y; 
		 z += p.z;
	 }

	 
	 public void divEquals( int d ) {	
		if ( d != 0 )
			set( NanoMath.divFixed( x, d ), NanoMath.divFixed( y, d ), NanoMath.divFixed( z, d ) );
	} // fim do operador /=

	 
	 public final void multEquals( int m ) {
		 x = NanoMath.mulFixed( x, m ); 
		 y = NanoMath.mulFixed( y, m ); 
		 z = NanoMath.mulFixed( z, m ); 
	 }

	 
	// calcula o m�dulo do vetor
	public final int getModule() {
		return NanoMath.sqrtFixed( NanoMath.mulFixed( x, x ) + NanoMath.mulFixed( y, y ) + NanoMath.mulFixed( z, z ) ); 
	}

	
	// retorna o vetor normalizado
	public final XYZ getNormal() {
		final XYZ value = buffer[ bufferIndex ];
		value.set( this.div( getModule() ) );
		
		bufferIndex = ( bufferIndex + 1 ) & BUFFER_SIZE_MOD;
		return value;		
	}

	
	// retorna o produto vetorial por p
	public final XYZ cross( XYZ p ) { 
		final XYZ value = buffer[ bufferIndex ];
		
		value.set( NanoMath.mulFixed( y, p.z ) - NanoMath.mulFixed( z, p.y ), NanoMath.mulFixed( z, p.x ) - NanoMath.mulFixed( x, p.z ), NanoMath.mulFixed( x, p.y ) - NanoMath.mulFixed( y, p.x ) );
		
		bufferIndex = ( bufferIndex + 1 ) & BUFFER_SIZE_MOD;
		return value;				
	}

	
	// retorna o produto interno por p
	public final int dot( XYZ p ) { 
		return ( NanoMath.mulFixed( x, p.x ) + NanoMath.mulFixed( y, p.y ) + NanoMath.mulFixed( z, p.z ) ); 
	} 

	
	// normaliza o vetor
	public final void normalize() { 
		set( getNormal() ); 
	} 

	
}
