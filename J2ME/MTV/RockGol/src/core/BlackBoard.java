/**
 * BlackBoard.java
 * �2008 Nano Games.
 *
 * Created on Apr 14, 2008 8:21:28 PM.
 */
//#if JAR == "full"

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class BlackBoard extends DrawableGroup implements Constants {

	
	//#if SCREEN_SIZE == "SMALL"
//# 	private static final byte CHALK_TOTAL = 7;
	//#else
	private static final byte CHALK_TOTAL = 8;
	//#endif
	
	private static final byte TOTAL_ITEMS = 5 + CHALK_TOTAL;
	
	private final DrawableImage[] chalks;
	
	
	private final Pattern patternTop;
	private final Pattern patternFill;
	private final Pattern patternBottom;
	
	private final DrawableImage imgBottom;
	
	
	
	public BlackBoard() throws Exception {
		super( TOTAL_ITEMS );
		
		patternFill = new Pattern( BLACKBOARD_COLOR );
		insertDrawable( patternFill );
		
		//#if JAR == "full"
		
		chalks = new DrawableImage[ CHALK_TOTAL ];

		for ( byte i = 0; i < CHALK_TOTAL; ++i ) {
			chalks[ i ] = new DrawableImage( PATH_CHALKS + i + ".png" );
			insertDrawable( chalks[ i ] );
		}
		
		//#endif

		final String PATH = PATH_IMAGES + "board_";
		DrawableImage temp;
		
		temp = new DrawableImage( PATH + "top.png" );
		patternTop = new Pattern( temp );
		patternTop.setSize( 0, temp.getHeight() );
		insertDrawable( patternTop );
		
		
		temp = new DrawableImage( PATH + "bottom.png" );
		patternBottom = new Pattern( temp );
		patternBottom.setSize( 0, temp.getHeight() );
		insertDrawable( patternBottom );
		
		imgBottom = new DrawableImage( PATH + "img.png" );
		insertDrawable( imgBottom );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		patternTop.setSize( width, patternTop.getHeight() );
		
		patternFill.setSize( width, height - patternBottom.getHeight() );
		patternFill.setPosition( 0, patternTop.getHeight() );
		
		patternBottom.setSize( width - imgBottom.getWidth(), patternBottom.getHeight() );
		patternBottom.setPosition( imgBottom.getWidth(), height - patternBottom.getHeight() );
		
		imgBottom.setPosition( 0, height - imgBottom.getHeight() );
		
		//#if JAR == "full"
		
		// sorteia os rabiscos e suas posi��es
		for ( byte i = 0; i < CHALK_TOTAL; ++i ) {
			chalks[ i ].setVisible( false );
		}

		final byte MAX_CHALKS = ( byte ) ( Math.min( CHALK_TOTAL, Math.max( width, height ) / CHALK_DIVISOR ) );
		for ( byte i = 0; i < MAX_CHALKS; ++i ) {
			int randIndex;
			do {
				randIndex = NanoMath.randInt( CHALK_TOTAL );
			} while ( chalks[ randIndex ].isVisible() );

			final DrawableImage chalk = chalks[ randIndex ];
			chalk.setVisible( true );

			final int w = -chalk.getWidth() >> 1;
			final int h = -chalk.getHeight() >> 1;

			chalk.setPosition( ( w >> 1 ) + NanoMath.randInt( width - w ), 
							   ( h >> 1 ) + NanoMath.randInt( height - h ) );
		}
		
		//#endif
	}
	
	
	public final int getVisibleAreaY() {
		return patternTop.getHeight();
	}
	
	
	public final int getVisibleAreaHeight() {
		return patternBottom.getPosY() - patternTop.getHeight();
	}
	
	
}

//#endif