/**
 * Cray.java
 * �2008 Nano Games.
 *
 * Created on Apr 14, 2008 6:43:48 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class Cray extends DrawableGroup implements Constants {

	private static final byte TOTAL_ITEMS = 4;
	
	private final Pattern up;
	private final Pattern down;
	private final Pattern left;
	private final Pattern right;
	
	private final int LINE_WIDTH;
	
	
	public Cray() throws Exception {
		super( TOTAL_ITEMS );
		
		final DrawableImage imgHorizontal = new DrawableImage( PATH_IMAGES + "h.png" );
		
		//#if ROTATE_BUG == "true"
//# 		final DrawableImage imgVertical = new DrawableImage( PATH_IMAGES + "v.png" );
		//#else
		final DrawableImage imgVertical = new DrawableImage( imgHorizontal );
		imgVertical.setTransform( TRANS_ROT90 );
		imgVertical.defineReferencePixel( 0, 0 );
		//#endif
		
		LINE_WIDTH = imgVertical.getWidth();
		
		up = new Pattern( imgHorizontal );
		down = new Pattern( imgHorizontal );
		left = new Pattern( imgVertical );
		right = new Pattern( imgVertical );
		
		insertDrawable( up );
		insertDrawable( down );
		insertDrawable( left );
		insertDrawable( right );
	}


	public final void setTarget( Drawable d ) {
		final int WIDTH = LINE_WIDTH << 1;
		
		setSize( d.getWidth() + WIDTH, d.getHeight() + WIDTH );
		defineReferencePixel( LINE_WIDTH, LINE_WIDTH );
		
		setPosition( d.getPosX() - LINE_WIDTH, d.getPosY() - LINE_WIDTH );
		
		up.setSize( size.x, LINE_WIDTH );
		
		down.setSize( size.x, LINE_WIDTH );
		down.setPosition( 0, size.y - LINE_WIDTH );
		
		left.setSize( LINE_WIDTH, size.y );
		
		right.setSize( LINE_WIDTH, size.y );
		right.setPosition( size.x - LINE_WIDTH, 0 );
	}
	
	
	
}
