/**
 * IntroBox.java
 * �2008 Nano Games.
 *
 * Created on Apr 9, 2008 4:15:50 PM.
 */

package core;

import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class InfoBox extends GenericInfoBox implements Constants {
	
	public static final byte TYPE_PRESENTATION = 0;
	public static final byte TYPE_FINAL_RESULT = 1;

	private final RichLabel label;
	
	private final String team1Name;
	private final String team2Name;
	
	private final String team1ShortName;
	private final String team2ShortName;
	
	private final short[] score;
	
	
	public InfoBox( byte division, byte stage, Team team1, Team team2, short[] score ) throws Exception {
		super( 2 );
		
		team1Name = team1.getName();
		team2Name = team2.getName();
		
		//#if SCREEN_SIZE == "SMALL"
//# 		team1ShortName = team1.getShortName();
//# 		team2ShortName = team2.getShortName();
		//#else
		team1ShortName = team1Name;
		team2ShortName = team2Name;
		//#endif
		
		this.score = score;
		
		StringBuffer buffer = new StringBuffer( "<ALN_H>" );
		
		buffer.append( GameMIDlet.getText( TEXT_MTV_ROCKGOL ) );
		switch ( division ) {
			case DIVISION_VERSUS:
				buffer.append( GameMIDlet.getText( TEXT_VERSUS ) );
			break;
			
			case DIVISION_3:
				buffer.append( GameMIDlet.getText( TEXT_3RD_DIVISION ) );
			break;
			
			case DIVISION_2:
				buffer.append( GameMIDlet.getText( TEXT_2ND_DIVISION ) );
			break;
			
			case DIVISION_1:
				buffer.append( GameMIDlet.getText( TEXT_1ST_DIVISION ) );
			break;
		}
		buffer.append( '\n' );
		
		int indexStage;
		
		switch ( stage ) {
			case STAGE_SINGLE_MATCH:
				indexStage = TEXT_STAGE_SINGLE_MATCH;
			break;

			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
			case STAGE_4TH_ROUND:
			case STAGE_5TH_ROUND:
			case STAGE_6TH_ROUND:
				indexStage = TEXT_STAGE_FIRST_MATCH + ( stage - STAGE_1ST_ROUND );
			break;

			case STAGE_FINAL_MATCH_1:
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				indexStage = TEXT_STAGE_FINAL_MATCH_1 + ( stage - STAGE_FINAL_MATCH_1 );
			break;

			default:
				indexStage = -1;
		}
		
		if ( indexStage > 0 ) {
			buffer.append( GameMIDlet.getText( indexStage ) );
			buffer.append( '\n' );
		}
		buffer.append( '\n' );
		
		final RichLabel title = new RichLabel( GameMIDlet.getFont( FONT_INDEX_BOARD ), buffer.toString(), ScreenManager.SCREEN_WIDTH, null );
		insertDrawable( title );
		
		label = new RichLabel( GameMIDlet.getFont( FONT_INDEX_BOARD ), null, ScreenManager.SCREEN_WIDTH, null );
		label.setPosition( 0, title.getHeight() );
		insertDrawable( label );
	}
	
	
	public final void setType( byte type ) {
		final StringBuffer buffer = new StringBuffer( "<ALN_H>");

		switch ( type ) {
			case TYPE_PRESENTATION:
				buffer.append( team1Name );
				buffer.append( "\nX\n" );
				buffer.append( team2Name );
				buffer.append( "\n\n" );
			break;
			
			case TYPE_FINAL_RESULT:
				buffer.append( GameMIDlet.getText( TEXT_FINAL_RESULT ) );
				buffer.append( '\n' );

				buffer.append( team1ShortName );
				buffer.append( ' ' );
				buffer.append( score[ 0 ] );
				
				buffer.append( " X " );
				
				buffer.append( score[ 1 ] );
				buffer.append( ' ' );
				buffer.append( team2ShortName );
				
				buffer.append( "\n\n" );				
			break;
		}
		
		label.setText( buffer.toString() );
		label.setSize( ScreenManager.SCREEN_WIDTH, label.getTextTotalHeight() );
		
		setSize( ScreenManager.SCREEN_WIDTH, label.getPosY() + label.getHeight() );
		setPosition( 0, -size.y );
	}
	
	
}
