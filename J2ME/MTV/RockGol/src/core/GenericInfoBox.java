/**
 * GenericInfoBox.java
 * �2008 Nano Games.
 *
 * Created on Apr 6, 2008 11:52:59 AM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MUV;


/**
 * 
 * @author Peter
 */
public abstract class GenericInfoBox extends UpdatableGroup implements Constants {
	
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;
	
	protected byte state;
	
	/** Dura��o da transi��o (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 700;
	
	private static final int BKG_COLOR = 0x000000;
	
	protected final Pattern patternBkg = new Pattern( BKG_COLOR );

	protected final Pattern patternBottom;
	
	private int visibleTime;
	
	private final MUV speed = new MUV();
	
	
	protected GenericInfoBox( int nItems ) throws Exception {
		super( nItems + 2 );
		
		final DrawableImage bottomImage = new DrawableImage( PATH_IMAGES + "bottom.png" );
		patternBottom = new Pattern( bottomImage );
		patternBottom.setSize( 0, bottomImage.getHeight() );
		
		insertDrawable( patternBkg );
		insertDrawable( patternBottom );
		
		setState( STATE_HIDDEN );
	}
	
	
	public void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
				
				setPosition( 0, -size.y );
			break;
			
			case STATE_APPEARING:
				if ( state == STATE_SHOWN )
					return;
				
				speed.setSpeed( size.y * 1000 / TRANSITION_TIME, false );
				
				setVisible( true );
			break;
			
			case STATE_SHOWN:
				setVisible( true );
				
				setPosition( 0, 0 );
			break;
			
			case STATE_HIDING:
				if ( state == STATE_HIDDEN )
					return;
				
				speed.setSpeed( size.y * -1000 / TRANSITION_TIME, false );
				
				setVisible( true );
			break;
		}
		
		this.state = ( byte ) state;
	}


	public void update( int delta ) {
		switch ( state ) {
			case STATE_HIDDEN:
			return;
			
			case STATE_APPEARING:
				position.y += speed.updateInt( delta );
				
				if ( position.y >= 0 )
					setState( STATE_SHOWN );
			break;
			
			case STATE_SHOWN:
				if ( visibleTime > 0 ) {
					visibleTime -= delta;
					
					if ( visibleTime <= 0 ) {
						visibleTime = 0;
						setState( STATE_HIDING );
					}
				}
			break;
			
			case STATE_HIDING:
				position.y += speed.updateInt( delta );
				
				if ( position.y <= -size.y )
					setState( STATE_HIDDEN );
			break;
		}
		
		super.update( delta );
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height + patternBottom.getHeight() );
		
		patternBkg.setSize( width, height );
		patternBottom.setSize( width, patternBottom.getHeight() );
		patternBottom.setPosition( 0, height );
	}
	
	
	public final void setVisibleTime( int time ) {
		visibleTime = time;
	}
	
}
