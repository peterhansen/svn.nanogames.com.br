/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho das imagens das bandeiras dos times. */
	public static final String PATH_TEAMS = PATH_IMAGES + "teams/";
	
	/** Caminho das imagens dos batedores de p�nalti. */
	public static final String PATH_SHOOTERS = PATH_IMAGES + "shooters/";
	
	//#if SCREEN_SIZE == "SMALL"
//# 	/** Caminho das imagens dos batedores de p�nalti da vers�o de baixa mem�ria. */
//# 	public static final String PATH_SHOOTERS_LOW = PATH_SHOOTERS;
	//#else
	/** Caminho das imagens dos batedores de p�nalti da vers�o de baixa mem�ria. */
	public static final String PATH_SHOOTERS_LOW = PATH_SHOOTERS + "low_";
	//#endif
	
	/** Caminho das imagens dos goleiros. */
	public static final String PATH_KEEPERS = PATH_IMAGES + "keepers/";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Caminho das imagens do placar animado. */
	public static final String PATH_BOARD = PATH_IMAGES + "board/";
	
	/** Caminho das imagens de giz. */
	public static final String PATH_CHALKS = PATH_IMAGES + "chalks/";
	
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	
	/** �ndice do slot de grava��o das op��es na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �ndice do slot de grava��o de um campeonato salvo. */
	public static final byte DATABASE_SLOT_CHAMPIONSHIP = 2;
	
	/** �ndice do slot de grava��o dos replays. */
	public static final byte DATABASE_SLOT_REPLAYS = 3;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 3;
	
	/** Cor padr�o do fundo de tela. */
	public static final int BLACKBOARD_COLOR = 0x00482f;
	
	/** Offset da fonte padr�o. */
	public static final byte DEFAULT_FONT_OFFSET = -1;
	
	/** �ndice da fonte padr�o do jogo. */
	public static final byte FONT_INDEX_DEFAULT	= 0;
	
	/** �ndice da fonte do placar. */
	public static final byte FONT_INDEX_BOARD	= 1;
	
	/** �ndice da fonte utilizada para textos. */
	public static final byte FONT_INDEX_TEXT	= 2;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 3;
	
	/** N�mero total de times (incluindo o do jogador). */
	public static final byte TEAMS_NUMBER_TOTAL = 12;
	
	
	//�ndices dos sons do jogo
	//#if JAR == "min"
//# 	public static final byte SOUND_INDEX_HIT_BOARD	= 0;
//# 	public static final byte SOUND_INDEX_WHISTLE	= 1;
//# 	public static final byte SOUND_INDEX_PLAY_GOOD	= 2;
//# 	public static final byte SOUND_INDEX_PLAY_BAD	= 3;
//# 	public static final byte SOUND_INDEX_CHAMPION	= 4;
//# 	public static final byte SOUND_INDEX_POST		= 5;
//# 	public static final byte SOUND_INDEX_KICK_BALL	= 6;
//# 	
//# 	public static final byte SOUND_TOTAL = 7;
	//#else
	public static final byte SOUND_INDEX_SPLASH		= 0;
	public static final byte SOUND_INDEX_WHISTLE	= 1;
	public static final byte SOUND_INDEX_PLAY_GOOD	= 2;
	public static final byte SOUND_INDEX_PLAY_BAD	= 3;
	public static final byte SOUND_INDEX_CHAMPION	= 4;
	public static final byte SOUND_INDEX_LOSE_CUP	= 5;
	public static final byte SOUND_INDEX_KICK_BALL	= 6;
	public static final byte SOUND_INDEX_POST		= 7;
	public static final byte SOUND_INDEX_HIT_BOARD	= 8;
	
	public static final byte SOUND_TOTAL = 9;
	//#endif
	
	/** Dura��o da vibra��o ao atingir a trave, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 250;
	
	//#if DEMO == "true"
//# 	/** N�mero de jogadas iniciais dispon�veis na vers�o demo. */
//# 	public static final byte DEMO_MAX_PLAYS = 10;	
	//#endif
	
	// <editor-fold defaultstate="collapsed" desc="ETAPAS DE UMA PARTIDA">
	
	public static final byte STAGE_VERSUS				= -2;
	public static final byte STAGE_SINGLE_MATCH			= -1;
	public static final byte STAGE_1ST_ROUND			= 0;
	public static final byte STAGE_2ND_ROUND			= STAGE_1ST_ROUND + 1;
	public static final byte STAGE_3RD_ROUND			= STAGE_2ND_ROUND + 1;
	public static final byte STAGE_4TH_ROUND			= STAGE_3RD_ROUND + 1;
	public static final byte STAGE_5TH_ROUND			= STAGE_4TH_ROUND + 1;
	public static final byte STAGE_6TH_ROUND			= STAGE_5TH_ROUND + 1;
	public static final byte STAGE_FINAL_MATCH_1		= STAGE_6TH_ROUND + 1;
	public static final byte STAGE_FINAL_MATCH_2		= STAGE_FINAL_MATCH_1 + 1;
	public static final byte STAGE_FINAL_MATCH_3		= STAGE_FINAL_MATCH_2 + 1;
	public static final byte STAGE_PLAYER_WON			= STAGE_FINAL_MATCH_3 + 1;
	public static final byte STAGE_PLAYER_ELIMINATED	= STAGE_PLAYER_WON + 1;	
	
	// </editor-fold>
	
	// <editor-fold defaulstate="collapsed" desc="DIVIS�ES DO CAMPEONATO">

	public static final byte DIVISION_VERSUS		= -4;
	public static final byte DIVISION_NONE			= -3;
	/** Indica para carregar o campeonato salvo anteriormente. */
	public static final byte DIVISION_NO_SAVED_GAME	= -2;
	public static final byte DIVISION_LOAD_GAME		= -1;
	public static final byte DIVISION_3				= 0;
	public static final byte DIVISION_2				= 1;
	public static final byte DIVISION_1				= 2;	
	public static final byte DIVISIONS_TOTAL		= 3;	
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_PAUSE							= TEXT_OPTIONS + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_TIPS							= TEXT_CONTROLS + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_HELP_TIPS						= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_HELP_TIPS + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_MORE_GAMES					= TEXT_CANCEL + 1;	
	public static final byte TEXT_CONTINUE						= TEXT_MORE_GAMES + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_MTV					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_MTV + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_NO							= TEXT_YES + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_NO + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT_1				= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_CONFIRM_EXIT_2				= TEXT_CONFIRM_EXIT_1 + 1;
	public static final byte TEXT_CONFIRM_EXIT_3				= TEXT_CONFIRM_EXIT_2 + 1;
	public static final byte TEXT_CONFIRM_EXIT_4				= TEXT_CONFIRM_EXIT_3 + 1;
	public static final byte TEXT_CONFIRM_EXIT_5				= TEXT_CONFIRM_EXIT_4 + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT_5 + 1;
	public static final byte TEXT_LOG_TITLE						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_LOG_TITLE + 1;
	public static final byte TEXT_PRESS_5_TO_BUY				= TEXT_GAME_OVER + 1;
	public static final byte TEXT_BUY_FULL_GAME_TITLE			= TEXT_PRESS_5_TO_BUY + 1;
	public static final byte TEXT_BUY_FULL_GAME_TEXT			= TEXT_BUY_FULL_GAME_TITLE + 1;
	public static final byte TEXT_2_OR_MORE_PLAYS_REMAINING_1	= TEXT_BUY_FULL_GAME_TEXT + 1;
	public static final byte TEXT_2_OR_MORE_PLAYS_REMAINING_2	= TEXT_2_OR_MORE_PLAYS_REMAINING_1 + 1;
	public static final byte TEXT_1_PLAY_REMAINING				= TEXT_2_OR_MORE_PLAYS_REMAINING_2 + 1;
	public static final byte TEXT_REPLAYS						= TEXT_1_PLAY_REMAINING + 1;
	public static final byte TEXT_TRAINING						= TEXT_REPLAYS + 1;
	public static final byte TEXT_PARTIAL_RESULT				= TEXT_TRAINING + 1;
	public static final byte TEXT_CHAMPIONSHIP					= TEXT_PARTIAL_RESULT + 1;
	public static final byte TEXT_3RD_DIVISION					= TEXT_CHAMPIONSHIP + 1;
	public static final byte TEXT_2ND_DIVISION					= TEXT_3RD_DIVISION + 1;
	public static final byte TEXT_1ST_DIVISION					= TEXT_2ND_DIVISION + 1;
	public static final byte TEXT_SAVED_GAME_FOUND				= TEXT_1ST_DIVISION + 1;
	public static final byte TEXT_TEAM_NAME						= TEXT_SAVED_GAME_FOUND + 1;
	public static final byte TEXT_GAMES							= TEXT_TEAM_NAME + 1;
	public static final byte TEXT_VICTORIES						= TEXT_GAMES + 1;
	public static final byte TEXT_LOSSES						= TEXT_VICTORIES + 1;
	public static final byte TEXT_GOALS_DIFF					= TEXT_LOSSES + 1;
	public static final byte TEXT_GOALS_FOR						= TEXT_GOALS_DIFF + 1;	
	public static final byte TEXT_SAVE_REPLAY					= TEXT_GOALS_FOR + 1;	
	public static final byte TEXT_LOAD_REPLAY					= TEXT_SAVE_REPLAY + 1;	
	public static final byte TEXT_SAVE							= TEXT_LOAD_REPLAY + 1;	
	public static final byte TEXT_LOAD							= TEXT_SAVE + 1;	
	public static final byte TEXT_REPLAY_NUMBER					= TEXT_LOAD + 1;	
	public static final byte TEXT_EMPTY							= TEXT_REPLAY_NUMBER + 1;	
	public static final byte TEXT_GOAL							= TEXT_EMPTY + 1;	
	public static final byte TEXT_POST_BACK						= TEXT_GOAL + 1;	
	public static final byte TEXT_POST_GOAL						= TEXT_POST_BACK + 1;	
	public static final byte TEXT_POST_OUT						= TEXT_POST_GOAL + 1;	
	public static final byte TEXT_OUT							= TEXT_POST_OUT + 1;	
	public static final byte TEXT_DEFENDED						= TEXT_OUT + 1;	
	public static final byte TEXT_PLAY_RESULT_GOAL				= TEXT_DEFENDED + 1;	
	public static final byte TEXT_PLAY_RESULT_POST				= TEXT_PLAY_RESULT_GOAL + 1;	
	public static final byte TEXT_PLAY_RESULT_OUT				= TEXT_PLAY_RESULT_POST + 1;	
	public static final byte TEXT_PLAY_RESULT_DEFENDED			= TEXT_PLAY_RESULT_OUT + 1;	
	public static final byte TEXT_CONFIRM_OVERWRITE				= TEXT_PLAY_RESULT_DEFENDED + 1;	
	public static final byte TEXT_REPLAY_SAVED					= TEXT_CONFIRM_OVERWRITE + 1;	
	public static final byte TEXT_SUNDAY						= TEXT_REPLAY_SAVED + 1;	
	public static final byte TEXT_MONDAY						= TEXT_SUNDAY + 1;	
	public static final byte TEXT_TUESDAY						= TEXT_MONDAY + 1;	
	public static final byte TEXT_WEDNESDAY						= TEXT_TUESDAY + 1;	
	public static final byte TEXT_THURSDAY						= TEXT_WEDNESDAY + 1;	
	public static final byte TEXT_FRIDAY						= TEXT_THURSDAY + 1;	
	public static final byte TEXT_SATURDAY						= TEXT_FRIDAY + 1;	
	public static final byte TEXT_ATTACK						= TEXT_SATURDAY + 1;	
	public static final byte TEXT_DEFEND						= TEXT_ATTACK + 1;	
	public static final byte TEXT_MTV_ROCKGOL					= TEXT_DEFEND + 1;
	public static final byte TEXT_STAGE_SINGLE_MATCH			= TEXT_MTV_ROCKGOL + 1;
	public static final byte TEXT_STAGE_FIRST_MATCH				= TEXT_STAGE_SINGLE_MATCH + 1;
	public static final byte TEXT_STAGE_SECOND_MATCH			= TEXT_STAGE_FIRST_MATCH + 1;
	public static final byte TEXT_STAGE_THIRD_MATCH				= TEXT_STAGE_SECOND_MATCH + 1;
	public static final byte TEXT_STAGE_FOURTH_MATCH			= TEXT_STAGE_THIRD_MATCH + 1;
	public static final byte TEXT_STAGE_FIFTH_MATCH				= TEXT_STAGE_FOURTH_MATCH + 1;
	public static final byte TEXT_STAGE_SIXTH_MATCH				= TEXT_STAGE_FIFTH_MATCH + 1;
	public static final byte TEXT_STAGE_FINAL					= TEXT_STAGE_SIXTH_MATCH + 1;
	public static final byte TEXT_STAGE_FINAL_MATCH_1			= TEXT_STAGE_FINAL + 1;
	public static final byte TEXT_STAGE_FINAL_MATCH_2			= TEXT_STAGE_FINAL_MATCH_1 + 1;
	public static final byte TEXT_STAGE_FINAL_MATCH_3			= TEXT_STAGE_FINAL_MATCH_2 + 1;
	public static final byte TEXT_STAGE_CHAMPION_DIVISION_3		= TEXT_STAGE_FINAL_MATCH_3 + 1;
	public static final byte TEXT_STAGE_CHAMPION_DIVISION_2		= TEXT_STAGE_CHAMPION_DIVISION_3 + 1;
	public static final byte TEXT_STAGE_CHAMPION_DIVISION_1		= TEXT_STAGE_CHAMPION_DIVISION_2 + 1;
	public static final byte TEXT_STAGE_ELIMINATED				= TEXT_STAGE_CHAMPION_DIVISION_1 + 1;
	public static final byte TEXT_FINAL_RESULT					= TEXT_STAGE_ELIMINATED + 1;
	public static final byte TEXT_REPLAY_SPEED_NORMAL			= TEXT_FINAL_RESULT + 1;
	public static final byte TEXT_REPLAY_SPEED_2X_SLOWER		= TEXT_REPLAY_SPEED_NORMAL + 1;
	public static final byte TEXT_REPLAY_SPEED_4X_SLOWER		= TEXT_REPLAY_SPEED_2X_SLOWER + 1;
	public static final byte TEXT_REPLAY_PAUSED					= TEXT_REPLAY_SPEED_4X_SLOWER + 1;
	public static final byte TEXT_TEAMS_UNKNOWN					= TEXT_REPLAY_PAUSED + 1;
	public static final byte TEXT_CHAMPION_TITLE				= TEXT_TEAMS_UNKNOWN + 1;
	public static final byte TEXT_TEAM_NAME_CLESTON				= TEXT_CHAMPION_TITLE + 1;
	public static final byte TEXT_TEAM_NAME_LAST				= TEXT_TEAM_NAME_CLESTON + TEAMS_NUMBER_TOTAL - 1;
	public static final byte TEXT_NAME_KEEPER_0					= TEXT_TEAM_NAME_LAST + 1;
	public static final byte TEXT_NAME_KEEPER_1					= TEXT_NAME_KEEPER_0 + 1;
	public static final byte TEXT_NAME_KEEPER_2					= TEXT_NAME_KEEPER_1 + 1;
	public static final byte TEXT_NAME_KEEPER_3					= TEXT_NAME_KEEPER_2 + 1;
	public static final byte TEXT_TEAM_NAME_SHORT_CLESTON		= TEXT_NAME_KEEPER_3 + 1;
	public static final short TEXT_TEAM_NAME_SHORT_LAST			= TEXT_TEAM_NAME_SHORT_CLESTON + TEAMS_NUMBER_TOTAL - 1;
	public static final short TEXT_REPLAY_CONTROLS				= TEXT_TEAM_NAME_SHORT_LAST + 1;
	public static final short TEXT_REPLAY_CONTROLS_SAVE			= TEXT_REPLAY_CONTROLS + 1;
	public static final short TEXT_CLESTON_PLAY_BEGIN			= TEXT_REPLAY_CONTROLS_SAVE + 1;
	public static final short TEXT_CLESTON_PLAY_END				= TEXT_CLESTON_PLAY_BEGIN + 1;
	public static final short TEXT_CHAMPION						= TEXT_CLESTON_PLAY_END + 1;
	public static final short TEXT_CHOOSE_YOUR_SHOOTER			= TEXT_CHAMPION + 1;
	public static final short TEXT_CHOOSE_YOUR_KEEPER			= TEXT_CHOOSE_YOUR_SHOOTER + 1;
	public static final short TEXT_CHOOSE_OPPONENT_SHOOTER		= TEXT_CHOOSE_YOUR_KEEPER + 1;
	public static final short TEXT_CHOOSE_OPPONENT_KEEPER		= TEXT_CHOOSE_OPPONENT_SHOOTER + 1;
	public static final short TEXT_PLAYER_1						= TEXT_CHOOSE_OPPONENT_KEEPER + 1;
	public static final short TEXT_PLAYER_2						= TEXT_PLAYER_1 + 1;
	public static final short TEXT_VERSUS						= TEXT_PLAYER_2 + 1;
	public static final short TEXT_PASS_TO_PLAYER_1				= TEXT_VERSUS + 1;
	public static final short TEXT_PASS_TO_PLAYER_2				= TEXT_PASS_TO_PLAYER_1 + 1;
	public static final short TEXT_LOG							= TEXT_PASS_TO_PLAYER_2 + 1;
	
	/** n�mero total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_LOG + 1;
	
	public static final byte TEXT_CONFIRM_EXIT_TOTAL = 5;
	// </editor-fold>	
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_CHOOSE_SOUND			= 0;
	public static final byte SCREEN_SPLASH_NANO				= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_MTV				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_SPLASH_MTV + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME_MENU			= SCREEN_MAIN_MENU + 1;	
	public static final byte SCREEN_SAVED_GAME_FOUND		= SCREEN_NEW_GAME_MENU + 1;	
	public static final byte SCREEN_CHAMPIONSHIP_MENU		= SCREEN_SAVED_GAME_FOUND + 1;	
	public static final byte SCREEN_MATCH					= SCREEN_CHAMPIONSHIP_MENU + 1;	
	public static final byte SCREEN_CHAMPIONSHIP			= SCREEN_MATCH + 1;	
	public static final byte SCREEN_OPTIONS					= SCREEN_CHAMPIONSHIP + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_OBJECTIVES			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS			= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_HELP_TIPS				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_HELP_TIPS + 1;
	public static final byte SCREEN_PAUSE					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU			= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT			= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1				= SCREEN_CONFIRM_EXIT + 1;	
	public static final byte SCREEN_LOADING_2				= SCREEN_LOADING_1 + 1;	
	public static final byte SCREEN_LOADING_PLAY_SCREEN		= SCREEN_LOADING_2 + 1;	
	
	//#if JAR != "min"
	public static final byte SCREEN_SAVE_REPLAY				= SCREEN_LOADING_PLAY_SCREEN + 1;	
	public static final byte SCREEN_LOAD_REPLAY				= SCREEN_SAVE_REPLAY + 1;	
	public static final byte SCREEN_CHOOSE_PLAYER_TRAINING	= SCREEN_LOAD_REPLAY + 1;	
	//#else
//# 	public static final byte SCREEN_CHOOSE_PLAYER_TRAINING	= SCREEN_LOADING_PLAY_SCREEN + 1;	
	//#endif
	
	public static final byte SCREEN_CHOOSE_PLAYER_2_PLAYERS	= SCREEN_CHOOSE_PLAYER_TRAINING + 1;	
	public static final byte SCREEN_ERROR_LOG				= SCREEN_CHOOSE_PLAYER_2_PLAYERS + 1;	
	
	// </editor-fold>
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	//#if JAR == "min"
//# 	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
//# 	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
//# 	public static final byte ENTRY_MAIN_MENU_HELP		= 2;
//# 	public static final byte ENTRY_MAIN_MENU_CREDITS	= 3;
//# 	public static final byte ENTRY_MAIN_MENU_EXIT		= 4;	
	//#else
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_REPLAYS		= 2;
	
		//#if DEMO == "true"
	//# 	/** �ndice da entrada do menu principal para se comprar a vers�o completa do jogo. */
	//# 	public static final byte ENTRY_MAIN_MENU_BUY_FULL_GAME	= 3;
	//# 	
	//# 	
	//# 	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	//# 	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	//# 	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;	
		//#endif
	
	public static final byte ENTRY_MAIN_MENU_HELP		= 3;
	public static final byte ENTRY_MAIN_MENU_CREDITS	= 4;
	public static final byte ENTRY_MAIN_MENU_EXIT		= 5;
	
	//#endif
	
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE NOVO JOGO">
	
	public static final byte ENTRY_NEW_GAME_MENU_TRAINING		= 0;
	public static final byte ENTRY_NEW_GAME_MENU_CHAMPIONSHIP	= 1;
	public static final byte ENTRY_NEW_GAME_MENU_VERSUS			= 2;
	public static final byte ENTRY_NEW_GAME_MENU_BACK			= 3;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DO CAMPEONATO">
	
	public static final byte ENTRY_CHAMPIONSHIP_MENU_3RD_DIVISION	= 0;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_2ND_DIVISION	= 1;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_1ST_DIVISION	= 2;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_BACK			= 3;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_TIPS						= 2;
	public static final byte ENTRY_HELP_MENU_BACK						= 3;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>
	
	
	// poss�veis estados da bola
	public static final byte BALL_STATE_NONE				= 0;
	public static final byte BALL_STATE_STOPPED				= 0;
	public static final byte BALL_STATE_MOVING				= 1;
	public static final byte BALL_STATE_GOAL				= 2;
	public static final byte BALL_STATE_OUT					= 4;
	public static final byte BALL_STATE_POST_BACK			= 8;	// bateu na trave e voltou
	public static final byte BALL_STATE_POST_GOAL			= 10;	// bateu na trave e entrou
	public static final byte BALL_STATE_POST_OUT			= 12;	// bateu na trave e foi para fora
	public static final byte BALL_STATE_REJECTED_KEEPER		= 16;
	public static final byte BALL_STATE_REJECTED_BARRIER	= 32;		
	
	
	/*******************************************************************************************
	 *                              DEFINI��ES GEN�RICAS                                       *
	 *******************************************************************************************/
	// comprimentos do "mundo real"
	public static final int FP_REAL_PENALTY_TO_GOAL		= 589824; // oficial: 11m, usado: 9m
	public static final int FP_REAL_GOAL_WIDTH			= 436469; // oficial: 7,32m; usado: 6,66m
	public static final int FP_REAL_FLOOR_TO_BAR		= 196608; // oficial: 2,44m; usado: 3m
	public static final int FP_REAL_POST_WIDTH			= 16384; // oficial: 12cm; usado: proporcional � imagem (8 pixels de largura)
	public static final int FP_REAL_BALL_RAY			= 8192; // oficial: 17,5cm; usado: proporcional � imagem (12 pixels de largura)
	public static final int FP_REAL_GOAL_DEPTH			= -137626; // -2,1m (profundidade m�xima que a bola vai no gol)
	public static final int FP_REAL_PENALTY_GOAL_DEPTH	= -42599; // profundidade m�xima que a bola vai no gol de p�nalti (-0,65m)
	
	/** acelera��o da gravidade (equivalente a 9,8m/s�) */
	public static final int FP_GRAVITY_ACCELERATION	= -642253;	
	
	// porcentagem de desvio em rela��o � trajet�ria normal causada pela curva. Ex.: num p�nalti, a bola se
	// desvia no m�ximo 1,1m da trajet�ria reta caso o fator seja 0.1 (11m * 0.1). Valor atual: 0,35
	public static final int FP_BALL_CURVE_FACTOR	= 22937;	

	// frame no qual o jogador de fato chuta a bola
	public static final byte PENALTY_PLAYER_SHOT_FRAME = 4;

	
	//#if SCREEN_SIZE == "SMALL"
//# 	
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	/** Espa�amento vertical entre os itens de menu. */
//# 	public static final byte MENU_ITEMS_SPACING = 3;	
//# 	
//# 	/** Varia��o na posi��o do gol em rela��o � grama, para que as traves estejam na posi��o correta em rela��o � linha de fundo. */
//# 	public static final byte GOAL_OFFSET_Y = 12;
//# 	
//# 	/** Varia��o na posi��o da rede em rela��o ao gol. */
//# 	public static final byte NET_OFFSET = 3;
//# 	
//# 	/** Varia��o na posi��o inferior de desenho do goleiro em rela��o � altura da linha de fundo. */
//# 	public static final byte KEEPER_OFFSET_Y = 11;
//# 	
//# 	/** Altura da �rea vis�vel da rede. */
//# 	public static final byte NET_HEIGHT = 40;
//# 	
//# 	/** Espa�amento entre os �cones dos replays, em pixels. */
//# 	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;
//# 	
//# 	/** Posi��o y inicial do medidor na barra de for�a.*/
//# 	public static final byte CONTROL_POWER_BAR_INTERNAL_Y		= 8;
//# 	
//# 	/** Altura m�xima do medidor na barra de for�a. */
//# 	public static final byte CONTROL_POWER_BAR_INTERNAL_HEIGHT	= 38;
//# 
//# 	
//# 	// medidas da imagem do campo, usadas para realizar os c�lculos da posi��o da bola. Os valores
//# 	// s�o em pixels, e devem ser alterados caso a imagem do campo mude.
//# 	//                    GOAL_WIDTH
//# 	//                     _________
//# 	//               /     |       |
//# 	// FLOOR_TO_BAR__\_____|_______|________
//# 	//                /\ 
//# 	// PENALTY_TO_GOAL||
//# 	//                ||  
//# 	// MARK___________\/_______0____________
//# 	public static final byte PENALTY_MARK_TO_GOAL	= 51;
//# 	public static final byte PENALTY_FLOOR_TO_BAR	= 44;
//# 
//# 
//# 	/**  Largura da parte interna do gol (entre as traves) em pixels */
//# 	public static final byte PENALTY_GOAL_WIDTH	= 99;
//# 	
//# 	/** Largura padr�o da tela de jogo, utilizada como base para posicionar os itens. */
//# 	public static final short SCREEN_DEFAULT_WIDTH = 128;
//# 	
//# 	/** Altura padr�o da tela de jogo, utilizada como base para posicionar os itens. */
//# 	public static final short SCREEN_DEFAULT_HEIGHT = 128;
//# 	
//# 	/** Altura m�nima da tela de jogo (para que seja utilizada a movimenta��o de c�mera). */
//# 	public static final short FIELD_MIN_HEIGHT = 190;
//# 	
//# 	/** Altura m�nima da tela para que os controles de replay sejam exibidos. */
//# 	public static final short SCREEN_REPLAY_CONTROLS_VISIBLE_MIN_HEIGHT = 132;
//# 	
//# 	/** Altura em pixels da parte inferior da imagem da marca do p�nalti � parte inferior da altura padr�o da tela de jogo. */
//# 	public static final byte PENALTY_MARK_Y_OFFSET = -30;
//# 	
//# 	/** Posi��o x,y das imagens de patrocinadores na torcida. */
//# 	public static final byte ADVERTISING_OFFSET = 2;
//# 	
//# 	/** Posi��o y da imagem do campo. */
//# 	public static final byte FIELD_Y = 55;
//# 	
//# 	/** Posi��o y da linha de fundo. */
//# 	public static final byte BACK_LINE_Y = FIELD_Y + 36;
//# 	
//# 	/** Divisor utilizado para calcular a quantidade m�xima de rabiscos na tela. */
//# 	public static final byte CHALK_DIVISOR = 40;
//# 	
//# 	/** Varia��o na posi��o y da torcida em rela��o � imagem de fundo do campo, para telas com largura menor ou igual � padr�o. */
//# 	public static final byte MATCH_CROWD_Y_OFFSET = 4;
//# 	
//#  	/** Offset na posi��o inferior do pattern do c�u em rela��o � posi��o y da torcida. */
//#  	public static final byte SKY_PATTERN_CROWD_Y_OFFSET = 36;	
//# 	
//# 	/** Posi��o x do primeiro bot�o do replay. */
//# 	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;
//# 	
//# 	/** Largura de cada bot�o do controle de replay. */
//# 	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;
//# 	
//# 	/** Offset na posi��o x da ta�a em rela��o � imagem do Cleston. */
//# 	public static final byte TROPHY_OFFSET_X = 41;
//# 	
//# 	// valores j� convertidos para ponto fixo (devem ser atualizados caso os valores em pixel sejam alterados)
//# 	public static final int FP_PENALTY_MARK_TO_GOAL	= 3342336;
//# 	public static final int FP_PENALTY_FLOOR_TO_BAR	= 2883584;
//# 	public static final int FP_PENALTY_GOAL_WIDTH	= 6488064;		
//# 	
//# 	//</editor-fold>
//# 	
	//#elif SCREEN_SIZE == "MEDIUM"
	
	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA M�DIA" defaultstate="collapsed">
	
	/** Espa�amento vertical entre os itens de menu. */
	public static final byte MENU_ITEMS_SPACING = 3;	
	
	/** Varia��o na posi��o do gol em rela��o � grama, para que as traves estejam na posi��o correta em rela��o � linha de fundo. */
	public static final byte GOAL_OFFSET_Y = 12;
	
	/** Varia��o na posi��o da rede em rela��o ao gol. */
	public static final byte NET_OFFSET = 3;
	
	/** Varia��o na posi��o inferior de desenho do goleiro em rela��o � altura da linha de fundo. */
	public static final byte KEEPER_OFFSET_Y = 8;
	
	/** Altura da �rea vis�vel da rede. */
	public static final byte NET_HEIGHT = 57;
	
	/** Espa�amento entre os �cones dos replays, em pixels. */
	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;
	
	/** Posi��o y inicial do medidor na barra de for�a.*/
	public static final byte CONTROL_POWER_BAR_INTERNAL_Y		= 11;
	
	/** Altura m�xima do medidor na barra de for�a. */
	public static final byte CONTROL_POWER_BAR_INTERNAL_HEIGHT	= 47;

	
	// medidas da imagem do campo, usadas para realizar os c�lculos da posi��o da bola. Os valores
	// s�o em pixels, e devem ser alterados caso a imagem do campo mude.
	//                    GOAL_WIDTH
	//                     _________
	//               /     |       |
	// FLOOR_TO_BAR__\_____|_______|________
	//                /\ 
	// PENALTY_TO_GOAL||
	//                ||  
	// MARK___________\/_______0____________
	public static final byte PENALTY_MARK_TO_GOAL	= 78;
	public static final byte PENALTY_FLOOR_TO_BAR	= 56;


	/**  Largura da parte interna do gol (entre as traves) em pixels */
	public static final short PENALTY_GOAL_WIDTH	= 138;
	
	/** Largura padr�o da tela de jogo, utilizada como base para posicionar os itens. */
	public static final short SCREEN_DEFAULT_WIDTH = 176;
	
	/** Largura considerada para o maior splash. */
	public static final short SCREEN_SPLASH_WIDTH_BIG = 240;
	
	/** Altura "m�nima" da tela de splash (telas menores que esse alinham as imagens na parte inferior). */
	public static final short SCREEN_SPLASH_HEIGHT_MIN = 200;

	/** Altura padr�o da tela de jogo, utilizada como base para posicionar os itens. */
	public static final short SCREEN_DEFAULT_HEIGHT = 208;
	
	/** Altura m�nima da tela de jogo (para que seja utilizada a movimenta��o de c�mera). */
	public static final short FIELD_MIN_HEIGHT = 240;
	
	/** Altura m�nima da tela para que os controles de replay sejam exibidos. */
	public static final short SCREEN_REPLAY_CONTROLS_VISIBLE_MIN_HEIGHT = 230;
	
	/** Altura em pixels da parte inferior da imagem da marca do p�nalti � parte inferior da altura padr�o da tela de jogo. */
	public static final byte PENALTY_MARK_Y_OFFSET = -30;
	
	/** Posi��o x,y das imagens de patrocinadores na torcida. */
	public static final byte ADVERTISING_OFFSET = 2;
	
	/** Posi��o y da imagem do campo. */
	public static final byte FIELD_Y = 80;
	
	/** Posi��o y da linha de fundo. */
	public static final byte BACK_LINE_Y = FIELD_Y + 36;
	
	/** Offset na posi��o inferior do pattern do c�u em rela��o � posi��o y da torcida. */
	public static final byte SKY_PATTERN_CROWD_Y_OFFSET = 36;
	
	/** Altura da parte inferior do campo (montada dinamicamente no caso de aparelhos com pouca mem�ria). */
	public static final byte FIELD_BOTTOM_HEIGHT = 56;
	
	/** Altura da imagem da lama, relativa ao grupo do gramado inferior (somente na vers�o de baixa mem�ria). */
	public static final byte FIELD_MUD_Y = 2;
	
	/** Altura da parte de grama escura do gramado inferior do campo. */
	public static final byte FIELD_BOTTOM_DARK_HEIGHT = 4;
	
	/** Altura da parte de grama m�dia do gramado inferior do campo. */
	public static final byte FIELD_BOTTOM_MEDIUM_HEIGHT = 31;
	
	/** Altura da parte de grama clara do gramado inferior do campo. */
	public static final byte FIELD_BOTTOM_LIGHT_HEIGHT = FIELD_BOTTOM_HEIGHT - FIELD_BOTTOM_DARK_HEIGHT - FIELD_BOTTOM_MEDIUM_HEIGHT;
	
	/** Divisor utilizado para calcular a quantidade m�xima de rabiscos na tela. */
	public static final byte CHALK_DIVISOR = 60;
	
	/** Varia��o na posi��o y da torcida em rela��o � imagem de fundo do campo, para telas com largura menor ou igual � padr�o. */
	public static final byte MATCH_CROWD_Y_OFFSET_DEFAULT = 3;
	
	/** Varia��o na posi��o y da torcida em rela��o � imagem de fundo do campo, para telas com largura maior que a padr�o. */
	public static final byte MATCH_CROWD_Y_OFFSET_BIG = 9;

	/** Posi��o x do primeiro bot�o do replay. */
	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;
	
	/** Largura de cada bot�o do controle de replay. */
	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;
	
	/** Offset na posi��o x da ta�a em rela��o � imagem do Cleston. */
	public static final byte TROPHY_OFFSET_X = 74;
	
	/** Posi��o x do olho fechado do Cleston, para que ele possa piscar. */
	public static final byte CLESTON_EYE_X = 38;
	
	/** Posi��o y do olho fechado do Cleston, para que ele possa piscar. */
	public static final byte CLESTON_EYE_Y = 24;
	
	// valores j� convertidos para ponto fixo (devem ser atualizados caso os valores em pixel sejam alterados)
	public static final int FP_PENALTY_MARK_TO_GOAL	= 5111808;
	public static final int FP_PENALTY_FLOOR_TO_BAR	= 3670016;
	public static final int FP_PENALTY_GOAL_WIDTH	= 9043968;	
	
	//</editor-fold>	
	
	
	//#endif
	
}
