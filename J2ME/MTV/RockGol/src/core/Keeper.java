/**
 * Keeper.java
 * �2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:15:25 AM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;


/**
 * 
 * @author Peter
 */
public final class Keeper extends Player implements SpriteListener {
	
	//#if ( SCREEN_SIZE != "SMALL" && JAR != "full" ) || ( SCREEN_SIZE == "SMALL" && JAR == "min" )
//# 	private static final byte INDEX_CLESTON_1	= 0;
//# 	private static final byte INDEX_FAT_1		= 1;
//# 	private static final byte INDEX_FAT_2		= 2;
//# 	private static final byte INDEX_FAT_3		= 3;
//# 	private static final byte INDEX_CLESTON_2	= 4;
//# 	private static final byte INDEX_CLESTON_3	= 8;
	//#else
	private static final byte INDEX_CLESTON_1	= 0;
	private static final byte INDEX_DREAD_1		= 1;
	private static final byte INDEX_FAT_1		= 2;
	private static final byte INDEX_NERD_1		= 3;
	private static final byte INDEX_CLESTON_2	= 4;
	private static final byte INDEX_DREAD_2		= 5;
	private static final byte INDEX_FAT_2		= 6;
	private static final byte INDEX_NERD_2		= 7;
	private static final byte INDEX_CLESTON_3	= 8;
	private static final byte INDEX_DREAD_3		= 9;
	private static final byte INDEX_FAT_3		= 10;
	private static final byte INDEX_NERD_3		= 11;
	//#endif
	
	/** Sequ�ncia do goleiro parado, esperando a cobran�a. */
	public static final byte SEQUENCE_STOPPED			= 0;
	/** Sequ�ncia do goleiro preparando-se para um pulo na regi�o horizontal central do gol. */
	public static final byte SEQUENCE_PREPARING_MIDDLE	= 1;
	/** Sequ�ncia do goleiro preparando-se para um pulo nos cantos, ou para desistir. */
	public static final byte SEQUENCE_PREPARING_SIDE	= 2;
	/** Sequ�ncia do pulo alto e no meio do gol. */
	public static final byte SEQUENCE_JUMP_CENTER_HIGH	= 3;
	/** Sequ�ncia do pulo baixo e no meio do gol. */
	public static final byte SEQUENCE_JUMP_CENTER_LOW	= 4;
	/** Sequ�ncia do pulo alto e no canto. */
	public static final byte SEQUENCE_JUMP_SIDE_HIGH	= 5;
	/** Sequ�ncia do pulo � meia altura e no canto. */
	public static final byte SEQUENCE_JUMP_SIDE_MIDDLE	= 6;
	/** Sequ�ncia do pulo rasteiro e no canto. */
	public static final byte SEQUENCE_JUMP_SIDE_LOW		= 7;
	/** Sequ�ncia do goleiro desistindo da bola (pulou para o lado contr�rio ao da bola). */
	public static final byte SEQUENCE_GIVE_UP			= 8;
	/** Sequ�ncia do goleiro quicando ap�s bater no ch�o. */
	public static final byte SEQUENCE_BOUNCING_UP		= 9;
	/** Sequ�ncia do goleiro caindo ap�s quicar no ch�o. */
	public static final byte SEQUENCE_BOUNCING_DOWN		= 10;
	/** Sequ�ncia do goleiro deitado no ch�o. */
	public static final byte SEQUENCE_LAY_DOWN			= 11;
	/** Sequ�ncia do goleiro caindo de bunda. */
	public static final byte SEQUENCE_FALLING			= 12;
	
	
	/** Tipo do pulo atual. Valores v�lidos:
	 * <ul>
	 * <li>SEQUENCE_JUMP_CENTER_HIGH</li>
	 * <li>SEQUENCE_JUMP_CENTER_LOW</li>
	 * <li>SEQUENCE_JUMP_SIDE_HIGH</li>
	 * <li>SEQUENCE_JUMP_SIDE_MIDDLE</li>
	 * <li>SEQUENCE_JUMP_SIDE_LOW</li>
	 * <li>SEQUENCE_GIVE_UP</li>
	 * </ul>
	 */
	private byte jumpType;
	
	// estados do pulo do goleiro
	/** Est�gio do pulo: nenhum pulo. */
	private static final byte JUMP_STATE_NONE				= 0;
	/** Est�gio do pulo: subindo. */
	private static final byte JUMP_STATE_RISING				= 1;
	/** Est�gio do pulo: caindo. */
	private static final byte JUMP_STATE_FALLING			= 2;
	/** Est�gio do pulo: subindo ap�s primeiro quique no ch�o. */
	private static final byte JUMP_STATE_BOUNCING_UP_1		= 3;
	/** Est�gio do pulo: caindo ap�s primeiro quique no ch�o. */
	private static final byte JUMP_STATE_BOUNCING_DOWN_1	= 4;
	/** Est�gio do pulo: subindo ap�s segundo quique no ch�o (somente SEQUENCE_JUMP_CENTER_HIGH ou SEQUENCE_JUMP_SIDE_HIGH). */
	private static final byte JUMP_STATE_BOUNCING_UP_2		= 5;
	/** Est�gio do pulo: caindo ap�s segundo quique no ch�o (somente SEQUENCE_JUMP_CENTER_HIGH ou SEQUENCE_JUMP_SIDE_HIGH). */
	private static final byte JUMP_STATE_BOUNCING_DOWN_2	= 6;
	/** Est�gio do pulo: freando o movimento, j� em contato com o ch�o (somente SEQUENCE_JUMP_CENTER_LOW ou SEQUENCE_JUMP_SIDE_LOW). */
	private static final byte JUMP_STATE_BRAKING			= 7;
	/** Est�gio do pulo: movimento do pulo encerrado. */
	private static final byte JUMP_STATE_FINISHED			= 8;
	
	private byte jumpState;
	
	
	/** 
	 * Porcentagem da maior posi��o horizontal poss�vel numa jogada considerada como regi�o central do gol (separa
	 * os tipos de pulo CENTER dos SIDE).
	 */
	private static final int FP_REGION_H_MIDDLE_PERCENT = 24903;//NanoMath.divInt( 38, 100 );
	
	/** Porcentagem da altura m�xima de uma jogada que separa os tipos de pulo SEQUENCE_JUMP_CENTER_HIGH e SEQUENCE_JUMP_CENTER_LOW. */
	private static final int FP_REGION_V_MIDDLE_PERCENT = 41943;//NanoMath.divInt( 64, 100 );
	
	/** Porcentagem m�xima da altura de uma jogada para que o pulo seja do tipo SEQUENCE_JUMP_SIDE_LOW. */
	private static final int FP_REGION_SIDE_BOTTOM_PERCENT = 22282;//NanoMath.divInt( 34, 100 );
	
	/** Porcentagem da altura m�xima de uma jogada que separa os tipos de pulo SEQUENCE_JUMP_SIDE_MIDDLE e SEQUENCE_JUMP_SIDE_HIGH. */
	private static final int FP_REGION_SIDE_MIDDLE_PERCENT = 43909;//NanoMath.divInt( 67, 100 );
	
	/** Porcentagem das bolas em que o goleiro desiste ao perceber que pulou para o lado contr�rio ao da bola. */
	private static final byte GIVE_UP_PERCENT = 60;
	
	/** Indica se o goleiro desistiu do pulo na �ltima jogada (valor utilizado para que o replay seja igual � �ltima jogada). */
	private boolean gaveUpLastPlay;
	
	public static final byte GIVE_UP_BEHAVIOUR_RANDOM		= 0;
	public static final byte GIVE_UP_BEHAVIOUR_LAST_PLAY	= 1;
	
	/** Porcentagem da velocidade x do pulo no caso do goleiro desistir. */
	private static final int FP_GIVE_UP_JUMP_X_FACTOR		= 24248;//NanoMath.divInt( 37, 100 );
	
	/** Porcentagem da velocidade y do pulo no caso do goleiro desistir. */
	private static final int FP_GIVE_UP_JUMP_Y_FACTOR		= 54394;//NanoMath.divInt( 83, 100 );
	
	// valores dos vetores para cada �ngulo de inclina��o do goleiro (usados para posicionar
	// corretamente a �rea de colis�o do goleiro)
	private static final int FP_SQRT_3_DIV_2		= 54318;//NanoMath.divInt( 866025, 1000000 );
	private static final int FP_SQRT_2_DIV_2		= 22016;//NanoMath.divInt( 70710678, 100000000 );
	private static final int FP_HALF				= NanoMath.HALF;
	
	public static final XYZ VECTOR_UP_90			= new XYZ( 0, NanoMath.ONE, 0 );
	public static final XYZ VECTOR_RIGHT_90			= new XYZ( NanoMath.ONE, 0, 0 );

	public static final XYZ VECTOR_UP_60			= new XYZ( FP_HALF, FP_SQRT_3_DIV_2, 0 );
	public static final XYZ VECTOR_RIGHT_60			= new XYZ( FP_SQRT_3_DIV_2, -FP_HALF, 0 );
	public static final XYZ VECTOR_UP_60_LEFT		= new XYZ( -FP_HALF, FP_SQRT_3_DIV_2, 0 );
	public static final XYZ VECTOR_RIGHT_60_LEFT	= new XYZ( FP_SQRT_3_DIV_2, FP_HALF, 0 );

	public static final XYZ VECTOR_UP_45			= new XYZ( FP_SQRT_2_DIV_2, FP_SQRT_2_DIV_2, 0 );
	public static final XYZ VECTOR_RIGHT_45			= new XYZ( FP_SQRT_2_DIV_2, -FP_SQRT_2_DIV_2, 0 );
	public static final XYZ VECTOR_UP_45_LEFT		= new XYZ( -FP_SQRT_2_DIV_2, FP_SQRT_2_DIV_2, 0 );
	public static final XYZ VECTOR_RIGHT_45_LEFT	= VECTOR_UP_45;

	public static final XYZ VECTOR_UP_30			= VECTOR_RIGHT_60_LEFT;
	public static final XYZ VECTOR_RIGHT_30			= new XYZ( FP_HALF, -FP_SQRT_3_DIV_2, 0 );
	public static final XYZ VECTOR_UP_30_LEFT		= new XYZ( -FP_SQRT_3_DIV_2, FP_HALF, 0 );
	public static final XYZ VECTOR_RIGHT_30_LEFT	= VECTOR_UP_60;

	public static final XYZ VECTOR_UP_0				= VECTOR_RIGHT_90;
	public static final XYZ VECTOR_RIGHT_0			= new XYZ( 0, -NanoMath.ONE, 0 );
	public static final XYZ VECTOR_UP_0_LEFT		= new XYZ( -NanoMath.ONE, 0, 0 );
	public static final XYZ VECTOR_RIGHT_0_LEFT		= VECTOR_UP_90;

	// largura real do goleiro com os bra�os pr�ximos ao corpo
	public static final int FP_REAL_KEEPER_WIDTH		= NanoMath.ONE;
	
	/** largura real do goleiro com os bra�os abertos */
	public static final int FP_REAL_KEEPER_WIDTH_ARMS	= 108134;//NanoMath.divInt( 165, 100 );
	
	/** Altura do goleiro. */
	public static final int FP_REAL_KEEPER_HEIGHT		= 124518;//NanoMath.divInt( 190, 100 );
	
	/** Altura do goleiro abaixado. */
	public static final int FP_REAL_KEEPER_HEIGHT_DOWN	= 104857;//NanoMath.divInt( 160, 100 );
	
	/** altura real do goleiro com os bra�os esticados. */
	public static final int FP_REAL_KEEPER_HEIGHT_ARMS	= 172222;//NanoMath.divInt( 265, 100 );
	
	/** Dura��o m�nima do pulo em segundos. */
	private static final int FP_MIN_JUMP_TIME			= 53149;//NanoMath.divInt( 811, 1000 );	
	
	/** Tempo de frenagem do pulo atual. */
	private int fp_currentBrakingTime;
	
	/** Acelera��o causada pelo atrito com o ch�o, quando o pulo est� no estado JUMP_STATE_BRAKING. */
	private static final int FP_BRAKE_ACCELERATION = -294806;//NanoMath.divFixed( -Control.FP_CONTROL_DIRECTION_MAX_KEEPER_VALUE, FP_MAX_BRAKING_TIME );
	
	/** Ret�ngulo que define a �rea de colis�o no espa�o 3D. */
	private final Quad realCollisionArea = new Quad();
	
	/** Posi��o real do goleiro. */
	private final XYZ realPosition = new XYZ();
	
	/** Posi��o real inicial do goleiro. */
	private final XYZ realInitialPosition = new XYZ();
	
	/** Velocidade real inicial do pulo. */
	private final XYZ jumpSpeed = new XYZ();
	
	/** Velocidade real vertical instant�nea. */
	private int fp_instantYSpeed;
	
	private int fp_accRealTime;
	
	private final int INITIAL_X;
	private final int INITIAL_Y;
	
	/** 
	 * N�vel de dificuldade do goleiro da CPU (porcentagem de jogadas em que ele pula para o mesmo lado da bola). Caso
	 * seja menor ou igual a zero, o comportamento � normal (aleat�rio).
	 */
	private byte difficultyLevel;
	
	
	private Keeper( Drawable goal, int index ) throws Exception {
		super( PATH_KEEPERS, PATH_KEEPERS, index );
		
		// posiciona o goleiro em rela��o ao gol
		INITIAL_X = goal.getPosX() + ( goal.getWidth() >> 1 );
		INITIAL_Y = goal.getPosY() + goal.getHeight() + KEEPER_OFFSET_Y;
		
		defineReferencePixel( size.x >> 1, size.y );
		setRefPixelPosition( INITIAL_X, INITIAL_Y );
		
		setListener( this, 0 );
	}
	
	
	private Keeper( Drawable goal, int index, PaletteMap[] map ) throws Exception {
		super( PATH_KEEPERS, PATH_KEEPERS, index, map );
		
		// posiciona o goleiro em rela��o ao gol
		INITIAL_X = goal.getPosX() + ( goal.getWidth() >> 1 );
		INITIAL_Y = goal.getPosY() + goal.getHeight() + KEEPER_OFFSET_Y;
		
		defineReferencePixel( size.x >> 1, size.y );
		setRefPixelPosition( INITIAL_X, INITIAL_Y );
		
		setListener( this, 0 );
	}
	
	
	public static final Keeper createInstance( Drawable goal, int index ) throws Exception {
		//#if ( SCREEN_SIZE != "SMALL" && JAR != "full" ) || ( SCREEN_SIZE == "SMALL" && JAR == "min" )
//# 		index = convertIndex( index );
		//#endif
		
		switch ( index ) {
			case INDEX_CLESTON_1:
			case INDEX_FAT_1:
			
			//#if ( JAR == "full" ) || ( SCREEN_SIZE == "SMALL" && JAR != "min" )
			case INDEX_DREAD_1:
			case INDEX_NERD_1:
			//#endif
				
				return new Keeper( goal, getSpriteIndex( index ) );
			
			default:
				return new Keeper( goal, getSpriteIndex( index ), getPaletteMap( index ) );
		}
	}
	
	
	public static final DrawableImage getImage( int index ) throws Exception {
		final String path = PATH_KEEPERS + getSpriteIndex( index ) + "_0.png";
		
		switch ( index ) {
			case INDEX_CLESTON_1:
			case INDEX_FAT_1:
			
			//#if ( JAR == "full" ) || ( SCREEN_SIZE == "SMALL" && JAR != "min" )
			case INDEX_DREAD_1:
			case INDEX_NERD_1:
			//#endif
				
				return new DrawableImage( path );
				
			default:
				PaletteChanger pc = new PaletteChanger( path );
				final DrawableImage image = new DrawableImage( pc.createImage( getPaletteMap( index ) ) );
				pc = null;
				
				return image;
		}		
	}	
	
	
	public static final byte getSpriteIndex( int index ) {
		//#if ( SCREEN_SIZE != "SMALL" && JAR != "full" ) || ( SCREEN_SIZE == "SMALL" && JAR == "min" )
//# 		switch ( index ) {
//# 			case INDEX_CLESTON_1:
//# 			case INDEX_CLESTON_2:
//# 			case INDEX_CLESTON_3:
//# 				return 0;
//# 			
//# 			default:
//# 				return 2;
//# 		}
		//#else
		return ( byte ) ( index & 3 );
		//#endif
	}
	
	
	//#if ( SCREEN_SIZE != "SMALL" && JAR != "full" ) || ( SCREEN_SIZE == "SMALL" && JAR == "min" )
//# 	private static final int convertIndex( int index ) {
//# 		switch ( index ) {
//# 			case INDEX_CLESTON_1:
//# 			case INDEX_CLESTON_2:
//# 			case INDEX_CLESTON_3:
//# 				return index;
//# 			
//# 			default:
//# 				return index & 3;
//# 		}
//# 	}
	//#endif
	
	
	private static final PaletteMap[] getPaletteMap( int index ) {
		//#if ( SCREEN_SIZE != "SMALL" && JAR != "full" ) || ( SCREEN_SIZE == "SMALL" && JAR == "min" )
//# 		index = convertIndex( index );
		//#endif
		
		switch ( index ) {
			case INDEX_CLESTON_2:
				return new PaletteMap[] {
					new PaletteMap( 0x0066ff, 0xffffff ), // camisa clara e mei�o
					new PaletteMap( 0x0046b0, 0x000000 ), // camisa escura
				};
			
			case INDEX_CLESTON_3:
				return new PaletteMap[] {
					new PaletteMap( 0x0066ff, 0xff0000 ), // camisa clara e mei�o
					new PaletteMap( 0x0046b0, 0x000000 ), // camisa escura
				};

			case INDEX_FAT_2:
				return new PaletteMap[] {
					new PaletteMap( 0x0049ff, 0xff0038 ), // camisa clara
					new PaletteMap( 0x0030a7, 0xa70025 ), // camisa escura
				};

			case INDEX_FAT_3:
				return new PaletteMap[] {
					new PaletteMap( 0x0049ff, 0x00ff21 ), // camisa clara
					new PaletteMap( 0x0030a7, 0x00a716 ), // camisa escura
				};
			
			//#if JAR == "full"	||( SCREEN_SIZE == "SMALL" && JAR != "min" )
			
			case INDEX_DREAD_2:
				return new PaletteMap[] {
					new PaletteMap( 0xfff000, 0xff6000 ), // camisa clara
					new PaletteMap( 0xe9ba00, 0xe93700 ), // camisa escura
					new PaletteMap( 0x0011f2, 0x00c6f2 ), // short claro
					new PaletteMap( 0x000dbb, 0x0099bb ), // short escuro
				};

			case INDEX_DREAD_3:
				return new PaletteMap[] {
					new PaletteMap( 0xfff000, 0x00c6f2 ), // camisa clara
					new PaletteMap( 0xe9ba00, 0x0099bb ), // camisa escura
					new PaletteMap( 0x0011f2, 0x25282e ), // short claro
					new PaletteMap( 0x000dbb, 0x575b65 ), // short escuro
				};
				
			case INDEX_NERD_2:
				return new PaletteMap[] {
					new PaletteMap( 0x0049ff, 0xf100ff ), // camisa clara
					new PaletteMap( 0x0030a7, 0x9e00a7 ), // camisa escura
					new PaletteMap( 0x3b3f47, 0x9e00a7 ), // short claro
					new PaletteMap( 0x23252a, 0x5a505f ), // short escuro
					new PaletteMap( 0x575b65, 0xf100ff ), // chuteira clara
					new PaletteMap( 0x3b3f47, 0x5a00ff ), // chuteira escura
				};
				
			case INDEX_NERD_3:
				return new PaletteMap[] {
					new PaletteMap( 0x0049ff, 0xff5800 ), // camisa clara
					new PaletteMap( 0x0030a7, 0xa73900 ), // camisa escura
				};
				
			//#endif
				
			default:
				return null;
		}		
	}	
	

	/**
	 * Obt�m o quad de colis�o do goleiro na posi��o atual.
	 * 
	 * @return quad representando a �rea de colis�o atual.
	 */
	public final Quad getCollisionArea() {
		XYZ up = null;
		XYZ right = null;
		int fp_width = 0;
		int fp_height = 0;
		final boolean mirrored = getTransform() == TRANS_MIRROR_H;
		byte anchor = Quad.ANCHOR_BOTTOM;
		
		switch ( sequenceIndex ) {
			case SEQUENCE_STOPPED:
			case SEQUENCE_PREPARING_MIDDLE:
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT;
			break;

			case SEQUENCE_PREPARING_SIDE:
				if ( mirrored ) {
					up = VECTOR_UP_60_LEFT;
					right = VECTOR_RIGHT_60_LEFT;
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				} else {
					up = VECTOR_UP_60;
					right = VECTOR_RIGHT_60;
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT;
			break;
			
			case SEQUENCE_JUMP_CENTER_HIGH:
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT;
			break;
			
			case SEQUENCE_JUMP_CENTER_LOW:
				if ( mirrored ) {
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				} else {
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				}				
				
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT_DOWN;
			break;
			
			case SEQUENCE_JUMP_SIDE_HIGH:
				if ( mirrored ) {
					up = VECTOR_UP_60_LEFT;
					right = VECTOR_RIGHT_60_LEFT;
				} else {
					up = VECTOR_UP_60;
					right = VECTOR_RIGHT_60;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
			break;
			
			case SEQUENCE_JUMP_SIDE_MIDDLE:
				if ( mirrored ) {
					up = VECTOR_UP_30_LEFT;
					right = VECTOR_RIGHT_30_LEFT;
				} else {
					up = VECTOR_UP_30;
					right = VECTOR_RIGHT_30;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
			break;
			
			case SEQUENCE_JUMP_SIDE_LOW:
			case SEQUENCE_LAY_DOWN:
				if ( mirrored ) {
					up = VECTOR_UP_0_LEFT;
					right = VECTOR_RIGHT_0_LEFT;
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				} else {
					up = VECTOR_UP_0;
					right = VECTOR_RIGHT_0;
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
			break;
			
			case SEQUENCE_GIVE_UP:
			case SEQUENCE_BOUNCING_UP:
			case SEQUENCE_BOUNCING_DOWN:
				if ( mirrored ) {
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				} else {
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				}
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT_DOWN;
			break;
		}
		
		realCollisionArea.setValues( realPosition, anchor, up, right, fp_width, fp_height );
		return realCollisionArea;
	}


	public final boolean getLastGiveUp() {
		return gaveUpLastPlay;
	}


	/**
	 * Prepara o goleiro para um pulo.
	 * 
	 * @param keeperPlay dire��o e altura do pulo do goleiro.
	 * @param shooterPlay jogada realizada pelo batedor. Essa jogada � utilizada para definir se o goleiro ir� completar
	 * seu pulo, ou se ir� desistir no meio, caso perceba que escolheu o canto errado.
	 * @param replay indica se � a repeti��o da jogada anterior.
	 */
	public final void jump( Play keeperPlay, Play shooterPlay, boolean replay ) {
		if ( !replay ) {
			keeperPlay.fp_direction = NanoMath.mulFixed( keeperPlay.fp_direction, Control.FP_CONTROL_DIRECTION_KEEPER_FACTOR );
			keeperPlay.fp_height = NanoMath.mulFixed( keeperPlay.fp_height, Control.FP_CONTROL_HEIGHT_KEEPER_FACTOR );
			
			final int rnd = NanoMath.randInt( 100 );
			if ( rnd < difficultyLevel ) {
				// goleiro pula para o mesmo lado da bola
				if ( NanoMath.sgn( keeperPlay.fp_direction ) != NanoMath.sgn( shooterPlay.fp_direction ) )
					keeperPlay.fp_direction = -keeperPlay.fp_direction;
			}
		}
		
		final int fp_horizontalPercent = Math.abs( NanoMath.divFixed( keeperPlay.fp_direction, Control.FP_CONTROL_DIRECTION_MAX_KEEPER_VALUE ) );
		final int fp_verticalPercent = NanoMath.divFixed( keeperPlay.fp_height, Control.FP_CONTROL_HEIGHT_MAX_KEEPER_VALUE );
		
		setTransform( keeperPlay.fp_direction < 0 ? TRANS_MIRROR_H : TRANS_NONE );
		
		if ( fp_horizontalPercent < FP_REGION_H_MIDDLE_PERCENT ) {
			// goleiro realizou um pulo na regi�o horizontal central do gol
			
			if ( fp_verticalPercent < FP_REGION_V_MIDDLE_PERCENT ) {
				// goleiro fez um pulo rasteiro e no meio
				jumpType = SEQUENCE_JUMP_CENTER_LOW;
				calculateJumpSpeed( keeperPlay.fp_direction, 0 );
			} else {
				// pulo do goleiro � alto e no meio
				jumpType = SEQUENCE_JUMP_CENTER_HIGH;
				calculateJumpSpeed( keeperPlay.fp_direction, keeperPlay.fp_height );
			}
			
			setSequence( SEQUENCE_PREPARING_MIDDLE );
		} else {
			// goleiro realiza um pulo nos cantos, ou desiste. A primeira verifica��o � a da dire��o da bola - caso
			// seja contr�ria � dire��o do pulo do goleiro, sorteia seu comportamento (finaliza o pulo ou desiste).
			
			gaveUpLastPlay = replay ? gaveUpLastPlay : ( ( ( shooterPlay.fp_direction > 0 && keeperPlay.fp_direction < 0 ) || ( shooterPlay.fp_direction < 0 && keeperPlay.fp_direction > 0 ) ) 
								&& NanoMath.randInt( 100 ) < GIVE_UP_PERCENT );

			if ( gaveUpLastPlay ) {
				// goleiro percebeu que pulou para o lado contr�rio ao da bola e desistiu do pulo.
				jumpType = SEQUENCE_GIVE_UP;
				
				calculateJumpSpeed( NanoMath.mulFixed( keeperPlay.fp_direction, FP_GIVE_UP_JUMP_X_FACTOR  ),
									NanoMath.mulFixed( keeperPlay.fp_height, FP_GIVE_UP_JUMP_Y_FACTOR  ) );
			} else {
				// goleiro pulou para a dire��o certa, ou ent�o segue seu pulo mesmo ap�s perceber que n�o ter� sucesso.
				if ( fp_verticalPercent < FP_REGION_SIDE_BOTTOM_PERCENT ) {
					// goleiro deu um pulo rasteiro e no canto
					jumpType = SEQUENCE_JUMP_SIDE_LOW;
				} else if ( fp_verticalPercent < FP_REGION_SIDE_MIDDLE_PERCENT ) {
					// goleiro realizou um pulo � meia altura e no canto
					jumpType = SEQUENCE_JUMP_SIDE_MIDDLE;
				} else {
					// o pulo do goleiro � do tipo alto e no canto
					jumpType = SEQUENCE_JUMP_SIDE_HIGH;
				}
				
				calculateJumpSpeed( keeperPlay.fp_direction, keeperPlay.fp_height );
			}
			
			setSequence( SEQUENCE_PREPARING_SIDE );
		}
		
		//#if DEBUG == "true"
		System.out.println( "GOLEIRO: " + NanoMath.toString( fp_horizontalPercent ) + " -  " + NanoMath.toString( fp_verticalPercent ) + " -  " + jumpType );
		//#endif
		
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case SEQUENCE_PREPARING_MIDDLE:
			case SEQUENCE_PREPARING_SIDE:
				setSequence( jumpType );
				
				switch ( jumpType ) {
					case SEQUENCE_JUMP_CENTER_LOW:
					case SEQUENCE_JUMP_SIDE_LOW:
					case SEQUENCE_GIVE_UP:
						setJumpState( JUMP_STATE_BRAKING );
					break;
					
					default:
						setJumpState( JUMP_STATE_RISING );
				}
				
			break;
		}
	}
	
	
	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
	
	
	public final void reset() {
		fp_accRealTime = 0;
		setTransform( TRANS_NONE );
		setSequence( SEQUENCE_STOPPED );
		
		realPosition.set();
		realInitialPosition.set();
		
		jumpSpeed.set();
		jumpType = -1;
		jumpState = JUMP_STATE_NONE;
		fp_instantYSpeed = 0;
		fp_currentBrakingTime = 0;
		
		updateScreenPosition();
	}


	public final void setDifficultyLevel( int difficultyLevel ) {
		this.difficultyLevel = ( byte ) difficultyLevel;
	}
	
	
	private final void setJumpState( int state ) {
		switch ( state ) {
			case JUMP_STATE_BOUNCING_UP_1:
			case JUMP_STATE_BOUNCING_UP_2:
				realInitialPosition.x = realPosition.x;
				jumpSpeed.x = NanoMath.mulFixed( jumpSpeed.x, NanoMath.HALF );
				jumpSpeed.y = NanoMath.mulFixed( jumpSpeed.y, NanoMath.divInt( 80, 100 ) );
				fp_accRealTime = 0;
				
				if ( jumpType == SEQUENCE_JUMP_CENTER_HIGH )
					setSequence( SEQUENCE_FALLING );
				else
					setSequence( SEQUENCE_BOUNCING_UP );
			break;
			
			case JUMP_STATE_BOUNCING_DOWN_1:
			case JUMP_STATE_BOUNCING_DOWN_2:
				if ( jumpType == SEQUENCE_JUMP_CENTER_HIGH )
					setSequence( SEQUENCE_FALLING );
				else
					setSequence( SEQUENCE_BOUNCING_DOWN );
			break;
			
			case JUMP_STATE_BRAKING:
				realInitialPosition.x = realPosition.x;
				realPosition.y = 0;
				realInitialPosition.y = 0;
				fp_currentBrakingTime = Math.abs( NanoMath.divFixed( jumpSpeed.x, FP_BRAKE_ACCELERATION ) );
			break;
			
			case JUMP_STATE_FALLING:
				nextFrame();
			break;
			
			case JUMP_STATE_FINISHED:
				switch ( jumpType ) {
					case SEQUENCE_GIVE_UP:
					case SEQUENCE_JUMP_CENTER_LOW:
					break;
					
					case SEQUENCE_JUMP_CENTER_HIGH:
						nextFrame();
					break;
					
					default:
						setSequence( SEQUENCE_LAY_DOWN );
				}
			break;
		}
		
		jumpState = ( byte ) state;
	}
	
	
	private final void calculateJumpSpeed( int fp_direction, int fp_height ) {
		jumpSpeed.y = NanoMath.sqrtFixed( NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), FP_GRAVITY_ACCELERATION ), fp_height ) );

		int fp_jumpTime = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), jumpSpeed.y ), FP_GRAVITY_ACCELERATION );
		if ( fp_jumpTime < FP_MIN_JUMP_TIME )
			fp_jumpTime = FP_MIN_JUMP_TIME;
		
		jumpSpeed.x = NanoMath.divFixed( fp_direction, fp_jumpTime );
	}
	
	
	public final void update( int delta ) {
		delta = match.getDelta();
		
		super.update( delta );
		
		// converte o intervalo de atualiza��o de milisegundos para segundos
		delta = NanoMath.divInt( delta, 1000 );
		
		switch ( jumpState ) {
			case JUMP_STATE_RISING:
				updateRealPosition( delta );
				if ( fp_instantYSpeed <= 0 )
					setJumpState( JUMP_STATE_FALLING );
			break;

			case JUMP_STATE_BOUNCING_UP_1:
			case JUMP_STATE_BOUNCING_UP_2:
				updateRealPosition( delta );
				if ( fp_instantYSpeed <= 0 )
					setJumpState( jumpState + 1 );
			break;

			case JUMP_STATE_FALLING:
				updateRealPosition( delta );
				if ( realPosition.y <= 0 ) {
					switch ( jumpType ) {
						case SEQUENCE_JUMP_CENTER_HIGH:
						case SEQUENCE_JUMP_SIDE_HIGH:
						case SEQUENCE_JUMP_SIDE_MIDDLE:
							setJumpState( JUMP_STATE_BOUNCING_UP_1 );
						break;
						
						case SEQUENCE_JUMP_CENTER_LOW:
						case SEQUENCE_JUMP_SIDE_LOW:
							setJumpState( JUMP_STATE_BRAKING );
						break;
					}
				}
			break;

			case JUMP_STATE_BOUNCING_DOWN_1:
				updateRealPosition( delta );
				if ( realPosition.y <= 0 ) {
					switch ( jumpType ) {
						case SEQUENCE_JUMP_SIDE_HIGH:
						case SEQUENCE_JUMP_CENTER_HIGH:
							setJumpState( JUMP_STATE_BOUNCING_UP_2 );
						break;

						case SEQUENCE_JUMP_SIDE_MIDDLE:
							setJumpState( JUMP_STATE_FINISHED );
						break;
					}
				}
			break;

			case JUMP_STATE_BOUNCING_DOWN_2:
				updateRealPosition( delta );
				if ( realPosition.y <= 0 )
					setJumpState( JUMP_STATE_FINISHED );
			break;

			case JUMP_STATE_BRAKING:
				// calcula a posi��o x real atual do goleiro (n�o � necess�rio atualizar a posi��o y)
				fp_accRealTime += delta;

				if ( fp_accRealTime > fp_currentBrakingTime )
					fp_accRealTime = fp_currentBrakingTime;

				// x = x0 + v0t + at�/2;
				final int fp_t2_div2 = NanoMath.mulFixed( NanoMath.mulFixed( fp_accRealTime, fp_accRealTime ), NanoMath.HALF );
				realPosition.x = realInitialPosition.x + NanoMath.mulFixed( jumpSpeed.x, fp_accRealTime ) + 
						NanoMath.mulFixed( ( jumpSpeed.x >= 0 ? FP_BRAKE_ACCELERATION : -FP_BRAKE_ACCELERATION ), fp_t2_div2 );

				if ( fp_accRealTime >= fp_currentBrakingTime ) {
					setJumpState( JUMP_STATE_FINISHED );
				}
			break;
		}

		updateScreenPosition();
	}
	
	
	private final void updateRealPosition( int delta ) {
		fp_accRealTime += delta;

		realPosition.x = realInitialPosition.x + NanoMath.mulFixed( jumpSpeed.x, fp_accRealTime );

		final int fp_gt = NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accRealTime );
		fp_instantYSpeed = jumpSpeed.y + fp_gt;

		realPosition.y = NanoMath.mulFixed( jumpSpeed.y, fp_accRealTime ) + NanoMath.divFixed( NanoMath.mulFixed( fp_gt, fp_accRealTime ), NanoMath.toFixed( 2 ) );
		if ( realPosition.y < 0 )
			realPosition.y = 0;
	}
	
	
	private final void updateScreenPosition() {
		setRefPixelPosition( INITIAL_X + ( NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_GOAL_WIDTH, NanoMath.divFixed( realPosition.x, FP_REAL_GOAL_WIDTH ) ) ) ),
							 INITIAL_Y - ( NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_FLOOR_TO_BAR, NanoMath.divFixed( realPosition.y, FP_REAL_FLOOR_TO_BAR ) ) ) ) );
	}
	
	
	public final void setLastGiveUpBehaviour( boolean giveUp ) {
		gaveUpLastPlay = giveUp;
	}
	
	
	public final XYZ getRealPosition() {
		return realPosition;
	}

	
}
