/**
 * Control.java
 * �2008 Nano Games.
 *
 * Created on Mar 24, 2008 5:05:54 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class Control extends UpdatableGroup implements Constants {

	// tipos de controle dispon�veis
	public static final byte CONTROL_NONE		= 0;
	public static final byte CONTROL_DIRECTION	= 1;
	public static final byte CONTROL_HEIGHT		= 2;
	public static final byte CONTROL_CURVE		= 3;
	public static final byte CONTROL_POWER		= 4;
	
	public static final byte CONTROL_DONE		= 5;
	
	private static final byte CONTROL_TOTAL		= 6;

	// tempo em segundos que o controle leva para percorrer toda a faixa de valores
	public static final int FP_CONTROL_TIME = 34865;//NanoMath.divInt( 532, 1000 );

	// turnos poss�veis
	public static final byte TURN_ATTACK = 0;
	public static final byte TURN_DEFEND = 1;

	// n�mero de pontos desenhados para indicar o efeito na bola
	public static final byte CURVE_N_POINTS = 8;

	// fatores multiplicados pela jogada aleat�ria, de forma a reduzir o n�mero de chutes para fora
	public static final int FP_RANDOM_DIRECTION_FACTOR	= 60293;//NanoMath.divInt( 92, 100 );
	public static final int FP_RANDOM_HEIGHT_FACTOR		= 57016;//NanoMath.divInt( 87, 100 );
	
	
	// valores m�nimos e m�ximos de cada controle no modo p�nalti
	public static final int FP_CONTROL_DIRECTION_MIN_VALUE	= -349177;//NanoMath.mulFixed( FP_REAL_GOAL_WIDTH, NanoMath.divInt( -8, 10 )  );
	public static final int FP_CONTROL_DIRECTION_MAX_VALUE	= -FP_CONTROL_DIRECTION_MIN_VALUE; 
	public static final byte FP_CONTROL_HEIGHT_MIN_VALUE	= 0;
	public static final int FP_CONTROL_HEIGHT_MAX_VALUE		= 383385;//NanoMath.mulFixed( FP_REAL_FLOOR_TO_BAR, NanoMath.divInt( 195, 100 ) );
	
	public static final int FP_CONTROL_DIRECTION_KEEPER_FACTOR	= 29491;//NanoMath.divInt( 45, 100 );
	public static final int FP_CONTROL_HEIGHT_KEEPER_FACTOR		= 17694;//NanoMath.divInt( 27, 100 );
	public static final int FP_CONTROL_DIRECTION_MAX_KEEPER_VALUE	= 157128;//NanoMath.mulFixed( FP_CONTROL_DIRECTION_MAX_VALUE, FP_CONTROL_DIRECTION_KEEPER_FACTOR );
	public static final int FP_CONTROL_HEIGHT_MAX_KEEPER_VALUE	= 103509;//NanoMath.mulFixed( FP_CONTROL_HEIGHT_MAX_VALUE, FP_CONTROL_HEIGHT_KEEPER_FACTOR );
	
	// o ponto mais alto do controle que o jogador visualiza
	public static final int FP_CONTROL_HEIGHT_MAX_VISIBLE	= 263454;//NanoMath.mulFixed( FP_REAL_FLOOR_TO_BAR, NanoMath.divInt( 134, 100 ) );
	public static final int FP_CONTROL_POWER_MIN_VALUE 	= -707796;//NanoMath.mulFixed( FP_REAL_PENALTY_TO_GOAL, NanoMath.divInt( -12, 10 )  );
	public static final int FP_CONTROL_POWER_MAX_VALUE 	= -1592532;//NanoMath.mulFixed( FP_REAL_PENALTY_TO_GOAL, NanoMath.divInt( -27, 10 )  );
	
	private final int DIRECTION_PENALTY_MIN_X;
	private final int DIRECTION_PENALTY_MAX_X;
	
	private final int HEIGHT_PENALTY_MIN_Y;
	private final int HEIGHT_PENALTY_MAX_Y;
	
	private final int POWER_Y;
	private final int FP_POWER_HEIGHT;

	// passo de tempo em segundos do c�lculo dos pontos da curva
	public static final int FP_CURVE_POINT_STEP = 4718;//NanoMath.divInt( 72, 1000 );

	/** N�mero m�ximo de itens da cole��o. */
	private static final byte CONTROL_MAX_ITEMS = ( 4 + CURVE_N_POINTS );
	
	/** Valor m�nimo e m�ximo da curva. */
	public static final int FP_CURVE_MIN_VALUE	= -NanoMath.ONE;
	public static final int FP_CURVE_MAX_VALUE = NanoMath.ONE;
	
	/** Array que cont�m as imagens dos pontos do efeito da bola */
	private final Sprite[] curvePoints = new Sprite[ CURVE_N_POINTS ]; 
	
	private final Drawable direction;
	
	private final Drawable height;
	
	private final DrawableImage powerBar;
	
	private final DrawableImage powerLevel;

	// jogada armazenada temporariamente
	private final Play currentPlay = new Play();		
	
	// controle atualmente sendo usado
	private byte currentControl;	

	// TURN_ATTACK/TURN_DEFEND
	private byte turn;
	
	private final XYZ ballDirectionUnit = new XYZ();

	private int fpCurrentValue;
	private int fpCurrentMinValue;
	private int fpCurrentMaxValue;
	private int currentMinPos;
	private int currentMaxPos;
	
	private int fpAccTime;

	private boolean increasing;
	
	private final int CURVE_INITIAL_Y;
	
	private final Mutex mutex = new Mutex();
	
	
	/**
	 * 
	 * @param goal 
	 * @param ballPosY 
	 * @throws java.lang.Exception
	 */
	public Control( Drawable goal, int ballPosY, int totalHeight ) throws Exception {
		super( CONTROL_MAX_ITEMS );
		
		CURVE_INITIAL_Y = ballPosY;
		
		setSize( ScreenManager.SCREEN_WIDTH, totalHeight );
		
		// insere a seta indicadora de dire��o
		if ( GameMIDlet.isLowMemory() )
			direction = new DrawableImage( PATH_IMAGES + "arrow_3.png" );
		else
			direction = new Sprite( PATH_IMAGES + "arrow.dat", PATH_IMAGES + "arrow" );
		
		direction.defineReferencePixel( direction.getWidth() >> 1, direction.getHeight() );
		direction.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, goal.getPosY() );
		insertDrawable( direction );
		
		DIRECTION_PENALTY_MIN_X = goal.getPosX() - direction.getWidth();
		DIRECTION_PENALTY_MAX_X = DIRECTION_PENALTY_MIN_X + goal.getWidth() + ( direction.getWidth() << 1 );
		
		// insere a seta indicadora de altura
		HEIGHT_PENALTY_MIN_Y = goal.getPosY() + goal.getHeight();
		HEIGHT_PENALTY_MAX_Y = HEIGHT_PENALTY_MIN_Y - NanoMath.divFixed( NanoMath.mulFixed( FP_CONTROL_HEIGHT_MAX_VISIBLE, PENALTY_FLOOR_TO_BAR ), FP_REAL_FLOOR_TO_BAR );
	
		// insere a seta indicadora de altura
		//#if ROTATE_BUG == "true"
			//#if JAR == "full"
//# 			if ( GameMIDlet.isLowMemory() )
//# 				height = new DrawableImage( PATH_IMAGES + "arrow_rot_3.png" );
//# 			else
//# 				height = new Sprite( PATH_IMAGES + "arrow_rot.dat", PATH_IMAGES + "arrow_rot" );
			//#else
//# 				height = new DrawableImage( PATH_IMAGES + "arrow_rot_3.png" );
			//#endif		
		//#else
			//#if JAR == "full"
		if ( GameMIDlet.isLowMemory() )
			height = new DrawableImage( ( DrawableImage ) direction );
		else
			height = new Sprite( ( Sprite ) direction );
			//#else
//# 				height = new DrawableImage( ( DrawableImage ) direction );
			//#endif
				
			height.setTransform( TRANS_ROT270 );
		//#endif
		
		height.defineReferencePixel( height.getWidth(), height.getHeight() >> 1 );
		height.setRefPixelPosition( goal.getPosition() );
		insertDrawable( height );
		
		// insere o controle de curva
		final Sprite curve = new Sprite( PATH_IMAGES + "curve.dat", PATH_IMAGES + "ball" );
		curve.defineReferencePixel( curve.getWidth() >> 1, curve.getHeight() >> 1 );
		insertDrawable( curve );
		
		curvePoints[ CURVE_N_POINTS - 1 ] = curve;
		
		for ( byte i = CURVE_N_POINTS - 2; i >= 0; --i ) {
			final Sprite s = new Sprite( curve );
			curvePoints[ i ] = s;
			s.defineReferencePixel( s.getWidth() >> 1, s.getHeight() >> 1 );
			insertDrawable( s );
		}
		
		// insere o controle de for�a
		powerBar = new DrawableImage( PATH_IMAGES + "control_power.png" );
		powerBar.setPosition( ScreenManager.SCREEN_WIDTH - powerBar.getWidth(), CURVE_INITIAL_Y - ( ( powerBar.getHeight() * 3 ) >> 1 ) );		
		insertDrawable( powerBar );
		
		POWER_Y = powerBar.getPosY() + CONTROL_POWER_BAR_INTERNAL_Y;		
		FP_POWER_HEIGHT = NanoMath.toFixed( CONTROL_POWER_BAR_INTERNAL_HEIGHT );
		
		powerLevel = new DrawableImage( PATH_IMAGES + "control_power_level.png" );
		powerLevel.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( powerLevel );

		setVisible( false );
	}
	
	
	/**
	 *
	 */
	public final void reset() {
		mutex.acquire();
		
		setVisible( true );
		
		for ( byte i = 0; i < CONTROL_MAX_ITEMS; ++i )
			getDrawable( i ).setVisible( false );
		
		currentControl = CONTROL_DIRECTION;
		
		fpCurrentMinValue = FP_CONTROL_DIRECTION_MIN_VALUE;
		fpCurrentMaxValue = FP_CONTROL_DIRECTION_MAX_VALUE;
		
		currentMinPos = DIRECTION_PENALTY_MIN_X;
		currentMaxPos = DIRECTION_PENALTY_MAX_X;
		
		resetCurrentControlRandomly();
		
		direction.setVisible( true );
		
		mutex.release();
	} // fim do m�todo reset()
	
	
	/**
	 *
	 * 
	 * @return 
	 */
	public final boolean setControl() {
		mutex.acquire();
		
		switch ( currentControl ) {
			case CONTROL_DIRECTION:
				// o pr�ximo controle � da altura
				currentPlay.fp_direction = fpCurrentValue;
				
				fpCurrentMinValue = FP_CONTROL_HEIGHT_MIN_VALUE;
				fpCurrentMaxValue = FP_CONTROL_HEIGHT_MAX_VALUE;
				
				currentMinPos = HEIGHT_PENALTY_MIN_Y;
				currentMaxPos = HEIGHT_PENALTY_MAX_Y;
				
				height.setVisible( true );
			break;

			case CONTROL_CURVE:
				// o pr�ximo controle � da for�a
				currentPlay.fp_curve = fpCurrentValue;
				
				updateCurvePosition();
				
				fpCurrentMinValue = FP_CONTROL_POWER_MIN_VALUE;
				fpCurrentMaxValue = FP_CONTROL_POWER_MAX_VALUE;
				
				powerBar.setVisible( true );
				powerLevel.setVisible( true );
			break;
				
			case CONTROL_HEIGHT:
				// se o jogador estiver defendendo, encerra a jogada; caso esteja atacando, o pr�ximo controle �
				// da curva
				currentPlay.fp_height = fpCurrentValue;
				if ( turn == TURN_DEFEND ) {
					mutex.release();
					return true;
				}
				
				for ( byte i = 0; i < CURVE_N_POINTS; ++i ) {
					curvePoints[ i ].setVisible( true );
					curvePoints[ i ].setSequence( i );
				}
				
				fpCurrentMinValue = FP_CURVE_MIN_VALUE;
				fpCurrentMaxValue = FP_CURVE_MAX_VALUE;
			break;
				
			case CONTROL_POWER:
				// encerrou a jogada
				currentPlay.fp_power = fpCurrentValue;
				
				//#if DEBUG == "true"
				if ( turn != TURN_ATTACK )
					System.out.println( "turno inv�lido para controle de for�a!" );
				//#endif
				mutex.release();
			return true;
			
			case CONTROL_DONE:
			//#if DEBUG == "true"
			default:
				System.out.println( "Estado inv�lido do controle: " + currentControl );
			//#endif
				mutex.release();
				return false;
		} // fim switch ( currentControl )
		
		resetCurrentControlRandomly();
		currentControl = ( byte ) ( ( currentControl + 1 ) % CONTROL_TOTAL );
		
		mutex.release();
		
		update( 0 );
		
		return false;
	} // fim do m�todo setControl()
	
	
	private final void resetCurrentControlRandomly() {
		fpAccTime = 0;
		increasing = NanoMath.randInt( 100 ) < 50;
		fpCurrentValue = fpCurrentMinValue + NanoMath.mulFixed( NanoMath.randFixed(), ( fpCurrentMaxValue - fpCurrentMinValue ) );
	}
	
	
	/**
	 * Gera uma jogada aleat�ria, a partir da m�dia de duas jogadas aleat�rias, para reduzir
	 * o n�mero de chutes para fora do computador.
	 * 
	 * @return 
	 */
	public final Play getRandomPlay() {
		final Play randomPlay = new Play();
		
		int dirMinValue = FP_CONTROL_DIRECTION_MIN_VALUE;
		int dirMaxValue = FP_CONTROL_DIRECTION_MAX_VALUE;
		int heightMinValue = FP_CONTROL_HEIGHT_MIN_VALUE;
		int heightMaxValue = FP_CONTROL_HEIGHT_MAX_VALUE;
		int powerMinValue = FP_CONTROL_POWER_MIN_VALUE;
		int powerMaxValue = FP_CONTROL_POWER_MAX_VALUE;
		int curveMaxValue = FP_CURVE_MAX_VALUE;
		int curveMinValue = FP_CURVE_MIN_VALUE;
		
		final Play temp = new Play();
		
		final int FP_2 = NanoMath.toFixed( 2 );
		
		temp.fp_direction =  dirMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), dirMaxValue - dirMinValue ) );
		
		randomPlay.fp_direction =  dirMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), dirMaxValue - dirMinValue ) );
		randomPlay.fp_direction = NanoMath.divFixed( NanoMath.mulFixed( FP_RANDOM_DIRECTION_FACTOR, ( randomPlay.fp_direction + temp.fp_direction ) ), FP_2 );
		
		temp.fp_height =  heightMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), heightMaxValue - heightMinValue ) );
		
		randomPlay.fp_height =  heightMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), heightMaxValue - heightMinValue ) );
		randomPlay.fp_height = NanoMath.divFixed( NanoMath.mulFixed( FP_RANDOM_HEIGHT_FACTOR, ( randomPlay.fp_height + temp.fp_height ) ), FP_2 );
		
		temp.fp_curve =  curveMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), curveMaxValue - curveMinValue ) );
		
		randomPlay.fp_curve =  curveMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), curveMaxValue - curveMinValue ) );
		randomPlay.fp_curve = NanoMath.divFixed( randomPlay.fp_curve + temp.fp_curve, NanoMath.toFixed( 2 ) );
		
		randomPlay.fp_power = powerMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), powerMaxValue - powerMinValue ) );
		
		return randomPlay;
	} // fim do m�todo getRandomPlay()
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		// converte o tempo de milisegundos para segundos
		final int fp_delta = NanoMath.divInt( delta, 1000 );
		
		switch ( currentControl ) {
			case CONTROL_DIRECTION:
			case CONTROL_HEIGHT:
			case CONTROL_CURVE:
				mutex.acquire();
				
				fpAccTime += fp_delta;
		
				if ( increasing ) {
					fpCurrentValue = fpCurrentMinValue + NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpCurrentMaxValue - fpCurrentMinValue ), FP_CONTROL_TIME );
					if ( fpCurrentValue >= fpCurrentMaxValue ) {
						increasing = !increasing;
						fpAccTime = 0;
					}
				} else {
					fpCurrentValue = fpCurrentMaxValue - NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpCurrentMaxValue - fpCurrentMinValue ), FP_CONTROL_TIME );
					if ( fpCurrentValue <= fpCurrentMinValue ) {
						increasing = !increasing;
						fpAccTime = 0;
					}
				}
				
				fpCurrentValue = NanoMath.clamp( fpCurrentValue, fpCurrentMinValue, fpCurrentMaxValue );
				if ( currentControl == CONTROL_CURVE ) {
					// calcula o valor do coeficiente de efeito
					updateCurvePosition();
				}
				
				mutex.release();
			break;
			
			case CONTROL_POWER:
				mutex.acquire();
				
				fpAccTime = ( fpAccTime + fp_delta ) % FP_CONTROL_TIME;
				fpCurrentValue = fpCurrentMinValue + NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpCurrentMaxValue - fpCurrentMinValue ), FP_CONTROL_TIME );
				
				mutex.release();
			break;
			
			default:
			return;
		} // fim switch ( currentControl )

		// atualiza apenas a posi��o do controle atual
		final int fp_diffPos = NanoMath.toFixed( currentMaxPos - currentMinPos );

		// atualiza posi��o
		switch ( currentControl ) {
			case CONTROL_POWER:
				powerLevel.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, POWER_Y + NanoMath.toInt( 
								NanoMath.divFixed( NanoMath.mulFixed( FP_CONTROL_POWER_MAX_VALUE - fpCurrentValue, FP_POWER_HEIGHT ), 
										   FP_CONTROL_POWER_MAX_VALUE - FP_CONTROL_POWER_MIN_VALUE ) ) );
			break;

			case CONTROL_DIRECTION:
				direction.setRefPixelPosition( ( ( currentMinPos + currentMaxPos ) >> 1 ) + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fpCurrentValue, fp_diffPos ), fpCurrentMaxValue - fpCurrentMinValue ) ), 
						direction.getRefPixelY() );
			break;

			case CONTROL_HEIGHT:
				height.setRefPixelPosition( height.getRefPixelX(), currentMinPos + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fpCurrentValue, fp_diffPos ), fpCurrentMaxValue - fpCurrentMinValue ) ) );
			break;
		}
	} // fim do m�todo update( int )
	
	
	public final void updateCurvePosition() {
		// calcula o valor do coeficiente de efeito
		final XYZ yVector = new XYZ( 0, NanoMath.ONE, 0 );
		final XYZ curveVector = new XYZ();
		final XYZ pointPosition = new XYZ();
		ballDirectionUnit.set( -currentPlay.fp_direction, 0, FP_REAL_PENALTY_TO_GOAL  );
		ballDirectionUnit.normalize();		
		int fp_time;
		
		for ( byte i = 0; i < CURVE_N_POINTS; ++i ) {
			fp_time = NanoMath.mulFixed( NanoMath.toFixed( i + 1 ), FP_CURVE_POINT_STEP );
			
			curveVector.set( ballDirectionUnit.cross( yVector ).mult( NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.mulFixed( FP_CONTROL_POWER_MAX_VALUE, fpCurrentValue ), FP_BALL_CURVE_FACTOR ), fp_time ) ) );
			pointPosition.set( ballDirectionUnit.mult( NanoMath.mulFixed( FP_CONTROL_POWER_MIN_VALUE, fp_time ) ).add( curveVector.mult( NanoMath.mulFixed( fp_time, fp_time ) ) ) );
			
			curvePoints[ i ].setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( pointPosition.x, FP_PENALTY_GOAL_WIDTH ), FP_REAL_GOAL_WIDTH ) ),
						CURVE_INITIAL_Y + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( pointPosition.z, FP_PENALTY_MARK_TO_GOAL ), FP_REAL_PENALTY_TO_GOAL ) ) );
		}
	} // fim do m�todo updateCurvePosition()
	
	
	public final void setTurn( byte t ) { 
		turn = t; 
	}
	 
	 
	public final byte getTurn() { 
		return turn; 
	 
	}
	 
	 
	public byte getCurrentControl() { 
		return currentControl; 
	}	
	 
	 
	public final Play getCurrentPlay() {
		return currentPlay;
	}


	public final boolean contains( int x, int y ) {
		return currentControl != CONTROL_DONE && currentControl != CONTROL_NONE && contains( x, y );
	}
	
	
	/**
	 * Obt�m a posi��o y do controle de dire��o.
	 */
	public final int getDirectionIconPosY() {
		return direction.getPosY();
	}
	
	
}
