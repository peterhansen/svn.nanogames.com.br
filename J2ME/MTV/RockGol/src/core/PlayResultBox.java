/**
 * PlayResultBox.java
 * �2008 Nano Games.
 *
 * Created on Apr 7, 2008 11:25:35 AM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class PlayResultBox extends GenericInfoBox implements Constants
		//#if JAR == "full"
		, SpriteListener 
		//#endif
{
	
	private static final byte TOTAL_ITEMS = 4;
	
	public final short PARTIAL_HEIGHT;
	
	private static final byte MODE_TEXT_ONLY			= 0;
	
	private byte mode;
	
	private final MarqueeLabel label;
	
	/** �ndice da anima��o de fim de jogo. */
	public static final byte PLAY_RESULT_ANIMATION_ENDING = 111;
	
	
	//#if JAR == "full"
	
	private static final byte MODE_TEXT_AND_ANIMATION	= 1;
	
	private static final byte ANIMATION_STATE_NONE				= 0;
	private static final byte ANIMATION_STATE_PLAYER_SHOOTING	= 1;
	private static final byte ANIMATION_STATE_BALL_MOVING		= 2;
	private static final byte ANIMATION_STATE_PLAY_RESULT		= 3;
	private static final byte ANIMATION_STATE_CHEERS			= 4;
	private static final byte ANIMATION_STATE_ENDING			= 5;
	
	private byte animationState;
	
	private byte lastPlayResult;
	
	public static final byte ANIMATED_BOARD_WIDTH = 126;
	public static final byte ANIMATED_BOARD_HEIGHT = 55;
	
	public final short FULL_HEIGHT;

	private static final byte PLAYER_X = 6;
	private static final byte KEEPER_X = 80;
	private static final byte PLAYERS_INITIAL_Y = 16;	

	/** N�mero m�ximo e m�nimo de "caracteres" num xingamento. */
	private static final byte CURSE_MAX_LENGTH = 5;
	
	/** Tipos de xingamento diferentes (n�mero total de frames). */
	private static final byte CURSE_N_FRAMES = 7;

	/** �ndice do "xingador" batedor. */
	private static final byte CURSER_PLAYER	= 0;

	/** �ndice do "xingador" goleiro. */
	private static final byte CURSER_KEEPER	= 1;

	/** Tipo de defesa do goleiro: o goleiro toma uma bolada e cai sentado. */
	private static final byte REJECTED_KEEPER_FALL = 0;

	/** Tipo de defesa do goleiro: espalma a bola*/
	private static final byte REJECTED_KEEPER_AWAY = 1;

	/** Tipo de defesa do goleiro: "encaixa" a bola. */
	private static final byte REJECTED_KEEPER_HOLD = 2;

	/** Quantidade de tipos de defesa do goleiro. */
	private static final byte REJECTED_KEEPER_N_TYPES = 3;


	/** Tipo de comemora��o: apenas fica parado. */
	private static final byte CHEER_NONE = 0;

	/** Tipo de comemora��o: levanta os bra�os. */
	private static final byte CHEER_RISE_ARMS = 1;

	/** Tipo de comemora��o: fica pulando. */
	private static final byte CHEER_JUMP = 2;

	/** Quantidade de tipos de comemora��o. */
	private static final byte CHEER_N_TYPES = 3;	
	
	// posi��o inicial da bola
	private static final byte BALL_INITIAL_X = PLAYER_X + 18;
	private static final byte BALL_INITIAL_Y = 37;
	
	private static final byte BALL_HELD_UP_X = KEEPER_X + 5;

	// posi��o y dos xingamentos
	private static final byte CURSE_Y = PLAYERS_INITIAL_Y - 8;
	// espa�amento horizontal dos xingamentos
	private static final byte CURSE_X_SPACING = 1;

	// posi��o da imagem do gol
	private static final byte GOAL_X = 102;
	private static final byte GOAL_Y =  12;


	// posi��o da explos�o mostrada quando a bola atinge a trave
	private static final byte EXPLOSION_X = GOAL_X - 10;
	private static final byte EXPLOSION_Y = GOAL_Y - 5;

	// posi��o y do ch�o
	private static final byte FLOOR_Y = 43;
	
	/** Cor utilizada para pintar o ch�o. */
	private static final int FLOOR_COLOR = 0xffd700;
	
	private static final byte SPRITE_ID_PLAYER		= 0;
	private static final byte SPRITE_ID_COLLISION	= 1;
	
	private static final short TIME_PLAY_RESULT = 2000;
	
	private static final byte PLAYER_SEQUENCE_STOPPED	= 0;
	private static final byte PLAYER_SEQUENCE_SHOOTING	= 1;
	private static final byte PLAYER_SEQUENCE_SHOT		= 2;
	private static final byte PLAYER_SEQUENCE_SMILING	= 3;
	private static final byte PLAYER_SEQUENCE_CURSING	= 4;
	private static final byte PLAYER_SEQUENCE_RISE_ARM	= 5;

	// �ndices das sequ�ncias do goleiro
	private static final byte KEEPER_SEQUENCE_STOPPED	= 0;
	private static final byte KEEPER_SEQUENCE_HOLD		= 1;
	private static final byte KEEPER_SEQUENCE_FALLEN	= 2;
	private static final byte KEEPER_SEQUENCE_RISE_ARM	= 3;
	
	private static final byte COLLISION_SEQUENCE_NONE		= 0;
	private static final byte COLLISION_SEQUENCE_EXPLODING	= 1;
	// indica o tempo que um jogador permanece no ar e no ch�o durante a comemora��o do pulo
	private static final byte TIME_CHEER_JUMP_UP = 127;
	private static final short TIME_CHEER_JUMP_DOWN = 230;

	
	private final short BALL_GRAVITY_ACCELERATION = -98;
	// altura do pulo dos jogadores na comemora��o do pulo
	private static final byte BOARD_JUMP_OFFSET_Y = -6;
	// velocidades verticais iniciais da bola para cada tipo de chute
	private static final byte BALL_KEEPER_Y_SPEED = -16;
	private static final byte BALL_GOAL_Y_SPEED = 25;
	private static final byte BALL_POST_GOAL_Y_SPEED = -30;
	private static final byte BALL_POST_BACK_Y_SPEED = -34;
	private static final byte BALL_POST_OUT_Y_SPEED = -35;
	private static final byte BALL_OUT_Y_SPEED = -39;
	
	// ponto de colis�o com a trave
	private static final byte BOARD_POST_X_LIMIT = GOAL_X - 6;

	// velocidades da bola ap�s colis�o com a trave
	private static final byte BOARD_POST_BACK_X_SPEED = -30;
	private static final byte BOARD_POST_BACK_Y_SPEED = -20;
	private static final byte BOARD_POST_GOAL_X_SPEED = 30;
	private static final byte BOARD_POST_GOAL_Y_SPEED = 27;
	private static final byte BOARD_POST_OUT_X_SPEED = 30;
	private static final byte BOARD_POST_OUT_Y_SPEED = -20;
	private static final byte BOARD_GOAL_X_LIMIT = GOAL_X + 14;
	private static final byte BOARD_OUT_X_LIMIT = ANIMATED_BOARD_WIDTH;
	private static final byte BALL_INITIAL_X_SPEED = 50;
	private static final byte BOARD_REJECTED_KEEPER_X_LIMIT = KEEPER_X;
	private static final byte BOARD_REJECTED_KEEPER_X_SPEED = -20;
	private static final byte BOARD_REJECTED_MIN_Y_SPEED = 7;
	private static final byte BOARD_REJECTED_MAX_Y_SPEED = 30;	
	
	private int accAnimTime;
	private int accBallTime;

	// tipo de comemora��o atual do goleiro ou jogador
	private byte cheerType;
	// tipo de defesa do goleiro
	private byte rejectedType;

	// posi��o inicial da bola do placar
	private final Point initialBallPos = new Point();
	// posi��o atual da bola
	private final Point ballPos = new Point();
	
	/** Velocidade atual da bola em pixels por segundo. */
	private final Point ballSpeed = new Point();	
	
	private final Sprite player;
	private final DrawableImage ball;
	private final Sprite keeper;
	private final UpdatableGroup curses;
	private final Sprite collision;
	
	private final UpdatableGroup animationGroup;
	
	//#endif
	
	
	public PlayResultBox( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		label = new MarqueeLabel( font, null );
		label.setSize( ScreenManager.SCREEN_WIDTH, font.getHeight() );
		
		insertDrawable( label );
		
		PARTIAL_HEIGHT = ( short ) ( font.getHeight() );
		
		//#if JAR == "full"
		if ( GameMIDlet.isLowMemory() ) {
			FULL_HEIGHT = PARTIAL_HEIGHT;
			player = null;
			ball = null;
			keeper = null;
			curses = null;
			collision = null;
			animationGroup = null;
		} else {
			FULL_HEIGHT = ( short ) ( PARTIAL_HEIGHT + ANIMATED_BOARD_HEIGHT );

			String path;
			animationGroup = new UpdatableGroup( 7 );
			animationGroup.setSize( ANIMATED_BOARD_WIDTH, ANIMATED_BOARD_HEIGHT );
			animationGroup.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
			animationGroup.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, FULL_HEIGHT );
			insertDrawable( animationGroup  );

			final Pattern floor = new Pattern( FLOOR_COLOR );
			floor.setSize( ANIMATED_BOARD_WIDTH, 2 );
			floor.setPosition( 0, FLOOR_Y );
			animationGroup.insertDrawable( floor );

			path = PATH_BOARD + "player";
			player = new Sprite( path + ".dat", path );
			Thread.yield();
			player.setPosition( PLAYER_X, PLAYERS_INITIAL_Y );
			player.setListener( this, SPRITE_ID_PLAYER );
			animationGroup.insertDrawable( player  );

			path = PATH_BOARD + "keeper";
			keeper = new Sprite( path + ".dat", path );
			Thread.yield();
			keeper.setPosition( KEEPER_X, PLAYERS_INITIAL_Y );
			animationGroup.insertDrawable( keeper  );

			ball = new DrawableImage( PATH_BOARD + "ball.png" );
			Thread.yield();
			ball.setPosition( BALL_INITIAL_X, BALL_INITIAL_Y );
			animationGroup.insertDrawable( ball );

			final DrawableImage goal = new DrawableImage( PATH_BOARD + "goal.png" );
			Thread.yield();
			goal.setPosition( GOAL_X, GOAL_Y );
			animationGroup.insertDrawable( goal );

			path = PATH_BOARD + "collision";
			collision = new Sprite( path + ".dat", path );
			Thread.yield();
			collision.setPosition( EXPLOSION_X, EXPLOSION_Y );
			collision.setListener( this, SPRITE_ID_COLLISION );
			animationGroup.insertDrawable( collision );

			path = PATH_BOARD + "curse";
			final Sprite curse = new Sprite( path + ".dat", path );
			Thread.yield();
			curses = new UpdatableGroup( CURSE_MAX_LENGTH  );
			curses.insertDrawable( curse );
			for ( byte i = 1; i < CURSE_MAX_LENGTH; ++i )
				curses.insertDrawable( new Sprite( curse ) );
			curses.setSize( 0, curse.getHeight() );
			animationGroup.insertDrawable( curses );
		}
		
		//#endif
		
		setMode( MODE_TEXT_ONLY );
		
		setState( STATE_HIDDEN );
	}
	
	
	private final void setMode( byte mode ) {
		//#if JAR == "full"
		if ( GameMIDlet.isLowMemory() )
			mode = MODE_TEXT_ONLY;
		
		switch ( mode ) {
			case MODE_TEXT_ONLY:
				setSize( ScreenManager.SCREEN_WIDTH, PARTIAL_HEIGHT );
				
				if ( animationGroup != null )
					animationGroup.setVisible( false );
				
				setVisibleTime( 0 );
			break;
			
			case MODE_TEXT_AND_ANIMATION:
				setSize( ScreenManager.SCREEN_WIDTH, FULL_HEIGHT );
				animationGroup.setVisible( true );
				setVisibleTime( 0 );
			break;
		}
		
		this.mode = mode;
		//#else
//# 		setSize( ScreenManager.SCREEN_WIDTH, PARTIAL_HEIGHT );
//# 
//# 		setVisibleTime( 0 );		
		//#endif
		
		
		setState( state );
	}	
	

	private final void setClestonText() {
		final StringBuffer buffer = new StringBuffer( GameMIDlet.getText( TEXT_CLESTON_PLAY_BEGIN ) );
		
		final byte E = ( byte ) NanoMath.randInt( 25 );
		for ( byte i = 0; i < E; ++i )
			buffer.append( '�' );
		buffer.append( GameMIDlet.getText( TEXT_CLESTON_PLAY_END ) );
		
		setText( buffer.toString(), true );
	}	
		
	
	public final void setText( int textIndex ) {
		setText( textIndex, false );
	}
	
	
	public final void setText( String text ) {
		setText( text, false );
	}
	
	
	private final void setText( int textIndex, boolean fullSize ) {
		setText( GameMIDlet.getText( textIndex ), fullSize );
	}
	
	
	private final void setText( String text, boolean fullSize ) {
		label.setText( text, false );
		label.setTextOffset( label.getWidth() );
		
		//#if JAR != "full"
//# 		setMode( MODE_TEXT_ONLY );
		//#else
		setMode( fullSize ? MODE_TEXT_AND_ANIMATION : MODE_TEXT_ONLY );
		//#endif
		
		setState( STATE_APPEARING );
	}
	
	
	public final void setPlayResult( byte playResult, boolean playerShot, boolean versusMode ) {
		//#if JAR == "full"
		lastPlayResult = playResult;
		//#endif
		
		switch ( playResult ) {
			case BALL_STATE_GOAL:
			case BALL_STATE_POST_GOAL:
				if ( !versusMode && playerShot )
					setClestonText();
				else
					setText( TEXT_PLAY_RESULT_GOAL, true );
			break;

			case BALL_STATE_OUT:
			case BALL_STATE_POST_OUT:
				setText( TEXT_PLAY_RESULT_OUT, true );
			break;

			case BALL_STATE_REJECTED_KEEPER:
				if ( !versusMode && !playerShot )
					setClestonText();
				else
					setText( TEXT_PLAY_RESULT_DEFENDED, true );
			break;

			case BALL_STATE_POST_BACK:
				setText( TEXT_PLAY_RESULT_POST, true );
			break;
			
 			case PLAY_RESULT_ANIMATION_ENDING:
				setText( TEXT_CHAMPION, true );
 			break;
		
		}
		
	}	
	
	
	//#if JAR == "full"
	
	
	public final void setState( int state ) {
		super.setState( state );
		
		switch ( this.state ) {
			case STATE_APPEARING:
				switch ( mode ) {
					case MODE_TEXT_AND_ANIMATION:
						if ( lastPlayResult == PLAY_RESULT_ANIMATION_ENDING )
							setAnimationState( ANIMATION_STATE_ENDING );
						else
							setAnimationState( ANIMATION_STATE_PLAYER_SHOOTING );
					break;
				}
			break;
		}
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		switch ( state ) {
			case STATE_APPEARING:
			case STATE_SHOWN:
			case STATE_HIDING:
				accAnimTime += delta;

				switch ( animationState ) {
					case ANIMATION_STATE_BALL_MOVING:
					case ANIMATION_STATE_CHEERS:
						updateAnimation( delta );
					break;

					case ANIMATION_STATE_PLAY_RESULT:
						updateAnimation( delta );

						if ( accAnimTime >= TIME_PLAY_RESULT ) {
							setAnimationState( ANIMATION_STATE_CHEERS );
						}
					break;
				} // fim switch ( animationState )				
			break;
		}		
	}


	public final void onSequenceEnded( int id, int sequence ) {
		// jogador acabou de chutar a bola
		switch ( state ) {
			case STATE_APPEARING:
			case STATE_SHOWN:
			case STATE_HIDING:
				switch ( id ) {
					case SPRITE_ID_PLAYER:
						switch ( sequence ) {
							case PLAYER_SEQUENCE_SHOOTING:
								setAnimationState( ANIMATION_STATE_BALL_MOVING );
								player.setSequence( PLAYER_SEQUENCE_SHOT );
							break;
						}
					break;
					
					case SPRITE_ID_COLLISION:
						collision.setSequence( COLLISION_SEQUENCE_NONE );
						collision.setVisible( false );
					break;
				}
			break;
		}
	}
	
	
	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
	
	
	private final void setAnimationState( int animationState ) {
		this.animationState = ( byte ) animationState;

		accAnimTime = 0;

		switch ( animationState ) {
			case ANIMATION_STATE_PLAYER_SHOOTING:
				player.setPosition( PLAYER_X, PLAYERS_INITIAL_Y );
				player.setSequence( PLAYER_SEQUENCE_SHOOTING );

				keeper.setPosition( KEEPER_X, PLAYERS_INITIAL_Y );
				keeper.setSequence( KEEPER_SEQUENCE_STOPPED );

				ball.setPosition( BALL_INITIAL_X, BALL_INITIAL_Y );
				ball.setVisible( true );

				collision.setVisible( false );
				collision.setSequence( COLLISION_SEQUENCE_NONE );

				curses.setVisible( false );

				ballSpeed.x = 0;
				ballSpeed.y = 0;

				initialBallPos.x = BALL_INITIAL_X;
				initialBallPos.y = BALL_INITIAL_Y;
				accBallTime = 0;
			break; // fim case ANIM_KICKING_BALL

			case ANIMATION_STATE_BALL_MOVING:
				ballSpeed.x = BALL_INITIAL_X_SPEED;

				// define a velocidade vertical da bola de acordo com o resultado da jogada
				switch ( lastPlayResult ) {
					case BALL_STATE_REJECTED_KEEPER:
						ballSpeed.y = BALL_KEEPER_Y_SPEED;
					break;

					case BALL_STATE_GOAL:
						ballSpeed.y = -NanoMath.randInt( BALL_GOAL_Y_SPEED );
					break;

					case BALL_STATE_POST_GOAL:
						ballSpeed.y = BALL_POST_GOAL_Y_SPEED;
					break;

					case BALL_STATE_POST_BACK:
						ballSpeed.y = BALL_POST_BACK_Y_SPEED;
					break;

					case BALL_STATE_POST_OUT:
						ballSpeed.y = BALL_POST_OUT_Y_SPEED;
					break;

					case BALL_STATE_OUT:
						ballSpeed.y = BALL_OUT_Y_SPEED;
					break;
				} // fim switch ( playResult )
			break; // fim case ANIMATION_STATE_BALL_MOVING

			case ANIMATION_STATE_PLAY_RESULT:
				switch ( lastPlayResult ) {
					case BALL_STATE_POST_GOAL:
						collision.setVisible( true );
						collision.setSequence( COLLISION_SEQUENCE_EXPLODING );
					case BALL_STATE_GOAL:
						player.setSequence( ( NanoMath.randInt( 128 ) > 64 ) ? PLAYER_SEQUENCE_SMILING : PLAYER_SEQUENCE_RISE_ARM );
						keeper.setSequence( ( NanoMath.randInt( 128 ) > 32 ) ? KEEPER_SEQUENCE_STOPPED : KEEPER_SEQUENCE_FALLEN );
						setCurseWords( CURSER_KEEPER );
					break;

					case BALL_STATE_REJECTED_KEEPER:
						player.setSequence( PLAYER_SEQUENCE_CURSING );
						setCurseWords( CURSER_PLAYER );

						rejectedType = ( byte ) NanoMath.randInt( REJECTED_KEEPER_N_TYPES );

						switch ( rejectedType ) {
							case REJECTED_KEEPER_AWAY:
								ballSpeed.x = BOARD_REJECTED_KEEPER_X_SPEED;

								ballSpeed.y = ( BOARD_REJECTED_MIN_Y_SPEED ) - ( NanoMath.randInt( BOARD_REJECTED_MAX_Y_SPEED ) );

								initialBallPos.set( ball.getPosition() );
								accBallTime = 0;
							break;

							case REJECTED_KEEPER_FALL:
								keeper.setSequence( KEEPER_SEQUENCE_FALLEN );
							break;

							case REJECTED_KEEPER_HOLD:
								keeper.setSequence( KEEPER_SEQUENCE_HOLD );
								ballSpeed.x = 0;
								ballSpeed.y = 0;
								accBallTime = 0;

								ball.setVisible( false );
							break;
						} // fim switch ( rejectedType )
					break;

					case BALL_STATE_POST_BACK:
						collision.setVisible( true );
						collision.setSequence( COLLISION_SEQUENCE_EXPLODING );
						
						player.setSequence( PLAYER_SEQUENCE_CURSING );
						setCurseWords( CURSER_PLAYER );
					break;

					case BALL_STATE_POST_OUT:
						collision.setVisible( true );
						collision.setSequence( COLLISION_SEQUENCE_EXPLODING );
					case BALL_STATE_OUT:
						player.setSequence( PLAYER_SEQUENCE_CURSING );
						setCurseWords( CURSER_PLAYER );
					break;
				} // fim switch ( playResult )
			break; // fim case ANIM_PLAY_RESULT
				
			case ANIMATION_STATE_CHEERS:
				if ( lastPlayResult == BALL_STATE_REJECTED_KEEPER && rejectedType == REJECTED_KEEPER_FALL ) {
					// se o goleiro estiver ca�do, permanece parado
					cheerType = CHEER_NONE;
				} else {
					cheerType = ( byte ) NanoMath.randInt( CHEER_N_TYPES );
				}

				if ( cheerType == CHEER_RISE_ARMS ) {
					switch ( lastPlayResult ) {
						case BALL_STATE_REJECTED_KEEPER:
							if ( rejectedType == REJECTED_KEEPER_HOLD ) {
								ballSpeed.x = 0;
								ballSpeed.y = 0;
								accBallTime = 0;
								ballPos.x = initialBallPos.x = BALL_HELD_UP_X;
								ballPos.y = initialBallPos.y = PLAYERS_INITIAL_Y;
								ball.setPosition( ballPos );
								ball.setVisible( true );
							}
						case BALL_STATE_POST_BACK:
						case BALL_STATE_OUT:
						case BALL_STATE_POST_OUT:
							keeper.setSequence( KEEPER_SEQUENCE_RISE_ARM );
						break;
					} // fim switch ( playResult )
				}
			break;
			
			case ANIMATION_STATE_ENDING:
				if ( !GameMIDlet.isLowMemory() ) {
					ball.setPosition( BALL_INITIAL_X, BALL_INITIAL_Y );
					player.setPosition( PLAYER_X, PLAYERS_INITIAL_Y );
					player.setSequence( PLAYER_SEQUENCE_RISE_ARM );
					keeper.setSequence( KEEPER_SEQUENCE_RISE_ARM );
					keeper.setPosition( KEEPER_X, PLAYERS_INITIAL_Y );
					collision.setVisible( false );
					curses.setVisible( false );
				}
			break;
		} // fim switch ( state )
	}
	

	private final void updateAnimation( int delta ) {
		accBallTime += delta;
		accAnimTime += delta;

		if ( animationState == ANIMATION_STATE_BALL_MOVING || lastPlayResult != BALL_STATE_REJECTED_KEEPER || rejectedType != REJECTED_KEEPER_HOLD ) {
			ballPos.x = initialBallPos.x + ( ballSpeed.x * accBallTime / 1000 );

			ballPos.y = initialBallPos.y + ( ( ballSpeed.y * accBallTime ) / 1000 ) - 
										   ( BALL_GRAVITY_ACCELERATION * accBallTime * accBallTime / 10000000 );
			if ( ballPos.y > BALL_INITIAL_Y ) {
				ballPos.y = BALL_INITIAL_Y;
				ballSpeed.y = -Math.abs( ballSpeed.y >> 1 );

				initialBallPos.set( ballPos );
				accBallTime = 0;
			}
		}
		
		switch ( animationState ) {
			case ANIMATION_STATE_PLAY_RESULT:
				updatePlayResult( delta );
			break;

			case ANIMATION_STATE_BALL_MOVING:
				updateBallMoving( delta );
			break;

			case ANIMATION_STATE_CHEERS:
				updateCheers( delta );
			break;
		} // fim switch ( animationState )

		ball.setPosition( ballPos );
	} // fim do m�todo updateAnimation()


	private final void updateBallMoving( int time ) {
		switch ( lastPlayResult ) {
			case BALL_STATE_POST_GOAL:
				if ( ballPos.x >= BOARD_POST_X_LIMIT ) {
					collision.setVisible( true );

					initialBallPos.set( ball.getPosition() );

					ballSpeed.x = BOARD_POST_GOAL_X_SPEED;
					ballSpeed.y = BOARD_POST_GOAL_Y_SPEED;
					accBallTime = 0;

					setAnimationState( ANIMATION_STATE_PLAY_RESULT );
				}
			break;

			case BALL_STATE_GOAL:
				if ( ballPos.x >= BOARD_GOAL_X_LIMIT ) {
					ballPos.x = BOARD_GOAL_X_LIMIT;
					initialBallPos.x = ballPos.x;
					ballSpeed.x = 0;

					setAnimationState( ANIMATION_STATE_PLAY_RESULT );
				}
			break;

			case BALL_STATE_REJECTED_KEEPER:
				if ( ballPos.x > BOARD_REJECTED_KEEPER_X_LIMIT ) {
					ballPos.x = BOARD_REJECTED_KEEPER_X_LIMIT;

					if ( rejectedType != REJECTED_KEEPER_HOLD ) {
						ballSpeed.x = BOARD_REJECTED_KEEPER_X_SPEED;

						ballSpeed.y = BOARD_REJECTED_MIN_Y_SPEED - NanoMath.randInt( BOARD_REJECTED_MAX_Y_SPEED );

						initialBallPos.set( ball.getPosition() );
						accBallTime = 0;
					}

					setAnimationState( ANIMATION_STATE_PLAY_RESULT );
				}
			break;

			case BALL_STATE_POST_BACK:
				if ( ballPos.x >= BOARD_POST_X_LIMIT ) {
					collision.setVisible( true );

					initialBallPos.set( ball.getPosition() );

					ballSpeed.x = BOARD_POST_BACK_X_SPEED;
					ballSpeed.y = BOARD_POST_BACK_Y_SPEED;

					accBallTime = 0;
					setAnimationState( ANIMATION_STATE_PLAY_RESULT );
				}
			break;

			case BALL_STATE_POST_OUT:
				if ( ballPos.x > BOARD_POST_X_LIMIT ) {
					collision.setVisible( true );

					initialBallPos.set( ball.getPosition() );

					ballSpeed.x = BOARD_POST_OUT_X_SPEED;
					ballSpeed.y = BOARD_POST_OUT_Y_SPEED;

					accBallTime = 0;

					setAnimationState( ANIMATION_STATE_PLAY_RESULT );
				}
			break;

			case BALL_STATE_OUT:
				if ( ballPos.x > BOARD_OUT_X_LIMIT ) {
					setAnimationState( ANIMATION_STATE_PLAY_RESULT );
				}
			break;
		} // fim switch ( playResult )
	} // fim do m�todo updateBallMoving()


	private final void updatePlayResult( int time ) {
		switch ( lastPlayResult ) {
			case BALL_STATE_POST_GOAL:
			case BALL_STATE_GOAL:
				if ( ballPos.x >= BOARD_GOAL_X_LIMIT ) {
					ballPos.x = BOARD_GOAL_X_LIMIT;
					initialBallPos.x = ballPos.x;
					ballSpeed.x = 0;
				}
			break;
		} // fim switch ( playResult )

		if ( accAnimTime >= TIME_PLAY_RESULT ) {
			setAnimationState( ANIMATION_STATE_CHEERS );
		}
	} // fim do m�todo updatePlayResult()
	
	
	private final void updateCheers( int time ) {
		switch ( lastPlayResult ) {
			case BALL_STATE_POST_OUT:
			case BALL_STATE_OUT:
			case BALL_STATE_POST_BACK:
			case BALL_STATE_REJECTED_KEEPER:
				if ( cheerType == CHEER_JUMP ) {
					if ( accAnimTime <= TIME_CHEER_JUMP_UP ) {
						keeper.setPosition( KEEPER_X, PLAYERS_INITIAL_Y + BOARD_JUMP_OFFSET_Y );
					} else if ( accAnimTime <= TIME_CHEER_JUMP_DOWN ) {
						keeper.setPosition( KEEPER_X, PLAYERS_INITIAL_Y );

						if ( rejectedType == REJECTED_KEEPER_HOLD )
							ball.setVisible( false );
					} else {
						accAnimTime = 0;
					}
				} // fim if ( cheerType == CHEER_JUMP )
			break; // BALL_REJECTED_KEEPER

			case BALL_STATE_POST_GOAL:
				if ( ballPos.x >= BOARD_GOAL_X_LIMIT ) {
					ballPos.x = BOARD_GOAL_X_LIMIT;
					initialBallPos.x = ballPos.x;
					ballSpeed.x = 0;
				}

			case BALL_STATE_GOAL:
				switch ( cheerType ) {
					case CHEER_JUMP:
						if ( accAnimTime <= TIME_CHEER_JUMP_UP ) {
							player.setPosition( PLAYER_X, PLAYERS_INITIAL_Y + BOARD_JUMP_OFFSET_Y );
						} else if ( accAnimTime <= TIME_CHEER_JUMP_DOWN ) {
							player.setPosition( PLAYER_X, PLAYERS_INITIAL_Y );
						} else {
							accAnimTime = 0;
						}
					break;
				}
			break; // fim case BALL_GOAL | BALL_POST_GOAL
		} // fim switch ( playResult )
	} // fim do m�todo updateCheers()


	/**
	 * 
	 * @param curserIndex
	 */
	private final void setCurseWords( byte curserIndex ) {
		final byte CURSE_LENGTH = ( byte ) NanoMath.randInt( CURSE_MAX_LENGTH  );
		
		if ( CURSE_LENGTH > 0 ) {
			int x = 0;
			
			for ( byte i = 0; i < CURSE_LENGTH; ++i ) {
				final Sprite curse = ( Sprite ) curses.getDrawable( i );
				
				curse.setFrame( NanoMath.randInt( CURSE_N_FRAMES ) );
				curse.setPosition( x, 0 );
				x += curse.getWidth() + CURSE_X_SPACING;
				curse.setVisible( true );
			}
			
			curses.setSize( x, curses.getHeight() );

			switch ( curserIndex ) {
				case CURSER_PLAYER:
					curses.defineReferencePixel( ANCHOR_HCENTER );
					curses.setRefPixelPosition( player.getPosX(), CURSE_Y );
				break;

				case CURSER_KEEPER:
					curses.defineReferencePixel( ANCHOR_HCENTER );
					curses.setRefPixelPosition( keeper.getPosX(), CURSE_Y );				
				break;
			} // fim switch ( curserIndex )
			
			curses.setVisible( true );
		}
	} // fim do m�todo setCurseWords()


	//#endif

}
