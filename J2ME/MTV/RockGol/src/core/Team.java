/**
 * Team.java
 * �2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:52:41 AM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class Team implements Constants, Serializable {
	
	private final byte shooterIndex;
	
	private final byte keeperIndex;
	
	private byte victories;
	
	private short goalsScored;
	
	private short goalsDiff;
	
	/** �ndice do time do Cleston. */
	public static final byte INDEX_CLESTON = 0;
	
	
	public Team( int teamIndex ) {
		this.shooterIndex = ( byte ) teamIndex;
		keeperIndex = shooterIndex;
	}
	
	
	public Team( int shooterIndex, int keeperIndex ) {
		this.shooterIndex = ( byte ) shooterIndex;
		this.keeperIndex = ( byte ) keeperIndex;
	}
	
	
	public final byte getIndex() {
		return shooterIndex;
	}


	public final short getGoalsDiff() {
		return goalsDiff;
	}


	public final short getGoalsScored() {
		return goalsScored;
	}


	public final byte getVictories() {
		return victories;
	}
	
	
	public final Shooter loadShooter() throws Exception {
		return Shooter.createInstance( shooterIndex );
	}
	
	
	public final Keeper loadKeeper( Drawable goal ) throws Exception {
		return Keeper.createInstance( goal, keeperIndex );
	}
	
	
	public final void updateStatus( boolean won, int goalsScored, int goalsDiff ) {
		if ( won )
			++victories;
		
		this.goalsScored += goalsScored;
		this.goalsDiff += goalsDiff;
	}


	public final void write( DataOutputStream output ) throws Exception {
		output.writeByte( victories );
		output.writeShort( goalsScored );
		output.writeShort( goalsDiff );
	}


	public final void read( DataInputStream input ) throws Exception {
		victories = input.readByte();
		goalsScored = input.readShort();
		goalsDiff = input.readShort();
	}
	
	
	public final String getName() {
		return getName( shooterIndex  );
	}
	
	
	public static final String getName( int index ) {
		return GameMIDlet.getText( TEXT_TEAM_NAME_CLESTON + index );
	}
	
	
	public final String getShortName() {
		return GameMIDlet.getText( TEXT_TEAM_NAME_SHORT_CLESTON + shooterIndex );
	}
	
}
