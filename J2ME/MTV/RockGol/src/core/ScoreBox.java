/**
 * ScoreBox.java
 * �2008 Nano Games.
 *
 * Created on Apr 9, 2008 11:00:12 AM.
 */

package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class ScoreBox extends GenericInfoBox implements Constants, SpriteListener {
	
	/** Quantidade total de caixas indicadoras do resultado de uma jogada. */
	private static final byte TOTAL_BOXES = 12;
	
	private static final byte BOXES_VISIBLE_HORIZONTAL = 5;
	
	private static final byte BOX_SEQUENCE_NONE				= 0;
	private static final byte BOX_SEQUENCE_GOAL				= 1;
	private static final byte BOX_SEQUENCE_NO_GOAL			= 2;
	private static final byte BOX_SEQUENCE_BLINK_GOAL		= 3;
	private static final byte BOX_SEQUENCE_BLINK_NO_GOAL	= 4;
	
	/** N�mero total de itens do grupo. */
	private static final byte TOTAL_ITEMS = 10;
	
	private final Sprite[] boxes = new Sprite[ TOTAL_BOXES ];
	
	private final RichLabel scoreTeam1;
	private final RichLabel scoreTeam2;
	
	/** Velocidade de movimenta��o das caixas. */
	private final MUV boxSpeed = new MUV();
	
	private boolean scrollingBoxes;

	
	public ScoreBox( Team team1, Team team2 ) throws Exception {
		super( TOTAL_ITEMS );
		
		final ImageFont font = GameMIDlet.getFont( FONT_INDEX_BOARD );
		
		final UpdatableGroup boxGroup = new UpdatableGroup( TOTAL_BOXES );
		insertDrawable( boxGroup );
		
		boxes[ 0 ] = new Sprite( PATH_BOARD + "box.dat", PATH_BOARD + "box" );
		boxes[ 0 ].setListener( this, 0 );
		boxGroup.insertDrawable( boxes[ 0 ] );
		
		// define a velocidade de movimenta��o das caixas
		boxSpeed.setSpeed( -boxes[ 0 ].getWidth() >> 2 );
		
		boxGroup.setSize( boxes[ 0 ].getWidth() * BOXES_VISIBLE_HORIZONTAL, boxes[ 0 ].getHeight() << 1 );
		boxGroup.defineReferencePixel( ANCHOR_RIGHT );

		// define o tamanho do grupo
		setSize( ScreenManager.SCREEN_WIDTH, boxGroup.getHeight() + ( font.getHeight() << 1 ) );

		// insere o t�tulo
		final Label labelTitle = new Label( font, GameMIDlet.getText( TEXT_PARTIAL_RESULT ) );
		labelTitle.setPosition( ( size.x - labelTitle.getWidth() ) >> 1, 0 );
		insertDrawable( labelTitle );
		

		final int SCORE_WIDTH = font.getTextWidth( "999" );
		final int TEAM_WIDTH = Math.max( font.getTextWidth( team1.getShortName() ), font.getTextWidth( team2.getShortName() ) );
		final int VISIBLE_WIDTH = boxGroup.getWidth() + SCORE_WIDTH + TEAM_WIDTH;
		final int INITIAL_X = ( ScreenManager.SCREEN_WIDTH - VISIBLE_WIDTH ) >> 1;
		
		boxGroup.setRefPixelPosition( INITIAL_X + VISIBLE_WIDTH, ( labelTitle.getHeight() * 3 ) >> 1 );

		for ( byte i = 1; i < TOTAL_BOXES; ++i ) {
			final Sprite box = new Sprite( boxes[ 0 ] );
			
			box.setPosition( ( i >> 1 ) * box.getWidth(), ( i & 1 ) == 0 ? 0 : box.getHeight() );
			
			box.setListener( this, i );
			
			boxes[ i ] = box;
			boxGroup.insertDrawable( box );
		}

		final Label labelTeam1 = new Label( font, team1.getShortName() );
		labelTeam1.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_RIGHT );
		insertDrawable( labelTeam1 );
		final Label labelTeam2 = new Label( font, team2.getShortName() );
		labelTeam2.defineReferencePixel( ANCHOR_RIGHT );
		insertDrawable( labelTeam2 );
		
		final String scoreString = "<ALN_H>0";
		
		scoreTeam1 = new RichLabel( font, scoreString, SCORE_WIDTH, null );
		scoreTeam1.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_RIGHT );
		insertDrawable( scoreTeam1 );
		
		scoreTeam2 = new RichLabel( font, scoreString, SCORE_WIDTH, null );
		scoreTeam2.defineReferencePixel( ANCHOR_RIGHT );
		insertDrawable( scoreTeam2 );
		
		int x = boxGroup.getPosX();
		final int y = boxGroup.getPosY() + ( boxGroup.getHeight() >> 1 );
		
		scoreTeam1.setRefPixelPosition( x, y );
		scoreTeam2.setRefPixelPosition( x, y );

		x -= SCORE_WIDTH;
		
		labelTeam1.setRefPixelPosition( x, y );
		labelTeam2.setRefPixelPosition( x, y );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case BOX_SEQUENCE_BLINK_GOAL:
				boxes[ id ].setSequence( BOX_SEQUENCE_GOAL );
			break;
			
			case BOX_SEQUENCE_BLINK_NO_GOAL:
				boxes[ id ].setSequence( BOX_SEQUENCE_NO_GOAL );
			break;
		}
	}
	
	
	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}	
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( scrollingBoxes ) {
			final int dx = boxSpeed.updateInt( delta );
			
			if ( dx != 0 ) {
				for ( byte i = 0; i < TOTAL_BOXES; ++i )
					boxes[ i ].move( dx, 0 );
				
				if ( boxes[ 0 ].getPosX() <= -boxes[ 0 ].getWidth() ) {
					shiftBoxes();
					scrollingBoxes = false;
				}
			} // fim if ( dx != 0 )
		} // fim if ( scrollingBoxes )
	} // fim do m�todo update( int )
	
	
	/**
	 * Define o resultado de uma jogada.
	 * 
	 * @param score
	 * @param playResult 
	 * @param currentTurn
	 */
	public final void updateBoard( short[] score, byte playResult, short currentTurn ) {
		int index;

		if ( currentTurn >= 10 ) {
			if ( ( currentTurn & 1 ) == 0 )
				index = 10;
			else
				index = 9;
		} else {
			index = currentTurn % TOTAL_BOXES;
		}

		if ( currentTurn >= 10 && ( ( currentTurn & 1 ) == 0 ) )
			scrollingBoxes = true;
		
		final String alignTag = "<ALN_H>";
		scoreTeam1.setText( alignTag + score[ 0 ] );
		scoreTeam2.setText( alignTag + score[ 1 ] );

		switch ( playResult ) {
			case BALL_STATE_GOAL:
			case BALL_STATE_POST_GOAL:
				boxes[ index ].setSequence( BOX_SEQUENCE_BLINK_GOAL );
			break;
			
			default:
				boxes[ index ].setSequence( BOX_SEQUENCE_BLINK_NO_GOAL );
		}
	} // fim do m�todo setPlayResult( byte, short )	


	/**
	 * Atualiza as caixas.
	 */
	private final void shiftBoxes() {
		for ( byte i = 0; i < TOTAL_BOXES; ++i ) {
			final Sprite box = boxes[ i ];
			box.setPosition( ( i >> 1 ) * box.getWidth(), ( i & 1 ) == 0 ? 0 : box.getHeight() );

			if ( i < TOTAL_BOXES - 2 )
				boxes[ i ].setSequence( boxes[ i + 2 ].getSequence() );
			else
				boxes[ i ].setSequence( BOX_SEQUENCE_NONE );						
		}
	}
	
	
	public final void setState( byte state ) {
		super.setState( state );

		switch ( state ) {
			case STATE_HIDDEN:
				// caso a caixa tenha sido escondida durante a anima��o de scroll das caixas, atualiza o
				// estado das mesmas
				if ( scrollingBoxes ) {
					scrollingBoxes = false;

					shiftBoxes();
				} // fim if ( scrollingBoxes )
			break;
		} // fim switch ( boxState )
	} // fim do m�todo setBoxState( byte )	


}
