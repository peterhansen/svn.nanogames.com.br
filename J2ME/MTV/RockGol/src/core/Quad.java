
/*
 * Quad.java
 *
 * Created on 21 de Agosto de 2007, 22:53
 *
 */
package core;

import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author peter
 */
public final class Quad {
	
	/** Toler�ncia na compara��o de igualdade de 2 valores. */
	private static final int FP_MIN_TOLERANCE = NanoMath.divInt( 1, 100 );
	
	// �ncoras do quad
	public static final byte ANCHOR_TOP_LEFT		= 0;
	public static final byte ANCHOR_TOP				= 1;
	public static final byte ANCHOR_TOP_RIGHT		= 2;
	public static final byte ANCHOR_RIGHT			= 3;
	public static final byte ANCHOR_BOTTOM_RIGHT	= 4;
	public static final byte ANCHOR_BOTTOM			= 5;
	public static final byte ANCHOR_BOTTOM_LEFT		= 6;
	public static final byte ANCHOR_LEFT			= 7;
	public static final byte ANCHOR_CENTER			= 8;
	
	// vetores unit�rios do plano
	public final XYZ top		= new XYZ();
	public final XYZ right		= new XYZ();
	public final XYZ normal		= new XYZ();
	public final XYZ position	= new XYZ();	// a orienta��o considerada � sempre top-left
	public final XYZ center		= new XYZ();	// armazena a posi��o central do quad, de forma a agilizar c�lculos de colis�o
	public int fp_d;							// coordenada d da equa��o do plano: p = ax + by + cz + d
	public int fp_halfDiagonal;					// metade do comprimento da diagonal do quad (acelera futuros c�lculos)
	
	// m�dulos dos vetores (definem a largura e altura do Quad)
	public int fp_width;
	public int fp_height;	
	
	
	public Quad( XYZ pos, byte anchor, XYZ top, XYZ right, int fp_width, int fp_height  ) {
		top.normalize();
		right.normalize();
		normal.set( right.cross( top ) );
		normal.normalize();

		calculateHalfDiagonal();

		setPosByAnchor( pos, anchor  );

		// para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) � um ponto qualquer pertencente a esse plano.
		fp_d = ( normal.mult( -1 ) ).dot( position );		
	}
	
	
	public Quad( XYZ bottomLeft, XYZ topLeft, XYZ bottomRight ) {
		setValues( bottomLeft, topLeft, bottomRight );
	}
	
	
	public Quad() {
	}
	
	
	public final void setValues( XYZ bottomLeft, XYZ topLeft, XYZ bottomRight ) {
		top.set( topLeft.sub( bottomLeft ) );
		fp_height = top.getModule();
		top.normalize();

		right.set( bottomRight.sub(bottomLeft ) );
		fp_width = right.getModule();
		right.normalize();

		normal.set( right.cross( top ) );
		normal.normalize();

		position.set( topLeft );
		final int FP_2 = NanoMath.toFixed( 2 );
		center.set( topLeft.add( right.mult( NanoMath.divFixed( fp_width, FP_2 ) ) ).sub( top.mult( NanoMath.divFixed( fp_height, FP_2 ) ) ) );

		calculateHalfDiagonal();

		// para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) � um ponto qualquer pertencente a esse plano.
		fp_d = ( normal.mult( -1 ) ).dot( position );		
	}
	
	
	public final void setValues( XYZ pos, byte anchor, XYZ t, XYZ r, int fp_width, int fp_height  ) {
		top.set( t );
		right.set( r );
		this.fp_width = fp_width;
		this.fp_height = fp_height;

		top.normalize();
		right.normalize();
		normal.set( right.cross( top ) );
		normal.normalize();

		calculateHalfDiagonal();

		setPosByAnchor( pos, anchor  );

		// para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) � um ponto qualquer pertencente a esse plano.
		fp_d = ( normal.mult( -1 ) ).dot( position );		
	}
	
	
	// 
	/**
	 * Retorna a dist�ncia do ponto ao plano do quad em nota��o de ponto fixo.
	 * 
	 * @param point
	 * @return
	 */
	public final int distanceTo( XYZ point ) {
		return NanoMath.mulFixed( point.x, normal.x ) + 
			   NanoMath.mulFixed( point.y, normal.y ) + 
			   NanoMath.mulFixed( point.z, normal.z ) +
			   fp_d;		
	}
	
	
	/**
	 * Indica se o ponto est� dentro dos limites de altura/largura do quad (n�o testa se o ponto pertence ao plano).
	 * 
	 * @param point 
	 * @param tolerance 
	 * @return 
	 */
	public final boolean isInsideQuad( XYZ point, int tolerance ) {
		if ( tolerance < FP_MIN_TOLERANCE )
			tolerance = FP_MIN_TOLERANCE;

		// primeiro teste: se ponto estiver numa dist�ncia em rela��o ao centro do quad maior que a
		// metade da diagonal do mesmo, n�o pode estar colidindo
		final XYZ distance = point.sub( center );
		if ( distance.getModule() > ( fp_halfDiagonal + tolerance ) )
			return false;

		// colide com a esfera ao redor do centro do Quad; agora verifica-se se o ponto colide com o
		// Quad em si. Para isso, projeta-se o ponto nos vetores top e right do Quad. Caso o m�dulo
		// das proje��es sejam menores que a altura e largura do Quad, h� colis�o. Como os m�dulos
		// de top e right s�o, respectivamente, height e width, n�o h� necessidade de calculas seus
		// m�dulos.
		// c�lculo da proje��o de um vetor sobre outro: proj(B,A) = ( (A.B)/|A|� ) * A
		final XYZ projTop = top.mult( NanoMath.divFixed( top.dot( distance ), NanoMath.mulFixed( fp_height, fp_height ) ) );

		final int FP_2 = NanoMath.toFixed( 2 );
		if ( projTop.getModule() < NanoMath.divFixed( fp_height, FP_2 ) + tolerance ) {
			final XYZ projRight = right.mult( NanoMath.divFixed( right.dot( distance ), NanoMath.mulFixed( fp_width, fp_width ) ) );

			if ( projRight.getModule() < NanoMath.divFixed( fp_width, FP_2 ) + tolerance )
				return true;
		}

		return false;		
	}
	

	private final void setPosByAnchor( XYZ pos, byte anchor  ) {
		final XYZ r = right.mult( fp_width );
		final XYZ t = top.mult( fp_height );
		
		switch ( anchor ) {
			case ANCHOR_TOP_LEFT:
				center.set( pos.add( r.mult( NanoMath.HALF ) ).sub( t.mult( NanoMath.HALF ) ) );
				position.set( pos );
			break;
		
			case ANCHOR_TOP:
				center.set( pos.sub( t.mult( NanoMath.HALF ) ) );
				position.set( pos.sub( r.mult( NanoMath.HALF ) ) );
			break;
			
			case ANCHOR_TOP_RIGHT:
				center.set( pos.sub( r.mult( NanoMath.HALF ) ).sub( t.mult( NanoMath.HALF ) ) );
				position.set( pos.sub( r ) );
			break;
			
			case ANCHOR_RIGHT:
				center.set( pos.sub( r.mult( NanoMath.HALF ) ) );
				position.set( pos.sub( r ).add( t.mult( NanoMath.HALF ) ) );
			break;
			
			case ANCHOR_BOTTOM_RIGHT:
				center.set( pos.sub( r.mult( NanoMath.HALF ) ).add( t.mult( NanoMath.HALF ) ) );
				position.set( pos.sub( r ).add( t ) );
			break;
			
			case ANCHOR_BOTTOM:
				center.set( pos.add( t.mult( NanoMath.HALF ) ) );
				position.set( pos.sub( r.mult( NanoMath.HALF ) ).add( t ) );
			break;
			
			case ANCHOR_BOTTOM_LEFT:
				center.set( pos.add( r.mult( NanoMath.HALF ) ).add( t.mult( NanoMath.HALF ) ) );
				position.set( pos.add( t ) );
			break;
			
			case ANCHOR_LEFT:
				center.set( pos.add( r.mult( NanoMath.HALF ) ) );
				position.set( pos.add( t.mult( NanoMath.HALF ) ) );
			break;
		
			case ANCHOR_CENTER:
				center.set( pos );
				position.set( pos.sub( r.mult( NanoMath.HALF ) ).add( t.mult( NanoMath.HALF ) ) );
			break;
		}		
	}
	
	
	private final void calculateHalfDiagonal() {
		fp_halfDiagonal = NanoMath.mulFixed( NanoMath.sqrtFixed( NanoMath.mulFixed( fp_width, fp_width ) + NanoMath.mulFixed( fp_height, fp_height ) ), NanoMath.HALF );		
	}
	
}
