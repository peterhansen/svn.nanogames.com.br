/**
 * Replay.java
 * �2008 Nano Games.
 *
 * Created on Apr 3, 2008 4:38:37 PM.
 */

//#if JAR != "min"

package core;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 * 
 * @author Peter
 */
public final class Replay implements Constants, Serializable {
	
	/** Momento da grava��o do replay. */
	public long saveTime;
	
	/** �ndice do time do batedor. */
	public byte shooterIndex;
	
	/** �ndice do time do goleiro. */
	public byte keeperIndex;
	
	/** Jogada realizada pelo batedor. */
	public final Play shooterPlay = new Play();

	/** Jogada realizada pelo goleiro. */
	public final Play keeperPlay = new Play();
	
	/** Indica se o goleiro escolheu o canto errado e desistiu do pulo na jogada. */
	public boolean keeperGaveUp;
	
	/** Resultado da jogada. */
	public byte playResult = BALL_STATE_NONE;
	
	/** Dire��o de movimenta��o da bola ap�s a colis�o. */
	public final XYZ collisionDirection = new XYZ();
	
	/** Posi��o exata da bola no momento da "colis�o" com a linha de fundo. */
	public final XYZ ballEndLinePosition = new XYZ();
	
	/** Instante em que houve a defini��o do resultado da jogada. */
	public int collisionTime;
	
	
	public final void write( DataOutputStream output ) throws Exception {
		// resultado da jogada
		output.writeByte( playResult );
		
		if ( playResult != BALL_STATE_NONE ) {
			// momento da grava��o do replay
			output.writeLong( saveTime );
			
			// �ndices dos times do batedor e do goleiro
			output.writeByte( shooterIndex );
			output.writeByte( keeperIndex );
			
			// jogada do batedor
			output.writeInt( shooterPlay.fp_direction  );
			output.writeInt( shooterPlay.fp_height  );
			output.writeInt( shooterPlay.fp_curve  );
			output.writeInt( shooterPlay.fp_power  );
			
			// jogada do goleiro
			output.writeInt( keeperPlay.fp_direction  );
			output.writeInt( keeperPlay.fp_height  );
			output.writeInt( keeperPlay.fp_curve  );
			output.writeInt( keeperPlay.fp_power  );
			output.writeBoolean( keeperGaveUp );
			
			// momento da defini��o do resultado da jogada
			output.writeInt( collisionTime );
			
			// dire��o da bola imediatamente ap�s a defini��o da jogada
			output.writeInt( collisionDirection.x );
			output.writeInt( collisionDirection.y );
			output.writeInt( collisionDirection.z );
			
			// posi��o da bola no momento da defini��o da jogada
			output.writeInt( ballEndLinePosition.x );
			output.writeInt( ballEndLinePosition.y );
			output.writeInt( ballEndLinePosition.z );
		} // fim if ( saved )
	}


	public final void read( DataInputStream input ) throws Exception {
		// resultado da jogada
		playResult = input.readByte();		
		
		if ( playResult != BALL_STATE_NONE ) {
			// momento da grava��o do replay
			saveTime = input.readLong();
			
			// �ndices dos times do batedor e do goleiro
			shooterIndex = input.readByte();
			keeperIndex = input.readByte();
			
			// jogada do batedor
			shooterPlay.fp_direction = input.readInt();
			shooterPlay.fp_height = input.readInt();
			shooterPlay.fp_curve = input.readInt();
			shooterPlay.fp_power = input.readInt();
			
			// jogada do goleiro
			keeperPlay.fp_direction = input.readInt();
			keeperPlay.fp_height = input.readInt();
			keeperPlay.fp_curve = input.readInt();
			keeperPlay.fp_power = input.readInt();
			keeperGaveUp = input.readBoolean();
			
			// momento da defini��o do resultado da jogada
			collisionTime = input.readInt();
			
			// dire��o da bola imediatamente ap�s a defini��o da jogada
			collisionDirection.x = input.readInt();
			collisionDirection.y = input.readInt();
			collisionDirection.z = input.readInt();
			
			// posi��o da bola no momento da defini��o da jogada
			ballEndLinePosition.x = input.readInt();
			ballEndLinePosition.y = input.readInt();
			ballEndLinePosition.z = input.readInt();
			
		} // fim if ( saved )
	}	

}

//#endif