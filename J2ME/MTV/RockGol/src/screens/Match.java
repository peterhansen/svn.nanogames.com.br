/**
 * Stadium.java
 * �2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:47:44 AM.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Ball;
import core.Constants;
import core.Control;
import core.GenericInfoBox;
import core.InfoBox;
import core.Keeper;
import core.Play;
import core.PlayResultBox;
import core.Player;
import core.ScoreBox;
import core.Shooter;
import core.Team;
import javax.microedition.lcdui.Graphics;

//#if JAR != "min"
import core.Replay;
import core.Fireworks;
//#endif

/**
 * 
 * @author Peter
 */
public final class Match extends UpdatableGroup implements Constants, KeyListener, SpriteListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener
		//#endif
{

	private static final byte TOTAL_ITEMS = 10;
	private static final byte FIELD_GROUP_TOTAL_ITEMS = 25;
	
	private static final byte STATE_CREATED					= 0;
	private static final byte STATE_PRESENTATION			= 1;
	private static final byte STATE_LOAD_PLAYERS_MSG		= 2;
	private static final byte STATE_BEGIN_TURN				= 3;
	private static final byte STATE_WAITING_PLAYER_1		= 4;	// esperando que o primeiro jogador defina a jogada
	private static final byte STATE_WAITING_PLAYER_2		= 5;	// esperando que o segundo jogador defina a jogada
	private static final byte STATE_WHISTLE					= 6;	// apito do juiz e reposicionamento de c�mera, se necess�rio
	private static final byte STATE_MOVING					= 7;	// jogada em andamento
	private static final byte STATE_PLAY_RESULT				= 8;	// mostra se foi gol, trave, etc
	private static final byte STATE_PARTIAL_RESULT			= 9;
	private static final byte STATE_REPLAY					= 10;	// DEVE SER PAR!
	private static final byte STATE_REPLAY_WAIT				= 11;	// DEVE SER �MPAR!
	
	//#if JAR != "min"
	private static final byte STATE_REPLAY_ONLY				= 12;	// mostra apenas o replay de uma jogada (volta ao menu de replay ap�s visualiza��o do mesmo)
	private static final byte STATE_REPLAY_ONLY_WAIT		= 13;	// DEVE SER �MPAR! (STATE_REPLAY_ONLY DEVE SER PAR)
	//#endif
	
	private static final byte STATE_FINAL_RESULT			= 14;
	private static final byte STATE_ENDED					= 15;
	private static final byte STATE_CHAMPION_APPEARS		= 16;
	private static final byte STATE_CHAMPION_SHOWN			= 17;
	private static final byte STATE_PLAYER_1_TURN			= 18;
	private static final byte STATE_PLAYER_2_TURN			= 19;
	
	private byte state;
	
	/** Cor s�lida utilizada para preencher a parte inferior da tela (caso a altura da tela seja maior que a altura padr�o). */
	private static final int GRASS_COLOR_LIGHT	= 0x128005;
	private static final int GRASS_COLOR_MEDIUM = 0x107a04;
	private static final int GRASS_COLOR_DARK	= 0x0d6d02;
	

	// tipos de disputa de p�nalti
	/** Partida normal, com jogador chutando primeiro */
	public static final byte MODE_CHAMPIONSHIP_MATCH_HOME	= 0;
	/** Partida normal, com jogador defendendo primeiro. */
	public static final byte MODE_CHAMPIONSHIP_MATCH_AWAY	= 1;
	/** Modo da tela de jogo: treino (jogador sempre come�a chutando). */
	public static final byte MODE_TRAINING					= 2;	
	
	//#if JAR != "min"
	/** Modo da tela de jogo: exibe replay. */
	public static final byte MODE_REPLAY					= 3;
	
	/** Modo da tela de jogo: exibe a anima��o de vit�ria do campeonato. */
	public static final byte MODE_CHAMPION					= 4;
	//#endif
	
	/** Modo da tela de jogo: 2 jogadores. */
	public static final byte MODE_2_PLAYERS					= 5;
	
	private final byte mode;

	/** Placar da partida. */
	private final short[] score = new short[ 2 ];

	/** Turno atual (n�mero total de p�naltis cobrados). */
	private short currentTurn;
	
	// dura��o dos timers em milisegundos
	private static final short TIME_PLAY_RESULT_TRAINING		= 2500;
	private static final short TIME_PRESENTATION				= 6000;
	
	// tempo que a barra leva para aparecer/sumir
	private static final short TIME_PARTIAL_RESULT_ANIM	= 400;
	private static final short TIME_BOARD_ANIMATION		= 5500;
	
	/** Tempo que a mensagem indicando o jogador da vez � exibido. */
	private static final short TIME_PLAYER_TURN_MESSAGE	= 2500;
	
	// tempo entre a defini��o dos controles pelo jogador e o in�cio da movimenta��o do batedor
	private static final short TIME_WHISTLE				= 1200;
	
	/** Pattern utilizado para desenhar o c�u. */
	private static Pattern sky;
	
	/** Cor utilizada para desenhar o c�u na vers�o de baixa mem�ria. */
	private static final int COLOR_SKY = 0x2aa7ff;

	//#if SCREEN_SIZE == "SMALL"
//# 	/** Imagem da parte esquerda do gramado. */
//# 	private static DrawableImage fieldLeft;
//# 	
	//#else
	
	/** Imagem superior esquerda do gramado. */
	private static DrawableImage fieldCenterTopLeft;
	
	/** Imagem superior direita do gramado. */
	private static DrawableImage fieldCenterTopRight;

	/** Imagem central inferior do gramado. */
	private static Drawable fieldCenterBottom;
	
	//#endif
	
	//#if JAR != "min"
	/** Pattern da torcida. */
	private static Pattern crowd;
	
	/** Imagens dos patroc�nios. */
	private static DrawableImage[] ads;
	
	//#endif

	/** Controles do jogador. */
	private final Control control;
	
	/** �ltima jogada realizada pelo batedor. */
	private final Play lastShooterPlay = new Play();
	
	/** �ltima jogada realizada pelo goleiro. */
	private final Play lastKeeperPlay = new Play();
	
	/** Bola do jogo. */
	private final Ball ball;
	
	/** Imagem da rede (� separada do gol para permitir que a bola fique entre as traves e a rede). */
	private static DrawableImage net;
	
	/** Grupo do gol (trave esquerda, travess�o e trave direita). */
	private static DrawableGroup goal;
	
	/** Goleiro atual. */
	private Keeper currentKeeper;

	/** Batedor atual. */
	private Shooter currentShooter;
	
	// tempo em milisegundos at� a pr�xima transi��o (utilizado para emular o comportamento dos times em BREW)
	private int timeToNextTransition;
	
	/** Tempo m�nimo restante para que possa ser acelerada a troca de estado. */
	private int timeMinToAccelerate;
	
	private static final byte REPLAY_SPEED_NORMAL		= 0;
	private static final byte REPLAY_SPEED_SLOW			= 1;
	private static final byte REPLAY_SPEED_VERY_SLOW	= 2;
	
	private byte currentReplaySpeed;
	
	private boolean replayPaused;
	
	/** Intervalo de tempo efetivamente passado aos jogadores e bola, considerando a velocidade do jogo ou do replay. */
	private int lastDelta;

	//#if JAR == "min"
//# 	/** �cone do replay. */
//# 	private static Label replayLabel;
//# 	
//# 	private static final short REPLAY_BLINK_TIME = 444;
//# 	
//# 	private short replayAccTime;
	//#else
	
	/** �cone do replay. */
	private static Sprite replayIcon;
	
	private static final byte REPLAY_ICON_SEQUENCE_NONE				= 0;
	private static final byte REPLAY_ICON_SEQUENCE_APPEARING		= 1;
	private static final byte REPLAY_ICON_SEQUENCE_HIDING			= 2;
	private static final byte REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR	= 3;
	
	/** Imagem da rede balan�ando. */
	private static DrawableImage netAnimation;
	
	/** Viewport da anima��o da rede (utilizado para evitar que a imagem seja desenhada al�m da �rea interna do gol). */
	private final Rectangle netAnimationViewport;
	
	/** Identifica��o do sprite do �cone de replay. */
	private static final byte REPLAY_ICON_SPRITE_ID = 123;
	
	
	/** Controles do replay. */
	private static UpdatableGroup replayControls;
	
	/** Parte direita do controle de replay, no modo de visualiza��o de replay. */
	private static DrawableImage replayControlRightLoad;
	
	/** Parte direita do controle de replay, no modo de jogo (exibe tamb�m a op��o de gravar). */
	private static DrawableImage replayControlRightSave;
	
	private static final byte REPLAY_CONTROLS_STATE_ALL_HIDDEN			= 0;
	private static final byte REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN		= 1;
	private static final byte REPLAY_CONTROLS_STATE_APPEARING_PARTIAL	= 2;
	private static final byte REPLAY_CONTROLS_STATE_APPEARING_FULL		= 3;
	private static final byte REPLAY_CONTROLS_STATE_SHOWN				= 4;
	private static final byte REPLAY_CONTROLS_STATE_HIDING_FULL			= 5;
	private static final byte REPLAY_CONTROLS_STATE_HIDING_PARTIAL		= 6;
	
	/** Estado atual dos controels do replay. */
	private byte replayControlState = -1;
	
	/** Velocidade atual da anima�ao de entrada/sa�da dos controles do replay. */
	private final MUV replayControlSpeed = new MUV();
	
	/** �cone que indica o comando para exibir/esconder o replay. */
	private static Sprite replayHideIcon;
	
	private static final byte REPLAY_HIDE_ICON_FRAME_HIDE = 0;
	private static final byte REPLAY_HIDE_ICON_FRAME_SHOW = 1;
	
	/** �cone que indica a velocidade atual do replay. */
	private static Sprite replaySpeedIcon;
	
		//#if SCREEN_SIZE != "SMALL"
		
			//#if JAR == "full"
		/** Anima��o do rolo de filme girando. */
		private static Sprite replaySpeedAnim;
			//#else
//# 			/** Imagem do rolo de filme. */
//# 			private static DrawableImage replaySpeedAnim;
			//#endif
		//#endif
	
	//#endif
	
	/** Tela que mostra uma anima��o indicando o resultado de uma jogada. */
	private static PlayResultBox playResultBox;
	
	/** Tela que mostra o resultado parcial do jogo. */
	private final ScoreBox scoreBox;
	
	/** Tela que mostra a apresenta��o dos times e o resultado final. */
	private final InfoBox infoBox;
	
	private final Team team1;
	private final Team team2;
	
	private final Shooter[] shooters = new Shooter[ 2 ];
	private final Keeper[] keepers = new Keeper[ 2 ];
	
	private final int X_OFFSET;
	private final int Y_OFFSET;
	
	private final int difficultyLevel;
	
	/** Imagem do Cleston comemorando a vit�ria do campeonato. */
	private final DrawableGroup cleston;
	
	//#if SCREEN_SIZE != "SMALL"
	/** Imagem do olho do Cleston fechado, para piscar eventualmente para o jogador. */
	private final DrawableImage clestonEye;
	
	/** Tempo que o olho do Cleston permanece invis�vel. */
	private static final short CLESTON_EYE_INVISIBLE_TIME = 3000;
	
	/** Tempo que o olho do Cleston permanece vis�vel. */
	private static final short CLESTON_EYE_VISIBLE_TIME = 333;
	
	private int clestonEyeTime = CLESTON_EYE_VISIBLE_TIME;
	//#endif
	
	/** Velocidade de anima��o da imagem do Cleston com o trof�u na tela de fim de jogo. */
	private final MUV clestonSpeed;
	
	//<editor-fold desc="PAR�METROS DA C�MERA" defaultstate="collapsed">
	
	/** Tempo padr�o em segundos que a c�mera leva para chegar � posi��o de destino. */
	private static final int FP_CAMERA_MOVE_DEFAULT_TIME = NanoMath.divInt( 1200, 1000 );
	
	/** Posi��o y do topo da tela ao mostrar controles do jogador. */
	private final short CAMERA_CONTROL_Y;
	
	/** Posi��o y (topo) m�xima da c�mera. */
	private final short CAMERA_MAX_Y;
	
	/** Posi��o y da c�mera ao mostrar o controle de curva. */
	private final short CAMERA_CURVE_Y;
	
	/** Velocidade da c�mera. */
	private int fp_cameraSpeed;
	
	/** Posi��o atual da c�mera. */
	private int fp_cameraY;
	
	/** Tempo total em segundos at� a c�mera chegar � posi��o de destino. */
	private int fp_timeToDestination;
	
	/** Tempo atual da c�mera � acumulado para agilizar teste de chegada ao destino. */
	private int fp_cameraCurrentTime;
	
	// estados da c�mera
	private static final byte CAMERA_STATE_STOPPED	= 0;
	private static final byte CAMERA_STATE_MOVING	= 1;
	
	/** Estado atual da c�mera. */
	private byte cameraState;

	//</editor-fold>
	
	private UpdatableGroup fieldGroup;
	
	/** Mutex utilizado para evitar problemas de sincroniza��o entre onSequenceEnded e setState. */
	private final Mutex mutex = new Mutex();
	
	/** Indica se est� dentro da �rea cr�tica do m�todo setState (utilizado para evitar que o estado possa ser pulado 
	 * mesmo que o tempo m�nimo n�o tenha sido expirado).
	 */
	private boolean changingState;
	
	/** Grupo utilizado para indicar uma mensagem ao jogador (carregando, passe para o jogador 1, passe para o jogador 2). */
	private final DrawableGroup messageGroup;
	
	/** Label utilizado para indicar uma mensagem ao jogador. */
	private final RichLabel messageLabel;
	
	
	//#if JAR != "min"
	/**
	 * Aloca uma nova tela de jogo para exibir um replay.
	 * 
	 * @param replay
	 * @throws java.lang.Exception
	 */
	public Match( Replay replay ) throws Exception {
		this( MODE_REPLAY, new Team( replay.shooterIndex ), new Team( replay.keeperIndex )  );
		
		// recupera a jogada armazenada no replay
		lastKeeperPlay.set( replay.keeperPlay );
		lastShooterPlay.set( replay.shooterPlay );
		control.getCurrentPlay().set( lastShooterPlay );
		ball.loadReplayData( replay );
		currentKeeper.setLastGiveUpBehaviour( replay.keeperGaveUp );
	}
	//#endif
	
	
	public Match( byte mode, Team team1, Team team2 ) throws Exception {
		this( mode, 0, 0, 0, team1, team2 );
	}
	
	
	public Match( byte mode, int division, int difficultyLevel, int stage, Team team1, Team team2 ) throws Exception {
		super( TOTAL_ITEMS );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		this.mode = mode;
		
		fieldGroup = new UpdatableGroup( FIELD_GROUP_TOTAL_ITEMS );
		fieldGroup.setSize( ScreenManager.SCREEN_WIDTH, NanoMath.max( ScreenManager.SCREEN_HEIGHT, FIELD_MIN_HEIGHT ) );
		insertDrawable( fieldGroup );
		
		X_OFFSET = ( ScreenManager.SCREEN_WIDTH - SCREEN_DEFAULT_WIDTH ) >> 1;
		Y_OFFSET = ( fieldGroup.getHeight() - SCREEN_DEFAULT_HEIGHT ) >> 1;
		
		// insere o pattern do c�u (posicionamento � feito em rela��o � torcida)
		fieldGroup.insertDrawable( sky );
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		//#if JAR != "min"
		// insere o pattern da torcida
		crowd.setSize( size.x, crowd.getFill().getHeight() );
		fieldGroup.insertDrawable( crowd );
		//#endif
		
		//#if SCREEN_SIZE == "SMALL"
//# 		
//# 		fieldLeft.setPosition( ( size.x >> 1 ) - fieldLeft.getWidth(), FIELD_Y + Y_OFFSET );
//# 		fieldGroup.insertDrawable( fieldLeft );
//# 		
//# 		final DrawableImage fieldRight = new DrawableImage( fieldLeft );
//# 		fieldRight.setTransform( TRANS_MIRROR_H );
//# 		fieldRight.setPosition( size.x >> 1, fieldLeft.getPosY() );
//# 		fieldGroup.insertDrawable( fieldRight );
//# 		
		//#if JAR != "min"
//# 		// define a posi��o da torcida
//# 		crowd.setPosition( 0, fieldLeft.getPosY() - crowd.getHeight() + MATCH_CROWD_Y_OFFSET );		
		//#endif
//# 		
		//#else

		// insere as imagens centrais do gramado (j� foram alocadas)
		fieldCenterTopLeft.setPosition( X_OFFSET, FIELD_Y + Y_OFFSET );
		fieldGroup.insertDrawable( fieldCenterTopLeft );
		
		fieldCenterTopRight.setPosition( fieldCenterTopLeft.getPosX() + fieldCenterTopLeft.getWidth(), fieldCenterTopLeft.getPosY() );
		fieldGroup.insertDrawable( fieldCenterTopRight );
		
		fieldCenterBottom.setPosition( X_OFFSET, fieldCenterTopLeft.getPosY() + fieldCenterTopLeft.getHeight() );
		fieldGroup.insertDrawable( fieldCenterBottom );
		
		// se a tela for maior que a largura padr�o do campo, insere tamb�m imagens do gramado nas laterais
		if ( X_OFFSET > 0 ) {
			final DrawableImage imgFieldRight = new DrawableImage( PATH_IMAGES + "field_r.png" );
			imgFieldRight.setPosition( fieldCenterTopRight.getPosX() + fieldCenterTopRight.getWidth(), fieldCenterTopRight.getPosY() );
			fieldGroup.insertDrawable( imgFieldRight );
			
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
			
			if ( GameMIDlet.isLowMemory() ) {
				// espelha para a parte esquerda
				final DrawableImage imgFieldLeft = new DrawableImage( imgFieldRight );
				imgFieldLeft.setTransform( TRANS_MIRROR_H );
				imgFieldLeft.setPosition( fieldCenterTopLeft.getPosX() - imgFieldLeft.getWidth(), fieldCenterTopRight.getPosY() );
				fieldGroup.insertDrawable( imgFieldLeft );
			}  
			//#if JAR == "full"
			else {
				// utiliza imagem pr�pria na esquerda
				final DrawableImage imgFieldLeft = new DrawableImage( PATH_IMAGES + "field_l.png" );
				imgFieldLeft.setPosition( fieldCenterTopLeft.getPosX() - imgFieldLeft.getWidth(), fieldCenterTopRight.getPosY() );
				fieldGroup.insertDrawable( imgFieldLeft );
			}
			//#endif
			
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
			
			// define a posi��o da torcida
			crowd.setPosition( 0, fieldCenterTopLeft.getPosY() - crowd.getHeight() + MATCH_CROWD_Y_OFFSET_BIG );
		} else {
			// define a posi��o da torcida
			crowd.setPosition( 0, fieldCenterTopLeft.getPosY() - crowd.getHeight() + MATCH_CROWD_Y_OFFSET_DEFAULT );
		}
		
		//#endif
		
		//#if JAR == "min"
//# 		// posiciona o c�u
//# 		sky.setSize( size.x, fieldLeft.getPosY() + SKY_PATTERN_CROWD_Y_OFFSET );
		//#else
		// adiciona os patroc�nios
		final short CROWD_WIDTH = ( short ) crowd.getFill().getWidth();
		final byte TOTAL_ADS = ( byte ) ( ( size.x + CROWD_WIDTH - 1 ) / CROWD_WIDTH );
		for ( byte i = 0 ; i < TOTAL_ADS; ++i ) {
			final DrawableImage ad = new DrawableImage( ads[ i & 1 ] );
			ad.setPosition( ( CROWD_WIDTH * i ) + ADVERTISING_OFFSET, crowd.getPosY() + ADVERTISING_OFFSET );
			
			fieldGroup.insertDrawable( ad );
		}
		
		// posiciona o c�u
		sky.setSize( size.x, crowd.getPosY() + SKY_PATTERN_CROWD_Y_OFFSET );
		
		//#endif
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		// se a altura da tela permitir, insere a cor s�lida do gramado abaixo das imagens do gramado
		//#if SCREEN_SIZE == "SMALL"
//# 		final int PATTERN_Y = fieldLeft.getPosY() + fieldLeft.getHeight();
		//#else
		final int PATTERN_Y = fieldCenterBottom.getPosY() + fieldCenterBottom.getHeight();
		//#endif
		if ( PATTERN_Y < fieldGroup.getHeight() ) {
			final Pattern patternGrass = new Pattern( GRASS_COLOR_LIGHT );
			patternGrass.setPosition( 0, PATTERN_Y );
			patternGrass.setSize( ScreenManager.SCREEN_WIDTH, fieldGroup.getHeight() - PATTERN_Y );
			fieldGroup.insertDrawable( patternGrass );
		}
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		// insere a marca do p�nalti
		final DrawableImage mark = new DrawableImage( PATH_IMAGES + "mark.png" );
		mark.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		mark.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, Y_OFFSET + BACK_LINE_Y + PENALTY_MARK_TO_GOAL );
		fieldGroup.insertDrawable( mark );
		
		CAMERA_MAX_Y = ( short ) NanoMath.min( 0, size.y - fieldGroup.getHeight() );
		CAMERA_CURVE_Y = ( short ) NanoMath.max( CAMERA_MAX_Y, -mark.getRefPixelY() );
		
		// posiciona o gol (j� foi alocado; inser��o � feita depois)
		net.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, Y_OFFSET + BACK_LINE_Y );
		goal.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, Y_OFFSET + BACK_LINE_Y );
		
		//#if JAR != "min"
		// insere a anima��o da rede do gol (j� alocada)
		fieldGroup.insertDrawable( netAnimation );
		netAnimationViewport = new Rectangle( goal.getPosX() + NET_OFFSET, goal.getPosY() + NET_OFFSET, goal.getWidth() - ( NET_OFFSET << 1 ), NET_HEIGHT );
		netAnimation.setViewport( netAnimationViewport );
		netAnimation.setVisible( false );
		//#endif
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		fieldGroup.insertDrawable( net );
		fieldGroup.insertDrawable( goal );

		// insere a bola do jogo
		ball = new Ball( this, mark.getRefPixelY(), fieldGroup.getHeight() );
		fieldGroup.insertDrawable( ball );
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		// insere os controles do jogador
		control = new Control( goal, mark.getRefPixelY(), fieldGroup.getHeight() );
		fieldGroup.insertDrawable( control );
		
		this.team1 = team1;
		this.team2 = team2;
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		this.difficultyLevel = difficultyLevel;
		
		//#if JAR != "min"
		if ( mode == MODE_CHAMPION ) {
			cleston = new DrawableGroup( 3 );
			final DrawableImage imgCleston = new DrawableImage( PATH_SPLASH + "cleston.png" );
			
			//#if SCREEN_SIZE == "MEDIUM" || JAR == "full"
			final DrawableImage imgTrophy = new DrawableImage( PATH_IMAGES + "trophy.png" );
			imgTrophy.defineReferencePixel( ANCHOR_BOTTOM );
			imgTrophy.setRefPixelPosition( TROPHY_OFFSET_X, imgCleston.getHeight() );
			//#endif
			
			cleston.insertDrawable( imgCleston );
			
			//#if SCREEN_SIZE != "SMALL"
			clestonEye = new DrawableImage( PATH_IMAGES + "eye.png" );
			clestonEye.setPosition( CLESTON_EYE_X, CLESTON_EYE_Y );
			clestonEye.setVisible( false );
			cleston.insertDrawable( clestonEye );
			//#endif
			
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
			
			
			//#if SCREEN_SIZE == "MEDIUM" || JAR == "full"
			cleston.insertDrawable( imgTrophy );
			cleston.setSize( TROPHY_OFFSET_X + imgTrophy.getWidth(), imgCleston.getHeight() );
			//#else
//# 			cleston.setSize( imgCleston.getSize() );
			//#endif
			
			cleston.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			
			clestonSpeed = new MUV( -cleston.getHeight() >> 2 );
			
			scoreBox = null;
			infoBox = null;
		} else {
		//#endif
			Player.setMatch( this );
			Ball.setMatch( this );

			// n�o � necess�rio alocar o goleiro do primeiro time no modo de visualiza��o de replay. No caso de aparelhos com
			// pouca mem�ria, somente o goleiro atual est� alocado a cada instante.
			//#if JAR == "full"
			if ( !GameMIDlet.isLowMemory() && mode != MODE_REPLAY ) {
				keepers[ 0 ] = team1.loadKeeper( goal );
				fieldGroup.insertDrawable( keepers[ 0 ] );
			}
			//#endif
			keepers[ 1 ] = team2.loadKeeper( goal );
			currentKeeper = keepers[ 1 ];
			
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
			
			switch ( mode ) {
				case MODE_CHAMPIONSHIP_MATCH_AWAY:
					if ( keepers[ 0 ] != null )
						keepers[ 0 ].setDifficultyLevel( difficultyLevel );
					break;
					
				case MODE_CHAMPIONSHIP_MATCH_HOME:
					if ( keepers[ 1 ] != null )
						keepers[ 1 ].setDifficultyLevel( difficultyLevel );
				break;
			}
			fieldGroup.insertDrawable( keepers[ 1 ] );		

			shooters[ 0 ] = team1.loadShooter();
			shooters[ 0 ].setPosition( X_OFFSET, Y_OFFSET );
			currentShooter = shooters[ 0 ];
			fieldGroup.insertDrawable( shooters[ 0 ] );
			
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

			// n�o � necess�rio alocar o batedor do segundo time no modo de visualiza��o de replay. No caso de aparelhos com
			// pouca mem�ria, somente o batedor atual est� alocado a cada instante.
			//#if JAR == "full"
			if ( !GameMIDlet.isLowMemory() && mode != MODE_REPLAY ) {
				shooters[ 1 ] = team2.loadShooter();
				shooters[ 1 ].setPosition( X_OFFSET, Y_OFFSET );
				fieldGroup.insertDrawable( shooters[ 1 ] );
			}
			//#endif
			
			for ( byte i = 0; i < 2; ++i ) {
				if ( shooters[ i ] != null ) {
					shooters[ i ].setVisible( false );
					shooters[ i ].setSequence( Shooter.SEQUENCE_STOPPED );
				}

				if ( keepers[ i ] != null ) {
					keepers[ i ].setVisible( false );
					keepers[ i ].setSequence( Keeper.SEQUENCE_STOPPED );
				}
			}			

			//#if JAR == "min"
//# 			// utiliza label para indicar que est� no replay
//# 			replayLabel = new Label( GameMIDlet.getFont( FONT_INDEX_TEXT ), "REPLAY 1X" );
//# 			replayLabel.setVisible( false );
//# 			insertDrawable( replayLabel );
			//#else
			// insere o �cone e imagem do controle do replay
			//#if SCREEN_SIZE == "SMALL"
//# 			replayIcon.setPosition( ScreenManager.SCREEN_WIDTH - ( replayIcon.getWidth() * 11 / 10 ), replayIcon.getHeight() );
			//#else
			replayIcon.setPosition( ScreenManager.SCREEN_WIDTH - ( ( replayIcon.getWidth() * 3 ) >> 1 ), replayIcon.getHeight() );
				clestonEye = null;
			//#endif
			replayIcon.setListener( this, REPLAY_ICON_SPRITE_ID );
			setReplayIconSequence( REPLAY_ICON_SEQUENCE_NONE );
			insertDrawable( replayIcon );
			//#endif
			
			cleston = null;
			clestonSpeed = null;
			
			// insere a caixa de texto que mostra o resultado parcial
			scoreBox = new ScoreBox( team1, team2 );
			scoreBox.setState( ScoreBox.STATE_HIDDEN );
			insertDrawable( scoreBox );	
			
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
			
			// insere a caixa de texto que mostra a tela de apresenta��o e de resultado final
			infoBox = new InfoBox( ( byte ) division, ( byte ) stage, team1, team2, score );
			infoBox.setState( InfoBox.STATE_HIDDEN );
			insertDrawable( infoBox );		
			
		// fecha as chaves
		//#if JAR != "min"
		}
		//#endif
		
		// insere a caixa de texto que mostra o resultado das jogadas e a anima��o dos bonecos
		playResultBox.setState( PlayResultBox.STATE_HIDDEN );
		insertDrawable( playResultBox );
		
		// define a posi��o da c�mera durante a exibi��o dos controles e da jogada
		CAMERA_CONTROL_Y = ( short ) NanoMath.max( CAMERA_MAX_Y, -control.getDirectionIconPosY() + playResultBox.PARTIAL_HEIGHT + 10 );		
		
		//#if JAR == "min"
//# 		replayLabel.setPosition( size.x - replayLabel.getWidth(), playResultBox.PARTIAL_HEIGHT + 6 );
		//#else
		setReplayControlsState( REPLAY_CONTROLS_STATE_ALL_HIDDEN );
		replayControls.setVisible( false );
		
		insertDrawable( replayControls );
		//#endif
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		// cria e insere o grupo que mostra mensagens ao jogador
		messageGroup = new DrawableGroup( 2 );
		messageGroup.setSize( size );
		messageGroup.setVisible( false );
		
		messageLabel = new RichLabel( GameMIDlet.getFont( FONT_INDEX_DEFAULT ), null, size.x * 9 / 10, null );
		
		final Pattern messageBkg = new Pattern( BLACKBOARD_COLOR );
		messageBkg.setSize( size );
		
		messageGroup.insertDrawable( messageBkg );
		messageGroup.insertDrawable( messageLabel );
		
		insertDrawable(  messageGroup );
		
		switch ( mode ) {
			case MODE_TRAINING:
				setState( STATE_BEGIN_TURN );
				//#if JAR != "min"
				replayControlRightLoad.setVisible( false );
				//#endif
			break;
			
			//#if JAR != "min"
			case MODE_REPLAY:
				setState( STATE_REPLAY_ONLY );
				replayControlRightLoad.setVisible( true );
			break;
			
			case MODE_CHAMPION:
				insertDrawable( cleston );
				insertDrawable( new Fireworks() );
				setState( STATE_CHAMPION_APPEARS );
			break;
			//#endif
			
			default:
				setState( STATE_PRESENTATION );
				
				//#if JAR != "min"
				replayControlRightLoad.setVisible( false );
				//#endif
		}
		//#if JAR != "min"
		replayControlRightSave.setVisible( !replayControlRightLoad.isVisible() );
		//#endif
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				switch ( state ) {
					case STATE_WAITING_PLAYER_1:
					case STATE_WAITING_PLAYER_2:
						// esperando que jogador defina jogada
						if ( control.setControl() ) {
							timerExpired();
						} else {
							if ( control.getCurrentControl() == Control.CONTROL_CURVE )
								moveCameraTo( CAMERA_CURVE_Y, FP_CAMERA_MOVE_DEFAULT_TIME );
						}
					break; // fim case STATE_WAITING


					//#if JAR != "min"
					case STATE_REPLAY_ONLY_WAIT:
						GameMIDlet.setScreen( SCREEN_LOAD_REPLAY );
					break;
					//#endif
					
					case STATE_PLAY_RESULT:   
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:
					case STATE_PARTIAL_RESULT:
						checkKeyPressedTimerExpired();
					break;

					case STATE_FINAL_RESULT:
						scoreBox.setState( GenericInfoBox.STATE_HIDING );

						setState( STATE_ENDED );
					break;

					//#if JAR != "min"
					case STATE_REPLAY_ONLY:
					//#endif
					case STATE_PRESENTATION:
					case STATE_REPLAY:
					case STATE_REPLAY_WAIT:
					case STATE_ENDED:
					case STATE_CHAMPION_APPEARS:
					case STATE_CHAMPION_SHOWN:
						timerExpired();
					break;
				} // fim switch ( state )
			break;

			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
				switch ( state ) {
					case STATE_CREATED:
					case STATE_PRESENTATION:
					break;
					
					//#if JAR != "min"
					case STATE_REPLAY_ONLY:
					case STATE_REPLAY_ONLY_WAIT:
						GameMIDlet.setScreen( SCREEN_LOAD_REPLAY );
					break;
					//#endif
					
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:	
						checkKeyPressedTimerExpired();
					break;
					
					case STATE_CHAMPION_APPEARS:
					case STATE_CHAMPION_SHOWN:
						timerExpired();
					break;

					default:
						GameMIDlet.setScreen( SCREEN_PAUSE );
				}
			break; // fim case ScreenManager.KEY_CLEAR

			//#if JAR != "min"
			case ScreenManager.KEY_NUM6:
				switch ( state ) {
					case STATE_REPLAY:
					case STATE_REPLAY_WAIT:
						GameMIDlet.setScreen( SCREEN_SAVE_REPLAY );
					break;
					
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:
						checkKeyPressedTimerExpired();
					break;
				} // fim switch ( state )
			break;
			
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_POUND:
			case ScreenManager.KEY_STAR:
			case ScreenManager.KEY_NUM0:
			case ScreenManager.KEY_NUM7:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM9:
			case ScreenManager.UP:
			case ScreenManager.DOWN:
				switch ( state ) {
					case STATE_REPLAY:
					case STATE_REPLAY_ONLY:
					case STATE_REPLAY_WAIT:
					case STATE_REPLAY_ONLY_WAIT:
						switch ( replayControlState ) {
							case REPLAY_CONTROLS_STATE_APPEARING_PARTIAL:
							case REPLAY_CONTROLS_STATE_APPEARING_FULL:
							case REPLAY_CONTROLS_STATE_SHOWN:
								setReplayControlsState( REPLAY_CONTROLS_STATE_HIDING_PARTIAL );
							break;
							
							case REPLAY_CONTROLS_STATE_ALL_HIDDEN:
							case REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN:
							case REPLAY_CONTROLS_STATE_HIDING_FULL:
							case REPLAY_CONTROLS_STATE_HIDING_PARTIAL:
								setReplayControlsState( REPLAY_CONTROLS_STATE_APPEARING_FULL );
							break;
						}
					break;
					
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:
						checkKeyPressedTimerExpired();
					break;
				}
			break;
			//#endif

			default:
				switch ( state ) {
					//#if JAR != "min"
					case STATE_REPLAY_ONLY:
					case STATE_REPLAY_ONLY_WAIT:
					//#endif
						
					case STATE_REPLAY_WAIT:
					case STATE_REPLAY:
						switch ( key ) {
							case ScreenManager.LEFT:
							case ScreenManager.KEY_NUM1:
								replayPaused = false;
								setReplaySpeed( currentReplaySpeed + 1 );
							break;
							
							case ScreenManager.KEY_NUM2:
								replayPaused = !replayPaused;
								setReplaySpeed( currentReplaySpeed );
							break;
							
							case ScreenManager.RIGHT:
							case ScreenManager.KEY_NUM3:
								replayPaused = false;
								setReplaySpeed( currentReplaySpeed - 1 );
							break;
							
							case ScreenManager.KEY_NUM4:
								// reinicia o replay
								setState( ( byte ) ( state - ( state & 1 ) ) );
							break;
							
							default:
								setState( ( byte ) ( ( state & 1 ) == 0 ? state + 1 : state ) );
						} // fim switch ( wParam )
					break;
					
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:		
						checkKeyPressedTimerExpired();
					break;
					
					case STATE_CHAMPION_APPEARS:
					case STATE_CHAMPION_SHOWN:
						timerExpired();
					break;
				} // fim switch ( state )
		} // fim switch ( key )		
	}


	public final void keyReleased( int key ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	/**
	 * Atualiza a partida ap�s "time" milisegundos.
	 */
	public final void update( int delta ) {
		int cameraDelta;
		switch ( state ) {
			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
			case STATE_REPLAY_ONLY_WAIT:
			//#endif
			case STATE_MOVING:
			case STATE_PLAY_RESULT:
			case STATE_REPLAY:
			case STATE_PARTIAL_RESULT:
			case STATE_FINAL_RESULT:
			case STATE_REPLAY_WAIT:
				if ( replayPaused )
					lastDelta = 0;
				else
					lastDelta = delta >> currentReplaySpeed;
				
				cameraDelta = lastDelta;
			break;
			
			default:
				lastDelta = 0;
				
				cameraDelta = delta;
		}
		
		super.update( delta );
		
		// reposiciona a c�mera na tela
		switch ( cameraState ) {
			case CAMERA_STATE_MOVING:
				final int fp_delta = NanoMath.divInt( cameraDelta, 1000 );
				fp_cameraCurrentTime += fp_delta;
				
				if ( fp_cameraCurrentTime >= fp_timeToDestination ) {
					fp_cameraCurrentTime = fp_timeToDestination;
					cameraState = CAMERA_STATE_STOPPED;
				}
				
				fp_cameraY += NanoMath.mulFixed( fp_cameraSpeed, fp_delta );
				
				fieldGroup.setPosition( 0, NanoMath.toInt( fp_cameraY ) );
			break;
		}		
		
		if ( timeToNextTransition > 0 ) {
			timeToNextTransition -= delta;
			if ( timeToNextTransition <= 0 ) {
				timerExpired();
			}
		}

		switch ( state ) {
			case STATE_MOVING:		   
				// jogada em andamento
				switch ( ball.getState() ) {
					case Ball.BALL_STATE_GOAL:
					case Ball.BALL_STATE_POST_GOAL:
						// gol - atualiza o placar
						final int index = ( currentTurn & 1 );
						++score[ index ];
						if ( score[ index ] > 999 )
							score[ index ] = 999;
					break;

					case Ball.BALL_STATE_OUT:
					case Ball.BALL_STATE_POST_OUT:
					case Ball.BALL_STATE_POST_BACK:
					case Ball.BALL_STATE_REJECTED_KEEPER:
					break;

					default:
						// jogada ainda est� em andamento
						return;
				} // fim switch ( ball.getState() )

				// jogada foi encerrada
				timerExpired();
			break;

			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
			//#endif
			case STATE_REPLAY:
 			case STATE_REPLAY_WAIT:
			//#if JAR == "min"
//# 				replayAccTime -= delta;
//# 				if ( replayAccTime < 0 ) {
//# 					replayAccTime = REPLAY_BLINK_TIME;
//# 					replayLabel.setVisible( !replayLabel.isVisible() );
//# 				}
			//#else
			case STATE_REPLAY_ONLY_WAIT:
			case STATE_PLAY_RESULT:
				switch ( ball.getState() ) {
					case Ball.BALL_STATE_GOAL:
					case Ball.BALL_STATE_POST_GOAL:
						netAnimationViewport.y = goal.getPosY() + NET_OFFSET + fieldGroup.getPosY();
						netAnimation.setRefPixelPosition( ball.getSpriteRefPixelX(), ball.getSpriteRefPixelY() );
						netAnimation.setVisible( true );
					break;
				}
			//#endif
			break;
			
			case STATE_CHAMPION_APPEARS:
				cleston.move( 0, clestonSpeed.updateInt( delta ) );
				
				if ( cleston.getRefPixelY() <= size.y )
					timerExpired();
			break;
			
			//#if SCREEN_SIZE != "SMALL"
			case STATE_CHAMPION_SHOWN:
				clestonEyeTime -= delta;
				if ( clestonEyeTime <= 0 ) {
					clestonEye.setVisible( !clestonEye.isVisible() );
					if ( clestonEye.isVisible() )
						clestonEyeTime = CLESTON_EYE_VISIBLE_TIME;
					else
						clestonEyeTime = CLESTON_EYE_INVISIBLE_TIME;
				}
			break;
			//#endif
			
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				// reduz o tempo para que seja verificado o tempo m�nimo para passar de estado (troca somente ao pressionar tecla)
				if ( timeToNextTransition < ( TIME_PLAYER_TURN_MESSAGE >> 2 ) )
					timeToNextTransition = TIME_PLAYER_TURN_MESSAGE >> 2;
			break; 
		} // fim switch( state )
		
		//#if JAR != "min"
		switch ( replayControlState ) {
			case REPLAY_CONTROLS_STATE_APPEARING_FULL:
			case REPLAY_CONTROLS_STATE_APPEARING_PARTIAL:
				replayControls.move( 0, replayControlSpeed.updateInt( delta ) );
				
				//#if SCREEN_SIZE == "SMALL"
//# 				final int Y_LIMIT = size.y - ( replayControlState == REPLAY_CONTROLS_STATE_APPEARING_FULL ? replayControls.getHeight() : replayHideIcon.getHeight() );
				//#else
				final int Y_LIMIT = size.y - ( replayControlState == REPLAY_CONTROLS_STATE_APPEARING_FULL ? replayControls.getHeight() : replaySpeedAnim.getPosY() + replaySpeedAnim.getHeight() );
				//#endif
				
				if ( replayControls.getPosY() <= Y_LIMIT )
					setReplayControlsState( replayControlState == REPLAY_CONTROLS_STATE_APPEARING_FULL ? REPLAY_CONTROLS_STATE_SHOWN : REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN );
			break;
			
			case REPLAY_CONTROLS_STATE_HIDING_PARTIAL:
			case REPLAY_CONTROLS_STATE_HIDING_FULL:
				replayControls.move( 0, replayControlSpeed.updateInt( delta ) );
				
				//#if SCREEN_SIZE == "SMALL"
//# 				final int LIMIT_Y = size.y - ( replayControlState == REPLAY_CONTROLS_STATE_HIDING_PARTIAL ? replayHideIcon.getHeight() : 0 );
				//#else
				final int LIMIT_Y = size.y - ( replayControlState == REPLAY_CONTROLS_STATE_HIDING_PARTIAL ? 
												replaySpeedAnim.getPosY() + replaySpeedAnim.getHeight() : 0 );
				//#endif
				
				if ( replayControls.getPosY() >= LIMIT_Y )
					setReplayControlsState( replayControlState == REPLAY_CONTROLS_STATE_HIDING_PARTIAL ? REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN : REPLAY_CONTROLS_STATE_ALL_HIDDEN );
			break;
		}
		//#endif
		
		// atualiza a ordem de desenho dos drawables
		updateZOrder();		
	} // fim do m�todo update( int )


	/**
	 * Define os softkeys da tela de jogo, de acordo com o tipo de partida e estado atual.
	 */
	public final void setSoftKeyLabel() {
		switch ( state ) {
			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
			case STATE_REPLAY_ONLY_WAIT:
			//#endif
				
			case STATE_REPLAY:
			case STATE_REPLAY_WAIT:
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
			break;

			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;
			
			case STATE_CHAMPION_APPEARS:
			case STATE_CHAMPION_SHOWN:
			case STATE_ENDED:
			case STATE_FINAL_RESULT:
			case STATE_LOAD_PLAYERS_MSG:
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;
			
			default:
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_PAUSE );
		}
	}


	public final void unloadMatch() {
		Player.setMatch( null );
		Ball.setMatch( null );
		
		currentKeeper = null;
		currentShooter = null;
		
		for ( byte i = 0; i < 2; ++i ) {
			shooters[ i ] = null;
			keepers[ i ] = null;
		}
		ball.setKeeper( null );
		
		if ( fieldGroup != null ) {
			while ( fieldGroup.getUsedSlots() > 0 )
				fieldGroup.removeDrawable( 0 );
			
			fieldGroup = null;
		}

		while ( activeDrawables > 0 )
			removeDrawable( 0 );
		
		System.gc();
	}

	
	/**
	 * M�todo chamado quando um timer expira (utilizado para emular o comportamento dos timers em BREW).
	 */
	private final void timerExpired() {
		switch ( state ) {
			// obt�m o estado atual da partida (o estado que ser� encerrado)
			case STATE_PRESENTATION:
				scoreBox.setState( GenericInfoBox.STATE_HIDING );
				infoBox.setState( GenericInfoBox.STATE_HIDING );
				
				if ( GameMIDlet.isLowMemory() )
					setState( STATE_LOAD_PLAYERS_MSG  );
				else
					setState( STATE_BEGIN_TURN );
			break;
			
			case STATE_LOAD_PLAYERS_MSG:
				// desaloca o goleiro antigo
				for ( byte i = 0; i < fieldGroup.getUsedSlots(); ++i ) {
					if ( fieldGroup.getDrawable( i ) == currentKeeper ) {
						fieldGroup.removeDrawable( i );
						break;
					}
				}
				ball.setKeeper( null );

				currentKeeper = null;

				// desaloca o batedor antigo
				for ( byte i = 0; i < fieldGroup.getUsedSlots(); ++i ) {
					if ( fieldGroup.getDrawable( i ) == currentShooter ) {
						fieldGroup.removeDrawable( i );
						break;
					}
				}
				currentShooter = null;
				shooters[ 0 ] = null;
				shooters[ 1 ] = null;
				keepers[ 0 ] = null;
				keepers[ 1 ] = null;
				System.gc();					

				try {
					if ( ( currentTurn & 1 ) == 0 ) {
						currentShooter = team1.loadShooter();

						currentKeeper = team2.loadKeeper( goal );

						// define o n�vel de dificuldade do goleiro da IA
						if ( mode == MODE_CHAMPIONSHIP_MATCH_HOME )
							currentKeeper.setDifficultyLevel( difficultyLevel );
					} else {
						currentShooter = team2.loadShooter();
						currentKeeper = team1.loadKeeper( goal );
						// define o n�vel de dificuldade do goleiro da IA
						if ( mode == MODE_CHAMPIONSHIP_MATCH_AWAY )
							currentKeeper.setDifficultyLevel( difficultyLevel );
					}

					for ( int i = 0; i < fieldGroup.getUsedSlots(); ++i ) {
						if ( fieldGroup.getDrawable( i ) == goal ) {
							fieldGroup.insertDrawable( currentKeeper, i + 1 );
							fieldGroup.insertDrawable( currentShooter, i + 3 );
							break;
						}
					}
					currentShooter.setPosition( X_OFFSET, Y_OFFSET );
					setState( STATE_BEGIN_TURN );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif

					throw new RuntimeException();
				}			
			break;	

			case STATE_BEGIN_TURN:
				setState( STATE_WAITING_PLAYER_1 );
			break;

			case STATE_WAITING_PLAYER_2:
			case STATE_WAITING_PLAYER_1:
				// jogador fez sua jogada
				switch ( mode ) {
					case MODE_2_PLAYERS:
						switch ( state ) {
							case STATE_WAITING_PLAYER_1:
								lastShooterPlay.set( control.getCurrentPlay() );
								setState( ( byte ) ( STATE_PLAYER_2_TURN - ( currentTurn & 1 ) ) );
							break;
							
							case STATE_WAITING_PLAYER_2:
								lastKeeperPlay.set( control.getCurrentPlay() );
								setState( STATE_WHISTLE );
							break;
						}
					break;
					
					default:
						if ( isPlayerShooting() ) {
							lastShooterPlay.set( control.getCurrentPlay() );
							lastKeeperPlay.set( control.getRandomPlay() );
						} else {
							lastShooterPlay.set( control.getRandomPlay() );
							lastKeeperPlay.set( control.getCurrentPlay() );
						}
						
						setState( STATE_WHISTLE );
				}
			break;

			case STATE_WHISTLE:
				setState( STATE_MOVING );
			break;

			case STATE_MOVING:        
				// jogada em andamento
				setState( STATE_PLAY_RESULT );
			break;

			case STATE_PLAY_RESULT:
				playResultBox.setState( PlayResultBox.STATE_HIDING );
				
				setState( STATE_REPLAY );
			break;

			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
				setState( STATE_REPLAY_ONLY_WAIT );
			break;
			//#endif

			case STATE_REPLAY:
				setState( STATE_REPLAY_WAIT );
			break;

			case STATE_REPLAY_WAIT:
				++currentTurn;
				
				//#if DEBUG == "true"
				System.out.println( "PLACAR AP�S " + currentTurn + " CHUTE(S): " + score[ 0 ] + " X " + score[ 1 ] );
				//#endif				
				
				//#if I_AM_THE_PENALTY_KING == "true"
//# 					score[ 0 ] = 1;
//# 					score[ 1 ] = 0;
//# 					
//# 					setState( STATE_FINAL_RESULT );
				//#else
				switch ( mode ) {
					case MODE_TRAINING:
						currentShooter.setSequence( Shooter.SEQUENCE_STOPPED );
						
						if ( GameMIDlet.isLowMemory() )
							setState( STATE_LOAD_PLAYERS_MSG );
						else						
							setState( STATE_BEGIN_TURN );
					break;
					
					default:
						if ( hasEnded( getCurrentTurn(), score ) )
							setState( STATE_FINAL_RESULT );
						else
							setState( STATE_PARTIAL_RESULT );
					break;
						
				}
				//#endif
			break; // fim case STATE_REPLAY_WAIT

			case STATE_PARTIAL_RESULT:
				scoreBox.setState( GenericInfoBox.STATE_HIDING );
				
				if ( GameMIDlet.isLowMemory() )
					setState( STATE_LOAD_PLAYERS_MSG  );
				else
					setState( STATE_BEGIN_TURN );
			break;

			case STATE_ENDED:
			case STATE_FINAL_RESULT:
				GameMIDlet.matchEnded( this );
			break;
			
			case STATE_CHAMPION_APPEARS:
				setState( STATE_CHAMPION_SHOWN );
			break;
			
			case STATE_CHAMPION_SHOWN:
				GameMIDlet.setScreen( SCREEN_CHAMPIONSHIP );
			break;
			
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				final byte diff = ( byte ) ( state - STATE_PLAYER_1_TURN );
				
				if ( diff == ( currentTurn & 1 ) ) 
					setState( STATE_BEGIN_TURN );
				else
					setState( STATE_WAITING_PLAYER_2 );
					
				setSoftKeyLabel();
			break;
		} // fim switch( pMatch.state )
	} // fim do m�todo timerExpired()
	
	
	public final byte getMode() {
		return mode;
	}


	/**
	 * Simula o resultado da partida, armazenando o placar em score e retornando o �ndice do time vencedor.
	 * 
	 * @param score
	 * @return 
	 */
	public static final byte simulate( short[] score ) {
		score[ 0 ] = score[ 1 ] = 0;
		short turn = 0;

		do {
			if ( NanoMath.randInt( 100 ) > 19 ) {
				 // gol
				++( score[ turn & 1 ] );
			}
			++turn;
		} while ( !hasEnded( turn, score ) );

		return ( byte ) ( score[ 0 ] > score[ 1 ] ? 0 : 1 );
	} // fim do m�todo simulate()


	/**
	 * 
	 * @param state
	 */
	private final void setState( byte state ) {
		mutex.acquire();
		
		changingState = true;
		
		//#if DEBUG == "true"
		System.out.println( "Match.setState: " + this.state + " -> " + state );
		//#endif
		
		this.state = state;
		timeToNextTransition = 0;		
		timeMinToAccelerate = 0;
		
		fieldGroup.setVisible( true );
		messageGroup.setVisible( false );
		
		switch ( state ) {
			case STATE_PRESENTATION:
				infoBox.setType( InfoBox.TYPE_PRESENTATION );
				infoBox.setState( GenericInfoBox.STATE_APPEARING );
				playResultBox.setState( PlayResultBox.STATE_HIDDEN );

				timeToNextTransition = TIME_PRESENTATION;
				timeMinToAccelerate = TIME_PRESENTATION >> 1;
			break; 
			
			case STATE_LOAD_PLAYERS_MSG:
				if ( currentTurn == 0 ) {
					mutex.release();
					setState( STATE_BEGIN_TURN );
					return;
				} else {
					messageLabel.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_LOADING ) );
					messageLabel.setSize( messageLabel.getWidth(), messageLabel.getTextTotalHeight() );
					messageLabel.setPosition( ( size.x - messageLabel.getWidth() ) >> 1, ( size.y - messageLabel.getHeight() ) >> 1 );

					fieldGroup.setVisible( false );
					messageGroup.setVisible( true );
				}
				timeToNextTransition = 171;
			break;
			
			case STATE_BEGIN_TURN:
				//#if JAR == "min"
//# 				replayLabel.setVisible( false );
				//#else
				netAnimation.setVisible( false );
				
				setReplayIconSequence( REPLAY_ICON_SEQUENCE_NONE );
				
				setReplayControlsState( REPLAY_CONTROLS_STATE_HIDING_FULL );
				//#endif

				// jogador chuta caso seja uma rodada �mpar fora de casa, ou rodada par quando
				// joga em casa ou treino.

				// define a vez do jogador (chutar ou defender)
				if ( isPlayerShooting() ) {
					control.setTurn( Control.TURN_ATTACK );
					
					if ( mode == MODE_2_PLAYERS ) {
						playResultBox.setText( GameMIDlet.getText( TEXT_PLAYER_1 + ( currentTurn & 1 ) ) + GameMIDlet.getText( TEXT_ATTACK ) );
					} else {
						playResultBox.setText( TEXT_ATTACK );
					}
				} else {
					control.setTurn( Control.TURN_DEFEND );
					
					if ( mode == MODE_2_PLAYERS ) {
						playResultBox.setText( GameMIDlet.getText( TEXT_PLAYER_1 + ( currentTurn & 1 ) ) + GameMIDlet.getText( TEXT_DEFEND ) );
					} else {
						playResultBox.setText( TEXT_DEFEND );
					}
				}

				//#if JAR == "full"
				if ( !GameMIDlet.isLowMemory() ) {
					// define o batedor e goleiro
					for ( byte i = 0; i < 2; ++i ) {
						if ( shooters[ i ] != null ) {
							shooters[ i ].setVisible( false );
							shooters[ i ].setSequence( Shooter.SEQUENCE_STOPPED );
						}
						
						if ( keepers[ i ] != null ) {
							keepers[ i ].setVisible( false );
							keepers[ i ].setSequence( Keeper.SEQUENCE_STOPPED );
						}
					}

					if ( ( currentTurn & 1 ) == 0 ) {
						currentShooter = shooters[ 0 ];
						currentKeeper = keepers[ 1 ];
					} else {
						currentShooter = shooters[ 1 ];
						currentKeeper = keepers[ 0 ];
					}
				}
				//#endif
				currentShooter.setVisible( true );
				currentKeeper.setVisible( true );

				replayPaused = false;
				setReplaySpeed( REPLAY_SPEED_NORMAL );
				
				ball.reset( currentKeeper );
				control.reset();

				currentKeeper.reset();
				
				currentShooter.setListener( this, currentTurn & 1 );

				updateZOrder();
				
				setZOrder( currentKeeper, goal, 1 );
				setZOrder( control, currentKeeper, 1 );
				
				moveCameraTo( CAMERA_CONTROL_Y, FP_CAMERA_MOVE_DEFAULT_TIME );

				timeToNextTransition = TIME_PARTIAL_RESULT_ANIM;
			break; 

			case STATE_WAITING_PLAYER_2:
				control.setTurn( Control.TURN_DEFEND );
				control.reset();
				
				moveCameraTo( CAMERA_CONTROL_Y, FP_CAMERA_MOVE_DEFAULT_TIME );
				
				playResultBox.setText( GameMIDlet.getText( TEXT_PLAYER_2 - ( currentTurn & 1 ) ) + GameMIDlet.getText( TEXT_DEFEND ) );
			break;

			case STATE_WHISTLE:
				// toca o apito do juiz
				MediaPlayer.play( SOUND_INDEX_WHISTLE );
				
				playResultBox.setState( PlayResultBox.STATE_HIDING );

				ball.reset( currentKeeper );
				// esconde os controles
				control.setVisible( false ); 

				// agenda o in�cio da movimenta��o do jogador
				timeToNextTransition = TIME_WHISTLE;
				
				moveCameraTo( CAMERA_MAX_Y, FP_CAMERA_MOVE_DEFAULT_TIME  );
			break;
			
			case STATE_MOVING:        
				// jogada em andamento
				playResultBox.setState( PlayResultBox.STATE_HIDING );

				// jogador se movimenta para chutar a bola (goleiro s� se move ap�s o chute em si)
				currentShooter.setSequence( Shooter.SEQUENCE_SHOOTING );
			break;
			
			case STATE_PLAY_RESULT:   
				setReplaySpeed( REPLAY_SPEED_NORMAL );
				
				switch ( ball.getLastPlayResult() ) {
					case BALL_STATE_POST_GOAL:
					case BALL_STATE_GOAL:
						if ( isPlayerShooting() )
							MediaPlayer.play( SOUND_INDEX_PLAY_GOOD );
						else
							MediaPlayer.play( SOUND_INDEX_PLAY_BAD );
					break;
					
					default:
						if ( isPlayerShooting() )
							MediaPlayer.play( SOUND_INDEX_PLAY_BAD );
						else
							MediaPlayer.play( SOUND_INDEX_PLAY_GOOD );
				}
				
				
				// mostra se foi gol, trave, etc
				switch ( mode ) {
					case MODE_CHAMPIONSHIP_MATCH_AWAY:
					case MODE_CHAMPIONSHIP_MATCH_HOME:
					case MODE_2_PLAYERS:
						playResultBox.setPlayResult( ball.getLastPlayResult(), isPlayerShooting(), mode == MODE_2_PLAYERS );
						scoreBox.updateBoard( score, ball.getLastPlayResult(), currentTurn );
						timeToNextTransition = TIME_BOARD_ANIMATION;
					break;
					
					case MODE_TRAINING:
						scoreBox.updateBoard( score, ball.getLastPlayResult(), currentTurn );
					//#if JAR != "min"
					case MODE_REPLAY:
					//#endif
						timeToNextTransition = TIME_PLAY_RESULT_TRAINING;
					break;
				}

				timeMinToAccelerate = timeToNextTransition - 1500;
			break; // fim case STATE_PLAY_RESULT

			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
				// define o batedor e goleiro atuais
				currentShooter = shooters[ 0 ];
				currentKeeper = keepers[ 1 ];
				
				currentShooter.setVisible( true );
				currentKeeper.setVisible( true );

				currentShooter.setListener( this, 0 );
			//#endif
				
			case STATE_REPLAY:
				moveCameraTo( CAMERA_MAX_Y, 0 );

				//#if JAR == "min"
//# 				replayLabel.setVisible( true );
				//#else
				netAnimation.setVisible( false );
				setReplayIconSequence( REPLAY_ICON_SEQUENCE_APPEARING );
				
				if ( replayControlState == REPLAY_CONTROLS_STATE_ALL_HIDDEN ) {
					if ( size.y > SCREEN_REPLAY_CONTROLS_VISIBLE_MIN_HEIGHT || ScreenManager.getInstance().hasPointerEvents() )
						setReplayControlsState( REPLAY_CONTROLS_STATE_APPEARING_FULL );
					else
						setReplayControlsState( REPLAY_CONTROLS_STATE_APPEARING_PARTIAL );
				}
				//#endif

				playResultBox.setState( PlayResultBox.STATE_HIDING );

				// se o jogador tiver reiniciado o replay manualmente, n�o altera sua velocidade
				replayPaused = false;
				setReplaySpeed( currentReplaySpeed );

				ball.reset( currentKeeper );
				currentKeeper.reset();
				currentShooter.setSequence( Shooter.SEQUENCE_SHOOTING );
				
				timeToNextTransition = 0;
				
				updateZOrder();
			break; 
			
			//#if JAR == "min"
//# 			case STATE_REPLAY_WAIT:
//# 				playResultBox.setText( TEXT_REPLAY_CONTROLS );				
//# 			break;
			//#else
			case STATE_REPLAY_ONLY_WAIT:
			case STATE_REPLAY_WAIT:
				if ( state == STATE_REPLAY_ONLY_WAIT )
					playResultBox.setText( TEXT_REPLAY_CONTROLS );
				else
					playResultBox.setText( TEXT_REPLAY_CONTROLS_SAVE );				
			break;
			//#endif
			
			case STATE_PARTIAL_RESULT:
				//#if JAR == "min"
//# 				replayLabel.setVisible( false );
				//#else
				setReplayControlsState( REPLAY_CONTROLS_STATE_HIDING_FULL );

				setReplayIconSequence( REPLAY_ICON_SEQUENCE_HIDING );
				//#endif
				scoreBox.setState( GenericInfoBox.STATE_APPEARING );
				playResultBox.setState( GenericInfoBox.STATE_HIDING );

				timeToNextTransition = TIME_BOARD_ANIMATION;
				timeMinToAccelerate = TIME_BOARD_ANIMATION - 1500;
			break; // fim case STATE_PARTIAL_RESULT
			
			case STATE_ENDED:
				infoBox.setState( GenericInfoBox.STATE_HIDING );
				timeToNextTransition = GenericInfoBox.TRANSITION_TIME + 50;
				timeMinToAccelerate = timeToNextTransition;
			break;
			
			case STATE_FINAL_RESULT:
				//#if JAR == "min"
//# 				replayLabel.setVisible( false );
				//#else
				setReplayControlsState( REPLAY_CONTROLS_STATE_HIDING_FULL );
				setReplayIconSequence( REPLAY_ICON_SEQUENCE_HIDING );
				//#endif
				
				infoBox.setType( InfoBox.TYPE_FINAL_RESULT );
				infoBox.setState( GenericInfoBox.STATE_APPEARING );
				playResultBox.setState( GenericInfoBox.STATE_HIDING );
				
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break; // fim case STATE_FINAL_RESULT
			
			case STATE_CHAMPION_APPEARS:
				MediaPlayer.play( SOUND_INDEX_CHAMPION );
				cleston.setRefPixelPosition( size.x >> 1, size.y + cleston.getHeight() );
			break;
			
			case STATE_CHAMPION_SHOWN:
				cleston.setRefPixelPosition( cleston.getRefPixelX(), size.y );
				playResultBox.setPlayResult( PlayResultBox.PLAY_RESULT_ANIMATION_ENDING, false, false );
			break;
			
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				messageLabel.setText( TEXT_PASS_TO_PLAYER_1 + ( state - STATE_PLAYER_1_TURN ) );
				messageLabel.setSize( messageLabel.getWidth(), messageLabel.getTextTotalHeight() );
				messageLabel.setPosition( ( size.x - messageLabel.getWidth() ) >> 1, ( size.y - messageLabel.getHeight() ) >> 1 );
				
				fieldGroup.setVisible( false );
				messageGroup.setVisible( true );
				
				timeToNextTransition = TIME_PLAYER_TURN_MESSAGE;
				timeMinToAccelerate = timeToNextTransition >> 1;
			break;
		} // fim switch ( state )
		
		setSoftKeyLabel();
		
		changingState = false;
		
		mutex.release();
	} // fim do m�todo setState()	


	/**
	 * Verifica se o jogo acabou, ou seja, se a diferen�a de gols � maior do que o n�mero de p�naltis restantes.
	 */
	private static final boolean hasEnded( short currentTurn, short[] score ) {
		//#if I_AM_THE_PENALTY_KING == "true"
//# 			return true;
		//#else
		// diferen�a (absoluta) de placar
		int diff = Math.abs( score[ 0 ] - score[ 1 ] );
		if ( currentTurn < 10 ) {
			// primeiros 5 p�naltis para cada time
			if ( ( diff > 5 - ( currentTurn >> 1 ) ) || ( ( 5 - ( ( currentTurn + 1 ) >> 1 ) ) < score[ 1 ] - score[ 0 ] ) ) {
				// se a diferen�a de gols for maior que o n�mero de penaltis a serem batidos, termina
				return true;
			}
		} else if ( diff > 0 && ( currentTurn & 1 ) == 0 ) {
			// cada time bate 1 p�nalti - m�dulo de currentTurn = 0 significa que pr�ximo batedor � do primeiro time; 
			// logo, o segundo time acabou de bater o p�nalti.
			// se houver diferen�a de gols ap�s segundo time bater, termina
			return true;
		}

		return false;
		//#endif
	} // fim do m�todo hasEnded()	


	/**
	 * Move a c�mera para a posi��o "position" real em "time" segundos. Caso "time" seja menor ou igual a zero, 
	 * a c�mera move-se instantaneamente para a posi��o recebida.
	 * 
	 * @param y posi��o y em pixels do topo da visualiza��o da c�mera.
	 * @param fp_time tempo em segundos que a c�mera levar� at� chegar ao destino.
	 */
	private final void moveCameraTo( int y, int fp_time ) {
		if ( y > 0 ) {
			y = 0;
		} else if ( y < CAMERA_MAX_Y ) {
			y = CAMERA_MAX_Y;
		}

		if ( fp_time > 0 ) {
			y = NanoMath.toFixed( y );
			
			fp_timeToDestination = fp_time;
			fp_cameraCurrentTime = 0;
			cameraState = CAMERA_STATE_MOVING;
			fp_cameraY = NanoMath.toFixed( fieldGroup.getPosY() );
			fp_cameraSpeed = NanoMath.divFixed( y - fp_cameraY, fp_time );
		} else {
			fieldGroup.setPosition( 0, y );
			fp_timeToDestination = 0;
			fp_cameraSpeed = 0;
			cameraState = CAMERA_STATE_STOPPED;
		}
	} // fim do m�todo moveCameraTo()


	public final byte getState() {
		return state;
	}


	public final short getCurrentTurn() {
		return currentTurn;
	}


	public final void fillScore( short[] score ) {
		score[ 0 ] = this.score[ 0 ];
		score[ 1 ] = this.score[ 1 ];
	}


	public final void onSequenceEnded( int id, int sequence ) {
		mutex.acquire();
		
		switch ( id ) {
			//#if JAR != "min"
			case REPLAY_ICON_SPRITE_ID:
				switch ( sequence ) {
					case REPLAY_ICON_SEQUENCE_HIDING:
						setReplayIconSequence( REPLAY_ICON_SEQUENCE_NONE );
					break;
				}
			break;
			//#endif
	
			default:
			switch ( sequence ) {
				case Shooter.SEQUENCE_SHOOTING:
					final boolean replay = state != STATE_MOVING && state != STATE_PLAY_RESULT;
					ball.kick( lastShooterPlay, replay );

					currentKeeper.jump( lastKeeperPlay, lastShooterPlay, replay );
					currentShooter.setSequence( Shooter.SEQUENCE_SHOT );
					
					MediaPlayer.play( SOUND_INDEX_KICK_BALL );
					
					// move a c�mera para mostrar a jogada
					//#if JAR == "min"
//# 					// n�o mostra tanto a parte superior do cen�rio nessa vers�o, j� que � vazio
//# 					moveCameraTo( CAMERA_CONTROL_Y - 8 - playResultBox.PARTIAL_HEIGHT, ball.getTimeToGoal() );
					//#else
					moveCameraTo( CAMERA_CONTROL_Y, ball.getTimeToGoal() );
					//#endif
					
					switch ( state ) {
						//#if JAR != "min"
						case STATE_REPLAY_ONLY:
						//#endif
							
						case STATE_REPLAY:
							timeToNextTransition = TIME_PARTIAL_RESULT_ANIM + NanoMath.toInt( NanoMath.mulFixed( ball.getTimeToGoal(), NanoMath.toFixed( 1000 ) ) );
						break;
					}
				break;
			}
		}
		
		mutex.release();
	}
	
	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
	
	
	/**
	 * Indica se � a vez do jogador (humano) chutar.
	 * 
	 * @return boolean indicando se � a vez do jogador chutar a gol.
	 */
	private final boolean isPlayerShooting() {
		switch ( mode ) {
			case MODE_CHAMPIONSHIP_MATCH_HOME:
			case MODE_TRAINING:
				return ( currentTurn & 1 ) == 0;
				
			case MODE_2_PLAYERS:
				return true;
			
			default:
				return ( currentTurn & 1 ) == 1;
		}
//		return ( mode == MODE_CHAMPIONSHIP_MATCH_AWAY && ( ( currentTurn & 1 ) == 1 ) ) ||
//			   ( mode != MODE_CHAMPIONSHIP_MATCH_AWAY && ( ( currentTurn & 1 ) == 0 ) );
	}
	
	
	/**
	 * Retorna o intervalo considerado para efeitos de atualiza��o do estado dos jogadores e bola, considerando
	 * a velocidade do replay.
	 * 
	 * @return 
	 */
	public final int getDelta() {
		return lastDelta;
	}
	
	
	private final void setReplaySpeed( int replaySpeed ) {
		if ( replaySpeed < 0 )
			replaySpeed = 0;
		else if ( replaySpeed > REPLAY_SPEED_VERY_SLOW )
			replaySpeed = REPLAY_SPEED_VERY_SLOW;
		
		currentReplaySpeed = ( byte ) replaySpeed;
		
		//#if JAR == "min"
//# 		if ( replayPaused ) {
//# 			replayLabel.setText( "REPLAY PAUSADO" );
//# 		} else {
//# 			switch ( replaySpeed ) {
//# 				case REPLAY_SPEED_NORMAL:
//# 					replayLabel.setText( "REPLAY 1X" );
//# 				break;
//# 
//# 				case REPLAY_SPEED_SLOW:
//# 					replayLabel.setText( "REPLAY 1/2X" );
//# 				break;
//# 
//# 				case REPLAY_SPEED_VERY_SLOW:
//# 					replayLabel.setText( "REPLAY 1/4X" );
//# 				break;
//# 			}
//# 		}
//# 		replayLabel.setPosition( size.x - replayLabel.getWidth(), playResultBox.PARTIAL_HEIGHT + 6 );
		//#else
			//#if SCREEN_SIZE == "SMALL"
//# 			if ( replayPaused ) {
//# 				replaySpeedIcon.setFrame( REPLAY_SPEED_VERY_SLOW + 1 );
//# 			} else {
//# 				replaySpeedIcon.setFrame( currentReplaySpeed );
//# 			}
			//#else
			if ( replayPaused ) {
				//#if JAR == "full"
				replaySpeedAnim.setSequence( REPLAY_SPEED_VERY_SLOW + 1 );
				//#endif
				
				replaySpeedIcon.setFrame( REPLAY_SPEED_VERY_SLOW + 1 );
			} else {
				//#if JAR == "full"
				replaySpeedAnim.setSequence( currentReplaySpeed );
				//#endif
				
				replaySpeedIcon.setFrame( currentReplaySpeed );
			}
			//#endif
		//#endif
		
		//#if DEBUG == "true"
		System.out.println( "Velocidade do replay: " + currentReplaySpeed );
		//#endif
	}


	private final void updateZOrder() {
		final int fp_ballZ = ball.getRealPosition().z;
		
		if ( fp_ballZ < 0 ) {
			switch ( ball.getState() ) {
				case Ball.BALL_STATE_GOAL:
				case Ball.BALL_STATE_POST_GOAL:
					setZOrder( ball, net, 1 );
				break;

				case Ball.BALL_STATE_POST_OUT:
				case Ball.BALL_STATE_OUT:
					setZOrder( ball, net, -1 );
				break;
			}
		} else if ( fp_ballZ <= FP_REAL_PENALTY_TO_GOAL ) {
			// bola est� entre a linha de fundo e a marca de p�nalti
			// o teste da refer�ncia para currentKeeper � necess�rio pois pode ocorrer de este m�todo ser chamado entre a
			// aloca��o/desaloca��o dos goleiros (no caso de aparelhos com pouca mem�ria)
			if ( currentKeeper != null )
				setZOrder( ball, currentKeeper, 1 );
		} else {
			// bola est� antes da marca do p�nalti (somente no caso de p�naltis batidos fortes na trave)
			// o teste da refer�ncia para currentShooter � necess�rio pois pode ocorrer de este m�todo ser chamado entre a
			// aloca��o/desaloca��o dos batedores (no caso de aparelhos com pouca mem�ria)
			if ( currentShooter != null )			
				setZOrder( ball, currentShooter, 1 );
		}
	}
	
	
	private final void setZOrder( Drawable d1, Drawable d2, int offset ) {
		int d1Index = -1;
		int d2Index = -1;

		try { // TODO try/catch em setZOrder (elimina travamentos aleat�rios?)
			for ( int i = 0; i < fieldGroup.getUsedSlots(); ++i ) {
				final Drawable d = fieldGroup.getDrawable( i );

				if ( d == d2 ) {
					d2Index = i;
					if ( d1Index >= 0 )
						break;
				} else if ( d == d1 ) {
					d1Index = i;
					if ( d2Index >= 0 )
						break;
				}
			}

			//#if DEBUG == "true"
		if ( d2Index < 0 || d1Index < 0 )
			throw new RuntimeException( "setZOrder: �ndice inv�lido. dIndex: " + d2Index + ", ballIndex: " + d1Index );
			//#endif


			d1Index = Math.max( d1Index, 0 );
			d2Index = Math.max( d2Index + offset, 0 );

			if ( d1Index != d2Index && d1Index < fieldGroup.getUsedSlots() && d2Index < fieldGroup.getUsedSlots() )		
				fieldGroup.setDrawableIndex( d1Index, d2Index );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}

	
	//#if DEBUG == "true"
	private final Point getScreenPos( int fp_x, int fp_y ) {
		final int INITIAL_X = size.x >> 1;
		final int INITIAL_Y = goal.getPosY() + goal.getHeight();		

		return new Point( INITIAL_X + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fp_x, FP_PENALTY_GOAL_WIDTH ), FP_REAL_GOAL_WIDTH ) ),
						  INITIAL_Y - NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fp_y, FP_PENALTY_FLOOR_TO_BAR ), FP_REAL_FLOOR_TO_BAR ) ) );
	}
	
	public final void paint( Graphics g ) {
		super.paint( g );
		
//		g.setColor( 0xffff00 );
//		g.drawRect( netAnimationViewport.x, netAnimationViewport.y, netAnimationViewport.width, netAnimationViewport.height );
		
		g.setColor( 0xff0000 );
		g.setClip( 0, 0, 1000, 1000 );
		
		
		final int[][] positions = new int[][] {
			{ Ball.FP_REAL_LEFT_POST_START, Ball.FP_REAL_BAR_BOTTOM  },
			{ Ball.FP_REAL_LEFT_POST_START, 0 },
			{ Ball.FP_REAL_LEFT_POST_END, Ball.FP_REAL_BAR_TOP },
			{ Ball.FP_REAL_LEFT_POST_END, 0 },
			
			{ Ball.FP_REAL_RIGHT_POST_START, Ball.FP_REAL_BAR_BOTTOM  },
			{ Ball.FP_REAL_RIGHT_POST_START, 0 },
			{ Ball.FP_REAL_RIGHT_POST_END, Ball.FP_REAL_BAR_TOP },
			{ Ball.FP_REAL_RIGHT_POST_END, 0 },
		};
		for ( int i = 0; i < positions.length; ++i ) {
			final Point pos = getScreenPos( positions[ i ][ 0 ], positions[ i ][ 1 ] );
			g.fillRect( pos.x - 1, fieldGroup.getPosY() + pos.y - 1, 2, 2 );
		}
		
//		
//		if ( currentKeeper != null ) {
//			final Quad keeperQuad = currentKeeper.getCollisionArea();
//			
//			final int INITIAL_X = goal.getPosX() + ( goal.getWidth() >> 1 );
//			final int INITIAL_Y = goal.getPosY() + goal.getHeight() + 8;
//		
//			final XYZ realPosition = new XYZ( keeperQuad.position );
//			
//			final Point topLeft = new Point( ( int ) ( INITIAL_X + ( realPosition.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//										 	 ( int ) ( INITIAL_Y - ( realPosition.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//			
//			final XYZ temp = new XYZ( realPosition );
//
//			temp.addEquals( keeperQuad.right.mult( keeperQuad.width ) );
//			final Point topRight = new Point( ( int ) ( INITIAL_X + ( temp.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//										 	 ( int ) ( INITIAL_Y - ( temp.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//
//			temp.set( realPosition );
//			temp.addEquals( keeperQuad.top.mult( -keeperQuad.height ) );
//			final Point bottomLeft = new Point( ( int ) ( INITIAL_X + ( temp.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//										 	 ( int ) ( INITIAL_Y - ( temp.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//			
//			temp.set( realPosition );
//			temp.addEquals( keeperQuad.top.mult( -keeperQuad.height ) );
//			temp.addEquals( keeperQuad.right.mult( keeperQuad.width ) );
//			
//			final Point bottomRight = new Point( ( int ) ( INITIAL_X + ( temp.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//												 ( int ) ( INITIAL_Y - ( temp.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//			
//			g.setColor( 0xff0000 );
//			g.drawLine( topLeft.x, topLeft.y, topRight.x, topRight.y );
//			g.drawLine( topLeft.x, topLeft.y, bottomLeft.x, bottomLeft.y );
//			g.drawLine( bottomLeft.x, bottomLeft.y, bottomRight.x, bottomRight.y );
//			g.drawLine( topRight.x, topRight.y, bottomRight.x, bottomRight.y );
//		}
	}
	//#endif
	

	//#if JAR != "min"
	public final void saveReplayData( Replay replay ) {
		replay.shooterIndex = ( currentTurn & 1 ) == 0 ? team1.getIndex() : team2.getIndex();
		replay.keeperIndex = ( currentTurn & 1 ) == 0 ? team2.getIndex() : team1.getIndex();
		replay.shooterPlay.set( lastShooterPlay );
		replay.keeperPlay.set( lastKeeperPlay );
		ball.saveReplayData( replay );
		replay.keeperGaveUp = currentKeeper.getLastGiveUp();
		replay.saveTime = System.currentTimeMillis();
	}
	
	private final void setReplayControlsState( byte state ) {
		switch ( state ) {
			case REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN:
				//#if SCREEN_SIZE == "SMALL"
//# 				replayControls.setPosition( 0, size.y - replayHideIcon.getHeight() );
				//#else
				replayControls.setPosition( 0, size.y - replaySpeedAnim.getPosY() - replaySpeedAnim.getHeight() );
				//#endif
			break;
			
			case REPLAY_CONTROLS_STATE_ALL_HIDDEN:
				replayControls.setPosition( 0, size.y );
			break;
			
			case REPLAY_CONTROLS_STATE_SHOWN:
				replayControls.setPosition( 0, size.y - replayControls.getHeight() );
			break;
			
			case REPLAY_CONTROLS_STATE_APPEARING_PARTIAL:
				switch ( replayControlState ) {
					case REPLAY_CONTROLS_STATE_APPEARING_PARTIAL:
					case REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN:
					return;
				}				
				replayControlSpeed.setSpeed( -replayHideIcon.getHeight() );
			break;
			
			case REPLAY_CONTROLS_STATE_APPEARING_FULL:
				switch ( replayControlState ) {
					case REPLAY_CONTROLS_STATE_APPEARING_FULL:
					case REPLAY_CONTROLS_STATE_SHOWN:
					return;
				}				
				replayControlSpeed.setSpeed( -replayControls.getHeight() );
			break;
			
			case REPLAY_CONTROLS_STATE_HIDING_FULL:
				switch ( replayControlState ) {
					case REPLAY_CONTROLS_STATE_ALL_HIDDEN:
					case REPLAY_CONTROLS_STATE_HIDING_FULL:
					return;
				}
				replayControlSpeed.setSpeed( replayControls.getHeight() );
			break;
			
			case REPLAY_CONTROLS_STATE_HIDING_PARTIAL:
				switch ( replayControlState ) {
					case REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN:
					case REPLAY_CONTROLS_STATE_HIDING_PARTIAL:
					return;
				}
				replayControlSpeed.setSpeed( replayControls.getHeight() );
			break;
		} // fim switch ( state )

		replayControls.setVisible( state != REPLAY_CONTROLS_STATE_ALL_HIDDEN );
		replayControlState = state;
		
		switch ( replayControlState ) {
			case REPLAY_CONTROLS_STATE_APPEARING_FULL:
			case REPLAY_CONTROLS_STATE_APPEARING_PARTIAL:
			case REPLAY_CONTROLS_STATE_SHOWN:
				replayHideIcon.setFrame( REPLAY_HIDE_ICON_FRAME_HIDE );
			break;
			
			case REPLAY_CONTROLS_STATE_HIDING_FULL:
			case REPLAY_CONTROLS_STATE_HIDING_PARTIAL:
			case REPLAY_CONTROLS_STATE_PARTIAL_HIDDEN:
				replayHideIcon.setFrame( REPLAY_HIDE_ICON_FRAME_SHOW );
			break;
		}
	}	
	
	private final void setReplayIconSequence( byte sequence ) {
		switch ( replayIcon.getSequence() ) {
			case REPLAY_ICON_SEQUENCE_NONE:
				switch ( sequence ) {
					case REPLAY_ICON_SEQUENCE_NONE:
					case REPLAY_ICON_SEQUENCE_HIDING:
						replayIcon.setVisible( false );
					return;
				}
			break;
			
			case REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR:
			case REPLAY_ICON_SEQUENCE_APPEARING:
				if ( sequence == REPLAY_ICON_SEQUENCE_APPEARING )
					sequence = REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR;
			break;
		}
		replayIcon.setVisible( sequence != REPLAY_ICON_SEQUENCE_NONE );
		replayIcon.setSequence( sequence );
	}	
	//#endif
	
	
	/**
	 * Verifica se j� � poss�vel acelerar um estado do jogo atrav�s de teclas.
	 */
	private final void checkKeyPressedTimerExpired() {
		if ( !changingState && timeToNextTransition <= timeMinToAccelerate )
			timerExpired();
	}
	
	
	/**
	 * Pr�-carrega alguns recursos sempre utilizados na tela de jogo, de forma a acelerar futuros carregamentos e evitar
	 * fragmenta��o de mem�ria.
	 * 
	 * @throws java.lang.Exception 
	 */
	public static final void loadImages() throws Exception {
		System.gc();
		
		//#if SCREEN_SIZE == "SMALL"
//# 		// imagem da parte esquerda do gramado
//# 		fieldLeft = new DrawableImage( PATH_IMAGES + "field_left.png" );
//# 		
		//#if JAR == "min"
//# 		// pattern do c�u
//# 		sky = new Pattern( SKY_COLOR );
		//#else
//# 		// pattern do c�u
//# 		sky = new Pattern( new DrawableImage( PATH_IMAGES + "sky.png" ) );
		//#endif
//# 		
		//#else
		
		// imagem central esquerda do gramado
		fieldCenterTopLeft = new DrawableImage( PATH_IMAGES + "field_ctl.png" );
		
		// imagem superior direita do gramado (somente espelha a imagem esquerda)
		fieldCenterTopRight = new DrawableImage( fieldCenterTopLeft );
		fieldCenterTopRight.setTransform( TRANS_MIRROR_H );
		
		if ( GameMIDlet.isLowMemory() ) {
			// pattern do c�u
			sky = new Pattern( COLOR_SKY );
			
			
			// monta o gramado
			final int[][] grassPosition = new int[][] {
				{ 13, 0 },
				{ 131, 0 },
				{ 0, 1 },
				{ 118, 1 },
				{ 145, 1 },
				{ 30, 6 },
				{ -12, 9 },
				{ 112, 10 },
				{ 132, 15 },
				{ 0, 20 },
				{ 15, 22 },
				{ 101, 27 },
				{ 14, 32 },
				{ 7, 36 },
				{ 59, 40 },
				{ 145, 41 },
			};
			final DrawableGroup fieldGroup = new DrawableGroup( grassPosition.length + 4 );
			fieldCenterBottom = fieldGroup;
			fieldGroup.setSize( SCREEN_DEFAULT_WIDTH, FIELD_BOTTOM_HEIGHT );
			
			// insere os patterns que d�o as cores principais da grama
			Pattern patternGrass = new Pattern( GRASS_COLOR_DARK );
			patternGrass.setSize( SCREEN_DEFAULT_WIDTH, FIELD_BOTTOM_DARK_HEIGHT );
			fieldGroup.insertDrawable( patternGrass  );
			
			patternGrass = new Pattern( GRASS_COLOR_MEDIUM );
			patternGrass.setSize( SCREEN_DEFAULT_WIDTH, FIELD_BOTTOM_MEDIUM_HEIGHT );
			patternGrass.setPosition( 0, FIELD_BOTTOM_DARK_HEIGHT );
			fieldGroup.insertDrawable( patternGrass  );
			
			patternGrass = new Pattern( GRASS_COLOR_LIGHT );
			patternGrass.setSize( SCREEN_DEFAULT_WIDTH, FIELD_BOTTOM_LIGHT_HEIGHT );
			patternGrass.setPosition( 0, FIELD_BOTTOM_DARK_HEIGHT + FIELD_BOTTOM_MEDIUM_HEIGHT );
			fieldGroup.insertDrawable( patternGrass );
			
			// insere as falhas na grama
			final DrawableImage imgGrass = new DrawableImage( PATH_IMAGES + "grass.png" );
			for ( byte i = 0; i < grassPosition.length; ++i ) {
				final DrawableImage g = new DrawableImage( imgGrass );
				g.setPosition( grassPosition[ i ][ 0 ], grassPosition[ i ][ 1 ] );
				fieldGroup.insertDrawable( g );
			}
			
			// insere a falha principal da grama (lama)
			final DrawableImage mud = new DrawableImage( PATH_IMAGES + "mud.png" );
			mud.setPosition( ( SCREEN_DEFAULT_WIDTH - mud.getWidth() ) >> 1, FIELD_MUD_Y );
			fieldGroup.insertDrawable( mud );
			
		} else {
			// pattern do c�u
			if ( GameMIDlet.isLowMemory() )
				sky = new Pattern( COLOR_SKY );
			else
				sky = new Pattern( new DrawableImage( PATH_IMAGES + "sky.png" ) );
			
			// imagem central inferior do gramado
			fieldCenterBottom = new DrawableImage( PATH_IMAGES + "field_cb.png" );
		}
		
		//#endif
		
		//#if JAR != "min"
		// torcida
		crowd = new Pattern( new DrawableImage( PATH_IMAGES + "crowd.png" ) );
		
		// insere as propagandas
		ads = new DrawableImage[ 2 ];
		for ( byte i = 0; i < 2; ++i )
			ads[ i ] = new DrawableImage( PATH_IMAGES + "ad_" + i + ".png" );
		//#endif
		
		// gol (traves e travess�o - rede � separada para permitir que a bola fique entre a trave e a rede)
		net = new DrawableImage( PATH_IMAGES + "goal_net.png" );
		net.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		
		final DrawableImage postLeft = new DrawableImage( PATH_IMAGES + "post_left.png" );
		
		//#if JAR == "min"
//# 		final DrawableImage postRight = new DrawableImage( postLeft );
//# 		postRight.setTransform( TRANS_MIRROR_H );
//# 		postRight.defineReferencePixel( 0, 0 );
		//#else
		final DrawableImage postRight = new DrawableImage( PATH_IMAGES + "post_right.png" );
		//#endif
		final DrawableImage bar = new DrawableImage( PATH_IMAGES + "bar.png" );
		
		bar.setPosition( postLeft.getWidth(), 0 );
		postRight.setPosition( bar.getPosX() + bar.getWidth(), 0 );
		
		goal = new DrawableGroup( 3 );
		goal.setSize( postRight.getPosX() + postRight.getWidth(), postRight.getHeight() );
		goal.insertDrawable( postLeft );
		goal.insertDrawable( postRight );
		goal.insertDrawable( bar );
		
		goal.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );		

		// caixa que cont�m a anima��o indicadora do resultado da jogada
		playResultBox = new PlayResultBox( GameMIDlet.getFont( FONT_INDEX_BOARD ) );
		
		//#if JAR != "min"
		// controle do replay
		replayIcon = new Sprite( PATH_IMAGES + ( GameMIDlet.isLowMemory() ? "replay_low.dat" : "replay.dat" ), PATH_IMAGES + "replay" );
		
		// anima��o da rede balan�ando ap�s um gol
		netAnimation = new DrawableImage( PATH_IMAGES + "net.png" );
		netAnimation.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		//#if SCREEN_SIZE == "MEDIUM" && JAR == "full"
		final String path = PATH_IMAGES + ( GameMIDlet.isLowMemory() || 
									( ScreenManager.SCREEN_HEIGHT < SCREEN_REPLAY_CONTROLS_VISIBLE_MIN_HEIGHT 
										&& !ScreenManager.getInstance().hasPointerEvents() ) ? "replay_low" : "replay" );
		//#else
//# 		final String path = PATH_IMAGES + "replay";
		//#endif
		replayControls = new UpdatableGroup( 6 );
		final DrawableImage imgControlLeft = new DrawableImage( path + "_left.png" );
		replayControlRightLoad = new DrawableImage( path + "_right_l.png" );
		replayControlRightSave = new DrawableImage( path + "_right_s.png" );
		
		replaySpeedIcon = new Sprite( PATH_IMAGES + "replay_icon.dat", PATH_IMAGES + "replay_icon" );
		
		//#if SCREEN_SIZE == "MEDIUM" && JAR == "full"
		if ( GameMIDlet.isLowMemory() || ScreenManager.SCREEN_HEIGHT < SCREEN_REPLAY_CONTROLS_VISIBLE_MIN_HEIGHT 
						&& !ScreenManager.getInstance().hasPointerEvents() )
			replayHideIcon = new Sprite( PATH_IMAGES + "hide_low.dat", PATH_IMAGES + "hide_low" );
		else
		//#endif
		replayHideIcon = new Sprite( PATH_IMAGES + "hide.dat", PATH_IMAGES + "hide" );
			//#if SCREEN_SIZE == "SMALL" 
//# 				replaySpeedIcon.setPosition( replayHideIcon.getWidth(), 0 );
//# 				imgControlLeft.setPosition( 0, replayHideIcon.getHeight() );
			//#else
				//#if JAR == "full"
				if ( GameMIDlet.isLowMemory() )
					replaySpeedAnim = new Sprite( PATH_IMAGES + "replay_anim_low.dat", PATH_IMAGES + "replay_anim" );
				else
					replaySpeedAnim = new Sprite( PATH_IMAGES + "replay_anim.dat", PATH_IMAGES + "replay_anim" );
				//#else
//# 				replaySpeedAnim = new DrawableImage( path + "_anim_0.png" );
				//#endif

			replaySpeedIcon.setPosition( replayHideIcon.getWidth() + ( ( replaySpeedAnim.getWidth() - replaySpeedIcon.getWidth() ) >> 1 ), 0 );
			replayHideIcon.setPosition( 0, replaySpeedIcon.getHeight() >> 1 );
			replaySpeedAnim.setPosition( replayHideIcon.getWidth(), replayHideIcon.getPosY() );
			imgControlLeft.setPosition( 0, replaySpeedAnim.getPosY() + replaySpeedAnim.getHeight() );
			//#endif
		
		
		
		replayControlRightLoad.setPosition( imgControlLeft.getWidth(), imgControlLeft.getPosY() );
		replayControlRightSave.setPosition( replayControlRightLoad.getPosition() );
		replayControls.setSize( replayControlRightSave.getPosX() + replayControlRightSave.getWidth(), 
								imgControlLeft.getPosY() + imgControlLeft.getHeight() );
		
		replayControls.insertDrawable( replayHideIcon );
		replayControls.insertDrawable( replaySpeedIcon );
		
		//#if SCREEN_SIZE != "SMALL"
		replayControls.insertDrawable( replaySpeedAnim );
		//#endif
		
		replayControls.insertDrawable( imgControlLeft );
		replayControls.insertDrawable( replayControlRightLoad );
		replayControls.insertDrawable( replayControlRightSave );
		//#endif
	}

	
	//#if SCREEN_SIZE != "SMALL"

	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		switch ( state ) {
			case STATE_REPLAY:
			case STATE_REPLAY_ONLY:
			case STATE_REPLAY_WAIT:
			case STATE_REPLAY_ONLY_WAIT:
				// controla o replay atrav�s do ponteiro
				if ( replayControls.contains( x, y ) ) {
					x -= replayControls.getPosX();
					y -= replayControls.getPosY();
					
					if ( y < replayControlRightLoad.getPosY() ) {
						// verifica se jogador clicou no �cone para esconder/mostrar o replay
						if ( x <= replayHideIcon.getWidth() )
							keyPressed( ScreenManager.KEY_STAR );
					} else {
						// jogador clicou num dos bot�es
						final int buttonIndex = ( x - REPLAY_CONTROL_FIRST_BUTTON_X + REPLAY_CONTROL_BUTTON_WIDTH - 1 ) / REPLAY_CONTROL_BUTTON_WIDTH;
						if ( buttonIndex < 6 ) {
							// jogador pressionou um dos bot�es de velocidade do replay, de rein�cio ou de fim
							keyPressed( ScreenManager.KEY_NUM0 + buttonIndex );
						} else if ( replayControlRightSave.isVisible() ) {
							// jogador pressionou o bot�o de grava��o de replay
							keyPressed( ScreenManager.KEY_NUM6 );
						}
					} // fim if ( y >= replayControlRightLoad.getPosY() && x >= REPLAY_CONTROL_FIRST_BUTTON_X )
					break;
				} // fim if ( replayControls.contains( x, y ) )
			// sem break aqui
			
			default:
				keyPressed( ScreenManager.FIRE );
		}
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif
	
	
}
