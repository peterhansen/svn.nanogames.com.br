/**
 * Championship.java
 * �2008 Nano Games.
 *
 * Created on Mar 31, 2008 7:17:13 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.Cray;
import core.Team;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 * 
 * @author Peter
 */
public final class Championship extends UpdatableGroup implements Constants, KeyListener
//#if SCREEN_SIZE != "SMALL"		
		, PointerListener
//#endif
{
	
	/** Divis�es vencidas pelo jogador (indica o progresso do jogo, ou seja, que divis�es o jogador pode escolher). */
	private static byte highestDivisionWon = DIVISION_NONE;
	
	
	//#if DEBUG == "true"
//	private static final String[] STAGE_STRING = { "1A RODADA", "2A RODADA", "3A RODADA", "4A RODADA", "5A RODADA", "6A RODADA", "FINAL 1", "PARTIDA ESPECIAL", "JOGADOR CAMPE�O", "JOGADOR ELIMINADO" };
	//#endif
	
	private byte currentStage;
	
	/** Quantidade total de times de cada divis�o: o do jogador e mais 3 na fase de grupos. */
	public static final byte TEAMS_TOTAL = 4;
	
	/** Quantidade de times na fase do grupo. */
	public static final byte TEAMS_IN_GROUP = 4;
	
	/** N�mero total de jogos por divis�o. */
	private static final byte TOTAL_MATCHES = STAGE_FINAL_MATCH_3 + 1;
	
	/** Quantidade de labels por cada linha de informa��o (nome do time, jogos, vit�rias, derrotas, saldo de gols, gols pr�). */
	private static final byte GROUP_LABELS_PER_LINE = 6;
	
	private static RichLabel[][] teamInfo;
	
	/** Times do campeonato atual (todos os times, n�o ordenados). */
	private final Team[] teams = new Team[ TEAMS_TOTAL ];
	
	/** Times da etapa do grupo, ordenados de acordo com sua classifica��o. */
	private final Team[] group = new Team[ TEAMS_IN_GROUP ];
	
	/** Tabela dos times do grupo. */
	private static UpdatableGroup labelsGroup;
	
	/** Times que disputam a final. */
	private final Team[] finalMatch = new Team[ 2 ];
	
	/** Resultado da partida final (n�mero de partidas ganhas de cada time). */
	private final short[] finalMatchScore = new short[ 2 ];
	
	private static RichLabel labelFinal;
	
	private static RichLabel labelChampion;

	/** Letreiro que mostra informa��es na parte inferior da tela. */
	private static MarqueeLabel labelBottom;
	
	/** Setas de scroll. */
	private final DrawableImage[] scrollArrows;
	
	private final boolean[] scrolling = new boolean[ 4 ];
	
	private short accScrollTime;
	
	private boolean arrowsVisible;
	
	private static final byte SCROLL_ARROW_UP		= 0;
	private static final byte SCROLL_ARROW_DOWN		= 1;
	private static final byte SCROLL_ARROW_LEFT		= 2;
	private static final byte SCROLL_ARROW_RIGHT	= 3;
	
	private static final short SCROLL_ARROW_BLINK_TIME = 666; // dem�in!
	
	/** Velocidade atual de movimenta��o horizontal da tela. */
	private final MUV speedX = new MUV();
	
	/** Velocidade atual de movimenta��o horizontal da tela. */
	private final MUV speedY = new MUV();
	
	/** M�dulo da velocidade de movimenta��o da tela. */
	private final int MOVE_SPEED = ScreenManager.SCREEN_WIDTH >> 1;
	
	private static final byte MOVE_LIMIT_LEFT = 0;
	private final int MOVE_LIMIT_RIGHT;
	private final int MOVE_LIMIT_DOWN;
	//#if JAR != "full"
//# 	private static final byte MOVE_LIMIT_UP = 0;
	//#else
	private final byte MOVE_LIMIT_UP = GameMIDlet.isLowMemory() ? 0 : ( byte ) GameMIDlet.getBlackBoard().getVisibleAreaY();
	//#endif
	
	//#if SCREEN_SIZE != "SMALL"
	/** �ltima posi��o do ponteiro. */
	private final Point pointerPos = new Point();
	//#endif
	
	private byte division;
	
	
	public Championship( byte division ) throws Exception {
		super( 12 );
		
		final ImageFont font = GameMIDlet.getFont( FONT_INDEX_DEFAULT );
		
		final int BORDER_SPACING = NanoMath.max( MOVE_LIMIT_UP, 6 );
		
		// insere a borda da tabela do grupo
		final Cray cray = new Cray();
		cray.setTarget( labelsGroup );
		insertDrawable( cray );
		
		insertDrawable( labelsGroup );

		if ( division == DIVISION_LOAD_GAME ) {
			loadSavedGame( this );
		} else {
			this.division = division;

			loadTeams();
		}
		
		// insere o label da final do campeonato
		labelFinal.setPosition( labelsGroup.getPosX() + labelsGroup.getWidth() + ( BORDER_SPACING << 1 ), labelsGroup.getPosY() );
		insertDrawable( labelFinal );
		formatLabel( currentStage );

		// insere o t�tulo da divis�o
		int indexTitle = -1;
		switch ( this.division ) {
			case DIVISION_1:
				indexTitle = TEXT_1ST_DIVISION;
			break;

			case DIVISION_2:
				indexTitle = TEXT_2ND_DIVISION;
			break;

			case DIVISION_3:
				indexTitle = TEXT_3RD_DIVISION;
			break;
			
			default:
				throw new IllegalArgumentException();
		}			
	
		// insere o label indicando o campe�o da divis�o
		labelChampion.setText( TEXT_CHAMPION_TITLE );
		labelChampion.setPosition( labelFinal.getPosX(), labelFinal.getPosY() + labelFinal.getHeight() + BORDER_SPACING );
		insertDrawable( labelChampion );
	
		// insere o letreiro que mostra informa��es na parte inferior da tela
		labelBottom.setPosition( 0, labelChampion.getPosY() + labelChampion.getHeight() + font.getHeight() + BORDER_SPACING );
		insertDrawable( labelBottom );
		
		setSize( labelFinal.getPosX() + labelFinal.getWidth() + BORDER_SPACING, Math.max( labelBottom.getPosY() + ( font.getHeight() << 1 ) + BORDER_SPACING, ScreenManager.SCREEN_HEIGHT ) );
		
		labelBottom.setSize( size.x, font.getHeight() );
		
		final Label title = new Label( font, GameMIDlet.getText( TEXT_MTV_ROCKGOL ) + GameMIDlet.getText( indexTitle ) );
		title.setPosition( ( size.x - title.getWidth() ) >> 1, BORDER_SPACING );
		insertDrawable( title );
		
		MOVE_LIMIT_RIGHT = Math.min( ScreenManager.SCREEN_WIDTH - size.x + MOVE_LIMIT_LEFT, MOVE_LIMIT_LEFT );
		MOVE_LIMIT_DOWN = Math.min( ScreenManager.SCREEN_HEIGHT - size.y + MOVE_LIMIT_UP, MOVE_LIMIT_UP );				
		
		// insere as setas de scroll (todas utilizam a mesma imagem)
		scrollArrows = new DrawableImage[ 4 ];
		scrollArrows[ SCROLL_ARROW_UP ] = new DrawableImage( PATH_IMAGES + "scroll.png" );
		insertDrawable( scrollArrows[ SCROLL_ARROW_UP ] );
		scrolling[ SCROLL_ARROW_UP ] = false;
		
		//#if ROTATE_BUG == "false"
	
		for ( byte i = 1; i < 4; ++i ) {
			scrollArrows[ i ] = new DrawableImage( scrollArrows[ SCROLL_ARROW_UP ] );
			scrollArrows[ i ].setVisible( false );
			insertDrawable( scrollArrows[ i ] );
			
			scrolling[ i ] = false;
		}
		scrollArrows[ SCROLL_ARROW_LEFT ].setTransform( TRANS_ROT270 | TRANS_MIRROR_V );
		scrollArrows[ SCROLL_ARROW_RIGHT ].setTransform( TRANS_ROT90 );
		
		//#else
//# 	
//# 		scrollArrows[ SCROLL_ARROW_DOWN ] = new DrawableImage( scrollArrows[ SCROLL_ARROW_UP ] );
//# 		scrollArrows[ SCROLL_ARROW_DOWN ].setVisible( false );
//# 		insertDrawable( scrollArrows[ SCROLL_ARROW_DOWN ] );
//# 		scrolling[ SCROLL_ARROW_DOWN ] = false;
//# 		
//# 		scrollArrows[ SCROLL_ARROW_LEFT ] = new DrawableImage( PATH_IMAGES + "scroll_h.png" );
//# 		scrollArrows[ SCROLL_ARROW_LEFT ].setVisible( false );
//# 		insertDrawable( scrollArrows[ SCROLL_ARROW_LEFT ] );
//# 		scrolling[ SCROLL_ARROW_LEFT ] = false;
//# 		
//# 		scrollArrows[ SCROLL_ARROW_RIGHT ] = new DrawableImage( scrollArrows[ SCROLL_ARROW_LEFT ] );
//# 		scrollArrows[ SCROLL_ARROW_RIGHT ].setVisible( false );
//# 		insertDrawable( scrollArrows[ SCROLL_ARROW_RIGHT ] );
//# 		scrolling[ SCROLL_ARROW_RIGHT ] = false;
//# 		scrollArrows[ SCROLL_ARROW_RIGHT ].setTransform( TRANS_MIRROR_H );
//# 	
		//#endif
		
		scrollArrows[ SCROLL_ARROW_UP ].defineReferencePixel( ANCHOR_HCENTER );
		scrollArrows[ SCROLL_ARROW_UP ].setTransform( TRANS_MIRROR_H );
		
		scrollArrows[ SCROLL_ARROW_DOWN ].setTransform( TRANS_MIRROR_H | TRANS_MIRROR_V );
		scrollArrows[ SCROLL_ARROW_DOWN ].defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );

		scrollArrows[ SCROLL_ARROW_LEFT ].defineReferencePixel( ANCHOR_VCENTER );
		scrollArrows[ SCROLL_ARROW_RIGHT ].defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		
		// atualiza labels e a posi��o da c�mera
		setStage( currentStage );
		updateGroupLabels();
	} // fim do construtor
	
	
	/**
	 * Carrega os times da divis�o atual, preenchendo os arrays teams[] e group[].
	 */
	private final void loadTeams() throws Exception {
		// explica��o da conta: normalmente, multiplicaria-se o �ndice da divis�o por TEAMS_TOTAL. Mas, como o time do Cleston
		// sempre participa do campeonato, o offset � dado pela quantidade de outros times de cada divis�o.
		final byte INDEX_OFFSET = ( byte ) ( division * ( TEAMS_IN_GROUP ) );
		for ( byte i = 0; i < TEAMS_TOTAL; ++i )
			teams[ i ] = new Team( i + INDEX_OFFSET );

		for ( byte i = 0; i < TEAMS_IN_GROUP; ++i )
			group[ i ] = teams[ i ];
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				checkAndSaveGame();
				
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
			
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				playRound();
			break;
			
			case ScreenManager.KEY_NUM1:
				speedX.setSpeed( MOVE_SPEED );
				
			case ScreenManager.KEY_NUM2:
			case ScreenManager.UP:
				speedY.setSpeed( MOVE_SPEED );
			break;

			case ScreenManager.KEY_NUM9:
				speedX.setSpeed( -MOVE_SPEED );
				
			case ScreenManager.KEY_NUM8:
			case ScreenManager.DOWN:
				speedY.setSpeed( -MOVE_SPEED );
			break;

			case ScreenManager.KEY_NUM7:
				speedY.setSpeed( -MOVE_SPEED );
				
			case ScreenManager.KEY_NUM4:
			case ScreenManager.LEFT:
				speedX.setSpeed( MOVE_SPEED );
			break;

			case ScreenManager.KEY_NUM3:
				speedY.setSpeed( MOVE_SPEED );
			
			case ScreenManager.KEY_NUM6:
			case ScreenManager.RIGHT:
				speedX.setSpeed( -MOVE_SPEED );
			break;
		}
	}


	public final void keyReleased( int key ) {
		speedX.setSpeed( 0 );
		speedY.setSpeed( 0 );
	}
	
	
	private final void centerCameraAt( Drawable d ) {
		centerCameraAt( d.getPosX() + ( d.getWidth() >> 1 ), d.getPosY() + ( d.getHeight() >> 1 ) );
	}
	
	
	private final void centerCameraAt( int x, int y ) {
		setPosition( ScreenManager.SCREEN_HALF_WIDTH - x, ScreenManager.SCREEN_HALF_HEIGHT - y );
		
		checkBounds();
	}


	/**
	 * Joga a rodada atual.
	 */
	private final void playRound() {
		keyReleased( 0 );
		
		switch ( currentStage ) {
			case STAGE_1ST_ROUND:
				playMatch( teams[ 0 ], teams[ 1 ] );
			break;
			
			case STAGE_4TH_ROUND:
				playMatch( teams[ 1 ], teams[ 0 ] );
			break;
			
			case STAGE_2ND_ROUND:
				playMatch( teams[ 0 ], teams[ 2 ] );
			break;
			
			case STAGE_5TH_ROUND:
				playMatch( teams[ 2 ], teams[ 0 ] );
			break;
			
			case STAGE_3RD_ROUND:
				playMatch( teams[ 0 ], teams[ 3 ] );
			break;
			
			case STAGE_6TH_ROUND:
				playMatch( teams[ 3 ], teams[ 0 ] );
			break;
			
			case STAGE_FINAL_MATCH_1:
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				playMatch( finalMatch[ 0 ], finalMatch[ 1 ] );
			break;
			
			case STAGE_PLAYER_WON:
			case STAGE_PLAYER_ELIMINATED:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		}
	} // fim do m�todo playRound()
	
	
	private final void playMatch( Team team1, Team team2 ) {
		final byte difficultyLevel = ( byte ) Math.min( 5 + ( ( ( division * TOTAL_MATCHES ) + currentStage ) * 100 / ( DIVISIONS_TOTAL * TOTAL_MATCHES ) ), 100 );

		//#if DEBUG == "true"
		System.out.println( "DIFICULDADE DA PARTIDA ATUAL: " + difficultyLevel );
		//#endif
		
		GameMIDlet.playMatch( division, currentStage, difficultyLevel, team1 == teams[ 0 ] ? Match.MODE_CHAMPIONSHIP_MATCH_HOME : Match.MODE_CHAMPIONSHIP_MATCH_AWAY, team1, team2 );
	}
	
	
	public final void playerMatchEnded( Match match ) {
		switch ( currentStage ) {
			case STAGE_1ST_ROUND:
			case STAGE_4TH_ROUND:
				if ( currentStage == STAGE_1ST_ROUND ) {
					// jogos: 0 x 1 e 2 x 3
					updateTeams( match, teams[ 0 ], teams[ 1 ] );
					updateTeams( null, teams[ 2 ], teams[ 3 ] );
				} else {
					// jogos: 1 x 0 e 3 x 2
					updateTeams( match, teams[ 1 ], teams[ 0 ] );
					updateTeams( null, teams[ 3 ], teams[ 2 ] );
				}
				
				setStage( currentStage + 1 );
				sortTeams();
			break;
			
			case STAGE_2ND_ROUND:
			case STAGE_5TH_ROUND:
				if ( currentStage == STAGE_2ND_ROUND ){
					// jogos: 0 x 2 e 1 x 3
					updateTeams( match, teams[ 0 ], teams[ 2 ] );
					updateTeams( null, teams[ 1 ], teams[ 3 ] );
				} else {
					// jogos: 2 x 0 e 3 x 1
					updateTeams( match, teams[ 2 ], teams[ 0 ] );
					updateTeams( null, teams[ 3 ], teams[ 1 ] );
				}
				
				setStage( currentStage + 1 );
				sortTeams();
			break;
			
			case STAGE_3RD_ROUND:
			case STAGE_6TH_ROUND:
				// o jogador � sempre teams[ 0 ]
				if ( currentStage == STAGE_3RD_ROUND ) {
					// jogos: 0 x 3 e 1 x 2
					updateTeams( match, teams[ 0 ], teams[ 3 ] );
					updateTeams( null, teams[ 1 ], teams[ 2 ] );	
					
					sortTeams();
					
					setStage( currentStage + 1 );
					sortTeams();
				} else {
					//#if I_AM_THE_PENALTY_KING == "true"
//# 					teams[ 0 ].updateStatus( true, 5, 5 );
					//#endif
					
					// jogos: 3 x 0 e 2 x 1
					updateTeams( match, teams[ 3 ], teams[ 0 ] );
					updateTeams( null, teams[ 2 ], teams[ 1 ] );	
					
					sortTeams();
					
					if ( group[ 0 ] == teams[ 0 ] || group[ 1 ] == teams[ 0 ] ) {
						// jogador passou � final

						setStage( STAGE_FINAL_MATCH_1 );
					} else {
						// jogador foi eliminado na fase de grupo
						prepareFinalMatch();

						setStage( STAGE_PLAYER_ELIMINATED );
					}
				}
				updateGroupLabels();
			break;
			
			case STAGE_FINAL_MATCH_1:
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				final short[] score = new short[ 2 ];
				match.fillScore( score );
				++finalMatchScore[ score[ 0 ] > score[ 1 ] ? 0 : 1 ];
				
				// verifica se o campeonato acabou
				if ( finalMatchScore[ 0 ] >= 2 ) {
					if ( finalMatch[ 0 ] == teams[ 0 ] ) {
						// jogador ganhou o campeonato
						setStage( STAGE_PLAYER_WON );
						
						//#if JAR != "min"
						GameMIDlet.prepareEnding();
						return;
						//#endif
					} else {
						// jogador perdeu a final
						setStage( STAGE_PLAYER_ELIMINATED );
					}
				} else if ( finalMatchScore[ 1 ] >= 2 ) {
					if ( finalMatch[ 1 ] == teams[ 0 ] ) {
						// jogador ganhou o campeonato
						setStage( STAGE_PLAYER_WON );
						
						//#if JAR != "min"
						GameMIDlet.prepareEnding();
						return;
						//#endif
					} else {
						// jogador perdeu a final
						setStage( STAGE_PLAYER_ELIMINATED );
					}
				} else {
					setStage( currentStage + 1 );
				}
			break;
			
			case STAGE_PLAYER_WON:
			case STAGE_PLAYER_ELIMINATED:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		}		
		
		GameMIDlet.setScreen( SCREEN_CHAMPIONSHIP );
		keyReleased( 0 );
	} // fim do m�todo playerMatchEnded( Match )


	/**
	 * Prepara a partida final, obtendo os 2 primeiros colocados do grupo e atualizando os labels.
	 */
	private final void prepareFinalMatch() {
		finalMatch[ 0 ] = group[ 0 ];
		finalMatch[ 1 ] = group[ 1 ];
	}


	private final void setStage( int stage ) {
		switch ( stage ) {
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
			case STAGE_4TH_ROUND:
			case STAGE_5TH_ROUND:
			case STAGE_6TH_ROUND:
				labelBottom.setText( TEXT_STAGE_FIRST_MATCH + ( stage - STAGE_1ST_ROUND ), false );
				
				centerCameraAt( labelsGroup );
			break;
				
			case STAGE_FINAL_MATCH_1:
				prepareFinalMatch();
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				formatLabel( stage );
				
				labelBottom.setText( TEXT_STAGE_FINAL_MATCH_1 + ( stage - STAGE_FINAL_MATCH_1 ), false );
				
				centerCameraAt( labelFinal );
			break;
			
			case STAGE_PLAYER_WON:
				// atualiza o progresso do jogador caso necess�rio
				highestDivisionWon = ( byte ) Math.max( division, highestDivisionWon );
				
				// apaga o campeonato salvo anteriormente, armazenando somente o progresso do jogador
				eraseSavedGame();
				
				formatLabel( STAGE_FINAL_MATCH_3 );
				updateChampionLabel();
				
				switch ( division ) {
					case DIVISION_3:
					case DIVISION_2:
					case DIVISION_1:
						labelBottom.setText( TEXT_STAGE_CHAMPION_DIVISION_3 + ( division - DIVISION_3 ), false );
					break;
				}
				
				centerCameraAt( labelChampion );
			break;
			
			case STAGE_PLAYER_ELIMINATED:
				final short[] score = new short[ 2 ];
				while ( finalMatchScore[ 0 ] < 2 && finalMatchScore[ 1 ] < 2 ) {
					Match.simulate( score );
					++finalMatchScore[ score[ 0 ] > score[ 1 ] ? 0 : 1 ];
				}
				formatLabel( STAGE_FINAL_MATCH_3 );
				
				// atualiza o label do campe�o
				updateChampionLabel();
				
				//#if JAR != "min"
				MediaPlayer.play( SOUND_INDEX_LOSE_CUP );
				//#endif
				labelBottom.setText( TEXT_STAGE_ELIMINATED, false);
				
				centerCameraAt( labelBottom );
			break;
		}
		
		currentStage = ( byte ) stage;
		
		updateBottomLabelText();
		
		checkAndSaveGame();
		
		//#if DEBUG == "true"
//		System.out.println( "CHAMPIONSHIP STAGE: "+ STAGE_STRING[ currentStage ] );
		//#endif
	}
	
	
	/**
	 * Atualiza o label inferior da tela de acordo com a etapa atual.
	 */
	private final void updateBottomLabelText() {
		final StringBuffer text = new StringBuffer();
		
		switch ( currentStage ) {
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
			case STAGE_4TH_ROUND:
			case STAGE_5TH_ROUND:
			case STAGE_6TH_ROUND:
				text.append( GameMIDlet.getText( TEXT_STAGE_FIRST_MATCH + currentStage - STAGE_1ST_ROUND ) );
				text.append( ": " );
				if ( currentStage < STAGE_4TH_ROUND )
					text.append( GameMIDlet.getText( TEXT_TEAM_NAME_CLESTON ) );
				else
					text.append( teams[ ( currentStage % ( TEAMS_IN_GROUP - 1 ) ) + 1 ].getName() );
					
				text.append( " X " );
				
				if ( currentStage < STAGE_4TH_ROUND )
					text.append( teams[ ( currentStage % ( TEAMS_IN_GROUP - 1 ) ) + 1 ].getName() );
				else
					text.append( GameMIDlet.getText( TEXT_TEAM_NAME_CLESTON ) );
			break;
				
			case STAGE_FINAL_MATCH_1:
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				text.append( GameMIDlet.getText( TEXT_STAGE_FINAL_MATCH_1 + ( currentStage - STAGE_FINAL_MATCH_1 ) ) );
				text.append( ": " );
				text.append( finalMatch[ 0 ].getName() );
				text.append( " X " );
				text.append( finalMatch[ 1 ].getName() );
				text.append( "\n" );
			break;
			
			case STAGE_PLAYER_WON:
				switch ( division ) {
					case DIVISION_3:
					case DIVISION_2:
					case DIVISION_1:
						text.append( GameMIDlet.getText( TEXT_STAGE_CHAMPION_DIVISION_3 + division - DIVISION_3 ) );
					break;
				}
			break;
			
			case STAGE_PLAYER_ELIMINATED:
				text.append( GameMIDlet.getText( TEXT_STAGE_ELIMINATED ) );
			break;
		}		
		
		labelBottom.setText( text.toString(), false );
	}
	
	
	private final void updateChampionLabel() {
		labelChampion.setText( GameMIDlet.getText( TEXT_CHAMPION_TITLE ) + ( finalMatchScore[ 0 ] > finalMatchScore[ 1 ] ? finalMatch[ 0 ].getName() : finalMatch[ 1 ].getName() ) );
		labelChampion.setSize( labelChampion.getWidth(), labelChampion.getTextTotalHeight() );
	}
	
	
	private final void formatLabel( int stage ) {
		final StringBuffer buffer = new StringBuffer( "<ALN_H>" );

		switch ( stage ) {
			case STAGE_FINAL_MATCH_1:
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				buffer.append( GameMIDlet.getText( TEXT_STAGE_FINAL_MATCH_1 + ( stage - STAGE_FINAL_MATCH_1 ) ) );
				buffer.append( "\n\n" );
				
				if ( finalMatch[ 0 ] == null ) {
					buffer.append( GameMIDlet.getText( TEXT_TEAMS_UNKNOWN ) );
				} else {
					buffer.append( finalMatch[ 0 ].getShortName() );
					
					if ( finalMatchScore[ 0 ] == 0 && finalMatchScore[ 1 ] == 0 ) {
						// partida ainda n�o foi realizada
						buffer.append( "  X  " );
					} else {
						// preenche o resultado da partida final
						buffer.append( ' ' );
						buffer.append( finalMatchScore[ 0 ] );
						buffer.append( " X " );
						buffer.append( finalMatchScore[ 1 ] );
						buffer.append( ' ' );
					}
					
					buffer.append( finalMatch[ 1 ].getShortName() );
				}
			break;
			
			default:
				buffer.append( GameMIDlet.getText( TEXT_STAGE_FINAL ) );
				buffer.append( "\n\n" );
				buffer.append( GameMIDlet.getText( TEXT_TEAMS_UNKNOWN ) );
		}
		
		labelFinal.setText( buffer.toString() );
		labelFinal.setSize( labelFinal.getWidth(), labelFinal.getTextTotalHeight() );
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		// atualiza a posi��o da tela, caso o jogador a esteja movendo
		move( speedX.updateInt( delta ), speedY.updateInt( delta ) );
		
		checkBounds();
		
		// atualiza o estado de visibilidade das setas de scroll
		accScrollTime -= delta;
		if ( accScrollTime <= 0 ) {
			arrowsVisible = !arrowsVisible;
			accScrollTime = SCROLL_ARROW_BLINK_TIME;
			
			for ( byte i = 0; i < 4; ++i ) {
				if ( scrolling[ i ] )
					scrollArrows[ i ].setVisible( arrowsVisible );
			}
		}
		
		//#if JAR == "full"
		if ( !GameMIDlet.isLowMemory() )
			GameMIDlet.getBlackBoard().setPosition( position.x - MOVE_LIMIT_LEFT, position.y - MOVE_LIMIT_UP );
		//#endif
	}
	
	
	private final void checkBounds() {
		if ( position.x > MOVE_LIMIT_LEFT ) {
			position.x = MOVE_LIMIT_LEFT;
			speedX.setSpeed( 0 );
			
			scrollArrows[ SCROLL_ARROW_LEFT ].setVisible( false );
		} else if ( position.x < MOVE_LIMIT_RIGHT ) {
			position.x = MOVE_LIMIT_RIGHT;
			speedX.setSpeed( 0 );
			
			scrollArrows[ SCROLL_ARROW_RIGHT ].setVisible( false );
		}
		
		if ( position.y > MOVE_LIMIT_UP ) {
			position.y = MOVE_LIMIT_UP;
			speedY.setSpeed( 0 );
			
			scrollArrows[ SCROLL_ARROW_UP ].setVisible( false );
		} else if ( position.y < MOVE_LIMIT_DOWN ) {
			position.y = MOVE_LIMIT_DOWN;
			speedY.setSpeed( 0 );

			scrollArrows[ SCROLL_ARROW_DOWN ].setVisible( false );
		}
		
		scrolling[ SCROLL_ARROW_UP ] = position.y < MOVE_LIMIT_UP;
		scrolling[ SCROLL_ARROW_DOWN ] = position.y > MOVE_LIMIT_DOWN;
		scrolling[ SCROLL_ARROW_LEFT ] = position.x < MOVE_LIMIT_LEFT;
		scrolling[ SCROLL_ARROW_RIGHT ] = position.x > MOVE_LIMIT_RIGHT;
		
		scrollArrows[ SCROLL_ARROW_UP ].setRefPixelPosition( -position.x + ScreenManager.SCREEN_HALF_WIDTH, -position.y );
		scrollArrows[ SCROLL_ARROW_DOWN ].setRefPixelPosition( -position.x + ScreenManager.SCREEN_HALF_WIDTH, -position.y + ScreenManager.SCREEN_HEIGHT );
		scrollArrows[ SCROLL_ARROW_LEFT ].setRefPixelPosition( -position.x, -position.y + ScreenManager.SCREEN_HALF_HEIGHT );
		scrollArrows[ SCROLL_ARROW_RIGHT ].setRefPixelPosition( -position.x + ScreenManager.SCREEN_WIDTH, -position.y + ScreenManager.SCREEN_HALF_HEIGHT );
	}
	
	
	/**
	 * Atualiza o estado de 2 times ap�s uma partida.
	 * 
	 * @param match partida entre as 2 equipes. Caso seja null, � simulado o resultado.
	 */
	private final void updateTeams( Match match, Team team1, Team team2 ) {
		final short[] score = new short[ 2 ];
		
		if ( match == null )
			Match.simulate( score );
		else
			match.fillScore( score );
		
		final int goalDiff = score[ 0 ] - score[ 1 ];
		team1.updateStatus( goalDiff > 0, score[ 0 ], goalDiff );
		team2.updateStatus( goalDiff < 0, score[ 1 ], -goalDiff );
	}
	
	
	/**
	 * Ordena os times de acordo com sua classifica��o e atualiza os labels do grupo.
	 */
	private final void sortTeams() {
		// reordena os times, caso seja necess�rio
		for ( byte i = 0; i < TEAMS_IN_GROUP; ++i ) {
			int bestIndex = i;
			Team best = group[ bestIndex ];
			
			for ( int j = i + 1; j < TEAMS_IN_GROUP; ++j ) {
				final Team t2 = group[ j ];
				
				if ( best.getVictories() < t2.getVictories() ) {
					bestIndex = j;
					best = group[ bestIndex ];
				} else if ( best.getVictories() == t2.getVictories() ) {
					// empataram no n�mero de vit�rias; o 2� crit�rio de desempate � o saldo de gols
					if ( best.getGoalsDiff() < t2.getGoalsDiff() ) {
						bestIndex = j;
						best = group[ bestIndex ];
					} else if ( best.getGoalsDiff() == t2.getGoalsDiff() ) {
						// empataram no saldo de gols; vai para o �ltimo crit�rio de desempate: gols marcados
						if ( best.getGoalsScored() < t2.getGoalsScored() ) {
							bestIndex = j;
							best = group[ bestIndex ];
						}
					}
				}
			} // fim for ( int j = i + 1; j < TEAMS_IN_GROUP; ++j )
			
			if ( bestIndex != i ) {
				// faz uma troca de posi��es - h� um time melhor que o time de �ndice 'i'
				final Team temp = group[ i ];
				group[ i ] = group[ bestIndex ];
				group[ bestIndex ] = temp;
			}
			
			//#if DEBUG == "true"
			System.out.println( i + ": " + group[ i ].getVictories() + " / " + group[ i ].getGoalsDiff() + " / " + group[ i ].getGoalsScored() );
			//#endif			
		} // fim for ( byte i = 0; i < TEAMS_IN_GROUP; ++i )
		
		updateGroupLabels();
	}
	
	
	private final void updateGroupLabels() {
		// atualiza os labels
		final byte stage = ( byte ) Math.min( currentStage, STAGE_FINAL_MATCH_1 );
		for ( byte i = 0, j = 0; i < TEAMS_IN_GROUP; ++i, j = 0 ) {
			final Team team = group[ i ];
			
			teamInfo[ i ][ j++ ].setText( "<ALN_H>" + team.getShortName() );
			teamInfo[ i ][ j++ ].setText( "<ALN_H>" + stage );
			teamInfo[ i ][ j++ ].setText( "<ALN_H>" + team.getVictories() );
			teamInfo[ i ][ j++ ].setText( "<ALN_H>" + ( Math.min( currentStage, STAGE_FINAL_MATCH_1 ) - team.getVictories() ) );
			teamInfo[ i ][ j++ ].setText( "<ALN_H>" + team.getGoalsDiff() );
			teamInfo[ i ][ j   ].setText( "<ALN_H>" + team.getGoalsScored() );
		}				
	}
	
	
	/**
	 * Obt�m o progresso atual do jogador, ou seja, qual a divis�o mais alta vencida por ele.
	 */
	public static final byte getHighestDivisionWon() {
		return highestDivisionWon;
	}
	
	
	public static final byte getTeamsAvailable() {
		switch ( getHighestDivisionWon() ) {
			case DIVISION_1:
				return ( byte ) ( TEAMS_TOTAL * ( getHighestDivisionWon() + 1 ) );
				
			case DIVISION_2:
			case DIVISION_3:
				return ( byte ) ( ( TEAMS_TOTAL * ( getHighestDivisionWon() + 1 ) ) + 1 );

			default:
				return TEAMS_IN_GROUP;
		}		
	}
	
	
	//#if SCREEN_SIZE != "SMALL"
	
	public final void onPointerDragged( int x, int y ) {
		move( x - pointerPos.x, y - pointerPos.y );
		pointerPos.set( x, y );
		
		checkBounds();
	}


	public final void onPointerPressed( int x, int y ) {
		pointerPos.set( x, y );
	}


	public final void onPointerReleased( int x, int y ) {
	}	
	
	//#endif
	
	
	public static final void loadResources() throws Exception {
		final ImageFont font = GameMIDlet.getFont( FONT_INDEX_DEFAULT );
		
		labelFinal = new RichLabel( font, null, ScreenManager.SCREEN_WIDTH, null );
		
		//#if JAR != "full"
//# 		final int BORDER_SPACING = 6;
		//#else
		final int BORDER_SPACING = GameMIDlet.isLowMemory() ? 6 : GameMIDlet.getBlackBoard().getVisibleAreaY();
		//#endif
		
		final int LABEL_WIDTH = font.getTextWidth( "999" );
		final int LABEL_NAME_WIDTH = font.getTextWidth( GameMIDlet.getText( TEXT_TEAM_NAME ) );
		final int TOTAL_GROUP_LABELS = GROUP_LABELS_PER_LINE * ( TEAMS_IN_GROUP + 1 );
		
		final DrawableImage imgH = new DrawableImage( PATH_IMAGES + "h.png" );

		// adiciona-se 2 ao n�mero de itens devido �s linhas que separam os t�tulos das colunas e os nomes dos times das informa��es
		labelsGroup = new UpdatableGroup( TOTAL_GROUP_LABELS + 2 );
		labelsGroup.setPosition( BORDER_SPACING << 1, BORDER_SPACING + ( font.getHeight() << 1 ) );
		labelsGroup.setSize( LABEL_NAME_WIDTH + ( LABEL_WIDTH * ( GROUP_LABELS_PER_LINE - 1 ) ), font.getHeight() * ( TEAMS_IN_GROUP + 1 ) + imgH.getHeight() );
		
		final Point labelPosition = new Point( 0, 0 );
		
		final String[] texts = new String[] {
			"<ALN_H>" + GameMIDlet.getText( TEXT_TEAM_NAME ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_GAMES ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_VICTORIES ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_LOSSES ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_GOALS_DIFF ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_GOALS_FOR ),
		};		
		
		
		// insere primeiro os indicadores dos labels (Nome, J, V, D, SG, GP)
		for ( byte i = 0; i < GROUP_LABELS_PER_LINE; ++i ) {
			final RichLabel label = new RichLabel( font, texts[ i ], i == 0 ? LABEL_NAME_WIDTH : LABEL_WIDTH, null  );
			label.setPosition( labelPosition );
			labelsGroup.insertDrawable( label );
			
			labelPosition.x += label.getWidth();
		}
		labelPosition.set( 0, font.getHeight() + imgH.getHeight() );
		
		final Pattern lineH = new Pattern( imgH );
		lineH.setSize( labelsGroup.getWidth(), imgH.getHeight() );
		lineH.setPosition( 0, labelPosition.y - imgH.getHeight() );
		labelsGroup.insertDrawable( lineH );
		
		//#if ROTATE_BUG == "false"
		final DrawableImage imgV = new DrawableImage( imgH );
		imgV.setTransform( TRANS_ROT90 );
		imgV.defineReferencePixel( 0, 0 );
		//#else
//# 		final DrawableImage imgV = new DrawableImage( PATH_IMAGES + "v.png" );
		//#endif
		
		final Pattern lineV = new Pattern( imgV );
		lineV.setSize( imgV.getWidth(), labelsGroup.getHeight() );
		lineV.setPosition( LABEL_NAME_WIDTH, 0 );
		labelsGroup.insertDrawable( lineV );
		
		// insere os n�meros indicando as estat�sticas de cada time
		teamInfo = new RichLabel[ TEAMS_IN_GROUP ][];
		for ( byte i = 0; i < TEAMS_IN_GROUP; ++i, labelPosition.x = 0, labelPosition.y += font.getHeight() ) {
			teamInfo[ i ] = new RichLabel[ GROUP_LABELS_PER_LINE ];
			
			final RichLabel labelName = new RichLabel( font, " ", LABEL_NAME_WIDTH, null );
			labelName.setPosition( labelPosition );
			teamInfo[ i ][ 0 ] = labelName;
			labelsGroup.insertDrawable( labelName );
			
			labelPosition.x += LABEL_NAME_WIDTH;
			
			for ( byte j = 1; j < GROUP_LABELS_PER_LINE; ++j, labelPosition.x += LABEL_WIDTH ) {
				final RichLabel label = new RichLabel( font, "<ALN_H>0", LABEL_WIDTH, null );
				label.setPosition( labelPosition );
				
				teamInfo[ i ][ j ] = label;
				
				labelsGroup.insertDrawable( label );
			}
		}		
		
		labelChampion = new RichLabel( font, GameMIDlet.getText( TEXT_CHAMPION_TITLE ), ScreenManager.SCREEN_WIDTH, null );
		
		labelBottom = new MarqueeLabel( font, PATH_TEAMS, ScreenManager.SCREEN_WIDTH >> 2, MarqueeLabel.SCROLL_MODE_LEFT );
	}
	
	
	/**
	 * Verifica se o campeonato est� num est�gio que deve ser gravado (ou seja, n�o est� encerrado), e o grava caso necess�rio.
	 */
	public final void checkAndSaveGame() {
		switch ( currentStage ) {
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
			case STAGE_4TH_ROUND:
			case STAGE_5TH_ROUND:
			case STAGE_6TH_ROUND:
			case STAGE_FINAL_MATCH_1:
			case STAGE_FINAL_MATCH_2:
			case STAGE_FINAL_MATCH_3:
				saveGame();
			break;

			default:
				eraseSavedGame();
		}		
	}


	private final void saveGame() {
		final SavedGame save = new SavedGame( this );
		save.RMSMode = save.RMS_MODE_SAVE_GAME;

		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			System.out.println( "Erro ao gravar campeonato." );
			e.printStackTrace();
			//#endif
		}
	}


	private final void eraseSavedGame() {
		final SavedGame save = new SavedGame( this );
		save.RMSMode = save.RMS_MODE_ERASE_SAVED_GAME;

		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			System.out.println( "Erro ao apagar campeonato salvo." );
			e.printStackTrace();
			//#endif
		}
	}


	private static final void loadSavedGame( Championship c ) throws Exception {
		final SavedGame save = new SavedGame( c );
		save.RMSMode = save.RMS_MODE_LOAD_SAVED_GAME;			

		GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save );			
	}	
	
	
	/**
	 * Verifica qual a divis�o mais alta vencida, para que todos os personagens estejam dispon�veis.
	 */
	public static final void checkHighestDivision() {
		if ( highestDivisionWon == DIVISION_NONE ) {
			try {
				loadSavedGame( null );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
		}
	}

	
	private static final class SavedGame implements Serializable {
		
		private final byte RMS_MODE_SAVE_GAME			= 0;
		private final byte RMS_MODE_LOAD_SAVED_GAME		= 1;
		private final byte RMS_MODE_ERASE_SAVED_GAME	= 2;

		/** Modo de grava��o ou leitura atual no RMS. */
		private byte RMSMode;		

		private final Championship championship;
		
		
		private SavedGame( Championship c ) {
			championship = c;
		}
		
		
		public final void write( DataOutputStream output ) throws Exception {
			switch ( RMSMode ) {
				case RMS_MODE_SAVE_GAME:
					// armazena primeiro o progresso do jogador
					output.writeByte( highestDivisionWon );
					
					output.writeByte( championship.division );
					output.writeByte( championship.currentStage );

					// cada time grava suas informa��es
					for ( byte i = 0; i < TEAMS_IN_GROUP; ++i )
						championship.teams[ i ].write( output  );

					output.writeShort( championship.finalMatchScore[ 0 ] );
					output.writeShort( championship.finalMatchScore[ 1 ] );
				break;
				
				case RMS_MODE_ERASE_SAVED_GAME:
					// armazena a informa��o do progresso do jogador
					output.writeByte( highestDivisionWon );
					
					// indica que n�o h� jogo salvo
					output.writeByte( DIVISION_NO_SAVED_GAME );
				break;

				default:
					return;
			} // fim switch ( RMSMode )
		}


		public final void read( DataInputStream input ) throws Exception {
			switch ( RMSMode ) {
				case RMS_MODE_LOAD_SAVED_GAME:
					// l� o progresso do jogador
					highestDivisionWon = input.readByte();
					
					// l� a divis�o do campeonato salvo e carrega os times dessa divis�o
					championship.division = input.readByte();
					if ( championship.division == DIVISION_NO_SAVED_GAME ) {
						//#if DEBUG == "true"
							throw new Exception( "N�o h� jogo salvo (championship.division == DIVISION_NO_SAVED_GAME)." );
						//#else
//# 							throw new Exception();
						//#endif
					}
								
					championship.loadTeams();
					
					final byte stage = input.readByte();

					// cada time carrega suas informa��es
					for ( byte i = 0; i < TEAMS_IN_GROUP; ++i )
						championship.teams[ i ].read( input );
					
					// atualiza a tabela do grupo
					championship.sortTeams();
					
					// caso o jogo esteja salvo numa das partidas da final, atualiza as informa��es
					switch ( stage ) {
						case STAGE_FINAL_MATCH_1:
						case STAGE_FINAL_MATCH_2:
						case STAGE_FINAL_MATCH_3:
							championship.prepareFinalMatch();
							championship.finalMatchScore[ 0 ] = input.readShort();
							championship.finalMatchScore[ 1 ] = input.readShort();		
						break;
					}
					
					// depois ser� chamado o m�todo setStage(), para inicializar as vari�veis corretamente
					championship.currentStage = stage;
				break;
			}
		}
	} // fim da classe interna SavedGame

	
}
