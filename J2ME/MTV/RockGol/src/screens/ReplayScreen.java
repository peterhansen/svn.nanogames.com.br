/**
 * ReplayScreen.java
 * �2008 Nano Games.
 *
 * Created on Apr 3, 2008 1:10:37 PM.
 */

//#if JAR != "min"

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.Replay;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Calendar;
import java.util.Date;


/**
 * 
 * @author Peter
 */
public final class ReplayScreen extends UpdatableGroup implements Constants, MenuListener, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif

{
	
	//#if SCREEN_SIZE == "SMALL"
//# 	/** Quantidade total de replays salvos. */
//# 	public static final byte TOTAL_REPLAYS = 8;
	//#else
	/** Quantidade total de replays salvos. */
	public static final byte TOTAL_REPLAYS = 10;
	//#endif
	
	/** Quantidade de linhas de �cones dos replays. */
	private static final byte REPLAY_ICONS_ROWS = 2;
	
	/** Quantidade de �cones de replays por linha. */
	private static final byte REPLAY_ICONS_PER_ROW = TOTAL_REPLAYS / REPLAY_ICONS_ROWS;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private static final Replay[] replays = new Replay[ TOTAL_REPLAYS ];
	
	private final Label labelTitle;
	
	private final Menu menuReplays;
	
	private final RichLabel bottomPanel;
	
	private static final byte ICON_INDEX_DEFENDED	= 0;
	private static final byte ICON_INDEX_OUT		= 1;
	private static final byte ICON_INDEX_GOAL		= 2;
	private static final byte ICON_INDEX_POST_BACK	= 3;
	private static final byte ICON_INDEX_POST_OUT	= 4;
	private static final byte ICON_INDEX_POST_GOAL	= 5;
	private static final byte ICON_INDEX_EMPTY		= 6;
	
	private static final byte ICON_INDEX_TOTAL		= 7;
	
	private final DrawableImage[] icons = new DrawableImage[ ICON_INDEX_TOTAL ];
	
	/** Tela de replay ativa atualmente (SCREEN_SAVE_REPLAY ou SCREEN_LOAD_REPLAY). */
	private final byte screen;
	
	/** Refer�ncia para a tela de jogo atual (utilizada para salvar um replay). */
	private final Match match;
	
	private static final byte DISPLAY_MODE_REPLAY_INFO			= 0;
	private static final byte DISPLAY_MODE_REPLAY_SAVED			= 1;
	private static final byte DISPLAY_MODE_CONFIRM_OVERWRITE	= 2;
	
	private static final byte INDEX_CONFIRM_OVERWRITE_YES	= 1;
	private static final byte INDEX_CONFIRM_OVERWRITE_NO	= 2;
	
	/** Modo de visualiza��o atual. */
	private byte displayMode;
	
	/** Velocidade de movimenta��o do painel inferior, caso o texto n�o caiba por completo. */
	private final MUV bottomPanelSpeed = new MUV();
	
	/** Limite de movimenta��o do painel inferior. */
	private int bottomPanelLimit;
	
	/** Tempo de espera para reiniciar o scroll do painel inferior. */
	private static final short BOTTOM_PANEL_WAIT_TIME = 1666;
	
	private int bottomPanelWaitTime;
	
	private static final byte BOTTOM_PANEL_SCROLL_STOPPED	= 0;
	private static final byte BOTTOM_PANEL_SCROLL_MOVING	= 1;
	private static final byte BOTTOM_PANEL_SCROLL_WAITING	= 2;
	
	private byte bottomPanelScrollState;
	
	
	static {
		for ( byte i = 0; i < TOTAL_REPLAYS; ++i )
			replays[ i ] = new Replay();
	}
	
	
	public ReplayScreen( ImageFont font, Match match, int screen ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.screen = ( byte ) screen;
		this.match = match;
		
		int idTitle;
		switch ( screen ) {
			case SCREEN_SAVE_REPLAY:
				idTitle = TEXT_SAVE_REPLAY;
			break;
			
			case SCREEN_LOAD_REPLAY:
				idTitle = TEXT_LOAD_REPLAY;
			break;
			
			default:
				throw new IllegalArgumentException();
		}
		
		// insere o t�tulo
		labelTitle = new Label( font, GameMIDlet.getText( idTitle ) );
		insertDrawable( labelTitle  );
		
		// insere o menu de �cones dos replays
		final String path = PATH_IMAGES + "cursor_";
		for ( byte i = 0; i < ICON_INDEX_TOTAL; ++i )
			icons[ i ] = new DrawableImage( path + i + ".png" );
		
		final int ICON_WIDTH = icons[ 0 ].getWidth() + REPLAY_SCREEN_ICON_SPACING;
		final int ICON_HEIGHT = icons[ 0 ].getHeight() + REPLAY_SCREEN_ICON_SPACING;
		
		menuReplays = new Menu( this, DISPLAY_MODE_REPLAY_INFO, TOTAL_REPLAYS );
		menuReplays.setCircular( true );
		menuReplays.setSize( ( ICON_WIDTH * REPLAY_ICONS_PER_ROW ) + 10, ( ICON_HEIGHT * REPLAY_ICONS_ROWS ) + 10 );
		menuReplays.defineReferencePixel( ANCHOR_HCENTER );
		
		final byte INITIAL_POS = ( REPLAY_SCREEN_ICON_SPACING >> 1 ) + 5;
		for ( short i = 0, index = 0, x = INITIAL_POS, y = INITIAL_POS; i < REPLAY_ICONS_ROWS; ++i, x = INITIAL_POS, y += ICON_HEIGHT ) {
			for ( short j = 0; j < REPLAY_ICONS_PER_ROW; ++j, ++index, x += ICON_WIDTH ) {
				final DrawableImage icon = new DrawableImage( getReplayIcon( index ) );
				icon.setPosition( x, y );
				
				menuReplays.insertDrawable( icon );
			}
		}
		
		insertDrawable( menuReplays );
		
		bottomPanel = new RichLabel( font, null, size.x, null );
		insertDrawable( bottomPanel );
		
		GameMIDlet.setMenuCursor( menuReplays );
		setDisplayMode( DISPLAY_MODE_REPLAY_INFO );
	}
	
	
	private final DrawableImage getReplayIcon( int replayIndex ) {
		switch ( replays[ replayIndex ].playResult ) {
			case BALL_STATE_NONE:
				replayIndex = ICON_INDEX_EMPTY;
			break;
			
			case BALL_STATE_GOAL:
				replayIndex = ICON_INDEX_GOAL;
			break;
			
			case BALL_STATE_POST_GOAL:
				replayIndex = ICON_INDEX_POST_GOAL;
			break;
			
			case BALL_STATE_OUT:
				replayIndex = ICON_INDEX_OUT;
			break;
			
			case BALL_STATE_POST_OUT:
				replayIndex = ICON_INDEX_POST_OUT;
			break;
			
			case BALL_STATE_POST_BACK:
				replayIndex = ICON_INDEX_POST_BACK;
			break;
			
			case BALL_STATE_REJECTED_KEEPER:
				replayIndex = ICON_INDEX_DEFENDED;
			break;
			
			default:
				return null;
		}
		return icons[ replayIndex ];
	}
	
	
	public static final void saveReplays() {
		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_REPLAYS, new ReplayManager() );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}
	
	
	public static final void loadReplays() {
		try {
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_REPLAYS, new ReplayManager() );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}
	
	
	/**
	 * Classe interna utilizada para gravar e ler todos os replays.
	 */
	private static final class ReplayManager implements Serializable {

		public final void write( DataOutputStream output ) throws Exception {
			for ( byte i = 0; i < TOTAL_REPLAYS; ++i )
				replays[ i ].write( output );
		}


		public final void read( DataInputStream input ) throws Exception {
			for ( byte i = 0; i < TOTAL_REPLAYS; ++i )
				replays[ i ].read( input );			
		}
		
	} // fim da classe interna ReplayManager


	public final void keyPressed( int key ) {
		switch ( displayMode ) {
			case DISPLAY_MODE_REPLAY_INFO:
				switch ( key ) {
					case ScreenManager.KEY_NUM7:
						keyPressed( ScreenManager.DOWN );
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						menuReplays.previousIndex();
					break;

					case ScreenManager.KEY_NUM3:
						keyPressed( ScreenManager.UP );			
					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						menuReplays.nextIndex();
					break;

					case ScreenManager.KEY_NUM1:
						keyPressed( ScreenManager.LEFT );			
					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						menuReplays.setCurrentIndex( ( TOTAL_REPLAYS + menuReplays.getCurrentIndex() - REPLAY_ICONS_PER_ROW ) % TOTAL_REPLAYS );
					break;

					case ScreenManager.KEY_NUM9:
						keyPressed( ScreenManager.RIGHT );			
					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						menuReplays.setCurrentIndex( ( menuReplays.getCurrentIndex() + REPLAY_ICONS_PER_ROW ) % TOTAL_REPLAYS );
					break;

					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						menuReplays.keyPressed( ScreenManager.FIRE );
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						switch ( screen ) {
							case SCREEN_SAVE_REPLAY:
								GameMIDlet.setScreen( SCREEN_MATCH );
							break;

							case SCREEN_LOAD_REPLAY:
								GameMIDlet.setScreen( SCREEN_MAIN_MENU );
							break;
						}
					break;
				} // fim switch ( key )				
			break; // fim case DISPLAY_MODE_REPLAY_INFO
			
			case DISPLAY_MODE_CONFIRM_OVERWRITE:
				switch ( key ) {
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:						
						onChoose( null, 0, INDEX_CONFIRM_OVERWRITE_YES );
					break;
					
					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						setDisplayMode( DISPLAY_MODE_REPLAY_INFO );
					break;
				} // fim switch ( key )					
			break;
			
			case DISPLAY_MODE_REPLAY_SAVED:
				GameMIDlet.setScreen( SCREEN_MATCH );
			break;
		}
	}


	public final void keyReleased( int key ) {
	}


	//#if SCREEN_SIZE != "SMALL"
	
	public final void onPointerDragged( int x, int y ) {
		final int index = getReplayIndexAt( x, y );

		if ( index >= 0 )
			menuReplays.setCurrentIndex( index );
	}


	public final void onPointerPressed( int x, int y ) {
		final int index = getReplayIndexAt( x, y );
		if ( index >= 0 ) {
			if ( menuReplays.getCurrentIndex() == index )
				keyPressed( ScreenManager.FIRE );
			else
				menuReplays.setCurrentIndex( index );
		} 
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	
	//#endif


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( screen ) {
			case SCREEN_SAVE_REPLAY:
				switch ( displayMode ) {
					case DISPLAY_MODE_REPLAY_INFO:
						if ( replays[ index ].playResult == BALL_STATE_NONE ) {
							saveReplay( index );
							
							setDisplayMode( DISPLAY_MODE_REPLAY_SAVED );
						} else {
							// pede para confirmar no caso de slot j� estar gravado
							setDisplayMode( DISPLAY_MODE_CONFIRM_OVERWRITE );
						}
					break;
					
					case DISPLAY_MODE_CONFIRM_OVERWRITE:
						switch ( index ) {
							case INDEX_CONFIRM_OVERWRITE_YES:
								saveReplay( menuReplays.getCurrentIndex() );
								
								setDisplayMode( DISPLAY_MODE_REPLAY_SAVED );
							break;
						}
					break;
				}
			break;
		
			case SCREEN_LOAD_REPLAY:
				if ( replays[ index ].playResult != BALL_STATE_NONE )
					GameMIDlet.playMatch( replays[ index ] );
			break;
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		// atualiza informa��o do replay selecionado atualmente
		if ( id == DISPLAY_MODE_REPLAY_INFO )
			updateReplayInfoLabel();
		
		GameMIDlet.updateCursor( menu.getDrawable( menu.getCurrentIndex() ) );
	}
	
	
	private final void saveReplay( int index ) {
		match.saveReplayData( replays[ index ] );
		saveReplays();

		// atualiza o �cone do replay
		try {
			final DrawableImage icon = new DrawableImage( getReplayIcon( index ) );
			final Point pos = menuReplays.removeDrawable( index ).getPosition();
			icon.setPosition( pos );
			menuReplays.insertDrawable( icon, index );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}		
	}
	
	
	private final int getReplayIndexAt( int x, int y ) {
		if ( menuReplays.contains( x, y ) ) {
			x -= menuReplays.getPosX();
			y -= menuReplays.getPosY();

			for ( byte i = 0; i < TOTAL_REPLAYS; ++i ) {
				if ( menuReplays.getDrawable( i ).contains( x, y ) )
					return i;
			}
		}
		
		return -1;
	}
	
	
	private final void setDisplayMode( byte displayMode ) {
		switch ( displayMode ) {
			case DISPLAY_MODE_REPLAY_INFO:
				if ( screen == SCREEN_SAVE_REPLAY )
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_SAVE );
				else
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_LOAD );
				
				updateReplayInfoLabel();
				
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
			break;
			
			case DISPLAY_MODE_CONFIRM_OVERWRITE:
				bottomPanel.setText( TEXT_CONFIRM_OVERWRITE );
				
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_YES );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_NO );
				checkBottomPanelHeight();
			break;
			
			case DISPLAY_MODE_REPLAY_SAVED:
				bottomPanel.setText( TEXT_REPLAY_SAVED );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
				checkBottomPanelHeight();
			break;
		}
		
		this.displayMode = displayMode;
	}


	private final void updateReplayInfoLabel() {
		final StringBuffer text = new StringBuffer( 100 );
		
		text.append( GameMIDlet.getText( TEXT_REPLAY_NUMBER ) );
		text.append( ( menuReplays.getCurrentIndex() + 1 ) );
		text.append( '\n' );
		
		final Replay replay = replays[ menuReplays.getCurrentIndex() ];
		if ( replay.playResult == BALL_STATE_NONE ) {
			// o replay est� vazio
			text.append( GameMIDlet.getText( TEXT_EMPTY ) );
		} else {
			// mostra as informa��es do replay salvo
			text.append( GameMIDlet.getText( TEXT_TEAM_NAME_CLESTON + replay.shooterIndex ) );
			text.append( " X " );
			text.append( GameMIDlet.getText( TEXT_TEAM_NAME_CLESTON + replay.keeperIndex ) );
			text.append( '\n' );
			
			switch ( replay.playResult ) {
				case BALL_STATE_GOAL:
					text.append( GameMIDlet.getText( TEXT_GOAL ) );
				break;
				
				case BALL_STATE_POST_GOAL:
					text.append( GameMIDlet.getText( TEXT_POST_GOAL ) );
				break;
				
				case BALL_STATE_OUT:
					text.append( GameMIDlet.getText( TEXT_OUT ) );
				break;
				
				case BALL_STATE_POST_OUT:
					text.append( GameMIDlet.getText( TEXT_POST_OUT ) );
				break;
				
				case BALL_STATE_POST_BACK:
					text.append( GameMIDlet.getText( TEXT_POST_BACK ) );
				break;
				
				case BALL_STATE_REJECTED_KEEPER:
					text.append( GameMIDlet.getText( TEXT_DEFENDED ) );
				break;
			}
			text.append( '\n' );
			
			// formata a data
			final Date date = new Date( replay.saveTime );
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime( date );
			
			int temp;
			
			text.append( GameMIDlet.getText( TEXT_SUNDAY + calendar.get( Calendar.DAY_OF_WEEK ) - 1 ) );
			text.append( ' ' );
			
			temp = calendar.get( Calendar.DAY_OF_MONTH );
			if ( temp < 10 )
				text.append( '0' );
			text.append( temp );
			
			text.append( '/' );
			
			temp = calendar.get( Calendar.MONTH ) + 1;
			if ( temp < 10 )
				text.append( '0' );			
			text.append( temp );
			
			text.append( '/' );
			text.append( calendar.get( Calendar.YEAR ) );
			text.append( ' ' );
			
			temp = calendar.get( Calendar.HOUR_OF_DAY );
			if ( temp < 10 )
				text.append( '0' );			
			text.append( temp );
			text.append( ':' );
			
			temp = calendar.get( Calendar.MINUTE );
			if ( temp < 10 )
				text.append( '0' );			
			text.append( temp );	
		}
		
		bottomPanel.setText( text.toString() );
		checkBottomPanelHeight();
	} // fim do m�todo updateReplayInfoLabel()
	
	
	private final void checkBottomPanelHeight() {
		final int textTotalHeight = bottomPanel.getTextTotalHeight();
		if ( textTotalHeight > bottomPanel.getHeight() ) {
			// as informa��es do replay n�o couberam na parte inferior da tela
			bottomPanelLimit = bottomPanel.getHeight() - textTotalHeight;
			
			bottomPanelScrollState = BOTTOM_PANEL_SCROLL_WAITING;
			bottomPanelWaitTime = BOTTOM_PANEL_WAIT_TIME;
		} else {
			bottomPanelScrollState = BOTTOM_PANEL_SCROLL_STOPPED;
		}
		bottomPanel.setTextOffset( 0 );		
	}


	public final void update( int delta ) {
		super.update( delta );
		
		final int FONT_HEIGHT = GameMIDlet.getFont( FONT_INDEX_DEFAULT ).getHeight();
		switch ( bottomPanelScrollState ) {
			case BOTTOM_PANEL_SCROLL_MOVING:
				bottomPanel.setTextOffset( bottomPanel.getTextOffset() + bottomPanelSpeed.updateInt( delta ) );
				
				if ( bottomPanel.getTextOffset() < bottomPanelLimit ) {
					bottomPanel.setTextOffset( bottomPanelLimit );
					bottomPanelScrollState = BOTTOM_PANEL_SCROLL_WAITING;
					bottomPanelWaitTime = BOTTOM_PANEL_WAIT_TIME;
					bottomPanelSpeed.setSpeed( FONT_HEIGHT );
				} else if ( bottomPanel.getTextOffset() > 0 ) {
					bottomPanel.setTextOffset( 0 );
					bottomPanelScrollState = BOTTOM_PANEL_SCROLL_WAITING;
					bottomPanelWaitTime = BOTTOM_PANEL_WAIT_TIME;
					bottomPanelSpeed.setSpeed( -FONT_HEIGHT );
				}
			break;
			
			case BOTTOM_PANEL_SCROLL_WAITING:
				bottomPanelWaitTime -= delta;
				if ( bottomPanelWaitTime <= 0 ) { 
					bottomPanelScrollState = BOTTOM_PANEL_SCROLL_MOVING;
					bottomPanelSpeed.setSpeed( bottomPanel.getTextOffset() >= 0 ? -FONT_HEIGHT : FONT_HEIGHT );
				}
			break;
		}
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		final ImageFont font = GameMIDlet.getFont( FONT_INDEX_DEFAULT );
		
		labelTitle.setPosition( ( size.x - labelTitle.getWidth() ) >> 1, 0 );
		menuReplays.setRefPixelPosition( size.x >> 1, font.getHeight() );
		
		//#if JAR == "full"
		if ( GameMIDlet.isLowMemory() )
	 		bottomPanel.setSize( size.x, size.y - menuReplays.getHeight() - menuReplays.getPosY() - GameMIDlet.getFont( FONT_INDEX_TEXT ).getHeight() );
		else
			bottomPanel.setSize( size.x, size.y - menuReplays.getHeight() - menuReplays.getPosY() );
		//#else
//# 		bottomPanel.setSize( size.x, size.y - menuReplays.getHeight() - menuReplays.getPosY() - GameMIDlet.getFont( FONT_INDEX_TEXT ).getHeight() );
		//#endif
		bottomPanel.setPosition( 0, menuReplays.getPosY() + menuReplays.getHeight() );
		
		updateReplayInfoLabel();
	}
	
	
}

//#endif