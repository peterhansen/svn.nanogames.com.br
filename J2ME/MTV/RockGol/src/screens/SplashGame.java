/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;

//#if SCREEN_SIZE != "SMALL"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public final class SplashGame extends DrawableGroup implements Constants, Updatable, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif
{
	
	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 20;
	
	
	//#if SCREEN_SIZE == "MEDIUM"
	private static final byte IMG_CLESTON_X = -19;
	private static final byte IMG_CLESTON_Y = 94;
	
	private static final byte IMG_PENALTI_X = IMG_CLESTON_X + 56;
	private static final byte IMG_PENALTI_Y = IMG_CLESTON_Y - 56;
	
	private static final byte IMG_MTV_X = IMG_CLESTON_X + 56;
	private static final byte IMG_MTV_Y = IMG_CLESTON_Y - 28;
	
	private static final byte IMG_CLESTON_LOGO_X = IMG_CLESTON_X + 49;
	private static final short IMG_CLESTON_LOGO_Y = IMG_CLESTON_Y + 60;
	
	private static final byte IMG_BALL_X = 27;
	
	//#elif JAR != "min"
//# 	
//# 	private static final byte IMG_OFFSET_X = 4;
//# 	
//# 	private static final byte IMG_PENALTI_OFFSET_Y = -74;
//# 	private static final byte IMG_MTV_OFFSET_Y = -54;
//# 	
	//#endif	
	
	
	/** tempo restante de exposi��o da tela de splash do jogo */
	private short visibleTime = 2700;
	
	/** texto indicando "pressione qualquer tecla" */
	private final RichLabel pressKeyLabel;
	
	/** velocidade do "pisca-pisca" do label */
	private final short BLINK_RATE = -666; // dem�in! >:)
	
	
	public SplashGame( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		//#if SCREEN_SIZE == "SMALL"
			//#if JAR == "min"
//# 		
//# 			final Pattern bkg = new Pattern( 0x000000 );
//# 			bkg.setSize( size );
//# 			insertDrawable( bkg );
//# 			
//# 			final DrawableImage logoMTV = new DrawableImage( PATH_SPLASH + "mtv.png" );
//# 			logoMTV.setPosition( ( size.x - logoMTV.getWidth() >> 1 ), ( size.y - logoMTV.getHeight() ) >> 1 );
//# 			insertDrawable( logoMTV );
//# 
//# 			// aloca o texto "PENALTI"
//# 			final DrawableImage titlePenalti = new DrawableImage( PATH_SPLASH + "penalti.png" );
//# 			titlePenalti.setPosition( ( size.x - titlePenalti.getWidth() >> 1 ), logoMTV.getPosY() - titlePenalti.getHeight() );			
//# 			insertDrawable( titlePenalti );
//# 
//# 			// aloca o texto "com Cleeeston"
//# 			final DrawableImage clestonLogo = new DrawableImage( PATH_SPLASH + "cleston_logo.png" );
//# 			clestonLogo.setPosition( ( size.x - clestonLogo.getWidth() >> 1 ), logoMTV.getPosY() + logoMTV.getHeight() );			
//# 			insertDrawable( clestonLogo );
//# 
//# 			// aloca a imagem do Cleston
//# 			final DrawableImage cleston = new DrawableImage( PATH_SHOOTERS + "0_0.png" );
//# 			cleston.setPosition( 0, size.y - cleston.getHeight() );			
//# 			insertDrawable( cleston );
//# 			
			//#else
//# 		
//# 			final DrawableImage splash = new DrawableImage( PATH_SPLASH + "splash.png" );
//# 			splash.setPosition( ( size.x - splash.getWidth() ) >> 1, ( size.y - splash.getHeight() ) >> 1 );
//# 			insertDrawable( splash );
//# 
//# 			// aloca a imagem do Cleston
//# 			final DrawableImage cleston = new DrawableImage( PATH_SPLASH + "cleston.png" );
//# 			cleston.setPosition( 0, size.y - cleston.getHeight() );
//# 
//# 			// aloca o texto "com Cleeeston"
//# 			final DrawableImage clestonLogo = new DrawableImage( PATH_SPLASH + "cleston_logo.png" );
//# 			clestonLogo.setPosition( IMG_OFFSET_X + ( ( size.x - clestonLogo.getWidth() ) >> 1 ), size.y - clestonLogo.getHeight() );
//# 
//# 			// insere o logo da MTV
//# 			final DrawableImage titleMTV = new DrawableImage( PATH_SPLASH + "mtv_splash.png" );
//# 			titleMTV.setPosition( IMG_OFFSET_X + ( ( size.x - titleMTV.getWidth() ) >> 1 ), clestonLogo.getPosY() + IMG_MTV_OFFSET_Y );
//# 
//# 			// aloca o texto "PENALTI"
//# 			final DrawableImage titlePenalti = new DrawableImage( PATH_SPLASH + "penalti.png" );
//# 			titlePenalti.setPosition( titleMTV.getPosX(), clestonLogo.getPosY() + IMG_PENALTI_OFFSET_Y );
//# 
//# 			insertDrawable( titlePenalti );
//# 			insertDrawable( titleMTV );
//# 			insertDrawable( cleston );
//# 			insertDrawable( clestonLogo );
//# 		
			//#endif
//# 		
		//#else
		
		final int OFFSET_X = ( size.x - SCREEN_DEFAULT_WIDTH ) >> 1;
 		final int OFFSET_Y = ( size.y - SCREEN_DEFAULT_HEIGHT ) >> 1;
 		final int OFFSET_Y2 = ScreenManager.SCREEN_HEIGHT >= SCREEN_SPLASH_HEIGHT_MIN ? OFFSET_Y : -IMG_PENALTI_Y;
		
		// insere a parte central do background (areia e bola)
		final DrawableImage bkgCenter = new DrawableImage( PATH_SPLASH + "bkg_center.png" );
		bkgCenter.setPosition( OFFSET_X, OFFSET_Y + SCREEN_DEFAULT_HEIGHT - bkgCenter.getHeight() );
		
		insertDrawable( bkgCenter );
		
		// verifica se a tela � maior que as dimens�es padr�o, e insere mais imagens caso necess�rio.
		if ( size.x > SCREEN_DEFAULT_WIDTH ) {
			// insere imagens nas laterais
			final DrawableImage bkgLeft1 = new DrawableImage( PATH_SPLASH + "bkg_left1.png" );
			bkgLeft1.setPosition( bkgCenter.getPosX() - bkgLeft1.getWidth(), bkgCenter.getPosY() );
			insertDrawable( bkgLeft1 );

			final DrawableImage bkgRight1 = new DrawableImage( PATH_SPLASH + "bkg_right1.png" );
			bkgRight1.setPosition( OFFSET_X + SCREEN_DEFAULT_WIDTH, bkgCenter.getPosY() );
			insertDrawable( bkgRight1 );
			
			//#if JAR == "full"
			if ( size.x > SCREEN_SPLASH_WIDTH_BIG ) {
				final DrawableImage bkgLeft2 = new DrawableImage( PATH_SPLASH + "bkg_left2.png" );
				bkgLeft2.setPosition( 0, bkgLeft1.getPosY() );
				insertDrawable( bkgLeft2 );

				final DrawableImage bkgRight2 = new DrawableImage( PATH_SPLASH + "bkg_right2.png" );
				bkgRight2.setPosition( bkgRight1.getPosX() + bkgRight1.getWidth(), bkgRight1.getPosY() );
				insertDrawable( bkgRight2 );				
			}
			//#endif
		}
		
		if ( size.y > SCREEN_DEFAULT_HEIGHT ) {
			// insere imagem abaixo
			final DrawableImage bkgBottom = new DrawableImage( PATH_SPLASH + "bkg_bottom_c.png" );
			bkgBottom.setPosition( ( size.x - bkgBottom.getWidth() ) >> 1, bkgCenter.getPosY() + bkgCenter.getHeight() );
			insertDrawable( bkgBottom );
			
			if ( size.x > SCREEN_DEFAULT_WIDTH ) {
				final DrawableImage bkgBottomLeft1 = new DrawableImage( PATH_SPLASH + "bkg_bl1.png" );
				bkgBottomLeft1.setPosition( 0, bkgBottom.getPosY() );
				insertDrawable( bkgBottomLeft1 );

				final DrawableImage bkgBottomRight1 = new DrawableImage( PATH_SPLASH + "bkg_br1.png" );
				bkgBottomRight1.setPosition( bkgBottom.getPosX() + bkgBottom.getWidth(), bkgBottom.getPosY() );
				insertDrawable( bkgBottomRight1 );
				
				//#if JAR == "full"
				if ( size.x > SCREEN_SPLASH_WIDTH_BIG ) {
					final DrawableImage bkgBottomLeft2 = new DrawableImage( PATH_SPLASH + "bkg_bl2.png" );
					bkgBottomLeft2.setPosition( bkgBottomLeft1.getPosX() + bkgBottomLeft1.getWidth(), bkgBottomLeft1.getPosY() );
					insertDrawable( bkgBottomLeft2 );

					final DrawableImage bkgBottomRight2 = new DrawableImage( PATH_SPLASH + "bkg_br2.png" );
					bkgBottomRight2.setPosition( bkgBottomRight1.getPosX() + bkgBottomRight1.getWidth(), bkgBottomRight1.getPosY() );
					insertDrawable( bkgBottomRight2 );
				}
				//#endif
			}
		}
		
		// insere o pattern do fundo verde
		final DrawableImage imgGreen = new DrawableImage( PATH_SPLASH + "green.png" );
		imgGreen.defineReferencePixel( 0, imgGreen.getHeight() - bkgCenter.getPosY() );
		final Pattern patternGreen = new Pattern( imgGreen );
		patternGreen.setSize( size.x, bkgCenter.getPosY() );
		insertDrawable( patternGreen );
		
		// insere a parte superior da bola
		final DrawableImage imgBall = new DrawableImage( PATH_SPLASH + "bkg_ball.png" );
		imgBall.setPosition( OFFSET_X + IMG_BALL_X, bkgCenter.getPosY() - imgBall.getHeight() );
		insertDrawable( imgBall );
		
		// insere o texto "PENALTI"
		final DrawableImage titlePenalti = new DrawableImage( PATH_SPLASH + "penalti.png" );
		titlePenalti.setPosition( OFFSET_X + IMG_PENALTI_X, OFFSET_Y2 + IMG_PENALTI_Y );
		insertDrawable( titlePenalti );
	
		// insere o logo da MTV
		final DrawableImage titleMTV = new DrawableImage( PATH_SPLASH + "mtv_splash.png" );
		titleMTV.setPosition( OFFSET_X + IMG_MTV_X, OFFSET_Y2 + IMG_MTV_Y );
		insertDrawable( titleMTV );
		
		// insere a imagem do Cleston
		final DrawableImage cleston = new DrawableImage( PATH_SPLASH + "cleston.png" );
		cleston.setPosition( OFFSET_X + IMG_CLESTON_X, OFFSET_Y2 + IMG_CLESTON_Y );
		insertDrawable( cleston );
		
		// insere o texto "com Cleeeston"
		final DrawableImage clestonLogo = new DrawableImage( PATH_SPLASH + "cleston_logo.png" );
		clestonLogo.setPosition( OFFSET_X + IMG_CLESTON_LOGO_X, OFFSET_Y2 + IMG_CLESTON_LOGO_Y );
		insertDrawable( clestonLogo );		
		
		//#endif
		
		//#if JAR != "min"
		// insere o logo Nano Games
		final DrawableImage nano = new DrawableImage( PATH_SPLASH + "nano_2.png" );
		nano.setPosition( size.x - nano.getWidth(), size.y - nano.getHeight() );
		insertDrawable( nano );
		//#endif
		
		pressKeyLabel = new RichLabel( font, AppMIDlet.getText( TEXT_PRESS_ANY_KEY ), ( size.x * 3 ) >> 2, null );
		pressKeyLabel.setPosition( ( size.x - pressKeyLabel.getSize().x ) >> 1, size.y - pressKeyLabel.getSize().y );
		pressKeyLabel.setVisible( false );
		insertDrawable( pressKeyLabel );
	}


	public final void update( int delta ) {
		visibleTime -= delta;
		
		if ( visibleTime <= BLINK_RATE ) {
			// pisca o texto "Pressione qualquer tecla"
			pressKeyLabel.setVisible( !pressKeyLabel.isVisible() );
			visibleTime %= BLINK_RATE;
		}
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
		// permite pular a tela de splash
		visibleTime = 0;
		//#endif
		
		if ( visibleTime <= 0 ) {
			//#if DEMO == "true"
//# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
			//#else
			GameMIDlet.setScreen( SCREEN_LOADING_2 );
			//#endif
		}
	}


	public final void keyReleased( int key ) {
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif

}
