/**
 * GameMIDlet.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Ball;
import core.Constants;
import core.Cray;
import core.Team;
import javax.microedition.midlet.MIDletStateChangeException;

//#if JAR == "full"
import core.BlackBoard;
//#endif

//#if JAR != "min"
import core.Replay;
import core.AnimatedSoftkey;
//#endif

import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener {
	
	private static final byte GAME_MAX_FRAME_TIME = 96;
	
	private static final byte BACKGROUND_TYPE_BLACKBOARD	= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;
	
	
	private static Cray cursor;
	
 	private static ImageFont[] FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	
	//#if JAR != "min"
	/** gerenciador da anima��o da soft key esquerda */
	private static AnimatedSoftkey softkeyLeft;	
	
	/** gerenciador da anima��o da soft key direita */
	private static AnimatedSoftkey softkeyRight;	
	//#endif
	
	//#if JAR == "full"
	/** Quadro negro usado como fundo de tela. */
 	private static BlackBoard blackboard;
	/** Limite m�nimo de mem�ria para que comece a haver cortes nos recursos. */
	private static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif
	
	/** Refer�ncia para a tela de jogo, para que seja poss�vel retornar � tela de jogo ap�s entrar na tela de pausa. */
	private static Match match;
	
	/** Refer�ncia para a tela de campeonato, para que seja poss�vel retornar a ela ap�s uma partida. */
	private static Championship championship;
	
	private static boolean lowMemory;
	
	private static LoadListener loader;
	

	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
		super( -1, GAME_MAX_FRAME_TIME );
		//#endif
	}

	
	protected final void loadResources() throws Exception {		
		//#if JAR == "full"
		switch ( getVendor() ) {
			case VENDOR_SONYERICSSON:
			case VENDOR_NOKIA:
			case VENDOR_SIEMENS:
			break;

			default:
				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
		//#else
//# 			lowMemory = true;
		//#endif
//		lowMemory = true; // teste
		
		//#if SCREEN_SIZE == "SMALL"
//# 		FONTS[ FONT_INDEX_DEFAULT ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
//# 		FONTS[ FONT_INDEX_DEFAULT ].setCharOffset( DEFAULT_FONT_OFFSET );			
		//#else

			//#if JAR == "full"
			if ( isLowMemory() || ScreenManager.SCREEN_HEIGHT <= SCREEN_DEFAULT_HEIGHT ) {
				FONTS[ FONT_INDEX_DEFAULT ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_text.png", PATH_IMAGES + "font_text.dat" );
				FONTS[ FONT_INDEX_DEFAULT ].setCharOffset( DEFAULT_FONT_OFFSET );
			} else {
				FONTS[ FONT_INDEX_DEFAULT ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
				FONTS[ FONT_INDEX_DEFAULT ].setCharOffset( DEFAULT_FONT_OFFSET );	
			}
			//#else
//# 				FONTS[ FONT_INDEX_DEFAULT ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_text.png", PATH_IMAGES + "font_text.dat" );
//# 				FONTS[ FONT_INDEX_DEFAULT ].setCharOffset( DEFAULT_FONT_OFFSET );
			//#endif
		//#endif
		
		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}
		
		loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts.dat" );
		
		setScreen( SCREEN_LOADING_1 );
	} // fim do m�todo loadResources()
	

	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( championship != null ) {
			championship.checkAndSaveGame();
			championship = null;
		}
		
		super.destroyApp( unconditional );
	}
	

	protected final int changeScreen( int screen ) throws Exception {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_BLACKBOARD;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;	
		
		boolean fitInBoard = true;
		
		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				final BasicConfirmScreen confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, false );
				if ( !MediaPlayer.isMuted() )
					confirm.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
				
				setMenuCursor( confirm );

				nextScreen = confirm;
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_SPLASH_NANO:
				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "MEDIUM"
				nextScreen = new BasicSplashNano( SCREEN_SPLASH_MTV, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH );				
				//#else
					//#if JAR == "min"
//# 					nextScreen = new BasicSplashNano( SCREEN_SPLASH_MTV, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, ( byte ) -1 );				
					//#else
//# 					nextScreen = new BasicSplashNano( SCREEN_SPLASH_MTV, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH );				
					//#endif
				//#endif
				
				fitInBoard = false;
			break;
			
			case SCREEN_SPLASH_MTV:
				bkgType = BACKGROUND_TYPE_NONE;
				nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_SPLASH + "mtv.png", TEXT_SPLASH_MTV );

				fitInBoard = false;
			break;
			
			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame( getFont( FONT_INDEX_DEFAULT ) );
				bkgType = BACKGROUND_TYPE_NONE;
				
				fitInBoard = false;
			break;
			
			case SCREEN_MAIN_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_NEW_GAME,
					TEXT_OPTIONS,
					
					//#if JAR != "min"
					TEXT_REPLAYS,
					//#endif

					//#if DEMO == "true"
//# 					TEXT_BUY_FULL_GAME_TITLE,
					//#endif

					TEXT_HELP,
					TEXT_CREDITS,

					TEXT_EXIT,

					//#if DEBUG == "true"
					TEXT_LOG_TITLE,
					//#endif							
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_EXIT;				
			break;
			
			case SCREEN_NEW_GAME_MENU:
				//#if DEBUG == "true"
				System.gc();
				long freeMem = Runtime.getRuntime().freeMemory();
				//#endif
				unloadMatch();
				championship = null;
				System.gc();
				//#if DEBUG == "true"
				freeMem = Runtime.getRuntime().freeMemory() - freeMem;
				texts[ TEXT_LOG ] += "diff: " + freeMem + "\n";
				//#endif
				
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_TRAINING,
					TEXT_CHAMPIONSHIP,
					TEXT_VERSUS,
					TEXT_BACK
				} );
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;						
			break;
			
			case SCREEN_CHAMPIONSHIP_MENU:
				int divisionsAvailable;
				
				switch ( Championship.getHighestDivisionWon() ) {
					case DIVISION_3:
						divisionsAvailable = 2;
					break;
					
					case DIVISION_2:
					case DIVISION_1:
						divisionsAvailable = DIVISIONS_TOTAL;
					break;
					
					default:
						divisionsAvailable = 1;
				}
				
				final int[] entries = new int[ divisionsAvailable + 1 ];
				for ( byte i = 0; i < divisionsAvailable; ++i )
					entries[ i ] = TEXT_3RD_DIVISION + i;
				entries[ divisionsAvailable ] = TEXT_BACK;
				
				nextScreen = createBasicMenu( screen, entries );
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;							
			break;
			
			case SCREEN_SAVED_GAME_FOUND:
				final BasicConfirmScreen confirmScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_SAVED_GAME_FOUND, TEXT_YES, TEXT_NO, false );
				confirmScreen.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
				setMenuCursor( confirmScreen );
				
				nextScreen = confirmScreen;
				
				indexSoftLeft = TEXT_OK;
			break;		
			
			case SCREEN_CHAMPIONSHIP:
				//#if JAR == "full"
				if ( !isLowMemory() )
					blackboard.setSize( championship.getSize() );
				//#endif
				
				nextScreen = championship;
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
				
				fitInBoard = false;
			break;
			
			case SCREEN_MATCH:
				nextScreen = match;
				match.setSoftKeyLabel();
				bkgType = BACKGROUND_TYPE_NONE;
				
				fitInBoard = false;
				indexSoftRight = SOFT_KEY_DONT_CHANGE;
			break;
			
			//#if JAR != "min"
			case SCREEN_SAVE_REPLAY:
			case SCREEN_LOAD_REPLAY:
				nextScreen = new ReplayScreen( getFont( FONT_INDEX_DEFAULT ), match, screen );
				indexSoftLeft = SOFT_KEY_DONT_CHANGE;
				indexSoftRight = SOFT_KEY_DONT_CHANGE;
			break;
			//#endif
			
			case SCREEN_OPTIONS:
				BasicOptionsScreen optionsScreen = null;
				
				if ( MediaPlayer.isVibrationSupported() ) {
					optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
				}
				nextScreen = optionsScreen;
				
				setMenuCursor( optionsScreen );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_HELP_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_OBJECTIVES,
					TEXT_CONTROLS,
					TEXT_TIPS,
					TEXT_BACK,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
			case SCREEN_HELP_TIPS:
				nextScreen = new BasicTextScreen( SCREEN_HELP_MENU, getFont( FONT_INDEX_TEXT ), getText( TEXT_HELP_OBJECTIVES + ( screen - SCREEN_HELP_OBJECTIVES ) ) + getVersion(), false, null );

				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_DEFAULT ), TEXT_CREDITS_TEXT, true );

				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_PAUSE:
				BasicOptionsScreen pauseScreen = null;
				
				if ( MediaPlayer.isVibrationSupported() ) {
					pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
					
				}
				nextScreen = pauseScreen;

				setMenuCursor( pauseScreen );
				
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;				
			break;
			
			case SCREEN_CONFIRM_MENU:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO, false );
				setMenuCursor( ( Menu ) nextScreen );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT:
				final int TEXT_EXIT_INDEX = TEXT_CONFIRM_EXIT_1 + NanoMath.randInt( TEXT_CONFIRM_EXIT_TOTAL );
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_DEFAULT ), null, TEXT_EXIT_INDEX, TEXT_YES, TEXT_NO, false );
				setMenuCursor( ( Menu ) nextScreen );
				indexSoftLeft = TEXT_OK;
			break;
			
			//#if DEMO == "true"
//# 			
//# 			// vers�o demo
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, midlet.cursor, TEXT_BUY_FULL_GAME_TEXT, TEXT_YES, TEXT_NO );
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
//# 			case SCREEN_PLAYS_REMAINING:
//# 				final String text = playsRemaining > 1 ? 
//# 						getText( TEXT_2_OR_MORE_PLAYS_REMAINING_1 ) + playsRemaining + getText( TEXT_2_OR_MORE_PLAYS_REMAINING_2 ) :
//# 						getText( TEXT_1_PLAY_REMAINING );
//# 				nextScreen = new BasicTextScreen( midlet, index, FONT_DEFAULT, text, true );
//# 						
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
			//#endif
			
			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen( 
					new LoadListener() {
						public final void load() throws Exception {
							// aloca a fonte do placar
							String path = PATH_IMAGES + "font_score";
							FONTS[ FONT_INDEX_BOARD ] = ImageFont.createMultiSpacedFont( path + ".png", path + ".dat" );

							//#if SCREEN_SIZE == "SMALL"
//# 											FONTS[ FONT_INDEX_TEXT ] = FONTS[ FONT_INDEX_DEFAULT ];
							//#else
							if ( isLowMemory() ) {
								FONTS[ FONT_INDEX_TEXT ] = FONTS[ FONT_INDEX_DEFAULT ];
							} else {
								path = PATH_IMAGES + "font_text";
								FONTS[ FONT_INDEX_TEXT ] = ImageFont.createMultiSpacedFont( path + ".png", path + ".dat" );
								FONTS[ FONT_INDEX_TEXT ].setCharOffset( DEFAULT_FONT_OFFSET );
							}
							//#endif


							// aloca os sons
							final String[] soundList = new String[ SOUND_TOTAL ];
							for ( byte i = 0; i < SOUND_TOTAL; ++i )
								soundList[ i ] = PATH_SOUNDS + i + ".mid";

							MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );

							cursor = new Cray();

							//#if JAR != "min"
							ReplayScreen.loadReplays();

							softkeyLeft = new AnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
							softkeyRight = new AnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );	
							//#if JAR == "full"
							if ( !isLowMemory())
								blackboard = new BlackBoard();
							//#endif

							final ScreenManager manager = ScreenManager.getInstance();
							manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
							manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );	
							//#endif
							
							setScreen( SCREEN_CHOOSE_SOUND );
						}
					}, getFont( FONT_INDEX_DEFAULT ), TEXT_LOADING );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;
			
			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {
						public final void load() throws Exception {
							Ball.loadImages();
							
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

							// pr�-carrega algumas das imagens sempre utilizadas na tela de jogo
							Match.loadImages();
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

							// pr�-carrega algumas das imagens utilizadas na tela de campeonato
							Championship.loadResources();
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
							
							setScreen( SCREEN_MAIN_MENU );
						}
					}, getFont( FONT_INDEX_DEFAULT ), TEXT_LOADING );
				
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;
			
			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen( loader, getFont( FONT_INDEX_DEFAULT ), TEXT_LOADING );
				loader = null;
				
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;
			
			case SCREEN_CHOOSE_PLAYER_TRAINING:
			case SCREEN_CHOOSE_PLAYER_2_PLAYERS:
				// verifica qual a divis�o mais alta salva, para que todos os jogadores estejam dispon�veis
				Championship.checkHighestDivision();
				nextScreen = new ChoosePlayerScreen( screen );
				
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;
			
			//#if DEBUG == "true"
			case SCREEN_ERROR_LOG:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_TEXT ), texts[ TEXT_LOG ], false );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				indexSoftRight = TEXT_BACK;
			break;
			//#endif
		} // fim switch ( screen )
		
		
		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );	
		
		setBackground( bkgType );
		
		//#if JAR != "full"
//# 		if ( fitInBoard ) {
//# 			if ( nextScreen.getWidth() > 0 && nextScreen.getHeight() > 0 )
//# 				nextScreen.setSize( Math.min( ScreenManager.SCREEN_WIDTH, nextScreen.getWidth() ), Math.min( ScreenManager.SCREEN_HEIGHT, nextScreen.getHeight() ) );
//# 			else
//# 				nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//# 		}
		//#else
		if ( fitInBoard ) {
			if ( blackboard == null ) {
				if ( nextScreen.getWidth() > 0 && nextScreen.getHeight() > 0 )
					nextScreen.setSize( Math.min( ScreenManager.SCREEN_WIDTH, nextScreen.getWidth() ), Math.min( ScreenManager.SCREEN_HEIGHT, nextScreen.getHeight() ) );
				else
					nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );			
			} else {
				blackboard.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				blackboard.setPosition( 0, 0 );

				if ( nextScreen.getWidth() > 0 && nextScreen.getHeight() > 0 )
					nextScreen.setSize( Math.min( ScreenManager.SCREEN_WIDTH, nextScreen.getWidth() ), Math.min( blackboard.getVisibleAreaHeight(), nextScreen.getHeight() ) );
				else
					nextScreen.setSize( ScreenManager.SCREEN_WIDTH, blackboard.getVisibleAreaHeight() );


				nextScreen.setPosition( ( ScreenManager.SCREEN_WIDTH - nextScreen.getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - nextScreen.getHeight() ) >> 1 );
			}
		}
		//#endif
		
		midlet.manager.setCurrentScreen( nextScreen );
		
		return screen;
	} // fim do m�todo changeScreen( int )


	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			//#if JAR == "full"
			case BACKGROUND_TYPE_BLACKBOARD:
				midlet.manager.setBackground( blackboard, true  );				
			break;
			//#endif
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( BLACKBOARD_COLOR );
			break;
		}
	} // fim do m�todo setBackground( byte )
	
	
	private static final BasicMenu createBasicMenu( int index, int[] entries ) throws Exception {
		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, getFont( FONT_INDEX_DEFAULT ), entries );		
		setMenuCursor( menu );
		
		return menu;
	}	
	
	
	public static final boolean isLowMemory() {
		return lowMemory;
	}
	
	
	//#if JAR != "min"
	public static final void prepareEnding() {
		loader = null;
		loader = new LoadListener() {
			public final void load() throws Exception {
				MediaPlayer.free();
				unloadMatch();
				match = new Match( Match.MODE_CHAMPION, null, null );
				setScreen( SCREEN_MATCH );
			}
		};
		
		setScreen( SCREEN_LOADING_PLAY_SCREEN );
	}
	
	
	public static final void playMatch( final Replay replay ) {
		loader = null;
		loader = new LoadListener() {
			public final void load() throws Exception {
				MediaPlayer.free();
				unloadMatch();				
				match = new Match( replay );
				setScreen( SCREEN_MATCH );
			}
		};
		setScreen( SCREEN_LOADING_PLAY_SCREEN );
	}	
	
	//#endif
	
	
	public static final void playMatch( final byte division, final byte stage, final int difficultyLevel, final byte mode, final Team team1, final Team team2 ) {
		loader = null;
		loader = new LoadListener() {
			public final void load() throws Exception {
				MediaPlayer.free();
				unloadMatch();				
				match = new Match( mode, division, difficultyLevel, stage, team1, team2 );
				setScreen( SCREEN_MATCH );
			}
		};
		setScreen( SCREEN_LOADING_PLAY_SCREEN );
	}

	
	public static final void matchEnded( Match match ) {
		switch ( match.getMode() ) {
			case Match.MODE_CHAMPIONSHIP_MATCH_HOME:
			case Match.MODE_CHAMPIONSHIP_MATCH_AWAY:
				championship.playerMatchEnded( match );
			break;
			
			default:
				setScreen( SCREEN_MAIN_MENU );
		}
	}	


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						setScreen( SCREEN_NEW_GAME_MENU ); 
					break;
					
					//#if JAR != "min"
					case ENTRY_MAIN_MENU_REPLAYS:
						setScreen( SCREEN_LOAD_REPLAY );
					break;
					//#endif
					
					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
					break;
					
					//#if DEMO == "true"
//# 					case ENTRY_MAIN_MENU_BUY_FULL_GAME:
//# 						setScreen( SCREEN_BUY_GAME_RETURN );
//# 					break;
					//#endif
					
					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					//#if DEBUG == "true"
					case ENTRY_MAIN_MENU_ERROR_LOG:
						setScreen( SCREEN_ERROR_LOG );
					break;
					//#endif
					
					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU
			
			case SCREEN_NEW_GAME_MENU:
				switch ( index ) {
					case ENTRY_NEW_GAME_MENU_TRAINING:
						setScreen( SCREEN_CHOOSE_PLAYER_TRAINING );
					break;

					case ENTRY_NEW_GAME_MENU_CHAMPIONSHIP:
						// verifica se h� campeonato salvo. Caso exista, tenta carreg�-lo. Se houver algum erro ao carregar o
						// campeonato, o comportamento � igual ao caso de n�o existir jogo salvo.
						if ( loadChampionship() )
							setScreen( SCREEN_SAVED_GAME_FOUND );
						else
							setScreen( SCREEN_CHAMPIONSHIP_MENU );
					break;

					case ENTRY_NEW_GAME_MENU_VERSUS:
						setScreen( SCREEN_CHOOSE_PLAYER_2_PLAYERS );
					break;

					case ENTRY_NEW_GAME_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;
				}
			break;
			
			case SCREEN_SAVED_GAME_FOUND:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						setScreen( SCREEN_CHAMPIONSHIP );
					break;
					
					case BasicConfirmScreen.INDEX_NO:
						// inicia um novo campeonato
						setScreen( SCREEN_CHAMPIONSHIP_MENU );
					break;
				}
			break;
			
			case SCREEN_CHAMPIONSHIP_MENU:
				try {
					switch ( Championship.getHighestDivisionWon() ) {
						case DIVISION_NONE:
							switch ( index ) {
								case ENTRY_CHAMPIONSHIP_MENU_3RD_DIVISION:
								break;
								
								default:
									index = ENTRY_CHAMPIONSHIP_MENU_BACK;
							}
						break;
						
						case DIVISION_3:
							switch ( index ) {
								case ENTRY_CHAMPIONSHIP_MENU_3RD_DIVISION:
								case ENTRY_CHAMPIONSHIP_MENU_2ND_DIVISION:
								break;
								
								default:
									index = ENTRY_CHAMPIONSHIP_MENU_BACK;
							}
						break;
					}
					
					switch ( index ) {
						case ENTRY_CHAMPIONSHIP_MENU_3RD_DIVISION:
							championship = new Championship( Championship.DIVISION_3 );
						break;

						case ENTRY_CHAMPIONSHIP_MENU_2ND_DIVISION:
							championship = new Championship( Championship.DIVISION_2 );
						break;

						case ENTRY_CHAMPIONSHIP_MENU_1ST_DIVISION:
							championship = new Championship( Championship.DIVISION_1 );
						break;

						case ENTRY_CHAMPIONSHIP_MENU_BACK:
							setScreen( SCREEN_NEW_GAME_MENU );
						return;
					}
					
					// s� n�o chega nesse ponto caso o jogador tenha selecionado a op��o de voltar
					setScreen( SCREEN_CHAMPIONSHIP );					
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
					
					exit();
				}
			break;
			
			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_TIPS:
						setScreen( SCREEN_HELP_TIPS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MATCH );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:	
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:	
							setScreen( SCREEN_MATCH );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:	
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:			
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}					
				}
			break; // fim case SCREEN_PAUSE
			
			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_BACK:	
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
						break;		
					}
				}
			break; // fim case SCREEN_OPTIONS
			
			//#if DEMO == "true"
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 							e.printStackTrace();
							//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif
			
			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						//#if DEMO == "false"
//						HighScoresScreen.setScore( playScreen.getScore() );						
//						playScreen.resetScore();
						MediaPlayer.saveOptions();
						setScreen( SCREEN_MAIN_MENU );
						//#else
//# 						gameOver( 0 );
						//#endif
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;
			
			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;			
			
			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				
				setScreen( SCREEN_SPLASH_NANO );
			break;
		} // fim switch ( id )		
	}
	
	
	//#if JAR == "full"
	public static final BlackBoard getBlackBoard() {
		return blackboard;
	}
	
	//#endif


	public final void onItemChanged( Menu menu, int id, int index ) {
		updateCursor( menu.getDrawable( index ) );
	}
	
	
	public static final void updateCursor( Drawable item ) {
		cursor.setTarget( item );
	}
	
	
	public static final void setMenuCursor( Menu menu ) {
		try {
			cursor = new Cray();
			menu.setCursor( cursor, Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_LEFT | Drawable.ANCHOR_TOP );
			menu.setCurrentIndex( menu.getCurrentIndex() );
		} catch ( Exception e ) {
			//#if DEBUG ==" true"
//# 			e.printStackTrace();
			//#endif
			
			exit();
		}
	}
	
	
	/**
	 * Define uma soft key a partir de um texto. Equivalente � chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece vis�vel. Para o label estar sempre vis�vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( getFont( FONT_INDEX_TEXT ), getText( textIndex ) ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}				
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null )
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
			break;
			
			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) 
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
			break;
		}
		//#endif
	} // fim do m�todo setSoftKey( byte, Drawable, boolean, int )		
	
	
	public static final ImageFont getFont( int index ) {
		return FONTS[ index ];
	}
    
    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "1.0";

        return "<ALN_H>Vers�o " + version + "\n\n";        
    }    
	
	
	private static final void unloadMatch() {
		if ( match != null ) {
			match.unloadMatch();
			match = null;
		}
		System.gc();
	}
	
	
	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	
	private static interface LoadListener {
		public void load() throws Exception;
	}
	
	
	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;
		
		private static final byte MAX_DOTS_MODULE = MAX_DOTS -1 ;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;
		
		private final byte previousScreen;


		/**
		 * 
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );
			
			previousScreen = currentScreen;
			
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
			
			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo s�o carregados aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa, para evitar poss�veis atrasos em transi��es e
					// garantir que novos recursos s� ser�o carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );
						loadThread = new Thread() {
							public final void run() {
								try {
									System.gc();
									listener.load();
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									texts[ TEXT_LOG ] += e.getMessage().toUpperCase();

									setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									//#else
//# 										// volta � tela anterior
//# 										setScreen( previousScreen );
									//#endif
								}
							} // fim do m�todo run()
						};
						loadThread.start();
					}
				} else {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for ( byte i = 0; i < dots; ++i )
						temp += '.';

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );			
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}					
				}
			}
		} // fim do m�todo update( int )
		
		
		public final void paint( Graphics g ) {
			super.paint( g );
			
			painted = true;
		}

	} // fim da classe interna LoadScreen


	/**
	 * Tenta carregar o campeonato salvo. 
	 * 
	 * @return boolean indicando se o campeonato foi carregado corretamente.
	 */
	private final boolean loadChampionship() {
		// continua o jogo salvo
		try {
			championship = null;
			championship = new Championship( Championship.DIVISION_LOAD_GAME );
			
			return true;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return false;
		}
	}
	
	// </editor-fold>

}
