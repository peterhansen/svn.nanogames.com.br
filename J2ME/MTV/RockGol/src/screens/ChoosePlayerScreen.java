/**
 * ChoosePlayerScreen.java
 * �2008 Nano Games.
 *
 * Created on May 3, 2008 8:00:54 PM.
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import core.Keeper;
import core.Shooter;
import core.Team;

//#if SCREEN_SIZE != "SMALL"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif


/**
 * 
 * @author Peter
 */
public final class ChoosePlayerScreen extends UpdatableGroup implements Constants, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif

{

	private static final byte STATE_CHOOSE_SHOOTER_0	= 0;
	private static final byte STATE_CHOOSE_KEEPER_0		= 1;
	private static final byte STATE_CHOOSE_SHOOTER_1	= 2;
	private static final byte STATE_CHOOSE_KEEPER_1		= 3;
	
	private byte state;
	
	private byte currentIndex;
	
	private final byte totalTeams;
	
	/** �ndices dos batedores e goleiros (0 e 1 do primeiro time, 2 e 3 do segundo). */
	private final byte[] players = new byte[ 4 ];
	
	private final byte id;
	
	//#if SCREEN_SIZE == "SMALL"
//# 	private final MarqueeLabel title;
	//#else
	private final RichLabel title;
	//#endif
	
	private final RichLabel name;
	
	private DrawableImage currentPlayerImage;
	
	/** �ndice da imagem do jogador no grupo. */
	private static final byte PLAYER_DRAWABLE_INDEX = 2;
	
	private final DrawableImage arrowLeft;
	private final DrawableImage arrowRight;
	
	private static final short ARROW_BLINK_TIME = 666; // dem�in!
	
	private short accArrowTime = ARROW_BLINK_TIME;
	
	private static final byte TOTAL_ITEMS = 5;
	
	/** Intervalo m�nimo entre 2 trocas de personagem. */
	private static final short MIN_CHANGE_INTERVAL = 333;
	
	/** Momento da �ltima troca de personagem. */
	private long lastChangeTime;
			
	
	public ChoosePlayerScreen( int id ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.id = ( byte ) id;
		totalTeams = Championship.getTeamsAvailable();
		
		final ImageFont font = GameMIDlet.getFont( FONT_INDEX_DEFAULT );
		
		String strTitle;
		switch ( id ) {
			case SCREEN_CHOOSE_PLAYER_2_PLAYERS:
				strTitle = GameMIDlet.getText( TEXT_PLAYER_1 ) + GameMIDlet.getText( TEXT_CHOOSE_YOUR_SHOOTER );
			break;
			
			case SCREEN_CHOOSE_PLAYER_TRAINING:
				strTitle = GameMIDlet.getText( TEXT_CHOOSE_YOUR_SHOOTER );
			break;
			
			default:
				//#if DEBUG == "true"
				throw new IllegalArgumentException( "id inv�lida: " + id );
				//#else
//# 				throw new IllegalArgumentException();
				//#endif
		}
		
		//#if SCREEN_SIZE == "SMALL"
//# 		title = new MarqueeLabel( font, strTitle, ScreenManager.SCREEN_WIDTH >> 2, MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
//# 		title.setWaitTime( 800 );
//# 		title.setSize( ScreenManager.SCREEN_WIDTH, font.getHeight() << 1 );
		//#else
		title = new RichLabel( font, "<ALN_H>" + strTitle, ScreenManager.SCREEN_WIDTH, null );
		title.setSize( ScreenManager.SCREEN_WIDTH, font.getHeight() << 1 );
		//#endif
		insertDrawable( title );
		
		name = new RichLabel( font, null, ScreenManager.SCREEN_WIDTH, null );
		name.setSize( ScreenManager.SCREEN_WIDTH, font.getHeight() << 1 );
		insertDrawable( name );
		
		updateCurrentPlayer();
		
		//#if ROTATE_BUG == "false"
		arrowLeft = new DrawableImage( PATH_IMAGES + "scroll.png" );
		arrowRight = new DrawableImage( arrowLeft );

		arrowLeft.setTransform( TRANS_ROT270 | TRANS_MIRROR_V );
		arrowRight.setTransform( TRANS_ROT90 );
		//#else
//# 		arrowLeft = new DrawableImage( PATH_IMAGES + "scroll_h.png" );
//# 
//# 		arrowRight = new DrawableImage( arrowLeft );
//# 		arrowRight.setTransform( TRANS_MIRROR_H );
		//#endif
	
		arrowLeft.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( arrowLeft );
		arrowRight.defineReferencePixel( ANCHOR_VCENTER );
		insertDrawable( arrowRight );
	}
	

	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.LEFT:
			case ScreenManager.UP:
				if ( checkChangeTime() ) {
					int previousPlayer = -1;
					do {
						currentIndex = ( byte ) ( ( currentIndex - 1 + totalTeams ) % totalTeams );

						// evita que 2 personagens iguais sejam selecionados
						switch ( state ) {
							case STATE_CHOOSE_KEEPER_1:
								previousPlayer = players[ 1 ];
							break;

							case STATE_CHOOSE_SHOOTER_1:
								previousPlayer = players[ 0 ];
							break;
						}
					} while ( currentIndex == previousPlayer );

					updateCurrentPlayer();
				}
			break;
			
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.RIGHT:
			case ScreenManager.DOWN:
				if ( checkChangeTime() ) {
					int previousIndex = -1;
					do {
						currentIndex = ( byte ) ( ( currentIndex + 1 ) % totalTeams );

						// evita que 2 personagens iguais sejam selecionados
						switch ( state ) {
							case STATE_CHOOSE_KEEPER_1:
								previousIndex = players[ 1 ];
							break;

							case STATE_CHOOSE_SHOOTER_1:
								previousIndex = players[ 0 ];
							break;
						}
					} while ( currentIndex == previousIndex );

					updateCurrentPlayer();
				}
			break;
			
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_SOFT_LEFT:
				if ( checkChangeTime() ) {
					switch ( state ) {
						case STATE_CHOOSE_SHOOTER_0:
						case STATE_CHOOSE_KEEPER_0:
						case STATE_CHOOSE_SHOOTER_1:
							players[ state ] = currentIndex;

							++state;
							currentIndex = 0;

							if ( state <= STATE_CHOOSE_KEEPER_1 ) {
								String strTitle;
								switch ( id ) {
									case SCREEN_CHOOSE_PLAYER_2_PLAYERS:
										strTitle = GameMIDlet.getText( TEXT_PLAYER_1 + ( state >> 1 ) ) + GameMIDlet.getText( TEXT_CHOOSE_YOUR_SHOOTER + ( state & 1 ) );
									break;

									case SCREEN_CHOOSE_PLAYER_TRAINING:
										strTitle = GameMIDlet.getText( TEXT_CHOOSE_YOUR_SHOOTER + state );
									break;

									default:
										//#if DEBUG == "true"
											throw new IllegalArgumentException( "id inv�lida: " + id );
										//#else
//# 										throw new IllegalArgumentException();
										//#endif
								}
								//#if SCREEN_SIZE == "SMALL"
//# 								title.setText( strTitle );
								//#else
								title.setText( "<ALN_H>" + strTitle );
								//#endif
							}

							switch ( state ) {
								case STATE_CHOOSE_KEEPER_1:
									if ( players[ 1 ] == 0 )
										currentIndex = 1;
								break;

								case STATE_CHOOSE_SHOOTER_1:
									if ( players[ 0 ] == 0 )
										currentIndex = 1;
								break;
							}

							updateCurrentPlayer();
						break;

						case STATE_CHOOSE_KEEPER_1:
							players[ state ] = currentIndex;

							switch ( id ) {
								case SCREEN_CHOOSE_PLAYER_TRAINING:
									GameMIDlet.playMatch( DIVISION_NONE, STAGE_SINGLE_MATCH, 0, Match.MODE_TRAINING, 
														  new Team( players[ 0 ], players[ 1 ] ), new Team( players[ 2 ], players[ 3 ] ) );
								break;

								case SCREEN_CHOOSE_PLAYER_2_PLAYERS:
									GameMIDlet.playMatch( DIVISION_VERSUS, STAGE_VERSUS, 0, Match.MODE_2_PLAYERS, 
														  new Team( players[ 0 ], players[ 1 ] ), new Team( players[ 2 ], players[ 3 ] ) );
								break;
							}
						break;
					}
				}
			break;
			
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				GameMIDlet.setScreen( SCREEN_NEW_GAME_MENU );
			break;
		}
	}
	
	
	private final boolean checkChangeTime() {
		final long interval = System.currentTimeMillis() - lastChangeTime;
		if ( interval >= MIN_CHANGE_INTERVAL ) {
			lastChangeTime = System.currentTimeMillis();
			return true;
		}
		
		return false;
	}
	
	
	public final void keyReleased( int key ) {
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		accArrowTime -= delta;
		if ( accArrowTime <= 0 ) {
			accArrowTime = ARROW_BLINK_TIME;
			arrowLeft.setVisible( !arrowLeft.isVisible() );
			arrowRight.setVisible( !arrowRight.isVisible() );
		}
	}
	
	
	private final void updateCurrentPlayer() {
		try {
			if ( activeDrawables == TOTAL_ITEMS )
				removeDrawable( PLAYER_DRAWABLE_INDEX );

			switch ( state ) {
				case STATE_CHOOSE_SHOOTER_0:
				case STATE_CHOOSE_SHOOTER_1:
					currentPlayerImage = Shooter.getImage( currentIndex );
					name.setText( "<ALN_H>" + Team.getName( currentIndex ) );
				break;
				
				case STATE_CHOOSE_KEEPER_0:
				case STATE_CHOOSE_KEEPER_1:
					currentPlayerImage = Keeper.getImage( currentIndex );
					name.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_NAME_KEEPER_0 + Keeper.getSpriteIndex( currentIndex ) ) );
				break;
			}

			currentPlayerImage.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			currentPlayerImage.setRefPixelPosition( size.x >> 1, size.y >> 1 );
			
			insertDrawable( currentPlayerImage, PLAYER_DRAWABLE_INDEX );		
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif

			GameMIDlet.exit();
		}
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		currentPlayerImage.setRefPixelPosition( size.x >> 1, size.y >> 1 );
		
		name.setPosition( 0, currentPlayerImage.getPosY() + currentPlayerImage.getHeight() );
		
		arrowLeft.setRefPixelPosition( size.x >> 2, size.y >> 1 );
		arrowRight.setRefPixelPosition( ( size.x * 3 ) >> 2, size.y >> 1 );
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		if ( arrowLeft.contains( x, y ) ) {
			keyPressed( ScreenManager.KEY_NUM4 );
		} else if ( arrowRight.contains( x, y ) ) {
			keyPressed( ScreenManager.KEY_NUM6 );
		} else if ( currentPlayerImage.contains( x, y ) || name.contains( x, y ) ) {
			keyPressed( ScreenManager.FIRE );
		}		
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif

}
