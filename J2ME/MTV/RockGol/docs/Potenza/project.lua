-- descritor para conversão de arquivos para o formato Potenza
-- Peter Hansen

-- exemplos de aparelhos-chefe com poucos caracteres
-- gradiente gf690: grgf690
-- motorola v3: mtv3
-- nokia n76: nkn76
-- nokia 5200: nk5200
-- samsung d820: sgd820
-- samsung c420: sgc420
-- lg me970: lgme970
-- lg mg225d: lgmg225d
-- benq-siemens el71: bsel71

-- nome do aplicativo/jogo
APP_NAME = [[Penalti MTV com Cleston]]

-- caminho do arquivo original de especificação do aplicativo J2ME
PATH_APP_SPEC = [[326002-EST001-NanoGames.doc]]

-- caminho do formulário original de aprovação de aplicações Vivo Downloads
PATH_FAAVD = [[FAAVD_supportcomm.doc]]

-- caminho do arquivo original de solicitação de teste
PATH_SOL = [[326002-SOL001.01.1.xls]]

VERSIONS = {
	{
		-- benq
		  ['jad'] = [[benq.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.0]]
		, ['device'] = [[bsel71]]
	},
	{
		-- big
		  ['jad'] = [[big.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.1]]
		, ['device'] = [[nkn95]]
	},
	{
		-- c420
		  ['jad'] = [[c420.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.2]]
		, ['device'] = [[sgc425]]
	},
	{
		-- c65
		  ['jad'] = [[c65.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.3]]
		, ['device'] = [[bsc65]]
	},
	{
		-- d820
		  ['jad'] = [[d820.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.4]]
		, ['device'] = [[sgd836]]
	},
	{
		-- gf690
		  ['jad'] = [[gf690.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.5]]
		, ['device'] = [[grgf690]]
	},
	{
		-- me970
		  ['jad'] = [[me970.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.6]]
		, ['device'] = [[lgme970]]
	},
--	{
--		-- medium - versão com problemas nos aparelhos Motorola K1, V3 e SonyEricsson W580
--		  ['jad'] = [[medium.jad]]
--		, ['jar'] = [[]]
--		, ['number'] = [[1.0.7]]
--		, ['device'] = [[mtv3]]
--	},
	{
		-- medium
		  ['jad'] = [[medium.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.12]]
		, ['device'] = [[mtv3]]
	},
	{
		-- medium_lg
		  ['jad'] = [[kg800.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.8]]
		, ['device'] = [[lgmg320d]]
	},	
	{
		-- small
		  ['jad'] = [[small.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.9]]
		, ['device'] = [[nk5200]]
	},
	{
		-- small_low
		  ['jad'] = [[low.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.10]]
		, ['device'] = [[nk2660]]
	},
	{
		-- small_min
		  ['jad'] = [[min.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.11]]
		, ['device'] = [[lgmg225d]]
	},
}

