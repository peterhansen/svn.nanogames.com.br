-- descritor para conversão de arquivos para o formato Potenza
-- Peter Hansen

-- exemplos de aparelhos-chefe com poucos caracteres
-- gradiente gf690: grgf690
-- motorola v3: mtv3
-- nokia n76: nkn76
-- samsung d820: sgd820
-- samsung c420: c420
-- lg me970: lgme970
-- lg mg155: lgmg155

-- nome do aplicativo/jogo
APP_NAME = [[LevPeso]]

-- caminho do arquivo original de especificação do aplicativo J2ME
PATH_APP_SPEC = [[326002-EST001-NanoGames.doc]]

-- caminho do formulário original de aprovação de aplicações Vivo Downloads
PATH_FAAVD = [[FAAVD_supportcomm.doc]]

-- caminho do arquivo original de solicitação de teste
PATH_SOL = [[326002-SOL001.01.1.xls]]

VERSIONS = {
	{
		-- big touch screen
		  ['jad'] = [[ba1200.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.0]]
		, ['device'] = [[mta1200]]
	},
	{
		-- big flush graphics
		  ['jad'] = [[bfg.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.2]]
		, ['device'] = [[lgme970]]
	},
	{
		-- big standard
		  ['jad'] = [[bs.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.4]]
		, ['device'] = [[nk6600]]
	},
	{
		-- big samsung (very difficult)
		  ['jad'] = [[bsamvd.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.3]]
		, ['device'] = [[sgd836]]
	},
	{
		-- big very difficult
		  ['jad'] = [[bvd.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.5]]
		, ['device'] = [[nkn95]]
	},
	{
		-- medium standard
		  ['jad'] = [[ms.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.7]]
		, ['device'] = [[lgMG320]]
	},	
	{
		-- medium very difficult
		  ['jad'] = [[mv360.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.8]]
		, ['device'] = [[mtv3]]
	},
	{
		-- small nokia series 40 2nd edition
		  ['jad'] = [[s6822.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.12]]
		, ['device'] = [[nk6061]]
	},
	{
		-- small samsung (very difficult)
		  ['jad'] = [[sc420.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.10]]
		, ['device'] = [[sge256]]
	},
	{
		-- small very difficult
		  ['jad'] = [[sl6.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.11]]
		, ['device'] = [[sew200]]
	},
	{
		-- small standard
		  ['jad'] = [[ss.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.16]]
		, ['device'] = [[lgmg230]]
	},
	{
		-- small samsung widescreen
		  ['jad'] = [[ssamw.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.14]]
		, ['device'] = [[sgu106]]
	},
	{
		-- small very easy
		  ['jad'] = [[sve.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.17]]
		, ['device'] = [[nk2630]]
	},
}

