/**
 * PlayScreen.java
 */
package screens;

//#ifndef LOW_JAR
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import game.Birdie;
import game.BirdieStateListener;
import game.Detonator;
import game.DetonatorHands;
import game.StatusBar;
import game.StrengthBar;
import game.StrengthBarListener;
import game.Weight;
import javax.microedition.lcdui.Graphics;

/**
 * �2007 Nano Games
 * @author Daniel L. Alves
 */
public final class PlayScreen extends UpdatableGroup implements KeyListener, Constants, StrengthBarListener, SpriteListener, BirdieStateListener
//#ifndef LOW_JAR
	// N�o h� aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do c�digo)
, PointerListener
//#endif
{
	//<editor-fold defaultstate="collapsed" desc="Posicionamento dos elementos da tela de jogo">
	
	/** Dist�ncia das barras de for�a para as extremidades laterais da tela */
	private final byte STR_BAR_DIST_FROM_SCREEN = 2;
	
	/** Dist�ncia das barras de for�a para a barra de status */
	//#if SCREEN_SIZE == "SMALL"
		private final byte STRENGTH_BAR_DIST_FROM_STATUS_BAR = 11;
	//#else
//# 	private final byte STRENGTH_BAR_DIST_FROM_STATUS_BAR = 24;
	//#endif
	
	/** Dist�ncia do personagem para a barra de status */
	//#if SCREEN_SIZE == "SMALL"
		private final byte DETONATOR_DIST_FROM_STATUS_BAR = 5;
	//#else
//# 	private final byte DETONATOR_DIST_FROM_STATUS_BAR = 32;
	//#endif
	
	/** Dist�ncia das m�os que apertam os bot�es 4 e 6 para as barras de for�a */
	//#if SCREEN_SIZE == "SMALL"
		private final byte BUTTON_HAND_DIST_FROM_STRENGTH_BAR = 3;
	//#else
//# 	private final byte BUTTON_HAND_DIST_FROM_STRENGTH_BAR = 24;
	//#endif
	
	/** Dist�ncia da tomada para o rodap� */
	private final byte TOMMY_DIST_FROM_BASEBOARD = 5;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Estados do jogo">

	// Estados do jogo
	private final byte STATE_RUNNING		=  0;
	private final byte STATE_RUNNING_BT_5	=  1;
	private final byte STATE_PAUSED			=  2;
	private final byte STATE_LEVEL_ENDED	=  3; // Espera a anima��o do personagem para iniciar a transi��o de fases
	private final byte STATE_TRANSITION_1	=  4; // Torna a tela preta
	private final byte STATE_TRANSITION_2	=  5; // Levanta a cortina preta
	private final byte STATE_TRANSITION_3	=  6; // Exibe a fase e o "Prepare-se!"
	private final byte STATE_TRANSITION_4	=  7; // Coloca as barras de for�a e a barra de status na tela. Faz o personagem segurar a barra
	private final byte STATE_TRANSITION_5	=  8; // Exibe o "Vai!"
	private final byte STATE_LOST_LIFE_1	=  9; // Mostra o personagem triste e retira alguns elementos da tela
	private final byte STATE_LOST_LIFE_2	= 10; // Torna a tela preta
	private final byte STATE_RESETING_LEVEL	= 11; // Reinicia a fase
	
	/** Estado atual do jogo */
	private byte currState;
	
	/** Estado anterior ao atual. Necess�rio para voltarmos do estado STATE_PAUSED */
	private byte prevState;
	
	/** Tempo de dura��o do estado STATE_LEVEL_ENDED. Deve ser maior que ( Detonator.FINISH_ANIM_DUR * 2 ) !!! */
	private final short STATE_LEVEL_ENDED_TIME = 1500;
	
	/** Tempo de dura��o do estado STATE_TRANSITION_3 */
	private final short TRANSITION_3_TIME = 1000;
	
	/** Tempo de dura��o do estado STATE_TRANSITION_5 */
	private final short TRANSITION_5_TIME = 1000;

	//</editor-fold>

	/** Barras indicativas da for�a do personagem */
	private final StrengthBar[] strengthBars = new StrengthBar[BOTH_SIDES];

	// TODO : Quer�amos que o jogador pudesse perder uma vida por um disnivelamento muito grande da for�a dos
	// bra�os. Infelizmente n�o houve tempo
	///** Percentual de diferen�a permitido entre as for�as dos bra�os */
	//private final short MAX_ARMS_STR_DIFF = 20;

	/** M�os que indicam quando o jogador deve apertar os bot�es */
	private final Drawable[] buttonHands = new Drawable[TOTAL_HANDS];

	//#ifdef SYNCHRONIZE_ARMS
//# 	
//# 		/** Tempo decorrido desde que heldArm foi bloqueado */
//# 		private short heldArmCounter;
//# 
//# 		/** Tempo em milissegundos que um bra�o fica bloqueado */
//# 		private final short HELD_ARM_DEFAULT_TIME = 100;
//# 
//# 		/** Indica que bra�o n�o est� recebendo eventos. Serve para evitar que o jogador utilize v�rias teclas
//# 		 *  ao mesmo tempo */
//# 		private byte heldArm;
//# 
	//#endif

	/** Peso levantado pelo personagem */
	private final Weight weight;

	/** Personagem que o jogador ir� controlar no jogo */
	private final Detonator detonator;
	
	/** M�os do personagem. S�o separadas do corpo para que o peso fique entre elas e o corpo */
	private final DetonatorHands detonatorHands;

	/** Barra para exibir os dados da partida: pontos, n�mero de vidas e tempo */
	private final StatusBar statusBar;
	
	/** Ajuda a animar a barra de status */
	private final MUV statusBarSpeed;
	
	/** Passarinho que atrapalha o jogador */
	private final Birdie birdie;
	
	/** Controla quantas vezes o passarinho j� entrou na tela durante a fase atual */
	private int birdieControlCounter;
	
	/** Controla quando o passarinho deve entrar em cena */
	private final byte[] birdieControl = new byte[ MAX_BIRDIE_APPEARENCES_BY_LEVEL ];
	
	/** Controla a quanto tempo o passarinho j� est� pulando no peso */
	private int birdieJumpControl;
	
	/** Fase atual do jogo */
	private byte level = -1;
	
	/** Define quando que as barras de for�a come�ar�o a piscar para indicar que o tempo da fase est� acabando */
	public final byte TIME_IS_SHORT = 10;
	
	/** Indica quais etapas da fase atual j� foram alcan�adas pelo menos uma vez */
	final boolean[] stepReached = new boolean[N_STRENGTH_BAR_STEPS];
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos Drawables do Grupo">

	public final byte PLAYSCREEN_BACKGROUND_INDEX	= 0;
	private final byte PLAYSCREEN_PAPERFOLD_INDEX	= 8;
	private final byte PLAYSCREEN_BUTTON5_INDEX		= 13;
	private final byte PLAYSCREEN_GO_INDEX			= 14;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Transi��es">
	
	/** Controla a transi��o de fases */
	private final MUV transitionControl = new MUV();
	
	/** �ltimo valor gerado por <code>transitionControl</code> */
	private int lastTransitionDx;
	
	/** Em quantas fatias a tela ser� cortada na transi��o de fases */
	private final byte TRANSITION_N_SLICES = 10;
	
	/** Forma a faixa que exibe a fase atual assim que come�amos um novo n�vel */
	private final Pattern transitionPaper;
	
	/** Forma a faixa que exibe a fase atual assim que come�amos um novo n�vel */
	private final Sprite transitionFold;
	
	/** Label onde fica escrita a informa��o da fase atual */
	private final Label transitionLabel;
	
	// Define os �ndices das sequ�ncias do sprite. Ver descritor correspondente */
	private final byte TRANSITION_FOLD_SEQ_PLAYING = 0;
	private final byte TRANSITION_FOLD_SEQ_STOPPED = 1;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Sequ�ncias do Sprite do Bot�o 5">
	
	private final byte BT5_SEQ_STOPPED			= 0;
	private final byte BT5_SEQ_BLINKING_BIG		= 1;
	private final byte BT5_SEQ_GOING_DOWN		= 2;
	private final byte BT5_SEQ_BLINKING_SMALL	= 3;
	
	//</editor-fold>
	
	/** Construtor */
	public PlayScreen() throws Exception
	{
		// Inicializa o grupo
		super( 15 );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		Thread.yield();

		// Cria o personagem do jogo
		detonator = new Detonator();
		Thread.yield();
		
		// Garante um slot para o background da tela de jogo. Ele ser� realmente alocado no m�todo onNewLevel
		insertDrawable( detonator );
		
		// Insere o personagem do jogo em seu slot final
		insertDrawable( detonator );

		// Cria o peso
		weight = new Weight();
		Thread.yield();
		insertDrawable( weight );
		
		// Cria as m�os do personagem
		detonatorHands = new DetonatorHands( weight );
		Thread.yield();
		insertDrawable( detonatorHands );
		detonator.setListener( detonatorHands );
		
		// Cria o passarinho
		birdie = new Birdie( this );
		Thread.yield();
		insertDrawable( birdie );
		
		// Coloca o passarinho para observar o peso e vice-versa
		weight.setListener( birdie );
		birdie.setListener( Birdie.LISTENER_WEIGHT, weight );
		birdie.setListener( Birdie.LISTENER_DETONATOR, detonator );
		
		// Cria a barra de for�a da esquerda
		strengthBars[ITEM_LEFT] = new StrengthBar( this );
		Thread.yield();
		insertDrawable( strengthBars[ITEM_LEFT] );

		// Cria a barra de for�a da direita
		strengthBars[ITEM_RIGHT] = new StrengthBar( this );
		Thread.yield();
		insertDrawable( strengthBars[ITEM_RIGHT] );
		
		// Transi��o
		final String imgExt = ".png";
		final DrawableImage fill = new DrawableImage( "/whitePaper" + imgExt );
		Thread.yield();
		transitionPaper = new Pattern( fill );
		insertDrawable( transitionPaper );
		transitionPaper.setSize( 0, fill.getHeight() );
		transitionPaper.setVisible( false );

		String path = "/whitePaperFold";
		final String descExt = ".bin";
		transitionFold = new Sprite( path + descExt, path );
		Thread.yield();
		insertDrawable( transitionFold );
		transitionFold.setVisible( false );
		transitionFold.setListener( this, PLAYSCREEN_PAPERFOLD_INDEX );
		transitionFold.setSequence( TRANSITION_FOLD_SEQ_STOPPED ); // Fica parado por enquanto. Ver descritor correspondente
		
		path = "/fontTransition";
		final ImageFont transitionFont = ImageFont.createMultiSpacedFont( path + ".png", path + ".dat" );
		Thread.yield();
		transitionLabel = new Label( transitionFont, "" );
		insertDrawable( transitionLabel );
		transitionLabel.setSize( 0, transitionFont.getHeight() );
		transitionLabel.setVisible( false );

		// Cria a barra de status
		statusBar = new StatusBar();
		Thread.yield();
		insertDrawable( statusBar );
		statusBarSpeed = new MUV( ( 5 * statusBar.getHeight() ) / 3 );

		// Cria as m�os que indicam quando o jogador deve apertar os bot�es
		buttonHands[ITEM_RIGHT] = createPressingButtonHand( ITEM_RIGHT );
		Thread.yield();
		insertDrawable( buttonHands[ITEM_RIGHT] );
		
		buttonHands[ITEM_LEFT] = createPressingButtonHand( ITEM_LEFT );
		Thread.yield();
		insertDrawable( buttonHands[ITEM_LEFT] );

		path = "/button5";
		buttonHands[ITEM_CENTER] = new Sprite( path + descExt, path );
		Thread.yield();
		insertDrawable( buttonHands[ITEM_CENTER] );
		(( Sprite )buttonHands[ITEM_CENTER] ).setListener( this, PLAYSCREEN_BUTTON5_INDEX );
		
		// Posiciona o personagem
		detonator.setPosition( ( ScreenManager.SCREEN_WIDTH - detonator.getWidth() ) >> 1, StatusBar.STATUS_BAR_HEIGHT + DETONATOR_DIST_FROM_STATUS_BAR );
		
		// Posiciona as m�os do personagem
		detonatorHands.setPosition( detonator.getPosition() );
		
		// Posiciona as m�os que apertam os bot�es 4, 5 e 6
		final int handsY = StatusBar.STATUS_BAR_HEIGHT + STRENGTH_BAR_DIST_FROM_STATUS_BAR + strengthBars[ITEM_LEFT].getHeight() + BUTTON_HAND_DIST_FROM_STRENGTH_BAR;
		//#if SCREEN_SIZE == "SMALL"
			buttonHands[ITEM_LEFT].setPosition( STR_BAR_DIST_FROM_SCREEN + ( ( strengthBars[ITEM_LEFT].getWidth() - buttonHands[ITEM_LEFT].getWidth() ) >> 1 ), handsY );
			
			final int strengthBarRightWidth = strengthBars[ITEM_RIGHT].getWidth();
			buttonHands[ITEM_RIGHT].setPosition( ScreenManager.SCREEN_WIDTH - STR_BAR_DIST_FROM_SCREEN - strengthBarRightWidth + ( ( strengthBarRightWidth - buttonHands[ITEM_RIGHT].getWidth() ) >> 1 ), handsY );
		//#else
//# 		buttonHands[ITEM_LEFT].setPosition( 0, handsY );
//# 		buttonHands[ITEM_RIGHT].setPosition( ScreenManager.SCREEN_WIDTH - buttonHands[ITEM_RIGHT].getWidth(), handsY );
		//#endif
		buttonHands[ITEM_RIGHT].setVisible( false );
		buttonHands[ITEM_LEFT].setVisible( false );

		buttonHands[ITEM_CENTER].setVisible( false );
		
		// Imagem que inicia cada fase, liberando o input do usu�rio
		final DrawableImage go = new DrawableImage( "/go" + imgExt );
		Thread.yield();
		insertDrawable( go );
		go.setVisible( false );
		go.setPosition( ( ScreenManager.SCREEN_WIDTH - go.getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - go.getHeight() ) >> 1 );
		
		// Inicializa a fase
		onNewLevel();
		setState( STATE_TRANSITION_3 );
	}
	
	/** Inicializa os membros da classe para o in�cio da nova fase */
	private final boolean onNewLevel() throws Exception
	{
		// Atualiza a fase atual
		level = ( byte )( level + 1 );
		if( level >= MAX_LEVEL )
			return false;

		final String spacement = "  ";
		final char[] levelNumber = String.valueOf( level + 1 ).toCharArray();
		String text = spacement + AppMIDlet.getText( TEXT_LEVEL ) + spacement;
		text += levelNumber[0];
		
		for( byte i = 1 ; i < levelNumber.length ; ++i )
			text += " " + levelNumber[i];
			
		transitionLabel.setText( text + spacement + AppMIDlet.getText( TEXT_GET_READY ) );
		
		resetLevel( false );
		
		return true;
	}
	
	/** Reinicia a fase atual */
	private final void resetLevel( boolean halfScore ) throws Exception
	{
		// Inicializa as vari�veis que determinam a dificuldade da fase
		setLevelDifficulty();
		
		// Coloca alguns elementos fora da tela
		final int strengthBarWidth = strengthBars[ITEM_LEFT].getWidth();
		final int strengthBarY = StatusBar.STATUS_BAR_HEIGHT + STRENGTH_BAR_DIST_FROM_STATUS_BAR;
		strengthBars[ITEM_LEFT].setPosition( -strengthBarWidth, strengthBarY );
		strengthBars[ITEM_RIGHT].setPosition( ScreenManager.SCREEN_WIDTH, strengthBarY );
		
		statusBar.setPosition( 0 , -statusBar.getHeight() );
		
		// CANCELED : Achamos melhor n�o dividir os pontos do jogador
//		if( halfScore )
//			statusBar.halfScore();
//		else
//			statusBar.updateScore( 0, true );
		if( !halfScore )
			statusBar.updateScore( 0, true );

		// Modifica o background
		final Drawable bkg = createBackground();
		if( bkg != null )
			setDrawable( bkg, PLAYSCREEN_BACKGROUND_INDEX  );
			
		// Modifica a apar�ncia do peso
		weight.setAppearence( level );

		// CANCELED : N�o � mais utilizado. A MTV encrencou com a mudan�a de cores do "uniforme" do Detonator
		// Modifica a apar�ncia do personagem
//		detonator.setAppearence( level );
		
		// Determina o estado do personagem
		detonator.setState( Detonator.DETONATOR_STATE_STANDING );
		
		// Determina os pontos de pouso do passarinho
		Point leftWeightCenterTop = new Point(), rightWeightCenterTop = new Point();
		weight.getCoordinates( leftWeightCenterTop, rightWeightCenterTop );
		birdie.setWeightAttributes( leftWeightCenterTop, rightWeightCenterTop );
		
		for( byte i = 0 ; i < N_STRENGTH_BAR_STEPS; ++i )
			stepReached[i] = false;
	}
	
	/** Inicializa as vari�veis que determinam a dificuldade da fase */
	private final void setLevelDifficulty()
	{
		// Configura as barras de for�a
		strengthBars[ ITEM_LEFT ].reset( level );
		strengthBars[ ITEM_RIGHT ].reset( level );

		// Configura o passarinho
		birdieJumpControl = 0;
		birdieControlCounter = 0;
		for( byte i = MAX_BIRDIE_APPEARENCES_BY_LEVEL - 1 ; i >= 0 ; --i )
			birdieControl[i] = -1;
		
		// Verifica quantas vezes o passarinho vai entrar na tela nesta fase
		int nTimes = level / ( MAX_LEVEL / ( MAX_BIRDIE_APPEARENCES_BY_LEVEL + 1 ) ); 
		if( nTimes > MAX_BIRDIE_APPEARENCES_BY_LEVEL )
			nTimes = MAX_BIRDIE_APPEARENCES_BY_LEVEL;
		
		// Determina quando que o passarinho vai entrar na tela
		for( byte i = 0 ; i < nTimes ; ++i )
		{
			final int interval = 80 / nTimes;
			birdieControl[i] = ( byte )( ( ( nTimes - i - 1 ) * interval ) + NanoMath.randInt( interval + 1 ) + 20 );
		}

		//#ifdef BigA1200
//# 			if( level < 10 )
//# 				statusBar.setTime( TOTAL_LEVEL_TIME, true );
//# 			else
//# 				statusBar.setTime( TOTAL_LEVEL_TIME_LAST_LEVELS, true );
		//#else
			statusBar.setTime( TOTAL_LEVEL_TIME, true );
		//#endif

		//#ifdef SYNCHRONIZE_ARMS
//# 			heldArm = ITEM_NONE;
//# 			heldArmCounter = 0;
		//#endif
	}

	/** Aloca o background correspondente � fase atual */
	private final Drawable createBackground() throws Exception
	{
		DrawableGroup bkg = null;
		final byte BKG_FLOOR_INDEX = 0;
		
		//#if SCREEN_SIZE == "SMALL"
			final int BKG_HEIGHT = 49;
			final byte PUBLIC_OFFSET = -5;
		//#elif SCREEN_SIZE == "BIG"
//# 		final int BKG_HEIGHT = 107;
//# 		final byte PUBLIC_OFFSET = -6;
		//#else // #if SCREEN_SIZE == "MEDIUM"
//# 		final int BKG_HEIGHT = 107;
		//#endif
			
		//#if ( SCREEN_SIZE == "SMALL" ) || ( SCREEN_SIZE == "BIG" )
			final byte extraSlots = 2;
		//#else
//# 		final byte extraSlots = 0;
		//#endif
			
		byte BKG_FLOOR_OFFSET = 0;
		final String dir = "/Backgrounds/", imgExt = ".png";
			
		if( level == 0 )
		{
			bkg = new DrawableGroup( 6 + extraSlots );
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

			final DrawableImage floorImg = new DrawableImage( dir + "floor_0" + imgExt );
			final Pattern bkgFloor = new Pattern( floorImg );
			bkg.insertDrawable( bkgFloor );
			bkgFloor.setSize( ScreenManager.SCREEN_WIDTH, floorImg.getHeight() );
			bkgFloor.setPosition( 0, BKG_HEIGHT );

			final Pattern bkgWall = new Pattern( 0xEAEBEE );
			bkg.insertDrawable( bkgWall );
			bkgWall.setSize( ScreenManager.SCREEN_WIDTH, BKG_HEIGHT );

			final DrawableImage baseBoardImage = new DrawableImage( dir + "baseBoard" + imgExt );
			final Pattern baseBoard = new Pattern( baseBoardImage );
			bkg.insertDrawable( baseBoard );
			baseBoard.setSize( ScreenManager.SCREEN_WIDTH, baseBoardImage.getHeight() );
			baseBoard.setPosition( 0 , BKG_HEIGHT );
			
			final DrawableImage crack = new DrawableImage( dir + "crack" + imgExt );
			bkg.insertDrawable( crack );
			crack.setPosition( ( ScreenManager.SCREEN_WIDTH - crack.getWidth() ) >> 1, StatusBar.STATUS_BAR_HEIGHT );
			
			final DrawableImage brick = new DrawableImage( dir + "brick" + imgExt );
			bkg.insertDrawable( brick );
			brick.setPosition( ( ScreenManager.SCREEN_HALF_WIDTH - brick.getWidth() ) >> 1, StatusBar.STATUS_BAR_HEIGHT + (( BKG_HEIGHT - StatusBar.STATUS_BAR_HEIGHT - brick.getHeight() ) >> 1 ));
			
			final DrawableImage tommy = new DrawableImage( dir + "tommy" + imgExt );
			bkg.insertDrawable( tommy );
			tommy.setPosition( ScreenManager.SCREEN_HALF_WIDTH + (( ScreenManager.SCREEN_HALF_WIDTH - tommy.getWidth() ) >> 1), baseBoard.getPosY() - tommy.getHeight() - TOMMY_DIST_FROM_BASEBOARD );
		}
		else if( level == 3 )
		{
			bkg = new DrawableGroup( 2 + extraSlots );
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

			// Parte do ch�o que fica por tr�s da cortina
			BKG_FLOOR_OFFSET = 2;
			final DrawableImage floorImg = new DrawableImage( dir + "floor_1" + imgExt );
			final Pattern bkgFloor = new Pattern( floorImg );
			bkg.insertDrawable( bkgFloor );
			bkgFloor.setSize( ScreenManager.SCREEN_WIDTH, floorImg.getHeight() + BKG_FLOOR_OFFSET );
			bkgFloor.setPosition( 0, BKG_HEIGHT - BKG_FLOOR_OFFSET );

			final Pattern bkgWall = new Pattern( new DrawableImage( dir + "curtain" + imgExt ) );
			bkg.insertDrawable( bkgWall );
			bkgWall.setSize( ScreenManager.SCREEN_WIDTH, BKG_HEIGHT );
		}
		else if( level == 9 )
		{
			bkg = new DrawableGroup( 4 + extraSlots );
			bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

			final DrawableImage floorImg = new DrawableImage( dir + "floor_2" + imgExt );
			final Pattern bkgFloor = new Pattern( floorImg );
			bkg.insertDrawable( bkgFloor );
			bkgFloor.setSize( ScreenManager.SCREEN_WIDTH, floorImg.getHeight() );
			bkgFloor.setPosition( 0, BKG_HEIGHT );
			
			final Pattern bkgWall = new Pattern( new DrawableImage( dir + "olimpic" + imgExt ) );
			bkg.insertDrawable( bkgWall );
			bkgWall.setSize( ScreenManager.SCREEN_WIDTH, BKG_HEIGHT );
			
			final DrawableImage rings = new DrawableImage( dir + "olimpicRings" + imgExt );
			bkg.insertDrawable( rings );
			final byte RINGS_OFFSET_Y = -3;
			rings.setPosition( ( ScreenManager.SCREEN_WIDTH - rings.getWidth() ) >> 1, StatusBar.STATUS_BAR_HEIGHT + RINGS_OFFSET_Y + (( BKG_HEIGHT - StatusBar.STATUS_BAR_HEIGHT - rings.getHeight() ) >> 1 ));

			final DrawableImage baseBoardImage = new DrawableImage( dir + "mtvOlimpic" + imgExt );
			final Pattern baseBoard = new Pattern( baseBoardImage );
			bkg.insertDrawable( baseBoard );
			baseBoard.setSize( ScreenManager.SCREEN_WIDTH, baseBoardImage.getHeight() );
			baseBoard.setPosition( 0 , BKG_HEIGHT );
		}
		
		//#if ( SCREEN_SIZE == "SMALL" ) || ( SCREEN_SIZE == "BIG" )
			if(( level == 0 ) || ( level == 3 ) || ( level == 9 ))
			{
				final int stageY = BKG_HEIGHT + bkg.getDrawable( BKG_FLOOR_INDEX ).getHeight() - BKG_FLOOR_OFFSET;
				if( stageY < ScreenManager.SCREEN_HEIGHT )
				{
					final DrawableImage stageImage = new DrawableImage( dir + "stage" + imgExt );
					final Pattern stage = new Pattern( stageImage );
					bkg.insertDrawable( stage );
					stage.setSize( ScreenManager.SCREEN_WIDTH, stageImage.getHeight() );
					stage.setPosition( 0, stageY );

					final int crowdY = stage.getPosY() + stage.getHeight() + PUBLIC_OFFSET;
					if( crowdY < ScreenManager.SCREEN_HEIGHT )
					{
						final DrawableImage crowdImage = new DrawableImage( dir + "public" + imgExt );
						final Pattern crowd = new Pattern( crowdImage );
						bkg.insertDrawable( crowd );
						crowd.setSize( ScreenManager.SCREEN_WIDTH, crowdImage.getHeight() );
						crowd.setPosition( 0, crowdY );
					}
				}
			}
		//#endif
		
		return bkg;
	}
	
	/** Aloca as m�os que indicam que o personagem deve apertar os bot�es */
	private final Drawable createPressingButtonHand( byte screenSide ) throws Exception
	{
		//#if SCREEN_SIZE == "SMALL"
		
			final Sprite hand;
			
			if( screenSide == ITEM_LEFT )
			{
				final String path = "/button4";
				hand = new Sprite( path + ".bin", path );
			}
			else // if( screenSide == ITEM_RIGHT )
			{
				final String path = "/button6";
				hand = new Sprite( path + ".bin", path );
			}
			
			return hand;
			
		//#else
//# 		
//# 			// Cria o grupo
//# 			final UpdatableGroup group = new UpdatableGroup( 2 );
//# 
//# 			// Cria a m�o que aperta o bot�o
//# 			final Sprite hand;
//# 			final byte GROUP_HAND_INDEX = 0;
//# 			if( screenSide == ITEM_RIGHT )
//# 			{
//# 				hand = new Sprite( "/buttonHand.bin", "/buttonHand" );
//# 				hand.setSequence( 1 );
//# 			}
//# 			else
//# 			{
//# 				hand = new Sprite(( Sprite )((( UpdatableGroup )buttonHands[ ITEM_RIGHT ] ).getDrawable( GROUP_HAND_INDEX )));
//# 				hand.mirror( TRANS_MIRROR_H );
//# 			}
//# 
//# 			group.insertDrawable( hand );
//# 
//# 			// Cria o bot�o
//# 			int dx = 0;
//# 			final DrawableImage button;
//# 			if( screenSide == ITEM_RIGHT )
//# 			{
//# 				button = new DrawableImage( "/button6.png" );
//# 			}
//# 			else
//# 			{
//# 				button = new DrawableImage( "/button4.png" );
//# 				dx = hand.getWidth() - button.getWidth();
//# 			}
//# 
//# 			group.insertDrawable( button );
//# 
//# 			button.move( dx, hand.getHeight() );
//# 
//# 			// Determina o tamanho do grupo
//# 			group.setSize( hand.getWidth(), hand.getHeight() + button.getHeight() );
//# 
//# 			return group;
//# 		
		//#endif
	}
	
	private final void skipTransition3()
	{
		if( currState == STATE_TRANSITION_3 )
		{
			transitionFold.setSequence( TRANSITION_FOLD_SEQ_STOPPED ); // P�ra a anima��o. Ver descritor correspondente
			transitionFold.setVisible( false );

			transitionLabel.setVisible( false );
			transitionPaper.setVisible( false );

			setState( STATE_TRANSITION_4 );
		}
	}
	
	public final void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		if( ( key == ScreenManager.KEY_BACK ) || ( key == ScreenManager.KEY_CLEAR ) || ( key == ScreenManager.KEY_SOFT_RIGHT ) )
		{
			pause();
			return;
		}
		
		if( currState == STATE_TRANSITION_3 )
		{
			skipTransition3();
			return;
		}
		
		if( currState > STATE_RUNNING_BT_5 )
			return;

		//#if DEBUG == "true"
//# 		try
//# 		{
			//#endif

			switch( key )
			{
				case ScreenManager.KEY_NUM4:
					putStrength( ITEM_LEFT );
					break;

				case ScreenManager.KEY_NUM6:
					putStrength( ITEM_RIGHT );
					break;

				case ScreenManager.FIRE:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_SOFT_LEFT:
					try
					{
						liftWeight();
					}
					catch( Exception ex )
					{
						// N�o � para acontecer...
						GameMIDlet.exit();
					}
					break;
			}

		//#if DEBUG == "true"
//# 		}
//# 		catch( Exception e )
//# 		{
//# 			e.printStackTrace();
//# 		}
	//#endif
	}

	public void keyReleased( int key )
	{
	}

	/** Acrescenta um pouco de for�a no bra�o recebido como par�metro */
	private final void putStrength( int arm )
	{
		if( currState > STATE_RUNNING_BT_5 )
			return;
			
		//#ifdef SYNCHRONIZE_ARMS
//# 			// Se recebemos um evento para o bra�o bloqueado, sai
//# 			if( heldArm == arm )
//# 				return;
		//#endif

		// Faz o bra�o recebido como par�metro ganhar for�a
		strengthBars[ arm ].putStrength();

		//#ifdef SYNCHRONIZE_ARMS
//# 			// ( arm + 1 ) & 1 == ( arm + 1 ) % 2
//# 			holdArm( ( byte )( ( arm + 1 ) & 1 ) );
		//#endif

		// TODO : Quer�amos que o jogador pudesse perder uma vida por um disnivelamento muito grande da for�a dos
//		
//		// Se a diferen�a de for�as est� grande, abaixo do pr�ximo n�vel e o peso j� saiu do ch�o...
//		// Indica que bra�o deve imprimir mais for�a
//		if( !strengthBars[ ITEM_LEFT ].isBlinking() )
//			strengthBars[ ITEM_LEFT ].setBlink( true );
//		
//		if( !strengthBars[ ITEM_RIGHT ].isBlinking() )
//			strengthBars[ ITEM_RIGHT ].setBlink( true );

		// Se a diferen�a entre as for�as for maior que MAX_ARMS_STR_DIFF, perde uma vida
//		if( Math.abs( strengthBars[ ITEM_LEFT ].getStrength() - strengthBars[ ITEM_RIGHT ].getStrength() ) > MAX_ARMS_STR_DIFF )
//		{
//			// TODO : Quer�amos fazer uma anima��o do personagem largando a barra. Infelizmente n�o houve tempo
//			onLifeLost();
//		}
//		else
//		{
			// Movimenta os bra�os do personagem
			moveDetonatorArm( ( byte )arm );
//		}
	}

	/** Levanta o peso, passsando de etapa na fase atual */
	//#if defined( PLEASE_HELP_ME )
//# private final void liftWeight() throws Exception
	//#else
	private synchronized final void liftWeight() throws Exception
	//#endif
	{
		if( currState != STATE_RUNNING_BT_5 )
			return;
		
		setState( STATE_RUNNING );
		
		// Ganha pontos por completar a etapa pela 1a vez
		final byte currStep = strengthBars[ ITEM_LEFT ].getStep();
		if( !stepReached[ currStep ] )
		{
			stepReached[ currStep ] = true;
			
			final short STEP_COMPLETED_POINTS = 417;
			statusBar.changeScore( STEP_COMPLETED_POINTS );
		}
		
		// Marca a pr�xima etapa nas barras de for�a
		final byte nextStep = ( byte )( currStep + 1 );
		strengthBars[ ITEM_LEFT ].setStep( nextStep );
		strengthBars[ ITEM_RIGHT ].setStep( nextStep );
		
		// Muda o estado do personagem
		changeDetonatorState();
			
		// OBS: Ambas as barras est�o na mesma etapa, ent�o s� precisamos testar uma
		if( nextStep >= N_STRENGTH_BAR_STEPS )
		{
			giveLevelCompletedPoints();
			setState( STATE_LEVEL_ENDED );
		}
	}
	
	/** Calcula quanto o jogador deve ganhar por terminar a fase atual */
	private final void giveLevelCompletedPoints()
	{
		int points;
		
		if( level > 3 )
			points = level * 250;
		else if( level > 0 )
			points = ( level + 1 ) * 250;
		else
			points = 289;
		
		// B�nus por tempo
		final byte N_POINTS_PER_SEC = 37;
		points += statusBar.getTime() * N_POINTS_PER_SEC;
		
		// B�nus pelo n�mero de vidas atual
		final short N_POINTS_PER_LIFE = 157;
		points += statusBar.getLives() * N_POINTS_PER_LIFE;

		statusBar.changeScore( points );
	}

	//#ifdef SYNCHRONIZE_ARMS
//# 
//# 		/** Bloqueia o input do bra�o recebido como par�metro */
//# 		private final void holdArm( byte arm )
//# 		{
//# 			heldArm = arm;
//# 			if( heldArmCounter <= 0 )
//# 				heldArmCounter = HELD_ARM_DEFAULT_TIME;
//# 		}
//# 
	//#endif
	
	/** M�todo chamado pelas barras de for�a para indicar quando uma etapa foi completada */
	//#if defined( PLEASE_HELP_ME )
//# 	public void onStepCompleted( boolean completed )
	//#else
		public synchronized void onStepCompleted( boolean completed )
	//#endif
	{
		if( completed )
		{
			if( currState == STATE_RUNNING )
			{
				// Se as duas barras est�o OK, mostramos o indicador de que o jogador pode levantar o peso
				if( strengthBars[ ITEM_LEFT ].isStepCompleted() && strengthBars[ ITEM_RIGHT ].isStepCompleted() && ( strengthBars[ ITEM_LEFT ].getStep() == strengthBars[ ITEM_RIGHT ].getStep() ) )
					setState( STATE_RUNNING_BT_5 );
			}
		}
		else
		{
			if( currState == STATE_RUNNING_BT_5 )
				setState( STATE_RUNNING );
		}
	}
	
	/** M�todo chamado pelas barras de for�a para indicar que a etapa foi retrocedida */
	//#if defined ( PLEASE_HELP_ME )
//# 	public void onStepLowered( boolean stepChanged )
	//#else
		public synchronized void onStepLowered( boolean stepChanged )
	//#endif
	{
		if( currState > STATE_RUNNING_BT_5 )
			return;
		
		// N�o pode mais apertar o bot�o para levantar o peso
		if( currState == STATE_RUNNING_BT_5 )
			setState( STATE_RUNNING );
		
		// Se mudou de etapa
		if( stepChanged )
		{
			// Vibra
			MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
			
			// Sempre perde pontos por deixar baixar uma etapa
			final byte STEP_LOWERED_N_POINTS = -23;
			statusBar.changeScore( STEP_LOWERED_N_POINTS );

			// Deixa as duas barras de for�a no mesmo n�vel
			final byte leftStep = strengthBars[ ITEM_LEFT ].getStep();
			final byte rightStep = strengthBars[ ITEM_RIGHT ].getStep();

			if( leftStep < rightStep )
				strengthBars[ ITEM_RIGHT ].setStep( leftStep );
			else if( leftStep > rightStep )
				strengthBars[ ITEM_LEFT ].setStep( rightStep );

			// Muda o estado do personagem
			changeDetonatorState();
		}
	}
	
	/** M�todo chamado pelas barras de for�a para indicar perda de for�a */
	public void onLoseStrength( StrengthBar caller )
	{
		// Movimenta os bra�os do personagem
		if( currState <= STATE_RUNNING_BT_5 )
			moveDetonatorArm( caller == strengthBars[ ITEM_LEFT ] ? ITEM_LEFT : ITEM_RIGHT );
	}
	
	/** Muda o estado do personagem de acordo com a etapa atual da for�a */
	private final void changeDetonatorState()
	{
		if( currState == STATE_LOST_LIFE_1 )
		{
			detonator.setState( Detonator.DETONATOR_STATE_SAD );
			return;
		}
		
		// OBS: Ambas as barras est�o na mesma etapa, ent�o s� precisamos testar uma
		switch( strengthBars[ ITEM_LEFT ].getStep() )
		{
			case 0:
				detonator.setState( Detonator.DETONATOR_STATE_1ST_STEP );
				break;
				
			case 1:
				detonator.setState( Detonator.DETONATOR_STATE_2ND_STEP );
				break;
				
			case 2:
				detonator.setState( Detonator.DETONATOR_STATE_2ND_TO_3RD_STEP );
				break;
				
			case 3:
				//#ifndef NO_SOUND
				MediaPlayer.free();
				MediaPlayer.play( SOUND_INDEX_LEVEL_COMPLETED, 1 );
				//#endif
		
				detonator.setState( Detonator.DETONATOR_STATE_FINISHED_0 );
				break;
		}
	}

	/** Movimenta os bra�os do personagem de acordo com a porcentagem conclu�da da etapa atual */
	private final void moveDetonatorArm( byte arm )
	{
		detonator.setArmState( arm , strengthBars[ arm ].getStep(), strengthBars[ arm ].getStepPercentage() );
	}
	
	/** Chama a tela do menu de pausa */
	private final void pause()
	{
		if( currState != STATE_PAUSED )
		{
			setState( STATE_PAUSED );
			GameMIDlet.setScreen( GameMIDlet.SCREEN_PAUSE );
		}
	}
	
	/** Retoma o jogo */
	public final void unpause()
	{
		// N�o chama setState( prevState ) pois repetir�amos anima��es
		if( currState == STATE_PAUSED )
			currState = prevState;
	}

	/** M�todo chamado quando o jogador perde uma vida */
	//#if defined ( PLEASE_HELP_ME )
//# 	private final void onLifeLost()
	//#else
		private synchronized final void onLifeLost()
	//#endif
	{
		//#ifndef NO_SOUND
		MediaPlayer.free();
		MediaPlayer.play( SOUND_INDEX_LIFE_LOST, 1 );
		//#endif
				
		setState( STATE_LOST_LIFE_1 );
		statusBar.setLives( ( byte )( statusBar.getLives() - 1 ) );
		changeDetonatorState();
	}

	public final void update( int delta )
	{
		//#if DEBUG == "true"
//# 		try
//# 		{
		//#endif

			switch( currState )
			{
				case STATE_RUNNING_BT_5:
					final Sprite bt5 = ( Sprite )buttonHands[ ITEM_CENTER ];
					if( bt5.getSequence() == BT5_SEQ_GOING_DOWN )
					{
						final int bt5Height = bt5.getHeight();
						//#if SCREEN_SIZE == "MEDIUM"
//# 						final int detonatorEnd = detonator.getPosY() + detonator.getHeight();
//# 						int nextY = detonatorEnd + ( ( ScreenManager.SCREEN_HEIGHT - detonatorEnd - buttonHands[ITEM_CENTER].getHeight() ) >> 1 );
						//#else
							final byte BKG_FLOOR_INDEX = 0;
							final Drawable bkgFloor = (( DrawableGroup )drawables[ PLAYSCREEN_BACKGROUND_INDEX ]).getDrawable( BKG_FLOOR_INDEX );
							int nextY = bkgFloor.getPosY() + bkgFloor.getHeight() - bt5Height;
						//#endif
							
						if( nextY + bt5Height >= ScreenManager.SCREEN_HEIGHT )
							nextY = ScreenManager.SCREEN_HEIGHT - bt5Height;
						
						final int dy = transitionControl.updateInt( delta );
						final int currY = buttonHands[ ITEM_CENTER ].getPosY();
						
						if( currY + dy < nextY )
						{
							buttonHands[ ITEM_CENTER ].setPosition( buttonHands[ ITEM_CENTER ].getPosX(), currY + dy );
						}
						else
						{
							buttonHands[ ITEM_CENTER ].setPosition( buttonHands[ ITEM_CENTER ].getPosX(), nextY );
							bt5.setSequence( BT5_SEQ_BLINKING_SMALL );
						}
					}
					
				case STATE_RUNNING:
					final int currTime = statusBar.getTime();
					if( currTime <= 0 )
					{
						onLifeLost();
						return;
					}
					// Se o tempo da fase est� acabando, come�a a piscar as barras de for�a
					else if( currTime <= TIME_IS_SHORT )
					{
						// Verifica que barra est� abaixo do �ltimo n�vel da fase
						strengthBars[ITEM_LEFT].setBlink( strengthBars[ITEM_LEFT].getStep() < StrengthBar.N_STRENGTH_BAR_STEPS - 1 );
						strengthBars[ITEM_RIGHT].setBlink( strengthBars[ITEM_RIGHT].getStep() < StrengthBar.N_STRENGTH_BAR_STEPS - 1 );
					}
					
					// Controla o passarinho
					updateBirdie( delta, currTime );

					// Atualiza os componentes do grupo
					super.update( delta );

					//#ifdef SYNCHRONIZE_ARMS
//# 						// Se existe algum bra�o para ser bloqueado, efetua o bloqueio
//# 						if( heldArm != ITEM_NONE )
//# 						{
//# 							heldArmCounter -= delta;
//# 
//# 							if( heldArmCounter < 0 )
//# 								heldArm = ITEM_NONE;
//# 						}
					//#endif
					break;
					
				case STATE_LOST_LIFE_1:
				case STATE_LEVEL_ENDED:
					birdie.update( delta );
					detonator.update( delta );
					statusBar.updateScore( delta, false );
					
					lastTransitionDx -= delta; // Usa lastTransitionDx como contador de tempo
					if( lastTransitionDx <= 0 )
						setState( currState + 1 );
					
					break;

				case STATE_TRANSITION_5:
					lastTransitionDx -= delta; // Usa lastTransitionDx como contador de tempo
					if( lastTransitionDx <= 0 )
					{
						getDrawable( PLAYSCREEN_GO_INDEX ).setVisible( false );
						buttonHands[ ITEM_LEFT ].setVisible( true );
						buttonHands[ ITEM_RIGHT ].setVisible( true );
						setState( STATE_RUNNING );
					}
					break;
					
				case STATE_TRANSITION_4:
					
					// Coloca a barra de status na tela
					int done = 0;
					final int statusBarY = statusBar.getPosY();

					if( statusBarY != 0 )
					{
						final int dy = statusBarSpeed.updateInt( delta );
						if( statusBarY + dy < 0 )
						{
							statusBar.setPosition( statusBar.getPosX(), statusBarY + dy );
							final int doubleDY = dy << 1;
							transitionFold.setPosition( transitionFold.getPosX(), transitionFold.getPosY() - doubleDY );
							transitionLabel.setPosition( transitionLabel.getPosX(), transitionLabel.getPosY() - doubleDY );
							transitionPaper.setPosition( transitionPaper.getPosX(), transitionPaper.getPosY() - doubleDY );
						}
						else
						{
							statusBar.setPosition( 0, 0 );
							transitionFold.setVisible( false );
							transitionLabel.setVisible( false );
							transitionPaper.setVisible( false );
						}
					}
					else
					{
						++done;
					}
					
					final int strengthBarX = strengthBars[ ITEM_LEFT ].getPosX();
					if( strengthBarX < STR_BAR_DIST_FROM_SCREEN )
					{
						final int dx = transitionControl.updateInt( delta );
						final int strengthBarWidth = strengthBars[ ITEM_RIGHT ].getWidth();
						if( strengthBarX + dx < STR_BAR_DIST_FROM_SCREEN )
						{
							strengthBars[ ITEM_LEFT ].setPosition( strengthBarX + dx, strengthBars[ ITEM_LEFT ].getPosY() );
							strengthBars[ ITEM_RIGHT ].setPosition( strengthBars[ ITEM_RIGHT ].getPosX() - dx, strengthBars[ ITEM_RIGHT ].getPosY() );
						}
						else
						{
							strengthBars[ ITEM_LEFT ].setPosition( STR_BAR_DIST_FROM_SCREEN, strengthBars[ ITEM_LEFT ].getPosY() );
							strengthBars[ ITEM_RIGHT ].setPosition( ScreenManager.SCREEN_WIDTH - strengthBarWidth - STR_BAR_DIST_FROM_SCREEN, strengthBars[ ITEM_RIGHT ].getPosY() );
						}
					}
					else
					{
						++done;
					}
					
					if( detonator.getState() < Detonator.DETONATOR_STATE_STANDING_TO_1ST_STEP )
						detonator.setState( Detonator.DETONATOR_STATE_STANDING_TO_1ST_STEP );
					
					detonator.update( delta );
					
					if( done == 2 )
						setState( STATE_TRANSITION_5 );
					
					break;

				case STATE_TRANSITION_3:
					if( transitionPaper.getWidth() >= ScreenManager.SCREEN_WIDTH )
					{
						transitionFold.setSequence( TRANSITION_FOLD_SEQ_STOPPED ); // P�ra a anima��o. Ver descritor correspondente
						transitionFold.setVisible( false );
						
 						lastTransitionDx -= delta; // Usa lastTransitionDx como contador de tempo
						if( lastTransitionDx <= 0 )
							setState( STATE_TRANSITION_4 );
					}
					else
					{
						transitionFold.update( delta );
					}
					break;

				case STATE_LOST_LIFE_2:
				case STATE_RESETING_LEVEL:
				case STATE_TRANSITION_1:
				case STATE_TRANSITION_2:
					lastTransitionDx += transitionControl.updateInt( delta );
					birdie.update( delta );
					detonator.update( delta );
					break;
			}

		//#if DEBUG == "true"
//# 		}
//# 		catch( Exception e )
//# 		{
//# 			e.printStackTrace();
//# 		}
		//#endif
	}
	
	/** Controla o passarinho */
	private void updateBirdie( int delta, int currLevelTime )
	{
		final byte birdieState = birdie.getState();
		if( birdieState == Birdie.BIRDIE_STATE_NONE )
		{
			if( birdieControlCounter >= MAX_BIRDIE_APPEARENCES_BY_LEVEL || birdieControl[ birdieControlCounter ] == -1 )
				return;
			
			// Verifica se o passarinho deve entrar na tela
			final int percent = ( currLevelTime * 100 / TOTAL_LEVEL_TIME );
			if( birdieControl[ birdieControlCounter ] >= percent )
			{
				++birdieControlCounter;
				
				// Determina quanto tempo vai ficar pulando
				birdieJumpControl = BIRDIE_JUMP_TIME_MIN + NanoMath.randInt( BIRDIE_JUMP_TIME_MAX - BIRDIE_JUMP_TIME_MIN + 1 );
				
				// Tem 20% de chance de variar a dificuldade
				byte difficulty = ( byte )( level / ( MAX_LEVEL / Birdie.N_BIRDIE_DIFFICULTIES ));
				if( NanoMath.randInt( 100 ) < 20 )
					difficulty = ( byte )NanoMath.randInt( difficulty + 1 );

				// Determina a dificuldade
				birdie.setDifficulty( difficulty );
				
				// Manda entrar na tela
				birdie.enterScreen();
			}
		}
		else if( birdieState == Birdie.BIRDIE_STATE_JUMPING )
		{
			// Verifica se o passarinho deve sair da tela
			birdieJumpControl -= delta;
			if( birdieJumpControl <= 0 )
				birdie.goAway();
		}
	}
	
	public void onSequenceEnded( int id, int sequence )
	{
		switch( id )
		{
			case PLAYSCREEN_PAPERFOLD_INDEX:
				final int foldWidthClosed = transitionFold.getFrameImage( sequence, 0 ).getWidth();
				final int paperWidth = transitionPaper.getWidth() + foldWidthClosed;
				transitionPaper.setSize( paperWidth, transitionPaper.getHeight() );

				transitionFold.setPosition( paperWidth - foldWidthClosed, 0 );
				
				break;
				
			case PLAYSCREEN_BUTTON5_INDEX:
				if( sequence < BT5_SEQ_BLINKING_SMALL )
					( ( Sprite )buttonHands[ ITEM_CENTER ] ).setSequence( ( byte )( sequence + 1 ) );
				break;
		}
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
		switch( id )
		{
			case PLAYSCREEN_PAPERFOLD_INDEX:
				int width = transitionFold.getPosX() + transitionFold.getFrameOffset( transitionFold.getCurrFrameIndex() ).x;
				if( width < transitionLabel.getWidth() )
					width = transitionLabel.getWidth();
				transitionLabel.setSize( width, transitionLabel.getHeight() );
				
				break;
		
			case PLAYSCREEN_BUTTON5_INDEX:
				break;
		}
	}
	
	public final int getScore()
	{
		return statusBar.getScore();
	}

	//#if defined ( PLEASE_HELP_ME )
//# 	public final void setState( int state )
	//#else
public synchronized final void setState( int state )
	//#endif
	{
		switch( state )
		{
			case STATE_RUNNING:
				buttonHands[ITEM_CENTER].setVisible( false );
				(( Sprite )buttonHands[ITEM_CENTER]).setSequence( BT5_SEQ_STOPPED );
				break;

			case STATE_RUNNING_BT_5:
				if( !buttonHands[ITEM_CENTER].isVisible() )
				{
					final int detonatorEnd = detonator.getPosY() + detonator.getHeight();
					final int nextY = detonatorEnd + ( ( ScreenManager.SCREEN_HEIGHT - detonatorEnd - buttonHands[ITEM_CENTER].getHeight() ) >> 1 );
					final int currY = ( ScreenManager.SCREEN_HEIGHT - buttonHands[ITEM_CENTER].getHeight() ) >> 1;
					
					lastTransitionDx = 0;
					transitionControl.setSpeed( ( nextY - currY ) * 5, true );
					
					buttonHands[ITEM_CENTER].setPosition( ( ScreenManager.SCREEN_WIDTH - buttonHands[ITEM_CENTER].getWidth() ) >> 1, currY );
					(( Sprite )buttonHands[ITEM_CENTER]).setSequence( BT5_SEQ_BLINKING_BIG );
					buttonHands[ITEM_CENTER].setVisible( true );
				}
				break;

			case STATE_LOST_LIFE_1:
			case STATE_LEVEL_ENDED:
				lastTransitionDx = STATE_LEVEL_ENDED_TIME; // Usa lastTransitionDx como contador de tempo
				
				// Faz o passarinho sair da tela
				birdie.goAway();
		
				// Deixa as m�os dos bot�es invis�veis
				buttonHands[ ITEM_LEFT ].setVisible( false );
				buttonHands[ ITEM_RIGHT ].setVisible( false );
				buttonHands[ ITEM_CENTER ].setVisible( false );
				
				// P�ra de piscar as barras de for�a
				strengthBars[ ITEM_LEFT ].setBlink( false );
				strengthBars[ ITEM_LEFT ].noMarks();
				strengthBars[ ITEM_RIGHT ].setBlink( false );
				strengthBars[ ITEM_RIGHT ].noMarks();

				break;

			case STATE_LOST_LIFE_2:
			case STATE_TRANSITION_1:
				
				// Garante que o passarinho est� fora da tela
				birdie.reset();
				
				lastTransitionDx = 0;
				transitionControl.setSpeed( ScreenManager.SCREEN_WIDTH / TRANSITION_N_SLICES, true );
				break;
		
			case STATE_RESETING_LEVEL:
			case STATE_TRANSITION_2:
				try
				{
					if( state == STATE_RESETING_LEVEL )
					{
						if( statusBar.getLives() < 0 )
							GameMIDlet.gameOver( statusBar.getScore() >> 1 );
						else
							resetLevel( true );
					}
					else
					{
						if( !onNewLevel() )
						{
							// TODO : Era para transitar para uma tela de zeramento antes de ir para a tela de
							// de recordes. Infelizmente n�o houve tempo
							GameMIDlet.gameOver( statusBar.getScore() );
							return;
						}
					}
				}
				catch( Exception ex )
				{
					//#if DEBUG == "true"
//# 						ex.printStackTrace();
					//#endif
						
					// N�o � para acontecer, mas...
					GameMIDlet.exit();
				}
				
				lastTransitionDx = 0;
				transitionControl.setSpeed( ScreenManager.SCREEN_HEIGHT, true );
				break;
				
			case STATE_TRANSITION_3:
				lastTransitionDx = TRANSITION_3_TIME; // Usa lastTransitionDx como contador de tempo
				
				// Posiciona os elementos da transi��o
				transitionFold.setPosition( 0, 0 );
				transitionFold.setVisible( true );
				transitionFold.setSequence( TRANSITION_FOLD_SEQ_PLAYING );

				final int foldWidthClosed = transitionFold.getFrameImage( 0, 0 ).getWidth();
				transitionPaper.setPosition( 0, 0 );
				transitionPaper.setSize( foldWidthClosed, transitionPaper.getHeight() );
				transitionPaper.setVisible( true );
				
				transitionLabel.setPosition( 0, ( transitionPaper.getFill().getHeight() - transitionLabel.getFont().getHeight() ) >> 1 );
				transitionLabel.setSize( 0, transitionLabel.getHeight() );
				transitionLabel.setVisible( true );
				
				break;
				
			case STATE_TRANSITION_4:
				lastTransitionDx = 0;
				transitionControl.setSpeed( ( STR_BAR_DIST_FROM_SCREEN + strengthBars[ ITEM_LEFT ].getWidth() )* 10, true );

				//#if SCREEN_SIZE != "BIG"
					// Exibe novamente a softKey de pausa
					GameMIDlet.showSoftKeys();
				//#endif
					
				break;
				
			case STATE_TRANSITION_5:
				lastTransitionDx = TRANSITION_5_TIME; // Usa lastTransitionDx como contador de tempo
				getDrawable( PLAYSCREEN_GO_INDEX ).setVisible( true );
				break;
				
			case STATE_PAUSED:
				prevState = currState;
				break;

			//#if DEBUG == "true"
//# 			default:
//# 				System.out.println( "PlayScreen.setState->estado inv�lido: " + state );
			//#endif
		} // fim switch ( currState )

		currState = ( byte ) state;
	}

	protected void paint( Graphics g )
	{
		switch( currState )
		{
			case STATE_TRANSITION_1:
			case STATE_LOST_LIFE_2:
				super.paint( g );
				paintTransition( g );
				break;
			
			case STATE_TRANSITION_2:
			case STATE_RESETING_LEVEL:
				if( lastTransitionDx > 0 )
					super.paint( g );
				
				paintTransition( g );
				break;
				
			default:
				super.paint( g );
		}
	}
	
	private void paintTransition( Graphics g )
	{
		g.setColor( 0xFF000000 );
		
		if( ( currState == STATE_TRANSITION_1 ) || ( currState == STATE_LOST_LIFE_2 ) )
		{
			final short sliceWidth = ( short )( ScreenManager.SCREEN_WIDTH / TRANSITION_N_SLICES );
			int x = 0;
			
			// +1 => Evita imprecis�es na divis�o da largura da tela
			for( byte i = 0 ; i < TRANSITION_N_SLICES + 1 ; ++i )
			{
				g.fillRect( x, 0, lastTransitionDx, ScreenManager.SCREEN_HEIGHT );
				x += sliceWidth;
			}
			
			// Garante que fechou a cortina
			final byte JUST_TO_GET_SURE = 5;
			if( lastTransitionDx > sliceWidth + JUST_TO_GET_SURE )
				setState( currState + 1 );
		}
		else // if( currState == STATE_TRANSITION_2 ) || ( currState == STATE_RESETING_LEVEL )
		{
			final int height = ScreenManager.SCREEN_HEIGHT - lastTransitionDx;
			if( height > 0 )
				g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, height );
			else
				setState( STATE_TRANSITION_3 );
		}
	}

	public void onBirdieStateChanged( byte state, byte side )
	{
		if( state == Birdie.BIRDIE_STATE_JUMPING )
		{
			final byte difficulty = birdie.getDifficulty();
			//#if defined( BigEL71 ) || defined( BigA1200 ) || defined( SmallC420 ) || defined( BigFlushGraphics ) || defined( PLEASE_HELP_ME ) || defined( SmallNokia6822 ) || defined( SmallSamsungWidescreen )
//# 			strengthBars[ side ].changeLevelDifficulty( strengthBars[ side ].strengthGainFactor, strengthBars[ side ].strengthLossFactor + difficulty + 1, strengthBars[ side ].strengthDuration );
			//#else
				strengthBars[ side ].changeLevelDifficulty( strengthBars[ side ].strengthGainFactor - ( difficulty > 1 ? 1 : 0 ) , strengthBars[ side ].strengthLossFactor + difficulty + 1, strengthBars[ side ].strengthDuration );
			//#endif
		}
		else if( state == Birdie.BIRDIE_STATE_LEAVING )
		{
			strengthBars[ side ].setDefaultDifficulty();
		}
	}
	
	//#ifndef LOW_JAR
	
	public final void onPointerDragged( int x, int y )
	{
	}

	public final void onPointerPressed( int x, int y )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		if( currState == STATE_TRANSITION_3 )
		{
			skipTransition3();
			return;
		}
		
		if( currState > STATE_RUNNING_BT_5 )
			return;

		final int touchAreaWidth = ScreenManager.SCREEN_WIDTH / 3;
		if( x < touchAreaWidth )
		{
			putStrength( ITEM_LEFT );
		}
		else if( x < ( touchAreaWidth << 1 ) )
		{
			try
			{
				liftWeight();
			}
			catch( Exception ex )
			{
			}
		}
		else
		{
			putStrength( ITEM_RIGHT );
		}
	}

	public final void onPointerReleased( int x, int y )
	{
	}

	//#endif
}
