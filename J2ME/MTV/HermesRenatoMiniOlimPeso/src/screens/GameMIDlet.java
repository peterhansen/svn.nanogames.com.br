/**
 * GameMIDlet.java
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.AnimatedSoftkey;
import core.Constants;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * �2007 Nano Games
 * @author Daniel L. Alves
 */

public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener
{	
	/** Gerenciador da anima��o da soft key esquerda */
	private final AnimatedSoftkey softkeyLeft;
	
	/** Gerenciador da anima��o da soft key direita */
	private final AnimatedSoftkey softkeyRight;

	/** Refer�ncia para a tela de jogo */
	private PlayScreen playScreen;
	
	/** Fonte padr�o do jogo */
	private ImageFont FONT_DEFAULT;
	private final byte DEFAULT_FONT_OFFSET = 1;
	
	/** N�mero de �cones que a aplica��o possui */
	private final byte N_GAME_ICONS = 3;
	
	/** �cones da aplica��o */
	private final Drawable[] icons = new Drawable[ N_GAME_ICONS ];
	
	/** Fundo utilizado nos menus. Deixa pr�-alocado para melhorar o tempo de carregamento das telas de menus */
	public DrawableImage TOP_LEFT;
	public Pattern TOP_CENTER;
	public DrawableImage TOP_RIGHT;
	
	public Pattern MIDDLE_LEFT;
	public Pattern MIDDLE_CENTER;
	public Pattern MIDDLE_RIGHT;
	
	public DrawableImage BOTTOM_LEFT;
	public Pattern BOTTOM_CENTER;
	public DrawableImage BOTTOM_RIGHT;
	
	public DrawableImage SKULL;
	public DrawableImage METAL_HAND;
	public DrawableImage CLIP;
	public DrawableImage TITLE;
		
	/** Tempo que as soft keys ficam vis�veis na tela */
	private static final short SOFT_KEY_VISIBLE_TIME = 1000;
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos �cones da aplica��o">
	
	public static final byte ICON_BACK	= 0;
	public static final byte ICON_OK	= 1;
	public static final byte ICON_PAUSE	= 2;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das telas do jogo">
	public static final byte SCREEN_LOAD_RESOURCES		= 0;
	public static final byte SCREEN_ENABLE_SOUNDS		= 1;
	public static final byte SCREEN_SPLASH_NANO			= 2;
	public static final byte SCREEN_SPLASH_BRAND		= 3;
	public static final byte SCREEN_SPLASH_GAME			= 4;
	public static final byte SCREEN_MAIN_MENU			= 5;
	public static final byte SCREEN_LOADING				= 6;
	public static final byte SCREEN_OPTIONS				= 7;
	public static final byte SCREEN_CREDITS				= 8;
	public static final byte SCREEN_HELP				= 9;
	public static final byte SCREEN_RECORDS				= 10;
	public static final byte SCREEN_GAME				= 11;
	public static final byte SCREEN_PAUSE				= 12;
	public static final byte SCREEN_GAMEOVER			= 13;
	public static final byte SCREEN_YN_ERASE_RECORDS	= 14;
	public static final byte SCREEN_YN_EXIT_GAME		= 15;
	public static final byte SCREEN_YN_BACK_TO_MENU		= 16;
	public static final byte SCREEN_UNPAUSE				= 17;
	//#if LOG == "true"
//#		public static final byte SCREEN_LOG				= 18;
	//#endif
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu principal">
	
	public static final byte MAIN_MENU_NEW_GAME		= 0;
	public static final byte MAIN_MENU_OPTIONS		= 1;
	public static final byte MAIN_MENU_RECORDS		= 2;
	public static final byte MAIN_MENU_HELP			= 3;
	public static final byte MAIN_MENU_CREDITS		= 4;
	public static final byte MAIN_MENU_EXIT			= 5;
	public static final byte MAIN_MENU_LOG			= 6;
	
	public static final byte MAIN_MENU_CONTINUING_NEW_GAME	= 0;
	public static final byte MAIN_MENU_CONTINUING_CONTINUE	= 1;
	public static final byte MAIN_MENU_CONTINUING_OPTIONS	= 2;
	public static final byte MAIN_MENU_CONTINUING_RECORDS	= 3;
	public static final byte MAIN_MENU_CONTINUING_HELP		= 4;
	public static final byte MAIN_MENU_CONTINUING_CREDITS	= 5;
	public static final byte MAIN_MENU_CONTINUING_EXIT		= 6;
	public static final byte MAIN_MENU_CONTINUING_LOG		= 7;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu de pausa">
	
	//#ifdef NO_SOUND
//# 		public static final byte PAUSE_MENU_CONTINUE			= 0;
//# 		public static final byte PAUSE_MENU_TOGGLE_VIBRATION	= 1;
//# 		public static final byte PAUSE_MENU_BACK_TO_MENU		= 2;	
//# 		public static final byte PAUSE_MENU_EXIT_GAME			= 3;
//# 
//# 		public static final byte PAUSE_MENU_NO_VIB_CONTINUE			= 0;
//# 		public static final byte PAUSE_MENU_NO_VIB_BACK_TO_MENU		= 1;	
//# 		public static final byte PAUSE_MENU_NO_VIB_EXIT_GAME		= 2;
	//#else
	public static final byte PAUSE_MENU_CONTINUE			= 0;
	public static final byte PAUSE_MENU_TOGGLE_SOUND		= 1;
	public static final byte PAUSE_MENU_TOGGLE_VIBRATION	= 2;
	public static final byte PAUSE_MENU_BACK_TO_MENU		= 3;	
	public static final byte PAUSE_MENU_EXIT_GAME			= 4;

	public static final byte PAUSE_MENU_NO_VIB_CONTINUE			= 0;
	public static final byte PAUSE_MENU_NO_VIB_TOGGLE_SOUND		= 1;
	public static final byte PAUSE_MENU_NO_VIB_BACK_TO_MENU		= 2;	
	public static final byte PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das op��es do menu de op��es">
	
	//#ifdef NO_SOUND
//# 		public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 0;
//# 		public static final byte OPTIONS_MENU_ERASE_RECORDS		= 1;
//# 		public static final byte OPTIONS_MENU_BACK				= 2;
//# 
//# 		public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 0;
//# 		public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;
	//#else
	public static final byte OPTIONS_MENU_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 1;
	public static final byte OPTIONS_MENU_ERASE_RECORDS		= 2;
	public static final byte OPTIONS_MENU_BACK				= 3;

	public static final byte OPTIONS_MENU_NO_VIB_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 1;
	public static final byte OPTIONS_MENU_NO_VIB_BACK				= 2;
	//#endif
	
	//</editor-fold>

	//#ifndef LOW_JAR
	// Indica se o device possui pouca mem�ria
	private boolean lowMemory;
	//#endif
	
	/** Indica se os recursos do jogo j� foram alocados */
	private boolean resourcesLoaded;
	
	private static final short LOW_FPS_FRAME_TIME = 250;

	public GameMIDlet() throws Exception
	{
		//#ifdef SmallSagemGradiente
//# 		super( VENDOR_SAGEM_GRADIENTE, LOW_FPS_FRAME_TIME );
		//#else
			super( -1, LOW_FPS_FRAME_TIME );
		//#endif

		softkeyLeft = new AnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
		softkeyRight = new AnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );

		loadTexts( TEXT_TOTAL, "/pt.dat" );
	}
	
	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException
	{
		if( playScreen != null )
		{
			// Grava a pontua��o mesmo que esteja no meio do jogo
			GameRecordsScreen.setScore( playScreen.getScore() );
			resourcesLoaded = false;
			playScreen = null;
		}
		super.destroyApp( unconditional );
	}
	
	protected final void loadResources() throws Exception
	{	
		//#ifndef LOW_JAR
		switch( getVendor() )
		{
			case VENDOR_SONYERICSSON:
			case VENDOR_NOKIA:
			case VENDOR_SIEMENS:
			break;

			default:
				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
		//#endif

		try
		{
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
		}

		// Carrega a fonte da aplica��o
		final String imgExt = ".png";
		final String path = "/fontMain";
		FONT_DEFAULT = ImageFont.createMultiSpacedFont( path + imgExt, path + ".dat" );
		FONT_DEFAULT.setCharOffset( DEFAULT_FONT_OFFSET );
		
		// Vai para a tela onde iremos carregar os outros recursos do midlet
		setScreen( SCREEN_LOAD_RESOURCES );
	}
	
	private final void loadImagesAndSounds() throws Exception
	{
		final String imgExt = ".png";
		TOP_LEFT = new DrawableImage( "/clipboardTopLeft" + imgExt );
		Thread.yield();
		
		TOP_CENTER = new Pattern( new DrawableImage( "/clipboardTopCenter" + imgExt ) );
		Thread.yield();
		
		TOP_RIGHT = new DrawableImage( "/clipboardTopRight" + imgExt );
		Thread.yield();
		
		MIDDLE_LEFT = new Pattern( new DrawableImage( "/clipboardMiddleLeft" + imgExt ) );
		Thread.yield();
		
		MIDDLE_CENTER = new Pattern( new DrawableImage( "/clipboardMiddleCenter" + imgExt ) );
		Thread.yield();
		
		MIDDLE_RIGHT = new Pattern( new DrawableImage( "/clipboardMiddleRight" + imgExt ) );
		Thread.yield();
		
		BOTTOM_LEFT = new DrawableImage( "/clipboardBottomLeft" + imgExt );
		Thread.yield();
		
		BOTTOM_CENTER = new Pattern( new DrawableImage( "/clipboardBottomCenter" + imgExt ) );
		Thread.yield();
		
		BOTTOM_RIGHT = new DrawableImage( "/clipboardBottomRight" + imgExt );
		Thread.yield();
		
		SKULL = new DrawableImage( "/skull" + imgExt );
		Thread.yield();
		
		METAL_HAND = new DrawableImage( "/metalHand" + imgExt );
		Thread.yield();
		
		CLIP = new DrawableImage( "/clipboardClip" + imgExt );
		Thread.yield();
		
		TITLE = new DrawableImage( "/SplashGame/title" + imgExt );
		Thread.yield();
	
		// Inicializa as vari�veis est�ticas da classe GameRecordsScreen
		GameRecordsScreen aux = GameRecordsScreen.createInstance( FONT_DEFAULT );
		aux = null;
		System.gc();
		
		icons[ ICON_BACK  ] = new DrawableImage( "/btBack" + imgExt );
		Thread.yield();
		
		icons[ ICON_OK    ] = new DrawableImage( "/btOk"  + imgExt );
		Thread.yield();
		
		icons[ ICON_PAUSE ] = new DrawableImage( "/btPause"  + imgExt );
		Thread.yield();

		//#ifdef NO_SOUND
//# 		MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, null );
//# 		MediaPlayer.setMute( true );
		//#else
			final String[] SOUNDS = new String[SOUND_TOTAL];

			for( byte i = 0; i < SOUND_TOTAL; ++i )
				SOUNDS[i] = "/" + i + ".mid";

			MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, SOUNDS );
		//#endif

		manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
		manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );
		manager.setBackgroundColor( -1 );
		
		resourcesLoaded = true;
	}

	public static final void setSoftKey( byte softKey, Drawable d, int visibleTime, boolean doAnimation )
	{
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );

		switch( softKey )
		{
			case ScreenManager.SOFT_KEY_LEFT:
				midlet.softkeyLeft.setNextSoftkey( d, visibleTime, true, doAnimation );
				break;

			case ScreenManager.SOFT_KEY_RIGHT:
				midlet.softkeyRight.setNextSoftkey( d, visibleTime, true, doAnimation );
				break;
		}
	}
	
	public static final void showSoftKeys()
	{
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );

		final Drawable softKeyLeftDrawable = midlet.softkeyLeft.getSoftKey();
		if( softKeyLeftDrawable != null )
			midlet.softkeyLeft.setNextSoftkey( softKeyLeftDrawable, SOFT_KEY_VISIBLE_TIME, true, false );
		
		final Drawable softKeyRightDrawable = midlet.softkeyRight.getSoftKey();
		if( softKeyRightDrawable != null )
			midlet.softkeyRight.setNextSoftkey( softKeyRightDrawable, SOFT_KEY_VISIBLE_TIME, true, false );
	}

	public synchronized final void onChoose( Menu menu, int id, int index )
	{
		switch( id )
		{
			case SCREEN_MAIN_MENU:
				if( playScreen != null )
				{
					switch( index )
					{
						case MAIN_MENU_CONTINUING_NEW_GAME:
							setScreen( SCREEN_LOADING );
							break;
							
						case MAIN_MENU_CONTINUING_CONTINUE:
							setScreen( SCREEN_UNPAUSE );
							break;

						case MAIN_MENU_CONTINUING_OPTIONS:
							setScreen( SCREEN_OPTIONS );
							break;

						case MAIN_MENU_CONTINUING_RECORDS:
							setScreen( SCREEN_RECORDS );
							break;

						case MAIN_MENU_CONTINUING_HELP:
							setScreen( SCREEN_HELP );
							break;

						case MAIN_MENU_CONTINUING_CREDITS:
							setScreen( SCREEN_CREDITS );
							break;

						//#if LOG == "true"
	//#					case MAIN_MENU_CONTINUING_LOG:
	//#						setScreen( SCREEN_LOG );
	//#						break;
						//#endif

						case MAIN_MENU_CONTINUING_EXIT:
							exit();
							break;
					}
				}
				else
				{
					switch( index )
					{
						case MAIN_MENU_NEW_GAME:
							setScreen( SCREEN_LOADING );
							break;

						case MAIN_MENU_OPTIONS:
							setScreen( SCREEN_OPTIONS );
							break;

						case MAIN_MENU_RECORDS:
							setScreen( SCREEN_RECORDS );
							break;

						case MAIN_MENU_HELP:
							setScreen( SCREEN_HELP );
							break;

						case MAIN_MENU_CREDITS:
							setScreen( SCREEN_CREDITS );
							break;

						//#if LOG == "true"
	//#						case MAIN_MENU_LOG:
	//#							setScreen( SCREEN_LOG );
	//#							break;
						//#endif

						case MAIN_MENU_EXIT:
							exit();
							break;
					}
				}
				break; // fim case SCREEN_MAIN_MENU


			//#if LOG == "true"
//#				case SCREEN_LOG:
//#					setScreen( SCREEN_MAIN_MENU );
//#					break;
			//#endif

			case SCREEN_SPLASH_BRAND:
				setScreen( SCREEN_SPLASH_GAME );
				break;

			case SCREEN_CREDITS:
				setScreen( SCREEN_MAIN_MENU );
				break;

			case SCREEN_HELP:
				setScreen( SCREEN_MAIN_MENU );
				break;

			case SCREEN_PAUSE:
				if( MediaPlayer.isVibrationSupported() )
				{
					switch( index )
					{
						case PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_UNPAUSE );
							break;

						//#ifndef NO_SOUND
						case PAUSE_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
						break;
						//#endif

						case PAUSE_MENU_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
							break;

						case PAUSE_MENU_BACK_TO_MENU:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_BACK_TO_MENU );
							break;

						case PAUSE_MENU_EXIT_GAME:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_EXIT_GAME );
							break;
					}
				}
				else
				{
					switch( index )
					{
						case PAUSE_MENU_NO_VIB_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_UNPAUSE );
							break;

						//#ifndef NO_SOUND
					case PAUSE_MENU_NO_VIB_TOGGLE_SOUND:
						MediaPlayer.setMute( !MediaPlayer.isMuted() );
						break;
						//#endif

						case PAUSE_MENU_NO_VIB_BACK_TO_MENU:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_BACK_TO_MENU );
							break;

						case PAUSE_MENU_NO_VIB_EXIT_GAME:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_EXIT_GAME );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE												 

			case SCREEN_OPTIONS:
				if( MediaPlayer.isVibrationSupported() )
				{
					switch( index )
					{
						//#ifndef NO_SOUND
						case OPTIONS_MENU_TOGGLE_SOUND:
							break;
						//#endif
							
						case OPTIONS_MENU_TOGGLE_VIBRATION:
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
							break;
							
						case OPTIONS_MENU_ERASE_RECORDS:
							setScreen( SCREEN_YN_ERASE_RECORDS );
							break;
							
						case OPTIONS_MENU_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				else
				{
					switch( index )
					{
						//#ifndef NO_SOUND
						case OPTIONS_MENU_NO_VIB_TOGGLE_SOUND:
							break;
						//#endif
							
						case OPTIONS_MENU_NO_VIB_ERASE_RECORDS:
							setScreen( SCREEN_YN_ERASE_RECORDS );
							break;
							
						case OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS


 			case SCREEN_YN_ERASE_RECORDS:
 				switch( index )
				{
 					case GameYesNoScreen.INDEX_YES:
 						GameRecordsScreen.eraseRecords();
 						
 					case GameYesNoScreen.INDEX_NO:
 						setScreen( SCREEN_OPTIONS );
						break;
 				}
				break;
				
			case SCREEN_YN_BACK_TO_MENU:
				switch( index )
				{
					case GameYesNoScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						
						// Queremos garantir que a m�sica vai voltar a tocar do in�cio
						MediaPlayer.free();

						//#ifndef SIEMENS
							//#ifndef NO_SOUND
								//#if SAMSUNG_BAD_SOUND == "true"
		//#							MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
								//#else
									MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
								//#endif
							//#endif
						//#endif

 						setScreen( SCREEN_MAIN_MENU );
						break;
 						
 					case GameYesNoScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_YN_EXIT_GAME:
				switch( index )
				{
					case GameYesNoScreen.INDEX_YES:
 						GameMIDlet.exit();
						break;
 						
 					case GameYesNoScreen.INDEX_NO:
 						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_ENABLE_SOUNDS:
				MediaPlayer.setMute( index == GameYesNoScreen.INDEX_NO );
				setScreen( SCREEN_SPLASH_NANO );
				break;

		} // fim switch ( id )
	}

	public final void onItemChanged( Menu menu, int id, int index )
	{
	}

	public static final void gameOver( int score )
	{
		final GameMIDlet midlet = ( GameMIDlet )instance;
		midlet.playScreen = null;
		System.gc();
		
		// Queremos garantir que a m�sica vai voltar a tocar do in�cio
		MediaPlayer.free();

		//#ifndef SIEMENS
			//#ifndef NO_SOUND
				//#if SAMSUNG_BAD_SOUND == "true"
//#					MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
				//#else
					MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
				//#endif
			//#endif
		//#endif
						
		if( GameRecordsScreen.setScore( score ) )
			setScreen( SCREEN_RECORDS );
		else
			setScreen( SCREEN_MAIN_MENU );
	}

	public static final void setSoftKeysInTransition( boolean inTransition )
	{
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		midlet.softkeyLeft.setInTransition( inTransition );
		midlet.softkeyRight.setInTransition( inTransition );
	}

	public static final boolean isLowMemory()
	{
		//#ifdef LOW_JAR
//# 		return true;
		//#else
			return (( GameMIDlet )getInstance() ).lowMemory;
		//#endif
	}
	
	public static String formatNumStr( int value, int nChars, char filler )
	{
		final String valueStr = String.valueOf( value );
		String s = "";

		int nFillers = nChars - valueStr.length();
		while( nFillers-- > 0 )
			s += filler;
		
		return s += valueStr;
	}

	private static final class LoadScreen extends Label implements Updatable
	{
		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;
		private long lastUpdateTime;
		private static final byte MAX_DOTS = 4;
		private byte dots;
		private final GameMIDlet midlet;
		private Thread loadThread;
		private final int nextScreen;

		private LoadScreen( GameMIDlet midlet, int nextScreen ) throws Exception
		{
			super( midlet.FONT_DEFAULT, GameMIDlet.getText( TEXT_LOADING ) );
			this.midlet = midlet;
			this.nextScreen = nextScreen;
			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}

		protected void paint( Graphics g )
		{
			// Coloca uma cor de fundo
			pushClip( g );
			g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			g.setColor( 255, 188, 0 );
			g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			popClip( g );
		
			// Desenha o label
			super.paint( g );
		}

		public final void update( int delta )
		{
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if( interval >= CHANGE_TEXT_INTERVAL )
			{
				// as imagens do jogo s�o carregadas aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if( loadThread == null )
				{
					// s� inicia a thread quando a tela atual for a ativa (ou seja, a transi��o da TV estiver encerrada)
					if( ScreenManager.getInstance().getCurrentScreen() == this )
					{
						loadThread = new Thread()
						{
							public final void run()
							{
								try
								{
									MediaPlayer.free();
									GameMIDlet.setScreen( nextScreen );
								}
								catch( Exception e )
								{
									// Sai do jogo
									exit();
								}
							}
						};
						loadThread.start();
					}
				}
				else
				{
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) % MAX_DOTS );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for( byte i = 0; i < dots; ++i )
					{
						temp += '.';
					}

					setText( temp );

					try
					{
						// Permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					}
					catch( Exception e )
					{
						//#if DEBUG == "true"
//# 						e.printStackTrace();
						//#endif
					}
				}
			}
		}
	}

	private String getAppVersion()
	{
		String version = getAppProperty( "MIDlet-Version" );
		if ( version == null )
			version = "0.0.9";
		return "<ALN_H>VERS�O " + version;
	}
	
	protected int changeScreen( int screen )
	{
		//#if DEBUG == "true"
//# 			System.out.print( "setScreen( " + screen + " )" );
		//#endif

		final GameMIDlet midlet = ( GameMIDlet ) instance;

		if( screen != currentScreen )
		{
			Drawable nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;
			
			int softKeyVisibleTime = 0;
			boolean doAnimation  = false;

			try
			{
				switch( screen )
				{
					case SCREEN_LOAD_RESOURCES:
						//#ifdef NO_SOUND
//#							nextScreen = new LoadScreen( midlet, SCREEN_SPLASH_NANO );
						//#else
							nextScreen = new LoadScreen( midlet, SCREEN_ENABLE_SOUNDS );
						//#endif
						break;

					case SCREEN_ENABLE_SOUNDS:
						if( !resourcesLoaded )
							loadImagesAndSounds();

						nextScreen = new GameYesNoScreen( midlet, screen, FONT_DEFAULT, TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, true );
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						break;

					case SCREEN_SPLASH_NANO:
						if( !resourcesLoaded )
							loadImagesAndSounds();

						nextScreen = new SplashNano( PATH_SPLASH, getText( TEXT_SPLASH_NANO ), SCREEN_SPLASH_BRAND );
						
						// Queremos garantir que a m�sica vai voltar a tocar do in�cio
						MediaPlayer.free();

						//#ifndef NO_SOUND
							//#if SAMSUNG_BAD_SOUND == "true"
	//#							MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
							//#else
								MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
							//#endif
						//#endif
						break;

					case SCREEN_SPLASH_BRAND:
						nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, "/SplashMTV/mtv.png", TEXT_SPLASH_MTV );
						break;

					case SCREEN_SPLASH_GAME:
						nextScreen = new SplashGame( FONT_DEFAULT );
						break;

					case SCREEN_MAIN_MENU:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;

						if( playScreen != null )
						{
							nextScreen = new GameMenu( midlet, screen, FONT_DEFAULT, new int[]{
														  TEXT_NEW_GAME,
														  TEXT_CONTINUE,
														  TEXT_OPTIONS,
														  TEXT_RECORDS,
														  TEXT_HELP,
														  TEXT_CREDITS,
														  TEXT_EXIT,
														  //#if LOG == "true"
	//#													  TEXT_LOG,
														  //#endif							
													  }, MAIN_MENU_CONTINUING_EXIT, ( byte )0 );
						}
						else
						{
							nextScreen = new GameMenu( midlet, screen, FONT_DEFAULT, new int[]{
														  TEXT_NEW_GAME,
														  TEXT_OPTIONS,
														  TEXT_RECORDS,
														  TEXT_HELP,
														  TEXT_CREDITS,
														  TEXT_EXIT,
														  //#if LOG == "true"
	//#													  TEXT_LOG,
														  //#endif							
													  }, MAIN_MENU_EXIT, ( byte )0 );
						}
						break;

					case SCREEN_LOADING:
						nextScreen = new LoadScreen( midlet, SCREEN_GAME );
						break;

					case SCREEN_OPTIONS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						if( MediaPlayer.isVibrationSupported() )
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_TURN_VIBRATION_OFF,
//# 																 TEXT_ERASE_RECORDS,
//# 																 TEXT_BACK,
//# 															 }, OPTIONS_MENU_BACK, OPTIONS_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
							//#else
						nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
															 TEXT_TURN_SOUND_OFF,
															 TEXT_TURN_VIBRATION_OFF,
															 TEXT_ERASE_RECORDS,
															 TEXT_BACK,
														 }, OPTIONS_MENU_BACK, OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, OPTIONS_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON, true );
							//#endif
						}
						else
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_ERASE_RECORDS,
//# 																 TEXT_BACK,
//# 															 }, OPTIONS_MENU_NO_VIB_BACK, -1, -1 );
							//#else
						nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
															 TEXT_TURN_SOUND_OFF,
															 TEXT_ERASE_RECORDS,
															 TEXT_BACK,
														 }, OPTIONS_MENU_NO_VIB_BACK, OPTIONS_MENU_NO_VIB_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1, true );
							//#endif
						}
						break;

					case SCREEN_CREDITS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						//#if ( SCREEN_SIZE == "SMALL" )
//# 							nextScreen = new GameTextScreen( midlet, screen, FONT_DEFAULT, getText( TEXT_CREDITS_TEXT ) + "\n\n\n", null );
						//#elif ( SCREEN_SIZE == "MEDIUM" )
							nextScreen = new GameTextScreen( midlet, screen, FONT_DEFAULT, getText( TEXT_CREDITS_TEXT ) + "\n\n", null );
						//#else
//# 							nextScreen = new GameTextScreen( midlet, screen, FONT_DEFAULT, getText( TEXT_CREDITS_TEXT ), null );
						//#endif
						break;

					case SCREEN_HELP:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new GameTextScreen( midlet, screen, FONT_DEFAULT, getText( TEXT_HELP_TEXT ) + "\n\n" + getAppVersion() + "\n", null );
						break;

					case SCREEN_PAUSE:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						if( MediaPlayer.isVibrationSupported() )
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_CONTINUE,
//# 																 TEXT_TURN_VIBRATION_OFF,
//# 																 TEXT_BACK_MENU,
//# 																 TEXT_EXIT_GAME,
//# 															 }, PAUSE_MENU_CONTINUE, PAUSE_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
							//#else
						nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
															 TEXT_CONTINUE,
															 TEXT_TURN_SOUND_OFF,
															 TEXT_TURN_VIBRATION_OFF,
															 TEXT_BACK_MENU,
															 TEXT_EXIT_GAME,
														 }, PAUSE_MENU_CONTINUE, PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, PAUSE_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON, false );
							//#endif
						}
						else
						{
							//#ifdef NO_SOUND
//# 							nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
//# 																 TEXT_CONTINUE,
//# 																 TEXT_BACK_MENU,
//# 																 TEXT_EXIT_GAME,
//# 															 }, PAUSE_MENU_NO_VIB_CONTINUE, -1, -1 );
							//#else
						nextScreen = new GameOptionsScreen( midlet, screen, FONT_DEFAULT, new int[]{
															 TEXT_CONTINUE,
															 TEXT_TURN_SOUND_OFF,
															 TEXT_BACK_MENU,
															 TEXT_EXIT_GAME,
														 }, PAUSE_MENU_NO_VIB_CONTINUE, PAUSE_MENU_NO_VIB_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1, false );
							//#endif
						}

						break;

					case SCREEN_YN_BACK_TO_MENU:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						//#if ( SCREEN_SIZE == "SMALL" ) && ( WIDESCREEN == "false" )
//# 						nextScreen = new GameYesNoScreen( midlet, screen, FONT_DEFAULT, TEXT_BACK_MENU_2, TEXT_YES, TEXT_NO, false );
						//#else
							nextScreen = new GameYesNoScreen( midlet, screen, FONT_DEFAULT, TEXT_BACK_MENU, TEXT_YES, TEXT_NO, false );
						//#endif
						break;
						
					case SCREEN_YN_EXIT_GAME:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new GameYesNoScreen( midlet, screen, FONT_DEFAULT, TEXT_EXIT_GAME, TEXT_YES, TEXT_NO, false );
						break;
						
					case SCREEN_YN_ERASE_RECORDS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = new GameYesNoScreen( midlet, screen, FONT_DEFAULT, TEXT_ERASE_RECORDS, TEXT_YES, TEXT_NO, false );
						break;
 
 					case SCREEN_RECORDS:
						indexSoftRight = ICON_BACK;
						indexSoftLeft = ICON_OK;
						
						nextScreen = GameRecordsScreen.createInstance( FONT_DEFAULT );
						break;

					//#if LOG == "true"
//#						case SCREEN_LOG:
//#							nextScreen = new BasicTextScreen( midlet, screen, FONT_DEFAULT, TEXT_ERROR_LOG, false );
//#							indexSoftRight = TEXT_BACK;
//#							break;
					//#endif

					case SCREEN_GAME:
						playScreen = new PlayScreen();
						System.gc();
						
					case SCREEN_UNPAUSE:
						MediaPlayer.free();
						
						nextScreen = playScreen;
						(( PlayScreen )nextScreen ).unpause();
						
						indexSoftRight = ICON_PAUSE;
						doAnimation = false;
						//#if SCREEN_SIZE != "BIG"
						if( !ScreenManager.getInstance().hasPointerEvents() )
							softKeyVisibleTime = SOFT_KEY_VISIBLE_TIME;
						//#endif
						break;

				} // fim switch ( screen )

				if( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
					setSoftKey( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft == SOFT_KEY_REMOVE ? null : icons[ indexSoftLeft ], softKeyVisibleTime, doAnimation );

				if( indexSoftRight != SOFT_KEY_DONT_CHANGE )
					setSoftKey( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight == SOFT_KEY_REMOVE ? null : icons[ indexSoftRight ], softKeyVisibleTime, doAnimation );
			}
			catch( Exception e )
			{
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, ent�o sai do jogo
				exit();
			}

			if( nextScreen != null )
			{
				midlet.manager.setCurrentScreen( nextScreen );
				return screen;
			}
			else
			{
				return -1;
			}
		} // fim if ( screen != currentScreen )
		return screen;
	}
}
