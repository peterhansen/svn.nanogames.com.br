/**
 * SplashGame.java
 * �2007 Nano Games
 */

package screens;

//#ifndef LOW_JAR
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class SplashGame extends UpdatableGroup implements Constants, KeyListener
//#ifndef LOW_JAR
// N�o h� aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do c�digo)
, PointerListener
//#endif
{
	/** Tempo de dura��o do splash */
	private short timeCounter = SPLASH_GAME_DUR;
	
	/** Quanto da imagem das folhas de louro deve entrar na imagem dos an�is ol�mpicos em X */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte LEAFS_RINGS_OFFSET_X = 2;
	//#else
	private final byte LEAFS_RINGS_OFFSET_X = 10;
	//#endif
	
	/** Quanto da imagem das folhas de louro deve entrar na imagem dos an�is ol�mpicos em Y */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte LEAFS_RINGS_OFFSET_Y = 12;
	//#else
	private final byte LEAFS_RINGS_OFFSET_Y = 15;
	//#endif
	
	/** Quantos tri�ngulos teremos rodando no background. OBS: 360 % N_TRIANGLES DEVE SER IGUAL A ZERO */
	private final byte N_TRIANGLES = 30;
	
	/** Ajudam a calcular os vetores na anima��o de girar o background */
	private final Point vecCalculator1 = new Point(), vecCalculator2 = new Point();
	
	/** M�dulo utilizado por <code>vecCalculator1</code> e <code>vecCalculator2</code> no c�lculo dos vetores */
	private final int vecModule;
	
	/** �ngulo a partir do qual come�amos a calcular os tri�ngulos */
	private short startAngle;
	
	/** Em quantos graus incrementamos <code>startAngle</code> a cada etapa da anima��o */
	private final byte START_ANGLE_INC = 5;
	
	/** Velocidade de anima��o do background */
	private final short BKG_ANIM_TIME = 100;
	
	/** Controla o incremento de �ngulos da anima��o de girar o background ao longo do tempo */
	private final MUV animControl = new MUV();
	
	/** �ndice do logo Hermes & Renato no grupo */
	private final byte LOGO_INDEX = 0;
	
	/** �ndice do label com a mensagem de "Pressione Qualquer Tecla" */
	private final byte PRESS_ANY_KEY_LABEL_INDEX = 6;
	
	/** Controla a anima��o de piscar o label de "Pressione Qualquer Tecla" */
	private short blinkCounter;
	
	/** Dura��o da anima��o do label de "Pressione Qualquer Tecla" */
	private final short PRESS_ANY_KEY_BLINK_ANIM_TIME = 250;

	/** Melhora o posicionamento do logo */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte LOGO_OFFSET_X = 2;
	//#else
	private final byte LOGO_OFFSET_X = 0;
	//#endif
	
	/** Construtor */
	public SplashGame( ImageFont font ) throws Exception
	{
		// Configura o grupo
		super( 7 );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// Carrega o logo
		final String imgExt = ".png";
		final String dir = "/SplashGame/";
		final DrawableImage logo = new DrawableImage( dir + "logo" + imgExt );
		insertDrawable( logo );
		
		// Carrega o t�tulo
		final DrawableImage title = new DrawableImage((( GameMIDlet )GameMIDlet.getInstance()).TITLE );
		insertDrawable( title );
		
		// Carrega  os arcos ol�mpicos
		final DrawableImage ringsLeft = new DrawableImage( dir + "rings" + imgExt );
		insertDrawable( ringsLeft );
		
		final DrawableImage ringsRight = new DrawableImage( ringsLeft );
		insertDrawable( ringsRight );
		ringsRight.mirror( TRANS_MIRROR_H );
		ringsRight.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
		
		// Carrega as folhas de louro
		final DrawableImage leafsLeft = new DrawableImage( dir + "leafs" + imgExt );
		insertDrawable( leafsLeft );
		leafsLeft.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_RIGHT );
		
		final DrawableImage leafsRight = new DrawableImage( leafsLeft );
		insertDrawable( leafsRight );
		leafsRight.mirror( TRANS_MIRROR_H );
		leafsRight.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		
		// Cria o label de "Pressione Qualquer Tecla"
		final Label pressAnyKeyLabel = new Label( font, AppMIDlet.getText( TEXT_PRESS_ANY_KEY ) );
		insertDrawable( pressAnyKeyLabel );
		pressAnyKeyLabel.setPosition( ( ScreenManager.SCREEN_WIDTH - pressAnyKeyLabel.getWidth() ) >> 1, ScreenManager.SCREEN_HEIGHT - pressAnyKeyLabel.getHeight() );
		pressAnyKeyLabel.setVisible( false );
		
		// Posiciona os elementos
		ringsLeft.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ringsLeft.getWidth(), ScreenManager.SCREEN_HALF_HEIGHT );
		ringsRight.setPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		
		final int ringsEnd = ScreenManager.SCREEN_HALF_HEIGHT + ringsLeft.getHeight();
		final int height = ScreenManager.SCREEN_HEIGHT - ringsEnd - pressAnyKeyLabel.getHeight();
		int titleY = ringsEnd + ( ( height - title.getHeight() ) >> 1 );
		//#if SCREEN_SIZE == "SMALL"
//# 			if( titleY <= ringsEnd )
//# 				titleY = ringsEnd + 1;
		//#endif
		title.setPosition( ( ScreenManager.SCREEN_WIDTH - title.getWidth() ) >> 1, titleY );
		
		leafsLeft.setRefPixelPosition( ringsLeft.getPosX() + LEAFS_RINGS_OFFSET_X, ringsLeft.getPosY() + LEAFS_RINGS_OFFSET_Y );
		leafsRight.setRefPixelPosition( ringsRight.getRefPixelX() - LEAFS_RINGS_OFFSET_X, ringsRight.getRefPixelY() + LEAFS_RINGS_OFFSET_Y );
		
		logo.setPosition( (( ScreenManager.SCREEN_WIDTH - logo.getWidth() ) >> 1) + LOGO_OFFSET_X, ScreenManager.SCREEN_HALF_HEIGHT - logo.getHeight() );
		
		// Calcula a maior dimens�o da tela
		vecModule = ( ScreenManager.SCREEN_WIDTH > ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH : ScreenManager.SCREEN_HEIGHT ) << 1;
		
		// Calcula a velocidade da anima��o
		animControl.setSpeed( ( 1000 / BKG_ANIM_TIME ) * START_ANGLE_INC );
	}

	public final void update( int delta )
	{
		// Controla a anima��o de girar o background
		startAngle = ( short )( ( startAngle + animControl.updateInt( delta ) ) % 360 );
		
		if( timeCounter < 0 )
		{
			blinkCounter += delta;
			
			if( blinkCounter >= PRESS_ANY_KEY_BLINK_ANIM_TIME )
			{
				blinkCounter = ( short )( blinkCounter % PRESS_ANY_KEY_BLINK_ANIM_TIME );
				final Label pressAnyKeyLabel = ( Label )getDrawable( PRESS_ANY_KEY_LABEL_INDEX );
				pressAnyKeyLabel.setVisible( !pressAnyKeyLabel.isVisible() );
			}
		}
		else
		{
			timeCounter -=delta;
		}
	}

	protected void paint( Graphics g )
	{
		// Desenha o background. Ao inv�s de desenharmos tri�ngulos alternando cores, pintamos toda a tela de
		// uma cor e depois os tri�ngulos na outra cor. Assim otimizamos o m�todo, j� que o n�mero de chamadas
		// a Graphics.fillTriangle cair� pela metade
		g.setColor( 255, 228, 0 );
		g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		g.setColor( 255, 188, 0 );
		
		final int INC = 360 / N_TRIANGLES;
		final int DOUBLE_INC = INC << 1;
		final short max = ( short )( 360 + startAngle );
		for( short i = startAngle ; i < max ; i += DOUBLE_INC )
		{
			vecCalculator1.setVector( i, vecModule );
			vecCalculator1.addEquals( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
			
			vecCalculator2.setVector( i + INC, vecModule );
			vecCalculator2.addEquals( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
			
			g.fillTriangle( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT - ( getDrawable( LOGO_INDEX ).getHeight() >> 1 ), vecCalculator1.x, vecCalculator1.y, vecCalculator2.x, vecCalculator2.y );
		}
		
		// Desenha as imagens
		super.paint( g );
	}

	public void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.exit();
				break;

			default:
			//#if DEBUG == "true"
				// Os splashs n�o podem ser acelerados pelo usu�rio
			callNextScreen();
			//#else
//# 				if( timeCounter < 0 )
//# 					callNextScreen();
			//#endif
		}
	}

	public void keyReleased( int key )
	{
	}

	private final void callNextScreen()
	{
		AppMIDlet.setScreen( GameMIDlet.SCREEN_MAIN_MENU );
	}

	//#ifndef LOW_JAR
	public void onPointerDragged( int x, int y )
	{
	}

	public void onPointerPressed( int x, int y )
	{
		if( timeCounter < 0 )
			callNextScreen();
	}

	public void onPointerReleased( int x, int y )
	{
	}
	//#endif
}
