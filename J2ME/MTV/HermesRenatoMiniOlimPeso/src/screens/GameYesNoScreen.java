/**
 * GameYesNoScreen.java
 * �2008 Nano Games.
 *
 * Created on 28/04/2008 15:41:29.
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 * @author Daniel L. Alves
 */

public final class GameYesNoScreen extends GameMenu
{
	/** �ndice da op��o "sim" da tela de confirma��o. */
	public static final byte INDEX_YES = 0;

	/** �ndice da op��o "n�o" da tela de confirma��o. */
	public static final byte INDEX_NO = 1;

	/** Pergunta */
	private static final byte NON_SELECTABLE = 1;
	
	/** Indica se a aplica��o deve terminar ao receber um evento de "cancelar" nesta tela */
	final boolean exitOnBack;
	
	/**
	 * Cria uma nova tela de confirma��o.
	 * 
	 * @param listener
	 * @param id
	 * @param font
	 * @param cursor 
	 * @param idTitle
	 * @param idYes 
	 * @param idNo 
	 * @throws java.lang.Exception
	 */
	public GameYesNoScreen( MenuListener listener, int id, ImageFont font, int idTitle, int idYes, int idNo, boolean exitOnBack ) throws Exception
	{
		super( listener, id, ( byte )3, NON_SELECTABLE );
		this.exitOnBack = exitOnBack;
		
		final String[] aux = new String[]{ AppMIDlet.getText( idTitle ), AppMIDlet.getText( idYes ), AppMIDlet.getText( idNo ) };
		
		if( aux[0].charAt( aux[0].length() - 1 ) != '?' )
			aux[0] += "?";
		
		if( aux[0].charAt( 0 ) == '�' )
			aux[0] = aux[0].substring( 2 );

		super.construct( listener, font, aux, INDEX_NO );
		
		positionItems( true );
		setCurrentIndex( INDEX_NO );
	}

	public void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if( exitOnBack )
					AppMIDlet.exit();
				else
					super.keyPressed( key );
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				super.keyPressed( ScreenManager.KEY_NUM8 );
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				super.keyPressed( ScreenManager.KEY_NUM2 );
				break;

			default:
				super.keyPressed( key );
		}
	}

	public void update( int delta )
	{
		positionItems( false );
		super.update( delta );
	}

	protected void positionItems( boolean force )
	{
		if( force || ( ( size.x != ScreenManager.SCREEN_WIDTH ) || ( size.y != ScreenManager.SCREEN_HEIGHT ) ) )
		{
			positionItems();

			final int redMargin = (( DrawableGroup )drawables[ GAME_MENU_BKG_INDEX ] ).getDrawable( GameMenuBkg.BKG_CLIPBOARD_TOP_LEFT ).getWidth();
			final Label title = ( Label )getDrawable( FIRST_LABEL_INDEX );
			title.setPosition( redMargin + ( ( ScreenManager.SCREEN_WIDTH - redMargin - title.getWidth() ) >> 1 ), title.getPosY() );
			
			updateCursorPosition();
		}
	}
}
