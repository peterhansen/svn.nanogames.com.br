/**
 * GameTextScreen.java
 * �2008 Nano Games.
 *
 * Created on 24/04/2008 17:24:22.
 */

package screens;

//#ifndef LOW_JAR
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import core.Constants;

/**
 * @author Daniel L. Alves
 */

public class GameTextScreen extends UpdatableGroup implements Constants, KeyListener
//#ifndef LOW_JAR
, PointerListener
//#endif
{
	/** Velocidade m�xima de movimenta��o do texto. No caso de movimenta��o autom�tica ou atrav�s de teclas, esse valor
	 * � sempre utilizado. No caso de movimenta��o atrav�s de ponteiro, a velocidade � gradual.
	 */
	private final short TEXT_SPEED;

	/** Limite superior do texto. */
	private int textLimitTop;

	/** Limite inferior do texto. */
	private int textLimitBottom;

	/** Velocidade de movimenta��o do texto. */
	private final MUV textSpeed = new MUV();

	/** �ltima posi��o do ponteiro (valor utilizado para fazer scroll). */
	private int lastPointerY;

	/** Listener que receber� o evento de menu quando o usu�rio confirmar a sa�da da tela. */
	private final MenuListener listener;

	/** Valor passado ao listener para identifica��o da tela que enviou o evento de menu. */
	private final int id;

	/** Label que cont�m o texto */
	private final RichLabel textLabel;
	
	/** Background da tela de texto */
	private final GameMenuBkg bkg;
	
	/** Dist�ncia do texto para as bordas laterais da folha de papel */
	private final byte TEXT_DIST_FROM_BORDER = 4;
	
	/** Quantas linhas depois do t�tulo queremos que o texto comece */
	private final byte N_LINES_AFTER_TITLE = 2;
	
	/** C�digo da �ltima tecla pressionada */
	private int lastKeyPressed;
	
	/**
	 * Cria uma nova tela b�sica de texto.
	 * 
	 * @param listener listener que receber� o evento indicando que o usu�rio saiu da tela.
	 * @param id identifica��o da tela, passada ao listener ao receber eventos.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param textIndex �ndice do texto no arquivo de resource.
	 * @param specialChars caracteres especiais utilizados no texto.
	 * @throws java.lang.Exception
	 */
	public GameTextScreen( MenuListener listener, int id, ImageFont font, String text, Drawable[] specialChars ) throws Exception
	{
		super( 2 );

		this.listener = listener;
		this.id = id;

		final int redMargin = GameMenuBkg.getWidthBeforeRedMargin();
		textLabel = new RichLabel( font, text, ScreenManager.SCREEN_WIDTH - GameMenuBkg.CLIPBOARD_NO_PAPER_WIDTH - redMargin - ( TEXT_DIST_FROM_BORDER << 1 ), specialChars );
		
		final short[][] linesInfo = textLabel.getLinesInfo();
		for( int i = 0; i < linesInfo.length; ++i )
			linesInfo[ i ][ RichLabel.INFO_LINE_HEIGHT ] = GameMenuBkg.LINE_HEIGHT;

		bkg = new GameMenuBkg();
		insertDrawable( bkg );
		insertDrawable( textLabel );
		
		bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_SKULL ).setVisible( false );
		bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_METAL_HAND ).setVisible( false );

		final int y = bkg.getNLineAfterTitleY( N_LINES_AFTER_TITLE ) + 2;
		bkg.enlarge( textLabel.getTextTotalHeight(), y );

		TEXT_SPEED = ( short )( ( font.getImage().getHeight() * 13 ) >> 1 );
		textLimitBottom = 0;
		setTextSpeed( 0 );

		textLabel.setPosition( redMargin + (( ScreenManager.SCREEN_WIDTH - redMargin - textLabel.getWidth() - GameMenuBkg.CLIPBOARD_NO_PAPER_WIDTH ) >> 1 ), y );
		
		// Determina o tamanho do grupo
		textLabel.setSize( textLabel.getWidth(), textLabel.getTextTotalHeight() );
		setSize( bkg.getSize() );
	}

	public void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		lastKeyPressed = key;

		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				setTextSpeed( 0 );
				// N�o deveria passar null aqui... mas o tempo urge
				listener.onChoose( null, id, 0 );
				return;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setTextSpeed( Math.abs( TEXT_SPEED ) );
				break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setTextSpeed( -Math.abs( TEXT_SPEED ) );
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				setOffset( ScreenManager.SCREEN_HEIGHT );
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				setOffset( -ScreenManager.SCREEN_HEIGHT );
				break;

			default:
				setTextSpeed( 0 );
		}
	}

	public void keyReleased( int key )
	{
		if( key == lastKeyPressed )
			setTextSpeed( 0 );
	}

	public void update( int delta )
	{
		super.update( delta );

		if( textSpeed.getSpeed() != 0 )
		{
			final int dy = textSpeed.updateInt( delta );
			if( dy != 0 )
				setOffset( dy );
		}
	}

	private void setTextSpeed( int speed )
	{
		textSpeed.setSpeed( speed );
	}

	public void setOffset( int dy )
	{
		final int bkgPrevPosY = bkg.getPosY();
		int bkgNextPosY = bkgPrevPosY + dy;
			
		if( bkgNextPosY >= textLimitBottom )
		{
			bkgNextPosY = textLimitBottom;
			setTextSpeed( 0 );
		}
		else
		{
			if( bkgNextPosY <= textLimitTop )
			{
				bkgNextPosY = textLimitTop;
				setTextSpeed( 0 );
			}
		}
		
		bkg.setPosition( 0, bkgNextPosY );
		textLabel.move( 0, bkgNextPosY - bkgPrevPosY );

		final int labelY = textLabel.getPosY();
		if( labelY < 0 )
			textLabel.setInitialLine( -( labelY / GameMenuBkg.LINE_HEIGHT ) );
		else
			textLabel.setInitialLine( 0 );
	}

	public void setSize( int width, int height )
	{
		super.setSize( width, height );
		
		// Calcula quanto do grupo est� para fora da tela
		final int diff = ScreenManager.SCREEN_HEIGHT - height;
		textLimitTop = diff >= 0 ? 0 : diff;
	}
	
	//#ifndef LOW_JAR
	
	public void onPointerDragged( int x, int y )
	{
		setOffset( y - lastPointerY );
		lastPointerY = y;
	}

	public void onPointerPressed( int x, int y )
	{
		lastPointerY = y;
	}

	public void onPointerReleased( int x, int y )
	{
	}
	
	//#endif
}
