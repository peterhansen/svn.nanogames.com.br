/**
 * GameRecordsScreen.java
 * �2008 Nano Games.
 *
 * Created on 28/04/2008 12:34:46.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * @author Daniel L. Alves
 */

public final class GameRecordsScreen extends GameMenu implements Serializable, Constants
{
	private static final byte TOTAL_SCORES = 5;
	
	private static final int[] scores = new int[TOTAL_SCORES];
	
	private final short BLINK_RATE = 388;
	
	private byte lastHighScoreIndex = -1;
	
	/** Pontua��o base para os recordes dummy */
	private static final short DUMMY_BASE = 0;
	
	private static GameRecordsScreen instance;
	
	private int accTime;
	
	/** Construtor */
	private GameRecordsScreen( ImageFont font ) throws Exception
	{
		super( null, 0, TOTAL_SCORES, ( byte )0 );
	}

	/**
	 * Cria uma nova inst�ncia da tela de recordes, ou apenas uma refer�ncia para ela, caso j� tenha sido criada anteriormente.
	 *
	 * @param font fonte utilizada para criar os labels.
	 * @throws java.lang.Exception caso haja problemas ao alocar recursos.
	 * @return uma refer�ncia para a inst�ncia da tela de recordes.
	 */
	public static final GameRecordsScreen createInstance( ImageFont font ) throws Exception
	{
		if( instance == null )
		{
			instance = new GameRecordsScreen( font );
			loadRecords();
			
			final String value = "0. " + String.valueOf( MAX_SCORE );
			final String[] aux = new String[ scores.length ];
			for( int i = 0 ; i < scores.length ; ++i )
				aux[i] = value;
			
			instance.construct( null, font, aux, -1 );
		}

		instance.updateLabels();

		return instance;
	}
	
	/** JAVA nojento que n�o deixa eu executar nada dentro de um construtor antes de uma chamada para super()
	 * ou this() !!!!!!!!!!! */
	protected void construct( MenuListener listener, ImageFont font, String[] entries, int backIndex ) throws Exception
	{
		super.construct( listener, font, entries, backIndex );
		cursor.setVisible( false );
	}

	public static final boolean setScore( int score )
	{
		for( int i = 0; i < scores.length; ++i )
		{
			if( score > scores[i] )
			{
				// �ltima pontua��o � maior que uma pontua��o anteriormente gravada
				for( int j = scores.length - 1; j > i; --j )
					scores[j] = scores[j - 1];

				instance.lastHighScoreIndex = ( byte ) i;
				instance.accTime = 0;

				scores[i] = score;
				try
				{
					GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_SCORES, instance );
				}
				catch( Exception e )
				{
					//#if DEBUG == "true"
//# 						e.printStackTrace();
					//#endif
				}
				return true;
			}
		}
		return false;
	}

	public static final int isHighScore( int score )
	{
		for( int i = 0; i < scores.length; ++i )
		{
			if( score > scores[i] )
				return i;
		}
		return -1;
	}

	public final void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				stopBlink();
				GameMIDlet.setScreen( GameMIDlet.SCREEN_MAIN_MENU );
				break;
		}
	}

	public final void write( DataOutputStream output ) throws Exception
	{
		for( int i = 0; i < TOTAL_SCORES; ++i )
			output.writeInt( scores[i] );
	}

	public final void read( DataInputStream input ) throws Exception
	{
		for( int i = 0; i < TOTAL_SCORES; ++i )
			scores[i] = input.readInt();
	}

	private final void updateLabels()
	{
		for( int i = 0; i < scores.length; ++i )
		{
			final Label label = ( Label )getDrawable( i + N_NON_SELECTABLE_ENTRIES );

			label.setText( ( i + 1 ) + ". " + GameMIDlet.formatNumStr( scores[i], String.valueOf( MAX_SCORE ).length(), '0' ) );

			label.defineReferencePixel( label.getWidth() >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, label.getRefPixelY() );
		}
	}
	
	public final void update( int delta )
	{
		// Visa corrigir um bug de posicionamento que s� ficou evidente no LG MG155. � melhor deixar para todo
		// mundo, j� que a perda de desempenho nesta tela n�o � algo com o que devemos nos reocupar
		final GameMenuBkg bkg = ( GameMenuBkg )getDrawable( GAME_MENU_BKG_INDEX );
		final Drawable middleCenter = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_MIDDLE_CENTER );
		final Drawable bottomCenter = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_CENTER );
		final int middleCenterEnd = middleCenter.getPosY() + middleCenter.getHeight();
		if( bottomCenter.getPosY() != middleCenterEnd )
		{
			final Drawable bottomLeft = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_LEFT );
			final Drawable bottomRight = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_RIGHT );

			bottomLeft.setPosition( 0, middleCenterEnd );
			bottomCenter.setPosition( bottomLeft.getWidth(), middleCenterEnd );
			bottomRight.setPosition( bottomLeft.getWidth() + bottomCenter.getWidth(), middleCenterEnd );
			positionItems();
			updateCursorPosition();
		}
		
		super.update( delta );

		if( lastHighScoreIndex >= 0 )
		{
			accTime += delta;

			if( accTime >= BLINK_RATE )
			{
				accTime %= BLINK_RATE;

				final Drawable aux = getDrawable( lastHighScoreIndex + N_NON_SELECTABLE_ENTRIES );
				aux.setVisible( !aux.isVisible() );
			}
		}
	}

	private final void stopBlink()
	{
		if( lastHighScoreIndex >= 0 )
			getDrawable( lastHighScoreIndex + N_NON_SELECTABLE_ENTRIES ).setVisible( true );

		lastHighScoreIndex = -1;
	}

	public static final void eraseRecords()
	{
		try
		{
			GameMIDlet.eraseSlot( DATABASE_NAME, DATABASE_SLOT_SCORES );
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif
		}

		loadRecords();
	}

	private static final void loadRecords()
	{
		try
		{
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_SCORES, instance );
		}
		catch( Exception e )
		{
			//#if DEBUG == "true"
//# 				e.printStackTrace();
			//#endif

			// Preenche a tabela de pontos com valores "dummy"
			for( int i = 0; i < TOTAL_SCORES; ++i )
				scores[i] = ( TOTAL_SCORES - i ) * DUMMY_BASE;

			try
			{
				GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_SCORES, instance );
			}
			catch( Exception ex )
			{
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
			}
		}
	}
}
