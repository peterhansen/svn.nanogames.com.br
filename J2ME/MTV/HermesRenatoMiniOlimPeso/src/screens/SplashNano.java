/*
 * SplashNano.java
 */
package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;

//#ifdef SIEMENS
//# import br.com.nanogames.components.util.MediaPlayer;
//#endif

import core.Constants;

/**
 * @author Daniel L. Alves
 */
public final class SplashNano extends UpdatableGroup implements Constants, KeyListener
{
	private final int BACKLIGHT_COLOR = 0x1A9DBA;

	private int lightModule;

	//#if SCREEN_SIZE == "SMALL"
//# 		// velocidade do efeito da luz em pixels por segundo
//# 		private final int LIGHT_MOVE_SPEED = 115;
//# 
//# 		private final int LIGHT_X_OFFSET = 0;
//# 		private final int LIGHT_X_LEFT;
//# 		private final int LIGHT_X_RIGHT;	
//# 
//# 		// offsets na posi��o dos logos em rela��o ao texto "Nano"
//# 		private final short IMG_GAMES_OFFSET_X = 26;
//# 		private final short IMG_GAMES_OFFSET_Y = 10;
//# 		private final short IMG_TURTLE_OFFSET_X = 24;
//# 		private final short IMG_TURTLE_OFFSET_Y = -32;	
	//#else
	// velocidade do efeito da luz em pixels por segundo
	private final int LIGHT_MOVE_SPEED = 157;

	private final int LIGHT_X_OFFSET = 8;

	private final int LIGHT_X_LEFT;

	private final int LIGHT_X_RIGHT;

	// offsets na posi��o dos logos em rela��o ao texto "Nano"
	private final short IMG_GAMES_OFFSET_X = 65;

	private final short IMG_GAMES_OFFSET_Y = 25;

	private final short IMG_TURTLE_OFFSET_X = 59;

	private final short IMG_TURTLE_OFFSET_Y = -69;
	//#endif
	private static final byte TOTAL_ITEMS = 10;

	private final DrawableImage imgNano;

	private final DrawableImage imgNanoLight;

	private final DrawableImage imgGames;

	private final DrawableImage imgTurtle;

	private final ImageFont font;

	private final Pattern transparency;

	private final Pattern backColor;

	private final Pattern blackLeft;

	private final Pattern blackRight;

	// estados da anima��o do splash
	private final byte STATE_NANO_APPEARS = 0;

	private final byte STATE_LIGHT_RIGHT = STATE_NANO_APPEARS + 1;

	private final byte STATE_LIGHT_LEFT = STATE_LIGHT_RIGHT + 1;

	private final byte STATE_WAIT = STATE_LIGHT_LEFT + 1;

	private final byte STATE_NONE = STATE_WAIT + 1;

	// dura��o em milisegundos de cada estado de anima��o
	private final short DURATION_NANO_APPEARS = 600;

	private final short DURATION_WAIT = 2100;

	/** pr�xima tela */
	private final byte NEXT_SCREEN_INDEX;

	private byte currentState;

	private int accTime;

	/** Creates a new instance of SplashNano */
	public SplashNano( String imagesPath, String text, byte nextScreenIndex ) throws Exception
	{
		super( TOTAL_ITEMS );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		final Pattern bkg = new Pattern( null );
		bkg.setSize( size );
		bkg.setFillColor( 0x000000 );
		insertDrawable( bkg );

		NEXT_SCREEN_INDEX = nextScreenIndex;

		// logo da Nano
		imgNano = new DrawableImage( imagesPath + "nano.png" );
		imgNano.defineReferencePixel( imgNano.getWidth() >> 1, 0 );

		//#if SCREEN_SIZE == "SMALL"
//# 		imgNano.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT - 6 );
		//#else
			imgNano.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		//#endif

		LIGHT_X_LEFT = imgNano.getPosX() + LIGHT_X_OFFSET;
		LIGHT_X_RIGHT = imgNano.getPosX() + imgNano.getWidth();

		// efeito da luz passando
		imgNanoLight = new DrawableImage( imagesPath + "light.png" );
		imgNanoLight.setPosition( imgNano.getPosX() - imgNanoLight.getWidth(), imgNano.getPosY() );

		// cor de fundo do logo
		backColor = new Pattern( null );
		backColor.setFillColor( BACKLIGHT_COLOR );
		backColor.setSize( imgNano.getSize() );
		backColor.setPosition( imgNano.getPosition() );

		// adiciona as �reas pretas � direita e esquerda do texto "Nano", para evitar que o efeito da luz passando fique errado
		blackLeft = new Pattern( null );
		blackLeft.setFillColor( 0x000000 );
		blackLeft.setSize( imgNano.getPosX(), imgNano.getHeight() );
		blackLeft.setPosition( 0, imgNano.getPosY() );

		blackRight = new Pattern( null );
		blackRight.setFillColor( 0x000000 );
		blackRight.setSize( ScreenManager.SCREEN_WIDTH - LIGHT_X_RIGHT, imgNano.getHeight() );
		blackRight.defineReferencePixel( blackRight.getWidth(), 0 );
		blackRight.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, imgNano.getPosY() );

		insertDrawable( backColor );
		insertDrawable( imgNanoLight );
		insertDrawable( blackLeft );
		insertDrawable( blackRight );
		insertDrawable( imgNano );

		imgGames = new DrawableImage( imagesPath + "games.png" );
		imgGames.setPosition( imgNano.getPosX() + IMG_GAMES_OFFSET_X, imgNano.getPosY() + IMG_GAMES_OFFSET_Y );
		insertDrawable( imgGames );

		imgTurtle = new DrawableImage( imagesPath + "turtle.png" );
		imgTurtle.setPosition( imgNano.getPosX() + IMG_TURTLE_OFFSET_X, imgNano.getPosY() + IMG_TURTLE_OFFSET_Y );
		insertDrawable( imgTurtle );

		font = ImageFont.createMultiSpacedFont( imagesPath + "font_credits.png", imagesPath + "font_credits.dat" );

		final RichLabel label = new RichLabel( font, text, size.x * 3 / 4, null );
		label.defineReferencePixel( label.getWidth() >> 1, label.getHeight() );
		label.setRefPixelPosition( size.x >> 1, size.y );
		insertDrawable( label );

		transparency = new Pattern( new DrawableImage( imagesPath + "transparency.png" ) );
		transparency.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		transparency.defineReferencePixel( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		transparency.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( transparency );

		setState( STATE_NANO_APPEARS );
	} // fim do construtor SplashNano( String )

	private final void setState( int state )
	{
		switch( state )
		{
			case STATE_NANO_APPEARS:
				break;

			case STATE_LIGHT_RIGHT:
				//#ifdef SIEMENS
//# 					MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
				//#endif
				transparency.setVisible( false );

			case STATE_LIGHT_LEFT:
			case STATE_WAIT:
			case STATE_NONE:
				break;

			default:
				return;
		} // fim switch ( state )

		currentState = ( byte ) state;
		lightModule = 0;
		accTime = 0;
	} // fim do m�todo setState( byte )

	public final void update( int delta )
	{
		super.update( delta );

		accTime += delta;
		switch( currentState )
		{
			case STATE_NANO_APPEARS:
				if( accTime >= DURATION_NANO_APPEARS )
				{
					setState( currentState + 1 );
				}
				break;

			case STATE_LIGHT_RIGHT:
				final int dsRight = lightModule + ( LIGHT_MOVE_SPEED * delta );
				final int dxRight = dsRight / 1000;
				lightModule = dsRight % 1000;

				imgNanoLight.move( dxRight, 0 );

				if( imgNanoLight.getPosX() >= LIGHT_X_RIGHT )
				{
					imgNanoLight.setPosition( LIGHT_X_RIGHT, imgNanoLight.getPosY() );
					setState( currentState + 1 );
				}
				break;

			case STATE_LIGHT_LEFT:
				final int dsLeft = lightModule - ( LIGHT_MOVE_SPEED * delta );
				final int dxLeft = dsLeft / 1000;
				lightModule = dsLeft % 1000;

				imgNanoLight.move( dxLeft, 0 );

				if( imgNanoLight.getPosX() <= LIGHT_X_LEFT )
				{
					imgNanoLight.setPosition( LIGHT_X_LEFT, imgNanoLight.getPosY() );
					setState( currentState + 1 );
				}
				break;

			case STATE_WAIT:
				if( accTime >= DURATION_WAIT )
					callNextScreen();
				break;
		} // fim switch ( state )		
	} // fim do m�todo update( int )

	public void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.exit();
				break;

			// Os splashs n�o podem ser acelerados pelo usu�rio
			//#if DEBUG == "true"
//# 			default:
//# 				callNextScreen();
			//#endif
		}
	}

	public void keyReleased( int key )
	{
	}
	
	private final void callNextScreen()
	{
		setState( STATE_NONE );
		AppMIDlet.setScreen( NEXT_SCREEN_INDEX );
	}
}
