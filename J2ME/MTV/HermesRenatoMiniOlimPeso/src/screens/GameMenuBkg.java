/**
 * GameMenuBkg.java
 * �2008 Nano Games.
 *
 * Created on 24/04/2008 17:51:53.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class GameMenuBkg extends DrawableGroup
{
	/** Dist�ncia do desenho do cr�nio para a extremidade inferior esquerda da tela */
	//#if SCREEN_SIZE == "SMALL"
//# 	public static final byte SKULL_DIST_FROM_SCREEN_X = 8;
//# 	public static final byte SKULL_DIST_FROM_SCREEN_Y = 0;
	//#elif SCREEN_SIZE == "MEDIUM"
	public static final byte SKULL_DIST_FROM_SCREEN_X = 10;
	public static final byte SKULL_DIST_FROM_SCREEN_Y = -6;
	//#else // if SCREEN_SIZE == "BIG"
//# 		public static final byte SKULL_DIST_FROM_SCREEN_X = 18;
//# 		public static final byte SKULL_DIST_FROM_SCREEN_Y = 4;
	//#endif
	

	/** Dist�ncia do desenho do m�o para a extremidade direita da tela */
	public static final byte METAL_HAND_DIST_FROM_SCREEN_X = 7;

	/** Quanto do pegador de papel fica para fora da prancheta */
	//#if SCREEN_SIZE == "SMALL"
//# 		public static final byte CLIP_HEIGHT_OUTSIDE_CLIPBOARD = 6;
	//#else
	public static final byte CLIP_HEIGHT_OUTSIDE_CLIPBOARD = 10;
	//#endif

	/** Altura de uma linha na folha de caderno da clipboard. OBS: Ver imagem do clipboard, pois as op��es devem
	 * estar centralizadas nas linhas da folha de papel
	 */
	//#if SCREEN_SIZE == "SMALL"
//# 		public static final byte LINE_HEIGHT = 9;
	//#else
		public static final byte LINE_HEIGHT = 13;
	//#endif

	/** Parte do clipboard, na direita, que n�o � ocupada pela imagem do papel */
	//#if SCREEN_SIZE == "SMALL"
//# 		public static final byte CLIPBOARD_NO_PAPER_WIDTH = 3;
	//#else
	public static final byte CLIPBOARD_NO_PAPER_WIDTH = 4;
	//#endif
	
	/** Quanto (eixo Y) da quina superior do clipboard � transparente */
	private final byte CORNER_EXTRA = 4;
	
	/** Dist�ncia em pixels para a 1a linha da imagem da pattern central */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte DISTANCE_TO_FIRST_LINE = 5;
	//#else
	private final byte DISTANCE_TO_FIRST_LINE = 1;
	//#endif
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos elementos do grupo">
	
	public static final byte BKG_CLIPBOARD_TOP_LEFT			= 0;
	public static final byte BKG_CLIPBOARD_TOP_CENTER		= 1;
	public static final byte BKG_CLIPBOARD_TOP_RIGHT		= 2;
	public static final byte BKG_CLIPBOARD_BOTTOM_LEFT		= 3;
	public static final byte BKG_CLIPBOARD_BOTTOM_CENTER	= 4;
	public static final byte BKG_CLIPBOARD_BOTTOM_RIGHT		= 5;
	public static final byte BKG_CLIPBOARD_MIDDLE_LEFT		= 6;
	public static final byte BKG_CLIPBOARD_MIDDLE_CENTER	= 7;
	public static final byte BKG_CLIPBOARD_MIDDLE_RIGHT		= 8;
	public static final byte BKG_CLIPBOARD_TITLE			= 9;
	public static final byte BKG_CLIPBOARD_SKULL			= 10;
	public static final byte BKG_CLIPBOARD_METAL_HAND		= 11;
	public static final byte BKG_CLIPBOARD_METAL_CLIP		= 12;
	
	//</editor-fold>
		
	/** Construtor */
	public GameMenuBkg() throws Exception
	{
		super( 13 );
		final GameMIDlet midlet = ( GameMIDlet )GameMIDlet.getInstance();
		
		// Parte superior
		final String imgExt = ".png";
		final DrawableImage topLeft = midlet.TOP_LEFT;
		insertDrawable( topLeft );
		
		final Pattern topCenter = midlet.TOP_CENTER;
		insertDrawable( topCenter );
		
		final DrawableImage topRight = midlet.TOP_RIGHT;
		insertDrawable( topRight );
		
		topCenter.setSize( ScreenManager.SCREEN_WIDTH - ( topLeft.getWidth() + topRight.getWidth() ), topLeft.getHeight() );
		
		topLeft.setPosition( 0, CLIP_HEIGHT_OUTSIDE_CLIPBOARD );
		topCenter.setPosition( topLeft.getWidth(), CLIP_HEIGHT_OUTSIDE_CLIPBOARD );
		topRight.setPosition( topLeft.getWidth() + topCenter.getWidth(), CLIP_HEIGHT_OUTSIDE_CLIPBOARD );
		
		// Parte inferior
		final DrawableImage bottomLeft = midlet.BOTTOM_LEFT;
		insertDrawable( bottomLeft );
		
		final Pattern bottomCenter = midlet.BOTTOM_CENTER;
		insertDrawable( bottomCenter );
		
		final DrawableImage bottomRight = midlet.BOTTOM_RIGHT;
		insertDrawable( bottomRight );
		
		bottomCenter.setSize( ScreenManager.SCREEN_WIDTH - ( bottomLeft.getWidth() + bottomRight.getWidth() ), bottomLeft.getHeight() );
		
		// Parte do meio
		final Pattern middleLeft = midlet.MIDDLE_LEFT;
		insertDrawable( middleLeft );

		final Pattern middleCenter = midlet.MIDDLE_CENTER;
		insertDrawable( middleCenter );
		
		final Pattern middleRight = midlet.MIDDLE_RIGHT;
		insertDrawable( middleRight );
		
		int middlePatternsHeight = ScreenManager.SCREEN_HEIGHT - ( topLeft.getHeight() + bottomLeft.getHeight() + CLIP_HEIGHT_OUTSIDE_CLIPBOARD );
		middlePatternsHeight = nextMulOf( middlePatternsHeight, LINE_HEIGHT );
			
		middleLeft.setSize( middleLeft.getFill().getWidth(), middlePatternsHeight );
		middleRight.setSize( middleRight.getFill().getWidth(), middlePatternsHeight );
		middleCenter.setSize( ScreenManager.SCREEN_WIDTH - ( middleLeft.getWidth() + middleRight.getWidth() ), middlePatternsHeight );
		
		int patternY = topLeft.getHeight() + CLIP_HEIGHT_OUTSIDE_CLIPBOARD;
		middleLeft.setPosition( 0, patternY );
		middleCenter.setPosition( middleLeft.getWidth(), patternY );
		middleRight.setPosition( middleLeft.getWidth() + middleCenter.getWidth(), patternY );
		
		patternY = topLeft.getHeight() + middlePatternsHeight + CLIP_HEIGHT_OUTSIDE_CLIPBOARD;
		bottomLeft.setPosition( 0, patternY );
		bottomCenter.setPosition( bottomLeft.getWidth(), patternY );
		bottomRight.setPosition( bottomLeft.getWidth() + bottomCenter.getWidth(), patternY );

		// Outros elementos
		patternY = getFirstLineY() - 1;
		final DrawableImage title = new DrawableImage( midlet.TITLE );
		insertDrawable( title );
		title.setPosition( middleLeft.getWidth() + (( ScreenManager.SCREEN_WIDTH - middleLeft.getWidth() - title.getWidth() ) >> 1), patternY - LINE_HEIGHT );
		
		final DrawableImage skullDrawing = new DrawableImage( midlet.SKULL );
		insertDrawable( skullDrawing );
		
		//#if SCREEN_SIZE == "SMALL"
//# 			final DrawableImage softKeyLeft = new DrawableImage( "/btOk" + imgExt );
//# 			skullDrawing.setPosition( bottomLeft.getWidth() + SKULL_DIST_FROM_SCREEN_X, ScreenManager.SCREEN_HEIGHT - softKeyLeft.getHeight() - skullDrawing.getHeight() - SKULL_DIST_FROM_SCREEN_Y );
		//#else
		skullDrawing.setPosition( bottomLeft.getWidth() + SKULL_DIST_FROM_SCREEN_X, bottomLeft.getPosY() - skullDrawing.getHeight() - SKULL_DIST_FROM_SCREEN_Y );
		//#endif
		
		final DrawableImage metalHandDrawing = new DrawableImage( midlet.METAL_HAND );
		insertDrawable( metalHandDrawing );
		metalHandDrawing.setPosition( ScreenManager.SCREEN_WIDTH - metalHandDrawing.getWidth() - METAL_HAND_DIST_FROM_SCREEN_X - CLIPBOARD_NO_PAPER_WIDTH, patternY + (( middleRight.getHeight() - metalHandDrawing.getHeight() ) >> 1 ));
		
		//#if SCREEN_SIZE == "SMALL"
//# 			if( ScreenManager.SCREEN_HEIGHT < 150 )
//# 			{
//# 				skullDrawing.setVisible( false );
//# 				metalHandDrawing.setVisible( false );
//# 			}
		//#endif
		
		final DrawableImage clip = new DrawableImage( midlet.CLIP );
		insertDrawable( clip );
		
		clip.setPosition( ( ScreenManager.SCREEN_WIDTH - clip.getWidth() ) >> 1, 0 );
		
		// Determina o tamanho do grupo
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
	
	public void enlarge( int forceMiddlePatternHeight, int startAtY )
	{
		final int diff = forceMiddlePatternHeight - drawables[ BKG_CLIPBOARD_MIDDLE_LEFT ].getHeight();
		if( diff <= 0 )
			return;

		drawables[ BKG_CLIPBOARD_SKULL ].setVisible( false );
		drawables[ BKG_CLIPBOARD_METAL_HAND ].setVisible( false );
		
		final int middlePatternsHeight = nextMulOf( forceMiddlePatternHeight + ( startAtY - drawables[ BKG_CLIPBOARD_MIDDLE_CENTER ].getPosY() ), LINE_HEIGHT );
		
		drawables[ BKG_CLIPBOARD_MIDDLE_LEFT   ].setSize( drawables[ BKG_CLIPBOARD_MIDDLE_LEFT   ].getWidth(), middlePatternsHeight );
		drawables[ BKG_CLIPBOARD_MIDDLE_CENTER ].setSize( drawables[ BKG_CLIPBOARD_MIDDLE_CENTER ].getWidth(), middlePatternsHeight );
		drawables[ BKG_CLIPBOARD_MIDDLE_RIGHT  ].setSize( drawables[ BKG_CLIPBOARD_MIDDLE_RIGHT  ].getWidth(), middlePatternsHeight );
		
		final int patternY = drawables[ BKG_CLIPBOARD_TOP_LEFT ].getHeight() + middlePatternsHeight + CLIP_HEIGHT_OUTSIDE_CLIPBOARD;
		drawables[ BKG_CLIPBOARD_BOTTOM_LEFT   ].setPosition( 0, patternY );
		drawables[ BKG_CLIPBOARD_BOTTOM_CENTER ].setPosition( drawables[ BKG_CLIPBOARD_BOTTOM_LEFT ].getWidth(), patternY );
		drawables[ BKG_CLIPBOARD_BOTTOM_RIGHT  ].setPosition( drawables[ BKG_CLIPBOARD_BOTTOM_LEFT ].getWidth() + drawables[ BKG_CLIPBOARD_BOTTOM_CENTER ].getWidth(), patternY );
		
		drawables[ BKG_CLIPBOARD_SKULL ].move( 0, diff );
		drawables[ BKG_CLIPBOARD_METAL_HAND ].move( 0, diff );
		
		setSize( getSize().x, drawables[ BKG_CLIPBOARD_TOP_LEFT ].getHeight() + middlePatternsHeight + drawables[ BKG_CLIPBOARD_BOTTOM_LEFT ].getHeight() + CLIP_HEIGHT_OUTSIDE_CLIPBOARD );
	}
	
	public void checkDrawableCollision( int myDrawableIndex, Drawable d )
	{
		if( !drawables[ myDrawableIndex ].isVisible() )
			return;

		final Rectangle r = new Rectangle( d.getPosX(), d.getPosY(), d.getWidth(), d.getHeight() );
		final Rectangle myDrawableRect = new Rectangle( drawables[ myDrawableIndex ].getPosX(), drawables[ myDrawableIndex ].getPosY(), drawables[ myDrawableIndex ].getWidth(), drawables[ myDrawableIndex ].getHeight() );
		
		if( myDrawableRect.intersects( r ) )
			drawables[ myDrawableIndex ].setVisible( false );
	}
		
	/** Retorna a coordenada Y da primeira linha da folha de papel */
	public int getFirstLineY()
	{
		return drawables[ BKG_CLIPBOARD_MIDDLE_CENTER ].getPosY() + DISTANCE_TO_FIRST_LINE;
	}
	
	/** Obt�m a coordenada Y da n-�sima linha ap�s o t�tulo */
	public int getNLineAfterTitleY( int n )
	{
		return getFirstLineY() + ( n * LINE_HEIGHT );
	}
	
	/** Desenha o menu  na tela */
	protected void paint( Graphics g )
	{
		// Coloca uma cor de fundo
		g.setColor( 255, 188, 0 );
		g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, CLIP_HEIGHT_OUTSIDE_CLIPBOARD + CORNER_EXTRA );
		
		// Desenha os componentes do grupo
		super.paint( g );
	}

	/** Retorna quantos pixels temos at� a margem vermelha do caderno */
	public static int getWidthBeforeRedMargin() throws Exception
	{
		return (( GameMIDlet )AppMIDlet.getInstance()).MIDDLE_LEFT.getWidth();
	}
	
	/** Obt�m o pr�ximo inteiro m�ltiplo de m */
	public static int nextMulOf( int n, int m )
	{
		final int aux = n % m;
		if( aux == 0 )
			return n;
		return n + ( m - aux );
	}
}
