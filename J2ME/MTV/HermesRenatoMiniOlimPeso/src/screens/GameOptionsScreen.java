/**
 * GameOptionsScreen.java
 * �2008 Nano Games.
 *
 * Created on 25/04/2008 17:33:05.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;

/**
 * @author Daniel L. Alves
 */

public final class GameOptionsScreen extends GameMenu
{
	private final ImageFont font;

	//#ifndef NO_SOUND
		private final byte INDEX_SOUND;

		private final String textSoundOn;
		private final String textSoundOff;
		
		private boolean playSoundOnToggle;
	//#endif

	private final byte INDEX_VIBRATION;

	private final String textVibrationOn;
	private final String textVibrationOff;

	/**
	 * Cria uma nova inst�ncia de um menu b�sico de op��es.
	 * @param listener
	 * @param font
	 * @param id
	 * @param entries
	 * @param backIndex 
	 * @param soundIndex �ndice da entrada relativa � op��o ligar/desligar som. Caso essa op��o n�o exista, basta
	 * passar valores negativos como argumento.
	 * @param soundOffText �ndice do texto de som desligado. Caso n�o exista a op��o de ligar/desligar som, esse
	 * valor � ignorado.
	 * @param vibrationIndex �ndice da entrada relativa � op��o ligar/desligar vibra��o. Caso essa op��o n�o 
	 * exista, basta passar valores negativos como argumento.
	 * @param vibrationOffText �ndice do texto de vibra��o desligada. Caso n�o exista a op��o de ligar/desligar 
	 * som, esse valor � ignorado.
	 * @throws java.lang.Exception
	 */
	//#ifdef NO_SOUND
//# 	public GameOptionsScreen( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, int vibrationIndex, int vibrationOffText ) throws Exception
	//#else
public GameOptionsScreen( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, int soundIndex, int soundOffText, int vibrationIndex, int vibrationOffText, boolean playSoundOnToggle ) throws Exception
	//#endif
	{
		super( listener, id, font, entries, backIndex, ( byte )0 );

		this.font = font;
		
		//#ifndef NO_SOUND
			this.playSoundOnToggle = playSoundOnToggle;

			INDEX_SOUND = ( byte ) soundIndex;
			if( INDEX_SOUND >= 0 )
			{
				textSoundOn = AppMIDlet.getText( entries[INDEX_SOUND] );
				textSoundOff = AppMIDlet.getText( soundOffText );

				// Se a op��o colide com a m�o metal, torna-a invis�vel
				final Label l = ( Label ) getDrawable( INDEX_SOUND + N_NON_SELECTABLE_ENTRIES );			

				l.setText( textSoundOff );
				(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_METAL_HAND, l );

				//#if SCREEN_SIZE == "SMALL"
//# 				(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_SKULL, l );
				//#endif

				l.setText( textSoundOn );
				(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_METAL_HAND, l );

				//#if SCREEN_SIZE == "SMALL"
//# 				(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_SKULL, l );
				//#endif

				// Coloca o texto correto no label
				updateText( INDEX_SOUND );
			}
			else
			{
				textSoundOff = null;
				textSoundOn = null;
			}
		//#endif

		INDEX_VIBRATION = ( byte ) vibrationIndex;
		if( INDEX_VIBRATION >= 0 )
		{
			textVibrationOn = AppMIDlet.getText( entries[INDEX_VIBRATION] );
			textVibrationOff = AppMIDlet.getText( vibrationOffText );
			
			// Se a op��o colide com a m�o metal, torna-a invis�vel
			final Label l = ( Label ) getDrawable( INDEX_VIBRATION + N_NON_SELECTABLE_ENTRIES );			
			
			l.setText( textVibrationOff );
			(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_METAL_HAND, l );
			
			//#if SCREEN_SIZE == "SMALL"
//# 			(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_SKULL, l );
			//#endif
			
			l.setText( textVibrationOn );
			(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_METAL_HAND, l );
			
			//#if SCREEN_SIZE == "SMALL"
//# 			(( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ]).checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_SKULL, l );
			//#endif

			// Coloca o texto correto no label
			updateText( INDEX_VIBRATION );
		}
		else
		{
			textVibrationOn = null;
			textVibrationOff = null;
		}
	}

	public final void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		switch( key )
		{
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.RIGHT:
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				//#ifndef NO_SOUND
				if( currentIndex == INDEX_SOUND + N_NON_SELECTABLE_ENTRIES )
				{
					toggleSound();
					return;
				}
				else
				{
				//#endif
					if( currentIndex == INDEX_VIBRATION + N_NON_SELECTABLE_ENTRIES )
					{
						toggleVibration();
						return;
					}
				//#ifndef NO_SOUND
				}
				//#endif

			default:
				super.keyPressed( key );
		}
	}

//#ifndef NO_SOUND
	private final void toggleSound()
	{
		if( MediaPlayer.isMuted() )
		{
			MediaPlayer.setMute( false );
			
			//#ifndef SIEMENS
 			if( playSoundOnToggle )
 			{
				//#if SAMSUNG_BAD_SOUND == "true"
//# 					MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
				//#else
 					MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
				//#endif
 			}
			//#endif
		}
		else
		{
			MediaPlayer.setMute( true );
			MediaPlayer.stop();
			MediaPlayer.free();
		}

		// Atualiza o texto de forma correspondente
		updateText( INDEX_SOUND );
	}
//#endif

	private final void toggleVibration()
	{
		final boolean previousOption = MediaPlayer.isVibration();
		MediaPlayer.setVibration( !MediaPlayer.isVibration() );
		
		MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );

		// Caso a op��o tenha sido alterada, atualiza o texto de forma correspondente
		if( previousOption != MediaPlayer.isVibration() )
			updateText( INDEX_VIBRATION );
	}

	public void update( int delta )
	{
		// Visa corrigir um bug de posicionamento que s� ficou evidente no LG MG155. � melhor deixar para todo
		// mundo, j� que a perda de desempenho nesta tela n�o � algo com o que devemos nos reocupar
		final GameMenuBkg bkg = ( GameMenuBkg )getDrawable( GAME_MENU_BKG_INDEX );
		final Drawable middleCenter = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_MIDDLE_CENTER );
		final Drawable bottomCenter = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_CENTER );
		final int middleCenterEnd = middleCenter.getPosY() + middleCenter.getHeight();
		if( bottomCenter.getPosY() != middleCenterEnd )
		{
			final Drawable bottomLeft = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_LEFT );
			final Drawable bottomRight = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_RIGHT );
			
			bottomLeft.setPosition( 0, middleCenterEnd );
			bottomCenter.setPosition( bottomLeft.getWidth(), middleCenterEnd );
			bottomRight.setPosition( bottomLeft.getWidth() + bottomCenter.getWidth(), middleCenterEnd );
			positionItems();
			updateCursorPosition();
		}
		
		super.update( delta );
	}

	private final void updateText( byte index )
	{
		final Label l = ( Label ) getDrawable( index + N_NON_SELECTABLE_ENTRIES );

		//#ifndef NO_SOUND
		if( index == INDEX_SOUND )
		{
			l.setText( MediaPlayer.isMuted() ? textSoundOff : textSoundOn );
		}
		else
		{
		//#endif
			if( index == INDEX_VIBRATION )
				l.setText( MediaPlayer.isVibration() ? textVibrationOn : textVibrationOff );
		//#ifndef NO_SOUND
		}
		//#endif

		// Como a largura do texto pode mudar, atualiza a posi��o do cursor
		updateCursorPosition();
	}
}


