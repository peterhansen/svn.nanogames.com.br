/**
 * GameMenu.java
 * �2008 Nano Games.
 *
 * Created on 22/04/2008 10:49:57.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;

/**
 * @author Daniel L. Alves
 */

public class GameMenu extends Menu implements Constants, MenuListener
{
	/** Background */
	protected final byte N_NON_SELECTABLE_ENTRIES;
		
	/** �ndice da op��o do menu que reprenta a op��o de votlar */
	private byte backIndex;
	
	/** Listener original do menu */
	private MenuListener gameMenuListener;
	
	/** �ndice do background no menu */
	protected final byte GAME_MENU_BKG_INDEX = 0;
	
	/** �ndice do primeiro label no menu */
	protected final byte FIRST_LABEL_INDEX = 1;
	
	/** Construtor */
	public GameMenu( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, byte nNonSelectableItems ) throws Exception
	{
		// +1 => Background
		super( listener, id, entries.length + 1 );
		N_NON_SELECTABLE_ENTRIES = ( byte )( nNonSelectableItems + 1 );
		
		String[] aux = new String[ entries.length ];
		for( int i = 0 ; i < aux.length ; ++i )
			aux[i] = AppMIDlet.getText( entries[ i ] );
			
		construct( listener, font, aux, backIndex );
	}
	
	/** Construtor */
	protected GameMenu( MenuListener listener, int id, byte nEntries, byte nNonSelectableItems ) throws Exception
	{
		// +1 => Background
		super( listener, id, nEntries + 1 );
		N_NON_SELECTABLE_ENTRIES = ( byte )( nNonSelectableItems + 1 );
	}
	
	/** JAVA nojento que n�o deixa eu executar nada dentro de um construtor antes de uma chamada para super()
	 * ou this() !!!!!!!!!!! */
	protected void construct( MenuListener listener, ImageFont font, String[] entries, int backIndex ) throws Exception
	{
		// Armazena os par�metros necess�rios
		this.listener = this;		
		gameMenuListener = listener;
		
		this.backIndex = ( byte )( backIndex + N_NON_SELECTABLE_ENTRIES );
		
		// Determina o tamanho da cole��o
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// Indica que o menu � circular
		setCircular( true );
		
		// Insere o background na cole��o
		insertDrawable( new GameMenuBkg() );
		
		// Cria as entradas do menu
		for( int i = 0; i < entries.length; ++i )
			insertDrawable( new Label( font, entries[i] ) );
		
		// Posiciona os itens do menu
		positionItems();
		
		// Cria o cursor do menu
		setCursor( createCursor(), CURSOR_DRAW_AFTER_MENU, ANCHOR_TOP | ANCHOR_LEFT );
		setCurrentIndex( N_NON_SELECTABLE_ENTRIES );
	}
	
	protected void positionItems()
	{
		// Obt�m uma refer�ncia do background
		final GameMenuBkg bkg = ( GameMenuBkg )getDrawable( GAME_MENU_BKG_INDEX );
		
		// Verifica qual � a op��o mais larga do menu
		int widestOpt = 0;
		for( int i = N_NON_SELECTABLE_ENTRIES; i < drawables.length; ++i )
		{
			final int optWidth = (( Label )getDrawable( i )).getWidth();
			if( optWidth > widestOpt )
				widestOpt = optWidth;
		}
		
		// Calcula a coordenada X das entradas do menu
		final ImageFont font = (( Label )getDrawable( FIRST_LABEL_INDEX )).getFont();
		final Drawable aux = (( DrawableGroup )drawables[ GAME_MENU_BKG_INDEX ] ).getDrawable( GameMenuBkg.BKG_CLIPBOARD_TOP_LEFT );
		final int x = aux.getWidth() + (( ScreenManager.SCREEN_WIDTH - aux.getWidth() - widestOpt ) >> 1 ) - font.getCharWidth( '�' );
		
		// Calcula a coordenada Y da 1a entrada do menu. Teremos uma nova linha de caderno a cada LINE_HEIGHT
		// pixels
		final int firstLinePos = (( GameMenuBkg )drawables[ GAME_MENU_BKG_INDEX ] ).getFirstLineY();
		final int inc = ScreenManager.SCREEN_HEIGHT < 300 ? 0 : GameMenuBkg.LINE_HEIGHT;

		// -1 => Background
		final int totalOptsHeight = ( ( drawables.length - 1 )* ( GameMenuBkg.LINE_HEIGHT + inc ) ) - inc;
		final Drawable title = (( DrawableGroup )drawables[ GAME_MENU_BKG_INDEX ] ).getDrawable( GameMenuBkg.BKG_CLIPBOARD_TITLE  );
		final int titleEnd = title.getPosY() + title.getHeight();
		
		//#if SCREEN_SIZE == "SMALL"
//# 
//# 			int heightAvailable;
//# 			if( ScreenManager.SCREEN_HEIGHT < 150 )
//# 				heightAvailable = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_BOTTOM_CENTER ).getPosY() - titleEnd;
//# 			else
//# 				heightAvailable = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_SKULL ).getPosY() - titleEnd;
//# 			
//# 			int y = titleEnd + ( ( heightAvailable - totalOptsHeight ) >> 1 );
//# 
//# 			// Obt�m a coordenada y onde ficar� a primeira op��o do menu
//# 		    y = firstLinePos + GameMenuBkg.nextMulOf( y - firstLinePos, GameMenuBkg.LINE_HEIGHT ) + 1;
//# 			
//# 			if( ScreenManager.SCREEN_HEIGHT < 127 )
//# 			{
//# 				// Verifica se vamos ter espa�o para todas as op��es do menu
//# 				int diff = ( y + totalOptsHeight ) - ( titleEnd + heightAvailable + 5 );
//# 				if( diff > 0 )
//# 				{
//# 					// A diferen�a deve ser m�ltipla de GameMenuBkg.LINE_HEIGHT
//# 					diff = GameMenuBkg.nextMulOf( diff, GameMenuBkg.LINE_HEIGHT );
//# 
//# 					// Aumenta a parte do meio do background
//# 					final Drawable middleCenter = (( DrawableGroup )drawables[ GAME_MENU_BKG_INDEX ] ).getDrawable( GameMenuBkg.BKG_CLIPBOARD_MIDDLE_CENTER );
//# 					bkg.enlarge( middleCenter.getHeight() + diff, middleCenter.getPosY() );
//# 
//# 					// Determina o novo tamanho do menu
//# 					setSize( bkg.getSize() );
//# 
//# 					// Reposiciona o menu
//# 					setPosition( 0, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
//# 
//# 					// Recalcula y
//# 					y = titleEnd + ( ( heightAvailable + diff - totalOptsHeight ) >> 1 );
//# 					y = firstLinePos + GameMenuBkg.nextMulOf( y - firstLinePos, GameMenuBkg.LINE_HEIGHT ) + 1;
//# 				}
//# 			}
		//#else
		int heightAvailable = bkg.getDrawable( GameMenuBkg.BKG_CLIPBOARD_SKULL ).getPosY() - titleEnd;
		int y = titleEnd + ( ( heightAvailable - totalOptsHeight ) >> 1 );
	    y = firstLinePos + GameMenuBkg.nextMulOf( y - firstLinePos, GameMenuBkg.LINE_HEIGHT ) + 2;
		//#endif
		
		// Posiciona as entradas do menu
		Drawable d = null;
		for( int i = FIRST_LABEL_INDEX; i < drawables.length ; ++i )
		{
			d = getDrawable( i );
			d.setPosition( x, y );
			y += ( GameMenuBkg.LINE_HEIGHT + inc );
			
			// Se a op��o colide com a m�o metal, torna-a invis�vel
			bkg.checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_METAL_HAND, d );
			bkg.checkDrawableCollision( GameMenuBkg.BKG_CLIPBOARD_SKULL, d );
		}
	}

	/** Cria o cursor do menu */
	public static Drawable createCursor() throws Exception
	{	
		final byte CURSOR_REFERENCE_PIXEL_X = 1, CURSOR_REFERENCE_PIXEL_Y = 0;
		
		final Sprite menuCursor = new Sprite( "/cursorMenu.bin", "/cursorMenu" );
		menuCursor.defineReferencePixel( CURSOR_REFERENCE_PIXEL_X, CURSOR_REFERENCE_PIXEL_Y );
		return menuCursor;
	}
	
	/** Trata eventos de tecla */
	public void keyPressed( int key )
	{
		// �, querido leitor, por mais incr�vel que pare�a, acontece...
		if( this != ScreenManager.getInstance().getCurrentScreen() )
			return;
		
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if( backIndex >= 0 )
				{
					setCurrentIndex( backIndex );
					super.keyPressed( ScreenManager.KEY_NUM5 );
				}
				break;

			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
				break;

			default:
				super.keyPressed( key );
		}
	}
	
	/** Evento recebido quando o menu troca a op��o que est� selecionada */
	public void setCurrentIndex( int index )
	{
		//#ifndef LOW_JAR
			// S� precisamos deste if se for touch screen
			if( index < 0 )
				return;
		//#endif
			
		if( index < N_NON_SELECTABLE_ENTRIES )
		{
			if( currentIndex == N_NON_SELECTABLE_ENTRIES )
				super.setCurrentIndex( activeDrawables - 1 );
			else
				super.setCurrentIndex( N_NON_SELECTABLE_ENTRIES );
		}
		else
		{
			super.setCurrentIndex( index );
		}

		// Reinicia a anima��o do cursor
		(( Sprite )cursor ).setSequence( 0 );
	}

	public void onChoose( Menu menu, int id, int index )
	{
		if( gameMenuListener != null )
			gameMenuListener.onChoose( this, id, index - N_NON_SELECTABLE_ENTRIES );
	}

	public void onItemChanged( Menu menu, int id, int index )
	{
	}
	
	protected int getEntryAt( int x, int y )
	{
		x -= position.x;
		y -= position.y;

		for( byte i = N_NON_SELECTABLE_ENTRIES; i < activeDrawables; ++i )
		{
			final Drawable entry = drawables[i];
			if( entry.contains( x, y ) )
			{
				return i;
			}
		}
		return -1;
	}
}
