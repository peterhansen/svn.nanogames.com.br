/**
 * Constants.java
 */

package core;

/**
 * �2007 Nano Games
 * @author Daniel L. Alves
 */

public interface Constants
{
	/** �ndices utilizados para a represent��o das posi��es dos itens (barras de for�a, m�os, bra�os, ...)
	 * exibidos na tela */
	public static final byte ITEM_NONE = -1;

	public static final byte ITEM_LEFT = 0;

	public static final byte ITEM_RIGHT = 1;

	public static final byte ITEM_CENTER = 2;

	public static final byte BOTH_SIDES = 2;

	public static final byte TOTAL_HANDS = 3;
	
	/** N�mero de fases do jogo */
	public static final byte MAX_LEVEL = 15;
	
	/** Quantas etapas cada barra de for�a possui */
	public static final byte N_STRENGTH_BAR_STEPS = 3;
	
	/** Quanto, em porcentegem, cada etapa representa da for�a total */
	public static final byte STEP_MAX_STRENGTH_PERCENTAGE = 30;
	
	/** Tempo de dura��o da fase */
	//#ifdef BAD_INPUT
//# 	public static final byte TOTAL_LEVEL_TIME = 60;
	//#else
		public static final byte TOTAL_LEVEL_TIME = 60;
		//#ifdef BigA1200
//# 			public static final byte TOTAL_LEVEL_TIME_LAST_LEVELS = 75;
		//#endif
	//#endif
	
	/** M�xima pontua��o poss�vel */
	public static final int MAX_SCORE = 99999;
	
	/** N�mero m�ximo de vezes que o passarinho pode aparecer por fase */
	public static final byte MAX_BIRDIE_APPEARENCES_BY_LEVEL = 3;
	
	/** Tempo m�ximo que o passarinho fica pulando no peso */
	public static final int BIRDIE_JUMP_TIME_MAX = 8000;
	
	/** Tempo m�nimo que o passarinho fica pulando no peso */
	public static final int BIRDIE_JUMP_TIME_MIN = 5000;
	
	/** Quantidade m�nima de mem�ria para que o jogo rode com sua capacidade m�xima */
	public static final int LOW_MEMORY_LIMIT = 900000;
	
	/** Tempo de dura��o do splash do jogo */
	public static final short SPLASH_GAME_DUR = 3000;
	
	/** Tempo de dura��o do splash da marca */
	public static final short SPLASH_BRAND_DUR = 3000;
	
	/** Nome da base de dados */
	public static final String DATABASE_NAME = "n";
	
	/** �ndice do slot de op��es na base de dados */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �ndice do slot de recordes de pontos na base de dados */
	public static final byte DATABASE_SLOT_SCORES = 2;
	
	/** N�mero total de slots na base de dados */
	public static final byte DATABASE_TOTAL_SLOTS = 2;

	//<editor-fold defaultstate="collapsed" desc="�ndices das m�sicas/sons do jogo">
	
//#ifndef NO_SOUND
	// TODO : N�o tivemos tempo de fazer uma tela de zeramento e uma tela de gameover, ent�o o n�mero total de
	// sons fica menor
	//public static final byte SOUND_TOTAL = 5;

	/** Quantidade total de m�sicas / sons do jogo */
	public static final byte SOUND_TOTAL = 3;
		
	public static final byte SOUND_INDEX_THEME				= 0;
	public static final byte SOUND_INDEX_LEVEL_COMPLETED	= 1;
	public static final byte SOUND_INDEX_LIFE_LOST			= 2;	
	
	// TODO : N�o tivemos tempo de fazer uma tela de zeramento e uma tela de gameover, ent�o n�o utilizamos
	// estas m�sicas
	//	public static final byte SOUND_INDEX_GAMEOVER			= 3;
	//	public static final byte SOUND_INDEX_ENDING				= 4;
	
//#endif
	
	//</editor-fold>
	
	/** Caminho das imagens da tela de splash */
	public static final String PATH_SPLASH = "/Splash/";

	/** N�mero de vezes que uma m�sica � repetida quando n�o possu�mos suporte a loop */
//#ifndef NO_SOUND
	//#if SAMSUNG_BAD_SOUND == "true"
//# 		public static final byte SAMSUNG_LOOP_INFINITE = 100;
	//#endif
//#endif
	
	/** Dura��o da vibra��o padr�o */
	//#if SAMSUNG_BAD_SOUND == "true"
//# 		public static final byte DEFAULT_VIBRATION_TIME = 80;
	//#else
	public static final short DEFAULT_VIBRATION_TIME = 200;
	//#endif
	
	//<editor-fold defaultstate="collapsed" desc="Textos do Jogo">

	public static final byte TEXT_BACK					= 0;
	public static final byte TEXT_NEW_GAME				= 1;
	public static final byte TEXT_EXIT					= 2;
	public static final byte TEXT_OPTIONS				= 3;
	public static final byte TEXT_CREDITS				= 4;
	public static final byte TEXT_CREDITS_TEXT			= 5;
	public static final byte TEXT_HELP					= 6;
	public static final byte TEXT_HELP_TEXT				= 7;
	public static final byte TEXT_TURN_SOUND_ON			= 8;
	public static final byte TEXT_TURN_SOUND_OFF		= 9;
	public static final byte TEXT_TURN_VIBRATION_ON		= 10;
	public static final byte TEXT_TURN_VIBRATION_OFF	= 11;
	public static final byte TEXT_CONTINUE				= 12;
	public static final byte TEXT_SPLASH_NANO			= 13;
	public static final byte TEXT_SPLASH_MTV			= 14;
	public static final byte TEXT_LOADING				= 15;
	public static final byte TEXT_DO_YOU_WANT_SOUND		= 16;
	public static final byte TEXT_YES					= 17;
	public static final byte TEXT_NO					= 18;
	public static final byte TEXT_EXIT_GAME				= 19;
	public static final byte TEXT_PRESS_ANY_KEY			= 20;
	public static final byte TEXT_RECORDS				= 21;
	public static final byte TEXT_LEVEL					= 22;
	public static final byte TEXT_GET_READY				= 23;
	public static final byte TEXT_ERASE_RECORDS			= 24;
	public static final byte TEXT_BACK_MENU				= 25;
	//#if ( SCREEN_SIZE == "SMALL" ) && ( WIDESCREEN == "false" )
//# 		public static final byte TEXT_BACK_MENU_2		= 26;
		//#if LOG == "true"
//# 			public static final byte TEXT_LOG			= 27;
		//#endif
	//#else
		//#if LOG == "true"
//#			public static final byte TEXT_LOG			= 26;
		//#endif
	//#endif
		
	/** N�mero total de textos do jogo */
	//#if ( SCREEN_SIZE == "SMALL" ) && ( WIDESCREEN == "false" )
		//#if LOG == "true"
//#			public static final byte TEXT_TOTAL			= 28;
		//#else
//# 			public static final byte TEXT_TOTAL			= 27;
		//#endif
	//#else
		//#if LOG == "true"
//#			public static final byte TEXT_TOTAL			= 27;
		//#else
		public static final byte TEXT_TOTAL			= 26;
		//#endif
	//#endif

	//</editor-fold>
}
