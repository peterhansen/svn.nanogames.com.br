/**
 * Spritebiarra.java
 * �2008 Nano Games.
 *
 * Created on 09/04/2008 15:11:08.
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.Point;

/**
 * @author Daniel L. Alves
 */

public final class Spritebiarra extends Sprite
{
	/** Array de offsets por sequ�ncias */
	private Point[][] offsetsBySequences;
	
	/** Deriva��o de construtor */
	public Spritebiarra( String descriptorFilename, String filenamePrefix ) throws Exception
	{
		super( descriptorFilename, filenamePrefix );
		
		offsetsBySequences = new Point[ sequences.length ][];
		
		 for( int i = 0 ; i < sequences.length ; ++i )
			offsetsBySequences[i] = new Point[ sequences[i].length ];
	}
	
	/** Deriva��o de construtor de c�pia */
	public Spritebiarra( Spritebiarra s )
	{
		super( s );
		
		// Copia os offsets
		offsetsBySequences = new Point[ s.offsetsBySequences.length ][];
		for( int i = 0 ; i < offsetsBySequences.length ; ++i )
		{
			offsetsBySequences[i] = new Point[ s.offsetsBySequences[i].length ];
			
			for( int j = 0 ; j < offsetsBySequences[i].length ; ++j )
				offsetsBySequences[i][j] = new Point( s.offsetsBySequences[i][j] );
		}
	}

	public void setSequence( int sequence )
	{
		// Pode ainda n�o ter sido alocado
		if( offsetsBySequences != null )
		{
			for( int i = 0 ; i < offsetsBySequences[ sequence ].length ; ++i )
			{
				// Se possui opera��o de espelhamento horizontal, reseta os offsets
				if( ( transform & TRANS_MASK_MIRROR ) != 0 )
					offsets[ sequences[ sequence ][i] ] = new Point( size.x - offsetsBySequences[ sequence ][i].x - frames[ sequences[ sequence ][i] ].getWidth(), offsetsBySequences[ sequence ][i].y );
				else
					offsets[ sequences[ sequence ][i] ] = offsetsBySequences[ sequence ][i];
			}
			
			
		}
		super.setSequence( sequence );
	}
	
	/**
	* Determina o offset de um frame em uma dada sequ�ncia.
	* 
	* @param sequenceIndex �ndice da sequ�ncia
	* @param frameIndex �ndice do frame na sequ�ncia
	*/
	public final void setOffset( int sequence, int frame, Point offset )
	{
		offsetsBySequences[sequence][frame] = offset;
	}

	/**
	 * Obt�m o offset de um frame em uma dada sequ�ncia.
	 * 
	 * @param sequenceIndex �ndice da sequ�ncia
	 * @param frameIndex �ndice do frame na sequ�ncia
	 * 
	 * @return Offset do frame na seuq�ncia 
	 */
	public final Point getOffset( int sequence, int frame )
	{
		return offsetsBySequences[sequence][frame];
	}
}
