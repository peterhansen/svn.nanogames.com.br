/**
 * WeightListener.java
 * �2008 Nano Games.
 *
 * Created on 01/04/2008 11:11:45.
 */

package game;

import br.com.nanogames.components.util.Point;

/**
 * @author Daniel L. Alves
 */

public interface WeightListener
{
	/** Informa a nova posi��o de uma das anilhas do peso
	 * @param movingSide Anilha que est� se movimentando
	 * @param newCenterTop Nova coordenada superior-central da anilha
	 */
	public void onWeightPositionChanged( byte movingSide, Point newCenterTop );
}
