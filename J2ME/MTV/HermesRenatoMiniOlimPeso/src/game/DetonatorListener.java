/**
 * DetonatorListener.java
 * �2008 Nano Games.
 *
 * Created on 14/04/2008 10:07:45.
 */

package game;

/**
 *
 * @author Daniel L. Alves
 */

public interface DetonatorListener
{
	/** Informa para qual estado o personagem est� indo */
	public void onDetonatorStateChanged( byte state );
	
	/** Informa quando um dos bra�os do personagem muda de estado */
	public void onDetonatorArmStateChanged( byte detonatorState, byte side, int dy );
}
