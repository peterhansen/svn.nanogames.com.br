/**
 * DetonatorHands.java
 * �2008 Nano Games.
 *
 * Created on 14/04/2008 10:10:10.
 */

package game;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.Spritebiarra;
import javax.microedition.lcdui.Image;

/**
 * @author Daniel L. Alves
 */

public final class DetonatorHands extends DrawableGroup implements DetonatorListener, Constants
{
	/** Refer�ncias para os drawables da cole��o */
	private final Spritebiarra[] hands = new Spritebiarra[BOTH_SIDES];
	
	/** Posi��o original das m�os no estado atual */
	private final short[] handOriginalY = { 0, 0 };
	
	/** Objeto que ir� receber as informa��es de mudan�a de estado */
	private final DetonatorHandsListener listener;
	
	/** Construtor */
	public DetonatorHands( DetonatorHandsListener listener ) throws Exception
	{
		super( 2 );
		
		// Armazena uma refer�ncia para o objeto que ir� receber as informa��es de mudan�a de estado
		this.listener = listener;
		
		// Cria os objetos do grupo
		final String path = "/Detonator/hands";
		hands[ITEM_LEFT] = new Spritebiarra( path + ".bin", path );
		insertDrawable( hands[ITEM_LEFT] );
		setOffsets();
		
		hands[ITEM_RIGHT] = new Spritebiarra( hands[ITEM_LEFT] );
		insertDrawable( hands[ITEM_RIGHT] );
		hands[ITEM_RIGHT].mirror( TRANS_MIRROR_H );
		
		// Determina o tamanho do grupo
		setSize( hands[ITEM_LEFT].getSize() );
	}
	
	/** Determina os offsets das imagens das m�os */
	private void setOffsets()
	{
		//#if SCREEN_SIZE == "SMALL"
//# 			hands[ITEM_LEFT].setOffset(  0, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 0 ) ) );
//# 			hands[ITEM_LEFT].setOffset(  1, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 1 ) ) );
//# 			hands[ITEM_LEFT].setOffset(  2, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 2 ) ) );
//# 			hands[ITEM_LEFT].setOffset(  3, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 2 ) ) );
//# 			hands[ITEM_LEFT].setOffset(  4, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -5 ) );
//# 			hands[ITEM_LEFT].setOffset(  5, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -10 ) );
//# 			hands[ITEM_LEFT].setOffset(  6, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -13 ) );
//# 			hands[ITEM_LEFT].setOffset(  7, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -16 ) );
//# 			hands[ITEM_LEFT].setOffset(  8, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -19 ) );
//# 			hands[ITEM_LEFT].setOffset(  9, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 3 ) ) );
//# 			hands[ITEM_LEFT].setOffset( 10, 0, hands[ITEM_LEFT].getFrameOffset( 3 ).add( 0, 6 ) );
		//#else
			hands[ITEM_LEFT].setOffset(  0, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 0 ) ) );
			hands[ITEM_LEFT].setOffset(  1, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 1 ) ) );
			hands[ITEM_LEFT].setOffset(  2, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 2 ) ) );
			hands[ITEM_LEFT].setOffset(  3, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0,  -2 ) );
			hands[ITEM_LEFT].setOffset(  4, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -11 ) );
			hands[ITEM_LEFT].setOffset(  5, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -19 ) );
			hands[ITEM_LEFT].setOffset(  6, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -24 ) );
			hands[ITEM_LEFT].setOffset(  7, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -28 ) );
			hands[ITEM_LEFT].setOffset(  8, 0, hands[ITEM_LEFT].getFrameOffset( 2 ).add( 0, -32 ) );
			hands[ITEM_LEFT].setOffset(  9, 0, new Point( hands[ITEM_LEFT].getFrameOffset( 3 ) ) );
			hands[ITEM_LEFT].setOffset( 10, 0, hands[ITEM_LEFT].getFrameOffset( 3 ).add( 0, 11 ) );
		//#endif
	}
	
	/** Modifica o estado da m�o de acordo com o novo estado do personagem */
	//#if defined( PLEASE_HELP_ME )
//# 	public void onDetonatorStateChanged( byte state )
	//#else
		public synchronized void onDetonatorStateChanged( byte state )
	//#endif
	{
		// Coloca as m�os em suas posi��es originais
		hands[ ITEM_LEFT ].setPosition( hands[ ITEM_LEFT ].getPosX(), handOriginalY[ ITEM_LEFT ] );
		hands[ ITEM_RIGHT ].setPosition( hands[ ITEM_RIGHT ].getPosX(), handOriginalY[ ITEM_RIGHT ] );
		
		// Nos 2 primeiros estados do personagem, n�o possu�mos um estado de m�o correspondente. Por isso
		// utilizamos esta diferen�a para sabermos que estado devemos utilizar
		final int DIFF_BETWEEN_DETONATOR_N_HAND_STATES = 2;
		
		switch( state )
		{
			case Detonator.DETONATOR_STATE_STANDING:
			case Detonator.DETONATOR_STATE_STANDING_TO_1ST_STEP:
				// Nos 2 primeiros estados do personagem, o peso deve ser desenhado como se estivesse no estado
				// DETONATOR_STATE_1ST_STEP
				hands[ ITEM_LEFT ].setSequence( 0 );
				hands[ ITEM_RIGHT ].setSequence( 0 );
				
				// Nesses estados as m�os est�o nas imagens do corpo
				setVisible( false );
				break;

			/* � mais otimizado utilizar o case default do que todos os outros cases
			case Detonator.DETONATOR_STATE_1ST_STEP:
			case Detonator.DETONATOR_STATE_2ND_STEP:
			case Detonator.DETONATOR_STATE_2ND_TO_3RD_STEP:
			case Detonator.DETONATOR_STATE_3RD_STEP_0:
			case Detonator.DETONATOR_STATE_3RD_STEP_1:
			case Detonator.DETONATOR_STATE_3RD_STEP_2:
			case Detonator.DETONATOR_STATE_3RD_STEP_3:
			case Detonator.DETONATOR_STATE_3RD_STEP_4:
			case Detonator.DETONATOR_STATE_3RD_STEP_5:
			case Detonator.DETONATOR_STATE_FINISHED_0:
			case Detonator.DETONATOR_STATE_FINISHED_1: */
			default:
				// Torna o grupo vis�vel
				setVisible( true );
		
				hands[ ITEM_LEFT ].setSequence( state - DIFF_BETWEEN_DETONATOR_N_HAND_STATES );
				hands[ ITEM_RIGHT ].setSequence( state - DIFF_BETWEEN_DETONATOR_N_HAND_STATES );
				break;
		}
		
		// Guarda as posi��es originais das m�os
		handOriginalY[ ITEM_LEFT ] = ( short )hands[ ITEM_LEFT ].getPosY();
		handOriginalY[ ITEM_RIGHT ] = ( short )hands[ ITEM_RIGHT ].getPosY();
		
		// Passa a informa��o da mudan�a de estado
		proxyState( state );
	}

	/** Recebido quando um dos bra�os do personagem muda de estado */
	//#if defined ( PLEASE_HELP_ME )
//# 	public void onDetonatorArmStateChanged( byte detonatorState, byte side, int dy )
	//#else
		public synchronized void onDetonatorArmStateChanged( byte detonatorState, byte side, int dy )
	//#endif
	{
		// Desloca a m�o de acordo com o deslocamento do bra�o
		hands[ side ].setPosition( hands[ side ].getPosX(), handOriginalY[ side ] + dy );
		
		// Passa a informa��o da mudan�a de estado adiante
		proxyState( detonatorState );
	}
	
	/** Passa a informa��o da mudan�a de estado adiante*/
	private void proxyState( byte state )
	{
		if( listener != null )
		{
			final Point handOffset = hands[ ITEM_LEFT ].getOffset( hands[ ITEM_LEFT ].getSequence(), 0 );
			final Image frame = hands[ ITEM_LEFT ].getFrameImage( hands[ ITEM_LEFT ].getSequence(), 0 );
			final Point aux = new Point( frame.getWidth(), frame.getHeight() );
			
			final Point leftHandPos = position.add( handOffset.x, handOffset.y + ( hands[ ITEM_LEFT ].getPosY() - handOriginalY[ ITEM_LEFT ] ));
			final Point rightHandPos = position.add( size.x - frame.getWidth() - handOffset.x, handOffset.y + ( hands[ ITEM_RIGHT ].getPosY() - handOriginalY[ ITEM_RIGHT ] ));

			listener.onDetonatorHandsScreenPosChanged( ITEM_LEFT, state, leftHandPos, rightHandPos, aux );
			listener.onDetonatorHandsScreenPosChanged( ITEM_RIGHT, state, leftHandPos, rightHandPos, aux );
		}
	}
}
