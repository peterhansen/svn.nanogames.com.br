/**
 * BirdieStateListener.java
 * �2008 Nano Games.
 *
 * Created on 09/05/2008 16:18:52.
 */

package game;

/**
 * @author Daniel L. Alves
 */
public interface BirdieStateListener
{
	/** Indica que mudou de estado */
	public void onBirdieStateChanged( byte state, byte side );
}
