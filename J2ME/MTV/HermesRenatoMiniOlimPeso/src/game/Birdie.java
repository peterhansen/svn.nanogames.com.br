/**
 * Birdie.java
 * �2008 Nano Games.
 *
 * Created on 27/03/2008 18:53:54.
 */

package game;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;

/**
 * @author Daniel L. Alves
 * OBS: Eu sei, os coment�rios ao longo dessa classe est�o bem gays (ex. "Anima o passarinho")
 */

public final class Birdie extends UpdatableGroup implements WeightListener, SpriteListener, Constants
{
	/** Permite um observador para as mudan�as de estado */
	private final BirdieListener[] listener = new BirdieListener[2];
	
	/** �ndices dos observadores do passarinho (Sujeira!!!) */
	public static final byte LISTENER_WEIGHT = 0;
	public static final byte LISTENER_DETONATOR = 1;
	
	/** Controlador do movimento do passarinho */
	private final BezierCurve flightBezier = new BezierCurve();
	
	/** Dist�ncia do passarinho para a barra de status */
	private final byte BIRDIE_DIST_FROM_STATUS_BAR = 8;
	
	/** Ponto de destino da anima��o atual */
	private Point dest = new Point();
	
	/** Indica quanto do tempo total da anima��o atual j� passou */
	private int animTime;
	
	/** Auxiliares de anima��o */
	private byte animCounter, animCounterLimit;
	
	/** Dura��es das anima��es que empregam movimenta��o(-1 => este slot n�o � levado em conta, mas � mantido
	* no array por motivo de organiza��o de c�digo, j� que os �ndices s�o compartilhados entre algumas
	* vari�veis)
	*/
	private final short[] animDur = new short[]{ 500, 1000, 1000, -1, -1, 1000 };
	
	/** Ponto centro-topo das anilhas da esquerda */
	private Point leftWeightCenterTop;
	
	/** Ponto centro-topo das anilhas da direita */
	private Point rightWeightCenterTop;
	
	/** Partes do passarinho */
	private final Sprite body, head;
	
	/** Dificuldade que o passarinho deve empregar */
	private byte difficulty;
	
	//<editor-fold defaultstate="collapsed" desc="Dificuldades que o passarinho pode empregar">
	
	public static final byte BIRDIE_DIFFICULTY_EASY		= 0;
	public static final byte BIRDIE_DIFFICULTY_MEDIUM	= 1;
	public static final byte BIRDIE_DIFFICULTY_HARD		= 2;
	
	public static final byte N_BIRDIE_DIFFICULTIES		= 3;
	
	 //</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos sprites no grupo">
	private final byte BIRDIE_BODY_INDEX	= 0;
	private final byte BIRDIE_HEAD_INDEX	= 1;
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices das sequ�ncias de anima��o">
	private final byte BIRDIE_BODY_SEQ_FLIGHT		= 0;
	private final byte BIRDIE_BODY_SEQ_JUMPING_SLOW	= 1;
	private final byte BIRDIE_BODY_SEQ_JUMPING_MED	= 2;
	private final byte BIRDIE_BODY_SEQ_JUMPING_FAST	= 3;
	private final byte BIRDIE_BODY_SEQ_STILL		= 4;
	private final byte BIRDIE_BODY_SEQ_LANDING		= 5;

	private final byte BIRDIE_HEAD_SEQ_FLIGHT_0			= 0;
	private final byte BIRDIE_HEAD_SEQ_FLIGHT_1			= 1;
	private final byte BIRDIE_HEAD_SEQ_FLIGHT_2			= 2;
	private final byte BIRDIE_HEAD_SEQ_FLIGHT_3			= 3;
	private final byte BIRDIE_HEAD_SEQ_JUMPING_SLOW		= 4;
	private final byte BIRDIE_HEAD_SEQ_JUMPING_MED		= 5;
	private final byte BIRDIE_HEAD_SEQ_JUMPING_FAST		= 6;
	private final byte BIRDIE_HEAD_SEQ_STILL			= 7;
	private final byte BIRDIE_HEAD_SEQ_LANDING			= 8;
	private final byte BIRDIE_HEAD_SEQ_LOOKING_LEFT		= 9;
	private final byte BIRDIE_HEAD_SEQ_LOOKING_RIGHT	= 10;
	
	private final int BIRDIE_N_HEAD_FLIGHT_SEQS = 4;
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Estados do passarinho">
	
	public static final byte BIRDIE_STATE_NONE			= -1;
	public static final byte BIRDIE_STATE_ARRIVING		=  0;
	public static final byte BIRDIE_STATE_TARGETING		=  1;
	public static final byte BIRDIE_STATE_LANDING		=  2;
	public static final byte BIRDIE_STATE_LANDED		=  3;
	public static final byte BIRDIE_STATE_JUMPING		=  4;
	public static final byte BIRDIE_STATE_LEAVING		=  5;
	
	//</editor-fold>
	
	/** Estado atual do passarinho */
	private byte currState = BIRDIE_STATE_NONE;
	
	/** Quanto da imagem do passarinho fica por cima das anilhas do peso */
	private final byte BIRDIE_LANDING_OFFSET = 2;
	
	/** Objeto que recebe informa��es de mudan�as de estado */
	final BirdieStateListener stateListener;
	
	/** Construtor */
	public Birdie( BirdieStateListener stateListener ) throws Exception
	{
		super( 2 );
		this.stateListener = stateListener;
		
		// Cria o corpo do passarinho
		body = new Sprite( "/Birdie/birdieBody.bin", "/Birdie/birdieBody" );
		insertDrawable( body );
		
		// Cria a cabe�a do passarinho
		head = new Sprite( "/Birdie/birdieHead.bin", "/Birdie/birdieHead" );
		insertDrawable( head );
		
		// Coloca o grupo como observador das mudan�as dos sprites
		body.setListener( this, BIRDIE_BODY_INDEX );
		head.setListener( this, BIRDIE_HEAD_INDEX );
		
		// Determina o tamanho do grupo. "body" e "head" possuem o mesmo tamanho/�rea de clip. Este valor
		// � igual ao tamanho original do frame antes de passar pelo SpriteScissors. Assim j� teremos a anima��o
		// "montada" apesar de termos duas imagens separadas
		setSize( body.getSize() );
		
		// Define o ponto �ncora do sprite
		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		
		// Por enquanto, fica invis�vel
		setVisible( false );
	}
	
	/** Armazena uma refer�ncia para o observador */
	public final void setListener( byte listenerIndex, BirdieListener listener )
	{
		this.listener[ listenerIndex ] = listener;
	}

	/** Retorna o estado atual do passarinho */
	public final byte getState()
	{
		return currState;
	}
	
	/** Determina o estado atual do passarinho */
	private final void setState( byte state )
	{
		final byte prevState = currState;
		
		animTime = 0;
		currState = state;
		head.setVisible( true );
		
		switch( currState )
		{
			case BIRDIE_STATE_NONE:
				setVisible( false );
				return;
			
			case BIRDIE_STATE_LEAVING:
				listener[ LISTENER_WEIGHT ].setBirdieLanded( ITEM_NONE );
				listener[ LISTENER_DETONATOR ].setBirdieLanded( ITEM_NONE );

				if( prevState == BIRDIE_STATE_JUMPING )
				{
					final int aux = listener[ LISTENER_WEIGHT ].getBirdieJumpOffset();
					if( aux > 0 )
						listener[ LISTENER_WEIGHT ].onBirdieJump( dest.x == leftWeightCenterTop.x ? ITEM_LEFT : ITEM_RIGHT, -aux );
				}
					
			case BIRDIE_STATE_TARGETING:				
			case BIRDIE_STATE_ARRIVING:
				head.setSequence( NanoMath.randInt( BIRDIE_N_HEAD_FLIGHT_SEQS ) );
				body.setSequence( BIRDIE_BODY_SEQ_FLIGHT );
				break;
				
			case BIRDIE_STATE_LANDING:
				head.setSequence( BIRDIE_HEAD_SEQ_LANDING );
				body.setSequence( BIRDIE_BODY_SEQ_LANDING );
				break;
				
			case BIRDIE_STATE_LANDED:
				head.setSequence( BIRDIE_HEAD_SEQ_STILL );
				body.setSequence( BIRDIE_BODY_SEQ_STILL );
				
				final byte landingSide = dest.x == leftWeightCenterTop.x ? ITEM_LEFT : ITEM_RIGHT;
				listener[ LISTENER_WEIGHT ].setBirdieLanded( landingSide );
				listener[ LISTENER_DETONATOR ].setBirdieLanded( landingSide );
				break;
				
			case BIRDIE_STATE_JUMPING:
				head.setSequence( BIRDIE_HEAD_SEQ_JUMPING_SLOW + difficulty );
				body.setSequence( BIRDIE_BODY_SEQ_JUMPING_SLOW + difficulty );
				break;
		}
		if( stateListener != null )
			stateListener.onBirdieStateChanged( state, getRefPixelX() == leftWeightCenterTop.x ? ITEM_LEFT : ITEM_RIGHT );
	}
	
	/** Fornece as informa��es iniciais sobre o peso, ou seja, onde o passarinho ir� pousar
	 * @param leftWeightCenterTop Ponto centro-topo das anilhas da esquerda
	 * @param rightWeightCenterTop Ponto centro-topo das anilhas da direita
	 * @see onWeightPositionChanged
	 */
	public final void setWeightAttributes( Point leftWeightCenterTop, Point rightWeightCenterTop )
	{
		this.leftWeightCenterTop = leftWeightCenterTop;
		this.rightWeightCenterTop = rightWeightCenterTop;
	}
	
	/** Determina a dificuldade do passarinho
	 * @param difficulty Dificuldade que o passarinho deve empregar. Pode recerber os valores:
	 * <br>BIRDIE_DIFFICULTY_EASY</br>
	 * <br>BIRDIE_DIFFICULTY_MEDIUM</br>
	 * <br>BIRDIE_DIFFICULTY_HARD</br>
	 */
	public final void setDifficulty( byte difficultyLevel )
	{
		difficulty = difficultyLevel;
	}
	
	/** Obt�m a dificuldade atual do passarinho */
	public final byte getDifficulty()
	{
		return difficulty;
	}
	
	/** Faz com que o passarinho entre na tela */
	public final void enterScreen()
	{
		if( currState != BIRDIE_STATE_NONE )
			return;

		// Zera algumas vari�veis de controle
		animCounter = 0;
		animCounterLimit = 0;
						
		// Define o estado atual
		setState( BIRDIE_STATE_ARRIVING );
		
		// Coloca o passarinho em uma posi��o aleat�ria entre os centros das anilhas
		setRefPixelPosition( leftWeightCenterTop.x + NanoMath.randInt( rightWeightCenterTop.x - leftWeightCenterTop.x + 1 ), -( size.y >> 1 ) );
		
		// Torna o passarinho vis�vel
		setVisible( true );
		
		// Define o ponto final da anima��o
		dest.set( getRefPixelX(), StatusBar.STATUS_BAR_HEIGHT + BIRDIE_DIST_FROM_STATUS_BAR + ( size.y >> 1 ) );
		
		// Define o trajeto da anima��o
		Point currPos = getRefPixelPosition();
		final int stepInY = ( dest.y - currPos.y ) / 3;
		final short ANYTHING = 9573;
		final int controlPointX = 20 * (( NanoMath.randInt( ANYTHING ) & 0x1 ) == 0 ? -1 : 1 );
		flightBezier.setValues( currPos, currPos.add( -controlPointX, stepInY ), dest.add( controlPointX, -stepInY ), dest );
	}
	
	/** Faz o passarinho sair da tela */
	public final void goAway()
	{
		if( ( currState == BIRDIE_STATE_NONE ) || ( currState == BIRDIE_STATE_LEAVING ) )
			return;

		// Define o estado atual
		setState( BIRDIE_STATE_LEAVING );
		
		// Define o ponto final da anima��o
		dest.set( getRefPixelX(), -( size.y >> 1 ) );
		
		// Define o trajeto da anima��o
		Point currPos = getRefPixelPosition();
		final int stepInY = currPos.y / 3; // N�o precisa fazer "( currPos.y - dest.y ) / 3" pois dest.y ser� 0
		final short ANYTHING = 973;
		final int controlPointX = 20 * (( NanoMath.randInt( ANYTHING ) & 0x1 ) == 0 ? -1 : 1 );
		flightBezier.setValues( currPos, currPos.add( -controlPointX, -stepInY ), dest.add( controlPointX, stepInY ), dest );
	}
	
	/** Garante que o passarinho est� fora da tela */
	public final void reset()
	{
		final int finalY = -( size.y >> 1 );
		if( ( currState != BIRDIE_STATE_NONE ) || ( getRefPixelY() != finalY ) )
		{
			setState( BIRDIE_STATE_NONE );
			setRefPixelPosition( getRefPixelX(), finalY );
		}
	}
	
	public final void update( int delta )
	{	
		// Anima o v�o do passarinho
		switch( currState )
		{
			case BIRDIE_STATE_NONE:
				return;
			
			// Faz o passarinho entrar voando na tela
			case BIRDIE_STATE_ARRIVING:
				{
					animTime += delta;
					final Point newPos = new Point();
					flightBezier.getPointAt( newPos, ( animTime * 100 ) / animDur[ currState ] );
					setRefPixelPosition( newPos );
					
					if( newPos.equals( dest ) )
					{
						final int controlPointY = 50;
						final int distFromLeftWeight = Math.abs( newPos.x - leftWeightCenterTop.x );
						final int distFromRightWeight = Math.abs( newPos.x - rightWeightCenterTop.x );
						
						if( distFromLeftWeight > distFromRightWeight )
						{
							dest = new Point( leftWeightCenterTop.x, StatusBar.STATUS_BAR_HEIGHT + BIRDIE_DIST_FROM_STATUS_BAR + ( size.y >> 1 ));
							final int stepInX = distFromLeftWeight / 3;
							flightBezier.setValues( newPos, newPos.add( -stepInX, controlPointY ), dest.add( stepInX, -controlPointY ), dest );
						}
						else
						{
							dest = new Point( rightWeightCenterTop.x, StatusBar.STATUS_BAR_HEIGHT + BIRDIE_DIST_FROM_STATUS_BAR + ( size.y >> 1 ));
							final int stepInX = distFromRightWeight / 3;
							flightBezier.setValues( newPos, newPos.add( stepInX, controlPointY ), dest.add( -stepInX, -controlPointY ), dest );
						}
						animCounter = 0;
						animCounterLimit = ( byte )Math.max( 2, NanoMath.randInt( 6 ) );
		
						setState( BIRDIE_STATE_TARGETING );
					}
				}
				break;
			
			// Faz o passarinho ficar sobrevoando o personagem antes de pousar
			case BIRDIE_STATE_TARGETING:
				{
					animTime += delta;
					final Point newPos = new Point();
					flightBezier.getPointAt( newPos, ( animTime * 100 ) / animDur[ currState ] );
					setRefPixelPosition( newPos );
					
					if( animCounter < animCounterLimit )
					{
						if( newPos.equals( dest ) )
						{
							final int controlPointY = 50;
							final int stepInX = ( rightWeightCenterTop.x - leftWeightCenterTop.x )/ 3;
							
							if( newPos.x == leftWeightCenterTop.x )
							{
								dest = new Point( rightWeightCenterTop.x, StatusBar.STATUS_BAR_HEIGHT + BIRDIE_DIST_FROM_STATUS_BAR + ( size.y >> 1 ) );
								flightBezier.setValues( newPos, newPos.add( stepInX, controlPointY ), dest.add( -stepInX, -controlPointY ), dest );
							}
							else
							{
								dest = new Point( leftWeightCenterTop.x, StatusBar.STATUS_BAR_HEIGHT + BIRDIE_DIST_FROM_STATUS_BAR + ( size.y >> 1 ) );
								flightBezier.setValues( newPos, newPos.add( -stepInX, controlPointY ), dest.add( stepInX, -controlPointY ), dest );
							}
							animTime = 0;
							++animCounter;
						}
					}
					else
					{
						final int distFromLeftWeight = Math.abs( newPos.x - leftWeightCenterTop.x );
						final int distFromRightWeight = Math.abs( newPos.x - rightWeightCenterTop.x );
						
						if( distFromLeftWeight > distFromRightWeight )
							dest = new Point( leftWeightCenterTop.x, leftWeightCenterTop.y - ( size.y >> 1 ) + BIRDIE_LANDING_OFFSET );
						else
							dest = new Point( rightWeightCenterTop.x, rightWeightCenterTop.y - ( size.y >> 1 ) + BIRDIE_LANDING_OFFSET  );
						
						flightBezier.setValues( newPos, dest.sub( 0, dest.y ), dest.sub( 0, dest.y ), dest );

						setState( BIRDIE_STATE_LANDING );
					}
				}
				break;
				
			case BIRDIE_STATE_LANDED:
				break;
				
			case BIRDIE_STATE_JUMPING:
				if( ( body.getFrameSequenceIndex() == 0 ) && ( animCounter == 0 ) )
				{
					animCounter = 1;
					
					final byte landingSide = dest.x == leftWeightCenterTop.x ? ITEM_LEFT : ITEM_RIGHT;
					listener[ LISTENER_WEIGHT ].onBirdieJump( landingSide, BIRDIE_LANDING_OFFSET + 1 );
					listener[ LISTENER_DETONATOR ].onBirdieJump( landingSide, BIRDIE_LANDING_OFFSET + 1 );
				}
				else if( ( body.getFrameSequenceIndex() == 1 ) && ( animCounter == 1 ) )
				{
					animCounter = 0;
					
					final byte landingSide = dest.x == leftWeightCenterTop.x ? ITEM_LEFT : ITEM_RIGHT;
					listener[ LISTENER_WEIGHT ].onBirdieJump( landingSide, -listener[ LISTENER_WEIGHT ].getBirdieJumpOffset() );
				}
				else if( ( body.getFrameSequenceIndex() == 2 ) && ( animCounter == 2 ) )
				{
					animCounter = 0;
				}
				break;
		
			case BIRDIE_STATE_LANDING:
				{
					animTime += delta;
					final Point newPos = new Point();
					final int percent = ( animTime * 100 ) / animDur[ currState ];
					flightBezier.getPointAt( newPos, percent );
					
					// Corre��o de bug: caso o personagem levante o peso e o passarinho estiver pousando
					if(( newPos.y >= dest.y ) && ( percent > 75 ))
					{
						setRefPixelPosition( dest );
						setState( BIRDIE_STATE_LANDED );
					}
					else
					{
						// Corre��o de bug: caso o personagem abaixe o peso e o passarinho estiver pousando
						if( percent >= 100 )
							flightBezier.destiny.set( dest );

						setRefPixelPosition( newPos );
					}
				}
				break;
				
			case BIRDIE_STATE_LEAVING:
				{
					animTime += delta;
					final Point newPos = new Point();
					flightBezier.getPointAt( newPos, ( animTime * 100 ) / animDur[ currState ] );
					setRefPixelPosition( newPos );
					
					if( newPos.equals( dest ) )
						setState( BIRDIE_STATE_NONE );
				}
				break;
		}
		// Anima o passarinho
		super.update( delta );
	}

	/** Se o peso que o personagem levanta mudar de posi��o, temos que mudar o ponto de pouso do passarinho */
	public void onWeightPositionChanged( byte movingSide, Point newCenterTop )
	{
		if( currState == BIRDIE_STATE_NONE )
			return;

		final byte currSide = dest.x == leftWeightCenterTop.x ? ITEM_LEFT : ITEM_RIGHT;
			
		if( movingSide == ITEM_LEFT )
			leftWeightCenterTop = newCenterTop;
		else
			rightWeightCenterTop = newCenterTop;
		
		switch( currState )
		{
			case BIRDIE_STATE_LANDING:
				// Define o novo ponto de destino
				if( currSide == ITEM_LEFT )
					dest.set( leftWeightCenterTop.x, leftWeightCenterTop.y - ( size.y >> 1 ) + BIRDIE_LANDING_OFFSET );
				else
					dest.set( rightWeightCenterTop.x, rightWeightCenterTop.y - ( size.y >> 1 ) + BIRDIE_LANDING_OFFSET );
					
				// Verifica se, com o novo destino, j� passou do ponto de pouso
				// OBS: Acho que n�o precisa deste if, pois j� trato os poss�veis bugs de pouso no m�todo update(),
				// case BIRDIE_STATE_LANDING. No entanto, como n�o est� ferindo ningu�m e o tempo urge, resolvi
				// deix�-lo aqui
				final int percent = ( animTime * 100 ) / animDur[ currState ];
				if(( getRefPixelY() >= dest.y ) && ( percent > 75 ))
				{
					setRefPixelPosition( dest );
					setState( BIRDIE_STATE_LANDED );
				}
				break;
				
			case BIRDIE_STATE_LANDED:
			case BIRDIE_STATE_JUMPING:
				if( currSide == ITEM_LEFT )
					setPosition( getPosX(), leftWeightCenterTop.y - size.y + BIRDIE_LANDING_OFFSET );
				else
					setPosition( getPosX(), rightWeightCenterTop.y - size.y + BIRDIE_LANDING_OFFSET );
				break;
		}
	}

	public void onSequenceEnded( int id, int sequence )
	{
		switch( id )
		{
			case BIRDIE_BODY_INDEX:
				switch( currState )
				{
					case BIRDIE_STATE_JUMPING:
						head.setVisible( true );
						body.setSequence( BIRDIE_BODY_SEQ_JUMPING_SLOW + difficulty );
						head.setSequence( BIRDIE_HEAD_SEQ_JUMPING_SLOW + difficulty );
						break;
				}
				break;
				
			case BIRDIE_HEAD_INDEX:
				switch( currState )
				{
					case BIRDIE_STATE_LANDED:
						if( sequence == BIRDIE_HEAD_SEQ_STILL )
						{
							head.setSequence( dest.x == leftWeightCenterTop.x ? BIRDIE_HEAD_SEQ_LOOKING_RIGHT : BIRDIE_HEAD_SEQ_LOOKING_LEFT );
						}
						else
						{
							animCounter = 2;
							setState( BIRDIE_STATE_JUMPING );
						}
						break;

					case BIRDIE_STATE_ARRIVING:
					case BIRDIE_STATE_TARGETING:
					case BIRDIE_STATE_LEAVING:
					case BIRDIE_STATE_LANDING:
						body.setSequence( body.getSequence() );
						
						// Garante que o passarinho n�o vai piscar v�rias vezes seguidas
						final byte lastSeq = head.getSequence();
						final byte nextSeq = ( byte )NanoMath.randInt( BIRDIE_N_HEAD_FLIGHT_SEQS );
						if( lastSeq != nextSeq )
							head.setSequence( nextSeq );
						else
							head.setSequence( ( nextSeq + 1 ) % BIRDIE_N_HEAD_FLIGHT_SEQS );
						break;
				}
				break;
		}
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
		switch( id )
		{
			case BIRDIE_BODY_INDEX:
				switch( currState )
				{
					case BIRDIE_STATE_JUMPING:
						if( frameSequenceIndex == 1 )
							head.setVisible( false );
						break;
				}
				break;
		}
	}
}
