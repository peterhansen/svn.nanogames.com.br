/**
 * Weight.java
 * �2008 Nano Games.
 *
 * Created on 26/03/2008 13:21:20.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class Weight extends UpdatableGroup implements BirdieListener, DetonatorHandsListener, Constants
{
	/** N�mero de tamanhos das anilhas */
	public static final byte N_WEIGHT_SIZES = 3;
	
	/** Largura da barra que liga os pesos */
	//#if SCREEN_SIZE == "SMALL"
//# 		public final byte BAR_WIDTH = 51;
	//#else
	public final byte BAR_WIDTH = 81;
	//#endif
	
	/** Altura da barra que liga os pesos */
	private final byte BAR_HEIGHT = 3;
	
	/** Estado atual do peso */
	private byte currState;
	
	/** Permite um observador para as mudan�as de atributos */
	private WeightListener listener;
	
	/** Deslocamento causado no peso por causa do pulo do passarinho */
	private int birdieJumpOffsetY;
	
	/** Indica se o passarinho est� pousado no peso */
	private byte birdieLandedOnMe = ITEM_NONE;
	
	/** Pontos de conex�o da barra com as m�os do personagem */
	private final Point[] connectionPoint = { new Point(), new Point(), new Point(), new Point(), new Point(), new Point() };
	
	//<editor-fold defaultstate="collapsed" desc="�ndices dos pontos de conex�o">
	
	private final byte CONNECTION_POINT_INNER_LEFT_HAND		= 0;
	private final byte CONNECTION_POINT_INNER_RIGHT_HAND	= 1;
	private final byte CONNECTION_POINT_LEFT_WEIGHT			= 2;
	private final byte CONNECTION_POINT_OUTER_LEFT_HAND		= 3;
	private final byte CONNECTION_POINT_RIGHT_WEIGHT		= 4;
	private final byte CONNECTION_POINT_OUTER_RIGHT_HAND	= 5;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Apar�ncias do peso">

	private final byte APPEARENE_GREEN	= 1;
	private final byte APPEARENE_BLUE	= 2;
	private final byte APPEARENE_PURPLE	= 3;
	private final byte APPEARENE_RED	= 4;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Estados do peso">

	private final byte PAINT_SINGLE	= 0;
	private final byte PAINT_PARTS	= 1;
	
	//</editor-fold>
	
	/** Construtor
	 * @param level Fase para a qual estamos criando o peso
	 */
	public Weight() throws Exception
	{
		// As imagens ser�o alocadas posteriormente atrav�s do m�todo setAppearence
		super( 2 );
		
		// Deixa este grupo do tamanho da tela
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
	
	/** Armazena uma refer�ncia para o observador */
	public final void setListener( WeightListener listener )
	{
		this.listener = listener;
	}
	
	/** Retorna o raio das anilhas do peso na fase atual */
	public final int getWeightRadius()
	{
		return getWeightDiameter() >> 1;
	}
	
	/** Retorna o di�metro das anilhas do peso na fase atual */
	public final int getWeightDiameter()
	{
		return drawables[0].getWidth();
	}
	
	/** Retorna a coordenada central superior de cada anilha do peso */
	public final void getCoordinates( Point leftWeightCenterTop, Point rightWeightCenterTop )
	{
		final int weightDiameter = getWeightRadius();
		leftWeightCenterTop.set( drawables[ ITEM_LEFT ].getPosX() + weightDiameter, drawables[ ITEM_LEFT ].getPosY() );
		rightWeightCenterTop.set( drawables[ ITEM_RIGHT ].getPosX() + weightDiameter, drawables[ ITEM_RIGHT ].getPosY() );
	}
		
	/** Modifica a apar�ncia do peso de acordo com a fase recebida como par�metro
	 * @param level Fase para a qual estamos criando o peso
	 */
	public final void setAppearence( int level ) throws Exception
	{
		// Se estamos em uma das primeiras fases, n precisamos mudar a cor do peso
		DrawableImage aux;
		final String imagePath = "/Weight/weight_";
		if( level < N_WEIGHT_SIZES )
		{
			// Aloca a imagem
			aux = new DrawableImage( imagePath + level + ".png" );
		}
		else
		{
			// Cria o modificador de paletas
			PaletteChanger weightChanger = new PaletteChanger( imagePath + ( level % N_WEIGHT_SIZES ) + ".png" );
			
			// Monta os mapas de paleta
			final byte N_COLORS_TO_CHANGE = 7;
			PaletteMap[] map = new PaletteMap[N_COLORS_TO_CHANGE];
			
			switch( level / N_WEIGHT_SIZES )
			{	
				case APPEARENE_GREEN:
					map[0] = new PaletteMap( 108, 106, 106, 122, 189, 108 );
					map[1] = new PaletteMap(  95,  94,  94,  95, 168,  79 );
					map[2] = new PaletteMap(  86,  85,  85,  93, 160,  79 );
					map[3] = new PaletteMap(  77,  76,  76,  79, 144,  65 );
					map[4] = new PaletteMap(  64,  64,  64,  64, 129,  50 );
					map[5] = new PaletteMap(  52,  51,  51,  46, 108,  33 );
					map[6] = new PaletteMap(  26,  26,  26,  36,  71,   8 );
					break;
					
				case APPEARENE_BLUE:
					map[0] = new PaletteMap( 108, 106, 106,  85, 136, 211 );
					map[1] = new PaletteMap(  95,  94,  94,  55, 111, 193 );
					map[2] = new PaletteMap(  86,  85,  85,  56, 107, 182 );
					map[3] = new PaletteMap(  77,  76,  76,  43,  93, 165 );
					map[4] = new PaletteMap(  64,  64,  64,  28,  78, 150 );
					map[5] = new PaletteMap(  52,  51,  51,  14,  60, 127 );
					map[6] = new PaletteMap(  26,  26,  26,   8,  50,  71 );
					break;
					
				case APPEARENE_PURPLE:
					map[0] = new PaletteMap( 108, 106, 106, 202, 106, 212 );
					map[1] = new PaletteMap(  95,  94,  94, 190,  71, 202 );
					map[2] = new PaletteMap(  86,  85,  85, 170,  52, 185 );
					map[3] = new PaletteMap(  77,  76,  76, 150,  45, 162 );
					map[4] = new PaletteMap(  64,  64,  64, 129,  39, 139 );
					map[5] = new PaletteMap(  52,  51,  51, 103,  31, 111 );
					map[6] = new PaletteMap(  26,  26,  26,  46,  17,  62 );
					break;
					
				case APPEARENE_RED:
					map[0] = new PaletteMap( 108, 106, 106, 230, 73, 52 );
					map[1] = new PaletteMap(  95,  94,  94, 222, 52, 38 );
					map[2] = new PaletteMap(  86,  85,  85, 209, 44, 35 );
					map[3] = new PaletteMap(  77,  76,  76, 196, 47, 36 );
					map[4] = new PaletteMap(  64,  64,  64, 181, 46, 33 );
					map[5] = new PaletteMap(  52,  51,  51, 160, 21, 49 );
					map[6] = new PaletteMap(  26,  26,  26, 143, 32, 31 );
					break;
			}
			
			// Aloca a imagem
			aux = new DrawableImage( weightChanger.createImage( map ) );
		}

		// Insere as imagens no grupo
		setDrawable( aux, ITEM_LEFT );
		setDrawable( new DrawableImage( aux ), ITEM_RIGHT );
	}

	protected final void paint( Graphics g )
	{
		// Desenha as anilhas
		super.paint( g );

		if( currState == PAINT_SINGLE )
		{
			// Desenha uma haste inteira
			paintBar( g, connectionPoint[CONNECTION_POINT_LEFT_WEIGHT].x, connectionPoint[CONNECTION_POINT_LEFT_WEIGHT].y, connectionPoint[CONNECTION_POINT_RIGHT_WEIGHT].x, connectionPoint[CONNECTION_POINT_RIGHT_WEIGHT].y );
		}
		else
		{
			// Desenha a haste que vai da anilha esquerda para a m�o esquerda
			paintBar( g, connectionPoint[CONNECTION_POINT_LEFT_WEIGHT].x, connectionPoint[CONNECTION_POINT_LEFT_WEIGHT].y, connectionPoint[CONNECTION_POINT_OUTER_LEFT_HAND].x, connectionPoint[CONNECTION_POINT_OUTER_LEFT_HAND].y );

			// Desenha a haste que fica entre as m�os
			paintBar( g, connectionPoint[CONNECTION_POINT_INNER_LEFT_HAND].x, connectionPoint[CONNECTION_POINT_INNER_LEFT_HAND].y, connectionPoint[CONNECTION_POINT_INNER_RIGHT_HAND].x, connectionPoint[CONNECTION_POINT_INNER_RIGHT_HAND].y );

			// Desenha a haste que vai da m�o direita para a anilha direita
			paintBar( g, connectionPoint[CONNECTION_POINT_OUTER_RIGHT_HAND].x, connectionPoint[CONNECTION_POINT_OUTER_RIGHT_HAND].y, connectionPoint[CONNECTION_POINT_RIGHT_WEIGHT].x, connectionPoint[CONNECTION_POINT_RIGHT_WEIGHT].y );
		}
	}
	
	private final void paintBar( Graphics g, int x1, int y1, int x2, int y2 )
	{
		g.setColor( 209, 208, 217 );
		g.drawLine( x1, y1, x2, y2 );

		g.setColor( 125, 133, 130 );
		g.drawLine( x1, y1 + 1, x2, y2 + 1 );

		g.setColor( 78, 69, 52 );
		g.drawLine( x1, y1 + 2, x2, y2 + 2 );
	}
	
	/** Movimenta o peso para dar impress�o que o passarinho est� pesando ao pular */
	public final void onBirdieJump( byte movingSide, int dy )
	{
		// Economiza processamento
		if( dy == 0 )
			return;

		// Armazena o deslocamento causado pelo pulo
		birdieJumpOffsetY += dy;
		
		// Movimenta a anilha na qual o passarinho est� pousado
		if( movingSide == ITEM_LEFT )
		{
			drawables[ ITEM_LEFT ].move( 0, dy );
			connectionPoint[CONNECTION_POINT_LEFT_WEIGHT].y += dy;
		}
		else
		{
			drawables[ ITEM_RIGHT ].move( 0, dy );
			connectionPoint[CONNECTION_POINT_RIGHT_WEIGHT].y += dy;
		}
	}

	public void onDetonatorHandsScreenPosChanged( byte movingSide, byte detonatorState, Point leftHandPos, Point rightHandPos, Point handImageSize )
	{
		// Evita que uma anilha seja puxada para cima sem ter sido empurrada primeiro para baixo. Isto se torna
		// necess�rio porque, ao mudarmos o estado da barra, reposicionamos as anilhas sem levar em conta o
		// deslocamento aplicado pelo passarinho. Logo, haveria um bug se n�o execut�ssemos este if.
		int birdieJumpOffsetAux = 0;
		if( birdieLandedOnMe != ITEM_NONE )
		{
			birdieJumpOffsetAux = birdieJumpOffsetY;
			onBirdieJump( birdieLandedOnMe, -birdieJumpOffsetY );
		}
		
		int barOffsetY;

		switch( detonatorState )
		{
			case Detonator.DETONATOR_STATE_2ND_STEP:
				currState = PAINT_PARTS;
				
				// Ver imagem correspondente
				//#if SCREEN_SIZE == "SMALL"
//# 					barOffsetY = 4;
				//#else
				barOffsetY = 5;
				//#endif
				break;

			case Detonator.DETONATOR_STATE_FINISHED_0:
			case Detonator.DETONATOR_STATE_FINISHED_1:
				currState = PAINT_PARTS;
				
				// Ver imagem correspondente
				//#if SCREEN_SIZE == "SMALL"
//# 					barOffsetY = 3;
				//#else
				barOffsetY = 6;
				//#endif
				break;

			case Detonator.DETONATOR_STATE_STANDING:
			case Detonator.DETONATOR_STATE_STANDING_TO_1ST_STEP:
				currState = PAINT_SINGLE;
				
				// Ver imagem correspondente
				//#if SCREEN_SIZE == "SMALL"
//# 					barOffsetY = 3;
				//#else
				barOffsetY = 5;
				//#endif
				break;

			/* � mais otimizado utilizar o case default do que todos os outros cases
			case Detonator.DETONATOR_STATE_1ST_STEP:
			case Detonator.DETONATOR_STATE_2ND_TO_3RD_STEP:
			case Detonator.DETONATOR_STATE_3RD_STEP_0:
			case Detonator.DETONATOR_STATE_3RD_STEP_1:
			case Detonator.DETONATOR_STATE_3RD_STEP_2:
			case Detonator.DETONATOR_STATE_3RD_STEP_3:
			case Detonator.DETONATOR_STATE_3RD_STEP_4:
			case Detonator.DETONATOR_STATE_3RD_STEP_5:
			*/
			default:
				currState = PAINT_PARTS;
				
				// Ver imagem correspondente
				//#if SCREEN_SIZE == "SMALL"
//# 					barOffsetY = 3;
				//#else
				barOffsetY = 5;
				//#endif
				break;
		}
		
		// Determina os pontos de conex�o relativos �s m�os
		connectionPoint[CONNECTION_POINT_OUTER_LEFT_HAND].set( leftHandPos.x, leftHandPos.y + barOffsetY );
		connectionPoint[CONNECTION_POINT_INNER_LEFT_HAND].set( leftHandPos.x + handImageSize.x - 1, leftHandPos.y + barOffsetY );
		
		connectionPoint[CONNECTION_POINT_INNER_RIGHT_HAND].set( rightHandPos.x + 1, rightHandPos.y + barOffsetY );
		connectionPoint[CONNECTION_POINT_OUTER_RIGHT_HAND].set( rightHandPos.x + handImageSize.x - 1, rightHandPos.y + barOffsetY );
		
		// Calcula o espa�o entre uma m�o e sua anilha mais pr�xima:  O--w------w--O
		final int distFromHand = ( BAR_WIDTH - ( handImageSize.x << 1 ) - ( connectionPoint[CONNECTION_POINT_INNER_RIGHT_HAND].x - connectionPoint[CONNECTION_POINT_INNER_LEFT_HAND].x ) ) >> 1;
		
		// Obt�m o di�metro atual do peso
		int x, y;
		final int weightDiameter = getWeightDiameter();

		if( movingSide == ITEM_LEFT )
		{
			// Calcula a posi��o da anilha 
			x = leftHandPos.x - distFromHand - weightDiameter;
			y = connectionPoint[CONNECTION_POINT_OUTER_LEFT_HAND].y - (( weightDiameter - BAR_HEIGHT ) >> 1);
			
			// Posiciona a anilha
			drawables[ ITEM_LEFT ].setPosition( x, y ); 
					
			// Determina o ponto de conex�o da anilha
			connectionPoint[CONNECTION_POINT_LEFT_WEIGHT].set( x + weightDiameter, y + (( weightDiameter - BAR_HEIGHT ) >> 1 ) );
		}
		else
		{
			// Calcula a posi��o da anilha 
			x = rightHandPos.x + handImageSize.x + distFromHand;
			y = connectionPoint[CONNECTION_POINT_OUTER_RIGHT_HAND].y - (( weightDiameter - BAR_HEIGHT ) >> 1);
			
			// Posiciona a anilha
			drawables[ ITEM_RIGHT ].setPosition( x, y ); 
					
			// Determina o ponto de conex�o da anilha
			connectionPoint[CONNECTION_POINT_RIGHT_WEIGHT].set( x - 1, y + (( weightDiameter - BAR_HEIGHT ) >> 1 ) );
		}
		
		if( listener != null )
			listener.onWeightPositionChanged( movingSide, new Point( x + ( weightDiameter >> 1 ), y ) );
		
		// Coloca novamente o offset causado pelo pulo do passarinho
		if( birdieLandedOnMe != ITEM_NONE )
			onBirdieJump( birdieLandedOnMe, birdieJumpOffsetAux );
	}

	public void setBirdieLanded( byte side )
	{
		birdieLandedOnMe = side;
	}

	public int getBirdieJumpOffset()
	{
		return birdieJumpOffsetY;
	}
}
