/**
 * StrengthBar.java
 * �2008 Nano Games.
 *
 * Created on 24/03/2008 19:09:38.
 */

package game;

import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.util.MUV;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class StrengthBar extends UpdatableGroup implements Constants
{
	/** Largura da borda */
	private static final byte FRAME_N_PIXELS = 3;
	
	/** N�mero de cores na anima��o de piscar. As cores est�o declaradas no m�todo paintFrame() */
	private static final byte BLINK_N_COLORS = 6;
		
	/** Dura��o que uma cor permanece ativa na anima��o de piscar */
	private static final short BLINK_COLOR_DUR = 100;
	
	/** For�a retida pelo personagem quando ele sobe uma etapa */
	//#ifdef PLEASE_HELP_ME
//# 		private final byte PERCENTAGE_WHEN_COMPLETING = 30;
//# 		private final byte PERCENTAGE_WHEN_COMPLETING_LAST_LEVELS = 40;
	//#else
	private final byte PERCENTAGE_WHEN_COMPLETING = 20;
	private final byte PERCENTAGE_WHEN_COMPLETING_LAST_LEVELS = 30;
	//#endif
	private byte percentWhenCompleting;
	
	/** For�a retida pelo personagem quando ele desce uma etapa */
	private final byte PERCENTAGE_WHEN_LOWERING = 80;
	
	/** For�a m�xima que a barra representa */
	private int maxStr;
	
	/** Tempo que a for�a atual do bra�o se mant�m antes de come�ar a diminuir */
	public int strengthDuration;

	/** Determina quanto o jogador ganha de for�a cada vez que pressiona o bot�o relativo a um bra�o */
	public int strengthGainFactor;
	
	/** Determina quanto o jogador perde de for�a a cada strengthDuration mil�simos*/
	public int strengthLossFactor;
	
	/** Armazenam a configura��o padr�o da fase */
	private int defaultStrengthGainFactor, defaultStrengthLossFactor, defaultStrengthDuration;

	/** Controla a anima��o de perda de for�a */
	private final MUV strengthBarsSpeed = new MUV();
	
	/** Indica se a barra est� piscando */
	private boolean blinking;
	
	/** Auxiliar de anima��o */
	private int blinkCounter;
	
	/** Cor atual da borda da barra */
	private byte currBlinkColor;

	/** Viewport da imagem (ainda n�o temos este conceito implementado nos componentes, ent�o fornece uma
	 * alternativa "menos completa") */
	private short viewportHeight;
	
	/** Etapa atual da barra de for�a */
	private byte currStep;
	
	/** For�a aplicada na barra */
	private int currStrength;
	
	/** For�a aplicada na barra antes da for�a atual */
	private int prevStrength;
	
	/** Objeto que recebe informa��es das mudan�as de estado da barra de for�a */
	private final StrengthBarListener listener;
	
	/** Preenchimento da barra de for�a */
	private final DrawableImage barFill;
	
	/** Marcas indicativas das etapas de for�a */
	private final DrawableImage barStepMarkNext, barStepMarkPrev;

	/** Construtor */
	public StrengthBar( StrengthBarListener listener ) throws Exception
	{
		super( 4 );
		
		// Armazena o listener
		this.listener = listener;

		// Preenchimento da barra
		Pattern barFillBkg = new Pattern( new DrawableImage( "/strengthBarFillBkg.png" ) );
		insertDrawable( barFillBkg );
		barFillBkg.setPosition( FRAME_N_PIXELS, FRAME_N_PIXELS );
		
		// Fundo da barra
		barFill = new DrawableImage( "/strengthBarFill.png" );
		insertDrawable( barFill );
		barFill.setPosition( FRAME_N_PIXELS, FRAME_N_PIXELS );
		
		// Determina o tamanho do fundo da barra
		barFillBkg.setSize( barFill.getSize() );
		
		// Cria a marca que determina a etapa de for�a
		barStepMarkNext = new DrawableImage( "/strengthBarMarkNext.png" );
		insertDrawable( barStepMarkNext );
		
		// Cria a marca que determina a etapa de for�a
		barStepMarkPrev = new DrawableImage( "/strengthBarMarkPrev.png" );
		insertDrawable( barStepMarkPrev );

		// A barra come�a vazia
		viewportHeight = 0;

		// Determina o tamanho do grupo
		setSize( barFill.getWidth() + ( FRAME_N_PIXELS << 1 ), barFill.getHeight() + ( FRAME_N_PIXELS << 1 ) );
	}

	/** Determina o n�vel atual do jogo */
	private final void setLevel( int level )
	{
		percentWhenCompleting = PERCENTAGE_WHEN_COMPLETING;
		
		switch( level )
		{
			case 0:
			case 1:
			case 2:
				//#ifdef PLEASE_HELP_ME
//# 					maxStr = 90 + ( level * 20 );
//# 					changeLevelDifficulty( 9, 3, 1000 );
				//#elif SmallNokia6822
//# 					maxStr = 70 + ( level * 10 );
//# 					changeLevelDifficulty( 9, 3, 1200 );
				//#elif BAD_INPUT
//# 					maxStr = 120 + ( level * 40 );
//# 					changeLevelDifficulty( 8, 3, 600 );
				//#else
					//#if defined( MediumV360 ) || defined ( SmallL6 ) || defined( VERY_DIFFICULT )
//# 					maxStr = 120 + ( level * 40 );
//# 					changeLevelDifficulty( 6, 2, 250 );
					//#elif defined( BigFlushGraphics )
//# 						maxStr = 100 + ( level * 30 );
//# 						changeLevelDifficulty( 6, 2, 350 );
					//#else
					maxStr = 120 + ( level * 40 );
					changeLevelDifficulty( 6, 2, 350 );
					//#endif
				//#endif
				
				break;
				
			case 3:
			case 4:
			case 5:
				//#ifdef PLEASE_HELP_ME
//# 					maxStr = 70 + ( ( level - 3 ) * 10 );
//# 					changeLevelDifficulty( 8, 3, 900 );
				//#elif SmallNokia6822
//# 					maxStr = 100 + ( level * 10 );
//# 					changeLevelDifficulty( 9, 3, 1200 );
				//#elif BAD_INPUT
//# 					maxStr = 220 + ( ( level - 3 ) * 60 );
//# 					changeLevelDifficulty( 7, 3, 500 );
				//#else
					//#if defined( MediumV360 ) || defined ( SmallL6 ) || defined( VERY_DIFFICULT )
//# 					maxStr = 240 + ( ( level - 3 ) * 30 );
//# 					changeLevelDifficulty( 5, 2, 130 );
					//#elif defined( BigFlushGraphics )
//# 						maxStr = 190 + ( ( level - 3 ) * 30 );
//# 						changeLevelDifficulty( 5, 2, 250 );
					//#else
					maxStr = 240 + ( ( level - 3 ) * 70 );
					changeLevelDifficulty( 5, 2, 200 );
					//#endif
				//#endif
				break;
				
			case 6:
			case 7:
			case 8:
				//#ifdef PLEASE_HELP_ME
//# 					maxStr = 100 + ( ( level - 6 ) * 10 );
//# 					changeLevelDifficulty( 8, 3, 900 );
				//#elif SmallNokia6822
//#						maxStr = 70 + ( level * 10 );
//# 					changeLevelDifficulty( 9, 3, 1000 );
				//#elif BAD_INPUT
//# 					maxStr = 250 + ( ( level - 6 ) * 50 );
//# 					changeLevelDifficulty( 6, 3, 500 );
				//#else
					//#if defined( BigEL71 ) || defined( BigA1200 ) || defined( SmallC420 ) || defined( SmallSamsungWidescreen )
//# 					maxStr = 240 + ( ( level - 6 ) * 70 );
//# 					changeLevelDifficulty( 4, 2, 250 );
					//#elif defined( MediumV360 ) || defined ( SmallL6 ) || defined( VERY_DIFFICULT )
//# 						maxStr = 210 + ( ( level - 6 ) * 30 );
//# 						changeLevelDifficulty( 4, 2, 130 );
					//#elif defined( BigFlushGraphics )
//# 						maxStr = 180 + ( ( level - 6 ) * 30 );
//# 						changeLevelDifficulty( 4, 2, 200 );
					//#else
					maxStr = 240 + ( ( level - 6 ) * 70 );
					changeLevelDifficulty( 4, 2, 200 );
					//#endif
				//#endif
				break;
				
			case 9:
			case 10:
			case 11:
				//#ifdef PLEASE_HELP_ME
//# 					maxStr = 130 + ( ( level - 9 ) * 10 );
//# 					changeLevelDifficulty( 8, 3, 800 );
				//#elif SmallNokia6822
//# 					maxStr = 100 + ( level * 10 );
//# 					changeLevelDifficulty( 9, 3, 1000 );
				//#elif BAD_INPUT
//# 					maxStr = 220 + ( ( level - 9 ) * 40 );
//# 					changeLevelDifficulty( 6, 3, 450 );
				//#else
					//#if defined( BigEL71 ) || defined( BigA1200 ) || defined( SmallC420 ) || defined( SmallSamsungWidescreen )
//# 					maxStr = 100 + ( ( level - 9 ) * 10 );
//# 					changeLevelDifficulty( 4, 3, 285 );
					//#elif defined( MediumV360 ) || defined ( SmallL6 ) || defined( VERY_DIFFICULT )
//# 					maxStr = 190 + ( ( level - 9 ) * 30 );
//# 					changeLevelDifficulty( 4, 3, 185 );
					//#elif defined( BigFlushGraphics )
//# 						maxStr = 75 + ( ( level - 9 ) * 10 );
//# 						changeLevelDifficulty( 4, 3, 275 );
					//#else
						maxStr = 190 + ( ( level - 9 ) * 30 );
						changeLevelDifficulty( 4, 3, 285 );
					//#endif
				//#endif
				break;
				
			case 12:
			case 13:
			case 14:
				//#ifdef PLEASE_HELP_ME
//# 					maxStr = 160 + ( ( level - 12 ) * 5 );
//# 					changeLevelDifficulty( 8, 3, 800 );
				//#elif SmallNokia6822
//# 					maxStr = 125 + ( level * 5 );
//# 					changeLevelDifficulty( 9, 3, 1000 );
				//#elif BAD_INPUT
//# 					maxStr = 170 + ( ( level - 12 ) * 15 );
//# 					changeLevelDifficulty( 6, 3, 400 );
				//#else
					//#if defined( BigEL71 ) || defined( BigA1200 ) || defined( SmallC420 ) || defined( SmallSamsungWidescreen )
//# 					maxStr = 50 + ( ( level - 12 ) * 5 );
//# 					percentWhenCompleting = PERCENTAGE_WHEN_COMPLETING_LAST_LEVELS;
//# 					changeLevelDifficulty( 4, 3, 250 );
					//#elif defined( MediumV360 ) || defined ( SmallL6 ) || defined( VERY_DIFFICULT )
//# 					maxStr = 170 + ( ( level - 12 ) * 30 );
//# 					changeLevelDifficulty( 4, 3, 150 );
					//#elif defined( BigFlushGraphics )
//# 						maxStr = 55 + ( ( level - 12 ) * 5 );
//# 						percentWhenCompleting = PERCENTAGE_WHEN_COMPLETING_LAST_LEVELS;
//# 						changeLevelDifficulty( 4, 3, 250 );
					//#else
						maxStr = 170 + ( ( level - 12 ) * 25 );
						changeLevelDifficulty( 4, 3, 250 );
					//#endif
				//#endif
				break;
		}
	}
	
	/** Modifica a dificuldade padr�o da fase */
	public final void changeLevelDifficulty( int newStrengthGainFactor, int newStrengthLossFactor, int newStrengthDuration )
	{
		defaultStrengthGainFactor = strengthGainFactor;
		defaultStrengthLossFactor = strengthLossFactor;
		defaultStrengthDuration = strengthDuration;
		
		strengthGainFactor = newStrengthGainFactor;
		strengthLossFactor = newStrengthLossFactor;
		strengthDuration = newStrengthDuration;
		
		resetLossFactor();
	}
	
	/** Retoma a dificuldade padr�o da fase */
	public final void setDefaultDifficulty()
	{
		strengthGainFactor = defaultStrengthGainFactor;
		strengthLossFactor = defaultStrengthLossFactor;
		strengthDuration = defaultStrengthDuration;
		
		resetLossFactor();
	}
	
	/** Inicializa os atributos da barra de for�a */
	public final void reset( int level )
	{
		setLevel( level );
		setStep( ( byte )0 );
		setBlink( false );
		setStrength( 0 );
		prevStrength = 0;
		resetLossFactor();
	}
	
	/** Modifica o fator de perda de for�a padr�o da fase */
	private final void resetLossFactor()
	{
		if( strengthDuration > 0 )
		{
			final int speed = ( 1000 * strengthLossFactor ) / strengthDuration;
			strengthBarsSpeed.setSpeed( speed, true );
		}
	}
	
	/** Obt�m a etapa de for�a atual */
	public final byte getStep()
	{
		return currStep;
	}
	
	/** Determina a etapa de for�a atual */
	public final boolean setStep( byte step )
	{
		// N�o deixa determinar uma etapa fora dos limites
		if( ( step > N_STRENGTH_BAR_STEPS ) || ( step < 0 ) )
			return false;
		
		// Armazena a etapa atual
		final byte dir = step > currStep ? ( byte )1 : step < currStep ? ( byte )-1 : 0;
		currStep = step;
		
		// Reposiciona as marcas
		final int barFillFirstPos = ( barFill.getHeight() * STEP_MAX_STRENGTH_PERCENTAGE )/100;
		final int marksPosY = size.y - FRAME_N_PIXELS - ( barStepMarkNext.getHeight() >> 1 );
		
		barStepMarkPrev.setPosition( 0, marksPosY - ( currStep * barFillFirstPos ));
		barStepMarkNext.setPosition( 0, marksPosY - (( currStep + 1 ) * barFillFirstPos ));
		
		// Verifica a visibilidade das marcas de for�a
		if( currStep == 0 )
		{
			barStepMarkPrev.setVisible( false );
			barStepMarkNext.setVisible( true );
		}
		else if( currStep == N_STRENGTH_BAR_STEPS )
		{
			barStepMarkPrev.setVisible( false );
			barStepMarkNext.setVisible( false );
		}
		else
		{
			barStepMarkPrev.setVisible( true );
			barStepMarkNext.setVisible( true );
		}
		
		// Modifica a for�a retida pelo personagem, visando evitar macetes
		final byte DIR_NONE = 0;
		final byte DIR_UP = 1;
		// N�o usamos final byte DIR_DOWN = -1;
		if( dir != DIR_NONE )
		{
			final byte percent = dir == DIR_UP ? percentWhenCompleting : PERCENTAGE_WHEN_LOWERING;
			final int prevStepStr = getStepStrength( ( byte )( currStep - 1 ));
			final int currStepStr = getStepStrength( currStep );
			final int strength = prevStepStr + (( ( currStepStr - prevStepStr ) * percent ) / 100 );
				
			if( strength < currStrength )
				setStrength( strength );
		}
		return true;
	}
	
	public final void noMarks()
	{
		barStepMarkPrev.setVisible( false );
		barStepMarkNext.setVisible( false );
	}
	
	/** Obt�m a for�a representada pela barra */
	public final int getStrength()
	{
		return currStrength;
	}
	
	/** Indica o n�vel de for�a que deve ser representado pela barra */
	public final void setStrength( int strength )
	{
		// Armazena a for�a recebida como par�metro
		prevStrength = currStrength;
		currStrength = strength;
		
		// Garante que n�o ir� passar do limite m�ximo/m�nimo da for�a
		if( currStrength > maxStr )
			currStrength = maxStr;
		else if( currStrength < 0 )
			currStrength = 0;

		// Regrinha de 3:
		// MaxStrength - Height
		//     currStrength     -   x
		viewportHeight = ( short )( ( barFill.getHeight() * currStrength ) / maxStr );
	}
	
	/** Aumenta a for�a atual */
	public final void putStrength()
	{
		setStrength( currStrength + strengthGainFactor );
	}
	
	/** Diminui a for�a atual */
	private final void loseStrength( int delta )
	{
		final int aux = currStrength - strengthBarsSpeed.updateInt( delta );
		setStrength( aux );
		
		if( aux > 0 )
			listener.onLoseStrength( this );
	}
	
	public final boolean isStepCompleted()
	{
		if( currStep + 1 <= N_STRENGTH_BAR_STEPS )
			return currStrength >= getStepStrength( currStep );
		return false;
	}
	
	private final int getStepStrength( byte step )
	{
		return ( step + 1 ) * ( ( maxStr * STEP_MAX_STRENGTH_PERCENTAGE ) / 100 );
	}
	
	public final boolean isStepLowered()
	{
		if( currStep == 0 )
		{
			final int stepStrengh = getStepStrength( currStep );
			return ( prevStrength >= stepStrengh ) && ( currStrength < stepStrengh );
		}

		return currStrength < getStepStrength( ( byte )( currStep - 1 ) );
	}
	
	public final byte getStepPercentage()
	{
		if( currStep == N_STRENGTH_BAR_STEPS )
			return 0;
		
		final int prevStepStrength = getStepStrength( ( byte )( currStep - 1 ) );
		final int aux = ( ( currStrength - prevStepStrength ) * 100 ) / ( getStepStrength( currStep ) - prevStepStrength );
		return ( byte )( aux > 100 ? 100 : aux );
	}
	
	public void update( int delta )
	{
		super.update( delta );
		
		// Faz a anima��o de piscar a moldura da barra
		if( blinking )
		{
			blinkCounter += delta;
			if( blinkCounter > BLINK_COLOR_DUR )
			{
				currBlinkColor = ( byte )( ( currBlinkColor + 1 ) % BLINK_N_COLORS );
				blinkCounter = 0;
			}
		}
		
		// Diminui a for�a com o passar do tempo
		loseStrength( delta );
		
		// Informa ao listener se mudou de estado
		listener.onStepCompleted( isStepCompleted() );

		if( isStepLowered() )
		{
			boolean stepChanged = setStep( ( byte )( currStep - 1 ) );
			listener.onStepLowered( stepChanged );
		}
	}
	
	/** Informa se o contorno da barra deve come�ar a piscar
	 * @param state Usar o valor true para iniciar a anima��o e false para par�-la
	 */
	public final void setBlink( boolean state )
	{
		if( blinking != state )
		{
			blinking = state;
			blinkCounter = 0;
			currBlinkColor = 0;
		}
	}
	
	/** Indica se o contorno da barra est� piscando */
	public final boolean isBlinking()
	{
		return blinking;
	}

	protected final void paint( Graphics g )
	{
		// Sempre desenha a borda
		paintFrame( g );

		// Desenha o fundo da barra
		getDrawable( 0 ).draw( g );
		
		// Desenha o interior de acordo com o viewport
		if( viewportHeight > 0 )
		{
			pushClip( g );
			
			g.clipRect( translate.x + barFill.getPosX(), translate.y + barFill.getPosY() + ( barFill.getHeight() - viewportHeight ), barFill.getWidth(), viewportHeight );
			barFill.draw( g );
			
			popClip( g );
		}
		
		// Desenha as marcas de for�a
		barStepMarkPrev.draw( g );
		barStepMarkNext.draw( g );
	}
	
	/** Desenha a moldura da barra de for�a */
	private final void paintFrame( Graphics g )
	{
		int[] outer = null, middle = null, inner = null;
		final byte COLOR_MEDIUM = 0, COLOR_LIGHT1 = 1, COLOR_LIGHT2 = 2, COLOR_LIGHT3 = 3, COLOR_DARK1 = 4, COLOR_DARK2 = 5;
		
		switch( currBlinkColor )
		{
			case COLOR_MEDIUM:
				inner	= new int[]{  78,  69,  52 };
				middle	= new int[]{ 125, 133, 130 };
				outer	= new int[]{ 209, 208, 217 };
				break;
				
			case COLOR_LIGHT1:
				inner	= new int[]{ 100,  91,  73 };
				middle	= new int[]{ 145, 153, 150 };
				outer	= new int[]{ 218, 217, 225 };
				break;
				
			case COLOR_LIGHT2:
				inner	= new int[]{ 122, 113,  95 };
				middle	= new int[]{ 163, 171, 168 };
				outer	= new int[]{ 225, 225, 231 };
				break;
				
			case COLOR_LIGHT3:
				inner	= new int[]{ 114, 101,  76 };
				middle	= new int[]{ 183, 195, 191 };
				outer	= new int[]{ 255, 255, 255 };
				break;
				
			case COLOR_DARK1:
				inner	= new int[]{  51,  44,  30 };
				middle	= new int[]{  97, 106, 103 };
				outer	= new int[]{ 195, 194, 205 };
				break;
				
			case COLOR_DARK2:
				inner	= new int[]{  39,  32,  20 };
				middle	= new int[]{  82,  91,  88 };
				outer	= new int[]{ 186, 185, 197 };
				break;
		}
		
		g.setColor( outer[0], outer[1], outer[2] );
		g.fillRect( translate.x + 1, translate.y, size.x - 2, 1 );		
		g.fillRect( translate.x + 1, translate.y + size.y - 1, size.x - 2, 1 );
		g.fillRect( translate.x, translate.y + 1, 1, size.y - 2 );
		g.fillRect( translate.x + size.x - 1, translate.y + 1, 1, size.y - 2 );
		
		g.setColor( middle[0], middle[1], middle[2] );
		g.drawRect( translate.x + 1 , translate.y + 1, size.x - 3, size.y - 3 );
		
		g.setColor( inner[0], inner[1], inner[2] );
		g.drawRect( translate.x + 2 , translate.y + 2, size.x - 5, size.y - 5 );
	}
}
