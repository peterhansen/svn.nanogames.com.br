/**
 * StrengthBarListener.java
 * �2008 Nano Games.
 *
 * Created on 16/04/2008 17:14:51.
 */

package game;

/**
 * @author Daniel L. Alves
 */

public interface StrengthBarListener
{
	/** Informa que a etapa atual de for�a foi completada */
	public void onStepCompleted( boolean completed );
	
	/** Informa que houve retrocesso de uma etapa */
	public void onStepLowered( boolean stepChanged );
	
	/** Informa quando h� perda de for�a */
	public void onLoseStrength( StrengthBar caller );
}
