/**
 * BirdieListener.java
 * �2008 Nano Games.
 *
 * Created on 03/04/2008 16:16:43.
 */

package game;

/**
 * @author Daniel L. Alves
 */

public interface BirdieListener
{
	/** Evento recebido a cada pulo do passarinho
	 * @param movingSide Em qual anilha o passarinho est� pousado
	 * @param dy Quanto o passarinho quer se afastar da anilha no eixo y
	 */
	public void onBirdieJump( byte movingSide, int dy );
	
	/** Indica se o passarinho est� pousado no objeto */
	public void setBirdieLanded( byte side );
	
	/** Retorna o offset que est� de fato sendo aplicado pelo pulo do passarinho */
	public int getBirdieJumpOffset();
}
