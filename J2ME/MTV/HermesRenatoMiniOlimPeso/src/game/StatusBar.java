/**
 * StatusBar.java
 * �2008 Nano Games.
 *
 * Created on 28/03/2008 16:04:44.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import core.Constants;
import screens.GameMIDlet;
import screens.GameMenuBkg;

/**
 * @author Daniel L. Alves
 */

public final class StatusBar extends UpdatableGroup implements Constants
{		
	/** Espa�amento da fonte do rel�gio */
	private final byte FONT_TIME_CHAR_OFFSET = 1;
	
	/** Posicionamento do label do cron�metro dentro de sua imagem */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte CHRONOMETER_LABEL_POS_X = 7;
//# 		private final byte CHRONOMETER_LABEL_POS_Y = 4;
	//#else
	private final byte CHRONOMETER_LABEL_POS_X = 11;
	private final byte CHRONOMETER_LABEL_POS_Y = 7;
	//#endif
	
	/** Peda�o do post-it que est� amassado */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte POSTIT_WRECKED_WIDTH = 2;
	//#else
	private final byte POSTIT_WRECKED_WIDTH = 4;
	//#endif
	
	/** Em que coordenada x fica a margem vermelha do post-it */
	private final byte POSTIT_RED_MARGIN_X = 7;
	
	/** Espa�amento da fonte utilizada pela barra de status */
	private final byte FONT_POINTS_CHAR_OFFSET = 1;
	
	/** Altura da barra de status */
	public static byte STATUS_BAR_HEIGHT;
	
	/** Espa�amento entre as vidas */
	private final byte N_PIXELS_BETWEEN_LIVES = 4;
	
	/** �ndices dos frames do sprite que indica o n�mero de vidas */
	private final byte LIFE_FRAME_NORMAL = 0;
	private final byte LIFE_FRAME_LOST = 1;
	
	/** Posicionamento dos �cones de vida no eixo y */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte LIFES_LABEL_POS_Y = 1;
	//#else
	private final byte LIFES_LABEL_POS_Y = 2;
	//#endif
	
	/** �ndice do primeiro srite de vida no grupo */
	private final byte STATUS_BAR_FIRST_LIFE_SPRITE = 7;
	
	//<editor-fold defaultstate="collapsed" desc="Pontua��o">
	
	/** Pontua��o atual */
	private int score;
	
	/** Label que exibe a pontua��o atual do jogador */
	private final Label lbScore;
	
	/** Pontua��o que est� sendo exibida no placar */
	private int scoreShown;

	/** Objeto para controlar a velocidade de atualiza��o da pontua��o */
	private final MUV scoreSpeed = new MUV();
	
	/** Indica se os pontos est�o subindo ou descendo. Auxiliar de anima��o */
	private boolean scoreGoingUp;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Tempo da Fase">
	
	/** Tempo decorrido desde que o jogador iniciou a fase */
	private int timeElapsed;
	
	/** Label que exibe o tempo restante da fase */
	private final Label lbTime;
	
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="N�mero de Vidas">
	
	/** M�ximo de vidas que o jogador pode obter */
	public static final byte MAX_LIVES = 3;

	/** N�mero de vidas que o jogador possui ao iniciar o jogo */
	private static final byte N_LIVES_AT_START = 3;

	/** N�mero de vidas que o jogador possui no momento */
	private byte lives = N_LIVES_AT_START;
	
	//</editor-fold>
	
	/** Construtor */
	public StatusBar() throws Exception
	{
		// Cria o grupo
		super( 7 + N_LIVES_AT_START );
		final GameMIDlet midlet = ( GameMIDlet )GameMIDlet.getInstance();
		
		// Cria o background da barra de status
		final DrawableImage clipboardLeft = new DrawableImage( midlet.BOTTOM_LEFT );
		insertDrawable( clipboardLeft );
		
		final Pattern clipboardCenter = new Pattern( midlet.BOTTOM_CENTER );
		insertDrawable( clipboardCenter );
		
		clipboardCenter.setSize( ScreenManager.SCREEN_WIDTH - ( clipboardLeft.getWidth() << 1 ), clipboardLeft.getHeight() );
		clipboardCenter.setPosition( clipboardLeft.getWidth(), 0 );
		
		final DrawableImage clipboardRight = new DrawableImage( midlet.BOTTOM_RIGHT );
		insertDrawable( clipboardRight );
		clipboardRight.setPosition( clipboardLeft.getWidth() + clipboardCenter.getWidth(), 0 );
		
		// Determina o tamanho do grupo
		setSize( ScreenManager.SCREEN_WIDTH, clipboardLeft.getHeight() );
		
		// Sujeira r�pida
		STATUS_BAR_HEIGHT = ( byte )clipboardLeft.getHeight();
		
		// Cria o cron�metro que exibe o tempo restante da fase
		final DrawableImage chronometer = new DrawableImage( "/chronometer.png" );
		insertDrawable( chronometer );
		//#if SCREEN_SIZE == "SMALL"
//# 			chronometer.setPosition( ( size.x - chronometer.getWidth() ) >> 1, 0 );
		//#else
			chronometer.setPosition( ( size.x - chronometer.getWidth() ) >> 1, ( size.y - chronometer.getHeight() ) >> 1 );
		//#endif
		
		// Se a largura da tela � par...
		if( ( ScreenManager.SCREEN_WIDTH & 0x1 ) == 0 )
			chronometer.move( 1, 0 ); // Deixa mais espa�o do lado esquerdo do que do lado direito
		
		final ImageFont fontTime = ImageFont.createMultiSpacedFont( "/fontTime.png", "/fontTime.dat" );
		fontTime.setCharOffset( FONT_TIME_CHAR_OFFSET );
		lbTime = new Label( fontTime, null );
		insertDrawable( lbTime );
		lbTime.setPosition( chronometer.getPosX() + CHRONOMETER_LABEL_POS_X, chronometer.getPosY() + CHRONOMETER_LABEL_POS_Y );
		
		// Cria o post-it que exibe a pontua��o
		final DrawableImage postIt = new DrawableImage( "/postIt.png" );
		insertDrawable( postIt );
		postIt.setPosition( clipboardLeft.getWidth() - 1 - POSTIT_RED_MARGIN_X, 0 );
		
		final ImageFont fontPoints = ImageFont.createMultiSpacedFont( "/fontPoints.png", "/fontPoints.dat" );
		fontPoints.setCharOffset( FONT_POINTS_CHAR_OFFSET  );
		lbScore = new Label( fontPoints, GameMIDlet.formatNumStr( 0, String.valueOf( MAX_SCORE ).length(), '0' ) );
		insertDrawable( lbScore );
		lbScore.setPosition( ( postIt.getPosX() + postIt.getWidth() + POSTIT_WRECKED_WIDTH - lbScore.getWidth() ) >> 1, ( postIt.getPosY() + postIt.getHeight() - lbScore.getHeight() ) >> 1 );
		
		// Aloca os indicadores das tentativas que o jogador possui
		final Sprite life = new Sprite( "/life.bin", "/life" );
		insertDrawable( life );
		
		final int totalWidth = (( N_PIXELS_BETWEEN_LIVES + life.getWidth() ) * N_LIVES_AT_START ) - N_PIXELS_BETWEEN_LIVES;
		final int chronometerEnd = chronometer.getPosX() + chronometer.getWidth();
		final int widthAvailable = ScreenManager.SCREEN_WIDTH - chronometerEnd - GameMenuBkg.CLIPBOARD_NO_PAPER_WIDTH;
		final int lifeX = chronometerEnd + ( ( widthAvailable - totalWidth ) >> 1 );
		
		life.setPosition( lifeX, LIFES_LABEL_POS_Y );
		
		// Cria as outras vidas
		for( int i = 1 ; i < N_LIVES_AT_START ; ++i )
		{
			final Sprite aux = new Sprite( life );
			insertDrawable( aux );
			aux.setPosition( lifeX + ( i * ( N_PIXELS_BETWEEN_LIVES + life.getWidth() ) ), LIFES_LABEL_POS_Y );
		}
	}

	public byte getLives()
	{
		return lives;
	}

	public void setLives( byte nLives )
	{
		// N�o deixa passar do n�mero m�ximo de vidas
		if( lives > MAX_LIVES )
			lives = MAX_LIVES;
		else
			lives = nLives;
		
		if( lives < 0 )
			return;

		// Indica as vidas perdidas
		byte i = 0;
		final byte max = ( byte )( MAX_LIVES - lives );
		for( ; i < max ; ++i )
			(( Sprite )getDrawable( STATUS_BAR_FIRST_LIFE_SPRITE + i )).setSequence( LIFE_FRAME_LOST );
		
		// Indica as vidas que o jogador ainda possui
		for( ; i < MAX_LIVES ; ++i )
			(( Sprite )getDrawable( STATUS_BAR_FIRST_LIFE_SPRITE + i )).setSequence( LIFE_FRAME_NORMAL );
	}

	public int getScore()
	{
		return score;
	}

	public void halfScore()
	{
		score >>= 1;
		scoreShown >>= 1;
		lbScore.setText( GameMIDlet.formatNumStr( scoreShown, lbScore.getText().length(), '0' ) );
	}
	
	public void changeScore( int points )
	{
		score += points;

		if( score > MAX_SCORE )
			score = MAX_SCORE;
		else if( score < 0 )
			score = 0;

		final int diff = score - scoreShown;
		scoreGoingUp = diff > 0 ? true : false;
		scoreSpeed.setSpeed( diff, false );
	}
	
	public void setTime( int time, boolean resetTimeCounter )
	{
		if( resetTimeCounter )
			timeElapsed = 0;
		
		final byte minutes = ( byte )( time / 60 );
		final byte seconds = ( byte )( time % 60 );

		lbTime.setText( GameMIDlet.formatNumStr( minutes, 2, '0' ) + ":" + GameMIDlet.formatNumStr( seconds, 2, '0' ) );
	}
	
	public void update( int delta )
	{
		super.update( delta );
		
		// Modifica o tempo restante para o t�rmino da fase
		changeTimeRemaining( delta );
		
		// Atualiza a pontua��o
		updateScore( delta, false );
	}

	/** Retorna quanto tempo falta para a fase atual terminar */
	public final int getTime()
	{
		final String clockText = lbTime.getText();
		return Integer.parseInt( clockText.substring( 0, 2 ) ) * 60 + Integer.parseInt( clockText.substring( 3, 5 ) );
	}
		
	/** Altera o tempo restante da fase */
	private void changeTimeRemaining( int t )
	{
		// Atualiza o tempo decorrido
		timeElapsed += t;
		
		// Calcula o tempo total da fase
		final int dec = timeElapsed / 1000;
		timeElapsed %= 1000;
		final int currTime = getTime() - dec;
		
		// Atualiza o rel�gio
		if( currTime < 0 )
			setTime( 0, false );
		else if( dec > 0 )
			setTime( currTime, false );
	}

	/**
	 * Atualiza o label da pontua��o
	 * @param delta Tempo decorrido desde a �ltima chama ao m�todo
	 * @param equalize Indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real
	 */
	public final void updateScore( int delta, boolean changeNow )
	{
		if( scoreShown != score )
		{
			if( changeNow )
			{
				scoreShown = score;
			}
			else
			{
				scoreShown += scoreSpeed.updateInt( delta );
				
				if( ( scoreGoingUp && ( scoreShown > score ) ) || ( !scoreGoingUp && ( scoreShown < score ) ) )
					scoreShown = score;
			}
			lbScore.setText( GameMIDlet.formatNumStr( scoreShown, lbScore.getText().length(), '0' ) );
		}
	}
}
