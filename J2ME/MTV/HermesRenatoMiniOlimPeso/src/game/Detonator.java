/**
 * Character.java
 * �2008 Nano Games.
 *
 * Created on 28/03/2008 15:53:44.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.Spritebiarra;

/**
 * @author Daniel L. Alves
 */

public final class Detonator extends UpdatableGroup implements Constants, BirdieListener, SpriteListener
{
	/** Tronco do personagem: lado esquerdo e lado direito */
	private final Spritebiarra[] body = new Spritebiarra[ BOTH_SIDES ];
	
	/** Bra�os do personagem: esquerdo e direito */
	private final Spritebiarra[] arms = new Spritebiarra[ BOTH_SIDES ];
	
	/** Pernas do personagem: esquerda e direita */
	private final Sprite[] legs = new Sprite[ BOTH_SIDES ];
	
	/** Posi��o original da cabe�a no estado atual */
	private short headOriginalY;
	
	/** Posi��o original dos lados do tronco no estado atual */
	private short bodyOriginalY;
	
	/** Posi��o original dos p�los do corpo */
	private short bodyHairOriginalY;
	
	/** Posi��o original dos bra�os no estado atual */
	private final short[] armOriginalY = { 0, 0 };
	
	/** Deslocamentos m�nimo e m�ximo dos bra�os em cada etapa de for�a */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte[][] armDYByStep = { { 0, 2 }, { -1, 1 } };
	//#else
	private final byte[][] armDYByStep = { { 0, 3 }, { -1, 2 } };
	//#endif
	
	/** Rosto do personagem */
	private final Sprite head;
	
	/** Cinto do personagem */
	private final DrawableImage belt;
	
	/** P�los do peito */
	private final DrawableImage bodyHair;
	
	/** Objeto que ir� receber as informa��es de mudan�a de estado */
	private DetonatorListener listener;
	
	/** Controlador da anima��o de t�rmino de fase */
	private short animAux;
	
	/** Dura��o da anima��o de t�rmino de fase */
	private final short FINISH_ANIM_DUR = 500;
	
	/** Dura��o da anima��o de in�cio de fase */
	private final short START_ANIM_DUR = 50;
	
	/** Dura��o da anima��o da transi��o da etapa 2 para a etapa 3 */
	private final short VUPT_ANIM_DUR = 50;
	
	//<editor-fold defaultstate="collapsed" desc="Estados do personagem">
	
	public static final byte DETONATOR_STATE_STANDING				= 0;
	public static final byte DETONATOR_STATE_STANDING_TO_1ST_STEP	= 1;
	public static final byte DETONATOR_STATE_1ST_STEP				= 2;
	public static final byte DETONATOR_STATE_2ND_STEP				= 3;
	public static final byte DETONATOR_STATE_2ND_TO_3RD_STEP		= 4;
	public static final byte DETONATOR_STATE_3RD_STEP_0				= 5;
	public static final byte DETONATOR_STATE_3RD_STEP_1				= 6;
	public static final byte DETONATOR_STATE_3RD_STEP_2				= 7;
	public static final byte DETONATOR_STATE_3RD_STEP_3				= 8;
	public static final byte DETONATOR_STATE_3RD_STEP_4				= 9;
	public static final byte DETONATOR_STATE_3RD_STEP_5				= 10;
	public static final byte DETONATOR_STATE_FINISHED_0				= 11;
	public static final byte DETONATOR_STATE_FINISHED_1				= 12;
	public static final byte DETONATOR_STATE_SAD					= 13;
	
	public static final byte DETONATOR_N_STATES = 14;
	
	private byte currState = DETONATOR_STATE_STANDING;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Estados do bra�o personagem">
	
	public static final byte DETONATOR_ARM_STATE_1ST_STEP_0				= 0;
	public static final byte DETONATOR_ARM_STATE_1ST_STEP_1				= 1;
	public static final byte DETONATOR_ARM_STATE_1ST_STEP_2				= 2;
	public static final byte DETONATOR_ARM_STATE_1ST_STEP_3				= 3;
	public static final byte DETONATOR_ARM_STATE_2ND_STEP_0				= 4;
	public static final byte DETONATOR_ARM_STATE_2ND_STEP_1				= 5;
	public static final byte DETONATOR_ARM_STATE_2ND_STEP_2				= 6;
	public static final byte DETONATOR_ARM_STATE_2ND_STEP_3				= 7;
	
	public static final byte DETONATOR_ARM_N_STATES = 13;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Apar�ncias do personagem">
	
	private final byte APPEARENE_GREEN	= 0;
	private final byte APPEARENE_PURPLE	= 1;
	private final byte APPEARENE_PINK	= 2;
	
	private final byte N_APPEARENCES = 3;
	
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="�ndices dos sprites do grupo">
	
	private final byte DETONATOR_INDEX_BODY_LEFT	= 0;
	private final byte DETONATOR_INDEX_BODY_RIGHT	= 1;
	private final byte DETONATOR_INDEX_BODY_HAIR	= 2;
	private final byte DETONATOR_INDEX_LEG_LEFT		= 3;
	private final byte DETONATOR_INDEX_LEG_RIGHT	= 4;
	private final byte DETONATOR_INDEX_ARM_LEFT		= 5;
	private final byte DETONATOR_INDEX_ARM_RIGHT	= 6;
	private final byte DETONATOR_INDEX_HEAD			= 7;
	private final byte DETONATOR_INDEX_BELT			= 8;
		
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="�ndices utilizados por armDYByStep">
	
	private final byte MIN = 0;
	private final byte MAX = 1;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Sequ�ncias da cabe�a do personagem">
	
		private final byte HEAD_SEQ_NORMAL				= 0;
		private final byte HEAD_SEQ_SCREAMING			= 1;
		private final byte HEAD_SEQ_HAPPY				= 2;
		private final byte HEAD_SEQ_WHAT_THE_F			= 3;
		private final byte HEAD_SEQ_SAD					= 4;
		private final byte HEAD_SEQ_WATCHING_BIRDIE_MID	= 5;
		private final byte HEAD_SEQ_PAIN				= 6;
		
		private final byte N_HEAD_SEQS					= 7;
		
	//</editor-fold>
	
	/** N�mero de drawables existente no grupo */
	private static byte MAX_DRAWABLES = 9;
	
	/** Construtor */
	public Detonator() throws Exception
	{
		super( MAX_DRAWABLES );
		
		final String descExt = ".bin";
		final String dir = "/Detonator/";
		
		// Corpo
		String path = dir + "body";
		body[ ITEM_LEFT ] = new Spritebiarra( path + descExt, path );
		insertDrawable( body[ ITEM_LEFT ] );
		setBodyOffsets();
		
		body[ ITEM_RIGHT ] = new Spritebiarra( body[ ITEM_LEFT ] );
		body[ ITEM_RIGHT ].mirror( TRANS_MIRROR_H );
		insertDrawable( body[ ITEM_RIGHT ] );
		
		// P�los do peito
		final String imgExt = ".png";
		path = dir + "bodyHair";
		bodyHair = new DrawableImage( path + imgExt );
		insertDrawable( bodyHair );
		
		// Pernas
		path = dir + "legs";
		legs[ ITEM_LEFT ] = new Sprite( path + descExt, path );
		insertDrawable( legs[ ITEM_LEFT ] );
		
		legs[ ITEM_RIGHT ] = new Sprite( legs[ ITEM_LEFT ] );
		legs[ ITEM_RIGHT ].mirror( TRANS_MIRROR_H );
		insertDrawable( legs[ ITEM_RIGHT ] );
		
		// Bra�os
		path = dir + "arms";
		arms[ ITEM_LEFT ] = new Spritebiarra( path + descExt, path );
		insertDrawable( arms[ ITEM_LEFT ] );
		
		setArmOffsets();
		
		arms[ ITEM_RIGHT ] = new Spritebiarra( arms[ ITEM_LEFT ] );
		arms[ ITEM_RIGHT ].mirror( TRANS_MIRROR_H );
		insertDrawable( arms[ ITEM_RIGHT ] );
		
		// Cabe�a
		path = dir + "head";
		head = new Sprite( path + descExt, path );
		insertDrawable( head );
		head.setListener( this, DETONATOR_INDEX_HEAD );
		
		// Cinto
		belt = new DrawableImage( dir + "belt" + imgExt );
		insertDrawable( belt );
		
		// Determina o tamanho do grupo
		setSize( arms[ ITEM_LEFT ].getSize() );
		
		// Deixa pr�-calculado a posi��o X do cinto, da cabe�a, do cabelo e dos p�los do corpo
		belt.setPosition( ( size.x - belt.getWidth() ) >> 1, 0 );
		
		// Posicionamento calculado pelo Sprite Scissors
		//#if SCREEN_SIZE == "SMALL"
//# 			bodyHair.setPosition( 12, 27 );
		//#else
			bodyHair.setPosition( 21, 44 );
		//#endif
		
		// Deixa o personagem com a express�o inicial
		changeHeadState( ( byte )0, HEAD_SEQ_NORMAL );
	}
	
	/** Armazena uma refer�ncia para o objeto que ir� receber as informa��es de mudan�a de estado */
	public void setListener( DetonatorListener listener )
	{
		this.listener = listener;
	}
	
	/** Determina o estado do personagem */
	//#if defined( PLEASE_HELP_ME )
//# 		public void setState( byte state )
	//#else
	public synchronized void setState( byte state )
	//#endif
	{
		// Zera a vari�vel auxiliar
		animAux = 0;
		
		// Armazena o estado recebido
		final byte prevState = currState;
		currState = state;
		
		// Coloca as partes do corpo que se movimentam em suas posi��es originais
		for( byte i = 0 ; i < BOTH_SIDES ; ++i )
		{
			body[i].setPosition( body[i].getPosX(), bodyOriginalY );
			arms[i].setPosition( arms[i].getPosX(), armOriginalY[i] );
		}
		bodyHair.setPosition( bodyHair.getPosX(), bodyHairOriginalY );
		
		// Modifica o estado
		byte forceHeadState = -1;
		switch( currState )
		{
			case DETONATOR_STATE_STANDING:
			case DETONATOR_STATE_STANDING_TO_1ST_STEP:
				forceHeadState = HEAD_SEQ_NORMAL;
					
			case DETONATOR_STATE_1ST_STEP:
			case DETONATOR_STATE_2ND_STEP:
				changeHeadState( prevState, forceHeadState );
			
				for( byte i = 0 ; i < BOTH_SIDES ; ++i )
				{
					arms[i].setSequence( currState );
					legs[i].setSequence( currState );
					body[i].setSequence( currState );
				}
				break;
			
			case DETONATOR_STATE_2ND_TO_3RD_STEP:
				changeHeadState( prevState, forceHeadState );
						
			case DETONATOR_STATE_3RD_STEP_0:
			case DETONATOR_STATE_3RD_STEP_1:
			case DETONATOR_STATE_3RD_STEP_2:
			case DETONATOR_STATE_3RD_STEP_3:
			case DETONATOR_STATE_3RD_STEP_4:
				for( byte i = 0 ; i < BOTH_SIDES ; ++i )
				{
					arms[i].setSequence( currState );
					legs[i].setSequence( 3 );
					body[i].setSequence( 4 );
				}
				break;
				
			case DETONATOR_STATE_3RD_STEP_5:
				for( byte i = 0 ; i < BOTH_SIDES ; ++i )
				{
					arms[i].setSequence( 10 );
					legs[i].setSequence( 3 );
					body[i].setSequence( 5 );
				}

				break;
				
			case DETONATOR_STATE_FINISHED_0:
				for( byte i = 0 ; i < BOTH_SIDES ; ++i )
				{
					arms[i].setSequence( 11 );
					legs[i].setSequence( 0 );
					body[i].setSequence( 6 );
				}
				changeHeadState( ( byte )0, HEAD_SEQ_HAPPY );

				break;
				
			case DETONATOR_STATE_FINISHED_1:
				for( byte i = 0 ; i < BOTH_SIDES ; ++i )
				{
					arms[i].setSequence( 12 );
					body[i].setSequence( 7 );
				}
				legs[ITEM_LEFT].setSequence( 4 );
				legs[ITEM_RIGHT].setSequence( 5 );
				
				break;
				
			case DETONATOR_STATE_SAD:
				changeHeadState( ( byte )0, HEAD_SEQ_SAD );
				currState = prevState;
				break;
		}
		
		// Obt�m o offset do corpo no eixo Y
		final byte seqInd = body[ ITEM_LEFT ].getSequence();
		final int bodyOffsetY = body[ ITEM_LEFT ].getOffset( seqInd, 0 ).y;
			
		// Posiciona o cinto
		//#if SCREEN_SIZE == "SMALL"
//# 			final byte[] beltOffsets = { -1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, 0 };
		//#else
		final byte[] beltOffsets = { -2, -2, -5, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4 };
		//#endif
		belt.setPosition( belt.getPosX(), bodyOffsetY + body[ ITEM_LEFT ].getFrameImage( seqInd, 0 ).getHeight() - ( belt.getHeight() >> 1 ) + beltOffsets[ currState ] );
		
		// Posiciona a cabe�a
		//#if SCREEN_SIZE == "SMALL"
//# 			final byte[] headOffsetsY = { -1, 5, 16, 9, 9, 9, 9, 9, 9, 9, 9, -1, 5 };
		//#else
		final byte[] headOffsetsY = { 2, 11, 26, 18, 18, 18, 18, 18, 18, 18, 18, 1, 13 };
		//#endif
		head.setPosition( body[ ITEM_LEFT ].getPosX(), body[ ITEM_LEFT ].getPosY() + headOffsetsY[ currState ] );
		
		// Posiciona os p�los do peito
		//#if SCREEN_SIZE == "SMALL"
//# 			final byte[] bodyHairOffsetY = { 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5 };
		//#else
		final byte[] bodyHairOffsetY = { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 6, 6 };
		//#endif
		bodyHair.setPosition( bodyHair.getPosX(), bodyOffsetY + bodyHairOffsetY[ currState ] );
		
		// Guarda as posi��es originais das partes do corpo que se movimentam
		headOriginalY = ( short )head.getPosY();
		bodyOriginalY = ( short )body[ ITEM_LEFT ].getPosY();
		bodyHairOriginalY = ( short )bodyHair.getPosY();
		
		for( byte i = 0 ; i < BOTH_SIDES ; ++i )
			armOriginalY[i] = ( short )arms[i].getPosY();
		
		// Informa a mudan�a de estado
		if( listener != null )
			listener.onDetonatorStateChanged( currState );
	}

	public void update( int delta )
	{
		switch( currState )
		{
			case DETONATOR_STATE_STANDING_TO_1ST_STEP:
				animAux += delta;
				if( animAux >= START_ANIM_DUR )
					setState( DETONATOR_STATE_1ST_STEP );
				break;
				
			case DETONATOR_STATE_2ND_TO_3RD_STEP:
				animAux += delta;
				if( animAux >= VUPT_ANIM_DUR )
					setState( DETONATOR_STATE_3RD_STEP_0 );
				break;
				
			case DETONATOR_STATE_FINISHED_0:
				animAux += delta;
				if( animAux >= FINISH_ANIM_DUR )
					setState( DETONATOR_STATE_FINISHED_1 );
				break;
		}
		super.update( delta );
	}
	
	//#if defined( PLEASE_HELP_ME )
//# 	private void changeHeadState( byte prevState, byte forceState )
	//#else
		private synchronized void changeHeadState( byte prevState, byte forceState )
	//#endif
	{
		// Cancela um poss�vel espelhamento da cabe�a
		head.setTransform( TRANS_NONE );

		if( forceState != -1 )
		{
			head.setSequence( forceState );
			return;
		}
		
		if(( prevState < DETONATOR_STATE_1ST_STEP ) || ( prevState > DETONATOR_STATE_3RD_STEP_5 ))
			return;

		if( currState > prevState )
			head.setSequence( HEAD_SEQ_SCREAMING );
		else if( currState < prevState )
			head.setSequence( HEAD_SEQ_WHAT_THE_F );
	}
	
	/** Obt�m o estado do personagem */
	public byte getState()
	{
		return currState;
	}
	
	/** Movimenta um dos bra�os do personagem */
	//#if defined ( PLEASE_HELP_ME )
//# 	public void setArmState( byte side, byte step, byte percentage )
	//#else
		public synchronized void setArmState( byte side, byte step, byte percentage )
	//#endif
	{
		if( currState >= DETONATOR_STATE_FINISHED_0 )
			return;

		int dy = 0;
		if( step  < 2 )
		{
			// Regrinha de 3:
			// Deslocamento Total - 100%
			//          x         - percentage
			// Onde:
			// Deslocamento Total = Deslocamento M�ximo - Deslocamento M�nimo + 1
			// Somo o Deslocamento M�nimo no final pois os intervalos de deslocamento podem ter essas caras:
			// [  1, 5 ]
			// [ -7, 3 ]
			if( percentage >= 100 )
				dy = -armDYByStep[ step ][ MAX ];
			else
				dy = -((( percentage * ( armDYByStep[ step ][ MAX ] - armDYByStep[ step ][ MIN ] + 1 )) / 100 ) + armDYByStep[ step ][ MIN ]);
			
			arms[ side ].setPosition( arms[ side ].getPosX(), armOriginalY[ side ] + dy );
			
			// Move a cabe�a e o tronco levando em considera��o os dois bra�os
			final int minDY = Math.min( armOriginalY[ ITEM_LEFT ] - arms[ ITEM_LEFT ].getPosY(),
										armOriginalY[ ITEM_RIGHT ] - arms[ ITEM_RIGHT ].getPosY() );
			
			head.setPosition( head.getPosX(), headOriginalY - minDY );
			body[ ITEM_LEFT ].setPosition( body[ ITEM_RIGHT ].getPosX(), bodyOriginalY - minDY );
			body[ ITEM_RIGHT ].setPosition( body[ ITEM_RIGHT ].getPosX(), bodyOriginalY - minDY );
			bodyHair.setPosition( bodyHair.getPosX(), bodyHairOriginalY - minDY );
		}
		else
		{
			if( currState == DETONATOR_STATE_2ND_TO_3RD_STEP )
				return;
			
			// Regra de 3. Ver coment�rio acima
			final int STEP_FRAMES_VAR = DETONATOR_STATE_3RD_STEP_5 - DETONATOR_STATE_3RD_STEP_0;
			final byte detonatorState = ( byte )( (( percentage * STEP_FRAMES_VAR ) / 100 ) + DETONATOR_STATE_3RD_STEP_0 );
			setState( detonatorState );
		}
		
		// Informa o deslocamento causado pela mudan�a de estado
		listener.onDetonatorArmStateChanged( currState, side, dy );
	}
	
	// CANCELED : N�o � mais utilizado. A MTV encrencou com a mudan�a de cores do "uniforme" do Detonator
	//<editor-fold>
//	/** Modifica a apar�ncia da roupa do personagem */
//	public void setAppearence( byte level ) throws Exception
//	{
//		// Sorteia uma apar�ncia e monta os mapas de paleta
//		final byte N_COLORS_TO_CHANGE = 3;
//		PaletteMap[] map = null;
//
//		if( level >= 6 && level <= 8 )
//		{
//			map = new PaletteMap[N_COLORS_TO_CHANGE];
//			map[0] = new PaletteMap(  88, 1,  92, 11, 146, 1 );
//			map[1] = new PaletteMap( 124, 0, 129, 15, 200, 1 );
//			map[2] = new PaletteMap( 148, 5, 154, 18, 255, 0 );
//		}
//		else if( ( level >= 3 && level <= 5 ) || ( level >= 9 && level <= 11 ) )
//		{
//			map = new PaletteMap[N_COLORS_TO_CHANGE];
//			map[0] = new PaletteMap(  88, 1,  92, 152, 0,  76 );
//			map[1] = new PaletteMap( 124, 0, 129, 220, 0, 108 );
//			map[2] = new PaletteMap( 148, 5, 154, 255, 0, 144 );
//		}
//		
//		if( activeDrawables == MAX_DRAWABLES )
//		{
//			for( int i = DETONATOR_INDEX_LEG_RIGHT ; i >= DETONATOR_INDEX_BODY_LEFT ; --i )
//				removeDrawable( i );
//		}
//		
//		final String descExt = ".bin";
//		final String dir = "/Detonator/";
//		
//		// Pernas
//		String path = dir + "legs";
//		legs[ ITEM_LEFT ] = new Sprite( path + descExt, path, map );
//		legs[ ITEM_RIGHT ] = new Sprite( legs[ ITEM_LEFT ] );
//		legs[ ITEM_RIGHT ].mirror( TRANS_MIRROR_H );
//		
//		insertDrawable( legs[ ITEM_RIGHT ], DETONATOR_INDEX_BODY_LEFT );
//		insertDrawable( legs[ ITEM_LEFT ], DETONATOR_INDEX_BODY_LEFT );
//		
//		// Corpo
//		path = dir + "body";
//		body[ ITEM_LEFT ] = new Spritebiarra( path + descExt, path, map );
//		setBodyOffsets();
//		
//		body[ ITEM_RIGHT ] = new Spritebiarra( body[ ITEM_LEFT ] );
//		body[ ITEM_RIGHT ].mirror( TRANS_MIRROR_H );
//		
//		insertDrawable( body[ ITEM_RIGHT ], DETONATOR_INDEX_BODY_LEFT );
//		insertDrawable( body[ ITEM_LEFT ], DETONATOR_INDEX_BODY_LEFT );
//	}
	//</editor-fold>

	private void setBodyOffsets()
	{
		//#if SCREEN_SIZE == "SMALL"
//# 			final byte BODY_0_OFFSET_ON_1ST_SEQ = 6;
//# 			final byte BODY_2_OFFSET_ON_4TH_SEQ = 0;
//# 			final byte BODY_4_OFFSET_ON_7TH_SEQ = 6;
		//#else
		final byte BODY_0_OFFSET_ON_1ST_SEQ =  9;
		final byte BODY_2_OFFSET_ON_4TH_SEQ =  0;
		final byte BODY_4_OFFSET_ON_7TH_SEQ = 11;
		//#endif
		
		body[ ITEM_LEFT ].setOffset( 0, 0, new Point( body[ ITEM_LEFT ].getFrameOffset( 0 ) ) );
		body[ ITEM_LEFT ].setOffset( 1, 0, body[ ITEM_LEFT ].getFrameOffset( 0 ).add( 0, BODY_0_OFFSET_ON_1ST_SEQ ) );
		body[ ITEM_LEFT ].setOffset( 2, 0, new Point( body[ ITEM_LEFT ].getFrameOffset( 1 ) ) );
		body[ ITEM_LEFT ].setOffset( 3, 0, new Point( body[ ITEM_LEFT ].getFrameOffset( 2 ) ) );
		body[ ITEM_LEFT ].setOffset( 4, 0, body[ ITEM_LEFT ].getFrameOffset( 2 ).add( 0, BODY_2_OFFSET_ON_4TH_SEQ ) );
		body[ ITEM_LEFT ].setOffset( 5, 0, new Point( body[ ITEM_LEFT ].getFrameOffset( 3 ) ) );
		body[ ITEM_LEFT ].setOffset( 6, 0, new Point( body[ ITEM_LEFT ].getFrameOffset( 4 ) ) );
		body[ ITEM_LEFT ].setOffset( 7, 0, body[ ITEM_LEFT ].getFrameOffset( 4 ).add( 0, BODY_4_OFFSET_ON_7TH_SEQ ) );
	}
	
	private void setArmOffsets()
	{
		final byte ARMS_N_FRAMES = 12;
		for( byte i = 0 ; i < ARMS_N_FRAMES ; ++i )
			arms[ ITEM_LEFT ].setOffset( i, 0, new Point( arms[ ITEM_LEFT ].getFrameOffset( i )));
		
		//#if SCREEN_SIZE == "SMALL"
//# 			final byte ARM_11_OFFSET_ON_12TH_SEQ = 6;
		//#else
		final byte ARM_11_OFFSET_ON_12TH_SEQ = 11;
		//#endif
		arms[ ITEM_LEFT ].setOffset( ARMS_N_FRAMES, 0, arms[ ITEM_LEFT ].getFrameOffset( ARMS_N_FRAMES - 1 ).add( 0, ARM_11_OFFSET_ON_12TH_SEQ ) );
	}

	public void onBirdieJump( byte movingSide, int dy )
	{
		if( currState >= DETONATOR_STATE_FINISHED_0 )
			return;

		changeHeadState( ( byte )0, HEAD_SEQ_PAIN );
	}

	public void setBirdieLanded( byte side )
	{
		if( side == ITEM_NONE )
		{
			if( ( currState < DETONATOR_STATE_FINISHED_0 ) && ( head.getSequence() != HEAD_SEQ_SAD ) )
				changeHeadState( ( byte )0, HEAD_SEQ_PAIN );
		}
		else
		{
			if( ( currState < DETONATOR_STATE_FINISHED_0 ) && ( head.getSequence() != HEAD_SEQ_SAD ) )
			{
				changeHeadState( ( byte )0, HEAD_SEQ_WATCHING_BIRDIE_MID );

				if( side == ITEM_LEFT )
					head.mirror( TRANS_MIRROR_H );
			}
		}
	}

	public int getBirdieJumpOffset()
	{
		return 0;
	}

	public void onSequenceEnded( int id, int sequence )
	{
		if( id == DETONATOR_INDEX_HEAD )
		{
			switch( sequence )
			{
				case HEAD_SEQ_SCREAMING:
				case HEAD_SEQ_WHAT_THE_F:
					head.setSequence( HEAD_SEQ_PAIN );
					break;
			}
		}
	}

	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
}
