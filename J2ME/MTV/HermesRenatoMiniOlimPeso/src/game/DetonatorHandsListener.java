/**
 * DetonatorHandsListener.java
 * �2008 Nano Games.
 *
 * Created on 14/04/2008 10:30:41.
 */
package game;

import br.com.nanogames.components.util.Point;

/**
 *
 * @author Daniel L. Alves
 */

public interface DetonatorHandsListener
{
	/** Informa quando uma das m�os do personagem muda de posi��o */
	public void onDetonatorHandsScreenPosChanged( byte movingSide, byte detonatorState, Point leftHandPos, Point rightHandPos, Point handImageSize );
}
