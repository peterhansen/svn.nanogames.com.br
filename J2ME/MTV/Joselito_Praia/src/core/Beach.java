/**
 * Beach.java
 *
 * Created on 28/11/2007 11:48:39
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.UpdatablePattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;
import screens.PlayScreen;

//#if BRANDS == "true"
import screens.BrandScreen;
//#endif


/**
 * Essa classe descreve a praia, utilizada como background da tela de jogo. O controle da inser��o e remo��o de elementos
 * (farofeiros e n�o-farofeiros) tamb�m � feita nesta classe.
 * �2007 Nano Games
 * @author peter
 */
public final class Beach extends UpdatableGroup implements Constants, SpriteListener {
	
	private static final byte TOTAL_ITEMS = 11;
	
	private static final byte PARALLAX_PERCENT_SKY		= 92;
	private static final byte PARALLAX_PERCENT_CLOUDS	= 80;
	private static final byte PARALLAX_PERCENT_ISLAND	= 70;
	private static final byte PARALLAX_PERCENT_WATER	= 40;
	private static final byte PARALLAX_PERCENT_SAND		= 20;
	
	private final Pattern sand;
	
	private final Pattern water;
	
	private final Pattern sky;
	
	/** ilha no fundo de tela */
	private final DrawableImage island;
	
	private final Boom boom;
	private static Sprite boomSet;
	/** �ndice do sprite da explos�o na cole��o em que est� atualmente */
	private int boomIndex = -1;
	/** fileira que est� com o sprite da explos�o atualmente */
	private UpdatableGroup boomGroup;
	
	/** elemento atingido pelo �ltimo tiro (grupo � o mesmo do �ltimo tiro, caso tenha acertado) */
	private Element elementHit;
	
	// <editor-fold desc="fileiras de elementos">
	/** n�mero m�ximo de fileiras de elementos */
	private static final byte MAX_ELEMENT_ROWS = 6;
	
	/** n�mero m�ximo de elementos por fileira */
	private static final byte ELEMENTS_PER_ROW_MAX	= 9;
	
	/** n�mero m�nimo de farofeiros adicionados para cada elemento atingido */
	private static final byte GROUP_SIZE_MIN = 2;
	
	/** n�mero m�ximo de farofeiros adicionados para cada elemento atingido */
	private static final byte GROUP_SIZE_MAX = 4;
	
	/** tamanho do grupo atual */
	private byte currentGroupSize;
	
	/** quantidade m�xima de farofeiros ativos simultaneamente */
	//#ifdef LOW_JAR
//# 	private static final byte ACTIVE_FAROFERS_MAX = 3;
	//#else
	private static final byte ACTIVE_FAROFERS_MAX = GROUP_SIZE_MAX;
	//#endif
	
	/** Quantidade m�nima de farofeiros para que haja inser��o de um novo grupo. */
	private static final byte ACTIVE_FAROFERS_MIN = 0;
	
	/** quantidade total de farofeiros ativos no momento */
	private short activeFarofers;
	
	/** quantidade total de n�o-farofeiros ativos no momento */
	private short activeNonFarofers;	
	
	
	/** fileiras de elementos - o valor de fileiras � duplicado, pois s�o intercaladas fileiras de elementos 
	 * farofeiros (pares) e n�o-farofeiros (�mpares)
	 */
	private final UpdatableGroup[] elementRows = new UpdatableGroup[ MAX_ELEMENT_ROWS ];
	
	/** posi��o y de inser��o dos elementos em cada fileira */
	private final int[] ROW_ELEMENT_Y = new int[ MAX_ELEMENT_ROWS ];
	
	// </editor-fold>
	
	//#if DEMO == "true"
//# 	// na vers�o demo h� menos farofeiros por n�vel
//# 	
//# 	/** quantidade total m�nima de farofeiros por n�vel */
//# 	private static final byte FAROFERS_PER_LEVEL_MIN = 3;
//# 	/** quantidade total m�xima de farofeiros por n�vel */
//# 	private static final byte FAROFERS_PER_LEVEL_MAX = 12;
	//#else
	/** quantidade total m�nima de farofeiros por n�vel */
	private static final byte FAROFERS_PER_LEVEL_MIN = 5;
	/** quantidade total m�xima de farofeiros por n�vel */
	private static final byte FAROFERS_PER_LEVEL_MAX = 30;
	//#endif
	
	/** quantidade restante de farofeiros no n�vel atual */
	private byte farofersLeft;
	
	//#ifndef LOW_JAR
	/** bal�o de publicidade */
	private DrawableImage balloon;
	
	/** controla a velocidade de movimenta��o do bal�o de publicidade */
	private final MUV balloonSpeed = new MUV();
	//#endif
	
	/** porcentagem inicial de n�o-farofeiros */
	private static final byte NON_FAROFERS_PERCENT_INITIAL	= 56;
	/** porcentagem final de n�o-farofeiros */
	private static final short NON_FAROFERS_PERCENT_FINAL	= 157;
	
	/** porcentagem atual de n�o-farofeiros */
	private short currentNonFarofersPercent;
	
	/** quantidade m�xima de elementos n�o-farofeiros est�ticos por fileira */
	private static final byte STATIC_NON_FAROFERS_MAX = BEACH_WIDTH_MAX / 70;
	
	// limita a quantidade de nuves, por quest�es de desempenho
	//#ifndef JAR_100_KB
		// n�o h� nuvens na vers�o de jar pequeno
		//#ifdef LOW_JAR
//# 		/** n�mero total de imagens de nuvens */
//# 		private static final byte CLOUDS_TOTAL_IMAGES = 2;
//# 		
//# 		private static final byte MAX_CLOUDS = 3;
		//#else
		/** n�mero total de imagens de nuvens */
		private static final byte CLOUDS_TOTAL_IMAGES = 4;
		
	private static final byte MAX_CLOUDS = 7;
		//#endif
		private final Cloud[] clouds = new Cloud[ MAX_CLOUDS ];
		private final UpdatableGroup cloudsGroup;

		private static Sprite cloudSet;
	//#endif
	
	//#ifndef LOW_JAR
	/** Tipo de cen�rio atual. */
	private byte currentBkgType;
	
	/** �ndice do cen�rio que n�o possui nuvens (noite). */
	private static final byte BKG_INDEX_NO_CLOUDS = 3;
	
	/** Tipos de cen�rio diferentes. */
	private static final byte BACKGROUND_TYPES_TOTAL = 4;

	/** Tipo de mar para cada cen�rio. */ 
	private static final byte[] BACKGROUND_OCEAN_TYPES = { 0, 0, 2, 3 };			
	//#endif
	
	private static boolean imagesLoaded;
	
	private int LIMIT_RIGHT;
	
	private final PlayScreen playScreen;


	public Beach( PlayScreen playScreen ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.playScreen = playScreen;
		
		// insere o c�u
		DrawableImage img = new DrawableImage( PATH_BACKGROUND_0 + "sky.png" );
		sky = new Pattern( img );
		sky.setSize( 0, img.getSize().y );
		insertDrawable( sky );
		
		//#ifndef JAR_100_KB
		// insere as nuvens e o bal�o de publicidade
		cloudsGroup = new UpdatableGroup( MAX_CLOUDS + 1 );
		cloudsGroup.setSize( 0, sky.getSize().y );
		insertDrawable( cloudsGroup );
		//#endif
		
		// insere a ilha
		island = new DrawableImage( PATH_IMAGES + "island.png" );
		island.setPosition( 0, sky.getSize().y - island.getSize().y );
		insertDrawable( island  );
		
		// insere a �gua
		//#ifdef LOW_JAR
//# 			final DrawableImage waterImage = new DrawableImage( PATH_BACKGROUND_0 + "ocean_0.png" );
//# 			water = new Pattern( waterImage );
//# 			water.setSize( 0, waterImage.getSize().y );		
		//#else
		if ( GameMIDlet.isLowMemory() ) {
			final DrawableImage waterImage = new DrawableImage( PATH_BACKGROUND_0 + "ocean_0.png" );
			water = new Pattern( waterImage );
			water.setSize( 0, waterImage.getSize().y );
		} else {
			final Sprite waterSprite = loadWaterSprite( currentBkgType );
			water = new UpdatablePattern( waterSprite );			
			water.setSize( 0, waterSprite.getSize().y );
		}			
		//#endif
		water.setPosition( 0, sky.getSize().y );
		insertDrawable( water );
		
		// insere a areia
		sand = new Pattern( null );
		
		//#if SCREEN_SIZE == "BIG"
//# 		final int ELEMENT_ROW_HEIGHT = ScreenManager.SCREEN_HEIGHT >= 320 ? BEACH_ELEMENT_ROW_HEIGHT_BIG : BEACH_ELEMENT_ROW_HEIGHT_MEDIUM;
		//#else
		final int ELEMENT_ROW_HEIGHT = BEACH_ELEMENT_ROW_HEIGHT;
		//#endif
		
		// garante a altura m�nima da faixa de areia, de forma a comportar todas as fileiras de elementos
		final int SAND_MIN_SIZE = ELEMENT_ROW_HEIGHT * ( MAX_ELEMENT_ROWS + 1 );
		final int DIFF = ScreenManager.SCREEN_HEIGHT - ( water.getPosition().y + water.getSize().y + SAND_MIN_SIZE );
		
		if ( DIFF < 0 ) {
			sky.setSize( 0, sky.getSize().y + DIFF );
			island.move( 0, DIFF );
			water.move( 0, DIFF );
			
			//#ifndef JAR_100_KB
			cloudsGroup.setSize( 0, cloudsGroup.getSize().y + DIFF );	
			//#endif
		}
		
		sand.setSize( 0, ScreenManager.SCREEN_HEIGHT - water.getPosition().y - water.getSize().y );
		sand.setPosition( 0, water.getPosition().y + water.getSize().y );
		insertDrawable( sand );
		
		// insere os grupos de elementos
		int y = sand.getPosition().y + ELEMENT_ROW_HEIGHT;
		for ( byte i = 0; i < MAX_ELEMENT_ROWS; ++i, y += ELEMENT_ROW_HEIGHT ) {
			// soma-se 3 � quantidade total de elementos de cada fileira por causa do sprite do tiro
			final UpdatableGroup group = new UpdatableGroup( ELEMENTS_PER_ROW_MAX + 3 );
			elementRows[ i ] = group;
			
			ROW_ELEMENT_Y[ i ] = y;
			group.setSize( 0, ScreenManager.SCREEN_HEIGHT );
			insertDrawable( group );
		}
		
		// aloca o fogo (inser��o � feita na fileira de elementos atingida, no m�todo fire() )
		boom = new Boom( this );
		
		loadBackground( false );
	} // fim do construtor


	//#ifndef LOW_JAR
	public final void update( int delta ) {
		super.update( delta );
		
		balloon.move( balloonSpeed.updateInt( delta ), 0 );
		if ( balloon.getPosition().x >= size.x ) {
			balloonSpeed.setSpeed( -BEACH_CLOUD_MIN_SPEED + NanoMath.randInt( BEACH_CLOUD_MAX_SPEED - BEACH_CLOUD_MIN_SPEED ) );
			
			//#if SCREEN_SIZE == "SMALL"
//# 			if ( water.getPosition().y < balloon.getSize().y )
//# 				balloon.setPosition( -balloon.getSize().x, ( water.getPosition().y - balloon.getSize().y ) >> 1 );
//# 			else
			//#endif
				
			balloon.setPosition( -balloon.getSize().x, NanoMath.randInt( sky.getSize().y - balloon.getSize().y ) );
		}
		
	}
	//#endif
	
	
	/**
	 * 
	 * @param crosshairPosition
	 * @return
	 */
	public final byte fire( Point crosshairPosition ) {
		//#if DEBUG == "true"
		try {
		//#endif		
		
		// corrige a posi��o da mira de acordo com o paralaxe do cen�rio
		final Point firePos = new Point( crosshairPosition.x - elementRows[ 0 ].getPosition().x, crosshairPosition.y );
		
		// caso a anima��o do fogo esteja em andamento, n�o pode dar outro tiro (ao encerrar a anima��o, o valor de
		// fireIndex volta a ficar negativo)
		if ( boomIndex < 0 ) {
			UpdatableGroup lastGroup = null;

			for ( byte i = MAX_ELEMENT_ROWS - 1; i >= 0; --i ) {
				final UpdatableGroup group = elementRows[ i ];
				final int groupSize = group.getUsedSlots();

				for ( byte j = 0; j < groupSize; ++j ) {
					final Drawable d = group.getDrawable( j );

					if ( d.contains( firePos ) ) {
						// acertou um elemento
						final Element e = ( Element ) d;
						
						// antes de inserir o sprite do fogo num grupo, verifica se ele j� estava inserido em outro
						removeFireFromGroup();

						boomGroup = group;
						boomIndex = group.insertDrawable( boom  );
						boom.setRefPixelPosition( e.getRefPixelX(), e.getPosition().y + ( e.getSize().y >> 1 ) );
						boom.setSequence( 0 );

						//#if DEBUG == "true"
						System.out.println( "ATINGIU ELEMENTO(" + e.getType() + "): " + e.getRefPixelX() + ", " + e.getRefPixelY() + " -> " + e.getPosition().x + ", " + e.getPosition().y  );
						//#endif

						switch ( e.getType() ) {
							case ELEMENT_TYPE_FAROFER:
								MediaPlayer.vibrate( VIBRATION_TIME );
								
								--farofersLeft;
								--activeFarofers;
								
								// terminou o n�vel
								if ( farofersLeft <= 0 ) {
									//#if DEMO == "true"
//# 									if ( playScreen.getCurrentLevel() >= DEMO_MAX_LEVELS ) {
//# 										playScreen.setState( PlayScreen.STATE_GAME_OVER );
//# 									} else
									//#endif
									playScreen.setState( PlayScreen.STATE_END_LEVEL_MESSAGE );
								}
								
								elementHit = e;
								return FIRE_HIT_FAROFER;
							
							case ELEMENT_TYPE_NON_FAROFER:
								--activeNonFarofers;
								elementHit = e;
								return FIRE_HIT_NON_FAROFER;
							
							case ELEMENT_TYPE_STATIC:
								return FIRE_HIT_STATIC;
								
							case ELEMENT_TYPE_TOASTED:
								return FIRE_HIT_TOASTED;
						} // fim switch ( e.getType() )
					} // fim if ( collisionArea.intersects( collisionAreaElement ) )
				} // fim for ( byte j = 0; j < groupSize; ++j )

				if ( ROW_ELEMENT_Y[ i ] < firePos.y )
					break;
				else
					lastGroup = group;
			} // fim for ( byte i = MAX_ELEMENT_ROWS - 1; i >= 0; --i )

			// n�o colidiu com nenhum elemento; insere a explos�o na fileira mais adequada
			if ( lastGroup != null ) {
				boomGroup = lastGroup;
				boomIndex = lastGroup.insertDrawable( boom  );
				boom.setRefPixelPosition( firePos );
				boom.setSequence( 0 );						
			}
			return FIRE_HIT_NOTHING;
		} // fim if ( fireIndex < 0 )
		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif		
		
		return FIRE_HIT_CANT_SHOOT;
	} // fim do m�todo fire( Point )


	/**
	 * Carrega o cen�rio do n�vel atual.
	 * 
	 * @param loadAll indica se o c�u, areia e �gua precisam ser recarregados. No in�cio do jogo eles j� est�o carregados;
	 * nas outras trocas de cen�rio, devem ser trocados.
	 * @throws java.lang.Exception
	 */
	private final void loadBackground( boolean loadAll ) throws Exception {
		// troca de cen�rio
		//#ifdef LOW_JAR
//# 		final byte currentBkgType = 0;
		//#else
		if ( loadAll && playScreen.getCurrentLevel() > 1 )
			currentBkgType = ( byte ) ( ( currentBkgType + 1 ) % BACKGROUND_TYPES_TOTAL );
		
		// anula primeiro a refer�ncia para os patterns antigos para evitar fragmenta��o de mem�ria nos aparelhos
		// Motorola
		if ( loadAll ) {
			sky.setFill( null );
			water.setFill( null );
			cloudSet = null;
		}
		sand.setFill( null );
		
		//#endif
		
		final String BKG_PATH = PATH_BACKGROUND + currentBkgType + "/";
		
		sand.setFill( new DrawableImage( BKG_PATH + "sand.png" ) );
        
        Thread.yield();

		//#ifndef JAR_100_KB
		while ( cloudsGroup.getUsedSlots() > 0 )
			cloudsGroup.removeDrawable( 0 );
		
		System.gc();
		
		if ( loadAll ) {
			System.gc();
			sky.setFill( new DrawableImage( BKG_PATH + "sky.png" ) );
            
            Thread.yield();
			
			//#ifndef LOW_JAR
			System.gc();
			water.setFill( loadWaterSprite( BACKGROUND_OCEAN_TYPES[ currentBkgType ] ) );
			//#endif
		}
		
		if ( loadAll || playScreen.getCurrentLevel() <= 1 ) {
			// na vers�o de jar pequeno, n�o precisa fazer o teste, pois s� h� 1 cen�rio.
			//#ifndef LOW_JAR
			if ( currentBkgType != BKG_INDEX_NO_CLOUDS ) {
			//#endif
				final Image[] images = new Image[ CLOUDS_TOTAL_IMAGES ];
				byte[][] sequences = new byte[ 1 ][ CLOUDS_TOTAL_IMAGES ];
				for ( byte i = 0; i < CLOUDS_TOTAL_IMAGES; ++i ) {
					images[ i ] = ImageLoader.loadImage( BKG_PATH + "cloud_" + i + ".png" );
					sequences[ 0 ][ i ] = i;
                    
                    Thread.yield();
				}

				cloudSet = new Sprite( images, sequences, new short[] { 0, 0, 0, 0 } );
				for ( byte i = 0; i < MAX_CLOUDS; ++i ) {
					clouds[ i ] = new Cloud( this );
					cloudsGroup.insertDrawable( clouds[ i ] );
				}		
				
			//#ifndef LOW_JAR
			}
			//#endif
		}
		//#endif		
	
	} // fim do m�todo loadBackground( boolean )
	
	
	private final void removeFireFromGroup() {
		if ( boomGroup != null ) {
			boomGroup.removeDrawable( boomIndex  );
			boomGroup = null;
			boomIndex = -1;
		}
	}
	
	
	//#ifndef LOW_JAR
	private final Sprite loadWaterSprite( int index ) throws Exception {
		final byte WATER_TOTAL_FRAMES = 3;
		final short WATER_FRAME_TIME = 750;
		final Image[] waterFrames = new Image[ WATER_TOTAL_FRAMES ];
		for ( byte i = 0; i < WATER_TOTAL_FRAMES; ++i ) {
			waterFrames[ i ] = ImageLoader.loadImage( PATH_BACKGROUND + index + "/ocean_" + i + ".png" );
            
            Thread.yield();
		}

		final byte[][] waterSequences = new byte[][] { { 0, 0, 1, 2, 1 } };

		return new Sprite( waterFrames, waterSequences, new short[] { WATER_FRAME_TIME } );	
	}
	//#endif
	

	public final void prepare( int level ) throws Exception {
		setSize( BEACH_WIDTH_MIN + ( BEACH_WIDTH_MAX - BEACH_WIDTH_MIN ) * level / MAX_LEVEL, ScreenManager.SCREEN_HEIGHT );
		
		Element.setInfo( level, size.x );
		
		for ( byte i = 0; i < elementRows.length; ++i )
			elementRows[ i ].setSize( size.x, elementRows[ i ].getSize().y );

		// calcula a quantidade de farofeiros do n�vel
		farofersLeft = ( byte ) ( FAROFERS_PER_LEVEL_MIN + ( FAROFERS_PER_LEVEL_MAX - FAROFERS_PER_LEVEL_MIN ) * level / MAX_LEVEL );
		
		currentGroupSize = ( byte ) ( GROUP_SIZE_MIN + ( GROUP_SIZE_MAX - GROUP_SIZE_MIN ) * level / MAX_LEVEL );
		
		currentNonFarofersPercent = ( short ) ( NON_FAROFERS_PERCENT_INITIAL + ( NON_FAROFERS_PERCENT_FINAL - NON_FAROFERS_PERCENT_INITIAL ) * level / MAX_LEVEL );
		
		activeFarofers = 0;
		activeNonFarofers = 0;
		
		onSequenceEnded( 0, 0 );
		
		// no in�cio de cada n�vel, remove todos os elementos 
		for ( int i = 0; i < elementRows.length; ++i ) {
			final UpdatableGroup group = elementRows[ i ];

			// remove do final para o in�cio, pois assim evita-se excesso de rearruma��es internas do grupo
			while ( group.getUsedSlots() > 0 )
				group.removeDrawable( group.getUsedSlots() - 1 );
		}		
		
		updateRows( true );
		
		//#ifndef LOW_JAR
			// exceto nas vers�es de jar pequeno, faz a troca do cen�rio a cada CHANGE_BACKGROUND_LEVELS n�veis
			if ( playScreen.getCurrentLevel() % CHANGE_BACKGROUND_LEVELS == 0 )
				loadBackground( true );

			// insere o bal�o com a marca atual
			if ( balloon != null ) {
				balloon = null;
				for ( byte i = 0; i < cloudsGroup.getUsedSlots(); ++i ) {
					final Drawable d = cloudsGroup.getDrawable( i );
					if ( !( d instanceof Cloud ) )
						cloudsGroup.removeDrawable( i );
				}
			}
			//#if BRANDS == "true"
			// sorteia entre os bal�es da MTV, Skol, Coca-Cola e Oi
			balloon = new DrawableImage( PATH_BALLOON + BrandScreen.getCurrentBrand() + ".png" );
			//#else
//# 			// sorteia entre os bal�es da MTV e Nano
//# 			balloon = new DrawableImage( PATH_BALLOON + ( BRAND_MTV + ( playScreen.getCurrentLevel() & 1 ) ) + ".png" );
			//#endif
			// posiciona o bal�o no final do n�vel, para que ele possa ser reinserido na posi��o correta
			balloon.setPosition( size.x, 0 );
			cloudsGroup.insertDrawable( balloon );
		//#endif
	}
	
	
	/**
	 * Atualiza as fileiras de elementos, garantindo que nenhuma delas possui menos que o m�nimo de elementos ou mais que
	 * o m�ximo permitido no n�vel atual. 
	 */
	private final void updateRows( boolean startLevel ) {
		//#if DEBUG == "true"
		try {
		//#endif
	
		byte farofersToInsert;
		if ( startLevel ) {
			final byte MAX_STATIC = ( byte ) ( STATIC_NON_FAROFERS_MAX * Math.min( playScreen.getCurrentLevel(), MAX_LEVEL ) / MAX_LEVEL );
			
			if ( MAX_STATIC > 0 ) {
				for ( byte i = 1; i < elementRows.length; i += 2 ) {
					final UpdatableGroup group = elementRows[ i ];

					final byte STATIC_ELEMENTS = MAX_STATIC <= 1 ? 1 : ( byte ) ( ( MAX_STATIC >> 1 ) + NanoMath.randInt( MAX_STATIC >> 1 ) );
					// sorteia a quantidade e posi��o dos elementos est�ticos
					if ( STATIC_ELEMENTS > 0 ) {
						final int WIDTH = size.x / STATIC_ELEMENTS;

						int x = WIDTH >> 1;

						for ( byte j = 0; j < STATIC_ELEMENTS; ++j, x += WIDTH ) {
							final Element e = Element.getStaticElement();
							if ( NanoMath.randInt( 128 ) < 64 )
								e.setTransform( TRANS_MIRROR_H );

							final int RANGE = WIDTH - e.getSize().x;

							e.setRefPixelPosition( x - ( RANGE >> 1 ) + NanoMath.randInt( RANGE ), ROW_ELEMENT_Y[ i ] );
							group.insertDrawable( e );
						}
					}
				}
			} // fim if ( MAX_STATIC > 0 )
			
			// insere um grupo no in�cio de cada n�vel
			farofersToInsert = FAROFERS_PER_LEVEL_MIN;
		} else {
			if ( farofersLeft <= 0 )
				return;
			
			farofersToInsert = ( byte ) Math.min( currentGroupSize, farofersLeft - activeFarofers );
		}
		
		
		while ( activeFarofers < ACTIVE_FAROFERS_MAX && farofersToInsert > 0 ) {
			// ainda h� farofeiros a adicionar
			
			// sorteia uma fileira de farofeiros que esteja com menos da metade dos farofeiros ativos (caso haja
			// menos de 2 farofeiros ativos, pode ser qualquer fileira)
			final byte initialRow = ( byte ) ( NanoMath.randInt( MAX_ELEMENT_ROWS >> 1 ) << 1 );
			byte row = ( byte ) ( ( initialRow + 2 ) % MAX_ELEMENT_ROWS );
			while ( elementRows[ row ].getUsedSlots() > ( activeFarofers >> 1 ) && activeFarofers > 2 && row != initialRow ) {
				row = ( byte ) ( ( row + 2 ) % MAX_ELEMENT_ROWS );
			}
			
			insertElement( row, Element.getRandomElement( ELEMENT_TYPE_FAROFER ) );
			++activeFarofers;
			--farofersToInsert;
			
			//#if DEBUG == "true"
			System.out.println( "inseriu farofeiro na fileira " + row );
			//#endif
		}
		
		// inseriu os farofeiros; verifica se a propor��o m�nima de n�o-farofeiros est� sendo cumprida
		int percent = activeNonFarofers * 100 / activeFarofers;
		while ( percent < currentNonFarofersPercent ) {
			// ainda h� n�o-farofeiros a adicionar
			
			// sorteia uma fileira de n�o-farofeiros que esteja com menos da metade dos farofeiros ativos (caso haja
			// menos de 2 n�o-farofeiros ativos, pode ser qualquer fileira)
			final byte initialRow = ( byte ) ( ( NanoMath.randInt( MAX_ELEMENT_ROWS >> 1 ) << 1 ) + 1 );
			byte row = ( byte ) ( ( initialRow + 2 ) % MAX_ELEMENT_ROWS );
			while ( elementRows[ row ].getUsedSlots() > ( activeNonFarofers >> 1 ) && activeNonFarofers > 2 && row != initialRow ) {
				row = ( byte ) ( ( row + 2 ) % MAX_ELEMENT_ROWS );
			}
			
			insertElement( row, Element.getRandomElement( ELEMENT_TYPE_NON_FAROFER ) );
			++activeNonFarofers;			
			
			//#if DEBUG == "true"
			System.out.println( "inseriu N�O-farofeiro na fileira " + row );
			//#endif
			
			percent = activeNonFarofers * 100 / activeFarofers;
		}
		
		//#if DEBUG == "true"
		System.out.println( "FAROFEIROS: " + activeFarofers + "/" + farofersLeft + "\nN�O-FAROFEIROS: " + activeNonFarofers );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	} // fim do m�todo updateRows()
	
	
	private final void insertElement( int row, Element e ) {
		elementRows[ row ].insertDrawable( e );
		e.setRefPixelPosition( e.getRefPixelX(), ROW_ELEMENT_Y[ row ] );
	}
	

	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		LIMIT_RIGHT = -size.x + ScreenManager.SCREEN_WIDTH;
		
		sky.setSize( width, sky.getSize().y );
		water.setSize( width, water.getSize().y );
		sand.setSize( width, sand.getSize().y );

		//#ifndef JAR_100_KB
		cloudsGroup.setSize( width, cloudsGroup.getSize().y );
		// quando cloudSet � nulo, � sinal de que a imagem das nuvens ainda n�o foi carregada
		if ( cloudSet != null ) {
			for ( byte i = 0; i < MAX_CLOUDS; ++i )
				clouds[ i ].reset( true );
		}
		//#endif
	}


	public final void setPosition( int x, int y ) {
		if ( x < LIMIT_RIGHT )
			x = LIMIT_RIGHT;
		else if ( x > 0 )
			x = 0;

		super.setPosition( x, y );

		// faz o paralaxe dos drawables
		sky.setPosition( -position.x * PARALLAX_PERCENT_SKY / 100, sky.getPosition().y );
		
		//#ifndef JAR_100_KB
		cloudsGroup.setPosition( -position.x * PARALLAX_PERCENT_CLOUDS / 100, cloudsGroup.getPosition().y );
		//#endif
		
		island.setPosition( -position.x * PARALLAX_PERCENT_ISLAND / 100, island.getPosition().y );
		water.setPosition( -position.x * PARALLAX_PERCENT_WATER / 100, water.getPosition().y );
		
		x = -position.x * PARALLAX_PERCENT_SAND / 100;
		sand.setPosition( x, sand.getPosition().y );
		for ( byte i = 0; i < MAX_ELEMENT_ROWS; ++i ) {
			elementRows[ i ].setPosition( x, elementRows[ i ].getPosition().y );
		}
	}
	
	// </editor-fold>
	
	
	// <editor-fold defaultstate="collapsed" desc="classe interna: Cloud">

	//#ifndef JAR_100_KB
	
	private static final class Cloud extends Sprite implements Updatable {
		
		private final MUV muv = new MUV();

		private final Beach beach;

		private Cloud( Beach beach ) throws Exception {
			super( cloudSet );
			
			this.beach = beach;
			reset( false );
		}

		
		public final void update( int delta ) {
			move( muv.updateInt( delta ), 0 );
			
			if ( position.x <= -size.x ) {
				reset( false );
			}
		}
		
		
		public final void reset( boolean useAllWidth  ) {
			muv.setSpeed( BEACH_CLOUD_MIN_SPEED - NanoMath.randInt( BEACH_CLOUD_MAX_SPEED - BEACH_CLOUD_MIN_SPEED ) );
			
			//#ifndef JAR_100_KB
			setFrame( NanoMath.randInt( CLOUDS_TOTAL_IMAGES ) );
			//#endif
			
			defineReferencePixel( 0, size.y >> 1 );
			
			setRefPixelPosition( useAllWidth ? NanoMath.randInt( beach.getSize().x ) : beach.getSize().x + NanoMath.randInt( ScreenManager.SCREEN_WIDTH ),
								 NanoMath.randInt( beach.sky.getSize().y ) );
		}
		
	} // fim da classe interna Cloud
	
	//#endif
	
	//</editor-fold>
	
	
	
	public static final void loadImages() throws Exception {
		if ( !imagesLoaded ) {
			boomSet = new Sprite( PATH_IMAGES + "boom.dat", PATH_IMAGES + "boom" );
			
			imagesLoaded = true;
		}
	}
    
    
	public final void onSequenceEnded( int id, int sequence ) {
		//#if DEBUG == "true"
		try {
		//#endif
		// n�o � necess�rio verificar o id nem qual sequ�ncia foi encerrada, pois somente o fogo possui listener
		// registrado.
		
		removeFireFromGroup();
		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	}
		
	
	public final void removeToastedElement( Element toasted, UpdatableGroup group ) {
		//#if DEBUG == "true"
		try {
		//#endif
		
		for ( int i = 0, slots = group.getUsedSlots(); i < slots; ++i ) {
			if ( group.getDrawable( i ) == toasted ) {
				// se o sprite do fogo estiver no mesmo grupo e tiver um �ndice superior ao do elemento removido,
				// o �ndice do fogo deve ser atualizado para evitar inconsist�ncia no momento de remover este do
				// grupo.
				if ( boomGroup == group && boomIndex > i )
					--boomIndex;
				
				group.removeDrawable( i );
				
				// se o n�mero de farofeiros ativos estiver abaixo do m�nimo, insere novo grupo de farofeiros
				if ( activeFarofers <= ACTIVE_FAROFERS_MIN )
					updateRows( false );
				
				return;
			}
		}
		
		//#if DEBUG == "true"
		// se chegou aqui, � porque houve erro (obrigatoriamente, o elemento tostado deve fazer parte do grupo recebido
		// como par�metro)
		throw new Exception( "ERRO: ELEMENTO TOSTADO - " + toasted + " - N�O FAZ PARTE DO GRUPO - " + group );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	} // fim do m�todo removeToastedElement( ToastedElement, UpdatableGroup )
	
	
	/**
	 * Invade a praia de farofeiros (fim de jogo).
	 */
	public final void invadeFarofers() {
		//#if DEBUG == "true"
		try {
		//#endif
			
		for ( byte i = 0; i < MAX_ELEMENT_ROWS; ++i ) {
			final UpdatableGroup group = elementRows[ i ];
			
			while ( group.getUsedSlots() < ELEMENTS_PER_ROW_MAX - 1 ) {
				insertElement( i, Element.getRandomElement( ELEMENT_TYPE_FAROFER ) );
			}
		}
		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	}
	
	
	private final void toastElement() {
		// remove personagem atingido, e insere um personagem tostado
		if ( elementHit != null ) {
			final Point refPosition = elementHit.getRefPixelPosition();
			for ( byte i = 0; i < boomGroup.getUsedSlots(); ++i ) {
				if ( boomGroup.getDrawable( i ) == elementHit ) {
					boomGroup.removeDrawable( i );
					break;
				}
			}
			elementHit = null;
			
			// t�tica "suja" para garantir que boomIndex esteja correto, evitando assim problemas de inconsist�ncia entre
			// boomIndex e o �ndice real da anima��o de explos�o no grupo
			for ( byte i = 0; i < boomGroup.getUsedSlots(); ++i ) {
				if ( boomGroup.getDrawable( i ) == boom ) {
					boomIndex = i;
					break;
				}				
			}

			final Element toasted = Element.getToastedElement( this, boomGroup );
			toasted.setRefPixelPosition( refPosition );
			final int toastedIndex = boomGroup.insertDrawable( toasted );
			
			boomGroup.setDrawableIndex( boomIndex, toastedIndex );
			boomIndex = toastedIndex;
		}						
	}
	
	
	private static final class Boom extends Sprite {
		
		/** �ndice do do frame da anima��o de explos�o onde ocorre a troca do elemento pelo elemento tostado */
		private static final byte BOOM_FRAME_TOAST_ELEMENT = 3;
	
		private final Beach beach;
		
		
		private Boom( Beach beach ) {
			super( boomSet );
			
			this.beach = beach;
			setListener( beach, 0 );
			
			defineReferencePixel( size.x >> 1, size.y >> 1 );
		}


		public final void setFrame( int frame ) {
			super.setFrame( frame );

			if ( frameSequenceIndex == BOOM_FRAME_TOAST_ELEMENT )
				beach.toastElement();
		}
		
	} // fim da classe interna Boom

    
    public final void onFrameChanged( int id, int frameSequenceIndex ) {
    }
	
}
