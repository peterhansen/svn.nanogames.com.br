/**
 * ChannelIndicator.java
 * �2007 Nano Games
 *
 * Created on 21/12/2007 12:16:27
 *
 */
//#ifndef LOW_JAR

package core;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class ChannelIndicator extends RichLabel implements Updatable, Constants {
	
	private static final short VISIBLE_TIME = 2700;
	
	private short visibleTime;
	
	
	public ChannelIndicator() throws Exception {
		super( ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_tv.png", PATH_IMAGES + "font_tv.dat" ),
				null, ScreenManager.SCREEN_WIDTH, null );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
	
	
	//#ifndef LOW_JAR
	public final boolean contains( int x, int y ) {
		// sempre retorna false, para evitar que ele intercepte eventos de ponteiros direcionados � soft key direita
		return false;
	}
	//#endif


	public final void update( int delta ) {
		visibleTime -= delta;
		
		if ( visibleTime <= 0 )
			GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, null, true, 0 );
	}


	public final void setText( String text, boolean formatText, boolean autoSize ) {
		super.setText( text == null ? null : GameMIDlet.getText( TEXT_CHANNEL_LEFT ) + text + GameMIDlet.getText( TEXT_CHANNEL_RIGHT ), formatText, autoSize );
		
		GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, this, true, 0 );
		visibleTime = VISIBLE_TIME;
	}
	

}

//#endif