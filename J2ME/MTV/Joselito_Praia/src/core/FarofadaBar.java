/*
 * FarofadaBar.java
 * 
 * Created on 23/12/07 00:25
 * 
 * �2007 Nano Games
 * 
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.MUV;

/**
 *
 * @author peter
 */
public final class FarofadaBar extends DrawableGroup implements Updatable, Constants {
	
	private static final byte TOTAL_ITEMS = 2;
	
	private final DrawableImage bar;
	
	/** largura total da barra de farofada */
	private final short FAROFADA_BAR_WIDTH;	
	
	private final boolean demoMode;
	
	private final MUV speed;
	private short currentFarofada;
	
	
	public FarofadaBar( boolean demo ) throws Exception {
		super( TOTAL_ITEMS );
		
		demoMode = demo;
		if ( demo )
			speed = new MUV( FAROFADA_MAX >> 1 );
		else
			speed = null;
		
		final DrawableImage imgBack = new DrawableImage( PATH_IMAGES + "bar_0.png" );
		insertDrawable( imgBack );		
        
        Thread.yield();
		
		bar = new DrawableImage( PATH_IMAGES + "bar_1.png" );
		insertDrawable( bar );	
        
        Thread.yield();
		
		FAROFADA_BAR_WIDTH = ( short ) bar.getSize().x;
		setSize( FAROFADA_BAR_WIDTH, bar.getSize().y );		
	}
	
	
	public final void setFarofada( int farofada ) {
		bar.setSize( FAROFADA_BAR_WIDTH * farofada / FAROFADA_MAX, size.y );
		
		if ( demoMode )
			currentFarofada = ( short ) farofada;
	}

	
	public final void update( int delta ) {
		if ( demoMode ) {
			currentFarofada += speed.updateInt( delta );
			
			if ( currentFarofada <= 0 || currentFarofada >= FAROFADA_MAX )
				speed.setSpeed( -speed.getSpeed() );
			
			setFarofada( currentFarofada );
		}
	}
}
