/**
 * Constants.java
 * �2007 Nano Games
 *
 * Created on 27/11/2007 17:55:07
 *
 */

package core;

/**
 *
 * �2007 Nano Games
 * @author peter
 */
public interface Constants {
	
	// DEFINI��ES INDEPENDENTES DE TAMANHO DE TELA
	
	/** caminho das imagens do jogo */
	public static final String PATH_IMAGES = "/";
	/** caminho do arquivo descritor dos textos do jogo */
	public static final String PATH_TEXTS = "/pt.dat";
	/** caminho do arquivo descritor dos elementos do jogo */
	public static final String PATH_ELEMENTS_DESCRIPTOR = "/elements.dat";
	/** caminho das imagens dos elementos */
	public static final String PATH_ELEMENTS = PATH_IMAGES + "elements/";
	/** caminho das imagens da tela de splash */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	/** caminho das imagens da tela de splash do jogo */
	public static final String PATH_SPLASH_GAME = PATH_IMAGES + "splash_game/";	
	/** caminho dos frames do cursor */
	public static final String PATH_CURSOR = PATH_IMAGES + "cursor/";
	/** caminho das imagens das marcas */
	public static final String PATH_BRANDS = PATH_IMAGES;	
	/** caminho dos logos das marcas */
	public static final String PATH_LOGOS = PATH_BRANDS + "logo_";	
	/** caminho dos arquivos de som */
	public static final String PATH_SOUNDS = "/";	
	/** caminho dos bal�es de publicidade */
	public static final String PATH_BALLOON = PATH_BRANDS + "balloon_";	
	/** caminho dos elementos de cen�rio (c�u, �gua, areia) */
	public static final String PATH_BACKGROUND = PATH_IMAGES + "bkg/";	
	/** caminho do primeiro cen�rio */
	public static final String PATH_BACKGROUND_0 = PATH_BACKGROUND + "0/";	
	
	//#ifdef C420_VIBRATION_BUG
//# 	/** dura��o da vibra��o ao atingir um farofeiro, em milisegundos */
//# 	public static final byte VIBRATION_TIME = 120;
	//#else
	/** dura��o da vibra��o ao atingir um farofeiro, em milisegundos */
	public static final short VIBRATION_TIME = 250;
	//#endif
	
	
	/** nome da base de dados */
	public static final String DATABASE_NAME = "NANO";
	
	/** �ndice do slot de op��es na base de dados */
	public static final byte DATABASE_SLOT_OPTIONS	= 1;
	
	/** �ndice do slot de recordes de pontos na base de dados */
	public static final byte DATABASE_SLOT_SCORES	= 2;
	
	/** �ndice do slot de marcas na base de dados */
	public static final byte DATABASE_SLOT_BRANDS	= 3;	
	
	//#if DEMO == "true"
//# 	/** Atributo do JAD que mostra o endere�o de compra da vers�o completa. */
//# 	public static final String MIDLET_PROPERTY_URL_BUY_FULL = "FULL_URL";
//# 	
//# 	/** Valor definido no JAD indicando a vers�o do jogo. */
//# 	public static final String MIDLET_PROPERTY_MIDLET_VERSION = "MIDlet-Version";
//# 	
//# 	/** Atributo do jad indicando o texto alternativo a ser utilizado para compra da vers�o completa do jogo. */
//# 	public static final String MIDLET_PROPERTY_BUY_ALT_TEXT = "BUY_ALT_TEXT";
//# 	
//# 	/** Atributo do jad indicando a operadora do jogo - o valor � utilizado para que o servidor saiba para onde
//# 	 * redirecionar o usu�rio. */
//# 	public static final String MIDLET_PROPERTY_CARRIER = "CARRIER";
//# 	
//# 	/** String concatenada � URL para indicar a vers�o do jogo. */
//# 	public static final String BUY_FULL_URL_VERSION = "&version=";
//# 	
//# 	/** String concatenada � URL para indicar a operadora do jogo. */
//# 	public static final String BUY_FULL_URL_CARRIER = "&carrier=";
//# 	
//# 	/** �ndice do slot de marcas na base de dados */
//# 	public static final byte DATABASE_SLOT_DEMO	= 4;		
//# 	
//# 	/** quantidade total de slots da base de dados */
//# 	public static final byte DATABASE_TOTAL_SLOTS	= 4;	
	//#else
	/** quantidade total de slots da base de dados */
	public static final byte DATABASE_TOTAL_SLOTS	= 3;	
	//#endif
	
	
	//#ifdef JAR_100_KB
//# 	// na vers�o de jar menor que 100kb, a m�sica de fundo do jogo tamb�m � utilizada na apresenta��o
//# 	/** m�sica de abertura do jogo */
//# 	public static final byte SOUND_SPLASH		= 0;
//# 	public static final byte SOUND_AMBIENT		= 0;
//# 	public static final byte SOUND_GAME_OVER	= 1;
//# 	
//# 	/** quantidade total de m�sicas e efeitos sonoros do jogo */
//# 	public static final byte SOUND_TOTAL = 2;	
	//#else
	/** m�sica de abertura do jogo */
	public static final byte SOUND_SPLASH		= 0;
	public static final byte SOUND_GAME_OVER	= 1;
	public static final byte SOUND_AMBIENT		= 2;
	
	/** frequ�ncia do "bip" da tela do padr�o de cores */
	public static final byte SOUND_COLOR_PATTERN = 70;
	
	/** quantidade total de m�sicas e efeitos sonoros do jogo */
	public static final byte SOUND_TOTAL = 3;	
	//#endif
	
	//#if BRANDS == "true"
	public static final byte BRAND_COCA_COLA	= 0;
	public static final byte BRAND_OI			= 1;
	public static final byte BRAND_SKOL			= 2;
	public static final byte BRAND_MTV			= 3;	
	
	/** quantidade total de marcas */
	public static final byte BRANDS_TOTAL	= 4;
	//#else
//# 	public static final byte BRAND_MTV			= 3;	
//# 	public static final byte BRAND_NANO			= 4;
	//#endif
	
	/** Cor padr�o do fundo de tela. */
	public static final int BACKGROUND_COLOR = 0x00a000;
	
	// <editor-fold defaultstate="collapsed" desc="telas do jogo">
	// telas do jogo
	public static final byte SCREEN_CHOOSE_SOUND			= 3;
	public static final byte SCREEN_SPLASH_NANO				= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_MTV				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_SPLASH_MTV + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_OPTIONS					= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_CREDITS + 1;
	public static final byte SCREEN_HELP_OBJECTIVES			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS			= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_HELP_TIPS				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_GAME					= SCREEN_HELP_TIPS + 1;
	public static final byte SCREEN_CONTINUE_GAME			= SCREEN_GAME + 1;
	public static final byte SCREEN_NEXT_LEVEL				= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_PAUSE					= SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_CONFIRM_MENU			= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT			= SCREEN_CONFIRM_MENU + 1;
	
	//#if DEMO == "false"
	public static final byte SCREEN_CONFIRM_ERASE_RECORDS	= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_HIGH_SCORES				= SCREEN_CONFIRM_ERASE_RECORDS + 1;
	public static final byte SCREEN_LOADING					= SCREEN_HIGH_SCORES + 1;	
	//#else
//# 	public static final byte SCREEN_BUY_GAME_EXIT			= SCREEN_CONFIRM_EXIT + 1;
//# 	public static final byte SCREEN_BUY_GAME_RETURN			= SCREEN_BUY_GAME_EXIT + 1;
//# 	public static final byte SCREEN_BUY_ALTERNATIVE_EXIT	= SCREEN_BUY_GAME_RETURN + 1;
//# 	public static final byte SCREEN_BUY_ALTERNATIVE_RETURN	= SCREEN_BUY_ALTERNATIVE_EXIT + 1;
//# 	public static final byte SCREEN_PLAYS_REMAINING			= SCREEN_BUY_ALTERNATIVE_RETURN + 1;
//# 	public static final byte SCREEN_LOADING					= SCREEN_PLAYS_REMAINING + 1;
	//#endif
	
	public static final byte SCREEN_FAROFERS				= SCREEN_LOADING + 1;
	public static final byte SCREEN_BRAND					= SCREEN_FAROFERS + 1;
	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG				= SCREEN_BRAND + 1;
	//#endif
	
	// </editor-fold>
	
	
	// <editor-fold desc="�ndices das entradas dos menus">
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	
	//#if DEMO == "true"
//# 	/** �ndice da entrada do menu principal para se comprar a vers�o completa do jogo. */
//# 	public static final byte ENTRY_MAIN_MENU_BUY_FULL_GAME	= 2;
//# 	
//# 	/** N�mero de jogadas iniciais dispon�veis na vers�o demo. */
//# 	public static final byte DEMO_MAX_PLAYS = 10;
//# 	
//# 	/** Quantidade de n�veis na vers�o demo. */
//# 	public static final byte DEMO_MAX_LEVELS = 4;
//# 	
	//#else
	public static final byte ENTRY_MAIN_MENU_RECORDS		= 2;
	//#endif
	
	public static final byte ENTRY_MAIN_MENU_HELP		= 3;
	public static final byte ENTRY_MAIN_MENU_CREDITS	= 4;
	public static final byte ENTRY_MAIN_MENU_EXIT		= 5;
		
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	//#ifdef NO_SOUND
//# 	// tela de pausa
//# 	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
//# 	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 1;
//# 	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 2;
//# 	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 3;
//# 	
//# 	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 1;
//# 	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 2;
//# 	
//# 	// tela de op��es
//# 	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 0;
//# 	public static final byte ENTRY_OPTIONS_MENU_VIB_ERASE_RECORDS		= 1;
//# 	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
//# 	
//# 	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_ERASE_RECORDS	= 0;
//# 	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;	
	//#else
	// tela de pausa
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
		//#if DEMO == "false"
		public static final byte ENTRY_OPTIONS_MENU_VIB_ERASE_RECORDS		= 2;
		public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 3;

		public static final byte ENTRY_OPTIONS_MENU_NO_VIB_ERASE_RECORDS	= 1;
		public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 2;
		//#else
//# 		public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
//# 
//# 		public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
		//#endif
		
	//#endif
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_TIPS						= 2;
	public static final byte ENTRY_HELP_MENU_BACK						= 3;
	
	// </editor-fold>
	
	
	// <editor-fold defaultstate="collapsed" desc="textos do jogo">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_PAUSE							= TEXT_OPTIONS + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_TIPS							= TEXT_CONTROLS + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_HELP_TIPS						= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_CONFIRM_BUY_SHORT				= TEXT_HELP_TIPS + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_CONFIRM_BUY_SHORT + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_MORE_GAMES					= TEXT_CANCEL + 1;	
	public static final byte TEXT_CONTINUE						= TEXT_MORE_GAMES + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_MTV					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_MTV + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_NO							= TEXT_YES + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_NO + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT_1				= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_CONFIRM_EXIT_2				= TEXT_CONFIRM_EXIT_1 + 1;
	public static final byte TEXT_CONFIRM_EXIT_3				= TEXT_CONFIRM_EXIT_2 + 1;
	public static final byte TEXT_CONFIRM_EXIT_4				= TEXT_CONFIRM_EXIT_3 + 1;
	public static final byte TEXT_CONFIRM_EXIT_5				= TEXT_CONFIRM_EXIT_4 + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT_5 + 1;
	public static final byte TEXT_ERROR_LOG						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_ERROR_LOG + 1;
	public static final byte TEXT_RECORDS						= TEXT_GAME_OVER + 1;
	public static final byte TEXT_ERASE_RECORDS					= TEXT_RECORDS + 1;
	public static final byte TEXT_ERASE_RECORDS_CONFIRM			= TEXT_ERASE_RECORDS + 1;
	public static final byte TEXT_PRESS_5_TO_BUY				= TEXT_ERASE_RECORDS_CONFIRM + 1;
	public static final byte TEXT_CONFIRM_BUY					= TEXT_PRESS_5_TO_BUY + 1;
	public static final byte TEXT_SPONGEBOB_BUY_URL				= TEXT_CONFIRM_BUY + 1;
	public static final byte TEXT_BUY_ERROR						= TEXT_SPONGEBOB_BUY_URL + 1;
	public static final byte TEXT_SPONGEBOB						= TEXT_BUY_ERROR + 1;
	public static final byte TEXT_PLAY_AGAIN					= TEXT_SPONGEBOB + 1;
	public static final byte TEXT_LEVEL_END						= TEXT_PLAY_AGAIN + 1;
	public static final byte TEXT_OFFERED_BY					= TEXT_LEVEL_END + 1;
	public static final byte TEXT_COPYRIGHT						= TEXT_OFFERED_BY + 1;
	public static final byte TEXT_COCA_COLA						= TEXT_COPYRIGHT + 1;
	public static final byte TEXT_OI							= TEXT_COCA_COLA + 1;
	public static final byte TEXT_SKOL							= TEXT_OI + 1;
	public static final byte TEXT_MTV							= TEXT_SKOL + 1;
	public static final byte TEXT_ALL_RIGHTS_RESERVED			= TEXT_MTV + 1;
	public static final byte TEXT_CHANNEL						= TEXT_ALL_RIGHTS_RESERVED + 1;
	public static final byte TEXT_LEVEL							= TEXT_CHANNEL + 1;
	public static final byte TEXT_CHANNEL_LEFT					= TEXT_LEVEL + 1;
	public static final byte TEXT_CHANNEL_RIGHT					= TEXT_CHANNEL_LEFT + 1;
	public static final byte TEXT_FAROFERS_TITLE				= TEXT_CHANNEL_RIGHT + 1;
	public static final byte TEXT_FAROFERS_BEGIN				= TEXT_FAROFERS_TITLE + 1;
	public static final byte TEXT_FAROFERS_END					= TEXT_FAROFERS_BEGIN + 1;
	public static final byte TEXT_FAROFERS						= TEXT_FAROFERS_END + 1;
	public static final byte TEXT_BUY_FULL_GAME_TITLE			= TEXT_FAROFERS + 1;
	public static final byte TEXT_BUY_FULL_GAME_TEXT			= TEXT_BUY_FULL_GAME_TITLE + 1;
	public static final byte TEXT_2_OR_MORE_PLAYS_REMAINING_1	= TEXT_BUY_FULL_GAME_TEXT + 1;
	public static final byte TEXT_2_OR_MORE_PLAYS_REMAINING_2	= TEXT_2_OR_MORE_PLAYS_REMAINING_1 + 1;
	public static final byte TEXT_1_PLAY_REMAINING				= TEXT_2_OR_MORE_PLAYS_REMAINING_2 + 1;
	public static final byte TEXT_LOG							= TEXT_1_PLAY_REMAINING + 1;
	
	/** n�mero total de textos do jogo */
	public static final byte TEXT_TOTAL = TEXT_LOG + 1;
	
	public static final byte TEXT_CONFIRM_EXIT_TOTAL = 5;
	// </editor-fold>
	
	/** porcentagem da largura da tela referente � �rea central em que a mira pode ser movida antes que a c�mera 
	 * comece a se mover */
	public static final byte PLAYSCREEN_CAMERA_START_MOVE_PERCENT = 33;
	
	/** n�vel m�ximo de dificuldade */
	public static final byte MAX_LEVEL = 13;
	
	/** n�vel m�ximo da farofada */
	public static final short FAROFADA_MAX = 10000;
	
	/** resultado de um tiro: n�o atingiu nenhum elemento */
	public static final byte FIRE_HIT_NOTHING		= 0;
	/** resultado de um tiro: atingiu um farofeiro */
	public static final byte FIRE_HIT_FAROFER		= 1;
	/** resultado de um tiro: atingiu um n�o-farofeiro */
	public static final byte FIRE_HIT_NON_FAROFER	= 2;
	/** resultado de um tiro: atingiu elemento est�tico */
	public static final byte FIRE_HIT_STATIC		= 3;
	/** resultado de um tiro: n�o pode atirar na posi��o atual */
	public static final byte FIRE_HIT_CANT_SHOOT	= 4;
	/** resultado de um tiro: atingiu um elemento j� tostado */
	public static final byte FIRE_HIT_TOASTED		= 5;
	
	/** tipo de elemento da praia: farofeiro */
	public static final byte ELEMENT_TYPE_FAROFER		= 0;
	/** tipo de elemento da praia: n�o-farofeiro */
	public static final byte ELEMENT_TYPE_NON_FAROFER	= 1;
	/** tipo de elemento da praia: est�tico */
	public static final byte ELEMENT_TYPE_STATIC		= 2;
	/** tipo de elemento da praia: tostado */
	public static final byte ELEMENT_TYPE_TOASTED		= 3;	
	
	/** quantidade total de frames da anima��o do cursor na vers�o de baixa mem�ria */
	public static final byte CURSOR_TOTAL_FRAMES_LOW_MEMORY = 1;
	/** quantidade total de frames da anima��o do cursor na vers�o completa */
 	public static final byte CURSOR_TOTAL_FRAMES_FULL = 8;
	
	/** dura��o de cada frame do cursor em milisegundos */
	public static final byte CURSOR_FRAME_TIME = 127;
	
	/** quantidade m�nima de mem�ria para que o jogo rode com sua capacidade m�xima */
	public static final int LOW_MEMORY_LIMIT = 900000;
	
	//#ifndef LOW_JAR
	/** N�veis completados para que haja troca de cen�rio. Na vers�o demo, o cen�rio � trocado a cada n�vel. */
		//#if DEMO == "false"
		public static final byte CHANGE_BACKGROUND_LEVELS = 4;
		//#else
//# 		public static final byte CHANGE_BACKGROUND_LEVELS = 1;
		//#endif
	//#endif
	
	// DEFINI��ES DEPENDENTES DE TAMANHO DE TELA
	//#if SCREEN_SIZE == "BIG"
//# 		// <editor-fold desc="defini��es da vers�o de tela m�dia">
//# 	
//# 		/** espa�amento vertical entre os itens de menu */
//# 		public static final byte MENU_ITEMS_SPACING = 3;
//# 		
//# 		/** velocidade horizontal da anima��o de background dos menus em pixels por segundo */
//# 		public static final byte BACKGROUND_SPEED_X = 10;
//# 		
//# 		/** velocidade vertical da anima��o de background dos menus em pixels por segundo */
//# 		public static final byte BACKGROUND_SPEED_Y = 10;
//# 
//# 		/** velocidade m�nima de movimenta��o das nuvens, em pixels por segundo */
//# 		public static final byte BEACH_CLOUD_MIN_SPEED = -1;
//# 		/** velocidade m�xima de movimenta��o das nuvens, em pixels por segundo */
//# 		public static final byte BEACH_CLOUD_MAX_SPEED = 28;
//# 		
//# 		/** largura m�nima da praia em pixels */
//# 		public static final short BEACH_WIDTH_MIN = 360;
//# 		
//# 		/** largura m�xima da praia em pixels */
//# 		public static final short BEACH_WIDTH_MAX = 560;
//# 		
//# 		/** Altura de cada fileira de elementos (telas com 320 pixels ou mais de altura). */
//# 		public static final byte BEACH_ELEMENT_ROW_HEIGHT_BIG = 31;
//# 		
//# 		/** Altura de cada fileira de elementos (telas com menos de 320 pixels de altura). */
//# 		public static final byte BEACH_ELEMENT_ROW_HEIGHT_MEDIUM = 17;
//# 
//# 		/** velocidade m�xima da mira em pixels por segundo */
//# 		public static final short CROSSHAIR_MAX_SPEED = 160;
//# 		
//# 		/** velocidade m�nima considerada da mira antes de par�-la */
//# 		public static final byte CROSSHAIR_MIN_SPEED = 18;		
//# 
//# 		/** acelera��o da mira em pixels por segundo ao quadrado */
//# 		public static final short CROSSHAIR_ACCELERATION = CROSSHAIR_MAX_SPEED * 3;
//# 		
//# 		/** velocidade m�xima de movimenta��o da c�mera */
//# 		public static final short PLAYSCREEN_CAMERA_MAX_SPEED = CROSSHAIR_MAX_SPEED * 32 / 10;
//# 		
//# 		/** velocidade m�nima de movimenta��o dos elementos, em pixels (dificuldade inicial) */
//# 		public static final short ELEMENT_MOVE_SPEED_MIN = 27;
//# 		
//# 		/** velocidade m�xima de movimenta��o dos elementos, em pixels (dificuldade m�xima) */
//# 		public static final short ELEMENT_MOVE_SPEED_MAX = 141;
//# 		
//# 		/** offset no posicionamento do cursor em rela��o �s entradas de menu */
//# 		public static final byte CURSOR_OFFSET_X = 17;		
//# 		
//# 		/** offset da fonte padr�o do jogo */
//# 		public static final byte DEFAULT_FONT_OFFSET = -1;
//# 		
//# 		/** altura m�nima da tela para que haja reposicionamento dos personagens na tela de splash do jogo */
//# 		public static final short SPLASH_GAME_MIN_HEIGHT = 208;
//# 		
//# 		// </editor-fold>	
	//#elif SCREEN_SIZE == "MEDIUM"
		// <editor-fold desc="defini��es da vers�o de tela m�dia">
	
		/** espa�amento vertical entre os itens de menu */
		public static final byte MENU_ITEMS_SPACING = 3;
		
		/** velocidade horizontal da anima��o de background dos menus em pixels por segundo */
		public static final byte BACKGROUND_SPEED_X = 10;
		
		/** velocidade vertical da anima��o de background dos menus em pixels por segundo */
		public static final byte BACKGROUND_SPEED_Y = 10;

		/** velocidade m�nima de movimenta��o das nuvens, em pixels por segundo */
		public static final byte BEACH_CLOUD_MIN_SPEED = -1;
		/** velocidade m�xima de movimenta��o das nuvens, em pixels por segundo */
		public static final byte BEACH_CLOUD_MAX_SPEED = 26;
		
		/** largura m�nima da praia em pixels */
		public static final short BEACH_WIDTH_MIN = 352;
		
		/** largura m�xima da praia em pixels */
		public static final short BEACH_WIDTH_MAX = 480;
		
		/** altura de cada fileira de elementos */
		public static final byte BEACH_ELEMENT_ROW_HEIGHT = 13;

		//#ifdef LG
//# 		/** velocidade m�xima da mira em pixels por segundo */
//# 		public static final short CROSSHAIR_MAX_SPEED = 120;
//# 		
//# 		/** velocidade m�nima considerada da mira antes de par�-la */
//# 		public static final byte CROSSHAIR_MIN_SPEED = 19;		
		//#else
		/** velocidade m�xima da mira em pixels por segundo */
		public static final short CROSSHAIR_MAX_SPEED = 137;
		
		/** velocidade m�nima considerada da mira antes de par�-la */
		public static final byte CROSSHAIR_MIN_SPEED = 15;		
		//#endif

		/** acelera��o da mira em pixels por segundo ao quadrado */
		public static final short CROSSHAIR_ACCELERATION = CROSSHAIR_MAX_SPEED * 3;
		
		/** velocidade m�xima de movimenta��o da c�mera */
		public static final short PLAYSCREEN_CAMERA_MAX_SPEED = CROSSHAIR_MAX_SPEED * 32 / 10;
		
		/** velocidade m�nima de movimenta��o dos elementos, em pixels (dificuldade inicial) */
		public static final short ELEMENT_MOVE_SPEED_MIN = 27;
		
		/** velocidade m�xima de movimenta��o dos elementos, em pixels (dificuldade m�xima) */
		public static final byte ELEMENT_MOVE_SPEED_MAX = 116;
		
		/** dimens�o (largura e altura) da �rea de colis�o do tiro */
		public static final byte PLAYSCREEN_FIRE_COLLISION_SIZE = 4;
		
		/** offset no posicionamento do cursor em rela��o �s entradas de menu */
		public static final byte CURSOR_OFFSET_X = 17;		
		
		/** offset da fonte padr�o do jogo */
		public static final byte DEFAULT_FONT_OFFSET = -1;
		
		/** altura m�nima da tela para que haja reposicionamento dos personagens na tela de splash do jogo */
		public static final short SPLASH_GAME_MIN_HEIGHT = 208;
		
		// </editor-fold>
	//#else
//# 		// <editor-fold desc="defini��es da vers�o de tela pequena">
//#  	
//#  		/** espa�amento vertical entre os itens de menu */
//#  		public static final byte MENU_ITEMS_SPACING = 3;
//#  		
//#  		/** velocidade horizontal da anima��o de background dos menus em pixels por segundo */
//#  		public static final byte BACKGROUND_SPEED_X = 10;
//#  		
//#  		/** velocidade vertical da anima��o de background dos menus em pixels por segundo */
//#  		public static final byte BACKGROUND_SPEED_Y = 10;
//#  
//#  		/** velocidade m�nima de movimenta��o das nuvens, em pixels por segundo */
//#  		public static final byte BEACH_CLOUD_MIN_SPEED = -1;
//#  		/** velocidade m�xima de movimenta��o das nuvens, em pixels por segundo */
//#  		public static final byte BEACH_CLOUD_MAX_SPEED = 20;
//#  		
//#  		/** largura m�nima da praia em pixels */
//#  		public static final short BEACH_WIDTH_MIN = 256;
//#  		
//#  		/** largura m�xima da praia em pixels */
//#  		public static final short BEACH_WIDTH_MAX = 380;
//#  		
//#  		/** altura de cada fileira de elementos */
//#  		public static final byte BEACH_ELEMENT_ROW_HEIGHT = 11;
//#  
		//#ifdef LG
//#  		/** velocidade m�xima da mira em pixels por segundo */
//#  		public static final short CROSSHAIR_MAX_SPEED = 120;
//#  		
//#  		/** velocidade m�nima considerada da mira antes de par�-la */
//#  		public static final byte CROSSHAIR_MIN_SPEED = 19;				
		//#else
//#  		/** velocidade m�xima da mira em pixels por segundo */
//#  		public static final short CROSSHAIR_MAX_SPEED = 137;
//#  		
//#  		/** velocidade m�nima considerada da mira antes de par�-la */
//#  		public static final byte CROSSHAIR_MIN_SPEED = 15;				
		//#endif
//#  
//#  		/** acelera��o da mira em pixels por segundo ao quadrado */
//#  		public static final short CROSSHAIR_ACCELERATION = CROSSHAIR_MAX_SPEED * 3;
//#  		
//#  		/** velocidade m�xima de movimenta��o da c�mera */
//#  		public static final short PLAYSCREEN_CAMERA_MAX_SPEED = CROSSHAIR_MAX_SPEED * 32 / 10;
//#  		
//#  		/** velocidade m�nima de movimenta��o dos elementos, em pixels (dificuldade inicial) */
//#  		public static final short ELEMENT_MOVE_SPEED_MIN = 27;
//#  		
//#  		/** velocidade m�xima de movimenta��o dos elementos, em pixels (dificuldade m�xima) */
//#  		public static final byte ELEMENT_MOVE_SPEED_MAX = 93;
//#  		
//#  		/** dimens�o (largura e altura) da �rea de colis�o do tiro */
//#  		public static final byte PLAYSCREEN_FIRE_COLLISION_SIZE = 4;
//#  		
//#  		/** offset no posicionamento do cursor em rela��o �s entradas de menu */
//#  		public static final byte CURSOR_OFFSET_X = 17;		
//#  		
//#  		/** offset da fonte padr�o do jogo */
//#  		public static final byte DEFAULT_FONT_OFFSET = -1;
//# 		
//# 		/** altura m�xima da tela para que haja reposicionamento de itens em algumas telas */
//# 		public static final short SCREEN_HEIGHT_FIT = 132;
//# 		
//# 		// </editor-fold>
	//#endif
	
}
