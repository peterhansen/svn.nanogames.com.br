/**
 * Element.java
 *
 * Created on 27/11/2007 18:44:05
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Vector;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * �2007 Nano Games
 * @author peter
 */
public final class Element extends Sprite implements Constants {

	//#ifdef JAR_100_KB
//# 	private static final byte SEQUENCE_STOPPED_2	= 0;
//# 	private static final byte SEQUENCE_MOVING		= 1;		
//# 	
//#  	/** quantidade total de sequ�ncias de anima��o dos elementos */
//#  	private static final byte TOTAL_SEQUENCES = 2;		
	//#else
 	private static final byte SEQUENCE_STOPPED_1	= 0;
 	private static final byte SEQUENCE_STOPPED_2	= 1;
 	private static final byte SEQUENCE_MOVING		= 2;	
	
 	/** quantidade total de sequ�ncias de anima��o dos elementos */
 	private static final byte TOTAL_SEQUENCES = 3;		
	//#endif
 	
	/** tipo de movimento onde o elemento move-se somente numa dire��o, at� atingir o limite do n�vel. Ele ent�o 
	 * � reposicionado no in�cio do n�vel, e assim segue indefinidamente.
	 */
	private static final byte MOVE_TYPE_UNIDIRECTIONAL	= 0;
	/** tipo de movimento onde a sequ�ncia de dire��es do elemento � pr�-definida */
	private static final byte MOVE_TYPE_PRE_DEFINED		= 1;
	/** demontra��o do elemento (parado) */
	private static final byte MOVE_TYPE_DEMO			= 3;
	
	
	/** tipo de movimento atual do elemento */
	private byte moveType;
	
	/** limites de movimenta��o em cada etapa no modo de movimento pr�-definido */
	private final Movement movement;
	
	private static final byte MOVE_DIRECTION_NONE		= -1;
	private static final byte MOVE_DIRECTION_STOPPED	= 0;
	private static final byte MOVE_DIRECTION_LEFT		= 1;
	private static final byte MOVE_DIRECTION_RIGHT		= 2;
	
	/** chance de se sortear a dire��o de movimento MOVE_DIRECTION_NONE */
	private static final byte MOVE_DIRECTION_NONE_PERCENT = 56;
	
	/** dire��o atual de movimento */
	private byte moveDirection;
	
	/** velocidade do movimento */
	private final MUV moveSpeed = new MUV();
	
	/** limite do movimento atual (posi��o, no caso de MOVE_DIRECTION_LEFT e MOVE_DIRECTION_RIGHT, e tempo, no caso de
	 * MOVE_DIRECTION_NONE)
	 */
	private int moveLimit;
	
	/** tempo m�nimo de espera, em milisegundos (quanto maior a dificuldade, menor o tempo de espera) */
	private static final short WAIT_TIME_MIN = 906;		
	
	/** tempo m�ximo de espera, em milisegundos (quanto maior a dificuldade, menor o tempo de espera) */
	private static final short WAIT_TIME_MAX = 5022;
	
	/** dura��o padr�o de um quadro de anima��o dos elementos */
	private static final byte FRAME_TIME = 127;
	
	private static Sprite[] SPRITES;
	
	//#ifdef JAR_100_KB
//# 	/** frame dos personagens a ser ignorado na vers�o de jar pequeno */
//# 	private static final byte FRAME_IGNORE_LOW_JAR = 4;
	//#endif
	
	/** n�mero total de elementos distintos */
	//#ifdef LOW_JAR
		//#ifdef JAR_100_KB
//# 		private static final byte ELEMENT_TOTAL = 9;
		//#else
//# 		private static final byte ELEMENT_TOTAL = 11;
		//#endif
	//#elif SCREEN_SIZE == "BIG"
//# 	/** Na vers�o com personagens grandes rodando em telas m�dias, deve-se desconsiderar os 2 �ltimos elementos, 
//# 	 * que s�o as barracas de praia grandes, que causam erro na perspectiva caso a dist�ncia entre as fileiras de
//# 	 * elementos n�o seja suficiente.
//# 	 */
	//#ifdef SAMSUNG_BIG
//# 	private static final byte ELEMENT_TOTAL = ( byte ) ( ScreenManager.SCREEN_HEIGHT >= 320 ? 18 : 16 );
	//#else
//# 	private static final byte ELEMENT_TOTAL = ( byte ) ( ScreenManager.SCREEN_HEIGHT >= 320 ? 26 : 24 );
	//#endif
//# 	
	//#else
	private static final byte ELEMENT_TOTAL = 13;
	//#endif
	
	/** �ndices dos flamenguistas (n�o h� espelhamento durante anima��o dele dan�ando).*/
	//#ifndef JAR_100_KB
		//#ifdef LOW_JAR
//# 			private static final byte INDEX_FRAMENGO_1 = 10;
		//#else
			//#if SCREEN_SIZE == "BIG"
				//#ifdef SAMSUNG_BIG
//# 				private static final byte INDEX_FRAMENGO_1 = 3;
				//#else
//# 				private static final byte INDEX_FRAMENGO_1 = 5;
//# 				private static final byte INDEX_FRAMENGO_2 = 23;
				//#endif
			//#else
			private static final byte INDEX_FRAMENGO_1 = 12;
			//#endif
		//#endif
	//#endif
	
	
	/** tipo do elemento */
	private static final byte[] TYPE = new byte[ ELEMENT_TOTAL ];	
	
	/** �ndice do elemento "tostado" */
	private static byte INDEX_TOASTED_ELEMENT;
	
	/** limite do n�vel atual */
	private static int RIGHT_LIMIT;
	
	/** n�vel atual */
	private static short CURRENT_LEVEL;
	
	private final byte index;
	
	private final Beach beach;
	private final UpdatableGroup group;
	
	
	private Element( Beach beach, UpdatableGroup group ) {
		this( INDEX_TOASTED_ELEMENT, false, beach, group );
	}
	
	
	private Element( int index, boolean demo ) {
		this( index, demo, null, null );
	}
	

	private Element( int index, boolean demo, Beach beach, UpdatableGroup group ) {
		super( SPRITES[ index ] );
		
		setSize( SPRITES[ index ].getSize() );
		
		this.index = ( byte ) index;
		
		this.beach = beach;
		this.group = group;		
		
		defineReferencePixel( size.x >> 1, size.y );
		
		if ( demo ) {
			movement = null;
			if ( NanoMath.randInt( 128 ) < 64 )
				mirror( TRANS_MIRROR_H );
			
			setSequence( NanoMath.randInt( 100 ) % TOTAL_SEQUENCES );
			
			moveType = MOVE_TYPE_DEMO;
		} else {
			switch ( TYPE[ index ] ) {
				case ELEMENT_TYPE_STATIC:
					setMoveDirection( MOVE_DIRECTION_NONE );
					movement = null;
				break;

				case ELEMENT_TYPE_FAROFER:
				case ELEMENT_TYPE_NON_FAROFER:
					int random = NanoMath.randInt( 128 );

					bornRandomly();

					if ( random < 30 ) {
						moveType = MOVE_TYPE_UNIDIRECTIONAL;

						// posi��o inicial j� foi definida aleatoriamente; define a dire��o de movimento
						if ( getRefPixelX() < 0 ) {
							setMoveDirection( MOVE_DIRECTION_RIGHT );
							moveLimit = RIGHT_LIMIT + ( size.x >> 1 );
						} else {
							setMoveDirection( MOVE_DIRECTION_LEFT );
							moveLimit = -size.x >> 1;
						}

						movement = null;
					} else {
						moveType = MOVE_TYPE_PRE_DEFINED;

						// posi��o inicial j� foi definida aleatoriamente; define a dire��o de movimento
						if ( getRefPixelX() < 0 )
							setMoveDirection( MOVE_DIRECTION_RIGHT );
						else
							setMoveDirection( MOVE_DIRECTION_LEFT );
						moveSpeed.setSpeed( getRandomSpeed( moveDirection ) );

						movement = new Movement( this );
					}
				break;
				
				case ELEMENT_TYPE_TOASTED:
					//#if SCREEN_SIZE == "BIG"
//# 					final int rand = NanoMath.randInt( 100 );
//# 					if ( rand < 50 ) {
//# 						setMoveDirection( MOVE_DIRECTION_STOPPED );
//# 						moveLimit = sequence.length * frameTime;
//# 					} else if ( rand < 75 ) {
//# 						setMoveDirection( MOVE_DIRECTION_LEFT );
//# 						moveLimit = -size.x >> 1;
//# 					} else {
//# 						setMoveDirection( MOVE_DIRECTION_RIGHT );
//# 						moveLimit = beach.getSize().x + ( size.x >> 1 );
//# 					}					
					//#else
					if ( NanoMath.randInt( 100 ) < 50 ) {
						setMoveDirection( MOVE_DIRECTION_LEFT );
						moveLimit = -size.x >> 1;
					} else {
						setMoveDirection( MOVE_DIRECTION_RIGHT );
						moveLimit = beach.getSize().x + ( size.x >> 1 );
					}
					//#endif

					moveSpeed.setSpeed( getRandomSpeed( moveDirection ) + getRandomSpeed( moveDirection ) );					

				default:
					movement = null;
			} // fim switch ( TYPE[ index ] )
		}
	}
	
	
	private final void setMoveDirection( byte direction ) {
		if ( moveDirection != direction ) {
			moveDirection = direction;
			
			switch ( direction ) {
				case MOVE_DIRECTION_STOPPED:
					//#ifdef JAR_100_KB
//# 					setSequence( SEQUENCE_STOPPED_2 );
					//#else
					setSequence( NanoMath.randInt( 128 ) < 32 ? SEQUENCE_STOPPED_1 : SEQUENCE_STOPPED_2 );
					//#endif
				break;
				
				case MOVE_DIRECTION_LEFT:
					setSequence( SEQUENCE_MOVING );
					moveSpeed.setSpeed( getRandomSpeed( direction ) );
					setTransform( TRANS_NONE );
				break;
				
				case MOVE_DIRECTION_RIGHT:
					setSequence( SEQUENCE_MOVING );
					moveSpeed.setSpeed( getRandomSpeed( direction ) );
					setTransform( TRANS_MIRROR_H );
				break;
			}
		}
	}
	
	
	public static final Element getRandomElement( byte type ) {
		int index;
		do {
			index = NanoMath.randInt( ELEMENT_TOTAL );
		} while ( TYPE[ index ] != type );
		
		return new Element( index, false );
	}
	
	
	public static final Element getToastedElement( Beach beach, UpdatableGroup group ) {
		// esse tipo de construtor � usado somente para criar um novo elemento tostado
		return new Element( beach, group );
	}
	
	
	/**
	 * Obt�m um array com todos os elementos de um determinado tipo.
	 * @param type tipo de elemento.
	 * @return array contendo 1 inst�ncia de cada elemento do tipo desejado.
	 */
	public static final Element[] getElements( byte type ) {
		final Vector elements = new Vector();
		
		for ( byte i = 0; i < ELEMENT_TOTAL; ++i ) {
			if ( TYPE[ i ] == type ) {
				final Element e = new Element( i, true );
				e.setMoveDirection( MOVE_DIRECTION_STOPPED );
				e.moveType = MOVE_TYPE_DEMO;
				elements.addElement( e );
			}
		}
		
		final Element[] ret = new Element[ elements.size() ];
		for ( byte i = 0; i < ret.length; ++i )
			ret[ i ] = ( Element ) elements.elementAt( i );
		
		return ret;
	}
	
	
	public static final Element getStaticElement() {
		int index;
		int tries = 0;
		do {
			index = NanoMath.randInt( ELEMENT_TOTAL );
			++tries;
			if ( tries > 8 ) {
				while ( TYPE[ index ] != ELEMENT_TYPE_STATIC ) {
					index = ( index + 1 ) % ELEMENT_TOTAL;
				}
				break;
			}
		} while ( TYPE[ index ] != ELEMENT_TYPE_STATIC );
		
		return new Element( index, false );
	}
	
	
	public void update( int delta ) {
		super.update( delta );
		
		switch ( moveDirection ) {
			case MOVE_DIRECTION_STOPPED:
				moveLimit -= delta;
				if ( moveLimit <= 0 )
					endMove();
			break;
			
			case MOVE_DIRECTION_LEFT:
				move( moveSpeed.updateInt( delta ), 0 );
				
				if ( getRefPixelX() <= moveLimit )
					endMove();
			break;
			
			case MOVE_DIRECTION_RIGHT:
				move( moveSpeed.updateInt( delta ), 0 );
				
				if ( getRefPixelX() >= moveLimit )
					endMove();				
			break;
		} // fim switch ( moveDirection )
	} // fim do m�todo update( int )
	
	
	public final byte getType() {
		return TYPE[ index ];
	}
	
	
	private final void endMove() {
		if ( TYPE[ index ] == ELEMENT_TYPE_TOASTED ) {
			beach.removeToastedElement( this, group );
		} else {
			switch ( moveType ) {
				case MOVE_TYPE_UNIDIRECTIONAL:
					switch ( moveDirection ) {
						case MOVE_DIRECTION_LEFT:
							setRefPixelPosition( RIGHT_LIMIT, getRefPixelY() );
						break;

						case MOVE_DIRECTION_RIGHT:
							setRefPixelPosition( -size.x >> 1, getRefPixelY() );
						break;
					}
				break;

				case MOVE_TYPE_PRE_DEFINED:
					movement.nextMove();
				break;
			} // fim switch ( moveType )
		}
	} // fim do m�todo endMove()
	
	
	public static final void setInfo( int level, int rightLimit  ) {
		CURRENT_LEVEL = ( short ) level;
		RIGHT_LIMIT = rightLimit;
	}
	

	private static final short getRandomWaitTime() {
		return ( short ) ( WAIT_TIME_MIN + NanoMath.randInt( 1 + WAIT_TIME_MAX * ( MAX_LEVEL - CURRENT_LEVEL ) / MAX_LEVEL ) );
	}
	
	
	private static final int getRandomSpeed( byte direction ) {
		final int speed = ELEMENT_MOVE_SPEED_MIN + NanoMath.randInt( ELEMENT_MOVE_SPEED_MAX * CURRENT_LEVEL / MAX_LEVEL );
		return direction == MOVE_DIRECTION_RIGHT ? speed : - speed;
	}
	
	
	public final void setSequence( int sequence ) {
		super.setSequence( sequence );
		
		//#ifndef JAR_100_KB
		switch ( index ) {
			case INDEX_FRAMENGO_1:
				
			// na vers�o Samsung Big, s� h� um flamenguista
			//#ifndef SAMSUNG_BIG
				//#if SCREEN_SIZE == "BIG"
//# 				case INDEX_FRAMENGO_2:
				//#endif
			//#endif
				switch ( sequence ) {
					case SEQUENCE_STOPPED_2:
						setTransform( TRANS_NONE );
					break;
				}
			break;
		}
		//#endif
	}
	

	/**
	 * Sorteia o movimento de entrada do elemento no n�vel (pode vir da esquerda ou da direita).
	 */
	private final void bornRandomly() {
		if ( NanoMath.randInt( 128 ) < 64 ) {
			// entra pela esquerda
			setRefPixelPosition( -size.x >> 1, getRefPixelY() );
			setMoveDirection( MOVE_DIRECTION_RIGHT );
		} else {
			// entra pela direita
			setRefPixelPosition( RIGHT_LIMIT + ( size.x >> 1 ), getRefPixelY() );
			setMoveDirection( MOVE_DIRECTION_LEFT );
		}
		
		moveSpeed.setSpeed( getRandomSpeed( moveDirection ) );
	}
	

	// <editor-fold defaultstate="collapsed" desc="m�todos loadFrameSets() e unloadFrameSets()">
	public static final void loadFrameSets() throws Exception {
		//#if DEBUG == "true"
		try {
		//#endif
			
		if ( SPRITES == null ) {
			SPRITES = new Sprite[ ELEMENT_TOTAL ];
			
			GameMIDlet.openJarFile( PATH_ELEMENTS_DESCRIPTOR, new Serializable() {

				public final void write( DataOutputStream output ) throws Exception {
				}


				public final void read( DataInputStream input ) throws Exception {
					// l� a descri��o dos elementos do jogo
					
					// etapas da leitura dos personagens
					final byte READ_FILENAME			= 0;
					final byte READ_TYPE				= 1;
					
					// na vers�o de tela grande, a descri��o das anima��es dos personagens � feita dentro do arquivo
					// .dat de cada personagem
					//#if SCREEN_SIZE == "BIG"
//# 					final byte READ_SIZE				= 2;
//# 					
//# 					/** Dimens�es totais originais do sprite (para fazer o posicionamento das anima��es). */
//# 					final Point totalSize = new Point();
					//#else
					final byte READ_TOTAL_FRAMES		= 2;
					final byte READ_SEQUENCE_STOPPED_1	= 3;
					final byte READ_SEQUENCE_STOPPED_2	= 4;
					final byte READ_SEQUENCE_MOVING		= 5;
					
					final Vector vectorStopped2 = new Vector();
					final Vector vectorMoving = new Vector();
					byte totalFrames = 0;
					
					final short[] frameTimes = new short[ TOTAL_SEQUENCES ];
					for ( byte i = 0; i < TOTAL_SEQUENCES; ++i ) {
						frameTimes[ i ] = FRAME_TIME;
					}					
					
					// sequ�ncia atual de frames
						//#ifndef JAR_100_KB
						final Vector vectorStopped1 = new Vector();
						//#endif
					
					//#endif
					
					byte readState = READ_FILENAME;
					
					byte currentElement = 0;
					final StringBuffer buffer = new StringBuffer();
					
					String filename = null;			
					
					do { 
						final char c = ( char ) input.readUnsignedByte();
						//#if DEBUG == "true"
						System.out.println( "c: " + c );
						//#endif						
						
						switch ( c ) {
							
 							case ' ':
 								if ( buffer.length() > 0 ) {
 									switch ( readState ) {
									//#if SCREEN_SIZE == "BIG"
//# 										case READ_SIZE:
//# 											totalSize.x = Integer.valueOf( buffer.toString() ).intValue();
//# 										break;
									//#else
										case READ_SEQUENCE_STOPPED_1:
											//#ifndef JAR_100_KB
 											vectorStopped1.addElement( Integer.valueOf( buffer.toString() ) );
											//#endif
										break;										
										
										case READ_SEQUENCE_STOPPED_2:
											vectorStopped2.addElement( Integer.valueOf( buffer.toString() ) );
										break;
										
										case READ_SEQUENCE_MOVING:
											vectorMoving.addElement( Integer.valueOf( buffer.toString() ) );
										break;
							//#endif										
										
										//#if DEBUG == "true"
										default:
											throw new Exception( "estado de leitura inv�lido ao ler sequ�ncia: " + readState );
										//#endif
 									} // fim switch ( readState )
 									buffer.delete( 0, buffer.length() );
 								} // fim if ( buffer.length() > 0 )
 							break;

							
							case '\t':
							case '\n':
							case '\r':
								if ( buffer.length() > 0 ) {
									switch ( readState ) {
										case READ_FILENAME:
											buffer.insert( 0, PATH_ELEMENTS );
											filename = buffer.toString();
										break;
										
										case READ_TYPE:
											final char type = buffer.toString().charAt( 0 );
											switch ( type ) {
												case 'f':
												case 'F':
													TYPE[ currentElement ] = ELEMENT_TYPE_FAROFER;
												break;
												
												case 'n':
												case 'N':
													TYPE[ currentElement ] = ELEMENT_TYPE_NON_FAROFER;
												break;
												
												case 'e':
												case 'E':
													TYPE[ currentElement ] = ELEMENT_TYPE_STATIC;
												break;
												
												case 't':
												case 'T':
													TYPE[ currentElement ] = ELEMENT_TYPE_TOASTED;
													INDEX_TOASTED_ELEMENT = currentElement;
												break;
												
												//#if DEBUG == "true"
												default:
													throw new Exception( "tipo de elemento inv�lido: "+  type );
												//#endif
											}
										break;
										
										//#if SCREEN_SIZE == "BIG"
//# 										
//# 										case READ_SIZE:
//# 											// l� a altura original do sprite
//# 											totalSize.y = Integer.valueOf( buffer.toString() ).intValue();
//# 										break;
//# 										
										//#else
										
										case READ_TOTAL_FRAMES:
											totalFrames = Integer.valueOf( buffer.toString() ).byteValue();
										break;
										
										case READ_SEQUENCE_STOPPED_1:
											//#ifndef JAR_100_KB
 											vectorStopped1.addElement( Integer.valueOf( buffer.toString() ) );
											//#endif
										break;										
										
										case READ_SEQUENCE_STOPPED_2:
											vectorStopped2.addElement( Integer.valueOf( buffer.toString() ) );
										break;
										
										case READ_SEQUENCE_MOVING:
											vectorMoving.addElement( Integer.valueOf( buffer.toString() ) );
										break;
										
										//#endif
										
										//#if DEBUG == "true"
										default:
											throw new Exception( "estado de leitura inv�lido: " + readState );
										//#endif
									} // fim switch ( readState )
									
									buffer.delete( 0, buffer.length() );
									
									++readState;
									//#if DEBUG == "true"
									System.out.println( "readState: " + readState );
									//#endif

									//#if SCREEN_SIZE == "BIG"
//# 									if ( readState > READ_SIZE ) {
//# 										// carrega o sprite a partir do arquivo descritor
//# 										SPRITES[ currentElement ] = new Sprite( filename + ".dat", filename );	
//# 										
//# 										// define o tamanho original do sprite, para evitar que haja cortes no desenho
//# 										// causados pelos offsets de cada frame
//# 										SPRITES[ currentElement ].setSize( totalSize );
//# 										
//# 										readState = READ_FILENAME;
//# 										++currentElement;
//# 										
//# 										// como o carregamento dos personagens � feito numa thread separada, permite a
//# 										// execu��o de outras threads para evitar longos per�odos de pausa durante as
//# 										// telas de splash
//# 										if ( ( currentElement & 3 ) == 0 )
//# 											Thread.yield();
//# 										
										//#if DEBUG == "true"
//# 										System.out.println( "currentElement: " + currentElement );
										//#endif													
//# 									}
									//#else
									if ( readState > READ_SEQUENCE_MOVING ) {
										readState = READ_FILENAME;
										
										// terminou de ler a descri��o de um elemento; carrega as imagens e anima��es
										final Image[] frames = new Image[ totalFrames ];
										final Point size = new Point();
										for ( byte i = 0; i < totalFrames; ++i ) {
											//#if DEBUG == "true"
											System.out.print( "path: " + ( filename + i + ".png" ) + " ..." );
											//#endif
											
											//#ifdef JAR_100_KB
//# 											// na vers�o de jar pequeno, ignora o frame do personagem parado
//# 											if ( i == FRAME_IGNORE_LOW_JAR ) {
//# 												frames[ i ] = frames[ 0 ];
//# 												continue;
//# 											}
											//#endif
											
											final Image img = ImageLoader.loadImage( filename + i + ".png" );
											frames[ i ] = img;
											if ( img.getWidth() > size.x )
												size.x = img.getWidth();
											if ( img.getHeight() > size.y )
												size.y = img.getHeight();
                                            
                                            Thread.yield();
											
											//#if DEBUG == "true"
											System.out.println( " OK" );
											//#endif											
										}
										
										final byte[][] sequences = new byte[ TOTAL_SEQUENCES ][];
										byte[] sequence;
										
										// adiciona a sequ�ncia do elemento parado (exceto na vers�o de jar pequeno)
										
										//#ifndef JAR_100_KB
 										sequences[ SEQUENCE_STOPPED_1 ] = new byte[ vectorStopped1.size() ];
 										sequence = sequences[ SEQUENCE_STOPPED_1 ];
 										for ( byte i = 0; i < sequence.length; ++i )
 											sequence[ i ] = ( ( Integer ) ( vectorStopped1.elementAt( i ) ) ).byteValue();
										//#endif
										
										// adiciona a sequ�ncia do elemento parado (com anima��o)
										sequences[ SEQUENCE_STOPPED_2 ] = new byte[ vectorStopped2.size() ];
										sequence = sequences[ SEQUENCE_STOPPED_2 ];
										for ( byte i = 0; i < sequence.length; ++i )
											sequence[ i ] = ( ( Integer ) ( vectorStopped2.elementAt( i ) ) ).byteValue();
										
										// adiciona a sequ�ncia do elemento se movendo
										sequences[ SEQUENCE_MOVING ] = new byte[ vectorMoving.size() ];
										sequence = sequences[ SEQUENCE_MOVING ];
										for ( byte i = 0; i < sequence.length; ++i )
											sequence[ i ] = ( ( Integer ) ( vectorMoving.elementAt( i ) ) ).byteValue();
										
										// pr�-define os offsets de cada frame
										final Point[] offsets = new Point[ frames.length ];
										for ( int i = 0; i < offsets.length; ++i ) {
											final Image frame = frames[ i ];
											offsets[ i ] = new Point( ( size.x - frame.getWidth() >> 1 ), size.y - frame.getHeight() );
										}					
										
										SPRITES[ currentElement ] = new Sprite( frames, offsets, sequences, frameTimes );

										++currentElement;
										// como o carregamento dos personagens � feito numa thread separada, permite a
										// execu��o de outras threads para evitar longos per�odos de pausa durante as
										// telas de splash
										if ( ( currentElement & 3 ) == 0 )
											Thread.yield();	
										//#if DEBUG == "true"
										System.out.println( "currentElement: " + currentElement );
										//#endif													
										
										//#ifndef JAR_100_KB
 										vectorStopped1.removeAllElements();
										//#endif
										
										vectorStopped2.removeAllElements();
										vectorMoving.removeAllElements();
									} // fim if ( readState > READ_SEQUENCE_MOVING )
									//#endif
									
								} // fim if ( buffer.length() > 0 )
							break;
							
							default:
								buffer.append( c );
						} // fim switch ( c )
						
                        Thread.yield();
					} while ( currentElement < ELEMENT_TOTAL );
				} // fim do m�todo read( DataInputStream )
			} );
		} // fim if ( FRAMESETS == null )
		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
			throw e;
		}
		//#endif
	} // fim do m�todo loadFrameSets()
	
	// </editor-fold>
	

	// <editor-fold defaultstate="collapsed" desc="classe interna descritora de movimento pr�-definido">
	
	private static final class Movement {
		
		/** limites de posi��o ou tempo de cada movimento */
		private final short[] movementLimits;
		
		/** velocidade de cada movimento */
		private final int[] movementSpeeds;
		
		/** �ndice do movimento atual */
		private byte movementIndex;
		
		/** refer�ncia para o elemento cujo movimento ser� definido */
		private final Element element;
		
		/** indica se o �ndice est� crescendo ou decrescendo */
		private boolean increaseIndex = true;
		
		
		public Movement( Element e ) {
			element = e;
			
			// valor utilizado para evitar que o elemento saia da tela
			final int ELEMENT_WIDTH = element.getSize().x;
			
			// varia��o m�nima de posi��o a cada movimento
			final int MIN_MOVEMENT = ELEMENT_WIDTH << 3;			
			
			// quantidade m�nima de movimentos
			final byte MIN_MOVEMENTS = 3;
			
			// varia��o na quantidade m�xima de movimentos
			final byte MAX_MOVEMENTS_DIFF = 7;
			
			final byte movements = ( byte ) ( MIN_MOVEMENTS + NanoMath.randInt( MAX_MOVEMENTS_DIFF ) );
			
			movementLimits = new short[ movements ];
			movementSpeeds = new int[ movements ];
			
			int nonStopMovements;
			
			do {
				nonStopMovements = 0;
				
				// �ltima posi��o x, utilizada para calcular a varia��o m�xima de posi��o em cada movimento
				int x = ( RIGHT_LIMIT >> 1 ) - ( ELEMENT_WIDTH << 1 ) + NanoMath.randInt( ELEMENT_WIDTH << 2 );
				
				// define o primeiro limite inicial
				element.moveLimit = x;				
				
				final int ELEMENT_RIGHT_LIMIT = RIGHT_LIMIT - ( ELEMENT_WIDTH << 1 );
				final int ELEMENT_LEFT_LIMIT = ELEMENT_WIDTH >> 1;
				
				for ( byte i = 0; i < movements; ++i ) {
					if ( NanoMath.randInt( 100 ) < MOVE_DIRECTION_NONE_PERCENT ) {
						// personagem p�ra
						movementLimits[ i ] = getRandomWaitTime();
						movementSpeeds[ i ] = 0;
					} else {
						if ( x >= ELEMENT_RIGHT_LIMIT || 
							( x > ELEMENT_LEFT_LIMIT && NanoMath.randInt( 128 ) < ( x * 128 / RIGHT_LIMIT ) ) ) {
							// personagem anda para a esquerda
							++nonStopMovements;
							movementSpeeds[ i ] = getRandomSpeed( MOVE_DIRECTION_LEFT );
							x -= MIN_MOVEMENT + NanoMath.randInt( x );
							if ( x < ELEMENT_LEFT_LIMIT )
								x = ELEMENT_LEFT_LIMIT;
							
							movementLimits[ i ] = ( short ) x;
						} else {
							// personagem anda para a direita
							++nonStopMovements;
							movementSpeeds[ i ] = getRandomSpeed( MOVE_DIRECTION_RIGHT );
							x += MIN_MOVEMENT + NanoMath.randInt( RIGHT_LIMIT - x );
							if ( x > ELEMENT_RIGHT_LIMIT )
								x = ELEMENT_RIGHT_LIMIT;
							
							movementLimits[ i ] = ( short ) x;							
						}
					}
				} // fim for ( byte i = 0; i < movements; ++i )
			} while ( nonStopMovements < 2 );
			
			//#if DEBUG == "true"
			System.out.println( this + " ->movimentos:" );
			for ( int i = 0; i < movementLimits.length; ++i ) {
				System.out.println( "\t" + i + ": " + movementLimits[ i ] + ", " + movementSpeeds[ i ] );
			}
			//#endif
			
		} // fim do construtor


		private final void nextMove() {
			if ( increaseIndex ) {
				if ( movementIndex + 1 < movementLimits.length )
					++movementIndex;
				else
					increaseIndex = false;
			} else {
				if ( movementIndex > 0 )
					--movementIndex;
				else
					increaseIndex = true;
			}
			
			element.moveSpeed.setSpeed( increaseIndex ? movementSpeeds[ movementIndex ] : -movementSpeeds[ movementIndex ] );
			
			if ( element.moveSpeed.getSpeed() == 0 )
				element.setMoveDirection( MOVE_DIRECTION_STOPPED );
			else if ( element.moveSpeed.getSpeed() > 0 )
				element.setMoveDirection( MOVE_DIRECTION_RIGHT );
			else
				element.setMoveDirection( MOVE_DIRECTION_LEFT );
			
			element.moveLimit = movementLimits[ movementIndex ];
		} // fim do m�todo nextMove()
		
	} // fim da classe interna Movement
	
	// </editor-fold>	
	
}
