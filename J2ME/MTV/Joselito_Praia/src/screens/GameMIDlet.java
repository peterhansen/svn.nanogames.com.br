/**
 * GameMIDlet.java
 *
 * Created on 27/11/2007 17:32:30
 *
 */

package screens;

import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.Beach;
import core.Constants;
import core.Element;
import core.FarofadaBar;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDletStateChangeException;

//#ifndef LOW_JAR
import core.ChannelIndicator;
//#endif

/**
 *
 * �2007 Nano Games
 * @author peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener 
															//#if DEMO == "true"
//# 															, Serializable
															//#endif
																					{
	
	//#ifdef LOW_JAR
//# 	private static final byte MAX_GAME_FRAME_TIME = 117;
	//#else
	private static final byte MAX_GAME_FRAME_TIME = 100;
	//#endif
	
	/** gerenciador da anima��o da soft key esquerda */
	private final BasicAnimatedSoftkey softkeyLeft;	
	
	/** gerenciador da anima��o da soft key direita */
	private final BasicAnimatedSoftkey softkeyRight;
	
	/** refer�ncia para a tela de jogo */
	private static PlayScreen playScreen;
	
	/** fonte padr�o do jogo */
	private static ImageFont FONT_DEFAULT;
	
	//#ifndef LOW_JAR
	// na vers�o de jar pequeno, � utilizada uma cor s�lida devido a limita��es de processamento
 	private static BasicAnimatedPattern background;
	
	private static TransitionTV transitionTV;
	
	private static ChannelIndicator channel;
	//#endif
	
	/** Indica se os recursos do jogo estao carregados. */
	private boolean resourcesLoaded;
	
	private static final byte BACKGROUND_TYPE_NONE		= 0;
	private static final byte BACKGROUND_TYPE_PATTERN	= 1;
	
	//#ifndef LOW_JAR
	private static boolean lowMemory;
	//#endif
	
	/** armazena o �ltimo momento em que houve uma troca de tela */
	private static long lastScreenChange;
	private static final short MIN_INTERVAL_BETWEEN_SCREEN_CHANGE = 300;
	
	/** Tempo m�nimo da tela de farofeiros. */
	private static final short MIN_FAROFERS_SCREEN_TIME = 1800;
	
	private static long lastFarofersScreenTime;
	
	//#if DEMO == "true"
//# 	/** N�mero de jogadas restantes na vers�o de demonstra��o. */
//# 	private static byte playsRemaining = DEMO_MAX_PLAYS;
	//#endif
	
	/** Armazena o instante da �ltima op��o selecionada para continuar jogo (evita que v�rias chamadas num curto intervalo
	 * causem problema ao entrar na tela de jogo (congelamento).
	 */
	private static long lastContinueGameTime;
	
	/** Intervalo m�nimo entre 2 confirma��es da op��o de continuar jogo. */
	private static final short MIN_CONTINUE_GAME_INTERVAL = 500;
	
	
	public GameMIDlet() throws Exception {
		//#ifdef SWITCH_SOFT_KEYS
//# 		super( VENDOR_SAGEM_GRADIENTE, MAX_GAME_FRAME_TIME );
		//#endif
		
		softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
		softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );
		currentScreen = -1;
		
		loadTexts( TEXT_TOTAL, PATH_TEXTS );
	}


	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( playScreen != null ) {
			//#if DEMO == "false"
			HighScoresScreen.setScore( playScreen.getScore() );
			//#endif
			
			playScreen = null;
		}
		//#if DEMO == "true"
//# 		try {
//# 			saveData( DATABASE_NAME, DATABASE_SLOT_DEMO, this );
//# 		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 		}
		//#endif
		
		super.destroyApp( unconditional );
	}
	
	
	protected final void loadResources() throws Exception {
		//#ifndef LOW_JAR
		switch ( getVendor() ) {
			case VENDOR_SONYERICSSON:
			case VENDOR_NOKIA:
			case VENDOR_SIEMENS:
			break;

			default:
				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
		//#endif

		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}
		
		// na vers�o demo, carrega a quantidade de jogadas restantes
		//#if DEMO == "true"
//# 		try {
//# 			loadData( DATABASE_NAME, DATABASE_SLOT_DEMO, this );
//# 		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 		}
		//#endif

		FONT_DEFAULT = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
		FONT_DEFAULT.setCharOffset( DEFAULT_FONT_OFFSET );
        
        Thread.yield();

		//#ifndef LOW_JAR
			if ( !isLowMemory() )
				background = new BasicAnimatedPattern( new DrawableImage( PATH_IMAGES + "menu.png" ), BACKGROUND_SPEED_X, BACKGROUND_SPEED_Y );		
        
        Thread.yield();
		
		transitionTV = new TransitionTV( manager );
		transitionTV.setSize( transitionTV.getSize().x, transitionTV.getSize().y * 13 / 10 );
		channel = new ChannelIndicator();
		//#endif

		//#ifdef NO_SOUND
//# 				MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, null );
//# 				MediaPlayer.setMute( true );
		//#else
		final String[] SOUNDS = new String[ SOUND_TOTAL ];
		for ( byte i = 0; i < SOUND_TOTAL; ++i )
			SOUNDS[ i ] = PATH_SOUNDS + i + ".mid";
		MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, SOUNDS );
		//#endif
		
		manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
		manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );		
		
		//#ifdef NO_SOUND
//# 				setScreen( SCREEN_SPLASH_NANO );
		//#else
			//#if DEMO == "true"
//# 			// caso seja a vers�o demo e o n�mero de jogadas tenha expirado, mostra a tela de compra da vers�o completa
//# 			if ( playsRemaining <= 0 )
//# 				setScreen( SCREEN_BUY_GAME_EXIT );
//# 			else
			//#endif
			setScreen( SCREEN_CHOOSE_SOUND );
		//#endif
	}
	
	
	public synchronized final int changeScreen( int index ) {
		//#if DEBUG == "true"
		System.out.print( "setScreen( " + index + ")..." );
		//#endif
		
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		// garante um intervalo m�nimo entre trocas de tela, evitando problemas em alguns aparelhos Samsung (como o D820)
		final long interval = System.currentTimeMillis() - lastScreenChange;
		if ( interval < MIN_INTERVAL_BETWEEN_SCREEN_CHANGE ) {
			try {
				Thread.sleep( MIN_INTERVAL_BETWEEN_SCREEN_CHANGE );
			} catch ( InterruptedException ex ) {
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
		}
		lastScreenChange = System.currentTimeMillis();
		
		if ( index != currentScreen ) {
			Drawable nextScreen = null;
			
			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;
			
			byte bkgType = BACKGROUND_TYPE_PATTERN;
			
			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;
			
			//#ifndef LOW_JAR
			String channelText = getText( TEXT_CHANNEL ) + String.valueOf( index );
			
			boolean useTvTransition = true;
			//#endif
			
			try {
				switch ( index ) {
					case SCREEN_CHOOSE_SOUND:
						final BasicConfirmScreen confirm = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, getCursor(), TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO );
						if ( !MediaPlayer.isMuted() )
							confirm.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
						
						nextScreen = confirm;
						indexSoftLeft = TEXT_OK;
						
						updateBackground();
					break;

					case SCREEN_SPLASH_NANO:
						updateBackground();				

						//#if DEMO == "false"
						HighScoresScreen.createInstance( FONT_DEFAULT );
						//#endif

						bkgType = BACKGROUND_TYPE_NONE;
						//#if SCREEN_SIZE == "SMALL"
//# 						nextScreen = new BasicSplashNano( SCREEN_SPLASH_MTV, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_SPLASH );
						//#else
						nextScreen = new BasicSplashNano( SCREEN_SPLASH_MTV, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_SPLASH );
						//#endif
					break;
					
					case SCREEN_SPLASH_MTV:
						bkgType = BACKGROUND_TYPE_NONE;
						nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_LOGOS + BRAND_MTV + ".png", TEXT_SPLASH_MTV );
					break;
					
					case SCREEN_SPLASH_GAME:
						nextScreen = new SplashGame( FONT_DEFAULT );
					break;
					
					//#if DEMO == "true"
//# 					
//# 					case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 					case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 						final String buyAlternativeText = midlet.getAppProperty( MIDLET_PROPERTY_BUY_ALT_TEXT  );
//# 						
//# 						if ( buyAlternativeText == null ) {
//# 							// n�o encontrou o texto para compra alternativa do jogo
//# 							if ( index == SCREEN_BUY_ALTERNATIVE_RETURN ) {
//# 								index = SCREEN_MAIN_MENU;
//# 							} else {
//# 								midlet.unlockChangeScreen();
//# 								exit();
//# 							}
//# 						} else {
//# 							nextScreen = new BasicTextScreen( midlet, index, FONT_DEFAULT, "<ALN_H>" + buyAlternativeText, false );
//# 							indexSoftLeft = TEXT_OK;
//# 							break;
//# 						}
//# 						
					//#endif

					case SCREEN_MAIN_MENU:
						playScreen = null;
						System.gc();
						
						nextScreen = createBasicMenu( index, new int[] {
							TEXT_NEW_GAME,
							TEXT_OPTIONS,
							
							//#if DEMO == "true"
//# 							TEXT_BUY_FULL_GAME_TITLE,
							//#else
							TEXT_RECORDS,
							//#endif
							
							TEXT_HELP,
							TEXT_CREDITS,
							
							TEXT_EXIT,
							
							//#if DEBUG == "true"
							TEXT_ERROR_LOG,
							//#endif							
							} );
						
						indexSoftLeft = TEXT_OK;
						indexSoftRight = TEXT_EXIT;
					break;
					
					case SCREEN_LOADING:
						nextScreen = new LoadScreen( midlet );
					break;
					
					case SCREEN_OPTIONS:
						//#ifdef NO_SOUND
//# 						if ( MediaPlayer.isVibrationSupported() ) {
//# 							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
//# 								TEXT_TURN_VIBRATION_OFF,
								//#if DEMO == "false"
//# 								TEXT_ERASE_RECORDS,
								//#endif
//# 								TEXT_BACK,
//# 							}, ENTRY_OPTIONS_MENU_VIB_BACK, -1, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
//# 						} else {
//# 							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
								//#if DEMO == "false"
//# 								TEXT_ERASE_RECORDS,
								//#endif
//# 								TEXT_BACK,
//# 							}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, -1, TEXT_TURN_SOUND_ON, -1, -1 );							
//# 						}
						//#else
						if ( MediaPlayer.isVibrationSupported() ) {
							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
								TEXT_TURN_SOUND_OFF,
								TEXT_TURN_VIBRATION_OFF,
								//#if DEMO == "false"
								TEXT_ERASE_RECORDS,
								//#endif
								TEXT_BACK,
							}, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
						} else {
							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
								TEXT_TURN_SOUND_OFF,
								//#if DEMO == "false"
								TEXT_ERASE_RECORDS,
								//#endif
								TEXT_BACK,
							}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
						}
						//#endif
						
						( ( BasicOptionsScreen ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT | Graphics.VCENTER );						
						
						indexSoftLeft = TEXT_OK;
						indexSoftRight = TEXT_BACK;
					break;
					
					case SCREEN_CREDITS:
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, TEXT_CREDITS_TEXT, true );
						
						indexSoftRight = TEXT_BACK;
					break;
					
					case SCREEN_HELP_MENU:
						nextScreen = createBasicMenu( index, new int[] {
							TEXT_OBJECTIVES,
							TEXT_CONTROLS,
							TEXT_TIPS,
							TEXT_BACK,
							} );
						
						indexSoftLeft = TEXT_OK;
						indexSoftRight = TEXT_BACK;
					break;

					case SCREEN_HELP_OBJECTIVES:
					case SCREEN_HELP_CONTROLS:
					case SCREEN_HELP_TIPS:
						nextScreen = new BasicTextScreen( SCREEN_HELP_MENU, FONT_DEFAULT, 
														  getText( TEXT_HELP_OBJECTIVES + ( index - SCREEN_HELP_OBJECTIVES ) ) + getVersion(), false,
														  index == SCREEN_HELP_OBJECTIVES ? new Drawable[] { new FarofadaBar( true ) }: null );
						
						indexSoftRight = TEXT_BACK;
					break;
					
					case SCREEN_NEXT_LEVEL:
						playScreen.prepareToNextLevel();
					
					case SCREEN_CONTINUE_GAME:
						nextScreen = playScreen;

						if ( currentScreen == SCREEN_FAROFERS ) {
							MediaPlayer.stop();
							
							indexSoftLeft = SOFT_KEY_REMOVE;
							indexSoftRight = SOFT_KEY_REMOVE;
						} else {
							indexSoftLeft = SOFT_KEY_DONT_CHANGE;
							indexSoftRight = SOFT_KEY_DONT_CHANGE;
						}

						bkgType = BACKGROUND_TYPE_NONE;

						playScreen.unpause();

						//#ifndef LOW_JAR
						channelText = getText( TEXT_LEVEL ) + playScreen.getCurrentLevel();
						
						// solu��o apelativa para evitar problemas em alguns aparelhos durante a troca da tela de pausa
						// para a tela do jogo
							//#if SCREEN_SIZE == "SMALL"
//# 							useTvTransition = false;
							//#else
							if ( currentScreen == SCREEN_PAUSE )
								useTvTransition = false;
							//#endif
						//#endif

						index = SCREEN_GAME;
					break;

					case SCREEN_PAUSE:
						//#ifdef NO_SOUND
//# 						if ( MediaPlayer.isVibrationSupported() ) {
//# 							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
//# 								TEXT_CONTINUE,
//# 								TEXT_TURN_VIBRATION_OFF,
//# 								TEXT_BACK_MENU,
//# 								TEXT_EXIT_GAME,
//# 							}, ENTRY_PAUSE_MENU_CONTINUE, -1, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
//# 						} else {
//# 							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
//# 								TEXT_CONTINUE,
//# 								TEXT_TURN_SOUND_OFF,
//# 								TEXT_BACK_MENU,
//# 								TEXT_EXIT_GAME,
//# 							}, ENTRY_PAUSE_MENU_CONTINUE, -1, TEXT_TURN_SOUND_ON, -1, -1 );							
//# 						}						
						//#else
						if ( MediaPlayer.isVibrationSupported() ) {
							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
								TEXT_CONTINUE,
								TEXT_TURN_SOUND_OFF,
								TEXT_TURN_VIBRATION_OFF,
								TEXT_BACK_MENU,
								TEXT_EXIT_GAME,
							}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
						} else {
							nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT, new int[] {
								TEXT_CONTINUE,
								TEXT_TURN_SOUND_OFF,
								TEXT_BACK_MENU,
								TEXT_EXIT_GAME,
							}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
						}
						//#endif

						( ( BasicOptionsScreen ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT | Graphics.VCENTER );
						
						indexSoftLeft = TEXT_OK;
						indexSoftRight = TEXT_CONTINUE;
					break;
					
					case SCREEN_CONFIRM_MENU:
						nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, getCursor(), TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO );
						indexSoftLeft = TEXT_OK;
					break;
					
					case SCREEN_CONFIRM_EXIT:
						final int TEXT_EXIT_INDEX = TEXT_CONFIRM_EXIT_1 + NanoMath.randInt( TEXT_CONFIRM_EXIT_TOTAL );
						nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, getCursor(), TEXT_EXIT_INDEX, TEXT_YES, TEXT_NO );
						indexSoftLeft = TEXT_OK;
					break;

					//#if DEMO == "false"
					case SCREEN_CONFIRM_ERASE_RECORDS:
						nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, getCursor(), TEXT_ERASE_RECORDS_CONFIRM, TEXT_YES, TEXT_NO );
						indexSoftLeft = TEXT_OK;
					break;

					case SCREEN_HIGH_SCORES:
						//#ifndef LOW_JAR
							//#if SCREEN_SIZE == "SMALL"
//# 								if ( currentScreen != SCREEN_MAIN_MENU )
//#                                     useTvTransition = false;
							//#endif
						//#endif
						
						nextScreen = HighScoresScreen.createInstance( FONT_DEFAULT );
						indexSoftRight = TEXT_BACK;
					break;
					//#else
//# 					// vers�o demo
//# 					case SCREEN_BUY_GAME_EXIT:
//# 					case SCREEN_BUY_GAME_RETURN:
//# 						nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, midlet.cursor, TEXT_BUY_FULL_GAME_TEXT, TEXT_YES, TEXT_NO );
//# 						indexSoftLeft = TEXT_OK;
//# 					break;
//# 					
//# 					case SCREEN_PLAYS_REMAINING:
//# 						final String text = playsRemaining > 1 ? 
//# 								getText( TEXT_2_OR_MORE_PLAYS_REMAINING_1 ) + playsRemaining + getText( TEXT_2_OR_MORE_PLAYS_REMAINING_2 ) :
//# 								getText( TEXT_1_PLAY_REMAINING );
//# 						nextScreen = new BasicTextScreen( midlet, index, FONT_DEFAULT, text, true );
//# 						
//# 						indexSoftLeft = TEXT_OK;
//# 					break;
					//#endif
					
					//#if DEBUG == "true"
					case SCREEN_ERROR_LOG:
//						texts[ TEXT_ERROR_LOG ] = MediaPlayer.LOG.toString().toUpperCase();
//						texts[ TEXT_ERROR_LOG ] = ScreenManager.LOG.toString().toUpperCase();
						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, TEXT_ERROR_LOG, false  );
						indexSoftRight = TEXT_BACK;
					break;
					//#endif
					
					case SCREEN_GAME:
						if ( currentScreen == SCREEN_FAROFERS ) {
							return currentScreen;
						}
						
						playScreen = new PlayScreen();
						
						index = SCREEN_FAROFERS;
					
					case SCREEN_FAROFERS:
						final Drawable[] farofers = Element.getElements( ELEMENT_TYPE_FAROFER );
						final String textFarofers = getText( TEXT_FAROFERS );

						if ( textFarofers.length() < 10 ) {
							final StringBuffer buffer = new StringBuffer();
							
							// caso n�o seja a vers�o de tela pequena, adiciona espa�os para posicionar melhor o texto e
							// os farofeiros
							buffer.append( getText( TEXT_FAROFERS_TITLE ) );
							for ( byte i = 0; i < farofers.length; ++i ) {
								buffer.append( getText( TEXT_FAROFERS_BEGIN ) );
								buffer.append( i );
								buffer.append( getText( TEXT_FAROFERS_END ) );
							}
							
							texts[ TEXT_FAROFERS ] = buffer.toString();
						}
						nextScreen = new BasicTextScreen( SCREEN_NEXT_LEVEL, FONT_DEFAULT, TEXT_FAROFERS, true, farofers );
						indexSoftLeft = TEXT_OK;
						
						lastFarofersScreenTime = System.currentTimeMillis();
						
						//#ifndef LOW_JAR
							//#if SCREEN_SIZE == "SMALL"
//# 							useTvTransition = false;
							//#endif
						//#endif
					break;

					//#if BRANDS == "true"
					case SCREEN_BRAND:
						nextScreen = new BrandScreen();
					break;
					//#endif
					
				} // fim switch ( index )
				
				if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
					setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );
				
				if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );
				
				//#ifndef LOW_JAR
				channel.setText( channelText );
				//#endif
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				
				if ( index != SCREEN_ERROR_LOG ) {
					texts[ TEXT_ERROR_LOG ] += new String( "\nSCREEN #" + index + ":\n" +  e.toString() + "\n" + e.getClass() + "\n" + e.getMessage() ).toUpperCase();
					
//					TODO setScreen( SCREEN_ERROR_LOG );
//					return;
				}
				//#endif
				
				// deu erro ao alocar tela de texto; sai do jogo
				exit();
			}
			
			if ( nextScreen != null ) {
				setBackground( bkgType );
				
				//#ifdef LOW_JAR
//# 				midlet.manager.setCurrentScreen( nextScreen );
				//#else
				if ( true || useTvTransition ) // TODO testar sempre uso da transi��o
					transitionTV.setNextScreen( nextScreen, currentScreen == SCREEN_SPLASH_NANO );
				else
					midlet.manager.setCurrentScreen( nextScreen );
				//#endif
				
				//#if DEBUG == "true"
				System.out.println( "OK" );
				//#endif				
			}
		} // fim if ( index != currentScreen )
        
        return index;
	} // fim do m�todo setScreen( byte )
	
	
	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_PATTERN:
				if ( isLowMemory() ) {
					midlet.manager.setBackgroundColor( BACKGROUND_COLOR );
				}
				//#ifndef LOW_JAR
				else {
					background.setAnimation( BasicAnimatedPattern.ANIMATION_RANDOM );
					midlet.manager.setBackground( background, true );				
				}
				//#endif
			break;
		}
	}
	
	
	private static final BasicMenu createBasicMenu( int index, int[] entries ) throws Exception {
		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, FONT_DEFAULT, entries );		
		menu.setCursor( getCursor(), BasicMenu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT | Graphics.VCENTER );
		
		return menu;
	}
	
	
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( FONT_DEFAULT, getText( textIndex ) ), true, 0 );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}				
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );
		
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				midlet.softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
			break;
			
			case ScreenManager.SOFT_KEY_MID:
				// utilizada para mostrar o canal atual
				midlet.manager.setSoftKey( softKey, d );
			break;
			
			case ScreenManager.SOFT_KEY_RIGHT:
				midlet.softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
			break;
		}
	} // fim do m�todo setSoftKey( byte, Drawable, boolean, int )	
	
	
	public static final Drawable getCursor() {
		try {
			Drawable cursor;
			//#ifndef LOW_JAR
				if ( !isLowMemory() )
					cursor = new Sprite( PATH_CURSOR + "cursor.dat", PATH_CURSOR + 'c' );
				else
			//#endif
				cursor = new DrawableImage( PATH_CURSOR + "c_0.png" );
			cursor.defineReferencePixel( ( cursor.getWidth() >> 1 ) + CURSOR_OFFSET_X, cursor.getHeight() >> 1 );
			
			return cursor;
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
			
			return null;
		}
	} // fim do m�todo getCursor()	
	
	
	/**
	 * Verifica se pode continuar jogo, ou seja, se o intervalo desde a �ltima op��o de continuar (na tela de pausa)
	 * � maior que o intervalo m�nimo. Caso o intervalo seja adequado, faz a troca de tela.
	 */
	private static final void checkContinueGame() {
		final long interval = System.currentTimeMillis() - lastContinueGameTime;
		
		if ( interval > MIN_CONTINUE_GAME_INTERVAL ) {
			lastContinueGameTime = System.currentTimeMillis();
			setScreen( SCREEN_CONTINUE_GAME );
		}
	}


	public synchronized final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						if ( resourcesLoaded ) {
							lastFarofersScreenTime = System.currentTimeMillis();
							setScreen( SCREEN_GAME );
						} else {
							setScreen( SCREEN_LOADING );
						}
					break;
					
					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
					break;
					
					//#if DEMO == "false"
					case ENTRY_MAIN_MENU_RECORDS:
						setScreen( SCREEN_HIGH_SCORES );
					break;
					//#else
//# 					case ENTRY_MAIN_MENU_BUY_FULL_GAME:
//# 						setScreen( SCREEN_BUY_GAME_RETURN );
//# 					break;
					//#endif
					
					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					//#if DEBUG == "true"
					case ENTRY_MAIN_MENU_ERROR_LOG:
						setScreen( SCREEN_ERROR_LOG );
					break;
					//#endif
					
					case ENTRY_MAIN_MENU_EXIT:
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU
			
			
			//#if DEBUG == "true"
			case SCREEN_ERROR_LOG:
				setScreen( SCREEN_MAIN_MENU );
			break;
			//#endif

			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_TIPS:
						setScreen( SCREEN_HELP_TIPS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							checkContinueGame();
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:	
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:	
							checkContinueGame();
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:	
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:			
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}					
				}
			break; // fim case SCREEN_PAUSE
			
			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						//#if DEMO == "false"
						case ENTRY_OPTIONS_MENU_VIB_ERASE_RECORDS:	
							setScreen( SCREEN_CONFIRM_ERASE_RECORDS );
						break;
						//#endif
						
						case ENTRY_OPTIONS_MENU_VIB_BACK:	
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				} else {
					switch ( index ) {
						//#if DEMO == "false"
						case ENTRY_OPTIONS_MENU_NO_VIB_ERASE_RECORDS:	
							setScreen( SCREEN_CONFIRM_ERASE_RECORDS );
						break;
						//#endif
						
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							setScreen( SCREEN_MAIN_MENU );
						break;		
					}
				}
			break; // fim case SCREEN_OPTIONS
			
			//#if DEMO == "false"
			case SCREEN_CONFIRM_ERASE_RECORDS:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						HighScoresScreen.eraseRecords();
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_OPTIONS );
					break;
				}
			break;
			//#else
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 							e.printStackTrace();
							//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif
			
			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						//#if DEMO == "false"
						HighScoresScreen.setScore( playScreen.getScore() );						
						playScreen.resetScore();
						setScreen( SCREEN_MAIN_MENU );
						//#else
//# 						gameOver( 0 );
						//#endif
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;
			
			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						exit();
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;			
			
			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				
				setScreen( SCREEN_SPLASH_NANO );
			break;
			
			case SCREEN_FAROFERS:
				//#if BRANDS == "true"
				setScreen( SCREEN_BRAND ); 
				//#else
//# 				// TODO ainda acontece? evita que eventos de ponteiros chamem SCREEN_NEXT_LEVEL seguidamente, fazendo o jogo
//# 				// iniciar no 2� n�vel e travar
//# 				final long interval = System.currentTimeMillis() - lastFarofersScreenTime;
//# 				if ( interval > MIN_FAROFERS_SCREEN_TIME && playScreen.getCurrentLevel() <= 0 ) {
//# 					lastFarofersScreenTime = Long.MAX_VALUE;
//# 					setScreen( SCREEN_NEXT_LEVEL ); 
//# 				}
				//#endif
			break;
		} // fim switch ( id )
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
	}
	
	
	public static final void gameOver( int score ) {
		//#if DEMO == "false"
		if ( HighScoresScreen.setScore( score ) ) {
			playScreen.resetScore();
			setScreen( SCREEN_HIGH_SCORES );
		} else {
			setScreen( SCREEN_MAIN_MENU );
		}
		//#else
//# 		setScreen( SCREEN_BUY_GAME_EXIT );
		//#endif
	}	
	
	
	public static final boolean isPlaying() {
		return playScreen != null;
	}
	
	
	/**
	 * Atualiza as dimens�es de objetos dependentes do tamanho de tela, para evitar problemas em aparelhos
	 * onde as dimens�es de tela cheia n�o s�o definidas imediatamente (como no RAZR)
	 */
	private static final void updateBackground() {
		//#ifndef LOW_JAR
		if ( !isLowMemory() )
			background.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		transitionTV.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		channel.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		//#endif
	}	
	
	
	public static final void setSoftKeysInTransition( boolean inTransition ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		midlet.softkeyLeft.setInTransition( inTransition );
		midlet.softkeyRight.setInTransition( inTransition );
	}	
    
    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "1.0";

        return "<ALN_H>VERS�O " + version + "\n\n";        
    }
	
	
	public static final boolean isLowMemory() {
		//#ifdef LOW_JAR
//# 		return true;
		//#elifdef NO_SOUND
//# 		// vers�o do Samsung C420 (n�o possui sons)
//# 		return false;
		//#else
		return lowMemory;
		//#endif
	}


	//#if DEMO == "true"
//# 	public final void write( DataOutputStream output ) throws Exception {
//# 		output.writeByte( playsRemaining > 0 ? playsRemaining - 1 : 0 );
//# 	}
//# 
//# 
//# 	public final void read( DataInputStream input ) throws Exception {
//# 		playsRemaining = input.readByte();
//# 	}
//# 	
	//#endif
	
	
	private static final class LoadScreen extends Label implements Updatable {
		
		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;
		
		private long lastUpdateTime;
		
		private static final byte MAX_DOTS = 4;
		
		private byte dots;
		
		private final GameMIDlet midlet;
		
		private Thread loadThread;
		
		
		private LoadScreen( GameMIDlet m ) throws Exception {
			super( FONT_DEFAULT, GameMIDlet.getText( TEXT_LOADING ) );
			
			this.midlet = m;
			
			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;
			
			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// as imagens do jogo s�o carregadas aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa (ou seja, a transi��o da TV estiver encerrada)
					if ( ScreenManager.getInstance().getCurrentScreen() == this ) {
						loadThread = new Thread() {
							public final void run() {
								try {
									MediaPlayer.free();
									Beach.loadImages();
									Element.loadFrameSets();	

									midlet.resourcesLoaded = true;

									GameMIDlet.setScreen( SCREEN_GAME );
								} catch ( Exception e ) {
									//#if DEBUG == "true"
							texts[ TEXT_ERROR_LOG ] += e.getMessage().toUpperCase();
										
							setScreen( SCREEN_ERROR_LOG );
							e.printStackTrace();
									//#else
//# 										// somente sai do jogo
//# 										exit();
									//#endif
								}
							}
						};
						//#ifndef LOW_JAR
						// p�ra de atualizar o background
						if ( !isLowMemory() )
							midlet.manager.setBackground( background, false );
						//#endif
						
						loadThread.start();
					}
				} else {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) % MAX_DOTS );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for ( byte i = 0; i < dots; ++i )
						temp += '.';

					setText( temp );
					
					try {
						// permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );			
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}					
				}
			}
		}
		
		
	} // fim da classe interna LoadScreen

}
