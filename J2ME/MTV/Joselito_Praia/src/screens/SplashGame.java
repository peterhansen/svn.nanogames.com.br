/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import core.Constants;

//#ifndef LOW_JAR
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public final class SplashGame extends DrawableGroup implements Constants, Updatable, KeyListener
		//#ifndef LOW_JAR
		, PointerListener 
		//#endif
{
	
	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 5;
	
	
	/** tempo restante de exposi��o da tela de splash do jogo */
	private short visibleTime = 2700;
	
	/** texto indicando "pressione qualquer tecla" */
	private final RichLabel pressKeyLabel;
	
	/** velocidade do "pisca-pisca" do label */
	private final short BLINK_RATE = -666; // dem�in! >:)
	
	//#if SCREEN_SIZE != "SMALL"
	private final byte TOTAL_CHARACTERS = 7;
	//#endif
	
	
	public SplashGame( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		//#if SCREEN_SIZE == "SMALL"
//# 		
//# 		/** corre��o na posi��o vertical do logo Hermes e Renato em rela��o ao t�tulo do jogo */
//# 		final byte VISIBLE_HEIGHT = 115;
//# 		
//# 		final byte TITLE_Y_OFFSET = 72;
//# 		
		//#ifndef JAR_100_KB
//# 		// posi��o da imagem do joselito relativa � posi��o do t�tulo do jogo
//# 		final Point JOSELITO_OFFSET = new Point( 78, -29 );
//# 		final DrawableImage imgJoselito = new DrawableImage( PATH_SPLASH_GAME + "0.png" );
//# 		insertDrawable( imgJoselito );	
		//#endif
//# 		
		//#else
		
		final Point[] CHARACTERS_POSITION = { 
											  new Point( 73, 18 ), // Joselito
											  new Point( 20, 0 ), // Away
											  new Point( 6, 18 ), // Hermes
											  new Point( 0, 53 ), // Massacration
											  new Point( 114, 55 ), // Renato
											  new Point( 16, 51 ), // Cantor
											  new Point( 49, 65 ), // Bo�a
		};
		
		final Point GROUP_SIZE = new Point( 162, 115 );
		
		/** corre��o na posi��o vertical do logo Hermes e Renato em rela��o ao t�tulo do jogo */
		final byte LOGO_Y_OFFSET = 9;
		
		final DrawableGroup charactersGroup = new DrawableGroup( TOTAL_CHARACTERS );
		charactersGroup.setSize( GROUP_SIZE );
		charactersGroup.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( GROUP_SIZE.x >> 1 ),
									 ScreenManager.SCREEN_HEIGHT - GROUP_SIZE.y );
		insertDrawable( charactersGroup );
		
		for ( byte i = 0; i < TOTAL_CHARACTERS; ++i ) {
			final DrawableImage img = new DrawableImage( PATH_SPLASH_GAME + i + ".png" );
			img.setPosition( CHARACTERS_POSITION[ i ].x, CHARACTERS_POSITION[ i ].y );
			charactersGroup.insertDrawable( img );
		}		
 				
		//#endif
		
		final DrawableImage title = new DrawableImage( PATH_SPLASH_GAME + "title.png" );
		final DrawableImage logo = new DrawableImage( PATH_SPLASH_GAME + "logo.png" );
		
		insertDrawable( logo );
		insertDrawable( title );
		
		//#if SCREEN_SIZE == "SMALL"
//# 			logo.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( logo.getSize().x >> 1 ),
//# 							  ( size.y - VISIBLE_HEIGHT ) >> 1 );			
//# 		
//# 			title.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( title.getSize().x >> 1 ),
//# 							   logo.getPosition().y + TITLE_Y_OFFSET );
//# 
			//#ifndef JAR_100_KB
//# 			imgJoselito.setPosition( title.getPosition().x + JOSELITO_OFFSET.x, title.getPosition().y + JOSELITO_OFFSET.y );
			//#endif
		//#else
			title.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( title.getSize().x >> 1 ),
							   ScreenManager.SCREEN_HALF_HEIGHT - ( title.getSize().y >> 1 ) );

			logo.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( logo.getSize().x >> 1 ),
							  title.getPosition().y - logo.getSize().y + LOGO_Y_OFFSET );	
			
			if ( size.y < SPLASH_GAME_MIN_HEIGHT ) {
				charactersGroup.move( 0, SPLASH_GAME_MIN_HEIGHT - size.y );
			}
		//#endif
		
		pressKeyLabel = new RichLabel( font, AppMIDlet.getText( TEXT_PRESS_ANY_KEY ), ( size.x * 3 ) >> 2, null );
		pressKeyLabel.setPosition( ( size.x - pressKeyLabel.getSize().x ) >> 1, size.y - pressKeyLabel.getSize().y );
		pressKeyLabel.setVisible( false );
		insertDrawable( pressKeyLabel );
	}


	public final void update( int delta ) {
		visibleTime -= delta;
		
		if ( visibleTime <= BLINK_RATE ) {
			// pisca o texto "Pressione qualquer tecla"
			pressKeyLabel.setVisible( !pressKeyLabel.isVisible() );
			visibleTime %= BLINK_RATE;
		}
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
		visibleTime = 0;
		//#endif
		
		if ( visibleTime <= 0 ) {
			//#if DEMO == "true"
//# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
			//#else
			GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			//#endif
		}
	}


	public final void keyReleased( int key ) {
	}


	//#ifndef LOW_JAR
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif

}
