/**
 * BrandScreen.java
 * �2007 Nano Games
 * 
 * Created on 11/12/2007 18:09:35 
 *
 */

//#if BRANDS == "true"
package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author peter
 */
public final class BrandScreen extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 5;
	
	private static final int[] BACKGROUND_COLORS = { 0x000000, 
													 0x683a88,
													 0xf8ce0e,
													 0x000000,
													};
	
	/** marca atualmente exibida */
	private static byte currentBrand = -1;
	
	/** tempo de exposi��o da marca em milisegundos */
	private short visibleTime = 3000;
	
	
	public BrandScreen() throws Exception {
		super( TOTAL_ITEMS );
		
		checkSavedBrand();
		currentBrand = ( byte ) ( ( currentBrand + 1 ) % BRANDS_TOTAL );
		
		GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_BRANDS, new BrandSaver() );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// insere o background (cor s�lida da marca)
		final Pattern bkg = new Pattern( null );
		bkg.setFillColor( BACKGROUND_COLORS[ currentBrand ] );
		bkg.setSize( size );
		insertDrawable( bkg );
		
		String pathImg = PATH_BRANDS + "font_";
		String pathDat = PATH_BRANDS + "font_";
		switch ( currentBrand ) {
			case BRAND_SKOL:
			case BRAND_COCA_COLA:
				pathImg += BRAND_COCA_COLA + ".png";
				pathDat += BRAND_COCA_COLA + ".dat";
			break;

			case BRAND_OI:
			case BRAND_MTV:
				pathImg += BRAND_OI + ".png";
				pathDat += BRAND_OI + ".dat";
			break;

			//#if DEBUG == "true"
			default:
				throw new Exception( "�ndice de marca inv�lida: " + currentBrand );
			//#endif
		}
		
		// insere o logo da marca
		final DrawableImage logo = new DrawableImage( PATH_LOGOS + currentBrand + ".png" );
		logo.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( logo.getSize().x >> 1 ),
						  ScreenManager.SCREEN_HALF_HEIGHT - ( logo.getSize().y >> 1 ) );
		insertDrawable( logo );
		
		final ImageFont font = ImageFont.createMultiSpacedFont( pathImg, pathDat );
		
		RichLabel text = new RichLabel( font, GameMIDlet.getText( TEXT_OFFERED_BY ), ScreenManager.SCREEN_WIDTH, null );
		text.setSize( ScreenManager.SCREEN_WIDTH, text.getTextTotalHeight() );
		text.setPosition( 0, logo.getPosition().y >> 1 );
		
		insertDrawable( text );		
		
		//#if SCREEN_SIZE == "SMALL"
//# 		// reposiciona o texto no caso de logo da MTV, evitando que o texto de cr�ditos se sobreponha sobre a logo da MTV
//# 		// em telas pequenas
//# 		if ( currentBrand == BRAND_MTV && size.y < SCREEN_HEIGHT_FIT ) {
//# 			text.setPosition( text.getPosition().x, 0 );
//# 			logo.setPosition( logo.getPosition().x, text.getSize().y );
//# 		}
		//#endif		
		
		text = new RichLabel( font, GameMIDlet.getText( TEXT_COPYRIGHT ) + GameMIDlet.getText( TEXT_COCA_COLA + currentBrand )
								+ GameMIDlet.getText( TEXT_ALL_RIGHTS_RESERVED ), ScreenManager.SCREEN_WIDTH, null );
		text.setSize( ScreenManager.SCREEN_WIDTH, text.getTextTotalHeight() );
		text.setPosition( 0, ScreenManager.SCREEN_HEIGHT - text.getSize().y );
		
		insertDrawable( text );		
	}


	public final void update( int delta ) {
		super.update( delta );
		
		visibleTime -= delta;
		if ( visibleTime <= 0 ) {
			visibleTime = Short.MAX_VALUE;
			GameMIDlet.setScreen( SCREEN_NEXT_LEVEL );
		}
	}
	
	
	public static final byte getCurrentBrand() {
		checkSavedBrand();
		
		return currentBrand;
	}
	
	
	private static final void checkSavedBrand() {
		if ( currentBrand == -1 ) {
			try {
				GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_BRANDS, new BrandSaver() );
			} catch ( Exception e ) {
				currentBrand = ( byte ) ( NanoMath.randInt( BRANDS_TOTAL ) );
			}
		}
	}
	
	
	private static final class BrandSaver implements Serializable {

		public void write( DataOutputStream output ) throws Exception {
			output.writeByte( currentBrand );
		}

		
		public void read( DataInputStream input ) throws Exception {
			currentBrand = input.readByte();
		}
		
	}

}

//#endif