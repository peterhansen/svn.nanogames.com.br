/**
 * PlayScreen.java
 *
 * Created on 27/11/2007 17:29:40
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import core.Beach;
import core.Constants;
import core.FarofadaBar;
import javax.microedition.lcdui.Image;

//#ifndef LOW_JAR
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * �2007 Nano Games
 * @author peter
 */
public final class PlayScreen extends UpdatableGroup implements KeyListener, ScreenListener, Constants
		// n�o h� aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do c�digo)
		//#ifndef LOW_JAR
		, PointerListener 
		//#endif

{
	
	/** n�mero total de drawables da tela de jogo */
	private static final byte TOTAL_ITEMS = 15;
	
	
	public static final byte STATE_NONE					= 0;
	public static final byte STATE_PLAYING_SHOW_PAUSE	= 1;
	public static final byte STATE_PLAYING_SHOW_SCORE	= 2;
	public static final byte STATE_PAUSED				= 3;
	public static final byte STATE_GAME_OVER			= 4;
	public static final byte STATE_BEGIN_LEVEL			= 5;
	public static final byte STATE_END_LEVEL_MESSAGE	= 6;
	public static final byte STATE_END_LEVEL_CHANGE		= 7;
	
	/** estado atual da tela de jogo */
	private byte state;
	/** estado anterior da tela de jogo (valor utilizado para retornar da pausa) */
	private byte previousState;
	
	/** tempo total em milisegundos em que uma mensagem � exibida */
	private static final short TIME_MESSAGE = 3313;
	
	/** tempo total em milisegundos em que o label de pausa � exibido */
	private static final short TIME_PAUSE = 1500;
	
	/** tempo restante para que a mensagem atual seja exibida */
	private short messageTime;
	
	/** n�vel atual */
	private short level;
	
	/** pontua��o do jogador */
	private int score;
	
	/** pontua��o exibida atualmente */
	private int scoreShown;
	
	/** pontua��o m�xima mostrada */
	private static final int SCORE_SHOWN_MAX = 999999;

	/** velocidade de atualiza��o da pontua��o */
	private final MUV scoreSpeed = new MUV();
	
	/** label que indica a pontua��o e o multiplicador */
	private final RichLabel scoreLabel;
	
	/** label que mostra mensagens de fim de n�vel ou fim de jogo */
	private final RichLabel labelMessage;
	
	/** decremento na farofada a cada n�vel passado */
	private static final short FAROFADA_DECREASE_PER_LEVEL = -FAROFADA_MAX * 23 / 100;
	
	/** decremento na farofada a cada farofeiro atingido (nos LG o valor � maior devido � lentid�o da mira) */
	//#ifdef LG
//# 	private static final short FAROFADA_DECREASE_PER_FAROFER_HIT = -FAROFADA_MAX / 8;
	//#else
		//#if SCREEN_SIZE == "BIG"
//# 		private static final short FAROFADA_DECREASE_PER_FAROFER_HIT = -FAROFADA_MAX / 10;
		//#else
	private static final short FAROFADA_DECREASE_PER_FAROFER_HIT = -FAROFADA_MAX / 9;
		//#endif
	//#endif
	
	/** incremento do n�vel da farofada ao atingir um n�o-farofeiro */
	private static final short FAROFADA_INCREASE_HIT_NON_FAROFER = FAROFADA_MAX / 6;
	
	/** velocidade inicial de incremento da farofada por segundo */
	private static final short FAROFADA_INCREASE_PER_SECOND_INITIAL	= FAROFADA_MAX / 44;
	
	/** velocidade final de incremento da farofada por segundo */
	//#if SCREEN_SIZE == "SMALL"
		//#ifdef LG
//# 		private static final short FAROFADA_INCREASE_PER_SECOND_FINAL	= FAROFADA_MAX / 34;
		//#else
//# 		private static final short FAROFADA_INCREASE_PER_SECOND_FINAL	= FAROFADA_MAX / 24;
		//#endif
	//#elif SCREEN_SIZE == "MEDIUM"
		//#ifdef LG
//# 		private static final short FAROFADA_INCREASE_PER_SECOND_FINAL	= FAROFADA_MAX / 30;
		//#else		
	private static final short FAROFADA_INCREASE_PER_SECOND_FINAL	= FAROFADA_MAX / 21;
		//#endif
	//#else
//# 	private static final short FAROFADA_INCREASE_PER_SECOND_FINAL	= FAROFADA_MAX / 15;
	//#endif
	
	/** velocidade de incremento da farofada */
	private final MUV farofadaSpeed = new MUV();
	
	/** velocidade de incremento da barra da farofada */
	private final MUV farofadaBarSpeed = new MUV();	
	
	/** aumento na pontua��o a cada farofeiro atingido */
	private static final byte SCORE_INCREASE_PER_FAROFER = 10;
	
	/** multiplicador atual da pontua��o */
	private byte scoreMultiplier;
	
	/** multiplicador m�ximo da pontua��o */
	private static final byte SCORE_MULTIPLIER_MAX = 99;
	
	/** Quantidade inicial de farofeiros atingidos em sequ�ncia para que o multiplicador seja aumentado.
	 *  Exemplo: 3 para passar de 1x para 2x, 6 para 3x, mais 9 para 4x, e assim por diante. 
	 */
	private static final byte SCORE_MULTIPLIER_INCREASE = 2;
	
	/** quantidade atual de farofeiros atingidos em sequ�ncia */
	private short comboCounter;
	
	/** n�vel atual da farofada */
	private short farofada;
	
	/** n�vel da farofada mostrado atualmente */
	private short farofadaShown;
	
	/** barra indicadora do n�vel de farofada */
	private final FarofadaBar farofadaBar;
	
	/** Tempo m�nimo antes que o jogador possa dar pausa no jogo (utilizado para evitar travamentos em alguns aparelhos). */
	private static final short MIN_TIME_BEFORE_PAUSE = 1400;
	
	/** Momento da �ltima entrada na tela de jogo. */
	private long lastUnpauseTime;
	
	//<editor-fold defaultstate="collapsed" desc="mira do jogador e c�mera">
	
	/** mira do jogador */
	private final DrawableImage crosshair;
	
	/** m�o do jogador */
	private final Hand hand;
	
	/** velocidade da anima��o de entrada/sa�da da m�o */
	private final MUV handSpeed;
	
	private static final byte HAND_FRAME_CENTER = 0;
	private static final byte HAND_FRAME_LEFT	= 1;
	private static final byte HAND_FRAME_RIGHT	= 2;
	
	/** posi��o absoluta da mira (a posi��o do drawable � a posi��o relativa � tela) */
	private final Point crosshairPosition = new Point();
	
	// dire��es de movimenta��o da mira (podem ser combinadas)
	private static final byte DIRECTION_NONE	= 0;
	private static final byte DIRECTION_RIGHT	= 1;
	private static final byte DIRECTION_LEFT	= 2;
	private static final byte DIRECTION_UP		= 4;
	private static final byte DIRECTION_DOWN	= 8;
	
	private static final byte DIRECTION_H_MASK = DIRECTION_RIGHT | DIRECTION_LEFT;
	private static final byte DIRECTION_V_MASK = DIRECTION_UP | DIRECTION_DOWN;
	
	private static final byte DIRECTION_ALL_MASK = DIRECTION_H_MASK | DIRECTION_V_MASK;
	
	private byte crosshairDirection;
	
	/** posi��o x da tela a partir da qual a c�mera come�a a se mover para a esquerda */
	private final int CAMERA_START_MOVE_LEFT;
	/** posi��o x da tela a partir da qual a c�mera come�a a se mover para a direita */
	private final int CAMERA_START_MOVE_RIGHT;
	
	/** velocidade de movimenta��o atual da c�mera */
	private final MUV cameraSpeed = new MUV();
	
	/** velocidade de movimenta��o horizontal atual da mira */
	private final MUV crosshairSpeedX = new MUV();
	
	/** velocidade de movimenta��o vertical atual da mira */
	private final MUV crosshairSpeedY = new MUV();	
	
	/** acelera��o da mira no eixo x */
	private final MUV crosshairAccX = new MUV();
	
	/** acelera��o da mira no eixo y */
	private final MUV crosshairAccY = new MUV();
	
	/** limite de velocidade atual da mira */
	private final Point crosshairSpeedLimit = new Point();
	
	/** limite m�ximo da mira */
	private final Point crosshairLimitMax = new Point();
	
	private boolean usingCrosshair;
	
	//#ifndef LOW_JAR
	/** Indica o estado do ponteiro. */
	private byte pointerStatus;
	
	private static final byte POINTER_STATUS_RELEASED	= 0;
	private static final byte POINTER_STATUS_PRESSED	= 1;
	private static final byte POINTER_STATUS_DRAGGED	= 2;
	
	/** �ltima posi��o do ponteiro. */
	private final Point lastPointerPos = new Point();
	
	/** Momento do �ltimo evento onPointerPressed recebido. */
	private long lastPointerPressedTime;
	
	/** Toler�ncia de tempo para que o evento onPointerReleased fa�a o jogador atirar, mesmo que o �ltimo evento de 
	 * ponteiro n�o tenha sido onPointerPressed. Essa toler�ncia � utilizada para evitar que cliques n�o perfeitos,
	 * ou seja, quando a posi��o onde o ponteiro foi pressionado n�o � igual � posi��o onde foi liberado.
	 */
	private static final byte POINTER_CLICK_TIME_TOLERANCE = 120;
	//#endif
	
	//</editor-fold>
	
	/** praia */
	private final Beach beach;
	
	
	public PlayScreen() throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// define os pontos de in�cio de movimenta��o da c�mera
		final int diff = ( size.x * ( 100 - PLAYSCREEN_CAMERA_START_MOVE_PERCENT ) / 100 ) >> 1;
		CAMERA_START_MOVE_LEFT = diff;
		CAMERA_START_MOVE_RIGHT = size.x - diff;
		
		// insere a praia
		beach = new Beach( this );
		insertDrawable( beach );
		
		// insere a mira do jogador
		crosshair = new DrawableImage( PATH_IMAGES + "crosshair.png" );
		crosshair.defineReferencePixel( crosshair.getSize().x >> 1, crosshair.getSize().y >> 1 );
		insertDrawable( crosshair );
		
		hand = Hand.loadHand();
		insertDrawable( hand );
		
		handSpeed = new MUV();
		
		// insere a barra indicadora da farofada
		farofadaBar = new FarofadaBar( false );
		
		final ImageFont fontScore = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
		final int maxFontLabelWidth = fontScore.getTextWidth( String.valueOf( SCORE_SHOWN_MAX ) );
		scoreLabel = new RichLabel( fontScore, "<ALN_R>1X\n0", maxFontLabelWidth, null );
		scoreLabel.setSize( maxFontLabelWidth, fontScore.getImage().getHeight() << 1 );
		
		labelMessage = new RichLabel( fontScore, null, size.x * 9 / 10, null );
		labelMessage.setSize( labelMessage.getSize().x, fontScore.getImage().getHeight() << 1 );
		labelMessage.defineReferencePixel( labelMessage.getSize().x >> 1, labelMessage.getSize().y >> 1 );
		labelMessage.setRefPixelPosition( size.x >> 1, size.y >> 1 );
		insertDrawable( labelMessage );
	}


	/**
	 * Obt�m o n�vel atual. Observa��o: o n�vel utilizado para c�lculos de dificuldade jamais ultrapassa MAX_LEVEL.
	 * @return o n�vel atual.
	 */
	public final short getCurrentLevel() {
		return level;
	}

	
	public synchronized final void keyPressed( int key ) {
		//#if DEBUG == "true"
		try {
		//#endif
	
		switch ( state ) {
			case STATE_GAME_OVER:
				if ( messageTime <= 0 ) {
					setState( STATE_NONE );
					GameMIDlet.gameOver( score );
				}
			break;
			
			case STATE_PLAYING_SHOW_PAUSE:
			case STATE_PLAYING_SHOW_SCORE:
				switch ( key ) {
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						final long interval = System.currentTimeMillis() - lastUnpauseTime;
						if ( interval > MIN_TIME_BEFORE_PAUSE ) {
							lastUnpauseTime = System.currentTimeMillis();
							setState( STATE_PAUSED );
						}
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
						switch ( beach.fire( crosshairPosition ) ) {
							case FIRE_HIT_CANT_SHOOT:
							break;

							case FIRE_HIT_FAROFER:
								++comboCounter;
								if ( comboCounter >= scoreMultiplier * SCORE_MULTIPLIER_INCREASE )
									setScoreMultiplier( scoreMultiplier + 1 );
								
								changeScore( SCORE_INCREASE_PER_FAROFER * scoreMultiplier );

								// quanto maior o multiplicador, maior a redu��o no n�vel da farofada ap�s um tiro certeiro
								changeFarofada( ( FAROFADA_DECREASE_PER_FAROFER_HIT * ( 3 + scoreMultiplier ) ) / 4 );
							break;

							case FIRE_HIT_NON_FAROFER:
								changeFarofada( FAROFADA_INCREASE_HIT_NON_FAROFER );

							case FIRE_HIT_NOTHING:
							case FIRE_HIT_STATIC:
							case FIRE_HIT_TOASTED:
								setScoreMultiplier( 1 );
							break;
						}
					break;

					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						addCrosshairDirection( DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						addCrosshairDirection( DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						addCrosshairDirection( DIRECTION_UP );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						addCrosshairDirection( DIRECTION_DOWN );
					break;

					case ScreenManager.KEY_NUM1:
						addCrosshairDirection( DIRECTION_UP | DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM3:
						addCrosshairDirection( DIRECTION_UP | DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM7:
						addCrosshairDirection( DIRECTION_DOWN | DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM9:
						addCrosshairDirection( DIRECTION_DOWN | DIRECTION_RIGHT );
					break;
				} // fim switch ( key )				
			break;
			
		}
		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	}

	
	public synchronized final void keyReleased( int key ) {
		//#if DEBUG == "true"
		try {
		//#endif
	
		switch ( GameMIDlet.getVendor() ) {
			//#ifdef JAR_100_KB
//# 			// a vers�o de 100kb � utilizada somente em alguns aparelhos Samsung. Nesses aparelhos, tamb�m ocorre o problema
//# 			// da mira "presa", onde ela continua se movendo numa dire��o mesmo ap�s se soltar a tecla correspondente.
//# 			case GameMIDlet.VENDOR_SAMSUNG:
			//#endif
			
			case GameMIDlet.VENDOR_HTC:
			case GameMIDlet.VENDOR_LG:
				// nos aparelhos HTC, o estado das teclas fica inv�lido ao se pressionar mais de uma tecla simultaneamente.
				// O evento keyReleased s� � chamado para uma das teclas, e mesmo o valor retornado por getKeyState �
				// inv�lido, armazenando "lixo" mesmo ap�s todas as teclas pressionadas serem soltas. Nesses aparelhos,
				// o jogo s� tratar� uma tecla por vez.
				removeCrosshairDirection( DIRECTION_ALL_MASK );
			break;
			
			default:
				switch ( key ) {
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						removeCrosshairDirection( DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						removeCrosshairDirection( DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						removeCrosshairDirection( DIRECTION_UP );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						removeCrosshairDirection( DIRECTION_DOWN );
					break;

					case ScreenManager.KEY_NUM1:
						removeCrosshairDirection( DIRECTION_UP | DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM3:
						removeCrosshairDirection( DIRECTION_UP | DIRECTION_RIGHT );
					break;

					case ScreenManager.KEY_NUM7:
						removeCrosshairDirection( DIRECTION_DOWN | DIRECTION_LEFT );
					break;

					case ScreenManager.KEY_NUM9:
						removeCrosshairDirection( DIRECTION_DOWN | DIRECTION_RIGHT );
					break;			
				} // fim switch ( key )						
		}

		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	}
	
	
	public final void hideNotify() {
		// verifica se o jogo ainda est� em andamento; caso contr�rio, significa que o hideNotify � referente � sa�da
		// do aplicativo
		if ( GameMIDlet.isPlaying() )
			setState( STATE_PAUSED );
		
		//#ifndef LOW_JAR
		pointerStatus = POINTER_STATUS_RELEASED;
		//#endif
	}

	
	public final void showNotify() {
	}

	
	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	private synchronized final void addCrosshairDirection( int direction ) {
		lockCrosshair();
		
		crosshairDirection |= direction;
		
		updateCrosshairAcceleration();
		
		unlockCrosshair();
	}


	/**
	 * Altera o n�vel da farofada.
	 * @param diff varia��o do n�vel da farofada.
	 */
	private final void changeFarofada( int diff ) {
		farofada += diff;
		
		if ( farofada < 0 ) {
			farofada = 0;
		} else if ( farofada >= FAROFADA_MAX ) {
			farofada = FAROFADA_MAX;
			setState( STATE_GAME_OVER );
			updateScore( 0, true );
		}
	
		farofadaBarSpeed.setSpeed( farofada - farofadaShown, false );
	}


	/**
	 * Define a posi��o da mira relativa � tela. Caso ela esteja dentro da �rea de scroll direita ou esquerda, atualiza
	 * a dire��o da c�mera de acordo.
	 * @param pos posi��o da mira relativa � tela.
	 */
	private final void setCrosshairScreenPosition( Point pos ) {
		crosshair.setRefPixelPosition( pos );
		if ( pos.x < CAMERA_START_MOVE_LEFT ) {
			setHandFrame( HAND_FRAME_LEFT );
			cameraSpeed.setSpeed( ( CAMERA_START_MOVE_LEFT - pos.x ) * PLAYSCREEN_CAMERA_MAX_SPEED / 100, false );
		} else if ( pos.x > CAMERA_START_MOVE_RIGHT ) {
			setHandFrame( HAND_FRAME_RIGHT );
			cameraSpeed.setSpeed( ( CAMERA_START_MOVE_RIGHT - pos.x ) * PLAYSCREEN_CAMERA_MAX_SPEED / 100, false );
		} else {
			setHandFrame( HAND_FRAME_CENTER );
			cameraSpeed.setSpeed( 0 );
		}						

	}
	
	
	/**
	 * Atualiza a barra da farofada.
	 * @param equalize indica se a farofada mostrada deve ser automaticamente igualada � farofada real.
	 */
	private final void updateFarofada( int delta, boolean equalize ) {
		if ( farofadaShown != farofada ) {
			if ( equalize ) {
				farofadaShown = farofada;
			} else  {
				farofadaShown += farofadaBarSpeed.updateInt( delta );
				if ( farofadaShown > farofada ) {
					farofadaShown = farofada;
					farofadaBarSpeed.setSpeed( 0 );
				}
			}
			
			farofadaBar.setFarofada( farofadaShown );
		}
	} // fim do m�todo updateFarofada( int, boolean )	
	
	
	private synchronized final void removeCrosshairDirection( int direction ) {
		lockCrosshair();
		
		crosshairDirection ^= crosshairDirection & direction;
		
		updateCrosshairAcceleration();
		
		unlockCrosshair();
	}


	private final void setScoreMultiplier( int scoreMultiplier ) {
		if ( scoreMultiplier > SCORE_MULTIPLIER_MAX )
			this.scoreMultiplier = SCORE_MULTIPLIER_MAX;
		else
			this.scoreMultiplier = ( byte ) scoreMultiplier;
		
		comboCounter = 0;
		
		updateScoreLabel();
	}
	
	
	private final void updateCrosshairAcceleration() {
		switch ( crosshairDirection & DIRECTION_H_MASK ) {
			case DIRECTION_LEFT:
				crosshairAccX.setSpeed( -CROSSHAIR_ACCELERATION, false );
				crosshairSpeedLimit.x = CROSSHAIR_MAX_SPEED;
			break;
			
			case DIRECTION_RIGHT:
				crosshairAccX.setSpeed( CROSSHAIR_ACCELERATION, false );
				crosshairSpeedLimit.x = CROSSHAIR_MAX_SPEED;
			break;
			
			default:
				// jogador n�o est� apertando nem para direita nem para esquerda, ou ent�o est� apertando para direita e
				// esquerda ao mesmo tempo
				crosshairAccX.setSpeed( crosshairSpeedX.getSpeed() >= 0 ? -Math.abs( crosshairAccX.getSpeed() ) : 
										Math.abs( crosshairAccX.getSpeed() ), false );
				crosshairSpeedLimit.x = 0;
			break;
		} // fim switch ( crosshairDirection & DIRECTION_H_MASK )
		
		switch ( crosshairDirection & DIRECTION_V_MASK ) {
			case DIRECTION_UP:
				crosshairAccY.setSpeed( -CROSSHAIR_ACCELERATION, false );
				crosshairSpeedLimit.y = CROSSHAIR_MAX_SPEED;
			break;
			
			case DIRECTION_DOWN:
				crosshairAccY.setSpeed( CROSSHAIR_ACCELERATION, false );
				crosshairSpeedLimit.y = CROSSHAIR_MAX_SPEED;
			break;
			
			default:
				// jogador n�o est� apertando nem para cima nem para baixo, ou ent�o est� apertando para cima e baixo 
				// ao mesmo tempo
				crosshairAccY.setSpeed( crosshairSpeedY.getSpeed() >= 0 ? -Math.abs( crosshairAccY.getSpeed() ) : 
										Math.abs( crosshairAccY.getSpeed() ), false );
				crosshairSpeedLimit.y = 0;
			break;
		} // fim switch ( crosshairDirection & DIRECTION_V_MASK )		
	} // fim do m�todo updateCrosshairAcceleration()
	
	
	private final void updateCrosshair( int delta ) {
		//#ifndef LOW_JAR
		switch ( pointerStatus ) {
			case POINTER_STATUS_DRAGGED:
				final Point previousCrosshairPos = new Point( crosshair.getRefPixelPosition() );
				setCrosshairScreenPosition( lastPointerPos );

				crosshairPosition.addEquals( lastPointerPos.x - previousCrosshairPos.x, lastPointerPos.y - previousCrosshairPos.y );				
			break;
			
			default:
		//#endif
				// atualiza a movimenta��o horizontal da mira
				int speed = crosshairSpeedX.getSpeed();
				speed += crosshairAccX.updateInt( delta );

				int speedTemp = speed > 0 ? speed : -speed;

				// jogador n�o est� movendo a mira horizontalmente
				if ( ( crosshairDirection & DIRECTION_H_MASK ) == DIRECTION_NONE ) {
					crosshairSpeedLimit.x = 0;

					if ( speed > 0 ) {
						if ( crosshairAccX.getSpeed() > 0 )
							crosshairAccX.setSpeed( -crosshairAccX.getSpeed(), false );
					} else {
						if ( crosshairAccX.getSpeed() < 0 )
							crosshairAccX.setSpeed( -crosshairAccX.getSpeed(), false );
					}
				}

				// o teste a seguir a princ�pio n�o seria necess�rio, mas soluciona o problema da mira que come�a a se
				// mover sozinha (quando o jogador p�ra de mover a mira, ela deveria desacelerar at� parar, mas segue
				// acelerando no sentido contr�rio)		
				if ( speedTemp >= crosshairSpeedLimit.x ) {
					if ( crosshairSpeedLimit.x == 0 ) {
						// mira est� desacelerando; caso fique abaixo do m�nimo, p�ra de se mover
						if ( speedTemp <= CROSSHAIR_MIN_SPEED ) {
							speed = 0;
							crosshairAccX.setSpeed( 0 );
						}
					} else {
						// mira chegou � velocidade m�xima
						speed = speed < 0 ? -crosshairSpeedLimit.x : crosshairSpeedLimit.x;
					}
				} // fim if ( speedTemp > crosshairSpeedLimit.x )
				crosshairSpeedX.setSpeed( speed, false );

				// atualiza a movimenta��o vertical da mira
				speed = crosshairSpeedY.getSpeed();
				speed += crosshairAccY.updateInt( delta );

				speedTemp = speed > 0 ? speed : -speed;

				// o teste a seguir a princ�pio n�o seria necess�rio, mas soluciona o problema da mira que come�a a se
				// mover sozinha (quando o jogador p�ra de mover a mira, ela deveria desacelerar at� parar, mas segue
				// acelerando no sentido contr�rio)
				if ( ( crosshairDirection & DIRECTION_V_MASK ) == DIRECTION_NONE ) {
					crosshairSpeedLimit.y = 0;

					if ( speed > 0 ) {
						if ( crosshairAccY.getSpeed() > 0 )
							crosshairAccY.setSpeed( -crosshairAccY.getSpeed(), false );
					} else {
						if ( crosshairAccY.getSpeed() < 0 )
							crosshairAccY.setSpeed( -crosshairAccY.getSpeed(), false );
					}
				}

				if ( speedTemp >= crosshairSpeedLimit.y ) {
					if ( crosshairSpeedLimit.y == 0 ) {
						// mira est� desacelerando; caso fique abaixo do m�nimo, p�ra de se mover
						if ( speedTemp <= CROSSHAIR_MIN_SPEED ) {
							speed = 0;
							crosshairAccY.setSpeed( 0 );
						}
					} else {
						// mira chegou � velocidade m�xima
						speed = speed < 0 ? -crosshairSpeedLimit.y : crosshairSpeedLimit.y;
					}
				} // fim if ( speedTemp > crosshairSpeedLimit.y )
				crosshairSpeedY.setSpeed( speed, false );

				// move a mira de acordo com a �ltima velocidade registrada, respeitando os limites da tela e do n�vel
				crosshairPosition.addEquals( crosshairSpeedX.updateInt( delta ), crosshairSpeedY.updateInt( delta ) );
				if ( crosshairPosition.x < 0 )
					crosshairPosition.x = 0;
				else if ( crosshairPosition.x > beach.getSize().x )
					crosshairPosition.x = beach.getSize().x;

				if ( crosshairPosition.y < 0 )
					crosshairPosition.y = 0;
				else if ( crosshairPosition.y > size.y )
					crosshairPosition.y = size.y;
		
		//#ifndef LOW_JAR
		} // fim switch ( pointerStatus )
		//#endif
		
		final Point pos = new Point( crosshairPosition );
		if ( pos.x < 0 )
			pos.x = 0;
		else if ( pos.x > crosshairLimitMax.x )
			pos.x = crosshairLimitMax.x;

		if ( pos.y < 0 )
			pos.y = 0;
		else if ( pos.y > crosshairLimitMax.y )
			pos.y = crosshairLimitMax.y;

		// atualiza a c�mera de acordo com a posi��o da mira
		// pos agora armazena a posi��o relativa da mira na tela
		beach.move( cameraSpeed.updateInt( delta ), 0 );
		pos.addEquals( beach.getPosition() );
		
		setCrosshairScreenPosition( pos );
	} // fim do m�todo updateCrosshair( int )
	
	
	private final void setHandFrame( byte frame ) {
		hand.setFrame( frame );
		hand.defineReferencePixel( hand.getSize().x >> 1, hand.getSize().y );
		hand.setRefPixelPosition( size.x >> 1, size.y );
	}
	
	
	private synchronized final void resetCrosshairDirection() {
		lockCrosshair();
		
		crosshairDirection = DIRECTION_NONE;
		crosshairSpeedLimit.set( 0, 0 );
		crosshairAccX.setSpeed( 0 );
		crosshairAccY.setSpeed( 0 );
		crosshairSpeedX.setSpeed( 0 );
		crosshairSpeedY.setSpeed( 0 );
		
		unlockCrosshair();
	}
	
	
	private final void prepare( int level ) {
		final int DIFFICULTY_LEVEL = Math.min( level, MAX_LEVEL );
		
		this.level = ( short ) level;
		
		try {
			beach.prepare( DIFFICULTY_LEVEL );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			GameMIDlet.exit();
		}
		
		// atualiza o limite m�ximo da mira
		crosshairLimitMax.set( beach.getSize().x, size.y );
		
		// posiciona a mira e a c�mera no centro do n�vel atual
		crosshairPosition.set( beach.getSize().x >> 1, size.y >> 1 );
		beach.setPosition( ( size.x - beach.getSize().x ) >> 1, beach.getPosition().y );
		crosshair.setRefPixelPosition( crosshairPosition );
		crosshair.getPosition().addEquals( beach.getPosition() );
		
		if ( DIFFICULTY_LEVEL <= 1 ) {
			setScoreMultiplier( 1 );
			farofada = 0;
			resetScore();
		} else {
			// decrementa apenas parte da farofada ao passar de n�vel
			changeFarofada( FAROFADA_DECREASE_PER_LEVEL );
			updateFarofada( 0, true );
		}
		
		farofadaSpeed.setSpeed( FAROFADA_INCREASE_PER_SECOND_INITIAL + ( FAROFADA_INCREASE_PER_SECOND_FINAL - FAROFADA_INCREASE_PER_SECOND_INITIAL ) * DIFFICULTY_LEVEL / MAX_LEVEL );
		updateScore( 0, true );
	}
	
	
	public final void prepareToNextLevel() {
		prepare( level + 1 );
	}
	
	
	public synchronized final void setState( int state ) {
		resetCrosshairDirection();
		
		switch ( state ) {
			case STATE_NONE:
			break;
			
			case STATE_PLAYING_SHOW_PAUSE:
				//#ifndef LOW_JAR
				pointerStatus = POINTER_STATUS_RELEASED;
				//#endif
				
				showMessage( -1 );
				
				messageTime = TIME_PAUSE;
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, farofadaBar, true, 0 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_PAUSE );
				
				lastUnpauseTime = System.currentTimeMillis();
				
				// em alguns aparelhos Gradiente, o desempenho do jogo fica muito ruim caso a m�sica ambiente
				// esteja ativa.
				//#ifndef DONT_PLAY_AMBIENT_MUSIC
				if ( !MediaPlayer.isPlaying() ) {
					//#ifdef LOW_JAR
//# 					if ( GameMIDlet.getVendor() == GameMIDlet.VENDOR_NOKIA ) {
//# 						final Thread loadSoundThread = new Thread() {
//# 							public void run() {
//# 								MediaPlayer.play( SOUND_AMBIENT, MediaPlayer.LOOP_INFINITE );
//# 							}
//# 						};
//# 						loadSoundThread.setPriority( Thread.MIN_PRIORITY );
//# 						loadSoundThread.start();
//# 					} else {
//# 						MediaPlayer.play( SOUND_AMBIENT, MediaPlayer.LOOP_INFINITE );
//# 					}
					//#else
					MediaPlayer.play( SOUND_AMBIENT, MediaPlayer.LOOP_INFINITE );
					//#endif
				}
				//#endif				
			break;
			
			case STATE_PLAYING_SHOW_SCORE:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, scoreLabel, false, 0 );
			break;
			
			case STATE_PAUSED:
				switch ( this.state ) {
					case STATE_PAUSED:
					case STATE_END_LEVEL_MESSAGE:
					case STATE_END_LEVEL_CHANGE:
					case STATE_GAME_OVER:
					return;
				}
				
				previousState = this.state;
				GameMIDlet.setScreen( SCREEN_PAUSE );
			break;
			
			case STATE_GAME_OVER:
				MediaPlayer.play( SOUND_GAME_OVER, 1 );
				
				// enche a praia de farofeiros
				beach.invadeFarofers();
				
				handSpeed.setSpeed( hand.getSize().y );
				crosshairSpeedY.setSpeed( size.y >> 1 );
				
				showMessage( TEXT_GAME_OVER );
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null, true, 0 );
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null, true, 0 );
			break;
			
			case STATE_BEGIN_LEVEL:
				showMessage( -1 );
				
				setHandFrame( HAND_FRAME_CENTER );
				hand.setRefPixelPosition( size.x >> 1, size.y + hand.getSize().y );
				crosshair.setRefPixelPosition( crosshair.getRefPixelX(), ( size.y * 3 ) >> 1 );
				
				handSpeed.setSpeed( -hand.getSize().y >> 1 );
				crosshairSpeedY.setSpeed( -size.y >> 1 );
			break;
			
			case STATE_END_LEVEL_MESSAGE:
				handSpeed.setSpeed( hand.getSize().y );
				crosshairSpeedY.setSpeed( size.y >> 1 );
				
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null, true, 0 );
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null, true, 0 );				
				
				showMessage( TEXT_LEVEL_END );
			break;			
			
			case STATE_END_LEVEL_CHANGE:
				//#if BRANDS == "true"
				// na vers�o com propagandas, a cada troca de n�vel exibe a tela de uma das marcas
				GameMIDlet.setScreen( SCREEN_BRAND );
				break;
				//#else
//# 				// na vers�o sem marcas, apenas troca de tela
//# 				this.state = ( byte ) state;
//# 				GameMIDlet.setScreen( SCREEN_NEXT_LEVEL );
//# 				return;
				//#endif
			
			//#if DEBUG == "true"
			default:
				System.out.println( "PlayScreen.setState->estado inv�lido: " + state );
			//#endif
		} // fim switch ( state )
		
		this.state = ( byte ) state;
	}
	
	
	public final void unpause() {
		//#if DEBUG == "true"
		System.out.println( "unpause: " + state + ", " + previousState );
		//#endif
		
		switch ( previousState ) {
			case STATE_PLAYING_SHOW_PAUSE:
			case STATE_PLAYING_SHOW_SCORE:
				//#ifndef LOW_JAR
				// evita que a mira esteja sobre a �rea de pausa caso o jogador a tenha deixado ali antes da pausa,
				// caso contr�rio torna-se dif�cil arrast�-la de volta utilizando-se o ponteiro
				final Point crosshairLimit = new Point( size.x - ( crosshair.getSize().x >> 1 ), size.y - ( crosshair.getSize().y >> 1 ) );
				final Point diff = new Point( crosshair.getRefPixelPosition().x - crosshairLimit.x, crosshair.getRefPixelPosition().y - crosshairLimit.y );
				if ( diff.x > 0 && diff.y > 0 ) {
					crosshairPosition.sub( diff );
					setCrosshairScreenPosition( crosshairLimit );
				}
				//#endif
				
				setState( STATE_PLAYING_SHOW_PAUSE );
			break;
			
			case STATE_END_LEVEL_CHANGE:
			case STATE_NONE:
				setState( STATE_BEGIN_LEVEL );
			break;
			
			case STATE_BEGIN_LEVEL:
				state = previousState;
			break;
			
			default:
				return;
		}
		previousState = STATE_NONE;
	}
	
	
	public final void update( int delta ) {
		//#if DEBUG == "true"
		try {
		//#endif
		
		switch ( state ) {
			case STATE_NONE:
			break;
			
			case STATE_PLAYING_SHOW_PAUSE:
				messageTime -= delta;
				
				if ( messageTime <= 0 )
					setState( STATE_PLAYING_SHOW_SCORE );
				
			case STATE_PLAYING_SHOW_SCORE:
				updateCrosshair( delta );

				updateScore( delta, false );
				changeFarofada( farofadaSpeed.updateInt( delta ) );
				updateFarofada( delta, false );				
			break;
			
			case STATE_PAUSED:
			return;
			
			case STATE_BEGIN_LEVEL:
				updateHandsAnimation( delta );
				if ( crosshair.getRefPixelY() <= ( size.y >> 1 ) ||
					 hand.getRefPixelY() <= size.y ) {
				
					crosshair.setRefPixelPosition( crosshair.getRefPixelX(), size.y >> 1 );
					crosshairSpeedY.setSpeed( 0 );
					
					setHandFrame( HAND_FRAME_CENTER );
					setState( STATE_PLAYING_SHOW_PAUSE );
				}
			break;

			case STATE_END_LEVEL_MESSAGE:
				messageTime -= delta;
				
				if ( messageTime <= 0 )
					setState( STATE_END_LEVEL_CHANGE );				
				
			case STATE_GAME_OVER:
				if ( messageTime >= 0 )
					messageTime -= delta;
				
				if ( hand.getPosition().y < size.y || crosshair.getPosition().y < size.y )
					updateHandsAnimation( delta );
			break;
			
			case STATE_END_LEVEL_CHANGE:
			break;
			
			//#if DEBUG == "true"
			default:
				System.out.println( "PlayScreen.update->estado inv�lido: " + state );
			//#endif
		} // fim switch ( state )
		
		super.update( delta );
		
		//#if DEBUG == "true"
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		//#endif
	} // fim do m�todo update( int )
	
	
	private final void changeScore( int diff ) {
		score += diff;
		
		scoreSpeed.setSpeed( score - scoreShown, false );
	}


	private final void updateHandsAnimation( int delta ) {
		final int dy = handSpeed.updateInt( delta );
		hand.move( 0, dy );
		
		crosshair.move( 0, crosshairSpeedY.updateInt( delta ) );
	}
	
	
	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			updateScoreLabel();
		}
	} // fim do m�todo updateScore( boolean )	
	
	
	private final void updateScoreLabel() {
		scoreLabel.setText( "<ALN_R>" + scoreMultiplier + "X\n" + scoreShown );
	}
	
	
	private final void showMessage( int index ) {
		if ( index >= 0 ) {
			labelMessage.setText( GameMIDlet.getText( index ) );
			labelMessage.setVisible( true );		
			messageTime = TIME_MESSAGE;
		} else {
			labelMessage.setVisible( false );
		}
	}
	
	
	private synchronized final void lockCrosshair() {
		while ( usingCrosshair ) {
			try {
				wait();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}
		} // fim while ( usingCrosshair )
		usingCrosshair = true;		
	} // fim do m�todo lockCrosshair()
	

	private synchronized final void unlockCrosshair() {
		usingCrosshair = false;
		notifyAll();		
	} // fim do m�todo unlockCrosshair()	
	
	
	public final int getScore() {
		return score;
	}

	
	private static final class Hand extends Sprite {
		
		private static final Hand loadHand() throws Exception {
			//#ifdef LOW_JAR
//# 			final byte HAND_TOTAL_FRAMES = 1;
			//#else
			final byte HAND_TOTAL_FRAMES = 3;
			//#endif
			
			final Image[] handFrames = new Image[ HAND_TOTAL_FRAMES ];
			for ( byte i = 0; i < HAND_TOTAL_FRAMES; ++i )
				handFrames[ i ] = ImageLoader.loadImage( PATH_IMAGES + "hand_" + i + ".png" );
			
			final byte[][] sequences = new byte[ 1 ][ HAND_TOTAL_FRAMES ];
			for ( byte i = 0; i < HAND_TOTAL_FRAMES; ++i )
				sequences[ 0 ][ i ] = i;
			
			return new Hand( handFrames, sequences );
		}
		
		
		private Hand( Image[] frames, byte[][] sequences ) {
			super( frames, sequences, new short[] { 0 } );
		}

		
		public final void setFrame( int frame ) {
			//#ifdef LOW_JAR
//# 			super.setFrame( 0 );
			//#else
			super.setFrame( frame );
			
			final Image img = getCurrentFrameImage();
			setSize( img.getWidth(), img.getHeight() );
			//#endif
			
			setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( size.x >> 1 ), ScreenManager.SCREEN_HEIGHT - size.y );
		}
		
	} // fim da classe interna Hand
	
	
	public final void resetScore() {
		score = 0;
		scoreShown = 0;
	}


	//#ifndef LOW_JAR
	public final void onPointerDragged( int x, int y ) {
		switch ( state ) {
			case STATE_PLAYING_SHOW_PAUSE:
			case STATE_PLAYING_SHOW_SCORE:
				if ( pointerStatus != POINTER_STATUS_RELEASED || crosshair.contains( x, y ) ) {
					pointerStatus = POINTER_STATUS_DRAGGED;

					lastPointerPos.set( x, y );
				}
			break;
		}
	}


	public final void onPointerPressed( int x, int y ) {
		switch ( state ) {
			case STATE_PLAYING_SHOW_PAUSE:
			case STATE_PLAYING_SHOW_SCORE:
				if ( crosshair.contains( x, y ) ) {
					lastPointerPressedTime = System.currentTimeMillis();
					pointerStatus = POINTER_STATUS_PRESSED;

					lastPointerPos.set( x, y );
				}
			break;
			
			case STATE_GAME_OVER:
				// confirma o fim de jogo
				keyPressed( ScreenManager.FIRE );
			break;
		}
	}


	public final void onPointerReleased( int x, int y ) {
		switch ( state ) {		
			case STATE_PLAYING_SHOW_PAUSE:
			case STATE_PLAYING_SHOW_SCORE:
				final long interval = System.currentTimeMillis() - lastPointerPressedTime;
				if ( interval < POINTER_CLICK_TIME_TOLERANCE || ( pointerStatus == POINTER_STATUS_PRESSED && crosshair.contains( x, y ) ) ) {
					keyPressed( ScreenManager.FIRE );
				}				
			break;
		}
		
		pointerStatus = POINTER_STATUS_RELEASED;
	}
	//#endif
	
	
}
