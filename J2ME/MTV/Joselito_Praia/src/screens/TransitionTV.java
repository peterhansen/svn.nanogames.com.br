/*
 * TransitionTV.java
 *
 * Created on December 11, 2007, 10:35 PM
 *
 */
//#ifndef LOW_JAR

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.UpdatablePattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public final class TransitionTV extends DrawableGroup implements Constants, Updatable {
	
	/** quantidade total de drawables no grupo */
	private static final byte TOTAL_ITEMS = 7;
    
	private static final byte DRAWABLE_ITEMS = 3;    
    
    private static final byte TRANSITION_INACTIVE   = 0;
    private static final byte TRANSITION_APPEARING  = 1;
    private static final byte TRANSITION_SHOWN      = 2;
    private static final byte TRANSITION_HIDING     = 3;

	/** estado atual de transi��o */
	private byte transitionState;
    
	/** Tela atual (que ser� substitu�da). */
	private Drawable previousScreen;
	
	/** Pr�xima tela a ser definida como ativa pelo ScreenManager */
	private Drawable nextScreen;
	
	private final ScreenManager manager;
	
	/** cor s�lida de preenchimento de fundo */
	private final Pattern background;
	
	/** "chuvisco" da tv */
	private final UpdatablePattern interference;
	
	/** padr�o de cores */
	private final DrawableGroup colorsGroup;
	
	private static final int[] COLORS = { 
		0xffffff,
		0xffff00,
		0x00ffff,
		0x00ff00,
		0xffff00,
		0xff0000,
		0x0000ff,
		0x000000
	};
	
	/** dura��o m�xima da interfer�ncia (chuvisco) da tv */
	private static final short MAX_INTERFERENCE_TIME = 333;
	
	/** dura��o da tela do padr�o de cores */
	private static final short COLORS_TIME = 1700;
	
	/** tempo restante at� a pr�xima transi��o */
	private short remainingTime;
	
	private static final byte FLASH_STATE_WHITE	= 0;
	private static final byte FLASH_STATE_DONE	= 1;
	
	private byte flashState;

	private boolean showColors;
    
   	private final Mutex mutex = new Mutex();
	

	public TransitionTV( ScreenManager manager ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.manager = manager;
		
		// insere o background de cor s�lida
		background = new Pattern( null );
		insertDrawable( background );
		
		// insere o pattern animado do chuvisco
		final byte INTERFERENCE_TOTAL_IMAGES = 3;
		final short INTERFERENCE_FRAME_TIME = 81;
		
		final Image[] images = new Image[ INTERFERENCE_TOTAL_IMAGES ];
		for ( byte i = 0; i < INTERFERENCE_TOTAL_IMAGES; ++i )
			images[ i ] = ImageLoader.loadImage( PATH_IMAGES + "tv_" + i + ".png" );
        
        Thread.yield();
        
		final byte[][] sequences = new byte[][] { { 0, 1, 2 } };
		final Sprite sprite = new Sprite( images, sequences, new short[] { INTERFERENCE_FRAME_TIME } );
		
		// cria o padr�o de cores
		colorsGroup = new DrawableGroup( COLORS.length );
		insertDrawable( colorsGroup );
		final int COLOR_WIDTH = ScreenManager.SCREEN_WIDTH / COLORS.length;
		for ( int i = 0, x = 0; i < COLORS.length; ++i, x += COLOR_WIDTH ) {
			final Pattern color = new Pattern( null );
			color.setFillColor( COLORS[ i ] );
			
			if ( i < COLORS.length - 1 )
				color.setSize( COLOR_WIDTH, ScreenManager.SCREEN_HEIGHT );
			else {
				// em aparelhos com telas n�o m�ltiplas da quantidade de cores, a �ltima cor pode n�o cobrir toda
				// a largura da tela.
				color.setSize( COLOR_WIDTH + ( ScreenManager.SCREEN_WIDTH % COLORS.length ), ScreenManager.SCREEN_HEIGHT );
			}
			
			color.setPosition( x, 0 );
			colorsGroup.insertDrawable( color );
		}
		
		interference = new UpdatablePattern( sprite );
		insertDrawable( interference );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public synchronized final void setNextScreen( Drawable nextScreen, boolean forceColors ) {
		while ( activeDrawables > DRAWABLE_ITEMS )
			removeDrawable( 0 );
		
		previousScreen = ScreenManager.getInstance().getCurrentScreen();
		if ( previousScreen == this ) {
			previousScreen = null;
		}
		
		this.nextScreen = nextScreen;
        
        if ( previousScreen != null )
            insertDrawable( previousScreen, 0 );
        
		insertDrawable( nextScreen, 0 );
		
		showColors = forceColors;
		
		manager.setCurrentScreen( this );
        setTransitionState( TRANSITION_APPEARING );
	}


	public synchronized final void setTransitionState( byte state ) {
		mutex.acquire();
        
		switch ( state ) {
			case TRANSITION_APPEARING:
				interference.setVisible( true );
				background.setVisible( false );
				remainingTime = ( short ) NanoMath.randInt( MAX_INTERFERENCE_TIME );
				
				showColors = showColors || ( remainingTime > MAX_INTERFERENCE_TIME * 93 / 100 );
			break;
			
			case TRANSITION_SHOWN:
				interference.setVisible( true );
				background.setVisible( true );
				remainingTime = ( short ) NanoMath.randInt( MAX_INTERFERENCE_TIME );
			break;
			
			case TRANSITION_HIDING:
				setFlashState( FLASH_STATE_WHITE );
				
				remainingTime = MAX_INTERFERENCE_TIME;
			break;
			
			case TRANSITION_INACTIVE:
				GameMIDlet.setSoftKeysInTransition( false );
                manager.setCurrentScreen( nextScreen );
                nextScreen = null;

			break;
		}
		
		transitionState = state;
		
		mutex.release();
	}

	
	public final void update( int delta ) {
//		super.update( delta );
        interference.update( delta );
		
		switch ( transitionState ) {
			case TRANSITION_APPEARING:
			case TRANSITION_SHOWN:
				remainingTime -= delta;
				
				if ( remainingTime <= 0 ) {
					// terminou uma fase do "chuvisco"
					if ( transitionState == TRANSITION_APPEARING ) {
						setTransitionState( ( byte ) ( transitionState + 1 ) );
					} else {
						if ( showColors ) {
							colorsGroup.setVisible( true );
							GameMIDlet.setSoftKeysInTransition( true);
							remainingTime = COLORS_TIME;
							background.setVisible( false);
							interference.setVisible( false );							
							showColors = false;
						} else {
                            setTransitionState( TRANSITION_HIDING );
						}
					}
				}
			break;
			
			case TRANSITION_HIDING:
				switch ( flashState ) {
					case FLASH_STATE_WHITE:
						setFlashState( flashState + 1 );
//					break;
//					
//					case FLASH_STATE_DONE:
//						remainingTime -= delta;
//						
//						if ( remainingTime <= 0 )
							setTransitionState( TRANSITION_INACTIVE );
					break;
				}
			break;
		}		
	}
	
	
	private final void setFlashState( int flashState ) {
		switch ( flashState ) {
			case FLASH_STATE_WHITE:
				interference.setVisible( true );
				background.setVisible( true );
				background.setFillColor( 0xffffff );
                
                previousScreen = null;
                System.gc();
				while ( activeDrawables > DRAWABLE_ITEMS + 1 )
					removeDrawable( 0 );	                
			break;
			
			case FLASH_STATE_DONE:
				colorsGroup.setVisible( false );
				background.setVisible( false);
//				interference.setVisible( false );
			break;
			
		}
		
		this.flashState = ( byte ) flashState;
	}
    
    
	protected final void paint( Graphics g ) {
		mutex.acquire();
		
		super.paint( g );
		
		mutex.release();
	}    


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		background.setSize( size );
		interference.setSize( size );
		colorsGroup.setSize( size );
		for ( byte i = 0; i < colorsGroup.getUsedSlots(); ++i ) {
			final Drawable d = colorsGroup.getDrawable( i );
			d.setSize( d.getSize().x, size.y );
		}
	}	
	
}

//#endif