-- descritor para conversão de arquivos para o formato Potenza
-- Peter Hansen

-- exemplos de aparelhos-chefe com poucos caracteres
-- gradiente gf690: grgf690
-- motorola v3: mtv3
-- nokia n76: nkn76
-- nokia 5200: nk5200
-- samsung d820: sgd820
-- samsung c420: sgc420
-- lg me970: lgme970
-- lg mg225d: lgmg225d

-- nome do aplicativo/jogo
APP_NAME = [[Barrigadas Ornamentais]]

-- caminho do arquivo original de especificação do aplicativo J2ME
PATH_APP_SPEC = [[326002-EST001-NanoGames.doc]]

-- caminho do formulário original de aprovação de aplicações Vivo Downloads
PATH_FAAVD = [[FAAVD_supportcomm.doc]]

-- caminho do arquivo original de solicitação de teste
PATH_SOL = [[326002-SOL001.01.1.xls]]

VERSIONS = {
	{
		-- big
		  ['jad'] = [[big.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.0]]
		, ['device'] = [[nkn95]]
	},
	{
		-- c420
		  ['jad'] = [[c420.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.1]]
		, ['device'] = [[sgc425]]
	},
	{
		-- d820
		  ['jad'] = [[d820.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.2]]
		, ['device'] = [[sgd836]]
	},
	{
		-- gf690
		  ['jad'] = [[gf690.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.3]]
		, ['device'] = [[grgf690]]
	},
	{
		-- gf930
		  ['jad'] = [[gf930.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.4]]
		, ['device'] = [[grgf930]]
	},
	{
		-- me970
		  ['jad'] = [[me970.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.5]]
		, ['device'] = [[lgme970]]
	},
	{
		-- medium
		  ['jad'] = [[medium.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.6]]
		, ['device'] = [[mtv3]]
	},
	{
		-- min
		  ['jad'] = [[min.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.8]]
		, ['device'] = [[lgmg225d]]
	},
	{
		-- small
		  ['jad'] = [[small.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.7]]
		, ['device'] = [[nk5200]]
	},
}

