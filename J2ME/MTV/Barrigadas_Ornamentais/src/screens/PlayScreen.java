/**
 * PlayScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 2, 2008 6:50:28 PM.
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Serializable;
import core.Boca;
import core.Constants;
import core.MoveQueue;
import core.SplashEmitter;
import core.SpringBoard;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 * 
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif
{
	
	public static final byte STATE_NONE					= 0;
	public static final byte STATE_BEGIN_LEVEL_X		= STATE_NONE + 1;
	public static final byte STATE_BEGIN_LEVEL_Y		= STATE_BEGIN_LEVEL_X + 1;
	public static final byte STATE_CHOOSE_MOVES			= STATE_BEGIN_LEVEL_Y + 1;
	public static final byte STATE_PRESS_TO_JUMP		= STATE_CHOOSE_MOVES + 1;
	public static final byte STATE_PREPARING_JUMP		= STATE_PRESS_TO_JUMP + 1;
	public static final byte STATE_JUMP_TO_SPRINGBOARD	= STATE_PREPARING_JUMP + 1;
	public static final byte STATE_SET_JUMP				= STATE_JUMP_TO_SPRINGBOARD + 1;
	public static final byte STATE_JUMPING				= STATE_SET_JUMP + 1;
	public static final byte STATE_HIT_POOL				= STATE_JUMPING + 1;
	public static final byte STATE_ON_POOL				= STATE_HIT_POOL + 1;
	public static final byte STATE_SHOW_RESULT_WAIT		= STATE_ON_POOL + 1;
	public static final byte STATE_SHOW_RESULT_2		= STATE_SHOW_RESULT_WAIT + 1;
	public static final byte STATE_LEVEL_CLEARED		= STATE_SHOW_RESULT_2 + 1;
	public static final byte STATE_GAME_OVER			= STATE_LEVEL_CLEARED + 1;
	public static final byte STATE_GAME_OVER_RECORD		= STATE_GAME_OVER + 1;


	private byte state;
	
	public static final short TIME_MESSAGE = 3200;
	
	public static final short TIME_MOVE_UP = 2000;
	
	/** Dura��o em milisegundos da prepara��o para o pulo do Bo�a. */
	private static final short TIME_PREPARE_JUMP = 900;
	
	private static final short TIME_ON_POOL_WAIT = 5950;
	
	private short timeToNextState;
	
	/** Pontua��o do jogador. */
	private static int score;
	
	private static short level;
	
	/** �ndice do n�vel de treinamento. */
	public static final byte LEVEL_TRAINING = 0;
	
	/** pontua��o exibida atualmente */
	private static int scoreShown;
	
	/** Pontua��o obtida no n�vel atual. */
	private int scoreLevel;
	
	/** label que indica a pontua��o. */
	private final Label scoreLabel;	
	
	/** MUV que controla a velocidade de entrada/sa�da do label de pontos. */
	private final MUV scoreLabelMUV = new MUV();
	
	/** Label utilizado para mostrar uma mensagem ao jogador (fim de jogo, n�vel completo, etc.).  */
	private final RichLabel labelMessage;
	
	/** Taxa de intermit�ncia do label. */
	private static final short LABEL_BLINK_RATE = 700;
	
	private short labelBlinkTime;
	
	/** pontua��o m�xima mostrada */
	private static final int SCORE_SHOWN_MAX = 2097152000;

	/** Velocidade de atualiza��o da pontua��o. */
	private final MUV scoreSpeed = new MUV();	
	
	/** Fila dos movimentos do jogador. */
	private final MoveQueue moveQueue;
	
	private final DrawableImage platform;
	private final DrawableImage platformTop;
	private final Pattern platformPattern;
	
	/** Trampolim. */
	private final SpringBoard springBoard;
	
	/** Trampolim "dummy", que mostra a posi��o �tima do pulo. */
	private final SpringBoard springBoardOptimal;
	
	/** Tempo que o trampolim "dummy" permanece vis�vel. */
	private static final byte SPRINGBOARD_VISIBLE_TIME = 100;
	
	/** Vari�vel utilizada para fazer o trampolim "dummy" piscar durante a prepara��o do pulo. */
	private short springBoardAccTime;
	
	/** Seta indicando a posi��o ideal do pulo. */
	private final DrawableImage jumpArrow;
	
	/** Label indicando para pressionar o bot�o de pulo. */
	private final RichLabel jumpLabel;
	
	private final Pattern pool;
	private final Pattern underWaterPattern;
	private final DrawableImage poolRight;
	private final Pattern poolBorder;
	
	private final Pattern stadium;
	
	//#if SCREEN_SIZE != "SMALL"
	/** Grupo dos controles para uso de ponteiros. */
	private final DrawableGroup pointerControls;
	
	/** Velocidade de entrada/sa�da dos controles de ponteiros. */
	private final MUV pointerMUV = new MUV();
	
	/** �ndice do �ltimo controle de ponteiro pressionado. */
	private byte lastPointerControl = -1;
	
	//#endif
	
	/** Grupo que cont�m os sprites da torcida. */
	private final DrawableGroup crowdGroup;
	
	/** Altura da maior plataforma alcan�ada pelo jogador. */
	private static int fp_highestPlatform = FP_PLATFORM_CHOOSE_MIN_HEIGHT;
	
	/** Velocidade inicial vertical do pulo, em metros por segundo. */
	private int fp_jumpInitialYSpeed;
	
	/** Altura m�xima do pulo (valor utilizado para fazer a anima��o do trampolim). */
	private int fp_jumpMaxHeight;
	
	/** Precis�o da altura do pulo. */
	private int fp_jumpPrecision;
	
	/** Armazena a precis�o da barrigada no momento do impacto - como o Bo�a muda de frame e rota��o ao mergulhar na 
	 * piscina, o valor deve ser guardado antes. */
	private int fp_splashPrecision;
	
	/** Acelera��o vertical do pulo do Bo�a durante o trampolim. */
	private int fp_jumpAccelerationY;
	
	/** Velocidade horizontal do pulo, em metros por segundo. */
	private int fp_jumpXSpeed;
	
	/** Tempo acumulado do pulo em segundos. */
	private int fp_jumpAccTime;
	
	/** Posi��o vertical inicial do pulo. */
	private static int fp_jumpInitialY;
	
	/** Posi��o vertical inicial do pulo. */
	private int jumpInitialY;
	
	/** Posi��o horizontal da ponta do trampolim. */
	private int jumpInitialX;
	
	public final Boca boca;
	
	private final SplashEmitter splashEmitter;
	
	private static final int FP_100 = 6553600; // 100
	
	
	//<editor-fold desc="IMAGENS PR�-ALOCADAS" defaultstate="collapsed">
	private static DrawableImage imgStar;
	private static DrawableImage imgSun;
	private static DrawableImage imgMoon;
	private static DrawableImage imgPlatform;
	private static DrawableImage imgPlatformTop;
	private static DrawableImage imgPlatformPattern;
	private static DrawableImage imgStadium;
	private static DrawableImage imgJumpArrow;
	private static DrawableImage imgPool;
	private static DrawableImage imgPoolRight;
	private static DrawableImage imgSky;
	private static DrawableImage[] imgClouds = new DrawableImage[ 2 ];
	private static Sprite spriteCrowd;
	//</editor-fold>
	
	//<editor-fold desc="PAR�METROS DA C�MERA" defaultstate="collapsed">
	
	/** Posi��o y do topo da tela ao mostrar o jogador na plataforma. */
	private short CAMERA_PLATFORM_Y;
	
	/** Posi��o y (topo) m�xima da c�mera. */
	private final short CAMERA_MAX_Y;
	
	/** Posi��o y da c�mera ao mostrar a piscina. */
	private final short CAMERA_MIN_Y;
	
	/** Posi��o y da c�mera ao mostrar a piscina. */
	private final int FP_CAMERA_MIN_Y;
	
	/** Posi��o x esquerda m�nima da c�mera. */
	private final short CAMERA_MIN_X;
	
	/** Posi��o x m�xima da c�mera. */
	private final short CAMERA_MAX_X;
	
	/** Posi��o y utilizada como o zero real. */
	private final short GROUND_Y;
	
	/** Velocidade da c�mera. */
	private int fp_cameraSpeed;
	
	/** Posi��o atual da c�mera. */
	private int fp_cameraY;
	
	/** Tempo total em segundos at� a c�mera chegar � posi��o de destino. */
	private int fp_cameraTotalTime;
	
	/** Tempo atual da c�mera � acumulado para agilizar teste de chegada ao destino. */
	private int fp_cameraAccTime;
	
	// estados da c�mera
	private static final byte CAMERA_STATE_STOPPED						= 0;
	private static final byte CAMERA_STATE_MOVING						= 1;
	private static final byte CAMERA_STATE_FOLLOW_PLAYER_1				= 2;
	private static final byte CAMERA_STATE_FOLLOW_PLAYER_2				= 3;
	private static final byte CAMERA_STATE_FOLLOW_PLAYER_3				= 4;
	private static final byte CAMERA_STATE_FOLLOW_PLAYER_4				= 5;
	
	private static final int FP_FOLLOW_PLAYER_JUMP_PERCENT = 32768; //NanoMath.divInt( 50, 100 );
	
	/** Estado atual da c�mera. */
	private byte cameraState;
	
	/** Dist�ncia inicial do jogador relativa ao topo da tela enquanto ele est� no pulo. */
	private short cameraInitialPlayerOffset;
	
	/** Dist�ncia final do jogador relativa ao topo da tela enquanto ele est� no pulo. */
	private short cameraFinalPlayerOffset;
	
	/** Velocidade horizontal da c�mera. */
	private final MUV cameraXSpeed = new MUV();
	
	//</editor-fold>	
	
	/** Grupos do fundo da tela. Eles s�o divididos em 3 para poder realizar o paralaxe. */
	private final UpdatableGroup[] background = new UpdatableGroup[ 3 ];
	
	/** �ndice do background que cont�m as montanhas, c�u e etc. */
	private static final byte BKG_BACKGROUND	= 0;
	/** �ndice do background que cont�m a torcida. */
	private static final byte BKG_MIDDLE		= 1;
	/** �ndice do background que cont�m o personagem, a piscina e a plataforma. */
	private static final byte BKG_FOREGROUND	= 2;
	

	public PlayScreen() throws Exception {
		super( 10 );
		
		final int PLAYSCREEN_TOTAL_HEIGHT = NanoMath.toInt( NanoMath.mulFixed( FP_PLAYSCREEN_TOTAL_HEIGHT, FP_METER_TO_PIXELS ) );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		//#if SCREEN_SIZE == "SMALL"
			//#if JAR == "min"
//# 			final byte MAX_BKG_ELEMENTS = 5;
			//#else
//# 			final byte MAX_BKG_ELEMENTS = ( byte ) ( GameMIDlet.isLowMemory() ? 7 : 21 );
			//#endif
		//#else
		final byte MAX_BKG_ELEMENTS = ( byte ) ( GameMIDlet.isLowMemory() ? 10 : 29 * size.x / 176 );
		//#endif
		
		for ( byte i = 0; i < background.length; ++i ) {
			background[ i ] = new UpdatableGroup( 20 + MAX_BKG_ELEMENTS );
			background[ i ].setSize( PLAYSCREEN_TOTAL_WIDTH, convertPos( PLAYSCREEN_TOTAL_HEIGHT, i ) );
			insertDrawable( background[ i ] );
		}
		
		
		// calcula a posi��o x m�nima (esquerda) que pode ser visualizada, para que n�o sejam feitas inser��es de drawables
		// que jamais ser�o vistos
		final int[] MIN_X = new int[ 3 ];
		final int[] MAX_X_DIFF = new int[ 3 ];
		MIN_X[ BKG_FOREGROUND ] = PLAYSCREEN_TOTAL_WIDTH - ScreenManager.SCREEN_HALF_WIDTH - imgPlatform.getWidth() + PLATFORM_RIGHT_X_OFFSET + toPixels( FP_JUMP_X_OFFSET );
		// o -30 � para corrigir um erro no posicionamento da camada, onde uma �rea da parte esquerda da tela 
		// n�o � desenhada em algumas situa��es 
		for ( byte i = 0; i < BKG_FOREGROUND; ++i )
			MIN_X[ i ] = convertPos( MIN_X[ BKG_FOREGROUND ] - 30, i );
		
		for ( byte i = 0; i < 3; ++i )
			MAX_X_DIFF[ i ] = PLAYSCREEN_TOTAL_WIDTH - MIN_X[ i ];
		
		// insere o fundo de tela das estrelas
		final DrawableGroup groupStar = new DrawableGroup( MAX_BKG_ELEMENTS );
		groupStar.setSize( background[ BKG_BACKGROUND ].getWidth(), background[ BKG_BACKGROUND ].getHeight() * BKG_STARS_PERCENT / 100 );
		Pattern p = new Pattern( BKG_STAR_COLOR );
		p.setSize( MAX_X_DIFF[ BKG_BACKGROUND ], groupStar.getHeight() - imgSky.getHeight() );
		p.setPosition( MIN_X[ BKG_BACKGROUND ], 0 );
		groupStar.insertDrawable( p );
		
		p = new Pattern( imgSky );
		p.setSize( MAX_X_DIFF[ BKG_BACKGROUND ], imgSky.getHeight() );
		p.setPosition( MIN_X[ BKG_BACKGROUND ], groupStar.getHeight() - p.getHeight() );
		groupStar.insertDrawable( p );
		
		Thread.yield();
		
		int tempHeight = groupStar.getHeight() - p.getHeight();
		
		while ( groupStar.getUsedSlots() < MAX_BKG_ELEMENTS - 1 ) {
			final DrawableImage star = new DrawableImage( imgStar );
			star.setPosition( MIN_X[ BKG_BACKGROUND ] + NanoMath.randInt( MAX_X_DIFF[ BKG_BACKGROUND ] ), NanoMath.randInt( tempHeight ) );
			groupStar.insertDrawable( star );
		}
		final DrawableImage moon = new DrawableImage( imgMoon );
		//#if SCREEN_SIZE == "SMALL"
//# 		moon.setPosition( MIN_X[ BKG_BACKGROUND ] + ( MAX_X_DIFF[ BKG_BACKGROUND ] / 6 ), groupStar.getHeight() >> 1 );
		//#else
		moon.setPosition( MIN_X[ BKG_BACKGROUND ] + ( MAX_X_DIFF[ BKG_BACKGROUND ] >> 2 ), groupStar.getHeight() >> 1 );
		//#endif
		groupStar.insertDrawable( moon );
		background[ BKG_BACKGROUND ].insertDrawable( groupStar );
		
		// insere o grupo de estrelas do background do meio
		final DrawableGroup groupStarMiddle = new DrawableGroup( MAX_BKG_ELEMENTS );
		groupStarMiddle.setSize( background[ BKG_MIDDLE ].getWidth(), background[ BKG_MIDDLE ].getHeight() * BKG_STARS_PERCENT / 100 );
		while ( groupStar.getUsedSlots() < MAX_BKG_ELEMENTS ) {
			final DrawableImage star = new DrawableImage( imgStar );
			star.setPosition( MIN_X[ BKG_MIDDLE ] + NanoMath.randInt( MAX_X_DIFF[ BKG_MIDDLE ] ), NanoMath.randInt( groupStarMiddle.getHeight() ) );
			groupStarMiddle.insertDrawable( star );
		}		
		background[ BKG_MIDDLE ].insertDrawable( groupStarMiddle );
		
		Thread.yield();
		
		// insere o fundo de tela do sol
		final DrawableGroup groupSun = new DrawableGroup( MAX_BKG_ELEMENTS );
		groupSun.setSize( MAX_X_DIFF[ BKG_BACKGROUND ], background[ BKG_BACKGROUND ].getHeight() * BKG_SUN_PERCENT / 100 );
		groupSun.setPosition( MIN_X[ BKG_BACKGROUND ], groupStar.getHeight() );
		p = new Pattern( BKG_SUN_SKY_COLOR );
		p.setSize( groupSun.getSize() );
		groupSun.insertDrawable( p );

		tempHeight = groupSun.getHeight() - imgClouds[ 0 ].getHeight();
		while ( groupSun.getUsedSlots() < MAX_BKG_ELEMENTS - 1 ) {
			final DrawableImage cloud = new DrawableImage( imgClouds[ groupSun.getUsedSlots() & 1 ] );
			cloud.setPosition( MIN_X[ BKG_BACKGROUND ] + NanoMath.randInt( MAX_X_DIFF[ BKG_BACKGROUND ] ), NanoMath.randInt( tempHeight ) );
			groupSun.insertDrawable( cloud );
		}		
		
		final DrawableImage sun = new DrawableImage( imgSun );
		//#if SCREEN_SIZE == "SMALL"
//# 		sun.setPosition( ( MAX_X_DIFF[ BKG_BACKGROUND ] - imgSun.getWidth() ) / 6, groupSun.getHeight() >> 1 );
		//#else
		sun.setPosition( ( MAX_X_DIFF[ BKG_BACKGROUND ] - imgSun.getWidth() ) >> 2, groupSun.getHeight() >> 1 );
		//#endif
		groupSun.insertDrawable( sun );
		background[ BKG_BACKGROUND ].insertDrawable( groupSun );	
		
		Thread.yield();
		
		// insere as nuvens do background do meio
		final DrawableGroup groupSunMiddle = new DrawableGroup( MAX_BKG_ELEMENTS );
		groupSunMiddle.setSize( MAX_X_DIFF[ BKG_MIDDLE ], background[ BKG_MIDDLE ].getHeight() * BKG_SUN_PERCENT / 100 );
		groupSunMiddle.setPosition( MIN_X[ BKG_MIDDLE ], groupStarMiddle.getHeight() );
		final int CLOUD_MIDDLE_MAX_HEIGHT = groupSunMiddle.getHeight() - imgClouds[ 0 ].getHeight();
		while ( groupSunMiddle.getUsedSlots() < MAX_BKG_ELEMENTS ) {
			final DrawableImage cloud = new DrawableImage( imgClouds[ groupSunMiddle.getUsedSlots() & 1 ] );
			cloud.setPosition( MIN_X[ BKG_MIDDLE ] + NanoMath.randInt( MAX_X_DIFF[ BKG_MIDDLE ] ), NanoMath.randInt( CLOUD_MIDDLE_MAX_HEIGHT ) );
			groupSunMiddle.insertDrawable( cloud );
		}
		background[ BKG_MIDDLE ].insertDrawable( groupSunMiddle );
		
		// insere o fundo de tela do c�u azul
		final DrawableGroup groupSky = new DrawableGroup( 2 );
		groupSky.setSize( background[ BKG_BACKGROUND ].getWidth(), background[ BKG_BACKGROUND ].getHeight() * BKG_BLUE_SKY_PERCENT / 100 );
		groupSky.setPosition( 0, groupSun.getPosY() + groupSun.getHeight() );
		// esse � o �nico pattern que tem a largura completa da fase, pois dependendo da altura da tela ele pode ser visualizado
		// durante a movimenta��o inicial da c�mera
		p = new Pattern( BKG_SUN_SKY_COLOR );
		p.setSize( groupSky.getSize() );
		groupSky.insertDrawable( p );
		
		background[ BKG_BACKGROUND ].insertDrawable( groupSky );	
		
		Thread.yield();
		
		// insere a piscina
		poolRight = new DrawableImage( imgPoolRight );
		background[ BKG_FOREGROUND ].insertDrawable( poolRight );
		
		// insere a arquibancada
		stadium = new Pattern( imgStadium  );
		stadium.setSize( background[ BKG_FOREGROUND ].getWidth(), imgStadium.getHeight() );
		background[ BKG_FOREGROUND ].insertDrawable( stadium );
		
		// insere o grupo da torcida
		final byte CROWD_TOTAL = ( byte ) ( ( ( PLAYSCREEN_TOTAL_WIDTH * STADIUM_TOTAL_STEPS / spriteCrowd.getWidth() ) * 3 ) >> 2 );
		crowdGroup = new DrawableGroup( CROWD_TOTAL );
		crowdGroup.setSize( stadium.getSize() );
		for ( byte i = 0; i < CROWD_TOTAL; ++i ) {
			final Sprite s = new Sprite( spriteCrowd );
			crowdGroup.insertDrawable( s );
		}
		background[ BKG_FOREGROUND ].insertDrawable( crowdGroup );
		
		// insere a borda da piscina
		poolBorder = new Pattern( BKG_POOL_COLOR );
		background[ BKG_FOREGROUND ].insertDrawable( poolBorder );
		
		Thread.yield();
		
		pool = new Pattern( imgPool );
		background[ BKG_FOREGROUND ].insertDrawable( pool );
		
		// define a posi��o "zero" real
		GROUND_Y = ( short ) ( PLAYSCREEN_TOTAL_HEIGHT - ( poolRight.getHeight() >> 1 ) );
		
		// insere o emissor de part�culas de �gua
		splashEmitter = new SplashEmitter( background[ BKG_FOREGROUND ].getSize() );
		background[ BKG_FOREGROUND ].insertDrawable( splashEmitter );

		Thread.yield();
		
		// insere a plataforma
		platform = new DrawableImage( imgPlatform );
		platform.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		
		springBoardOptimal = new SpringBoard( true );
		background[ BKG_FOREGROUND ].insertDrawable( springBoardOptimal );
		
		springBoard = new SpringBoard( false );
		background[ BKG_FOREGROUND ].insertDrawable( springBoard );
		
		// insere a seta e o texto indicando para pular no momento correto
		jumpArrow = new DrawableImage( imgJumpArrow );
		//#if SCREEN_SIZE == "SMALL"
//# 		jumpArrow.defineReferencePixel( jumpArrow.getWidth() + 14, jumpArrow.getHeight() >> 1 );
		//#else
		jumpArrow.defineReferencePixel( jumpArrow.getWidth() + 18, jumpArrow.getHeight() >> 1 );
		//#endif
		background[ BKG_FOREGROUND ].insertDrawable( jumpArrow );
		
		jumpLabel = new RichLabel( GameMIDlet.getFont( FONT_INDEX_TEXT ), GameMIDlet.getText( TEXT_JUMP ), 0 );
		jumpLabel.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		background[ BKG_FOREGROUND ].insertDrawable( jumpLabel );		
		
		// insere o Bo�a depois do trampolim
		boca = Boca.loadBoca();
		background[ BKG_FOREGROUND ].insertDrawable( boca );	
		
		// insere o pattern da �gua que simula o corpo do Bo�a embaixo d'�gua
		underWaterPattern = new Pattern( new DrawableImage( PATH_IMAGES + "underwater.png" ) );
		background[ BKG_FOREGROUND ].insertDrawable( underWaterPattern );	
		
		// a plataforma � inserida depois do trampolim
		background[ BKG_FOREGROUND ].insertDrawable( platform );
		
		platformTop = new DrawableImage( imgPlatformTop );
		platformTop.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
		background[ BKG_FOREGROUND ].insertDrawable( platformTop );
		
		platformPattern = new Pattern( imgPlatformPattern );
		background[ BKG_FOREGROUND ].insertDrawable( platformPattern );

		
		Thread.yield();
		
		// insere as nuvens � frente do Bo�a
		final int FOREGROUND_CLOUD_MIN_Y = PLAYSCREEN_TOTAL_HEIGHT * BKG_STARS_PERCENT / 100;
		final int FOREGROUND_CLOUD_HEIGHT = ( PLAYSCREEN_TOTAL_HEIGHT * BKG_SUN_PERCENT / 100 ) - imgClouds[ 0 ].getHeight();
		for ( byte i = 0; i < MAX_BKG_ELEMENTS; ++i ) {
			final DrawableImage cloud = new DrawableImage( imgClouds[ i & 1 ] );
			cloud.setPosition( MIN_X[ BKG_FOREGROUND ] + NanoMath.randInt( MAX_X_DIFF[ BKG_FOREGROUND ] ), FOREGROUND_CLOUD_MIN_Y + NanoMath.randInt( FOREGROUND_CLOUD_HEIGHT ) );
			background[ BKG_FOREGROUND ].insertDrawable( cloud );
		}
		
		moveQueue = new MoveQueue( false, boca );
		insertDrawable( moveQueue );
		
		Thread.yield();
		
		//#if SCREEN_SIZE != "SMALL"
		// se o aparelho tiver touchscreen, adiciona os controles
		if ( ScreenManager.getInstance().hasPointerEvents() ) {
			final byte SPACING = 4;
			
			pointerControls = new DrawableGroup( 5 );
			
			final DrawableImage up = new DrawableImage( MoveQueue.IMG_STEPS[ STEP_UP ] );
			final DrawableImage down = new DrawableImage( MoveQueue.IMG_STEPS[ STEP_DOWN ] );
			final DrawableImage fire = new DrawableImage( MoveQueue.IMG_STEPS[ STEP_FIRE ] );
			final DrawableImage left = new DrawableImage( MoveQueue.IMG_STEPS[ STEP_LEFT_IMAGE_INDEX ] );
			final DrawableImage right = new DrawableImage( MoveQueue.IMG_STEPS[ STEP_RIGHT_IMAGE_INDEX ] );
			
			pointerControls.setSize( left.getWidth() + fire.getWidth() + right.getWidth() + ( SPACING << 1 ), 
									 up.getHeight() + fire.getHeight() + down.getHeight() + ( SPACING << 1 ) );
			
			up.setPosition( ( pointerControls.getWidth() - up.getWidth() ) >> 1, 0 );
			down.setPosition( ( pointerControls.getWidth() - down.getWidth() ) >> 1, pointerControls.getHeight() - down.getHeight() );
			fire.setPosition( left.getWidth() + SPACING, ( pointerControls.getHeight() - fire.getHeight() ) >> 1 );
			left.setPosition( 0, ( pointerControls.getHeight() - left.getHeight() ) >> 1 );
			right.setPosition( fire.getPosX() + fire.getWidth() + SPACING, ( pointerControls.getHeight() - right.getHeight() ) >> 1 );
			
			pointerControls.insertDrawable( up );
			pointerControls.insertDrawable( down );
			pointerControls.insertDrawable( fire );
			pointerControls.insertDrawable( left );
			pointerControls.insertDrawable( right );
			
			pointerControls.setPosition( size.x - pointerControls.getWidth(), moveQueue.movesPanel.getHeight() );
			
			insertDrawable( pointerControls );
		} else {
			pointerControls = null;
		}
		
		//#endif
		
		// insere o label indicando a pontua��o
		scoreLabel = new Label( GameMIDlet.getBigFont(), "0" );
		updateScoreLabel();
		insertDrawable( scoreLabel );
		
		// insere o label que mostra mensagens ao jogador
		labelMessage = new RichLabel( GameMIDlet.getFont( FONT_INDEX_BIG  ), "", size.x, null );
		insertDrawable( labelMessage );		
		
		// define as posi��es da c�mera
		CAMERA_MAX_Y = 0;
		CAMERA_MIN_Y = ( short ) ( ScreenManager.SCREEN_HEIGHT - PLAYSCREEN_TOTAL_HEIGHT );
		FP_CAMERA_MIN_Y = -NanoMath.toFixed( CAMERA_MIN_Y );
		CAMERA_MIN_X = ( short ) ( ( short ) ScreenManager.SCREEN_WIDTH - PLAYSCREEN_TOTAL_WIDTH );
		CAMERA_MAX_X = 0;
	}
	
	
	public final void update( int delta ) {
		// verifica se a tela est� ativa, para evitar atualiza��es do jogo durante a transi��o da tela
		if ( ScreenManager.getInstance().getCurrentScreen() != this )
			return;
		
		super.update( delta );
		
		//#if SCREEN_SIZE != "SMALL"
		if ( pointerMUV.getSpeed() != 0 ) {
			pointerControls.move( pointerMUV.updateInt( delta ), 0 );
			
			if ( pointerMUV.getSpeed() < 0 ) {
				final int limit = size.x - pointerControls.getWidth();
				if ( pointerControls.getPosX() <= limit ) {
					pointerControls.setPosition( limit, pointerControls.getPosY() );
					pointerMUV.setSpeed( 0 );
				}
			} else {
				if ( pointerControls.getPosX() >= size.x ) {
					pointerControls.setVisible( false );
					pointerControls.setPosition( size.x, pointerControls.getPosY() );
					pointerMUV.setSpeed( 0 );
				}
			}
		} // fim if ( pointerMUV.getSpeed() != 0 )
		//#endif
		
		// faz o texto da mensagem piscar
		if ( labelBlinkTime > 0 ) {
			labelBlinkTime -= delta;
			
			if ( labelBlinkTime <= 0 ) {
				labelBlinkTime = LABEL_BLINK_RATE;
				labelMessage.setVisible( !labelMessage.isVisible() );
			}
		}
		
		switch ( state ) {
			case STATE_BEGIN_LEVEL_X:
				updateCameraX( delta );
				
				if ( background[ BKG_FOREGROUND ].getPosX() <= CAMERA_MIN_X ) {
					stateEnded();
				}
			case STATE_GAME_OVER:
			case STATE_LEVEL_CLEARED:
			case STATE_BEGIN_LEVEL_Y:
			case STATE_CHOOSE_MOVES:
				// atualiza a pontua��o
				updateScore( delta, false );
			break;
			
			case STATE_SET_JUMP:
				// faz o trampolim "dummy" piscar
				springBoardAccTime -= delta;
				if ( springBoardAccTime <= 0 ) {
					springBoardAccTime += SPRINGBOARD_VISIBLE_TIME;
					springBoardOptimal.setVisible( !springBoardOptimal.isVisible() );
					jumpLabel.setVisible( springBoardOptimal.isVisible() );
					jumpArrow.setVisible( springBoardOptimal.isVisible() );
				}
				
			case STATE_ON_POOL:
				fp_jumpAccTime += NanoMath.divInt( delta, 1000 );
				final int FP_CURRENT_Y = NanoMath.mulFixed( fp_jumpInitialYSpeed, fp_jumpAccTime  ) +
										NanoMath.mulFixed( NanoMath.mulFixed( fp_jumpAccelerationY, NanoMath.mulFixed( fp_jumpAccTime, fp_jumpAccTime )  ), NanoMath.HALF );
				final int boca_y = jumpInitialY - toPixels( FP_CURRENT_Y );
				
				boca.setRefPixelPosition( boca.getRefPixelX(), boca_y );
				
				switch ( state ) {
					case STATE_SET_JUMP:
						springBoard.setPercent( NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( FP_CURRENT_Y, fp_jumpMaxHeight ), FP_100 ) ) );
						if ( fp_jumpAccTime >= FP_PREPARE_JUMP_TOTAL_TIME ) {
							fp_jumpAccTime = FP_PREPARE_JUMP_TOTAL_TIME;
							stateEnded();
						}
					break;
					
					case STATE_ON_POOL:
						final int LIMIT_Y = underWaterPattern.getPosY() - 5;
						if ( boca_y <= LIMIT_Y ) {
							boca.setRefPixelPosition( boca.getRefPixelX(), LIMIT_Y );
						}
					break;				
				}
			break;
			
			case STATE_JUMP_TO_SPRINGBOARD:
				updateCameraX( delta );
			case STATE_JUMPING:
			case STATE_HIT_POOL:
				fp_jumpAccTime += NanoMath.divInt( delta, 1000 );
				// s = s0 + v0t + at�/2
				final int y = jumpInitialY - toPixels( NanoMath.mulFixed( fp_jumpInitialYSpeed, fp_jumpAccTime  ) +
										NanoMath.mulFixed( NanoMath.mulFixed( FP_GRAVITY_ACC, NanoMath.mulFixed( fp_jumpAccTime, fp_jumpAccTime ) ), NanoMath.HALF ) );

				switch ( state ) {
					case STATE_JUMP_TO_SPRINGBOARD:
						boca.setRefPixelPosition( springBoard.getRefPixelX() + toPixels( NanoMath.mulFixed( fp_jumpXSpeed, fp_jumpAccTime ) ), y );
						if ( y >= jumpInitialY && fp_jumpAccTime >= FP_PREPARE_JUMP_TIME )
							setState( STATE_SET_JUMP );
					break;
					
					case STATE_JUMPING:
						boca.setRefPixelPosition( jumpInitialX + toPixels( NanoMath.mulFixed( fp_jumpXSpeed, fp_jumpAccTime ) ), NanoMath.min( GROUND_Y, y ) );
						if ( y >= GROUND_Y )
							setState( STATE_HIT_POOL );
					break;
					
					case STATE_HIT_POOL:
						boca.setRefPixelPosition( jumpInitialX + toPixels( NanoMath.mulFixed( fp_jumpXSpeed, fp_jumpAccTime ) ), y );
						if ( y > background[ BKG_FOREGROUND ].getHeight() + boca.getHeight() )
							setState( STATE_ON_POOL );
					break;
				}
			break;
		}
		
		// reposiciona a c�mera na tela
		switch ( cameraState ) {
			case CAMERA_STATE_MOVING:
				final int fp_delta = NanoMath.divInt( delta, 1000 );
				fp_cameraAccTime += fp_delta;
				
				if ( fp_cameraAccTime >= fp_cameraTotalTime ) {
					fp_cameraAccTime = fp_cameraTotalTime;
					setCameraState( CAMERA_STATE_STOPPED );
				}
				
				fp_cameraY += NanoMath.mulFixed( fp_cameraSpeed, fp_delta );
				
				setCameraY( NanoMath.toInt( fp_cameraY ) );
			break;
			
			case CAMERA_STATE_FOLLOW_PLAYER_1:
			case CAMERA_STATE_FOLLOW_PLAYER_2:
			case CAMERA_STATE_FOLLOW_PLAYER_3:
			case CAMERA_STATE_FOLLOW_PLAYER_4:
				final int fp_delta2 = NanoMath.divInt( delta, 1000 );
				fp_cameraAccTime += fp_delta2;
				
				if ( fp_cameraAccTime >= fp_cameraTotalTime ) {
					fp_cameraAccTime = fp_cameraTotalTime;
					
					if ( cameraState < CAMERA_STATE_FOLLOW_PLAYER_4 )
						setCameraState( cameraState + 1 );
				}
				
				final int currentOffset = cameraInitialPlayerOffset + NanoMath.toInt( NanoMath.mulFixed( 
												NanoMath.divFixed( fp_cameraAccTime, fp_cameraTotalTime ), 
												NanoMath.toFixed( cameraFinalPlayerOffset - cameraInitialPlayerOffset ) ) );
				
				setCameraAtBoca( currentOffset );
			break;
		}
		
		// caso o estado atual tenha uma dura��o m�xima definida, atualiza o contador
		if ( timeToNextState > 0 ) {
			timeToNextState -= delta;
			
			if ( timeToNextState <= 0 )
				stateEnded();
		}
	}	


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				switch ( state ) {
					case STATE_SHOW_RESULT_WAIT:
					case STATE_GAME_OVER:
					case STATE_GAME_OVER_RECORD:
						stateEnded();
					break;
					
					default:
						GameMIDlet.setScreen( SCREEN_PAUSE );
				}
			break;

			default:
				switch ( state ) {
					case STATE_BEGIN_LEVEL_X:
					case STATE_BEGIN_LEVEL_Y:
					case STATE_CHOOSE_MOVES:
					case STATE_PRESS_TO_JUMP:
					case STATE_SET_JUMP:
					case STATE_GAME_OVER:
					case STATE_GAME_OVER_RECORD:
					case STATE_LEVEL_CLEARED:
					case STATE_SHOW_RESULT_WAIT:
						stateEnded();
					break;

					case STATE_JUMPING:
						moveQueue.keyPressed( key );
					break;
				} // fim switch ( state )
			// fim default
		} // fim switch ( key )
	}


	public final void keyReleased( int key ) {
		switch ( state ) {
			case STATE_JUMPING:
				moveQueue.keyReleased( key );
			break;
		}
	}
	
	
	public final void hideNotify() {
		switch ( state ) {
			case STATE_SHOW_RESULT_WAIT:
			case STATE_SHOW_RESULT_2:
			case STATE_GAME_OVER:
			case STATE_GAME_OVER_RECORD:
			break;

			default:
				GameMIDlet.setScreen( SCREEN_PAUSE );
		}		
	}


	public final void showNotify() {
	}


	public final void sizeChanged( int width, int height ) {
	}	

	
	//#if SCREEN_SIZE != "SMALL"

	public final void onPointerDragged( int x, int y ) {
		onPointerPressed( x, y );
	}


	public final void onPointerPressed( int x, int y ) {
		switch ( state ) {
			case STATE_BEGIN_LEVEL_X:
			case STATE_BEGIN_LEVEL_Y:
			case STATE_CHOOSE_MOVES:
			case STATE_PRESS_TO_JUMP:
			case STATE_SET_JUMP:
			case STATE_GAME_OVER:
			case STATE_GAME_OVER_RECORD:
			case STATE_LEVEL_CLEARED:
			case STATE_SHOW_RESULT_WAIT:
				stateEnded();
			break;
			
			case STATE_JUMPING:
				// verifica se o controle pressionado � diferente do anterior. Caso seja igual, considera-se que o controle
				// ainda est� sendo mantido pressionado.
				final byte controlPressed = getPointerControlAt( x, y );
				if ( controlPressed != lastPointerControl ) {
					lastPointerControl = controlPressed;
					
					switch ( controlPressed ) {
						case STEP_UP:
							moveQueue.keyPressed( ScreenManager.UP );
						break;

						case STEP_FIRE:
							moveQueue.keyPressed( ScreenManager.FIRE );
						break;

						case STEP_DOWN:
							moveQueue.keyPressed( ScreenManager.DOWN );
						break;

						case STEP_LEFT_IMAGE_INDEX:
							moveQueue.keyPressed( ScreenManager.LEFT );
						break;

						case STEP_RIGHT_IMAGE_INDEX:
							moveQueue.keyPressed( ScreenManager.RIGHT );
						break;
					}
				}
			// fim default
		} // fim switch ( state )
	}
	
	
	public final void onPointerReleased( int x, int y ) {
		keyReleased( 0 );
		lastPointerControl = -1;
	}
	
	
	
	private final byte getPointerControlAt( int x, int y ) {
		if ( pointerControls.contains( x, y ) ) {
			x -= pointerControls.getPosX();
			y -= pointerControls.getPosY();

			final short TOTAL_SLOTS = pointerControls.getUsedSlots();
			for ( byte index = 0; index < TOTAL_SLOTS; ++index ) {
				if ( pointerControls.getDrawable( index ).contains( x, y ) )
					return index;
			}
		} // fim if ( pointerControls.contains( x, y ) )
		
		return -1;
	}
	
	//#endif
	
	
	public final void setState( int state ) {
		this.state = ( byte ) state;
		
		timeToNextState = 0;
		labelMessage.setVisible( false );
		labelBlinkTime = 0;
		
		switch ( state ) {
			case STATE_NONE:
				scoreLabel.setPosition( scoreLabel.getPosX(), -scoreLabel.getHeight() );
				score = 0;
				scoreShown = 0;
				updateScore( 0, true );
				// posiciona horizontalmente o cen�rio
				setCameraX( 0 );
			break;
			
			case STATE_BEGIN_LEVEL_X:
				prepareLevel();
				scoreLevel = 0;
				
				splashEmitter.reset();
				
				final StringBuffer buffer = new StringBuffer();
				if ( level == LEVEL_TRAINING ) {
					buffer.append( "<ALN_H>" + GameMIDlet.getText( TEXT_TRAINING ) );
				} else {
					buffer.append( GameMIDlet.getText( TEXT_LEVEL ) );
					buffer.append( String.valueOf( level ) );
				}
				buffer.append( '\n' );
				buffer.append( NanoMath.toString( fp_jumpInitialY ) );
				buffer.append( GameMIDlet.getText( TEXT_METER ) );
				showMessage( buffer.toString(), FONT_INDEX_BIG );
				
				boca.setViewport( null );
				boca.reset();
				
				// n�o mostra a pontua�a� se estiver no modo treino
				if ( level <= 1 )
					scoreLabel.setPosition( scoreLabel.getPosX(), -scoreLabel.getHeight() );
				
				scoreLabelMUV.setSpeed( scoreLabel.getHeight() );

				moveCameraTo( CAMERA_MIN_Y, 0 );
				
				springBoard.setVibrating( false );
				jumpArrow.setVisible( false );
				jumpLabel.setVisible( false );
				springBoardOptimal.setVisible( false );				
				
				// define a velocidade horizontal de movimenta��o da c�mera
				cameraXSpeed.setSpeed( CAMERA_MIN_X * 1000 / TIME_MESSAGE );
				if ( background[ BKG_FOREGROUND ].getPosX() == CAMERA_MIN_X )
					setCameraX( CAMERA_MAX_X );
				
				//#if SCREEN_SIZE != "SMALL"
				if ( pointerControls != null ) {
					pointerMUV.setSpeed( 0 );
					pointerControls.setPosition( size.x, pointerControls.getPosY() );
				}
				//#endif
				
				moveQueue.setState( MoveQueue.STATE_HIDDEN );
			break;
			
			case STATE_BEGIN_LEVEL_Y:
				timeToNextState = TIME_MOVE_UP;
				
				setCameraX( CAMERA_MIN_X );
				
				moveCameraTo( CAMERA_PLATFORM_Y, NanoMath.divInt( TIME_MOVE_UP, 1000 ) );
			break;
			
			case STATE_CHOOSE_MOVES:
				updateScore( 0, true );
				scoreLabelMUV.setSpeed( -scoreLabel.getHeight() );
				
				timeToNextState = TIME_MESSAGE >> 1;
				moveCameraTo( CAMERA_PLATFORM_Y, 0 );
			break;
			
			case STATE_PRESS_TO_JUMP:
				scoreLabel.setPosition( scoreLabel.getPosX(), -scoreLabel.getHeight() );
				
				showMessage( GameMIDlet.getText( TEXT_PRESS_TO_JUMP ), FONT_INDEX_BIG );
				labelBlinkTime = LABEL_BLINK_RATE;
				
				moveCameraTo( CAMERA_PLATFORM_Y, 0 );
			break;
			
			case STATE_PREPARING_JUMP:
				// define os movimentos do gerenciador de movimentos
				try {
					moveQueue.prepare();
				} catch ( Exception e ) {
					e.printStackTrace();
				}
				
				boca.setSequence( Boca.SEQUENCE_PREPARING_JUMP );
				
				timeToNextState = TIME_PREPARE_JUMP;
			break;
			
			case STATE_JUMP_TO_SPRINGBOARD:
				// calcula as vari�veis do pulo do Bo�a at� a ponta do trampolim
				fp_jumpAccTime = 0;
				fp_jumpInitialYSpeed = getInitialYSpeed( FP_PREPARE_JUMP_Y_MAX );
				final int FP_JUMP_TIME = getJumpTime( 0, fp_jumpInitialYSpeed );
				final int JUMP_TIME = NanoMath.toInt( NanoMath.mulFixed( FP_JUMP_TIME, NanoMath.toFixed( 1000 ) ) );
				final int JUMP_X_DISTANCE = jumpInitialX - boca.getRefPixelX();
				fp_jumpXSpeed = NanoMath.divFixed( NanoMath.divFixed( NanoMath.toFixed( JUMP_X_DISTANCE ), FP_METER_TO_PIXELS ), FP_JUMP_TIME );
				// define a velocidade horizontal da c�mera de forma a acompanhar o Bo�a pulando para o trampolim
				final int BOCA_RELATIVE_X = boca.getRefPixelX() + background[ BKG_FOREGROUND ].getPosX();
				cameraXSpeed.setSpeed( ( JUMP_X_DISTANCE + BOCA_RELATIVE_X - ScreenManager.SCREEN_HALF_WIDTH ) * -1000 / JUMP_TIME );
				
				boca.setSequence( Boca.SEQUENCE_JUMPING );
			break;
			
			case STATE_SET_JUMP:
				boca.setRefPixelPosition( jumpInitialX, jumpInitialY );
				cameraXSpeed.setSpeed( 0 );
			
				fp_jumpMaxHeight = -NanoMath.divFixed( NanoMath.toFixed( SPRINGBOARD_MAX_HEIGHT >> 2 ), FP_METER_TO_PIXELS );
				fp_jumpInitialYSpeed = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.ONE << 1, fp_jumpMaxHeight ), FP_PREPARE_JUMP_TIME >> 1 );
				// a = -2 * v0 / t
				fp_jumpAccelerationY = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), fp_jumpInitialYSpeed ), FP_PREPARE_JUMP_TIME );
				fp_jumpXSpeed = 0;
				fp_jumpAccTime = 0;
				
				jumpArrow.setVisible( true );
				jumpLabel.setVisible( true );
				springBoardOptimal.setVisible( true );
				springBoardAccTime = SPRINGBOARD_VISIBLE_TIME;
				
				boca.setSequence( Boca.SEQUENCE_PREPARING_JUMP );
			break;
			
			case STATE_JUMPING:
				springBoardOptimal.setVisible( false );
				
				// define a altura do pulo de acordo com a precis�o na plataforma
				fp_jumpPrecision = NanoMath.max( NanoMath.ONE - NanoMath.divFixed( NanoMath.abs( fp_jumpAccTime - FP_PREPARE_JUMP_TIME ), FP_PREPARE_JUMP_TIME ), 0 );
				//#if DEBUG == "true"
				System.out.println( "PRECIS�O DO PULO: " + NanoMath.toString( NanoMath.mulFixed( fp_jumpPrecision, NanoMath.toFixed( 100 ) ) ) + "%" );
				//#endif
				fp_jumpInitialYSpeed = getInitialYSpeed( FP_JUMP_HEIGHT_MIN + NanoMath.mulFixed( FP_JUMP_HEIGHT_MAX_DIFF, fp_jumpPrecision ) );
				final int FP_TOTAL_JUMP_TIME = getJumpTime( fp_jumpInitialY, fp_jumpInitialYSpeed );
				
				final int FP_DELTA = NanoMath.mulFixed( fp_jumpInitialYSpeed, fp_jumpInitialYSpeed ) - 
									 NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( 4 ), fp_jumpInitialY ), FP_GRAVITY_ACC );

				fp_jumpMaxHeight = NanoMath.divFixed( -FP_DELTA, NanoMath.mulFixed( NanoMath.toFixed( 4 ), FP_GRAVITY_ACC ) );
				
				springBoard.setVibrating( true );
				
				labelMessage.setVisible( false );
				setCameraState( CAMERA_STATE_FOLLOW_PLAYER_1 );
				
				fp_jumpXSpeed = NanoMath.divFixed( FP_JUMP_X_OFFSET, FP_TOTAL_JUMP_TIME );
				
				jumpArrow.setVisible( false );
				jumpLabel.setVisible( false );
				springBoardOptimal.setVisible( false );
				
				boca.setSequence( Boca.SEQUENCE_JUMPING );
				
				//#if SCREEN_SIZE != "SMALL"
				if ( pointerControls != null ) {
					pointerControls.setVisible( true );
					
					pointerMUV.setSpeed( -pointerControls.getWidth() * 1000 / MoveQueue.TIME_APPEAR );
				}
				//#endif
				
				moveQueue.setState( MoveQueue.STATE_APPEARING );
			break;
			
			case STATE_HIT_POOL:
				fp_splashPrecision = getBocaSplashPrecisionFP();
				int fp_rotationPercent = NanoMath.mulFixed( FP_SPLASH_PERCENT_ROTATION, fp_splashPrecision );
				
				splashEmitter.explode( boca.getRefPixelPosition(), boca.getFrameWidth(), 
									   NanoMath.mulFixed( NanoMath.divFixed( fp_jumpMaxHeight, FP_PLAYSCREEN_TOTAL_HEIGHT ), FP_SPLASH_PERCENT_JUMP_HEIGHT ) + fp_rotationPercent );
				moveQueue.setState( MoveQueue.STATE_HIDING );
				
				//#if SCREEN_SIZE != "SMALL"
				if ( pointerControls != null ) {
					pointerMUV.setSpeed( pointerControls.getWidth() * 1000 / MoveQueue.TIME_APPEAR );
				}
				//#endif
				
				setCameraState( CAMERA_STATE_STOPPED );
				setCameraY( CAMERA_MIN_Y );
			break;
			
			case STATE_ON_POOL:
				boca.reset();
				
				keyReleased( 0 );
				//#if SCREEN_SIZE != "SMALL"
					onPointerReleased( 0, 0 );
				//#endif
				
				// calcula a velocidade inicial e a acelera��o do Bo�a, de forma que ele mergulhe o tempo e a profundidade esperados
				jumpInitialY = boca.getRefPixelY();
				fp_jumpMaxHeight = -NanoMath.divFixed( NanoMath.toFixed( underWaterPattern.getPosY() - ( boca.getHeight() >> 1 ) - jumpInitialY ), FP_METER_TO_PIXELS );
				fp_jumpInitialYSpeed = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.ONE << 1, fp_jumpMaxHeight ), FP_TIME_DIVING >> 1 );
				// a = -2 * v0 / t
				fp_jumpAccelerationY = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), fp_jumpInitialYSpeed ), FP_TIME_DIVING );
				fp_jumpXSpeed = 0;
				fp_jumpAccTime = 0;

				timeToNextState = TIME_ON_POOL_WAIT;
			break;
			
			case STATE_SHOW_RESULT_WAIT:
				GameMIDlet.setScreen( SCREEN_RESULTS );
			break;
			
			case STATE_SHOW_RESULT_2:
				timeToNextState = 700;
			break;
			
			case STATE_GAME_OVER:
				scoreLabelMUV.setSpeed( scoreLabel.getHeight() );
				
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ), FONT_INDEX_BIG );
				
				MediaPlayer.play( SOUND_INDEX_GAME_OVER );
			break;
			
			case STATE_GAME_OVER_RECORD:
				updateScore( 0, true );
				showMessage( GameMIDlet.getText( TEXT_NEW_RECORD ) + NanoMath.toString( score ), FONT_INDEX_BIG );
				labelBlinkTime = LABEL_BLINK_RATE;
			break;
			
			case STATE_LEVEL_CLEARED:
				scoreLabelMUV.setSpeed( scoreLabel.getHeight() );
				
				showMessage( GameMIDlet.getText( TEXT_LEVEL_COMPLETED ), FONT_INDEX_BIG );
				timeToNextState = TIME_MESSAGE;
				
				MediaPlayer.play( SOUND_INDEX_LEVEL_CLEARED );
			break;
		}
	} // fim do m�todo setState( int )	
	
	
	private final void stateEnded() {
		switch ( state ) {
			case STATE_BEGIN_LEVEL_X:
				setState( STATE_BEGIN_LEVEL_Y );
			break;
			
			case STATE_BEGIN_LEVEL_Y:
				setState( STATE_CHOOSE_MOVES );
			break;
			
			case STATE_CHOOSE_MOVES:
				GameMIDlet.setScreen( SCREEN_JUMP_MENU );
			break;
			
			case STATE_PRESS_TO_JUMP:
				setState( STATE_PREPARING_JUMP );
			break;
			
			case STATE_PREPARING_JUMP:
				setState( STATE_JUMP_TO_SPRINGBOARD );
			break;
			
			case STATE_SET_JUMP:
				setState( STATE_JUMPING );
			break;
			
			case STATE_ON_POOL:
				setState( STATE_SHOW_RESULT_WAIT );
			break;
			
			case STATE_SHOW_RESULT_WAIT:
				setState( STATE_SHOW_RESULT_2 );
			break;
			
			case STATE_SHOW_RESULT_2:
				if ( level == LEVEL_TRAINING ) {
					// mostra menu para escolher pr�xima a��o: repetir pulo, escolher nova altura, voltar ao menu
					GameMIDlet.setScreen( SCREEN_REPEAT_JUMP_MENU );
				} else {
					if ( scoreLevel >= getScoreRequired( true ) ) {
						setState( STATE_LEVEL_CLEARED );
					} else {
						setState( STATE_GAME_OVER );
					}
				}
			break;
			
			case STATE_LEVEL_CLEARED:
				setLevel( level + 1 );
				saveGame();
				
				setState( PlayScreen.STATE_BEGIN_LEVEL_X );
			break;
			
			case STATE_GAME_OVER:
				if ( HighScoresScreen.setScore( score ) ) {
					setState( STATE_GAME_OVER_RECORD );
				} else {
					GameMIDlet.setScreen( SCREEN_MAIN_MENU );
				}
			break;
			
			case STATE_GAME_OVER_RECORD:
				score = 0;
				GameMIDlet.setScreen( SCREEN_HIGH_SCORES );
			break;
		}
	} // fim do m�todo stateEnded()
	
	
	private final void prepareLevel() {
		final int PLATFORM_HEIGHT = NanoMath.toInt( NanoMath.mulFixed( fp_jumpInitialY, FP_METER_TO_PIXELS ) );		
		
		// atualiza a altura da maior plataforma alcan�ada pelo jogador
		if ( fp_jumpInitialY > fp_highestPlatform )
			fp_highestPlatform = fp_jumpInitialY;
		
		jumpInitialY = GROUND_Y - PLATFORM_HEIGHT - ( boca.getHeight() >> 1 );
		platform.setRefPixelPosition( PLAYSCREEN_TOTAL_WIDTH + PLATFORM_RIGHT_X_OFFSET, GROUND_Y - PLATFORM_HEIGHT );
		platformTop.setRefPixelPosition( platform.getPosX() + platform.getWidth(), platform.getPosY() + platform.getHeight() );
		platformPattern.setPosition( platformTop.getPosX(), platformTop.getPosY() + platformTop.getHeight() );
		platformPattern.setSize( imgPlatformPattern.getWidth(), background[ BKG_FOREGROUND ].getHeight() - platformPattern.getPosY() );
		springBoard.setRefPixelPosition( platform.getPosX() + SPRINGBOARD_X_OFFSET, platform.getPosY() + SPRINGBOARD_Y_OFFSET );
		springBoardOptimal.setPosition( springBoard.getPosition() );
		
		jumpArrow.setRefPixelPosition( springBoard.getPosX(), springBoard.getRefPixelY() );
		jumpLabel.setRefPixelPosition( jumpArrow.getPosX(), jumpArrow.getRefPixelY() );
		
		// define a posi��o da piscina e do Bo�a a partir da posi��o da plataforma
		poolRight.setPosition( platform.getPosX() - poolRight.getWidth(), background[ BKG_FOREGROUND ].getHeight() - poolRight.getHeight() );
		pool.setSize( poolRight.getPosX(), imgPool.getHeight() );
		pool.setPosition( 0, poolRight.getPosY() );
		underWaterPattern.setSize( pool.getWidth(), underWaterPattern.getFill().getHeight() );
		underWaterPattern.setPosition( pool.getPosX(), pool.getPosY() + pool.getHeight() - underWaterPattern.getHeight() );
		poolBorder.setPosition( poolRight.getPosX() + poolRight.getWidth(), poolRight.getPosY() - 4 );
		poolBorder.setSize( PLAYSCREEN_TOTAL_WIDTH - poolBorder.getPosX(), background[ BKG_FOREGROUND ].getHeight() - poolBorder.getPosY() );
		
		stadium.setPosition( 0, poolRight.getPosY() - stadium.getHeight() );		
		crowdGroup.setPosition( stadium.getPosition() );
		
		jumpInitialX = springBoard.getPosX();
		boca.setRefPixelPosition( springBoard.getRefPixelX(), jumpInitialY );
		
		CAMERA_PLATFORM_Y = ( short ) -( jumpInitialY - ScreenManager.SCREEN_HALF_HEIGHT );
		
		// atualiza o label da pontua��o
		if ( level <= 1 ) {
			score = 0;
			scoreShown = 0;
		}
		updateScoreLabel();
	}
	
	
	/**
	 * Prepara a torcida para um n�vel.
	 */
	public final void prepareCrowd( int crowdLevel ) {
		if ( crowdLevel > LEVEL_MAX )
			crowdLevel = LEVEL_MAX;
		
		// preenche a torcida
		if ( crowdLevel == LEVEL_TRAINING ) {
			crowdGroup.setVisible( false );
		} else {
			crowdGroup.setVisible( true );
			
			// preenche a arquibancada
			final byte CROWD_TOTAL = ( byte ) ( NanoMath.max( STADIUM_TOTAL_STEPS, crowdLevel * crowdGroup.getTotalSlots() / LEVEL_MAX ) );
			final byte PEOPLE_PER_LINE = ( byte ) ( CROWD_TOTAL / STADIUM_TOTAL_STEPS );
			final short SLOT_WIDTH = ( short ) ( PLAYSCREEN_TOTAL_WIDTH / PEOPLE_PER_LINE );
			
			for ( int i = 0, y = 0, step = 0, lastDiff = 0; step < STADIUM_TOTAL_STEPS; ++step, y += STADIUM_STEP_HEIGHT ) {
				for ( int x = 0, c = 0; c < PEOPLE_PER_LINE; ++c, ++i ) {
					final Sprite s = ( Sprite ) crowdGroup.getDrawable( i );

					s.setVisible( true );
					s.setFrame( NanoMath.randInt( s.getFrameTimes()[ 0 ].length ) );
					
					final int temp = NanoMath.randInt( 1 + lastDiff + SLOT_WIDTH - s.getCurrentFrameImage().getWidth() );
					s.setPosition( x + temp, y );
					lastDiff = temp;
					x += temp + s.getCurrentFrameImage().getWidth();
				}
			}
			
			// marca como invis�veis os sprites de torcedores n�o presentes
			for ( byte i = CROWD_TOTAL; i < crowdGroup.getUsedSlots(); ++i ) {
				crowdGroup.getDrawable( i ).setVisible( false );
			}
		}		
	}
	
	
	/**
	 * Prepara as vari�veis internas para iniciar um novo campeonato.
	 */
	public static final void reset() {
		score = 0;
		setLevel( 1 );
	}
	
	
	/**
	 * Calcula a pontua��o da fase, atualizando a pontua��o total, e retorna uma string com a pontua��o detalhada.
	 * @return
	 */
	public final String calculateScore() {
		// pontos da altura do pulo
		final int FP_HEIGHT_SCORE = NanoMath.mulFixed( fp_jumpPrecision, FP_JUMP_HEIGHT_MAX_SCORE );

		// pontos dos movimentos
		final int[] FP_MOVES_SCORE = new int[ 2 ];
		moveQueue.fillScore( FP_MOVES_SCORE );

		final int FP_LEVEL_SPLASH_SCORE = NanoMath.mulFixed( fp_splashPrecision, FP_SPLASH_SCORE ) + moveQueue.getBarrigadaScore();

		scoreLevel = FP_HEIGHT_SCORE + FP_MOVES_SCORE[ 0 ] + FP_LEVEL_SPLASH_SCORE;
		changeScore( scoreLevel );
		
		if ( scoreLevel < getScoreRequired( true ) )
			eraseSavedGame();

		final StringBuffer msg = new StringBuffer();
		msg.append( GameMIDlet.getText( TEXT_RESULT ) );

		// na vers�o de tela pequena, h� quebras extras de linha pois os resultados n�o cabem numa mesma linha
		
		msg.append( GameMIDlet.getText( TEXT_HEIGHT ) );
		//#if SCREEN_SIZE == "SMALL"
//# 		msg.append( '\n' );
		//#endif
		msg.append( NanoMath.toString( FP_HEIGHT_SCORE ) );
		msg.append( GameMIDlet.getText( TEXT_SEPARATOR ) );
		msg.append( NanoMath.toString( FP_JUMP_HEIGHT_MAX_SCORE ) );
		msg.append( '\n' );

		msg.append( GameMIDlet.getText( TEXT_MOVES ) );
		//#if SCREEN_SIZE == "SMALL"
//# 		msg.append( '\n' );
		//#endif
		msg.append( NanoMath.toString( FP_MOVES_SCORE[ 0 ] ) );
		msg.append( GameMIDlet.getText( TEXT_SEPARATOR ) );
		msg.append( NanoMath.toString( FP_MOVES_SCORE[ 1 ] ) );
		msg.append( '\n' );

		msg.append( GameMIDlet.getText( TEXT_SPLASH ) );
		//#if SCREEN_SIZE == "SMALL"
//# 		msg.append( '\n' );
		//#endif
		msg.append( NanoMath.toString( FP_LEVEL_SPLASH_SCORE  ) );
		msg.append( GameMIDlet.getText( TEXT_SEPARATOR ) );
		msg.append( NanoMath.toString( FP_SPLASH_TOTAL_SCORE ) );
		msg.append( '\n' );
		msg.append( '\n' );

		msg.append( GameMIDlet.getText( TEXT_RESULT_TOTAL ) );
		//#if SCREEN_SIZE == "SMALL"
//# 		msg.append( '\n' );
		//#endif
		msg.append( NanoMath.toString( scoreLevel ) );
		msg.append( '\n' );

		msg.append( GameMIDlet.getText( TEXT_SCORE_MIN ) );
		//#if SCREEN_SIZE == "SMALL"
//# 		msg.append( '\n' );
		//#endif
		msg.append( NanoMath.toString( getScoreRequired( true ) ) );		
		msg.append( '\n' );
		msg.append( '\n' );
		
		return msg.toString();
	}
	
	
	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreLabelMUV.getSpeed() != 0 ) {
			scoreLabel.move( 0, scoreLabelMUV.updateInt( delta ) );
			
			if ( scoreLabel.getPosY() > 0 ) {
				scoreLabel.setPosition( scoreLabel.getPosX(), 0 );
				scoreLabelMUV.setSpeed( 0 );
			} else if ( scoreLabel.getPosY() < -scoreLabel.getHeight() ) {
				scoreLabel.setPosition( scoreLabel.getPosX(), -scoreLabel.getHeight() );
				scoreLabelMUV.setSpeed( 0 );
			}
		}
		
		if ( scoreShown != score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			updateScoreLabel();
		}
	} // fim do m�todo updateScore( boolean )		

	
	private final void updateScoreLabel() {
		scoreLabel.setText( GameMIDlet.getText( TEXT_SCORE ) + NanoMath.toString( scoreShown ) );
	}	
	
	
	/**
	 * Incrementa a pontua��o e atualiza as vari�veis e labels correspondentes.
	 * @param diff varia��o na pontua��o (positiva ou negativa).
	 */
	private final void changeScore( int diff ) {
		score += diff;
		
		if ( score >= SCORE_SHOWN_MAX )
			score = SCORE_SHOWN_MAX;
		
		scoreSpeed.setSpeed( ( score - scoreShown ) >> 1, false );
	}	
	
	
	private final void showMessage( String message, byte fontIndex ) {
		labelMessage.setFont( GameMIDlet.getFont( fontIndex ) );
		labelMessage.setText( message );
		labelMessage.setSize( labelMessage.getWidth(), labelMessage.getTextTotalHeight() );
		labelMessage.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		labelMessage.setRefPixelPosition( size.x >> 1, size.y >> 1 );		
		labelMessage.setVisible( true );
	}	
	
	
	public final int getScore() {
		return score;
	}	
	
	
	/**
	 * Move a c�mera para a posi��o "position" real em "time" segundos. Caso "time" seja menor ou igual a zero, 
	 * a c�mera move-se instantaneamente para a posi��o recebida.
	 * 
	 * @param y posi��o y em pixels do topo da visualiza��o da c�mera.
	 * @param fp_time tempo em segundos que a c�mera levar� at� chegar ao destino.
	 */
	private final void moveCameraTo( int y, int fp_time ) {
		if ( y < CAMERA_MIN_Y ) {
			y = CAMERA_MIN_Y;
		} else if ( y > CAMERA_MAX_Y ) {
			y = CAMERA_MAX_Y;
		}

		if ( fp_time > 0 ) {
			y = NanoMath.toFixed( y );
			
			fp_cameraTotalTime = fp_time;
			fp_cameraAccTime = 0;
			setCameraState( CAMERA_STATE_MOVING );
			fp_cameraY = NanoMath.toFixed( background[ BKG_FOREGROUND ].getPosY() );
			fp_cameraSpeed = NanoMath.divFixed( y - fp_cameraY, fp_time );
		} else {
			setCameraY( y );
			setCameraState( CAMERA_STATE_STOPPED );
		}
	} // fim do m�todo moveCameraTo()	
	
	
	/**
	 * Posiciona efetivamente a c�mera na posi��o <code>y</code>, ou seja, posiciona os grupos de drawables da tela de jogo,
	 *  levando em conta o paralaxe.
	 * 
	 * @param y posi��o y do topo da c�mera.
	 */
	private final void setCameraY( int y ) {
		if ( y < CAMERA_MIN_Y ) {
			y = CAMERA_MIN_Y;
		} else if ( y > CAMERA_MAX_Y ) {
			y = CAMERA_MAX_Y;
		}
		
		background[ BKG_FOREGROUND ].setPosition( background[ BKG_FOREGROUND ].getPosX(), y );
		final int FP_PERCENT = NanoMath.divFixed( NanoMath.toFixed( y ), FP_CAMERA_MIN_Y );
		for ( byte i = 0; i < BKG_FOREGROUND; ++i )
			background[ i ].setPosition( background[ i ].getPosX(), 
										 NanoMath.toInt( NanoMath.mulFixed( FP_PERCENT, NanoMath.toFixed( background[ i ].getHeight() - ScreenManager.SCREEN_HEIGHT ) ) ) );
	}
	
	
	/**
	 * Posiciona efetivamente a c�mera na posi��o <code>x</code>, ou seja, posiciona os grupos de drawables da tela de jogo,
	 *  levando em conta o paralaxe de cada camada.
	 * 
	 * @param x posi��o x da esquerda da c�mera.
	 */
	private final void setCameraX( int x ) {
		if ( x > CAMERA_MAX_X ) {
			x = CAMERA_MAX_X;
		} else if ( x < CAMERA_MIN_X ) {
			x = CAMERA_MIN_X;
		}
		
		for ( byte i = 0; i < background.length; ++i )
			background[ i ].setPosition( convertPos( x, i ), background[ i ].getPosY() );
	}
	
	
	private final void updateCameraX( int delta ) {
		background[ BKG_FOREGROUND ].move( cameraXSpeed.updateInt( delta ), 0 );
		setCameraX( background[ BKG_FOREGROUND ].getPosX() );
	}
	
	
	private final void setCameraState( int state ) {
		switch ( state ) {
			case CAMERA_STATE_MOVING:
			break;

			case CAMERA_STATE_STOPPED:
				fp_cameraTotalTime = 0;
				fp_cameraSpeed = 0;
				cameraState = CAMERA_STATE_STOPPED;
			break;

			case CAMERA_STATE_FOLLOW_PLAYER_1:
				cameraInitialPlayerOffset = ( short ) NanoMath.max( getBocaRelativeY(), CAMERA_PLAYER_MIN_BORDER_DISTANCE );
				cameraFinalPlayerOffset = CAMERA_PLAYER_MIN_BORDER_DISTANCE;
				fp_cameraAccTime = 0;
				fp_cameraTotalTime = NanoMath.mulFixed( NanoMath.divFixed( -fp_jumpInitialYSpeed, FP_GRAVITY_ACC ), FP_FOLLOW_PLAYER_JUMP_PERCENT );
			break;

			case CAMERA_STATE_FOLLOW_PLAYER_2:
				cameraInitialPlayerOffset = ( short ) NanoMath.max( getBocaRelativeY(), CAMERA_PLAYER_MIN_BORDER_DISTANCE );
				cameraFinalPlayerOffset = ( short ) ( ( ScreenManager.SCREEN_HEIGHT - boca.getHeight() ) >> 1 );
				fp_cameraAccTime = 0;
			break;

			case CAMERA_STATE_FOLLOW_PLAYER_3:
				cameraInitialPlayerOffset = ( short ) NanoMath.max( getBocaRelativeY(), CAMERA_PLAYER_MIN_BORDER_DISTANCE );
				cameraFinalPlayerOffset = ( short ) ( ScreenManager.SCREEN_HEIGHT - CAMERA_PLAYER_MIN_BORDER_DISTANCE - boca.getHeight() );
				fp_cameraAccTime = 0;
			break;

			case CAMERA_STATE_FOLLOW_PLAYER_4:
				cameraInitialPlayerOffset = ( short ) NanoMath.min( getBocaRelativeY(), ScreenManager.SCREEN_HEIGHT - CAMERA_PLAYER_MIN_BORDER_DISTANCE - boca.getHeight() );
				cameraFinalPlayerOffset = ( short ) ( ( ScreenManager.SCREEN_HEIGHT - boca.getHeight() ) >> 1 );
				fp_cameraAccTime = 0;
			break;
		}
		
		cameraState = ( byte ) state;
	}
	
	
	/**
	 * Obt�m a posi��o y (topo) do Bo�a relativa ao topo da tela.
	 */
	private final int getBocaRelativeY() {
		return background[ BKG_FOREGROUND ].getPosY() + boca.getPosY();
	}
	
	
	private final void setCameraAtBoca( int offset ) {
		setCameraX( -boca.getRefPixelX() + ScreenManager.SCREEN_HALF_WIDTH );
		setCameraY( -boca.getPosY() + offset );
	}
	
	
	private final int convertPos( int pos, int bkgLevel ) {
		return pos >> ( BKG_FOREGROUND - bkgLevel );
	}
	
	
	/**
	 * Calcula a velocidade y inicial para que se atinja uma determinada altura m�xima, considerando a acelera��o da gravidade.
	 */
	private final int getInitialYSpeed( int fp_maxHeight ) {
		return NanoMath.sqrtFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), NanoMath.mulFixed( FP_GRAVITY_ACC, fp_maxHeight ) ) );		
	}
	
	
	/**
	 * Calcula o tempo total de pulo at� atingir a altura zero.
	 * @param fp_initialY posi��o y inicial do pulo.
	 * @param fp_initialYSpeed velocidade vertical do pulo.
	 * @return
	 */
	private final int getJumpTime( int fp_initialY, int fp_initialYSpeed ) {
		return NanoMath.divFixed( -fp_initialYSpeed - NanoMath.sqrtFixed( NanoMath.mulFixed( fp_initialYSpeed, 
										fp_initialYSpeed ) - NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( 4 ), 
											fp_initialY ), FP_GRAVITY_ACC ) ), FP_GRAVITY_ACC );		
	}
	
	
	private final int toPixels( int fp_value ) {
		return NanoMath.toInt( NanoMath.mulFixed( fp_value, FP_METER_TO_PIXELS ) );
	}
	
	
	public static final void setLevel( int level ) {
//		level = 20; // usado para testes
		PlayScreen.level = ( short ) level;
		
		if ( level > LEVEL_TRAINING ) {
			// calcula a altura da plataforma do n�vel atual
			final int difficultyLevel = NanoMath.min( level - 1, LEVEL_MAX );
			final int FP_LEVEL_PERCENT = NanoMath.divInt( difficultyLevel * difficultyLevel, LEVEL_MAX * LEVEL_MAX );
			fp_jumpInitialY = NanoMath.toFixed( NanoMath.round( FP_PLATFORM_MIN_HEIGHT + NanoMath.mulFixed( FP_PLATFORM_MAX_HEIGHT - FP_PLATFORM_MIN_HEIGHT, FP_LEVEL_PERCENT ) ) );
			
			//#if DEBUG == "true"
			System.out.println ( "N�VEL " + level + ". %: " + NanoMath.toString( FP_LEVEL_PERCENT ) + ", y: "+ NanoMath.toString( fp_jumpInitialY ) );
			//#endif
		} else {
			score = 0;
		}
	}
	
	
	public static final short getLevel() {
		return level;
	}
	
	
	/**
	 * Define a altura da plataforma para o modo de treino.
	 * @param fp_platformHeight altura da plataforma, em metros na nota��o de ponto fixo.
	 */
	public static final void setPlatformHeight( int fp_platformHeight ) {
		setLevel( LEVEL_TRAINING );
		fp_jumpInitialY = fp_platformHeight;
	}
	
	
	/**
	 * @return a quantidade m�xima de movimentos permitidos, de acordo com a altura atual da plataforma.
	 */
	public static final int getMaxMoves() {
		final int FP_PLATFORM_PERCENT = NanoMath.divFixed( fp_jumpInitialY - FP_PLATFORM_MIN_HEIGHT, FP_PLATFORM_MAX_HEIGHT );
		return MOVES_MIN + NanoMath.toInt( NanoMath.mulFixed( FP_PLATFORM_PERCENT, NanoMath.toFixed( MOVES_MAX - MOVES_MIN ) ) );
	}
	
	
	/**
	 * @param completeScore indica se deve-se levar em conta o m�ximo de pontos que se pode atingir, ou somente a pontua��o
	 * dos movimentos.
	 * @return pontua��o necess�ria para passar do n�vel atual.
	 */
	public static final int getScoreRequired( boolean completeScore ) {
		final int FP_PLATFORM_PERCENT = NanoMath.divFixed( fp_jumpInitialY, FP_PLATFORM_MAX_HEIGHT );
		return FP_LEVEL_SCORE_MIN + NanoMath.mulFixed( FP_PLATFORM_PERCENT, FP_LEVEL_SCORE_MAX - FP_LEVEL_SCORE_MIN - ( completeScore ? 0 : FP_NON_MOVE_MAX_SCORE ) );
	}
	
	
	/**
	 * @return a altura da maior plataforma alcan�ada pelo jogador.
	 */
	public static final int getHighestPlatformFP() {
		return fp_highestPlatform;
	}
	
	
	/**
	 * Calcula a precis�o da barrigada.
	 * @return
	 */
	private final int getBocaSplashPrecisionFP() {
		int FP_FRAME_PERCENT = NanoMath.HALF;
		switch ( boca.getSequence() ) {
			case Boca.SEQUENCE_BARRIGADA:
				FP_FRAME_PERCENT += ( NanoMath.HALF + ( moveQueue.getCurrentMove().getStepsMadePercentFP() >> 1 ) ) >> 1;
			break;
		}
		
		switch ( boca.getPlayerRotation() ) {
			case ROTATION_180:
				// de barriga
				return FP_FRAME_PERCENT;
				
			case ROTATION_0:
				// de costas
				return NanoMath.mulFixed( NanoMath.divInt( 84, 100 ), FP_FRAME_PERCENT );

			case ROTATION_90:
			case ROTATION_270:
			default:
				// em p� ou de cabe�a
				return NanoMath.mulFixed( NanoMath.divInt( 64, 100 ), FP_FRAME_PERCENT );
		}	
	} // fim do m�todo getBocaSplashPrecisionFP()
	
	
	public static final void loadImages() throws Exception {
		imgStar = new DrawableImage( PATH_IMAGES + "star.png" );		
		imgStadium = new DrawableImage( PATH_IMAGES + "stadium.png" );
		Thread.yield();
		imgMoon = new DrawableImage( PATH_IMAGES + "moon.png" );
		Thread.yield();
		imgSun = new DrawableImage( PATH_IMAGES + "sun.png" );		
		Thread.yield();
		imgPlatform = new DrawableImage( PATH_IMAGES + "platform.png" );		
		Thread.yield();
		imgPlatformPattern = new DrawableImage( PATH_IMAGES + "platform_pattern.png" );		
		Thread.yield();
		imgPlatformTop = new DrawableImage( PATH_IMAGES + "platform_top.png" );		
		Thread.yield();
		imgJumpArrow = new DrawableImage( PATH_IMAGES + "jump_arrow.png" );		
		imgPool = new DrawableImage( PATH_IMAGES + "pool.png" );		
		Thread.yield();
		imgPoolRight = new DrawableImage( PATH_IMAGES + "pool_right.png" );		
		Thread.yield();
		imgSky = new DrawableImage( PATH_IMAGES + "sky.png" );		
		Thread.yield();
		spriteCrowd = new Sprite( PATH_IMAGES + ( GameMIDlet.isLowMemory() ? "crowd_low.bin" : "crowd.bin" ), PATH_IMAGES + "crowd" );
		Thread.yield();

		for ( byte i = 0; i < imgClouds.length; ++i )
			imgClouds[ i ] = new DrawableImage( PATH_IMAGES + "cloud_" + i + ".png" );		
		Thread.yield();
	}
	
	
	private static final void saveGame() {
		final SavedGame save = new SavedGame();
		save.RMSMode = save.RMS_MODE_SAVE_GAME;

		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_SAVED_GAME, save );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			System.out.println( "Erro ao gravar campeonato." );
			e.printStackTrace();
			//#endif
		}
	}


	public static final void eraseSavedGame() {
		final SavedGame save = new SavedGame();
		save.RMSMode = save.RMS_MODE_ERASE_SAVED_GAME;

		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_SAVED_GAME, save );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			System.out.println( "Erro ao apagar campeonato salvo." );
			e.printStackTrace();
			//#endif
		}
	}


	public static final void loadSavedGame() throws Exception {
		final SavedGame save = new SavedGame();
		save.RMSMode = save.RMS_MODE_LOAD_SAVED_GAME;			

		GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_SAVED_GAME, save );
	}	
	
	
	private static final class SavedGame implements Serializable {
		
		private final byte RMS_MODE_SAVE_GAME			= 0;
		private final byte RMS_MODE_LOAD_SAVED_GAME		= 1;
		private final byte RMS_MODE_ERASE_SAVED_GAME	= 2;

		/** Modo de grava��o ou leitura atual no RMS. */
		private byte RMSMode;		

		
		private SavedGame() {
		}
		
		
		public final void write( DataOutputStream output ) throws Exception {
			switch ( RMSMode ) {
				case RMS_MODE_SAVE_GAME:
					// maior plataforma alcan�ada
					output.writeInt( fp_highestPlatform );
					
					// n�vel do jogo
					output.writeShort( level );
					
					// pontua��o
					output.writeInt( score );
				break;
				
				case RMS_MODE_ERASE_SAVED_GAME:
					// maior plataforma alcan�ada
					output.writeInt( fp_highestPlatform );
					
					// grava um n�vel dummy para indicar que n�o h� jogo salvo
					output.writeShort( -1 );
				break;

				default:
					return;
			} // fim switch ( RMSMode )
		}


		public final void read( DataInputStream input ) throws Exception {
			switch ( RMSMode ) {
				case RMS_MODE_LOAD_SAVED_GAME:
					// maior plataforma alcan�ada
					fp_highestPlatform = NanoMath.max( fp_highestPlatform, input.readInt() );
					
					// n�vel do jogo
					setLevel( input.readShort() );
					
					// pontua��o
					score = input.readInt();					
					scoreShown = score;
				break;
			}
		}
	} // fim da classe interna SavedGame


}
