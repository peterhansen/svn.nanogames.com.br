/**
 * PlatformHeightScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 22, 2008 7:44:44 PM.
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;


/**
 * 
 * @author Peter
 */
public final class PlatformHeightScreen extends DrawableGroup implements Constants, KeyListener, Updatable 
	//#if SCREEN_SIZE != "SMALL"
	, PointerListener
	//#endif
{

	private final Pattern selectorFill;
	
	private final DrawableImage selector;
	
	private final Label labelHeight;
	
	private final int FP_MOVE_SPEED;
	
	private final MUV selectorMUV = new MUV();
	
	private final int FP_PLATFORM_MAX;
	
	private int fp_currentHeight;
	
	private static final short MIN_INTERVAL = GameMIDlet.GAME_MAX_FRAME_TIME + 1;
	
	private int timeSinceLastKeyPressed = -1;
	
	//#if SCREEN_SIZE != "SMALL"
	private boolean dragging;
	//#endif
	
	
	public PlatformHeightScreen() throws Exception {
		super( 10 );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		FP_PLATFORM_MAX = PlayScreen.getHighestPlatformFP();
		FP_MOVE_SPEED = NanoMath.divFixed( FP_PLATFORM_MAX - FP_PLATFORM_MIN_HEIGHT, NanoMath.toFixed( 5 ) );
		
		final RichLabel title = new RichLabel( GameMIDlet.getFont( FONT_INDEX_BIG ), GameMIDlet.getText( TEXT_CHOOSE_PLATFORM_HEIGHT ), size.x, null );
		insertDrawable( title );
		
		selectorFill = new Pattern( new DrawableImage( PATH_IMAGES + "selector_f2.png" ) );
		selectorFill.setSize( size.x * 7 / 10, selectorFill.getFill().getHeight() );
		selectorFill.setPosition( ( size.x - selectorFill.getWidth() ) >> 1, size.y >> 1 );
		insertDrawable( selectorFill );
		
		final DrawableImage borderLeft = new DrawableImage( PATH_IMAGES + "selector_b.png" );
		borderLeft.setPosition( selectorFill.getPosX() - borderLeft.getWidth(), selectorFill.getPosY() );
		insertDrawable( borderLeft );

		final DrawableImage borderRight = new DrawableImage( borderLeft );
		borderRight.setTransform( TRANS_MIRROR_H );
		borderRight.setPosition( selectorFill.getPosX() + selectorFill.getWidth(), selectorFill.getPosY() );
		insertDrawable( borderRight );
		
		selector = new DrawableImage( PATH_IMAGES + "selector.png" );
		selector.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		selector.setRefPixelPosition( selectorFill.getPosX(), selectorFill.getPosY() + ( selectorFill.getHeight() >> 1 ) );
		insertDrawable( selector );
		
		title.setPosition( 0, ( selector.getPosY() - title.getHeight() ) >> 1 );
		
		labelHeight = new Label( GameMIDlet.getFont( FONT_INDEX_BIG  ), "" );
		insertDrawable( labelHeight );
		
		setCurrentHeight( FP_PLATFORM_MAX >> 1 );
	}
	

	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				selectorMUV.setSpeed( -FP_MOVE_SPEED );
				timeSinceLastKeyPressed = 0;
			break;
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				selectorMUV.setSpeed( FP_MOVE_SPEED );
				timeSinceLastKeyPressed = 0;
			break;
			
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				PlayScreen.setPlatformHeight( fp_currentHeight );
				GameMIDlet.setScreen( SCREEN_NEW_GAME );
			break;
			
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				GameMIDlet.setScreen( SCREEN_NEW_GAME_MENU );
			break;
		}
	}


	public final void keyReleased( int key ) {
		if ( timeSinceLastKeyPressed >= 0 && timeSinceLastKeyPressed < MIN_INTERVAL && selectorMUV.getSpeed() != 0 ) {
			final int currentHeight = NanoMath.round( fp_currentHeight );
			
			setCurrentHeight( NanoMath.toFixed( currentHeight + NanoMath.sgn( selectorMUV.getSpeed() ) ) );
			
			timeSinceLastKeyPressed = -1;
		}
		
		selectorMUV.setSpeed( 0 );
	}

	
	//#if SCREEN_SIZE != "SMALL"

	public final void onPointerDragged( int x, int y ) {
		if ( dragging )
			onPointerPressed( NanoMath.clamp( x, selectorFill.getPosX(), selectorFill.getPosX() + selectorFill.getWidth() ), selector.getPosY() );
	}


	public final void onPointerPressed( int x, int y ) {
		if ( ( y >= selector.getPosY() && y <= selector.getPosY() + selector.getHeight() ) && ( x >= selectorFill.getPosX() && x <= selectorFill.getPosX() + selectorFill.getWidth() ) ) {
			dragging = true;
			x -= selectorFill.getPosX();
			
			final int FP_PERCENT = NanoMath.divInt( x, selectorFill.getWidth() );
			setCurrentHeight( FP_PLATFORM_MIN_HEIGHT + NanoMath.mulFixed( FP_PERCENT, FP_PLATFORM_MAX - FP_PLATFORM_MIN_HEIGHT ) );
		}
	}


	public final void onPointerReleased( int x, int y ) {
		dragging = false;
		keyReleased( 0 );
	}
	
	//#endif


	public final void update( int delta ) {
		if ( timeSinceLastKeyPressed >= 0 )
			timeSinceLastKeyPressed += delta;
		
		if ( selectorMUV.getSpeed() != 0 ) {
			setCurrentHeight( fp_currentHeight + selectorMUV.updateInt( delta ) );
		}
	}
	
	
	private final void setCurrentHeight( int fp_height ) {
		fp_currentHeight = NanoMath.clamp( fp_height, FP_PLATFORM_MIN_HEIGHT, FP_PLATFORM_MAX );

		if ( fp_currentHeight <= FP_PLATFORM_MIN_HEIGHT || fp_currentHeight >= FP_PLATFORM_MAX )
			selectorMUV.setSpeed( 0 );

		selector.setRefPixelPosition( selectorFill.getPosX() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_currentHeight - FP_PLATFORM_MIN_HEIGHT, FP_PLATFORM_MAX - FP_PLATFORM_MIN_HEIGHT ), 
											NanoMath.toFixed( selectorFill.getWidth() ) ) ), selector.getRefPixelY() );
		
		labelHeight.setText( NanoMath.toString( fp_currentHeight ) + GameMIDlet.getText( TEXT_METER ) );
		labelHeight.setPosition( ( size.x - labelHeight.getWidth() ) >> 1, selector.getPosY() + selector.getHeight() );
	}
	
}
