/**
 * MovesScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 17, 2008 7:27:48 PM.
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import core.Boca;
import core.Constants;
import core.Move;
import core.MoveQueue;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class MovesScreen extends UpdatableGroup implements KeyListener, Constants
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif
{

	private final MoveQueue moveQueue;
	
	private final Boca boca;
	
	private static final short KEY_REPEAT_FIRST		= 700;
	private static final short KEY_REPEAT_INTERVAL	= MoveQueue.MOVE_CHANGE_TIME;
	
	private boolean keyRepeated;
	
	private short keyAccTime;
	
	private int keyHeld;
	
	private final MarqueeLabel labelInfo;
	
	
	//<editor-fold desc="BARRA DE PONTUA��O" defaultstate="collapsed">
	
	/** Barra de pontua��o m�xima. */
	private final Pattern scoreBar;
	
	/** Barra da pontua��o do movimento atual. */
	private final Pattern moveBar;
	
	/** Barra da pontua��o do pulo atual. */
	private final Pattern jumpBar;
	
	//</editor-fold>
			
	
	public MovesScreen() throws Exception {
		super( 10 );
		
		final int TOTAL_MOVES = PlayScreen.getMaxMoves();
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		//#if SCREEN_SIZE == "SMALL"
//# 		final MarqueeLabel title = new MarqueeLabel( GameMIDlet.getFont( FONT_INDEX_TEXT ), GameMIDlet.getText( TEXT_CHOOSE ) + TOTAL_MOVES + GameMIDlet.getText( TEXT_MOVE ) );
//# 		title.setSize( size.x, title.getHeight() );
//# 		title.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
//# 		title.setSpeed( size.x >> 2 );
//# 		title.setWaitTime( 1000 );
		//#else
		final Label title = new Label( GameMIDlet.getFont( FONT_INDEX_TEXT ), GameMIDlet.getText( TEXT_CHOOSE ) + TOTAL_MOVES + GameMIDlet.getText( TEXT_MOVE ) );
		//#endif
		
		title.setPosition( ( size.x - title.getWidth() ) >> 1, title.getHeight() >> 2 );
		insertDrawable( title );
		
		boca = Boca.loadBoca();

		moveQueue = new MoveQueue( true, boca );
		moveQueue.setPosition( 0, title.getPosY() + title.getHeight() );
		moveQueue.setSize( moveQueue.getWidth(), size.y - moveQueue.getPosY() );

		final Move[] moves = new Move[ TOTAL_MOVES + 2 ];
		for ( byte i = 0; i < TOTAL_MOVES; ++i ) {
			moves[ i ] = new Move( Move.TYPE_EMPTY );
			Thread.yield();
		}
		
		moves[ TOTAL_MOVES ] = new Move( Move.TYPE_BARRIGADA );
		moves[ TOTAL_MOVES + 1] = new Move( Move.TYPE_OK );
		MoveQueue.setMoves( moves );
		moveQueue.prepare();
		Thread.yield();
		insertDrawable( moveQueue );
		
		// insere o texto informativo
		final int INITIAL_X = moveQueue.stepsPanel.getPosX() + moveQueue.stepsPanel.getWidth();
		labelInfo = new MarqueeLabel( GameMIDlet.getFont( FONT_INDEX_TEXT ), 
										PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING ? GameMIDlet.getText( TEXT_CHOOSE_MOVES_TRAINING ) : null );
		labelInfo.setPosition( INITIAL_X, moveQueue.getPosY() + moveQueue.movesPanel.getPosY() + moveQueue.movesPanel.getHeight() );
		labelInfo.setSize( size.x - INITIAL_X, labelInfo.getHeight() );
		insertDrawable( labelInfo );
		
		// insere a barra indicadora de pontua��o
		final int SCORE_BAR_WIDTH = ( size.x - moveQueue.stepsPanel.getWidth() ) * 8 / 10;
		scoreBar = new Pattern( new DrawableImage( PATH_IMAGES + "selector_f.png" ) );
		
		//#if SCREEN_SIZE == "SMALL"
//# 		// "espreme" os itens na tela por falta de espa�o
//# 		scoreBar.setPosition( INITIAL_X + ( ( size.x - INITIAL_X - SCORE_BAR_WIDTH ) >> 1 ), labelInfo.getPosY() + labelInfo.getHeight() );
		//#else
		scoreBar.setPosition( INITIAL_X + ( ( size.x - INITIAL_X - SCORE_BAR_WIDTH ) >> 1 ), labelInfo.getPosY() + ( ( labelInfo.getHeight() * 3 ) >> 1 ) );
		//#endif
		
		scoreBar.setSize( SCORE_BAR_WIDTH, scoreBar.getFill().getHeight() );
		scoreBar.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		insertDrawable( scoreBar );
		
		final DrawableImage scoreBorderLeft = new DrawableImage( PATH_IMAGES + "selector_b.png" );
		scoreBorderLeft.setPosition( scoreBar.getPosX() - 2, scoreBar.getPosY() );
		insertDrawable( scoreBorderLeft );
		
		Thread.yield();
		
		final DrawableImage scoreBorderRight = new DrawableImage( scoreBorderLeft );
		scoreBorderRight.setTransform( TRANS_MIRROR_H );
		scoreBorderRight.setPosition( scoreBar.getPosX() + scoreBar.getWidth() - 2, scoreBar.getPosY() );
		insertDrawable( scoreBorderRight );
		
		jumpBar = new Pattern( new DrawableImage( PATH_IMAGES + "aim_pattern.png" ) );
		jumpBar.setPosition( scoreBar.getPosX(), scoreBar.getPosY() + 2 );
		jumpBar.setSize( 0, scoreBar.getHeight() - 4 );
		insertDrawable( jumpBar );
		
		Thread.yield();
		
		moveBar = new Pattern( 0xff0000 );
		moveBar.setPosition( jumpBar.getPosition() );
		moveBar.setSize( jumpBar.getSize() );
		insertDrawable( moveBar );
		
		DrawableImage meter = null;
		if ( PlayScreen.getLevel() != PlayScreen.LEVEL_TRAINING ) {
			meter = new DrawableImage( PATH_IMAGES + "selector.png" );
			meter.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			insertDrawable( meter );
		}
		
		moveQueue.setBars( scoreBar, jumpBar, moveBar, meter, labelInfo );
		
		// posiciona e insere o Bo�a
		final int INITIAL_Y = scoreBar.getPosY() + scoreBar.getHeight();
		boca.setRefPixelPosition( size.x - ( ( size.x - INITIAL_X ) >> 1 ), size.y - ( ( size.y - INITIAL_Y ) >> 1 ) );
		insertDrawable( boca );
	}
	

	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				moveQueue.changeCurrentMoveType( -1 );
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				moveQueue.changeCurrentMoveType( 1 );
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				moveQueue.changeMoveIndex( -1 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				moveQueue.changeMoveIndex( 1 );
			break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				if ( moveQueue.isOnLastMoveIndex() ) {
					keyReleased( 0 );
					GameMIDlet.backToPlayScreen();
					return;
				} else {
					keyPressed( ScreenManager.RIGHT );
				}
			break;

			case ScreenManager.KEY_SOFT_LEFT:
				keyReleased( 0 );
				GameMIDlet.setScreen( SCREEN_HELP_MOVES_SCREEN );
			return;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				keyReleased( 0 );
				GameMIDlet.setScreen( SCREEN_JUMP_MENU );
			return;
		}

		keyHeld = key;
		keyAccTime = 0;
	} // fim do m�todo keyPressed( int )


	public final void update( int delta ) {
		super.update( delta );
		
		if ( keyHeld != 0 ) {
			keyAccTime += delta;
			
			if ( keyAccTime >= ( keyRepeated ? KEY_REPEAT_INTERVAL : KEY_REPEAT_FIRST ) ) {
				keyRepeated = true;
				keyAccTime %= KEY_REPEAT_INTERVAL;
				keyPressed( keyHeld );
			}
		}
	}
	
	
	public final void keyReleased( int key ) {
		keyHeld = 0;
		keyAccTime = 0;
		keyRepeated = false;
	}

	
	//#if SCREEN_SIZE != "SMALL"

	public final void onPointerDragged( int x, int y ) {
		onPointerPressed( x, y );
	}


	public final void onPointerPressed( int x, int y ) {
		x -= moveQueue.getPosX();
		y -= moveQueue.getPosY();
		
		final int MOVE_X = x - moveQueue.movesPanel.getPosX();
		final int MOVE_Y = y - moveQueue.movesPanel.getPosY();
		
		if ( moveQueue.arrowLeft.contains( MOVE_X, MOVE_Y ) ) {
			keyPressed( ScreenManager.LEFT );
		} else if ( moveQueue.arrowRight.contains( MOVE_X, MOVE_Y ) ) {
			keyPressed( ScreenManager.RIGHT );
		} else if ( moveQueue.arrowUp.contains( MOVE_X, MOVE_Y ) ) {
			keyPressed( ScreenManager.UP );
		} else if ( moveQueue.movesPanelAim.contains( MOVE_X, MOVE_Y ) ) {
			keyPressed( ScreenManager.FIRE );
		} else {
			final int STEP_X = x - moveQueue.stepsPanel.getPosX();
			final int STEP_Y = y - moveQueue.stepsPanel.getPosY();
			if ( moveQueue.arrowDown.contains( STEP_X, STEP_Y ) ) {
				keyPressed( ScreenManager.DOWN );
			}
		}
	}


	public final void onPointerReleased( int x, int y ) {
		keyReleased( 0 );
	}
	
	//#endif

}
