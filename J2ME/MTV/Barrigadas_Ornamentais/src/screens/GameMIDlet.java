/**
 * GameMIDlet.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Boca;
import core.Constants;
import core.Move;
import core.MoveQueue;
import core.ScrollBar;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener {
	
	public static final byte GAME_MAX_FRAME_TIME = 110;
	
	private static final byte BACKGROUND_TYPE_SPECIFIC		= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;
	
	private static DrawableImage cursorImage;
	
 	private static ImageFont[] FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	
	//#if JAR != "min"
	/** gerenciador da anima��o da soft key esquerda */
	private static BasicAnimatedSoftkey softkeyLeft;	
	
	/** gerenciador da anima��o da soft key direita */
	private static BasicAnimatedSoftkey softkeyRight;	
	/** Gerenciador da anima��o de transi��o de tela. */
	private static WaterTransition transition;
	
		private static BasicAnimatedPattern bkg;
	//#endif
	
	//#if JAR == "full"
	/** Limite m�nimo de mem�ria para que comece a haver cortes nos recursos. */
	private static final int LOW_MEMORY_LIMIT = 900000;
	//#endif
	
	/** Refer�ncia para a tela de jogo, para que seja poss�vel retornar � tela de jogo ap�s entrar na tela de pausa. */
	private static PlayScreen playScreen;
	
	private static MovesScreen movesScreen;
	
	
	
	private static final byte BKG_SPEED = -20;
	
	private static boolean lowMemory;
	
	private static LoadListener loader;
	

	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
		super( -1, GAME_MAX_FRAME_TIME );
		//#endif
	}

	
	protected final void loadResources() throws Exception {		
		//#if JAR == "full"
		switch ( getVendor() ) {
			case VENDOR_SONYERICSSON:
			case VENDOR_NOKIA:
			case VENDOR_SIEMENS:
			break;
			
			case VENDOR_LG:
				lowMemory = true;
			break;

			default:
				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}

		bkg = new BasicAnimatedPattern( new DrawableImage( PATH_IMAGES + "pattern_menu.png" ), BKG_SPEED, 0 );
		bkg.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		//#else
//# 			lowMemory = true;
		//#endif
//		lowMemory = true; // teste
		
		FONTS[ FONT_INDEX_TEXT ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_text.png", PATH_IMAGES + "font_text.dat" );
		FONTS[ FONT_INDEX_TEXT ].setCharOffset( FONT_TEXT_OFFSET );		
		
		
		//#if SCREEN_SIZE == "SMALL"
//# 		FONTS[ FONT_INDEX_BIG ] = FONTS[ FONT_INDEX_TEXT ];
		//#else

			//#if JAR == "full"
			if ( isLowMemory() ) {
				FONTS[ FONT_INDEX_BIG ] = FONTS[ FONT_INDEX_TEXT ];
			} else {
				FONTS[ FONT_INDEX_BIG ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );
				FONTS[ FONT_INDEX_BIG ].setCharOffset( FONT_BIG_OFFSET );	
			}
			//#else
//# 				FONTS[ FONT_INDEX_DEFAULT ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_text.png", PATH_IMAGES + "font_text.dat" );
//# 				FONTS[ FONT_INDEX_DEFAULT ].setCharOffset( DEFAULT_FONT_OFFSET );
			//#endif
		//#endif
		
		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}
		
		cursorImage = new DrawableImage( PATH_IMAGES + "cursor.png" );
		
		final String[] sounds = new String[ SOUND_TOTAL ];
		for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
			sounds[ i ] = PATH_SOUNDS + i + ".mid";
		}
		MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, sounds );
		
		//#ifdef NO_VIBRATION
//# 		MediaPlayer.setVibration( false );
		//#endif
		
		loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts.dat" );
		
		setScreen( SCREEN_LOADING_1 );
	} // fim do m�todo loadResources()
	

	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		playScreen = null;
		
		super.destroyApp( unconditional );
	}
	
	
	protected final int changeScreen( int screen ) throws Exception {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_SPECIFIC;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;	
		
		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				final BasicConfirmScreen confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_BIG  ), getCursor(), TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, false );
				confirm.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				confirm.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				
				if ( !MediaPlayer.isMuted() )
					confirm.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
				
				nextScreen = confirm;
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_SPLASH_NANO:
				HighScoresScreen.createInstance( getFont( FONT_INDEX_BIG  ), DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
				
				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "MEDIUM"
				nextScreen = new BasicSplashNano( SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH );				
				//#else
//# 				nextScreen = new BasicSplashNano( SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH );				
				//#endif
			break;
			
			case SCREEN_SPLASH_BRAND:
				bkgType = BACKGROUND_TYPE_NONE;
				nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_SPLASH + "mtv.png", TEXT_SPLASH_BRAND  );
			break;
			
			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame( getBigFont() );
				bkgType = BACKGROUND_TYPE_NONE;
			break;
			
			case SCREEN_MAIN_MENU:
				movesScreen = null;
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_NEW_GAME,
					TEXT_OPTIONS,
					TEXT_HIGH_SCORES,
					
					//#if DEMO == "true"
//# 					TEXT_BUY_FULL_GAME_TITLE,
					//#endif

					TEXT_HELP,
					TEXT_CREDITS,

					TEXT_EXIT,

					//#if DEBUG == "true"
					TEXT_LOG_TITLE,
					//#endif							
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_EXIT;				
			break;
			
			case SCREEN_NEW_GAME_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_TRAINING,
					TEXT_CHAMPIONSHIP,
					TEXT_BACK,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;						
			break;
			
			case SCREEN_SAVED_GAME_FOUND:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_BIG  ), getCursor(), TEXT_CONTINUE_SAVED_GAME, TEXT_YES, TEXT_NO, TEXT_BACK, false  );
				( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );

				indexSoftLeft = TEXT_OK;		
				( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( BasicConfirmScreen.INDEX_YES );
			break;
			
			case SCREEN_REPEAT_JUMP_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_REPEAT_JUMP,
					TEXT_CHANGE_PLATFORM,
					TEXT_BACK_MENU,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;						
			break;
			
			case SCREEN_CHOOSE_PLATFORM_HEIGHT:
				nextScreen = new PlatformHeightScreen();
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;								
			break;
			
			case SCREEN_JUMP_MENU:
				nextScreen = new BasicConfirmScreen( midlet, screen, getBigFont(), getCursor(), TEXT_MOVEMENT_SEQUENCE_TYPE, TEXT_AUTOMATIC, TEXT_MANUAL, TEXT_BACK, false );
				( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );

				indexSoftLeft = TEXT_OK;
				( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( BasicConfirmScreen.INDEX_YES );
			break;
			
			case SCREEN_NEW_GAME:
				playScreen.setState( PlayScreen.STATE_NONE );
			case SCREEN_PREPARE_LEVEL:
				playScreen.prepareCrowd( PlayScreen.getLevel() );
				playScreen.setState( PlayScreen.STATE_BEGIN_LEVEL_X );
			case SCREEN_CONTINUE_GAME:
				nextScreen = playScreen;
				bkgType = BACKGROUND_TYPE_NONE;
				
				indexSoftRight = TEXT_PAUSE;
			break;
			
			case SCREEN_RESULTS:
				final ScrollRichLabel label = new ScrollRichLabel( new RichLabel( FONTS[ FONT_INDEX_TEXT ], playScreen.calculateScore(), ScreenManager.SCREEN_WIDTH ) ) {
				
					public final void keyPressed( int key ) {
						switch ( key ) {
							case ScreenManager.FIRE:
							case ScreenManager.KEY_BACK:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_SOFT_MID:
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.KEY_NUM5:
							case ScreenManager.KEY_SOFT_LEFT:
								if ( PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING ) {
									setScreen( SCREEN_REPEAT_JUMP_MENU );
								} else {
									playScreen.prepareCrowd( PlayScreen.getLevel() + 1 );
									playScreen.keyPressed( key );
									setScreen( SCREEN_CONTINUE_GAME );
								}
							break;
							
							default:
								super.keyPressed( key );
						} // fim switch ( key )
					} // fim do m�todo keyPressed( int )
				};
				label.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				label.setScrollFull( new ScrollBar( ScrollBar.TYPE_BACKGROUND ) );
				label.setScrollPage( new ScrollBar( ScrollBar.TYPE_FOREGROUND ) );				
				nextScreen = label;
				
				indexSoftLeft = TEXT_OK;
			break;
			
			case SCREEN_OPTIONS:
				BasicOptionsScreen optionsScreen = null;
				
				//#ifdef NO_VIBRATION
//# 				if ( false ) {
				//#else
				if ( MediaPlayer.isVibrationSupported() ) {
				//#endif
					optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_BIG ), new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_BIG ), new int[] {
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK,
					}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
				}
				nextScreen = optionsScreen;

				optionsScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_HIGH_SCORES:
				nextScreen = HighScoresScreen.createInstance( getFont( FONT_INDEX_BIG ), DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
				
				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_HELP_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_OBJECTIVES,
					TEXT_CONTROLS,
					TEXT_TIPS,
					TEXT_BACK,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
			case SCREEN_HELP_MOVEMENTS:
				Drawable[] drawables = null;
				if ( screen == SCREEN_HELP_MOVEMENTS ) {
					drawables = new Drawable[ Move.TYPE_TOTAL - Move.TYPE_BARRIGADA ];
					for ( byte i = 0; i < drawables.length; ++i ) {
						drawables[ i ] = new Move( Move.TYPE_BARRIGADA + i );
						Thread.yield();
					}
				}
				
                nextScreen = new BasicTextScreen( SCREEN_HELP_MENU, getFont( FONT_INDEX_TEXT ), 
                                                    getText( TEXT_HELP_OBJECTIVES + ( screen - SCREEN_HELP_OBJECTIVES ) ) + getVersion(), false, drawables );
				( ( BasicTextScreen ) nextScreen ).setScrollFull( new ScrollBar( ScrollBar.TYPE_BACKGROUND ) );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( new ScrollBar( ScrollBar.TYPE_FOREGROUND ) );

				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_HELP_MOVES_SCREEN:
				nextScreen = new BasicTextScreen( SCREEN_CHOOSE_YOUR_MOVES, getFont( FONT_INDEX_TEXT ), TEXT_CHOOSE_MOVES_HELP, false, null );
				( ( BasicTextScreen ) nextScreen ).setScrollFull( new ScrollBar( ScrollBar.TYPE_BACKGROUND ) );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( new ScrollBar( ScrollBar.TYPE_FOREGROUND ) );

				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_TEXT ), TEXT_CREDITS_TEXT, true );

				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_PAUSE:
				BasicOptionsScreen pauseScreen = null;
				
				//#ifdef NO_VIBRATION
//# 				if ( false ) {
				//#else
				if ( MediaPlayer.isVibrationSupported() ) {
				//#endif
					pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_BIG ), new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_TURN_VIBRATION_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
				} else {
					pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_INDEX_BIG ), new int[] {
						TEXT_CONTINUE,
						TEXT_TURN_SOUND_OFF,
						TEXT_BACK_MENU,
						TEXT_EXIT_GAME,
					}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );							
					
				}
				nextScreen = pauseScreen;

				pauseScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;				
			break;
			
			case SCREEN_CONFIRM_MENU_TRAINING:
			case SCREEN_CONFIRM_MENU_SAVE:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_BIG ), getCursor(), 
									screen == SCREEN_CONFIRM_MENU_TRAINING ? TEXT_CONFIRM_BACK_MENU : TEXT_CONFIRM_BACK_MENU_SAVE, TEXT_YES, TEXT_NO, false );
				( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT_TRAINING:
			case SCREEN_CONFIRM_EXIT_SAVE:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_INDEX_BIG ), getCursor(), 
									screen == SCREEN_CONFIRM_EXIT_TRAINING ? TEXT_CONFIRM_EXIT : TEXT_CONFIRM_EXIT_SAVE, TEXT_YES, TEXT_NO, false );
				( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				indexSoftLeft = TEXT_OK;
			break;
			
			case SCREEN_CHOOSE_YOUR_MOVES:
				if ( movesScreen == null )
					movesScreen = new MovesScreen(); 
				nextScreen = movesScreen;
				indexSoftLeft = TEXT_HELP;
				indexSoftRight = TEXT_BACK;
			break;
			
			//#if DEMO == "true"
//# 			
//# 			// vers�o demo
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, midlet.cursor, TEXT_BUY_FULL_GAME_TEXT, TEXT_YES, TEXT_NO );
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
//# 			case SCREEN_PLAYS_REMAINING:
//# 				final String text = playsRemaining > 1 ? 
//# 						getText( TEXT_2_OR_MORE_PLAYS_REMAINING_1 ) + playsRemaining + getText( TEXT_2_OR_MORE_PLAYS_REMAINING_2 ) :
//# 						getText( TEXT_1_PLAY_REMAINING );
//# 				nextScreen = new BasicTextScreen( midlet, index, FONT_DEFAULT, text, true );
//# 						
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
			//#endif
			
			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen( 
					new LoadListener() {
						public final void load( final LoadScreen loadScreen ) throws Exception {
							// aloca os sons
							final String[] soundList = new String[ SOUND_TOTAL ];
							for ( byte i = 0; i < SOUND_TOTAL; ++i )
								soundList[ i ] = PATH_SOUNDS + i + ".mid";
							
							MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );
							

							//#if JAR != "min"
								transition = new WaterTransition();
								
							softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
							softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );	

							final ScreenManager manager = ScreenManager.getInstance();
							manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
							manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );	
							//#endif
							
							loadScreen.setActive( false );

							setScreen( SCREEN_CHOOSE_SOUND );
						}
					}, getFont( FONT_INDEX_BIG ), TEXT_LOADING );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;
			
			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {
						public final void load( final LoadScreen loadScreen ) throws Exception {
							MoveQueue.loadImages();
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

							PlayScreen.loadImages();
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
							
							Boca.loadImages();
							Thread.yield();
							
							Move.loadImages();
							Thread.yield();
							
							ScrollBar.loadImages();
							Thread.yield();
							
							playScreen = new PlayScreen();
							
							// tenta carregar a altura m�xima da plataforma gravada, se houver
							try {
								PlayScreen.loadSavedGame();
							} catch ( Exception e ) {
							}
							
							loadScreen.setActive( false );
							
							setScreen( SCREEN_MAIN_MENU );
						}
					}, getFont( FONT_INDEX_BIG ), TEXT_LOADING );
				
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;
			
			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen( loader, getFont( FONT_INDEX_BIG ), TEXT_LOADING );
				loader = null;
				
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;
			
			//#if DEBUG == "true"
			case SCREEN_ERROR_LOG:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_TEXT ), texts[ TEXT_LOG_TEXT ], false );
				( ( BasicTextScreen ) nextScreen ).setScrollFull( new ScrollBar( ScrollBar.TYPE_BACKGROUND ) );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( new ScrollBar( ScrollBar.TYPE_FOREGROUND ) );				
				
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				indexSoftRight = TEXT_BACK;
			break;
			//#endif
		} // fim switch ( screen )
		
		
		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );	
		
		
		//#if JAR == "low" || JAR == "min"
//# 			ScreenManager.getInstance().setCurrentScreen( nextScreen );
//# 			setBackground( bkgType );
		//#else
		if ( transition == null ) {
			ScreenManager.getInstance().setCurrentScreen( nextScreen );
			setBackground( bkgType );
		} else {
			transition.setNextScreen( nextScreen, bkgType );
		}
		//#endif
		
		return screen;
	} // fim do m�todo changeScreen( int )


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
			break;
			
			case BACKGROUND_TYPE_SPECIFIC:
			//#if JAR != "min"
				midlet.manager.setBackgroundColor( -1 );
				midlet.manager.setBackground( bkg, true );
			break;
			//#endif
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( BACKGROUND_COLOR );
			break;
		}
	} // fim do m�todo setBackground( byte )
	
	
	private static final BasicMenu createBasicMenu( int index, int[] entries ) throws Exception {
		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, getFont( FONT_INDEX_BIG  ), entries );		
		menu.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER ); 
		
		return menu;
	}	
	
	
	public static final boolean isLowMemory() {
		return lowMemory;
	}
	

	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						setScreen( SCREEN_NEW_GAME_MENU );
					break;
					
					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
					break;
					
					case ENTRY_MAIN_MENU_HIGH_SCORES:
						setScreen( SCREEN_HIGH_SCORES );
					break;
					
					//#if DEMO == "true"
//# 					case ENTRY_MAIN_MENU_BUY_FULL_GAME:
//# 						setScreen( SCREEN_BUY_GAME_RETURN );
//# 					break;
					//#endif
					
					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					//#if DEBUG == "true"
					case ENTRY_MAIN_MENU_ERROR_LOG:
						setScreen( SCREEN_ERROR_LOG );
					break;
					//#endif
					
					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU
			
			case SCREEN_NEW_GAME_MENU:
				switch ( index ) {
					case ENTRY_NEW_GAME_MENU_TRAINING:
						setScreen( SCREEN_CHOOSE_PLATFORM_HEIGHT );
					break;
					
					case ENTRY_NEW_GAME_MENU_CHAMPIONSHIP:
						// verifica se h� jogo salvo
						try {
							PlayScreen.loadSavedGame();
							setScreen( SCREEN_SAVED_GAME_FOUND );
						} catch ( Exception e ) {
							PlayScreen.setLevel( 1 );
							setScreen( SCREEN_NEW_GAME );
						}
					break;
					
					case ENTRY_NEW_GAME_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;
				}
			break;

			case SCREEN_REPEAT_JUMP_MENU:
				switch ( index ) {
					case ENTRY_REPEAT_JUMP_MENU_REPEAT:
						PlayScreen.setLevel( PlayScreen.LEVEL_TRAINING );
						setScreen( SCREEN_PREPARE_LEVEL );
					break;
					
					case ENTRY_REPEAT_JUMP_MENU_CHOOSE_PLATFORM:
						setScreen( SCREEN_CHOOSE_PLATFORM_HEIGHT );
					break;
					
					case ENTRY_REPEAT_JUMP_BACK_TO_MAIN_MENU:
						setScreen( SCREEN_MAIN_MENU );
					break;
				}
			break;
			
			case SCREEN_JUMP_MENU:
				switch ( index ) {
					case ENTRY_JUMP_MENU_AUTOMATIC:
						// seleciona automaticamente os movimentos
						MoveQueue.setMoves( null );
						backToPlayScreen();
					break;
					
					case ENTRY_JUMP_MENU_MANUAL:
						setScreen( SCREEN_CHOOSE_YOUR_MOVES );
					break;					
					
					case ENTRY_JUMP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_SAVED_GAME_FOUND:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						setScreen( SCREEN_PREPARE_LEVEL );
					break;
					
					case BasicConfirmScreen.INDEX_NO:
						HighScoresScreen.setScore( playScreen.getScore() );
						PlayScreen.eraseSavedGame();
						PlayScreen.reset();
						setScreen( SCREEN_NEW_GAME );
					break;
					
					case BasicConfirmScreen.INDEX_CANCEL:
						setScreen( SCREEN_NEW_GAME_MENU );
					break;
				}
			break;
			
			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_TIPS:
						setScreen( SCREEN_HELP_MOVEMENTS  );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_PAUSE:
				//#ifdef NO_VIBRATION
//# 				if ( false ) {
				//#else
				if ( MediaPlayer.isVibrationSupported() ) {
				//#endif
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_CONTINUE_GAME );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:	
							setScreen( PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING ? SCREEN_CONFIRM_MENU_TRAINING : SCREEN_CONFIRM_MENU_SAVE );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING ? SCREEN_CONFIRM_EXIT_TRAINING : SCREEN_CONFIRM_EXIT_SAVE );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:	
							setScreen( SCREEN_CONTINUE_GAME );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:	
							setScreen( PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING ? SCREEN_CONFIRM_MENU_TRAINING : SCREEN_CONFIRM_MENU_SAVE );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:			
							setScreen( PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING ? SCREEN_CONFIRM_EXIT_TRAINING : SCREEN_CONFIRM_EXIT_SAVE );
						break;
					}					
				}
			break; // fim case SCREEN_PAUSE
			
			case SCREEN_OPTIONS:
				//#ifdef NO_VIBRATION
//# 				if ( false ) {
				//#else
				if ( MediaPlayer.isVibrationSupported() ) {
				//#endif
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_BACK:	
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
						break;		
					}
				}
			break; // fim case SCREEN_OPTIONS
			
			//#if DEMO == "true"
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 							e.printStackTrace();
							//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif
			
			case SCREEN_CONFIRM_MENU_TRAINING:
			case SCREEN_CONFIRM_MENU_SAVE:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						//#if DEMO == "false"
						MediaPlayer.saveOptions();
						setScreen( SCREEN_MAIN_MENU );
						//#else
//# 						gameOver( 0 );
						//#endif
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;
			
			case SCREEN_CONFIRM_EXIT_TRAINING:
			case SCREEN_CONFIRM_EXIT_SAVE:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
					break;
						
					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;			
			
			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				
				setScreen( SCREEN_SPLASH_NANO );
			break;
		} // fim switch ( id )		
	}
	
	
	public final void onItemChanged( Menu menu, int id, int index ) {
	}
	

	/**
	 * Carrega um novo cursor, para evitar comportamentos estranhos durante transi��es de tela, uma vez que o cursor
	 * pode estar presente em 2 menus ao mesmo tempo durante transi��es.
	 * @return
	 */
	public static final Drawable getCursor() {
		final BasicAnimatedPattern cursor = new BasicAnimatedPattern( cursorImage, BKG_SPEED, 0 );
		cursor.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		cursor.setSize( ScreenManager.SCREEN_WIDTH, cursor.getFill().getHeight() );
		cursor.defineReferencePixel( Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
		
		return cursor;
	}
	
	
	/**
	 * Define uma soft key a partir de um texto. Equivalente � chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece vis�vel. Para o label estar sempre vis�vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( getBigFont(), getText( textIndex ) ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}				
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null )
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
			break;
			
			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) 
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
			break;
		}
		//#endif
	} // fim do m�todo setSoftKey( byte, Drawable, boolean, int )		
	
	
	public static final ImageFont getFont( int index ) {
		return FONTS[ index ];
	}
	
	
	/**
	 * Verifica se a tela possui a altura m�nima para que a fonte grande seja utilizada em algumas telas de jogo espec�ficas.
	 */
	public static final ImageFont getBigFont() {
		return getFont( ScreenManager.SCREEN_HEIGHT < SCREEN_HEIGHT_FONT_BIG ? FONT_INDEX_TEXT : FONT_INDEX_BIG );
	}
	
	
	public static final void backToPlayScreen() {
		movesScreen = null;
		playScreen.setState( PlayScreen.STATE_PRESS_TO_JUMP );
		setScreen( SCREEN_CONTINUE_GAME );
	}
    
    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "1.0";

        return "<ALN_H>Vers�o " + version + "\n\n";        
    }
	
	
	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	
	private static interface LoadListener {
		public void load( final LoadScreen loadScreen ) throws Exception;
	}
	
	
	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;
		
		private static final byte MAX_DOTS_MODULE = MAX_DOTS -1 ;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;
		
		private final byte previousScreen;
		
		private boolean active = true;


		/**
		 * 
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );
			
			previousScreen = currentScreen;
			
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
			
			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo s�o carregados aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa, para evitar poss�veis atrasos em transi��es e
					// garantir que novos recursos s� ser�o carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );
						
						final LoadScreen loadScreen = this;
						
						loadThread = new Thread() {
							public final void run() {
								try {
									System.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
									texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();

									setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									//#else
//# 										// volta � tela anterior
//# 										setScreen( previousScreen );
									//#endif
								}
							} // fim do m�todo run()
						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for ( byte i = 0; i < dots; ++i )
						temp += '.';

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}					
				}
			}
		} // fim do m�todo update( int )
		
		
		public final void paint( Graphics g ) {
			super.paint( g );
			
			painted = true;
		}
		
		
		private final void setActive( boolean a ) {
			active = a;
		}

	} // fim da classe interna LoadScreen


	// </editor-fold>

}
