/**
 * SplashGame.java
 * �2007 Nano Games
 */

package screens;

//#ifndef LOW_JAR
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class SplashGame extends UpdatableGroup implements Constants, KeyListener
//#ifndef LOW_JAR
// N�o h� aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do c�digo)
, PointerListener
//#endif
{
	/** Tempo de dura��o do splash */
	private short timeCounter = 3000;
	
	/** Quantos tri�ngulos teremos rodando no background. OBS: 360 % N_TRIANGLES DEVE SER IGUAL A ZERO */
	private final byte N_TRIANGLES = 30;
	
	private static final int COLOR_BKG = 0x0037ff;
	
	private static final int COLOR_TRIANGLE = 0x0096ff;
	
	/** Ajudam a calcular os vetores na anima��o de girar o background */
	private final Point vecCalculator1 = new Point(), vecCalculator2 = new Point();
	
	/** M�dulo utilizado por <code>vecCalculator1</code> e <code>vecCalculator2</code> no c�lculo dos vetores */
	private final int vecModule;
	
	/** �ngulo a partir do qual come�amos a calcular os tri�ngulos */
	private short startAngle;
	
	/** Em quantos graus incrementamos <code>startAngle</code> a cada etapa da anima��o */
	private final byte START_ANGLE_INC = 5;
	
	/** Velocidade de anima��o do background */
	private final short BKG_ANIM_TIME = 100;
	
	/** Controla o incremento de �ngulos da anima��o de girar o background ao longo do tempo */
	private final MUV animControl = new MUV();
	
	/** �ndice do logo Hermes & Renato no grupo */
	private final byte LOGO_INDEX = 0;
	
	/** Melhora o posicionamento do logo */
	//#if SCREEN_SIZE == "SMALL"
//# 	private final byte LOGO_OFFSET_X = -19;
//# 	private final byte LOGO_OFFSET_Y = 19;
//# 	
//# 	private final byte LEAF_LEFT_OFFSET_X = -21;
//# 	private final byte LEAF_RIGHT_OFFSET_X = 21;
//# 	
//# 	private final byte LEAVES_OFFSET_Y = -45;
	//#else
	private final byte LOGO_OFFSET_X = -50;
	private final byte LOGO_OFFSET_Y = 41;
	
	private final byte LEAF_LEFT_OFFSET_X = -37;
	private final byte LEAF_RIGHT_OFFSET_X = 34;
	
	private final byte LEAVES_OFFSET_Y = -90;
	//#endif
    
    private final MarqueeLabel pressAnyKeyLabel;
	
	/** Construtor */
	public SplashGame( ImageFont font ) throws Exception
	{
		// Configura o grupo
		super( 7 );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// Carrega o logo
		final String imgExt = ".png";
		final String dir = PATH_SPLASH;
		final DrawableImage logo = new DrawableImage( dir + "logo" + imgExt );
		insertDrawable( logo );
		
		logo.setPosition( ( ScreenManager.SCREEN_WIDTH - logo.getWidth() ) >> 1, 
						  Math.max( 0, ScreenManager.SCREEN_HALF_HEIGHT - logo.getHeight() ) );
		
		
		// Carrega os arcos ol�mpicos
		final DrawableImage ringsLeft = new DrawableImage( dir + "rings" + imgExt );
		final DrawableImage ringsRight = new DrawableImage( ringsLeft );
		ringsRight.mirror( TRANS_MIRROR_H );
		ringsRight.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
		
		// Carrega as folhas de louro
		final DrawableImage leafLeft = new DrawableImage( dir + "leaf" + imgExt );
		insertDrawable( leafLeft );
		
		final DrawableImage leafRight = new DrawableImage( leafLeft  );
		insertDrawable( leafRight );
		leafRight.mirror( TRANS_MIRROR_H );
		leafRight.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		
		// insere os arcos depois das folhas de louro
		insertDrawable( ringsLeft );
		insertDrawable( ringsRight );
		
		// Posiciona os elementos
		ringsLeft.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ringsLeft.getWidth(), ScreenManager.SCREEN_HALF_HEIGHT );
		ringsRight.setPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

		// Carrega o t�tulo
		final DrawableImage title = new DrawableImage( PATH_SPLASH + "title.png" );
		insertDrawable( title );
		title.setPosition( ringsLeft.getPosX() + LOGO_OFFSET_X, Math.min( ringsLeft.getPosY() + LOGO_OFFSET_Y, 
						   size.y - ( title.getHeight() * 4 / 5 ) ) );
		
		leafLeft.setPosition( ringsLeft.getPosX() + LEAF_LEFT_OFFSET_X, ringsLeft.getPosY() + LEAVES_OFFSET_Y );
		leafRight.setPosition( ringsRight.getPosX() + LEAF_RIGHT_OFFSET_X, ringsRight.getPosY() + LEAVES_OFFSET_Y );
		
		// Cria o label de "Pressione Qualquer Tecla"
		pressAnyKeyLabel = new MarqueeLabel( font, AppMIDlet.getText( TEXT_PRESS_ANY_KEY ) );
		insertDrawable( pressAnyKeyLabel );
		pressAnyKeyLabel.setSize( size.x, pressAnyKeyLabel.getHeight() );
		pressAnyKeyLabel.setPosition( 0, ScreenManager.SCREEN_HEIGHT - pressAnyKeyLabel.getHeight() );
        pressAnyKeyLabel.setVisible( false );
		
		// Calcula a maior dimens�o da tela
		vecModule = ( ScreenManager.SCREEN_WIDTH > ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH : ScreenManager.SCREEN_HEIGHT ) << 1;
		
		// Calcula a velocidade da anima��o
		animControl.setSpeed( ( 1000 / BKG_ANIM_TIME ) * START_ANGLE_INC );
	}

	
	public final void update( int delta )
	{
		
		// Controla a anima��o de girar o background
		startAngle = ( short )( ( startAngle + animControl.updateInt( delta ) ) % 360 );
		
		if( timeCounter < 0 )
		{
			super.update( delta );
		}
		else
		{
			timeCounter -= delta;
            
            if ( timeCounter < 0 ) {
                pressAnyKeyLabel.setTextOffset( size.x );
                pressAnyKeyLabel.setVisible( true );
            }
		}
	}

	protected final void paint( Graphics g )
	{
		// Desenha o background. Ao inv�s de desenharmos tri�ngulos alternando cores, pintamos toda a tela de
		// uma cor e depois os tri�ngulos na outra cor. Assim otimizamos o m�todo, j� que o n�mero de chamadas
		// a Graphics.fillTriangle cair� pela metade
		g.setColor( COLOR_BKG );
		g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		g.setColor( COLOR_TRIANGLE );
		
		final int INC = 360 / N_TRIANGLES;
		final int DOUBLE_INC = INC << 1;
		final short max = ( short )( 360 + startAngle );
		final int y1 = ScreenManager.SCREEN_HALF_HEIGHT - ( getDrawable( LOGO_INDEX ).getHeight() >> 1 );
		for( short i = startAngle ; i < max ; i += DOUBLE_INC )
		{
			vecCalculator1.setVector( i, vecModule );
			vecCalculator1.addEquals( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
			
			vecCalculator2.setVector( i + INC, vecModule );
			vecCalculator2.addEquals( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
			
			g.fillTriangle( ScreenManager.SCREEN_HALF_WIDTH, y1, vecCalculator1.x, vecCalculator1.y, vecCalculator2.x, vecCalculator2.y );
			
//			g.fillTriangle( ScreenManager.SCREEN_HALF_WIDTH, y1, 
//							NanoMath.min( ScreenManager.SCREEN_WIDTH, NanoMath.max( 0, vecCalculator1.x ) ),
//							NanoMath.min( ScreenManager.SCREEN_HEIGHT, NanoMath.max( 0, vecCalculator1.y ) ), 
//							NanoMath.min( ScreenManager.SCREEN_WIDTH, NanoMath.max( 0, vecCalculator2.x ) ),
//							NanoMath.min( ScreenManager.SCREEN_HEIGHT, NanoMath.max( 0, vecCalculator2.y ) ) );			
		}
		
		// Desenha as imagens
		super.paint( g );
	}

	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.exit();
				break;

			default:
			//#if DEBUG == "true"
				// Os splashs n�o podem ser acelerados pelo usu�rio
			callNextScreen();
			//#else
//# 				if( timeCounter < 0 )
//# 					callNextScreen();
			//#endif
		}
	}

	public final void keyReleased( int key )
	{
	}

	private final void callNextScreen()
	{
		AppMIDlet.setScreen( GameMIDlet.SCREEN_LOADING_2 );
	}

	//#ifndef LOW_JAR
	public final void onPointerDragged( int x, int y )
	{
	}

	public final void onPointerPressed( int x, int y )
	{
		if( timeCounter < 0 )
			callNextScreen();
	}

	public final void onPointerReleased( int x, int y )
	{
	}
	//#endif
}
