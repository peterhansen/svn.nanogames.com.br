/**
 * BubblesTransition.java
 * �2008 Nano Games.
 *
 * Created on May 6, 2008 5:31:46 PM.
 */

//#if JAR != "min"

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class WaterTransition extends UpdatableGroup implements Constants, KeyListener
//#if SCREEN_SIZE != "SMALL"
	, PointerListener
//#endif
{
	
	/** quantidade total de drawables no grupo */
	private static final byte TOTAL_ITEMS = 4;
	
	private static final byte DRAWABLE_ITEMS = 1;
	
	/** Dura��o da anima��o de transi��o da �gua. */
	private static final short WATER_TRANSITION_TIME = 1400;
	
	private static final byte STATE_NONE	= 0;
	private static final byte STATE_UP		= 1;
	private static final byte STATE_DOWN	= 2;

	/** estado atual de transi��o */
	private byte transitionState;
	
	/** Tela atual (que ser� substitu�da). */
	private Drawable previousScreen;
	
	/** Viewport da tela anterior. */
	private final Rectangle previousViewport = new Rectangle();
	
	/** Pr�xima tela a ser definida como ativa pelo ScreenManager */
	private Drawable nextScreen;
	
	/** Viewport da pr�xima tela. */
	private final Rectangle nextViewport = new Rectangle();
	
	/** Grupo que cont�m o pattern de baixo, preenchimento do meio e o pattern de cima. */
	private final UpdatableGroup group;
	
	private final MUV bubblesSpeed;
	
	private final byte BUBBLES_HEIGHT;
	
	private final Mutex mutex = new Mutex();
	
	private byte bkgType;
	

	public WaterTransition() throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// insere o grupo da �gua
		final DrawableImage imgBubbles = new DrawableImage( PATH_IMAGES + "bubbles.png" );
		group = new UpdatableGroup( 4 );
		insertDrawable( group );
		
		final BasicAnimatedPattern top = new BasicAnimatedPattern( imgBubbles, ScreenManager.SCREEN_HALF_WIDTH, 0 );
		top.setAnimation( BasicAnimatedPattern.ANIMATION_ALTERNATE_HORIZONTAL );
		top.setSize( size.x, imgBubbles.getHeight() );
		
		BUBBLES_HEIGHT = ( byte ) ( top.getHeight() );
		group.setSize( size.x, size.y + BUBBLES_HEIGHT );
		
		final Pattern fill = new Pattern( new DrawableImage( PATH_IMAGES + "water.png" ) );
		fill.setPosition( 0, BUBBLES_HEIGHT >> 1 );
		fill.setSize( size.x, NanoMath.min( group.getHeight() - fill.getPosY(), fill.getFill().getHeight() ) );

		group.insertDrawable( fill );
		group.insertDrawable( top );
	
		int FILL_Y = fill.getPosY() + fill.getHeight();
		if ( FILL_Y < group.getHeight() ) {
			final Pattern solid = new Pattern( BACKGROUND_COLOR );
			solid.setSize( size.x, group.getHeight() - FILL_Y );
			solid.setPosition( 0, FILL_Y );
			
			group.insertDrawable( solid );
		}
		
		group.defineReferencePixel( 0, BUBBLES_HEIGHT );
		
		bubblesSpeed = new MUV( size.y + group.getHeight() * 1000 / WATER_TRANSITION_TIME );
	}


	public synchronized final void setNextScreen( Drawable nextScreen, byte bkgType ) {
		this.bkgType = bkgType;
		
		while ( activeDrawables > DRAWABLE_ITEMS )
			removeDrawable( 0 );
		
		previousScreen = ScreenManager.getInstance().getCurrentScreen();
		if ( previousScreen == this ) {
			previousScreen = null;
		} else if ( previousScreen != null ) {
			previousScreen.setViewport( previousViewport );
		}
		
		this.nextScreen = nextScreen;
		nextScreen.setViewport( nextViewport );
		insertDrawable( previousScreen, 0 );
		insertDrawable( nextScreen, 0 );
		
		ScreenManager.getInstance().setCurrentScreen( this );
		setTransitionState( STATE_UP );
	}


	public synchronized final void setTransitionState( byte state ) {
		mutex.acquire();
		
		transitionState = state;
		
		switch ( state ) {
			case STATE_NONE:
				if ( previousScreen != null ) {
					previousScreen.setViewport( null );
					previousScreen = null;
				}

				nextScreen.setViewport( null );

				ScreenManager.getInstance().setCurrentScreen( nextScreen );

				nextScreen = null;

				while ( activeDrawables > DRAWABLE_ITEMS )
					removeDrawable( 0 );				
			break;
			
			case STATE_UP:
				bubblesSpeed.setSpeed( -Math.abs( bubblesSpeed.getSpeed() ) );
				
				group.setPosition( 0, size.y );
				previousViewport.set( ScreenManager.viewport );
				nextViewport.set( 0, 0, 0, 0 );
			break;
			
			case STATE_DOWN:
				GameMIDlet.setBackground( bkgType );
				
				bubblesSpeed.setSpeed( Math.abs( bubblesSpeed.getSpeed() ) );
				
				group.setPosition( 0, -BUBBLES_HEIGHT );
				previousViewport.set( 0, 0, 0, 0 );							
				nextViewport.set( 0, 0, 0, 0 );
			break;
		}
		
		mutex.release();
	}

	
	public final void update( int delta ) {
		super.update( delta );
		
		switch ( transitionState ) {
			case STATE_UP:
				group.move( 0, bubblesSpeed.updateInt( delta ) );
				
				previousViewport.set( 0, 0, size.x, group.getRefPixelY() );
				
				if ( group.getPosY() <= -BUBBLES_HEIGHT )
					setTransitionState( STATE_DOWN );
			break;
			
			case STATE_DOWN:
				group.move( 0, bubblesSpeed.updateInt( delta ) );
				
				previousViewport.set( 0, group.getRefPixelY(), size.x, size.y - group.getRefPixelY() );
				nextViewport.set( 0, 0, size.x, group.getRefPixelY() );						
				
				if ( group.getPosY() >= size.y )
					setTransitionState( STATE_NONE );				
			break;
		}
	}


	protected final void paint( Graphics g ) {
		mutex.acquire();
		
		super.paint( g );
		
		mutex.release();
	}
	

	public final void keyPressed( int key ) {
		switch ( transitionState ) {
			case STATE_UP:
				setTransitionState( STATE_DOWN );
			break;
			
			case STATE_DOWN:
				setTransitionState( STATE_NONE );
			break;
		}
	}


	public final void keyReleased( int key ) {
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif
	
}

//#endif