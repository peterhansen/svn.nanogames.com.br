/**
 * Bo�a.java
 * �2008 Nano Games.
 *
 * Created on Jun 9, 2008 6:32:56 PM.
 */

package core;

import br.com.nanogames.components.Sprite;


/**
 * 
 * @author Peter
 */
public final class Boca extends Sprite implements Constants {
	
	public static final byte SEQUENCE_STAND_UP			= 0;
	public static final byte SEQUENCE_PREPARING_JUMP	= 1;
	public static final byte SEQUENCE_JUMPING			= 2;
	public static final byte SEQUENCE_BARRIGADA			= 3;
	public static final byte SEQUENCE_TWIST				= 4;
	public static final byte SEQUENCE_STAR				= 5;
	public static final byte SEQUENCE_TUCK				= 6;
	
	/** Quantidade de passos necess�ria para realizar a troca de um frame. */
	private static final byte STEPS_FRAME_CHANGE = 2;

	private static Boca boca;
	
	public static final byte ROTATION_NONE				= 0;
	public static final byte ROTATION_CLOCKWISE			= 1;
	public static final byte ROTATION_COUNTER_CLOCKWISE	= 2;
	
	private byte rotationDiretion;
	
	private byte rotation;
	
	private short accRotationTime;
	
	
	private Boca() throws Exception {
		super( PATH_IMAGES + "boca.bin", PATH_IMAGES + "boca" );
	}
	
	
	private Boca( Boca boca ) {
		super( boca );
		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		setRotation( ROTATION_90 );		
	}
	
	
	public static final Boca loadBoca() throws Exception {
		return new Boca( boca );
	}
	
	
	public static final void loadImages() throws Exception {
		boca = new Boca();
	}


	/**
	 * Atualiza o movimento de rota��o atual ap�s um intervalo de tempo.
	 * @param delta intervalo de tempo em milisegundos.
	 * @return boolean indicando se uma rota��o de 45� foi completada.
	 */
	public final boolean updateRotation( int delta ) {
		accRotationTime += delta;
		
		if ( accRotationTime >= ROTATION_90_TIME ) {
			accRotationTime %= ROTATION_90_TIME;
			
			switch ( rotationDiretion ) {
				case ROTATION_CLOCKWISE:
					setRotation( ( rotation + ROTATION_360 - 1 ) % ROTATION_360 );
				return true;
				
				case ROTATION_COUNTER_CLOCKWISE:
					setRotation( ( rotation + 1 ) % ROTATION_360 );
				return true;
			}
		}
		
		return false;
	}
	
	
	public final void reset() {
		cancelRotation();
		setSequence( SEQUENCE_STAND_UP );
		setRotation( ROTATION_90 );
		setVisible( true );
	}
	
	
	public final void cancelRotation() {
		accRotationTime = 0;
		rotationDiretion = ROTATION_NONE;
	}
	
	
	public final void setRotationDirection( byte direction ) {
		this.rotationDiretion = direction;
	}


	public final void setRotation( int rotation ) {
		this.rotation = ( byte ) rotation;
		
		switch ( rotation ) {
			case ROTATION_0:
				setTransform( TRANS_ROT90 );
			break;
			
			case ROTATION_90:
				setTransform( TRANS_NONE );
			break;
			
			case ROTATION_180:
				setTransform( TRANS_ROT270 );
			break;
			
			case ROTATION_270:
				setTransform( TRANS_ROT180 );
			break;
		}
	}
	
	
	public final byte getPlayerRotation() {
		return rotation;
	}
	
	
	public final int getFrameWidth() {
		switch ( rotation ) {
			case ROTATION_0:
			case ROTATION_180:
				return frame.getHeight();

			default:
				return frame.getWidth();
		}
	}
	
	
	/**
	 * Define o frame atual do Bo�a a partir da quantidade de passos realizados no movimento atual.
	 */
	public final void setStepsMade( int stepsMade ) {
		setFrame( ( stepsMade / STEPS_FRAME_CHANGE ) % sequence.length );
	}
	
}
