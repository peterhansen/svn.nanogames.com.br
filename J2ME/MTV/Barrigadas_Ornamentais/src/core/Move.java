/**
 * Sequence.java
 * �2008 Nano Games.
 *
 * Created on Jun 3, 2008 11:09:10 PM.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.util.NanoMath;


/**
 * 
 * @author Peter
 */
public final class Move extends DrawableGroup implements Constants {
	
	public static final byte TYPE_EMPTY				= 0;
	public static final byte TYPE_OK				= 1;
	public static final byte TYPE_BARRIGADA			= 2;
	public static final byte TYPE_TWIST				= 3;
	public static final byte TYPE_TWIST_2X			= 4;
	public static final byte TYPE_TWIST_3X			= 5;
	public static final byte TYPE_TWIST_360			= 6;
	public static final byte TYPE_TWIST_INVERTED	= 7;
	public static final byte TYPE_TWIST_720			= 8;
	public static final byte TYPE_STAR				= 9;
	public static final byte TYPE_STAR_2X			= 10;
	public static final byte TYPE_STAR_3X			= 11;
	public static final byte TYPE_STAR_360			= 12;
	public static final byte TYPE_STAR_720			= 13;
	public static final byte TYPE_BOMB_360			= 14;
	public static final byte TYPE_BOMB_720			= 15;
	public static final byte TYPE_BOMB_INVERTED		= 16;
	public static final byte TYPE_COFRINHO_360		= 17;
	public static final byte TYPE_COFRINHO_720		= 18;
	
	public static final byte TYPE_TOTAL = TYPE_COFRINHO_720 + 1;
	
	private static final byte IMG_EMPTY			= 0;
	private static final byte IMG_BARRIGADA		= 1;
	private static final byte IMG_TWIST			= 2;
	private static final byte IMG_STAR			= 3;
	private static final byte IMG_COFRINHO		= 4;
	private static final byte IMG_BOMB			= 5;
	private static final byte IMG_360			= 6;
	private static final byte IMG_720			= 7;
	private static final byte IMG_2X			= 8;
	private static final byte IMG_3X			= 9;
	private static final byte IMG_INVERT		= 10;
	private static final byte IMG_OK			= 11;
	
	/** Quantidade total de �cones diferentes. */
	private static final byte IMAGES_TOTAL = IMG_OK + 1;
	
	private static final DrawableImage[] IMAGES = new DrawableImage[ IMAGES_TOTAL ];
	
	/** Sequ�ncias de passos de cada movimento. */
	private static final byte[][] STEPS = { 
		// vazio
		{ },
		// ok
		{ },
		// barrigada
		{ STEP_FIRE, STEP_FIRE, STEP_FIRE, STEP_FIRE, STEP_FIRE },
		// twist
		{ STEP_DOWN, STEP_DOWN, STEP_FIRE, STEP_FIRE, STEP_UP, STEP_UP, STEP_FIRE, STEP_FIRE },
		// twist 2X
		{ STEP_DOWN, STEP_DOWN, STEP_FIRE, STEP_FIRE, STEP_UP, STEP_UP, STEP_FIRE, STEP_FIRE, STEP_DOWN, STEP_DOWN, STEP_FIRE, STEP_FIRE, STEP_UP, STEP_UP, STEP_FIRE, STEP_FIRE },
		// twist 3X
		{ STEP_DOWN, STEP_DOWN, STEP_FIRE, STEP_FIRE, STEP_UP, STEP_UP, STEP_FIRE, STEP_FIRE, STEP_DOWN, STEP_DOWN, STEP_FIRE, STEP_FIRE, STEP_UP, STEP_UP, STEP_FIRE, STEP_FIRE, STEP_DOWN, STEP_DOWN, STEP_FIRE, STEP_FIRE, STEP_UP, STEP_UP, STEP_FIRE, STEP_FIRE },
		// twist 360�
		{ STEP_DOWN, STEP_DOWN, STEP_RIGHT_90, STEP_FIRE, STEP_FIRE, STEP_RIGHT_90, STEP_UP, STEP_UP, STEP_RIGHT_90, STEP_FIRE, STEP_FIRE, STEP_RIGHT_90, },
		// twist invertido
		{ STEP_RIGHT_180, STEP_DOWN, STEP_FIRE, STEP_UP, STEP_FIRE, STEP_DOWN, STEP_FIRE, STEP_UP, STEP_FIRE, STEP_DOWN, STEP_FIRE, STEP_UP, STEP_FIRE, STEP_DOWN, STEP_FIRE, STEP_UP, STEP_FIRE, STEP_LEFT_180  },
		// twist 720�
		{ STEP_DOWN, STEP_RIGHT_90, STEP_DOWN, STEP_RIGHT_90, STEP_FIRE, STEP_RIGHT_90, STEP_FIRE, STEP_RIGHT_90, STEP_UP, STEP_RIGHT_90, STEP_UP, STEP_RIGHT_90, STEP_FIRE, STEP_RIGHT_90, STEP_FIRE, STEP_RIGHT_90, },
		// estrela
		{ STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP },
		// estrela 2X
		{ STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP },
		// estrela 3X
		{ STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP },
		// estrela 360�
		{ STEP_DOWN, STEP_UP, STEP_RIGHT_90, STEP_DOWN, STEP_UP, STEP_RIGHT_90, STEP_DOWN, STEP_UP, STEP_RIGHT_90, STEP_DOWN, STEP_UP, STEP_RIGHT_90, },
		// estrela 720�
		{ STEP_DOWN, STEP_RIGHT_90, STEP_UP, STEP_RIGHT_90, STEP_DOWN, STEP_RIGHT_90, STEP_UP, STEP_RIGHT_90, STEP_DOWN, STEP_RIGHT_90, STEP_UP, STEP_RIGHT_90, STEP_DOWN, STEP_RIGHT_90, STEP_UP, STEP_RIGHT_90, },
		// bomba 360�
		{ STEP_FIRE, STEP_LEFT_360, STEP_FIRE },
		// bomba 720�
		{ STEP_FIRE, STEP_FIRE, STEP_LEFT_720, STEP_FIRE, STEP_FIRE },
		// bomba invertida
		{ STEP_FIRE, STEP_LEFT_360, STEP_FIRE, STEP_RIGHT_360, STEP_FIRE },
		// cofrinho 360�
		{ STEP_DOWN, STEP_UP, STEP_LEFT_360, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP  },
		// cofrinho 720�
		{ STEP_DOWN, STEP_UP, STEP_LEFT_720, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP, STEP_DOWN, STEP_UP  },
	};
	
	/** Multiplicador de dificuldade de cada sequ�ncia. */
	private static final int[] FP_DIFFICULTY_MULTIPLIER = { 
		0, // vazio
		0, // ok
		FP_MAX_DIFFICULTY_MULTIPLIER * 30 / 100, // barrigada
		FP_MAX_DIFFICULTY_MULTIPLIER * 15 / 100, // twist
		FP_MAX_DIFFICULTY_MULTIPLIER * 35 / 100, // twist 2X
		FP_MAX_DIFFICULTY_MULTIPLIER * 70 / 100, // twist 3X
		FP_MAX_DIFFICULTY_MULTIPLIER * 85 / 100, // twist 360�
		FP_MAX_DIFFICULTY_MULTIPLIER * 90 / 100, // twist invertido
		FP_MAX_DIFFICULTY_MULTIPLIER * 100 / 100, // twist 720�
		FP_MAX_DIFFICULTY_MULTIPLIER * 15 / 100, // estrela
		FP_MAX_DIFFICULTY_MULTIPLIER * 35 / 100, // estrela 2X
		FP_MAX_DIFFICULTY_MULTIPLIER * 70 / 100, // estrela 3X
		FP_MAX_DIFFICULTY_MULTIPLIER * 85 / 100, // estrela 360�
		FP_MAX_DIFFICULTY_MULTIPLIER * 100 / 100, // estrela 720�
		FP_MAX_DIFFICULTY_MULTIPLIER * 35 / 100, // bomba 360�
		FP_MAX_DIFFICULTY_MULTIPLIER * 50 / 100, // bomba 720�
		FP_MAX_DIFFICULTY_MULTIPLIER * 65 / 100, // bomba invertida
		FP_MAX_DIFFICULTY_MULTIPLIER * 45 / 100, // cofrinho 360�
		FP_MAX_DIFFICULTY_MULTIPLIER * 65 / 100, // cofrinho 720�
	};
	
	
	/**
	 * Sequ�ncia de anima��o do Bo�a utilizada em cada movimento.
	 */
	private static final byte[] SEQUENCES = {
		Boca.SEQUENCE_STAR, // vazio
		Boca.SEQUENCE_STAR, // ok
		Boca.SEQUENCE_BARRIGADA, // barrigada
		Boca.SEQUENCE_TWIST, // twist
		Boca.SEQUENCE_TWIST, // twist 2X
		Boca.SEQUENCE_TWIST, // twist 3X
		Boca.SEQUENCE_TWIST, // twist 360�
		Boca.SEQUENCE_TWIST, // twist invertido
		Boca.SEQUENCE_TWIST, // twist 720�
		Boca.SEQUENCE_STAR, // estrela
		Boca.SEQUENCE_STAR, // estrela 2X
		Boca.SEQUENCE_STAR, // estrela 3X
		Boca.SEQUENCE_STAR, // estrela 360�
		Boca.SEQUENCE_STAR, // estrela 720�
		Boca.SEQUENCE_TUCK, // bomba 360�
		Boca.SEQUENCE_TUCK, // bomba 720�
		Boca.SEQUENCE_TUCK, // bomba invertida
		Boca.SEQUENCE_STAR, // cofrinho 360�
		Boca.SEQUENCE_STAR, // cofrinho 720�
	};
	
	
	public final byte[] steps;

	public final byte type;
	
	/** Quantidade de erros de passos do jogador nesse movimento. */
	private byte mistakes;
	
	private byte stepsMade;
	
	/** Passos simples (sem repeti��o) realizados - valor armazenado para associar a sequ�ncia de teclas � sequ�ncia de frames do Bo�a. */
	private byte singleStepsMade;
	
	
	public Move( int type ) throws Exception {
		super( 2 );
		
		this.type = ( byte ) type;
		steps = STEPS[ type ];
		
		setSize( MOVE_ICON_WIDTH, MOVE_ICON_HEIGHT );
		
		byte iconIndex = -1;
		int smallIconIndex = -1;
		
		switch ( type ) {
			case TYPE_EMPTY:
				iconIndex = IMG_EMPTY;
			break;
			
			case TYPE_BARRIGADA:
				iconIndex = IMG_BARRIGADA;
			break;
			
			case TYPE_OK:
				iconIndex = IMG_OK;
			break;
			
			case TYPE_TWIST_2X:
			case TYPE_TWIST_3X:
				smallIconIndex = type == TYPE_TWIST_2X ? IMG_2X : IMG_3X;
			case TYPE_TWIST:
				iconIndex = IMG_TWIST;
			break;
			
			case TYPE_TWIST_360:
			case TYPE_TWIST_720:
				smallIconIndex = type == TYPE_TWIST_360 ? IMG_360 : IMG_720;
				iconIndex = IMG_TWIST;
			break;
			
			case TYPE_STAR_2X:
			case TYPE_STAR_3X:
				smallIconIndex = IMG_2X + type - TYPE_STAR_2X;
			case TYPE_STAR:
				iconIndex = IMG_STAR;
			break;
			
			case TYPE_STAR_360:
			case TYPE_STAR_720:
				iconIndex = IMG_STAR;
				smallIconIndex = IMG_360 + type - TYPE_STAR_360;
			break;
			
			case TYPE_BOMB_360:
			case TYPE_BOMB_720:
				iconIndex = IMG_BOMB;
				smallIconIndex = IMG_360 + type - TYPE_BOMB_360;
			break;
			
			case TYPE_BOMB_INVERTED:
				iconIndex = IMG_BOMB;
				smallIconIndex = IMG_INVERT;
			break;
			
			case TYPE_COFRINHO_360:
			case TYPE_COFRINHO_720:
				iconIndex = IMG_COFRINHO;
				smallIconIndex = IMG_360 + type - TYPE_COFRINHO_360;
			break;

			case TYPE_TWIST_INVERTED:
				iconIndex = IMG_TWIST;
				smallIconIndex = IMG_INVERT;
			break;
		} // fim switch ( type )
		
		final DrawableImage icon = new DrawableImage( IMAGES[ iconIndex ] );
		icon.setPosition( ( size.x - icon.getWidth() ) >> 1, ( size.y - icon.getHeight() ) >> 1 );
		insertDrawable( icon );
		
		if ( smallIconIndex >= 0 ) {
			final DrawableImage smallIcon = new DrawableImage( IMAGES[ smallIconIndex ] );
			smallIcon.setPosition( size.x - smallIcon.getWidth(), size.y - smallIcon.getHeight() );
			insertDrawable( smallIcon );
		}
	}
	
	
	public static final void loadImages() throws Exception {
		for ( byte i = 0; i < IMAGES_TOTAL; ++i )
			IMAGES[ i ] = new DrawableImage( PATH_MOVES + i + ".png" );
	}
	
	
	/**
	 * Verifica se o passo executado est� correto, e atualiza a contagem de passos realizados e precis�o do movimento.
	 * @param stepType passo executado.
	 * @param increment indica se o contador de passos feitos deve ser atualizado. Caso seja <code>false</code>, atualiza
	 * apenas o contador de erros (caso o passo seja errado).
	 * @return boolean indicando se o passo executado foi o correto ou n�o.
	 */
	public final boolean checkStep( byte stepType, boolean increment ) {
		final boolean correct = stepType == steps[ stepsMade ];
		if ( correct ) {
			if ( increment ) {
				switch ( STEPS[ type ][ stepsMade ] ) {
					case STEP_DOWN:
					case STEP_FIRE:
					case STEP_UP:
						++singleStepsMade;
					break;
				}			

				++stepsMade;
			}
		} else {
			++mistakes;
		}
		
		return correct;
	}
	
	
	/**
	 * @return pontua��o do movimento em nota��o de ponto fixo.
	 */
	public final int getScoreFP() {
		if ( stepsMade > 0 ) {
			final int FP_MISTAKES = NanoMath.toFixed( mistakes );
			final int FP_STEPS_MADE = NanoMath.toFixed( stepsMade );
			final int FP_MOVES_MADE_PERCENT = NanoMath.divFixed( FP_STEPS_MADE, NanoMath.toFixed( steps.length ) );
			final int FP_PRECISION = NanoMath.ONE - NanoMath.divFixed( NanoMath.toFixed( mistakes ), FP_STEPS_MADE + FP_MISTAKES );
			
			return NanoMath.mulFixed( NanoMath.mulFixed( getMaxScoreFP(), FP_MOVES_MADE_PERCENT ), FP_PRECISION );
		}
		
		return 0;
	}
	
	
	/**
	 * @return pontua��o m�xima do movimento atual, levando-se em conta o multiplicador.
	 */
	public final int getMaxScoreFP() {
		return NanoMath.mulFixed( FP_DEFAULT_MOVE_SCORE, FP_DIFFICULTY_MULTIPLIER[ type ] );
	}
	
	
	public static final int getHighestScore() {
		return NanoMath.mulFixed( FP_DEFAULT_MOVE_SCORE, FP_MAX_DIFFICULTY_MULTIPLIER );
	}

	
	public static final int getBarrigadaMaxScoreFP() {
		return NanoMath.mulFixed( FP_DEFAULT_MOVE_SCORE, FP_DIFFICULTY_MULTIPLIER[ TYPE_BARRIGADA ] );
	}	
	
	
	public final void reset() {
		stepsMade = 0;
		singleStepsMade = 0;
		mistakes = 0;
	}
	
	
	public final byte getSequence() {
		return SEQUENCES[ type ];
	}
	
	
	/**
	 * @return quantidade de passos simples realizados.
	 */
	public final byte getSingleStepsMade() {
		return singleStepsMade;
	}

	
	public static final int getRandomType() {
		return TYPE_TWIST + NanoMath.randInt( TYPE_TOTAL - TYPE_TWIST );
	}
	
	
	public static final int getIndex( int previousIndex, int diff ) {
		int index = ( previousIndex + TYPE_TOTAL + diff ) % TYPE_TOTAL;
		return ( ( index == TYPE_BARRIGADA || index == TYPE_OK ) ? getIndex( index, diff ) : index );
	}
	
	
	public final int getStepsMadePercentFP() {
		return NanoMath.divInt( stepsMade, steps.length );
	}
}
