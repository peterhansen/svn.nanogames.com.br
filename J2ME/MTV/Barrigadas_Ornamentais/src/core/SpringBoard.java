/**
 * SpringBoard.java
 * �2008 Nano Games.
 *
 * Created on Jun 16, 2008 12:26:59 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class SpringBoard extends Drawable implements Updatable, Constants {
	
	private static final int COLOR = 0xcecece;
	private static final int COLOR_DARK = 0x002544;
	
	private static final int COLOR_DEMO = 0xf6fc01;
	private static final int COLOR_DARK_DEMO = 0xfcc401;
	
	private final int color;
	private final int colorDark;
	
	protected byte percent;
	
	private final MUV speed = new MUV( 1500 );
	
	/** Indica se est� vibrando (logo ap�s o pulo do personagem). */
	private boolean vibrating;
	
	private short currentMaxHeight = SPRINGBOARD_MAX_HEIGHT;
	
	
	public SpringBoard( boolean demo ) {
		setSize( SPRINGBOARD_WIDTH << 1, SPRINGBOARD_MAX_HEIGHT );
		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		
		color = demo ? COLOR_DEMO : COLOR;
		colorDark = demo ? COLOR_DARK_DEMO : COLOR_DARK;
		
		setPercent( 0 );
	}
	
	
	protected final void paint( Graphics g ) {
		// o offset em x � para evitar que a ponta do trampolim fique vertical (essa �rea passa a ficar fora da interse��o
		// de clip), e em y � para corrigir o posicionamento vertical do trampolim quando percent troca de sinal
		drawEllipse( g, translate.x + ( size.x >> 1 ), translate.y + ( size.y >> 1 ) + ( percent < 0 ? -4 : 2 ), ( size.x >> 1 ) + 10, size.y >> 1 );
	}
	
	
	private final void drawEllipse( Graphics g, int centerX, int centerY, int xRadius, int yRadius ) {
		int x = xRadius;
		int y = 0;
		int xChange = yRadius * yRadius * ( 1 - ( xRadius << 1 ) );
		int yChange = xRadius * xRadius;
		int ellipseError = 0;
		final int twoA2 = yChange << 1;
		final int twoB2 = ( yRadius * yRadius ) << 1;
		int stopX = twoB2 * xRadius;
		int stopY = 0;
		
		while ( stopX >= stopY ) { // 1st set of points, y > -1
			plot( g, centerX, centerY, x, y ); // subroutine appears later
			++y;
			stopY += twoA2;
			ellipseError += yChange;
			yChange += twoA2;
			if ( ( ( ellipseError << 1 ) + xChange ) > 0 ) {
				--x;
				stopX -= twoB2;
				ellipseError += xChange;
				xChange += twoB2;
			}
		}
		
		// 1st point set is done; start the 2nd set of points
		x = 0;
		y = yRadius;
		xChange = twoB2 >> 1;
		yChange = xRadius * xRadius * ( 1 - ( yRadius << 1 ) );
		ellipseError = 0;
		stopX = 0;
		stopY = twoA2 * yRadius;
		while ( stopX <= stopY ) { //2nd set of points, y < -1
			plot( g, centerX, centerY, x, y ); // subroutine appears later
			++x;
			stopX += twoB2;
			ellipseError += xChange;
			xChange += twoB2;
			if ( ( ( ellipseError << 1 ) + yChange ) > 0 ) {
				--y;
				stopY -= twoA2;
				ellipseError += yChange;
				yChange += twoA2;
			}
		}
	}
	
	
	private final void plot( Graphics g, int centerX, int centerY, int x, int y ) {
		centerX -= x;
		g.setColor( colorDark );
		if ( percent >= 0 ) {
			centerY -= y;
			
			// 3� quadrante
			g.drawRect( centerX, centerY + 1, 1, 1 );
			g.drawRect( centerX, centerY - 1, 1, 1 );
			
			g.setColor( color );
			g.drawRect( centerX, centerY, 2, 1 );
		} else {
			centerY += y;
			
			// 2� quadrante
			g.drawRect( centerX, centerY + 1, 1, 1 );
			g.drawRect( centerX, centerY - 1, 1, 1 );
			
			g.setColor( COLOR );
			g.drawRect( centerX, centerY, 2, 1 );
		}

		
		// originalmente:
		// 1� quadrante
//		g.drawRect( centerX + x, centerY + y, 1, 1 );
		// 2� quadrante
//		g.drawRect( centerX - x, centerY + y, 1, 1 );
		// 3� quadrante
//		g.drawRect( centerX - x, centerY - y, 2, 2 );
		// 4� quadrante
//		g.drawRect( centerX + x, centerY - y, 1, 1 );
	}
	

	public final void setPercent( int percent ) {
		percent = NanoMath.clamp( percent, -100, 100 );
		this.percent = ( byte ) percent;
		
		final Point previousRefPoint = getRefPixelPosition();
		setSize( size.x, NanoMath.max( NanoMath.abs( percent ) * currentMaxHeight / 100, SPRINGBOARD_MIN_HEIGHT ) );
		if ( percent >= 0 ) {
			defineReferencePixel( size.x >> 1, 5 );
		} else {
			defineReferencePixel( size.x >> 1, size.y - 4 );
		}
		
		setRefPixelPosition( previousRefPoint );
	}


	public final void update( int delta ) {
		if ( vibrating ) {
			setPercent( percent + speed.updateInt( delta ) );

			if ( percent >= 100 || percent <= -100 ) {
				// reduz a amplitude da vibra��o, at� parar
				currentMaxHeight = ( short ) ( currentMaxHeight * 89 / 100 );
				if ( currentMaxHeight <= ( SPRINGBOARD_MIN_HEIGHT >> 1 ) )
					setVibrating( false );
				else
					speed.setSpeed( -speed.getSpeed() );
			}
		}
	}
	
	
	public final void setVibrating( boolean vibrating ) {
		this.vibrating = vibrating;
		if ( !vibrating ) {
			setPercent( 0 );
			currentMaxHeight = SPRINGBOARD_MAX_HEIGHT;
		}
	}
	
	
}
