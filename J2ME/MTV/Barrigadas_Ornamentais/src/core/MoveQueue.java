/**
 * MovementQueue.java
 * �2008 Nano Games.
 *
 * Created on Jun 3, 2008 11:24:08 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
import screens.PlayScreen;


/**
 * 
 * @author Peter
 */
public final class MoveQueue extends UpdatableGroup implements KeyListener, Constants {

	/** Quantidade total de tipos de passos diferentes. */
	private static final byte TOTAL_STEPS = 7;
	
	/** Imagens dos passos (pr�-alocados). */
	public static DrawableImage[] IMG_STEPS;
	
	private static short stepImageLargestWidth;
	
	private static DrawableImage imgRepeatLeft;
	private static DrawableImage imgRepeatRight;
	private static DrawableImage imgRepeatLeftBottom;
	private static DrawableImage imgRepeatRightBottom;
	
	private static DrawableImage imgArrowRight;
	private static DrawableImage imgMoveBorderLeft;
	private static DrawableImage imgKnot;
	private static DrawableImage imgBar;
	private static DrawableImage imgBarH;
	private static DrawableImage imgFill;
	private static DrawableImage imgArrowUp;
	
	
	/** Movimentos. */
	private static Move[] moves;
	
	/** �ndice do movimento atual. */
	private byte currentMoveIndex;
	
	/** Refer�ncia para o movimento atual. */
	private Move currentMove;
	
	/** �ndice do passo atual do movimento atual. */
	private byte currentStepIndex;
	
	/** �ndice absoluto do passo atual no grupo de passos. */
	private byte currentStepAbsolutIndex; // Absolut irm�o!
	
	/** Tecla mantida pressionada atualmente (0 indica nenhuma). */
	private int keyHeld;
	
	/** Vari�vel utilizada para melhorar a jogabilidade em aparelhos com tempo de resposta de teclas lento (e/ou taxas de 
	 * atualiza��o baixas), onde muitas vezes a rota��o de 90� torna-se dif�cil pois ou rota��o n�o � atualizada, 
	 * ou o passo de tempo � muito alto - logo, ou n�o ocorre rota��o, ou ela � de 180�.
	 */
	private boolean rotateUpdated = true;
	
	private static final byte ROTATE_DELTA_MIN = ROTATION_90_TIME / 3;
	
	private static final int KEY_DUMMY = -12345;
	
	/** Tecla que deve ser mantida pressionada para encerrar o passo atual. */
	private int keyToHold = KEY_DUMMY;
	
	/** Vari�vel que faz o controle de quantas rota��es de 45� faltam ser realizadas na rota��o atual, para garantir
	 * que, ao final de um passo de repeti��o, o personagem tenha feito exatamente a rota��o esperada.
	 */
	private byte rotationsLeft;
	
	/** Velocidade de movimenta��o vertical dos passos. */
	private final MUV stepsMUV = new MUV( );
	
	/** Limite do movimento vertical do passo atual. */
	private short stepsYLimit;
	
	/** Tempo necess�rio para que o pr�ximo passo chegue � mira. */
	private static final short STEP_CHANGE_TIME = 200;

	/** Tempo necess�rio para que o pr�ximo movimento chegue � mira. */
	public static final short MOVE_CHANGE_TIME = 333;
	
	private static short STEP_ROTATION_SPEED;
	
	/** Painel que cont�m o grupo de movimentos e as bordas. */
	public final DrawableGroup movesPanel;
	
	public final Pattern movesPanelAim;
	private final Pattern movesPanelTop;
	private final Pattern movesPanelBottom;
	private final DrawableImage movesPanelRight;
	private final Pattern movesPanelFill;
	public final DrawableImage arrowRight;
	
	private static final int MOVE_AIM_MIN_GREEN = 0x00;
	private static final int MOVE_AIM_MAX_GREEN = 0xff;
	
	private int moveAimColor = 0xff0000;
	
	private final MUV moveAimColorSpeed;
	
	/** Grupo dos movimentos do n�vel atual. */
	private DrawableGroup movesGroup;
	
	/** Velocidade de movimenta��o horizontal dos �cones dos movimentos. */
	private final MUV movesMUV = new MUV();
	
	/** Limite de movimenta��o do grupo dos �cones dos movimentos. */
	private short movesXLimit;
	
	/** Painel que cont�m o grupo de passos e as bordas. */
	public final UpdatableGroup stepsPanel;
	
	private final BasicAnimatedPattern stepsPanelAim;
	private final BasicAnimatedPattern stepsPanelAimRed;
	private final Pattern stepsPanelFill;
	private final Pattern stepsPanelLeft;
	private final Pattern stepsPanelRight;
	
	/** Dura��o em milisegundos da visibilidade da mira vermelha, ao errar um passo. */
	private static final short STEPS_AIM_RED_VISIBLE_TIME = 300;
	
	private short stepsAimRedAccTime;
	
	/** Grupo dos passos do movimento atual. */
	private StepsGroup stepsGroup;
	
	/** �ndice do grupo de movimentos no seu cont�iner. */
	private final byte MOVES_GROUP_INDEX;
	
	/** �ndice do grupo de passos no seu cont�iner. */
	private static final byte STEPS_GROUP_INDEX = 3;
	
	/** Tempo em milisegundos para ser feita a anima��o de entrada/sa�da dos grupos de passos e movimentos. */
	public static final short TIME_APPEAR = 500;
	
	/** Tempo acumulado da anima��o de entrada/sa�da. */
	private short accTime;
	
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;
	
	private byte state;
	
	private final boolean demoMode;
	
	/** Intervalo em milisegundos entre as teclas pressionadas automaticamente. */
	private static final short DEMO_MODE_KEY_INTERVAL = 901;
	
	/** Intervalo em milisegundos ap�s o fim dos passos para que eles sejam repetidos. */
	private static final short DEMO_MODE_KEY_LAST_STEP_WAIT = 2000;
	
	/** Tempo restante at� a pr�xima tecla pressionada automaticamente (modo demo). */
	private short timeToNextKeyPress;
	
	private Drawable scoreBar;
	private Drawable jumpBar;
	private Pattern moveBar;
	private Drawable meter;
	private Label label;
	public final DrawableImage arrowUp;
	public final DrawableImage arrowDown;
	public final DrawableImage arrowLeft;
	
	private static final short ARROW_BLINK_TIME = 600;
	
	private short arrowAccTime;
	
	private boolean arrowsVisible;	
	
	private static final short LABEL_BLINK_TIME = 200;
	private short labelAccTime;
	
	private int fp_jumpScore;
	private int fp_ScoreRequired;
	
	private final Mutex mutex = new Mutex();
	
	private final Boca boca;
	
	
	public MoveQueue( boolean demoMode, Boca boca ) throws Exception {
		super( MOVES_MAX + 2 );

		this.demoMode = demoMode;
		this.boca = boca;
		
		arrowRight = demoMode ? new DrawableImage( imgArrowRight ) : null;
		final DrawableImage moveBorderLeft = new DrawableImage( imgMoveBorderLeft );
		final DrawableImage knot = new DrawableImage( imgKnot );
		final DrawableImage bar = new DrawableImage( imgBar );
		final DrawableImage barH = new DrawableImage( imgBarH );
		final DrawableImage fill = new DrawableImage( imgFill );
		arrowUp = demoMode ? new DrawableImage( imgArrowUp ) : null;
		DrawableImage temp;
		
		// <editor-fold desc="montagem do painel dos movimentos" defaultstate="collapsed">
		
		movesPanel = new DrawableGroup( 15 );
		
		movesPanelFill = new Pattern( new DrawableImage( fill ) );
		movesPanel.insertDrawable( movesPanelFill );
		
		final DrawableImage borderLeft = new DrawableImage( moveBorderLeft );
		if ( demoMode )
			borderLeft.setPosition( arrowRight.getWidth() - 1, arrowUp.getHeight() - 1 );
		else
			borderLeft.setPosition( 1, 1 );
		movesPanel.insertDrawable( borderLeft );
		
		
		temp = new DrawableImage( barH );
		temp.defineReferencePixel( 0, 0 );
		movesPanelTop = new Pattern( temp );
		movesPanelTop.setSize( 0, temp.getHeight() );
		movesPanelTop.setPosition( borderLeft.getPosX() + borderLeft.getWidth(), borderLeft.getPosY() );
		movesPanel.insertDrawable( movesPanelTop );
		
		movesPanelFill.setPosition( borderLeft.getPosX() + ( borderLeft.getWidth() >> 1 ), movesPanelTop.getPosY() + movesPanelTop.getHeight() );
		
		temp = new DrawableImage( barH );
		temp.defineReferencePixel( 0, 0 );
		movesPanelBottom = new Pattern( temp );
		movesPanelBottom.setSize( 0, temp.getHeight() );
		movesPanelBottom.setPosition( movesPanelTop.getPosX(), borderLeft.getPosY() + borderLeft.getHeight() - movesPanelBottom.getHeight() );
		movesPanel.insertDrawable( movesPanelBottom );
		
		movesPanelRight = new DrawableImage( moveBorderLeft );
		movesPanelRight.setTransform( TRANS_MIRROR_H );
		movesPanel.insertDrawable( movesPanelRight );
		
		movesPanel.setSize( 0, movesPanelBottom.getPosY() + movesPanelBottom.getHeight() );
		
		insertDrawable( movesPanel );
		
		// </editor-fold>
		
		
		// <editor-fold desc="montagem do painel dos passos" defaultstate="collapsed">
		
		stepsPanel = new UpdatableGroup( 15 );
		
		temp = new DrawableImage( barH );
		temp.defineReferencePixel( 0, 0 );
		final Pattern stepsPanelTop = new Pattern( temp );
		stepsPanelTop.setPosition( movesPanelTop.getPosX(), 1 );
		
		stepsPanelAim = new BasicAnimatedPattern( new DrawableImage( PATH_IMAGES + "aim.png" ), -40, 0 );
		stepsPanelAim.setSize( STEP_AIM_WIDTH, stepsPanelAim.getFill().getHeight() );
		stepsPanelAim.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		temp = new DrawableImage( bar );
		temp.setTransform( TRANS_MIRROR_H );
		temp.defineReferencePixel( 0, 0 );
		stepsPanelLeft = new Pattern( temp );
		stepsPanelLeft.setSize( temp.getWidth(), 0 );
		
		stepsPanelTop.setSize( stepsPanelAim.getWidth(), stepsPanelTop.getFill().getHeight() );
		
		stepsPanelLeft.setPosition( borderLeft.getPosX(), knot.getHeight() );

		stepsPanelAim.setPosition( stepsPanelLeft.getPosX() + stepsPanelLeft.getWidth(), stepsPanelTop.getPosY() + stepsPanelTop.getHeight() );
		stepsPanel.insertDrawable( stepsPanelAim );
		
		if ( demoMode ) {
			stepsPanelAimRed = null;
		} else {
			stepsPanelAimRed = new BasicAnimatedPattern( new DrawableImage( PATH_IMAGES + "aim_red.png" ), -40, 0 );
			stepsPanelAimRed.setSize( STEP_AIM_WIDTH, stepsPanelAimRed.getFill().getHeight() );
			stepsPanelAimRed.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
			stepsPanelAimRed.setPosition( stepsPanelAim.getPosition() );
			
			stepsPanelAimRed.setVisible( false );
			stepsPanel.insertDrawable( stepsPanelAimRed );
		}
		
		stepsPanel.insertDrawable( stepsPanelLeft );
		stepsPanel.insertDrawable( stepsPanelTop );
		
		temp = new DrawableImage( bar );
		stepsPanelRight = new Pattern( temp );
		stepsPanelRight.setPosition( stepsPanelAim.getPosX() + stepsPanelAim.getWidth(), knot.getHeight() );
		stepsPanelRight.setSize( temp.getWidth(), 0 );
		
		stepsPanel.insertDrawable( stepsPanelRight );
		
		// insere os n�s da parte superior da mira
		final int KNOT_POS_X_LEFT = borderLeft.getPosX() - 1;
		final int KNOT_POS_X_RIGHT = stepsPanelAim.getPosX() + stepsPanelAim.getWidth() - 1;
		temp = new DrawableImage( knot );
		temp.setPosition( KNOT_POS_X_LEFT, 0 );
		stepsPanel.insertDrawable( temp );
		
		temp = new DrawableImage( knot );
		temp.setPosition( KNOT_POS_X_RIGHT, 0 );
		stepsPanel.insertDrawable( temp );			
		
		temp = new DrawableImage( barH );
		temp.defineReferencePixel( 0, 0 );
		final Pattern aimBottom = new Pattern( temp );
		aimBottom.setPosition( stepsPanelAim.getPosX(), stepsPanelAim.getPosY() + stepsPanelAim.getHeight() );
		aimBottom.setSize( stepsPanelTop.getSize() );
		stepsPanel.insertDrawable( aimBottom );
		
		// insere os n�s da parte inferior da mira
		temp = new DrawableImage( knot );
		temp.setPosition( KNOT_POS_X_LEFT, aimBottom.getPosY() - 1 );
		stepsPanel.insertDrawable( temp );
		
		temp = new DrawableImage( knot );
		temp.setPosition( KNOT_POS_X_RIGHT, aimBottom.getPosY() - 1 );
		stepsPanel.insertDrawable( temp );
		
		temp = new DrawableImage( fill );
		stepsPanelFill = new Pattern( temp );
		stepsPanelFill.setPosition( stepsPanelLeft.getPosX() + stepsPanelLeft.getWidth(), aimBottom.getPosY() + aimBottom.getHeight() );
		stepsPanel.insertDrawable( stepsPanelFill, 1 );
		
		stepsPanel.setSize( stepsPanelRight.getPosX() + stepsPanelRight.getWidth() + 1, 0 );
		stepsPanel.setPosition( 0, movesPanel.getHeight() - movesPanelBottom.getHeight() - 1 );
		
		insertDrawable( stepsPanel );
		
		// </editor-fold>		
		
		// cria a "mira" dos movimentos
		final Pattern movesPanelAimRight = new Pattern( new DrawableImage( bar ) );
		movesPanelAimRight.setPosition( KNOT_POS_X_RIGHT + 1, movesPanelTop.getPosY() + movesPanelTop.getHeight() );
		movesPanelAimRight.setSize( bar.getWidth(), movesPanelBottom.getPosY() - movesPanelAimRight.getPosY() );
		movesPanel.insertDrawable( movesPanelAimRight  );
		
		temp = new DrawableImage( knot );
		temp.setPosition( KNOT_POS_X_RIGHT, movesPanelTop.getPosY() - 1 );
		movesPanel.insertDrawable( temp );			
		
		temp = new DrawableImage( knot );
		temp.setPosition( KNOT_POS_X_RIGHT, movesPanelBottom.getPosY() - 1 );
		movesPanel.insertDrawable( temp );			
		
		
		// se estiver no modo de demonstra��o, insere as setas no painel de movimentos
		if ( demoMode ) {
			arrowUp.setPosition( stepsPanelAim.getPosX() + ( ( stepsPanelAim.getWidth() - arrowUp.getWidth() ) >> 1 ), 0 );
			movesPanel.insertDrawable( arrowUp );
			
			arrowDown = new DrawableImage( arrowUp );
			arrowDown.setTransform( TRANS_MIRROR_V );
			arrowDown.setPosition( arrowUp.getPosX(), stepsPanelTop.getPosY() + stepsPanelTop.getHeight() - 1 );
			stepsPanel.insertDrawable( arrowDown );

			arrowLeft = new DrawableImage( arrowRight );
			arrowLeft.setTransform( TRANS_MIRROR_H );
			arrowLeft.setPosition( 0, movesPanelTop.getPosY() + ( ( movesPanelRight.getHeight() - arrowLeft.getHeight() ) >> 1 ) );
			movesPanel.insertDrawable( arrowLeft );		
			
			arrowRight.setPosition( 0, arrowLeft.getPosY() );
			
			movesPanel.insertDrawable( arrowRight );
			
			movesPanelAim = new Pattern( 0xff0000 );
			movesPanelAim.setPosition( movesPanelFill.getPosition() );
			movesPanelAim.setSize( movesPanelAimRight.getPosX() - movesPanelAim.getPosX() - movesPanelAim.getWidth(),
								   movesPanelBottom.getPosY() - movesPanelTop.getPosY() - movesPanelTop.getHeight() );
			movesPanel.insertDrawable( movesPanelAim, 0 );
			
			MOVES_GROUP_INDEX = 2;
			moveAimColorSpeed = new MUV( MOVE_AIM_MAX_GREEN - MOVE_AIM_MIN_GREEN );
		} else {
			movesPanelAim = null;
			moveAimColorSpeed = null;
			MOVES_GROUP_INDEX = 1;
			arrowDown = null;
			arrowLeft = null;
		}
		
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final int getBarrigadaScore() {
		return moves[ moves.length - 1 ].getScoreFP();
	}


	public final void prepare() throws Exception {
		mutex.acquire();
		
		if ( !demoMode ) {
			// se o array de movimentos for nulo, gera um automaticamente
			if ( moves == null ) {
				final int TOTAL_MOVES_OK = PlayScreen.getMaxMoves();
				final int TOTAL_MOVES = TOTAL_MOVES_OK - 1;
				final int FP_SCORE_REQUIRED = PlayScreen.getScoreRequired( false );
				int fp_jumpScoreTemp = 0;

				moves = new Move[ TOTAL_MOVES_OK ];
				moves[ TOTAL_MOVES ] = new Move( Move.TYPE_BARRIGADA );
				
				for ( byte i = 0; i < TOTAL_MOVES; ++i ) {
					moves[ i ] = new Move( Move.getRandomType() );
					fp_jumpScoreTemp += moves[ i ].getMaxScoreFP();
				}
				
				// adiciona a pontua��o do movimento da barrigada
				fp_jumpScoreTemp += moves[ TOTAL_MOVES ].getMaxScoreFP();

				// troca o movimento de menor valor por outro maior, at� que seja atingido o m�nimo de pontos do n�vel
				while ( fp_jumpScoreTemp < FP_SCORE_REQUIRED ) {
					// procura o movimento de menor pontua��o
					int lowestIndex = 0;
					for ( byte i = 0; i < TOTAL_MOVES; ++i ) {
						if ( moves[ i ].getMaxScoreFP() < moves[ lowestIndex ].getMaxScoreFP() )
							lowestIndex = i;
					}

					// troca o movimento de menor pontua��o por outro melhor
					final int LOWEST_SCORE = moves[ lowestIndex ].getMaxScoreFP();
					do {
						moves[ lowestIndex ] = new Move( Move.getRandomType() );
					} while ( moves[ lowestIndex ].getMaxScoreFP() <= LOWEST_SCORE );

					fp_jumpScoreTemp += moves[ lowestIndex ].getMaxScoreFP() - LOWEST_SCORE;
				}		
			} // fim if ( moves == null )

			// remove os poss�veis movimentos vazios da lista
			int totalMoves = moves.length;
			for ( int i = 0; i < totalMoves; ) {
				if ( moves[ i ].type == Move.TYPE_EMPTY || moves[ i ].type == Move.TYPE_OK ) {
					--totalMoves;
					for ( int j = i; j < totalMoves; ++j ) {
						moves[ j ] = moves[ j + 1 ];
					}
					moves[ totalMoves ] = null;
				} else {
					++i;
				}
			}
			if ( totalMoves != moves.length ) {
				final Move[] temp = new Move[ totalMoves ];
				System.arraycopy( moves, 0, temp, 0, totalMoves );
				moves = temp;
			}
			
			stepsPanelAimRed.setVisible( false );
			stepsPanelAim.setVisible( true );
		} // fim if ( !demoMode )
		
		currentMove = moves[ 0 ];
		
		if ( stepsPanel.getDrawable( STEPS_GROUP_INDEX ) == stepsGroup ) {
			stepsPanel.removeDrawable( STEPS_GROUP_INDEX );
		}
		stepsGroup = null;
		
		// cria o grupo e adiciona os �cones dos movimentos
		if ( movesGroup != null ) {
			movesGroup = null;	
			movesPanel.removeDrawable( MOVES_GROUP_INDEX );
		}
		movesGroup = new DrawableGroup( moves.length );
		for ( int i = 0; i < moves.length; ++i ) {
			movesGroup.insertDrawable( moves[ i ] );
		}
		movesPanel.insertDrawable( movesGroup, MOVES_GROUP_INDEX );
		movesGroup.setViewport( new Rectangle( getPosX() + movesPanel.getPosX() + movesPanelFill.getPosX(), 
											   0, 
											   movesPanelFill.getWidth(),
											   getPosY() + movesPanel.getHeight() ) );
		
		positionMoveIcons( 0 );

		if ( demoMode )
			timeToNextKeyPress = DEMO_MODE_KEY_INTERVAL;
		
		prepareStepsGroup();
		
		currentMoveIndex = 0;
		currentStepIndex = 0;
		currentStepAbsolutIndex = 0;
		
		
		setStepsYLimit( false );
		
		if ( !demoMode )
			checkStepKeyToHold();
		
		boca.reset();
		
		movesGroup.setPosition( movesPanel.getWidth(), movesGroup.getPosY() );
		setCurrentMoveIndex( 0 );
		
		setState( demoMode ? STATE_SHOWN : STATE_HIDDEN );
		
		mutex.release();
	}
	
	
	public static final void setMoves( Move[] moves ) {
		MoveQueue.moves = moves;
	}
	
	
	private final void prepareStepsGroup() throws Exception {
		// cria o grupo e adiciona os �cones dos passos
		if ( stepsPanel.getDrawable( STEPS_GROUP_INDEX ) == stepsGroup )
			stepsPanel.removeDrawable( STEPS_GROUP_INDEX );
		stepsGroup = null;
			
		if ( demoMode ) {
			stepsGroup = new StepsGroup( currentMove.steps.length );
		} else {
			short totalSteps = 0;
			for ( byte i = 0; i < moves.length; ++i )
				totalSteps += moves[ i ].steps.length;
			
			stepsGroup = new StepsGroup( totalSteps );
		}
		stepsPanel.insertDrawable( stepsGroup, STEPS_GROUP_INDEX );
		
		stepsGroup.setSize( stepImageLargestWidth, 0 );
		
		int y = 0;
		Drawable icon = null;
		final short AIM_SPACING = ( short ) ( stepsPanelAim.getHeight() >> 2 );
		for ( int i = demoMode ? currentMoveIndex : 0; i < ( demoMode ? currentMoveIndex + 1 : moves.length ); ++i, y += IMG_STEPS[ 0 ].getHeight() ) {
			//#if DEBUG == "true"
			System.out.print( "moves[ " + i + "]: " );
			//#endif
			
			final Move tempMove = moves[ i ];
			for ( byte j = 0; j < tempMove.steps.length; ++j ) {
				//#if DEBUG == "true"
				System.out.print( tempMove.steps[ j ] + ", " );
				//#endif
				
				switch ( tempMove.steps[ j ] ) {
					case STEP_LEFT_90:
					case STEP_LEFT_180:
					case STEP_LEFT_360:
					case STEP_LEFT_720:
						icon = loadRepeatGroup( STEP_LEFT_REPEAT_IMAGE_INDEX, imgRepeatLeft, imgRepeatLeftBottom, tempMove.steps[ j ] );
					break;
					
					case STEP_RIGHT_90:
					case STEP_RIGHT_180:
					case STEP_RIGHT_360:
					case STEP_RIGHT_720:
						icon = loadRepeatGroup( STEP_RIGHT_REPEAT_IMAGE_INDEX, imgRepeatRight, imgRepeatRightBottom, tempMove.steps[ j ] );
					break;
					
					default:
						icon = new DrawableImage( IMG_STEPS[ tempMove.steps[ j ] ] );
					// fim default
				} // fim switch ( moves[ i ].index )
				
				icon.setPosition( ( stepImageLargestWidth - icon.getWidth() ) >> 1, y );
				stepsGroup.insertDrawable( icon );
				y += icon.getHeight() + AIM_SPACING;			
			}
			
			//#if DEBUG == "true"
			System.out.println();
			//#endif			
		}
		stepsGroup.setSize( stepImageLargestWidth, y );
		stepsGroup.setPosition( stepsPanelFill.getPosX() + ( ( stepsPanelFill.getWidth() - stepsGroup.getWidth() ) >> 1 ), stepsPanel.getHeight() );
		stepsGroup.setViewport( new Rectangle( 0, getPosY() + stepsPanel.getPosY() + stepsPanelAim.getPosY(), stepsPanel.getWidth(), stepsPanel.getHeight() ) );
	} // fim do m�todo prepareStepsGroup()
	
	
	private final DrawableGroup loadRepeatGroup( int imgIndex, DrawableImage imgFill, DrawableImage imgBottom, byte stepIndex ) throws Exception {
		final DrawableGroup group = new DrawableGroup( 3 );
		final DrawableImage step = new DrawableImage( IMG_STEPS[ imgIndex ] );
		final Pattern repeat = new Pattern( new DrawableImage( imgFill ) );
		final DrawableImage end = new DrawableImage( imgBottom );
		
		int rotations = 0;
		switch ( stepIndex ) {
			case STEP_LEFT_90:
			case STEP_RIGHT_90:
				rotations = 1;
			break;

			case STEP_LEFT_180:
			case STEP_RIGHT_180:
				rotations = 2;
			break;
			
			case STEP_LEFT_360:
			case STEP_RIGHT_360:
				rotations = 4;
			break;
			
			case STEP_RIGHT_720:
			case STEP_LEFT_720:
				rotations = 8;
			break;
		}
		
		// altura interna de cada passo de rota��o (no caso de uma rota��o de 360�) - a divis�o por 8 refere-se � divis�o de 360 por 45
		final int REPEAT_TOTAL_HEIGHT = imgFill.getHeight() * rotations;
		
		repeat.setSize( step.getWidth(), REPEAT_TOTAL_HEIGHT );
		repeat.setPosition( 0, step.getHeight() );
		end.setPosition( 0, repeat.getPosY() + repeat.getHeight() );
		group.insertDrawable( step );
		group.insertDrawable( repeat );
		group.insertDrawable( end );
		
		group.setSize( repeat.getWidth(), end.getPosY() + end.getHeight() );
		
		return group;
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM2:
			case ScreenManager.UP:
				checkStep( STEP_UP );
			break;
			
			case ScreenManager.KEY_NUM8:
			case ScreenManager.DOWN:
				checkStep( STEP_DOWN );
			break;
			
			case ScreenManager.KEY_NUM4:
			case ScreenManager.LEFT:
				key = ScreenManager.LEFT;
				
				if ( hasMoreSteps() ) { 
					if ( isCurrentStepOnTarget() ) {
						switch ( keyToHold ) {
							case ScreenManager.LEFT:
							case ScreenManager.RIGHT:
								if ( key == keyToHold ) {
									rotateUpdated = false;
									
									setStepsYLimit( true );
									stepsMUV.setSpeed( STEP_ROTATION_SPEED, true );
								} else {
									// errou um passo
									currentMove.checkStep( currentMove.steps[ currentStepIndex ], false );
									stepError();
									
									// p�ra o movimento de subida do �cone atual 
									stepsMUV.setSpeed( 0, true );
									return;
								}
								
							default:
								boca.setRotationDirection( Boca.ROTATION_COUNTER_CLOCKWISE );
						} // fim switch ( keyToHold )
					}
				} else {
					rotateUpdated = false;
					
					boca.setRotationDirection( Boca.ROTATION_COUNTER_CLOCKWISE );
				}
			break;
			
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				checkStep( STEP_FIRE );
			break;
			
			case ScreenManager.KEY_NUM6:
			case ScreenManager.RIGHT:
				key = ScreenManager.RIGHT;
				
				if ( hasMoreSteps() ) { 
					if ( isCurrentStepOnTarget() ) {
						switch ( keyToHold ) {
							case ScreenManager.LEFT:
							case ScreenManager.RIGHT:
								if ( key == keyToHold ) {
									rotateUpdated = false;
									
									setStepsYLimit( true );
									stepsMUV.setSpeed( STEP_ROTATION_SPEED, true );
								} else {
									// errou um passo
									currentMove.checkStep( currentMove.steps[ currentStepIndex ], false );
									stepError();
									
									// p�ra o movimento de subida do �cone atual
									stepsMUV.setSpeed( 0, true );
									return;
								}
								
							default:
								rotateUpdated = false;
								boca.setRotationDirection( Boca.ROTATION_CLOCKWISE );
						} // fim switch ( keyToHold )
					}
				} else {
					boca.setRotationDirection( Boca.ROTATION_CLOCKWISE );
				}	
			break;
			
			default:
				checkStep( STEP_DUMMY );
		}
		
		keyHeld = key;
	}
	
	
	private final boolean checkStep( byte stepType ) {
		if ( hasMoreSteps() && currentMove.checkStep( stepType, true ) ) {
			boca.setStepsMade( currentMove.getSingleStepsMade() );
			nextStep();
			return true;
		}

		stepError();
		
		return false;
	}
	
	
	private final void stepError() {
		// errou um passo
		MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
		stepsAimRedAccTime = STEPS_AIM_RED_VISIBLE_TIME;
		stepsPanelAimRed.setVisible( true );
		stepsPanelAim.setVisible( false );		
	}
	
	
	private final boolean hasMoreSteps() {
		return currentMoveIndex < moves.length && currentStepIndex < currentMove.steps.length;
	}
	
	
	/**
	 * M�todo chamado quando um passo � conclu�do, para preparar a movimenta��o dos �cones e fazer o gerenciamento
	 * dos passos e movimentos.
	 */
	private final void nextStep() {
		keyReleased( keyHeld );
		
		setStepsYLimit( true );
		stepsMUV.setSpeed( ( stepsYLimit - stepsGroup.getPosY() ) * 1000 / STEP_CHANGE_TIME );
		
		++currentStepIndex;
		++currentStepAbsolutIndex;
		if ( currentStepIndex <= 2 ) {
			// verifica se o Bo�a est� na sequ�ncia correta
			if ( boca.getSequence() != currentMove.getSequence() )
				boca.setSequence( currentMove.getSequence() );
		} else if ( currentStepIndex >= currentMove.steps.length ) {
			// terminou o movimento atual
			if ( demoMode ) {
				currentMove.reset();
				setCurrentMoveIndex( currentMoveIndex );
			} else {
				// verifica se ainda h� movimentos a executar. Ao finalizar o �ltimo, esconde o gerenciador.
				if ( currentMoveIndex + 1 < moves.length )
					setCurrentMoveIndex( currentMoveIndex + 1 );
				else {
					setState( STATE_HIDING );
					
					// retorna para n�o efetuar o teste da tecla a ser segura
					return;
				}
			}
		}
		
		if ( currentMoveIndex < moves.length )
			checkStepKeyToHold();
	}
	
	
	public final void changeCurrentMoveType( int diff ) {
		if ( currentMoveIndex < moves.length - 2 ) {
			try {
				final Move previousMove = currentMove;
				final Move newMove = new Move( Move.getIndex( previousMove.type, diff ) );
				moves[ currentMoveIndex ] = newMove;
				currentMove = newMove;
				newMove.setPosition( previousMove.getPosition() );
				newMove.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
				movesGroup.setDrawable( currentMove, currentMoveIndex );

				prepareStepsGroup();

				setCurrentMoveIndex( currentMoveIndex );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
		}
	}	


	public final void keyReleased( int key ) {
		boca.setRotationDirection( Boca.ROTATION_NONE );
		if ( keyHeld == keyToHold ) {
			if ( isCurrentStepOnTarget() ) {
				if ( rotationsLeft > 0 ) {
					stepsMUV.setSpeed( 0, true );
				} else {
					// se j� tiver feito o n�mero de rota��es esperado, avan�a para o pr�ximo passo
					return;
				}
			}
		}
		keyHeld = 0;
	}
	

	/**
	 * Indica se o passo atual est� na mira, ou se a anima��o de transi��o ainda est� sendo realizada.
	 * @return
	 */
	private final boolean isCurrentStepOnTarget() {
		return stepsGroup.getDrawable( currentStepAbsolutIndex ).getPosY() + stepsGroup.getPosY() <= stepsPanelAim.getPosY() + stepsPanelAim.getHeight();
	}
	
	
	private final void checkStepKeyToHold() {
		if ( currentMove.type != Move.TYPE_EMPTY && currentMove.type != Move.TYPE_OK ) {
			switch ( currentMove.steps[ currentStepIndex ] ) {
				case STEP_LEFT_90:
				case STEP_LEFT_180:
				case STEP_LEFT_360:
				case STEP_LEFT_720:
					rotationsLeft = ( byte ) ( 1 << ( currentMove.steps[ currentStepIndex ] - STEP_LEFT_90 ) );
					STEP_ROTATION_SPEED = ( short ) ( -stepsGroup.getDrawable( currentStepAbsolutIndex ).getHeight() * 910 / ( ROTATION_90_TIME * rotationsLeft ) );
					keyToHold = ScreenManager.LEFT;
				break;

				case STEP_RIGHT_90:
				case STEP_RIGHT_180:
				case STEP_RIGHT_360:
				case STEP_RIGHT_720:
					rotationsLeft = ( byte ) ( 1 << ( currentMove.steps[ currentStepIndex ] - STEP_RIGHT_90 ) );
					STEP_ROTATION_SPEED = ( short ) ( -stepsGroup.getDrawable( currentStepAbsolutIndex ).getHeight() * 910 / ( ROTATION_90_TIME * rotationsLeft ) );
					keyToHold = ScreenManager.RIGHT;
				break;

				default:
					keyToHold = KEY_DUMMY;
			}		
			timeToNextKeyPress = DEMO_MODE_KEY_INTERVAL;
		}
	}
	
	
	private final void setStepsYLimit( boolean aboveTarget ) {
		if ( currentMove.type != Move.TYPE_EMPTY && currentMove.type != Move.TYPE_OK ) {
			final Drawable currentStep = stepsGroup.getDrawable( currentStepAbsolutIndex );
			if ( aboveTarget ) {
				if ( currentStepAbsolutIndex < stepsGroup.getTotalSlots() - 1 ) {
					// anda h� passos a executar - posiciona o pr�ximo passo

					switch ( currentMove.steps[ currentStepIndex ] ) {
						case STEP_LEFT_90:
						case STEP_LEFT_180:
						case STEP_LEFT_360:
						case STEP_LEFT_720:
						case STEP_RIGHT_90:
						case STEP_RIGHT_180:
						case STEP_RIGHT_360:
						case STEP_RIGHT_720:
							final int temp = -currentStep.getPosY() - currentStep.getHeight() + stepsPanelAim.getPosY() +
									( ( stepsPanelAim.getHeight() - IMG_STEPS[ STEP_LEFT_IMAGE_INDEX ].getHeight() ) >> 1 );
							if ( stepsGroup.getPosY() > temp ) {
								stepsYLimit = ( short ) temp;
								break;
							}

						default:
							// verifica o tipo do pr�ximo passo para posicion�-lo
							final Drawable nextStep = stepsGroup.getDrawable( currentStepAbsolutIndex + 1 );
							final int nextStepIndex = currentStepIndex + 1 >= currentMove.steps.length ? 0 : currentStepIndex + 1;
							final int nextMoveIndex = nextStepIndex == 0 ? currentMoveIndex + 1 : currentMoveIndex;
							
							switch ( moves[ nextMoveIndex ].steps[ nextStepIndex ] ) {
								case STEP_LEFT_90:
								case STEP_LEFT_180:
								case STEP_LEFT_360:
								case STEP_LEFT_720:
								case STEP_RIGHT_90:
								case STEP_RIGHT_180:
								case STEP_RIGHT_360:
								case STEP_RIGHT_720:
									stepsYLimit = ( short ) ( -nextStep.getPosY() + stepsPanelAim.getPosY() + ( ( stepsPanelAim.getHeight() - IMG_STEPS[ STEP_LEFT_IMAGE_INDEX ].getHeight() ) >> 1 ) );
								break;
								
								default:
									stepsYLimit = ( short ) ( -nextStep.getPosY() + stepsPanelAim.getPosY() + ( ( stepsPanelAim.getHeight() - nextStep.getHeight() ) >> 1 ) );
							}
						// fim default
					}		
				} else {
					// � o �ltimo passo
					stepsYLimit = ( short ) ( -currentStep.getPosY() - currentStep.getHeight() );		
				}
			} else {
				// posiciona o passo atual na mira
				switch ( currentMove.steps[ currentStepIndex ] ) {
					case STEP_LEFT_90:
					case STEP_LEFT_180:
					case STEP_LEFT_360:
					case STEP_LEFT_720:
					case STEP_RIGHT_90:
					case STEP_RIGHT_180:
					case STEP_RIGHT_360:
					case STEP_RIGHT_720:
						stepsYLimit = ( short ) ( stepsPanelAim.getPosY() - currentStep.getPosY() + ( ( stepsPanelAim.getHeight() - IMG_STEPS[ STEP_LEFT_IMAGE_INDEX ].getHeight() ) >> 1 ) );		
					break;
					
					default:
						stepsYLimit = ( short ) ( stepsPanelAim.getPosY() - currentStep.getPosY() + ( ( stepsPanelAim.getHeight() - currentStep.getHeight() ) >> 1 ) );		
				}
				// define a velocidade do primeiro passo para fazer a anima��o de entrada
				if ( currentStepAbsolutIndex == 0 )
					stepsMUV.setSpeed( ( stepsYLimit - stepsGroup.getPosY() ) * 1000 / STEP_CHANGE_TIME );
			}
		}
	} // fim do m�todo setStepsYLimit( boolean )
	
	
	/**
	 * Posiciona os �cones de movimentos internamente no grupo de movimentos.
	 * @param startIndex primeiro �cone de movimento a ser posicionado.
	 */
	private final void positionMoveIcons( int startIndex ) {
		final Move firstMove = moves[ startIndex ];
		
		final short MOVE_ICON_SPACING = ( short ) ( stepsPanelAim.getWidth() >> 2 );
		int x = firstMove.getPosX() + firstMove.getWidth() + MOVE_ICON_SPACING;
		for ( int i = startIndex + 1; i < moves.length; ++i ) {
			final Move move = moves[ i ];
			move.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
			move.setPosition( x, 0 );
			x += move.getWidth() + MOVE_ICON_SPACING;
		}
		movesGroup.setSize( x, moves[ 0 ].getHeight() );
		movesGroup.setPosition( movesGroup.getPosX(), movesPanelRight.getPosY() + ( ( movesPanelRight.getHeight() - movesGroup.getHeight() ) >> 1 ) );
	}


	public final void update( int delta ) {
		mutex.acquire();
		
		super.update( delta );
		
		// se estiver no modo de demonstra��o, realiza os passos automaticamente
		if ( demoMode ) {
			moveAimColor += moveAimColorSpeed.updateInt( delta );
			if ( moveAimColor < MOVE_AIM_MIN_GREEN || moveAimColor > MOVE_AIM_MAX_GREEN ) {
				moveAimColor = NanoMath.clamp( moveAimColor, MOVE_AIM_MIN_GREEN, MOVE_AIM_MAX_GREEN    );
				
				moveAimColorSpeed.setSpeed( -moveAimColorSpeed.getSpeed() );
			}
			movesPanelAim.setFillColor( 0xff0000 | ( moveAimColor << 8 ) );
			if ( moveBar != null )
				moveBar.setFillColor( movesPanelAim.getFillColor() );
			
			if ( currentMove.type != Move.TYPE_EMPTY && currentMove.type != Move.TYPE_OK && timeToNextKeyPress > 0 ) {
				timeToNextKeyPress -= delta;

				if ( timeToNextKeyPress <= 0 ) {
					if ( currentMoveIndex < moves.length ) {
						if ( keyToHold == KEY_DUMMY ) {
							checkStep( currentMove.steps[ currentStepIndex ] );
							timeToNextKeyPress = DEMO_MODE_KEY_INTERVAL;
						} else {
							keyPressed( keyToHold );
						}
					}
				} // fim if ( timeToNextKeyPress <= 0 )
			}
			
			// faz o label indicador de informa��o da pontua��o piscar, caso esteja abaixo do n�vel minimo
			if ( label != null && labelAccTime > 0 ) {
				labelAccTime -= delta;
				
				if ( labelAccTime <= 0 ) {
					labelAccTime = LABEL_BLINK_TIME;
					label.setVisible( !label.isVisible() );
				}
			}
			
			arrowAccTime += delta;
			if ( arrowAccTime >= ARROW_BLINK_TIME ) {
				arrowAccTime %= ARROW_BLINK_TIME;

				arrowsVisible = !arrowsVisible;
				arrowLeft.setVisible( arrowsVisible );
				arrowRight.setVisible( arrowsVisible );

				if ( currentMoveIndex < moves.length - 2 ) {
					arrowUp.setVisible( arrowsVisible );
					arrowDown.setVisible( arrowsVisible );
				}
			} // fim if ( arrowAccTime >= ARROW_BLINK_TIME )			
		} else {
			if ( stepsAimRedAccTime > 0 ) {
				stepsAimRedAccTime -= delta;
				
				if ( stepsAimRedAccTime <= 0 ) {
					stepsAimRedAccTime = 0;
					stepsPanelAimRed.setVisible( false );
					stepsPanelAim.setVisible( true );
				}
			}
		}

		// atualiza a rota��o do Bo�a
		int rotateDelta = delta;
		switch ( keyHeld ) {
			default:
				if ( !rotateUpdated ) {
					keyHeld = keyToHold;
					rotateDelta = NanoMath.min( delta, ROTATE_DELTA_MIN );
				}
				
			// sem break
				
			case ScreenManager.LEFT:
			case ScreenManager.RIGHT:
				if ( boca.updateRotation( rotateDelta ) && rotationsLeft > 0 && isCurrentStepOnTarget() ) {
					--rotationsLeft;
					
					// p�ra a rota��o do Bo�a quando ele completa o movimento exigido
					if ( rotationsLeft <= 0 )
						boca.setRotationDirection( Boca.ROTATION_NONE );
				}
			break;
		}
		rotateUpdated = true;

		if ( stepsGroup != null && movesGroup != null ) {
			// atualiza a anima��o de entrada/sa�da do gerenciador de movimentos
			switch ( state ) {
				case STATE_APPEARING:
					accTime += delta;
					if ( accTime < TIME_APPEAR )
						updatePanelsPosition();
					else
						setState( STATE_SHOWN );
				break;
				
				case STATE_HIDING:
					accTime -= delta;
					if ( accTime > 0 )
						updatePanelsPosition();
					else
						setState( STATE_HIDDEN );
				break;
				
				case STATE_HIDDEN:
					mutex.release();
					return;
			} // fim switch ( state )
			
			if ( stepsMUV.getSpeed() != 0 ) {
				stepsGroup.move( 0, stepsMUV.updateInt( rotateDelta ) );
				
				final int diff = stepsGroup.getPosY() - stepsYLimit;
				if ( diff <= 0 || ( rotationsLeft <= 0 && diff <= 6 ) ) {
					// o passo atual foi feito
					stepsGroup.setPosition( stepsGroup.getPosX(), stepsYLimit );
					stepsMUV.setSpeed( 0, true );

					if ( currentMoveIndex >= moves.length ) {
						if ( timeToNextKeyPress <= 0 )
							timeToNextKeyPress = DEMO_MODE_KEY_LAST_STEP_WAIT;
					} else {
						if ( keyToHold != KEY_DUMMY && rotationsLeft <= 0 ) {
							// atualiza o contador de passos do movimento atual
							currentMove.checkStep( currentMove.steps[ currentStepIndex ], true );
							nextStep();
						}
					}
				}
				
				// verifica quais os �cones de passos vis�veis
				short i = 0;
				if ( currentStepAbsolutIndex < stepsGroup.getUsedSlots() ) {
					final int VISIBLE_Y_LIMIT_TOP = - stepsGroup.getPosY();
					for ( i = currentStepAbsolutIndex; i > 0; --i ) {
						if ( stepsGroup.getDrawable( i ).getPosY() + stepsGroup.getDrawable( i ).getHeight() <= VISIBLE_Y_LIMIT_TOP ) {
							break;
						}
					}
					stepsGroup.firstIndex = i;
				}
				
				final int VISIBLE_Y_LIMIT_BOTTOM = stepsPanel.getHeight() - stepsGroup.getPosY();
				for ( i = currentStepAbsolutIndex; i < stepsGroup.getUsedSlots(); ++i ) {
					if ( stepsGroup.getDrawable( i ).getPosY() >= VISIBLE_Y_LIMIT_BOTTOM ) {
						break;
					}
				}
				stepsGroup.lastIndex = ( short ) NanoMath.min( i + 1, stepsGroup.getUsedSlots() );
			} // fim if ( stepsMUV.getSpeed() != 0 )
			
			// atualiza a movimenta��o do grupo de movimentos
			if ( movesMUV.getSpeed() != 0 ) {
				movesGroup.move( movesMUV.updateInt( delta ), 0 );
				
				if ( ( movesMUV.getSpeed() < 0 && movesGroup.getPosX() <= movesXLimit ) || ( movesMUV.getSpeed() > 0 && movesGroup.getPosX() >= movesXLimit ) ) {
					movesGroup.setPosition( movesXLimit, movesGroup.getPosY() );
					movesMUV.setSpeed( 0 );

					if ( !demoMode ) {
						for ( int i = currentMoveIndex - 1; i >= 0; --i ) {
							if ( movesGroup.getDrawable( i ).isVisible() )
								break;

							movesGroup.getDrawable( i ).setVisible( false );
						}
					}
				}
			} // fim if ( movesMUV.getSpeed() != 0 )
		} // fim if ( stepsGroup != null && movesGroup != null )
		
		mutex.release();
	} // fim do m�todo update( int )
	
	
	public final void changeMoveIndex( int diff ) {
		setCurrentMoveIndex( ( currentMoveIndex + moves.length + diff ) % moves.length );
		
		if ( !isJumpScoreAbove() && currentMoveIndex == moves.length - 1 )
			changeMoveIndex( diff );
	}
	
	
	/**
	 * Posiciona o cursor do menu de movimentos.
	 * @param index
	 */
	public final void setCurrentMoveIndex( int index ) {
		currentMoveIndex = ( byte ) index;
		moves[ currentMoveIndex ].reset();
		currentStepIndex = 0;
		
		currentMove = moves[ currentMoveIndex ];
		
		// no modo de demonstra��o, somente os passos do movimento atual s�o exibidos
		if ( demoMode ) {
			boca.reset();
			
			currentStepAbsolutIndex = 0;
			try {
				prepareStepsGroup();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}
		}
		
		setStepsYLimit( false );
		
		positionMoveIcons( currentMoveIndex );
		
		boca.setVisible( currentMove.type != Move.TYPE_EMPTY && currentMove.type != Move.TYPE_OK );
		boca.setSequence( currentMove.getSequence() );

		movesXLimit = ( short ) ( stepsPanelAim.getPosX() - currentMove.getPosX() + ( ( stepsPanelAim.getWidth() - currentMove.getWidth() ) >> 1 ) );
		movesMUV.setSpeed( ( movesXLimit - movesGroup.getPosX() ) * 1000 / MOVE_CHANGE_TIME );
		
		checkStepKeyToHold();
		
		if ( demoMode ) {
			fillBars();
			
			arrowUp.setVisible( currentMoveIndex < moves.length - 2 );
			arrowDown.setVisible( arrowUp.isVisible() );		
		}
	} // fim do m�todo setCurrentMove( int )
	
	
	private final void updatePanelsPosition() {
		movesPanel.setPosition( movesPanel.getPosX(), -movesPanel.getHeight() + ( accTime * movesPanel.getHeight() / TIME_APPEAR ) );
		stepsPanel.setPosition( -stepsPanel.getWidth() + ( accTime * stepsPanel.getWidth() / TIME_APPEAR ), stepsPanel.getPosY() );		
	}
	
	
	public final void setState( int state) { 
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
			break;
			
			case STATE_APPEARING:
				switch ( this.state ) {
					case STATE_APPEARING:
					case STATE_SHOWN:
					return;
				}
				
				accTime = 0;
				setVisible( true );
				movesPanel.setPosition( 0, -movesPanel.getHeight() );
				stepsPanel.setPosition( -stepsPanel.getWidth(), stepsPanel.getPosY() );
			break;
			
			case STATE_SHOWN:
				setVisible( true );
				movesPanel.setPosition( 0, 0 );
				stepsPanel.setPosition( 0, stepsPanel.getPosY() );
			break;
			
			case STATE_HIDING:
				switch ( this.state ) {
					case STATE_HIDDEN:
					case STATE_HIDING:
					return;
				}
				
				accTime = TIME_APPEAR;
				setVisible( true );
			break;
		}
		
		this.state = ( byte ) state;
	}
	
	
	public static final void loadImages() throws Exception {
		IMG_STEPS = new DrawableImage[ TOTAL_STEPS ];
		for ( byte i = 0; i < TOTAL_STEPS; ++i ) {
			IMG_STEPS[ i ] = new DrawableImage( PATH_STEPS + i + ".png" );
			
			if ( IMG_STEPS[ i ].getWidth() > stepImageLargestWidth )
				stepImageLargestWidth = ( short ) IMG_STEPS[ i ].getWidth();
			
			Thread.yield();
		}
		
		imgRepeatLeft = new DrawableImage( PATH_STEPS + "l.png" );
		imgRepeatRight = new DrawableImage( PATH_STEPS + "r.png" );
		Thread.yield();
		imgRepeatLeftBottom = new DrawableImage( PATH_STEPS + "l_bottom.png" );
		imgRepeatRightBottom = new DrawableImage( PATH_STEPS + "r_bottom.png" );
		Thread.yield();
		
		imgArrowRight = new DrawableImage( PATH_MOVES + "arrow_right.png" );
		imgMoveBorderLeft = new DrawableImage( PATH_IMAGES + "border_left.png" );
		imgKnot = new DrawableImage( PATH_IMAGES + "knot.png" );
		imgBar = new DrawableImage( PATH_IMAGES + "bar.png" );
		Thread.yield();

		imgBarH = new DrawableImage( PATH_IMAGES + "bar_h.png" );
		imgFill = new DrawableImage( PATH_SPLASH + "transparency.png" );
		imgArrowUp = new DrawableImage( PATH_MOVES + "arrow_up.png" );	
		Thread.yield();
	}
	

	public final short getStepImageLargestWidth() {
		return stepImageLargestWidth;
	}
	
	
	/**
	 * Preenche os medidores de pontos do movimento atual e o total do pulo.
	 */
	private final void fillBars() {
		if ( scoreBar != null ) {
			final int FP_TOTAL_WIDTH = NanoMath.toFixed( scoreBar.getWidth() );
			final int FP_MAX_SCORE = NanoMath.mulFixed( Move.getHighestScore(), NanoMath.toFixed( moves.length - 2 ) ) + Move.getBarrigadaMaxScoreFP();
			int fp_jumpScoreTemp = 0;
			int fp_moveStart = 0;
			final int fp_moveScore = currentMove.getMaxScoreFP();

			final byte limit = ( byte ) NanoMath.min( currentMoveIndex, moves.length - 1 );
			for ( byte i = 0; i < limit; ++i ) {
				fp_jumpScoreTemp += moves[ i ].getMaxScoreFP();
			}

			fp_moveStart = fp_jumpScoreTemp;

			for ( int i = limit; i < moves.length - 1; ++i ) {
				fp_jumpScoreTemp += moves[ i ].getMaxScoreFP();
			}
			
			fp_jumpScore = fp_jumpScoreTemp;

			jumpBar.setSize( NanoMath.max( 2, NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_jumpScoreTemp, FP_MAX_SCORE ), FP_TOTAL_WIDTH ) ) ), jumpBar.getHeight() );

			if ( currentMoveIndex >= moves.length - 1 ) {
				moveBar.setSize( 0, moveBar.getHeight() );
			} else {
				moveBar.setPosition( jumpBar.getPosX() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_moveStart, FP_MAX_SCORE ), FP_TOTAL_WIDTH ) ), moveBar.getPosY() );
				moveBar.setSize( NanoMath.max( 2, ( jumpBar.getWidth() & 1 ) + NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_moveScore, FP_MAX_SCORE ), FP_TOTAL_WIDTH ) ) ), moveBar.getHeight() );
			}
			
			if ( meter != null )
				meter.setRefPixelPosition( scoreBar.getPosX() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_ScoreRequired, FP_MAX_SCORE ), FP_TOTAL_WIDTH ) ), scoreBar.getRefPixelY() );
			
			if ( PlayScreen.getLevel() != PlayScreen.LEVEL_TRAINING ) {
				if ( isJumpScoreAbove() ) {
					label.setText( GameMIDlet.getText( TEXT_CHOOSE_MOVES_ABOVE ) );
					labelAccTime = 0;
					label.setVisible( true );
				} else {
					label.setText( GameMIDlet.getText( TEXT_CHOOSE_MOVES_BELOW ) );
					labelAccTime = LABEL_BLINK_TIME;
				}
			}
		}
	} // fim do m�todo fillBars()
	
	
	/**
	 * Define as barras de pontua��o que ser�o atualizadas pelo gerenciador de movimentos, durante a tela de sele��o de movimentos.
	 * @param scoreBar
	 * @param jumpBar
	 * @param moveBar
	 * @param meter
	 */
	public final void setBars( Drawable scoreBar, Drawable jumpBar, Pattern moveBar, Drawable meter, Label label ) {
		this.scoreBar = scoreBar;
		this.jumpBar = jumpBar;
		this.moveBar = moveBar;
		this.meter = meter;
		this.label = label;
		
		fp_ScoreRequired = PlayScreen.getScoreRequired( false );
		
		fillBars();
	}
	
	
	public final boolean isJumpScoreAbove() {
		return ( PlayScreen.getLevel() == PlayScreen.LEVEL_TRAINING && fp_jumpScore > 0 ) || fp_jumpScore >= fp_ScoreRequired;
	}

	
	/**
	 * Preenche o array contendo a pontua��o total dos movimentos.
	 * @param fp_score array que ser� preenchido com a pontua��o dos movimentos. fp_score[ 0 ] armazenar� a pontua��o conseguida,
	 * e fp_score[ 1 ] armazenar� a pontua��o m�xima que poderia ser obtida.
	 */
	public final void fillScore( int[] fp_score  ) {
		for ( byte i = 0; i < moves.length; ++i ) {
			fp_score[ 0 ] += moves[ i ].getScoreFP();
			fp_score[ 1 ] += moves[ i ].getMaxScoreFP();
		}
	}
	
	
	public final boolean isOnLastMoveIndex() {
		return currentMoveIndex == moves.length - 1;
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		if ( demoMode ) {
			movesPanel.setSize( width, movesPanel.getHeight() );
			
			width -= arrowRight.getWidth() - 1;
			arrowRight.setPosition( width - 1, arrowRight.getPosY() );
		} else {
			width -= movesPanel.getPosX();
			movesPanel.setSize( width, movesPanel.getHeight() );
		}
		
		width -= movesPanelRight.getWidth();
		movesPanelRight.setPosition( width, movesPanelTop.getPosY() );
		movesPanelFill.setSize( movesPanelRight.getPosX() + ( movesPanelRight.getWidth() >> 1 ) - movesPanelFill.getPosX(), movesPanelBottom.getPosY() - movesPanelTop.getHeight() );
		
		width = movesPanelFill.getWidth() - ( movesPanelRight.getWidth() >> 1 );
		movesPanelTop.setSize( width, movesPanelTop.getHeight() );
		movesPanelBottom.setSize( width, movesPanelBottom.getHeight() );

		stepsPanel.setSize( stepsPanel.getWidth(), size.y - stepsPanel.getPosY() );
		stepsPanelLeft.setSize( stepsPanelLeft.getWidth(), stepsPanel.getHeight() );
		stepsPanelRight.setSize( stepsPanelRight.getWidth(), stepsPanel.getHeight() );
		stepsPanelFill.setSize( stepsPanelRight.getPosX() - stepsPanelLeft.getPosX() - stepsPanelLeft.getWidth(), stepsPanel.getHeight() );
	}
	
	
	public final Move getCurrentMove() {
		return currentMove;
	}
	
	
	private static final class StepsGroup extends DrawableGroup {
		
		private short firstIndex;
		
		private short lastIndex;
		
		
		private StepsGroup( int nSlots ) throws Exception {
			super( nSlots );
		}


		protected final void paint( Graphics g ) {
			for ( int i = firstIndex; i < lastIndex; ++i )
				drawables[ i ].draw( g );
		}
	}

}
