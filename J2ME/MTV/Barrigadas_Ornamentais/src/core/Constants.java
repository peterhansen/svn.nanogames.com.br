/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/*******************************************************************************************
	 *                              DEFINI��ES GEN�RICAS                                       *
	 *******************************************************************************************/
	
	
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho das imagens dos �cones de cada sequ�ncia. */
	public static final String PATH_MOVES = PATH_IMAGES + "moves/m_";
	
	/** Caminho das imagens dos �cones dos passos de cada sequ�ncia. */
	public static final String PATH_STEPS = PATH_IMAGES + "moves/s_";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	
	/** �ndice do slot de grava��o das op��es na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �ndice do slot de grava��o de um campeonato salvo. */
	public static final byte DATABASE_SLOT_HIGH_SCORES = 2;
	
	/** �ndice do slot de grava��o de um jogo salvo. */
	public static final byte DATABASE_SLOT_SAVED_GAME = 3;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 3;
	
	/** Cor padr�o do fundo de tela. */
	public static final int BACKGROUND_COLOR = 0x0081fe;
	
	/** Offset da fonte grande. */
	public static final byte FONT_BIG_OFFSET = -2;
	
	/** Offset da fonte de texto. */
	public static final byte FONT_TEXT_OFFSET = -1;
	
	/** �ndice da fonte utilizada para textos. */
	public static final byte FONT_INDEX_TEXT	= 0;
	
	/** �ndice da fonte grande do jogo. */
	public static final byte FONT_INDEX_BIG		= 1;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 2;
	
	//�ndices dos sons do jogo
	public static final byte SOUND_INDEX_SPLASH			= 0;
	public static final byte SOUND_INDEX_LEVEL_CLEARED	= 1;
	public static final byte SOUND_INDEX_GAME_OVER		= 2;
	public static final byte SOUND_INDEX_PREPARE		= 3;
	
	public static final byte SOUND_TOTAL = 4;
	
	/** Dura��o padr�o da vibra��o, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 222;
	
	public static final byte STEP_UP		= 0;
	public static final byte STEP_DOWN		= 1;
	public static final byte STEP_FIRE		= 2;
	public static final byte STEP_LEFT_90	= 3;
	public static final byte STEP_LEFT_180	= 4;
	public static final byte STEP_LEFT_360	= 5;
	public static final byte STEP_LEFT_720	= 6;
	public static final byte STEP_RIGHT_90	= 7;
	public static final byte STEP_RIGHT_180	= 8;
	public static final byte STEP_RIGHT_360	= 9;
	public static final byte STEP_RIGHT_720	= 10;
	
	public static final byte STEP_LEFT_IMAGE_INDEX = 3;
	public static final byte STEP_RIGHT_IMAGE_INDEX = 4;
	
	public static final byte STEP_LEFT_REPEAT_IMAGE_INDEX = 5;
	public static final byte STEP_RIGHT_REPEAT_IMAGE_INDEX = 6;
	
	/** Passo "dummy", usado no caso do jogador pressionar uma tecla n�o utilizada no jogo. */
	public static final byte STEP_DUMMY = -1;
	
	/** N�mero m�nimo de movimentos por fase. */
	public static final byte MOVES_MIN = 2;
	
	/** N�mero m�ximo de movimentos por fase. */
	public static final byte MOVES_MAX = 8;
	
	/** Altura m�nima da tela para que todas as telas utilizem a fonte grande, exceto as telas de ajuda e cr�ditos. */
	public static final short SCREEN_HEIGHT_FONT_BIG = 250;
	
	
	//#if DEMO == "true"
//# 	/** N�mero de jogadas iniciais dispon�veis na vers�o demo. */
//# 	public static final byte DEMO_MAX_PLAYS = 10;	
	//#endif
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_HIGH_SCORES					= TEXT_OPTIONS + 1;
	public static final byte TEXT_PAUSE							= TEXT_HIGH_SCORES + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_TIPS							= TEXT_CONTROLS + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_HELP_TIPS						= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_HELP_TIPS + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_BRAND					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_BRAND + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_NO							= TEXT_YES + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_NO + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU_SAVE		= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU_SAVE + 1;
	public static final byte TEXT_CONFIRM_EXIT					= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_CONFIRM_EXIT_SAVE				= TEXT_CONFIRM_EXIT + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT_SAVE + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_NEW_RECORD					= TEXT_GAME_OVER + 1;
	public static final byte TEXT_LEVEL							= TEXT_NEW_RECORD + 1;
	public static final byte TEXT_CHOOSE						= TEXT_LEVEL + 1;
	public static final byte TEXT_MOVE							= TEXT_CHOOSE + 1;
	public static final byte TEXT_MOVES							= TEXT_MOVE + 1;
	public static final byte TEXT_HEIGHT						= TEXT_MOVES + 1;
	public static final byte TEXT_SPLASH						= TEXT_HEIGHT + 1;
	public static final byte TEXT_RESULT						= TEXT_SPLASH + 1;
	public static final byte TEXT_RESULT_TOTAL					= TEXT_RESULT + 1;
	public static final byte TEXT_SEPARATOR						= TEXT_RESULT_TOTAL + 1;
	public static final byte TEXT_TRAINING						= TEXT_SEPARATOR + 1;
	public static final byte TEXT_CHAMPIONSHIP					= TEXT_TRAINING + 1;
	public static final byte TEXT_CHOOSE_PLATFORM_HEIGHT		= TEXT_CHAMPIONSHIP + 1;
	public static final byte TEXT_MOVEMENT_SEQUENCE_TYPE		= TEXT_CHOOSE_PLATFORM_HEIGHT + 1;
	public static final byte TEXT_AUTOMATIC						= TEXT_MOVEMENT_SEQUENCE_TYPE + 1;
	public static final byte TEXT_MANUAL						= TEXT_AUTOMATIC + 1;
	public static final byte TEXT_CONTINUE_SAVED_GAME			= TEXT_MANUAL + 1;
	public static final byte TEXT_CHOOSE_MOVES_HELP				= TEXT_CONTINUE_SAVED_GAME + 1;
	public static final byte TEXT_CHOOSE_MOVES_BELOW			= TEXT_CHOOSE_MOVES_HELP + 1;
	public static final byte TEXT_CHOOSE_MOVES_ABOVE			= TEXT_CHOOSE_MOVES_BELOW + 1;
	public static final byte TEXT_CHOOSE_MOVES_TRAINING			= TEXT_CHOOSE_MOVES_ABOVE + 1;
	public static final byte TEXT_METER							= TEXT_CHOOSE_MOVES_TRAINING + 1;
	public static final byte TEXT_LOG_TITLE						= TEXT_METER + 1;
	public static final byte TEXT_REPEAT_JUMP					= TEXT_LOG_TITLE + 1;
	public static final byte TEXT_CHANGE_PLATFORM				= TEXT_REPEAT_JUMP + 1;
	public static final byte TEXT_LEVEL_COMPLETED				= TEXT_CHANGE_PLATFORM + 1;
	public static final byte TEXT_PRESS_TO_JUMP					= TEXT_LEVEL_COMPLETED + 1;
	public static final byte TEXT_SCORE_MIN						= TEXT_PRESS_TO_JUMP + 1;
	public static final byte TEXT_SCORE							= TEXT_SCORE_MIN + 1;
	public static final byte TEXT_JUMP							= TEXT_SCORE + 1;
	public static final byte TEXT_LOG_TEXT						= TEXT_JUMP + 1;
	
	/** n�mero total de textos do jogo */
	public static final byte TEXT_TOTAL = TEXT_LOG_TEXT + 1;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_CHOOSE_SOUND			= 0;
	public static final byte SCREEN_SPLASH_NANO				= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND			= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME_MENU			= SCREEN_MAIN_MENU + 1;	
	public static final byte SCREEN_SAVED_GAME_FOUND		= SCREEN_NEW_GAME_MENU + 1;	
	public static final byte SCREEN_REPEAT_JUMP_MENU		= SCREEN_SAVED_GAME_FOUND + 1;	
	public static final byte SCREEN_CHOOSE_PLATFORM_HEIGHT	= SCREEN_REPEAT_JUMP_MENU + 1;	
	public static final byte SCREEN_JUMP_MENU				= SCREEN_CHOOSE_PLATFORM_HEIGHT + 1;	
	public static final byte SCREEN_NEW_GAME				= SCREEN_JUMP_MENU + 1;	
	public static final byte SCREEN_PREPARE_LEVEL			= SCREEN_NEW_GAME + 1;	
	public static final byte SCREEN_CONTINUE_GAME			= SCREEN_PREPARE_LEVEL + 1;	
	public static final byte SCREEN_RESULTS					= SCREEN_CONTINUE_GAME + 1;	
	public static final byte SCREEN_OPTIONS					= SCREEN_RESULTS + 1;
	public static final byte SCREEN_HIGH_SCORES				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_HIGH_SCORES + 1;
	public static final byte SCREEN_HELP_OBJECTIVES			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS			= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_HELP_MOVEMENTS				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_HELP_MOVES_SCREEN		= SCREEN_HELP_MOVEMENTS + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_HELP_MOVES_SCREEN + 1;
	public static final byte SCREEN_PAUSE					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU_TRAINING	= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT_TRAINING	= SCREEN_CONFIRM_MENU_TRAINING + 1;
	public static final byte SCREEN_CONFIRM_MENU_SAVE		= SCREEN_CONFIRM_EXIT_TRAINING + 1;
	public static final byte SCREEN_CONFIRM_EXIT_SAVE		= SCREEN_CONFIRM_MENU_SAVE + 1;
	public static final byte SCREEN_CHOOSE_YOUR_MOVES		= SCREEN_CONFIRM_EXIT_SAVE + 1;
	public static final byte SCREEN_LOADING_1				= SCREEN_CHOOSE_YOUR_MOVES + 1;	
	public static final byte SCREEN_LOADING_2				= SCREEN_LOADING_1 + 1;	
	public static final byte SCREEN_LOADING_PLAY_SCREEN		= SCREEN_LOADING_2 + 1;	
	
	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG				= SCREEN_LOADING_PLAY_SCREEN + 1;	
	//#endif
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_HIGH_SCORES	= 2;
	
	//#if DEMO == "true"
	//# 	/** �ndice da entrada do menu principal para se comprar a vers�o completa do jogo. */
	//# 	public static final byte ENTRY_MAIN_MENU_BUY_FULL_GAME	= 3;
	//# 	
	//# 	
	//# 	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	//# 	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	//# 	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;	
	//#endif
	
	public static final byte ENTRY_MAIN_MENU_HELP		= 3;
	public static final byte ENTRY_MAIN_MENU_CREDITS	= 4;
	public static final byte ENTRY_MAIN_MENU_EXIT		= 5;
	
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_TIPS						= 2;
	public static final byte ENTRY_HELP_MENU_BACK						= 3;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE NOVO JOGO">
	
	public static final byte ENTRY_NEW_GAME_MENU_TRAINING		= 0;
	public static final byte ENTRY_NEW_GAME_MENU_CHAMPIONSHIP	= 1;
	public static final byte ENTRY_NEW_GAME_MENU_BACK			= 2;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE ESCOLHA DO PULO">
	
	public static final byte ENTRY_JUMP_MENU_AUTOMATIC	= 1;
	public static final byte ENTRY_JUMP_MENU_MANUAL		= 2;
	public static final byte ENTRY_JUMP_MENU_BACK		= 3;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU AP�S PULO NO TREINO">
	
	public static final byte ENTRY_REPEAT_JUMP_MENU_REPEAT				= 0;
	public static final byte ENTRY_REPEAT_JUMP_MENU_CHOOSE_PLATFORM		= 1;
	public static final byte ENTRY_REPEAT_JUMP_BACK_TO_MAIN_MENU		= 2;
	
	// </editor-fold>		
	
	
	/** Tempo necess�rio para o personagem fazer uma rota��o de 90�, em milisegundos. */
	public static final short ROTATION_90_TIME = 140;
	
	/** Altura do fundo de tela das estrelas, relativa � porcentagem da altura total da tela de jogo. */
	public static final byte BKG_STARS_PERCENT		= 50;
	/** Altura do fundo de tela do sol, relativa � porcentagem da altura total da tela de jogo. */
	public static final byte BKG_SUN_PERCENT		= 30;
	/** Altura do fundo de tela do c�u azul, relativa � porcentagem da altura total da tela de jogo. */
	public static final byte BKG_BLUE_SKY_PERCENT	= 100 - BKG_STARS_PERCENT - BKG_SUN_PERCENT;
	
	/** Cor do fundo de tela das estrelas. */
	public static final int BKG_STAR_COLOR = 0x000c4e;
	
	/** Cor do fundo de tela do c�u azul. */
	public static final int BKG_SUN_SKY_COLOR = 0x0081fe;
	
	/** Cor do piso ao redor da piscina. */
	public static final int BKG_POOL_COLOR = 0xffd76d;
	
	
	// <editor-fold desc="DEFINI��ES GEN�RICAS DO JOGO">
	
	/** Pontua��o padr�o por movimento (antes do multiplicador). */
	public static final int FP_DEFAULT_MOVE_SCORE = 65536; // 1
	
	/** Multiplicador m�ximo de pontua��o de um movimento. */
	public static final int FP_MAX_DIFFICULTY_MULTIPLIER = 655360; 
	
	/** Pontua��o m�xima dada pela precis�o no pulo inicial. */
	public static final int FP_JUMP_HEIGHT_MAX_SCORE = 327680; // 5
	
	/** Pontua��o m�xima dada pela entrada na �gua. */
	public static final int FP_SPLASH_SCORE = 786432; // 12 (o restante � do movimento da barrigada)
	
	/** Pontua��o m�xima total dada pela entrada na �gua. */
	public static final int FP_SPLASH_TOTAL_SCORE = 983040; // 15
	
	/** Pontua��o m�xima que pode ser obtida com elementos n�o ligados aos movimentos. */
	public static final int FP_NON_MOVE_MAX_SCORE = FP_JUMP_HEIGHT_MAX_SCORE + FP_SPLASH_TOTAL_SCORE;
	
	/** Pontua��o m�nima inicial para passar de n�vel. */
	public static final int FP_LEVEL_SCORE_MIN = FP_DEFAULT_MOVE_SCORE * 80 / 10; // NanoMath.mulFixed( NanoMath.mulFixed( FP_DEFAULT_MOVE_SCORE, FP_MAX_DIFFICULTY_MULTIPLIER ), NanoMath.HALF );
	
	/** Pontua��o m�nima final para passar de n�vel. */
	public static final int FP_LEVEL_SCORE_MAX = 3276800;// 50;  3604400 = NanoMath.mulFixed( ( FP_SPLASH_MAX_SCORE + FP_JUMP_HEIGHT_MAX_SCORE + NanoMath.mulFixed( NanoMath.mulFixed( FP_DEFAULT_MOVE_SCORE, FP_MAX_DIFFICULTY_MULTIPLIER ), NanoMath.toFixed( MOVES_MAX ) ) ), NanoMath.divInt( 55, 100 ) );
	
	/** Altura total em metros da tela de jogo. */
	public static final int FP_PLAYSCREEN_TOTAL_HEIGHT = 13107200; // 200m
	
	/** N�vel de dificuldade m�xima do jogo. */
	public static final byte LEVEL_MAX = 20;
	
	/** Altura m�nima da plataforma, em unidades reais. */
	public static final int FP_PLATFORM_MIN_HEIGHT = 131072; // 2m
	
	/** Altura m�nima da plataforma no modo treino, em unidades reais. */
	public static final int FP_PLATFORM_CHOOSE_MIN_HEIGHT = 655360; // 10m
	
	/** Altura m�xima da plataforma, em unidades reais. */
	public static final int FP_PLATFORM_MAX_HEIGHT = 7864320; // 120m
	
	/** Acelera��o da gravidade em metros por segundo ao quadrado. */
	public static final int FP_GRAVITY_ACC = -300000; //NanoMath.divInt( -98, 10 ) = -642253;
	
	/** Impuls�o m�nima ao pular da plataforma, em metros. */
	public static final int FP_JUMP_HEIGHT_MIN = 16384; // 0,25m
	
	/** Impuls�o m�xima ao pular da plataforma, em metros. */
	public static final int FP_JUMP_HEIGHT_MAX_DIFF = FP_PLAYSCREEN_TOTAL_HEIGHT - FP_JUMP_HEIGHT_MIN - FP_PLATFORM_MAX_HEIGHT;
	
	/** Dist�ncia em metros percorrida ao longo do pulo pelo personagem. */
	public static final int FP_JUMP_X_OFFSET = -327680; //NanoMath.toFixed( -5 );
	
	/** Altura m�xima do pulo do Bo�a ao pular antes do trampolim. */
	public static final int FP_PREPARE_JUMP_Y_MAX = NanoMath.ONE << 1;
	
	/** Dist�ncia horizontal para a ponta do trampolim, de onde Bo�a inicia seu pulo. */
	public static final int FP_PREPARE_JUMP_X = NanoMath.ONE;
	
	/** Tempo que o jogador leva desde a pisada no trampolim at� voltar � posi��o horizontal do mesmo. */
	public static final int FP_PREPARE_JUMP_TIME = 39321; // NanoMath.divInt( 600, 1000 );
	
	/** Tempo total que o jogador tem para realizar a jogada de impuls�o no trampolim, em segundos. */
	public static final int FP_PREPARE_JUMP_TOTAL_TIME = 49152; // NanoMath.divInt( 750, 1000 ); 
	
	/** Dura��o do mergulho do Bo�a, em segundos. */
	public static final int FP_TIME_DIVING = 390000;	// ~ 6
	
	/** "Peso" da altura m�xima alcan�ada pelo Bo�a no c�lculo do splash. */
	public static final int FP_SPLASH_PERCENT_JUMP_HEIGHT = 42598;//NanoMath.divInt( 65, 100 );
	
	/** "Peso" do �ngulo de entrada na �gua no c�lculo do splash. */
	public static final int FP_SPLASH_PERCENT_ROTATION = NanoMath.ONE - FP_SPLASH_PERCENT_JUMP_HEIGHT;
	
	public static final byte ROTATION_0		= 0;
	public static final byte ROTATION_90	= 1;
	public static final byte ROTATION_180	= 2;
	public static final byte ROTATION_270	= 3;
	public static final byte ROTATION_360	= 4;
	
	
	// </editor-fold>

	
	//#if SCREEN_SIZE == "SMALL"
//# 	
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	/** Quantidade de pixels correspondentes a 1 metro "real". */
//# 	public static final int FP_METER_TO_PIXELS = 1860506; //NanoMath.divFixed( NanoMath.toFixed( 73 ), NanoMath.divInt( 18, 10 ) );
//# 	
//# 	/** Largura total da tela de jogo. */
//# 	public static final short PLAYSCREEN_TOTAL_WIDTH = 480;
//# 	
//# 	/** Dist�ncia da plataforma ao canto direito da tela de jogo. */
//# 	public static final byte PLATFORM_RIGHT_X_OFFSET = -12;
//# 	
//# 	/** Dist�ncia m�nima em pixels que o jogador pode ser posicionado relativamente � tela (parte superior ou inferior da tela). */
//# 	public static final byte CAMERA_PLAYER_MIN_BORDER_DISTANCE = 20;
//# 	
//# 	/** Posi��o x do pixel de refer�ncia da prancha em rela��o ao 0,0 da imagem da plataforma. */
//# 	public static final byte SPRINGBOARD_X_OFFSET = 36;
//# 	
//# 	/** Posi��o y do pixel de refer�ncia da prancha em rela��o ao 0,0 da imagem da plataforma. */
//# 	public static final byte SPRINGBOARD_Y_OFFSET = 17;
//# 	
//# 	/** Largura da prancha em pixels. */
//# 	public static final byte SPRINGBOARD_WIDTH = 72;
//# 	
//# 	/** Altura m�xima da prancha em pixels (valor utilizado para fazer o desenho da prancha). */
//# 	public static final short SPRINGBOARD_MAX_HEIGHT = 80;
//# 	
//# 	/** Altura m�nima da prancha, em pixels. */
//# 	public static final byte SPRINGBOARD_MIN_HEIGHT = 6;	
//# 	
//# 	public static final byte JUMP_BAR_REF_Y = 13;
//# 	
//# 	/** Largura do �cone de movimento. */
//# 	public static final byte MOVE_ICON_WIDTH = 28;
//# 	
//# 	/** Altura do �cone de movimento. */
//# 	public static final byte MOVE_ICON_HEIGHT = 26;
//# 	
//# 	/** Quantidade m�xima de degraus que os torcedores podem variar na arquibancada. */
//# 	public static final byte STADIUM_TOTAL_STEPS = 3;		
//# 	
//# 	/** Dist�ncia vertical entre os degraus do est�dio. */
//# 	public static final byte STADIUM_STEP_HEIGHT = 9;
//# 	
//# 	/** Largura interna do pattern da mira dos passos. */
//# 	public static final byte STEP_AIM_WIDTH = 28;	
//# 	
//# 	//</editor-fold>
//# 	
	//#elif SCREEN_SIZE == "MEDIUM"
	
	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA M�DIA" defaultstate="collapsed">
	
	/** Quantidade de pixels correspondentes a 1 metro "real". */
	public static final int FP_METER_TO_PIXELS = 2657866; //NanoMath.divFixed( NanoMath.toFixed( 73 ), NanoMath.divInt( 18, 10 ) );
	
	/** Largura total da tela de jogo. */
	public static final short PLAYSCREEN_TOTAL_WIDTH = 480;
	
	/** Dist�ncia da plataforma ao canto direito da tela de jogo. */
	public static final byte PLATFORM_RIGHT_X_OFFSET = -12;
	
	/** Dist�ncia m�nima em pixels que o jogador pode ser posicionado relativamente � tela (parte superior ou inferior da tela). */
	public static final byte CAMERA_PLAYER_MIN_BORDER_DISTANCE = 20;
	
	/** Posi��o x do pixel de refer�ncia da prancha em rela��o ao 0,0 da imagem da plataforma. */
	public static final byte SPRINGBOARD_X_OFFSET = 36;
	
	/** Posi��o y do pixel de refer�ncia da prancha em rela��o ao 0,0 da imagem da plataforma. */
	public static final byte SPRINGBOARD_Y_OFFSET = 23;
	
	/** Largura da prancha em pixels. */
	public static final byte SPRINGBOARD_WIDTH = 90;
	
	/** Altura m�xima da prancha em pixels (valor utilizado para fazer o desenho da prancha). */
	public static final short SPRINGBOARD_MAX_HEIGHT = 100;
	
	/** Altura m�nima da prancha, em pixels. */
	public static final byte SPRINGBOARD_MIN_HEIGHT = 6;	
	
	public static final byte JUMP_BAR_REF_Y = 13;
	
	/** Largura do �cone de movimento. */
	public static final byte MOVE_ICON_WIDTH = 37;
	
	/** Altura do �cone de movimento. */
	public static final byte MOVE_ICON_HEIGHT = 34;
	
	/** Quantidade m�xima de degraus que os torcedores podem variar na arquibancada. */
	public static final byte STADIUM_TOTAL_STEPS = 5;		
	
	/** Dist�ncia vertical entre os degraus do est�dio. */
	public static final byte STADIUM_STEP_HEIGHT = 10;
	
	/** Largura interna do pattern da mira dos passos. */
	public static final byte STEP_AIM_WIDTH = 37;
	
	//</editor-fold>	
	
	
	//#endif
	
}
