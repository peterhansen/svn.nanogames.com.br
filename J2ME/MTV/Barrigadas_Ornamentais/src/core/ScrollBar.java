/**
 * ScrollBar.java
 * �2008 Nano Games.
 *
 * Created on Jul 16, 2008 1:16:34 PM.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;


/**
 * 
 * @author Peter
 */
public final class ScrollBar extends DrawableGroup implements Constants {

	private static final byte TOTAL_ITEMS = 5;
	
	private static final byte IMG_BACK_FILL = 0;
	private static final byte IMG_FORE_FILL = 1;
	private static final byte IMG_TOP = 2;
	private static final byte IMG_CENTER = 3;
	private static final byte IMG_BOTTOM = 4;
	
	public static final byte TYPE_BACKGROUND = 0;
	public static final byte TYPE_FOREGROUND = 1;
	
	private final DrawableImage top;
	
	private final DrawableImage bottom;
	
	private final Pattern fill;
	
	private final DrawableImage center;
	
	private static DrawableImage[] IMAGES;
	
	
	public ScrollBar( byte type ) throws Exception {
		super( TOTAL_ITEMS );
		
		top = new DrawableImage( IMAGES[ IMG_TOP ] );
		insertDrawable( top );
		
		bottom = new DrawableImage( IMAGES[ IMG_BOTTOM ] );
		insertDrawable( bottom );
		
		if ( type == TYPE_FOREGROUND ) {
			fill = new Pattern( new DrawableImage( IMAGES[ IMG_FORE_FILL ] ) );
			insertDrawable( fill );
			
			center = new DrawableImage( IMAGES[ IMG_CENTER ] );
			insertDrawable( center );
		} else {
			fill = new Pattern( new DrawableImage( IMAGES[ IMG_BACK_FILL ] ) );
			insertDrawable( fill );
			
			center = null;
		}
		
		fill.setPosition( 0, top.getHeight() );
		fill.setSize( fill.getFill().getWidth(), 0 );

		size.x = top.getWidth();
	}
	
	
	public static final void loadImages() throws Exception {
		IMAGES = new DrawableImage[ TOTAL_ITEMS ];
		
		final String PATH = PATH_IMAGES + "scroll_";
		for ( byte i = 0; i < TOTAL_ITEMS; ++i )
			IMAGES[ i ] = new DrawableImage( PATH + i + ".png" );		
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		bottom.setPosition( 0, size.y - bottom.getHeight() );
		fill.setSize( fill.getWidth(), bottom.getPosY() - top.getHeight() );
		
		if ( center != null ) {
			if ( size.y - top.getHeight() - bottom.getHeight() > center.getHeight() ) {
				center.setPosition( 0, ( size.y - center.getHeight() ) >> 1 );
				center.setVisible( true );
			} else {
				center.setVisible( false );
			}
		}
	}
	
	
}
