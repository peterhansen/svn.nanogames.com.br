/**
 * SplashEmitter.java
 * �2008 Nano Games.
 *
 * Created on May 1, 2008 10:53:37 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class SplashEmitter extends Drawable implements Updatable, Constants {
	
	private static final short MIN_PARTICLES = 80;
	//#if JAR == "min"
//# 	// aparelhos com pouco desempenho
//# 	private static final short MAX_PARTICLES = 380;
	//#else
	private static final short MAX_PARTICLES = 550;
	//#endif
	private static final short MAX_PARTICLES_DIFF = ( short ) ( MAX_PARTICLES - MIN_PARTICLES );
	
	private final int FP_MAX_Y_SPEED;
	
	private static final int FP_EXPLOSION_MAX_WIDTH = 180000; // ~3m
	private static final int FP_EXPLOSION_MAX_HEIGHT = 280000; // ~4,3m
	private static final int FP_BORN_MAX_Y_DIFF = NanoMath.ONE;
	
	private final int BORN_MAX_Y_DIFF;
	private final int BORN_MAX_HALF_Y_DIFF;

	private static final int FP_MINUS_TWO = -131072; // NanoMath.toFixed( -2 );
	
	private static final int FP_HALF_GRAVITY_ACC = FP_GRAVITY_ACC >> 1;
	
	private final Particle[] particles;
	
	private short activeParticles;
	
	
	public SplashEmitter( Point parentSize ) throws Exception {
		setSize( parentSize );
		
		// b = sqrt( -4 * MAX_Y * a ), onde b = velocidade inicial e a = gravidade
		FP_MAX_Y_SPEED =  NanoMath.sqrtFixed( NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( -4 ), FP_GRAVITY_ACC ), FP_EXPLOSION_MAX_HEIGHT ) );
		
		BORN_MAX_Y_DIFF = NanoMath.toInt( NanoMath.mulFixed( FP_BORN_MAX_Y_DIFF, FP_METER_TO_PIXELS ) );
		BORN_MAX_HALF_Y_DIFF = BORN_MAX_Y_DIFF >> 1;		
		
		particles = new Particle[ MAX_PARTICLES ];
		for ( short i = 0; i < MAX_PARTICLES; ++i ) {
			particles[ i ] = new Particle( this );
		}
	}


	protected final void paint( Graphics g ) {
		for ( short i = 0; i < activeParticles; ++i ) {
			particles[ i ].draw( g );
		}
	}
	
	
	public final void update( int delta ) {
		final short previousActiveParticles = activeParticles;
		final int FP_DELTA = NanoMath.divInt( delta, 1000 );
		for ( short i = 0; i < activeParticles; ++i ) {
			particles[ i ].update( FP_DELTA );
		}		
		
		if ( previousActiveParticles != activeParticles )
			sort();
	}	
	
	
	public final void reset() {
		activeParticles = 0;
	}	
	
	
	private final void sort() {
		short i;
		short j;
		for ( i = 0, j = MAX_PARTICLES - 1; i < MAX_PARTICLES; ++i ) {
			if ( particles[ i ].state == Particle.STATE_DEAD ) {
				for ( ; j > i; --j ) {
					if ( particles[ j ].state != Particle.STATE_DEAD ) {
						final Particle temp = particles[ i ];
						particles[ i ] = particles[ j ];
						particles[ j ] = temp;
						break;
					}
				}
			}
		}
		
		activeParticles = j;
	}
	
	
	public final void explode( Point pos, int width, int fp_percent ) {
		//#if DEBUG == "true"
		System.out.println( "SPLASH EXPLODE: " + NanoMath.toString( NanoMath.mulFixed( fp_percent, NanoMath.toFixed( 100 ) ) ) + "%" );
		//#endif
		
		final int PERCENT = NanoMath.toInt( NanoMath.mulFixed( fp_percent, NanoMath.toFixed( 100 ) ) );
		
		final short PARTICLES = ( short ) ( MIN_PARTICLES + ( PERCENT * MAX_PARTICLES_DIFF / 100 ) );
		final short MAX_HALF_WIDTH = ( short ) ( width >> 1 );
		
		final int FP_CURRENT_EXPLOSION_WIDTH = NanoMath.mulFixed( FP_EXPLOSION_MAX_WIDTH, fp_percent );
		
		activeParticles = 0;
		final Point temp = new Point();
		for ( short i = 0; i < PARTICLES; ++i ) {
			temp.set( pos.x - MAX_HALF_WIDTH + NanoMath.randInt( width ), pos.y - BORN_MAX_HALF_Y_DIFF + NanoMath.randInt( BORN_MAX_Y_DIFF ) );
			particles[ i ].born( temp, fp_percent, FP_CURRENT_EXPLOSION_WIDTH );
		}
		activeParticles = PARTICLES;
	}
	
	
	private final void onDie() {
		--activeParticles;
		
		//#if DEBUG == "true"
		if ( activeParticles < 0 )
			throw new RuntimeException( "N�mero de part�culas negativo." );
		//#endif
	}
	
	
	/**
	 * Classe interna que descreve uma part�cula de fogos de artif�cio. 
	 */
	private static final class Particle extends Drawable implements Updatable {
		
		private static final int FP_LIFE_TIME_EXPLODED = 26214; // NanoMath.divInt( 4, 10 );
		
		/** Tempo de vida da part�cula. */
		private int fp_lifeTime;
		
		/** Tempo acumulado da part�cula. */
		private int fp_accTime;
		
		private int fp_speedX;
		private int fp_speedY;
		
		private final Point fp_Pos = new Point();
		
		private final Point initialPos = new Point();
		
		private static final byte STATE_DEAD		= 0;
		private static final byte STATE_FLYING		= 1;
		private static final byte STATE_EXPLODED	= 2;
		
		/** Estado atual da part�cula. */
		private byte state;
		
		private static final int COLOR_FLYING = 0x88eeff;
		private static final int COLOR_EXPLODED = 0x00aaff;
		
		private int color;
		
		private final SplashEmitter parent;
		
		
		private Particle( SplashEmitter parent  ) {
			this.parent = parent;
			setSize( 3, 2 );
		}
		
		
		private final void born( Point bornPosition, int fp_percent, int fp_explosionMaxWidth ) {
			fp_speedY = 1 + NanoMath.randFixed( NanoMath.mulFixed( parent.FP_MAX_Y_SPEED, fp_percent ) );
			fp_lifeTime = NanoMath.divFixed( NanoMath.mulFixed( fp_speedY, FP_MINUS_TWO ), FP_GRAVITY_ACC );
			
			fp_accTime = 0;
			
			color = COLOR_FLYING;
			
			fp_speedX = NanoMath.divFixed( -( fp_explosionMaxWidth >> 1 ) + NanoMath.randFixed( fp_explosionMaxWidth ), fp_lifeTime + 1 );
			initialPos.set( bornPosition );
			setPosition( bornPosition );
			state = STATE_FLYING;
			
			setVisible( true );
		}

		
		/**
		 * Prepara a part�cula para ser desenhada. � basicamente o m�todo <code>draw</code> de Drawable, por�m n�o
		 * realiza testes de clip (utiliza a �rea definida pelo pai, ou seja, pelo emissor).
		 * 
		 * @param g
		 */
		public final void draw( Graphics g ) {
			if ( visible ) {
				translate.addEquals( position );

				g.setColor( color );
				g.fillRect( translate.x, translate.y, size.x, size.y );

				translate.subEquals( position );
			}
		} // fim do m�todo draw( Graphics )		
		
		
		protected final void paint( Graphics g ) {
		}


		public final void update( int fp_delta ) {
			fp_accTime += fp_delta;
			
			switch ( state ) {
				case STATE_FLYING:
					setSize( size.y, size.x );
					if ( fp_accTime >= fp_lifeTime ) {
						fp_lifeTime = FP_LIFE_TIME_EXPLODED;
						setSize( 3, 2 );
						fp_accTime = 0;
						color = COLOR_EXPLODED;
						state = STATE_EXPLODED;
					} else {
						fp_Pos.x = NanoMath.mulFixed( fp_speedX, fp_accTime );
						fp_Pos.y = NanoMath.mulFixed( fp_speedY, fp_accTime ) + NanoMath.mulFixed( FP_HALF_GRAVITY_ACC, NanoMath.mulFixed( fp_accTime, fp_accTime ) );
					}
					
					setPosition( initialPos.x + NanoMath.toInt( NanoMath.mulFixed( fp_Pos.x, FP_METER_TO_PIXELS ) ),
								 initialPos.y - NanoMath.toInt( NanoMath.mulFixed( fp_Pos.y, FP_METER_TO_PIXELS ) ) );
				break;
				
				case STATE_EXPLODED:
					if ( fp_accTime >= fp_lifeTime ) {
						state = STATE_DEAD;
						setVisible( false );
						parent.onDie();
					}
				break;
			}
		}
	}


}
