/**
 * Play.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:08:24 AM.
 */

package core;

import br.com.nanogames.components.util.Point3f;


/**
 * 
 * @author Peter
 */
public final class Play {

	public final Point3f direction = new Point3f();
	public int fp_height;
	public int fp_power;
	public int fp_curve;
	
	
	public Play() {
	}
	
	
	public Play( Point3f direction, int height, int power, int curve ) {
		set( direction, height, power, curve );
	}
	
	
	public final void set( Point3f direction, int height, int power, int curve ) {
		this.direction.set( direction );
		fp_height = height;
		fp_power = power;
		fp_curve = curve;
	}

	
	public final void set( Play p ) {
		set( p.direction, p.fp_height, p.fp_power, p.fp_curve                );
	}
	
}
