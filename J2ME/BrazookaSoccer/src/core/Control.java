/**
 * Control.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 5:05:54 PM.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
//#if DEBUG == "true"
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
//#endif
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
import screens.Match;


/**
 * 
 * @author Peter
 */
public final class Control extends UpdatableGroup implements Constants, KeyListener {

	// tipos de controle disponíveis
	private static final byte STATE_NONE					= 0;
	private static final byte STATE_DIRECTION_POWER_APPEAR	= 1;
	private static final byte STATE_DIRECTION_POWER			= 2;
	private static final byte STATE_HEIGHT_CURVE_APPEAR		= 3;
	public static final byte STATE_HEIGHT_CURVE			    = 4;
	public static final byte STATE_HIDING					= 5;
	private static final byte STATE_DONE					= 6;
	//#if TOUCH == "true"
		private final Point lastPointerPos = new Point();
	//#endif
	/** tempo em segundos que o controle de força leva para percorrer toda a faixa de valores */
	public static final int FP_POWER_CONTROL_TIME = 34865;

	public static final int FP_CROSSHAIR_SPEED = NanoMath.ONE;

	// valores mínimos e máximos de cada controle
	public static final int FP_CONTROL_HEIGHT_MIN_VALUE		= NanoMath.divInt( 0, 10 );
	public static final int FP_CONTROL_HEIGHT_MAX_VALUE		= NanoMath.divInt( 4, 10 );
	
	public static final int FP_CONTROL_POWER_MIN_VALUE 	= 500000;//NanoMath.mulFixed( FP_REAL_PENALTY_TO_GOAL, NanoMath.divInt( -12, 10 )  );
	public static final int FP_CONTROL_POWER_MAX_VALUE 	= 3500000;
	
	/** Número máximo de itens da coleção. */
	private static final byte CONTROL_MAX_ITEMS = 9; // if debug == false CONTROL_MAX_ITEMS = 7;

	private final DrawableImage cameraFolow;

	private final DrawableImage powerBar;
	private final DrawableImage powerLevel;
	private int lastX ;
	private int lastY;

	private static final short TIME_APPEAR = 333;

	private short offset;

	private final Rectangle powerLevelViewport = new Rectangle();

	/** Valor mínimo e máximo da curva. */
	public static final int FP_CURVE_MIN_VALUE	= -NanoMath.ONE;
	public static final int FP_CURVE_MAX_VALUE = NanoMath.ONE;
	
	// jogada armazenada temporariamente
	private final Play currentPlay = new Play();		
	
	// controle atualmente sendo usado
	private byte currentControl;	

	private int fpCurrentValue;
	
	private int fp_accTime;

	private final Match match;

	private final IsoArrow arrow;
	//#if TOUCH == "true"
	private final DrawableImage arrowR;
	private final DrawableImage arrowL;
	private final DrawableImage Directionball;
    //#endif
	private static final byte DIRECTION_ANGLE_SPEED = 120;

	private final MUV directionMUV = new MUV( DIRECTION_ANGLE_SPEED );

	public static final byte DIRECTION_MODE_NONE				= 0;
	public static final byte DIRECTION_MODE_CLOCKWISE			= 1;
	public static final byte DIRECTION_MODE_COUNTER_CLOCKWISE	= 2;

	private byte directionMode;

	private short directionAngle;

	private final Ball ball;

	private final DrawableImage ballBig;

	private final DrawableImage crosshair;

	/** Velocidade da mira em porcentagem por segundo, em notação de ponto fixo. */
	private final Point crosshairSpeed = new Point();

	/** Porcentagem referente à posição da mira, em notação de ponto fixo. */
	private final Point crosshairPercent = new Point();
    /** Margem de erro da mira */
	private final Point crosshairError = new Point();
    /** Valor maximo da margem erro da mira representa o raio em pixels dessa margem */
	private static final short MAX_ERROR = (NanoMath.ONE/9);

    /** Variaveis temporarias */
	private final Point tempPoint = new Point();
	private int tempInt;

    /** Tempo total de execucao do controle */
	private int totaltime;

	//#if DEBUG == "true"
    private final Label lbltest;
    //#endif
	
	/**
	 * 
	 * @param goal 
	 * @param ballPosY 
	 * @throws java.lang.Exception
	 */
	public Control( Match match, IsoArrow arrow, Ball ball ) throws Exception {
		super( CONTROL_MAX_ITEMS );

        totaltime = 0;

        cameraFolow = new DrawableImage( PATH_IMAGES + "i_1.png" );
		insertDrawable( cameraFolow );
		cameraFolow.setVisible( false );
        cameraFolow.setPosition(ScreenManager.SCREEN_WIDTH-cameraFolow.getSize().x, 0);

		powerBar = new DrawableImage( PATH_IMAGES + "power.png" );
		insertDrawable( powerBar );

		powerLevel = new DrawableImage( PATH_IMAGES + "power_level.png" );
		powerLevel.setViewport( powerLevelViewport );
		insertDrawable( powerLevel );

		ballBig = new DrawableImage( PATH_IMAGES + "ball_big.png" );
		ballBig.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		insertDrawable( ballBig );

		crosshair = new DrawableImage( PATH_IMAGES + "crosshair_big.png" );
		crosshair.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		insertDrawable( crosshair );

		//#if TOUCH == "true"
		arrowR = new DrawableImage(PATH_IMAGES + "ar.png");
		arrowL = new DrawableImage(arrowR.getImage());
		arrowL.mirror(TRANS_MIRROR_H);
		Directionball = new DrawableImage(PATH_IMAGES + "crosshair_big.png");
		Directionball.setPosition(ScreenManager.SCREEN_HALF_WIDTH- Directionball.getWidth()/2, ScreenManager.SCREEN_HEIGHT - Directionball.getHeight() - Directionball.getHeight()/2);
		arrowL.setPosition(Directionball.getPosX()- arrowL.getWidth(), Directionball.getPosY()+Directionball.getHeight()/2 - arrowL.getHeight()/2);
		arrowR.setPosition(Directionball.getPosX()+ Directionball.getWidth(), Directionball.getPosY()+Directionball.getHeight()/2 - arrowL.getHeight()/2);

		insertDrawable(Directionball);
		insertDrawable(arrowR);
		insertDrawable(arrowL);
		//#endif

		this.match = match;
		this.arrow = arrow;
		this.ball = ball;

        //#if DEBUG == "true"
        lbltest = new Label(0 ,"("+crosshairPercent.x+","+crosshairPercent.y+")");
		insertDrawable(lbltest);
		//#endif

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setVisible( false );
	}
	
	
	/**
	 *
	 */
	public final void reset() {
		setState( STATE_DIRECTION_POWER_APPEAR );
	} // fim do método reset()


	public final void setPlay( Play p ) {
		currentPlay.set( p );
	}
	

	/**
	 *
	 * 
	 * @return 
	 */
	public final boolean confirm() {
		switch ( currentControl ) {
			case STATE_DIRECTION_POWER:
				setState( STATE_HEIGHT_CURVE_APPEAR );
			break;

			case STATE_HEIGHT_CURVE:
				setState( STATE_HIDING );
			return true;

			//#if DEBUG == "true"
            case STATE_DONE:
            default:
                System.out.println( "Estado inválido do controle: " + currentControl );
            return false;
			//#endif
		} // fim switch ( currentControl )

		return false;
	} // fim do método setControl()


	private final void setState( int state ) {
		this.currentControl = ( byte ) state;

		switch ( state ) {
			case STATE_DIRECTION_POWER_APPEAR:
				setVisible( true );

				ballBig.setVisible( false );
                //#if DEBUG == "true"
                lbltest.setVisible( false );
                //#endif
				crosshair.setVisible( false );

				powerBar.setPosition( -powerBar.getWidth(), powerBar.getPosY() );
				fp_accTime = TIME_APPEAR;

				// define o ângulo inicial de forma a apontar aproximadamente para o gol
				final Point3f right = new Point3f( NanoMath.ONE, 0, 0 );
				final Point3f direction = ball.getRealPosition().mul( -NanoMath.ONE );
				final int fp_cosTheta = NanoMath.divFixed( right.dot( direction ), NanoMath.mulFixed( right.getModule(), direction.getModule() ) );
				directionAngle = ( short ) ( -90 + NanoMath.toInt( NanoMath.mulFixed( fp_cosTheta, NanoMath.toFixed( 90 ) ) ) );
				refreshArrow();

				fpCurrentValue = FP_CONTROL_POWER_MIN_VALUE + NanoMath.mulFixed( NanoMath.randFixed(), ( FP_CONTROL_POWER_MAX_VALUE - FP_CONTROL_POWER_MIN_VALUE ) );
			break;

			case STATE_DIRECTION_POWER:
				//#if TOUCH == "true"
				arrowL.setVisible(true);
				arrowR.setVisible(true);
				Directionball.setVisible(true);
				//#endif
				powerBar.setPosition( 0, powerBar.getPosY() );
				powerBar.setVisible( true );
				powerLevel.setVisible( true );
			break;

			case STATE_HEIGHT_CURVE_APPEAR:
				//#if TOUCH == "true"
				arrowL.setVisible(false);
				arrowR.setVisible(false);
				Directionball.setVisible(false);
				//#endif
				fp_accTime = 0;
				ballBig.setVisible( true );
				ballBig.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, -ScreenManager.SCREEN_HALF_HEIGHT );
                //#if DEBUG == "true"
                lbltest.setVisible( true );
                lbltest.setPosition(ballBig.getPosX(), ballBig.getPosY()-40);
                //#endif
			break;

			case STATE_HEIGHT_CURVE:
				ballBig.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
				
				currentPlay.fp_power = fpCurrentValue;
				currentPlay.direction.set( getArrowDirection() );

				crosshairPercent.set( NanoMath.HALF, NanoMath.HALF );
                crosshairError.set( 0, 0 );
				crosshairSpeed.set( 0, 0 );
				crosshair.setVisible( true );
				lastX =crosshair.getPosX() + crosshair.getWidth()/2;
				lastY =crosshair.getPosY() + crosshair.getHeight()/2;

			break;

			case STATE_HIDING:
				fp_accTime = 0;
				// encerrou a jogada
				currentPlay.direction.normalize();

				currentPlay.direction.y = FP_CONTROL_HEIGHT_MIN_VALUE + NanoMath.mulFixed( FP_CONTROL_HEIGHT_MAX_VALUE - FP_CONTROL_HEIGHT_MIN_VALUE, getCrosshairPercentY() /*crosshairPercent.y*/ );
				currentPlay.direction.set( currentPlay.direction.getNormal().mul( currentPlay.fp_power ) );

				// o valor é negativo pois para pôr efeito para a direita deve-se bater à esquerda da bola e vice-versa
				currentPlay.fp_curve = -( FP_CURVE_MIN_VALUE + NanoMath.mulFixed( FP_CURVE_MAX_VALUE - FP_CURVE_MIN_VALUE, getCrosshairPercentX() /*crosshairPercent.x*/ ) );
			break;

			case STATE_DONE:
				setVisible( false );
			break;
		}
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		totaltime += delta;

		// converte o tempo de milisegundos para segundos
		final int fp_delta = NanoMath.divInt( delta, 1000 );

		switch ( currentControl ) {
			case STATE_DIRECTION_POWER_APPEAR:
				fp_accTime -= delta;

				setPowerBarPosition( -powerBar.getWidth() * fp_accTime / TIME_APPEAR, powerBar.getPosY() );

				if ( fp_accTime <= 0 ) {
					setState( STATE_DIRECTION_POWER );
				}
			break;
			
			case STATE_DIRECTION_POWER:
				fp_accTime += fp_delta;

				fpCurrentValue = FP_CONTROL_POWER_MIN_VALUE + NanoMath.divFixed( NanoMath.mulFixed( fp_accTime, FP_CONTROL_POWER_MAX_VALUE - FP_CONTROL_POWER_MIN_VALUE ), FP_POWER_CONTROL_TIME );
				if ( fpCurrentValue >= FP_CONTROL_POWER_MAX_VALUE ) {
					fp_accTime = 0;
				}

				fpCurrentValue = NanoMath.clamp( fpCurrentValue, FP_CONTROL_POWER_MIN_VALUE, FP_CONTROL_POWER_MAX_VALUE );

				final int diff = NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( FP_CONTROL_POWER_MAX_VALUE - fpCurrentValue, NanoMath.toFixed( powerBar.getHeight() ) ),
										   FP_CONTROL_POWER_MAX_VALUE - FP_CONTROL_POWER_MIN_VALUE ) ); //((cpMax - cv) * pbar.h) / (cpMax - cpMin)
				powerLevelViewport.set( 0, powerLevel.getPosY() + diff, getWidth(), powerLevel.getHeight() );

				switch ( directionMode ) {
					case DIRECTION_MODE_CLOCKWISE:
						directionAngle = ( short ) ( ( directionAngle + directionMUV.updateInt( delta ) ) % 360 );
						refreshArrow();
					break;

					case DIRECTION_MODE_COUNTER_CLOCKWISE:
						directionAngle = ( short ) ( ( directionAngle - directionMUV.updateInt( delta ) ) % 360 );
						refreshArrow();
					break;
				}
			break;

			case STATE_HEIGHT_CURVE_APPEAR:
				fp_accTime += delta;

				ballBig.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 
											-ScreenManager.SCREEN_HALF_HEIGHT + ( fp_accTime * ScreenManager.SCREEN_HEIGHT / TIME_APPEAR ) );
                
                //#if DEBUG == "true"
                lbltest.setPosition(ballBig.getPosX(), ballBig.getPosY()-40);
                //#endif

				offset = ( short ) ( getWidth() * fp_accTime / TIME_APPEAR );

				if ( fp_accTime >= TIME_APPEAR ) {
					setState( STATE_HEIGHT_CURVE );
				}
			break;

			case STATE_HEIGHT_CURVE:
				lastX = crosshair.getPosX()+crosshair.getWidth()/2;
				lastY = crosshair.getPosY()+crosshair.getHeight()/2;
				crosshairPercent.x = NanoMath.clamp( crosshairPercent.x + NanoMath.mulFixed( crosshairSpeed.x, fp_delta ), 0, NanoMath.ONE );
				crosshairPercent.y = NanoMath.clamp( crosshairPercent.y + NanoMath.mulFixed( crosshairSpeed.y, fp_delta ), 0, NanoMath.ONE );
                crosshairError.x = NanoMath.mulFixed(NanoMath.cosInt((totaltime)),MAX_ERROR);
                crosshairError.y = NanoMath.mulFixed(NanoMath.cosInt((totaltime*9)/10),MAX_ERROR);

//				final int fp_module = Math.max( NanoMath.ONE, NanoMath.sqrtFixed( NanoMath.mulFixed( crosshairPercent.x, crosshairPercent.x ) + NanoMath.mulFixed( crosshairPercent.y, crosshairPercent.y ) ) );
//
//				if ( fp_module == 0 ) {
//					crosshair.setRefPixelPosition( ballBig.getRefPixelPosition() );
//				} else {
//					final int fp_sin = NanoMath.divFixed( crosshairPercent.y, fp_module );
//					final int fp_cos = NanoMath.divFixed( crosshairPercent.x, fp_module );
//
//					final int fp_test = NanoMath.divFixed( fp_module, NanoMath.sqrtFixed( NanoMath.toFixed( 2 ) ) );
//
////					final int fp_maxX = NanoMath.mulFixed( NanoMath.toFixed( ballBig.getWidth() >> 1 ), fp_module );
////					final int fp_maxY = NanoMath.mulFixed( NanoMath.toFixed( ballBig.getHeight() >> 1 ), fp_module );
//					final int fp_maxX = NanoMath.toFixed( ballBig.getWidth() >> 1 );
//					final int fp_maxY = NanoMath.toFixed( ballBig.getHeight() >> 1 );
////					final int fp_maxX = NanoMath.mulFixed( NanoMath.toFixed( ballBig.getWidth() >> 1 ), fp_test );
////					final int fp_maxY = NanoMath.mulFixed( NanoMath.toFixed( ballBig.getHeight() >> 1 ), fp_test );
//
//					System.out.println( "x: " +  NanoMath.toString( fp_cos ) + " -> " + NanoMath.toString( fp_maxX ) + " -> " + NanoMath.toString( fp_module ) );
//					System.out.println( "y: " +  NanoMath.toString( fp_sin ) + " -> " + NanoMath.toString( fp_maxY ) + " -> " + NanoMath.toString( fp_module ) );
//	//
//					crosshair.setRefPixelPosition( ballBig.getRefPixelPosition().add( NanoMath.toInt( NanoMath.mulFixed( fp_maxX, fp_cos ) ), NanoMath.toInt( NanoMath.mulFixed( fp_maxY, fp_sin ) ) ) );
//				}

                //#if DEBUG == "true"
                lbltest.setText("("+NanoMath.toInt(NanoMath.mulFixed(getCrosshairPercentX(),NanoMath.toFixed(100)))+","+NanoMath.toInt(NanoMath.mulFixed(getCrosshairPercentY(),NanoMath.toFixed(100)))+ ")", true);
                //#endif
				crosshair.setRefPixelPosition( (ballBig.getPosX() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( ballBig.getWidth() ), getCrosshairPercentX() ))),
											   (ballBig.getPosY() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( ballBig.getHeight() ), getCrosshairPercentY() ))));

                if(crosshair.getRefPixelPosition().distanceTo(ballBig.getRefPixelPosition())>ballBig.getWidth()/2) {
                    tempPoint.x = crosshair.getRefPixelX() - ballBig.getRefPixelX();
                    tempPoint.y = crosshair.getRefPixelY() - ballBig.getRefPixelY();
                    tempInt = NanoMath.sqrtInt((tempPoint.x*tempPoint.x)+(tempPoint.y*tempPoint.y));
                    tempPoint.x = NanoMath.toFixed(tempPoint.x);
                    tempPoint.y = NanoMath.toFixed(tempPoint.y);
                    tempPoint.divEquals(tempInt);
                    tempPoint.mulEquals(ballBig.getWidth()/2);
                    crosshair.setRefPixelPosition(ballBig.getRefPixelX() + NanoMath.toInt(tempPoint.x), ballBig.getRefPixelY() + NanoMath.toInt(tempPoint.y));
                }

			break;

			case STATE_HIDING:
				fp_accTime += delta;

				setPowerBarPosition( -powerBar.getWidth() * fp_accTime / TIME_APPEAR, powerBar.getPosY() );
				ballBig.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH,
											 ScreenManager.SCREEN_HALF_HEIGHT - ( fp_accTime * ScreenManager.SCREEN_HALF_HEIGHT / TIME_APPEAR ) );
                
                //#if DEBUG == "true"
                lbltest.setPosition(ballBig.getPosX(), ballBig.getPosY()-40);
                //#endif

				offset = ( short ) ( getWidth() * ( TIME_APPEAR - fp_accTime ) / TIME_APPEAR );

				if ( fp_accTime >= TIME_APPEAR ) {
					setState( STATE_DONE );
				}

			break;
		} // fim switch ( currentControl )
	} // fim do método update( int )

    private final int getCrosshairPercentX() {
        return NanoMath.clamp((crosshairPercent.x + crosshairError.x),0,NanoMath.ONE);
    }

    private final int getCrosshairPercentY() {
        return NanoMath.clamp((crosshairPercent.y + crosshairError.y),0,NanoMath.ONE);
    }


	private final void setPowerBarPosition( int x, int y ) {
		powerBar.setPosition( x, y );
		powerLevel.setPosition( (x + POWER_LEVEL_X), (y + POWER_LEVEL_Y) );
	}


	private final void refreshArrow() {
		arrow.set( ball.getRealPosition(), getArrowDirection() );
	}


	private final Point3f getArrowDirection() {
		return new Point3f( NanoMath.mulFixed( NanoMath.ONE, NanoMath.cosInt( directionAngle ) ),
							0,
							NanoMath.mulFixed( NanoMath.ONE, NanoMath.sinInt( directionAngle ) ) );
	}
	
	
	public byte getCurrentControl() { 
		return currentControl; 
	}	
	 
	 
	public final Play getCurrentPlay() {
		return currentPlay;
	}


	public final boolean contains( int x, int y ) {
		return currentControl != STATE_DONE && currentControl != STATE_NONE && contains( x, y );
	}


	private final void setDirectionMode( byte mode ) {
		directionMode = mode;
	}


	public final void setVisible( boolean v ) {
		super.setVisible( v );
		arrow.setVisible( v );
	}


	protected void paint( Graphics g ) {
		switch ( currentControl ) {
			case STATE_HIDING:
			case STATE_HEIGHT_CURVE:
			case STATE_HEIGHT_CURVE_APPEAR:
				g.setColor( 0x000000 );
				for ( int y = 0; y < size.y; y += 3 ) {
					g.drawRect( 0, y, offset, 0 );
					g.drawRect( size.x - offset, y + 1, offset, 0 );
				}
			break;
		}
		
		super.paint( g );
	}


	public final void keyPressed( int key ) {

		switch ( currentControl ) {
			case STATE_DIRECTION_POWER:
				switch ( key ) {
					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						setDirectionMode( Control.DIRECTION_MODE_CLOCKWISE );
					break;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						setDirectionMode( Control.DIRECTION_MODE_COUNTER_CLOCKWISE );
					break;
					case ScreenManager.SOFT_KEY_LEFT:
						break;
					
					default:
						match.keyPressed(ScreenManager.FIRE);
						//setDirectionMode( Control.DIRECTION_MODE_NONE );
				}
			break;

			case STATE_HEIGHT_CURVE:
				switch (key) {
					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						crosshairSpeed.x = -FP_CROSSHAIR_SPEED;
						break;

					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						crosshairSpeed.x = FP_CROSSHAIR_SPEED;
						break;

					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						crosshairSpeed.y = -FP_CROSSHAIR_SPEED;
						break;

					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						crosshairSpeed.y = FP_CROSSHAIR_SPEED;
						break;

					default:
						match.keyPressed(ScreenManager.FIRE);


				}
				break;
		}
	}


	public final void keyReleased( int key ) {
		switch ( currentControl ) {
			case STATE_DIRECTION_POWER:
				setDirectionMode( Control.DIRECTION_MODE_NONE );
			break;

			case STATE_HEIGHT_CURVE:
				crosshairSpeed.set( 0, 0 );
			break;
		}
	}

	public final int getState() {
		return currentControl;
	}

    private final void setCrosshair(int x, int y) {
		if (ballBig.contains(x, y)) {
            crosshairPercent.x = NanoMath.divFixed(NanoMath.toFixed(x-ballBig.getPosX()),NanoMath.toFixed(ballBig.getSize().x));
            crosshairPercent.y = NanoMath.divFixed(NanoMath.toFixed(y-ballBig.getPosY()),NanoMath.toFixed(ballBig.getSize().y));
        }
    }

	public final void CameraFolowButtonShow() {
		cameraFolow.setVisible( true );
	}

	public final void CameraFolowButtonHide() {
		cameraFolow.setVisible( false );
	}

	//#if TOUCH == "true"
	public final void onPointerDragged( int x, int y ) {
		switch (currentControl) {
			case STATE_HEIGHT_CURVE:
                setCrosshair(x, y);
				break;
		}
	}

	public final void onPointerPressed(int x, int y) {
		   
		System.out.println("pressionado"+lastX + " " + lastY);

		switch (currentControl) {
			case STATE_DIRECTION_POWER:
				if (arrowR.contains(x, y)) {
					setDirectionMode(Control.DIRECTION_MODE_CLOCKWISE);
				}
                else if (arrowL.contains(x, y)) {
					setDirectionMode(Control.DIRECTION_MODE_COUNTER_CLOCKWISE);
				}
				if (GameMIDlet.softkeyLeft.contains(x, y)) {
					setState(STATE_HEIGHT_CURVE_APPEAR);
				}
			break;

			case STATE_HEIGHT_CURVE:
				/*if (GameMIDlet.softkeyLeft.contains(x, y)) {
					match.keyPressed(ScreenManager.FIRE);
				}*/
				setCrosshair(x,y);
			break;
		}
	}

	public final void onPointerReleased(int x, int y) {
		switch (currentControl) {
			case STATE_DIRECTION_POWER:
				setDirectionMode(Control.DIRECTION_MODE_NONE);
                //match.setState(Match.CAMERA_FOLLOW_BALL);
                if(cameraFolow.contains(x, y)) {
                    CameraFolowButtonHide();
                    match.setCameraStateFollowBall();
                }
			break;

			case STATE_HEIGHT_CURVE:
				/*keyReleased(ScreenManager.RIGHT);
				keyReleased(ScreenManager.UP);
				keyReleased(ScreenManager.LEFT);
				keyReleased(ScreenManager.DOWN);*/
			break;
		}
	}
		
	//#endif

	
}
