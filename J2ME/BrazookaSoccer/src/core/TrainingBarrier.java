/*
 * TrainingBarrier.java
 *
 * Created on 30 de Agosto de 2007, 23:20
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import screens.GameMIDlet;
import screens.Match;


/**
 *
 * @author peter
 */
public final class TrainingBarrier extends PlayerBarrier implements Constants {
	
	
	public TrainingBarrier( Match match ) throws Exception {
		super( match, true  );
	}


	/**
	 * Atualiza o frame da imagem da barreira no modo treino, de modo a estar virada para 
	 * a posição da falta.
	 */
	 public final void updateFrame() {
		byte currentFrame;
		byte mirror = TRANS_NONE;

		switch ( direction ) {
			case ISO_DIRECTION_RIGHT:
				currentFrame = 1;
				mirror = TRANS_MIRROR_H;
			break;
			
			case ISO_DIRECTION_DOWN_RIGHT:
				currentFrame = 0;
			break;

			case ISO_DIRECTION_DOWN:
				currentFrame = 1;
			break;

			case ISO_DIRECTION_DOWN_LEFT:
				currentFrame = 2;
			break;

			case ISO_DIRECTION_LEFT:
				currentFrame = 2;
			break;
			
			default:
				return;
		} // fim switch ( direction )

		for ( byte i = 0; i < BARRIER_MAX_N_PLAYERS; ++i ) {
			players[ i ].setFrame( currentFrame );
			players[ i ].setTransform( mirror );
		}
	} // fim do método updateFrame()

}
