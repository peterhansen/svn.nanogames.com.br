/**
 * ReplayControl.java
 * 
 * Created on 31/Dez/2009, 9:06:02
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;
import screens.Match;

/**
 *
 * @author Peter
 */
public final class ReplayControl extends UpdatableGroup implements Constants {
	
	/** multiplicador do tempo atual do replay */
	private int fp_replayMultiplier;

	// indica se o replay está paralisado
	private boolean paused;

	/** Ã?cone do replay. */
	private static Sprite replayIcon;

	private static final byte REPLAY_ICON_SEQUENCE_NONE				= 0;
	private static final byte REPLAY_ICON_SEQUENCE_APPEARING		= 1;
	private static final byte REPLAY_ICON_SEQUENCE_HIDING			= 2;
	private static final byte REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR	= 3;

	public static final byte STATE_ALL_HIDDEN			= 0;
	public static final byte STATE_PARTIAL_HIDDEN		= 1;
	public static final byte STATE_APPEARING_PARTIAL	= 2;
	public static final byte STATE_APPEARING_FULL		= 3;
	public static final byte STATE_SHOWN				= 4;
	public static final byte STATE_HIDING_FULL			= 5;
	public static final byte STATE_HIDING_PARTIAL		= 6;

	/** Estado atual dos controels do replay. */
	private byte state = -1;

	/** Velocidade atual da animaçao de entrada/saída dos controles do replay. */
	private final MUV replayControlSpeed = new MUV();

	/** Mudança na velocidade do replay a cada tecla pressionada. */
	public static final int FP_REPLAY_SPEED_STEP = NanoMath.divInt( 25, 100 );

	private static final int FP_REPLAY_MAX_SPEED = NanoMath.toFixed( 4 );
	private static final int FP_REPLAY_MIN_SPEED = NanoMath.divInt( 5, 100 );

	private static final byte REPLAY_HIDE_ICON_FRAME_HIDE = 0;
	private static final byte REPLAY_HIDE_ICON_FRAME_SHOW = 1;
	private final DrawableImage frame;
	private final DrawableImage rec;
	private final DrawableImage rewind;
	private final DrawableImage play;
	private final DrawableImage pause;
	private final DrawableImage fast;
	private final DrawableImage slow;
	private final Match match;

    //private final byte[] POSITIONS_X = { 8, 27, 26, 30, 0, 57 };
    //private final byte[] POSITIONS_Y = { 14, 0, 30, 60, 33, 31 };
    private final byte[] POSITIONS_X = { 10, 29, 28, 32, 2, 59 };
    private final byte[] POSITIONS_Y = { 16, 2, 32, 62, 35, 33 };


	
	public ReplayControl( boolean viewOnly , Match match) throws Exception {
		super( 7 );
		this.match = match;
		
		//final DrawableImage imgControlLeft = new DrawableImage( PATH_IMAGES + "rc.png" );
		//setSize( replayControlRightSave.getPosX() + replayControlRightSave.getWidth(),
		//						imgControlLeft.getPosY() + imgControlLeft.getHeight() );

        frame = new DrawableImage(PATH_IMAGES + "frame.png");
		rec = new DrawableImage(PATH_IMAGES + "rec.png");
		rewind = new DrawableImage(PATH_IMAGES + "rewind.png");
		play = new DrawableImage(PATH_IMAGES + "play.png");
		pause = new DrawableImage(PATH_IMAGES + "pause.png");
		fast = new DrawableImage(PATH_IMAGES + "+.png");
		slow = new DrawableImage(PATH_IMAGES + "-.png");
        Pattern p = new Pattern(0x000000);
		//insertDrawable( p );
		insertDrawable( frame );
		insertDrawable( rec );
		insertDrawable( play );
		insertDrawable( pause );
		insertDrawable( slow );
		insertDrawable( rewind );
		insertDrawable( fast );
		pause.setVisible(false);
		//p.setSize(slow.getWidth()+play.getWidth()+fast.getWidth(), rec.getHeight()+play.getHeight()+rewind.getHeight());

		setSize(82, 82);
	}
	
	
	public final void setPaused( boolean paused ) {
        pause.setVisible(!paused);
		this.paused = paused;
	}
	
	
	public final boolean isPaused() {
		return paused;
	}
	
	
	public final int getSpeedMultiplier() {
		return fp_replayMultiplier;
	}

	public final void setSize(int width, int height) {
		super.setSize(width, height);

		frame.setPosition( POSITIONS_X[0] , POSITIONS_Y[0] );
		rec.setPosition (  POSITIONS_X[1] , POSITIONS_Y[1] );
		play.setPosition(  POSITIONS_X[2] , POSITIONS_Y[2] );
		pause.setPosition( play.getPosX() , play.getPosY() );
		rewind.setPosition(POSITIONS_X[3] , POSITIONS_Y[3] );
		slow.setPosition(  POSITIONS_X[4] , POSITIONS_Y[4] );
		fast.setPosition(  POSITIONS_X[5] , POSITIONS_Y[5] );
	}

	public final void toggleVisibility() {
		switch ( state ) {
			case STATE_APPEARING_PARTIAL:
			case STATE_APPEARING_FULL:
			case STATE_SHOWN:
				setState( STATE_HIDING_PARTIAL );
			break;

			case STATE_ALL_HIDDEN:
			case STATE_PARTIAL_HIDDEN:
			case STATE_HIDING_FULL:
			case STATE_HIDING_PARTIAL:
				setState( STATE_APPEARING_FULL );
			break;
		}
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
	
	switch ( state ) {
		case STATE_APPEARING_FULL:
			case STATE_APPEARING_PARTIAL:
                move( 0, replayControlSpeed.updateInt( delta ) );

				//#if SCREEN_SIZE == "SMALL"
//# 				final int Y_LIMIT = size.y - ( state == REPLAY_CONTROLS_STATE_APPEARING_FULL ? getHeight() : replayHideIcon.getHeight() );
//# 			////#else
//# 				//	final int Y_LIMIT = size.y - ( state == STATE_APPEARING_FULL ? getHeight() : replaySpeedAnim.getPosY() + replaySpeedAnim.getHeight() );
				//#endif

				//if ( getPosY() <= Y_LIMIT )
				//	setState( state == STATE_APPEARING_FULL ? STATE_SHOWN : STATE_PARTIAL_HIDDEN );
			break;

			case STATE_HIDING_PARTIAL:
			case STATE_HIDING_FULL:
				move( 0, replayControlSpeed.updateInt( delta ) );

				//#if SCREEN_SIZE == "SMALL"
//# 				final int LIMIT_Y = size.y - ( state == REPLAY_CONTROLS_STATE_HIDING_PARTIAL ? replayHideIcon.getHeight() : 0 );
				//#else
				//	final int LIMIT_Y = size.y - ( state == STATE_HIDING_PARTIAL ?
					//								replaySpeedAnim.getPosY() + replaySpeedAnim.getHeight() : 0 );
				//#endif

			//	if ( getPosY() >= LIMIT_Y )
				//	setState( state == STATE_HIDING_PARTIAL ? STATE_PARTIAL_HIDDEN : STATE_ALL_HIDDEN );
			break;
		}
	}
	
	
	/**
	 * Define o multiplicador de velocidade do replay.
	 * @param fp_multiplier multiplicador de velocidade do replay, em notação de ponto fixo.
	 */
	public final void setSpeed( int fp_multiplier ) {
		fp_replayMultiplier = NanoMath.clamp( fp_multiplier, FP_REPLAY_MIN_SPEED, FP_REPLAY_MAX_SPEED );
		//#if DEBUG == "true"
			System.out.println( "REPLAY SPEED: " + NanoMath.toString( fp_replayMultiplier ) );
		//#endif
	}


	public final void setState( byte state ) {
	switch ( state ) {
			case STATE_PARTIAL_HIDDEN:
				//#if SCREEN_SIZE == "SMALL"
//# 				setPosition( 0, size.y - replayHideIcon.getHeight() );
				//#else
				//	setPosition( 0, size.y - replaySpeedAnim.getPosY() - replaySpeedAnim.getHeight() );
				//#endif
			break;

			case STATE_ALL_HIDDEN:
				setPosition( 0, size.y );
			break;

			case STATE_SHOWN:
				setPosition( 0, size.y - getHeight() );
			break;

			case STATE_APPEARING_PARTIAL:
				switch ( state ) {
					case STATE_APPEARING_PARTIAL:
					case STATE_PARTIAL_HIDDEN:
					return;
				}
			//	replayControlSpeed.setSpeed( -replayHideIcon.getHeight() );
		break;

			case STATE_APPEARING_FULL:
				switch ( state ) {
					case STATE_APPEARING_FULL:
					case STATE_SHOWN:
					return;
				}
				replayControlSpeed.setSpeed( -getHeight() );
			break;

			case STATE_HIDING_FULL:
				switch ( state ) {
					case STATE_ALL_HIDDEN:
					case STATE_HIDING_FULL:
					return;
				}
				replayControlSpeed.setSpeed( getHeight() );
			break;

			case STATE_HIDING_PARTIAL:
				switch ( state ) {
					case STATE_PARTIAL_HIDDEN:
					case STATE_HIDING_PARTIAL:
					return;
				}
				replayControlSpeed.setSpeed( getHeight() );
			break;
		} // fim switch ( state )

		setVisible( state != STATE_ALL_HIDDEN );
		this.state = state;

		switch ( state ) {
			case STATE_APPEARING_FULL:
			case STATE_APPEARING_PARTIAL:
			case STATE_SHOWN:
			//	replayHideIcon.setFrame( REPLAY_HIDE_ICON_FRAME_HIDE );
			break;

			case STATE_HIDING_FULL:
			case STATE_HIDING_PARTIAL:
			case STATE_PARTIAL_HIDDEN:
			//	replayHideIcon.setFrame( REPLAY_HIDE_ICON_FRAME_SHOW );
			break;
		}
	}
	

	private final void setReplayIconSequence( byte sequence ) {
		switch ( replayIcon.getSequenceIndex() ) {
			case REPLAY_ICON_SEQUENCE_NONE:
				switch ( sequence ) {
					case REPLAY_ICON_SEQUENCE_NONE:
					case REPLAY_ICON_SEQUENCE_HIDING:
						replayIcon.setVisible( false );
					return;
				}
			break;

			case REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR:
			case REPLAY_ICON_SEQUENCE_APPEARING:
				if ( sequence == REPLAY_ICON_SEQUENCE_APPEARING )
					sequence = REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR;
			break;
		}
		replayIcon.setVisible( sequence != REPLAY_ICON_SEQUENCE_NONE );
		replayIcon.setSequence( sequence );
	}

//#if TOUCH == "true"
	public final void onPointerReleased( int x, int y ) {
		
	}

	public final void onPointerPressed( int x, int y ) {
        if (rec.contains(x, y- rec.getHeight())) {
            match.keyPressed(ScreenManager.UP);
        } else
        if (rewind.contains(x, y- rec.getHeight())) {
            match.keyPressed(ScreenManager.DOWN);
        } else
        if (slow.contains(x, y- rec.getHeight())) {
            match.keyPressed(ScreenManager.LEFT);
        } else
        if (fast.contains(x, y - rec.getHeight())) {
            match.keyPressed(ScreenManager.RIGHT);
        } else
        if (play.contains(x, y - rec.getHeight())) {
            match.keyPressed(ScreenManager.FIRE);
        }
	}
	//#endif
}
