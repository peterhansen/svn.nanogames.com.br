/*
 * GenericIsoPlayer.java
 *
 * Created on 22 de Agosto de 2007, 00:15
 *
 */

package core;

import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import screens.Match;


/**
 *
 * @author peter
 */
public abstract class GenericIsoPlayer extends UpdatableGroup implements Constants {
	
	// limites das tangentes dos ângulos para realizar a rotação dos jogadores
	// os valores referem-se às tangentes de 30 e 60 graus.
	protected static final int FP_ANGLE_LOW		= 38011;
	protected static final int FP_ANGLE_HIGH	= 113377;
	
	// direção para a qual o jogador está virado
	protected byte direction;

	// estados do jogador
	public static final byte PLAYER_STOPPED		= 0;	// parado
	public static final byte PLAYER_RUNNING		= 1;	// correndo
	public static final byte PLAYER_SHOOTING	= 2;	// está chutando a bola
	public static final byte PLAYER_SHOT		= 3;	// já chutou a bola
	public static final byte PLAYER_JUMPING		= 4;	// pulando

	// estado atual do jogador
	protected byte state;

	// retângulo que define a área de colisão no espaço 3d
	protected final Quad collisionArea = new Quad();

	// indica se o teste de colisão com a bola deve ser feito
	protected boolean checkBallCollision;

	// posição real do jogador
	protected final Point3f realPosition = new Point3f();

	protected final Match match;

	
	public GenericIsoPlayer( Match match, int slots ) throws Exception {
		super( slots );
		this.match = match;
	}
	
	
	public final Point3f getRealPosition() {
		return realPosition;
	}


	public final void reset() {
		reset( null );
	}
	
	
	public abstract void reset( Point3f position );

	
	// define a posição real do jogador
	public final void setRealPosition( Point3f realPosition ) {
		this.realPosition.set( realPosition );
		refreshScreenPosition();
	}

	
	public final void lookAt() {
		lookAt( null, false );
	}
	
	
	public final void lookAt( Point3f position ) {
		lookAt( position, false );
	}


	public final Quad getCollisionArea() {
		return collisionArea;
	}
	
	
	// faz o jogador virar de frente para a posição "position"
	public final void lookAt( Point3f position, boolean forceFrameUpdate ) {
		if ( position == null )
			position = new Point3f();
		
		final Point3f distance = realPosition.sub( position );
		final Point3f normalDistance = distance.getNormal();
		int fp_angle = Math.abs( NanoMath.divFixed( normalDistance.z, normalDistance.x == 0 ? NanoMath.divInt( 1, 10 ) : normalDistance.x ) );
		byte oldDirection = direction;

		if ( distance.z >= 0.0 ) { // ISO_DIRECTION_LEFT/ISO_DIRECTION_UP_LEFT/ISO_DIRECTION_UP/ISO_DIRECTION_UP_RIGHT/ISO_DIRECTION_RIGHT
			if ( distance.x <= 0.0 ) { // ISO_DIRECTION_UP/ISO_DIRECTION_UP_RIGHT/ISO_DIRECTION_RIGHT
				if ( fp_angle >= FP_ANGLE_HIGH )
					direction = ISO_DIRECTION_UP;
				else if ( fp_angle <= FP_ANGLE_LOW )
					direction = ISO_DIRECTION_RIGHT;
				else
					direction = ISO_DIRECTION_UP_RIGHT;
			} // fim if ( distance.x >= 0 )
			else { // x < 0			ISO_DIRECTION_UP/ISO_DIRECTION_UP_LEFT/ISO_DIRECTION_LEFT
				if ( fp_angle >= FP_ANGLE_HIGH )
					direction = ISO_DIRECTION_UP_LEFT;
				else if ( fp_angle <= FP_ANGLE_LOW )
					direction = ISO_DIRECTION_LEFT;
				else
					direction = ISO_DIRECTION_UP_LEFT;
			}
		} // fim if ( distance.z > 0 )
		else { // z < 0				ISO_DIRECTION_LEFT/ISO_DIRECTION_DOWN_LEFT/ISO_DIRECTION_DOWN/ISO_DIRECTION_DOWN_RIGHT/ISO_DIRECTION_RIGHT
			if ( distance.x <= 0 ) { // ISO_DIRECTION_DOWN/ISO_DIRECTION_DOWN_RIGHT/ISO_DIRECTION_RIGHT
				if ( fp_angle >= FP_ANGLE_HIGH )
					direction = ISO_DIRECTION_DOWN;
				else if ( fp_angle <= FP_ANGLE_LOW )
					direction = ISO_DIRECTION_RIGHT;
				else
					direction = ISO_DIRECTION_DOWN_RIGHT;
			}
			else { // x < 0			ISO_DIRECTION_DOWN/ISO_DIRECTION_DOWN_LEFT/ISO_DIRECTION_LEFT
				if ( fp_angle >= FP_ANGLE_HIGH )
					direction = ISO_DIRECTION_DOWN;
				else if ( fp_angle <= FP_ANGLE_LOW )
					direction = ISO_DIRECTION_LEFT;
				else
					direction = ISO_DIRECTION_DOWN_LEFT;
			}
		} // fim else ( distance.z <= 0 ) 

		if ( forceFrameUpdate || oldDirection != direction )
			updateFrame(); // atualiza o frame do jogador		
	}


	// função utilitária para atualizar a posição do jogador na tela.
	protected abstract void refreshScreenPosition();

	
	// atualiza o frame atual do jogador, de acordo com a direção para a qual está virado (classes que extendem
	// essa devem implementá-la pois cada tipo de jogador possui animações específicas)
	protected abstract void updateFrame();


	// define se o teste de colisão com a bola deve ser feito ou não
	public final void setBallCollisionTest( boolean checkCollision ) {
		checkBallCollision = checkCollision;
	}

	
	// retorna se o teste de colisão com a bola será feito ou não
	public final boolean isBallCollisionTest() {
		return checkBallCollision;
	}


	public final byte getState() {
		return state;
	}


	public void setState( byte state ) {
		this.state = state;
	}





}
