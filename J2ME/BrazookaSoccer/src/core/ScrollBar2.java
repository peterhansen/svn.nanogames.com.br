/**
 * ScrollBar2.java
 * 
 * Created on Mar 8, 2009, 6:35:50 PM
 *
 */


package core;

import br.com.nanogames.components.Drawable;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class ScrollBar2 extends br.com.nanogames.components.userInterface.form.ScrollBar {

	/** Dimensão mínima (altura ou largura) da barra de scroll. */
	private static final byte MIN_DIMENSION = 5;

	
	public ScrollBar2() throws Exception {
		super( 3, TYPE_VERTICAL );

		barFull = GameMIDlet.getScrollFull();
		insertDrawable( barFull );
		barPage = GameMIDlet.getScrollPage();
		insertDrawable( barPage );

		size.set( barFull.getWidth(), barPage.getHeight() );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		barPage.setSize( barPage.getWidth(), height );
		barFull.setSize( barFull.getWidth(), height );
	}
	

	public final void refreshScroll( int currentValue, int currentPosition, int maxValue ) {
		barPage.setSize( barPage.getWidth(), Math.max( MIN_DIMENSION, ( currentValue * getHeight() / maxValue ) + 1 ) );
		barPage.setPosition( barPage.getPosX(), currentPosition * getHeight() / maxValue );
	}

}

