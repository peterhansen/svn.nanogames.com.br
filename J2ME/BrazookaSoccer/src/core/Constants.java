/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/*******************************************************************************************
	 *                              DEFINIÇÕES GENÉRICAS                                       *
	 *******************************************************************************************/
	
	/** Nome curto do jogo, para submiss�o ao Nano Online. */
	public static final String APP_SHORT_NAME = "BRAZ";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static String URL_DEFAULT = "http://lux.supp.com.br/amigo";
	
	/** Caminho das imagens do jogo. */
	public static String PATH_IMAGES = "/";

	public static String PATH_BKG = PATH_IMAGES + "bkg/";
	
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho dos sons do jogo. */
	public static String PATH_SOUNDS = "/";

	/** Caminho das imagens das bordas. */
	public static String PATH_BORDER = PATH_IMAGES + "border/";

	public static String PATH_SCROLL = PATH_IMAGES + "scroll/";
	
	public static String PATH_MENU = PATH_IMAGES + "menu/";
	
	/***/
	public static final int RANKING_TYPE_SCORE = 0;

	/** Nome da base de dados. */
	public static String DATABASE_NAME = "N";
	
	/** �?ndice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �?ndice do slot de gravação de um campeonato salvo. */
	public static final byte DATABASE_SLOT_HIGH_SCORES = 2;

	/** �?ndice do slot de gravação dos replays. */
	public static final byte DATABASE_SLOT_REPLAYS = 3;
	
	/** �?ndice do slot de gravação do idioma. */
	public static final byte DATABASE_SLOT_LANGUAGE = 4;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 4;

	public static final byte BACKGROUND_TYPE_SPECIFIC = 0;

	public static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;

	public static final byte BACKGROUND_TYPE_NONE = 2;
	
	/** Cor padrão do fundo de tela. */
	public static final int COLOR_BACKGROUND = 0xe9a700;

	public static final int COLOR_BLUE				= 0x00878E;
	public static final int COLOR_BLUE_DARK			= 0x0C4B4C;
	public static final int COLOR_BLUE_DARKER		= 0x0c2c2e;
	public static final int COLOR_ORANGE			= 0xFF7E00;
	public static final int COLOR_ORANGE_DARK		= 0xE9A700;
	public static final int COLOR_ORANGE_DARKER		= 0x745200;

	/***/
	public static final int COLOR_INFOBOX = 0x01644b;

	public static final int COLOR_SCROLL_BACK = 0xf7f0d4;
	public static final int COLOR_SCROLL_FORE = 0x945b01;

	public static final byte VIBRATION_SCREEN_OFFSET = 6;
	public static final byte VIBRATION_SCREEN_HALF_OFFSET = VIBRATION_SCREEN_OFFSET >> 1;

	/** Duração da animação de transição das soft keys, em milisegundos. */
	public static final short TIME_SOFTKEY_TRANSITION = 300;
	
	public static final byte LANGUAGE_ENGLISH		= 0;
	public static final byte LANGUAGE_SPANISH		= 1;
	public static final byte LANGUAGE_PORTUGUESE	= 2;

	public static final byte LANGUAGE_DEFAULT		= LANGUAGE_ENGLISH;
	
	/** �?ndice da fonte padrão do jogo. */
	public static final byte FONT_MENU	= 0;

	/** �?ndice da fonte utilizada para textos. */
	public static final byte FONT_TEXT	= 1;

	/** �?ndice da fonte utilizada para textos. */
	public static final byte FONT_SMALL	= 2;

	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 3;
	
	//índices dos sons do jogo
	public static final byte SOUND_INDEX_SPLASH		= 0;
	public static final byte SOUND_INDEX_WHISTLE	= 1;
	public static final byte SOUND_INDEX_GOAL		= 2;
	public static final byte SOUND_INDEX_POST		= 3;
	public static final byte SOUND_INDEX_CHAMPION	= 4;
	public static final byte SOUND_INDEX_LOSE_CUP	= 5;
	public static final byte SOUND_INDEX_KICK_BALL	= 6;
	public static final byte SOUND_INDEX_BOARD		= 7;

	public static final byte SOUND_TOTAL = SOUND_INDEX_BOARD + 1;

	//#ifdef VIBRATION_BUG
//# 		/** Duração padrão da vibração, em milisegundos. */
//# 		public static final short VIBRATION_TIME_DEFAULT = 150;
//#
//# 		/***/
//# 		public static final short VIBRATION_TIME_EXPLOSION = 200;
	//#else
		/** Duração padrão da vibração, em milisegundos. */
		public static final short VIBRATION_TIME_DEFAULT = 350;

		/***/
		public static final short VIBRATION_TIME_EXPLOSION = 500;
	//#endif
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_HIGH_SCORES					= TEXT_OPTIONS + 1;
	public static final byte TEXT_PAUSE							= TEXT_HIGH_SCORES + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_RECOMMEND_SENT				= TEXT_CONTROLS + 1;
	public static final byte TEXT_RECOMMEND_TEXT				= TEXT_RECOMMEND_SENT + 1;
	public static final byte TEXT_TIPS							= TEXT_RECOMMEND_TEXT + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_REPLAY_TITLE					= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_SCORE_TARGET					= TEXT_REPLAY_TITLE + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_SCORE_TARGET + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_BRAND					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_BRAND + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_CLEAR							= TEXT_YES + 1;
	public static final byte TEXT_NO							= TEXT_CLEAR + 1;
	public static final byte TEXT_VERSION						= TEXT_NO + 1;
	public static final byte TEXT_RECOMMEND_SMS					= TEXT_VERSION + 1;
	public static final byte TEXT_RECOMMEND_TITLE				= TEXT_RECOMMEND_SMS + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_RECOMMEND_TITLE + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT					= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_NEW_PERSONAL_RECORD			= TEXT_GAME_OVER + 1;
	public static final byte TEXT_NEW_LOCAL_RECORD				= TEXT_NEW_PERSONAL_RECORD + 1;
	public static final byte TEXT_NEW_PERSONAL_LOCAL_RECORD		= TEXT_NEW_LOCAL_RECORD + 1;
	public static final byte TEXT_LEVEL							= TEXT_NEW_PERSONAL_LOCAL_RECORD + 1;
	public static final byte TEXT_FOUL_CHALLENGE				= TEXT_LEVEL + 1;
	public static final byte TEXT_TRAINING						= TEXT_FOUL_CHALLENGE + 1;
	public static final byte TEXT_SAVE_REPLAY					= TEXT_TRAINING + 1;
	public static final byte TEXT_LOAD_REPLAY					= TEXT_SAVE_REPLAY + 1;
	public static final byte TEXT_SAVE							= TEXT_LOAD_REPLAY + 1;
	public static final byte TEXT_LOAD							= TEXT_SAVE + 1;
	public static final byte TEXT_REPLAY_NUMBER					= TEXT_LOAD + 1;
	public static final byte TEXT_EMPTY							= TEXT_REPLAY_NUMBER + 1;
	public static final byte TEXT_GOAL							= TEXT_EMPTY + 1;
	public static final byte TEXT_POST_BACK						= TEXT_GOAL + 1;
	public static final byte TEXT_POST_GOAL						= TEXT_POST_BACK + 1;
	public static final byte TEXT_POST_OUT						= TEXT_POST_GOAL + 1;
	public static final byte TEXT_OUT							= TEXT_POST_OUT + 1;
	public static final byte TEXT_DEFENDED						= TEXT_OUT + 1;
	public static final byte TEXT_PLAY_RESULT_GOAL				= TEXT_DEFENDED + 1;
	public static final byte TEXT_PLAY_RESULT_POST				= TEXT_PLAY_RESULT_GOAL + 1;
	public static final byte TEXT_PLAY_RESULT_OUT				= TEXT_PLAY_RESULT_POST + 1;
	public static final byte TEXT_PLAY_RESULT_DEFENDED			= TEXT_PLAY_RESULT_OUT + 1;
	public static final byte TEXT_PLAY_RESULT_BARRIER 			= TEXT_PLAY_RESULT_DEFENDED + 1;
	public static final byte TEXT_CONFIRM_OVERWRITE				= TEXT_PLAY_RESULT_BARRIER + 1;
	public static final byte TEXT_REPLAY_SAVED					= TEXT_CONFIRM_OVERWRITE + 1;
	public static final byte TEXT_SUNDAY						= TEXT_REPLAY_SAVED + 1;
	public static final byte TEXT_MONDAY						= TEXT_SUNDAY + 1;
	public static final byte TEXT_TUESDAY						= TEXT_MONDAY + 1;
	public static final byte TEXT_WEDNESDAY						= TEXT_TUESDAY + 1;
	public static final byte TEXT_THURSDAY						= TEXT_WEDNESDAY + 1;
	public static final byte TEXT_FRIDAY						= TEXT_THURSDAY + 1;
	public static final byte TEXT_SATURDAY						= TEXT_FRIDAY + 1;
	public static final byte TEXT_TRIES							= TEXT_SATURDAY + 1;
	public static final byte TEXT_INFO							= TEXT_TRIES + 1;
	public static final byte TEXT_TOTAL_POINTS					= TEXT_INFO + 1;
	public static final byte TEXT_POSITION_BALL					= TEXT_TOTAL_POINTS + 1;
	public static final byte TEXT_CURRENT_PROFILE				= TEXT_POSITION_BALL + 1;
	public static final byte TEXT_NO_PROFILE					= TEXT_CURRENT_PROFILE + 1;
	public static final byte TEXT_CONFIRM						= TEXT_NO_PROFILE + 1;
	public static final byte TEXT_CHOOSE_ANOTHER				= TEXT_CONFIRM + 1;
	public static final byte TEXT_VOLUME						= TEXT_CHOOSE_ANOTHER + 1;

	
	/** número total de textos do jogo */
	public static final byte TEXT_TOTAL = TEXT_VOLUME + 1;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_CHOOSE_SOUND				= 0;
	public static final byte SCREEN_SPLASH_NANO					= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_INTRO						= SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU					= SCREEN_INTRO + 1;
	public static final byte SCREEN_CONTINUE_GAME				= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_NEXT_LEVEL					= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_OPTIONS						= SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_HIGH_SCORES					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_MENU					= SCREEN_HIGH_SCORES + 1;
	public static final byte SCREEN_HELP_OBJECTIVES				= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS				= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_CREDITS						= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_PAUSE						= SCREEN_CREDITS + 1;
	public static final byte SCREEN_NANO_RANKING_MENU			= SCREEN_PAUSE + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_CONFIRM_MENU				= SCREEN_NO_PROFILE + 1;
	public static final byte SCREEN_CONFIRM_EXIT				= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1					= SCREEN_CONFIRM_EXIT + 1;	
	public static final byte SCREEN_LOADING_2					= SCREEN_LOADING_1 + 1;	
	public static final byte SCREEN_LOADING_GAME_FULL			= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_LOADING_GAME_TRAINING		= SCREEN_LOADING_GAME_FULL + 1;
	public static final byte SCREEN_RETRY						= SCREEN_LOADING_GAME_TRAINING + 1;
	public static final byte SCREEN_GAME_MENU					= SCREEN_RETRY + 1;
	public static final byte SCREEN_SAVE_REPLAY					= SCREEN_GAME_MENU + 1;
	public static final byte SCREEN_LOAD_REPLAY					= SCREEN_SAVE_REPLAY + 1;
	public static final byte SCREEN_SHOW_REPLAY					= SCREEN_LOAD_REPLAY + 1;
	public static final byte SCREEN_CHOOSE_LANGUAGE				= SCREEN_SHOW_REPLAY + 1;
	public static final byte SCREEN_LOADING_PLAY_SCREEN			= SCREEN_CHOOSE_LANGUAGE + 1;
	
	//#if DEBUG == "true"
		public static final byte SCREEN_ERROR_LOG				= SCREEN_LOADING_PLAY_SCREEN + 1;
	//#endif
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_HIGH_SCORES	= 2;
	
	//#if DEMO == "true"
	//# 	/** �?ndice da entrada do menu principal para se comprar a versão completa do jogo. */
	//# 	public static final byte ENTRY_MAIN_MENU_BUY_FULL_GAME	= 3;
	//# 	
	//# 	
	//# 	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	//# 	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	//# 	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;	
	//#endif
	
	public static final byte ENTRY_MAIN_MENU_REPLAY		= 3;
	//public static final byte ENTRY_MAIN_MENU_RECOMMEND	= 4;
	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;
	
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE NOVO JOGO">

	public static final byte ENTRY_GAME_MENU_CHALLENGE					= 0;
	public static final byte ENTRY_GAME_MENU_TRAINING					= 1;
	public static final byte ENTRY_GAME_MENU_BACK						= 2;

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	public static final byte ENTRY_PAUSE_MENU_VOLUME		        = 5;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 1;
	public static final byte  ENTRY_OPTIONS_MENU_VOLUME				    = 3;
	public static final byte ENTRY_OPTIONS_MENU_BACK			    	= 2;
			
	
	// </editor-fold>


	/*******************************************************************************************
	 *                              DEFINIÇÕES GENÉRICAS                                       *
	 *******************************************************************************************/
	// valores dos vetores para cada ângulo de inclinação do goleiro (usados para posicionar
	// corretamente a área de colisão do goleiro)
	public static final int FP_SQRT_3_DIV_2		= 54318;//NanoMath.divInt( 866025, 1000000 );
	public static final int FP_SQRT_2_DIV_2		= 22016;//NanoMath.divInt( 70710678, 100000000 );
	public static final int FP_HALF				= NanoMath.HALF;

	// comprimentos do "mundo real"
	public static final int FP_REAL_PENALTY_TO_GOAL		= 432538; // 6,6m - oficial: 11m
	public static final int FP_REAL_GOAL_WIDTH			= 436469; // oficial: 7,32m; usado: 6,66m
	public static final int FP_REAL_GOAL_WIDTH_HALF		= FP_REAL_GOAL_WIDTH >> 1; // oficial: 7,32m; usado: 6,66m
	public static final int FP_REAL_FLOOR_TO_BAR		= 159908; // oficial: 2,44m
	public static final int FP_REAL_POST_WIDTH			= 12000; // oficial: 12cm, usado: 18,3cm
	public static final int FP_REAL_BALL_RAY			= 5735; // oficial: 17,5cm
	public static final int FP_REAL_GOAL_DEPTH			= -137626; // -2,1m (profundidade máxima que a bola vai no gol)

	/** aceleração da gravidade (equivalente a 9,8m/s²) */
	public static final int FP_GRAVITY_ACCELERATION	= -642253;

	// porcentagem de desvio em rela��o � trajet�ria normal causada pela curva. Ex.: num pênalti, a bola se
	// desvia no máximo 1,1m da trajetória reta caso o fator seja 0.1 (11m * 0.1). Valor atual: 0,35
	public static final int FP_BALL_CURVE_FACTOR	= 22937;

	/** Pontuação máxima exibida. */
	public static final int SCORE_SHOW_MAX = 999999;

	/***/
	public static final short LEVEL_SHOWN_MAX = 999;

	// possíveis estados da bola
	public static final byte BALL_STATE_STOPPED				= 0;
	public static final byte BALL_STATE_MOVING				= 1;
	public static final byte BALL_STATE_GOAL				= 2;
	public static final byte BALL_STATE_OUT					= 4;
	public static final byte BALL_STATE_POST_BACK			= 8;	// bateu na trave e voltou
	public static final byte BALL_STATE_POST_GOAL			= BALL_STATE_POST_BACK | BALL_STATE_GOAL;	// bateu na trave e entrou
	public static final byte BALL_STATE_POST_OUT			= BALL_STATE_POST_BACK | BALL_STATE_OUT;	// bateu na trave e foi para fora
	public static final byte BALL_STATE_REJECTED_KEEPER		= 16;
	public static final byte BALL_STATE_REJECTED_BARRIER	= 32;

	// tipos de colisão com jogador
	public static final byte COLLISION_KEEPER = 0;
	public static final byte COLLISION_BARRIER = 1;

	// possíveis direções do modo isométrico
	public static final byte ISO_DIRECTION_NONE			= 0;
	public static final byte ISO_DIRECTION_UP			= 1;
	public static final byte ISO_DIRECTION_UP_RIGHT		= 2;
	public static final byte ISO_DIRECTION_RIGHT		= 3;
	public static final byte ISO_DIRECTION_DOWN_RIGHT	= 4;
	public static final byte ISO_DIRECTION_DOWN			= 5;
	public static final byte ISO_DIRECTION_DOWN_LEFT	= 6;
	public static final byte ISO_DIRECTION_LEFT			= 7;
	public static final byte ISO_DIRECTION_UP_LEFT		= 8;

	/** Modo de controle: segue a orientação isométrica padrão do "mundo real" do jogo. Visualmente: 2 vai para superior
	 * direita, 6 para inferior direita, 8 para inferior esquerda e 4 para superior esquerda.
	 */
	public static final byte KEY_MODE_ISO_UP_RIGHT	= 0;
	/** Modo de controle: segue a orientação absoluta da tela. Visualmente: 2 vai para cima, 6 para a direita, 8 para
	 * baixo e 4 para esquerda.
	 */
	public static final byte KEY_MODE_ABSOLUTE		= 1;
	/** Modo de controle: segue a orientação alternativa do "mundo real" do jogo. Visualmente: 2 vai para superior esquerda,
	 * 6 para superior direita, 8 para inferior direita e 4 para inferior esquerda.
	 */
	public static final byte KEY_MODE_ISO_UP_LEFT	= 2;


	// número máximo e mínimo de jogadores na barreira
	public static final byte BARRIER_MAX_N_PLAYERS = 4;
	public static final byte BARRIER_MIN_N_PLAYERS = 1;

	/** Quantidade total de jogadores diferentes na barreira. */
	public static final byte BARRIER_DIFFERENT_PLAYERS = 4;

	// dimensões da pequena área
	public static final int FP_REAL_SMALL_AREA_DEPTH = 216269;  // 3,3m - oficial: 5,5m
	public static final int FP_REAL_SMALL_AREA_DEPTH_HALF = FP_REAL_SMALL_AREA_DEPTH >> 1;
	public static final int FP_REAL_SMALL_AREA_WIDTH = 707789; // 10,8m - oficial: 18m
	public static final int FP_REAL_SMALL_AREA_WIDTH_HALF = FP_REAL_SMALL_AREA_WIDTH >> 1;

	// dimensões da grande área
	public static final int FP_REAL_BIG_AREA_DEPTH = 609485; // 9,3m - oficial: 16,5m
	public static final int FP_REAL_BIG_AREA_DEPTH_HALF = FP_REAL_BIG_AREA_DEPTH >> 1; // oficial: 16,5m
	public static final int FP_REAL_BIG_AREA_WIDTH = 1258291; // 19,2m - oficial: 40m
	public static final int FP_REAL_BIG_AREA_WIDTH_HALF = FP_REAL_BIG_AREA_WIDTH >> 1; // oficial: 40m

	// dimensões totais do campo
	public static final int FP_REAL_FIELD_WIDTH = 2516582;		// 38,4m - oficial: mínimo de 64m e máximo de 75m
	public static final int FP_REAL_FIELD_WIDTH_HALF = FP_REAL_FIELD_WIDTH >> 1;		// oficial: mínimo de 64m e máximo de 75m
	public static final int FP_REAL_FIELD_DEPTH = 3538944;	// 54m - oficial: mínimo de 100m e máximo de 110
	public static final int FP_REAL_FIELD_DEPTH_HALF = FP_REAL_FIELD_DEPTH >> 1;	// oficial: mínimo de 100m e máximo de 110


	// travessão
	public static final int FP_REAL_HALF_POST_WIDTH		= FP_REAL_POST_WIDTH >> 1;

	/** distância máxima do centro da bola ao centro de uma trave numa colisão. */
	public static final int FP_REAL_MAX_TOUCH_DISTANCE		= FP_REAL_HALF_POST_WIDTH + FP_REAL_BALL_RAY;

	public static final int FP_REAL_BAR_MIDDLE				= FP_REAL_FLOOR_TO_BAR;
	public static final int FP_REAL_BAR_BOTTOM				= FP_REAL_BAR_MIDDLE - FP_REAL_MAX_TOUCH_DISTANCE;
	public static final int FP_REAL_BAR_TOP					= FP_REAL_BAR_MIDDLE + FP_REAL_MAX_TOUCH_DISTANCE;
	// trave esquerda
	public static final int FP_REAL_LEFT_POST_MIDDLE		= -FP_REAL_GOAL_WIDTH_HALF - FP_REAL_HALF_POST_WIDTH;
	public static final int FP_REAL_LEFT_POST_START			= FP_REAL_LEFT_POST_MIDDLE + FP_REAL_MAX_TOUCH_DISTANCE;
	public static final int FP_REAL_LEFT_POST_END			= FP_REAL_LEFT_POST_MIDDLE - FP_REAL_MAX_TOUCH_DISTANCE;
	// trave direita
	public static final int FP_REAL_RIGHT_POST_MIDDLE		= -FP_REAL_LEFT_POST_MIDDLE;
	public static final int FP_REAL_RIGHT_POST_START		= FP_REAL_RIGHT_POST_MIDDLE - FP_REAL_MAX_TOUCH_DISTANCE;
	public static final int FP_REAL_RIGHT_POST_END			= FP_REAL_RIGHT_POST_MIDDLE + FP_REAL_MAX_TOUCH_DISTANCE;
	
	/** Dist�ncia real das placas de publicidade �s linhas do campo. */
	public static final int FP_REAL_BOARD_FIELD_DISTANCE			= 393216; // 6m
	/** Altura real em metros das placas de publicidade. */
	public static final int FP_REAL_BOARD_HEIGHT					= 163840; // 2,5m

	public static final int FP_REAL_BOARD_X_LEFT			= -FP_REAL_FIELD_WIDTH_HALF - FP_REAL_BOARD_FIELD_DISTANCE;
	public static final int FP_REAL_BOARD_X_RIGHT			= -FP_REAL_BOARD_X_LEFT;
	public static final int FP_REAL_BOARD_Z_MIN				= -FP_REAL_BOARD_FIELD_DISTANCE;
	public static final int FP_REAL_BOARD_Z_MAX				= FP_REAL_FIELD_DEPTH_HALF;
	

	// largura do goleiro com os braços próximos ao corpo
	public static final int FP_REAL_KEEPER_WIDTH = 65536; // 1m
	// largura do goleiro com os braços abertos
	public static final int FP_REAL_KEEPER_WIDTH_ARMS = 101580; // 1,55
	// valor mínimo do pulo do goleiro para que ele pule para cima (no caso da bola estar próxima
	// a ele horizontalmente)
	public static final int FP_REAL_KEEPER_HEIGHT = 117965; // 1,8
	// altura do goleiro com os braços esticados
	public static final int FP_REAL_KEEPER_HEIGHT_ARMS = 150733; // 2,3
	// duração mínima do pulo
	public static final int FP_MIN_JUMP_TIME = 26214; // 0,4

	public static final short REAL_WORLD_SIZE = Short.MAX_VALUE;
	public static final short REAL_WORLD_OFFSET = REAL_WORLD_SIZE >> 1;
	
	// número de tentativas que o jogador tem para acumular os pontos necessários para passar à próxima fase
	public static final int FOUL_CHALLENGE_TRIES_PER_FOUL = 3;	

	// estados da câmera
	public static final byte CAMERA_STOPPED					= 0;
	public static final byte CAMERA_FOLLOW_BALL				= 1;
	public static final byte CAMERA_GOTO_BALL_THEN_FOLLOW	= 2;
	public static final byte CAMERA_FOLLOW_PLAYER			= 3;
	public static final byte CAMERA_MOVING					= 4;
	public static final byte CAMERA_MANUAL					= 5;

	public static final int FP_CAMERA_MOVE_SPEED = 983040; // 15 em fixed

	// estados do jogo
	//public static final byte MATCH_STATE_CREATED			= 0; // nao usado
	//public static final byte MATCH_STATE_BEGIN				= 1; // nao usado
	public static final byte MATCH_STATE_POSITION_BALL		= 2;
	public static final byte MATCH_STATE_CONTROL			= 3;
	public static final byte MATCH_STATE_MOVING             = 4;
	public static final byte MATCH_STATE_PLAY_RESULT		= 5;
	public static final byte MATCH_STATE_REPLAY             = 6;
	public static final byte MATCH_STATE_REPLAY_WAIT		= 7;
	public static final byte MATCH_STATE_REPLAY_ONLY		= 8;
	public static final byte MATCH_STATE_REPLAY_ONLY_WAIT	= 9;
	public static final byte MATCH_STATE_SHOW_INFO			= 10;
	public static final byte MATCH_STATE_GAME_OVER			= 11;
	//public static final byte MATCH_STATE_NEW_RECORD		= 12; // nao usado

	// tempo padrão que a câmera leva para chegar à posição de destino
	public static final int FP_CAMERA_MOVE_DEFAULT_TIME = 78643; // 1,2
	// tempo padrão que a câmera leva para de fato chegar à posição do objeto sendo seguido
	public static final int FP_CAMERA_FOLLOW_DEFAULT_TIME = 9830; // 0,15

	public static final short SOFT_KEY_VISIBLE_TIME = 3800;

	/** Quantidade total de tipos de grama. */
	public static final byte GRASS_PATTERNS_TOTAL = 2;
	
	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPEC�?FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	public static final byte SCORE_PATTERN_WIDTH = 48;
//# 
//# 	public static final byte TITLE_SPACING = 8;
//# 
//# 	public static final byte ENTRIES_SPACING = 2;
//# 
//# 	public static final short HEIGHT_MIN = 128;
//# 
//# 	public static final short HEIGHT_DEFAULT = 128;
//# 
//# 	public static final byte GAME_INFO_SPACING = 8;
//# 
//# 	public static final byte TIMEBAR_SPACING = 4;
//# 
//# 	/** Distância padrão da parte inferior da borda à parte inferior da tela. */
//# 	public static final byte BOARD_DEFAULT_BOTTOM_Y = -9;
//# 
//#  	public static final short MIN_HEIGHT_PORTRAIT_LUX = 200;
//# 
//# 	public static final byte BORDER_THICKNESS = 3;
//# 
//# 	/** Memória mínima necessária para carregar todas as fontes do jogo. */
//# 	public static final int LOAD_ALL_FONTS_MEMORY = 1600000;
//# 
//# 	//</editor-fold>
//# 
	//#elif SCREEN_SIZE == "MEDIUM"
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPEC�?FICAS PARA TELA MÉDIA" defaultstate="collapsed">
//# 
//# 	public static final byte SCORE_PATTERN_WIDTH = 70;
//# 
//# 	public static final byte TITLE_SPACING = 10;
//# 
//# 	public static final byte ENTRIES_SPACING = 4;
//# 
//# 	public static final short HEIGHT_MIN = 190;
//# 
//# 	public static final short HEIGHT_DEFAULT = 206;
//# 
//# 	public static final byte GAME_INFO_SPACING = 8;
//# 
//# 	public static final byte TIMEBAR_SPACING = 8;
//# 
//# 	/** Distância padrão da parte inferior da borda à parte inferior da tela. */
//# 	public static final byte BOARD_DEFAULT_BOTTOM_Y = -24;
//# 
//#  	public static final short MIN_HEIGHT_PORTRAIT_LUX = 200;
//# 
//# 		public static final byte BORDER_THICKNESS = 4;
//# 
//# 	// número de pixels em x e y percorridos para cada 1 metro real percorrido no eixo x
//# 	public static final int FP_ISO_XREAL_X_OFFSET = 964344;
//# 	public static final int FP_ISO_XREAL_Y_OFFSET = FP_ISO_XREAL_X_OFFSET >> 2; // proporção 4:1
//# 	// número de pixels em x e y percorridos para cada 1 metro real percorrido no eixo z
//# 	public static final int FP_ISO_ZREAL_X_OFFSET = -990000; // -8
//# 	public static final int FP_ISO_ZREAL_Y_OFFSET = ( -FP_ISO_ZREAL_X_OFFSET ) >> 2; // 220000; // 3,62
//# 
//# 	public static final byte GOAL_BOTTOM_OFFSET_X = -52;
//# 	public static final byte GOAL_BOTTOM_OFFSET_Y = -78;
//# 	public static final byte GOAL_TOP_OFFSET_X = GOAL_BOTTOM_OFFSET_X + 3;
//# 	public static final byte GOAL_TOP_OFFSET_Y = GOAL_BOTTOM_OFFSET_Y - 3;
//# 
//# 	// altura em pixels da trave
//# 	public static final byte ISO_FLOOR_TO_BAR = 54;
//# 	public static final int FP_ISO_FLOOR_TO_BAR = 3538944;
//# 
//# 	/** Largura padrão da borda. */
//# 
//# 	/** Espaçamento do título para os itens dos menus. */
//# 
//# 	/** Espaçamento entre os itens dos menus. */
//# 
//# 	// offset na posição da meia-lua para que ela seja desenhada centralizada
//# 	public static final byte ISO_MEIA_LUA_X_OFFSET = -53;
//# 	public static final byte ISO_MEIA_LUA_Y_OFFSET = -8;
//# 
//# 	// offset na posição de desenho do círculo central
//# 	public static final byte ISO_CIRCLE_OFFSET_X = -44;
//# 	public static final byte ISO_CIRCLE_OFFSET_Y = -13;
//# 
//# 	// offset na posição de desenho das bandeiras de escanteio
//# 	public static final byte ISO_CORNER_LEFT_FLAG_OFFSET_X = -5;
//# 	public static final byte ISO_CORNER_LEFT_FLAG_OFFSET_Y = -33;
//# 	public static final byte ISO_CORNER_RIGHT_FLAG_OFFSET_X	= -14;
//# 	public static final byte ISO_CORNER_RIGHT_FLAG_OFFSET_Y	= -33;
//# 
//# 	/** Posição x do primeiro botão do replay. */
//# 	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;
//# 
//# 	/** Largura de cada botão do controle de replay. */
//# 	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;
//# 
//# 	/** Largura total de cada tag, em pixels. */
//# 	public static final short TAG_WIDTH = 140;
//# 
//# 	/** Largura mínima da ponta de uma tag. */
//# 	public static final byte TAG_MIN_OFFSET_X = 4;
//# 
//# 	/** Largura máxima da ponta de uma tag. */
//# 	public static final byte TAG_MAX_OFFSET_X = 11;
//# 
//# 	/** Deslocamento horizontal da opção selecionada num menu. */
//# 	public static final byte TAG_SELECTED_X_OFFSET = 23;
//# 
//# 	/** Espaçamento entre os ícones dos replays, em pixels. */
//# 	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;
//# 
//# 	public static final byte SPLASH_GRASS_SPEED_X = -10;
//# 
//# 	public static final short SPLASH_DEFAULT_WIDTH = 240;
//# 	public static final short SPLASH_DEFAULT_HEIGHT = 320;
//# 
//# 	public static final byte SPLASH_SKY_Y = 31;
//# 
//# 	public static final short SPLASH_PLAYER_Y = 132;
//# 
//# 	public static final short SPLASH_BALL_Y = 221;
//# 
//# 	public static final byte SPLASH_CLOUD_1_X = 5;
//# 	public static final byte SPLASH_CLOUD_1_Y = 51;
//# 
//# 	public static final byte SPLASH_CLOUD_2_X = 49;
//# 	public static final byte SPLASH_CLOUD_2_Y = 73;
//# 
//# 	public static final short SPLASH_GRASS_Y = 234;
//# 
//# 	public static final short SPLASH_SKY_BOTTOM_Y = 181;
//# 	public static final byte SPLASH_SKY_BOTTOM_HEIGHT = 77;
//# 
//# 	public static final byte SPLASH_LOGO_X = 11;
//# 	public static final byte SPLASH_LOGO_Y = 17;
//# 
//# 	public static final byte SPLASH_TITLE_X = 90;
//# 	public static final byte SPLASH_TITLE_TOP_Y = 36;
//# 	public static final byte SPLASH_TITLE_BOTTOM_Y = 65;
//# 
//# 	/** Altura do pixel de referência do goleiro. */
//# 	public static final byte KEEPER_REF_Y = 51;
//# 
//# 	/** Largura total da tela de informações. */
//# 	public static final short INFOBOX_TOTAL_WIDTH = 180;
//# 
//# 	/** Altura interna da tela de informações. */
//# 	public static final short INFOBOX_INTERNAL_HEIGHT = 174;
//# 
//# 	/** Largura do triângulo que forma a borda esquerda da tela de informações. */
//# 	public static final byte INFOBOX_EDGE_WIDTH = 20;
//# 
//# 	/** Largura do triângulo que forma a borda esquerda do background da tela de informações. */
//# 	public static final byte INFOBOX_BKG_EDGE_WIDTH = 60;
//# 
//# 	/** Posição x da barra de força. */
//# 	public static final byte POWER_LEVEL_X = 20;
//# 	/** Posição y da barra de força. */
//# 	public static final byte POWER_LEVEL_Y = 14;
//# 	//</editor-fold>
//# 
//# 
	//#elif SCREEN_SIZE == "BIG"

	//<editor-fold desc="DEFINIÇÕES ESPEC�?FICAS PARA TELA GRANDE" defaultstate="collapsed">

	// número de pixels em x e y percorridos para cada 1 metro real percorrido no eixo x
	public static final int FP_ISO_XREAL_X_OFFSET = 964344;
	public static final int FP_ISO_XREAL_Y_OFFSET = FP_ISO_XREAL_X_OFFSET >> 2; // proporção 4:1
	// número de pixels em x e y percorridos para cada 1 metro real percorrido no eixo z
	public static final int FP_ISO_ZREAL_X_OFFSET = -990000; // -8
	public static final int FP_ISO_ZREAL_Y_OFFSET = ( -FP_ISO_ZREAL_X_OFFSET ) >> 2; // 220000; // 3,62

	public static final byte GOAL_BOTTOM_OFFSET_X = -52;
	public static final byte GOAL_BOTTOM_OFFSET_Y = -78;
	public static final byte GOAL_TOP_OFFSET_X = GOAL_BOTTOM_OFFSET_X + 3;
	public static final byte GOAL_TOP_OFFSET_Y = GOAL_BOTTOM_OFFSET_Y - 3;

	// altura em pixels da trave
	public static final byte ISO_FLOOR_TO_BAR = 54;
	public static final int FP_ISO_FLOOR_TO_BAR = 3538944;

	/** Largura padrão da borda. */
	public static final byte BORDER_THICKNESS = 4;

	/** Espaçamento do título para os itens dos menus. */
	public static final byte TITLE_SPACING = 10;

	/** Espaçamento entre os itens dos menus. */
	public static final byte ENTRIES_SPACING = 4;

	// offset na posição da meia-lua para que ela seja desenhada centralizada
	public static final byte ISO_MEIA_LUA_X_OFFSET = -53;
	public static final byte ISO_MEIA_LUA_Y_OFFSET = -8;

	// offset na posição de desenho do círculo central
	public static final byte ISO_CIRCLE_OFFSET_X = -44;
	public static final byte ISO_CIRCLE_OFFSET_Y = -13;

	// offset na posição de desenho das bandeiras de escanteio
	public static final byte ISO_CORNER_LEFT_FLAG_OFFSET_X = -5;
	public static final byte ISO_CORNER_LEFT_FLAG_OFFSET_Y = -33;
	public static final byte ISO_CORNER_RIGHT_FLAG_OFFSET_X	= -14;
	public static final byte ISO_CORNER_RIGHT_FLAG_OFFSET_Y	= -33;

	/** Posição x do primeiro botão do replay. */
	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;

	/** Largura de cada botão do controle de replay. */
	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;

	/** Largura total de cada tag, em pixels. */
	public static final short TAG_WIDTH = 140;

	/** Largura mínima da ponta de uma tag. */
	public static final byte TAG_MIN_OFFSET_X = 4;

	/** Largura máxima da ponta de uma tag. */
	public static final byte TAG_MAX_OFFSET_X = 11;

	/** Deslocamento horizontal da opção selecionada num menu. */
	public static final byte TAG_SELECTED_X_OFFSET = 23;

	/** Espaçamento entre os ícones dos replays, em pixels. */
	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;

	public static final byte SPLASH_GRASS_SPEED_X = -10;

	public static final short SPLASH_DEFAULT_WIDTH = 240;
	public static final short SPLASH_DEFAULT_HEIGHT = 320;

	public static final byte SPLASH_SKY_Y = 31;

	public static final short SPLASH_PLAYER_Y = 132;

	public static final short SPLASH_BALL_Y = 221;

	public static final byte SPLASH_CLOUD_1_X = 5;
	public static final byte SPLASH_CLOUD_1_Y = 51;

	public static final byte SPLASH_CLOUD_2_X = 49;
	public static final byte SPLASH_CLOUD_2_Y = 73;

	public static final short SPLASH_GRASS_Y = 234;

	public static final short SPLASH_SKY_BOTTOM_Y = 181;
	public static final byte SPLASH_SKY_BOTTOM_HEIGHT = 77;

	public static final byte SPLASH_LOGO_X = 11;
	public static final byte SPLASH_LOGO_Y = 17;

	public static final byte SPLASH_TITLE_X = 90;
	public static final byte SPLASH_TITLE_TOP_Y = 36;
	public static final byte SPLASH_TITLE_BOTTOM_Y = 65;

	/** Altura do pixel de referência do goleiro. */
	public static final byte KEEPER_REF_Y = 51;

	/** Largura total da tela de informações. */
	public static final short INFOBOX_TOTAL_WIDTH = 180;

	/** Altura interna da tela de informações. */
	public static final short INFOBOX_INTERNAL_HEIGHT = 174;

	/** Largura do triângulo que forma a borda esquerda da tela de informações. */
	public static final byte INFOBOX_EDGE_WIDTH = 20;

	/** Largura do triângulo que forma a borda esquerda do background da tela de informações. */
	public static final byte INFOBOX_BKG_EDGE_WIDTH = 60;

	/** Posição x da barra de força. */
	public static final byte POWER_LEVEL_X = 21;
	/** Posição y da barra de força. */
	public static final byte POWER_LEVEL_Y = 15;

	//</editor-fold>

	//#endif


	public static final byte BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;
	
}
