/*
 * PlayerBarrier.java
 *
 * Created on 30 de Agosto de 2007, 23:43
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import screens.GameMIDlet;
import screens.Match;

/**
 *
 * @author peter
 */
public class PlayerBarrier extends Barrier implements Constants {
	
	private static final byte PLAYER_BARRIER_N_ITEMS = BARRIER_MAX_N_PLAYERS + 2;
	
	// distâncias que correspondem ao mínimo e máximo de jogadores presentes na barreira
	private static final int FP_DISTANCE_MAX_PLAYERS = NanoMath.toFixed( 15 );
	private static final int FP_DISTANCE_MIN_PLAYERS = NanoMath.toFixed( 35 );
	
	protected final Sprite[] players;


	public PlayerBarrier( Match match ) throws Exception {
		this( match, false );
	}

	
	public PlayerBarrier( Match match, boolean training ) throws Exception {
		super( match, PLAYER_BARRIER_N_ITEMS );

		players = new Sprite[ BARRIER_MAX_N_PLAYERS ];
		if ( training ) {
			final Sprite base = new Sprite( PATH_IMAGES + "bt" );
		for ( byte i = 0; i < BARRIER_MAX_N_PLAYERS; ++i ) {
				final Sprite player = new Sprite( base );
				players[ i ] = player;
				player.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				insertDrawable( player );
			}
		} else {
			for ( byte i = 0; i < BARRIER_MAX_N_PLAYERS; ++i ) {
				final Sprite player = new Sprite( PATH_IMAGES + "b_" + ( i % BARRIER_DIFFERENT_PLAYERS ) );
				players[ i ] = player;
				player.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				insertDrawable( player );
			}
		}
		
		//#if DEBUG == "true"
			insertDrawable( new IsoQuad( collisionArea ) );
		//#endif

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	} // fim do contrutor


	/**
	 * Prepara a barreira para uma falta cobrada a partir de "ballPosition". O número de jogadores na 
	 * barreira é definido de acordo com a distância da falta ao gol.
	 */
	public final void prepare( Point3f pBallPosition ) {
		// obtém distância da bola ao centro do gol
		int fp_distance = NanoMath.clamp( pBallPosition.getModule(), FP_DISTANCE_MAX_PLAYERS, FP_DISTANCE_MIN_PLAYERS );

		numberOfPlayers = ( byte ) ( BARRIER_MAX_N_PLAYERS - NanoMath.toInt( NanoMath.mulFixed(
					  NanoMath.divFixed( fp_distance - FP_DISTANCE_MAX_PLAYERS, FP_DISTANCE_MIN_PLAYERS - FP_DISTANCE_MAX_PLAYERS )
					, NanoMath.toFixed( BARRIER_MAX_N_PLAYERS - BARRIER_MIN_N_PLAYERS ) ) ) );

//		numberOfPlayers = BARRIER_MAX_N_PLAYERS; // TODO teste

		final Point3f currentPosition = new Point3f();
		final Point3f postPosition = new Point3f();

		// verifica qual a trave mais próxima da barreira
		postPosition.set( pBallPosition.x < 0 ? -FP_REAL_GOAL_WIDTH_HALF : FP_REAL_GOAL_WIDTH_HALF, 0, 0 );

		// vetor no eixo y
		final Point3f vectorY = new Point3f( 0, NanoMath.ONE, 0 );
		final Point3f postToBallVector = postPosition.sub( pBallPosition );
		// vetor ortogonal à linha da bola (normalizado)
		final Point3f ortho = vectorY.cross( postToBallVector ).getNormal();

		// direção de inserção dos jogadores
		final Point3f insertDirection = ortho.mul( FP_BARRIER_PLAYERS_DISTANCE );

		// define a posição do jogador-base da barreira (do jogador mais à esquerda)
		final Point3f basePosition = pBallPosition.add(
						   postToBallVector.getNormal().mul( FP_BARRIER_DISTANCE_TO_BALL ).sub(
						   // desloca a barreira de forma a proteger um pouco mais a trave
						   // mais próxima da bola
						   insertDirection.mul( NanoMath.divInt( numberOfPlayers, 10 ) ) ) );


		if ( pBallPosition.x < 0 ) {
			// define o quad de colisão
			collisionArea.setValues( basePosition.add( insertDirection.mul( NanoMath.divInt( numberOfPlayers, 10 ) ) ),
									 Quad.ANCHOR_BOTTOM_LEFT,
									 vectorY,
									 ortho.mul( -NanoMath.ONE ),
									 NanoMath.mulFixed( NanoMath.toFixed( numberOfPlayers ), FP_BARRIER_PLAYERS_DISTANCE ),
									 FP_REAL_BARRIER_HEIGHT );

			insertDirection.mulEquals( -NanoMath.ONE );
		} else {
			final int FP_NUMBER_OF_PLAYERS_09 = NanoMath.mulFixed( NanoMath.divInt( 9, 10 ), NanoMath.toFixed( numberOfPlayers ) );
			// define o quad de colisão
			collisionArea.setValues( basePosition.add( insertDirection.mul( FP_NUMBER_OF_PLAYERS_09 ) ),
									 basePosition.add( insertDirection.mul( FP_NUMBER_OF_PLAYERS_09 ).add( vectorY.mul( FP_REAL_BARRIER_HEIGHT ) ) ),
									 basePosition );
		}

		// define a posição geral da barreira como a posição do seu jogador central, para que
		// após o lookAt() todos estejam virados para a bola.
		realPosition.set( basePosition );
		int higherZ = realPosition.z; // armazena o maior z da barreira, para que o cálculo da troca de ordem z com a bola seja correto

		byte i;
		final int fp_zOrder[] = new int[ numberOfPlayers ];
		for ( i = 0; i < numberOfPlayers; ++i ) {
			final Sprite player = players[ i ];

			if ( player != null ) {
				currentPosition.set( basePosition.add( insertDirection.mul( NanoMath.toFixed( i ) ) ) );
				fp_zOrder[ i ] = currentPosition.z;

				if ( currentPosition.z > higherZ )
					higherZ = currentPosition.z;

				if ( i == 0 ) {
					fp_zOrder[ 0 ] = currentPosition.z;
				} else {
					for ( int j = 0; j < i; j++ ) {
						if ( currentPosition.z < fp_zOrder[ j ] ) {
							setDrawableIndex( j, i ); // TODO conferir setDrawableZOrder( j, i );

							int zTemp = fp_zOrder[ j ];
							fp_zOrder[ j ] = fp_zOrder[ i ];
							fp_zOrder[ i ] = zTemp;
						}
					}
				}

				player.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( currentPosition ) );
				player.setVisible( true );
			}
		}

		realPosition.z = higherZ;

		// marca os jogadores que não estarão na barreira como não-visíveis
		for ( ; i < BARRIER_MAX_N_PLAYERS; ++i ) {
			players[ i ].setVisible( false );
		}

		// força a troca de frame para evitar que alguns jogadores que não estavam presentes na última
		// falta virem para o lado errado (isso ocorria quando oldDirection era igual a direction em
		// lookAt, e o número de jogadores aumentava)
		lookAt( pBallPosition, true );

		setBallCollisionTest( true ); 

		setVisible( true );
	} // fim do método prepare()


	/**
	 * Atualiza os frames dos jogadores da barreira no modo desafio de faltas, de forma a olharem para a bola.
	 */
	public void updateFrame() {
		for ( byte i = 0; i < numberOfPlayers; ++i ) {
			byte sequence = 0;
			byte mirror = TRANS_NONE;
			
			switch ( i ) {
				case 0:
					switch ( direction ) {
						case ISO_DIRECTION_UP:
						case ISO_DIRECTION_UP_LEFT:
							sequence = 1;
							mirror = TRANS_NONE;
						break;

						case ISO_DIRECTION_UP_RIGHT:
							sequence = 1;
							mirror = TRANS_MIRROR_H;
						break;

						case ISO_DIRECTION_RIGHT:
						case ISO_DIRECTION_DOWN_RIGHT:
							sequence = 0;
							mirror = TRANS_MIRROR_H;
						break;

						case ISO_DIRECTION_DOWN:
						case ISO_DIRECTION_DOWN_LEFT:
						case ISO_DIRECTION_LEFT:
							sequence = 0;
							mirror = TRANS_NONE;
						break;
					} // fim switch ( direction )
				break;

				case 1:
				case 2:
				case 3:
					switch ( direction ) {
						case ISO_DIRECTION_UP:
							sequence = 2;
							mirror = TRANS_MIRROR_H;
						break;

						case ISO_DIRECTION_UP_RIGHT:
						case ISO_DIRECTION_RIGHT:
						case ISO_DIRECTION_DOWN:
						case ISO_DIRECTION_DOWN_LEFT:
							sequence = 0;
							mirror = TRANS_MIRROR_H;
						break;

						case ISO_DIRECTION_LEFT:
						case ISO_DIRECTION_UP_LEFT:
							sequence = 2;
							mirror = TRANS_NONE;
						break;

						case ISO_DIRECTION_DOWN_RIGHT:
							sequence = 1;
							mirror = TRANS_NONE;
						break;
					} // fim switch ( direction )
				break;
			}
				
			players[ i ].setSequence( sequence );
			players[ i ].setTransform( mirror );
		}
	} // fim do método updateFrame()


	protected final void refreshScreenPosition() {
	}

	
}
