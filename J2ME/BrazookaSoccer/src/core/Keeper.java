package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import screens.GameMIDlet;
import screens.Match;


public class Keeper extends GenericIsoPlayer implements Constants {
	// porcentagem máxima do gol que o goleiro se desloca ao se reposicionar de acordo com a barreira
	private static final int FP_MAX_GOAL_PERCENT = NanoMath.divInt( 7, 10 );

	// velocidade lateral do goleiro em metros por segundo antes do pulo (no caso de bolas distantes, para
	// posicionar o goleiro mais próximo da posição final da bola e evitar assim que goleiro fique "bobo"
	// nesse tipo de jogada)
	private static final int FP_BEFORE_JUMP_X_SPEED = NanoMath.ONE;

	// tempo mínimo da bola ao gol para que o goleiro se mova horizontalmente antes do pulo
	private static final int FP_KEEPER_MOVE_MIN_TIME = NanoMath.divInt( 11, 10 );

	// possíveis estados do pulo do goleiro
	public static final byte KEEPER_STATE_WAIT		= 0;
	public static final byte KEEPER_STATE_START		= 1;
	public static final byte KEEPER_STATE_JUMPING	= 2;
	public static final byte KEEPER_STATE_ON_FLOOR	= 3;
	public static final byte KEEPER_STATE_STOPPED	= 4;

	public static final byte SEQUENCE_CENTER_LOW	= 0;
	public static final byte SEQUENCE_CENTER_HIGH	= 1;
	public static final byte SEQUENCE_RIGHT_HIGH	= 2;
	public static final byte SEQUENCE_RIGHT_MEDIUM	= 3;
	public static final byte SEQUENCE_RIGHT_LOW		= 4;
	public static final byte SEQUENCE_LEFT_HIGH		= 5;
	public static final byte SEQUENCE_LEFT_MEDIUM	= 6;
	public static final byte SEQUENCE_LEFT_LOW		= 7;

	// índices dos frames
	public static final byte FRAME_STOPPED					= 7;

	public static final byte FRAME_CENTER_LOW				= 8;

	public static final byte FRAME_PREPARE_CENTER_HIGH		= 9;
	public static final byte FRAME_CENTER_HIGH				= 10;

	public static final byte FRAME_PREPARE_RIGHT			= 11;
	public static final byte FRAME_PREPARE_RIGHT_MID_LOW	= 12;
	public static final byte FRAME_RIGHT_HIGH_START			= 13;
	public static final byte FRAME_RIGHT_HIGH_AIR			= 16;
	public static final byte FRAME_RIGHT_HIGH_FALL			= 17;
	public static final byte FRAME_RIGHT_MIDDLE				= 14;
	public static final byte FRAME_RIGHT_FLOOR				= 15;

	public static final byte FRAME_PREPARE_LEFT				= 6;
	public static final byte FRAME_PREPARE_LEFT_MID_LOW		= 1;
	public static final byte FRAME_LEFT_HIGH_START			= 5;
	public static final byte FRAME_LEFT_HIGH_AIR			= 4;
	public static final byte FRAME_LEFT_HIGH_FALL			= 3;
	public static final byte FRAME_LEFT_MIDDLE				= 0;
	public static final byte FRAME_LEFT_FLOOR				= 2;


	// número de itens da coleção
	private static final byte ISO_KEEPER_N_ITEMS = 4;

	// valores que indicam a ordem dos frames do goleiro na imagem, e também são usados para
	// definir os possíveis tipos de pulo do goleiro
	private static final byte JUMP_TYPE_CENTER_LOW	= 0;	// pouca altura e pouco deslocamento lateral
	private static final byte JUMP_TYPE_SIDE_HIGH	= 1;	// pulando para o lado e para cima
	private static final byte JUMP_TYPE_SIDE_MEDIUM	= 2;	// pulando para o lado, altura média 
	private static final byte JUMP_TYPE_SIDE_LOW	= 3;	// pulando para o lado, baixa altura 
	private static final byte JUMP_TYPE_CENTER_HIGH	= 4;	// pulando para cima, com pouca variação na direção lateral

	// distâncias máximas que o goleiro consegue percorrer com um pulo. Os valores referem-se
	// à variação da posição dos seus pés em relação à posição original.
	private static final int FP_MAX_X_DISTANCE = NanoMath.divInt( 165, 100 );

	private static final int FP_MAX_Y_DISTANCE = NanoMath.divInt( 9, 10 );


	// tempo em segundos que o goleiro leva no frame de início de pulo (tempo de reflexo em relação ao chute - quanto maior, mais lento)
	private static final int FP_MIN_START_JUMP_TIME = NanoMath.divInt( 33, 100 );

	private static final int FP_START_JUMP_TIME = NanoMath.divInt( 45, 100 );

	// tempo em que o goleiro ainda se move após tocar o chão durante o pulo
	private static final int FP_FLOOR_TIME = NanoMath.divInt( 5, 10 );

	private static final int FP_FLOOR_DISTANCE_PERCENT = NanoMath.divInt( 45, 100 );

	public static final Point3f VECTOR_UP_90			= new Point3f( 0, NanoMath.ONE, 0 );
	public static final Point3f VECTOR_RIGHT_90			= new Point3f( NanoMath.ONE, 0, 0 );

	public static final Point3f VECTOR_UP_60			= new Point3f( FP_HALF, FP_SQRT_3_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_60			= new Point3f( FP_SQRT_3_DIV_2, -FP_HALF, 0 );
	public static final Point3f VECTOR_UP_60_LEFT		= new Point3f( -FP_HALF, FP_SQRT_3_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_60_LEFT	= new Point3f( FP_SQRT_3_DIV_2, FP_HALF, 0 );

	public static final Point3f VECTOR_UP_45			= new Point3f( FP_SQRT_2_DIV_2, FP_SQRT_2_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_45			= new Point3f( FP_SQRT_2_DIV_2, -FP_SQRT_2_DIV_2, 0 );
	public static final Point3f VECTOR_UP_45_LEFT		= new Point3f( -FP_SQRT_2_DIV_2, FP_SQRT_2_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_45_LEFT	= VECTOR_UP_45;

	public static final Point3f VECTOR_UP_30			= VECTOR_RIGHT_60_LEFT;
	public static final Point3f VECTOR_RIGHT_30			= new Point3f( FP_HALF, -FP_SQRT_3_DIV_2, 0 );
	public static final Point3f VECTOR_UP_30_LEFT		= new Point3f( -FP_SQRT_3_DIV_2, FP_HALF, 0 );
	public static final Point3f VECTOR_RIGHT_30_LEFT	= VECTOR_UP_60;

	public static final Point3f VECTOR_UP_0				= VECTOR_RIGHT_90;
	public static final Point3f VECTOR_RIGHT_0			= new Point3f( 0, -NanoMath.ONE, 0 );
	public static final Point3f VECTOR_UP_0_LEFT		= new Point3f( -NanoMath.ONE, 0, 0 );
	public static final Point3f VECTOR_RIGHT_0_LEFT		= VECTOR_UP_90;

	// indica o tipo de pulo
	private byte jumpType;
	
	// indica o estado da animação do pulo
	private byte jumpState;

	// vetor velocidade do pulo
	private final Point3f jumpSpeed = new Point3f();

	// posição inicial antes de mover-se
	private final Point3f initialPosition = new Point3f();

	// posição x inicial do pulo (no caso de bolas muito distantes, o goleiro pode mover-se lateralmente um pouco antes do pulo)
	private int fp_jumpXPosition;

	private int fp_instantYSpeed;	// velocidade vertical instantânea

	// tempo acumulado desde o início do pulo
	private int fp_accTime;

	/** Duração total do pulo. */
	private int fp_jumpTime;

	// tempo que o goleiro espera para iniciar o pulo
	private int fp_waitTime;

	private final Sprite keeper;

	//private final Sprite shadow;


	public Keeper( Match match ) throws Exception {
		super( match, ISO_KEEPER_N_ITEMS );

	//	shadow = new Sprite( PATH_IMAGES + "keeper_shadow" );
		//shadow.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		//insertDrawable( shadow );

		keeper = new Sprite( PATH_IMAGES + "keeper" );
		keeper.defineReferencePixel( keeper.getWidth() >> 1, KEEPER_REF_Y );
		insertDrawable( keeper );

		//#if DEBUG == "true"
			insertDrawable( new IsoQuad( collisionArea, 0xffff00 ) );
		//#endif

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	} // fim do método createInstance()


	/**
	 * Reposiciona o goleiro e o prepara para uma nova jogada.
	 * @param position
	 */
	public final void reset( Point3f position ) {
		if ( position != null ) {
			realPosition.set( position );
		} else {
			realPosition.set();
		}
		initialPosition.set( realPosition );

		setState( PLAYER_STOPPED );

		setBallCollisionTest( true ); 
		refreshScreenPosition();
	} // fim do método reset()


	/**
	 * Atualiza o estado do goleiro após "time" milisegundos.
	 * @param delta
	 */
	public final void update( int delta ) {
		final int fp_delta = match.getDeltaFP();

		switch ( jumpState ) {
			case KEEPER_STATE_START:
				keeper.update( delta );

				fp_accTime += fp_delta;
				realPosition.x += NanoMath.mulFixed( fp_jumpXPosition - realPosition.x, fp_delta );

				if ( fp_accTime >= fp_waitTime )
					setJumpState( KEEPER_STATE_JUMPING );
			break;

			case KEEPER_STATE_JUMPING:
				switch ( jumpType ) {
					case JUMP_TYPE_SIDE_HIGH:
						handleJumpTopSide( fp_delta );
					break;

					case JUMP_TYPE_SIDE_MEDIUM:
						handleJumpMedSide( fp_delta );
					break;

					case JUMP_TYPE_SIDE_LOW:
						handleJumpLowSide( fp_delta );
					break;

					case JUMP_TYPE_CENTER_HIGH:
						handleJumpUp( fp_delta );
					break;
				} // fim switch( jumpType )
				
				refreshScreenPosition();
			break;

			case KEEPER_STATE_ON_FLOOR:
				switch ( jumpType ) {
					case JUMP_TYPE_SIDE_HIGH:
					case JUMP_TYPE_SIDE_MEDIUM:
					case JUMP_TYPE_SIDE_LOW:
						updateRealPosition( fp_delta );
					break;

					case JUMP_TYPE_CENTER_HIGH:
						keeper.update( delta );
						updateFrame();
					break;
				} // fim switch( jumpType )

				refreshScreenPosition();
			break;
		}

		updateFrame();
	} // fim do método update()


	/**
	 * Reposiciona o goleiro, de acordo com a posição da falta e do número de jogadores na barreira.
	 * @param pBallPosition
	 * @param barrierPlayers
	 */
	public final void prepare( Point3f pBallPosition, int barrierPlayers ) {
		int fp_percent = NanoMath.mulFixed( FP_MAX_GOAL_PERCENT,
											NanoMath.mulFixed( NanoMath.divInt( barrierPlayers, BARRIER_MAX_N_PLAYERS ),
															   NanoMath.divFixed( pBallPosition.x, FP_REAL_FIELD_WIDTH_HALF ) ) );

		// o goleiro se posiciona no canto oposto ao da bola
		realPosition.x = NanoMath.mulFixed( ( FP_REAL_GOAL_WIDTH >> 1 ), -fp_percent );

		initialPosition.set( realPosition );
		updateFrame();

		refreshScreenPosition();
	} // fim do método prepare()


	public final Point3f getInitialPosition() {
		return initialPosition;
	}


	public final Point3f getJumpSpeed() {
		return jumpSpeed;
	}


	public final void loadReplayData( Replay replay ) {
		jumpSpeed.set( replay.keeperJumpSpeed );
		setRealPosition( replay.keeperPosition );
		initialPosition.set( replay.keeperPosition );
	}


	/**
	 * Define a direção do pulo do goleiro, de acordo com a direção e força da bola e número de jogadores na barreira.
	 * @param pDirection
	 * @param nBarrierPlayers
	 * @param fp_timeToGoal
	 */
	public final void setJumpDirection( Point3f jumpDirection, byte nBarrierPlayers, int fp_timeToGoal ) {
		setJumpState( KEEPER_STATE_WAIT );

//		jumpDirection.set( FP_MAX_X_DISTANCE, NanoMath.divFixed( FP_MAX_Y_DISTANCE, NanoMath.toFixed( 1 ) ), 0 ); // TODO teste
//		jumpDirection.set( -FP_MAX_X_DISTANCE, 0, 0 ); // TODO teste

		// o pulo total do goleiro depende da sua posição atual
		jumpDirection.subEquals( realPosition );

		// limita o pulo do goleiro
		jumpDirection.x = NanoMath.clamp( jumpDirection.x, -FP_MAX_X_DISTANCE, FP_MAX_X_DISTANCE );
		jumpDirection.y = NanoMath.clamp( jumpDirection.y, 0, FP_MAX_Y_DISTANCE );

		if ( Math.abs( jumpDirection.x ) <= NanoMath.divFixed( FP_REAL_KEEPER_WIDTH, NanoMath.divInt( 14, 10 ) ) ) {
			// goleiro move-se pouco horizontalmente
			if ( jumpDirection.y >= NanoMath.mulFixed( FP_MAX_Y_DISTANCE, NanoMath.HALF ) ) {
				// pula para cima
				calculateJumpTime( jumpDirection.y );

				jumpSpeed.x = NanoMath.divFixed( jumpDirection.x, fp_jumpTime );

				jumpType = JUMP_TYPE_CENTER_HIGH;
			} else {
				// fica parado (apenas desloca-se levemente para o lado da bola)
				jumpSpeed.y = 0;

				jumpType = JUMP_TYPE_CENTER_LOW;
			}
		} // fim if ( direction.x <= REAL_KEEPER_WIDTH )
		else { // direction.x > REAL_KEEPER_WIDTH / 2.0
			// goleiro pula para os lados
			calculateJumpTime( jumpDirection.y );

			jumpSpeed.x = NanoMath.divFixed( jumpDirection.x, fp_jumpTime );

			if ( jumpDirection.y >= NanoMath.mulFixed( FP_MAX_Y_DISTANCE, NanoMath.divInt( 67, 100 ) ) ) {
				jumpType = JUMP_TYPE_SIDE_HIGH;
			} else if ( jumpDirection.y < NanoMath.divFixed( FP_MAX_Y_DISTANCE, NanoMath.toFixed( 3 ) ) ) {
				jumpType = JUMP_TYPE_SIDE_LOW;
			} else {
				jumpType = JUMP_TYPE_SIDE_MEDIUM;
			}
		}

		// tempo que o goleiro espera antes de iniciar o pulo
		fp_waitTime = fp_timeToGoal + FP_MIN_START_JUMP_TIME - NanoMath.divFixed( NanoMath.mulFixed( FP_START_JUMP_TIME, Math.abs( jumpDirection.x ) ), FP_MAX_X_DISTANCE );
		if ( fp_waitTime > fp_timeToGoal ) {
			fp_waitTime = fp_timeToGoal - FP_MIN_START_JUMP_TIME;
		} else if ( fp_waitTime < FP_MIN_START_JUMP_TIME ) {
			fp_waitTime = FP_MIN_START_JUMP_TIME;
		}

		if ( fp_timeToGoal >= FP_KEEPER_MOVE_MIN_TIME ) {
			fp_jumpXPosition = realPosition.x + NanoMath.mulFixed( NanoMath.mulFixed( jumpDirection.x, Math.abs( FP_KEEPER_MOVE_MIN_TIME - fp_waitTime ) ), NanoMath.HALF );
		} else {
			fp_jumpXPosition = realPosition.x;
		}


		switch ( jumpType ) {
			case JUMP_TYPE_SIDE_HIGH:
				keeper.setSequence( SEQUENCE_RIGHT_HIGH + ( jumpDirection.x >= 0 ? 0 : SEQUENCE_LEFT_HIGH - SEQUENCE_RIGHT_HIGH ) );
			break;

			case JUMP_TYPE_SIDE_MEDIUM:
				keeper.setSequence( SEQUENCE_RIGHT_MEDIUM + ( jumpDirection.x >= 0 ? 0 : SEQUENCE_LEFT_HIGH - SEQUENCE_RIGHT_HIGH ) );
			break;

			case JUMP_TYPE_SIDE_LOW:
				keeper.setSequence( SEQUENCE_RIGHT_LOW + ( jumpDirection.x >= 0 ? 0 : SEQUENCE_LEFT_HIGH - SEQUENCE_RIGHT_HIGH ) );
			break;

			case JUMP_TYPE_CENTER_HIGH:
				keeper.setSequence( SEQUENCE_CENTER_HIGH );
			break;

			case JUMP_TYPE_CENTER_LOW:
				keeper.setSequence( SEQUENCE_CENTER_LOW );
			break;
		}
		updateFrame();

		setJumpState( KEEPER_STATE_START );
	} // fim do método setJumpDirection()


	private final void calculateJumpTime( int fp_jumpHeight ) {
		jumpSpeed.y = NanoMath.sqrtFixed( NanoMath.mulFixed( NanoMath.toFixed( -4 ), NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_jumpHeight ) ) );
		fp_jumpTime = Math.max( NanoMath.divFixed( -jumpSpeed.y, FP_GRAVITY_ACCELERATION ), FP_MIN_JUMP_TIME );
	}


	public final void setState( byte state ) {
		super.setState( state );

		switch ( state ) {
			case PLAYER_STOPPED:
				keeper.setSequence( SEQUENCE_CENTER_LOW );
			break;
		}

		updateFrame();
	} // fim do método setState()


	/**
	 * Atualiza o frame atual do jogador, de acordo com a direção para a qual ele está virado.
	 */
	protected final void updateFrame() {
		int fp_width = FP_REAL_KEEPER_WIDTH;
		int fp_height = 0;
		final Point3f top = new Point3f();
		final Point3f right = new Point3f();
		byte orientation = Quad.ANCHOR_BOTTOM;

		switch ( keeper.getCurrFrameIndex() ) {
			case FRAME_CENTER_HIGH:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_90 );
				right.set( VECTOR_RIGHT_90 );
			break;

			case FRAME_RIGHT_HIGH_START:
				fp_height = FP_REAL_KEEPER_HEIGHT;
				top.set( VECTOR_UP_60 );
				right.set( VECTOR_RIGHT_60 );
			break;

			case FRAME_PREPARE_RIGHT:
			case FRAME_PREPARE_RIGHT_MID_LOW:
				fp_height = FP_REAL_KEEPER_HEIGHT;
				top.set( VECTOR_UP_60 );
				right.set( VECTOR_RIGHT_60 );
			break;

			case FRAME_LEFT_HIGH_START:
				fp_height = FP_REAL_KEEPER_HEIGHT;
				top.set( VECTOR_UP_60_LEFT );
				right.set( VECTOR_RIGHT_60_LEFT );
			break;

			case FRAME_PREPARE_LEFT:
			case FRAME_PREPARE_LEFT_MID_LOW:
				fp_height = FP_REAL_KEEPER_HEIGHT;
				top.set( VECTOR_UP_60_LEFT );
				right.set( VECTOR_RIGHT_60_LEFT );
			break;
			
			case FRAME_RIGHT_HIGH_AIR:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_30 );
				right.set( VECTOR_RIGHT_30 );
			break;

			case FRAME_LEFT_HIGH_AIR:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_30_LEFT );
				right.set( VECTOR_RIGHT_30_LEFT );
			break;

			case FRAME_RIGHT_HIGH_FALL:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_0 );
				right.set( VECTOR_RIGHT_0 );
				orientation = Quad.ANCHOR_BOTTOM_RIGHT;
			break;
			
			case FRAME_LEFT_HIGH_FALL:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_0_LEFT );
				right.set( VECTOR_RIGHT_0_LEFT );
				orientation = Quad.ANCHOR_BOTTOM_LEFT;
			break;

			case FRAME_RIGHT_MIDDLE:
			case FRAME_RIGHT_FLOOR:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_0 );
				right.set( VECTOR_RIGHT_0 );
//				orientation = Quad.ANCHOR_BOTTOM_RIGHT;
			break;

			case FRAME_LEFT_MIDDLE:
			case FRAME_LEFT_FLOOR:
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
				top.set( VECTOR_UP_0_LEFT );
				right.set( VECTOR_RIGHT_0_LEFT );
//				orientation = Quad.ANCHOR_BOTTOM_LEFT;
			break;

			case FRAME_PREPARE_CENTER_HIGH:
			case FRAME_STOPPED:
			default:
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT;
				top.set( VECTOR_UP_90 );
				right.set( VECTOR_RIGHT_90 );
			break; // fim case PLAYER_STOPPED
		} // fim switch ( state )

		//#if DEBUG == "true"
			if ( fp_height == 0 )
				throw new IllegalStateException( "fp_height == 0" );
		//#endif

		collisionArea.setValues( realPosition, orientation, top, right, fp_width, fp_height );
		//shadow.setFrame( keeper.getFrameSequenceIndex() );
	} // fim do método IsoFrame::updateFrame()


	/**
	 * Trata a animação do goleiro pulando alto para o lado.
	 * @param fp_delta
	 */
	private final void handleJumpTopSide( int fp_delta ) {
		updateRealPosition( fp_delta );

		switch ( jumpState ) {
			case KEEPER_STATE_JUMPING:
				final int fp_percent = NanoMath.divFixed( fp_accTime, fp_jumpTime );

				byte expectedFrame = -1;
				if ( fp_percent < NanoMath.divInt( 27, 100 ) ) {
					expectedFrame = 2;
				} else if ( fp_percent < NanoMath.divInt( 67, 100 ) ) {
					expectedFrame = 3;
				} else {
					expectedFrame = 4;
				}

				if ( keeper.getFrameSequenceIndex() != expectedFrame ) {
					setKeeperFrame( expectedFrame );
				}
			break;
		}
	} // fim do método handleJumpTopSide()


	/**
	 * Trata a animação do goleiro pulando para o lado à meia altura.
	 * @param fp_delta
	 */
	private final void handleJumpMedSide( int fp_delta ) {
		updateRealPosition( fp_delta );

		switch ( jumpState ) {
			case KEEPER_STATE_JUMPING:
				final int fp_percent = NanoMath.divFixed( fp_accTime, fp_jumpTime );

				byte expectedFrame = -1;
				if ( fp_percent < NanoMath.divInt( 25, 100 ) ) {
					expectedFrame = 2;
				} else {
					expectedFrame = 3;
				}

				if ( keeper.getFrameSequenceIndex() != expectedFrame ) {
					setKeeperFrame( expectedFrame );
				}
			break;
		}
	} // fim do método handleJumpMedSide()


	/**
	 * Trata a animação do goleiro pulando rasteiro para o lado.
	 * @param fp_delta
	 */
	private final void handleJumpLowSide( int fp_delta ) {
		updateRealPosition( fp_delta );

		switch ( jumpState ) {
			case KEEPER_STATE_JUMPING:
				final int fp_percent = NanoMath.divFixed( fp_accTime, fp_jumpTime );

				byte expectedFrame = -1;
				if ( fp_percent < NanoMath.divInt( 25, 100 ) ) {
					expectedFrame = 2;
				} else {
					expectedFrame = 3;
				}

				if ( keeper.getFrameSequenceIndex() != expectedFrame ) {
					setKeeperFrame( expectedFrame );
				}
			break;
		}

	} // fim do método handleJumpLowSide()


	/**
	 * Trata a animação do goleiro pulando para cima.
	 * @param fp_delta
	 */
	private final void handleJumpUp( int fp_delta ) {
		updateRealPosition( fp_delta );

		switch ( jumpState ) {
			case KEEPER_STATE_JUMPING:
				final int fp_percent = NanoMath.divFixed( fp_accTime, fp_jumpTime );

				byte expectedFrame = -1;
				if ( fp_percent < NanoMath.divInt( 12, 100 ) ) {
					expectedFrame = 2;
				} else if ( realPosition.y > 0 ) {
					expectedFrame = 3;
				} else {
					expectedFrame = 4;
				}

				if ( keeper.getFrameSequenceIndex() != expectedFrame ) {
					setKeeperFrame( expectedFrame );
				}
			break;
		}
	} // fim do método handleJumpUp()


	/**
	 * Atualiza a posição real do goleiro após "time" segundos.
	 * @param fp_delta
	 */
	private final void updateRealPosition( int fp_delta ) {
		fp_accTime += fp_delta;

		switch ( jumpState ) {
			case KEEPER_STATE_JUMPING:
				realPosition.x = initialPosition.x + NanoMath.mulFixed( jumpSpeed.x, fp_accTime );

				fp_instantYSpeed = jumpSpeed.y + NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accTime );

				realPosition.y = Math.max( 0, initialPosition.y + NanoMath.mulFixed( jumpSpeed.y, fp_accTime ) +
									NanoMath.mulFixed( NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accTime ), fp_accTime ) );

				if ( fp_accTime >= fp_jumpTime && realPosition.y <= 0 ) {
					setJumpState( KEEPER_STATE_ON_FLOOR );
				}
			break;

			case KEEPER_STATE_ON_FLOOR:
				realPosition.x = initialPosition.x + NanoMath.mulFixed( NanoMath.mulFixed( jumpSpeed.x, fp_accTime ), FP_FLOOR_DISTANCE_PERCENT );

				if ( fp_accTime >= FP_FLOOR_TIME ) {
					setJumpState( KEEPER_STATE_STOPPED );
				}
			break;
		}
	} // fim do método updateRealPosition()


	/**
	 * Define o estado atual do pulo.
	 * @param state
	 */
	private final void setJumpState( byte state ) {
		jumpState = state;

		switch ( jumpState ) {
			case KEEPER_STATE_JUMPING:
				initialPosition.x = realPosition.x = fp_jumpXPosition;
			case KEEPER_STATE_WAIT:
			case KEEPER_STATE_START:
				fp_accTime = 0;
			break;
				
			case KEEPER_STATE_ON_FLOOR:
				fp_accTime = 0;

				initialPosition.set( initialPosition.x + NanoMath.mulFixed( jumpSpeed.x, fp_jumpTime ), 0, 0 );
				realPosition.set( initialPosition );

				switch ( jumpType ) {
					case JUMP_TYPE_CENTER_HIGH:
						setKeeperFrame( 4 );
					break;

					case JUMP_TYPE_SIDE_HIGH:
					case JUMP_TYPE_SIDE_LOW:
					case JUMP_TYPE_SIDE_MEDIUM:
						setKeeperFrame( keeper.getSequence( keeper.getSequenceIndex() ).length - 1 );
					break;
				}
			break;

			case KEEPER_STATE_STOPPED:
				fp_accTime = 0;
				
				initialPosition.set( initialPosition.x + NanoMath.mulFixed( NanoMath.mulFixed( jumpSpeed.x, FP_FLOOR_TIME ), FP_FLOOR_DISTANCE_PERCENT ), 0, 0 );
				realPosition.set( initialPosition );

				switch ( jumpType ) {
					case JUMP_TYPE_SIDE_HIGH:
					case JUMP_TYPE_SIDE_LOW:
					case JUMP_TYPE_SIDE_MEDIUM:
						setKeeperFrame( keeper.getSequence( keeper.getSequenceIndex() ).length - 1 );
					break;
				}
			break;
		}
	} // fim do método setJumpState()


	protected final void refreshScreenPosition() {
		keeper.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( realPosition ) );
		//shadow.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( realPosition.x, 0, realPosition.z ) );
	}


	private final void setKeeperFrame( int frameIndex ) {
		keeper.setFrame( frameIndex );
		updateFrame();
	}


}