//#if DEBUG == "true"
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

public final class IsoQuad extends Drawable implements Constants {
	private static final int COLOR_LINE_DEFAULT = 0xff0000;

	private static final byte TOP_LEFT = 0;
	private static final byte TOP_RIGHT = 1;
	private static final byte BOTTOM_LEFT = 2;
	private static final byte BOTTOM_RIGHT = 3;

	private static final byte TOTAL_POINTS = 4;

	private final Point[] points = new Point[ TOTAL_POINTS ];

	private final Quad quad;

	private final int color;

	
	public IsoQuad( Quad q ) {
		this( q, COLOR_LINE_DEFAULT );
	}


	public IsoQuad( Quad q, int color ) {
		quad = q;

		this.color = color;

		for ( byte i = 0; i < TOTAL_POINTS; ++i ) {
			points[ i ] = new Point();
		}

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	}
	

	public final void setLines() {
		final Point3f quadPos = quad.position;

		points[ TOP_LEFT ].set( GameMIDlet.isoGlobalToPixels( quadPos ) );

		// superior direito
		points[ TOP_RIGHT ].set( GameMIDlet.isoGlobalToPixels( quadPos.add( quad.right.mul( quad.fp_width ) ) ) );

		// inferior esquerdo
		points[ BOTTOM_LEFT ].set( GameMIDlet.isoGlobalToPixels( quadPos.sub( quad.top.mul( quad.fp_height ) ) ) );

		// inferior direito
		points[ BOTTOM_RIGHT ].set( GameMIDlet.isoGlobalToPixels( quadPos.sub( quad.top.mul( quad.fp_height ) ).add( quad.right.mul( quad.fp_width ) ) ) );

//		System.out.println( "TL: " + quadPos );
//		System.out.println( "TR: " + ( quadPos.add( quad.right.mul( quad.fp_width ) ) ) );
//		System.out.println( "BL: " + ( quadPos.sub( quad.top.mul( quad.fp_height ) ) ) );
//		System.out.println( "BR: " + ( quadPos.sub( quad.top.mul( quad.fp_height ) ).add( quad.right.mul( quad.fp_width ) ) ) );

		// superior direito
		points[ TOP_RIGHT ].set( GameMIDlet.isoGlobalToPixels( quadPos.add( quad.right.mul( quad.fp_width ) ) ) );

		// inferior esquerdo
		points[ BOTTOM_LEFT ].set( GameMIDlet.isoGlobalToPixels( quadPos.sub( quad.top.mul( quad.fp_height ) ) ) );

		// inferior direito
		points[ BOTTOM_RIGHT ].set( GameMIDlet.isoGlobalToPixels( quadPos.sub( quad.top.mul( quad.fp_height ) ).add( quad.right.mul( quad.fp_width ) ) ) );
	}


	protected final void paint( Graphics g ) {
		g.setColor( color );

		setLines();
		
		Point p1 = points[ TOP_LEFT ];
		Point p2 = points[ TOP_RIGHT ];
		g.drawLine( translate.x + p1.x, translate.y + p1.y, translate.x + p2.x, translate.y + p2.y );

		p1 = points[ TOP_LEFT ];
		p2 = points[ BOTTOM_LEFT ];
		g.drawLine( translate.x + p1.x, translate.y + p1.y, translate.x + p2.x, translate.y + p2.y );

		p1 = points[ TOP_RIGHT ];
		p2 = points[ BOTTOM_RIGHT ];
		g.drawLine( translate.x + p1.x, translate.y + p1.y, translate.x + p2.x, translate.y + p2.y );

		p1 = points[ BOTTOM_LEFT ];
		p2 = points[ BOTTOM_RIGHT ];
		g.drawLine( translate.x + p1.x, translate.y + p1.y, translate.x + p2.x, translate.y + p2.y );
	}

}

//#endif