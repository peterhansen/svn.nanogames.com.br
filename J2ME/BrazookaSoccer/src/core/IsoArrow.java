/**
 * IsoArrow.java
 * 
 * Created on Jun 30, 2009, 3:17:45 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class IsoArrow extends Drawable implements Constants {

	private static final int ARROW_SIZE = NanoMath.ONE;

	private static final int TAIL_SIZE = NanoMath.mulFixed( ARROW_SIZE, NanoMath.toFixed( 4 ) );

	private final int color;

	private final Point origin = new Point();
	private final Point p1 = new Point();
	private final Point p2 = new Point();
	private final Point p3 = new Point();

	
	public IsoArrow( int color ) {
		this.color = color;

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	}


	public final void set( Point3f realPosition, Point3f direction ) {
		origin.set( GameMIDlet.isoGlobalToPixels( realPosition ) );

		direction.normalize();

		// o vetor unitário em y é garantidamente perpendicular ao vetor direção e os ortogonais
		final Point3f ortho1 = direction.cross( new Point3f( 0, NanoMath.ONE, 0 ) );
		final Point3f ortho2 = direction.cross( new Point3f( 0, -NanoMath.ONE, 0 ) );
		final Point3f base = realPosition.add( direction.mul( TAIL_SIZE ) );

		p1.set( GameMIDlet.isoGlobalToPixels( base.add( ortho1.getNormal().mul( ARROW_SIZE ) ) ) );
		p2.set( GameMIDlet.isoGlobalToPixels( base.add( ortho2.getNormal().mul( ARROW_SIZE ) ) ) );
		p3.set( GameMIDlet.isoGlobalToPixels( base.add( direction.getNormal().mul( ARROW_SIZE ) ) ) );
	}


	protected final void paint( Graphics g ) {
		g.setColor( color );

		final int x = translate.x + origin.x;
		final int y = translate.y + origin.y;
		final int x2 = translate.x + p3.x;
		final int y2 = translate.y + p3.y;
		g.drawLine( x    , y    , x2    , y2     );
		g.drawLine( x    , y + 1, x2    , y2 + 1 );
		g.drawLine( x    , y - 1, x2    , y2 - 1 );
		g.drawLine( x + 1, y    , x2 + 1, y2     );
		g.drawLine( x - 1, y    , x2 - 1, y2     );
	
		g.fillTriangle( translate.x + p1.x, translate.y + p1.y,
						translate.x + p2.x, translate.y + p2.y,
						translate.x + p3.x, translate.y + p3.y );
	}

}
