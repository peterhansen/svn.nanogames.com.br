/**
 * Tag.java
 * 
 * Created on Apr 23, 2009, 10:27:24 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Tag extends DrawableGroup implements Constants {

    protected  static  Label label;

	protected static  EdgeBorder border;

	private final DrawableGroup group;


	public Tag( int textIndex ) throws Exception {
		this( EdgeBorder.COLOR_BLUE, FONT_MENU, textIndex );
	}


	public Tag( int color, byte fontIndex, int textIndex ) throws Exception {
		this( color, fontIndex );
		setText( textIndex );
	}
	
	
	public Tag( int color, byte fontIndex, String text ) throws Exception {
		this( color, fontIndex );
		setText( text );
	}


	public Tag( String text ) throws Exception {
		this( EdgeBorder.COLOR_BLUE, FONT_MENU );
		setText( text );
	}


	public  Tag( int color, byte fontIndex ) throws Exception {
		super( 1 );

		final ImageFont font = GameMIDlet.GetFont(FONT_MENU);

		group = new DrawableGroup( 2 );
		insertDrawable( group );

		border = new EdgeBorder( color );
		border.setSize( TAG_WIDTH + TAG_SELECTED_X_OFFSET, font.getHeight() + ENTRIES_SPACING );
		group.insertDrawable( border );

		label = new Label( font, null );
		group.insertDrawable( label );

		setSize( ScreenManager.SCREEN_WIDTH, border.getHeight() );
		group.setSize( size );
		setSelected( false );
	}


	public static final void setText( String text ) {
		label.setText( text );
		label.setPosition( ( TAG_SELECTED_X_OFFSET + border.getWidth() - label.getWidth() ) >> 1, ENTRIES_SPACING >> 1 );
	}


	public final void setText( int textIndex ) {
		label.setText( textIndex );
		label.setPosition( ( border.getInternalWidth() - label.getWidth() ) >> 1, ENTRIES_SPACING >> 1 );
	}


	public final void setSelected( boolean selected ) {
		group.setPosition( selected ? 0 : Math.max( -TAG_SELECTED_X_OFFSET, -label.getPosX() ), group.getPosY() );
	//final int POS_X = ( ScreenManager.SCREEN_WIDTH - fillBig.getWidth() ) >> 1;
		final int POS_X = ( ScreenManager.SCREEN_WIDTH - getWidth()  ) >> 1;
		setPosition( selected ? POS_X + TAG_SELECTED_X_OFFSET : POS_X,getPosY() );
	}



}
