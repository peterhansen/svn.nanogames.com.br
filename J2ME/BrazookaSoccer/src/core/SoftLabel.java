package core;

import screens.*;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;


public final class SoftLabel extends UpdatableGroup implements Constants {

	//#if JAR == "min"
//# 		private short accTime;
	//#endif

	
	public SoftLabel( byte softKey, int textIndex ) throws Exception {
		super( 4 );
		
		 final ImageFont font = GameMIDlet.GetFont(FONT_MENU);
		//#if SCREEN_SIZE == "BIG"
			final MarqueeLabel label = new MarqueeLabel(font, GameMIDlet.getText( textIndex ) );
		//#else
//# 				final MarqueeLabel label = new MarqueeLabel(font, GameMIDlet.getText( textIndex ) );
		//#endif

		final int TOTAL_HEIGHT = label.getPosY() + label.getHeight() + ENTRIES_SPACING;

		final EdgeBorder fill = new EdgeBorder( EdgeBorder.COLOR_ORANGE, softKey == ScreenManager.SOFT_KEY_LEFT ? EdgeBorder.TYPE_EDGE_RIGHT : EdgeBorder.TYPE_EDGE_LEFT );
		insertDrawable( fill );

		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_RIGHT:
				label.setSize( label.getFont().getTextWidth( label.getText() ) + 4, label.getHeight() );
				label.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );

				//#if JAR == "min"
//# 					accTime = SOFT_KEY_VISIBLE_TIME;
				//#endif
			break;

			case ScreenManager.SOFT_KEY_LEFT:
				label.setSize( label.getFont().getTextWidth( label.getText() ) + 4, label.getHeight() );
				label.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
				label.setPosition( 2, label.getPosY() );

				//#if JAR == "min"
//# 					accTime = SOFT_KEY_VISIBLE_TIME;
				//#endif
			break;

			case ScreenManager.SOFT_KEY_MID:
				setSize( ScreenManager.SCREEN_WIDTH, TOTAL_HEIGHT );
				label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
				label.setSize( size.x - 2, label.getHeight() );
				label.setPosition( 1, label.getPosY() );
				label.setTextOffset( label.getWidth() );

				fill.setSize( getWidth(), TOTAL_HEIGHT );
			break;

			//#if DEBUG == "true"
				default:
					throw new Exception();
			//#endif
		}

		fill.setSize( label.getWidth(), TOTAL_HEIGHT );
		label.setPosition( fill.getInternalPosX() + 2, ENTRIES_SPACING >> 1 );
		setSize( fill.getSize() );
		insertDrawable( label );
	}


	//#if JAR == "min"
//# 		// como na versão de jar mínimo não há a classe BasicAnimatedSoftKey, é implementada uma alternativa para
//# 		// fazer os labels sumirem após algum tempo
//# 		public final void update( int delta ) {
//# 			super.update( delta );
//#
//# 			if ( accTime > 0 ) {
//# 				accTime -= delta;
//# 				if ( accTime <= 0 )
//# 					setVisible( false );
//# 			}
//# 		}
	//#endif

}
