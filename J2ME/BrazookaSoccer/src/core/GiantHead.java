/**
 * GiantHead.java
 * 
 * Created on 15/Dez/2009, 19:24:29
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public class GiantHead extends Drawable {

	private static final byte CIRCLES_TOTAL = 6;
	private static final byte TRIANGLES_TOTAL = 3;
	
	//private final byte[] CIRCLE_X_PERCENT = { 8, 7, 16, 22, 51, 58 };
	private final byte[] CIRCLE_X_PERCENT = { 8, 7, 16, 22, 40, 55 };
	private final byte[] CIRCLE_Y_PERCENT = { -1, 14, 31, 44, 65, 71 };
	private final byte[] CIRCLE_SIZE_PERCENT = { 22, 30, 27, 30, 17, 20 };

	
	private final short[] sizes = new short[ CIRCLES_TOTAL ];
	private final Point[] positions = new Point[ CIRCLES_TOTAL ];
	private final Point[] trianglesPoints = new Point[ TRIANGLES_TOTAL*3 ];
    /* X%3 trinagulos:
     * p1[X] = (X%3)
     * p2[X] = (X%3 + 1)
     * p3[X] = (X%3 + 2)
     */

    private final int COLOR_BACK = 0x00848f;
    private final int COLOR_HEAD = 0xe9a700;
    // final int COLOR_CONTOUR = 0xa9a750;
	
	private short diffX;
	
	
	public GiantHead() {
		for ( byte i = 0; i < CIRCLES_TOTAL; ++i )
			positions[ i ] = new Point();

		for ( byte i = 0; i < TRIANGLES_TOTAL*3; ++i )
			trianglesPoints[ i ] = new Point();
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	protected void paint( Graphics g ) {
		g.setColor( COLOR_BACK );
		g.fillRect( translate.x, translate.y, getWidth(), getHeight() );
		
		translate.x += diffX;

		/* "Degrade"(Contorno) do cabelo
        g.setColor( COLOR_CONTOUR );
		for ( byte i = 0; i < CIRCLES_TOTAL; ++i ) {
			g.fillArc( translate.x + positions[ i ].x - 10,
					   translate.y + positions[ i ].y - 10,
					   sizes[ i ] + 20, sizes[ i ] + 20, 0, 360 );
		}*/

		g.setColor( COLOR_HEAD );
		
		for ( byte i = 0; i < CIRCLES_TOTAL; ++i ) {
			g.fillArc( translate.x + positions[ i ].x, 
					   translate.y + positions[ i ].y, 
					   sizes[ i ], sizes[ i ], 0, 360 );
		}

		for ( byte i = 0; i < (TRIANGLES_TOTAL*3); i+=3 ){
            g.fillTriangle( translate.x + trianglesPoints[i].x,
                            translate.y + trianglesPoints[i].y,
                            translate.x + trianglesPoints[i+1].x,
                            translate.y + trianglesPoints[i+1].y,
                            translate.x + trianglesPoints[i+2].x,
                            translate.y + trianglesPoints[i+2].y);
        }
		
		g.fillRect( translate.x + trianglesPoints[1].x, translate.y, getClip().width, getHeight() );
		
		translate.x -= diffX;
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		diffX = ( short ) Math.max( getWidth() - ( height * 22 / 32 ), 0 );
		
		for ( byte i = 0; i < CIRCLES_TOTAL; ++i ) {
			sizes[ i ] = ( short ) ( height * CIRCLE_SIZE_PERCENT[ i ] / 100 );
			positions[ i ].set( CIRCLE_X_PERCENT[ i ] * getWidth() / 100, ( CIRCLE_Y_PERCENT[ i ] * getHeight() / 100 ) );
		}

		trianglesPoints[0].set(positions[ 0 ].x, positions[ 0 ].y);
		trianglesPoints[1].set(positions[ CIRCLES_TOTAL - 1 ].x + (4*sizes[ CIRCLES_TOTAL - 1 ])/5, translate.x + sizes[ CIRCLES_TOTAL - 1 ]/2 + positions[ CIRCLES_TOTAL - 1 ].y);
		trianglesPoints[2].set(trianglesPoints[1].x, 0 );

		trianglesPoints[3].set(sizes[ CIRCLES_TOTAL - 1 ]/2 + positions[ CIRCLES_TOTAL - 1 ].x, sizes[ CIRCLES_TOTAL - 1 ]/2 + positions[ CIRCLES_TOTAL - 1 ].y);
		trianglesPoints[4].set(trianglesPoints[1].x, getHeight());
		trianglesPoints[5].set(trianglesPoints[1].x, sizes[ CIRCLES_TOTAL - 1 ]/2 + positions[ CIRCLES_TOTAL - 1 ].y );

		trianglesPoints[6].set(sizes[ CIRCLES_TOTAL - 3 ]/2 + positions[ CIRCLES_TOTAL - 3 ].x, sizes[ CIRCLES_TOTAL - 3 ]/2 + positions[ CIRCLES_TOTAL - 3 ].y);
		trianglesPoints[7].set(trianglesPoints[1].x, sizes[ CIRCLES_TOTAL - 1 ]/2 + positions[ CIRCLES_TOTAL - 1 ].y);
		trianglesPoints[8].set(trianglesPoints[1].x, sizes[ CIRCLES_TOTAL - 3 ]/2 + positions[ CIRCLES_TOTAL - 3 ].y);
	}

}
