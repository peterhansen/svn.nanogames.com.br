package core;


import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import screens.GameMIDlet;
import screens.Match;

public final class Player extends GenericIsoPlayer implements Constants {
	// número de itens da coleção
	private static final byte ISO_PLAYER_N_ITEMS = 3;

	public static final byte SEQUENCE_STOPPED	= 0;
	public static final byte SEQUENCE_SHOOTING	= 1;
	public static final byte SEQUENCE_SHOT		= 2;

	private final PlayerSprite player;


	public Player( Match match ) throws Exception {
		super( match, ISO_PLAYER_N_ITEMS );

		player = new PlayerSprite();
		insertDrawable( player );

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	}


	public final void nextFrame() {
		player.nextFrame();
	}


	/**
	 * Reposiciona o jogador na posição "position". Se "relativeToBall" for true, o jogador posiciona-se para chutar a
	 * bola na posição recebida como parâmetro; caso contrário, o jogador coloca-se exatamente na posição recebida.
	 * @param position
	 * @param relativeToBall
	 */
	public final void reset( Point3f position, boolean relativeToBall ) {
		if ( position != null ) {
			realPosition.set( position );

			setState( PLAYER_STOPPED );
			setBallCollisionTest( true );
			refreshScreenPosition();
			lookAt();
		}
	} // fim do método reset()


	/**
	 * Reposiciona o jogador na posição "position".
	 * @param position
	 */
	public final void reset( Point3f position ) {
		reset( position, false );
	} // fim do método reset()


	public final void setSequence( int sequence ) {
		player.setSequence( sequence );
	}
	

	/**
	 * Define o estado do jogador.
	 * @param myState
	 */
	public final void setState( byte myState ) {
		state = myState;

		switch ( state ) {
			case PLAYER_STOPPED:
				player.setSequence( SEQUENCE_STOPPED );
			break;
			
			case PLAYER_SHOOTING:
				player.setSequence( SEQUENCE_SHOOTING );
			break;
			
			case PLAYER_SHOT:
				player.setSequence( SEQUENCE_SHOT );
			break;

			case PLAYER_RUNNING:
			case PLAYER_JUMPING:
			break;
		}

		updateFrame();
	} // fim do método setState()


	/**
	 * Atualiza o frame atual do jogador, de acordo com a direção para a qual ele está virado.
	 */
	protected final void updateFrame() {
		player.setTransform( realPosition.x < FP_REAL_BIG_AREA_WIDTH_HALF ? TRANS_NONE : TRANS_MIRROR_H );
	} // fim do método updateFrame()


	protected final void refreshScreenPosition() {
		player.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( realPosition ) );
	}


	public final void setListener( SpriteListener listener, int id ) {
		player.setListener( listener, id );
	}


	private static final class PlayerSprite extends Sprite {

		private final Point lastFrameOffset = new Point();

		public PlayerSprite() throws Exception {
			super( PATH_IMAGES + "player" );
			defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );

			lastFrameOffset.set( offsets[ offsets.length - 1 ].x, offsets[ 0 ].y );
		}


		public final Point getFrameOffset( int frameIndex ) {
			switch ( sequenceIndex ) {
				case SEQUENCE_SHOT:
					if ( frameIndex == 0 )
						return lastFrameOffset;
					
				// não tem break

				default:
					return super.getFrameOffset( frameIndex );
			}
		}


		public final boolean mirror( int mirrorType ) {
			final boolean ret = super.mirror( mirrorType );
			
			lastFrameOffset.set( offsets[ offsets.length - 1 ].x, offsets[ 0 ].y );

			return ret;
		}



	}

}