/**
 * FieldLines.java
 * 
 * Created on Jun 26, 2009, 1:07:47 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class FieldLines extends Drawable implements Constants {

	private final int COLOR_LINE = 0xeeffee;

	private final Point[] points;


	public FieldLines() {
		points = new Point[] {
			// linha de fundo
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, 0 ) ),
			// pequena área - frente
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_SMALL_AREA_WIDTH_HALF, 0, FP_REAL_SMALL_AREA_DEPTH ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_SMALL_AREA_WIDTH_HALF, 0, FP_REAL_SMALL_AREA_DEPTH ) ),
			// pequena área - esquerda
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_SMALL_AREA_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_SMALL_AREA_WIDTH_HALF, 0, FP_REAL_SMALL_AREA_DEPTH ) ),
			// pequena área - direita
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_SMALL_AREA_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_SMALL_AREA_WIDTH_HALF, 0, FP_REAL_SMALL_AREA_DEPTH ) ),
			// grande área - frente
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_BIG_AREA_WIDTH_HALF, 0, FP_REAL_BIG_AREA_DEPTH ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BIG_AREA_WIDTH_HALF, 0, FP_REAL_BIG_AREA_DEPTH ) ),
			// grande área - esquerda
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_BIG_AREA_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_BIG_AREA_WIDTH_HALF, 0, FP_REAL_BIG_AREA_DEPTH ) ),
			// grande área - direita
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BIG_AREA_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BIG_AREA_WIDTH_HALF, 0, FP_REAL_BIG_AREA_DEPTH ) ),
			// lateral esquerda
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_FIELD_DEPTH ) ),
			// meio de campo
			new Point( GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_FIELD_DEPTH_HALF ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_FIELD_DEPTH_HALF ) ),
			// lateral direita
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, 0 ) ),
			new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_FIELD_DEPTH ) ),
		};

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	}


	protected final void paint( Graphics g ) {
		g.setColor( COLOR_LINE );
		for ( byte i = 0; i < points.length; ) {
			final Point p1 = points[ i++ ];
			final Point p2 = points[ i++ ];
			g.drawLine( translate.x + p1.x, translate.y + p1.y, translate.x + p2.x, translate.y + p2.y );
		}

//		final Point3f temp = new Point3f( 0, 0, FP_REAL_FIELD_DEPTH_HALF );
//		for ( int i = 0; i < 360; ++i ) {
////			final Point3f r = new Point3f( NanoMath.cosFixed( NanoMath.divInt( i, 3 ) ), 0, NanoMath.cosFixed( NanoMath.divInt( i, 3 ) ) );
//			final Point3f r = new Point3f( NanoMath.cosInt( i ), 0, NanoMath.sinInt( i ) );
//			final Point p = GameMIDlet.isoGlobalToPixels( temp.add( r.mul( NanoMath.divInt( 915, 100 ) >> 1 ) ) );
//			g.drawRect( translate.x + p.x, translate.y + p.y, 0, 0 );
//		}
//
//		temp.set( 0, 0, FP_REAL_PENALTY_TO_GOAL );
//		for ( int i = 0; i < 360; ++i ) {
////			final Point3f r = new Point3f( NanoMath.cosFixed( NanoMath.divInt( i, 3 ) ), 0, NanoMath.cosFixed( NanoMath.divInt( i, 3 ) ) );
//			final Point3f r = new Point3f( NanoMath.cosInt( i ), 0, NanoMath.sinInt( i ) );
//			final Point p = GameMIDlet.isoGlobalToPixels( temp.add( r.mul( NanoMath.divInt( 915, 100 ) >> 1 ) ) );
//			g.drawRect( translate.x + p.x, translate.y + p.y, 0, 0 );
//		}
	}

}
