/**
 * Crowd.java
 * 
 * Created on Dec 17, 2009, 6:25:25 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableQuad;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Crowd extends DrawableGroup implements Constants {

	public static final byte STAIRS_TOTAL = 6;

	private static final int FP_STAIR_DEPTH		= 98304;
	private static final int FP_STAIR_HEIGHT	= NanoMath.HALF;
	
	public static final int FP_STAIRS_DEPTH		= FP_STAIR_DEPTH * STAIRS_TOTAL;
	
	private static final int FP_PEOPLE_MIN_DISTANCE = NanoMath.ONE << 1;

    private final int MAX_PEOPLE_CENTER;
	private final int MAX_PEOPLE_REAR;
	private final int MAX_PEOPLE_SIDE;

	public static   final int MAX_PEOPLE_PERCENT = 100 ;
	public static   final int MIN_PEOPLE_PERCENT = 2 ;

    /** Cores */
    private static final int COLOR_DARK_GREEN = 0x017e7e;
	private static final int COLOR_LIGHT_GREEN = 0x0068b8;

	private static final int COLOR_LIGHT = 0x999999;
	private static final int COLOR_MEDIUMLIGHT = 0x888888;
	private static final int COLOR_MEDIUMDARK = 0x7F7F7F;
	private static final int COLOR_DARK = 0x777777;

	private static final int COLOR_LIGHT_YELLOW = 0xffcc00;
    private static final int COLOR_MEDIUMLIGHT_YELLOW = 0xffbb00; // gradiente das 2 principais
	private static final int COLOR_MEDIUMDARK_YELLOW = 0xffaa00; // gradiente das 2 principais
    private static final int COLOR_DARK_YELLOW = 0xff9900;

    private static final int COLOR_GRAY = 0xBBBBBB;
    private static final int COLOR_LIGHT_GRAY = 0xDDDDDD;
    private static final int COLOR_DARK_WHITE = 0xEEEEEE;
    private static final int COLOR_WHITE = 0xFFFFFF;

	private final DrawableGroup peopleCenter;
	private final DrawableGroup peopleSide;
	private final DrawableGroup peopleRear;

    // Dados para o desnho das grades da arquibancada
    private final int LINHAS_HORIZONTAIS = 3; // Quantidades de linhas horizontais na grade
    private final int FP_INCREMENT_H = NanoMath.divInt( 1, 3 ); // Distancia entre as linhas horizontais da grade
    private final int FP_INCREMENT_V = NanoMath.ONE*5; // Distancia entre as linhas verticais da grade
    private final int FP_VSIZE = FP_INCREMENT_H * LINHAS_HORIZONTAIS; // Tamanho das linhas verticais da grade
    private final int LINHAS_VERTICAIS_LATERAIS = (FP_REAL_BOARD_Z_MAX-FP_REAL_BOARD_Z_MIN)/FP_INCREMENT_V; // Quantidades de linhas verticais na grade lateral
    private final int LINHAS_VERTICAIS_TRASEIRA = (FP_REAL_BOARD_X_RIGHT-(FP_REAL_BOARD_X_LEFT + FP_INCREMENT_V))/FP_INCREMENT_V; // Quantidades de linhas verticais na grade traseira
       
    // Pontos para o desnho das grades da arquibancada
    private final Point[] pvl1 = new Point[LINHAS_VERTICAIS_LATERAIS];
    private final Point[] pvl2 = new Point[LINHAS_VERTICAIS_LATERAIS];
    private final Point[] pvt1 = new Point[LINHAS_VERTICAIS_TRASEIRA];
    private final Point[] pvt2 = new Point[LINHAS_VERTICAIS_TRASEIRA];
    private final Point[] pt = new Point[LINHAS_HORIZONTAIS];;
    private final Point[] pc = new Point[LINHAS_HORIZONTAIS];;
    private final Point[] pl = new Point[LINHAS_HORIZONTAIS];;

    private final DrawableImage[] imagesPeople = new DrawableImage[]{
            new DrawableImage( PATH_IMAGES + "torcida1.png" ),
            new DrawableImage( PATH_IMAGES + "torcida2.png" ),
            new DrawableImage( PATH_IMAGES + "torcida3.png" ),
            new DrawableImage( PATH_IMAGES + "torcida4.png" ),
            new DrawableImage( PATH_IMAGES + "torcida5.png" ),
            new DrawableImage( PATH_IMAGES + "torcida6.png" ),
            new DrawableImage( PATH_IMAGES + "torcida7.png" )};


	public Crowd() throws Exception {
		super( STAIRS_TOTAL * 7 );

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );

		for ( byte i = STAIRS_TOTAL - 1; i >= 0; --i ) {
			final int fp_realY1 = FP_REAL_BOARD_HEIGHT + NanoMath.mulFixed( NanoMath.toFixed( i ), FP_STAIR_HEIGHT );
			final int fp_realY2 = fp_realY1 + FP_STAIR_HEIGHT;
			final int fp_realZ1 = FP_REAL_BOARD_Z_MIN - NanoMath.mulFixed( NanoMath.toFixed( i ), FP_STAIR_DEPTH );
			final int fp_realZ2 = fp_realZ1 - FP_STAIR_DEPTH;
			final int fp_realX1 = FP_REAL_BOARD_X_LEFT - NanoMath.mulFixed( NanoMath.toFixed( i ), FP_STAIR_DEPTH );
			final int fp_realX2 = fp_realX1 - FP_STAIR_DEPTH;


			final Point p1 = GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, fp_realY1, fp_realZ2 );
			final Point p2 = GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_RIGHT, fp_realY1, fp_realZ2 );
			final Point p3 = GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, fp_realY1, fp_realZ1 );
			final Point p4 = GameMIDlet.isoGlobalToPixels( fp_realX1, fp_realY1, FP_REAL_BOARD_Z_MIN  );
			final Point p5 = GameMIDlet.isoGlobalToPixels( fp_realX2, fp_realY1, FP_REAL_BOARD_Z_MIN  );
			final Point p6 = GameMIDlet.isoGlobalToPixels( fp_realX2, fp_realY1, FP_REAL_BOARD_Z_MAX    );
			final Point p7 = GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, fp_realY2, fp_realZ2 );

			final Pattern pf = new Pattern( COLOR_LIGHT );
			pf.setPosition( p5.x, p5.y - 1 );
			pf.setSize( p1.x - p5.x, p3.y - p1.y + 2 );
			insertDrawable( pf );
			pf.setClipTest( false );

			if ( i > 0 ) {
				final Pattern pt = new Pattern( COLOR_MEDIUMDARK );
				pt.setPosition( p4 );
				pt.setSize( p7.x - p4.x, p1.y - p7.y );
				insertDrawable( pt );
				pt.setClipTest( false );
			}
			
			// fundo do campo
			final DrawableQuad floorBack = new DrawableQuad( new Point( p1 ),
															 new Point( p2 ),
															 new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_RIGHT, fp_realY1, fp_realZ1 ) ),
															 new Point( p3 )
														   );
			floorBack.setSize( getSize() );
			floorBack.setClipTest( false );
			floorBack.setColor( COLOR_LIGHT );
			insertDrawable( floorBack );

			if ( i < STAIRS_TOTAL - 1 ) {
				final DrawableQuad upBack = new DrawableQuad( new Point( p7 ),
															  new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_RIGHT, fp_realY2, fp_realZ2 ) ),
															  new Point( p2 ),
															  new Point( p1 )
														   );
				upBack.setSize( getSize() );
				upBack.setColor( COLOR_DARK );
				upBack.setClipTest( false );
				insertDrawable( upBack );
			}


			// lateral do campo
			final DrawableQuad floorSide = new DrawableQuad( new Point( GameMIDlet.isoGlobalToPixels( fp_realX1, fp_realY1, FP_REAL_BOARD_Z_MAX    ) ),
															 new Point( p4 ),
															 new Point( p5 ),
															 new Point( p6 )
														   );
			floorSide.setSize( getSize() );
			floorSide.setColor( COLOR_LIGHT );
			insertDrawable( floorSide );

			if ( i < STAIRS_TOTAL - 1 ) {
				final DrawableQuad upSide = new DrawableQuad( new Point( GameMIDlet.isoGlobalToPixels( fp_realX2, fp_realY2, FP_REAL_BOARD_Z_MAX    ) ),
															  new Point( GameMIDlet.isoGlobalToPixels( fp_realX2, fp_realY2, FP_REAL_BOARD_Z_MIN  ) ),
															  new Point( p5 ),
															  new Point( p6 )
														   );
				upSide.setSize( getSize() );
				upSide.setColor( COLOR_MEDIUMLIGHT );
				upSide.setClipTest( false );
				insertDrawable( upSide );
			}
		}

		final DrawableQuad back = new DrawableQuad( new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT, FP_REAL_BOARD_Z_MIN  ) ),
													new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_RIGHT, FP_REAL_BOARD_HEIGHT, FP_REAL_BOARD_Z_MIN  ) ),
													new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_RIGHT, 0, FP_REAL_BOARD_Z_MIN  ) ),
													new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, 0, FP_REAL_BOARD_Z_MIN  ) ) );
		back.setSize( getSize() );
		back.setColor( COLOR_DARK );
		back.setClipTest( false );
		insertDrawable( back );

		final DrawableQuad side = new DrawableQuad( new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT, FP_REAL_BOARD_Z_MAX    ) ),
													new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT, FP_REAL_BOARD_Z_MIN  ) ),
													new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, 0, FP_REAL_BOARD_Z_MIN  ) ),
													new Point( GameMIDlet.isoGlobalToPixels( FP_REAL_BOARD_X_LEFT, 0, FP_REAL_BOARD_Z_MAX    ) ) );
		side.setSize( getSize() );
		side.setColor( COLOR_MEDIUMLIGHT );
		side.setClipTest( false );
		insertDrawable( side );
		
		final int FP_STAIRS_TOTAL = NanoMath.toFixed( STAIRS_TOTAL );
		MAX_PEOPLE_REAR = NanoMath.toInt( NanoMath.mulFixed( FP_STAIRS_TOTAL, NanoMath.divFixed( FP_REAL_BOARD_X_RIGHT - FP_REAL_BOARD_X_LEFT, FP_PEOPLE_MIN_DISTANCE ) ) );
		MAX_PEOPLE_SIDE = NanoMath.toInt( NanoMath.mulFixed( FP_STAIRS_TOTAL, NanoMath.divFixed( FP_REAL_BOARD_Z_MAX - FP_REAL_BOARD_Z_MIN, FP_PEOPLE_MIN_DISTANCE ) ) );
		MAX_PEOPLE_CENTER = NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( NanoMath.mulFixed(FP_STAIR_DEPTH * STAIRS_TOTAL, FP_STAIR_DEPTH * STAIRS_TOTAL ) , FP_PEOPLE_MIN_DISTANCE ) , NanoMath.ONE*3) );
        //System.out.println("MPC="+MAX_PEOPLE_CENTER+" SD*ST=" + NanoMath.toInt(FP_STAIR_DEPTH * STAIRS_TOTAL) + " sqrtInt=" + NanoMath.sqrtInt( NanoMath.toInt(FP_STAIR_DEPTH * STAIRS_TOTAL) ));

        peopleCenter = new DrawableGroup( MAX_PEOPLE_CENTER );
		peopleCenter.setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
		insertDrawable( peopleCenter );

		peopleSide = new DrawableGroup( MAX_PEOPLE_SIDE );
		peopleSide.setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
		insertDrawable( peopleSide );
		
		peopleRear = new DrawableGroup( MAX_PEOPLE_REAR );
		peopleRear.setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
		insertDrawable( peopleRear );

        // Cria pontos das linhas verticais da grade lateral
        for(int i = 0; i < LINHAS_VERTICAIS_LATERAIS ; ++i ) {
            pvl1[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT, (FP_REAL_BOARD_Z_MIN + (i*FP_INCREMENT_V))));
            pvl2[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT + FP_VSIZE, (FP_REAL_BOARD_Z_MIN + (i*FP_INCREMENT_V))));
        }
        // Cria pontos das linhas verticais da grade traseira
        for(int i = 0; i < LINHAS_VERTICAIS_TRASEIRA ; ++i ) {
            pvt1[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_LEFT + (FP_INCREMENT_V*(i+1)), FP_REAL_BOARD_HEIGHT, FP_REAL_BOARD_Z_MIN));
            pvt2[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_LEFT + (FP_INCREMENT_V*(i+1)), FP_REAL_BOARD_HEIGHT + FP_VSIZE, FP_REAL_BOARD_Z_MIN));
        }
        // Cria pontos das linhas horizontais da grade
        for(int i = 0; i < LINHAS_HORIZONTAIS; ++i) {
            pc[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT + (FP_INCREMENT_H*(i+1)), FP_REAL_BOARD_Z_MIN));
            pl[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_RIGHT, FP_REAL_BOARD_HEIGHT + (FP_INCREMENT_H*(i+1)), FP_REAL_BOARD_Z_MIN));
            pt[i] = new Point(GameMIDlet.isoGlobalToPixels(FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_HEIGHT + (FP_INCREMENT_H*(i+1)), FP_REAL_BOARD_Z_MAX));
        }
	}
	
	
	public final void setPercent( int percent ) {
        try {
            final short CROWD_REAR_TOTAL = ( short ) ( MAX_PEOPLE_REAR * percent / 100 );
            final short PEOPLE_PER_LINE_REAR = ( short ) ( CROWD_REAR_TOTAL / STAIRS_TOTAL );
            final short CROWD_SIDE_TOTAL = ( short ) ( MAX_PEOPLE_SIDE * percent / 100 );
            final short PEOPLE_PER_LINE_SIDE = ( short ) ( CROWD_SIDE_TOTAL / STAIRS_TOTAL );
            final short CROWD_CENTER_TOTAL = ( short ) ( MAX_PEOPLE_CENTER * percent / 100 );
            final int PEOPLE_SIZE = NanoMath.HALF;
            final int cos45xPs = NanoMath.mulFixed(NanoMath.cosInt(45), PEOPLE_SIZE);
            final int centerProgresion = NanoMath.divFixed(NanoMath.sqrtFixed(NanoMath.toFixed(2*CROWD_CENTER_TOTAL)) , NanoMath.toFixed(STAIRS_TOTAL-1));
            int FP_SLOT_WIDTH;
            int temp;
            int PeopleOnCenter = 0;

            peopleRear.removeAllDrawables();
            peopleSide.removeAllDrawables();
            peopleCenter.removeAllDrawables();

            for ( int stair = STAIRS_TOTAL - 1, fp_lastDiff = 0; stair >= 0; --stair ) {

                final int fp_realY = FP_REAL_BOARD_HEIGHT + NanoMath.mulFixed( NanoMath.toFixed( stair ), FP_STAIR_HEIGHT );
                final int fp_realZ = FP_REAL_BOARD_Z_MIN - NanoMath.mulFixed( NanoMath.toFixed( stair ), FP_STAIR_DEPTH );
                final int fp_realX = FP_REAL_BOARD_X_LEFT - NanoMath.mulFixed( NanoMath.toFixed( stair ), FP_STAIR_DEPTH );

                final short PEOPLE_PER_LINE_CENTER = ( short ) ( NanoMath.toInt( stair*centerProgresion ) ) ;
                final int CENTER_SIZE = NanoMath.sqrtFixed(2*NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( stair ), FP_STAIR_DEPTH ), NanoMath.mulFixed( NanoMath.toFixed( stair ), FP_STAIR_DEPTH )));
                //CENTER_SIZE = NanoMath.sqrtFixed( (NanoMath.mulFixed(fp_realZ - FP_REAL_BOARD_Z_MIN, fp_realZ - FP_REAL_BOARD_Z_MIN) + NanoMath.mulFixed(fp_realX - FP_REAL_BOARD_X_LEFT, fp_realX - FP_REAL_BOARD_X_LEFT)) );

                System.out.println("PPLC="+PEOPLE_PER_LINE_CENTER+" CS=" + CENTER_SIZE + " cP=" + centerProgresion + " CCT=" + CROWD_CENTER_TOTAL);

                if(PeopleOnCenter < CROWD_CENTER_TOTAL)
                {
                    for ( int fp_x = fp_realX, fp_z = FP_REAL_BOARD_Z_MIN, c = 0; c < PEOPLE_PER_LINE_CENTER && fp_x < FP_REAL_BOARD_X_LEFT; ++c ) {
                        final DrawableImage s = new DrawableImage( imagesPeople[NanoMath.randInt(imagesPeople.length)] );
                        s.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
                        peopleCenter.insertDrawable( s );
                        s.mirror( TRANS_MIRROR_H ); // espelha a imagem

                        FP_SLOT_WIDTH = (FP_REAL_BOARD_X_LEFT - fp_x) / (PEOPLE_PER_LINE_CENTER - c);
                        //System.out.println("FP_SW="+FP_SLOT_WIDTH);
                        temp = NanoMath.randFixed( FP_SLOT_WIDTH );
                        fp_x += temp;//NanoMath.mulFixed(cos45xPs,temp);
                        fp_z -= temp;//NanoMath.mulFixed(cos45xPs,temp);
                        s.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( fp_x, fp_realY, fp_z ) ); // posiciona s no ponto convertido para Pixel
                        ++PeopleOnCenter;
                        fp_x += cos45xPs;
                        fp_z -= cos45xPs;
                    }
                }

                // preenche a arquibancada de trás
                for ( int fp_x = FP_REAL_BOARD_X_LEFT, c = 0; c < PEOPLE_PER_LINE_REAR && fp_x < FP_REAL_BOARD_X_RIGHT; ++c ) {
                    final DrawableImage s = new DrawableImage( imagesPeople[NanoMath.randInt(imagesPeople.length)] );
                    s.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
                    peopleRear.insertDrawable( s );

                    FP_SLOT_WIDTH = NanoMath.divFixed( FP_REAL_BOARD_X_RIGHT - fp_x, NanoMath.toFixed( PEOPLE_PER_LINE_REAR - c ) ); // calcula o espaco restante de arquibancada
                    fp_x += NanoMath.randFixed( FP_SLOT_WIDTH ); // sorteia o espaco entre o torcedor anterior e o atual
                    s.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( fp_x , fp_realY, (fp_realZ - NanoMath.HALF)) ); // posiciona s no ponto convertido para Pixel
                    fp_x += NanoMath.HALF; // adiciona o tamanho aproximado que o torcedor ocupa
                }

                // preenche a arquibancada lateral
                for ( int fp_z = FP_REAL_BOARD_Z_MIN, c = 0; c < PEOPLE_PER_LINE_SIDE && fp_z < FP_REAL_BOARD_Z_MAX; ++c ) {
                    final DrawableImage s = new DrawableImage( imagesPeople[NanoMath.randInt(imagesPeople.length)] );
                    s.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
                    peopleSide.insertDrawable( s );
                    s.mirror( TRANS_MIRROR_H ); // espelha a imagem

                    FP_SLOT_WIDTH =  NanoMath.divFixed( FP_REAL_BOARD_Z_MAX - fp_z, NanoMath.toFixed( PEOPLE_PER_LINE_SIDE - c ) ); // calcula o espaco restante de arquibancada
                    fp_z += NanoMath.randFixed( FP_SLOT_WIDTH );  // sorteia o espaco entre o torcedor anterior e o atual
                    s.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( (fp_realX - NanoMath.HALF), fp_realY, fp_z ) ); // posiciona s no ponto convertido para Pixel
                    fp_z += PEOPLE_SIZE; // adiciona o tamanho aproximado que o torcedor ocupa
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
	}

    public final void paint(Graphics g) {
        super.paint(g);

        // Desenha linhas verticais da grade lateral
        for(int i = 0; i < LINHAS_VERTICAIS_LATERAIS; ++i) {
            g.setColor( COLOR_GRAY );
            g.drawLine( translate.x + pvl1[i].x + 1, translate.y + pvl1[i].y, translate.x + pvl2[i].x + 1, translate.y + pvl2[i].y );
            g.setColor( COLOR_LIGHT_GRAY );
            g.drawLine( translate.x + pvl1[i].x, translate.y + pvl1[i].y, translate.x + pvl2[i].x, translate.y + pvl2[i].y );
            g.setColor( COLOR_GRAY );
            g.drawLine( translate.x + pvl1[i].x - 1, translate.y + pvl1[i].y, translate.x + pvl2[i].x - 1, translate.y + pvl2[i].y );
        }
        // Desenha linhas verticais da grade traseira
        for(int i = 0; i < LINHAS_VERTICAIS_TRASEIRA; ++i) {
            g.setColor( COLOR_GRAY );
            g.drawLine( translate.x + pvt1[i].x + 1, translate.y + pvt1[i].y, translate.x + pvt2[i].x + 1, translate.y + pvt2[i].y );
            g.setColor( COLOR_LIGHT_GRAY );
            g.drawLine( translate.x + pvt1[i].x, translate.y + pvt1[i].y, translate.x + pvt2[i].x, translate.y + pvt2[i].y );
            g.setColor( COLOR_GRAY );
            g.drawLine( translate.x + pvt1[i].x - 1, translate.y + pvt1[i].y, translate.x + pvt2[i].x - 1, translate.y + pvt2[i].y );
        }
        // Desenha linhas horizontais da grade
        for(int i = 0; i < LINHAS_HORIZONTAIS; ++i) {
            if(i != (LINHAS_HORIZONTAIS-1)) { // desenha linhas intermediarias
                g.setColor( COLOR_LIGHT_GRAY );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y, translate.x + pt[i].x, translate.y + pt[i].y );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y, translate.x + pl[i].x, translate.y + pl[i].y );
                g.setColor( COLOR_GRAY );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y + 1, translate.x + pt[i].x, translate.y + pt[i].y + 1 );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y + 1, translate.x + pl[i].x, translate.y + pl[i].y + 1 );
            }
            else { // desenha linha final
                g.setColor( COLOR_WHITE );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y, translate.x + pt[i].x, translate.y + pt[i].y );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y, translate.x + pl[i].x, translate.y + pl[i].y );
                g.setColor( COLOR_DARK_WHITE );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y + 1, translate.x + pt[i].x, translate.y + pt[i].y + 1 );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y + 1, translate.x + pl[i].x, translate.y + pl[i].y + 1 );
                g.setColor( COLOR_LIGHT_GRAY );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y + 2, translate.x + pt[i].x, translate.y + pt[i].y + 2 );
                g.drawLine( translate.x + pc[i].x, translate.y + pc[i].y + 2, translate.x + pl[i].x, translate.y + pl[i].y + 2 );
            }
        }
    }
}
