/**
 * EdgeBorder.java
 * 
 * Created on Jul 1, 2009, 4:20:41 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public final class EdgeBorder extends DrawableGroup implements Constants {

	public static final byte TYPE_EDGE_RIGHT		= 0;
	public static final byte TYPE_EDGE_LEFT			= 1;
	public static final byte TYPE_EDGE_LEFT_RIGHT	= 2;
	public static final byte TYPE_BIG_EDGE_LEFT		= 3;
	public static final byte TYPE_BIG_BKG_LEFT		= 4;

	private static final byte FILL_BOTTOM_HEIGHT = 2;

	private final Pattern fillBig;
	private final Pattern fillBottom;

	private final Point edge = new Point();

	private final boolean edgeTop;


	public EdgeBorder( int color ) {
		this( color, TYPE_EDGE_RIGHT );
	}


	public EdgeBorder( int color, byte type ) {
		super( 3 );
		
		fillBig = new Pattern( color );
		insertDrawable( fillBig );

		switch ( type ) {
			case TYPE_EDGE_RIGHT:
			case TYPE_EDGE_LEFT:
			case TYPE_EDGE_LEFT_RIGHT:
				edgeTop = NanoMath.randInt( 128 ) < 64;
				edge.x = TAG_MIN_OFFSET_X + NanoMath.randInt( TAG_MAX_OFFSET_X - TAG_MIN_OFFSET_X );
				if ( type == TYPE_EDGE_LEFT ) {
					fillBig.setPosition( edge.x, 0 );
					edge.x = -edge.x;
				}
			break;

			case TYPE_BIG_BKG_LEFT:
				edgeTop = true;
				edge.x = -INFOBOX_BKG_EDGE_WIDTH;
				fillBig.setPosition( -edge.x, 0 );
			break;

			case TYPE_BIG_EDGE_LEFT:
			default:
				edgeTop = false;
				edge.x = -INFOBOX_EDGE_WIDTH;
				fillBig.setPosition( -edge.x, 0 );
			break;
		}

		fillBottom = new Pattern( 0x000000 );
		insertDrawable( fillBottom );

		setColor( color );
	}


	public final void setColor( int color ) {
		int colorBottom = 0;
		switch ( color ) {
			case COLOR_INFOBOX:
			case COLOR_BLUE:
				colorBottom = COLOR_BLUE_DARK;
			break;

			case COLOR_BLUE_DARK:
				colorBottom = COLOR_BLUE_DARKER;
			break;

			case COLOR_ORANGE:
				colorBottom = COLOR_ORANGE_DARK;
			break;

			case COLOR_ORANGE_DARK:
				colorBottom = COLOR_ORANGE_DARKER;
			break;
		}
		fillBottom.setFillColor( colorBottom );
		fillBig.setFillColor( color );
	}


	public final int getInternalWidth() {
		return fillBig.getWidth();
	}


	public final int getInternalPosX() {
		return fillBig.getPosX();
	}


	public final int getEdgeWidth() {
		return edge.x;
	}


	public final void setSize( int width, int height ) {
		super.setSize( width + Math.abs( edge.x ), height );

		fillBig.setSize( width, height - FILL_BOTTOM_HEIGHT );
		edge.y = edgeTop ? 0 : fillBig.getHeight();
		fillBottom.setSize( fillBig.getWidth() + ( edgeTop ? 0 : Math.abs( edge.x ) ) - FILL_BOTTOM_HEIGHT, FILL_BOTTOM_HEIGHT );
		if ( edge.x >= 0 )
			fillBottom.setPosition( fillBig.getPosX(), fillBig.getHeight() );
		else
			fillBottom.setPosition( ( edgeTop ? -edge.x : 0 ) + FILL_BOTTOM_HEIGHT, fillBig.getHeight() );
	}


	protected final void paint( Graphics g ) {
		super.paint( g );

		g.setColor( fillBig.getFillColor() );
		if ( edge.x >= 0 ) {
			final int x = translate.x + fillBig.getWidth();
			g.fillTriangle( x + edge.x, translate.y + edge.y, x, translate.y, x, translate.y + fillBig.getHeight() );
		} else {
			final int x = translate.x + fillBig.getPosX();
			g.fillTriangle( x + edge.x, translate.y + edge.y, x, translate.y, x, translate.y + fillBig.getHeight() );
		}
	}

}
