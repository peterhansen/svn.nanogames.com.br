/**
 * Replay.java
 * ©2008 Nano Games.
 *
 * Created on Apr 3, 2008 4:38:37 PM.
 */

//#if JAR != "min"

package core;

import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 * 
 * @author Peter
 */
public final class Replay implements Constants, Serializable {
	
	// atributos comuns:
	public final Play play = new Play();							// direção, altura, efeito e força do chute
	public final Point3f collisionDirection = new Point3f();				// vetor velocidade da bola após colisão com barreira, trave, goleiro
	public int	fp_collisionTime;					// intervalo de tempo entre o chute e a colisão que definiu a jogada
	public byte	playResult;						// resultado da jogada (gol, trave, fora, barreira, etc)
	public byte replayType;						// não salvo, pênalti (treino), pênalti (copa), treino de faltas, desafio de faltas
	public long saveTime;						// data da gravação

	// atributos do modo falta:
	public byte	grassIndex;						// tipo do pattern do gramado
	public final Point3f ballPosition = new Point3f();					// posição de origem do chute
	public final Point3f ballPositionAtGoal = new Point3f();				// posição da bola ao cruzar a linha de fundo
	public final Point3f boardCollisionDirection = new Point3f();		// vetor velocidade da bola após colisão com placa de publicidade
	public final Point3f keeperPosition = new Point3f();					// posição do goleiro
	public final Point3f keeperJumpSpeed = new Point3f();			// vetor velocidade do pulo do goleiro
	public final Point3f crosshairPosition = new Point3f();				// posição do alvo
    public short level;

	
	public final void write( DataOutputStream output ) throws Exception {
		// resultado da jogada
		output.writeByte( playResult );
		
		if ( playResult != BALL_STATE_STOPPED ) {
			output.writeByte( replayType );
			output.writeByte( playResult );
			output.writeByte( grassIndex );

			// momento da gravação do replay
			output.writeLong( saveTime );
			
			// jogada do batedor
			output.writeInt( play.direction.x );
			output.writeInt( play.direction.y );
			output.writeInt( play.direction.z );
			output.writeInt( play.fp_height );
			output.writeInt( play.fp_curve );
			output.writeInt( play.fp_power );
			
			// jogada do goleiro
			output.writeInt( keeperJumpSpeed.x );
			output.writeInt( keeperJumpSpeed.y );
			output.writeInt( keeperJumpSpeed.z );

			// momento da definição do resultado da jogada
			output.writeInt( fp_collisionTime );

			// posição da bola no momento da definição da jogada
			output.writeInt( ballPositionAtGoal.x );
			output.writeInt( ballPositionAtGoal.y );
			output.writeInt( ballPositionAtGoal.z );
			
			// direção da bola imediatamente após a definição da jogada
			output.writeInt( collisionDirection.x );
			output.writeInt( collisionDirection.y );
			output.writeInt( collisionDirection.z );
			
			// direção da bola imediatamente após colidir com uma placa de publicidade
			output.writeInt( boardCollisionDirection.x );
			output.writeInt( boardCollisionDirection.y );
			output.writeInt( boardCollisionDirection.z );

			// posição da bola
			output.writeInt( ballPosition.x );
			output.writeInt( ballPosition.y );
			output.writeInt( ballPosition.z );

			// posição do goleiro
			output.writeInt( keeperPosition.x );
			output.writeInt( keeperPosition.y );
			output.writeInt( keeperPosition.z );

			// posição da mira
			output.writeInt( crosshairPosition.x );
			output.writeInt( crosshairPosition.y );
			output.writeInt( crosshairPosition.z );

            output.writeShort( level );
		} // fim if ( saved )
	}


	public final void read( DataInputStream input ) throws Exception {
		// resultado da jogada
		playResult = input.readByte();		
		
		if ( playResult != BALL_STATE_STOPPED ) {
			replayType = input.readByte();
			playResult = input.readByte();
			grassIndex = input.readByte();

			// momento da gravação do replay
			saveTime = input.readLong();

			// jogada do batedor
			play.set( new Point3f( input.readInt(), input.readInt(), input.readInt() ), input.readInt(), input.readInt(), input.readInt() );

			// jogada do goleiro
			keeperJumpSpeed.set( input.readInt(), input.readInt(), input.readInt() );

			// momento da definição do resultado da jogada
			fp_collisionTime = input.readInt();

			// posição da bola no momento da definição da jogada
			ballPositionAtGoal.set( input.readInt(), input.readInt(), input.readInt() );

			// direção da bola imediatamente após a definição da jogada
			collisionDirection.set( input.readInt(), input.readInt(), input.readInt() );

			// direção da bola imediatamente após colidir com uma placa de publicidade
			boardCollisionDirection.set( input.readInt(), input.readInt(), input.readInt() );

			// posição da bola
			ballPosition.set( input.readInt(), input.readInt(), input.readInt() );

			// posição do goleiro
			keeperPosition.set( input.readInt(), input.readInt(), input.readInt() );

			// posição da mira
			crosshairPosition.set( input.readInt(), input.readInt(), input.readInt() );

			level = input.readShort();
		} // fim if ( saved )
	}	

}

//#endif