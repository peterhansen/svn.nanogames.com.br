/**
 * InfoBox.java
 * 
 * Created on Jul 6, 2009, 1:05:42 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class TopTextBox extends DrawableGroup implements Updatable, Constants {

	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;

	private byte state;

	private final MarqueeLabel labelText;

	private final Pattern patternFill;

	private final Pattern patternBottom;

	/** Velocidade da animação de transição, em pixels por segundo. */
	private final MUV speed = new MUV();


	public TopTextBox() throws Exception {
		super( 5 );

		patternFill = new Pattern( EdgeBorder.COLOR_ORANGE );
		insertDrawable( patternFill );

		patternBottom = new Pattern( EdgeBorder.COLOR_ORANGE_DARK );
		insertDrawable( patternBottom );

		labelText = new MarqueeLabel( FONT_MENU, null );
		labelText.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
//		labelText.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		insertDrawable( labelText );

		setSize( ScreenManager.SCREEN_WIDTH, labelText.getHeight() + 2 );
		setState( STATE_HIDDEN );
	}


	public final void setState( byte newState ) {
		switch ( newState ) {
			case STATE_HIDDEN:
				setVisible( false );
				setPosition( 0, -getHeight() );
			break;

			case STATE_APPEARING:
				setVisible( true );
				speed.setSpeed( size.y * 1000 / TIME_SOFTKEY_TRANSITION );
			break;

			case STATE_SHOWN:
				setVisible( true );
				setPosition( 0, 0 );
			break;

			case STATE_HIDING:
				setVisible( true );
				speed.setSpeed( -size.y * 1000 / TIME_SOFTKEY_TRANSITION );
			break;
		} // fim switch ( state )

		state = newState;
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_APPEARING:
				move( 0, speed.updateInt( delta ) );
				if ( getPosY() >= 0 )
					setState( STATE_SHOWN );

			case STATE_SHOWN:
				labelText.update( delta );
			break;

			case STATE_HIDING:
				move( 0, speed.updateInt( delta ) );
				if ( getPosY() <= -size.y )
					setState( STATE_HIDDEN );
				
				labelText.update( delta );
			break;
		} // fim switch ( state )
	}
	
	
	public final void setText( int textIndex ) {
		setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setText( String text ) {
		labelText.setText( text, false );
		labelText.setTextOffset( labelText.getWidth() );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		labelText.setSize( width, labelText.getHeight() );
		labelText.setSpeed( width * 1000 / 3000 );

		patternFill.setSize( labelText.getSize() );
		patternBottom.setPosition( 0, patternFill.getHeight() );
		patternBottom.setSize( width, 2 );
	}

}
