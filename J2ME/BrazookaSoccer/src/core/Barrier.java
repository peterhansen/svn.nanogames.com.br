/*
 * Barrier.java
 *
 * Created on 30 de Agosto de 2007, 23:13
 *
 */

package core;

import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import screens.Match;

/**
 *
 * @author peter
 */
public abstract class Barrier extends GenericIsoPlayer {
	
	// altura da barreira (valor é baixo pois existe uma tolerância na detecção)
	public static final int FP_REAL_BARRIER_HEIGHT = NanoMath.divInt( 167, 100 );

	// distância oficial da barreira em metros
	public static final int FP_BARRIER_DISTANCE_TO_BALL = NanoMath.divInt( 915, 100 );

	// distância lateral entre os jogadores da barreira
	public static final int FP_BARRIER_PLAYERS_DISTANCE = NanoMath.divInt( 81, 100 );

	// número de jogadores atualmente na barreira
	protected byte numberOfPlayers; 
	
	
	protected Barrier( Match match, int slots ) throws Exception {
		super( match, slots );
	}


	public abstract void prepare( Point3f ballPosition );
	
	
	/**
	 * Retorna o número de jogadores atualmente na barreira.
	 */
	public final byte getNumberOfPlayers() { 
		return numberOfPlayers; 
	}	
	
	
	/**
	 * Reposiciona a barreira com o máximo de jogadores para um chute em position.
	 */
	public void reset( Point3f position ) {
		prepare( position );	
	}
	
	
	// atualiza o frame atual dos jogadores, de acordo com a direção para a qual estão virados
	protected abstract void updateFrame();
	
}
