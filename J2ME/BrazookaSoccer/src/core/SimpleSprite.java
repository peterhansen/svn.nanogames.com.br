package core;

import br.com.nanogames.components.Sprite;
import javax.microedition.lcdui.Graphics;


public final class SimpleSprite extends Sprite {

	public SimpleSprite( Sprite s ) throws Exception {
		super( s );
	}


	public final void drawWithoutClip( Graphics g ) {
		if ( visible ) {
			translate.addEquals( position );
			paint( g );
			translate.subEquals( position );
		}
	}
}
