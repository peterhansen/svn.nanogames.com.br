/**
 * InfoBox.java
 * 
 * Created on Jul 6, 2009, 1:05:42 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class InfoBox extends DrawableGroup implements Updatable, Constants {

	public static final byte STATE_HIDDEN				= 0;
	public static final byte STATE_APPEARING			= 1;
	public static final byte STATE_UPDATING_VALUES		= 2;
	public static final byte STATE_SHOWN				= 3;
	public static final byte STATE_HIDING				= 4;

	private byte state;

	private static final int FP_TIME_UPDATE_VALUES = 70000;

	private static final short TIME_MIN = 200;

	private final EdgeBorder borderBig;

	private final Label labelInfo;

	private final Label labelTotalPointsTitle;

	private final Label labelTotalPoints;

	private final Label labelLevelTitle;

	private final Label labelLevel;

	private final Label[] labelsShotScores;

	private final Label labelTriesTitle;
	
	private final Label labelLevelScore;

	/** Velocidade da animação de transição, em pixels por segundo. */
	private final MUV speed = new MUV();

	private final Rectangle playScreenViewport;

	private final Point topTextBoxPosition;

	private final Point topTextBoxSize;

	private final EdgeBorder bkg;

	private byte currentTry;

	/** Velocidade da animação da pontuação incrementando. */
	private final MUV scoreSpeed = new MUV();

	private int fp_totalScore;
	private int fp_totalScoreShown;
	private int fp_tryScore;
	private int fp_tryScoreShown;
	private int fp_accTryScore;
	private int fp_accTryScoreShown;
	private int fp_tryMinScore;

	private short accTime;

	private final EdgeBorder[] tryScoreBorders;

	private final EdgeBorder borderLevelScore;


	public InfoBox( Rectangle viewport, Point topTextBoxPosition, Point topTextBoxSize ) throws Exception {
		super( 25 );

		playScreenViewport = viewport;
		this.topTextBoxPosition = topTextBoxPosition;
		this.topTextBoxSize = topTextBoxSize;

		int y = ScreenManager.SCREEN_HEIGHT >> 2;

		bkg = new EdgeBorder( COLOR_INFOBOX, EdgeBorder.TYPE_BIG_BKG_LEFT );
		bkg.setSize( INFOBOX_TOTAL_WIDTH, ScreenManager.SCREEN_HEIGHT );
		insertDrawable( bkg );

		labelInfo = new Label( FONT_MENU, TEXT_INFO );
		final ImageFont font = GameMIDlet.GetFont(FONT_MENU);

		final EdgeBorder borderInfo = new EdgeBorder( EdgeBorder.COLOR_ORANGE, EdgeBorder.TYPE_EDGE_LEFT );
		borderInfo.setSize( labelInfo.getWidth() + borderInfo.getEdgeWidth() + ENTRIES_SPACING, labelInfo.getHeight() );
		borderInfo.setPosition( labelInfo.getPosX() + labelInfo.getWidth() - borderInfo.getWidth(), y );
		
		labelInfo.setPosition( borderInfo.getPosX() + borderInfo.getInternalPosX(), y );

		y += ( labelInfo.getHeight() * 3 ) >> 2;

		borderBig = new EdgeBorder( EdgeBorder.COLOR_BLUE, EdgeBorder.TYPE_BIG_EDGE_LEFT );
		borderBig.setSize( INFOBOX_TOTAL_WIDTH, INFOBOX_INTERNAL_HEIGHT );
		borderBig.setPosition( 0, y );
		insertDrawable( borderBig );

		y += TITLE_SPACING;

		labelLevelTitle = new Label( FONT_MENU, TEXT_LEVEL );
		labelLevelTitle.setPosition( INFOBOX_EDGE_WIDTH + ENTRIES_SPACING, y );
		insertDrawable( labelLevelTitle );

		labelLevel = new Label( FONT_MENU, null );
		labelLevel.setSize( labelLevel.getFont().getTextWidth( String.valueOf( LEVEL_SHOWN_MAX + 1 ) ), labelLevel.getHeight() );
		labelLevel.setPosition( labelLevelTitle.getPosX() + labelLevelTitle.getWidth() + ENTRIES_SPACING, y );

		y += labelLevelTitle.getHeight() + ENTRIES_SPACING;

		labelTotalPointsTitle = new Label( FONT_MENU, TEXT_TOTAL_POINTS );
		labelTotalPointsTitle.setPosition( INFOBOX_EDGE_WIDTH + ENTRIES_SPACING, y );
		insertDrawable( labelTotalPointsTitle );
		
		labelTotalPoints = new Label( FONT_MENU, null );
		labelTotalPoints.setSize( labelTotalPoints.getFont().getTextWidth( String.valueOf( SCORE_SHOW_MAX + 1 ) ), labelTotalPoints.getHeight() );
		labelTotalPoints.setPosition( INFOBOX_TOTAL_WIDTH - ENTRIES_SPACING - labelTotalPoints.getWidth(), y );

//		y += labelTotalPointsTitle.getHeight() + ENTRIES_SPACING;

		final EdgeBorder borderTotalPoints = new EdgeBorder( EdgeBorder.COLOR_BLUE_DARK, EdgeBorder.TYPE_EDGE_LEFT );
		borderTotalPoints.setPosition( labelTotalPointsTitle.getPosX() + labelTotalPointsTitle.getWidth() + ENTRIES_SPACING, labelTotalPoints.getPosY() );
		borderTotalPoints.setSize( INFOBOX_TOTAL_WIDTH, labelTotalPoints.getHeight() );

		insertDrawable( borderTotalPoints );
		insertDrawable( labelTotalPoints );

		y += labelTotalPoints.getHeight() + ENTRIES_SPACING;

		final EdgeBorder borderLevel = new EdgeBorder( EdgeBorder.COLOR_BLUE_DARK, EdgeBorder.TYPE_EDGE_RIGHT );
		borderLevel.setSize( labelLevel.getWidth() + borderTotalPoints.getEdgeWidth() + ( ENTRIES_SPACING << 1 ), labelLevel.getHeight() );
		borderLevel.setPosition( labelLevel.getPosX() - ENTRIES_SPACING, labelLevel.getPosY() );

		insertDrawable( borderLevel );
		insertDrawable( labelLevel );

		labelTriesTitle = new Label( FONT_MENU, TEXT_TRIES );
		labelTriesTitle.setPosition( INFOBOX_EDGE_WIDTH + ENTRIES_SPACING, y );
		insertDrawable( labelTriesTitle );
		
		y += labelTriesTitle.getHeight() + ENTRIES_SPACING;

		
		final int LABEL_WIDTH = font.getTextWidth( "999.999" );
		final int LABEL_TOTAL_WIDTH =font.getTextWidth( GameMIDlet.getText( TEXT_TOTAL_POINTS ) );
		final int TRY_SCORE_OFFSET_X = font.getTextWidth( "11. " );
		
		labelsShotScores = new Label[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
		tryScoreBorders = new EdgeBorder[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
		final Label[] labelsShotsTitles = new Label[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
		for ( byte i = 0; i < FOUL_CHALLENGE_TRIES_PER_FOUL; ++i ) {
			final Label title = new Label( FONT_MENU, ( i + 1 ) + ". " );
			labelsShotsTitles[ i ] = title;			
			title.setPosition( labelTriesTitle.getPosX(), y );
			
			final Label labelValue = new Label( FONT_MENU, null );
			labelsShotScores[ i ] = labelValue;
			
			final EdgeBorder borderPartial = new EdgeBorder( EdgeBorder.COLOR_BLUE_DARK, EdgeBorder.TYPE_EDGE_RIGHT );
			tryScoreBorders[ i ] = borderPartial;
			borderPartial.setSize( LABEL_WIDTH + borderPartial.getInternalPosX(), title.getHeight() );
			borderPartial.setPosition( title.getPosX() + TRY_SCORE_OFFSET_X, y );
			
			labelValue.setPosition( borderPartial.getPosX() + ENTRIES_SPACING, y );
			
			insertDrawable( borderPartial );
			insertDrawable( title );
			insertDrawable( labelValue );
			
			y += title.getHeight() + ENTRIES_SPACING;
		}
		
		y += ENTRIES_SPACING;
		final Label labelTotal = new Label( FONT_MENU, TEXT_SCORE_TARGET );
		labelTotal.setPosition( labelInfo.getPosX() + ENTRIES_SPACING, y );
		insertDrawable( labelTotal );
		
		borderLevelScore = new EdgeBorder( EdgeBorder.COLOR_BLUE_DARK, EdgeBorder.TYPE_EDGE_LEFT );
		borderLevelScore.setPosition( labelTotal.getPosX() + labelTotal.getWidth(), y );
		borderLevelScore.setSize( INFOBOX_TOTAL_WIDTH, labelTotal.getHeight() );
		insertDrawable( borderLevelScore );
		
		labelLevelScore = new Label( FONT_MENU, null );
		labelLevelScore.setPosition( borderLevelScore.getPosX() + borderLevelScore.getInternalPosX() + ENTRIES_SPACING, y );
		insertDrawable( labelLevelScore );

		insertDrawable( borderInfo );
		insertDrawable( labelInfo );		

		setSize( INFOBOX_TOTAL_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setPosition( ScreenManager.SCREEN_WIDTH, 0 );
		setState( STATE_HIDDEN );

		refreshLabels();
	}


	public final void setState( byte newState ) {
		state = newState;

		switch ( newState ) {
			case STATE_HIDDEN:
				setVisible( false );
				setPosition( ScreenManager.SCREEN_WIDTH, getPosY() );
			break;

			case STATE_APPEARING:
				setVisible( true );
				speed.setSpeed( -size.x * 1000 / TIME_SOFTKEY_TRANSITION );
				refreshLabels();
			break;

			case STATE_UPDATING_VALUES:
				setVisible( true );
				setPosition( ScreenManager.SCREEN_WIDTH - size.x, getPosY() );
				
				if ( currentTry < 0 ) {
//					setState( STATE_SHOWN );
				} else {

				}
			break;

			case STATE_SHOWN:
				fp_accTryScoreShown = fp_accTryScore;
				fp_totalScoreShown = fp_totalScore;
				fp_tryScoreShown = fp_tryScore;

				refreshLabels();

				setVisible( true );
				//GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;

			case STATE_HIDING:
				setVisible( true );
				speed.setSpeed( size.x * 1000 / TIME_SOFTKEY_TRANSITION );
				//GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;
		} // fim switch ( state )
	}


	public final void toggleState() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_HIDING:
				setState( STATE_APPEARING );
			break;
			
			case STATE_UPDATING_VALUES:
			case STATE_SHOWN:
			case STATE_APPEARING:
				setState( STATE_HIDING );
			break;
		}
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_APPEARING:
				move( speed.updateInt( delta ), 0 );
				if ( getPosX() <= ScreenManager.SCREEN_WIDTH - size.x )
					setState( STATE_UPDATING_VALUES );
			break;

			case STATE_HIDING:
				move( speed.updateInt( delta ), 0 );
				if ( getPosX() >= ScreenManager.SCREEN_WIDTH )
					setState( STATE_HIDDEN );
			break;

			case STATE_UPDATING_VALUES:
				final int diff = scoreSpeed.updateInt( delta );

				fp_accTryScoreShown += diff;
				fp_totalScoreShown += diff;
				fp_tryScoreShown += diff;

				accTime += delta;

				if ( fp_accTryScoreShown < fp_accTryScore || accTime < TIME_MIN )
					refreshLabels();
				else
					setState( STATE_SHOWN );
			break;
		} // fim switch ( state )

		playScreenViewport.y = topTextBoxPosition.y + topTextBoxSize.y;
	}


	private final void refreshLabels() {
		fp_accTryScoreShown = Math.min( fp_accTryScoreShown, fp_accTryScore );
		fp_totalScoreShown = Math.min( fp_totalScoreShown, fp_totalScore );
		fp_tryScoreShown = Math.min( fp_tryScoreShown, fp_tryScore );

		if ( currentTry >= 0 ) {
			labelsShotScores[ currentTry ].setText( NanoMath.toString( fp_tryScoreShown ) );
			tryScoreBorders[ currentTry ].setColor( fp_tryScoreShown >= Crosshair.FP_TOTAL_MAX_POINTS ? EdgeBorder.COLOR_ORANGE : EdgeBorder.COLOR_BLUE_DARK );
		}
		
		labelTotalPoints.setText( NanoMath.toString( fp_totalScoreShown ) );
		labelTotalPoints.setPosition( getWidth() - labelTotalPoints.getWidth() - ENTRIES_SPACING, labelTotalPoints.getPosY() );
		
		labelLevelScore.setText( NanoMath.toString( fp_accTryScoreShown ) + " / " + NanoMath.toString( fp_tryMinScore ) );
		borderLevelScore.setColor( fp_accTryScoreShown >= fp_tryMinScore ? EdgeBorder.COLOR_ORANGE : EdgeBorder.COLOR_BLUE_DARK );
	}


	public final void setPosition( int x, int y ) {
		super.setPosition( x, y );

		playScreenViewport.set( 0, playScreenViewport.y, x + INFOBOX_BKG_EDGE_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void setLevel( int level ) {
		level = NanoMath.clamp( level, 1, LEVEL_SHOWN_MAX );
		
		if ( level < 10 )
			labelLevel.setText( "00" + String.valueOf( level ), false );
		else if ( level < 100 )
			labelLevel.setText( "0" + String.valueOf( level ), false );
		else
			labelLevel.setText( String.valueOf( level ), false );
	}


	public final void setTotalScore( int fp_totalScore ) {
		labelTotalPoints.setText( NanoMath.toString( fp_totalScore ) );
		this.fp_totalScore = fp_totalScore;
	}


	public final void setTryScore( int currentTry, int[] fp_tryScore, int fp_tryMinScore ) {
		this.currentTry = ( byte ) currentTry;
		this.fp_tryMinScore = fp_tryMinScore;
		fp_tryScoreShown = 0;

		fp_accTryScore = 0;
		for ( byte i = 0; i < FOUL_CHALLENGE_TRIES_PER_FOUL; ++i ) {
			fp_accTryScore += fp_tryScore[ i ];
			labelsShotScores[ i ].setText( i <= currentTry ? NanoMath.toString( fp_tryScore[ i ] ) : null );
			tryScoreBorders[ i ].setColor( fp_tryScore[ i ] >= Crosshair.FP_TOTAL_MAX_POINTS ? EdgeBorder.COLOR_ORANGE : EdgeBorder.COLOR_BLUE_DARK );
		}

		if ( currentTry < 0 ) {
			fp_accTryScoreShown = 0;
			labelLevelScore.setText( NanoMath.toString( fp_accTryScore ) + " / " + NanoMath.toString( fp_tryMinScore ) );
		} else {
			this.fp_tryScore = fp_tryScore[ currentTry ];
			scoreSpeed.setSpeed( NanoMath.mulFixed( fp_accTryScore, FP_TIME_UPDATE_VALUES ) );
		}

		accTime = 0;
	}


	public final byte getState() {
		return state;
	}


}
