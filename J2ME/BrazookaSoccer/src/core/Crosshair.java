package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import core.Constants;
import screens.GameMIDlet;

public final class Crosshair extends DrawableGroup implements Constants, Updatable {

	// tempo em milisegundos em que a mira é exibida ou some
	public static final short CROSSHAIR_BLINK_ON_TIME = 400;
	public static final short CROSSHAIR_BLINK_OFF_TIME = 100;

	// máximo de pontos ganhos num chute
	public static final int FP_TOTAL_MAX_POINTS	= NanoMath.toFixed( 100 );

	// número de pontos ganhos por cada bola restante ao passar de nível
	public static final int FP_BONUS_POINTS = NanoMath.toFixed( 150 );

	// distância máxima ao centro do alvo na qual força-se o goleiro a não defender a bola
	public static final int FP_BULLS_EYE_DISTANCE = NanoMath.divFixed( 16, 10 );

	// distância máxima do alvo que conta pontos
	private static final int FP_MAX_SCORE_DISTANCE = NanoMath.divInt( 45, 10 );

	// pontos de "consolação" (pontos mínimos ganhos numa jogada)
	private static final int FP_MIN_POINTS = NanoMath.ONE;
	private static final int FP_AFTER_BARRIER_POINTS = NanoMath.toFixed( 5 );

	// pontos ganhos por marcar um gol
	private static final int FP_POINTS_GOAL	= NanoMath.toFixed( 20 );
	// pontos ganhos por atingir a trave
	private static final int FP_POINTS_POST = NanoMath.toFixed( 5 );

	// porcentagem dos pontos ganhos quando a bola vai para fora
	private static final int FP_FACTOR_OUT = NanoMath.divInt( 7, 10 );

	// número máximo de pontos ganhos pela distância ao centro do alvo
	private static final int FP_PERFECT_SHOOT_POINTS = ( FP_TOTAL_MAX_POINTS - FP_MIN_POINTS - FP_AFTER_BARRIER_POINTS - FP_POINTS_GOAL );

	// distância máxima x ao centro do campo, para efeitos de cálculo da posição do alvo
	private static final int FP_MAX_FOUL_X_DISTANCE = ( FP_REAL_FIELD_WIDTH_HALF - NanoMath.toFixed( 5 ) );

	/** porcentagem da área do gol válida para posicionamento do alvo */
	private static final int FP_GOAL_AREA_PERCENT = NanoMath.divInt( 4, 10 );

	/** porcentagem da área do gol válida para posicionamento do alvo */
	private static final int FP_GOAL_AREA_PERCENT_HALF = FP_GOAL_AREA_PERCENT >> 1;

	private final DrawableImage shadow;

	private final DrawableImage crosshair;

	private short accTime;

	private final Point3f realPosition = new Point3f();


	public Crosshair() throws Exception {
		super( 2 );
		
		shadow = new DrawableImage( PATH_IMAGES + "crosshair_shadow.png" );
		shadow.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( shadow );

		crosshair = new DrawableImage( PATH_IMAGES + "crosshair.png" );
		crosshair.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		insertDrawable( crosshair );

		setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
	}


	public final void update( int delta ) {
		accTime += delta;

		if ( visible ) {
			if ( accTime > CROSSHAIR_BLINK_ON_TIME ) {
				setVisible( false );
				accTime %= CROSSHAIR_BLINK_ON_TIME;
			}
		} else {
			// não está visível
			if ( accTime > CROSSHAIR_BLINK_OFF_TIME ) {
				setVisible( true );
				accTime %= CROSSHAIR_BLINK_OFF_TIME;
			}
		}
	} // fim do método Crosshair::update()


	public final Point3f getRealPosition() {
		return realPosition;
	}


	/**
	 * Gera uma nova posição semi-aleatória do alvo, de acordo com a posição da falta.
	 * @param pBallPosition
	 */
	public final void reset( Point3f pBallPosition ) {
		final Point3f rnd = new Point3f();

		int leftPercent = FP_GOAL_AREA_PERCENT_HALF - NanoMath.divFixed( NanoMath.mulFixed( FP_GOAL_AREA_PERCENT_HALF, pBallPosition.x ), FP_MAX_FOUL_X_DISTANCE );
		if ( leftPercent < 0 )
			leftPercent = 0;
		else if ( leftPercent > FP_GOAL_AREA_PERCENT )
			leftPercent = FP_GOAL_AREA_PERCENT;
		
		int rightPercent = FP_GOAL_AREA_PERCENT - leftPercent;

		// posições válidas estão compreendidas entre as traves menos a largura da bola
		rnd.x = NanoMath.randFixed( FP_GOAL_AREA_PERCENT );
		if ( rnd.x < leftPercent ) {
			rnd.x = FP_REAL_LEFT_POST_START + NanoMath.mulFixed( NanoMath.mulFixed( FP_REAL_GOAL_WIDTH, leftPercent ), rnd.x );
		} else {
			rnd.x = FP_REAL_RIGHT_POST_START - NanoMath.mulFixed( NanoMath.mulFixed( FP_REAL_GOAL_WIDTH, rightPercent), rnd.x );
		}

		// posição varia de 0 até a altura do travessão menos a largura da bola
		rnd.y = NanoMath.randFixed( FP_REAL_BAR_BOTTOM );
		rnd.z = 0;

		setRealPosition( rnd );
	} // fim do método Crosshair::reset()


	/**
	 * Define a posição do alvo no mundo.
	 * @param position
	 */
	public final void setRealPosition( Point3f position ) {
		realPosition.set( position );
		final Point pos = GameMIDlet.isoGlobalToPixels( position );
		crosshair.setRefPixelPosition( pos );

		// atualiza a posição da sombra
		pos.set( GameMIDlet.isoGlobalToPixels( position.x, 0, position.z ) );
		shadow.setRefPixelPosition( pos.x, pos.y );
	} // fim do método Crosshair::setRealPosition()


	/**
	 * Calcula a pontuação obtida com o último chute.
	 * @param pBall
	 * @return
	 */
	public final int calculateScore( Ball ball ) {
		int fp_points = FP_MIN_POINTS + FP_AFTER_BARRIER_POINTS;
		final Point3f ballPos = new Point3f( ball.getRealPosition() );

		// desconsidera-se a posição z para efeito de cálculo dos pontos
		ballPos.z = 0;

		final Point3f distance = realPosition.sub( ballPos );
		int fp_distanceModule = distance.getModule();

		// soma-se 0.9 para arredondar a soma para cima, permitindo atingir de fato
		// o máximo de pontos, sem jamais ultrapassar 100 pontos numa jogada ou 300
		// na somatória
		if ( fp_distanceModule <= FP_MAX_SCORE_DISTANCE ) {
			final int fp_distSquared = NanoMath.mulFixed( fp_distanceModule, fp_distanceModule );
			final int fp_maxDistSquared = NanoMath.mulFixed( FP_MAX_SCORE_DISTANCE, FP_MAX_SCORE_DISTANCE );
			fp_points += FP_PERFECT_SHOOT_POINTS + NanoMath.divInt( 9, 10 ) -
					  NanoMath.mulFixed( NanoMath.divFixed( FP_PERFECT_SHOOT_POINTS, fp_maxDistSquared ), fp_distSquared );
		}

		if ( fp_points > FP_TOTAL_MAX_POINTS - FP_POINTS_GOAL )
			fp_points = FP_TOTAL_MAX_POINTS - FP_POINTS_GOAL;

		switch ( ball.getLastPlayResult() ) {
			case BALL_STATE_GOAL:
			case BALL_STATE_POST_GOAL:
				fp_points += FP_POINTS_GOAL;
			break;
			
			case BALL_STATE_POST_BACK:
				fp_points += FP_POINTS_POST;
			break;

			case BALL_STATE_REJECTED_BARRIER:
				fp_points = FP_MIN_POINTS;
			break;

			case BALL_STATE_POST_OUT:
				fp_points += FP_POINTS_POST;
			case BALL_STATE_OUT:
				fp_points = NanoMath.mulFixed( fp_points, FP_FACTOR_OUT );
			break;
		}

		//#if DEBUG == "true"
			System.out.println( "PONTUAÇÃO DA JOGADA: " + NanoMath.toString( fp_distanceModule ) + "m -> " + NanoMath.toString( fp_points ) );
		//#endif

		return fp_points;
	}


	public final boolean hasHitBullsEye( Point3f pBallPosition ) {
		final Point3f pos = new Point3f( pBallPosition );
		pos.z = 0;
		pos.subEquals( realPosition );
		
		return ( pos.getModule() <= FP_BULLS_EYE_DISTANCE );
	}

}