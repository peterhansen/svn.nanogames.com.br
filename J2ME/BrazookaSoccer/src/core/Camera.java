/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import screens.GameMIDlet;


/**
 *
 * @author caiolima
 */
public class Camera implements Constants{
	
	// <editor-fold defaultstate="collapsed" desc="Contantes">
    // velocidade com que a câmera se move quando controlada pelo jogador
	private static final int FP_CAMERA_MOVE_PLAYER_TIME = NanoMath.divFixed( FP_CAMERA_MOVE_DEFAULT_TIME, NanoMath.toFixed( 3 ) );
    // distância em metros à frente do jogador que a câmera aponta
	private static final int FP_CAMERA_SET_CONTROL_LOOK_AHEAD = NanoMath.toFixed( 5 );
    // velocidade com que a câmera se move quando controlada pelo jogador
	private static final int CAMERA_MOVE_PLAYER_TIME = NanoMath.divFixed( FP_CAMERA_MOVE_DEFAULT_TIME, NanoMath.toFixed( 3 ) ); 
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Limites da camera">
	private int CAMERA_LIMIT_X_LEFT;
	private int CAMERA_LIMIT_X_RIGHT;
	private int CAMERA_LIMIT_Y_UP;
	private int CAMERA_LIMIT_Y_DOWN;

    final Point p1 = GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_BOARD_Z_MIN );
    final Point p2 = GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_BOARD_Z_MAX );
    final Point p3 = GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_BOARD_Z_MIN );
    final Point p4 = GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, FP_REAL_BOARD_Z_MAX );
    // </editor-fold>	
    
	// <editor-fold defaultstate="collapsed" desc="Parâmetros da câmera"> 
	private final Point3f cameraSpeed = new Point3f(); // velocidade real de movimentação da câmera
	private final Point3f cameraPosition = new Point3f(); // posição real atual da câmera
	private final Point cameraPosition2D = new Point(); // posição real atual da câmera em 2D
	private byte cameraState; // estado atual da camera
    private boolean camera2D = false;
    // </editor-fold>

    private int a;
    private int b;
    private int dist;
    private int norma;
    
	private int fp_timeToDestination; // tempo total em segundos até a câmera chegar à posição de destino
	private int fp_cameraCurrentTime; // tempo atual é acumulado para agilizar teste de chegada ao destino

	private final DrawableGroup field; // instancia do campo
    
    
    public Camera(DrawableGroup fieldgroup) {
        field = fieldgroup;
    }

    /*public final int getPosX() {
        return cameraPosition.x;
    }

    public final int getPosY() {
        return cameraPosition.y;
    }*/

    // <editor-fold defaultstate="collapsed" desc="SetCameraCenter">
	/**
	 * Centraliza a câmera numa posição "real".
	 * @param realPosition
	 */
	private final void setCameraCenter( Point3f realPosition ) {
        /*
        // <editor-fold defaultstate="collapsed" desc="Debug Message">
		//#if DEBUG == "true"
        System.out.print("pos3D: ("+ (realPosition.x >> 14) +
                "," + (realPosition.y >> 14)  +
                "," + (realPosition.z  >> 14)  + ")cm -> ");
        //#endif
        // </editor-fold>
        */
        cameraPosition.set(realPosition);
		setCameraCenter( GameMIDlet.isoGlobalToPixels( realPosition ));
	}

	/**
	 * Centraliza a câmera na posição dada pelo Point position.
	 * @param position
	 * @param sinc
	 */
	private final void setCameraCenter( Point position) {
		setCameraCenter( position.x, position.y);
	}

	private final void setCameraCenter( int globalX, int globalY) {
        setCameraCenter2D(globalX, globalY);
	}

    private final void setCameraCenter2D ( Point camPos2D, boolean turn2D ) {
        if(turn2D) setCamera2D();
        setCameraCenter2D(camPos2D);
    }
    
    private final void setCameraCenter2D ( Point camPos2D ) {
		setCameraCenter2D(camPos2D.x, camPos2D.y);
    }
    
    /**
	 * Centraliza a câmera na posição x,y recebida.
	 * @param globalX
	 * @param globalY
	 */
    private final void setCameraCenter2D ( int globalX, int globalY) {
		cameraPosition2D.x = NanoMath.clamp( globalX, CAMERA_LIMIT_X_LEFT, CAMERA_LIMIT_X_RIGHT );
		cameraPosition2D.y = NanoMath.clamp( globalY, CAMERA_LIMIT_Y_UP, CAMERA_LIMIT_Y_DOWN );
        //cameraPosition2D.set(ParallelogramClamp(cameraPosition2D));

        /*
        // <editor-fold defaultstate="collapsed" desc="Debug Message">
		//#if DEBUG == "true"
        System.out.println("camPos2D: ("+globalX+","+globalY+")px "+(camera2D ? "Camera 2D.": "Camera 3D."));
        //#endif
        // </editor-fold>
        */
        field.setPosition(ScreenManager.SCREEN_HALF_WIDTH - cameraPosition2D.x, ScreenManager.SCREEN_HALF_HEIGHT - cameraPosition2D.y);
    }
    // </editor-fold>

    private final void setCamera2D() {
        if(!camera2D) {
            cameraPosition2D.set(GameMIDlet.isoGlobalToPixels( cameraPosition ));
            camera2D = true;
        }
    }

    private final void setCamera3D() {
        if(camera2D) {
            cameraPosition.set(GameMIDlet.pixelsToIsoGlobal(cameraPosition2D.x, cameraPosition2D.y, 0));
            camera2D = false;
        }
    }

	/**
	 * Desloca a camera para na direcao indicada.
	 * @param dx
	 * @param dy
	 * @param sinc
	 */
    public final void moveCamera2D(int dx, int dy){
		setCameraState(CAMERA_MANUAL);
        setCamera2D();
        cameraPosition2D.subEquals(dx, dy);
    }

	/**
	 * Define o vetor velocidade da câmera no modo manual.
	 * @param speed
	 */
	public final void setCameraSpeed( Point3f speed ) {
        System.out.println("("+speed.x+","+speed.y+","+speed.x+")");
		cameraSpeed.set( speed );
		setCameraState(CAMERA_MANUAL);
	}

	/**
	 * Troca o estado da camera.
	 * @param cameraState
	 */
	public final void setCameraState( byte cameraState ) {
		if(cameraState!=this.cameraState ){
            // <editor-fold defaultstate="collapsed" desc="Debug Message">
            //#if DEBUG == "true"
            switch(this.cameraState){
                case CAMERA_STOPPED:
                    System.out.print("CAMERA_STOPPED -> ");
                break;
                case CAMERA_FOLLOW_BALL:
                    System.out.print("CAMERA_FOLLOW_BALL -> ");
                break;
                case CAMERA_GOTO_BALL_THEN_FOLLOW:
                    System.out.print("CAMERA_GOTO_BALL_THEN_FOLLOW -> ");
                break;
                case CAMERA_FOLLOW_PLAYER:
                    System.out.print("CAMERA_FOLLOW_PLAYER -> ");
                break;
                case CAMERA_MOVING:
                    System.out.print("CAMERA_MOVING -> ");
                break;
                case CAMERA_MANUAL:
                    System.out.print("CAMERA_MANUAL -> ");
                break;
            }
            switch(cameraState){
                case CAMERA_STOPPED:
                    System.out.println("CAMERA_STOPPED");
                break;
                case CAMERA_FOLLOW_BALL:
                    System.out.println("CAMERA_FOLLOW_BALL");
                break;
                case CAMERA_GOTO_BALL_THEN_FOLLOW:
                    System.out.println("CAMERA_GOTO_BALL_THEN_FOLLOW");
                break;
                case CAMERA_FOLLOW_PLAYER:
                    System.out.println("CAMERA_FOLLOW_PLAYER");
                break;
                case CAMERA_MOVING:
                    System.out.println("CAMERA_MOVING");
                break;
                case CAMERA_MANUAL:
                    System.out.println("CAMERA_MANUAL");
                break;
            }
            //#endif
            // </editor-fold>
            this.cameraState = ( byte ) cameraState;
            setCamera3D();
        }
	}


	/**
	 * Atualiza o posicionamento da camera.
	 * @param fp_delta
	 * @param fp_cameraDelta
	 * @param isPaused
	 * @param ballRealPosition
	 */
	public final void updateCamera( int fp_delta, int fp_cameraDelta, boolean isPaused, Point3f ballRealPosition) {
        // reposiciona a câmera na tela
        switch ( cameraState ) {
            case CAMERA_MANUAL:
                fp_cameraCurrentTime += fp_cameraDelta;
				cameraPosition.addEquals( cameraSpeed.mul( fp_cameraDelta ) );
                //moveCamera( cameraSpeed.mul( fp_delta ) );
//				cameraPosition.addEquals( cameraSpeed.mul( fp_delta ) );
            break;

            case CAMERA_GOTO_BALL_THEN_FOLLOW:
            case CAMERA_MOVING:
                fp_cameraCurrentTime += fp_cameraDelta;
                //moveCamera( cameraSpeed.mul( fp_cameraDelta ) );
				cameraPosition.addEquals( cameraSpeed.mul( fp_cameraDelta ) );

                // <editor-fold defaultstate="collapsed" desc="Debug Message">
                //#if DEBUG == "true"
                System.out.println("cp = ("+cameraPosition.x+","+cameraPosition.y+","+cameraPosition.z+")"+" | ("+(cameraPosition.x>>16)+","+(cameraPosition.y>>16)+","+(cameraPosition.z>>16)+")");
                System.out.println("fp_cD = "+fp_cameraDelta+" | ("+cameraSpeed.x+","+cameraSpeed.y+","+cameraSpeed.z+")"+" | ("+(cameraSpeed.x>>16)+","+(cameraSpeed.y>>16)+","+(cameraSpeed.z>>16)+")");
                //#endif
                // </editor-fold>

                if ( fp_cameraCurrentTime >= fp_timeToDestination ) {
                    //cameraPosition.addEquals( cameraSpeed );
                    if(cameraState == CAMERA_MOVING) {
                        cancelCameraMovement(true);
                        //setCameraState(CAMERA_MANUAL);
                    } else {
                        setCameraState(CAMERA_FOLLOW_BALL);
                    }
                }
            break;

            case CAMERA_FOLLOW_BALL:
                if ( !isPaused ) {
                    cameraSpeed.set( ballRealPosition.sub( cameraPosition ).div( FP_CAMERA_FOLLOW_DEFAULT_TIME ) );
                    moveCamera( cameraSpeed.mul( fp_cameraDelta ) );
//						cameraPosition.addEquals( cameraSpeed.mul( fp_cameraDelta ) );
//						moveCameraTo( ball.getRealPosition(), FP_CAMERA_FOLLOW_DEFAULT_TIME );
                }
            break;
        }

        if(!camera2D) setCameraCenter(cameraPosition);
        else  setCameraCenter2D(cameraPosition2D);
    }

    public final void resetCameraState(byte state) {
		switch ( state ) {
			case MATCH_STATE_POSITION_BALL: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_CONTROL: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_MOVING: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_PLAY_RESULT: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_REPLAY: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_REPLAY_WAIT: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_SHOW_INFO: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_GAME_OVER: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_REPLAY_ONLY: setCameraState(CAMERA_FOLLOW_BALL); break;
			case MATCH_STATE_REPLAY_ONLY_WAIT: setCameraState(CAMERA_FOLLOW_BALL); break;
        }
    }

	public final void moveCamera( Point3f diff ) {
		moveCameraTo( cameraPosition.add( diff ) );
	}

	public final void moveCameraTo( Point3f position ) {
		moveCameraTo( position, 0 );
	}

	/**
	 * Move a câmera para a posição "position" real em "time" milisegundos. Caso "time" seja menor ou igual a zero, a câmera
	 * move-se instantaneamente para a posição recebida.
	 */
	 public final void moveCameraTo( Point3f position, int fp_time ) {
		final Point3f destination = new Point3f( position );
		// TODO
		destination.x = NanoMath.clamp( destination.x, FP_REAL_BOARD_X_LEFT, FP_REAL_BOARD_X_RIGHT );
		destination.z = NanoMath.clamp( destination.z, FP_REAL_BOARD_Z_MIN, FP_REAL_BOARD_Z_MAX );

//		if ( destination.y < 0 )
//			destination.y = 0;

		if ( fp_time > 1 ) {
			fp_timeToDestination = fp_time;
			fp_cameraCurrentTime = 0;
			setCameraState(CAMERA_MOVING);
			cameraSpeed.set( destination.sub( cameraPosition ).div( fp_time ) );
		} else {
//			cameraState = CAMERA_STOPPED;
			cameraPosition.set( destination );
			fp_timeToDestination = 0;
		}
	} // fim do método moveCameraTo

    private final Point ParallelogramClamp(Point pos){

        // TODO
        if(pos.y < p1.y ) { pos.y = p1.y; }
        else if(pos.y > p4.y) {  pos.y = p4.y; }
        if(pos.x < p2.x ) { pos.x = p2.x; }
        else if(pos.x > p3.x) {  pos.x = p3.x; }

        //if(pos.y >= p1.y && pos.y <= p4.y && pos.x >= p2.x && pos.x <= p3.x) {}

        if(p1.x != p2.x) a = NanoMath.toFixed(p1.y- p2.y)/(p1.x- p2.x); else a = 0;
        b = p1.y - NanoMath.toInt(a * p1.x);
        System.out.println("pos=("+pos.x+","+pos.y+")");
        System.out.println("p1=("+p1.x+","+p1.y+") p2=("+p2.x+","+p2.y+")");
        System.out.println("y = "+a+"x + ("+b+")");

        if(NanoMath.toFixed(pos.y) < ((pos.x*a)+NanoMath.toFixed(b))) {
            norma = NanoMath.sqrtFixed(NanoMath.mulFixed(a,a) + NanoMath.ONE);
            dist = NanoMath.divFixed((a*pos.x +NanoMath.toFixed(pos.y + b)),norma);
            if(dist < 0) dist = -dist;
            pos.x += NanoMath.toInt(NanoMath.divFixed(dist*1,norma));
            pos.y += NanoMath.toInt(NanoMath.divFixed(NanoMath.mulFixed(dist,a),norma));
        }

        return pos;
    }

	/**
	 * Cancela o movimento da câmera. Caso "goToDestination" seja true, a câmera termina o movimento que estava realizando.
	 * Caso contrário, apenas pára de se mover.
	 * @param goToDestination
	 */
	public final void cancelCameraMovement( boolean goToDestination ) {

        // <editor-fold defaultstate="collapsed" desc="Debug Message">
        //#if DEBUG == "true"
        System.out.println("->Call cancelCameraMovement");
        //#endif
        // </editor-fold>

		if ( goToDestination )
			cameraPosition.addEquals( cameraSpeed.mul( fp_timeToDestination - fp_cameraCurrentTime ) );

		fp_cameraCurrentTime = fp_timeToDestination;
		cameraSpeed.set();
		setCameraState(CAMERA_MANUAL);
	} // fim do método cancelCameraMovement()

    public final void setSize(int width, int height){
        CAMERA_LIMIT_X_LEFT = p2.x + ScreenManager.SCREEN_HALF_WIDTH;
        CAMERA_LIMIT_X_RIGHT = p3.x - ScreenManager.SCREEN_HALF_WIDTH;
        CAMERA_LIMIT_Y_UP = p1.y - ScreenManager.SCREEN_HALF_HEIGHT;
        CAMERA_LIMIT_Y_DOWN = p4.y + ScreenManager.SCREEN_HALF_HEIGHT;
    }
}
