package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import br.com.nanogames.components.util.Rectangle;
import core.Ball;
import core.Barrier;
import core.Constants;
import core.Control;
import core.Crosshair;
import br.com.nanogames.components.online.RankingScreen;
import core.Camera;
import core.Crowd;
import core.FieldLines;
import core.InfoBox;
import core.IsoArrow;
import core.Keeper;
import core.Player;
import core.PlayerBarrier;
import core.Replay;
import core.ReplayControl;
import core.TopTextBox;
import core.TrainingBarrier;

//#if DEBUG == "true"
import core.IsoQuad;
//#endif

public final class Match extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
//#if TOUCH == "true"
		, PointerListener 
//#endif
{
    // <editor-fold defaultstate="collapsed" desc="Constantes">
	// quantidade de placas de publicidade atrás do gol
	private static final byte ISO_N_BOARDS = 10;

	// duração dos timers em milisegundos
	private static final short TIME_PLAY_RESULT = 3000;
	// tempo que a barra leva para aparecer ou sumir
	private static final short TIME_PARTIAL_RESULT_ANIM = 700;
	private static final short TIME_PARTIAL_RESULT_ANIM_2 = ( TIME_PARTIAL_RESULT_ANIM << 1 );
	private static final short TIME_PARTIAL_RESULT = 5500;

	///////////////////////////////////////
	// definiçÃµes do modo desafio de faltas
	// distância mínima da lateral do campo que a bola deve estar para uma falta ser cobrada
	private static final int FP_FOUL_MIN_DISTANCE_TO_LINE = NanoMath.toFixed( 6 );
	// distância mínima da bola à linha de fundo no modo desafio de faltas
	private static final int FP_FOUL_MIN_Z_DISTANCE = NanoMath.toFixed( 11 );
	// distância z máxima que a bola pode estar para ser cobrada uma falta
	private static final int FP_FOUL_CHALLENGE_MAX_Z = NanoMath.toFixed( 30 );
	// pontuação mínima exigida para passar para o próximo nível
	private static final int FP_FOUL_CHALLENGE_MIN_SCORE = NanoMath.toFixed( 50 );
	// pontuação máxima exigida para passar para o próximo nível (pontuação mínima cresce com o nível)
	private static final int FP_FOUL_CHALLENGE_MAX_SCORE = NanoMath.toFixed( 270 );
	// incremento na pontuação mínima a cada nível
	private static final int FP_FOUL_CHALLENGE_SCORE_INCREASE = NanoMath.divInt( 18982, 10000 );
	// limites laterais para cobrança de falta
	private static final int FP_FOUL_CHALLENGE_LEFT_LIMIT = -FP_REAL_FIELD_WIDTH_HALF + FP_FOUL_MIN_Z_DISTANCE;
	private static final int FP_FOUL_CHALLENGE_RIGHT_LIMIT = FP_REAL_FIELD_WIDTH_HALF - FP_FOUL_MIN_Z_DISTANCE;

	// deslocamento em metros da bola ao posicioná-la
	public static final int FP_ISO_POSITION_BALL_SPEED = NanoMath.ONE;

	// limites para cobrança de falta
	private static final int FP_FOUL_LEFT_LIMIT = -FP_REAL_FIELD_WIDTH_HALF + FP_FOUL_MIN_DISTANCE_TO_LINE;
	private static final int FP_FOUL_RIGHT_LIMIT = FP_REAL_FIELD_WIDTH_HALF - FP_FOUL_MIN_DISTANCE_TO_LINE;
	private static final int FP_FOUL_MAX_Z_LIMIT = FP_REAL_FIELD_DEPTH_HALF - FP_FOUL_MIN_DISTANCE_TO_LINE;
	private static final int FP_FOUL_MIN_Z_LIMIT = FP_FOUL_MIN_DISTANCE_TO_LINE;

	// número máximo de componentes da partida
	private static final byte MAX_MATCH_COMPONENTS = 14;
	// número mínimo de componentes do campo: marcaçÃµes do campo, bola, 2 imagens do gol, placas de
	// publicidade, sombra da bola (número de jogadores é adicionado depois)
	private static final byte MIN_FIELD_COMPONENTS = ( ISO_N_BOARDS + 15 );

	// tempo em que as setas de scroll permanecem visíveis/invisíveis
	private static final short ARROWS_FRAME_TIME = 300;

	// índice de cada item da coleção das setas de scroll
	private static final byte ARROW_INDEX_ARROWS = 0;
	private static final byte ARROW_INDEX_ICON = 1;
	// índice do frame da seta amarela para cima
	private static final byte ARROW_FRAME_UP_INDEX = 2;

	// tempo em que o replay é exibido após a definição da jogada
	private static final short TIME_REPLAY = 4000;

	// tempo em segundos que o indicador de velocidade do replay permanece visível
	private static final short TIME_REPLAY_LABEL_VISIBLE = 850;

	// distância em pixels do label que mostra a velocidade do replay à borda da tela e ao controle do replay
	private static final byte REPLAY_LABEL_DISTANCE = 2;
    // </editor-fold>

	// estado da partida
	private byte state;

	private short timeToNextState;

	public static final byte MATCH_MODE_TRAINING		= 0;
	public static final byte MATCH_MODE_FOUL_CHALLENGE	= 1;
	public static final byte MATCH_MODE_REPLAY			= 2;

	// indica se está no modo treino
	private final byte matchMode;

	// índice da imagem (Pattern) do gramado
	private byte grassIndex;

	private final Pattern grass;

	// guarda a última posição da bola no modo de treino
	private final Point3f lastBallPosition = new Point3f( 0, 0, FP_REAL_BIG_AREA_DEPTH );
	// armazena a última posição do goleiro, para reiniciá-lo corretamente no replay
	private final Point3f lastKeeperPosition = new Point3f();

	/** Ãšltimo passo de tempo considerado. */
	private int fp_lastDelta;

	/***/
	private int lastDeltaModule;

	// indica se o jogo está paralisado
	private boolean paused;

	// tempo que o label do replay está visível
	private short accReplayLabelTime;

	/** Direção definida pela tecla sendo segurada atualmente, de acordo com o modo de controle escolhido. */
	private int keyDirection;

	/** Velocidade em metros por segundo do posicionamento da bola. */
	private static final int FP_BALL_POSITION_SPEED = 655360;

	/** bola da partida */
	private final Ball ball;

	/** Armazena se houve alguma mudança de posição da bola desde que o jogador começou a apertar a tecla, no estado 
	 * STATE_POSITION_BALL. Se a tecla for solta antes de haver mudança de posição, move a bola a distância padrão, para
	 * evitar dificuldades de posicionamento em aparelhos com resposta de teclas lenta ou com rolagem estilo BlackBerry.
	 */
	private boolean ballMoved;
	// coleção que contém todos os drawables do campo (campo, bola, linhas, jogadores, etc)
	private final DrawableGroup field;
	// controle do jogo
	private final Control control;

	/** Modo de controle da câmera e posicionamento da bola. */
	//private static byte keyMode;
	private static byte keyMode = KEY_MODE_ABSOLUTE; // KEY_MODE_ISO_UP_LEFT // KEY_MODE_ABSOLUTE

	// goleiro
	private final Keeper keeper;
	// jogador atualmente sendo controlado
	private final Player player;
	// barreira
	private final Barrier barrier;

	private final IsoArrow arrow;

	// imagens que formam o gol. São armazenadas para poder realizar a troca de ordem z da bola, de forma
	// que ela pareça estar dentro do gol ou passando por cima dele.
	private final DrawableImage goalBottom;
	private final DrawableImage goalTop;

    // <editor-fold defaultstate="collapsed" desc="Setas de indicacao">
	// setas que indicam ao jogador o controle de scroll e movimentação da bola
	private static final byte ARROW_UP		= 0;
	private static final byte ARROW_RIGHT	= 1;
	private static final byte ARROW_DOWN	= 2;
	private static final byte ARROW_LEFT	= 3;
	private final UpdatableGroup arrows;
	private short accArrowTime;
    // </editor-fold>

	/** Tempo utilizado para fazer os elementos (jogadores, bola, barreira) piscarem enquanto se estiver no posicionamento da bola. */
	private short accVisibleTime;

	/** Tempo de troca do estado de visibilidade dos elementos do jogo, durante o posicionamento da bola. */
	private static final short TIME_BLINK = 350;
	
    // <editor-fold defaultstate="collapsed" desc="Icones">
	private static final byte ICON_INFO		= 0;
	private static final byte ICON_KICK		= 1;
	private static final byte ICON_PAUSE	= 2;
	
	private static final byte ICONS_TOTAL	= 3;
	
	private final DrawableImage[] icons;
    // </editor-fold>

	/////////////////////////
	// modo desafio de faltas
	// alvo (modo desafio de faltas)
	private final Crosshair crosshair;
	// pontuação acumulada do jogador no modo desafio de faltas
	private int fp_accScore;
	// nível atual
	private short currentLevel = 1;
	// tentativa da falta atual
	private byte currentTry;
	private final int[] fp_tryScore = new int[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
	// pontuação mínima para passar para próximo nível
	private int fp_minTryScore = FP_FOUL_CHALLENGE_MIN_SCORE;
	// indica se o jogador ainda está jogando
	private boolean playing = true;
	// indica se a pontuação já foi atualizada após a jogada atual
	private boolean scoreUpdated;

	/** Caixa de texto que mostra as informaçÃµes do modo desafio de faltas. */
	private final InfoBox infoBox;

	/** Caixa de texto que mostra informaçÃµes gerais para o jogador. */
	private final TopTextBox topTextBox;

	//#if SCREEN_SIZE == "BIG"
		private final Point lastPointerPos = new Point();
	//#endif
		
	private final ReplayControl replayControl;
	private final Crowd crowd;
    
	private final Camera camera;

    private final Label gameOverLabel;

    // <editor-fold defaultstate="collapsed" desc="Contrutores">
	public Match( byte matchMode ) throws Exception {
		this( matchMode, null );
	}

	public Match( byte matchMode, Replay replay ) throws Exception {
		super( MAX_MATCH_COMPONENTS );

		this.matchMode = matchMode;

		final Point p = new Point();

		field = new DrawableGroup( MIN_FIELD_COMPONENTS );
		field.setSize( REAL_WORLD_SIZE, REAL_WORLD_SIZE );
		field.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		insertDrawable( field );

		grass = new Pattern( new DrawableImage( PATH_IMAGES + "g_" + NanoMath.randInt( GRASS_PATTERNS_TOTAL ) + ".png" ) ); 
		grass.setSize( field.getSize() );
		field.insertDrawable( grass );

		final DrawableImage mark = new DrawableImage( PATH_IMAGES + "mark.png" );
		mark.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		mark.setRefPixelPosition( GameMIDlet.isoGlobalToPixels( 0, 0, FP_REAL_PENALTY_TO_GOAL ) );
		field.insertDrawable( mark );

		final DrawableImage half = new DrawableImage( PATH_IMAGES + "half.png" );
		half.setPosition( GameMIDlet.isoGlobalToPixels( 0, 0, FP_REAL_BIG_AREA_DEPTH ).add( ISO_MEIA_LUA_X_OFFSET, ISO_MEIA_LUA_Y_OFFSET ) );
		field.insertDrawable( half );

		final DrawableImage circle = new DrawableImage( PATH_IMAGES + "circle.png" );
		circle.setPosition( GameMIDlet.isoGlobalToPixels( 0, 0, FP_REAL_FIELD_DEPTH_HALF ).add( ISO_CIRCLE_OFFSET_X, ISO_CIRCLE_OFFSET_Y ) );
		field.insertDrawable( circle );

		field.insertDrawable( new FieldLines() );

		crowd = new Crowd();
		field.insertDrawable( crowd );

		final DrawableImage cornerLeft = new DrawableImage( PATH_IMAGES + "c_l.png" );
		cornerLeft.setPosition( GameMIDlet.isoGlobalToPixels( -FP_REAL_FIELD_WIDTH_HALF, 0, 0 ).add( ISO_CORNER_LEFT_FLAG_OFFSET_X, ISO_CORNER_LEFT_FLAG_OFFSET_Y ) );
		field.insertDrawable( cornerLeft );

		final DrawableImage cornerRight = new DrawableImage( PATH_IMAGES + "c_r.png" );
		cornerRight.setPosition( GameMIDlet.isoGlobalToPixels( FP_REAL_FIELD_WIDTH_HALF, 0, 0 ).add( ISO_CORNER_RIGHT_FLAG_OFFSET_X, ISO_CORNER_RIGHT_FLAG_OFFSET_Y ) );
		field.insertDrawable( cornerRight );

		goalBottom = new DrawableImage( PATH_IMAGES + "goal_b.png" );
		p.set( GameMIDlet.isoGlobalToPixels( new Point3f() ) );
		goalBottom.setPosition( p.x + GOAL_BOTTOM_OFFSET_X, p.y + GOAL_BOTTOM_OFFSET_Y );
		field.insertDrawable( goalBottom );
		goalTop = new DrawableImage( PATH_IMAGES + "goal_t.png" );
		goalTop.setPosition( p.x + GOAL_TOP_OFFSET_X, p.y + GOAL_TOP_OFFSET_Y );
		field.insertDrawable( goalTop );

		//#if DEBUG == "true"
			// insere um quad mostrando a região interna do gol
			field.insertDrawable( new IsoQuad( new Quad(
					new Point3f( -FP_REAL_GOAL_WIDTH_HALF, 0, 0 ),
					new Point3f( -FP_REAL_GOAL_WIDTH_HALF, FP_REAL_FLOOR_TO_BAR ),
					new Point3f( FP_REAL_GOAL_WIDTH_HALF, 0, 0 )
					) ) );

			// insere outro quad mostrando a área externa das traves
			field.insertDrawable( new IsoQuad( new Quad(
					new Point3f( -FP_REAL_GOAL_WIDTH_HALF - FP_REAL_POST_WIDTH, 0, 0 ),
					new Point3f( -FP_REAL_GOAL_WIDTH_HALF - FP_REAL_POST_WIDTH, FP_REAL_FLOOR_TO_BAR + FP_REAL_POST_WIDTH ),
					new Point3f( FP_REAL_GOAL_WIDTH_HALF + FP_REAL_POST_WIDTH, 0, 0 )
					), 0x0000ff ) );


		//#endif

		arrow = new IsoArrow( 0xff0000 );
		field.insertDrawable( arrow );

		ball = new Ball( this );
		field.insertDrawable( ball );

		crosshair = new Crosshair();
		field.insertDrawable( crosshair );

		keeper = new Keeper( this );
		field.insertDrawable( keeper );

		switch ( matchMode ) {
			case MATCH_MODE_TRAINING:
				barrier = new TrainingBarrier( this );
			break;
			
			case MATCH_MODE_REPLAY:
				if ( replay.replayType == MATCH_MODE_TRAINING ) {
                    currentLevel = replay.level;
					barrier = new TrainingBarrier( this );
                    crowd.setPercent(getMaxCrowd());
					break;
				}

			case MATCH_MODE_FOUL_CHALLENGE:
			default:
				barrier = new PlayerBarrier( this );
		}
		field.insertDrawable( barrier );

		player = new Player( this );
		player.setListener( this, 0 );
		field.insertDrawable( player );

		control = new Control( this, arrow, ball );
		insertDrawable( control );

		arrows = new UpdatableGroup( 4 );
		final DrawableImage arrowRight = new DrawableImage( PATH_IMAGES + "ar.png" );
		arrowRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		final DrawableImage arrowUp = new DrawableImage( PATH_IMAGES + "au.png" );
		arrowUp.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		final DrawableImage arrowLeft = new DrawableImage( arrowRight );
		arrowLeft.mirror( TRANS_MIRROR_H );
		arrowLeft.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		final DrawableImage arrowDown = new DrawableImage( arrowUp );
		arrowDown.mirror( TRANS_MIRROR_V );
		arrowDown.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		arrows.insertDrawable( arrowUp );
		arrows.insertDrawable( arrowRight );
		arrows.insertDrawable( arrowDown );
		arrows.insertDrawable( arrowLeft );
		insertDrawable( arrows );

		replayControl = new ReplayControl( matchMode == MATCH_MODE_REPLAY, this );
		insertDrawable( replayControl );
		
		topTextBox = new TopTextBox();
		insertDrawable( topTextBox );

		field.setViewport( new Rectangle() );
		infoBox = new InfoBox( field.getViewport(), topTextBox.getPosition(), topTextBox.getSize() );
		updateInfoBox();
		insertDrawable( infoBox );

        gameOverLabel = new Label(0, TEXT_GAME_OVER);
        gameOverLabel.setVisible(false);
		insertDrawable( gameOverLabel );
		
		icons = new DrawableImage[ ICONS_TOTAL ];
		final String PATH_ICONS = PATH_IMAGES + "i_";
		for ( byte i = 0; i < ICONS_TOTAL; ++i )
			icons[ i ] = new DrawableImage( PATH_ICONS + i + ".png" );

        camera = new Camera(field);

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		switch ( matchMode ) {
			case MATCH_MODE_FOUL_CHALLENGE:
				setState( MATCH_STATE_POSITION_BALL );
			case MATCH_MODE_TRAINING:
				setState( MATCH_STATE_POSITION_BALL );
			break;

			case MATCH_MODE_REPLAY:
				loadReplay( replay );
				setState( MATCH_STATE_REPLAY_ONLY );
			break;

			//#if DEBUG == "true"
				default:
					throw new IllegalArgumentException( "Invalid matchMode: " + matchMode );
			//#endif
		}
	}
    // </editor-fold>

	public final int getDeltaFP() {
		return fp_lastDelta;
	}
    
    public final void setCameraStateFollowBall(){
        camera.setCameraState(CAMERA_FOLLOW_BALL);
    }

	/**
	 * Retorna os pontos acumulados da partida
	 * @return Pontos acumulados
	 */
	public final int getScore() {
		return fp_accScore;
	}

	private void updateArrows( int delta ) {
		accArrowTime += delta;

		if ( accArrowTime >= TIME_BLINK ) {
			accArrowTime %= TIME_BLINK;
			arrows.setVisible( !arrows.isVisible() );
		}
	}

	public final void update( int delta ) {
		if ( !paused ) {
			if ( timeToNextState > 0 ) {
				timeToNextState -= delta;
				if ( timeToNextState <= 0 ) {
					stateEnded();
				}
			}

			// converte o tempo de milisegundos para segundos
			int fp_delta = NanoMath.divInt( delta, 1000 );
			int fp_cameraDelta = fp_delta;
			
			switch ( state ) {
				case MATCH_STATE_POSITION_BALL:
					final Point3f speed = getSpeedVector( keyDirection, FP_BALL_POSITION_SPEED );
					setBallRealPosition( ball.getRealPosition().add( speed.mul( fp_delta ) ) );

					ballMoved = true;

					accVisibleTime -= delta;
					if ( accVisibleTime <= 0 ) {
						accVisibleTime += TIME_BLINK;
						setElementsVisible( !player.isVisible() );
					}

					updateArrows( delta );
				break;
				
				case MATCH_STATE_CONTROL:
					control.update( delta );
					updateArrows( delta );
				break;
				
				case MATCH_STATE_REPLAY:
				case MATCH_STATE_REPLAY_ONLY:
					//(CAMERA_FOLLOW_BALL);
					if ( !replayControl.isPaused() ) {
						accReplayLabelTime += delta;
						fp_lastDelta = NanoMath.mulFixed( fp_delta, replayControl.getSpeedMultiplier() );
						fp_cameraDelta = fp_lastDelta;

						if ( updateAllMoving( delta, true ) ) {
							stateEnded();
						}
					}
					updateArrows( delta );
//					if ( accReplayLabelTime >= TIME_REPLAY_LABEL_VISIBLE )
//						replayLabel.setVisible( false );
				break;

				case MATCH_STATE_REPLAY_WAIT:
				case MATCH_STATE_REPLAY_ONLY_WAIT:
					if ( !replayControl.isPaused() ) {
						fp_lastDelta = NanoMath.mulFixed( fp_delta, replayControl.getSpeedMultiplier() );
						fp_cameraDelta = fp_lastDelta;
						updateAllMoving( delta, true );
					}
				break;

				case MATCH_STATE_MOVING:
					control.update( delta );
					
					fp_lastDelta = fp_delta;

					if ( updateAllMoving( delta, false ) ) {
						stateEnded();
					}
				break;

				case MATCH_STATE_PLAY_RESULT:
					fp_lastDelta = fp_delta;
					updateAllMoving( delta, false );
				break;
			} // fim switch ( state )

            camera.updateCamera(fp_delta, fp_cameraDelta, replayControl.isPaused(), ball.getRealPosition());
		} // fim if ( !paused )

		topTextBox.update( delta );
		infoBox.update( delta );
	} // fim do método update( int )


	public final void setSize( int width, int height ) {
		if ( width != size.x || height != size.y ) {
			super.setSize( width, height );

            gameOverLabel.setPosition(((width/2)-(gameOverLabel.getWidth()/2)), height/2-(gameOverLabel.getHeight()/2));

			//replayControl.setSize(width, height);
			topTextBox.setSize( width, topTextBox.getHeight() );
			replayControl.setPosition( 0, topTextBox.getHeight() );

			arrows.setSize( width, height - topTextBox.getHeight() );
			arrows.setPosition( 0, topTextBox.getHeight() );
			arrows.getDrawable( ARROW_UP ).setRefPixelPosition( arrows.getWidth() >> 1, 0 );
			arrows.getDrawable( ARROW_DOWN ).setRefPixelPosition( arrows.getWidth() >> 1, arrows.getHeight() );
			arrows.getDrawable( ARROW_RIGHT ).setRefPixelPosition( arrows.getWidth(), arrows.getHeight() >> 1 );
			arrows.getDrawable( ARROW_LEFT ).setRefPixelPosition( 0, arrows.getHeight() >> 1 );

            camera.setSize(width, height);
		}
	}

	private void setElementsVisible( boolean b ) {
		player.setVisible( b );
		barrier.setVisible( b );
		keeper.setVisible( b );
	}

    public final void resetStateSoftKeys() {
		switch( state ) {
			case MATCH_STATE_POSITION_BALL:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, icons[ ICON_PAUSE ], true, -1 );
                break;

			case MATCH_STATE_CONTROL:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, icons[ ICON_PAUSE ], true, -1 );
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, icons[ ICON_KICK ], true, -1 );
                break;

			case MATCH_STATE_MOVING:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null, true, -1 );
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null, true, -1 );
                break;

			case MATCH_STATE_SHOW_INFO:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null, true, -1 );
                break;

			case MATCH_STATE_REPLAY:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, icons[ ICON_INFO ], true, -1 );
                break;

			case MATCH_STATE_REPLAY_ONLY:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, icons[ ICON_INFO ], true, -1 );
                break;

			case MATCH_STATE_GAME_OVER:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null, true, -1 );
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null, true, -1 );
                break;
        }
    }

	/**
	 * Método chamado para realizar transiçÃµes de estado da partida.
	 */
	private void stateEnded() {
		switch ( state ) { // obtém o estado atual da partida (o estado que será encerrado)
			case MATCH_STATE_POSITION_BALL: setState( MATCH_STATE_CONTROL ); break;
			case MATCH_STATE_CONTROL: setState( MATCH_STATE_MOVING ); break;
			case MATCH_STATE_MOVING: setState( MATCH_STATE_PLAY_RESULT ); break;
			case MATCH_STATE_PLAY_RESULT: setState( MATCH_STATE_REPLAY ); break;
			case MATCH_STATE_REPLAY: setState( MATCH_STATE_REPLAY_WAIT ); break;

			case MATCH_STATE_REPLAY_WAIT:
                if(matchMode == MATCH_MODE_TRAINING) setState( MATCH_STATE_POSITION_BALL );
                else setState( MATCH_STATE_SHOW_INFO );
			break;

			case MATCH_STATE_SHOW_INFO:
				if ( playing ) {
					if ( currentTry >= FOUL_CHALLENGE_TRIES_PER_FOUL )
						setLevel( currentLevel + 1 );
                    setState( MATCH_STATE_POSITION_BALL );
                } else {
					setState( MATCH_STATE_GAME_OVER );
				}
			break;

			case MATCH_STATE_GAME_OVER:
				switch ( GameMIDlet.isHighScore( getScore() ) ) {
					case RankingScreen.SCORE_STATUS_NEW_LOCAL_RECORD: break;
					case RankingScreen.SCORE_STATUS_NEW_PERSONAL_RECORD: break;
					case RankingScreen.SCORE_STATUS_NEW_PERSONAL_AND_LOCAL_RECORD: break;
					case RankingScreen.SCORE_STATUS_NONE:
                    default: GameMIDlet.gameOver( fp_accScore ); break;
				}
			break;

			case MATCH_STATE_REPLAY_ONLY:	setState( MATCH_STATE_REPLAY_ONLY_WAIT );	break;
			case MATCH_STATE_REPLAY_ONLY_WAIT: gameOverLabel.setVisible(false); GameMIDlet.setScreen( SCREEN_LOAD_REPLAY ); break;
		} // fim switch( state )
	} // fim método stateEnded()

	/**
	 * Define o estado da partida.
	 * @param newState
	 */
	public final void setState( int newState ) {
		//#if DEBUG == "true"
			final String[] states = new String[] { "STATE_CREATED", "STATE_BEGIN", "STATE_POSITION_BALL", "STATE_CONTROL", "STATE_REPLAY", "STATE_REPLAY_ONLY", "STATE_REPLAY_WAIT", "STATE_REPLAY_ONLY_WAIT", "STATE_SHOW_INFO", "STATE_MOVING", "STATE_PLAY_RESULT", "STATE_GAME_OVER" };
			System.out.println( "setMatchState: " + states[ state ] + " -> " + states[ newState ] );
		//#endif

		final byte previousState = state;
		state = ( byte ) newState;
		timeToNextState = 0;

		switch( state ) {
			case MATCH_STATE_POSITION_BALL:
                crowd.setPercent(getMaxCrowd());

				replayControl.setVisible(false);
				replayControl.setState( ReplayControl.STATE_HIDING_FULL );

				if ( matchMode == MATCH_MODE_TRAINING ) {
					arrows.setVisible( true );

					// bola é reposicionada na última posição onde foi cobrada uma falta
					ball.reset( lastBallPosition );

					topTextBox.setState( TopTextBox.STATE_APPEARING );
					topTextBox.setText( TEXT_POSITION_BALL );
				} else {
					infoBox.setState( InfoBox.STATE_HIDING );

					scoreUpdated = false;

					if ( currentTry % FOUL_CHALLENGE_TRIES_PER_FOUL == 0 ) {
						updateInfoBox();
						infoBox.setState( InfoBox.STATE_APPEARING );

						// bola é reposicionada aleatoriamente
						final Point3f ballPos = new Point3f();
						do {
							ballPos.x = FP_FOUL_CHALLENGE_LEFT_LIMIT + NanoMath.randFixed( FP_FOUL_CHALLENGE_RIGHT_LIMIT - FP_FOUL_CHALLENGE_LEFT_LIMIT );
							ballPos.z = FP_FOUL_MIN_Z_DISTANCE + NanoMath.randFixed( FP_FOUL_CHALLENGE_MAX_Z - FP_FOUL_MIN_Z_DISTANCE );
						} while ( !isValidFoulPosition( ballPos ) );

						ball.reset( ballPos );
						crosshair.reset( ball.getRealPosition() );
					} else {
						
						ball.reset( lastBallPosition ); // bola é reposicionada na última posição onde foi cobrada uma falta

//						pTryBox.setAnimation( TIME_PARTIAL_RESULT_ANIM, WAIT_TIME_INFINITE, BOX_POS_HCENTER | BOX_POS_BOTTOM_OUT, BOX_POS_HCENTER | BOX_POS_BOTTOM_IN );
//						pTryBox.setState( TopTextBox.STATE_APPEARING );
//
////							ISHELL_LoadResString( pA.m_pIShell, RES_FILE, TEXT_TRIES_LEFT + pGameInfo.languageOffset, temp, 30 * sizeof( AECHAR ) );
////							WSPRINTF( text, 60 * sizeof( AECHAR ), L"%d PTS / %d PTS   %s%d", ( short ) accTryScore, ( short ) minTryScore, temp, FOUL_CHALLENGE_TRIES_PER_FOUL - currentTry );
//
//						pTryBox.setText( text );
//						pTryBox.setTextMode( TEXT_SCROLL_LEFT );
					}
				}
				keeper.reset();
				updateZOrder();

                if(matchMode == MATCH_MODE_FOUL_CHALLENGE) stateEnded();
			break;

			case MATCH_STATE_CONTROL:
				replayControl.setVisible(false);
				setElementsVisible( true );
				arrows.setVisible( true );
				crosshair.setVisible( true );

				topTextBox.setState( TopTextBox.STATE_HIDING );
				if ( matchMode == MATCH_MODE_FOUL_CHALLENGE ) {
					infoBox.setState( InfoBox.STATE_HIDING );
				}

				// TODO melhorar posicionamento da câmera de forma a mostrar a bola E o gol
				final Point3f distance = ball.getRealPosition().mul( -NanoMath.ONE );
				final Point distPixels = GameMIDlet.isoGlobalToPixels( distance );

				// prepara a barreira
				barrier.prepare( ball.getRealPosition() );
				// reposiciona o goleiro de acordo com o número de jogadores na barreira
				keeper.prepare( ball.getRealPosition(), barrier.getNumberOfPlayers() );

				player.setVisible( true );

				control.reset();
				player.reset( ball.getRealPosition(), true );
				keeper.lookAt( ball.getRealPosition() );
			break; // fim case case STATE_CONTROL

			case MATCH_STATE_MOVING:
				// zera o tempo acumulado
				replayControl.setVisible(false);
				arrows.setVisible( false );

				if ( matchMode == MATCH_MODE_FOUL_CHALLENGE )
					crosshair.setVisible( true );

				player.setState( Player.PLAYER_SHOOTING );
			break; // fim case STATE_MOVING

			case MATCH_STATE_PLAY_RESULT:
				topTextBox.setState( TopTextBox.STATE_APPEARING );


				switch ( ball.getLastPlayResult() ) {
					case BALL_STATE_POST_GOAL:
					case BALL_STATE_GOAL:
						topTextBox.setText( TEXT_PLAY_RESULT_GOAL );
					break;

					case BALL_STATE_POST_OUT:
					case BALL_STATE_OUT:
						topTextBox.setText( TEXT_PLAY_RESULT_OUT );
					break;

					case BALL_STATE_REJECTED_KEEPER:
						topTextBox.setText( TEXT_PLAY_RESULT_DEFENDED );
					break;

					case BALL_STATE_REJECTED_BARRIER:
						topTextBox.setText( TEXT_PLAY_RESULT_BARRIER );
					break;

					case BALL_STATE_POST_BACK:
						topTextBox.setText( TEXT_PLAY_RESULT_POST );
					break;
				}
				timeToNextState = TIME_PLAY_RESULT;
			break; // fim case STATE_PLAY_RESULT

			case MATCH_STATE_REPLAY:
			case MATCH_STATE_REPLAY_ONLY:
				replayControl.setState( ReplayControl.STATE_APPEARING_FULL );
				replayControl.setPaused( false );
				replayControl.setVisible(true);


				if ( matchMode == MATCH_MODE_FOUL_CHALLENGE ) {
					infoBox.setState( InfoBox.STATE_HIDING );
				}

				switch ( previousState ) {
					case MATCH_STATE_REPLAY:
					case MATCH_STATE_REPLAY_WAIT:
					case MATCH_STATE_REPLAY_ONLY:
					case MATCH_STATE_REPLAY_ONLY_WAIT:
					break;

					default: replayControl.setSpeed( NanoMath.ONE );
					break;
				}

				ball.reset( lastBallPosition );
				keeper.reset( lastKeeperPosition );
				player.reset( lastBallPosition, true );
				player.setState( Player.PLAYER_SHOOTING );

				// desabilita a colisão com a barreira e goleiro, caso a bola tenha
				// passado por eles na jogada passada
				switch ( ball.getLastPlayResult() ) {
					case BALL_STATE_GOAL:
					case BALL_STATE_POST_GOAL:
					case BALL_STATE_POST_BACK:
					case BALL_STATE_OUT:
					case BALL_STATE_POST_OUT:
						keeper.setBallCollisionTest( false );
						barrier.setBallCollisionTest( false );
					break;

					default:
						barrier.setBallCollisionTest( true );
				}
			break; // fim case STATE_REPLAY / STATE_REPLAY_ONLY

			case MATCH_STATE_REPLAY_ONLY_WAIT:
			case MATCH_STATE_REPLAY_WAIT:
			break; // fim case STATE_REPLAY_WAIT

			case MATCH_STATE_SHOW_INFO:
		      	crosshair.setVisible( false );
				replayControl.setVisible(false);
				replayControl.setState( ReplayControl.STATE_HIDING_FULL );
				topTextBox.setState( TopTextBox.STATE_HIDING );
				if ( matchMode == MATCH_MODE_FOUL_CHALLENGE )
					infoBox.setState( InfoBox.STATE_APPEARING );
			break; // fim case STATE_SHOW_INFO

			case MATCH_STATE_GAME_OVER:
                gameOverLabel.setVisible(true);
				infoBox.setState( InfoBox.STATE_HIDING );
			break; // fim case STATE_GAME_OVER
		} // fim do switch(state)
        resetStateSoftKeys();
        camera.resetCameraState(state);
	} // fim do método setMatchState

	public final void saveReplayData( Replay replay ) {
		// preenche apenas as informaçÃµes necessárias no modo falta
		replay.replayType = matchMode;
		replay.playResult = ball.getLastPlayResult();
		replay.ballPosition.set( lastBallPosition );
		ball.saveReplayData( replay );
		replay.grassIndex = grassIndex;
		replay.keeperJumpSpeed.set( keeper.getJumpSpeed() );
		replay.keeperPosition.set( keeper.getInitialPosition() );
		replay.play.set( control.getCurrentPlay() );
		// armazena a data da gravação do replay
		replay.saveTime = System.currentTimeMillis();

		if ( matchMode == MATCH_MODE_FOUL_CHALLENGE )
			replay.crosshairPosition.set( crosshair.getRealPosition() );
	} // fim do método CB_fillReplayData()

	private int getMaxCrowd() {
		int percent = NanoMath.toInt((fp_minTryScore)/10)+ currentLevel;

		if (percent > Crowd.MAX_PEOPLE_PERCENT) {
			percent = Crowd.MAX_PEOPLE_PERCENT;
		}
		System.out.println(percent+"%");
		return Math.min(percent , Crowd.MAX_PEOPLE_PERCENT);
	}
		
	private void setLevel( int level ) {
		for ( byte i = 0; i < FOUL_CHALLENGE_TRIES_PER_FOUL; ++i )
			fp_tryScore[ i ] = 0;
		currentTry = 0;
		currentLevel = ( short ) level;

		// incrementa a pontuação mínima exigida para passar de nível
		fp_minTryScore = FP_FOUL_CHALLENGE_MIN_SCORE + NanoMath.mulFixed( FP_FOUL_CHALLENGE_SCORE_INCREASE, NanoMath.toFixed( level ) );
		if ( fp_minTryScore > FP_FOUL_CHALLENGE_MAX_SCORE )
			fp_minTryScore = FP_FOUL_CHALLENGE_MAX_SCORE;
	}

	/**
	 * Verifica se uma posição real é válida para a cobrança de uma falta, ou seja, se é dentro do campo e fora da área.
	 * @param pos
	 * @return
	 */
	private boolean isValidFoulPosition( Point3f pos ) {
		if ( ( pos.z >= FP_FOUL_MIN_Z_LIMIT ) && ( pos.x >= FP_FOUL_LEFT_LIMIT && pos.x <= FP_FOUL_RIGHT_LIMIT ) ) {
			// está dentro do campo
			if ( pos.z <= FP_REAL_BIG_AREA_DEPTH ) {
				// bola está na área, ou entre a área e as laterais
				if ( pos.x <= -FP_REAL_BIG_AREA_WIDTH_HALF || pos.x >= FP_REAL_BIG_AREA_WIDTH_HALF ) {
					// bola está entre a área e as laterais
					return true;
				}
			} else {
				// bola está entre a área e o restante do campo
				return true;
			}
		}

		// bola está fora do campo ou dentro da grande área
		return false;
	} // fim do método isValidFoulPosition()

	private Point3f getSpeedVector( int direction, int fp_speed ) {
		final Point3f speed = new Point3f();
        final int fp_speedCos60 = NanoMath.mulFixed(fp_speed, NanoMath.cosInt(60));

		switch ( direction ) {
			case ISO_DIRECTION_UP:
				speed.set( 0, 0, -fp_speed );
			break;

			case ISO_DIRECTION_DOWN:
				speed.set( 0, 0, fp_speed );
			break;

			case ISO_DIRECTION_LEFT:
				speed.set( -fp_speed , 0, 0 );
			break;

			case ISO_DIRECTION_RIGHT:
				speed.set( fp_speed, 0, 0 );
			break;

			case ISO_DIRECTION_UP_LEFT:
				speed.set( -fp_speed, 0, -fp_speed );
			break;

			case ISO_DIRECTION_UP_RIGHT:
				speed.set( fp_speedCos60, 0, -fp_speedCos60 );
//				speed.set( fp_speed, 0, -fp_speed );
			break;

			case ISO_DIRECTION_DOWN_LEFT:
				speed.set( -fp_speedCos60, 0, fp_speedCos60 );
//				speed.set( -fp_speed, 0, fp_speed );
			break;

			case ISO_DIRECTION_DOWN_RIGHT:
				speed.set( fp_speed, 0, fp_speed );
			break;
		}
		return speed;
	}

	public final byte getIsoDirection( int key ) {
		switch ( keyMode ) {
			case KEY_MODE_ABSOLUTE:
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						return ISO_DIRECTION_UP_LEFT;

					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						return ISO_DIRECTION_DOWN_RIGHT;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						return ISO_DIRECTION_DOWN_LEFT;

					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						return ISO_DIRECTION_UP_RIGHT;

					case ScreenManager.KEY_NUM1:
						return ISO_DIRECTION_LEFT;

					case ScreenManager.KEY_NUM3:
						return ISO_DIRECTION_UP;

					case ScreenManager.KEY_NUM7:
						return ISO_DIRECTION_DOWN;

					case ScreenManager.KEY_NUM9:
						return ISO_DIRECTION_RIGHT;
				}
			break;

			case KEY_MODE_ISO_UP_LEFT:
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						return ISO_DIRECTION_LEFT;

					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						return ISO_DIRECTION_RIGHT;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						return ISO_DIRECTION_DOWN;

					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						return ISO_DIRECTION_UP;

					case ScreenManager.KEY_NUM1:
						return ISO_DIRECTION_DOWN_LEFT;

					case ScreenManager.KEY_NUM3:
						return ISO_DIRECTION_UP_LEFT;

					case ScreenManager.KEY_NUM7:
						return ISO_DIRECTION_DOWN_RIGHT;

					case ScreenManager.KEY_NUM9:
						return ISO_DIRECTION_UP_RIGHT;
				}
			break;

			case KEY_MODE_ISO_UP_RIGHT:
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						return ISO_DIRECTION_UP;

					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						return ISO_DIRECTION_DOWN;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						return ISO_DIRECTION_LEFT;

					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						return ISO_DIRECTION_RIGHT;

					case ScreenManager.KEY_NUM1:
						return ISO_DIRECTION_UP_LEFT;

					case ScreenManager.KEY_NUM3:
						return ISO_DIRECTION_UP_RIGHT;

					case ScreenManager.KEY_NUM7:
						return ISO_DIRECTION_DOWN_LEFT;

					case ScreenManager.KEY_NUM9:
						return ISO_DIRECTION_DOWN_RIGHT;
				}
			break;
		}

		return ISO_DIRECTION_NONE;
	}

	/**
	 * Posiciona a bola manualmente no campo, respeitando os limites para cobrança de faltas.
	 * @param realPosition
	 */
	private void setBallRealPosition( Point3f realPosition ) {
		// mantém a bola dentro da região válida
		if ( !isValidFoulPosition( realPosition ) ) {
			// verifica qual o limite mais próximo da bola
			if ( realPosition.z < FP_REAL_BIG_AREA_DEPTH && realPosition.x > -FP_REAL_BIG_AREA_WIDTH_HALF && realPosition.x < FP_REAL_BIG_AREA_WIDTH_HALF ) {
				// está dentro da área
				final int xLeft = Math.abs( -FP_REAL_BIG_AREA_WIDTH_HALF - realPosition.x );
				final int xRight = Math.abs( FP_REAL_BIG_AREA_WIDTH_HALF - realPosition.x );
				final int z = FP_REAL_BIG_AREA_DEPTH - realPosition.z;

				if ( xLeft < xRight ) {
					if ( xLeft < z ) {
						// menor distância é para a esquerda
						realPosition.x = -FP_REAL_BIG_AREA_WIDTH_HALF;
					} else {
						// menor distância é à frente
						realPosition.z = FP_REAL_BIG_AREA_DEPTH;
					}
				} else {
					if ( xRight < z ) {
						// menor distância é para a direita
						realPosition.x = FP_REAL_BIG_AREA_WIDTH_HALF;
					} else {
						// menor distância é à frente
						realPosition.z = FP_REAL_BIG_AREA_DEPTH;
					}
				}
			} else {
				// está fora da região válida de faltas (linha de fundo ou laterais), mas fora da área
				realPosition.x = NanoMath.clamp( realPosition.x, FP_FOUL_LEFT_LIMIT, FP_FOUL_RIGHT_LIMIT );
				realPosition.z = NanoMath.clamp( realPosition.z, FP_FOUL_MIN_Z_LIMIT, FP_FOUL_MAX_Z_LIMIT );
			}
		}

		ball.setRealPosition( realPosition );
		camera.moveCameraTo( realPosition, FP_CAMERA_FOLLOW_DEFAULT_TIME );

		// atualiza as posiçÃµes do batedor, barreira e goleiro
		keeper.prepare( realPosition, BARRIER_MAX_N_PLAYERS );
		barrier.prepare( realPosition );
		player.reset( realPosition, true );
	}

	/**
	 * Atualiza a ordem de desenho dos drawables do campo.
	 */
	private void updateZOrder() {
		final Point3f ballPos = ball.getRealPosition();
		final Point3f playerPos = player.getRealPosition();

		if ( ballPos.z < 0 ) {
			switch ( ball.getState() ) {
				case BALL_STATE_POST_GOAL:
				case BALL_STATE_GOAL:
					 setZOrder( ball, goalBottom, 1 );
				break;
				
				case BALL_STATE_OUT:
				case BALL_STATE_POST_OUT:
					if ( ballPos.y > FP_REAL_FLOOR_TO_BAR ) {
						 setZOrder( ball, goalTop, 1 );
					} else {
						if ( ballPos.x < 0 )
							 setZOrder( ball, goalBottom, -1 );
						else
							 setZOrder( ball, goalTop, 1 );
					}
				break;
				
				default:
					 setZOrder( ball, keeper, 1 );
			}
		} else if ( ballPos.z <= barrier.getRealPosition().z ) {
			 setZOrder( ball, barrier, -1 );
		} else if ( ballPos.z <= playerPos.z ) {
			 setZOrder( ball, barrier, 1 );
		} else {
			 setZOrder( ball, player, 1 );
		}
	} // fim do método updateZOrder()

	private void setZOrder( Drawable d1, Drawable d2, int offset ) {
		int d1Index = -1;
		int d2Index = -1;

		try { // TODO try/catch em setZOrder (elimina travamentos aleatórios?)
			for ( int i = 0; i < field.getUsedSlots(); ++i ) {
				final Drawable d = field.getDrawable( i );

				if ( d == d2 ) {
					d2Index = i;
					if ( d1Index >= 0 )
						break;
				} else if ( d == d1 ) {
					d1Index = i;
					if ( d2Index >= 0 )
						break;
				}
			}

			//#if DEBUG == "true"
				if ( d2Index < 0 || d1Index < 0 )
					throw new RuntimeException( "setZOrder: índice inválido. dIndex: " + d2Index + ", ballIndex: " + d1Index );
			//#endif


			d1Index = Math.max( d1Index, 0 );
			d2Index = Math.max( d2Index + offset, 0 );

			if ( d1Index != d2Index && d1Index < field.getUsedSlots() && d2Index < field.getUsedSlots() )
				field.setDrawableIndex( d1Index, d2Index );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}

	/**
	 * Atualiza a pontuação do jogador e o estado da partida no modo desafio de faltas após uma falta cobrada.
	 * Retorna true caso a partida ainda esteja ativa, ou false caso o jogador tenha perdido.
	 * @return
	 */
	private boolean updateFoulChallengeScore() {
		scoreUpdated = true;
		final int fp_lastPlayScore = crosshair.calculateScore( ball );
		fp_tryScore[ currentTry ] = fp_lastPlayScore;
		
		int fp_accTryScore = 0;
		for ( byte i = 0; i < FOUL_CHALLENGE_TRIES_PER_FOUL; ++i )
			fp_accTryScore += fp_tryScore[ i ];

		fp_accScore += fp_lastPlayScore;

		// atualiza a caixa de texto da jogada antes que valores sejam (possivelmente) reiniciados
//		infoBox.setInfo( FOUL_TEXT_BOX_DURING_LEVEL, currentFoul, ( short ) minTryScore, ( int ) lastPlayScore, ( int ) accTryScore, ( int ) accScore );
//		infoBox.setAnimation( TIME_PARTIAL_RESULT_ANIM, WAIT_TIME_INFINITE, BOX_POS_HCENTER | BOX_POS_TOP_OUT, BOX_POS_HCENTER | BOX_POS_VCENTER );

		++currentTry;

		// se o jogador atingiu a meta e ainda restam chutes, passa de nível e dá ao jogador o máximo de pontos por cada chute restante
		if ( fp_accTryScore >= fp_minTryScore ) {
			while ( currentTry < FOUL_CHALLENGE_TRIES_PER_FOUL ) {
				fp_accScore += Crosshair.FP_BONUS_POINTS;
				fp_tryScore[ currentTry ] = Crosshair.FP_BONUS_POINTS;
				fp_accTryScore += Crosshair.FP_BONUS_POINTS;
				updateInfoBox();
				++currentTry;
			}
		}

		if ( currentTry >= FOUL_CHALLENGE_TRIES_PER_FOUL ) {
			// se a pontuação acumulada no total de tentativas por falta for menor do que o mínimo
			// necessário, retorna false (game over)
			if ( fp_accTryScore < fp_minTryScore ) {
				updateInfoBox();
				return false;
			}
		}
		
		updateInfoBox();

		return true;
	} // fim do método updateFoulChallengeScore()

	private void updateInfoBox() {
		infoBox.setTotalScore( fp_accScore );
		infoBox.setLevel( currentLevel );
		infoBox.setTryScore( currentTry - 1, fp_tryScore, fp_minTryScore );
	}

	private void loadReplay( Replay replay ) {
		// carrega alguns valores usados nos cálculos da bola e do goleiro
		ball.loadReplayData( replay );
		lastBallPosition.set( ball.getRealPosition() );

		barrier.prepare( ball.getRealPosition() );

		if ( matchMode == MATCH_MODE_FOUL_CHALLENGE )
			crosshair.setRealPosition( replay.crosshairPosition );

		control.setPlay( replay.play );

		arrows.setVisible( false );
		keeper.loadReplayData( replay );
		lastKeeperPosition.set( keeper.getInitialPosition() );
	} // fim do método loadReplay()

    public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case Player.SEQUENCE_SHOOTING:
				final boolean replay = state == MATCH_STATE_REPLAY || state == MATCH_STATE_REPLAY_ONLY;
				player.setSequence( Player.SEQUENCE_SHOT );

				// jogador acabou de chutar a bola
				// define a velocidade e direção da bola
				final Point3f ballDestination = ball.kick( control.getCurrentPlay(), replay );

				MediaPlayer.play( SOUND_INDEX_KICK_BALL );

//					control.hide(); TODO
				lastBallPosition.set( ball.getRealPosition() );

				lastKeeperPosition.set( keeper.getRealPosition() );
				keeper.setJumpDirection( ballDestination, barrier.getNumberOfPlayers(), ball.getTimeToGoal() );
				keeper.setState( Keeper.PLAYER_JUMPING ); // goleiro pula

				// testa se a bola acertará o alvo; caso acerte, força o goleiro a não defender o chute
				if ( !replay && matchMode != MATCH_MODE_TRAINING && crosshair.hasHitBullsEye( ballDestination ) )
					keeper.setBallCollisionTest( false );
			break;
		}
	}

	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}

	private boolean updateAllMoving( int delta, boolean replay ) {
		if ( replay ) {
			// altera o delta de forma a ficar compatível com a velocidade do replay
			final int percent = NanoMath.toInt( NanoMath.mulFixed( replayControl.getSpeedMultiplier(), NanoMath.toFixed( 100 ) ) );
			final int temp = lastDeltaModule + ( delta * percent );
			delta = temp / 100;
			lastDeltaModule = temp % 100;
		}
		final byte previousBallState = ball.getState();

		player.update( delta );
		barrier.update( delta );

		if ( previousBallState != BALL_STATE_STOPPED ) {
			ball.update( delta );

			ball.checkPlayerCollision( barrier, COLLISION_BARRIER );
			keeper.update( delta );
			ball.checkPlayerCollision( keeper, COLLISION_KEEPER );

			final byte currentBallState = ball.getState();

			if ( currentBallState != previousBallState ) {
				switch ( currentBallState ) {
					case BALL_STATE_POST_GOAL:
					case BALL_STATE_POST_BACK:
					case BALL_STATE_POST_OUT:
						MediaPlayer.play( SOUND_INDEX_POST );
					case BALL_STATE_REJECTED_BARRIER:
						if ( currentBallState == BALL_STATE_REJECTED_BARRIER && matchMode == MATCH_MODE_TRAINING )
							MediaPlayer.play( SOUND_INDEX_BOARD );
					case BALL_STATE_GOAL:
					case BALL_STATE_REJECTED_KEEPER:
					case BALL_STATE_OUT:
						if ( matchMode != MATCH_MODE_TRAINING && !scoreUpdated ) {
							playing = updateFoulChallengeScore();
						}
					break;
				} // fim switch ( ball.getState() )

				updateZOrder();
				return true;
			} // if ( ball.update( time ) != previousBallState )
		}
		updateZOrder();

		return false;
	}

    // <editor-fold defaultstate="collapsed" desc="SpriteListener">
    public void hideNotify(boolean deviceEvent) {
        
    }

    public void showNotify(boolean deviceEvent) {
        
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="KeyListener">
	public final void keyPressed( int key ) {

		switch ( infoBox.getState() ) {
			case InfoBox.STATE_HIDDEN:
			case InfoBox.STATE_HIDING:
			break;

			default:
				switch ( key ) {
					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
					case ScreenManager.SOFT_KEY_LEFT:
					case ScreenManager.SOFT_KEY_MID:
					case ScreenManager.SOFT_KEY_RIGHT:
						infoBox.setState( InfoBox.STATE_HIDING );

						switch ( state ) {
							case MATCH_STATE_SHOW_INFO:
								stateEnded();
								break;
							case MATCH_STATE_POSITION_BALL:
								stateEnded();
								break;
						}
					break;
				}
			return;
		}

		switch ( state ) {
			case MATCH_STATE_POSITION_BALL:
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.DOWN:
					case ScreenManager.LEFT:
					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM1: 
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						keyDirection = getIsoDirection( key );
                    break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_SOFT_LEFT:
						stateEnded();
					break;

					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
						GameMIDlet.setScreen( SCREEN_PAUSE );
					break;
					
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
						infoBox.toggleState();
					break;
				}
			break;

			case MATCH_STATE_CONTROL:
				switch (key) {
					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_SOFT_MID:
						if (control.confirm())
							stateEnded();
					break;

					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						camera.setCameraSpeed(getSpeedVector(getIsoDirection(key), FP_CAMERA_MOVE_SPEED));
					break;

					case ScreenManager.KEY_NUM0:
						//setCameraStateFollowBall();
					break;

					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
						GameMIDlet.setScreen( SCREEN_PAUSE );
					break;

					default:
						control.keyPressed( key );

				}
			break;

			case MATCH_STATE_REPLAY_ONLY:
			case MATCH_STATE_REPLAY_ONLY_WAIT:
			case MATCH_STATE_REPLAY_WAIT:
			case MATCH_STATE_REPLAY:
				switch ( key ) {
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						camera.setCameraSpeed( getSpeedVector( getIsoDirection( key ), FP_CAMERA_MOVE_SPEED ) );
					break;
					
					case ScreenManager.LEFT:
						replayControl.setPaused( false );
						replayControl.setSpeed( replayControl.getSpeedMultiplier() - ReplayControl.FP_REPLAY_SPEED_STEP );
					break;

					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_SOFT_MID:
						replayControl.setPaused( !replayControl.isPaused() );
						replayControl.setSpeed( replayControl.getSpeedMultiplier() );
					break;

					case ScreenManager.RIGHT:
						replayControl.setPaused( false );
						replayControl.setSpeed( replayControl.getSpeedMultiplier() + ReplayControl.FP_REPLAY_SPEED_STEP );
					break;

					case ScreenManager.DOWN:
						// reinicia o replay
						switch ( state ) {
							case MATCH_STATE_REPLAY:
							case MATCH_STATE_REPLAY_WAIT:
								setState( MATCH_STATE_REPLAY );
							break;

							case MATCH_STATE_REPLAY_ONLY:
							case MATCH_STATE_REPLAY_ONLY_WAIT:
								setState( MATCH_STATE_REPLAY_ONLY );
							break;
						}
					break;

					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_SOFT_LEFT:
						replayControl.toggleVisibility();
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						stateEnded();
					break;

					case ScreenManager.KEY_NUM0:
						//setCameraStateFollowBall();
					break;

					case ScreenManager.UP:
						switch ( state ) {
							case MATCH_STATE_REPLAY:
							case MATCH_STATE_REPLAY_WAIT:
								GameMIDlet.setScreen( SCREEN_SAVE_REPLAY );
							break;
						}
					break;

					default:
						switch ( state ) {
							case MATCH_STATE_REPLAY:
							case MATCH_STATE_REPLAY_ONLY:
								setState( state );
							break;
						}
				} // fim switch ( wParam )
			break;

			case MATCH_STATE_MOVING:
				switch ( key ) {
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
						GameMIDlet.setScreen( SCREEN_PAUSE );
					break;
				}
			break;

			case MATCH_STATE_GAME_OVER:
				// TODO adicionar tempo mínimo na tela de game over
				stateEnded();
			break;
		}
	} // fim do método keyPressed( int )

	public final void keyReleased( int key ) {
		switch ( state ) {
			case MATCH_STATE_POSITION_BALL:
				if ( !ballMoved ) {
					setBallRealPosition( getSpeedVector( keyDirection, FP_BALL_POSITION_SPEED ) );
				} else {
					ballMoved = false;
				}
				keyDirection = ISO_DIRECTION_NONE;
			break;

			case MATCH_STATE_CONTROL:
				control.keyReleased( key );

				switch ( key ) {
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						camera.cancelCameraMovement(false);
					break;
                }
//				if(cameraState == CAMERA_MANUAL){
//                    keyDirection = ISO_DIRECTION_NONE;
//                    cancelCameraMovement( false );
//                }
			break;

			case MATCH_STATE_REPLAY:
				switch ( key ) {
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						camera.cancelCameraMovement(false);
					break;
                }
            break;

			case MATCH_STATE_REPLAY_ONLY:
			case MATCH_STATE_REPLAY_WAIT:
			case MATCH_STATE_REPLAY_ONLY_WAIT:
//				if(cameraState == CAMERA_MANUAL){
//                    keyDirection = ISO_DIRECTION_NONE;
//                    cancelCameraMovement( false );
//                }
			break;
		}
	} // fim do método keyReleased( int )
    // </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ScreenListener">
	//#if TOUCH == "true"
	public final void onPointerDragged(int x, int y) {
		final Point3f diff = GameMIDlet.isoPixelsToGlobalY0(lastPointerPos.x - x, lastPointerPos.y - y);

		switch (state) {

			case MATCH_STATE_REPLAY:
			case MATCH_STATE_REPLAY_ONLY:
			case MATCH_STATE_REPLAY_WAIT:
			case MATCH_STATE_REPLAY_ONLY_WAIT:
				break;

			case MATCH_STATE_CONTROL:
				control.CameraFolowButtonShow();
				if (control.getState() != Control.STATE_HEIGHT_CURVE) {
					camera.moveCamera2D((x-lastPointerPos.x),(y-lastPointerPos.y));
				}				
				control.onPointerDragged(x, y);
				break;

			case MATCH_STATE_MOVING:
				break;
		}

		lastPointerPos.set(x, y);
	}

	public final void onPointerPressed(int x, int y) {
		switch (state) {
			case MATCH_STATE_CONTROL:
				//setCameraState(CAMERA_STOPPED);

				if (control.getState() == Control.STATE_HIDING) {
					if (GameMIDlet.softkeyLeft.contains(x, y)) {
						if(control.confirm()) { stateEnded(); lastPointerPos.set(x, y); return; }
					}
				}
				control.onPointerPressed(x, y);
            break;

			case MATCH_STATE_REPLAY:
			case MATCH_STATE_REPLAY_ONLY:
			case MATCH_STATE_REPLAY_WAIT:
			case MATCH_STATE_REPLAY_ONLY_WAIT:
				if (replayControl.contains(x, y)) {
					replayControl.onPointerPressed(x, y);
					break;
				} // fim if ( replayControls.contains( x, y ) )

            case MATCH_STATE_GAME_OVER: stateEnded(); lastPointerPos.set(x, y); return;
		}
        lastPointerPos.set(x, y);

		switch (infoBox.getState()) {
			case InfoBox.STATE_HIDDEN:
			case InfoBox.STATE_HIDING:
				break;

			default:
				infoBox.setState(InfoBox.STATE_HIDING);
				switch (state) {
					case MATCH_STATE_SHOW_INFO:
					case MATCH_STATE_POSITION_BALL:
						stateEnded();
						break;
				}
            return;
		}
	}

	public final void onPointerReleased(int x, int y) {
		switch (state) {
			case MATCH_STATE_CONTROL:
				control.onPointerReleased(x, y);
				break;
		}
	}
	//#endif
    // </editor-fold>
}