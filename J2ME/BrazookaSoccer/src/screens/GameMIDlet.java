/**
 * GameMIDlet.java
 * Â©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */
package screens;

import core.SoftLabel;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Border;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Graphics;
import core.Ball;

//#ifndef NO_RECOMMEND
	import br.com.nanogames.components.util.SmsSender;
	import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if JAR != "min"
	import core.ScrollBar;
	import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
	import br.com.nanogames.components.util.NanoMath;
	import br.com.nanogames.components.util.Point;
	import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Serializable;
import core.Crosshair;
import core.EdgeBorder;
import core.GiantHead;
import core.Replay;
import core.Tag;
	import java.util.Hashtable;
//#endif

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable {

	private static final short GAME_MAX_FRAME_TIME = 145;

	private static int softKeyRightText;
	//#if JAR != "min"
		/** gerenciador da animação da soft key esquerda */
		public static BasicAnimatedSoftkey softkeyLeft;

		/** gerenciador da animação da soft key direita */
		private static BasicAnimatedSoftkey softkeyRight;

	//#endif

	//#if JAR == "full"
		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
		private static final int LOW_MEMORY_LIMIT = 1050000;
		private static final int LOW_MEMORY_LIMIT_2 = 1350000;
	//#endif

	private static final byte BUFFER_SIZE = 32;
	private static final byte BUFFER_SIZE_MOD = BUFFER_SIZE - 1;
	private static final Point[] pointBuffer = new Point[ BUFFER_SIZE ];
	private static byte pointBufferIndex;

	private static byte indexMessage;

	/** ReferÃªncia para a tela de jogo, para que seja possível retornar à tela de jogo após entrar na tela de pausa. */
	private static Match playScreen;

	private static boolean lowMemory;

	private static LoadListener loader;

	/** Indica se o menu já foi exibido alguma vez (caso contrário, não mostra transição ao trocar para ele). */
	private boolean menuWasShown;

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif

	//#ifndef NO_RECOMMEND
		private Drawable recommendScreen;
	//#endif

		
	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#elif VENDOR == "MOTOROLA"
//# 			super( VENDOR_MOTOROLA, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
		//#endif

		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];

		for ( int i = 0; i < BUFFER_SIZE; ++i ) {
			pointBuffer[ i ] = new Point();
		}
			
//		// cria a base de dados do jogo
//		try {
//			createDatabase( "BLABLA", 1 );
//		} catch ( Exception e ) {
//		}
//
//		Logger.setRMS( "BLABLA", 1 );
//		log( 0 );
	}


	public static final void log( int i ) {
//		AppMIDlet.gc();
//		Logger.log( i + ": " + Runtime.getRuntime().freeMemory() );
	}


	protected final void loadResources() throws Exception {
		log( 1 );
		//#if JAR == "full"
			switch ( getVendor() ) {
				//#if SCREEN_SIZE == "BIG"
					case VENDOR_SAMSUNG:
//						lowMemory = Runtime.getRuntime().totalMemory() < LOAD_ALL_MEMORY_SAMSUNG;
//					break;

					case VENDOR_NOKIA:
					case VENDOR_MOTOROLA:
				//#endif
				case VENDOR_SONYERICSSON:
				case VENDOR_SIEMENS:
				case VENDOR_BLACKBERRY:
				case VENDOR_HTC:
				break;

				default:
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
		//#else
//# 			lowMemory = true;
		//#endif
//		lowMemory = true; // teste
//
//		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
//			FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
//
//			if ( i == FONT_TEXT )
//				FONTS[ i ].setCharExtraOffset( 1 );
//		}

			for (byte i = 0; i < FONT_TYPES_TOTAL; ++i) {
			switch (i) {
				default:
					FONTS[i] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);
			}
		}

		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}

		log( 7 );

		//#if BLACKBERRY_API == "true"
//#
//# 			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
//# 			int[][] keys = null;
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 },
//# 						{ 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 },
//# 						{ 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 },
//# 						{ 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 },
//# 						{ 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 },
//# 						{ 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 },
//# 						{ 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 },
//# 						{ 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 },
//# 						{ 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 },
//# 						{ 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 },
//# 						{ 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 },
//# 						{ 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 },
//# 						{ 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 },
//# 						{ 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 },
//# 						{ 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 },
//# 						{ 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR },
//# 						{ 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR },
//# 						{ 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR },
//# 						{ 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR },
//# 						{ 'S', ScreenManager.KEY_STAR },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 },
//# 						{ ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					keys = new int[][] {
//# 						{ 'w', ScreenManager.KEY_NUM1 },
//# 						{ 'W', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM3 },
//# 						{ 'R', ScreenManager.KEY_NUM3 },
//# 						{ 'z', ScreenManager.KEY_NUM7 },
//# 						{ 'Z', ScreenManager.KEY_NUM7 },
//# 						{ 'c', ScreenManager.KEY_NUM9 },
//# 						{ 'C', ScreenManager.KEY_NUM9 },
//# 						{ 'e', ScreenManager.KEY_NUM2 },
//# 						{ 'E', ScreenManager.KEY_NUM2 },
//# 						{ 's', ScreenManager.KEY_NUM4 },
//# 						{ 'S', ScreenManager.KEY_NUM4 },
//# 						{ 'd', ScreenManager.KEY_NUM5 },
//# 						{ 'D', ScreenManager.KEY_NUM5 },
//# 						{ 'f', ScreenManager.KEY_NUM6 },
//# 						{ 'F', ScreenManager.KEY_NUM6 },
//# 						{ 'x', ScreenManager.KEY_NUM8 },
//# 						{ 'X', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'y', ScreenManager.KEY_NUM1 },
//# 						{ 'Y', ScreenManager.KEY_NUM1 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'b', ScreenManager.KEY_NUM7 },
//# 						{ 'B', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'u', ScreenManager.UP },
//# 						{ 'U', ScreenManager.UP },
//# 						{ 'h', ScreenManager.LEFT },
//# 						{ 'H', ScreenManager.LEFT },
//# 						{ 'j', ScreenManager.FIRE },
//# 						{ 'J', ScreenManager.FIRE },
//# 						{ 'k', ScreenManager.RIGHT },
//# 						{ 'K', ScreenManager.RIGHT },
//# 						{ 'n', ScreenManager.DOWN },
//# 						{ 'N', ScreenManager.DOWN },
//# 					 };
//# 			}
//# 			for ( byte i = 0; i < keys.length; ++i )
//# 				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
//#
//# 			// em aparelhos antigos (e/ou com versÃµes de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms não recebe os eventos de teclado corretamente, sumindo somente após a aplicação ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
//#
		//#elif SCREEN_SIZE == "BIG"

			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
			final int[][] keys = {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );

		//#endif

		// inicializa as tabelas de ranking online
		//#if NANO_RANKING == "true"
			RankingScreen.init( new int[] { TEXT_HIGH_SCORES }, new RankingFormatter() {
				public final String format( int type, long score ) {
					return NanoMath.toString( ( int ) score );
				}

				public final void initLocalEntry( int type, RankingEntry entry, int index ) {
					entry.setScore( NanoMath.mulFixed( NanoMath.toFixed( RankingScreen.LOCAL_RANKING_ENTRIES_TOTAL - index ), Crosshair.FP_TOTAL_MAX_POINTS ) );
					
					final String[] names = new String[] {
						"piteco", "tommy", "vivs", "weifols", "cammy",
						"eddie", "maxxx", "monty", "dimmy", "peta",
						"mary", "miki", "nanoca", "dudris", "fulano",
						"beltrano", "cicrano", "huguinho", "zezinho", "luizinho"
					};
					entry.setNickname( names[ index % names.length ] );
				}
		} );
		//#endif	
		
		try {
			loadData( DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
				
			setLanguage( LANGUAGE_DEFAULT );
		}
		
		setScreen( SCREEN_LOADING_1 );
	} // fim do método loadResources()


	public final void destroy() {
		if ( playScreen != null ) {
//			HighScoresScreen.setScore( playScreen.getScore() ); TODO
			playScreen = null;
		}

		super.destroy();
	}

	private static final Drawable createBasicMenu( int index, int [] entries ) throws Exception {
        final Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			menuEntries[ i ] = new Tag( EdgeBorder.COLOR_BLUE, FONT_MENU, entries[ i ] );
		}

		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, menuEntries, ENTRIES_SPACING, 0, entries.length - 1, null );

		return menu;
	}


	public static final byte isHighScore( int score ) {
		//#if NANO_RANKING == "true"
			final Customer c = NanoOnline.getCurrentCustomer();
			try {
			//	return RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif
					
				return RankingScreen.SCORE_STATUS_NONE;
			}
			return 0;
		//#endif
	}


	public static final void gameOver( long score ) {
		//#if NANO_RANKING == "true"
			try {
			final Customer c = NanoOnline.getCurrentCustomer();
			if ( score > 0 && RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true , true) > 0 ) {//TODO 
					setScreen( SCREEN_LOADING_HIGH_SCORES );
				} else {
					setScreen( SCREEN_MAIN_MENU );
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				setScreen( SCREEN_MAIN_MENU );
		}
		//#else
//# 			if ( HighScoresScreen.setScore( score ) ) {
//# 				setScreen( SCREEN_HIGH_SCORES );
//# 			} else {
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			}
		//#endif
	}
	public static void  setMessageInfo(byte index){
		 indexMessage = index;
	}


	protected final int changeScreen( int screen ) throws Exception {
		log( 10 );
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;
		boolean useTransition = false;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_SPECIFIC;

	    byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;
		byte indexSoftMid = SOFT_KEY_REMOVE;

		short visibleTime = manager.hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME;

		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				final BasicConfirmScreen confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, getConfirmTitle( TEXT_DO_YOU_WANT_SOUND ), new Tag( TEXT_YES ), new Tag( TEXT_NO ), null, false );
				if ( !MediaPlayer.isMuted() ) {
					confirm.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
				}

				confirm.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				confirm.setSpacing( TITLE_SPACING, ENTRIES_SPACING );

				nextScreen = confirm;
				indexSoftLeft = TEXT_OK;
			break;
			
			case SCREEN_CHOOSE_LANGUAGE:
				nextScreen = createBasicMenu( screen, new String[] { "ENGLISH", "ESPAÑOL", "PORTUGUÊS" } );
				( ( BasicMenu ) nextScreen ).setCurrentIndex( getLanguage() );
				( ( BasicMenu ) nextScreen ).setPosition(( ( BasicMenu ) nextScreen ).getPosX(), ScreenManager.SCREEN_HALF_HEIGHT - ( ( BasicMenu ) nextScreen ).getHeight()/2);
				indexSoftLeft = TEXT_OK;
			break;			

			case SCREEN_SPLASH_NANO:
				//#if NANO_RANKING == "false"
//# 					HighScoresScreen.createInstance( DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
				//#endif

				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "SMALL"
//# 					nextScreen = new BasicSplashNano( SCREEN_INTRO, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, MediaPlayer.LOOP_INFINITE );
				//#else
					nextScreen = new BasicSplashNano( SCREEN_INTRO, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, MediaPlayer.LOOP_INFINITE );
				//#endif
			break;

			case SCREEN_INTRO:
				nextScreen = new Intro();
				bkgType = BACKGROUND_TYPE_NONE;
			break;

			case SCREEN_MAIN_MENU:
				playScreen = null;
				//#ifdef WEB_EMULATOR
//# 					nextScreen = createBasicMenu( screen, new int[] {
//#  							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_PRODUCTS_TITLE,
//# 							TEXT_RECOMMEND_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS } );
				//#else
					indexSoftRight = TEXT_EXIT;
					//#ifdef NO_RECOMMEND
//# 						nextScreen = createBasicMenu( screen, new int[] {
//# 								TEXT_NEW_GAME,
//# 								TEXT_OPTIONS,
//# 								TEXT_HIGH_SCORES,
//# 								TEXT_PRODUCTS_TITLE,
//# 								TEXT_HELP,
//# 								TEXT_CREDITS,
//# 								TEXT_EXIT, } );
					//#else
						//#if BLACKBERRY_API == "true"
//# 							nextScreen = createBasicMenu( screen, ( showRecommendScreen && SmsSender.isSupported() ) ? new int[] {
						//#else
								nextScreen = createBasicMenu( screen, new int[] {
						//#endif
								TEXT_NEW_GAME,
								TEXT_OPTIONS,
								TEXT_HIGH_SCORES,
								TEXT_REPLAY_TITLE,
							//	TEXT_RECOMMEND_TITLE,
								TEXT_HELP,
								TEXT_CREDITS,
								TEXT_EXIT,});
							
					//#endif
				//#endif

				if ( !menuWasShown ) {
					menuWasShown = true;
					useTransition = true;
				}
                ( ( BasicMenu ) nextScreen ).setPosition(( ( BasicMenu ) nextScreen ).getPosX(), ScreenManager.SCREEN_HALF_HEIGHT - ( ( BasicMenu ) nextScreen ).getHeight()/2);
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_GAME_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_FOUL_CHALLENGE,
					TEXT_TRAINING,
					TEXT_BACK
				} );
				 ( ( BasicMenu ) nextScreen ).setPosition(( ( BasicMenu ) nextScreen ).getPosX(), ScreenManager.SCREEN_HALF_HEIGHT - ( ( BasicMenu ) nextScreen ).getHeight()/2);
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_NEXT_LEVEL:
//				playScreen.prepareNextLevel();
//				if ( playScreen.getLevel() <= 1 )
//					useTransition = true;
			case SCREEN_RETRY:
			case SCREEN_CONTINUE_GAME:
//				if ( screen == SCREEN_RETRY )
//					retryLevel = true;

				nextScreen = playScreen;
				bkgType = BACKGROUND_TYPE_NONE;

				//indexSoftRight = TEXT_PAUSE;
//
//				if ( screen == SCREEN_CONTINUE_GAME ) {
//					playScreen.sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//					playScreen.setState( PlayScreen.STATE_UNPAUSING );
//				}
			break;

			case SCREEN_LOAD_REPLAY:
			case SCREEN_SAVE_REPLAY:
				nextScreen = new ReplayScreen( getFont( FONT_MENU ), playScreen, screen );
			break;

			case SCREEN_OPTIONS:

			if (MediaPlayer.isVibrationSupported()) {
					nextScreen = createBasicMenu(screen, new int[]{
						MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
						MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON,
						TEXT_BACK,
						TEXT_VOLUME,
						});
				} else {
					nextScreen = createBasicMenu(screen, new int[]{
							MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
							TEXT_BACK,
							TEXT_VOLUME,
							});
				}
			 ( ( BasicMenu ) nextScreen ).setPosition(( ( BasicMenu ) nextScreen ).getPosX(), ScreenManager.SCREEN_HALF_HEIGHT - ( ( BasicMenu ) nextScreen ).getHeight()/2);
			break;
			
			//#if NANO_RANKING == "true"

			case SCREEN_HIGH_SCORES:
				setSpecialKeyMapping( false );

				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_NANO_RANKING_MENU:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_NANO_ONLINE:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						MediaPlayer.free();
						setSpecialKeyMapping( false );

						nanoOnlineForm = NanoOnline.load( language, APP_SHORT_NAME, SCREEN_MAIN_MENU );

						loadScreen.setActive( false );

						setScreen( SCREEN_NANO_RANKING_MENU );
					}

				} );

				break;

			case SCREEN_NANO_RANKING_PROFILES:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_PROFILES_SCREEN:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						setSpecialKeyMapping( false );
						nanoOnlineForm = NanoOnline.load( language, APP_SHORT_NAME, SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT );

						loadScreen.setActive( false );

						setScreen( SCREEN_NANO_RANKING_PROFILES );
					}
				} );
				break;

			case SCREEN_CHOOSE_PROFILE:
				setSpecialKeyMapping( false );

				final Customer c = NanoOnline.getCurrentCustomer();

				// a fonte só possui caracteres maiúsculos
				final String name = ( getText( TEXT_CURRENT_PROFILE ) + ( c.getId() == Customer.ID_NONE ? getText( TEXT_NO_PROFILE ) : c.getNickname() ) ).toUpperCase();

				final BasicConfirmScreen profileScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null,
															getConfirmTitle( name ), new Tag( TEXT_CONFIRM ), new Tag( TEXT_CHOOSE_ANOTHER ), new Tag( TEXT_BACK ), false );

				profileScreen.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				profileScreen.setSpacing( TITLE_SPACING, ENTRIES_SPACING );
				profileScreen.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
				profileScreen.setCircular( true );

				nextScreen = profileScreen;
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_LOADING_HIGH_SCORES:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						MediaPlayer.free();

						setSpecialKeyMapping( false );
						playScreen = null;
						nanoOnlineForm = NanoOnline.load( language, APP_SHORT_NAME, SCREEN_MAIN_MENU, NanoOnline.SCREEN_NEW_RECORD );

						loadScreen.setActive( false );

						setScreen( SCREEN_HIGH_SCORES );
					}

				} );
				break;

			//#else
//# 				case SCREEN_HIGH_SCORES:
//# 					nextScreen = HighScoresScreen.createInstance( DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
//# 					indexSoftRight = TEXT_BACK;
//# 				break;
			//#endif
			
			case SCREEN_HELP_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
							TEXT_OBJECTIVES,
							TEXT_CONTROLS,
							TEXT_BACK, } );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
				 ( ( BasicMenu ) nextScreen ).setPosition(( ( BasicMenu ) nextScreen ).getPosX(), ScreenManager.SCREEN_HALF_HEIGHT - ( ( BasicMenu ) nextScreen ).getHeight()/2);
			break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
				String title = null;
				switch (screen) {
					case SCREEN_HELP_OBJECTIVES:
						title = getText(TEXT_OBJECTIVES);
						break;

					case SCREEN_HELP_CONTROLS:
						title = getText(TEXT_CONTROLS);
						break;

				}
				nextScreen = new BasicTextScreen(SCREEN_HELP_MENU, getFont(FONT_TEXT), getText(TEXT_HELP_OBJECTIVES + (screen - SCREEN_HELP_OBJECTIVES)) + getText(TEXT_VERSION) + getMIDletVersion() + "\n\n", false);
				break;

			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_MENU ), TEXT_CREDITS_TEXT, true );

				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_PAUSE:
				//#if JAR == "min"
//# 					BasicOptionsScreen pauseScreen = null;
//# 					if ( MediaPlayer.isVibrationSupported() ) {
//# 						pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU ), new int[] {
//# 							TEXT_CONTINUE,
//# 							TEXT_TURN_SOUND_OFF,
//# 							TEXT_TURN_VIBRATION_OFF,
//# 							TEXT_BACK_MENU,
//# 							TEXT_EXIT_GAME,
//# 						}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
//# 					} else {
//# 						pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU ), new int[] {
//# 							TEXT_CONTINUE,
//# 							TEXT_TURN_SOUND_OFF,
//# 							TEXT_BACK_MENU,
//# 							TEXT_EXIT_GAME,
//# 						}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );
//# 					}
//# 					pauseScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
						if ( MediaPlayer.isVibrationSupported() ) {
							nextScreen = createBasicMenu( screen, new int[] {
								TEXT_CONTINUE,
								MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
								MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON,
								TEXT_BACK_MENU,

								//#ifndef WEB_EMULATOR
									TEXT_EXIT_GAME,
								//#endif

								TEXT_VOLUME,
							});
						
					}
				//#endif
              ( ( BasicMenu ) nextScreen ).setPosition(( ( BasicMenu ) nextScreen ).getPosX(), ScreenManager.SCREEN_HALF_HEIGHT - ( ( BasicMenu ) nextScreen ).getHeight()/2);
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;
			break;

			case SCREEN_CONFIRM_MENU:
				//#if JAR == "min"
//# 					nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO, true );
//# 					( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
					 if ( isLowMemory() ) {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO, false );
					 } else {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, getConfirmTitle( TEXT_CONFIRM_BACK_MENU ), new Tag( TEXT_YES ), new Tag( TEXT_NO ), null, false );
					 }
				//#endif
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				( ( BasicConfirmScreen ) nextScreen ).setSpacing( TITLE_SPACING, ENTRIES_SPACING );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT:
				//#if JAR == "min"
//# 					nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, TEXT_CONFIRM_EXIT, TEXT_YES, TEXT_NO, true );
//# 					( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
					if ( isLowMemory() ) {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, TEXT_CONFIRM_EXIT, TEXT_YES, TEXT_NO, true );
					} else {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), null, getConfirmTitle( TEXT_CONFIRM_EXIT ), new Tag( TEXT_YES ), new Tag( TEXT_NO ), null, true );
					}
				//#endif
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				( ( BasicConfirmScreen ) nextScreen ).setSpacing( TITLE_SPACING, ENTRIES_SPACING );
				indexSoftLeft = TEXT_OK;
			break;

			//#if DEMO == "true"
//# 			
//# 			// versão demo
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, midlet.cursor, TEXT_BUY_FULL_GAME_TEXT, TEXT_YES, TEXT_NO );
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
//# 			case SCREEN_PLAYS_REMAINING:
//# 				final String text = playsRemaining > 1 ? 
//# 						getText( TEXT_2_OR_MORE_PLAYS_REMAINING_1 ) + playsRemaining + getText( TEXT_2_OR_MORE_PLAYS_REMAINING_2 ) :
//# 						getText( TEXT_1_PLAY_REMAINING );
//# 				nextScreen = new BasicTextScreen( midlet, index, FONT_DEFAULT, text, true );
//# 						
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
			//#endif

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								// aloca os sons
								final String[] soundList = new String[ SOUND_TOTAL ];
								for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
									soundList[i] = PATH_SOUNDS + i + ".mid";
									log( 11 );
								}

								MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								//#if BLACKBERRY_API == "true"
//# 									MediaPlayer.setVolume( 60 );
								//#endif

								log( 13 );

								Ball.loadImages();
								
								softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT, BasicAnimatedSoftkey.DIRECTION_HORIZONTAL );
								softkeyLeft.setTransitionTime( TIME_SOFTKEY_TRANSITION );
								softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT, BasicAnimatedSoftkey.DIRECTION_HORIZONTAL );
								softkeyRight.setTransitionTime( TIME_SOFTKEY_TRANSITION );

								log( 22 );
								manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
								manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );

								loadScreen.setActive( false );

								setScreen( SCREEN_CHOOSE_LANGUAGE );
							}

						} );

				useTransition = true;
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						log( 23 );

						ReplayScreen.loadReplays();

						//#if JAR != "min"
							log( 24 );
							ScrollBar.loadImages();
							log( 25 );
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						//#endif
						
						loadScreen.setActive( false );

						setScreen( SCREEN_MAIN_MENU );
					}

				} );

				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_GAME_FULL:
			case SCREEN_LOADING_GAME_TRAINING:
			case SCREEN_SHOW_REPLAY:
				final byte chosenScreen = ( byte ) screen;
				final Replay replay = screen == SCREEN_SHOW_REPLAY ? ReplayScreen.getSelectedReplay(): null;
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						switch ( chosenScreen ) {
							case SCREEN_LOADING_GAME_FULL:
								playScreen = new Match( Match.MATCH_MODE_FOUL_CHALLENGE, replay );
							break;

							case SCREEN_LOADING_GAME_TRAINING:
								playScreen = new Match( Match.MATCH_MODE_TRAINING, replay );
							break;

							default:
								playScreen = new Match( Match.MATCH_MODE_REPLAY, replay );
						}

						loadScreen.setActive( false );

						setScreen( SCREEN_NEXT_LEVEL );
					}

				} );

				MediaPlayer.free();
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen( loader );
				loader = null;

				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			//#ifndef NO_RECOMMEND

				case SCREEN_RECOMMEND_SENT:
					nextScreen = new BorderedTextScreen( getText( TEXT_RECOMMEND_SENT ) + getRecommendURL() + "\n\n", SCREEN_MAIN_MENU );
					indexSoftRight = TEXT_BACK;
				break;

				case SCREEN_RECOMMEND:
					nextScreen = new Form( new ScreenRecommend( SCREEN_MAIN_MENU ) );
					indexSoftRight = TEXT_BACK;
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				break;

			//#endif

			
		} // fim switch ( screen )


		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft, visibleTime );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, visibleTime );

		if ( indexSoftMid != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, indexSoftMid, visibleTime );

		manager.setCurrentScreen( nextScreen );
        if(nextScreen instanceof Match) ((Match)nextScreen).resetStateSoftKeys();
        if(nextScreen instanceof ReplayScreen) ((ReplayScreen)nextScreen).resetStateSoftKeys();
		setBackground( bkgType );

		return screen;
	} // fim do método changeScreen( int )


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SPECIFIC:
				final GiantHead bkg = new GiantHead();
//				bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				midlet.manager.setBackground( bkg, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
			break;
		}
	} // fim do método setBackground( byte )


	public static final Drawable getScrollFull() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollF = new Pattern( COLOR_SCROLL_BACK );
//# 			scrollF.setSize( 8, 0 );
//# 			return scrollF;
		//#else
			return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
		//#endif
	}


	public static final Drawable getScrollPage() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollP = new Pattern( COLOR_SCROLL_FORE );
//# 			scrollP.setSize( 8, 0 );
//# 			return scrollP;
		//#else
			return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
		//#endif
	}
	
	
//	private static final Drawable createBasicMenu( int index, int[] entries ) throws Exception {
//		final String[] stringEntries = new String[ entries.length ];
//		for ( byte i = 0; i < stringEntries.length; ++i )
//			stringEntries[ i ] = getText( entries[ i ] );
//
//		return createBasicMenu( index, stringEntries );
//	}


	private static final Drawable createBasicMenu( int index, String[] entries ) throws Exception {
		final  Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			menuEntries[ i ] = new Tag( EdgeBorder.COLOR_BLUE, FONT_MENU, entries[ i ] );
		}

		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, menuEntries, ENTRIES_SPACING, 0, entries.length - 1, null );

		return menu;
	}


	public static final boolean isLowMemory() {
		return lowMemory;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif
					
				//#ifdef NO_RECOMMEND
//# 					if ( index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//# 						++index;
//# 					}
				//#endif

				/*//#if JAR != "min"
				//#ifndef WEB_EMULATOR
				if ( !SmsSender.isSupported() && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
				++index;
				}
				//#endif
				//#endif*/

				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						setScreen( SCREEN_GAME_MENU );
						break;

					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
						break;

					case ENTRY_MAIN_MENU_HIGH_SCORES:
						//#if NANO_RANKING == "true"
							setScreen( SCREEN_LOADING_NANO_ONLINE );
						//#else
//# 							setScreen( SCREEN_HIGH_SCORES );
						//#endif
						break;

					case ENTRY_MAIN_MENU_REPLAY:
						 setScreen( SCREEN_LOAD_REPLAY );
						break;

					//case ENTRY_MAIN_MENU_RECOMMEND:
					//	setScreen( SCREEN_RECOMMEND );
					//	break;

					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
						break;

					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
						break;


					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
						break;
				} // fim switch ( index )
				break; // fim case SCREEN_MAIN_MENU

			case SCREEN_GAME_MENU:
				switch ( index ) {
					case ENTRY_GAME_MENU_CHALLENGE:
						setScreen( SCREEN_CHOOSE_PROFILE );
					break;

					case ENTRY_GAME_MENU_TRAINING:
						setScreen( SCREEN_LOADING_GAME_TRAINING );
					break;

					case ENTRY_GAME_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;
				}
			break;
			
				//#if NANO_RANKING == "true"
					case SCREEN_CHOOSE_PROFILE:
						switch ( index ) {
							case BasicConfirmScreen.INDEX_YES:
								setScreen( SCREEN_LOADING_GAME_FULL );
							break;

							case BasicConfirmScreen.INDEX_NO:
								setScreen( SCREEN_LOADING_PROFILES_SCREEN );
							break;

							case BasicConfirmScreen.INDEX_CANCEL:
								setScreen( SCREEN_GAME_MENU );
							break;
						}
					break;
				//#endif				

			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
						break;

					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
						break;

					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;

			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
							case ENTRY_PAUSE_MENU_VOLUME:
							MediaPlayer.setVolume( ( MediaPlayer.getVolume() + 25 ) % 125 );
							Tag.setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE

			case SCREEN_OPTIONS:
				switch (index) {

					case ENTRY_OPTIONS_MENU_TOGGLE_SOUND:
						MediaPlayer.play(SOUND_INDEX_WHISTLE );
						MediaPlayer.setMute( !MediaPlayer.isMuted() );
						setScreen(SCREEN_MAIN_MENU);
						MediaPlayer.saveOptions();
						break;
					case ENTRY_OPTIONS_MENU_VIB_BACK:
						MediaPlayer.setVibration( !MediaPlayer.isVibration() );
						MediaPlayer.saveOptions();
						setScreen(SCREEN_MAIN_MENU);
						break;
					case ENTRY_OPTIONS_MENU_VOLUME:
							MediaPlayer.setVolume( ( MediaPlayer.getVolume() + 25 ) % 125 );
							Tag.setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
						break;
					
					case ENTRY_OPTIONS_MENU_BACK:
						MediaPlayer.saveOptions();
						setScreen(SCREEN_MAIN_MENU);
						break;
				}
				break;

			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						//#if NANO_RANKING == "true"
							gameOver( playScreen.getScore() );
						//#else
							//#if DEMO == "false"
//# 								HighScoresScreen.setScore( playScreen.getScore() );
//# 								playScreen = null;
//# 								MediaPlayer.saveOptions();
//# 								setScreen( SCREEN_MAIN_MENU );
							//#else
	//# 						gameOver( 0 );
							//#endif
						//#endif
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );

				setScreen( SCREEN_SPLASH_NANO );
				break;
				
			case SCREEN_CHOOSE_LANGUAGE:
				setLanguage( ( byte ) index );
				try {
					saveData( DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this );
				} catch ( Exception ex ) {
				}
				setScreen( SCREEN_CHOOSE_SOUND );
			break;
		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		for ( byte i = 0; i < menu.getTotalSlots(); ++i ) {
			if ( menu.getDrawable( i ) instanceof Tag ) {
				( ( Tag ) menu.getDrawable( i ) ).setSelected( i == index );
			}
		}
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		//#if JAR != "min"
			if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
				softKeyRightText = textIndex;
		//#endif
		
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new SoftLabel( softKey, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do método setSoftKeyLabel( byte, int )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	//#endif
	} // fim do método setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}

	protected ImageFont getFont(int index) {
		//#if JAR == "min"
//# 			switch ( index ) {
//# 				case FONT_POINTS:
//# 				case FONT_BIG:
//# 					return FONTS[ index ];
//#
//# 				default:
//# 					return FONTS[ 0 ];
//# 			}
		//#else
		return FONTS[index];
		//#endif
	}


	private static final RichLabel getConfirmTitle( int textIndex ) throws Exception {
		return new RichLabel( FONT_MENU, textIndex, ScreenManager.SCREEN_WIDTH * 9 / 10 );
	}


	private static final RichLabel getConfirmTitle( String text ) throws Exception {
		return new RichLabel( FONT_MENU, text, ScreenManager.SCREEN_WIDTH * 9 / 10 );
	}

	
	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}

		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
		}
	}


	public static String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	// converte uma posição do mundo real para pixels
	public static final Point isoGlobalToPixels( Point3f realPosition ) {
		return isoGlobalToPixels( realPosition.x, realPosition.y, realPosition.z );
	}
    

	public static final Point isoGlobalToPixels( int fp_realX, int fp_realY, int fp_realZ ) {
		final Point p = pointBuffer[ pointBufferIndex ];
		pointBufferIndex = ( byte ) ( ( pointBufferIndex + 1 ) & BUFFER_SIZE_MOD );

		// obs.: eixo y real não causa offset em x
		p.x = ( REAL_WORLD_OFFSET ) + NanoMath.toInt(
			  NanoMath.mulFixed( FP_ISO_XREAL_X_OFFSET, fp_realX ) +	// offset em x causado pelo eixo x real
			  NanoMath.mulFixed( FP_ISO_ZREAL_X_OFFSET, fp_realZ ) );	// offset em x causado pelo eixo z real

		p.y = ( REAL_WORLD_OFFSET ) + NanoMath.toInt(
			  // offset em y causado pelo eixo y real
			  NanoMath.divFixed( NanoMath.mulFixed( -fp_realY, FP_ISO_FLOOR_TO_BAR ), FP_REAL_FLOOR_TO_BAR ) +
			  // offset em y causado pelo eixo x real
			  NanoMath.mulFixed( FP_ISO_XREAL_Y_OFFSET, fp_realX ) +
			  // offset em y causado pelo eixo z real
			  NanoMath.mulFixed( FP_ISO_ZREAL_Y_OFFSET, fp_realZ ) );

		return p;
	}
    
	public static final Point findXandY( int exp1, int exp2, int h, int a, int b, int c, int d) {
		final Point p = pointBuffer[ pointBufferIndex ];
		pointBufferIndex = ( byte ) ( ( pointBufferIndex + 1 ) & BUFFER_SIZE_MOD );
        
        p.y = NanoMath.divFixed((exp2 - NanoMath.divFixed(NanoMath.mulFixed(c,exp1),a)),(d-NanoMath.divFixed(NanoMath.mulFixed(c,b),a)));
        p.x = NanoMath.divFixed((exp1 - NanoMath.mulFixed(p.y,b)),a);
        
        return p;
    }

	public static final Point3f pixelsToIsoGlobal( int realX, int realY, int h) {
		final Point3f p = new Point3f();

        p.y = h;

        p.x = findXandY(realX, realY, h, FP_ISO_XREAL_X_OFFSET, FP_ISO_ZREAL_X_OFFSET, FP_ISO_XREAL_Y_OFFSET, FP_ISO_ZREAL_Y_OFFSET).x;

        p.z = findXandY(realX, realY, h, FP_ISO_XREAL_X_OFFSET, FP_ISO_ZREAL_X_OFFSET, FP_ISO_XREAL_Y_OFFSET, FP_ISO_ZREAL_Y_OFFSET).y;

		return p;
	}


	public static final Point3f isoPixelsToGlobalY0( Point p ) {
		return isoPixelsToGlobalY0( p.x, p.y );
	}


	/**
	 * Converte uma posição da tela para uma posição do mundo real. Assume que y no mundo real é 0.
	 */
	public static final Point3f isoPixelsToGlobalY0( int x, int y ) {
		// Equação 1:
		// xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * zGlobal )
		// Equação 2:
		// yPixel = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) + ( ISO_XREAL_Y_OFFSET * xGlobal ) + ( ISO_ZREAL_Y_OFFSET * zGlobal )

		// Simplificando para yGlobal sempre = 0:
		// yPixel = ( ISO_XREAL_Y_OFFSET * xGlobal ) + ( ISO_ZREAL_Y_OFFSET * zGlobal )
		// Isola zGlobal na Equação 2:
		// zGlobal = ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) / ISO_ZREAL_Y_OFFSET
		// Substitui na Equação 1:
		// xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * ( ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) / ISO_ZREAL_Y_OFFSET ) )
		// Isola xGlobal
		// 1) xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * (( yPixel / ISO_ZREAL_Y_OFFSET ) - ( ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET ))
		// 2) xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET ) - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET )
		// 3) ( ISO_XREAL_X_OFFSET * xGlobal ) = xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET ) + ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET )
		// 4) ( ISO_XREAL_X_OFFSET * xGlobal ) - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET ) = xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET )
		// 5) xGlobal * ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET )) = xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET )
		// 6) xGlobal = ( xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET )) / ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET ))
		// 7) xGlobal = ( xPixel - ( ISO_ZREAL_X_OFFSET / ISO_ZREAL_Y_OFFSET * yPixel )) / ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET ))

		// Calcula xGlobal
		x = NanoMath.toFixed( x );
		y = NanoMath.toFixed( y );

		final int divider = FP_ISO_XREAL_X_OFFSET - ( NanoMath.divFixed( NanoMath.mulFixed( FP_ISO_ZREAL_X_OFFSET, FP_ISO_XREAL_Y_OFFSET ), FP_ISO_ZREAL_Y_OFFSET ) );
		final int fp_x = NanoMath.divFixed( ( x - ( NanoMath.mulFixed( NanoMath.divFixed( FP_ISO_ZREAL_X_OFFSET, FP_ISO_ZREAL_Y_OFFSET ), y ) ) ), divider );
		final int fp_z = NanoMath.divFixed( ( y - NanoMath.mulFixed( FP_ISO_XREAL_Y_OFFSET, fp_x ) ), FP_ISO_ZREAL_Y_OFFSET );

//		float xGlobal = ( x - ( ISO_ZREAL_X_OFFSET / ISO_ZREAL_Y_OFFSET * y )) / ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET ));
//		float xGlobal = ( float ) ( ( x - ( -8.0 / 3.62 * y ) ) / ( 8.743015 - ( -8.0 * 4.3715 / 3.62 ) ) );
//		float zGlobal = ( y - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) / ISO_ZREAL_Y_OFFSET; 
//		float zGlobal = ( float ) ( ( y - ( 4.3715 * xGlobal ) ) / 3.62 );

		return new Point3f( fp_x, 0, fp_z );
	}


	private  final class BorderedTextScreen extends UpdatableGroup implements KeyListener, PointerListener {

		private final ScrollRichLabel textScreen;

		private final int nextScreen;

		public BorderedTextScreen( String text, int nextScreen ) throws Exception {
			super( 2 );

			this.nextScreen = nextScreen;

			//#if SCREEN_SIZE == "SMALL"
//# 				if ( ScreenManager.SCREEN_HEIGHT < HEIGHT_MIN )
//# 					setSize( ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
//# 				else
//# 					setSize( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#else
				setSize( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#endif
			defineReferencePixel( Drawable.ANCHOR_HCENTER | Drawable.ANCHOR_VCENTER );
			setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

			textScreen = new ScrollRichLabel( new RichLabel( getFont( FONT_TEXT ), text, getWidth() ) );
			textScreen.setSize( getWidth() - ( BORDER_THICKNESS * 3 ), getHeight() - ( BORDER_THICKNESS << 1 ) );
			textScreen.setPosition( BORDER_THICKNESS << 1, BORDER_THICKNESS );
			textScreen.setScrollFull( getScrollFull() );
			textScreen.setScrollPage( getScrollPage() );

			final Border border = new Border();
			border.setSize( getSize() );
			insertDrawable( border );
			insertDrawable( textScreen );
		}


		public final void keyPressed( int key ) {
			switch ( key ) {
				case ScreenManager.FIRE:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					GameMIDlet.setScreen( nextScreen );
                    break;

				default:
					textScreen.keyPressed( key );
			}
		}


		public final void keyReleased( int key ) {
			textScreen.keyReleased( key );
		}


		public final void onPointerDragged( int x, int y ) {
			textScreen.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			textScreen.onPointerPressed( x, y );

		}


		public final void onPointerReleased( int x, int y ) {
			textScreen.onPointerReleased( x, y );
		}

	}
	
	
	public final void write( DataOutputStream output ) throws Exception {
		output.writeByte( getLanguage() );
	}


	public final void read( DataInputStream input ) throws Exception {
		setLanguage( input.readByte() );
	}
	
	
	protected final void changeLanguage( byte language ) {
		String file;
		switch ( language ) {
			case LANGUAGE_PORTUGUESE:
				file = "pt";
			break;
			
			case LANGUAGE_SPANISH:
				file = "es";
			break;
			
			case LANGUAGE_ENGLISH:
			default:
				file = "en";
			break;
		}

		try {
			loadTexts( TEXT_TOTAL, PATH_IMAGES + file + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
				
			exit();
		}
	}	


	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;


	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualização do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private final byte previousScreen;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener ) throws Exception {
			super( AppMIDlet.GetFont( FONT_MENU ), AppMIDlet.getText( TEXT_LOADING ) );

			previousScreen = currentScreen;

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );

			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo são carregados aqui para evitar sobrecarga do método loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// só inicia a thread quando a tela atual for a ativa, para evitar possíveis atrasos em transiçÃµes e
					// garantir que novos recursos só serão carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
								}
							} // fim do método run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execução
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do método update( int )


		public final void paint( Graphics g ) {
			super.paint( g );

			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	// </editor-fold>
}
