/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Pattern;

import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;

import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author icustodio
 */
public class BezierTest extends Pattern implements Constants, Updatable, KeyListener {


    private final Point origem = new Point(20, 20);
    private final Point control1 = new Point(40, 140);
    private final Point control2 = new Point(0, 20);

    private final Point destino = new Point(80, 100);
    private final BezierCurve curve;

    private short accTime;
    private boolean invert = false;
    private Point bezier = new Point();


    private static final int TIME = 500;

    public BezierTest() throws Exception {
        super( 0xffff00 );

        curve = new BezierCurve(origem, control1, control2, destino);

        setSize( 20, 20 );
    }


    public final void update(int delta) {
        if (invert) {
            delta = -delta;
        }

        accTime += delta;
        if ( accTime > TIME ) {
            invert = true;
            delta = -delta;
        } else if ( accTime < 0 ) {
            invert = false;
            delta = -delta;
        }

        curve.getPointAtFixed(bezier, NanoMath.divFixed( accTime, TIME ) );
    }


    public void draw( Graphics g ) {
        translate.addEquals( bezier );
        super.draw( g );
        translate.subEquals( bezier );

    }


    public void keyPressed(int key) {
        if (key == ScreenManager.KEY_NUM8) {
            move(0, 10);
        }
    }


    public void keyReleased(int key) {

    }
}
