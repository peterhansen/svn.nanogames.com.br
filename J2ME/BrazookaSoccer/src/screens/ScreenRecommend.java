/**
 * RegisterScreen2.java
 *
 * Created on 21/Dez/2008, 15:51:00
 *
 */

//#ifndef NO_RECOMMEND

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.SmsSender;
import br.com.nanogames.components.util.SmsSenderListener;
import core.Border;
import core.Constants;
import core.ScrollBar2;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 *
 * @author Peter
 */
public final class ScreenRecommend extends Container implements Constants, EventListener, SmsSenderListener {

	private static final byte KEYPAD_KEYS_PER_ROW = 6;

	/***/
	private static final byte ENTRY_TEXT			= 0;
	private static final byte ENTRY_BOXES			= ENTRY_TEXT + 1;
	private static final byte ENTRY_KEYPAD			= ENTRY_BOXES + 1;

	/***/
	private static final byte ENTRY_BUTTON_OK			= ENTRY_KEYPAD + 1;
	/***/
	private static final byte ENTRY_BUTTON_BACK			= ENTRY_BUTTON_OK + 1;

	/***/
	private static final byte ENTRIES_TOTAL = ( ENTRY_BUTTON_BACK + 1 ) << 1;

	/***/
	private final TextBox textBox;

	/***/
	private final Button buttonOK;

	/***/
	private final Button buttonBack;


	//#if SCREEN_SIZE == "BIG"
		private final DrawableComponent keypad;

		private final Sprite keypadPressed;
	//#endif


	public ScreenRecommend( int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );

		FlowLayout flowLayout = new FlowLayout( FlowLayout.AXIS_VERTICAL, ANCHOR_HCENTER );
		flowLayout.gap.set( ENTRIES_SPACING, ENTRIES_SPACING );

		setLayout( flowLayout );

		setScrollableY( true );
		setScrollBarV( new ScrollBar2() );

		final ImageFont font = GameMIDlet.GetFont( FONT_TEXT );

		final FormText text = new FormText( font, ScreenManager.SCREEN_WIDTH, ( ScreenManager.SCREEN_HEIGHT / font.getHeight() ) * 8 / 10 );
		text.setText( GameMIDlet.getText( TEXT_RECOMMEND_TEXT ) );
		text.setScrollableY( true );
		text.setScrollBarV( new ScrollBar2() );
		text.getScrollBarV().setSize( 0, 1 );
		LineBorder lineBorder = new LineBorder();
		lineBorder.setVisible( false );
		lineBorder.setLeft( 2 );
		lineBorder.setRight( 3 );
		lineBorder.setTop( 2 );
		lineBorder.setBottom( 0 );
		text.setBorder( lineBorder );
		text.addEventListener( this );
		insertDrawable( text );

		textBox = new TextBox( font, null, 12, TextBox.INPUT_MODE_NUMBERS, false );
		textBox.setCaret( new DrawableImage( PATH_IMAGES + "caret.png" ) );
		textBox.setId( ENTRY_BOXES );
		textBox.addEventListener( this );
		textBox.setBorder( new Border() );
		insertDrawable( textBox );

		//#if SCREEN_SIZE == "BIG"
			if ( ScreenManager.getInstance().hasPointerEvents() ) {
				final DrawableGroup group = new DrawableGroup( 2 );
				final DrawableImage keypadImage = new DrawableImage( PATH_IMAGES + "keypad.png" );
				group.insertDrawable( keypadImage );
				group.setSize( keypadImage.getSize() );

				keypadPressed = new Sprite( PATH_IMAGES + "keypad" );
				keypadPressed.setVisible( false );
				group.insertDrawable( keypadPressed );

				keypad = new DrawableComponent( group );
				keypad.setId( ENTRY_KEYPAD );
				keypad.setBorder( new Border() );
				keypad.addEventListener( this );
				keypad.setFocusable( false );
				insertDrawable( keypad );
			} else {
				keypad = null;
				keypadPressed = null;
			}
		//#endif

		buttonOK = new Button( font, GameMIDlet.getText( TEXT_OK ) );
		buttonOK.setId( ENTRY_BUTTON_OK );
		buttonOK.setBorder( new Border() );
		buttonOK.setKeyListener( this );
		buttonOK.addEventListener( this );


		buttonBack = new Button( font, GameMIDlet.getText( TEXT_BACK ) );
		buttonBack.setId( ENTRY_BUTTON_BACK );
		buttonBack.addEventListener( this );
		buttonBack.setKeyListener( this );
		buttonBack.setBorder( new Border() );


		flowLayout = new FlowLayout( FlowLayout.AXIS_HORIZONTAL );
		flowLayout.gap.set( ENTRIES_SPACING, ENTRIES_SPACING );

		final Container container = new Container( 2, flowLayout );
		container.insertDrawable( buttonOK );
		container.insertDrawable( buttonBack );
		insertDrawable( container );


		final int SPACER_HEIGHT = font.getHeight();
		final Component spacer = new Component( 0 ) {

			public final Point calcPreferredSize( Point maximumSize ) {
				return new Point( 0, SPACER_HEIGHT );
			}


			public final String getUIID() {
				return "";
			}
		};
		spacer.setKeyListener( this );

		insertDrawable( spacer );
	}


	public final Point calcPreferredSize( Point maximumSize ) {
		return new Point( ScreenManager.SCREEN_WIDTH, Integer.MAX_VALUE );
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		}
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_BUTTON_BACK:
						GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						evt.consume();
					break;

					case ENTRY_BUTTON_OK:
						final String number = textBox.getText();
						if ( number.length() > 7 ) {
							final SmsSender sender = new SmsSender();
							sender.send( textBox.getText(), GameMIDlet.getText( TEXT_RECOMMEND_SMS ) + GameMIDlet.getRecommendURL(), this );
							GameMIDlet.setScreen( SCREEN_RECOMMEND_SENT );
						} else {
							textBox.requestFocus();
						}
						evt.consume();
					break;
				}
			break;

			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
				evt.consume();
			break;

			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				switch ( sourceId ) {
					case ENTRY_BOXES:
						// se estiver no modo editável, altera o label da soft key direita ao dar foco a um dos textBox
						if ( buttonBack.getId() == ENTRY_BUTTON_BACK && GameMIDlet.getSoftKeyRightTextIndex() != TEXT_CLEAR )
							GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_CLEAR );
					break;

					default:
						if ( GameMIDlet.getSoftKeyRightTextIndex() != TEXT_BACK )
							GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						evt.consume();
					break;
				} // fim switch ( key )
			break;

			//#if SCREEN_SIZE == "BIG"
				case Event.EVT_POINTER_PRESSED:
				case Event.EVT_POINTER_DRAGGED:
					if ( sourceId == ENTRY_KEYPAD && ( evt.eventType == Event.EVT_POINTER_PRESSED || keypad.contains( ( Point ) evt.data ) ) ) {
						final int number = getNumber( evt );
						keypadPressed.setFrame( number );
						keypadPressed.setVisible( true );
					} else {
						keypadPressed.setVisible( false );
					}
				break;

				case Event.EVT_POINTER_RELEASED:
					if ( sourceId == ENTRY_KEYPAD ) {
						final int number = getNumber( evt );
						keypadPressed.setVisible( false );

						textBox.requestFocus();
						if ( number < 10 ) {
							textBox.keyPressed( ScreenManager.KEY_NUM0 + ( ( number + 1 ) % 10 ) );
							textBox.keyReleased( ScreenManager.KEY_NUM0 + ( ( number + 1 ) % 10 ) );
						} else {
							textBox.keyPressed( ScreenManager.KEY_CLEAR );
							textBox.keyReleased( ScreenManager.KEY_CLEAR );
						}
					}
					evt.consume();
				break;
			//#endif
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	//#if SCREEN_SIZE == "BIG"
	private final byte getNumber( Event evt ) {
		final Point clickPos = ( Point ) evt.data;
		final Point temp = new Point();
		evt.source.getAbsolutePos( temp );
		clickPos.subEquals( temp );
		clickPos.y += getScrollY();

		return ( byte ) ( ( clickPos.x * KEYPAD_KEYS_PER_ROW / evt.source.getWidth() ) + ( ( clickPos.y << 1 ) / evt.source.getHeight() * KEYPAD_KEYS_PER_ROW ) );
	}
	//#endif


	public final void onSMSSent( int id, boolean smsSent ) {
		//#if DEBUG == "true"
		System.out.println( "onSMSSent( " + id + ", " + smsSent );
		//#endif
	}

		public void onSMSLog( int id, String log ) {
	//#if DEBUG == "true"
			System.out.println( "onSMSLog( " + id + ", " + log );

	//#endif
		}

}


//#endif