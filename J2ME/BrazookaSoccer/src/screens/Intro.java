/**
 * SplashGame.java
 * ©2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Graphics;

//#if SCREEN_SIZE == "BIG"
	import br.com.nanogames.components.userInterface.PointerListener;
	import br.com.nanogames.components.userInterface.ScreenListener;
	import br.com.nanogames.components.util.ImageLoader;
	import br.com.nanogames.components.util.MUV;
	import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
	import br.com.nanogames.components.util.Point;
	import javax.microedition.lcdui.Graphics;
//#endif

/**
 *
 * @author Peter
 */
public final class Intro extends DrawableGroup implements Constants, Updatable, ScreenListener
		//#if DEBUG == "true"
			, KeyListener
			
			//#if SCREEN_SIZE == "BIG"
			, PointerListener
			//#endif
		//#endif
{

	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 21;

	private static final byte STATE_HERO				= 0;
	private static final byte STATE_SEMI_FLASH_1		= 1;
	private static final byte STATE_PRE_PIC_1			= 2;
	private static final byte STATE_PIC_1				= 3;
	private static final byte STATE_SEMI_FLASH_2		= 4;
	private static final byte STATE_PRE_PIC_2			= 5;
	private static final byte STATE_PIC_2				= 6;
	private static final byte STATE_SEMI_FLASH_3		= 7;
	private static final byte STATE_PRE_PIC_3			= 8;
	private static final byte STATE_PIC_3				= 9;
	private static final byte STATE_SEMI_FLASH_4		= 10;
	private static final byte STATE_PRE_PIC_4			= 11;
	private static final byte STATE_PIC_4				= 12;
	private static final byte STATE_SEMI_FLASH_5		= 13;
	private static final byte STATE_PRE_SPLASH			= 14;
	private static final byte STATE_FADE				= 15;
	private static final byte STATE_HEAD_APPEARS		= 16;
	private static final byte STATE_LOGO_APPEARS		= 17;
	private static final byte STATE_SHOW_LOGO			= 18;

	private byte state;

	private static final int COLOR_SKY_TOP = 0x15e6ea;
	private static final int COLOR_SKY_BOTTOM = 0xe7b117;

	private static final short TIME_HERO = 3200;
	private static final short TIME_PIC = 800;
	private static final short TIME_FLASH = 50;
	private static final short TIME_FADE = 600;
	private static final short TIME_SHOW_LOGO = 1600;
	
	private short accTime;

	private static final byte PICTURES_TOTAL = 4;

	private final DrawableImage[] pictures = new DrawableImage[ PICTURES_TOTAL ];

	private final Pattern sky;

	private final Pattern skyTop;

	private final Pattern skyBottom;

	private final Pattern black;

	private final DrawableImage cloud1;
	
	private final DrawableImage cloud2;

	private final BasicAnimatedPattern grass;

	private final Pattern pattern;

	private final DrawableImage player;

	private final DrawableImage ball;

	private final DrawableImage logo;

	private final Label labelTop;

	private final Label labelBottom;

	private final Point offset = new Point();

	private final MUV speed = new MUV();

	private int diff;

	
	public Intro() throws Exception {
		super( TOTAL_ITEMS );

		sky = new Pattern( new DrawableImage( PATH_SPLASH + "sky.png" ) );
		insertDrawable( sky );

		skyTop = new Pattern( COLOR_SKY_TOP );
		insertDrawable( skyTop );

		cloud1 = new DrawableImage( PATH_SPLASH + "c_0.png" );
		insertDrawable( cloud1 );

		cloud2 = new DrawableImage( PATH_SPLASH + "c_1.png" );
		insertDrawable( cloud2 );

		skyBottom = new Pattern( COLOR_SKY_BOTTOM );
		insertDrawable( skyBottom );

		player = new DrawableImage( PATH_SPLASH + "p.png" );
		insertDrawable( player );

		ball = new DrawableImage( PATH_SPLASH + "b.png" );
		insertDrawable( ball );

		grass = new BasicAnimatedPattern( new DrawableImage( PATH_SPLASH + "g.png" ), 0, 0 );
		grass.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		insertDrawable( grass );

		black = new Pattern( 0x000000 );
		insertDrawable( black );

		for ( byte i = 0; i < PICTURES_TOTAL; ++i ) {
			pictures[ i ] = new DrawableImage( PATH_SPLASH + "p_" + i + ".png" );
			pictures[ i ].setVisible( false );
			insertDrawable( pictures[ i ] );
		}

		logo = new DrawableImage( PATH_SPLASH + "logo.png" );
		logo.setVisible( false );
		insertDrawable( logo );

		final ImageFont font = ImageFont.createMultiSpacedFont( PATH_SPLASH + "font_splash" );
		font.setCharExtraOffset( 2 );

		labelTop = new Label( font, "BRAZOOKA" );
		labelTop.setVisible( false );
		insertDrawable( labelTop );

		labelBottom = new Label( font, "SOCCER_" );
		labelBottom.setVisible( false );
		insertDrawable( labelBottom );
		
		final PaletteChanger p = new PaletteChanger( PATH_SPLASH + "transparency.png" );
		pattern = new Pattern( new DrawableImage( p.createImage( 0x000000, 0xffffff ) ) );
		insertDrawable( pattern );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setState( STATE_HERO );
	}


	public final void update( int delta ) {
		accTime += delta;

		switch ( state ) {
			case STATE_HERO:
				if ( accTime >= TIME_HERO )
					setState( state + 1 );
			break;

			case STATE_PIC_1:
			case STATE_PIC_2:
			case STATE_PIC_3:
			case STATE_PIC_4:
				if ( accTime >= TIME_PIC ) {
					setState( state + 1 );
				}
			break;

			case STATE_PRE_PIC_1:
			case STATE_PRE_PIC_2:
			case STATE_PRE_PIC_3:
			case STATE_PRE_PIC_4:
				if ( accTime >= TIME_FLASH )
					setState( state + 1 );
			break;

			case STATE_SEMI_FLASH_1:
			case STATE_SEMI_FLASH_2:
			case STATE_SEMI_FLASH_3:
			case STATE_SEMI_FLASH_4:
			case STATE_SEMI_FLASH_5:
			case STATE_PRE_SPLASH:
				setState( state + 1 );
			break;

			case STATE_FADE:
				if ( accTime >= TIME_FADE )
					setState( state + 1 );
			break;

			case STATE_HEAD_APPEARS:
				setState( state + 1 );
			break;

			case STATE_LOGO_APPEARS:
				final int percent = accTime * 100 / TIME_SHOW_LOGO;
				
				logo.setPosition( offset.x + NanoMath.lerpInt( -logo.getWidth(), SPLASH_LOGO_X, percent ), offset.y + SPLASH_LOGO_Y );
				labelTop.setPosition( offset.x + SPLASH_TITLE_X, offset.y + NanoMath.lerpInt( -labelTop.getHeight(), SPLASH_TITLE_TOP_Y, percent ) );
				labelBottom.setPosition( offset.x + NanoMath.lerpInt( getWidth() + labelTop.getWidth(), SPLASH_TITLE_X, percent ), offset.y + SPLASH_TITLE_BOTTOM_Y );

//				pictures[ 2 ].setPosition( );

				if ( accTime >= TIME_SHOW_LOGO )
					setState( state + 1 );
			break;

			case STATE_SHOW_LOGO:
				if ( accTime >= TIME_SHOW_LOGO ) {
					GameMIDlet.setScreen( SCREEN_MAIN_MENU );
				}
			break;
		}

		diff += speed.updateInt( delta );

		int temp = NanoMath.toInt( NanoMath.divFixed( NanoMath.toFixed( diff ), NanoMath.toFixed( 6 ) ) );

		cloud1.setPosition( temp + offset.x + SPLASH_CLOUD_1_X, cloud1.getPosY() );
		cloud2.setPosition( temp + offset.x + SPLASH_CLOUD_2_X, cloud1.getPosY() );

		temp = NanoMath.toInt( NanoMath.divFixed( NanoMath.toFixed( diff ), NanoMath.toFixed( -2 ) ) );
		player.setPosition( temp + ( ( getWidth() - player.getWidth() ) >> 1 ), player.getPosY() );
		grass.getOffset().x = temp;

		temp = NanoMath.toInt( NanoMath.divFixed( NanoMath.toFixed( diff ), NanoMath.toFixed( 2 ) ) );
		ball.setPosition( temp + ( ( ( getWidth() >> 1 ) - ball.getWidth() ) >> 1 ), ball.getPosY() );
	}


	//#if DEBUG == "true"
		public final void keyPressed( int key ) {
			setState( STATE_SHOW_LOGO );
			accTime = 5000;
	//		if ( state == STATE_PRE_SPLASH ) {
	//			//#if DEMO == "true"
	////# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
	//			//#else
	//				GameMIDlet.setScreen( SCREEN_SPLASH_GAME );
	//			//#endif
	//		}
		}


		public final void keyReleased( int key ) {
		}



		//#if SCREEN_SIZE == "BIG"
			public final void onPointerDragged( int x, int y ) {
			}


			public final void onPointerPressed( int x, int y ) {
				keyPressed( 0 );
			}


			public final void onPointerReleased( int x, int y ) {
			}

		//#endif

	//#endif

	
	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		if ( width != size.x || height != size.y ) {
			super.setSize( width, height );

			speed.setSpeed( width / 20 );

			offset.set( ( width - SPLASH_DEFAULT_WIDTH ) >> 1, ( height - SPLASH_DEFAULT_HEIGHT ) >> 1 );

			sky.setSize( width, sky.getFill().getHeight() );
			sky.setPosition( 0, offset.y + SPLASH_SKY_Y );

			skyTop.setSize( sky.getWidth(), sky.getPosY() );
			
			cloud1.setPosition( offset.x + SPLASH_CLOUD_1_X, offset.y + SPLASH_CLOUD_1_Y );
			cloud2.setPosition( offset.x + SPLASH_CLOUD_2_X, offset.y + SPLASH_CLOUD_2_Y );

			skyBottom.setSize( width, SPLASH_SKY_BOTTOM_HEIGHT );
			skyBottom.setPosition( 0, offset.y + SPLASH_SKY_BOTTOM_Y );

			player.setPosition( ( width - player.getWidth() ) >> 1, offset.y + SPLASH_PLAYER_Y );

			ball.setPosition( ( ( width >> 1 ) - ball.getWidth() ) >> 1, offset.y + SPLASH_BALL_Y );
			grass.setSize( width, grass.getFill().getHeight() );
			grass.setPosition( 0, offset.y + SPLASH_GRASS_Y );

			pattern.setSize( width, height );

			black.setPosition( 0, grass.getPosY() + grass.getHeight() );
			black.setSize( width, height - black.getPosY() );

			for ( byte i = 0; i < PICTURES_TOTAL; ++i ) {
				switch ( i ) {
					case 0:
						pictures[ i ].setPosition( Math.max( 0, ( ( width >> 1 ) - pictures[ i ].getWidth() ) >> 1 ), 0 );
					break;

					case 1:
						pictures[ i ].setPosition( Math.min( width - pictures[ i ].getWidth(), ( ( width * 3 / 2 ) - pictures[ i ].getWidth() ) >> 1 ), 0 );
					break;

					case 2:
						pictures[ i ].setPosition( Math.max( 0, ( ( width >> 1 ) - pictures[ i ].getWidth() ) >> 1 ), height - pictures[ i ].getHeight() );
					break;

					case 3:
						pictures[ i ].setPosition( Math.min( width - pictures[ i ].getWidth(), ( ( width * 3 / 2 ) - pictures[ i ].getWidth() ) >> 1 ), height - pictures[ i ].getHeight() );
					break;
				}
			}
		}
	}


	protected final void paint( Graphics g ) {
		switch ( state ) {
			case STATE_PRE_PIC_1:
			case STATE_PRE_PIC_2:
			case STATE_PRE_PIC_3:
			case STATE_PRE_PIC_4:
			case STATE_PRE_SPLASH:
				g.setColor( 0xffffff );
				g.fillRect( 0, 0, getWidth(), getHeight() );
			break;

			case STATE_FADE:
				final int bkgInitColor = 0xffffff;
				final int bkgFinalColor = COLOR_BACKGROUND;
				
				int percentage = ( accTime * 100 ) / TIME_FADE;
				if( percentage > 100 )
					percentage = 100;

				final int red = NanoMath.lerpInt( ( bkgInitColor & 0x00FF0000 ) >> 16, ( bkgFinalColor & 0x00FF0000 ) >> 16, percentage );
				final int green = NanoMath.lerpInt( ( bkgInitColor & 0x0000FF00 ) >>  8, ( bkgFinalColor & 0x0000FF00 ) >>  8, percentage );
				final int blue = NanoMath.lerpInt( bkgInitColor & 0x000000FF, bkgFinalColor & 0x000000FF, percentage );

				g.setColor( 0xFF000000 | ( red << 16 ) | ( green << 8 ) | blue );
				g.fillRect( 0, 0, getWidth(), getHeight() );
			break;

			default:
				super.paint( g );
		}
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;

		pattern.setVisible( false );
		switch ( state ) {
			case STATE_PIC_1:
			case STATE_PIC_2:
			case STATE_PIC_3:
			case STATE_PIC_4:
				pictures[ ( state - STATE_PIC_1 ) / 3 ].setVisible( true );
			break;

			case STATE_SEMI_FLASH_1:
			case STATE_SEMI_FLASH_2:
			case STATE_SEMI_FLASH_3:
			case STATE_SEMI_FLASH_4:
			case STATE_SEMI_FLASH_5:
				pattern.setVisible( true );
			break;

			case STATE_FADE:
				grass.setVisible( false );
				ball.setVisible( false );
				player.setVisible( false );
				black.setVisible( false );
				cloud1.setVisible( false );
				cloud2.setVisible( false );
				sky.setVisible( false );
				skyBottom.setVisible( false );
				skyTop.setVisible( false );

				for ( byte i = 0; i < PICTURES_TOTAL; ++i )
					pictures[ i ].setPosition( -1000, -1000 );
			break;

			case STATE_LOGO_APPEARS:
				GameMIDlet.setBackground( BACKGROUND_TYPE_SPECIFIC );
				
				logo.setVisible( true );
				labelTop.setVisible( true );
				labelBottom.setVisible( true );
				update( 0 );
			break;
		}
	}

    public void hideNotify(boolean deviceEvent) {
        
    }

    public void showNotify(boolean deviceEvent) {
        
    }



}
