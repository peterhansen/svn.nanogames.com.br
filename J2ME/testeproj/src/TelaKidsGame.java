import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
/*
 * TelaKidsGame.java
 *
 * Created on May 18, 2007, 5:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class TelaKidsGame extends UpdatableGroup implements KeyListener{
    private static final int NUMERO_DE_ELEMENTOS = 3;
    final AnimatedBalloon animBalao = new AnimatedBalloon();
    final Pattern sky;
	final Pattern ground;
	static int vel_ground;
	static int lMod_ground;
	static int ds;
	static int dx;
    static int offset;
	static int offsetTarget;
	private boolean chaseing = false;
	
	static Sprite outDoor;
    
    
    /**
     * Creates a new instance of TelaKidsGame
     */
    public TelaKidsGame() throws Exception {
        super(NUMERO_DE_ELEMENTOS);
        
        sky = new Pattern( new DrawableImage( "/azulzim.png") );
        sky.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		
		ground = new Pattern ( new DrawableImage("/ground.png"));
		ground.setSize( ScreenManager.SCREEN_WIDTH * 100, ScreenManager.SCREEN_HEIGHT );
		ground.setPosition( 0, ScreenManager.SCREEN_HEIGHT - ground.getFill().getSize().y);
		
        criaMomento();
    }
    
    
    private final void criaMomento() {
        insertDrawable(sky);
		insertDrawable(ground);
        insertDrawable(animBalao);
    }

    
    public void keyPressed(int key) {
        switch ( ScreenManager.getKeyCodeGameAction( key ) ) {
            case ScreenManager.UP:
                key = ScreenManager.KEY_NUM2;
                break;
            case ScreenManager.DOWN:
                key = ScreenManager.KEY_NUM8;
                break;
            case ScreenManager.LEFT:
                key = ScreenManager.KEY_NUM4;
                break;
            case ScreenManager.RIGHT:
                key = ScreenManager.KEY_NUM6;
                break;

        }
        switch (key){
        case ScreenManager.KEY_NUM2:
            //animBalao.somaVely(2);
            animBalao.setPressedVelo(AnimatedBalloon.UP);
            break;
        case ScreenManager.KEY_NUM8:
            //animBalao.somaVely(-2);
            animBalao.setPressedVelo(AnimatedBalloon.DOWN);
            break;
        case ScreenManager.KEY_NUM6:
            //animBalao.somaVelx(2);
            animBalao.setPressedVelo(AnimatedBalloon.RIGHT);
            break;
        case ScreenManager.KEY_NUM4:
            //animBalao.somaVelx(-2);
            animBalao.setPressedVelo(AnimatedBalloon.LEFT);
            break;
        }
    }

	
    public void keyReleased(int key) {
        if (key == 0 ){
            animBalao.resetVelo();
            return;
        }
        
        switch ( ScreenManager.getKeyCodeGameAction( key ) ) {
            case ScreenManager.UP:
                key = ScreenManager.KEY_NUM8;
                break;
            case ScreenManager.DOWN:
                key = ScreenManager.KEY_NUM2;
                break;
            case ScreenManager.LEFT:
                key = ScreenManager.KEY_NUM4;
                break;
            case ScreenManager.RIGHT:
                key = ScreenManager.KEY_NUM6;
                break;
       }
        
        switch (key){
        case ScreenManager.KEY_NUM2:
            animBalao.setReleasedVelo(AnimatedBalloon.UP);
            break;
        case ScreenManager.KEY_NUM8:
            animBalao.setReleasedVelo(AnimatedBalloon.DOWN);
            break;
        case ScreenManager.KEY_NUM6:
            animBalao.setReleasedVelo(AnimatedBalloon.RIGHT);
            break;
        case ScreenManager.KEY_NUM4:
            animBalao.setReleasedVelo(AnimatedBalloon.LEFT);
            break;
        }
    }

	
	public final void update( int delta ) {
		super.update( delta );
		int vel;
		// Atualizando o cenario
		ground.setPosition(-offset, ground.getPosition().y);
		
		if(offsetTarget <= offset){
			chaseing = false;
			offsetTarget = offset;
		}
		
		//if(offsetTarget > offset + animBalao.getSize().x  || chaseing){
			chaseing = true;
			ds = lMod_ground + ( vel_ground * delta );
			dx = ds / 1000;
			lMod_ground = ds % 1000;
			
			offset +=dx;
			
			vel_ground = (offsetTarget-offset)*(offsetTarget-offset)/30;
			
//			if (vel_ground - animBalao.velocidade.x > 2 ){
//				vel_ground = animBalao.velocidade.x;
//			}
			
		//}
			
		
	}
    
}
