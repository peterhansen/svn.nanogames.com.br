/*
 * MeuMidlet.java
 *
 * Created on May 7, 2007, 6:49 PM
 */

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

/**
 *
 * @author  malvezzi
 * @version
 */
public class MeuMidlet extends AppMIDlet implements MenuListener {
    
    private static MeuMidlet instancia;
    public static final byte SCREEN_MAIN_MENU   = 0;
    public static final byte SCREEN_PLAY_MENU   = 1;
    public static final byte SCREEN_GAMEPLAY    = 2;
    public static final byte SCREEN_MOMENTO    = 3;
    
    
    public static ScreenManager meuScreenManager;
    
   
    public void startApp() throws MIDletStateChangeException {
        super.startApp();
        if ( instancia == null ) {
            meuScreenManager = ScreenManager.createInstance( false, 200 );
            meuScreenManager.start();
            // inicializa dados etc
            instancia = this;
            UpdatableGroup UpGroup = null;
            
            setScreen( SCREEN_MAIN_MENU );
            try {
			   BackGroundSky bkgd = new BackGroundSky();
			   bkgd.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
			   meuScreenManager.setBackground(bkgd,true);
            } catch (Exception ex) {
                 exit();
            } finally {
                System.out.println("finally s");
            }
        }
    }
    
    
    public static final Menu createMainMenu()throws Exception{
        
        Menu mainMenu = new Menu( instancia, SCREEN_MAIN_MENU  , 6 );
        
        DrawableImage main_novo = new DrawableImage("/main_novo.png");
        DrawableImage main_help = new DrawableImage("/main_help.png");
        DrawableImage main_cred = new DrawableImage("/main_cred.png");
        DrawableImage main_sair = new DrawableImage("/main_sair.png");

		DrawableImage cursor    = new DrawableImage("/main_cursor.png");
        
        //main_help.setRefPixelPosition(40,60);
        main_novo.setPosition(cursor.getSize().x,0);
        main_help.setPosition(cursor.getSize().x, main_novo.getSize().y);
        main_cred.setPosition(cursor.getSize().x, main_help.getPosition().y + main_help.getSize().y);
        main_sair.setPosition(cursor.getSize().x, main_cred.getPosition().y + main_cred.getSize().y);

        //Main Menu
        mainMenu.setPosition(ScreenManager.SCREEN_HALF_WIDTH/4,ScreenManager.SCREEN_HALF_HEIGHT/4);
        mainMenu.insertDrawable(main_novo);
        mainMenu.insertDrawable(main_help);
        mainMenu.insertDrawable(main_cred);
        mainMenu.insertDrawable(main_sair);
        mainMenu.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
        mainMenu.setCircular(true);
        cursor.defineReferencePixel(cursor.getSize().x, cursor.getSize().y >> 1);
        mainMenu.setCursor(cursor,Menu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT|Graphics.VCENTER);

        return mainMenu;
    }

    
    public static final Menu createPlayMenu()throws Exception{
        
        Menu playMenu = new Menu( instancia, SCREEN_PLAY_MENU, 3 );
        
        DrawableImage item01  = new DrawableImage("/ms01_fr.png");
        DrawableImage item01b = new DrawableImage("/ms01_bk.png");
        DrawableImage item02  = new DrawableImage("/ms02_fr.png");
        DrawableImage item02b = new DrawableImage("/ms02_bk.png");
        DrawableImage cursor  = new DrawableImage("/main_cursor.png");
        
        item02.setRefPixelPosition(0,60);
        item01b.setPosition(0,120);
        item02b.setPosition(0,178);

        playMenu.insertDrawable(item01b);
        playMenu.insertDrawable(item02b);
        playMenu.setSize(100,300);
        playMenu.setCursor(cursor,Menu.CURSOR_DRAW_AFTER_MENU,Graphics.LEFT);
        
        return playMenu;
    }


    public void onChoose(int id, int index) {
        switch ( id ) {
            case SCREEN_MAIN_MENU:
                switch ( index ) {
                    case 0:
                        setScreen( SCREEN_PLAY_MENU );
                    break;
                    case 1:
                        setScreen (SCREEN_GAMEPLAY);
                    break;
                    case 2:
                        setScreen (SCREEN_MOMENTO);
                    break;
					case 3:
						MeuMidlet.exit();
                }
            break;
            
            case SCREEN_PLAY_MENU:
                switch ( index ) {
                    case 0:
                        setScreen( SCREEN_MAIN_MENU );
                    break;
                    case 1:
                    break;
                    case 2:
                    break;
                }                
            break;
        }
    }
    
    
    public static final void setScreen( byte index ) {
        Drawable nextScreen = null;
        try {
            switch ( index ) {
                case SCREEN_MAIN_MENU:
                    nextScreen = createMainMenu();
                    meuScreenManager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
                break;
                case SCREEN_PLAY_MENU:
                    nextScreen = createPlayMenu();
                break;
                case SCREEN_GAMEPLAY:
                    nextScreen = createGamePlay();
                break;
                case SCREEN_MOMENTO:
                    nextScreen = createKidsGame();
                break;
            }
        } catch ( Exception e ) {
            e.printStackTrace();
            System.out.println( "deu merda!" );
        }
        
        if ( nextScreen != null )
            meuScreenManager.setCurrentScreen( nextScreen, ScreenManager.TRANSITION_TYPE_IMMEDIATE );
    }

    public static Drawable createGamePlay() throws Exception{
        TelaDeSplash gamePlay = new TelaDeSplash();
        gamePlay.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
        Label label = new Label(ImageFont.createMultiSpacedFont("/font.png","/font.dat"),"Voltar");
        meuScreenManager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, label );

        return gamePlay;
    }
    
    public static Drawable createKidsGame() throws Exception{
        TelaKidsGame kidsgame = new TelaKidsGame();
        kidsgame.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
        return kidsgame;
    }
    public void itemChanged(int id, int index) {
        
    }
}