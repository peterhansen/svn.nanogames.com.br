import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.FrameSet;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import java.util.Random;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;
/*
 * TelaDeSplash.java
 *
 * Created on May 11, 2007, 5:49 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class TelaDeSplash extends UpdatableGroup implements KeyListener,RbConfig {

	private final DrawableImage logoRB			= new DrawableImage("/logorb.png");
	private final DrawableImage logoNN			= new DrawableImage("/logonn.png");
	private final Random rand					= new Random( teste += 34217 );
	static long teste = System.currentTimeMillis();
  	
	private final Balao[] balao = new Balao[ 36 ];

	private static int acumulaTempo;
	
	private final int ATO1 = 0;
	private final int ATO2 = ATO1 + 1;
	private final int ATO3 = ATO2 + 1;
	private final int ATO4 = ATO3 + 1;
	private final int ATO5 = ATO4 + 1;
	private final int ATO6 = ATO5 + 1;
	private final int ATO7 = ATO6 + 1;
	private final int ATO8 = ATO7 + 1;
	private final int ATO9 = ATO8 + 1;
	
	private static int ato;

	
    /**
	 * Creates a new instance of TelaDeSplash
	 */
    public TelaDeSplash(int slot) throws Exception {
        super(slot);
        criaAnimacao();
    }
    
    
    public TelaDeSplash()throws Exception {
        this(1600);
    }
    
    
    private final void criaAnimacao() throws Exception{
        final int quantidade = 601;
        FrameSet frame = new FrameSet(1);
        frame.addSequence( new Image[] { Image.createImage( "/balao_splash_on.png" ), 
                                         Image.createImage( "/balao_splash_off.png" ) }, 500 );
        
//        FrameSet frame1 = new FrameSet(1);
//        frame1.addSequence( new Image[] { Image.createImage( "/amarelo.png" ), 
//                                          Image.createImage( "/main_cursor.png" ) }, 487 );
		
		DrawableImage splashRB = new DrawableImage( "/logorb.png" );
		insertDrawable(logoRB);
		insertDrawable(logoNN);
		
		logoRB.setVisible(false);
		logoRB.setPosition( ScreenManager.SCREEN_HALF_WIDTH  - (logoRB.getSize().x/2),
							ScreenManager.SCREEN_HALF_HEIGHT - (logoRB.getSize().y/2));
		logoNN.setVisible(false);
		logoNN.setPosition( ScreenManager.SCREEN_HALF_WIDTH  - (logoNN.getSize().x/2),
							ScreenManager.SCREEN_HALF_HEIGHT - (logoNN.getSize().y/2));

		for(int i=0 ; i < balao.length ; ++i ){
            balao[i] = new Balao(frame);
			balao[i].setPosition( -ScreenManager.SCREEN_WIDTH, -ScreenManager.SCREEN_HEIGHT);
			insertDrawable(balao[i]);
		}
		setAto(ATO1);
	}

	
    public void keyPressed(int key) {
        switch(ScreenManager.getSpecialKey( key )){
            case ScreenManager.KEY_SOFT_RIGHT:
            case ScreenManager.KEY_BACK:
                 MeuMidlet.setScreen(MeuMidlet.SCREEN_MAIN_MENU);
            break;    
			default:
				 //MeuMidlet.setScreen(MeuMidlet.SCREEN_MAIN_MENU);
				if(Balao.stopanimation)
					Balao.stopanimation=(false);
				else
					Balao.stopanimation=(true);
        }
    }

	
    public void keyReleased(int key) {
    }
	
	
	protected final void setPosBezierP0toP1(){
		for(int i=0 ; i < balao.length ; ++i ){
			balao[i].origem.set(ScreenManager.SCREEN_HALF_WIDTH,
								ScreenManager.SCREEN_HALF_HEIGHT * 3 );
			balao[i].c1.set( rand.nextInt(ScreenManager.SCREEN_WIDTH), rand.nextInt(ScreenManager.SCREEN_HEIGHT) );
			balao[i].c2.set( rand.nextInt(ScreenManager.SCREEN_WIDTH), rand.nextInt(ScreenManager.SCREEN_HEIGHT) );
			balao[i].destino.set( (logoRB.getPosition().x - logoRB.getSize().x/10) + (i % 12) * balao[i].getSize().x/3,
								  (logoRB.getPosition().y - logoRB.getSize().y/10) + (i / 12) * balao[i].getSize().y/2 );
			balao[i].setBezierValue();
		}
	}
	
	
	protected final void setPosBezierP1toP2(){
		for(int i=0 ; i < balao.length ; ++i ){
			balao[i].origem.set( (logoRB.getPosition().x - logoRB.getSize().x/10) + (i % 12) * balao[i].getSize().x/3,
								 (logoRB.getPosition().y - logoRB.getSize().y/10) + (i / 12) * balao[i].getSize().y/2 );
			//( destino + ( destino - c2 )) = c1`
			balao[i].c1.set( balao[i].destino.x + (balao[i].destino.x - balao[i].c2.x ),
							 balao[i].destino.y + (balao[i].destino.y - balao[i].c2.y ));
			balao[i].c2.set( rand.nextInt(ScreenManager.SCREEN_WIDTH), rand.nextInt(ScreenManager.SCREEN_HEIGHT) );
			balao[i].destino.set( 0, -ScreenManager.SCREEN_HEIGHT*2 );
			balao[i].setBezierValue();
		}
	}
	
	protected final void setAto(int indice){
		switch(indice){
			case ATO1:
				//Balao sobe para p1 sem logo na tela 
				logoRB.setVisible(false);
				logoNN.setVisible(false);
				setPosBezierP0toP1();
				Balao.tempoAnimacao = tempoAto1;
				break;
				
			case ATO2:
				//Balao sobe de p1 para p2 com o primeiro logo na tela
				setPosBezierP1toP2();
				logoNN.setVisible(true);
				Balao.tempoAnimacao = tempoAto2;
				break;
				
			case ATO3:
				//Balao sobe de p0 para p1 com o primeiro logo na tela
				setPosBezierP0toP1();
				Balao.tempoAnimacao = tempoAto3;
				break;
				
			case ATO4:
				//Balao sobe de p1 para p2 com o segundo logo na tela
				setPosBezierP1toP2();
				logoNN.setVisible(false);
				logoRB.setVisible(true);
				Balao.tempoAnimacao = tempoAto4;
				break;
				
			case ATO5:
				//Nao sei... Ainda...
				for(int i=0 ; i < balao.length ; ++i ){
					
					balao[i].origem .set( -ScreenManager.SCREEN_WIDTH, -ScreenManager.SCREEN_HEIGHT );
					balao[i].destino.set( -ScreenManager.SCREEN_WIDTH, -ScreenManager.SCREEN_HEIGHT );
					balao[i].c1.set( -ScreenManager.SCREEN_WIDTH, -ScreenManager.SCREEN_HEIGHT );
					balao[i].c2.set( -ScreenManager.SCREEN_WIDTH, -ScreenManager.SCREEN_HEIGHT );
					balao[i].setBezierValue();
				}
			
				break;

			case ATO6:
			acumulaTempo = 0;				
			MeuMidlet.setScreen(MeuMidlet.SCREEN_MAIN_MENU);

			default:
				return;
		}
		ato = indice;
		acumulaTempo = 0;
		Balao.resetTempo();
	}

	
	public void update(int delta) {
		Balao.updateAnimation( delta );
		super.update(delta);	
		
		acumulaTempo += delta;
		switch(ato){
			case ATO1:
				if (acumulaTempo >= tempoAto1)
					setAto(ATO2);
				break;
			case ATO2:
				
				if (acumulaTempo >= tempoAto2)
					setAto(ATO3);
				break;
			case ATO3:
				
				if (acumulaTempo >= tempoAto3)
					setAto(ATO4);
				break;
			case ATO4:
				
				if (acumulaTempo >= tempoAto4)
					setAto(ATO5);
				break;
			case ATO5:
				
				if (acumulaTempo >= tempoAto5)
					setAto(ATO6);
				break;
		}	
	}
	

	
}
