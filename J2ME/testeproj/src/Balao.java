import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.FrameSet;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.Point;
import java.util.Random;
import javax.microedition.lcdui.Graphics;
/*
 * Balao.java
 *
 * Created on May 11, 2007, 3:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class Balao extends Sprite implements Updatable,RbConfig{
    
    static long teste = System.currentTimeMillis();
    
    private static final String PATH = IMG_01;
    private final BezierCurve bezier = new BezierCurve();
    private final Random rand   = new Random( teste += 34217 );
    public final Point origem  = new Point();
    public final Point c1      = new Point ();
    public final Point c2      = new Point ();
    public final Point destino = new Point ();
    public static int tempoAnimacao;
    private static int tempoAcumulado;
	private static int percent;
	public static boolean stopanimation = false;
    
    
    
    /**
     * Creates a new instance of Balao
     */
    public Balao( FrameSet frame ) throws Exception{
        super(frame);
        resetBezierValue();
        defineReferencePixel( size.x >> 1, 0 );
        accTime = rand.nextInt( frame.getAnimTimePerFrame( 0 ) );
    }

    public void resetBezierValue(){
        
        destino.x = rand.nextInt(ScreenManager.SCREEN_WIDTH);
        destino.y = - (ScreenManager.SCREEN_HALF_HEIGHT * 3);

		c1.x = rand.nextInt( 2 * ScreenManager.SCREEN_WIDTH ) - ScreenManager.SCREEN_HALF_WIDTH;
        c1.y = rand.nextInt( 2 * ScreenManager.SCREEN_HEIGHT );
        c2.x = rand.nextInt( 2 * ScreenManager.SCREEN_WIDTH ) - ScreenManager.SCREEN_HALF_WIDTH;
        c2.y = rand.nextInt( 2 * ScreenManager.SCREEN_HEIGHT );
        
        origem.set( ScreenManager.SCREEN_HALF_WIDTH, ( ScreenManager.SCREEN_HALF_HEIGHT * 3 ) );
        bezier.setValues( origem, c1, c2, destino );
    }
	
	public void setBezierValue(){
		bezier.setValues(origem, c1, c2, destino);
	}
	

	public void setBezierValue(Point origem, Point c1,Point c2, Point destino){
		this.origem.set(origem);
		this.c1.set(c1);
		this.c2.set(c2);
		this.destino.set(destino);
		bezier.setValues(origem, c1, c2, destino);
	}
	

	public void update( int delta ) {
        super.update( delta );
        		
        setRefPixelPosition( bezier.getPointAt(percent) );
    }
	
	
	public static final void updateAnimation( int delta ) {
        tempoAcumulado += delta;
//        if (tempoAcumulado >= tempoAnimacao)
//            tempoAcumulado -= tempoAnimacao;
		
        percent = tempoAcumulado * 100 / tempoAnimacao;
	}
	
	public static final void resetTempo() {
		tempoAcumulado = 0;
		percent = 0;
	}
	
}
