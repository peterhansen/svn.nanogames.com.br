/*
 * Fisica.java
 *
 * Created on May 17, 2007, 7:19 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author malvezzi
 */
public class Fisica {
    
    private static final byte CALCULO_MUA    = 0;
    private static final byte CALCULO_MRU    = 1;
    private static final byte CALCULO_BEZIER = 2;
    private static final byte CALCULO_FOLLOW = 3;
    private static final byte CALCULO_P2P    = 4;
    private byte tipoDeCalculo;
    private final Point ds	    	= new Point();
    private final Point dx		= new Point();
    private final Point dxa		= new Point();
    private final Point pt		= new Point();
    private final Point dv		= new Point();
    private final Point dva		= new Point();	
    private final Point v0		= new Point();
    private final Point s0		= new Point();
    private final Point a		= new Point();
    private int tempoTotal;
    private int tempoAcumulado;
    private Point preencher;
    private static boolean active = true;

	
    /**
     * Creates a new instance of Fisica
     */
    public Fisica() {
		
    }
    
    public final void setActive(boolean x){
	active = x;
    }
    
    
    public final void setMUA( Point a_v0, Point a_ac, Point ps ) {
        tipoDeCalculo = CALCULO_MUA;
	preencher = ps;
	v0.set(a_v0);
	a.set(a_ac);
		
    }
    
    public final void setMRU( Point a_v0 , Point ps ) {
        tipoDeCalculo = CALCULO_MRU;
	v0.set(a_v0);
	preencher = ps;
		
    }    
    
    
    public final void setFOLLOW( Point vel, Point ps) {
        tipoDeCalculo = CALCULO_FOLLOW;
	preencher = ps;
		
    }
    
    
	/**
	 * 
	 * @param pT Ponto Target
	 * @param ps Ponto a ser alterado
	 * @param tempo Tempo para a anima��o.
	 *
	 */
    public final void setP2P( Point pT, Point ps, int tempo) {
	tipoDeCalculo = CALCULO_P2P;
	tempoTotal = tempo;
	pt.set(pT);
	// s = s0 + vt => v = (s-s0)/t
	dv.x = (pt.x-ps.x)*1000/tempoTotal;  //vel/s
	dv.y = (pt.y-ps.y)*1000/tempoTotal;  //vel/s
	//ds guarda o ponto de inicial do movimento.
	ds.set(ps);
        preencher = ps;
    }
    
    
    public final void atualiza( int delta ) {
	if (active==false)
	    return;
	
        switch ( tipoDeCalculo ) {
        case CALCULO_BEZIER:
            break;

        case CALCULO_MRU: //s=s0+vt
            ds.set( dxa.x + ( v0.x * delta ), dxa.y + (v0.y * delta));
            
            dx.x  = ds.x / 1000;
            dxa.x = ds.x % 1000;
            dx.y  = ds.y / 1000;
            dxa.y = ds.y % 1000;
            
            preencher.add(dx);
            break;
				
        case CALCULO_MUA:// s = s0+vt => v = v0 + at   V0 � alterado a cada loop
                         // s = s0 + vt
            ds.set( dxa.x + ( v0.x * delta ), dxa.y + (v0.y * delta));
            dx.x  = ds.x / 1000;
            dxa.x = ds.x % 1000;
            dx.y  = ds.y / 1000;
            dxa.y = ds.y % 1000;

	    preencher.add(dx);
				
		//v = v0 + at
	    dv.set( dva.x + ( a.x * delta ), dva.y + ( a.y * delta ));
	    dva.x = dv.x % 1000;
	    dva.y = dv.y % 1000;
	    dv.x  = dv.x / 1000;
	    dv.y  = dv.y / 1000;

	    v0.add( dv );
	// NAO ESTA PRONTO !!!  TESTAR !!!!
	    break;
				
	case CALCULO_FOLLOW: // manhattan distance ruled...
				
	    int moduloVel = v0.x + v0.y;
	    int moduloDir;

	    dv.x = pt.x - preencher.x;
	    dv.y = pt.y - preencher.y;
				
	    moduloDir = dv.x + dv.y;
				
	    dva.x = (dv.x * moduloVel)/moduloDir;
	    dva.y = (dv.y * moduloVel)/moduloDir;
				
	    ds.set( dxa.x + ( dva.x * delta ), dxa.y + (dva.y * delta));
	    dx.x  = ds.x / 1000;
	    dxa.x = ds.x % 1000;
	    dx.y  = ds.y / 1000;
	    dxa.y = ds.y % 1000;
				
	    preencher.add(dx);
	    break;
                                
	case CALCULO_P2P: // Ponto a Ponto...//s = s0+vt
					    // (s-s0)/t = v
	    tempoAcumulado += delta;
	    if (tempoAcumulado > tempoTotal){
		dv.set(0,0);
		ds.set(0,0);
	    }
	    else{
		dx.set(ds.x+dv.x*tempoAcumulado, ds.y + dv.y*tempoAcumulado);
            }
	    
            preencher.add(dx);
	    break;
        }
    }
    
}
