import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import java.util.Random;
/*
 * BackGroundSky.java
 *
 * Created on 7 de Junho de 2007, 15:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Malvezzi
 */
public class BackGroundSky extends UpdatableGroup implements RbConfig  {
    protected Pattern sky				 = new Pattern( new DrawableImage( "/azulzim.png" ));
    protected DrawableImage[] nuvemPequena = new DrawableImage[NUVEMPQN];
    protected DrawableImage[] nuvemMedia   = new DrawableImage[NUVEMMDA];
    protected DrawableImage[] nuvemGrande  = new DrawableImage[NUVEMGRD];

	protected Fisica[] fisicaNuvemPequena	= new Fisica[NUVEMPQN];
	protected Fisica[] fisicaNuvemMedia		= new Fisica[NUVEMMDA];
	protected Fisica[] fisicaNuvemGrande	= new Fisica[NUVEMGRD];

	private final Random rand					= new Random( teste += 34217 );
	static long teste = System.currentTimeMillis();
	
    
    /** Creates a new instance of BackGroundSky */
    public BackGroundSky( ) throws Exception{
        super(NUVEMPQN + NUVEMMDA + NUVEMGRD + 100);
        criaAnimacao();
    }
    
	
    public void criaAnimacao()throws Exception{
        sky.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		insertDrawable(sky);
		
		createNuvemArray(nuvemGrande,  fisicaNuvemGrande,  "/CloudGRD.png");
		createNuvemArray(nuvemMedia,   fisicaNuvemMedia,   "/CloudMDA.png");
		createNuvemArray(nuvemPequena, fisicaNuvemPequena, "/CloudPQN.png");
	}

	
	protected void createNuvemArray(DrawableImage[] vetDraw, Fisica[] vetFis, String imagem)throws Exception{
		for(int i=0; i < vetDraw.length; ++i){
			vetDraw[i] = new DrawableImage( imagem );
			vetDraw[i].setPosition(-ScreenManager.SCREEN_HALF_WIDTH,ScreenManager.SCREEN_HEIGHT);
			vetFis[i] = new Fisica();
     		vetFis[i].setMRU(new Point(-3000,1), vetDraw[i].getPosition());
			insertDrawable(vetDraw[i]);
		}
	}
    

	protected void updateNuvemArray(DrawableImage[] vetDraw, Fisica[] vetFis, int retardo, int delta){
		for(int i = 0; i < vetDraw.length;i++){
			vetFis[i].atualiza(delta);

			if(vetDraw[i].getPosition().x < 0 - vetDraw[i].getSize().x){
				int height;
				int velocidade;

				//altera a posicao da nuvem
				height = ScreenManager.SCREEN_HEIGHT - rand.nextInt(ScreenManager.SCREEN_HEIGHT);
				velocidade = - ( ((ScreenManager.SCREEN_HEIGHT - height)/retardo) + rand.nextInt( ScreenManager.SCREEN_WIDTH/(retardo*2)));
						
				vetDraw[i].setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt(ScreenManager.SCREEN_WIDTH), height);
				//altera a velocidade da nuvem
				vetFis[i].setMRU( new Point (velocidade, rand.nextInt(2)-1 ), vetDraw[i].getPosition());
			}
		}
	}
	
	
    public void update(int delta){
		super.update(delta);
		
		updateNuvemArray( nuvemPequena, fisicaNuvemPequena, RETARDOPQN, delta );
		updateNuvemArray( nuvemMedia,   fisicaNuvemMedia,   RETARDOMDA, delta );
		updateNuvemArray( nuvemGrande,  fisicaNuvemGrande,  RETARDOGRD, delta );
    }
}
