import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
/*
 * AnimatedBalloon.java
 *
 * Created on May 18, 2007, 5:28 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class AnimatedBalloon extends DrawableImage implements Updatable,RbConfig{
    final Point velocidade				= new Point();
    private final Point ds				= new Point();
    private final Point lastModule		= new Point();
    private final Point dx				= new Point();
    private final Point atrito			= new Point(0,30); 
    private final Point dvAcumulado		= new Point();
    private final Point dvAtrito		= new Point();
    private final Point dtAtrito		= new Point(500,500);
    private final Point realposition	= new Point();
    private final Point velo			= new Point();
	
    
    
    private final int abVel = 60;
    
    public static final byte UP    = 1;
    public static final byte DOWN  = 2;
    public static final byte LEFT  = 4;
    public static final byte RIGHT = 8;
    
    //private final Point 
    private final int dv = 1;
    
    /** Creates a new instance of AnimatedBalloon */
    public AnimatedBalloon() throws Exception {
        super("/balao.png"); 
        setPosition( POSICAOINICIAL_X, POSICAOINICIAL_Y );
        realposition.set( POSICAOINICIAL_X, POSICAOINICIAL_Y );
        velo.set(atrito);
    }

    
    public void setPressedVelo(int Dir){
        
        switch(Dir){
        case UP:
            velo.y = -abVel;
            break;
        case DOWN:
            velo.y =  abVel;
            break;
        case LEFT:
            velo.x = -abVel;
            break;
        case RIGHT:
            velo.x =  abVel;
            break;
        }
    }
     

    public void setReleasedVelo(int Dir){
        
        switch(Dir){
        case UP:
        case DOWN:
            velo.y = atrito.y;
            break;
        case LEFT:
        case RIGHT:
            velo.x = atrito.x;
            break;
        }
    }
     

    public void resetVelo(){
        velo.set(atrito);
    }
    
    
    public void update(int delta) {
        
        ds.set( lastModule.x + ( velocidade.x * delta ), lastModule.y + (velocidade.y * delta));
        dx.x = ds.x / 1000;
        lastModule.x = ds.x % 1000;
        dx.y = ds.y / 1000;
        lastModule.y = ds.y % 1000;
        
        realposition.add(dx);
		//Rela��o da posi��o na tela com a posi��o "real".
		position.set( realposition.x - TelaKidsGame.offset, realposition.y );
		
		if ( realposition.x <= TelaKidsGame.offset ) {
            velocidade.x = - velocidade.x;
            realposition.x = 0 + TelaKidsGame.offset;
        }
        
        if ( realposition.y >= ScreenManager.SCREEN_HEIGHT - size.y) {
            velocidade.y = - velocidade.y;
            realposition.y = ScreenManager.SCREEN_HEIGHT - size.y;
        } else if ( realposition.y <= 0 ) {
			velocidade.y = - velocidade.y;
            realposition.y = 0;
        }
        
        dvAtrito.set(dvAcumulado.x + ((velo.x - velocidade.x) * delta), dvAcumulado.y + ((velo.y - velocidade.y) * delta));
       //dvAtrito.set(dvAcumulado.x + (atrito.x * delta),dvAcumulado.y + (atrito.y * delta));

        dvAcumulado.x = dvAtrito.x % dtAtrito.x;
        dvAcumulado.y = dvAtrito.y % dtAtrito.y;
        dvAtrito.x = dvAtrito.x / dtAtrito.x;
        dvAtrito.y = dvAtrito.y / dtAtrito.y;


       velocidade.add(dvAtrito);
	   
	   //Offset
       if (position.x > ScreenManager.SCREEN_HALF_WIDTH && dx.x > 0 )
			TelaKidsGame.offsetTarget += dx.x ;

       System.out.println("offset: " + TelaKidsGame.offsetTarget);
       System.out.println("x:  " + position.x + " y:  "+ position.y );
       System.out.println("rx: "+realposition.x + " ry: "+ realposition.y);
       //System.out.println("screen 3/4: " + ScreenManager.SCREEN_WIDTH * 3 / 4);
    }

}
