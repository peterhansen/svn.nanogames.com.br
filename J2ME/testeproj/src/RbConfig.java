/*
 * RbConfig.java
 *
 * Created on May 16, 2007, 2:23 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public interface RbConfig {
    
    /** Todas as configurações do jogo devem ficar aqui. */
    final int NUMERO_INICIAL_BALOES = 30;
    final int BALAO_X = 10;
    final int BALAO_Y = 40;
    
    
    
    //Arquivo de vocábulos
    final String VOCAB_FILE = "/arquivo.txt";
    
    //Img
    final String IMG_01 = "/main_cursor.png";
    
    //BackGroundSky
	//Utilizado no calculo da velocidade das nuvens
	final int RETARDOPQN = 6;
	final int RETARDOMDA = 8;
	final int RETARDOGRD = 20;
	final int NUVEMPQN = 10;
	final int NUVEMMDA =  5;
	final int NUVEMGRD =  2;
	
	
	//Splash
	final int SPLASH_RB_GP		= 3;
	final int SPLASH_NANO_GP	= 3;
	

	
	static int tempoAto1 = 2000;
	static int tempoAto2 = 2000;
	static int tempoAto3 = 2000;
	static int tempoAto4 = 3000;
	static int tempoAto5 = 1000;
	
	//KidsGame
	final int POSICAOINICIAL_X = 50;
	final int POSICAOINICIAL_Y = 50;

    
    
}
