/*
 * HighScoresScreen.java
 *
 * Created on June 25, 2007, 9:14 PM
 *
 */

package screens;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 *
 * @author peter
 */
public final class HighScoresScreen extends Menu implements Serializable, Constants {
	
	private static final byte TOTAL_SCORES = 5;
	
	private static final byte TOTAL_ITEMS = TOTAL_SCORES + 1;
	
	private static final int[] scores = new int[ TOTAL_SCORES ];
	
	private final Label title;
	private final Label[] scoreLabels = new Label[ TOTAL_SCORES ];
	
	private static HighScoresScreen instance;
	
	private int lastHighScoreIndex = -1;
	
	private final int BLINK_RATE = 388;
	
	private int accTime;
			
	
	/** Creates a new instance of HighScoresScreen */
	private HighScoresScreen( ImageFont font ) throws Exception {
		super( null, 0, TOTAL_ITEMS );
		
		int yLabel = MENU_ITEMS_SPACING;
		
		title = new Label( font, GameMIDlet.getText( TEXT_HIGH_SCORES ) );
		title.defineReferencePixel( title.getSize().x >> 1, 0 );
		title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, yLabel );
		insertDrawable( title );
		
		yLabel += title.getSize().y + ( MENU_ITEMS_SPACING << 1 );
		
		for ( int i = 0; i < scoreLabels.length; ++i ) {
			scoreLabels[ i ] = new Label( font, String.valueOf( i + 1 ) + ".  " + String.valueOf( scores[ i ] ) );
			final Label label = scoreLabels[ i ];
			
			label.defineReferencePixel( label.getSize().x >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, yLabel );
			
			yLabel += label.getSize().y + MENU_ITEMS_SPACING;
			
			insertDrawable( scoreLabels[ i ] );
		}
		
		setSize( ScreenManager.SCREEN_WIDTH, yLabel );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
	}
	
	
	public static final HighScoresScreen createInstance( ImageFont font ) throws Exception {
		if ( instance == null ) {
			instance = new HighScoresScreen( font );
			
			try {
				GameMIDlet.loadData( DATABASE_NAME, DATABASE_INDEX_SCORES, instance );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif

				for ( int i = 0; i < scores.length; ++i )
					scores[ i ] = ( scores.length - i ) * 1000;	
			}					
		}
		
		instance.updateLabels();
		
		return instance;
	}
	
	
	public static final boolean setScore( int score ) {
		for ( int i = 0; i < scores.length; ++i ) {
			if ( score > scores[ i ] ) {
				// �ltima pontua��o � maior que uma pontua��o anteriormente gravada
				for ( int j = scores.length - 1; j > i; --j ) {
					scores[ j ] = scores[ j - 1 ];
				}
				
				instance.lastHighScoreIndex = i;
				instance.accTime = 0;
				
				scores[ i ] = score;
				try {
					GameMIDlet.saveData( DATABASE_NAME, DATABASE_INDEX_SCORES, instance );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
				}
				
				return true;
			} // fim if ( score > scores[ i ] )
		} // fim for ( int i = 0; i < scores.length; ++i )
		
		return false;
	} // fim do m�todo setScore( int )
	
	
	public static final boolean isHighScore( int score ) {
		for ( int i = 0; i < scores.length; ++i ) {
			if ( score > scores[ i ] )
				return true;
		}
		
		return false;
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				if ( lastHighScoreIndex >= 0 )
					scoreLabels[ lastHighScoreIndex ].setVisible( true );
				
				lastHighScoreIndex = -1;
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
			
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				if ( lastHighScoreIndex >= 0 )
					scoreLabels[ lastHighScoreIndex ].setVisible( true );								

				lastHighScoreIndex = -1;
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			break;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )
	}

	
	public final void write( DataOutputStream output ) throws Exception {
		for ( int i = 0; i < scores.length; ++i )
			output.writeInt( scores[ i ] );
	}

	
	public final void read( DataInputStream input ) throws Exception {
		for ( int i = 0; i < scores.length; ++i )
			 scores[ i ] = input.readInt();
	}
	
	
	private final void updateLabels() {
		for ( int i = 0; i < scoreLabels.length; ++i ) {
			final Label label = scoreLabels[ i ];
			
			title.setText( GameMIDlet.getText( TEXT_HIGH_SCORES ) );
			title.defineReferencePixel( title.getSize().x >> 1, 0 );
			title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, title.getRefPixelY() );			
			
			label.setText( String.valueOf( i + 1 ) + ".  " + String.valueOf( scores[ i ] ) );
			
			label.defineReferencePixel( label.getSize().x >> 1, 0 );
			label.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, label.getRefPixelY() );
		}		
	}	

	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( lastHighScoreIndex >= 0 ) {
			accTime += delta;
			
			if ( accTime >= BLINK_RATE ) {
				accTime %= BLINK_RATE;
				scoreLabels[ lastHighScoreIndex ].setVisible( !scoreLabels[ lastHighScoreIndex ].isVisible() );
			}
		}
	}
	
	
	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerPressed( int x, int y ) {
		keyPressed( ScreenManager.KEY_BACK );
	}
	//#endif	
	
}
