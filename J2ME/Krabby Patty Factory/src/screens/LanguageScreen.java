/*
 * LanguageScreen.java
 *
 * Created on August 14, 2007, 4:12 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class LanguageScreen extends Menu implements Constants {
	
	public static final byte INDEX_TITLE		= 0;
	public static final byte INDEX_PORTUGUESE	= 1;
	public static final byte INDEX_SPANISH		= 2;
	public static final byte INDEX_ENGLISH		= 3;
	public static final byte INDEX_BACK			= 4;
	
	private static final byte TOTAL_LABELS = 5;
	
	private static final byte NUMBER_OF_ITEMS = TOTAL_LABELS + 1;
	
	
	/** Creates a new instance of LanguageScreen */
	public LanguageScreen( MenuListener listener, int id, ImageFont font ) throws Exception {
		super( listener, id, NUMBER_OF_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, 0 );
		
		final String[] texts = new String[] { GameMIDlet.getText( TEXT_LANGUAGE ),
											  GameMIDlet.getText( TEXT_PORTUGUESE ),
											  GameMIDlet.getText( TEXT_SPANISH ),
											  GameMIDlet.getText( TEXT_ENGLISH ),
											  GameMIDlet.getText( TEXT_BACK ),
												};

		int y = MENU_ITEMS_SPACING;
		for ( int i = 0; i < TOTAL_LABELS; ++i ) {
			final Label l = new Label( font, texts[ i ] );

			l.defineReferencePixel( l.getSize().x >> 1, 0 );
			l.setRefPixelPosition( size.x >> 1, y );
			insertDrawable( l );

			y += l.getSize().y;

			if ( i == 0 )
				y += MENU_ITEMS_SPACING;
		}

		setSize( size.x, y );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );				

		final Drawable cursor = GameMIDlet.getCursor();
		setCursor( cursor, Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );		
		
		setCircular( true );
		setCurrentIndex( 0 );		
	}
	
	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				setCurrentIndex( INDEX_BACK );

			case ScreenManager.KEY_SOFT_LEFT:
				key = ScreenManager.KEY_NUM5;
			
			default:
				super.keyPressed( key );
		}
	} // fim do m�todo keyPressed( int )	

	
	public final void setCurrentIndex( int index ) {
		if ( index == 0 ) {
			if ( currentIndex == 0 || currentIndex == ( activeDrawables - 1 ) )
				index = 1;
			else
				index = activeDrawables - 1;
		}
		
		super.setCurrentIndex( index );
	}	
	
}
