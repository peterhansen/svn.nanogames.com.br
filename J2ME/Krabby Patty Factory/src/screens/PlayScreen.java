/*
 * PlayScreen.java
 *
 * Created on May 3, 2007, 8:11 PM
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.ArmGroup;
import core.Character;
import core.Constants;
import core.FlyingGroup;
import core.Ingredient;
import core.MrKrabs;
import core.OrderQueue;
import core.Patty;
import core.PattyGroup;
import core.SatisfactionMeter;
import core.Tray;


/**
 *
 * @author Peter Hansen
 */
public final class PlayScreen extends UpdatableGroup implements KeyListener, Constants, ScreenListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif
{
	
	// n�mero m�ximo de itens do grupo
	private static final short NUMBER_OF_SLOTS = 25;
 
    // pontua��o do jogador. A pontua��o mostrada � incrementada parcialmente at� se chegar ao valor final.
	private int score;
	private int scoreShown;
	
	// incremento na pontua��o mostrada a cada frame
	private static final byte SCORE_INCREMENT = 5;
	
    // esteiras
	public final Tray tray;
	
    // jogador
	public final Character player;
	
	// bra�o mec�nico que atinge os ingredientes
	private final ArmGroup arm;
	 
    // n�vel atual
	private short level;
	
	// indicador do pr�ximo ingrediente a ser inserido
	private final OrderQueue orderQueue;
	
	// grupo que gerencia a troca de hamb�rguers
	private final PattyGroup pattyGroup;
	 
    // pedidos montados
	private Patty[] patties;
	
	// labels
	private final Label labelScore;
	private final Label labelIngredientScore;
	private final Label labelMultiplier;
	private final Label labelMessage;
	
	private boolean updateLabelIngredientScore;
	private int accLabelDx;
	
	// quantidade inicial de ingredientes perfeitos entregues em sequ�ncia para que o multiplicador seja aumentado.
	// Exemplo: 3 para passar de 1x para 2x, 6 para 3x, mais 9 para 4x, e assim por diante.
	private static final byte MULTIPLIER_INCREASE = 5;
	
	private short multiplier = 1;
	
	// vari�veis utilizadas para controlar o tempo em que a mudan�a do multiplicador � destacada
	private static final short MULTIPLIER_CHANGE_TIME = 666; // dem�in!
	private short multiplierChangeTime;
	
	// contador do combo (ingredientes perfeitos entregues em sequ�ncia)
	private short comboCounter;
	
	private int labelModule;
	
	private final FlyingGroup flyingGroup;
	
	private final MrKrabs mrKrabs;
	
	//#if SCREEN_SIZE != "SMALL"
		/** Sprite que indica os controles para uso do ponteiro. */
		private final Sprite pointerControl;
	//#endif
	
	private int newRecordIndex = -1;
	
	private static final short MAX_SATISFACTION = 30000;

	// satisfa��o geral dos clientes
	private int satisfaction = MAX_SATISFACTION;
	
	// satisfa��o m�nima e m�xima necess�rias para que o jogador passe de n�vel
	private static final short MIN_SATISFACTION_TO_PASS = MAX_SATISFACTION * 30 / 100;
	private static final short MAX_SATISFACTION_TO_PASS = MAX_SATISFACTION * 89 / 100;
	
	private static final short SATISFACTION_DECREASE_PER_MISSED_INGREDIENT	= -MAX_SATISFACTION / 6;
	
	// n�vel m�nimo de satisfa��o necess�rio para passar do n�vel atual
	private short satisfactionToPass;
	
	private static final short SATISFACTION_INCREASE_PER_PERFECT_INGREDIENT	= -SATISFACTION_DECREASE_PER_MISSED_INGREDIENT / 6;
	private static final short SATISFACTION_INCREASE_PER_GREAT_INGREDIENT	= SATISFACTION_INCREASE_PER_PERFECT_INGREDIENT * 80 / 100;
	private static final short SATISFACTION_INCREASE_PER_GOOD_INGREDIENT	= SATISFACTION_INCREASE_PER_PERFECT_INGREDIENT * 40 / 100;
	private static final short SATISFACTION_DECREASE_PER_BAD_INGREDIENT		= SATISFACTION_DECREASE_PER_MISSED_INGREDIENT / 2;

	public final SatisfactionMeter satisfactionMeter;
	private int satisfactionMeterIndex = -1;
	
	// estados da tela de jogo
	public static final byte STATE_PLAYING		= 0;
	public static final byte STATE_BEGIN_LEVEL	= STATE_PLAYING + 1;
	public static final byte STATE_END_LEVEL	= STATE_BEGIN_LEVEL + 1;
	public static final byte STATE_PAUSED		= STATE_END_LEVEL + 1;
	public static final byte STATE_GAME_OVER_1	= STATE_PAUSED + 1;
	public static final byte STATE_GAME_OVER_2	= STATE_GAME_OVER_1 + 1;
	public static final byte STATE_TUTORIAL_1	= STATE_GAME_OVER_2 + 1;
	public static final byte STATE_TUTORIAL_2	= STATE_TUTORIAL_1 + 1;
	
	private byte state;
	
	// caso seja false, � sinal de que o jogador cancelou o carregamento da tela de jogo
	private static boolean load;
	
	// cor de preenchimento do ch�o na vers�o de baixa mem�ria
	private static final int FLOOR_FILL_COLOR = 0x8E4E00;
	
	// momento da �ltima vez que o jogador entrou no modo PLAYING - valor utilizado para reduzir probabilidade de problemas 
	// causados por chamadas r�pidas consecutivas da tela de pausa quando o jogador est� para perder o jogo. Solu��o
	// deselegante para o problema detectado no Samsung X820, e que ocorre em todos os aparelhos, por�m s� em alguns causa
	// problemas mais s�rios, como o pr�prio X820, onde o Sr. Siriguejo p�ra de responder a eventos - na maioria dos
	// aparelhos, causa apenas erros nos soft keys, mas pode-se considerar isso apenas uma "feature"
	private long lastPlayTime;
	
	// intervalo m�nimo entre 2 pausas
	private int MIN_TIME_BETWEEN_PAUSES = 555;

	private final Pattern bkg;
	private final Pattern floor;
	private final DrawableImage door;
	private final DrawableImage window;
	
	
	public PlayScreen( byte character, LoadScreen loadScreen ) throws Exception {
		super( NUMBER_OF_SLOTS );
		load = true;
		
		checkLoad();
        
		loadScreen.setProgress( 0 );
        Ingredient.loadSprites();
		loadScreen.setProgress( 20 );
		
		checkLoad();
		
		// insere o background
		DrawableImage img = new DrawableImage( PATH_IMAGES + "bkg_pattern.png" );
		loadScreen.setProgress( 25 );
		bkg = new Pattern( img );
		loadScreen.setProgress( 30 );	
		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( bkg );
		
		checkLoad();	
		
		// janela com o Sr. Siriguejo e porta
		if ( GameMIDlet.isLowMemory() ) {
			door = null;
			window = null;
		} else {
			window = new DrawableImage( PATH_IMAGES + "window.png" );
			loadScreen.setProgress( 35 );
			insertDrawable( window );
			checkLoad();
			
			door = new DrawableImage( PATH_IMAGES + "door.png" );
			loadScreen.setProgress( 40 );
			door.defineReferencePixel( door.getSize() );
			insertDrawable( door );
		}
		
		checkLoad();
		
		// aloca e insere o personagem
		player = new Character( this, character );
		loadScreen.setProgress( 45 );
        insertDrawable( player );
		checkLoad();

		// aloca o indicador de ingredientes do pedido atual (ser� inserido posteriormente)
		orderQueue = new OrderQueue();
		loadScreen.setProgress( 50 );
		checkLoad();
        tray = new Tray( player.type, this, orderQueue );
		loadScreen.setProgress( 55 );
		
		checkLoad();
		
		floor = new Pattern( GameMIDlet.isLowMemory() ? null : new DrawableImage( PATH_IMAGES + "floor.png" ) );
		floor.setFillColor( FLOOR_FILL_COLOR );
		loadScreen.setProgress( 60 );
		checkLoad();
		
		floor.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - tray.wallPattern.getPosition().y - tray.wallPattern.getSize().y );
		floor.defineReferencePixel( 0, floor.getSize().y );
		insertDrawable( floor );
		
		checkLoad();

		insertDrawable( orderQueue );
		
		insertDrawable( tray );
		
		// aloca e insere o bra�o mec�nico
		arm = new ArmGroup( this, tray.getTargetOffset() );
		loadScreen.setProgress( 65 );
		insertDrawable( arm );
		
		checkLoad();
		
		// cria o grupo dos hamb�rguers
		pattyGroup = new PattyGroup();
		loadScreen.setProgress( 70 );
		insertDrawable( pattyGroup );
		
		checkLoad();
		
		// cria e insere o grupo dos ingredientes "voadores"
		flyingGroup = new FlyingGroup( pattyGroup );
		loadScreen.setProgress( 75 );
		flyingGroup.setSize( size );
		
		checkLoad();
		
		insertDrawable( flyingGroup );

		// cria o indicador de satisfa��o dos clientes (inser��o � feita depois, para evitar erros de redesenho durante
		// o tutorial)
		satisfactionMeter = new SatisfactionMeter();
		loadScreen.setProgress( 80 );
		
		checkLoad();
		
		// cria e insere no grupo os labels
		labelScore = new Label( GameMIDlet.getDefaultFont(), String.valueOf( score ) );
		loadScreen.setProgress( 85 );
        insertDrawable( labelScore );
		
		checkLoad();
		
		labelIngredientScore = new Label( GameMIDlet.getDefaultFont(), "" );
		loadScreen.setProgress( 85 );
		insertDrawable( labelIngredientScore );
		labelIngredientScore.setVisible( false );
		
		checkLoad();
		
		labelMultiplier = new Label( GameMIDlet.getDefaultFont(), "" );
		loadScreen.setProgress( 90 );
		insertDrawable( labelMultiplier );
		
		//#if SCREEN_SIZE != "SMALL"
		if ( ScreenManager.getInstance().hasPointerEvents() ) {
			pointerControl = new Sprite( PATH_IMAGES + "button.dat", PATH_IMAGES + "button" );
			
			insertDrawable( pointerControl );
		} else {
			pointerControl = null;
		}
		//#endif		
		
		checkLoad();
		
		labelMessage = new Label( GameMIDlet.getBigFont(), "" );
		loadScreen.setProgress( 95 );
		labelMessage.defineReferencePixel( 0, labelMessage.getSize().y >> 1 );
		insertDrawable( labelMessage );
		
		// cria e insere o Sr. Siriguejo
		mrKrabs = new MrKrabs( this );
		insertDrawable( mrKrabs );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		loadScreen.setProgress( 100 );
	}

    
    public final void prepare( int level ) {
		try {
			this.level = ( short ) level;

			final int difficultyLevel = ( level > MAX_DIFFICULTY_LEVEL ? MAX_DIFFICULTY_LEVEL : level );

			orderQueue.prepare ( difficultyLevel );

			// prepara as esteiras
			tray.prepare( ( short ) difficultyLevel );
			
			player.setSequence( Character.SEQUENCE_STOPPED );
			
			// remove todos os ingredientes que possam estar voando
			Drawable d = flyingGroup.removeDrawable( 0 );
			while ( d != null ) {
				d = flyingGroup.removeDrawable( 0 );
			}

			patties = null;
			patties = new Patty[ orderQueue.getTotalOrders() ];
			for ( int i = 0; i < patties.length; ++i ) {
				patties[ i ] = new Patty( this, orderQueue.getOrderAt( i ).length );
				patties[ i ].setRefPixelPosition( size.x >> 1, ScreenManager.SCREEN_HEIGHT );
			}
			pattyGroup.insertDrawable( patties[ orderQueue.getCurrentOrderIndex() ] );
			updateScore( true );

			if ( level <= 1 ) {
				scoreShown = -1;
				score = 0;
				changeSatisfaction( MAX_SATISFACTION );
				updateScore( true );
				tray.setStop( false );
				setMultiplier( 1 );
			}
			
			satisfactionToPass = ( short ) ( MIN_SATISFACTION_TO_PASS + ( MAX_SATISFACTION_TO_PASS - MIN_SATISFACTION_TO_PASS ) * difficultyLevel / MAX_DIFFICULTY_LEVEL );
			satisfactionMeter.setMinSatisfaction( satisfactionToPass * 100 / MAX_SATISFACTION );
			
			
			if ( level == 0 ) {
				setState( STATE_TUTORIAL_1 );
			} else {
				setState( STATE_BEGIN_LEVEL );
				
				satisfactionMeter.setDemoMode( false );
				satisfactionMeter.setVisible( true );
				if ( satisfactionMeterIndex < 0 )
					satisfactionMeterIndex = insertDrawable( satisfactionMeter );

				if ( newRecordIndex >= 0 ) {
					removeDrawable( newRecordIndex );
					newRecordIndex = -1;
				}
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			GameMIDlet.setScreen( SCREEN_MAIN_MENU );
		}
	} // fim do m�todo prepare( int )
    
	
	public final void update( int delta ) {
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				moveLabel( labelMessage, delta );
				if ( labelMessage.getPosition().x <= -labelMessage.getSize().x )
					setState( state == STATE_BEGIN_LEVEL ? STATE_PLAYING : STATE_TUTORIAL_2 );
			break; // fim case STATE_BEGIN_LEVEL
			
			case STATE_END_LEVEL:
				// espera que o �ltimo pedido esteja pronto e fora da tela para iniciar o pr�ximo n�vel
				if ( flyingGroup.getUsedSlots() <= 0 && pattyGroup.getCurrentPatty() == null ) {
					//#if DEMO == "false"
						prepare( level + 1 );
					//#else
//# 						setState( STATE_GAME_OVER_1 );
					//#endif
				}
			break;
			
			case STATE_GAME_OVER_1:
				moveLabel( labelMessage, delta );
				if ( labelMessage.getRefPixelPosition().x <= ScreenManager.SCREEN_HALF_WIDTH )
					setState( STATE_GAME_OVER_2 );
			break;
			
			case STATE_PAUSED:
				return;
			
			case STATE_PLAYING:
				updateScore( false );
			break; // fim case STATE_PLAYING
			
			case STATE_TUTORIAL_1:
			case STATE_TUTORIAL_2:
				updateScore( false );
			break;
			
		} // fim switch ( state )
		
		if ( updateLabelIngredientScore )
			moveLabel( labelIngredientScore, delta );		
		
		if ( multiplierChangeTime != 0 ) {
			multiplierChangeTime -= delta;
			
			if ( multiplierChangeTime <= 0 ) {
				multiplierChangeTime = 0;
				labelMultiplier.setFont( GameMIDlet.getDefaultFont() );
			}
		}
		
		super.update( delta );		
	} // fim do m�todo update( int )

	
	public synchronized final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_MID:
						setState( STATE_PAUSED );
					return;
				} // fim switch ( key )	
			break;

			case STATE_PLAYING:
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						setState( STATE_PAUSED );
					return;

					case ScreenManager.UP:
					case ScreenManager.KEY_NUM1:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM3:
						hit( TRAY_TOP );
					return;

					case ScreenManager.FIRE:
					case ScreenManager.RIGHT:
					case ScreenManager.LEFT:					
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_NUM6:
						hit( TRAY_MIDDLE );
					return;
					
					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM7:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_NUM9:
						hit( TRAY_BOTTOM );
					return;
				}
			break;
			
			case STATE_TUTORIAL_1:
			case STATE_TUTORIAL_2:
			case STATE_GAME_OVER_1:
			case STATE_GAME_OVER_2:
				if ( mrKrabs != null )
					mrKrabs.keyPressed( key );
			return;			
			
		} // fim switch ( state )		
	} // fim do m�todo keyPressed( int )

	
	public final void keyReleased( int key ) {
		switch ( state ) {
			case STATE_TUTORIAL_1:
			case STATE_TUTORIAL_2:
			case STATE_GAME_OVER_1:
			case STATE_GAME_OVER_2:
				if ( mrKrabs != null )
					mrKrabs.keyReleased( key );
			break;
		}
	}

	
	/**
	 * M�todo chamado no momento que o personagem atinge uma esteira.
	 * @param trayIndex �ndice da esteira atingida.
	 */
	public final void removeIngredientAt( byte trayIndex ) {
		final Ingredient ingredient = tray.removeIngredientAtTray( trayIndex );
		if ( ingredient != null ) {
			flyingGroup.insertDrawable( ingredient );
			
			final byte ingredientAddResult = orderQueue.addIngredient( ingredient.getType() );
			
			if ( ( ingredientAddResult & OrderQueue.RESULT_INCORRECT_INGREDIENT ) != 0 ) {
				changeSatisfaction( SATISFACTION_DECREASE_PER_MISSED_INGREDIENT );
				
				ingredient.setPrecisionLevel( PRECISION_BAD );
				setMultiplier( 1 );
				
				if ( state == STATE_PLAYING )
					MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
			} else {
				switch ( ingredient.getPrecisionLevel() ) {
					case PRECISION_PERFECT:
						comboCounter += 2;
						if ( comboCounter >= ( MULTIPLIER_INCREASE * multiplier ) )
							setMultiplier( multiplier + 1 );
						
						changeSatisfaction( SATISFACTION_INCREASE_PER_PERFECT_INGREDIENT );
					break;

					case PRECISION_GREAT:
						++comboCounter;
						if ( comboCounter >= ( MULTIPLIER_INCREASE * multiplier ) )
							setMultiplier( multiplier + 1 );
						
						changeSatisfaction( SATISFACTION_INCREASE_PER_GREAT_INGREDIENT );
					break;
					
					case PRECISION_GOOD:
						changeSatisfaction( SATISFACTION_INCREASE_PER_GOOD_INGREDIENT );
						setMultiplier( 1 );
					break;
					
					default:
						changeSatisfaction( SATISFACTION_DECREASE_PER_BAD_INGREDIENT );
						setMultiplier( 1 );
				} // fim switch ( ingredient.getPrecisionLevel() )
				
				score += ingredient.getScore() * multiplier;
			}
		} // fim if ( ingredient != null )
	} // fim do m�todo removeIngredientAt( int )
	
	
	public final void hit( byte trayIndex ) {
		arm.hit( trayIndex );
		tray.hit();
	}
	
	
	public final void changePatty() {
		player.setSequence( Character.SEQUENCE_PATTY );
		
		if ( orderQueue.hasMoreOrders() ) {
			pattyGroup.insertDrawable( patties[ orderQueue.getCurrentOrderIndex() ] );
		} else {
			pattyGroup.setMoving( true );
			if ( level > 0 ) {
				// caso o jogador esteja no tutorial, deve passar de n�vel manualmente
				if ( satisfaction >= satisfactionToPass ) {
					//#if DEMO == "false"
						MediaPlayer.play( SOUND_NEXT_LEVEL, 1 );
					//#endif

					setState( STATE_END_LEVEL );
				} else {
					setState( STATE_GAME_OVER_1 );
				}
			}
		}
	} // fim do m�todo updatePatty( boolean, boolean )
	
	
	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += SCORE_INCREMENT * multiplier;
				if ( scoreShown > score )
					scoreShown = score;
			}
			labelScore.setText( String.valueOf( scoreShown ) );
		}
	} // fim do m�todo updateScore( boolean )
	
	
	public final void setState( int state ) {
		try {
			switch ( state ) {
				case STATE_BEGIN_LEVEL:
					labelMessage.setText( GameMIDlet.getText( TEXT_LEVEL ) + " " + level );

					labelMessage.defineReferencePixel( 0, labelMessage.getSize().y >> 1 );
					labelMessage.setRefPixelPosition( size.x, size.y >> 1 );
					labelMessage.setVisible( true );
					labelModule = 0;
					
					mrKrabs.setState( MrKrabs.STATE_NONE );
				break;

				case STATE_END_LEVEL:
				break;

				case STATE_GAME_OVER_1:
					updateScore( true );

					player.setSequence( Character.SEQUENCE_GAME_OVER );
					tray.setStop( true );

					labelMessage.setText( GameMIDlet.getText( TEXT_GAME_OVER ) );

					labelMessage.setRefPixelPosition( size.x, ( size.y >> 1 ) - labelMessage.getSize().y );
					labelMessage.defineReferencePixel( labelMessage.getSize().x >> 1, labelMessage.getSize().y >> 1 );
					labelMessage.setVisible( true );
					labelModule = 0;
					
					if ( pattyGroup.getCurrentPatty() != null )
						pattyGroup.getCurrentPatty().setVisible( false );
					
					mrKrabs.setState( MrKrabs.STATE_GAME_OVER );
					
					satisfactionMeter.setVisible( false );
					
					MediaPlayer.play( SOUND_GAME_OVER, 1 );
				break;

				case STATE_GAME_OVER_2:
					updateScore( true );
					
					player.setSequence( Character.SEQUENCE_GAME_OVER );
					labelMessage.setRefPixelPosition( size.x >> 1, labelMessage.getRefPixelY() );

					if ( HighScoresScreen.isHighScore( score ) ) {
						final Label labelHighScore = new Label( GameMIDlet.getDefaultFont(), GameMIDlet.getText( TEXT_NEW_RECORD ) );
						labelHighScore.defineReferencePixel( labelHighScore.getSize().x >> 1, 0 );
						labelHighScore.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, labelMessage.getPosition().y + labelMessage.getSize().y );

						newRecordIndex = insertDrawable( labelHighScore );
					} // fim if ( HighScoresScreen.isHighScore( score ) )
				break;

				case STATE_PAUSED:
					if ( System.currentTimeMillis() - lastPlayTime < MIN_TIME_BETWEEN_PAUSES || GameMIDlet.getInstance() == null )
						return;
					
					labelMessage.setVisible( false );
					GameMIDlet.setScreen( SCREEN_PAUSE );

					GameMIDlet.setBackground( true, true, BACKGROUND_COLOR );
				break;

				case STATE_PLAYING:
					lastPlayTime = System.currentTimeMillis();
					
					labelMessage.setVisible( false );
					tray.setActive( true );
					tray.setMoveSlots( true );
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_PAUSE );
				break;

				case STATE_TUTORIAL_1:
					mrKrabs.setState( MrKrabs.STATE_ASK_USER );
				case STATE_TUTORIAL_2:
				break;

				default:
					return;
			} // fim switch ( state )

			this.state = ( byte ) state;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif			
			
			backToMainMenu();
		}
	} // fim do m�todo setState( int )
	
	
	public final void unpause() {
		GameMIDlet.setBackground( false, false, -1 );

		switch ( this.state ) {
			case STATE_TUTORIAL_1:
			case STATE_TUTORIAL_2:
			break;					

			case STATE_PAUSED:
				setState( STATE_PLAYING );
			default:
				GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_PAUSE );
		}		
	}
	

	private final void setMultiplier( int multiplier ) {
		final short previousMultiplier = this.multiplier;
		this.multiplier = ( byte ) multiplier;
		
		if ( multiplier != previousMultiplier ) {
			multiplierChangeTime = MULTIPLIER_CHANGE_TIME;
			labelMultiplier.setFont( GameMIDlet.getBigFont() );
			
			if ( multiplier > previousMultiplier ) {
				MediaPlayer.play( SOUND_MULTIPLIER, 1 );
				player.setSequence( Character.SEQUENCE_HAPPY );
			}
		}
		
		comboCounter = 0;
		
		labelMultiplier.setText( String.valueOf( multiplier ) + 'X' );
	}

	
	public final void ingredientInserted( Ingredient insertedIngredient ) {
		String text;
		switch ( insertedIngredient.getPrecisionLevel() ) {
			case PRECISION_PERFECT:
				text = GameMIDlet.getText( TEXT_PERFECT );
			break;
			
			case PRECISION_GREAT:
				text = GameMIDlet.getText( TEXT_GREAT );
			break;
			
			case PRECISION_GOOD:
				text = GameMIDlet.getText( TEXT_GOOD );
			break;
			
			case PRECISION_BAD:
				if ( state == STATE_PLAYING )
					MediaPlayer.play( SOUND_BAD_MOVE, 1 );
				
				text = GameMIDlet.getText( TEXT_BAD );
			break;
			
			default:
				return;
		}
		
		labelIngredientScore.setText( text );
		labelIngredientScore.defineReferencePixel( labelIngredientScore.getSize().x >> 1, labelIngredientScore.getSize().y );
		labelIngredientScore.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 
				patties[ 0 ].getPosition().y + insertedIngredient.getPosition().y );
		labelModule = 0;
		updateLabelIngredientScore = true;
		labelIngredientScore.setVisible( true );
		accLabelDx = 0;
	}

	
	private final void moveLabel( Label label, int delta ) {
		final int dxLabel = labelModule + ( LABEL_LEVEL_SPEED * delta );
		final int dx = dxLabel / 1000;
		labelModule = dxLabel % 1000;

		if ( dx != 0 ) {
			if ( label == labelMessage ) {
				labelMessage.move( dx, 0 );

				if ( labelMessage.getPosition().x <= -labelMessage.getSize().x ) {
					labelMessage.setVisible( false );
				}
			} else if ( label == labelIngredientScore ) {
				accLabelDx += dx;
				label.move( 0, dx );
				
				if ( accLabelDx <= ( -label.getSize().y << 1 ) ) {
					updateLabelIngredientScore = false;
					label.setVisible( false );
					
					// mostra a pr�xima parte do tutorial (caso o jogador n�o tenha pulado)
					if ( level == 0 && mrKrabs != null && state != STATE_END_LEVEL )
						mrKrabs.setVisible( true );					
				}
			}
		} // fim if ( dx != 0 )
	}

	
	public final void missedIngredient() {
		changeSatisfaction( SATISFACTION_DECREASE_PER_MISSED_INGREDIENT );
		
		if ( state == STATE_PLAYING ) {
			// s� toca o som no caso de a vibra��o estar desabilitada, pois v�rios aparelhos apresentam problema no som
			// ao se tocar um som e vibrar ao mesmo tempo
			if ( MediaPlayer.isVibration() )
				MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
			else
				MediaPlayer.play( SOUND_BAD_MOVE, 1 );
			
			player.setSequence( Character.SEQUENCE_MISS );
		}
		setMultiplier( 1 );
	}
	
	
	private final void changeSatisfaction( int ds ) {
		if ( ds > 0 && multiplier > 1 ) {
			// a satisfa��o dos clientes aumenta mais rapidamente quando o multiplicador cresce
			ds += ds * multiplier >> 2;
		}
		
		satisfaction += ds;
		
		if ( satisfaction > MAX_SATISFACTION ) {
			satisfaction = MAX_SATISFACTION;
		} else if ( satisfaction <= satisfactionToPass ) {
			switch ( state ) {
				case STATE_GAME_OVER_1:
				case STATE_GAME_OVER_2:
				break;
				
				default:
					if ( satisfaction <= 0 ) {
						satisfaction = 0;
						setState( STATE_GAME_OVER_1 );
					} else {
						player.setSequence( Character.SEQUENCE_SCARED );
					}
				// fim default
			} // fim switch ( state )
		} else {
			player.setSequence( Character.SEQUENCE_BLINKING );
		}
		
		satisfactionMeter.setSatisfaction( satisfaction * 100 / MAX_SATISFACTION );
	}
	
	
	public final void backToMainMenu() {
		if ( HighScoresScreen.setScore( score ) )
			GameMIDlet.setScreen( SCREEN_HIGH_SCORES );
		else
			GameMIDlet.setScreen( SCREEN_MAIN_MENU );

		score = 0;
	}

	
	public final int getScore() {
		return score;
	}

	
	public final boolean isSatisfactionAboveMin() {
		return satisfaction > satisfactionToPass;
	}
	
	
	public final boolean isGameOver() {
		switch ( state ) {
			case STATE_GAME_OVER_1:
			case STATE_GAME_OVER_2:
				return true;
				
			default:
				return false;
		}
	}
	
	
	public static final void cancelLoad() {
		load = false;
	}

	
	public final void hideNotify() {
		switch ( state ) {
			case STATE_PLAYING:
			case STATE_BEGIN_LEVEL:
				lastPlayTime = 0;
				setState( STATE_PAUSED );
			break;
		}
	}

	
	public final void showNotify() {
	}
	
	
	private final void checkLoad() throws Exception {
		if ( !load )
			throw new Exception();		
		
		Thread.yield();
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != size.x || height != size.y ) {
			setSize( width, height );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

		// janela com o Sr. Siriguejo e porta
		if ( window != null && door != null ) {
			window.setPosition( 0, 0 );
			door.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		}

		player.setRefPixelPosition( size.x >> 1, tray.control.getPosition().y );

		// posiciona o indicador de pedidos dentro da �rea do visor
		orderQueue.setPosition( tray.getPosition().x + tray.control.getPosition().x + tray.control.screenPosition.x + ORDER_INDICATOR_INGREDIENT_X,
								tray.getPosition().y + tray.control.getPosition().y + tray.control.screenPosition.y + ORDER_INDICATOR_INGREDIENT_Y );

		floor.setSize( size.x, size.y - tray.wallPattern.getPosition().y - tray.wallPattern.getSize().y );
		floor.defineReferencePixel( 0, floor.getSize().y );
		floor.setRefPixelPosition( 0, size.y );

		if ( patties != null ) {
			for ( int i = 0; i < patties.length; ++i ) {
				if ( patties[ i ] != null )
					patties[ i ].setRefPixelPosition( size.x >> 1, ScreenManager.SCREEN_HEIGHT );
			}
		}

		pattyGroup.setSize( size );
		flyingGroup.setSize( size );

		labelMultiplier.setPosition( labelScore.getPosition().x, labelScore.getPosition().y + labelScore.getSize().y );

		satisfactionMeter.setRefPixelPosition( 0, ScreenManager.SCREEN_HEIGHT );

		//#if SCREEN_SIZE != "SMALL"
			if ( pointerControl != null ) {
				pointerControl.setPosition( orderQueue.getPosX() + BUTTON_X_OFFSET, orderQueue.getPosY() + BUTTON_Y_OFFSET );
			}
		//#endif
	}
	
	
	//#if SCREEN_SIZE != "SMALL"
	
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		switch ( state ) {
			case STATE_TUTORIAL_1:
			case STATE_TUTORIAL_2:
				if ( mrKrabs.contains( x, y ) ) {
					mrKrabs.setState( MrKrabs.STATE_BEGIN_GAME );
					break;
				}
				
			default:
				int index = -1;
				if ( pointerControl != null && pointerControl.contains( x, y ) ) {
					y -= pointerControl.getPosY();
					index = y * 3 / pointerControl.getHeight();
				} else {
					y -= tray.getPosY();
					index = tray.getTrayAt( y );
				}
				
				switch ( index ) {
					case 0:
						keyPressed( ScreenManager.KEY_NUM2 );
						pointerControl.setSequence( 1 );
					break;

					case 1:
						keyPressed( ScreenManager.KEY_NUM5 );
						pointerControl.setSequence( 2 );
					break;

					case 2:
						keyPressed( ScreenManager.KEY_NUM8 );
						pointerControl.setSequence( 3 );
					break;
				}
				
			// fim default
		}
	}


	public final void onPointerReleased( int x, int y ) {
		keyReleased( 0 );
	}
	
	
	//#endif
	
}
 

