/*
 * GameMIDlet.java
 *
 * Created on May 3, 2007, 8:11 PM
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Serializable;
import core.Character;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;


/**
 *
 * @author  peter
 * @version
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable {
	
	// tempo m�ximo considerado de um frame
	private static final int GAME_MAX_FRAME_TIME = 107;
	
	public static final byte LANGUAGE_ENGLISH		= 0;
	public static final byte LANGUAGE_PORTUGUESE	= 1;
	public static final byte LANGUAGE_SPANISH		= 2;
	
	public static final byte LANGUAGE_TOTAL			= 3;
	
	//#if DEFAULT_LANGUAGE == "pt"
		private static final byte DEFAULT_LANGUAGE = LANGUAGE_PORTUGUESE;
	//#else 
		//#if DEFAULT_LANGUAGE == "es"
//# 			private static final byte DEFAULT_LANGUAGE = LANGUAGE_SPANISH;
		//#else
//# 			private static final byte DEFAULT_LANGUAGE = LANGUAGE_ENGLISH;
		//#endif
	//#endif

	private static String[] LANGUAGE_IDS = { "en", "pt", "es" };
	
	// fontes utilizadas
	private static ImageFont FONT_DEFAULT;
	private static ImageFont FONT_BIG;
	
	private static PlayScreen playScreen;
	
	private byte character;
	
	
	//#if LOW_JAR == "false"
	private static BasicAnimatedPattern background;
	private static BubblesTransition transition;
	//#endif
	
	private boolean lowMemory;
	
	
	//#ifdef SWITCH_SOFT_KEYS
//# 	public GameMIDlet() {
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
//# 	}
	//#elifdef FORCE_MOTOROLA
//# 		public GameMIDlet() {
//# 			super( VENDOR_MOTOROLA, GAME_MAX_FRAME_TIME );
//# 		}
	//#endif
	
	
	protected final void loadResources() throws Exception {
		//#if LOW_JAR == "false"
			//#if SCREEN_SIZE != "SMALL"
				switch ( getVendor() ) {
					case VENDOR_SONYERICSSON:
						// todos os SonyEricsson possuem mem�ria e desempenho suficientes para rodar o jogo completo,
						// por�m como a mem�ria � din�mica, o valor retornado de totalMemory corresponde a somente
						// parte da mem�ria que pode ser de fato utilizada no jogo.
						( ( GameMIDlet ) instance ).lowMemory = false;
					break;

					default:
						( ( GameMIDlet ) instance ).lowMemory = ScreenManager.SCREEN_HEIGHT < 240 && Runtime.getRuntime().totalMemory() < MEMORY_LOW_LIMIT;
				}
			//#else
//# 					( ( GameMIDlet ) instance ).lowMemory = false;
			//#endif
		//#else
//# 				( ( GameMIDlet ) instance ).lowMemory = true;
		//#endif

		// na vers�o de baixa mem�ria, as transi��es de tela s�o imediatas (n�o h� a transi��o das bolhas)
		//#if LOW_JAR == "false"
		if ( !isLowMemory() )
			transition = new BubblesTransition();
		//#endif

		byte languageToLoad = DEFAULT_LANGUAGE;

		if ( hasDatabase( DATABASE_NAME, DATABASE_TOTAL ) ) {
			loadLanguage();

			// momentaneamente define um idioma inv�lido, para que garantidamente carregue os textos no m�todo
			// setLanguage()
			languageToLoad = language;
			language = -1;
		} else {
			// n�o havia op��o salva de idioma; utiliza o idioma padr�o definido

//					String defaultLanguage = System.getProperty( "microedition.locale" );
//					
//					// caso o aparelho n�o retorne corretamente o idioma, utiliza o idioma padr�o definido
//					if ( defaultLanguage != null ) {
//						defaultLanguage = defaultLanguage.toLowerCase();
//						
//						for ( int i = 0; i < LANGUAGE_IDS.length; ++i ) {
//							if ( defaultLanguage.indexOf( LANGUAGE_IDS[ i ] ) >= 0 ) {
//								languageToLoad = ( byte ) i;
//								break;
//							}
//						} // fim for ( int i = 0; i < LANGUAGE_IDS.length; ++i )
//					} // fim if ( defaultLanguage != null )

			try {
				createDatabase( DATABASE_NAME, DATABASE_TOTAL );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
						e.printStackTrace();
				//#endif
			} // fim catch ( Exception e )
		} // fim else => if ( hasDatabase( DATABASE_NAME ) )

		setLanguage( languageToLoad );


	//#if BLACKBERRY_API == "true"
//#
//# 			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
//# 			int[][] keys = null;
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 },
//# 						{ 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 },
//# 						{ 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 },
//# 						{ 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 },
//# 						{ 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 },
//# 						{ 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 },
//# 						{ 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 },
//# 						{ 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 },
//# 						{ 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 },
//# 						{ 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 },
//# 						{ 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 },
//# 						{ 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 },
//# 						{ 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 },
//# 						{ 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 },
//# 						{ 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 },
//# 						{ 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR },
//# 						{ 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR },
//# 						{ 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR },
//# 						{ 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR },
//# 						{ 'S', ScreenManager.KEY_STAR },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 },
//# 						{ ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					keys = new int[][] {
//# 						{ 'w', ScreenManager.KEY_NUM1 },
//# 						{ 'W', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM3 },
//# 						{ 'R', ScreenManager.KEY_NUM3 },
//# 						{ 'z', ScreenManager.KEY_NUM7 },
//# 						{ 'Z', ScreenManager.KEY_NUM7 },
//# 						{ 'c', ScreenManager.KEY_NUM9 },
//# 						{ 'C', ScreenManager.KEY_NUM9 },
//# 						{ 'e', ScreenManager.KEY_NUM2 },
//# 						{ 'E', ScreenManager.KEY_NUM2 },
//# 						{ 's', ScreenManager.KEY_NUM4 },
//# 						{ 'S', ScreenManager.KEY_NUM4 },
//# 						{ 'd', ScreenManager.KEY_NUM5 },
//# 						{ 'D', ScreenManager.KEY_NUM5 },
//# 						{ 'f', ScreenManager.KEY_NUM6 },
//# 						{ 'F', ScreenManager.KEY_NUM6 },
//# 						{ 'x', ScreenManager.KEY_NUM8 },
//# 						{ 'X', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'y', ScreenManager.KEY_NUM1 },
//# 						{ 'Y', ScreenManager.KEY_NUM1 },
//# 						{ 'i', ScreenManager.KEY_NUM3 },
//# 						{ 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'b', ScreenManager.KEY_NUM7 },
//# 						{ 'B', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 },
//# 						{ 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'u', ScreenManager.UP },
//# 						{ 'U', ScreenManager.UP },
//# 						{ 'h', ScreenManager.LEFT },
//# 						{ 'H', ScreenManager.LEFT },
//# 						{ 'j', ScreenManager.FIRE },
//# 						{ 'J', ScreenManager.FIRE },
//# 						{ 'k', ScreenManager.RIGHT },
//# 						{ 'K', ScreenManager.RIGHT },
//# 						{ 'n', ScreenManager.DOWN },
//# 						{ 'N', ScreenManager.DOWN },
//# 					 };
//# 			}
//# 			for ( byte i = 0; i < keys.length; ++i )
//# 				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
//#
//# 			// em aparelhos antigos (e/ou com vers�es de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms n�o recebe os eventos de teclado corretamente, sumindo somente ap�s a aplica��o ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
//#
	//#elif SCREEN_SIZE == "BIG"

		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
		final int[][] keys = {
				{ 'q', ScreenManager.KEY_NUM1 },
				{ 'Q', ScreenManager.KEY_NUM1 },
				{ 'e', ScreenManager.KEY_NUM3 },
				{ 'E', ScreenManager.KEY_NUM3 },
				{ 'z', ScreenManager.KEY_NUM7 },
				{ 'Z', ScreenManager.KEY_NUM7 },
				{ 'c', ScreenManager.KEY_NUM9 },
				{ 'C', ScreenManager.KEY_NUM9 },
				{ 'w', ScreenManager.UP },
				{ 'W', ScreenManager.UP },
				{ 'a', ScreenManager.LEFT },
				{ 'A', ScreenManager.LEFT },
				{ 's', ScreenManager.FIRE },
				{ 'S', ScreenManager.FIRE },
				{ 'd', ScreenManager.RIGHT },
				{ 'D', ScreenManager.RIGHT },
				{ 'x', ScreenManager.DOWN },
				{ 'X', ScreenManager.DOWN },

				{ 'r', ScreenManager.KEY_NUM1 },
				{ 'R', ScreenManager.KEY_NUM1 },
				{ 'y', ScreenManager.KEY_NUM3 },
				{ 'Y', ScreenManager.KEY_NUM3 },
				{ 'v', ScreenManager.KEY_NUM7 },
				{ 'V', ScreenManager.KEY_NUM7 },
				{ 'n', ScreenManager.KEY_NUM9 },
				{ 'N', ScreenManager.KEY_NUM9 },
				{ 't', ScreenManager.KEY_NUM2 },
				{ 'T', ScreenManager.KEY_NUM2 },
				{ 'f', ScreenManager.KEY_NUM4 },
				{ 'F', ScreenManager.KEY_NUM4 },
				{ 'g', ScreenManager.KEY_NUM5 },
				{ 'G', ScreenManager.KEY_NUM5 },
				{ 'h', ScreenManager.KEY_NUM6 },
				{ 'H', ScreenManager.KEY_NUM6 },
				{ 'b', ScreenManager.KEY_NUM8 },
				{ 'B', ScreenManager.KEY_NUM8 },

				{ 10, ScreenManager.FIRE }, // ENTER
				{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

				{ 'u', ScreenManager.KEY_STAR },
				{ 'U', ScreenManager.KEY_STAR },
				{ 'j', ScreenManager.KEY_STAR },
				{ 'J', ScreenManager.KEY_STAR },
				{ '#', ScreenManager.KEY_STAR },
				{ '*', ScreenManager.KEY_STAR },
				{ 'm', ScreenManager.KEY_STAR },
				{ 'M', ScreenManager.KEY_STAR },
				{ 'p', ScreenManager.KEY_STAR },
				{ 'P', ScreenManager.KEY_STAR },
				{ ' ', ScreenManager.KEY_STAR },
				{ '$', ScreenManager.KEY_STAR },
			 };
		for ( byte i = 0; i < keys.length; ++i )
			table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );

	//#endif


		//#ifdef NO_SOUND
//# 					MediaPlayer.init( DATABASE_NAME, DATABASE_INDEX_MEDIA, null );
//# 					MediaPlayer.setMute( true );
		//#else
			final String[] sounds = new String[] { PATH_SOUNDS + "splash.mid",
												   PATH_SOUNDS + "level.mid",
												   PATH_SOUNDS + "mult.mid",
												   PATH_SOUNDS + "gameover.mid",
												   PATH_SOUNDS + "bad.mid",

			};
			MediaPlayer.init( DATABASE_NAME, DATABASE_INDEX_MEDIA, sounds );
		//#endif

		manager = ScreenManager.createInstance( GAME_MAX_FRAME_TIME );
		manager.start();

		FONT_DEFAULT = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font.png", PATH_IMAGES + "font.dat" );

		if ( isLowMemory() )
			FONT_BIG = FONT_DEFAULT;
		else
			FONT_BIG = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_big.png", PATH_IMAGES + "font_big.dat" );

		HighScoresScreen.createInstance( FONT_DEFAULT );

		//#if LOW_JAR == "false"
			if ( !isLowMemory() )
				background = new BasicAnimatedPattern( new DrawableImage( PATH_SPLASH + "pattern.png" ), BACKGROUND_SPEED, BACKGROUND_SPEED );
		//#endif

		setScreen( SCREEN_CHOOSE_SOUND );
	} // fim do m�todo startApp()
	
	
	protected final int changeScreen( int index ) {
		Drawable nextScreen = null;

		final GameMIDlet midlet = ( GameMIDlet ) instance;

		boolean patternBkg = true;
		boolean updateBkg = true;
		int bgColor = BACKGROUND_COLOR;

		try {
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
			setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, -1 );
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );

			switch ( index ) {
				case SCREEN_CHOOSE_SOUND:
				case SCREEN_CONFIRM_MENU:
				case SCREEN_CONFIRM_EXIT:
					int idTitle = 0;
					byte defaultOption = BasicConfirmScreen.INDEX_NO;
					switch ( index ) {
						case SCREEN_CHOOSE_SOUND:
							patternBkg = false;
							idTitle = TEXT_DO_YOU_WANT_SOUND;
							if ( !MediaPlayer.isMuted() )
								defaultOption = BasicConfirmScreen.INDEX_YES;

							bgColor = 0x000000;
						break;

						case SCREEN_CONFIRM_MENU:
							idTitle = TEXT_CONFIRM_BACK_MENU;
						break;

						case SCREEN_CONFIRM_EXIT:
							idTitle = TEXT_CONFIRM_EXIT;
						break;
					} // fim switch ( index )

					nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, getCursor(), idTitle, TEXT_YES, TEXT_NO );

					//#ifndef LG_C1100
						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
					//#endif

					( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( defaultOption );
					( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				break;

				case SCREEN_SPLASH_NANO:
					nextScreen = new BasicSplashNano( SCREEN_SPLASH_NICK,
					//#if SCREEN_SIZE == "SMALL"
//# 						BasicSplashNano.SCREEN_SIZE_SMALL,
					//#else
						BasicSplashNano.SCREEN_SIZE_MEDIUM,
					//#endif
					PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_SPLASH );
					patternBkg = false;
					bgColor = 0x000000;
				break;

				case SCREEN_SPLASH_NICK:
					nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_SPLASH + "nick.png", TEXT_SPLASH_NICK );
					patternBkg = false;
					bgColor = 0x000000;
				break;

				case SCREEN_SPLASH_GAME:
					nextScreen = new SplashGame( FONT_DEFAULT );
				break;

				case SCREEN_MAIN_MENU:
					playScreen = null;

					nextScreen = new BasicMenu( midlet, index, FONT_DEFAULT, new int[] {
						TEXT_NEW_GAME,
						TEXT_OPTIONS,
						TEXT_HIGH_SCORES,
						TEXT_HELP,
						TEXT_CREDITS,
						TEXT_EXIT,
					} );
					( ( Menu ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_AFTER_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_EXIT );
				break;

				case SCREEN_OPTIONS:
					if ( MediaPlayer.isVibrationSupported() ) {
						nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT,
								new int[] {	TEXT_TURN_SOUND_OFF, TEXT_TURN_VIBRATION_OFF, TEXT_LANGUAGE, TEXT_BACK }, OPTIONS_MENU_VIB_INDEX_BACK,
								OPTIONS_MENU_VIB_INDEX_SOUND, TEXT_TURN_SOUND_ON, OPTIONS_MENU_VIB_INDEX_VIBRATION, TEXT_TURN_VIBRATION_ON );
					} else {
						nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT,
								new int[] { TEXT_TURN_SOUND_OFF, TEXT_LANGUAGE, TEXT_BACK }, OPTIONS_MENU_NO_VIB_INDEX_BACK,
								OPTIONS_MENU_NO_VIB_INDEX_SOUND, TEXT_TURN_SOUND_ON, -1, TEXT_TURN_VIBRATION_ON  );
					}


					( ( Menu ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_AFTER_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_CREDITS:
					nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, getText( TEXT_CREDITS_TEXT ) + getMIDletVersion(), true );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_HELP:
					nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, FONT_DEFAULT, getText( TEXT_HELP_TEXT ) + getMIDletVersion(), false );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_NEW_GAME:
					playScreen.prepare( 0 );
				case SCREEN_CONTINUE_GAME:
					nextScreen = playScreen;
					playScreen.unpause();
					bgColor = -1;
					patternBkg = false;
				break;

				case SCREEN_PAUSE:
					if ( MediaPlayer.isVibrationSupported() ) {
						nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT,
							new int[] {
								TEXT_CONTINUE,
								TEXT_TURN_SOUND_OFF,
								TEXT_TURN_VIBRATION_OFF,
								TEXT_LANGUAGE,

								//#if DEMO == "false"
									TEXT_MAIN_MENU,
								//#endif

								TEXT_EXIT },
							PAUSE_MENU_VIB_INDEX_CONTINUE, PAUSE_MENU_VIB_INDEX_SOUND,
							TEXT_TURN_SOUND_ON, PAUSE_MENU_VIB_INDEX_VIBRATION, TEXT_TURN_VIBRATION_ON );
					} else {
						nextScreen = new BasicOptionsScreen( midlet, index, FONT_DEFAULT,
							new int[] { 
								TEXT_CONTINUE,
								TEXT_TURN_SOUND_OFF,
								TEXT_LANGUAGE,

								//#if DEMO == "false"
									TEXT_MAIN_MENU,
								//#endif

								TEXT_EXIT },
							PAUSE_MENU_NO_VIB_INDEX_CONTINUE, PAUSE_MENU_NO_VIB_INDEX_SOUND,
							TEXT_TURN_SOUND_ON, -1, TEXT_TURN_VIBRATION_ON );
					}

					//#ifndef LG_C1100
						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
					//#endif

					( ( Menu ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_AFTER_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_CONTINUE );
				break;

				case SCREEN_CHOOSE_CHARACTER:
					nextScreen = new ChooseCharacterScreen( midlet, index );
					//#ifndef LG_C1100
						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
					//#endif

					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_HIGH_SCORES:
					playScreen = null;

					nextScreen = HighScoresScreen.createInstance( FONT_DEFAULT );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_ERROR:
					nextScreen = new BasicTextScreen( index, FONT_DEFAULT, TEXT_LAST_ERROR, false );
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_LANGUAGE_OPTIONS:
				case SCREEN_LANGUAGE_PAUSE:
					nextScreen = new LanguageScreen( midlet, index, FONT_DEFAULT );

					//#ifndef LG_C1100
						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_OK );
					//#endif

					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
				break;

				case SCREEN_LOADING:
					updateBkg = false;
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_CANCEL );

					nextScreen = new LoadScreen( FONT_DEFAULT );
				break;

				//#if DEMO == "true"
//# 					case SCREEN_BUY_FULL_VERSION:
//# 						nextScreen = new BasicTextScreen( SCREEN_EXIT, getDefaultFont(), TEXT_BUY_FULL_VERSION, false ) {
//# 							public final void keyPressed( int key ) {
//# 								switch ( key ) {
//# 									case ScreenManager.KEY_SOFT_MID:
//# 									case ScreenManager.KEY_SOFT_LEFT:
//# 										setTextSpeed( 0 );
//# 
//# 										try {
//# 											GameMIDlet.getInstance().platformRequest( DOWNLOAD_URL );
//# 											GameMIDlet.exit();
//# 										} catch ( Exception e ) {
											//#if DEMO == "true"
//# 												e.printStackTrace();
											//#endif
//# 										}
//# 									return;
//# 
//# 									case ScreenManager.KEY_SOFT_RIGHT:
//# 									case ScreenManager.KEY_BACK:
//# 									case ScreenManager.KEY_CLEAR:
//# 									case ScreenManager.KEY_NUM5:
//# 									case ScreenManager.FIRE:
//# 										exit();
//# 									break;
//# 
//# 									default:
//# 										super.keyPressed( key );
//# 								} // fim switch ( key )
//# 							} // fim do m�todo keyPressed( int )
//# 						};
//# 
//# 						setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_EXIT );
//# 						setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_YES );
//# 					break;
				//#endif
			} // fim switch ( index )

			setBackground( patternBkg, updateBkg, bgColor );
			//#if LOW_JAR == "true"
//# 				manager.setCurrentScreen( nextScreen );
			//#else
			if ( isLowMemory() )
				manager.setCurrentScreen( nextScreen );
			else
				transition.setNextScreen( nextScreen );
			//#endif

			return index;
		} catch ( Exception e ) {
			if ( index != SCREEN_ERROR ) {
				texts[ TEXT_LAST_ERROR ] += new String( e.toString() + "\n" + e.getClass() + "\n" + e.getMessage() ).toUpperCase();

				return changeScreen( SCREEN_ERROR );
			}

			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif

			return -1;
		}
	} // fim do m�todo changeScreen( int )
	
	
	public final void onChoose( Menu menu, int id, int index ) {
		if ( id == currentScreen ) {
			switch ( id ) {
				case SCREEN_CHOOSE_SOUND:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
						case BasicConfirmScreen.INDEX_NO:
							MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
						default:
							setScreen( SCREEN_SPLASH_NANO );
					}				
				break;			
				
				case SCREEN_MAIN_MENU:
					switch ( index ) {
						case MAIN_MENU_INDEX_NEW_GAME:
							setScreen( SCREEN_CHOOSE_CHARACTER );
						break;

						case MAIN_MENU_INDEX_OPTIONS:
							setScreen( SCREEN_OPTIONS );
						break;
						
						case MAIN_MENU_INDEX_HIGH_SCORES:
							setScreen( SCREEN_HIGH_SCORES );
						break;						

						case MAIN_MENU_INDEX_HELP:
							setScreen( SCREEN_HELP );
						break;

						case MAIN_MENU_INDEX_CREDITS:
							setScreen( SCREEN_CREDITS );
						break;

						case MAIN_MENU_INDEX_EXIT:
							exit();
						break;
					}
				break;

				case SCREEN_OPTIONS:
					if ( MediaPlayer.isVibrationSupported() ) {
						switch ( index ) {
							case OPTIONS_MENU_VIB_INDEX_LANGUAGE:
								setScreen( SCREEN_LANGUAGE_OPTIONS );
							break;

							case OPTIONS_MENU_VIB_INDEX_BACK:
								MediaPlayer.saveOptions();
								setScreen( SCREEN_MAIN_MENU );
							break;
						}				
					} else {
						switch ( index ) {
							case OPTIONS_MENU_NO_VIB_INDEX_LANGUAGE:
								setScreen( SCREEN_LANGUAGE_OPTIONS );
							break;

							case OPTIONS_MENU_NO_VIB_INDEX_BACK:
								MediaPlayer.saveOptions();
								setScreen( SCREEN_MAIN_MENU );
							break;
						}				
					}
				break;
				
				case SCREEN_PAUSE:
					if ( MediaPlayer.isVibrationSupported() ) {
						switch ( index ) {
							case PAUSE_MENU_VIB_INDEX_CONTINUE:
								MediaPlayer.saveOptions();
								setScreen( SCREEN_CONTINUE_GAME );
							break;

							//#if DEMO == "false"
								case PAUSE_MENU_VIB_INDEX_MENU:
									setScreen( SCREEN_CONFIRM_MENU );
								break;
							//#endif

							case PAUSE_MENU_VIB_INDEX_LANGUAGE:
								setScreen( SCREEN_LANGUAGE_PAUSE );
							break;

							case PAUSE_MENU_VIB_INDEX_EXIT:
								setScreen( SCREEN_CONFIRM_EXIT );
							break;
						}
					} else {
						switch ( index ) {
							case PAUSE_MENU_NO_VIB_INDEX_CONTINUE:
								MediaPlayer.saveOptions();
								setScreen( SCREEN_CONTINUE_GAME );
							break;

							//#if DEMO == "false"
								case PAUSE_MENU_NO_VIB_INDEX_MENU:
									setScreen( SCREEN_CONFIRM_MENU );
								break;
							//#endif

							case PAUSE_MENU_NO_VIB_INDEX_LANGUAGE:
								setScreen( SCREEN_LANGUAGE_PAUSE );
							break;

							case PAUSE_MENU_NO_VIB_INDEX_EXIT:
								setScreen( SCREEN_CONFIRM_EXIT );
							break;
						}
					}
				break;
				
				case SCREEN_CONFIRM_MENU:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
							MediaPlayer.saveOptions();
							playScreen.backToMainMenu();
						break;
						
						default:
							setScreen( SCREEN_PAUSE );
					}				
				break;
				
				case SCREEN_CONFIRM_EXIT:
					switch ( index ) {
						case BasicConfirmScreen.INDEX_YES:
							HighScoresScreen.setScore( playScreen.getScore() );
							MediaPlayer.saveOptions();
							exit();
						break;
						
						default:
							setScreen( SCREEN_PAUSE );
					}				
				break;				
				
				case SCREEN_CHOOSE_CHARACTER:
					switch( index ) {
						case ChooseCharacterScreen.INDEX_CHAR_BOB:
							character = Character.SPONGEBOB;
						break;
						
						case ChooseCharacterScreen.INDEX_CHAR_PATRICK:
							character = Character.PATRICK;
						break;
						
						case ChooseCharacterScreen.INDEX_CHAR_SQUIDWARD:
							character = Character.SQUIDWARD;
						break;
						
						default:
							return;
					}
					setScreen( SCREEN_LOADING );
				break;
				
				case SCREEN_LANGUAGE_OPTIONS:
				case SCREEN_LANGUAGE_PAUSE:
					switch ( index ) {
						case LanguageScreen.INDEX_ENGLISH:
							setLanguage( LANGUAGE_ENGLISH );
						break;
						
						case LanguageScreen.INDEX_SPANISH:
							setLanguage( LANGUAGE_SPANISH );
						break;
						
						case LanguageScreen.INDEX_PORTUGUESE:
							setLanguage( LANGUAGE_PORTUGUESE );
						break;
					} // fim switch ( index )
					
					if ( id == SCREEN_LANGUAGE_OPTIONS )
						setScreen( SCREEN_OPTIONS );
					else
						setScreen( SCREEN_PAUSE );
				break;
				
			} // fim switch ( id )
		} // fim if ( id == currentScreen )
	} // fim do m�todo onChoose( int, int )

	
	public final void onItemChanged( Menu menu, int id, int index ) {
	}
	
	
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		try {
			if ( textIndex < 0 )
				( ( GameMIDlet ) instance ).manager.setSoftKey( softKey, null );
			else
				( ( GameMIDlet ) instance ).manager.setSoftKey( softKey, new Label( FONT_DEFAULT, getText( textIndex ) ) );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable label ) {
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );
		
		if ( midlet != null && midlet.manager != null )
			midlet.manager.setSoftKey( softKey, label );
	} // fim do m�todo setSoftKesetSoftKeyyLabel( byte, Drawable )	
	
	
	/**
	 * O cursor � sempre alocado em vez de se armazenar uma refer�ncia para ele para evitar que ele "pule" de posi��o
	 * durante a troca de telas.
	 * @return
	 */
	public static final Drawable getCursor() {
		try {
			//#if SCREEN_SIZE == "SMALL"
//# 				final DrawableImage cursor = new DrawableImage(PATH_IMAGES + "icon.png" );
			//#else
				final DrawableImage cursor = new DrawableImage(PATH_IMAGES + "cursor.png" );
			//#endif
			cursor.defineReferencePixel( cursor.getSize().x + 3, cursor.getSize().y >> 1 );		
			
			return cursor;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return null;
		}
	} // fim do m�todo loadCursor()
	
	
	private static final void saveLanguage() {
		try {
			saveData( DATABASE_NAME, DATABASE_INDEX_LANGUAGE, ( GameMIDlet ) instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo saveLanguage()
	
	
	private static final void loadLanguage() {
		try {
			loadData( DATABASE_NAME, DATABASE_INDEX_LANGUAGE, ( GameMIDlet ) instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}
	
	
	public static final void setBackground( boolean usePattern, boolean update, int color ) {
		final GameMIDlet midlet = ( GameMIDlet ) getInstance();
		
		// o teste � necess�rio para evitar NullPointerException ao sair do jogo
		if ( midlet != null ) {
			//#if LOW_JAR == "true"
//# 				midlet.manager.setBackground( null, false );
//# 				midlet.manager.setBackgroundColor( color );
			//#else
		if ( usePattern && !isLowMemory() ) {
			background.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
			midlet.manager.setBackground( background, update );
			GameMIDlet.background.setAnimation( BasicAnimatedPattern.ANIMATION_RANDOM );
		} else {
			midlet.manager.setBackground( null, false );
			midlet.manager.setBackgroundColor( color );
		}
			//#endif
		}
	}

	
	public final void write( DataOutputStream output ) throws Exception {
		output.writeByte( language );
	}

	
	public final void read( DataInputStream input ) throws Exception {
		language = input.readByte();
	}
	
	
	public static final ImageFont getDefaultFont() {
		return FONT_DEFAULT;
	}
	
	
	public static final ImageFont getBigFont() {
		return FONT_BIG;
	}
	
	
	public static final boolean isLowMemory() {
		//#if LOW_JAR == "true"
//# 		return true;
		//#else
		return ( ( GameMIDlet ) instance ).lowMemory;
		//#endif
	}

	
	public static final void cancelLoading() {
		PlayScreen.cancelLoad();
		( ( GameMIDlet ) instance ).character = -1;
		Thread.yield();
	}
	
	
	public static final void loadPlayScreen( final LoadScreen loadScreen ) {
		final TimerTask loadThread = new TimerTask() {
			public final void run() {
				final GameMIDlet midlet = ( GameMIDlet ) instance;
				try {
					MediaPlayer.free();
					GameMIDlet.playScreen = null;
					System.gc();
					
					GameMIDlet.playScreen = new PlayScreen( midlet.character, loadScreen );

					if ( midlet.character >= 0 )
						setScreen( SCREEN_NEW_GAME );
					else
						setScreen( SCREEN_MAIN_MENU );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
					setScreen( SCREEN_MAIN_MENU );
				}
			}
		};
		
		final Timer loadTimer = new Timer();
		loadTimer.schedule( loadThread, 300 );
	} // fim do m�todo loadPlayScreen( LoadScreen )


	protected final void changeLanguage( byte language ) {
		switch ( language ) {
			default:
				language = DEFAULT_LANGUAGE;

			case LANGUAGE_ENGLISH:
			case LANGUAGE_PORTUGUESE:
			case LANGUAGE_SPANISH:
				try {
					loadTexts( TEXT_TOTAL, PATH_TEXTS + LANGUAGE_IDS[ language ] + ".dat" );
					saveLanguage();
				} catch ( Exception ex ) {
					//#if DEBUG == "true"
						ex.printStackTrace();
					//#endif
				}
			break;
		}
	}


}
