/**
 * BubblesTransition.java
 * �2008 Nano Games.
 *
 * Created on May 6, 2008 5:31:46 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class BubblesTransition extends UpdatableGroup implements Constants {

	private static final byte TRANSITION_APPEARING	= 0;
	private static final byte TRANSITION_SHOWN		= 1;
	private static final byte TRANSITION_HIDING		= 2;
	private static final byte TRANSITION_INACTIVE	= 3;

	/** quantidade total de drawables no grupo */
	private static final byte TOTAL_ITEMS = 3;

	/** estado atual de transi��o */
	private byte transitionState;
	
	/** Tela atual (que ser� substitu�da). */
	private Drawable previousScreen;
	
	/** Viewport da tela anterior. */
	private final Rectangle previousViewport = new Rectangle();
	
	/** Pr�xima tela a ser definida como ativa pelo ScreenManager */
	private Drawable nextScreen;
	
	/** Viewport da pr�xima tela. */
	private final Rectangle nextViewport = new Rectangle();
	
	/** Cor s�lida de preenchimento de fundo */
	private final Pattern bubbles;
	
	private final MUV bubblesSpeed;
	
	private final Mutex mutex = new Mutex();
	

	public BubblesTransition() throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// insere as bolhas
		bubbles = new Pattern( new DrawableImage( PATH_IMAGES + "bubbles.png" ) );
		bubbles.setSize( size.x, bubbles.getFill().getHeight() );
		insertDrawable( bubbles );
		
		bubblesSpeed = new MUV( -( size.y + bubbles.getHeight() ) * 1000 / TRANSITION_BUBBLES_TIME );
	}


	public synchronized final void setNextScreen( Drawable nextScreen ) {
		while ( activeDrawables > 1 )
			removeDrawable( 0 );
		
		previousScreen = ScreenManager.getInstance().getCurrentScreen();
		if ( previousScreen == this ) {
			previousScreen = null;
		} else if ( previousScreen != null ) {
			insertDrawable( previousScreen, 0 );
			previousScreen.setViewport( previousViewport );
		}
		
		this.nextScreen = nextScreen;
		nextScreen.setViewport( nextViewport );
		insertDrawable( nextScreen, 0 );
		
		ScreenManager.getInstance().setCurrentScreen( this );
		setTransitionState( TRANSITION_APPEARING );
	}


	public synchronized final void setTransitionState( byte state ) {
		mutex.acquire();
		
		switch ( state ) {
			case TRANSITION_APPEARING:
				bubbles.setPosition( 0, size.y );
				previousViewport.set( ScreenManager.viewport );
				nextViewport.set( 0, 0, 0, 0 );
			break;
			
			case TRANSITION_SHOWN:
			case TRANSITION_HIDING:
			case TRANSITION_INACTIVE:
				if ( previousScreen != null ) {
					previousScreen.setViewport( null );
					previousScreen = null;
				}
				
				nextScreen.setViewport( null );
				
				ScreenManager.getInstance().setCurrentScreen( nextScreen );
				
				nextScreen = null;
				
				while ( activeDrawables > 1 )
					removeDrawable( 0 );
			break;
		}
		
		transitionState = state;
		
		mutex.release();
	}

	
	public final void update( int delta ) {
		super.update( delta );
		
		switch ( transitionState ) {
			case TRANSITION_APPEARING:
				bubbles.move( 0, bubblesSpeed.updateInt( delta ) );
				
				final int y = bubbles.getPosY() + ( bubbles.getHeight() >> 1 );
				previousViewport.set( 0, 0, size.x, y );
				nextViewport.set( 0, y, size.x, size.y - y );
				
				if ( y <= -bubbles.getHeight() )
					setTransitionState( TRANSITION_INACTIVE );
			break;
		}		
	}
	
	
	public final void updateTransition( int delta ) {
	}


	public final byte getTransitionState() {
		return transitionState;
	}


	protected void paint( Graphics g ) {
		super.paint( g );
	}



	

}
