/*
 * LoadScreen.java
 *
 * Created on August 21, 2007, 4:47 PM
 *
 */

package screens;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class LoadScreen extends Label implements Constants {
	
	/** Creates a new instance of LoadScreen */
	public LoadScreen( ImageFont font ) throws Exception {
		super( font, null );
		
		setProgress( 0 );
		
		GameMIDlet.loadPlayScreen( this );
	}
	
	
	public final void setProgress( int percent ) {
		if ( percent < 0 )
			percent = 0;
		else if ( percent >= 100 )
			percent = 100;
		
		setText( GameMIDlet.getText( TEXT_LOADING ) + percent + '%' );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
	}


	protected final void paint( Graphics g ) {
		super.paint( g );

		try {
			Thread.sleep( 135 );
		} catch ( InterruptedException ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
		}
	}
	
	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				ScreenManager.setKeyListener( null );
				ScreenManager.setPointerListener( null );
				GameMIDlet.cancelLoading();
			break;
		}
	}
	

}
