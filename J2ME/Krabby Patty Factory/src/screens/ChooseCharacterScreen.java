/*
 * ChooseCharacterScreen.java
 *
 * Created on June 22, 2007, 5:17 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import core.Constants;


/**
 *
 * @author peter
 */
public final class ChooseCharacterScreen extends Menu implements Constants
//#if DEMO == "true"
//# 	, Updatable
//#endif

{

	public static final byte INDEX_CHAR_PATRICK		= 0;
	public static final byte INDEX_CHAR_SQUIDWARD	= 1;
	public static final byte INDEX_CHAR_BOB			= 2;
	private static final byte INDEX_TITLE			= 3;
	private static final byte INDEX_NAME			= 4;
	
	private static final byte NUMBER_OF_ITEMS = INDEX_NAME + 4;
	
	private final DrawableImage bob;
	private final DrawableImage patrick;
	private final DrawableImage squidward;
	
	private final RichLabel name;
	
	private final Sprite arrowLeft;
	private final Sprite arrowRight;
	
	// sequ�ncias de anima��o das setas
	private final byte ARROW_SEQUENCE_STOPPED = 0;
	private final byte ARROW_SEQUENCE_PRESSED = 1;

	private final RichLabel title;

	//#if DEMO == "true"
//# 		private final short BLINK_TIME = 300;
//# 		private short blinkTime;
	//#endif
	
	
	/** Creates a new instance of ChooseCharacterScreen */
	public ChooseCharacterScreen( MenuListener listener, int id ) throws Exception {
		super( listener, id, NUMBER_OF_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final Point offset = new Point( 0, ( ScreenManager.SCREEN_HEIGHT - CHOOSE_CHARACTER_DEFAULT_HEIGHT ) >> 1 );
		
		patrick = new DrawableImage( PATH_SPLASH + "patrick.png" );
		patrick.defineReferencePixel( patrick.getSize().x >> 1, 0 );
		patrick.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, SPLASH_GAME_PATRICK_OFFSET_Y );
		patrick.move( offset );
		insertDrawable( patrick );
		
		squidward = new DrawableImage( PATH_SPLASH + "lula.png" );
		squidward.defineReferencePixel( squidward.getSize().x >> 1, 0 );
		squidward.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, SPLASH_GAME_SQUIDWARD_OFFSET_Y );
		squidward.move( offset );
		insertDrawable( squidward );
		
		//#ifdef HEIGHT_176
//# 			squidward.move( 0, 14 );
		//#endif
		
		bob = new DrawableImage( PATH_SPLASH + "bob.png" );
		bob.defineReferencePixel( bob.getSize().x >> 1, 0 );
		bob.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, SPLASH_GAME_BOB_OFFSET_Y );
		bob.move( offset );
		insertDrawable( bob );
		
		//#ifdef HEIGHT_176
//# 			bob.move( 0, -6 );
		//#endif
		
		title = new RichLabel( GameMIDlet.getDefaultFont(), "", ScreenManager.SCREEN_WIDTH, null );
		title.setSize( ScreenManager.SCREEN_WIDTH, title.getSize().y );
		title.setText( TEXT_CHOOSE_CHARACTER );
		title.setSize( ScreenManager.SCREEN_WIDTH, title.getTextTotalHeight() );
		title.defineReferencePixel( title.getSize().x >> 1, title.getSize().y );
		title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, SPLASH_GAME_PATRICK_OFFSET_Y - ( MENU_ITEMS_SPACING << 1 ) );
		title.move( 0, offset.y );
		if ( title.getPosition().y < MENU_ITEMS_SPACING )
			title.setPosition( title.getPosition().x, MENU_ITEMS_SPACING );
		insertDrawable( title );
		

		name = new RichLabel( GameMIDlet.getBigFont(), "", ScreenManager.SCREEN_WIDTH, null );
		name.setSize( ScreenManager.SCREEN_WIDTH, name.getSize().y );
		name.setText( TEXT_CHOOSE_CHARACTER );
		name.setSize( ScreenManager.SCREEN_WIDTH, name.getFont().getImage().getHeight() << 1 );
		name.defineReferencePixel( name.getSize().x >> 1, name.getSize().y );
		name.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT - MENU_ITEMS_SPACING );
		insertDrawable( name );		
		
		arrowLeft = new Sprite( PATH_TUTORIAL + "right.dat", PATH_TUTORIAL + "right" );
		arrowLeft.setTransform( TRANS_MIRROR_H );
		arrowLeft.defineReferencePixel( ANCHOR_VCENTER );
		arrowLeft.setRefPixelPosition( 0, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( arrowLeft );
		
		arrowRight = new Sprite( arrowLeft );
		arrowRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		arrowRight.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( arrowRight );
		
		setCircular( true );
		setCurrentIndex( INDEX_CHAR_BOB );
	}

	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			return;

			case ScreenManager.KEY_SOFT_MID:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				//#if DEMO == "false"
					super.keyPressed( ScreenManager.FIRE );
				//#else
//# 					if ( currentIndex == INDEX_CHAR_BOB )
//# 						super.keyPressed( ScreenManager.FIRE );
				//#endif
			break;
			
			case ScreenManager.RIGHT:
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM6:
				nextIndex();
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM4:
				previousIndex();
			break;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )
	} // fim do m�todo keyPressed( int )
	
	
	public final void keyReleased( int key ) {
		arrowLeft.setSequence( ARROW_SEQUENCE_STOPPED );
		arrowRight.setSequence( ARROW_SEQUENCE_STOPPED );		
	}	
	
	
	public final void setCurrentIndex( int index ) {
		switch ( index ) {
			case INDEX_TITLE:
				if ( currentIndex > index )
					index = INDEX_CHAR_BOB;
				else
					index = 0;
			break;
			
			case INDEX_NAME:
				if ( currentIndex == 0 )
					index = INDEX_CHAR_BOB;
				else
					index = 0;
			break;
		}
		
		if ( index > INDEX_CHAR_BOB ) {
			if ( currentIndex == INDEX_CHAR_BOB + 1 )
				index = 0;
			else
				index = INDEX_CHAR_BOB;
		}
		
		super.setCurrentIndex( index );
		
		for ( int i = INDEX_CHAR_PATRICK; i <= INDEX_CHAR_BOB; ++i )
			getDrawable( i ).setVisible( i == index );
		
		switch ( currentIndex ) {
			case INDEX_CHAR_BOB:
				name.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_SPONGEBOB ) );

				//#if DEMO == "true"
//# 					title.setText( TEXT_CHOOSE_CHARACTER );
//# 					title.setSize( title.getWidth(), title.getTextTotalHeight() );
//# 					blinkTime = 0;
//# 					title.setVisible( true );
				//#endif
			break;
			
			case INDEX_CHAR_PATRICK:
				name.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_PATRICK_STAR ) );
				
				//#if DEMO == "true"
//# 					title.setText( TEXT_FULL_VERSION_ONLY );
//# 					title.setSize( title.getWidth(), title.getTextTotalHeight() );
//# 					blinkTime = BLINK_TIME;
				//#endif
			break;

			case INDEX_CHAR_SQUIDWARD:
				name.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_SQUIDWARD_TENTACLES ) );

				//#if DEMO == "true"
//# 					title.setText( TEXT_FULL_VERSION_ONLY );
//# 					title.setSize( title.getWidth(), title.getTextTotalHeight() );
//# 					blinkTime = BLINK_TIME;
				//#endif
			break;
		}
	}


	//#if DEMO == "true"
//# 		public final void update( int delta ) {
//# 			if ( blinkTime > 0 ) {
//# 				blinkTime -= delta;
//# 
//# 				if ( blinkTime <= 0 ) {
//# 					blinkTime = BLINK_TIME;
//# 					title.setVisible( !title.isVisible() );
//# 				}
//# 			}
//# 		}
	//#endif

	
	public final void previousIndex() {
		super.previousIndex();
		arrowLeft.setSequence( ARROW_SEQUENCE_PRESSED );
		arrowRight.setSequence( ARROW_SEQUENCE_STOPPED );		
	}

	
	public final void nextIndex() {
		super.nextIndex();
		arrowRight.setSequence( ARROW_SEQUENCE_PRESSED );
		arrowLeft.setSequence( ARROW_SEQUENCE_STOPPED );		
	}


	public final void onPointerPressed( int x, int y ) {
		if ( arrowLeft.contains( x, y ) ) {
			previousIndex();
		} else if ( arrowRight.contains( x, y ) ) {
			nextIndex();
		} else {
			for ( byte i = 0; i < INDEX_TITLE; ++i ) {
				final Drawable d = getDrawable( i );
				if ( d.isVisible() && d.contains( x, y ) ) {
					keyPressed( ScreenManager.FIRE );
					break;
				}
			}
		}
	}
	
	
	public final void onPointerDragged( int x, int y ) {
	}
	
	
	public final void onPointerReleased( int x, int y ) {
		keyReleased( 0 );
	}



}
