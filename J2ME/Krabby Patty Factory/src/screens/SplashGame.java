/*
 * SplashGame.java
 *
 * Created on June 18, 2007, 7:38 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Point;
import core.Constants;


/**
 *
 * @author peter
 */
public final class SplashGame extends UpdatableGroup implements Constants, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif
{
	
	// tempo m�nimo que o logo do jogo � exibido
	private final short MIN_TIME = 3000;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private int accTime;
	
	private final MarqueeLabel labelPressAnyKey;
	
	
	/** Creates a new instance of SplashGame */
	public SplashGame( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final Point offset = new Point( ( size.x - SPLASH_GAME_WIDTH ) >> 1, ( size.y - SPLASH_GAME_HEIGHT ) >> 1 );
		
		final DrawableImage patrick = new DrawableImage( PATH_SPLASH + "patrick.png" );
		patrick.setPosition( SPLASH_GAME_PATRICK_OFFSET_X, SPLASH_GAME_PATRICK_OFFSET_Y );
		patrick.move( offset );
		
		final DrawableImage squid = new DrawableImage( PATH_SPLASH + "lula.png" );
		squid.setPosition( SPLASH_GAME_SQUIDWARD_OFFSET_X, SPLASH_GAME_SQUIDWARD_OFFSET_Y );
		squid.move( offset );
		
		String pathLogo;
		switch ( GameMIDlet.getLanguage() ) {
			case GameMIDlet.LANGUAGE_PORTUGUESE:
			case GameMIDlet.LANGUAGE_SPANISH:
				pathLogo = PATH_SPLASH + "logo_1.png";
			break;
			
			case GameMIDlet.LANGUAGE_ENGLISH:
			default:
				pathLogo = PATH_SPLASH + "logo_0.png";
		}
		
		final DrawableImage logo = new DrawableImage( pathLogo );
		logo.setPosition( SPLASH_GAME_SPONGEBOB_OFFSET_X, SPLASH_GAME_SPONGEBOB_OFFSET_Y );
		logo.move( offset );
		
		final DrawableImage bob = new DrawableImage( PATH_SPLASH + "bob.png" );
		bob.setPosition( SPLASH_GAME_BOB_OFFSET_X, SPLASH_GAME_BOB_OFFSET_Y );
		bob.move( offset );
		
		final DrawableImage nick = new DrawableImage( PATH_SPLASH + "nick_small.png" );
		nick.setPosition( SPLASH_GAME_NICK_OFFSET_X, SPLASH_GAME_NICK_OFFSET_Y );
		//#ifdef HEIGHT_176
//# 			nick.move( offset.x, 0 );
		//#else
			nick.move( offset );		
		//#endif
		
		final DrawableImage title = new DrawableImage( PATH_SPLASH + "title_" + GameMIDlet.getLanguage() + ".png" );
		title.setPosition( GameMIDlet.getLanguage() == GameMIDlet.LANGUAGE_SPANISH ? SPLASH_GAME_TITLE_OFFSET_X_SPANISH : SPLASH_GAME_TITLE_OFFSET_X, SPLASH_GAME_TITLE_OFFSET_Y );
		title.move( offset );
		
		final DrawableImage nano = new DrawableImage( PATH_SPLASH + "nano_small.png" );
		nano.defineReferencePixel( 0, nano.getSize().y );
		nano.setRefPixelPosition( 0, size.y );
		
		// ordem de inser��o das imagens do Bob e do Patrick s�o diferentes na tela pequena
		//#if SCREEN_SIZE == "SMALL"
//# 			insertDrawable( bob );
//# 			insertDrawable( squid );
//# 			insertDrawable( logo );
//# 			insertDrawable( patrick );
		//#else
			insertDrawable( patrick );
			insertDrawable( squid );
			insertDrawable( logo );
			insertDrawable( bob );
		//#endif
		
		insertDrawable( nick );
		insertDrawable( title );		
		insertDrawable( nano );
		
		labelPressAnyKey = new MarqueeLabel( font, GameMIDlet.getText( TEXT_PRESS_ANY_KEY ), ScreenManager.SCREEN_WIDTH / 3, MarqueeLabel.SCROLL_MODE_LEFT );
		labelPressAnyKey.setSize( ScreenManager.SCREEN_WIDTH, labelPressAnyKey.getSize().y );
		labelPressAnyKey.setVisible( false );
		labelPressAnyKey.defineReferencePixel( 0, labelPressAnyKey.getSize().y );
		labelPressAnyKey.setTextOffset( -labelPressAnyKey.getSize().x );
		labelPressAnyKey.setRefPixelPosition( 5000, 5000 );
		insertDrawable( labelPressAnyKey );
	}

	
	public final void update( int delta ) {
		super.update( delta );
		accTime += delta;
		
		if ( accTime >= MIN_TIME ) {
			if ( !labelPressAnyKey.isVisible() ) {
				labelPressAnyKey.setRefPixelPosition( 0, ScreenManager.SCREEN_HEIGHT - MENU_ITEMS_SPACING );
				labelPressAnyKey.setTextOffset( size.x * 3 / 2 );
				labelPressAnyKey.setSpeed( ScreenManager.SCREEN_WIDTH / 3 );
				labelPressAnyKey.setVisible( true );				
			}
		}
	}

	
	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
		accTime = MIN_TIME;
		//#endif
		
		if ( accTime >= MIN_TIME )
			GameMIDlet.setScreen( SCREEN_MAIN_MENU );
	}

	
	public final void keyReleased( int key ) {
	}

	
	//#if SCREEN_SIZE != "SMALL"

	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( ScreenManager.FIRE );
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif


}
