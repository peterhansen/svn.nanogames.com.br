/*
 * MrKrabs.java
 *
 * Created on July 16, 2007, 4:06 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;
import screens.HighScoresScreen;
import screens.PlayScreen;


/**
 *
 * @author peter
 */
public final class MrKrabs extends UpdatableGroup implements Constants, KeyListener {
	
	private static final byte TOTAL_ITEMS = 9;
	
	public static final byte STATE_NONE				= -1;
	public static final byte STATE_ASK_USER			= 0;
	public static final byte STATE_INTRO			= STATE_ASK_USER + 1;
	public static final byte STATE_TRAY_TOP			= STATE_INTRO + 1;
	public static final byte STATE_TRAY_MIDDLE		= STATE_TRAY_TOP + 1;
	public static final byte STATE_TRAY_BOTTOM		= STATE_TRAY_MIDDLE + 1;
	public static final byte STATE_SATISFACTION		= STATE_TRAY_BOTTOM + 1;
	public static final byte STATE_END				= STATE_SATISFACTION + 1;
	public static final byte STATE_BEGIN_GAME		= STATE_END + 1;
	public static final byte STATE_GAME_OVER		= STATE_BEGIN_GAME + 1;
	public static final byte STATE_TRY_AGAIN		= STATE_GAME_OVER + 1;
	public static final byte STATE_RETURN_MAIN_MENU	= STATE_TRY_AGAIN + 1;
	

	// setas indicadoras do scroll do texto
	private final Sprite arrowUp;
	private final Sprite arrowDown;
	private final DrawableImage fireIcon;
	
	private byte state = STATE_NONE;
	
	private final int TEXT_MOVE_SPEED; 
	
	private int textLimit;
	
	private final PlayScreen playScreen;
	
	private final RichLabel label;
	
	private int lastSpeedModule;
	
	private final SatisfactionMeter meter;
	
	// dire��es do scroll do texto
	private static final byte DIRECTION_NONE		= 0;
	private static final byte DIRECTION_FORWARD		= 1;
	private static final byte DIRECTION_BACKWARD	= 2;
	
	private byte direction = DIRECTION_NONE;
	
	private final int TEXT_HEIGHT;
	
	/** Imagem ou sprite do Sr. Siriguejo. */
	private final Drawable krabs;
	
	
	/** Creates a new instance of MrKrabs
	 * @param playScreen
	 * @throws java.lang.Exception 
	 */
	public MrKrabs( PlayScreen playScreen ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.playScreen = playScreen;
		
		final int FONT_HEIGHT = GameMIDlet.getDefaultFont().getImage().getHeight();
		
		// Sr. Siriguejo
		if ( GameMIDlet.isLowMemory() ) {
			krabs = new DrawableImage( PATH_TUTORIAL + "krabs_3.png" );
		} else {
			krabs = new Sprite( PATH_TUTORIAL + "krabs.dat", PATH_TUTORIAL + "krabs" );
		}
		
		krabs.defineReferencePixel( krabs.getSize().x, 0 );
		insertDrawable( krabs );
		
		final int TEMP_1 = ( FONT_HEIGHT * 11 / 2 ) + MENU_ITEMS_SPACING;
		final int TEMP_2 = ScreenManager.SCREEN_HEIGHT - playScreen.tray.getBottomPosition();
		
		final int TEXT_AREA_HEIGHT = Math.min( TEMP_1, TEMP_2 );
		
		// esse valor � utilizado para que o scroll pare ao mostrar todo o texto
		TEXT_HEIGHT = TEXT_AREA_HEIGHT - FONT_HEIGHT;
		
		setSize( ScreenManager.SCREEN_WIDTH, TEXT_AREA_HEIGHT + krabs.getSize().y );
		defineReferencePixel( 0, size.y );
		setRefPixelPosition( 0, ScreenManager.SCREEN_HEIGHT );
		
		krabs.setRefPixelPosition( size.x, 0 );
		
		// barra separando o Sr. Siriguejo do texto
		final DrawableImage barImage = new DrawableImage( PATH_TUTORIAL + "bar.png" );
		final Pattern barTop = new Pattern( barImage );
		barTop.setSize( size.x, barImage.getSize().y );
		barTop.defineReferencePixel( 0, barTop.getSize().y );
		barTop.setRefPixelPosition( 0, krabs.getSize().y );
		insertDrawable( barTop );
		
		// �rea preta sob o texto
		final Pattern pattern = new Pattern( null );
		insertDrawable( pattern );
		pattern.setFillColor( 0x000000 );
		pattern.setPosition( 0, krabs.getSize().y );
		pattern.setSize( size.x, TEXT_AREA_HEIGHT );
		
		// caracteres especiais a serem utilizados na fonte
		meter = playScreen.satisfactionMeter;
		
		arrowUp = new Sprite( PATH_TUTORIAL + "arrow.dat", PATH_TUTORIAL + "arrow" );
		arrowDown = new Sprite( arrowUp );
		arrowDown.setTransform( TRANS_MIRROR_V );
		
		fireIcon = new DrawableImage( PATH_TUTORIAL + "ok.png" );
		
		final Drawable[] specialChars = new Drawable[] { meter };
		
		// texto
		label = new RichLabel( GameMIDlet.getDefaultFont(), "", size.x, specialChars );
		label.setSize( size.x, TEXT_AREA_HEIGHT - arrowUp.getSize().y - barTop.getSize().y );
		label.setPosition( 0, krabs.getSize().y );
		insertDrawable( label );
		
		// barra separando o texto dos bot�es de scroll
		final Pattern barBottom = new Pattern( barImage );
		barBottom.setSize( size.x, barImage.getSize().y );
		barBottom.defineReferencePixel( 0, barBottom.getSize().y );
		barBottom.setRefPixelPosition( 0, label.getPosition().y + label.getSize().y );
		insertDrawable( barBottom );		

		TEXT_MOVE_SPEED = FONT_HEIGHT * 9 / 2;

		setState( STATE_NONE );
	}

	
	public final void keyPressed( int key ) {
		if ( state == STATE_NONE )
			return;
		
		byte tray = -1;
		
		switch ( key ) {
			case ScreenManager.LEFT:
			case ScreenManager.KEY_SOFT_LEFT:
				switch ( state ) {
					case STATE_ASK_USER:
						// inicia o tutorial
						setState( STATE_INTRO );
					return;

					case STATE_TRY_AGAIN:
						setState( STATE_BEGIN_GAME );
					return;
				} // fim switch ( state )
			// n�o tem break, para que possa ser usado como scroll nas outras telas
			
			case ScreenManager.UP:
				// no tutorial, deve-se diferenciar se o UP veio das teclas direcionais ou da tecla 2
				direction = DIRECTION_BACKWARD;
				updateArrows();
			return;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				switch ( state ) {
					case STATE_ASK_USER:
						// pula o tutorial
						setState( STATE_BEGIN_GAME );
					return;

					case STATE_TRY_AGAIN:
						//#if DEMO == "false"
							setState( STATE_RETURN_MAIN_MENU );
						//#else
//# 							GameMIDlet.exit();
						//#endif
					return;
				}
			// n�o tem break, para que possa ser usado como scroll nas outras telas
			
			case ScreenManager.DOWN:
				// no tutorial, deve-se diferenciar se o DOWN veio das teclas direcionais ou da tecla 8
				direction = DIRECTION_FORWARD;
				updateArrows();
			return;

			case ScreenManager.KEY_SOFT_MID:
				// no tutorial, deve-se diferenciar se o FIRE veio das teclas direcionais ou da tecla 5
				switch ( state ) {
					case STATE_INTRO:
					case STATE_SATISFACTION:
					case STATE_END:
						if ( isVisible() && label.getTextOffset() <= textLimit )
							setState( state + 1 );
					return;

					case STATE_ASK_USER:
						// pula o tutorial
						setState( STATE_BEGIN_GAME );									
					return;

					case STATE_GAME_OVER:
					case STATE_TRY_AGAIN:
						keyPressed( ScreenManager.KEY_NUM5 );
					return;
				}
			break;
		
			case ScreenManager.FIRE:
			case ScreenManager.KEY_STAR:
			case ScreenManager.KEY_POUND:
			case ScreenManager.KEY_NUM0:
				// pula o tutorial
				switch ( state ) {
					case STATE_TRAY_TOP:
					case STATE_TRAY_MIDDLE:
					case STATE_TRAY_BOTTOM:
						if ( isVisible() )
							setState( STATE_BEGIN_GAME );
					break;
					
					case STATE_INTRO:
					case STATE_SATISFACTION:
					case STATE_END:
						if ( isVisible() ) {
							if ( label.getTextOffset() <= textLimit ) {
								keyPressed( ScreenManager.KEY_SOFT_MID );
								return;
							}
							setState( STATE_BEGIN_GAME );
						}
					return;
					
					case STATE_ASK_USER:
						// pula o tutorial
						setState( STATE_BEGIN_GAME );									
					return;
					
					case STATE_GAME_OVER:
					case STATE_TRY_AGAIN:
						keyPressed( ScreenManager.KEY_NUM5 );
					return;					
				}
			break;			
			
			case ScreenManager.KEY_NUM2:
				if ( state == STATE_TRAY_TOP )
					tray = TRAY_TOP;
			break;

			case ScreenManager.KEY_NUM5:
				switch ( state ) {
					case STATE_TRAY_MIDDLE:
						tray = TRAY_MIDDLE;
					break;

					case STATE_ASK_USER:
						// inicia o tutorial
						setState( STATE_INTRO );
					return;

					case STATE_GAME_OVER:
						//#if DEMO == "false"
							setState( STATE_TRY_AGAIN );
						//#else
//# 							GameMIDlet.setScreen( SCREEN_BUY_FULL_VERSION );
						//#endif
					return;

					case STATE_TRY_AGAIN:
						//#if DEMO == "false"
							setState( STATE_BEGIN_GAME );
						//#else
//# 							GameMIDlet.exit();
						//#endif
					return;											
				}
			break;

			case ScreenManager.KEY_NUM8:
				if ( state == STATE_TRAY_BOTTOM )
					tray = TRAY_BOTTOM;
			break;
		} // fim switch ( key )

		// jogador deve pressionar a tecla correta somente quando a esteira parar, indicando o
		// ponto certo da mira
		if ( tray >= 0 && !playScreen.tray.isActive() ) {
			playScreen.hit( tray );
			setVisible( false );
			setState( state + 1 );
		}
		else
			setWrongPlayText();			
	} // fim do m�todo keyPressed( int )

	
	public synchronized final void keyReleased( int key ) {
		switch ( direction ) {
			case DIRECTION_BACKWARD:
			case DIRECTION_FORWARD:
				// TODO se tempo for curto, passa uma linha (rolagem do BlackBerry)
			break;
		}

		direction = DIRECTION_NONE;
		updateArrows();
	}
	
	
	public final void setState( int state ) {
		try {
			switch ( state ) {
				case STATE_NONE:
					setVisible( false );
					this.state = ( byte ) state;
				return;

				case STATE_ASK_USER:
					setVisible( true );

					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_ASK ) );
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_YES );
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_NO );
				break;

				case STATE_INTRO:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_INTRO_1 ) + 
								   playScreen.player.getName() + 
								   GameMIDlet.getText( TEXT_TUTORIAL_INTRO_2 ) );

					GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, arrowUp );
					GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, arrowDown );
				break;

				case STATE_TRAY_TOP:
					playScreen.tray.setActive( true );
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_TRAY_TOP ) );
				break;

				case STATE_TRAY_MIDDLE:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_TRAY_MIDDLE ) );
				break;

				case STATE_TRAY_BOTTOM:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_TRAY_BOTTOM ) );
				break;

				case STATE_SATISFACTION:
					meter.setDemoMode( true );
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_SATISFACTION ) );
				break;

				case STATE_END:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_END ) );
				break;

				case STATE_BEGIN_GAME:
					if ( state != this.state ) {
						switch ( this.state ) {
							case STATE_TRAY_TOP:
							case STATE_TRAY_MIDDLE:
							case STATE_TRAY_BOTTOM:
							case STATE_SATISFACTION:
								if ( !isVisible() ) {
									playScreen.setState( PlayScreen.STATE_END_LEVEL );
									break;
								}
							
							default:
								// submete a pontua��o e come�a um novo jogo
								HighScoresScreen.setScore( playScreen.getScore() );
								playScreen.prepare( 1 );
						}
						setVisible( false );

						break;
					}
				break;

				case STATE_GAME_OVER:
					setVisible( true );

					//#if DEMO == "false"
						// no caso de o jogo estar em espanhol, deve-se adicionar a exclama��o invertida '�' no in�cio da frase
						label.setText( ( GameMIDlet.getLanguage() == GameMIDlet.LANGUAGE_SPANISH ? "�" : "" ) +
										playScreen.player.getName() + GameMIDlet.getText( TEXT_GAME_OVER_MR_KRABS ) );
					//#else
//# 						label.setText( TEXT_DEMO_VERSION_OVER );
					//#endif

					GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, arrowUp );
					GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, arrowDown );
				break;

				case STATE_TRY_AGAIN:
					label.setText( GameMIDlet.getText( TEXT_TRY_AGAIN ) );
						
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_YES );
					GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_NO );				
				break;

				case STATE_RETURN_MAIN_MENU:
					setVisible( false );
					playScreen.backToMainMenu();
				break;

				default:
					return;
			} // fim switch ( state )

			this.state = ( byte ) state;
			direction = DIRECTION_NONE;
			label.setTextOffset( 0 );
			textLimit = -label.getTextTotalHeight() + TEXT_HEIGHT;
			if ( textLimit > 0 )
				textLimit = 0;

			updateArrows();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}

	
	private final void setWrongPlayText() {
		try {
			switch ( state ) {
				case STATE_TRAY_TOP:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_PLAY_WRONG_TOP ) );
				break;

				case STATE_TRAY_MIDDLE:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_PLAY_WRONG_MIDDLE ) );
				break;

				case STATE_TRAY_BOTTOM:
					label.setText( GameMIDlet.getText( TEXT_TUTORIAL_PLAY_WRONG_BOTTOM ) );
				break;
			}

			direction = DIRECTION_NONE;
			label.setTextOffset( 0 );
			textLimit = -label.getTextTotalHeight() + TEXT_HEIGHT;
			if ( textLimit > 0 )
				textLimit = 0;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}

	
	public final void setVisible( boolean visible ) {
		super.setVisible( visible );
		
		if ( visible ) {
			switch ( state ) {
				case STATE_ASK_USER:
				break;

				default:
					GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, arrowUp );
					GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, arrowDown );
					
					updateArrows();
//					if ( label.getTextOffset() <= textLimit )
//						GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, fireIcon );
			}
		} else {
			GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_LEFT, null );
			GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, null );
			GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
		}
	} // fim do m�todo setVisible( boolean )

	
	public final void update( int delta ) {
		if ( state != STATE_NONE ) {
			super.update( delta );

			switch ( direction ) {
				case DIRECTION_BACKWARD:
				case DIRECTION_FORWARD:
					final int ds = lastSpeedModule + ( ( direction == DIRECTION_BACKWARD ? TEXT_MOVE_SPEED : -TEXT_MOVE_SPEED ) * delta );
					final int dy = ds / 1000;
					lastSpeedModule = ds % 1000;

					if ( dy != 0 ) {
						label.setTextOffset( label.getTextOffset() + dy );

						if ( label.getTextOffset() > 0 ) {
							label.setTextOffset( 0 );
							direction = DIRECTION_NONE;
							updateArrows();
						} else {
							if ( label.getTextOffset() < textLimit ) {
								label.setTextOffset( textLimit );
								direction = DIRECTION_NONE;
								updateArrows();
							}
						}
					} // fim if ( dy != 0 )
				break;

				default:
					return;
			} // fim switch ( direction )
		}
	} // fim do m�todo update( int )

	
	private final void updateArrows() {
		if ( state != STATE_NONE ) {
			switch ( direction ) {
				case DIRECTION_BACKWARD:
					if ( label.getTextOffset() >= 0 )
						arrowUp.setSequence( SCROLL_ARROW_SEQUENCE_PRESSED );
					else
						arrowUp.setSequence( SCROLL_ARROW_SEQUENCE_RED );

					arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_YELLOW );
				break;

				case DIRECTION_FORWARD:
					if ( label.getTextOffset() <= textLimit ) {
						switch ( state ) {
							case STATE_TRAY_BOTTOM:
							case STATE_TRAY_MIDDLE:
							case STATE_TRAY_TOP:
								arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_PRESSED );
							break;

							case STATE_ASK_USER:
							case STATE_TRY_AGAIN:
							case STATE_BEGIN_GAME:
							case STATE_RETURN_MAIN_MENU:
							break;

							default:
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, fireIcon );
								arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_GREEN  );
							break;
						}
					} else {
						switch ( state ) {
							case STATE_GAME_OVER:
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, fireIcon );
							break;

							default:
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, null );
						}

						arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_RED );
					}

					arrowUp.setSequence( SCROLL_ARROW_SEQUENCE_YELLOW  );				
				break;

				default:
					if ( label.getTextOffset() >= 0 )
						arrowUp.setSequence( SCROLL_ARROW_SEQUENCE_PRESSED );
					else
						arrowUp.setSequence( SCROLL_ARROW_SEQUENCE_YELLOW );

					if ( label.getTextOffset() <= textLimit ) {
						switch ( state ) {
							case STATE_TRAY_BOTTOM:
							case STATE_TRAY_MIDDLE:
							case STATE_TRAY_TOP:
								arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_PRESSED );

							case STATE_ASK_USER:
							case STATE_TRY_AGAIN:
							case STATE_RETURN_MAIN_MENU:
							case STATE_BEGIN_GAME:
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, null );
							break;

							default:
								arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_GREEN );
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, fireIcon );
							break;
						}
					} else {
						switch ( state ) {
							case STATE_GAME_OVER:
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, fireIcon );
							break;

							default:
								GameMIDlet.setSoftKey( ScreenManager.SOFT_KEY_MID, null );
						}

						arrowDown.setSequence( SCROLL_ARROW_SEQUENCE_YELLOW );
					}
				break;
			} // fim switch ( direction )
		} // fim if ( state != STATE_NONE )
	} // fim do m�todo updateArrows()


	public final boolean contains( int x, int y ) {
		return ( super.contains( x, y ) && krabs.contains( x, y - getPosY() ) );
	}
}
