/*
 * Character.java
 *
 * Created on May 3, 2007, 8:11 PM
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;
import screens.PlayScreen;


public final class Character extends UpdatableGroup implements Constants, SpriteListener {
	
	public static final byte SPONGEBOB	= 0;
	public static final byte PATRICK	= 1;
	public static final byte SQUIDWARD	= 2;
	
	public final byte type;
	
	// sequ�ncias de anima��o de cada personagem
	public static final byte SEQUENCE_STOPPED	= 0;
	public static final byte SEQUENCE_BLINKING	= SEQUENCE_STOPPED + 1;
	public static final byte SEQUENCE_MISS		= SEQUENCE_BLINKING + 1;
	public static final byte SEQUENCE_GAME_OVER	= SEQUENCE_MISS + 1;
	public static final byte SEQUENCE_SCARED	= SEQUENCE_GAME_OVER + 1;
	public static final byte SEQUENCE_HAPPY		= SEQUENCE_SCARED + 1;
	public static final byte SEQUENCE_PATTY		= SEQUENCE_HAPPY + 1;
	
	public static final byte SEQUENTE_TOTAL		= SEQUENCE_PATTY + 1;
	
	private final Sprite face;
	
	private final PlayScreen playScreen;
	
	// intervalo m�ximo em milisegundos entre piscadas de olho quando o jogador est� parado
	private final short MAX_TIME_BETWEEN_BLINK = 9999;
	private int timeToNextBlink;	
	
	
	public Character( PlayScreen playScreen, byte character ) throws Exception {
		super( 3 );
		
		this.type = character;
		
		this.playScreen = playScreen;
		
		String characterPath;
		final Point faceOffset = new Point();
		short yOffset;
		short xOffset = 0;
		switch ( character ) {
			case Character.SPONGEBOB:
				characterPath = PATH_BOB;
				yOffset = BOB_BODY_OFFSET_Y;
				
				faceOffset.set( BOB_FACE_OFFSET_X, BOB_FACE_OFFSET_Y );
			break;
			
			case Character.PATRICK:
				characterPath = PATH_PATRICK;
				yOffset = PATRICK_BODY_OFFSET_Y;
				
				faceOffset.set( PATRICK_FACE_OFFSET_X, PATRICK_FACE_OFFSET_Y );
			break;
			
			case Character.SQUIDWARD:
				characterPath = PATH_SQUIDWARD;
				yOffset = SQUIDWARD_BODY_OFFSET_Y;
				xOffset = SQUIDWARD_FACE_REF_X_OFFSET;
				
				faceOffset.set( SQUIDWARD_FACE_OFFSET_X, SQUIDWARD_FACE_OFFSET_Y );
			break;
			
			default:
				//#if DEBUG == "true"
						throw new IllegalArgumentException( "invalid character: "+ character );
				//#else
//# 					throw new IllegalArgumentException();
				//#endif
		}
		
		final DrawableImage body = new DrawableImage( characterPath + "body.png" );
		insertDrawable( body );
		
		String pathDat;
		if ( GameMIDlet.isLowMemory() ) {
			pathDat = characterPath + "low.dat";
		} else {
			pathDat = characterPath + "full.dat";
		}		
		
		face = new Sprite( pathDat, characterPath + 'f' );
		face.setPosition( faceOffset );
		face.setListener( this, 0 );
		insertDrawable( face );
		
		setSize( body.getSize() );
        defineReferencePixel( ( size.x >> 1 ) + xOffset, size.y + yOffset );
	}
	
	
	public final String getName() {
		switch ( type ) {
			case SPONGEBOB:
				return GameMIDlet.getText( TEXT_SPONGEBOB );
			
			case SQUIDWARD:
				return GameMIDlet.getText( TEXT_SQUIDWARD_TENTACLES );
			
			case PATRICK:
				return GameMIDlet.getText( TEXT_PATRICK_STAR );
			
			default:
				return null;
		} // fim switch ( type )
	} // fim do m�todo getName()
	
	
	public final void setSequence( byte sequence ) {
		face.setSequence( sequence );
	}


	public final void onSequenceEnded( int id, int sequence ) {
		// n�o � necess�rio verificar o id
		switch ( sequence ) {
			case SEQUENCE_BLINKING:
			case SEQUENCE_MISS:
			case SEQUENCE_HAPPY:
			case SEQUENCE_PATTY:
				if ( playScreen.isSatisfactionAboveMin() )
					face.setSequence( SEQUENCE_STOPPED );
				else {
					if ( playScreen.isGameOver() )
						face.setSequence( SEQUENCE_GAME_OVER );
					else
						face.setSequence( SEQUENCE_SCARED );
				}
			break;
		}				
	}
	
	
	public final void update( int delta) {
		super.update( delta );
		
		switch ( face.getSequence() ) {
			case SEQUENCE_STOPPED:
				timeToNextBlink -= delta;
				
				if ( timeToNextBlink <= 0 ) {
					face.setSequence( SEQUENCE_BLINKING );
					timeToNextBlink = NanoMath.randInt( MAX_TIME_BETWEEN_BLINK );
				}
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
	
	
}
 
