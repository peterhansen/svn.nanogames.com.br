/*
 * FlyingGroup.java
 *
 * Esse � um grupo "dummy", que gerencia a transi��o dos ingredientes retirados da bandeja at� o hamb�rguer.
 *
 * Created on May 14, 2007, 5:27 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;


/**
 *
 * @author peter
 */
public final class FlyingGroup extends UpdatableGroup {
	
	// m�ximo de ingredientes "voando" da esteira para o hamb�rguer
	private static final byte MAX_FLYING_INGREDIENTS = 16;	
	
	private final PattyGroup pattyGroup;
	
	/** Creates a new instance of FlyingGroup */
	public FlyingGroup( PattyGroup pattyGroup ) throws Exception {
		super( MAX_FLYING_INGREDIENTS );
	
		if ( pattyGroup == null ) {
			//#if DEBUG == "true"
				throw new NullPointerException( "pattyGroup null" );
			//#else
//# 				throw new NullPointerException();
			//#endif
		}
		this.pattyGroup = pattyGroup;
	}

	
	public final int insertDrawable( Ingredient ingredient ) {
		final int retValue = super.insertDrawable( ingredient );
		ingredient.setDirection( Ingredient.DIRECTION_UP );	
		
		return retValue;
	}
	
	
	public final void update( int delta ) {
		for ( short i = 0; i < activeUpdatables; ) {
			final Ingredient ingredient = ( Ingredient ) updatables[ i ];
			ingredient.update( delta );
			
			if ( ingredient.getRefPixelY() < -ingredient.getSize().y ) {
				// o hamb�rguer atual pode ser nulo caso o n�mero de ingredientes na esteira seja maior que o n�mero
				// de ingredientes restantes para terminar um n�vel (sem esse tratamento, podia ocorrer NullPointerException
				// ao adicionar ingredientes no final de um n�vel)
				if ( pattyGroup.getCurrentPatty() == null || pattyGroup.getCurrentPatty().addIngredient( ingredient ) ) {
					// ingrediente saiu da tela e foi inserido no hamb�rguer atual (que passa a gerenciar a anima��o de queda)
					removeDrawable( i );
					continue;
				} // fim if ( pattyGroup.getCurrentPatty().addIngredient( ingredient ) )
				else {
					ingredient.setDirection( Ingredient.DIRECTION_NONE );
				}
			} // fim if ( ingredient.getDirection() == Ingredient.DIRECTION_UP ... ) )
			
			++i;
		} // fim for ( short i = 0; i < activeUpdatables; ++i )
	} // fim do m�todo update( int )

}
