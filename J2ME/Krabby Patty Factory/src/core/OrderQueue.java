/*
 * OrderQueue.java
 *
 * Created on May 15, 2007, 3:10 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public final class OrderQueue extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 4;
	
	// pr�ximo ingrediente
	private final Sprite ingredientIcon;
	
	private final DrawableImage screenPattern;
	
	// �ndice do pedido atual
	private byte currentOrderIndex;
	private byte currentIngredientIndex;
	
	private byte[][] orders;
	
	// dura��o em milisegundos da anima��o de transi��o dos ingredientes
	private static final short TRANSITION_TIME = 200;
	
	// resto da divis�o do �ltimo c�lculo da anima��o de transi��o dos ingredientes
	private int lastSpeedModule;
	
	private static final byte TRANSITION_NONE		= 0;
	private static final byte TRANSITION_EXITING	= 1;
	private static final byte TRANSITION_ENTERING	= 2;	
	
	private byte transitionState;
	
	// resultados poss�veis da inser��o de um ingrediente
	public static final byte RESULT_INCORRECT_INGREDIENT	= 1;
	public static final byte RESULT_CORRECT_INGREDIENT		= 2;
	public static final byte RESULT_ORDER_FINISHED			= 4;
	public static final byte RESULT_ALL_ORDERS_FINISHED		= 8;
	
	private final int BKG_COLOR = 0x333342;
	
	
	/**
	 * Creates a new instance of OrderQueue
	 */
	public OrderQueue() throws Exception {
		super( TOTAL_ITEMS );
		setSize( ORDER_INDICATOR_WIDTH, ORDER_INDICATOR_HEIGHT );
		
		final Pattern bkg = new Pattern( null );
		bkg.setSize( size );
		bkg.setFillColor( BKG_COLOR );
		insertDrawable( bkg );
		
		// carrega os �cones dos ingredientes
		final Sprite[] ingredientSprites = Ingredient.getSprites();
		final Image[] frames = new Image[ Ingredient.TOTAL_INGREDIENT_TYPES ];
		final short[] frameTimes = new short[ Ingredient.TOTAL_INGREDIENT_TYPES ];
		final byte[][] sequences = new byte[ 1 ][ Ingredient.TOTAL_INGREDIENT_TYPES ];
		for ( byte i = 0; i < Ingredient.TOTAL_INGREDIENT_TYPES; ++i ) {
			frames[ i ] = ingredientSprites[ i ].getFrameImage( Ingredient.SEQUENCE_TRAY, 0 );
			sequences[ 0 ][ i ] = i;
		}
		ingredientIcon = new Sprite( frames, sequences, frameTimes );
		ingredientIcon.defineReferencePixel( ingredientIcon.getSize().x >> 1, ingredientIcon.getSize().y >> 1 );
		ingredientIcon.setRefPixelPosition( ORDER_INDICATOR_INGREDIENT_X + ( ORDER_INDICATOR_WIDTH >> 1 ), ORDER_INDICATOR_INGREDIENT_Y );
		
		insertDrawable( ingredientIcon );
		
		screenPattern = new DrawableImage( PATH_IMAGES + "screenPattern.png" );
		insertDrawable( screenPattern );
	}
	
	
	public final void prepare( int level ) {
		orders = null;
		
		System.gc();
		
		if ( level == 0 ) {
			// prepara o tutorial
			orders = new byte[ 1 ][];
			orders[ 0 ] = new byte[] { Ingredient.INGREDIENT_BREAD_BOTTOM, Ingredient.INGREDIENT_PATTY, Ingredient.INGREDIENT_BREAD_TOP };
		} else {
			// calcula a quantidade de pedidos. A quantidade de ingredientes de cada um � calculada dentro do loop, pois �
			// semi-aleat�ria (quanto maior o n�vel, maior a probabilibade de pedidos grandes)
			final int totalOrders = MIN_ORDERS_PER_LEVEL + ( MAX_ORDERS_PER_LEVEL - MIN_ORDERS_PER_LEVEL ) * level / MAX_DIFFICULTY_LEVEL;
			orders = new byte[ totalOrders ][];

			// cria os pedidos
			final int LEVEL_ORDER_SIZE_DIFF = ( ORDER_MAX_SIZE - ORDER_MIN_SIZE ) * level / MAX_DIFFICULTY_LEVEL;
			final int LEVEL_MAX_ORDER_SIZE = ORDER_MIN_SIZE + LEVEL_ORDER_SIZE_DIFF;
			final int LEVEL_MIN_ORDER_SIZE = ( LEVEL_MAX_ORDER_SIZE >> 1 ) < ORDER_MIN_SIZE ? ORDER_MIN_SIZE : ( LEVEL_MAX_ORDER_SIZE >> 1 );
			for ( int i = 0; i < totalOrders; ++i ) {
				int orderSize = LEVEL_MIN_ORDER_SIZE + NanoMath.randInt( 1 + LEVEL_ORDER_SIZE_DIFF );
				if ( orderSize > ORDER_MAX_SIZE )
					orderSize = ORDER_MAX_SIZE;
				orders[ i ] = new byte[ orderSize ];

				// preenche os ingredientes
				final byte[] ingredients = orders[ i ];

				// primeiro ingrediente � sempre a parte inferior do p�o
				ingredients[ 0 ] = Ingredient.INGREDIENT_BREAD_BOTTOM;

				boolean hasPatty = false;
				byte nextIngredient;
				for ( int j = 1; j < orderSize - 1; ++j ) {
					// impede que os ingredientes intermedi�rios sejam a parte superior ou inferior do p�o, ou que sejam repetidos
					// (exce��o � a carne, que pode ser inserida 2 vezes seguidas)
					boolean incorrect = true;
					do {
						nextIngredient = ( byte ) NanoMath.randInt( Ingredient.TOTAL_INGREDIENT_TYPES );
						switch ( nextIngredient ) {
							case Ingredient.INGREDIENT_PATTY:
								if ( j <= 1 || nextIngredient != ingredients[ j - 1 ] || nextIngredient == ingredients[ j - 2 ] )
									incorrect = false;
							break;

							case Ingredient.INGREDIENT_BREAD_BOTTOM:
							case Ingredient.INGREDIENT_BREAD_TOP:
							break;
							
							case Ingredient.INGREDIENT_BREAD_MIDDLE:
								// evita dois p�es seguidos
								if ( j == 1 || j == orderSize - 2 )
									break;

							default:
								if ( nextIngredient != ingredients[ j - 1 ] )
									incorrect = false;
						} // fim switch ( nextIngredient )
					} while ( incorrect );

					if ( nextIngredient == Ingredient.INGREDIENT_PATTY )
						hasPatty = true;

					ingredients[ j ] = nextIngredient;
				} // fim for ( int j = 1; j < orderSize - 1; ++j )

				// garante que o pedido tenha pelo menos uma carne
				if ( !hasPatty )
					ingredients[ orderSize >> 1 ] = Ingredient.INGREDIENT_PATTY;

				// �ltimo ingrediente � sempre a parte superior do p�o
				ingredients[ orderSize - 1 ] = Ingredient.INGREDIENT_BREAD_TOP;

				//#if DEBUG == "true"
				System.out.print( "Novo pedido(" + orderSize + "): ");
				for ( int temp = 0; temp < orderSize; ++temp ) {
					System.out.print( ingredients[ temp ] + " " );
				}
				System.out.println(); 
				//#endif			
			} // fim for ( int i = 0; i < totalOrders; ++i )
		}
		
		currentOrderIndex = 0;
		currentIngredientIndex = 0;
		
		transitionState = TRANSITION_ENTERING;
		
		ingredientIcon.setFrame( orders[ currentOrderIndex ][ currentIngredientIndex ] );
		final Image img = ingredientIcon.getCurrentFrameImage();
		ingredientIcon.setSize( img.getWidth(), img.getHeight() );
		ingredientIcon.defineReferencePixel( ingredientIcon.getSize().x >> 1, ingredientIcon.getSize().y >> 1 );
	} // fim do m�todo prepare( int )
	
	
	public final int getCurrentOrderIndex() {
		return currentOrderIndex;
	}
	
	
	public final int getTotalOrders() {
		return orders.length;
	}
	
	
	public final byte getCurrentIngredient() {
		try {
			return orders[ currentOrderIndex ][ currentIngredientIndex ];
		} catch ( Exception e ) {
			return -1;
		}
	}
	
	
	/**
	 * Obt�m a quantidade de ingredientes do pedido atual.
	 * @return quantidade de ingredientes do pedido atual.
	 */
	public final int getCurrentOrderSize() {
		try {
			return orders[ currentOrderIndex ].length;
		} catch ( Exception e ) {
			return 0;
		}
	}
	
	
	public final byte[] getOrderAt( int index ) {
		try {
			return orders[ index ];
		} catch ( Exception e ) {
			return null;
		}
	}
	
	
	public final byte getCurrentIngredientIndex() {
		return currentIngredientIndex;
	}
	
	
	/**
	 * Obt�m o tipo do pr�ximo ingrediente.
	 * @return tipo do pr�ximo ingrediente (tipos definidos na classe Ingredient), ou -1 caso n�o haja mais pedidos pendentes.
	 */
	public final byte getNextIngredient() {
		if ( currentIngredientIndex + 1 < orders[ currentOrderIndex ].length ) {
			return orders[ currentOrderIndex ][ currentIngredientIndex + 1 ];	
		} else {
			// verifica se h� mais pedidos pendentes
			if ( currentOrderIndex + 1 < orders.length ) {
				return orders[ currentOrderIndex + 1 ][ 0 ];	
			} else {
				return -1;
			}
		}		
	} // fim do m�todo getNextIngredient()
	
	
	/**
	 * Avan�a para o pr�ximo ingrediente, realizando a troca dos indicadores do ingrediente atual e do pr�ximo.
	 * @return true, caso ainda haja pedidos pendentes, e false caso o �ltimo pedido tenha sido completado.
	 */
	public final byte addIngredient( byte ingredientType ) {
		byte ret = 0;
		
		if ( hasMoreOrders() ) {
			transitionState = TRANSITION_EXITING;
			lastSpeedModule = 0;

			if ( ingredientType == orders[ currentOrderIndex ][ currentIngredientIndex ] )
				ret |= RESULT_CORRECT_INGREDIENT;
			else
				ret |= RESULT_INCORRECT_INGREDIENT;		

			++currentIngredientIndex;
			if ( currentIngredientIndex >= orders[ currentOrderIndex ].length ) {
				// terminou um pedido; verifica se h� mais pedidos pendentes
				currentIngredientIndex = 0;

				transitionState = TRANSITION_EXITING;
				++currentOrderIndex;
				if ( hasMoreOrders() )
					ret |= RESULT_ORDER_FINISHED;
				else
					ret |= RESULT_ALL_ORDERS_FINISHED;
			}
		}
		
		return ret;
	} // fim do m�todo addIngredient()
	
	
	/**
	 * Indica se ainda h� pedidos a serem entregues.
	 * @return boolean indicando se ainda h� pedidos (conta o pedido atual) a entregar.
	 */
	public final boolean hasMoreOrders() {
		return currentOrderIndex < orders.length;
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		screenPattern.setVisible( !screenPattern.isVisible() );

		if ( transitionState != TRANSITION_NONE ) {
			final int ds = lastSpeedModule + ( INGREDIENT_CHANGE_SPEED * delta );
			final int dx = ds / 1000;
			lastSpeedModule = ds % 1000;
			
			ingredientIcon.move( dx, 0 );
			
			switch ( transitionState ) {
				case TRANSITION_EXITING:
					if ( ingredientIcon.getRefPixelX() <= ( -size.x >> 1 ) ) {
						// ingrediente saiu da �rea vis�vel
						if ( hasMoreOrders() ) {
							if ( currentIngredientIndex < orders[ currentOrderIndex ].length )
								ingredientIcon.setFrame( orders[ currentOrderIndex ][ currentIngredientIndex ] );	
							else
								ingredientIcon.setFrame( orders[ currentOrderIndex ][ 0 ] );
							
							final Image img = ingredientIcon.getCurrentFrameImage();
							ingredientIcon.setSize( img.getWidth(), img.getHeight() );
							ingredientIcon.defineReferencePixel( ingredientIcon.getSize().x >> 1, ingredientIcon.getSize().y >> 1 );
							
							transitionState = TRANSITION_ENTERING;
						} else {
							transitionState = TRANSITION_NONE;
						}
						ingredientIcon.setRefPixelPosition( size.x * 3 / 2, ingredientIcon.getRefPixelY() );
					} // fim if ( ingredientIcon.getRefPixelX() <= ( -size.x >> 1 ) )
				break;

				case TRANSITION_ENTERING:
					if ( ingredientIcon.getRefPixelX() <= ( size.x >> 1 ) ) {
						ingredientIcon.setRefPixelPosition( size.x >> 1, ingredientIcon.getRefPixelY() );
						
						transitionState = TRANSITION_NONE;
					}
				break;
			} // fim switch ( transitionState )
		} // fim if ( transitionState != TRANSITION_NONE )
	} // fim do m�todo update( int )

    
    final byte[] getCurrentevelIngredients() {
        int quantity = 0;
        for ( int i = 0; i < orders.length; ++i ) {
            quantity += orders[ i ].length;
        }
        
        final byte[] ingredientTypes = new byte[ quantity ];
        for ( int i = 0, counter = 0; i < orders.length; ++i ) {
            for ( int j = 0; j < orders[ i ].length; ++j ) {
                ingredientTypes[ counter++ ] = orders[ i ][ j ];
            }
        }
        
        return ingredientTypes;
    }
	

}
