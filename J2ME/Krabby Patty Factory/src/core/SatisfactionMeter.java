/*
 * SatisfactionMeter.java
 *
 * Created on July 18, 2007, 2:36 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;



/**
 *
 * @author peter
 */
public final class SatisfactionMeter extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 3;
	
	
	//#if LOW_JAR == "true"
//# 	private final DrawableImage fish;
	//#else
	private final Sprite fish;
	private static final byte FISH_TOTAL_FRAMES = 4;
	//#endif
	
	private final Pattern fill;
	
	// cores de preenchimento do pattern
	private final int COLOR_MIN_GREEN = 0x66;
	private final int FILL_PATTERN_COLOR_GOOD  = 0x00ff00;
	
	// velocidade da troca de cores
	private static final int COLOR_CHANGE_SPEED = 431;
	
	private boolean colorIncreasing;
	private int lastColorModule;
	private int currentGreen;
	
	private int satisfactionShown;
	private int satisfaction;
	
	private int lastSatisfactionModule;
	
	private short minSatisfactionPercent;
	
	private boolean demoMode;
	
	private final DrawableImage bar;
	
	
	/**
	 * Creates a new instance of SatisfactionMeter
	 */
	public SatisfactionMeter() throws Exception {
		super( TOTAL_ITEMS );
		
		//#if LOW_JAR == "true"
//# 		fish = new DrawableImage( PATH_IMAGES + "fish_0.png" );
		//#else
		fish = new Sprite( PATH_IMAGES + "fish.dat", PATH_IMAGES + "fish" );
		//#endif
		
		setSize( fish.getSize() );
		defineReferencePixel( 0, size.y );
		
		fill = new Pattern( null );
		fill.setFillColor( FILL_PATTERN_COLOR_GOOD );
		
		insertDrawable( fill );
		insertDrawable( fish );
		
		bar = new DrawableImage( PATH_IMAGES + "bar.png" );
		insertDrawable( bar );
		
		setSatisfaction( 100 );
	}
	
	
	public final void setSatisfaction( int percent ) {
		satisfaction = percent;
		
		if ( satisfaction >= minSatisfactionPercent )
			fill.setFillColor( FILL_PATTERN_COLOR_GOOD );
	}

	
	public final void update( int delta ) {
		if ( satisfaction != satisfactionShown ) {
			final int ds = lastSatisfactionModule + 
					( ( satisfaction > satisfactionShown ? SATISFACTION_CHANGE_SPEED : -SATISFACTION_CHANGE_SPEED ) * delta );
			final int dx = ds / 1000;
			lastSatisfactionModule = ds % 1000;
			
			if ( dx != 0 ) {
				satisfactionShown += dx;
				if ( dx > 0 ) {
					if ( satisfactionShown > satisfaction )
						satisfactionShown = satisfaction;
				} else {
					if ( satisfactionShown < satisfaction )
						satisfactionShown = satisfaction;					
				}
				
				if ( demoMode && satisfactionShown == satisfaction )
					satisfaction = 100 - satisfaction;
				
				fill.setSize( SATISFACTION_METER_FILL_WIDTH, SATISFACTION_METER_FILL_HEIGHT * satisfactionShown / 100 );
				fill.defineReferencePixel( 0, fill.getSize().y );
				fill.setRefPixelPosition( SATISFACTION_METER_FILL_X, SATISFACTION_METER_FILL_Y );

				//#if LOW_JAR == "false"
				// s� troca os frames do peixe na vers�o completa 
				if ( !GameMIDlet.isLowMemory() ) {
					if ( satisfactionShown >= 80 ) {
						fish.setFrame( 0 );
					} else if ( satisfactionShown >= 55 ) {
						fish.setFrame( 1 );
					} else if ( satisfactionShown >= 25 ) {
						fish.setFrame( 2 );
					} else {
						fish.setFrame( 3 );
					}
				}
				//#endif
			} // fim if ( dx != 0 )
		} // fim if ( satisfaction != satisfactionShown )

		if ( satisfactionShown < minSatisfactionPercent ) {
			final int dc = lastColorModule + COLOR_CHANGE_SPEED * delta;
			final int dColor = dc / 1000;
			lastColorModule = dc % 1000;

			if ( dColor != 0 ) {
				if ( colorIncreasing ) {
					currentGreen += dColor;

					if ( currentGreen >= 0xff ) {
						currentGreen = 0xff;
						colorIncreasing = false;
					}
				} else {
					currentGreen -= dColor;

					if ( currentGreen <= COLOR_MIN_GREEN ) {
						currentGreen = COLOR_MIN_GREEN;
						colorIncreasing = true;
					}
				}
				fill.setFillColor( 0xff0000 | ( currentGreen << 8 ) );
			} // fim if ( dColor != 0 )
		} else {
			fill.setFillColor( FILL_PATTERN_COLOR_GOOD );
		}		
	} // fim do m�todo update( int )
	
	
	public final void setDemoMode( boolean demoMode ) {
		this.demoMode = demoMode;
		if ( demoMode ) {
			satisfaction = 100;
			satisfactionShown = 0;
			setMinSatisfaction( 50 );
		}
	}

	
	public final void setMinSatisfaction( int minSatisfactionPercent ) {
		this.minSatisfactionPercent = ( short ) minSatisfactionPercent;
		
		bar.setPosition( 0, SATISFACTION_METER_FILL_HEIGHT * ( 100 - minSatisfactionPercent ) / 100 );
	}
	
	
}
