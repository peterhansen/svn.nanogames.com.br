/*
 * Constants.java
 *
 * Created on 9 de Maio de 2007, 10:52
 *
 */

package core;

/**
 *
 * @author Peter Hansen
 */
public interface Constants {
	
	//////////////////////////////////////////////
	// DEFINI��ES INDEPENDENTES DO TAMANHO DE TELA
    
    // caminhos dos recursos
	public static String PATH_IMAGES			= "/";
	public static String PATH_TUTORIAL			= PATH_IMAGES + "tutorial/";
	public static String PATH_INGREDIENTS		= PATH_IMAGES + "ingredients/";
	public static String PATH_SPLASH			= PATH_IMAGES + "splash/";
	public static String PATH_BOB				= PATH_IMAGES + "bob/";
	public static String PATH_PATRICK			= PATH_IMAGES + "patrick/";
	public static String PATH_SQUIDWARD			= PATH_IMAGES + "lula/";
    public static String PATH_SOUNDS			= "/";
	public static String PATH_TEXTS				= "/";
	
	// cor padr�o de fundo da tela
	public static final int BACKGROUND_COLOR = 0x002b6a;
	
	// dura��o da anima��o de transi��o das bolhas
	public static final short TRANSITION_BUBBLES_TIME = 733;

	//#if DEMO == "true"
//# 		public static String DOWNLOAD_URL = "http://wap.nick.playp.biz/checkout.aspx?ItemId=43030883";
	//#endif
	
	// �ndices dos sons e m�sicas
	public static final byte SOUND_SPLASH		= 0;
	public static final byte SOUND_NEXT_LEVEL	= 1;
	public static final byte SOUND_MULTIPLIER	= 2;
	public static final byte SOUND_GAME_OVER	= 3;
	public static final byte SOUND_BAD_MOVE		= 4;
	
	
	public static String DATABASE_NAME			= "N";
	public static final byte DATABASE_INDEX_MEDIA		= 1;
	public static final byte DATABASE_INDEX_LANGUAGE	= 2;
	public static final byte DATABASE_INDEX_SCORES		= 3;
	
	/** quantidade total de slots da base de dados */
	public static final byte DATABASE_TOTAL				= 3;
    
    // �ltimo n�vel do jogo considerado para c�lculos da dificuldade
    public static final short MAX_DIFFICULTY_LEVEL = 12;    
	
	//#ifdef NO_SOUND
//# 		// essa vers�o � utilizada no Samsung C420, que trata de maneira incorreta o tempo de vibra��o (� muito mais longo
//# 		// do que o tempo passado como par�metro)
//# 		public static final short DEFAULT_VIBRATION_TIME = 100;
	//#else
		public static final short DEFAULT_VIBRATION_TIME = 370;
	//#endif

	// n�mero m�ximo e m�nimo de ingredientes por pedido
	//#if DEMO == "false"
		public static final byte ORDER_MIN_SIZE = 3;
		public static final byte ORDER_MAX_SIZE = 16;
	//#else
//# 		public static final byte ORDER_MIN_SIZE = 5;
//# 		public static final byte ORDER_MAX_SIZE = 20;
	//#endif
	
	// n�mero m�ximo e m�nimo de pedidos por n�vel
	//#if DEMO == "false"
		public static final byte MIN_ORDERS_PER_LEVEL = 4;
	//#else
//# 		public static final byte MIN_ORDERS_PER_LEVEL = 6;
	//#endif
	public static final byte MAX_ORDERS_PER_LEVEL = 8;
	
	
	// �ndices das esteiras
	public static final byte TRAY_TOP		= 0;
	public static final byte TRAY_MIDDLE	= 1;
	public static final byte TRAY_BOTTOM	= 2;
	
	public static final byte TRAY_TOTAL		= 3;
	
	// �ndices de cada parte da mira no array
	public static final byte TRAY_TARGET_INDEX_LEFT		= 0;
	public static final byte TRAY_TARGET_INDEX_CENTER	= 1;
	public static final byte TRAY_TARGET_INDEX_RIGHT	= 2;
	
	
	// pontua��o m�xima obtida ao inserir um ingrediente
	public static final short MAX_SCORE_PER_INGREDIENT = 100;	
	

	// n�veis m�nimos de precis�o para cada faixa de pontua��o por ingrediente
	public static final byte PRECISION_PERFECT	= 95;
	public static final byte PRECISION_GREAT	= 87;
	public static final byte PRECISION_GOOD		= 80;
	public static final byte PRECISION_BAD		= 0;	
	
	
	// telas do jogo
	public static final byte SCREEN_CHOOSE_SOUND		= 0;
	public static final byte SCREEN_SPLASH_NANO			= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_NICK			= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME			= SCREEN_SPLASH_NICK + 1;
	public static final byte SCREEN_MAIN_MENU			= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_OPTIONS				= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CREDITS				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP				= SCREEN_CREDITS + 1;
	public static final byte SCREEN_NEW_GAME			= SCREEN_HELP + 1;
	public static final byte SCREEN_CONTINUE_GAME		= SCREEN_NEW_GAME + 1;
	public static final byte SCREEN_PAUSE				= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_CONFIRM_MENU		= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT		= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_CHOOSE_CHARACTER	= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_HIGH_SCORES			= SCREEN_CHOOSE_CHARACTER + 1;
	public static final byte SCREEN_ERROR				= SCREEN_HIGH_SCORES + 1;
	public static final byte SCREEN_LANGUAGE_OPTIONS	= SCREEN_ERROR + 1;
	public static final byte SCREEN_LANGUAGE_PAUSE		= SCREEN_LANGUAGE_OPTIONS + 1;
	public static final byte SCREEN_LOADING				= SCREEN_LANGUAGE_PAUSE + 1;
	public static final byte SCREEN_BUY_FULL_VERSION	= SCREEN_LOADING + 1;
	public static final byte SCREEN_EXIT				= SCREEN_BUY_FULL_VERSION + 1;

	
	// defini��es dos textos
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= 1;
	public static final byte TEXT_NEW_GAME						= 2;
	public static final byte TEXT_EXIT							= 3;
	public static final byte TEXT_OPTIONS						= 4;
	public static final byte TEXT_PAUSE							= 5;
	public static final byte TEXT_CREDITS						= 6;
	public static final byte TEXT_CREDITS_TEXT					= 7;
	public static final byte TEXT_HELP							= 8;
	public static final byte TEXT_HELP_TEXT						= 9;
	public static final byte TEXT_PERFECT						= 10;
	public static final byte TEXT_GREAT							= 11;
	public static final byte TEXT_GOOD							= 12;
	public static final byte TEXT_BAD							= 13;
	public static final byte TEXT_SCORE							= 14;
	public static final byte TEXT_LEVEL							= 15;
	public static final byte TEXT_TURN_SOUND_ON					= 16;
	public static final byte TEXT_TURN_SOUND_OFF				= 17;
	public static final byte TEXT_TURN_VIBRATION_ON				= 18;
	public static final byte TEXT_TURN_VIBRATION_OFF			= 19;
	public static final byte TEXT_CANCEL						= 20;
	public static final byte TEXT_SATISFACTION					= 21;
	public static final byte TEXT_CONTINUE						= 22;
	public static final byte TEXT_ENGLISH						= 23;
	public static final byte TEXT_PORTUGUESE					= 24;
	public static final byte TEXT_SPANISH						= 25;
	public static final byte TEXT_DO_YOU_WANT_SOUND				= 26;
	public static final byte TEXT_YES							= 27;
	public static final byte TEXT_NO							= 28;
	public static final byte TEXT_MAIN_MENU						= 29;
	public static final byte TEXT_CONFIRM_BACK_MENU				= 30;
	public static final byte TEXT_CONFIRM_EXIT					= 31;
	public static final byte TEXT_PRESS_ANY_KEY					= 32;
	public static final byte TEXT_CHOOSE_CHARACTER				= 33;
	public static final byte TEXT_SPONGEBOB						= 34;
	public static final byte TEXT_PATRICK_STAR					= 35;
	public static final byte TEXT_SQUIDWARD_TENTACLES			= 36;
	public static final byte TEXT_LAST_ERROR					= 37;
	public static final byte TEXT_GAME_OVER						= 38;
	public static final byte TEXT_HIGH_SCORES					= 39;
	public static final byte TEXT_JUMP							= 40;
	public static final byte TEXT_TUTORIAL_INTRO_1				= 41;
	public static final byte TEXT_TUTORIAL_INTRO_2				= 42;
	public static final byte TEXT_TUTORIAL_TRAY_TOP				= 43;
	public static final byte TEXT_TUTORIAL_TRAY_MIDDLE			= 44;
	public static final byte TEXT_TUTORIAL_TRAY_BOTTOM			= 45;
	public static final byte TEXT_TUTORIAL_PLAY_WRONG_TOP		= 46;
	public static final byte TEXT_TUTORIAL_PLAY_WRONG_MIDDLE	= 47;
	public static final byte TEXT_TUTORIAL_PLAY_WRONG_BOTTOM	= 48;
	public static final byte TEXT_TUTORIAL_END					= 49;
	public static final byte TEXT_TUTORIAL_ASK					= 50;
	public static final byte TEXT_TUTORIAL_SATISFACTION			= 51;
	public static final byte TEXT_LANGUAGE						= 52;
	public static final byte TEXT_NEW_RECORD					= 53;
	public static final byte TEXT_GAME_OVER_MR_KRABS			= 54;
	public static final byte TEXT_TRY_AGAIN						= 55;
	public static final byte TEXT_SPLASH_NANO					= 56;
	public static final byte TEXT_SPLASH_NICK					= 57;
	public static final byte TEXT_FULL_VERSION_ONLY				= 58;
	public static final byte TEXT_DEMO_VERSION_OVER				= 59;
	public static final byte TEXT_BUY_FULL_VERSION				= 60;
	public static final byte TEXT_LOADING						= 61;
	
	
	public static final byte TEXT_TOTAL							= TEXT_LOADING + 1;
	
	
	public static final byte SCROLL_ARROW_SEQUENCE_YELLOW				= 0;
	public static final byte SCROLL_ARROW_SEQUENCE_GREEN				= 1;
	public static final byte SCROLL_ARROW_SEQUENCE_PRESSED				= 2;
	public static final byte SCROLL_ARROW_SEQUENCE_RED					= 3;
	public static final byte SCROLL_ARROW_SEQUENCE_PRESS_AND_RELEASE	= 4;
	
	
	public static final byte MAIN_MENU_INDEX_NEW_GAME		= 0;
	public static final byte MAIN_MENU_INDEX_OPTIONS		= 1;
	public static final byte MAIN_MENU_INDEX_HIGH_SCORES	= 2;
	public static final byte MAIN_MENU_INDEX_HELP			= 3;
	public static final byte MAIN_MENU_INDEX_CREDITS		= 4;
	public static final byte MAIN_MENU_INDEX_EXIT			= 5;

	
	public static final byte OPTIONS_MENU_NO_VIB_INDEX_SOUND	= 0;
	public static final byte OPTIONS_MENU_NO_VIB_INDEX_LANGUAGE	= 1;
	public static final byte OPTIONS_MENU_NO_VIB_INDEX_BACK		= 2;
	
	public static final byte PAUSE_MENU_NO_VIB_INDEX_CONTINUE	= 0;
	public static final byte PAUSE_MENU_NO_VIB_INDEX_SOUND		= 1;
	public static final byte PAUSE_MENU_NO_VIB_INDEX_LANGUAGE	= 2;
	//#if DEMO == "false"
		public static final byte PAUSE_MENU_NO_VIB_INDEX_MENU		= 3;
		public static final byte PAUSE_MENU_NO_VIB_INDEX_EXIT		= 4;
	//#else
//# 		public static final byte PAUSE_MENU_NO_VIB_INDEX_EXIT		= 3;
	//#endif
	
	public static final byte OPTIONS_MENU_VIB_INDEX_SOUND		= 0;
	public static final byte OPTIONS_MENU_VIB_INDEX_VIBRATION	= 1;
	public static final byte OPTIONS_MENU_VIB_INDEX_LANGUAGE	= 2;
	public static final byte OPTIONS_MENU_VIB_INDEX_BACK		= 3;
	
	public static final byte PAUSE_MENU_VIB_INDEX_CONTINUE	= 0;
	public static final byte PAUSE_MENU_VIB_INDEX_SOUND		= 1;
	public static final byte PAUSE_MENU_VIB_INDEX_VIBRATION	= 2;
	public static final byte PAUSE_MENU_VIB_INDEX_LANGUAGE	= 3;

	//#if DEMO == "false"
		public static final byte PAUSE_MENU_VIB_INDEX_MENU		= 4;
		public static final byte PAUSE_MENU_VIB_INDEX_EXIT		= 5;
	//#else
//# 		public static final byte PAUSE_MENU_VIB_INDEX_EXIT		= 4;
	//#endif
	
	
	////////////////////////////////////////////
	// DEFINI��ES DEPENDENTES DO TAMANHO DE TELA
	
	//#if SCREEN_SIZE == "SMALL"
//# 		// defini��es de telas pequenas
//# 	
//#  		// limite m�nimo de mem�ria considerado para que comece a haver "racionamento" de mem�ria
//#  		public static final int MEMORY_LOW_LIMIT = 600000;
//#  	
//#  		// velocidade de movimenta��o do label de in�cio de n�vel em pixels por segundo
//#  		public static final byte LABEL_LEVEL_SPEED = -68;
//#  
//#  		// velocidade da anima��o do background em pixels por segundo
//#  		public static final int BACKGROUND_SPEED = 38;
//#  		
//#  		// espa�amento vertical entre os itens dos menus
//#  		public static final byte MENU_ITEMS_SPACING = 3;
//#  		
//#  		
//#  		// defini��es do indicador de satisfa��o
//#  		public static final byte SATISFACTION_METER_FILL_X		= 1;
//#  		public static final byte SATISFACTION_METER_FILL_Y		= 30;
//#  		public static final byte SATISFACTION_METER_FILL_WIDTH	= 4;
//#  		public static final byte SATISFACTION_METER_FILL_HEIGHT	= 29;
//#  		
//#  		// mudan�a do n�vel de satisfa��o em pixels por segundo
//#  		public static final byte SATISFACTION_CHANGE_SPEED = 13;	
//# 		
//#  		// defini��es da esteira
//#  		// velocidades m�xima e m�nima da esteira em pixels por segundo
//#  		public static final byte TRAY_SPEED_MIN = 45;
//#  		public static final byte TRAY_SPEED_MAX = 106;	
//#  
//#  		// largura em pixels da �rea v�lida para se atingir um ingrediente
//#  		public static final byte TRAY_HIT_AREA_WIDTH = 32;
//#  		// dist�ncia m�xima em pixels do centro do ingrediente ao centro da mira para que a jogada seja considerada perfeita
//#  		public static final byte TRAY_BULLS_EYE_OFFSET = TRAY_HIT_AREA_WIDTH / 6;
//#  
//#  		// dist�ncias m�xima e m�nima entre os slots (quanto maior a dificuldade, menor a dist�ncia)
//#  		public static final short TRAY_SLOT_DISTANCE_INITIAL_MIN	= TRAY_HIT_AREA_WIDTH + 34;
//#  		public static final short TRAY_SLOT_DISTANCE_INITIAL_MAX	= TRAY_SLOT_DISTANCE_INITIAL_MIN * 2;		
//#  		public static final short TRAY_SLOT_DISTANCE_FINAL_MIN		= TRAY_HIT_AREA_WIDTH + 27;
//#  		public static final short TRAY_SLOT_DISTANCE_FINAL_MAX		= TRAY_SLOT_DISTANCE_FINAL_MIN * 113 / 100;		
//#  
//#  		// dist�ncia do centro da mira superior ao centro da tela. As posi��es das outras miras s�o calculadas dinamicamente
//#  		// no construtor da classe Tray.
//#  		public static final short TRAY_TARGET_TOP_X_OFFSET = -34;
//#  
//#  		// dist�ncia da parte superior da mira � imagem do laser
//#  		public static final byte TRAY_AIM_TOP_OFFSET_X = -6;
//#  		public static final byte TRAY_AIM_TOP_OFFSET_Y = -5;
//#  
//#  		// dist�ncia da parte inferior da mira � imagem do laser
//#  		public static final byte TRAY_AIM_BOTTOM_OFFSET_X = 14;
//#  		public static final byte TRAY_AIM_BOTTOM_OFFSET_Y = 22;	
//#  	
//#  		// valor utilizado para calcular a velocidade de anima��o das engrenagens da esteira
//#  		public static final short TRAY_GEAR_FACTOR = 3000;
//#  		
//#  		// posi��o do indicador do pedido relativa � posi��o dos controles
//#  		public static final byte ORDER_INDICATOR_X_OFFSET = 62;
//#  		public static final byte ORDER_INDICATOR_Y_OFFSET = -10;
//#  
//#  		// posi��o relativa dos ingredientes no interior do indicador de pedidos
//#  		public static final byte ORDER_INDICATOR_INGREDIENT_X = 5;
//#  		public static final byte ORDER_INDICATOR_INGREDIENT_Y = 5;
//#  
//#  		// �rea vis�vel dos ingredientes no interior do indicador de pedidos
//#  		public static final byte ORDER_INDICATOR_WIDTH  = 13;
//#  		public static final byte ORDER_INDICATOR_HEIGHT = 7;
//#  
//#  		// velocidade de movimenta��o da esteira grande (hamb�rguer em primeiro plano), em pixels por segundo
//#  		public static final short BIG_TRAY_MOVE_SPEED = -190;
//#  
//#  		// velocidade de movimenta��o dos ingredientes durante a anima��o de troca
//#  		public static final short INGREDIENT_CHANGE_SPEED = -230;
//#  		
//#  		// velocidade do reposicionamento vertical do hamb�rguer quando ele ultrapassa seu limite
//#  		public static final byte PATTY_VERTICAL_SPEED = 12;
//#  
//#  		// acelera��o da "gravidade" em pixels por segundo ao quadrado
//#  		public static final short GRAVITY_ACCELERATION = 415;		
//# 		
//#  		// tela de sele��o de personagens
//#  		public static final short CHOOSE_CHARACTER_DEFAULT_HEIGHT = 160;
//#  
//#  
//#  		// defini��es do splash
//#  		public static final short SPLASH_GAME_WIDTH	= 128;
//#  		public static final byte SPLASH_GAME_HEIGHT	= 112;
//#  
//#  		public static final byte SPLASH_GAME_BOB_OFFSET_X = 44;
//#  		public static final byte SPLASH_GAME_BOB_OFFSET_Y = 57;
//#  
//#  		public static final byte SPLASH_GAME_SQUIDWARD_OFFSET_X = 100;
//#  		public static final byte SPLASH_GAME_SQUIDWARD_OFFSET_Y = 36;
//#  
//#  		public static final byte SPLASH_GAME_PATRICK_OFFSET_X = 71;
//#  		public static final byte SPLASH_GAME_PATRICK_OFFSET_Y = 47;
//#  
//#  		public static final byte SPLASH_GAME_TITLE_OFFSET_X = 3;
//#  		public static final byte SPLASH_GAME_TITLE_OFFSET_X_SPANISH = 0;
//#  		public static final byte SPLASH_GAME_TITLE_OFFSET_Y = 44;
//#  
//#  		public static final byte SPLASH_GAME_NICK_OFFSET_X = 9;
//#  		public static final byte SPLASH_GAME_NICK_OFFSET_Y = 7;
//#  
//#  		public static final byte SPLASH_GAME_SPONGEBOB_OFFSET_X = 33;
//#  		public static final byte SPLASH_GAME_SPONGEBOB_OFFSET_Y = 8;		
//# 
//# 		
//#  		// defini��es dos personagens
//#  		public static final byte LEFT_ARM_N_FRAMES = 2;
//#  		public static final byte RIGHT_ARM_N_FRAMES = 2;
//#  
//#  		// dist�ncia entre os bot�es
//#  		public static final byte BUTTONS_X_DISTANCE = 6;
//#  
//#  		// BOB ESPONJA
//#  		public static final byte BOB_BODY_OFFSET_Y = -16;
//#  		
//#  		public static final byte BOB_FACE_OFFSET_X = 5;
//#  		public static final byte BOB_FACE_OFFSET_Y = 27;
//#  		// bra�o esquerdo
//#  		public static final byte BOB_LEFT_ARM_0_OFFSET_X = 41;
//#  		public static final byte BOB_LEFT_ARM_0_OFFSET_Y = -14;
//#  		public static final byte BOB_LEFT_ARM_1_OFFSET_X = 44;
//#  		public static final byte BOB_LEFT_ARM_1_OFFSET_Y = -13;
//#  
//#  		// bra�o direito
//#  		public static final byte BOB_RIGHT_ARM_0_OFFSET_X = 3;
//#  		public static final byte BOB_RIGHT_ARM_0_OFFSET_Y = -8;
//#  		public static final byte BOB_RIGHT_ARM_1_OFFSET_X = 5;
//#  		public static final byte BOB_RIGHT_ARM_1_OFFSET_Y = -8;	
//#  
//#  		// PATRICK ESTRELA
//#  		public static final byte PATRICK_BODY_OFFSET_Y = -29;
//#  		
//#  		public static final byte PATRICK_FACE_OFFSET_X = 14;
//#  		public static final byte PATRICK_FACE_OFFSET_Y = 20;
//#  		// bra�o esquerdo
//#  		public static final byte PATRICK_LEFT_ARM_0_OFFSET_X = 42;
//#  		public static final byte PATRICK_LEFT_ARM_0_OFFSET_Y = -25;
//#  		public static final byte PATRICK_LEFT_ARM_1_OFFSET_X = 42;
//#  		public static final byte PATRICK_LEFT_ARM_1_OFFSET_Y = -20;
//#  
//#  		// bra�o direito
//#  		public static final byte PATRICK_RIGHT_ARM_0_OFFSET_X = 2;
//#  		public static final byte PATRICK_RIGHT_ARM_0_OFFSET_Y = -26;
//#  		public static final byte PATRICK_RIGHT_ARM_1_OFFSET_X = 5;
//#  		public static final byte PATRICK_RIGHT_ARM_1_OFFSET_Y = -26;	
//#  
//#  		// LULA MOLUSCO
//#  		public static final byte SQUIDWARD_FACE_REF_X_OFFSET = -5;
//#  		public static final byte SQUIDWARD_BODY_OFFSET_Y = -15;
//#  		
//#  		public static final byte SQUIDWARD_FACE_OFFSET_X = 3;
//#  		public static final byte SQUIDWARD_FACE_OFFSET_Y = 24;
//#  		// bra�o esquerdo
//#  		public static final byte SQUIDWARD_LEFT_ARM_0_OFFSET_X = 43;
//#  		public static final byte SQUIDWARD_LEFT_ARM_0_OFFSET_Y = -10;
//#  		public static final byte SQUIDWARD_LEFT_ARM_1_OFFSET_X = 43;
//#  		public static final byte SQUIDWARD_LEFT_ARM_1_OFFSET_Y = -6;
//#  
//#  		// bra�o direito
//#  		public static final byte SQUIDWARD_RIGHT_ARM_0_OFFSET_X = 18;
//#  		public static final byte SQUIDWARD_RIGHT_ARM_0_OFFSET_Y = -10;
//#  		public static final byte SQUIDWARD_RIGHT_ARM_1_OFFSET_X = 19;
//#  		public static final byte SQUIDWARD_RIGHT_ARM_1_OFFSET_Y = -12;		
//#  
//#  		
//#  		// defini��es do bra�o mec�nico
//#  		public static final short ARM_GROUP_BASE_WIDTH = 128;
//#  		
//#  		public static final byte ARM_GROUP_FRAME_0_OFFSET_X	= -32;
//#  		public static final byte ARM_GROUP_FRAME_0_OFFSET_Y	= -5;
//#  		public static final byte ARM_GROUP_FRAME_1_OFFSET_X	= -32;
//#  		public static final byte ARM_GROUP_FRAME_1_OFFSET_Y	= -10;
//#  		public static final byte ARM_GROUP_FRAME_2_OFFSET_X	= -32;
//#  		public static final byte ARM_GROUP_FRAME_2_OFFSET_Y	= -15;
//#  		public static final byte ARM_GROUP_FRAME_3_OFFSET_X	= -32;
//#  		public static final byte ARM_GROUP_FRAME_3_OFFSET_Y	= -5;
//#  		public static final byte ARM_GROUP_FRAME_4_OFFSET_X	= -32;
//#  		public static final byte ARM_GROUP_FRAME_4_OFFSET_Y	= 0;		
//# 	
//# 		// fim das defini��es de telas pequenas
	//#else
		// defini��es de telas m�dias e grandes
	
		// limite m�nimo de mem�ria considerado para que comece a haver "racionamento" de mem�ria
		public static final int MEMORY_LOW_LIMIT = 900000;
	
		// velocidade de movimenta��o do label de in�cio de n�vel em pixels por segundo
		public static final byte LABEL_LEVEL_SPEED = -98;

		// velocidade da anima��o do background em pixels por segundo
		public static final byte BACKGROUND_SPEED = 50;
		
		// espa�amento vertical entre os itens dos menus
		//#ifdef HEIGHT_176
//# 			public static final byte MENU_ITEMS_SPACING = 3;
		//#else
			public static final byte MENU_ITEMS_SPACING = 8;
		//#endif
		
		
		// defini��es do indicador de satisfa��o
		public static final byte SATISFACTION_METER_FILL_X		= 2;
		public static final byte SATISFACTION_METER_FILL_Y		= 52;
		public static final byte SATISFACTION_METER_FILL_WIDTH	= 6;
		public static final byte SATISFACTION_METER_FILL_HEIGHT	= 49;
		
		// mudan�a do n�vel de satisfa��o em pixels por segundo
		public static final byte SATISFACTION_CHANGE_SPEED = 19;
		

		// defini��es da esteira
		// velocidades m�xima e m�nima da esteira em pixels por segundo
		public static final byte TRAY_SPEED_MIN = 63;
		public static final short TRAY_SPEED_MAX = 147;	

		// largura em pixels da �rea v�lida para se atingir um ingrediente
		public static final byte TRAY_HIT_AREA_WIDTH = 40;
		// dist�ncia m�xima em pixels do centro do ingrediente ao centro da mira para que a jogada seja considerada perfeita
		public static final byte TRAY_BULLS_EYE_OFFSET = TRAY_HIT_AREA_WIDTH / 6;

		// dist�ncias m�xima e m�nima entre os slots (quanto maior a dificuldade, menor a dist�ncia)
		public static final short TRAY_SLOT_DISTANCE_INITIAL_MIN	= TRAY_HIT_AREA_WIDTH + 48;
		public static final short TRAY_SLOT_DISTANCE_INITIAL_MAX	= TRAY_SLOT_DISTANCE_INITIAL_MIN * 2;		
		public static final short TRAY_SLOT_DISTANCE_FINAL_MIN		= TRAY_HIT_AREA_WIDTH + 37;
		public static final short TRAY_SLOT_DISTANCE_FINAL_MAX		= TRAY_SLOT_DISTANCE_FINAL_MIN * 113 / 100;		

		// dist�ncia do centro da mira superior ao centro da tela. As posi��es das outras miras s�o calculadas dinamicamente
		// no construtor da classe Tray.
		public static final byte TRAY_TARGET_TOP_X_OFFSET = -45;

		// dist�ncia da parte superior da mira � imagem do laser
		public static final byte TRAY_AIM_TOP_OFFSET_X = -12;
		public static final byte TRAY_AIM_TOP_OFFSET_Y = -10;

		// dist�ncia da parte inferior da mira � imagem do laser
		public static final byte TRAY_AIM_BOTTOM_OFFSET_X = 24;
		public static final byte TRAY_AIM_BOTTOM_OFFSET_Y = 42;	
	
		// valor utilizado para calcular a velocidade de anima��o das engrenagens da esteira
		public static final short TRAY_GEAR_FACTOR = 22000;
		
		// posi��o do indicador do pedido relativa � posi��o dos controles
		public static final byte ORDER_INDICATOR_X_OFFSET = 105;
		public static final byte ORDER_INDICATOR_Y_OFFSET = -17;

		// posi��o relativa dos ingredientes no interior do indicador de pedidos
		public static final byte ORDER_INDICATOR_INGREDIENT_X = 9;
		public static final byte ORDER_INDICATOR_INGREDIENT_Y = 9;

		// �rea vis�vel ingredientes no interior do indicador de pedidos
		public static final byte ORDER_INDICATOR_WIDTH  = 27;
		public static final byte ORDER_INDICATOR_HEIGHT = 12;

		// velocidade de movimenta��o da esteira grande (hamb�rguer em primeiro plano), em pixels por segundo
		public static final short BIG_TRAY_MOVE_SPEED = -260;

		// velocidade de movimenta��o dos ingredientes durante a anima��o de troca
		public static final short INGREDIENT_CHANGE_SPEED = -313;
		
		// velocidade do reposicionamento vertical do hamb�rguer quando ele ultrapassa seu limite
		public static final byte PATTY_VERTICAL_SPEED = 36;

		// acelera��o da "gravidade" em pixels por segundo ao quadrado
		public static final short GRAVITY_ACCELERATION = 445;
		
		
		// tela de sele��o de personagens
		public static final short CHOOSE_CHARACTER_DEFAULT_HEIGHT = 320;


		// defini��es do splash
		public static final short SPLASH_GAME_WIDTH		= 176;
		public static final short SPLASH_GAME_HEIGHT	= 220;

		public static final byte SPLASH_GAME_BOB_OFFSET_X = 36;
		public static final short SPLASH_GAME_BOB_OFFSET_Y = 135;

		public static final byte SPLASH_GAME_SQUIDWARD_OFFSET_X = 95;
		public static final byte SPLASH_GAME_SQUIDWARD_OFFSET_Y = 79;

		public static final byte SPLASH_GAME_PATRICK_OFFSET_X = 108;
		public static final byte SPLASH_GAME_PATRICK_OFFSET_Y = 87;

		public static final byte SPLASH_GAME_TITLE_OFFSET_X = 6;
		public static final byte SPLASH_GAME_TITLE_OFFSET_X_SPANISH = 0;
		public static final byte SPLASH_GAME_TITLE_OFFSET_Y = 89;

		//#ifdef HEIGHT_176
//# 			public static final byte SPLASH_GAME_NICK_OFFSET_X = -10;
//# 			public static final byte SPLASH_GAME_NICK_OFFSET_Y = 0;
		//#else
			public static final byte SPLASH_GAME_NICK_OFFSET_X = 5;
			public static final byte SPLASH_GAME_NICK_OFFSET_Y = 3;			
		//#endif		

		public static final byte SPLASH_GAME_SPONGEBOB_OFFSET_X = 29;
		public static final byte SPLASH_GAME_SPONGEBOB_OFFSET_Y = 3;

		
		// offset na posi��o dos bot�es para eventos de ponteiro em rela��o ao indicador de pedidos
		public static final byte BUTTON_X_OFFSET = -3;
		public static final byte BUTTON_Y_OFFSET = -78;


		// defini��es dos personagens
		public static final byte LEFT_ARM_N_FRAMES = 2;
		public static final byte RIGHT_ARM_N_FRAMES = 2;

		// dist�ncia entre os bot�es
		public static final byte BUTTONS_X_DISTANCE = 13;

		// BOB ESPONJA
		public static final byte BOB_BODY_OFFSET_Y = -28;
		
		public static final byte BOB_FACE_OFFSET_X = 9;
		public static final byte BOB_FACE_OFFSET_Y = 44;
		// bra�o esquerdo
		public static final byte BOB_LEFT_ARM_0_OFFSET_X = 75;
		public static final byte BOB_LEFT_ARM_0_OFFSET_Y = -24;
		public static final byte BOB_LEFT_ARM_1_OFFSET_X = 72;
		public static final byte BOB_LEFT_ARM_1_OFFSET_Y = -23;

		// bra�o direito
		public static final byte BOB_RIGHT_ARM_0_OFFSET_X = 5;
		public static final byte BOB_RIGHT_ARM_0_OFFSET_Y = -17;
		public static final byte BOB_RIGHT_ARM_1_OFFSET_X = 5;
		public static final byte BOB_RIGHT_ARM_1_OFFSET_Y = -19;	

		// PATRICK ESTRELA
		public static final byte PATRICK_BODY_OFFSET_Y = -48;
		
		public static final byte PATRICK_FACE_OFFSET_X = 23;
		public static final byte PATRICK_FACE_OFFSET_Y = 34;
		// bra�o esquerdo
		public static final byte PATRICK_LEFT_ARM_0_OFFSET_X = 67;
		public static final byte PATRICK_LEFT_ARM_0_OFFSET_Y = -45;
		public static final byte PATRICK_LEFT_ARM_1_OFFSET_X = 70;
		public static final byte PATRICK_LEFT_ARM_1_OFFSET_Y = -36;

		// bra�o direito
		public static final byte PATRICK_RIGHT_ARM_0_OFFSET_X = 3;
		public static final byte PATRICK_RIGHT_ARM_0_OFFSET_Y = -42;
		public static final byte PATRICK_RIGHT_ARM_1_OFFSET_X = 6;
		public static final byte PATRICK_RIGHT_ARM_1_OFFSET_Y = -38;	

		// LULA MOLUSCO
		public static final byte SQUIDWARD_FACE_REF_X_OFFSET = -18;
		public static final byte SQUIDWARD_BODY_OFFSET_Y = -25;
		
		public static final byte SQUIDWARD_FACE_OFFSET_X = 6;
		public static final byte SQUIDWARD_FACE_OFFSET_Y = 47;
		// bra�o esquerdo
		public static final byte SQUIDWARD_LEFT_ARM_0_OFFSET_X = 72;
		public static final byte SQUIDWARD_LEFT_ARM_0_OFFSET_Y = -18;
		public static final byte SQUIDWARD_LEFT_ARM_1_OFFSET_X = 71;
		public static final byte SQUIDWARD_LEFT_ARM_1_OFFSET_Y = -14;

		// bra�o direito
		public static final byte SQUIDWARD_RIGHT_ARM_0_OFFSET_X = 26;
		public static final byte SQUIDWARD_RIGHT_ARM_0_OFFSET_Y = -23;
		public static final byte SQUIDWARD_RIGHT_ARM_1_OFFSET_X = 30;
		public static final byte SQUIDWARD_RIGHT_ARM_1_OFFSET_Y = -23;		

		
		// defini��es do bra�o mec�nico
		public static final short ARM_GROUP_BASE_WIDTH = 240;
		
		public static final byte ARM_GROUP_FRAME_0_OFFSET_X	= -32;
		public static final byte ARM_GROUP_FRAME_0_OFFSET_Y	= -5;
		public static final byte ARM_GROUP_FRAME_1_OFFSET_X	= -32;
		public static final byte ARM_GROUP_FRAME_1_OFFSET_Y	= -10;
		public static final byte ARM_GROUP_FRAME_2_OFFSET_X	= -32;
		public static final byte ARM_GROUP_FRAME_2_OFFSET_Y	= -15;
		public static final byte ARM_GROUP_FRAME_3_OFFSET_X	= -32;
		public static final byte ARM_GROUP_FRAME_3_OFFSET_Y	= -5;
		public static final byte ARM_GROUP_FRAME_4_OFFSET_X	= -32;
		public static final byte ARM_GROUP_FRAME_4_OFFSET_Y	= 0;
		
		// fim das defini��es de telas m�dias e grandes
	//#endif
	
	
}
