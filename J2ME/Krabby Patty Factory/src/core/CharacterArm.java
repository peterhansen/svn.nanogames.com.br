/*
 * CharacterArm.java
 *
 * Created on June 1, 2007, 4:13 PM
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public final class CharacterArm extends Sprite implements Constants, SpriteListener {
	
	public static final byte SEQUENCE_STOPPED	= 0;
	public static final byte SEQUENCE_HIT		= 1;
	
	
	private final Point[] FRAME_POSITION;
	
	public static final byte ARM_LEFT = 0;
	public static final byte ARM_RIGHT = 1;
	
	// dura��o em milisegundos de cada frame de anima��o das m�os
	private static final short HAND_LEFT_FRAME_TIME = 101;
	private static final short HAND_RIGHT_FRAME_TIME = 164;		
	
	private final byte type;
	
	// vari�vel utilizada para fazer a troca aleat�ria do bot�o pressionado pelo personagem
	private boolean alternatePosition;
	
	// indica se o bra�o pode mover-se para o lado (somente Bob Esponja)
	private final boolean moveRight;
	
	
	/**
	 * Creates a new instance of CharacterArm
	 */
	public CharacterArm( byte character, byte arm ) throws Exception {
		super( loadSprite( character, arm ) );
		
		setListener( this, 0 );
		
		moveRight = ( character == Character.SPONGEBOB );
		
		switch ( arm ) {
			case ARM_LEFT:
				FRAME_POSITION = new Point[ LEFT_ARM_N_FRAMES ];
				
				switch ( character ) {
					case Character.SPONGEBOB:
						FRAME_POSITION[ 0 ] = new Point( BOB_LEFT_ARM_0_OFFSET_X, BOB_LEFT_ARM_0_OFFSET_Y );
						FRAME_POSITION[ 1 ] = new Point( BOB_LEFT_ARM_1_OFFSET_X, BOB_LEFT_ARM_1_OFFSET_Y );
					break;
					
					case Character.PATRICK:
						FRAME_POSITION[ 0 ] = new Point( PATRICK_LEFT_ARM_0_OFFSET_X, PATRICK_LEFT_ARM_0_OFFSET_Y );
						FRAME_POSITION[ 1 ] = new Point( PATRICK_LEFT_ARM_1_OFFSET_X, PATRICK_LEFT_ARM_1_OFFSET_Y );						
					break;
					
					case Character.SQUIDWARD:
						FRAME_POSITION[ 0 ] = new Point( SQUIDWARD_LEFT_ARM_0_OFFSET_X, SQUIDWARD_LEFT_ARM_0_OFFSET_Y );
						FRAME_POSITION[ 1 ] = new Point( SQUIDWARD_LEFT_ARM_1_OFFSET_X, SQUIDWARD_LEFT_ARM_1_OFFSET_Y );						
					break;
				}
			break;
			
			case ARM_RIGHT:
				FRAME_POSITION = new Point[ RIGHT_ARM_N_FRAMES ];		
				
				switch ( character ) {
					case Character.SPONGEBOB:
						FRAME_POSITION[ 0 ] = new Point( BOB_RIGHT_ARM_0_OFFSET_X, BOB_RIGHT_ARM_0_OFFSET_Y );
						FRAME_POSITION[ 1 ] = new Point( BOB_RIGHT_ARM_1_OFFSET_X, BOB_RIGHT_ARM_1_OFFSET_Y );
					break;
					
					case Character.PATRICK:
						FRAME_POSITION[ 0 ] = new Point( PATRICK_RIGHT_ARM_0_OFFSET_X, PATRICK_RIGHT_ARM_0_OFFSET_Y );
						FRAME_POSITION[ 1 ] = new Point( PATRICK_RIGHT_ARM_1_OFFSET_X, PATRICK_RIGHT_ARM_1_OFFSET_Y );							
					break;
					
					case Character.SQUIDWARD:
						FRAME_POSITION[ 0 ] = new Point( SQUIDWARD_RIGHT_ARM_0_OFFSET_X, SQUIDWARD_RIGHT_ARM_0_OFFSET_Y );
						FRAME_POSITION[ 1 ] = new Point( SQUIDWARD_RIGHT_ARM_1_OFFSET_X, SQUIDWARD_RIGHT_ARM_1_OFFSET_Y );							
					break;
				}				
			break;
			
			default:
				//#if DEBUG == "true"
					throw new Exception( "Invalid arm index: " + arm );
				//#else
//# 					throw new Exception();
				//#endif
		}
		
		type = arm;
		setFrame( 0 );
	}
	
	
	private static final Sprite loadSprite( byte character, byte arm ) throws Exception {
		String path;
		
		switch ( character ) {
			case Character.SPONGEBOB:
				path = PATH_BOB;
			break;
			
			case Character.PATRICK:
				path = PATH_PATRICK;
			break;
			
			case Character.SQUIDWARD:
				path = PATH_SQUIDWARD;
			break;
			
			default:
				//#if DEBUG == "true"
					throw new Exception( "Invalid character: "+ character );
				//#else
//# 					throw new Exception();
				//#endif
		}
		
		short frameTime;
		byte totalImages;
		switch ( arm ) {
			case ARM_LEFT:
				path += "left_";
				totalImages = LEFT_ARM_N_FRAMES;
				frameTime = HAND_LEFT_FRAME_TIME;
			break;
			
			case ARM_RIGHT:
				path += "right_";
				totalImages = RIGHT_ARM_N_FRAMES;
				frameTime = HAND_RIGHT_FRAME_TIME;
			break;
			
			default:
				//#if DEBUG == "true"
					throw new Exception( "Invalid arm index: " + arm );
				//#else
//# 					throw new Exception();
				//#endif
		}
		
		final Image[] images = new Image[ totalImages ];
		final byte[][] sequences = new byte[ 2 ][ images.length ];
		final short[] frameTimes = new short[] { 0, frameTime };
		
		//#if SCREEN_SIZE == "BIG"
		if ( arm == ARM_LEFT && character == Character.SQUIDWARD ) {
		//#else
//# 		if ( arm == ARM_LEFT && character != Character.SPONGEBOB ) {
		//#endif
			for ( byte i = 0; i < images.length; ++i ) {
				images[ i ] = ImageLoader.loadImage( path + "0.png" );	
				sequences[ 1 ][ i ] = i;
			}
		} else {
			for ( byte i = 0; i < images.length; ++i ) {
				images[ i ] = ImageLoader.loadImage( path + i + ".png" );
				sequences[ 1 ][ i ] = i;
			}
		}
		return new Sprite( images, sequences, frameTimes );
	}

	
	public final void setFrame( int frame ) {
		super.setFrame( frame );
		
		if ( FRAME_POSITION != null ) {
			setPosition( FRAME_POSITION[ frame ] );
			if ( moveRight && type == ARM_LEFT && sequenceIndex == SEQUENCE_HIT && frame == 1 ) {
				if ( NanoMath.randInt( 100 ) < 50 )
					alternatePosition = !alternatePosition;

				if ( alternatePosition )
					move( BUTTONS_X_DISTANCE, 0 );			
			}
		}
	}
	
	
	public final void setPositionOffset( Point offset ) {
		for ( int i = 0; i < FRAME_POSITION.length; ++i ) {
			FRAME_POSITION[ i ].addEquals( offset );
		}
		
		setFrame( frameSequenceIndex );
	}

	
	public final void onSequenceEnded( int id, int sequence ) {
		setSequence( SEQUENCE_STOPPED );
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}

	
}
