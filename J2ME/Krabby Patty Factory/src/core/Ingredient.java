/*
 * Ingredient.java
 *
 * Created on May 3, 2007, 8:11 PM
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.util.Vector;
import javax.microedition.lcdui.Image;


/**
 *
 * @author Peter Hansen
 */
public class Ingredient extends Sprite implements Constants, SpriteListener {
	
	public static final byte INGREDIENT_PATTY				= 0;
	public static final byte INGREDIENT_BREAD_BOTTOM		= INGREDIENT_PATTY + 1;
	public static final byte INGREDIENT_BREAD_MIDDLE		= INGREDIENT_BREAD_BOTTOM + 1;
	public static final byte INGREDIENT_BREAD_TOP			= INGREDIENT_BREAD_MIDDLE + 1;	
	public static final byte INGREDIENT_SEA_LETTUCE			= INGREDIENT_BREAD_TOP + 1;
	public static final byte INGREDIENT_SEA_CHEESE			= INGREDIENT_SEA_LETTUCE + 1;
	public static final byte INGREDIENT_TOMATO				= INGREDIENT_SEA_CHEESE + 1;
	public static final byte INGREDIENT_ONION				= INGREDIENT_TOMATO + 1;	
	public static final byte INGREDIENT_PICKLES				= INGREDIENT_ONION + 1;
	
	public static final byte TOTAL_INGREDIENT_TYPES		= INGREDIENT_PICKLES + 1;
	
	// varia��o na posi��o y do hamb�rguer a cada ingrediente inserido
	public static final byte[] PATTY_Y_OFFSET = new byte[ TOTAL_INGREDIENT_TYPES ];
	
	// dura��o de um frame em milisegundos
	private static final short FRAME_TIME = 99;
	
	private static Sprite[] sprites;
	private static boolean spritesLoaded;
	
	private final byte type;
	
	// sequ�ncias de anima��o dos ingredientes
	// ingrediente na esteira
	public static final byte SEQUENCE_TRAY			= 0;
	// ingrediente parado no hamb�rguer
	public static final byte SEQUENCE_PATTY_STOP	= SEQUENCE_TRAY + 1;	
	// ingrediente atingindo o hamb�rguer (se achata um pouco)
	public static final byte SEQUENCE_PATTY_HIT		= SEQUENCE_PATTY_STOP + 1;
	
	public static final byte TOTAL_SEQUENCES		= 3;
	
	// dire��es de movimento
	public static final byte DIRECTION_NONE	= 0;
	public static final byte DIRECTION_UP	= 1;
	public static final byte DIRECTION_DOWN	= 2;
	
	private byte direction;

	private int lastAccModule;
	
	private int lastSpeed;
	
	private int lastSpeedModule;

	// estados da leitura do arquivo descritor de ingredientes
	private static final byte READ_INGREDIENT_STRING	= 0;
	private static final byte READ_INGREDIENT_N_FRAMES	= 1;
	private static final byte READ_INGREDIENT_Y_OFFSET	= 2;
	private static final byte READ_INGREDIENT_SEQUENCES = 3;
	
	// precis�o no acerto do ingrediente
	private byte precision;
	
	// n�vel de precis�o (no caso de o ingrediente ter sido inserido na posi��o errada, seu n�vel de precis�o � o pior poss�vel)
	private byte precisionLevel;
	
	
	public Ingredient( int type ) throws Exception {
		super( sprites[ type ] );
		this.type = ( byte ) type;
		
		setListener( this, 0 );
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
	}
	
	
	public final void setDirection( byte direction ) {
		switch ( direction ) {
			case DIRECTION_DOWN:
				setSequence( Ingredient.SEQUENCE_PATTY_STOP );
				lastSpeed = 0;
				lastSpeedModule = 0;				
				lastAccModule = 0;
				setRefPixelPosition( getRefPixelX(), -ScreenManager.SCREEN_HEIGHT >> 2 );
			break;
			
			case DIRECTION_UP:
				lastSpeed = -NanoMath.sqrtInt( ( getRefPixelY() + ( ScreenManager.SCREEN_HEIGHT >> 2 ) ) * ( GRAVITY_ACCELERATION << 1 ) );
			case DIRECTION_NONE:
				lastSpeedModule = 0;				
				lastAccModule = 0;
			break;
			
			default:
				return;
		} // fim switch ( direction )
		this.direction = direction;
	}
	
	
	public final byte getDirection() {
		return direction;
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		switch ( direction ) {
			case DIRECTION_UP:
			case DIRECTION_DOWN:
				if ( direction == DIRECTION_DOWN || lastSpeed <= 0 ) {
					// s = s0 + vt;
					final int deltaPos = lastSpeedModule + ( lastSpeed * delta );
					final int deltaY = deltaPos / 1000;
					lastSpeedModule = deltaPos % 1000;

					// v = v0 + at;
					final int deltaSpeed = lastAccModule + ( GRAVITY_ACCELERATION * delta );
					final int deltaV = deltaSpeed / 1000;
					lastAccModule = deltaSpeed % 1000;

					lastSpeed += deltaV;

					move( 0, deltaY );
				} // fim if ( direction == DIRECTION_DOWN || lastSpeed < 0 )
			break;
			
		} // fim switch ( direction )
	} // fim do m�todo update( int )
	
	
	/**
	 * Carrega os quadros de anima��o dos ingredientes.
	 * @throws java.lang.Exception caso haja erro na leitura do arquivo descritor
	 */
	public static final void loadSprites() throws Exception {
		if ( !spritesLoaded ) {
			InputStream inputStream = null;
			DataInputStream dataInputStream = null;

			byte currentIngredient = INGREDIENT_PATTY;
			byte readState = READ_INGREDIENT_STRING;					

			sprites = new Sprite[ TOTAL_INGREDIENT_TYPES ];
			
			try {
				final StringBuffer tempBuffer = new StringBuffer();

				// armazena os �ndices dos frames da sequ�ncia atual (quantidade de frames pode variar)
				final Vector sequenceVector = new Vector();
				
				final byte[][] sequences = new byte[ TOTAL_SEQUENCES ][];

				final Image[][] ingredientFrames = new Image[ TOTAL_INGREDIENT_TYPES ][];
				
				final short[] frameTimes = new short[] { 0, 0, FRAME_TIME };

				short index = 0;

				inputStream = sequenceVector.getClass().getResourceAsStream( PATH_INGREDIENTS + "info.txt" );
				dataInputStream = new DataInputStream( inputStream );
				
				String filename = "";
				
				boolean negative = false;
				
				while ( currentIngredient < TOTAL_INGREDIENT_TYPES ) {
					final char c = java.lang.Character.toLowerCase( ( char ) dataInputStream.readUnsignedByte() );

					switch ( readState ) {
						case READ_INGREDIENT_STRING:
							switch ( c ) {
								case ' ':
									if ( tempBuffer.length() > 0 ) {
										// terminou de ler o in�cio do nome do arquivo
										tempBuffer.insert( 0, PATH_INGREDIENTS );
										tempBuffer.append( '_' );
										filename = tempBuffer.toString();
										
										readState = READ_INGREDIENT_N_FRAMES;
										
										tempBuffer.delete( 0, tempBuffer.length() );
										index = 0;
									} // fim if ( tempBuffer.length() > 0 ) {
								break;

								case '\n':
								case '\t':
								break;

								default:
									tempBuffer.append( c );
							} // fim switch ( c )
						break; // fim case READ_INGREDIENT_STRING

						case READ_INGREDIENT_N_FRAMES:
							switch ( c ) {
								case ' ':
								case '\n':
									if ( tempBuffer.length() > 0 ) {
										// leu a quantidade de frames de uma sequ�ncia
										int totalFrames = Integer.parseInt( tempBuffer.toString() );
										
										ingredientFrames[ currentIngredient ] = new Image[ totalFrames ];
										tempBuffer.delete( 0, tempBuffer.length() );
										
										tempBuffer.append( filename );
										final int radicalLength = tempBuffer.length();
										
										// terminou de ler o nome do arquivo e a quantidade total de frames; carrega
										// as imagens de cada frame
										for ( int i = 0; i < totalFrames; ++i ) {
											tempBuffer.append( i );
											tempBuffer.append( ".png" );

											//#if DEBUG == "true"
											System.out.print( "carregando imagem: " + tempBuffer.toString() );
											//#endif

											// carrega ao frame a partir do arquivo
											ingredientFrames[ currentIngredient ][ i ] = ImageLoader.loadImage( tempBuffer.toString() );
											//#if DEBUG == "true"
											System.out.println( "...ok" );
											//#endif
											
											// apaga o final do nome do arquivo, mantendo somente o radical contendo o nome
											// do ingrediente e o �ndice da sequ�ncia atual
											tempBuffer.delete( radicalLength, tempBuffer.length() );
										} // fim for ( int i = 0; i < totalFrames; ++i )
									} else {
										//#if DEBUG == "true"
											throw new Exception( "n�mero de frames do ingrediente #" + currentIngredient + " n�o informado." );
										//#else
//# 											throw new Exception();
										//#endif
									}

									index = 0;
									readState = READ_INGREDIENT_Y_OFFSET;
									sequenceVector.removeAllElements();
									tempBuffer.delete( 0, tempBuffer.length() );
								break;

								default:
									if ( c >= '0' && c <= '9' )
										tempBuffer.append( c );
							} // fim switch ( c )						
						break; // fim case READ_INGREDIENT_N_FRAMES
						
						case READ_INGREDIENT_Y_OFFSET:
							switch ( c ) {
								case '-':
									negative = true;
								break;
								
								case ' ':
								case '\n':
									if ( tempBuffer.length() > 0 ) {
										// leu o offset y no hamb�rguer a cada ingrediente inserido
										PATTY_Y_OFFSET[ currentIngredient ] = ( byte ) Integer.parseInt( tempBuffer.toString() );
										if ( negative )
											PATTY_Y_OFFSET[ currentIngredient ] = ( byte ) -Math.abs( PATTY_Y_OFFSET[ currentIngredient ] );
									} else {
										//#if DEBUG == "true"
											throw new Exception( "offset y do ingrediente #" + currentIngredient + " n�o informado." );
										//#else
//# 											throw new Exception();
										//#endif
									}
									
									index = 0;
									readState = READ_INGREDIENT_SEQUENCES;
									sequenceVector.removeAllElements();
									tempBuffer.delete( 0, tempBuffer.length() );									
								break;
								
								default:
									if ( c >= '0' && c <= '9' )
										tempBuffer.append( c );								
							} // fim switch ( c )
						break; // fim case READ_INGREDIENT_Y_OFFSET

						case READ_INGREDIENT_SEQUENCES:
							switch ( c ) {
								case ' ':
									// caso j� tenha sido lido algum valor, o adiciona � sequ�ncia
									if ( tempBuffer.length() > 0 ) {
										sequenceVector.addElement( Integer.valueOf( tempBuffer.toString() ) );
										
										tempBuffer.delete( 0, tempBuffer.length() );
									}
								break;

								case '\n':
									// caso j� tenha sido lido algum valor, o adiciona � sequ�ncia
									if ( tempBuffer.length() > 0 ) {
										sequenceVector.addElement( Integer.valueOf( tempBuffer.toString() ) );
										tempBuffer.delete( 0, tempBuffer.length() );
									}									
									
									// terminou de ler uma sequ�ncia
									sequences[ index ] = new byte[ sequenceVector.size() ];
									final int sequenceLength = sequences[ index ].length;
									for ( byte i = 0; i < sequenceLength; ++i )
										sequences[ index ][ i ] = ( ( Integer ) sequenceVector.elementAt( i ) ).byteValue();
									
									if ( ++index == TOTAL_SEQUENCES ) {
										// leu todas as sequ�ncias; segue para o pr�ximo ingrediente
										sprites[ currentIngredient ] = new Sprite( ingredientFrames[ currentIngredient ], sequences, frameTimes );
										
										for ( byte i = 0; i < ingredientFrames[ currentIngredient ].length; ++i ) {
											if ( ( i & 1 ) == 0 )
												Thread.yield();
											
											final Point offset = sprites[ currentIngredient ].getFrameOffset( i );
											final Image frame = ingredientFrames[ currentIngredient ][ i ];
											final Point spriteSize = sprites[ currentIngredient ].getSize();
											offset.set( ( spriteSize.x - frame.getWidth() ) >> 1, spriteSize.y - frame.getHeight() );
										}
										
										readState = READ_INGREDIENT_STRING;
										++currentIngredient;
										negative = false;
										
										Thread.yield();
									}

									tempBuffer.delete( 0, tempBuffer.length() );
									sequenceVector.removeAllElements();
								break;

								default:
									if ( c >= '0' && c <= '9' )
										tempBuffer.append( c );
							} // fim switch ( c )						
						break; // fim case READ_INGREDIENT_SEQUENCES

					} // fim switch ( readState )
				} // fim while ( currentIngredient < TOTAL_INGREDIENT_TYPES )
			} catch ( EOFException e ) {
				// terminou de ler o arquivo; verifica se todos os ingredientes foram lidos
				if ( currentIngredient < TOTAL_INGREDIENT_TYPES ) {
					//#if DEBUG == "true"
						throw new Exception( "Ingredient: leitura incompleta dos ingredientes: " + currentIngredient + " lidos, de um total de " + TOTAL_INGREDIENT_TYPES + "." );
					//#else
//# 						throw new Exception();
					//#endif
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif

				// desaloca os conjuntos de frames j� carregados, caso existam
				unloadSprites();
				//#if DEBUG == "true"
					throw new Exception( "Erro ao ler imagens do ingrediente #" + currentIngredient );
				//#else
//# 					throw new Exception();
				//#endif
			} finally {
				try {
					dataInputStream.close();
					dataInputStream = null;
					inputStream = null;				
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif				
				}
			} // fim finally

			spritesLoaded = true;
		} // fim if ( !frameSetsLoaded )
	} // fim do m�todo loadFrameSets()
	
	public static final void unloadSprites() {
		sprites = null;
		
		spritesLoaded = false;
	}
	
	
	public final void setPrecision( int precision ) {
		this.precision = ( byte ) precision;
		
		int newPrecisionLevel = 100 - Math.abs( precision );
		if ( newPrecisionLevel >= PRECISION_PERFECT )
			setPrecisionLevel( PRECISION_PERFECT );
		else if ( newPrecisionLevel >= PRECISION_GREAT )
			setPrecisionLevel( PRECISION_GREAT );
		else if ( newPrecisionLevel >= PRECISION_GOOD )
			setPrecisionLevel( PRECISION_GOOD );
		else
			setPrecisionLevel( PRECISION_BAD );
	}
	
	
	public final int getPrecision() {
		return precision;
	}
	
	
	public final byte getPrecisionLevel() {
		return precisionLevel;
	}
	
	
	public final void setPrecisionLevel( int precisionLevel ) {
		this.precisionLevel	= ( byte ) precisionLevel;
	}
	
	
	public final int getScore() {
		return ( 100 - Math.abs( precision ) ) * MAX_SCORE_PER_INGREDIENT / 100;
	}
	
	
	public final byte getType() {
		return type;
	}
	
	
	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case SEQUENCE_PATTY_HIT:
				setSequence( SEQUENCE_PATTY_STOP );
			break;
		}
	}
	
	
	public static final Sprite[] getSprites() {
		return sprites;
	}
	
	
	public final void positionAfter( Ingredient ingredient ) {
		if ( direction != DIRECTION_DOWN ) {
			setDirection( DIRECTION_DOWN );

			if ( ingredient != this ) {
				lastSpeed = ingredient.lastSpeed >> 2;
				final int yPos = ingredient.getPosition().y - size.y;
				setRefPixelPosition( getRefPixelX(), yPos >= -size.y ? -size.y : yPos );
			} // fim if ( ingredient != this )
		} // fim if ( direction != DIRECTION_DOWN )
	} // fim do m�todo positionAfter( Ingredient )


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}

	
}
 
