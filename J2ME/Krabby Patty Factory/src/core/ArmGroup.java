/*
 * ArmGroup.java
 *
 * Created on May 29, 2007, 2:40 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Image;
import screens.PlayScreen;

/**
 *
 * @author peter
 */
public final class ArmGroup extends UpdatableGroup implements Constants, SpriteListener {
	
	private static final byte TOTAL_ITEMS = 2;
	
	public static final byte ARM_SEQUENCE_STOPPED		= 0;
	public static final byte ARM_SEQUENCE_BEFORE_HIT	= 1;
	public static final byte ARM_SEQUENCE_AFTER_HIT		= 2;	
	
	private final Sprite arm;
	
	private final Point[] ARM_GROUP_POSITION = { 
		new Point( ( ( ScreenManager.SCREEN_WIDTH - ARM_GROUP_BASE_WIDTH ) >> 1 ) + ARM_GROUP_FRAME_0_OFFSET_X, ARM_GROUP_FRAME_0_OFFSET_Y ),
		new Point( ( ( ScreenManager.SCREEN_WIDTH - ARM_GROUP_BASE_WIDTH ) >> 1 ) + ARM_GROUP_FRAME_1_OFFSET_X, ARM_GROUP_FRAME_1_OFFSET_Y ),
		new Point( ( ( ScreenManager.SCREEN_WIDTH - ARM_GROUP_BASE_WIDTH ) >> 1 ) + ARM_GROUP_FRAME_2_OFFSET_X, ARM_GROUP_FRAME_2_OFFSET_Y ),
		new Point( ( ( ScreenManager.SCREEN_WIDTH - ARM_GROUP_BASE_WIDTH ) >> 1 ) + ARM_GROUP_FRAME_3_OFFSET_X, ARM_GROUP_FRAME_3_OFFSET_Y ),
		new Point( ( ( ScreenManager.SCREEN_WIDTH - ARM_GROUP_BASE_WIDTH ) >> 1 ) + ARM_GROUP_FRAME_4_OFFSET_X, ARM_GROUP_FRAME_4_OFFSET_Y ),
		new Point( ( ( ScreenManager.SCREEN_WIDTH - ARM_GROUP_BASE_WIDTH ) >> 1 ) + ARM_GROUP_FRAME_4_OFFSET_X, ARM_GROUP_FRAME_4_OFFSET_Y ),
		};
	
	private final Point[] ARM_TARGET_OFFSET;
	
	private byte tray;
	
	private final PlayScreen playScreen;
	
	private final byte ID_SEQUENCE	= 0;
	private final byte ID_FRAME		= 1;
	
	
	/**
	 * Creates a new instance of ArmGroup
	 */
	public ArmGroup( PlayScreen playScreen, Point[] targetOffset ) throws Exception {
		super( TOTAL_ITEMS );
		
		if ( playScreen == null ) {
			//#if DEBUG == "true"
				throw new NullPointerException( "playScreen is null." );
			//#else
//# 				throw new NullPointerException();
			//#endif
		}
		
		this.playScreen = playScreen;

		final DrawableImage base = new DrawableImage( PATH_IMAGES + "arm_base.png" );
		insertDrawable( base );
		
		arm = new Sprite( PATH_IMAGES + "arm.dat", PATH_IMAGES + "arm" ) {
			public final void setFrame( int frame ) {
				super.setFrame( frame );
				
				if ( listener != null )
					listener.onSequenceEnded( ID_FRAME, frame );
			}
		};
		arm.setListener( this, ID_SEQUENCE );
		insertDrawable( arm );
		
		setSize( arm.getSize() );
		defineReferencePixel( size.x, size.y );
		
		ARM_TARGET_OFFSET = targetOffset;
		for ( int i = 0; i < ARM_TARGET_OFFSET.length; ++i )
			ARM_TARGET_OFFSET[ i ].y -= size.y;		
		
		onSequenceEnded( ID_FRAME, TRAY_MIDDLE );
	}
	
	
	/**
	 * O bra�o mec�nico atinge uma fileira caso ele esteja pronto, ou seja, caso ele n�o esteja no meio de uma outra anima��o.
	 * Quando sua anima��o estiver no ponto em que ele de fato atinge o ingrediente (caso haja algum ingrediente na
	 * mira da esteira escolhida), o m�todo <i>hit</i> da classe PlayScreen � chamado.
	 * @param tray �ndice da fileira a ser atingida.
	 */
	public final void hit( byte tray ) {
		switch ( arm.getSequence() ) {
			case ARM_SEQUENCE_STOPPED:
				this.tray = tray;
				arm.setSequence( ARM_SEQUENCE_BEFORE_HIT );
			break;
		} // fim switch ( currentSequence )
	}	
	

	public final void onSequenceEnded( int id, int sequence ) {
		switch( id ) {
			case ID_SEQUENCE:
				switch ( sequence ) {
					case ARM_SEQUENCE_BEFORE_HIT:
						playScreen.removeIngredientAt( tray );
						arm.setSequence( ARM_SEQUENCE_AFTER_HIT );
					break;

					case ARM_SEQUENCE_AFTER_HIT:
						arm.setSequence( ARM_SEQUENCE_STOPPED );
					break;
				}
			break;
			
			case ID_FRAME:
				// esse m�todo � utilizado aqui como um "onSetFrame"
				setPosition( ARM_GROUP_POSITION[ sequence ].add( ARM_TARGET_OFFSET[ tray ]) );
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
	
	
}
