/*
 * Control.java
 *
 * Created on June 1, 2007, 4:07 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author peter
 */
public final class Control extends UpdatableGroup implements Constants {
	
	private static final byte TOTAL_ITEMS = 4;
	
	// m�os do personagem
	private final CharacterArm rightArm;
	private final CharacterArm leftArm;
	
	
	public final Point screenPosition;
			
	
	/** Creates a new instance of Control */
	public Control( byte character ) throws Exception {
		super( TOTAL_ITEMS );
		
		String characterPath;
		switch ( character ) {
			case Character.SPONGEBOB:
				characterPath = PATH_BOB;
			break;
			
			case Character.PATRICK:
				characterPath = PATH_PATRICK;
			break;
			
			case Character.SQUIDWARD:
				characterPath = PATH_SQUIDWARD;
			break;
			
			default:
				//#if DEBUG == "true"
					throw new Exception( "Invalid character: "+ character );
				//#else
//# 					throw new Exception();
				//#endif
		}
		
		final DrawableImage control = new DrawableImage( PATH_IMAGES + "control.png" );
		control.defineReferencePixel( control.getSize().x >> 1, control.getSize().y );
		
		rightArm = new CharacterArm( character, CharacterArm.ARM_RIGHT );
		leftArm = new CharacterArm( character, CharacterArm.ARM_LEFT );

		setSize( ScreenManager.SCREEN_WIDTH, control.getSize().y + Math.max( rightArm.getSize().y, leftArm.getSize().y ) );
		
		control.setRefPixelPosition( size.x >> 1, size.y );
		leftArm.setPositionOffset( control.getPosition() );
		rightArm.setPositionOffset( control.getPosition() );
		
		final Point controlPosition = new Point( control.getPosition() );
		
		// insere o indicador do pr�ximo pedido
		final DrawableImage screen = new DrawableImage( PATH_IMAGES + "screen.png" );
		screen.setPosition( controlPosition.x + ORDER_INDICATOR_X_OFFSET, controlPosition.y + ORDER_INDICATOR_Y_OFFSET );
		
		screenPosition = screen.getPosition();
		
		insertDrawable( screen );
		insertDrawable( control );
		insertDrawable( rightArm );
		insertDrawable( leftArm );
	}
	
	
	public final void hit() {
		switch ( rightArm.getSequence() ) {
			case CharacterArm.SEQUENCE_STOPPED:
				rightArm.setSequence( CharacterArm.SEQUENCE_HIT );
				leftArm.setSequence( CharacterArm.SEQUENCE_HIT );				
			break;
		}
	}
	
	
}
