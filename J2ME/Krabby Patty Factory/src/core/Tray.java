/*
 * Tray.java
 *
 * Created on May 3, 2007, 8:11 PM
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import screens.PlayScreen;


/**
 *
 * @author Peter Hansen
 */
public final class Tray extends UpdatableGroup implements Constants {
	
	private static final byte LAST_TUTORIAL_LEVEL = 5;
    
	private static final byte MAX_SLOTS = 10;
    
    private static final byte TOTAL_DRAWABLES = MAX_SLOTS + 10;    
 
	private final Slot[] slots = new Slot[ MAX_SLOTS ];
    
    // tipos de ingredientes de todos os pedidos do n�vel
    private byte[] ingredients;
    
    // vari�veis utilizadas para fazer o controle dos ingredientes do n�vel
    private short firstIngredient;
    private short lastIngredient;
	
	// velocidade da esteira em pixels por segundo
	private int speed;    
    
    // resto do �ltimo c�lculo da varia��o de posi��o da esteira
    private int lastSpeedModule;
	
    private static short level;
    
	// refer�ncia para o indicador do pedido atual, para que a esteira possa sempre ter os ingredientes necess�rios para
	// se completar o pedido
	private final OrderQueue orderQueue;
	
	// �ndice do pr�ximo slot a ser testado. Caso o jogador deixe passar um ingrediente v�lido
	private int currentSlot;
	
	private final Pattern patternTray;
	private final Pattern patternGear;
	
	public final Pattern wallPattern;
	
	private final short topXOffset;
	
	private final short[] TRAY_INGREDIENT_Y;
	private final short TRAY_Y_DISTANCE;
	
	// �reas da mira. Para cada altura poss�vel dos ingredientes, h� 3 pontos de refer�ncia para facilitar os c�lculos
	// de colis�o e pontua��o: esquerda, meio e direita.
	private final int[][] targetArea = new int[ TRAY_TOTAL ][ 3 ];
	
	private final DrawableImage aimTop;
	private final DrawableImage aimBottom;
	private final Sprite aimLight;
	
	private boolean active;
	
	public final Control control;
	
	private final PlayScreen playScreen;
	
	private boolean moveSlots;
	
	// velocidade de frenagem da esteira (ap�s fim de jogo)
	private static final short STOP_SPEED = TRAY_SPEED_MIN / 5;
	private boolean stopping;
	private int lastStopModule;
			
	
    public Tray( byte character, PlayScreen playScreen, OrderQueue orderQueue ) throws Exception {
        super( TOTAL_DRAWABLES );
		
		if ( playScreen == null ) {
			//#if DEBUG == "true"
				throw new NullPointerException( "playScreen null." );
			//#else
//# 				throw new NullPointerException();
			//#endif
		}
		
		this.playScreen = playScreen;
		
		if ( orderQueue == null ) {
			//#if DEBUG == "true"
				throw new NullPointerException( "Indicador do pedido nulo." );	
			//#else
//# 				throw new NullPointerException();	
			//#endif
		}
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		this.orderQueue = orderQueue;
		
		// cria o pattern animado das engrenagens sob a esteira
		final Sprite gear = new Sprite( PATH_IMAGES + "roldana.dat", PATH_IMAGES + "roldana" );
		patternGear = new Pattern( gear );
		patternGear.setSize( size.x, gear.getSize().y );
		
		insertDrawable( patternGear );
		
		final DrawableImage imgWall = new DrawableImage( PATH_IMAGES + "wall.png" );
		wallPattern = new Pattern( imgWall );
		wallPattern.setSize( size.x, imgWall.getSize().y );
		insertDrawable( wallPattern );
		
		// cria a mira. Seu posicionamento correto � feito no m�todo setSize(), e sua inser��o no grupo � feita em etapas.
		aimTop = new DrawableImage( PATH_IMAGES + "target_top.png" );
		aimBottom = new DrawableImage( PATH_IMAGES + "target_bottom.png" );
		
		// cria o sprite do laser da mira
		aimLight = new Sprite( PATH_IMAGES + "light.dat", PATH_IMAGES + "light" );
		aimLight.defineReferencePixel( aimLight.getSize().x >> 1, aimLight.getSize().y >> 1 );
		
		// cria o pattern da esteira
		final DrawableImage tray = new DrawableImage( PATH_IMAGES + "esteira.png" );
		topXOffset = ( short ) -tray.getSize().x;
		
		patternTray = new Pattern( tray );
		patternTray.setPosition( 0, ScreenManager.SCREEN_HALF_HEIGHT );
		patternTray.setSize( size.x + tray.getSize().x, tray.getSize().y );
		insertDrawable( patternTray );
		
		// o posicionamento de alguns drawables � feito aqui pois s�o relativos � posi��o da esteira
		patternGear.setPosition( 0, patternTray.getPosition().y + patternTray.getSize().y );
		wallPattern.setPosition( 0, patternGear.getPosition().y + patternGear.getSize().y );

		// cria o manche e os bot�es
		control = new Control( character );
		control.defineReferencePixel( control.getSize().x >> 1, control.getSize().y );
		control.setRefPixelPosition( size.x >> 1, patternTray.getPosition().y + 2 );		
		insertDrawable( control );
		
		insertDrawable( aimTop );

		TRAY_INGREDIENT_Y = new short[ TRAY_TOTAL ];
		final int initialY = control.getPosition().y + control.getSize().y;
		TRAY_Y_DISTANCE = ( short ) ( tray.getSize().y / TRAY_TOTAL );
		TRAY_INGREDIENT_Y[ TRAY_TOP ]	 = ( short ) initialY;
		TRAY_INGREDIENT_Y[ TRAY_MIDDLE ] = ( short ) ( initialY + ( tray.getSize().y >> 1 ) );
		TRAY_INGREDIENT_Y[ TRAY_BOTTOM ] = ( short ) ( -1 + initialY + tray.getSize().y );
		
		
		// na vers�o simplificada, n�o h� anima��o da seta indicando a tecla a ser pressionada
		//#if LOW_JAR == "false"
		final Sprite slotArrow = new Sprite( PATH_TUTORIAL + "key.dat", PATH_TUTORIAL + "key" );
		//#else
//# 		final Sprite slotArrow = new Sprite( PATH_TUTORIAL + "key_low.dat", PATH_TUTORIAL + "key" );
		//#endif
		
		slotArrow.defineReferencePixel( slotArrow.getSize().x >> 1, slotArrow.getSize().y );
		
        for ( int i = 0; i < MAX_SLOTS; ++i ) {
            slots[ i ] = new Slot( slotArrow );
            insertDrawable( slots[ i ] );
        }
		
		insertDrawable( aimLight );
		insertDrawable( aimBottom );
		
		// calcula a posi��o da mira para cada posi��o vertical poss�vel do ingrediente
		final int offsetX = TRAY_Y_DISTANCE;
		final int baseX = ( size.x >> 1 ) + TRAY_TARGET_TOP_X_OFFSET;
		for ( int i = 0, targetX = baseX; i < TRAY_TOTAL; ++i, targetX += offsetX ) {
			targetArea[ i ][ 0 ] = ( targetX - ( ( TRAY_HIT_AREA_WIDTH << 1 ) / 3 ) ) - 3;
			targetArea[ i ][ 1 ] = targetX - 3;
			targetArea[ i ][ 2 ] = ( targetX + ( TRAY_HIT_AREA_WIDTH >> 1 ) ) - 3;
			
			if ( i == 1 ) {
				// posiciona as imagens da mira
				aimLight.setRefPixelPosition( targetX, patternTray.getPosition().y + ( patternTray.getSize().y >> 1 ) );
				aimTop.setPosition( aimLight.getPosition().x + TRAY_AIM_TOP_OFFSET_X, aimLight.getPosition().y + TRAY_AIM_TOP_OFFSET_Y );
				aimBottom.setPosition( aimLight.getPosition().x + TRAY_AIM_BOTTOM_OFFSET_X, aimLight.getPosition().y + TRAY_AIM_BOTTOM_OFFSET_Y );
			}
		} // fim for ( int i = 0, targetX = baseX; i < TRAY_TOTAL; ++i, targetX += offsetX )
    }
	
	
    /**
     * Prepara a esteira.
     * @param level n�vel para o qual a esteira ser� preparada.
     */
    public final void prepare( short level ) {
		Tray.level = level;
		
        // define a velocidade da esteira
		setSpeed( -( TRAY_SPEED_MIN + ( ( TRAY_SPEED_MAX - TRAY_SPEED_MIN ) * level / MAX_DIFFICULTY_LEVEL ) ) );
        
		ingredients = null;
        ingredients = orderQueue.getCurrentevelIngredients();
        
		currentSlot = 0;
		firstIngredient = 0;
		lastIngredient = 0;

		if ( level == 0 ) {
			moveSlots = true;
			
			setActive( false );
			
			prepareToNextOrders( false, true );

			for ( int i = 0; i < slots.length; ++i ) {
				final Slot s = slots[ i ];
				s.setTray( i % TRAY_TOTAL );
				s.setRefPixelPosition( s.getRefPixelX(), TRAY_INGREDIENT_Y[ s.getTray() ] );			
			}
		} else {
			moveSlots = false;
			
			// limpa as esteiras
			for ( int i = 0; i < slots.length; ++i )
				slots[ i ].setIngredient( null );
			
			prepareToNextOrders( false, false );
			
			if ( level < LAST_TUTORIAL_LEVEL )
				slots[ 0 ].setArrowVisible( true );
			
			setActive( true );
		}
    } // fim do m�todo prepare( int )
	
	
	public final void setSpeed( int speed ) {
        //#if DEBUG == "true"
        System.out.println( "tray speed: " + speed );
        //#endif
		
		this.speed = speed;
        
        lastSpeedModule = 0;        
		
        if ( speed < 0 ) {
            for ( int slotX = size.x + slots[ 0 ].getSize().x, i = 0; i < MAX_SLOTS; ++i ) {
                slots[ i ].setRefPixelPosition( slotX, slots[ i ].getRefPixelY() );
                slotX += getRandomDistance();
            }
        } else {
            for ( int slotX = -size.x, i = 0; i < MAX_SLOTS; ++i ) {
                slots[ i ].setRefPixelPosition( slotX, slots[ i ].getRefPixelY() );
                slotX -= getRandomDistance();
            }            
        }

		final Sprite s = ( Sprite ) patternGear.getFill();
		final short[][] frameTimes = s.getFrameTimes();
		final short FRAME_TIME = ( short ) ( speed == 0 ? 0 : TRAY_GEAR_FACTOR / Math.abs( speed ) );
		for ( byte i = 0; i < frameTimes.length; ++i ) {
			for ( byte j = 0; j < frameTimes.length; ++j ) {
				frameTimes[ i ][ j ] = FRAME_TIME;
			}
		}
	} // fim do m�todo setSpeed( int )
	
	
	/**
	 * Prepara a esteira para o(s) pr�ximo(s) pedido(s), garantindo que todos os ingredientes necess�rios existam na
	 * esteira.
	 */
	private final void prepareToNextOrders( boolean correct, boolean addArrows ) {
		if ( lastIngredient < ingredients.length || !correct ) {
			// obt�m os primeiros slots n�o-vis�veis de cada lado (a varredura � sempre feita crescentemente)
			
			if ( !correct )
				lastIngredient = firstIngredient;
			
			final byte lastIngredientType = ingredients[ lastIngredient ];
			
			// primeiro �ndice n�o-vis�vel da esteira (� direita da tela)
			int initialSlot = currentSlot;
			while ( slots[ initialSlot ].getPosition().x < size.x ) {
				++initialSlot;
				if ( initialSlot >= MAX_SLOTS )
					initialSlot = 0;
			}

			// �ltimo �ndice n�o-vis�vel da esteira (pode estar � direita ou � esquerda da tela)
			int finalSlot = currentSlot;
			while ( slots[ finalSlot ].getPosition().x <= slots[ currentSlot ].getPosition().x ) {
				--finalSlot;

				if ( finalSlot < 0 ) {
					finalSlot = MAX_SLOTS - 1;
					if ( currentSlot == 0 && slots[ finalSlot ].getPosition().x > slots[ currentSlot ].getPosition().x )
						break;
				}
			} // fim while ( slots[ finalSlot ].getPosition().x <= slots[ currentSlot ].getPosition().x )
			
			//#if DEBUG == "true"
//			for ( int i = 0; i < slots.length; ++i ) {
//				if ( i == initialSlot )
//					System.out.print( "i-->" );
//				if ( i == finalSlot )
//					System.out.print( "f-->" );
//				if ( i == currentSlot )
//					System.out.print( "c-->" );
//				
//				System.out.println( i + ": " + slots[ i ].getPosition().x + 
//						( ( slots[i].getIngredient() == null ) ? " null" : ( " tipo " + slots[i].getIngredient().getType() ) ) );
//			}            
			//#endif
			
			// insere os ingredientes na esteira
			for ( int slotIndex = initialSlot; ; ) {
                if ( correct ) {
					if ( slots[ slotIndex ].getIngredient() == null )
						addIngredient( slots[ slotIndex ], ingredients[ lastIngredient ] );
					else
						break;
                } else {
                    if ( slots[ slotIndex ].getIngredient() == null || slots[ slotIndex ].getIngredient().getType() != ingredients[ lastIngredient ] ) {
						final int previousIndex = ( slotIndex + slots.length - 1 ) % slots.length;
						final Slot previousSlot = slots[ previousIndex ];
						
						// evita que apare�am 2 ingredientes iguais seguidos, o que confunde o jogador
						if ( previousSlot.getIngredient() == null || previousIndex == currentSlot || previousSlot.getIngredient().getType() != ingredients[ lastIngredient ] )
							addIngredient( slots[ slotIndex ], ingredients[ lastIngredient ] );
					}	
                }
				++lastIngredient;

                if ( slotIndex == finalSlot || lastIngredient >= ingredients.length )
                    break;
                else if ( ++slotIndex >= MAX_SLOTS )
					slotIndex = 0;
			} // fim for ( int slotIndex = initialSlot; ; )
			
			// caso o jogador tenha errado a �ltima jogada, refor�a a indica��o de qual � o pr�ximo ingrediente correto.
			if ( !correct && addArrows ) {
				for ( int i = ( currentSlot + 1 ) % slots.length; i != finalSlot; i = ( i + 1 ) % slots.length ) {
					if ( slots[ i ].getIngredient() != null && slots[ i ].getIngredient().getType() == lastIngredientType && !stopping ) {
						slots[ i ].setArrowVisible( true );
						break;
					}
				} 
			} // fim if ( !correct )
		} // fim if ( lastIngredient < ingredients.length || !correct )
	} // fim prepareToNextOrders()

    
    public final void update( int delta ) {
		if ( active ) {
			super.update( delta );
			
			if ( stopping ) {
				final int ds = lastStopModule + ( STOP_SPEED * delta );
				final int dx = ds / 1000;
				lastStopModule = ds % 1000;
				
				if ( dx != 0 ) {
					speed += dx;

					if ( speed >= 0 )
						active = false;
				}
			}

			final int ds = lastSpeedModule + ( speed * delta );
			final int dx = ds / 1000;
			lastSpeedModule = ds % 1000;

			if ( dx != 0 ) {
				// move a esteira
				patternTray.move( dx, 0 );
				if ( patternTray.getRefPixelX() <= topXOffset )
					patternTray.move( -topXOffset, 0 );
					
				// no in�cio de cada n�vel, a esteira � animada, mas os ingredientes s� passam ap�s a mensagem indicando o 
				// n�vel atual
				if ( moveSlots ) {
					for ( int i = 0; i < MAX_SLOTS; ++i ) {
						final Slot slot = slots[ i ];
						slot.move( dx, 0 );

						if ( i == currentSlot ) {
							if ( level == 0 ) {
								// modo tutorial; p�ra a esteira no momento em que o ingrediente est� na posi��o correta
								if ( slot.getRefPixelX() <= targetArea[ slot.getTray() ][ TRAY_TARGET_INDEX_CENTER ] ) {
									if ( slot.getIngredient() != null ) {
										setActive( false );
										slot.setArrowVisible( true );
									}
								}
							} else {
								// modo normal de jogo
								if ( ( slot.getPosition().x + slot.getSize().x ) <= targetArea[ slot.getTray() ][ TRAY_TARGET_INDEX_LEFT ] ) {
									final Ingredient ingredient = slot.getIngredient();
									if ( ingredient != null && ingredient.getType() == orderQueue.getCurrentIngredient() ) {
										 // jogador deixou passar ingrediente v�lido; ajusta esteira para que sequ�ncia volte a ficar correta
										slot.setArrowVisible( false );
										prepareToNextOrders( false, true ); // jogador deixou passar ingrediente v�lido

										playScreen.missedIngredient();
									} // fim if ( ingredient != null && ingredient.getType() == orderQueue.getCurrentIngredient() )

									if ( ++currentSlot >= MAX_SLOTS )
										currentSlot = 0;

									if ( level < LAST_TUTORIAL_LEVEL && !stopping )
										slots[ currentSlot ].setArrowVisible( true );
								} // fim if ( slot.getRefPixelX() <= targetArea[ slot.getTray() ][ TRAY_TARGET_INDEX_LEFT ] )
							}
						} // fim if ( i == currentSlot )

						// testa se a bandeja saiu da esteira
						// bandeja se move para a esquerda
						if ( slot.getRefPixelX() <= -slot.getSize().x ) {
							// bandeja saiu da esteira; a reposiciona
							final Slot previousSlot = slots[ ( MAX_SLOTS + i - 1 ) % MAX_SLOTS ];

							final int tray = NanoMath.randInt( TRAY_TOTAL );
							slot.setTray( tray );

							slot.setRefPixelPosition( previousSlot.getRefPixelX() + getRandomDistance() + ( TRAY_Y_DISTANCE * tray ), slot.getRefPixelY() );
							slot.setIngredient( null );

							prepareToNextOrders( true, true ); // bandeja saiu da esteira
						} // fim if ( slot.getRefPixelX() <= -slot.getSize().x )
					} // fim for ( int i = 0; i < MAX_SLOTS; ++i )
				} // fim if ( moveSlots )
			} // fim if ( dx != 0 )
		} else {
			// else => !active
			slots[ currentSlot ].update( delta );
		}
    } // fim do m�todo update( int )
    
    
    /**
     * M�todo auxiliar utilizado para obter uma nova dist�ncia semi-aleat�ria entre as bandejas, considerando o n�vel atual
     * do jogo (quanto maior o n�vel, menor a dist�ncia m�dia entre as bandejas).
     * @return dist�ncia para a pr�xima bandeja.
     */
    private final int getRandomDistance() {
		final int MIN_DISTANCE = TRAY_SLOT_DISTANCE_INITIAL_MIN + ( TRAY_SLOT_DISTANCE_FINAL_MIN - TRAY_SLOT_DISTANCE_INITIAL_MIN ) * level / MAX_DIFFICULTY_LEVEL;
		final int MAX_DISTANCE = TRAY_SLOT_DISTANCE_INITIAL_MAX + ( TRAY_SLOT_DISTANCE_FINAL_MAX - TRAY_SLOT_DISTANCE_INITIAL_MAX ) * level / MAX_DIFFICULTY_LEVEL;
		
        return MAX_DISTANCE - ( NanoMath.randInt( MAX_DISTANCE - MIN_DISTANCE ) * level / MAX_DIFFICULTY_LEVEL );
    }
	
	
	public final Ingredient removeIngredientAtTray( byte tray ) {
		// verifica se o ingrediente atual est� na mira
		final Slot s = slots[ currentSlot ];
		if ( tray == s.getTray() && targetArea[ tray ][ TRAY_TARGET_INDEX_RIGHT ] >= s.getPosition().x && targetArea[ tray ][ TRAY_TARGET_INDEX_LEFT ] <= ( s.getPosition().x + s.getSize().x ) ) {
			// a bandeja intercepta a mira
			final Ingredient ingredient = s.getIngredient();
			if ( ingredient != null ) {
				// a bandeja possui um ingrediente; o posiciona na coordenada global e o remove do slot
				ingredient.move( s.getPosition().x + position.x, s.getPosition().y + position.y );

				final int COLLISION_LIMIT = ( ingredient.getSize().x + TRAY_HIT_AREA_WIDTH ) >> 1;
				
				int d = s.getRefPixelX() - targetArea[ tray ][ TRAY_TARGET_INDEX_CENTER ];
				if ( Math.abs( d ) < TRAY_BULLS_EYE_OFFSET ) {
					d = 0;
				} else {
					if ( d < -COLLISION_LIMIT )
						d = -COLLISION_LIMIT;
					else if ( d > COLLISION_LIMIT )
						d = COLLISION_LIMIT;
				}
				// ( ( ingredient.getSize().x + TRAY_HIT_AREA_WIDTH ) >> 1 ) � a dist�ncia m�xima v�lida para detec��o
				// de colis�o
				int precision = d * 50 / COLLISION_LIMIT;
				
				ingredient.setPrecision( precision );

				//#if DEBUG == "true"
				System.out.println( "precis�o: " + precision + ", d: " + d );
				//#endif

				s.setIngredient( null );
				
				// incrementa o �ndice do primeiro ingrediente da lista restante a inserir
				final short previousIngredient = firstIngredient;
				++firstIngredient;
				if ( previousIngredient < ingredients.length && ingredient.getType() != ingredients[ previousIngredient ] && firstIngredient < ingredients.length )
					prepareToNextOrders( false, true ); // jogador adicionou ingrediente inv�lido

				if ( !active ) {
					if ( ++currentSlot >= MAX_SLOTS )
						currentSlot = 0;					
					
					setActive( true );
				}
			} // fim if ( ingredient != null )
			
			return ingredient;
		} // fim if ( limitLeft <= ( s.getPosition().x + s.getSize().x ) && limitRight >= s.getPosition().x )
		return null;
	} // fim do m�todo removeIngredientAtTray()
	
	
	/**
	 * Adiciona um novo ingrediente de forma semi-aleat�ria, levando em considera��o o pedido atual, de forma que sempre
	 * haja ingredientes necess�rios para que o jogador monte corretamente o pedido.
	 * @param slot bandeja onde ser� inserido o novo ingrediente.
	 */
	private final void addIngredient( Slot slot, int ingredientType ) {
		if ( ingredientType < 0 )
			ingredientType = NanoMath.randInt( Ingredient.TOTAL_INGREDIENT_TYPES );
		
		try {
			final Ingredient i = new Ingredient( ingredientType );
			slot.setIngredient( i );
			
			slot.setRefPixelPosition( slot.getRefPixelX(), TRAY_INGREDIENT_Y[ slot.getTray() ] );			
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}			
	} // fim do m�todo addIngredient()
	
	
	public final Point[] getTargetOffset() {
		return new Point[] { new Point( 0, TRAY_INGREDIENT_Y[ 0 ] + ( TRAY_Y_DISTANCE * 3 / 2 ) ), 
							 new Point( TRAY_Y_DISTANCE, TRAY_INGREDIENT_Y[ 1 ] + TRAY_Y_DISTANCE ), 
							 new Point( TRAY_Y_DISTANCE << 1, TRAY_INGREDIENT_Y[ 2 ] + ( TRAY_Y_DISTANCE >> 1 ) ) 
							};
	}
	
	
	public final void hit() {
		control.hit();
	}
	
	
	public final void setActive( boolean active ) {
		this.active = active;
	}
	
	
	public final boolean isActive() {
		return active;
	}
	
	
	public final int getPattyLimit() {
		return aimBottom.getPosition().y + aimBottom.getSize().y;
	}

	
	public final void setMoveSlots( boolean moveSlots ) {
		this.moveSlots = moveSlots;
	}

	
	public final void setStop( boolean stop ) {
		stopping = stop;
		if ( stopping ) {
			for ( int i = 0; i < slots.length; ++i )
				slots[ i ].setArrowVisible( false );
		}
	}
	
	
	/**
	 * Retorna a posi��o y da esteira mais baixa, a ser passada � classe MrKrabs, de forma que o texto de tutorial n�o
	 * oculte a esteira.
	 * @return 
	 */
	public final int getBottomPosition() {
		return patternGear.getPosition().y + 3;
	}

	
	//#if SCREEN_SIZE != "SMALL"

	public final int getTrayAt( int y ) {
		if ( super.contains( position.x, y ) ) {
			if ( y >= TRAY_INGREDIENT_Y[ TRAY_TOP ] && y <= TRAY_INGREDIENT_Y[ TRAY_BOTTOM ] ) {
				return ( y - TRAY_INGREDIENT_Y[ TRAY_TOP ] ) / TRAY_Y_DISTANCE;
			}
		}
		
		return -1;
	}
	
	//#endif
}
 
