/*
 * Slot.java
 *
 * Created on May 4, 2007, 4:58 PM
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;


/**
 *
 * @author peter
 */
public final class Slot extends UpdatableGroup implements Constants {
	
	private Ingredient ingredient;
	
	// �ndice da esteira onde o ingrediente passou	
	private byte tray;	
	
	private boolean arrowVisible;
	
	// armazena a refer�ncia para o �ltimo slot com a seta, pois s� pode haver um de cada vez
	private static Slot slotWithArrow;
	
	private final Sprite arrow;
	
	
	/** Creates a new instance of Slot */
	public Slot( Sprite arrow ) throws Exception {
		super( 2 );

		this.arrow = arrow;
		
		setSize( 0, ScreenManager.SCREEN_HALF_HEIGHT );
	}
	
	
	public final void setIngredient( Ingredient i ) {
		ingredient = i;
		arrowVisible = false;

		removeDrawable( 0 );
		removeDrawable( 0 );
		if ( i != null ) {
			setVisible( true );
			setSize( i.getSize().x, size.y );
			
			updateReferencePixel();
			
			insertDrawable( i );
		} else {
			setVisible( false );
        }
	} // fim do m�todo setIngredient( Ingredient )
	
	
	public final Ingredient getIngredient() {
		return ingredient;
	}
	
	
	public final byte getTray() {
		return tray;
	}
	
	
	public final void setTray( int tray ) {
		this.tray = ( byte ) tray;
		
		updateReferencePixel();
	}
	
	
	private final void updateReferencePixel() {
		final Point ingredientSize = ( ingredient == null ) ? new Point() : ingredient.getSize();
		
		switch ( tray ) {
			case TRAY_TOP:
				defineReferencePixel( size.x >> 1, size.y - ingredientSize.y );
				
				if ( ingredient != null )
					ingredient.setRefPixelPosition( referencePixel.x, referencePixel.y + ingredient.getCurrentFrameImage().getHeight() );
			break;

			case TRAY_MIDDLE:
				defineReferencePixel( size.x >> 1, size.y - ( ingredientSize.y >> 1 ) );

				if ( ingredient != null )
					ingredient.setRefPixelPosition( referencePixel.x, referencePixel.y + ( ingredient.getCurrentFrameImage().getHeight() >> 1 ) );
			break;

			case TRAY_BOTTOM:
				defineReferencePixel( size.x >> 1, size.y );
				
				if ( ingredient != null )
					ingredient.setRefPixelPosition( referencePixel );				
			break;
		}
	}
    
	
	protected final void setArrowVisible( boolean visible ) {
		arrowVisible = visible;
		
		if ( visible ) {
			slotWithArrow = this;

			if ( ingredient != null ) {
				insertDrawable( arrow );
				arrow.setSequence( tray );
				arrow.setRefPixelPosition( size.x >> 1, ingredient.getPosition().y + ( ingredient.getHeight() >> 1 ) );
			}
		} else {
			slotWithArrow = null;
			
			if ( ingredient == null )
				removeDrawable( 0 );
			else
				removeDrawable( 1 );
		}
	}

	
	public final void update( int delta ) {
		if ( arrowVisible ) {
			if ( slotWithArrow == this ) {
				super.update( delta );
				
				if ( getRefPixelX() < 0 )
					slotWithArrow = null;
			} else {
				setArrowVisible( false );
			}
		} // fim if ( arrowVisible )
	} // fim do m�todo update( int )

	
}
