/*
 * PattyGroup.java
 *
 * Created on May 15, 2007, 6:40 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;

/**
 *
 * @author peter
 */
public final class PattyGroup extends UpdatableGroup implements Constants {
	
	private static final byte MAX_ITEMS = 3;
	
	private static final byte INDEX_BIG_TRAY	= 0;
	private static final byte INDEX_OLD_PATTY	= 0;
	private static final byte INDEX_NEW_PATTY	= 1;
	
	private boolean moving;
	
	private int lastSpeedModule;
	
	private int limit;
	
	
	/** Creates a new instance of PattyGroup */
	public PattyGroup() throws Exception {
		super( MAX_ITEMS );
	}

	
	public final void update( int delta ) {
		// s� atualiza o hamb�rguer antigo
		if ( updatables[ INDEX_OLD_PATTY ] != null )
			updatables[ INDEX_OLD_PATTY ].update( delta );
		
		if ( moving ) {
			// move os hamburguers
			final int ds = lastSpeedModule + ( BIG_TRAY_MOVE_SPEED * delta );
			final int dx = ds / 1000;
			lastSpeedModule = ds % 1000;

			switch ( activeDrawables ) {
				case 1:
					// 1 hamb�rguer ativo
					drawables[ INDEX_OLD_PATTY ].move( dx, 0 );

					if ( drawables[ INDEX_OLD_PATTY ].getRefPixelX() <= limit ) {
						drawables[ INDEX_OLD_PATTY ].setRefPixelPosition( limit, size.y );
						moving = false;
						if ( limit < 0 )
							removeDrawable( INDEX_OLD_PATTY );
					}					
				break;
				
				case 2:
					// 2 hamb�rguers ativos
					drawables[ INDEX_OLD_PATTY ].move( dx, 0 );
					drawables[ INDEX_NEW_PATTY ].move( dx, 0 );
					
					if ( drawables[ INDEX_NEW_PATTY ].getRefPixelX() <= limit ) {
						drawables[ INDEX_NEW_PATTY ].setRefPixelPosition( limit, size.y );
						removeDrawable( INDEX_OLD_PATTY );
						moving = false;
					}					
				break;
			}
		}
	} // fim do m�todo update( int )
	
	
	public final Patty getCurrentPatty() {
		switch ( activeDrawables ) {
			case 1:
				// 1 hamb�rguer ativo
				return ( Patty ) drawables[ INDEX_OLD_PATTY ];				

			case 2:
				// 2 hamb�rguers ativos
				return ( Patty ) drawables[ INDEX_NEW_PATTY ];

			default:
				return null;
		}
	}

	
	public final short insertDrawable( Drawable drawable ) {
		final short index = super.insertDrawable( drawable );
		
		// inseriu um novo hamb�rguer (pedido anterior foi completado)
		lastSpeedModule = 0;
		moving = true;
		limit = size.x >> 1;
		drawable.setRefPixelPosition( size.x + ( drawable.getSize().x >> 1 ), size.y );
		
		return index;
	}
	
	
	public final void setMoving( boolean moving ) {
		this.moving = moving;
		limit = -size.x >> 1;
	}
	
}
