/*
 * Patty.java
 *
 * Created on May 3, 2007, 8:11 PM
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import javax.microedition.lcdui.Graphics;
import screens.PlayScreen;

                     
/**
 *
 * @author Peter Hansen
 */
public final class Patty extends UpdatableGroup implements Constants {
	
	private final Ingredient[] fallingIngredients = new Ingredient[ ORDER_MAX_SIZE ];
	private byte falling;
	
	// posi��o y considerada para colis�o (do ingrediente do topo)
	private int topIngredientY;
	
	// varia��o na posi��o y do hamb�rguer, de forma que ele jamais cubra a esteira
	private int currentOffsetY;
	private int finalOffsetY;
	
	private int lastSpeedModule;
			
	private final PlayScreen playScreen;
	
	private boolean complete;
	
	
	public Patty( PlayScreen playScreen, int numberOfIngredient ) throws Exception {
		super( numberOfIngredient + 1 );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		defineReferencePixel( size.x >> 1, size.y );
		
		if ( playScreen == null ) {
			//#if DEBUG == "true"
				throw new NullPointerException( "playScreen null." );
			//#else
//# 				throw new NullPointerException();
			//#endif
		}
		
		this.playScreen = playScreen;
		
		final DrawableImage platform = new DrawableImage( PATH_IMAGES + "platform.png" );
		platform.defineReferencePixel( platform.getSize().x >> 1, platform.getSize().y );
		platform.setRefPixelPosition( size.x >> 1, size.y );
		insertDrawable( platform );
		
		topIngredientY = size.y;
	}
 
	
	/**
	 * Insere um ingrediente no hamburguer.
	 * 
	 * @param ingredient ingrediente a ser inserido.
	 */
	public final boolean addIngredient( Ingredient ingredient ) {
		if ( !complete && insertDrawable( ingredient ) > 0 ) {
			// ainda n�o realiza a troca do hamb�rguer vis�vel na tela, pois ainda h� pelo menos um ingrediente caindo
			if ( activeDrawables >= drawables.length )
				complete = true;
			
			ingredient.setDirection( Ingredient.DIRECTION_NONE );
			
			// a posi��o do ingrediente varia de acordo com a precis�o da jogada; na pior das hip�teses, a dist�ncia entre
			// o centro do ingrediente e o centro do hamb�rguer � igual a 2/3 da largura do ingrediente.
			ingredient.setRefPixelPosition( ( size.x >> 1 ) + ( ingredient.getCurrentFrameImage().getWidth() * ingredient.getPrecision() / 75 ), ingredient.getRefPixelY() );

			fallingIngredients[ falling ] = ingredient;
			++falling;

			return true;
		} // fim if ( !complete && insertDrawable( ingredient ) > 0 )
		
		return false;
	} // fim do m�todo addIngredient( Ingredient )
	
	
	public final void update( int delta ) {
		if ( getRefPixelX() <= ScreenManager.SCREEN_HALF_WIDTH ) {
			super.update( delta );

			if ( currentOffsetY != finalOffsetY ) {
				final boolean increase = currentOffsetY < finalOffsetY;
				final int ds = lastSpeedModule + ( ( increase ? PATTY_VERTICAL_SPEED : -PATTY_VERTICAL_SPEED ) * delta );
				int dy = ds / 1000;
				lastSpeedModule = ds % 1000;

				if ( dy != 0 ) {
					if ( increase ) {
						if ( currentOffsetY + dy > finalOffsetY )
							dy = finalOffsetY - currentOffsetY;
					} else {
						if ( currentOffsetY + dy < finalOffsetY )
							dy = finalOffsetY - currentOffsetY;
					}

					currentOffsetY += dy;
					topIngredientY += dy;

					// move todos os elementos do grupo, mas mant�m a posi��o anterior do grupo em si, de forma a n�o causar
					// erros de redesenho nos ingredientes caindo
					for ( int i = 0; i < activeDrawables; ++i ) {
						drawables[ i ].move( 0, dy );
					}

					if ( currentOffsetY == finalOffsetY )
						lastSpeedModule = 0;
				} // fim if ( dy != 0 )
			} // fim if ( currentOffsetY != finalOffsetY )

			Ingredient lastIngredient = fallingIngredients[ 0 ];
			if ( lastIngredient != null && lastIngredient.getDirection() != Ingredient.DIRECTION_DOWN )
				lastIngredient.setDirection( Ingredient.DIRECTION_DOWN );
			
			int yTop = topIngredientY + currentOffsetY;
			for ( int i = 0; i < falling; ) {
				final Ingredient ingredient = fallingIngredients[ i ];
				
				if ( ingredient.getDirection() != Ingredient.DIRECTION_DOWN ) {
					if ( lastIngredient.getPosition().y >= 0 )
						ingredient.positionAfter( lastIngredient );
					else
						break;
				}

				lastIngredient = ingredient;

				if ( ingredient.getRefPixelY() >= yTop ) {
					// ingrediente se achata ao colidir com o hamb�rguer
					ingredient.setSequence( Ingredient.SEQUENCE_PATTY_HIT );
					ingredient.setDirection( Ingredient.DIRECTION_NONE );

					// atualiza o array de ingredientes caindo
					for ( int j = i; j < fallingIngredients.length - 1; ++j )
						fallingIngredients[ j ] = fallingIngredients[ j + 1 ];
					fallingIngredients[ falling ] = null;
					--falling;
					
					ingredient.setRefPixelPosition( ingredient.getRefPixelX(), yTop );					

					topIngredientY -= Ingredient.PATTY_Y_OFFSET[ ingredient.getType() ];
					yTop -= Ingredient.PATTY_Y_OFFSET[ ingredient.getType() ];

					if ( yTop <= playScreen.tray.getPattyLimit() )
						finalOffsetY += playScreen.tray.getPattyLimit() - yTop ;

					playScreen.ingredientInserted( ingredient );

					// caso tenha sido adicionada a parte superior do p�o, realiza a troca do hamb�rguer vis�vel
					if ( i == falling && complete ) {
						playScreen.changePatty();
						finalOffsetY = 0;
					}

					// n�o incrementa i
					continue;
				} // fim if ( ingredient.getRefPixelY() >= topIngredientY )
				++i;
			} // fim for ( int i = 0; i < falling; ++i )
		} // fim if ( getRefPixelX() <= ScreenManager.SCREEN_HALF_WIDTH )
	} // fim do m�todo update( int )

}
 
