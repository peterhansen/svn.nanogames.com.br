package core;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface CoreConstants {
	//em pixels
	public static final int ROOM_WIDTH = 1000;
	public static final long MILISECOND = 1;
	public static final long MILISECONDS = MILISECOND;
	public static final long SECOND = 1000 * MILISECONDS;
	public static final long SECONDS = SECOND;

	public static final String DATABASE_NAME = "K";
	public static final byte GAME_DATA_SLOT = 2;
	public static final byte MAX_DEPRESSION_COUNT = 3;
	public static final byte AMOUNT_MESSAGE_PER_MOOD = 3;
	
	
	public static final String PATH_TEXTS = "/texts/";

//<editor-folder desc = "indices dos textos" >
	public static final int TEXT_TITLE = 0;

	public static final int TEXT_NEED_SLEEP_1 = TEXT_TITLE + 1;
	public static final int TEXT_NEED_SLEEP_2 = TEXT_NEED_SLEEP_1 + 1;
	public static final int TEXT_NEED_SLEEP_3 = TEXT_NEED_SLEEP_2 + 1;
	public static final int TEXT_NEED_FOOD_1 = TEXT_NEED_SLEEP_3 + 1;
	public static final int TEXT_NEED_FOOD_2 = TEXT_NEED_FOOD_1 + 1;
	public static final int TEXT_NEED_FOOD_3 = TEXT_NEED_FOOD_2 + 1;
	public static final int TEXT_NEED_BUY_1 = TEXT_NEED_FOOD_3 + 1;
	public static final int TEXT_NEED_BUY_2 = TEXT_NEED_BUY_1 + 1;
	public static final int TEXT_NEED_BUY_3 = TEXT_NEED_BUY_2 + 1;
	public static final int TEXT_NEED_CLEANESS_1 = TEXT_NEED_BUY_3 + 1;
	public static final int TEXT_NEED_CLEANESS_2 = TEXT_NEED_CLEANESS_1 + 1;
	public static final int TEXT_NEED_CLEANESS_3 = TEXT_NEED_CLEANESS_2 + 1;
	public static final int TEXT_NEED_FUN_1 = TEXT_NEED_CLEANESS_3 + 1;
	public static final int TEXT_NEED_FUN_2 = TEXT_NEED_FUN_1 + 1;
	public static final int TEXT_NEED_FUN_3 = TEXT_NEED_FUN_2 + 1;
	public static final int TEXT_ACT_OUT_1 = TEXT_NEED_FUN_3 + 1;
	public static final int TEXT_ACT_OUT_2 = TEXT_ACT_OUT_1 + 1;
	public static final int TEXT_ACT_OUT_3 = TEXT_ACT_OUT_2 + 1;
	public static final int TEXT_ACT_ATTENTION_1 = TEXT_ACT_OUT_3 + 1;
	public static final int TEXT_ACT_ATTENTION_2 = TEXT_ACT_ATTENTION_1 + 1;
	public static final int TEXT_ACT_ATTENTION_3 = TEXT_ACT_ATTENTION_2 + 1;
	public static final int TEXT_ACT_EAT_1 = TEXT_ACT_ATTENTION_3 + 1;
	public static final int TEXT_ACT_EAT_2 = TEXT_ACT_EAT_1 + 1;
	public static final int TEXT_ACT_EAT_3 = TEXT_ACT_EAT_2 + 1;
	public static final int TEXT_ACT_SLEEP_1 = TEXT_ACT_EAT_3 + 1;
	public static final int TEXT_ACT_SLEEP_2 = TEXT_ACT_SLEEP_1 + 1;
	public static final int TEXT_ACT_SLEEP_3 = TEXT_ACT_SLEEP_2 + 1;
	public static final int TEXT_ACT_BUY_1 = TEXT_ACT_SLEEP_3 + 1;
	public static final int TEXT_ACT_BUY_2 = TEXT_ACT_BUY_1 + 1;
	public static final int TEXT_ACT_BUY_3 = TEXT_ACT_BUY_2 + 1;
	public static final int TEXT_ACT_PLAY_1 = TEXT_ACT_BUY_3 + 1;
	public static final int TEXT_ACT_PLAY_2 = TEXT_ACT_PLAY_1 + 1;
	public static final int TEXT_ACT_PLAY_3 = TEXT_ACT_PLAY_2 + 1;
	public static final int TEXT_ACT_STILL_1 = TEXT_ACT_PLAY_3 + 1;
	public static final int TEXT_ACT_STILL_2 = TEXT_ACT_STILL_1 + 1;
	public static final int TEXT_ACT_STILL_3 = TEXT_ACT_STILL_2 + 1;
	public static final int TEXT_ACT_BATH_1 = TEXT_ACT_STILL_3 + 1;
	public static final int TEXT_ACT_BATH_2 = TEXT_ACT_BATH_1 + 1;
	public static final int TEXT_ACT_BATH_3 = TEXT_ACT_BATH_2 + 1;
	public static final int TEXT_ACTIVITY_SLEEP = TEXT_ACT_BATH_3 + 1;
	public static final int TEXT_ACTIVITY_EAT = TEXT_ACTIVITY_SLEEP + 1;
	public static final int TEXT_ACTIVITY_BUY = TEXT_ACTIVITY_EAT + 1;
	public static final int TEXT_ACTIVITY_PLAY = TEXT_ACTIVITY_BUY + 1;
	public static final int TEXT_ACTIVITY_PAINT = TEXT_ACTIVITY_PLAY + 1;
	public static final int TEXT_ACTIVITY_READ = TEXT_ACTIVITY_PAINT + 1;
	public static final int TEXT_ACTIVITY_WEAR = TEXT_ACTIVITY_READ + 1;
	public static final int TEXT_ACTIVITY_BATH = TEXT_ACTIVITY_WEAR + 1;
	public static final int TEXTS_TOTAL = TEXT_ACTIVITY_BATH + 1;

	

	public static final byte MAX_NEED_TEXT_INDEX = TEXT_NEED_FUN_3;
	public static final byte NO_MESSAGE = -1;
	public static final byte MESSAGE_TYPE_THINK = 0;
//</editor-fold>
}

