package graphics;

import core.CoreConstants;
import graphics.sceneelements.HelloKitty;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface GraphicalConstants {

	//telas de jogo
	public static final int SCREEN_NANO_SPLASH = 0;
	public static final int SCREEN_GAMESCREEN = 1;

	//caminhos
	public static final String PATH_IMAGES = "/images/";
	public static final String PATH_OBJECT_IMAGES = PATH_IMAGES + "objs/";
	public static final String PATH_MOODS = PATH_IMAGES + "mood/";

	//constantes de interface
	public static final byte DEFAULT_CAMERA_HORIZONTAL_SPEED = HelloKitty.WALKSPEED;

	public static final byte FONTS_TOTAL = 3;
	public static final byte DEFAULT_FONT_OFFSET = -5;
	public static final byte FONT_MESSAGE_BOX = 0;
	public static final byte FONT_ACTIVITY_SELECTION = 1;
	public static final byte FONT_ACTIVITY_SELECTED = 2;
	public static final long DEFAULT_BOX_VISIBLE_TIME = 5 * CoreConstants.SECONDS;
	public static final long DEFAULT_TALK_INTERVAL = 10 * CoreConstants.SECONDS;
	public static final byte DEFAULT_SPACE_FROM_WALL = 25;
}
