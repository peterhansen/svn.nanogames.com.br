package graphics.screens;

import graphics.sceneelements.ActivityListener;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import br.com.nanogames.hellokitty.GameMIDlet;
import core.CoreConstants;
import graphics.GraphicalConstants;
import graphics.sceneelements.Bed;
import graphics.sceneelements.CustomizableObject;
import graphics.sceneelements.Desk;
import graphics.sceneelements.Easel;
import graphics.sceneelements.Floor;
import graphics.sceneelements.FocusRequestListener;
import graphics.sceneelements.HelloKitty;
import graphics.sceneelements.Wall;
import graphics.sceneelements.Wardrobe;
import graphics.sceneelements.Window;
import graphics.userinterface.ActivityPopUp;
import graphics.userinterface.ActivitySelectionListener;
import graphics.userinterface.ActivitySelectionWidget;
import graphics.userinterface.HelloKittyMessage;
import graphics.userinterface.HorizontalLayoutUpdatableGroup;
import graphics.userinterface.VerticalLayoutUpdatableGroup;
import graphics.userinterface.VisibilityListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Vector;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public final class GameScreen extends UpdatableGroup implements GraphicalConstants, CoreConstants, KeyListener, ActivitySelectionListener,
		Serializable, VisibilityListener, FocusRequestListener, ActivityListener
//#if TOUCH == "true"
//# 		,PointerListener
//#endif
{
	
	public static final int TOTAL_ITEMS = 30;
	public static final int MAX_ROOM_OBJECTS = 20;
	public static final int GROUND_LEVEL_FAR = 3 * ScreenManager.SCREEN_HEIGHT / 5;
	public static final int GROUND_LEVEL_NEAR = GROUND_LEVEL_FAR +  ScreenManager.SCREEN_HEIGHT / 5;

	private static GameScreen instance = null;
	private ActivityPopUp popup;
	//objetos de cenário
	private Floor floor;
	private Wall wall;
	private UpdatableGroup roomObjects;
	private UpdatableGroup wallDecorations;
	//private Door door;
	private HelloKitty helloKitty;
	private Bed bed;
	private Window window;
	private Desk desk;
	private Easel easel;
	private Wardrobe wardrobe;
	
	//objetos de interface
	private Pattern background;
	private Point cameraSpeed;
	private Point cameraPosition;
	private CustomizableObject cameraTarget;
	private ActivitySelectionWidget activities;
	private long hoveredTime;
	private Vector roomObjectList;
	private Vector wallDecorationList;
	private HelloKittyMessage kittyMessage;
	private DrawableImage statusBar;
	private DrawableImage statusBarFill;

	/***
	* Construtor - apenas instancia e insere. Não define tamanho nem estado inicial
	 */
	public GameScreen() {

		super( TOTAL_ITEMS  );
		try {
			roomObjectList = new Vector();
			wallDecorationList = new Vector();
			background = new Pattern( 0x0000FF );
			cameraSpeed = new Point();
			cameraPosition = new Point();
			wall = new Wall();
			floor = new Floor();
			helloKitty = new HelloKitty( cameraPosition, this, this );
			helloKitty.setRoomPosition( ROOM_WIDTH / 2, GROUND_LEVEL_NEAR );
			helloKitty.moveTo( 100 );
			//monta a display list:
			//fundo
			insertDrawable( background );
			//a sala
			insertDrawable( wall );
			insertDrawable( floor );
			//objetos na sala
			roomObjects = new UpdatableGroup( MAX_ROOM_OBJECTS );
			insertDrawable( roomObjects );
			wallDecorations = new UpdatableGroup( MAX_ROOM_OBJECTS );
			insertDrawable( wallDecorations );

			bed = new Bed();

			addRoomElementAt( bed, new Point( 0, GROUND_LEVEL_FAR ) );
			/*
			door = new Door();
			addWallDecorationAt( door,
								 new Point( ( 2 * ROOM_WIDTH ) / 8, GROUND_LEVEL_FAR - door.getHeight() ) );
			 * 
			 */
			window = new Window();
			addWallDecorationAt( window,
								 new Point( ( 1 * ROOM_WIDTH ) / 8, GROUND_LEVEL_FAR - ( 4 * window.getHeight() / 3 ) ) );
			desk = new Desk();
			addRoomElementAt( desk,
							  new Point( ( 3 * ROOM_WIDTH ) / 8, GROUND_LEVEL_FAR - desk.getHeight() ) );
			easel = new Easel();
			addRoomElementAt( easel,
							  new Point( ( 5 * ROOM_WIDTH ) / 8, GROUND_LEVEL_FAR - easel.getHeight() ) );
			wardrobe = new Wardrobe();
			addRoomElementAt( wardrobe,
								 new Point( ( 7 * ROOM_WIDTH ) / 8, GROUND_LEVEL_FAR - ( int )( wardrobe.getHeight() * 0.95f ) ) );

			insertDrawable( helloKitty );
			activities = new ActivitySelectionWidget( this );
			insertDrawable( activities );
			kittyMessage = new HelloKittyMessage( this );
			insertDrawable( kittyMessage );
			activities.setPosition( 0,
									ScreenManager.SCREEN_HEIGHT - activities.getHeight() );
			statusBar = new DrawableImage( PATH_IMAGES + "statusbar.png" );
			insertDrawable( statusBar );
			statusBarFill = new DrawableImage( PATH_IMAGES + "statusbarcontent.png" );
			insertDrawable( statusBarFill );

			kittyMessage.setPosition( 0, statusBar.getHeight() );

			cameraTarget = helloKitty;
		} catch ( Exception ex ) {
//#if DEBUG == "true"
//# 			ex.printStackTrace();
//#endif
		}
	}

	/***
	* Enquanto que as coisas foram instanciadas, elas não foram devidamente inicializadas.
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		background.setSize( width, height );

		//parede ocupa 3/5 da tela.
		wall.setSize( width, ( height * 3 ) / 5 );

		// e por fim, o teto
		floor.setSize( width, 2 * height / 5 );

		roomObjects.setSize( ROOM_WIDTH, ScreenManager.SCREEN_HEIGHT );
		wallDecorations.setSize( ROOM_WIDTH, 3 * height / 5 );

		cameraPosition.x = ROOM_WIDTH >> 2;
	}

	/**
	 *
	 * @param item
	 * @param pos
	 */
	public void addWallDecorationAt( CustomizableObject item, Point pos ) {
		wallDecorations.insertDrawable(  item );
		wallDecorationList.addElement( item );
		item.setPosition( pos );
		item.setRoomPosition( pos );
	}

/**
 *
 * @param item
 * @param pos
 */
	public void addRoomElementAt( CustomizableObject item, Point pos ) {
		roomObjects.insertDrawable(  item );
		roomObjectList.addElement( item );
		item.setPosition( pos );
		item.setRoomPosition( pos );
	}

	/**
	 *
	 * @param delta
	 */
	public void update( int delta ) {

		if ( popup != null ) {

			popup.update( delta );

			if ( !popup.isVisible() ) {
				popup = null;
				activities.setAllNormal();
				GameMIDlet.gc();
			}
			
			return;
		}

		super.update( delta );

		cameraPosition.addEquals(  cameraSpeed );

		cameraPosition.x = NanoMath.clamp( cameraPosition.x, 0, ROOM_WIDTH );

		//...e começa onde termina o teto
		wall.setPosition( - cameraPosition.x, cameraPosition.y );
		// e o chão começa onde a parede termina
		floor.setPosition( - cameraPosition.x, cameraPosition.y + wall.getPosY() + wall.getHeight() );

		roomObjects.setPosition( - cameraPosition.x, DEFAULT_SPACE_FROM_WALL );
		wallDecorations.setPosition( - cameraPosition.x, wall.getPosY() );

		if ( cameraTarget != null )
			seek( cameraTarget );

		if ( hoveredTime < 0 )
			cameraTarget = helloKitty;
		else
			hoveredTime -= delta;

		statusBarFill.setSize( ( int ) ( statusBar.getWidth() * ( helloKitty.getGeneralHappiness() / 100 ) ), statusBar.getHeight() );
	}

	public static boolean isKittySayingAnything() {
		return instance.kittyMessage.isVisible();
	}

	/**
	 *
	 * @param msg
	 */
	public static void showKittyMessage( String msg ) {
		if ( instance != null ) {
			instance.kittyMessage.setVisible( true );
			instance.kittyMessage.setMessage( msg );
		}
	}

	/**
	 *
	 * @param key
	 */
	public void keyPressed( int key ) {

		if ( !activities.isVisible() && kittyMessage.isAllowedToDismiss() ) {
			kittyMessage.setVisible( false );
			return;
		}

		switch ( key ) {
			case ScreenManager.KEY_LEFT:
					activities.prevIndex();
				break;
			case ScreenManager.KEY_RIGHT:
					activities.nextIndex();
				break;
			case ScreenManager.KEY_UP:
				break;
			case ScreenManager.KEY_DOWN:
				break;
			case ScreenManager.KEY_FIRE:
					if ( activities.getCurrentOption() != activities.getCurrentBoldAction() ) {
						activities.selectCurrent();
						activities.setBoldOption();
					}
				break;

		}
	}


	/**
	 *
	 * @param key
	 */
	public void keyReleased( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_LEFT:
			case ScreenManager.KEY_RIGHT:
				cameraSpeed.x = 0;
				break;
		}
	}

/**
 *
 * @param target
 */
	private void seek( CustomizableObject target ) {
		int targetX = target.getRoomPosition().x;

		int diff = ( targetX - cameraPosition.x - ScreenManager.SCREEN_HALF_WIDTH );

		int normal = 0;

		if ( diff != 0 )
			normal = diff / NanoMath.abs( diff );

		if ( NanoMath.abs( diff ) > NanoMath.abs( DEFAULT_CAMERA_HORIZONTAL_SPEED ) ) {
			cameraSpeed.x = normal * DEFAULT_CAMERA_HORIZONTAL_SPEED;
		} else
			cameraSpeed.x = 0;
	}


	/**
	 *
	 * @param currentIndex
	 */
	public void itemSelection( int currentIndex ) {

		switch ( currentIndex ) {

			case HelloKitty.ACTION_BATH:
				helloKitty.moveToAndPerform( bed.getRoomPosition().x, HelloKitty.ACTION_BATH );

				try {
					
					popup = new ActivityPopUp( 6, null );
					popup.show();
					//HorizontalLayoutUpdatableGroup horizontal = new HorizontalLayoutUpdatableGroup( 4 );
					//VerticalLayoutUpdatableGroup vertical = new VerticalLayoutUpdatableGroup( 3 );
					//popup.setBorderSize( 10, 20 );
					//popup.setBackground( new DrawableImage( PATH_OBJECT_IMAGES + "wall00.png" ) );
					//popup.setBackground( 0x00FF00 );
					DrawableImage bkg = new DrawableImage( PATH_OBJECT_IMAGES + "bathbkg.png" );
					popup.addItem( bkg );
					popup.addItem( new Sprite( PATH_OBJECT_IMAGES + "bathanim" ) );

					//popup.addItem( vertical );
					//vertical.insertDrawable( new DrawableImage( PATH_OBJECT_IMAGES + "floor.png" ) );
					//vertical.insertDrawable( new DrawableImage( PATH_OBJECT_IMAGES + "bonsaikitten.png" ) );

					//DrawableImage dismissButton = new DrawableImage( PATH_OBJECT_IMAGES + "bonsaikitten.png" );
					//popup.addItem( dismissButton );
					//vertical.sizeToFit();

					insertDrawable( popup );
					//vertical.setLayout( VerticalLayoutUpdatableGroup.LAYOUT_CENTERED );
					popup.sizeToFit( );
					popup.centerAtScreen();
					popup.setDismissTimeout( 5 * SECONDS );
					popup.setDismissObject( bkg );
					
				} catch ( Exception ex ) {
					//#if DEBUG == "true"
//# 						ex.printStackTrace();
					//#endif
				}
				break;

			case HelloKitty.ACTION_GIFT:
				helloKitty.moveToAndPerform( wardrobe.getRoomPosition().x, HelloKitty.ACTION_PLAY );
				break;

			case HelloKitty.ACTION_SLEEP:
				helloKitty.moveToAndPerform( bed.getRoomPosition().x + bed.getWidth(), HelloKitty.ACTION_SLEEP );
				break;

			case HelloKitty.ACTION_EAT:
				helloKitty.moveToAndPerform( desk.getRoomPosition().x, HelloKitty.ACTION_EAT );
				cameraTarget = desk;
				break;

			case HelloKitty.ACTION_WEAR:
				helloKitty.moveToAndPerform( wardrobe.getRoomPosition().x, HelloKitty.ACTION_WEAR );
				break;

			case HelloKitty.ACTION_PAINT:
				helloKitty.moveToAndPerform( easel.getRoomPosition().x, HelloKitty.ACTION_PAINT );
				break;

			case HelloKitty.ACTION_READ:
				helloKitty.moveToAndPerform( desk.getRoomPosition().x, HelloKitty.ACTION_READ );
				cameraTarget = desk;
				break;

			case HelloKitty.ACTION_PLAY:
				helloKitty.moveToAndPerform( ROOM_WIDTH / 2, HelloKitty.ACTION_PLAY );
				cameraTarget = helloKitty;
				break;

		}
	}

/**
 *
 * @param currentIndex
 */
	public void itemHovered( int currentIndex ) {

		if ( !activities.isVisible() )
			return;

		switch ( currentIndex ) {

			case HelloKitty.ACTION_BATH:
				cameraTarget = helloKitty;
				break;

			case HelloKitty.ACTION_EAT:
				cameraTarget = desk;
				break;

			case HelloKitty.ACTION_WEAR:
				cameraTarget = wardrobe;
				break;

			case HelloKitty.ACTION_PAINT:
				cameraTarget = easel;
				break;

			case HelloKitty.ACTION_READ:
				cameraTarget = desk;
				break;

			case HelloKitty.ACTION_SLEEP:
				cameraTarget = bed;
				break;

			case HelloKitty.ACTION_GIFT:
				cameraTarget = wardrobe;
				break;

			case HelloKitty.ACTION_PLAY:
			case HelloKitty.ACTION_SEEK_CAMERA:
				cameraTarget = helloKitty;
				break;
		}
		
		//10 segundos
		hoveredTime = 10000;
	}

//#if TOUCH == "true"
//# 	public void onPointerDragged( int x, int y ) {
//# 		if ( popup != null )
//# 			popup.onPointerDragged( x, y );
//# 	}
//# 
//# 
//# 	public void onPointerPressed( int x, int y ) {
//# 		activities.onPointerPressed( x - activities.getPosX(), y - activities.getPosY() );
//# 		
//# 		if ( popup != null )
//# 			popup.onPointerPressed( x, y );
//# 	}
//# 
//# 
//# 	public void onPointerReleased( int x, int y ) {
//# 		if ( popup != null )
//# 			popup.onPointerReleased( x, y );
//# 	}
//# 
//# 
//#endif

	/**
	 *
	 * @param output
	 * @throws Exception
	 */
	public void write( DataOutputStream output ) throws Exception {

		long lastTime = System.currentTimeMillis();
		output.writeLong( lastTime );
		
		if ( helloKitty.getDepressionCount() > MAX_DEPRESSION_COUNT + 1 )
			return;

		helloKitty.write( output );

		for ( int c = 0; c < roomObjectList.size(); ++c ) {
			( ( CustomizableObject ) roomObjectList.elementAt( c ) ).write( output );
		}

		for ( int c = 0; c < wallDecorationList.size(); ++c ) {
			( ( CustomizableObject ) wallDecorationList.elementAt( c ) ).write( output );
		}
	}

/**
 *
 * @param input
 * @throws Exception
 */
	public void read( DataInputStream input ) throws Exception {
		
		long lastTime = input.readLong();
		System.out.println( "last time:" + lastTime );
		helloKitty.read( input );
		helloKitty.updateStats( ( System.currentTimeMillis() - lastTime ) );

		for ( int c = 0; c < roomObjectList.size(); ++c ) {
			( ( CustomizableObject ) roomObjectList.elementAt( c ) ).read( input );
		}

		for ( int c = 0; c < wallDecorationList.size(); ++c ) {
			( ( CustomizableObject ) wallDecorationList.elementAt( c ) ).read( input );
		}
	}

	/**
	 *
	 * @param state
	 */
	public void notifyVisible( boolean state ) {
		activities.setVisible( !state );
	}

	/**
	 * 
	 */
	public void focusRequest() {
		cameraTarget = helloKitty;
	}


	public void itemDesselected() {
	}


	public void ActivityFinished() {
		
		if ( activities != null )
			activities.setAllNormal();
	}




	public void notifyConstructionReady() {
		instance = this;
	}
}