package graphics.userinterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class HorizontalLayoutUpdatableGroup extends UpdatableGroup {
	private int lastX;

	private byte currentLayout;

	public static final byte LAYOUT_SNAP_TO_LOWEST = 0;
	public static final byte LAYOUT_CENTERED = 1;
	public static final byte LAYOUT_SNAP_TO_BIGGEST = 2;

	public HorizontalLayoutUpdatableGroup( int slots ) {
		super( slots );
		currentLayout = LAYOUT_SNAP_TO_LOWEST;
	}

	public void setLayout( byte newLayout ) {
		currentLayout = newLayout;
		updateLayout();
	}

	public short insertDrawable( Drawable drawable ) {
		short toReturn = 0;

		toReturn = super.insertDrawable( drawable );

		if ( toReturn != -1 ) {
			drawable.setPosition( lastX, 0 );
			lastX += drawable.getWidth();
			updateLayout();
		}

		return toReturn;
	}

	public void sizeToFit() {

		Point neededSize = new Point();
		Drawable element = null;

		for ( int c = 0; c < super.getUsedSlots(); ++c ) {
			element = getDrawable( c );

			neededSize.x += element.getWidth();
			neededSize.y = NanoMath.max( element.getPosY() + element.getHeight(), neededSize.y );
		}

		setSize( neededSize );
	}

	private void updateLayout() {
		Drawable current;

		for ( int c = 0; c < super.getUsedSlots(); ++c ) {

			current = getDrawable( c );

			switch( currentLayout ) {
				case LAYOUT_CENTERED:
					current.setPosition( current.getPosX(), ( getHeight() - current.getHeight() ) / 2 );
					break;

				case LAYOUT_SNAP_TO_LOWEST:
					current.setPosition( current.getPosX(), 0 );
					break;

				case LAYOUT_SNAP_TO_BIGGEST:
					current.setPosition( current.getPosX(), getHeight() - current.getHeight() );
					break;
			}
		}
	}
}
