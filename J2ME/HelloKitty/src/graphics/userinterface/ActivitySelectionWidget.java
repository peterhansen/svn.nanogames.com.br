package graphics.userinterface;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.hellokitty.GameMIDlet;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class ActivitySelectionWidget extends UpdatableGroup implements graphics.GraphicalConstants, core.CoreConstants
//#if TOUCH == "true"
//# 		,PointerListener
//#endif
{
	public static final byte TOTAL_ACTIVITIES = 8;
	
	public static final int[] actions = {
										TEXT_ACTIVITY_SLEEP,
										TEXT_ACTIVITY_EAT,
										TEXT_ACTIVITY_BUY,
										TEXT_ACTIVITY_PLAY,
										TEXT_ACTIVITY_PAINT,
										TEXT_ACTIVITY_READ,
										TEXT_ACTIVITY_WEAR,
										TEXT_ACTIVITY_BATH,
										};



	private DrawableImage leftArrow;
	private DrawableImage rightArrow;
	private RichLabel current;
	private int currentIndex;
	private int currentActive;
	private RichLabel[] options;
	private ActivitySelectionListener listener;


	public ActivitySelectionWidget( ActivitySelectionListener listener ) {
		super( TOTAL_ACTIVITIES );

		this.listener = listener;

		currentActive = -1;
		try {
			leftArrow = new DrawableImage( PATH_IMAGES + "right.png" );
			leftArrow.setTransform( DrawableImage.TRANS_MIRROR_H );
			rightArrow = new DrawableImage( PATH_IMAGES + "right.png" );

			options = new RichLabel[ TOTAL_ACTIVITIES ];

			for ( int c = 0; c < TOTAL_ACTIVITIES; ++c ) {
				options[ c ] = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance()).getFont( FONT_ACTIVITY_SELECTION ), actions[ c ] );
				options[ c ].setSize( ScreenManager.SCREEN_WIDTH - leftArrow.getWidth() - rightArrow.getWidth(), ScreenManager.SCREEN_HEIGHT / 5 );
			}

			//insertDrawable( background );
			insertDrawable( leftArrow );
			insertDrawable( rightArrow );
			
			current = null;
			setSize( ScreenManager.SCREEN_WIDTH, 0 );
			setCurrentOption( 2 );
		} catch ( Exception ex ) {
			ex.printStackTrace();
		}

	}

	public void setSize( int width, int height ) {
		super.setSize( width, ScreenManager.SCREEN_HEIGHT / 5 );

		rightArrow.setPosition( width - rightArrow.getWidth(), ( getHeight() - rightArrow.getHeight() ) / 2 );
		leftArrow.setPosition( 0, ( getHeight() - leftArrow.getHeight() ) / 2 );
	}

	public void nextIndex () {
		
		currentIndex = ( ++currentIndex ) % TOTAL_ACTIVITIES;

		setCurrentOption( currentIndex );

		if ( listener != null ) {
			listener.itemHovered( currentIndex );
		}
	}


	public void prevIndex () {
		--currentIndex;

		if ( currentIndex < 0 )
			currentIndex = TOTAL_ACTIVITIES - currentIndex - 2;

		setCurrentOption( currentIndex );

		if ( listener != null ) {
			listener.itemHovered( currentIndex );
		}
	}

	public int getCurrentOption() {
		return currentIndex;
	}

	private void setCurrentOption( int i ) {
		if ( current != null )
			removeDrawable( current );

		currentIndex = i;
		current = ( RichLabel ) options[ i ];
		insertDrawable( current );
		current.setPosition( ( getWidth() - current.getTextWidth() ) / 2, ( getHeight() - current.getTextTotalHeight() ) / 2 );
	}

	public void selectCurrent() {
		if ( listener != null ) {
			listener.itemSelection( currentIndex );
		}
	}

//#if TOUCH == "true"
//# 	public void onPointerDragged( int x, int y ) {
//# 	}
//# 
//# 
//# 	public void onPointerPressed( int x, int y ) {
//# 		if ( leftArrow.contains( x, y ) )
//# 			prevIndex();
//# 
//# 		if ( current.contains( x, y ) )
//# 			selectCurrent();
//# 
//# 		if ( rightArrow.contains( x, y ) )
//# 			nextIndex();
//# 	}
//# 
//# 
//# 	public void onPointerReleased( int x, int y ) {
//# 	}
//# 
//# 
//#endif
	
	public void setBoldOption() {

		setAllNormal();
		currentActive = currentIndex;
		current.setFont( 2 );
	}

	public int getCurrentBoldAction() {
		return currentActive;
	}


	public void setAllNormal() {

		currentActive = -1;

		if ( options != null )
			for ( int c = 0; c < TOTAL_ACTIVITIES && options[ c ] != null; ++c ) {
				options[ c ].setFont( 1 );
			}
	}
}
