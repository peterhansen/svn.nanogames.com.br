package graphics.userinterface;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.hellokitty.GameMIDlet;

/**
 *
 * @author monteiro
 */
public class HelloKittyMessage extends UpdatableGroup implements core.CoreConstants, graphics.GraphicalConstants {

	public static final byte MSGTYPE_SAY = 0;
	public static final byte MSGTYPE_THOUGHT = 1;

	private DrawableImage diagBottomSay;
	private DrawableImage diagBottomThink;

	private RichLabel message;
	private int visibleTime;

	public static int lastMsg = -1;
	public static int msgType = MSGTYPE_SAY;

	VisibilityListener listener;

	public HelloKittyMessage( VisibilityListener listener ) {
		super( 12 );

		this.listener = listener;
		
		try {
			DrawableImage img[ ] = new DrawableImage[ 8 ];
			Pattern pattern[ ] = new Pattern[ 5 ];


			//diagonal superior esquerda
			img[ 0 ] = new DrawableImage( PATH_IMAGES + "diag_corner.png" );
			insertDrawable( img[ 0 ] );

			//diagonal inferior direita
			img[ 1 ] = new DrawableImage( PATH_IMAGES + "diag_corner.png" );
			img[ 1 ].setPosition( ScreenManager.SCREEN_WIDTH - img[ 1 ].getWidth(), ( ScreenManager.SCREEN_HEIGHT / 4 ) - 2 * img[ 1 ].getHeight() );
			img[ 1 ].setTransform( DrawableImage.TRANS_MIRROR_H | DrawableImage.TRANS_MIRROR_V );
			insertDrawable( img[ 1 ] );

			//diagonal superior direita
			img[ 2 ] = new DrawableImage( PATH_IMAGES + "diag_corner.png" );
			img[ 2 ].setPosition( ScreenManager.SCREEN_WIDTH - img[ 2 ].getWidth(), 0 );
			img[ 2 ].setTransform( DrawableImage.TRANS_MIRROR_H );
			insertDrawable( img[ 2 ] );

			//diagonal inferior esquerda
			img[ 3 ] = new DrawableImage( PATH_IMAGES + "diag_corner.png" );
			img[ 3 ].setPosition( 0, ( ScreenManager.SCREEN_HEIGHT / 4 ) - 2 * img[ 3 ].getHeight() );
			img[ 3 ].setTransform( DrawableImage.TRANS_MIRROR_V );
			insertDrawable( img[ 3 ] );

			// topo
			img[ 4 ] = new DrawableImage( PATH_IMAGES + "diag_top.png" );
			pattern[ 0 ] = new Pattern( img[ 4 ] );
			insertDrawable( pattern[ 0 ] );
			pattern[ 0 ].setSize( ScreenManager.SCREEN_WIDTH - 2 * img[ 0 ].getWidth(), img[ 4 ].getHeight() );
			pattern[ 0 ].setPosition( img[ 0 ].getWidth(), 0 );

			// lateral esqueda
			img[ 5 ] = new DrawableImage( PATH_IMAGES + "diag_top.png" );
			img[ 5 ].rotate( DrawableImage.TRANS_ROT270 );
			pattern[ 1 ] = new Pattern( img[ 5 ] );
			insertDrawable( pattern[ 1 ] );
			pattern[ 1 ].setSize( img[ 5 ].getWidth(), ( ScreenManager.SCREEN_HEIGHT / 4 ) - 3 * img[ 0 ].getHeight() );
			pattern[ 1 ].setPosition( 0, img[ 0 ].getHeight() );

			// lateral direita
			img[ 6 ] = new DrawableImage( PATH_IMAGES + "diag_top.png" );
			img[ 6 ].rotate( DrawableImage.TRANS_ROT90 );
			pattern[ 2 ] = new Pattern( img[ 6 ] );
			insertDrawable( pattern[ 2 ] );
			pattern[ 2 ].setSize( img[ 6 ].getWidth(), ( ScreenManager.SCREEN_HEIGHT / 4 ) - 3 * img[ 0 ].getHeight() );
			pattern[ 2 ].setPosition( ScreenManager.SCREEN_WIDTH -  img[ 6 ].getWidth(),  img[ 0 ].getHeight() );

			//linha de baixo
			img[ 7 ] = new DrawableImage( PATH_IMAGES + "diag_top.png" );
			img[ 7 ].setTransform( DrawableImage.TRANS_MIRROR_V );
			pattern[ 3 ] = new Pattern( img[ 7 ] );
			insertDrawable( pattern[ 3 ] );
			pattern[ 3 ].setSize( ScreenManager.SCREEN_WIDTH - 2 * img[ 0 ].getWidth(), img[ 4 ].getHeight() );
			pattern[ 3 ].setPosition( img[ 0 ].getWidth(), ( ScreenManager.SCREEN_HEIGHT / 4 ) - 2 * img[ 7 ].getHeight() );

			pattern[ 4 ] = new Pattern( 0xFFFFFF );
			pattern[ 4 ].setPosition( img[ 5 ].getWidth(), img[ 4 ].getHeight() );
			pattern[ 4 ].setSize( ScreenManager.SCREEN_WIDTH - img[ 5 ].getWidth() - img[ 6 ].getWidth() ,
									( ScreenManager.SCREEN_HEIGHT / 4 ) - 3 * img[ 7 ].getHeight() );
			insertDrawable( pattern[ 4 ] );


			diagBottomSay = new DrawableImage( PATH_IMAGES + "diag_talk_bottom.png" );
			diagBottomThink = new DrawableImage( PATH_IMAGES + "diag_think_bottom.png" );
			
			insertDrawable( diagBottomSay );
			insertDrawable( diagBottomThink );

			diagBottomSay.setPosition( ScreenManager.SCREEN_HALF_WIDTH - diagBottomSay.getWidth() / 2, ScreenManager.SCREEN_HEIGHT / 4 - diagBottomSay.getHeight() - 3 );
			diagBottomThink.setPosition( diagBottomSay.getPosition() );
			
			diagBottomThink.setVisible( false );
			diagBottomSay.setVisible( true );

			message = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance() ).getFont( FONT_MESSAGE_BOX ), "COMPRAS" );
			insertDrawable( message );

			setSize( ScreenManager.SCREEN_WIDTH,
					 ScreenManager.SCREEN_HEIGHT / 4 );

			setVisible( false );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 			ex.printStackTrace();
			//#endif
		}

	}

	/**
	 *
	 * @param msg
	 */
	public void setMessage( String msg ) {
		
		if ( diagBottomThink != null )
			diagBottomThink.setVisible( msgType == 0 );

		if ( diagBottomSay != null )
			diagBottomSay.setVisible( msgType != 0 );

		message.setText( msg, true );
	}

	/**
	 *
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );
		message.setSize( ( int ) ( width * 0.95f ), ( int ) ( height * 0.95f ) );
		message.setPosition( ( int ) ( width * 0.05f ), ( int ) ( height * 0.05f ) );
	}


	/**
	 *
	 * @param visible
	 */
	public void setVisible( boolean visible ) {
		super.setVisible( visible );

		if ( listener != null )
			listener.notifyVisible( visible );

		visibleTime = 0;
	}


	/**
	 *
	 * @param delta
	 */
	public void update( int delta ) {
		super.update( delta );

		if ( visibleTime > DEFAULT_BOX_VISIBLE_TIME ) {
			setVisible( false );
			lastMsg = -1;
		} else
			visibleTime += delta;
	}


	public boolean isAllowedToDismiss() {
		return visibleTime > DEFAULT_BOX_VISIBLE_TIME / 2;
	}
}
