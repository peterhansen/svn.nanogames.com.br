package graphics.userinterface;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface ActivityPopUpListener {
	public void renderContentRequested();
	public void contentDismissed();
	public void itemSelected( int id );
}
