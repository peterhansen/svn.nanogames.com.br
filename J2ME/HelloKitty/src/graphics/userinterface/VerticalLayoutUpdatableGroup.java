package graphics.userinterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class VerticalLayoutUpdatableGroup extends UpdatableGroup {
	private int lastY;

	private byte currentLayout;

	public static final byte LAYOUT_SNAP_TO_LOWEST = 0;
	public static final byte LAYOUT_CENTERED = 1;
	public static final byte LAYOUT_SNAP_TO_BIGGEST = 2;

	public VerticalLayoutUpdatableGroup( int slots ) {
		super( slots );
		currentLayout = LAYOUT_SNAP_TO_LOWEST;
	}

	public void setLayout( byte newLayout ) {
		currentLayout = newLayout;
		updateLayout();
	}

	public short insertDrawable( Drawable drawable ) {
		short toReturn = 0;

		toReturn = super.insertDrawable( drawable );
		
		if ( toReturn != -1 ) {
			
			drawable.setPosition( 0, lastY );
			lastY += drawable.getHeight();
			updateLayout();
		}

		return toReturn;
	}

	public void sizeToFit() {

		Point neededSize = new Point();
		Drawable element = null;

		for ( int c = 0; c < super.getUsedSlots(); ++c ) {
			element = getDrawable( c );

			neededSize.x = NanoMath.max( element.getPosX() + element.getWidth(), neededSize.x );
			neededSize.y += element.getHeight();
		}

		setSize( neededSize );
	}

	private void updateLayout() {
		Drawable current;

		for ( int c = 0; c < super.getUsedSlots(); ++c ) {

			current = getDrawable( c );

			switch( currentLayout ) {
				case LAYOUT_CENTERED:
					current.setPosition( ( getWidth() - current.getWidth() ) / 2, current.getPosY() );
					break;

				case LAYOUT_SNAP_TO_LOWEST:
					current.setPosition( 0, current.getPosY() );
					break;

				case LAYOUT_SNAP_TO_BIGGEST:
					current.setPosition( getWidth() - current.getWidth(), current.getPosY() );
					break;
			}
		}
	}
}
