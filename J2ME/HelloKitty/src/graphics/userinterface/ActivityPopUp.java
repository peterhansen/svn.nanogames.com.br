package graphics.userinterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import java.util.Vector;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class ActivityPopUp extends UpdatableGroup implements graphics.GraphicalConstants, core.CoreConstants, KeyListener
//#if DEBUG == "true"
//# 		,PointerListener
//#endif		
{
	private Point borderSize;
	private Drawable background;
	private Drawable cursor;
	private Drawable content;
	private Drawable dissmissObject;
	private long timeToTimeout;
	private ActivityPopUpListener listener;
	private Vector elements;
	private int cursorPosition;
	private Drawable cursorObject;

	public ActivityPopUp( int slots, ActivityPopUpListener listener ) {
		super( slots + 2 );
		borderSize = new Point();
		cursorPosition = 0;
		this.listener = listener;
		timeToTimeout = -1;
		elements = new Vector();
		setVisible( false );
	}


	public void keyPressed( int key ) {
	}


	public void keyReleased( int key ) {
	}

//#if TOUCH == "true"
//# 	public void onPointerDragged( int x, int y ) {
//# 	}
//# 
//# 
//# 	public void onPointerPressed( int x, int y ) {
//# 	}
//# 
//# 	public void onPointerReleased( int x, int y ) {
//# 		Point click = new Point( x - getPosX(), y - getPosY() );
//# 
//# 		if ( dissmissObject != null && dissmissObject.contains( click ) )
//# 			dismiss();
//# 	}
//#endif

	public void addCursorObject( Drawable cursorObject ){
		cursor = cursorObject;
		insertDrawable( cursorObject );
		setCursorObject( cursorObject );
	}

	public void setCursorObject( Drawable cursorObject ){
		this.cursorObject = cursorObject;
	}
	
	public void setDismissObject( Drawable dismissObject ) {
		this.dissmissObject = dismissObject;
	}


	private void dismiss() {
		startHideAnimation();
	}

	public void show() {

		if ( listener != null )
			listener.renderContentRequested();

		startShowAnimation();
	}

	private void startHideAnimation() {
		setVisible( false );

		if ( listener != null )
			listener.contentDismissed();
	}


	private void startShowAnimation() {
		setVisible( true );
	}


	public void addItem( Drawable drawable ) {
		elements.addElement( drawable );
		drawable.setPosition( drawable.getPosition().add( borderSize ) );
		insertDrawable( drawable );
	}


	public void setBackground( int color ) {

		if ( background != null )
			removeDrawable( background );

		background = new Pattern( color );
		insertDrawable( background );
	}

	public void setBackground( Drawable item ) {

		if ( background != null )
			removeDrawable( background );

		background = new Pattern( item );
		insertDrawable( background );
	}


	public void sizeToFit() {

		Point neededSize = new Point();
		Drawable element = null;

		for ( int c = 0; c < elements.size(); ++c ) {
			element = ( Drawable ) elements.elementAt( c );

			neededSize.x = NanoMath.max( neededSize.x, ( element.getPosX() + element.getWidth() ) );
			neededSize.y = NanoMath.max( neededSize.y, ( element.getPosX() + element.getHeight() ) );
		}

		neededSize.x += borderSize.x << 1;
		neededSize.y += borderSize.y << 1;

		setSize( neededSize );
	}

	public void centerAtScreen() {
		setPosition( ScreenManager.SCREEN_HALF_WIDTH - getWidth() / 2, ScreenManager.SCREEN_HALF_HEIGHT - getHeight() / 2 );
	}


	public void setBorderSize( int width, int height ) {
		borderSize.x = width;
		borderSize.y = height;
	}

	public void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( background != null )
			background.setSize( width, height );
	}


	public void sizeToFitFillForBackground() {
		Point neededSize = new Point();
		Drawable element = null;

		neededSize.x = ( neededSize.x < background.getWidth() ) ? background.getWidth() : neededSize.x;
		neededSize.y = ( neededSize.y < background.getHeight() ) ? background.getHeight() : neededSize.y;

		for ( int c = 0; c < elements.size(); ++c ) {
			element = ( Drawable ) elements.elementAt( c );

			neededSize.x = NanoMath.max( neededSize.x, ( element.getPosX() + element.getWidth() ) );
			neededSize.y = NanoMath.max( neededSize.y, ( element.getPosX() + element.getHeight() ) );
		}

		neededSize.x += borderSize.x << 1;
		neededSize.y += borderSize.y << 1;

		setSize( neededSize );

	}


	/**
	 *
	 * @param delta
	 */
	public void update( int delta ) {
		super.update( delta );

		if ( timeToTimeout < -1 ) {
			dismiss();
		} else {
			timeToTimeout -= delta;
			if ( timeToTimeout == -1 )
				timeToTimeout = -2;

		}
	}


	/**
	 *
	 * @param l
	 */
	public void setDismissTimeout( long l ) {
		timeToTimeout = l;
	}


}
