package graphics.userinterface;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface VisibilityListener {
	public void notifyVisible( boolean state );
}
