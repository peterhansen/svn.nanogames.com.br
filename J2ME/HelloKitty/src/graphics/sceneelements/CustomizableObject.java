package graphics.sceneelements;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Montys" Monteiro
 */
public class CustomizableObject extends UpdatableGroup implements Updatable, Serializable {
	protected Point roomPosition;
	private Drawable visual;
	
	public void setImage( Drawable visual ) {
		if ( visual != null )
			removeDrawable( visual );
		
		this.visual = visual;
		
		insertDrawable( visual );
		setSize( visual.getSize() );
	}

	public CustomizableObject( int slots ) {
		super( slots );
		roomPosition = new Point();
	}


	public Point getRoomPosition() {
		return roomPosition;
	}


	public void setRoomPosition( Point pos ) {
		roomPosition.set( pos );
	}


	public void write( DataOutputStream output ) {
	}


	public void read( DataInputStream input ) throws Exception {
	}

}
