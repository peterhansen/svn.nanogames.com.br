package graphics.sceneelements;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface FocusRequestListener {
	public void focusRequest();
}
