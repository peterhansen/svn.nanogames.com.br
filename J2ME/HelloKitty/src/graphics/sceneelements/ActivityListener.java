package graphics.sceneelements;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface ActivityListener {
	public void ActivityFinished();
}
