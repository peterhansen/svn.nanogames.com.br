package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Wall extends CustomizableObject implements GraphicalConstants, CoreConstants {

	private Pattern upperPattern;
	private Pattern lowerPattern;

	/**
	 * 
	 */
	public Wall() {
		super( 2 );
		try {
			DrawableImage img;

			img = new DrawableImage( PATH_OBJECT_IMAGES + "wall01.png" );
			upperPattern = new Pattern( img );
			img = new DrawableImage( PATH_OBJECT_IMAGES + "wall00.png" );
			lowerPattern = new Pattern( img );

			lowerPattern.setSize( ScreenManager.SCREEN_WIDTH , img.getHeight() );
			
			insertDrawable(  upperPattern );
			insertDrawable(  lowerPattern );
		} catch ( Exception ex ) {
			//#if DEBUG
//# 			ex.printStackTrace();
			//#endif
		}
	}

	/***
	 * Define o tamanho do objeto - mas a largura é ignorada.
	 * Apenas a altura é usada. A largura depende de CoreConstants.ROOM_WIDTH.
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( ROOM_WIDTH, height );

		upperPattern.setSize( ROOM_WIDTH, height - lowerPattern.getHeight() );
		lowerPattern.setSize( ROOM_WIDTH, lowerPattern.getHeight() );
		lowerPattern.setPosition( 0, upperPattern.getHeight() + upperPattern.getPosY() );
	}


	public void read( DataInputStream input ) throws Exception {
	}


	public void write( DataOutputStream output ) {
	}

}
