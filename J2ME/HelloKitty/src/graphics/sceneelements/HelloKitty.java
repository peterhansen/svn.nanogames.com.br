package graphics.sceneelements;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.hellokitty.GameMIDlet;
import core.CoreConstants;
import core.Queue;
import graphics.GraphicalConstants;
import graphics.screens.GameScreen;
import graphics.userinterface.HelloKittyMessage;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class HelloKitty extends CustomizableObject implements GraphicalConstants, CoreConstants {
	public static final byte MOVINGSTATE_STILL = 3;
	public static final byte MOVINGSTATE_MOVINGRIGHT= 0;
	public static final byte MOVINGSTATE_MOVINGLEFT= 1;
	public static final byte MOVINGSTATE_PERFORMING_ACTION= 2;
	public static final byte HEADPIECE_Y_OFFSET = 0;
	public static final byte BODYPIECE_Y_OFFSET = 0;
	public static final byte LEGSPIECE_Y_OFFSET = 20;
	public static final byte WALKSPEED = 4;
	public static final byte MAX_QUEUED_ACTIONS = 20;

	private static final String activityItems[] = {
		null,
		"food",
		null,
		"videogame",
		null,
		"book",
		null,
		null,
		null,
		null,
		null,
	};

	public static final byte ACTION_SLEEP = 0;
	public static final byte ACTION_EAT = 1;
	public static final byte ACTION_DOOR = 2;
	public static final byte ACTION_PLAY = 3;
	public static final byte ACTION_PAINT = 4;
	public static final byte ACTION_READ = 5;
	public static final byte ACTION_WEAR = 6;
	public static final byte ACTION_BATH = 7;
	public static final byte ACTION_STILL = 8;
	public static final byte ACTION_GIFT = 9;
	public static final byte ACTION_SEEK_CAMERA = 10;

	public static final byte GAUGE_HUNGER = 0;
	public static final byte GAUGE_SLEEPY = 1;
	public static final byte GAUGE_BUYING = 2;
	public static final byte GAUGE_CLEANESS = 3;
	public static final byte GAUGE_FUN = 4;

	public static final float MAX_GAUGE_VALUE = 25.0f;
	public static final float MIN_GAUGE_VALUE = -25.0f;

	public static final byte MOODS = 7;

	private float[] gauge;
	private long[] TASK_DURACTION = { 20 * SECONDS, 5 * SECONDS, 1 * SECOND, 5 * SECONDS, 5 * SECONDS, 3 * SECONDS, 5 * SECONDS, 5 * SECONDS, 0 };
	Sprite currentMood;
	int currentMoodIndex;
	
	private Point camera;
	private Sprite kittySprite;
	private Sprite headPieceSprite;
	private Sprite activityItemSprite;
	private Sprite bodyPieceSprite;
	private Sprite legsPieceSprite;

	private Queue actionsToPerform;	
	private byte movingState;
	private int targetX;
	private int performingAction;
	private long neededTimeToPerformAction;
	private int depressionCount;
	private long timeToTalk;
	private FocusRequestListener listener;
	private ActivityListener activityListener;

	/**
	 *
	 */
	public HelloKitty( Point camera, FocusRequestListener listener, ActivityListener activityListener ) {
		super( 6 );

		this.listener = listener;
		this.activityListener = activityListener;

		try {
			this.camera = camera;
			targetX = 0;
			currentMoodIndex = -1;
			depressionCount = 0;
			actionsToPerform = new Queue();
			
			gauge = new float[ 5 ];

			kittySprite = new Sprite( PATH_OBJECT_IMAGES + "hellokitty" );
			insertDrawable( kittySprite );
		//	setHeadPiece( "lace");
		//	setBodyPiece( "trainer");
		//	setLegsPiece( "bluetrainers");
			resetStats();

			setStill();			
			setSize( kittySprite.getCurrentFrame().width, kittySprite.getCurrentFrame().height );
			updateFaceMood();
		} catch ( Exception ex ) {
			//#if DEBUG
//# 			ex.printStackTrace();
			//#endif
		}
	}

	private void resetStats() {
		for ( int c = 0; c < gauge.length; ++c )
			gauge[ c ] = MAX_GAUGE_VALUE;
	}

	private void giveBackSomeHappiness() {
		for ( int c = 0; c < gauge.length; ++c )
			gauge[ c ] = 0.0f;
	}


	/**
	 *
	 * @param index
	 * @return
	 */
	public float getStat( int index ) {
		return gauge[ index ];
	}

	/**
	 *
	 * @param frame
	 */
	public synchronized  void syncAllFrames( int frame ) {
		kittySprite.setSequence( movingState );

		if ( currentMood != null )
			currentMood.setSequence( 0 );

		if ( activityItemSprite != null )
			activityItemSprite.setSequence( 0 );
		
		if ( headPieceSprite != null )
			headPieceSprite.setSequence( movingState );

		if ( bodyPieceSprite != null )
			bodyPieceSprite.setSequence( movingState );

		if ( legsPieceSprite != null )
			legsPieceSprite.setSequence( movingState );
	}

	/**
	 *
	 * @param spriteName
	 */
	public synchronized void setHeadPiece( String spriteName ) {
		try {
			if ( headPieceSprite != null ) {
				removeDrawable( headPieceSprite );
				headPieceSprite = null;
			}

			if ( spriteName.length() == 0 )
				return;

			headPieceSprite = new Sprite( PATH_OBJECT_IMAGES + spriteName );
			insertDrawable( headPieceSprite );
			headPieceSprite.setPosition( 0, HEADPIECE_Y_OFFSET );
			headPieceSprite.setSequence(  kittySprite.getSequenceIndex() );
			syncAllFrames(  kittySprite.getCurrFrameIndex() );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 			ex.printStackTrace();
			//#endif

		}
	}


	/**
	 * 
	 * @param delta
	 */
	public void updateStats( long delta ) {

		decGauge( GAUGE_SLEEPY, delta, 0.0005f );
		decGauge( GAUGE_BUYING, delta, 0.0001f );
		decGauge( GAUGE_CLEANESS, delta, 0.0001f );
		decGauge( GAUGE_FUN, delta, 0.00025f );
		decGauge( GAUGE_HUNGER, delta, 0.0005f );

		tickActionStats( delta );

		int lowGauges = 0;

		for ( int c = 0; c < gauge.length; ++c ) {
			if ( gauge[ c ] < MIN_GAUGE_VALUE ) {
				gauge[ c ] = MIN_GAUGE_VALUE;
				++lowGauges;
			}

			if ( gauge[ c ] >= MAX_GAUGE_VALUE ) {
				gauge[ c ] = MAX_GAUGE_VALUE;
			}
		}

		if ( lowGauges == gauge.length ) {
			++depressionCount;
			giveBackSomeHappiness();
		}
		
		
		if ( depressionCount == MAX_DEPRESSION_COUNT + 1 ) {
			( ( GameMIDlet ) GameMIDlet.getInstance() ).onGameOver();
		}


		//DEBUG_printStats();
	}


	/**
	 *
	 * @param spriteName
	 */
	public synchronized void setBodyPiece( String spriteName ) {
		try {
			if ( bodyPieceSprite != null ) {
				removeDrawable( bodyPieceSprite );
				bodyPieceSprite = null;
			}

			if ( spriteName.length() == 0 )
				return;

			bodyPieceSprite = new Sprite( PATH_OBJECT_IMAGES + spriteName );
			insertDrawable( bodyPieceSprite );
			bodyPieceSprite.setPosition( 0, BODYPIECE_Y_OFFSET );
			bodyPieceSprite.setSequence(  kittySprite.getSequenceIndex() );
			syncAllFrames(  kittySprite.getCurrFrameIndex() );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 			ex.printStackTrace();
			//#endif

		}
	}


	/**
	 *
	 * @param spriteName
	 */
	public synchronized void setLegsPiece( String spriteName ) {
		try {
			if ( legsPieceSprite != null ) {
				removeDrawable( legsPieceSprite );
				legsPieceSprite = null;
			}

			if ( spriteName.length() == 0 )
				return;

			legsPieceSprite = new Sprite( PATH_OBJECT_IMAGES + spriteName );
			insertDrawable( legsPieceSprite );
			legsPieceSprite.setPosition( 0, LEGSPIECE_Y_OFFSET );
			legsPieceSprite.setSequence(  kittySprite.getSequenceIndex() );
			syncAllFrames(  kittySprite.getCurrFrameIndex() );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 			ex.printStackTrace();
			//#endif
		}
	}


	/**
	 *
	 * @param posX
	 */
	public void moveTo( int posX ) {
		targetX = posX;		

		int diff = ( targetX - roomPosition.x );		

		if ( diff > 0 )
			movingState = MOVINGSTATE_MOVINGRIGHT;
		else
			movingState = MOVINGSTATE_MOVINGLEFT;

		neededTimeToPerformAction = 0;
		
		kittySprite.setSequence( movingState );

		if ( headPieceSprite != null )
			headPieceSprite.setSequence( movingState );

		if ( bodyPieceSprite != null )
			bodyPieceSprite.setSequence( movingState );

		if ( legsPieceSprite != null )
			legsPieceSprite.setSequence( movingState );
	}

	/**
	 *
	 * @param posX
	 * @param posY
	 */
	public void setRoomPosition( int posX, int posY ) {
		targetX = posX;
		roomPosition.set( posX, posY );
	}

	/**
	 *
	 * @param delta
	 */
	public synchronized void update( int delta ) {
		super.update( delta );

		int diff = ( targetX - roomPosition.x );		
		int normal = 0;
		
		if ( NanoMath.abs( diff ) > WALKSPEED )
			normal = diff / NanoMath.abs( diff );

		if ( normal != 0 ) {
			roomPosition.x +=  normal * WALKSPEED;
		} else {
			
			if ( neededTimeToPerformAction <= 0 ) {
				if ( movingState != MOVINGSTATE_STILL ) {
					
					if ( activityListener != null && movingState == MOVINGSTATE_PERFORMING_ACTION )
						activityListener.ActivityFinished();

					setStill();
				}
			} else {
				neededTimeToPerformAction -= delta;
			}			
		}

		super.position.x =  roomPosition.x - camera.x;
		super.position.y = roomPosition.y - getHeight();


		//#if DEBUG == "true"
//# 			updateStats( delta );
//# 
//# 			if ( timeToTalk < 0 )
//# 				updateMessage();
//# 			else
//# 				timeToTalk -= delta;
		//#endif

		updateFaceMood();
	}


	/**
	 *
	 */
	private void setStill() {
		movingState = MOVINGSTATE_STILL;
		performingAction = ACTION_STILL;
		kittySprite.setSequence( MOVINGSTATE_STILL );
		
		if ( headPieceSprite != null )
			headPieceSprite.setSequence(  MOVINGSTATE_STILL );

		if ( bodyPieceSprite != null )
			bodyPieceSprite.setSequence(  MOVINGSTATE_STILL );

		if ( legsPieceSprite != null )
			legsPieceSprite.setSequence(  MOVINGSTATE_STILL );

		setActivityItem( null );
		flushActions();
	}


	/**
	 *
	 * @param x
	 * @param action
	 */
	public void moveToAndPerform( int x, int action ) {
		moveTo( x );
		pushAction( action );
		setActivityItem( null );
	}


	/**
	 *
	 * @param action
	 */
	private void pushAction( int action ) {
		actionsToPerform.push( new Integer( action ) );
	}

	public void flushActions() {

		if ( actionsToPerform.empty() )
			return;

		movingState = MOVINGSTATE_PERFORMING_ACTION;
		kittySprite.setSequence(  movingState );
		performingAction = ( ( Integer ) actionsToPerform.pop() ).intValue();
		neededTimeToPerformAction = TASK_DURACTION[ performingAction ];
		
		if ( activityItems[ performingAction ] != null )
			setActivityItem( activityItems[ performingAction ] );

	}

	public float getGeneralHappiness() {
		float mean = 0;
		
		for ( int c = 0; c < gauge.length; ++c ) {
			mean += gauge[ c ] - MIN_GAUGE_VALUE;
		}

		mean /= gauge.length;

		// ja que cada indice vai de -25 a 25
		mean = mean * ( 100 / ( MAX_GAUGE_VALUE - MIN_GAUGE_VALUE) ) ;

		return mean;
	}

	public void tickActionStats( long delta ) {

		switch ( performingAction ) {

			case ACTION_SLEEP:
				incGauge( GAUGE_SLEEPY, delta, 0.025f );
				decGauge( GAUGE_BUYING, delta, 0.00005f );
				decGauge( GAUGE_CLEANESS, delta, 0.00001f );
				incGauge( GAUGE_FUN, delta, 0.00002f );
				incGauge( GAUGE_HUNGER, delta, 0.00015f );
				break;
				
			case ACTION_GIFT:
				decGauge( GAUGE_SLEEPY, delta, 0.0002f );
				incGauge( GAUGE_BUYING, delta, 0.00002f );
				decGauge( GAUGE_CLEANESS, delta, 0.0002f );
				incGauge( GAUGE_FUN, delta, 0.0005f );
				decGauge( GAUGE_HUNGER, delta, 0.00005f );
				break;

			case ACTION_BATH:
				incGauge( GAUGE_SLEEPY, delta, 0.002f );
				incGauge( GAUGE_BUYING, delta, 0.00002f );
				incGauge( GAUGE_CLEANESS, delta, 0.2f );
				incGauge( GAUGE_FUN, delta, 0.0005f );
				decGauge( GAUGE_HUNGER, delta, 0.00005f );
				break;

			case ACTION_DOOR:
				break;

			case ACTION_EAT:
				incGauge( GAUGE_SLEEPY, delta, 0.0005f );
				decGauge( GAUGE_BUYING, delta, 0.0001f );
				decGauge( GAUGE_CLEANESS, delta, 0.0003f );
				incGauge( GAUGE_FUN, delta, 0.0005f );
				incGauge( GAUGE_HUNGER, delta, 0.0025f );
				break;

			case ACTION_PLAY:
				incGauge( GAUGE_SLEEPY, delta, 0.0001f );
				decGauge( GAUGE_BUYING, delta, 0.0001f );
				decGauge( GAUGE_CLEANESS, delta, 0.0003f );
				incGauge( GAUGE_FUN, delta, 0.0004f );
				incGauge( GAUGE_HUNGER, delta, 0.0005f );
				break;

			case ACTION_PAINT:
				incGauge( GAUGE_SLEEPY, delta, 0.0001f );
				decGauge( GAUGE_BUYING, delta, 0.0001f );
				decGauge( GAUGE_CLEANESS, delta, 0.03f );
				incGauge( GAUGE_FUN, delta, 0.0004f );
				incGauge( GAUGE_HUNGER, delta, 0.0005f );
				break;

			case ACTION_READ:
				incGauge( GAUGE_SLEEPY, delta, 0.0001f );
				decGauge( GAUGE_BUYING, delta, 0.0001f );
				decGauge( GAUGE_CLEANESS, delta, 0.0003f );
				incGauge( GAUGE_FUN, delta, 0.0004f );
				incGauge( GAUGE_HUNGER, delta, 0.0005f );
				break;

			case ACTION_WEAR:
				incGauge( GAUGE_SLEEPY, delta, 0.0001f );
				decGauge( GAUGE_BUYING, delta, 0.0001f );
				incGauge( GAUGE_CLEANESS, delta, 0.03f );
				incGauge( GAUGE_FUN, delta, 0.0004f );
				incGauge( GAUGE_HUNGER, delta, 0.0005f );
				break;
		}		
	}

//#if DEBUG == "true"
//# 	private void DEBUG_printStats() {
//# 
//# 		for ( int c = 0; c < gauge.length; ++c ) {
//# 			System.out.println("[ " + c + " ] = " + gauge[ c ] );
//# 		}
//# 	}
//#endif
	
	/**
	 *
	 * @param gaugeIndex
	 * @param delta
	 * @param f
	 */
	private void incGauge( byte gaugeIndex, long delta, float f ) {
		gauge[ gaugeIndex ] += f * delta;
	}

	/**
	 *
	 * @param gaugeIndex
	 * @param delta
	 * @param f
	 */
	private void decGauge( byte gaugeIndex, long delta, float f ) {
		gauge[ gaugeIndex ] -= f * delta;
	}


	/**
	 *
	 * @return
	 */
	public int getDepressionCount() {
		return depressionCount;
	}

	/**
	 *
	 * @param input
	 */
	public void read( DataInputStream input ) {
		try {
			depressionCount = input.readInt();
			System.out.println( "depression count: " + depressionCount );

			for ( int c = 0; c < gauge.length; ++c ) 
				gauge[ c ] = input.readFloat();

			if ( depressionCount > MAX_DEPRESSION_COUNT + 1 ) {
				depressionCount = 0;
				giveBackSomeHappiness();
			}
			
			int lastAction = input.readInt();
			int n = input.readInt();

			actionsToPerform = new Queue( n + 1 );

			pushAction( lastAction );

			for ( int c = 0; c < n; ++c ) {
				pushAction( input.readInt() );
			}

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
//# 			ex.printStackTrace();
			//#endif
			resetStats();
		}
	}

	/**
	 *
	 * @param output
	 */
	public void write( DataOutputStream output ) {
		try {
			output.writeInt( depressionCount );
			System.out.println( "depression count: " + depressionCount );
			
			for ( int c = 0; c < gauge.length; ++c ) {
				output.writeFloat( gauge[c] );
			}
	
			output.writeInt( performingAction );
			int size = actionsToPerform.size();
			output.writeInt( size );
			for ( int c = 0; c < actionsToPerform.size(); ++c ) {
				output.writeInt( ( ( Integer ) actionsToPerform.pop() ).intValue() );
			}
		} catch ( Exception ex ) {
//#if DEBUG == "true"
//# 			ex.printStackTrace();
//#endif
		}
	}


	/**
	 * 
	 */
	private void updateMessage() {

		int msg = NO_MESSAGE;
		
		switch ( performingAction ) {

			case ACTION_DOOR:
				msg = TEXT_ACT_OUT_1;
				break;

			case ACTION_SEEK_CAMERA:
				msg = TEXT_ACT_ATTENTION_1;
				break;

			case ACTION_EAT:
				msg = TEXT_ACT_EAT_1;
				break;

			case ACTION_PLAY:
				msg = TEXT_ACT_PLAY_1;
				break;

			case ACTION_GIFT:
				msg = TEXT_ACT_BUY_1;
				break;

			case ACTION_SLEEP:
				msg = TEXT_ACT_SLEEP_1;
				break;

			case ACTION_STILL:
				msg = TEXT_ACT_STILL_1;
				break;

			case ACTION_BATH:
				msg = TEXT_ACT_BATH_1;
				break;
			}
		

		for ( int c = gauge.length - 1; c >= 0; --c ) {

			if ( gauge[ c ] <= ( MAX_DEPRESSION_COUNT + MIN_GAUGE_VALUE ) / 2 ) {
				
				switch ( c ) {

					case GAUGE_BUYING:
						msg = TEXT_NEED_BUY_1;
						break;

					case GAUGE_CLEANESS:
						msg = TEXT_NEED_CLEANESS_1;
						break;

					case GAUGE_FUN:
						msg = TEXT_NEED_FUN_1;
						break;

					case GAUGE_HUNGER:
						msg = TEXT_NEED_FOOD_1;
						break;

					case GAUGE_SLEEPY:
						msg = TEXT_NEED_SLEEP_1;
						break; 
				}
			}
		}

		if ( ( HelloKittyMessage.lastMsg != msg ) &&  ( msg != NO_MESSAGE ) && ( msg <= MAX_NEED_TEXT_INDEX || GameScreen.isKittySayingAnything() ) ) {

			if ( listener != null )
				listener.focusRequest();

			int randomness = NanoMath.randFixed( AMOUNT_MESSAGE_PER_MOOD );
			//HelloKittyMessage.msgType = randomness % ( AMOUNT_MESSAGE_PER_MOOD - 1 );
			HelloKittyMessage.msgType = MESSAGE_TYPE_THINK;
			GameScreen.showKittyMessage( AppMIDlet.getText( msg + randomness ) );
			timeToTalk = DEFAULT_TALK_INTERVAL;
		}

		HelloKittyMessage.lastMsg = msg;
	}


	public int getCurrentAction() {
		return performingAction;
	}


	private void updateFaceMood() {
		float happiness = getGeneralHappiness();
		
		int index = NanoMath.min( ( int ) ( ( happiness * MOODS ) / 100.0f ), MOODS - 1 );

		if ( currentMoodIndex != index && movingState == MOVINGSTATE_STILL ) {
			try {
				if ( currentMood != null ) {
					removeDrawable( currentMood );
				}
				currentMoodIndex = index;
				currentMood = new Sprite( PATH_MOODS + "mood_" + index );
				insertDrawable( currentMood );

			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 				ex.printStackTrace();
				//#endif
			}
		}

		if ( currentMood != null )
			currentMood.setVisible( movingState == MOVINGSTATE_STILL );
	}


	private void setActivityItem( String spriteName ) {
		if ( activityItemSprite != null )
			removeDrawable( activityItemSprite );

		if ( spriteName != null ) {
			try {
				activityItemSprite = new Sprite( PATH_OBJECT_IMAGES + spriteName );
				insertDrawable( activityItemSprite );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 				ex.printStackTrace();
				//#endif
			}
		}
	}
}