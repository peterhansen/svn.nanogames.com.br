package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Desk extends CustomizableObject implements GraphicalConstants, CoreConstants {
	private DrawableImage image;

	/**
	 *
	 */
	public Desk() {
		super( 1 );

		try {
			setImage( new DrawableImage( PATH_OBJECT_IMAGES + "desk.png" ) );
		} catch ( Exception ex ) {
			//#if DEBUG
//# 			ex.printStackTrace();
			//#endif
		}
	}


	public void read( DataInputStream input ) throws Exception {
	}


	public void write( DataOutputStream output ) {
	}
}
