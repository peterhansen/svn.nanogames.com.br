package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Gift extends CustomizableObject implements GraphicalConstants, CoreConstants {

	/**
	 *
	 */
	public Gift() {
		super( 1 );
		
		try {
			setImage( new DrawableImage( PATH_OBJECT_IMAGES + "gift.png" ) );
		} catch ( Exception ex ) {
			//#if DEBUG
//# 			ex.printStackTrace();
			//#endif
		}
	}

	public void read( DataInputStream input ) throws Exception {
	}

	public void write( DataOutputStream output ) {
	}	
}
