package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.util.Serializable;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Bed extends CustomizableObject implements GraphicalConstants, CoreConstants, Serializable {
	private DrawableImage image;

	/**
	 *
	 */
	public Bed() {
		super( 1 );

		try {
			setImage( image = new DrawableImage( PATH_OBJECT_IMAGES + "bed.png" ) );
		} catch ( Exception ex ) {
			//#if DEBUG
//# 			ex.printStackTrace();
			//#endif
		}
	}


	public void setPosition( int x, int y ) {
		super.setPosition( x, y - image.getHeight() / 3 );
	}


	public void read( DataInputStream input ) throws Exception {
	}


	public void write( DataOutputStream output ) {
	}
}
