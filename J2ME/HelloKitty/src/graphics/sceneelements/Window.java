package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Window extends CustomizableObject implements GraphicalConstants, CoreConstants {
	private DrawableImage windowFrame;
	private DrawableImage windowCurtains;
	private Sprite timeOfTheDay;

	/**
	 *
	 */
	public Window() {
		super( 3 );

		try {
			timeOfTheDay = new Sprite( PATH_OBJECT_IMAGES + "nightandday" );
			insertDrawable( timeOfTheDay );
			timeOfTheDay.setFrame( 1 );

			windowFrame = new DrawableImage( PATH_OBJECT_IMAGES + "window.png" );
			insertDrawable( windowFrame );

			windowCurtains = new DrawableImage( PATH_OBJECT_IMAGES + "curtain.png" );
			insertDrawable( windowCurtains );

			setSize( windowFrame.getSize() );
		} catch ( Exception ex ) {
			//#if DEBUG
//# 			ex.printStackTrace();
			//#endif
		}
	}

	public void read( DataInputStream input ) throws Exception {
	}


	public void write( DataOutputStream output ) {
	}

}
