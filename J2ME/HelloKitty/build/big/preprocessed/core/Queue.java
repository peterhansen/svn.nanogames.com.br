package core;

import java.util.Vector;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Queue {
	private Vector vector;
	int top;

	public Queue( int reserve ) {
		vector = new Vector( reserve );
		top = 0;
	}


	public Queue() {
		vector = new Vector();
		top = 0;
	}

	public void push( Object obj ) {
		vector.addElement( obj );
	}

	public Object pop() {
		top++;
		return vector.elementAt( top - 1 );
	}

	public void pack() {
		Vector tmp = new Vector( vector.size() - top );

		while ( !empty() ) {
			tmp.addElement( pop() );
		}

		top = 0;
		vector = tmp;
	}

	public boolean empty() {
		return size() == 0;
	}

	public int size() {
		return vector.size() - top;
	}
}
