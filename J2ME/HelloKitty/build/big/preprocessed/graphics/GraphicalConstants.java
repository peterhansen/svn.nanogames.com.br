package graphics;

import core.CoreConstants;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public interface GraphicalConstants {

	//telas de jogo
	public static final int SCREEN_NANO_SPLASH = 0;
	public static final int SCREEN_GAMESCREEN = 1;

	//caminhos
	public static final String PATH_IMAGES = "/images/";
	public static final String PATH_OBJECT_IMAGES = PATH_IMAGES + "objs/";

	//constantes de interface
	public static final byte DEFAULT_CAMERA_HORIZONTAL_SPEED = 3;

	public static final byte FONTS_TOTAL = 1;
	public static final byte DEFAULT_FONT_OFFSET = 0;

	public static final byte FONT_ACTIVITY_SELECTION = 0;
	public static final int DEFAULT_BOX_VISIBLE_TIME = ( int ) ( 5 * CoreConstants.SECONDS );
}
