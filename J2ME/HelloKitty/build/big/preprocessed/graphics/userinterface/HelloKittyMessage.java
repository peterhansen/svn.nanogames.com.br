package graphics.userinterface;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.hellokitty.GameMIDlet;

/**
 *
 * @author monteiro
 */
public class HelloKittyMessage extends UpdatableGroup implements core.CoreConstants, graphics.GraphicalConstants {

	public static final byte MSGTYPE_SAY = 0;
	public static final byte MSGTYPE_THOUGHT = 1;

	private Pattern backgroundSay;
	private Pattern backgroundThought;

	private RichLabel message;
	private int visibleTime;

	public static int lastMsg = -1;
	public static int msgType = MSGTYPE_SAY;


	public HelloKittyMessage() {
		super( 3 );
		try {

			backgroundSay = new Pattern( new DrawableImage( PATH_IMAGES + "dialogBackground0.png" ) );
			backgroundThought = new Pattern( new DrawableImage( PATH_IMAGES + "dialogBackground1.png" ) );
			
			insertDrawable( backgroundSay );
			insertDrawable( backgroundThought );

			backgroundThought.setVisible( false );
			backgroundSay.setVisible( true );

			message = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance() ).getFont( FONT_ACTIVITY_SELECTION ), "COMPRAS" );
			insertDrawable( message );

			setSize( ScreenManager.SCREEN_WIDTH,
					 ScreenManager.SCREEN_HEIGHT / 4 );

			setVisible( false );

		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}

	}

	/**
	 *
	 * @param msg
	 */
	public void setMessage( String msg ) {
		
		if ( backgroundThought != null )
			backgroundThought.setVisible( msgType == 0 );

		if ( backgroundSay != null )
			backgroundSay.setVisible( msgType != 0 );

		message.setText( msg, true );
	}

	/**
	 *
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		backgroundThought.setSize( width , height );
		backgroundSay.setSize( width , height );
		message.setSize( backgroundThought.getSize() );
	}


	/**
	 *
	 * @param visible
	 */
	public void setVisible( boolean visible ) {
		super.setVisible( visible );

		visibleTime = 0;
	}


	/**
	 *
	 * @param delta
	 */
	public void update( int delta ) {
		super.update( delta );

		if ( visibleTime > DEFAULT_BOX_VISIBLE_TIME ) {
			setVisible( false );
			lastMsg = -1;
		} else
			visibleTime += delta;
	}
}
