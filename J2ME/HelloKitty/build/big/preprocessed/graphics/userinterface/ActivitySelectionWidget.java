package graphics.userinterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.hellokitty.GameMIDlet;
import graphics.sceneelements.HelloKitty;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class ActivitySelectionWidget extends UpdatableGroup implements graphics.GraphicalConstants, core.CoreConstants
//#if TOUCH == "true"
		,PointerListener
//#endif
{
	public static final byte TOTAL_ACTIVITIES = 4;
	
	public static final byte[] actions = {
										HelloKitty.ACTION_GIFT,
										HelloKitty.ACTION_DOOR,
										HelloKitty.ACTION_SEEK_CAMERA,
										HelloKitty.ACTION_SLEEP
										};


	private Pattern background;
	private DrawableImage leftArrow;
	private DrawableImage rightArrow;
	private Drawable current;
	private int currentIndex;
	private Drawable[] options;
	private ActivitySelectionListener listener;


	public ActivitySelectionWidget( ActivitySelectionListener listener ) {
		super( 5 );

		this.listener = listener;

		try {
			background = new Pattern( new DrawableImage( PATH_IMAGES + "boxBackground.png" ) );
			leftArrow = new DrawableImage( PATH_IMAGES + "left.png" );
			rightArrow = new DrawableImage( PATH_IMAGES + "right.png" );
			options = new Drawable[ TOTAL_ACTIVITIES ];

			options[ 0 ] = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance()).getFont( FONT_ACTIVITY_SELECTION ), TEXT_ACTIVITY_BUY );
			options[ 0 ].setSize( ScreenManager.SCREEN_WIDTH - leftArrow.getWidth() - rightArrow.getWidth(), ScreenManager.SCREEN_HEIGHT / 5 );
			options[ 1 ] = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance()).getFont( FONT_ACTIVITY_SELECTION ), TEXT_ACTIVITY_PLAY );
			options[ 1 ].setSize( ScreenManager.SCREEN_WIDTH - leftArrow.getWidth() - rightArrow.getWidth(), ScreenManager.SCREEN_HEIGHT / 5 );
			options[ 2 ] = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance()).getFont( FONT_ACTIVITY_SELECTION ), TEXT_ACTIVITY_FOCUS );
			options[ 2 ].setSize( ScreenManager.SCREEN_WIDTH - leftArrow.getWidth() - rightArrow.getWidth(), ScreenManager.SCREEN_HEIGHT / 5 );
			options[ 3 ] = new RichLabel( ( ( GameMIDlet ) GameMIDlet.getInstance()).getFont( FONT_ACTIVITY_SELECTION ), TEXT_ACTIVITY_SLEEP );
			options[ 3 ].setSize( ScreenManager.SCREEN_WIDTH - leftArrow.getWidth() - rightArrow.getWidth(), ScreenManager.SCREEN_HEIGHT / 5 );

			insertDrawable( background );
			insertDrawable( leftArrow );
			insertDrawable( rightArrow );
			current = null;
			setSize( ScreenManager.SCREEN_WIDTH, 0 );
			setCurrentOption( 2 );
		} catch ( Exception ex ) {
			ex.printStackTrace();
		}

	}

	public void setSize( int width, int height ) {
		super.setSize( width, ScreenManager.SCREEN_HEIGHT / 5 );

		background.setSize( width, ScreenManager.SCREEN_HEIGHT / 5 );
		rightArrow.setPosition( width - rightArrow.getWidth(), ( getHeight() - rightArrow.getHeight() ) / 2 );
		leftArrow.setPosition( 0, ( getHeight() - leftArrow.getHeight() ) / 2 );
	}

	public void nextIndex () {
		
		currentIndex = ( ++currentIndex ) % TOTAL_ACTIVITIES;

		setCurrentOption( currentIndex );

		if ( listener != null ) {
			listener.itemHovered( currentIndex );
		}
	}


	public void prevIndex () {
		--currentIndex;

		if ( currentIndex < 0 )
			currentIndex = TOTAL_ACTIVITIES - currentIndex - 2;

		setCurrentOption( currentIndex );

		if ( listener != null ) {
			listener.itemHovered( currentIndex );
		}
	}

	private void setCurrentOption( int i ) {
		if ( current != null )
			removeDrawable( current );

		currentIndex = i;
		current = options[ i ];
		insertDrawable( current );
		current.setPosition( ( getWidth() - current.getWidth() ) / 2, ( getHeight() - current.getHeight() ) / 2 );
	}

	public void selectCurrent() {
		if ( listener != null ) {
			listener.itemSelection( currentIndex );
		}
	}

//#if TOUCH == "true"
	public void onPointerDragged( int x, int y ) {
	}


	public void onPointerPressed( int x, int y ) {
		if ( leftArrow.contains( x, y ) )
			prevIndex();

		if ( current.contains( x, y ) )
			selectCurrent();

		if ( rightArrow.contains( x, y ) )
			nextIndex();
	}


	public void onPointerReleased( int x, int y ) {
	}
//#endif
}
