/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graphics.userinterface;

/**
 *
 * @author monteiro
 */
public interface ActivitySelectionListener {


	public void itemSelection( int currentIndex );

	public void itemHovered( int currentIndex );

}
