package graphics.screens;

import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.CoreConstants;
import graphics.GraphicalConstants;
import graphics.sceneelements.Bed;
import graphics.sceneelements.CustomizableObject;
import graphics.sceneelements.Door;
import graphics.sceneelements.Floor;
import graphics.sceneelements.Gift;
import graphics.sceneelements.HelloKitty;
import graphics.sceneelements.Wall;
import graphics.userinterface.ActivitySelectionListener;
import graphics.userinterface.ActivitySelectionWidget;
import graphics.userinterface.HelloKittyMessage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class GameScreen extends UpdatableGroup implements GraphicalConstants, CoreConstants, KeyListener, ActivitySelectionListener, Serializable
//#if TOUCH == "true"
		,PointerListener
//#endif
{
	
	public static final int TOTAL_ITEMS = 20;
	public static final int MAX_ROOM_OBJECTS = 20;
	public static final int GROUND_LEVEL_FAR = 3 * ScreenManager.SCREEN_HEIGHT / 5;
	public static final int GROUND_LEVEL_NEAR = GROUND_LEVEL_FAR +  ScreenManager.SCREEN_HEIGHT / 10;

	private static GameScreen instance = null;

	//objetos de cenário
	private Floor floor;
	private Wall wall;
	private UpdatableGroup roomObjects;
	private UpdatableGroup wallDecorations;
	private Door door;
	private HelloKitty helloKitty;
	private Gift gift;
	private Bed bed;
	
	//objetos de interface
	private Pattern background;
	private Point cameraSpeed;
	private Point cameraPosition;
	private CustomizableObject cameraTarget;
	private ActivitySelectionWidget activities;
	private long hoveredTime;
	private Vector roomObjectList;
	private Vector wallDecorationList;
	private HelloKittyMessage kittyMessage;

	/***
	* Construtor - apenas instancia e insere. Não define tamanho nem estado inicial
	 */
	public GameScreen() {

		super( TOTAL_ITEMS  );

		roomObjectList = new Vector();
		wallDecorationList = new Vector();
		background = new Pattern( 0x0000FF );
		cameraSpeed = new Point();
		cameraPosition = new Point();
		wall = new Wall();
		floor = new Floor();
		helloKitty = new HelloKitty( cameraPosition );
		helloKitty.setRoomPosition( ROOM_WIDTH / 2, GROUND_LEVEL_NEAR );
		helloKitty.moveTo( 100 );
		
		//monta a display list:
		//fundo
		insertDrawable( background );

		//a sala
		insertDrawable( wall );
		insertDrawable( floor );		
		
		//objetos na sala
		roomObjects = new UpdatableGroup( MAX_ROOM_OBJECTS );
		insertDrawable( roomObjects );

		wallDecorations = new UpdatableGroup( MAX_ROOM_OBJECTS );
		insertDrawable( wallDecorations );

		gift = new Gift();
		addRoomElementAt( gift, new Point( ROOM_WIDTH / 4, GROUND_LEVEL_NEAR ) );

		bed = new Bed();
		addRoomElementAt( bed, new Point( ROOM_WIDTH / 2, GROUND_LEVEL_FAR ) );


		door = new Door();
		addWallDecorationAt( door, new Point( ( 3 * ROOM_WIDTH ) / 4, GROUND_LEVEL_FAR - door.getHeight() ) );

		insertDrawable( helloKitty );

		activities = new ActivitySelectionWidget( this );
		insertDrawable( activities );

		kittyMessage = new HelloKittyMessage();
		insertDrawable( kittyMessage );
		
		activities.setPosition( 0, ScreenManager.SCREEN_HEIGHT - activities.getHeight() );

		cameraTarget = helloKitty;

		instance = this;
	}


	protected void paint( Graphics g ) {
		super.paint( g );

		g.setColor( 0xFF0000 );
		g.fillRect( 0, ScreenManager.SCREEN_HEIGHT / 5, ( int ) ( ( ScreenManager.SCREEN_WIDTH / 100 ) * helloKitty.getGeneralHappiness() ), 20 );

		for ( int c = 0; c < 5; ++c ) {
			g.setColor( c * ( 0xFFFFFF / 5 ) );
			g.fillRect( c * 20,( ScreenManager.SCREEN_HEIGHT / 5 ) + 20, 20, ( int ) ( helloKitty.getStat( c ) + 25 ) );
		}
  
	}


	/***
	* Enquanto que as coisas foram instanciadas, elas não foram devidamente inicializadas.
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		background.setSize( width, height );

		//parede ocupa 3/5 da tela.
		wall.setSize( width, ( height * 3 ) / 5 );

		// e por fim, o teto
		floor.setSize( width, height / 5 );

		roomObjects.setSize( ROOM_WIDTH, ScreenManager.SCREEN_HEIGHT );
		wallDecorations.setSize( ROOM_WIDTH, 3 * height / 5 );

		cameraPosition.x = ROOM_WIDTH >> 2;
	}

	public void addWallDecorationAt( CustomizableObject item, Point pos ) {
		wallDecorations.insertDrawable(  item );
		wallDecorationList.addElement( item );
		item.setPosition( pos );
		item.setRoomPosition( pos );
	}


	public void addRoomElementAt( CustomizableObject item, Point pos ) {
		roomObjects.insertDrawable(  item );
		roomObjectList.addElement( item );
		item.setPosition( pos );
		item.setRoomPosition( pos );
	}

	public void update( int delta ) {
		super.update( delta );

		cameraPosition.addEquals(  cameraSpeed );

		if ( cameraPosition.x >= - getWidth() / 2 && ( cameraPosition.x + ( getWidth() ) / 2 ) <= ROOM_WIDTH ) {
			//...e começa onde termina o teto
			wall.setPosition( - cameraPosition.x, cameraPosition.y );
			// e o chão começa onde a parede termina
			floor.setPosition( - cameraPosition.x, cameraPosition.y + wall.getPosY() + wall.getHeight() );

			roomObjects.setPosition( - cameraPosition.x, 0 );
			wallDecorations.setPosition( - cameraPosition.x, wall.getPosY() );
		}

		if ( cameraTarget != null )
			seek( cameraTarget );

		if ( hoveredTime < 0 )
			cameraTarget = helloKitty;
		else
			hoveredTime -= delta;
	}

	public static boolean isKittySayingAnything() {
		return instance.kittyMessage.isVisible();
	}

	public static void showKittyMessage( String msg ) {
		if ( instance != null ) {
			instance.kittyMessage.setVisible( true );
			instance.kittyMessage.setMessage( msg );
		}
	}

	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_LEFT:
					activities.prevIndex();
				break;
			case ScreenManager.KEY_RIGHT:
					activities.nextIndex();
				break;
			case ScreenManager.KEY_UP:
					helloKitty.setBodyPiece( "yellowshirt" );
				break;
			case ScreenManager.KEY_DOWN:
					helloKitty.setBodyPiece( "redshirt" );
				break;
			case ScreenManager.KEY_FIRE:
					activities.selectCurrent();
				break;

		}
	}


	public void keyReleased( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_LEFT:
			case ScreenManager.KEY_RIGHT:
				cameraSpeed.x = 0;
				break;
		}
	}


	private void seek( CustomizableObject target ) {

		int diff = ( target.getRoomPosition().x - cameraPosition.x - ScreenManager.SCREEN_HALF_WIDTH );

		int normal = 0;

		if ( diff != 0 )
			normal = diff / NanoMath.abs( diff );

		if ( NanoMath.abs( diff ) > NanoMath.abs( DEFAULT_CAMERA_HORIZONTAL_SPEED ) ) {
			cameraSpeed.x = normal * DEFAULT_CAMERA_HORIZONTAL_SPEED;
		} else
			cameraSpeed.x = 0;
	}


	public void itemSelection( int currentIndex ) {
		switch ( currentIndex ) {

			case HelloKitty.ACTION_DOOR:
				helloKitty.moveToAndPerform( door.getRoomPosition().x, HelloKitty.ACTION_DOOR );
				break;

			case HelloKitty.ACTION_GIFT:
				helloKitty.moveToAndPerform( gift.getRoomPosition().x, HelloKitty.ACTION_PLAY );
				break;

			case HelloKitty.ACTION_SLEEP:
				helloKitty.moveToAndPerform( bed.getRoomPosition().x, HelloKitty.ACTION_SLEEP );
				break;
		}
	}


	public void itemHovered( int currentIndex ) {
		switch ( currentIndex ) {

			case HelloKitty.ACTION_DOOR:
				cameraTarget = door;
				break;

			case HelloKitty.ACTION_SLEEP:
				cameraTarget = bed;
				break;

			case HelloKitty.ACTION_GIFT:
				cameraTarget = gift;
				break;

			case HelloKitty.ACTION_SEEK_CAMERA:
				cameraTarget = helloKitty;
				break;
		}
		
		//10 segundos
		hoveredTime = 10000;
	}

//#if TOUCH == "true"
	public void onPointerDragged( int x, int y ) {
	}


	public void onPointerPressed( int x, int y ) {
		activities.onPointerPressed( x - activities.getPosX(), y - activities.getPosY() );
	}


	public void onPointerReleased( int x, int y ) {
	}


//#endif

	public void write( DataOutputStream output ) throws Exception {

		if ( helloKitty.getDepressionCount() == MAX_DEPRESSION_COUNT + 1 )
			return;

		long lastTime = System.currentTimeMillis();
		output.writeLong( lastTime );

		helloKitty.write( output );

		for ( int c = 0; c < roomObjectList.size(); ++c ) {
			( ( CustomizableObject ) roomObjectList.elementAt( c ) ).write( output );
		}

		for ( int c = 0; c < wallDecorationList.size(); ++c ) {
			( ( CustomizableObject ) wallDecorationList.elementAt( c ) ).write( output );
		}
	}


	public void read( DataInputStream input ) throws Exception {
		long lastTime = input.readLong();

		helloKitty.read( input );
		helloKitty.updateStats( ( System.currentTimeMillis() - lastTime ) );

		for ( int c = 0; c < roomObjectList.size(); ++c ) {
			( ( CustomizableObject ) roomObjectList.elementAt( c ) ).read( input );
		}

		for ( int c = 0; c < wallDecorationList.size(); ++c ) {
			( ( CustomizableObject ) wallDecorationList.elementAt( c ) ).read( input );
		}
	}
}