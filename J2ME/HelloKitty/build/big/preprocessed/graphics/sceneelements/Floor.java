package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Floor extends CustomizableObject implements GraphicalConstants, CoreConstants {

	private DrawableImage image;
	private Pattern imagePattern;

	/**
	 * 
	 */
	public Floor() {
		super( 1 );
		try {
			image = new DrawableImage( PATH_OBJECT_IMAGES + "floor.png" );
			imagePattern = new Pattern( image );
			insertDrawable(  imagePattern );
		} catch ( Exception ex ) {
			//#if DEBUG
			ex.printStackTrace();
			//#endif
		}
	}

	/**
	 * Define o tamanho do objeto - mas a largura é ignorada.
	 * Apenas a altura é usada. A largura depende de CoreConstants.ROOM_WIDTH.
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		super.setSize( ROOM_WIDTH, height );

		imagePattern.setSize( ROOM_WIDTH, height );
	}


	public void read( DataInputStream input ) throws Exception {
		super.read( input );
	}


	public void write( DataOutputStream output ) {
		super.write( output );
	}	
}
