package graphics.sceneelements;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import core.CoreConstants;
import graphics.GraphicalConstants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Door extends CustomizableObject implements GraphicalConstants, CoreConstants {
	private DrawableImage image;	

	/**
	 *
	 */
	public Door() {
		super( 1 );

		try {
			image = new DrawableImage( PATH_OBJECT_IMAGES + "door.png" );
			insertDrawable( image );
			setSize( image.getSize() );
		} catch ( Exception ex ) {
			//#if DEBUG
			ex.printStackTrace();
			//#endif
		}
	}


	public void write( DataOutputStream output ) {
		super.write( output );
	}


	public void read( DataInputStream input ) throws Exception {
		super.read( input );
	}
}

