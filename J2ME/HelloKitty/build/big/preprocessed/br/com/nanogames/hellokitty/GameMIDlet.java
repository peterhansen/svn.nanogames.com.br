package br.com.nanogames.hellokitty;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.CoreConstants;
import graphics.GraphicalConstants;
import graphics.screens.GameScreen;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * @author Daniel "Monty " Monteiro
 */
public class GameMIDlet extends AppMIDlet implements GraphicalConstants, CoreConstants, Serializable {


	public void onGameOver() {
		exit();
	}

	private GameScreen gameScreen;

	public GameMIDlet() {
		super();
		language = NanoOnline.LANGUAGE_pt_BR;
		FONTS = new ImageFont[ FONTS_TOTAL ];
	}

	/**
	 * 
	 * @param screen
	 * @return
	 * @throws Exception
	 */
	protected int changeScreen( int screen ) throws Exception {
		Drawable nextScreen = null;

		switch ( screen ) {
			case SCREEN_NANO_SPLASH:
				//nextScreen = new BasicSplashNano( SCREEN_GAMESCREEN, BasicSplashNano.SCREEN_SIZE_MEDIUM, _FORMAT_DDMMYY, screen, screen)
				break;
			case SCREEN_GAMESCREEN:
				gameScreen = new GameScreen();
				nextScreen = gameScreen;
				break;
		}

		nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		ScreenManager.getInstance().setCurrentScreen( nextScreen );		

		return screen;
	}


	public ImageFont getFont( int fontIndex ) {
		return super.getFont( fontIndex );
	}



	/**
	 *
	 * @throws Exception
	 */
	protected void loadResources() throws Exception {

		//<editor-fold desc="LOADING FONTS">
			for ( byte i = 0; i < FONTS_TOTAL; ++i ) {

				switch ( i ) {
					
					default:
						FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
				}

				switch ( i ) {

					default:
							FONTS[ i ].setCharExtraOffset( DEFAULT_FONT_OFFSET );
				}
			}
		//</editor-fold>

		loadTexts();
		changeScreen( SCREEN_GAMESCREEN );
		loadRMSData();
	}

	public void loadTexts() {
		try {
			loadTexts( TEXTS_TOTAL, PATH_TEXTS + "texts_" + language + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif

			exit();
		}
	}

	public void destroy() {
		try {
			saveData( DATABASE_NAME, GAME_DATA_SLOT, this );
		} catch ( Exception ex ) {
//#if DEBUG == "true"
			ex.printStackTrace();
//#endif
		}

		super.destroy();
	}




	private void loadRMSData() {
		try {
			createDatabase( DATABASE_NAME, GAME_DATA_SLOT + 1 );
		} catch ( Exception ex ) {
//#if DEBUG == "true"
			ex.printStackTrace();
//#endif
		}
		try {
			loadData( DATABASE_NAME, GAME_DATA_SLOT, this );
		} catch ( Exception ex ) {
//#if DEBUG == "true"
			ex.printStackTrace();
//#endif

		}
	}


	public void write( DataOutputStream output ) throws Exception {

		if ( gameScreen != null )
			gameScreen.write( output );
	}


	public void read( DataInputStream input ) throws Exception {

		if ( gameScreen != null )
			gameScreen.read( input );
	}
}