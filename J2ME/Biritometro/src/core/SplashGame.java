/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;

//#if SCREEN_SIZE != "SMALL"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public final class SplashGame extends DrawableGroup implements Constants, Updatable, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener 
		//#endif
{
	
	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 10;
	
	/** tempo restante de exposi��o da tela de splash do jogo */
	private short visibleTime = 2700;
	
	/** texto indicando "pressione qualquer tecla" */
	private final RichLabel pressKeyLabel;
	
	/** velocidade do "pisca-pisca" do label */
	private final short BLINK_RATE = -666; // dem�in! >:)
	
	
	public SplashGame( ImageFont font ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final int OFFSET_X = NanoMath.max( ( size.x - SPLASH_DEFAULT_WIDTH ) >> 1, 0 );
		final int OFFSET_Y = NanoMath.max( ( size.y - SPLASH_DEFAULT_HEIGHT ) >> 1, 0 );
		
		final Pattern bkg = new Pattern( new DrawableImage( PATH_SPLASH + "pattern.png" ) );
		bkg.setSize( size );
		insertDrawable( bkg );
		
		final DrawableImage graph = new DrawableImage( PATH_SPLASH + "iso.png" );
		graph.setPosition( OFFSET_X + SPLASH_GRAPH_X, OFFSET_Y + SPLASH_GRAPH_Y );
		insertDrawable( graph );
		
		final DrawableImage title = new DrawableImage( PATH_SPLASH + "title.png" );
		title.setPosition( OFFSET_X + SPLASH_TITLE_X, OFFSET_Y + SPLASH_TITLE_Y );
		insertDrawable( title );
		
		if ( title.getPosX() + title.getWidth() < size.x ) {
			// insere o pattern � direita do t�tulo
			final Pattern p = new Pattern( new DrawableImage( PATH_SPLASH + "title_p.png" ) );
			p.setPosition( title.getPosX() + title.getWidth(), title.getPosY() + title.getHeight() - p.getFill().getHeight() );
			p.setSize( size.x - p.getPosX(), p.getFill().getHeight() );
			insertDrawable( p );
		}
		
		final DrawableImage subtitle = new DrawableImage( PATH_SPLASH + "sub.png" );
		subtitle.setPosition( OFFSET_X + SPLASH_SUBTITLE_X, OFFSET_Y + SPLASH_SUBTITLE_Y );
		insertDrawable( subtitle );
		
		final DrawableImage nano = new DrawableImage( PATH_SPLASH + "nano_2.png" );
		nano.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_RIGHT );
		nano.setRefPixelPosition( size );
		insertDrawable( nano );
		
		pressKeyLabel = new RichLabel( font, AppMIDlet.getText( TEXT_PRESS_ANY_KEY ), ( size.x * 3 ) >> 2, null );
		pressKeyLabel.setPosition( ( size.x - pressKeyLabel.getSize().x ) >> 1, size.y - pressKeyLabel.getSize().y );
		pressKeyLabel.setVisible( false );
		insertDrawable( pressKeyLabel );
	}


	public final void update( int delta ) {
		visibleTime -= delta;
		
		if ( visibleTime <= BLINK_RATE ) {
			// pisca o texto "Pressione qualquer tecla"
			pressKeyLabel.setVisible( !pressKeyLabel.isVisible() );
			visibleTime %= BLINK_RATE;
		}
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
		// permite pular a tela de splash
		visibleTime = 0;
		//#endif
		
		if ( visibleTime <= 0 ) {
			//#if DEMO == "true"
//# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
			//#else
			GameMIDlet.setScreen( SCREEN_LOADING_1 );
			//#endif
		}
	}


	public final void keyReleased( int key ) {
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	//#endif

}
