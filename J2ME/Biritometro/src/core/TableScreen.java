/**
 * TableScreen.java
 * �2008 Nano Games.
 *
 * Created on Jul 18, 2008 9:57:16 PM.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;


/**
 * 
 * @author Peter
 */
public final class TableScreen extends UpdatableGroup implements Constants, KeyListener, PointerListener {
	
	private static final byte POINTER_ACTION_NONE		= 0;
	private static final byte POINTER_ACTION_PRESSED	= 1;
	private static final byte POINTER_ACTION_DRAGGED	= 2;
	private static final byte POINTER_ACTION_RELEASED	= 3;

	private static final byte TOTAL_ITEMS = Birita.TYPES_TOTAL + 60;
	
	public static final byte CONTROL_MODE_MOVE = 0;
	public static final byte CONTROL_MODE_EDIT = 1;
	
	private static final byte LINE_WIDTH = 1;
	
	public static final int[] fp_graphValues = new int[ TOTAL_HOURS ];
	
	private final Birita[] biritas = new Birita[ Birita.TYPES_TOTAL ];
	
	/** Grupo que cont�m os labels das quantidades de cada birita e o cursor. */
	private final UpdatableGroup labelGroup;
	
	/** Grupo que cont�m os labels dos nomes de cada birita. */
	private final DrawableGroup biritaNameGroup;
	
	/** Painel que cont�m o grupo de labels (usado para mover o grupo). */
	private final UpdatableGroup labelPanel;
	
	/** Grupo que cont�m os labels das horas. */
	private final UpdatableGroup hourGroup;
	
	/** Painel que cont�m o grupo das horas. */
	private final UpdatableGroup hourPanel;
	
	private final RichLabel[] labelHour;
	
	private byte controlMode = CONTROL_MODE_MOVE;
	
	private Birita currentBirita;
	
	private byte biritaIndex;
	
	private byte currentHour;
	
	private final Pattern cursor;
	
	private static final short CURSOR_CHANGE_TIME = 222;
	
	/** Velocidade de movimenta��o horizontal do cursor. */
	private final MUV tableMUVX = new MUV();
	
	/** Velocidade de movimenta��o vertical do cursor. */
	private final MUV tableMUVY = new MUV();
	
	/** Limite de movimenta��o horizontal do cursor. */
	private short tableXLimit;
	
	/** Limite de movimenta��o vertical do cursor. */
	private short tableYLimit;
	
	/** Tecla sendo mantida pressionada. */
	private int keyHeld;
	
	/** Tempo que a tecla est� sendo mantida pressionada. */
	private short keyAccTime;
	
	/** �ltima posi��o do ponteiro. */
	private final Point lastPointerPos = new Point();
	
	private byte lastPointerAction;
	
	private final short BIRITA_WIDTH;
	
	private BiritaEditor editor;
	
	/** 
	 * �cone do gr�fico - n�o � utilizado o funcionamento padr�o do softkey em ScreenManager, pois o �cone deve 
	 * ser desenhado sob o editor de birita (caso esteja vis�vel).
	 */
	private final DrawableImage softKeyLeft;
	
	/**
	 * �cone do softkey direito - n�o � utilizado o funcionamento padr�o do softkey em ScreenManager, pois o �cone deve 
	 * ser desenhado sob o editor de birita (caso esteja vis�vel).
	 */
	private final DrawableGroup softKeyRight;
	
	/** �ndice do editor no grupo. */
	private short editorIndex = -1;
	
	
	public TableScreen() throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final ImageFont font = GameMIDlet.getFont( FONT_INDEX_OUTLINE );
		final int FONT_HEIGHT = font.getHeight();
		
		Pattern p = null;
		
		hourGroup = new UpdatableGroup( TOTAL_HOURS << 1 );
		hourGroup.setSize( COLLUMN_WIDTH * TOTAL_HOURS, FONT_HEIGHT );
		
		p = new Pattern( COLOR_TABLE_BLUE );
		p.setSize( size.x, hourGroup.getHeight() );
		insertDrawable( p );
		
		labelGroup = new UpdatableGroup( ( TOTAL_HOURS * Birita.TYPES_TOTAL * 2 ) + 1 );
		labelGroup.setSize( hourGroup.getWidth(), Birita.TYPES_TOTAL * ROW_HEIGHT );
		
		// insere a primeira linha de labels
		BIRITA_WIDTH = ( short ) font.getTextWidth( "Champanhe" );
		final Label title = new Label( font, GameMIDlet.getText( TEXT_BIRITAS ) );
		title.setPosition( ( BIRITA_WIDTH - title.getWidth() ) >> 1, 0 );
		
		biritaNameGroup = new DrawableGroup( Birita.TYPES_TOTAL << 1 );
		
		final Pattern biritaNamePattern = new Pattern( COLOR_TABLE_BLUE );
		biritaNamePattern.setSize( BIRITA_WIDTH, size.y  );
		
		insertDrawable( biritaNamePattern );
		insertDrawable( title );
		
		for ( int i = 0, y = 0; i < Birita.TYPES_TOTAL; ++i, y += ROW_HEIGHT ) {
			// insere os t�tulos das biritas (primeira coluna)
			final Birita birita = new Birita( font, i );
			biritas[ i ] = birita;
			
			final Label biritaName = new RichLabel( font, "<ALN_H>" + birita.getName(), BIRITA_WIDTH  );
			biritaName.setPosition( 0, y + FONT_HEIGHT + ( ( ROW_HEIGHT - biritaName.getHeight() ) >> 1 ) );
			
			biritaNameGroup.insertDrawable( biritaName );
			
			for ( int j = 0, x = 0; j < TOTAL_HOURS; ++j, x += COLLUMN_WIDTH ) {
				final RichLabel label = birita.labels[ j ];
				label.setPosition( x, y );
				
				labelGroup.insertDrawable( label );
			}
			
			// insere a linha sobre o label
			p = new Pattern( COLOR_LINE );
			p.setPosition( 0, y + FONT_HEIGHT );
			p.setSize( size.x, LINE_WIDTH );
			biritaNameGroup.insertDrawable( p );
		}
		insertDrawable( biritaNameGroup );
		biritaNameGroup.setSize( size.x, labelGroup.getHeight() );
		biritaNameGroup.setViewport( new Rectangle( 0, hourGroup.getHeight(), size.x, size.y - hourGroup.getHeight() ) );
		
		// insere as linhas separadoras de cada hora e os labels das horas
		labelHour = new RichLabel[ TOTAL_HOURS ];
		for ( int i = 0, x = 0; i <= TOTAL_HOURS; ++i, x += COLLUMN_WIDTH ) {
			if ( i < TOTAL_HOURS ) {
				final RichLabel hour = new RichLabel( font, "<ALN_H>" + ( ( User.getInitialHour() + i ) % 24 ) + "h", COLLUMN_WIDTH );
				labelHour[ i ] = hour;
				hour.setSize( COLLUMN_WIDTH, FONT_HEIGHT );
				hour.setPosition( x, 0 );
				
				hourGroup.insertDrawable( hour );				
			}
			
			// insere a linha � esquerda do label
			p = new Pattern( COLOR_LINE );
			p.setPosition( x, 0 );
			p.setSize( LINE_WIDTH, labelGroup.getHeight() );
			labelGroup.insertDrawable( p );
			
			p = new Pattern( COLOR_LINE );
			p.setPosition( x, 0 );
			p.setSize( LINE_WIDTH, hourGroup.getHeight() );
			hourGroup.insertDrawable( p );
		}
		
		// insere e posiciona o painel dos labels de quantidades
		labelPanel = new UpdatableGroup( 2 );
		
		cursor = new Pattern( COLOR_CURSOR );
		cursor.setSize( COLLUMN_WIDTH, ROW_HEIGHT - 1 );
		labelPanel.insertDrawable( cursor );		
		
		labelPanel.setPosition( BIRITA_WIDTH, FONT_HEIGHT  );
		labelPanel.setSize( size.x - labelPanel.getPosX(), size.y - labelPanel.getPosY() );
		labelPanel.insertDrawable( labelGroup );
		insertDrawable( labelPanel );		
		
		// insere e posiciona o painel dos labels das horas
		hourPanel = new UpdatableGroup( 1 );
		hourPanel.setPosition( BIRITA_WIDTH, 0  );
		hourPanel.setSize( labelPanel.getWidth(), FONT_HEIGHT );
		hourPanel.insertDrawable( hourGroup );
		insertDrawable( hourPanel );		
		
		p = new Pattern( COLOR_LINE );
		p.setPosition( BIRITA_WIDTH, 0  );
		p.setSize( LINE_WIDTH, size.y );
		insertDrawable( p );
		
		p = new Pattern( COLOR_LINE );
		p.setPosition( 0, FONT_HEIGHT );
		p.setSize( size.x, LINE_WIDTH );
		insertDrawable( p );		
		
		softKeyLeft = new DrawableImage( PATH_IMAGES + "graph.png" );
		softKeyLeft.setPosition( 1, ScreenManager.SCREEN_HEIGHT - softKeyLeft.getHeight() - 1 );
		insertDrawable( softKeyLeft );
		
		softKeyRight = GameMIDlet.loadSoftKeyGroup( ScreenManager.SOFT_KEY_RIGHT, TEXT_MENU );
		softKeyRight.setPosition( ScreenManager.SCREEN_WIDTH - softKeyRight.getWidth(), ScreenManager.SCREEN_HEIGHT - softKeyRight.getHeight() );
		insertDrawable( softKeyRight );
		
		setCurrentBirita( 0 );
	}


	public final void keyPressed( int key ) {
		switch ( controlMode ) {
			case CONTROL_MODE_MOVE:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						setCurrentBirita( biritaIndex - 1 );
					break;

					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						setHour( currentHour - 1 );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						setHour( currentHour + 1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						setCurrentBirita( biritaIndex + 1 );
					break;					

					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						setControlMode( CONTROL_MODE_EDIT );
					return;
					
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_NUM0:
						// mostra o gr�fico
						updateGraphValues();
						GameMIDlet.setScreen( SCREEN_GRAPH );
					return;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						GameMIDlet.setScreen( SCREEN_MAIN_MENU );
					return;					
				}
			break;

			case CONTROL_MODE_EDIT:
				editor.keyPressed( key );
			return;
		} // fim switch ( controlMode )
		
		keyHeld = key;
	} // fim do m�todo keyPressed( int )


	public final void keyReleased( int key ) {
		if ( editor == null ) {
			keyHeld = 0;
			keyAccTime = 0;
		} else {
			editor.keyReleased( key );
		}
	}


	public final void onPointerDragged( int x, int y ) {
		switch ( controlMode ) {
			case CONTROL_MODE_MOVE:
				moveLabelGroup( x - lastPointerPos.x, y - lastPointerPos.y );
				
				lastPointerPos.set( x, y );
				lastPointerAction = POINTER_ACTION_DRAGGED;
			break;
			
			case CONTROL_MODE_EDIT:
				editor.onPointerDragged( x - editor.getPosX(), y - editor.getPosY() );
			break;
		}
	}


	public final void onPointerPressed( int x, int y ) {
		switch ( controlMode ) {
			case CONTROL_MODE_MOVE:
				if ( softKeyLeft.contains( x, y ) ) {
					keyPressed( ScreenManager.KEY_SOFT_LEFT );
				} else if ( softKeyRight.contains( x, y ) ) {
					keyPressed( ScreenManager.KEY_SOFT_RIGHT );
				} else {
					lastPointerPos.set( x, y );
					lastPointerAction = POINTER_ACTION_PRESSED;
				}
			break;
			
			case CONTROL_MODE_EDIT:
				editor.onPointerPressed( x - editor.getPosX(), y - editor.getPosY() );
			break;
		}
	}
	
	
	private final Point getCellAt( int x, int y ) {
		return new Point( ( x - labelGroup.getPosX() - BIRITA_WIDTH ) / COLLUMN_WIDTH,
						  ( y - labelGroup.getPosY() - labelPanel.getPosY() ) / ROW_HEIGHT );
	}


	public final void onPointerReleased( int x, int y ) {
		switch ( controlMode ) {
			case CONTROL_MODE_MOVE:
				lastPointerPos.set( x, y );
				
				switch ( lastPointerAction ) {
					case POINTER_ACTION_DRAGGED:
					break;
					
					case POINTER_ACTION_PRESSED:
						final Point cell = getCellAt( x, y );
						
						if ( currentHour == cell.x && biritaIndex == cell.y ) {
							setControlMode( CONTROL_MODE_EDIT );
						} else {
							setHour( cell.x );

							setCurrentBirita( cell.y );
						}
					break;
				}
				
				lastPointerAction = POINTER_ACTION_RELEASED;
			break;
			
			case CONTROL_MODE_EDIT:
				editor.onPointerReleased( x - editor.getPosX(), y - editor.getPosY() );
			break;
		}		
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		if ( keyHeld != 0 ) {
			keyAccTime += delta;
			
			if ( keyAccTime >= KEY_REPEAT_TIME ) {
				keyAccTime %= KEY_REPEAT_TIME;
				
				keyPressed( keyHeld );
			}
		} // fim if ( keyHeld != 0 )
		
		// atualiza a movimenta��o horizontal do cursor
		if ( tableMUVX.getSpeed() != 0 || tableMUVY.getSpeed() != 0 )
			moveLabelGroup( tableMUVX.updateInt( delta ), tableMUVY.updateInt( delta ) );
	}
	
	
	private final void moveLabelGroup( int dx, int dy ) {
		labelGroup.move( dx, dy );

		if ( tableMUVX.getSpeed() == 0 && tableMUVY.getSpeed() == 0 ) {
			labelGroup.setPosition( NanoMath.clamp( labelGroup.getPosX(), labelPanel.getWidth() - labelGroup.getWidth(), 0 ),
									NanoMath.clamp( labelGroup.getPosY(), labelPanel.getHeight() - labelGroup.getHeight(), 0 ) );
		} else {
			if ( ( tableMUVX.getSpeed() < 0 && labelGroup.getPosX() <= tableXLimit ) || 
				 ( tableMUVX.getSpeed() > 0 && labelGroup.getPosX() >= tableXLimit ) ) {
				labelGroup.setPosition( tableXLimit, labelGroup.getPosY() );
				tableMUVX.setSpeed( 0 );
			}
			
			if ( ( tableMUVY.getSpeed() < 0 && labelGroup.getPosY() <= tableYLimit ) || 
				 ( tableMUVY.getSpeed() > 0 && labelGroup.getPosY() >= tableYLimit ) ) {
				labelGroup.setPosition( labelGroup.getPosX(), tableYLimit );
				tableMUVY.setSpeed( 0 );
			}
		}
		
		hourGroup.setPosition( labelGroup.getPosX(), hourGroup.getPosY() );	
		biritaNameGroup.setPosition( 0, labelGroup.getPosY() );		
		
		updateCursorPosition();		
		
//		System.out.println( labelGroup.getPosX() + ", " + labelGroup.getPosY() );
	}
	
	
	private final void setCurrentBirita( int biritaIndex ) {
		this.biritaIndex = ( byte ) ( ( biritaIndex + Birita.TYPES_TOTAL ) % Birita.TYPES_TOTAL );
		currentBirita = biritas[ this.biritaIndex ];
		
		final int RELATIVE_Y = labelGroup.getPosY() + ( this.biritaIndex * ROW_HEIGHT );
		if ( RELATIVE_Y > labelPanel.getHeight() - cursor.getHeight() ) {
			tableYLimit = ( short ) ( labelGroup.getPosY() + labelPanel.getHeight() - cursor.getHeight() - RELATIVE_Y );
		} else if ( RELATIVE_Y < 0 ) {
			tableYLimit = ( short ) ( labelGroup.getPosY() - RELATIVE_Y );
		} else {
			tableYLimit = ( short ) labelGroup.getPosY();
		}
		tableMUVY.setSpeed( ( tableYLimit - labelGroup.getPosY() ) * 1000 / CURSOR_CHANGE_TIME );
		
		updateCursorPosition();
	}
	
	
	private final void setHour( int hour ) {
		currentHour = ( byte ) ( ( hour + TOTAL_HOURS ) % TOTAL_HOURS );
		
		// movimenta a tabela toda, caso necess�rio
		final int RELATIVE_X = labelGroup.getPosX() + ( currentHour * COLLUMN_WIDTH );
		if ( RELATIVE_X >= labelPanel.getWidth() - cursor.getWidth() ) {
			tableXLimit = ( short ) ( labelGroup.getPosX() + labelPanel.getWidth() - cursor.getWidth() - RELATIVE_X );
		} else if ( RELATIVE_X <= 0 ) {
			tableXLimit = ( short ) ( labelGroup.getPosX() - RELATIVE_X );
		} else {
			tableXLimit = ( short ) labelGroup.getPosX();
		}
		tableMUVX.setSpeed( ( tableXLimit - labelGroup.getPosX() ) * 1000 / CURSOR_CHANGE_TIME );

		updateCursorPosition();
	}
	
	
	private final void updateCursorPosition() {
		cursor.setPosition( labelGroup.getPosX() + ( currentHour * COLLUMN_WIDTH ), 
							labelGroup.getPosY() + ( biritaIndex * ROW_HEIGHT ) + 1 );		
	}
	
	
	public final void setControlMode( byte mode ) {
		controlMode = mode;
		
		switch ( mode ) {
			case CONTROL_MODE_MOVE:
				if ( editorIndex >= 0 ) {
					removeDrawable( editorIndex );
					editor = null;
					editorIndex = -1;
				}
			break;
			
			case CONTROL_MODE_EDIT:
				try {
					editor = new BiritaEditor( this, currentHour, currentBirita );
					if ( editorIndex < 0 )
						editorIndex = insertDrawable( editor );
					else
						setDrawable( editor, editorIndex );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					e.printStackTrace();
					//#endif
				}
			break;
		}
	}
	
	
	private final void updateGraphValues() {
		// zera os valores do gr�fico
		for ( byte h = 0; h < TOTAL_HOURS; ++h ) {
			fp_graphValues[ h ] = 0;
		}
		
		final int FP_ABSORPTION = NanoMath.mulFixed( User.getFPAbsorptionRate(), User.getFp_weight() );
		
		for ( byte h = 0; h < TOTAL_HOURS; ++h ) {
			for ( byte b = 0; b < Birita.TYPES_TOTAL; ++b ) {
				biritas[ b ].updateAlcoholValue( fp_graphValues, User.getFp_weight(), FP_ABSORPTION, h );
			} // fim for ( byte b = 0; b < Birita.TYPES_TOTAL; ++b )
		}
	} // fim do m�todo updateGraphValues()
	
	
	public final void reset() {
		setCurrentBirita( 0 );
		setHour( 0 );
		for ( byte i = 0; i < biritas.length; ++i ) {
			biritas[ i ].reset();
		}
	}
	
	
	public final void resetHourLabels() {
		for ( byte i = 0; i < TOTAL_HOURS; ++i )
			labelHour[ i ].setText( "<ALN_H>" + ( ( User.getInitialHour() + i ) % 24 ) + "h" );
	}
	
}
