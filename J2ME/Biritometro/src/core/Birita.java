/**
 * Birita.java
 * �2008 Nano Games.
 *
 * Created on Jul 18, 2008 9:49:09 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.util.NanoMath;


/**
 * 
 * @author Peter
 */
public final class Birita implements Constants {

	public static final byte CERVEJA_CHOPE		= 0;
	public static final byte CAIPIRINHA			= 1;
	public static final byte VODKA				= 2;
	public static final byte SHOTS				= 3;
	public static final byte DRINKS				= 4;
	public static final byte VINHO_CHAMPANHE	= 5;
	public static final byte WHISKY				= 6;
	
	public static final byte TYPES_TOTAL = 7;
	
	private static final byte ICONS_TOTAL = 25;
	
	private static DrawableImage[] ICONS;
	
	private static final byte ICON_CERVEJA_GARRAFA			= 0;
	private static final byte ICON_CERVEJA_LATA				= 1;
	private static final byte ICON_CERVEJA_COPO				= 2;
	private static final byte ICON_VINHO_GARRAFA			= 3;
	private static final byte ICON_VINHO_TACA				= 4;
	private static final byte ICON_VODKA_GARRAFA			= 5;
	private static final byte ICON_VODKA_COPO				= 6;
	private static final byte ICON_WHISKY_GARRAFA			= 7;
	private static final byte ICON_WHISKY_COPO				= 8;
	private static final byte ICON_SHOT_MARROM				= 9;
	private static final byte ICON_CAIPIRINHA				= 10;
	private static final byte ICON_CHAMPANHE_GARRAFA		= 11;
	private static final byte ICON_CHOPE_TULIPA				= 12;
	private static final byte ICON_CHOPE_GAROTINHO			= 13;
	private static final byte ICON_SHOT_VERDE				= 14;
	private static final byte ICON_SHOT_TRANSPARENTE		= 15;
	private static final byte ICON_COPO_ROSA				= 16;
	private static final byte ICON_CERVEJA_LATAO			= 17;
	private static final byte ICON_CHAMPANHE_TACA			= 18;
	private static final byte ICON_ICE_GARRAFA				= 19;
	private static final byte ICON_HIFI						= 20;
	private static final byte ICON_SEX_ON_THE_BEACH			= 21;
	private static final byte ICON_BATIDA_FRUTAS			= 22;
	private static final byte ICON_CUBA_LIBRE				= 23;
	private static final byte ICON_PINA_COLADA				= 24;
	
	private static final byte[][] ICON_INDEX = {
		// cerveja
		{ 
			ICON_CERVEJA_COPO,
			ICON_CERVEJA_LATA,
			ICON_CERVEJA_LATAO,
			ICON_CERVEJA_GARRAFA,
			ICON_CHOPE_GAROTINHO,
			ICON_CHOPE_TULIPA,
		},
		// caipirinha
		{ 
			ICON_CAIPIRINHA,
			ICON_CAIPIRINHA,
			ICON_CAIPIRINHA,
			ICON_CAIPIRINHA,
		},		
		// vodka
		{  
			ICON_ICE_GARRAFA,
			ICON_SHOT_TRANSPARENTE,
			ICON_VODKA_COPO,
			ICON_VODKA_GARRAFA,
		},
		// shots
		{  
			ICON_SHOT_TRANSPARENTE,
			ICON_SHOT_TRANSPARENTE,
			ICON_SHOT_TRANSPARENTE,
			ICON_SHOT_MARROM,
			ICON_SHOT_MARROM,
			ICON_SHOT_TRANSPARENTE,
			ICON_SHOT_MARROM,
			ICON_SHOT_VERDE,
		},
		// drinks
		{  
			ICON_BATIDA_FRUTAS,
			ICON_CUBA_LIBRE,
			ICON_COPO_ROSA,
			ICON_HIFI,
			ICON_SEX_ON_THE_BEACH,
			ICON_PINA_COLADA,
		},
		// vinho e champanhe
		{  
			ICON_VINHO_TACA,
			ICON_VINHO_GARRAFA,
			ICON_CHAMPANHE_TACA,
			ICON_CHAMPANHE_GARRAFA,
			ICON_CHAMPANHE_TACA,
			ICON_CHAMPANHE_GARRAFA,
		},
		// whisky
		{
			ICON_WHISKY_COPO,
			ICON_WHISKY_GARRAFA,
		},
	};

	private static final String[] NAMES = { 
		"Cerveja e chopp",
		"Caipirinha",
		"Vodka",
		"Shots",
		"Drinks",
		"Vinho e champanhe",
		"Whisky",
	};
	
	/** Quantidade de �lcool por dose, em gramas. */
	private static final int[][] LEVELS = { 
		// cerveja
		{
			389284,
			778568,
			1115947,
			1415578,
			471859,
			707789,
		},
		// caipirinha
		{ 
			2097152,
			2097152,
			2097152,
			838861,
		},		
		// vodka
		{  
			792986,
			1048576,
			3145728,
			20971520,
		},
		// shots
		{  
			1048576,
			1048576,
			503316,
			1048576,
			655360,
			1179648,
			1048576,
			1703936,
		},
		// drinks
		{  
			1677722,
			2097152,
			1179648,
			1677722,
			1677722,
			1677722,
		},
		// vinho e champanhe
		{  
			1284506,
			5505024,
			381682,
			374342,
			503316,
			2076180,
		},
		// whisky
		{
			2306867,
			23068672,
		},
	};
	
	private static final String[][] DOSES_NAMES = {
		// cerveja
		{
			"copo 165ml",
			"latinha 330ml",
			"lat�o 473ml",
			"garrafa 600ml",
			"chopp garotinho",
			"chopp tulipa",
		},
		// caipirinha
		{ 
			"caipirinha 200ml",
			"caipir�ssima 200ml",
			"caipivodka 200ml",
			"caipisaqu� 200ml",
		},		
		// vodka
		{  
			"garrafa ice 275ml",
			"shot 50ml",
			"copo 150ml",
			"garrafa 1L",
		},
		// shots
		{  
			"cacha�a 50ml",
			"tequila 50ml",
			"saqu� 60ml",
			"conhaque 50ml",
			"licor 50ml",
			"gim 50ml",
			"rum 50ml",
			"absinto 50ml",
		},
		// drinks
		{  
			"batida de frutas 150ml",
			"cuba libre",
			"gummy",
			"hi-fi",
			"sex on the beach",
			"pi�a colada",
		},
		// vinho e champanhe
		{  
			"ta�a vinho 175ml",
			"garrafa vinho 750ml",
			"ta�a champanhe 160ml",
			"garrafa champanhe 750ml",
			"ta�a sidra 160ml",
			"garrafa sidra 660ml",
		},
		// whisky
		{
			"copo 100ml",
			"garrafa 1L",
		},
	};
	
	/** Quantidade absoluta de �lcool ingerida a cada hora. */
	private final int[] alcoholLevel = new int[ TOTAL_HOURS ];
	
	private final byte[][] doses = new byte[ TOTAL_HOURS ][];
	
	public final RichLabel[] labels = new RichLabel[ TOTAL_HOURS ];
	
	private final byte type;
	
	
	public Birita( ImageFont font, int type ) throws Exception {
		this.type = ( byte ) type;
		
		for ( byte h = 0; h < TOTAL_HOURS; ++h )
			doses[ h ] = new byte[ LEVELS[ type ].length ];
		
		final Drawable[] LABEL_ICONS = new Drawable[ ICONS.length ];
		for ( byte i = 0; i < LABEL_ICONS.length; ++i )
			LABEL_ICONS[ i ] = new DrawableImage( ICONS[ i ] );
		
		// cria os labels (inser��o � feita externamente)
		for ( int i = 0; i < TOTAL_HOURS; ++i ) {
			final RichLabel label = new RichLabel( font, "<ALN_H>0", COLLUMN_WIDTH, LABEL_ICONS );
			label.setSize( COLLUMN_WIDTH, ROW_HEIGHT );
			label.setVisible( false );

			labels[ i ] = label;
		}		
	}
	
	
	public final String getName() {
		return NAMES[ type ];
	}
	
	
	public final int[] getLevels() {
		return LEVELS[ type ];
	}
	
	
	public final String[] getDosesNames() {
		return DOSES_NAMES[ type ];
	}
	
	
	public final byte getDose( byte hour, byte doseType ) {
		return doses[ hour ][ doseType ];
	}
	
	
	/**
	 * Define a quantidade de doses bebidas em determinada hora.
	 * @param hour
	 * @param doses
	 */
	public final void setDoses( int hour, byte[] doses ) {
		System.arraycopy( doses, 0, this.doses[ hour ], 0, doses.length );
		
		final StringBuffer buffer = new StringBuffer( "<ALN_H><ALN_B>" );
		final String STR_IMG = "<IMG_";
		
		// quantidade bruta de �lcool por cada dose de birita
		final int[] ALCOHOL_LEVEL = getLevels();
		
		int level = 0;
		for ( byte i = 0, count = 0; i < doses.length; ++i ) {
			level += NanoMath.mulFixed( ALCOHOL_LEVEL[ i ], NanoMath.toFixed( doses[ i ] ) );
			
			// indica a quantidade atrav�s de �cones
			for ( byte j = 0; j < doses[ i ]; ++j, ++count ) {
				// quebra a linha a cada BIRITA_PER_LINE_MAX �cones
				if ( count > 0 && count % BIRITA_PER_LINE_MAX == 0 )
					buffer.append( '\n' );
				
				buffer.append( STR_IMG );
				buffer.append( ICON_INDEX[ type ][ i ] );
				buffer.append( '>' );
			}
		}
		labels[ hour ].setText( buffer.toString(), true, false );
		
		final int TEXT_HEIGHT = labels[ hour ].getTextTotalHeight();
		labels[ hour ].setTextOffset( ( labels[ hour ].getHeight() - TEXT_HEIGHT ) >> 1 );
		
		alcoholLevel[ hour ] = level;
		
		if ( alcoholLevel[ hour ] > 0 ) {
			labels[ hour ].setVisible( true );
			
		} else {
			labels[ hour ].setVisible( false );
		}
	}
	
	
	public final void updateAlcoholValue( int[] fp_graphValues, int fp_weight, int fp_absorption, int h ) {
		final int FP_ALCOHOL_TOTAL = alcoholLevel[ h ];

		if ( FP_ALCOHOL_TOTAL > 0 ) {
			// para cada hora subsequente, adiciona o impacto da bebida
			for ( int h1 = h; h1 < TOTAL_HOURS; ++h1 ) {
				final int FP_ALCOHOL_CURRENT = FP_ALCOHOL_TOTAL - NanoMath.mulFixed( fp_absorption, NanoMath.toFixed( h1 - h )    );

				if ( FP_ALCOHOL_CURRENT > 0 )
					fp_graphValues[ h1 ] += NanoMath.divFixed( FP_ALCOHOL_CURRENT, fp_weight );
				else
					break;
			}
		} // fim if ( FP_INITIAL_LEVEL > 0 )		
	}
	
	
	public final DrawableImage getIcon( byte doseIndex ) {
		return ICONS[ ICON_INDEX[ type ][ doseIndex ] ];
	}
	
	
	public final void reset() {
		for ( byte i = 0; i < alcoholLevel.length; ++i )
			alcoholLevel[ i ] = 0;
		
		for ( byte i = 0; i < doses.length; ++i ) {
			for ( byte j = 0; j < doses[ i ].length; ++j )
				doses[ i ][ j ] = 0;
		}
		
		for ( byte i = 0; i < labels.length; ++i )
			labels[ i ].setText( "" );
	}
	
	
	public static final void loadImages() throws Exception {
		ICONS = new DrawableImage[ ICONS_TOTAL ];
		
		for ( byte i = 0; i < ICONS_TOTAL; ++i ) {
			ICONS[ i ] = new DrawableImage( PATH_BIRITA_ICONS + i + ".png" );
			
			Thread.yield();
		}
	}
	
}
