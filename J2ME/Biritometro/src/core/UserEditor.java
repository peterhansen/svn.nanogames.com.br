/**
 * UserEditor.java
 * �2008 Nano Games.
 *
 * Created on Aug 7, 2008 4:18:46 PM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;


/**
 * 
 * @author Peter
 */
public final class UserEditor extends UpdatableGroup implements Constants, KeyListener, PointerListener {

	private static DrawableImage IMG_MINUS;
	private static DrawableImage IMG_PLUS;
	private static DrawableImage IMG_SELECTOR_B;
	private static DrawableImage IMG_SELECTOR_F;
	private static DrawableImage IMG_SELECTOR;
	private static Sprite SPRITE_GENRE;
	
	private static final byte FRAME_MALE_MEDIUM		= 0;
	private static final byte FRAME_FEMALE_MEDIUM	= 1;
	private static final byte FRAME_MALE_THIN		= 2;
	private static final byte FRAME_FEMALE_THIN		= 3;
	private static final byte FRAME_MALE_FAT		= 4;
	private static final byte FRAME_FEMALE_FAT		= 5;
	
	private static final byte ENTRY_GENRE			= 0;
	private static final byte ENTRY_WEIGHT			= 1;
	private static final byte ENTRY_INITIAL_HOUR	= 2;
	private static final byte ENTRY_OK				= 3;
	private static final byte ENTRY_CANCEL			= 4;
	
	private static final byte ENTRY_TOTAL = ENTRY_CANCEL + 1;
	
	private byte currentEntry;

	private final Pattern selectorFill;
	
	private final DrawableImage selector;

	private final Label labelWeightTitle;
	
	private final Label labelWeight;
	
	private final int FP_MOVE_SPEED;
	
	private final MUV selectorMUV = new MUV();
	
	private int fp_currentWeight;
	
	private byte currentInitialHour;
	
 	private byte currentGenre;
	
 	private static final short MIN_INTERVAL = GameMIDlet.GAME_MAX_FRAME_TIME + 1;
	
	private int timeSinceLastKeyPressed = -1;
	
	private boolean dragging;
	
	private int keyHeld;
	
	private short keyAccTime;
	
	private final Pattern patternEntry;
	
	private final Pattern patternGenre;
	
	private final DrawableImage imgMinus;
	
	private final DrawableImage imgPlus;
	
	private final Sprite spriteThin;
	
	private final Sprite spriteFat;
	
	private final Sprite spriteMale;
			
	private final Sprite spriteFemale;
	
	private final Label labelGenre;
	
	private final RichLabel labelHour;

	private final Label labelHourTitle;
			
	private final Label labelOK;
	
	private final Label labelCancel;	
	
	private final UpdatableGroup panel;
	
	private final UpdatableGroup group;
	
	private final Pattern scrollBar;


	public UserEditor() throws Exception {
		super( 5 );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final Pattern bkg = new Pattern( COLOR_BIRITA_EDITOR_BKG );
		bkg.setSize( size );
		insertDrawable( bkg );
		
		FP_MOVE_SPEED = NanoMath.divFixed( FP_WEIGHT_MAX - FP_WEIGHT_MIN, NanoMath.toFixed( 9 ) );
		
		final RichLabel title = new RichLabel( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), GameMIDlet.getText( TEXT_USER_INFO_TITLE ), ScreenManager.SCREEN_WIDTH, null );
		title.setPosition( 0, title.getFont().getHeight() >> 1 );
		insertDrawable( title );
		
		panel = new UpdatableGroup( 3 );
		panel.setPosition( 1, title.getPosY() + ( ( title.getHeight() * 3 ) >> 1 ) );
		panel.setSize( size.x - 2, size.y - panel.getPosY() - BIRITA_ENTRY_SPACING - SOFTKEY_HEIGHT );
		insertDrawable( panel );
		
		final DrawableRect border = new DrawableRect( COLOR_BIRITA_ENTRY_SELECTED );
		border.setSize( SCROLL_WIDTH, panel.getHeight() );
		border.setPosition( panel.getWidth() - SCROLL_WIDTH - 1, 0 );
		
		scrollBar = new Pattern( COLOR_BACKGROUND );
		scrollBar.setSize( SCROLL_WIDTH - 2, 1 + ( panel.getHeight() - 2 ) / ENTRY_TOTAL );
		scrollBar.setPosition( border.getPosX() + 1, 0 );
		panel.insertDrawable( scrollBar );
		panel.insertDrawable( border );
		
		group = new UpdatableGroup( 20 );
		group.setSize( panel.getWidth() - 3 - SCROLL_WIDTH, 0 );
		group.setPosition( 1, 1 );
		panel.insertDrawable( group );
		
		patternEntry = new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
		group.insertDrawable( patternEntry );		
		
		patternGenre = new Pattern( COLOR_BACKGROUND );
		group.insertDrawable( patternGenre );
		
		labelGenre = new Label( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), GameMIDlet.getText( TEXT_GENRE ) );
		labelGenre.setPosition( ( group.getWidth() - labelGenre.getWidth() ) >> 1, 0 );
		group.insertDrawable( labelGenre );

		spriteMale = new Sprite( SPRITE_GENRE );
		spriteMale.defineReferencePixel( spriteMale.getWidth() >> 1, 0 );
		spriteMale.setRefPixelPosition( group.getWidth() >> 2, labelGenre.getPosY() + labelGenre.getHeight() + BIRITA_ENTRY_SPACING );
		spriteMale.setFrame( FRAME_MALE_MEDIUM );
		group.insertDrawable( spriteMale );
		
		spriteFemale = new Sprite( SPRITE_GENRE );
		spriteFemale.defineReferencePixel( spriteFemale.getWidth() >> 1, 0 );
		spriteFemale.setRefPixelPosition( ( group.getWidth() * 3 ) >> 2, spriteMale.getPosY() );
		spriteFemale.setFrame( FRAME_FEMALE_MEDIUM );
		group.insertDrawable( spriteFemale );

		patternGenre.setSize( ( group.getWidth() >> 1 ) - ( BIRITA_ENTRY_SPACING << 1 ), spriteMale.getHeight() + ( BIRITA_ENTRY_SPACING << 1 ) );
		patternGenre.setPosition( BIRITA_ENTRY_SPACING, spriteMale.getPosY() - BIRITA_ENTRY_SPACING );
		
		
		// insere o seletor de peso
		labelWeightTitle = new Label( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), GameMIDlet.getText( TEXT_WEIGHT ) );
		labelWeightTitle.setPosition( ( group.getWidth() - labelWeightTitle.getWidth() ) >> 1, patternGenre.getPosY() + patternGenre.getHeight() + BIRITA_ENTRY_SPACING );
		group.insertDrawable( labelWeightTitle );
		
		final DrawableImage borderLeft = new DrawableImage( IMG_SELECTOR_B );
		final DrawableImage borderRight = new DrawableImage( borderLeft );
		
		spriteThin = new Sprite( SPRITE_GENRE );
		spriteThin.setPosition( BIRITA_ENTRY_SPACING, labelWeightTitle.getPosY() + labelWeightTitle.getHeight() + BIRITA_ENTRY_SPACING );
		group.insertDrawable( spriteThin );
		
		spriteFat = new Sprite( SPRITE_GENRE );
		spriteFat.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
		spriteFat.setRefPixelPosition( group.getWidth() - BIRITA_ENTRY_SPACING, spriteThin.getRefPixelY() );
		group.insertDrawable( spriteFat );
		
		selectorFill = new Pattern( new DrawableImage( IMG_SELECTOR_F ) );
		selectorFill.setSize( group.getWidth() - spriteThin.getWidth() - spriteFat.getWidth() - ( borderLeft.getWidth() << 1 ) - 6, 
							  selectorFill.getFill().getHeight() );
		selectorFill.setPosition( ( group.getWidth() - selectorFill.getWidth() ) >> 1, 
								  spriteThin.getPosY() + ( ( spriteThin.getHeight() - selectorFill.getHeight() ) >> 1 ) );
		group.insertDrawable( selectorFill );
		
		
		borderLeft.setPosition( selectorFill.getPosX() - borderLeft.getWidth(), selectorFill.getPosY() );
		group.insertDrawable( borderLeft );

		borderRight.setTransform( TRANS_MIRROR_H );
		borderRight.setPosition( selectorFill.getPosX() + selectorFill.getWidth(), selectorFill.getPosY() );
		group.insertDrawable( borderRight );
		
		selector = new DrawableImage( IMG_SELECTOR );
		selector.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		selector.setRefPixelPosition( selectorFill.getPosX(), selectorFill.getPosY() + ( selectorFill.getHeight() >> 1 ) );
		group.insertDrawable( selector );
		
		labelWeight = new Label( GameMIDlet.getFont( FONT_INDEX_OUTLINE  ), "" );
		labelWeight.setPosition( 0, selector.getPosY() + selector.getHeight() );
		group.insertDrawable( labelWeight );
		
		labelHourTitle = new Label( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), GameMIDlet.getText( TEXT_INITIAL_HOUR ) );
		labelHourTitle.setPosition( ( group.getWidth() - labelHourTitle.getWidth() ) >> 1, labelWeight.getPosY() + labelWeight.getHeight() + BIRITA_ENTRY_SPACING );
		group.insertDrawable( labelHourTitle );

		final int distance = GameMIDlet.getFont( FONT_INDEX_OUTLINE ).getTextWidth( "222h" );
		
		imgPlus = new DrawableImage( IMG_PLUS );
		imgPlus.setPosition( ( ( group.getWidth() + distance ) >> 1 ) + BIRITA_ENTRY_SPACING, labelHourTitle.getPosY() + labelHourTitle.getHeight() + BIRITA_ENTRY_SPACING );
		group.insertDrawable( imgPlus );
		
		imgMinus = new DrawableImage( IMG_MINUS );
		imgMinus.setPosition( ( ( group.getWidth() - distance ) >> 1 ) + BIRITA_ENTRY_SPACING - imgMinus.getWidth(), imgPlus.getPosY() + ( ( imgPlus.getHeight() - imgMinus.getHeight() ) >> 1 ) );
		group.insertDrawable( imgMinus );
		
		labelHour = new RichLabel( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), "19h", group.getWidth() );
		labelHour.setPosition( 0, labelHourTitle.getPosY() + labelHourTitle.getHeight() + BIRITA_ENTRY_SPACING + ( ( imgPlus.getHeight() - labelHour.getHeight() ) >> 1 ) );
		group.insertDrawable( labelHour );
		
		// insere os bot�es
		labelOK = new Label( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CONFIRM ) );
		labelOK.setPosition( ( group.getWidth() - labelOK.getWidth() ) >> 1, 
							 labelHour.getPosY() + labelHour.getHeight() + BIRITA_ENTRY_SPACING + ( ( BUTTON_HEIGHT - labelOK.getHeight() ) >> 1 ) );
		group.insertDrawable( labelOK );

		labelCancel = new Label( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CANCEL ) );
		labelCancel.setPosition( ( group.getWidth() - labelCancel.getWidth() ) >> 1, labelOK.getPosY() + BUTTON_HEIGHT + 1 );
		group.insertDrawable( labelCancel );		
		
		group.setSize( group.getWidth(), labelCancel.getPosY() + labelCancel.getHeight() + 1 );
		
		currentInitialHour = User.getInitialHour();
		currentGenre = User.getGenre();
		updateGenre();
		
		changeInitialHour( 0 );
		setCurrentWeight( User.getFp_weight() == 0 ? FP_WEIGHT_DEFAULT : User.getFp_weight() );
		setCurrentEntry( ENTRY_GENRE );
	}	
	
	
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				switch ( currentEntry ) {
					case ENTRY_WEIGHT:
						setCurrentEntry( ENTRY_GENRE );
					break;
					
					case ENTRY_INITIAL_HOUR:
						setCurrentEntry( ENTRY_WEIGHT );
					break;
					
					case ENTRY_OK:
						setCurrentEntry( ENTRY_INITIAL_HOUR );
					break;

					case ENTRY_CANCEL:
						setCurrentEntry( ENTRY_OK );
					break;
				}
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				switch ( currentEntry ) {
					case ENTRY_GENRE:
						setCurrentEntry( ENTRY_WEIGHT );
					break;
					
					case ENTRY_WEIGHT:
						setCurrentEntry( ENTRY_INITIAL_HOUR );
					break;
					
					case ENTRY_INITIAL_HOUR:
						setCurrentEntry( ENTRY_OK );
					break;

					case ENTRY_OK:
						setCurrentEntry( ENTRY_CANCEL );
					break;
				}						
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				switch ( currentEntry ) {
					case ENTRY_GENRE:
						currentGenre = User.GENRE_MALE;
						updateGenre();
					break;
					
					case ENTRY_WEIGHT:
						selectorMUV.setSpeed( -FP_MOVE_SPEED );
						timeSinceLastKeyPressed = 0;
					break;
					
					case ENTRY_INITIAL_HOUR:
						changeInitialHour( -1 );
					break;
				}
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				switch ( currentEntry ) {
					case ENTRY_GENRE:
						currentGenre = User.GENRE_FEMALE;
						updateGenre();
					break;
					
					case ENTRY_WEIGHT:
						selectorMUV.setSpeed( FP_MOVE_SPEED );
						timeSinceLastKeyPressed = 0;
					break;
					
					case ENTRY_INITIAL_HOUR:
						changeInitialHour( 1 );
					break;
				}
			break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_SOFT_LEFT:
				switch ( currentEntry ) {
					case ENTRY_GENRE:
					case ENTRY_INITIAL_HOUR:
					case ENTRY_WEIGHT:
						setCurrentEntry( ( byte ) ( currentEntry + 1 ) );
					break;

					case ENTRY_CANCEL:
						keyPressed( ScreenManager.KEY_SOFT_RIGHT );
					return;
					
					case ENTRY_OK:
						User.setFp_weight( fp_currentWeight );
						User.setInitialHour( currentInitialHour );
						User.setGenre( currentGenre );
						
						GameMIDlet.setScreen( SCREEN_CONTINUE_TABLE );
					break;
				}						
			return;
			
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
			return;
		}
		
		keyHeld = key;
	}


	public final void keyReleased( int key ) {
		keyHeld = 0;
		keyAccTime = 0;		
		
		if ( timeSinceLastKeyPressed >= 0 && timeSinceLastKeyPressed < MIN_INTERVAL && selectorMUV.getSpeed() != 0 ) {
			final int currentZoom = NanoMath.toInt( NanoMath.mulFixed( fp_currentWeight, NanoMath.toFixed( 100 )  ) );
			
			setCurrentWeight( NanoMath.divInt( currentZoom + NanoMath.sgn( selectorMUV.getSpeed() ), 100 ) );
			
			timeSinceLastKeyPressed = -1;
		}
		
		selectorMUV.setSpeed( 0 );		
	}


	public final void onPointerDragged( int x, int y ) {
		if ( dragging ) {
			onPointerPressed( NanoMath.clamp( x, selectorFill.getPosX(), selectorFill.getPosX() + selectorFill.getWidth() ), selector.getPosY() );
		} else if ( keyHeld != 0 ) {
			onPointerPressed( x, y );
		} else if ( panel.contains( x, y ) ) {
			x -= panel.getPosX();
			
			if ( x > group.getWidth() ) {
				// clicou na barra de rolagem
				y -= panel.getPosY();
				setCurrentEntry( y / scrollBar.getHeight() );				
			}
		}
	}


	public final void onPointerPressed( int x, int y ) {
		boolean releaseKey = true;
		
		if ( panel.contains( x, y ) ) {
			x -= panel.getPosX();
			y -= panel.getPosY();
			
			if ( x <= group.getWidth() ) {
				x -= group.getPosX();
				y -= group.getPosY();
				
				// clicou numa das informa��es
				if ( y >= labelGenre.getPosY() && y <= labelGenre.getPosY() + labelGenre.getHeight() ) {
					setCurrentEntry( ENTRY_GENRE );
				} else if ( y >= patternGenre.getPosY() && y <= patternGenre.getHeight() ) {
					if ( currentEntry == ENTRY_GENRE ) {
						if ( x < BIRITA_ENTRY_SPACING + ( group.getWidth() >> 1 ) )
							keyPressed( ScreenManager.LEFT );
						else
							keyPressed( ScreenManager.RIGHT );
					} else {
						setCurrentEntry( ENTRY_GENRE );
					}
				} else if ( dragging || ( y >= selector.getPosY() && y <= selector.getPosY() + selector.getHeight() ) && ( x >= selectorFill.getPosX() && x <= selectorFill.getPosX() + selectorFill.getWidth() ) ) {
					// clicou na barra de peso
					if ( currentEntry == ENTRY_WEIGHT ) {
						dragging = true;
						x -= selectorFill.getPosX();

						final int FP_PERCENT = NanoMath.divInt( x, selectorFill.getWidth() );
						setCurrentWeight( FP_WEIGHT_MIN + NanoMath.mulFixed( FP_PERCENT, FP_WEIGHT_MAX - FP_WEIGHT_MIN ) );
					} else {
						setCurrentEntry( ENTRY_WEIGHT );
					}
				} else if ( ( y >= labelWeightTitle.getPosY() && y <= labelWeightTitle.getPosY() + labelWeightTitle.getHeight() ) 
							|| ( y >= labelWeight.getPosY() && y <= labelWeight.getPosY() + labelWeight.getHeight() ) ) {
					setCurrentEntry( ENTRY_WEIGHT );
				} else if ( ( y >= labelHourTitle.getPosY() && y <= labelHourTitle.getPosY() + labelHourTitle.getHeight() ) 
							|| ( y >= labelHour.getPosY() && y <= labelHour.getPosY() + labelHour.getHeight() ) ) {
					
					if ( currentEntry == ENTRY_INITIAL_HOUR ) {
						if ( x <= imgMinus.getPosX() + imgMinus.getWidth() ) {
							// usu�rio clicou no s�mbolo de -
							releaseKey = false;
							if ( keyHeld != ScreenManager.LEFT )
								keyPressed( ScreenManager.LEFT );				
						} else if ( x >= imgPlus.getPosX() ) {
							// usu�rio clicou no s�mbolo de +
							releaseKey = false;
							if ( keyHeld != ScreenManager.RIGHT )
								keyPressed( ScreenManager.RIGHT );						
						} else {
							setCurrentEntry( ENTRY_INITIAL_HOUR );
						}
					} else {
						setCurrentEntry( ENTRY_INITIAL_HOUR );
					}
				} else if ( y >= labelOK.getPosY() && y <= labelOK.getPosY() + labelOK.getHeight() ) {
					if ( currentEntry == ENTRY_OK ) {
						keyPressed( ScreenManager.FIRE );
					} else {
						setCurrentEntry( ENTRY_OK );
					}
				} else if ( y >= labelCancel.getPosY() && y <= labelCancel.getPosY() + labelCancel.getHeight() ) {
					if ( currentEntry == ENTRY_CANCEL ) {
						keyPressed( ScreenManager.FIRE );
					} else {
						setCurrentEntry( ENTRY_CANCEL );
					}
				}
			} else {
				// clicou na barra de rolagem
				setCurrentEntry( y / scrollBar.getHeight() );
			}
		}
		
		if ( releaseKey )
			keyReleased( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
		dragging = false;
		keyReleased( 0 );
	}
	
	
	public final void update( int delta ) {
		if ( timeSinceLastKeyPressed >= 0 )
			timeSinceLastKeyPressed += delta;

		if ( selectorMUV.getSpeed() != 0 ) {
			setCurrentWeight( fp_currentWeight + selectorMUV.updateInt( delta ) );
		}				
		
		if ( keyHeld != 0 ) {
			keyAccTime += delta;
			
			if ( keyAccTime >= KEY_REPEAT_TIME ) {
				keyAccTime %= KEY_REPEAT_TIME;
				
				keyPressed( keyHeld );
			}
		} // fim if ( keyHeld != 0 )				
	
		super.update( delta );
	}	
	
	
	private final void updateGenre() {
		patternGenre.setPosition( BIRITA_ENTRY_SPACING + ( ( group.getWidth() >> 1 ) * currentGenre ), patternGenre.getPosY() );
		if ( currentGenre == User.GENRE_MALE ) {
			spriteThin.setFrame( FRAME_MALE_THIN );
			spriteFat.setFrame( FRAME_MALE_FAT );
		} else {
			spriteThin.setFrame( FRAME_FEMALE_THIN );
			spriteFat.setFrame( FRAME_FEMALE_FAT );
		}
	}
	
	
	private final void changeInitialHour( int diff ) {
		currentInitialHour = ( byte ) ( ( currentInitialHour + diff + 24 ) % 24 );
		labelHour.setText( "<ALN_H>" + currentInitialHour + "h" );
	}
	
	
	private final void setCurrentWeight( int fp_weight  ) {
		fp_currentWeight = NanoMath.clamp( fp_weight, FP_WEIGHT_MIN, FP_WEIGHT_MAX  );

		if ( fp_currentWeight <= FP_WEIGHT_MIN || fp_currentWeight >= FP_WEIGHT_MAX )
			selectorMUV.setSpeed( 0 );

		selector.setRefPixelPosition( selectorFill.getPosX() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_currentWeight - FP_WEIGHT_MIN, FP_WEIGHT_MAX - FP_WEIGHT_MIN ), 
											NanoMath.toFixed( selectorFill.getWidth() ) ) ), selector.getRefPixelY() );
		
		labelWeight.setText( NanoMath.toString( fp_currentWeight ) + GameMIDlet.getText( TEXT_KG ) );
		labelWeight.setPosition( ( size.x - labelWeight.getWidth() ) >> 1, labelWeight.getPosY() );
	}	
	
	
	private final void setCurrentEntry( int entry ) {
		currentEntry = ( byte ) entry;
		
		switch ( currentEntry ) {
			case ENTRY_GENRE:
				patternEntry.setSize( size.x - 2, patternGenre.getHeight() + 2 );
				patternEntry.setPosition( 1, patternGenre.getPosY() - 1 );
			break;
			
			case ENTRY_WEIGHT:
				patternEntry.setPosition( 1, spriteThin.getPosY() - BIRITA_ENTRY_SPACING );
				patternEntry.setSize( size.x - 2, labelWeight.getPosY() - spriteThin.getPosY() + labelWeight.getHeight() + ( BIRITA_ENTRY_SPACING << 1 ) );
			break;
			
			case ENTRY_INITIAL_HOUR:
				patternEntry.setSize( size.x - 2, selector.getHeight() + ( BIRITA_ENTRY_SPACING << 1 ) );
				patternEntry.setPosition( 1, labelHour.getPosY() - BIRITA_ENTRY_SPACING );
			break;
			
			case ENTRY_OK:
			case ENTRY_CANCEL:
				patternEntry.setSize( size.x - 2, BUTTON_HEIGHT );
				patternEntry.setPosition( 1, labelOK.getPosY() + ( ( labelOK.getHeight() - BUTTON_HEIGHT ) >> 1 ) + ( BUTTON_HEIGHT * ( currentEntry - ENTRY_OK ) ) );
			break;
		}
		
		if ( currentEntry == 0 ) {
			group.setPosition( group.getPosX(), 0 );
		} else {
			final int OFFSET_Y = group.getPosY() + patternEntry.getPosY();

			if ( OFFSET_Y < 0 ) {
				group.setPosition( group.getPosX(), -patternEntry.getPosY() + labelGenre.getHeight() + 1 );
			} else if ( OFFSET_Y > panel.getHeight() - patternEntry.getHeight() ) {
				group.setPosition( group.getPosX(), -patternEntry.getPosY() - patternEntry.getHeight() + panel.getHeight() );
			}
		}

		scrollBar.setPosition( scrollBar.getPosX(), ( currentEntry * scrollBar.getHeight() ) );
	}	
	
	
	public static final void loadImages() throws Exception {
		IMG_SELECTOR_B = new DrawableImage( PATH_IMAGES + "selector_b.png" );
		IMG_MINUS = new DrawableImage( PATH_IMAGES + "minus.png" );
		IMG_PLUS = new DrawableImage( PATH_IMAGES + "plus.png" );
		Thread.yield();
		IMG_SELECTOR_F = new DrawableImage( PATH_IMAGES + "selector_f2.png" );
		IMG_SELECTOR = new DrawableImage( PATH_IMAGES + "selector.png" );
		Thread.yield();
		SPRITE_GENRE = new Sprite( PATH_IMAGES + "genre.bin", PATH_IMAGES + "genre" );
		Thread.yield();
	}
	
	
}
