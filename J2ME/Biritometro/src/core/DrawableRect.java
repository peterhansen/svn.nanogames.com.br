/**
 * DrawableRect.java
 * �2008 Nano Games.
 *
 * Created on Aug 6, 2008 11:25:13 AM.
 */

package core;

import br.com.nanogames.components.Drawable;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class DrawableRect extends Drawable {
	
	private int color;
	
	
	public DrawableRect( int color ) {
		setColor( color );
	}
	
	
	public final int getColor() {
		return color;
	}
	
	
	public final void setColor( int color ) {
		this.color = color;
	}

	
	protected final void paint( Graphics g ) {
		g.setColor( color );
		g.drawRect( translate.x, translate.y, size.x - 1, size.y - 1 );
	}

}
