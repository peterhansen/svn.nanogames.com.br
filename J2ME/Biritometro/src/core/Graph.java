/**
 * Graph.java
 * �2008 Nano Games.
 *
 * Created on Jul 20, 2008 11:26:37 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class Graph extends UpdatableGroup implements Constants, KeyListener, PointerListener {

	/** Escala de cada eixo (x,y). */
	private final Point fp_scale = new Point();
	
	/** N�vel de �lcool no sangue a cada hora. */
	private final int[] values;
	
	/** Posi��es (n�vel de �lcool, hora) em pixels do gr�fico. */
	private final Point[] points;
	
	/** �rea colorida em vermelho do gr�fico. */
	private final Pattern bkgRed;
	
	/** �rea colorida em verde (abaixo do limite de �lcool) do gr�fico. */
	private final Pattern bkgGreen;
	
	/** T�tulo do gr�fico. */
	private final MarqueeLabel title;
	
	/** Legendas do eixo x. */
	private final Label[] labelHour;
	
	/** Legendas do eixo y. */
	private final Label[] labelLevel;
	
	public static final int FP_ZOOM_LEVEL_MIN = NanoMath.ONE >> 2;
	
	public static final int FP_ZOOM_LEVEL_MAX = NanoMath.ONE << 1;
	
	private int fp_zoomLevel;
	
	private final short SPEED;
	
	private final MUV speedX = new MUV();
	
	private final MUV speedY = new MUV();
	
	private final short LIMIT_LEFT = 0;
	private final short LIMIT_UP = 0;
	private short LIMIT_RIGHT;
	private short LIMIT_DOWN;
	
	private final DrawableGroup group;
	
	private final Point lastPointerPos = new Point();
	
	private final ZoomEditor zoomEditor;
	
	private final DrawableGraph drawableGraph;
	
	private final DrawableImage softKeyMid;
	
	/** 
	 * �cone do gr�fico - n�o � utilizado o funcionamento padr�o do softkey em ScreenManager, pois o �cone deve 
	 * ser desenhado sob o editor de birita (caso esteja vis�vel).
	 */
	private final DrawableGroup softKeyLeft;
	
	/**
	 * �cone do softkey direito - n�o � utilizado o funcionamento padr�o do softkey em ScreenManager, pois o �cone deve 
	 * ser desenhado sob o editor de birita (caso esteja vis�vel).
	 */
	private final DrawableGroup softKeyRight;	
	
	/** Indica se est� no modo de edi��o de n�vel de zoom. */
	private boolean editZoom;
	
	/** N�vel m�ximo de �lcool no sangue. */
	private int fp_alcoholMaxLevel;
	
	/** �ndice da hora em que o n�vel de �lcool no sangue atingiu seu limite. */
	private byte hourMaxAlcohol;
	
	/** �ndice da hora em que o n�vel de �lcool no sangue atingiu seu limite. */
	private byte hourFirstBirita;
	
	
	public Graph() throws Exception {
		super( 8 );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		SPEED = ( short ) ( NanoMath.min( size.x, size.y ) >> 1 );
		
		group = new UpdatableGroup( 50 );
		insertDrawable( group );
		
		values = new int[ TableScreen.fp_graphValues.length + 1 ];
		System.arraycopy( TableScreen.fp_graphValues, 0, values, 1, TableScreen.fp_graphValues.length );
		
		points = new Point[ values.length + 1 ];
		for ( byte i = 0; i < points.length; ++i )
			points[ i ] = new Point();
		
		
		bkgRed = new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
		group.insertDrawable( bkgRed );
		
		bkgGreen = new Pattern( COLOR_GREEN );
		group.insertDrawable( bkgGreen );
		
		final ImageFont FONT = GameMIDlet.getFont( FONT_INDEX_CAPTIONS );
		
		title = new MarqueeLabel( FONT, GameMIDlet.getText( TEXT_GRAPH_TITLE ) );
		title.setSize( size.x, FONT.getHeight() );
		group.insertDrawable( title );
		
		labelHour = new Label[ TOTAL_HOURS + 1 ];
		for ( byte i = 0; i < labelHour.length; ++i ) {
			final Label label = new Label( FONT, ( ( User.getInitialHour() + i ) % 24 ) + "h" );
			labelHour[ i ] = label;
			label.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
			group.insertDrawable( label );
		}
		
		labelLevel = new Label[ labelHour.length ];
		final int LABEL_LEVEL_WIDTH = FONT.getTextWidth( "0,0000" );
		for ( byte i = 0; i < labelLevel.length; ++i ) {
			final Label label = new Label( FONT, null );
			label.setSize( LABEL_LEVEL_WIDTH, FONT.getHeight() );
			label.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
			
			labelLevel[ i ] = label;
			
			group.insertDrawable( label );
		}
		
		bkgRed.setPosition( LABEL_LEVEL_WIDTH, title.getPosY() + ( title.getHeight() << 1 ) );
		bkgGreen.setPosition( bkgRed.getPosition() );
		
		drawableGraph = new DrawableGraph( this );
		insertDrawable( drawableGraph );
		
		softKeyLeft = GameMIDlet.loadSoftKeyGroup( ScreenManager.SOFT_KEY_LEFT, TEXT_INFO );
		softKeyLeft.setPosition( 0, ScreenManager.SCREEN_HEIGHT - softKeyLeft.getHeight() );
		insertDrawable( softKeyLeft );
		
		softKeyMid = new DrawableImage( PATH_IMAGES + "magnify.png" );
		softKeyMid.setPosition( ( ScreenManager.SCREEN_WIDTH - softKeyMid.getWidth() ) >> 1, ScreenManager.SCREEN_HEIGHT - softKeyMid.getHeight() - 1 );
		insertDrawable( softKeyMid );
		
		softKeyRight = GameMIDlet.loadSoftKeyGroup( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
		softKeyRight.setPosition( ScreenManager.SCREEN_WIDTH - softKeyRight.getWidth(), ScreenManager.SCREEN_HEIGHT - softKeyRight.getHeight() );
		insertDrawable( softKeyRight );

		zoomEditor = new ZoomEditor( this );
		insertDrawable( zoomEditor );
		
		fitGraph();
	}
	
	
	public final void fitGraph() {
		// calcula o n�vel de zoom necess�rio para que a altura do gr�fico caiba na tela
		final int FP_HEIGHT = NanoMath.toFixed( size.y - ( title.getHeight() << 2 ) );
		
		final int ZOOM_LEVEL = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.ONE << 1, FP_HEIGHT  ), NanoMath.mulFixed( FP_GRAPH_HOUR_WIDTH_DEFAULT, NanoMath.toFixed( TOTAL_HOURS ) ) );

		setZoomLevel( ZOOM_LEVEL );
	}
	

	public final void setZoomLevel( int fp_zoomLevel ) {
		fp_zoomLevel = NanoMath.clamp( fp_zoomLevel, FP_ZOOM_LEVEL_MIN, FP_ZOOM_LEVEL_MAX );
		this.fp_zoomLevel = fp_zoomLevel;
		
		// atualiza a escala do eixo x
		fp_scale.x = NanoMath.mulFixed( FP_GRAPH_HOUR_WIDTH_DEFAULT, fp_zoomLevel );
		
		// recalcula o tamanho total do gr�fico, em pixels
		final int BKG_WIDTH = NanoMath.toInt( NanoMath.mulFixed( fp_scale.x, NanoMath.toFixed( TOTAL_HOURS ) ) );
		bkgRed.setSize( BKG_WIDTH, BKG_WIDTH >> 1 );
		
		// posiciona os labels do eixo x (caso 2 labels se sobreponham, somente o primeiro � desenhado)
		final int HOUR_Y = bkgRed.getPosY() + bkgRed.getHeight() + labelHour[ 0 ].getHeight();
		Label lastVisibleLabel = null;
		for ( int h = 0, fp_x = 0; h < labelHour.length; ++h, fp_x += fp_scale.x ) {
			labelHour[ h ].setRefPixelPosition( bkgRed.getPosX() + NanoMath.toInt( fp_x ), HOUR_Y );
			
			labelHour[ h ].setVisible( lastVisibleLabel == null || labelHour[ h ].getPosX() > lastVisibleLabel.getPosX() + lastVisibleLabel.getWidth() );
			if ( labelHour[ h ].isVisible() )
				lastVisibleLabel = labelHour[ h ];
		}

		hourFirstBirita = -1;
		hourMaxAlcohol = -1;
		int fp_alcoholMaxLevelShown = 0;
		for ( byte i = 0; i < values.length; ++i ) {
			if ( values[ i ] > fp_alcoholMaxLevelShown ) {
				fp_alcoholMaxLevelShown = values[ i ];
				hourMaxAlcohol = i;
			}
			
			if ( hourFirstBirita < 0 && values[ i ] > 0 )
				hourFirstBirita = i;
		}
		
		fp_alcoholMaxLevel = fp_alcoholMaxLevelShown;
		
		if ( fp_alcoholMaxLevelShown == 0 )
			fp_alcoholMaxLevelShown = FP_ALCOHOL_LEVEL_MAX;
		else
			fp_alcoholMaxLevelShown = NanoMath.mulFixed( fp_alcoholMaxLevelShown, NanoMath.divInt( labelLevel.length, labelLevel.length - 1 )        );

		fp_scale.y = -NanoMath.divFixed( NanoMath.toFixed( bkgRed.getHeight() ), fp_alcoholMaxLevelShown        );

		bkgGreen.setSize( BKG_WIDTH, NanoMath.min( bkgRed.getHeight(), NanoMath.toInt( NanoMath.mulFixed( FP_ALCOHOL_LIMIT, -fp_scale.y ) ) ) );
		bkgGreen.setPosition( bkgGreen.getPosX(), bkgRed.getPosY() + bkgRed.getHeight() - bkgGreen.getHeight() );
		
		final int FP_STEP_Y = NanoMath.divFixed( fp_alcoholMaxLevelShown, NanoMath.toFixed( labelLevel.length - 1 )        );
		final int START_Y = bkgRed.getPosY() + bkgRed.getHeight();
		lastVisibleLabel = null;
		for ( int i = 0, fp_level = 0; i < labelLevel.length; ++i, fp_level += FP_STEP_Y ) {
			final Label label = labelLevel[ i ];
			final int previousX = label.getRefPixelX();
			label.setText( NanoMath.toString( fp_level, 2 ) );
			label.setRefPixelPosition( previousX, START_Y + NanoMath.toInt( NanoMath.mulFixed( fp_level, fp_scale.y ) ) );
			
			label.setVisible( lastVisibleLabel == null || label.getPosY() + label.getHeight() <= lastVisibleLabel.getPosY() );
			if ( label.isVisible() )
				lastVisibleLabel = label;
		}
		
		final int FP_POS_X = NanoMath.divInt( group.getPosX(), NanoMath.min( LIMIT_RIGHT, -1 ) );
		final int FP_POS_Y = NanoMath.divInt( group.getPosY(), NanoMath.min( LIMIT_DOWN, -1 ) );
		
		group.setSize( NanoMath.max( ( bkgRed.getPosX() << 1 ) + BKG_WIDTH, size.x ), NanoMath.max( HOUR_Y + ( labelHour[ 0 ].getHeight() * 3 ), size.y ) );
		drawableGraph.setSize( group.getSize() );
		title.setSize( group.getWidth(), title.getHeight() );
		
		LIMIT_RIGHT = ( short ) ( size.x - group.getWidth() );
		LIMIT_DOWN = ( short ) ( size.y - group.getHeight() );
		
		group.setPosition( NanoMath.toInt( NanoMath.mulFixed( FP_POS_X, NanoMath.toFixed( LIMIT_RIGHT ) ) ),
						   NanoMath.toInt( NanoMath.mulFixed( FP_POS_Y, NanoMath.toFixed( LIMIT_DOWN ) ) ) );
		
		checkBounds();
	} // fim do m�todo setZoomLevel( int )
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		// atualiza posi��o do gr�fico
		if ( speedX.getSpeed() != 0 ) {
			group.move( speedX.updateInt( delta ), 0 );
			
			if ( speedX.getSpeed() < 0 && group.getPosX() <= LIMIT_RIGHT ) { 
				group.setPosition( LIMIT_RIGHT, group.getPosY() );
				speedX.setSpeed( 0 );
			} else if ( speedX.getSpeed() > 0 && group.getPosX() >= LIMIT_LEFT ) {
				group.setPosition( LIMIT_LEFT, group.getPosY() );
				speedX.setSpeed( 0 );
			}
		}
		
		if ( speedY.getSpeed() != 0 ) {
			group.move( 0, speedY.updateInt( delta ) );
			
			if ( speedY.getSpeed() < 0 && group.getPosY() <= LIMIT_DOWN ) { 
				group.setPosition( group.getPosX(), LIMIT_DOWN );
				speedY.setSpeed( 0 );
			} else if ( speedY.getSpeed() > 0 && group.getPosY() >= LIMIT_UP ) {
				group.setPosition( group.getPosX(), LIMIT_UP );
				speedY.setSpeed( 0 );
			}
		}
	}


	public final void keyPressed( int key ) {
		if ( editZoom ) {
			zoomEditor.keyPressed( key );
		} else {
			switch ( key ) {
				case ScreenManager.KEY_NUM1:
					speedX.setSpeed( SPEED );
				case ScreenManager.UP:
				case ScreenManager.KEY_NUM2:
					speedY.setSpeed( SPEED );
				break;

				case ScreenManager.KEY_NUM9:
					speedX.setSpeed( -SPEED );
				case ScreenManager.DOWN:
				case ScreenManager.KEY_NUM8:
					speedY.setSpeed( -SPEED );
				break;

				case ScreenManager.KEY_NUM7:
					speedY.setSpeed( -SPEED );
				case ScreenManager.LEFT:
				case ScreenManager.KEY_NUM4:
					speedX.setSpeed( SPEED );
				break;

				case ScreenManager.KEY_NUM3:
					speedY.setSpeed( SPEED );
				case ScreenManager.RIGHT:
				case ScreenManager.KEY_NUM6:
					speedX.setSpeed( -SPEED );
				break;

				case ScreenManager.KEY_POUND:
				case ScreenManager.KEY_NUM0:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.FIRE:
					// mostra o pop-up de mudan�a de zoom
					setZoomEditor( true );
				break;
				
				case ScreenManager.KEY_STAR:
				case ScreenManager.KEY_SOFT_LEFT:
					GameMIDlet.setScreen( SCREEN_GRAPH_INFO );
				break;
				
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					GameMIDlet.setScreen( SCREEN_CONTINUE_TABLE );
				break;
			}
		}
	}


	public final void keyReleased( int key ) {
		if ( editZoom ) {
			zoomEditor.keyReleased( key );
		} else {
			speedX.setSpeed( 0 );
			speedY.setSpeed( 0 );
		}
	}


	public final void onPointerDragged( int x, int y ) {
		if ( editZoom ) {
			x -= zoomEditor.getPosX();
			y -= zoomEditor.getPosY();

			zoomEditor.onPointerDragged( x, y );
		} else {		
			group.move( x - lastPointerPos.x, y - lastPointerPos.y );
			lastPointerPos.set( x, y );

			checkBounds();
		}
	}


	public final void onPointerPressed( int x, int y ) {
		if ( softKeyMid.contains( x, y ) ) {
			keyPressed( ScreenManager.KEY_SOFT_MID );
		} else if ( softKeyLeft.contains( x, y ) ) {
			keyPressed( ScreenManager.KEY_SOFT_LEFT );
		} else if ( softKeyRight.contains( x, y ) ) {
			keyPressed( ScreenManager.KEY_SOFT_RIGHT );
		} else {
			if ( editZoom ) {
				x -= zoomEditor.getPosX();
				y -= zoomEditor.getPosY();

				zoomEditor.onPointerPressed( x, y );
			} else {
				lastPointerPos.set( x, y );
			}
		}
	}


	public final void onPointerReleased( int x, int y ) {
		zoomEditor.onPointerReleased( x, y );
	}
	
	
	private final void checkBounds() {
		if ( group.getPosX() < LIMIT_RIGHT ) { 
			group.setPosition( LIMIT_RIGHT, group.getPosY() );
		} else if ( group.getPosX() > LIMIT_LEFT ) {
			group.setPosition( LIMIT_LEFT, group.getPosY() );
		}
		
		if ( group.getPosY() < LIMIT_DOWN ) { 
			group.setPosition( group.getPosX(), LIMIT_DOWN );
		} else if ( group.getPosY() > LIMIT_UP ) {
			group.setPosition( group.getPosX(), LIMIT_UP );
		}		
	}
	
	
	public final String getDescription() {
		final StringBuffer buffer = new StringBuffer();
		
		
		if ( fp_alcoholMaxLevel <= 0 ) {
			// usu�rio n�o bebeu nada
			buffer.append( "voc� n�o tomou nenhuma birita! Muito bem, voc� est� em condi��es de ser " );
			
			if ( User.getGenre() == User.GENRE_MALE )
				buffer.append( "o amigo" );
			else
				buffer.append( "a amiga" );
			
			buffer.append( " da vez e levar todos os seus amigos pingu�os pra casa, sem problemas com a lei!\n\n" );
		} else {
			// usu�rio bebeu pelo menos 1 dose de birita
			buffer.append( "voc� tomou sua primeira birita �s " );
			
			buffer.append( ( ( User.getInitialHour() + hourFirstBirita + 23 ) % 24 ) );
			buffer.append( "h " );
			
			if ( fp_alcoholMaxLevel <= FP_ALCOHOL_LEVEL_1 ) {
				buffer.append( "e chegou aos " );
				buffer.append( NanoMath.toString( fp_alcoholMaxLevel, 3  ) );
				buffer.append( " g/l �s " );
				buffer.append( ( ( User.getInitialHour() + hourMaxAlcohol ) % 24 ) );
				buffer.append( "h, ficando bem alegrinh" );
				if ( User.getGenre() == User.GENRE_MALE )
					buffer.append( "o" );
				else
					buffer.append( "a" );
				buffer.append( ", falando pra pessoa do lado o quanto gosta dela.\n\nDe verdade.\n\n\n� s�rio, cara, n�o � pra rir n�o, te considero muito!\n\n" );
			} else if ( fp_alcoholMaxLevel <= FP_ALCOHOL_LEVEL_2 ) {
				buffer.append( "e chegou aos " );
				buffer.append( NanoMath.toString( fp_alcoholMaxLevel, 3  ) );
				buffer.append( " g/l �s " );
				buffer.append( ( ( User.getInitialHour() + hourMaxAlcohol ) % 24 ) );
				buffer.append( "h.\n\n\"Um prato de trigo pra tr�s tigres tristes\" e uma frase em russo arcaico s�o igualmente impronunci�veis, e levantar para ir ao banheiro j� exige mais per�cia do que voc� podia imaginar.\n\n" );				
			} else if ( fp_alcoholMaxLevel <= FP_ALCOHOL_LEVEL_3 ) {
				buffer.append( "e muito provavelmente chamou o Raaaaauuuuuuuul por volta das " );
				buffer.append( ( ( User.getInitialHour() + hourMaxAlcohol ) % 24 ) );
				buffer.append( "h, logo depois de esbarrar na parede e discutir com ela.\n\nAh, nem pense em fazer o \"4\", a essa hora o ch�o j� deve estar rodando e tudo ficou dobrado, inclusive a sua dor de cabe�a no dia seguinte!\n\n" );
			} else if ( fp_alcoholMaxLevel <= FP_ALCOHOL_LEVEL_LETHAL ) {
				buffer.append( "e, �s " );
				buffer.append( ( ( User.getInitialHour() + hourMaxAlcohol ) % 24 ) );
				buffer.append( "h, chegou aos " );
				buffer.append( NanoMath.toString( fp_alcoholMaxLevel, 3  ) );
				buffer.append( " g/l de �lcool no sangue, ultrapassando o limite de \"Jeremias Muito Louco\" e falou que \"o c�o bot� p� n�is beb�\", logo o neg�cio j� come�a a ficar bem s�rio.\n\nVoc� j� n�o vai lembrar das besteiras que est� fazendo e/ou dizendo, mas lembre-se que hoje em dia quase tudo � gravado e poder� ser usado contra voc� no e-mail de amanh� de manh�!\n\n" );
			} else {
				buffer.append( "e a� n�o parou mais... Muito cuidado, voc� passou dos limites na birita! Com essa quantidade de sangue no seu �lcool, digo, �lcool no seu sangue, a brincadeira pode acabar mais cedo pra voc�, j� que voc� � s�ri" );
				if ( User.getGenre() == User.GENRE_MALE )
					buffer.append( "o candidato" );
				else
					buffer.append( "a candidata" );

				buffer.append( " a visitar um hospital, ou at� mesmo um caix�o...\n\nAntes que chegue a esse ponto...\n\n<ALN_H>PARE DE BEBER!\n\n" );
			}
		}
		
		return buffer.toString();
	}
	
	
	/**
	 * Retorna o n�vel de zoom, em nota��o de ponto fixo (100% = 1).
	 */
	public final int getZoomLevel() {
		return fp_zoomLevel;
	}
	
	
	public final void setZoomEditor( boolean active ) {
		editZoom = active;
		
		if ( active )
			zoomEditor.setState( STATE_APPEARING );
	}
	
	
	private static final class DrawableGraph extends Drawable {

		private final Graph graph;
		
		private DrawableGraph( Graph graph ) {
			this.graph = graph;
		}

		
		protected final void paint( Graphics g ) {
			final int START_X = translate.x + graph.group.getPosX();
			final int START_Y = translate.y + graph.group.getPosY() + graph.bkgRed.getPosY() + graph.bkgRed.getHeight();

			// calcula a posi��o em pixels dos n�veis de �lcool a cada hora
			graph.points[ 0 ].set( START_X + graph.labelHour[ 0 ].getRefPixelX(), START_Y );
			for ( byte i = 0; i < graph.values.length; ++i ) {
				graph.points[ i + 1 ].x = START_X + graph.labelHour[ i ].getRefPixelX();
				graph.points[ i + 1 ].y = START_Y + NanoMath.toInt( NanoMath.mulFixed( graph.values[ i ], graph.fp_scale.y ) );
			}

			final short LINE_SPACE = 6;
			final short LINE_START_Y = ( short ) ( translate.y + graph.group.getPosY() + graph.bkgRed.getPosY() - LINE_SPACE );
			final short LINE_HEIGHT = ( short ) ( graph.bkgRed.getHeight() + ( LINE_SPACE << 1 ) );

			final short LINE_START_X = ( short ) ( translate.x + graph.group.getPosX() + graph.bkgRed.getPosX() - LINE_SPACE );
			final short LINE_WIDTH = ( short ) ( graph.bkgRed.getWidth() + ( LINE_SPACE << 1 ) );

			final short TRANSLATE_Y = ( short ) ( translate.y + graph.group.getPosY() );

			// desenhas a primeira e a �ltima linha horizontal
			g.setColor( COLOR_BIRITA_EDITOR_BKG );
			g.drawRect( LINE_START_X, TRANSLATE_Y + graph.bkgRed.getPosY(), LINE_WIDTH, 0 );		
			g.drawRect( LINE_START_X, TRANSLATE_Y + graph.labelLevel[ 0 ].getRefPixelY() - 1, LINE_WIDTH, 1 );		

			for ( byte i = 1; i < graph.points.length; ++i ) {
				final Point point = graph.points[ i ];
				final Point previous = graph.points[ i - 1 ];

				// desenha as linhas verticais principais do gr�fico
				g.setColor( COLOR_BIRITA_EDITOR_BKG );
				g.drawRect( point.x - 1, LINE_START_Y, 1, LINE_HEIGHT );
				g.drawRect( point.x - ( ( point.x - previous.x ) >> 1 ), LINE_START_Y, 0, LINE_HEIGHT );

				// desenhas as linhas horizontais
				if ( i < graph.labelLevel.length ) {
					if ( ( i & 1 ) == 1 )
						g.drawRect( LINE_START_X, TRANSLATE_Y + graph.labelLevel[ i ].getRefPixelY(), LINE_WIDTH, 0 );
					else	
						g.drawRect( LINE_START_X, TRANSLATE_Y + graph.labelLevel[ i ].getRefPixelY() - 1, LINE_WIDTH, 1 );
				}
			}

			// desenha o gr�fico somente no 2� loop, para garantir que suas linhas estejam sobre as linhas de marca��o do gr�fico
			for ( byte i = 1; i < graph.points.length; ++i ) {
				final Point point = graph.points[ i ];
				final Point previous = graph.points[ i - 1 ];

				g.setColor( COLOR_GRAPH_LINE );
				g.drawLine( previous.x, previous.y,		point.x, point.y );
				g.drawLine( previous.x, previous.y + 1,	point.x, point.y + 1 );
				g.drawLine( previous.x + 1, previous.y + 1,	point.x + 1, point.y + 1 );
			}				
		}			
	}
		
	
}
