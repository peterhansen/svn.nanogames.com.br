/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/*******************************************************************************************
	 *                              DEFINI��ES GEN�RICAS                                       *
	 *******************************************************************************************/
	
	
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho dos �cones das biritas. */
	public static final String PATH_BIRITA_ICONS = PATH_IMAGES + "birita/";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Offset da fonte padr�o. */
	public static final byte FONT_OUTLINE_OFFSET = -1;
	
	/** �ndice da fonte padr�o do jogo. */
	public static final byte FONT_INDEX_OUTLINE	= 0;
	
	/** �ndice da fonte clara. */
	public static final byte FONT_INDEX_LIGHT	= 1;
	
	/** �ndice da fonte escura. */
	public static final byte FONT_INDEX_DARK	= 2;
	
	/** �ndice da fonte utilizada nas legendas do gr�fico. */
	public static final byte FONT_INDEX_CAPTIONS	= 2;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 3;
	
	//#if DEMO == "true"
//# 	/** N�mero de jogadas iniciais dispon�veis na vers�o demo. */
//# 	public static final byte DEMO_MAX_PLAYS = 10;	
	//#endif
	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_CHOOSE_ZOOM_LEVEL				= TEXT_EXIT + 1;
	public static final byte TEXT_MENU							= TEXT_CHOOSE_ZOOM_LEVEL + 1;
	public static final byte TEXT_BIRITAS						= TEXT_MENU + 1;
	public static final byte TEXT_CREDITS						= TEXT_BIRITAS + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_TIPS							= TEXT_CONTROLS + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_HELP_TIPS						= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_INFO							= TEXT_HELP_TIPS + 1;
	public static final byte TEXT_CHOOSE_QUANTITY				= TEXT_INFO + 1;
	public static final byte TEXT_CONFIRM						= TEXT_CHOOSE_QUANTITY + 1;
	public static final byte TEXT_USER_INFO_TITLE				= TEXT_CONFIRM + 1;
	public static final byte TEXT_CANCEL						= TEXT_USER_INFO_TITLE + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_NANO + 1;	
	public static final byte TEXT_YES							= TEXT_LOADING + 1;
	public static final byte TEXT_NO							= TEXT_YES + 1;
	public static final byte TEXT_USER_INFO						= TEXT_NO + 1;
	public static final byte TEXT_GENRE							= TEXT_USER_INFO + 1;
	public static final byte TEXT_KG							= TEXT_GENRE + 1;
	public static final byte TEXT_INITIAL_HOUR					= TEXT_KG + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_INITIAL_HOUR + 1;
	public static final byte TEXT_WEIGHT						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_GRAPH_TITLE					= TEXT_WEIGHT + 1;
	public static final byte TEXT_CHANGE_INFO					= TEXT_GRAPH_TITLE + 1;
	public static final byte TEXT_DISCLAIMER					= TEXT_CHANGE_INFO + 1;
	public static final byte TEXT_GRAPH_INFO					= TEXT_DISCLAIMER + 1;
	public static final byte TEXT_LOG_TITLE						= TEXT_GRAPH_INFO + 1;
	public static final byte TEXT_LOG_TEXT						= TEXT_LOG_TITLE + 1;
	
	/** n�mero total de textos do jogo */
	public static final byte TEXT_TOTAL = TEXT_LOG_TEXT + 1;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_SPLASH_NANO				= 0;
	public static final byte SCREEN_DISCLAIMER				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_DISCLAIMER + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_TABLE				= SCREEN_MAIN_MENU + 1;	
	public static final byte SCREEN_CONTINUE_TABLE			= SCREEN_NEW_TABLE + 1;	
	public static final byte SCREEN_GRAPH_SETUP				= SCREEN_CONTINUE_TABLE + 1;	
	public static final byte SCREEN_GRAPH					= SCREEN_GRAPH_SETUP + 1;	
	public static final byte SCREEN_HELP_MENU				= SCREEN_GRAPH + 1;
	public static final byte SCREEN_HELP_OBJECTIVES			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS			= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_HELP_TIPS				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_HELP_TIPS + 1;
	public static final byte SCREEN_LOADING_1				= SCREEN_CREDITS + 1;	
	public static final byte SCREEN_GRAPH_INFO				= SCREEN_LOADING_1 + 1;	
	public static final byte SCREEN_USER_INFO				= SCREEN_GRAPH_INFO + 1;	
	
	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG				= SCREEN_USER_INFO + 1;	
	//#endif
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL (1� execu��o)">
	
	// menu principal
	public static final byte ENTRY_MENU_BIRITOMETRO	= 0;
	public static final byte ENTRY_MENU_HELP		= 1;
	public static final byte ENTRY_MENU_CREDITS		= 2;
	public static final byte ENTRY_MENU_EXIT		= 3;
	
	//#if DEBUG == "true"
	public static final byte ENTRY_MENU_ERROR_LOG	= ENTRY_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL (CONTINUAR)">
	
	public static final byte ENTRY_CONTINUE_CONTINUE	= 0;
	public static final byte ENTRY_CONTINUE_USER_INFO	= 1;
	public static final byte ENTRY_CONTINUE_BIRITOMETRO	= 2;
	public static final byte ENTRY_CONTINUE_HELP		= 3;
	public static final byte ENTRY_CONTINUE_CREDITS		= 4;
	public static final byte ENTRY_CONTINUE_EXIT		= 5;
	
	//#if DEBUG == "true"
	public static final byte ENTRY_CONTINUE_ERROR_LOG	= ENTRY_CONTINUE_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_TIPS						= 2;
	public static final byte ENTRY_HELP_MENU_BACK						= 3;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>
	
	/** Tempo em milisegundos para ser feita a anima��o de entrada/sa�da. */
	public static final short TIME_APPEAR = 427;
	
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;	
	
	/** Intervalo em milisegundos para que uma tecla seja repetida. */
	public static final short KEY_REPEAT_TIME = 460;	
	
	/** Cor padr�o do fundo de tela. */
	public static final int COLOR_BACKGROUND = 0xffe399;	
	
	/** Cor do cursor. */
	public static final int COLOR_CURSOR = 0xd16767;
	
	/** Cor das linhas divis�rias de cada c�lula. */
	public static final int COLOR_LINE = 0xffe399;
	
	/** Cor da c�lula n�o selecionada. */
	public static final int COLOR_CELL = 0xd3bd83;
	
	/** Cor de fundo da entrada selecionada no seletor de quantidades de biritas. */
	public static final int COLOR_BIRITA_ENTRY_SELECTED = 0xdb6f6f;
	
	/** Cor da �rea do gr�fico abaixo do limite de �lcool no sangue. */
	public static final int COLOR_GREEN = 0x00bb00;
	
	/** Cor de fundo do seletor de quantidades de biritas. */
	public static final int COLOR_BIRITA_EDITOR_BKG = 0x863d3d;
	
	/** Cor da linha tra�ada no gr�fico. */
	public static final int COLOR_GRAPH_LINE = 0xffc600;
	
	/** Cor do pattern azul sob a primeira linha e coluna da tabela. */
	public static final int COLOR_TABLE_BLUE = 0x626679;
	
	/** Quantidade total de horas mostradas a partir da hora de in�cio. */
	public static final byte TOTAL_HOURS = 12;
	
	/** Quantidade m�xima de um tipo de birita por hora. */
	public static final byte BIRITA_MAX_QUANTITY = 12;	
	
	public static final byte BIRITA_PER_LINE_MAX = BIRITA_MAX_QUANTITY / 3;
	
	/** Taxa de elimina��o de �lcool no sangue, em g/h (homens). */
	public static final int FP_ABSORPTION_MEN = 6553; // 0,1
	
	/** Taxa de elimina��o de �lcool no sangue, em g/h (mulheres). */
	public static final int FP_ABSORPTION_WOMEN = 5570; // 0,085
	
	/** N�vel m�ximo de �lcool no sangue permitido pela lei brasileira. */
	public static final int FP_ALCOHOL_LIMIT = 13107; // 0,2g/l
	
	/** N�vel de �lcool no sangue letal. */
	public static final int FP_ALCOHOL_LEVEL_LETHAL = 262144;// 4
	
	public static final int FP_ALCOHOL_LEVEL_1 = 52428; // 0,8 g/l
	
	public static final int FP_ALCOHOL_LEVEL_2 = 98304; // 1,5 g/l
	
	public static final int FP_ALCOHOL_LEVEL_3 = 163840; // 2,5 g/l
	
	/** N�vel de �lcool no sangue m�ximo exibido no gr�fico. */
	public static final int FP_ALCOHOL_LEVEL_MAX = 288356; //NanoMath.mulFixed( FP_ALCOHOL_LEVEL_LETHAL, NanoMath.divInt( 11, 10 ) 
	
	public static final int FP_WEIGHT_MIN = 2621440; // 40
	
	public static final int FP_WEIGHT_MAX = 13107200; // 200
	
	public static final int FP_WEIGHT_DEFAULT = 4587520; // 70
	
	
	//#if SCREEN_SIZE == "SMALL"
//# 	
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	/** Largura de cada coluna de horas, em pixels. */
//# 	public static final byte COLLUMN_WIDTH = 68;
//# 	
//# 	/** Altura de cada linha (birita), em pixels. */
//# 	public static final byte ROW_HEIGHT = 74;
//# 	
//# 	public static final short BIRITA_ENTRY_WIDTH = 116;
//# 	
//# 	/** Largura da barra de scroll, em pixels. */
//# 	public static final byte SCROLL_WIDTH = 10;
//# 	
//# 	public static final byte BIRITA_ENTRY_HEIGHT = 30;
//# 	
//# 	public static final byte BIRITA_ENTRY_SPACING = 2;
//# 	
//# 	public static final byte BIRITA_QUANTITY_WIDTH = BIRITA_ENTRY_HEIGHT - ( BIRITA_ENTRY_SPACING << 1 );
//# 	
//# 	public static final short BIRITA_EDITOR_WIDTH = BIRITA_ENTRY_WIDTH + SCROLL_WIDTH + 3;
//# 	
//# 	/** Altura dos bot�es de sele��o de birita, em pixels. */
//# 	public static final short BUTTON_HEIGHT = 16;
//# 	
//# 	/** Largura padr�o de cada hora no gr�fico (pixels, em nota��o de ponto fixo). */
//# 	public static final int FP_GRAPH_HOUR_WIDTH_DEFAULT = 1572864; // 24
//# 	
//# 	/** Largura do �cone do softkey, em pixels. */
//# 	public static final byte SOFTKEY_WIDTH = 45;
//# 	
//# 	/** Altura do �cone do softkey, em pixels. */
//# 	public static final byte SOFTKEY_HEIGHT = 17;
//# 	
//# 	/** Largura padr�o da tela de splash. */
//# 	public static final short SPLASH_DEFAULT_WIDTH = 128;
//# 	
//# 	/** Altura padr�o da tela de splash. */
//# 	public static final short SPLASH_DEFAULT_HEIGHT = 128;
//# 	
//# 	/** Posi��o x do t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
//# 	public static final byte SPLASH_TITLE_X = 12;
//# 	
//# 	/** Posi��o y do t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
//# 	public static final byte SPLASH_TITLE_Y = 9;
//# 	
//# 	/** Posi��o x do sub-t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
//# 	public static final byte SPLASH_SUBTITLE_X = 54;
//# 	
//# 	/** Posi��o y do sub-t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
//# 	public static final byte SPLASH_SUBTITLE_Y = 34;
//# 	
//# 	/** Posi��o x do gr�fico da tela de splash em rela��o � posi��o 0 da tela padr�o. */
//# 	public static final byte SPLASH_GRAPH_X = 3;
//# 	
//# 	/** Posi��o y do gr�fico da tela de splash em rela��o � posi��o 0 da tela padr�o. */
//# 	public static final byte SPLASH_GRAPH_Y = 0;	
//# 	
//# 	//</editor-fold>
//# 	
	//#elif SCREEN_SIZE == "MEDIUM"
	
	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA M�DIA" defaultstate="collapsed">
	
	/** Largura de cada coluna de horas, em pixels. */
	public static final byte COLLUMN_WIDTH = 68;
	
	/** Altura de cada linha (birita), em pixels. */
	public static final byte ROW_HEIGHT = 74;
	
	public static final short BIRITA_ENTRY_WIDTH = 116;
	
	/** Largura da barra de scroll, em pixels. */
	public static final byte SCROLL_WIDTH = 10;
	
	public static final byte BIRITA_ENTRY_HEIGHT = 30;
	
	public static final byte BIRITA_ENTRY_SPACING = 2;
	
	public static final byte BIRITA_QUANTITY_WIDTH = BIRITA_ENTRY_HEIGHT - ( BIRITA_ENTRY_SPACING << 1 );
	
	public static final short BIRITA_EDITOR_WIDTH = BIRITA_ENTRY_WIDTH + SCROLL_WIDTH + 3;
	
	/** Altura dos bot�es de sele��o de birita, em pixels. */
	public static final short BUTTON_HEIGHT = 16;
	
	/** Largura padr�o de cada hora no gr�fico (pixels, em nota��o de ponto fixo). */
	public static final int FP_GRAPH_HOUR_WIDTH_DEFAULT = 2097152; // 32
	
	/** Largura do �cone do softkey, em pixels. */
	public static final byte SOFTKEY_WIDTH = 45;
	
	/** Altura do �cone do softkey, em pixels. */
	public static final byte SOFTKEY_HEIGHT = 17;
	
	/** Largura padr�o da tela de splash. */
	public static final short SPLASH_DEFAULT_WIDTH = 176;
	
	/** Altura padr�o da tela de splash. */
	public static final short SPLASH_DEFAULT_HEIGHT = 220;
	
	/** Posi��o x do t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
	public static final byte SPLASH_TITLE_X = 12;
	
	/** Posi��o y do t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
	public static final byte SPLASH_TITLE_Y = 9;
	
	/** Posi��o x do sub-t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
	public static final byte SPLASH_SUBTITLE_X = 71;
	
	/** Posi��o y do sub-t�tulo da tela de splash em rela��o � posi��o 0 da tela padr�o. */
	public static final byte SPLASH_SUBTITLE_Y = 48;
	
	/** Posi��o x do gr�fico da tela de splash em rela��o � posi��o 0 da tela padr�o. */
	public static final byte SPLASH_GRAPH_X = 9;
	
	/** Posi��o y do gr�fico da tela de splash em rela��o � posi��o 0 da tela padr�o. */
	public static final byte SPLASH_GRAPH_Y = 0;
	
	//</editor-fold>	
	
	//#endif
	
}
