/**
 * BiritaEditor.java
 * �2008 Nano Games.
 *
 * Created on Jul 31, 2008 2:18:20 PM.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;


/**
 * 
 * @author Peter
 */
public final class BiritaEditor extends UpdatableGroup implements KeyListener, PointerListener, Constants {

	private static DrawableImage imgMinus;
	
	private static DrawableImage imgPlus;
	
	/** Tempo acumulado da anima��o de entrada/sa�da. */
	private short accTime;
	
	private byte state;	
	
	private final Entry[] entries;
	
	private final UpdatableGroup entriesPanel;
	
	private final UpdatableGroup entriesGroup;
	
	private static final byte ENTRY_OK = -1;
	
	private static final byte ENTRY_CANCEL = -2;
	
	private byte currentEntry;
	
	private int keyHeld;
	
	private short keyAccTime;
	
	private final short TOTAL_HEIGHT;
	
	private final TableScreen tableScreen;
	
	private final Birita birita;
	
	private final byte hour;
	
	private final Pattern patternOK;
			
	private final Label labelOK;
	
	private final Pattern patternCancel;
			
	private final Label labelCancel;
	
	private final Pattern scrollBar;
	
	
	public BiritaEditor( TableScreen table, byte hour, Birita birita ) throws Exception {
		super( 10 );
		
		tableScreen = table;
		this.birita = birita;
		this.hour = hour;
		
		final Pattern bkg = new Pattern( COLOR_BIRITA_EDITOR_BKG );
		insertDrawable( bkg );
		
		final RichLabel title = new RichLabel( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CHOOSE_QUANTITY ), BIRITA_EDITOR_WIDTH );
		title.setPosition( 0, title.getHeight() >> 1 );
		insertDrawable( title );
		
		final int[] levels = birita.getLevels();
		
		entries = new Entry[ levels.length ];
		
		final int MAX_ENTRIES_HEIGHT = ScreenManager.SCREEN_HEIGHT - title.getPosY() - ( title.getHeight() << 1 ) - ( BUTTON_HEIGHT << 1 ) - 2;
		final int TOTAL_ENTRIES_HEIGHT = entries.length * ( BIRITA_ENTRY_HEIGHT + 1 );

		// itens do painel: grupo de entradas, moldura da barra de rolagem e a barra de rolagem
		entriesPanel = new UpdatableGroup( 3 );
		entriesPanel.setSize( BIRITA_EDITOR_WIDTH - 2, NanoMath.min( MAX_ENTRIES_HEIGHT, TOTAL_ENTRIES_HEIGHT ) );
		entriesPanel.setPosition( 1, title.getPosY() + ( title.getHeight() << 1 ) );
		insertDrawable( entriesPanel );
		
		entriesGroup = new UpdatableGroup( entries.length );
		entriesGroup.setSize( BIRITA_ENTRY_WIDTH, TOTAL_ENTRIES_HEIGHT );
		entriesPanel.insertDrawable( entriesGroup );

		final DrawableRect border = new DrawableRect( COLOR_BIRITA_ENTRY_SELECTED );
		border.setSize( SCROLL_WIDTH, entriesPanel.getHeight() );
		border.setPosition( entriesGroup.getWidth() + 1, 0 );
		
		scrollBar = new Pattern( COLOR_BACKGROUND );
		scrollBar.setSize( SCROLL_WIDTH - 2, 1 + ( entriesPanel.getHeight() - 2 ) / entries.length );
		scrollBar.setPosition( border.getPosX() + 1, 0 );
		entriesPanel.insertDrawable( scrollBar );
		entriesPanel.insertDrawable( border );
		
		for ( int i = 0, y = 0; i < levels.length; ++i ) {
			entries[ i ] = new Entry( this, birita, hour, i );
			entries[ i ].setPosition( 0, y );
			
			y += entries[ i ].getHeight();
			entriesGroup.insertDrawable( entries[ i ] );
		}
		
		// insere os bot�es
		patternOK = new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
		patternOK.setSize( BIRITA_EDITOR_WIDTH - 2, BUTTON_HEIGHT );
		patternOK.setPosition( 1, entriesPanel.getPosY() + entriesPanel.getHeight() + BIRITA_ENTRY_SPACING );
		insertDrawable( patternOK );
		
		labelOK = new Label( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CONFIRM ) );
		labelOK.setPosition( ( BIRITA_EDITOR_WIDTH - labelOK.getWidth() ) >> 1, patternOK.getPosY() + ( ( BUTTON_HEIGHT - labelOK.getHeight() ) >> 1 ) );
		insertDrawable( labelOK );

		patternCancel = new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
		patternCancel.setSize( BIRITA_EDITOR_WIDTH - 2, BUTTON_HEIGHT );
		patternCancel.setPosition( 1, patternOK.getPosY() + patternOK.getHeight() + 1 );
		insertDrawable( patternCancel );
		
		labelCancel = new Label( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CANCEL ) );
		labelCancel.setPosition( ( BIRITA_EDITOR_WIDTH - labelCancel.getWidth() ) >> 1, patternCancel.getPosY() + ( ( BUTTON_HEIGHT - labelCancel.getHeight() ) >> 1 ) );
		insertDrawable( labelCancel );
		
		setCurrentEntry( 0 );
		
		setSize( BIRITA_EDITOR_WIDTH, labelCancel.getPosY() + labelCancel.getHeight() + 1 );
		bkg.setSize( size );
		
		TOTAL_HEIGHT = ( short ) size.y;
		
		defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		
		setViewport( new Rectangle() );
		
		setState( STATE_APPEARING );
	}
	
	
	public final void update( int delta ) {
		switch ( state ) {
			case STATE_HIDDEN:
			return;
			
			case STATE_APPEARING:
			case STATE_HIDING:
				accTime += delta;
				
				final int FP_PERCENT = NanoMath.divInt( state == STATE_APPEARING ? accTime : TIME_APPEAR - accTime, TIME_APPEAR );
				final int WIDTH = NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( BIRITA_EDITOR_WIDTH ), FP_PERCENT  ) );
				final int HEIGHT = NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( TOTAL_HEIGHT ), FP_PERCENT  ) );
				viewport.set( ScreenManager.SCREEN_HALF_WIDTH - ( WIDTH >> 1 ), ScreenManager.SCREEN_HALF_HEIGHT - ( HEIGHT >> 1 ), WIDTH, HEIGHT );
				
				if ( accTime >= TIME_APPEAR )
					setState( state == STATE_APPEARING ? STATE_SHOWN : STATE_HIDDEN );
			break;
		}
		
		super.update( delta );
		
		if ( keyHeld != 0 ) {
			keyAccTime += delta;
			
			if ( keyAccTime >= KEY_REPEAT_TIME ) {
				keyAccTime %= KEY_REPEAT_TIME;
				
				keyPressed( keyHeld );
			}
		} // fim if ( keyHeld != 0 )		
	}
	

	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_SHOWN:
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						switch ( currentEntry ) {
							case ENTRY_CANCEL:
								setCurrentEntry( ENTRY_OK );
							break;
							
							case ENTRY_OK:
								setCurrentEntry( entries.length - 1 );
							break;
							
							case 0:
							break;
							
							default:
								setCurrentEntry( currentEntry - 1 );
						}
						
					break;

					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						switch ( currentEntry ) {
							case ENTRY_CANCEL:
							break;
							
							case ENTRY_OK:
								setCurrentEntry( ENTRY_CANCEL );
							break;
							
							default:
								if ( currentEntry < entries.length - 1 )
									setCurrentEntry( currentEntry + 1 );
								else
									setCurrentEntry( ENTRY_OK );
						}						
					break;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						switch ( currentEntry ) {
							case ENTRY_OK:
							case ENTRY_CANCEL:
							break;
							
							default:
								entries[ currentEntry ].changeQuantity( -1, true );
						}
					break;

					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						switch ( currentEntry ) {
							case ENTRY_OK:
							case ENTRY_CANCEL:
							break;
							
							default:
								entries[ currentEntry ].changeQuantity( 1, true );
						}
					break;

					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_SOFT_LEFT:
						if ( currentEntry != ENTRY_CANCEL ) {
							setCurrentEntry( ENTRY_OK );
						
                            // define a quantidade de doses ingeridas
                            final byte[] doses = new byte[ entries.length ];
                            for ( byte i = 0; i < doses.length; ++i ) {
                                doses[ i ] = entries[ i ].quantity;
                            }

                            birita.setDoses( hour, doses );
                        }
						
						setState( STATE_HIDING );
					break;
					
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						setCurrentEntry( ENTRY_CANCEL );
						setState( STATE_HIDING );
					break;
				}

				keyHeld = key;
			break;
		}
	}


	public final void keyReleased( int key ) {
		keyHeld = 0;
		keyAccTime = 0;		
	}


	public final void onPointerDragged( int x, int y ) {
		onPointerPressed( x, y );
	}


	public final void onPointerPressed( int x, int y ) {
		if ( patternOK.contains( x, y ) ) {
			keyPressed( ScreenManager.FIRE );
		} else if ( patternCancel.contains( x, y ) ) {
			keyPressed( ScreenManager.KEY_BACK );
		} else if ( entriesPanel.contains( x, y ) ) {
			x -= entriesPanel.getPosX();
			y -= entriesPanel.getPosY();
			
			if ( x <= entriesGroup.getWidth() ) {
				// clicou numa das biritas - obt�m o �ndice da birita clicada
				final int biritaPressed = ( y - entriesGroup.getPosY() ) / BIRITA_ENTRY_HEIGHT;
				
				if ( biritaPressed == currentEntry ) {
					// clicou na birita atual - verifica se clicou nos �cones de + e -
					final Entry e = entries[ currentEntry ];
					
					x -= e.getPosX();
					y -= e.getPosY();
					
					if ( x >= e.minus.getPosX() && x <= e.minus.getPosX() + e.minus.getWidth() ) {
						// usu�rio clicou no s�mbolo de -
						if ( keyHeld != ScreenManager.LEFT )
							keyPressed( ScreenManager.LEFT );
					} else if ( x >= e.plus.getPosX() && x <= e.plus.getPosX() + e.plus.getWidth() ) {
						// usu�rio clicou no s�mbolo de +
						if ( keyHeld != ScreenManager.RIGHT )
							keyPressed( ScreenManager.RIGHT );
					} else {
						// usu�rio n�o clicou sobre um dos �cones - considera de soltou as teclas, para evitar
						// atualiza��o autom�tica das quantidades
						keyReleased( 0 );
					}
				} else {
					setCurrentEntry( biritaPressed );
				}
			} else {
				// clicou na barra de rolagem
				setCurrentEntry( y / scrollBar.getHeight() );
			}
		}
	} // fim do m�todo onPointerPressed( int, int )


	public final void onPointerReleased( int x, int y ) {
		keyReleased( 0 );
	}
	
	
	private final void setCurrentEntry( int entry ) {
		switch ( currentEntry ) {
			case ENTRY_CANCEL:
			case ENTRY_OK:
			break;
			
			default:
				entries[ currentEntry ].setSelected( false );
		}
		currentEntry = ( byte ) NanoMath.min( entry, entries.length - 1 );

		switch ( entry ) {
			case ENTRY_CANCEL:
			break;
			
			case ENTRY_OK:
			break;
			
			default:
				final Entry temp = entries[ currentEntry ];
				temp.setSelected( true );
				
				final int OFFSET_Y = entriesGroup.getPosY() + temp.getPosY();
				
				if ( OFFSET_Y < 0 ) {
					entriesGroup.setPosition( entriesGroup.getPosX(), -temp.getPosY() );
				} else if ( OFFSET_Y > entriesPanel.getHeight() - temp.getHeight() ) {
					entriesGroup.setPosition( entriesGroup.getPosX(), -temp.getPosY() - temp.getHeight() + entriesPanel.getHeight() );
				}
				
				scrollBar.setPosition( scrollBar.getPosX(), ( currentEntry * scrollBar.getHeight() ) );
		}
		
		patternOK.setVisible( currentEntry == ENTRY_OK );
		patternCancel.setVisible( currentEntry == ENTRY_CANCEL );
	}
	
	
	public final void setState( int state) { 
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
				
				tableScreen.setControlMode( TableScreen.CONTROL_MODE_MOVE );
			break;
			
			case STATE_APPEARING:
				switch ( this.state ) {
					case STATE_APPEARING:
					case STATE_SHOWN:
					return;
				}
				
				accTime = 0;
				setVisible( true );
			break;
			
			case STATE_SHOWN:
				setVisible( true );
			break;
			
			case STATE_HIDING:
				switch ( this.state ) {
					case STATE_HIDDEN:
					case STATE_HIDING:
					return;
				}
				
				accTime = 0;
				setVisible( true );
			break;
		}
		
		this.state = ( byte ) state;
	}	
	
	
	/**
	 * Verifica se as doses inseridas est�o dentro do limite de doses por hora, e ajusta a quantidade das outras
	 * doses, caso necess�rio.
	 */
	private final void checkLimits( int index ) {
		short totalDoses = 0;
		byte highestIndex = -1;
		byte highestDose = 0;
		
		for ( byte i = 0; i < entries.length; ++i ) {
			totalDoses += entries[ i ].quantity;
			
			if ( i != index && entries[ i ].quantity > highestDose ) {
				highestDose = entries[ i ].quantity;
				highestIndex = i;
			}
		}
		
		if ( totalDoses > BIRITA_MAX_QUANTITY ) {
			entries[ highestIndex ].changeQuantity( -1, false );
		}
	}
	
	
	public static final void loadImages() throws Exception {
		imgMinus = new DrawableImage( PATH_IMAGES + "minus.png" );
		imgPlus = new DrawableImage( PATH_IMAGES + "plus.png" );
	}
	
	
	private static final class Entry extends UpdatableGroup {

		private final RichLabel labelQuantity;
		
		private final MarqueeLabel labelDose;
		
		private final Pattern patternBkg;
		
		private final Pattern patternQuantity;
		
		private final DrawableImage minus;
		
		private final DrawableImage plus;
		
		private byte quantity;
		
		private final BiritaEditor editor;
		
		private final byte index;
		
		
		private Entry( BiritaEditor editor, Birita birita, int hour, int index ) throws Exception {
			super( 9 );
			
			setSize( BIRITA_ENTRY_WIDTH, BIRITA_ENTRY_HEIGHT );
			
			this.editor = editor;
			this.index = ( byte ) index;
			
			quantity = birita.getDose( ( byte ) hour, ( byte ) index );
			
			patternBkg = new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
			patternBkg.setSize( size );
			insertDrawable( patternBkg );
			
			patternQuantity = new Pattern( COLOR_BACKGROUND );
			patternQuantity.setPosition( BIRITA_ENTRY_SPACING, BIRITA_ENTRY_SPACING );
			patternQuantity.setSize( BIRITA_QUANTITY_WIDTH, BIRITA_QUANTITY_WIDTH );
			insertDrawable( patternQuantity );
			
			labelQuantity = new RichLabel( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), null, BIRITA_QUANTITY_WIDTH );
			labelQuantity.setSize( patternQuantity.getWidth(), labelQuantity.getFont().getHeight() );
			labelQuantity.setPosition( BIRITA_ENTRY_SPACING, ( size.y - labelQuantity.getHeight() >> 1 ) );
			insertDrawable( labelQuantity );
			
			minus = new DrawableImage( imgMinus );
			minus.setPosition( labelQuantity.getPosX() + labelQuantity.getWidth() + BIRITA_ENTRY_SPACING, ( size.y - minus.getHeight() ) >> 1 );
			insertDrawable( minus );
			
			final DrawableImage imgBirita = birita.getIcon( ( byte ) index );
			imgBirita.setPosition( minus.getPosX() + minus.getWidth() + BIRITA_ENTRY_SPACING, ( size.y - imgBirita.getHeight() ) >> 1 );
			insertDrawable( imgBirita );
			
			plus = new DrawableImage( imgPlus );
			plus.setPosition( imgBirita.getPosX() + imgBirita.getWidth() + BIRITA_ENTRY_SPACING, ( size.y - plus.getHeight() ) >> 1 );
			insertDrawable( plus );
			
			final int BIRITA_ENTRY_DOSE_NAME_WIDTH = size.x - plus.getPosX() - plus.getWidth() - 3;
			
			labelDose = new MarqueeLabel( GameMIDlet.getFont( FONT_INDEX_LIGHT  ), birita.getDosesNames()[ index ] );
			labelDose.setSize( BIRITA_ENTRY_DOSE_NAME_WIDTH, labelDose.getFont().getHeight() );
			labelDose.setPosition( plus.getPosX() + plus.getWidth() + BIRITA_ENTRY_SPACING, ( size.y - labelDose.getHeight() ) >> 1 );
			
			insertDrawable( labelDose );
			
			changeQuantity( 0, false );
			
			setSelected( false );
		}
		
		
		private final void setSelected( boolean selected ) {
			patternBkg.setVisible( selected );
			patternQuantity.setVisible( selected );
			minus.setVisible( selected );
			plus.setVisible( selected );
			
			// a entrada selecionada realiza scroll do nome da dose
			if ( selected ) {
				labelDose.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
			} else {
				labelDose.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
				labelDose.setTextOffset( 0 );
			}
		}
		
		
		private final void changeQuantity( int diff, boolean checkLimits ) {
			quantity = ( byte ) NanoMath.clamp( quantity + diff, 0, BIRITA_MAX_QUANTITY );
			
			labelQuantity.setVisible( quantity > 0 );
			if ( quantity > 0 ) {
				labelQuantity.setText( "<ALN_H>x" + quantity );
			}
			
			if ( checkLimits )
				editor.checkLimits( index );
		}
	} // fim da classe interna Entry

}
