/**
 * User.java
 * �2008 Nano Games.
 *
 * Created on Aug 7, 2008 8:13:56 PM.
 */

package core;

import br.com.nanogames.components.util.NanoMath;


/**
 * 
 * @author Peter
 */
public final class User implements Constants {
	
	public static final byte GENRE_MALE		= 0;
	public static final byte GENRE_FEMALE	= 1;

	private static byte genre = GENRE_MALE;

	private static int fp_weight;
	
	private static byte initialHour = 19;

	
	private User() {
	}
	

	public static final int getFp_weight() {
		return fp_weight;
	}


	public static final void setFp_weight( int fp_weight ) {
		User.fp_weight = NanoMath.clamp( fp_weight, FP_WEIGHT_MIN, FP_WEIGHT_MAX );
	}


	public static final byte getGenre() {
		return genre;
	}


	public static final void setGenre( byte genre ) {
		//#if DEBUG == "true"
		switch ( genre ) {
			case GENRE_FEMALE:
			case GENRE_MALE:
			break;
			
			default:
				throw new IllegalArgumentException();
		}
		//#endif
		
		User.genre = genre;
	}


	public static final byte getInitialHour() {
		return initialHour;
	}


	public static final void setInitialHour( byte initialHour ) {
		initialHour = ( byte ) ( initialHour % 24 );
		
		User.initialHour = initialHour;
	}
	
	
	public static final int getFPAbsorptionRate() {
		return genre == GENRE_MALE ? FP_ABSORPTION_MEN : FP_ABSORPTION_WOMEN;
	}


}
