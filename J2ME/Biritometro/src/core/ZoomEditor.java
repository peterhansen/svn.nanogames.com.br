/**
 * ZoomEditor.java
 * �2008 Nano Games.
 *
 * Created on Aug 5, 2008 10:17:51 AM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;


/**
 * 
 * @author Peter
 */
public final class ZoomEditor extends UpdatableGroup implements Constants, KeyListener, PointerListener {
	
	private static DrawableImage IMG_MINUS;
	private static DrawableImage IMG_PLUS;
	private static DrawableImage IMG_SELECTOR_B;
	private static DrawableImage IMG_SELECTOR_F;
	private static DrawableImage IMG_SELECTOR;

	private static final byte ENTRY_ZOOM_LEVEL	= 0;
	private static final byte ENTRY_OK			= 1;
	private static final byte ENTRY_CANCEL		= 2;
	
	private byte currentEntry;

	/** Tempo acumulado da anima��o de entrada/sa�da. */
	private short accTime;
	
	private byte state;	
	
	private final Pattern selectorFill;
	
	private final DrawableImage selector;
	
	private final Label labelZoom;
	
	private final int FP_MOVE_SPEED;
	
	private final MUV selectorMUV = new MUV();
	
	private int fp_currentZoom;
	
	private int fp_previousZoom;
	
	private static final short MIN_INTERVAL = GameMIDlet.GAME_MAX_FRAME_TIME + 1;
	
	private int timeSinceLastKeyPressed = -1;
	
	private boolean dragging;
	
	private final Graph graph;
	
	private final Pattern patternEntry;
			
	private final Label labelOK;
	
	private final Label labelCancel;	
	
	private final short HEIGHT_TOTAL;


	public ZoomEditor( Graph graph ) throws Exception {
		super( 15 );
		
		this.graph = graph;
		
		viewport = new Rectangle();
		
		final Pattern bkg = new Pattern( COLOR_BIRITA_EDITOR_BKG );
		insertDrawable( bkg );
		
		FP_MOVE_SPEED = NanoMath.divFixed( Graph.FP_ZOOM_LEVEL_MAX - Graph.FP_ZOOM_LEVEL_MIN, NanoMath.toFixed( 5 ) );
		
		final RichLabel title = new RichLabel( GameMIDlet.getFont( FONT_INDEX_OUTLINE ), GameMIDlet.getText( TEXT_CHOOSE_ZOOM_LEVEL ), ScreenManager.SCREEN_WIDTH, null );
		insertDrawable( title );
		
		patternEntry = new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
		insertDrawable( patternEntry );		
		
		final DrawableImage borderLeft = new DrawableImage( IMG_SELECTOR_B );
		final DrawableImage borderRight = new DrawableImage( borderLeft );		
		
		final DrawableImage imgMinus = new DrawableImage( IMG_MINUS );
		imgMinus.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		insertDrawable( imgMinus );
		
		final DrawableImage imgPlus = new DrawableImage( IMG_PLUS );
		imgPlus.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( imgPlus );
		
		selectorFill = new Pattern( new DrawableImage( IMG_SELECTOR_F ) );
		selectorFill.setSize( ScreenManager.SCREEN_WIDTH - imgMinus.getWidth() - imgPlus.getWidth() - ( borderLeft.getWidth() << 1 ) - 6, 
							  selectorFill.getFill().getHeight() );
		selectorFill.setPosition( ( ScreenManager.SCREEN_WIDTH - selectorFill.getWidth() ) >> 1, ( title.getHeight() * 3 ) >> 1 );
		insertDrawable( selectorFill );
		
		imgMinus.setRefPixelPosition( BIRITA_ENTRY_SPACING, selectorFill.getPosY() + ( selectorFill.getHeight() >> 1 ) );
		imgPlus.setRefPixelPosition( ScreenManager.SCREEN_WIDTH - BIRITA_ENTRY_SPACING, imgMinus.getRefPixelY() );
		
		borderLeft.setPosition( selectorFill.getPosX() - borderLeft.getWidth(), selectorFill.getPosY() );
		insertDrawable( borderLeft );

		borderRight.setTransform( TRANS_MIRROR_H );
		borderRight.setPosition( selectorFill.getPosX() + selectorFill.getWidth(), selectorFill.getPosY() );
		insertDrawable( borderRight );
		
		selector = new DrawableImage( IMG_SELECTOR );
		selector.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		selector.setRefPixelPosition( selectorFill.getPosX(), selectorFill.getPosY() + ( selectorFill.getHeight() >> 1 ) );
		insertDrawable( selector );
		
		title.setPosition( 0, ( selector.getPosY() - title.getHeight() ) >> 1 );
		
		labelZoom = new Label( GameMIDlet.getFont( FONT_INDEX_OUTLINE  ), "" );
		labelZoom.setPosition( 0, selector.getPosY() + selector.getHeight() );
		insertDrawable( labelZoom );
		
		// insere os bot�es
		labelOK = new Label( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CONFIRM ) );
		labelOK.setPosition( ( ScreenManager.SCREEN_WIDTH - labelOK.getWidth() ) >> 1, 
							 labelZoom.getPosY() + labelZoom.getHeight() + BIRITA_ENTRY_SPACING + ( ( BUTTON_HEIGHT - labelOK.getHeight() ) >> 1 ) );
		insertDrawable( labelOK );

		labelCancel = new Label( GameMIDlet.getFont( FONT_INDEX_LIGHT ), GameMIDlet.getText( TEXT_CANCEL ) );
		labelCancel.setPosition( ( ScreenManager.SCREEN_WIDTH - labelCancel.getWidth() ) >> 1, labelOK.getPosY() + BUTTON_HEIGHT + 1 );
		insertDrawable( labelCancel );		
		
		HEIGHT_TOTAL = ( short ) ( labelCancel.getPosY() + labelCancel.getHeight() + BIRITA_ENTRY_SPACING );
		setSize( ScreenManager.SCREEN_WIDTH, HEIGHT_TOTAL );
		bkg.setSize( size );
		
		setPosition( 0, ScreenManager.SCREEN_HALF_HEIGHT - ( HEIGHT_TOTAL >> 1 ) );
		
		setVisible( false );
		
		setCurrentEntry( ENTRY_ZOOM_LEVEL );
	}	
	
	
	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_SHOWN:
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
						switch ( currentEntry ) {
							case ENTRY_OK:
								setCurrentEntry( ENTRY_ZOOM_LEVEL );
							break;
							
							case ENTRY_CANCEL:
								setCurrentEntry( ENTRY_OK );
							break;
						}
					break;

					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						switch ( currentEntry ) {
							case ENTRY_ZOOM_LEVEL:
								setCurrentEntry( ENTRY_OK );
							break;
							
							case ENTRY_OK:
								setCurrentEntry( ENTRY_CANCEL );
							break;
						}						
					break;

					case ScreenManager.LEFT:
					case ScreenManager.KEY_NUM4:
						switch ( currentEntry ) {
							case ENTRY_ZOOM_LEVEL:
								selectorMUV.setSpeed( -FP_MOVE_SPEED );
								timeSinceLastKeyPressed = 0;
							break;
						}
					break;

					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						switch ( currentEntry ) {
							case ENTRY_ZOOM_LEVEL:
								selectorMUV.setSpeed( FP_MOVE_SPEED );
								timeSinceLastKeyPressed = 0;
							break;
						}
					break;

					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_SOFT_MID:
						setCurrentEntry( ENTRY_OK );
						keyPressed( ScreenManager.FIRE );
					return;
					
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						setCurrentEntry( ENTRY_CANCEL );
						keyPressed( ScreenManager.FIRE );
					return;
						
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						switch ( currentEntry ) {
							case ENTRY_ZOOM_LEVEL:
								setCurrentEntry( ENTRY_OK );
							break;
							
							case ENTRY_CANCEL:
								setCurrentZoom( fp_previousZoom );
							case ENTRY_OK:
								setState( STATE_HIDING );
							break;
						}						
					break;
				}
			break;
		}		
	}


	public final void keyReleased( int key ) {
		if ( timeSinceLastKeyPressed >= 0 && timeSinceLastKeyPressed < MIN_INTERVAL && selectorMUV.getSpeed() != 0 ) {
			final int currentZoom = NanoMath.toInt( NanoMath.mulFixed( fp_currentZoom, NanoMath.toFixed( 100 ) ) );
			
			setCurrentZoom( NanoMath.divInt( currentZoom + NanoMath.sgn( selectorMUV.getSpeed() ), 100 ) );
			
			timeSinceLastKeyPressed = -1;
		}
		
		selectorMUV.setSpeed( 0 );		
	}


	public final void onPointerDragged( int x, int y ) {
		if ( dragging )
			onPointerPressed( NanoMath.clamp( x, selectorFill.getPosX(), selectorFill.getPosX() + selectorFill.getWidth() ), selector.getPosY() );
	}


	public final void onPointerPressed( int x, int y ) {
		if ( dragging || ( y >= selector.getPosY() && y <= selector.getPosY() + selector.getHeight() ) && ( x >= selectorFill.getPosX() && x <= selectorFill.getPosX() + selectorFill.getWidth() ) ) {
			setCurrentEntry( ENTRY_ZOOM_LEVEL );
			
			dragging = true;
			x -= selectorFill.getPosX();
			
			final int FP_PERCENT = NanoMath.divInt( x, selectorFill.getWidth() );
			setCurrentZoom( Graph.FP_ZOOM_LEVEL_MIN + NanoMath.mulFixed( FP_PERCENT, Graph.FP_ZOOM_LEVEL_MAX - Graph.FP_ZOOM_LEVEL_MIN ) );
		} else if ( y >= labelOK.getPosY() && y <= labelOK.getPosY() + labelOK.getHeight() ) {
			if ( currentEntry == ENTRY_OK ) {
				keyPressed( ScreenManager.FIRE );
			} else {
				setCurrentEntry( ENTRY_OK );
			}
		} else if ( y >= labelCancel.getPosY() && y <= labelCancel.getPosY() + labelCancel.getHeight() ) {
			if ( currentEntry == ENTRY_CANCEL ) {
				keyPressed( ScreenManager.FIRE );
			} else {
				setCurrentEntry( ENTRY_CANCEL );
			}
		}
	}


	public final void onPointerReleased( int x, int y ) {
		dragging = false;
		keyReleased( 0 );
	}
	
	
	public final void setState( int state) { 
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
				
				graph.setZoomEditor( false );
			break;
			
			case STATE_APPEARING:
				switch ( this.state ) {
					case STATE_APPEARING:
					case STATE_SHOWN:
					return;
				}
				
				fp_previousZoom = graph.getZoomLevel();
				setCurrentZoom( fp_previousZoom );
				setCurrentEntry( ENTRY_ZOOM_LEVEL );
				
				accTime = 0;
				setVisible( true );
			break;
			
			case STATE_SHOWN:
				setVisible( true );
			break;
			
			case STATE_HIDING:
				switch ( this.state ) {
					case STATE_HIDDEN:
					case STATE_HIDING:
					return;
				}
				
				accTime = 0;
				setVisible( true );
			break;
		}
		
		this.state = ( byte ) state;
	}		
	
	
	public final void update( int delta ) {
		switch ( state ) {
			case STATE_HIDDEN:
			return;
			
			case STATE_APPEARING:
			case STATE_HIDING:
				accTime += delta;
				
				final int FP_PERCENT = NanoMath.divInt( state == STATE_APPEARING ? accTime : TIME_APPEAR - accTime, TIME_APPEAR );
				final int WIDTH = NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( size.x ), FP_PERCENT  ) );
				final int HEIGHT = NanoMath.toInt( NanoMath.mulFixed( NanoMath.toFixed( HEIGHT_TOTAL ), FP_PERCENT  ) );
				viewport.set( ScreenManager.SCREEN_HALF_WIDTH - ( WIDTH >> 1 ), ScreenManager.SCREEN_HALF_HEIGHT - ( HEIGHT >> 1 ), WIDTH, HEIGHT );
				
				if ( accTime >= TIME_APPEAR )
					setState( state == STATE_APPEARING ? STATE_SHOWN : STATE_HIDDEN );
			break;
			
			case STATE_SHOWN:
				if ( timeSinceLastKeyPressed >= 0 )
					timeSinceLastKeyPressed += delta;

				if ( selectorMUV.getSpeed() != 0 ) {
					setCurrentZoom( fp_currentZoom + selectorMUV.updateInt( delta ) );
				}				
			break;
		}
		
		super.update( delta );
	}	
	
	
	private final void setCurrentZoom( int fp_height ) {
		fp_currentZoom = NanoMath.clamp( fp_height, Graph.FP_ZOOM_LEVEL_MIN, Graph.FP_ZOOM_LEVEL_MAX );

		if ( fp_currentZoom <= Graph.FP_ZOOM_LEVEL_MIN || fp_currentZoom >= Graph.FP_ZOOM_LEVEL_MAX )
			selectorMUV.setSpeed( 0 );

		selector.setRefPixelPosition( selectorFill.getPosX() + NanoMath.toInt( NanoMath.mulFixed( NanoMath.divFixed( fp_currentZoom - Graph.FP_ZOOM_LEVEL_MIN, Graph.FP_ZOOM_LEVEL_MAX - Graph.FP_ZOOM_LEVEL_MIN ), 
											NanoMath.toFixed( selectorFill.getWidth() ) ) ), selector.getRefPixelY() );
		
		labelZoom.setText( NanoMath.toString( NanoMath.mulFixed( fp_currentZoom, NanoMath.toFixed( 100 ) ) ) + '%' );
		labelZoom.setPosition( ( size.x - labelZoom.getWidth() ) >> 1, labelZoom.getPosY() );
		
		graph.setZoomLevel( fp_currentZoom );
	}	
	
	
	private final void setCurrentEntry( byte entry ) {
		currentEntry = entry;
		
		switch ( currentEntry ) {
			case ENTRY_ZOOM_LEVEL:
				patternEntry.setSize( size.x - 2, selector.getHeight() + ( BIRITA_ENTRY_SPACING << 1 ) );
				patternEntry.setPosition( 1, selector.getPosY() - BIRITA_ENTRY_SPACING );
			break;
			
			case ENTRY_OK:
			case ENTRY_CANCEL:
				patternEntry.setSize( size.x - 2, BUTTON_HEIGHT );
				patternEntry.setPosition( 1, labelOK.getPosY() + ( ( labelOK.getHeight() - BUTTON_HEIGHT ) >> 1 ) + ( BUTTON_HEIGHT * ( currentEntry - ENTRY_OK ) ) );
			break;
		}
	}
	
	
	public static final void loadImages() throws Exception {
		IMG_SELECTOR_B = new DrawableImage( PATH_IMAGES + "selector_b.png" );
		IMG_MINUS = new DrawableImage( PATH_IMAGES + "minus.png" );
		IMG_PLUS = new DrawableImage( PATH_IMAGES + "plus.png" );
		Thread.yield();
		IMG_SELECTOR_F = new DrawableImage( PATH_IMAGES + "selector_f2.png" );
		IMG_SELECTOR = new DrawableImage( PATH_IMAGES + "selector.png" );
		Thread.yield();
	}

}
