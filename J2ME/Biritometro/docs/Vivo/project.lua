-- descritor para conversão de arquivos para o formato Potenza
-- Peter Hansen

-- exemplos de aparelhos-chefe com poucos caracteres
-- gradiente gf690: grgf690
-- motorola v3: mtv3
-- nokia n76: nkn76
-- nokia 5200: nk5200
-- samsung d820: sgd820
-- samsung c420: c420
-- lg me970: lgme970
-- lg mg155: lgmg155

-- nome do aplicativo/jogo
APP_NAME = [[Biritômetro]]

-- caminho do arquivo original de especificação do aplicativo J2ME
PATH_APP_SPEC = [[326002-EST001-NanoGames.doc]]

-- caminho do formulário original de aprovação de aplicações Vivo Downloads
PATH_FAAVD = [[FAAVD_supportcomm.doc]]

-- caminho do arquivo original de solicitação de teste
PATH_SOL = [[326002-SOL001.01.1.xls]]

VERSIONS = {
	{
		-- big
		  ['jad'] = [[big.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.0]]
		, ['device'] = [[nkn76]]
	},
	{
		-- gf690
		  ['jad'] = [[gf690.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.1]]
		, ['device'] = [[grgf690]]
	},
	{
		-- me970
		  ['jad'] = [[me970.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.2]]
		, ['device'] = [[lgme970]]
	},
	{
		-- medium
		  ['jad'] = [[medium.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.3]]
		, ['device'] = [[mtv3]]
	},
	{
		-- small
		  ['jad'] = [[small.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.4]]
		, ['device'] = [[nk5200]]
	},
}

