/**
 * PlayScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 2, 2008 6:50:28 PM.
 */

package screens;

import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.MUV;
import core.Constants;


/**
 * 
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, PointerListener {
	
	public static final byte STATE_NONE				= 0;
	public static final byte STATE_BEGIN_LEVEL		= 1;
	public static final byte STATE_PLAYING			= 2;
	public static final byte STATE_LEVEL_CLEARED	= 3;
	public static final byte STATE_GAME_OVER		= 4;
	
	private byte state;
	
	public static final short TIME_MESSAGE = 3000;
	
	private short timeToNextState;
	
	private static final byte MAX_LIVES = 3;
	
	/** N�mero de vidas restantes do jogador. */
	private byte lives = MAX_LIVES;
	
	/** Pontua��o do jogador. */
	private int score;
	
	private short level;
	
	/** pontua��o exibida atualmente */
	private int scoreShown;
	
	/** label que indica a pontua��o e o multiplicador */
	private final Label scoreLabel;	
	
	/** Label utilizado para mostrar uma mensagem ao jogador (fim de jogo, n�vel completo, etc.).  */
	private final Label labelMessage;
	
	/** pontua��o m�xima mostrada */
	private static final int SCORE_SHOWN_MAX = 999999;

	/** Velocidade de atualiza��o da pontua��o. */
	private final MUV scoreSpeed = new MUV();	
	
	

	public PlayScreen() throws Exception {
		super( 10 );
		
		// insere o label indicando a pontua��o
		scoreLabel = new Label( GameMIDlet.getFont( FONT_INDEX_DEFAULT ), "0" );
		insertDrawable( scoreLabel );		
		
		// insere o label que mostra mensagens ao jogador
		labelMessage = new Label( GameMIDlet.getFont( FONT_INDEX_DEFAULT ), "" );
		insertDrawable( labelMessage );		
	}
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		// caso o estado atual tenha uma dura��o m�xima definida, atualiza o contador
		if ( timeToNextState > 0 ) {
			timeToNextState -= delta;
			
			if ( timeToNextState <= 0 )
				stateEnded();
		}
		
		// atualiza a pontua��o
		updateScore( delta, false );
	}	


	public final void keyPressed( int key ) {
	}


	public final void keyReleased( int key ) {
	}


	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
	}


	public final void onPointerReleased( int x, int y ) {
	}
	
	
	public final void setState( int state ) {
		this.state = ( byte ) state;
		
		timeToNextState = 0;
		labelMessage.setVisible( false );
		
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				showMessage( String.valueOf( level ) );
				
				timeToNextState = TIME_MESSAGE;
			break;
			
			case STATE_PLAYING:
				labelMessage.setVisible( false );
			break;
			
			case STATE_GAME_OVER:
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ) );
			break;
			
			case STATE_LEVEL_CLEARED:
				showMessage( String.valueOf( level ) );
				timeToNextState = TIME_MESSAGE;
			break;
		}
	}	
	
	
	private final void stateEnded() {
		switch ( state ) {
			case STATE_LEVEL_CLEARED:
				prepareLevel( level + 1 );
			break;
			
			case STATE_BEGIN_LEVEL:
				setState( STATE_PLAYING );
			break;
			
			case STATE_GAME_OVER:
				GameMIDlet.gameOver( score );
			break;
		}
	}
	
	
	private final void prepareLevel( int level ) {
		this.level = ( short ) level;
		
		setState( STATE_BEGIN_LEVEL );
	}	
	
	
	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			updateScoreLabel();
		}
	} // fim do m�todo updateScore( boolean )		

	
	private final void updateScoreLabel() {
		scoreLabel.setText( String.valueOf( scoreShown ) );
	}	
	
	
	/**
	 * Incrementa a pontua��o e atualiza as vari�veis e labels correspondentes.
	 * @param diff varia��o na pontua��o (positiva ou negativa).
	 */
	private final void changeScore( int diff ) {
		score += diff;
		
		scoreSpeed.setSpeed( score - scoreShown, false );
	}	
	
	
	private final void showMessage( String message ) {
		labelMessage.setText( message );
		labelMessage.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		labelMessage.setRefPixelPosition( size.x >> 1, size.y >> 1 );		
		labelMessage.setVisible( true );
	}	
	
	
	public final int getScore() {
		return score;
	}	

}
