/*
 * TextScreen.java
 *
 * Created on June 4, 2007, 3:53 PM
 *
 */

package telas;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.AnimatedTransition;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;


/**
 *
 * @author peter
 */
public final class TelaDeTexto extends RichLabel implements SmackThatConfig {
	
	// estado da anima��o de transi��o
	protected byte transitionState;
	
	private final short TEXT_MOVE_SPEED;
	
	// dire��es do scroll do texto
	private static final byte DIRECTION_NONE		= 0;
	private static final byte DIRECTION_FORWARD		= 1;
	private static final byte DIRECTION_BACKWARD	= 2;
	
	private byte direction = DIRECTION_NONE;	
	
	private final int textLimit;
	
	private int lastSpeedModule;
	
	
	/**
	 * Creates a new instance of TextScreen
	 */
	public TelaDeTexto( ImageFont font, byte textIndex ) throws Exception {
		super( font, null, null );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - font.getImage().getHeight() );
		
		setText( MeuMidlet.getText( textIndex ) );
		
		TEXT_MOVE_SPEED = ( short ) ( font.getImage().getHeight() * 9 / 2 );
		
		textLimit = ( -getTextTotalHeight() + size.y ) >= 0 ? 0 : ( -getTextTotalHeight() + size.y );
	}

	
	public final void keyPressed( int key ) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.UP:
				key = ScreenManager.KEY_NUM2;
			break;
			
			case ScreenManager.DOWN:
				key = ScreenManager.KEY_NUM8;
			break;
			
			case ScreenManager.FIRE:
				key = ScreenManager.KEY_NUM5;
			break;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )


		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
				direction = DIRECTION_NONE;
				MeuMidlet.setScreen( SCREEN_MAIN_MENU );
			return;
			
			default:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
						direction = DIRECTION_BACKWARD;
					break;

					case ScreenManager.KEY_NUM8:
						direction = DIRECTION_FORWARD;
					break;
					
					case ScreenManager.KEY_NUM5:
						MeuMidlet.setScreen( SCREEN_MAIN_MENU );

					default:
						direction = DIRECTION_NONE;
						return;
				} // fim switch ( key )						
		}
	} // fim do m�todo keyPressed( int )
	

	public final void keyReleased( int key ) {
		direction = DIRECTION_NONE;
	}	
	
	
	public final byte getTransitionState() {
		return transitionState;
	}	
	
	
//	public final void updateTransition( int delta ) {
//		switch ( transitionState ) {
//			case TRANSITION_APPEARING:
//				// o drawable que est� entrando na tela � o respons�vel pela atualiza��o das bolhas
//				Bubbles.updateTransition( delta );
//			
//			case TRANSITION_HIDING:				
//				if ( Bubbles.getAccTransitionTime() >= TRANSITION_BUBBLES_TIME ) {
//					if ( transitionState == TRANSITION_APPEARING )
//						setTransitionState( TRANSITION_SHOWN );
//					else
//						setTransitionState( TRANSITION_INACTIVE );
//				}
//			break;
//			
//			default: 
//				return;
//		}
//	} // fim do m�todo updateTransition( int )	
	

//	public final void setTransitionState( byte state ) {
//		switch ( state ) {
//			case TRANSITION_APPEARING:
//				Bubbles.reset();			
//			case TRANSITION_SHOWN:
//			case TRANSITION_HIDING:
//			case TRANSITION_INACTIVE:
//			break;
//			
//			default: 
//				return;
//		}
//		
//		transitionState = state;
//	} // fim do m�todo setTransitionState( byte )	

	
//	public void paint( Graphics g ) {
//		switch ( transitionState ) {
//			case TRANSITION_INACTIVE:
//			case TRANSITION_HIDING:
//			break;
//			
//			default:
//				super.paint( g );
//		}
//	}

	
//	public final void draw( Graphics g ) {
//		switch ( transitionState ) {
//			case TRANSITION_APPEARING:
//			case TRANSITION_HIDING:
//				Bubbles.drawMe( g, this, transitionState );
//			break;
//			
//			default:
//				super.draw( g );
//		}
//	} // fim do m�todo draw( Graphics )

	
//	public final void paintMe( Graphics g ) {
//		switch ( transitionState ) {
//			case TRANSITION_HIDING:
//			case TRANSITION_INACTIVE:
//				super.paint( g );
//			break;
//			
//			default:
//				paint( g );
//		}		
//	}

	
//	public final void update( int delta ) {
//		super.update( delta );
//		
//		switch ( direction ) {
//			case DIRECTION_BACKWARD:
//			case DIRECTION_FORWARD:
//				final int ds = lastSpeedModule + ( ( direction == DIRECTION_BACKWARD ? TEXT_MOVE_SPEED : -TEXT_MOVE_SPEED ) * delta );
//				final int dy = ds / 1000;
//				lastSpeedModule = ds % 1000;
//				
//				if ( dy != 0 ) {
//					setTextOffset( getTextOffset() + dy );
//					
//					if ( getTextOffset() > 0 ) {
//						setTextOffset( 0 );
//						direction = DIRECTION_NONE;
//					} else {
//						if ( getTextOffset() < textLimit ) {
//							setTextOffset( textLimit );
//							direction = DIRECTION_NONE;
//						}
//					}
//				} // fim if ( dy != 0 )
//			break;
//			
//			default:
//				return;
//		} // fim switch ( direction )		
//	}
	
}
