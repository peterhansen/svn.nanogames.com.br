/*
 * YesNoScreen.java
 *
 * Created on May 7, 2007, 12:36 PM
 *
 */

package telas;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.AnimatedTransition;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;


/**
 *
 * @author peter
 */
public final class YesNoScreen extends Menu implements SmackThatConfig {
	
	public static final byte INDEX_YES = 1;
	public static final byte INDEX_NO  = 2;
	
	// estado da anima��o de transi��o
	protected byte transitionState;
	
	private static final byte TOTAL_ITEMS = 5;
	
	
	/**
	 * Creates a new instance of YesNoScreen
	 * @param listener MenuListener who will get the answer.
	 * @param id screen id
	 * @param font font to write the question and the possibles answer.
	 * @param idTitle Text id for the question.
	 */
	public YesNoScreen( MenuListener listener, int id, ImageFont font, int idTitle ) throws Exception {
        super( listener, id, TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		
		RichLabel title = new RichLabel( font, "", null );
		// deixa uma margem de cada lado da tela, para distribuir melhor o texto do t�tulo
		title.setSize( ScreenManager.SCREEN_WIDTH * 9 / 10, title.getTextTotalHeight() );
		title.setText( MeuMidlet.getText( idTitle ) );
		title.setSize( title.getSize().x, title.getTextTotalHeight() );
		
		title.defineReferencePixel( title.getSize().x >> 1, title.getSize().y );
		title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( title );
		
		final int y = ScreenManager.SCREEN_HALF_HEIGHT + title.getSize().y;
		
		Label l = new Label( font, MeuMidlet.getText( TEXT_YES ) );
		l.defineReferencePixel( l.getSize().x, 0 );
		l.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH - ( MENU_ITEMS_SPACING << 1 ), y );
		insertDrawable( l );
		
		l = new Label( font, MeuMidlet.getText( TEXT_NO ) );
		l.defineReferencePixel( 0, 0 );
		l.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH + ( MENU_ITEMS_SPACING << 1 ), y );		
		insertDrawable( l );
		
		setCursor( MeuMidlet.loadCursor(), CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );
		
		setCurrentIndex( INDEX_NO );
	}

	
	public final void setCurrentIndex( int index ) {
		if ( index == 0 )
			index = 1;
		
		super.setCurrentIndex( index );
	}

	
	public final void keyPressed( int key ) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.RIGHT:
				key = ScreenManager.KEY_NUM8;
			break;
			
			case ScreenManager.LEFT:
				key = ScreenManager.KEY_NUM2;
			break;
			
			default:
				switch ( key ) {
					case ScreenManager.KEY_NUM6:
						key = ScreenManager.KEY_NUM8;
					break;
					
					case ScreenManager.KEY_NUM4:
						key = ScreenManager.KEY_NUM2;
					break;
					
					default:
						switch ( ScreenManager.getSpecialKey( key ) ) {
							case ScreenManager.KEY_SOFT_LEFT:
								key = ScreenManager.KEY_NUM5;
							break;
						} // fim switch ( ScreenManager.getSpecialKey( key ) )
					// fim default ==> switch ( key )
				} // fim switch ( key )
			// fim default ==> switch ( ScreenManager.getKeyCodeGameAction( key ) )
		} // fim switch ( ScreenManager.getKeyCodeGameAction( key ) )
		
		super.keyPressed( key );
	}
	
	
	public final byte getTransitionState() {
		return transitionState;
	}	
}
