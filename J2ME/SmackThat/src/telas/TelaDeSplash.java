package telas;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;
/*
 * TelaDeSplash.java
 *
 * Created on June 15, 2007, 4:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class TelaDeSplash extends UpdatableGroup implements KeyListener,SmackThatConfig {
	
	private int timeCont = GAME_SPLASH_TIMECONT;
	
	
	/** Creates a new instance of TelaDeSplash */
	public TelaDeSplash() throws Exception {
		super(10);
		this.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
		DrawableImage logo		= new DrawableImage( PATH_GAMESPLASH + "logo_jimmy.png");
		DrawableImage jimmy		= new DrawableImage( PATH_GAMESPLASH + "jimmy.png");
		DrawableImage diversao	= new DrawableImage( PATH_GAMESPLASH + "diversao.png");
		
		logo.setPosition ( ScreenManager.SCREEN_HALF_WIDTH - (logo.getSize().x >> 1), (ScreenManager.SCREEN_HALF_HEIGHT>>1) - (logo.getSize().y>>1));
		jimmy.setPosition ( ScreenManager.SCREEN_WIDTH - jimmy.getSize().x, ScreenManager.SCREEN_HEIGHT - jimmy.getSize().y);
		diversao.setPosition( 0, ScreenManager.SCREEN_HALF_HEIGHT );
		
		insertDrawable( jimmy );
		insertDrawable( diversao );
		insertDrawable( logo );

	}
	

	public void keyPressed(int key) {
	}

	
	public void keyReleased(int key) {
	}

	public void update(int delta) {
		super.update(delta);
		
		if(timeCont < 0)
			MeuMidlet.setScreen( SCREEN_MAIN_MENU );
		timeCont -=delta;
	}
	
}
