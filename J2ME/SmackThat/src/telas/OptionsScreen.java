/*
 * OptionsScreen.java
 *
 * Created on June 4, 2007, 5:00 PM
 *
 */

package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;


/**
 *
 * @author peter
 */
public final class OptionsScreen extends Menu implements SmackThatConfig{
	
	public static final byte INDEX_OPTIONS		= 0;
	public static final byte INDEX_SOUND		= 1;
	public static final byte INDEX_VIBRATION	= 2;
	public static final byte INDEX_LANGUAGE		= 3;
	public static final byte INDEX_BACK			= 4;
	
	private static final byte TOTAL_LABELS		= 5;
	
	private static final byte TOTAL_ITEMS		= TOTAL_LABELS + 1;
	
	private final Label[] labels = new Label[ TOTAL_LABELS ];
	
	private final ImageFont font;
	private boolean textsLoaded;
	
	
	/** Creates a new instance of OptionsScreen */
	public OptionsScreen( MenuListener listener, ImageFont font, int id ) throws Exception {
		super( listener, id, TOTAL_ITEMS );
		
		this.font = font;
		
		setSize( ScreenManager.SCREEN_WIDTH, 0 );
		
		loadTexts();
		
		final Drawable cursor = MeuMidlet.loadCursor();
		setCursor( cursor, Menu.CURSOR_DRAW_BEFORE_MENU, Graphics.VCENTER | Graphics.LEFT );		
		
		setCircular( true );
		setCurrentIndex( 0 );
	}

	
	public final void keyPressed( int key ) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.FIRE:
				key = ScreenManager.KEY_NUM5;
			break;
			
			case ScreenManager.RIGHT:
				key = ScreenManager.KEY_NUM6;
			break;

			case ScreenManager.LEFT:
				key = ScreenManager.KEY_NUM4;
			break;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )

		switch ( key ) {
			case ScreenManager.KEY_NUM4:
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_NUM6:
				switch ( currentIndex ) {
					case INDEX_SOUND:
						toggleSound();
					break;

					case INDEX_VIBRATION:
						toggleVibration();
					break;
				}				

			default:
				switch ( ScreenManager.getSpecialKey( key ) ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						MeuMidlet.setScreen( SCREEN_MAIN_MENU );
					break;
					
					default:
						super.keyPressed( key );
				}
			// fim default ( switch ( key ) )
		} // fim switch ( key )					
	} // fim do m�todo keyPressed( int )
	
	
	private final void toggleSound() {
		MediaPlayer.setMute( !MediaPlayer.isMuted() );
		
		if ( !MediaPlayer.isMuted() ) {
			// TODO tocar som
		}
			
		loadTexts();
	}
	
	
	private final void toggleVibration() {
		MediaPlayer.setVibration( !MediaPlayer.isVibration() );
		
		if ( MediaPlayer.isVibration() )
			MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
		
		loadTexts();
	}
	
	
	private final void loadTexts() {
		try {
			final String[] texts = new String[] { MeuMidlet.getText( TEXT_OPTIONS ),
												  MeuMidlet.getText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF ),
												  MeuMidlet.getText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON ),
												  MeuMidlet.getText( TEXT_LANGUAGE ),
												  MeuMidlet.getText( TEXT_BACK )
													};

			if ( !textsLoaded ) {
				int y = 0;
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					final Label l = new Label( font, texts[ i ] );

					labels[ i ] = l;
					l.defineReferencePixel( l.getSize().x >> 1, 0 );
					l.setRefPixelPosition( size.x >> 1, y );
					insertDrawable( l );

					y += l.getSize().y + MENU_ITEMS_SPACING;
					
					if ( i == 0 )
						y += MENU_ITEMS_SPACING;
				}
				
				setSize( size.x, y );
				defineReferencePixel( size.x >> 1, size.y >> 1 );
				setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );				
				
				textsLoaded = true;
			} else {
				for ( int i = 0; i < TOTAL_LABELS; ++i ) {
					final Label l = ( Label ) getDrawable( i );

					l.setText( texts[ i ] );

					l.defineReferencePixel( l.getSize().x >> 1, 0 );
					l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
				}
			}
			
			MeuMidlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
			
			// reposiciona o cursor (largura do texto pode mudar)
			updateCursorPosition();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	} // fim do m�todo loadTexts()

	
	public final void setCurrentIndex( int index ) {
		if ( index == 0 ) {
			if ( currentIndex == 0 || currentIndex == ( activeDrawables - 1 ) )
				index = 1;
			else
				index = activeDrawables - 1;
		}
		
		super.setCurrentIndex( index );
	}
	
}
