package telas;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;
/*
 * TelaDeRecorde.java
 *
 * Created on June 19, 2007, 5:47 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class TelaDeRecorde extends UpdatableGroup implements KeyListener,SmackThatConfig{
	
	private final Label slots[] = new Label[ TOTAL_RECORDS ];
	private int scoreBlinkDelay;
	
	/** Creates a new instance of TelaDeRecorde */
	public TelaDeRecorde() throws Exception {
		super(1000);
		
		for (int i = 0; i < TOTAL_RECORDS ; ++i){
			slots[i] = new Label( MeuMidlet.imgFont,(i+1) + ") " + MeuMidlet.topRecords[i] );
			slots[i].setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( slots[i].getSize().x >> 1 ),
								( ScreenManager.SCREEN_HEIGHT * 30/100) + (ScreenManager.SCREEN_HALF_HEIGHT / TOTAL_RECORDS) * i );
			
			insertDrawable(slots[i]);
		}
		
		
		Label recorde = new Label(MeuMidlet.imgFont,MeuMidlet.getText(TEXT_RECORD));
		recorde.setPosition( ScreenManager.SCREEN_HALF_WIDTH - (recorde.getSize().x >> 1 ), recorde.getSize().y );

		Label anyKey = new Label(MeuMidlet.imgFont,MeuMidlet.getText(TEXT_BACK));
		anyKey.setPosition( ScreenManager.SCREEN_HALF_WIDTH - (anyKey.getSize().x >> 1 ), ScreenManager.SCREEN_HEIGHT - (anyKey.getSize().y * 2 ));
		
		insertDrawable( recorde );
		insertDrawable( anyKey );
	}
	

	public void keyPressed(int key) {
		MeuMidlet.setScreen(SCREEN_MAIN_MENU);//Todo: Isso falha TBT !!!
	}

	public void keyReleased(int key) {
	}

	public void update(int delta) {
		super.update(delta);
		
		scoreBlinkDelay += delta;
		
		if( scoreBlinkDelay > SCOREBLINKDELAY && MeuMidlet.scoreEntry != TOTAL_RECORDS ){
			scoreBlinkDelay = 0;
			
			slots[ MeuMidlet.scoreEntry ].setVisible( !slots[ MeuMidlet.scoreEntry ].isVisible() );
		}
		
		
	}
	
	
	
}
