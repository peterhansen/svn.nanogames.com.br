package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import java.util.Random;
import javax.microedition.lcdui.Image;
import util.MyRandom;
import jogo.SmackThatConfig;
import util.Fisica;
/*
 * TelaBackGroundSpace.java
 *
 * Created on 7 de Junho de 2007, 15:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Malvezzi
 */
public class TelaBackGroundSpace extends UpdatableGroup implements SmackThatConfig {
	
	protected DrawableImage[] smallStar = new DrawableImage[NUMERO_SMALLSTAR];
	protected DrawableImage[] bigStar   = new DrawableImage[NUMERO_BIGSTAR];
	protected DrawableImage nebula1;
	protected DrawableImage nebula2;
	protected Pattern space;
	
	protected Fisica[] fisicaSmallStar	= new Fisica[ NUMERO_SMALLSTAR ];
	protected Fisica[] fisicaBigStar	= new Fisica[ NUMERO_BIGSTAR ];
	protected Fisica fisicaNebula1 = new Fisica();
	protected Fisica fisicaNebula2 = new Fisica();

	private final MyRandom rand			= new MyRandom( );
	static long teste = System.currentTimeMillis();
	
 
	/** Creates a new instance of BackGroundSky */
	public TelaBackGroundSpace( ) throws Exception{
		super( 1000 );
		// Pano de Fundo
		space = new Pattern( null );
		space.setFillColor( 0x000000 );
		space.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		this.insertDrawable(space);
		
		criaAnimacao();
	}

	
	public void criaAnimacao()throws Exception{
		
		nebula1 = new DrawableImage( PATH_IMG + "nebula1.png");
		nebula2 = new DrawableImage( PATH_IMG + "nebula2.png");
		
		nebula1.setPosition( rand.nextInt(ScreenManager.SCREEN_WIDTH), rand.nextInt( ScreenManager.SCREEN_HALF_HEIGHT >> 1));
		nebula2.setPosition( rand.nextInt(ScreenManager.SCREEN_WIDTH), ScreenManager.SCREEN_HEIGHT - nebula2.getSize().y);
		fisicaNebula1.setMRU( new Point( - NEBULASPEED , 0), nebula1.getPosition());
		fisicaNebula2.setMRU( new Point( - NEBULASPEED , 0), nebula2.getPosition());
		insertDrawable(nebula1);
		insertDrawable(nebula2);
		
		
		createStarArray ( bigStar, fisicaBigStar, PATH_IMG + "bigStar.png");
		createStarArray ( smallStar, fisicaSmallStar, PATH_IMG + "smallStar.png");
		
	}

	
	protected void createStarArray( DrawableImage[] vetDraw, Fisica[] vetFis, String imagem)throws Exception{
		for(int i=0; i < vetDraw.length; ++i){
			vetDraw[i] = new DrawableImage( imagem );
			vetDraw[i].setPosition(-ScreenManager.SCREEN_HALF_WIDTH,ScreenManager.SCREEN_HEIGHT);
			vetFis[i] = new Fisica();
	 		vetFis[i].setMRU(new Point(-3000,1), vetDraw[i].getPosition());
			insertDrawable(vetDraw[i]);
		}
	}


	protected void updateStarArray(DrawableImage[] vetDraw, Fisica[] vetFis, int retardo, int delta){
		for(int i = 0; i < vetDraw.length;i++){
			vetFis[i].atualiza(delta);

			if(vetDraw[i].getPosition().x < 0 - vetDraw[i].getSize().x){
				int height;
				int velocidade;

				//altera a posicao da estrela
				height = rand.nextInt(ScreenManager.SCREEN_HEIGHT);
				velocidade = - rand.nextInt( (ScreenManager.SCREEN_HALF_WIDTH << 1) + ScreenManager.SCREEN_WIDTH/(retardo)  );
						
				vetDraw[i].setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt(ScreenManager.SCREEN_WIDTH), height);
				//altera a velocidade da estrela
				vetFis[i].setMRU( new Point (velocidade, rand.nextInt(2)-1 ), vetDraw[i].getPosition());
			}
		}
	}
	
	
	public void update(int delta){
		super.update(delta);
		
		fisicaNebula1.atualiza(delta);
		fisicaNebula2.atualiza(delta);
		
		if ( nebula1.getPosition().x < -nebula1.getSize().x )
			nebula1.setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt(ScreenManager.SCREEN_HALF_WIDTH), rand.nextInt(ScreenManager.SCREEN_HALF_HEIGHT));
		if ( nebula2.getPosition().x < -nebula2.getSize().x )
			nebula2.setPosition( ScreenManager.SCREEN_WIDTH + rand.nextInt( ScreenManager.SCREEN_HALF_WIDTH), ScreenManager.SCREEN_HEIGHT - nebula2.getSize().y);
		
		updateStarArray( smallStar, fisicaSmallStar, DELAYSMALL, delta );
		updateStarArray( bigStar,   fisicaBigStar,   DELAYBIG,   delta );

		space.setFillColor( 0x000000 );
		space.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		
	}
}
