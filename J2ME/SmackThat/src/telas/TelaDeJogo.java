package telas;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.FrameSet;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import java.util.Random;
import javax.microedition.lcdui.Image;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;
import jogo.Tabuleiro;
/*
 * TelaDeJogo.java
 *
 * Created on June 15, 2007, 5:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class TelaDeJogo extends UpdatableGroup implements KeyListener, SmackThatConfig {
	
	static long teste = System.currentTimeMillis();
	private static final short TOTALDEITENS = 1000;	// TODO: 1000?
	//private final Random rand				= new Random( teste += 34217 );
	//private final Point cursor				= new Point();
	
	private static int scoreShown;
	private int dv;
	private int dva;
	
	private final Tabuleiro tabuleiro;

	private final Label lbPontos			;
	private final Label lbFase				;
	private final Label lbTirosRestantes	;
	
	

	/** Creates a new instance of TelaDeJogo */
	public TelaDeJogo() throws Exception {
		super(TOTALDEITENS);
		
		MeuMidlet.scoreEntry = TOTAL_RECORDS;
		
		setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
		
		DrawableImage jimmy  = new DrawableImage( PATH_TELADEJOGO + "jimmy_game_play.png");
		DrawableImage placar = new DrawableImage( PATH_TELADEJOGO + "placar.png");
		
		jimmy.setPosition( ScreenManager.SCREEN_WIDTH - jimmy.getSize().x, ScreenManager.SCREEN_HEIGHT - jimmy.getSize().y);
		placar.setPosition( (jimmy.getPosition().x + (jimmy.getSize().x >>1)) - placar.getSize().x, ScreenManager.SCREEN_HEIGHT - placar.getSize().y);
		
		
		lbPontos			= new Label( MeuMidlet.myFont, " PONTOS :00000"); //TODO:getText();
		lbFase				= new Label( MeuMidlet.myFont, " FASE: 1" );
		lbTirosRestantes	= new Label( MeuMidlet.myFont, " TIROS: 30" );
		tabuleiro			= new Tabuleiro( this );
		
		tabuleiro.setRefPixelPosition( size.x >> 1, (tabuleiro.getSize().y>>1) + ScreenManager.SCREEN_HEIGHT / 50 );
		
		
		lbFase.			setPosition ( tabuleiro.getPosition().x, tabuleiro.getPosition().y + tabuleiro.getSize().y + SPACER_LABEL_Y);
		lbTirosRestantes.setPosition( placar.getPosition().x + SPACER_LABEL_X, ScreenManager.SCREEN_HEIGHT - lbTirosRestantes.getSize().y );
		lbPontos.		setPosition ( placar.getPosition().x + SPACER_LABEL_X, lbTirosRestantes.getPosition().y - lbPontos.getSize().y - SPACER_LABEL_Y );
		
		
		insertDrawable( tabuleiro);
		insertDrawable( placar );
		insertDrawable( jimmy );
		insertDrawable( lbPontos );
		insertDrawable( lbFase );
		insertDrawable( lbTirosRestantes );
		
	}
	

	public final void keyPressed(int key) {
		switch ( ScreenManager.getSpecialKey( key ) ) {
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				MeuMidlet.setScreen( SCREEN_YN_BACKGAME );
			break;
			case ScreenManager.RIGHT:
				key = ScreenManager.KEY_NUM6;
			break;
			
			case ScreenManager.LEFT:
				key = ScreenManager.KEY_NUM4;
			break;
			
			case ScreenManager.UP:
				key = ScreenManager.KEY_NUM2;
			break;
			
			case ScreenManager.DOWN:
				key = ScreenManager.KEY_NUM8;
			break;
			
			case ScreenManager.FIRE:
				key = ScreenManager.KEY_NUM5;
			break;
		}
		
		switch ( key ) {
			case ScreenManager.KEY_NUM6:
				tabuleiro.moveCursor( DIR_RIGHT );
			break;
			
			case ScreenManager.KEY_NUM4:
				tabuleiro.moveCursor( DIR_LEFT );
			break;
			
			case ScreenManager.KEY_NUM2:
				tabuleiro.moveCursor( DIR_UP );
			break;
			
			case ScreenManager.KEY_NUM8:
				tabuleiro.moveCursor( DIR_DOWN );
			break;			
			
			case ScreenManager.KEY_NUM5:
				tabuleiro.fire();
			break;
			
			case ScreenManager.KEY_NUM0:
				// TODO: TESTE!
				//#if DEBUG == "true"
				tabuleiro.fillSmacks( 72 );
				//#endif
			break;
			
			case ScreenManager.KEY_NUM1:
				tabuleiro.menosShoots();
			
			default:
				}
	}


	
	public void keyReleased(int key) {
	}

	
	
	public final void resetScoreShown(){
		scoreShown = 0;
	}
	

	
	private final void updateScore( boolean equalize, int delta ) {
		if ( scoreShown < MeuMidlet.score ) {
			if ( equalize ) {
				scoreShown = MeuMidlet.score;
			} else  {
				dv  = (dva + (delta * SCORE_INCREMENT)) / 1000;
				dva = (delta * SCORE_INCREMENT) % 1000;
				scoreShown += dv;
				if ( scoreShown > MeuMidlet.score )
				scoreShown = MeuMidlet.score;
			}
			//labelScore.setText( String.valueOf( scoreShown ) );
		}
	} // fim do m�todo updateScore( boolean )


	
	public void update(int delta) {
		super.update(delta);

		updateScore(false,delta);
		lbPontos.setText("PONTOS: " + scoreShown);

		lbTirosRestantes.setText( "TIROS: " + tabuleiro.shoots );
		lbFase.setText("FASE: " + tabuleiro.levelCount);
		tabuleiro.updateLabels = false;
		
		
	}
}
