/*
 * TelaSplashNick.java
 *
 * Created on June 18, 2007, 7:35 PM
 *
 */

package telas;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AnimatedTransition;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import jogo.MeuMidlet;

/**
 *
 * @author peter
 */
public final class TelaSplashNick extends UpdatableGroup {
	
	// tempo total em milisegundos em que a logo da Nick � exibida
	private final int TOTAL_LOGO_TIME = 3000;
	
	private int accTime;
	
	private static final int[][] conversionTable = { { 0x199cb9, 0xf18a00 } };

	private final byte nextScreen;
	
	
	/**
	 * Creates a new instance of TelaSplashNick
	 */
	public TelaSplashNick( String imagesPath, String text, byte nextScreen ) throws Exception {
		super( 3 );
		
		this.nextScreen = nextScreen;
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		// insere um background "dummy", para que durante a transi��o para o o splash do jogo n�o seja vista incorretamente
		// a mudan�a de background, que passa da cor preta para o pattern das flores
		//final Pattern bkg = new Pattern( null );
//		bkg.setFillColor( 0x000000 );
//		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//		insertDrawable( bkg);
		
		final DrawableImage logo = new DrawableImage( imagesPath + "nick.png" );
		logo.defineReferencePixel( logo.getSize().x >> 1, logo.getSize().y >> 1 );
		logo.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
		insertDrawable( logo );
		
		final Image fontImage = ImageLoader.loadImage( imagesPath + "font_credits.png", conversionTable );
		final ImageFont font = ImageFont.createMultiSpacedFont( fontImage, imagesPath + "font_credits.dat" );
		
		final RichLabel label = new RichLabel( font, null, null );
		label.setSize( size.x * 3 / 4, 0 );
		label.setText( text );
		label.setSize( label.getSize().x, label.getTextTotalHeight() );
		label.defineReferencePixel( label.getSize().x >> 1, label.getSize().y );
		label.setRefPixelPosition( size.x >> 1, size. y );
		insertDrawable( label );		
	}

	
	public final void update( int delta ) {
		accTime += delta;
		
		if ( accTime >= TOTAL_LOGO_TIME )
			MeuMidlet.setScreen( nextScreen );
		
	}
	
}
