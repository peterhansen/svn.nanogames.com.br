package telas;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import jogo.MeuMidlet;
import jogo.SmackThatConfig;
/*
 * TelaDeFimDeJogo.java
 *
 * Created on July 23, 2007, 5:14 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class TelaDeFimDeJogo extends UpdatableGroup implements KeyListener,MenuListener,SmackThatConfig{
	
	private int scoreBlinkDelay;
	private int delayKeyBoard;
	private Label recordValue;
	private Menu endGameMenu;
	
	
	/** Creates a new instance of TelaDeFimDeJogo */
	public TelaDeFimDeJogo() throws Exception {
		super(1000);//Todo: 1000?
		this.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		endGameMenu	= new Menu( this, SCREEN_FIMDEJOGO, 3 );
		Label gameOver		= new Label( MeuMidlet.myFont, MeuMidlet.getText( TEXT_GAMEOVER ));
		Label menuReStart	= new Label( MeuMidlet.myFont, MeuMidlet.getText( TEXT_RESTART ));
		Label menuRecord	= new Label( MeuMidlet.myFont, MeuMidlet.getText( TEXT_RECORD ));
		Label menuBack		= new Label( MeuMidlet.myFont, MeuMidlet.getText( TEXT_BACK ));
		Label newRecord		= new Label( MeuMidlet.myFont, MeuMidlet.getText( TEXT_NEWRECORD ));
		recordValue	= new Label( MeuMidlet.myFont, String.valueOf(MeuMidlet.score));
		
		gameOver.setPosition	( ScreenManager.SCREEN_HALF_WIDTH - (gameOver.getSize().x >>1)		, ScreenManager.SCREEN_HALF_HEIGHT >> 1);
		newRecord.setPosition	( ScreenManager.SCREEN_HALF_WIDTH - (newRecord.getSize().x >>1)		, gameOver.getPosition().y  + (gameOver.getSize().y <<1));
		recordValue.setPosition	( ScreenManager.SCREEN_HALF_WIDTH - (recordValue.getSize().x >>1)	, newRecord.getPosition().y + (newRecord.getSize().y<<1));
		menuReStart.setPosition ( ScreenManager.SCREEN_HALF_WIDTH - (menuReStart.getSize().x >>1)	, gameOver.getPosition().y  + (menuReStart.getSize().y <<1));
		menuRecord.setPosition	( ScreenManager.SCREEN_HALF_WIDTH - (menuRecord.getSize().x >>1)	, menuReStart.getPosition().y + (menuRecord.getSize().y <<1));
		menuBack.setPosition	( ScreenManager.SCREEN_HALF_WIDTH - (menuBack.getSize().x >>1)		, menuRecord.getPosition().y  + (menuBack.getSize().y <<1));
		
		//Todo: new record ? yes : no
		if(  MeuMidlet.scoreEntry != TOTAL_RECORDS ){
			newRecord.setVisible( true );
			
		}
		else{
			newRecord.setVisible( false );
		}
		
		endGameMenu.setSize( ScreenManager.SCREEN_WIDTH, menuBack.getPosition().y + ( menuBack.getSize().y << 1 ) );
		endGameMenu.defineReferencePixel( endGameMenu.getSize().x >> 1, endGameMenu.getSize().y >> 1 );
		endGameMenu.setRefPixelPosition(  ScreenManager.SCREEN_HALF_WIDTH,ScreenManager.SCREEN_HALF_HEIGHT );

		
		endGameMenu.insertDrawable( menuReStart );
		endGameMenu.insertDrawable( menuRecord );
		endGameMenu.insertDrawable( menuBack );
		
		
		
		endGameMenu.setCircular( true );
		final Drawable cursor = MeuMidlet.loadCursor();
		endGameMenu.setCursor( cursor, Menu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT|Graphics.VCENTER );
		insertDrawable( gameOver );
		insertDrawable( newRecord );
		insertDrawable( recordValue);
		insertDrawable( endGameMenu );
		endGameMenu.setVisible( false );
	}
	
	

	public void onChoose(int id, int index) {
		switch ( id ) {
			case SCREEN_FIMDEJOGO:
				switch ( index ) {
					case OPT_ENDGAME_RETRY:
						MeuMidlet.setScreen( SCREEN_JOGO );
						break;
					case OPT_ENDGAME_RECORD:
						MeuMidlet.setScreen( SCREEN_RECORD );
						break;
					case OPT_ENDGAME_BACK:
						MeuMidlet.setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;
		}
	}

	public void itemChanged(int id, int index) {
	}


	public void keyPressed(int key) {
		if( delayKeyBoard > DELAY_KEY_BOARD ){
			endGameMenu.keyPressed( key );
		}
	}

	public void keyReleased(int key) {
	}

	
	public void update(int delta) {
		super.update(delta);
		scoreBlinkDelay += delta;
		delayKeyBoard += delta;
		if( scoreBlinkDelay > SCOREBLINKDELAY && MeuMidlet.scoreEntry != TOTAL_RECORDS ){
			scoreBlinkDelay = 0;
			
			recordValue.setVisible( !recordValue.isVisible() );
		}
		
		if ( delayKeyBoard > DELAY_KEY_BOARD )
			endGameMenu.setVisible( true );
		
	}
}
