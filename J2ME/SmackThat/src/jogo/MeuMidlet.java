package jogo;
/*
 * MeuMidlet.java
 *
 * Created on June 15, 2007, 3:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
/*
 * MeuMidlet.java
 *
 * Created on May 7, 2007, 6:49 PM
 */

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Serializable;
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;
import telas.TelaDeFimDeJogo;
import telas.TelaDeSplash;
import telas.YesNoScreen;
import telas.TelaDeJogo;
import telas.TelaDeRecorde;
import telas.TelaBackGroundSpace;
import telas.TelaDeTexto;
import telas.TelaSplashNano;
import telas.TelaSplashNick;

/**
 *
 * @author  malvezzi
 * @version
 */
public class MeuMidlet extends AppMIDlet implements MenuListener, SmackThatConfig {
	
	private static MeuMidlet instancia;
	
	private static String[] texts;
	private static byte language = LANGUAGE_ENGLISH;

	public static int score;													// Main score
	public static byte scoreEntry = TOTAL_RECORDS;								// Position on Record
	public static int level;													// Main Level
	public static int topRecords[] = new int[TOTAL_RECORDS];

	public static Drawable saveScreen = null;									// Game Screen saved
	public static ScreenManager meuScreenManager;
	public static ImageFont imgFont ;											// Main font
	public static ImageFont myFont ;											// Second font

	public static HighScoreManage highScoreManage;
	
	
	public void startApp() throws MIDletStateChangeException {
		super.startApp();
		if ( instancia == null ) {
			meuScreenManager = ScreenManager.createInstance( false, 100 );
			meuScreenManager.start();
			// inicializa dados etc
			instancia = this;
			UpdatableGroup UpGroup = null;
			
			imgFont = ImageFont.createMultiSpacedFont( PATH_IMG + "font.png",PATH_IMG + "font.dat" );
			myFont  = ImageFont.createMultiSpacedFont( PATH_IMG + "minhafont.png", PATH_IMG + "minhaFonte.dat");
			highScoreManage = new HighScoreManage();
			try {
				if ( !hasDatabase(DATABASE_NAME) )
					createDatabase( DATABASE_NAME, DATABASE_IDX_TOTAL);

				MediaPlayer.init( DATABASE_NAME, DATABASE_IDX_OPTIONS, new String[] {	PATH_SOUND + "ending.mid",
																						PATH_SOUND + "level_clear.mid",
																						PATH_SOUND + "level_lost.mid",
																						PATH_SOUND + "splash.mid"});
				
				MeuMidlet.loadData(DATABASE_NAME, DATABASE_IDX_RECORD,MeuMidlet.highScoreManage);
			
			} catch (Exception ex) {
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
			
			
			setLanguage(LANGUAGE_PORTUGUESE);
			
			
			
			
			try {
				TelaBackGroundSpace bkgd = new TelaBackGroundSpace();
				bkgd.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
				meuScreenManager.setBackground(bkgd,true);
//				meuScreenManager.setBackground( null, true );
//				meuScreenManager.setBackgroundColor(0x000000);
	

			} catch (Exception ex) {
				exit();
			} finally {
				//#if DEBUG == "true"
				System.out.println("finally s");
				//#endif
			}
			setScreen( SCREEN_MAIN_MENU );
			//setScreen(SCREEN_SPLASH_NANO);
			//setScreen( SCREEN_SPLASH_GAME );
		}
	}
	

	public static final Drawable loadCursor(){
		DrawableCircle cursor	= new DrawableCircle();
		cursor.color_in  = 0xff0000;
		cursor.color_out = 0xffffff;
		cursor.setSize(10, 10);
		cursor.defineReferencePixel(cursor.getSize().x * 3 / 2, cursor.getSize().y >> 1);
		return cursor;
	}
	
	
	public static final Menu createMainMenu()throws Exception{
		
		Menu mainMenu = new Menu( instancia, SCREEN_MAIN_MENU, 6 );
		
		Label main_novo = new Label( myFont, getText( TEXT_NEW_GAME ));
		Label main_optn = new Label( myFont, getText( TEXT_OPTIONS ));
		Label main_recd = new Label( myFont, getText( TEXT_RECORD ));
		Label main_help = new Label( myFont, getText( TEXT_HELP ));
		Label main_cred = new Label( myFont, getText( TEXT_CREDITS ));
		Label main_sair = new Label( myFont, getText( TEXT_EXIT ));
		
		
		//main_help.setRefPixelPosition(40,60);
		main_novo.defineReferencePixel( main_novo.getSize().x >> 1, 0 );
		main_novo.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, main_novo.getSize().y);
		main_optn.defineReferencePixel( main_optn.getSize().x >> 1, 0 );
		main_optn.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, main_novo.getPosition().y + ( main_novo.getSize().y << 1));
		main_recd.defineReferencePixel( main_recd.getSize().x >> 1, 0 );
		main_recd.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, main_optn.getPosition().y + ( main_optn.getSize().y << 1));
		main_help.defineReferencePixel( main_help.getSize().x >> 1, 0 );
		main_help.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, main_recd.getPosition().y + ( main_recd.getSize().y << 1));
		main_cred.defineReferencePixel( main_cred.getSize().x >> 1, 0 );
		main_cred.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, main_help.getPosition().y + ( main_help.getSize().y << 1));
		main_sair.defineReferencePixel( main_sair.getSize().x >> 1, 0 );
		main_sair.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, main_cred.getPosition().y + ( main_cred.getSize().y << 1));
		
		//Main Menu
		mainMenu.setSize(ScreenManager.SCREEN_WIDTH, main_sair.getPosition().y + ( main_sair.getSize().y << 1 ) );
		mainMenu.defineReferencePixel( mainMenu.getSize().x >> 1, mainMenu.getSize().y >> 1 );
		mainMenu.setRefPixelPosition(  ScreenManager.SCREEN_HALF_WIDTH,ScreenManager.SCREEN_HALF_HEIGHT );
		
		mainMenu.insertDrawable(main_novo);
		mainMenu.insertDrawable(main_optn);
		mainMenu.insertDrawable(main_recd);
		mainMenu.insertDrawable(main_help);
		mainMenu.insertDrawable(main_cred);
		mainMenu.insertDrawable(main_sair);
		
		mainMenu.setCircular(true);
		final Drawable cursor = loadCursor();
		mainMenu.setCursor(cursor,Menu.CURSOR_DRAW_AFTER_MENU, Graphics.LEFT|Graphics.VCENTER);
		
		return mainMenu;
	}
	
	
	
	public void onChoose(int id, int index) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				switch ( index ) {
					case OPT_NEWGAME:
						setScreen( SCREEN_JOGO );
						break;
					case OPT_OPTION:
						setScreen( SCREEN_OPTIONS );
						break;
					case OPT_RECORD:
						setScreen( SCREEN_RECORD );
						break;
					case OPT_HELP:
						setScreen( SCREEN_HELP );
						break;
					case OPT_CREDITS:
						setScreen( SCREEN_CREDITOS );
						break;
					case OPT_EXIT:
						MeuMidlet.exit();
			
				}
				break;
			case SCREEN_YN_BACKGAME:
				switch ( index ){
					case YesNoScreen.INDEX_YES:
						setScreen( SCREEN_MAIN_MENU );
						break;
					case YesNoScreen.INDEX_NO:
						setScreen( SCREEN_GAMEBACK );
						break;
				}
				break;

			case SCREEN_FIMDEJOGO:
				switch ( index ) {
					case OPT_ENDGAME_RETRY:
						MeuMidlet.setScreen( SCREEN_JOGO );
						break;
					case OPT_ENDGAME_RECORD:
						MeuMidlet.setScreen( SCREEN_RECORD );
						break;
					case OPT_ENDGAME_BACK:
						MeuMidlet.setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;
		}
	}
	

	
	public static final void setScreen( byte index ) {
		Drawable nextScreen = null;
		try {
			switch ( index ) {
				case SCREEN_MAIN_MENU:
					nextScreen = createMainMenu();
					meuScreenManager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
					saveScreen = null;
					break;
				case SCREEN_JOGO:
					nextScreen = criaJogo();
					saveScreen = nextScreen;
				case SCREEN_CONTINUE:
					nextScreen = saveScreen;
					break;
				case SCREEN_RECORD:
					nextScreen = criaRecorde();
					break;
				case SCREEN_CREDITOS:
					nextScreen = criaCreditos();
					break;
				case SCREEN_HELP:
					nextScreen = criaHelp();
					break;
				case SCREEN_SPLASH_NANO: //Todo: getText()
					nextScreen = new TelaSplashNano( PATH_SPLASH, "<ALN_H>DESENVOLVIDO POR\n�2007 NANO GAMES.\nTODOS OS DIREITOS RESERVADOS.\n", SCREEN_SPLASH_MARCA );
					break;
				case SCREEN_SPLASH_MARCA: //Todo: getText()
					nextScreen = new TelaSplashNick( PATH_SPLASH, "<ALN_H>DESENVOLVIDO POR\n�2007 NANO GAMES.\nTODOS OS DIREITOS RESERVADOS.\n", SCREEN_MAIN_MENU );
					break;
				case SCREEN_SPLASH_GAME:
					nextScreen = new TelaDeSplash();
					break;
				case SCREEN_YN_BACKGAME: 
					nextScreen = new YesNoScreen( instancia, SCREEN_YN_BACKGAME , imgFont, TEXT_BACK );
					break;
				case SCREEN_GAMEBACK:
					nextScreen = saveScreen;
					break;
				case SCREEN_FIMDEJOGO:
					saveScreen = null;
					//nextScreen = createEndGameMenu();
					nextScreen = new TelaDeFimDeJogo();
					break;
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
				System.out.println( "deu merda!" );
			//#endif
		}
		
		if ( nextScreen != null )
			meuScreenManager.setCurrentScreen( nextScreen, ScreenManager.TRANSITION_TYPE_IMMEDIATE );
	}
	
	
	public static Drawable criaJogo() throws Exception{
		TelaDeJogo jogo = new TelaDeJogo();
		
		jogo.setSize( ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT );
		insertSoftKey( ScreenManager.SOFT_KEY_LEFT, TEXT_BACK );
		//#if DEBUG == "true"
			System.out.println("criaJogo()");
		//#endif
		
		return jogo;
	} // fim do m�todo criaJogo
	
	
	public static Drawable criaHelp() throws Exception{
		TelaDeTexto tela = new TelaDeTexto( imgFont, TEXT_HELP_TEXT );
		tela.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);		
		return tela;
	}
	
	
	public static Drawable criaRecorde() throws Exception{
		TelaDeRecorde tela = new TelaDeRecorde();
		tela.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);
		return tela;
	}
	
	
	public static Drawable criaCreditos() throws Exception{
		TelaDeTexto tela = new TelaDeTexto( imgFont, TEXT_CREDITS_TEXT );
		tela.setSize(ScreenManager.SCREEN_WIDTH,ScreenManager.SCREEN_HEIGHT);		
		return tela;
	}
	
	
	public void itemChanged(int id, int index) {
		
	}
	
	
	public static final void insertSoftKey(int softKeyType, int textIndex ) throws Exception{
		Label label = new Label( imgFont, getText(textIndex) );
		meuScreenManager.setSoftKey( softKeyType, label );
	}
	
	/**
	 * getText get�s the text from a string array
		*
	 */
	public static final String getText( int index ) {
		return texts[ index ];
	} // fim do m�todo getText
	
	
	public static final void setLanguage( int language ) {
		switch ( language ) {
			case LANGUAGE_ENGLISH:
			case LANGUAGE_PORTUGUESE:
			case LANGUAGE_SPANISH:
				( ( MeuMidlet ) instance ).language = ( byte ) language;
				loadTexts();
				break;
		}
	} // fim do m�todo setLanguage( int )

	
    public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
        try {
            if ( textIndex < 0 )
                ( ( MeuMidlet ) instance ).manager.setSoftKey( softKey, null );
            else
                ( ( MeuMidlet ) instance ).manager.setSoftKey( softKey, new Label( imgFont, getText( textIndex ) ) );
        } catch ( Exception e ) {
            //#if DEBUG == "true"
            e.printStackTrace();
            //#endif
        }
    } // fim do m�todo setSoftKeyLabel( byte, int )	
	
	private static final void loadTexts() {
		texts = null;
		switch ( language ) {
			case LANGUAGE_PORTUGUESE:
				texts = new String[] {
					"OK",
					"VOLTAR",
					"NOVO JOGO",
					"SAIR",
					"OP��ES",
					"PAUSA",
					"CR�DITOS",
					"<ALN_H>NANO GAMES LTDA.\n\n2007 VIACOM INTERNATIONAL INC.\n\nTODOS LOS DERECHOS RESERVADOS.\n\nWWW.NANOGAMES.COM.BR",
					"AJUDA",
					"CONTROLES: 1, 2 OU 3: PEGA O INGREDIENTE DA PARTE SUPERIOR DA ESTEIRA\n4, 5 OU 6: PEGA O INGREDIENTE DO MEIO DA ESTEIRA\n7, 8 OU 9: PEGA O INGREDIENTE DA PARTE INFERIOR DA ESTEIRA.",
					"RECORDE",
					"�TIMO",
					"BOM",
					"RUIM",
					"PONTOS",
					"N�VEL",
					"LIGAR SOM",
					"DESLIGAR SOM",
					"LIGAR VIBRA��O",
					"DESLIGAR VIBRA��O",
					"CANCELAR",
					"SATISFA��O",
					"CONTINUAR",
					"ENGLISH",
					"PORTUGU�S",
					"ESPA�OL",
					"ATIVAR SONS?",
					"SIM",
					"N�O",
					"MENU1",
					"MENU2",
					"GAME OVER",
					"TENTAR NOVAMENTE",
					"NOVO RECORDE!!!",
					"MENU6",
				};
				break;
				
			case LANGUAGE_SPANISH:
				texts = new String[] {
					"OK",
					"VOLTAR",
					"NOVO JOGO",
					"SAIR",
					"OP��ES",
					"PAUSA",
					"CR�DITOS",
					"<ALN_H>NANO GAMES LTDA.\n\n2007 VIACOM INTERNATIONAL INC.\n\nTODOS LOS DERECHOS RESERVADOS.\n\nWWW.NANOGAMES.COM.BR",
					"AJUDA",
					"CONTROLES: 1, 2 OU 3: PEGA O INGREDIENTE DA PARTE SUPERIOR DA ESTEIRA\n4, 5 OU 6: PEGA O INGREDIENTE DO MEIO DA ESTEIRA\n7, 8 OU 9: PEGA O INGREDIENTE DA PARTE INFERIOR DA ESTEIRA.",
					"RECORDE",
					"�TIMO",
					"BOM",
					"RUIM",
					"PONTOS",
					"N�VEL",
					"LIGAR SONIDO",
					"DESLIGAR SONIDO",
					"LIGAR VIBRA��O",
					"DESLIGAR VIBRA��O",
					"CANCELAR",
					"SATISFA��O",
					"CONTINUAR",
					"ENGLISH",
					"PORTUGU�S",
					"ESPA�OL",
					"ACTIVAR SONIDOS?",
					"S�",
					"NO",
					"MENU",
				};
				break;
				
			case LANGUAGE_ENGLISH:
			default:
				texts = new String[] {
					"OK",
					"BACK",
					"NEW GAME",
					"EXIT",
					"OPTIONS",
					"PAUSE",
					"CREDITS",
					"<ALN_H>NANO GAMES LTDA.\n\n2007 VIACOM INTERNATIONAL INC.\n\nALL RIGHTS RESERVED.\n\nWWW.NANOGAMES.COM.BR",
					"HELP",
					"CONTROLES: 1, 2 OU 3: PEGA O INGREDIENTE DA PARTE SUPERIOR DA ESTEIRA\n4, 5 OU 6: PEGA O INGREDIENTE DO MEIO DA ESTEIRA\n7, 8 OU 9: PEGA O INGREDIENTE DA PARTE INFERIOR DA ESTEIRA.",
					"RECORD",
					"GREAT",
					"GOOD",
					"BAD",
					"SCORE",
					"LEVEL",
					"TURN SOUND ON",
					"TURN SOUND OFF",
					"TURN VIBRATION ON",
					"TURN VIBRATION OFF",
					"CANCEL",
					"SATISFACTION",
					"CONTINUE",
					"ENGLISH",
					"PORTUGU�S",
					"ESPA�OL",
					"ACTIVATE SOUNDS?",
					"YES",
					"NO",
					"MENU",
				};
		} // fim switch ( language )
	} // fim do m�todo loadTexts()
	
}