package jogo;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.FrameSet;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import java.util.Random;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Item;
import telas.TelaDeJogo;
import util.MyRandom;
/*
 * Tabuleiro.java
 *
 * Created on June 18, 2007, 9:19 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public final class Tabuleiro extends UpdatableGroup implements SmackThatConfig{
	private static int itens = 1000; // TODO definir tamanho
	
	private final Smack[] smack		= new Smack[ TABULEIRO ];
	private final MiniSmack[] miniSmack	= new MiniSmack[ MINISMACK_TOT ];
	
	private static int miniSmackCont; 
	public static boolean cleanUp;

	
	private final Sprite cursor;
	private final Sprite boom = null;
	private final Point cursorCell = new Point();
	private final Point cellSize;
	private boolean cursorActive = true;
	
	public int shoots ;
	public int levelCount  ;
	
	public boolean updateLabels;
	
	private TelaDeJogo minhaTelaDeJogo;
	private Level level = new Level(0); // ToDo: Toda a classe...

	
	/** Creates a new instance of Tabuleiro */
	public Tabuleiro(TelaDeJogo tela) throws Exception {
		super(itens);
		minhaTelaDeJogo = tela;
		//Pattern do Tabuleiro
		DrawableImage img = new DrawableImage( PATH_TABULEIRO + "board_pattern.png");
		DrawableImage board_img = new DrawableImage( PATH_TABULEIRO + "board_img.png");
		
		
		cellSize = img.getSize();
//		cellSize = new Point();
//		cellSize.set(25,25);
		
//		board_img.defineReferencePixel( board_img.getSize().x >> 1, board_img.getSize().y >> 1 );
//		board_img.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT);
		
		board_img.setPosition( 0, 0);
		insertDrawable( board_img );
		
		
		Pattern pattern = new Pattern(img);
		pattern.setSize( img.getSize().x * NUMERO_CELL, img.getSize().y * NUMERO_CELL);
		setSize( pattern.getSize() );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		//insertDrawable( pattern );
		//Fim Pattern do Tabuleiro
		
		final FrameSet smackFrameSet = Smack.loadFrameSet();
		// Init smack Array
		for(int i = 0 ; i < TABULEIRO; ++i) {
			smack[i] = new Smack(smackFrameSet, this);
			final Smack s = smack[i]; 
			s.defineReferencePixel( s.getSize().x >> 1, s.getSize().y >> 1 );
			insertDrawable( s );
		}	
		fillCells(smack);
		
		final FrameSet minismackFrameSet = MiniSmack.loadFrameSet();
		
		for (int i=0; i < MINISMACK_TOT; ++i){
			miniSmack[i] = new MiniSmack(minismackFrameSet, this);
			miniSmack[i].setActive(false);
			insertDrawable(miniSmack[i]);
		}
		
			
		final Image[] cursorImages = new Image[ CURSOR_TOTAL_IMAGES ];
		for ( int i = 0; i < cursorImages.length; ++i )
			cursorImages[ i ] = Image.createImage( PATH_TABULEIRO + "cursor_" + i + ".png" );
		
		final FrameSet cursorFrameSet = new FrameSet( CURSOR_TOTAL_SEQS ); 
		
		//NORMAL_CURSOR
		cursorFrameSet.addSequence( new Image[] {	cursorImages[0], 
													cursorImages[1],
													cursorImages[2], 
													cursorImages[3],
													cursorImages[4],
													cursorImages[5],
													cursorImages[4],
													cursorImages[3],
													cursorImages[2],
													cursorImages[1]}, CURSOR_NORMAL_ANIM_TIME );
		//TERMINAL_CURSOR
		cursorFrameSet.addSequence( new Image[] {	cursorImages[6], 
													cursorImages[7],
													cursorImages[8], 
													cursorImages[9],
													cursorImages[8],
													cursorImages[7]}, CURSOR_TERMINAL_ANIM_TIME );
		cursor = new Sprite( cursorFrameSet );
		//cursor.setSequence(NORMAL_CURSOR);
		cursor.defineReferencePixel( cursor.getSize().x >> 1, cursor.getSize().y >> 1 );
		
		insertDrawable ( cursor );

		try {
			MeuMidlet.loadData(DATABASE_NAME, DATABASE_IDX_RECORD,MeuMidlet.highScoreManage);
		} catch (Exception ex) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}

		startLevelOne();
	}
	
	
	private void startLevelOne() {
		levelCount = 1;
		shoots = 10;
		MeuMidlet.score = 0;
		fillSmacks(70);
		minhaTelaDeJogo.resetScoreShown();
	}
	
	
	private void nextLevel(){
		++levelCount;
		updateLabels = true;
		shoots += 1; //Todo: 
		cursorTestSeq();
		fillSmacks(FILLSMACKCONT - ( levelCount ));
	}
	
	
	public final void moveCursor( byte direction ) {
		if (!cursorActive)
			return;
		
		switch ( direction ) {
			case DIR_RIGHT:
				++cursorCell.x;
				
				if ( cursorCell.x >= NUMERO_CELL )
					cursorCell.x = 0;
			break;
			
			case DIR_LEFT:
				--cursorCell.x;
				
				if ( cursorCell.x < 0 )
					cursorCell.x = NUMERO_CELL - 1;				
			break;
			
			case DIR_UP:
				--cursorCell.y;

				if ( cursorCell.y < 0 )
					cursorCell.y = NUMERO_CELL - 1;								
			break;
			
			case DIR_DOWN:
				++cursorCell.y;
				
				if ( cursorCell.y >= NUMERO_CELL )
					cursorCell.y = 0;
			break;
			
			default:
				return;
		}
		setDrawableAtCell( cursor, cursorCell );
	}
	
	
	private final void setDrawableAtCell( Drawable d, Point cell ) {
		d.setRefPixelPosition( ( cellSize.x >> 1 ) + cell.x * cellSize.x, ( cellSize.y >> 1 ) + cell.y * cellSize.y );
	}
	
	
	private final void fillCells( Smack[] smacks) {
		final Point cell = new Point();

		for (int i = 0; i < smacks.length ;i ++){
			cell.x = i % NUMERO_CELL;
			cell.y = i / NUMERO_CELL;
			
			setDrawableAtCell( smacks[i], cell );
		}
	}
	
	
	public final void fillSmacks( int times ) {//Todo: param B1, B2, B3, B4
		final MyRandom rand = new MyRandom();
		final Point cell = new Point();
		
		for ( int i = 0; i < smack.length; ++i )
			smack[i].setState( 0 );
		
//		for ( int i = 0; i < times; ) {
//			cell.x = rand.nextInt( NUMERO_CELL );
//			cell.y = rand.nextInt( NUMERO_CELL );
//			
//			final Smack s = smack[ cell.y * NUMERO_CELL + cell.x ];
//
//			int insertSize = 1 + rand.nextInt( Smack.TOTAL_ESTADOS - 2 );
//			if ( insertSize + s.getState() >= Smack.TOTAL_ESTADOS )
//				insertSize = Smack.TOTAL_ESTADOS - s.getState() - 1;
//			
//			if ( i + insertSize >= times )
//				insertSize = times - i;
//			
//			if ( s.getState() < Smack.TOTAL_ESTADOS ) {
//				s.setState( s.getState() + insertSize );
//				
//				i += insertSize;
//			}
//		}
		
		for ( int i = 0; i < times; ){
			cell.x = rand.nextInt( NUMERO_CELL );
			cell.y = rand.nextInt( NUMERO_CELL );
			Smack s = smack[ cell.y * NUMERO_CELL + cell.x ];
			if ( s.getState() == STATE_VAZIO ){
				s.setState( rand.nextInt( STATE_BOOM ) );
				i += s.getState();
			}
		}
		
		//#if DEBUG == "true"
		for ( int i = 0; i < smack.length; ++i)
			System.out.println("smack["+i+"] = " + smack[i].getState() );
		//#endif
	}
	

	private final void cursorActive(boolean bl) {
		if (bl){
			cursor.setVisible(true);
		}else{
			cursor.setVisible(false);
		}
		cursorActive = bl;
		cursorTestSeq();
	}

	
	private final void cursorTestSeq(){
		//Trocando a sequencia do cursor.
		if ( shoots <= 3 ) {
			if ( cursor.getCurrentSequence() != TERMINAL_CURSOR )
				cursor.setSequence( TERMINAL_CURSOR );
		} else {
			if ( cursor.getCurrentSequence() != NORMAL_CURSOR )
				cursor.setSequence( NORMAL_CURSOR );			
		} 
		
	}
	

	public final void fire(){
		if(!cursorActive)
			return;
		
		if ( shoots > 0 ) {
			--shoots;
			cursorTestSeq();

			updateLabels = true;
			int pos = cursorCell.x + NUMERO_CELL * cursorCell.y;
			fire( pos, 0 , false);
		} else {
			testEndLevel();
		}
	}
	
	
	public final void fire(int pos, int carry, boolean bl){
		
		if( !smack[pos].increaseState(bl)){
			boom(pos, carry);
		}else{
			if(miniSmackCont == 0)
				testEndLevel();
		}
	
	}
	
	
	
	private void boom(int pos, int carry) {
		Point pt = new Point();
		int x = pos % NUMERO_CELL;
		int y = pos / NUMERO_CELL;
		smack[pos].setState( STATE_BOOM );
		if (carry > level.levelCarry){
			++shoots;
			carry = -1;
		}
		
		cursorActive(false);
		
		
		
		pt.set(( cellSize.x >> 1 ) + x * cellSize.x, ( cellSize.y >> 1 ) + y * cellSize.y );
		miniSmack[ miniSmackCont ].go( miniSmackCont++, pt, x, y, DIR_UP, ++carry  );
		miniSmack[ miniSmackCont ].go( miniSmackCont++, pt, x, y, DIR_RIGHT, carry );
		miniSmack[ miniSmackCont ].go( miniSmackCont++, pt, x, y, DIR_LEFT, carry  );
		miniSmack[ miniSmackCont ].go( miniSmackCont++, pt, x, y, DIR_DOWN, carry  );
		
		MeuMidlet.score += miniSmackCont;
		//#if DEBUG == "true"
		System.out.println( "BOOM !!! - 4 novos miniSmack�s total ativo = " + miniSmackCont );
		System.out.println( " x: " + x + "  y: " + y);
		//#endif
	}


	boolean testCollision(int indice) {
		MiniSmack ms = miniSmack[indice];
		int next = ms.nextToCollision();
		//System.out.println("next: " + next);
		int posNextCell = -1;
		if( next > (NUMERO_CELL * NUMERO_CELL)-1 || next < 0)
			return false;
		
		switch(ms.getDir()){
			case DIR_UP:
				//System.out.println("Posicao do smack["+next+"]: " + (smack[next].getPosition().y + cellSize.y) );
				//System.out.println("Posicao do miniSmack:" + ms.getPosition().y);
				posNextCell = smack[next].getPosition().y + cellSize.y;
				if( posNextCell <= ms.getPosition().y )
					return false;
				break;
			case DIR_DOWN:
				posNextCell = smack[next].getPosition().y;
				if( posNextCell >= ms.getRefPixelY())
					return false;

				break;
			
			case DIR_LEFT:
				posNextCell = smack[next].getPosition().x + cellSize.x;
				if( posNextCell <= ms.getRefPixelX())
					return false;

				break;
			
			case DIR_RIGHT:
				posNextCell = smack[next].getPosition().x;
				if( posNextCell >= ms.getRefPixelX())
					return false;

				break;
		}
		

		if ( smack[next].getState() == STATE_VAZIO || smack[next].getState() == STATE_BOOM ) {
			ms.setNextCollision();
			return false;
		}
		else {
			fire(next, ms.carry, true);
			return true;
		}
	
	}
	
	private void setMiniSmackListCleanUp() {
		//#if DEBUG == "true"
		System.out.print( "\nANTES: " );
		for ( int i = 0; i < miniSmackCont; ++i ) {
			System.out.print( miniSmack[i].isActive() ? "+ " : "- " );
		}
		//#endif
		
		for ( int i = 0; i < miniSmackCont; ++i ) {
			if ( !miniSmack[ i ].isActive() ) {
				for ( ; miniSmackCont > i; --miniSmackCont ) {
					if ( miniSmack[ miniSmackCont ].isActive() ) {
						doMiniSmaskListSwitch( i, miniSmackCont );
						break;
					}
				}
			}
		}
		
		//#if DEBUG == "true"
		System.out.print( "\nDEPOIS: " );
		for ( int i = 0; i < miniSmackCont; ++i ) {
			System.out.print( miniSmack[i].isActive() ? "+ " : "- " );
		}		
		System.out.println();
		//#endif
		if( miniSmackCont <= 0){
			cursorActive(true);
			testEndLevel();
			updateLabels = true;
		}
		
		cleanUp = false;
	}
	

	private void doMiniSmaskListSwitch(int index1, int index2){
		MiniSmack msTemp;
		msTemp = miniSmack[index2];
		miniSmack[index2] = miniSmack[index1];
		miniSmack[index2].setIndex(index2);
		miniSmack[index1] = msTemp;
		miniSmack[index1].setIndex(index1);
	}
	
	
	private void testEndLevel(){
		int count = 0;
		for (int i=0; i < smack.length; ++i){
			if ( smack[i].getState() != STATE_BOOM )
			count += smack[i].getState();
		}
		//#if DEBUG == "true"
		System.out.println("cont: " + count);
		System.out.println("shoot: " + shoots);
		//#endif
		if (shoots <= 0 && count > 0 )// Fim de jogo
			gameOver();

		if(count <= 0) // Prox fase
			nextLevel();
		//#if DEBUG == "true"
		System.out.println("testEndLevel");
		//#endif
	}
	
	
	private void gameOver(){
		int aux;
		int curr = MeuMidlet.score;
		MeuMidlet.scoreEntry = TOTAL_RECORDS;
		if( MeuMidlet.score > MeuMidlet.topRecords[TOTAL_RECORDS - 1] ){
			for( byte i = 0 ; i < TOTAL_RECORDS ; ++i){
				if( curr > MeuMidlet.topRecords[i]){
					aux = MeuMidlet.topRecords[i];
					MeuMidlet.topRecords[i] = curr;
					curr = aux;
					if ( MeuMidlet.scoreEntry > i )
						MeuMidlet.scoreEntry = i;
				}
			}
			
			try {
				MeuMidlet.saveData(DATABASE_NAME,DATABASE_IDX_RECORD,MeuMidlet.highScoreManage);
			} catch (Exception ex) {
				//#if DEBUG == "true"
				ex.printStackTrace();
				//#endif
			}
			MeuMidlet.setScreen( SCREEN_FIMDEJOGO );	
		}
		else{
			MeuMidlet.setScreen( SCREEN_FIMDEJOGO );
		}
		
	}
	
	public final void menosShoots(){
		--shoots;
	}

	
	public final void update(int delta) {
		super.update(delta);
		if(cleanUp)
			setMiniSmackListCleanUp();
	}
}
