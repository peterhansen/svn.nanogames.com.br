package jogo;
import br.com.nanogames.components.Drawable;
import javax.microedition.lcdui.Graphics;
/*
 * DrawableCircle.java
 *
 * Created on June 15, 2007, 4:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class DrawableCircle extends Drawable {
	public int color_in;
	public int color_out;
	
	/** Creates a new instance of DrawableCircle */
	public DrawableCircle() {
	}

	
	protected void paint(Graphics g) {
		g.setColor(color_in);
		g.fillArc( translate.x, translate.y, size.x - 1, size.y - 1, 0, 360 );
		
		if ( color_out != color_in ) {
			g.setColor(color_out);
			g.drawArc( translate.x, translate.y, size.x - 1, size.y - 1, 0, 360 );
		}
	}
	
}
