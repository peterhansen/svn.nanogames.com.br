package jogo;
import br.com.nanogames.components.FrameSet;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
/*
 * MiniSmack.java
 *
 * Created on June 20, 2007, 6:38 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class MiniSmack extends Sprite implements SmackThatConfig{
	
	private boolean active;
	private int acumulaTempo;
	private int indice;
	private int direction;
	private final Point v0  = new Point();
	private final Point ds  = new Point();
	private final Point dx  = new Point();
	private final Point dxa = new Point();
	private final int[] collisionList = new int[ NUMERO_CELL - 1 ];
	private final Tabuleiro meuTabuleiro;
	public int carry;
	
	/** Creates a new instance of MiniSmack */
	public MiniSmack(FrameSet frame, Tabuleiro tab) {
		super(frame);
		meuTabuleiro = tab;
	}
	
	
	public void setActive(boolean bl){
		if (!bl){
			Tabuleiro.cleanUp = true;
			carry = 0;
			setVisible(false);
		}else{
			setVisible(true);
		}
		active = bl;
	}
	
	
	public boolean isActive(){
		return active;
	}
	
	
	public void setIndex(int idx){
		indice = idx;
	}
	
	
	public void go(int idx, Point pt ,int x, int y, int dir, int carry){
		setActive(true);
		this.carry = carry;
		indice = idx;
		direction = dir;
		makeCollisionList(x, y);
		
		switch (dir) {
			case DIR_RIGHT:
				//setTransform(TRANS_ROT90);
				v0.set( MINISMACK_VEL, 0 );
				break;
			case DIR_LEFT:
				//setTransform(TRANS_ROT270);
				v0.set( -MINISMACK_VEL, 0 );
				break;
			case DIR_UP:
				//setTransform( TRANS_NONE );
				v0.set( 0, -MINISMACK_VEL );
				break;
			case DIR_DOWN:
				//setTransform(TRANS_ROT180);
				v0.set( 0, MINISMACK_VEL );
				break;
			default:
		}
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( pt );
	}
	
	

	static FrameSet loadFrameSet() throws Exception {
		final FrameSet frameSet = new FrameSet( 1 ); //Todo: 1 ???
		Image[] imagens = new Image[ MINISMACK_IMAGENS_TOTAL];
		for ( int i = 0; i < imagens.length; ++i )
			imagens[ i ] = Image.createImage(PATH_SMACK + "minismack_" + i + ".png");
		
		frameSet.addSequence( new Image[] { imagens[ 0 ],
											imagens[ 1 ], 
											imagens[ 2 ], 
											imagens[ 3 ], 
											imagens[ 4 ], 
											}, 328 );
		
		return frameSet;
	}
	
	
	

	private void makeCollisionList(int x, int y) {
		switch(direction){
			case DIR_RIGHT:
				//Preenche a lista de colisao
				for(int i = 1; i < NUMERO_CELL - x; ++i){
					collisionList[i-1] = NUMERO_CELL * y + x + i;
				}
				if( x > 0) // Cria uma flag para fim da lista
					collisionList[ (NUMERO_CELL-1) - x ] = -1;
				
				break;
			case DIR_LEFT:
				for(int i = 1; i <=  x ; ++i){
					collisionList[i-1] = NUMERO_CELL * y + x - i;
				}
				if( x < NUMERO_CELL - 1)
					collisionList[ x ] = -1;
				
				break;
			case DIR_UP:
				for(int i = 1; i <= y ; ++i){
					collisionList[i-1] = NUMERO_CELL * (y - i) + x ;
				}
				if( y < NUMERO_CELL - 1 )
					collisionList[ y ] = -1;
				
				break;
			case DIR_DOWN:
				for(int i = 1; i < NUMERO_CELL - y; ++i){
					collisionList[i-1] = NUMERO_CELL * ( y + i ) + x ;
				}
				if( y > 0)
					collisionList[ ( NUMERO_CELL-1 ) - y ] = -1;

				break;
			default:
		
		}
	}

	
	public final int getDir() {
		return direction;
	}
	

	public final int nextToCollision() {
		return collisionList[0];
	}

	
	final void setNextCollision() {
		for (int i = 0; i < collisionList.length - 1; ++i){
			collisionList[i] = collisionList[i+1];
		}
		collisionList[ collisionList.length - 1 ] = -1;
	}


	

	public void update(int delta) {
		if (!active)
		    return;
		super.update(delta);
		acumulaTempo += delta;
		//System.out.println("Ativo:" + active +"  AcumulaTempo:"+ acumulaTempo);
		
//		if (acumulaTempo > 3000){
//			acumulaTempo=0;
//			setActive(false);
//		}
		

		//super.update(delta);
		ds.set( dxa.x + ( v0.x * delta ), dxa.y + (v0.y * delta));

		dx.x  = ds.x / 1000;
		dxa.x = ds.x % 1000;
		dx.y  = ds.y / 1000;
		dxa.y = ds.y % 1000;

		//preencher.add(dx);
		this.setPosition( this.getPosition().x + dx.x,
				  this.getPosition().y + dx.y );
		
		//Teste de Colisao
		if (meuTabuleiro.testCollision(indice)){
					//Colidiu
					this.setActive(false);
			//setNextCollision();
		}
		
		switch ( direction )  {
			case DIR_UP:
				if ( position.y > 0 )
					return;
				break;
				
			case DIR_DOWN:
				if ( getRefPixelY() < meuTabuleiro.getSize().y )
					return;
				break;
				
			case DIR_LEFT:
				if ( position.x > 0 )
					return;
				break;
				
			case DIR_RIGHT:
				if ( getRefPixelX() < meuTabuleiro.getSize().x )
					return;
				break;
			default:
				return;
		}
		//Todo: Animacao do minismack batendo!!!
		setActive(false);
		setVisible(false);
		
                
//		System.out.println("Delta:"+delta+" TempoAc:"+ acumulaTempo +" POS x: "+ this.getPosition().x + "  POS y: "+this.getPosition().y);
	}

//	protected void paint(Graphics g) {
//		switch(carry){
//			case 1:
//				g.setColor(0xffffff);
//				break;
//			case 2:
//				g.setColor(0x00ff00);
//				break;
//			case 3:
//				g.setColor(0xffff00);
//				break;
//			case 4:
//				g.setColor(0xff0000);
//				break;
//			default:
//				g.setColor(0x000000);
//		}
//		g.drawRect(0,0,size.x ,size.y );
//		super.paint(g);
//	}

	//Fim da animacao...
	protected void sequenceEnded(int currentSequence) {
		
	}
}