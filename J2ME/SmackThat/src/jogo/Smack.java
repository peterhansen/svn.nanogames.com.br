package jogo;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.FrameSet;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
/*
 * Smack.java
 *
 * Created on June 15, 2007, 5:14 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public class Smack extends Sprite implements SmackThatConfig{
	protected int state;
	private final Tabuleiro meuTabuleiro;
	
	/** Creates a new instance of Smack */
	public Smack(FrameSet set, Tabuleiro tab) {
		super(set);
		meuTabuleiro = tab;
	}
	
	
	public int getState(){
		return state;
	}
	
	
	public void setState(int state) {
		switch(state){
			case STATE_VAZIO:
				this.setVisible(false);
				break;
				
			case STATE_B1:
			case STATE_B2:
			case STATE_B3:
			case STATE_B4: 
			case STATE_BOOM:
				this.setVisible( true );
				this.setSequence( state );
				break;
				
			default:
				return;
		}
		this.state = state;
	}	
	
	
	public static final FrameSet loadFrameSet() throws Exception {
		final FrameSet frameSet = new FrameSet( TOTAL_ESTADOS );
		Image[] imagens = new Image[ IMAGENS_TOTAL ];
		for ( int i = 0; i < IMAGENS_TOTAL; ++i ){
				imagens[ i ] = Image.createImage( PATH_SMACK + "smack_" + i +".png");
				System.out.println( PATH_SMACK + "smack_" + i +".png");
			}
		frameSet.addSequence( new Image[] { imagens[ 0 ] }, 0 );

		//B1_FRAMESET
		frameSet.addSequence( new Image[] { imagens[ 0 ],
											imagens[ 1 ], 
											imagens[ 2 ],  
											imagens[ 3 ], 
											imagens[ 2 ], 
											imagens[ 1 ],  
											}, STATE_B1_TIME );
		//B2_FRAMESET
		frameSet.addSequence( new Image[] { imagens[ 4 ],  
											imagens[ 5 ],
											imagens[ 6 ], 
											imagens[ 7 ],  
											imagens[ 6 ],
											imagens[ 5 ], 
											}, STATE_B2_TIME );
		//B3_FRAMESET
		frameSet.addSequence( new Image[] { imagens[ 8 ], 
											imagens[ 9 ],  
											imagens[ 10 ],
											imagens[ 11 ], 
											imagens[ 12 ],  
											imagens[ 13 ], 
											imagens[ 12 ],  
											imagens[ 11 ],
											imagens[ 10 ], 
											imagens[ 9 ],  
											}, STATE_B3_TIME );
		//B4_FRAMESET
		frameSet.addSequence( new Image[] { imagens[ 14 ],
											imagens[ 15 ],  
											imagens[ 16 ], 
											imagens[ 17 ],  
											imagens[ 18 ], 
											imagens[ 17 ],  
											imagens[ 16 ], 
											imagens[ 15 ],  
											}, STATE_B4_TIME );
		//BOOM_FRAMESET
		frameSet.addSequence( new Image[] { imagens[ 19 ],
											imagens[ 20 ],
											imagens[ 21 ],
											imagens[ 22 ]}, STATE_BOOM_TIME );
		return frameSet;
	}

	
	protected final void sequenceEnded(int currentSequence) {
		if ( currentSequence == STATE_BOOM ){
			setState( STATE_VAZIO );
		}
	}

	
	public final void setSequence(int sequence) {
		final Point previousRefPixelPosition = getRefPixelPosition();
		
		super.setSequence( sequence );
	
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( previousRefPixelPosition );
	}
	
	
	public final void setFrame( int frame ) {
		// armazena o ponto de refer�ncia anterior, e depois o redefine, de forma que uma poss�vel mudan�a nas dimens�es
		// reais do frame n�o alterem a posi��o do ingrediente
		final Point previousRefPoint = new Point( getRefPixelPosition() );

		super.setFrame( frame );

		final Image currentImage = frameSet.getFrame( currentSequence, currentFrame );
		setSize( currentImage.getWidth(), currentImage.getHeight() );
		defineReferencePixel( size.x >> 1, size.y >> 1 );
		setRefPixelPosition( previousRefPoint );
	}

	
	public final boolean increaseState(boolean bl) {
		if( state == STATE_B4 ){
			setState( STATE_VAZIO );
			MeuMidlet.score += STATE_B4_POINTS;
			return false;
		}
		else
		{
			if (bl)
				switch( state ){
					case STATE_B1:
						MeuMidlet.score += STATE_B1_POINTS;
						break;

					case STATE_B2:
						MeuMidlet.score += STATE_B2_POINTS;
						break;

					case STATE_B3:
						MeuMidlet.score += STATE_B3_POINTS;
						break;
				}
				
			setState( state + 1 );
			return true;
		}
	}

	
	public void update(int delta) {
		if ( state != STATE_VAZIO )
			super.update(delta);
	}
	
	
}
