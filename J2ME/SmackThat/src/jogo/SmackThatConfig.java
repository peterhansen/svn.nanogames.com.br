package jogo;
/*
 * SmackThatConfig.java
 *
 * Created on June 15, 2007, 3:59 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author malvezzi
 */
public interface SmackThatConfig {
	//DataBase
	
	public static final String DATABASE_NAME = "smackthat";
	public static final int DATABASE_IDX_OPTIONS = 1;
	public static final int DATABASE_IDX_RECORD = 2;
	public static final int DATABASE_IDX_TOTAL = 2;
	
	public static final byte TOTAL_RECORDS = 5;
	
	//LANGUAGE
	public static final byte LANGUAGE_ENGLISH		= 0;
	public static final byte LANGUAGE_PORTUGUESE	= 1;
	public static final byte LANGUAGE_SPANISH		= 2;
	public static final byte LANGUAGE_TOTAL			= 3;
	
	//SCREENS
	public static final byte SCREEN_SPLASH_NANO		= 0;
	public static final byte SCREEN_SPLASH_MARCA	= 1;
	public static final byte SCREEN_SPLASH_GAME		= 2;
	public static final byte SCREEN_MAIN_MENU	= 3;
	public static final byte SCREEN_JOGO		= 4;
	public static final byte SCREEN_FIMDEJOGO	= 5;
	public static final byte SCREEN_NEWRECORD	= 6;
	public static final byte SCREEN_RECORD		= 7;
	public static final byte SCREEN_HELP		= 8;
	public static final byte SCREEN_CREDITOS	= 9;
	public static final byte SCREEN_CONTINUE	= 10;
	public static final byte SCREEN_YN_BACKGAME	= 11;
	public static final byte SCREEN_OPTIONS		= 12;
	public static final byte SCREEN_GAMEBACK	= 13;

	// MENU
	public static final byte OPT_NEWGAME	= 0;
	public static final byte OPT_OPTION		= 1;
	public static final byte OPT_RECORD		= 2;
	public static final byte OPT_HELP		= 3;
	public static final byte OPT_CREDITS	= 4;
	public static final byte OPT_EXIT		= 5;


	//RES PATH
	
	public static final String PATH_IMG			= "/imagens/";
	public static final String PATH_TABULEIRO	= PATH_IMG + "tabuleiro/";
	public static final String PATH_SMACK		= PATH_TABULEIRO + "smack/";
	public static final String PATH_TELADEJOGO	= PATH_IMG + "teladejogo/";
	public static final String PATH_SOUND		= "/sound/";
	public static final String PATH_SPLASH 		= PATH_IMG + "splash/";
	public static final String PATH_GAMESPLASH	= PATH_IMG + "gameSplash/";
	
	//Tabuleiro
	public static final int NUMERO_CELL		= 6;
	public static final int TABULEIRO		= NUMERO_CELL * NUMERO_CELL;
	public static final int FASE_INICIAL	= 65;
	
	//Cursor
	public static final byte CURSOR_TERMINAL_IMAGES = 4;
	public static final byte CURSOR_NORMAL_IMAGES = 6;
	public static final byte CURSOR_TOTAL_IMAGES = CURSOR_TERMINAL_IMAGES + CURSOR_NORMAL_IMAGES;
	public static final byte NORMAL_CURSOR		= 0;
	public static final byte TERMINAL_CURSOR	= 1;
	public static final byte CURSOR_TOTAL_SEQS = 3;
	public static final short CURSOR_NORMAL_ANIM_TIME = CURSOR_NORMAL_IMAGES * 213;
	public static final short CURSOR_TERMINAL_ANIM_TIME = CURSOR_TERMINAL_IMAGES * 213;
	
	
	// dire��es de movimento no tabuleiro
	public static final byte DIR_UP		= 0;
	public static final byte DIR_DOWN	= 1;
	public static final byte DIR_RIGHT	= 2;
	public static final byte DIR_LEFT	= 3;
	
	//Smack

	public static final byte IMAGENS_TOTAL = 23;	
	public static final byte TOTAL_ESTADOS = 6;		
	public static final int STATE_VAZIO = 0;
	public static final int STATE_B1 	= 1;
	public static final int STATE_B2 	= 2;
	public static final int STATE_B3 	= 3;
	public static final int STATE_B4 	= 4;
	public static final int STATE_BOOM 	= 5;
	public static final int STATE_B1_TIME 	= 1111;
	public static final int STATE_B2_TIME 	= 1111;
	public static final int STATE_B3_TIME 	= 1333;
	public static final int STATE_B4_TIME 	= 1222;
	public static final int STATE_BOOM_TIME	= 1632;
	public static final int FILLSMACKCONT = 70;
	public static final int B1_FRAMESET = 4;
	public static final int B2_FRAMESET = 4;
	public static final int B3_FRAMESET = 6;
	public static final int B4_FRAMESET = 5;
	
	//Pontuacao
	public static final int STATE_B1_POINTS = 42;
	public static final int STATE_B2_POINTS = 23;
	public static final int STATE_B3_POINTS = 11;
	public static final int STATE_B4_POINTS = 12;
	public static final int SCORE_INCREMENT = 487;
	
	//MiniSmack
	//velocidade em px/s
	public static final int MINISMACK_VEL = 168;
	public static final int MINISMACK_TOT = TABULEIRO * 3;
	public static final int MINISMACK_IMAGENS_TOTAL	= 5;
	
	//Telas
	//BackGround
	public static final int NUMERO_SMALLSTAR	= 19;
	public static final int NUMERO_BIGSTAR		= 3;
	public static final int DELAYSMALL			= 3;
	public static final int DELAYBIG			= 17;
	public static final int NEBULASPEED			= 1;
	
	//GameSplash
	public static final int GAME_SPLASH_TIMECONT = 3000;

	//Tela de Jogo
	public static final int SPACER_LABEL_X		= 5;
	public static final int SPACER_LABEL_Y		= 1;

	//Fim de Jogo
	public static final int  DELAY_KEY_BOARD		= 1438;
	public static final byte OPT_ENDGAME_RETRY		= 0;
	public static final byte OPT_ENDGAME_RECORD		= 1;
	public static final byte OPT_ENDGAME_BACK		= 2;
	
	//Record
	public static final int SCOREBLINKDELAY		= 512;
	
	//Vibration
	public static final int DEFAULT_VIBRATION_TIME = 50;
	public static final int MENU_ITEMS_SPACING = 10;
	
	
	// T e x t
	// defini��es dos textos
	public static final byte TEXT_OK					= 0;
	public static final byte TEXT_BACK					= 1;
	public static final byte TEXT_NEW_GAME				= 2;
	public static final byte TEXT_EXIT					= 3;
	public static final byte TEXT_OPTIONS				= 4;
	public static final byte TEXT_PAUSE					= 5;
	public static final byte TEXT_CREDITS				= 6;
	public static final byte TEXT_CREDITS_TEXT			= 7;
	public static final byte TEXT_HELP					= 8;
	public static final byte TEXT_HELP_TEXT				= 9;
	public static final byte TEXT_RECORD				= 10;
	public static final byte TEXT_ANYKEY				= 11;
	public static final byte TEXT_GOOD					= 12;
	public static final byte TEXT_BAD					= 13;
	public static final byte TEXT_SCORE					= 14;
	public static final byte TEXT_LEVEL					= 15;
	public static final byte TEXT_TURN_SOUND_ON			= 16;
	public static final byte TEXT_TURN_SOUND_OFF		= 17;
	public static final byte TEXT_TURN_VIBRATION_ON		= 18;
	public static final byte TEXT_TURN_VIBRATION_OFF	= 19;
	public static final byte TEXT_CANCEL				= 20;
	public static final byte TEXT_SATISFACTION			= 21;
	public static final byte TEXT_CONTINUE				= 22;
	public static final byte TEXT_ENGLISH				= 23;
	public static final byte TEXT_PORTUGUESE			= 24;
	public static final byte TEXT_SPANISH				= 25;
	public static final byte TEXT_DO_YOU_WANT_SOUND		= 26;
	public static final byte TEXT_YES					= 27;
	public static final byte TEXT_NO					= 28;
	public static final byte TEXT_MAIN_MENU				= 29;
	public static final byte TEXT_LANGUAGE				= 30;
	public static final byte TEXT_GAMEOVER				= 31;
	public static final byte TEXT_RESTART				= 32;
	public static final byte TEXT_NEWRECORD				= 33;
	
	public static final byte TOTAL_TEXTS				= 34;

}
