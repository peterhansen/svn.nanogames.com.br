/**
 * CBVNewsEntry.java
 *
 * Created on Apr 13, 2010 8:32:05 PM
 *
 */

package core;

import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.online.newsfeeder.NewsFeederEntry;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class PenaltyNewsEntry extends Component implements Constants {

	public static final byte BKG_HEIGHT = 30;
	private static final byte TEXT_HEIGHT = 50;
	public static final byte SPACING = 4;

	private static final byte TOTAL_HEIGHT = BKG_HEIGHT + TEXT_HEIGHT + ( SPACING << 1 );

	private final MarqueeLabel title;

	private final Pattern bkg;

	private final RichLabel text;

	private boolean focused;

	private NewsFeederEntry entry;

	private final Pattern cursor;

	private long lastFocus;


	public PenaltyNewsEntry( NewsFeederEntry entry ) throws Exception {
		super( 10 );

		cursor = new Pattern( 0x00aa00 );
		cursor.setSize( Short.MAX_VALUE, TOTAL_HEIGHT );
		insertDrawable( cursor );

		bkg = new Pattern( 0x008700 );
		bkg.setSize( 0, BKG_HEIGHT );
		bkg.setPosition( 0, SPACING );
		insertDrawable( bkg );

		this.entry = entry;

		final String titleString = AppMIDlet.formatTime( entry.getTime(), GameMIDlet.getText( TEXT_TIME_FORMAT ) ) + " - " + entry.getTitle();

		//#if SCREEN_SIZE == "GIANT"
//# 			title = new MarqueeLabel( FONT_INDEX_TITLE, titleString );
		//#else
			title = new MarqueeLabel( FONT_INDEX_TITLE, titleString.toUpperCase() );
		//#endif
		title.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		title.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		title.setPosition( 0, bkg.getPosY() + ( ( bkg.getHeight() - title.getHeight() ) >> 1 ) );
		insertDrawable( title );

		text = new RichLabel( FONT_INDEX_TEXT, entry.getContent(), ScreenManager.SCREEN_WIDTH );
		text.setPosition( 0, bkg.getPosY() + bkg.getHeight() + SPACING );
		insertDrawable( text );

		setFocused( false );

		setPreferredSize( new Point( 10000, TOTAL_HEIGHT ) );

		setSize( size );
	}


	public final Point calcPreferredSize( Point maximumSize ) {
		return new Point( 10000, TOTAL_HEIGHT );
	}


	public final NewsFeederEntry getEntry() {
		return entry;
	}


	public final boolean isFocused() {
		return focused && ( System.currentTimeMillis() - lastFocus >= 50 );
	}


	public final void setSize( int width, int height ) {
		if ( width <= 0 )
			width = 1000;
		
		bkg.setSize( width, bkg.getHeight() );
		text.setSize( width, TEXT_HEIGHT );
		title.setSize( width, title.getHeight() );

		super.setSize( width, TOTAL_HEIGHT );
	}


	public final void setFocused( boolean focused ) {
		lastFocus = System.currentTimeMillis();
		this.focused = focused;
		cursor.setVisible( focused );
		if ( !focused )
			title.setTextOffset( 0 );
	}


	public final void update( int delta ) {
		if ( focused )
			super.update( delta );
	}


	public final String getUIID() {
		return "p";
	}

}
