/**
 * Play.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:08:24 AM.
 */

package core;


/**
 * 
 * @author Peter
 */
public final class Play {

	public int fp_direction;
	public int fp_height;
	public int fp_power;
	public int fp_curve;
	
	
	public Play() {
	}
	
	
	public Play( int direction, int height, int power, int curve ) {
		set( direction, height, power, curve );
	}
	
	
	public final void set( int direction, int height, int power, int curve ) {
		fp_direction = direction;
		fp_height = height;
		fp_power = power;
		fp_curve = curve;
	}

	
	public final void set( Play p ) {
		set( p.fp_direction, p.fp_height, p.fp_power, p.fp_curve                );
	}
	
}
