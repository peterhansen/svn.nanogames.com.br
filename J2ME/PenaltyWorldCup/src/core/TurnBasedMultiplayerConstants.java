/**
 * TurnBasedMultiplayerConstants.java
 *
 * Created on May 26, 2010 12:32:39 AM
 *
 */
package core;

//#if MULTIPLAYER == "true"
import br.com.nanogames.components.online.NanoOnlineConstants;


/**
 *
 * @author peter
 */
public interface TurnBasedMultiplayerConstants {

	/** Id de parâmetro de conexão para partida multiplayer: quantidade mínima de jogadores da partida. Formato dos dados: byte
	 */
	public static final byte PARAM_MATCH_MIN_PLAYERS = -1;

	/** Id de parâmetro de conexão para partida multiplayer: quantidade máxima de jogadores da partida. Formato dos dados: byte */
	public static final byte PARAM_MATCH_MAX_PLAYERS = PARAM_MATCH_MIN_PLAYERS - 1;

	/** Id de parâmetro de conexão para partida multiplayer: id da partida. Formato dos dados: int */
	public static final byte PARAM_MATCH_ID = PARAM_MATCH_MAX_PLAYERS - 1;

	/** Id de parâmetro de conexão para partida multiplayer: id do jogador (customer). Formato dos dados: int */
	public static final byte PARAM_PLAYER_ID = PARAM_MATCH_ID - 1;

	/** Id de parâmetro de conexão para partida multiplayer: apelido do jogador (customer). Formato dos dados: string */
	public static final byte PARAM_PLAYER_NICKNAME = PARAM_PLAYER_ID - 1;

	/** Id de parâmetro de conexão para partida multiplayer: . Formato dos dados: */
	public static final byte PARAM_PLAYER_ORDER = PARAM_PLAYER_NICKNAME - 1;

	/** Id de parâmetro de conexão para partida multiplayer: quantidade de jogadores. Formato dos dados: byte */
	public static final byte PARAM_N_PLAYERS = PARAM_PLAYER_ORDER - 1;

	/** Id de parâmetro de conexão para partida multiplayer: dados da rodada. Formato dos dados: variável (byte[])*/
	public static final byte PARAM_ROUND_DATA = PARAM_N_PLAYERS - 1;

	/** Id de parâmetro de conexão para partida multiplayer: pontuação global do jogador. Formato dos dados: long */
	public static final byte PARAM_PLAYER_GLOBAL_SCORE = PARAM_ROUND_DATA - 1;

	/** Id de parâmetro de conexão para partida multiplayer: posição global do jogador no ranking. Formato dos dados: int */
	public static final byte PARAM_PLAYER_GLOBAL_POSITION = PARAM_PLAYER_GLOBAL_SCORE - 1;

	/** Id de parâmetro de conexão para partida multiplayer: fim das informações de um jogador. Formato dos dados: nenhum */
	public static final byte PARAM_PLAYER_INFO_END = PARAM_PLAYER_GLOBAL_POSITION - 1;

	/** Id de parâmetro de conexão para partida multiplayer: índice do turno. Formato dos dados: short */
	public static final byte PARAM_TURN_INDEX = PARAM_PLAYER_INFO_END - 1;

	/** Id de parâmetro de conexão para partida multiplayer: jogador está saindo do jogo. Formato dos dados: int */
	public static final byte PARAM_PLAYER_QUITTING = PARAM_TURN_INDEX - 1;

	/** Id de parâmetro de conexão para partida multiplayer: resultado final de uma partida. Formato dos dados:
	 * byte indicando a quantidade de jogadores, e para cada jogador:
	 *   - int: id do jogador
	 *   - long: pontuação final
	 *   - long: variação na pontuação global do jogador
	 */
	public static final byte PARAM_MATCH_FINAL_SCORES = PARAM_PLAYER_QUITTING - 1;

	/** Id de parâmetro de conexão para partida multiplayer: jogador não respondeu a tempo. Formato dos dados: int  */
	public static final byte PARAM_PLAYER_TIMEOUT = PARAM_MATCH_FINAL_SCORES - 1;

	/** Id de parâmetro de conexão para partida multiplayer: esperando outros jogadores entrarem no jogo. Formato dos dados: nenhum */
	public static final byte PARAM_WAITING_OPPONENTS = PARAM_PLAYER_TIMEOUT - 1;

	/** Id de parâmetro de conexão para partida multiplayer: esperando outros jogadores enviarem sua jogada. Formato dos dados: nenhum */
	public static final byte PARAM_WAITING_ACTIONS = PARAM_WAITING_OPPONENTS - 1;

	/** Id de parâmetro de conexão para partida multiplayer: jogador já está em uma partida. Formato dos dados: nenhum */
	public static final byte PARAM_ALREADY_IN_MATCH = PARAM_WAITING_ACTIONS - 1;

	/** Id de parâmetro de conexão para partida multiplayer: jogador não encontrado. Formato dos dados: nenhum */
	public static final byte PARAM_PLAYER_NOT_FOUND = PARAM_ALREADY_IN_MATCH - 1;

	/** Id de parâmetro de conexão para partida multiplayer: número do turno errado. Formato dos dados: nenhum */
	public static final byte PARAM_WRONG_TURN_NUMBER = PARAM_PLAYER_NOT_FOUND - 1;

	/** Id de parâmetro de conexão para partida multiplayer: quantidade de jogadas dos oponentes. Formato dos dados: int */
	public static final byte PARAM_N_ACTIONS = PARAM_WRONG_TURN_NUMBER - 1;

	/** Id de parâmetro de conexão para partida multiplayer: customer_id ou match inválidos. Formato dos dados: nenhum */
	public static final byte PARAM_INVALID_PLAYER_OR_MATCH = PARAM_N_ACTIONS - 1;

	// TODO adicionar id de parâmetro para dados específicos de jogador ao entrar numa partida. Exemplo: time escolhido
	/** Id de parâmetro de conexão para partida multiplayer: . Formato dos dados: */
	public static final byte PARAM = PARAM_INVALID_PLAYER_OR_MATCH - 1;
	
}

//#endif