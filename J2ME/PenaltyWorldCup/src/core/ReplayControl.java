/**
 * ReplayControl.java
 *
 * Created on Jun 6, 2010 11:54:37 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class ReplayControl extends Menu implements Constants {

	public static final byte REPLAY_SPEED_NORMAL		= 0;
	public static final byte REPLAY_SPEED_SLOW			= 1;
	public static final byte REPLAY_SPEED_VERY_SLOW		= 2;

	private static final byte REPLAY_SPEED_TOTAL		= 3;

	private byte currentReplaySpeed;

	/** Ícone do replay. */
	private final Drawable replayIcon;

	public static final byte REPLAY_ICON_SEQUENCE_NONE				= 0;
	public static final byte REPLAY_ICON_SEQUENCE_APPEARING			= 1;
	public static final byte REPLAY_ICON_SEQUENCE_HIDING			= 2;
	public static final byte REPLAY_ICON_SEQUENCE_HIDE_AND_APPEAR	= 3;

	private boolean replayPaused;

	private final Pattern replayMiddle;

	private final DrawableImage rewind;
	private final DrawableImage play;
	private final DrawableImage pause;
	private final DrawableImage record;
	private final DrawableImage eject;
	private final DrawableImage replayRight;

	public static final byte INDEX_SPEED		= 0;
	public static final byte INDEX_REWIND		= 1;
	public static final byte INDEX_PLAY_PAUSE	= 2;
	public static final byte INDEX_RECORD		= 3;
	public static final byte INDEX_EJECT		= 4;

	private final Label speedLabel;

//	private final DrawableImage speedArrows;

	private final int[] data;

	private final Image img;

	private boolean buttonPressed = false;


	public ReplayControl( MenuListener listener, boolean showRecord ) throws Exception {
		super( listener, 0, 10 );

		replayMiddle = new Pattern( new DrawableImage( PATH_IMAGES + "rm.png" ) );
		final int controlWidth = replayMiddle.getFill().getWidth();
		final int controlHeight = replayMiddle.getFill().getHeight();
		replayMiddle.setSize( ( showRecord ? 4 : 3 ) * controlWidth, controlHeight );
		insertDrawable( replayMiddle );

		final int DATA_LENGTH = controlWidth * controlHeight;
		img = Image.createImage( controlWidth, controlHeight );
		data = new int[ DATA_LENGTH ];

		replayRight = new DrawableImage( PATH_IMAGES + "rr.png" );
		replayRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( replayRight );

		play = new DrawableImage( PATH_IMAGES + "play.png" );
		play.defineReferencePixel( ANCHOR_CENTER );
		play.setVisible( false );
		insertDrawable( play );

		rewind = new DrawableImage( PATH_IMAGES + "rewind.png" );
		rewind.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( rewind );
		
		pause = new DrawableImage( PATH_IMAGES + "pause.png" );
		pause.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( pause );

		if ( showRecord ) {
			record = new DrawableImage( PATH_IMAGES + "rec.png" );
			record.defineReferencePixel( ANCHOR_CENTER );
			insertDrawable( record );
		} else {
			record = null;
		}

		eject = new DrawableImage( PATH_IMAGES + "eject.png" );
		eject.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( eject );
/*
        speedArrows = new DrawableImage( PATH_IMAGES + "speed.png" );
		speedArrows.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( speedArrows );
*/
		speedLabel = new Label( FONT_INDEX_TITLE, "1X" );
		speedLabel.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( speedLabel );

		replayIcon = GameMIDlet.getButton( "R", 0, null, Border.BORDER_COLOR_BLACK );
		insertDrawable( replayIcon );

		setCurrentIndex( INDEX_EJECT );

		setCircular( true );
		setVisible( false );
	}


	public final void draw( Graphics g ) {
		if ( isVisible() ) {
			super.draw( g );

			final int controlWidth = replayMiddle.getFill().getWidth();
			final int controlHeight = replayMiddle.getFill().getHeight();

			final Graphics g2 = img.getGraphics();
			g2.setColor( 0xff00ff );
			g2.fillRect( 0, 0, controlWidth, controlHeight );
			final Point previousTranslate = new Point( translate );

			final int controlX = currentIndex * controlWidth;

			translate.set( -getPosX() - controlX, -replayMiddle.getPosY() );
			super.draw( g2 );
			translate.set( previousTranslate );
			img.getRGB( data, 0, controlWidth, 0, 0, controlWidth, controlHeight );
            final int magenta = g.getDisplayColor(0x00ff00ff) & 0x00ff00ff;
			for ( int i = 0; i < data.length; ++i ) {
                final int color = g.getDisplayColor(data [i]) ;
				if ( ( color & 0x00ff00ff ) == magenta ) {
					data[ i ] = 0;
				} else {
					data[ i ] = 0xff000000 | Drawable.changeColorLightness( color, buttonPressed ? -85 : 50 );
				}
			}
			g.drawRGB( data, 0, controlWidth, translate.x + getPosX() + controlX, translate.y + getPosY() + replayMiddle.getPosY(), controlWidth, controlHeight, true );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		final int START_Y = height - replayMiddle.getHeight();
		final int OFFSET_X = replayMiddle.getFill().getWidth();
		final int refY = START_Y + ( replayMiddle.getHeight() >> 1 ) ;

		//speedArrows.setRefPixelPosition( OFFSET_X >> 1, refY );
		//speedLabel.setRefPixelPosition( speedArrows.getRefPixelPosition() );
        speedLabel.setRefPixelPosition( OFFSET_X >> 1, refY );

		replayMiddle.setPosition( 0, START_Y );
		replayRight.setPosition( replayMiddle.getWidth(), START_Y );
		play.setRefPixelPosition( OFFSET_X * INDEX_PLAY_PAUSE + ( OFFSET_X >> 1 ), refY );
		pause.setRefPixelPosition( play.getRefPixelPosition() );
		rewind.setRefPixelPosition( OFFSET_X * INDEX_REWIND + ( OFFSET_X >> 1 ), refY );

		if ( record == null ) {
			eject.setRefPixelPosition( OFFSET_X * ( INDEX_EJECT - 1 ) + ( OFFSET_X >> 1 ), refY );
		} else {
			record.setRefPixelPosition( OFFSET_X * INDEX_RECORD + ( OFFSET_X >> 1 ), refY );
			eject.setRefPixelPosition( OFFSET_X * INDEX_EJECT + ( OFFSET_X >> 1 ), refY );
		}

		replayIcon.setPosition( getWidth() - replayIcon.getWidth() - BORDER_OFFSET, BORDER_OFFSET );
	}


	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				if ( getCurrentIndex() == INDEX_SPEED ) {
					setReplaySpeed( currentReplaySpeed - 1 );
				}
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				if ( getCurrentIndex() == INDEX_SPEED ) {
					setReplaySpeed( currentReplaySpeed + 1 );
				}
			break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				previousIndex();
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				nextIndex();
			break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				buttonPressed = true;
				switch ( getCurrentIndex() ) {
					case INDEX_PLAY_PAUSE:
						setPaused( !isPaused() );
					break;

					case INDEX_SPEED:
						setReplaySpeed( currentReplaySpeed + 1 );
					break;

					default:
						super.keyPressed( key );
				}
			break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				if ( listener != null )
					listener.onChoose( this, menuId, INDEX_EJECT );
			break;
		} // fim switch ( key )
	} // fim do método keyPressed( int )


	public final void setVisible( boolean v ) {
		super.setVisible( v );
		keyReleased( 0 );
	}


	public void keyReleased( int key ) {
		super.keyReleased( key );
		buttonPressed = false;
	}


	public final void nextIndex() {
		if ( currentIndex < INDEX_EJECT )
			setCurrentIndex( currentIndex + 1 );
		else if ( circular )
			setCurrentIndex( 0 );
	}


	public final void previousIndex() {
		if ( currentIndex > 0 )
			setCurrentIndex( currentIndex - 1 );
		else if ( circular )
			setCurrentIndex( INDEX_EJECT );
	}

//#if TOUCH == "true"
//# 	public final void onPointerPressed( int x, int y ) {
//# 		final int index = getEntryAt( x, y );
//# 		if ( index >= 0 ) {
//# 			if ( currentIndex != index ) {
//# 				setCurrentIndex( index );
//# 				onPointerReleased( x, y );
//# 			}
//# 			
//# 			keyPressed( ScreenManager.FIRE );
//# 		}
//# 	}
//# 
//# 
//# 	public final void onPointerReleased( int x, int y ) {
//# 		super.onPointerReleased( x, y );
//# 		buttonPressed = false;
//# 	}
//# 
//#endif
	protected final int getEntryAt( int x, int y ) {
		if ( y >= replayMiddle.getPosY() && x <= replayRight.getRefPixelX() ) {
			return x / replayMiddle.getFill().getWidth();
		}

		return -1;
	}


	public final void setReplayIconSequence( byte sequence ) {
	}


	public final void setPaused( boolean p ) {
		replayPaused = p;
		play.setVisible( replayPaused );
		pause.setVisible( !replayPaused );
	}


	public final boolean isPaused() {
		return replayPaused;
	}


	public final int getReplayDelta( int delta ) {
		if ( replayPaused )
			return 0;

		return delta >> currentReplaySpeed;
	}


	public final void update( int delta ) {
		if ( isVisible() ) {
			super.update( delta );
		}
	}


	public final void setReplaySpeed( int replaySpeed ) {
		currentReplaySpeed = ( byte ) ( ( replaySpeed + REPLAY_SPEED_TOTAL ) % REPLAY_SPEED_TOTAL );
        final int START_Y = this.getHeight() - replayMiddle.getHeight();
		final int OFFSET_X = replayMiddle.getFill().getWidth();
        final int refY = START_Y + ( replayMiddle.getHeight() >> 1 ) ;

		switch ( currentReplaySpeed ) {
			case REPLAY_SPEED_NORMAL:
				speedLabel.setText( "1X" );
			break;
			
			case REPLAY_SPEED_SLOW:
				speedLabel.setText( "1/2X" );
			break;

			case REPLAY_SPEED_VERY_SLOW:
				speedLabel.setText( "1/4X" );
			break;
		}
		speedLabel.defineReferencePixel( ANCHOR_CENTER );
		//speedLabel.setRefPixelPosition( speedArrows.getRefPixelPosition() );
        speedLabel.setRefPixelPosition( OFFSET_X >> 1, refY );
	}


	public final byte getReplaySpeed() {
		return currentReplaySpeed;
	}


}
