/**
 * IntroBox.java
 * ©2008 Nano Games.
 *
 * Created on Apr 9, 2008 4:15:50 PM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class InfoBox extends GenericInfoBox implements Constants {
	
	public static final byte TYPE_PRESENTATION = 0;
	public static final byte TYPE_FINAL_RESULT = 1;

	private final Label labelScore;
	
	private final Label team1Name;
	private final Label team2Name;

	private final DrawableImage team1Flag;
	private final DrawableImage team2Flag;
	
	private final short[] score;

	private final RichLabel title;

	private byte type;
	
	
	public InfoBox( byte stage, Team team1, Team team2, short[] score ) throws Exception {
		super( 10 );
		
		team1Name = new Label( FONT_INDEX_DEFAULT, team1.getNameShort() );
		team1Name.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( team1Name );
		team2Name = new Label( FONT_INDEX_DEFAULT, team2.getNameShort() );
		team2Name.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( team2Name );
		
		labelScore = new Label( FONT_INDEX_DEFAULT, null );
		insertDrawable( labelScore );

		team1Flag = new DrawableImage( team1.getFlag() );
		team1Flag.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( team1Flag );
		team2Flag = new DrawableImage( team2.getFlag() );
		team2Flag.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		insertDrawable( team2Flag );
		
		this.score = score;

		StringBuffer buffer = new StringBuffer( "<ALN_H>" );
		
		int indexStage;
		
		switch ( stage ) {
			case STAGE_VERSUS:
				indexStage = TEXT_VERSUS;
			break;

			case STAGE_SINGLE_MATCH:
				indexStage = TEXT_STAGE_SINGLE_MATCH;
			break;

			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
				buffer.append( GameMIDlet.getText( TEXT_WORLD_CUP ) );
				buffer.append( '\n' );
				indexStage = TEXT_STAGE_FIRST_MATCH + ( stage - STAGE_1ST_ROUND );
			break;

			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
				buffer.append( GameMIDlet.getText( TEXT_WORLD_CUP ) );
				buffer.append( '\n' );
				indexStage = TEXT_EIGHTH_FINALS + ( stage - STAGE_EIGHTH_FINALS );
			break;

			case STAGE_3RD_PLACE_MATCH:
				buffer.append( GameMIDlet.getText( TEXT_WORLD_CUP ) );
				buffer.append( '\n' );
				indexStage = TEXT_STAGE_3RD_PLACE_MATCH;
			break;

			case STAGE_FINAL_MATCH:
				buffer.append( GameMIDlet.getText( TEXT_WORLD_CUP ) );
				buffer.append( '\n' );
				indexStage = TEXT_STAGE_FINAL_MATCH;
			break;

			default:
				throw new IllegalArgumentException();
		}
		
		buffer.append( GameMIDlet.getText( indexStage ) );
		buffer.append( '\n' );
		buffer.append( '\n' );
		
		title = new RichLabel( GameMIDlet.GetFont( FONT_INDEX_BUTTON ), buffer.toString(), ScreenManager.SCREEN_WIDTH, null );
		insertDrawable( title );
		
		setType( TYPE_PRESENTATION, new short[] { 0, 0 } );
	}

	
	public final void setType( byte type, short[] score ) {
		this.type = type;
		if ( type == TYPE_FINAL_RESULT ) {
			labelScore.setText( " " + score[ 0 ] + " X " + score[ 1 ] + " " );
		} else {
			labelScore.setText( " X " );
		}
		labelScore.defineReferencePixel( ANCHOR_CENTER );
		
		setSize( 0, 0 );
	}


	public final void setSize( int width, int height ) {
		width = type == TYPE_PRESENTATION ? INFO_BOX_WIDTH_PARTIAL : INFO_BOX_WIDTH_FULL;

		int y = BORDER_OFFSET;
		
		title.setSize( width - ( BORDER_OFFSET << 1 ), title.getHeight() );
		title.setPosition( BORDER_OFFSET, y );

		y += title.getHeight() + ITEMS_SPACING;

		labelScore.setRefPixelPosition( width >> 1, y );

		team1Flag.setRefPixelPosition( labelScore.getPosX() - ITEMS_SPACING, y );
		team2Flag.setRefPixelPosition( labelScore.getPosX() + labelScore.getWidth() + ITEMS_SPACING, y );

		y += ( team1Flag.getHeight() >> 1 ) + ITEMS_SPACING;

		team1Name.setRefPixelPosition( team1Flag.getPosX() + ( team1Flag.getWidth() >> 1 ), y );
		team2Name.setRefPixelPosition( team2Flag.getPosX() + ( team2Flag.getWidth() >> 1 ), y );
		y += team1Name.getHeight() + BORDER_OFFSET;
		
		super.setSize( width, y );
	}
	
}
