/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public interface Constants {

	public static final byte TEAMS_TOTAL = 32;
	/** Quantidade de times na fase do grupo. */
	public static final byte TEAMS_IN_GROUP = 4;

	public static final byte GROUPS_TOTAL = TEAMS_TOTAL / TEAMS_IN_GROUP;

	public static final int  NO_TITLE = -1;

	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	public static final String APP_SHORT_NAME = "NPWC";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";

	public static final String PATH_SPLASH_GAME = PATH_IMAGES + "splashGame/";
	
	/** Caminho das imagens das bandeiras dos times. */
	public static final String PATH_TEAMS = PATH_IMAGES + "teams/";
	
	/** Caminho das imagens dos batedores de pênalti. */
	public static final String PATH_SHOOTERS = PATH_IMAGES + "shooters/";
	
	/** Caminho das imagens dos goleiros. */
	public static final String PATH_KEEPERS = PATH_IMAGES + "keepers/";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Caminho das imagens do placar animado. */
	public static final String PATH_BOARD = PATH_IMAGES + "board/";
	
	/** Caminho das imagens de giz. */
	public static final String PATH_MENU_BORDER = PATH_IMAGES + "border/";

	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	public static final String PATH_ONLINE = PATH_IMAGES + "online/";
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";

	/***/
	public static final byte RANKING_TYPE_GOALS_DIFF_SINGLE_PLAYER	= 0;
	public static final byte RANKING_TYPE_GOALS_DIFF_MULTIPLAYER	= 1;
	
	/**Índice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/**Índice do slot de gravação de um campeonato salvo. */
	public static final byte DATABASE_SLOT_CHAMPIONSHIP = 2;
	
	/**Índice do slot de gravação dos replays. */
	public static final byte DATABASE_SLOT_REPLAYS = 3;

	public static final byte DATABASE_SLOT_LANGUAGE = 4;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 4;
	
	/** Cor padrão do fundo de tela. */
	//public static final int BLACKBOARD_COLOR = 0x00482f;
	
	/** Offset da fonte padrão. */
	public static final byte DEFAULT_FONT_OFFSET = -1;
	
	/**Índice da fonte padrão do jogo. */
	public static final byte FONT_INDEX_DEFAULT	= 0;
	
	/**Índice da fonte utilizada para textos. */
	public static final byte FONT_INDEX_TEXT	= 1;

	public static final byte FONT_INDEX_TITLE	= 2;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 3;
	
	
	//índices dos sons do jogo
	public static final byte SOUND_INDEX_SPLASH				= 0;
	public static final byte SOUND_INDEX_WHISTLE			= 1;
	public static final byte SOUND_INDEX_PLAY_UUUUH			= 2;
	public static final byte SOUND_INDEX_PLAY_BAD			= 3;
	public static final byte SOUND_INDEX_CHAMPION			= 4;
	public static final byte SOUND_INDEX_LOSE_CUP			= 5;
	public static final byte SOUND_INDEX_KICK_BALL			= 6;
	public static final byte SOUND_INDEX_POST				= 7;
	public static final byte SOUND_INDEX_HIT_BOARD			= 8;
	public static final byte SOUND_INDEX_CROWD_AMBIENT		= 9;
	public static final byte SOUND_INDEX_PLAY_GOOD_MP3		= 10;
	public static final byte SOUND_INDEX_GAME_WIN			= 11;
	public static final byte SOUND_INDEX_GAME_LOSE			= 12;
	
	public static final byte SOUND_TOTAL = SOUND_INDEX_GAME_LOSE + 1;
	
	/** Duração da vibração ao atingir a trave, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 300;
	
	// <editor-fold defaultstate="collapsed" desc="ETAPAS DE UMA PARTIDA">
	
	public static final byte STAGE_VERSUS				= -2;
	public static final byte STAGE_SINGLE_MATCH			= -1;
	public static final byte STAGE_1ST_ROUND			= 0;
	public static final byte STAGE_2ND_ROUND			= STAGE_1ST_ROUND + 1;
	public static final byte STAGE_3RD_ROUND			= STAGE_2ND_ROUND + 1;
	public static final byte STAGE_EIGHTH_FINALS		= STAGE_3RD_ROUND + 1;
	public static final byte STAGE_QUARTER_FINALS		= STAGE_EIGHTH_FINALS + 1;
	public static final byte STAGE_SEMI_FINALS			= STAGE_QUARTER_FINALS + 1;
	public static final byte STAGE_3RD_PLACE_MATCH		= STAGE_SEMI_FINALS + 1;
	public static final byte STAGE_FINAL_MATCH			= STAGE_3RD_PLACE_MATCH + 1;
	public static final byte STAGE_PLAYER_WON			= STAGE_FINAL_MATCH + 1;
	public static final byte STAGE_PLAYER_ELIMINATED	= STAGE_PLAYER_WON + 1;
	
	// </editor-fold>
	
	// <editor-fold defaulstate="collapsed" desc="DIVISÕES DO CAMPEONATO">

	public static final byte DIVISION_VERSUS		= -4;
	public static final byte DIVISION_NONE			= -3;
	/** Indica para carregar o campeonato salvo anteriormente. */
	public static final byte DIVISION_NO_SAVED_GAME	= -2;
	public static final byte INDEX_LOAD_GAME		= -1;
	public static final byte DIVISION_3				= 0;
	public static final byte DIVISION_2				= 1;
	public static final byte DIVISION_1				= 2;	
	public static final byte DIVISIONS_TOTAL		= 3;

	
	// </editor-fold>


	// <editor-fold defaultstate="collapsed" desc="PALETA DE CORES DOS JOGADORES">

	public static final int COLOR_WHITE						= 0xffffff;
	public static final int COLOR_GRAY_LIGHTER				= 0xd0d0d0;
	public static final int COLOR_GRAY_LIGHT				= 0xb9b9b9;
	public static final int COLOR_GRAY_MEDIUM				= 0xc7c7c7;
	public static final int COLOR_GRAY_DARK					= 0x8f8077;
	public static final int COLOR_GRAY_DARKER				= 0x564a44;
	public static final int COLOR_GRAY_DARKEST				= 0x191919;
	public static final int COLOR_BLACK						= 0x000000;

	public static final int COLOR_BLUE_LIGHTEST				= 0x88cdff;
	public static final int COLOR_BLUE_LIGHTER				= 0x52b3ff;
	public static final int COLOR_BLUE_LIGHT				= 0x1d65e0;
	public static final int COLOR_BLUE_MEDIUM				= 0x0a45be;
	public static final int COLOR_BLUE_DARK					= 0x001d66;
	public static final int COLOR_BLUE_DARKER				= 0x001342;
	public static final int COLOR_BLUE_DARKEST				= 0x000c2b;

	public static final int COLOR_GREEN_LIGHTER				= 0x448932;
	public static final int COLOR_GREEN_LIGHT				= 0x046c00;
	public static final int COLOR_GREEN_MEDIUM				= 0x045600;
	public static final int COLOR_GREEN_DARK				= 0x033e00;
	public static final int COLOR_GREEN_DARKER				= 0x033301;

	public static final int COLOR_ORANGE_LIGHT				= 0xed5811;
	public static final int COLOR_ORANGE_MEDIUM				= 0xed3f11;
	public static final int COLOR_ORANGE_DARK				= 0xb53512;

	public static final int COLOR_BLOND_LIGHT				= 0xffa700;
	public static final int COLOR_BLOND_MEDIUM				= 0xd08000;
	public static final int COLOR_BLOND_DARK				= 0x985900;

	public static final int COLOR_RED_HAIR_LIGHT			= 0xee5500;
	public static final int COLOR_RED_HAIR_MEDIUM			= 0xc23d00;
	public static final int COLOR_RED_HAIR_DARK				= 0x8e2800;

	public static final int COLOR_BROWN_LIGHT				= 0x5f3812;
	public static final int COLOR_BROWN_MEDIUM				= 0x311400;
	public static final int COLOR_BROWN_DARK				= 0x1f0c00;

	public static final int COLOR_RED_LIGHT					= 0xff0000;
	public static final int COLOR_RED_MEDIUM				= 0xce0000;
	public static final int COLOR_RED_DARK					= 0x670000;

	public static final int COLOR_WINE_LIGHT				= 0x840002;
	public static final int COLOR_WINE_MEDIUM				= 0x680000;
	public static final int COLOR_WINE_DARK					= 0x450000;

	public static final int COLOR_YELLOW_LIGHTER			= 0xf3db29;
	public static final int COLOR_YELLOW_LIGHT				= 0xffbd06;
	public static final int COLOR_YELLOW_MEDIUM				= 0xf79f00;
	public static final int COLOR_YELLOW_DARK				= 0xe58900;
	public static final int COLOR_YELLOW_DARKER				= 0xdc6500;

	public static final int COLOR_SKIN_WHITER_LIGHTER		= 0xffffd6;
	public static final int COLOR_SKIN_WHITER_LIGHT			= 0xf6ca9d;
	public static final int COLOR_SKIN_WHITER_MEDIUM		= 0xefb274;
	public static final int COLOR_SKIN_WHITER_DARK			= 0xc7783d;

	public static final int COLOR_SKIN_WHITE_LIGHTER		= 0xffcb7d;
	public static final int COLOR_SKIN_WHITE_LIGHT			= 0xffaa4b;
	public static final int COLOR_SKIN_WHITE_MEDIUM			= 0xf19238;
	public static final int COLOR_SKIN_WHITE_DARK			= 0xa6561f;

	public static final int COLOR_SKIN_ASIAN_LIGHTER		= 0xffcb50;
	public static final int COLOR_SKIN_ASIAN_LIGHT			= 0xe09e00;
	public static final int COLOR_SKIN_ASIAN_MEDIUM			= 0xd36d00;
	public static final int COLOR_SKIN_ASIAN_DARK			= 0x613000;

	public static final int COLOR_SKIN_INDIAN_LIGHTER		= 0xff7f10;
	public static final int COLOR_SKIN_INDIAN_LIGHT			= 0xc75400;
	public static final int COLOR_SKIN_INDIAN_MEDIUM		= 0x9f3e00;
	public static final int COLOR_SKIN_INDIAN_DARK			= 0x601e00;

	public static final int COLOR_SKIN_BROWN_LIGHTER		= 0xd18b42;
	public static final int COLOR_SKIN_BROWN_LIGHT			= 0xa16220;
	public static final int COLOR_SKIN_BROWN_MEDIUM			= 0x8b5012;
	public static final int COLOR_SKIN_BROWN_DARK			= 0x5b3002;

	public static final int COLOR_SKIN_BLACK_LIGHTER		= 0x884c23;
	public static final int COLOR_SKIN_BLACK_LIGHT			= 0x552a10;
	public static final int COLOR_SKIN_BLACK_MEDIUM			= 0x411e0a;
	public static final int COLOR_SKIN_BLACK_DARK			= 0x1f0e05;

	// </editor-fold>

	
	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS TEXTOS">
	public static final int TEXT_OK						     	= 0;
	public static final int TEXT_BACK							= TEXT_OK + 1;
	public static final int TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final int TEXT_OPTIONS						= TEXT_NEW_GAME + 1;
	public static final int TEXT_NANO_ONLINE					= TEXT_OPTIONS + 1;
	public static final int TEXT_REPLAYS						= TEXT_NANO_ONLINE + 1;
	public static final int TEXT_HELP							= TEXT_REPLAYS + 1;
	public static final int TEXT_CREDITS						= TEXT_HELP + 1;
	public static final int TEXT_EXIT							= TEXT_CREDITS + 1;
	public static final int TEXT_PAUSE							= TEXT_EXIT + 1;
	public static final int TEXT_CREDITS_TEXT					= TEXT_PAUSE + 1;
	public static final int TEXT_CONTROLS						= TEXT_CREDITS_TEXT + 1;
	public static final int TEXT_OBJECTIVES					    = TEXT_CONTROLS + 1;
	public static final int TEXT_TIPS							= TEXT_OBJECTIVES + 1;
	public static final int TEXT_HELP_CONTROLS					= TEXT_TIPS + 1;
	public static final int TEXT_HELP_OBJECTIVES				= TEXT_HELP_CONTROLS + 1;
	//public static final int TEXT_HELP_TIPS						= TEXT_HELP_OBJECTIVES + 1;
	public static final int TEXT_TURN_SOUND_ON					= TEXT_HELP_OBJECTIVES + 1;
	public static final int TEXT_TURN_SOUND_OFF					= TEXT_TURN_SOUND_ON + 1;
	public static final int TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final int TEXT_TURN_VIBRATION_OFF				= TEXT_TURN_VIBRATION_ON + 1;
	public static final int TEXT_CANCEL							= TEXT_TURN_VIBRATION_OFF + 1;
	public static final int TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final int TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final int TEXT_LOADING						= TEXT_SPLASH_NANO + 1;
	public static final int TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final int TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final int TEXT_NO								= TEXT_YES + 1;
	public static final int TEXT_BACK_MENU						= TEXT_NO + 1;
	public static final int TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final int TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final int TEXT_CONFIRM_EXIT_1					= TEXT_EXIT_GAME + 1;
	public static final int TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT_1 + 1;
	public static final int TEXT_GAME_OVER						= TEXT_PRESS_ANY_KEY + 1;
	public static final int TEXT_TRAINING						= TEXT_GAME_OVER + 1;
	public static final int TEXT_PARTIAL_RESULT					= TEXT_TRAINING + 1;
	public static final int TEXT_SAVED_GAME_FOUND				= TEXT_PARTIAL_RESULT + 1;
	public static final int TEXT_TEAM_NAME						= TEXT_SAVED_GAME_FOUND + 1;
	public static final int TEXT_GAMES							= TEXT_TEAM_NAME + 1;
	public static final int TEXT_VICTORIES						= TEXT_GAMES + 1;
	public static final int TEXT_LOSSES						= TEXT_VICTORIES + 1;
	public static final int TEXT_GOALS_DIFF					= TEXT_LOSSES + 1;
	public static final int TEXT_GOALS_FOR						= TEXT_GOALS_DIFF + 1;
	public static final int TEXT_SAVE_REPLAY					= TEXT_GOALS_FOR + 1;
	public static final int TEXT_LOAD_REPLAY					= TEXT_SAVE_REPLAY + 1;
	public static final int TEXT_SAVE							= TEXT_LOAD_REPLAY + 1;
	public static final int TEXT_LOAD							= TEXT_SAVE + 1;
	public static final int TEXT_REPLAY_NUMBER					= TEXT_LOAD + 1;
	public static final int TEXT_EMPTY							= TEXT_REPLAY_NUMBER + 1;
	public static final int TEXT_GOAL							= TEXT_EMPTY + 1;
	public static final int TEXT_POST_BACK						= TEXT_GOAL + 1;
	public static final int TEXT_POST_GOAL						= TEXT_POST_BACK + 1;
	public static final int TEXT_POST_OUT						= TEXT_POST_GOAL + 1;
	public static final int TEXT_OUT							= TEXT_POST_OUT + 1;
	public static final int TEXT_DEFENDED						= TEXT_OUT + 1;
	public static final int TEXT_PLAY_RESULT_GOAL				= TEXT_DEFENDED + 1;
	public static final int TEXT_PLAY_RESULT_POST				= TEXT_PLAY_RESULT_GOAL + 1;
	public static final int TEXT_PLAY_RESULT_OUT				= TEXT_PLAY_RESULT_POST + 1;
	public static final int TEXT_PLAY_RESULT_DEFENDED			= TEXT_PLAY_RESULT_OUT + 1;
	public static final int TEXT_CONFIRM_OVERWRITE				= TEXT_PLAY_RESULT_DEFENDED + 1;
	public static final int TEXT_REPLAY_SAVED					= TEXT_CONFIRM_OVERWRITE + 1;
	public static final int TEXT_ATTACK						= TEXT_REPLAY_SAVED + 1;
	public static final int TEXT_DEFEND						= TEXT_ATTACK + 1;
	public static final int TEXT_WORLD_CUP					= TEXT_DEFEND + 1;
	public static final int TEXT_STAGE_SINGLE_MATCH			= TEXT_WORLD_CUP + 1;
	public static final int TEXT_STAGE_FIRST_MATCH				= TEXT_STAGE_SINGLE_MATCH + 1;
	public static final int TEXT_STAGE_SECOND_MATCH			= TEXT_STAGE_FIRST_MATCH + 1;
	public static final int TEXT_STAGE_THIRD_MATCH				= TEXT_STAGE_SECOND_MATCH + 1;
	public static final int TEXT_STAGE_FINAL					= TEXT_STAGE_THIRD_MATCH + 1;
	public static final int TEXT_STAGE_FINAL_MATCH				= TEXT_STAGE_FINAL + 1;
	public static final int TEXT_STAGE_ELIMINATED				= TEXT_STAGE_FINAL_MATCH + 1;
	public static final int TEXT_FINAL_RESULT					= TEXT_STAGE_ELIMINATED + 1;
	public static final int TEXT_CHAMPION_TITLE					= TEXT_FINAL_RESULT + 1;
	public static final int TEXT_CHAMPION						= TEXT_CHAMPION_TITLE + 1;
	public static final int TEXT_PLAYER_1						= TEXT_CHAMPION + 1;
	public static final int TEXT_PLAYER_2						= TEXT_PLAYER_1 + 1;
	public static final int TEXT_VERSUS						= TEXT_PLAYER_2 + 1;
	public static final int TEXT_PASS_TO_PLAYER_1				= TEXT_VERSUS + 1;
	public static final int TEXT_PASS_TO_PLAYER_2				= TEXT_PASS_TO_PLAYER_1 + 1;
	public static final int TEXT_CHOOSE_LANGUAGE				= TEXT_PASS_TO_PLAYER_2 + 1;
	public static final int TEXT_ENGLISH						= TEXT_CHOOSE_LANGUAGE + 1;
	public static final int TEXT_PORTUGUESE					= TEXT_ENGLISH + 1;
	public static final int TEXT_CURRENT_PROFILE		        = TEXT_PORTUGUESE + 1;
    public static final int TEXT_CHOOSE_ANOTHER		        = TEXT_CURRENT_PROFILE + 1;
    public static final int TEXT_CONFIRM				        = TEXT_CHOOSE_ANOTHER + 1;
    public static final int TEXT_NO_PROFILE				    = TEXT_CONFIRM + 1;
    public static final int TEXT_CUSTOMIZE				    = TEXT_NO_PROFILE + 1;
    public static final int TEXT_CHOOSE_YOUR_TEAM			    = TEXT_CUSTOMIZE + 1;
	
    public static final int TEXT_SOUTH_AFRICA			        = TEXT_CHOOSE_YOUR_TEAM + 1;
	public static final int TEXT_MEXICO			              = TEXT_SOUTH_AFRICA + 1;
	public static final int TEXT_URUGUAI			            = TEXT_MEXICO + 1;
    public static final int TEXT_FRANCE				        = TEXT_URUGUAI + 1;

	public static final int TEXT_ARGENTINA			        = TEXT_FRANCE + 1;
	public static final int TEXT_NIGER			            = TEXT_ARGENTINA + 1;
	public static final int TEXT_KOREA_REPUBLIC				= TEXT_NIGER + 1;
	public static final int TEXT_GREECE					    = TEXT_KOREA_REPUBLIC + 1;

	public static final int TEXT_ENGLAND				        = TEXT_GREECE + 1;
	public static final int TEXT_USA				            = TEXT_ENGLAND + 1;
	public static final int TEXT_ALGERIA			            = TEXT_USA + 1;
	public static final int TEXT_SLOVEINA						= TEXT_ALGERIA + 1;
	public static final int TEXT_GERMANY				        = TEXT_SLOVEINA + 1;
	public static final int TEXT_AUSTRALIA			        = TEXT_GERMANY + 1;
    public static final int TEXT_SERBIA				        = TEXT_AUSTRALIA + 1;
	public static final int TEXT_GHANA			            = TEXT_SERBIA + 1;
	public static final int TEXT_NETHERLANDS				    = TEXT_GHANA + 1;
	public static final int TEXT_DENMARK						=TEXT_NETHERLANDS + 1;
    public static final int TEXT_JAPAN						= TEXT_DENMARK + 1;
    public static final int TEXT_CAMEROON			            = TEXT_JAPAN + 1;
	public static final int TEXT_ITALY				        = TEXT_CAMEROON + 1;
    public static final int TEXT_PARAGUAY			            = TEXT_ITALY + 1;
	public static final int TEXT_NEW_ZELAND			        = TEXT_PARAGUAY + 1;
	public static final int TEXT_SLOVAKIA				        = TEXT_NEW_ZELAND + 1;
    public static final int TEXT_BRAZIL			            = TEXT_SLOVAKIA + 1;
    public static final int TEXT_KOREA_DPR					= TEXT_BRAZIL + 1;
    public static final int TEXT_COAST_MARFIN			        = TEXT_KOREA_DPR + 1;
	public static final int TEXT_PORTUGAL					    = TEXT_COAST_MARFIN + 1;
    public static final int TEXT_SPAIN				        = TEXT_PORTUGAL + 1;
    public static final int TEXT_SWITZERLAND				    = TEXT_SPAIN + 1;
	public static final int TEXT_HONDURAS			            = TEXT_SWITZERLAND + 1;
	public static final int TEXT_CHILE			            = TEXT_HONDURAS + 1;
    public static final int TEXT_CUSTOMIZE_NEW_TEAM		    = TEXT_CHILE + 1;
    public static final int TEXT_CHOOSE_OPPONENT_TEAM		    = TEXT_CUSTOMIZE_NEW_TEAM + 1;
    public static final int TEXT_GROUP_A	                    = TEXT_CHOOSE_OPPONENT_TEAM + 1;
    public static final int TEXT_GROUP_B	                    = TEXT_GROUP_A + 1;
    public static final int TEXT_GROUP_C	                    = TEXT_GROUP_B + 1;
    public static final int TEXT_GROUP_D	                    = TEXT_GROUP_C + 1;
    public static final int TEXT_GROUP_E	                    = TEXT_GROUP_D + 1;
    public static final int TEXT_GROUP_F	                    = TEXT_GROUP_E + 1;
    public static final int TEXT_GROUP_G	                    = TEXT_GROUP_F + 1;
    public static final int TEXT_GROUP_H	                    = TEXT_GROUP_G + 1;

    public static final int TEXT_SOUTH_AFRICA_SHORT			        = TEXT_GROUP_H + 1;
	public static final int TEXT_MEXICO_SHORT			            = TEXT_SOUTH_AFRICA_SHORT + 1;
	public static final int TEXT_URUGUAI_SHORT			            = TEXT_MEXICO_SHORT + 1;
	public static final int TEXT_FRANCE_SHORT				        = TEXT_URUGUAI_SHORT + 1;

	public static final int TEXT_ARGENTINA_SHORT			        = TEXT_FRANCE_SHORT + 1;
	public static final int TEXT_NIGER_SHORT			            = TEXT_ARGENTINA_SHORT + 1;
	public static final int TEXT_KOREA_REPUBLIC_SHORT			    = TEXT_NIGER_SHORT + 1;
	public static final int TEXT_GREECE_SHORT					    = TEXT_KOREA_REPUBLIC_SHORT + 1;

	public static final int TEXT_ENGLAND_SHORT				        = TEXT_GREECE_SHORT + 1;
	public static final int TEXT_USA_SHORT				            = TEXT_ENGLAND_SHORT + 1;
	public static final int TEXT_ALGERIA_SHORT			            = TEXT_USA_SHORT + 1;
	public static final int TEXT_SLOVEINA_SHORT						= TEXT_ALGERIA_SHORT + 1;

	public static final int TEXT_GERMANY_SHORT				        = TEXT_SLOVEINA_SHORT + 1;
	public static final int TEXT_AUSTRALIA_SHORT			        = TEXT_GERMANY_SHORT + 1;
    public static final int TEXT_SERBIA_SHORT				        = TEXT_AUSTRALIA_SHORT + 1;
	public static final int TEXT_GHANA_SHORT			            = TEXT_SERBIA_SHORT + 1;

	public static final int TEXT_NETHERLANDS_SHORT				    = TEXT_GHANA_SHORT + 1;
	public static final int TEXT_DENMARK_SHORT						=TEXT_NETHERLANDS_SHORT + 1;
    public static final int TEXT_JAPAN_SHORT						= TEXT_DENMARK_SHORT + 1;
    public static final int TEXT_CAMEROON_SHORT			            = TEXT_JAPAN_SHORT + 1;

	public static final int TEXT_ITALY_SHORT				        = TEXT_CAMEROON_SHORT + 1;
    public static final int TEXT_PARAGUAY_SHORT			            = TEXT_ITALY_SHORT + 1;
	public static final int TEXT_NEW_ZELAND_SHORT			        = TEXT_PARAGUAY_SHORT + 1;
	public static final int TEXT_SLOVAKIA_SHORT				        = TEXT_NEW_ZELAND_SHORT + 1;

    public static final int TEXT_BRAZIL_SHORT			            = TEXT_SLOVAKIA_SHORT + 1;
    public static final int TEXT_KOREA_DPR_SHORT					= TEXT_BRAZIL_SHORT + 1;
    public static final int TEXT_COAST_MARFIN_SHORT			        = TEXT_KOREA_DPR_SHORT + 1;
	public static final int TEXT_PORTUGAL_SHORT					    = TEXT_COAST_MARFIN_SHORT + 1;

    public static final int TEXT_SPAIN_SHORT				        = TEXT_PORTUGAL_SHORT + 1;
    public static final int TEXT_SWITZERLAND_SHORT				    = TEXT_SPAIN_SHORT + 1;
	public static final int TEXT_HONDURAS_SHORT			            = TEXT_SWITZERLAND_SHORT + 1;
	public static final int TEXT_CHILE_SHORT			            = TEXT_HONDURAS_SHORT + 1;

	public static final int TEXT_SKIN_COLOR		                    = TEXT_CHILE_SHORT + 1;
	public static final int TEXT_SHIRT		                        = TEXT_SKIN_COLOR + 1;
	public static final int HAIR		                            = TEXT_SHIRT + 1;
	public static final int ACCESSORIES		                        = HAIR + 1;
	public static final int FACIAL_HAIR		                        = ACCESSORIES + 1;

	public static final int TEXT_EIGHTH_FINALS					    = FACIAL_HAIR+1;
	public static final int TEXT_QUARTER_FINALS						= TEXT_EIGHTH_FINALS+1;
	public static final int TEXT_SEMI_FINALS						= TEXT_QUARTER_FINALS+1;
	public static final int TEXT_STAGE_3RD_PLACE_MATCH				= TEXT_SEMI_FINALS + 1;
	public static final int TEXT_FINAL					            = TEXT_STAGE_3RD_PLACE_MATCH+1;

	public static final int TEXT_RANKING_GOALS_DIFF_SINGLE_PLAYER	= TEXT_FINAL + 1;
	public static final int TEXT_RANKING_GOALS_DIFF_MULTIPLAYER		= TEXT_RANKING_GOALS_DIFF_SINGLE_PLAYER + 1;
	public static final int TEXT_WAITING_OPPONENTS					= TEXT_RANKING_GOALS_DIFF_MULTIPLAYER + 1;
	public static final int TEXT_PLAY								= TEXT_WAITING_OPPONENTS + 1;
	public static final int TEXT_TIME_FORMAT						= TEXT_PLAY + 1;
	public static final int TEXT_VOLUME						        = TEXT_TIME_FORMAT + 1;
	public static final int TEXT_CONNECTING					        = TEXT_VOLUME + 1;
	public static final int TEXT_VERSION					        = TEXT_CONNECTING + 1;
	public static final int TEXT_MULTIPLAYER				        = TEXT_VERSION + 1;
	public static final int TEXT_NO_NEWS_FOUND				        = TEXT_MULTIPLAYER + 1;
	//public static final int TEXT_HELP_NEWS					        = TEXT_NO_NEWS_FOUND + 1;
	public static final int TEXT_UPDATE					            = TEXT_NO_NEWS_FOUND + 1;
	public static final int TEXT_NEWS					            = TEXT_UPDATE + 1;
	public static final int TEXT_NEWS_GENERAL						= TEXT_NEWS + 1;
	public static final int TEXT_NEWS_SCORERS					    = TEXT_NEWS_GENERAL + 1;
	public static final int TEXT_NEWS_TABLE					        = TEXT_NEWS_SCORERS + 1;

	
	/** número total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_NEWS_TABLE + 1;
	
	public static final byte TEXT_CONFIRM_EXIT_TOTAL = 5;
	// </editor-fold>


	public static final int PLAYOFFS_KEYS_INITIAL_POS = 30;

	// caixa que contém as informações de cada chave (imagens e nomes dos corredores, além do resultado)
	public static final byte CHAMPIONSHIP_BOX_RACER_NAME_X			= 35;
	public static final byte CHAMPIONSHIP_BOX_RACER_NAME_WIDTH		= 55;
	public static final byte CHAMPIONSHIP_BOX_RACER_LABEL_HEIGHT	= 11;

	public static final byte CHAMPIONSHIP_BOX_FIRST_RACER_LABEL_Y	= 7;
	public static final byte CHAMPIONSHIP_BOX_SECOND_RACER_LABEL_Y	= 22;

	public static final byte CHAMPIONSHIP_BOX_RACER_SCORE_X			= 94;
	public static final byte CHAMPIONSHIP_BOX_RACER_SCORE_WIDTH		= 6;

	public static final byte CHAMPIONSHIP_BOX_RACER_FACE_X			= 2;
	public static final byte CHAMPIONSHIP_BOX_FIRST_RACER_FACE_Y	= 2;
	public static final byte CHAMPIONSHIP_BOX_SECOND_RACER_FACE_Y	= 19;

	public static final byte CHAMPIONSHIP_BOX_PLAYER_OFFSET_X			= -7;
	public static final byte CHAMPIONSHIP_BOX_PLAYER_FIRST_OFFSET_Y		= 1;
	public static final byte CHAMPIONSHIP_BOX_PLAYER_SECOND_OFFSET_Y	= 18;

	public static final byte CHAMPIONSHIP_KEY_WIDTH			= 20;
	public static final byte CHAMPIONSHIP_KEY_SMALL_HEIGHT	= 41;
	public static final byte CHAMPIONSHIP_KEY_MEDIUM_HEIGHT	= 83;
	public static final short CHAMPIONSHIP_KEY_BIG_HEIGHT	= 163;
	
	
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_CHOOSE_SOUND			= 0;
	public static final byte SCREEN_SPLASH_NANO				= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME_MENU			= SCREEN_MAIN_MENU + 1;	
	public static final byte SCREEN_SAVED_GAME_FOUND		= SCREEN_NEW_GAME_MENU + 1;	
	public static final byte SCREEN_NEW_CHAMPIONSHIP		= SCREEN_SAVED_GAME_FOUND + 1;
	public static final byte SCREEN_MATCH					= SCREEN_NEW_CHAMPIONSHIP + 1;
	public static final byte SCREEN_CHAMPIONSHIP			= SCREEN_MATCH + 1;	
	public static final byte SCREEN_OPTIONS					= SCREEN_CHAMPIONSHIP + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_OBJECTIVES			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_CONTROLS			= SCREEN_HELP_OBJECTIVES + 1;
	public static final byte SCREEN_HELP_TIPS				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_HELP_TIPS + 1;
	public static final byte SCREEN_PAUSE					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU			= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT			= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1				= SCREEN_CONFIRM_EXIT + 1;	
	public static final byte SCREEN_LOADING_2				= SCREEN_LOADING_1 + 1;	
	public static final byte SCREEN_LOADING_PLAY_SCREEN		= SCREEN_LOADING_2 + 1;	
	public static final byte SCREEN_CHOOSE_LANGUAGE			= SCREEN_LOADING_PLAY_SCREEN + 1;
	public static final byte SCREEN_NANO_RANKING_MENU		= SCREEN_CHOOSE_LANGUAGE + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
    public static final byte SCREEN_RECORDS						= SCREEN_NO_PROFILE+1;
    public static final byte SCREEN_CUSTOMIZE					= SCREEN_RECORDS+1;
    public static final byte SCREEN_SELECT_TEAM					= SCREEN_CUSTOMIZE+1;
    public static final byte SCREEN_OP_CUSTOMIZE				= SCREEN_SELECT_TEAM+1;
    public static final byte SCREEN_TABLE_GROUPS				= SCREEN_OP_CUSTOMIZE+1;
    public static final byte SCREEN_PLAYOFFS				    = SCREEN_TABLE_GROUPS+1;
	public static final byte SCREEN_SAVE_REPLAY					= SCREEN_PLAYOFFS + 1;
	public static final byte SCREEN_LOAD_REPLAY					= SCREEN_SAVE_REPLAY + 1;
	public static final byte SCREEN_CHOOSE_COUNTRY_CUP			= SCREEN_LOAD_REPLAY + 1;
	public static final byte SCREEN_CHOOSE_COUNTRY_TRAINING		= SCREEN_CHOOSE_COUNTRY_CUP + 1;
	public static final byte SCREEN_CHOOSE_COUNTRY_MULTIPLAYER	= SCREEN_CHOOSE_COUNTRY_TRAINING + 1;
	public static final byte SCREEN_CHOOSE_COUNTRY_OPPONENT		= SCREEN_CHOOSE_COUNTRY_MULTIPLAYER + 1;
	public static final byte SCREEN_NEWS_MENU					= SCREEN_CHOOSE_COUNTRY_OPPONENT + 1;
	public static final byte SCREEN_NEWS_VIEW_GENERAL			= SCREEN_NEWS_MENU + 1;
	public static final byte SCREEN_NEWS_VIEW_SCORERS			= SCREEN_NEWS_VIEW_GENERAL + 1;
	public static final byte SCREEN_NEWS_VIEW_TABLE				= SCREEN_NEWS_VIEW_SCORERS + 1;
	public static final byte SCREEN_NEWS_LIST_GENERAL			= SCREEN_NEWS_VIEW_TABLE + 1;
	public static final byte SCREEN_NEWS_LIST_SCORERS			= SCREEN_NEWS_LIST_GENERAL + 1;
	public static final byte SCREEN_NEWS_LIST_TABLE				= SCREEN_NEWS_LIST_SCORERS + 1;
	public static final byte SCREEN_NEWS_HELP					= SCREEN_NEWS_LIST_TABLE + 1;
	public static final byte SCREEN_NEWS_REFRESH				= SCREEN_NEWS_HELP + 1;
	public static final byte SCREEN_ERROR_LOG					= SCREEN_NEWS_REFRESH + 1;
	public static final byte SCREEN_START_MULTIPLAYER_MATCH		= SCREEN_ERROR_LOG + 1;
	
	// </editor-fold>


	//times
	public static final byte AFRICA_DO_SUL = 0;
	public static final byte MEXICO = 1;
	public static final byte URUGUAI = 2;
	public static final byte FRANCA = 3;

	public static final byte ARGENTINA = 4;
	public static final byte NIGERIA = 5;
	public static final byte COREIA_DO_SUL = 6;
	public static final byte GRECIA = 7;

	public static final byte INGLATERRA = 8;
	public static final byte US = 9;
	public static final byte ARGELIA = 10;
	public static final byte ESLOVENIA = 11;

	public static final byte ALEMANHA = 12;
	public static final byte AUSTRALIA = 13;
	public static final byte SERVIA = 14;
	public static final byte GHANA = 15;

	public static final byte HOLANDA = 16;
	public static final byte DINAMARCA = 17;
	public static final byte JAPAO = 18;
	public static final byte CAMAROES = 19;

	public static final byte ITALIA = 20;
	public static final byte PARAGUAI = 21;
	public static final byte NOVA_ZELANDIA = 22;
	public static final byte ESLOVAQUIA = 23;

	public static final byte BRASIL = 24;
	public static final byte COREIA_DO_NORTE = 25;
	public static final byte COSTA_DO_MARFIM = 26;
	public static final byte PORTUGAL = 27;

	public static final byte ESPANHA = 28;
	public static final byte SUICA = 29;
	public static final byte HONDURAS = 30;
	public static final byte CHILE = 31;

	public static final int GROUP_A = 0;
	public static final int GROUP_B = 1;
	public static final int GROUP_C = 2;
	public static final int GROUP_D = 3;
	public static final int GROUP_E = 4;
	public static final int GROUP_F = 5;
	public static final int GROUP_G = 6;
	public static final int GROUP_H = 7;

	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_NEWS		    = 2;
	public static final byte ENTRY_MAIN_MENU_NANO_ONLINE		        = 3;
	public static final byte ENTRY_MAIN_MENU_REPLAYS		= 4;
	public static final byte ENTRY_MAIN_MENU_HELP			= 5;
	public static final byte ENTRY_MAIN_MENU_CREDITS		= 6;
	public static final byte ENTRY_MAIN_MENU_EXIT			= 7;
	
	//#if DEBUG == "true"
		public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE NOVO JOGO">
	
	public static final byte ENTRY_NEW_GAME_MENU_CHAMPIONSHIP	= 0;
	public static final byte ENTRY_NEW_GAME_MENU_MULTIPLAYER	= 1;
	public static final byte ENTRY_NEW_GAME_MENU_VERSUS			= 2;
	public static final byte ENTRY_NEW_GAME_MENU_TRAINING		= 3;
	public static final byte ENTRY_NEW_GAME_MENU_BACK			= 4;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DO CAMPEONATO">
	
	public static final byte ENTRY_CHAMPIONSHIP_MENU_3RD_DIVISION	= 0;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_2ND_DIVISION	= 1;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_1ST_DIVISION	= 2;
	public static final byte ENTRY_CHAMPIONSHIP_MENU_BACK			= 3;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VOLUME				= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 4;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 5;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 3;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 4;
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VOLUME					= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 2;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 2;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 3;
			
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE JOGO SALVO ENCONTRADO">
	
	public static final byte ENTRY_SAVED_GAME_CONTINUE					= 0;
	public static final byte ENTRY_SAVED_GAME_NEW_GAME					= 1;
	public static final byte ENTRY_SAVED_GAME_BACK						= 2;
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE NOT�?CIAS">
	
	public static final byte ENTRY_NEWS_GENERAL		= 0;
	public static final byte ENTRY_NEWS_SCORERS		= 1;
	public static final byte ENTRY_NEWS_TABLE		= 2;
	//public static final byte ENTRY_NEWS_HELP		= 3;
	public static final byte ENTRY_NEWS_BACK		= 3;

	// </editor-fold>


	public static final byte SHOOTER_SEQUENCE_STOPPED	= 0;
	public static final byte SHOOTER_SEQUENCE_SHOOTING	= 1;
	public static final byte SHOOTER_SEQUENCE_SHOT		= 2;

	
	// possíveis estados da bola
	public static final byte BALL_STATE_NONE				= 0;
	public static final byte BALL_STATE_STOPPED				= 0;
	public static final byte BALL_STATE_MOVING				= 1;
	public static final byte BALL_STATE_GOAL				= 2;
	public static final byte BALL_STATE_OUT					= 4;
	public static final byte BALL_STATE_POST_BACK			= 8;	// bateu na trave e voltou
	public static final byte BALL_STATE_POST_GOAL			= 10;	// bateu na trave e entrou
	public static final byte BALL_STATE_POST_OUT			= 12;	// bateu na trave e foi para fora
	public static final byte BALL_STATE_REJECTED_KEEPER		= 16;
	public static final byte BALL_STATE_REJECTED_BARRIER	= 32;		
	
	
	/*******************************************************************************************
	 *                              DEFINIÇÕES GENÉRICAS                                       *
	 *******************************************************************************************/
	// comprimentos do "mundo real"
	public static final int FP_REAL_PENALTY_TO_GOAL		= 589824; // oficial: 11m, usado: 9m
	public static final int FP_REAL_GOAL_WIDTH			= 436469; // oficial: 7,32m; usado: 6,66m
	public static final int FP_REAL_FLOOR_TO_BAR		= 196608; // oficial: 2,44m; usado: 3m
	public static final int FP_REAL_POST_WIDTH			= 16384; // oficial: 12cm; usado: proporcional à imagem (8 pixels de largura)
	public static final int FP_REAL_BALL_RAY			= 8192; // oficial: 17,5cm; usado: proporcional à imagem (12 pixels de largura)
	public static final int FP_REAL_GOAL_DEPTH			= -137626; // -2,1m (profundidade máxima que a bola vai no gol)
	public static final int FP_REAL_PENALTY_GOAL_DEPTH	= -42599; // profundidade máxima que a bola vai no gol de pênalti (-0,65m)
	
	/** aceleração da gravidade (equivalente a 9,8m/s²) */
	public static final int FP_GRAVITY_ACCELERATION	= -642253;	
	
	// porcentagem de desvio em relação à trajetória normal causada pela curva. Ex.: num pênalti, a bola se
	// desvia no máximo 1,1m da trajetória reta caso o fator seja 0.1 (11m * 0.1). Valor atual: 0,35
	public static final int FP_BALL_CURVE_FACTOR	= 22937;	

	// frame no qual o jogador de fato chuta a bola
	public static final byte PENALTY_PLAYER_SHOT_FRAME = 4;

	
	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPEC�?FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	/** Espaçamento vertical entre os itens de menu. */
//# 	public static final byte MENU_ITEMS_SPACING = 3;
//# 
//# 	/** Variação na posição do gol em relação à grama, para que as traves estejam na posição correta em relação à linha de fundo. */
//# 	public static final byte GOAL_OFFSET_Y = 3;
//# 
//# 	/** Variação na posição da rede em relação ao gol. */
//# 	public static final byte NET_OFFSET = 0;
//# 
//# 	/** Variação na posição inferior de desenho do goqleiro em relação à altura da linha de fundo. */
//# 	public static final byte KEEPER_OFFSET_Y = 2;
//# 
//# 	/** Altura da área visível da rede. */
//# 	public static final byte NET_HEIGHT = 40;
//# 
//# 	/** Espaçamento entre os ícones dos replays, em pixels. */
//# 	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;
//# 
//# 	/** Posição y inicial do medidor na barra de força.*/
//# 	public static final byte CONTROL_POWER_BAR_INTERNAL_Y		= 8;
//# 
//# 	/** Altura máxima do medidor na barra de força. */
//# 	public static final byte CONTROL_POWER_BAR_INTERNAL_HEIGHT	= 38;
//# 
//# 
//# 	// medidas da imagem do campo, usadas para realizar os cálculos da posição da bola. Os valores
//# 	// são em pixels, e devem ser alterados caso a imagem do campo mude.
//# 	//                    GOAL_WIDTH
//# 	//                     _________
//# 	//               /     |       |
//# 	// FLOOR_TO_BAR__\_____|_______|________
//# 	//                /\
//# 	// PENALTY_TO_GOAL||
//# 	//                ||
//# 	// MARK___________\/_______0____________
//# 	public static final byte PENALTY_MARK_TO_GOAL	= 51;
//# 	public static final byte PENALTY_FLOOR_TO_BAR	= 46;
//# 
//# 
//#  	public static final byte PLAYOFF_BOX_WIDTH = 40;
//#  	public static final byte PLAYOFF_BOX_HEIGHT = 26;
//# 	public static final byte PLAYOFF_KEY_LINE_LENGTH = 20;
//# 	public static final int  PLAYOFF_BOX_SPACE_Y = 6;
//# 	public static final byte BORDER_OFFSET = 3;
//# 
//# 	public static final short INFO_BOX_WIDTH_PARTIAL	= 124;
//#     public static final short INFO_BOX_WIDTH_FULL		= INFO_BOX_WIDTH_PARTIAL + 4;
//# 	public static final byte FONT_INDEX_BUTTON			= FONT_INDEX_DEFAULT;
//# 	public static final byte FONT_INDEX_CREDITS			= FONT_INDEX_DEFAULT;
//# 
//# 
//# 	public static final byte POST_ADJUST = 2;
//# 
//# 	public static final byte FONT_TITLE_EXTRA_OFFSET = 1;
//# 
//# 	public static final byte PLAYOFF_KEY_LINE_THICKNESS = 1;
//# 
//# 	public static final int PLAYOFF_SPACE_EIGHTH = 14;
//# 
//# 	public static final byte MENUBAR_BOTTOM_SPACING = 4;
//# 
//# 	public static final short SCORE_BOX_WIDTH = 128;
//# 
//# 
//# 
//#     /** Altura das placas de publicidade no fundo do campo. */
//# 	public static final byte BOARDS_HEIGHT = 15;
//# 
//# 	public static final byte WALL_HEIGHT = 0;
//# 	public static final byte CROWD_HEIGHT = 20;
//# 	public static final byte CEILING_HEIGHT = 13;
//# 	public static final short CROWD_OFFSET = -65;
//# 	public static final short FIELD_IMAGE_SIZE= 160;
//# 
//# 	public static final byte TITLE_BAR_SPACING_Y = 4;
//# 
//# 	public static final byte  TABLE_LABEL_WIDTH = 18;
//# 
//# 
//#     /** Posição y da linha de fundo. */
//#  	public static final short BACK_LINE_Y_OFFSET = 15;
//# 
//# 	 public static final byte BIG_AREA_LINE_Y = 123;
//# 
//#     public static final short LINE_SMALL_AREA_X_OFFSET = 128;
//# 
//# 	public static final byte SHOOTER_OFFSET_Y = -2;
//# 
//# 	public static final byte BAR_OFFSET_X = 2;
//#  	public static final int MEMORY_FULL = 500; // TODO usar valor suficiente para ambos os goleiros
//# 
//# 	/**  Largura da parte interna do gol (entre as traves) em pixels */
//# 	public static final byte PENALTY_GOAL_WIDTH	= 108;
//# 
//# 	/** Largura padrão da tela de jogo, utilizada como base para posicionar os itens. */
//# 	public static final short SCREEN_DEFAULT_WIDTH = 128;
//# 
//# 	/** Altura padrão da tela de jogo, utilizada como base para posicionar os itens. */
//# 	public static final short SCREEN_DEFAULT_HEIGHT = 128;
//# 
//# 	/** Altura mínima da tela de jogo (para que seja utilizada a movimentação de câmera). */
//# 	public static final short FIELD_MIN_HEIGHT = 40;
//# 
//# 	/** Altura mínima da tela para que os controles de replay sejam exibidos. */
//# 	public static final short SCREEN_REPLAY_CONTROLS_VISIBLE_MIN_HEIGHT = 132;
//# 
//# 	/** Altura em pixels da parte inferior da imagem da marca do pênalti à parte inferior da altura padrão da tela de jogo. */
//# 	public static final byte PENALTY_MARK_Y_OFFSET = -20;
//# 
//# 	/** Posição x,y das imagens de patrocinadores na torcida. */
//# 	public static final byte ADVERTISING_OFFSET = 2;
//# 
//# 	/** Posição y da imagem do campo. */
//# 	public static final byte FIELD_Y = 30;
//# 
//# 	/** Posição y da linha de fundo. */
//# 	public static final byte BACK_LINE_Y = FIELD_Y +20;
//# 
//# 	/** Divisor utilizado para calcular a quantidade máxima de rabiscos na tela. */
//# 	public static final byte CHALK_DIVISOR = 30;
//# 
//# 	/** Variação na posição y da torcida em relação à imagem de fundo do campo, para telas com largura menor ou igual à padrão. */
//# 	public static final byte MATCH_CROWD_Y_OFFSET = 4;
//# 
//#  	/** Offset na posição inferior do pattern do céu em relação à posição y da torcida. */
//#  	public static final byte SKY_PATTERN_CROWD_Y_OFFSET = 0;
//# 
//# 	/** Posição x do primeiro botão do replay. */
//# 	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;
//# 
//# 	/** Largura de cada botão do controle de replay. */
//# 	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;
//# 
//# 	//</editor-fold>
//# 
	//#elif SCREEN_SIZE == "MEDIUM"

	//<editor-fold desc="DEFINIÇÕES ESPECÍ?FICAS PARA TELA MÉDIA" defaultstate="collapsed">

	/** Variação na posição do gol em relação à grama, para que as traves estejam na posição correta em relação à linha de fundo. */
	public static final byte GOAL_OFFSET_Y = 5;

	/** Variação na posição da rede em relação ao gol. */
	public static final byte NET_OFFSET = 3;

	/** Variação na posição inferior de desenho do goleiro em relação à altura da linha de fundo. */
	public static final byte KEEPER_OFFSET_Y = 4;

	/** Altura da área visível da rede. */
	public static final byte NET_HEIGHT = 57;

	/** Espaçamento entre os ícones dos replays, em pixels. */
	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;

	/** Posição y inicial do medidor na barra de força.*/
	public static final byte CONTROL_POWER_BAR_INTERNAL_Y		= 11;

	// medidas da imagem do campo, usadas para realizar os cálculos da posição da bola. Os valores
	// são em pixels, e devem ser alterados caso a imagem do campo mude.
	//                    GOAL_WIDTH
	//                     _________
	//               /     |       |
	// FLOOR_TO_BAR__\_____|_______|________
	//                /\
	// PENALTY_TO_GOAL||
	//                ||
	// MARK___________\/_______0____________
	public static final byte PENALTY_MARK_TO_GOAL	= 78;
	public static final byte PENALTY_FLOOR_TO_BAR	= 63;


	/**  Largura da parte interna do gol (entre as traves) em pixels */
	public static final short PENALTY_GOAL_WIDTH	= 143;

	/** Posição x,y das imagens de patrocinadores na torcida. */
	public static final byte ADVERTISING_OFFSET = 2;

	/** Posição x do primeiro botão do replay. */
	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;

	/** Largura de cada botão do controle de replay. */
	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;

    public static final short INFO_BOX_WIDTH_PARTIAL	= 168;
    public static final short INFO_BOX_WIDTH_FULL		= INFO_BOX_WIDTH_PARTIAL + 8;
	public static final byte FONT_INDEX_BUTTON			= FONT_INDEX_DEFAULT;
	public static final byte FONT_INDEX_CREDITS			= FONT_INDEX_DEFAULT;
	public static final byte BORDER_OFFSET = 3;

    /** Posição y da linha de fundo. */
 	public static final short BACK_LINE_Y_OFFSET = 28;

    public static final short SCORE_BOX_WIDTH = 176;

    /** Altura das placas de publicidade no fundo do campo. */
	public static final byte BOARDS_HEIGHT = 24;

	public static final byte WALL_HEIGHT = 4;
	public static final byte CROWD_HEIGHT = 65;
	public static final byte CEILING_HEIGHT = 13;
	public static final short CROWD_OFFSET = -140;
	public static final short FIELD_IMAGE_SIZE= 295;

    public static final byte BIG_AREA_LINE_Y = 123;

    public static final short LINE_SMALL_AREA_X_OFFSET = 276;

   	public static final byte BAR_OFFSET_X = 2;
 	public static final int MEMORY_FULL = 1000; // TODO usar valor suficiente para ambos os goleiros
 	public static final byte TITLE_BAR_SPACING_Y = 4;
	/** Offset da posição do goleiro em relação à posição y de baixo do campo. */
 	public static final byte SHOOTER_OFFSET_Y = -82;

	public static final int PLAYOFF_BOX_SPACE_Y = 6;

 	public static final byte PLAYOFF_BOX_WIDTH = 64;
 	public static final byte PLAYOFF_BOX_HEIGHT = 38;

	public static final byte  TABLE_LABEL_WIDTH = 25;

	///ajustes do jogador  na tela de selecao de times
	///regula os bitshifts que ajustam a posicao do jogador na tela
	public static final byte TEAM_SELECT_SHOOTER_VERTICAL_ADJUSTMENT = 5;
	public static final byte TEAM_SELECT_SHOOTER_HORIZONTAL_ADJUSTMENT = 3;
	///da o peteleco final na posicao do jogador na tela. +[wdith/height * multiplier]/divider
	///porque nao um float? bem, THIS IS SPARTA!
	public static final byte TEAM_SELECT_SHOOTER_VERTICAL_MULTIPLIER = -20;
	public static final byte TEAM_SELECT_SHOOTER_HORIZONTAL_MULTIPLIER = 1;
	public static final byte TEAM_SELECT_SHOOTER_DIVIDER = 20;

	public static final byte POST_ADJUST = 2;

	public static final byte FONT_TITLE_EXTRA_OFFSET = 1;

	public static final byte PLAYOFF_KEY_LINE_THICKNESS = 3;

	public static final byte PLAYOFF_KEY_LINE_LENGTH = 30;

	public static final int PLAYOFF_SPACE_EIGHTH = 14;

	public static final byte MENUBAR_BOTTOM_SPACING = 4;

    //</editor-fold>
	//#elif SCREEN_SIZE == "BIG"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPEC�?FICAS PARA TELA GRANDE" defaultstate="collapsed">
//#
//# 	/** Variação na posição do gol em relação à grama, para que as traves estejam na posição correta em relação à linha de fundo. */
//# 	public static final byte GOAL_OFFSET_Y = 5;
//#
//# 	/** Variação na posição da rede em relação ao gol. */
//# 	public static final byte NET_OFFSET = 3;
//#
//# 	/** Variação na posição inferior de desenho do goleiro em relação à altura da linha de fundo. */
//# 	public static final byte KEEPER_OFFSET_Y = 4;
//#
//# 	/** Altura da área visível da rede. */
//# 	public static final byte NET_HEIGHT = 57;
//#
//# 	/** Espaçamento entre os ícones dos replays, em pixels. */
//# 	public static final byte REPLAY_SCREEN_ICON_SPACING = 7;
//#
//# 	/** Posição y inicial do medidor na barra de força.*/
//# 	public static final byte CONTROL_POWER_BAR_INTERNAL_Y		= 11;
//#
//# 	// medidas da imagem do campo, usadas para realizar os cálculos da posição da bola. Os valores
//# 	// são em pixels, e devem ser alterados caso a imagem do campo mude.
//# 	//                    GOAL_WIDTH
//# 	//                     _________
//# 	//               /     |       |
//# 	// FLOOR_TO_BAR__\_____|_______|________
//# 	//                /\
//# 	// PENALTY_TO_GOAL||
//# 	//                ||
//# 	// MARK___________\/_______0____________
//# 	public static final byte PENALTY_MARK_TO_GOAL	= 78;
//# 	public static final byte PENALTY_FLOOR_TO_BAR	= 63;
//#
//#
//# 	/**  Largura da parte interna do gol (entre as traves) em pixels */
//# 	public static final short PENALTY_GOAL_WIDTH	= 143;
//#
//# 	/** Posição x,y das imagens de patrocinadores na torcida. */
//# 	public static final byte ADVERTISING_OFFSET = 2;
//#
//# 	/** Posição x do primeiro botão do replay. */
//# 	public static final byte REPLAY_CONTROL_FIRST_BUTTON_X = 6;
//#
//# 	/** Largura de cada botão do controle de replay. */
//# 	public static final byte REPLAY_CONTROL_BUTTON_WIDTH = 15;
//#
//#     public static final short INFO_BOX_WIDTH_PARTIAL	= 190;
//#     public static final short INFO_BOX_WIDTH_FULL		= INFO_BOX_WIDTH_PARTIAL + 30;
//# 	public static final byte FONT_INDEX_BUTTON			= FONT_INDEX_DEFAULT;
//# 	public static final byte FONT_INDEX_CREDITS			= FONT_INDEX_DEFAULT;
//# 	public static final byte BORDER_OFFSET = 5;
//#
//#     /** Posição y da linha de fundo. */
//#  	public static final short BACK_LINE_Y_OFFSET = 28;
//#
//#     public static final short SCORE_BOX_WIDTH = 210;
//#
//#     /** Altura das placas de publicidade no fundo do campo. */
//# 	public static final byte BOARDS_HEIGHT = 32;
//#
//# 	public static final byte WALL_HEIGHT = 4;
//# 	public static final byte CROWD_HEIGHT = 65;
//# 	public static final byte CEILING_HEIGHT = 13;
//# 	public static final short CROWD_OFFSET = -140;
//# 	public static final short FIELD_IMAGE_SIZE= 295;
//#
//#     public static final byte BIG_AREA_LINE_Y = 123;
//#
//#     public static final short LINE_SMALL_AREA_X_OFFSET = 276;
//#
//#    	public static final byte BAR_OFFSET_X = 2;
//#  	public static final int MEMORY_FULL = 1000; // TODO usar valor suficiente para ambos os goleiros
//#  	public static final byte TITLE_BAR_SPACING_Y = 10;
//# 	/** Offset da posição do goleiro em relação à posição y de baixo do campo. */
//#  	public static final byte SHOOTER_OFFSET_Y = -82;
//#
//# 	public static final int PLAYOFF_BOX_SPACE_Y = 12;
//#
//#  	public static final byte PLAYOFF_BOX_WIDTH = 84;
//#  	public static final byte PLAYOFF_BOX_HEIGHT = 52;
//#
//#  	public static final byte  TABLE_LABEL_WIDTH = 27;
//#
//#
//#
//# 	///ajustes do jogador  na tela de selecao de times
//# 	///regula os bitshifts que ajustam a posicao do jogador na tela
//# 	public static final byte TEAM_SELECT_SHOOTER_VERTICAL_ADJUSTMENT = 5;
//# 	public static final byte TEAM_SELECT_SHOOTER_HORIZONTAL_ADJUSTMENT = 3;
//# 	///da o peteleco final na posicao do jogador na tela. +[wdith/height * multiplier]/divider
//# 	///porque nao um float? bem, THIS IS SPARTA!
//# 	public static final byte TEAM_SELECT_SHOOTER_VERTICAL_MULTIPLIER = -20;
//# 	public static final byte TEAM_SELECT_SHOOTER_HORIZONTAL_MULTIPLIER = 1;
//# 	public static final byte TEAM_SELECT_SHOOTER_DIVIDER = 20;
//#
//# 	public static final byte POST_ADJUST = 2;
//#
//# 	public static final byte FONT_TITLE_EXTRA_OFFSET = 1;
//#
//# 	public static final byte PLAYOFF_KEY_LINE_THICKNESS = 3;
//#
//# 	public static final byte PLAYOFF_KEY_LINE_LENGTH = 30;
//#
//# 	public static final int PLAYOFF_SPACE_EIGHTH = 14;
//#
//# 	public static final byte MENUBAR_BOTTOM_SPACING = 6;
//#
//#     //</editor-fold>
	//#elif SCREEN_SIZE == "GIANT"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE" defaultstate="collapsed">
//#
//# 	/** Variação na posição do gol em relação à grama, para que as traves estejam na posição correta em relação à linha de fundo. */
//# 	public static final byte GOAL_OFFSET_Y = 2;
//#
//# 	/** Variação na posição da rede em relação ao gol. */
//# 	public static final byte NET_OFFSET = 3;
//#
//# 	/** Variação na posição inferior de desenho do goleiro em relação à altura da linha de fundo. */
//# 	public static final byte KEEPER_OFFSET_Y = 12;
//#
//# 	/** Altura da área visível da rede. */
//# 	public static final byte NET_HEIGHT = 95;
//#
//# 	/** Espaçamento entre os ícones dos replays, em pixels. */
//# 	public static final byte REPLAY_SCREEN_ICON_SPACING = 3;
//#
//# 	/** Posição y inicial do medidor na barra de força.*/
//# 	public static final byte CONTROL_POWER_BAR_INTERNAL_Y		= 11;
//#
//# 	// medidas da imagem do campo, usadas para realizar os cálculos da posição da bola. Os valores
//# 	// são em pixels, e devem ser alterados caso a imagem do campo mude.
//# 	//                    GOAL_WIDTH
//# 	//                     _________
//# 	//               /     |       |
//# 	// FLOOR_TO_BAR__\_____|_______|________
//# 	//                /\
//# 	// PENALTY_TO_GOAL||
//# 	//                ||
//# 	// MARK___________\/_______0____________
//# 	public static final byte PENALTY_MARK_TO_GOAL	= 83;
//# 	public static final byte PENALTY_FLOOR_TO_BAR	= 117;
//#
//# 	/**  Largura da parte interna do gol (entre as traves) em pixels */
//# 	public static final short PENALTY_GOAL_WIDTH	= 246;
//#
//# 	public static final byte BAR_OFFSET_X = 2;
//#
//# 	/** Largura padrão da tela de jogo, utilizada como base para posicionar os itens. */
//# 	public static final short SCREEN_DEFAULT_WIDTH = 320;
//#
//# 	/** Altura padrão da tela de jogo, utilizada como base para posicionar os itens. */
//# 	public static final short SCREEN_DEFAULT_HEIGHT = 320;
//#
//# 	/** Altura mínima da tela de jogo (para que seja utilizada a movimentação de câmera). */
//# 	public static final short FIELD_MIN_HEIGHT = 320;
//#
//# 	/** Altura em pixels da parte inferior da imagem da marca do pênalti à parte inferior da altura padrão da tela de jogo. */
//# 	public static final byte PENALTY_MARK_Y_OFFSET = -30;
//#
//# 	public static final byte BIG_AREA_LINE_Y = 123;
//#
//# 	public static final short LINE_SMALL_AREA_X_OFFSET = 276;
//#
//# 	public static final byte SMALL_AREA_LINE_Y = 37;
//#
//# 	/** Posição x,y das imagens de patrocinadores na torcida. */
//# 	public static final byte ADVERTISING_OFFSET = 2;
//#
//# 	/** Posição y da linha de fundo. */
//# 	public static final short BACK_LINE_Y_OFFSET = 28;
//#
//# 	/** Altura das placas de publicidade no fundo do campo. */
//# 	public static final byte BOARDS_HEIGHT = 40;
//#
//# 	public static final byte WALL_HEIGHT = 17;
//# 	public static final byte CROWD_HEIGHT = 96;
//# 	public static final byte CEILING_HEIGHT = 25;
//# 	public static final short CROWD_OFFSET = -199;
//# 	public static final  short FIELD_IMAGE_SIZE= 425;
//#
//# 	/** Offset na posição inferior do pattern do céu em relação à posição y da torcida. */
//# 	public static final byte SKY_PATTERN_CROWD_Y_OFFSET = 36;
//#
//# 	public static final byte FONT_INDEX_BUTTON	= FONT_INDEX_DEFAULT;
//# 		public static final byte FONT_INDEX_CREDITS			= FONT_INDEX_TITLE;
//#
//# 	public static final byte BORDER_OFFSET = 5;
//#
//# 	public static final short SCORE_BOX_WIDTH = 280;
//#
//# 	public static final short INFO_BOX_WIDTH_PARTIAL	= 250;
//# 	public static final short INFO_BOX_WIDTH_FULL		= INFO_BOX_WIDTH_PARTIAL + 30;
//#
//# 	public static final byte TITLE_BAR_SPACING_Y = 10;
//#
//# 	public static final int MEMORY_FULL = 1000; // TODO usar valor suficiente para ambos os goleiros
//#
//# 	/** Offset da posição do goleiro em relação à posição y de baixo do campo. */
//# 	public static final byte SHOOTER_OFFSET_Y = -82;
//#
//# 	public static final byte PLAYOFF_BOX_WIDTH = 84;
//# 	public static final byte PLAYOFF_BOX_HEIGHT = 52;
//#
//# 	public static final byte  TABLE_LABEL_WIDTH = 35;
//#
//# 	public static final byte POST_ADJUST = 1;
//#
//# 	public static final int PLAYOFF_BOX_SPACE_Y = 20;
//#
//# 	///ajustes do jogador  na tela de selecao de times
//# 	///regula os bitshifts que ajustam a posicao do jogador na tela
//# 	public static final byte TEAM_SELECT_SHOOTER_VERTICAL_ADJUSTMENT = 1;
//# 	public static final byte TEAM_SELECT_SHOOTER_HORIZONTAL_ADJUSTMENT = 2;
//# 	///da o peteleco final na posicao do jogador na tela. +[wdith/height * multiplier]/divider
//# 	///porque nao um float? bem, THIS IS SPARTA!
//# 	public static final byte TEAM_SELECT_SHOOTER_VERTICAL_MULTIPLIER = -11;
//# 	public static final byte TEAM_SELECT_SHOOTER_HORIZONTAL_MULTIPLIER = -2;
//# 	public static final byte TEAM_SELECT_SHOOTER_DIVIDER = 10;
//#
//# 	public static final byte FONT_TITLE_EXTRA_OFFSET = -1;
//#
//# 	public static final byte PLAYOFF_KEY_LINE_THICKNESS = 3;
//#
//# 	public static final byte PLAYOFF_KEY_LINE_LENGTH = 30;
//#
//# 	public static final int PLAYOFF_SPACE_EIGHTH = 14;
//#
//# 	public static final byte MENUBAR_BOTTOM_SPACING = 10;
//#
//#
//#
//# 	//</editor-fold>
//#
//#
	//#endif

	public static final int KEY_WIDTH = PLAYOFF_BOX_WIDTH + ( PLAYOFF_KEY_LINE_LENGTH << 1 );
	public static final int KEY_HEIGHT = PLAYOFF_BOX_HEIGHT + PLAYOFF_BOX_SPACE_Y;

	public static final int KEY_WIDTH_FINAL = ( PLAYOFF_BOX_WIDTH + PLAYOFF_KEY_LINE_LENGTH ) << 1;

	public static final int PLAYOFFS_TOTAL_WIDTH = ( KEY_WIDTH * 6 ) + KEY_WIDTH_FINAL;

	public static final int PLAYOFFS_TOTAL_HEIGHT = ( KEY_HEIGHT * 8 );

	// valores já convertidos para ponto fixo (devem ser atualizados caso os valores em pixel sejam alterados)
	public static final int FP_PENALTY_MARK_TO_GOAL	= PENALTY_MARK_TO_GOAL * NanoMath.ONE;
	public static final int FP_PENALTY_FLOOR_TO_BAR	= PENALTY_FLOOR_TO_BAR * NanoMath.ONE;
	public static final int FP_PENALTY_GOAL_WIDTH	= PENALTY_GOAL_WIDTH * NanoMath.ONE;
	
}
