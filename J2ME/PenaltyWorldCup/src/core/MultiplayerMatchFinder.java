/**
 * MultiplayerMatchFinder.java
 *
 * Created on Jun 19, 2010 5:00:20 PM
 *
 */

//#if MULTIPLAYER == "true"
package core;

import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoOnline;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class MultiplayerMatchFinder extends MessageBox implements TurnBasedMultiplayerConstants {

	/** Tempo esperado até iniciar uma nova tentativa de conexão. */
	private static final short TIME_RECONNECTION = 2000;


	public MultiplayerMatchFinder() throws Exception {
		super( NanoOnline.NANO_ONLINE_URL+ "multiplayer/enter_game", null );
		setNextScreen( SCREEN_NEW_GAME_MENU );
	}


	public final void processData( int id, byte[] data ) {
		super.processData( id, data );
		DataInputStream input = null;
		
		try {
			final Hashtable table = NanoOnline.readGlobalData( data );

			// o código de retorno é obrigatório
			final short returnCode = ( ( Short ) table.get( new Byte( NanoOnline.ID_RETURN_CODE ) ) ).shortValue();

			switch ( returnCode ) {
				case NanoOnline.RC_OK:
					final byte[] specificData = ( byte[] ) table.get( new Byte( NanoOnline.ID_SPECIFIC_DATA ) );
					input = new DataInputStream( new ByteArrayInputStream( specificData ) );
					final MultiplayerMatchInfo matchInfo = new MultiplayerMatchInfo();
					MultiplayerMatchInfo.Player[] players = null;
					byte currentPlayer = 0;

					byte param = -1;
					do {
						param = input.readByte();

						switch ( param ) {
							case PARAM_WAITING_OPPONENTS:
								showMessage( MSG_TYPE_INFO, GameMIDlet.getText( TEXT_WAITING_OPPONENTS ) );
								reconnect();
							break;

							case PARAM_MATCH_ID:
								matchInfo.setMatchId( input.readInt() );
							break;

							case PARAM_N_PLAYERS:
								players = new MultiplayerMatchInfo.Player[ input.readByte() ];
							break;

							case PARAM_PLAYER_ID:
								players[ currentPlayer ].setId( input.readInt() );
							break;

							case PARAM_PLAYER_ORDER:
							break;

							case PARAM_PLAYER_NICKNAME:
								players[ currentPlayer ].setNickname( input.readUTF() );
							break;

							case PARAM_PLAYER_INFO_END:
								++currentPlayer;
							break;

							case PARAM_ALREADY_IN_MATCH:
							break;

							case PARAM_PLAYER_NOT_FOUND:
							break;
						}
					} while ( input.available() > 0 );

					if ( currentPlayer == 2 ) {
						GameMIDlet.playMatch( matchInfo );
					}
				break;

				default:
					showMessage( MSG_TYPE_ERROR, NanoOnline.getText( NanoOnline.TEXT_ERROR_SERVER_INTERNAL ) );
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
				
			showMessage( MSG_TYPE_ERROR, NanoOnline.getText( NanoOnline.TEXT_ERROR ) );
		}
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		super.onError( id, errorIndex, extraData );

		reconnect();
	}


	private final void reconnect() {
		final TimerTask task = new TimerTask() {
			public final void run() {
				connect();
			}
		};

		final Timer timer = new Timer();
		timer.schedule( task, TIME_RECONNECTION );
	}

}

//#endif