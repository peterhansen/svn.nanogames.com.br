/*
 * Ball.java
 *
 * Created on 28 de Agosto de 2007, 00:53
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;
import screens.Match;

/**
 *
 * @author peter
 */
public final class Ball extends UpdatableGroup implements Constants {
	
	//<editor-fold desc="DEFINIÇÕES GENÉRICAS DA BOLA" defaultstate="collapsed">
	
	/** "fundo" do campo (placas de publicidade) */
	private static final int FP_PENALTY_BALL_Z_LIMIT = -163840; //NanoMath.divInt( -25, 10 );
	
	/** Altura das placas de publicidade em metros. */
	private static final int FP_PENALTY_BALL_Y_LIMIT = NanoMath.ONE;

	// limites de colisão com as traves
	// travessão
	private static final int FP_REAL_HALF_POST_WIDTH		= 8192;//NanoMath.mulFixed( FP_REAL_POST_WIDTH, NanoMath.HALF );
	
	/** distância máxima do centro da bola ao centro de uma trave numa colisão. */
	private static final int FP_REAL_MAX_TOUCH_DISTANCE		= FP_REAL_HALF_POST_WIDTH + FP_REAL_BALL_RAY;
	
	public static final int FP_REAL_BAR_MIDDLE				= 196608;//FP_REAL_FLOOR_TO_BAR;
	public static final int FP_REAL_BAR_BOTTOM				= FP_REAL_BAR_MIDDLE - FP_REAL_MAX_TOUCH_DISTANCE;
	public static final int FP_REAL_BAR_TOP					= FP_REAL_BAR_MIDDLE + FP_REAL_MAX_TOUCH_DISTANCE;
	// trave esquerda
	public static final int FP_REAL_LEFT_POST_MIDDLE		= -226426;//-NanoMath.mulFixed( FP_REAL_GOAL_WIDTH, NanoMath.HALF ) - FP_REAL_HALF_POST_WIDTH;
	public static final int FP_REAL_LEFT_POST_START			= FP_REAL_LEFT_POST_MIDDLE + FP_REAL_MAX_TOUCH_DISTANCE;
	public static final int FP_REAL_LEFT_POST_END			= FP_REAL_LEFT_POST_MIDDLE - FP_REAL_MAX_TOUCH_DISTANCE;
	// trave direita
	public static final int FP_REAL_RIGHT_POST_MIDDLE		= -FP_REAL_LEFT_POST_MIDDLE;
	public static final int FP_REAL_RIGHT_POST_START		= FP_REAL_RIGHT_POST_MIDDLE - FP_REAL_MAX_TOUCH_DISTANCE;
	public static final int FP_REAL_RIGHT_POST_END			= FP_REAL_RIGHT_POST_MIDDLE + FP_REAL_MAX_TOUCH_DISTANCE;
	

	// posições reais a partir das quais a bola troca seu frame (muda de tamanho)
	private static final int FP_BALL_PENALTY_FRAME_1_Z = 458752;//NanoMath.toFixed( 7 );
	private static final int FP_BALL_PENALTY_FRAME_2_Z = 196608;//NanoMath.toFixed( 3 );
	private static final int FP_BALL_PENALTY_FRAME_3_Z = FP_REAL_PENALTY_GOAL_DEPTH;
	
	private static final int FP_BALL_MAX_Y_SHADOW_NORMAL = NanoMath.HALF;
	private static final int FP_BALL_MAX_Y_SHADOW_SMALLER_1 = NanoMath.ONE;
	private static final int FP_BALL_MAX_Y_SHADOW_SMALLER_2 = NanoMath.ONE + NanoMath.HALF;
	
	/** Imprecisão máxima na direção da bola causada pela força do chute (quanto maior a força, maior a probabilidade de imprecisão). */
	private static final int FP_BALL_MAX_DIRECTION_IMPRECISION = 11796;//NanoMath.divInt( 18, 100 );

	/** Imprecisão máxima na altura da bola causada pela força do chute (quanto maior a força, maior a probabilidade de imprecisão). */
	private static final int FP_BALL_MAX_HEIGHT_IMPRECISION = 9175;//NanoMath.divInt( 14, 100 );

	//</editor-fold>
	

	/** número de itens da coleção (imagem da bola e sombra) */
	public static final byte BALL_N_ITEMS = 2;

	/** posição real da marca do pênalti */
	public static final Point3f REAL_PENALTY_POSITION = new Point3f( 0, 0, FP_REAL_PENALTY_TO_GOAL );

	/** tipos de colisão com jogador */
	public static final byte COLLISION_KEEPER	= 0;
	public static final byte COLLISION_BARRIER	= 1;

	/** possíveis colisões da bola */
	public static final byte POST_LEFT	= 0;
	public static final byte POST_RIGHT	= 1;
	public static final byte POST_BAR	= 2;
	
	
	/** "fator de quicada" da bola (qual fração da velocidade ela retoma ao quicar) */
	private static final int FP_BALL_BOUNCE_FACTOR = 45875;//NanoMath.divInt( 70, 100 );

	/** "fator de elasticidade" da bola ao colidir com uma trave, ou seja, quantos
	 * % da velocidade máxima que ela pode alcançar ao ser rebatida por uma trave.
	 */
	private static final int FP_REAL_ELASTIC_FACTOR	= -36045;//NanoMath.divInt( -55, 100 );
	/** velocidade vertical mínima da bola - valor usado para evitar que bola quique no chão indefinidamente, ou seja, que ela possa rolar  */
	private static final int FP_REAL_MIN_Y_SPEED	= 45875;//NanoMath.divInt( 70, 100 );

	/** fator de desaceleração da bola após ela colidir com algo (valor multiplicado pela velocidade a cada segundo) */
	private static final int FP_BALL_SLOW_DOWN_FACTOR	= 53084;//NanoMath.divInt( 81, 100 );

	/** velocidade máxima (positiva ou negativa) da bola após colisão com goleiro ou barreira */
	private static final int FP_AFTER_COLLISION_MIN_Y	= -786432;//NanoMath.toFixed( -12 );
	private static final int FP_AFTER_COLLISION_MAX_Y	= 1703936;//NanoMath.toFixed( 26 );

	/** Distância real que a bola percorre no ar para que haja troca de frame da rotação. */
	private static final int FP_SPIN_DISTANCE_AIR			= 22937;//NanoMath.divInt( 35, 100 );	
	
	/** Distância real que a bola percorre no chão para que haja troca de frame da rotação. */
	private static final int FP_SPIN_DISTANCE_GROUND		= FP_SPIN_DISTANCE_AIR >> 1;	
	
	/** Sequência de animação da bola grande. */
	private static final byte SEQUENCE_BIG			= 0;
	
	/** Sequência de animação da bola média. */
	private static final byte SEQUENCE_MEDIUM		= 1;
	
	/** Sequência de animação da bola pequena. */
	private static final byte SEQUENCE_SMALL		= 2;

	/** Sequência de animação da bola pequena. */
	private static final byte SEQUENCE_VERY_SMALL	= 3;
	
	
	/** imagem da bola */
	private static Sprite sprite;
	
	//#if JAR != "min"
	/** imagem da sombra da bola */
	private static Sprite shadow;
	//#endif

	/** Indica as posições x, y, e z da bola no "mundo real". Esses são os valores efetivamente
	 * usados para realizar os cálculos da bola.
	 */
	private final Point3f realPosition = new Point3f();
	private final Point3f realInitialPosition = new Point3f(); 
	
	/** usado para realizar os cálculos após colisão com goleiro, trave ou barreira */
	private final Point3f realSpeed = new Point3f();
	
	/** usado para realizar os cálculos de pontos no modo desafio de faltas */
	private final Point3f realLastGoalPosition = new Point3f();
	
	/** vetor da aceleração causada pelo efeito */
	private final Point3f curveVector = new Point3f();

	/** Usado para realizar cálculos de colisão da bola */
	private int fp_realInitialYSpeed;

	/** Tempo acumulado em segundos desde a última colisão (incluindo chute ou colisão com jogadores) */
	private int fp_accTime;
	
	/** Tempo acumulado em segundos desde a última colisão com o chão (ou chute) */
	private int fp_accTimeY;	
	
	/** tempo acumulado da "rotação" da bola */
	private int fp_accSpinTime;

	/** Estado da bola (parada, movendo, etc) */
	private byte state;

	/** tempo total que a bola levará para chegar ao gol (em segundos) */
	private int fp_timeToGoal;

	/** indica se já foi efetuado o cálculo de colisão com o gol na movimentação atual da bola */
	private boolean checkedGoalCollision;

	/** armazena o instante de tempo da última jogada em que ocorreu a definição do resultado (gol, trave, fora, etc), 
	 * para que no replay o teste de colisão seja feito exatamente nesse instante */
	private int fp_lastCollisionTime;
	private byte lastPlayResult;

	private final Point3f lastCollisionDirection = new Point3f();

	/** caso a jogada seja replay, força o último resultado calculado */
	private boolean isReplay;	
	
	/** Posição inicial da bola na tela. */
	private int INITIAL_Y;
	
	private static Match match;
	
	/** Goleiro com o qual será testada a colisão. */
	private Keeper keeper;
	
	private final Mutex mutex = new Mutex();
	
	
	public Ball( Match match ) throws Exception {
		super( BALL_N_ITEMS );
		
		//#if JAR != "min"
			shadow = new Sprite(PATH_IMAGES + "shadow");
			shadow.defineReferencePixel( shadow.getWidth() >> 1, shadow.getHeight() );
			insertDrawable( shadow );
		//#endif
		sprite = new Sprite(PATH_IMAGES + "ball");
		sprite.defineReferencePixel( sprite.getWidth() >> 1, ( sprite.getHeight() * 14 ) >> 4 );
		insertDrawable( sprite );
		
		setVisible( false );
	}


	public final void setInitialY( int initialY ) {
		INITIAL_Y = initialY;
	}
	
	
	public final void reset( Keeper keeper ) {
		setVisible( true );
		
		setKeeper( keeper );

		sprite.setSequence( SEQUENCE_BIG );
		sprite.setRefPixelPosition( size.x >> 1, INITIAL_Y );
		
		//#if JAR != "min"
		shadow.setSequence( SEQUENCE_BIG );
		shadow.setRefPixelPosition( size.x >> 1, INITIAL_Y );
		//#endif

		checkedGoalCollision = false;
		realPosition.set( REAL_PENALTY_POSITION );

		realSpeed.set();
		state = BALL_STATE_STOPPED;
		fp_accTime = 0;
		fp_accTimeY = 0;	
		
		updateScreenPosition();
	}
	
	
	public final void setKeeper( Keeper keeper ) {
		this.keeper = keeper;
	}
	
	
	/**
	 * Recebe um ponteiro para uma jogada e define os novos valores da velocidade da bola.
	 * O valor retornado refere-se à posição final estimada da bola, quando esta cruza a 
	 * linha de fundo. Para o cálculo da posição final da bola, não são considerados possíveis
	 * obstáculos no caminho, como a barreira, goleiro ou jogadores.
	 * 
	 * @param p
	 * @param replay
	 * @return 
	 */
	public final Point3f kick( Play p, boolean replay ) {
		mutex.acquire();
		
		if ( p != null ) {
			isReplay = replay;
			if ( !replay ) {
				// adiciona uma imprecisão no chute (quanto mais forte, maior a chance de imprecisão)
				final int FP_POWER_PERCENT = NanoMath.divFixed( p.fp_power, Control.FP_CONTROL_POWER_MAX_VALUE );
				final int FP_RAND_DIR_MAX = NanoMath.mulFixed( FP_POWER_PERCENT, FP_BALL_MAX_DIRECTION_IMPRECISION );
				
				int fp_imprecision_max_diff = NanoMath.mulFixed( FP_RAND_DIR_MAX, Control.FP_CONTROL_DIRECTION_MAX_VALUE );
				int fp_imprecision_min_value = p.fp_direction - ( fp_imprecision_max_diff >> 1 );
				
				fp_imprecision_max_diff = NanoMath.mulFixed( FP_RAND_DIR_MAX, p.fp_direction < 0 ? -p.fp_direction : p.fp_direction );
				fp_imprecision_min_value = p.fp_direction - ( fp_imprecision_max_diff >> 1 );					
				p.fp_direction = fp_imprecision_min_value + NanoMath.randFixed( fp_imprecision_max_diff );
				
				final int FP_RAND_HEIGHT_MAX = NanoMath.mulFixed( FP_POWER_PERCENT, FP_BALL_MAX_HEIGHT_IMPRECISION );
				fp_imprecision_max_diff = NanoMath.mulFixed( FP_RAND_HEIGHT_MAX, Control.FP_CONTROL_HEIGHT_MAX_VALUE );
				fp_imprecision_min_value = p.fp_height - NanoMath.divFixed( fp_imprecision_max_diff, NanoMath.toFixed( 3 ) );					
				p.fp_height = fp_imprecision_min_value + NanoMath.randFixed( fp_imprecision_max_diff );
				
				fp_lastCollisionTime = 0;
				lastCollisionDirection.set();
			}
			checkedGoalCollision = false;
			realInitialPosition.set( realPosition );
			
			// obtém a distância no eixo x entre a posição da bola e a posição onde será chutada
			int fp_distanceX = p.fp_direction - realPosition.x;
			// o atributo "power" da jogada refere-se ao módulo da velocidade da bola. Os pontos da posição da bola,
			// seu ponto projetado no eixo x (ou seja, posição x,0,0) e o ponto do chute formam um triângulo retângulo; a
			// distância real percorrida pela bola é e hipotenusa desse triângulo, calculada pela fórmula 
			// h² = a² + b² (logo, a distância é a raiz quadrada da soma dos quadrados das distâncias X e Z).
			
			// não é necessário calcular o módulo da distância no modo pênalti, pois ela sempre será 11.0
//			final DOUBLE distanceModule = Math.sqrt( ( realPosition.x * realPosition.x ) + ( realPosition.z * realPosition.z ) );
			final int fp_distanceModule = FP_REAL_PENALTY_TO_GOAL;

			fp_timeToGoal = Math.abs( NanoMath.divFixed( fp_distanceModule, p.fp_power ) );


			// calcula as velocidades individuais dos eixos X, Y e Z
			realSpeed.x = NanoMath.divFixed( fp_distanceX, fp_timeToGoal );
			fp_realInitialYSpeed = NanoMath.divFixed( p.fp_height, fp_timeToGoal ) - NanoMath.mulFixed( NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, NanoMath.HALF ), fp_timeToGoal );
			realSpeed.z = NanoMath.divFixed( -realPosition.z, fp_timeToGoal );
			realSpeed.y = 0; // realSpeed.y não é de fato usado (é zerado aqui para que o cálculo do efeito seja correto)

			// calcula o valor do coeficiente de efeito
			final Point3f yVector = new Point3f( 0, NanoMath.ONE, 0 );
			curveVector.set( ( realSpeed.getNormal() ).cross( yVector ).mul( NanoMath.divFixed( NanoMath.mulFixed( NanoMath.mulFixed( p.fp_curve, FP_BALL_CURVE_FACTOR ), fp_distanceModule ), fp_timeToGoal ) ) );

			// tempo que a bola levará de fato para chegar ao gol, considerando-se a variação causada pela curva
			if ( curveVector.z != 0 ) {
				final int FP_2 = NanoMath.toFixed( 2 );
				fp_timeToGoal = NanoMath.divFixed( ( realSpeed.z + NanoMath.sqrtFixed( NanoMath.mulFixed( realSpeed.z, realSpeed.z ) - NanoMath.mulFixed( NanoMath.mulFixed( FP_2, curveVector.z ), realPosition.z ) ) ), -curveVector.z );
			}

			state = BALL_STATE_MOVING;
			fp_accTime = 0;
			fp_accTimeY = 0;

			final int FP_TIME_TO_GOAL_SQUARED = NanoMath.mulFixed( fp_timeToGoal, fp_timeToGoal );
			final Point3f destination = new Point3f( realInitialPosition.x + NanoMath.mulFixed( realSpeed.x, fp_timeToGoal ) + NanoMath.mulFixed( curveVector.x, FP_TIME_TO_GOAL_SQUARED ),
											 0,
											 realInitialPosition.z + NanoMath.mulFixed( realSpeed.z, fp_timeToGoal ) + NanoMath.mulFixed( NanoMath.mulFixed( curveVector.z, fp_timeToGoal ), FP_TIME_TO_GOAL_SQUARED ) );

			int fp_initialY = realInitialPosition.y;
			int fp_initialYSpeed = fp_realInitialYSpeed;
			int fp_time;
			int fp_totalTime = 0;
			
			final int FP_10 = NanoMath.toFixed( 10 );
			
			do {
				fp_time = fp_timeToGoal - fp_totalTime;
				destination.y = fp_initialY + NanoMath.mulFixed( fp_initialYSpeed, fp_time ) + NanoMath.mulFixed( NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_time ), fp_time );
				if ( destination.y < 0 ) {
					fp_totalTime -= NanoMath.divFixed( fp_initialYSpeed, FP_GRAVITY_ACCELERATION );
					fp_initialYSpeed *= NanoMath.mulFixed( fp_initialYSpeed, FP_BALL_BOUNCE_FACTOR );

					// teste para evitar excesso de loops quando a bola é chutada rasteira
					if ( fp_initialYSpeed < NanoMath.mulFixed( FP_REAL_MIN_Y_SPEED, FP_10 ) ) {
						destination.y = 0;
						break;
					}
				}
			} while ( fp_time > 0 && destination.y < 0 );
			
			//#if DEBUG == "true"
			System.out.println( "DESTINO: " + NanoMath.toString( destination.x ) + ", " + NanoMath.toString( destination.y ) + ", " + NanoMath.toString( destination.z ) );
			System.out.println( "JOGADA: " + NanoMath.toString( p.fp_direction ) + ", " + NanoMath.toString( p.fp_height ) + ", " + NanoMath.toString( p.fp_curve ) + ", " + NanoMath.toString( p.fp_power ) );
			System.out.println( "TEMPO: " + NanoMath.toString( fp_timeToGoal ) );
			//#endif

			mutex.release();
			return destination;
		} // fim if ( p )

		mutex.release();
		return null;		
	} // fim do método kick( Play, boolean )
	
	
	 /**
	  * Atualiza o estado da bola após "delta" milisegundos.
	  */
	public final void update( int delta ) {
		mutex.acquire();
		
		if ( state != BALL_STATE_STOPPED ) {
			delta = match.getDelta();
			
			// converte o tempo de milisegundos para segundos
			int fp_partialTime = NanoMath.divInt( delta, 1000 );

			if ( isReplay && !checkedGoalCollision ) {
				if ( fp_accTime + fp_partialTime > fp_lastCollisionTime )
					fp_partialTime = fp_lastCollisionTime - fp_accTime;
			}

			fp_accTime += fp_partialTime;
			fp_accTimeY += fp_partialTime;

			// garante que teste de colisão seja feito exatamente na linha do gol
			if ( !checkedGoalCollision ) {
				if ( realPosition.z <= 0 ) {
					//lastCollisionTime = accTime;
					checkedGoalCollision = true;
				} else if ( fp_accTime >= fp_timeToGoal ) {
					fp_accTime = fp_timeToGoal;
					checkedGoalCollision = true;
				}
			}

			realPosition.z = realInitialPosition.z + NanoMath.mulFixed( realSpeed.z, fp_accTime );
			realPosition.x = realInitialPosition.x + NanoMath.mulFixed( realSpeed.x, fp_accTime );
			// y = y0 + v0*t + a/2*t*t
			// verifica se a bola quicou no chão
			realSpeed.y = fp_realInitialYSpeed + NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accTimeY );
			if ( realSpeed.y < 0 && realPosition.y <= 0 ) {
				fp_accTimeY = 0;
				realInitialPosition.y = 0;
				realPosition.y = 0;

				fp_realInitialYSpeed = Math.abs( fp_realInitialYSpeed );
				realSpeed.y = Math.abs( realSpeed.y );
				fp_realInitialYSpeed = NanoMath.mulFixed( Math.max( fp_realInitialYSpeed, realSpeed.y ), FP_BALL_BOUNCE_FACTOR );
			}
			
			realPosition.y = realInitialPosition.y + NanoMath.mulFixed( fp_realInitialYSpeed, fp_accTimeY ) + NanoMath.mulFixed( NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accTimeY ), fp_accTimeY );
			if ( realPosition.y < 0 )
				realPosition.y = 0;

			int fp_mid = NanoMath.mulFixed( realSpeed.x + realSpeed.z, NanoMath.HALF );
			fp_accSpinTime += fp_partialTime;
			if ( Math.abs( NanoMath.mulFixed( fp_mid, fp_accSpinTime ) ) >= ( realPosition.y <= FP_REAL_BALL_RAY ? FP_SPIN_DISTANCE_GROUND : FP_SPIN_DISTANCE_AIR ) ) {
				fp_accSpinTime = 0;
				
				// gira a bola
				if ( fp_mid >= 0 )
					sprite.previousFrame();
				else
					sprite.nextFrame();
			}

			// variação na posição causada pelo efeito da bola
			realPosition.addEquals( curveVector.mul( NanoMath.mulFixed( fp_accTime, fp_accTime ) ) );
			
			switch ( state ) {
				case BALL_STATE_MOVING:
					if ( realPosition.z <= 0 || fp_accTime == fp_timeToGoal ) { 
						// bola chegou na linha de fundo; primeiro verifica a colisão com o goleiro
						checkPlayerCollision( COLLISION_KEEPER );
						
						// se não houver alteração no estado da bola, é porque goleiro não defendeu
						if ( state == BALL_STATE_MOVING )
							state = checkGoalCollision();
						
						switch ( state ) {
							case BALL_STATE_POST_BACK:
							case BALL_STATE_POST_GOAL:
							case BALL_STATE_POST_OUT:
								MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
								MediaPlayer.play( SOUND_INDEX_POST );
							break;
							
							case BALL_STATE_REJECTED_KEEPER:
								MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							break;
						}
						
						realLastGoalPosition.set( realPosition );
					}
				break;
				
				case BALL_STATE_GOAL:
				case BALL_STATE_POST_GOAL:
					reduceBallSpeed( fp_partialTime );
					keepBallInsideGoal();
				break;
				
				case BALL_STATE_OUT:
				case BALL_STATE_POST_OUT:
					reduceBallSpeed( fp_partialTime );
					handleBallOut();
				break;
				
				case BALL_STATE_POST_BACK:
				case BALL_STATE_REJECTED_KEEPER:
				case BALL_STATE_REJECTED_BARRIER:
					// a bola desacelera após ter colidido com alguma coisa ou ser chutada para fora
					reduceBallSpeed( fp_partialTime );
				break;
			}

			// converte as coordenadas reais da bola para uma posição da tela
			updateScreenPosition();
	   } // fim if ( state != BALL_STATE_STOPPED )
		
		mutex.release();
	} // fim do método update()
	
	
	public final byte checkPlayerCollision( byte type  ) {
		if ( keeper != null ) {
			// caso seja replay e o instante de tempo atual seja inferior ao instante da
			// definição da última jogada, não colide
			if ( isReplay ) {
				if ( fp_accTime < fp_lastCollisionTime ) {
					return state;
				} else {
					realInitialPosition.set( realPosition );
					fp_accTime = 0;
					fp_accTimeY = 0;
					curveVector.set();
					realSpeed.set( lastCollisionDirection );
					fp_realInitialYSpeed = realSpeed.y;
					state = lastPlayResult;

					return lastPlayResult;
				} // fim else ( accTime >= lastCollisionTime )
			} // fim if ( isReplay )

			final Quad playerArea = keeper.getCollisionArea();
			int fp_distanceToPlayer = playerArea.distanceTo( realPosition );

			if ( fp_distanceToPlayer <= FP_REAL_BALL_RAY && fp_distanceToPlayer >= NanoMath.toFixed( -3 ) ) {
				// colidiu com o plano do quad do jogador
				Point3f collisionPosition = realPosition;
				if ( type == COLLISION_KEEPER )
					collisionPosition.z = 0;

				// cosTheta = ( a * b ) / ( modA * modB )
				// como modB == modNormal == 1:
				realSpeed.y = 0;
				int fp_cosTheta = realSpeed.getNormal().dot( playerArea.normal );

				// realSpeed = orthoSpeed + normalSpeed;
				// vetor da velocidade ortogonal ao plano de contato do jogador
				final Point3f orthoSpeed = new Point3f( realSpeed );
				orthoSpeed.mulEquals( fp_cosTheta );

				// obtém a velocidade com que a bola se aproxima do plano
				final Point3f normalSpeed = new Point3f( realSpeed );
				normalSpeed.subEquals( orthoSpeed );
				// calcula o tempo que a bola deve retroceder para que o teste de colisão seja feito
				// com exatamente um ponto da bola tocando o plano, ou seja, a bola não invade o plano
				int fp_timeBack = NanoMath.divFixed( fp_distanceToPlayer + FP_REAL_BALL_RAY, normalSpeed.getModule() );

				// retrocede a bola no tempo. obs.: para facilitar os cálculos, considera-se que a
				// variação na direção da bola causada pelo efeito da bola nesse curto intervalo
				// de tempo é desprezível, ou seja, a bola descreve uma trajetória retílinea.
				realPosition.subEquals( normalSpeed.mul( fp_timeBack ) );

				// agora é possível obter o ponto exato da colisão com o plano, e verificar se ele
				// está dentro dos limites de largura/altura do quad.
				// normaliza o vetor da velocidade normal
				normalSpeed.normalize();

				if ( playerArea.isInsideQuad( collisionPosition, NanoMath.mulFixed( FP_REAL_BALL_RAY, NanoMath.HALF ) ) ) {
					// colidiu de fato com o jogador

					realPosition.set( collisionPosition );
					realInitialPosition.set( collisionPosition );
					curveVector.set(); // não há efeito na bola após a colisão
					
					final int FP_140_PERCENT = NanoMath.divInt( 140, 100 );
					
					realSpeed.z = NanoMath.mulFixed( NanoMath.mulFixed( realSpeed.z, FP_REAL_ELASTIC_FACTOR ), FP_140_PERCENT - Math.abs( fp_cosTheta ) );
					if ( realSpeed.z < 0 )
						realSpeed.z = -realSpeed.z;

					if ( type == COLLISION_BARRIER ) {
						realSpeed.x = NanoMath.mulFixed( NanoMath.mulFixed( realSpeed.x, FP_REAL_ELASTIC_FACTOR ), fp_cosTheta );
					} else {
						// caso tenha colidido com goleiro, velocidade x é semi-aleatória
						// (há mais possibilidade da bola manter a direção atual)
						realSpeed.x = NanoMath.mulFixed( realSpeed.x, Math.abs( NanoMath.mulFixed( NanoMath.mulFixed( FP_REAL_ELASTIC_FACTOR, fp_cosTheta ), NanoMath.mulFixed( NanoMath.randFixed(), FP_140_PERCENT  ) ) ) );
					}

					// obs.: esse teste é necessário devido a um bug onde o teste de colisão com o 
					// goleiro era executado mais de uma vez, e com isso no replay o tempo até o gol era 0.0
					if ( fp_accTime > 0 ) {
						fp_lastCollisionTime = fp_accTime;
					}

					// direção vertical da bola é semi-aleatória após impacto
					fp_realInitialYSpeed = realSpeed.y = FP_AFTER_COLLISION_MIN_Y + NanoMath.randFixed( FP_AFTER_COLLISION_MAX_Y );
					
					lastCollisionDirection.set( realSpeed );

					fp_accTime = 0;
					fp_accTimeY = 0;

					if ( type == COLLISION_BARRIER )
						state = lastPlayResult = BALL_STATE_REJECTED_BARRIER;
					else
						state = lastPlayResult = BALL_STATE_REJECTED_KEEPER;
					checkedGoalCollision = true;
				} // fim if ( playerArea.isInsideQuad( collisionPosition, REAL_BALL_RAY * 0.5 ) )
			} // fim if ( distanceToPlayer <= REAL_BALL_RAY && distanceToPlayer >= -3.0 )
		} // fim if ( keeper != null )

		// se não for passado um jogador para testar colisão com a bola, apenas retorna seu estado atual
		return state;		
	}


	// métodos  get e set (maioria dos métodos set são usados somente no caso de carregar replay gravado)
	public final byte getState() {
		return state;
	}


	public final byte getLastPlayResult() {
		return lastPlayResult;
	}


	public final Point3f getRealPosition() {
		return realPosition;
	}


	public final void setRealPosition( Point3f realPosition ) {
		this.realPosition.set( realPosition );
	}


	 public final void updateScreenPosition() {
		if ( realPosition.z < FP_BALL_PENALTY_FRAME_3_Z ) {
			if ( sprite.getSequenceIndex() != SEQUENCE_VERY_SMALL )
				sprite.setSequence( SEQUENCE_VERY_SMALL );
			
			//#if JAR != "min"
			shadow.setSequence( SEQUENCE_VERY_SMALL );
			//#endif
		} else if ( realPosition.z < FP_BALL_PENALTY_FRAME_2_Z ) {
			if ( sprite.getSequenceIndex() != SEQUENCE_SMALL )
				sprite.setSequence( SEQUENCE_SMALL );

			//#if JAR != "min"
			if ( realPosition.y < FP_BALL_MAX_Y_SHADOW_NORMAL )
				shadow.setSequence( SEQUENCE_SMALL );
			else
				shadow.setSequence( SEQUENCE_VERY_SMALL );
			//#endif
		} else if ( realPosition.z < FP_BALL_PENALTY_FRAME_1_Z ) {
			if ( sprite.getSequenceIndex() != SEQUENCE_MEDIUM )
				sprite.setSequence( SEQUENCE_MEDIUM );

			//#if JAR != "min"
			if ( realPosition.y < FP_BALL_MAX_Y_SHADOW_NORMAL )
				shadow.setSequence( SEQUENCE_MEDIUM );
			else if ( realPosition.y < FP_BALL_MAX_Y_SHADOW_SMALLER_1 )
				shadow.setSequence( SEQUENCE_SMALL );
			else
				shadow.setSequence( SEQUENCE_VERY_SMALL );
			//#endif
		} else {
			if ( sprite.getSequenceIndex() != SEQUENCE_BIG )
				sprite.setSequence( SEQUENCE_BIG );
			
			//#if JAR != "min"
			if ( realPosition.y < FP_BALL_MAX_Y_SHADOW_NORMAL )
				shadow.setSequence( SEQUENCE_BIG );
			else if ( realPosition.y < FP_BALL_MAX_Y_SHADOW_SMALLER_1 )
				shadow.setSequence( SEQUENCE_MEDIUM );
			else if ( realPosition.y < FP_BALL_MAX_Y_SHADOW_SMALLER_2 )
				shadow.setSequence( SEQUENCE_SMALL );
			else
				shadow.setSequence( SEQUENCE_VERY_SMALL );
			//#endif
		}

		// define nova posição
		final int xOffset = ScreenManager.SCREEN_HALF_WIDTH + ( NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_GOAL_WIDTH, NanoMath.divFixed( realPosition.x, FP_REAL_GOAL_WIDTH ) ) ) );
		final int yOffsetY = NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_FLOOR_TO_BAR, NanoMath.divFixed( realPosition.y, FP_REAL_FLOOR_TO_BAR ) ) );
		final int yOffsetZ = NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_MARK_TO_GOAL, NanoMath.divFixed( FP_REAL_PENALTY_TO_GOAL - Math.max( realPosition.z, FP_REAL_GOAL_DEPTH ), FP_REAL_PENALTY_TO_GOAL ) ) );
		
		sprite.setRefPixelPosition( xOffset, INITIAL_Y - yOffsetY - yOffsetZ );
		
		//#if JAR != "min"
		shadow.setRefPixelPosition( xOffset, INITIAL_Y - yOffsetZ );
		//#endif
	} // fim do método updatePosition()

	 
	/**
	 * Evita que a bola "fure" a rede após um gol.
	 */
	public final void keepBallInsideGoal() {
		if ( realPosition.z < FP_REAL_PENALTY_GOAL_DEPTH )
			realPosition.z = FP_REAL_PENALTY_GOAL_DEPTH;

		if ( realPosition.x < FP_REAL_LEFT_POST_START )
			realPosition.x = FP_REAL_LEFT_POST_START;
		else if ( realPosition.x > FP_REAL_RIGHT_POST_START )
			realPosition.x = FP_REAL_RIGHT_POST_START;

		if ( realPosition.y >= FP_REAL_BAR_BOTTOM ) {
			realPosition.y = realInitialPosition.y = FP_REAL_BAR_BOTTOM;
		}
	} // fim do método keepBallInsideGoal()	
	
	
	/**
	 * Trata a movimentação da bola após ela ir para fora, de forma que colida com as placas de publicidade, mas não 
	 * com a torcida.
	 */
	 public final void handleBallOut() {
		if ( realPosition.z <= FP_PENALTY_BALL_Z_LIMIT && realSpeed.z < 0 ) {
			if ( realPosition.y >= FP_PENALTY_BALL_Y_LIMIT ) {
				setVisible( false );
			} else {
				// bola bateu nas placas de publicidade
				if ( !MediaPlayer.isPlaying() )
					MediaPlayer.play( SOUND_INDEX_HIT_BOARD  );
				
				realSpeed.z = NanoMath.mulFixed( realSpeed.z, NanoMath.divInt( -10, 100 ) );
				realSpeed.x = NanoMath.mulFixed( realSpeed.x, NanoMath.divInt( 80, 100 ) );
				fp_accTime = 0;
				realInitialPosition.set( realPosition );
			}
		}
	} // fim do método handleBallOut()

	
	/**
	 * Realiza os testes de colisão com as traves e retorna o novo estado da bola, que no fundo é o resultado final da 
	 * jogada (gol, fora, trave, etc).
	 * 
	 * @return 
	 */
	public final byte checkGoalCollision() {
		// caso seja replay e o instante de tempo atual seja inferior ao instante da
		// definição da última jogada, não colide
		if ( isReplay ) {
			if ( fp_accTime < fp_lastCollisionTime ) {
				return state;
			} else {
				realPosition.set( realLastGoalPosition );
				realInitialPosition.set( realLastGoalPosition );
				fp_accTime = 0;
				fp_accTimeY = 0;
				realSpeed.set( lastCollisionDirection );
				curveVector.set();
				fp_realInitialYSpeed = realSpeed.y;
				
				return lastPlayResult;
			}
		}

		final int FP_2 = NanoMath.toFixed( 2 );

		// testa se a bola entrou no gol ou colidiu com alguma trave
		if ( realPosition.z <= FP_2 && realPosition.y <= FP_REAL_BAR_TOP && realPosition.x >= FP_REAL_LEFT_POST_END && realPosition.x <= FP_REAL_RIGHT_POST_END ) {
			if ( realPosition.y >= FP_REAL_BAR_BOTTOM ) {
				// bateu no travessão
				lastPlayResult = handlePostCollision( POST_BAR );
				
				//#if DEBUG == "true"
				System.out.println( "TRAVESSÃO!" );
				//#endif				
				
				return lastPlayResult;
			} else {
				// bola abaixo do travessão. testa colisão com traves.

				if ( realPosition.x <= FP_REAL_LEFT_POST_START ) { 
					// bateu na trave esquerda
					lastPlayResult = handlePostCollision( POST_LEFT );
					
					//#if DEBUG == "true"
					System.out.println( "TRAVE ESQUERDA!" );
					//#endif					
					
					return lastPlayResult;
				}
				else if ( realPosition.x >= FP_REAL_RIGHT_POST_START ) { 
					// bateu na trave direita
					lastPlayResult = handlePostCollision( POST_RIGHT );
					
					//#if DEBUG == "true"
					System.out.println( "TRAVE DIREITA!" );
					//#endif					
					
					return lastPlayResult;
				} else {
					realInitialPosition.x = realPosition.x;
					realInitialPosition.z = realPosition.z;
					// atualiza a direção da bola como a última soma da velocidade com o efeito
					realSpeed.addEquals( curveVector.mul( NanoMath.mulFixed( fp_accTime, FP_2 ) ) );

					realSpeed.y = fp_realInitialYSpeed + NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, NanoMath.mulFixed( fp_accTimeY, FP_2 ) );

					lastCollisionDirection.set( realSpeed );

					curveVector.set();

					fp_lastCollisionTime = fp_accTime;
					fp_accTime = 0;
					lastPlayResult = BALL_STATE_GOAL;
					checkedGoalCollision = true;
					
					//#if DEBUG == "true"
					System.out.println( "GOL!" );
					//#endif					
					
					return BALL_STATE_GOAL; // GOL!
				}
			} // fim else if (realPosition.Y >= R_FLOOR_TO_BAR)
		} else {
			realInitialPosition.x = realPosition.x;
			realInitialPosition.z = realPosition.z;
			// atualiza a direção da bola como a última soma da velocidade com o efeito
			realSpeed.addEquals( curveVector.mul( NanoMath.mulFixed( fp_accTime, FP_2 ) ) );

			realSpeed.y = fp_realInitialYSpeed + NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, NanoMath.mulFixed( fp_accTimeY, FP_2 ) );

			lastCollisionDirection.set( realSpeed );

			curveVector.set();
			fp_lastCollisionTime = fp_accTime;
			fp_accTime = 0;

			lastPlayResult = BALL_STATE_OUT;
			
			//#if DEBUG == "true"
			System.out.println( "FORA!" );
			//#endif

			return BALL_STATE_OUT; // fora!
		}
	} // fim do método checkGoalCollision()
	
	
	/**
	 * Trata a colisão com as traves.
	 * 
	 * @param postIndex 
	 * @return
	 */
	 public final byte handlePostCollision( byte postIndex ) {
		// armazena a velocidade antes da colisão
		final Point3f beforeCollisionSpeed = realSpeed;

		// define a posição global da trave com a qual a bola colidiu
		final Point3f postPosition = new Point3f();
		
		switch ( postIndex ) {
			case POST_LEFT:
				postPosition.set( FP_REAL_LEFT_POST_MIDDLE, realPosition.y );
			break;
			
			case POST_RIGHT:
				postPosition.set( FP_REAL_RIGHT_POST_MIDDLE, realPosition.y );
			break;
			
			case POST_BAR:
				postPosition.set( realPosition.x, FP_REAL_BAR_MIDDLE );
			break;
		} // fim switch ( postIndex )

		// atualiza a velocidade instantânea, de acordo com a variação causada pelo efeito
		realSpeed.addEquals( curveVector.mul( fp_accTime ) );
		// obtém a velocidade normalizada
		// no site: N
		final Point3f normalSpeed = realSpeed.getNormal();

		// 1. POSICIONA BOLA NO PONTO DE CONTATO
		// ADAPTADO DE: http://www.gamasutra.com/features/20020118/vandenhuevel_02.htm
		// obtém distância inicial do centro da bola ao centro da trave
		final Point3f distance = new Point3f( postPosition );
		distance.subEquals( realPosition );
		// no site: C
		int fp_distanceModule = distance.getModule();
		final Point3f normalDistance = distance.getNormal();
		// cálculo da projeção de um vetor sobre outro: proj(B,A) = ( (A.B)/|A|² ) * A
		final Point3f projectionV_D = realSpeed.mul( NanoMath.divFixed( realSpeed.dot( distance ), NanoMath.mulFixed( realSpeed.getModule(), realSpeed.getModule() ) ) );	
		// no site: D
		int fp_projectionModule = projectionV_D.getModule();
		
		// não há necessidade de se extrair a raiz de F, pois seu valor só é utilizado ao quadrado
		// distanceModule² = projectionModule² + F²; logo:
		// no site: F
		int fp_F_squared = NanoMath.mulFixed( fp_distanceModule, fp_distanceModule ) - NanoMath.mulFixed( fp_projectionModule, fp_projectionModule );

		// o módulo da hipotenusa do triângulo retângulo final, formado entre os pontos central da bola e da trave 
		// e o ponto ortogonal ao centro da trave no vetor velocidade, é a soma dos raios da trave e da bola. Seu
		// valor só é utilizado ao quadrado, logo o quadrado é calculado de uma vez.
		// no site: sumRadiiSquared
		int fp_sumRadiiSquared = NanoMath.mulFixed( FP_REAL_MAX_TOUCH_DISTANCE, FP_REAL_MAX_TOUCH_DISTANCE );
		
		// encontra o módulo da distância que a bola deve retroceder no vetor da velocidade, até chegar ao
		// ponto exato de contato
		// no site: T
		int fp_T = fp_sumRadiiSquared - fp_F_squared;
		// no site: distance
		int fp_backDistance = fp_projectionModule - NanoMath.sqrtFixed( fp_T );

		// finalmente, posiciona a bola no ponto correto de contato
		realPosition.addEquals( normalSpeed.mul( fp_backDistance ) );
		distance.set( postPosition.sub( realPosition ) );

		// 2. CALCULA DIREÇÃO E VELOCIDADE DA BOLA APÓS A COLISÃO
		// recalcula a projeção do vetor velocidade sobre o vetor distância entre os centros da bola e da trave.
		// Aqui os vetores são normalizados, pois o importante é o ângulo formado entre eles. O quadrado da
		// projeção é usado para descobrir o ângulo formado entre esses dois vetores:
		// 0:	ortogonal (90º)
		// 0.5:	diagonal  (45º)
		// 1.0: paralelo  (0º)
		normalDistance.set( distance.getNormal() );

		projectionV_D.set( normalSpeed.mul( NanoMath.divFixed( normalSpeed.dot( normalDistance ), NanoMath.mulFixed( normalSpeed.getModule(), normalSpeed.getModule() ) ) ) );	
		fp_projectionModule = projectionV_D.getModule();
		fp_projectionModule = NanoMath.mulFixed( fp_projectionModule, fp_projectionModule );

		// observação: o valor da projeção é reduzido aqui para aumentar a incidência de bolas que batem na trave e entram no gol
		fp_projectionModule -= NanoMath.divInt( 14, 100 );
		if ( fp_projectionModule < 0 )
			fp_projectionModule = 0;

		realSpeed.y = NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accTimeY ); // v = v0 + at;

		byte returnValue = BALL_STATE_POST_GOAL;
		int fp_realSpeedModule = realSpeed.getModule();
		// reajusta a quantidade de movimento após a colisão. O cálculo é feito com base no fato de que,
		// quanto mais frontal à trave for o chute, maior a perda de energia. Ou seja, bolas que apenas
		// "raspam" a trave perdem pouca energia.
		fp_realSpeedModule -= NanoMath.mulFixed( fp_projectionModule, NanoMath.mulFixed( NanoMath.ONE - Math.abs( FP_REAL_ELASTIC_FACTOR ), fp_realSpeedModule ) );
		realSpeed.set( realSpeed.getNormal().mul( fp_realSpeedModule ) );
		
		final int FP_2 = NanoMath.toFixed( 2 );
		
		// o cálculo da variação em z é igual para todas as traves; o que muda é o cálculo no outro eixo (x no
		// caso das traves e y no caso do travessão)
		realSpeed.z += NanoMath.mulFixed( NanoMath.mulFixed( FP_2, fp_projectionModule ), fp_realSpeedModule );
		switch ( postIndex ) {
			case POST_LEFT:
				if ( realPosition.x > postPosition.x )
					realSpeed.x = Math.abs( realSpeed.x + NanoMath.mulFixed( NanoMath.ONE - NanoMath.mulFixed( FP_2, Math.abs( NanoMath.HALF - fp_projectionModule ) ), fp_realSpeedModule ) );
				else
					realSpeed.x = -Math.abs( realSpeed.x - NanoMath.mulFixed( NanoMath.ONE - NanoMath.mulFixed( FP_2, Math.abs( NanoMath.HALF - fp_projectionModule ) ), fp_realSpeedModule ) );

				if ( realSpeed.z > 0 ) {
					returnValue = BALL_STATE_POST_BACK;
				} else {
					// velocidade z menor ou igual a zero; bola foi para fora ou para o gol
					if ( realPosition.x > postPosition.x )
						returnValue = BALL_STATE_POST_GOAL;
					else
						returnValue = BALL_STATE_POST_OUT;
				}
			break; // fim case POST_LEFT
			
			case POST_RIGHT:
				if ( realPosition.x < postPosition.x )
					realSpeed.x = -Math.abs( realSpeed.x - NanoMath.mulFixed( NanoMath.ONE - NanoMath.mulFixed( FP_2, Math.abs( NanoMath.HALF - fp_projectionModule ) ), fp_realSpeedModule ) );
				else
					realSpeed.x = Math.abs( realSpeed.x + NanoMath.mulFixed( NanoMath.ONE - NanoMath.mulFixed( FP_2, Math.abs( NanoMath.HALF - fp_projectionModule ) ), fp_realSpeedModule ) );

				if ( realSpeed.z > 0 ) {
					returnValue = BALL_STATE_POST_BACK;
				} else {
					// velocidade z menor ou igual a zero; bola foi para fora ou para o gol
					if ( realPosition.x < postPosition.x )
						returnValue = BALL_STATE_POST_GOAL;
					else
						returnValue = BALL_STATE_POST_OUT;
				}
			break; // fim case POST_RIGHT
			
			case POST_BAR:
			default:
				final int FP_70_PERCENT = NanoMath.divInt( 70, 100 );
				final int FP_120_PERCENT = NanoMath.divInt( 120, 100 );
				
				if ( realPosition.y < postPosition.y )
					realSpeed.y = -Math.abs( realSpeed.y - NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.ONE - NanoMath.mulFixed( FP_2, Math.abs( NanoMath.HALF - fp_projectionModule ) ), fp_realSpeedModule ), FP_70_PERCENT ) );
				else
					realSpeed.y = Math.abs( realSpeed.y + NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.ONE - NanoMath.mulFixed( FP_2, Math.abs( NanoMath.HALF - fp_projectionModule ) ), fp_realSpeedModule ), FP_120_PERCENT ) );

				if ( realSpeed.z > 0 ) {
					returnValue = BALL_STATE_POST_BACK;
				} else {
					// velocidade z menor ou igual a zero; bola foi para fora ou para o gol
					if ( realPosition.y < postPosition.y )
						returnValue = BALL_STATE_POST_GOAL;
					else
						returnValue = BALL_STATE_POST_OUT;
				}		
			break;
		}

		// evita que a bola ganhe velocidade após a colisão
		int fp_beforeCollisionModule = beforeCollisionSpeed.getModule();
		if ( realSpeed.getModule() > fp_beforeCollisionModule ) {
			realSpeed.normalize();
			realSpeed.mulEquals( fp_beforeCollisionModule );
			// pega o maior "culpado" pelo módulo da velocidade e o reduz
			if ( Math.abs( realSpeed.x ) > Math.abs( realSpeed.z ) ) {
				if ( Math.abs( realSpeed.y ) > Math.abs( realSpeed.x ) )
					realSpeed.y = NanoMath.mulFixed( realSpeed.y, Math.abs( FP_REAL_ELASTIC_FACTOR ) );
				else
					realSpeed.x = NanoMath.mulFixed( realSpeed.x, Math.abs( FP_REAL_ELASTIC_FACTOR ) );
			} else {
				if ( Math.abs( realSpeed.y ) > Math.abs( realSpeed.z ) )
					realSpeed.y = NanoMath.mulFixed( realSpeed.y, Math.abs( FP_REAL_ELASTIC_FACTOR ) );
				else
					realSpeed.z = NanoMath.mulFixed( realSpeed.z, Math.abs( FP_REAL_ELASTIC_FACTOR ) );
			}
		}

		checkedGoalCollision = true;
		// não há mais efeito na bola após colisão com a trave
		curveVector.set(); 

		lastCollisionDirection.set( realSpeed );
		fp_lastCollisionTime = fp_accTime;
		fp_accTime = 0;
		fp_accTimeY = 0;

		fp_realInitialYSpeed = realSpeed.y;

		// posição inicial para cálculo da posição da bola passa a ser o ponto de colisão com a trave
		realInitialPosition.set( realPosition );

		return returnValue;
	} // fim do método handlePostCollision()
	 
	 
//	 private final void print( String s, Point3f xyz ) {
//		 System.out.println( s + NanoMath.toString( xyz.x ) + ", " + NanoMath.toString( xyz.y ) + ", " + NanoMath.toString( xyz.z ) );
//	 }
//	 
//	 private final void print( String s, int f ) {
//		 System.out.println( s + NanoMath.toString( f ) );
//	 }

	
	/**
	 * Desacelera a bola (usado após alguma colisão).
	 * 
	 * @param time 
	 */
	 public final void reduceBallSpeed( int time ) {
		realInitialPosition.x = realPosition.x;
		realInitialPosition.z = realPosition.z;

		fp_accTime = 0;
		realSpeed.subEquals( realSpeed.mul( NanoMath.mulFixed( FP_BALL_SLOW_DOWN_FACTOR, time ) ) );
	} // fim do método reduceBallSpeed()
	 
	 
	 public final int getSpriteRefPixelX() {
		 return sprite.getRefPixelX();
	 }
	 
	 
	 public final int getSpriteRefPixelY() {
		 return sprite.getRefPixelY();
	 }
	 
	 
	 public final int getTimeToGoal() {
		 return fp_timeToGoal;
	 }


	//#if JAR != "min"
	public final void saveReplayData( Replay replay ) {
		replay.playResult = lastPlayResult;
		replay.collisionTime = fp_lastCollisionTime;
		replay.collisionDirection.set( lastCollisionDirection );
		replay.ballEndLinePosition.set( realLastGoalPosition );
	}


	public final void loadReplayData( Replay replay ) {
		lastPlayResult = replay.playResult;
		fp_lastCollisionTime = replay.collisionTime;
		lastCollisionDirection.set( replay.collisionDirection );		
		realLastGoalPosition.set( replay.ballEndLinePosition );
	}
	//#endif
	
	
	public static final void loadImages() throws Exception {
		String path = PATH_IMAGES + "ball";
		sprite = new Sprite( PATH_IMAGES + "ball" );

		//#if JAR != "min"
		path = PATH_IMAGES + "shadow";
		shadow = new Sprite( path + ".bin", path );
		//#endif
	}
	
	
	public static final void setMatch( Match m ) {
		match = m;
	}

}
