/**
 * Replay.java
 * ©2008 Nano Games.
 *
 * Created on Apr 3, 2008 4:38:37 PM.
 */

//#if JAR != "min"

package core;

import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 * 
 * @author Peter
 */
public final class Replay implements Constants, Serializable {
	
	/** Momento da gravação do replay. */
	public long saveTime;
	
	/** Índice do time do batedor. */
	public byte shooterTeamIndex;
	
	/** Índice do batedor no time. */
	public byte shooterPlayerIndex;
	
	/** Índice do time do goleiro. */
	public byte keeperTeamIndex;

	/** Jogada realizada pelo batedor. */
	public final Play shooterPlay = new Play();

	/** Jogada realizada pelo goleiro. */
	public final Play keeperPlay = new Play();
	
	/** Indica se o goleiro escolheu o canto errado e desistiu do pulo na jogada. */
	public boolean keeperGaveUp;
	
	/** Resultado da jogada. */
	public byte playResult = BALL_STATE_NONE;
	
	/** Direção de movimentação da bola após a colisão. */
	public final Point3f collisionDirection = new Point3f();
	
	/** Posição exata da bola no momento da "colisão" com a linha de fundo. */
	public final Point3f ballEndLinePosition = new Point3f();
	
	/** Instante em que houve a definição do resultado da jogada. */
	public int collisionTime;
	
	
	public final void write( DataOutputStream output ) throws Exception {
		// resultado da jogada
		output.writeByte( playResult );
		
		if ( playResult != BALL_STATE_NONE ) {
			// momento da gravação do replay
			output.writeLong( saveTime );
			
			// índices dos times do batedor e do goleiro
			output.writeByte( shooterTeamIndex );
			output.writeByte( shooterPlayerIndex );
			output.writeByte( keeperTeamIndex );
			
			// jogada do batedor
			output.writeInt( shooterPlay.fp_direction  );
			output.writeInt( shooterPlay.fp_height  );
			output.writeInt( shooterPlay.fp_curve  );
			output.writeInt( shooterPlay.fp_power  );
			
			// jogada do goleiro
			output.writeInt( keeperPlay.fp_direction  );
			output.writeInt( keeperPlay.fp_height  );
			output.writeInt( keeperPlay.fp_curve  );
			output.writeInt( keeperPlay.fp_power  );
			output.writeBoolean( keeperGaveUp );
			
			// momento da definição do resultado da jogada
			output.writeInt( collisionTime );
			
			// direção da bola imediatamente após a definição da jogada
			output.writeInt( collisionDirection.x );
			output.writeInt( collisionDirection.y );
			output.writeInt( collisionDirection.z );
			
			// posição da bola no momento da definição da jogada
			output.writeInt( ballEndLinePosition.x );
			output.writeInt( ballEndLinePosition.y );
			output.writeInt( ballEndLinePosition.z );
		} // fim if ( saved )
	}


	public final void read( DataInputStream input ) throws Exception {
		// resultado da jogada
		playResult = input.readByte();		
		
		if ( playResult != BALL_STATE_NONE ) {
			// momento da gravação do replay
			saveTime = input.readLong();
			
			// índices dos times do batedor e do goleiro
			shooterTeamIndex = input.readByte();
			shooterPlayerIndex = input.readByte();
			keeperTeamIndex = input.readByte();
			
			// jogada do batedor
			shooterPlay.fp_direction = input.readInt();
			shooterPlay.fp_height = input.readInt();
			shooterPlay.fp_curve = input.readInt();
			shooterPlay.fp_power = input.readInt();
			
			// jogada do goleiro
			keeperPlay.fp_direction = input.readInt();
			keeperPlay.fp_height = input.readInt();
			keeperPlay.fp_curve = input.readInt();
			keeperPlay.fp_power = input.readInt();
			keeperGaveUp = input.readBoolean();
			
			// momento da definição do resultado da jogada
			collisionTime = input.readInt();
			
			// direção da bola imediatamente após a definição da jogada
			collisionDirection.x = input.readInt();
			collisionDirection.y = input.readInt();
			collisionDirection.z = input.readInt();
			
			// posição da bola no momento da definição da jogada
			ballEndLinePosition.x = input.readInt();
			ballEndLinePosition.y = input.readInt();
			ballEndLinePosition.z = input.readInt();
			
		} // fim if ( saved )
	}	

}

//#endif