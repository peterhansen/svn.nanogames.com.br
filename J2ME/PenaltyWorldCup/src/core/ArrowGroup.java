/**
 * ArrowGroup.java
 *
 * Created on Jun 3, 2010 7:41:56 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author peter
 */
public final class ArrowGroup extends DrawableGroup implements Constants, Updatable {

	protected final DrawableImage arrowRight;
	protected final DrawableImage arrowLeft;

	private final BezierCurve cursorMove;

	protected int accTime;

	protected static final short TIME_CURSOR_ANIM = 450;

	private static DrawableImage ARROW_RIGHT;

	private boolean animationActive = true;

	public static final byte INDEX_LEFT = 0;
	public static final byte INDEX_CENTER = 1;
	public static final byte INDEX_RIGHT = 2;

	
	public ArrowGroup() throws Exception {
		super( 2 );

		if ( ARROW_RIGHT == null )
			ARROW_RIGHT = new DrawableImage(PATH_IMAGES + "setadir.png");

		arrowRight = new DrawableImage( ARROW_RIGHT );
		arrowLeft = new DrawableImage( arrowRight );
		arrowLeft.mirror( TRANS_MIRROR_H );
		arrowLeft.defineReferencePixel( ANCHOR_RIGHT );

		insertDrawable( arrowRight );
		insertDrawable( arrowLeft );

		cursorMove = new BezierCurve( new Point( 0, 0 ), new Point( arrowRight.getWidth() >> 2, 0 ), new Point( arrowRight.getWidth() >> 2, 0 ), new Point( 0, 0 ) );
	}


	public final int getInternalPosX() {
		return arrowLeft.getWidth() + ( arrowLeft.getWidth() >> 2 );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, Math.max( height, arrowLeft.getHeight() ) );

		arrowLeft.setPosition( 0, ( getHeight() - arrowLeft.getHeight() ) >> 1 );
		arrowRight.setPosition( width - arrowRight.getWidth(), ( getHeight() - arrowLeft.getHeight() ) >> 1 );
		arrowRight.defineReferencePixel( ANCHOR_RIGHT );
	}


	public final int getInternalWidth() {
		return getWidth() - ( arrowLeft.getWidth() << 1 ) - ( arrowLeft.getWidth() >> 1 );
	}


	public final void update( int delta ) {
		if ( animationActive ) {
			accTime += delta;
			final int diffX = cursorMove.getXFixed( NanoMath.divInt( accTime, TIME_CURSOR_ANIM ) );
			arrowLeft.setPosition( diffX, arrowLeft.getPosY() );
			arrowRight.setRefPixelPosition( getWidth() - diffX, arrowRight.getPosY() );
			if ( accTime >= TIME_CURSOR_ANIM )
				accTime %= TIME_CURSOR_ANIM;
		}
	}

	
	public final void setAnimationActive( boolean active ) {
		animationActive = active;
	}


	public final byte getActionAt( int x ) {
		if ( x <= getInternalPosX() ) {
			return INDEX_LEFT;
		} else if ( x >= getInternalPosX() + getInternalWidth() ) {
			return INDEX_RIGHT;
		} else {
			return INDEX_CENTER;
		}
	}


}
