package core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import screens.*;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Border;
import core.Team;


public final class PlayoffKey extends DrawableGroup implements Constants, Serializable {

	private final RichLabel labelScoreTeam1;

	private final RichLabel labelScoreTeam2;

	private Drawable box1;

	private Drawable box2;

	// usado somente nas finais
	private Drawable boxWinner;

	final int side;

	private int level;

	final Pattern imgH;

	final Pattern imgH2;

	final Pattern imgH3;

	final Pattern imgV;

	final Pattern imgV2;

	final Pattern imgH4;

	private final short[] score = new short[ 2 ];

	private int team1Index = -1;

	private int team2Index = -1;

	private final byte box1DrawableIndex;


	public PlayoffKey( int side, int level ) throws Exception {
		super( 15 );
		this.level = level;
		this.side = side;
		
		imgH = new Pattern( 0xffffff );
		imgH2 = new Pattern( 0xffffff );
		imgH3 = new Pattern( 0xffffff );
		imgV = new Pattern( 0xffffff );
		if ( level == PlayOffs.FINAL ) {
			imgV2 = new Pattern( 0xffffff );
			imgH4 = new Pattern( 0xffffff );
		} else {
			imgV2 = null;
			imgH4 = null;
		}
		box1 = new Border( Border.BORDER_COLOR_BLUE );
		box1.setSize( PLAYOFF_BOX_WIDTH, PLAYOFF_BOX_HEIGHT );
		box2 = new Border( Border.BORDER_COLOR_BLUE );
		box2.setSize( box1.getSize() );

		insertDrawable( imgV );
		insertDrawable( imgH );
		insertDrawable( imgH2 );
		insertDrawable( imgH3 );

		box1DrawableIndex = ( byte ) insertDrawable( box1 );
		insertDrawable( box2 );
		imgH.setSize( PLAYOFF_KEY_LINE_LENGTH, PLAYOFF_KEY_LINE_THICKNESS );
		imgH2.setSize( imgH.getSize() );
		imgH3.setSize( PLAYOFF_KEY_LINE_LENGTH, PLAYOFF_KEY_LINE_THICKNESS );

		labelScoreTeam1 = new RichLabel( GameMIDlet.GetFont( FONT_INDEX_TEXT ), null, PLAYOFF_BOX_WIDTH / 5 );
		labelScoreTeam2 = new RichLabel( GameMIDlet.GetFont( FONT_INDEX_TEXT ), null, PLAYOFF_BOX_WIDTH / 5 );

		switch ( level ) {
			case PlayOffs.EIGHTH:
				imgV.setSize( PLAYOFF_KEY_LINE_THICKNESS, PLAYOFF_BOX_HEIGHT + PLAYOFF_BOX_SPACE_Y );
				setSize( KEY_WIDTH, ( PLAYOFF_BOX_HEIGHT + PLAYOFF_BOX_SPACE_Y ) << 1 );
			break;

			case PlayOffs.QUARTER:
				imgV.setSize( PLAYOFF_KEY_LINE_THICKNESS, ( PLAYOFF_BOX_HEIGHT + PLAYOFF_BOX_SPACE_Y ) << 1 );
				setSize( KEY_WIDTH, PLAYOFF_BOX_HEIGHT + imgV.getHeight() + PLAYOFF_BOX_SPACE_Y );
			break;

			case PlayOffs.SEMI:
				imgH3.setSize( PLAYOFF_KEY_LINE_LENGTH, PLAYOFF_KEY_LINE_THICKNESS );
				imgV.setSize( PLAYOFF_KEY_LINE_THICKNESS, ( PLAYOFF_BOX_HEIGHT + PLAYOFF_BOX_SPACE_Y ) << 2 );
				setSize( KEY_WIDTH, PLAYOFF_BOX_HEIGHT + imgV.getHeight() + PLAYOFF_BOX_SPACE_Y );
			break;

			case PlayOffs.FINAL:
				insertDrawable( imgH4 );
				imgH4.setSize( imgH.getSize() );
				imgH3.setSize( imgH.getSize() );
				imgH3.setVisible( false );
				imgH.setVisible( false );
				insertDrawable( imgV2 );
				imgV.setSize( PLAYOFF_KEY_LINE_THICKNESS, PLAYOFF_BOX_SPACE_Y );
				imgV2.setSize( PLAYOFF_KEY_LINE_THICKNESS, PLAYOFF_BOX_SPACE_Y );

				boxWinner = new Border( Border.BORDER_COLOR_BLUE );
				boxWinner.setSize( box1.getSize() );
				insertDrawable( boxWinner );

				setSize( KEY_WIDTH_FINAL, ( PLAYOFF_BOX_HEIGHT + PLAYOFF_KEY_LINE_LENGTH ) << 1 );
			break;
		}
		insertDrawable( labelScoreTeam1 );
		insertDrawable( labelScoreTeam2 );
	}


	public final void refreshBorders( int playerTeamIndex ) {
		if ( team1Index >= 0 && team2Index >= 0 ) {
			if ( team1Index == playerTeamIndex ) {
				removeDrawable( box1 );
				box1 = new Border( Border.BORDER_COLOR_GREEN );
				box1.setSize( box2.getSize() );
				insertDrawable( box1, box1DrawableIndex );
			} else if ( team2Index == playerTeamIndex ) {
				removeDrawable( box2 );
				box2 = new Border( Border.BORDER_COLOR_GREEN );
				box2.setSize( box1.getSize() );
				insertDrawable( box2, box1DrawableIndex + 1 );
			}

			setSize( size );
		}
	}


	public final void setTeams( int indexTeam1, int indexTeam2 ) {
		if ( indexTeam1 >= 0 && indexTeam2 >= 0 ) {
			final DrawableImage flag1 = Team.getFlag( indexTeam1 );
			final DrawableImage flag2 = Team.getFlag( indexTeam2 );
			insertDrawable( flag1 );
			insertDrawable( flag2 );

			team1Index = indexTeam1;
			team2Index = indexTeam2;

			switch ( side ) {
				case PlayOffs.RIGHT:
					flag1.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH - flag1.getWidth() - PLAYOFF_BOX_WIDTH / 11, box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - flag1.getHeight() / 2 );
					flag2.setPosition( box2.getPosX() + PLAYOFF_BOX_WIDTH - flag2.getWidth() - PLAYOFF_BOX_WIDTH / 11, box2.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - flag2.getHeight() / 2 );
				break;

				case PlayOffs.LEFT:
				case PlayOffs.CENTER:
					flag1.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH / 11, box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - flag1.getHeight() / 2 );
					flag2.setPosition( box2.getPosX() + PLAYOFF_BOX_WIDTH / 11, box2.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - flag2.getHeight() / 2 );
				break;
			}
		}
	}


	public final void setScore( short scoreTeam1, short scoreTeam2 ) {
		score[ 0 ] = scoreTeam1;
		score[ 1 ] = scoreTeam2;

		labelScoreTeam1.setText( String.valueOf( score[ 0 ] ) );
		labelScoreTeam2.setText( String.valueOf( score[ 1 ] ) );

		if ( level == PlayOffs.FINAL && team1Index >= 0 && team2Index >= 0 ) {
			final DrawableImage flag = Team.getFlag( score[ 0 ] > score[ 1 ] ? team1Index : team2Index );
			insertDrawable( flag );

			flag.setPosition( boxWinner.getPosX() + boxWinner.getWidth() / 11, boxWinner.getPosY() + boxWinner.getHeight() / 2 - flag.getHeight() / 2 );
		}
	}


	public final int getTeam1Index() {
		return team1Index;
	}


	public final int getTeam2Index() {
		return team2Index;
	}


	public final short[] getScore() {
		return score;
	}


	public final int getWinnerTeamIndex() {
		return score[ 0 ] > score[ 1] ? getTeam1Index() : getTeam2Index();
	}


	public final int getLoserTeamIndex() {
		return score[ 0 ] > score[ 1] ? getTeam2Index() : getTeam1Index();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		switch ( side ) {
			case PlayOffs.LEFT:
				box1.setPosition( 0, 0 );
				imgH.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH, box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - imgH.getHeight() / 2 );
				imgV.setPosition( imgH.getPosX() + PLAYOFF_KEY_LINE_LENGTH - imgV.getWidth(), imgH.getPosY() );
				box2.setPosition( box1.getPosX(), imgV.getPosY() + imgV.getHeight() - PLAYOFF_BOX_HEIGHT / 2 );
				imgH2.setPosition( box2.getPosX() + PLAYOFF_BOX_WIDTH, box2.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - imgH2.getHeight() / 2 );
				imgH3.setPosition( imgV.getPosX() + imgV.getWidth(), imgV.getPosY() + imgV.getHeight() / 2 - imgH3.getHeight() / 2 );
				labelScoreTeam1.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH - labelScoreTeam1.getWidth(), box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - labelScoreTeam1.getHeight() / 2 );
				labelScoreTeam2.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH - labelScoreTeam1.getWidth(), box2.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - labelScoreTeam1.getHeight() / 2 );
			break;

			case PlayOffs.RIGHT:
				box1.setPosition( getWidth() - PLAYOFF_BOX_WIDTH, 0 );
				imgH.setPosition( box1.getPosX() - PLAYOFF_KEY_LINE_LENGTH, box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - imgH.getHeight() / 2 );
				imgV.setPosition( imgH.getPosX(), imgH.getPosY() );
				box2.setPosition( box1.getPosX(), imgV.getPosY() + imgV.getHeight() - PLAYOFF_BOX_HEIGHT / 2 );
				imgH2.setPosition( box2.getPosX() - imgH2.getWidth(), box2.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - imgH2.getHeight() / 2 );
				imgH3.setPosition( imgV.getPosX() - PLAYOFF_KEY_LINE_LENGTH, imgV.getPosY() + imgV.getHeight() / 2 - imgH3.getHeight() / 2 );
				labelScoreTeam1.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH / 15, box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - labelScoreTeam1.getHeight() / 2 );
				labelScoreTeam2.setPosition( box2.getPosX() + PLAYOFF_BOX_WIDTH / 15, box2.getPosY() + PLAYOFF_BOX_WIDTH / 14 + PLAYOFF_BOX_HEIGHT / 2 - labelScoreTeam1.getHeight() / 2 );
			break;

			case PlayOffs.CENTER:
				//CHAVE FINAL --> fica no centro
				// caixa indicando o campeão e o 3º colocado
				boxWinner.setPosition( ( getWidth() - boxWinner.getWidth() ) >> 1, 0 );

				// está sendo usado como linha vertical
				imgH4.setPosition( boxWinner.getPosX() + ( boxWinner.getWidth() >> 1 ) - ( PLAYOFF_KEY_LINE_THICKNESS >> 1 ), boxWinner.getPosY() + boxWinner.getHeight() );

				box1.setPosition( 0, getHeight() - PLAYOFF_BOX_HEIGHT );
				box2.setPosition( getWidth() - PLAYOFF_BOX_WIDTH, box1.getPosY() );

				imgV.setSize( imgH.getHeight(), PLAYOFF_KEY_LINE_LENGTH );
				imgV2.setSize( imgV.getSize() );
				imgH4.setSize( imgV.getSize() );

				imgV.setPosition( box1.getPosX() + ( ( PLAYOFF_BOX_WIDTH - imgV.getWidth() ) >> 1 ), imgH4.getPosY() + imgH4.getHeight() );
				imgV2.setPosition( box2.getPosX() + ( ( PLAYOFF_BOX_WIDTH - imgV2.getWidth() ) >> 1 ), imgV.getPosY() );

				imgH2.setSize( imgV2.getPosX() - imgV.getPosX(), imgH.getHeight() );
				imgH2.setPosition( imgV.getPosX(), imgH4.getPosY() + imgH4.getHeight() );
				
				labelScoreTeam1.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH - labelScoreTeam1.getWidth(), box1.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - labelScoreTeam1.getHeight() / 2 );
				labelScoreTeam2.setPosition( box1.getPosX() + PLAYOFF_BOX_WIDTH - labelScoreTeam1.getWidth(), box2.getPosY() + PLAYOFF_BOX_HEIGHT / 2 - labelScoreTeam1.getHeight() / 2 );
			break;
		}

		defineReferencePixel( ANCHOR_CENTER );
	}


	public final void write( DataOutputStream output ) throws Exception {
		output.writeInt( team1Index );
		output.writeInt( team2Index );
		output.writeShort( score[ 0 ] );
		output.writeShort( score[ 1 ] );
	}


	public final void read( DataInputStream input ) throws Exception {
		setTeams( input.readInt(), input.readInt() );
		setScore( input.readShort(), input.readShort() );
	}
}
