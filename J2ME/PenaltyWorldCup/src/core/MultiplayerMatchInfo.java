/**
 * MultiplayerMatchInfo.java
 *
 * Created on Jun 19, 2010 6:30:38 PM
 *
 */
//#if MULTIPLAYER == "true"
package core;

/**
 *
 * @author peter
 */
public final class MultiplayerMatchInfo {

	private int matchId;

	private Player[] players;


	public final int getMatchId() {
		return matchId;
	}


	public final void setMatchId( int matchId ) {
		this.matchId = matchId;
	}


	public final Player[] getPlayers() {
		return players;
	}


	public final void setPlayers( Player[] players ) {
		this.players = players;
	}


	public static final class Player {

		private int id;

		private String nickname;

		private int teamIndex;


		public final int getTeamIndex() {
			return teamIndex;
		}


		public final void setTeamIndex( int teamIndex ) {
			this.teamIndex = teamIndex;
		}


		public final int getId() {
			return id;
		}


		public final void setId( int id ) {
			this.id = id;
		}


		public final String getNickname() {
			return nickname;
		}


		public final void setNickname( String nickname ) {
			this.nickname = nickname;
		}

	}

}

//#endif