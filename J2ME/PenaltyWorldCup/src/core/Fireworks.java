/**
 * Fireworks.java
 * ©2008 Nano Games.
 *
 * Created on May 1, 2008 10:53:37 PM.
 */

//#if JAR != "min"

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class Fireworks extends Drawable implements Updatable {
	
	private final short MAX_PARTICLES;
	
	/** Número de partículas liberadas a cada explosão. */
	private short EXPLOSION_N_PARTICLES = 30;
	
	private short EXPLOSION_MAX_WIDTH;
	private short EXPLOSION_MAX_HALF_WIDTH;
	
	private static final short EMITION_INTERVAL = 250;
	
	private final MUV gravityAcc = new MUV( 20 );
	
	private int gravityDy;
	
	private short timeToNextEmition;
	
	private final Particle[] particles;
	
	private int activeParticles;
	
	
	public Fireworks() throws Exception {
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		final int SIZE = Math.max( getWidth(), getHeight() );
		MAX_PARTICLES = ( short ) ( ( SIZE * 3 ) >> 1 );

		particles = new Particle[ MAX_PARTICLES ];
		for ( short i = 0; i < MAX_PARTICLES; ++i ) {
			particles[ i ] = new Particle( this );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		final int SIZE = Math.max( width, height );

		gravityAcc.setSpeed( SIZE >> 3 );

		EXPLOSION_N_PARTICLES = ( short ) ( SIZE / 10 );

		EXPLOSION_MAX_WIDTH = ( short ) ( SIZE >> 2 );
		EXPLOSION_MAX_HALF_WIDTH = ( short ) ( EXPLOSION_MAX_WIDTH >> 1 );
	}


	protected final void paint( Graphics g ) {
		for ( short i = 0; i < activeParticles; ++i ) {
			particles[ i ].draw( g );
		}
	}
	
	
	public final void update( int delta ) {
		gravityDy = gravityAcc.updateInt( delta );
		
		timeToNextEmition -= delta;
		if ( timeToNextEmition <= 0 ) {
			timeToNextEmition = EMITION_INTERVAL;
			emit( new Point( NanoMath.randInt( size.x ), size.y ), Particle.STATE_EMITTING );
		}
		
		final int previousActiveParticles = activeParticles;
		for ( short i = 0; i < activeParticles; ++i ) {
			particles[ i ].update( delta );
		}		
		
		if ( previousActiveParticles != activeParticles )
			sort();
	}	
	
	
	private final void sort() {
		int i;
		int j;
		for ( i = 0, j = MAX_PARTICLES - 1; i < MAX_PARTICLES; ++i ) {
			if ( particles[ i ].state == Particle.STATE_DEAD ) {
				for ( ; j > i; --j ) {
					if ( particles[ j ].state != Particle.STATE_DEAD ) {
						final Particle temp = particles[ i ];
						particles[ i ] = particles[ j ];
						particles[ j ] = temp;
						break;
					}
				}
			}
		}
		
		activeParticles = j;
	}
	
	
	private final void emit( Point pos, byte state ) {
		if ( activeParticles < MAX_PARTICLES ) {
			switch ( state ) {
				case Particle.STATE_EMITTING:
					final int lifeTime = Particle.LIFETIME_EMITTING_MIN + NanoMath.randInt( Particle.LIFETIME_EMITTING_MAX_DIFF );
					final int destinationX = NanoMath.randInt( size.x );
					final int destinationY = size.y >> 2 + NanoMath.randInt( size.y >> 1 );
					particles[ activeParticles ].born( pos, ( destinationX - pos.x ) * 1000 / lifeTime, 
															( destinationY - pos.y ) * 1000 / lifeTime, state, lifeTime );
				break;
				
				case Particle.STATE_FALLING:
					particles[ activeParticles ].born( pos, 0, 0, state, Particle.LIFETIME_FALLING_MIN + NanoMath.randInt( Particle.LIFETIME_FALLING_MAX ) );
				break;
				
				case Particle.STATE_EXPLODED:
					final int time = Particle.LIFETIME_EXPLODED_MIN + NanoMath.randInt( Particle.LIFETIME_EXPLODED_MAX );
					particles[ activeParticles ].born( pos, ( -EXPLOSION_MAX_HALF_WIDTH + NanoMath.randInt( EXPLOSION_MAX_WIDTH ) ) * 1000 / time, 
															( -EXPLOSION_MAX_WIDTH + NanoMath.randInt( EXPLOSION_MAX_WIDTH ) ) * 1000 / time, state, time );
				break;
				
				default:
					return;
			}
			++activeParticles;
		}
	}
	
	
	private final void explode( Point pos ) {
		for ( short i = 0; i < EXPLOSION_N_PARTICLES; ++i ) {
			emit( pos, Particle.STATE_EXPLODED );
		}
	}
	
	
	private final void onDie() {
		--activeParticles;
		
		//#if DEBUG == "true"
		if ( activeParticles < 0 )
			throw new RuntimeException( "Número de partículas negativo." );
		//#endif
	}
	
	
	/**
	 * Classe interna que descreve uma partícula de fogos de artifício. 
	 */
	private static final class Particle extends Drawable implements Updatable {
		
		/** Tempo de vida restante da partícula. */
		private short lifeTime;
		
		private static final short LIFETIME_EMITTING_MIN = 1000;
		private static final short LIFETIME_EMITTING_MAX_DIFF = 4000;
		
		private static final short LIFETIME_FALLING_MIN = 100;
		private static final short LIFETIME_FALLING_MAX = 500;
		private static final short LIFETIME_EXPLODED_MIN = 300;
		private static final short LIFETIME_EXPLODED_MAX = 2300;
		
		/** Intervalo de emissao de novas partículas. */
		private static final short EMISSION_INTERVAL = 100;
		
		private short timeToNextEmission;
		
		private final MUV speedX = new MUV();
		private final MUV speedY = new MUV();
		
		private static final byte STATE_DEAD		= 0;
		private static final byte STATE_EMITTING	= 1;
		private static final byte STATE_FALLING		= 2;
		private static final byte STATE_EXPLODED	= 3;
		
		/** Estado atual da partícula. */
		private byte state;
		
		private int color;
		
		private final Fireworks parent;
		
		
		private Particle( Fireworks parent ) {
			this.parent = parent;
		}
		
		
		private final void born( Point bornPosition, int moduleX, int moduleY, byte state, int lifeTime ) {
			speedX.setSpeed( moduleX );
			speedY.setSpeed( moduleY );
			setPosition( bornPosition );
			this.state = state;
			
			this.lifeTime = ( short ) lifeTime;
			setVisible( true );
			
			switch ( state ) {
				case STATE_EMITTING:
					timeToNextEmission = EMISSION_INTERVAL;
					color = NanoMath.randInt( 0xffffff );
					setSize( 3, 2 );
				break;
				
				case STATE_FALLING:
					color = 0xffffff;
					setSize( 1, 2 );
				break;
				
				case STATE_EXPLODED:
					color = NanoMath.randInt( 0xffffff );
					setSize( 2, 3 );
				break;
			}
		}

		
		/**
		 * Prepara a partícula para ser desenhada. É basicamente o método <code>draw</code> de Drawable, porém não
		 * realiza testes de clip (utiliza a área definida pelo pai, ou seja, pelo emissor).
		 * 
		 * @param g
		 */
		public final void draw( Graphics g ) {
			if ( visible ) {
				translate.addEquals( position );

				paint( g );

				translate.subEquals( position );
			}
		} // fim do método draw( Graphics )		
		
		
		protected final void paint( Graphics g ) {
			g.setColor( color );
			g.fillRect( translate.x, translate.y, size.x, size.y );
		}


		public final void update( int delta ) {
			switch ( state ) {
				case STATE_EMITTING:
					timeToNextEmission -= delta;
					if ( timeToNextEmission <= 0 ) {
						timeToNextEmission = EMISSION_INTERVAL;
						parent.emit( position, STATE_FALLING );
					}
					
				case STATE_FALLING:
				case STATE_EXPLODED:
					setSize( size.y, size.x );
					lifeTime -= delta;
					if ( lifeTime <= 0 ) {
						parent.onDie();
						
						if ( state == STATE_EMITTING )
							parent.explode( position );
							
						setVisible( false );
						state = STATE_DEAD;
					} else {
						move( speedX.updateInt( delta ), speedY.updateInt( delta ) );
						speedY.setSpeed( speedY.getSpeed() + parent.gravityDy, false );
						
						if ( position.y > parent.size.y )
							lifeTime = 0;
					}
				break;
			}
		}
	}


}

//#endif