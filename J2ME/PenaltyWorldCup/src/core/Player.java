/**
 * Player.java
 * ©2008 Nano Games.
 *
 * Created on Mar 28, 2008 10:32:03 AM.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;
import screens.Match;


/**
 * 
 * @author Peter
 */
public class Player extends DrawableGroup implements Constants, SpriteListener, Updatable {
	
	protected static Match match;

	private static final byte MAX_SPRITES = 10;

	/** Tag da leitura do descritor de jogador: pele. Dados: cor */
	public static final char TAG_SKIN				= 'p';
	/** Tag da leitura do descritor de jogador: camisa.
	 * Dados: índice do tipo, cor principal, cor secundária */
	public static final char TAG_SHOOTER_SHIRT		= 'j';
	/** Tag da leitura do descritor de jogador: camisa.
	 * Dados: índice do tipo, cor principal, cor secundária */
	public static final char TAG_KEEPER_SHIRT		= 'g';
	/** Tag da leitura do descritor de jogador: cabelo.
	 * Dados: tipo, cor */
	public static final char TAG_HAIR				= 'h';
	/** Tag da leitura do descritor de jogador: meião. Dados: cor principal, cor secundária */
	public static final char TAG_SOCKS				= 'm';
	/** Tag da leitura do descritor de jogador: acessórios. Dados: tipo, cor */
	public static final char TAG_ACCESSORIES		= 'a';
	/** Tag da leitura do descritor de jogador: pé. Dados: tipo ('d' ou 'c') */
	public static final char TAG_FOOT				= 'f';
	/** Tag da leitura do descritor de jogador: barba. Dados: tipo, cor */
	public static final char TAG_BEIRD				= 'b';
	/** Tag da leitura do descritor de jogador: manga da camisa do goleiro. Dados: tipo ('c' ou 'l') */
	public static final char TAG_SLEEVES			= 'l';
	/** Tag da leitura do descritor de jogador: início dos dados do goleiro */
	public static final char TAG_KEEPER_INFO_START	= '{';
	/** Tag da leitura do descritor de jogador: fim dos dados do goleiro */
	public static final char TAG_KEEPER_INFO_END	= '}';
	/** Tag da leitura do descritor de jogador: short. 
	 * Dados (batedor): cor
	 * Dados (goleiro): tipo ('c' ou 'l'), cor*/
	public static final char TAG_SHORTS				= 's';
	/** Tag da leitura do descritor de jogador: início dos dados dos jogadores.*/
	public static final char TAG_PLAYER_INFO_START	= '[';
	/** Tag da leitura do descritor de jogador: fim dos dados de um jogador */
	public static final char TAG_PLAYER_INFO_END	= ']';
	/** Tag da leitura do descritor de jogador: quantidade total de batedores. Dados: quantidade de batedores. */
	public static final char TAG_TOTAL_SHOOTERS		= 't';
	/** Tag da leitura do descritor de jogador: chuteira. Dados: cor. */
	public static final char TAG_SHOES				= 'x';

	public static final byte COLOR_TYPE_ORIGINAL		= -1;
	public static final byte COLOR_TYPE_SKIN_WHITER		= 0;
	public static final byte COLOR_TYPE_SKIN_WHITE		= 1;
	public static final byte COLOR_TYPE_SKIN_ASIAN		= 2;
	public static final byte COLOR_TYPE_SKIN_INDIAN		= 3;
	public static final byte COLOR_TYPE_SKIN_BROWN		= 4;
	public static final byte COLOR_TYPE_SKIN_BLACK		= 5;

	public static final byte COLOR_TYPE_RED				= 6;
	public static final byte COLOR_TYPE_WINE			= 7;

	public static final byte COLOR_TYPE_BLUE_LIGHT		= 8;
	public static final byte COLOR_TYPE_BLUE_MEDIUM		= 9;
	public static final byte COLOR_TYPE_BLUE_DARK		= 10;

	public static final byte COLOR_TYPE_WHITE			= 11;
	public static final byte COLOR_TYPE_GRAY			= 12;
	public static final byte COLOR_TYPE_BLACK			= 13;
	
	public static final byte COLOR_TYPE_GREEN_LIGHT		= 14;
	public static final byte COLOR_TYPE_GREEN_MEDIUM	= 15;
	public static final byte COLOR_TYPE_GREEN_DARK		= 16;

	public static final byte COLOR_TYPE_YELLOW			= 17;
	public static final byte COLOR_TYPE_YELLOW_DARK		= 18;
	public static final byte COLOR_TYPE_ORANGE			= 19;
	
	public static final byte COLOR_TYPE_HAIR_BROWN		= 20;
	public static final byte COLOR_TYPE_HAIR_BLOND		= 21;
	public static final byte COLOR_TYPE_HAIR_RED		= 22;

	public static final int COLOR_SKIN_ORIGINAL_LIGHTER		= 0xffcb50;
	public static final int COLOR_SKIN_ORIGINAL_LIGHT		= 0xe09e00;
	public static final int COLOR_SKIN_ORIGINAL_MEDIUM		= 0xd36d00;
	public static final int COLOR_SKIN_ORIGINAL_DARK		= 0x613000;
	public static final int COLOR_SHIRT_ORIGINAL_LIGHT		= 0xff0000;
	public static final int COLOR_SHIRT_ORIGINAL_MEDIUM		= 0xce0000;
	public static final int COLOR_SHIRT_ORIGINAL_DARK		= 0x670000;
	public static final int COLOR_STRIPE_ORIGINAL_LIGHT		= 0x046c00;
	public static final int COLOR_STRIPE_ORIGINAL_MEDIUM	= 0x045600;
	public static final int COLOR_SHORTS_ORIGINAL_LIGHT		= 0x001152;
	public static final int COLOR_SHORTS_ORIGINAL_MEDIUM	= 0x000a30;
	public static final int COLOR_SHORTS_ORIGINAL_DARK		= 0x00061c;
	public static final int COLOR_HAIR_ORIGINAL_LIGHT		= 0x5f3812;
	public static final int COLOR_HAIR_ORIGINAL_MEDIUM		= 0x311400;
	public static final int COLOR_HAIR_ORIGINAL_DARK		= 0x1f0c00;
	public static final int COLOR_SOCKS_ORIGINAL_LIGHT		= 0xc6ddff;
	public static final int COLOR_SOCKS_ORIGINAL_MEDIUM		= 0x9bc3fe;
	public static final int COLOR_SOCKS_ORIGINAL_DARK		= 0x69a4fd;
	public static final int COLOR_SHOES_ORIGINAL_LIGHT		= 0x575b65;
	public static final int COLOR_SHOES_ORIGINAL_MEDIUM		= 0x25282e;
	public static final int COLOR_SHOES_ORIGINAL_DARK		= 0x171719;

	public static final byte HAIR_TYPE_NONE				= 0;
	public static final byte HAIR_TYPE_MULLETS			= 1;
	public static final byte HAIR_TYPE_KAKA				= 2;
	public static final byte HAIR_TYPE_BLACK_POWER		= 3;

	public static final byte SHIRT_TYPE_SOLID				= 0;
	public static final byte SHIRT_TYPE_STRIPES_HORIZONTAL	= 1;
	public static final byte SHIRT_TYPE_STRIPES_VERTICAL	= 2;
	public static final byte SHIRT_TYPE_STRIPES_DIAGONAL	= 3;

	public static final byte BEIRD_TYPE_NONE			= 0;
	public static final byte BEIRD_TYPE_SHORT			= 1;
	public static final byte BEIRD_TYPE_MEDIUM			= 2;
	public static final byte BEIRD_TYPE_LONG			= 3;

	public static final byte KEEPER_SLEEVE_TYPE_SHORT	= 0;
	public static final byte KEEPER_SLEEVE_TYPE_LONG	= 1;

	public static final byte KEEPER_SHORTS_TYPE_SHORT	= 0;
	public static final byte KEEPER_SHORTS_TYPE_LONG	= 1;

	public static final byte ACCESSORY_TYPE_SUNGLASSES	= 0;

	private final Sprite[] otherSprites;

	private byte totalOtherSprites;

	protected Sprite sprite;

	protected SpriteListener spriteListener;

	protected int spriteId;

	protected static final byte PLAYER_INDEX_KEEPER = -1;


	protected Player( int teamIndex, int playerIndex ) throws Exception {
		this( teamIndex, playerIndex, true );
	}


	public Player( int teamIndex, int playerIndex, boolean loadAll ) throws Exception {
		super( MAX_SPRITES );
		otherSprites = new Sprite[ MAX_SPRITES ];
		loadSprites( PATH_TEAMS, teamIndex, playerIndex, loadAll );
	}

	
	public void update( int delta ) {
		if ( match != null )
			sprite.update( match.getDelta() );
	}


	public final void setListener( SpriteListener listener, int id ) {
		spriteListener = listener;
		spriteId = id;
	}
	
	
	public static final void setMatch( Match match ) {
		Player.match = match;
	}


	public final int getPlayerHeight() {
		return sprite.getFrameImage( 0, 0 ).getHeight();
	}


	public static final PaletteMap[] getPaletteMap( char type, int color ) {
		switch ( type ) {
			case TAG_SKIN:
				switch ( color ) {
					case COLOR_TYPE_SKIN_WHITER:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHTER, COLOR_SKIN_WHITER_LIGHTER ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHT, COLOR_SKIN_WHITER_LIGHT ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_MEDIUM, COLOR_SKIN_WHITER_MEDIUM ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_DARK, COLOR_SKIN_WHITER_DARK ),
						};

					case COLOR_TYPE_SKIN_WHITE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHTER, COLOR_SKIN_WHITE_LIGHTER ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHT, COLOR_SKIN_WHITE_LIGHT ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_MEDIUM, COLOR_SKIN_WHITE_MEDIUM ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_DARK, COLOR_SKIN_WHITE_DARK ),
						};

					case COLOR_TYPE_SKIN_ASIAN:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHTER, COLOR_SKIN_ASIAN_LIGHTER ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHT, COLOR_SKIN_ASIAN_LIGHT ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_MEDIUM, COLOR_SKIN_ASIAN_MEDIUM ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_DARK, COLOR_SKIN_ASIAN_DARK ),
						};

					case COLOR_TYPE_SKIN_INDIAN:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHTER, COLOR_SKIN_INDIAN_LIGHTER ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHT, COLOR_SKIN_INDIAN_LIGHT ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_MEDIUM, COLOR_SKIN_INDIAN_MEDIUM ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_DARK, COLOR_SKIN_INDIAN_DARK ),
						};

					case COLOR_TYPE_SKIN_BROWN:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHTER, COLOR_SKIN_BROWN_LIGHTER ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHT, COLOR_SKIN_BROWN_LIGHT ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_MEDIUM, COLOR_SKIN_BROWN_MEDIUM ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_DARK, COLOR_SKIN_BROWN_DARK ),
						};

					case COLOR_TYPE_SKIN_BLACK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHTER, COLOR_SKIN_BLACK_LIGHTER ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_LIGHT, COLOR_SKIN_BLACK_LIGHT ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_MEDIUM, COLOR_SKIN_BLACK_MEDIUM ),
							new PaletteMap( COLOR_SKIN_ORIGINAL_DARK, COLOR_SKIN_BLACK_DARK ),
						};
				}
			break;

			case TAG_SHORTS:
				switch ( color ) {
					case COLOR_TYPE_BLUE_LIGHT:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_BLUE_LIGHTEST ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_BLUE_LIGHTER ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_BLUE_LIGHT ),
						};

					case COLOR_TYPE_BLUE_MEDIUM:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_BLUE_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_BLUE_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_BLUE_DARK ),
						};

					case COLOR_TYPE_BLUE_DARK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_BLUE_DARK ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_BLUE_DARKER ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_BLUE_DARKEST ),
						};

					case COLOR_TYPE_WHITE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_WHITE ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_GRAY_LIGHTER ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_GRAY_LIGHT ),
						};

					case COLOR_TYPE_BLACK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_GRAY_DARKER ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_GRAY_DARKEST ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_BLACK ),
						};

					case COLOR_TYPE_GREEN_LIGHT:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_GREEN_LIGHTER ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_GREEN_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_GREEN_MEDIUM ),
						};

					case COLOR_TYPE_GREEN_MEDIUM:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_GREEN_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_GREEN_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_GREEN_DARK ),
							};

					case COLOR_TYPE_GREEN_DARK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_GREEN_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_GREEN_DARK ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_GREEN_DARKER ),
						};

					case COLOR_TYPE_RED:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_RED_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_RED_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_RED_DARK ),
						};

					case COLOR_TYPE_YELLOW:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_YELLOW_LIGHTER ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_YELLOW_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_YELLOW_MEDIUM ),
						};

					case COLOR_TYPE_YELLOW_DARK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_YELLOW_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_YELLOW_DARK ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_YELLOW_DARKER ),
						};

					case COLOR_TYPE_ORANGE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_ORANGE_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_ORANGE_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_ORANGE_DARK ),
						};

					case COLOR_TYPE_WINE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_WINE_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_WINE_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_WINE_DARK ),
						};

					case COLOR_TYPE_GRAY:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHORTS_ORIGINAL_LIGHT, COLOR_GRAY_LIGHT ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_MEDIUM, COLOR_GRAY_MEDIUM ),
							new PaletteMap( COLOR_SHORTS_ORIGINAL_DARK, COLOR_GRAY_DARK ),
						};
				}
			break;

			case TAG_SHOOTER_SHIRT:
			case TAG_KEEPER_SHIRT:
				switch ( color ) {
					case COLOR_TYPE_BLUE_LIGHT:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_BLUE_LIGHTEST ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_BLUE_LIGHTER ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_BLUE_LIGHT ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_BLUE_LIGHTER ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_BLUE_LIGHT ),
						};

					case COLOR_TYPE_BLUE_MEDIUM:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_BLUE_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_BLUE_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_BLUE_DARK ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_BLUE_LIGHT ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_BLUE_MEDIUM ),
						};

					case COLOR_TYPE_BLUE_DARK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_BLUE_DARK ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_BLUE_DARKER ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_BLUE_DARKEST ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_BLUE_DARKER ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_BLUE_DARKEST ),
						};

					case COLOR_TYPE_WHITE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_WHITE ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_GRAY_LIGHTER ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_GRAY_LIGHT ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_WHITE ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_GRAY_LIGHTER ),
						};

					case COLOR_TYPE_BLACK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_GRAY_DARKER ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_GRAY_DARKEST ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_BLACK ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_GRAY_DARKEST ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_BLACK ),
						};

					case COLOR_TYPE_GREEN_LIGHT:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_GREEN_LIGHTER ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_GREEN_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_GREEN_MEDIUM ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_GREEN_LIGHTER ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_GREEN_LIGHT ),
						};

					case COLOR_TYPE_GREEN_MEDIUM:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_GREEN_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_GREEN_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_GREEN_DARK ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_GREEN_MEDIUM ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_GREEN_DARK ),
							};

					case COLOR_TYPE_GREEN_DARK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_GREEN_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_GREEN_DARK ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_GREEN_DARKER ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_GREEN_DARK ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_GREEN_DARKER ),
						};

					case COLOR_TYPE_RED:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_RED_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_RED_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_RED_DARK ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_RED_MEDIUM ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_RED_DARK ),
						};

					case COLOR_TYPE_YELLOW:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_YELLOW_LIGHTER ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_YELLOW_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_YELLOW_MEDIUM ),

							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_YELLOW_LIGHT ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_YELLOW_MEDIUM ),
						};

					case COLOR_TYPE_YELLOW_DARK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_YELLOW_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_YELLOW_DARK ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_YELLOW_DARKER ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_YELLOW_DARK ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_YELLOW_DARKER ),
						};

					case COLOR_TYPE_ORANGE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_ORANGE_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_ORANGE_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_ORANGE_DARK ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_ORANGE_LIGHT ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_ORANGE_MEDIUM ),
						};

					case COLOR_TYPE_WINE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_WINE_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_WINE_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_WINE_DARK ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_WINE_LIGHT ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_WINE_MEDIUM ),
						};

					case COLOR_TYPE_GRAY:
						return new PaletteMap[] {
							new PaletteMap( COLOR_SHIRT_ORIGINAL_LIGHT, COLOR_GRAY_LIGHT ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_MEDIUM, COLOR_GRAY_MEDIUM ),
							new PaletteMap( COLOR_SHIRT_ORIGINAL_DARK, COLOR_GRAY_DARK ),
							
							new PaletteMap( COLOR_STRIPE_ORIGINAL_LIGHT, COLOR_GRAY_LIGHT ),
							new PaletteMap( COLOR_STRIPE_ORIGINAL_MEDIUM, COLOR_GRAY_MEDIUM ),
						};
				}
			break;

			case TAG_HAIR:
			case TAG_BEIRD:
				switch ( color ) {
					case COLOR_TYPE_HAIR_RED:
						return new PaletteMap[] {
							new PaletteMap( COLOR_HAIR_ORIGINAL_LIGHT, COLOR_RED_LIGHT ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_MEDIUM, COLOR_RED_MEDIUM ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_DARK, COLOR_RED_DARK ),
						};

					case COLOR_TYPE_WHITE:
						return new PaletteMap[] {
							new PaletteMap( COLOR_HAIR_ORIGINAL_LIGHT, COLOR_WHITE ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_MEDIUM, COLOR_GRAY_LIGHTER ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_DARK, COLOR_GRAY_LIGHT ),
						};

					case COLOR_TYPE_BLACK:
						return new PaletteMap[] {
							new PaletteMap( COLOR_HAIR_ORIGINAL_LIGHT, COLOR_GRAY_DARKER ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_MEDIUM, COLOR_GRAY_DARKEST ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_DARK, COLOR_BLACK ),
						};

					case COLOR_TYPE_HAIR_BLOND:
					case COLOR_TYPE_YELLOW:
						return new PaletteMap[] {
							new PaletteMap( COLOR_HAIR_ORIGINAL_LIGHT, COLOR_YELLOW_LIGHT ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_MEDIUM, COLOR_YELLOW_MEDIUM ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_DARK, COLOR_YELLOW_DARK ),
						};

					case COLOR_TYPE_HAIR_BROWN:
						return new PaletteMap[] {
							new PaletteMap( COLOR_HAIR_ORIGINAL_LIGHT, COLOR_BROWN_LIGHT ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_MEDIUM, COLOR_BROWN_MEDIUM ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_DARK, COLOR_BROWN_DARK ),
						};

					case COLOR_TYPE_GRAY:
						return new PaletteMap[] {
							new PaletteMap( COLOR_HAIR_ORIGINAL_LIGHT, COLOR_GRAY_LIGHT ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_MEDIUM, COLOR_GRAY_MEDIUM ),
							new PaletteMap( COLOR_HAIR_ORIGINAL_DARK, COLOR_GRAY_DARK ),
						};
				}
			break;

			case TAG_SOCKS:
			case TAG_SHOES:
				final int[] ORIGINAL_COLORS = type == TAG_SOCKS ?
					new int[] { COLOR_SOCKS_ORIGINAL_LIGHT, COLOR_SOCKS_ORIGINAL_MEDIUM, COLOR_SOCKS_ORIGINAL_DARK } :
					new int[] { COLOR_SHOES_ORIGINAL_LIGHT, COLOR_SHOES_ORIGINAL_MEDIUM, COLOR_SHOES_ORIGINAL_DARK };

				switch ( color ) {
					case COLOR_TYPE_RED:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_RED_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_RED_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_RED_DARK ),
						};

					case COLOR_TYPE_BLUE_LIGHT:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_BLUE_LIGHTEST ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_BLUE_LIGHTER ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_BLUE_LIGHT ),
						};

					case COLOR_TYPE_BLUE_MEDIUM:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_BLUE_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_BLUE_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_BLUE_DARK ),
						};

					case COLOR_TYPE_BLUE_DARK:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_BLUE_DARK ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_BLUE_DARKER ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_BLUE_DARKEST ),
						};

					case COLOR_TYPE_WHITE:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_WHITE ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_GRAY_LIGHTER ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_GRAY_LIGHT ),
						};

					case COLOR_TYPE_BLACK:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_GRAY_DARKER ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_GRAY_DARKEST ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_BLACK ),
						};

					case COLOR_TYPE_GREEN_LIGHT:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_GREEN_LIGHTER ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_GREEN_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_GREEN_MEDIUM ),
						};

					case COLOR_TYPE_GREEN_MEDIUM:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_GREEN_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_GREEN_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_GREEN_DARK ),
						};

					case COLOR_TYPE_GREEN_DARK:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_GREEN_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_GREEN_DARK ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_GREEN_DARKER ),
						};

					case COLOR_TYPE_YELLOW:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_YELLOW_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_YELLOW_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_YELLOW_DARK ),
						};

					case COLOR_TYPE_YELLOW_DARK:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_YELLOW_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_YELLOW_DARK ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_YELLOW_DARKER ),
						};

					case COLOR_TYPE_ORANGE:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_ORANGE_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_ORANGE_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_ORANGE_DARK ),
						};

					case COLOR_TYPE_WINE:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_ORANGE_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_ORANGE_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_ORANGE_DARK ),
						};

					case COLOR_TYPE_GRAY:
						return new PaletteMap[] {
							new PaletteMap( ORIGINAL_COLORS[ 0 ], COLOR_GRAY_LIGHT ),
							new PaletteMap( ORIGINAL_COLORS[ 1 ], COLOR_GRAY_MEDIUM ),
							new PaletteMap( ORIGINAL_COLORS[ 2 ], COLOR_GRAY_DARK ),
						};
				}
			break;

			case TAG_ACCESSORIES:
			return null;
		}

		//#if DEBUG == "false"
//# 			return null;
		//#else
			System.out.println( "Padrão de cores inválida: " + type + ", " + color );
 			return null;
//			throw new IllegalArgumentException( "Padrão de cores inválida: " + type + ", " + color );
		//#endif
	}


	public final void loadSprites( final String path, final int teamIndex, final int playerIndex, final boolean loadAll ) throws Exception {

		GameMIDlet.openJarFile( path + teamIndex + ".bin", new Serializable() {
			public final void write( DataOutputStream output ) throws Exception {
			}


			public final void read( DataInputStream input ) throws Exception {
				boolean mirror = false;
				final Vector spritePaletteMaps = new Vector();
				final Vector otherSprites = new Vector();
				final Vector accessories = new Vector();
				byte totalPlayers = 1;
				byte currentPlayerIndex = -1;
				int playerIndexToRead = playerIndex;
				
				final byte READ_STATE_GENERIC	= 0;
				final byte READ_STATE_KEEPER	= 1;
				final byte READ_STATE_PLAYER	= 2;
				final byte READ_STATE_DONE		= 3;

				byte readState = READ_STATE_GENERIC;

				while ( input.available() > 0 && readState != READ_STATE_DONE ) {
					final char c = ( char ) input.readUnsignedByte();

					//#if DEBUG == "true"
						System.out.println( "LEU TAG: " + c + " -> " + input.available() + " bytes restantes" );
					//#endif

					switch ( c ) {
						case TAG_KEEPER_SHIRT:
						case TAG_SHOOTER_SHIRT:
							if ( ( playerIndexToRead >= 0 && readState != READ_STATE_KEEPER ) || ( playerIndexToRead < 0 && readState == READ_STATE_KEEPER ) ) {
								boolean read = true;
								String path = null;
								switch ( readState ) {
									case READ_STATE_KEEPER:
										if ( c == TAG_SHOOTER_SHIRT )
											read = false;
										path = PATH_KEEPERS;
									break;

									default:
										if ( c == TAG_KEEPER_SHIRT )
											read = false;
										else
											path = PATH_SHOOTERS;
									break;
								}
								if ( read ) {
									final byte shirtType = readNumber( input );
									final byte colorIndex = readNumber( input );
									addColors( spritePaletteMaps, c, colorIndex );

									switch ( shirtType ) {
										case SHIRT_TYPE_SOLID:
										break;

										default:
											final byte secondaryColor = readNumber( input );
											otherSprites.addElement( loadSprite( path, c, shirtType, secondaryColor, loadAll ) );
									}
								} else {
									skipLine( input );
								}
							} else {
								skipLine( input );
							}
						break;

						case TAG_TOTAL_SHOOTERS:
							totalPlayers = readNumber( input );
							playerIndexToRead %= totalPlayers;
						break;

						case TAG_SLEEVES:
						case TAG_SHORTS:
							if ( ( playerIndexToRead >= 0 && readState != READ_STATE_KEEPER ) || ( playerIndexToRead < 0 && readState == READ_STATE_KEEPER ) ) {
								final byte value = readNumber( input );
								byte color = value;
								addColors( spritePaletteMaps, c, color );
							}
						break;

						case TAG_SKIN:
						case TAG_SHOES:
						case TAG_SOCKS:
							if ( ( playerIndexToRead >= 0 && readState != READ_STATE_KEEPER ) || ( playerIndexToRead < 0 && readState == READ_STATE_KEEPER ) ) {
								final byte skinShoesColor = readNumber( input );
								addColors( spritePaletteMaps, c, skinShoesColor );
							} else {
								skipLine( input );
							}
						break;

						case TAG_HAIR:
						case TAG_BEIRD:
							if ( readState == READ_STATE_GENERIC ) {
								skipLine( input );
							} else {
								final byte hairType = readNumber( input );
								switch ( hairType ) {
									case HAIR_TYPE_NONE:
									break;

									default:
										final byte hairColor = readNumber( input );
										otherSprites.addElement( loadSprite( readState == READ_STATE_KEEPER ? PATH_KEEPERS : PATH_SHOOTERS, c, hairType, hairColor, loadAll ) );
								}
							}
						break;

						case TAG_FOOT:
							if ( readState == READ_STATE_PLAYER ) {
								char type;
								do {
									type = ( char ) input.readUnsignedByte();
								} while ( type != 'd' && type != 'c' );
								mirror = ( type == 'c' );
							}
						break;

						case TAG_KEEPER_INFO_START:
							if ( playerIndex < 0 ) {
								readState = READ_STATE_KEEPER;
							} else {
								char temp;
								do {
									temp = ( char ) input.readUnsignedByte();
								} while ( temp != TAG_KEEPER_INFO_END );
							}
						break;

						case TAG_PLAYER_INFO_START:
							if ( playerIndexToRead >= 0 ) {
								readState = READ_STATE_PLAYER;
								++currentPlayerIndex;
								while ( currentPlayerIndex < playerIndexToRead ) {
									final char temp = ( char ) input.readUnsignedByte();
									if ( temp == TAG_PLAYER_INFO_START )
										++currentPlayerIndex;
								}
							}
						break;

						case TAG_PLAYER_INFO_END:
						case TAG_KEEPER_INFO_END:
							// considera encerrada a leitura dos jogadores
							if ( playerIndexToRead <= currentPlayerIndex )
								readState = READ_STATE_DONE;
							else
								readState = READ_STATE_GENERIC;
						break;
					}
				}

				final PaletteMap[] map = new PaletteMap[ spritePaletteMaps.size() ];
				spritePaletteMaps.copyInto( map );

				if ( loadAll ) {
					final Sprite playerSprite = new Sprite( ( playerIndex >= 0 ? PATH_SHOOTERS : PATH_KEEPERS ) + "p", map.length > 0 ? map : null );
					insertDrawable( playerSprite );
				} else {
					final Sprite playerSprite = new Sprite( ( playerIndex >= 0 ? PATH_SHOOTERS : PATH_KEEPERS ) + "p_p",
															( playerIndex >= 0 ? PATH_SHOOTERS : PATH_KEEPERS ) + "p", map.length > 0 ? map : null );
					insertDrawable( playerSprite );
				}

				for ( Enumeration e = otherSprites.elements(); e.hasMoreElements(); ) {
					insertDrawable( ( Sprite ) e.nextElement() );
				}

				if ( mirror && loadAll )
					mirror( TRANS_MIRROR_H );
			}
		} );
	}


	public void reset() {
		refreshSpritesFrames();
	}


	/**
	 * Lê um número do stream, desprezando quaisquer outros caracteres.
	 * @param input
	 * @return
	 */
	private final byte readNumber( DataInputStream input ) throws Exception {
		final StringBuffer buffer = new StringBuffer();
		boolean startedNumber = false;
		boolean reading = true;

		while ( reading && input.available() > 0 ) {
			final char c = ( char ) input.readUnsignedByte();

			switch ( c ) {
				case '0': case '1': case '2': case '3': case '4':
				case '5': case '6': case '7': case '8': case '9':
					startedNumber = true;
					buffer.append( c );
				break;

				default:
					if ( startedNumber )
						reading = false;
			}
		}

		return Byte.parseByte( buffer.toString() );
	}


	private final void skipLine( DataInputStream input ) throws Exception {
		char c;
		do {
			c = ( char ) input.readUnsignedByte();
		} while ( c != '\r' && ( c != '\n' ) );
	}


	private final void addColors( Vector v, char tag, byte color ) {
		final PaletteMap[] colors = getPaletteMap( tag, color );
		if ( colors != null ) {
			for ( byte i = 0; i < colors.length; ++i )
				v.addElement( colors[ i ] );
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
		for ( byte i = 0; i < totalOtherSprites; ++i ) {
			otherSprites[ i ].setFrame( sprite.getCurrFrameIndex() );
		}
	}


	public void onSequenceEnded( int id, int sequence ) {
		if ( spriteListener != null )
			spriteListener.onSequenceEnded( spriteId, sequence );
	}


	protected void insertDrawable( Sprite s ) {
		if ( getUsedSlots() == 0 ) {
			sprite = s;
			sprite.setListener( this, 0 );
			setSize( sprite.getSize() );
		} else {
			otherSprites[ totalOtherSprites++ ] = s;
		}
		super.insertDrawable( s );
	}


	protected final Sprite loadSprite( String path, char type, byte index, byte color, boolean loadAll ) throws Exception {
		final String p = path + type + '_' + index;
		return new Sprite( loadAll ? p + ".bin" : p + "_p.bin", p + ".png", getPaletteMap( type, color ) );
	}


	public final void setSequence( int sequenceIndex ) {
		sprite.setSequence( sequenceIndex );
		refreshSpritesFrames();
	}
	
	
	private final void refreshSpritesFrames() {
		final byte FRAME_INDEX = sprite.getCurrFrameIndex();
		for ( byte i = 0; i < totalOtherSprites; ++i )
			otherSprites[ i ].setFrame( FRAME_INDEX );
	}


	public final void previousFrame() {
		sprite.previousFrame();
		refreshSpritesFrames();
	}


	public final void nextFrame() {
		sprite.nextFrame();
		refreshSpritesFrames();
	}


}
