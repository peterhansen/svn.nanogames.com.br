/**
 * NanoOnlineContainer.java
 * 
 * Created on 29/Jan/2009, 12:25:03
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.userInterface.form.layouts.Layout;
import core.Constants;

/**
 * Classe que encapsula funcionamentos básicos de telas do NanCBVContainerr Peter
 */
public class AbstractContainer extends Container implements Constants, EventListener {

	/** Espaçamento horizontal entre os componentes. */
	public static final byte LAYOUT_GAP_X = 3;
	/** Espaçamento vertical entre os componentes. */
	public static final byte LAYOUT_GAP_Y = 4;
	/** Posição horizontal de início dos componentes. */
	public static final byte LAYOUT_START_X = 3;
	/** Posição vertical de início dos componentes. */
	public static final byte LAYOUT_START_Y = 13;

	protected int backScreenIndex = -1;


	protected byte backEntry;


	public AbstractContainer( int totalItems, int backEntry ) throws Exception {
		this( totalItems );
		this.backEntry = ( byte ) backEntry;
	}


	public AbstractContainer( int[] items ) throws Exception {
		this( items, -1 );
	}


	public AbstractContainer( int[] items, int backEntry ) throws Exception {
		this( items.length );

		this.backEntry = ( byte ) backEntry;

		final String[] titles = new String[ items.length ];
		for ( byte i = 0; i < titles.length; ++i )
			titles[ i ] = AppMIDlet.getText( items[ i ] );

		addItems( titles );
	}


	public AbstractContainer( String[] items, int backEntry ) throws Exception {
		super( items.length );

		this.backEntry = ( byte ) backEntry;

		addItems( items );
	}


	protected final void addItems( String[] items ) throws Exception {
		for ( byte i = 0; i < items.length; ++i ) {
			insertDrawable( GameMIDlet.getButton( items[ i ], i, this ) );
		}
	}
	
	
	/**
	 * Cria um novo contâiner do Nano Online, utilizando um FlowLayout com eixo vertical.
	 * @param nSlots
	 * @throws java.lang.Exception
	 * @see #NanoOnlCBVContainerLayout )
	 */
	protected AbstractContainer( int nSlots ) throws Exception {
		this( nSlots, new FlowLayout( FlowLayout.AXIS_VERTICAL, ANCHOR_HCENTER ) );
	}
	
	
	/**
	 * Cria um novo contâiner do Nano Online, utilizando o layout escolhido.
	 * @param nSlots
	 * @param layout
	 * @throws java.lang.Exception
	 * @see CBVContainerr( int )
	 */
	protected AbstractContainer( int nSlots, Layout layout ) throws Exception {
		super( nSlots, layout );
		setScrollBarV( GameMIDlet.getScroll() );
	}


	public final void setLayout( Layout layout ) {
		super.setLayout( layout );

		if ( layout instanceof FlowLayout ) {
			setScrollableY( true );
//			( ( FlowLayout ) layout ).gap.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );
//			( ( FlowLayout ) layout ).start.set( LAYOUT_START_X, LAYOUT_START_Y );
		}
	}


	protected final void removeAllComponents() {
		while ( getUsedSlots() > 0 )
			removeDrawable( 0, false );
	}
	
	
	public final void setBackIndex( int backIndex ) {
		this.backScreenIndex = backIndex;
	}
	
	
	protected void onBack() {
		if ( backScreenIndex >= 0 ) {
			GameMIDlet.setScreen( backScreenIndex );
		}
	}


	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				buttonPressed( sourceId );
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						buttonPressed( backEntry );
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	private final void buttonPressed( int index ) {
		( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( null, getId(), index );
	}

	
}
