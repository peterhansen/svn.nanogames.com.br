/**
 * GameMIDlet.java
 * Â©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.online.ad.AdManager;
import br.com.nanogames.components.online.ad.ResourceManager;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.borders.DrawableBorder;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.Team;
import java.io.DataInputStream;
import java.io.DataOutputStream;

//#if JAR == "full"
import core.BkgMenu;
//#endif

//#if JAR != "min"
import core.Replay;
//#endif

//#if MULTIPLAYER == "true"
import core.MultiplayerMatchFinder;
import core.MultiplayerMatchInfo;
//#endif

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif
 import br.com.nanogames.components.util.Logger;

import core.Border;
import core.MessageBox;
import core.Player;
import core.ScrollBar;
import core.ScrollBar2;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener ,Serializable{
	
	private static final byte GAME_MAX_FRAME_TIME = 96;
	
	private static final byte BACKGROUND_TYPE_BLACKBOARD	= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;

	public static final byte OPTION_ENGLISH = 0;
	public static final byte OPTION_PORTUGUESE = 1;

	public static final byte OPTION_PLAY_SOUD = 0;
	public static final byte OPTION_NO_SOUND = 1;

	public static final byte CONFIRM_YES = 0;
	public static final byte CONFIRM_NO = 1;
	public static final byte CONFIRM_CANCEL = 2;

	private static BkgMenu bkg;
	public static StringBuffer log = new StringBuffer();

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif
	
//#if SCREEN_SIZE == "SMALL"
//# 		public static final int LOW_MEMORY_LIMIT = 900000;
	//#elif SCREEN_SIZE == "BIG"
//# 		public static final int LOW_MEMORY_LIMIT = 1600000;
    //#else
		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif

	/** ReferÃªncia para a tela de jogo , para que seja possÃ­vel retornar Ã  tela de jogo apÃ³s entrar na tela de pausa. */
	private static Match match;
	
	/** ReferÃªncia para a tela de campeonato, para que seja possÃ­vel retornar a ela apÃ³s uma partida. */
	private static GroupTableStage championship;
	//private static PlayOffs championship;
	
	private static boolean lowMemory;
	
	private static LoadListener loader;

	private static Team[] teams;

	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
			FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
		//#endif

//		log("GameMIDlet antes");
//		Vector v = new Vector();
//		try {
//			while (true) {
//				v.addElement( new byte[ 30000 ] );
//			}
//		} catch (Throwable e) {
//			v = null;
//		}
//
//		log("GameMIDlet depois");
//		System.out.println(Runtime.getRuntime().freeMemory());

	}

	
	public static final void setSpecialKeyMapping(boolean specialMapping) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}
		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
		}
	}


	public static final Button getButton( String text, int id, EventListener listener ) throws Exception {
		return getButton( text, id, listener, Border.BORDER_COLOR_GREEN );
	}


	public static final Button getButton( String text, int id, EventListener listener, byte color ) throws Exception {

		final Button button = new Button( GetFont( FONT_INDEX_BUTTON ), text ) {

			private int[] data;

			private Image img;
		
			public final void setSize( int width, int height ) {
				super.setSize( width + 4, height );

				if ( label != null )
					label.setPosition( ( getWidth() - label.getWidth() ) >> 1, label.getPosY() );

				if ( img == null || getWidth() != img.getWidth() || getHeight() != img.getHeight() ) {
					final int DATA_LENGTH = getWidth() * getHeight();
					data = null;
					img = null;
					img = Image.createImage( getWidth(), getHeight() );
					data = new int[ DATA_LENGTH ];
				}
			}

			
			public final void draw( Graphics g ) {
				switch ( state ) {
					case STATE_PRESSED:
					case STATE_ROLLOVER:
                        if (visible)
                        {
                            final Graphics g2 = img.getGraphics();
                            g2.setColor( 0xff00ff );
                            g2.fillRect( 0, 0, getWidth(), getHeight() );
                            final Point previousTranslate = new Point( translate );

                            translate.set( -getPosX(), -getPosY() );
                            super.draw( g2 );
                            translate.set( previousTranslate );
                            img.getRGB( data, 0, getWidth(), 0, 0, getWidth(), getHeight() );
                            final int magenta=g.getDisplayColor(0x00ff00ff) & 0x00ff00ff ;
                            for ( int i = 0; i < data.length; ++i ) {
                                final int color=g.getDisplayColor(data[i]);
                                if ( ( color & 0x00ff00ff ) == magenta ) {
                                    data[ i ] = 0;
                                } else {
                                    data[ i ] = 0xff000000 | Drawable.changeColorLightness( color, state == STATE_PRESSED ? -85 : 50 );
    //								data[ i ] &= ( ( NanoMath.clamp( i % getWidth(), 0, 0xff ) ) << 24 ) | 0x00ffffff;
                                }
                            }
                            g.drawRGB( data, 0, getWidth(), translate.x + getPosX(), translate.y + getPosY(), getWidth(), getHeight(), true );
                        }
					break;

					default:
						super.draw( g );
				}
			}

		};
		button.setId( id );

		if ( listener != null )
			button.addEventListener( listener );
		button.setBorder( getBorder( color ) );
		button.setSize( button.getWidth() + button.getBorder().getBorderWidth(), button.getHeight() + button.getBorder().getBorderHeight() );
//		button.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ), button.getHeight() + button.getBorder().getBorderHeight() );

		return button;
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final br.com.nanogames.components.userInterface.form.borders.Border getBorder( byte color ) throws Exception {
		final DrawableBorder b = new DrawableBorder( new Border( color ) );
		b.set( BORDER_OFFSET, BORDER_OFFSET, BORDER_OFFSET, BORDER_OFFSET );

		return b;
	}


	public static final Drawable getScrollFull() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollF = new Pattern( COLOR_SCROLL_BACK );+
//# 			scrollF.setSize( 8, 0 );
//# 			return scrollF;
		//#else
			return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
		//#endif
	}


	public static final Drawable getScrollPage() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollP = new Pattern( COLOR_SCROLL_FORE );
//# 			scrollP.setSize( 8, 0 );
//# 			return scrollP;
		//#else
			return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
		//#endif
	}


	public static final ScrollBar2 getScroll() throws Exception {
		return new ScrollBar2();
	}
	

	public static final void log(String s) {
		System.gc();

		log.append(s);
		log.append(": ");
		log.append(Runtime.getRuntime().freeMemory());

		log.append("\n");
	}

	
	protected final void loadResources() throws Exception {		
		//#if JAR == "full"
			switch ( getVendor() ) {
				case VENDOR_SONYERICSSON:
				case VENDOR_NOKIA:
				case VENDOR_SIEMENS:
				break;

				default:
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
			}
		//#else
//# 			lowMemory = true;
		//#endif
	  // lowMemory = true; // TODO

		for (byte i = 0; i < FONT_TYPES_TOTAL; ++i) {
			switch ( i ) {
				case FONT_INDEX_TEXT:
					// a fonte de texto é carregada somente quando necessário
				break;

				default:
					if (!isLowMemory()) {
						FONTS[i] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);
					} else {
						FONTS[i] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + FONT_INDEX_TEXT);
					}
			}
		}
		FONTS[ FONT_INDEX_TITLE ].setCharExtraOffset( FONT_TITLE_EXTRA_OFFSET );

		//#if SCREEN_SIZE =="SMALL"
//# 		FONTS[FONT_INDEX_DEFAULT].setCharExtraOffset(1);
//# 		FONTS[FONT_INDEX_TITLE].setCharExtraOffset(1);
		//#endif


		//#if NANO_RANKING == "true"
			// esse jogo não possui ranking local
			NanoOnline.setRankingTypes( NanoOnline.RANKING_TYPES_GLOBAL_ONLY );

			//#if MULTIPLAYER == "true"
				final int[] entries = new int[] { TEXT_RANKING_GOALS_DIFF_SINGLE_PLAYER, TEXT_RANKING_GOALS_DIFF_MULTIPLAYER };
			//#else
//# 				final int[] entries = new int[] { TEXT_RANKING_GOALS_DIFF_SINGLE_PLAYER };
			//#endif

			RankingScreen.init( entries, new RankingFormatter() {
				public String format(int type, long score) {
					return String.valueOf(score);
				}

				public final void initLocalEntry(int type, RankingEntry entry, int index) {
				}
			});
		//#endif
		
		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}
		
		try {
			AppMIDlet.loadData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
		} catch (Exception e) {
			setLanguage( NanoOnline.LANGUAGE_en_US );
		}
			
		setSpecialKeyMapping(true);
		setScreen( SCREEN_LOADING_1 );
		log("loadResources");
	} // fim do mÃ©todo loadResources()


	protected final void changeLanguage( byte language ) {
		try {
			switch ( language ) {
				case NanoOnline.LANGUAGE_pt_PT:
				case NanoOnline.LANGUAGE_pt_BR:
					loadTexts( TEXT_TOTAL, PATH_IMAGES + "textsPt.dat" );
				break;

				default:
					loadTexts( TEXT_TOTAL, PATH_IMAGES + "textsEn.dat" );
			}
		} catch ( Exception ex ) {
		}
	}

	
	public static void setTeams(Team[]teams){
		GameMIDlet.teams = teams;
	}


	public final void destroy(){
		if ( championship != null ) {
 			championship.checkAndSaveGame();
 			championship = null;
 		}
		
		super.destroy( );
	}


	protected final int changeScreen(int screen) throws Exception {
		final GameMIDlet midlet = (GameMIDlet) instance;

		Drawable nextScreen = null;
		//log("changeScreen " + screen);

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_BLACKBOARD;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;

		boolean fitInBoard = true;
		boolean bkgLogo = bkg != null && bkg.isLoaded();

		switch (screen) {
			case SCREEN_CHOOSE_SOUND:
				log("SCREEN_CHOOSE_SOUND");
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_YES,
							TEXT_NO,
						}, TEXT_DO_YOU_WANT_SOUND);
				break;

			case SCREEN_CHOOSE_LANGUAGE:
				log("SCREEN_CHOOSE_LANGUAGE");
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_ENGLISH,
							TEXT_PORTUGUESE,
						},TEXT_CHOOSE_LANGUAGE);
                break;

			case SCREEN_SPLASH_NANO:
				bkgLogo = true;
				log("SCREEN_SPLASH_NANO");
				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE != "SMALL"
				nextScreen = new BasicSplashNano(SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH);
				//#else
//# 				nextScreen = new BasicSplashNano(SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH);
				//#endif
				fitInBoard = false;
			//	nextScreen = new TestScreen();
				log("SCREEN_SPLASH_NANO");
				break;

			case SCREEN_SPLASH_GAME:
					log("SCREEN_SPLASH_NANO");
				nextScreen = new SplashGame();
				fitInBoard = false;
				break;

			case SCREEN_CUSTOMIZE:
				bkgLogo = false;
				//nextScreen = new Customize();
				fitInBoard = false;
				break;

			case SCREEN_PLAYOFFS:
				log("SCREEN_PLAYOFFS");
				nextScreen = new PlayOffs();
				fitInBoard = false;
				log("SCREEN_PLAYOFFS");
				break;

			case SCREEN_MAIN_MENU:
				log("SCREEN_MAIN_MENU");
				setSpecialKeyMapping(true);
				championship = null;
				unloadMatch();
				bkgLogo = true;
				
				//#if NANO_RANKING == "true"
					if (nanoOnlineForm != null) {
						NanoOnline.unload();
						nanoOnlineForm = null;
					}
				//#endif
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_PLAY,
							TEXT_OPTIONS,
							TEXT_NEWS,
							TEXT_NANO_ONLINE,
							TEXT_REPLAYS,
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,
						},NO_TITLE);
				log("SCREEN_MAIN_MENU");
			break;

			case SCREEN_NEW_GAME_MENU:
				log("SCREEN_NEW_GAME_MENU");
				setSpecialKeyMapping(true);

				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_WORLD_CUP,

							//#if MULTIPLAYER == "true"
								TEXT_MULTIPLAYER,
							//#endif
							
							TEXT_VERSUS,
							TEXT_TRAINING,
							TEXT_BACK
						}, NO_TITLE);
				log("SCREEN_NEW_GAME_MENU");
				break;

			case SCREEN_TABLE_GROUPS:
				log("SCREEN_TABLE_GROUPS");
				bkgLogo = false;
			break;

			case SCREEN_SAVED_GAME_FOUND:
				bkgLogo = false;
				nextScreen = createBasicMenu( screen, new int[] {
							TEXT_CONTINUE,
							TEXT_NEW_GAME,

							TEXT_BACK }, TEXT_SAVED_GAME_FOUND );
			break;

			case SCREEN_NEW_CHAMPIONSHIP:

				championship = new GroupTableStage( ChooseTeamScreen.getTeam1() );
				// sem break
				
			case SCREEN_CHAMPIONSHIP:

				try {
					log("SCREEN_CHAMPIONSHIP depois");
					nextScreen = championship;
				} catch (Exception e) {
					log("SCREEN_CHAMPIONSHIP depois");
					nextScreen = createTextScreen( SCREEN_HELP_MENU, FONT_INDEX_TEXT, log.toString(), "LOG", false, null );
				}
				fitInBoard = false;
			break;

			case SCREEN_MATCH:
				setSpecialKeyMapping(true);
				bkgLogo = false;
				nextScreen = match;
				match.setSoftKeyLabel();
				bkgType = BACKGROUND_TYPE_NONE;

				fitInBoard = false;
				break;

			case SCREEN_SAVE_REPLAY:
			case SCREEN_LOAD_REPLAY:
				final Button[] buttons = new Button[] {
					getButton( getText( TEXT_OK ), 0, null ),
					getButton( getText( TEXT_BACK ), 0, null )
				};
				final ReplayScreen replayScreen = new ReplayScreen(getFont(FONT_INDEX_DEFAULT), match, screen, buttons );
				nextScreen = new PenaltyForm( replayScreen, getText( screen == SCREEN_SAVE_REPLAY ? TEXT_SAVE_REPLAY : TEXT_LOAD_REPLAY ), buttons );
				bkgLogo =  false;
				break;

			case SCREEN_NEWS_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_NEWS_GENERAL,
					TEXT_NEWS_SCORERS,
					TEXT_NEWS_TABLE,
					//TEXT_HELP,
					TEXT_BACK,
					}, null );
			break;

			case SCREEN_NEWS_LIST_GENERAL:
			case SCREEN_NEWS_LIST_SCORERS:
			case SCREEN_NEWS_LIST_TABLE:
				final PenaltyForm form = new PenaltyForm( new NewsSelect( this, screen, SCREEN_NEWS_MENU ), getText( TEXT_NEWS_GENERAL + ( screen - SCREEN_NEWS_LIST_GENERAL ) ), new Button[] {
					getButton( getText( TEXT_UPDATE ), 0, null ),
					getButton( getText( TEXT_BACK ), 0, null ),
				});
				nextScreen = form;
			break;

			case SCREEN_NEWS_VIEW_SCORERS:
			case SCREEN_NEWS_VIEW_GENERAL:
			case SCREEN_NEWS_VIEW_TABLE:
				bkgLogo =  false;
				nextScreen = createTextScreen( SCREEN_NEWS_LIST_GENERAL + ( screen - SCREEN_NEWS_VIEW_GENERAL ), FONT_INDEX_TEXT, NewsSelect.getSelectedEntryText(), NewsSelect.getSelectedEntry().getEntry().getTitle().toUpperCase(), false, null );
			break;

			/*case SCREEN_NEWS_HELP:
			bkgLogo =  false;
			nextScreen = createTextScreen( SCREEN_NEWS_MENU, FONT_INDEX_TEXT, getText( TEXT_HELP_NEWS ) + getVersion(), getText( TEXT_HELP ), false, null );
			break;*/

			case SCREEN_NEWS_REFRESH:
				final int previousScreen = currentScreen;
				final MessageBox b = new MessageBox( NanoOnline.NANO_ONLINE_URL + "news_feeds/refresh", null );
				final ConnectionListener l = new ConnectionListener() {
					public final void processData( int id, byte[] data ) {
						// se chegou até aqui, a conexão foi bem-sucedida
						try {
							NanoOnline.readGlobalData( data );
						} catch ( Exception ex ) {
							//#if DEBUG == "true"
								ex.printStackTrace();
							//#endif
						}
						
						switch ( previousScreen ) {
							case SCREEN_NEWS_LIST_GENERAL:
							case SCREEN_NEWS_LIST_SCORERS:
							case SCREEN_NEWS_LIST_TABLE:
								b.changeToScreen( previousScreen );
							break;

							default:
								b.changeToScreen( SCREEN_NEWS_MENU );
						}
					}


					public final void onInfo( int id, int infoIndex, Object extraData ) {
					}


					public final void onError( int id, int errorIndex, Object extraData ) {
					}
				};
				b.setConnectionListener( l );
				b.setNextScreen( SCREEN_NEWS_MENU );
				final Form formNews = new Form();
				formNews.setContentPane( b );
				nextScreen = formNews;
//				fitInBoard = false;
			break;

			//#if MULTIPLAYER == "true"
				case SCREEN_START_MULTIPLAYER_MATCH:
					final Form f = new Form();
					f.setContentPane( new MultiplayerMatchFinder() );
					nextScreen = f;
				break;
			//#endif

			case SCREEN_OPTIONS:
				bkgLogo = true;
				if (MediaPlayer.isVibrationSupported()) {
					nextScreen = createBasicMenu(screen, new int[]{
						MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
						TEXT_VOLUME,
						MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON,
						TEXT_BACK,
						}, NO_TITLE);
				} else {
					nextScreen = createBasicMenu(screen, new int[]{
							MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
							TEXT_VOLUME,
							TEXT_BACK,
							}, NO_TITLE);
				}
			break;

			case SCREEN_HELP_MENU:
				bkgLogo = true;
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_OBJECTIVES,
							TEXT_CONTROLS,
							TEXT_BACK,}, NO_TITLE);
				break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
			case SCREEN_HELP_TIPS:
				bkgLogo = true;
				String title = null;
				switch ( screen ) {
					case SCREEN_HELP_OBJECTIVES:
						title = "LOG";
					//	title = getText( TEXT_OBJECTIVES );
					break;

					case SCREEN_HELP_CONTROLS:
						title = getText( TEXT_CONTROLS );
					break;
					
					case SCREEN_HELP_TIPS:
						title = getText( TEXT_TIPS );
					break;
				}
				//nextScreen = createTextScreen( SCREEN_HELP_MENU, FONT_INDEX_TEXT, log.toString(), title, false, null );
				 nextScreen = createTextScreen( SCREEN_HELP_MENU, FONT_INDEX_TEXT, getText( TEXT_HELP_CONTROLS + (screen - SCREEN_HELP_OBJECTIVES)) + getVersion(), title, false, null );
				break;

			case SCREEN_CREDITS:
				bkgLogo = false;
				final Form credits = createTextScreen( SCREEN_MAIN_MENU, FONT_INDEX_CREDITS, getText( TEXT_CREDITS_TEXT ), getText( TEXT_CREDITS ), true, null );
				nextScreen = credits;
			break;

			case SCREEN_PAUSE:
				bkgLogo = false;
				indexSoftRight = SOFT_KEY_REMOVE;
				
				if (MediaPlayer.isVibrationSupported()) {
					nextScreen = createBasicMenu(screen, new int[]{
								TEXT_CONTINUE,
								MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
								TEXT_VOLUME,
								MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON,
								TEXT_BACK_MENU,
								TEXT_EXIT_GAME}, NO_TITLE);
				} else {
					nextScreen = createBasicMenu(screen, new int[]{
								TEXT_CONTINUE,
								MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
								TEXT_VOLUME,
								TEXT_BACK_MENU,
								TEXT_EXIT_GAME}, NO_TITLE);
				}
			break;

			case SCREEN_CONFIRM_MENU:
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_YES,
							TEXT_NO
						}, TEXT_CONFIRM_BACK_MENU);
			break;

			case SCREEN_CONFIRM_EXIT:
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_YES,
							TEXT_NO
						}, TEXT_CONFIRM_EXIT_1);
			break;

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {
							public final void load(final LoadScreen loadScreen) throws Exception {
								// aloca os sons
								final String[] soundList = new String[SOUND_TOTAL];
								for (byte i = 0; i < SOUND_TOTAL; ++i) {
									//#if BAD_MP3 =="false"
									switch ( i ) {
										case SOUND_INDEX_PLAY_UUUUH:
										case SOUND_INDEX_PLAY_BAD:
										case SOUND_INDEX_PLAY_GOOD_MP3:
										case SOUND_INDEX_CROWD_AMBIENT:
											soundList[i] = PATH_SOUNDS + i + ".mp3";
										break;

										default:
											soundList[i] = PATH_SOUNDS + i + ".mid";
									}
									//#else
//# 											soundList[i] = PATH_SOUNDS + i + ".mid";
									//#endif
								}
								MediaPlayer.init(DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList);

								Border.load();
								ScrollBar.loadImages();
								//if(!isLowMemory())
								Team.loadFlags();

								ReplayScreen.loadReplays();

								if ( !isLowMemory() )
									bkg = new BkgMenu();
								
								loadScreen.setActive(false);
								setScreen(SCREEN_CHOOSE_LANGUAGE);
							}
						}, getFont(FONT_INDEX_DEFAULT), TEXT_LOADING);
				bkgType = BACKGROUND_TYPE_BLACKBOARD;
				log("SCREEN_LOADING_1");
				break;

			case SCREEN_LOADING_2:
				if (!isLowMemory()) {
					bkg.setLogo(true);
				}
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						// pré-carrega algumas das imagens sempre utilizadas na tela de jogo
						Match.loadImages();
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						// pré-carrega algumas das imagens utilizadas na tela de campeonato
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						loadScreen.setActive(false);
						setScreen(SCREEN_MAIN_MENU);
					}
				}, getFont(FONT_INDEX_DEFAULT), TEXT_LOADING);

				bkgType = BACKGROUND_TYPE_BLACKBOARD;
				log("SCREEN_LOADING_2");
				break;

			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen(loader, getFont(FONT_INDEX_DEFAULT), TEXT_LOADING);
				loader = null;

				bkgType = BACKGROUND_TYPE_BLACKBOARD;
			break;

			case SCREEN_CHOOSE_COUNTRY_TRAINING:
			case SCREEN_CHOOSE_COUNTRY_OPPONENT:
			case SCREEN_CHOOSE_COUNTRY_CUP:
			case SCREEN_CHOOSE_COUNTRY_MULTIPLAYER:
				bkgLogo = false;
				System.out.println("ChooseTeamScreen memótria antes: " + Runtime.getRuntime().totalMemory());
				nextScreen = new ChooseTeamScreen( screen );
				System.out.println("ChooseTeamScreen  memótria depois: " + Runtime.getRuntime().totalMemory());
			break;
			
			//#if NANO_RANKING == "true"

			case SCREEN_NANO_RANKING_MENU:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_NANO_ONLINE:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						MediaPlayer.free();
						setSpecialKeyMapping(false);

						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_MAIN_MENU);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_MENU);
					}
				});

				break;

			case SCREEN_NANO_RANKING_PROFILES:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_PROFILES_SCREEN:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						setSpecialKeyMapping(false);
						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_PROFILES);
					}
				});
				break;

			case SCREEN_CHOOSE_PROFILE:
				setSpecialKeyMapping(false);
				final Customer c = NanoOnline.getCurrentCustomer();
				// a fonte sÃ³ possui caracteres maiÃºsculos
				final String name = (getText(TEXT_CURRENT_PROFILE) + (c.getId() == Customer.ID_NONE ? getText(TEXT_NO_PROFILE) : c.getNickname())).toUpperCase();
				nextScreen = createBasicMenu(screen,  new int[]{
							TEXT_CONFIRM,
							TEXT_CHOOSE_ANOTHER,
							TEXT_BACK
						}, name);
				break;

			case SCREEN_LOADING_HIGH_SCORES:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						MediaPlayer.free();

						setSpecialKeyMapping(false);
						match = null;
						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_MAIN_MENU, NanoOnline.SCREEN_NEW_RECORD);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_MENU);
					}
				});
				break;
			//#endif
		} // fim switch ( screen )
		
		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );

		setBackground( bkgType );

		if ( bkg != null ) {
			bkg.setLogo( bkgLogo );
			bkg.setAnimation( NanoMath.randInt( 5 ) + 1 );
		}
		
		if ( fitInBoard ) {
			if ( bkg == null ) {
				if ( nextScreen.getWidth() > 0 && nextScreen.getHeight() > 0 )
					nextScreen.setSize( Math.min( ScreenManager.SCREEN_WIDTH, nextScreen.getWidth() ), Math.min( ScreenManager.SCREEN_HEIGHT, nextScreen.getHeight() ) );
				else
					nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );			
			} else {
				bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				bkg.setPosition( 0, 0 );

				if ( nextScreen.getWidth() > 0 && nextScreen.getHeight() > 0 )
					nextScreen.setSize( Math.min( ScreenManager.SCREEN_WIDTH, nextScreen.getWidth() ), Math.min( bkg.getVisibleAreaHeight(), nextScreen.getHeight() ) );
				else
					nextScreen.setSize( ScreenManager.SCREEN_WIDTH, bkg.getVisibleAreaHeight() );


				nextScreen.setPosition( ( ScreenManager.SCREEN_WIDTH - nextScreen.getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - nextScreen.getHeight() ) >> 1 );
			}
		}
		
		midlet.manager.setCurrentScreen( nextScreen );

		return screen;
	} // fim do mÃ©todo changeScreen( int )


	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			//#if JAR == "full"
			case BACKGROUND_TYPE_BLACKBOARD:
				midlet.manager.setBackground( bkg, true  );
			break;
			//#endif
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( 0x111f4c );
			break;
		}
	} // fim do mÃ©todo setBackground( byte )
	
	
	private static final MenuPenalty createBasicMenu(int screen, int[] entries, int title) throws Exception {
		final MenuPenalty menu = new MenuPenalty((GameMIDlet) instance, screen, entries, title);
		menu.setCircular(true);
		return menu;
	}
	

	private static final MenuPenalty createBasicMenu(int screen, int[] entries, String title2) throws Exception {
		final MenuPenalty menu = new MenuPenalty((GameMIDlet) instance, screen, entries, title2);
		menu.setCircular(true);
		return menu;
	}


	private static final Form createTextScreen( int backIndex, int fontIndex, String text, String title, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		final BasicTextScreen textScreen = new BasicTextScreen( backIndex, GetFont( fontIndex ), text, autoScroll, specialChars );
		if ( !autoScroll ) {
			textScreen.setScrollFull( getScrollFull() );
			textScreen.setScrollPage( getScrollPage() );
		}
		final DrawableComponent c = new DrawableComponent( textScreen );
		c.setBorder( getBorder( Border.BORDER_COLOR_BLUE ) );
		final PenaltyForm form = new PenaltyForm( c, title, new Button[] { getButton( getText( TEXT_BACK ), 0, null ) } );
		return form;
	}
	
	
	public static final boolean isLowMemory() {
		return lowMemory;
	}
	
	
	public static final void prepareEnding() {
		loader = null;
		loader = new LoadListener() {
			public final void load( final LoadScreen loadScreen ) throws Exception {
				MediaPlayer.free();
				unloadMatch();
				match = new Match( Match.MODE_CHAMPION, null, null );
				setScreen( SCREEN_MATCH );
			}
		};
		
		setScreen( SCREEN_LOADING_PLAY_SCREEN );
	}


	//#if MULTIPLAYER == "true"
		public static final void playMatch( final MultiplayerMatchInfo matchInfo ) {
			loader = null;
			loader = new LoadListener() {
				public final void load( final LoadScreen loadScreen ) throws Exception {
					MediaPlayer.free();
					unloadMatch();
					match = new Match( matchInfo );
					setScreen( SCREEN_MATCH );
				}
			};
			setScreen( SCREEN_LOADING_PLAY_SCREEN );
		}
	//#endif
	
	
	public static final void playMatch( final Replay replay ) {
		loader = null;
		loader = new LoadListener() {
			public final void load( final LoadScreen loadScreen ) throws Exception {
				MediaPlayer.free();
				unloadMatch();				
				match = new Match( replay );
				setScreen( SCREEN_MATCH );
			}
		};
		setScreen( SCREEN_LOADING_PLAY_SCREEN );
	}	
	
	
	public static final void playMatch( final byte stage, final int difficultyLevel, final byte mode, final Team team1, final Team team2 ) {
		loader = null;
		loader = new LoadListener() {
			public final void load( final LoadScreen loadScreen ) throws Exception {
				MediaPlayer.free();
				unloadMatch();				
				match = new Match( mode, difficultyLevel, stage, team1, team2 );
				setScreen( SCREEN_MATCH );
			}
		};
		setScreen( SCREEN_LOADING_PLAY_SCREEN );
	}

	
	public static final void matchEnded( Match match ) {
		switch ( match.getMode() ) {
			case Match.MODE_CHAMPIONSHIP_MATCH_HOME:
			case Match.MODE_CHAMPIONSHIP_MATCH_AWAY:
				refreshMatchScore( match, RANKING_TYPE_GOALS_DIFF_SINGLE_PLAYER, match.getMode() == Match.MODE_CHAMPIONSHIP_MATCH_HOME );
				championship.playerMatchEnded( match );
			break;

			//#if MULTIPLAYER == "true"
			case Match.MODE_MULTIPLAYER_MATCH_AWAY:
			case Match.MODE_MULTIPLAYER_MATCH_HOME:
				refreshMatchScore( match, RANKING_TYPE_GOALS_DIFF_MULTIPLAYER, match.getMode() == Match.MODE_MULTIPLAYER_MATCH_HOME );
			//#endif

			default:
				setScreen( SCREEN_MAIN_MENU );
		}
	}


	private static final void refreshMatchScore( Match match, byte rankingType, boolean home ) {
		//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				final short[] score = new short[ 2 ];
				match.fillScore( score );
				final int diff = home ? score[ 0 ] - score[ 1 ] : score[ 1 ] - score[ 0 ];
				RankingScreen.setHighScore( rankingType, c.getId(), diff, true, true );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		//#endif
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				setSpecialKeyMapping(true);
				switch (index) {
					case ENTRY_MAIN_MENU_NEW_GAME:
					//#if NANO_RANKING == "true"
						setScreen(SCREEN_CHOOSE_PROFILE);
					//#else
//# 							setScreen( SCREEN_NEW_GAME_MENU );
					//#endif
					break;

					case ENTRY_MAIN_MENU_REPLAYS:
						setScreen( SCREEN_LOAD_REPLAY );
					break;
					
					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen(SCREEN_OPTIONS);
					break;

					case ENTRY_MAIN_MENU_NEWS:
						setScreen( SCREEN_NEWS_MENU );
					break;

					//#if NANO_RANKING == "true"
						case ENTRY_MAIN_MENU_NANO_ONLINE:
							setScreen(SCREEN_LOADING_NANO_ONLINE);
						break;
					//#endif

					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU

			case SCREEN_NEWS_MENU:
				switch ( index ) {
					case ENTRY_NEWS_GENERAL:
						setScreen( SCREEN_NEWS_LIST_GENERAL );
					break;

					case ENTRY_NEWS_SCORERS:
						setScreen( SCREEN_NEWS_LIST_SCORERS );
					break;

					case ENTRY_NEWS_TABLE:
						setScreen( SCREEN_NEWS_LIST_TABLE );
					break;

					/*case ENTRY_NEWS_HELP:
					setScreen( SCREEN_NEWS_HELP );
					break;*/

					case ENTRY_NEWS_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;
				}
			break;
			
			case SCREEN_NEW_GAME_MENU:

				//#if MULTIPLAYER == "false"
//# 					if ( index >= ENTRY_NEW_GAME_MENU_MULTIPLAYER )
//# 						++index;
				//#endif

				switch ( index ) {
					case ENTRY_NEW_GAME_MENU_TRAINING:
						System.out.println("ENTRY_NEW_GAME_MENU_TRAINING memótria antes: " + Runtime.getRuntime().totalMemory());
						setScreen( SCREEN_CHOOSE_COUNTRY_TRAINING );
						System.out.println("ENTRY_NEW_GAME_MENU_TRAINING memótria: depois  " + Runtime.getRuntime().totalMemory());
					break;

					case ENTRY_NEW_GAME_MENU_CHAMPIONSHIP:
						// verifica se há campeonato salvo. Caso exista, tenta carregá-lo. Se houver algum erro ao carregar 
						// campeonato, o comportamento é igual ao caso de não existir jogo salvo.
						if ( GroupTableStage.hasSavedGame() ) {
							setScreen( SCREEN_SAVED_GAME_FOUND );
						} else {
							System.out.println("ENTRY_NEW_GAME_MENU_CHAMPIONSHIP memótria antes: " + Runtime.getRuntime().totalMemory());
							setScreen( SCREEN_CHOOSE_COUNTRY_CUP );
							System.out.println("ENTRY_NEW_GAME_MENU_CHAMPIONSHIP memótria depois: " + Runtime.getRuntime().totalMemory());
						}
					break;

					case ENTRY_NEW_GAME_MENU_MULTIPLAYER:
						if ( NanoOnline.getCurrentCustomer().isRegistered() )
							setScreen( SCREEN_START_MULTIPLAYER_MATCH );
						else
							setScreen( SCREEN_CHOOSE_PROFILE );
					break;

					case ENTRY_NEW_GAME_MENU_VERSUS:
						setScreen( SCREEN_CHOOSE_COUNTRY_OPPONENT );
					break;

					case ENTRY_NEW_GAME_MENU_BACK:
						//#if NANO_RANKING == "true"
							setScreen( SCREEN_CHOOSE_PROFILE );
						//#else
//# 							setScreen( SCREEN_MAIN_MENU );
						//#endif
					break;
				}
			break;
			
			case SCREEN_SAVED_GAME_FOUND:
				switch ( index ) {
					case ENTRY_SAVED_GAME_CONTINUE:
						if ( loadChampionship() )
							setScreen( SCREEN_CHAMPIONSHIP );
						else
							setScreen( SCREEN_CHOOSE_COUNTRY_CUP );
					break;

					case ENTRY_SAVED_GAME_NEW_GAME:
						// inicia um novo campeonato
						setScreen( SCREEN_CHOOSE_COUNTRY_CUP );
					break;

					case ENTRY_SAVED_GAME_BACK:
						setScreen( SCREEN_NEW_GAME_MENU );
					break;
				}
			break;

			//#if NANO_RANKING == "true"
				case SCREEN_CHOOSE_PROFILE:
					switch (index) {
						case CONFIRM_YES:
							if (nanoOnlineForm != null) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
							setScreen(SCREEN_NEW_GAME_MENU);
						break;

						case CONFIRM_NO:
							setScreen(SCREEN_LOADING_PROFILES_SCREEN);
						break;

						case CONFIRM_CANCEL:
							if (nanoOnlineForm != null) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
							setScreen(SCREEN_MAIN_MENU);

						break;
					}
				break;
			//#endif

			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setSpecialKeyMapping(true);
							setScreen(SCREEN_MATCH );
						break;

						case ENTRY_PAUSE_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							//#if BAD_MP3 == "false"
							MediaPlayer.play( MediaPlayer.support( MediaPlayer.MEDIA_TYPE_MP3 ) ? SOUND_INDEX_PLAY_GOOD_MP3 : SOUND_INDEX_WHISTLE );
							//#else
//# 							MediaPlayer.play(  SOUND_INDEX_WHISTLE );
							//#endif
							( ( MenuPenalty ) menu ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case ENTRY_PAUSE_MENU_VOLUME:
							MediaPlayer.setVolume( ( MediaPlayer.getVolume() + 25 ) % 125 );
							( ( MenuPenalty ) menu ).setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
						break;

						case ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							( ( MenuPenalty ) menu ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:
							
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							setSpecialKeyMapping(true);
							setScreen(SCREEN_MATCH);
							break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
						break;

						case ENTRY_PAUSE_MENU_VOLUME:
							MediaPlayer.setVolume( ( MediaPlayer.getVolume() + 25 ) % 125 );
							( ( MenuPenalty ) menu ).setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:			
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}					
				}
			break; // fim case SCREEN_PAUSE
			
			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							( ( MenuPenalty ) menu ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
							break;

						case ENTRY_OPTIONS_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							//#if BAD_MP3 == "false"
							MediaPlayer.play( MediaPlayer.support( MediaPlayer.MEDIA_TYPE_MP3 ) ? SOUND_INDEX_PLAY_GOOD_MP3 : SOUND_INDEX_WHISTLE );
							//#else
//# 							MediaPlayer.play( SOUND_INDEX_WHISTLE );
							//#endif
							( ( MenuPenalty ) menu ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case ENTRY_OPTIONS_MENU_VOLUME:
							MediaPlayer.setVolume( ( MediaPlayer.getVolume() + 25 ) % 125 );
							( ( MenuPenalty ) menu ).setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
						break;

						case ENTRY_OPTIONS_MENU_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen(  SCREEN_MAIN_MENU );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							//#if BAD_MP3 == "false"
							MediaPlayer.play(MediaPlayer.support(MediaPlayer.MEDIA_TYPE_MP3) ? SOUND_INDEX_PLAY_GOOD_MP3 : SOUND_INDEX_WHISTLE);
							//#else
//# 							MediaPlayer.play(SOUND_INDEX_WHISTLE);
							//#endif
							( ( MenuPenalty ) menu ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case ENTRY_OPTIONS_MENU_VOLUME:
							MediaPlayer.setVolume( ( MediaPlayer.getVolume() + 25 ) % 125 );
							( ( MenuPenalty ) menu ).setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
						break;

						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen(SCREEN_MAIN_MENU);
							break;
					}
				}
			break; // fim case SCREEN_OPTIONS
			
			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case CONFIRM_YES:
						MediaPlayer.saveOptions();
						MediaPlayer.stop();
						MediaPlayer.play(SOUND_INDEX_SPLASH);
						setScreen( SCREEN_MAIN_MENU );
					break;
						
					case CONFIRM_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;
			
			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case CONFIRM_YES:
						MediaPlayer.saveOptions();
						exit();
					break;
						
					case CONFIRM_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}
				break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute(index == CONFIRM_NO);
				setScreen(SCREEN_SPLASH_NANO);
			break;
				
			case SCREEN_CHOOSE_LANGUAGE:
				try {
					switch (index) {
						case OPTION_PORTUGUESE:
							setLanguage( NanoOnline.LANGUAGE_pt_BR );
						break;

						case OPTION_ENGLISH:
						default:
							setLanguage( NanoOnline.LANGUAGE_en_US );
						break;
					}
					AppMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
					setScreen(SCREEN_CHOOSE_SOUND);
				} catch (Exception e) {
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif

					exit();
				}
			break;

		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
	}
	
	
	/**
	 * Define uma soft key a partir de um texto. Equivalente Ã  chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visÃ­vel. Para o label estar sempre visÃ­vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
//		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
//		} else {
//			try {
//				setSoftKey( softKey, new Label( getFont( FONT_INDEX_TEXT ), getText( textIndex ) ), true, visibleTime );
//			} catch ( Exception e ) {
//				//#if DEBUG == "true"
//					e.printStackTrace();
//				//#endif
//			}
//		}
	} // fim do mÃ©todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
			ScreenManager.getInstance().setSoftKey( softKey, d );
		//#endif
	} // fim do mÃ©todo setSoftKey( byte, Drawable, boolean, int )		
	
	
	protected final ImageFont getFont( int index ) {
		switch ( index ) {
			case FONT_INDEX_TEXT:
				try {
					// como essa fonte é usada somente nas telas de ajuda e notícias, não precisa estar sempre carregada
					final ImageFont font = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + FONT_INDEX_TEXT );
					font.setCharExtraOffset( 1 );
					return font;
				} catch ( Exception ex ) {
					//#if DEBUG == "true"
						ex.printStackTrace();
					//#endif
					return null;
				}

			default:
				return FONTS[ index ];
		}
	}
    
    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "";

        return "<ALN_H>" + getText( TEXT_VERSION ) + version + "\n\n";
    }    
	
	
	private static final void unloadMatch() {
		if ( match != null ) {
			match.unloadMatch();
			match = null;
			AdManager.unload();
			ResourceManager.unload();
		}
		System.gc();
	}


	public final void write(DataOutputStream output) throws Exception {
		output.writeByte(language);
	}


	public final void read( DataInputStream input ) throws Exception {
		setLanguage( input.readByte() );
	}

	
// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;

	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualizaÃ§Ã£o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private final byte previousScreen;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.prof.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );

			previousScreen = currentScreen;

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );

			ScreenManager.setKeyListener( null );
			//#if TOUCH == "true"
//# 			ScreenManager.setPointerListener( null );
			//#endif

		}

		private LoadScreen( LoadListener listener ) throws Exception {

			super( AppMIDlet.GetFont( FONT_INDEX_DEFAULT ), AppMIDlet.getText( TEXT_LOADING ) );
            previousScreen = currentScreen;
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo sÃ£o carregados aqui para evitar sobrecarga do mÃ©todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// sÃ³ inicia a thread quando a tela atual for a ativa, para evitar possÃ­veis atrasos em transiÃ§Ãµes e
					// garantir que novos recursos sÃ³ serÃ£o carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						//#if TOUCH == "true"
//# 						ScreenManager.setPointerListener( null );
						//#endif

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
										e.printStackTrace();
										//texts[ TEXT_LOG ] += e.getMessage().toUpperCase();

										setScreen( SCREEN_ERROR_LOG );
										e.printStackTrace();
									//#else
//# 									// volta Ã  tela anterior
//# 									setScreen( previousScreen );
								//#endif
								}
							} // fim do mÃ©todo run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execuÃ§Ã£o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do mÃ©todo update( int )


		public final void paint( Graphics g ) {
			super.paint( g );

			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	/**
	 * Tenta carregar o campeonato salvo. 
	 */
	private final boolean loadChampionship() {
		// continua o jogo salvo
		try {
			championship = null;
			championship = new GroupTableStage( GroupTableStage.INDEX_LOAD_GAME );
			return true;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
			
			return false;
		}
	}
	
	// </editor-fold>

}
