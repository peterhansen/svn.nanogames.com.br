/**
 * GroupTableStage.java
 * Â©2008 Nano Games.
 *
 * Created on Mar 31, 2008 7:17:13 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.ArrowGroup;
import core.Constants;
import core.PlayoffKey;
import core.Team;
import java.io.DataInputStream;
import java.io.DataOutputStream;

//#if TOUCH == "true"
//# import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public final class GroupTableStage extends UpdatableGroup implements Constants, KeyListener, EventListener, ScreenListener
//#if TOUCH == "true"
//# 		, PointerListener
//#endif
{
	private byte currentStage;

	/** Número total de jogos. */
	private static final byte TOTAL_MATCHES = 7;

	private static final byte LINE_WIDTH = 1;
	private static final byte LINE_TOTAL_WIDTH = LINE_WIDTH << 1;

	/** Quantidade de labels por cada linha de informação (nome do time, jogos, vitórias, derrotas, saldo de gols, gols pró). */
	private static final byte GROUP_LABELS_PER_LINE = 6;
	private final RichLabel[][] teamsInfo;

	/** Times do campeonato atual (todos os times, ordenados). */
	private final Team[][] groups = new Team[ GROUPS_TOTAL ][ TEAMS_IN_GROUP ];

	/** Times do campeonato atual (todos os times, não ordenados). */
	private final byte[][] originalGroups = new byte[ GROUPS_TOTAL ][ TEAMS_IN_GROUP ];

	/** Tabela dos times do grupo. */
	private final DrawableGroup labelsGroup;

	/** Letreiro que mostra informaÃ§Ãµes na parte inferior da tela. */
	private final MarqueeLabel labelTop;
	// seu time
	private int playerTeamIndex;

	private final Button buttonLeft;
	private final Button buttonRight;

	/** Grupo atualmente exibido na tela. */
	private byte currentGroup;


	private final ImageFont font = GameMIDlet.GetFont(FONT_INDEX_DEFAULT);

	private final ArrowGroup arrows;

	private final Label groupTitle;

	private final DrawableGroup cray;

	final DrawableRect out;
	final DrawableRect in;

	private final PlayOffs playoffs;

	private final Pattern greenBar;

	private static final byte VIEW_MODE_GROUPS		= 0;
	private static final byte VIEW_MODE_PLAYOFFS	= 1;

	private byte viewMode;

	
	public GroupTableStage( int playerTeamIndex ) throws Exception {
		super( 30 );

		this.playerTeamIndex = ( byte ) playerTeamIndex;

		//final int LABEL_WIDTH = Math.max( font.getTextWidth( "99" ), font.getTextWidth( GameMIDlet.getText( TEXT_GOALS_DIFF ) ) );
		final int LABEL_NAME_WIDTH = Math.max( TABLE_LABEL_WIDTH, font.getTextWidth(GameMIDlet.getText(TEXT_TEAM_NAME)) );

		for ( byte i = 0, teamIndex = 0; i < GROUPS_TOTAL; ++i ) {
			for ( byte j = 0; j < TEAMS_IN_GROUP; ++teamIndex, ++j ) {
				groups[ i ][ j ] = new Team( teamIndex );
				originalGroups[ i ][ j ] = teamIndex;
			}
		}

		// adiciona-se 2 ao número de itens devido às linhas que separam os títulos das colunas e os nomes dos times das informações
		labelsGroup = new DrawableGroup( ( TEAMS_IN_GROUP + 2 ) * GROUP_LABELS_PER_LINE );
		labelsGroup.setSize(LABEL_NAME_WIDTH + (TABLE_LABEL_WIDTH * (GROUP_LABELS_PER_LINE - 1)), font.getHeight() * (TEAMS_IN_GROUP + 1) );

		final Point labelPosition = new Point();

		final String[] columns = new String[] {
			//#if SCREEN_SIZE=="SMALL"
//# 			"<ALN_H>" + "TM",
			//#else
			"<ALN_H>" + GameMIDlet.getText( TEXT_TEAM_NAME ),
			//#endif
			"<ALN_H>" + GameMIDlet.getText( TEXT_GAMES ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_VICTORIES ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_LOSSES ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_GOALS_DIFF ),
			"<ALN_H>" + GameMIDlet.getText( TEXT_GOALS_FOR )
		};

		// insere primeiro os indicadores dos labels (Nome, J, V, D, SG, GP)
		for (byte i = 0; i < GROUP_LABELS_PER_LINE; ++i) {
			final RichLabel l = new RichLabel(font, columns[i], i == 0 ? LABEL_NAME_WIDTH : TABLE_LABEL_WIDTH, null);
			l.setPosition(labelPosition);
			labelsGroup.insertDrawable(l);

			labelPosition.x += l.getWidth();
		}
		labelPosition.set(0, font.getHeight() );

		final Pattern lineH = new Pattern( 0xffffff );
		lineH.setSize(labelsGroup.getWidth(), 2 );
		lineH.setPosition(0, labelPosition.y - lineH.getHeight() );
		labelsGroup.insertDrawable(lineH);

		final Pattern lineV = new Pattern( 0xffffff );
		lineV.setSize( 2, labelsGroup.getHeight() );
		lineV.setPosition(LABEL_NAME_WIDTH, 0);
		labelsGroup.insertDrawable(lineV);

		// insere os nÃºmeros indicando as estatÃ­sticas de cada time
		teamsInfo = new RichLabel[ TEAMS_IN_GROUP ][];
		for ( byte i = 0; i < TEAMS_IN_GROUP; ++i, labelPosition.x = 0, labelPosition.y += font.getHeight() ) {
			teamsInfo[ i ] = new RichLabel[ GROUP_LABELS_PER_LINE ];

			final RichLabel labelName = new RichLabel( font, null, LABEL_NAME_WIDTH );
			labelName.setPosition(labelPosition);
			teamsInfo[ i ][ 0 ] = labelName;
			teamsInfo[ i ][ 0 ].setSize( LABEL_NAME_WIDTH, font.getHeight() );
			labelsGroup.insertDrawable(labelName);

			labelPosition.x += LABEL_NAME_WIDTH;

			for ( byte j = 1; j < GROUP_LABELS_PER_LINE; ++j, labelPosition.x += TABLE_LABEL_WIDTH ) {
				final RichLabel label = new RichLabel( font, null, TABLE_LABEL_WIDTH );
				label.setPosition( labelPosition );
				teamsInfo[ i ][ j ] = label;
				teamsInfo[ i ][ j ].setSize( TABLE_LABEL_WIDTH, font.getHeight() );
				labelsGroup.insertDrawable( label );
			}
		}

		playoffs = new PlayOffs();
		insertDrawable( playoffs );

		// insere a borda da tabela do grupo
		cray = new DrawableGroup( 2 );
		out = new DrawableRect( 0xffffff );
		in = new DrawableRect( 0xff0000 );

		cray.insertDrawable( out );
		cray.insertDrawable(in);

		insertDrawable(cray);
		insertDrawable(labelsGroup);
        //#if SCREEN_SIZE  !="SMALL"

		greenBar = new Pattern(new DrawableImage(PATH_IMAGES + "barraverde.png"));
		//#else
//# 		greenBar = new Pattern( new DrawableImage( PATH_IMAGES + "barrainicial.png" ) );
		//#endif
		greenBar.setSize( Short.MAX_VALUE, greenBar.getHeight() );
		greenBar.setPosition( 0, TITLE_BAR_SPACING_Y );
		insertDrawable( greenBar );

		groupTitle = new Label( FONT_INDEX_DEFAULT );
		insertDrawable( groupTitle );

		arrows = new ArrowGroup();
		insertDrawable( arrows );

		labelTop = new MarqueeLabel( GameMIDlet.GetFont( FONT_INDEX_DEFAULT ), null );
		labelTop.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		labelTop.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		labelTop.setPosition( 1, TITLE_BAR_SPACING_Y + PenaltyForm.BORDER_THICKNESS );
		insertDrawable( labelTop );

		buttonLeft = GameMIDlet.getButton( GameMIDlet.getText( TEXT_PLAY ), ScreenManager.SOFT_KEY_LEFT, this );
		insertDrawable( buttonLeft );
		buttonRight = GameMIDlet.getButton( GameMIDlet.getText( TEXT_BACK ), ScreenManager.SOFT_KEY_RIGHT, this );
		insertDrawable( buttonRight );

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		if ( playerTeamIndex == INDEX_LOAD_GAME )
			loadSavedGame( this );

		playoffs.setPlayerTeamIndex( playerTeamIndex );
		
		setStage( currentStage );

		setViewMode( VIEW_MODE_GROUPS );
	} // fim do construtor


	public final void updateTable( int indexGroup ) {
		refreshGroupLabels();
	}


	private final void setViewMode( byte viewMode ) {
		this.viewMode = viewMode;

		switch ( viewMode ) {
			case VIEW_MODE_GROUPS:
				labelsGroup.setVisible( true );
				arrows.setVisible( true );
				cray.setVisible( true );
				groupTitle.setVisible( true );
                if(playoffs!=null)
				playoffs.setVisible( false );

				setCurrentGroup( getPlayerGroup() );
				refreshGroupLabels();
			break;

			case VIEW_MODE_PLAYOFFS:
				labelsGroup.setVisible( false );
				arrows.setVisible( false );
				cray.setVisible( false );
				groupTitle.setVisible( false );
                if(playoffs!=null)
				playoffs.setVisible( true );
			break;
		}
	}

	
	public final void keyPressed(int key) {
		switch ( viewMode ) {
			case VIEW_MODE_GROUPS:
				switch (key) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						checkAndSaveGame();
						GameMIDlet.setScreen(SCREEN_MAIN_MENU);
					break;

					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						playRound();
					break;

					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						setCurrentGroup( currentGroup - 1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						setCurrentGroup( currentGroup + 1 );
					break;

					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_NUM0:
						setViewMode( VIEW_MODE_PLAYOFFS );
					break;
				}
			break;

			case VIEW_MODE_PLAYOFFS:
				switch ( key ) {
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_NUM0:
						setViewMode( VIEW_MODE_GROUPS );
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						checkAndSaveGame();
						GameMIDlet.setScreen(SCREEN_MAIN_MENU);
					break;

					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						playRound();
					break;

					default:
						if(playoffs!=null)
						playoffs.keyPressed( key );
				}
			break;
		}

	}


	private final void setCurrentGroup( int group ) {
		currentGroup = ( byte ) ( ( group + GROUPS_TOTAL ) % GROUPS_TOTAL );
		groupTitle.setText( TEXT_GROUP_A + currentGroup );
		groupTitle.setPosition(  ( ScreenManager.SCREEN_WIDTH - groupTitle.getWidth() ) >> 1, groupTitle.getPosY() );
		refreshGroupLabels();
	}
	

	public final void keyReleased(int key) {
		switch ( viewMode ) {
			case VIEW_MODE_PLAYOFFS:
				if(playoffs!=null)
				playoffs.keyReleased( key );
			break;
		}
	}

	
	/**Joga a rodada atual. **/
	private final void playRound() {
		keyReleased(0);

		final Team[] match = getPlayerMatch();

		switch (currentStage) {
			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
				if ( viewMode == VIEW_MODE_GROUPS ) {
					setViewMode( VIEW_MODE_PLAYOFFS );
					break;
				}
				
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
				playMatch( match[ 0 ], match[ 1 ] );
			break;

			case STAGE_3RD_PLACE_MATCH:
			case STAGE_FINAL_MATCH:
				if ( match == null ) {
					// jogador não está na disputa do 3º lugar
					roundEnded( null );
					setStage( STAGE_FINAL_MATCH );
				} else {
					playMatch( match[ 0 ], match[ 1 ] );
				}
			break;

			case STAGE_PLAYER_WON:
				GameMIDlet.playMatch( STAGE_PLAYER_WON, 0, Match.MODE_CHAMPION, getTeamByIndex( playerTeamIndex ), getTeamByIndex( playerTeamIndex ) );
			break;

			case STAGE_PLAYER_ELIMINATED:
				if ( viewMode == VIEW_MODE_GROUPS )
					setViewMode( VIEW_MODE_PLAYOFFS );
				else
					GameMIDlet.setScreen(SCREEN_MAIN_MENU);
			break;
		}
	} // fim do mÃ©todo playRound()


	private final Team[] getPlayerMatch() {
		final byte[] playerGroup = originalGroups[ getPlayerGroup() ];
		final Team[] match = new Team[ 2 ];
		final int[] teams = new int[ 2 ];
		
		switch (currentStage) {
			case STAGE_1ST_ROUND:
				if ( playerGroup[ 0 ] == playerTeamIndex || playerGroup[ 1 ] == playerTeamIndex ) {
					match[ 0 ] = getTeamByIndex( playerGroup[ 0 ] );
					match[ 1 ] = getTeamByIndex( playerGroup[ 1 ] );
				} else {
					match[ 0 ] = getTeamByIndex( playerGroup[ 2 ] );
					match[ 1 ] = getTeamByIndex( playerGroup[ 3 ] );
				}
			break;

			case STAGE_2ND_ROUND:
				if ( playerGroup[ 0 ] == playerTeamIndex || playerGroup[ 2 ] == playerTeamIndex ) {
					match[ 0 ] = getTeamByIndex( playerGroup[ 0 ] );
					match[ 1 ] = getTeamByIndex( playerGroup[ 2 ] );
				} else {
					match[ 0 ] = getTeamByIndex( playerGroup[ 1 ] );
					match[ 1 ] = getTeamByIndex( playerGroup[ 3 ] );
				}
			break;

			case STAGE_3RD_ROUND:
				if ( playerGroup[ 0 ] == playerTeamIndex || playerGroup[ 3 ] == playerTeamIndex ) {
					match[ 0 ] = getTeamByIndex( playerGroup[ 0 ] );
					match[ 1 ] = getTeamByIndex( playerGroup[ 3 ] );
				} else {
					match[ 0 ] = getTeamByIndex( playerGroup[ 1 ] );
					match[ 1 ] = getTeamByIndex( playerGroup[ 2 ] );
				}
			break;

			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
			case STAGE_FINAL_MATCH:
				if (playoffs != null) {
					if (playoffs.getPlayerMatch(currentStage, playerTeamIndex, teams)) {
						match[ 0] = getTeamByIndex(teams[ 0]);
						match[ 1] = getTeamByIndex(teams[ 1]);
					} else {
						return null;
					}
				}
			break;

			case STAGE_3RD_PLACE_MATCH:
				if (playoffs != null) {
					if (playoffs.getPlayerMatch(currentStage, playerTeamIndex, teams)) {
						match[ 0] = getTeamByIndex(teams[ 0]);
						match[ 1] = getTeamByIndex(teams[ 1]);
					} else {
						roundEnded(null);
						setStage(currentStage + 1);
						return getPlayerMatch();
					}
				}
			break;

			case STAGE_PLAYER_WON:
			case STAGE_PLAYER_ELIMINATED:
				return null;
		}

		return match;
	}

	
	private final void playMatch(Team team1, Team team2) {
		final byte difficultyLevel = ( byte ) Math.min( 5 + ( ( ( currentStage* TOTAL_MATCHES) + currentStage) * 100 / ( DIVISIONS_TOTAL * TOTAL_MATCHES ) ), 100 );
		GameMIDlet.playMatch( currentStage, difficultyLevel, team1.getIndex() == playerTeamIndex ? Match.MODE_CHAMPIONSHIP_MATCH_HOME : Match.MODE_CHAMPIONSHIP_MATCH_AWAY, team1, team2 );
	}


	private final Team getTeamByIndex( int teamIndex ) {
		for ( byte i = 0; i < GROUPS_TOTAL; ++i ) {
			for ( byte j = 0; j < TEAMS_IN_GROUP; ++j ) {
				if ( groups[ i ][ j ].getIndex() == teamIndex )
					return groups[ i ][ j ];
			}
		}

		return null;
	}


	private final byte getPlayerGroup() {
		for ( byte i = 0; i < GROUPS_TOTAL; ++i ) {
			for ( byte j = 0; j < TEAMS_IN_GROUP; ++j ) {
				if ( groups[ i ][ j ].getIndex() == playerTeamIndex )
					return i;
			}
		}

		return -1;
	}


	private final void roundEnded( Match match ) {
		switch ( currentStage ) {
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
				final Team team1 = match.getTeam1();
				final Team team2 = match.getTeam2();
				updateTeams( match, team1, team2 );
				final byte playerGroup = getPlayerGroup();
				for ( byte i = 0; i < GROUPS_TOTAL; ++i ) {
					final byte[] teams = originalGroups[ i ];

					switch ( currentStage ) {
						case STAGE_1ST_ROUND:
							if ( playerGroup == i ) {
								if ( team1.getIndex() == teams[ 0 ] || team1.getIndex() == teams[ 1 ] )
									updateTeams( null, getTeamByIndex( teams[ 2 ] ), getTeamByIndex( teams[ 3 ] ) );
								else
									updateTeams( null, getTeamByIndex( teams[ 0 ] ), getTeamByIndex( teams[ 1 ] ) );
							} else {
								updateTeams( null, getTeamByIndex( teams[ 2 ] ), getTeamByIndex( teams[ 3 ] ) );
								updateTeams( null, getTeamByIndex( teams[ 0 ] ), getTeamByIndex( teams[ 1 ] ) );
							}
						break;

						case STAGE_2ND_ROUND:
							if ( playerGroup == i ) {
								if ( team1.getIndex() == teams[ 0 ] || team1.getIndex() == teams[ 2 ] )
									updateTeams( null, getTeamByIndex( teams[ 1 ] ), getTeamByIndex( teams[ 3 ] ) );
								else
									updateTeams( null, getTeamByIndex( teams[ 0 ] ), getTeamByIndex( teams[ 2 ] ) );
							} else {
								updateTeams( null, getTeamByIndex( teams[ 1 ] ), getTeamByIndex( teams[ 3 ] ) );
								updateTeams( null, getTeamByIndex( teams[ 0 ] ), getTeamByIndex( teams[ 2 ] ) );
							}
						break;

						case STAGE_3RD_ROUND:
							if ( playerGroup == i ) {
								if ( team1.getIndex() == teams[ 0 ] || team1.getIndex() == teams[ 3 ] )
									updateTeams( null, getTeamByIndex( teams[ 1 ] ), getTeamByIndex( teams[ 2 ] ) );
								else
									updateTeams( null, getTeamByIndex( teams[ 0 ] ), getTeamByIndex( teams[ 3 ] ) );
							} else {
								updateTeams( null, getTeamByIndex( teams[ 1 ] ), getTeamByIndex( teams[ 2 ] ) );
								updateTeams( null, getTeamByIndex( teams[ 0 ] ), getTeamByIndex( teams[ 3 ] ) );
							}
						break;
					}
				}
			break;

			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
			case STAGE_3RD_PLACE_MATCH:
			case STAGE_FINAL_MATCH:
				if (playoffs != null) {
					final PlayoffKey[] keys = playoffs.getKeys(currentStage);
					final short[] score = new short[2];
					for (byte i = 0; i < keys.length; ++i) {
						if (keys[i].getTeam1Index() == playerTeamIndex || keys[i].getTeam2Index() == playerTeamIndex) {
							match.fillScore(score);
						} else {
							Match.simulate(score);
						}
						keys[i].setScore(score[ 0], score[ 1]);
						System.out.println("RESULTS[ " + i + "]: " + score[ 0] + "X" + score[ 1]);
					}
				}
			break;
		}
	}


	public final void playerMatchEnded( Match match ) {
		final short[] score = new short[ 2 ];
		match.fillScore(score);

		final boolean playerWon = ( match.getTeam1().getIndex() == playerTeamIndex && score[ 0 ] > score[ 1 ] ) || ( match.getTeam2().getIndex() == playerTeamIndex && score[ 1 ] > score[ 0 ] );

		switch (currentStage) {
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
				roundEnded( match );
				setStage(currentStage + 1);
				setCurrentGroup( getPlayerGroup() );

				if ( getPlayerMatch() == null )
					simulateCup();
			break;

			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
			case STAGE_3RD_PLACE_MATCH:
			case STAGE_FINAL_MATCH:
				roundEnded( match );
				setStage( currentStage + 1 );
				if ( !playerWon )
					simulateCup();
			break;

			case STAGE_PLAYER_ELIMINATED:
				GameMIDlet.setScreen(SCREEN_MAIN_MENU);
			break;
		}

		keyReleased(0);
		System.out.println("SCREEN_CHAMPIONSHIP memótria antes: " + Runtime.getRuntime().totalMemory());
		GameMIDlet.setScreen(SCREEN_CHAMPIONSHIP);
		System.out.println("SCREEN_CHAMPIONSHIP memótria depois: " + Runtime.getRuntime().totalMemory());
	} // fim do mÃ©todo playerMatchEnded( Match )


	private final void simulateCup() {
		setStage( currentStage + 1 );
		while ( currentStage <= STAGE_FINAL_MATCH ) {
			roundEnded( null );
			setStage( currentStage + 1 );
		}
		setStage( STAGE_PLAYER_ELIMINATED );
	}

	
	private final void setStage(int stage) {
		switch ( stage ) {
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
				sortTeams();
			break;

			case STAGE_EIGHTH_FINALS:
				sortTeams();
				
				final PlayoffKey[] eighthFinals = playoffs.getKeys( stage );
				eighthFinals[ 0 ].setTeams( groups[ 0 ][ 0 ].getIndex(), groups[ 1 ][ 1 ].getIndex() );
				eighthFinals[ 1 ].setTeams( groups[ 2 ][ 0 ].getIndex(), groups[ 3 ][ 1 ].getIndex() );
				eighthFinals[ 2 ].setTeams( groups[ 4 ][ 0 ].getIndex(), groups[ 5 ][ 1 ].getIndex() );
				eighthFinals[ 3 ].setTeams( groups[ 6 ][ 0 ].getIndex(), groups[ 7 ][ 1 ].getIndex() );

				eighthFinals[ 4 ].setTeams( groups[ 1 ][ 0 ].getIndex(), groups[ 0 ][ 1 ].getIndex() );
				eighthFinals[ 5 ].setTeams( groups[ 3 ][ 0 ].getIndex(), groups[ 2 ][ 1 ].getIndex() );
				eighthFinals[ 6 ].setTeams( groups[ 5 ][ 0 ].getIndex(), groups[ 4 ][ 1 ].getIndex() );
				eighthFinals[ 7 ].setTeams( groups[ 7 ][ 0 ].getIndex(), groups[ 6 ][ 1 ].getIndex() );
			break;

			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
				final PlayoffKey[] results = playoffs.getKeys( stage - 1 );
				final PlayoffKey[] currentKeys = playoffs.getKeys( stage );
				for ( byte i = 0, count = 0; i < currentKeys.length; ++i ) {
					currentKeys[ i ].setTeams( results[ count++ ].getWinnerTeamIndex(), results[ count++ ].getWinnerTeamIndex() );
				}
			break;
			
			case STAGE_3RD_PLACE_MATCH:
				final PlayoffKey[] semiFinalsResults = playoffs.getKeys( STAGE_SEMI_FINALS );
				final PlayoffKey[] thirdPlaceMatch = playoffs.getKeys( STAGE_3RD_PLACE_MATCH );
				final PlayoffKey[] finalMatch = playoffs.getKeys( STAGE_FINAL_MATCH );
				
				finalMatch[ 0 ].setTeams( semiFinalsResults[ 0 ].getWinnerTeamIndex(), semiFinalsResults[ 1 ].getWinnerTeamIndex() );
				thirdPlaceMatch[ 0 ].setTeams( semiFinalsResults[ 0 ].getLoserTeamIndex(), semiFinalsResults[ 1 ].getLoserTeamIndex() );
			break;
			
			case STAGE_FINAL_MATCH:
				// nesse ponto, os times da final já estão definidos
			break;

			case STAGE_PLAYER_WON:
				// apaga o campeonato salvo anteriormente, armazenando somente o progresso do jogador
				eraseSavedGame();
				buttonLeft.setText( TEXT_OK );
			break;

			case STAGE_PLAYER_ELIMINATED:
				MediaPlayer.play(SOUND_INDEX_LOSE_CUP);
				buttonLeft.setText( TEXT_OK );
			break;
		}
		currentStage = (byte) stage;
		if (playoffs != null) {
			playoffs.setPlayerTeamIndex(playerTeamIndex);
		}
		refreshTitle();
		checkAndSaveGame();
	}

	
	/**
	 * Atualiza o label inferior da tela de acordo com a etapa atual.
	 */
	private final void refreshTitle() {
		final StringBuffer text = new StringBuffer();
		final Team[] playerMatch = getPlayerMatch();
		
		switch (currentStage) {
			case STAGE_1ST_ROUND:
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
				text.append(GameMIDlet.getText(TEXT_STAGE_FIRST_MATCH + currentStage - STAGE_1ST_ROUND));
				text.append( ": " );
				text.append( playerMatch[ 0 ].getName() );
				text.append( " X " );
				text.append( playerMatch[ 1 ].getName() );
			break;

			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
			case STAGE_3RD_PLACE_MATCH:
			case STAGE_FINAL_MATCH:
				if ( playerMatch != null ) {
					text.append( GameMIDlet.getText( TEXT_EIGHTH_FINALS + currentStage - STAGE_EIGHTH_FINALS ) );
					text.append(": ");
					text.append( playerMatch[ 0 ].getName() );
					text.append( " X " );
					text.append( playerMatch[ 1 ].getName() );
				}
			break;

			case STAGE_PLAYER_ELIMINATED:
				text.append(GameMIDlet.getText(TEXT_STAGE_ELIMINATED));
			break;

			case STAGE_PLAYER_WON:
				text.append(GameMIDlet.getText( TEXT_CHAMPION ) );
			break;
		}

		labelTop.setText(text.toString(), false);
		final int TEXT_WIDTH = labelTop.getFont().getTextWidth( labelTop.getText() );
		labelTop.setTextOffset( TEXT_WIDTH < labelTop.getWidth() ? ( labelTop.getWidth() - TEXT_WIDTH ) >> 1 : labelTop.getWidth() );
	}

	
	/**
	 * Atualiza o estado de 2 times apÃ³s uma partida.
	 *
	 * @param match partida entre as 2 equipes. Caso seja null, Ã© simulado o resultado.
	 */
	private final void updateTeams( Match match, Team team1, Team team2 ) {
		final short[] score = new short[ 2 ];

		if (match == null) {
			Match.simulate( score );
		} else {
			match.fillScore( score );
		}

		final int goalDiff = score[ 0] - score[ 1];
		team1.updateStatus(goalDiff > 0, score[ 0], goalDiff);
		team2.updateStatus(goalDiff < 0, score[ 1], -goalDiff);
	}

	
	/**
	 * Ordena os times de acordo com sua classificaÃ§Ã£o e atualiza os labels do grupo.
	 */
	private final void sortTeams() {
		int teamIndex;
		for ( int groupIndex = 0; groupIndex < GROUPS_TOTAL; ++groupIndex ) {
			final Team[] teams = groups[ groupIndex ];
			
			for ( teamIndex = 0; teamIndex < TEAMS_IN_GROUP; ++teamIndex ) {
				int bestIndex = teamIndex;
				Team best = teams[bestIndex];

				for ( int j = teamIndex + 1; j < TEAMS_IN_GROUP; ++j ) {
					final Team t2 = teams[ j ];

					if (best.getVictories() < t2.getVictories()) {
						bestIndex = j;
						best = teams[bestIndex];
					} else if (best.getVictories() == t2.getVictories()) {
						// empataram no nÃºmero de vitÃ³rias; o 2Âº critÃ©rio de desempate Ã© o saldo de gols
						if (best.getGoalsDiff() < t2.getGoalsDiff()) {
							bestIndex = j;
							best = teams[bestIndex];
						} else if (best.getGoalsDiff() == t2.getGoalsDiff()) {
							// empataram no saldo de gols; vai para o Ãºltimo critÃ©rio de desempate: gols marcados
							if (best.getGoalsScored() < t2.getGoalsScored()) {
								bestIndex = j;
								best = teams[bestIndex];
							}
						}
					}
				} // for (int j = head + 1; j < head + TEAMS_IN_GROUP; ++j) {

				if (bestIndex != teamIndex) {
					// faz uma troca de posiÃ§Ãµes - hÃ¡ um time melhor que o time de Ã­ndice 'i'
					final Team temp = teams[teamIndex];
					teams[teamIndex] = teams[bestIndex];
					teams[bestIndex] = temp;
				}
			}
		}
	}


	private final void refreshGroupLabels() {
		// atualiza os labels
		int victories;
		int goalsDiff;
		int goalsScored;

		final byte stage = ( byte ) Math.min( currentStage, STAGE_3RD_ROUND + 1 );
		
		final Team[] teams = groups[ currentGroup ];
		for ( int i = 0, j = 0; i < TEAMS_IN_GROUP; ++i, j = 0 ) {
			victories = teams[ i ].getVictories();
			goalsDiff = teams[ i ].getGoalsDiff();
			goalsScored = teams[ i ].getGoalsScored();
			teamsInfo[ i ][ j++ ].setText( "<ALN_H>" + teams[ i ].getNameShort() );
			teamsInfo[ i ][ j++ ].setText( "<ALN_H>" + stage );
			teamsInfo[ i ][ j++ ].setText( "<ALN_H>" + String.valueOf( victories ) );
			teamsInfo[ i ][ j++ ].setText( "<ALN_H>" + String.valueOf( stage - victories ) );
			teamsInfo[ i ][ j++ ].setText( "<ALN_H>" + String.valueOf( goalsDiff ) );
			teamsInfo[ i ][ j ].setText( "<ALN_H>" + String.valueOf( goalsScored ) );
		}
	}

	
	//#if TOUCH == "true"
//# 		public final void onPointerDragged( int x, int y ) {
//# 			switch ( viewMode ) {
//# 				case VIEW_MODE_GROUPS:
//# 				break;
//#
//# 				case VIEW_MODE_PLAYOFFS:
//# 					playoffs.onPointerDragged( x, y );
//# 				break;
//# 			}
//# 		}
//#
//#
//# 		public final void onPointerPressed(int x, int y) {
//# 			buttonLeft.onPointerPressed( x, y );
//# 			buttonRight.onPointerPressed( x, y );
//#
//# 			switch ( viewMode ) {
//# 				case VIEW_MODE_GROUPS:
//# 					if ( arrows.contains( x, y ) ) {
//# 						x -= arrows.getPosX();
//#
//# 						switch ( arrows.getActionAt( x ) ) {
//# 							case ArrowGroup.INDEX_LEFT:
//# 								keyPressed( ScreenManager.LEFT );
//# 							break;
//#
//# 							case ArrowGroup.INDEX_RIGHT:
//# 								keyPressed( ScreenManager.RIGHT );
//# 							break;
//# 						}
//# 					}
//# 				break;
//#
//# 				case VIEW_MODE_PLAYOFFS:
//# 					playoffs.onPointerPressed( x, y );
//# 				break;
//# 			}
//# 		}
//#
//#
//# 		public final void onPointerReleased(int x, int y) {
//# 			buttonLeft.onPointerReleased( x, y );
//# 			buttonRight.onPointerReleased( x, y );
//# 			buttonLeft.setFocus( false );
//# 			buttonRight.setFocus( false );
//#
//# 			switch ( viewMode ) {
//# 				case VIEW_MODE_PLAYOFFS:
//# 					playoffs.onPointerReleased( x, y );
//# 				break;
//# 			}
//# 		}
//#
	//#endif


	/**
	 * Verifica se o campeonato estÃ¡ num estÃ¡gio que deve ser gravado (ou seja, nÃ£o estÃ¡ encerrado), e o grava caso necessÃ¡rio.
	 */
	public final void checkAndSaveGame() {
		switch (currentStage) {
			case STAGE_2ND_ROUND:
			case STAGE_3RD_ROUND:
			case STAGE_EIGHTH_FINALS:
			case STAGE_QUARTER_FINALS:
			case STAGE_SEMI_FINALS:
			case STAGE_3RD_PLACE_MATCH:
			case STAGE_FINAL_MATCH:
				saveGame();
			break;

			default:
				eraseSavedGame();
		}
	}
	

	private final void saveGame() {
		final SavedGame save = new SavedGame(this);
		save.RMSMode = save.RMS_MODE_SAVE_GAME;

		try {
			GameMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save);
		} catch (Exception e) {
			//#if DEBUG == "true"
				System.out.println( "Erro ao gravar campeonato." );
				e.printStackTrace();
			//#endif
		}
	}

	
	private final void eraseSavedGame() {
		final SavedGame save = new SavedGame(this);
		save.RMSMode = save.RMS_MODE_ERASE_SAVED_GAME;

		try {
			GameMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save);
		} catch (Exception e) {
			//#if DEBUG == "true"
			System.out.println( "Erro ao apagar campeonato salvo." );
			e.printStackTrace();
			//#endif
		}
	}
	

	private static final void loadSavedGame(GroupTableStage c) throws Exception {
		final SavedGame save = new SavedGame(c);
		save.RMSMode = save.RMS_MODE_LOAD_SAVED_GAME;

		GameMIDlet.loadData(DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save);
	}


	/**
	 * Verifica se há jogo salvo no RMS.
	 * @return
	 */
	public static final boolean hasSavedGame() {
		final SavedGame save = new SavedGame( null );
		save.RMSMode = save.RMS_MODE_CHECK_SAVED_GAME;
		
		try {
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_CHAMPIONSHIP, save );
			return true;
		} catch ( Exception ex ) {
			return false;
		}
	}


	public final void setSize(int width, int height) {
		super.setSize( width, height );

		labelTop.setSize( width - 2, labelTop.getHeight() );
		buttonLeft.setPosition( BORDER_OFFSET, height - buttonLeft.getHeight() - BORDER_OFFSET );
		buttonRight.setPosition( width - buttonRight.getWidth() - BORDER_OFFSET, buttonLeft.getPosY() );

		final int START_Y = greenBar.getPosY() + greenBar.getHeight() + BORDER_OFFSET;
		final int HEIGHT = buttonLeft.getPosY() - START_Y - BORDER_OFFSET;
		final int START_Y_2 = START_Y + ( ( HEIGHT - groupTitle.getHeight() - labelsGroup.getHeight() - ( BORDER_OFFSET << 1 ) ) >> 1 );

		groupTitle.setPosition( ( width - groupTitle.getWidth() ) >> 1, START_Y_2 );
		labelsGroup.setPosition( ( width - labelsGroup.getWidth() ) >> 1, START_Y_2 + groupTitle.getHeight() + BORDER_OFFSET );

		cray.setSize( labelsGroup.getWidth() + LINE_TOTAL_WIDTH, labelsGroup.getHeight() + LINE_TOTAL_WIDTH );
		cray.defineReferencePixel( LINE_WIDTH, LINE_WIDTH );

		cray.setPosition( labelsGroup.getPosX() - LINE_WIDTH, labelsGroup.getPosY() - LINE_WIDTH );

		out.setSize( cray.getSize() );
		in.setPosition( LINE_WIDTH, LINE_WIDTH );
		in.setSize( cray.getWidth() - LINE_TOTAL_WIDTH, cray.getHeight() - LINE_TOTAL_WIDTH );

		arrows.setSize( width, arrows.getHeight() );
		arrows.setPosition( 0, groupTitle.getPosY() + ( ( groupTitle.getHeight() - arrows.getHeight() ) >> 1 ) );

		playoffs.setSize( size );
	}


	public final void eventPerformed( Event evt ) {
		if ( evt.eventType == Event.EVT_BUTTON_CONFIRMED ) {
			switch ( evt.source.getId() ) {
				case ScreenManager.SOFT_KEY_LEFT:
					keyPressed( ScreenManager.FIRE );
				break;

				case ScreenManager.SOFT_KEY_RIGHT:
					keyPressed( ScreenManager.KEY_SOFT_RIGHT );
				break;
			}
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

	
	private static final class SavedGame implements Serializable {

		private final byte RMS_MODE_SAVE_GAME			= 0;
		private final byte RMS_MODE_LOAD_SAVED_GAME		= 1;
		private final byte RMS_MODE_ERASE_SAVED_GAME	= 2;
		private final byte RMS_MODE_CHECK_SAVED_GAME	= 3;
		
		/** Modo de gravação ou leitura atual no RMS. */
		private byte RMSMode;

		private final GroupTableStage championship;

		
		private SavedGame(GroupTableStage c) {
			championship = c;
		}


		public final void write(DataOutputStream output) throws Exception {
			switch (RMSMode) {
				case RMS_MODE_SAVE_GAME:
					output.writeByte( championship.currentStage );
					output.writeInt( championship.playerTeamIndex );

					// cada time grava suas informações
					for ( byte i = 0; i < GROUPS_TOTAL; ++i ) {
						for ( byte j = 0; j < TEAMS_IN_GROUP; ++j ) {
							championship.groups[ i ][ j ].write( output );
						}
					}
					championship.playoffs.write( output );
				break;

				case RMS_MODE_ERASE_SAVED_GAME:
					// indica que não há jogo salvo
					output.writeByte( DIVISION_NO_SAVED_GAME );
				break;

				default:
					return;
			} // fim switch ( RMSMode )
		}

		
		public final void read(DataInputStream input) throws Exception {
			switch ( RMSMode ) {
				case RMS_MODE_LOAD_SAVED_GAME:
					// lÃª a divisÃ£o do campeonato salvo e carrega os times dessa divisÃ£o
					championship.currentStage = input.readByte();
					championship.playerTeamIndex = input.readInt();
					

					// cada time carrega suas informações
					for ( byte i = 0; i < GROUPS_TOTAL; ++i ) {
						for ( byte j = 0; j < TEAMS_IN_GROUP; ++j ) {
							championship.groups[ i ][ j ].read( input );
						}
					}
					championship.playoffs.read( input );
				break;

				case RMS_MODE_CHECK_SAVED_GAME:
					if ( input.readByte() == DIVISION_NO_SAVED_GAME) {
						//#if DEBUG == "true"
							throw new Exception( "Não há jogo salvo (championship.currentStage == DIVISION_NO_SAVED_GAME)." );
						//#else
//# 						throw new Exception();
						//#endif
					}
					// se não der exceção, é sinal de que há jogo salvo
				break;
			}
		}
	} // fim da classe interna SavedGame
	
}
