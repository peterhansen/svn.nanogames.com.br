/**
 * ChooseTeamScreen.java
 * ©2008 Nano Games.
 *
 * Created on May 3, 2008 8:00:54 PM.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.events.Event;
import core.Constants;
import core.Team;

//#if TOUCH == "true"
//# import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.NanoMath;
import core.ArrowGroup;
import core.Border;
import core.Keeper;
import core.Player;
import javax.microedition.lcdui.game.GameCanvas;

/**
 * 
 * @author Peter
 */
public final class ChooseTeamScreen extends UpdatableGroup implements Constants, KeyListener, EventListener
		//#if TOUCH == "true"
//# 			, PointerListener
		//#endif
{

	private final RichLabel teamName;
	private final UpdatableGroup flagGroup;
	private final Border cursorChoosing;
	private final Border cursorChosen;

	private final Border[] panels;
	private final ArrowGroup arrows;
	private final DrawableGroup labelGroup;

	// time onde o cursor está posicionado
	private static byte team1;
	// time onde o cursor está posicionado
	private static byte team2;
	
	private byte currentTeam;

	private final Pattern labelGroupBar;

	//indica se iremos para o treino ou para a copa.
	private final int idScreen;

	private final byte STATE_CHOOSE_PLAYER_TEAM = 0;
	private final byte STATE_CHOOSE_OPPONENT_TEAM = 1;

	private byte state;

	private final DrawableImage[] flags = new DrawableImage[ TEAMS_TOTAL ];

	private Player shooter;
	private Player keeper;

	private final int shooterDrawableIndex;
	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
		//#if BLACKBERRY_API =="true"
//# 		private final byte shooterAdjust = -23;
//# 		private final byte keeperAdjust = -17;
//# 		private final int BLACK_BERRY_SCREEN_HEIGHT = 260 ;
//#
		//#endif
	//#endif

	private final Button buttonBack;

	
	public ChooseTeamScreen( int idScreen ) throws Exception {
		super( 10 );

		this.idScreen = idScreen;
		
		for (byte i = 0; i < TEAMS_TOTAL; ++i) {
			flags[ i ] = Team.getFlag( i );
			flags[ i ].defineReferencePixel( ANCHOR_CENTER );
		}

		final int PANEL_WIDTH = flags[ 0 ].getWidth() + ( BORDER_OFFSET << 2 );
		final int PANEL_HEIGHT = flags[ 0 ].getHeight() + ( BORDER_OFFSET << 2 );

		final ImageFont font = GameMIDlet.GetFont(FONT_INDEX_DEFAULT);
		flagGroup = new UpdatableGroup( TEAMS_TOTAL + 10 );

		cursorChoosing = new Border( Border.BORDER_COLOR_GREEN );
		cursorChoosing.setSize( PANEL_WIDTH, PANEL_HEIGHT );
		cursorChoosing.defineReferencePixel( ANCHOR_CENTER );

		cursorChosen = new Border( Border.BORDER_COLOR_BLACK );
		cursorChosen.setSize( PANEL_WIDTH, PANEL_HEIGHT );
		cursorChosen.defineReferencePixel( ANCHOR_CENTER );

		panels = new Border[ TEAMS_IN_GROUP ];
		for ( byte i = 0; i < panels.length; ++i ) {
			final Border panel = new Border( Border.BORDER_COLOR_BLUE );
			panel.setSize( PANEL_WIDTH, PANEL_HEIGHT );
			panel.defineReferencePixel( ANCHOR_CENTER );
			panel.setPosition( getColumn( i ) * ( PANEL_WIDTH + BORDER_OFFSET ),
							   getRow( i ) * ( PANEL_HEIGHT + BORDER_OFFSET ) );

			panels[ i ] = panel;

			flagGroup.insertDrawable( panel );
		}
		teamName = new RichLabel( font, null, ScreenManager.SCREEN_WIDTH );

		flagGroup.setSize( ( PANEL_WIDTH + BORDER_OFFSET ) << 1, ( PANEL_HEIGHT + BORDER_OFFSET ) << 1 );
		insertDrawable( flagGroup );

		flagGroup.insertDrawable( cursorChoosing );
		shooterDrawableIndex = flagGroup.insertDrawable( cursorChosen ) + 1;

		for ( byte i = 0; i < TEAMS_TOTAL; ++i ) {
			flagGroup.insertDrawable( flags[ i ] );
		}

		if ( ScreenManager.getInstance().hasPointerEvents() ) {
			buttonBack = GameMIDlet.getButton( GameMIDlet.getText( TEXT_BACK ), 0, this );
			buttonBack.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
			insertDrawable( buttonBack );
		} else {
			buttonBack = null;
		}

		arrows = new ArrowGroup();
		insertDrawable( arrows );
		
		labelGroup = new DrawableGroup( 3 );
		//#if SCREEN_SIZE != "SMALL"
		labelGroupBar = new Pattern(new DrawableImage(PATH_IMAGES + "barraverde.png"));
		//#else
//# 		labelGroupBar = new Pattern(new DrawableImage(PATH_IMAGES + "barrainicial.png"));
		//#endif
		labelGroup.insertDrawable(labelGroupBar);
		labelGroup.insertDrawable(teamName);
		insertDrawable(labelGroup);

		setState(STATE_CHOOSE_PLAYER_TEAM);
		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		setCurrentTeam( NanoMath.randInt( TEAMS_TOTAL ) );
		GameMIDlet.log("construtor ChooseTeamScreen");
	}

	
	public final void setSize(int width, int height) {
		super.setSize(width, height);
		labelGroupBar.setSize(width, labelGroupBar.getHeight());
		labelGroup.setSize(labelGroupBar.getSize());
		labelGroup.setPosition( 0, MENUBAR_BOTTOM_SPACING );

		teamName.setSize( width, teamName.getFont().getHeight() );
		teamName.setPosition( 0, labelGroup.getHeight()/2 - teamName.getHeight()/2);

		if ( buttonBack == null ) {
			flagGroup.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( flagGroup.getWidth() >> 1 ), ScreenManager.SCREEN_HEIGHT - flagGroup.getHeight() - teamName.getHeight() );
		} else {
			flagGroup.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( flagGroup.getWidth() >> 1 ), ScreenManager.SCREEN_HEIGHT - flagGroup.getHeight() - buttonBack.getHeight() - ( BORDER_OFFSET << 1 ) );
			buttonBack.setRefPixelPosition( getWidth() >> 1, flagGroup.getPosY() + flagGroup.getHeight() + BORDER_OFFSET );
		}

		arrows.setSize( width, arrows.getHeight() );
		arrows.setPosition( 0, flagGroup.getPosY() + ( ( flagGroup.getHeight() - arrows.getHeight() ) >> 1 ) );

	}
	

	public static final byte getTeam1() {
		return team1;
	}

	
	public final void setState(byte state) {
		this.state = state;
		switch (state) {
			case STATE_CHOOSE_PLAYER_TEAM:
				cursorChosen.setVisible( false );
			break;

			// só entra nesse estado quando é treino
			case STATE_CHOOSE_OPPONENT_TEAM:
				team1 = currentTeam;
				cursorChosen.setVisible( true );
				cursorChosen.setRefPixelPosition( cursorChoosing.getRefPixelPosition() );

				setCurrentTeam( currentTeam + 1 );
			break;
		}
	}


	private final void setCurrentTeam( int team ) {
		currentTeam = ( byte ) ( ( team + TEAMS_TOTAL ) % TEAMS_TOTAL );

		cursorChoosing.setRefPixelPosition( panels[ getCursorRow() * 2 + getCursorColumn() ].getRefPixelPosition() );

		final int indexGroup = currentTeam / TEAMS_IN_GROUP;

		if ( state == STATE_CHOOSE_OPPONENT_TEAM ) {
			final int indexGroup2 = team1 / TEAMS_IN_GROUP;
			cursorChosen.setVisible( indexGroup == indexGroup2 );
		}

		for (int i = 0; i < TEAMS_TOTAL; i++) {
			flags[ i ].setVisible( false );
		}

		final int teamStart = indexGroup * TEAMS_IN_GROUP;

		for (int i = teamStart; i < teamStart + TEAMS_IN_GROUP; ++i ) {
			flags[ i ].setVisible(true);
			flags[ i ].setRefPixelPosition( panels[ i % TEAMS_IN_GROUP ].getRefPixelPosition() );
		}

		teamName.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_SOUTH_AFRICA + currentTeam ) );

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
//# 			try {
//# 				removeDrawable( shooter );
//# 				removeDrawable( keeper );
//# 				shooter = null;
//# 				keeper = null;
//# 
//# 				shooter = new Player( currentTeam, NanoMath.randInt( 100 ), true );
//# 				//final Player shooter = new Player( currentTeam, NanoMath.randInt( 100 ), false );
//# 				shooter.setPosition(
//# 						  ( getWidth() >> TEAM_SELECT_SHOOTER_HORIZONTAL_ADJUSTMENT) + ((shooter.getWidth()* TEAM_SELECT_SHOOTER_HORIZONTAL_MULTIPLIER) / TEAM_SELECT_SHOOTER_DIVIDER )
//# 						, ( getHeight() >> TEAM_SELECT_SHOOTER_VERTICAL_ADJUSTMENT ) + ((shooter.getPlayerHeight() * TEAM_SELECT_SHOOTER_VERTICAL_MULTIPLIER)   / TEAM_SELECT_SHOOTER_DIVIDER )
//# 						);
//# 				System.out.println(shooter.getPosX());
//# 				if( shooter.getMirror() == TRANS_MIRROR_H){
//# 	           //shooter.setPosition(shooter.getPosX()-shooter.getWidth() + shooter.getWidth()/12, shooter.getPosY());
//# 				shooter.mirror(TRANS_MIRROR_H);
//# 				System.out.println("canhoto " + shooter.getPosX() + "tamanho" + shooter.getWidth());
//# 				}
//# 				insertDrawable( shooter, shooterDrawableIndex );
//# 
//# 				keeper = new Keeper( currentTeam, true );
//# 				//final Keeper keeper = new Keeper( currentTeam, false );
//# 				keeper.setRefPixelPosition( ( getWidth() * 3 / 4 ), shooter.getPosY() + shooter.getHeight() );
//# 				insertDrawable( keeper, shooterDrawableIndex + 1 );
//# 
				//#if SCREEN_SIZE == "BIG"
//# 				if (ScreenManager.SCREEN_WIDTH > ScreenManager.SCREEN_HEIGHT) {
//# 					shooter.setPosition(shooter.getPosX() - 15, shooter.getPosY());
//# 					keeper.setPosition(keeper.getPosX() + 15, keeper.getPosY());
//# 
//# 				}
				//#endif
//# 
				//#if BLACKBERRY_API=="true"
//# 				if (ScreenManager.SCREEN_HEIGHT <= BLACK_BERRY_SCREEN_HEIGHT) {
//# 					shooter.setPosition(shooter.getPosX(), shooter.getPosY() + shooterAdjust);
//# 					keeper.setPosition(keeper.getPosX(), keeper.getPosY() + keeperAdjust);
//# 				}
				//#endif
//# 			} catch ( Exception e ) {
//# 				removeDrawable( shooter );
//# 				removeDrawable( keeper );
//# 				shooter = null;
//# 				keeper = null;
				//#if DEBUG == "true"
//# 					e.printStackTrace();
				//#endif
//# 			}
		//#endif
	}


	private final int getCursorColumn() {
		return getColumn( currentTeam );
	}

	
	private final int getCursorRow() {
		return getRow( currentTeam );
	}


	private final int getColumn( int i ) {
		return i & 1;
	}


	private final int getRow( int i ) {
		return ( i % TEAMS_IN_GROUP ) >> 1;
	}


	public final void keyPressed(int key) {
		switch (key) {
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				setCurrentTeam( getCursorColumn() == 0 ? currentTeam + 1 : currentTeam + ( TEAMS_IN_GROUP - 1 ) );
				if ( state == STATE_CHOOSE_OPPONENT_TEAM && currentTeam == team1 )
					keyPressed( key );
			break;

			case ScreenManager.KEY_NUM4:
			case ScreenManager.LEFT:
				setCurrentTeam( getCursorColumn() == 1 ? currentTeam - 1 : currentTeam - ( TEAMS_IN_GROUP - 1 ) );
				if ( state == STATE_CHOOSE_OPPONENT_TEAM && currentTeam == team1 )
					keyPressed( key );
			break;

			case ScreenManager.KEY_NUM8:
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.UP:
				setCurrentTeam( getCursorRow() == 0 ? currentTeam + ( TEAMS_IN_GROUP >> 1 ) : currentTeam - ( TEAMS_IN_GROUP >> 1 ) );
				if ( state == STATE_CHOOSE_OPPONENT_TEAM && currentTeam == team1 )
					keyPressed( key );
			break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				switch (state) {
					case STATE_CHOOSE_PLAYER_TEAM:
						GameMIDlet.setScreen(SCREEN_NEW_GAME_MENU);
						break;
					case STATE_CHOOSE_OPPONENT_TEAM:
						setState(STATE_CHOOSE_PLAYER_TEAM);
						break;
				}
			break;

			case ScreenManager.FIRE:
			case ScreenManager.SOFT_KEY_LEFT:
			case ScreenManager.KEY_NUM5:

				switch ( idScreen ) {
					case SCREEN_CHOOSE_COUNTRY_CUP:
						team1 = currentTeam;
						try {
							GameMIDlet.log("SCREEN_CHOOSE_COUNTRY_CUP");
							GameMIDlet.setScreen(SCREEN_NEW_CHAMPIONSHIP);
						} catch (Exception e) {
							GameMIDlet.log("SCREEN_CHOOSE_COUNTRY_CUP");
							GameMIDlet.setScreen(SCREEN_HELP_OBJECTIVES);
						}
					break;

					case SCREEN_CHOOSE_COUNTRY_MULTIPLAYER:
						team1 = currentTeam;
						GameMIDlet.setScreen( SCREEN_START_MULTIPLAYER_MATCH );
					break;

					case SCREEN_CHOOSE_COUNTRY_OPPONENT:
					case SCREEN_CHOOSE_COUNTRY_TRAINING:
						switch (state) {
							case STATE_CHOOSE_PLAYER_TEAM:
								setState( STATE_CHOOSE_OPPONENT_TEAM );
							break;

							case STATE_CHOOSE_OPPONENT_TEAM:
								team2 = currentTeam;
								if ( idScreen == SCREEN_CHOOSE_COUNTRY_OPPONENT )
									GameMIDlet.playMatch( STAGE_VERSUS, 0, Match.MODE_2_PLAYERS, new Team( team1 ), new Team( team2 ) );
								else
									GameMIDlet.playMatch( STAGE_SINGLE_MATCH, 0, Match.MODE_TRAINING, new Team( team1 ), new Team( team2 ) );
							break;
						}
					break;
				}
			break;
		}
	}


	public final void keyReleased(int key) {
	}

	//#if TOUCH == "true"
//#
//# 	public final void onPointerDragged(int x, int y) {
//# 		if ( buttonBack != null ) {
//# 			buttonBack.onPointerDragged( x, y );
//# 		}
//# 	}
//#
//#
//# 	public final void onPointerPressed(int x, int y) {
//# 		if ( flagGroup.contains( x, y ) ) {
//# 			x -= flagGroup.getPosX();
//# 			y -= flagGroup.getPosY();
//#
//# 			final int column = x / ( flagGroup.getWidth() >> 1 );
//# 			final int row = y / ( flagGroup.getHeight() >> 1 );
//#
//# 			final int index = ( ( currentTeam - ( currentTeam % TEAMS_IN_GROUP ) ) + row * 2  + column );
//# 			if ( index == currentTeam )
//# 				keyPressed( ScreenManager.FIRE );
//# 			else if ( state != STATE_CHOOSE_OPPONENT_TEAM && index != team1 )
//# 				setCurrentTeam( index );
//# 		} else if ( y >= arrows.getPosY() && y <= arrows.getPosY() + arrows.getHeight() ) {
//# 			switch ( arrows.getActionAt( x ) ) {
//# 				case ArrowGroup.INDEX_LEFT:
//# 					setCurrentTeam( currentTeam - TEAMS_IN_GROUP );
//# 				break;
//#
//# 				case ArrowGroup.INDEX_RIGHT:
//# 					setCurrentTeam( currentTeam + TEAMS_IN_GROUP );
//# 				break;
//# 			}
//# 		} else if ( buttonBack != null && buttonBack.contains( x, y ) ) {
//# 			buttonBack.onPointerPressed( x, y );
//# 		}
//# 	}
//#
//#
//# 	public final void onPointerReleased(int x, int y) {
//# 		if ( buttonBack != null ) {
//# 			buttonBack.onPointerReleased( x, y );
//# 		}
//# 	}
//#endif

	public final void eventPerformed( Event evt ) {
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				keyPressed( ScreenManager.KEY_BACK );
			break;
		}
	}
	
}