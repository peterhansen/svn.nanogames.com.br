/**
 * ReplayScreen.java
 * ©2008 Nano Games.
 *
 * Created on Apr 3, 2008 1:10:37 PM.
 */


package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import br.com.nanogames.components.util.Serializable;
import core.ArrowGroup;
import core.Border;
import core.Constants;
import core.Replay;
import core.Team;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Calendar;
import java.util.Date;
import javax.microedition.lcdui.Graphics;
//#if TOUCH == "true"
//# import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 * 
 * @author Peter
 */
public final class ReplayScreen extends Container implements Constants, MenuListener, KeyListener
		//#if TOUCH == "true"
//# 		, PointerListener 
		//#endif
{
	
	/** Quantidade total de replays salvos. */
	public static final byte TOTAL_REPLAYS = 12;
	
	/** Quantidade de linhas de ícones dos replays. */
	private byte replayIconsRows;
	
	/** Quantidade de ícones de replays por linha. */
	private byte replayIconsPerRow;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private static final Replay[] replays = new Replay[ TOTAL_REPLAYS ];
	
	private final Menu menuReplays;
	
	private final RichLabel bottomPanel;
	
	private static final byte ICON_INDEX_DEFENDED	= 0;
	private static final byte ICON_INDEX_OUT		= 1;
	private static final byte ICON_INDEX_GOAL		= 2;
	private static final byte ICON_INDEX_POST_BACK	= 3;
	private static final byte ICON_INDEX_POST_OUT	= 4;
	private static final byte ICON_INDEX_POST_GOAL	= 5;
	
	private static final byte ICON_INDEX_TOTAL		= 6;
	
	private final DrawableImage[] icons = new DrawableImage[ ICON_INDEX_TOTAL ];
	
	/** Tela de replay ativa atualmente (SCREEN_SAVE_REPLAY ou SCREEN_LOAD_REPLAY). */
	private final byte screen;
	
	/** Referência para a tela de jogo atual (utilizada para salvar um replay). */
	private final Match match;
	
	private static final byte DISPLAY_MODE_REPLAY_INFO			= 0;
	private static final byte DISPLAY_MODE_REPLAY_SAVED			= 1;
	private static final byte DISPLAY_MODE_CONFIRM_OVERWRITE	= 2;
	
	private static final byte INDEX_CONFIRM_OVERWRITE_YES	= 1;
	private static final byte INDEX_CONFIRM_OVERWRITE_NO	= 2;
	
	/** Modo de visualização atual. */
	private byte displayMode;
	
	/** Velocidade de movimentação do painel inferior, caso o texto não caiba por completo. */
	private final MUV bottomPanelSpeed = new MUV();
	
	/** Limite de movimentação do painel inferior. */
	private int bottomPanelLimit;
	
	/** Tempo de espera para reiniciar o scroll do painel inferior. */
	private static final short BOTTOM_PANEL_WAIT_TIME = 1666;
	
	private int bottomPanelWaitTime;
	
	private static final byte BOTTOM_PANEL_SCROLL_STOPPED	= 0;
	private static final byte BOTTOM_PANEL_SCROLL_MOVING	= 1;
	private static final byte BOTTOM_PANEL_SCROLL_WAITING	= 2;
	
	private byte bottomPanelScrollState;

	private final ArrowGroup arrowGroup;

	private final Border panelBorder;

	private byte visibleIcons;

	private Button[] buttons;

	
	static {
		for ( byte i = 0; i < TOTAL_REPLAYS; ++i )
			replays[ i ] = new Replay();
	}
	
	
	public ReplayScreen( ImageFont font, Match match, int screen, Button[] buttons ) throws Exception {
		super( TOTAL_ITEMS );
		
		this.screen = ( byte ) screen;
		this.match = match;

		setFocusable( true );
		setHandlesInput( true );
		
		// insere o menu de ícones dos replays
		final String path = PATH_IMAGES + "cursor_";
		for ( byte i = 0; i < ICON_INDEX_TOTAL; ++i )
			icons[ i ] = new DrawableImage( path + i + ".png" );
		
		final int ICON_WIDTH = icons[ 0 ].getWidth() + REPLAY_SCREEN_ICON_SPACING;
		final int ICON_HEIGHT = icons[ 0 ].getHeight() + REPLAY_SCREEN_ICON_SPACING;
		
		menuReplays = new Menu( this, DISPLAY_MODE_REPLAY_INFO, TOTAL_REPLAYS );
		menuReplays.setViewport( new Rectangle() );
		menuReplays.setCircular( true );

		final Border b = new Border( Border.BORDER_COLOR_GREEN );
		b.setSize( ICON_WIDTH, ICON_WIDTH );
		b.defineReferencePixel( ANCHOR_CENTER );
		menuReplays.setCursor( b, Menu.CURSOR_DRAW_BEFORE_MENU, ANCHOR_CENTER );
		
		for ( short i = 0; i < TOTAL_REPLAYS; ++i ) {
			final DrawableGroup group = new DrawableGroup( 2 );
			final DrawableImage icon = getReplayIcon( i );
			final Border iconBorder = new Border( Border.BORDER_COLOR_BLACK );
			iconBorder.setSize( icon.getSize().add( BORDER_OFFSET, BORDER_OFFSET ) );
			icon.setPosition( BORDER_OFFSET, BORDER_OFFSET );
			group.insertDrawable( iconBorder );
			group.insertDrawable( icon );
			group.setSize( iconBorder.getSize() );
			menuReplays.insertDrawable( group );
		}
		
		insertDrawable( menuReplays );
		
		panelBorder = new Border( Border.BORDER_COLOR_BLACK );
		insertDrawable( panelBorder );

		bottomPanel = new RichLabel( GameMIDlet.GetFont( FONT_INDEX_TITLE ), null, size.x, null );
		insertDrawable( bottomPanel );

		arrowGroup = new ArrowGroup();
		insertDrawable( arrowGroup );

		this.buttons = buttons;

		setDisplayMode( DISPLAY_MODE_REPLAY_INFO );
	}


	private final DrawableImage getReplayIcon( int replayIndex ) {
		switch ( replays[ replayIndex ].playResult ) {
			case BALL_STATE_NONE:
				final DrawableImage empty = new DrawableImage( icons[ 0 ] );
				empty.setVisible( false );
				return empty;
			
			case BALL_STATE_GOAL:
				replayIndex = ICON_INDEX_GOAL;
			break;
			
			case BALL_STATE_POST_GOAL:
				replayIndex = ICON_INDEX_POST_GOAL;
			break;
			
			case BALL_STATE_OUT:
				replayIndex = ICON_INDEX_OUT;
			break;
			
			case BALL_STATE_POST_OUT:
				replayIndex = ICON_INDEX_POST_OUT;
			break;
			
			case BALL_STATE_POST_BACK:
				replayIndex = ICON_INDEX_POST_BACK;
			break;
			
			case BALL_STATE_REJECTED_KEEPER:
				replayIndex = ICON_INDEX_DEFENDED;
			break;
			
			default:
				return null;
		}
		return new DrawableImage( icons[ replayIndex ] );
	}
	
	
	public static final void saveReplays() {
		try {
			GameMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_REPLAYS, new ReplayManager() );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}
	
	
	public static final void loadReplays() {
		try {
			GameMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_REPLAYS, new ReplayManager() );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}
	
	
	/**
	 * Classe interna utilizada para gravar e ler todos os replays.
	 */
	private static final class ReplayManager implements Serializable {

		public final void write( DataOutputStream output ) throws Exception {
			for ( byte i = 0; i < TOTAL_REPLAYS; ++i )
				replays[ i ].write( output );
		}


		public final void read( DataInputStream input ) throws Exception {
			for ( byte i = 0; i < TOTAL_REPLAYS; ++i )
				replays[ i ].read( input );			
		}
		
	} // fim da classe interna ReplayManager


	public final void keyPressed( int key ) {
		switch ( displayMode ) {
			case DISPLAY_MODE_REPLAY_INFO:
				switch ( key ) {
					case ScreenManager.KEY_NUM7:
						keyPressed( ScreenManager.DOWN );
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						menuReplays.previousIndex();
					break;

					case ScreenManager.KEY_NUM3:
						keyPressed( ScreenManager.UP );			
					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						menuReplays.nextIndex();
					break;

					case ScreenManager.KEY_NUM1:
						keyPressed( ScreenManager.LEFT );			
					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						if ( menuReplays.getCurrentIndex() < replayIconsPerRow )
							setHandlesInput( false );
						else
							menuReplays.setCurrentIndex( ( TOTAL_REPLAYS + menuReplays.getCurrentIndex() - replayIconsPerRow ) % TOTAL_REPLAYS );
					break;

					case ScreenManager.KEY_NUM9:
						keyPressed( ScreenManager.RIGHT );			
					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						if ( menuReplays.getCurrentIndex() >= TOTAL_REPLAYS - replayIconsPerRow )
							setHandlesInput( false );
						else
							menuReplays.setCurrentIndex( ( menuReplays.getCurrentIndex() + replayIconsPerRow ) % TOTAL_REPLAYS );
					break;

					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:
						menuReplays.keyPressed( ScreenManager.FIRE );
					break;

					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						switch ( screen ) {
							case SCREEN_SAVE_REPLAY:
								GameMIDlet.setScreen( SCREEN_MATCH );
							break;

							case SCREEN_LOAD_REPLAY:
								GameMIDlet.setScreen( SCREEN_MAIN_MENU );
							break;
						}
					break;
				} // fim switch ( key )				
			break; // fim case DISPLAY_MODE_REPLAY_INFO
			
			case DISPLAY_MODE_CONFIRM_OVERWRITE:
				switch ( key ) {
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM5:						
						onChoose( null, 0, INDEX_CONFIRM_OVERWRITE_YES );
					break;
					
					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						setDisplayMode( DISPLAY_MODE_REPLAY_INFO );
					break;
				} // fim switch ( key )					
			break;
			
			case DISPLAY_MODE_REPLAY_SAVED:
				GameMIDlet.setScreen( SCREEN_MATCH );
			break;
		}
	}


	public final void keyReleased( int key ) {
	}


	public final void setFocus( boolean focus ) {
		super.setFocus( focus );
		if ( hasFocus )
			setHandlesInput( true );
	}


	//#if SCREEN_SIZE != "SMALL"
	
		public final void onPointerDragged( int x, int y ) {
			final int index = getReplayIndexAt( x, y );

			if ( index >= 0 )
				menuReplays.setCurrentIndex( index );
		}


		public final void onPointerPressed( int x, int y ) {
			final int index = getReplayIndexAt( x, y );
			if ( index >= 0 ) {
				if ( menuReplays.getCurrentIndex() == index )
					keyPressed( ScreenManager.FIRE );
				else
					menuReplays.setCurrentIndex( index );
			} else {
				if ( y >= arrowGroup.getPosY() && y <= arrowGroup.getPosY() + arrowGroup.getHeight() ) {
					switch ( arrowGroup.getActionAt( x ) ) {
						case ArrowGroup.INDEX_LEFT:
							keyPressed( ScreenManager.LEFT );
						break;

						case ArrowGroup.INDEX_RIGHT:
							keyPressed( ScreenManager.RIGHT );
						break;
					}
				}
			}
		}


	public final void onPointerReleased( int x, int y ) {
	}
	
	
	//#endif


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( screen ) {
			case SCREEN_SAVE_REPLAY:
				switch ( displayMode ) {
					case DISPLAY_MODE_REPLAY_INFO:
						if ( replays[ index ].playResult == BALL_STATE_NONE ) {
							saveReplay( index );
							
							setDisplayMode( DISPLAY_MODE_REPLAY_SAVED );
						} else {
							// pede para confirmar no caso de slot já estar gravado
							setDisplayMode( DISPLAY_MODE_CONFIRM_OVERWRITE );
						}
					break;
					
					case DISPLAY_MODE_CONFIRM_OVERWRITE:
						switch ( index ) {
							case INDEX_CONFIRM_OVERWRITE_YES:
								saveReplay( menuReplays.getCurrentIndex() );
								
								setDisplayMode( DISPLAY_MODE_REPLAY_SAVED );
							break;
						}
					break;
				}
			break;
		
			case SCREEN_LOAD_REPLAY:
				if ( replays[ index ].playResult != BALL_STATE_NONE )
					GameMIDlet.playMatch( replays[ index ] );
			break;
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		// atualiza informação do replay selecionado atualmente
		if ( id == DISPLAY_MODE_REPLAY_INFO )
			updateReplayInfoLabel();

		final Drawable entry = menu.getDrawable( menu.getCurrentIndex() );
		final int ICON_SCREEN_X = getPosX() + menuReplays.getPosX() + entry.getPosX();
		
		final int ICON_WIDTH = menuReplays.getDrawable( 0 ).getWidth();
		final int ICON_WIDTH_TOTAL = ICON_WIDTH + REPLAY_SCREEN_ICON_SPACING;

		final int COLUMN = entry.getPosX() / ICON_WIDTH_TOTAL;
		
		final int START_X = menuReplays.getViewport().x - BORDER_OFFSET;
		final int END_X = menuReplays.getViewport().x + menuReplays.getViewport().width - ICON_WIDTH_TOTAL;

		if ( ICON_SCREEN_X < START_X ) {
			menu.setPosition( START_X - getPosX() - COLUMN * ICON_WIDTH_TOTAL, menu.getPosY() );
		} else if ( ICON_SCREEN_X > END_X ) {
			menu.setPosition( START_X - getPosX() - ( COLUMN + 1 - visibleIcons ) * ICON_WIDTH_TOTAL, menu.getPosY() );
		}

		for ( byte i = 0; i < menu.getUsedSlots(); ++i ) {
			final DrawableGroup g = ( DrawableGroup ) menu.getDrawable( i );
			g.getDrawable( 0 ).setVisible( i != index );
		}
	}
	
	
	private final void saveReplay( int index ) {
		match.saveReplayData( replays[ index ] );
		saveReplays();

		// atualiza o ícone do replay
		try {
			final DrawableImage icon = getReplayIcon( index );
			final Point pos = menuReplays.removeDrawable( index ).getPosition();
			icon.setPosition( pos );
			menuReplays.insertDrawable( icon, index );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}		
	}
	
	
	private final int getReplayIndexAt( int x, int y ) {
		if ( menuReplays.getViewport().contains( x, y ) ) {
			x -= menuReplays.getPosX();
			y -= menuReplays.getPosY();

			for ( byte i = 0; i < TOTAL_REPLAYS; ++i ) {
				if ( menuReplays.getDrawable( i ).contains( x, y ) )
					return i;
			}
		}
		
		return -1;
	}
	
	
	private final void setDisplayMode( byte displayMode ) {
		buttons[ 1 ].setVisible( true );
		
		switch ( displayMode ) {
			case DISPLAY_MODE_REPLAY_INFO:
				if ( screen == SCREEN_SAVE_REPLAY )
					buttons[ 0 ].setText( GameMIDlet.getText( TEXT_SAVE ) );
				else
					buttons[ 0 ].setText( GameMIDlet.getText( TEXT_LOAD ) );
				
				updateReplayInfoLabel();

				buttons[ 1 ].setText( GameMIDlet.getText( TEXT_BACK ) );
			break;
			
			case DISPLAY_MODE_CONFIRM_OVERWRITE:
				bottomPanel.setText( TEXT_CONFIRM_OVERWRITE );
				
				buttons[ 0 ].setText( GameMIDlet.getText( TEXT_YES ) );
				buttons[ 1 ].setText( GameMIDlet.getText( TEXT_NO ) );
				checkBottomPanelHeight();
			break;
			
			case DISPLAY_MODE_REPLAY_SAVED:
				bottomPanel.setText( TEXT_REPLAY_SAVED );

				buttons[ 0 ].setText( GameMIDlet.getText( TEXT_OK ) );
				buttons[ 1 ].setVisible( false );
				checkBottomPanelHeight();
			break;
		}
		
		this.displayMode = displayMode;
	}


	private final void updateReplayInfoLabel() {
		final StringBuffer text = new StringBuffer( 100 );
		
		text.append( GameMIDlet.getText( TEXT_REPLAY_NUMBER ) );
		text.append( ( menuReplays.getCurrentIndex() + 1 ) );
		text.append( '\n' );
		
		final Replay replay = replays[ menuReplays.getCurrentIndex() ];
		if ( replay.playResult == BALL_STATE_NONE ) {
			// o replay está vazio
			text.append( GameMIDlet.getText( TEXT_EMPTY ) );
		} else {
			// mostra as informações do replay salvo
			text.append( Team.getName( replay.shooterTeamIndex ) );
			text.append( " X " );
			text.append( Team.getName( replay.keeperTeamIndex ) );
			text.append( '\n' );
			
			switch ( replay.playResult ) {
				case BALL_STATE_GOAL:
					text.append( GameMIDlet.getText( TEXT_GOAL ) );
				break;
				
				case BALL_STATE_POST_GOAL:
					text.append( GameMIDlet.getText( TEXT_POST_GOAL ) );
				break;
				
				case BALL_STATE_OUT:
					text.append( GameMIDlet.getText( TEXT_OUT ) );
				break;
				
				case BALL_STATE_POST_OUT:
					text.append( GameMIDlet.getText( TEXT_POST_OUT ) );
				break;
				
				case BALL_STATE_POST_BACK:
					text.append( GameMIDlet.getText( TEXT_POST_BACK ) );
				break;
				
				case BALL_STATE_REJECTED_KEEPER:
					text.append( GameMIDlet.getText( TEXT_DEFENDED ) );
				break;
			}
			text.append( '\n' );
			
			// formata a data
			text.append( GameMIDlet.formatTime( replay.saveTime, GameMIDlet.getText( TEXT_TIME_FORMAT ) ) );
		}
		
		bottomPanel.setText( text.toString() );
		checkBottomPanelHeight();
	} // fim do método updateReplayInfoLabel()
	
	
	private final void checkBottomPanelHeight() {
		final int textTotalHeight = bottomPanel.getTextTotalHeight();
		if ( textTotalHeight > bottomPanel.getHeight() ) {
			// as informações do replay não couberam na parte inferior da tela
			bottomPanelLimit = bottomPanel.getHeight() - textTotalHeight;
			
			bottomPanelScrollState = BOTTOM_PANEL_SCROLL_WAITING;
			bottomPanelWaitTime = BOTTOM_PANEL_WAIT_TIME;
		} else {
			bottomPanelScrollState = BOTTOM_PANEL_SCROLL_STOPPED;
		}
		bottomPanel.setTextOffset( 0 );		
	}


	public final void update( int delta ) {
		super.update( delta );
		
		final int FONT_HEIGHT = GameMIDlet.GetFont( FONT_INDEX_DEFAULT ).getHeight();
		switch ( bottomPanelScrollState ) {
			case BOTTOM_PANEL_SCROLL_MOVING:
				bottomPanel.setTextOffset( bottomPanel.getTextOffset() + bottomPanelSpeed.updateInt( delta ) );
				
				if ( bottomPanel.getTextOffset() < bottomPanelLimit ) {
					bottomPanel.setTextOffset( bottomPanelLimit );
					bottomPanelScrollState = BOTTOM_PANEL_SCROLL_WAITING;
					bottomPanelWaitTime = BOTTOM_PANEL_WAIT_TIME;
					bottomPanelSpeed.setSpeed( FONT_HEIGHT );
				} else if ( bottomPanel.getTextOffset() > 0 ) {
					bottomPanel.setTextOffset( 0 );
					bottomPanelScrollState = BOTTOM_PANEL_SCROLL_WAITING;
					bottomPanelWaitTime = BOTTOM_PANEL_WAIT_TIME;
					bottomPanelSpeed.setSpeed( -FONT_HEIGHT );
				}
			break;
			
			case BOTTOM_PANEL_SCROLL_WAITING:
				bottomPanelWaitTime -= delta;
				if ( bottomPanelWaitTime <= 0 ) { 
					bottomPanelScrollState = BOTTOM_PANEL_SCROLL_MOVING;
					bottomPanelSpeed.setSpeed( bottomPanel.getTextOffset() >= 0 ? -FONT_HEIGHT : FONT_HEIGHT );
				}
			break;
		}
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( width > height + 10 ) {
			replayIconsRows = 2;
		} else {
			replayIconsRows = 3;
		}
		replayIconsPerRow = ( byte ) ( TOTAL_REPLAYS / replayIconsRows );

		final int ICON_WIDTH = menuReplays.getDrawable( 0 ).getWidth();
		final int ICON_HEIGHT = menuReplays.getDrawable( 0 ).getHeight();
		final int ICON_WIDTH_TOTAL = ICON_WIDTH + REPLAY_SCREEN_ICON_SPACING;
		final int ICON_HEIGHT_TOTAL = ICON_HEIGHT + REPLAY_SCREEN_ICON_SPACING;

		for ( short i = 0, index = 0, x = 0, y = 0; i < replayIconsRows; ++i, x = 0, y += ICON_HEIGHT_TOTAL ) {
			for ( short j = 0; j < replayIconsPerRow; ++j, ++index, x += ICON_WIDTH_TOTAL ) {
				final Drawable icon = menuReplays.getDrawable( index );
				icon.setPosition( x, y );
			}
		}

		menuReplays.setSize( ( ICON_WIDTH_TOTAL * ( replayIconsPerRow - 1 ) ) + ( ICON_WIDTH ), ( ICON_HEIGHT_TOTAL * replayIconsRows ) + ( BORDER_OFFSET << 1 ) );
		menuReplays.defineReferencePixel( ANCHOR_HCENTER );
		menuReplays.setRefPixelPosition( size.x >> 1, 0 );

		arrowGroup.setSize( width, menuReplays.getHeight() );
		arrowGroup.setPosition( 0, menuReplays.getPosY() );

		final Point pos = new Point();
		getAbsolutePos( pos );
		visibleIcons = ( byte ) ( arrowGroup.getInternalWidth() / ICON_WIDTH_TOTAL );
		final int INTERNAL_WIDTH = ( visibleIcons - 1 ) * ICON_WIDTH_TOTAL + ICON_WIDTH;
		menuReplays.getViewport().set( ( ScreenManager.SCREEN_WIDTH - INTERNAL_WIDTH ) >> 1,
									   pos.y + menuReplays.getPosY(),
									   INTERNAL_WIDTH,
									   menuReplays.getHeight() );

		panelBorder.setSize( Math.min( menuReplays.getWidth(), arrowGroup.getInternalWidth() ), size.y - menuReplays.getHeight() - menuReplays.getPosY() - BORDER_OFFSET );
		
		panelBorder.setPosition( ( getWidth() - panelBorder.getWidth() ) >> 1, menuReplays.getPosY() + menuReplays.getHeight() + BORDER_OFFSET );

		bottomPanel.setSize( panelBorder.getSize().sub( BORDER_OFFSET << 1, BORDER_OFFSET << 1 ) );
		bottomPanel.setPosition( panelBorder.getPosition().add( BORDER_OFFSET, BORDER_OFFSET ) );

		menuReplays.setCurrentIndex( menuReplays.getCurrentIndex() );

		updateReplayInfoLabel();
	}

}

