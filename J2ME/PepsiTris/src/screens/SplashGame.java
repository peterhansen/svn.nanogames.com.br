/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import core.Constants;
import br.com.nanogames.components.util.Rectangle;

//#if SCREEN_SIZE != "SMALL"
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
//#endif

/**
 *
 * @author Peter
 */
public final class SplashGame extends DrawableGroup implements Constants, Updatable, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener, ScreenListener
		//#endif
{

	private final int[][] BLOCK_COLORS = {
		// verde
		{ 0x2A4A0E, 0x315611, 0x345C12, 0x386413, 0x3D6C15, 0x467D18, 0x52911C, 0x66B523 },
		// vermelho
		{ 0x41040B, 0x4B040D, 0x51050E, 0x58060E, 0x5E0510, 0x6E0712, 0x7E0715, 0x9E091A },
		// azul
		{ 0x064E34, 0x075741, 0x076141, 0x09614F, 0x096D51, 0x0B7963, 0x0E7F81, 0x129FA1 },
		// amarelo
		{ 0x554E08, 0x5E5B0A, 0x64620A, 0x6C6A0A, 0x75720C, 0x87840D, 0x9D990F, 0xC4BF13 },
	};

	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 120;

	private final short TIME_BLOCKS_APPEARING = 2000;

	/***/
	private final short TIME_SHOW_PEPSI_BLOCK = 420;
	private final short HALF_TIME_SHOW_PEPSI_BLOCK = TIME_SHOW_PEPSI_BLOCK >> 1;

	private final short VIBRATION_TIME_PEPSI_BLOCK = 300;
	
	private short accTime;

	private final Pattern bkg;

	private final DrawableImage[] blocks;

	private final DrawableImage[] pepsiBlocks;

	private static final byte STATE_BLOCKS_APPEARING	= 0;
	private static final byte STATE_SHOW_PEPSI_1		= 1;
	private static final byte STATE_SHOW_PEPSI_2		= 2;
	private static final byte STATE_SHOW_PEPSI_3		= 3;
	private static final byte STATE_PRESS_ANY_KEY		= 4;

	private byte state;
	
	
	public SplashGame() throws Exception {
		super( TOTAL_ITEMS );

		bkg = new Pattern( new DrawableImage( PATH_IMAGES + "bkg.png" ) );
		insertDrawable( bkg );

		setSize( ScreenManager.SCREEN_WIDTH + 4, ScreenManager.SCREEN_HEIGHT + 4 );

		int x = 0;
		int y = 0;

		final byte offset = ( byte ) ( GameMIDlet.getLanguage() == LANGUAGE_ES ? 0 : 3 );

		pepsiBlocks = new DrawableImage[ 3 ];
		for ( byte i = 2; i >= 0; --i ) {
			pepsiBlocks[ i ] = new DrawableImage( PATH_SPLASH + "p_" + ( i < 2 ? i + offset : i ) + ".png" );
			pepsiBlocks[ i ].setVisible( false );
			insertDrawable( pepsiBlocks[ i ] );
		}
		final int TOTAL_PEPSI_WIDTH = BLOCK_PEPSI_OFFSET_X + pepsiBlocks[ 2 ].getWidth();
		pepsiBlocks[ 0 ].setPosition( ( size.x - TOTAL_PEPSI_WIDTH ) >> 1, size.y >> 2 );
		pepsiBlocks[ 1 ].setPosition( pepsiBlocks[ 0 ].getPosX() + BLOCK_BIEN_OFFSET_X, pepsiBlocks[ 0 ].getPosY() );
		pepsiBlocks[ 2 ].setPosition( pepsiBlocks[ 0 ].getPosX() + BLOCK_PEPSI_OFFSET_X, pepsiBlocks[ 0 ].getPosY() + BLOCK_PEPSI_OFFSET_Y );

		final DrawableImage[] blocksTemp = new DrawableImage[ TOTAL_BLOCKS * BLOCK_COLORS.length ];
		blocks = new DrawableImage[ blocksTemp.length * 3 ];
		final Rectangle[] ranges = new Rectangle[ blocksTemp.length ];
		for ( int i = 0, blockIndex = 0; blockIndex < TOTAL_BLOCKS; ++blockIndex ) {
			final String PATH_BLOCK = PATH_IMAGES + "b_" + blockIndex + ".png";
			blocksTemp[ i ] = new DrawableImage( PATH_BLOCK );
			
			for ( byte j = 0; j < BLOCK_COLORS.length; ++j ) {
				if ( j > 0 ) {
					final PaletteMap[] maps = new PaletteMap[ BLOCK_COLORS[ j ].length ];
					for ( byte k = 0; k < maps.length; ++k )
						maps[ k ] = new PaletteMap( BLOCK_COLORS[ 0 ][ k ], BLOCK_COLORS[ j ][ k ] );

					final PaletteChanger paletteChanger = new PaletteChanger( PATH_BLOCK );
					blocksTemp[ i ] = new DrawableImage( paletteChanger.createImage( maps ) );
				}

				ranges[ i ] = new Rectangle( -( blocksTemp[ i ].getWidth() >> 1 ), -( blocksTemp[ i ].getHeight() >> 1 ),
											 ScreenManager.SCREEN_WIDTH + blocksTemp[ i ].getWidth(), ScreenManager.SCREEN_HEIGHT + blocksTemp[ i ].getHeight() );
				++i;
			}
		}

		final int limit = TOTAL_BLOCKS * BLOCK_COLORS.length * 3;
		for ( byte i = 0; i < limit; ++i ) {
			final int type = NanoMath.randInt( blocksTemp.length );
			final DrawableImage block = new DrawableImage( blocksTemp[ type ] );

			do {
				x = ranges[ type ].x + NanoMath.randInt( ranges[ type ].width ) + ( block.getWidth() >> 1 );
				y = ranges[ type ].y + NanoMath.randInt( ranges[ type ].height ) + ( block.getHeight() >> 1 );
			} while ( pepsiBlocks[ 0 ].contains( x, y ) || pepsiBlocks[ 1 ].contains( x, y ) || pepsiBlocks[ 2 ].contains( x, y ) );

			block.setPosition( x - ( block.getWidth() >> 1 ), y - ( block.getHeight() >> 1 ) );

			if ( NanoMath.randInt( 128 ) < 64 )
				block.mirror( TRANS_MIRROR_H );

			insertDrawable( block );
			block.setVisible( false );

			blocks[ i ] = block;
		}

		for ( byte i = 2; i >= 0; --i ) {
			insertDrawable( pepsiBlocks[ i ] );
		}
	}


	public final void update( int delta ) {
		accTime += delta;
		switch ( state ) {
			case STATE_BLOCKS_APPEARING:
				if ( accTime < TIME_BLOCKS_APPEARING ) {
					final int blocksVisible = accTime * blocks.length / TIME_BLOCKS_APPEARING;
					for ( byte i = 0; i < blocksVisible; ++i ) {
						blocks[ i ].setVisible( true );
					}
				} else {
					setState( STATE_SHOW_PEPSI_1 );
				}
			break;

			case STATE_SHOW_PEPSI_1:
			case STATE_SHOW_PEPSI_2:
			case STATE_SHOW_PEPSI_3:
				if ( accTime >= TIME_SHOW_PEPSI_BLOCK )
					setState( state + 1 );
				else if ( accTime <= HALF_TIME_SHOW_PEPSI_BLOCK )
					setPosition( position.x >= 0 ? -2 : 2, position.y >= 0 ? -2 : 2 );
				else
					setPosition( 2, 2 );
			break;
		}
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;

		switch ( state ) {
			case STATE_BLOCKS_APPEARING:
			break;

			case STATE_SHOW_PEPSI_1:
			case STATE_SHOW_PEPSI_2:
			case STATE_SHOW_PEPSI_3:
				pepsiBlocks[ state - STATE_SHOW_PEPSI_1 ].setVisible( true );

				//#ifdef VIBRATION_BUG
//# 					MediaPlayer.vibrate( VIBRATION_TIME_PEPSI_BLOCK / 3 );
				//#else
					MediaPlayer.vibrate( VIBRATION_TIME_PEPSI_BLOCK );
				//#endif
			break;

			case STATE_PRESS_ANY_KEY:
				// mostra o texto "Pressione qualquer tecla"
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, TEXT_PRESS_ANY_KEY, 0 );
				setPosition( 2, 2 );
			break;
		}
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
			// permite pular a tela de splash
			setState( STATE_PRESS_ANY_KEY );
		//#endif
		
		if ( state == STATE_PRESS_ANY_KEY ) {
			//#if DEMO == "true"
//# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
			//#else
				GameMIDlet.setScreen( SCREEN_LOADING_2 );
			//#endif
		}
	}


	public final void keyReleased( int key ) {
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		bkg.setSize( size );
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void hideNotify() {
	}


	public final void showNotify() {
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}

	//#endif

}
