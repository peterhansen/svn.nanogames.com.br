/**
 * PlayScreen.java
 * �2008 Nano Games.
 *
 * Created on Jun 2, 2008 6:50:28 PM.
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import core.PieceGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Border;
import core.Constants;
import core.Piece;
import core.Tag;
import br.com.nanogames.components.util.Mutex;

//#if SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

//#if DEBUG == "true"
	import javax.microedition.lcdui.Graphics;
//#endif

/**
 * 
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"
		, PointerListener
//#endif

{
	public static final byte STATE_NONE						= 0;
	public static final byte STATE_BEGIN_LEVEL				= 1;
	public static final byte STATE_PLAYING					= 2;
	public static final byte STATE_LEVEL_CLEARED			= 3;
	public static final byte STATE_GAME_OVER				= 4;
	public static final byte STATE_PAUSED					= 5;
	public static final byte STATE_UNPAUSING				= 6;
	public static final byte STATE_APPEARING_WORDS			= 7;
	public static final byte STATE_SHOWING_WORDS			= 8;
	public static final byte STATE_HIDING_WORDS				= 9;
	public static final byte STATE_NEW_RECORD				= 10;
	public static final byte STATE_GAME_COMPLETE			= 11;

	private byte state;
	
	private static final byte WORD_INVALID = 0;
	private static final byte WORD_REGULAR = 1;
	private static final byte WORD_REVERSE = 2;
	
	public static final short TIME_MESSAGE = 3000;

	/** Dura��o da transi��o de mostrar/esconder palavras, em milisegundos. */
	public static final short TIME_TRANSITION = 470;
	
	private short timeToNextState;

	//#if SCREEN_SIZE == "SMALL"
//# 		private short timeInfoVisible;
	//#endif

	/** Pontua��o do jogador. */
	private int score;
	
	private byte level;

	/** pontua��o exibida atualmente */
	private int scoreShown;
	
	/** label que indica a pontua��o e o multiplicador */
	private final Label scoreLabel;

	/** Posi��o x (direita) do label da pontua��o. */
	private short scoreLabelX;

	/** Tempo restante. */
	private int timeLeft;

	private final Label labelTime;

	private short timeLabelRightX;


	//#if SCREEN_SIZE == "SMALL"
//# 		private final DrawableImage statusBar;
	//#else
		private final Label labelNextChar;
		private final Label labelLevel;

		private final DrawableImage tagScorePortrait;
		private final DrawableImage tagTimePortrait;
		private final DrawableImage tagNextCharPortrait;

		private final Tag tagScoreLandscape;
		private final Tag tagTimeLandscape;
		private final Tag tagLevelLandscape;
		private final Tag tagNextCharLandscape;
	//#endif


	private final Tag tagWords;
	
	/** Label utilizado para mostrar uma mensagem ao jogador (fim de jogo, n�vel completo, etc.).  */
	private final RichLabel labelMessage;
	
	/** pontua��o m�xima mostrada */
	private static final int SCORE_SHOWN_MAX = 99999;

	/** Velocidade de atualiza��o da pontua��o. */
	private final MUV scoreSpeed = new MUV();

	private static final byte WORD_MIN_LENGTH = 2;
	private static final byte WORD_MAX_LENGTH = 9;

	/** Pontua��o dada por cada tamanho de palavra (come�ando por WORD_MIN_LENGTH) */
	private final short[] SCORES_PER_WORD = { 25, 50, 100, 150, 200, 300, 375, 500, 1000 };

	/** Palavras v�lidas (o tamanho � duplicado pois as palavras podem ser testadas na ordem inversa).
	 * Obs.: n�o foi poss�vel utilizar um Hashtable pois, apesar dos conte�dos das chaves (strings) serem
	 * iguais, os objetos s�o diferentes, logo uma chave usada para busca posterior nunca seria encontrada.
	 */
	private static final String[] words = new String[ TOTAL_WORDS << 1 ];

	/** Array de pe�as j� encaixadas. */
	private final Piece[][] pieces = new Piece[ TOTAL_COLUMNS ][ TOTAL_ROWS ];

	private final PieceGroup piecesGroup;

	private static final byte SCORE_LABELS_MAX = 3;

	private final ScoreLabel[] scoreLabels = new ScoreLabel[ SCORE_LABELS_MAX ];

	private final Pattern bkgTop;
	private final Pattern bkgBottom;
	private final Pattern bkgLeft;
	private final Pattern bkgRight;

	private final Border border;

	private final BasicAnimatedPattern bkgBoard;

	private byte currentBkg = -1;

	/** Velocidade vertical m�nima inicial durante a movimenta��o pe�a, em pixels por segundo */
	private static final short SPEED_MOVE_MIN_EASY = PIECE_HEIGHT * 1000 / 2500;
	/** Velocidade vertical m�xima inicial durante a movimenta��o pe�a, em pixels por segundo */
	private static final short SPEED_MOVE_MAX_EASY = PIECE_HEIGHT * 1000 / 750;

	/** Velocidade vertical m�nima final durante a movimenta��o pe�a, em pixels por segundo */
	private static final short SPEED_MOVE_MIN_HARD = PIECE_HEIGHT * 1000 / 850;
	/** Velocidade vertical m�xima final durante a movimenta��o pe�a, em pixels por segundo */
	private static final short SPEED_MOVE_MAX_HARD = PIECE_HEIGHT * 1000 / 400;

	private static short currentMinSpeed;
	private static short currentMaxSpeed;

	/** Velocidade vertical da pe�a. */
	private final MUV pieceMUV = new MUV();

	/** Pe�a que o jogador movimenta. */
	private final Piece movingPiece;

	/** Tempo de espera at� inserir a nova pe�a (evita problemas com pe�as caindo durante anima��es). */
	private short timeToNextPiece;

	/** Indicador da pr�xima pe�a. */
	private final Piece nextPiece;

	/** Quantidade de palavras usadas no sorteio de novas pe�as. */
	private static final byte WORDS_TO_SORT = 1;

	/** Pr�ximos caracteres a aparecer na tela. */
	private final char[] nextChars = new char[ WORD_MAX_LENGTH * WORDS_TO_SORT ];

	/** Contador de pe�as j� sorteadas a aparecer na tela. Quando o contador vai a zero, � feito um novo sorteio. */
	private byte nextCharsCount;

	/** �ndice da coluna atual da pe�a caindo. */
	private byte movingPieceColumn;

	/** Porcentagem atual da velocidade de queda da pe�a atual. */
	private byte fallSpeedPercent;

	/** Grupo das palavras. */
	private final WordsGroup wordsGroup;

	private static ImageFont FONT_SCORE;

	private static DrawableImage CAE_BIEN_BLOCK;

	/** Offset na posi��o horizontal dos elementos mostrados na tela. */
	private short offsetX;

	/** Limite da movimenta��o horizontal no modo de visualiza��o das palavras. */
	private short limitRight;

	/** Velocidade de movimenta��o da tela de jogo (durante transi��es para mostrar/esconder palavras). */
	private final MUV muv = new MUV();

	/** Tempo em milisegundos necess�rio para fazer o scroll de uma p�gina. */
	private static final short TIME_SCROLL_PAGE = 700;

	/***/
	private boolean hasScrolled;

	//#if SCREEN_SIZE == "BIG"
		private final DrawableImage caeBienLogo;

		private static final short TIME_DOUBLE_CLICK = 220;

		private static final byte PIECE_SPEED_Y_RANGE = 50;

		private long lastPointerPressed;

		private short pointerX;

		private boolean draggingPiece;
	//#endif

	private final DrawableImage caeBienMessage;
	
	private final DrawableImage caeBienBlock;

	private static final short KEY_REPEAT_TIME = 250;

	/** Tecla mantida pressionada. */
	private int keyHeld;

	/** Tempo que a tecla est� sendo pressionada. */
	private short accKeyTime;

	private final Mutex mutexPiece = new Mutex();


	public PlayScreen() throws Exception {
		super( SCORE_LABELS_MAX + 25 );

		final DrawableImage bkg = new DrawableImage( PATH_IMAGES + "bkg.png" );

		bkgTop = new Pattern( bkg );
		bkgBottom = new Pattern( bkg );
		bkgLeft = new Pattern( bkg );
		bkgRight = new Pattern( bkg );

		insertDrawable( bkgTop );
		insertDrawable( bkgBottom );
		insertDrawable( bkgLeft );
		insertDrawable( bkgRight );

		//#if SCREEN_SIZE == "BIG"
			caeBienLogo = new DrawableImage( PATH_IMAGES + "cae_bien_" + GameMIDlet.getLanguage() + ".png" );
			insertDrawable( caeBienLogo );
			caeBienMessage = new DrawableImage( caeBienLogo );
		//#else
//# 			caeBienMessage = new DrawableImage( PATH_IMAGES + "cae_bien_" + GameMIDlet.getLanguage() + ".png" );
		//#endif

		//#if SCREEN_SIZE != "SMALL"

			tagTimePortrait = new DrawableImage( PATH_IMAGES + "t_2.png" );
			insertDrawable( tagTimePortrait );

			tagNextCharPortrait = new DrawableImage( PATH_IMAGES + "t_0.png" );
			insertDrawable( tagNextCharPortrait );

			tagScorePortrait = new DrawableImage( PATH_IMAGES + "t_1.png" );
			insertDrawable( tagScorePortrait );

			tagNextCharLandscape = new Tag( Tag.COLOR_ORANGE, 0, FONT_TEXT_WHITE );
			insertDrawable( tagNextCharLandscape );

			tagScoreLandscape = new Tag( Tag.COLOR_BLUE, 0, FONT_TEXT_WHITE );
			insertDrawable( tagScoreLandscape );

			tagTimeLandscape = new Tag( Tag.COLOR_PINK, 0, FONT_TEXT_WHITE );
			insertDrawable( tagTimeLandscape );

			tagLevelLandscape = new Tag( Tag.COLOR_PINK, 0, FONT_TEXT_WHITE );
			insertDrawable( tagLevelLandscape );

		//#endif
		
		tagWords = new Tag( Tag.COLOR_PINK, WORDS_GROUP_WIDTH, FONT_TEXT_WHITE, TEXT_WORDS );
		tagWords.setText( GameMIDlet.getText( TEXT_WORDS ) + ": 0" );
		insertDrawable( tagWords );

//		Drawable fill = null;
//		final int nextBkg = GameMIDlet.isLowMemory() ? 0 : NanoMath.randInt( GAME_BKG_TYPES_TOTAL );
//		switch ( nextBkg ) {
//			case 0:
//				fill = new DrawableImage( PATH_IMAGES + "bkg_0.png" );
//			break;
//
//			case 1:
//				fill = new Sprite( PATH_IMAGES + "bkg_1" );
//			break;
//
//			case 2:
//				fill = new Sprite( PATH_IMAGES + "bkg_2" );
//			break;
//		}
		bkgBoard = new BasicAnimatedPattern( new DrawableImage( PATH_IMAGES + "bkg_0.png" ), 0, 0 );

		border = new Border( Border.TYPE_SPECIFIC, bkgBoard );
		insertDrawable( border );

		//#if SCREEN_SIZE == "SMALL"
//# 			statusBar = new DrawableImage( PATH_IMAGES + "bar.png" );
//# 			insertDrawable( statusBar );
		//#endif

		movingPiece = new Piece();
		movingPiece.setState( Piece.STATE_MOVING );
		piecesGroup = new PieceGroup( movingPiece );
		insertDrawable( piecesGroup );

		border.setSize( piecesGroup.getSize().add( BORDER_THICKNESS, BORDER_THICKNESS ) );

		// insere o label indicando a pontua��o
		scoreLabel = new Label( FONT_SCORE, "0" );
		insertDrawable( scoreLabel );

		// indicador da pr�xima pe�a
		nextPiece = new Piece();
		nextPiece.setState( Piece.STATE_IDLE );
		nextPiece.setColor( Piece.COLOR_ORANGE );
		insertDrawable( nextPiece );

		labelTime = new Label( FONT_SCORE, "0" );
		insertDrawable( labelTime );
		
		//#if SCREEN_SIZE != "SMALL"
			// insere o label indicando o tempo restante
			labelLevel = new Label( FONT_TEXT_WHITE, TEXT_LEVEL );
			insertDrawable( labelLevel );

			labelNextChar = new Label( FONT_TEXT_WHITE, TEXT_NEXT );
			insertDrawable( labelNextChar );
		//#endif

		for ( byte i = 0; i < SCORE_LABELS_MAX; ++i ) {
			scoreLabels[ i ] = new ScoreLabel( this );
			insertDrawable( scoreLabels[ i ] );
		}

		wordsGroup = new WordsGroup( this );
		insertDrawable( wordsGroup );

		// insere o label que mostra mensagens ao jogador
		insertDrawable( caeBienMessage );
		caeBienBlock = new DrawableImage( CAE_BIEN_BLOCK );
		caeBienBlock.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
		insertDrawable( caeBienBlock );
		labelMessage = new RichLabel( GameMIDlet.getFont( FONT_BIG ), "", getWidth() );
		insertDrawable( labelMessage );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public static final void load() throws Exception {
		//#if SCREEN_SIZE == "SMALL"
//# 			FONT_SCORE = GameMIDlet.getFont( FONT_TEXT_WHITE );
		//#else
			FONT_SCORE = ImageFont.createMonoSpacedFont( PATH_IMAGES + "font_numbers.png", ":0123456789" );
			FONT_SCORE.setCharOffset( 1 );
		//#endif

		CAE_BIEN_BLOCK = new DrawableImage( PATH_IMAGES + "bkg_msg.png" );
	}


	/** Atualiza as palavras de acordo com o idioma atual. Este m�todo deve ser chamado sempre que o idioma
	 * do jogo for alterado.
	 */
	public static final void updateWords() throws Exception {
		final String[] wordsTemp = new String[ TOTAL_WORDS ];
		GameMIDlet.loadTexts( wordsTemp, GameMIDlet.getWordsPath() );

		System.arraycopy( wordsTemp, 0, words, 0, TOTAL_WORDS );
		for ( byte i = 0; i < TOTAL_WORDS; ++i ) {
			// monta a string inversa
			final char[] chars = words[ i ].toCharArray();
			final int limit = chars.length >> 1;
			for ( byte j = 0; j < limit; ++j ) {
				final char c = chars[ j ];
				chars[ j ] = chars[ chars.length - 1 - j ];
				chars[ chars.length - 1 - j ] = c;
			}

			words[ TOTAL_WORDS + i ] = new String( chars );
		}
	}
	
	
	public final void update( int delta ) {
		switch ( state ) {
			case STATE_PAUSED:
			return;

			case STATE_UNPAUSING:
			break;

			case STATE_APPEARING_WORDS:
				move( muv.updateInt( delta ), 0 );

				if ( getPosX() >= 0 )
					setState( STATE_SHOWING_WORDS );
			break;

			case STATE_HIDING_WORDS:
				move( muv.updateInt( delta ), 0 );

				if ( getPosX() <= -offsetX )
					setState( STATE_PLAYING );
			break;

			case STATE_SHOWING_WORDS:
				final int previousX = position.x;
				move( muv.updateInt( delta ), 0 );
				position.x = NanoMath.clamp( position.x, limitRight, 0 );

				if ( previousX != position.x ) {
					hasScrolled = true;

					if ( position.x == 0 || position.x == limitRight )
						keyReleased( 0 );
				}

			break;

			case STATE_PLAYING:
				if ( keyHeld != 0 ) {
					accKeyTime += delta;

					if ( accKeyTime >= KEY_REPEAT_TIME ) {
						accKeyTime %= KEY_REPEAT_TIME;
						keyPressed( keyHeld );
					}
				}

				if ( timeToNextPiece > 0 ) {
					timeToNextPiece -= delta;

					if ( timeToNextPiece <= 0 ){
						timeToNextPiece = 0;

						insertNewPiece();
					}
				} else {
					updateTime( delta );
					
					if ( canMove() ) {
						movingPiece.move( 0, pieceMUV.updateInt( delta ) );

						if ( movingPiece.getPosY() >= movingPiece.getYLimit() ) {
							movingPiece.setPosition( movingPiece.getPosX(), movingPiece.getYLimit() );

							final byte currentRow = getAvailableRowsAtColumn( movingPieceColumn );
							if ( currentRow >= 0 ) {
								final Piece insertedPiece = piecesGroup.insertPiece( movingPiece );
								pieces[ movingPieceColumn ][ currentRow ] = insertedPiece;

								checkWords( movingPieceColumn, currentRow );
							} else {
								// fim de jogo
								setState( STATE_GAME_OVER );
							}

							insertNewPiece();
						}
					}
				}


			default:
				super.update( delta );
		}

		//#if SCREEN_SIZE == "SMALL"
//# 		if ( timeInfoVisible > 0 ) {
//# 			timeInfoVisible -= delta;
//#
//# 			if ( timeInfoVisible <= 0 )
//# 				setInfoVisible( false );
//# 		}
		//#endif
		
		// caso o estado atual tenha uma dura��o m�xima definida, atualiza o contador
		if ( timeToNextState > 0 ) {
			timeToNextState -= delta;
			
			if ( timeToNextState <= 0 )
				stateEnded();
		}

		// atualiza a pontua��o
		updateScore( delta, false );
	}


	//#if DEBUG == "true"
	protected void paint( Graphics g ) {
		super.paint( g );
		g.setColor( 0xff0000 );

		for ( byte i = 0; i < TOTAL_COLUMNS; ++i ) {
			for ( byte j = 0; j < TOTAL_ROWS; ++j ) {
				if ( pieces[ i ][ j ] != null )
					g.drawRect( translate.x + piecesGroup.getPosX() + pieces[ i ][ j ].getPosX(),
								translate.y + piecesGroup.getPosY() + pieces[ i ][ j ].getPosY(),
								pieces[ i ][ j ].getWidth(), pieces[ i ][ j ].getHeight() );
			}
		}
	}
	//#endif


	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_PLAYING:
				switch ( key ) {
					//#if DEBUG == "true"
						case ScreenManager.KEY_NUM9:
							updateTime( 30000 );
						break;

						case ScreenManager.KEY_NUM7:
							changeScore( 100 );
						break;
					//#endif

					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						setKeyHeld( ScreenManager.UP );
						setFallSpeedPercent( fallSpeedPercent - FALL_SPEED_CHANGE_STEP );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						setKeyHeld( ScreenManager.DOWN );
						setFallSpeedPercent( fallSpeedPercent + FALL_SPEED_CHANGE_STEP );
					break;

					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						setKeyHeld( ScreenManager.LEFT );
						setMovingPieceColumn( movingPieceColumn - 1 );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						setKeyHeld( ScreenManager.RIGHT );
						setMovingPieceColumn( movingPieceColumn + 1 );
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
						setKeyHeld( 0 );

						mutexPiece.acquire();
						if ( canMove() )
							movingPiece.setPosition( movingPiece.getPosX(), movingPiece.getYLimit() );
						mutexPiece.release();
					break;

					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
						setKeyHeld( 0 );
						setState( STATE_APPEARING_WORDS );
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
						setKeyHeld( 0 );
						setState( STATE_PAUSED );
					break;
				}
			break;

			case STATE_LEVEL_CLEARED:
			break;

			case STATE_GAME_OVER:
			case STATE_GAME_COMPLETE:
			case STATE_NEW_RECORD:
				stateEnded();
			break;

			case STATE_SHOWING_WORDS:
				switch ( key ) {
					case ScreenManager.RIGHT:
					case ScreenManager.KEY_NUM6:
						hasScrolled = false;
						muv.setSpeed( ScreenManager.SCREEN_WIDTH * -1000 / TIME_SCROLL_PAGE );
					break;
					
					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						hasScrolled = false;
						muv.setSpeed( ScreenManager.SCREEN_WIDTH * 1000 / TIME_SCROLL_PAGE );
					break;

					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
						setState( STATE_HIDING_WORDS );
					break;
				}
			break;

			case STATE_APPEARING_WORDS:
				switch ( key ) {
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
						setState( STATE_HIDING_WORDS );
					break;
				}
			break;

			case STATE_HIDING_WORDS:
				switch ( key ) {
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
						setState( STATE_APPEARING_WORDS );
					break;
				}
			break;
		} // fim switch ( state )
	}


	public final void keyReleased( int key ) {
		setKeyHeld( 0 );

		switch ( state ) {
			case STATE_SHOWING_WORDS:
				if ( !hasScrolled )
					move( muv.getSpeed() > 0 ? ScreenManager.SCREEN_WIDTH * 10 / 100 : ScreenManager.SCREEN_WIDTH * -10 / 100, 0 );

				muv.setSpeed( 0 );
			break;
		}
	}


	private final void setKeyHeld( int key ) {
		keyHeld = key;

		if ( key == 0 )
			accKeyTime = 0;
	}


//#if SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"

		public final void onPointerDragged( int x, int y ) {
			switch ( state ) {
				case STATE_PLAYING:
					if ( draggingPiece ) {
						setPieceColumnPointer( x + offsetX - piecesGroup.getPosX() );
						setFallSpeedPercentPointer( y - piecesGroup.getPosY() );
					}
				break;

				case STATE_SHOWING_WORDS:
					move( x - pointerX, 0 );
					pointerX = ( short ) x;
				break;
			}
		}


		public final void onPointerPressed( int x, int y ) {
			switch ( state ) {
				case STATE_PLAYING:
					x -= piecesGroup.getPosX() - offsetX;
					y -= piecesGroup.getPosY();

					if ( movingPiece.contains( x, y ) ) {
						draggingPiece = true;

						final long now = System.currentTimeMillis();
//						System.out.println( "interval: " + ( now - lastPointerPressed ) );
						if ( now - lastPointerPressed <= TIME_DOUBLE_CLICK ) {
							keyPressed( ScreenManager.FIRE );
						} else {
							setPieceColumnPointer( x );
							setFallSpeedPercentPointer( y );
						}
						lastPointerPressed = now;
					}
				break;

				case STATE_SHOWING_WORDS:
					pointerX = ( short ) x;
				break;

				default:
					keyPressed( ScreenManager.FIRE );
			}
		}


		public final void onPointerReleased( int x, int y ) {
			draggingPiece = false;
		}


		private final void setPieceColumnPointer( int x ) {
			final byte c = ( byte ) ( NanoMath.clamp( x, 0, piecesGroup.getWidth() ) / PIECE_WIDTH );
			boolean ret = true;

			while ( ret && c != movingPieceColumn ) {
				ret = setMovingPieceColumn( movingPieceColumn > c ? movingPieceColumn - 1 : movingPieceColumn + 1 );
			}
		}


		private final void setFallSpeedPercentPointer( int y ) {
			setFallSpeedPercent( 50 + ( ( NanoMath.clamp( y - movingPiece.getRefPixelY(), -PIECE_SPEED_Y_RANGE, PIECE_SPEED_Y_RANGE ) ) * 50 / PIECE_SPEED_Y_RANGE ) );
		}


	//#endif


	private final boolean setMovingPieceColumn( int column ) {
		return setMovingPieceColumn( column, true );
	}


	private final boolean setMovingPieceColumn( int column, final boolean useMutex ) {
		if ( useMutex )
			mutexPiece.acquire();

		try {
			if ( canMove() ) {
				if ( column < 0 || column >= TOTAL_COLUMNS ) {
					// pe�a n�o pode ser movida pois j� est� no limite (esquerdo ou direito)
					MediaPlayer.vibrate( VIBRATION_TIME_CANT_MOVE );
				} else {
					final short newYLimit = ( short ) ( getAvailableRowsAtColumn( column ) * PIECE_HEIGHT );

					if ( newYLimit >= movingPiece.getPosY() ) {
						// pe�a pode ser movida para o lado
						movingPiece.setYLimit( newYLimit );
						movingPieceColumn = ( byte ) column;
						movingPiece.setPosition( column * PIECE_WIDTH, movingPiece.getPosY() );

						return true;
					} else {
						// pe�a n�o pode ser movida pois h� outra pe�a impedindo a movimenta��o horizontal
						MediaPlayer.vibrate( VIBRATION_TIME_CANT_MOVE );
					}
				}
			}
		} finally {
			if ( useMutex )
				mutexPiece.release();
		}

		return false;
	} // fim do m�todo setFallingPieceColumn( int )


	private final boolean canMove() {
		return movingPiece.getState() != Piece.STATE_NONE;
	}


	/**
	 * Obt�m o n�mero de colunas livres em determinada coluna.
	 * @param column �ndice da coluna a ser testada.
	 * @return quantidade de colunas livres na coluna recebida, ou -1 caso todas estejam ocupadas (fim de jogo).
	 */
	private final byte getAvailableRowsAtColumn( int column ) {
		byte index = TOTAL_ROWS - 1;
		while ( index >= 0 && pieces[ column ][ index ] != null )
			--index;

		return index;
	}


	private final void setFallSpeedPercent( int percent ) {
		fallSpeedPercent = ( byte ) NanoMath.clamp( percent, 0, 100 );
		System.out.println( "percent: " + fallSpeedPercent );
		pieceMUV.setSpeed( currentMinSpeed + ( ( currentMaxSpeed - currentMinSpeed ) * fallSpeedPercent / 100 ), false );
	}


	private final ScoreLabel getNextScoreLabel() {
		byte candidate = 0;
		for ( byte i = 0; i < SCORE_LABELS_MAX; ++i ) {
			switch ( scoreLabels[ i ].state ) {
				case ScoreLabel.STATE_IDLE:
					return scoreLabels[ i ];

				case ScoreLabel.STATE_VANISHING:
					candidate = i;
				break;
			}
		}

		return scoreLabels[ candidate ];
	}


	public final void setState( int state ) {
		final byte previousState = this.state;
		this.state = ( byte ) state;
		
		timeToNextState = 0;
		labelMessage.setVisible( false );
		caeBienBlock.setVisible( false );
		caeBienMessage.setVisible( false );

		//#if SCREEN_SIZE == "SMALL"
//# 			setInfoVisible( false );
//# 			timeInfoVisible = 0;
		//#endif
		
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				showMessage( GameMIDlet.getText( TEXT_LEVEL_CAPS ) + level );

				insertNewPiece();
				
				timeToNextState = TIME_MESSAGE;

				setPosition( -offsetX, 0 );
			break;
			
			case STATE_PLAYING:
				setKeyHeld( 0 );
				setPosition( -offsetX, 0 );

				//#if SCREEN_SIZE == "SMALL"
//# 					timeInfoVisible = TIME_MESSAGE;
//# 					setInfoVisible( true );
				//#elif SCREEN_SIZE == "BIG" || WEB_EMULATOR == "true"
					onPointerReleased( 0, 0 );
				//#endif


				if ( previousState != STATE_HIDING_WORDS && !MediaPlayer.isPlaying() )
					MediaPlayer.play( SOUND_INDEX_AMBIENT_1 + NanoMath.randInt( 3 ), MediaPlayer.LOOP_INFINITE );

				final int speedX = ( GAME_BKG_SPEED_PERCENT_MIN + NanoMath.randInt( GAME_BKG_SPEED_PERCENT_MAX - GAME_BKG_SPEED_PERCENT_MIN ) ) * Math.max( ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_WIDTH ) / 100;
				final int speedY = ( GAME_BKG_SPEED_PERCENT_MIN + NanoMath.randInt( GAME_BKG_SPEED_PERCENT_MAX - GAME_BKG_SPEED_PERCENT_MIN ) ) * Math.max( ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_WIDTH ) / 100;
				bkgBoard.setSpeed( speedX, speedY );

				byte animationType = -1;
//				switch ( currentBkg ) {
//					case 0:
						do {
							animationType = ( byte ) NanoMath.randInt( BasicAnimatedPattern.TOTAL_ANIMATIONS );
						} while ( animationType == BasicAnimatedPattern.ANIMATION_ALTERNATE_HORIZONTAL || animationType == BasicAnimatedPattern.ANIMATION_ALTERNATE_VERTICAL );
//					break;
//
//					case 1:
//						animationType = BasicAnimatedPattern.ANIMATION_RANDOM;
//					break;
//
//					case 2:
//						do {
//							animationType = ( byte ) NanoMath.randInt( BasicAnimatedPattern.TOTAL_ANIMATIONS );
//						} while ( animationType == BasicAnimatedPattern.ANIMATION_ALTERNATE_HORIZONTAL || animationType == BasicAnimatedPattern.ANIMATION_ALTERNATE_VERTICAL );
//					break;
//				}
				bkgBoard.setAnimation( animationType );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, TEXT_WORDS );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_PAUSE );
			break;

			case STATE_GAME_OVER:
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ) );
				MediaPlayer.play( SOUND_INDEX_GAME_OVER );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;
			
			case STATE_LEVEL_CLEARED:
				showMessage( GameMIDlet.getText( TEXT_LEVEL_CAPS ) + level + GameMIDlet.getText( TEXT_COMPLETE ) );
				timeToNextState = TIME_MESSAGE;

				updateScore( 0, true );

				MediaPlayer.play( SOUND_INDEX_LEVEL_COMPLETE );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;

			case STATE_PAUSED:
				GameMIDlet.setScreen( SCREEN_PAUSE );
			break;

			case STATE_UNPAUSING:
				//#if SCREEN_SIZE == "SMALL"
//# 					setInfoVisible( true );
				//#endif

				timeToNextState = WATER_TRANSITION_TIME;
			break;

			case STATE_APPEARING_WORDS:
				muv.setSpeed( offsetX * 1000 / TIME_TRANSITION );
			break;

			case STATE_HIDING_WORDS:
				muv.setSpeed( -offsetX * 1000 / TIME_TRANSITION );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;

			case STATE_SHOWING_WORDS:
				setPosition( 0, 0 );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, TEXT_BACK );
			break;

			case STATE_NEW_RECORD:
				showMessage( GameMIDlet.getText( TEXT_NEW_RECORD ) );
			break;

			case STATE_GAME_COMPLETE:
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ) );
				MediaPlayer.play( SOUND_INDEX_GAME_COMPLETE );

				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, -1 );
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break;
		}
	} // fim do m�todo setState( int )
	
	
	private final void stateEnded() {
		switch ( state ) {
			case STATE_LEVEL_CLEARED:
				prepareNextLevel();
			break;
			
			case STATE_BEGIN_LEVEL:
			case STATE_UNPAUSING:
				setState( STATE_PLAYING );
			break;
			
			case STATE_GAME_OVER:
			case STATE_GAME_COMPLETE:
				if ( HighScoresScreen.isHighScore( score ) >= 0 )
					setState( STATE_NEW_RECORD );
				else
					GameMIDlet.gameOver( score );
			break;

			case STATE_NEW_RECORD:
				GameMIDlet.gameOver( score );
			break;
		}
	}


	public final void prepareNextLevel() {
		prepareLevel( level + 1 );
	}
	
	
	public final short getLevel() {
		return level;
	}


	public final void setPosition( int x, int y ) {
		super.setPosition( x, y );
		Piece.setOffset( x + piecesGroup.getPosX(), y + piecesGroup.getPosY() );

		switch ( state ) {
			case STATE_SHOWING_WORDS:
				position.x = NanoMath.clamp( position.x, limitRight, 0 );
			break;
		}
	}


	private final void prepareLevel( int level ) {
		this.level = ( byte ) level;

		//#if SCREEN_SIZE != "SMALL"
			labelLevel.setText( GameMIDlet.getText( TEXT_LEVEL ) + level, true );
		//#endif

		for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
			for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
				pieces[ c ][ r ] = null;
			}
		}

		piecesGroup.reset();

		currentMinSpeed = ( short ) ( SPEED_MOVE_MIN_EASY + ( SPEED_MOVE_MIN_HARD - SPEED_MOVE_MIN_EASY ) * level / TOTAL_LEVELS );
		currentMaxSpeed = ( short ) ( SPEED_MOVE_MAX_EASY + ( SPEED_MOVE_MAX_HARD - SPEED_MOVE_MAX_EASY ) * level / TOTAL_LEVELS );
		setFallSpeedPercent( 50 );

		timeLeft = LEVEL_TIME;
		updateTime( 0 );

		movingPiece.setPosition( 0, -( ( PIECE_HEIGHT * 3 ) >> 1 ) );

		nextCharsCount = 0;
		sortNextPieces();

		timeToNextPiece = 0;
		insertNewPiece();

		setState( STATE_BEGIN_LEVEL );
	}


	private final void insertNewPiece() {
		mutexPiece.acquire();

		//#if SCREEN_SIZE == "BIG"
			onPointerReleased( 0, 0 );
		//#endif

		if ( timeToNextPiece <= 0 ) {
			movingPiece.setPosition( 0, -( ( PIECE_HEIGHT * 3 ) >> 1 ) );
			movingPiece.setState( Piece.STATE_MOVING );
			movingPiece.setCharIndex( nextPiece.getCharIndex() );
			setMovingPieceColumn( TOTAL_COLUMNS >> 1, false );

			char nextChar = nextChars[ --nextCharsCount ];
			if ( nextCharsCount <= 0 )
				sortNextPieces();

			nextPiece.setChar( nextChar );
		} else {
			movingPiece.setState( Piece.STATE_NONE );
		}

		mutexPiece.release();
	}


	private final void sortNextPieces() {
		for ( byte i = 0; i < WORDS_TO_SORT; ++i ) {
			final String word = words[ NanoMath.randInt( TOTAL_WORDS ) ];
			final char[] chars = word.toCharArray();
			System.arraycopy( chars, 0, nextChars, nextCharsCount, chars.length );
			nextCharsCount += chars.length;

			//#if DEBUG == "true"
				System.out.println( "WORD " + i + ": " + word );
			//#endif
		}

		// troca a posi��o das letras
		final int limit = nextCharsCount << 2;
		for ( byte i = 0; i < limit; ++i ) {
			final int index1 = NanoMath.randInt( nextCharsCount );
			final int index2 = NanoMath.randInt( nextCharsCount );

			final char temp = nextChars[ index1 ];
			nextChars[ index1 ] = nextChars[ index2 ];
			nextChars[ index2 ] = temp;
		}

		//#if DEBUG == "true"
			for ( byte i = 0; i < nextCharsCount; ++i ) {
				System.out.print( nextChars[ i ] + ", " );
			}
			System.out.println();
		//#endif
	}


	/**
	 * Atualiza o label da pontua��o.
	 * @param equalize indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			updateScoreLabel();
		}
	} // fim do m�todo updateScore( boolean )


	private final void updateScoreLabel() {
//		AppMIDlet.gc();
//		scoreLabel.setText( String.valueOf( Runtime.getRuntime().freeMemory() ) );
		scoreLabel.setText( String.valueOf( scoreShown ) );
		scoreLabel.setPosition( scoreLabelX - scoreLabel.getWidth(), scoreLabel.getPosY() );
	}	

	
	private final void updateTime( int delta ) {
		if ( timeLeft > 0 ) {
			timeLeft -= delta;

			if ( timeLeft < 0 ) {
				// tempo esgotado
				timeLeft = 0;

				setState( level < TOTAL_LEVELS ? STATE_LEVEL_CLEARED : STATE_GAME_COMPLETE );
			}
		}

		final int minutes = ( timeLeft / 60000 );
		final int seconds = ( timeLeft - ( minutes * 60000 ) ) / 1000;
		final StringBuffer buffer = new StringBuffer();
		buffer.append( minutes );
		buffer.append( ':' );
		if ( seconds < 10 )
			buffer.append( '0' );
		buffer.append( seconds );
		labelTime.setText( buffer.toString() );

		//#if SCREEN_SIZE == "SMALL"
//# 			labelTime.setPosition( statusBar.getPosX() + TAG_TIME_LABEL_OFFSET_X + ( ( TAG_TIME_LABEL_WIDTH - labelTime.getWidth() ) >> 1 ), labelTime.getPosY() );
		//#endif
	}


	/**
	 * Incrementa a pontua��o e atualiza as vari�veis e labels correspondentes.
	 * @param diff varia��o na pontua��o (positiva ou negativa).
	 */
	private final void changeScore( int diff ) {
		score += diff;
		
		scoreSpeed.setSpeed( ( score - scoreShown ) >> 1, false );
	}


	private final void checkWords( int column, int row ) {
		printPieces( "ANTES");

		// verifica sequ�ncias na horizontal
		int start = column;
		int end = column;

		while( start > 0 && pieces[ start - 1 ][ row ] != null )
			--start;
		while( end < ( TOTAL_COLUMNS - 1 ) && pieces[ end + 1 ][ row ] != null )
			++end;

		int maxLength = end - start + 1;
		int currentLength = maxLength;
		char[] maxString = new char[ maxLength ];
		while ( currentLength >= WORD_MIN_LENGTH ) {
			int positions = maxLength - currentLength;

			// testa as poss�veis palavras
			for ( byte i = 0; i <= positions; ++i ) {
				// verifica somente as combina��es que incluam a nova pe�a inserida
				if ( ( start + i ) <= column && column < ( start + i + currentLength ) ) {
					// monta a string na ordem normal (esquerda para a direita) - o teste j� verifica a sequ�ncia inversa
					for ( byte j = i; j < i + currentLength; ++j ) {
						maxString[ j - i ] = pieces[ start + j ][ row ].getChar();
					}

					final byte wordType = hasWord( new String( maxString, 0, currentLength ) );

					if ( wordType != WORD_INVALID ) {
						final int startColumn = start + i;
						final int endColumn = startColumn + currentLength;

						wordMade( currentLength, pieces[ startColumn ][ row ], pieces[ endColumn - 1 ][ row ] );

						// atualmente, o loop � feito em toda a linha
						for ( byte c = 0; c < TOTAL_ROWS; ++c ) {
							if ( pieces[ c ][ row ] != null ) {
								int steps = 0;
								if ( c >= startColumn && c < endColumn ) {
									steps = wordType == WORD_REGULAR ? c - startColumn : endColumn - 1 - c;
									pieces[ c ][ row ].vanish( true, steps );
								} else {
									steps = currentLength;
									pieces[ c ][ row ].vanish( false, currentLength );
								}

								// faz as pe�as acima ca�rem
								for ( int k = row - 1; k >= 0; --k ) {
									if ( pieces[ c ][ k ] != null ) {
										pieces[ c ][ k ].fall( 1, steps );
									}

									pieces[ c ][ k + 1 ] = pieces[ c ][ k ];
								}
								pieces[ c ][ 0 ] = null;
							}
						}

						printPieces( "DEPOIS HORIZONTAL");
						return;
					}
				}
			}

			--currentLength;
		}

		// verifica sequ�ncias na vertical
		start = row;
		end = row;

		while( start > 0 && pieces[ column ][ start - 1 ] != null )
			--start;
		while( end < ( TOTAL_ROWS - 1 ) && pieces[ column ][ end + 1 ] != null )
			++end;

		maxLength = end - start + 1;
		maxString = new char[ maxLength ];
		currentLength = maxLength;
		while ( currentLength >= WORD_MIN_LENGTH ) {
			int positions = maxLength - currentLength;

			// testa as poss�veis palavras
			for ( byte i = 0; i <= positions; ++i ) {
				// verifica somente as combina��es que incluam a nova pe�a inserida
				if ( ( start + i ) <= row && row < ( start + i + currentLength ) ) {
					// monta a string na ordem normal (cima para baixo) - o teste j� verifica a sequ�ncia inversa
					for ( byte j = i; j < i + currentLength; ++j )
						maxString[ j - i ] = pieces[ column ][ start + j ].getChar();

					final byte wordType = hasWord( new String( maxString, 0, currentLength ) );

					if ( wordType != WORD_INVALID ) {
						wordMade( currentLength, pieces[ column ][ start ], pieces[ column ][ start + currentLength - 1 ] );

						for ( byte j = i; j < i + currentLength; ++j ) {
							pieces[ column ][ start + j ].vanish( true, wordType == WORD_REGULAR ? j - i : i + currentLength - 1 - j );
							pieces[ column ][ start + j ] = null;
						}

						for ( int j = start + i + currentLength; j < TOTAL_ROWS; ++j ) {
							pieces[ column ][ j ].vanish( false, currentLength );
							pieces[ column ][ j ] = null;
						}

						printPieces( "DEPOIS VERTICAL");
						return;
					}
				}
			}

			--currentLength;
		}
	} // fim do m�todo checkWords( int, int )


	private final void printPieces( String title ) {
		//#if DEBUG == "true"
			System.out.println( title );
			for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
				for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
					System.out.print( pieces[ c ][ r ] == null ? "- " : pieces[ c ][ r ].getChar() + " " );
				}
				System.out.println();
			}
		//#endif
	}


	private final void wordMade( int length, Piece start, Piece end ) {
		final ScoreLabel label = getNextScoreLabel();
		label.setState( ScoreLabel.STATE_APPEARING );
		label.setScore( start.getPosition().add( end.getPosition() ).div( 2 ), SCORES_PER_WORD[ length - WORD_MIN_LENGTH ] );

		timeToNextPiece = ( short ) ( length * VANISH_TIME );
	}


	private final byte hasWord( String word ) {
		for ( byte i = 0; i < words.length; ++i ) {
			if ( words[ i ].equals( word ) ) {
				wordsGroup.addWord( i % TOTAL_WORDS );
				return ( i < TOTAL_WORDS ? WORD_REGULAR : WORD_REVERSE );
			}
		}

		return WORD_INVALID;
	}


	private final void showMessage( String message ) {
		caeBienBlock.setVisible( true );
		caeBienMessage.setVisible( true );

		labelMessage.setText( "<ALN_H>" + message );
		labelMessage.setSize( ScreenManager.SCREEN_WIDTH, labelMessage.getTextTotalHeight() );
		labelMessage.setPosition( offsetX, ( size.y - labelMessage.getHeight() >> 1 ) + PLAYSCREEN_MESSAGE_OFFSET_Y );
		labelMessage.setVisible( true );

		//#if SCREEN_SIZE == "SMALL"
//# 			setInfoVisible( true );
		//#endif
	}


	//#if SCREEN_SIZE == "SMALL"
//# 	private final void setInfoVisible( boolean visible ) {
//# 		if ( size.y < HEIGHT_MIN ) {
//# 			scoreLabel.setVisible( visible );
//# 		}
//# 	}
	//#endif
	
	
	public final int getScore() {
		return score;
	}


	public final void setSize( int width, int height ) {
		//#if SCREEN_SIZE == "SMALL"
//# 			// o tabuleiro � centralizado horizontalmente na tela
//# 			final int diffX = ( width - piecesGroup.getWidth() ) >> 1;
//# 			// a posi��o vertical leva em conta a barra de status
//# 			final int diffY = Math.min( ( height - border.getHeight() + statusBar.getHeight() ) >> 1, height - border.getHeight() );
//#
//# 			// posiciona o quadro de palavras
//# 			offsetX = ( short ) ( WORDS_GROUP_X + wordsGroup.getWidth() + Math.max( WORDS_GROUP_BOARD_OFFSET, diffX ) );
//# 			super.setSize( width + offsetX, height );
//#
//# 			statusBar.setPosition( offsetX + ( ( ScreenManager.SCREEN_WIDTH - statusBar.getWidth() ) >> 1 ),
//# 								   Math.max( diffY - statusBar.getHeight(), 0 ) );
//# 			nextPiece.setPosition( statusBar.getPosition().add( TAG_NEXT_CHAR_ICON_OFFSET_X, TAG_NEXT_CHAR_ICON_OFFSET_Y ) );
//# 			scoreLabel.setPosition( statusBar.getPosition().add( TAG_SCORE_LABEL_OFFSET_X, TAG_SCORE_LABEL_OFFSET_Y ) );
//# 			scoreLabelX = ( short ) ( scoreLabel.getPosX() + TAG_SCORE_LABEL_WIDTH );
//# 			labelTime.setPosition( statusBar.getPosition().add( TAG_TIME_LABEL_OFFSET_X, TAG_TIME_LABEL_OFFSET_Y ) );
//#
//# 			piecesGroup.setRefPixelPosition( offsetX + ( width >> 1 ), diffY + BORDER_HALF_THICKNESS );
//# 			if ( piecesGroup.getPosY() <= statusBar.getPosY() + statusBar.getHeight() ) {
//# 				piecesGroup.setPosition( piecesGroup.getPosX(), height - piecesGroup.getHeight() );
//# 			}
//# 			border.setPosition( piecesGroup.getPosition().sub( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS ) );
//#
//# 			tagWords.setRightLimit( piecesGroup.getPosX() );
//# 			tagWords.setPosition( WORDS_GROUP_X + ( wordsGroup.getWidth() >> 1 ),
//# 								  Math.max( 0, piecesGroup.getPosY() - tagWords.getHeight() + BORDER_THICKNESS + TAG_BORDER_OFFSET_X ) );
//#
//# 			wordsGroup.setPosition( WORDS_GROUP_X,
//# 									Math.min( tagWords.getPosY() + tagWords.getHeight() + WORDS_TABLE_TAG_OFFSET_Y, height - wordsGroup.getHeight() ) );
		//#else
			if ( width <= height ) {
				// formato de tela: quadrado ou portrait

				// o tabuleiro � centralizado horizontalmente na tela
				final int diffX = ( width - piecesGroup.getWidth() ) >> 1;
				// a posi��o vertical leva em conta as tags
				final int diffY = Math.min( ( height - piecesGroup.getHeight() + tagTimePortrait.getHeight() ) >> 1, height - piecesGroup.getHeight() - BORDER_THICKNESS );

				// posiciona o quadro de palavras
				offsetX = ( short ) ( WORDS_GROUP_X + wordsGroup.getWidth() + Math.max( WORDS_GROUP_BOARD_OFFSET, diffX ) );
				super.setSize( width + offsetX, height );


				//#if SCREEN_SIZE == "BIG"
					if ( height < PLAYSCREEN_HEIGHT_MIN ) {
						piecesGroup.setRefPixelPosition( offsetX + ( width >> 1 ), height - piecesGroup.getHeight() );
						border.setPosition( piecesGroup.getPosition().sub( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS ) );
					} else {
						piecesGroup.setRefPixelPosition( offsetX + ( width >> 1 ), diffY );
						border.setPosition( piecesGroup.getPosition().sub( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS ) );
					}

					wordsGroup.setPosition( WORDS_GROUP_X, diffY + tagWords.getHeight() + WORDS_TABLE_TAG_OFFSET_Y );
					tagWords.setRightLimit( border.getPosX() );
					tagWords.setPosition( wordsGroup.getPosX() + ( wordsGroup.getWidth() >> 1 ),
										  wordsGroup.getPosY() - tagWords.getHeight() + TAGS_LANDSCAPE_Y_SPACING + WORDS_TABLE_TAG_OFFSET_Y );
				//#else
//# 					piecesGroup.setRefPixelPosition( offsetX + ( width >> 1 ), diffY );
//# 					border.setPosition( piecesGroup.getPosition().sub( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS ) );
//# 					tagWords.setRightLimit( piecesGroup.getPosX() );
//# 					tagWords.setPosition( WORDS_GROUP_X + ( wordsGroup.getWidth() >> 1 ), piecesGroup.getPosY() - tagWords.getHeight() + BORDER_THICKNESS + TAG_BORDER_OFFSET_X );
//#
//# 					wordsGroup.setPosition( WORDS_GROUP_X, tagWords.getPosY() + tagWords.getHeight() + WORDS_TABLE_TAG_OFFSET_Y );
				//#endif


				tagNextCharLandscape.setVisible( false );
				tagScoreLandscape.setVisible( false );
				tagTimeLandscape.setVisible( false );
				tagLevelLandscape.setVisible( false );

				tagNextCharPortrait.setVisible( true );
				tagScorePortrait.setVisible( true );
				tagTimePortrait.setVisible( true );

				tagNextCharPortrait.setPosition( border.getPosX() + BORDER_HALF_THICKNESS, Math.max( 0, border.getPosY() - tagNextCharPortrait.getHeight() ) );
				labelNextChar.setPosition( tagNextCharPortrait.getPosX() + TAG_NEXT_CHAR_LABEL_OFFSET_X, tagNextCharPortrait.getPosY() );
				nextPiece.setPosition( tagNextCharPortrait.getPosX() + TAG_NEXT_CHAR_ICON_OFFSET_X_PORTRAIT,
									   Math.min( tagNextCharPortrait.getPosY() + TAG_NEXT_CHAR_ICON_OFFSET_Y_PORTRAIT, piecesGroup.getPosY() - nextPiece.getHeight() ) );

				//#if SCREEN_SIZE == "BIG"
					if ( height < PLAYSCREEN_HEIGHT_MIN ) {
						tagScorePortrait.setPosition( border.getPosX() + border.getWidth() - tagScorePortrait.getWidth() - ( TAG_BORDER_OFFSET_X << 1 ),
													  border.getPosY() + SCORE_TAG_Y_OFFSET_HEIGHT_MIN );
						scoreLabel.setPosition( tagScorePortrait.getPosX() + TAG_SCORE_LABEL_OFFSET_X, tagScorePortrait.getPosY() + TAG_SCORE_LABEL_OFFSET_Y );
						scoreLabelX = ( short ) ( scoreLabel.getPosX() + TAG_SCORE_LABEL_WIDTH );
					} else {
						tagScorePortrait.setPosition( border.getPosX() + border.getWidth() - tagScorePortrait.getWidth() - ( TAG_BORDER_OFFSET_X << 1 ), border.getPosY() - tagScorePortrait.getHeight() );
						scoreLabel.setPosition( tagScorePortrait.getPosX() + TAG_SCORE_LABEL_OFFSET_X, tagScorePortrait.getPosY() + TAG_SCORE_LABEL_OFFSET_Y );
						scoreLabelX = ( short ) ( scoreLabel.getPosX() + TAG_SCORE_LABEL_WIDTH );
					}
				//#else
//# 					tagScorePortrait.setPosition( border.getPosX() + border.getWidth() - tagScorePortrait.getWidth() - ( TAG_BORDER_OFFSET_X << 1 ), border.getPosY() - tagScorePortrait.getHeight() );
//# 					scoreLabel.setPosition( tagScorePortrait.getPosX() + TAG_SCORE_LABEL_OFFSET_X, tagScorePortrait.getPosY() + TAG_SCORE_LABEL_OFFSET_Y );
//# 					scoreLabelX = ( short ) ( scoreLabel.getPosX() + TAG_SCORE_LABEL_WIDTH );
				//#endif

				tagTimePortrait.setPosition( border.getPosX() + border.getWidth() - tagTimePortrait.getWidth() - BORDER_HALF_THICKNESS,
											 Math.max( 0, border.getPosY() - tagTimePortrait.getHeight() ) );
				labelLevel.setPosition( tagTimePortrait.getPosX() + + TAG_TIME_LABEL_LEFT_OFFSET_X, tagTimePortrait.getPosY() );

				timeLabelRightX = ( short ) ( tagTimePortrait.getPosX() + TAG_TIME_LABEL_RIGHT_OFFSET_X );
				labelTime.setPosition( timeLabelRightX - labelTime.getFont().getTextWidth( "2:00" ), tagTimePortrait.getPosY() + TAG_SCORE_LABEL_OFFSET_Y );
			} else {
				// formato de tela: landscape

				final int diffY = ( height - piecesGroup.getHeight() ) >> 1;

				// posiciona o quadro de palavras
				//#if SCREEN_SIZE == "BIG"
					wordsGroup.setPosition( WORDS_GROUP_X, Math.min( caeBienLogo.getPosY() + caeBienLogo.getHeight(), height - wordsGroup.getHeight() ) );
				//#else
//# 					wordsGroup.setPosition( ( width - wordsGroup.getWidth() ) >> 1, diffY + tagWords.getHeight() + WORDS_TABLE_TAG_OFFSET_Y );
				//#endif
				offsetX = ( short ) NanoMath.max( WORDS_GROUP_X + wordsGroup.getWidth() + WORDS_GROUP_BOARD_OFFSET, ScreenManager.SCREEN_WIDTH );

				super.setSize( width + offsetX, height );

				piecesGroup.setRefPixelPosition( offsetX + BORDER_THICKNESS + ( piecesGroup.getWidth() >> 1 ), diffY );
				border.setPosition( piecesGroup.getPosition().sub( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS ) );

				tagWords.setRightLimit( border.getPosX() );

				//#if SCREEN_SIZE == "BIG"
					tagWords.setPosition( Math.max( wordsGroup.getPosX() + ( wordsGroup.getWidth() >> 1 ), caeBienLogo.getPosX() + caeBienLogo.getWidth() + WORDS_GROUP_LABEL_OFFSET_X ),
										  Math.max( wordsGroup.getPosY() - tagWords.getHeight() - TAGS_LANDSCAPE_Y_SPACING, 0 ) );
				//#else
//# 					tagWords.setPosition( wordsGroup.getPosX() + ( wordsGroup.getWidth() >> 1 ),
//# 										  Math.max( wordsGroup.getPosY() - tagWords.getHeight() - TAGS_LANDSCAPE_Y_SPACING, 0 ) );
				//#endif

				tagNextCharPortrait.setVisible( false );
				tagScorePortrait.setVisible( false );
				tagTimePortrait.setVisible( false );

				tagNextCharLandscape.setVisible( true );
				tagScoreLandscape.setVisible( true );
				tagTimeLandscape.setVisible( true );
				tagLevelLandscape.setVisible( true );

				tagNextCharLandscape.setRightLimit( getWidth() );
				tagScoreLandscape.setRightLimit( getWidth() );
				tagLevelLandscape.setRightLimit( getWidth() );
				tagTimeLandscape.setRightLimit( getWidth() );

				final int posX = border.getPosX() + border.getWidth() + BORDER_HALF_THICKNESS;

				tagNextCharLandscape.setInternalWidth( labelNextChar.getWidth() );
				tagNextCharLandscape.setPosition( posX, border.getPosY() + TAGS_LANDSCAPE_Y_SPACING );
				labelNextChar.setPosition( tagNextCharLandscape.getPosX() + TAG_TIME_LABEL_LEFT_OFFSET_X, tagNextCharLandscape.getPosY() );
				nextPiece.setPosition( tagNextCharLandscape.getPosX() + TAG_NEXT_CHAR_ICON_OFFSET_X_LANDSCAPE, tagNextCharLandscape.getPosY() + TAG_NEXT_CHAR_ICON_OFFSET_Y_PORTRAIT );

				final int SCORE_WIDTH = Math.min( getWidth() - posX - BORDER_HALF_THICKNESS, tagScoreLandscape.getFont().getTextWidth( "99" + String.valueOf( SCORE_SHOWN_MAX ) ) );
				tagScoreLandscape.setInternalWidth( SCORE_WIDTH );
				tagScoreLandscape.setPosition( posX, nextPiece.getPosY() + nextPiece.getHeight() + TAGS_LANDSCAPE_Y_SPACING );
				scoreLabel.setPosition( tagScoreLandscape.getPosX() + TAG_SCORE_LABEL_OFFSET_X, tagScoreLandscape.getPosY() + TAG_SCORE_LABEL_OFFSET_Y );
				scoreLabelX = ( short ) ( scoreLabel.getPosX() + SCORE_WIDTH );

				tagLevelLandscape.setInternalWidth( SCORE_WIDTH );
				tagLevelLandscape.setPosition( posX, tagScoreLandscape.getPosY() + tagScoreLandscape.getHeight() + TAGS_LANDSCAPE_Y_SPACING );
				labelLevel.setPosition( tagLevelLandscape.getPosX() + TAG_TIME_LABEL_LEFT_OFFSET_X, tagLevelLandscape.getPosY() );

				final int TIME_WIDTH = tagTimeLandscape.getFont().getTextWidth( "02:000" );
				final int TIME_WIDTH_2 = tagTimeLandscape.getFont().getTextWidth( "2" );
				tagTimeLandscape.setInternalWidth( TIME_WIDTH );
				tagTimeLandscape.setPosition( posX, tagLevelLandscape.getPosY() + tagLevelLandscape.getHeight() + TAGS_LANDSCAPE_Y_SPACING );
				timeLabelRightX = ( short ) ( tagTimeLandscape.getPosX() + TIME_WIDTH + TIME_WIDTH_2 );
				labelTime.setPosition( timeLabelRightX - TIME_WIDTH, tagTimeLandscape.getPosY() + TAG_SCORE_LABEL_OFFSET_Y );
			}
		//#endif

		caeBienBlock.setRefPixelPosition( offsetX + ScreenManager.SCREEN_HALF_WIDTH, ( ( size.y - caeBienBlock.getHeight() >> 1 ) + CAE_BIEN_MSG_OFFSET_Y ) );
		caeBienMessage.setPosition( offsetX + ( ( ScreenManager.SCREEN_WIDTH - caeBienMessage.getWidth() ) >> 1 ), ( ( height - caeBienMessage.getHeight() ) >> 1 ) - CAE_BIEN_MSG_OFFSET_Y );

		// posiciona os patterns do fundo de forma a preencher toda a �rea sem ter que repintar desnecessariamente o tabuleiro
		int temp = piecesGroup.getPosY();
		bkgTop.setSize( getWidth(), temp + BKG_SMALL_BLOCKS_SIZE - ( temp % BKG_SMALL_BLOCKS_SIZE ) );
		bkgLeft.setPosition( 0, bkgTop.getHeight() );
		temp = piecesGroup.getHeight();
		bkgLeft.setSize( border.getPosX() + BORDER_HALF_THICKNESS, temp + BKG_SMALL_BLOCKS_SIZE - ( temp % BKG_SMALL_BLOCKS_SIZE ) );
		bkgRight.setSize( bkgLeft.getSize() );
		temp = piecesGroup.getPosX() + piecesGroup.getWidth();
		bkgRight.setPosition( temp - ( temp % BKG_SMALL_BLOCKS_SIZE ), bkgTop.getHeight() );
		temp = bkgLeft.getPosY() + bkgLeft.getHeight() - BKG_SMALL_BLOCKS_SIZE - BORDER_HALF_THICKNESS;
		bkgBottom.setPosition( 0, temp + BKG_SMALL_BLOCKS_SIZE - ( temp % BKG_SMALL_BLOCKS_SIZE ) );
		bkgBottom.setSize( getWidth(), getHeight() - bkgBottom.getPosY() );

		limitRight = ( short ) ( ScreenManager.SCREEN_WIDTH - border.getPosX() );

		updateScoreLabel();
	}


	public final void hideNotify() {
		if ( state == STATE_PLAYING )
			setState( STATE_PAUSED );
	}


	public final void showNotify() {
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != size.x || height != size.y )
			setSize( width, height );
	}


	private static final class ScoreLabel extends Label implements Updatable {

		private static final short SPEED_MIN = ( short ) -( ScreenManager.SCREEN_HEIGHT >> 4 );

		private static final short SPEED_MAX_DIFF = ( short ) ( ScreenManager.SCREEN_HEIGHT >> 3 );

		private static final short LIFE_MIN			= 1000;

		private static final short LIFE_MAX_DIFF	= 1700;

		private short lifeTime;

		private final MUV muv = new MUV();

		private static final byte STATE_IDLE		= 0;
		private static final byte STATE_APPEARING	= 1;
		private static final byte STATE_VISIBLE		= 2;
		private static final byte STATE_VANISHING	= 3;

		private byte state;

		/** Tempo em milisegundos da anima��o de apari��o/sumi�o. */
		private static final short APPEARING_TIME	= 210;

		private final PlayScreen playScreen;


		public ScoreLabel( PlayScreen screen ) throws Exception {
			super( FONT_SCORE, null );

			playScreen = screen;

			setViewport( new Rectangle() );

			setState( STATE_IDLE );
		}


		public final void update( int delta ) {
			switch ( state ) {
				case STATE_APPEARING:
					lifeTime += delta;
					
					if ( lifeTime < APPEARING_TIME ) {
						final int currentWidth = getWidth() * lifeTime / APPEARING_TIME;
						final int currentHeight = getHeight() * lifeTime / APPEARING_TIME;
						viewport.set( getRefPixelX() - ( currentWidth >> 1 ), getRefPixelY() - ( currentHeight >> 1 ), currentWidth, currentHeight );
					} else {
						setState( STATE_VISIBLE );
					}
				break;

				case STATE_VISIBLE:
					lifeTime -= delta;

					if ( lifeTime > 0 ) {
						move( 0, muv.updateInt( delta ) );
					} else {
						setState( STATE_VANISHING );
					}
				break;

				case STATE_VANISHING:
					lifeTime -= delta;

					if ( lifeTime > 0 ) {
						final int currentWidth = getWidth() * lifeTime / APPEARING_TIME;
						final int currentHeight = getHeight() * lifeTime / APPEARING_TIME;
						viewport.set( getRefPixelX() - ( currentWidth >> 1 ), getRefPixelY() - ( currentHeight >> 1 ), currentWidth, currentHeight );
					} else {
						setState( STATE_IDLE );
					}
				break;
			}
		}


		public final void setScore( Point pos, int score ) {
			pos.addEquals( playScreen.piecesGroup.getPosition().add( PIECE_WIDTH >> 1, PIECE_HEIGHT >> 1 ) );
			
			setText( String.valueOf( score ) );
			defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

			playScreen.changeScore( score );
			
			setRefPixelPosition( pos );
			setState( STATE_APPEARING );
		}


		private final void setState( byte state ) {
			switch ( state ) {
				case STATE_IDLE:
					setVisible( false );
				break;

				case STATE_APPEARING:
					setVisible( true );
					muv.setSpeed( SPEED_MIN - NanoMath.randInt( SPEED_MAX_DIFF ) );
					lifeTime = 0;
					viewport.set( 0, 0, 0, 0 );
				break;

				case STATE_VISIBLE:
					lifeTime = ( short ) ( LIFE_MIN + NanoMath.randInt( LIFE_MAX_DIFF ) );
					viewport.set( 0, 0, playScreen.getWidth(), ScreenManager.SCREEN_HEIGHT );
				break;

				case STATE_VANISHING:
					lifeTime = APPEARING_TIME;
				break;
			}
			
			this.state = state;
		}

	} // fim da classe interna ScoreLabel


	private static final class WordsGroup extends DrawableGroup {

		private static final int COLOR_BKG_DARK = 0x9b032b;
		private static final int COLOR_BKG_LIGHT = 0xb30432;

		private final byte[] wordsDone = new byte[ TOTAL_WORDS ];

		private final Label[] labels = new Label[ TOTAL_WORDS ];

		private final PlayScreen playScreen;

		
		public WordsGroup( PlayScreen playScreen ) throws Exception {
			super( ( TOTAL_WORDS << 1 ) + 10 );

			this.playScreen = playScreen;

			final int DIFF = WORDS_FONT_HEIGHT_OFFSET / WORDS_GROUP_ROWS;

			final byte WORDS_PER_ROW = ( TOTAL_WORDS + ( TOTAL_WORDS % WORDS_GROUP_ROWS ) ) / WORDS_GROUP_ROWS;

			final ImageFont font = GameMIDlet.getFont( FONT_TEXT_DARK );
			int y = WORDS_GROUP_BORDER;
			int x = WORDS_GROUP_BORDER;

			for ( int c = 0; c < WORDS_GROUP_ROWS; ++c, x += WORDS_GROUP_WIDTH + WORDS_GROUP_X_DISTANCE ) {
				y = WORDS_GROUP_BORDER;

				final Pattern fill = new Pattern( COLOR_BKG_LIGHT );
				fill.setPosition( x - WORDS_GROUP_BORDER, y - WORDS_GROUP_BORDER );
				insertDrawable( fill );

				for ( int i = 0, start = c * WORDS_PER_ROW; i < WORDS_PER_ROW; ++i, y += font.getHeight() + WORDS_FONT_HEIGHT_OFFSET ) {
					Pattern bkg = null;
					if ( ( i & 1 ) == 0 ) {
						bkg = new Pattern( COLOR_BKG_DARK );
						insertDrawable( bkg );
					}
					
					if ( start + i < TOTAL_WORDS ) {
						final Label l = new Label( font, null );
						l.setPosition( x + WORDS_GROUP_LABEL_OFFSET_X, y + DIFF );
						l.setSize( WORDS_GROUP_WIDTH - ( WORDS_GROUP_LABEL_OFFSET_X << 1 ), font.getHeight() );
						labels[ start + i ] = l;

						if ( bkg != null ) {
							bkg.setSize( WORDS_GROUP_WIDTH - ( WORDS_GROUP_BORDER << 1 ), l.getHeight() + WORDS_FONT_HEIGHT_OFFSET );
							bkg.setPosition( x, y );
						}

						updateLabel( start + i );

						insertDrawable( l );
					}
				}

				fill.setSize( WORDS_GROUP_WIDTH, y );
			}

			setSize( x, y );
		}


		public final void addWord( int index ) {
			++wordsDone[ index ];
			updateLabel( index );

			short totalWords = 0;
			for ( byte i = 0; i < TOTAL_WORDS; ++i )
				totalWords += wordsDone[ i ];

			playScreen.tagWords.setText( GameMIDlet.getText( TEXT_WORDS ) + ": " + totalWords );
		}


		public final void updateLabel( int index ) {
			final StringBuffer buffer = new StringBuffer();
			if ( wordsDone[ index ] < 10 )
				buffer.append( '0' );
			buffer.append( wordsDone[ index ] );
			buffer.append( " - " );
			buffer.append( PlayScreen.words[ index ] );

			labels[ index ].setText( buffer.toString() );
			labels[ index ].setFont( GameMIDlet.getFont( wordsDone[ index ] > 0 ? FONT_TEXT_WHITE : FONT_TEXT_DARK ) );
		}
	}


}
