/**
 * GameMIDlet.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */
package screens;

import core.SoftLabel;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import core.Piece;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Graphics;

//#ifndef NO_RECOMMEND
	import br.com.nanogames.components.util.SmsSender;
	import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if DEBUG == "true"
	import br.com.nanogames.components.basic.BasicTextScreen;
//#endif

//#if JAR != "min"
import core.ScrollBar;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Border;
import core.Tag;
import javax.microedition.lcdui.Image;
//#endif

//#if SCREEN_SIZE == "BIG"
import java.util.Hashtable;
//#endif

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener {

	private static final short GAME_MAX_FRAME_TIME = 155;

	private static final byte BACKGROUND_TYPE_SPECIFIC = 0;

	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;

	private static final byte BACKGROUND_TYPE_NONE = 2;

	private static int softKeyRightText;
	//#if JAR != "min"
		/** gerenciador da anima��o da soft key esquerda */
		private static BasicAnimatedSoftkey softkeyLeft;

		/** gerenciador da anima��o da soft key esquerda */
		private static BasicAnimatedSoftkey softkeyMid;

		/** gerenciador da anima��o da soft key direita */
		private static BasicAnimatedSoftkey softkeyRight;

	//#endif

	//#if JAR == "full"
	/** Limite m�nimo de mem�ria para que comece a haver cortes nos recursos. */
	private static final int LOW_MEMORY_LIMIT = 950000;
	//#endif

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	/** Refer�ncia para a tela de jogo, para que seja poss�vel retornar � tela de jogo ap�s entrar na tela de pausa. */
	private static PlayScreen playScreen;

//	private static boolean lowMemory;

	private static LoadListener loader;

	private static Pattern bkg;


	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#elif VENDOR == "MOTOROLA"
//# 			super( VENDOR_MOTOROLA, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
		//#endif
			
//		// cria a base de dados do jogo\
//		try {
//			createDatabase( "BLABLA", 1 );
//		} catch ( Exception e ) {
//		}
//
//		Logger.setRMS( "BLABLA", 1 );
//		log( 0 );

		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	}


	public static final void log( int i ) {
//		AppMIDlet.gc();
//		Logger.log( i + ": " + Runtime.getRuntime().freeMemory() );
	}


//	public static final void log( Throwable t ) {
//		AppMIDlet.gc();
//		Logger.log( t.getMessage() );
//	}


	protected final void loadResources() throws Exception {
		log( 1 );
//		switch ( getVendor() ) {
//			//#if SCREEN_SIZE == "BIG"
////# //					case VENDOR_SAMSUNG:
////# //						lowMemory = Runtime.getRuntime().totalMemory() < LOAD_ALL_MEMORY_SAMSUNG;
////# //					break;
////#
////# 					case VENDOR_NOKIA:
////# //					case VENDOR_MOTOROLA:
//			//#endif
//			case VENDOR_SONYERICSSON:
//			case VENDOR_SIEMENS:
//			case VENDOR_BLACKBERRY:
//			case VENDOR_HTC:
//			break;
//
//			default:
//				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
//	}
//		lowMemory = true; // teste

		//#if JAR == "min"
//# 			FONTS[ 0 ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_2.png", PATH_IMAGES + "font_2.dat" );
//# 			FONTS[ 0 ].setCharOffset( 1 );
//#
//# 			FONTS[ FONT_POINTS ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_4.png", PATH_IMAGES + "font_4.dat" );
//# 			FONTS[ FONT_POINTS ].setCharOffset( 1 );
//#
//# 			FONTS[ FONT_BIG ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_3.png", PATH_IMAGES + "font_3.dat" );
		//#else
			for ( byte i = 0; i < FONT_TYPES_TO_LOAD; ++i ) {
				FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i + ".png", PATH_IMAGES + "font_" + i + ".dat" );
				if ( i != FONT_BIG )
					FONTS[ i ].setCharOffset( 1 );
				log( 6 );
			}

			final PaletteChanger p = new PaletteChanger( PATH_IMAGES + "font_" + FONT_TEXT_WHITE + ".png" );
			final Image image = p.createImage( new PaletteMap[] { new PaletteMap( COLOR_FONT_WHITE, COLOR_FONT_DARK ) } );
			FONTS[ FONT_TEXT_DARK ] = ImageFont.createMultiSpacedFont( image, PATH_IMAGES + "font_" + FONT_TEXT_WHITE + ".dat" );
			FONTS[ FONT_TEXT_DARK ].setCharOffset( 1 );
		//#endif

		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}


		final byte[] lang = new byte[] { LANGUAGE_ES };
		try {
			loadData( DATABASE_NAME, DATABASE_SLOT_LANGUAGE, new Serializable() {
				public final void write( DataOutputStream output ) throws Exception {
				}


				public final void read( DataInputStream input ) throws Exception {
					lang[ 0 ] = input.readByte();
				}
			});
		} catch ( Exception e ) {
			// Ou n�o conseguiu carregar o idioma, ou ent�o � a primeira execu��o - tenta detectar o idioma do aparelho
			// Se n�o encontrar ou o idioma n�o for portugu�s, usa o padr�o (espanhol)
			final String locale = System.getProperty( "microedition.locale" );
			if ( locale != null && locale.indexOf( "pt" ) > 0 )
				lang[ 0 ] = LANGUAGE_PT;
		}

		log( 7 );

		setLanguage( lang[ 0 ] );

		log( 8 );

		//#if BLACKBERRY_API == "true"
//#
//#
//# 			// em aparelhos antigos (e/ou com vers�es de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms n�o recebe os eventos de teclado corretamente, sumindo somente ap�s a aplica��o ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
//#
//#
//# 			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
//# 			int[][] keys = null;
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 },
//# 						{ ' ', ScreenManager.KEY_NUM0 },
//#
//# 					 };
//# 				break;
//#
//# 				default:
//# 					keys = new int[][] {
//# 						{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 						{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 						{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 						{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 						{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 						{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 						{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 						{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 						{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 						{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 						{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 						{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 					 };
//# 			}
//# 			for ( byte i = 0; i < keys.length; ++i )
//# 				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
//#
		//#elif SCREEN_SIZE == "BIG"

			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;
			final int[][] keys = {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );

		//#endif

		bkg = new Pattern( new DrawableImage( PATH_IMAGES + "bkg.png" ) );
		final int dim = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		bkg.setSize( dim, dim );

		setScreen( SCREEN_LOADING_1 );
	} // fim do m�todo loadResources()


	protected final void destroyApp( boolean unconditional ) throws MIDletStateChangeException {
		if ( playScreen != null ) {
			HighScoresScreen.setScore( playScreen.getScore() );
			playScreen = null;
		}

		super.destroyApp( unconditional );
	}


	public static final void gameOver( int score ) {
		if ( HighScoresScreen.setScore( score ) ) {
			setScreen( SCREEN_HIGH_SCORES );
		} else {
			setScreen( SCREEN_MAIN_MENU );
		}
	}


	protected final int changeScreen( int screen ) throws Exception {
		log( 10 );
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = BACKGROUND_TYPE_SPECIFIC;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;

		short visibleTime = ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME;

		switch ( screen ) {
			case SCREEN_CHOOSE_LANGUAGE:
			case SCREEN_CHOOSE_SOUND:
				BasicConfirmScreen confirm = null;
				byte defaultOption = 0;
				if ( screen == SCREEN_CHOOSE_LANGUAGE ) {
					confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_TEXT_WHITE ), null,
													  getConfirmTitle( TEXT_CHOOSE_LANGUAGE ),
													  new Tag( Tag.COLOR_BLUE, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_SPANISH ),
													  new Tag( Tag.COLOR_PINK, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_PORTUGUESE ), null, false );

					defaultOption = ( byte ) ( BasicConfirmScreen.INDEX_YES + getLanguage() );
				} else {
					confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_TEXT_WHITE ), null,
													  getConfirmTitle( TEXT_DO_YOU_WANT_SOUND ),
													  new Tag( Tag.COLOR_BLUE, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_YES ),
													  new Tag( Tag.COLOR_PINK, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_NO ), null, false );


					defaultOption = MediaPlayer.isMuted() ? BasicConfirmScreen.INDEX_NO : BasicConfirmScreen.INDEX_YES;
				}
				confirm.setSpacing( CONFIRM_TITLE_Y_OFFSET, MENU_ENTRY_Y_OFFSET );
				confirm.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				confirm.setCurrentIndex( defaultOption );

				nextScreen = confirm;
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_SPLASH_NANO:
				HighScoresScreen.createInstance( DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );

				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "SMALL"
//# 					nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, 1 );
				//#else
					nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, 1 );
				//#endif
			break;

//			case SCREEN_SPLASH_BRAND:
//				bkgType = BACKGROUND_TYPE_NONE;
//				nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_SPLASH + "nano.png", TEXT_SPLASH_BRAND  );
//			break;

			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame();
				bkgType = BACKGROUND_TYPE_NONE;
			break;

			case SCREEN_MAIN_MENU:
				playScreen = null;
				//#if WEB_EMULATOR == "true"
//# 					nextScreen = createBasicMenu( screen, new int[] {
//#  							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_RECOMMEND_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS,
									//#if DEBUG == "true"
//# 										TEXT_LOG_TITLE,
									//#endif
//# 								 } );
				//#else
					indexSoftRight = TEXT_EXIT;
					//#ifdef NO_RECOMMEND
//# 						nextScreen = createBasicMenu( screen, new int[] {
//# 								TEXT_NEW_GAME,
//# 								TEXT_OPTIONS,
//# 								TEXT_HIGH_SCORES,
//# 								TEXT_PRODUCTS_TITLE,
//# 								TEXT_HELP,
//# 								TEXT_CREDITS,
//# 								TEXT_EXIT,
									//#if DEBUG == "true"
//# 										TEXT_LOG_TITLE,
									//#endif
//# 									} );
					//#else
						//#if BLACKBERRY_API == "true"
//# 							nextScreen = createBasicMenu( screen, ( showRecommendScreen && SmsSender.isSupported() ) ? new int[] {
						//#else
							nextScreen = createBasicMenu( screen, SmsSender.isSupported() ? new int[] {
						//#endif
								TEXT_NEW_GAME,
								TEXT_OPTIONS,
								TEXT_HIGH_SCORES,
								TEXT_RECOMMEND_TITLE,
								TEXT_HELP,
								TEXT_CREDITS,
								TEXT_EXIT,
								//#if DEBUG == "true"
									TEXT_LOG_TITLE,
								//#endif
								}
							: new int[] {
								TEXT_NEW_GAME,
								TEXT_OPTIONS,
								TEXT_HIGH_SCORES,
								TEXT_HELP,
								TEXT_CREDITS,
								TEXT_EXIT,
								//#if DEBUG == "true"
									TEXT_LOG_TITLE,
								//#endif
								} );
					//#endif
				//#endif

				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_NEXT_LEVEL:
				playScreen.prepareNextLevel();
			case SCREEN_CONTINUE_GAME:
				nextScreen = playScreen;
				bkgType = BACKGROUND_TYPE_NONE;

				if ( screen == SCREEN_CONTINUE_GAME ) {
					playScreen.sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
					playScreen.setState( PlayScreen.STATE_UNPAUSING );
				}
			break;

			case SCREEN_OPTIONS:
					PepsiOptionsScreen optionsScreen = null;

					if ( MediaPlayer.isVibrationSupported() ) {
						optionsScreen = PepsiOptionsScreen.createInstance( midlet, screen, getFont( FONT_TEXT_WHITE ), new int[] {
							TEXT_TURN_SOUND_OFF,
							TEXT_TURN_VIBRATION_OFF,
							TEXT_BACK,
						}, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION );
					} else {
						optionsScreen = PepsiOptionsScreen.createInstance( midlet, screen, getFont( FONT_TEXT_WHITE ), new int[] {
							TEXT_TURN_SOUND_OFF,
							TEXT_BACK,
						}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, -1 );
					}
 				nextScreen = optionsScreen;

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HIGH_SCORES:
				nextScreen = HighScoresScreen.createInstance( DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );

				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HELP:
				nextScreen = new BorderedScreen( getText( TEXT_HELP_TEXT ) + getText( TEXT_VERSION ) + getMIDletVersion(), SCREEN_MAIN_MENU, false );

				bkgType = BACKGROUND_TYPE_NONE;

				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_CREDITS:
				nextScreen = new BorderedScreen( getText( TEXT_CREDITS_TEXT ) + getText( TEXT_VERSION ) + getMIDletVersion(), SCREEN_MAIN_MENU, true );

				bkgType = BACKGROUND_TYPE_NONE;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_PAUSE:
					PepsiOptionsScreen pauseScreen = null;
					if ( MediaPlayer.isVibrationSupported() ) {
						pauseScreen = PepsiOptionsScreen.createInstance( midlet, screen, getFont( FONT_TEXT_WHITE ), new int[] {
							TEXT_CONTINUE,
							TEXT_TURN_SOUND_OFF,
							TEXT_TURN_VIBRATION_OFF,
							TEXT_BACK_MENU,
							TEXT_EXIT_GAME,
						}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION );
					} else {
						pauseScreen = PepsiOptionsScreen.createInstance( midlet, screen, getFont( FONT_TEXT_WHITE ), new int[] {
							TEXT_CONTINUE,
							TEXT_TURN_SOUND_OFF,
							TEXT_BACK_MENU,
							TEXT_EXIT_GAME,
						}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, -1 );
					}

				nextScreen = pauseScreen;
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;
			break;

			case SCREEN_CONFIRM_MENU:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_TEXT_WHITE ), null,
													 getConfirmTitle( TEXT_CONFIRM_BACK_MENU ),
													 new Tag( Tag.COLOR_BLUE, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_YES ),
													 new Tag( Tag.COLOR_PINK, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_NO ), null, true );

				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				( ( BasicConfirmScreen ) nextScreen ).setSpacing( CONFIRM_TITLE_Y_OFFSET, MENU_ENTRY_Y_OFFSET );
				( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( BasicConfirmScreen.INDEX_NO );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT:
				nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_TEXT_WHITE ), null,
													 getConfirmTitle( TEXT_CONFIRM_EXIT ),
													 new Tag( Tag.COLOR_BLUE, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_YES ),
													 new Tag( Tag.COLOR_PINK, MENU_TAG_WIDTH, FONT_TEXT_WHITE, TEXT_NO ), null, true );

				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				( ( BasicConfirmScreen ) nextScreen ).setSpacing( CONFIRM_TITLE_Y_OFFSET, MENU_ENTRY_Y_OFFSET );
				( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( BasicConfirmScreen.INDEX_NO );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								// aloca os sons
								final String[] soundList = new String[ SOUND_TOTAL ];
								for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
									soundList[i] = PATH_SOUNDS + i + ".mid";
									log( 11 );
								}

								MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								//#if BLACKBERRY_API == "true"
//# 									MediaPlayer.setVolume( 60 );
								//#endif

								// teste
//								ScreenManager.SCREEN_HEIGHT = 260;
//								ScreenManager.SCREEN_HALF_HEIGHT = ScreenManager.SCREEN_HEIGHT >> 1;

								log( 12 );

								SoftLabel.load();
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								Border.load();

								log( 13 );

								Tag.load();
								
								softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
								softkeyMid = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_MID );
								softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );

								log( 21 );

								final ScreenManager manager = ScreenManager.getInstance();

								log( 22 );
								manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
								manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );

								loadScreen.setActive( false );

								setScreen( SCREEN_CHOOSE_LANGUAGE );
							}


						}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						Piece.load();

						log( 23 );

						log( 24 );
						ScrollBar.loadImages();
						log( 25 );
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						
						PlayScreen.load();

						log( 26 );

						loadScreen.setActive( false );

						setScreen( SCREEN_MAIN_MENU );
					}

				}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_GAME:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						playScreen = new PlayScreen();

						loadScreen.setActive( false );

						setScreen( SCREEN_NEXT_LEVEL );
					}

				}, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );

				MediaPlayer.free();
//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen( loader, getFont( FONT_TEXT_WHITE ), TEXT_LOADING );
				loader = null;

//				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			//#ifndef NO_RECOMMEND

			case SCREEN_RECOMMEND_SENT:
				nextScreen = new BorderedScreen( getText( TEXT_RECOMMEND_SENT ) + getRecommendURL() + "\n\n", SCREEN_MAIN_MENU, false );
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_RECOMMEND:
				final ScreenRecommend recommend = new ScreenRecommend( SCREEN_MAIN_MENU );
				final Form f = new Form( recommend );
				nextScreen = new BorderedScreen( f, 0 );
				recommend.setOffset( f.getPosition().mul( -1 ) );
				indexSoftRight = TEXT_BACK;
				bkgType = BACKGROUND_TYPE_NONE;
			break;

			//#endif

			//#if DEBUG == "true"
			case SCREEN_ERROR_LOG:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_TEXT_WHITE ), texts[ TEXT_LOG_TEXT ], false );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				indexSoftRight = TEXT_BACK;
			break;
			//#endif
		} // fim switch ( screen )


		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE ) {
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft, visibleTime );
		}

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE ) {
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, visibleTime );
		}

		setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, -1 );

		ScreenManager.getInstance().setCurrentScreen( nextScreen );
		setBackground( bkgType );

		return screen;
	} // fim do m�todo changeScreen( int )


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SPECIFIC:
				midlet.manager.setBackground( GameMIDlet.bkg, false );
			break;
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( BACKGROUND_COLOR );
			break;
		}
	} // fim do m�todo setBackground( byte )


	private static final DrawableGroup getConfirmTitle( int textIndex ) throws Exception {
		final DrawableGroup group = new DrawableGroup( 2 );

		//#if SCREEN_SIZE == "SMALL"
//# 			final RichLabel title = new RichLabel( getFont( FONT_TEXT_WHITE ), getText( textIndex ), Math.max( MENU_TAG_WIDTH, ScreenManager.SCREEN_WIDTH * 9 / 10 ) );
		//#else
			final RichLabel title = new RichLabel( getFont( FONT_TEXT_WHITE ), getText( textIndex ), MENU_TAG_WIDTH );
		//#endif
		title.setSize( title.getWidth(), title.getTextTotalHeight() );

		final Border border = new Border( Border.TYPE_ORANGE_BORDER_ORANGE_FILL );
		border.setSize( title.getSize().add( BORDER_THICKNESS, BORDER_THICKNESS ) );

		group.setSize( border.getSize() );
		group.insertDrawable( border );
		group.insertDrawable( title );

		title.setPosition( BORDER_HALF_THICKNESS, BORDER_HALF_THICKNESS );

		return group;
	}


	public static final Drawable getScrollFull() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollF = new Pattern( COLOR_SCROLL_BACK );
//# 			scrollF.setSize( 8, 0 );
//# 			return scrollF;
		//#else
			return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
		//#endif
	}


	public static final Drawable getScrollPage() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollP = new Pattern( COLOR_SCROLL_FORE );
//# 			scrollP.setSize( 8, 0 );
//# 			return scrollP;
		//#else
			return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
		//#endif
	}


	private static final Drawable createBasicMenu( int index, int[] entries ) throws Exception {
		final Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			menuEntries[ i ] = new Tag( i % Tag.COLORS_TOTAL, MENU_TAG_WIDTH, FONT_TEXT_WHITE, entries[ i ] );
		}

		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, menuEntries, MENU_ENTRY_Y_OFFSET, 0, entries.length - 1, null );

		return menu;
	}


	public static final boolean isLowMemory() {
//		return lowMemory;
		return false;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif
					
				//#ifdef NO_RECOMMEND
//# 					if ( index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//# 						++index;
//# 					}
				//#endif

				//#if WEB_EMULATOR == "false"
					if ( !SmsSender.isSupported() && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
						++index;
					}
				//#endif

				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						setScreen( SCREEN_LOADING_GAME );
						break;

					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
						break;

					case ENTRY_MAIN_MENU_HIGH_SCORES:
						setScreen( SCREEN_HIGH_SCORES );
						break;

					//#if DEMO == "true"
//# 					case ENTRY_MAIN_MENU_BUY_FULL_GAME:
//# 						setScreen( SCREEN_BUY_GAME_RETURN );
//# 					break;
					//#endif
						
					case ENTRY_MAIN_MENU_RECOMMEND:
						setScreen( SCREEN_RECOMMEND );
						break;

					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP );
						break;

					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
						break;

					//#if DEBUG == "true"
					case ENTRY_MAIN_MENU_ERROR_LOG:
						setScreen( SCREEN_ERROR_LOG );
					break;
					//#endif

					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
						break;
				} // fim switch ( index )
				break; // fim case SCREEN_MAIN_MENU


			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE

			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS

			//#if DEMO == "true"
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 							e.printStackTrace();
			//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif

			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						//#if DEMO == "false"
							HighScoresScreen.setScore( playScreen.getScore() );
							playScreen = null;
							MediaPlayer.saveOptions();
							MediaPlayer.stop();
							setScreen( SCREEN_MAIN_MENU );
						//#else
//# 						gameOver( 0 );
						//#endif
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );

				setScreen( SCREEN_SPLASH_NANO );
			break;

			case SCREEN_CHOOSE_LANGUAGE:
				index -= BasicConfirmScreen.INDEX_YES;
				if ( index != getLanguage() )
					setLanguage( ( byte ) index);

				setScreen( SCREEN_CHOOSE_SOUND );
			break;
		} // fim switch ( id )		
	} // fim do m�todo onChoose( Menu, int, int )


	public final void onItemChanged( Menu menu, int id, int index ) {
		for ( byte i = 0; i < menu.getUsedSlots(); ++i ) {
			if ( menu.getDrawable( i ) instanceof Tag ) {
				final Tag tag = ( Tag ) menu.getDrawable( i );
				tag.setSelected( i == index );
			}
		}
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente � chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece vis�vel. Para o label estar sempre vis�vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		//#if JAR != "min"
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
			softKeyRightText = textIndex;
		//#endif
		
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new SoftLabel( softKey, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do m�todo setSoftKeyLabel( byte, int )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_MID:
				if ( softkeyMid != null ) {
					softkeyMid.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	//#endif
	} // fim do m�todo setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}

	
	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	public static final String getWordsPath() {
		if ( language == LANGUAGE_PT )
			return PATH_IMAGES + "words_pt.bin";

		return PATH_IMAGES + "words_es.bin";
	}


	protected final void changeLanguage( byte language ) {
		try {
			switch ( language ) {
				case LANGUAGE_PT:
					( ( GameMIDlet ) instance ).loadTexts( TEXT_TOTAL, "/pt.dat" );
				break;

				default:
					language = LANGUAGE_ES;
				case LANGUAGE_ES:
					( ( GameMIDlet ) instance ).loadTexts( TEXT_TOTAL, "/es.dat" );
				break;
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				System.out.println( "Erro ao carregar idioma #" + language );
				e.printStackTrace();
			//#endif
		}

		final byte lang = language;
		try {
			saveData( DATABASE_NAME, DATABASE_SLOT_LANGUAGE, new Serializable() {
				public final void write( DataOutputStream output ) throws Exception {
					output.writeByte( lang );
				}


				public final void read( DataInputStream input ) throws Exception {
				}
			} );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				System.out.print( "Erro ao gravar idioma no RMS." );
				e.printStackTrace();
			//#endif
		}

		try {
			PlayScreen.updateWords();
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				System.out.println( "Erro ao atualizar tabelas de letras em PlayScreen" );
				e.printStackTrace();
			//#endif
		}
	}


	private static final class BorderedScreen extends UpdatableGroup implements KeyListener
			//#if SCREEN_SIZE != "SMALL"
			, PointerListener
			//#endif
	{

		private final KeyListener keyListener;

		//#if SCREEN_SIZE != "SMALL"
			private final PointerListener pointerListener;
		//#endif

		private final int nextScreen;

		private final boolean handleKeys;


		public BorderedScreen( int textIndex, int nextScreen ) throws Exception {
			this( textIndex, nextScreen, false );
		}


		public BorderedScreen( int textIndex, int nextScreen, boolean autoScroll ) throws Exception {
			this( loadScrollRichLabel( getText( textIndex ), autoScroll ), nextScreen, false );
		}


		public BorderedScreen( String text, int nextScreen, boolean autoScroll ) throws Exception {
			this( loadScrollRichLabel( text, autoScroll ), nextScreen, false );
		}


		public BorderedScreen( Drawable screen, int nextScreen ) throws Exception {
			this( screen, nextScreen, true );
		}

		public BorderedScreen( Drawable screen, int nextScreen, boolean setScreenSize ) throws Exception {
			super( 6 );

			this.nextScreen = nextScreen;

			//#if SCREEN_SIZE != "SMALL"
				if ( screen instanceof PointerListener )
					pointerListener = ( PointerListener ) screen;
				else
					pointerListener = null;
			//#endif

			if ( screen instanceof KeyListener )
				keyListener = ( KeyListener ) screen;
			else
				keyListener = null;

			final Pattern bkgTop = new Pattern( bkg );
			final Pattern bkgBottom = new Pattern( bkg );
			final Pattern bkgLeft = new Pattern( bkg );
			final Pattern bkgRight = new Pattern( bkg );

			insertDrawable( bkgTop );
			insertDrawable( bkgBottom );
			insertDrawable( bkgLeft );
			insertDrawable( bkgRight );

			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );


			final Point borderSize = detectSize();
			final Point diff = size.sub( borderSize ).div( 2 );

			if ( screen instanceof Form ) { // gambiarra para tratar corretamente o scroll
				handleKeys = false;
				if ( setScreenSize )
					screen.setSize( borderSize.x - ( BORDER_THICKNESS << 1 ), borderSize.y - BORDER_THICKNESS );
				screen.setPosition( diff.x + BORDER_THICKNESS + BORDER_HALF_THICKNESS, diff.y + BORDER_HALF_THICKNESS );
			} else {
				handleKeys = true;
				if ( setScreenSize )
					screen.setSize( borderSize.x - ( BORDER_THICKNESS << 1 ) - BORDER_HALF_THICKNESS, borderSize.y - BORDER_THICKNESS );
				screen.setPosition( diff.x + BORDER_THICKNESS, diff.y + BORDER_HALF_THICKNESS );
			}


			final Border border = new Border( Border.TYPE_BLUE_BORDER_ORANGE_FILL );
			border.setSize( borderSize );
			border.setPosition( diff );
			insertDrawable( border );
			insertDrawable( screen );

			// posiciona os patterns do fundo de forma a preencher toda a �rea sem ter que repintar desnecessariamente o tabuleiro
			int temp = diff.y;
			bkgTop.setSize( getWidth(), temp + BKG_SMALL_BLOCKS_SIZE - ( temp % BKG_SMALL_BLOCKS_SIZE ) );
			bkgLeft.setPosition( 0, bkgTop.getHeight() );
			temp = borderSize.y;
			bkgLeft.setSize( border.getPosX() + BORDER_HALF_THICKNESS, temp + BKG_SMALL_BLOCKS_SIZE - ( temp % BKG_SMALL_BLOCKS_SIZE ) );
			bkgRight.setSize( getWidth() - bkgRight.getPosX(), bkgLeft.getHeight() );
			temp = border.getPosX() + borderSize.x;
			bkgRight.setPosition( temp - ( temp % BKG_SMALL_BLOCKS_SIZE ), bkgTop.getHeight() );
			temp = bkgLeft.getPosY() + bkgLeft.getHeight() - BKG_SMALL_BLOCKS_SIZE - BORDER_HALF_THICKNESS;
			bkgBottom.setPosition( 0, temp - BKG_SMALL_BLOCKS_SIZE + ( temp % BKG_SMALL_BLOCKS_SIZE ) );
			bkgBottom.setSize( getWidth(), getHeight() - bkgBottom.getPosY() );
		}


		private static final ScrollRichLabel loadScrollRichLabel( String text, boolean autoScroll ) throws Exception {
			ScrollRichLabel textScreen;
			final Point size = detectSize();

			if ( autoScroll ) {
				textScreen = new ScrollRichLabel( new RichLabel( getFont( FONT_TEXT_WHITE ), text, size.x ) );
				textScreen.setSize( size.x - ( BORDER_THICKNESS << 1 ), size.y - BORDER_THICKNESS );
				textScreen.setAutoScroll( true );
				textScreen.setTextOffset( size.y );
			} else {
				final Drawable scrollFull = getScrollFull();
				final Drawable scrollPage = getScrollPage();

				textScreen = new ScrollRichLabel( new RichLabel( getFont( FONT_TEXT_WHITE ), text, size.x ) );
				textScreen.setSize( size.x - Math.max( scrollFull.getWidth(), scrollPage.getWidth() ), size.y - BORDER_THICKNESS );
				textScreen.setScrollFull( scrollFull );
				textScreen.setScrollPage( scrollPage );
			}

			return textScreen;
		}


		private static final Point detectSize() {
			//#if SCREEN_SIZE == "SMALL"
//# 				if ( ScreenManager.SCREEN_HEIGHT < HEIGHT_MIN )
//# 					return new Point( ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
//# 				else
//# 					return new Point( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#else
				return new Point( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#endif
		}


		public final void keyPressed( int key ) {
			switch ( key ) {
				case ScreenManager.FIRE:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					if ( handleKeys ) {
						GameMIDlet.setScreen( nextScreen );
						break;
					}

				default:
					if ( keyListener != null )
						keyListener.keyPressed( key );
			}
		}


		public final void keyReleased( int key ) {
			if ( keyListener != null )
				keyListener.keyReleased( key );
		}


		//#if SCREEN_SIZE != "SMALL"
			public final void onPointerDragged( int x, int y ) {
				if ( pointerListener != null )
					pointerListener.onPointerDragged( x, y );
			}


			public final void onPointerPressed( int x, int y ) {
				if ( pointerListener != null )
					pointerListener.onPointerPressed( x, y );
			}


			public final void onPointerReleased( int x, int y ) {
				if ( pointerListener != null )
					pointerListener.onPointerReleased( x, y );
			}
		//#endif

	} // fim da classe interna BorderedTextScreen


	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;


	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private final byte previousScreen;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );

			previousScreen = currentScreen;

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );

			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo s�o carregados aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa, para evitar poss�veis atrasos em transi��es e
					// garantir que novos recursos s� ser�o carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
									texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();

									setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									//#else
//# 									// volta � tela anterior
//# 									setScreen( previousScreen );
								//#endif
								}
							} // fim do m�todo run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do m�todo update( int )


		public final void paint( Graphics g ) {
			super.paint( g );

			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	// </editor-fold>
}
