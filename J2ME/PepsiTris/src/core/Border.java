/**
 * Border.java
 * 
 * Created on Mar 2, 2009, 2:33:55 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;

/**
 *
 * @author Peter
 */
public final class Border extends br.com.nanogames.components.userInterface.form.borders.Border implements Constants, Updatable {

	public static final byte TYPE_SPECIFIC					= 0;
	public static final byte TYPE_BLUE_BORDER_ORANGE_FILL	= 1;
	public static final byte TYPE_ORANGE_BORDER_ORANGE_FILL	= 2;
	public static final byte TYPE_BLUE_BORDER_BLUE_FILL		= 3;

	private static final byte BORDER_COLOR_BLUE = 0;
	private static final byte BORDER_COLOR_ORANGE = 1;

	private static final int COLOR_FILL_BLUE	= 0x00b6ef;
	private static final int COLOR_FILL_ORANGE	= 0xff7700;
	private static final int COLOR_FILL_FOCUSED	= 0x05ceff;
	private static final int COLOR_FILL_PRESSED	= 0x0187b2;

	private final Pattern patternLeft;
	private final Pattern patternRight;
	private final Pattern patternTop;
	private final Pattern patternBottom;

	private final Pattern fill;

	private final DrawableImage topLeft;
	private final DrawableImage topRight;
	private final DrawableImage bottomLeft;
	private final DrawableImage bottomRight;

	private final byte type;

	private final int fillColor;

	private static DrawableImage[] BORDER_TL = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_TR = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_BL = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_BR = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_TOP = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_LEFT = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_RIGHT = new DrawableImage[ 2 ];
	private static DrawableImage[] BORDER_BOTTOM = new DrawableImage[ 2 ];


	public static final void load() throws Exception {
		final String[] PATHS = { PATH_BORDER, PATH_BORDER2 };
		for ( byte i = 0; i < 2; ++i ) {
			final String PATH = PATHS[ i ];
			BORDER_TL[ i ] = new DrawableImage( PATH + "tl.png" );
			BORDER_BL[ i ] = new DrawableImage( PATH + "bl.png" );
			BORDER_TR[ i ] = new DrawableImage( PATH + "tr.png" );
			BORDER_BR[ i ] = new DrawableImage( PATH + "br.png" );

			BORDER_LEFT[ i ] = new DrawableImage( PATH + "left.png" );
			BORDER_TOP[ i ] = new DrawableImage( PATH + "top.png" );
			BORDER_RIGHT[ i ] = new DrawableImage( PATH + "right.png" );
			BORDER_BOTTOM[ i ] = new DrawableImage( PATH + "bottom.png" );
		}
	}


	public Border( byte type ) throws Exception {
		this( type, null );
	}


	public Border( byte type, Pattern fill ) throws Exception {
		super( 9 );

		this.type = type;

		String PATH = "";
		byte index;
		switch ( type ) {
			case TYPE_BLUE_BORDER_BLUE_FILL:
				PATH = PATH_BORDER;
				fillColor = COLOR_FILL_BLUE;
				index = BORDER_COLOR_BLUE;
			break;

			case TYPE_BLUE_BORDER_ORANGE_FILL:
				PATH = PATH_BORDER;
				fillColor = COLOR_FILL_ORANGE;
				index = BORDER_COLOR_BLUE;
			break;

			case TYPE_ORANGE_BORDER_ORANGE_FILL:
				PATH = PATH_BORDER2;
				fillColor = COLOR_FILL_ORANGE;
				index = BORDER_COLOR_ORANGE;
			break;

			default:
				PATH = PATH_BORDER;
				fillColor = COLOR_FILL_ORANGE;
				index = BORDER_COLOR_BLUE;
		}

		patternLeft = new Pattern( new DrawableImage( BORDER_LEFT[ index ] ) );
		patternLeft.setSize( patternLeft.getFill().getWidth(), 0 );
		insertDrawable( patternLeft );

		patternTop = new Pattern( new DrawableImage( BORDER_TOP[ index ] ) );
		patternTop.setSize( 0, patternTop.getFill().getHeight() );
		insertDrawable( patternTop );

		patternRight = new Pattern( new DrawableImage( BORDER_RIGHT[ index ] ) );
		patternBottom = new Pattern( new DrawableImage( BORDER_BOTTOM[ index ] ) );
		patternRight.setSize( patternRight.getFill().getWidth(), 0 );
		insertDrawable( patternRight );
		patternBottom.setSize( 0, patternBottom.getFill().getHeight() );
		insertDrawable( patternBottom );

		if ( fill != null ) {
			this.fill = fill;
		} else {
			this.fill = new Pattern( fillColor );
		}
		insertDrawable( this.fill );

		topLeft = new DrawableImage( BORDER_TL[ index ] );
		insertDrawable( topLeft );

		bottomLeft = new DrawableImage( BORDER_BL[ index ] );
		bottomLeft.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		insertDrawable( bottomLeft );
		
		topRight = new DrawableImage( BORDER_TR[ index ] );
		topRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_TOP );
		insertDrawable( topRight);

		bottomRight = new DrawableImage( BORDER_BR[ index ] );
		bottomRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_BOTTOM );
		insertDrawable( bottomRight );

		patternTop.setPosition( topLeft.getWidth(), 0 );
		patternLeft.setPosition( 0, topLeft.getHeight());
		patternRight.setPosition( 0, topRight.getHeight() );
		this.fill.setPosition( topLeft.getSize() );

		setLeft( patternLeft.getWidth() + 1 );
		setRight( patternRight.getWidth() + 1 );
		setTop( patternTop.getHeight() + 1 );
		setBottom( patternBottom.getHeight() + 1 );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		bottomLeft.setRefPixelPosition( 0, height );
		topRight.setRefPixelPosition( width, 0 );
		bottomRight.setRefPixelPosition( width, height );

		patternTop.setSize( width - patternTop.getPosX() - topRight.getWidth(), patternTop.getHeight() );
		patternBottom.setSize( patternTop.getWidth(), patternBottom.getHeight() );
		patternLeft.setSize( patternLeft.getWidth(), height - patternLeft.getPosY() - bottomLeft.getHeight() );
		patternRight.setSize( patternRight.getWidth(), patternLeft.getHeight() );

		fill.setSize( patternTop.getWidth(), patternLeft.getHeight() );

		patternBottom.setPosition( fill.getPosX(), bottomLeft.getPosY() );
		patternRight.setPosition( topRight.getPosX(), topRight.getHeight() );
	}


	public final br.com.nanogames.components.userInterface.form.borders.Border getCopy() throws Exception {
		return new Border( type );
	}


	public final void setState( int state ) {
		super.setState( state );

		if ( fill != null && fill.getFill() == null )  {
			switch ( state ) {
				case STATE_FOCUSED:
					fill.setFillColor( COLOR_FILL_FOCUSED );
				break;

				case STATE_PRESSED:
					fill.setFillColor( COLOR_FILL_PRESSED );
				break;

				case STATE_ERROR:
				case STATE_UNFOCUSED:
				case STATE_WARNING:
					fill.setFillColor( fillColor );
				break;
			}
		}
	}


	public final Pattern getFill() {
		return fill;
	}


	public final void update( int delta ) {
		if ( fill instanceof Updatable )
			fill.update( delta );
	}


}
