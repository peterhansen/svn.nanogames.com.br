/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/*******************************************************************************************
	 *                              DEFINI��ES GEN�RICAS                                       *
	 *******************************************************************************************/

	public static final byte LANGUAGE_ES = 0;
	public static final byte LANGUAGE_PT = 1;

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static final String URL_DEFAULT = "http://www.pepsimundo.com/mobile/games";
	
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	public static final String PATH_BKG = PATH_IMAGES + "bkg/";
	
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";

	/** Caminho das imagens das bordas. */
	public static final String PATH_BORDER = PATH_IMAGES + "border/";
	public static final String PATH_BORDER2 = PATH_IMAGES + "border2/";

	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";
	
	public static final String PATH_MENU = PATH_IMAGES + "menu/";
	public static final String PATH_CHARS = PATH_IMAGES + "chars.png";

	public static final byte TOTAL_PRODUCTS = 9;

	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	
	/** �ndice do slot de grava��o das op��es na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �ndice do slot de grava��o de um campeonato salvo. */
	public static final byte DATABASE_SLOT_HIGH_SCORES = 2;
	
	/** �ndice do slot de grava��o de um campeonato salvo. */
	public static final byte DATABASE_SLOT_LANGUAGE = 3;

	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 3;
	
	/** Cor padr�o do fundo de tela. */
	public static final int BACKGROUND_COLOR = 0x80b23e;

	public static final int COLOR_SCROLL_BACK = 0xf7f0d4;
	public static final int COLOR_SCROLL_FORE = 0x945b01;

	public static final int COLOR_SPLASH_TOP = 0xfff7b2;

	/** Quantidade total de palavras v�lidas. */
	public static final byte TOTAL_WORDS = 20;

	public static final byte WORDS_GROUP_ROWS = ( TOTAL_WORDS + 9 ) / 10;


	/** �ndice da fonte utilizada para mostrar mensagens na tela de jogo. */
	public static final byte FONT_BIG	= 0;
	
	/** �ndice da fonte utilizada para textos. */
	public static final byte FONT_TEXT_WHITE	= 1;
	
	/** �ndice da fonte utilizada para textos de menor destaque. */
	public static final byte FONT_TEXT_DARK	= 2;

	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 3;
	
	/** Total de tipos de fonte a carregar do jogo. */
	public static final byte FONT_TYPES_TO_LOAD	= FONT_TYPES_TOTAL - 1;

	public static final int COLOR_FONT_WHITE = 0xffffff;
	public static final int COLOR_FONT_DARK = 0xf28fae;

	//�ndices dos sons do jogo
	public static final byte SOUND_INDEX_SPLASH			= 0;
	public static final byte SOUND_INDEX_LEVEL_COMPLETE	= 1;
	public static final byte SOUND_INDEX_GAME_OVER		= 2;
	public static final byte SOUND_INDEX_GAME_COMPLETE	= 3;
	public static final byte SOUND_INDEX_AMBIENT_1		= 4;
	public static final byte SOUND_INDEX_AMBIENT_2		= 5;
	public static final byte SOUND_INDEX_AMBIENT_3		= 6;

	public static final byte SOUND_TOTAL = 7;

	//#ifdef VIBRATION_BUG
//#  		/** Dura��o padr�o da vibra��o, em milisegundos. */
//#  		public static final short VIBRATION_TIME_DEFAULT = 170;
//#
//#  		/***/
//#  		public static final short VIBRATION_TIME_CANT_MOVE = 130;
	//#else
		/** Dura��o padr�o da vibra��o, em milisegundos. */
		public static final short VIBRATION_TIME_DEFAULT = 350;

		/***/
		public static final short VIBRATION_TIME_CANT_MOVE = 300;
	//#endif

	/** Tamanho em pixels de cada quadradinho da imagem de fundo (valor usado para encaixar sem erros patterns). */
	public static final byte BKG_SMALL_BLOCKS_SIZE = 6;
	
	/** Dura��o da anima��o de transi��o da �gua. */
	public static final short WATER_TRANSITION_TIME = 450;

	/** Tempo total de cada n�vel: 3 minutos. */
	public static final int LEVEL_TIME = 3 * 60 * 1000;

	/** Altera��o na porcentagem de velocidade de queda a cada tecla pressionada (cima ou baixo). */
	public static final byte FALL_SPEED_CHANGE_STEP = 20;

	/** Dura��o da anima��o de desaparecimento da pe�a. */
	public static final short VANISH_TIME = 320;

	public static final byte TOTAL_COLUMNS	= 10;
	public static final byte TOTAL_ROWS		= 10;
	
	public static final byte TOTAL_PIECES = TOTAL_COLUMNS * TOTAL_ROWS;

	// <editor-fold defaultstate="collapsed" desc="�NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_HIGH_SCORES					= TEXT_OPTIONS + 1;
	public static final byte TEXT_PAUSE							= TEXT_HIGH_SCORES + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_RECOMMEND_SENT				= TEXT_HELP + 1;
	public static final byte TEXT_RECOMMEND_TEXT				= TEXT_RECOMMEND_SENT + 1;
	public static final byte TEXT_HELP_TEXT						= TEXT_RECOMMEND_TEXT + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_HELP_TEXT + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_BRAND					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_BRAND + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_CLEAR							= TEXT_YES + 1;
	public static final byte TEXT_NO							= TEXT_CLEAR + 1;
	public static final byte TEXT_VERSION						= TEXT_NO + 1;
	public static final byte TEXT_RECOMMEND_SMS					= TEXT_VERSION + 1;
	public static final byte TEXT_RECOMMEND_TITLE				= TEXT_RECOMMEND_SMS + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_RECOMMEND_TITLE + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT					= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_NEW_RECORD					= TEXT_GAME_OVER + 1;
	public static final byte TEXT_LEVEL							= TEXT_NEW_RECORD + 1;
	public static final byte TEXT_LEVEL_CAPS					= TEXT_LEVEL + 1;
	public static final byte TEXT_COMPLETE						= TEXT_LEVEL_CAPS + 1;
	public static final byte TEXT_WORDS							= TEXT_COMPLETE + 1;
	public static final byte TEXT_NEXT							= TEXT_WORDS + 1;
	public static final byte TEXT_CHOOSE_LANGUAGE				= TEXT_NEXT + 1;
	public static final byte TEXT_SPANISH						= TEXT_CHOOSE_LANGUAGE + 1;
	public static final byte TEXT_PORTUGUESE					= TEXT_SPANISH + 1;
	public static final byte TEXT_CONGRATULATIONS				= TEXT_PORTUGUESE + 1;
	public static final byte TEXT_LOG_TITLE						= TEXT_PORTUGUESE + 1;
	public static final byte TEXT_LOG_TEXT						= TEXT_LOG_TITLE + 1;
	
	/** n�mero total de textos do jogo */
	public static final byte TEXT_TOTAL = TEXT_LOG_TEXT + 1;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_CHOOSE_LANGUAGE			= 0;
	public static final byte SCREEN_CHOOSE_SOUND			= SCREEN_CHOOSE_LANGUAGE + 1;
	public static final byte SCREEN_SPLASH_NANO				= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND			= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME				= SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU				= SCREEN_SPLASH_GAME + 1;
//	public static final byte SCREEN_NEW_GAME				= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CONTINUE_GAME			= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_NEXT_LEVEL				= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_OPTIONS					= SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_HIGH_SCORES				= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP					= SCREEN_HIGH_SCORES + 1;
	public static final byte SCREEN_RECOMMEND_SENT			= SCREEN_HELP + 1;
	public static final byte SCREEN_RECOMMEND				= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_CREDITS					= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_PAUSE					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU			= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT			= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1				= SCREEN_CONFIRM_EXIT + 1;	
	public static final byte SCREEN_LOADING_2				= SCREEN_LOADING_1 + 1;	
	public static final byte SCREEN_LOADING_GAME			= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_LOADING_PLAY_SCREEN		= SCREEN_LOADING_GAME + 1;
	
	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG				= SCREEN_LOADING_PLAY_SCREEN + 1;
	//#endif
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_HIGH_SCORES	= 2;
	
	//#if DEMO == "true"
	//# 	/** �ndice da entrada do menu principal para se comprar a vers�o completa do jogo. */
	//# 	public static final byte ENTRY_MAIN_MENU_BUY_FULL_GAME	= 3;
	//# 	
	//# 	
	//# 	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	//# 	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	//# 	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;	
	//#endif
	
	public static final byte ENTRY_MAIN_MENU_RECOMMEND	= 3;
	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;
	
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>


	public static final short SOFT_KEY_VISIBLE_TIME = 2700;

	public static final byte TOTAL_LEVELS = 5;

	public static final byte GAME_BKG_TYPES_TOTAL = 3;

	public static final byte WORDS_GROUP_BORDER = 2;

	public static final byte WORDS_GROUP_X_DISTANCE = 2;

	public static final byte WORDS_GROUP_X = 4;

	public static final byte GAME_BKG_SPEED_PERCENT_MIN = 3;
	public static final byte GAME_BKG_SPEED_PERCENT_MAX = 25;
	
	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	public static final byte CONFIRM_TITLE_Y_OFFSET = 5;
//# 
//# 	public static final byte MENU_ENTRY_Y_OFFSET = -8;
//# 
//# 	public static final byte PIECE_WIDTH = 10;
//# 	public static final byte PIECE_HEIGHT = 10;
//# 
//# 	public static final byte PIECE_FULL_WIDTH = PIECE_WIDTH + 1;
//# 	public static final byte PIECE_FULL_HEIGHT = PIECE_HEIGHT + 1;
//# 
//# 	/** Largura em pixels do grupo das palavras na tela de jogo. */
//# 	public static final short WORDS_GROUP_WIDTH = 112;
//# 
//# 	/** Dist�ncia em pixels do grupo das palavras ao tabuleiro. */
//# 	public static final byte WORDS_GROUP_BOARD_OFFSET = 30;
//# 
//# 	public static final byte WORDS_GROUP_LABEL_OFFSET_X = 10;
//# 
//# 	/** Espessura da borda do tabuleiro. */
//# 	public static final byte BORDER_THICKNESS = 6;
//# 
//# 	/** Quantidade total de blocos usados na tela de splash. */
//# 	public static final byte TOTAL_BLOCKS = 4;
//# 
//# 	/** Offset na posi��o horizontal do block "bien" em rela��o ao bloco "cae" na tela de splash. */
//# 	public static final byte BLOCK_BIEN_OFFSET_X = 29;
//# 
//# 	/** Offset na posi��o horizontal do block "Pepsi" em rela��o ao bloco "cae" na tela de splash. */
//# 	public static final short BLOCK_PEPSI_OFFSET_X = 66;
//# 
//# 	/** Offset na posi��o vertical do block "Pepsi" em rela��o ao bloco "cae" na tela de splash. */
//# 	public static final byte BLOCK_PEPSI_OFFSET_Y = 8;
//# 
//# 	/** Largura interna de cada tag do menu. */
//# 	public static final short MENU_TAG_WIDTH = 84;
//# 
//# 	/** Deslocamento horizontal da tag selecionada. */
//# 	public static final byte TAG_SELECTED_X_OFFSET = -12;
//# 
//# 	/** Diferen�a na posi��o horizontal das tags do tabuleiro em rela��o � borda esquerda do tabuleiro. */
//# 	public static final byte TAG_BORDER_OFFSET_X = 5;
//# 
//# 	/***/
//# 	public static final byte TAG_SCORE_LABEL_OFFSET_X = 80;
//# 
//# 	/***/
//# 	public static final byte TAG_SCORE_LABEL_OFFSET_Y = -2;
//# 
//# 	public static final byte TAG_SCORE_LABEL_WIDTH = 45;
//# 
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X_PORTRAIT = 67;
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X_LANDSCAPE = 3;
//# 
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X = 36;
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_Y = 0;
//# 
//# 	public static final byte TAG_TIME_LABEL_OFFSET_X = 50;
//# 	public static final byte TAG_TIME_LABEL_OFFSET_Y = -2;
//# 	public static final byte TAG_TIME_LABEL_WIDTH = 27;
//# 
//# 	public static final byte WORDS_TABLE_TAG_OFFSET_Y = -9;
//# 
//# 	public static final byte TAGS_LANDSCAPE_Y_SPACING = 6;
//# 
//# 
//# 	public static final byte TAG_NEXT_CHAR_LABEL_OFFSET_X = 19;
//# 
//# 	/***/
//# 	public static final byte WORDS_FONT_HEIGHT_OFFSET = -6;
//# 
//# 	public static final byte CAE_BIEN_MSG_OFFSET_Y = 10;
//# 
//# 	/** Offset na posi��o vertical do label de mensagens da tela de jogo em rela��o ao centro da tela. */
//# 	public static final byte PLAYSCREEN_MESSAGE_OFFSET_Y = CAE_BIEN_MSG_OFFSET_Y - 3;
//# 
//# 	/***/
//# 	public static final byte HEIGHT_MIN = 120;
//# 
//# 	//</editor-fold>
//# 
	//#elif SCREEN_SIZE == "MEDIUM"
//#
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA M�DIA" defaultstate="collapsed">
//#
//# 	public static final byte CONFIRM_TITLE_Y_OFFSET = 8;
//#
//# 	public static final byte MENU_ENTRY_Y_OFFSET = -7;
//#
//# 	public static final byte PIECE_WIDTH = 16;
//# 	public static final byte PIECE_HEIGHT = 16;
//#
//# 	public static final byte PIECE_FULL_WIDTH = PIECE_WIDTH + 1;
//# 	public static final byte PIECE_FULL_HEIGHT = PIECE_HEIGHT + 1;
//#
//# 	/** Largura em pixels do grupo das palavras na tela de jogo. */
//# 	public static final short WORDS_GROUP_WIDTH = 112;
//#
//# 	/** Dist�ncia em pixels do grupo das palavras ao tabuleiro. */
//# 	public static final byte WORDS_GROUP_BOARD_OFFSET = 30;
//#
//# 	public static final byte WORDS_GROUP_LABEL_OFFSET_X = 10;
//#
//# 	/** Espessura da borda do tabuleiro. */
//# 	public static final byte BORDER_THICKNESS = 6;
//#
//# 	/** Quantidade total de blocos usados na tela de splash. */
//# 	public static final byte TOTAL_BLOCKS = 6;
//#
//# 	/** Offset na posi��o horizontal do block "bien" em rela��o ao bloco "cae" na tela de splash. */
//# 	public static final byte BLOCK_BIEN_OFFSET_X = 39;
//#
//# 	/** Offset na posi��o horizontal do block "Pepsi" em rela��o ao bloco "cae" na tela de splash. */
//# 	public static final short BLOCK_PEPSI_OFFSET_X = 87;
//#
//# 	/** Offset na posi��o vertical do block "Pepsi" em rela��o ao bloco "cae" na tela de splash. */
//# 	public static final byte BLOCK_PEPSI_OFFSET_Y = 11;
//#
//# 	/** Largura interna de cada tag do menu. */
//# 	public static final short MENU_TAG_WIDTH = 100;
//#
//# 	/** Deslocamento horizontal da tag selecionada. */
//# 	public static final byte TAG_SELECTED_X_OFFSET = -12;
//#
//# 	/** Diferen�a na posi��o horizontal das tags do tabuleiro em rela��o � borda esquerda do tabuleiro. */
//# 	public static final byte TAG_BORDER_OFFSET_X = 5;
//#
//# 	/***/
//# 	public static final byte TAG_SCORE_LABEL_OFFSET_X = 1;
//#
//# 	/***/
//# 	public static final byte TAG_SCORE_LABEL_OFFSET_Y = 3;
//#
//# 	public static final byte TAG_SCORE_LABEL_WIDTH = 53;
//#
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X_PORTRAIT = 67;
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X_LANDSCAPE = 3;
//#
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_Y_PORTRAIT = 0;
//#
//# 	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_Y_LANDSCAPE = 20;
//#
//# 	public static final byte TAG_TIME_LABEL_RIGHT_OFFSET_X = 108;
//# 	public static final byte TAG_TIME_LABEL_LEFT_OFFSET_X = 4;
//#
//# 	public static final byte WORDS_TABLE_TAG_OFFSET_Y = -9;
//#
//# 	public static final byte TAGS_LANDSCAPE_Y_SPACING = 6;
//#
//#
//# 	public static final byte TAG_NEXT_CHAR_LABEL_OFFSET_X = 19;
//#
//# 	/***/
//# 	public static final byte WORDS_FONT_HEIGHT_OFFSET = -2;
//#
//# 	public static final byte CAE_BIEN_MSG_OFFSET_Y = 18;
//#
//# 	/** Offset na posi��o vertical do label de mensagens da tela de jogo em rela��o ao centro da tela. */
//# 	public static final byte PLAYSCREEN_MESSAGE_OFFSET_Y = CAE_BIEN_MSG_OFFSET_Y - 7;
//#
//# 	//</editor-fold>
//#
//#
	//#elif SCREEN_SIZE == "BIG"

	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA GRANDE" defaultstate="collapsed">

	public static final byte CONFIRM_TITLE_Y_OFFSET = 10;

	public static final byte MENU_ENTRY_Y_OFFSET = -6;

	public static final byte PIECE_WIDTH = 22;
	public static final byte PIECE_HEIGHT = 22;

	public static final byte PIECE_FULL_WIDTH = PIECE_WIDTH + 1;
	public static final byte PIECE_FULL_HEIGHT = PIECE_HEIGHT + 1;

	/** Largura em pixels do grupo das palavras na tela de jogo. */
	public static final short WORDS_GROUP_WIDTH = 112;

	/** Dist�ncia em pixels do grupo das palavras ao tabuleiro. */
	public static final byte WORDS_GROUP_BOARD_OFFSET = 30;

	public static final byte WORDS_GROUP_LABEL_OFFSET_X = 10;

	public static final short PLAYSCREEN_HEIGHT_MIN = 280;

	/** Espessura da borda do tabuleiro. */
	public static final byte BORDER_THICKNESS = 6;

	/** Quantidade total de blocos usados na tela de splash. */
	public static final byte TOTAL_BLOCKS = 8;

	/** Offset na posi��o horizontal do block "bien" em rela��o ao bloco "cae" na tela de splash. */
	public static final byte BLOCK_BIEN_OFFSET_X = 61;

	/** Offset na posi��o horizontal do block "Pepsi" em rela��o ao bloco "cae" na tela de splash. */
	public static final short BLOCK_PEPSI_OFFSET_X = 138;

	/** Offset na posi��o vertical do block "Pepsi" em rela��o ao bloco "cae" na tela de splash. */
	public static final byte BLOCK_PEPSI_OFFSET_Y = 19;

	/** Largura interna de cada tag do menu. */
	public static final short MENU_TAG_WIDTH = 140;

	/** Deslocamento horizontal da tag selecionada. */
	public static final byte TAG_SELECTED_X_OFFSET = -18;

	/** Diferen�a na posi��o horizontal das tags do tabuleiro em rela��o � borda esquerda do tabuleiro. */
	public static final byte TAG_BORDER_OFFSET_X = 5;

	/***/
	public static final byte TAG_SCORE_LABEL_OFFSET_X = 1;

	/***/
	public static final byte TAG_SCORE_LABEL_OFFSET_Y = 3;

	public static final byte TAG_SCORE_LABEL_WIDTH = 72;

	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X_PORTRAIT = 53;
	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_X_LANDSCAPE = 3;
	public static final byte TAG_NEXT_CHAR_ICON_OFFSET_Y_PORTRAIT = 20;

	public static final byte TAG_TIME_LABEL_RIGHT_OFFSET_X = 108;
	public static final byte TAG_TIME_LABEL_LEFT_OFFSET_X = 4;

	public static final byte SCORE_TAG_Y_OFFSET_HEIGHT_MIN = -16;

	public static final byte WORDS_TABLE_TAG_OFFSET_Y = -9;

	public static final byte TAGS_LANDSCAPE_Y_SPACING = 7;

	public static final byte TAG_NEXT_CHAR_LABEL_OFFSET_X = 26;

	public static final byte WORDS_FONT_HEIGHT_OFFSET = 0;

	public static final byte CAE_BIEN_MSG_OFFSET_Y = 22;

	/** Offset na posi��o vertical do label de mensagens da tela de jogo em rela��o ao centro da tela. */
	public static final byte PLAYSCREEN_MESSAGE_OFFSET_Y = CAE_BIEN_MSG_OFFSET_Y - 7;
	//</editor-fold>


	//#endif

	/** Espessura da borda do tabuleiro. */
	public static final byte BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;

	public static final short TIME_FALL = 600;

	/** Velocidade de queda das pe�as, em pixels por segundo. */
	public static final short SPEED_FALL = PIECE_HEIGHT * 1000 / TIME_FALL;

}
