package core;

import br.com.nanogames.components.UpdatableGroup;


public final class PieceGroup extends UpdatableGroup implements Constants {

	public PieceGroup( Piece movingPiece ) throws Exception {
		super( TOTAL_PIECES + 1 );
		setSize( ( TOTAL_COLUMNS * PIECE_WIDTH ) + 1, ( TOTAL_ROWS * PIECE_HEIGHT ) + 1 );
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );

		insertDrawable( movingPiece );
		for ( byte i = 0; i < TOTAL_PIECES; ++i ) {
			insertDrawable( new Piece() );
		}
	}


	public final void reset() {
		for ( byte i = 1; i < activeDrawables; ++i )
			( ( Piece ) drawables[ i ] ).setState( Piece.STATE_NONE );
	}


	public final Piece insertPiece( Piece p ) {
		// come�a em 1 devido � movingPiece
		for ( byte i = 1; i < drawables.length; ++i ) {
			final Piece p1 = ( Piece ) drawables[ i ];
			if ( p1.getState() == Piece.STATE_NONE ) {
				final Piece ret = ( Piece ) drawables[ i ];
				ret.setCharIndex( p.getCharIndex() );
				ret.setState( Piece.STATE_IDLE );
				ret.setPosition( p.getPosition() );

				return ret;
			}
		}

		return null;
	}


}
