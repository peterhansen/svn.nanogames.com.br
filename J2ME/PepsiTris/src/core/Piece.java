/**
 * Fruit.java
 * 
 * Created on 14/Fev/2009, 12:55:56
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Piece extends Drawable implements Constants, Updatable {

	public static final byte STATE_NONE			= -1;
	public static final byte STATE_IDLE			= 0;
	public static final byte STATE_MOVING		= 1;
	public static final byte STATE_VANISHING	= 2;
	public static final byte STATE_VANISHING_2	= 3;
	public static final byte STATE_FALLING		= 4;

	public static final byte COLOR_BLUE		= 0;
	public static final byte COLOR_PINK		= 1;
	public static final byte COLOR_GREEN	= 2;
	public static final byte COLOR_ORANGE	= 3;

	public static final byte TOTAL_COLORS = 4;

	private static final int[][] PIECE_COLORS =
		{	{ 0x40dafc, 0x0accf7, 0x00bde7, 0x029ec1, 0x007fa0 },
			{ 0xff276c, 0xee004d, 0xd60045, 0xb6003b, 0x960030 },
			{ 0x82f75a, 0x5bce33, 0x51b92d, 0x449c26, 0x398120 },
			{ 0xfa8d42, 0xf96a07, 0xe15f06, 0xbd5005, 0x9c4204 } };

	/***/
	public static final byte TOTAL_CHARS = 26;

	/** �ndice do caracter (varia de zero a TOTAL_CHARS - 1 ). */
	private byte charIndex;

	/** �ndice da cor atual da pe�a. */
	private byte color;
	
	private static Image[] IMAGES_CHARS;

	private byte state;

	/** Limite de queda da pe�a. */
	private short yLimit;

	/** Velocidade de queda da pe�a. */
	private final MUV muv = new MUV();

	private short accTime;

	private static final Point offset = new Point();


	public static final void load() throws Exception {
		if ( IMAGES_CHARS == null ) {
			GameMIDlet.log( 14 );
			IMAGES_CHARS = new Image[ TOTAL_CHARS ];
			GameMIDlet.log( 15 );

			IMAGES_CHARS[ 0 ] = Image.createImage( PATH_CHARS );

			for ( byte i = 1; i < TOTAL_COLORS; ++i ) {
				final PaletteMap[] maps = new PaletteMap[ PIECE_COLORS[ i ].length ];
				for ( byte j = 0; j < maps.length; ++j )
					maps[ j ] = new PaletteMap( PIECE_COLORS[ COLOR_BLUE ][ j ], PIECE_COLORS[ i ][ j ] );

				final PaletteChanger paletteChanger = new PaletteChanger( PATH_CHARS );
				IMAGES_CHARS[ i ] = paletteChanger.createImage( maps );
			}
			GameMIDlet.log( 16 );
		}
	}


	public Piece() throws Exception {
		setSize( PIECE_FULL_WIDTH, PIECE_FULL_HEIGHT );
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		setColor( COLOR_BLUE );
		setState( STATE_NONE );
	}


	public final void setChar( char c ) {
		setCharIndex( ( byte ) ( c - 'A' ) );
	}
	
	
	public final void setCharIndex( byte charIndex ) {
		this.charIndex = charIndex;
	}


	public final byte getColor() {
		return color;
	}


	public final void setColor( int color ) {
		this.color = ( byte ) color;
	}


	public final byte getCharIndex() {
		return charIndex;
	}


	public final char getChar() {
		return ( char ) ( 'A' + charIndex );
	}


	public final void draw( Graphics g ) {
		if ( visible ) {
			pushClip( g );
			translate.addEquals( position );

			final Rectangle clip = clipStack[ currentStackSize ];
			clip.setIntersection( translate.x, translate.y, size.x, size.y );

			if ( clip.width > 0 && clip.height > 0 ) {
				g.setClip( clip.x, clip.y, clip.width, clip.height );
				g.drawImage( IMAGES_CHARS[ color ], translate.x - ( charIndex * PIECE_FULL_WIDTH ), translate.y, 0 );
			}

			translate.subEquals( position );
			popClip( g );
		}
	}


	protected final void paint( Graphics g ) {
	}


	public final byte getState() {
		return state;
	}


	public final void setState( int state ) {
		this.state = ( byte ) state;
		setViewport( null );

		setVisible( state != STATE_NONE );

		switch ( state ) {
			case STATE_IDLE:
				setColor( COLOR_BLUE );
			break;

			case STATE_MOVING:
				setColor( COLOR_PINK );
			break;

			case STATE_VANISHING:
				setColor( COLOR_GREEN );
			case STATE_VANISHING_2:
				setViewport( new Rectangle( offset.x + position.x, offset.y + position.y, size.x, size.y ) );
			break;

			case STATE_FALLING:
				muv.setSpeed( SPEED_FALL );
			break;
		}
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_VANISHING:
			case STATE_VANISHING_2:
				accTime -= delta;

				if ( accTime <= VANISH_TIME ) {
					if ( accTime > 0 ) {
						viewport.width = size.x * accTime / VANISH_TIME;
						viewport.x = offset.x + position.x + ( ( size.x - viewport.width ) >> 1 );

						viewport.height = size.y * accTime / VANISH_TIME;
						viewport.y = offset.y + position.y + size.y - viewport.height;
					} else {
						setState( STATE_NONE );
					}
				}
			break;

			case STATE_FALLING:
				if ( accTime > 0 ) {
					accTime -= delta;
				} else {
					move( 0, muv.updateInt( delta ) );

					if ( getPosY() >= yLimit ) {
						setPosition( getPosX(), yLimit );
						setState( STATE_IDLE );
					}
				}
			break;
		}
	}


	public final void setYLimit( int yLimit ) {
		this.yLimit = ( short ) yLimit;
	}


	public final short getYLimit() {
		return yLimit;
	}


	public final void fall( int pieces, int steps ) {
		accTime = ( short ) ( VANISH_TIME * steps );
		setYLimit( getPosY() + pieces * PIECE_HEIGHT );
		setState( STATE_FALLING );
	}


	public final void vanish( boolean green, int steps ) {
		accTime = ( short ) ( VANISH_TIME * ( steps + 1 ) );
		setState( green ? STATE_VANISHING : STATE_VANISHING_2 );
	}


	public static final void setOffset( int x, int y ) {
		offset.set( x, y );
	}

}
