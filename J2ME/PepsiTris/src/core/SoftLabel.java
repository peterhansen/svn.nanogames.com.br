package core;

import screens.*;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;


public final class SoftLabel extends UpdatableGroup implements Constants {

	private final DrawableImage borderLeft;
	private final DrawableImage borderRight;

	private static DrawableImage BORDER_LEFT;
	private static DrawableImage SCORE_FILL;

	//#if JAR == "min"
//# 		private short accTime;
	//#endif

	
	public static final void load() throws Exception {
		BORDER_LEFT = new DrawableImage( PATH_IMAGES + "tags/b_0.png" );
		SCORE_FILL = new DrawableImage( PATH_IMAGES + "tags/b_1.png" );
	}

	
	public SoftLabel( byte softKey, int textIndex ) throws Exception {
		super( 4 );
		
		final Pattern fill = new Pattern( SCORE_FILL );
		insertDrawable( fill );

		final MarqueeLabel label = new MarqueeLabel( GameMIDlet.getFont( FONT_TEXT_WHITE ), GameMIDlet.getText( textIndex ) );

		final int TOTAL_HEIGHT = label.getPosY() + label.getHeight() + 2;

		fill.setSize( label.getWidth(), fill.getFill().getHeight() );
		label.setPosition( 0, ( fill.getFill().getHeight() - label.getHeight() ) >> 1 );

		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_RIGHT:
				borderLeft = new DrawableImage( BORDER_LEFT );
				insertDrawable( borderLeft );
				
				label.setSize( label.getFont().getTextWidth( label.getText() ) + 2, label.getHeight() );
				label.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
				setSize( borderLeft.getWidth() + label.getWidth(), TOTAL_HEIGHT );
				fill.setPosition( borderLeft.getWidth(), 0 );
				label.setPosition( fill.getPosX(), label.getPosY() );

				borderRight = null;
				fill.setSize( label.getWidth(), TOTAL_HEIGHT );

				//#if JAR == "min"
//# 					accTime = SOFT_KEY_VISIBLE_TIME;
				//#endif
			break;

			case ScreenManager.SOFT_KEY_LEFT:
				borderRight = new DrawableImage( BORDER_LEFT );
				borderRight.mirror( TRANS_MIRROR_H );
				insertDrawable( borderRight );

				borderLeft = null;

				label.setSize( label.getFont().getTextWidth( label.getText() )  + 2, label.getHeight() );
				label.setScrollMode( MarqueeLabel.SCROLL_MODE_NONE );
				setSize( borderRight.getWidth() + label.getWidth(), TOTAL_HEIGHT );
				borderRight.setPosition( label.getWidth(), 0 );
				label.setPosition( fill.getPosX() + 2, label.getPosY() );

				fill.setSize( label.getWidth(), TOTAL_HEIGHT );

				//#if JAR == "min"
//# 					accTime = SOFT_KEY_VISIBLE_TIME;
				//#endif
			break;

			case ScreenManager.SOFT_KEY_MID:
				borderLeft = new DrawableImage( BORDER_LEFT );
				insertDrawable( borderLeft );
				borderRight = new DrawableImage( BORDER_LEFT );
				borderRight.mirror( TRANS_MIRROR_H );
				insertDrawable( borderRight );

				setSize( ScreenManager.SCREEN_WIDTH, TOTAL_HEIGHT );
				label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
				label.setSize( size.x - 2, label.getHeight() );
				label.setPosition( 1, label.getPosY() );
				label.setTextOffset( label.getWidth() );

				fill.setPosition( borderLeft.getWidth(), 0 );

				borderRight.setPosition( size.x - borderLeft.getWidth(), borderLeft.getPosY() );
				borderLeft.setVisible( true );
				borderRight.setVisible( true );
				fill.setSize( borderRight.getPosX() - fill.getPosX(), TOTAL_HEIGHT );
			break;

			default:
				throw new Exception();
		}

		insertDrawable( label );
	}


	//#if JAR == "min"
//# 		// como na vers�o de jar m�nimo n�o h� a classe BasicAnimatedSoftKey, � implementada uma alternativa para
//# 		// fazer os labels sumirem ap�s algum tempo
//# 		public final void update( int delta ) {
//# 			super.update( delta );
//#
//# 			if ( accTime > 0 ) {
//# 				accTime -= delta;
//# 				if ( accTime <= 0 )
//# 					setVisible( false );
//# 			}
//# 		}
	//#endif

}
