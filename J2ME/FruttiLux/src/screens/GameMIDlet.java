/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */
package screens;

import core.SoftLabel;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Border;
import core.Constants;
import core.DropEmitter;
import core.Fruit;
import core.TimeBar;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Graphics;

//#if NO_RECOMMEND == "false"
//# 	import br.com.nanogames.components.util.SmsSender;
//# 	import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if JAR != "min"
import core.ScrollBar;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.online.ad.Resource;
import br.com.nanogames.components.online.ad.ResourceManager;
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.*;
import core.MenuLabel;
import java.util.Hashtable;
//#endif

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable {

	private static final short GAME_MAX_FRAME_TIME = 145;

	private static final byte BACKGROUND_TYPE_SPECIFIC = 0;

	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;

	private static final byte BACKGROUND_TYPE_NONE = 2;
	private static int softKeyRightText;

	public static final byte OPTION_ENGLISH = 1;

	public static final byte OPTION_PORTUGUESE = 2;

	private byte indexLanguage;
	//#if JAR != "min"
		/** gerenciador da animação da soft key esquerda */
		private static BasicAnimatedSoftkey softkeyLeft;

		/** gerenciador da animação da soft key esquerda */
		private static BasicAnimatedSoftkey softkeyMid;

		/** gerenciador da animação da soft key direita */
		private static BasicAnimatedSoftkey softkeyRight;

		/** Gerenciador da animação de transição de tela. */
		private static WaterTransition transition;

		//#if GRAPHISM == "true"
			private static Graphism bkg;
		//#endif

	//#endif

	//#if JAR == "full"
		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
		private static final int LOW_MEMORY_LIMIT = 1050000;
		private static final int LOW_MEMORY_LIMIT_2 = 1350000;
	//#endif

	/** Referência para a tela de jogo, para que seja possível retornar à tela de jogo após entrar na tela de pausa. */
	private static PlayScreen playScreen;

	private static boolean lowMemory;

	private static LoadListener loader;

	/** Indica se o menu já foi exibido alguma vez (caso contrário, não mostra transição ao trocar para ele). */
	private boolean menuWasShown;

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif

	//#if NO_RECOMMEND == "false"
//# 		private Drawable recommendScreen;
	//#endif

	public static final byte ENTRY_MAIN_MENU_NEW_GAME = 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS = 1;
	//public static final byte ENTRY_MAIN_MENU_HIGH_SCORES	= 2;
	//#if DEMO == "true"
	//# 	/** Índice da entrada do menu principal para se comprar a versão completa do jogo. */
	//# 	public static final byte ENTRY_MAIN_MENU_BUY_FULL_GAME	= 3;
	//#
	//#
	//# 	public static final byte ENTRY_MAIN_MENU_HELP		= 4;
	//# 	public static final byte ENTRY_MAIN_MENU_CREDITS	= 5;
	//# 	public static final byte ENTRY_MAIN_MENU_EXIT		= 6;
	//#endif

	public static final byte MAIN_MENU_NANO_ONLINE = 2;
	public static final byte ENTRY_MAIN_MENU_PRODUCTS = 3;
	public static final byte ENTRY_MAIN_MENU_RECOMMEND = 4;
	public static final byte ENTRY_MAIN_MENU_HELP = 5;
	public static final byte ENTRY_MAIN_MENU_CREDITS = 6;
	public static final byte ENTRY_MAIN_MENU_EXIT = 7;
	public static final byte MAIN_MENU_RECOMMEND = 8;
	public static final byte MAIN_MENU_HELP = 9;
	public static final byte MAIN_MENU_CREDITS = 10;
	public static final byte MAIN_MENU_EXIT = 11;
	public static final byte MAIN_MENU_LOG = 12;
	//#if DEBUG == "true"
		public static final byte ENTRY_MAIN_MENU_ERROR_LOG = ENTRY_MAIN_MENU_EXIT + 1;
	//#endif

	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#elif VENDOR == "MOTOROLA"
//# 			super( VENDOR_MOTOROLA, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
		//#endif

		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
			
//		// cria a base de dados do jogo
//		try {
//			createDatabase( "BLABLA", 1 );
//		} catch ( Exception e ) {
//		}
//
//		Logger.setRMS( "BLABLA", 1 );
//		log( 0 );
		// TODO corrigir ©
	}


	public static final void log( int i ) {
//		AppMIDlet.gc();
//		Logger.log( i + ": " + Runtime.getRuntime().freeMemory() );
	}


	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}

		//#endif
	}


	protected final void loadResources() throws Exception {
		log( 1 );
		//#if JAR == "full"
			switch ( getVendor() ) {
				//#if SCREEN_SIZE == "BIG"

					case VENDOR_SAMSUNG:
						lowMemory = Runtime.getRuntime().totalMemory() < LOAD_ALL_MEMORY_SAMSUNG;
					break;

					case VENDOR_NOKIA:
					case VENDOR_MOTOROLA:
				//#endif
				case VENDOR_SONYERICSSON:
				case VENDOR_SIEMENS:
				case VENDOR_BLACKBERRY:
				case VENDOR_HTC:
				break;

				default:
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
		//#else
//# 			lowMemory = true;
		//#endif
	//lowMemory = true; // teste

		//#if JAR == "min"
//# 			FONTS[ 0 ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_2.png", PATH_IMAGES + "font_2.dat" );
//# 			FONTS[ 0 ].setCharOffset( 1 );
//#
//# 			FONTS[ FONT_POINTS ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_4.png", PATH_IMAGES + "font_4.dat" );
//# 			FONTS[ FONT_POINTS ].setCharOffset( 1 );
//#
//# 			FONTS[ FONT_BIG ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_3.png", PATH_IMAGES + "font_3.dat" );
		//#else
			for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
				if ( ( i != FONT_MENU && i != FONT_MENU_OVER ) || !isLowMemory() ) {
					FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i + ".png", PATH_IMAGES + "font_" + i + ".dat" );
					if ( i != FONT_BIG )
						FONTS[ i ].setCharExtraOffset( 1 );
					log( 6 );
				}
			}

			if ( isLowMemory() ) {
				FONTS[ FONT_MENU ] = FONTS[ FONT_TEXT ];
				FONTS[ FONT_MENU_OVER ] = FONTS[ FONT_MENU ];
			}
		//#endif

			// inicializa as tabelas de ranking online
		//#if NANO_RANKING == "true"
			RankingScreen.init( new int[] { TEXT_HIGH_SCORES }, new RankingFormatter() {
				public String format( int type, long score ) {
					return String.valueOf( score );
				}

				public final void initLocalEntry( int type, RankingEntry entry, int index ) {
					entry.setScore( ( 20 - index ) * 100 );
					final String[] names = new String[] {
						"piteco",
						"tommy",
						"vivs",
						"weifols",
						"cammy",
						"eddie",
						"maxxx",
						"monty",
						"dimmy",
						"peta",
					};
					entry.setNickname( names[ index % names.length ] );
				}
		} );
		//#endif

		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}

		log( 7 );
        loadTexts(TEXT_TOTAL, PATH_IMAGES + "en.dat");
		log( 8 );
		try{
			AppMIDlet.loadData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
		} catch (Exception e) {
			language = NanoOnline.LANGUAGE_en_US;
			loadTexts(TEXT_TOTAL, PATH_IMAGES + "en.dat");
		}

         setSpecialKeyMapping(true);
		setScreen( SCREEN_LOADING_1 );
	} // fim do método loadResources()


	public final void destroy() {
 		if ( playScreen != null ) {
			//#if NANO_RANKING == "true"
				try {
					final Customer c = NanoOnline.getCurrentCustomer();
					RankingScreen.setHighScore(RANKING_TYPE_SCORE, c.getId(), playScreen.getScore(), true);
				} catch (Exception e) {
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif
				}
			//#else
	//# 			HighScoresScreen.setScore( playScreen.getScore() );
			//#endif
 			playScreen = null;
		}

		super.destroy();
	}


	public static final void gameOver( int score ) {
		//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				if ( score > 0 && RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true ) > 0 ) {
					setScreen( SCREEN_LOADING_HIGH_SCORES );
				} else {
					setScreen( SCREEN_MAIN_MENU );
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				setScreen( SCREEN_MAIN_MENU );
			}
		//#else
//# 		if ( HighScoresScreen.setScore( score ) ) {
//# 			setScreen( SCREEN_HIGH_SCORES );
//# 		} else {
//# 			setScreen( SCREEN_MAIN_MENU );
//# 		}
			//#endif
	}


	protected final int changeScreen( int screen ) throws Exception {
		log( 10 );
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;
		boolean useTransition = false;
		boolean retryLevel = false;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_SPECIFIC;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;
		byte indexSoftMid = SOFT_KEY_REMOVE;

		short visibleTime = manager.hasPointerEvents() ? -1 : SOFT_KEY_VISIBLE_TIME;

		switch ( screen ) {
			case SCREEN_CHOOSE_SOUND:
				//#if JAR == "min"
//# 					final BasicConfirmScreen confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), getCursor(), TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, false );
//# 					confirm.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
//# 					confirm.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				//#else
					BasicConfirmScreen confirm = null;
					if ( isLowMemory() ) {
						confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), getCursor(), TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, false );
						confirm.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
						confirm.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
					} else {
						confirm = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, getConfirmTitle( TEXT_DO_YOU_WANT_SOUND ), new MenuLabel( TEXT_YES ), new MenuLabel( TEXT_NO ), null, false );
						if ( !MediaPlayer.isMuted() ) {
							confirm.setCurrentIndex( BasicConfirmScreen.INDEX_YES );
						}
					}
				//#endif

				confirm.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				confirm.setSpacing( TITLE_SPACING, ENTRIES_SPACING );

				nextScreen = confirm;
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CHOOSE_LANGUAGE:
				indexSoftRight = TEXT_BACK;
				indexSoftLeft = TEXT_OK;
				BasicConfirmScreen lang = null;
					
				if (!isLowMemory()) {
					lang = new BasicConfirmScreen(midlet, screen, getFont(FONT_MENU_OVER), null, getConfirmTitle(TEXT_CHOOSE_YOUR_LANGUAGE), new MenuLabel(TEXT_ENGLISH), new MenuLabel(TEXT_PORTUGUESE), null, false);
					lang.setSpacing(TITLE_SPACING, ENTRIES_SPACING);
					lang.setEntriesAlignment(BasicConfirmScreen.ALIGNMENT_VERTICAL);
					if (language == NanoOnline.LANGUAGE_en_US) {
						lang.setCurrentIndex(OPTION_ENGLISH);
					} else {
						lang.setCurrentIndex(OPTION_PORTUGUESE);
					}
					nextScreen = lang;
				} else {
					    lang = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), getCursor(), TEXT_CHOOSE_YOUR_LANGUAGE, TEXT_ENGLISH, TEXT_PORTUGUESE, false );
						lang.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
						lang.setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );

					if (language == NanoOnline.LANGUAGE_en_US) {
						lang.setCurrentIndex(OPTION_ENGLISH);
					} else {
						lang.setCurrentIndex(OPTION_PORTUGUESE);
					}
					nextScreen = lang;

				}

				
				indexSoftLeft = TEXT_OK;
				break;

			case SCREEN_SPLASH_NANO:
				//#if NANO_RANKING == "false"
//# 				HighScoresScreen.createInstance( getFont( FONT_MENU_OVER ), DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
                //#endif
				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "SMALL"
//# 					nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, MediaPlayer.LOOP_INFINITE );
				//#else
					nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, SOUND_INDEX_SPLASH, MediaPlayer.LOOP_INFINITE );
				//#endif
			break;

//			case SCREEN_SPLASH_BRAND:
//				bkgType = BACKGROUND_TYPE_NONE;
//				nextScreen = new BasicSplashBrand( SCREEN_SPLASH_GAME, 0x000000, PATH_SPLASH, PATH_SPLASH + "nano.png", TEXT_SPLASH_BRAND  );
//			break;

			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame();
				bkgType = BACKGROUND_TYPE_NONE;
			break;


			case SCREEN_MAIN_MENU:
				if (language == NanoOnline.LANGUAGE_pt_BR) {
					playScreen = null;
				//#if NANO_RANKING == "true"
				if (nanoOnlineForm != null) {
					NanoOnline.unload();
					nanoOnlineForm = null;
				}
				//#endif

				//#if NO_RECOMMEND == "false"
//# 				recommendScreen = null;
				//#endif
				//#ifdef WEB_EMULATOR
//# 					nextScreen = createBasicMenu( screen, new int[] {
//#  							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_PRODUCTS_TITLE,
//# 							TEXT_RECOMMEND_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS } );
				//#else
					indexSoftRight = TEXT_EXIT;
					//#if NO_RECOMMEND == "true"
//# 						nextScreen = createBasicMenu( screen, new int[] {
//# 								TEXT_NEW_GAME,
//# 								TEXT_OPTIONS,
//# 								TEXT_HIGH_SCORES,
//# 								TEXT_PRODUCTS_TITLE,
//# 								TEXT_HELP,
//# 								TEXT_CREDITS,
//# 								TEXT_EXIT, } );
					//#else
						//#if BLACKBERRY_API == "true"
//# 							nextScreen = createBasicMenu( screen, ( showRecommendScreen && SmsSender.isSupported() ) ? new int[] {
				//#else


				nextScreen = createBasicMenu(screen, SmsSender.isSupported() ? new int[]{
							//#endif

					//#if NANO_RANKING == "true"

							TEXT_NEW_GAME,
							TEXT_OPTIONS,
							TEXT_NANO_ONLINE,
							TEXT_PRODUCTS_TITLE,
							TEXT_RECOMMEND_TITLE,
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,}
						: new int[]{
							TEXT_NEW_GAME,
							TEXT_OPTIONS,
							TEXT_NANO_ONLINE,
							TEXT_PRODUCTS_TITLE,
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,});
				//#else
//# 				            TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_PRODUCTS_TITLE,
//# 							TEXT_RECOMMEND_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS,
//# 							TEXT_EXIT,}
//# 						: new int[]{
//# 							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_PRODUCTS_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS,
//# 							TEXT_EXIT,});
//#
//#
				//#endif
				//#endif
				//#endif

				if ( !menuWasShown ) {
					menuWasShown = true;
					useTransition = true;
				}

				indexSoftLeft = TEXT_OK;
				}else if (language == NanoOnline.LANGUAGE_en_US) {
					playScreen = null;
					//#if NANO_RANKING == "true"
					if (nanoOnlineForm != null) {
						NanoOnline.unload();
						nanoOnlineForm = null;
					}
					//#endif

				//#if NO_RECOMMEND == "false"
//# 				recommendScreen = null;
				//#endif
				//#ifdef WEB_EMULATOR
//# 					nextScreen = createBasicMenu( screen, new int[] {
//#  							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_PRODUCTS_TITLE,
//# 							TEXT_RECOMMEND_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS } );
				//#else
					indexSoftRight = TEXT_EXIT;
					//#if NO_RECOMMEND == "true"
//# 						nextScreen = createBasicMenu( screen, new int[] {
//# 								TEXT_NEW_GAME,
//# 								TEXT_OPTIONS,
//# 								TEXT_HIGH_SCORES,
//# 								TEXT_HELP,
//# 								TEXT_CREDITS,
//# 								TEXT_EXIT, } );
					//#else
						//#if BLACKBERRY_API == "true"
//# 							nextScreen = createBasicMenu( screen, ( showRecommendScreen && SmsSender.isSupported() ) ? new int[] {
				//#else


				nextScreen = createBasicMenu(screen, SmsSender.isSupported() ? new int[]{
							//#endif

					//#if NANO_RANKING == "true"

							TEXT_NEW_GAME,
							TEXT_OPTIONS,
							TEXT_NANO_ONLINE,
							TEXT_RECOMMEND_TITLE,
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,}
						: new int[]{
							TEXT_NEW_GAME,
							TEXT_OPTIONS,
							TEXT_NANO_ONLINE,
							TEXT_PRODUCTS_TITLE,
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,});
				//#else
//# 				            TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_RECOMMEND_TITLE,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS,
//# 							TEXT_EXIT,}
//# 						: new int[]{
//# 							TEXT_NEW_GAME,
//# 							TEXT_OPTIONS,
//# 							TEXT_HIGH_SCORES,
//# 							TEXT_HELP,
//# 							TEXT_CREDITS,
//# 							TEXT_EXIT,});
//#
//#
				//#endif
				//#endif
				//#endif

				if ( !menuWasShown ) {
					menuWasShown = true;
					useTransition = true;
				}

				indexSoftLeft = TEXT_OK;
				}

			break;

			case SCREEN_NEXT_LEVEL:
				playScreen.prepareNextLevel();
				if ( playScreen.getLevel() <= 1 )
					useTransition = true;
			case SCREEN_RETRY:
			case SCREEN_CONTINUE_GAME:
				if ( screen == SCREEN_RETRY )
					retryLevel = true;

				nextScreen = playScreen;
				bkgType = BACKGROUND_TYPE_NONE;

				indexSoftRight = TEXT_PAUSE;
//				visibleTime = SOFT_KEY_VISIBLE_TIME; TODO teste

				if ( screen == SCREEN_CONTINUE_GAME ) {
					playScreen.sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
					playScreen.setState( PlayScreen.STATE_UNPAUSING );
				}
			break;

			case SCREEN_OPTIONS:
				//#if JAR == "min"
//# 					BasicOptionsScreen optionsScreen = null;
//#
//# 					if ( MediaPlayer.isVibrationSupported() ) {
//# 						optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
//# 							TEXT_TURN_SOUND_OFF,
//# 							TEXT_TURN_VIBRATION_OFF,
//# 							TEXT_BACK,
//# 						}, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
//# 					} else {
//# 						optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
//# 							TEXT_TURN_SOUND_OFF,
//# 							TEXT_BACK,
//# 						}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );
//# 					}
//# 					optionsScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
					BasicOptionsScreen optionsScreen = null;
					if ( isLowMemory() ) {
						if ( MediaPlayer.isVibrationSupported() ) {
							new BasicOptionsScreen( midlet, 0, null, null, 0, 0, 0, 0, 0 );
							optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), 
								new int[] {
									TEXT_TURN_SOUND_OFF,
									TEXT_TURN_VIBRATION_OFF,
									TEXT_BACK,
								}, 
								ENTRY_OPTIONS_MENU_VIB_BACK,
								ENTRY_OPTIONS_MENU_TOGGLE_SOUND,
								TEXT_TURN_SOUND_ON, 
								ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION,
								TEXT_TURN_VIBRATION_ON )
							{

					public final void setSize(int width, int height) {
						super.setSize(width, height);

						if (getHeight() < ScreenManager.SCREEN_HEIGHT) {
							setPosition((ScreenManager.SCREEN_WIDTH - getWidth()) >> 1, (ScreenManager.SCREEN_HEIGHT - getHeight()) >> 1);
						}
					}
				};
						} else {
							optionsScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
								TEXT_TURN_SOUND_OFF,
								TEXT_BACK,
							}, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 ){

					public final void setSize(int width, int height) {
						super.setSize(width, height);

						if (getHeight() < ScreenManager.SCREEN_HEIGHT) {
							setPosition((ScreenManager.SCREEN_WIDTH - getWidth()) >> 1, (ScreenManager.SCREEN_HEIGHT - getHeight()) >> 1);
						}
					}
				};
						}
						optionsScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
					} else {
						if ( MediaPlayer.isVibrationSupported() ) {
							optionsScreen = OptionsScreen.createInstance( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
										TEXT_TURN_SOUND_OFF,
										TEXT_TURN_VIBRATION_OFF,
										TEXT_BACK, }, ENTRY_OPTIONS_MENU_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION );
						} else {
							optionsScreen = OptionsScreen.createInstance( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
										TEXT_TURN_SOUND_OFF,
										TEXT_BACK, }, ENTRY_OPTIONS_MENU_NO_VIB_BACK, ENTRY_OPTIONS_MENU_TOGGLE_SOUND, -1 );
						}
					}
				//#endif
 				nextScreen = optionsScreen;


				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			//#if NANO_RANKING == "true"

			case SCREEN_NANO_RANKING_MENU:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_NANO_ONLINE:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						MediaPlayer.free();
						setSpecialKeyMapping(false);

						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_MAIN_MENU);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_MENU);
					}
				});

				break;

			case SCREEN_NANO_RANKING_PROFILES:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_PROFILES_SCREEN:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						setSpecialKeyMapping(false);
						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_PROFILES);
					}
				});
				break;

			case SCREEN_CHOOSE_PROFILE:
				setSpecialKeyMapping(false);
				final Customer c = NanoOnline.getCurrentCustomer();
				// a fonte só possui caracteres maiúsculos
				final String name = (getText(TEXT_CURRENT_PROFILE) + (c.getId() == Customer.ID_NONE ? getText(TEXT_NO_PROFILE) : c.getNickname())).toUpperCase();

				final Label title = new RichLabel(FONT_MENU_OVER, name, ScreenManager.SCREEN_WIDTH * 9 / 10);

				if (!isLowMemory()) {
					nextScreen = new BasicConfirmScreen(midlet, screen, getFont(FONT_MENU), null,
							title, new MenuLabel(TEXT_CONFIRM), new MenuLabel(TEXT_CHOOSE_ANOTHER), new MenuLabel(TEXT_BACK), false);
					((BasicConfirmScreen) nextScreen).setEntriesAlignment(BasicConfirmScreen.ALIGNMENT_VERTICAL);
					((BasicConfirmScreen) nextScreen).setCurrentIndex(BasicConfirmScreen.INDEX_YES);
					((BasicConfirmScreen) nextScreen).setSpacing(5 << 1, 5);
				} else {
					final Label labelConfirm = new Label(FONT_MENU_OVER, TEXT_CONFIRM);
					final Label labelChoose = new Label(FONT_MENU_OVER, TEXT_CHOOSE_ANOTHER);
					final Label labelBack = new Label(FONT_MENU, TEXT_BACK);

					nextScreen = new BasicConfirmScreen(midlet, screen, getFont(FONT_MENU_OVER), getCursor(),
							title, labelConfirm, labelChoose, labelBack, false);
					((BasicConfirmScreen) nextScreen).setEntriesAlignment(BasicConfirmScreen.ALIGNMENT_VERTICAL);
					((BasicConfirmScreen) nextScreen).setCurrentIndex(BasicConfirmScreen.INDEX_YES);
					((BasicConfirmScreen) nextScreen).setSpacing(5 << 1, 5);
				}

				break;

			case SCREEN_LOADING_HIGH_SCORES:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						MediaPlayer.free();

						setSpecialKeyMapping(false);
						playScreen = null;
						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_MAIN_MENU, NanoOnline.SCREEN_NEW_RECORD);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_MENU);
					}
				});
				break;

			//#else
//#  					case SCREEN_HIGH_SCORES:
//# 		             nextScreen = HighScoresScreen.createInstance( getFont( FONT_MENU_OVER ), DATABASE_NAME, DATABASE_SLOT_HIGH_SCORES );
//# 	                 indexSoftRight = TEXT_BACK;
//#                      break;
			//#endif

			//#if LOG == "true"
//# 					case SCREEN_LOG:
//# 						Logger.log( log );
//# 						nextScreen = Logger.getLogScreen( SCREEN_MAIN_MENU, GameMIDlet.getFont( FONT_TEXT_WHITE ) );
//# 						(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
//# 						(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
//# 						break;
			//#endif

			case SCREEN_HELP_MENU:
				nextScreen = createBasicMenu(screen, new int[]{
							TEXT_OBJECTIVES,
							TEXT_CONTROLS,
							TEXT_BACK,});

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_PRODUCTS:
				//#if JAR == "min"
//# 					nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_TEXT ), TEXT_PRODUCTS_TEXT, false );
				//#else
					final Drawable[] products = new Drawable[ TOTAL_PRODUCTS ];
					for ( byte i = 0; i < products.length; ++i ) {
						products[ i ] = new DrawableImage( PATH_PRODUCTS + i + ".png" );
					}
					nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_TEXT ), TEXT_PRODUCTS_TEXT, false, products );
				//#endif

				( ( BasicTextScreen ) nextScreen ).setScrollFull( getScrollFull() );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( getScrollPage() );

				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
				Drawable[] chars = null;
				if ( screen == SCREEN_HELP_OBJECTIVES ) {
					final UpdatableGroup fruitGroup = new UpdatableGroup( Fruit.TOTAL_TYPES );
					for ( byte i = 0; i < Fruit.TOTAL_TYPES; ++i ) {
						final Fruit f = new Fruit( i );
						f.setState( Fruit.STATE_BOMB );
						f.setPosition( i * f.getWidth() >> 1, ( i & 1 ) == 0 ? 0 : f.getHeight() );
						fruitGroup.insertDrawable( f );
					}

					chars = new Drawable[ 2 ];

					final TimeBar bar = new TimeBar( true );
					bar.setSize( ScreenManager.SCREEN_HALF_WIDTH, bar.getHeight() );
					final DrawableImage clock = new DrawableImage( PATH_IMAGES + "clock.png" );
					bar.setPosition( clock.getWidth() + 1, CLOCK_Y_OFFSET );

					final UpdatableGroup group2 = new UpdatableGroup( 2 );
					group2.insertDrawable( clock );
					group2.insertDrawable( bar );
					group2.setSize( bar.getPosX() + bar.getWidth(), clock.getHeight() );
					final UpdatableGroup hintGroup = new UpdatableGroup( Fruit.TOTAL_TYPES << 1 );
					for ( byte i = 0; i < Fruit.TOTAL_TYPES; ++i ) {
						final Fruit f = new Fruit( i );
						f.defineReferencePixel( Drawable.ANCHOR_HCENTER | Drawable.ANCHOR_VCENTER );

						final Sprite hint = new Sprite( PlayScreen.getHintSprite() );
						hint.defineReferencePixel( Drawable.ANCHOR_HCENTER | Drawable.ANCHOR_VCENTER );
						hint.setSequence( HINT_SEQUENCE_ANIMATING );

						hint.setPosition( i * f.getWidth() >> 1, ( i & 1 ) == 1 ? 0 : f.getHeight() );
						f.setRefPixelPosition( hint.getRefPixelPosition() );
						
						hintGroup.insertDrawable( f );
						hintGroup.insertDrawable( hint );
					}
					hintGroup.setSize( hintGroup.getDrawable( 1 ).getWidth() * ( ( Fruit.TOTAL_TYPES >> 1 ) + ( Fruit.TOTAL_TYPES & 1 ) ), hintGroup.getDrawable( 1 ).getHeight() << 1 );
					fruitGroup.setSize( hintGroup.getSize() );

					chars = new Drawable[] { fruitGroup, group2, hintGroup };
				} else {

					chars = null;
				}
				nextScreen = new BasicTextScreen( SCREEN_HELP_MENU, getFont( FONT_TEXT ), getText( TEXT_HELP_OBJECTIVES + ( screen - SCREEN_HELP_OBJECTIVES ) ) + getText( TEXT_VERSION ) + getMIDletVersion() + "\n\n", false, chars );
				( ( BasicTextScreen ) nextScreen ).setScrollFull( getScrollFull() );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( getScrollPage() );

				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_MENU_OVER ), getText( TEXT_CREDITS_TEXT ) + getText( TEXT_VERSION ).toUpperCase() + getMIDletVersion() + "\n\n", true );

				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_BEAUTY_TIPS:
				final byte TIP_INDEX = ( byte ) NanoMath.randInt( TOTAL_BEAUTY_TIPS );
				final char[] buffer = new char[ 512 ];
				final Point counter = new Point();
				String tips = " ";
				if (language == NanoOnline.LANGUAGE_en_US) {
					tips = "tipsEn.bin";
				} else {
					tips = "tipsPt.bin";
				}
				
				openJarFile( PATH_IMAGES + tips, new Serializable() {

					public final void write( DataOutputStream output ) throws Exception {
					}


					public final void read( DataInputStream input ) throws Exception {
						short currentTip = 0;

						while ( currentTip < TIP_INDEX ) {
							if ( input.readUnsignedByte() == '\n' )
								++currentTip;
						} // fim while ( currentString < TEXT_TOTAL )

						// nesse ponto, já descartou as linhas anteriores à dica atual
						char c = ( char ) input.readByte();
						do {
							buffer[ counter.x++ ] = c;
							// se o texto estiver em UTF8, o converte
							byte b = input.readByte();
							if (b < 0) {
								b = (byte) (b - 0x100);
							}

							c = (char) b;

							if (c >= 0xC0) {
								c = (char) (((c << 8) | input.readUnsignedByte()) - 0xc2c0);
							} else if (c >= 0x80) {
								c = (char) (c - 0xc200);
							}

						} while ( c != '\n' && c != '\r' );
					}
				} );
				final String text = getText( TEXT_BEAUTY_TIP ) + ( TIP_INDEX + 1 ) + "\n\n<ALN_L>" + new String( buffer, 0, counter.x );
				nextScreen = new BorderedTextScreen( text, SCREEN_NEXT_LEVEL );
				indexSoftLeft = TEXT_CONTINUE;
			break;

			case SCREEN_PAUSE:
				//#if JAR == "min"
//# 					BasicOptionsScreen pauseScreen = null;
//# 					if ( MediaPlayer.isVibrationSupported() ) {
//# 						pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
//# 							TEXT_CONTINUE,
//# 							TEXT_TURN_SOUND_OFF,
//# 							TEXT_TURN_VIBRATION_OFF,
//# 							TEXT_BACK_MENU,
//# 							TEXT_EXIT_GAME,
//# 						}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
//# 					} else {
//# 						pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
//# 							TEXT_CONTINUE,
//# 							TEXT_TURN_SOUND_OFF,
//# 							TEXT_BACK_MENU,
//# 							TEXT_EXIT_GAME,
//# 						}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );
//# 					}
//# 					pauseScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
					BasicOptionsScreen pauseScreen = null;
					if ( isLowMemory() ) {
						if ( MediaPlayer.isVibrationSupported() ) {
							pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
								TEXT_CONTINUE,
								TEXT_TURN_SOUND_OFF,
								TEXT_TURN_VIBRATION_OFF,
								TEXT_BACK_MENU,

								//#ifndef WEB_EMULATOR
									TEXT_EXIT_GAME,
								//#endif

							}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
						} else {
							pauseScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
								TEXT_CONTINUE,
								TEXT_TURN_SOUND_OFF,
								TEXT_BACK_MENU,

								//#ifndef WEB_EMULATOR
									TEXT_EXIT_GAME,
								//#endif

							}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1 );
						}
						pauseScreen.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
						nextScreen = pauseScreen;
					} else {
						if ( MediaPlayer.isVibrationSupported() ) {
							pauseScreen = OptionsScreen.createInstance( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
										TEXT_CONTINUE,
										TEXT_TURN_SOUND_OFF,
										TEXT_TURN_VIBRATION_OFF,
										TEXT_BACK_MENU,
								//#ifndef WEB_EMULATOR
										TEXT_EXIT_GAME,
								//#endif
							}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION );
						} else {
							pauseScreen = OptionsScreen.createInstance( midlet, screen, getFont( FONT_MENU_OVER ), new int[] {
										TEXT_CONTINUE,
										TEXT_TURN_SOUND_OFF,
										TEXT_BACK_MENU,
									//#ifndef WEB_EMULATOR
										TEXT_EXIT_GAME,
									//#endif
							}, ENTRY_PAUSE_MENU_CONTINUE, ENTRY_PAUSE_MENU_TOGGLE_SOUND, -1 );
						}
					}
				//#endif

				nextScreen = pauseScreen;
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_CONTINUE;
			break;

			case SCREEN_CONFIRM_MENU:
				//#if JAR == "min"
//# 					nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO, true );
//# 					( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
					 if ( isLowMemory() ) {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, TEXT_CONFIRM_BACK_MENU, TEXT_YES, TEXT_NO, true );
						( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
					 } else {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, getConfirmTitle( TEXT_CONFIRM_BACK_MENU ), new MenuLabel( TEXT_YES ), new MenuLabel( TEXT_NO ), null, true );
					 }
				//#endif
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				( ( BasicConfirmScreen ) nextScreen ).setSpacing( TITLE_SPACING, ENTRIES_SPACING );
				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_CONFIRM_EXIT:
				//#if JAR == "min"
//# 					nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, TEXT_CONFIRM_EXIT, TEXT_YES, TEXT_NO, true );
//# 					( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
				//#else
					if ( isLowMemory() ) {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, TEXT_CONFIRM_EXIT, TEXT_YES, TEXT_NO, true );
						( ( BasicConfirmScreen ) nextScreen ).setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
					} else {
						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU_OVER ), null, getConfirmTitle( TEXT_CONFIRM_EXIT ), new MenuLabel( TEXT_YES ), new MenuLabel( TEXT_NO ), null, true );
					}
				//#endif
				( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
				( ( BasicConfirmScreen ) nextScreen ).setSpacing( TITLE_SPACING, ENTRIES_SPACING );
				indexSoftLeft = TEXT_OK;
			break;

			//#if DEMO == "true"
//# 			
//# 			// versão demo
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				nextScreen = new BasicConfirmScreen( midlet, index, FONT_DEFAULT, midlet.cursor, TEXT_BUY_FULL_GAME_TEXT, TEXT_YES, TEXT_NO );
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
//# 			case SCREEN_PLAYS_REMAINING:
//# 				final String text = playsRemaining > 1 ? 
//# 						getText( TEXT_2_OR_MORE_PLAYS_REMAINING_1 ) + playsRemaining + getText( TEXT_2_OR_MORE_PLAYS_REMAINING_2 ) :
//# 						getText( TEXT_1_PLAY_REMAINING );
//# 				nextScreen = new BasicTextScreen( midlet, index, FONT_DEFAULT, text, true );
//# 						
//# 				indexSoftLeft = TEXT_OK;
//# 			break;
//# 			
			//#endif

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								// aloca os sons
								final String[] soundList = new String[ SOUND_TOTAL ];
								for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
									soundList[i] = PATH_SOUNDS + i + ".mid";
									log( 11 );
								}

								MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								//#if BLACKBERRY_API == "true"
//# 									MediaPlayer.setVolume( 60 );
								//#endif

								log( 12 );

								SoftLabel.load();

								log( 13 );
								
								//#if JAR == "min"
//# 									Fruit.load();
//# 									Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
								//#else

									if ( isLowMemory() ) {
										Fruit.load();
										Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
									} else {
										MenuLabel.load();
										log( 19 );
										Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
										transition = new WaterTransition();
										log( 20 );
										Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
									}

									//#if GRAPHISM == "true"
										bkg = new Graphism();
										Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
									//#endif

									softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
									softkeyMid = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_MID );
									softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );

									log( 21 );

									final ScreenManager manager = ScreenManager.getInstance();

									log( 22 );
									manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
									manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );
								//#endif

								loadScreen.setActive( false );

								setScreen( SCREEN_CHOOSE_LANGUAGE );
							}


						}, getFont( FONT_MENU_OVER ), TEXT_LOADING_GENERIC );

				useTransition = true;
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						Fruit.load();

						log( 23 );

						DropEmitter.load();
						//#if JAR != "min"
							log( 24 );
							ScrollBar.loadImages();
							log( 25 );
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						//#endif
						
						PlayScreen.load();

						log( 26 );

						loadScreen.setActive( false );

						setScreen( SCREEN_MAIN_MENU );
					}

				}, getFont( FONT_MENU_OVER ), TEXT_LOADING );

				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_GAME:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						setSpecialKeyMapping( true );
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						playScreen = new PlayScreen();

						loadScreen.setActive( false );

						setScreen( SCREEN_NEXT_LEVEL );
					}

				}, getFont( FONT_MENU_OVER ), TEXT_LOADING );

				MediaPlayer.free();
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen( loader, getFont( FONT_MENU_OVER ), TEXT_LOADING );
				loader = null;

				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			//#if NO_RECOMMEND == "false"
//# 
//# 			case SCREEN_RECOMMEND_SENT:
//# 				nextScreen = new BorderedTextScreen( getText( TEXT_RECOMMEND_SENT ) + getRecommendURL() + "\n\n", SCREEN_MAIN_MENU );
//# 				indexSoftRight = TEXT_BACK;
//# 			break;
//# 
//# 			case SCREEN_RECOMMEND:
//# 				setSpecialKeyMapping( false );
//# 				nextScreen = new Form( new ScreenRecommend( SCREEN_MAIN_MENU ) );
				//#if SCREEN_SIZE == "BIG"
//# 					( ( Form ) nextScreen ).setTouchKeyPad( new TouchKeyPad( getFont( FONT_TEXT ), new DrawableImage( PATH_IMAGES + "clear.png" ), new DrawableImage( "/online/shift.png" ), getBorder(), 0xecddab, 0xccbd8b, 0xfcedbb ) );
				//#endif
//# 				indexSoftRight = TEXT_BACK;
//# 				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
//# 			break;
//# 
			//#endif

			//#if DEBUG == "true"
			case SCREEN_ERROR_LOG:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_TEXT ), texts[ TEXT_LOG_TEXT ], false );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				indexSoftRight = TEXT_BACK;
			break;
			//#endif
		} // fim switch ( screen )


		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft, visibleTime );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, visibleTime );

		if ( indexSoftMid != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, indexSoftMid, visibleTime );

		//#if JAR == "low" || JAR == "min"
//#  			if ( retryLevel )
//#  				playScreen.retryLevel();
//# 			ScreenManager.getInstance().setCurrentScreen( nextScreen );
//# 			setBackground( bkgType );
		//#else
		if ( !useTransition || transition == null ) {
			if ( retryLevel )
				playScreen.retryLevel();

			ScreenManager.getInstance().setCurrentScreen( nextScreen );
			setBackground( bkgType );
		} else {
			transition.setNextScreen( nextScreen, bkgType, retryLevel );
		}
		//#endif

		return screen;
	} // fim do método changeScreen( int )


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			//#if JAR != "min" && GRAPHISM == "true"

			case BACKGROUND_TYPE_SPECIFIC:
				bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				midlet.manager.setBackground( bkg, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			//#endif

			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( BACKGROUND_COLOR );
			break;
		}
	} // fim do método setBackground( byte )


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final br.com.nanogames.components.userInterface.form.borders.Border getBorder() throws Exception {
		return new Border();
	}


	public static final NanoOnlineScrollBar getScrollBar() throws Exception {
		return new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT,
										COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	public static final Drawable getCursor() throws Exception {
		final Fruit cursor = new Fruit( ( byte ) NanoMath.randInt( Fruit.TOTAL_TYPES ) );
		cursor.defineReferencePixel( cursor.getWidth(), cursor.getHeight() >> 1 );

		return cursor;
	}


	public static final Drawable getScrollFull() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollF = new Pattern( COLOR_SCROLL_BACK );
//# 			scrollF.setSize( 8, 0 );
//# 			return scrollF;
		//#else
			return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
		//#endif
	}


	public static final Drawable getScrollPage() throws Exception {
		//#if JAR == "min"
//# 			final Pattern scrollP = new Pattern( COLOR_SCROLL_FORE );
//# 			scrollP.setSize( 8, 0 );
//# 			return scrollP;
		//#else
			return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
		//#endif
	}


	private static final Drawable createBasicMenu( int index, int[] entries  ) throws Exception {
		//#if JAR == "min"
//# 		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, getFont( FONT_MENU_OVER ), entries );
//# 		menu.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT );
//#
//# 		return menu;
		//#else
			if (isLowMemory()) {
				final BasicMenu menu = new BasicMenu((GameMIDlet) instance, index, getFont(FONT_MENU_OVER), entries) {

					public final void setSize(int width, int height) {
						super.setSize(width, height);

						if (getHeight() < ScreenManager.SCREEN_HEIGHT) {
							setPosition((ScreenManager.SCREEN_WIDTH - getWidth()) >> 1, (ScreenManager.SCREEN_HEIGHT - getHeight()) >> 1);
						}
					}
				};

				menu.setCursor(getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_LEFT);

				return menu;
			} else {
				final Drawable[] menuEntries = new Drawable[ entries.length ];
				for ( byte i = 0; i < menuEntries.length; ++i ) {
					menuEntries[i] = new MenuLabel( entries[i] );
				}

				final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, menuEntries, ENTRIES_SPACING, 0, entries.length - 1, null ) {

					public final void setSize(int width, int height) {
						super.setSize(width, height);

						if ( getHeight() < ScreenManager.SCREEN_HEIGHT )
							setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - getHeight() ) >> 1 );
					}

				};

				return menu;
			}
		//#endif
	}

	public static final boolean isLowMemory() {
		return lowMemory;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif
					
				//#if NO_RECOMMEND == "true"
//# 					if ( index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//# 						++index;
//# 					}
				//#endif

				//#if JAR != "min"
					//#ifndef WEB_EMULATOR
						if ( !SmsSender.isSupported() && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
							++index;
						}
					//#endif
				//#endif

				switch ( index ) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						//#if NANO_RANKING == "true"
						setScreen(SCREEN_CHOOSE_PROFILE);
						//#else
//# 							setScreen(SCREEN_LOADING_GAME);
						//#endif

						break;
					

					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen(SCREEN_OPTIONS);
						break;

					case MAIN_MENU_NANO_ONLINE:
						//#if NANO_RANKING == "true"
							setScreen(SCREEN_LOADING_NANO_ONLINE);
						//#else
//# 						setScreen(SCREEN_HIGH_SCORES);
						//#endif
						break;

					/*case ENTRY_MAIN_MENU_HIGH_SCORES:
					setScreen( SCREEN_HIGH_SCORES );
					break;*/

					//#if DEMO == "true"
//# 					case ENTRY_MAIN_MENU_BUY_FULL_GAME:
//# 						setScreen( SCREEN_BUY_GAME_RETURN );
//# 					break;
					//#endif

					case ENTRY_MAIN_MENU_PRODUCTS:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_PRODUCTS);
						}
						if (language == NanoOnline.LANGUAGE_en_US) {
							//#if NO_RECOMMEND == "true"
//# 								setScreen( SCREEN_HELP_MENU );
							//#else
								setScreen(SCREEN_RECOMMEND);
							//#endif
						}
						break;

					case ENTRY_MAIN_MENU_RECOMMEND:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_RECOMMEND);
						}
						if (language == NanoOnline.LANGUAGE_en_US) {
							setScreen(SCREEN_HELP_MENU);
						}
						break;

					case ENTRY_MAIN_MENU_HELP:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_HELP_MENU);
						}
						if (language == NanoOnline.LANGUAGE_en_US) {
							setScreen(SCREEN_CREDITS);
						}
						break;

					case ENTRY_MAIN_MENU_CREDITS:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_CREDITS);
						}
						if (language == NanoOnline.LANGUAGE_en_US) {
							MediaPlayer.saveOptions();
							exit();
						}
						break;

					//#if DEBUG == "true"
					case ENTRY_MAIN_MENU_ERROR_LOG:
						setScreen( SCREEN_ERROR_LOG );
					break;
					//#endif

					case ENTRY_MAIN_MENU_EXIT :
						MediaPlayer.saveOptions();
						exit();
						break;
				} // fim switch ( index )
				break; // fim case SCREEN_MAIN_MENU

			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
						break;

					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
						break;

					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;

			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							setScreen( SCREEN_CONTINUE_GAME );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
							break;

						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE

			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS

			//#if DEMO == "true"
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 							e.printStackTrace();
			//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif

				//#if NANO_RANKING == "true"
			case SCREEN_CHOOSE_PROFILE:
				switch (index) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.free();
						if (nanoOnlineForm != null) {
							NanoOnline.unload();
							nanoOnlineForm = null;
						}
						setScreen(SCREEN_LOADING_GAME);
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen(SCREEN_LOADING_PROFILES_SCREEN);
						break;

					case BasicConfirmScreen.INDEX_CANCEL:
						if (nanoOnlineForm != null) {
							NanoOnline.unload();
							nanoOnlineForm = null;
						}
						setScreen(SCREEN_MAIN_MENU);

						break;
				}
				break;
			//#endif

			case SCREEN_CONFIRM_MENU:
				switch (index) {
					case BasicConfirmScreen.INDEX_YES:
						//#if NANO_RANKING == "false"
//# 						HighScoresScreen.setScore( playScreen.getScore() );
//# 						playScreen = null;
//# 						MediaPlayer.saveOptions();
//# 						setScreen(SCREEN_MAIN_MENU);
						//#else
						gameOver(playScreen.getScore());
						playScreen = null;
						MediaPlayer.saveOptions();

						//#endif
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						exit();
						break;

					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_CHOOSE_SOUND:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );

				setScreen( SCREEN_SPLASH_NANO );
				break;

			case SCREEN_CHOOSE_LANGUAGE:
				try {
					switch (index) {

						case OPTION_ENGLISH:
							language = NanoOnline.LANGUAGE_en_US;
							loadTexts(TEXT_TOTAL, PATH_IMAGES + "en.dat");
							setScreen(SCREEN_CHOOSE_SOUND);
							break;
						case OPTION_PORTUGUESE:
							language = NanoOnline.LANGUAGE_pt_BR;
							loadTexts(TEXT_TOTAL, PATH_IMAGES + "pt.dat");
							setScreen(SCREEN_CHOOSE_SOUND);
							break;
					}
					AppMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
				} catch (Exception e) {
					//#if DEBUG == "true"
							e.printStackTrace();
					//#endif

					exit();
				}

				break;
		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		//#if JAR != "min"
			if ( !isLowMemory() ) {
				for ( byte i = 0; i < menu.getTotalSlots(); ++i ) {
					if ( menu.getDrawable( i ) instanceof MenuLabel ) {
						( ( MenuLabel ) menu.getDrawable( i ) ).setActive( i == index );
					}
				}
			}
		//#endif
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		//#if JAR != "min"
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
			softKeyRightText = textIndex;
		//#endif
		
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new SoftLabel( softKey, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do método setSoftKeyLabel( byte, int )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_MID:
				if ( softkeyMid != null ) {
					softkeyMid.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	//#endif
	} // fim do método setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}

	
	public static final ImageFont getFont( int index ) {
		//#if JAR == "min"
//# 			switch ( index ) {
//# 				case FONT_POINTS:
//# 				case FONT_BIG:
//# 					return FONTS[ index ];
//#
//# 				default:
//# 					return FONTS[ 0 ];
//# 			}
		//#else
			return FONTS[ index ];
		//#endif
	}


	private static final RichLabel getConfirmTitle( int textIndex ) throws Exception {
		return new RichLabel( FONT_MENU_OVER, textIndex, ScreenManager.SCREEN_WIDTH * 9 / 10 );
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	protected void changeLanguage( byte language ) {
	}

	public final void write(DataOutputStream output) throws Exception {

		output.writeByte(language);
		output.writeBoolean(MediaPlayer.isMuted());
	}

	public final void read(DataInputStream input) throws Exception {
		language = input.readByte();
		input.readBoolean();
	}


	private static final class BorderedTextScreen extends UpdatableGroup implements KeyListener, PointerListener {

		private final ScrollRichLabel textScreen;

		private final int nextScreen;

		public BorderedTextScreen( String text, int nextScreen ) throws Exception {
			super( 2 );

			this.nextScreen = nextScreen;

			//#if SCREEN_SIZE == "SMALL"
//# 				if ( ScreenManager.SCREEN_HEIGHT < HEIGHT_MIN )
//# 					setSize( ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
//# 				else
//# 					setSize( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#else
				setSize( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#endif
			defineReferencePixel( Drawable.ANCHOR_HCENTER | Drawable.ANCHOR_VCENTER );
			setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

			textScreen = new ScrollRichLabel( new RichLabel( getFont( FONT_TEXT ), text, getWidth() ) );
			textScreen.setSize( getWidth() - ( BORDER_THICKNESS * 3 ), getHeight() - ( BORDER_THICKNESS << 1 ) );
			textScreen.setPosition( BORDER_THICKNESS << 1, BORDER_THICKNESS );
			textScreen.setScrollFull( getScrollFull() );
			textScreen.setScrollPage( getScrollPage() );

			final Border border = new Border();
			border.setSize( getSize() );
			insertDrawable( border );
			insertDrawable( textScreen );
		}


		public final void keyPressed( int key ) {
			switch ( key ) {
				case ScreenManager.FIRE:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					GameMIDlet.setScreen( nextScreen );
				break;

				default:
					textScreen.keyPressed( key );
			}
		}


		public final void keyReleased( int key ) {
			textScreen.keyReleased( key );
		}


		public final void onPointerDragged( int x, int y ) {
			textScreen.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			textScreen.onPointerPressed( x, y );
		}


		public final void onPointerReleased( int x, int y ) {
			textScreen.onPointerReleased( x, y );
		}

	}



	//#if GRAPHISM == "true"

	private static final class Graphism extends DrawableGroup {

		private static final byte IMG_BOTTOM_RIGHT	= 0;
		private static final byte IMG_BOTTOM_LEFT	= 1;
		private static final byte IMG_MIDDLE		= 2;
		private static final byte IMG_TOP_LEFT		= 3;
		private static final byte IMG_TOP_RIGHT		= 4;

		private static final byte TOTAL_IMAGES = 5;

		private final DrawableImage[] images = new DrawableImage[ TOTAL_IMAGES ];

		private final Pattern solid;

		public Graphism() throws Exception {
			super( TOTAL_IMAGES + 1 );

			solid = new Pattern( BACKGROUND_COLOR );
			insertDrawable( solid );

			for ( byte i = 0; i < TOTAL_IMAGES; ++i ) {
				//#if SCREEN_SIZE == "SMALL"
//# 					if ( ScreenManager.SCREEN_HEIGHT < 160 ) {
//# 						switch ( i ) {
//# 							case IMG_MIDDLE:
//# 							case IMG_TOP_RIGHT:
//# 							case IMG_BOTTOM_LEFT:
//# 							continue;
//# 						}
//# 					}
				//#endif
				images[ i ] = new DrawableImage( PATH_BKG + i + ".png" );
				insertDrawable( images[ i ] );
			}
		}


		public final void setSize( int width, int height ) {
			super.setSize( width, height );

			solid.setSize( size );

			images[ IMG_BOTTOM_RIGHT ].setPosition( width - images[ IMG_BOTTOM_RIGHT ].getWidth(), height - images[ IMG_BOTTOM_RIGHT ].getHeight() );

			//#if SCREEN_SIZE == "SMALL"
//# 				if ( images[ IMG_BOTTOM_LEFT ] == null || images[ IMG_BOTTOM_LEFT ] == null || images[ IMG_BOTTOM_LEFT ] == null )
//# 					return;
			//#endif
			images[ IMG_BOTTOM_LEFT ].setPosition( 0, height - images[ IMG_BOTTOM_LEFT ].getHeight() );
			images[ IMG_MIDDLE ].setPosition( ( width - images[ IMG_MIDDLE ].getWidth() ) >> 1, ( height - images[ IMG_MIDDLE ].getHeight() ) >> 1 );
			images[ IMG_TOP_RIGHT ].setPosition( width - images[ IMG_BOTTOM_RIGHT ].getWidth(), 0 );
		}

	} // fim da classe interna Graphism

	//#endif


	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	private static interface LoadListener {

		public void load(final LoadScreen loadScreen) throws Exception;
	}

	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualização do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;
		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private final byte previousScreen;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.prof.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );

			previousScreen = currentScreen;

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );

			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}

		private LoadScreen( LoadListener listener ) throws Exception {

			super( AppMIDlet.getFont( FONT_MENU_OVER ), AppMIDlet.getText( TEXT_LOADING ) );
            previousScreen = currentScreen;
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo são carregados aqui para evitar sobrecarga do método loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// só inicia a thread quando a tela atual for a ativa, para evitar possíveis atrasos em transições e
					// garantir que novos recursos só serão carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
									texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();

									setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									//#else
//# 									// volta à tela anterior
//# 									setScreen( previousScreen );
								//#endif
								}
							} // fim do método run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execução
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do método update( int )


		public final void paint( Graphics g ) {
			super.paint( g );

			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	// </editor-fold>
}
