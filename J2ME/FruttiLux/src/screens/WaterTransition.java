/**
 * WaterTransition.java
 * �2008 Nano Games.
 *
 * Created on May 6, 2008 5:31:46 PM.
 */

//#if JAR != "min"

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author Peter
 */
public final class WaterTransition extends UpdatableGroup implements Constants, KeyListener, ScreenListener
//#if SCREEN_SIZE != "SMALL"
	, PointerListener
//#endif
{

	private static final byte BOTTOM_SEQUENCE_DROP		= 0;
	private static final byte BOTTOM_SEQUENCE_LIQUID	= 1;

	/** Acelera��o da "gravidade". */
	private static final int FP_GRAVITY_ACC = 26789012;

	private static final int FP_HALF_GRAVITY_ACC = FP_GRAVITY_ACC >> 1;

	private int fp_dropSpeedY;

	private int fp_dropPosY;

	private int fp_accTime;

	/** quantidade total de drawables no grupo */
	private static final byte TOTAL_ITEMS = 30;

	private static final short SOURCE_MOVE_TIME = 480;

	private static final byte STATE_NONE		= 0;
	private static final byte STATE_DROP_DOWN	= 1;
	private static final byte STATE_ALL_UP		= 2;
	private static final byte STATE_ALL_DOWN	= 3;

	/** estado atual de transi��o */
	private byte transitionState;

	/** Tela atual (que ser� substitu�da). */
	private Drawable previousScreen;

	/** Pr�xima tela a ser definida como ativa pelo ScreenManager */
	private Drawable nextScreen;

	/** Viewport da tela. */
	private final Rectangle screenViewport = new Rectangle();

	/** Grupo que cont�m o pattern de baixo, preenchimento do meio e o pattern de cima. */
	private final UpdatableGroup group;

	private final UpdatableGroup screenGroup = new UpdatableGroup( 1 );

	private final DrawableImage[] sources;

	private final Pattern[] middles;

	private final Sprite[] drops;

	private final MUV sourceMUV = new MUV();

	private final MUV verticalSpeed = new MUV();

	private final byte BUBBLES_HEIGHT;

	private final Mutex mutex = new Mutex();

	private byte bkgType;

	private final Pattern solid;

	private final BasicAnimatedPattern top;

	private boolean retryLevel;


	public WaterTransition() throws Exception {
		super( TOTAL_ITEMS );

		screenGroup.setViewport( screenViewport );
		insertDrawable( screenGroup );

		// insere o grupo da �gua
		final DrawableImage imgSurface = new DrawableImage( PATH_IMAGES + "surface.png" );
		group = new UpdatableGroup( 4 );
		insertDrawable( group );

		top = new BasicAnimatedPattern( imgSurface, ScreenManager.SCREEN_WIDTH >> 2, 0 );
		top.setAnimation( BasicAnimatedPattern.ANIMATION_ALTERNATE_HORIZONTAL );
		top.setSize( 0, imgSurface.getHeight() );

		BUBBLES_HEIGHT = ( byte ) ( top.getHeight() );
		group.insertDrawable( top );

		solid = new Pattern( COLOR_SOAP );
		solid.setPosition( 0, BUBBLES_HEIGHT >> 1 );
		group.insertDrawable( solid );

		group.defineReferencePixel( 0, BUBBLES_HEIGHT );

		final byte TOTAL_SOURCES = ( byte ) ( ScreenManager.SCREEN_WIDTH > ScreenManager.SCREEN_HEIGHT ? 3 : 1 );

		sources = new DrawableImage[ TOTAL_SOURCES ];
		middles = new Pattern[ TOTAL_SOURCES ];
		drops = new Sprite[ TOTAL_SOURCES ];

		for ( byte i = 0; i < TOTAL_SOURCES; ++i ) {
			final DrawableImage source = new DrawableImage( PATH_IMAGES + "source.png" );
			sources[ i ] = source;
			source.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			insertDrawable( source );
		}

		for ( byte i = 0; i < TOTAL_SOURCES; ++i ) {
			final Pattern middle = new Pattern( new DrawableImage( PATH_IMAGES + "middle.png" ) );
			middles[ i ] = middle;
			middle.setSize( middle.getFill().getWidth(), 0 );
			middle.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			insertDrawable( middle );
		}

		final Sprite dropSprite = new Sprite( PATH_IMAGES + "drop" );

		for ( byte i = 0; i < TOTAL_SOURCES; ++i ) {
			final Sprite drop = new Sprite( dropSprite );
			drops[ i ] = drop;
			drop.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			insertDrawable( drop );
		}

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	
	public synchronized final void setNextScreen( Drawable nextScreen, byte bkgType ) {
		setNextScreen( nextScreen, bkgType, false );
	}


	public synchronized final void setNextScreen( Drawable nextScreen, byte bkgType, boolean retryLevel ) {
		mutex.acquire();

		this.bkgType = bkgType;

		this.retryLevel = retryLevel;

		sizeChanged( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		screenGroup.removeDrawable( 0 );

		previousScreen = ScreenManager.getInstance().getCurrentScreen();
		if ( previousScreen == this ) {
			previousScreen = null;
		}

		this.nextScreen = nextScreen;
		if ( previousScreen != null )
			screenGroup.insertDrawable( previousScreen );

		ScreenManager.getInstance().setCurrentScreen( this );

		mutex.release();

		setTransitionState( STATE_DROP_DOWN );
	}


	public synchronized final void setTransitionState( byte state ) {
		mutex.acquire();

		transitionState = state;

		switch ( state ) {
			case STATE_NONE:
				if ( previousScreen != null ) {
					try {
						if ( previousScreen instanceof Component )
							( ( Component ) previousScreen ).destroy();
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}
					previousScreen.setViewport( null );
					previousScreen = null;
				}

				nextScreen.setViewport( null );

				ScreenManager.getInstance().setCurrentScreen( nextScreen );

				nextScreen = null;

				screenGroup.removeDrawable( 0 );
			break;

			case STATE_DROP_DOWN:
				sourceMUV.setSpeed( sources[ 0 ].getHeight() * 1000 / SOURCE_MOVE_TIME );

				for ( byte i = 0; i < drops.length; ++i ) {
					drops[ i ].setSequence( BOTTOM_SEQUENCE_DROP );
					drops[ i ].setRefPixelPosition( drops[ i ].getRefPixelX(), 0 );
				}
				fp_accTime = 0;

				refreshMiddle();

				group.setPosition( 0, size.y + ( ( drops[ 0 ].getHeight() << 1 ) / 3 ) );

				screenViewport.set( 0, 0, size.x, size.y );
			break;

			case STATE_ALL_UP:
				sourceMUV.setSpeed( -sources[ 0 ].getHeight() * 1000 / SOURCE_MOVE_TIME );

				for ( byte i = 0; i < drops.length; ++i ) {
					drops[ i ].setSequence( BOTTOM_SEQUENCE_LIQUID );
					drops[ i ].setPosition( drops[ i ].getPosX(), size.y );
				}

				verticalSpeed.setSpeed( -Math.abs( verticalSpeed.getSpeed() ) );
			break;

			case STATE_ALL_DOWN:
				if ( retryLevel ) {
					if ( nextScreen instanceof PlayScreen )
						( ( PlayScreen ) nextScreen ).retryLevel();
				}
				screenGroup.setDrawable( nextScreen, 0 );

				GameMIDlet.setBackground( bkgType );
				for ( byte i = 0; i < drops.length; ++i ) {
					drops[ i ].setSequence( BOTTOM_SEQUENCE_LIQUID );
					drops[ i ].setPosition( drops[ i ].getPosX(), -drops[ i ].getHeight() );
				}

				verticalSpeed.setSpeed( Math.abs( verticalSpeed.getSpeed() ) );
				group.setPosition( 0, -BUBBLES_HEIGHT );

				screenViewport.set( 0, 0, 0, 0 );
			break;
		}

		mutex.release();
	}


	public final void update( int delta ) {
		super.update( delta );

		switch ( transitionState ) {
			case STATE_DROP_DOWN:
				fp_accTime += NanoMath.divInt( delta, 1000 );
				fp_dropPosY = NanoMath.mulFixed( fp_dropSpeedY, fp_accTime ) + NanoMath.mulFixed( FP_HALF_GRAVITY_ACC, NanoMath.mulFixed( fp_accTime, fp_accTime ) );

				final int dx = sourceMUV.updateInt( delta );
				final int posY = -drops[ 0 ].getHeight() + NanoMath.toInt( fp_dropPosY );
				for ( byte i = 0; i < drops.length; ++i ) {
					drops[ i ].setPosition( drops[ i ].getPosX(), posY );
					sources[ i ].move( 0, dx );
				}
				refreshMiddle();

				if ( drops[ 0 ].getPosY() >= size.y )
					setTransitionState( STATE_ALL_UP );
			break;

			case STATE_ALL_UP:
				group.move( 0, verticalSpeed.updateInt( delta ) );

				for ( byte i = 0; i < drops.length; ++i ) {
					drops[ i ].setPosition( drops[ i ].getPosX(), group.getPosY() - ( drops[ i ].getHeight() / 3 ) );
				}
				refreshMiddle();

				screenViewport.set( 0, 0, size.x, group.getRefPixelY() );

				if ( group.getPosY() < -BUBBLES_HEIGHT )
					setTransitionState( STATE_ALL_DOWN );
			break;

			case STATE_ALL_DOWN:
				group.move( 0, verticalSpeed.updateInt( delta ) );

				screenViewport.set( 0, 0, size.x, group.getRefPixelY() );

				if ( group.getPosY() >= size.y )
					setTransitionState( STATE_NONE );
			break;
		}
	}


	private final void refreshMiddle() {
		for ( byte i = 0; i < drops.length; ++i ) {
			final Sprite drop = drops[ i ];
			final DrawableImage source = sources[ i ];
			final Pattern middle = middles[ i ];

			if ( source.getPosY() >= drop.getPosY() - source.getHeight() ) {
				source.setPosition( source.getPosX(), drop.getPosY() - source.getHeight() );
			} else if ( source.getPosY() >= 0 ) {
				source.setPosition( source.getPosX(), 0 );
			}

			middle.setPosition( middle.getPosX(), source.getPosY() + source.getHeight() );
			middle.setSize( middle.getWidth(), drop.getPosY() - middle.getPosY() );
		}
	}


	protected final void paint( Graphics g ) {
		mutex.acquire();

		super.paint( g );

		mutex.release();
	}


	public final void keyPressed( int key ) {
		switch ( transitionState ) {
			case STATE_DROP_DOWN:
				setTransitionState( STATE_ALL_UP );
			break;

			case STATE_ALL_UP:
				setTransitionState( STATE_ALL_DOWN );
			case STATE_ALL_DOWN:
				setTransitionState( STATE_NONE );
			break;
		}
	}


	public final void keyReleased( int key ) {
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		fp_dropSpeedY = NanoMath.toFixed( height >> 1 );
		screenGroup.setSize( size );

		group.setSize( width, height + BUBBLES_HEIGHT );
		solid.setSize( size.x, group.getHeight() );
		top.setSize( size.x, top.getHeight() );

		final int DIVIDER = ( sources.length + 1 );
		for ( byte i = 0; i < sources.length; ++i ) {
			final int x = width * ( 1 + i ) / DIVIDER;
			sources[ i ].setRefPixelPosition( x, 0 );
			middles[ i ].setRefPixelPosition( x + 1, 0 );
			drops[ i ].setRefPixelPosition( x + 2, 0 );
		}

		verticalSpeed.setSpeed( group.getHeight() * 1000 / WATER_TRANSITION_TIME );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != size.x || height != size.y )
			setSize( width, height );
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}

	//#endif

}

//#endif