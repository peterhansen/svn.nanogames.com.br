/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;

//#if SCREEN_SIZE != "SMALL"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author Peter
 */
public final class SplashGame extends DrawableGroup implements Constants, Updatable, KeyListener
		//#if SCREEN_SIZE != "SMALL"
		, PointerListener
		//#endif
{

	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 10;
	
	/** tempo restante de exposi��o da tela de splash do jogo */
	private short visibleTime = 2700;
	//#if SCREEN_SIZE == "BIG"

	//#if VENDOR != "SAMSUNG"
	final Pattern bkg;
	final DrawableImage logo;
	final Pattern fruits1;
	final DrawableImage fruits2;
	final DrawableImage fruits3;
	private final Pattern auxbkg;
	//#else
//# 	private final DrawableImage splash;
	//#endif
	//#endif
	
	
	public SplashGame() throws Exception {
		super( TOTAL_ITEMS );

		//#if SCREEN_SIZE == "BIG"
		//#if VENDOR != "SAMSUNG"

		bkg = new Pattern(new DrawableImage(PATH_IMAGES + "pattern.png"));
		logo = new DrawableImage(PATH_IMAGES + "logo.png");

		if (ScreenManager.SCREEN_HEIGHT >= 360 || ScreenManager.SCREEN_WIDTH >= 360) {
			auxbkg = new Pattern(0xfef7b2);
			insertDrawable(auxbkg);
		} else {
			auxbkg = null;
		}
		insertDrawable(bkg);
		insertDrawable(logo);


		if (ScreenManager.SCREEN_HEIGHT >= 360 || ScreenManager.SCREEN_WIDTH >= 360) {
			insertDrawable(auxbkg);
			fruits1 = new Pattern(new DrawableImage(PATH_IMAGES+"frutinhas.png"));
			fruits2 = new DrawableImage(PATH_IMAGES+"frutinhas.png");
			fruits3 = new DrawableImage(PATH_IMAGES+"frutinhas.png");
			insertDrawable(fruits1);
			insertDrawable(fruits2);
			insertDrawable(fruits3);
		} else {
			fruits1 = null;
			fruits2 = null;
			fruits3 = null;
		}


		//#else
//# 		splash = new DrawableImage(PATH_IMAGES +"splash.png");
//# 		insertDrawable(splash);
		//#endif
		//#endif



		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		//#if SCREEN_SIZE == "MEDIUM"
//# 			// gra�as ao V3...
//# 			final DrawableImage center = new DrawableImage( PATH_IMAGES + "splash_c.png" );
//# 			center.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
//# 			center.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 0 );
//# 			insertDrawable( center );
//#
//# 			if ( size.x > center.getWidth() ) {
//# 				final DrawableImage left = new DrawableImage( PATH_IMAGES + "splash_l.png" );
//# 				left.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
//# 				left.setRefPixelPosition( center.getPosX(), 0 );
//# 				insertDrawable( left );
//#
//# 				final DrawableImage right = new DrawableImage( PATH_IMAGES + "splash_r.png" );
//# 				right.setPosition( center.getPosX() + center.getWidth(), 0 );
//# 				insertDrawable( right );
//# 			}
//#
//# 			if ( size.y > center.getHeight() ) {
//# 				final DrawableImage bottom = new DrawableImage( PATH_IMAGES + "splash_b.png" );
//# 				bottom.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
//# 				bottom.setRefPixelPosition( center.getRefPixelX(), center.getHeight() );
//# 				insertDrawable( bottom );
//# 			}
//#
//#
		//#elif  SCREEN_SIZE == "SMALL"
//# 			final DrawableImage bkg = new DrawableImage( PATH_IMAGES + "splash.png" );
//# 			bkg.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
//# 			bkg.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 0 );
//# 			insertDrawable( bkg );
//#
//#
	//#endif

	}
	//#if SCREEN_SIZE == "BIG"
	//#if VENDOR != "SAMSUNG"

	public final void setSize(int width, int height) {
		super.setSize(width, height);
		bkg.setSize(getWidth(), bkg.getHeight());
	    bkg.setPosition(0, height - bkg.getHeight());
		logo.setPosition(width / 2 - logo.getWidth() / 2, Math.max( 0, height - logo.getHeight() ) );
		if (auxbkg != null) {
			auxbkg.setSize(width, bkg.getPosY());
		}
		if (width > height && height < logo.getHeight() ) {
			if (auxbkg != null) {
				auxbkg.setSize(width, bkg.getPosY());
				auxbkg.setPosition(0, bkg.getPosY() - auxbkg.getHeight());
			}
		}
		if (fruits1 != null) {
			fruits1.setSize(width, fruits1.getHeight());
			fruits1.setPosition(0, logo.getPosY() - fruits1.getHeight());
		}
		if (fruits2 != null) {
			fruits2.setPosition(logo.getPosX() - fruits2.getWidth(), logo.getPosY());
		}
		if (fruits3 != null) {
			fruits3.setPosition(logo.getPosX() + logo.getWidth(), logo.getPosY());
		}

	//#if BLACKBERRY_API == "true"
//# 		if (height < 320) {
//# 			bkg.setPosition(0, height - bkg.getHeight() );
//# 			logo.setPosition(width / 2 - logo.getWidth() / 2, height - logo.getHeight() + 70);
//# 		}
	//#endif



	}

	//#endif

//#endif
	public final void sizeChanged(int width, int height) {
		if (width != size.x || height != size.y) {
			setSize(width, height);
		}
	}

	public final void update(int delta) {
		if (visibleTime > 0) {
			visibleTime -= delta;

			if (visibleTime <= 0) {
				// mostra o texto "Pressione qualquer tecla"
				GameMIDlet.setSoftKeyLabel(ScreenManager.SOFT_KEY_MID, TEXT_PRESS_ANY_KEY, 0);
			}
		}
	}

	public final void keyPressed(int key) {
		//#if DEBUG == "true"
		// permite pular a tela de splash
		visibleTime = 0;
		//#endif

		if (visibleTime <= 0) {
			//#if DEMO == "true"
//# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
			//#else
			GameMIDlet.setScreen(SCREEN_LOADING_2);
		//#endif
		}
	}

	public final void keyReleased( int key ) {
	}


	//#if SCREEN_SIZE != "SMALL"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}

	//#endif

}
