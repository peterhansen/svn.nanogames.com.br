/**
 * OptionsScreen.java
 * 
 * Created on Mar 3, 2009, 10:02:11 AM
 *
 */

//#if JAR != "min"

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import core.MenuLabel;

/**
 *
 * @author Peter
 */
public class OptionsScreen extends BasicOptionsScreen implements Constants {


	private OptionsScreen( MenuListener listener, int id, Drawable[] entries, int spacing, int backIndex, int soundIndex, int vibrationIndex ) throws Exception {
		super( listener, id, entries, spacing, backIndex, soundIndex, TEXT_TURN_SOUND_OFF, TEXT_TURN_SOUND_ON, vibrationIndex, TEXT_TURN_VIBRATION_OFF, TEXT_TURN_VIBRATION_ON, SOUND_INDEX_LEVEL_COMPLETE, 1 );
	}
	

	public static final OptionsScreen createInstance( MenuListener listener, int id, ImageFont font, int[] entries, int backIndex, int soundIndex, int vibrationIndex ) throws Exception {
		final Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			menuEntries[ i ] = new MenuLabel( entries[ i ] );
		}

		return new OptionsScreen( listener, id, menuEntries, 5, backIndex < 0 ? entries.length - 1 : backIndex, soundIndex, vibrationIndex );
	}
	

	protected void updateText( byte index ) {
		final MenuLabel l = ( MenuLabel ) getDrawable( index );

		if ( index == INDEX_SOUND ) {
			l.setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
		} else if ( index == INDEX_VIBRATION ) {
			l.setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
		}
		l.defineReferencePixel( l.getWidth() >> 1, 0 );
		l.setRefPixelPosition( size.x >> 1, l.getRefPixelY() );
	}

	public void setSize(int width, int height) {
		super.setSize(width, height);

		if ( getHeight() < ScreenManager.SCREEN_HEIGHT ) {
			setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1,
						 ( ScreenManager.SCREEN_HEIGHT - getHeight() ) >> 1 );
		}
	}


}

//#endif