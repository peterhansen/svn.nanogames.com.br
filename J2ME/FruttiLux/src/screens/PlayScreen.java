/**
 * PlayScreen.java
 * _2008 Nano Games.
 *
 * Created on Jun 2, 2008 6:50:28 PM.
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Border;
import core.Constants;
import core.DropEmitter;
import core.Fruit;
import core.TimeBar;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class PlayScreen extends UpdatableGroup implements Constants, KeyListener, SpriteListener, ScreenListener
//#if SCREEN_SIZE == "BIG"
		, PointerListener
//#endif

{
	
	public static final byte STATE_NONE				= 0;
	public static final byte STATE_BEGIN_LEVEL		= 1;
	public static final byte STATE_PLAYING			= 2;
	public static final byte STATE_LEVEL_CLEARED	= 3;
	public static final byte STATE_TIME_UP			= 4;
	public static final byte STATE_GAME_OVER		= 5;
	public static final byte STATE_PAUSED			= 6;
	public static final byte STATE_UNPAUSING		= 7;
	
	private byte state;
	
	public static final short TIME_MESSAGE = 3000;
	
	private short timeToNextState;

	//#if SCREEN_SIZE == "SMALL"
//# 		private short timeInfoVisible;
	//#endif

	/** Pontua__o b_sica dada para uma sequ_ncia m_nima (3 frutas). */
	private static final short SCORE_SEQUENCE	= 10;

	/***/
	private static final byte SCORE_EXTRA_FRUIT = SCORE_SEQUENCE >> 1;

	/** Contador de combos atual. */
	private short comboCounter;

	/** Contador de frutas eliminadas no combo atual. */
	private short fruitCounter;
	
	/** Pontua__o do jogador. */
	private int score;
	
	private short level;

	/** N_vel onde a dificuldade _ m_xima. */
	private static final byte LEVEL_MAX = 15;
	
	/** pontua__o exibida atualmente */
	private int scoreShown;
	
	/** label que indica a pontua__o e o multiplicador */
	private final Label scoreLabel;

	/** Posi__o x (direita) do label da pontua__o. */
	private short scoreLabelX;
	
	/** Label utilizado para mostrar uma mensagem ao jogador (fim de jogo, n_vel completo, etc.).  */
	private final RichLabel labelMessage;
	
	/** pontua__o m_xima mostrada */
	private static final int SCORE_SHOWN_MAX = 9999999;

	/** Velocidade de atualiza__o da pontua__o. */
	private final MUV scoreSpeed = new MUV();	
	
	private final Fruit[][] fruits;
	
	private final UpdatableGroup fruitGroup = new UpdatableGroup( TOTAL_FRUITS + 1 );
	
	private final Point fruitSize;
	
	/** C_lula (linha / coluna) que o cursor est_ atualmente */
	private final Point cursorCell = new Point();
	
	private static final byte CURSOR_STATE_AVAILABLE	= 0;
	private static final byte CURSOR_STATE_MOVING_FRUIT	= 1;
	private static final byte CURSOR_STATE_BLOCKED		= 2;
	private static final byte CURSOR_STATE_UNDOING		= 3;
	
	private byte cursorState;
	
	private final DrawableGroup cursor;

	private final DrawableImage cursorImg;

	private final Sprite cursorArrows;

	/** Armazena a _ltima jogada, para poder desfaz_-la caso seja inv_lida. */
	private final Point lastMove = new Point();

	/***/
	private final Point lastCellMoved = new Point();

	/***/
	private final Mutex fruitMutex = new Mutex();

	/** Tempo restante em milisegundos para o jogador. */
	private int timeRemaining;

	private static final int TIME_ACCELERATE = 140000;
	private static final int TIME_ACCELERATE_2 = TIME_ACCELERATE << 1;
	private static final int TIME_ACCELERATE_3 = TIME_ACCELERATE << 2;

	/** Tempo total acumulado na fase (ap_s um certo tempo ele _ acelerado). */
	private int totalTime;

	/** Meta de tempo em milisegundos para passar da fase atual. */
	private int currentTimeGoal;

	private int timeShown;

	/** Velocidade de atualiza__o do indicador de tempo. */
	private final MUV timeSpeed = new MUV();

	private static final int TIME_INITIAL	= 120000;
	private static final short TIME_FINAL	= 12000;

	private static final short TIME_PER_FRUIT_INITIAL = TIME_INITIAL / 119;
	private static final byte TIME_PER_FRUIT_FINAL	= TIME_FINAL / 146;

	private static final short HINT_TIME_PENALTY = TIME_FINAL / -10;

	private int currentTimePerFruit;

	private final Sprite hint;

	private static final byte HINT_ID = -123;

	private static final short EXPLOSION_FIRST_ID = 1000;

	private static final short LIQUIFY_FIRST_ID = 2000;

	private static final int TIME_HINT_INITIAL	= 7000;

	private static final int TIME_HINT_FINAL	= 60000;

	private int timeToHint;

	/** M_ximo de vidas do jogador. */
	private static final byte LIVES_MAX = 3;

	private byte lives = LIVES_MAX;

	private final Sprite[] hearts = new Sprite[ LIVES_MAX ];

	private static final byte HEART_SEQUENCE_ACTIVE		= 0;
	private static final byte HEART_SEQUENCE_BLINKING	= 1;

	private static final byte SCORE_LABELS_MAX = ( byte ) ( GameMIDlet.isLowMemory() ? 12 : 20 );

	private final ScoreLabel[] scoreLabels = new ScoreLabel[ SCORE_LABELS_MAX ];

	private final DropEmitter emitter;

	private final TimeBar timeBar;

	private final DrawableImage clock;

	private final Pattern bkgTop;
	private final Pattern bkgBottom;
	private final Pattern bkgLeft;
	private final Pattern bkgRight;

	private final Border border;

	private final DrawableImage scoreBorderLeft;
	private final DrawableImage scoreBorderRight;
	private final Pattern scoreBorderFill;

	/** Armazena a quantidade de pe_as especiais no final de um n_vel. */
	private byte shining;

	private short timeScreenVibrating;

	//#if SCREEN_SIZE == "BIG"
		private final DrawableImage lux;
		private final Sprite hintButton;

		private static final byte HINT_BUTTON_FRAME_UNPRESSED	= 0;
		private static final byte HINT_BUTTON_FRAME_PRESSED		= 1;
	//#endif

	//#if JAR != "min"
		private static final byte EXPLOSIONS_MAX = 24;
		private static final byte LIQUIFY_MAX = 7;
		private final SpriteGroup[] explosions;
		private final SpriteGroup[] liquify;
		private static Sprite[] SPRITES_LIQUIFY;
		private static Sprite[] SPRITES_EXPLOSION;

		private final Mutex mutexLiquify = new Mutex();
		private final Mutex mutexExplosion = new Mutex();
	//#endif

	private static Sprite SPRITE_HEART;
	private static Sprite SPRITE_ARROWS;
	private static DrawableImage IMG_GAME_CURSOR;
	private static ImageFont FONT_SCORE;
	private static DrawableImage IMG_SCORE_FILL;
	private static DrawableImage IMG_SCORE_BORDER;
	private static Sprite SPRITE_HINT;


	public PlayScreen() throws Exception {
		super( 90 );

		bkgTop = new Pattern( BACKGROUND_COLOR );
		bkgBottom = new Pattern( BACKGROUND_COLOR );
		bkgLeft = new Pattern( BACKGROUND_COLOR );
		bkgRight = new Pattern( BACKGROUND_COLOR );

		insertDrawable( bkgTop );
		insertDrawable( bkgBottom );
		insertDrawable( bkgLeft );
		insertDrawable( bkgRight );

		border = new Border();
		insertDrawable( border );

		//#if SCREEN_SIZE == "BIG"
			lux = new DrawableImage( PATH_IMAGES + "lux.png" );
			insertDrawable( lux );
		//#endif


		fruits = new Fruit[ TOTAL_COLUMNS ][ TOTAL_ROWS ];
		for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
			for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
				final Fruit f = new Fruit( 0 );
				fruits[ c ][ r ] = f;
				f.setListener( this );
				fruitGroup.insertDrawable( f );
			}
		}
		fruitSize = new Point( fruits[ 0 ][ 0 ].getSize() );
		fruitGroup.setSize( fruitSize.x * TOTAL_COLUMNS, fruitSize.y * TOTAL_ROWS );
		insertDrawable( fruitGroup );

		border.setSize( fruitGroup.getSize().add( BORDER_THICKNESS << 1, BORDER_THICKNESS << 1 ) );

		// insere a borda do placar
		scoreBorderLeft = new DrawableImage( IMG_SCORE_BORDER );
		insertDrawable( scoreBorderLeft );
		
		scoreBorderRight = new DrawableImage( scoreBorderLeft );
		scoreBorderRight.mirror( TRANS_MIRROR_H );
		insertDrawable( scoreBorderRight );

		scoreBorderFill = new Pattern( IMG_SCORE_FILL );
		scoreBorderFill.setSize( 0, scoreBorderFill.getFill().getHeight() );
		insertDrawable( scoreBorderFill );

		scoreBorderLeft.setPosition( scoreBorderFill.getPosX() - scoreBorderLeft.getWidth(), scoreBorderRight.getPosY() );

		timeBar = new TimeBar( false );
		insertDrawable( timeBar );
		timeBar.setSize( 0, timeBar.getHeight() );
		clock = new DrawableImage( PATH_IMAGES + "clock.png" );
		insertDrawable( clock );

		hint = new Sprite( SPRITE_HINT );
		hint.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		hint.setListener( this, HINT_ID );
		insertDrawable( hint );

		emitter = new DropEmitter();
		insertDrawable( emitter );

		// insere o label indicando a pontua__o
		scoreLabel = new Label( FONT_SCORE, "0" );
		scoreLabelX = ( short ) scoreBorderRight.getPosX();
		insertDrawable( scoreLabel );

		cursor = new UpdatableGroup( 2 );
		cursorImg = new DrawableImage( IMG_GAME_CURSOR );
		cursorImg.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		cursor.insertDrawable( cursorImg );

		cursorArrows = new Sprite( SPRITE_ARROWS );
		cursorArrows.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		cursor.insertDrawable( cursorArrows );

		cursor.setSize( cursorArrows.getSize() );
		cursorImg.setRefPixelPosition( cursor.getWidth() >> 1, cursor.getHeight() >> 1 );
		cursor.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );

		insertDrawable( cursor );
		setCursorState( CURSOR_STATE_BLOCKED );
		setCursorCell( TOTAL_COLUMNS >> 1, TOTAL_ROWS >> 1 );

		for ( byte i = 0; i < LIVES_MAX; ++i ) {
			final Sprite heart = new Sprite( SPRITE_HEART );
			hearts[ i ] = heart;
			insertDrawable( heart );
		}

		//#if SCREEN_SIZE == "BIG"
			if ( ScreenManager.getInstance().hasPointerEvents() ) {
				hintButton = new Sprite( PATH_IMAGES + "hint_button" );
				insertDrawable( hintButton );
			} else {
				hintButton = null;
			}
		//#endif

		//#if JAR != "min"
			if ( SPRITES_LIQUIFY == null ) {
				liquify = null;
			} else {
				liquify = new SpriteGroup[ LIQUIFY_MAX ];
				for ( byte i = 0; i < LIQUIFY_MAX; ++i ) {
					final SpriteGroup l = new SpriteGroup();
					liquify[ i ] = l;

					l.setSize( SPRITES_LIQUIFY[ 0 ].getSize() );
					l.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
					insertDrawable( l );

					for ( byte j = 0; j < Fruit.TOTAL_TYPES; ++j ) {
						final Sprite s = new Sprite( SPRITES_LIQUIFY[ j ] );
						s.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
						s.setListener( this, LIQUIFY_FIRST_ID + i );

						l.insertDrawable( s );
					}
				}
			}

			if ( SPRITES_EXPLOSION == null ) {
				explosions = null;
			} else {
				explosions = new SpriteGroup[ EXPLOSIONS_MAX ];
				for ( byte i = 0; i < EXPLOSIONS_MAX; ++i ) {
					final SpriteGroup e = new SpriteGroup();
					explosions[ i ] = e;

					e.setSize( SPRITES_EXPLOSION[ 0 ].getSize() );
					e.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
					insertDrawable( e );

					for ( byte j = 0; j < Fruit.TOTAL_TYPES; ++j ) {
						final Sprite s = new Sprite( SPRITES_EXPLOSION[ j ] );
						s.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
						s.setListener( this, EXPLOSION_FIRST_ID + i );

						e.insertDrawable( s );
					}
				}
			}
		//#endif

		for ( byte i = 0; i < SCORE_LABELS_MAX; ++i ) {
			scoreLabels[ i ] = new ScoreLabel( this );
			insertDrawable( scoreLabels[ i ] );
		}

		// insere o label que mostra mensagens ao jogador
		labelMessage = new RichLabel( GameMIDlet.getFont( FONT_BIG ), "", getWidth() );
		insertDrawable( labelMessage );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public static final void load() throws Exception {
		//#if JAR == "min"
//# 			FONT_SCORE = GameMIDlet.getFont( FONT_TEXT );
		//#else
			final byte LOAD_ALL = 0;
			final byte LOAD_NONE = 1;
			final byte LOAD_1_COLOR = 2;

			byte loadExplosion = LOAD_ALL;
			byte loadLiquify = LOAD_ALL;


			//#if SCREEN_SIZE == "BIG"

				switch ( GameMIDlet.getVendor() ) {
					case GameMIDlet.VENDOR_SIEMENS:
						// o BenQ EL71 (e talvez outros) demora demais para carregar imagens
						loadLiquify = LOAD_1_COLOR;
						loadExplosion = LOAD_1_COLOR;
					break;

					default:
						if ( GameMIDlet.isLowMemory() ) {
							loadLiquify = LOAD_1_COLOR;
							loadExplosion = LOAD_1_COLOR;
						}
				}

			//#elif SCREEN_SIZE == "MEDIUM"
//# 				switch ( GameMIDlet.getVendor() ) {
//# 					case GameMIDlet.VENDOR_MOTOROLA:
//# 						// V3...
//# 						loadLiquify = LOAD_1_COLOR;
//# 						loadExplosion = LOAD_NONE;
//# 					break;
//#
//# 					case GameMIDlet.VENDOR_LG:
//# 						loadLiquify = LOAD_1_COLOR;
//# 						loadExplosion = LOAD_1_COLOR;
//# 					break;
//#
//# 					default:
//# 						if ( GameMIDlet.isLowMemory() ) {
//# 							loadExplosion = LOAD_1_COLOR;
//# 							loadLiquify = LOAD_1_COLOR;
//# 						}
//# 					break;
//# 				}
//#
			//#elif SCREEN_SIZE == "SMALL"
//# 				switch ( GameMIDlet.getVendor() ) {
//# 					case GameMIDlet.VENDOR_SAGEM_GRADIENTE:
//# 						loadLiquify = LOAD_1_COLOR;
//# 						loadExplosion = LOAD_NONE;
//# 					break;
//#
//# 					case GameMIDlet.VENDOR_SAMSUNG:
//# 						loadLiquify = LOAD_1_COLOR;
//# 						loadExplosion = LOAD_1_COLOR;
//# 					break;
//#
//# 					default:
//# 						if ( GameMIDlet.isLowMemory() ) {
//# 							loadExplosion = LOAD_1_COLOR;
//# 							loadLiquify = LOAD_1_COLOR;
//# 						}
//# 					break;
//# 				}
//#
			//#endif

			switch ( loadLiquify ) {
				case LOAD_ALL:
					SPRITES_LIQUIFY = new Sprite[ Fruit.TOTAL_TYPES ];
					for ( byte i = Fruit.TOTAL_TYPES - 1; i >= 0; --i ) {
						final PaletteMap[] paletteMap = Fruit.getPaletteMap( i );
						SPRITES_LIQUIFY[ i ] = new Sprite( PATH_IMAGES + "liquify/l.bin", PATH_IMAGES + "liquify/l", paletteMap );
					}
				break;

				case LOAD_1_COLOR:
					SPRITES_LIQUIFY = new Sprite[ Fruit.TOTAL_TYPES ];
					SPRITES_LIQUIFY[ Fruit.ORIGINAL_FRUIT ] = new Sprite( PATH_IMAGES + "liquify/l" );
					for ( byte i = Fruit.TOTAL_TYPES - 1; i >= 0; --i ) {
						if ( i != Fruit.ORIGINAL_FRUIT )
							SPRITES_LIQUIFY[ i ] = new Sprite( SPRITES_LIQUIFY[ Fruit.ORIGINAL_FRUIT ] );
					}
				break;
			}

			switch ( loadExplosion ) {
				case LOAD_ALL:
					SPRITES_EXPLOSION = new Sprite[ Fruit.TOTAL_TYPES ];
					for ( byte i = Fruit.TOTAL_TYPES - 1; i >= 0; --i ) {
						final PaletteMap[] paletteMap = Fruit.getPaletteMap( i );
						SPRITES_EXPLOSION[ i ] = new Sprite( PATH_IMAGES + "explosion/e.bin", PATH_IMAGES + "explosion/e", paletteMap );
					}
				break;

				case LOAD_1_COLOR:
					SPRITES_EXPLOSION = new Sprite[ Fruit.TOTAL_TYPES ];
					SPRITES_EXPLOSION[ Fruit.ORIGINAL_FRUIT ] = new Sprite( PATH_IMAGES + "explosion/e" );
					for ( byte i = Fruit.TOTAL_TYPES - 1; i >= 0; --i ) {
						if ( i != Fruit.ORIGINAL_FRUIT )
							SPRITES_EXPLOSION[ i ] = new Sprite( SPRITES_EXPLOSION[ Fruit.ORIGINAL_FRUIT ] );
					}
				break;
			}
 			FONT_SCORE = ImageFont.createMonoSpacedFont( PATH_IMAGES + "font_score.png", "0123456789" );
			FONT_SCORE.setCharExtraOffset( 1 );
		//#endif

		SPRITE_HEART = new Sprite( PATH_IMAGES + "heart" );
		SPRITE_ARROWS = new Sprite( PATH_IMAGES + "arrows" );
		IMG_GAME_CURSOR = new DrawableImage( PATH_IMAGES + "game_cursor.png" );
		IMG_SCORE_FILL = new DrawableImage( PATH_IMAGES + "score_fill.png" );
		IMG_SCORE_BORDER = new DrawableImage( PATH_IMAGES + "score_border.png" );
		SPRITE_HINT = new Sprite( PATH_IMAGES + "hint" );
	}
	
	
	public final void update( int delta ) {
		switch ( state ) {
			case STATE_PAUSED:
			return;

			case STATE_UNPAUSING:
			break;

			default:
				super.update( delta );
		}

		//#if SCREEN_SIZE == "SMALL"
//# 		if ( timeInfoVisible > 0 ) {
//# 			timeInfoVisible -= delta;
//#
//# 			if ( timeInfoVisible <= 0 )
//# 				setInfoVisible( false );
//# 		}
		//#endif


		if ( timeScreenVibrating > 0 ) {
			timeScreenVibrating -= delta;

			if ( timeScreenVibrating > 0 )
				setPosition( -VIBRATION_SCREEN_HALF_OFFSET + NanoMath.randInt( VIBRATION_SCREEN_OFFSET ),
							 -VIBRATION_SCREEN_HALF_OFFSET + NanoMath.randInt( VIBRATION_SCREEN_OFFSET ) );
			else
				stopScreenVibration();
		}
		
		// caso o estado atual tenha uma dura__o m_xima definida, atualiza o contador
		if ( timeToNextState > 0 ) {
			timeToNextState -= delta;
			
			if ( timeToNextState <= 0 )
				stateEnded();
		}

		switch ( state ) {
			case STATE_PLAYING:
				switch ( cursorState ) {
					case CURSOR_STATE_AVAILABLE:
					case CURSOR_STATE_UNDOING:
					case CURSOR_STATE_MOVING_FRUIT:
						if ( timeRemaining > 0 ) {
							if ( timeRemaining >= currentTimeGoal ) {
								setState( STATE_LEVEL_CLEARED );
							} else {
								totalTime += delta;
								if ( level < LEVEL_MAX - 2 && totalTime >= TIME_ACCELERATE ) {
									if ( totalTime < ( TIME_ACCELERATE_2 ) )
										delta = delta * 115 / 100;
									else if ( totalTime < ( TIME_ACCELERATE_3 ) )
										delta = delta * 127 / 100;
									else
										delta = delta * 152 / 100;
								}
								
								changeTime( -delta );
								if ( !hint.isVisible() && timeToHint > 0 ) {
									timeToHint -= delta;
									if ( timeToHint <= 0 )
										setHintVisible( true );
								}
							}
						} else {
							// acabou o tempo do jogador
							--lives;
							hearts[ lives ].setSequence( HEART_SEQUENCE_BLINKING );
							if ( lives > 0 ) {
								setState( STATE_TIME_UP );
							} else {
								setState( STATE_GAME_OVER );
							}
						}
					break;
				}
			break;
		}
		if ( state != STATE_BEGIN_LEVEL )
			updateTime( delta, false );

		// atualiza a pontua__o
		updateScore( delta, false );
	}	


	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_PLAYING:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.UP:
						checkAndMoveCursorCell( 0, -1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.DOWN:
						checkAndMoveCursorCell( 0, 1 );
					break;

					case ScreenManager.KEY_NUM4:
					case ScreenManager.LEFT:
						checkAndMoveCursorCell( -1, 0 );
					break;

					case ScreenManager.KEY_NUM6:
					case ScreenManager.RIGHT:
						checkAndMoveCursorCell( 1, 0 );
					break;

					case ScreenManager.KEY_NUM1:
						checkAndMoveCursorCell( -1, -1 );
					break;

					case ScreenManager.KEY_NUM3:
						checkAndMoveCursorCell( 1, -1 );
					break;

					case ScreenManager.KEY_NUM7:
						checkAndMoveCursorCell( -1, 1 );
					break;

					case ScreenManager.KEY_NUM9:
						checkAndMoveCursorCell( 1, 1 );
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
						switch ( cursorState ) {
							case CURSOR_STATE_AVAILABLE:
								setCursorState( CURSOR_STATE_MOVING_FRUIT );
							break;

							case CURSOR_STATE_MOVING_FRUIT:
								setCursorState( CURSOR_STATE_AVAILABLE );
							break;
						}
					break;

					case ScreenManager.KEY_NUM0:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_POUND:
						if ( setHintVisible( true ) )
							changeTime( HINT_TIME_PENALTY );
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_SOFT_MID:
						setState( STATE_PAUSED );
					break;
				}
			break;

			case STATE_LEVEL_CLEARED:
			break;

			case STATE_GAME_OVER:
				stateEnded();
			break;
		} // fim switch ( state )
	}


	public final void keyReleased( int key ) {
	}


//#if SCREEN_SIZE == "BIG"

		public final void onPointerDragged( int x, int y ) {
			if ( fruitGroup.contains( x, y ) ) {
				switch ( cursorState ) {
					case CURSOR_STATE_AVAILABLE:
						setCursorCell( getCellAt( x, y ) );
					break;

					default:
						onPointerPressed( x, y );
				}
			} else if ( hintButton != null ) {
				if ( !hintButton.contains( x, y ) )
					hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
			}
		}


		public final void onPointerPressed( int x, int y ) {
			switch ( state ) {
				case STATE_PLAYING:
					if ( fruitGroup.contains( x, y ) ) {
						final Point cellClicked = getCellAt( x, y );

						switch ( cursorState ) {
							case CURSOR_STATE_AVAILABLE:
								setCursorCell( cellClicked );
								setCursorState( CURSOR_STATE_MOVING_FRUIT );
							break;

							case CURSOR_STATE_MOVING_FRUIT:
								if ( isAdjacentToCursor( cellClicked ) ) {
									checkAndMoveCursorCell( cellClicked.x - cursorCell.x, cellClicked.y - cursorCell.y );
								} else {
									setCursorCell( cellClicked );
								}
							break;
						}

						if ( hintButton != null )
							hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
					} else if ( hintButton != null ) {
						if ( hintButton.contains( x, y ) ) {
							hintButton.setFrame( HINT_BUTTON_FRAME_PRESSED );
							keyPressed( ScreenManager.KEY_STAR );
						} else {
							hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
						}
					}
				break;

				default:
					keyPressed( ScreenManager.FIRE );
			}
		}


		public final void onPointerReleased( int x, int y ) {
			if ( hintButton != null )
				hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
		}


	//#endif


	//#if JAR != "min"
	private final SpriteGroup getNextExplosion() {
		mutexExplosion.acquire();

		try {
			for ( byte i = 0; i < EXPLOSIONS_MAX; ++i ) {
				if ( explosions[ i ].isIdle() )
					return explosions[ i ];
			}

			return explosions[ NanoMath.randInt( EXPLOSIONS_MAX ) ];
		} finally {
			mutexExplosion.release();
		}
	}


	private final SpriteGroup getNextLiquify() {
		mutexLiquify.acquire();
		
		try {
			for ( byte i = 0; i < LIQUIFY_MAX; ++i ) {
				if ( liquify[ i ].isIdle() )
					return liquify[ i ];
			}

			return liquify[ NanoMath.randInt( LIQUIFY_MAX ) ];
		} finally {
			mutexLiquify.release();
		}
	}

	//#endif


	private final ScoreLabel getNextScoreLabel() {
		byte candidate = 0;
		for ( byte i = 0; i < SCORE_LABELS_MAX; ++i ) {
			switch ( scoreLabels[ i ].state ) {
				case ScoreLabel.STATE_IDLE:
					return scoreLabels[ i ];

				case ScoreLabel.STATE_VANISHING:
					candidate = i;
				break;
			}
		}

		return scoreLabels[ candidate ];
	}


	private final Point getCellAt( int x, int y ) {
		return new Point( ( x - fruitGroup.getPosX() ) / fruitSize.x, ( y - fruitGroup.getPosY() ) / fruitSize.y );
	}


	private final void checkAndMoveCursorCell( int columns, int rows ) {
		switch ( cursorState ) {
			case CURSOR_STATE_AVAILABLE:
			case CURSOR_STATE_BLOCKED:
				moveCursorCell( columns, rows );
			break;

			case CURSOR_STATE_MOVING_FRUIT:
				// o teste sobre as colunas e linhas _ para evitar movimentos nas diagonais
				if ( ( columns == 0 || rows == 0 ) && switchFruits( cursorCell, new Point( cursorCell.x + columns, cursorCell.y + rows ) ) ) {
					setCursorState( CURSOR_STATE_BLOCKED );
				}
			break;
		}
	}


	private final boolean switchFruits( Point cell1, Point cell2 ) {
		if ( cell2.x >= 0 && cell2.x < TOTAL_COLUMNS && cell2.y >= 0 && cell2.y < TOTAL_ROWS ) {
			final Point move = cell2.sub( cell1 );
			fruits[ cell1.x ][ cell1.y ].moveToCell( cell2.x, cell2.y, move.x == 0 ? Fruit.STATE_SWITCHING_V : Fruit.STATE_SWITCHING_H );
			fruits[ cell2.x ][ cell2.y ].moveToCell( cell1.x, cell1.y, move.x == 0 ? Fruit.STATE_SWITCHING_V : Fruit.STATE_SWITCHING_H );

			final Fruit temp = fruits[ cell1.x ][ cell1.y ];
			fruits[ cell1.x ][ cell1.y ] = fruits[ cell2.x ][ cell2.y ];
			fruits[ cell2.x ][ cell2.y ] = temp;

			lastCellMoved.set( cell1 );
			lastMove.set( move );
			
			return true;
		}
		return false;
	}


	private final boolean isAdjacentToCursor( Point cell ) {
		return isAdjacentToCursor( cell.x, cell.y );
	}


	private final boolean isEqual( Fruit[] sequence1, Fruit[] sequence2 ) {
		if ( sequence1 != null && sequence2 != null && sequence1.length == sequence2.length ) {
			byte equalCount = 0;
			for ( byte i = 0; i < sequence1.length; ++i ) {
				for ( byte j = 0; j < sequence1.length; ++j ) {
					if ( sequence1[ i ] == sequence2[ j ] ) {
						j = ( byte ) sequence1.length;
						++equalCount;
					}
				}
			}

			return equalCount == sequence1.length;
		}

		return false;
	}


	private final boolean intersects( Fruit[] sequence1, Fruit[] sequence2 ) {
		if ( sequence1 != null && sequence2 != null && sequence1 != sequence2 ) {
			for ( byte i = 0; i < sequence1.length; ++i ) {
				for ( byte j = 0; j < sequence2.length; ++j ) {
					if ( sequence1[ i ] == sequence2[ j ] )
						return true;
				}
			}
		}

		return false;
	}


	/**
	 * Indica se uma determinada c_lula _ adjacente _ posi__o do cursor, ou seja, se a c_lula est_ num desses 2 casos:
	 * 1. na mesma coluna que o cursor 
	 * @param column
	 * @param row
	 * @return
	 */
	private final boolean isAdjacentToCursor( int column, int row ) {
		final int columnDiff = Math.abs( column - cursorCell.x );
		final int rowDiff = Math.abs( row - cursorCell.y );

		return ( ( columnDiff == 1 && rowDiff == 0 ) || ( columnDiff == 0 && rowDiff == 1 ) );
	}
	
	
	/**
	 * Troca a c_lula do cursor.
	 * @param columns quantidade de colunas a deslocar o cursor.
	 * @param rows quantidade de linhas a deslocar o cursor.
	 */
	private final void moveCursorCell( int columns, int rows ) {
		setCursorCell( cursorCell.x + columns, cursorCell.y + rows );
	}


	private final void setCursorCell( int column, int row ) {
		cursorCell.set( ( TOTAL_COLUMNS + column ) % TOTAL_COLUMNS, ( TOTAL_ROWS + row ) % TOTAL_ROWS );
		cursor.setRefPixelPosition( fruitGroup.getPosX() + ( cursorCell.x * fruitSize.x ) + ( fruitSize.x >> 1 ),
									fruitGroup.getPosY() + ( cursorCell.y * fruitSize.y ) + ( fruitSize.y >> 1 ) );
	}


	private final void setCursorCell( Point cell ) {
		setCursorCell( cell.x, cell.y );
	}


	private final void setCursorState( byte cursorState ) {
		switch ( cursorState ) {
			case CURSOR_STATE_AVAILABLE:
				comboCounter = level;

				fruitCounter = 0;
			break;
		}
		this.cursorState = cursorState;
		cursorArrows.setVisible( cursorState == CURSOR_STATE_MOVING_FRUIT );
	}

    
	public final void setState( int state ) {
		this.state = ( byte ) state;
		
		timeToNextState = 0;
		labelMessage.setVisible( false );
		cursor.setVisible( state == STATE_PLAYING );

		//#if SCREEN_SIZE == "SMALL"
//# 			setInfoVisible( false );
//# 			timeInfoVisible = 0;
		//#endif
		
		switch ( state ) {
			case STATE_BEGIN_LEVEL:
				showMessage( GameMIDlet.getText( TEXT_LEVEL ) + level );
				
				timeToNextState = TIME_MESSAGE;
			break;
			
			case STATE_PLAYING:
				//#if SCREEN_SIZE == "SMALL"
//# 					timeInfoVisible = TIME_MESSAGE;
//# 					setInfoVisible( true );
				//#endif

				labelMessage.setVisible( false );
				if ( !MediaPlayer.isPlaying() ) {
					//#if JAR == "min"
//# 						// s_ tem 1 m_sica ambiente
//# 						MediaPlayer.play( SOUND_INDEX_AMBIENT_1, MediaPlayer.LOOP_INFINITE );
					//#else
						// sorteia uma das m_sicas ambiente
						MediaPlayer.play( NanoMath.randInt( 128 ) < 64 ? SOUND_INDEX_AMBIENT_1 : SOUND_INDEX_AMBIENT_2, MediaPlayer.LOOP_INFINITE );
					//#endif
				}
			break;

			case STATE_TIME_UP:
				showMessage( GameMIDlet.getText( TEXT_LIFE_LOST ) );
				MediaPlayer.play( SOUND_INDEX_TIME_UP );
				timeToNextState = TIME_MESSAGE;

				stopScreenVibration();
			break;
			
			case STATE_GAME_OVER:
				setCursorState( CURSOR_STATE_BLOCKED );
				showMessage( GameMIDlet.getText( TEXT_GAME_OVER ) );
				MediaPlayer.play( SOUND_INDEX_GAME_OVER );

				stopScreenVibration();
			break;
			
			case STATE_LEVEL_CLEARED:
				setCursorState( CURSOR_STATE_BLOCKED );
				showMessage( GameMIDlet.getText( TEXT_LEVEL ) + level + GameMIDlet.getText( TEXT_COMPLETE ) );
				timeToNextState = TIME_MESSAGE;

				updateScore( 0, true );

				MediaPlayer.play( SOUND_INDEX_LEVEL_COMPLETE );

				stopScreenVibration();
			break;

			case STATE_PAUSED:
				GameMIDlet.setScreen( SCREEN_PAUSE );
			break;

			case STATE_UNPAUSING:
				//#if SCREEN_SIZE == "SMALL"
//# 					setInfoVisible( true );
				//#endif

				timeToNextState = WATER_TRANSITION_TIME;
			break;
		}
	}


	private final void stopScreenVibration() {
		setPosition( 0, 0 );
		timeScreenVibrating = 0;
	}
	
	
	private final void stateEnded() {
		switch ( state ) {
			case STATE_LEVEL_CLEARED:
				for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
					for ( byte r = TOTAL_ROWS - 1; r >= 0; --r ) {
						if ( getFruit( c, r ).getState() == Fruit.STATE_BOMB )
							++shining;
					}
				}
				GameMIDlet.setScreen( SCREEN_BEAUTY_TIPS );
			break;
			
			case STATE_TIME_UP:
				GameMIDlet.setScreen( SCREEN_RETRY );
			break;
			
			case STATE_BEGIN_LEVEL:
			case STATE_UNPAUSING:
				setState( STATE_PLAYING );
			break;
			
			case STATE_GAME_OVER:
				GameMIDlet.gameOver( score );
			break;
		}
	}


	public final void prepareNextLevel() {
		prepareLevel( level + 1 );
	}
	
	
	public final void retryLevel() {
		prepareLevel( level );
	}


	public final short getLevel() {
		return level;
	}


	private final void prepareLevel( int level ) {
		this.level = ( short ) level;

		do {
			for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
				int yOffset = -fruitGroup.getHeight() - NanoMath.randInt( fruitGroup.getHeight() );
				for ( byte r = TOTAL_ROWS - 1; r >= 0; --r ) {
					final Fruit f = fruits[ c ][ r ];

					f.setType( Fruit.TYPE_RANDOM );
					yOffset -= NanoMath.randInt( f.getHeight() );
					f.reset( c, r, yOffset );
				}
			}

			// elimina as sequ_ncias j_ prontas
			Fruit[][] sequences = getAllSequences();
			do {
				for ( byte i = 0; i < sequences.length; ++i ) {
					for ( byte j = 0; j < sequences[ 0 ].length; ++j ) {
						sequences[ 0 ][ j ].setType( Fruit.TYPE_RANDOM );
					}
				}
				sequences = getAllSequences();
			} while ( sequences.length > 0 );
		} while ( !hasSequence() );

		final byte difficultyLevel = ( byte ) Math.min( level, LEVEL_MAX );

		stopScreenVibration();
		
		currentTimeGoal = TIME_INITIAL + ( ( TIME_FINAL - TIME_INITIAL ) * difficultyLevel / LEVEL_MAX );

		// reinicia a barra de tempo
		timeRemaining = 1;
		timeShown = 0;
		updateTime( 0, true );
		
		timeRemaining = currentTimeGoal >> 1;
		timeShown = 0;
		totalTime = 0;

		currentTimePerFruit = TIME_PER_FRUIT_INITIAL + ( ( TIME_PER_FRUIT_FINAL - TIME_PER_FRUIT_INITIAL ) * difficultyLevel / LEVEL_MAX );

		//#if JAR != "min"
			mutexExplosion.acquire();
			if ( explosions != null ) {
				for ( byte i = 0; i < EXPLOSIONS_MAX; ++i ) {
					explosions[ i ].idle();
				}
			}
			mutexExplosion.release();

			mutexLiquify.acquire();
			if ( liquify != null ) {
				for ( byte i = 0; i < LIQUIFY_MAX; ++i ) {
					liquify[ i ].idle();
				}
			}
			mutexLiquify.release();
		//#endif

		while ( shining > 0 ) {
			final Fruit f = getFruit( NanoMath.randInt( TOTAL_COLUMNS ), NanoMath.randInt( TOTAL_ROWS ) );
			if ( f.getState() != Fruit.STATE_BOMB ) {
				--shining;
				f.setState( Fruit.STATE_FALLING_BOMB );
			}
		}

		setHintVisible( false );
		
		setState( STATE_BEGIN_LEVEL );
	}	
	
	
	/**
	 * Atualiza o label da pontua__o.
	 * @param equalize indica se a pontua__o mostrada deve ser automaticamente igualada _ pontua__o real.
	 */
	private final void updateScore( int delta, boolean equalize ) {
		if ( scoreShown < score ) {
			if ( equalize ) {
				scoreShown = score;
			} else  {
				scoreShown += scoreSpeed.updateInt( delta );
				if ( scoreShown >= score ) {
					if ( scoreShown > SCORE_SHOWN_MAX )
						scoreShown = SCORE_SHOWN_MAX;
					
					scoreShown = score;
				}
			}
			updateScoreLabel();
		}
	} // fim do m_todo updateScore( boolean )


	private final void updateTime( int delta, boolean equalize ) {
		if ( timeShown != timeRemaining ) {
			if ( equalize ) {
				timeShown = timeRemaining;
			} else  {
				timeShown += timeSpeed.updateInt( delta );
				if ( ( timeSpeed.getSpeed() > 0 && timeShown >= timeRemaining ) || ( timeSpeed.getSpeed() < 0 && timeShown <= timeRemaining ) ) {
					timeShown = timeRemaining;
				}
			}
			timeBar.setTime( timeShown, currentTimeGoal );
		}
	} // fim do m_todo updateScore( boolean )


	public static final Sprite getHintSprite() {
		return SPRITE_HINT;
	}

	
	private final void updateScoreLabel() {
//		AppMIDlet.gc();
//		scoreLabel.setText( String.valueOf( Runtime.getRuntime().freeMemory() ) );
		scoreLabel.setText( String.valueOf( scoreShown ) );
		scoreLabel.setPosition( scoreLabelX - scoreLabel.getWidth(), scoreLabel.getPosY() );
	}	

	
	/**
	 * Incrementa a pontua__o e atualiza as vari_veis e labels correspondentes.
	 * @param diff varia__o na pontua__o (positiva ou negativa).
	 */
	private final void changeScore( int diff ) {
		score += diff;
		
		scoreSpeed.setSpeed( ( score - scoreShown ) >> 1, false );
	}


	private final void changeTime( int diff ) {
		timeRemaining += diff;
		timeSpeed.setSpeed( ( timeRemaining - timeShown ) >> 1, false );
	}


	private final void showMessage( String message ) {
		labelMessage.setText( "<ALN_H>" + message );
		labelMessage.setSize( getWidth(), labelMessage.getTextTotalHeight() );
		labelMessage.setPosition( 0, size.y - ( labelMessage.getHeight() ) >> 1 );
		labelMessage.setVisible( true );

		//#if SCREEN_SIZE == "SMALL"
//# 			setInfoVisible( true );
		//#endif
	}


	//#if SCREEN_SIZE == "SMALL"
//# 	private final void setInfoVisible( boolean visible ) {
//# 		if ( size.y < HEIGHT_MIN ) {
//# 			scoreLabel.setVisible( visible );
//# 			for ( byte i = 0; i < hearts.length; ++i )
//# 				hearts[ i ].setVisible( visible);
//# 			scoreBorderFill.setVisible( visible );
//# 			scoreBorderLeft.setVisible( visible );
//# 			scoreBorderRight.setVisible( visible );
//# 			scoreLabel.setVisible( visible );
//# 		}
//# 	}
	//#endif
	
	
	public final int getScore() {
		return score;
	}


	/**
	 * Obt_m todas as sequ_ncias e sub-sequ_ncias existentes na c_lula indicada.
	 * @param column
	 * @param row
	 * @return
	 */
	private final Fruit[][] getSequencesAt( int column, int row ) {
		final Fruit fruit = fruits[ column ][ row ];

		// primeiro testa o eixo horizontal
		int startX = column;
		int endX = column;
		// esquerda
		while ( startX > 0 && fruits[ startX - 1 ][ row ].equals( fruit ) ) {
			--startX;
		}
		// direita
		while ( endX < ( TOTAL_COLUMNS - 1 ) && fruits[ endX + 1 ][ row ].equals( fruit ) ) {
			++endX;
		}

		// testa o eixo vertical
		int startY = row;
		int endY = row;
		// cima
		while ( startY > 0 && fruits[ column ][ startY - 1 ].equals( fruit ) ) {
			--startY;
		}
		// baixo
		while ( endY < ( TOTAL_ROWS - 1 ) && fruits[ column ][ endY + 1 ].equals( fruit ) ) {
			++endY;
		}

		// caso tenha 2 sequ_ncias v_lidas na c_lula atual (horizontal E vertical), retorna a maior das 2
		final int diffX = endX - startX;
		final int diffY = endY - startY;

		final byte[] TOTAL_COMBINATIONS = { 1, 3, 6, 10, 15, 21, 28 };

		if ( diffX > diffY ) {
			if ( diffX >= 2 ) {
				//#if DEBUG == "true"
					System.out.println( "HORIZONTAL: " + startX + "/" + endX + ", " + row );
				//#endif

				final byte total = TOTAL_COMBINATIONS[ diffX - 2 ];
				final byte initialLength = ( byte ) ( diffX + 1 );
				final Fruit[][] sequences = new Fruit[ total ][];
				for ( byte i = 0, currentLength = initialLength; i < total; --currentLength ) {
					final byte limit = ( byte ) ( initialLength - currentLength );
					for ( byte j = 0; j <= limit; ++j, ++i ) {
						final Fruit[] sequence = new Fruit[ currentLength ];
						sequences[ i ] = sequence;
						for ( int c = 0; c < currentLength; ++c )
							sequence[ c ] = getFruit( startX + c + j, row );
					}
				}

				return sequences;
			}
		} else {
			if ( diffY >= 2 ) {
				//#if DEBUG == "true"
					System.out.println( "VERTICAL: " + column + ", " + startY + "/" + endY );
				//#endif

				final byte total = TOTAL_COMBINATIONS[ diffY - 2 ];
				final byte initialLength = ( byte ) ( diffY + 1 );
				final Fruit[][] sequences = new Fruit[ total ][];
				for ( byte i = 0, currentLength = initialLength; i < total; --currentLength ) {
					final byte limit = ( byte ) ( initialLength - currentLength );
					for ( byte j = 0; j <= limit; ++j, ++i ) {
						final Fruit[] sequence = new Fruit[ currentLength ];
						sequences[ i ] = sequence;
						for ( int c = 0; c < currentLength; ++c )
							sequence[ c ] = getFruit( column, startY + c + j );
					}
				}

				return sequences;
			}
		}

		return new Fruit[ 0 ][];
	}


	/**
	 * Retorna a lista de sequ_ncias _nicas existentes no tabuleiro.
	 * @return
	 */
	private final Fruit[][] getAllSequences() {
		final Vector v = new Vector();
		final Vector v2 = new Vector();

		for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
			for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
				final Fruit[][] sequences = getSequencesAt( c, r );

				for ( byte i = 0; i < sequences.length; ++i ) {
					final Fruit[] sequence = sequences[ i ];
					if ( sequence != null ) {
						v.addElement( sequence );
						v2.addElement( sequence );
					}
				}
			}
		}

		final int size1 = v.size();
		for ( int i = 0; i < size1; ++i ) {
			for ( int j = i + 1; j < v2.size(); ) {
				final Fruit[] sequence1 = ( Fruit[] ) v.elementAt( i );
				final Fruit[] sequence2 = ( Fruit[] ) v2.elementAt( j );
				
				if ( intersects( sequence1, sequence2 ) ) {
					if ( !v2.removeElement( sequence1.length < sequence2.length ? sequence1 : sequence2 ) )
						++j;
				} else {
					++j;
				}
			}
		}


		final Fruit[][] sequences = new Fruit[ v2.size() ][];
		for ( byte i = 0; i < sequences.length; ++i ) {
			sequences[ i ] = ( Fruit[] ) v2.elementAt( i );

			//#if DEBUG == "true"
				System.out.print( i + ": " );
				for ( byte j = 0; j < sequences[ i ].length; ++j ) {
					System.out.print( sequences[ i ][ j ].getCell() );
				}
				System.out.println();
			//#endif
		}

		return sequences;
	}


	private final boolean hasSequenceAt( int column, int row ) {
		return getSequencesAt( column, row ).length > 0;
	}


	private final boolean hasSequenceAt( int column, int row, int diffColumn, int diffRow ) {
		fruitMutex.acquire();
		final Fruit originalFruit = fruits[ column ][ row ];
		if ( column + diffColumn >= TOTAL_COLUMNS || column + diffColumn < 0 )
			diffColumn = 0;

		if ( row + diffRow >= TOTAL_ROWS || row + diffRow < 0 )
			diffRow = 0;
		
		try {
			fruits[ column ][ row ] = fruits[ column + diffColumn ][ row + diffRow ];
			fruits[ column + diffColumn ][ row + diffRow ] = originalFruit;
			
			return hasSequenceAt( column, row );
		} finally {
			// destroca as frutas
			fruits[ column + diffColumn ][ row + diffRow ] = fruits[ column ][ row ];
			fruits[ column ][ row ] = originalFruit;
			
			fruitMutex.release();
		}
	}


	/**
	 * Indica se h_ pelo menos uma jogada dispon_vel para o jogador com o tabuleiro atual.
	 */
	private final boolean hasSequence() {
		for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
			for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
				if ( hasSequenceAt( c, r ) || hasMoveAt( c, r ) ) {
					return true;
				}
			}
		}
		return false;
	}


	private final boolean hasMoveAt( int column, int row ) {
		return hasSequenceAt( column, row, -1, 0 ) || hasSequenceAt( column, row, 1, 0 ) || hasSequenceAt( column, row, 0, -1 ) || hasSequenceAt( column, row, 0, 1 );
	}


	private final Fruit getFruit( Point cell ) {
		return getFruit( cell.x, cell.y );
	}


	private final Fruit getFruit( int column, int row ) {
		return fruits[ column ][ row ];
	}


	private final boolean areFruitsIdle() {
		for ( byte c = 0; c < TOTAL_COLUMNS; ++c ) {
			for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
				if ( !getFruit( c, r ).isIdle() )
					return false;
			}
		}

		return true;
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if ( id == HINT_ID ) {
			setHintVisible( false );
			return;
		}
		//#if JAR != "min"
			else if ( liquify != null && id >= LIQUIFY_FIRST_ID ) {
				mutexLiquify.acquire();
				liquify[ id - LIQUIFY_FIRST_ID ].idle();
				mutexLiquify.release();

				return;
			} else if ( explosions != null && id >= EXPLOSION_FIRST_ID ) {
				mutexExplosion.acquire();
				explosions[ id - EXPLOSION_FIRST_ID ].idle();
				mutexExplosion.release();
				
				return;
			}
		//#endif
		
		final Point fruitCell = new Point( id % TOTAL_COLUMNS, id / TOTAL_COLUMNS );

		final Fruit fruit = getFruit( fruitCell );
		// desconta o offset do tipo de fruta no _ndice da sequ_ncia
		final byte fruitSequence = ( byte ) ( sequence - fruit.getType() );
		switch ( fruitSequence ) {
			case Fruit.SEQUENCE_EXPLODING:
			case Fruit.SEQUENCE_LIQUIFY:
				fruit.setState( Fruit.STATE_INVISIBLE );

				// retorna se ainda houver frutas na coluna atual se liquefazendo ou explodindo
				for ( byte r = 0; r < TOTAL_ROWS; ++r ) {
					switch ( getFruit( fruitCell.x, r ).getState() ) {
						case Fruit.STATE_EXPLODING:
						case Fruit.STATE_LIQUIFYING_SOAP_1:
						case Fruit.STATE_LIQUIFYING_SOAP_2:
						case Fruit.STATE_LIQUIFYING_VANISH:
						return;
					}
				}
				
				// faz todas as frutas acima cairem
				int offset = -1;
				int yOffset = -NanoMath.randInt( fruit.getHeight() >> 1 );
				for ( int r1 = 0; r1 < TOTAL_ROWS; ++r1 ) {
					final Fruit f1 = getFruit( fruitCell.x, r1 );
					switch ( f1.getState() ) {
						case Fruit.STATE_INVISIBLE:
							for ( int r2 = r1; r2 > 0; --r2 ) {
								fruits[ fruitCell.x ][ r2 ] = fruits[ fruitCell.x ][ r2 - 1 ];
								if ( fruitSequence == Fruit.SEQUENCE_LIQUIFY )
									fruits[ fruitCell.x ][ r2 - 1 ].moveToCell( fruitCell.x, r2, Fruit.STATE_FALLING );
								else
									fruits[ fruitCell.x ][ r2 - 1 ].moveToCell( fruitCell.x, r2, Fruit.STATE_FALLING, getFruit( fruitCell.x, r2 - 1 ).getInitialYSpeed() + f1.getRandomDiffYSpeed() );
							}

							// a fruta atual _ reposicionada no topo das anteriores
							fruits[ fruitCell.x ][ 0 ] = f1;
							f1.positionByCell( fruitCell.x, offset, yOffset );
							f1.setType( Fruit.TYPE_RANDOM );

							--offset;
							yOffset -= NanoMath.randInt( ( f1.getHeight() * 3 ) >> 1 );
							if ( fruitSequence == Fruit.SEQUENCE_LIQUIFY )
								f1.moveToCell( fruitCell.x, 0, Fruit.STATE_FALLING );
							else
								f1.moveToCell( fruitCell.x, 0, Fruit.STATE_FALLING, getFruit( fruitCell.x, r1 ).getInitialYSpeed() + f1.getRandomDiffYSpeed() );
						break;
					}
				}
			break;

			case Fruit.SEQUENCE_MOVING:
				if ( areFruitsIdle() ) {
					final Fruit[][] sequence1 = getSequencesAt( lastCellMoved.x, lastCellMoved.y );
					if ( sequence1.length > 0 )
						makeSequence( sequence1[ 0 ] );

					final Fruit[][] sequence2 = getSequencesAt( lastCellMoved.x + lastMove.x, lastCellMoved.y + lastMove.y );
					if ( sequence2.length > 0 )
						makeSequence( sequence2[ 0 ] );

					if ( cursorState == CURSOR_STATE_BLOCKED && sequence1.length <= 0 && sequence2.length <= 0 )
						undoLastMove();
					else if ( cursorState == CURSOR_STATE_UNDOING )
						setCursorState( CURSOR_STATE_AVAILABLE );
				}
			break;

			case Fruit.SEQUENCE_FALLING:
				if ( areFruitsIdle() ) {
					final Fruit[][] sequences = getAllSequences();

					if ( sequences.length > 0 ) {
						for ( byte i = 0; i < sequences.length; ++i )
							makeSequence( sequences[ i ] );
					} else {
						setCursorState( CURSOR_STATE_AVAILABLE );
					}
				}
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	private final void undoLastMove() {
		switchFruits( lastCellMoved, lastCellMoved.add( lastMove ) );
		setCursorState( CURSOR_STATE_UNDOING );
	}


	private final boolean setHintVisible( boolean visible ) {
		if ( visible ) {
			if ( cursorState == CURSOR_STATE_AVAILABLE && !hint.isVisible() ) {
				hint.setVisible( true );
				hint.setSequence( HINT_SEQUENCE_ANIMATING );
				
				final int initialColumn = NanoMath.randInt( TOTAL_COLUMNS );
				final int initialRow = NanoMath.randInt( TOTAL_ROWS );
				boolean traverseColumn = true;
				// mostra uma dica aleat_ria
				for ( int c = initialColumn; c != initialColumn || traverseColumn; c = ( c + 1 ) % TOTAL_COLUMNS ) {
					boolean traverseRow = true;
					for ( int r = initialRow; r != initialRow || traverseRow; r = ( r + 1 ) % TOTAL_ROWS ) {
						if ( hasMoveAt( c, r ) ) {
							hint.setRefPixelPosition( fruitGroup.getPosition().add( getFruit( c, r ).getRefPixelPosition() ) );
							return true;
						}
						traverseRow = false;
					}
					traverseColumn = false;
				}
			}
		} else {
			hint.setVisible( false );
			hint.setSequence( HINT_SEQUENCE_IDLE );

			timeToHint = TIME_HINT_INITIAL + ( TIME_HINT_FINAL - TIME_HINT_INITIAL ) * ( LEVEL_MAX - Math.max( level, LEVEL_MAX ) ) / LEVEL_MAX;
		}
		return false;
	}


	private final void makeSequence( Fruit[] sequence ) {
		int specialIndex = -1;
		int secondIndex = -1;
		byte specialType = -1;

		boolean liquifyInPlace = false;
		final Point destination = new Point();
		switch ( sequence.length ) {
			case 4:
			case 5:
			case 6:
				final Point cell1 = sequence[ 0 ].getCell();
				final Point cell2 = sequence[ sequence.length - 1 ].getCell();

				if ( cell1.x == cell2.x ) {
					// vertical
					specialIndex = Math.abs( cell1.y - ( TOTAL_ROWS >> 1 ) ) < Math.abs( cell2.y - ( TOTAL_ROWS >> 1 ) ) ? 0 : sequence.length - 1;
				} else {
					// horizontal
					specialIndex = Math.abs( cell1.x - ( TOTAL_COLUMNS >> 1 ) ) < Math.abs( cell2.x - ( TOTAL_COLUMNS >> 1 ) ) ? 0 : sequence.length - 1;
				}

				switch ( sequence[ specialIndex ].getState() ) {
					case Fruit.STATE_BOMB:
					case Fruit.STATE_EXPLODING:
						for ( byte i = 0; i < sequence.length; ++i ) {
							switch ( sequence[ i ].getState() ) {
								case Fruit.STATE_BOMB:
								case Fruit.STATE_EXPLODING:
								break;

								default:
									specialIndex = i;
									i = ( byte ) sequence.length;
							}
						}
					break;
				}

				specialType = Fruit.STATE_BOMB;
				MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT * ( sequence.length - 3 ) );
				destination.set( sequence[ 1 ].getPosition().add( sequence[ sequence.length - 1 ].getPosition() ).div( 2 ) );
			break;

			default:
				// faz uma pr_-an_lise da sequ_ncia, para detectar explos_es e evitar frutas sendo "sugadas" para dentro da explos_o
				for ( byte i = 0; i < sequence.length; ++i ) {
					switch ( sequence[ i ].getState() ) {
						case Fruit.STATE_BOMB:
						case Fruit.STATE_EXPLODING:
							i = ( byte ) sequence.length;
							liquifyInPlace = true;
						break;
					}
				}
				if ( !liquifyInPlace )
					destination.set( sequence[ 0 ].getPosition().add( sequence[ sequence.length - 1 ].getPosition() ).div( 2 ) );
		}
		secondIndex = ( specialIndex == 0 ) ? 1 : specialIndex - 1;

		//#if JAR != "min"
		if ( !liquifyInPlace ) {
			if ( liquify != null ) {
				final SpriteGroup liquifySprite = getNextLiquify();
				liquifySprite.setRefPixelPosition( destination.x + fruitGroup.getPosX() + ( sequence[ 0 ].getWidth() >> 1 ),
												   destination.y + fruitGroup.getPosY() );
				liquifySprite.animate( sequence[ 0 ].getType() );
			}
		}
		//#endif

		for ( byte i = 0; i < sequence.length; ++i ) {
			switch ( sequence[ i ].getState() ) {
				case Fruit.STATE_EXPLODING:
				break;

				case Fruit.STATE_BOMB:
					explodeFruitArea( sequence[ i ] );
				break;

				default:
					if ( i == specialIndex ) {
						sequence[ i ].setState( specialType );
					} else {
						if ( liquifyInPlace ) {
							destination.set( sequence[ i ].getPosition() );

							//#if JAR != "min"
								if ( liquify != null ) {
									final SpriteGroup liquifySprite = getNextLiquify();
									liquifySprite.setRefPixelPosition( destination.x + fruitGroup.getPosX() + ( sequence[ i ].getWidth() >> 1 ),
																	   destination.y + fruitGroup.getPosY() );
									liquifySprite.animate( sequence[ i ].getType() );
								}
							//#endif
						}
						sequence[ i ].liquify( destination, liquifyInPlace || ( i == secondIndex ) );
					}
				// fim default
			}
		}

		final int sequenceScore = comboCounter * ( SCORE_SEQUENCE + ( sequence.length - 3 ) * SCORE_EXTRA_FRUIT );
		getNextScoreLabel().setScore( sequence[ sequence.length >> 1 ].getCell(), sequenceScore );

//		System.out.println ( "SEQUENCE: "+ sequence.length + " x " + comboCounter + ": " + sequenceScore );
		++comboCounter;
		fruitCounter += sequence.length;

		changeTime( sequence.length * currentTimePerFruit );

		setHintVisible( false );
	}


	private final void explodeFruitArea( Fruit fruit ) {
		final Point cell = fruit.getCell();
		final Point limitMin = new Point( Math.max( 0, cell.x - 1 ), Math.max( 0, cell.y - 1 ) );
		final Point limitMax = new Point( Math.min( cell.x + 1, TOTAL_COLUMNS - 1 ), Math.min( cell.y + 1, TOTAL_ROWS - 1 ) );

		MediaPlayer.vibrate( VIBRATION_TIME_EXPLOSION );
		timeScreenVibrating = VIBRATION_TIME_EXPLOSION;

		explodeFruit( fruit );
		++comboCounter;
		for ( int c = limitMin.x; c <= limitMax.x; ++c ) {
			for ( int r = limitMin.y; r <= limitMax.y; ++r ) {
				final Fruit f = getFruit( c, r );
				if ( f != fruit ) {
					if ( f.getState() == Fruit.STATE_BOMB )
						explodeFruitArea( f );
					else
						explodeFruit( f );
				}
			}
		}
	}


	private final void explodeFruit( Fruit fruit ) {
		++fruitCounter;
		fruit.setState( Fruit.STATE_EXPLODING );
		getNextScoreLabel().setScore( fruit.getCell(), SCORE_SEQUENCE * comboCounter );

		//#if JAR != "min"
			if ( explosions != null ) {
				final SpriteGroup explosion = getNextExplosion();
				explosion.animate( fruit.getType() );
				explosion.setRefPixelPosition( fruit.getRefPixelPosition().add( fruitGroup.getPosition() ) );
			}
		//#endif
		emitter.emit( fruit.getType(), fruitGroup.getPosition().add( fruit.getRefPixelPosition() ) );

		changeTime( currentTimePerFruit );

		// faz todas as frutas acima pular
		final Point fruitCell = fruit.getCell();
		int initialYSpeed = fruit.getRandomInitialYSpeed();
		for ( int r1 = fruitCell.y - 1; r1 >= 0; --r1 ) {
			final Fruit f1 = getFruit( fruitCell.x, r1 );
			if ( f1.isIdle() ) {
				f1.moveToCell( f1.getCell().x, r1 + 1, Fruit.STATE_FALLING, initialYSpeed );
				initialYSpeed += f1.getRandomDiffYSpeed();
			}
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		//#if SCREEN_SIZE == "SMALL"
//# 			// evita problemas com aparelhos com tela menor que 128 pixels (Motorola V220)
//# 			final boolean landscape = height < HEIGHT_MIN && ( width - height > 20 );
		//#else
			final boolean landscape = ( height < HEIGHT_MIN || width > 400 ) && width > height;
		//#endif

		if ( landscape ) {
			if ( width > 400 )
				fruitGroup.setPosition( ( ScreenManager.SCREEN_WIDTH - fruitGroup.getWidth() - ( BORDER_THICKNESS << 1 ) ) >> 1, ScreenManager.SCREEN_HALF_HEIGHT - ( fruitGroup.getHeight() >> 1 ) );
			else
				fruitGroup.setPosition( ScreenManager.SCREEN_WIDTH - fruitGroup.getWidth() - ( BORDER_THICKNESS << 1 ), ScreenManager.SCREEN_HALF_HEIGHT - ( fruitGroup.getHeight() >> 1 ) );
		} else {
			//#if SCREEN_SIZE == "SMALL"
//# 				if ( height < HEIGHT_MIN ) {
//# 					fruitGroup.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( fruitGroup.getWidth() >> 1 ), 0 );
//# 				} else if ( height == HEIGHT_MIN ) {
//# 					fruitGroup.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( fruitGroup.getWidth() >> 1 ), scoreBorderLeft.getHeight() );
//# 				} else {
//#  					fruitGroup.setPosition( ScreenManager.SCREEN_HALF_WIDTH - ( fruitGroup.getWidth() >> 1 ), ( height - fruitGroup.getHeight() ) >> 1 );
//# 				}
			//#else
				if (height < MIN_HEIGHT_PORTRAIT_LUX) {
					fruitGroup.setPosition(ScreenManager.SCREEN_HALF_WIDTH - (fruitGroup.getWidth() >> 1), scoreBorderLeft.getHeight() + (BORDER_THICKNESS << 1));
				} else if (ScreenManager.SCREEN_HEIGHT > 400) {
					fruitGroup.setPosition(ScreenManager.SCREEN_HALF_WIDTH - (fruitGroup.getWidth() >> 1), height/2 - fruitGroup.getHeight()/2);
				} else {
					fruitGroup.setPosition(ScreenManager.SCREEN_HALF_WIDTH - (fruitGroup.getWidth() >> 1), ((height - HEIGHT_DEFAULT) >> 2) + HEIGHT_DEFAULT + BOARD_DEFAULT_BOTTOM_Y - fruitGroup.getHeight());
				}
			//#endif
		}

		Fruit.FRUIT_GROUP_POS = fruitGroup.getPosition();

		border.setPosition( fruitGroup.getPosition().sub( BORDER_THICKNESS, BORDER_THICKNESS ) );
		border.setSize( fruitGroup.getSize().add( BORDER_THICKNESS << 1, BORDER_THICKNESS << 1 ) );

		//#if SCREEN_SIZE == "BIG" 
			if ( landscape ) {
				if ( fruitGroup.getPosX() > lux.getWidth() + BORDER_THICKNESS ) {
					lux.setPosition( border.getPosX() - lux.getWidth() - BORDER_THICKNESS, border.getPosY() );
					lux.setVisible( true );
				} else {
					lux.setVisible( false );
				}
			} else {
				if ( fruitGroup.getPosY() > lux.getHeight() + BORDER_THICKNESS ) {
					lux.setPosition( border.getPosX(), border.getPosY() - lux.getHeight() - BORDER_THICKNESS );
					lux.setVisible( true );
				} else {
					lux.setVisible( false );
				}
			}

			final boolean useLux = lux.isVisible();

			if ( landscape ) {
				scoreBorderRight.setPosition( border.getPosX() - scoreBorderRight.getWidth() - BORDER_THICKNESS, GAME_INFO_SPACING + ( useLux ? lux.getPosY() + lux.getHeight() : border.getPosY() ) );
			} else {
				if ( height < MIN_HEIGHT_PORTRAIT_LUX ) {
					scoreBorderRight.setPosition( border.getPosX() + scoreBorderLeft.getWidth() + scoreBorderRight.getWidth() + SCORE_PATTERN_WIDTH,
												  BORDER_HALF_THICKNESS );
				} else {
					scoreBorderRight.setPosition( border.getPosX() + border.getWidth() - scoreBorderRight.getWidth(),
												  useLux ? lux.getPosY() : border.getPosY() - ( BORDER_THICKNESS << 1 ) - hearts[ 0 ].getHeight() - scoreBorderRight.getHeight() );
				}
			}
		//#else
//# 			if ( landscape ) {
//# 				scoreBorderRight.setPosition( border.getPosX() - scoreBorderRight.getWidth() - BORDER_THICKNESS, GAME_INFO_SPACING + border.getPosY() );
//# 			} else {
			//#if SCREEN_SIZE == "SMALL"
//# 					if ( height <= HEIGHT_MIN ) {
//# 						scoreBorderRight.setPosition( border.getPosX() + scoreBorderLeft.getWidth() + scoreBorderRight.getWidth() + SCORE_PATTERN_WIDTH, 0 );
//# 					} else
			//#endif
//# 				scoreBorderRight.setPosition( border.getPosX() + scoreBorderLeft.getWidth() + scoreBorderRight.getWidth() + SCORE_PATTERN_WIDTH,
//# 											  Math.max( 0, border.getPosY() - scoreBorderRight.getHeight() - BORDER_THICKNESS ) );
//# 			}
		//#endif

		final short currentScoreWidth = ( short ) ( landscape ? Math.min( scoreBorderRight.getPosX() - scoreBorderLeft.getWidth() - BORDER_HALF_THICKNESS, SCORE_PATTERN_WIDTH ) : SCORE_PATTERN_WIDTH );
		scoreBorderFill.setPosition( scoreBorderRight.getPosX() - currentScoreWidth, scoreBorderRight.getPosY() );
		scoreBorderFill.setSize( currentScoreWidth, scoreBorderFill.getHeight() );
		scoreBorderLeft.setPosition( scoreBorderFill.getPosX() - scoreBorderLeft.getWidth(), scoreBorderRight.getPosY() );


		//#if SCREEN_SIZE == "BIG"
			if ( landscape || height >= MIN_HEIGHT_PORTRAIT_LUX ) {
				if ( hintButton == null ) {
					for ( byte i = 0; i < LIVES_MAX; ++i )
						hearts[ i ].setPosition( scoreBorderRight.getPosX() + scoreBorderRight.getWidth() - hearts[ i ].getWidth() * ( LIVES_MAX - i ), scoreBorderFill.getPosY() + scoreBorderFill.getHeight() + BORDER_THICKNESS );
				} else {
					hintButton.setPosition( scoreBorderLeft.getPosX(), scoreBorderFill.getPosY() + scoreBorderFill.getHeight() + BORDER_THICKNESS );
					for ( byte i = 0; i < LIVES_MAX; ++i )
						hearts[ i ].setPosition( scoreBorderRight.getPosX() + scoreBorderRight.getWidth() - hearts[ i ].getWidth() * ( LIVES_MAX - i ), hintButton.getPosY() + ( ( hintButton.getHeight() - hearts[ i ].getHeight() ) >> 1 ) );
				}
			} else {
				for ( byte i = 0; i < LIVES_MAX; ++i )
					hearts[ i ].setPosition( fruitGroup.getPosX() + fruitGroup.getWidth() - hearts[ i ].getWidth() * ( LIVES_MAX - i ), scoreBorderLeft.getPosY() + ( ( scoreBorderLeft.getHeight() - hearts[ i ].getHeight() >> 1 ) ) );
			}

			if ( landscape ) {
				timeBar.setSize( border.getPosX() - BORDER_THICKNESS - scoreBorderLeft.getPosX(), timeBar.getHeight() );

				clock.setPosition( scoreBorderLeft.getPosX(), hearts[ 0 ].getPosY() + hearts[ 0 ].getHeight() + GAME_INFO_SPACING );
				timeBar.setPosition( scoreBorderLeft.getPosX(), clock.getPosY() + clock.getHeight() + BORDER_HALF_THICKNESS );
			} else {
				if ( hintButton != null ) {
					hintButton.setPosition( fruitGroup.getPosX(), NanoMath.min( fruitGroup.getPosY() + fruitGroup.getHeight() + TIMEBAR_SPACING, height - hintButton.getHeight() ) );
					clock.setPosition( hintButton.getPosX() + hintButton.getWidth() + ENTRIES_SPACING,
									   hintButton.getPosY() + ( ( hintButton.getHeight() - clock.getHeight() ) >> 1 ) );
				} else {
					clock.setPosition( fruitGroup.getPosX(), NanoMath.min( fruitGroup.getPosY() + fruitGroup.getHeight() + TIMEBAR_SPACING, height - timeBar.getHeight() ) );
				}

				timeBar.setPosition( clock.getPosX() + clock.getWidth() + 1, clock.getPosY() + CLOCK_Y_OFFSET );
				timeBar.setSize( fruitGroup.getPosX() + fruitGroup.getWidth() - timeBar.getPosX() - 1, timeBar.getHeight() );
			}
		//#else
//# 			if ( landscape ) {
//# 				for ( byte i = 0; i < LIVES_MAX; ++i )
//# 					hearts[ i ].setPosition( scoreBorderRight.getPosX() + scoreBorderRight.getWidth() - hearts[ i ].getWidth() * ( LIVES_MAX - i ), scoreBorderFill.getPosY() + scoreBorderFill.getHeight() + BORDER_THICKNESS );
//# 			} else {
//# 				for ( byte i = 0; i < LIVES_MAX; ++i )
//# 					hearts[ i ].setPosition( fruitGroup.getPosX() + fruitGroup.getWidth() - hearts[ i ].getWidth() * ( LIVES_MAX - i ), scoreBorderLeft.getPosY() + ( ( scoreBorderLeft.getHeight() - hearts[ i ].getHeight() >> 1 ) ) );
//# 			}
//#
			//#if SCREEN_SIZE == "SMALL"
//# 				if ( height <= HEIGHT_MIN ) {
//# 					clock.setVisible( false );
//# 					timeBar.setPosition( fruitGroup.getPosX(), height - timeBar.getHeight() );
//# 					timeBar.setSize( fruitGroup.getPosX() + fruitGroup.getWidth() - timeBar.getPosX() - 1, timeBar.getHeight() );
//# 				} else
			//#endif
//# 				if ( landscape ) {
//# 					timeBar.setSize( border.getPosX() - BORDER_THICKNESS - scoreBorderLeft.getPosX(), timeBar.getHeight() );
//#
//# 					clock.setPosition( scoreBorderLeft.getPosX(), hearts[ 0 ].getPosY() + hearts[ 0 ].getHeight() + GAME_INFO_SPACING );
//# 					timeBar.setPosition( scoreBorderLeft.getPosX(), clock.getPosY() + clock.getHeight() + BORDER_HALF_THICKNESS );
//# 				} else {
//#  					clock.setPosition( fruitGroup.getPosX(), NanoMath.min( fruitGroup.getPosY() + fruitGroup.getHeight() + TIMEBAR_SPACING, height - clock.getHeight() ) );
//# 					timeBar.setPosition( clock.getPosX() + clock.getWidth() + 1, clock.getPosY() + CLOCK_Y_OFFSET );
//# 					timeBar.setSize( fruitGroup.getPosX() + fruitGroup.getWidth() - timeBar.getPosX() - 1, timeBar.getHeight() );
//# 				}
		//#endif

		// insere o label indicando a pontua__o
		scoreLabelX = ( short ) scoreBorderRight.getPosX();
		scoreLabel.setPosition( scoreLabelX - scoreLabel.getWidth(), scoreBorderRight.getPosY() + ( ( scoreBorderRight.getHeight() - scoreLabel.getHeight() ) >> 1 ) );

		setCursorCell( cursorCell );


		labelMessage.setSize( width, labelMessage.getTextTotalHeight() );
		labelMessage.setPosition( 0, ( height - labelMessage.getHeight() ) >> 1 );

		// posiciona os patterns do fundo de forma a preencher toda a _rea sem ter que repintar desnecessariamente o tabuleiro
		bkgTop.setSize( getWidth(), fruitGroup.getPosY() + BORDER_HALF_THICKNESS );
		bkgLeft.setPosition( 0, bkgTop.getHeight() );
		bkgLeft.setSize( border.getPosX() + BORDER_HALF_THICKNESS, border.getHeight() - BORDER_THICKNESS - BORDER_HALF_THICKNESS );
		bkgRight.setPosition( fruitGroup.getPosX() + fruitGroup.getWidth() - BORDER_HALF_THICKNESS, bkgTop.getHeight() );
		bkgRight.setSize( getWidth() - bkgRight.getPosX(), bkgLeft.getHeight() );
		bkgBottom.setPosition( 0, bkgLeft.getPosY() + bkgLeft.getHeight() );
		bkgBottom.setSize( getWidth(), getHeight() - bkgBottom.getPosY() );
	}


	public final void hideNotify( boolean deviceEvent ) {
		if ( state == STATE_PLAYING )
			setState( STATE_PAUSED );
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != size.x || height != size.y )
			setSize( width, height );
	}


	private static final class ScoreLabel extends Label implements Updatable {

		private static final short SPEED_MIN = ( short ) -( ScreenManager.SCREEN_HEIGHT >> 4 );

		private static final short SPEED_MAX_DIFF = ( short ) ( ScreenManager.SCREEN_HEIGHT >> 3 );

		private static final short LIFE_MIN			= ( short ) ( GameMIDlet.isLowMemory() ? 500 : 1000 );

		private static final short LIFE_MAX_DIFF	= ( short ) ( GameMIDlet.isLowMemory() ? 1000 : 1700 );

		private short lifeTime;

		private final MUV muv = new MUV();

		private static final byte STATE_IDLE		= 0;
		private static final byte STATE_APPEARING	= 1;
		private static final byte STATE_VISIBLE		= 2;
		private static final byte STATE_VANISHING	= 3;

		private byte state;

		/** Tempo em milisegundos da anima__o de apari__o/sumi_o. */
		private static final short APPEARING_TIME	= 210;

		private final PlayScreen playScreen;


		public ScoreLabel( PlayScreen screen ) throws Exception {
			super( GameMIDlet.getFont( FONT_POINTS ), null );

			playScreen = screen;

			setViewport( new Rectangle() );

			setState( STATE_IDLE );
		}


		public final void update( int delta ) {
			switch ( state ) {
				case STATE_APPEARING:
					lifeTime += delta;
					
					if ( lifeTime < APPEARING_TIME ) {
						final int currentWidth = getWidth() * lifeTime / APPEARING_TIME;
						final int currentHeight = getHeight() * lifeTime / APPEARING_TIME;
						viewport.set( getRefPixelX() - ( currentWidth >> 1 ), getRefPixelY() - ( currentHeight >> 1 ), currentWidth, currentHeight );
					} else {
						setState( STATE_VISIBLE );
					}
				break;

				case STATE_VISIBLE:
					lifeTime -= delta;

					if ( lifeTime > 0 ) {
						move( 0, muv.updateInt( delta ) );
					} else {
						setState( STATE_VANISHING );
					}
				break;

				case STATE_VANISHING:
					lifeTime -= delta;

					if ( lifeTime > 0 ) {
						final int currentWidth = getWidth() * lifeTime / APPEARING_TIME;
						final int currentHeight = getHeight() * lifeTime / APPEARING_TIME;
						viewport.set( getRefPixelX() - ( currentWidth >> 1 ), getRefPixelY() - ( currentHeight >> 1 ), currentWidth, currentHeight );
					} else {
						setState( STATE_IDLE );
					}
				break;
			}
		}


		public final void setScore( Point cell, int score ) {
			setText( String.valueOf( score ) );
			defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

			playScreen.changeScore( score );
			
			final Point pos = playScreen.fruitGroup.getPosition().add( playScreen.getFruit( cell ).getRefPixelPosition() );
			setRefPixelPosition( pos );
			setState( STATE_APPEARING );
		}


		private final void setState( byte state ) {
			switch ( state ) {
				case STATE_IDLE:
					setVisible( false );
				break;

				case STATE_APPEARING:
					setVisible( true );
					muv.setSpeed( SPEED_MIN - NanoMath.randInt( SPEED_MAX_DIFF ) );
					lifeTime = 0;
					viewport.set( 0, 0, 0, 0 );
				break;

				case STATE_VISIBLE:
					lifeTime = ( short ) ( LIFE_MIN + NanoMath.randInt( LIFE_MAX_DIFF ) );
					viewport.set( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
				break;

				case STATE_VANISHING:
					lifeTime = APPEARING_TIME;
				break;
			}
			
			this.state = state;
		}

	} // fim da classe interna ScoreLabel


	private static final class SpriteGroup extends UpdatableGroup {
		
		private byte type = -1;

		public SpriteGroup() throws Exception {
			super( Fruit.TOTAL_TYPES );
		}


		public final void idle() {
			setVisible( false );
			type = -1;
		}


		public final boolean isIdle() {
			return type == -1;
		}
		

		public final void animate( int type ) {
			this.type = ( byte ) type;
			setVisible( true );
		}


		public final void update( int delta ) {
			if ( type >= 0 )
				( ( Sprite ) getDrawable( type ) ).update( delta );
		}


		protected final void paint( Graphics g ) {
			if ( type >= 0 )
				getDrawable( type ).draw( g );
		}
	}

}
