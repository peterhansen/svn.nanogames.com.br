/**
 * Fruit.java
 * 
 * Created on 14/Fev/2009, 12:55:56
 *
 */

package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
import screens.PlayScreen;

/**
 *
 * @author Peter
 */
public final class Fruit extends UpdatableGroup implements Constants {

	public static final byte ORIGINAL_FRUIT = 4;

	private static final int[][] FRUIT_COLORS =
		{	{ 0xfc7f7f, 0xfbebeb, 0xfba8a8 },
			{ 0xf2ded6, 0xdcb2a1, 0xa97763 },
			{ 0xffffff, 0xdedede, 0xb7b7b7 },
			{ 0xfdd99d, 0xfcf0db, 0xf1992c },
			{ 0xfcf5e6, 0xf9dea8, 0xe2ab41 } };


	private static final short LIQUIFY_TIME = 358;

	private static final short LIQUIFY_MOVE_TIME = 303;

	/***/
	public static final byte TOTAL_TYPES = 5;
	
	public static final byte TYPE_RANDOM = -1;

	private byte type;
	
	private static Sprite[] SPRITES_FRUITS;

	private static Sprite SPRITE_SHINE;

	public static final byte STATE_IDLE					= 0;
	public static final byte STATE_FALLING				= 1;
	public static final byte STATE_SWITCHING_H			= 2;
	public static final byte STATE_SWITCHING_V			= 3;
	public static final byte STATE_LIQUIFYING_VANISH	= 4;
	public static final byte STATE_LIQUIFYING_SOAP_1	= 5;
	public static final byte STATE_LIQUIFYING_SOAP_2	= 6;
	public static final byte STATE_INVISIBLE			= 7;
	public static final byte STATE_BOMB					= 8;
	public static final byte STATE_EXPLODING			= 9;
	public static final byte STATE_FALLING_BOMB			= 10;

	private byte state;

	private final short GRAVITY_ACC;

	/** �ndice da sequ�ncia de anima��o do 1� tipo de fruta. */
	public static final byte SEQUENCE_IDLE		= 0;

	/** �ndice da sequ�ncia da fruta brilhando. */
	public static final byte SEQUENCE_SHINING	= SEQUENCE_IDLE + TOTAL_TYPES;
	
	public static final byte SEQUENCE_EXPLODING	= SEQUENCE_SHINING + TOTAL_TYPES;

	/** �ndice da sequ�ncia de liquefa��o do 1� tipo de fruta. */
	public static final byte SEQUENCE_LIQUIFY	= SEQUENCE_EXPLODING + TOTAL_TYPES;

	/** �ndice de sequ�ncia passado ao PlayScreen ao terminar de mover uma fruta. */
	public static final byte SEQUENCE_MOVING	= SEQUENCE_LIQUIFY + TOTAL_TYPES;

	/** �ndice de sequ�ncia passado ao PlayScreen ao terminar um movimento de queda. */
	public static final byte SEQUENCE_FALLING	= SEQUENCE_MOVING + TOTAL_TYPES;
	public static final byte SEQUENCE_FALLING_BOMB	= SEQUENCE_FALLING + TOTAL_TYPES;

	/** Posi��o de destino da fruta. */
	private final Point destination = new Point();

	private final MUV muv = new MUV();

	private short yStart;

	private short initialYSpeed;

	private short moveTime;

	private SimpleSprite spriteFruit;

	private final SimpleSprite spriteShine;

	public static Point FRUIT_GROUP_POS;
	
	
	public static final void load() throws Exception {
		if ( SPRITES_FRUITS == null ) {
			GameMIDlet.log( 14 );
			SPRITES_FRUITS = new Sprite[ TOTAL_TYPES ];
			GameMIDlet.log( 15 );

			//#if SCREEN_SIZE == "BIG"
				if ( ScreenManager.SCREEN_WIDTH > 400 || ScreenManager.SCREEN_HEIGHT > 400 )
					SPRITES_FRUITS[ 0 ] = new Sprite( PATH_FRUITS_GIANT_DESCRIPTOR, PATH_FRUITS_PREFIX );
				else
					SPRITES_FRUITS[ 0 ] = new Sprite( PATH_FRUITS_DESCRIPTOR, PATH_FRUITS_PREFIX );
			//#else
//# 				SPRITES_FRUITS[ 0 ] = new Sprite( GameMIDlet.isLowMemory() ? PATH_FRUITS_DESCRIPTOR_LOW : PATH_FRUITS_DESCRIPTOR, PATH_FRUITS_PREFIX );
			//#endif

			GameMIDlet.log( 16 );
			for ( byte i = 1; i < TOTAL_TYPES; ++i )
				SPRITES_FRUITS[ i ] = new Sprite( SPRITES_FRUITS[ 0 ] );
			GameMIDlet.log( 17 );
		}

		if ( SPRITE_SHINE == null )
			SPRITE_SHINE = new Sprite( PATH_SHINE_PREFIX );
			GameMIDlet.log( 18 );
	}


	public static final PaletteMap[] getPaletteMap( int index ) {
		if ( GameMIDlet.isLowMemory() )
			return null;
		
		final PaletteMap[] maps = new PaletteMap[ FRUIT_COLORS[ index ].length ];
		for ( byte i = 0; i < maps.length; ++i )
			maps[ i ] = new PaletteMap( FRUIT_COLORS[ ORIGINAL_FRUIT ][ i ], FRUIT_COLORS[ index ][ i ] );
		
		return maps;
	}
	
	
	public Fruit( int type ) throws Exception {
		super( TOTAL_TYPES + 1 );

		for( byte i = 0; i < TOTAL_TYPES; ++i ) {
			final SimpleSprite s = new SimpleSprite( SPRITES_FRUITS[ i ] );
			s.setVisible( false );
			insertDrawable( s );
		}
		spriteFruit = ( SimpleSprite ) getDrawable( 0 );
		setSize( spriteFruit.getSize() );

		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		spriteShine = new SimpleSprite( SPRITE_SHINE );
		spriteShine.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		spriteShine.setRefPixelPosition( size.x >> 1, size.y >> 1 );
		insertDrawable( spriteShine );
		
		GRAVITY_ACC = ( short ) ( 10 * getHeight() );
		
		setType( ( byte ) type);
	}


	public final void setListener( SpriteListener listener ) {
		for ( byte i = 0; i < TOTAL_TYPES; ++i ) {
			( ( SimpleSprite ) getDrawable( i ) ).setListener( listener, 0 );
		}
	}
	
	
	public final void setType( byte type ) {
		if ( type == TYPE_RANDOM )
			type = ( byte ) NanoMath.randInt( TOTAL_TYPES );

		this.type = type;
		spriteFruit.setVisible( false );
		spriteFruit.setSequence( SEQUENCE_IDLE );
		setShining( false );

		spriteFruit = ( SimpleSprite ) getDrawable( type );
		spriteFruit.setVisible( true );
		spriteFruit.setSequence( type );
	}


	public final boolean equals( Fruit f ) {
		return f.type == type;
	}


	public final void update( int delta ) {
		super.update( delta );

		switch ( state ) {
			case STATE_SWITCHING_H:
				move( muv.updateInt( delta ), 0 );
				if ( ( muv.getSpeed() > 0 && getPosX() >= destination.x ) || ( muv.getSpeed() < 0 && getPosX() <= destination.x ) ) {
					setPosition( destination.x, getPosY() );
					refreshId();
					idle();
					spriteFruit.getListener().onSequenceEnded( spriteFruit.getId(), SEQUENCE_MOVING + getType() );
				}
			break;

			case STATE_FALLING_BOMB:
			case STATE_FALLING:
				moveTime += delta;
				setPosition( getPosX(), yStart + ( initialYSpeed * moveTime / 1000 ) + ( GRAVITY_ACC * ( moveTime * moveTime ) / 2000000 ) );
				if ( getPosY() > destination.y ) {
					setPosition( getPosX(), destination.y );
					refreshId();

					if ( state == STATE_FALLING )
						idle();
					else
						setState( STATE_BOMB );
					
					spriteFruit.getListener().onSequenceEnded( spriteFruit.getId(), SEQUENCE_FALLING + getType() );
				}
			break;
			
			case STATE_SWITCHING_V:
				move( 0, muv.updateInt( delta ) );
				if ( ( muv.getSpeed() > 0 && getPosY() >= destination.y ) || ( muv.getSpeed() < 0 && getPosY() <= destination.y ) ) {
					setPosition( getPosX(), destination.y );
					refreshId();

					idle();
					spriteFruit.getListener().onSequenceEnded( spriteFruit.getId(), SEQUENCE_MOVING + getType() );
				}
			break;

			case STATE_LIQUIFYING_SOAP_1:
			case STATE_LIQUIFYING_VANISH:
				moveTime += delta;
				if ( destination.x == getPosX() ) {
					move( 0, muv.updateInt( delta ) );

					if ( ( muv.getSpeed() > 0 && getPosY() >= destination.y ) || ( muv.getSpeed() < 0 && getPosY() <= destination.y ) || moveTime >= LIQUIFY_MOVE_TIME ) {
						setPosition( destination );
						idle();

						if ( state == STATE_LIQUIFYING_VANISH )
							spriteFruit.getListener().onSequenceEnded( spriteFruit.getId(), SEQUENCE_LIQUIFY + getType() );
						else
							setState( STATE_LIQUIFYING_SOAP_2 );
					}
				} else {
					move( muv.updateInt( delta ), 0 );
					if ( ( muv.getSpeed() > 0 && getPosX() >= destination.x ) || ( muv.getSpeed() < 0 && getPosX() <= destination.x ) || moveTime >= LIQUIFY_MOVE_TIME ) {
						setPosition( destination );
						idle();

						if ( state == STATE_LIQUIFYING_VANISH )
							spriteFruit.getListener().onSequenceEnded( spriteFruit.getId(), SEQUENCE_LIQUIFY + getType() );
						else
							setState( STATE_LIQUIFYING_SOAP_2 );
					}
				}
			break;

			case STATE_LIQUIFYING_SOAP_2:
				moveTime -= delta;

				if ( moveTime > 0 ) {
					viewport.width = size.x * moveTime / LIQUIFY_TIME;
					viewport.x = FRUIT_GROUP_POS.x + destination.x + ( ( size.x - viewport.width ) >> 1 );
					viewport.y = FRUIT_GROUP_POS.y + destination.y + size.y - ( size.y * moveTime / LIQUIFY_TIME );
				} else {
					spriteFruit.getListener().onSequenceEnded( spriteFruit.getId(), SEQUENCE_LIQUIFY + getType() );
				}
			break;
		}
	}


	public final void setState( byte state ) {
		this.state = state;
		setViewport( null );
		switch ( state ) {
			case STATE_EXPLODING:
				spriteFruit.setSequence( SEQUENCE_EXPLODING + type );
			break;

			case STATE_BOMB:
				spriteFruit.setSequence( SEQUENCE_SHINING + type );
				setShining( true );
			break;

			case STATE_LIQUIFYING_SOAP_2:
				setViewport( new Rectangle( FRUIT_GROUP_POS.x + destination.x, FRUIT_GROUP_POS.y + destination.y, size.x, size.y ) );
				moveTime = LIQUIFY_TIME;
			case STATE_LIQUIFYING_SOAP_1:
			case STATE_LIQUIFYING_VANISH:
			default:
				spriteFruit.setSequence( SEQUENCE_IDLE + type );
		}
		
		setVisible( state != STATE_INVISIBLE );
	}


	public final void liquify( Point destination, boolean turnToSoap ) {
		moveTo( destination, turnToSoap ? STATE_LIQUIFYING_SOAP_1 : STATE_LIQUIFYING_VANISH, 0 );
	}


	public final void positionByCell( Point cell ) {
		positionByCell( cell.x, cell.y );
	}


	public final void positionByCell( int column, int row ) {
		positionByCell( column, row, 0 );
	}


	public final void positionByCell( int column, int row, int yOffset ) {
		setPosition( getWidth() * column, ( getHeight() * row ) + yOffset );

		refreshId();
	}


	private final void refreshId() {
		// atualiza o sprite.getId() de acordo com a c�lula atual, pois � ele que permite que a tela de jogo
		// identifique onde a fruta est� no tabuleiro
		spriteFruit.setListener( spriteFruit.getListener(), ( getPosY() / getHeight() * TOTAL_COLUMNS ) + ( getPosX() / getWidth() ) );
	}


	public final Point getCell() {
		return new Point( spriteFruit.getId() % TOTAL_COLUMNS, spriteFruit.getId() / TOTAL_COLUMNS );
	}


	public final void moveToCell( Point cell, byte moveType ) {
		moveToCell( cell.x, cell.y, moveType );
	}


	public final void moveToCell( int column, int row, byte moveType ) {
		moveToCell( column, row, moveType, getHeight() );
	}


	public final void moveToCell( int column, int row, byte moveType, int initialYSpeed ) {
		moveTo( new Point( getWidth() * column, getHeight() * row ), moveType, initialYSpeed );
	}


	private final void moveTo( Point destination, byte moveType, int initialYSpeed ) {
		this.destination.set( destination );
		switch ( moveType ) {
			case STATE_SWITCHING_H:
				muv.setSpeed( ( destination.x - getPosX() ) * 1000 / FRUIT_SWITCH_TIME );
			break;

			case STATE_FALLING_BOMB:
			case STATE_FALLING:
				if ( getState() == STATE_FALLING || getState() == STATE_FALLING_BOMB )
					break;

				moveTime = 0;
				yStart = ( short ) getPosY();
				this.initialYSpeed = ( short ) initialYSpeed;
			break;

			case STATE_SWITCHING_V:
				muv.setSpeed( ( destination.y - getPosY() ) * 1000 / FRUIT_SWITCH_TIME );
			break;

			case STATE_LIQUIFYING_VANISH:
			case STATE_LIQUIFYING_SOAP_1:
				moveTime = 0;
				if ( destination.x == getPosX() )
					muv.setSpeed( ( destination.y - getPosY() ) * 1000 / LIQUIFY_MOVE_TIME );
				else
					muv.setSpeed( ( destination.x - getPosX() ) * 1000 / LIQUIFY_MOVE_TIME );
			break;
		}
		setState( moveType );
	}


	public final byte getState() {
		return state;
	}


	public final byte getType() {
		return type;
	}


	public final void reset( int column, int row, int yOffset ) {
		positionByCell( column, row - TOTAL_ROWS, yOffset );
		moveToCell( column, row, STATE_FALLING );
		setShining( false );
	}


	private final void setShining( boolean active ) {
		spriteShine.setVisible( active );
		spriteShine.setSequence( active ? 1 : 0 );
	}


	public final boolean isIdle() {
		switch ( getState() ) {
			case STATE_IDLE:
			case STATE_BOMB:
			case STATE_INVISIBLE:
			return true;

			default:
				return false;
		}
	}


	public final void idle() {
		setState( spriteShine.isVisible() ? STATE_BOMB : STATE_IDLE );
	}


	public final int getInitialYSpeed() {
		return initialYSpeed;
	}


	public final int getRandomInitialYSpeed() {
		return -getHeight() - NanoMath.randInt( 3 * getHeight() );
	}


	public final int getRandomDiffYSpeed() {
		return -( getHeight() >> 1 ) - NanoMath.randInt( getHeight() );
	}


	public final void draw( Graphics g ) {
		// tentativa desesperada de melhorar o desempenho no Motorola V3
		switch ( state ) {
			case STATE_IDLE:
			case STATE_BOMB:
			case STATE_SWITCHING_V:
			case STATE_SWITCHING_H:
				translate.addEquals( position );
				spriteFruit.drawWithoutClip( g );
				spriteShine.drawWithoutClip( g );
				translate.subEquals( position );
			break;

			case STATE_INVISIBLE:
			break;

			default:
				super.draw( g );
		}
	}

}
