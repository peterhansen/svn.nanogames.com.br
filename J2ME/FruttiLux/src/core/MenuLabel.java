/**
 * MenuLabel.java
 * 
 * Created on Mar 2, 2009, 3:14:44 PM
 *
 */

//#if JAR != "min"

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class MenuLabel extends DrawableGroup implements Constants {

	private static Sprite ICON;

	private static DrawableImage STRIPE;

	private static final byte LABEL_OFFSET_X = 12;

	private static final byte ICON_PLAY			= 0;
	private static final byte ICON_OPTIONS		= 1;
	private static final byte ICON_HELP			= 2;
	private static final byte ICON_RECOMMEND	= 3;
	private static final byte ICON_CREDITS		= 4;
	private static final byte ICON_BACK			= 5;
	private static final byte ICON_PRODUCTS		= 6;
	private static final byte ICON_EXIT			= 7;
	private static final byte ICON_RECORDS		= 8;
	private static final byte ICON_NO			= 9;
	private static final byte ICON_OBJECTIVES	= 10;
	private static final byte ICON_SOUND_OFF	= 11;
	private static final byte ICON_SOUND_ON		= 12;
	private static final byte ICON_YES			= 13;
	private static final byte ICON_VIBRA_ON		= 14;

	public static final byte TOTAL_TYPES = ICON_VIBRA_ON + 1;

	private final Sprite icon;

	private final Label label;

	private boolean active;


	public MenuLabel( int textIndex ) throws Exception {
		super( 3 );

		final DrawableImage stripe = new DrawableImage( STRIPE );
		insertDrawable( stripe );

		label = new Label( GameMIDlet.getFont( FONT_MENU ), GameMIDlet.getText( textIndex ) );
		insertDrawable( label );

		icon = new Sprite( ICON );
		insertDrawable( icon );

		stripe.setPosition( icon.getWidth(), ( icon.getHeight() - stripe.getHeight() ) >> 1 );
		label.setPosition( stripe.getPosX() + LABEL_OFFSET_X, stripe.getPosY() + ( ( stripe.getHeight() - label.getHeight() ) >> 1 ) );

		setSize( stripe.getPosX() + stripe.getWidth(), icon.getHeight() );
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		setText( textIndex );
	}


	public static final void load() throws Exception {
		ICON = new Sprite( PATH_MENU + "icon" );
		STRIPE = new DrawableImage( PATH_MENU + "stripe.png" );
	}


	public final void setActive( boolean active ) {
		this.active = active;
		icon.setFrame( active ? 1 : 0 );
		label.setFont( GameMIDlet.getFont( active ? FONT_MENU_OVER : FONT_MENU ) );
	}


	public final void setText( int textIndex ) {
		label.setText( textIndex );
		// evita que o texto seja cortado caso seja muito grande
		if ( label.getPosX() + label.getWidth() >= getWidth() )
			label.setPosition( getWidth() - label.getWidth(), label.getPosY() );
		
		int index = ICON_PLAY;
		switch ( textIndex ) {
			case TEXT_NEW_GAME:
			case TEXT_CONTINUE:
			case TEXT_CONTROLS:
				index = ICON_PLAY;
			break;

			case TEXT_OPTIONS:
				index = ICON_OPTIONS;
			break;

			case TEXT_HELP:
				index = ICON_HELP;
			break;

			case TEXT_CREDITS:
				index = ICON_CREDITS;
			break;

			case TEXT_CANCEL:
			case TEXT_BACK:
			case TEXT_BACK_MENU:
				index = ICON_BACK;
			break;

			case TEXT_PRODUCTS_TITLE:
				index = ICON_PRODUCTS;
			break;

			case TEXT_EXIT:
			case TEXT_EXIT_GAME:
				index = ICON_EXIT;
			break;

			case TEXT_RECOMMEND_TITLE:
			case TEXT_TURN_VIBRATION_ON:
				index = ICON_RECOMMEND;
			break;

			case TEXT_TURN_SOUND_OFF:
				index = ICON_SOUND_ON;
			break;

			case TEXT_TURN_SOUND_ON:
				index = ICON_SOUND_OFF;
			break;

			case TEXT_TURN_VIBRATION_OFF:
				index = ICON_VIBRA_ON;
			break;

			case TEXT_HIGH_SCORES:
			case TEXT_NANO_ONLINE:
				index = ICON_RECORDS;
			break;

			case TEXT_OBJECTIVES:
				index = ICON_OBJECTIVES;
			break;

			case TEXT_YES:
				index = ICON_YES;
			break;

			case TEXT_NO:
				index = ICON_NO;
			break;

			case TEXT_PORTUGUESE:
				case TEXT_ENGLISH:
				index = ICON_YES;
				break;
			case TEXT_CONFIRM:
			case TEXT_CURRENT_PROFILE:
				index = ICON_YES;
				break;
			case TEXT_CHOOSE_ANOTHER:
				index = ICON_NO;
				break;

			
			//#if DEBUG == "true"
			default:
				throw new IllegalArgumentException( "nenhum �cone definido para: " + textIndex );
			//#endif
		}
		icon.setSequence( index );
		setActive( active );
	}

}

//#endif