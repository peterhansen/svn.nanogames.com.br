/**
 * TimeBar.java
 * 
 * Created on Feb 27, 2009, 4:12:03 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public final class TimeBar extends UpdatableGroup implements Constants {

	//#if SCREEN_SIZE == "SMALL"
//# 		private static final byte BORDER_HEIGHT = 2;
	//#else
		private static final byte BORDER_HEIGHT = 4;
	//#endif

	private static final int COLOR_EMPTY		= 0xe1c27e;
	private static final int COLOR_RECTANGLE	= 0x390000;

	private final DrawableImage left;
	private final DrawableImage right;
	private final Pattern border;

	private final Pattern fillTime;
	private final Pattern fillEmpty;

	private final DrawableRect rectangle = new DrawableRect( COLOR_RECTANGLE );

	private int internalWidth;

	private final boolean demo;

	private final MUV demoMUV = new MUV( 33 );

	private byte demoPercent;


	public TimeBar( boolean demo ) throws Exception {
		super( 8 );

		this.demo = demo;

		fillTime = new Pattern( new DrawableImage( PATH_IMAGES + "bar_fill.png" ) );
		insertDrawable( fillTime );
		fillEmpty = new Pattern( COLOR_EMPTY );
		insertDrawable( fillEmpty );

		insertDrawable( rectangle );

		left = new DrawableImage( PATH_IMAGES + "bar_right.png" );
		left.mirror( TRANS_MIRROR_H );
		insertDrawable( left );
		right = new DrawableImage( left );
		insertDrawable( right );

		border = new Pattern( new DrawableImage( PATH_IMAGES + "bar_border.png" ) );
		border.setSize( 0, border.getFill().getHeight() );
		border.setPosition( left.getWidth(), 0 );
		insertDrawable( border );

		fillTime.setPosition( left.getWidth(), BORDER_HEIGHT + 1 );
		fillTime.setSize( 0, fillTime.getFill().getHeight() );

		//#if SCREEN_SIZE == "SMALL"
//# 			rectangle.setPosition( left.getWidth(), BORDER_HEIGHT );
//# 			rectangle.setSize( 0, fillTime.getHeight() + 1 );
		//#else
			rectangle.setPosition( left.getWidth() - 1, BORDER_HEIGHT );
			rectangle.setSize( 0, fillTime.getHeight() + 2 );
		//#endif
		fillEmpty.setSize( 0, rectangle.getHeight() );

		size.y = left.getHeight();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		border.setSize( width - left.getWidth() - right.getWidth(), border.getHeight() );
		right.setPosition( width - right.getWidth(), 0 );

		//#if SCREEN_SIZE == "SMALL"
//# 			internalWidth = width - left.getWidth() - right.getWidth();
		//#else
			internalWidth = width - left.getWidth() - right.getWidth() + 2;
		//#endif
	}


	public final void setTime( int time, int total ) {
		time = NanoMath.clamp( time, 0, total );
		
		rectangle.setSize( internalWidth * time / total, rectangle.getHeight() );
		fillTime.setSize( rectangle.getWidth(), fillTime.getHeight() );
		fillEmpty.setPosition( fillTime.getPosX() + fillTime.getWidth() - 1, BORDER_HEIGHT );
		fillEmpty.setSize( internalWidth - fillTime.getWidth(), fillEmpty.getHeight() );
	}


	public final void update( int delta ) {
		if ( demo ) {
			demoPercent = ( byte ) NanoMath.clamp( demoPercent + demoMUV.updateInt( delta ), 0, 100 );
			if ( demoPercent <= 0 || demoPercent >= 100 )
				demoMUV.setSpeed( -demoMUV.getSpeed() );

			setTime( demoPercent, 100 );
		}
	}

}
