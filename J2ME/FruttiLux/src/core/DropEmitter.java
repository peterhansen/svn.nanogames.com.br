/**
 * SplashEmitter.java
 * �2008 Nano Games.
 *
 * Created on May 1, 2008 10:53:37 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class DropEmitter extends Drawable implements Updatable, Constants {
	
	private static  byte EMIT_PARTICLES;
	private static short MAX_PARTICLES;
	
	private final int FP_MAX_Y_SPEED;
	
	private static final int FP_EXPLOSION_MAX_SPEED_X = NanoMath.toFixed( ScreenManager.SCREEN_WIDTH / 3 );
	private static final int FP_EXPLOSION_MAX_HEIGHT = -NanoMath.toFixed( ScreenManager.SCREEN_HEIGHT << 2 );
	
	private final int BORN_MAX_Y_DIFF;
	private final int BORN_MAX_HALF_Y_DIFF;

	/** Acelera��o da gravidade. */
	private static final int FP_GRAVITY_ACC = 9800000; //NanoMath.divInt( -98, 10 ) = -642253;

	private static final int FP_HALF_GRAVITY_ACC = FP_GRAVITY_ACC >> 1;
	
	private static Particle[] particles;
	
	private short activeParticles;

	private static SimpleSprite DROPS;
	
	
	public DropEmitter() throws Exception {
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		// b = sqrt( -4 * MAX_Y * a ), onde b = velocidade inicial e a = gravidade
		FP_MAX_Y_SPEED =  NanoMath.sqrtFixed( Math.abs( NanoMath.mulFixed( FP_GRAVITY_ACC << 2, FP_EXPLOSION_MAX_HEIGHT ) ) );
		BORN_MAX_Y_DIFF = 20;
		BORN_MAX_HALF_Y_DIFF = BORN_MAX_Y_DIFF >> 1;		

		Particle.parent = this;
	}


	public static final void load() throws Exception {
		//#if JAR != "min"
			DROPS = new SimpleSprite( new Sprite( PATH_IMAGES + "drops/d" ) );
		//#endif

		// normalmente aparelhos com pouca mem�ria tamb�m possuem baixo desempenho
		if ( GameMIDlet.isLowMemory() ) {
			EMIT_PARTICLES = 5;
		} else {
			//#if SCREEN_SIZE == "BIG"
				EMIT_PARTICLES = 11;
			//#else
//# 				EMIT_PARTICLES = 6;
			//#endif
		}
		MAX_PARTICLES = ( short ) ( ( 15 * EMIT_PARTICLES ) );

		particles = new Particle[ MAX_PARTICLES ];
		for ( short i = 0; i < MAX_PARTICLES; ++i ) {
			particles[ i ] = new Particle();
		}
	}


	protected final void paint( Graphics g ) {
		for ( short i = 0; i < activeParticles; ++i ) {
			particles[ i ].draw( g );
		}
	}
	
	
	public final void update( int delta ) {
		final short previousActiveParticles = activeParticles;
		final int FP_DELTA = NanoMath.divInt( delta, 1000 );
		for ( short i = 0; i < activeParticles; ++i )
			particles[ i ].update( FP_DELTA );
		
		if ( previousActiveParticles != activeParticles )
			sort();
	}	
	
	
	public final void reset() {
		activeParticles = 0;
	}	
	
	
	private final void sort() {
		short i = 0;
		short j = ( short ) ( MAX_PARTICLES - 1 );
		for ( ; i < MAX_PARTICLES; ++i ) {
			if ( !particles[ i ].visible ) {
				for ( ; j > i; --j ) {
					if ( particles[ j ].visible ) {
						final Particle temp = particles[ i ];
						particles[ i ] = particles[ j ];
						particles[ j ] = temp;
						break;
					}
				}
			}
		}
		
		activeParticles = j;
	}
	
	
	public final void emit( byte type, Point pos ) {
		final Point temp = new Point();
		final int limit = NanoMath.min( MAX_PARTICLES, EMIT_PARTICLES + activeParticles );
		for ( short i = activeParticles; i < limit; ++i ) {
			temp.set( pos.x, pos.y - BORN_MAX_HALF_Y_DIFF + NanoMath.randInt( BORN_MAX_Y_DIFF ) );
			particles[ i ].born( type, temp );
		}
		activeParticles = ( short ) limit;
	}
	
	
	private final void onDie() {
		--activeParticles;
		
		//#if DEBUG == "true"
		if ( activeParticles < 0 )
			throw new RuntimeException( "N�mero de part�culas negativo." );
		//#endif
	}
	
	
	/**
	 * Classe interna que descreve uma part�cula de fogos de artif�cio. 
	 */
	private static final class Particle {
		
		/** Tempo acumulado da part�cula. */
		private int fp_accTime;
		
		private int fp_speedX;
		private int fp_speedY;
		
		private final Point position = new Point();
		
		private final Point initialPos = new Point();
		
		private static DropEmitter parent;

		private boolean visible;

		private byte frame;

		
		private final void born( byte type, Point bornPosition ) {
			fp_speedY = -NanoMath.randFixed( parent.FP_MAX_Y_SPEED );
			
			fp_accTime = 0;
			
			fp_speedX = -( FP_EXPLOSION_MAX_SPEED_X >> 1 ) + NanoMath.randFixed( FP_EXPLOSION_MAX_SPEED_X );
			visible = true;

			//#if JAR == "min"
//# 				initialPos.set( bornPosition.sub( 2, 1 ) );
			//#else
				initialPos.set( bornPosition.sub( DROPS.getSize().div( 2 ) ) );
				this.frame = ( byte ) ( type + ( NanoMath.randInt( 3 ) * Fruit.TOTAL_TYPES ) );
			//#endif
		}

		
		public final void update( int fp_delta ) {
			if ( visible ) {
				fp_accTime += fp_delta;

				position.set( initialPos.x + NanoMath.toInt( NanoMath.mulFixed( fp_speedX, fp_accTime ) ),
							  initialPos.y + NanoMath.toInt( NanoMath.mulFixed( fp_speedY, fp_accTime ) + NanoMath.mulFixed( FP_HALF_GRAVITY_ACC, NanoMath.mulFixed( fp_accTime, fp_accTime ) ) ) );

				if ( position.x <= 0 || position.x >= parent.getWidth() || position.y >= parent.getHeight() ) {
					visible = false;
					parent.onDie();
				}
			}
		}


		public final void draw( Graphics g ) {
			if ( visible ) {
				//#if JAR == "min"
//# 					g.setColor( COLOR_DROP );
//# 					g.fillRect( translate.x + position.x, translate.y + position.y, 3, 2 );
				//#else
					DROPS.setFrame( frame );
					DROPS.setPosition( position.add( translate ) );
					DROPS.drawWithoutClip( g );
				//#endif
			} // fim if ( visible )
		}


	}

}
