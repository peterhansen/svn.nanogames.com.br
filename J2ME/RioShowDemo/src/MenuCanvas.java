
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VideoControl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author peter
 */
public final class MenuCanvas extends UpdatableGroup implements KeyListener {

	private static final byte SCREEN_MAIN			= 0;
	private static final byte SCREEN_EVENT_LIST		= 1;
	private static final byte SCREEN_EVENT_DETAILS	= 2;
	private static final byte SCREEN_GALLERY		= 3;
	private static final byte SCREEN_MAP			= 4;
	private static final byte SCREEN_PLACE_DETAILS	= 5;
	private static final byte SCREEN_LOADING		= 6;

	private byte screen;

	private int accTime;

	private final Point[] indexes = new Point[] {
		new Point( 1, 4 ),
		new Point( 9, 2 ),
		new Point( 12, 2 ),
		new Point( 14, 2 ),
		new Point( 21, 1 ),
		new Point( 17, 2 ),
		new Point( 22, 3 ),
	};

	private int index;

	private DrawableImage image;

	private final Menu menu;

	private final DrawableImage favorites;

	private final Menu search;
	

	public MenuCanvas() throws Exception {
		super( 20 );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		menu = new Menu( Menu.TYPE_MENU );
		insertDrawable( menu );
		
		search = new Menu( Menu.TYPE_SEARCH );
		insertDrawable( search );

		favorites = new DrawableImage("/tela_" + 1 + ".png");

		setScreen( SCREEN_MAIN );
	}


	public void keyPressed(int key) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
				switch ( menu.getState() ) {
					case Menu.STATE_APPEARING:
					case Menu.STATE_VISIBLE:
						menu.hide();
						return;

					default:
						search.hide();
						menu.show();
						return;
				}

			case ScreenManager.KEY_SOFT_RIGHT:
				switch ( search.getState() ) {
					case Menu.STATE_APPEARING:
					case Menu.STATE_VISIBLE:
						search.hide();
						return;

					default:
						menu.hide();
						search.show();
						return;
				}
		}

		switch ( screen ) {
			case SCREEN_MAIN:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_UP:
						setIndex( index - 1 );
					break;
					
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_DOWN:
						setIndex( index + 1 );
					break;

					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_NUM5:
						setScreen( SCREEN_EVENT_LIST );
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_LEFT:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						RioShow.exit();
					break;
				}
			break;

			case SCREEN_EVENT_LIST:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_UP:
						setIndex( index - 1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_DOWN:
						setIndex( index + 1 );
					break;

					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_NUM5:
						setScreen( SCREEN_EVENT_DETAILS );
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_LEFT:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						setScreen( SCREEN_MAIN );
					break;
				}
			break;

			case SCREEN_EVENT_DETAILS:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_UP:
						setIndex( index - 1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_DOWN:
						setIndex( index + 1 );
					break;

					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_NUM5:
						switch ( index ) {
							case 0:
								setScreen( SCREEN_PLACE_DETAILS );
							break;

							default:
								setScreen( SCREEN_GALLERY );
							break;
						}
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_LEFT:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						setScreen( SCREEN_EVENT_LIST );
					break;
				}
			break;

			case SCREEN_GALLERY:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_UP:
						setIndex( index - 1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_DOWN:
						setIndex( index + 1 );
					break;
					
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_NUM5:
//						if ( index == 0 ) {
//							try {
//								String ble = System.getProperty("video.encodings");
//								System.out.println("BLE: "+ ble);
//								String[] bla = Manager.getSupportedProtocols( "video" );
//								final Player player = Manager.createPlayer(getClass().getResourceAsStream( "/video.3gp"), "video/3gp" );
//								player.realize();
//								VideoControl videoControl = (VideoControl) player.getControl("javax.microedition.media.control.VideoControl");
//								if (videoControl == null) {
//									System.out.println("NÃO SUPORTA VÍDEO");
//								} else {
//									videoControl.initDisplayMode(VideoControl.USE_DIRECT_VIDEO, ScreenManager.getInstance() );
////									videoControl.setDisplayLocation( 50, 50 );
////									videoControl.setDisplaySize( 150, 100 );
//									videoControl.setDisplayFullScreen(true);
//									videoControl.setVisible(true);
//								}
//							} catch ( Exception ex ) {
//								ex.printStackTrace();
//							}
//						}
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_LEFT:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						setScreen( SCREEN_EVENT_DETAILS );
					break;
				}
			break;

			case SCREEN_MAP:
				switch ( key ) {
					case ScreenManager.KEY_LEFT:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						setScreen( SCREEN_PLACE_DETAILS );
					break;
				}
			break;

			case SCREEN_PLACE_DETAILS:
				switch ( key ) {
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_UP:
						setIndex( index - 1 );
					break;

					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_DOWN:
						setIndex( index + 1 );
					break;
					
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_NUM5:
						setScreen( SCREEN_LOADING );
					break;

					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_LEFT:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
						setScreen( SCREEN_EVENT_DETAILS );
					break;
				}
			break;

		}
	}


	public void keyReleased(int key) {
	}


	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;
		switch ( screen ) {
			case SCREEN_LOADING:
				if ( accTime >= 3000 )
					setScreen( SCREEN_MAP );
			break;
		}
	}


	private final void setScreen( int screen ) {
		this.screen = ( byte ) screen;
		accTime = 0;

		switch ( screen ) {
			case SCREEN_LOADING:
				setIndex( NanoMath.randInt( indexes[ screen ].y ) );
			break;

			default:
				setIndex( 0 );
		}
	}


	private final void setIndex( int index ) {
		index = ( indexes[ screen ].y + index ) % indexes[ screen ].y;
		this.index = index;
		setImage( indexes[ screen ].x + index );
	}


	private final void setImage( int imageIndex ) {
		if ( image != null ) {
			removeDrawable( image );
			image = null;
		}

		try {
			image = new DrawableImage("/tela_" + imageIndex + ".png");
			insertDrawable( image, 0 );
		} catch (Exception ex) {
			System.out.println("ERRO: " + imageIndex);
			ex.printStackTrace();
		}
	}

}
