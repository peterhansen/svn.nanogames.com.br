/**
 * TabbedPane.java
 *
 * Created on Feb 2, 2011 4:12:29 PM
 *
 */



import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
//#if TOUCH == "true"
//# import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author peter
 */
public final class Menu extends DrawableImage implements Updatable {

	public static final byte STATE_NONE			= -1;
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_VISIBLE		= 2;
	public static final byte STATE_HIDING		= 3;

	private byte state = STATE_NONE;

	private int accTime;

	private static final String[] FILENAMES = { "/menu.png", "/search.png" };

	private static final byte[] MENU_HEIGHTS = { 24, 28 };

	private final byte MENU_HEIGHT;

	public static final byte TYPE_MENU = 0;
	public static final byte TYPE_SEARCH = 1;

	private final byte type;

	private final BezierCurve tweener = new BezierCurve();

	/** Duração da transição (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 500;
	

	public Menu( byte type ) throws Exception {
		super( FILENAMES[ type ] );
		this.type = type;
		MENU_HEIGHT = MENU_HEIGHTS[ type ];
		setState( STATE_HIDDEN );
	}


	public final byte getState() {
		return state;
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_APPEARING:
				accTime += delta;
				refresh();

				if ( accTime >= TRANSITION_TIME ) {
					setState( STATE_VISIBLE );
				}
			break;

			case STATE_HIDING:
				accTime += delta;
				refresh();

				if ( accTime >= TRANSITION_TIME ) {
					setState( STATE_HIDDEN );
				}
			break;

			case STATE_VISIBLE:
			break;
		}
	}


	public final void show() {
		switch ( state ) {
			case STATE_APPEARING:
			case STATE_VISIBLE:
			return;

			default:
				setState( STATE_APPEARING );
		}
	}


	public final void hide() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_HIDING:
			return;

			default:
				setState( STATE_HIDING );
		}
	}


	public final void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
//                focusShadow.setVisible( false );
				accTime = TRANSITION_TIME;
				setTween( 0, ScreenManager.SCREEN_HEIGHT - MENU_HEIGHT );
				refresh();
			break;

			case STATE_APPEARING:
//                focusShadow.setVisible( true );
				switch ( this.state ) {
					case STATE_VISIBLE:
					return;
				}
				setTween( getPosY(), ScreenManager.SCREEN_HEIGHT - getHeight() );
				
				accTime = 0;
				refresh();
			break;

			case STATE_HIDING:
//                focusShadow.setVisible( false );
				if ( this.state == STATE_HIDDEN )
					return;
				accTime = 0;
				setTween( getPosY(), ScreenManager.SCREEN_HEIGHT - MENU_HEIGHT );
				refresh();
			break;
		}

		this.state = ( byte ) state;
	}


	public final int getPanePosY() {
		return getPosY();
	}

	
	private final void setTween( int startY, int endY ) {
		tweener.origin.y = startY;

		if ( endY < startY ) {
			tweener.destiny.y = endY;

			tweener.control1.y = startY;
			tweener.control2.y = endY;
		} else {
			tweener.destiny.y = endY;
			
			tweener.control1.y = startY;
			tweener.control2.y = endY;
		}

        // garante um percentual da tela livre
//        if ( tweener.destiny.y < Y_MINIMUM_MARGIN ) {
//            tweener.destiny.y = Y_MINIMUM_MARGIN;
//        }
	}

    /**
     * Calcula tamanho da sombra de foco (trama) para que ela obscureça
     * o que estiver através das abas.
     */
    private void setShadowSize() {
//        focusShadow.setSize(focusShadow.getWidth(), this.getPosY() + fill.getPosY() );
    }

	private final void refresh() {
		setRefPixelPosition( 0, tweener.getYFixed( NanoMath.divInt( accTime, TRANSITION_TIME ) ) );

        // usamos metade da altura da tela a fim de ter uma "gordurinha"
        // para o movimento do tween a fim de não faltar o background
//        fill.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
        setShadowSize();
	}


//	public final void setSize( int width, int height ) {
//		super.setSize( width, height + ( OFFSET_Y * 3 / 2 ) );
//
//		this.height = height;
//        setShadowSize();
//	}

	

}
