
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author peter
 */
public final class RioShow extends AppMIDlet {

	protected int changeScreen(int screen) throws Exception {
		ScreenManager.getInstance().setCurrentScreen( new MenuCanvas() );
		return screen;
	}

	protected void loadResources() throws Exception {
		setScreen( 0 );
	}

}
