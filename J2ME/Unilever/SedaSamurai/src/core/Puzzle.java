/**
 * Puzzle.java
 * 
 * Created on Jul 21, 2009, 3:40:08 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Puzzle extends DrawableGroup implements Constants, Updatable {

	public static final short PUZZLE_MAX_PIECES = 200;

	private static final byte PIECE_TOP_LEFT		= 0;
	private static final byte PIECE_TOP_RIGHT		= 1;
	private static final byte PIECE_BOTTOM_LEFT		= 2;
	private static final byte PIECE_BOTTOM_RIGHT	= 3;
	private static final byte PIECE_TOP_1			= 4;
	private static final byte PIECE_TOP_2			= 5;
	private static final byte PIECE_BOTTOM_1		= 6;
	private static final byte PIECE_BOTTOM_2		= 7;
	private static final byte PIECE_LEFT_1			= 8;
	private static final byte PIECE_LEFT_2			= 9;
	private static final byte PIECE_RIGHT_1			= 10;
	private static final byte PIECE_RIGHT_2			= 11;
	private static final byte PIECE_MIDDLE_1		= 12;
	private static final byte PIECE_MIDDLE_2		= 13;

	private static final byte FRAME_CORNER			= 0;
	private static final byte FRAME_TOP_BIG			= 1;
	private static final byte FRAME_TOP_SMALL		= 2;
	private static final byte FRAME_LEFT_BIG		= 3;
	private static final byte FRAME_LEFT_SMALL		= 4;
	private static final byte FRAME_MIDDLE_BIG		= 5;
	private static final byte FRAME_MIDDLE_SMALL	= 6;

	private static final byte SEQUENCE_CLEARED		= 0;
	private static final byte SEQUENCE_CHOSEN		= 1;
	private static final byte SEQUENCE_REMAINING	= 2;

	public static final byte STATE_CLOSED		= 0;
	public static final byte STATE_OPENING		= 1;
	public static final byte STATE_OPEN			= 2;
	public static final byte STATE_CLOSING		= 3;

	private byte state = -1;

	public static final short TIME_ANIMATION = 1800;

	private short accTime;
	private short previousVisiblePieces = -1;

	private Drawable bkg;
	private final Drawable seda;

	private static Drawable background;

	private final Pattern pattern;

	private final Mutex mutex = new Mutex();

	//#if SCREEN_SIZE == "BIG"
		private final Point[] sizes = new Point[] {
			new Point( 20, 14 ),
			new Point( 26, 14 ),
			new Point( 14, 19 ),
			new Point( 14, 26 ),
			new Point( 20, 16 ),
			new Point( 26, 16 ),
			new Point( 14, 26 ),
		};
	//#elif SCREEN_SIZE == "MEDIUM"
//# 		private final Point[] sizes = new Point[] {
//# 			new Point( 19, 13 ),
//# 			new Point( 25, 13 ),
//# 			new Point( 13, 17 ),
//# 			new Point( 13, 23 ),
//# 			new Point( 19, 15 ),
//# 			new Point( 25, 15 ),
//# 			new Point( 13, 23 ),
//# 		};
	//#elif SCREEN_SIZE == "SMALL"
//# 		private final Point[] sizes = new Point[] {
//# 			new Point( 13, 9 ),
//# 			new Point( 16, 9 ),
//# 			new Point( 10, 11 ),
//# 			new Point( 10, 14 ),
//# 			new Point( 13, 10 ),
//# 			new Point( 16, 10 ),
//# 			new Point( 10, 14 ),
//# 		};
	//#endif

	/** �ndice das pe�as. */
	private byte[][] pieces;

	private short clearedPieces;

	private short initialPieces;

	private short totalPieces;

	private final byte DRAWABLE_OFFSET = 3;

	/** Imagem pr�-renderizada do background e das pe�as, para acelerar (MUITO) o desenho do quebra-cabe�as. */
	private static Image buffer;


	public Puzzle() throws Exception {
		super( PUZZLE_MAX_PIECES + 3 );

		final Sprite s = new Sprite( PATH_IMAGES + "pieces/p" );

		pattern = GameMIDlet.getBackground();

		//#if SCREEN_SIZE == "BIG"
			insertDrawable( pattern );

			bkg = loadBackground();
			insertDrawable( bkg );
		//#else
//# 			if ( !GameMIDlet.isLowMemory() ) {
//# 				insertDrawable( pattern );
//# 				bkg = loadBackground();
//# 				insertDrawable( bkg );
//# 			}
		//#endif

		seda = new DrawableImage( PATH_IMAGES + "s.png" );
		insertDrawable( seda );
		
		for ( short i = 0; i < PUZZLE_MAX_PIECES; ++i ) {
			final Sprite temp = new Sprite( s ) {

				public final Point getFrameOffset( int frameIndex ) {
					return new Point();
				}

			};
			temp.setVisible( false );
			temp.setClipTest( false );
			insertDrawable( temp );
		}

		setState( STATE_CLOSED );
	}


	public static final Drawable loadBackground() throws Exception {
		if ( background == null ) {
			final DrawableGroup group = new DrawableGroup( 4 );
			background = group;

			final DrawableImage imageBkg = new DrawableImage( PATH_IMAGES + "bkg.png" );
			group.insertDrawable( imageBkg );
			group.setSize( imageBkg.getSize() );
		}

		return background;
	}


	public final void clearPiece( Sprite p, boolean refreshBuffer ) {
		setSpriteSequence( p, SEQUENCE_CLEARED );
		++clearedPieces;

		if ( refreshBuffer )
			refreshBuffer();

		//#if DEBUG == "true"
			//System.out.println( "PE�AS VIS�VEIS: " + getRemainingCount() + " / " + totalPieces );
		//#endif
	}


	public final void clearAll() {
		clearedPieces = 0;
		for ( short i = 0; i < totalPieces; ++i ) {
			final Sprite s = getPiece( i );
			setSpriteSequence( s, SEQUENCE_REMAINING );
		}
	}


	public final short getTotalPieces() {
		return totalPieces;
	}


	public final void reset( int piecesShown ) {
		clearAll();

		initialPieces = ( short ) ( totalPieces - piecesShown );
		fillPieces( initialPieces, false );
		refreshBuffer();

		setState( STATE_CLOSING );
	}


	protected final void paint( Graphics g ) {
		mutex.acquire();

		//#if SCREEN_SIZE != "BIG"
//# 			if ( buffer != null )
//# 				g.drawImage( buffer, translate.x, translate.y, 0 );
//# 
//# 			// na vers�o de baixa mem�ria, o buffer j� possui a imagem de background, e as pe�as vis�veis (escuras, que faltam
//# 			// ser encaixadas) devem ser desenhadas sobre ele
//# 			if ( GameMIDlet.isLowMemory() ) {
//# 				final int TOTAL = totalPieces + DRAWABLE_OFFSET;
//# 				for ( int i = 0; i < TOTAL; ++i )
//# 					drawables[ i ].draw( g );
//# 			}
		//#else
			if ( buffer != null )
				g.drawImage( buffer, translate.x, translate.y, 0 );
		//#endif

		mutex.release();
	}


	private final void fillPieces( int totalCleared, boolean reset ) {
		if ( reset ) {
			reset( 0 );
		}

		final int TOTAL_TRIES = 30;
		while ( clearedPieces < totalCleared ) {
			// sorteia uma das pe�as restantes
			Sprite p = null;
			byte tries = 0;

			do {
				p = getPiece( NanoMath.randInt( totalPieces ) );
				++tries;
			} while ( p.getSequenceIndex() == SEQUENCE_CLEARED && tries < TOTAL_TRIES );

			// percorre um a um, caso o sorteio n�o tenha dado resultado (evita loops infinitos ou muito longos)
			if ( tries >= TOTAL_TRIES ) {
				final short index = ( short ) NanoMath.randInt( totalPieces - 1 );
				int i = index + 1;

				for ( ; i != index && p.getSequenceIndex() == SEQUENCE_CLEARED; i = ( i + 1 ) % totalPieces ) {
					p = getPiece( i );
				}

				//#if DEBUG == "true"
//					if ( i == index ) {
//						throw new IllegalStateException( "n�o achou pe�a dispon�vel" );
//					}
				//#endif
			}

			clearPiece( p, false );
		}

		//#if DEBUG == "true"
			System.out.println( "fillPieces FIM: " + getRemainingCount() + " -> " + totalCleared + " / " + totalPieces );
		//#endif
	}


	public final void setSize( int width, int height ) {
		//#if SCREEN_SIZE != "BIG"
//# 			if ( GameMIDlet.isLowMemory() ) {
//# 				width = width * 94 / 100;
//# 				height = height * 92 / 100;
//# 			}
		//#endif

		final Point previousSize = new Point( size );
		
		final int PIECE_WIDTH = sizes[ FRAME_TOP_BIG ].x + sizes[ FRAME_TOP_SMALL ].x;
		final int PIECE_HEIGHT = sizes[ FRAME_LEFT_BIG ].y + sizes[ FRAME_LEFT_SMALL ].y;
		final int CORNERS_WIDTH = ( sizes[ FRAME_CORNER ].x << 1 );
		final int CORNERS_HEIGHT = ( sizes[ FRAME_CORNER ].y << 1 );

		width += sizes[ FRAME_TOP_BIG ].x;
		height += sizes[ FRAME_LEFT_SMALL ].y;

		if ( bkg == null ) {
			try {
				bkg = loadBackground();
				//#if DEBUG == "true"
					System.out.println( "BKG LOADED" );
				//#endif
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}

		if ( bkg != null && height > bkg.getHeight() )
			height = bkg.getHeight();
		
		int columns = 2 + ( ( ( width - CORNERS_WIDTH ) / PIECE_WIDTH ) << 1 );
		int rows = 2 + ( ( ( height - CORNERS_HEIGHT ) / PIECE_HEIGHT ) << 1 );

		if ( ( columns & 1 ) == 0 )
			--columns;
		if ( ( rows & 1 ) == 0 )
			--rows;

		if ( bkg != null && width > bkg.getWidth() )
			width = bkg.getWidth();

		totalPieces = ( short ) ( columns * rows );

		pieces = new byte[ columns ][ rows ];
		int count = 0;
		for ( int r = 0; r < rows; ++r ) {
			for ( int c = 0, x = 0; c < columns; ++c, ++count ) {
				if ( c == 0 ) {
					// esquerda
					if ( r == 0 ) {
						pieces[ c ][ r ] = PIECE_TOP_LEFT;
					} else if ( r == rows - 1 ) {
						pieces[ c ][ r ] = PIECE_BOTTOM_LEFT;
					} else {
						pieces[ c ][ r ] = ( ( r & 1 ) == 0 ) ? PIECE_LEFT_1 : PIECE_LEFT_2;
					}
				} else if ( c == columns - 1 ) {
					// direita
					if ( r == 0 ) {
						pieces[ c ][ r ] = PIECE_TOP_RIGHT;
					} else if ( r == rows - 1 ) {
						pieces[ c ][ r ] = PIECE_BOTTOM_RIGHT;
					} else {
						pieces[ c ][ r ] = ( ( r & 1 ) == 0 ) ? PIECE_RIGHT_1 : PIECE_RIGHT_2;
					}
				} else {
					// meio
					if ( r == 0 ) {
						pieces[ c ][ r ] = ( ( c & 1 ) == 0 ) ? PIECE_TOP_1 : PIECE_TOP_2;
					} else if ( r == rows - 1 ) {
						pieces[ c ][ r ] = ( ( c & 1 ) == 0 ) ? PIECE_BOTTOM_1 : PIECE_BOTTOM_2;
					} else {
						pieces[ c ][ r ] = ( ( r & 1 ) == ( c & 1 ) ) ? PIECE_MIDDLE_1 : PIECE_MIDDLE_2;
					}
				}

				final Sprite s = getPiece( count );
				setSpriteSequence( s, SEQUENCE_REMAINING );
				setFrame( s, pieces[ c ][ r ] );
				if ( r > 0 ) {
					final Sprite top = ( Sprite ) getDrawable( ( columns * ( r - 1 ) ) + c + DRAWABLE_OFFSET );
					s.setPosition( x, top.getPosY() + sizes[ top.getFrameSequenceIndex() ].y );
				} else {
					s.setPosition( x, 0 );
				}

				s.setVisible( true );

				if ( c == columns - 1 && r == rows - 1 ) {
					super.setSize( x + s.getCurrentFrameImage().getWidth(), s.getPosY() + s.getCurrentFrameImage().getHeight() );
					pattern.setSize( size );
					
					if ( bkg != null )
						bkg.setPosition( ( size.x - bkg.getWidth() ) >> 1, 0 );

					seda.setPosition( bkg.getPosition().add( PUZZLE_SEDA_X, PUZZLE_SEDA_Y ) );

					//#if DEBUG == "true"
						System.out.println( "PUZZLE SIZE: " + getSize() );
					//#endif
				}

				x += sizes[ s.getFrameSequenceIndex() ].x;
			}
		}

		//#if DEBUG == "true"
			System.out.println( "PE�AS( " + count + "):" );
			for ( byte r = 0; r < rows; ++r ) {
				for ( byte c = 0; c < columns; ++c ) {
					System.out.print( ( pieces[ c ][ r ] < 10 ? " " + pieces[ c ][ r ] : pieces[ c ][ r ] + "" ) + " " );
				}
				System.out.println();
			}
		//#endif

		for ( ; count < getUsedSlots(); ++count )
			getDrawable( count ).setVisible( false );

		fillPieces( clearedPieces, true );

		mutex.acquire();
		//#if SCREEN_SIZE == "BIG"
			if ( buffer == null || !getSize().equals( previousSize ) ) {
		//#else
//# 			if ( buffer == null || ( !GameMIDlet.isLowMemory() && !getSize().equals( previousSize ) ) ) {
		//#endif
			//#if DEBUG == "true"
				System.out.println( "NEW BUFFER CREATED" );
			//#endif

			buffer = null;
			buffer = Image.createImage( getWidth(), getHeight() );
		}
		mutex.release();
		
		refreshBuffer();
	}


	public final void refreshBuffer() {
		//#if DEBUG == "true"
			System.out.println( "REFRESH BUFFER: " + ( bkg == null ? "null" : "BKG" ) );
		//#endif

		//#if SCREEN_SIZE != "BIG"
//# 			// se o background for nulo, � sinal de que o buffer j� est� correto
//# 			if ( bkg == null )
//# 				return;
		//#endif

		mutex.acquire();
		
		if ( buffer != null ) {
			final Graphics g = buffer.getGraphics();

			final Point previousTranslate = new Point( translate );
			translate.set( 0, 0 );

			pushClip( g );
			final int TOTAL = totalPieces + DRAWABLE_OFFSET;

			//#if SCREEN_SIZE != "BIG"
//# 				if ( GameMIDlet.isLowMemory() ) {
//# 					// em baixa mem�ria, esses drawables n�o est�o inseridos no grupo
					//#if DEBUG == "true"
//# 						System.out.println( "PAINTED LOW MEMORY BUFFER" );
					//#endif
//# 
//# 					pattern.draw( g );
//# 					bkg.draw( g );
//# 				}
			//#endif

			for ( int i = 0; i < TOTAL; ++i )
				drawables[ i ].draw( g );
			
			popClip( g );

			translate.set( previousTranslate );

			//#if DEBUG == "true"
				System.out.println( "REFRESH BUFFER: " + ( bkg == null ? "null" : "BKG" ) + " -> FIM <-" );
			//#endif

			//#if SCREEN_SIZE != "BIG"
//# 				// libera a mem�ria usada pela imagem de fundo, pois n�o � mais necess�ria
//# 				if ( GameMIDlet.isLowMemory() ) {
//# //					System.gc();
//# //					final long before = Runtime.getRuntime().freeMemory();
//# 
//# 					removeDrawable( bkg );
//# 					bkg = null;
//# 					background = null;
//# 
//# 					System.gc();
//# //					System.out.println( "RELEASED " + ( before - Runtime.getRuntime().freeMemory() ) + " bytes." );
//# 				}
			//#endif
		}

		mutex.release();
	}


	/**
	 * Obt�m "total" pe�as dispon�veis do tabuleiro.
	 * @param total
	 * @return
	 */
	public final Sprite[] getPiecesRemaining( int total ) {
		total = Math.min( total, totalPieces - clearedPieces );
		final Sprite[] ret = new Sprite[ total ];

		int start = NanoMath.randInt( totalPieces - 1 );
		for ( int i = start + 1, index = 0; index < total; i = ( i + 1 ) % totalPieces ) {
			final Sprite p = getPiece( i );

			if ( p.getSequenceIndex() == SEQUENCE_REMAINING ) {
				//#if DEBUG == "true"
//					System.out.println( "SORTEADO: " + i + " -> " + p );
				//#endif

				setSpriteSequence( p, SEQUENCE_CHOSEN );

				ret[ index++ ] = p;
			}
		}

		return ret;
	}


	//#if DEBUG == "true"
		public final int getRemainingCount() {
			int visiblePieces = 0;
			int count = 0;
			for ( short i = 0; i < totalPieces; ++i ) {
				if ( getPiece( i ).getSequenceIndex() == SEQUENCE_REMAINING )
					++count;

				if ( getPiece( i ).isVisible() )
					visiblePieces++;
			}

			System.out.println( "VISIBLE PIECES: " + visiblePieces );

			return count;
		}


		public final void printStatus() {
			int visiblePieces = 0;
			for ( short i = 0; i < totalPieces; ++i ) {
				if ( getPiece( i ).isVisible() )
					visiblePieces++;
			}

			System.out.println( "VISIBLE PIECES: " + visiblePieces );
		}
	//#endif


	private final void setSpriteSequence( Sprite s, int sequence ) {
		final byte frame = s.getFrameSequenceIndex();
		s.setSequence( sequence );
		s.setFrame( frame );

		//#if SCREEN_SIZE != "BIG"
//# 			if ( GameMIDlet.isLowMemory() ) {
//# 				s.setVisible( bkg != null || sequence != SEQUENCE_CLEARED );
//# 			}
		//#endif
	}


	private final Sprite getPiece( int index ) {
		return ( Sprite ) getDrawable( index + DRAWABLE_OFFSET );
	}


	public final void setState( int state ) {
		if ( this.state != state ) {
			this.state = ( byte ) state;

			switch ( state ) {
				case STATE_CLOSED:
					accTime = 0;
					refreshAnimation();
				break;

				case STATE_OPENING:
					accTime = 0;
				break;

				case STATE_OPEN:
					accTime = TIME_ANIMATION;
					refreshAnimation();
				break;

				case STATE_CLOSING:
					if ( previousVisiblePieces > 0 )
						accTime = TIME_ANIMATION;
				break;
			}
		}
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_OPENING:
				accTime += delta;

				refreshAnimation();

				if ( accTime >= TIME_ANIMATION )
					setState( STATE_OPEN );
			break;

			case STATE_CLOSING:
				accTime -= delta;

				refreshAnimation();

				if ( accTime <= 0 )
					setState( STATE_CLOSED );
			break;
		}
	}


	private final void refreshAnimation() {
		final int visiblePieces = ( accTime * totalPieces / TIME_ANIMATION );

		//#if SCREEN_SIZE == "BIG"
			for ( short i = 0; i < totalPieces; ++i ) {
				getPiece( i ).setVisible( i > visiblePieces );
			}
		//#else
//# 			for ( short i = 0; i < totalPieces; ++i ) {
//# 				final Sprite p = getPiece( i );
//# 				p.setVisible( i > visiblePieces && ( !GameMIDlet.isLowMemory() || p.getSequenceIndex() != SEQUENCE_CLEARED ) );
//# 			}
		//#endif

		if ( visiblePieces != previousVisiblePieces ) {
			refreshBuffer();
			previousVisiblePieces = ( short ) visiblePieces;
		}
	}


	private final void setFrame( Sprite s, byte pieceType ) {
		byte frame = 0;
		int pieceTransform = TRANS_NONE;
		
		switch ( pieceType ) {
			case PIECE_TOP_LEFT:
			break;
			
			case PIECE_TOP_RIGHT:
				pieceTransform = TRANS_MIRROR_H;
			break;

			case PIECE_BOTTOM_LEFT:
				pieceTransform = TRANS_MIRROR_V;
			break;

			case PIECE_BOTTOM_RIGHT:
				pieceTransform = TRANS_MIRROR_H | TRANS_MIRROR_V;
			break;

			case PIECE_TOP_1:
				frame = FRAME_TOP_BIG;
			break;

			case PIECE_TOP_2:
				frame = FRAME_TOP_SMALL;
			break;

			case PIECE_BOTTOM_1:
				frame = FRAME_TOP_BIG;
				pieceTransform = TRANS_MIRROR_V;
			break;

			case PIECE_BOTTOM_2:
				frame = FRAME_TOP_SMALL;
				pieceTransform = TRANS_MIRROR_V;
			break;

			case PIECE_LEFT_1:
				frame = FRAME_LEFT_SMALL;
			break;

			case PIECE_LEFT_2:
				frame = FRAME_LEFT_BIG;
			break;

			case PIECE_RIGHT_1:
				frame = FRAME_LEFT_SMALL;
				pieceTransform = TRANS_MIRROR_H;
			break;

			case PIECE_RIGHT_2:
				frame = FRAME_LEFT_BIG;
				pieceTransform = TRANS_MIRROR_H;
			break;

			case PIECE_MIDDLE_1:
				frame = FRAME_MIDDLE_BIG;
			break;

			case PIECE_MIDDLE_2:
				frame = FRAME_MIDDLE_SMALL;
			break;
		}

		s.setFrame( frame );
		s.setTransform( pieceTransform );
	}

}
