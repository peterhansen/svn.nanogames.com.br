/**
 * PieceListener.java
 * 
 * Created on Aug 17, 2009, 7:16:11 PM
 *
 */

package core;

/**
 *
 * @author Peter
 */
public interface PieceListener {

	public void onVanish( Piece p );

	public void onDie( Piece p );

}
