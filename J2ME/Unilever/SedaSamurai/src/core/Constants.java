/**
 * Constants.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/*******************************************************************************************
	 *                              DEFINI��ES GEN�RICAS                                       *
	 *******************************************************************************************/

	/** Nome curto do jogo, para submiss�o ao Nano Online. */
	public static final String APP_SHORT_NAME = "SEDP";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static final String URL_DEFAULT = "http://game.reconstrucaoem10dias.com.br";
	
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";

	public static final String PATH_PIECES = PATH_IMAGES + "pieces/";

	/** Caminho das imagens das bordas. */
	public static final String PATH_BORDER = PATH_IMAGES + "border/";

	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";
	
	public static final String PATH_MENU = PATH_IMAGES + "menu/";

	/***/
	public static final byte RANKING_TYPE_SCORE = 0;

	/***/
	public static final String HIGH_SCORE_DEFAULT_NICKNAME = "----";

	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	
	/** �ndice do slot de grava��o das op��es na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** �ndice do slot de grava��o de um campeonato salvo. */
	public static final byte DATABASE_SLOT_HIGH_SCORES = 2;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 2;
	
	/** Cor padr�o do fundo de tela. */
	public static final int COLOR_BACKGROUND = 0x091c20;

	public static final int COLOR_BACKGROUND_LIGHT = 0x174950;

	public static final int COLOR_SCROLL_BACK = 0xf7f0d4;
	public static final int COLOR_SCROLL_FORE = 0x945b01;

	/** �ndice da fonte utilizada para mostrar mensagens na tela de jogo. */
	public static final byte FONT_BIG	= 0;
	
	/** �ndice da fonte utilizada para textos. */
	public static final byte FONT_TEXT_WHITE	= 1;
	
	public static final byte FONT_SCORE_SMALL	= 2;

	public static final byte FONT_COMBO	= 3;

	public static final byte FONT_MESSAGE	= 4;

	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 5;
	
	/** Total de tipos de fonte a carregar do jogo. */
	public static final byte FONT_TYPES_TO_LOAD	= FONT_TYPES_TOTAL;

	public static final int COLOR_FONT_WHITE = 0xffffff;
	public static final int COLOR_FONT_DARK = 0xf28fae;

	//�ndices dos sons do jogo
	public static final byte SOUND_INDEX_SPLASH			= 0;
	public static final byte SOUND_INDEX_LEVEL_COMPLETE	= 1;
	public static final byte SOUND_INDEX_GAME_OVER		= 2;
	public static final byte SOUND_INDEX_AMBIENT_1		= 3;
	public static final byte SOUND_INDEX_AMBIENT_2		= 4;

	public static final byte SOUND_TOTAL = SOUND_INDEX_AMBIENT_2 + 1;

	public static final byte SOUND_AMBIENT_TOTAL = 2;

	//#ifdef VIBRATION_BUG
//#  		/** Dura��o padr�o da vibra��o, em milisegundos. */
//#  		public static final short VIBRATION_TIME_DEFAULT = 130;
//# 
//#  		/***/
//#  		public static final short VIBRATION_TIME_CANT_MOVE = 130;
	//#else
		/** Dura��o padr�o da vibra��o, em milisegundos. */
		public static final short VIBRATION_TIME_DEFAULT = 350;

	//#endif

	/** N�vel de dificuldade m�ximo. */
	public static final byte LEVEL_HARD = 15;

	/** Pontos ganhos por cada pe�a atingida. */
	public static final short SCORE_PIECE = 10;

	public static final short SCORE_SUPER_COMBO = SCORE_PIECE * 10;

	/** Tempo m�ximo restante na dificuldade m�nima. */
	public static final int TIME_TOTAL_EASY = 60000;

	/** Tempo m�ximo restante na dificuldade m�xima. */
	public static final short TIME_TOTAL_HARD = 11000;

	/** Penalidade de tempo ao errar um tiro. */
	public static final short TIME_PENALTY_MISS = 2000;

	/** Incremento no tempo restante a cada pe�a atingida, em milisegundos. */
	public static final short TIME_PIECE_INC = 2180;

	/** Dura��o total do combo. */
	public static final short TIME_COMBO = 2710;

	public static final short TIME_COMBO_START_BLINK = TIME_COMBO / 3;

	/** Porcentagem do tempo padr�o caso o aparelho suporte touch screen. */
	public static final byte TIME_PERCENT_TOUCHSCREEN = 75;

	public static final byte CONFIRM_INDEX_YES		= 0;
	public static final byte CONFIRM_INDEX_NO		= 1;
	public static final byte CONFIRM_INDEX_CANCEL	= 2;

	/** Total de tipos de pe�as. */
	public static final byte PIECE_TYPES = 7;

	public static final byte PIECE_TYPE_BLUE	= 0;
	public static final byte PIECE_TYPE_GREEN	= 1;
	public static final byte PIECE_TYPE_PINK	= 2;
	public static final byte PIECE_TYPE_ORANGE	= 3;
	public static final byte PIECE_TYPE_RED		= 4;
	public static final byte PIECE_TYPE_PURPLE	= 5;
	public static final byte PIECE_TYPE_YELLOW	= 6;

	// <editor-fold defaultstate="collapsed" desc="�NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEW_GAME						= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEW_GAME + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_HIGH_SCORES					= TEXT_OPTIONS + 1;
	public static final byte TEXT_NANO_RANKING					= TEXT_HIGH_SCORES + 1;
	public static final byte TEXT_PAUSE							= TEXT_NANO_RANKING + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_RECOMMEND_SENT				= TEXT_HELP + 1;
	public static final byte TEXT_RECOMMEND_TEXT				= TEXT_RECOMMEND_SENT + 1;
	public static final byte TEXT_HELP_TEXT						= TEXT_RECOMMEND_TEXT + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_HELP_TEXT + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_BRAND					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_BRAND + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_CLEAR							= TEXT_YES + 1;
	public static final byte TEXT_NO							= TEXT_CLEAR + 1;
	public static final byte TEXT_VERSION						= TEXT_NO + 1;
	public static final byte TEXT_RECOMMEND_SMS					= TEXT_VERSION + 1;
	public static final byte TEXT_RECOMMEND_TITLE				= TEXT_RECOMMEND_SMS + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_RECOMMEND_TITLE + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT					= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT + 1;
	public static final byte TEXT_GAME_OVER						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_NEW_RECORD					= TEXT_GAME_OVER + 1;
	public static final byte TEXT_LEVEL							= TEXT_NEW_RECORD + 1;
	public static final byte TEXT_COMBO							= TEXT_LEVEL + 1;
	public static final byte TEXT_LEVEL_CAPS					= TEXT_COMBO + 1;
	public static final byte TEXT_COMPLETE						= TEXT_LEVEL_CAPS + 1;
	public static final byte TEXT_CHOOSE_PROFILE				= TEXT_COMPLETE + 1;
	public static final byte TEXT_NO_PROFILE					= TEXT_CHOOSE_PROFILE + 1;
	public static final byte TEXT_CONFIRM						= TEXT_NO_PROFILE + 1;
	public static final byte TEXT_CHOOSE_ANOTHER				= TEXT_CONFIRM + 1;
	public static final byte TEXT_LOG_TITLE						= TEXT_CHOOSE_ANOTHER + 1;
	public static final byte TEXT_LOG_TEXT						= TEXT_LOG_TITLE + 1;
	
	/** n�mero total de textos do jogo */
	public static final byte TEXT_TOTAL = TEXT_LOG_TEXT + 1;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS TELAS DO JOGO">
	
//	public static final byte SCREEN_CHOOSE_LANGUAGE				= 0;
	public static final byte SCREEN_CHOOSE_SOUND				= 0;
	public static final byte SCREEN_SPLASH_NANO					= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME					= SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU					= SCREEN_SPLASH_GAME + 1;
//	public static final byte SCREEN_NEW_GAME					= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CONTINUE_GAME				= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_NEXT_LEVEL					= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_OPTIONS						= SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_HIGH_SCORES					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_NANO_RANKING_MENU			= SCREEN_HIGH_SCORES + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_HELP						= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_HELP + 1;
	public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_CREDITS						= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_PAUSE						= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU				= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT				= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1					= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_LOADING_2					= SCREEN_LOADING_1 + 1;
	public static final byte SCREEN_LOADING_GAME				= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_LOADING_GAME + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_LOADING_PLAY_SCREEN			= SCREEN_NO_PROFILE + 1;
	
	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG				= SCREEN_LOADING_PLAY_SCREEN + 1;
	//#endif
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= ENTRY_MAIN_MENU_NEW_GAME + 1;

	//#if NANO_RANKING == "true"
		public static final byte ENTRY_MAIN_MENU_NANO_RANKING	= ENTRY_MAIN_MENU_OPTIONS + 1;
		public static final byte ENTRY_MAIN_MENU_RECOMMEND	= ENTRY_MAIN_MENU_NANO_RANKING + 1;
	//#else
//# 		public static final byte ENTRY_MAIN_MENU_LOCAL_RANKING	= ENTRY_MAIN_MENU_OPTIONS + 1;
//# 		public static final byte ENTRY_MAIN_MENU_RECOMMEND	= ENTRY_MAIN_MENU_LOCAL_RANKING + 1;
	//#endif
	public static final byte ENTRY_MAIN_MENU_HELP		= ENTRY_MAIN_MENU_RECOMMEND + 1;
	public static final byte ENTRY_MAIN_MENU_CREDITS	= ENTRY_MAIN_MENU_HELP + 1;
	public static final byte ENTRY_MAIN_MENU_EXIT		= ENTRY_MAIN_MENU_CREDITS + 1;
	
	//#if DEBUG == "true"
	public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>


	public static final short SOFT_KEY_VISIBLE_TIME = 2700;

	// cores da barra de scroll
	public static final int COLOR_FULL_LEFT_OUT	= 0x000b0d;
	public static final int COLOR_FULL_FILL		= 0x0b3944;
	public static final int COLOR_FULL_RIGHT	= 0x032026;
	public static final int COLOR_FULL_LEFT		= 0x245865;
	public static final int COLOR_PAGE_OUT		= 0x002b30;
	public static final int COLOR_PAGE_FILL		= 0x245865;
	public static final int COLOR_PAGE_LEFT_1	= 0x418898;
	public static final int COLOR_PAGE_LEFT_2	= 0x61a2af;


	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 	public static final byte HEIGHT_MIN = 120;
//# 
//# 	public static final byte CONFIRM_TITLE_Y_OFFSET = 10;
//# 
//# 	public static final byte MENU_ENTRY_Y_OFFSET = -6;
//# 
//# 	/** Espessura da borda do tabuleiro. */
//# 	public static final byte BORDER_THICKNESS = 4;
//# 
//# 	/** Di�metro do indicador de tempo da tela de jogo. */
//# 	public static final byte CLOCK_DIAMETER = 15;
//# 	/** Posi��o x do rel�gio, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte CLOCK_POS_X = 23;
//# 	/** Posi��o y do rel�gio, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte CLOCK_POS_Y = 2;
//# 
//# 	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
//# 	public static final byte CLOCK_SCORE_SPACING = 1;
//# 
//# 	/** Largura total interna do indicador da pr�xima pe�a. */
//# 	public static final byte NEXT_PIECE_WIDTH = 18;
//# 	/** Altura total interna do indicador da pr�xima pe�a. */
//# 	public static final byte NEXT_PIECE_HEIGHT = 14;
//# 
//# 	/** Posi��o x da pr�xima pe�a, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte NEXT_PIECE_X = 2;
//# 	/** Posi��o y da pr�xima pe�a, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte NEXT_PIECE_Y = 3;
//# 
//# 	/** Largura total interna da barra de combo. */
//# 	public static final byte COMBO_BAR_WIDTH = 29;
//# 	/** Altura total interna da barra de combo. */
//# 	public static final byte COMBO_BAR_HEIGHT = 12;
//# 
//# 	/** Posi��o x da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte COMBO_BAR_X = 39;
//# 	/** Posi��o y da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte COMBO_BAR_Y = 4;
//# 
//# 	/** Di�metro padr�o do c�rculo dos menus. */
//# 	public static final short CIRCLE_DEFAULT_DIAMETER = 128;
//# 
//# 	public static final byte CIRCLE_THICKNESS_1 = 34;
//# 	public static final byte CIRCLE_THICKNESS_2 = 2;
//# 	public static final byte CIRCLE_THICKNESS_3 = 2;
//# 	public static final byte CIRCLE_THICKNESS_4 = 4;
//# 	public static final byte CIRCLE_THICKNESS_5 = 6;
//# 	public static final byte CIRCLE_THICKNESS_6 = 2;
//# 	public static final byte CIRCLE_THICKNESS_7 = 2;
//# 
//# 	public static final short CIRCLE_INTERNAL_DIAMETER = 72;
//# 
//# 	public static final byte CIRCLE_MIN_THICKNESS = CIRCLE_DEFAULT_DIAMETER >> 1;
//# 
//# 	/** Posi��o x relativa do logo Seda � imagem dos produtos. */
//# 	public static final byte SEDA_OFFSET_X = 78;
//# 	/** Posi��o y relativa do logo Seda � imagem dos produtos. */
//# 	public static final byte SEDA_OFFSET_Y = -34;
//# 
//# 	/** Altura da barra relativa ao centro do c�rculo do menu. */
//# 	public static final byte MENU_BAR_Y = ( CIRCLE_DEFAULT_DIAMETER >> 2 ) - 2;
//# 	/** Largura total da barra do menu. */
//# 	public static final short MENU_BAR_WIDTH = CIRCLE_DEFAULT_DIAMETER - 6;
//# 
//# 	public static final byte PIECE_DEFAULT_WIDTH = 18;
//# 	public static final byte PIECE_DEFAULT_HEIGHT = 18;
//# 
//# 	public static final byte SPLASH_TITLE_OFFSET_X = 5;
//# 
//#  	public static final short SPLASH_CIRCLE_X = 84;
//#  	public static final short SPLASH_CIRCLE_Y = 27;
//# 
//# 	public static final byte PUZZLE_SEDA_X = 12;
//# 	public static final byte PUZZLE_SEDA_Y = 6;
//# 
//#  	public static final byte SPLASH_BAR_Y_SPACING = 3;
//# 
//# 	public static final byte FONT_PUZZLE_CHAR_OFFSET = 1;
//# 
//# 	public static final byte SPLASH_CIRCLE_BORDER_THICKNESS = 8;
//# 
//# 	//</editor-fold>
//# 
	//#elif SCREEN_SIZE == "MEDIUM"
//#
//# 	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA M�DIA" defaultstate="collapsed">
//#
//# 	public static final byte CONFIRM_TITLE_Y_OFFSET = 10;
//#
//# 	public static final byte MENU_ENTRY_Y_OFFSET = -6;
//#
//# 	/** Espessura da borda do tabuleiro. */
//# 	public static final byte BORDER_THICKNESS = 4;
//#
//# 	/** Di�metro do indicador de tempo da tela de jogo. */
//# 	public static final byte CLOCK_DIAMETER = 20;
//# 	/** Posi��o x do rel�gio, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte CLOCK_POS_X = 27;
//# 	/** Posi��o y do rel�gio, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte CLOCK_POS_Y = 3;
//#
//# 	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
//# 	public static final byte CLOCK_SCORE_SPACING = 1;
//#
//# 	/** Largura total interna do indicador da pr�xima pe�a. */
//# 	public static final byte NEXT_PIECE_WIDTH = 20;
//# 	/** Altura total interna do indicador da pr�xima pe�a. */
//# 	public static final byte NEXT_PIECE_HEIGHT = 16;
//#
//# 	/** Posi��o x da pr�xima pe�a, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte NEXT_PIECE_X = 4;
//# 	/** Posi��o y da pr�xima pe�a, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte NEXT_PIECE_Y = 5;
//#
//# 	/** Largura total interna da barra de combo. */
//# 	public static final byte COMBO_BAR_WIDTH = 24;
//# 	/** Altura total interna da barra de combo. */
//# 	public static final byte COMBO_BAR_HEIGHT = 14;
//#
//# 	/** Posi��o x da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte COMBO_BAR_X = 48;
//# 	/** Posi��o y da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
//# 	public static final byte COMBO_BAR_Y = 6;
//#
//# 	/** Di�metro padr�o do c�rculo dos menus. */
//# 	public static final short CIRCLE_DEFAULT_DIAMETER = 172;
//#
//# 	public static final byte CIRCLE_THICKNESS_1 = 42;
//# 	public static final byte CIRCLE_THICKNESS_2 = 2;
//# 	public static final byte CIRCLE_THICKNESS_3 = 2;
//# 	public static final byte CIRCLE_THICKNESS_4 = 4;
//# 	public static final byte CIRCLE_THICKNESS_5 = 12;
//# 	public static final byte CIRCLE_THICKNESS_6 = 2;
//# 	public static final byte CIRCLE_THICKNESS_7 = 4;
//#
//# 	public static final short CIRCLE_INTERNAL_DIAMETER = 98;
//#
//# 	public static final byte CIRCLE_MIN_THICKNESS = CIRCLE_DEFAULT_DIAMETER >> 1;
//#
//# 	/** Posi��o x relativa do logo Seda � imagem dos produtos. */
//# 	public static final byte SEDA_OFFSET_X = 78;
//# 	/** Posi��o y relativa do logo Seda � imagem dos produtos. */
//# 	public static final byte SEDA_OFFSET_Y = -34;
//#
//# 	/** Altura da barra relativa ao centro do c�rculo do menu. */
//# 	public static final byte MENU_BAR_Y = ( CIRCLE_DEFAULT_DIAMETER >> 2 ) - 2;
//# 	/** Largura total da barra do menu. */
//# 	public static final short MENU_BAR_WIDTH = CIRCLE_DEFAULT_DIAMETER - 20;
//#
//#  	public static final byte SPLASH_TITLE_OFFSET_X = 11;
//#
//#  	public static final byte PIECE_DEFAULT_WIDTH = 24;
//#  	public static final byte PIECE_DEFAULT_HEIGHT = 24;
//#
//# 	public static final short SPLASH_CIRCLE_X = 108;
//# 	public static final short SPLASH_CIRCLE_Y = 35;
//#
//# 	public static final byte PUZZLE_SEDA_X = 19;
//# 	public static final byte PUZZLE_SEDA_Y = 16;
//#
//# 	public static final byte SPLASH_BAR_Y_SPACING = 4;
//#
//# 	public static final byte FONT_PUZZLE_CHAR_OFFSET = 2;
//#
//# 	public static final byte SPLASH_CIRCLE_BORDER_THICKNESS = 10;
//#
//# 	//</editor-fold>
//#
//#
	//#elif SCREEN_SIZE == "BIG"

	//<editor-fold desc="DEFINI��ES ESPEC�FICAS PARA TELA GRANDE" defaultstate="collapsed">

	public static final byte CONFIRM_TITLE_Y_OFFSET = 10;

	public static final byte MENU_ENTRY_Y_OFFSET = -6;

	/** Espessura da borda do tabuleiro. */
	public static final byte BORDER_THICKNESS = 6;

	/** Di�metro do indicador de tempo da tela de jogo. */
	public static final byte CLOCK_DIAMETER = 29;
	/** Posi��o x do rel�gio, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
	public static final byte CLOCK_POS_X = 30;
	/** Posi��o y do rel�gio, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
	public static final byte CLOCK_POS_Y = 2;

	/** Espa�amento do rel�gio e da pontua��o �s bordas da tela. */
	public static final byte CLOCK_SCORE_SPACING = 3;

	/** Largura total interna do indicador da pr�xima pe�a. */
	public static final byte NEXT_PIECE_WIDTH = 26;
	/** Altura total interna do indicador da pr�xima pe�a. */
	public static final byte NEXT_PIECE_HEIGHT = 23;

	/** Posi��o x da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
	public static final byte NEXT_PIECE_X = 3;
	/** Posi��o y da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
	public static final byte NEXT_PIECE_Y = 6;

	/** Largura total interna da barra de combo. */
	public static final byte COMBO_BAR_WIDTH = 66;
	/** Altura total interna da barra de combo. */
	public static final byte COMBO_BAR_HEIGHT = 21;

	/** Posi��o x da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
	public static final byte COMBO_BAR_X = 58;
	/** Posi��o y da barra de combo, relativa � imagem da barra (rel�gio, pr�xima pe�a e barra de combo). */
	public static final byte COMBO_BAR_Y = 7;

	/** Di�metro padr�o do c�rculo dos menus. */
	public static final short CIRCLE_DEFAULT_DIAMETER = 232;

	public static final byte CIRCLE_THICKNESS_1 = 42;
	public static final byte CIRCLE_THICKNESS_2 = 2;
	public static final byte CIRCLE_THICKNESS_3 = 2;
	public static final byte CIRCLE_THICKNESS_4 = 4;
	public static final byte CIRCLE_THICKNESS_5 = 18;
	public static final byte CIRCLE_THICKNESS_6 = 2;
	public static final byte CIRCLE_THICKNESS_7 = 4;

	public static final short CIRCLE_INTERNAL_DIAMETER = 144;

	public static final byte CIRCLE_MIN_THICKNESS = CIRCLE_DEFAULT_DIAMETER >> 1;

	/** Posi��o x relativa do logo Seda � imagem dos produtos. */
	public static final byte SEDA_OFFSET_X = 78;
	/** Posi��o y relativa do logo Seda � imagem dos produtos. */
	public static final byte SEDA_OFFSET_Y = -34;

	/** Altura da barra relativa ao centro do c�rculo do menu. */
	public static final byte MENU_BAR_Y = ( CIRCLE_DEFAULT_DIAMETER >> 2 ) - 2;
	/** Largura total da barra do menu. */
	public static final short MENU_BAR_WIDTH = CIRCLE_DEFAULT_DIAMETER - 20;

	public static final byte FONT_PUZZLE_CHAR_OFFSET = 3;

	public static final byte SPLASH_TITLE_OFFSET_X = 11;

	public static final byte PIECE_DEFAULT_WIDTH = 30;
	public static final byte PIECE_DEFAULT_HEIGHT = 30;

	public static final short SPLASH_CIRCLE_X = 160;
	public static final short SPLASH_CIRCLE_Y = 55;

	public static final byte PUZZLE_SEDA_X = 37;
	public static final byte PUZZLE_SEDA_Y = 20;

	public static final byte SPLASH_BAR_Y_SPACING = 6;

	public static final byte SPLASH_CIRCLE_BORDER_THICKNESS = 12;

	//</editor-fold>


	//#endif

	/** Espessura da borda do tabuleiro. */
	public static final byte BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;

}
