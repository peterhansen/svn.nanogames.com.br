/**
 * Clock.java
 * 
 * Created on Jul 24, 2009, 11:30:39 AM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class Clock extends DrawableGroup implements Constants {

	public static final int COLOR_CLOCK_DARK = 0x143238;
	public static final int COLOR_CLOCK_BLINK = 0x2f5b63;
	public static final int COLOR_CLOCK_LIGHT = 0x6eaeb0;

	private short degrees;

	private final Pattern comboBarEmpty;
	private final Pattern comboBarFull;

	private final Label labelCombo;

	private final DrawableImage[] miniPieces;

	private static DrawableImage[] MINI_PIECES;

	private static final short TIME_BLINK = 250;
	private static final short TIME_COMBO_BLINK = 140;

	private int colorDark;

	private long lastColorTime;
	private long lastComboColorTime;


	public Clock() throws Exception {
		super( 4 + PIECE_TYPES );

		comboBarFull = new Pattern( COLOR_CLOCK_LIGHT );
		comboBarFull.setPosition( COMBO_BAR_X, COMBO_BAR_Y );
		comboBarFull.setSize( 0, COMBO_BAR_HEIGHT );
		insertDrawable( comboBarFull );
		
		comboBarEmpty = new Pattern( COLOR_CLOCK_DARK );
		comboBarEmpty.setPosition( COMBO_BAR_X, COMBO_BAR_Y );
		comboBarEmpty.setSize( 0, COMBO_BAR_HEIGHT );
		insertDrawable( comboBarEmpty );

		labelCombo = new Label( FONT_COMBO, null );
		insertDrawable( labelCombo );
		
		final DrawableImage clock = new DrawableImage( PATH_IMAGES + "clock.png" );
		insertDrawable( clock );

		miniPieces = new DrawableImage[ PIECE_TYPES ];
		for ( byte i = 0; i < PIECE_TYPES; ++i ) {
			final DrawableImage p = new DrawableImage( MINI_PIECES[ i ] );
			miniPieces[ i ] = p;
			p.setPosition( NEXT_PIECE_X + ( ( NEXT_PIECE_WIDTH - p.getWidth() ) >> 1 ), NEXT_PIECE_Y + ( ( NEXT_PIECE_HEIGHT - p.getHeight() ) >> 1 ) );
			insertDrawable( p );
		}
		
		setSize( clock.getSize() );
		setNextPiece( null );
	}


	public static final void load() throws Exception {
		if ( MINI_PIECES == null ) {
			MINI_PIECES = new DrawableImage[ PIECE_TYPES ];

			for ( byte i = 0; i < PIECE_TYPES; ++i ) {
				MINI_PIECES[ i ] = new DrawableImage( PATH_PIECES + "s_" + i + ".png" );
			}
		}
	}


	protected final void paint( Graphics g ) {
		final int x = translate.x + CLOCK_POS_X;
		final int y = translate.y + CLOCK_POS_Y;
		g.setColor( colorDark );
		g.fillArc( x, y, CLOCK_DIAMETER, CLOCK_DIAMETER, 0, 360 );

		if ( degrees > 0 ) {
			g.setColor( COLOR_CLOCK_LIGHT );
			g.fillArc( x, y, CLOCK_DIAMETER, CLOCK_DIAMETER, 90, -degrees );
		}

		super.paint( g );
	}


	public final void setProgress( int timeLeft, int timeMax, int comboTime ) {
		degrees = ( short ) ( NanoMath.clamp( 360 * timeLeft / timeMax, 0, 360 ) );
		comboBarFull.setSize( comboTime * COMBO_BAR_WIDTH / TIME_COMBO, comboBarFull.getHeight() );
		comboBarEmpty.setSize( COMBO_BAR_WIDTH - comboBarFull.getWidth(), comboBarEmpty.getHeight() );
		comboBarEmpty.setPosition( comboBarFull.getPosX() + comboBarFull.getWidth(), comboBarEmpty.getPosY() );

		if ( timeLeft <= 0 ) {
			colorDark = COLOR_CLOCK_DARK;
		} else if ( timeLeft <= ( timeMax >> 2 ) ) {
			final long diff = System.currentTimeMillis() - lastColorTime;
			if ( diff > TIME_BLINK ) {
				colorDark = colorDark == COLOR_CLOCK_DARK ? COLOR_CLOCK_BLINK : COLOR_CLOCK_DARK;
				lastColorTime = System.currentTimeMillis();
			}
		} else {
			colorDark = COLOR_CLOCK_DARK;
		}

		if ( comboTime <= TIME_COMBO_START_BLINK ) {
			final long diff = System.currentTimeMillis() - lastComboColorTime;
			if ( diff > TIME_COMBO_BLINK ) {
				comboBarFull.setFillColor( comboBarFull.getFillColor() == COLOR_CLOCK_LIGHT ? COLOR_CLOCK_BLINK : COLOR_CLOCK_LIGHT );
				lastComboColorTime = System.currentTimeMillis();
			}
		} else {
			comboBarFull.setFillColor( COLOR_CLOCK_LIGHT );
		}
	}


	public final void setCombo( int combo ) {
		if ( combo > 1 ) {
			labelCombo.setVisible( true );
			//#if SCREEN_SIZE == "BIG"
				labelCombo.setText( combo + GameMIDlet.getText( TEXT_COMBO ) );
			//#else
//# 				labelCombo.setText( combo + "X" );
			//#endif
			labelCombo.setPosition( comboBarFull.getPosX() + ( ( COMBO_BAR_WIDTH - labelCombo.getWidth() ) >> 1 ),
									comboBarFull.getPosY() + ( ( COMBO_BAR_HEIGHT - labelCombo.getHeight() ) >> 1 ) );
		} else {
			labelCombo.setVisible( false );
		}
	}


	public final void setNextPiece( Sprite piece ) {
		for ( byte i = 0; i < PIECE_TYPES; ++i )
			miniPieces[ i ].setVisible( piece != null && i == piece.getFrameSequenceIndex() );
	}


}
