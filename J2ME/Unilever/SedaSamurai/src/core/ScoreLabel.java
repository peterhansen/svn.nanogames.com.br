/**
 * ScoreLabel.java
 * 
 * Created on Jul 20, 2009, 5:10:32 PM
 *
 */
package core;

import br.com.nanogames.components.Label;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;
import screens.PlayScreen;


/**
 *
 * @author Peter
 */
public class ScoreLabel extends Label implements Updatable, Constants {

	private static final short SPEED_MIN = ( short ) -( ScreenManager.SCREEN_HEIGHT >> 4 );

	private static final short SPEED_MAX_DIFF = ( short ) ( ScreenManager.SCREEN_HEIGHT >> 3 );

	private static final short LIFE_MIN = 1000;

	private static final short LIFE_MAX_DIFF = 1700;

	private short lifeTime;

	private final MUV muv = new MUV();

	public static final byte STATE_IDLE = 0;

	public static final byte STATE_APPEARING = 1;

	public static final byte STATE_VISIBLE = 2;

	public static final byte STATE_VANISHING = 3;

	private byte state;

	/** Tempo em milisegundos da anima��o de apari��o/sumi�o. */
	private static final short APPEARING_TIME = 210;

	private final PlayScreen playScreen;


	public ScoreLabel( PlayScreen screen ) throws Exception {
		super( GameMIDlet.getFont( FONT_SCORE_SMALL ), null );

		playScreen = screen;

		setViewport( new Rectangle() );

		setState( STATE_IDLE );
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_APPEARING:
				lifeTime += delta;

				if ( lifeTime < APPEARING_TIME ) {
					final int currentWidth = getWidth() * lifeTime / APPEARING_TIME;
					final int currentHeight = getHeight() * lifeTime / APPEARING_TIME;
					viewport.set( getRefPixelX() - ( currentWidth >> 1 ), getRefPixelY() - ( currentHeight >> 1 ), currentWidth, currentHeight );
				} else {
					setState( STATE_VISIBLE );
				}
				break;

			case STATE_VISIBLE:
				lifeTime -= delta;

				if ( lifeTime > 0 ) {
					move( 0, muv.updateInt( delta ) );
				} else {
					setState( STATE_VANISHING );
				}
				break;

			case STATE_VANISHING:
				lifeTime -= delta;

				if ( lifeTime > 0 ) {
					final int currentWidth = getWidth() * lifeTime / APPEARING_TIME;
					final int currentHeight = getHeight() * lifeTime / APPEARING_TIME;
					viewport.set( getRefPixelX() - ( currentWidth >> 1 ), getRefPixelY() - ( currentHeight >> 1 ), currentWidth, currentHeight );
				} else {
					setState( STATE_IDLE );
				}
				break;
		}
	}


	public final void setScore( Point pos, int score ) {
		setText( String.valueOf( score ) );
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		pos.x = NanoMath.clamp( pos.x, getWidth() >> 1, ScreenManager.SCREEN_WIDTH - ( getWidth() >> 1 ) );
		setRefPixelPosition( pos );
		setState( STATE_APPEARING );
	}


	public final void setState( byte state ) {
		switch ( state ) {
			case STATE_IDLE:
				setVisible( false );
				break;

			case STATE_APPEARING:
				setVisible( true );
				muv.setSpeed( SPEED_MIN - NanoMath.randInt( SPEED_MAX_DIFF ) );
				lifeTime = 0;
				viewport.set( 0, 0, 0, 0 );
				break;

			case STATE_VISIBLE:
				lifeTime = ( short ) ( LIFE_MIN + NanoMath.randInt( LIFE_MAX_DIFF ) );
				viewport.set( 0, 0, playScreen.getWidth(), ScreenManager.SCREEN_HEIGHT );
				break;

			case STATE_VANISHING:
				lifeTime = APPEARING_TIME;
				break;
		}

		this.state = state;
	}


	public final byte getState() {
		return state;
	}


}
