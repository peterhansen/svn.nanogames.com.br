//#if NANO_RANKING == "false"
//# /*
//#  * HighScoresScreen.java
//#  *
//#  * Created on October 3, 2007, 11:47 AM
//#  *
//#  */
//# package screens;
//# 
//# import br.com.nanogames.components.RichLabel;
//# import br.com.nanogames.components.userInterface.Menu;
//# import br.com.nanogames.components.userInterface.ScreenManager;
//# import br.com.nanogames.components.util.Serializable;
//# import core.Constants;
//# import java.io.DataInputStream;
//# import java.io.DataOutputStream;
//# 
//# 
//# /**
//#  *
//#  * @author peter
//#  */
//# public final class HighScoresScreen extends Menu implements Serializable, Constants {
//# 	
//# 	private static final byte TOTAL_SCORES = 5;
//# 	
//# 	private static final byte TOTAL_ITEMS = TOTAL_SCORES + 1;
//# 	
//# 	private static final int[] scores = new int[ TOTAL_SCORES ];
//# 	
//# 	private static String databaseName;
//# 	
//# 	private static int databaseSlot;
//# 	
//# 	private final RichLabel title;
//# 	private final RichLabel[] scoreLabels = new RichLabel[ TOTAL_SCORES ];
//# 	
//# 	private static HighScoresScreen instance;
//# 	
//# 	private byte lastHighScoreIndex = -1;
//# 	
//# 	private final short BLINK_RATE = 388;
//# 	
//# 	private int accTime;
//# 	
//# 	
//# 	/** Creates a new instance of HighScoresScreen */
//# 	private HighScoresScreen() throws Exception {
//# 		super( null, 0, TOTAL_ITEMS );
//# 
		//#if SCREEN_SIZE == "SMALL"
//# 			final byte ITEMS_SPACING = ( byte ) ( ScreenManager.SCREEN_HEIGHT < HEIGHT_MIN ? -14 : -12 );
		//#else
//# 			final byte ITEMS_SPACING = MENU_ENTRY_Y_OFFSET;
		//#endif
//# 		
//# 		title = new RichLabel( FONT_TEXT_WHITE, TEXT_HIGH_SCORES, ScreenManager.SCREEN_WIDTH );
//# 		insertDrawable( title );
//# 		
//# 		int yLabel = 0;
//# 
		//#if SCREEN_SIZE == "SMALL"
//# 			yLabel += title.getHeight() + ( ITEMS_SPACING >> 1 );
		//#else
//# 			yLabel += title.getHeight() + Math.abs( ITEMS_SPACING << 1 );
		//#endif
//# 		
//# 		for ( int i = 0; i < scoreLabels.length; ++i ) {
//# 			scoreLabels[ i ] = new RichLabel( FONT_TEXT_WHITE, null, ScreenManager.SCREEN_WIDTH );
//# 			final RichLabel tag = scoreLabels[ i ];
//# 			
//# 			tag.setPosition( 0, yLabel );
//# 			
//# 			yLabel += tag.getSize().y + ITEMS_SPACING;
//# 			
//# 			insertDrawable( scoreLabels[ i ] );
//# 		}
//# 
//# 		yLabel -= ITEMS_SPACING;
//# 		
//# 		setSize( ScreenManager.SCREEN_WIDTH, Math.min( yLabel, ScreenManager.SCREEN_HEIGHT ) );
//# 		defineReferencePixel( size.x >> 1, size.y >> 1 );
//# 		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
//# 	}
//# 	
//# 	
//# 	/**
//# 	 * Cria uma nova inst�ncia da tela de recordes, ou apenas uma refer�ncia para ela, caso j� tenha sido criada anteriormente.
//# 	 *
//# 	 * @param font fonte utilizada para criar os labels.
//# 	 * @throws java.lang.Exception caso haja problemas ao alocar recursos.
//# 	 * @return uma refer�ncia para a inst�ncia da tela de recordes.
//# 	 */
//# 	public static final HighScoresScreen createInstance( String databaseName, int databaseSlot ) throws Exception {
//# 		if ( instance == null ) {
//# 			instance = new HighScoresScreen();
//# 			
//# 			HighScoresScreen.databaseName = databaseName;
//# 			HighScoresScreen.databaseSlot = databaseSlot;
//# 			
//# 			loadRecords();
//# 		} // fim if ( instance == null )
//# 		
//# 		instance.updateLabels();
//# 		
//# 		return instance;
//# 	}
//# 	
//# 	
//# 	public static final boolean setScore( int score ) {
//# 		for ( int i = 0; i < scores.length; ++i ) {
//# 			if ( score > scores[ i ] ) {
//# 				// �ltima pontua��o � maior que uma pontua��o anteriormente gravada
//# 				for ( int j = scores.length - 1; j > i; --j ) {
//# 					scores[ j ] = scores[ j - 1 ];
//# 				}
//# 				
//# 				instance.lastHighScoreIndex = ( byte ) i;
//# 				instance.accTime = 0;
//# 				
//# 				scores[ i ] = score;
//# 				try {
//# 					GameMIDlet.saveData( databaseName, databaseSlot, instance );
//# 				} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 					e.printStackTrace();
					//#endif
//# 				}
//# 				
//# 				return true;
//# 			} // fim if ( score > scores[ i ] )
//# 		} // fim for ( int i = 0; i < scores.length; ++i )
//# 		
//# 		return false;		
//# 	} // fim do m�todo setScore( int )
//# 	
//# 
//# 	/**
//# 	 * Verifica se uma pontua��o � suficiente para entrar na lista de recordes.
//# 	 * @param score
//# 	 * @return �ndice da pontua��o na lista de ranking, ou -1 caso n�o seja um recorde.
//# 	 */
//# 	public static final int isHighScore( int score ) {
//# 		for ( int i = 0; i < scores.length; ++i ) {
//# 			if ( score > scores[ i ] )
//# 				return i;
//# 		}
//# 		
//# 		return -1;		
//# 	}
//# 	
//# 	
//# 	public final void keyPressed( int key ) {
//# 		switch ( key ) {
//# 			case ScreenManager.KEY_BACK:
//# 			case ScreenManager.KEY_SOFT_RIGHT:
//# 			case ScreenManager.KEY_CLEAR:
//# 			case ScreenManager.KEY_SOFT_LEFT:
//# 			case ScreenManager.FIRE:
//# 			case ScreenManager.KEY_NUM5:
//# 				stopBlink();
//# 
//# 				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 		} // fim switch ( key )
//# 	}
//# 
//# 	
//# 	public final void write( DataOutputStream output ) throws Exception {
//# 		for ( int i = 0; i < TOTAL_SCORES; ++i )
//# 			output.writeInt( scores[ i ] );		
//# 	}
//# 
//# 	
//# 	public final void read( DataInputStream input ) throws Exception {
//# 		for ( int i = 0; i < TOTAL_SCORES; ++i )
//# 			 scores[ i ] = input.readInt();		
//# 	}
//# 	
//# 	
//# 	private final void updateLabels() {
//# 		for ( int i = 0; i < scoreLabels.length; ++i ) {
//# 			final RichLabel tag = scoreLabels[ i ];
//# 			
//# 			tag.setText( ( i + 1 ) + ". " + String.valueOf( scores[ i ]) );
//# 			
//# 			tag.setPosition( 0, tag.getPosY() );
//# 		} // fim for ( int i = 0; i < scoreLabels.length; ++i )
//# 	} // fim do m�todo updateLabels()
//# 
//# 	
//# 	public final void update( int delta ) {
//# 		super.update( delta );
//# 		
//# 		if ( lastHighScoreIndex >= 0 ) {
//# 			accTime += delta;
//# 
//# 			if ( accTime >= BLINK_RATE ) {
//# 				accTime %= BLINK_RATE;
//# 				scoreLabels[ lastHighScoreIndex ].setVisible( !scoreLabels[ lastHighScoreIndex ].isVisible() );
//# 			}
//# 		}						
//# 	}
//# 	
//# 	
//# 	private final void stopBlink() {
//# 		if ( lastHighScoreIndex >= 0 )
//# 			scoreLabels[ lastHighScoreIndex ].setVisible( true );
//# 
//# 		lastHighScoreIndex = -1;				
//# 	}
//# 
//# 	
//# 	public static final void eraseRecords() {
//# 		try {
//# 			GameMIDlet.eraseSlot( databaseName, databaseSlot );
//# 		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 		}
//# 		
//# 		loadRecords();
//# 	}
//# 	
//# 	
//# 	private static final void loadRecords() {
//# 		try {
//# 			GameMIDlet.loadData( databaseName, databaseSlot, instance );
//# 		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			e.printStackTrace();
			//#endif
//# 			
//# 			// preenche a tabela de pontos com valores "dummy"
//# 			for ( int i = 0; i < TOTAL_SCORES; ++i )
//# 				scores[ i ] = ( TOTAL_SCORES - i ) * 200;
//# 
//# 			try {
//# 				GameMIDlet.saveData( databaseName, databaseSlot, instance );
//# 			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 				e.printStackTrace();
				//#endif					
//# 			}
//# 		}
//# 	}
//# 	
//# }
//# 
//#endif