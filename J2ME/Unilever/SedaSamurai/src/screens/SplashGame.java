/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import core.Puzzle;
import br.com.nanogames.components.util.Mutex;

//#if SCREEN_SIZE != "SMALL"
	//#if TOUCH == "true"
		import br.com.nanogames.components.userInterface.PointerListener;
	//#endif

//#endif
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Peter
 */
public final class SplashGame extends UpdatableGroup implements Constants, Updatable, KeyListener, ScreenListener
		//#if TOUCH == "true"
 		, PointerListener
		//#endif
{

	private static final short TIME_WAIT			= 358;
	private static final short TIME_CIRCLE_GROW		= 312;
	private static final short TIME_PIECES_ENTERING = 3551;
	private static final short TIME_PIECE_VIEWPORT	= 847;
	private static final short TIME_TITLE_ENTERS	= 561;

	private static final int COLOR_CIRCLE = 0xf47b21;

	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 20;

	private short accTime;

	private static final byte STATE_WAIT				= 0;
	private static final byte STATE_CIRCLE_GROW			= 1;
	private static final byte STATE_PIECES_APPEAR		= 2;
	private static final byte STATE_CIRCLE_SHRINK		= 4;
	private static final byte STATE_TITLE_ENTERS		= 5;
	private static final byte STATE_PRESS_ANY_KEY		= 6;

	private byte state;

	private final DrawableImage seda;

	private final Label titleSeda;
	
	private final Label titlePuzzle;

	private final DrawableGroup barGroup;

	private final RichLabel pressAnyKey;

	private static final short TIME_BLINK = 450;

	private final Drawable bkg;

	private final Pattern patternBar;

	private final Sprite[] flyingPieces;

	private final BezierCurve[] bezierCurves;

	private final short[] piecesTimes;

	private byte circleDiameter;

	private final short CIRCLE_MAX_DIAMETER;

	private final int[] circleMask;
	private final int[] temp;

	private final Image circle;

	private final Image circleIn;

	private final Point circlePosition = new Point();

	private final Rectangle pieceViewport = new Rectangle();

	private final Rectangle titleViewport = new Rectangle();

	/** Evita acessos concorrentes ao desenho da �rea interna do c�rculo, causando pequenos erros de desenho. */
	private final Mutex mutex = new Mutex();

	
	public SplashGame() throws Exception {
		super( TOTAL_ITEMS );

		bkg = Puzzle.loadBackground();
		bkg.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		insertDrawable( bkg );
		barGroup = new DrawableGroup( 4 );
		insertDrawable( barGroup );

		if ( GameMIDlet.isLowMemory() )
			patternBar = new Pattern( COLOR_BACKGROUND_LIGHT );
		else
			patternBar = new Pattern( new DrawableImage( PATH_IMAGES + "p.png" ) );

		barGroup.insertDrawable( patternBar );

		final ImageFont fontPuzzle = ImageFont.createMultiSpacedFont( PATH_SPLASH + "font_puzzle" );
		fontPuzzle.setCharOffset( FONT_PUZZLE_CHAR_OFFSET );
		titleSeda = new Label( fontPuzzle, "seda" );
		barGroup.insertDrawable( titleSeda );
		titlePuzzle = new Label( fontPuzzle, "PUZZLE" );
		barGroup.insertDrawable( titlePuzzle );
		barGroup.setViewport( titleViewport );

		seda = new DrawableImage( PATH_MENU + "seda.png" );
		barGroup.insertDrawable( seda );

		final Sprite s = new Sprite( PATH_SPLASH + "fp" );
		flyingPieces = new Sprite[ s.getSequence( 0 ).length ];
		bezierCurves = new BezierCurve[ flyingPieces.length ];
		piecesTimes = new short[ flyingPieces.length ];

		for ( byte i = 0; i < flyingPieces.length; ++i ) {
			final Sprite p = new Sprite( s );
			p.setFrame( i );
			flyingPieces[ i ] = p;
			p.setPosition( -1000, -1000 );
			if ( i == flyingPieces.length - 1 ) {
				p.setViewport( pieceViewport );
			}

			piecesTimes[ i ] = ( short ) ( ( TIME_PIECES_ENTERING >> 1 ) + ( i * ( TIME_PIECES_ENTERING >> 1 ) / piecesTimes.length ) );

			bezierCurves[ i ] = new BezierCurve( new Point( ScreenManager.SCREEN_WIDTH, NanoMath.randInt( ScreenManager.SCREEN_HEIGHT ) ),
												 new Point( NanoMath.randInt( ScreenManager.SCREEN_WIDTH ), NanoMath.randInt( ScreenManager.SCREEN_HEIGHT ) ),
												 new Point( NanoMath.randInt( ScreenManager.SCREEN_WIDTH ), NanoMath.randInt( ScreenManager.SCREEN_HEIGHT ) ),
												 new Point() );
		}

		circle = Image.createImage( PATH_SPLASH + "circle.png" );
		// o -2 � para evitar poss�veis falhas no desenho do c�rculo (j� que os algoritmos de rasteriza��o s�o dependentes de aparelho)
		CIRCLE_MAX_DIAMETER = ( short ) ( circle.getWidth() - 2 );
		circleMask = new int[ CIRCLE_MAX_DIAMETER * CIRCLE_MAX_DIAMETER ];
		temp = new int[ CIRCLE_MAX_DIAMETER * CIRCLE_MAX_DIAMETER ];

		circleIn = Image.createImage( CIRCLE_MAX_DIAMETER, CIRCLE_MAX_DIAMETER );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		pressAnyKey = new RichLabel( FONT_BIG, TEXT_PRESS_ANY_KEY, getWidth() );
		insertDrawable( pressAnyKey );
		pressAnyKey.setPosition( 0, getHeight() - pressAnyKey.getHeight() );
		pressAnyKey.setVisible( false );
	}


	public final void update( int delta ) {
		super.update( delta );

		switch ( state ) {
			case STATE_WAIT:
				accTime += delta;

				if ( accTime >= TIME_WAIT ) {
					setState( STATE_CIRCLE_GROW );
				}
			break;

			case STATE_CIRCLE_GROW:
				accTime += delta;
				refreshMask( false );

				if ( accTime >= TIME_CIRCLE_GROW ) {
					setState( STATE_PIECES_APPEAR );
				}
			break;

			case STATE_PIECES_APPEAR:
				accTime = ( short ) NanoMath.clamp( accTime + delta, 0, TIME_PIECES_ENTERING );
				final Point p = new Point();
				for ( byte i = 0; i < flyingPieces.length; ++i ) {
					final int fp_percent = NanoMath.divInt( accTime, piecesTimes[ i ] );
					bezierCurves[ i ].getPointAtFixed( p, fp_percent );
					flyingPieces[ i ].setPosition( p );
				}

				if ( accTime >= TIME_PIECES_ENTERING - TIME_PIECE_VIEWPORT ) {
					final int accTime2 = ( short ) NanoMath.clamp( accTime + delta - ( TIME_PIECES_ENTERING - TIME_PIECE_VIEWPORT ), 0, TIME_PIECE_VIEWPORT );
					final int frameHeight = flyingPieces[ flyingPieces.length - 1 ].getCurrentFrameImage().getHeight();
					pieceViewport.set( 0, circleIn.getHeight() - frameHeight, circleIn.getWidth(), frameHeight * accTime2 / ( TIME_PIECE_VIEWPORT / 5 ) );
					refreshMask( true );
					
					if ( accTime >= TIME_PIECES_ENTERING )
						setState( STATE_CIRCLE_SHRINK );
				}
			break;

			case STATE_CIRCLE_SHRINK:
				accTime -= delta;
				refreshMask( false );
				if ( accTime <= 0 ) {
					setState( STATE_TITLE_ENTERS );
				}
			break;

			case STATE_TITLE_ENTERS:
				accTime = ( short ) NanoMath.clamp( accTime + delta, 0, TIME_TITLE_ENTERS );
				final int width = getWidth() * accTime / TIME_TITLE_ENTERS;
				titleViewport.width = width;
				titleViewport.x = ( getWidth() - width ) >> 1;

				if ( accTime >= TIME_TITLE_ENTERS ) {
					setState( STATE_PRESS_ANY_KEY );
				}
			break;

			case STATE_PRESS_ANY_KEY:
				accTime += delta;
				if ( accTime >= TIME_BLINK ) {
					accTime %= TIME_BLINK;
					pressAnyKey.setVisible( !pressAnyKey.isVisible() );
				}
			break;
		}
	}


	private final void refreshMask( boolean useFullSize ) {
		mutex.acquire();

		if ( useFullSize )
			circleDiameter = ( byte ) CIRCLE_MAX_DIAMETER;
		else
			circleDiameter = ( byte ) ( NanoMath.clamp( accTime, 0, TIME_CIRCLE_GROW ) * CIRCLE_MAX_DIAMETER / TIME_CIRCLE_GROW );

		final Graphics g = circleIn.getGraphics();
		g.setColor( 0x000000 );
		g.fillRect( 0, 0, circleIn.getWidth(), circleIn.getHeight() );
		g.drawImage( circle, 0, 0, 0 );

		switch ( state ) {
			case STATE_PIECES_APPEAR:
				if ( accTime < TIME_PIECES_ENTERING - TIME_PIECE_VIEWPORT )
					break;
				
			case STATE_CIRCLE_SHRINK:
			case STATE_TITLE_ENTERS:
			case STATE_PRESS_ANY_KEY:
				for ( byte i = 0; i < flyingPieces.length; ++i ) {
					flyingPieces[ i ].setPosition( 0, 0 );
					flyingPieces[ i ].draw( g );
				}
			break;
		}

		circleIn.getRGB( circleMask, 0, circleIn.getWidth(), 0, 0, circleIn.getWidth(), circleIn.getHeight() );

		final int teste = g.getDisplayColor( 0xffffff );
		g.setColor( 0xffffff );
		g.fillArc( ( circleIn.getWidth() - circleDiameter ) >> 1, ( circleIn.getHeight() - circleDiameter ) >> 1, circleDiameter, circleDiameter, 0, 360 );
		circleIn.getRGB( temp, 0, circleIn.getWidth(), 0, 0, circleIn.getWidth(), circleIn.getHeight() );
		for ( int i = 0; i < circleMask.length; ++i ) {
			if ( ( temp[ i ] & 0x00ffffff ) != teste )
				circleMask[ i ] = 0x00000000;
		}

		mutex.release();
	}


	protected void paint( Graphics g ) {
		super.paint( g );

		fillCircle( g, circlePosition.x + ( circleIn.getWidth() >> 1 ), circlePosition.y + ( circleIn.getHeight() >> 1 ), circleDiameter );
		if ( mutex.acquire() )
			g.drawRGB( circleMask, 0, circleIn.getWidth(), circlePosition.x, circlePosition.y, circleIn.getWidth(), circleIn.getHeight(), true );
		mutex.release();

		switch ( state ) {
			case STATE_CIRCLE_SHRINK:
			case STATE_TITLE_ENTERS:
			case STATE_PRESS_ANY_KEY:
			break;

			case STATE_PIECES_APPEAR:
				if ( accTime >= TIME_PIECES_ENTERING - TIME_PIECE_VIEWPORT )
						break;
			default:
				for ( byte i = 0; i < flyingPieces.length; ++i )
					flyingPieces[ i ].draw( g );
		}
	}


	private final void setState( int state ) {
		accTime = 0;
		this.state = ( byte ) state;

		switch ( state ) {
			case STATE_CIRCLE_SHRINK:
				accTime = TIME_CIRCLE_GROW;
				refreshMask( true );
			break;

			case STATE_TITLE_ENTERS:
			break;

			case STATE_PRESS_ANY_KEY:
				pressAnyKey.setVisible( true );
			break;
		}
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
			// permite pular a tela de splash
			setState( STATE_PRESS_ANY_KEY );
		//#endif
		
		if ( state == STATE_PRESS_ANY_KEY ) {
			//#if DEMO == "true"
//# 			GameMIDlet.setScreen( SCREEN_PLAYS_REMAINING );
			//#else
				GameMIDlet.setScreen( SCREEN_LOADING_2 );
			//#endif
		}
	}


	public final void keyReleased( int key ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public void setSize( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			super.setSize( width, height );
			bkg.setRefPixelPosition( width >> 1, height >> 1 );
			if ( bkg.getPosX() + SPLASH_CIRCLE_X + CIRCLE_MAX_DIAMETER + SPLASH_CIRCLE_BORDER_THICKNESS > width )
				bkg.move( width - bkg.getPosX() - SPLASH_CIRCLE_X - CIRCLE_MAX_DIAMETER - SPLASH_CIRCLE_BORDER_THICKNESS, 0 );
			
			circlePosition.set( bkg.getPosX() + SPLASH_CIRCLE_X, bkg.getPosY() + SPLASH_CIRCLE_Y );

			for ( byte i = 0; i < bezierCurves.length; ++i )
				bezierCurves[ i ].destiny.set( circlePosition );

			barGroup.setSize( width, seda.getHeight() + ( SPLASH_BAR_Y_SPACING << 1 ) );
			barGroup.setPosition( barGroup.getPosX(), ( ( height * 3 ) >> 2 ) - ( barGroup.getHeight() >> 1 ) );
			titleViewport.y = barGroup.getPosY();
			titleViewport.height = barGroup.getHeight();

			final int START_X = width >> 4;
			seda.setPosition( START_X, ( barGroup.getHeight() - seda.getHeight() ) >> 1 );
			titleSeda.setPosition( START_X + seda.getWidth() + SPLASH_TITLE_OFFSET_X, seda.getPosY() );
			titlePuzzle.setPosition( titleSeda.getPosX(), seda.getPosY() + seda.getHeight() - titlePuzzle.getHeight() );
			patternBar.setSize( barGroup.getSize() );
			patternBar.setPosition( 0, seda.getPosY() - SPLASH_BAR_Y_SPACING );
		}
	}


	public final void hideNotify() {
	}


	public final void showNotify() {
	}


	private final void fillCircle( Graphics g, int x, int y, int size ) {
		if ( size > 0 ) {
			g.setColor( COLOR_CIRCLE );
			size += SPLASH_CIRCLE_BORDER_THICKNESS;
			g.fillArc( x - ( size >> 1 ), y - ( size >> 1 ), size, size, 0, 360 );
		}
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
		}


		public final void onPointerPressed( int x, int y ) {
			keyPressed( 0 );
		}


		public final void onPointerReleased( int x, int y ) {
		}

	//#endif

}
