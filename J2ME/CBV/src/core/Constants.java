/**
 * Constants.java
 * _2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

/**
 *
 * @author Peter
 */
public interface Constants {
	
	/*******************************************************************************************
	 *                              DEFINI__ES GEN_RICAS                                       *
	 *******************************************************************************************/

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	/** Nome curto do jogo, para submiss_o ao Nano Online. */
	public static final String APP_SHORT_NAME = "CBVN";

	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";

	//#if DEBUG == "true"
		/** Endereço raiz do Nano Online. */
 		public static final String NANO_ONLINE_URL = "http://127.0.0.1:3000/";
	//#else
//# 		/** Endereço raiz do Nano Online. */
//# 		public static final String NANO_ONLINE_URL = "http://online.nanogames.com.br/";
	//#endif

	/***/
	public static final byte CBV_NEWS_CATEGORY_ID = 3;

	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	public static final String PATH_BKG = PATH_IMAGES + "bkg/";
	
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";

	/** Caminho das imagens das bordas. */
	public static final String PATH_BORDER = PATH_IMAGES + "border/";

	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";
	
	public static final String PATH_MENU = PATH_IMAGES + "menu/";
	public static final String PATH_PRODUCTS = PATH_IMAGES + "products/";

	public static final byte TOTAL_PRODUCTS = 9;

	public static final String PATH_FRUITS_GIANT_DESCRIPTOR = PATH_IMAGES + "fruits_giant.bin";
	public static final String PATH_FRUITS_DESCRIPTOR = PATH_IMAGES + "fruits.bin";
	public static final String PATH_FRUITS_DESCRIPTOR_LOW = PATH_IMAGES + "fruits_low.bin";
	public static final String PATH_FRUITS_PREFIX = PATH_IMAGES + "fruits";

	public static final String PATH_SHINE_PREFIX = PATH_IMAGES + "shine";
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	
	/** _ndice do slot de grava__o das op__es na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** _ndice do slot de grava__o de um campeonato salvo. */
	public static final byte DATABASE_SLOT_HIGH_SCORES = 2;

	public static final byte DATABASE_SLOT_LANGUAGE = 3;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 3;

	public static final byte RANKING_TYPE_SCORE = 0;

	
	/** Cor padr_o do fundo de tela. */
	public static final int BACKGROUND_COLOR = 0x1d4c78;

	public static final int COLOR_SCROLL_BACK = 0xf7f0d4;
	public static final int COLOR_SCROLL_FORE = 0x945b01;

	public static final int COLOR_SPLASH_TOP = 0xfff7b2;

	public static final byte VIBRATION_SCREEN_OFFSET = 6;
	public static final byte VIBRATION_SCREEN_HALF_OFFSET = VIBRATION_SCREEN_OFFSET >> 1;

	
	/** _ndice da fonte padr_o do jogo. */
	public static final byte FONT_MENU	= 0;

	/** _ndice da fonte padr_o do jogo. */
	public static final byte FONT_BLACK	= 1;
	
	/** _ndice da fonte utilizada para textos. */
	public static final byte FONT_WHITE	= 2;

	/** _ndice da fonte utilizada para mostrar mensagens na tela de jogo. */
	public static final byte FONT_BIG	= 3;
	
	/** _ndice da fonte utilizada par TODO. */
	public static final byte FONT_POINTS	= 4;

	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 5;
	
	//_ndices dos sons do jogo
	public static final byte SOUND_INDEX_SPLASH			= 0;
	public static final byte SOUND_INDEX_LEVEL_COMPLETE	= 1;
	public static final byte SOUND_INDEX_TIME_UP		= 2;
	public static final byte SOUND_INDEX_GAME_OVER		= 3;

	//#if JAR == "min"
//# 		public static final byte SOUND_INDEX_AMBIENT_1		= SOUND_INDEX_SPLASH;
//# 		public static final byte SOUND_TOTAL = 4;
	//#else
		public static final byte SOUND_INDEX_AMBIENT_1		= 4;
		public static final byte SOUND_INDEX_AMBIENT_2		= 5;

		public static final byte SOUND_TOTAL = 6;
	//#endif

	//#ifdef VIBRATION_BUG
//# 		/** Dura__o padr_o da vibra__o, em milisegundos. */
//# 		public static final short VIBRATION_TIME_DEFAULT = 150;
//#
//# 		/***/
//# 		public static final short VIBRATION_TIME_EXPLOSION = 200;
	//#else
		/** Dura__o padr_o da vibra__o, em milisegundos. */
		public static final short VIBRATION_TIME_DEFAULT = 350;

		/***/
		public static final short VIBRATION_TIME_EXPLOSION = 500;
	//#endif
	
	// <editor-fold defaultstate="collapsed" desc="_NDICES DOS TEXTOS">
	public static final byte TEXT_OK							= 0;
	public static final byte TEXT_BACK							= TEXT_OK + 1;
	public static final byte TEXT_NEWS							= TEXT_BACK + 1;
	public static final byte TEXT_EXIT							= TEXT_NEWS + 1;
	public static final byte TEXT_OPTIONS						= TEXT_EXIT + 1;
	public static final byte TEXT_CALENDAR						= TEXT_OPTIONS + 1;
	public static final byte TEXT_PAUSE							= TEXT_CALENDAR + 1;
	public static final byte TEXT_CREDITS						= TEXT_PAUSE + 1;
	public static final byte TEXT_CREDITS_TEXT					= TEXT_CREDITS + 1;
	public static final byte TEXT_HELP							= TEXT_CREDITS_TEXT + 1;
	public static final byte TEXT_OBJECTIVES					= TEXT_HELP + 1;
	public static final byte TEXT_CONTROLS						= TEXT_OBJECTIVES + 1;
	public static final byte TEXT_RECOMMEND_SENT				= TEXT_CONTROLS + 1;
	public static final byte TEXT_RECOMMEND_TEXT				= TEXT_RECOMMEND_SENT + 1;
	public static final byte TEXT_TIPS							= TEXT_RECOMMEND_TEXT + 1;
	public static final byte TEXT_HELP_OBJECTIVES				= TEXT_TIPS + 1;
	public static final byte TEXT_HELP_CONTROLS					= TEXT_HELP_OBJECTIVES + 1;
	public static final byte TEXT_PRODUCTS_TITLE				= TEXT_HELP_CONTROLS + 1;
	public static final byte TEXT_PRODUCTS_TEXT					= TEXT_PRODUCTS_TITLE + 1;
	public static final byte TEXT_TURN_SOUND_ON					= TEXT_PRODUCTS_TEXT + 1;
	public static final byte TEXT_TURN_SOUND_OFF				= TEXT_TURN_SOUND_ON + 1;
	public static final byte TEXT_TURN_VIBRATION_ON				= TEXT_TURN_SOUND_OFF + 1;
	public static final byte TEXT_TURN_VIBRATION_OFF			= TEXT_TURN_VIBRATION_ON + 1;
	public static final byte TEXT_CANCEL						= TEXT_TURN_VIBRATION_OFF + 1;
	public static final byte TEXT_CONTINUE						= TEXT_CANCEL + 1;
	public static final byte TEXT_SPLASH_NANO					= TEXT_CONTINUE + 1;
	public static final byte TEXT_SPLASH_BRAND					= TEXT_SPLASH_NANO + 1;
	public static final byte TEXT_LOADING						= TEXT_SPLASH_BRAND + 1;	
	public static final byte TEXT_DO_YOU_WANT_SOUND				= TEXT_LOADING + 1;
	public static final byte TEXT_YES							= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final byte TEXT_CLEAR							= TEXT_YES + 1;
	public static final byte TEXT_NO							= TEXT_CLEAR + 1;
	public static final byte TEXT_VERSION						= TEXT_NO + 1;
	public static final byte TEXT_RECOMMEND_SMS					= TEXT_VERSION + 1;
	public static final byte TEXT_RECOMMEND_TITLE				= TEXT_RECOMMEND_SMS + 1;
	public static final byte TEXT_BACK_MENU						= TEXT_RECOMMEND_TITLE + 1;
	public static final byte TEXT_CONFIRM_BACK_MENU				= TEXT_BACK_MENU + 1;
	public static final byte TEXT_EXIT_GAME						= TEXT_CONFIRM_BACK_MENU + 1;
	public static final byte TEXT_CONFIRM_EXIT					= TEXT_EXIT_GAME + 1;
	public static final byte TEXT_PRESS_ANY_KEY					= TEXT_CONFIRM_EXIT + 1;
	public static final byte TEXT_UPDATE						= TEXT_PRESS_ANY_KEY + 1;
	public static final byte TEXT_ERROR							= TEXT_UPDATE + 1;
	public static final byte TEXT_SEE_MORE							= TEXT_ERROR + 1;
	public static final byte TEXT_LIFE_LOST						= TEXT_SEE_MORE + 1;
	public static final byte TEXT_COMPLETE						= TEXT_LIFE_LOST + 1;
	public static final byte TEXT_BEAUTY_TIP					= TEXT_COMPLETE + 1;
	public static final byte TEXT_LOG_TITLE						= TEXT_BEAUTY_TIP + 1;
	public static final byte TEXT_LOG_TEXT						= TEXT_LOG_TITLE + 1;
	public static final byte TEXT_LOADING_GENERIC			    = TEXT_LOG_TEXT + 1;
	public static final byte TEXT_PORTUGUESE			        = TEXT_LOADING_GENERIC + 1;
	public static final byte TEXT_ENGLISH			            = TEXT_PORTUGUESE + 1;
	public static final byte TEXT_CHOOSE_YOUR_LANGUAGE	        = TEXT_ENGLISH + 1;
	public static final byte TEXT_NANO_ONLINE	                = TEXT_CHOOSE_YOUR_LANGUAGE + 1;
	public static final byte TEXT_CURRENT_PROFILE		        = TEXT_NANO_ONLINE + 1;
    public static final byte TEXT_CHOOSE_ANOTHER		        = TEXT_CURRENT_PROFILE + 1;
    public static final byte TEXT_CONFIRM				        = TEXT_CHOOSE_ANOTHER + 1;
    public static final byte TEXT_NO_PROFILE				    = TEXT_CONFIRM + 1;


	/** n_mero total de textos do jogo */

	public static final byte TEXT_TOTAL = TEXT_NO_PROFILE +1;
	
	// </editor-fold>	
	
	// <editor-fold defaultstate="collapsed" desc="_NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_SPLASH_NANO					= 0;
	public static final byte SCREEN_SPLASH_BRAND				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME					= SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU					= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_HELP						= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_HELP + 1;
	public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_CREDITS						= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_LOADING_1					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_LOADING_2					= SCREEN_LOADING_1 + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_NEWS_SELECT					= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_NEWS_VIEW					= SCREEN_NEWS_SELECT + 1;
	public static final byte SCREEN_REFRESH_NEWS				= SCREEN_NEWS_VIEW + 1;
	public static final byte SCREEN_CALENDAR					= SCREEN_REFRESH_NEWS + 1;

	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="_NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="_NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="_NDICES DAS ENTRADAS DA TELA DE PAUSA">
	
	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 4;
	
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 2;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;	
	
	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 1;
	
	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 2;
			
	
	// </editor-fold>


	public static final short SOFT_KEY_VISIBLE_TIME = 3800;

	public static final byte CLOCK_Y_OFFSET = 2;

	public static final byte HINT_SEQUENCE_IDLE			= 0;
	public static final byte HINT_SEQUENCE_ANIMATING	= 1;

	// cores da barra de scroll
	public static final int COLOR_FULL_LEFT_OUT	= 0x0059a7;
	public static final int COLOR_FULL_FILL		= 0x0059a7;//fundo barra
	public static final int COLOR_FULL_RIGHT	= 0x0090c7;
	public static final int COLOR_FULL_LEFT		= 0x0090c7;
	public static final int COLOR_PAGE_OUT		= 0x0090c7;
	public static final int COLOR_PAGE_FILL		= 0x0059a7;//barra
	public static final int COLOR_PAGE_LEFT_1	= 0x0090c7;
	public static final int COLOR_PAGE_LEFT_2	= 0x0090c7;//sombra barra scroll

	
	//#if SCREEN_SIZE == "SMALL"
//#
//# 	//<editor-fold desc="DEFINI__ES ESPEC_FICAS PARA TELA PEQUENA" defaultstate="collapsed">
//#
//# 	public static final byte SCORE_PATTERN_WIDTH = 48;
//#
//# 	public static final byte TITLE_SPACING = 8;
//#
//# 	public static final byte ENTRIES_SPACING = 2;
//#
//# 	public static final short HEIGHT_MIN = 128;
//#
//# 	public static final short HEIGHT_DEFAULT = 128;
//#
//# 	public static final byte GAME_INFO_SPACING = 8;
//#
//# 	public static final byte TIMEBAR_SPACING = 4;
//#
//# 	/** Dist_ncia padr_o da parte inferior da borda _ parte inferior da tela. */
//# 	public static final byte BOARD_DEFAULT_BOTTOM_Y = -9;
//#
//#  	public static final short MIN_HEIGHT_PORTRAIT_LUX = 200;
//#
//# 	public static final byte BORDER_THICKNESS = 3;
//#
//# 	/** Mem_ria m_nima necess_ria para carregar todas as fontes do jogo. */
//# 	public static final int LOAD_ALL_FONTS_MEMORY = 1600000;
//#
//# 	//</editor-fold>
//#
	//#elif SCREEN_SIZE == "MEDIUM"
//#
//# 	//<editor-fold desc="DEFINI__ES ESPEC_FICAS PARA TELA M_DIA" defaultstate="collapsed">
//#
//# 	public static final byte SCORE_PATTERN_WIDTH = 70;
//#
//# 	public static final byte TITLE_SPACING = 10;
//#
//# 	public static final byte ENTRIES_SPACING = 4;
//#
//# 	public static final short HEIGHT_MIN = 190;
//#
//# 	public static final short HEIGHT_DEFAULT = 206;
//#
//# 	public static final byte GAME_INFO_SPACING = 8;
//#
//# 	public static final byte TIMEBAR_SPACING = 8;
//#
//# 	/** Dist_ncia padr_o da parte inferior da borda _ parte inferior da tela. */
//# 	public static final byte BOARD_DEFAULT_BOTTOM_Y = -24;
//#
//#  	public static final short MIN_HEIGHT_PORTRAIT_LUX = 200;
//#
//# 		public static final byte BORDER_THICKNESS = 4;
//# 	//</editor-fold>
//#
//#
	//#elif SCREEN_SIZE == "BIG"

	//<editor-fold desc="DEFINI__ES ESPEC_FICAS PARA TELA GRANDE" defaultstate="collapsed">

	public static final byte SCORE_PATTERN_WIDTH = 100;

	public static final byte TITLE_SPACING = 10;

	public static final byte ENTRIES_SPACING = 12;

	public static final short HEIGHT_MIN = 280;

	public static final short HEIGHT_DEFAULT = 320;

	public static final byte GAME_INFO_SPACING = 16;

	public static final byte TIMEBAR_SPACING = 10;

	/** Dist_ncia padr_o da parte inferior da borda _ parte inferior da tela. */
	public static final byte BOARD_DEFAULT_BOTTOM_Y = -39;

	/***/
	public static final short MIN_HEIGHT_PORTRAIT_LUX = 320;

	/** Mem_ria m_nima em bytes para que todas as imagens de explos_o e liquefa__o sejam carregadas. */
	public static final int LOAD_ALL_MEMORY_SAMSUNG = 2500000;

	public static final byte BORDER_THICKNESS = 4;

	//</editor-fold>


	//#elif SCREEN_SIZE == "SUPER_BIG"
//#
//# 	//<editor-fold desc="DEFINI__ES ESPEC_FICAS PARA TELA GRANDE" defaultstate="collapsed">
//#
//# 	public static final byte SCORE_PATTERN_WIDTH = 100;
//#
//# 	public static final byte TITLE_SPACING = 10;
//#
//# 	public static final byte ENTRIES_SPACING = 5;
//#
//# 	public static final short HEIGHT_MIN = 320;
//#
//# 	public static final short HEIGHT_DEFAULT = 320;
//#
//# 	public static final byte GAME_INFO_SPACING = 16;
//#
//# 	public static final byte TIMEBAR_SPACING = 10;
//#
//# 	/** Dist_ncia padr_o da parte inferior da borda _ parte inferior da tela. */
//# 	public static final byte BOARD_DEFAULT_BOTTOM_Y = -39;
//#
//# 	/***/
//# 	public static final short MIN_HEIGHT_PORTRAIT_LUX = 320;
//#
//# 	/** Mem_ria m_nima em bytes para que todas as imagens de explos_o e liquefa__o sejam carregadas. */
//# 	public static final int LOAD_ALL_MEMORY_SAMSUNG = 2500000;
//#
//# 	public static final byte BORDER_THICKNESS = 8;
//#
//# 	//</editor-fold>
//#
//#
	//#endif


	public static final byte BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;
	
}
