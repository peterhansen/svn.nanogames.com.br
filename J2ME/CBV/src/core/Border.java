/**
 * Border.java
 * 
 * Created on Mar 2, 2009, 2:33:55 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;

/**
 *
 * @author Peter
 */
public final class Border extends br.com.nanogames.components.userInterface.form.borders.Border implements Constants {

	private static final int COLOR_FILL = 0xe1c27e;
	private static final int COLOR_FILL_FOCUSED = 0xefdeb3;
	private static final int COLOR_FILL_PRESSED = 0xd1a553;

	private final Pattern patternLeft;
	private final Pattern patternRight;
	private final Pattern patternTop;
	private final Pattern patternBottom;

	private final Pattern fill;

	private final DrawableImage topLeft;
	private final DrawableImage topRight;
	private final DrawableImage bottomLeft;
	private final DrawableImage bottomRight;


	public Border() throws Exception {
		super( 9 );

		patternLeft = new Pattern( new DrawableImage( PATH_BORDER + "left.png" ) );
		patternLeft.setSize( patternLeft.getFill().getWidth(), 0 );
		insertDrawable( patternLeft );

		patternTop = new Pattern( new DrawableImage( PATH_BORDER + "top.png" ) );
		patternTop.setSize( 0, patternTop.getFill().getHeight() );
		insertDrawable( patternTop );

		//#if SCREEN_SIZE == "SMALL"
//# 			patternRight = new Pattern( patternLeft.getFill() );
//# 			patternBottom = new Pattern( patternTop.getFill() );
		//#else
			patternRight = new Pattern( new DrawableImage( PATH_BORDER + "right.png" ) );
			patternBottom = new Pattern( new DrawableImage( PATH_BORDER + "bottom.png" ) );
		//#endif
		patternRight.setSize( patternRight.getFill().getWidth(), 0 );
		insertDrawable( patternRight );
		patternBottom.setSize( 0, patternBottom.getFill().getHeight() );
		insertDrawable( patternBottom );

		fill = new Pattern( COLOR_FILL );
		insertDrawable( fill );

		topLeft = new DrawableImage( PATH_BORDER + "tl.png" );
		insertDrawable( topLeft );

		bottomLeft = new DrawableImage( PATH_BORDER + "bl.png" );
		bottomLeft.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		insertDrawable( bottomLeft );
		
		topRight = new DrawableImage( PATH_BORDER + "tr.png" );
		topRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_TOP );
		insertDrawable( topRight);

		bottomRight = new DrawableImage( PATH_BORDER + "br.png" );
		bottomRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_BOTTOM );
		insertDrawable( bottomRight );

		patternTop.setPosition( topLeft.getWidth(), 0 );
		patternLeft.setPosition( 0, topLeft.getHeight());
		patternRight.setPosition( 0, topRight.getHeight() );
		fill.setPosition( topLeft.getSize() );

		setLeft( patternLeft.getWidth() + 1 );
		setRight( patternRight.getWidth() + 1 );
		setTop( patternTop.getHeight() + 1 );
		setBottom( patternBottom.getHeight() + 1 );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		bottomLeft.setRefPixelPosition( 0, height );
		topRight.setRefPixelPosition( width, 0 );
		bottomRight.setRefPixelPosition( width, height );

		patternTop.setSize( width - patternTop.getPosX() - topRight.getWidth(), patternTop.getHeight() );
		patternBottom.setSize( patternTop.getWidth(), patternBottom.getHeight() );
		patternLeft.setSize( patternLeft.getWidth(), height - patternLeft.getPosY() - bottomLeft.getHeight() );
		patternRight.setSize( patternRight.getWidth(), patternLeft.getHeight() );

		fill.setSize( patternTop.getWidth(), patternLeft.getHeight() );

		patternBottom.setPosition( fill.getPosX(), bottomLeft.getPosY() );
		patternRight.setPosition( topRight.getPosX(), topRight.getHeight() );
	}


	public final br.com.nanogames.components.userInterface.form.borders.Border getCopy() throws Exception {
		return new Border();
	}


	public final void setState( int state ) {
		super.setState( state );

		if ( fill != null)  {
			switch ( state ) {
				case STATE_FOCUSED:
					fill.setFillColor( COLOR_FILL_FOCUSED );
				break;

				case STATE_PRESSED:
					fill.setFillColor( COLOR_FILL_PRESSED );
				break;

				case STATE_ERROR:
				case STATE_UNFOCUSED:
				case STATE_WARNING:
					fill.setFillColor( COLOR_FILL );
				break;
			}
		}
	}


}
