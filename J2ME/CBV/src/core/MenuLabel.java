/**
 * MenuLabel.java
 * 
 * Created on Mar 2, 2009, 3:14:44 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class MenuLabel extends DrawableGroup implements Constants {

	public static final short WIDTH = 180;
	public static final byte HEIGHT = 40;

	private final Label label;

	private boolean active;


	public MenuLabel( int textIndex ) throws Exception {
		super( 3 );

		final Pattern p = new Pattern( 0x0059a7 );
		p.setSize( WIDTH, HEIGHT );
		insertDrawable( p );

		label = new Label( GameMIDlet.getFont( FONT_MENU ), GameMIDlet.getText( textIndex ) );
		insertDrawable( label );

		label.setPosition( ( WIDTH - label.getWidth() ) >> 1, ( HEIGHT - label.getHeight() ) >> 1 );

		setSize( WIDTH, HEIGHT );
		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		setText( textIndex );
	}


	public static final void load() throws Exception {
	}


	public final void setActive( boolean active ) {
		this.active = active;
	}


	public final void setText( int textIndex ) {
		label.setText( textIndex );
		
		setActive( active );
	}

}
