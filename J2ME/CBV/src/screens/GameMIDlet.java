/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Border;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Graphics;

//#ifndef NO_RECOMMEND
	import br.com.nanogames.components.util.SmsSender;
	import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if JAR != "min"
import core.ScrollBar;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.online.ad.Resource;
import br.com.nanogames.components.online.ad.ResourceManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import core.MenuLabel;
import java.util.Hashtable;
//#endif

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener ,Serializable{

	private static final short GAME_MAX_FRAME_TIME = 145;

	private static final byte BACKGROUND_TYPE_SPECIFIC = 0;

	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;
	private static final byte BACKGROUND_TYPE_WHITE = 2;

	private static final byte BACKGROUND_TYPE_NONE = 3;
	private static int softKeyRightText;

	public static final byte OPTION_ENGLISH = 1;

	public static final byte OPTION_PORTUGUESE = 2;

	private byte indexLanguage;
	//#if JAR != "min"
		/** gerenciador da animação da soft key esquerda */
		private static BasicAnimatedSoftkey softkeyLeft;

		/** gerenciador da animação da soft key esquerda */
		private static BasicAnimatedSoftkey softkeyMid;

		/** gerenciador da animação da soft key direita */
		private static BasicAnimatedSoftkey softkeyRight;

	//#endif

	//#if JAR == "full"
		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
		private static final int LOW_MEMORY_LIMIT = 1050000;
		private static final int LOW_MEMORY_LIMIT_2 = 1350000;
	//#endif

	private static boolean lowMemory;

	private static LoadListener loader;

	/** Indica se o menu já foi exibido alguma vez (caso contrário, não mostra transição ao trocar para ele). */
	private boolean menuWasShown;

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif

	//#ifndef NO_RECOMMEND
		private Drawable recommendScreen;
	//#endif

	private Form form;

	private MarqueeLabel title;

	public static final byte ENTRY_MAIN_MENU_NEWS			= 0;
	public static final byte ENTRY_MAIN_MENU_CALENDAR		= 1;
	public static final byte ENTRY_MAIN_MENU_HELP			= 2;
	public static final byte ENTRY_MAIN_MENU_RECOMMEND		= 3;
	public static final byte ENTRY_MAIN_MENU_CREDITS		= 4;
	public static final byte ENTRY_MAIN_MENU_EXIT			= 5;

	private byte currentAdIndex;

	private static final byte ADS_TOTAL = 3;


	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#elif VENDOR == "MOTOROLA"
//# 			super( VENDOR_MOTOROLA, GAME_MAX_FRAME_TIME );
		//#else
			super( VENDOR_GENERIC, GAME_MAX_FRAME_TIME );
		//#endif

		FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
			
//		// cria a base de dados do jogo
//		try {
//			createDatabase( "BLABLA", 1 );
//		} catch ( Exception e ) {
//		}
//
//		Logger.setRMS( "BLABLA", 1 );
//		log( 0 );
		// TODO corrigir ©
		// TODO remover a tecla zero
		// TODO testar V3
	}


	public static final void log( int i ) {
//		AppMIDlet.gc();
//		Logger.log( i + ": " + Runtime.getRuntime().freeMemory() );
	}


	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.FIRE },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.FIRE },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.FIRE },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}

		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i ) {
				table.put( new Integer( keys[i][ 0] ), new Integer( keys[i][ 1] ) );
			}
		}
	}


	protected final void loadResources() throws Exception {
		log( 1 );
		//#if JAR == "full"
			switch ( getVendor() ) {
				//#if SCREEN_SIZE == "BIG"
					case VENDOR_SAMSUNG:
						lowMemory = Runtime.getRuntime().totalMemory() < LOAD_ALL_MEMORY_SAMSUNG;
					break;

					case VENDOR_NOKIA:
					case VENDOR_MOTOROLA:
				//#endif
				case VENDOR_SONYERICSSON:
				case VENDOR_SIEMENS:
				case VENDOR_BLACKBERRY:
				case VENDOR_HTC:
				break;

				default:
					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		}
		//#else
//# 			lowMemory = true;
		//#endif
		lowMemory = false; // teste

		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
			if ( ( i != FONT_MENU && i != FONT_BLACK ) || !isLowMemory() ) {
				FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i + ".png", PATH_IMAGES + "font_" + i + ".dat" );
				if ( i != FONT_BIG )
					FONTS[ i ].setCharExtraOffset( 1 );
				log( 6 );
			}
		}

//		if ( isLowMemory() ) {
//			FONTS[ FONT_MENU ] = FONTS[ FONT_WHITE ];
//			FONTS[ FONT_BLACK ] = FONTS[ FONT_MENU ];
//		}

		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}

		log( 7 );
        loadTexts(TEXT_TOTAL, PATH_IMAGES + "pt.dat");
		log( 8 );
		language = NanoOnline.LANGUAGE_pt_BR;

		form = new Form();

		title = new MarqueeLabel( FONT_MENU, null );
		title.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_ALWAYS );
		title.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );

		final Pattern p2 = new Pattern( 0x1d4c78 );
		p2.setSize( Short.MAX_VALUE, title.getFont().getHeight() + 2 );
		final DrawableComponent d2 = new DrawableComponent( p2 );
		d2.setFocusable( false );
		form.setStatusBar( d2, false );

		//#if SCREEN_SIZE == "BIG"
			form.setTouchKeyPad( new TouchKeyPad( getFont( FONT_WHITE ), new DrawableImage( PATH_IMAGES + "clear.png" ), new DrawableImage( "/online/shift.png" ), getBorder(), 0xecddab, 0xccbd8b, 0xfcedbb ) );
		//#endif

        setSpecialKeyMapping(true);
		setScreen( SCREEN_LOADING_1 );
	} // fim do método loadResources()


	protected final int changeScreen( int screen ) throws Exception {
		log( 10 );
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_SPECIFIC;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;
		byte indexSoftMid = SOFT_KEY_REMOVE;

		String titleText = "CBV NEWS";

		short visibleTime = -1;

		setSpecialKeyMapping(false);

		switch ( screen ) {
			case SCREEN_SPLASH_NANO:
				bkgType = BACKGROUND_TYPE_NONE;
				nextScreen = new BasicSplashNano( SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
			break;

			case SCREEN_SPLASH_BRAND:
				bkgType = BACKGROUND_TYPE_NONE;
				nextScreen = new BasicSplashBrand( SCREEN_MAIN_MENU, 0xffffff, PATH_SPLASH, PATH_SPLASH + "cbv.png", TEXT_SPLASH_BRAND );
			break;

			case SCREEN_MAIN_MENU:
				ResourceManager.load();
				
				//#ifndef NO_RECOMMEND
				recommendScreen = null;
				//#endif
					indexSoftRight = TEXT_EXIT;
						//#if BLACKBERRY_API == "true"
//# 							nextScreen = createBasicMenu( screen, ( showRecommendScreen && SmsSender.isSupported() ) ? new int[] {
				//#else
					nextScreen = createBasicMenu(screen, SmsSender.isSupported() ? new int[]{
				//#endif

						TEXT_NEWS,
						TEXT_CALENDAR,
						TEXT_HELP,
						TEXT_RECOMMEND_TITLE,
						TEXT_CREDITS,
						TEXT_EXIT,}
					: new int[]{
						TEXT_NEWS,
						TEXT_CALENDAR,
						TEXT_HELP,
						TEXT_CREDITS,
						TEXT_EXIT,}
					);

				if ( !menuWasShown ) {
					menuWasShown = true;
				}

				indexSoftLeft = TEXT_OK;
			break;

			case SCREEN_HELP:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_BLACK ), TEXT_HELP_CONTROLS, false );
				indexSoftRight = TEXT_BACK;
				titleText = getText( TEXT_HELP );
			break;

			case SCREEN_NEWS_SELECT:
				nextScreen = new NewsSelect( midlet, screen, SCREEN_MAIN_MENU );
				bkgType = BACKGROUND_TYPE_WHITE;
				indexSoftRight = TEXT_BACK;
				titleText = getText( TEXT_NEWS );
			break;

			case SCREEN_CALENDAR:
				nextScreen = new CalendarPicker();
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_NEWS_VIEW:
				nextScreen = new BasicTextScreen( SCREEN_NEWS_SELECT, getFont( FONT_BLACK ), NewsSelect.getSelectedEntryText(), false ) {
					public final void keyPressed( int key ) {
						switch ( key ) {
							case ScreenManager.KEY_SOFT_LEFT:
								AppMIDlet.browseURL( NewsSelect.getSelectedEntry().getEntry().getURL(), true );
							break;

							default:
								super.keyPressed( key );
						}
					}
				};
				bkgType = BACKGROUND_TYPE_WHITE;
				indexSoftLeft = TEXT_SEE_MORE;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_REFRESH_NEWS:
				nextScreen = new CBVLoadScreen();
				indexSoftRight = TEXT_CANCEL;
			break;

			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_BLACK ), TEXT_CREDITS_TEXT, true );
				indexSoftRight = TEXT_BACK;
				titleText = getText( TEXT_CREDITS );
			break;

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								// aloca os sons
								final String[] soundList = new String[ SOUND_TOTAL ];
								for ( byte i = 0; i < SOUND_TOTAL; ++i ) {
									soundList[i] = PATH_SOUNDS + i + ".mid";
									log( 11 );
								}

								MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								ScrollBar.loadImages();

								//#if BLACKBERRY_API == "true"
//# 									MediaPlayer.setVolume( 60 );
								//#endif

								log( 13 );
								
								MenuLabel.load();
								log( 19 );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
								log( 20 );
								Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

								softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
								softkeyMid = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_MID );
								softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );

								log( 21 );

								final ScreenManager manager = ScreenManager.getInstance();

								log( 22 );
								manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
								manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );

								loadScreen.setActive( false );

								setScreen( SCREEN_SPLASH_NANO );
							}
						}, getFont( FONT_BLACK ), TEXT_LOADING_GENERIC );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen( new LoadListener() {

					public final void load( final LoadScreen loadScreen ) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

						log( 23 );

						//#if JAR != "min"
							log( 24 );
							ScrollBar.loadImages();
							log( 25 );
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						//#endif
						
						log( 26 );

						loadScreen.setActive( false );

						setScreen( SCREEN_MAIN_MENU );
					}

				}, getFont( FONT_BLACK ), TEXT_LOADING );

				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			//#ifndef NO_RECOMMEND

			case SCREEN_RECOMMEND_SENT:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_BLACK ), getText( TEXT_RECOMMEND_SENT ) + getRecommendURL() + "\n\n", false );
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_RECOMMEND:
				setSpecialKeyMapping( false );
				nextScreen = new ScreenRecommend( SCREEN_MAIN_MENU );
//				//#if SCREEN_SIZE == "BIG"
//					( ( Form ) nextScreen ).setTouchKeyPad( new TouchKeyPad( getFont( FONT_BLACK ), new DrawableImage( PATH_IMAGES + "clear.png" ), new DrawableImage( "/online/shift.png" ), getBorder(), 0xecddab, 0xccbd8b, 0xfcedbb ) );
//				//#endif
				indexSoftRight = TEXT_BACK;
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				titleText = getText( TEXT_RECOMMEND_TITLE );
			break;

			//#endif
		} // fim switch ( screen )


		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft, visibleTime );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, visibleTime );

		if ( indexSoftMid != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_MID, indexSoftMid, visibleTime );

//			ScreenManager.getInstance().setCurrentScreen( nextScreen );
		ScreenManager.getInstance().setCurrentScreen( form );
		changeAd();
		if ( nextScreen instanceof Form ) {
			ScreenManager.getInstance().setCurrentScreen( nextScreen );
		} else if ( nextScreen instanceof Container ) {
			midlet.form.setContentPane( ( Container ) nextScreen );
		} else {
			final Container c = new Container( 1 );
			c.setPreferredSize( new Point( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) );
			final Component d = new DrawableComponent( nextScreen );
			c.insertDrawable( d );
			midlet.form.setContentPane( c );
			d.setSize( c.getLayoutSize() );
	//		nextScreen.setSize( c.getLayoutSize() );
		}
		title.setSize( ScreenManager.SCREEN_WIDTH, title.getHeight() );
		title.setText( titleText );
		setBackground( bkgType );

		return screen;
	} // fim do método changeScreen( int )


	private final void changeAd() throws Exception {
		final Pattern p = new Pattern( 0x1d4c78 );
		p.setSize( Short.MAX_VALUE, 47 );

		final DrawableImage bb = getCurrentAdImage( false );
		bb.setPosition( 5, ( p.getHeight() - bb.getHeight() ) >> 1 );
		title.setPosition( ( bb.getPosX() << 1 ) + bb.getWidth(), ( p.getHeight() - title.getHeight() ) >> 1 );

		final UpdatableGroup group = new UpdatableGroup( 4 );
		group.insertDrawable( p );
		group.insertDrawable( bb );
		group.insertDrawable( title );

		group.setSize( p.getSize() );

		form.setTitleBar( new DrawableComponent( group ) );

		// troca a cada 2 telas
		currentAdIndex = (byte) ((currentAdIndex + 1) % ( ADS_TOTAL << 1 ) );
	}


	public static final DrawableImage getCurrentAdImage( boolean big ) throws Exception {
		// troca a cada 2 telas
		if ( big )
			return new DrawableImage( PATH_IMAGES + "ad_" + ( ( ( GameMIDlet ) instance ).currentAdIndex >> 1 ) + "b.png" );

		return new DrawableImage( PATH_IMAGES + "ad_" + ( ( ( GameMIDlet ) instance ).currentAdIndex >> 1 ) + ".png" );
	}


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_WHITE:
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( 0xffffff );
			break;
		}
	} // fim do método setBackground( byte )


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final br.com.nanogames.components.userInterface.form.borders.Border getBorder() throws Exception {
		final LineBorder lineBorder = new LineBorder( 0x0059a7, LineBorder.TYPE_ROUND_RAISED ) {
			public final void setState( int state ) {
				super.setState( state );

				switch ( state ) {
					case STATE_PRESSED:
						setColor( 0x001967 );
						setFillColor( 0x003987 );
					break;

					case STATE_FOCUSED:
						setColor( 0x0089d7 );
						setFillColor( 0x0089d7 );
					break;

					case STATE_UNFOCUSED:
					default:
						setColor( 0x0069b7 );
						setFillColor( 0x0069b7 );
					break;
				}
			}
		};

		return lineBorder;
	}


	public static final NanoOnlineScrollBar getScrollBar() throws Exception {
		return new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT,
										COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	public static final Drawable getCursor() throws Exception {
		final Pattern p = new Pattern( 0xffff00 );
		p.setSize( MenuLabel.WIDTH + 10, MenuLabel.HEIGHT + 10 );
		p.defineReferencePixel( Pattern.ANCHOR_HCENTER | Pattern.ANCHOR_VCENTER );

		return p;
	}


	public static final Drawable getScrollFull() throws Exception {
		return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
	}


	public static final Drawable getScrollPage() throws Exception {
		return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
	}


	private static final Drawable createBasicMenu( int index, int[] entries  ) throws Exception {
		final CBVContainer cbv = new CBVContainer( entries );
		cbv.setId( index );
		return cbv;
	}


	public static final Button getButton( String text, int id, EventListener listener ) throws Exception {
		final Button button = new Button( getFont( FONT_MENU ), text ) {
			public final void setSize( int width, int height ) {
				super.setSize( 180, 40 );

				if ( label != null ) {
//					label.setSize( label.getFont().getTextWidth( label.getText() ), label.getFont().getHeight() );
					label.setPosition( ( getWidth() - label.getFont().getTextWidth( label.getText() ) ) >> 1, ( getHeight() - label.getFont().getHeight() ) >> 1 );
//					super.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ),
//								   label.getHeight() + ( getBorder() == null ? 0 : getBorder().getBorderHeight() ) );
				}
			}

		};
		button.setId( id );

		if ( listener != null )
			button.addEventListener( listener );
		button.setBorder( getBorder() );
//		button.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ), button.getHeight() + button.getBorder().getBorderHeight() );

		return button;
	}
	

	public static final boolean isLowMemory() {
		return lowMemory;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif
					
				//#ifdef NO_RECOMMEND
//# 					if ( index >= ENTRY_MAIN_MENU_RECOMMEND ) {
//# 						++index;
//# 					}
				//#endif

				//#if JAR != "min"
					//#ifndef WEB_EMULATOR
						if ( !SmsSender.isSupported() && index >= ENTRY_MAIN_MENU_RECOMMEND ) {
							++index;
						}
					//#endif
				//#endif

				switch ( index ) {
					case ENTRY_MAIN_MENU_NEWS:
						//#if NANO_RANKING == "true"
							setScreen(SCREEN_NEWS_SELECT);
						//#else
//# 							setScreen(SCREEN_LOADING_GAME);
						//#endif

						break;

					case ENTRY_MAIN_MENU_CALENDAR:
						setScreen( SCREEN_CALENDAR );
					break;
					
					case ENTRY_MAIN_MENU_RECOMMEND:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_RECOMMEND);
						}
						break;

					case ENTRY_MAIN_MENU_HELP:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_HELP);
						}
						break;

					case ENTRY_MAIN_MENU_CREDITS:
						if (language == NanoOnline.LANGUAGE_pt_BR) {
							setScreen(SCREEN_CREDITS);
						}
						break;

					case ENTRY_MAIN_MENU_EXIT :
						MediaPlayer.saveOptions();
						exit();
						break;
				} // fim switch ( index )
				break; // fim case SCREEN_MAIN_MENU
		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		//#if JAR != "min"
			if ( !isLowMemory() ) {
				for ( byte i = 0; i < menu.getTotalSlots(); ++i ) {
					if ( menu.getDrawable( i ) instanceof MenuLabel ) {
						( ( MenuLabel ) menu.getDrawable( i ) ).setActive( i == index );
					}
				}
			}
		//#endif
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		//#if JAR != "min"
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
			softKeyRightText = textIndex;
		//#endif
		
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( FONT_MENU, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do método setSoftKeyLabel( byte, int )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		//#if JAR == "min"
//# 		ScreenManager.getInstance().setSoftKey( softKey, d );
		//#else
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_MID:
				if ( softkeyMid != null ) {
					softkeyMid.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	//#endif
	} // fim do método setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}

	
	public static final ImageFont getFont( int index ) {
		//#if JAR == "min"
//# 			switch ( index ) {
//# 				case FONT_POINTS:
//# 				case FONT_BIG:
//# 					return FONTS[ index ];
//#
//# 				default:
//# 					return FONTS[ 0 ];
//# 			}
		//#else
			return FONTS[ index ];
		//#endif
	}


	private static final RichLabel getConfirmTitle( int textIndex ) throws Exception {
		return new RichLabel( FONT_BLACK, textIndex, ScreenManager.SCREEN_WIDTH * 9 / 10 );
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	protected void changeLanguage( byte language ) {
	}

	public final void write(DataOutputStream output) throws Exception {

		output.writeByte(language);
		output.writeBoolean(MediaPlayer.isMuted());
	}

	public final void read(DataInputStream input) throws Exception {
		language = input.readByte();
		input.readBoolean();
	}


	private static final class BorderedTextScreen extends UpdatableGroup implements KeyListener, PointerListener {

		private final ScrollRichLabel textScreen;

		private final int nextScreen;

		public BorderedTextScreen( String text, int nextScreen ) throws Exception {
			super( 2 );

			this.nextScreen = nextScreen;

			//#if SCREEN_SIZE == "SMALL"
//# 				if ( ScreenManager.SCREEN_HEIGHT < HEIGHT_MIN )
//# 					setSize( ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
//# 				else
//# 					setSize( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#else
				setSize( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
			//#endif
			defineReferencePixel( Drawable.ANCHOR_HCENTER | Drawable.ANCHOR_VCENTER );
			setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

			textScreen = new ScrollRichLabel( new RichLabel( getFont( FONT_BLACK ), text, getWidth() ) );
			textScreen.setSize( getWidth() - ( BORDER_THICKNESS * 3 ), getHeight() - ( BORDER_THICKNESS << 1 ) );
			textScreen.setPosition( BORDER_THICKNESS << 1, BORDER_THICKNESS );
			textScreen.setScrollFull( getScrollFull() );
			textScreen.setScrollPage( getScrollPage() );

			final Border border = new Border();
			border.setSize( getSize() );
			insertDrawable( border );
			insertDrawable( textScreen );
		}


		public final void keyPressed( int key ) {
			switch ( key ) {
				case ScreenManager.FIRE:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					GameMIDlet.setScreen( nextScreen );
				break;

				default:
					textScreen.keyPressed( key );
			}
		}


		public final void keyReleased( int key ) {
			textScreen.keyReleased( key );
		}


		public final void onPointerDragged( int x, int y ) {
			textScreen.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			textScreen.onPointerPressed( x, y );
		}


		public final void onPointerReleased( int x, int y ) {
			textScreen.onPointerReleased( x, y );
		}

	}


	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;


	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualização do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private final byte previousScreen;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.prof.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );

			previousScreen = currentScreen;

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );

			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}

		private LoadScreen( LoadListener listener ) throws Exception {

			super( AppMIDlet.getFont( FONT_BLACK ), AppMIDlet.getText( TEXT_LOADING ) );
            previousScreen = currentScreen;
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo são carregados aqui para evitar sobrecarga do método loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// só inicia a thread quando a tela atual for a ativa, para evitar possíveis atrasos em transições e
					// garantir que novos recursos só serão carregados quando a tela anterior puder ser desalocada por completo
					if ( painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
										e.printStackTrace();
									//#else
//# 									// volta à tela anterior
//# 									setScreen( previousScreen );
								//#endif
								}
							} // fim do método run()


						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execução
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do método update( int )


		public final void draw( Graphics g ) {
			super.draw( g );

			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen


	// </editor-fold>
}
