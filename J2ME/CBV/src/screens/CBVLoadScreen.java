/**
 * CBVLoadScreen.java
 *
 * Created on Apr 19, 2010 6:54:05 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author peter
 */
public final class CBVLoadScreen extends UpdatableGroup implements Constants, KeyListener, ConnectionListener, ScreenListener {

	private final Label label;

	private final Pattern gauge;

	private final DrawableImage bb;

	private int lastConnection = -1;


	public CBVLoadScreen() throws Exception {
		super( 10 );

		final Pattern bkg = new Pattern( 0xffff00 );
		bkg.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		//insertDrawable( bkg );

		bb = GameMIDlet.getCurrentAdImage( true );
		insertDrawable( bb );

		label = new Label( FONT_BLACK, null );
		insertDrawable( label );

		gauge = new Pattern( 0x0000ff );
		insertDrawable( gauge );

		autoSize();

		final TimerTask task = new TimerTask() {
			public final void run() {
				seila();
			}
		};
		new Timer().schedule( task, 1000 );
	}


	private final void autoSize() {
		setSize( ScreenManager.SCREEN_WIDTH, 100 );
	}


	public void setSize( int width, int height ) {
		super.setSize( Math.min( width, ScreenManager.SCREEN_WIDTH * 7 / 10 ), bb.getHeight() + 30 );
		setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - 66 - getHeight() ) >> 1 );

		bb.setPosition( ( getWidth() - bb.getWidth() ) >> 1, 10 );
		label.setPosition( ( getWidth() - label.getWidth() ) >> 1, bb.getPosY() + bb.getHeight() );

		gauge.setPosition( ( getWidth() - gauge.getWidth() ) >> 1, label.getPosY() + label.getHeight() );
	}


	public void seila() {
		try {
			final ByteArrayOutputStream b = new ByteArrayOutputStream();
			final DataOutputStream out = new DataOutputStream( b );

			// escreve os dados globais - inclusive as informações do news feeder
			NanoOnline.writeGlobalData( out );
			out.flush();

			lastConnection = NanoConnection.post( NANO_ONLINE_URL + "news_feeds/refresh", b.toByteArray(), this, false );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}


	private final void setLabelText( String text ) {
		label.setText( text );
		label.setPosition( ( getWidth() - label.getWidth() ) >> 1, label.getPosY() );
	}


	public final void processData( int id, byte[] data ) {
		try {
			NanoOnline.readGlobalData( data );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
		}
		GameMIDlet.setScreen( SCREEN_NEWS_SELECT );
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		final StringBuffer buffer = new StringBuffer();
		switch ( infoIndex ) {
			case INFO_CONNECTION_OPENED:
				buffer.append( "conexão aberta" );
			break;

			case INFO_OUTPUT_STREAM_OPENED:
			case INFO_DATA_WRITTEN:
				buffer.append( "enviando dados" );
			break;

			case INFO_RESPONSE_CODE:
				buffer.append( "código de resposta: " );
				buffer.append( ( ( Integer ) extraData ).intValue() );
			break;

			case INFO_INPUT_STREAM_OPENED:
				buffer.append( "recebendo dados" );
			break;

			case INFO_CONTENT_LENGTH_TOTAL:
//									contentLengthTotal = ( ( Integer ) extraData ).intValue();
//									refreshProgress();
//									refreshLabel( buffer );
			break;

			case INFO_CONTENT_LENGTH_READ:
//									contentLengthRead = ( ( Integer ) extraData ).intValue();
//									refreshProgress();
//
//									buffer.append( NanoOnline.getText( TEXT_RECEIVING_DATA ) );
//									refreshLabel( buffer );
			break;

			case INFO_CONNECTION_ENDED:
			return;
		}
		setLabelText( buffer.toString() );
		//#if DEBUG == "true"
			System.out.println( "INFO -> " + buffer.toString() );
		//#endif
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		switch ( errorIndex ) {
			case ERROR_HTTP_CONNECTION_NOT_SUPPORTED:
			case ERROR_CANT_GET_RESPONSE_CODE:
			case ERROR_URL_BAD_FORMAT:
			case ERROR_CONNECTION_EXCEPTION:
			case ERROR_CANT_CLOSE_INPUT_STREAM:
			case ERROR_CANT_CLOSE_CONNECTION:
			case ERROR_CANT_CLOSE_OUTPUT_STREAM:
				final Exception e = ( Exception ) extraData;
				setLabelText( GameMIDlet.getText( TEXT_ERROR ) + errorIndex + ": " + e.getMessage() );
			break;
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		autoSize();
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if ( lastConnection != -1 ) {
					NanoConnection.cancel( lastConnection );
					lastConnection = -1;
					GameMIDlet.setScreen( SCREEN_NEWS_SELECT );
				}
			break;
		}
	}


	public final void keyReleased( int key ) {
	}

}
