/**
 * NewsSelect.java
 *
 * Created on Apr 13, 2010 6:47:55 PM
 * 
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.newsfeeder.NewsFeederEntry;
import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.Point;
import core.CBVNewsEntry;
import core.Constants;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;

/**
 *
 * @author peter
 */
public final class NewsSelect extends CBVContainer implements Constants, ScreenListener {

	private static final byte TOTAL_ENTRIES = NewsFeederManager.MAX_NEWS_ENTRIES + 10;

	protected final int backIndex;

	protected final byte firstEntry;

	/** Estrutura que armazena a última entrada selecionada de cada menu. A chave de cada entrada é o id do menu,
	 * e o valor da chave é o índice da última entrada utilizada.
	 */
	private static int lastIndex;

	private final CBVNewsEntry[] entries;

	private static CBVNewsEntry entrySelected;

	private static final byte BUTTON_UPDATE_ID = 0;
	private static final byte BUTTON_CANCEL_ID = 1;

	
	public NewsSelect( MenuListener listener, int id, int backIndex ) throws Exception {
		super( TOTAL_ENTRIES );

		this.backIndex = backIndex;
		firstEntry = 0;

		NanoOnline.load( NanoOnline.LANGUAGE_pt_BR, APP_SHORT_NAME, SCREEN_MAIN_MENU );
		final NewsFeederManager newsManager = new NewsFeederManager();
		newsManager.load();
		final NewsFeederEntry[] newsEntries = newsManager.getEntries();

		entries = new CBVNewsEntry[ newsEntries.length ];
		for ( byte i = 0; i < entries.length; ++i ) {
			final CBVNewsEntry e = new CBVNewsEntry( newsEntries[ i ] );
			entries[ i ] = e;

			final DrawableComponent d = new DrawableComponent( e );
			d.setPreferredSize( new Point( Short.MAX_VALUE, CBVNewsEntry.BKG_HEIGHT ) );
			d.setId( i );
			insertDrawable( d );

			d.addEventListener( new EventListener() {
				public final void eventPerformed( Event evt ) {
					switch ( evt.eventType ) {
						case Event.EVT_POINTER_PRESSED:
//						case Event.EVT_KEY_PRESSED:
							if ( e.isFocused() ) {
								entrySelected = e;
								GameMIDlet.setScreen( SCREEN_NEWS_VIEW );
							}
						break;

						case Event.EVT_FOCUS_GAINED:
							e.setFocused( true );
						break;

						case Event.EVT_FOCUS_LOST:
							e.setFocused( false );
						break;

						case Event.EVT_KEY_PRESSED:
							final Integer key = ( Integer ) evt.data;
							
							switch ( ScreenManager.getSpecialKey( key.intValue() ) ) {
								case ScreenManager.KEY_SOFT_LEFT:
									GameMIDlet.setScreen( SCREEN_REFRESH_NEWS );
								break;

								case ScreenManager.KEY_SOFT_RIGHT:
									keyPressed( key.intValue() );
								break;
							}
						break;
					}
				}
			});
		}

		if ( entries.length <= 0 ) {
			final FormText text = new FormText( GameMIDlet.getFont( FONT_BLACK ), ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//			text.setText( GameMIDlet.getText( TEXT_RECOMMEND_TEXT ) );
			text.setText( "Não há notícias salvas. Clique em \"atualizar\" para receber as notícias!" );
			text.setScrollableY( true );
//			text.setScrollBarV( GameMIDlet.get );
//			text.getScrollBarV().setSize( 0, 1 );
			text.setFocusable( true );
			text.addEventListener( this );
			insertDrawable( text );
		}
		
		final Button buttonUpdate = GameMIDlet.getButton( GameMIDlet.getText( TEXT_UPDATE ), BUTTON_UPDATE_ID, this );
		insertDrawable( buttonUpdate );

		final Button buttonCancel = GameMIDlet.getButton( GameMIDlet.getText( TEXT_BACK ), BUTTON_CANCEL_ID, this );
		insertDrawable( buttonCancel );
	}


	public final void eventPerformed( Event evt ) {
		switch ( evt.eventType ) {
			case Event.EVT_KEY_PRESSED:
				final Integer key = ( Integer ) evt.data;
				switch ( ScreenManager.getSpecialKey( key.intValue() ) ) {
					case ScreenManager.KEY_SOFT_LEFT:
						GameMIDlet.setScreen( SCREEN_REFRESH_NEWS );
					break;

					default:
						keyPressed( ScreenManager.getSpecialKey( key.intValue() ) );
				}
			break;

			case Event.EVT_BUTTON_CONFIRMED:
				switch ( evt.source.getId() ) {
					case BUTTON_CANCEL_ID:
						GameMIDlet.setScreen( SCREEN_MAIN_MENU );
					break;

					case BUTTON_UPDATE_ID:
						GameMIDlet.setScreen( SCREEN_REFRESH_NEWS );
					break;
				}
			break;
		}
	}


	public void setSize( int width, int height ) {
		for ( int i = 0; i < entries.length; ++i ) {
			entries[ i ].setSize( width, entries[ i ].getHeight() );
		}

		super.setSize( width, height );
	}


	public static final String getSelectedEntryText() {
		return entrySelected == null ? null : "<ALN_H>" + entrySelected.getEntry().getTitle() + "\n\n<ALN_L>" + entrySelected.getEntry().getContent();
	}


	public static final CBVNewsEntry getSelectedEntry() {
		return entrySelected;
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
				GameMIDlet.setScreen( SCREEN_REFRESH_NEWS );
			break;

			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

}
