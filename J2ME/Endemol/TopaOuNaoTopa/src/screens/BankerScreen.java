/**
 * BankerScreen.java
 *
 * Created on Sep 16, 2010 8:13:21 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.BankerBackground;
import core.Constants;
import core.Highlight;
import core.TitleLabel;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public final class BankerScreen extends UpdatableGroup implements Constants, SpriteListener {

	private final TitleLabel label;

	private boolean deal;

	private final Sprite arrowLeft;

	private final Sprite arrowRight;

	private final DrawableImage imageDeal;

	private final DrawableImage imageNoDeal;

	private final Sprite button;

	private final Pattern table;

	private Sprite[] glasses;

	private final Highlight highlight;

	private static final byte BUTTON_NONE	= 0;
	private static final byte BUTTON_LEFT	= 1;
	private static final byte BUTTON_RIGHT	= 2;

	private byte buttonPressed;

	private final SpriteListener listener;

	public static final byte BANKER_SEQUENCE_DEAL		= 0;
	public static final byte BANKER_SEQUENCE_NO_DEAL	= 1;

	private static final byte BUTTON_SEQUENCE_STOPPED	= 0;
	private static final byte BUTTON_SEQUENCE_PRESSING	= 1;
	private static final byte BUTTON_SEQUENCE_PRESSED	= 2;

	private static final byte GLASS_SEQUENCE_OPEN		= 0;
	private static final byte GLASS_SEQUENCE_CLOSING	= 1;
	private static final byte GLASS_SEQUENCE_CLOSED		= 2;

	private static final byte SPRITE_ID_GLASS	= 0;
	private static final byte SPRITE_ID_BUTTON	= 1;

	private boolean dealMode;

	private final UpdatableGroup group;

	private BankerBackground bkg;

	private static final byte BKG_DRAWABLE_INDEX = 0;
	private static final byte GLASSES_DRAWABLE_INDEX = 3;

	private static final short TIME_WAIT = 2200;

	private short accTime;

	private boolean accepted;

	private final Mutex mutex = new Mutex();
	

	public BankerScreen( SpriteListener listener ) throws Exception {
		super( 7 );

		this.listener = listener;

		group = new UpdatableGroup( 5 );
		insertDrawable( group );

		group.setViewport( new Rectangle() );
		highlight = new Highlight( group.getPosition(), group.getViewport() );
		insertDrawable( highlight );

		label = new TitleLabel();
		label.setFont( GameMIDlet.GetFont( FONT_BANKER_VALUE ) );
		label.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( label );

		table = new Pattern( new DrawableImage( PATH_BANKER + "table.png" ) );
		table.setSize( Short.MAX_VALUE, table.getHeight() );
		group.insertDrawable( table );

		button = new Sprite( PATH_BANKER + "button" );
		button.setListener( this, SPRITE_ID_BUTTON );
		button.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		group.insertDrawable( button );

		arrowLeft = new Sprite( PATH_BANKER + "arrow" );
		arrowLeft.mirror( TRANS_MIRROR_H );
		arrowLeft.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( arrowLeft );

		arrowRight = new Sprite( arrowLeft );
		arrowRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
		insertDrawable( arrowRight );

		imageDeal = new DrawableImage( PATH_BANKER + "deal.png" );
		imageDeal.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		insertDrawable( imageDeal );

		imageNoDeal = new DrawableImage( PATH_BANKER + "no_deal.png" );
		imageNoDeal.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		insertDrawable( imageNoDeal );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		accepted = false;

		setDealMode( false );
		refreshButtons();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		group.setSize( size );

		if ( bkg != null )
			bkg.setSize(width, height);

		highlight.setSize( size );

		label.setRefPixelPosition( width >> 1, TITLE_Y_SPACING );

		final Point highlightSize = highlight.getInternalSize();
		final int HEIGHT = highlightSize.y + arrowLeft.getHeight();
		final int INITIAL_Y = ( height - HEIGHT ) >> 1;
		final int ARROWS_Y = INITIAL_Y + highlightSize.y + 7;
		final int TEXT_Y = ARROWS_Y;
		final int CENTER_Y = INITIAL_Y + ( highlightSize.y >> 1 ) - 14;

		highlight.setTarget( width >> 1, CENTER_Y, 0 );
		highlight.update( Short.MAX_VALUE );

		imageDeal.setRefPixelPosition( width >> 1, TEXT_Y );
		imageNoDeal.setRefPixelPosition( imageDeal.getRefPixelPosition() );

		table.setPosition( 0, INITIAL_Y + highlightSize.y - table.getHeight() );
		//#if SCREEN_SIZE == "SMALL"
//# 			button.setRefPixelPosition( width >> 1, table.getPosY() + ( ( table.getHeight() >> 1 ) ) );
		//#else
		button.setRefPixelPosition( width >> 1, table.getPosY() + ( table.getHeight() >> 1 ) );
		//#endif
		final int biggestWidth = Math.max( imageDeal.getWidth(), imageNoDeal.getWidth() );

		refreshGlassesPosition();

		arrowLeft.setRefPixelPosition( ( width - biggestWidth ) >> 1, ARROWS_Y );
		arrowRight.setRefPixelPosition( ( width + biggestWidth ) >> 1, ARROWS_Y );

		setDealMode( dealMode );
	}


	private final void refreshGlassesPosition() {
		if ( glasses != null ) {
			glasses[ 0 ].setRefPixelPosition( button.getRefPixelX(), button.getRefPixelY() + BANKER_CASE_OFFSET_Y );
			glasses[ 1 ].setRefPixelPosition( (button.getRefPixelX()-1), button.getRefPixelY() + BANKER_CASE_OFFSET_Y );
		}
	}


	public final void setVisible( boolean visible ) {
		mutex.acquire();

		super.setVisible( visible );

		if ( visible ) {
			// em aparelhos com pouca memória, o cenário e o vidro são desalocados para permitir uma melhor execução

			try {
				if ( bkg == null ) {
					GameMIDlet.log( "Banker load bkg início" );
					bkg = new BankerBackground();
					bkg.setSize( size );
					group.insertDrawable( bkg, BKG_DRAWABLE_INDEX );
					GameMIDlet.log( "Banker load bkg fim" );
				}

				if ( table.getFill() == null ) {
					table.setFill( new DrawableImage( PATH_BANKER + "table.png" ) );
				}

				if ( glasses == null ) {
					GameMIDlet.log( "Banker load glasses início" );
					glasses = new Sprite[ 2 ];

					glasses[ 0 ] = new Sprite( PATH_BANKER + "glass" );
					glasses[ 0 ].defineReferencePixel( ANCHOR_BOTTOM_RIGHT );
					glasses[ 0 ].setListener( this, SPRITE_ID_GLASS );
					group.insertDrawable( glasses[ 0 ], GLASSES_DRAWABLE_INDEX );

					glasses[ 1 ] = new Sprite( glasses[ 0 ] );
					glasses[ 1 ].mirror( TRANS_MIRROR_H );
					glasses[ 1 ].defineReferencePixel( ANCHOR_BOTTOM_LEFT );
					group.insertDrawable( glasses[ 1 ], GLASSES_DRAWABLE_INDEX );

					refreshGlassesPosition();
					GameMIDlet.log( "Banker load glasses fim" );
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					GameMIDlet.log( e.getClass() + " -> " + e.getMessage() );
					e.printStackTrace();
				//#endif
			}
		} else {
			if ( GameMIDlet.isLowMemory() ) {
				GameMIDlet.log( "Banker UNload início" );
				group.removeDrawable( bkg );
				bkg = null;

				table.setFill( null );

				if ( glasses != null ) {
					group.removeDrawable( glasses[ 0 ] );
					group.removeDrawable( glasses[ 1 ] );
					glasses = null;
				}
				GameMIDlet.log( "Banker UNload fim" );
			}
		}

		mutex.release();
	}


	public final void setOffer( String value ) {
		label.setText( value, TITLE_WAIT_TIME );
	}


	public final void keyPressed( int key ) {
		mutex.acquire();

		if ( dealMode && !accepted ) {
			switch ( key ) {
				case ScreenManager.LEFT:
				case ScreenManager.KEY_NUM4:
					buttonPressed = BUTTON_LEFT;
				break;

				case ScreenManager.RIGHT:
				case ScreenManager.KEY_NUM6:
					buttonPressed = BUTTON_RIGHT;
				break;

				case ScreenManager.FIRE:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_NUM5:
					if ( accTime < 0 ) {
						//handleEvents = false;
						accTime = 0;
						label.setState( TitleLabel.STATE_LOGO_APPEARING );
						accepted = true;

						if ( deal ) {
							label.setText( TEXT_DEAL, TitleLabel.TIME_TOP_ANIMATION + TITLE_WAIT_TIME );
							MediaPlayer.play( SOUND_DEAL, MediaPlayer.LOOP_INFINITE );
							button.setSequence( BUTTON_SEQUENCE_PRESSING );
						} else {
							label.setText( TEXT_NO_DEAL, TitleLabel.TIME_TOP_ANIMATION + TITLE_WAIT_TIME );
							MediaPlayer.play( SOUND_NO_DEAL );
							glasses[ 0 ].setSequence( GLASS_SEQUENCE_CLOSING );
							glasses[ 1 ].setSequence( GLASS_SEQUENCE_CLOSING );
						}
					}

				default:
					buttonPressed = BUTTON_NONE;
			}
			refreshButtons();
		}

		mutex.release();
	}


	public final void keyReleased( int key ) {
		if ( dealMode ) {
			switch ( buttonPressed ) {
				case BUTTON_LEFT:
				case BUTTON_RIGHT:
					setDeal( !deal );
				break;
			}
			buttonPressed = BUTTON_NONE;
			refreshButtons();
		}
	}


	private final void refreshButtons() {
		switch ( buttonPressed ) {
			case BUTTON_NONE:
				arrowLeft.setFrame( 0 );
				arrowRight.setFrame( 0 );
			break;

			case BUTTON_LEFT:
				arrowLeft.setFrame( 1 );
				arrowRight.setFrame( 0 );
			break;

			case BUTTON_RIGHT:
				arrowLeft.setFrame( 0 );
				arrowRight.setFrame( 1 );
			break;
		}
	}


	private final void setDeal( boolean d ) {
		deal = d;
		imageDeal.setVisible( deal );
		imageNoDeal.setVisible( !deal );
	}


	public final void update( int delta ) {
		mutex.acquire();

		super.update( delta );

		if ( accTime >= 0 && ( button.getSequenceIndex() == BUTTON_SEQUENCE_PRESSED || 
				( glasses != null && glasses[ 0 ].getSequenceIndex() == GLASS_SEQUENCE_CLOSED ) ) ) {
			accTime += delta;

			if ( accTime >= TIME_WAIT ) {
				accTime = -1;
				mutex.release();
				
				if ( button.getSequenceIndex() == BUTTON_SEQUENCE_PRESSED )
					listener.onSequenceEnded( LISTENER_ID_BANKER, BANKER_SEQUENCE_DEAL );
				else
					listener.onSequenceEnded( LISTENER_ID_BANKER, BANKER_SEQUENCE_NO_DEAL );
				
				return;
			}
		}

		mutex.release();
	}


	public final void setDealMode( boolean d ) {
		mutex.acquire();

		if(GameMIDlet.isLowMemory()) {
			if ( bkg != null ) {
				if ( d )
					bkg.hideBanker();
				else
					bkg.showBanker();
			}
		}

		dealMode = d;
		button.setVisible( d );
		imageDeal.setVisible( d );
		imageNoDeal.setVisible( d );
		table.setVisible( d );
		highlight.setVisible( d );
		accTime = -1;
		accepted = false;

		if ( d ) {
			setDeal( false );
		} else {
			label.setState( TitleLabel.STATE_SHOW_LOGO );
			group.getViewport().set( 0, 0, getWidth(), getHeight() );
		}
		
		button.setSequence( BUTTON_SEQUENCE_STOPPED );

		if ( glasses != null ) {
			glasses[ 0 ].setSequence( GLASS_SEQUENCE_OPEN );
			glasses[ 1 ].setSequence( GLASS_SEQUENCE_OPEN );

			for ( byte i = 0; i < 2; ++i ) {
				glasses[ i ].setVisible( d );
			}
		}
		arrowLeft.setVisible( d );
		arrowLeft.setFrame( d ? 1 : 0 );
		arrowRight.setVisible( arrowLeft.isVisible() );
		arrowRight.setFrame( arrowRight.getFrameSequenceIndex() );

		mutex.release();
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( id ) {
			case SPRITE_ID_BUTTON:
				switch ( sequence ) {
					case BUTTON_SEQUENCE_PRESSING:
						button.setSequence( BUTTON_SEQUENCE_PRESSED );
					break;
				}
			break;

			case SPRITE_ID_GLASS:
				switch ( sequence ) {
					case GLASS_SEQUENCE_CLOSING:
						if ( glasses != null ) {
							glasses[ 0 ].setSequence( GLASS_SEQUENCE_CLOSED );
							glasses[ 1 ].setSequence( GLASS_SEQUENCE_CLOSED );
						}
					break;
				}
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}

	
	//#if TOUCH == "true"
		public final void onPointerPressed( int x, int y ) {
			if ( arrowLeft.contains( x, y ) ) {
				keyPressed( ScreenManager.LEFT );
			} else if ( arrowRight.contains( x, y ) ) {
				keyPressed( ScreenManager.RIGHT );
			} else if ( imageDeal.contains( x, y ) || imageNoDeal.contains( x, y ) || glasses[ 0 ].contains( x, y ) || glasses[ 1 ].contains( x, y ) ) {
				keyPressed( ScreenManager.FIRE );
			} else {
				keyReleased( 0 );
			}
		}


		public final void onPointerReleased( int x, int y ) {
			keyReleased( 0 );
		}
	//#endif
	
}
