/**
 * TopaMenu.java
 *
 * Created on Sep 21, 2010 12:02:13 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.ConfettiEmitter;
import core.Constants;
import core.MenuLabel;
import core.ParticleEmitter;
import core.SparkEmitter;
import core.SubMenu;
import core.TitleLabel;
import java.util.Hashtable;
//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif
	
/**
 *
 * @author peter
 */
public class TopaMenu extends UpdatableGroup implements Constants, KeyListener, ScreenListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{
	private final SubMenu menu;
	private final TitleLabel title;
	private boolean playSound = true;

	
	public TopaMenu( int screen, int[] entries, int title ) throws Exception {
		this( screen, entries, title >= 0 ? GameMIDlet.getText( title ) : null );
	}
	

	public TopaMenu( int screen, int[] entries, String titleText ) throws Exception {
		this( screen, getEntries( entries ), titleText );
	}

	
	public TopaMenu( int screen, String[] entries, String titleText ) throws Exception {
		super( 10 );

		final Drawable[] menuEntries = new Drawable[ entries.length ];
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			menuEntries[i] = new MenuLabel( entries[ i ] );
		}

		title = new TitleLabel();
		if ( titleText != null ) {
			title.setText( titleText, TITLE_WAIT_TIME );
		}
		insertDrawable( title );

		menu = new SubMenu( ( GameMIDlet ) GameMIDlet.getInstance(), screen, menuEntries, MENU_SPACING, 0, entries.length - 1, null );
		for ( byte i = 0; i < menuEntries.length; ++i ) {
			( ( MenuLabel ) menuEntries[ i ] ).setParent( menu );
		}
		insertDrawable( menu );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	private static final String[] getEntries( int[] entriesIds ) {
		final String[] ret = new String[ entriesIds.length ];
		
		for ( short i = 0; i < ret.length; ++i ) {
			ret[ i ] = GameMIDlet.getText( entriesIds[ i ] );
		}

		return ret;
	}
	

    public final Drawable getMenuDrawable(int index) {
        return menu.getDrawable(index);
    }


	public final void setCurrentIndex( int index ) {
		menu.setCurrentIndex( index );
	}


	public final void setBackIndex( int index ) {
		menu.setBackIndex( index );
	}

	
	public final void keyPressed( int key ) {
		menu.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		menu.keyReleased( key );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
		if ( playSound && !MediaPlayer.isPlaying() )
			MediaPlayer.play( SOUND_SPLASH, MediaPlayer.LOOP_INFINITE );
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

        if((title != null)){
           title.setPosition((width/2)-(title.getWidth()/2), TITLE_Y_SPACING);
        }

		final int TITLE_HEIGHT = title.getHeight() + ( TITLE_Y_SPACING << 1 );

		menu.setSize( width, ((title == null) ? (height) : (height - TITLE_HEIGHT)) );
		if ( menu.getHeight() < getHeight() ) {
            if(title == null)
				menu.setPosition( ( getWidth() - menu.getWidth() ) >> 1, ((( getWidth() - menu.getHeight() ) >> 1) ) );
            else
				menu.setPosition( ( getWidth() - menu.getWidth() ) >> 1, TITLE_HEIGHT );
        }
	}


	public final void setPlaySound( boolean play ) {
		playSound = play;
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			menu.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			menu.onPointerPressed( x, y );
		}


		public final void onPointerReleased( int x, int y ) {
			menu.onPointerReleased( x, y );
		}
	//#endif

}
