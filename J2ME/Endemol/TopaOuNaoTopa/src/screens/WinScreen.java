
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.ConfettiEmitter;
import core.Constants;
import core.SparkEmitter;

/**
 *
 * @author caiolima
 */
public final class WinScreen extends UpdatableGroup implements Constants {
    private final Pattern pattern;
    private final SparkEmitter sparkEmitter;
    private final ConfettiEmitter confettiEmitter;
	private final DrawableImage textVoceGanhou;
	private final RichLabel labelPrize;
	private final int WIDTH_OF_JUSTUS;
	private final int HEIGHT_OF_JUSTUS_TEXT;
	
	private int confettiMultiplier;

	
	public WinScreen(int justusWidth, int boxHeight, int prize ) throws Exception{
		super(5);

		confettiMultiplier = 0;
		WIDTH_OF_JUSTUS = justusWidth>>1;
		HEIGHT_OF_JUSTUS_TEXT = boxHeight;

		if( prize >= MIN_SCORE_FOR_CONFETTI) {


			confettiEmitter = new ConfettiEmitter( ScreenManager.SCREEN_WIDTH/MIN_WIDTH_PER_PARTICLE, NanoMath.toFixed(PARTICLES_MIN_CONFETTI_SPEED), NanoMath.toFixed(PARTICLES_MAX_CONFETTI_SPEED), PARTICLES_MAX_CONFETTIS);
			insertDrawable(confettiEmitter);
		} else {
			confettiEmitter = null;
		}
		
		textVoceGanhou = new DrawableImage( PATH_IMAGES + "voceganhou.png" );
		textVoceGanhou.defineReferencePixel((textVoceGanhou.getSize().x>>1),(textVoceGanhou.getSize().y));
		labelPrize = new RichLabel( FONT_MILLION, TEXT_MILLION, ScreenManager.SCREEN_WIDTH );

		labelPrize.setText( "<ALN_H>" + GameMIDlet.getCurrencyString( prize, true, false ) );
		if( prize == GameScreen.VALUE_MILLION ) {
			sparkEmitter = new SparkEmitter(new Point(), PARTICLES_SPARKS_PER_SECOND, 80, 100, NanoMath.toFixed(PARTICLES_MIN_SPARK_SPEED),NanoMath.toFixed(PARTICLES_SPARK_SPEED_RANGE), 20, 1000, 500, PARTICLES_MAX_SPARKS);
			pattern = new Pattern(sparkEmitter);
			pattern.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
			insertDrawable(pattern);
			
			confettiMultiplier = NanoMath.ONE;
		} else {
			sparkEmitter = null;
			pattern = null;
			
			if( prize >= MIN_SCORE_FOR_CONFETTI) {
				confettiMultiplier = NanoMath.toFixed((prize - MIN_SCORE_FOR_CONFETTI)/SCORE_FOR_CONFETTI_PRECISION)/SCORE_FOR_CONFETTI_STEPS; // confettiMultiplier_max = 1
				confettiMultiplier = NanoMath.sqrtFixed(confettiMultiplier);
				confettiMultiplier = NanoMath.sqrtFixed(confettiMultiplier);
			}
		}
		
        insertDrawable(textVoceGanhou);
		insertDrawable(labelPrize);

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		show();
	}

	public final void setSize(int width, int height){
		super.setSize(width, height);

		if ( sparkEmitter != null ) {
			pattern.setSize((getWidth()-WIDTH_OF_JUSTUS), getHeight() - HEIGHT_OF_JUSTUS_TEXT);
			final int nSparkEmitters = NanoMath.max((size.x-WIDTH_OF_JUSTUS)/MAX_WIDTH_SPARKEMITTER, 1);
			sparkEmitter.setEmissivePosition((size.x-WIDTH_OF_JUSTUS)/(2*nSparkEmitters), size.y - HEIGHT_OF_JUSTUS_TEXT);
			sparkEmitter.setSize((size.x-WIDTH_OF_JUSTUS)/nSparkEmitters, size.y - HEIGHT_OF_JUSTUS_TEXT);
		}

		setConfettiEmitter();
		setLabelSize();
	}
	
	private final void setConfettiEmitter() {
		if ( confettiEmitter != null ) {
			confettiEmitter.setSize( getWidth(), getHeight() );
			final int tempWidth = NanoMath.clamp((MAX_WIDTH_PER_PARTICLE - (NanoMath.toInt(confettiMultiplier * WIDTH_PER_PARTICLE_RANGE))), MIN_WIDTH_PER_PARTICLE, MAX_WIDTH_PER_PARTICLE);
			confettiEmitter.setParticlesPerSecond(size.x/tempWidth);
		}
	}

	private final void setLabelSize() {
		labelPrize.setSize( size );
		labelPrize.setPosition( 0, ( size.y - labelPrize.getTextTotalHeight() ) >> 1 );
		textVoceGanhou.setRefPixelPosition(size.x>>1, (labelPrize.getHeight()>>1) - (((labelPrize.getTextTotalHeight())>>1) + WINSCREEN_TEXT_DISTANCE) );
	}


	public final void show() {
		hide();
		setVisible(true);
		if( sparkEmitter != null )
			sparkEmitter.turnOn();
		if( confettiEmitter != null )
			confettiEmitter.turnOn();
	}

	public final void hide() {
		setVisible(false);
		if ( confettiEmitter != null ) {
			confettiEmitter.reset();
			confettiEmitter.turnOff();
		}

		if ( sparkEmitter != null ) {
			sparkEmitter.reset();
			sparkEmitter.turnOff();
		}
	}

}
