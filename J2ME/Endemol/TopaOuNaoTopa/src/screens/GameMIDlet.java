/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Serializable;
import core.Case;
import core.ConfettiEmitter;
import core.Constants;
import core.LedPattern;
import core.MenuLabel;
import core.ParticleEmitter;
import core.ScrollBar;
import core.TitleLabel;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

//#if NANO_RANKING == "true"
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.online.newsfeeder.NewsFeederEntry;
import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener ,Serializable
//#if NANO_RANKING == "true"
		, ConnectionListener, RankingFormatter
//#endif
{
	
	private static final short GAME_MAX_FRAME_TIME = 180;
	
	private static final byte BACKGROUND_TYPE_LED_PATTERN	= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;

	public static final byte OPTION_ENGLISH = 0;
	public static final byte OPTION_PORTUGUESE = 1;

	public static final byte OPTION_PLAY_SOUD = 0;
	public static final byte OPTION_NO_SOUND = 1;

	public static final byte CONFIRM_YES = 0;
	public static final byte CONFIRM_NO = 1;
	public static final byte CONFIRM_CANCEL = 2;


	public static StringBuffer log = new StringBuffer();

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif
	
	/** Referência para a tela de jogo, para que seja possível retornar à tela de jogo apÃ³s entrar na tela de pausa. */
	private GameScreen gameScreen;
	
	private static boolean lowMemory;
	
	private static LoadListener loader;

	private static LedPattern bkg;

	
	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
			FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
		//#endif

		log("GameMIDlet antes");

//		System.out.println(Runtime.getRuntime().freeMemory());
	}

	
	public static final void setSpecialKeyMapping(boolean specialMapping) {
		try {
			ScreenManager.resetSpecialKeysTable();
			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

			int[][] keys = null;
			final int offset = 'a' - 'A';

			//#if BLACKBERRY_API == "true"
	//# 			switch ( Keypad.getHardwareLayout() ) {
	//# 				case ScreenManager.HW_LAYOUT_REDUCED:
	//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
	//# 					keys = new int[][] {
	//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
	//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
	//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
	//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
	//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
	//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
	//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
	//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
	//#
	//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
	//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
	//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
	//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
	//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
	//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
	//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
	//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
	//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
	//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
	//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
	//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
	//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
	//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
	//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
	//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
	//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
	//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
	//#
	//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
	//# 					 };
	//# 				break;
	//#
	//# 				default:
	//# 					if ( specialMapping ) {
	//# 						keys = new int[][] {
	//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
	//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
	//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
	//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
	//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
	//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
	//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
	//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
	//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
	//#
	//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
	//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
	//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
	//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
	//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
	//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
	//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
	//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
	//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
	//#
	//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
	//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
	//# 						 };
	//# 					} else {
	//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
	//# 							table.put( new Integer( c ), new Integer( c ) );
	//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
	//# 						}
	//#
	//# 						final int[] chars = new int[]
	//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
	//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
	//# 						};
	//#
	//# 						for ( byte i = 0; i < chars.length; ++i )
	//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
	//# 					}
	//# 			}
	//#
			//#else

				if ( specialMapping ) {
					keys = new int[][] {
						{ 'q', ScreenManager.KEY_NUM1 },
						{ 'Q', ScreenManager.KEY_NUM1 },
						{ 'e', ScreenManager.KEY_NUM3 },
						{ 'E', ScreenManager.KEY_NUM3 },
						{ 'z', ScreenManager.KEY_NUM7 },
						{ 'Z', ScreenManager.KEY_NUM7 },
						{ 'c', ScreenManager.KEY_NUM9 },
						{ 'C', ScreenManager.KEY_NUM9 },
						{ 'w', ScreenManager.UP },
						{ 'W', ScreenManager.UP },
						{ 'a', ScreenManager.LEFT },
						{ 'A', ScreenManager.LEFT },
						{ 's', ScreenManager.FIRE },
						{ 'S', ScreenManager.FIRE },
						{ 'd', ScreenManager.RIGHT },
						{ 'D', ScreenManager.RIGHT },
						{ 'x', ScreenManager.DOWN },
						{ 'X', ScreenManager.DOWN },

						{ 'r', ScreenManager.KEY_NUM1 },
						{ 'R', ScreenManager.KEY_NUM1 },
						{ 'y', ScreenManager.KEY_NUM3 },
						{ 'Y', ScreenManager.KEY_NUM3 },
						{ 'v', ScreenManager.KEY_NUM7 },
						{ 'V', ScreenManager.KEY_NUM7 },
						{ 'n', ScreenManager.KEY_NUM9 },
						{ 'N', ScreenManager.KEY_NUM9 },
						{ 't', ScreenManager.KEY_NUM2 },
						{ 'T', ScreenManager.KEY_NUM2 },
						{ 'f', ScreenManager.KEY_NUM4 },
						{ 'F', ScreenManager.KEY_NUM4 },
						{ 'g', ScreenManager.KEY_NUM5 },
						{ 'G', ScreenManager.KEY_NUM5 },
						{ 'h', ScreenManager.KEY_NUM6 },
						{ 'H', ScreenManager.KEY_NUM6 },
						{ 'b', ScreenManager.KEY_NUM8 },
						{ 'B', ScreenManager.KEY_NUM8 },

						{ 10, ScreenManager.FIRE }, // ENTER
						{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

						{ 'u', ScreenManager.KEY_STAR },
						{ 'U', ScreenManager.KEY_STAR },
						{ 'j', ScreenManager.KEY_STAR },
						{ 'J', ScreenManager.KEY_STAR },
						{ '#', ScreenManager.KEY_STAR },
						{ '*', ScreenManager.KEY_STAR },
						{ 'm', ScreenManager.KEY_STAR },
						{ 'M', ScreenManager.KEY_STAR },
						{ 'p', ScreenManager.KEY_STAR },
						{ 'P', ScreenManager.KEY_STAR },
						{ ' ', ScreenManager.KEY_STAR },
						{ '$', ScreenManager.KEY_STAR },
					 };
				} else {
					for ( char c = 'A'; c <= 'Z'; ++c ) {
						table.put( new Integer( c ), new Integer( c ) );
						table.put( new Integer( c + offset ), new Integer( c + offset ) );
					}

					final int[] chars = new int[]
					{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
						'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
					};

					for ( byte i = 0; i < chars.length; ++i )
						table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
				}
			//#endif

			if ( keys != null ) {
				for ( byte i = 0; i < keys.length; ++i )
					table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				log( e.getClass() + e.getMessage() );
				e.printStackTrace();
			//#endif
		}
	}


	/**
	 * Adiciona uma entrada no log. Não é necessário remover chamadas a esse método em versões de release, pois elas
	 * são descartadas pelo obfuscator.
	 * @param s
	 */
	public static final void log(String s) {
		//#if DEBUG == "true"
			System.gc();

			log.append(s);
			log.append(": ");
			final long freeMem = Runtime.getRuntime().freeMemory();
			log.append( freeMem );
			log.append( ':' );
			log.append( freeMem * 100 / Runtime.getRuntime().totalMemory() );
			log.append( "%\n" );

			System.out.println( s + ": " + freeMem + ": " + ( freeMem * 100 / Runtime.getRuntime().totalMemory() ) + "%" );
		//#endif
	}


	//#if DEBUG == "true"
		public final void start() {
			log( "start 1" );
			super.start();
			log( "start 2" );
		}
	//#endif


	protected final void loadResources() throws Exception {
		log("loadResources início");

		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			Vector v = new Vector();
//# 			final int MEMORY_INCREASE = 170 * 1000; // size: 170kb
//# 			int total = 0;
//# 		    try {
//# 				while( Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT ) {
//# 					v.addElement(new byte[ MEMORY_INCREASE ]);
//# 					total += MEMORY_INCREASE;
//# 				}
//# 				lowMemory = false;
//# 			} catch( Throwable e ) {
//# 				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT && total < LOW_MEMORY_LIMIT;
//# 			} finally {
//# 				v = null;
//# 				System.gc();
//# 			}
//# 			//lowMemory = true; // TODO teste
		//#endif
	   

		for (byte i = 0; i < FONT_TYPES_TOTAL; ++i) {
			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//#  				switch ( i ) {
					//#if SCREEN_SIZE == "MEDIUM"
//#  						case FONT_NUMBER_BLACK:
//#  						case FONT_TITLE:
//#  							FONTS[ i ] = FONTS[ FONT_TEXT ];
//#  						break;
					//#endif
//# 
					//#if SCREEN_SIZE == "SMALL"
//# 						case FONT_CASE_SMALL:
//# 						case FONT_TITLE:
					//#endif
//#  					case FONT_TEXT_WHITE:
//#  					case FONT_MILLION:
//#  					break;
//# 
//#  					default:
//#  						FONTS[ i ] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);
//#  				}
			//#else
				if ( i != FONT_TEXT_WHITE )
					FONTS[ i ] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);
			//#endif

			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
				//#if SCREEN_SIZE == "SMALL"
//# 					FONTS[ FONT_CASE_SMALL ] = FONTS[ FONT_TEXT ];
				//#endif
//# 				FONTS[ FONT_TITLE ] = FONTS[ FONT_TEXT ];
			//#endif

			switch ( i ) {
				case FONT_TITLE:
					//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 					break;
					//#endif
				case FONT_BANKER_VALUE:
					if ( isLowMemory() ) {
						FONTS[ i ] = FONTS[ FONT_TEXT ];
						break;
					}
				case FONT_TEXT:
				case FONT_NUMBER_BLACK:
				case FONT_NUMBER_WHITE:
				case FONT_CASE_VALUE:
				case FONT_CASE_NUMBER:
				case FONT_CASE_SMALL:
					FONTS[ i ].setCharExtraOffset( 1 );
				break;

				//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 				case FONT_MILLION:
				//#endif
				case FONT_TEXT_WHITE:
				break;

				default:
					FONTS[ i ].setCharExtraOffset( DEFAULT_FONT_OFFSET );
			}
			log( "FONTE " + i );
		}

		log( "database 0" );
		//#if NANO_RANKING == "true"
			NanoOnline.setRankingTypes( NanoOnline.RANKING_TYPES_GLOBAL_ONLY );
			final int[] entries = new int[] { TEXT_RANKING_TOTAL_MONEY, TEXT_RANKING_MONEY_AVERAGE, TEXT_RANKING_MILLION, TEXT_RANKING_50_CENTS };
			RankingScreen.init( entries, this );
		//#endif
		
		log( "database 1" );
		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
		}
		
		log( "database 2" );

		setLanguage( ( byte ) 2 ); // NanoOnline.LANGUAGE_pt_BR
		log( "textos" );

		bkg = new LedPattern();
		log( "ledPattern" );

		setSpecialKeyMapping(true);
		log("loadResources fim");
		setScreen( SCREEN_LOADING_1 );
	} // fim do mÃ©todo loadResources()

	
	protected final void changeLanguage( byte language ) {
		GameMIDlet.log( "changeLanguage início" );
		language = 2;// NanoOnline.LANGUAGE_pt_BR;
		try {
			loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts_" + language + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
		}
		GameMIDlet.log( "changeLanguage fim" );
	}

	
	public final void destroy(){
		super.destroy();
		if ( gameScreen != null ) {
			onGameOver( gameScreen.getScore(), false );
			gameScreen = null;
		}
	}


	protected final int changeScreen(int screen) throws Exception {
		final GameMIDlet midlet = (GameMIDlet) instance;

		Drawable nextScreen = null;
		log("changeScreen " + screen);

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		bkg.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		byte bkgType = BACKGROUND_TYPE_LED_PATTERN;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;

		switch (screen) {
			case SCREEN_CHOOSE_SOUND:
				nextScreen = new TopaMenu(screen, new int[]{
							MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
							TEXT_VOLUME,
							TEXT_OK,
						}, TEXT_DO_YOU_WANT_SOUND);
				( ( TopaMenu ) nextScreen ).setCurrentIndex( ENTRY_SOUND_OK );
				for ( byte i = 0; i < 20; ++i )
					( ( TopaMenu ) nextScreen ).update( 200 );
				( ( TopaMenu ) nextScreen ).setPlaySound( false );
				refreshVolumeLabel( ( ( TopaMenu ) nextScreen ).getMenuDrawable( ENTRY_SOUND_VOLUME ) );
			break;

			case SCREEN_CHOOSE_LANGUAGE:
				nextScreen = new TopaMenu(screen, new int[]{
							TEXT_ENGLISH,
							TEXT_PORTUGUESE,
						},TEXT_CHOOSE_LANGUAGE);
                break;

			case SCREEN_SPLASH_NANO:
				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE != "SMALL"
					nextScreen = new BasicSplashNano(SCREEN_SPLASH_ENDEMOL, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#else
//# 				nextScreen = new BasicSplashNano(SCREEN_SPLASH_ENDEMOL, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#endif
				break;

			case SCREEN_SPLASH_ENDEMOL:
				nextScreen = new SplashEndemol();
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame();
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_MAIN_MENU:
				if ( isLowMemory() )
					gameScreen = null;

				setSpecialKeyMapping(true);
				
				//#if NANO_RANKING == "true"
					if (nanoOnlineForm != null) {
						NanoOnline.unload();
						nanoOnlineForm = null;
					}
				//#endif
				nextScreen = new TopaMenu(screen, new int[]{
							TEXT_NEW_GAME,
							TEXT_OPTIONS,
							TEXT_NEWS,
							TEXT_NANO_ONLINE,
							//#if RECOMMEND_SCREEN == "true"
//# 								TEXT_RECOMMEND_TITLE,
							//#endif
							TEXT_HELP,
							TEXT_CREDITS,
							TEXT_EXIT,
						}, -1 );
			break;

			case SCREEN_NEW_GAME:
				MediaPlayer.free();
				gameScreen.prepare();
				// sem break mesmo
				
			case SCREEN_CONTINUE_GAME:
				setSpecialKeyMapping(true);
				bkgType = BACKGROUND_TYPE_NONE;
				nextScreen = gameScreen;

				// em aparelhos com pouca memória, não mantém os sprites-base do menu alocados
				MediaPlayer.free();
				if ( isLowMemory() )
					MenuLabel.unload();
			break;

			case SCREEN_GAME_OVER:
				if ( isLowMemory() )
					gameScreen = null;
				
				nextScreen = new TopaMenu( screen, new int[] {
					TEXT_HIGH_SCORES,
					TEXT_NEW_GAME,
					TEXT_BACK_MENU,
					TEXT_EXIT_GAME,
					}, null );
			break;

			//#if NANO_RANKING == "true"
				case SCREEN_NEWS_MENU:
					NanoOnline.load( NanoOnline.LANGUAGE_pt_BR, APP_SHORT_NAME, SCREEN_MAIN_MENU );
					final NewsFeederManager newsManager = new NewsFeederManager();
					newsManager.load();

					final NewsFeederEntry[] newsEntries = newsManager.getEntries( NEWS_CATEGORY_ID );
					final String[] entries = new String[ newsEntries.length + 2 ];
					for ( short i = 0; i < newsEntries.length; ++i )
						entries[ i ] = newsEntries[ i ].getTitle().toUpperCase();
					entries[ newsEntries.length ] = getText( TEXT_UPDATE );
					entries[ newsEntries.length + 1 ] = getText( TEXT_BACK );

					nextScreen = new TopaMenu( screen, entries, getText( TEXT_NEWS ) );
				break;

				case SCREEN_NEWS_VIEW_FIRST: case SCREEN_NEWS_VIEW_FIRST + 1: case SCREEN_NEWS_VIEW_FIRST + 2:
				case SCREEN_NEWS_VIEW_FIRST + 3: case SCREEN_NEWS_VIEW_FIRST + 4: case SCREEN_NEWS_VIEW_FIRST + 5:
				case SCREEN_NEWS_VIEW_FIRST + 6: case SCREEN_NEWS_VIEW_FIRST + 7: case SCREEN_NEWS_VIEW_FIRST + 8:
				case SCREEN_NEWS_VIEW_FIRST + 9: case SCREEN_NEWS_VIEW_FIRST + 10: case SCREEN_NEWS_VIEW_FIRST + 11:
				case SCREEN_NEWS_VIEW_FIRST + 12: case SCREEN_NEWS_VIEW_FIRST + 13: case SCREEN_NEWS_VIEW_FIRST + 14:
				case SCREEN_NEWS_VIEW_FIRST + 15: case SCREEN_NEWS_VIEW_FIRST + 16: case SCREEN_NEWS_VIEW_FIRST + 17:
				case SCREEN_NEWS_VIEW_FIRST + 18: case SCREEN_NEWS_VIEW_LAST:
					final NewsFeederManager m = new NewsFeederManager();
					m.load();
					final NewsFeederEntry entry = m.getEntries( NEWS_CATEGORY_ID )[ screen - SCREEN_NEWS_VIEW_FIRST ];

					nextScreen = new TextScreen( SCREEN_NEWS_MENU, entry.getContent(), entry.getTitle().toUpperCase(), false, null );
				break;

				case SCREEN_NEWS_REFRESH:
					nextScreen = new LoadScreen(
						new LoadListener() {
							public final void load(final LoadScreen loadScreen) throws Exception {
								try {
									final ByteArrayOutputStream b = new ByteArrayOutputStream();
									final DataOutputStream out = new DataOutputStream( b );

									// escreve os dados globais - inclusive as informações do news feeder
									NanoOnline.writeGlobalData( out );
									out.flush();

									NanoConnection.post( NanoOnline.NANO_ONLINE_URL + "news_feeds/refresh", b.toByteArray(), midlet, false );
								} catch ( Exception e ) {
									//#if DEBUG == "true"
										e.printStackTrace();
									//#endif
									loadScreen.setActive(false);
									setScreen(SCREEN_MAIN_MENU);
								}
							}
						} );
					bkgType = BACKGROUND_TYPE_LED_PATTERN;
				break;

			//#endif

			case SCREEN_OPTIONS:
				if (MediaPlayer.isVibrationSupported()) {
					nextScreen = new TopaMenu(screen, new int[]{
						MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
						TEXT_VOLUME,
						MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON,
						TEXT_BACK,
						}, TEXT_OPTIONS );
				} else {
					nextScreen = new TopaMenu(screen, new int[]{
							MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
							TEXT_VOLUME,
							TEXT_BACK,
							}, TEXT_OPTIONS );
				}
				refreshVolumeLabel( ( ( TopaMenu ) nextScreen ).getMenuDrawable( ENTRY_OPTIONS_MENU_VOLUME ) );
			break;

			case SCREEN_HELP_MENU:
				nextScreen = new TopaMenu(screen, new int[]{
							TEXT_RULES,
							TEXT_CONTROLS,
							TEXT_BACK,}, TEXT_HELP );
				break;

			case SCREEN_ERROR_LOG:
			case SCREEN_HELP_RULES:
			case SCREEN_HELP_CONTROLS:
				String title = null;
				switch ( screen ) {
					case SCREEN_HELP_RULES:
						title = getText( TEXT_RULES );
					break;

					case SCREEN_HELP_CONTROLS:
						title = getText( TEXT_CONTROLS );
					break;
				}
				//#if DEBUG == "true"
					nextScreen = new TextScreen( SCREEN_HELP_MENU, log.toString(), title, false, null );
				//#else
//# 					nextScreen = new TextScreen( SCREEN_HELP_MENU, getText( TEXT_HELP_CONTROLS + (screen - SCREEN_HELP_CONTROLS)) + getVersion(), title, false, null );
				//#endif
				 
				break;

			case SCREEN_CREDITS:
				nextScreen = new TextScreen( SCREEN_MAIN_MENU, getText( TEXT_CREDITS_TEXT ), getText( TEXT_CREDITS ), true, null );
			break;

			case SCREEN_PAUSE:
				indexSoftRight = SOFT_KEY_REMOVE;
				
				if (MediaPlayer.isVibrationSupported()) {
					nextScreen = new TopaMenu(screen, new int[]{
								TEXT_CONTINUE,
								MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
								TEXT_VOLUME,
								MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON,
								TEXT_BACK_MENU,
								TEXT_EXIT_GAME}, TEXT_PAUSE );
				} else {
					nextScreen = new TopaMenu(screen, new int[]{
								TEXT_CONTINUE,
								MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF,
								TEXT_VOLUME,
								TEXT_BACK_MENU,
								TEXT_EXIT_GAME}, TEXT_PAUSE );
				}
				( ( TopaMenu ) nextScreen ).setPlaySound( false );
				( ( TopaMenu ) nextScreen ).setBackIndex( 0 );
				refreshVolumeLabel( ( ( TopaMenu ) nextScreen ).getMenuDrawable( ENTRY_PAUSE_MENU_VOLUME ) );
			break;

			case SCREEN_CONFIRM_MENU:
				nextScreen = new TopaMenu(screen, new int[]{
							TEXT_YES,
							TEXT_NO
						}, TEXT_CONFIRM_BACK_MENU);
			break;

			case SCREEN_CONFIRM_EXIT:
				nextScreen = new TopaMenu(screen, new int[]{
							TEXT_YES,
							TEXT_NO
						}, TEXT_CONFIRM_EXIT_1);
			break;

			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen(
						new LoadListener() {
							public final void load(final LoadScreen loadScreen) throws Exception {
								// aloca os sons
								final String[] soundList = new String[SOUND_TOTAL];
								for (byte i = 0; i < SOUND_TOTAL; ++i) {
									soundList[i] = PATH_SOUNDS + i + ".mid";
								}
								MediaPlayer.init(DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList);

								ScrollBar.loadImages();
								Case.loadImages();

								loadScreen.setActive(false);
								setScreen( SCREEN_CHOOSE_SOUND );
							}
						} );
				bkgType = BACKGROUND_TYPE_LED_PATTERN;
				break;

			case SCREEN_LOADING_2:
				nextScreen = new LoadScreen(new LoadListener() {
					public final void load(final LoadScreen loadScreen) throws Exception {
						Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
						if ( gameScreen == null ) {
							gameScreen = new GameScreen();
						}
						loadScreen.setActive(false);
						setScreen(SCREEN_NEW_GAME);
					}
				} );

				bkgType = BACKGROUND_TYPE_LED_PATTERN;
				break;

			case SCREEN_LOADING_PLAY_SCREEN:
				nextScreen = new LoadScreen( loader );
				loader = null;

				bkgType = BACKGROUND_TYPE_LED_PATTERN;
			break;

			//#if NANO_RANKING == "true"

			case SCREEN_NANO_RANKING_MENU:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
				break;

			case SCREEN_LOADING_NANO_ONLINE:
				nextScreen = new LoadScreen(new LoadListener() {
					public final void load(final LoadScreen loadScreen) throws Exception {
						MediaPlayer.free();
						setSpecialKeyMapping(false);

						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_MAIN_MENU);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_MENU);
					}
				});
			break;

			case SCREEN_NANO_RANKING_PROFILES:
				nextScreen = nanoOnlineForm;
				bkgType = BACKGROUND_TYPE_NONE;
			break;

			case SCREEN_HELP_PROFILE:
				nextScreen = new TextScreen( SCREEN_CHOOSE_PROFILE, getText( TEXT_HELP_PROFILE_TEXT ) + getVersion(), getText( TEXT_HELP ), false, null );
			break;

			case SCREEN_LOADING_PROFILES_SCREEN:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						setSpecialKeyMapping(false);
						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_PROFILES);
					}
				});
				break;

			case SCREEN_CHOOSE_PROFILE:
				setSpecialKeyMapping(false);
				final Customer c = NanoOnline.getCurrentCustomer();
				// a fonte sÃ³ possui caracteres maiÃºsculos
				final String name = (getText(TEXT_CURRENT_PROFILE) + (c.getId() == Customer.ID_NONE ? getText(TEXT_NO_PROFILE) : c.getNickname())).toUpperCase();
				nextScreen = new TopaMenu(screen,  new int[]{
							TEXT_CONFIRM,
							TEXT_CHOOSE_ANOTHER,
							TEXT_HELP,
							TEXT_BACK
						}, name);
				break;

			case SCREEN_LOADING_HIGH_SCORES:
				nextScreen = new LoadScreen(new LoadListener() {

					public final void load(final LoadScreen loadScreen) throws Exception {
						MediaPlayer.free();

						setSpecialKeyMapping(false);
						nanoOnlineForm = NanoOnline.load(language, APP_SHORT_NAME, SCREEN_MAIN_MENU, NanoOnline.SCREEN_RECORDS);

						loadScreen.setActive(false);

						setScreen(SCREEN_NANO_RANKING_MENU);
					}
				});
				break;
			//#endif
		} // fim switch ( screen )
		
		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );

		setBackground( bkgType );

		//#if DEBUG == "true"
			if ( nextScreen == null )
				throw new IllegalStateException( "Warning: nextScreen null (" + screen + ")." );
		//#endif

		midlet.manager.setCurrentScreen( nextScreen );

		return screen;
	} // fim do mÃ©todo changeScreen( int )


	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_LED_PATTERN:
				midlet.manager.setBackground( bkg, true );
			break;
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( 0x000000 );
			break;
		}
	} // fim do mÃ©todo setBackground( byte )

	
	public static final Drawable getScrollFull() throws Exception {
		return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
	}


	public static final Drawable getScrollPage() throws Exception {
		return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
	}


	private static final void refreshVolumeLabel( Drawable label ) {
		( ( MenuLabel ) label ).setText( getText( TEXT_VOLUME ) + MediaPlayer.getVolume() + '%' );
	}
	
	
	private static final void refreshSoundLabel( Drawable label ) {
		try {
			( ( MenuLabel ) label ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}


	private static final void refreshVibrationLabel( Drawable label ) {
		try {
			( ( MenuLabel ) label ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}


	public static final boolean isLowMemory() {
		return lowMemory;
	}
	
	
	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				setSpecialKeyMapping(true);
				//#if RECOMMEND_SCREEN == "false"
					if ( index >= ENTRY_MAIN_MENU_RECOMMEND )
						++index;
				//#endif
				switch (index) {
					case ENTRY_MAIN_MENU_NEW_GAME:
						//#if NANO_RANKING == "true"
							setScreen(SCREEN_CHOOSE_PROFILE);
						//#else
//# 								setScreen( SCREEN_LOADING_2 );
						//#endif
					break;

					case ENTRY_MAIN_MENU_RECOMMEND:
					break;
					
					case ENTRY_MAIN_MENU_OPTIONS:
						setScreen(SCREEN_OPTIONS);
					break;

					case ENTRY_MAIN_MENU_NEWS:
						setScreen( SCREEN_NEWS_MENU );
					break;

					//#if NANO_RANKING == "true"
						case ENTRY_MAIN_MENU_NANO_ONLINE:
							setScreen(SCREEN_LOADING_NANO_ONLINE);
						break;
					//#endif

					case ENTRY_MAIN_MENU_HELP:
						setScreen( SCREEN_HELP_MENU );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					case ENTRY_MAIN_MENU_EXIT:
						MediaPlayer.saveOptions();
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU

			case SCREEN_NEWS_MENU:
				if ( index == menu.getTotalSlots() - 1 ) {
					setScreen( SCREEN_MAIN_MENU );
				} else if ( index == menu.getTotalSlots() - 2 ) {
					setScreen( SCREEN_NEWS_REFRESH );
				} else {
					setScreen( SCREEN_NEWS_VIEW_FIRST + index );
				}
			break;

			case SCREEN_NEWS_VIEW_FIRST:
				setScreen( SCREEN_NEWS_MENU );
			break;

			case SCREEN_GAME_OVER:
				switch ( index ) {
					case ENTRY_GAME_OVER_HIGH_SCORES:
						setScreen( SCREEN_LOADING_HIGH_SCORES );
					break;

					case ENTRY_GAME_OVER_NEW_GAME:
						setScreen( SCREEN_NEW_GAME );
					break;

					case ENTRY_GAME_OVER_BACK_MENU:
						setScreen( SCREEN_MAIN_MENU );
					break;

					case ENTRY_GAME_OVER_EXIT_GAME:
						exit();
					break;
				}
			break;
			
			//#if NANO_RANKING == "true"
				case SCREEN_CHOOSE_PROFILE:
					switch (index ) {
						case ENTRY_CHOOSE_PROFILE_USE_CURRENT:
							if (nanoOnlineForm != null) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
							setScreen(SCREEN_LOADING_2);
						break;

						case ENTRY_CHOOSE_PROFILE_CHANGE:
							setScreen(SCREEN_LOADING_PROFILES_SCREEN);
						break;

						case ENTRY_CHOOSE_PROFILE_HELP:
							setScreen( SCREEN_HELP_PROFILE );
						break;

						case ENTRY_CHOOSE_PROFILE_BACK:
							if (nanoOnlineForm != null) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
							setScreen(SCREEN_MAIN_MENU);

						break;
					}
				break;
			//#endif

			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_RULES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			case SCREEN_PAUSE:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen(SCREEN_CONTINUE_GAME );
						break;

						case ENTRY_PAUSE_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							MediaPlayer.play( SOUND_CASE_GOOD );
							refreshSoundLabel( menu.getDrawable( index ) );
						break;

						case ENTRY_PAUSE_MENU_VOLUME:
							MediaPlayer.setVolume( 25 + ( MediaPlayer.getVolume() % 100 ) );
							refreshVolumeLabel( menu.getDrawable( index ) );
						break;

						case ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							refreshVibrationLabel( menu.getDrawable( index ) );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
						break;
						
						case ENTRY_PAUSE_MENU_VIB_EXIT_GAME:
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}
				} else {
					switch ( index ) {
						case ENTRY_PAUSE_MENU_CONTINUE:
							setSpecialKeyMapping(true);
							setScreen(SCREEN_CONTINUE_GAME);
							break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU:
							setScreen( SCREEN_CONFIRM_MENU );
						break;

						case ENTRY_PAUSE_MENU_VOLUME:
							MediaPlayer.setVolume( 25 + ( MediaPlayer.getVolume() % 100 ) );
							refreshVolumeLabel( menu.getDrawable( index ) );
						break;
						
						case ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME:			
							setScreen( SCREEN_CONFIRM_EXIT );
						break;
					}					
				}
			break; // fim case SCREEN_PAUSE
			
			case SCREEN_OPTIONS:
				if ( MediaPlayer.isVibrationSupported() ) {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT );
							( ( MenuLabel ) menu.getDrawable( index ) ).setText( MediaPlayer.isVibration() ? TEXT_TURN_VIBRATION_OFF : TEXT_TURN_VIBRATION_ON );
							break;

						case ENTRY_OPTIONS_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							MediaPlayer.play( SOUND_CASE_GOOD );
							( ( MenuLabel ) menu.getDrawable( index ) ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case ENTRY_OPTIONS_MENU_VOLUME:
							MediaPlayer.setVolume(  25 + ( ( MediaPlayer.getVolume() ) % 100 ) );
                            refreshVolumeLabel( ( MenuLabel ) menu.getDrawable( index ) );
						break;

						case ENTRY_OPTIONS_MENU_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen(  SCREEN_MAIN_MENU );
							break;
					}
				} else {
					switch ( index ) {
						case ENTRY_OPTIONS_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
							MediaPlayer.play(SOUND_CASE_GOOD);
							( ( MenuLabel ) menu.getDrawable( index ) ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
						break;

						case ENTRY_OPTIONS_MENU_VOLUME:
							MediaPlayer.setVolume(  25 +( ( MediaPlayer.getVolume() ) % 100 ) );
                            refreshVolumeLabel( ( MenuLabel ) menu.getDrawable( index ) );
						break;

						case ENTRY_OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen(SCREEN_MAIN_MENU);
							break;
					}
				}
			break; // fim case SCREEN_OPTIONS
			
			case SCREEN_CONFIRM_MENU:
				switch ( index ) {
					case CONFIRM_YES:
						MediaPlayer.saveOptions();
						MediaPlayer.stop();
						setScreen( SCREEN_MAIN_MENU );
					break;
						
					case CONFIRM_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}				
			break;
			
			case SCREEN_CONFIRM_EXIT:
				switch ( index ) {
					case CONFIRM_YES:
						MediaPlayer.saveOptions();
						exit();
					break;
						
					case CONFIRM_NO:
						setScreen( SCREEN_PAUSE );
					break;
				}
			break;

			case SCREEN_CHOOSE_SOUND:
				switch ( index ) {
					case ENTRY_SOUND_ON_OFF:
						MediaPlayer.setMute( !MediaPlayer.isMuted() );
						MediaPlayer.play(SOUND_CASE_GOOD);
						( ( MenuLabel ) menu.getDrawable( index ) ).setText( MediaPlayer.isMuted() ? TEXT_TURN_SOUND_ON : TEXT_TURN_SOUND_OFF );
					break;

					case ENTRY_SOUND_VOLUME:
						MediaPlayer.setVolume( 25 + ( ( MediaPlayer.getVolume() ) % 100 ) );
						refreshVolumeLabel( ( MenuLabel ) menu.getDrawable( index ) );
					break;

					case ENTRY_SOUND_OK:
						setScreen(SCREEN_SPLASH_NANO);
						break;
				}
			break;
				
//			case SCREEN_CHOOSE_LANGUAGE:
//				try {
//					switch (index) {
//						case OPTION_PORTUGUESE:
//							setLanguage( NanoOnline.LANGUAGE_pt_BR );
//						break;
//
//						case OPTION_ENGLISH:
//						default:
//							setLanguage( NanoOnline.LANGUAGE_en_US );
//						break;
//					}
//					AppMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
//					setScreen(SCREEN_CHOOSE_SOUND);
//				} catch (Exception e) {
//					//#if DEBUG == "true"
//						e.printStackTrace();
//					//#endif
//
//					exit();
//				}
//			break;
		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		try {
			for ( byte i = 0; i < menu.getTotalSlots(); ++i ) {
				if ( menu.getDrawable( i ) instanceof MenuLabel ) {
					( ( MenuLabel ) menu.getDrawable( i ) ).setCurrent( i == index );
				}
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}


	public static final void onGameOver( int score, boolean showGameOverScreen ) {
		//#if NANO_RANKING == "true"
			if ( score > 0 ) {
				final Customer c = NanoOnline.getCurrentCustomer();
				try {
					if ( c.isRegistered() ) {
						switch ( score ) {
							case GameScreen.VALUE_MIN:
								RankingScreen.setHighScore( RANKING_INDEX_50_CENTS, c.getId(), 1 );
							break;

							case GameScreen.VALUE_MILLION:
								RankingScreen.setHighScore( RANKING_INDEX_MILLION, c.getId(), 1 );
							break;
						}
						RankingScreen.setHighScore( RANKING_INDEX_MONEY_AVERAGE, c.getId(), score );
						RankingScreen.setHighScore( RANKING_INDEX_TOTAL_MONEY, c.getId(), score );
					}
				} catch ( Exception e ) {
					//#if DEBUG == "true"
						e.printStackTrace();
					//#endif
				}
			}
		//#endif
		
		if ( showGameOverScreen )
			setScreen( SCREEN_GAME_OVER );
	}



	/**
	 * Define uma soft key a partir de um texto. Equivalente Ã  chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visÃ­vel. Para o label estar sempre visÃ­vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
//		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
//		} else {
//			try {
//				setSoftKey( softKey, new Label( getFont( FONT_INDEX_TEXT ), getText( textIndex ) ), true, visibleTime );
//			} catch ( Exception e ) {
//				//#if DEBUG == "true"
//					e.printStackTrace();
//				//#endif
//			}
//		}
	} // fim do mÃ©todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		ScreenManager.getInstance().setSoftKey( softKey, d );
	} // fim do método setSoftKey( byte, Drawable, boolean, int )

	
	public static final String getCurrencyString( long value ) {
		return getCurrencyString( value, false, true );
	}


	public static final String getCurrencyString( long value, boolean useCurrency ) {
		return getCurrencyString( value, false, useCurrency );
	}


	public static final String getCurrencyString( long value, boolean shortMode, boolean useCurrency ) {
		final String currency = useCurrency ? getText( TEXT_CURRENCY ) : "";
		
		if ( value < 100 ) {
			// centavos
			if ( shortMode )
				return value + getText( TEXT_CURRENCY_CENTS );
			else
				return currency + "0," + value;
		} else {
			if ( shortMode && value == GameScreen.VALUE_MILLION ) {
				return getText( TEXT_MILLION );
			} else {
				String s = String.valueOf( value / 100 );
				String res = "";
				for ( int i = s.length() - 1, count = 2; i >= 0; --i ) {
					if ( count > 0 || i == 0 ) {
						--count;
						res = s.charAt( i ) + res;
					} else {
						count = 2;
						res = "." + s.charAt( i ) + res;
					}
				}

				if ( shortMode )
					if(value == 100 ) return res + getText(TEXT_CURRENCY_NAME_SINGULAR );
					else  return res + getText(TEXT_CURRENCY_NAME );
				else
					return currency + res;
			}
		}
	}


	protected final ImageFont getFont( int index ) {
		try {
			switch ( index ) {
				case FONT_TEXT_WHITE:
					final String prefix = PATH_IMAGES + "font_" + FONT_TEXT;
					final PaletteMap[] map = new PaletteMap[] {
						new PaletteMap( 0x060a23, 0xffffff ),
						new PaletteMap( 0x000000, 0xffffff )
					};
					final PaletteChanger p = new PaletteChanger( prefix + ".png" );
					final ImageFont font = ImageFont.createMultiSpacedFont( p.createImage( map ), prefix + ".bin" );
					font.setCharExtraOffset( getFont( FONT_TEXT ).getCharExtraOffset() );
					return font;

			case FONT_MILLION:
				return ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + index );

			case FONT_CASE_NUMBER:
				if ( FONTS[ index ] == null ) {
					FONTS[ index ] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + index );
					FONTS[ index ].setCharExtraOffset( 1 );
				}

			default:
				return FONTS[ index ];
			}
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
			//#endif
			return null;
		}
	}


	public static final void unloadFontCaseValue() {
		if ( isLowMemory() )
			( ( GameMIDlet ) instance ).FONTS[ FONT_CASE_NUMBER ] = null;
	}

    
    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "";

        return "<ALN_H>" + getText( TEXT_VERSION ) + version + "\n\n";
    }    
	
	
	public final void write(DataOutputStream output) throws Exception {
		output.writeByte(language);
	}


	public final void read( DataInputStream input ) throws Exception {
		setLanguage( input.readByte() );
	}


	//#if NANO_RANKING == "true"
		public final void processData( int id, byte[] data ) {
			try {
				NanoOnline.readGlobalData( data );
				setScreen(SCREEN_NEWS_MENU);
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif
				setScreen(SCREEN_MAIN_MENU);
			}
		}


		public final void onInfo( int id, int infoIndex, Object extraData ) {
			//#if DEBUG == "true"
				System.out.println( "onInfo: " + id + ", " + infoIndex + ", " + extraData );
			//#endif
		}


		public final void onError( int id, int errorIndex, Object extraData ) {
			//#if DEBUG == "true"
				System.out.println( "onError: " + id + ", " + errorIndex + ", " + extraData );
			//#endif
			setScreen(SCREEN_MAIN_MENU);
		}


		public String format( int type, long score ) {
			switch ( type ) {
				case RANKING_INDEX_50_CENTS:
				case RANKING_INDEX_MILLION:
					return String.valueOf( score );

				case RANKING_INDEX_MONEY_AVERAGE:
				case RANKING_INDEX_TOTAL_MONEY:
					return getCurrencyString( score, true );

				default:
					//#if DEBUG == "true"
						throw new IllegalStateException( "tipo de ranking inválido: " + type );
					//#else
//# 							return "";
					//#endif
			}
		}

		public final void initLocalEntry(int type, RankingEntry entry, int index) {
		}


		public final int getRankingType( int rankingIndex ) {
			switch ( rankingIndex ) {
				case RANKING_INDEX_MILLION:
				case RANKING_INDEX_MONEY_AVERAGE:
				return RANKING_TYPE_AVERAGE_DESCRESCENT;

				case RANKING_INDEX_50_CENTS:
				case RANKING_INDEX_TOTAL_MONEY:
				return RANKING_TYPE_CUMULATIVE_DECRESCENT;

				default:
					return -1;
			}
		}
	//#endif

}
