/**
 * SplashGame.java
 * �2007 Nano Games
 *
 * Created on 20/12/2007 20:01:20
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableArc;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Constants;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
import core.TitleLabel;
//#endif

import core.TitleLabel;

/**
 *
 * @author Peter
 */
public final class SplashGame extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{

	/** quantidade total de itens do grupo */
	private static final byte TOTAL_ITEMS = 20;
	
	private final DrawableArc arcTop;
	private final DrawableArc arcBottom;

	private final BasicAnimatedPattern bkg1;

	private static final short TIME_LOOP_ANIMATION = 4000;

	/***/
	private static final short TIME_WAIT_LOGO = 200;
	
	private static final short TIME_LOGO_ENTERS = 2000;

	private int cameraAccTime;
	private int cameraTotalTime;

	private final BezierCurve cameraTweener = new BezierCurve();

	private final Point cameraPosition = new Point();

	private final Point cameraLimit = new Point();

	private final Pattern bkg;

	private static final byte STATE_WAIT_LOGO	= 0;
	private static final byte STATE_LOGO_ENTERS	= 1;
	private static final byte STATE_SHINE		= 2;
	private static final byte STATE_ANIMATE		= 3;
	private static final byte STATE_PRESS_KEY	= 4;

	private byte state;

	private static final byte SEQUENCE_STOPPED	= 0;
	private static final byte SEQUENCE_FRONT	= 1;
	private static final byte SEQUENCE_BACK		= 2;

	private final Sprite[] shineBack = new Sprite[ 2 ];
	private final Sprite[] shineFront = new Sprite[ 2 ];

	private final Sprite shineRight;
	private final Sprite shineLeft;

	private final Drawable[] logoParts;
	private final BezierCurve[] logoCurves;

	private final short LOGO_WIDTH;

	private int accTime;
	
	
	public SplashGame() throws Exception {
		super( TOTAL_ITEMS );

		final int SPEED = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) / 7;
		final DrawableImage img1 = new DrawableImage( PATH_SPLASH + "bkg_1.png" );
		bkg1 = new BasicAnimatedPattern( img1, 0, SPEED );
		bkg1.setAnimation( BasicAnimatedPattern.ANIMATION_VERTICAL );
		insertDrawable( bkg1 );

		bkg = new Pattern(new DrawableImage(PATH_SPLASH + "bkg_0.png"));
		bkg.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		insertDrawable(bkg);

		img1.setSize( bkg.getFill().getWidth(), img1.getHeight() );

		arcTop = new DrawableArc();
		insertDrawable( arcTop );

		arcBottom = new DrawableArc();
		insertDrawable( arcBottom );

		for ( byte i = 0; i < 2; ++i ) {
			shineBack[ i ] = new Sprite( PATH_SPLASH + "shine" );
			shineBack[ i ].mirror( TRANS_MIRROR_H );
			shineBack[ i ].setListener( this, i << 1 );
			shineBack[ i ].defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
			shineBack[ i ].setVisible( false );

			if ( i == 0 )
				insertDrawable( shineBack[ i ] );
		}

		final DrawableGroup logo = TitleLabel.getLogo();
		LOGO_WIDTH = ( short ) logo.getWidth();
		logoParts = new Drawable[ logo.getTotalSlots() ];
		logoCurves = new BezierCurve[ logo.getTotalSlots() ];
		for ( byte i = 0; i < logoParts.length; ++i ) {
			logoParts[ i ] = logo.getDrawable( i );
			logoParts[ i ].setPosition( -10000, -10000 );
			insertDrawable( logoParts[ i ] );
			logoCurves[ i ] = new BezierCurve();
		}

		shineRight = new Sprite( PATH_SPLASH + "shine2" );
		shineRight.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		shineRight.setListener( this, -1 );
		shineRight.setVisible( false );
		insertDrawable( shineRight );

		shineLeft = new Sprite( shineRight );
		shineLeft.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		shineLeft.mirror( TRANS_MIRROR_H );
		shineLeft.setVisible( false );
		insertDrawable( shineLeft );

		for ( byte i = 0; i < 2; ++i ) {
			shineFront[ i ] = new Sprite( shineBack[ i ] );
			shineFront[ i ].defineReferencePixel( ANCHOR_VCENTER | ANCHOR_LEFT );
			shineFront[ i ].setListener( this, 1 + ( i << 1 ) );
			shineFront[ i ].setVisible( false );
			if ( i == 0 )
				insertDrawable( shineFront[ i ] );
		}

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setState( STATE_WAIT_LOGO );
	}


	private final void setCameraLoop() {
		cameraTotalTime = TIME_LOOP_ANIMATION;
		cameraTweener.origin.set( Math.min( 0, ( cameraLimit.x ) >> 1 ), cameraPosition.y );
		cameraTweener.destiny.set( cameraTweener.origin.x, cameraPosition.y );

		cameraTweener.control1.set( ( cameraLimit.x >> 1 ) + getWidth(), cameraPosition.y );
		cameraTweener.control2.set( ( cameraLimit.x >> 1 ) - getWidth(), cameraPosition.y );
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;

		switch ( state ) {
			case STATE_WAIT_LOGO:
				setCameraLoop();
			break;

			case STATE_LOGO_ENTERS:
				positionItems();
			break;

			case STATE_SHINE:
				positionItems();
				
				shineRight.setVisible( true );
				shineLeft.setVisible( true );

				shineRight.setSequence( 1 );
				shineLeft.setSequence( 1 );
			break;
			
			case STATE_ANIMATE:
				positionItems();
				shineBack[ 0 ].setSequence( SEQUENCE_FRONT );
				shineFront[ 0 ].setVisible( true );
				shineFront[ 0 ].setSequence( SEQUENCE_BACK );
			break;

			case STATE_PRESS_KEY:
			break;
		}
	}


	private final void positionItems() {
		final int y = getHeight() / 3;
		final int START_X = ( getWidth() - LOGO_WIDTH ) >> 1;

		logoCurves[ 0 ].origin.set( -logoParts[ 0 ].getWidth(), y );
		logoCurves[ 0 ].destiny.set( START_X, y );

		logoCurves[ 1 ].origin.set( ( getWidth() - logoParts[ 1 ].getWidth() ) >> 1, -logoParts[ 1 ].getHeight() );
		logoCurves[ 1 ].destiny.set( START_X + logoParts[ 0 ].getWidth(), y );

		logoCurves[ 2 ].origin.set( getWidth(), y );
		logoCurves[ 2 ].destiny.set( START_X + LOGO_WIDTH - logoParts[ 2 ].getWidth(), y );

		for ( byte i = 0; i < logoCurves.length; ++i ) {
			logoCurves[ i ].control1.set( NanoMath.lerpInt( logoCurves[ i ].origin.x, logoCurves[ i ].destiny.x, 15 ),
										  NanoMath.lerpInt( logoCurves[ i ].origin.y, logoCurves[ i ].destiny.y, 15 ) );
			logoCurves[ i ].control2.set( logoCurves[ i ].destiny );
		}

		for ( byte i = 0; i < 2; ++i ) {
			shineBack[ i ].setRefPixelPosition( getWidth() >> 1, logoParts[ 0 ].getPosY() + ( logoParts[ 0 ].getHeight() >> 1 ) + SPLASH_SHINE_OFFSET_Y );
			shineFront[ i ].setRefPixelPosition( getWidth() >> 1, shineBack[ i ].getRefPixelY() );
		}

		shineLeft.setRefPixelPosition( getWidth() >> 1, y );
		shineRight.setRefPixelPosition( shineLeft.getRefPixelX(), y + logoParts[ 0 ].getHeight() );
	}


	public final void setSize(int width, int height) {
		super.setSize( width, height );

		bkg1.setSize( STAGE_WIDTH, height );

		arcTop.setSize( width, height / 12 );
		arcTop.defineReferencePixel( ANCHOR_CENTER );
		arcTop.setRefPixelPosition( width >> 1, 0 );

		arcBottom.setSize( arcTop.getSize() );
		arcBottom.defineReferencePixel( ANCHOR_CENTER );
		arcBottom.setRefPixelPosition( width >> 1, height );

		cameraLimit.set( Math.min( 0, width - STAGE_WIDTH ), 0 );
	}

		
	public final void sizeChanged(int width, int height) {
		if (width != size.x || height != size.y) {
			setSize(width, height);
			positionItems();
		}
	}

	public final void update(int delta) {
		super.update( delta );
		updateCamera( delta );
		
		accTime += delta;
		switch ( state ) {
			case STATE_WAIT_LOGO:
				if ( accTime >= TIME_WAIT_LOGO )
					setState( STATE_LOGO_ENTERS );
			break;

			case STATE_LOGO_ENTERS:
				final int fp_progress = NanoMath.divInt( accTime, TIME_LOGO_ENTERS );
				for ( byte i = 0; i < logoCurves.length; ++i ) {
					final Point p = new Point();
					logoCurves[ i ].getPointAtFixed( p, fp_progress );
					logoParts[ i ].setPosition( p );
				}

				if ( fp_progress >= NanoMath.ONE ) {
					setState( STATE_SHINE );
				}
			break;

			case STATE_ANIMATE:
				if ( accTime >= TIME_WAIT_LOGO )
					setState( STATE_PRESS_KEY );
			break;

			case STATE_PRESS_KEY:
//				if (visibleTime > 0) {
//					visibleTime -= delta;
//
//					if (visibleTime <= 0) {
//						// mostra o texto "Pressione qualquer tecla"
//						GameMIDlet.setSoftKeyLabel(ScreenManager.SOFT_KEY_MID, TEXT_PRESS_ANY_KEY, 0);
//					}
//				}
			break;
		}
	}


	private final void updateCamera( int delta ) {
		cameraAccTime += delta;
		final Point p = new Point();
		cameraTweener.getPointAtFixed( p, NanoMath.divInt( cameraAccTime, cameraTotalTime ) );
		setScroll( p.x, p.y );

		if ( cameraAccTime >= cameraTotalTime ) {
			cameraAccTime = 0;
			setCameraLoop();
		}
	}


	private final void setScroll( int x, int y ) {
		cameraPosition.set( NanoMath.clamp( x, cameraLimit.x, 0 ), NanoMath.clamp( y, cameraLimit.y, 0 ) );
		bkg.setPosition( cameraPosition );
		bkg1.setPosition( cameraPosition );
	}


	public final void keyPressed(int key) {
		//#if DEBUG == "true"
			// permite pular a tela de splash
			if ( true ) {
				GameMIDlet.setScreen(SCREEN_MAIN_MENU);
			}
		//#endif

		if ( state == STATE_PRESS_KEY && accTime >= TIME_WAIT_LOGO ) {
			GameMIDlet.setScreen(SCREEN_MAIN_MENU);
		}
	}
	

	public final void keyReleased( int key ) {
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( id ) {
			case -1:
				shineRight.setSequence( 0 );
				shineLeft.setSequence( 0 );
				shineRight.setVisible( false );
				shineLeft.setVisible( false );

				setState( STATE_ANIMATE );
			break;
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
		if ( id >= 0 && state >= STATE_ANIMATE ) {
			final int arrayIndex = id >> 1;
			switch ( id ) {
				case 0:
				case 2:
					switch ( shineBack[ arrayIndex ].getFrameSequenceIndex() ) {
						case 0:
						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
							shineBack[ arrayIndex ].setVisible( false );
						break;

						case 6:
							if ( id == 0 && shineBack[ 1 ].getSequenceIndex() == SEQUENCE_STOPPED ) {
								shineBack[ 1 ].setSequence( SEQUENCE_FRONT );
								shineFront[ 1 ].setVisible( true );
								shineFront[ 1 ].setSequence( SEQUENCE_BACK );
							}
						default:
							shineBack[ arrayIndex ].setVisible( true );
					}
				break;

				case 1:
				case 3:
					switch ( shineFront[ arrayIndex ].getFrameSequenceIndex() ) {
						case 7:
						case 8:
						case 9:
							shineFront[ arrayIndex ].setVisible( false );
						break;

						default:
							shineFront[ arrayIndex ].setVisible( true );
					}
				break;
			}
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
		if(!MediaPlayer.isPlaying())
			MediaPlayer.play( SOUND_SPLASH, MediaPlayer.LOOP_INFINITE );
	}


	//#if SCREEN_SIZE != "SMALL"
		public final void onPointerDragged( int x, int y ) {
		}


		public final void onPointerPressed( int x, int y ) {
			keyPressed( 0 );
		}


		public final void onPointerReleased( int x, int y ) {
		}
	//#endif

}
