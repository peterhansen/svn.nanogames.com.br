/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class Stage extends UpdatableGroup implements Constants, Updatable {
	
	private static final byte BACKGROUND_MAIN_COLOR					= 0;
	private static final byte BACKGROUND_BALL_COLOR					= 1;
	private static final byte BACKGROUND_HORIZONTAL_LINE_BOTTOM		= 2;
	private static final byte BACKGROUND_HORIZONTAL_LINE			= 3;
	private static final byte BACKGROUND_SPOT_LIGHT_COLOR			= 4;
	private static final byte BACKGROUND_SPOT_COLOR					= 5;
	private static final byte BACKGROUND_STAIR_HORIZONTAL_LINES1	= 6;
	private static final byte BACKGROUND_STAIR_HORIZONTAL_LINES2	= 7;
	private static final byte BACKGROUND_STAIR_HORIZONTAL_LINES3	= 8;
	private static final byte BACKGROUND_STAIR_HORIZONTAL_LINES4	= 9;
	private static final byte BACKGROUND_STAIR_HORIZONTAL_LINES5	= 10;
	private static final byte BACKGROUND_STAIR_LED_BORDER			= 11;
	private static final byte BACKGROUND_STAIR_LED_REFLEX_1			= 12;
	private static final byte BACKGROUND_STAIR_LED_REFLEX_2			= 13;
	private static final byte BACKGROUND_STAIR_LED_REFLEX_3			= 14;
	private static final byte BACKGROUND_SCREEN_CENTER				= 15;
	private static final byte BACKGROUND_SCREEN_CENTRAL_BORDER		= 16;
	private static final byte BACKGROUND_SCREEN_EXTERNAL_BORDER		= 17;
	private static final byte BACKGROUND_SCREEN_MAIN_COLOR			= 18;

	private static final int BACKGROUND_NULL_COLOR					= 0x000000;
	
	/* { mainColor, ballContour, LinhaHorizontalBottom, LinhaHorizontal, SpotLightColor, SpotTop,
	 /*stairs*/ // lh1, main, lh2, lh3, lh4, ledborder, ledReflex1, ledReflex2, ledReflex3,
	 /*screen*/ // centro, bordaCentral, bordaExterna, Main };
	private static final int[] bkgBlue = { 0x08198f, 0x000f72, 0x0d1ba3, 0x0a1272, 0x101c6a,  0x0b144b ,
	 0x446873 , 0x749193, 0xf7ffff, 0xa8c5c5, 0x377147, 0x0000ff, 0xb6d9ff, 0x9abff7, 0x88a4de,
	 0xeafcff, 0x768aff, 0x0e3daf, 0x000053};
	private static final int[] bkgRed = { 0xde4d4c, 0x9f1311, 0xa42423, 0x9f1311, 0x98392a,  0x6a2116 ,
	 0xb57c77 , 0xc99089, 0xf0b9b3, 0xd59a93, 0x9f6959, 0xf95e42, 0xfe9784, 0xfe9784, 0xe5aba3,
	 0xffeaf3, 0xffb0a3, 0xff5153, 0x6d1d00 };
	private static final int[] bkgAmbar = { 0xcc9647, 0xb3540b, 0x70321b, 0xb3540b, 0x8a5322,  0x693f1a ,
	 0xc29481 , 0xd9af8a, 0xf8dcd3, 0xe0b0a1, 0xb17d5c, 0xff914e, 0xfe9784, 0xfe9784, 0xd9aca8,
	 0xffeaf3, 0xffb276, 0xfc472a, 0xffd0a7 };
	
	/*private static final int[] spotsBlue = { 0x0b144b, 0x333ca1, 0x101c6a  };
	private static final int[] spotsRed = { 0x702612, 0xc03f34, 0x9d3725 };
	private static final int[] spotsAmbar = { 0x693f1a, 0xab672a, 0x8a5322 };*/

	private int[] actualColors;
	private int bkgAccColorTime;
	private int bkgLastStepAccColorTime;
	private byte bkgColor;
	private byte bkgLastColor;
	private boolean bkgColorChanged;

	private static final short BACKGROUND_COLOR_CHANGE_TIME			= 1500;
	private final byte BACKGROUND_COLOR_CHANGE_STEPS;
	private final short BACKGROUND_COLOR_CHANGE_STEP_TIME;

	public static final byte BACKGROUND_COLOR_BLUE			= 0;
	public static final byte BACKGROUND_COLOR_RED			= 1;
	public static final byte BACKGROUND_COLOR_AMBAR			= 2;

	/** Tempo de troca da animação das telas no fundo do cenário. */
	private static final int TIME_SCREEN_ANIMATION_CHANGE = 10000;
	/***/
	private int accScreenTime;

	private final Pattern floor;
	private final Pattern bkg_balls;
	private final Pattern bkg_spots;
	private final Pattern floorTransition;
	private final Pattern stairReflex;
	private final Pattern stairLED;
	private final UpdatableGroup bkgBase;
	private final BasicAnimatedPattern[] bkgScreens;
	private final Pattern bkgPattern;
	private final BasicAnimatedPattern[] screen;

	private final short LED_SCREEN_SIZE_X;
	private final short LED_SCREEN_SIZE_Y;
	
	public Stage() throws Exception {
		super(7);

		// aparelhos de pouca memória normalmente são lentos também, então é melhor usar menos passos
		BACKGROUND_COLOR_CHANGE_STEPS = ( byte ) ( GameMIDlet.isLowMemory() ? 2 : 4 );
		BACKGROUND_COLOR_CHANGE_STEP_TIME	= ( short ) ( BACKGROUND_COLOR_CHANGE_TIME / BACKGROUND_COLOR_CHANGE_STEPS );

		stairLED = new Pattern( new DrawableImage( PATH_IMAGES + "stage_stairs_leds.png" ) );
		stairLED.getFill().setSize( stairLED.getFill().getWidth(), STAGE_STAIR_HEIGHT );
		insertDrawable( stairLED );
		stairLED.setSize( STAGE_WIDTH, ( STAGE_STAIRS_TOTAL_HEIGHT - (STAGE_STAIR_HEIGHT-STAGE_STAIR_REAL_HEIGHT) ) );
		stairLED.setPosition( 0, STAGE_STAIR_BEGIN + (STAGE_STAIR_HEIGHT-STAGE_STAIR_REAL_HEIGHT) );
		GameMIDlet.log( "stage_stairs_leds" );

		bkg_spots = new Pattern( new DrawableImage( PATH_IMAGES + "stage_bkg_spot.png" ) );
		bkg_spots.getFill().setSize( bkg_spots.getFill().getWidth() << 2, bkg_spots.getFill().getHeight() );
		insertDrawable( bkg_spots );
		bkg_spots.setSize( STAGE_WIDTH, (bkg_spots.getFill().getHeight()) );
		bkg_spots.setPosition( 0, (STAGE_TOP_SPOTS_HEIGHT - (bkg_spots.getFill().getHeight())) >> 1 );
		GameMIDlet.log( "stage_bkg_spot" );

		floorTransition = new Pattern( new DrawableImage( PATH_IMAGES + "stage_floor.png" ) );
		insertDrawable( floorTransition );
		floorTransition.setSize( STAGE_WIDTH, floorTransition.getFill().getHeight() );
		floorTransition.setPosition( 0, STAGE_FLOOR_BEGIN );
		GameMIDlet.log( "stage_floor" );

		floor = new Pattern( 0x2E2E2E );
		insertDrawable( floor );
		floor.setSize( STAGE_WIDTH, Math.max( 1, STAGE_HEIGHT - floorTransition.getFill().getHeight() ) );
		floor.setPosition( 0, floorTransition.getPosY() + floorTransition.getHeight() );

		stairReflex = new Pattern( new DrawableImage( PATH_IMAGES + "stage_stairs_reflex.png" ) );
		stairReflex.getFill().setSize( stairReflex.getFill().getWidth(), STAGE_STAIR_HEIGHT );
		insertDrawable( stairReflex );
		stairReflex.setSize( STAGE_WIDTH, ( STAGE_STAIRS_TOTAL_HEIGHT - STAGE_STAIR_HEIGHT ) );
		stairReflex.setPosition( 0, STAGE_STAIR_BEGIN + STAGE_STAIR_HEIGHT );
		GameMIDlet.log( "stage_stairs_reflex" );

		if( GameMIDlet.isLowMemory()) {
			screen = null;
			bkg_balls = null;
			bkgBase = null;
			bkgScreens = null;
			bkgPattern = null;
			LED_SCREEN_SIZE_X = 1;
			LED_SCREEN_SIZE_Y = 1;
		}
		else {			
			bkg_balls = new Pattern( new DrawableImage( PATH_IMAGES + "stage_bkg_balls.png" ) );
			insertDrawable( bkg_balls );
			bkg_balls.setSize( STAGE_WIDTH, STAGE_BKG_HEIGHT - ( STAGE_BKG_HORIZONTAL_LINE_HEIGHT - bkg_balls.getFill().getHeight() ) );
			bkg_balls.setPosition( 0, STAGE_BKG_BEGIN + ( ( STAGE_BKG_HORIZONTAL_LINE_HEIGHT - bkg_balls.getFill().getHeight() ) >> 1 ) + STAGE_BKG_HORIZONTAL_LINE_DENSITY );
			bkg_balls.getFill().setSize( bkg_balls.getFill().getWidth() , STAGE_BKG_HORIZONTAL_LINE_HEIGHT );
			GameMIDlet.log( "stage_bkg_balls" );

			// Início da construção das telas de LED
			bkgBase = new UpdatableGroup( NUMBER_OF_SCREENS << 1 );
			bkgScreens = new BasicAnimatedPattern[ NUMBER_OF_SCREENS ];
		
			final DrawableImage screenFront = new DrawableImage( PATH_IMAGES + "screen.png" );
			final Drawable screenPattern = new DrawableImage( PATH_IMAGES + "screen_0.png" );
			LED_SCREEN_SIZE_X = (short) screenFront.getWidth();
			LED_SCREEN_SIZE_Y = (short) screenFront.getHeight();
			screen = new BasicAnimatedPattern[NUMBER_OF_SCREENS];
			for ( byte i = 0; i < NUMBER_OF_SCREENS; ++i ) {
				screen[i] = new BasicAnimatedPattern( screenPattern, SCREEN_ANIMATION_MAX_SPEED, SCREEN_ANIMATION_MAX_SPEED );
				bkgScreens[ i ] = screen[i];
				screen[i].setSize( LED_SCREEN_SIZE_X, LED_SCREEN_SIZE_Y );
				bkgBase.insertDrawable( screen[i] );

				final DrawableImage front = new DrawableImage( screenFront );
				front.setClipTest( false );
				bkgBase.insertDrawable( front );

				final Point pos = new Point(( i & 1 ) == 0 ? 1 : (screenFront.getWidth() + BKG_SCREEN_SPACING), ( i * BKG_SCREEN_SPACING ));
				if( ( ( (pos.x + pos.y) & 1) == (ABSOLUTE_POSITION_INTERSECTION ? 0 : 1) ) ) pos.x++;

				front.setPosition( pos );
				screen[i].setPosition( pos );
			}
			bkgBase.setSize( ( screenFront.getWidth() + BKG_SCREEN_SPACING ) << 1, (BKG_SCREEN_SPACING*(NUMBER_OF_SCREENS-1))+screenFront.getHeight() );

			bkgPattern = new Pattern( bkgBase );
			bkgPattern.setSize( STAGE_WIDTH, bkgPattern.getFill().getHeight() );
			bkgPattern.setPosition( 0, BKG_SCREEN_Y );
			insertDrawable( bkgPattern );
			GameMIDlet.log( "bkgBase" );
			/* Finalização da construção das telas de LED */
		}
		
		bkgLastColor = BACKGROUND_COLOR_AMBAR;
		bkgColor = BACKGROUND_COLOR_AMBAR;
		actualColors = new int[bkgAmbar.length];
		for( int i = 0; i < bkgAmbar.length; ++i )
			actualColors[i] = bkgAmbar[i];
		updateImages();
		bkgColorChanged = true;
		bkgColorChanged = true;

		setSize(STAGE_WIDTH, STAGE_HEIGHT);
	}
	
	public void setSize ( int width, int height ) {
		super.setSize( NanoMath.max ( STAGE_WIDTH, width ), NanoMath.max ( STAGE_HEIGHT, height ) );

		bkg_spots.setSize( getWidth(), ( bkg_spots.getFill().getHeight() ) );
		stairLED.setSize( getWidth(), ( STAGE_STAIRS_TOTAL_HEIGHT - (STAGE_STAIR_HEIGHT-STAGE_STAIR_REAL_HEIGHT) ) );

		stairReflex.setSize( getWidth(), ( STAGE_STAIRS_TOTAL_HEIGHT - STAGE_STAIR_HEIGHT ) );

		if(bkg_balls != null) // se não existe não faz a atualização de tamanho
			bkg_balls.setSize( getWidth(), STAGE_BKG_HEIGHT - ( STAGE_BKG_HORIZONTAL_LINE_HEIGHT - bkg_balls.getFill().getHeight() ) );

		floorTransition.setSize( getWidth(), floorTransition.getFill().getHeight() );
		floor.setSize( getWidth() , Math.max( 1, height - STAGE_HEIGHT ) );

		if(bkgPattern != null)  {
			bkgPattern.setSize( STAGE_WIDTH, bkgPattern.getFill().getHeight() );
			bkgPattern.setPosition( 0, BKG_SCREEN_Y );
		}
	}

	public final void changeBackgroundColor ( byte color ) {
		if(bkgColor != color) {
			bkgLastColor = bkgColor;
			bkgColor = color;
			bkgColorChanged = false;
			bkgAccColorTime = 0;
			bkgLastStepAccColorTime = 0;
		}
	}

	private final int[] bkgColorType ( byte type ) {
		switch ( type ) {
			case BACKGROUND_COLOR_BLUE: return bkgBlue;
			case BACKGROUND_COLOR_RED: return bkgRed;
			default: return bkgAmbar;
		}
	}

	public final boolean colorIsBlue() {
		return ( ( bkgColor == BACKGROUND_COLOR_BLUE ) && ( bkgLastColor == BACKGROUND_COLOR_BLUE ) );
	}

	public final int lerpColor ( int initColor, int finalColor, int percentage ) {
		final int r = NanoMath.lerpInt( ( initColor & 0xFF0000 ) >> 16, ( finalColor & 0xFF0000 ) >> 16, percentage );
		final int g = NanoMath.lerpInt( ( initColor & 0x00FF00 ) >>  8, ( finalColor & 0x00FF00 ) >>  8, percentage );
		final int b = NanoMath.lerpInt( initColor & 0x0000FF, finalColor & 0x0000FF, percentage );
		return ( r << 16 ) | ( g << 8 ) | b;
	}

	public static final byte BACKGROUNG_BKG_COLORS = 6;
	
	private final void updateImages() {
		final PaletteMap[] map = new PaletteMap[ bkgBlue.length ];
		for(byte i = 0; i < map.length; ++i )
			map[i] = new PaletteMap( bkgBlue[i], actualColors[i] );

		try { /* Troca de paleta de todas as imagens */
			final PaletteChanger pSpots = new PaletteChanger( PATH_IMAGES + "stage_bkg_spot.png" );
			bkg_spots.setFill( new DrawableImage( pSpots.createImage(map)) );
			bkg_spots.getFill().setSize( bkg_spots.getFill().getWidth() << 2, bkg_spots.getFill().getHeight() );

			if(bkg_balls != null) {
				final PaletteChanger pBalls = new PaletteChanger( PATH_IMAGES + "stage_bkg_balls.png" );
				bkg_balls.setFill( new DrawableImage( pBalls.createImage(map)) );
				bkg_balls.getFill().setSize( bkg_balls.getFill().getWidth() , STAGE_BKG_HORIZONTAL_LINE_HEIGHT );
			}

			if(stairReflex != null) {
				final PaletteChanger pStairReflex =  new PaletteChanger( PATH_IMAGES + "stage_stairs_reflex.png" );
				stairReflex.setFill( new DrawableImage( pStairReflex.createImage(map)) );
				stairReflex.getFill().setSize( stairReflex.getFill().getWidth(), STAGE_STAIR_HEIGHT );
			}

			final PaletteChanger pStairLED =  new PaletteChanger( PATH_IMAGES + "stage_stairs_leds.png" );
			stairLED.setFill( new DrawableImage( pStairLED.createImage(map)) );
			stairLED.getFill().setSize( stairLED.getFill().getWidth(), STAGE_STAIR_HEIGHT );

			if(screen != null) {
				final PaletteChanger pScreen =  new PaletteChanger( PATH_IMAGES + "screen_0.png" );
				final DrawableImage tempScreen = new DrawableImage( pScreen.createImage(map));
				for ( byte i = 0; i < screen.length; ++i ) {
					screen[i].setFill( tempScreen );
					screen[i].setSize( LED_SCREEN_SIZE_X, LED_SCREEN_SIZE_Y );
				}
			}
		}
		catch (Exception e) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}
	}

	public final boolean animationEnded() {
		return bkgColorChanged;
	}

	public final void update ( int delta ) {
		super.update(delta);
		
		if(!bkgColorChanged)
		{
			bkgAccColorTime += delta;

			if(bkgAccColorTime >= BACKGROUND_COLOR_CHANGE_TIME) {
				final int[] color = bkgColorType(bkgColor);
				for(byte i = 0; i < bkgBlue.length; ++i )
					actualColors[i] = color[i];
				bkgColorChanged = true;
				updateImages();
				bkgLastColor = bkgColor;
			} else {
				final int[] initialColor = bkgColorType(bkgLastColor);
				final int[] finalColor = bkgColorType(bkgColor);
				for(byte i = 0; i < actualColors.length; ++i )
					actualColors[i] = lerpColor( initialColor[i], finalColor[i], 100*bkgAccColorTime/BACKGROUND_COLOR_CHANGE_TIME);

				if( bkgLastStepAccColorTime + BACKGROUND_COLOR_CHANGE_STEP_TIME <= bkgAccColorTime ) {
					bkgLastStepAccColorTime += BACKGROUND_COLOR_CHANGE_STEP_TIME;
					updateImages();
				}
			}
		}
		if( bkgScreens != null ) {
			accScreenTime += delta;
			if ( accScreenTime >= TIME_SCREEN_ANIMATION_CHANGE ) {
				accScreenTime %= TIME_SCREEN_ANIMATION_CHANGE;
				for ( byte i = 0 ; i < bkgScreens.length; ++i ) {
					bkgScreens[ i ].setSpeed( SCREEN_ANIMATION_MAX_SPEED + NanoMath.randInt( SCREEN_ANIMATION_MAX_SPEED ),
											  SCREEN_ANIMATION_MAX_SPEED + NanoMath.randInt( SCREEN_ANIMATION_MAX_SPEED ) );
					do {
						bkgScreens[ i ].setAnimation( BasicAnimatedPattern.ANIMATION_RANDOM );
					} while ( bkgScreens[ i ].getAnimation() == BasicAnimatedPattern.ANIMATION_NONE );
				}
			}
		}
	}

    public final void paint(Graphics g) {
		g.setColor( BACKGROUND_NULL_COLOR );
		g.fillRect(translate.x, translate.y, STAGE_WIDTH, STAGE_TOP_SPOTS_HEIGHT);

		g.setColor( actualColors[BACKGROUND_MAIN_COLOR] );
		g.fillRect(translate.x, translate.y + STAGE_BKG_BEGIN, STAGE_WIDTH, STAGE_BKG_HEIGHT);

		for( int i = 0; i < STAGE_BKG_HORIZONTAL_LINES; ++i ) {
			final int y = translate.y + STAGE_BKG_BEGIN + i*STAGE_BKG_HORIZONTAL_LINE_HEIGHT;
			g.setColor( actualColors[BACKGROUND_HORIZONTAL_LINE] );
			g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_BKG_HORIZONTAL_LINE_DENSITY  );
			g.setColor( actualColors[BACKGROUND_HORIZONTAL_LINE_BOTTOM] );
			g.fillRect( translate.x, y + STAGE_BKG_HORIZONTAL_LINE_DENSITY, STAGE_WIDTH , 1 );
		}

		int y = translate.y + STAGE_STAIR_BEGIN;
		g.setColor( actualColors[BACKGROUND_STAIR_HORIZONTAL_LINES1] );
		g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_STAIR_HORIZONTAL_LINES_HEIGHT1 );
		for( int i = 0; i < STAGE_STAIRS; ++i ) {
			y = translate.y + STAGE_STAIR_BEGIN + (i*STAGE_STAIR_HEIGHT) + STAGE_STAIR_HORIZONTAL_LINES_HEIGHT1;

			g.setColor( actualColors[BACKGROUND_STAIR_HORIZONTAL_LINES2] );
			g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_STAIR_HORIZONTAL_LINES_HEIGHT2  );
			y += STAGE_STAIR_HORIZONTAL_LINES_HEIGHT2;

			g.setColor( actualColors[BACKGROUND_STAIR_HORIZONTAL_LINES3] );
			g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_STAIR_HORIZONTAL_LINES_HEIGHT3  );
			y += STAGE_STAIR_HORIZONTAL_LINES_HEIGHT3;

			g.setColor( actualColors[BACKGROUND_STAIR_HORIZONTAL_LINES4] );
			g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_STAIR_HORIZONTAL_LINES_HEIGHT4  );
			y += STAGE_STAIR_HORIZONTAL_LINES_HEIGHT4;

			g.setColor( actualColors[BACKGROUND_STAIR_HORIZONTAL_LINES5] );
			g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_STAIR_HORIZONTAL_LINES_HEIGHT5  );
			y += STAGE_STAIR_HORIZONTAL_LINES_HEIGHT5;
			
			/*g.setColor( BACKGROUND_NULL_COLOR );
			g.fillRect( translate.x, y, STAGE_WIDTH, STAGE_STAIR_REAL_HEIGHT  );*/
		}

		super.paint(g);
	}
}
