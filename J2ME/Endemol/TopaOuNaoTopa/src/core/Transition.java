/**
 * Transition.java
 *
 * Created on Sep 20, 2010 1:11:25 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Rectangle;

/**
 *
 * @author peter
 */
public final class Transition extends Pattern implements Constants {

	private final MUV muv = new MUV();

	private static final short TIME_TRANSITION = 666; // demôin! >:D // Q medo!!!!

	private final Rectangle previousViewport = new Rectangle( 0, 0, Short.MAX_VALUE, Short.MAX_VALUE );
	private final Rectangle nextViewport = new Rectangle( 0, 0, Short.MAX_VALUE, Short.MAX_VALUE );

	private Rectangle previousOldViewport;
	private Rectangle nextOldViewport;

	private Drawable previous;
	private Drawable next;

	private final SpriteListener listener;


	public Transition( SpriteListener listener ) throws Exception {
		super( new DrawableImage( PATH_IMAGES + "transition.png" ) );

		this.listener = listener;

		setSize( Short.MAX_VALUE, fill.getHeight() );
		defineReferencePixel( ANCHOR_LEFT | ANCHOR_HCENTER );

		setVisible( false );
	}


	public final void update( int delta ) {
		if ( visible ) {
			super.update( delta );

			move( 0, muv.updateInt( delta ) );
			if ( getPosY() >= ScreenManager.SCREEN_HEIGHT ) {
				endTransition();
			} else {
				nextViewport.height = previousViewport.y = getRefPixelY();
			}
		}
	}

	private final void endTransition() {
		setVisible( false );

		if ( previous != null )
			previous.setViewport( previousOldViewport );
		next.setViewport( nextOldViewport );

		previous = null;
		next = null;

		listener.onSequenceEnded( LISTENER_ID_TRANSITION, 0 );
	}


	public final void doTransition( Drawable previous, Drawable next ) {
		if(isVisible()) endTransition();

		setPosition( 0, -getHeight() );

		muv.setSpeed( ScreenManager.SCREEN_HEIGHT * 1000 / TIME_TRANSITION );

		this.previous = previous;
		this.next = next;

		nextOldViewport = next.getViewport();

		if ( previous != null ) {
			previousOldViewport = previous.getViewport();
			previous.setViewport( previousViewport );
		}

		next.setViewport( nextViewport );

		setVisible( true );
		update( 0 );
	}
}
