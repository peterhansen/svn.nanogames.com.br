/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 * @author caiolima
 */
public class SparkEmitter extends ParticleEmitter {
	private final int BORN_MAX_Y_DIFF;
	private final int BORN_MAX_HALF_Y_DIFF;

    private final byte ANGLE_MIN;
    private final byte ANGLE_DIFF;
    private final int SPEED_MIN;
    private final int SPEED_DIFF;
    private final int LIFE_MIN;
    private final int LIFE_DIFF;
    private final Point emissivePoint;
    private final Sprite sprite;

	public SparkEmitter(Point emissivePoint, int sparksPerSecond, int minAngle, int maxAngle, int minSpeed, int speedRange, int dispersion, int minLife, int diffLife, int maxParticles) throws Exception {
        super(sparksPerSecond, true, new Sprite( PATH_IMAGES + "particle_spark" ), ScreenManager.SCREEN_WIDTH/2, ScreenManager.SCREEN_HEIGHT, 10, (short)maxParticles);

        this.emissivePoint = emissivePoint;

        ANGLE_MIN = (byte)minAngle;
        ANGLE_DIFF = (byte)(maxAngle - minAngle);
        SPEED_MIN = minSpeed;
        SPEED_DIFF = speedRange;
		LIFE_MIN = NanoMath.divInt( minLife, 1000 );
		LIFE_DIFF = NanoMath.divInt( diffLife, 1000 );

        sprite = new Sprite(PATH_IMAGES + "chuvadeprata");
        sprite.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
        sprite.setRefPixelPosition( getWidth() >> 1, getHeight() );
        insertDrawable(sprite);

        BORN_MAX_Y_DIFF = dispersion;
        BORN_MAX_HALF_Y_DIFF = (dispersion >> 1);
	}

	public final void setEmissivePosition(int x, int y) {
		emissivePoint.set(x,y);
        sprite.setRefPixelPosition( x , y );
	}

	public void turnOff() {
		super.turnOff();
		sprite.setVisible(false);
	}
	public void turnOn() {
		super.turnOn();
		sprite.setVisible(true);
	}

    public final Point RandSpeed() {
        final int speed = (NanoMath.randFixed(SPEED_DIFF) + SPEED_MIN);
        final int angle = (NanoMath.randInt(ANGLE_DIFF) + ANGLE_MIN);
        return new Point(NanoMath.mulFixed(NanoMath.cosInt(angle), speed) , -NanoMath.mulFixed(NanoMath.sinInt(angle), speed));
    }

    public final Point RandPosition() {
        return emissivePoint.add(0, - BORN_MAX_HALF_Y_DIFF + NanoMath.randInt( BORN_MAX_Y_DIFF ));
    }

	public int RandLifeTime() {
        return ( NanoMath.randInt( LIFE_DIFF ) + LIFE_MIN );
	}
    
}
