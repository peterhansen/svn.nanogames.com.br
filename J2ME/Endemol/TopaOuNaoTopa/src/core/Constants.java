/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public interface Constants {

	public static final String APP_SHORT_NAME = "TONT";

	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";

	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	public static final String PATH_ONLINE = PATH_IMAGES + "online/";
	public static final String PATH_BORDER = PATH_IMAGES + "border/";
	public static final String PATH_BANKER = PATH_IMAGES + "banker/";
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";

	/** Tipo de ranking do jogo: total de dinheiro ganho. */
	public static final byte RANKING_INDEX_TOTAL_MONEY		= 0;
	/** Tipo de ranking do jogo: média de premiação. */
	public static final byte RANKING_INDEX_MONEY_AVERAGE	= 1;
	/** Tipo de ranking do jogo: número de premiações máximas. */
	public static final byte RANKING_INDEX_MILLION			= 2;
	/** Tipo de ranking do jogo: número de premiações mínimas. */
	public static final byte RANKING_INDEX_50_CENTS			= 3;
	
	/**Índice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	public static final byte DATABASE_SLOT_LANGUAGE = 2;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 2;

	/** Id da categoria de notícias do Topa ou Não Topa, no Nano Online. */
	public static final byte NEWS_CATEGORY_ID = 6;
	
	/** Offset da fonte padrão. */
	public static final byte DEFAULT_FONT_OFFSET = -1;
	
	/**Índice da fonte padrão do jogo. */
	public static final byte FONT_INDEX_DEFAULT	= 0;
	
	/**Índice da fonte utilizada para textos. */
	public static final byte FONT_TITLE			= 0;
	public static final byte FONT_TEXT			= 1;
	public static final byte FONT_CASE_SMALL	= 2;
	public static final byte FONT_CASE_VALUE	= 3;
	public static final byte FONT_CASE_NUMBER	= 4;
	public static final byte FONT_NUMBER_BLACK	= 5;
	public static final byte FONT_NUMBER_WHITE	= 6;
	public static final byte FONT_BANKER_VALUE	= 7;
	public static final byte FONT_TEXT_WHITE	= 8;
	public static final byte FONT_MILLION		= 9;
	
	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 10;

	/** Unidade de dinheiro do jogo (1 real). */
	public static final byte REAL = 100;
	
	//índices dos sons do jogo
	public static final byte SOUND_OPEN_CASE				= 0;
	public static final byte SOUND_CAMERA_EFFECT			= 1;
	public static final byte SOUND_CASE_GOOD				= 2;
	public static final byte SOUND_CASE_BAD					= 3;
	public static final byte SOUND_CASE_CHOOSE				= 4;
	public static final byte SOUND_BANKER					= 5;
	public static final byte SOUND_SUSPENSE					= 6;
	public static final byte SOUND_NO_DEAL					= 7;
	public static final byte SOUND_DEAL						= 8;
	public static final byte SOUND_FLIP						= 9;
	public static final byte SOUND_SPLASH					= 10;
	public static final byte SOUND_PHONE					= 11;
	
	public static final byte SOUND_TOTAL = SOUND_PHONE + 1;
	
	/** Duração da vibração ao atingir a trave, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 300;

//#if SCREEN_SIZE == "SMALL"
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#elif SCREEN_SIZE == "BIG"
		public static final int LOW_MEMORY_LIMIT = 1600000;
    //#else
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif

	public static final byte LISTENER_ID_BIG_CASE	= 0;
	public static final byte LISTENER_ID_TRANSITION	= 1;
	public static final byte LISTENER_ID_VALUES_BOX	= 2;
	public static final byte LISTENER_ID_BANKER		= 3;

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS TEXTOS">
	public static final int TEXT_OK								     	= 0;
	public static final int TEXT_BACK									= TEXT_OK + 1;
	public static final int TEXT_NEW_GAME								= TEXT_BACK + 1;
	public static final int TEXT_OPTIONS								= TEXT_NEW_GAME + 1;
	public static final int TEXT_NANO_ONLINE							= TEXT_OPTIONS + 1;
	public static final int TEXT_HELP									= TEXT_NANO_ONLINE + 1;
	public static final int TEXT_CREDITS								= TEXT_HELP + 1;
	public static final int TEXT_EXIT									= TEXT_CREDITS + 1;
	public static final int TEXT_PAUSE									= TEXT_EXIT + 1;
	public static final int TEXT_CREDITS_TEXT							= TEXT_PAUSE + 1;
	public static final int TEXT_CONTROLS								= TEXT_CREDITS_TEXT + 1;
	public static final int TEXT_RULES								    = TEXT_CONTROLS + 1;
	public static final int TEXT_HELP_PROFILE							= TEXT_RULES + 1;
	public static final int TEXT_HELP_CONTROLS							= TEXT_HELP_PROFILE + 1;
	public static final int TEXT_HELP_OBJECTIVES						= TEXT_HELP_CONTROLS + 1;
	public static final int TEXT_HELP_PROFILE_TEXT						= TEXT_HELP_OBJECTIVES + 1;
	public static final int TEXT_TURN_SOUND_ON							= TEXT_HELP_PROFILE_TEXT + 1;
	public static final int TEXT_TURN_SOUND_OFF							= TEXT_TURN_SOUND_ON + 1;
	public static final int TEXT_TURN_VIBRATION_ON						= TEXT_TURN_SOUND_OFF + 1;
	public static final int TEXT_TURN_VIBRATION_OFF						= TEXT_TURN_VIBRATION_ON + 1;
	public static final int TEXT_CANCEL									= TEXT_TURN_VIBRATION_OFF + 1;
	public static final int TEXT_CONTINUE								= TEXT_CANCEL + 1;
	public static final int TEXT_SPLASH_NANO							= TEXT_CONTINUE + 1;
	public static final int TEXT_SPLASH_ENDEMOL							= TEXT_SPLASH_NANO + 1;
	public static final int TEXT_LOADING								= TEXT_SPLASH_ENDEMOL + 1;
	public static final int TEXT_DO_YOU_WANT_SOUND						= TEXT_LOADING + 1;
	public static final int TEXT_YES									= TEXT_DO_YOU_WANT_SOUND + 1;
	public static final int TEXT_NO										= TEXT_YES + 1;
	public static final int TEXT_BACK_MENU								= TEXT_NO + 1;
	public static final int TEXT_CONFIRM_BACK_MENU						= TEXT_BACK_MENU + 1;
	public static final int TEXT_EXIT_GAME								= TEXT_CONFIRM_BACK_MENU + 1;
	public static final int TEXT_CONFIRM_EXIT_1							= TEXT_EXIT_GAME + 1;
	public static final int TEXT_PRESS_ANY_KEY							= TEXT_CONFIRM_EXIT_1 + 1;
	public static final int TEXT_GAME_OVER								= TEXT_PRESS_ANY_KEY + 1;
	public static final int TEXT_RECOMMEND_TITLE						= TEXT_GAME_OVER + 1;
	public static final int TEXT_RECOMMEND_TEXT							= TEXT_RECOMMEND_TITLE + 1;
	public static final int TEXT_DEAL									= TEXT_RECOMMEND_TEXT + 1;
	public static final int TEXT_NO_DEAL								= TEXT_DEAL + 1;
	public static final int TEXT_CHOOSE_LANGUAGE						= TEXT_NO_DEAL + 1;
	public static final int TEXT_ENGLISH								= TEXT_CHOOSE_LANGUAGE + 1;
	public static final int TEXT_PORTUGUESE								= TEXT_ENGLISH + 1;
	public static final int TEXT_CURRENT_PROFILE						= TEXT_PORTUGUESE + 1;
    public static final int TEXT_CHOOSE_ANOTHER						    = TEXT_CURRENT_PROFILE + 1;
    public static final int TEXT_CONFIRM						        = TEXT_CHOOSE_ANOTHER + 1;
    public static final int TEXT_NO_PROFILE								= TEXT_CONFIRM + 1;
	public static final int TEXT_TIME_FORMAT							= TEXT_NO_PROFILE + 1;
	public static final int TEXT_HIGH_SCORES					        = TEXT_TIME_FORMAT + 1;
	public static final int TEXT_VOLUME							        = TEXT_HIGH_SCORES + 1;
	public static final int TEXT_CONNECTING						        = TEXT_VOLUME + 1;
	public static final int TEXT_VERSION						        = TEXT_CONNECTING + 1;
	public static final int TEXT_NO_NEWS_FOUND					        = TEXT_VERSION + 1;
	public static final int TEXT_HELP_NEWS						        = TEXT_NO_NEWS_FOUND + 1;
	public static final int TEXT_UPDATE						            = TEXT_HELP_NEWS + 1;
	public static final int TEXT_NEWS						            = TEXT_UPDATE + 1;
	public static final int TEXT_CURRENCY						        = TEXT_NEWS + 1;
	public static final int TEXT_CURRENCY_NAME						    = TEXT_CURRENCY + 1;
	public static final int TEXT_CURRENCY_NAME_SINGULAR				    = TEXT_CURRENCY_NAME + 1;
	public static final int TEXT_CURRENCY_CENTS						    = TEXT_CURRENCY_NAME_SINGULAR + 1;
	public static final int TEXT_MILLION								= TEXT_CURRENCY_CENTS + 1;
	public static final int TEXT_RANKING_TOTAL_MONEY					= TEXT_MILLION + 1;
	public static final int TEXT_RANKING_MONEY_AVERAGE					= TEXT_RANKING_TOTAL_MONEY + 1;
	public static final int TEXT_RANKING_MILLION						= TEXT_RANKING_MONEY_AVERAGE + 1;
	public static final int TEXT_RANKING_50_CENTS						= TEXT_RANKING_MILLION + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_1				= TEXT_RANKING_50_CENTS + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_2				= TEXT_JUSTUS_BEFORE_OPEN_CASE_1 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_3				= TEXT_JUSTUS_BEFORE_OPEN_CASE_2 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_4				= TEXT_JUSTUS_BEFORE_OPEN_CASE_3 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_5				= TEXT_JUSTUS_BEFORE_OPEN_CASE_4 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_6				= TEXT_JUSTUS_BEFORE_OPEN_CASE_5 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_7				= TEXT_JUSTUS_BEFORE_OPEN_CASE_6 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_8				= TEXT_JUSTUS_BEFORE_OPEN_CASE_7 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_9				= TEXT_JUSTUS_BEFORE_OPEN_CASE_8 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_10				= TEXT_JUSTUS_BEFORE_OPEN_CASE_9 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_11				= TEXT_JUSTUS_BEFORE_OPEN_CASE_10 + 1;
	public static final int TEXT_JUSTUS_BEFORE_OPEN_CASE_12				= TEXT_JUSTUS_BEFORE_OPEN_CASE_11 + 1;
	public static final int TEXT_JUSTUS_INTRO							= TEXT_JUSTUS_BEFORE_OPEN_CASE_12 + 1;
	public static final int TEXT_JUSTUS_PAUSE_INFO_KEY					= TEXT_JUSTUS_INTRO + 1;
	public static final int TEXT_JUSTUS_PAUSE_INFO_TOUCH				= TEXT_JUSTUS_PAUSE_INFO_KEY + 1;
	public static final int TEXT_JUSTUS_VALUES_BOX_INFO					= TEXT_JUSTUS_PAUSE_INFO_TOUCH + 1;
	public static final int TEXT_JUSTUS_VALUES_BOX_COMMAND_KEY			= TEXT_JUSTUS_VALUES_BOX_INFO + 1;
	public static final int TEXT_JUSTUS_VALUES_BOX_COMMAND_TOUCH		= TEXT_JUSTUS_VALUES_BOX_COMMAND_KEY + 1;
	public static final int TEXT_JUSTUS_BANKER_INTRO_1					= TEXT_JUSTUS_VALUES_BOX_COMMAND_TOUCH + 1;
	public static final int TEXT_JUSTUS_BANKER_INTRO_FINAL				= TEXT_JUSTUS_BANKER_INTRO_1 + 4;
	public static final int TEXT_JUSTUS_BANKER_CALLING_1				= TEXT_JUSTUS_BANKER_INTRO_FINAL + 1;
	public static final int TEXT_JUSTUS_BANKER_CALLING_FINAL			= TEXT_JUSTUS_BANKER_CALLING_1 + 4;
	public static final int TEXT_JUSTUS_BANKER_FIRST_OFFER_1			= TEXT_JUSTUS_BANKER_CALLING_FINAL + 1;
	public static final int TEXT_JUSTUS_BANKER_FIRST_OFFER_FINAL		= TEXT_JUSTUS_BANKER_FIRST_OFFER_1 + 2;
	public static final int TEXT_JUSTUS_BANKER_RAISE_1					= TEXT_JUSTUS_BANKER_FIRST_OFFER_FINAL + 1;
	public static final int TEXT_JUSTUS_BANKER_RAISE_2					= TEXT_JUSTUS_BANKER_RAISE_1 + 1;
	public static final int TEXT_JUSTUS_BANKER_RAISE_3					= TEXT_JUSTUS_BANKER_RAISE_2 + 1;
	public static final int TEXT_JUSTUS_BANKER_RAISE_FINAL				= TEXT_JUSTUS_BANKER_RAISE_3 + 1;
	public static final int TEXT_JUSTUS_BANKER_LOWER_1					= TEXT_JUSTUS_BANKER_RAISE_FINAL + 1;
	public static final int TEXT_JUSTUS_BANKER_LOWER_2					= TEXT_JUSTUS_BANKER_LOWER_1 + 1;
	public static final int TEXT_JUSTUS_BANKER_LOWER_3					= TEXT_JUSTUS_BANKER_LOWER_2 + 1;
	public static final int TEXT_JUSTUS_BANKER_LOWER_FINAL				= TEXT_JUSTUS_BANKER_LOWER_3 + 1;
	public static final int TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_1			= TEXT_JUSTUS_BANKER_LOWER_FINAL + 1;
	public static final int TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_2			= TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_1 + 1;
	public static final int TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_3			= TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_2 + 1;
	public static final int TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_FINAL		= TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_3 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_FIRST_CASE_1				= TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_FINAL + 1;
	public static final int TEXT_JUSTUS_CHOOSE_FIRST_CASE_2				= TEXT_JUSTUS_CHOOSE_FIRST_CASE_1 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_FIRST_CASE_3				= TEXT_JUSTUS_CHOOSE_FIRST_CASE_2 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_NEXT_CASE_1				= TEXT_JUSTUS_CHOOSE_FIRST_CASE_3 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_NEXT_CASE_2				= TEXT_JUSTUS_CHOOSE_NEXT_CASE_1 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_NEXT_CASE_3				= TEXT_JUSTUS_CHOOSE_NEXT_CASE_2 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_NEXT_CASE_4				= TEXT_JUSTUS_CHOOSE_NEXT_CASE_3 + 1;
	public static final int TEXT_JUSTUS_YOU_CHOSE_CASE					= TEXT_JUSTUS_CHOOSE_NEXT_CASE_4 + 1;
	public static final int TEXT_JUSTUS_CHOOSE_ONE_MORE_CASE			= TEXT_JUSTUS_YOU_CHOSE_CASE + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_1				= TEXT_JUSTUS_CHOOSE_ONE_MORE_CASE + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_2				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_1 + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_3				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_2 + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_4				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_3 + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_5				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_4 + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_6				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_5 + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_7				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_6 + 1;
	public static final int TEXT_JUSTUS_DEAL_OR_NO_DEAL_8				= TEXT_JUSTUS_DEAL_OR_NO_DEAL_7 + 1;
	public static final int TEXT_JUSTUS_THIS_CASE_HAD					= TEXT_JUSTUS_DEAL_OR_NO_DEAL_8 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_1					= TEXT_JUSTUS_THIS_CASE_HAD + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_2					= TEXT_JUSTUS_GOOD_RESULT_1 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_3					= TEXT_JUSTUS_GOOD_RESULT_2 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_4					= TEXT_JUSTUS_GOOD_RESULT_3 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_5					= TEXT_JUSTUS_GOOD_RESULT_4 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_6					= TEXT_JUSTUS_GOOD_RESULT_5 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_7					= TEXT_JUSTUS_GOOD_RESULT_6 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_8					= TEXT_JUSTUS_GOOD_RESULT_7 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_9					= TEXT_JUSTUS_GOOD_RESULT_8 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_10					= TEXT_JUSTUS_GOOD_RESULT_9 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_11					= TEXT_JUSTUS_GOOD_RESULT_10 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_12					= TEXT_JUSTUS_GOOD_RESULT_11 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_13					= TEXT_JUSTUS_GOOD_RESULT_12 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_14					= TEXT_JUSTUS_GOOD_RESULT_13 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_15					= TEXT_JUSTUS_GOOD_RESULT_14 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_16					= TEXT_JUSTUS_GOOD_RESULT_15 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_17					= TEXT_JUSTUS_GOOD_RESULT_16 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_18					= TEXT_JUSTUS_GOOD_RESULT_17 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_19					= TEXT_JUSTUS_GOOD_RESULT_18 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_20					= TEXT_JUSTUS_GOOD_RESULT_19 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_21					= TEXT_JUSTUS_GOOD_RESULT_20 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_22					= TEXT_JUSTUS_GOOD_RESULT_21 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_23					= TEXT_JUSTUS_GOOD_RESULT_22 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_24					= TEXT_JUSTUS_GOOD_RESULT_23 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_25					= TEXT_JUSTUS_GOOD_RESULT_24 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_26					= TEXT_JUSTUS_GOOD_RESULT_25 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_27					= TEXT_JUSTUS_GOOD_RESULT_26 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_FINAL				= TEXT_JUSTUS_GOOD_RESULT_27 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_1			= TEXT_JUSTUS_GOOD_RESULT_FINAL + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_2			= TEXT_JUSTUS_GOOD_RESULT_COMMENT_1 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_3			= TEXT_JUSTUS_GOOD_RESULT_COMMENT_2 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_4			= TEXT_JUSTUS_GOOD_RESULT_COMMENT_3 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_5			= TEXT_JUSTUS_GOOD_RESULT_COMMENT_4 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_6			= TEXT_JUSTUS_GOOD_RESULT_COMMENT_5 + 1;
	public static final int TEXT_JUSTUS_GOOD_RESULT_COMMENT_FINAL		= TEXT_JUSTUS_GOOD_RESULT_COMMENT_6 + 1;
	public static final int TEXT_JUSTUS_MEDIUM_RESULT_1					= TEXT_JUSTUS_GOOD_RESULT_COMMENT_FINAL + 1;
	public static final int TEXT_JUSTUS_MEDIUM_RESULT_2					= TEXT_JUSTUS_MEDIUM_RESULT_1 + 1;
	public static final int TEXT_JUSTUS_MEDIUM_RESULT_3					= TEXT_JUSTUS_MEDIUM_RESULT_2 + 1;
	public static final int TEXT_JUSTUS_MEDIUM_RESULT_4					= TEXT_JUSTUS_MEDIUM_RESULT_3 + 1;
	public static final int TEXT_JUSTUS_MEDIUM_RESULT_5					= TEXT_JUSTUS_MEDIUM_RESULT_4 + 1;
	public static final int TEXT_JUSTUS_YOU_HAVE_CHANCE_1				= TEXT_JUSTUS_MEDIUM_RESULT_5 + 1;
	public static final int TEXT_JUSTUS_YOU_HAVE_CHANCE_FINAL			= TEXT_JUSTUS_YOU_HAVE_CHANCE_1 + 2;
	public static final int TEXT_JUSTUS_NO_MORE_BAD_CASES				= TEXT_JUSTUS_YOU_HAVE_CHANCE_FINAL + 1;
	public static final int TEXT_JUSTUS_NO_MORE_GOOD_CASES				= TEXT_JUSTUS_NO_MORE_BAD_CASES + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_1					= TEXT_JUSTUS_NO_MORE_GOOD_CASES + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_2					= TEXT_JUSTUS_BAD_RESULT_1 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_3					= TEXT_JUSTUS_BAD_RESULT_2 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_4					= TEXT_JUSTUS_BAD_RESULT_3 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_5					= TEXT_JUSTUS_BAD_RESULT_4 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_6					= TEXT_JUSTUS_BAD_RESULT_5 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_7					= TEXT_JUSTUS_BAD_RESULT_6 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_8					= TEXT_JUSTUS_BAD_RESULT_7 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_9					= TEXT_JUSTUS_BAD_RESULT_8 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_10					= TEXT_JUSTUS_BAD_RESULT_9 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_11					= TEXT_JUSTUS_BAD_RESULT_10 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_FINAL				= TEXT_JUSTUS_BAD_RESULT_11 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_COMMENT_1			= TEXT_JUSTUS_BAD_RESULT_FINAL + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_COMMENT_2			= TEXT_JUSTUS_BAD_RESULT_COMMENT_1 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_COMMENT_3			= TEXT_JUSTUS_BAD_RESULT_COMMENT_2 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_COMMENT_4			= TEXT_JUSTUS_BAD_RESULT_COMMENT_3 + 1;
	public static final int TEXT_JUSTUS_BAD_RESULT_COMMENT_5			= TEXT_JUSTUS_BAD_RESULT_COMMENT_4 + 1;
	public static final int TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_1			= TEXT_JUSTUS_BAD_RESULT_COMMENT_5 + 1;
	public static final int TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_2			= TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_1 + 1;
	public static final int TEXT_JUSTUS_TURN_START_1					= TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_2 + 1;
	public static final int TEXT_JUSTUS_TURN_START_2					= TEXT_JUSTUS_TURN_START_1 + 1;
	public static final int TEXT_JUSTUS_TURN_START_3					= TEXT_JUSTUS_TURN_START_2 + 1;
	public static final int TEXT_JUSTUS_TURN_START_4					= TEXT_JUSTUS_TURN_START_3 + 1;
	public static final int TEXT_JUSTUS_TURN_START_5					= TEXT_JUSTUS_TURN_START_4 + 1;
	public static final int TEXT_JUSTUS_NEAR_END_1						= TEXT_JUSTUS_TURN_START_5 + 1;
	public static final int TEXT_JUSTUS_NEAR_END_2						= TEXT_JUSTUS_NEAR_END_1 + 1;
	public static final int TEXT_JUSTUS_NEAR_END_3						= TEXT_JUSTUS_NEAR_END_2 + 1;
	public static final int TEXT_JUSTUS_NEAR_END_4						= TEXT_JUSTUS_NEAR_END_3 + 1;
	public static final int TEXT_JUSTUS_NEAR_END_5						= TEXT_JUSTUS_NEAR_END_4 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_1						= TEXT_JUSTUS_NEAR_END_5 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_2						= TEXT_JUSTUS_LAST_TURN_1 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_FINAL					= TEXT_JUSTUS_LAST_TURN_2 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_COMMENT_1				= TEXT_JUSTUS_LAST_TURN_FINAL + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_COMMENT_2				= TEXT_JUSTUS_LAST_TURN_COMMENT_1 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_COMMENT_3				= TEXT_JUSTUS_LAST_TURN_COMMENT_2 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_COMMENT_4				= TEXT_JUSTUS_LAST_TURN_COMMENT_3 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_COMMENT_5				= TEXT_JUSTUS_LAST_TURN_COMMENT_4 + 1;
	public static final int TEXT_JUSTUS_LAST_TURN_COMMENT_FINAL			= TEXT_JUSTUS_LAST_TURN_COMMENT_5 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MIN_VALUE_1			= TEXT_JUSTUS_LAST_TURN_COMMENT_FINAL + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MIN_VALUE_2			= TEXT_JUSTUS_GAME_OVER_MIN_VALUE_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MIN_VALUE_FINAL		= TEXT_JUSTUS_GAME_OVER_MIN_VALUE_2 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_1			= TEXT_JUSTUS_GAME_OVER_MIN_VALUE_FINAL + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_2			= TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_3			= TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_2 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_4			= TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_3 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_1		= TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_4 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_2		= TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_3		= TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_2 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_4		= TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_3 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_5		= TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_4 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_1		= TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_5 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_2		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_3		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_2 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_4		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_3 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_5		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_4 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_6		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_5 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_7		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_6 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_FINAL	= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_7 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MAX_VALUE_1			= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_FINAL + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MAX_VALUE_2			= TEXT_JUSTUS_GAME_OVER_MAX_VALUE_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_MAX_VALUE_FINAL		= TEXT_JUSTUS_GAME_OVER_MAX_VALUE_2 + 1;
	public static final int TEXT_JUSTUS_DEAL_1							= TEXT_JUSTUS_GAME_OVER_MAX_VALUE_FINAL + 1;
	public static final int TEXT_JUSTUS_DEAL_2							= TEXT_JUSTUS_DEAL_1 + 1;
	public static final int TEXT_JUSTUS_DEAL_FINAL						= TEXT_JUSTUS_DEAL_2 + 1;
	public static final int TEXT_JUSTUS_SEE_REMAINING_CASES_1			= TEXT_JUSTUS_DEAL_FINAL + 1;
	public static final int TEXT_JUSTUS_SEE_REMAINING_CASES_2			= TEXT_JUSTUS_SEE_REMAINING_CASES_1 + 1;
	public static final int TEXT_JUSTUS_SEE_REMAINING_CASES_3			= TEXT_JUSTUS_SEE_REMAINING_CASES_2 + 1;
	public static final int TEXT_JUSTUS_SEE_REMAINING_CASES_FINAL		= TEXT_JUSTUS_SEE_REMAINING_CASES_3 + 1;
	public static final int TEXT_JUSTUS_SKIP_REMAINING_CASES_1			= TEXT_JUSTUS_SEE_REMAINING_CASES_FINAL + 1;
	public static final int TEXT_JUSTUS_SKIP_REMAINING_CASES_2			= TEXT_JUSTUS_SKIP_REMAINING_CASES_1 + 1;
	public static final int TEXT_JUSTUS_SKIP_REMAINING_CASES_FINAL		= TEXT_JUSTUS_SKIP_REMAINING_CASES_2 + 1;
	public static final int TEXT_JUSTUS_SKIPPING_REMAINING_CASES_1		= TEXT_JUSTUS_SKIP_REMAINING_CASES_FINAL + 1;
	public static final int TEXT_JUSTUS_SKIPPING_REMAINING_CASES_2		= TEXT_JUSTUS_SKIPPING_REMAINING_CASES_1 + 1;
	public static final int TEXT_JUSTUS_SKIPPING_REMAINING_CASES_FINAL	= TEXT_JUSTUS_SKIPPING_REMAINING_CASES_2 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_START_1				= TEXT_JUSTUS_SKIPPING_REMAINING_CASES_FINAL + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_START_2				= TEXT_JUSTUS_GAME_OVER_START_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_START_3				= TEXT_JUSTUS_GAME_OVER_START_2 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_START_4				= TEXT_JUSTUS_GAME_OVER_START_3 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_START_FINAL			= TEXT_JUSTUS_GAME_OVER_START_4 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_END_1					= TEXT_JUSTUS_GAME_OVER_START_FINAL + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_END_2					= TEXT_JUSTUS_GAME_OVER_END_1 + 1;
	public static final int TEXT_JUSTUS_GAME_OVER_END_3					= TEXT_JUSTUS_GAME_OVER_END_2 + 1;
	public static final int TEXT_JUSTUS_CHECK_ONLINE_RANKING_1			= TEXT_JUSTUS_GAME_OVER_END_3 + 1;
	public static final int TEXT_JUSTUS_CHECK_ONLINE_RANKING_2			= TEXT_JUSTUS_CHECK_ONLINE_RANKING_1 + 1;
	public static final int TEXT_JUSTUS_CHECK_ONLINE_RANKING_FINAL		= TEXT_JUSTUS_CHECK_ONLINE_RANKING_2 + 1;
	public static final int TEXT_JUSTUS_COMMAND_TIP						= TEXT_JUSTUS_CHECK_ONLINE_RANKING_FINAL + 1;
	public static final int TEXT_JUSTUS_GOOD_MORNING					= TEXT_JUSTUS_COMMAND_TIP + 1;
	public static final int TEXT_JUSTUS_GOOD_AFTERNOON					= TEXT_JUSTUS_GOOD_MORNING + 1;
	public static final int TEXT_JUSTUS_GOOD_NIGHT						= TEXT_JUSTUS_GOOD_AFTERNOON + 1;

	/*Textos "copiados"*/
	public static final int TEXT_JUSTUS_BEST_RESULT_COMMENT_1			= TEXT_JUSTUS_GOOD_RESULT_COMMENT_1;
	public static final int TEXT_JUSTUS_BEST_RESULT_COMMENT_TOTAL		= TEXT_JUSTUS_GOOD_RESULT_COMMENT_FINAL - TEXT_JUSTUS_BEST_RESULT_COMMENT_1;
	public static final int TEXT_JUSTUS_WORST_RESULT_COMMENT_1			= TEXT_JUSTUS_BAD_RESULT_COMMENT_1;
	public static final int TEXT_JUSTUS_WORST_RESULT_COMMENT_FINAL		= TEXT_JUSTUS_BAD_RESULT_COMMENT_5;
	public static final int TEXT_JUSTUS_WORST_RESULT_COMMENT_TOTAL		= TEXT_JUSTUS_WORST_RESULT_COMMENT_FINAL - TEXT_JUSTUS_WORST_RESULT_COMMENT_1;

	public static final byte TEXT_JUSTUS_BANKER_FIRST_OFFER_TOTAL			= TEXT_JUSTUS_BANKER_FIRST_OFFER_FINAL - TEXT_JUSTUS_BANKER_FIRST_OFFER_1 + 1;
	public static final byte TEXT_JUSTUS_BANKER_CALLING_TOTAL				= TEXT_JUSTUS_BANKER_CALLING_FINAL - TEXT_JUSTUS_BANKER_CALLING_1 + 1;
	public static final byte TEXT_JUSTUS_BANKER_RAISE_TOTAL					= TEXT_JUSTUS_BANKER_RAISE_FINAL - TEXT_JUSTUS_BANKER_RAISE_1 + 1;
	public static final byte TEXT_JUSTUS_BANKER_LOWER_TOTAL					= TEXT_JUSTUS_BANKER_LOWER_FINAL - TEXT_JUSTUS_BANKER_LOWER_1 + 1;
	public static final byte TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_TOTAL		= TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_FINAL - TEXT_JUSTUS_BANKER_PREVIOUS_OFFER_1 + 1;
	public static final byte TEXT_JUSTUS_DEAL_NO_DEAL_TOTAL					= 8;
	public static final byte TEXT_JUSTUS_BEFORE_OPEN_CASE_TOTAL				= 12;

	public static final byte TEXT_JUSTUS_PLAY_GOOD_TOTAL					= TEXT_JUSTUS_GOOD_RESULT_FINAL - TEXT_JUSTUS_GOOD_RESULT_1 + 1;
	public static final byte TEXT_JUSTUS_PLAY_GOOD_COMMENTS_TOTAL			= 7;
	public static final byte TEXT_JUSTUS_PLAY_MEDIUM_TOTAL					= 5;
	public static final byte TEXT_JUSTUS_PLAY_BAD_TOTAL						= TEXT_JUSTUS_BAD_RESULT_FINAL - TEXT_JUSTUS_BAD_RESULT_1 + 1;
	public static final byte TEXT_JUSTUS_PLAY_BAD_COMMENTS_TOTAL			= 5;

	public static final byte TEXT_JUSTUS_YOU_HAVE_CHANCE_TOTAL				= TEXT_JUSTUS_YOU_HAVE_CHANCE_FINAL - TEXT_JUSTUS_YOU_HAVE_CHANCE_1 + 1;

	public static final byte TEXT_JUSTUS_CHOOSE_NEXT_CASE_START_TOTAL		= 3;
	public static final byte TEXT_JUSTUS_YOU_CAN_STILL_CHOOSE_TOTAL			= 2;
	public static final byte TEXT_JUSTUS_NEAR_END_TOTAL						= 5;

	public static final byte TEXT_JUSTUS_TURN_START_TOTAL					= 5;
	public static final byte TEXT_JUSTUS_LAST_TURN_TOTAL					= TEXT_JUSTUS_LAST_TURN_FINAL - TEXT_JUSTUS_LAST_TURN_1 + 1;
	public static final byte TEXT_JUSTUS_LAST_TURN_COMMENT_1_TOTAL			= TEXT_JUSTUS_LAST_TURN_COMMENT_FINAL - TEXT_JUSTUS_LAST_TURN_COMMENT_1 + 1;

	public static final byte TEXT_JUSTUS_GAME_OVER_END_TOTAL				= 3;
	public static final byte TEXT_JUSTUS_GAME_OVER_MIN_TOTAL				= 3;
	public static final byte TEXT_JUSTUS_GAME_OVER_MAX_TOTAL				= 3;
	public static final byte TEXT_JUSTUS_GAME_OVER_START_TOTAL				= TEXT_JUSTUS_GAME_OVER_START_FINAL - TEXT_JUSTUS_GAME_OVER_START_1 + 1;
	public static final byte TEXT_JUSTUS_GAME_OVER_BAD_COMMENT_TOTAL		= 4;
	public static final byte TEXT_JUSTUS_GAME_OVER_MEDIUM_COMMENT_TOTAL		= 5;
	public static final byte TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_TOTAL		= TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_FINAL - TEXT_JUSTUS_GAME_OVER_GOOD_COMMENT_1 + 1;
	public static final byte TEXT_JUSTUS_GAME_OVER_MIN_VALUE_TOTAL			= TEXT_JUSTUS_GAME_OVER_MIN_VALUE_FINAL - TEXT_JUSTUS_GAME_OVER_MIN_VALUE_1 + 1;
	public static final byte TEXT_JUSTUS_GAME_OVER_MAX_VALUE_TOTAL          = TEXT_JUSTUS_GAME_OVER_MAX_VALUE_FINAL - TEXT_JUSTUS_GAME_OVER_MAX_VALUE_1 + 1;
	public static final byte TEXT_JUSTUS_SEE_REMAINING_CASES_TOTAL			= TEXT_JUSTUS_SEE_REMAINING_CASES_FINAL - TEXT_JUSTUS_SEE_REMAINING_CASES_1 + 1;
	public static final byte TEXT_JUSTUS_DEAL_TOTAL							= TEXT_JUSTUS_DEAL_FINAL - TEXT_JUSTUS_DEAL_1 + 1;
	public static final byte TEXT_JUSTUS_SKIP_REMAINING_CASES_TOTAL			= TEXT_JUSTUS_SKIP_REMAINING_CASES_FINAL - TEXT_JUSTUS_SKIP_REMAINING_CASES_1 + 1;
	public static final byte TEXT_JUSTUS_SKIPPING_REMAINING_CASES_TOTAL		= TEXT_JUSTUS_SKIPPING_REMAINING_CASES_FINAL - TEXT_JUSTUS_SKIPPING_REMAINING_CASES_1 + 1;
	public static final byte TEXT_JUSTUS_CHECK_ONLINE_RANKING_TOTAL			= TEXT_JUSTUS_CHECK_ONLINE_RANKING_FINAL - TEXT_JUSTUS_CHECK_ONLINE_RANKING_1 + 1;

	
	/** número total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_JUSTUS_GOOD_NIGHT + 1;
	
	public static final byte TEXT_CONFIRM_EXIT_TOTAL = 5;
	// </editor-fold>


	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_CHOOSE_SOUND				= 0;
	public static final byte SCREEN_SPLASH_NANO					= SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_ENDEMOL				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME					= SCREEN_SPLASH_ENDEMOL + 1;
	public static final byte SCREEN_MAIN_MENU					= SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_NEW_GAME					= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_CONTINUE_GAME				= SCREEN_NEW_GAME + 1;
	public static final byte SCREEN_OPTIONS						= SCREEN_CONTINUE_GAME + 1;
	public static final byte SCREEN_HELP_MENU					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_CONTROLS				= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_RULES				= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_HELP_PROFILE				= SCREEN_HELP_RULES + 1;
	public static final byte SCREEN_CREDITS						= SCREEN_HELP_PROFILE + 1;
	public static final byte SCREEN_PAUSE						= SCREEN_CREDITS + 1;
	public static final byte SCREEN_CONFIRM_MENU				= SCREEN_PAUSE + 1;
	public static final byte SCREEN_CONFIRM_EXIT				= SCREEN_CONFIRM_MENU + 1;
	public static final byte SCREEN_LOADING_1					= SCREEN_CONFIRM_EXIT + 1;
	public static final byte SCREEN_LOADING_2					= SCREEN_LOADING_1 + 1;
	public static final byte SCREEN_LOADING_PLAY_SCREEN			= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_CHOOSE_LANGUAGE				= SCREEN_LOADING_PLAY_SCREEN + 1;
	public static final byte SCREEN_NANO_RANKING_MENU			= SCREEN_CHOOSE_LANGUAGE + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
    public static final byte SCREEN_RECORDS						= SCREEN_NO_PROFILE+1;
	public static final byte SCREEN_NEWS_MENU					= SCREEN_RECORDS + 1;
	public static final byte SCREEN_NEWS_VIEW_FIRST				= SCREEN_NEWS_MENU + 1;
	public static final byte SCREEN_NEWS_VIEW_LAST				= SCREEN_NEWS_VIEW_FIRST + NewsFeederManager.MAX_NEWS_ENTRIES - 1;
	public static final byte SCREEN_NEWS_HELP					= SCREEN_NEWS_VIEW_LAST + 1;
	public static final byte SCREEN_NEWS_REFRESH				= SCREEN_NEWS_HELP + 1;
	public static final byte SCREEN_ERROR_LOG					= SCREEN_NEWS_REFRESH + 1;
	public static final byte SCREEN_GAME_OVER					= SCREEN_ERROR_LOG + 1;
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU PRINCIPAL">
	
	// menu principal
	public static final byte ENTRY_MAIN_MENU_NEW_GAME		= 0;
	public static final byte ENTRY_MAIN_MENU_OPTIONS		= 1;
	public static final byte ENTRY_MAIN_MENU_NEWS		    = 2;
	public static final byte ENTRY_MAIN_MENU_NANO_ONLINE    = 3;
	public static final byte ENTRY_MAIN_MENU_RECOMMEND		= 4;
	public static final byte ENTRY_MAIN_MENU_HELP			= 5;
	public static final byte ENTRY_MAIN_MENU_CREDITS		= 6;
	public static final byte ENTRY_MAIN_MENU_EXIT			= 7;
	
	//#if DEBUG == "true"
		public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif		
	
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DA TELA DE PAUSA">

	public static final byte ENTRY_PAUSE_MENU_CONTINUE				= 0;
	public static final byte ENTRY_PAUSE_MENU_TOGGLE_SOUND			= 1;
	public static final byte ENTRY_PAUSE_MENU_VOLUME				= 2;
	public static final byte ENTRY_PAUSE_MENU_VIB_TOGGLE_VIBRATION	= 3;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_TO_MENU		= 4;
	public static final byte ENTRY_PAUSE_MENU_VIB_EXIT_GAME			= 5;

	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_TO_MENU	= 3;
	public static final byte ENTRY_PAUSE_MENU_NO_VIB_EXIT_GAME		= 4;

	public static final byte ENTRY_OPTIONS_MENU_TOGGLE_SOUND			= 0;
	public static final byte ENTRY_OPTIONS_MENU_VOLUME					= 1;
	public static final byte ENTRY_OPTIONS_MENU_VIB_TOGGLE_VIBRATION	= 2;

	public static final byte ENTRY_OPTIONS_MENU_NO_VIB_BACK				= 2;
	public static final byte ENTRY_OPTIONS_MENU_VIB_BACK				= 3;


	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DA TELA DE FIM DE JOGO">

	public static final byte ENTRY_GAME_OVER_HIGH_SCORES	= 0;
	public static final byte ENTRY_GAME_OVER_NEW_GAME		= 1;
	public static final byte ENTRY_GAME_OVER_BACK_MENU		= 2;
	public static final byte ENTRY_GAME_OVER_EXIT_GAME		= 3;


	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DA TELA DE SELEÇÃO DE PERFIL">

	public static final byte ENTRY_CHOOSE_PROFILE_USE_CURRENT	= 0;
	public static final byte ENTRY_CHOOSE_PROFILE_CHANGE		= 1;
	public static final byte ENTRY_CHOOSE_PROFILE_HELP			= 2;
	public static final byte ENTRY_CHOOSE_PROFILE_BACK			= 3;


	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE NOT�?CIAS">

	public static final byte ENTRY_NEWS_GENERAL		= 0;
	public static final byte ENTRY_NEWS_SCORERS		= 1;
	public static final byte ENTRY_NEWS_TABLE		= 2;
	//public static final byte ENTRY_NEWS_HELP		= 3;
	public static final byte ENTRY_NEWS_BACK		= 3;

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU INICIAL DE SOM">

	public static final byte ENTRY_SOUND_ON_OFF		= 0;
	public static final byte ENTRY_SOUND_VOLUME		= 1;
	public static final byte ENTRY_SOUND_OK			= 2;

	// </editor-fold>

	/***/
	public static final byte CASES_TOTAL = 26;

	public static final byte SHINE_SEQUENCE_RIGHT	= 0;
	public static final byte SHINE_SEQUENCE_LEFT	= 1;
	public static final byte SHINE_SEUQENCE_STOPPED	= 2;

	public static final short TITLE_WAIT_TIME = 550;

	public static final int MIN_SCORE_FOR_CONFETTI = 100000 * REAL;
	public static final int MAX_SCORE_FOR_CONFETTI = 1000000 * REAL;
	public static final int SCORE_FOR_CONFETTI_RANGE = MAX_SCORE_FOR_CONFETTI - MIN_SCORE_FOR_CONFETTI;
	public static final int SCORE_FOR_CONFETTI_STEPS = 1000;
	public static final int SCORE_FOR_CONFETTI_PRECISION = SCORE_FOR_CONFETTI_RANGE/SCORE_FOR_CONFETTI_STEPS;

	
	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 		public static final short PARTICLES_MAX_CONFETTIS = 100;
//# 		public static final byte PARTICLES_MIN_CONFETTI_SPEED = 30;
//# 		public static final byte PARTICLES_MAX_CONFETTI_SPEED = 60;
//# 		public static final short PARTICLES_MIN_SPARK_SPEED = 125;
//# 		public static final byte PARTICLES_SPARK_SPEED_RANGE = 25;
//# 		public static final int PARTICLES_GRAVITY_SPEED = 9800000;
//# 		public static final short PARTICLES_MAX_SPARKS = 45;
//# 		public static final short PARTICLES_SPARKS_PER_SECOND = 15;
//# 
//#  		/** Altura da caixa de texto do Roberto Justus. */
//# 		public static final byte TEXTBOX_HEIGHT = 30;
//#  		public static final boolean ABSOLUTE_POSITION_INTERSECTION = false;
//# 
//# 		/** Espaçamento entre as entradas do menu. */
//# 		public static final byte MENU_SPACING = -5;
//# 
//#  		/***/
//#  		public static final byte VALUES_BOX_OFFSET = 3;
//#  		public static final byte VALUES_BOX_WIDTH = 42;
//#   		public static final byte VALUES_BOX_HEIGHT = 9;
//# 
//#  		public static final byte VALUES_BOX_TEXT_OFFSET = 3;
//# 
//#  		/***/
//#  		public static final byte MENU_LABEL_OFFSET_X = 4;
//# 
//# 		/** Largura total da borda colorida, no modo retrato. */
//# 		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_WIDTH = 128;
//# 		/** Largura total da borda colorida, no modo retrato. */
//# 		/** Altura total da borda colorida, no modo retrato. */
//# 		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_HEIGHT = 174;
//# 		/** Largura total da borda colorida, no modo paisagem. */
//# 		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_WIDTH = 146;
//# 		/** Altura total da borda colorida, no modo paisagem. */
//# 		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_HEIGHT = 122;
//# 
//#  		/** Velocidade máxima da movimentação das telas no cenário do jogo. */
//#  		public static final byte SCREEN_ANIMATION_MAX_SPEED = 20;
//# 
//#  		/** Aproximação usada para arredondar a posição do pattern animado dos spots, na tela de valores. */
//#  		public static final byte BKG_ANIMATION_ROUND = 7;
//# 
//#  		public static final short BANKER_HEART_X = 53;
//#  		public static final short BANKER_SCREENS_Y = 4;
//#  		public static final short BANKER_GRAPH_X = 29;
//# 		public static final short BANKER_HEART_WIDTH = 24;
//# 
//#  		public static final byte BANKER_PATTERN_CENTER_Y = 32;
//#  		public static final short BANKER_PATTERN_TOP_HEIGHT = BANKER_PATTERN_CENTER_Y + 65;
//#  		public static final byte BANKER_BKG_Y = BANKER_PATTERN_CENTER_Y;
//# 
//# 		public static final byte BANKER_BARS_LINE_DENSITY = 2;
//# 		public static final short BANKER_BARS_TOP_WIDTH = 77;
//# 
//#  		/***/
//#  		public static final short VALUES_BOX_BORDER_Y_OFFSET = 10;
//# 		public static final byte CASE_CASE_POSITION_X = 0;
//#  		public static final byte CASE_CASE_POSITION_Y = 28;
//#  		public static final byte CASE_GIRL_POSITION_X = 12;
//#  		public static final byte CASE_GIRL_POSITION_Y = 0;
//#  		public static final byte CASE_GIRL_REF_PIXEL_Y = -2;
//# 		public static final short CASE_SMALL_LABEL_Y = 4;
//#  		public static final short CASE_SMALL_NUMBER_ERROR = 1;
	//#endif
	//#if HYBRID == "true" || SCREEN_SIZE == "SMALL"
//# 
//#   		public static final short CASE_BIG_LABEL_VALUE_Y = 37;
//#   		public static final short CASE_BIG_LABEL_NUMBER_Y = 24;
//#   		public static final short CASE_BIG_LABEL_NUMBER_SMALL_Y = 15;
	//#endif
	//#if SCREEN_SIZE == "SMALL"
//# 
//#  		public static final byte BANKER_CASE_OFFSET_Y = 5;
//# 
//#  		public static final byte BKG_SCREEN_SPACING		= 9;
//#  		public static final byte BKG_SCREEN_Y			= 21;
//#  		public static final byte NUMBER_OF_SCREENS		= 5;
//# 
//#  		/** Largura total do palco, em pixels. */
//# 		public static final short STAGE_WIDTH = 310;
//#  		public static final byte STAGE_TOP_SPOTS_HEIGHT = 13;
//#  		public static final short STAGE_BKG_BEGIN = STAGE_TOP_SPOTS_HEIGHT;
//#  		public static final byte STAGE_BKG_HORIZONTAL_LINES = 3;
//#  		public static final short STAGE_BKG_HORIZONTAL_LINE_HEIGHT = 23;
//#  		public static final short STAGE_BKG_HEIGHT = STAGE_BKG_HORIZONTAL_LINE_HEIGHT * STAGE_BKG_HORIZONTAL_LINES;
//#  		public static final byte STAGE_BKG_HORIZONTAL_LINE_DENSITY = 2;
//#  		public static final byte STAGE_STAIR_HEIGHT = 14;
//# 
//#  		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT1 = 1;
//#  		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT2 = 4;
//#  		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT3 = 1;
//#  		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT4 = 1;
//#  		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT5 = 0;
//# 
//#  		public static final short STAGE_STAIR_BEGIN = STAGE_BKG_BEGIN + STAGE_BKG_HEIGHT;
//#  		public static final byte STAGE_STAIR_REAL_HEIGHT = 7;
//#  		public static final byte STAGE_STAIRS = 8;
//#  		public static final short STAGE_STAIRS_TOTAL_HEIGHT = STAGE_STAIR_HEIGHT*STAGE_STAIRS;
//#  		public static final short STAGE_FLOOR_BEGIN = STAGE_STAIR_BEGIN + STAGE_STAIRS_TOTAL_HEIGHT;
//#  		public static final byte STAGE_FLOOR_HEIGHT = 27; /** Altura mínima do pattern de chão da tela de jogo, em pixels. */
//#  		public static final short STAGE_HEIGHT = STAGE_TOP_SPOTS_HEIGHT + STAGE_BKG_HEIGHT + STAGE_STAIRS_TOTAL_HEIGHT + STAGE_FLOOR_HEIGHT;
//# 
//#  		public static final short GIRL_WIDTH = 39;
//#  		public static final byte TOUCH_TOLERANCE = 5;
//# 
//#  		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
//#  		public static final short SPOT_SIZE_SMALL	= 47;
//#  		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
//#  		public static final short SPOT_SIZE_BIG		= 80;
//# 
//#  		// Velocidade para percorer o menu
//#  		public static final short TIME_GOTO_MENUITEM = 500;
//# 
//#  		// Distancia entre o texto "Você ganhou" para o valor do premio
//#  		public static final byte WINSCREEN_TEXT_DISTANCE = 10;
//# 
//#  		// Dados do emissor de particulas
//#  		public static final int MIN_WIDTH_PER_PARTICLE = 10;
//#  		public static final int MAX_WIDTH_PER_PARTICLE = 100;
//#  		public static final byte MAX_WIDTH_SPARKEMITTER = 85;
//# 
//# 		/** Espaçamento vertical do título. */
//# 		public static final byte TITLE_Y_SPACING = 3;
//# 
//# 		/** Offset em y na animação do brilho girando em relação ao logo. */
//# 		public static final byte SPLASH_SHINE_OFFSET_Y = -4;
//# 
//# 		/** Altura mínima do pattern de chão da tela de jogo, em pixels. */
//# 		public static final byte FLOOR_HEIGHT = 70;
//# 
//# 
//# 	//</editor-fold>
//# 
	//#elif SCREEN_SIZE == "MEDIUM"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍ?FICAS PARA TELA MÉDIA" defaultstate="collapsed">
//# 		public static final short PARTICLES_MAX_CONFETTIS = 200;
//# 		public static final byte PARTICLES_MIN_CONFETTI_SPEED = 30;
//# 		public static final byte PARTICLES_MAX_CONFETTI_SPEED = 60;
//# 		public static final short PARTICLES_MIN_SPARK_SPEED = 150;
//# 		public static final byte PARTICLES_SPARK_SPEED_RANGE = 30;
//# 		public static final int PARTICLES_GRAVITY_SPEED = 12800000;
//# 		public static final short PARTICLES_MAX_SPARKS = 60;
//# 		public static final short PARTICLES_SPARKS_PER_SECOND = 20;
//#
//#  		/** Altura da caixa de texto do Roberto Justus. */
//# 		public static final byte TEXTBOX_HEIGHT = 38;
//#  		public static final boolean ABSOLUTE_POSITION_INTERSECTION = false;
//#
//# 		/** Espaçamento entre as entradas do menu. */
//# 		public static final byte MENU_SPACING = -4;
//#
//#  		/***/
//#  		public static final byte VALUES_BOX_OFFSET = 3;
//#  		public static final byte VALUES_BOX_WIDTH = 68;
//#   		public static final byte VALUES_BOX_HEIGHT = 12;
//#
//#  		public static final byte VALUES_BOX_TEXT_OFFSET = 5;
//#
//#  		/***/
//#  		public static final byte MENU_LABEL_OFFSET_X = 4;
//#
//#  		/** Largura total da borda colorida, no modo retrato. */
//#  		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_WIDTH = 175;
//#  		/** Altura total da borda colorida, no modo retrato. */
//#  		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_HEIGHT = 276;
//#
//#  		/** Largura total da borda colorida, no modo paisagem. */
//#  		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_WIDTH = 250;
//#  		/** Altura total da borda colorida, no modo paisagem. */
//#  		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_HEIGHT = 200;
//#
//#  		/** Velocidade máxima da movimentação das telas no cenário do jogo. */
//#  		public static final byte SCREEN_ANIMATION_MAX_SPEED = 20;
//#
//#  		/** Aproximação usada para arredondar a posição do pattern animado dos spots, na tela de valores. */
//#  		public static final byte BKG_ANIMATION_ROUND = 9;
//#
//#  		public static final short BANKER_HEART_X = 86;
//#  		public static final short BANKER_SCREENS_Y = 7;
//#  		public static final short BANKER_GRAPH_X = 46;
//# 		public static final short BANKER_HEART_WIDTH = 37;
//#
//#  		public static final byte BANKER_PATTERN_CENTER_Y = 32;
//#  		public static final short BANKER_PATTERN_TOP_HEIGHT = BANKER_PATTERN_CENTER_Y + 123;
//#  		public static final byte BANKER_BKG_Y = BANKER_PATTERN_CENTER_Y;
//#
//# 		public static final byte BANKER_BARS_LINE_DENSITY = 2;
//# 		public static final short BANKER_BARS_TOP_WIDTH = 125;
//#
//#  		/***/
//#  		public static final short VALUES_BOX_BORDER_Y_OFFSET = 10;
//#
//#  		public static final byte CASE_CASE_POSITION_X = 7;
//#  		public static final byte CASE_CASE_POSITION_Y = 36;
//#  		public static final byte CASE_GIRL_POSITION_X = 31;
//#  		public static final byte CASE_GIRL_POSITION_Y = 0;
//#
//# 		public static final byte CASE_GIRL_REF_PIXEL_Y = -4;
//# 		public static final short CASE_SMALL_LABEL_Y = 12;
//# 		public static final short CASE_SMALL_NUMBER_ERROR = -1;
//#
	//#if HYBRID != "true"
//# 		public static final short CASE_BIG_LABEL_VALUE_Y = 51;
//# 		public static final short CASE_BIG_LABEL_NUMBER_Y = 38;
//# 		public static final short CASE_BIG_LABEL_NUMBER_SMALL_Y = 12;
	//#endif
//#
//#  		public static final byte BANKER_CASE_OFFSET_Y = 9;
//#
//#  		public static final byte BKG_SCREEN_SPACING		= 24;
//#  		public static final byte BKG_SCREEN_Y			= 45;
//#  		public static final byte NUMBER_OF_SCREENS		= 2;
//#
//#  		/** Largura total do palco, em pixels. */
//# 		public static final short STAGE_WIDTH = 450;
//# 		public static final byte STAGE_TOP_SPOTS_HEIGHT = 18;
//# 		public static final short STAGE_BKG_BEGIN = STAGE_TOP_SPOTS_HEIGHT;
//# 		public static final byte STAGE_BKG_HORIZONTAL_LINES = 5;
//# 		public static final short STAGE_BKG_HORIZONTAL_LINE_HEIGHT = 20;
//#  		public static final short STAGE_BKG_HEIGHT = STAGE_BKG_HORIZONTAL_LINE_HEIGHT * STAGE_BKG_HORIZONTAL_LINES;
//# 		public static final byte STAGE_BKG_HORIZONTAL_LINE_DENSITY = 2;
//# 		public static final byte STAGE_STAIR_HEIGHT = 14;
//#
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT1 = 1;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT2 = 4;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT3 = 1;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT4 = 1;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT5 = 0;
//#
//# 		public static final short STAGE_STAIR_BEGIN = STAGE_BKG_BEGIN + STAGE_BKG_HEIGHT;
//# 		public static final byte STAGE_STAIR_REAL_HEIGHT = 7;
//# 		public static final byte STAGE_STAIRS = 8;
//# 		public static final short STAGE_STAIRS_TOTAL_HEIGHT = STAGE_STAIR_HEIGHT*STAGE_STAIRS;
//# 		public static final short STAGE_FLOOR_BEGIN = STAGE_STAIR_BEGIN + STAGE_STAIRS_TOTAL_HEIGHT;
//# 		public static final byte STAGE_FLOOR_HEIGHT = 48; /** Altura mínima do pattern de chão da tela de jogo, em pixels. */
//# 		public static final short STAGE_HEIGHT = STAGE_TOP_SPOTS_HEIGHT + STAGE_BKG_HEIGHT + STAGE_STAIRS_TOTAL_HEIGHT + STAGE_FLOOR_HEIGHT;
//#
//#  		public static final short GIRL_WIDTH = 50;
//#  		public static final byte TOUCH_TOLERANCE = 10;
//#
//#  		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
//#  		public static final short SPOT_SIZE_SMALL	= 75;
//#  		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
//#  		public static final short SPOT_SIZE_BIG		= 120;
//#
//#  		// Velocidade para percorer o menu
//#  		public static final short TIME_GOTO_MENUITEM = 500;
//#
//#  		// Distancia entre o texto "Você ganhou" para o valor do premio
//#  		public static final byte WINSCREEN_TEXT_DISTANCE = 20;
//#
//#  		// Dados do emissor de particulas
//#  		public static final int MIN_WIDTH_PER_PARTICLE = 10;
//#  		public static final int MAX_WIDTH_PER_PARTICLE = 100;
//#  		public static final byte MAX_WIDTH_SPARKEMITTER = 85;
//#
//# 		/** Espaçamento vertical do título. */
//# 		public static final byte TITLE_Y_SPACING = 3;
//#
//# 		/** Offset em y na animação do brilho girando em relação ao logo. */
//# 		public static final byte SPLASH_SHINE_OFFSET_Y = -4;
//#
//# 			/** Altura mínima do pattern de chão da tela de jogo, em pixels. */
//# 			public static final byte FLOOR_HEIGHT = 70;
//#
//#
//#  	//</editor-fold>
//#
//#     //</editor-fold>
	//#elif SCREEN_SIZE == "BIG"

	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE" defaultstate="collapsed">
		public static final short PARTICLES_MAX_CONFETTIS = 400;
		public static final byte PARTICLES_MIN_CONFETTI_SPEED = 30;
		public static final byte PARTICLES_MAX_CONFETTI_SPEED = 60;
		public static final short PARTICLES_MIN_SPARK_SPEED = 200;
		public static final byte PARTICLES_SPARK_SPEED_RANGE = 40;
		public static final int PARTICLES_GRAVITY_SPEED = 16800000;
		public static final short PARTICLES_MAX_SPARKS = 90;
		public static final short PARTICLES_SPARKS_PER_SECOND = 30;

		/** Altura da caixa de texto do Roberto Justus. */
		public static final short TEXTBOX_HEIGHT = 42;
		public static final boolean ABSOLUTE_POSITION_INTERSECTION = false;

			/** Espaçamento entre as entradas do menu. */
			public static final int MENU_SPACING = 6;

		/***/
		public static final byte VALUES_BOX_OFFSET = 3;
		public static final byte VALUES_BOX_WIDTH = 81;
 		public static final byte VALUES_BOX_HEIGHT = 15;

		public static final short VALUES_BOX_TEXT_OFFSET = 5;

		/***/
		public static final byte MENU_LABEL_OFFSET_X = 4;

		/** Largura total da borda colorida, no modo retrato. */
		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_WIDTH = 210;
		/** Altura total da borda colorida, no modo retrato. */
		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_HEIGHT = 357;

		/** Largura total da borda colorida, no modo paisagem. */
		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_WIDTH = 301;
		/** Altura total da borda colorida, no modo paisagem. */
		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_HEIGHT = 257;

		/** Velocidade máxima da movimentação das telas no cenário do jogo. */
		public static final byte SCREEN_ANIMATION_MAX_SPEED = 20;

		/** Aproximação usada para arredondar a posição do pattern animado dos spots, na tela de valores. */
		public static final byte BKG_ANIMATION_ROUND = 14;

		public static final short BANKER_HEART_X = 124;
		public static final short BANKER_SCREENS_Y = 12;
		public static final short BANKER_GRAPH_X = 66;
		public static final short BANKER_HEART_WIDTH = 51;

		public static final short BANKER_PATTERN_TOP_HEIGHT = 260;
		public static final byte BANKER_PATTERN_CENTER_Y = 60;
		public static final byte BANKER_BKG_Y = BANKER_PATTERN_CENTER_Y + 7;

		public static final byte BANKER_BARS_LINE_DENSITY = 3;
		public static final short BANKER_BARS_TOP_WIDTH = 178;

		/***/
		public static final short VALUES_BOX_BORDER_Y_OFFSET = 10;

		public static final byte CASE_CASE_POSITION_X = 5;
		public static final byte CASE_CASE_POSITION_Y = 42;
		public static final byte CASE_GIRL_POSITION_X = 31;
		public static final byte CASE_GIRL_POSITION_Y = 0;
		public static final byte CASE_GIRL_REF_PIXEL_Y = -5;

		public static final short CASE_SMALL_LABEL_Y = 15;
		public static final short CASE_BIG_LABEL_VALUE_Y = 67;
		public static final short CASE_BIG_LABEL_NUMBER_Y = 50;
		public static final short CASE_BIG_LABEL_NUMBER_SMALL_Y = 14;
		public static final short CASE_SMALL_NUMBER_ERROR = -2;

		public static final byte BANKER_CASE_OFFSET_Y = 9;

		public static final byte BKG_SCREEN_SPACING		= 24;
		public static final byte BKG_SCREEN_Y			= 45;
		public static final byte NUMBER_OF_SCREENS		= 5;

		/** Largura total do palco, em pixels. */
		public static final short STAGE_WIDTH = 500;
		public static final byte STAGE_TOP_SPOTS_HEIGHT = 28;
		public static final short STAGE_BKG_BEGIN = STAGE_TOP_SPOTS_HEIGHT;
		public static final byte STAGE_BKG_HORIZONTAL_LINES = 6;
		public static final short STAGE_BKG_HORIZONTAL_LINE_HEIGHT = 26;
		public static final short STAGE_BKG_HEIGHT = STAGE_BKG_HORIZONTAL_LINE_HEIGHT * STAGE_BKG_HORIZONTAL_LINES;
		public static final byte STAGE_BKG_HORIZONTAL_LINE_DENSITY = 2;
		public static final byte STAGE_STAIR_HEIGHT = 20;

		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT1 = 1;
		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT2 = 7;
		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT3 = 1;
		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT4 = 1;
		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT5 = 1;

		public static final short STAGE_STAIR_BEGIN = STAGE_BKG_BEGIN + STAGE_BKG_HEIGHT;
		public static final byte STAGE_STAIR_REAL_HEIGHT = 9;
		public static final byte STAGE_STAIRS = 8;
		public static final short STAGE_STAIRS_TOTAL_HEIGHT = STAGE_STAIR_HEIGHT*STAGE_STAIRS;
		public static final short STAGE_FLOOR_BEGIN = STAGE_STAIR_BEGIN + STAGE_STAIRS_TOTAL_HEIGHT;
		public static final byte STAGE_FLOOR_HEIGHT = 48; /** Altura mínima do pattern de chão da tela de jogo, em pixels. */
		public static final short STAGE_HEIGHT = STAGE_TOP_SPOTS_HEIGHT + STAGE_BKG_HEIGHT + STAGE_STAIRS_TOTAL_HEIGHT + STAGE_FLOOR_HEIGHT;

		public static final short GIRL_WIDTH = 66;
		public static final byte TOUCH_TOLERANCE = 10;

		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
		public static final short SPOT_SIZE_SMALL	= 103;
		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
		public static final short SPOT_SIZE_BIG		= 170;

		// Velocidade para percorer o menu
		public static final short TIME_GOTO_MENUITEM = 500;

		// Distancia entre o texto "Você ganhou" para o valor do premio
		public static final byte WINSCREEN_TEXT_DISTANCE = 20;

		// Dados do emissor de particulas
		public static final int MIN_WIDTH_PER_PARTICLE = 10;
		public static final int MAX_WIDTH_PER_PARTICLE = 100;
		public static final byte MAX_WIDTH_SPARKEMITTER = 85;

		/** Espaçamento vertical do título. */
		public static final byte TITLE_Y_SPACING = 5;

		/** Offset em y na animação do brilho girando em relação ao logo. */
		public static final byte SPLASH_SHINE_OFFSET_Y = -8;

		/** Altura mínima do pattern de chão da tela de jogo, em pixels. */
		public static final byte FLOOR_HEIGHT = 70;

	//</editor-fold>

    //</editor-fold>
	//#elif SCREEN_SIZE == "GIANT"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE" defaultstate="collapsed">
//# 		public static final short PARTICLES_MAX_CONFETTIS = 500;
//# 		public static final byte PARTICLES_MIN_CONFETTI_SPEED = 30;
//# 		public static final byte PARTICLES_MAX_CONFETTI_SPEED = 60;
//# 		public static final short PARTICLES_MIN_SPARK_SPEED = 230;
//# 		public static final byte PARTICLES_SPARK_SPEED_RANGE = 50;
//# 		public static final int PARTICLES_GRAVITY_SPEED = 19800000;
//# 		public static final short PARTICLES_MAX_SPARKS = 120;
//#			public static final short PARTICLES_SPARKS_PER_SECOND = 40;
//#
//# 		/***/
//# 		public static final byte MENU_LABEL_OFFSET_X = 4;
//#
//# 		public static final boolean ABSOLUTE_POSITION_INTERSECTION = true;
//#
//# 			/** Espaçamento entre as entradas do menu. */
//# 			public static final int MENU_SPACING = 8;
//#
//# 		/** Altura da caixa de texto do Roberto Justus. */
//# 		public static final short TEXTBOX_HEIGHT = 51;
//#
//# 		/***/
//# 		public static final byte VALUES_BOX_OFFSET = 5;
//# 		public static final byte VALUES_BOX_WIDTH = 124;
//# 		public static final byte VALUES_BOX_HEIGHT = 21;
//# 		public static final short VALUES_BOX_TEXT_OFFSET = 5;
//#
//# 		/** Largura total da borda colorida, no modo retrato. */
//# 		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_WIDTH = 325;
//# 		/** Altura total da borda colorida, no modo retrato. */
//# 		public static final short VALUES_BOX_PORTRAIT_TOTAL_MAX_HEIGHT = 513;
//#
//# 		/** Largura total da borda colorida, no modo paisagem. */
//# 		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_WIDTH = 464;
//# 		/** Altura total da borda colorida, no modo paisagem. */
//# 		public static final short VALUES_BOX_LANDSCAPE_TOTAL_MAX_HEIGHT = 369;
//#
//# 		/** Velocidade máxima da movimentação das telas no cenário do jogo. */
//# 		public static final byte SCREEN_ANIMATION_MAX_SPEED = 20;
//#
//# 		/** Aproximação usada para arredondar a posição do pattern animado dos spots, na tela de valores. */
//# 		public static final byte BKG_ANIMATION_ROUND = 14;
//#
//# 		public static final short BANKER_HEART_X = 169;
//# 		public static final short BANKER_SCREENS_Y = 15;
//# 		public static final short BANKER_GRAPH_X = 97;
//# 		public static final short BANKER_HEART_WIDTH = 65;
//#
//# 		public static final short BANKER_PATTERN_TOP_HEIGHT = 291;
//# 		public static final byte BANKER_PATTERN_CENTER_Y = 79;
//# 		public static final byte BANKER_BKG_Y = BANKER_PATTERN_CENTER_Y + 11;
//#
//# 		public static final byte BANKER_BARS_LINE_DENSITY = 4;
//# 		public static final short BANKER_BARS_TOP_WIDTH = 244;
//#
//# 		/***/
//# 		public static final short VALUES_BOX_BORDER_Y_OFFSET = 10;
//#
//# 		public static final byte CASE_CASE_POSITION_X = 0;
//# 		public static final byte CASE_CASE_POSITION_Y = 58;
//# 		public static final byte CASE_GIRL_POSITION_X = 31;
//# 		public static final byte CASE_GIRL_POSITION_Y = 0;
//# 		public static final byte CASE_GIRL_REF_PIXEL_Y = -7;
//#
//# 		public static final short CASE_SMALL_LABEL_Y = 18;
//# 		public static final short CASE_BIG_LABEL_VALUE_Y = 99;
//# 		public static final short CASE_BIG_LABEL_NUMBER_Y = 70;
//# 	    public static final short CASE_BIG_LABEL_NUMBER_SMALL_Y = 28; //34
//# 		public static final short CASE_SMALL_NUMBER_ERROR = 0;
//#
//# 		public static final byte BANKER_CASE_OFFSET_Y = 9;
//#
//# 		/** Largura total do palco, em pixels. */
//# 		public static final short STAGE_WIDTH = 960;
//# 		public static final byte STAGE_TOP_SPOTS_HEIGHT = 46;
//# 		public static final short STAGE_BKG_BEGIN = STAGE_TOP_SPOTS_HEIGHT;
//# 		public static final byte STAGE_BKG_HORIZONTAL_LINES = 6;
//# 		public static final short STAGE_BKG_HORIZONTAL_LINE_HEIGHT = 40;
//# 		public static final short STAGE_BKG_HEIGHT = STAGE_BKG_HORIZONTAL_LINE_HEIGHT * STAGE_BKG_HORIZONTAL_LINES;
//# 		public static final byte STAGE_BKG_HORIZONTAL_LINE_DENSITY = 2;
//# 		public static final byte STAGE_STAIR_HEIGHT = 29;
//#
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT1 = 2;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT2 = 9;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT3 = 1;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT4 = 2;
//# 		public static final byte STAGE_STAIR_HORIZONTAL_LINES_HEIGHT5 = 1;
//#
//# 		public static final short STAGE_STAIR_BEGIN = STAGE_BKG_BEGIN + STAGE_BKG_HEIGHT;
//# 		public static final byte STAGE_STAIR_REAL_HEIGHT = 14;
//# 		public static final byte STAGE_STAIRS = 8;
//# 		public static final short STAGE_STAIRS_TOTAL_HEIGHT = STAGE_STAIR_HEIGHT*STAGE_STAIRS;
//# 		public static final short STAGE_FLOOR_BEGIN = STAGE_STAIR_BEGIN + STAGE_STAIRS_TOTAL_HEIGHT;
//# 		public static final byte STAGE_FLOOR_HEIGHT = 58; /** Altura mínima do pattern de chão da tela de jogo, em pixels. */
//# 		public static final short STAGE_HEIGHT = STAGE_TOP_SPOTS_HEIGHT + STAGE_BKG_HEIGHT + STAGE_STAIRS_TOTAL_HEIGHT + STAGE_FLOOR_HEIGHT;
//#
//# 		public static final short GIRL_WIDTH = 99;
//#
//# 		/***/
//# 		public static final byte TOUCH_TOLERANCE = 10;
//#
//# 		public static final byte BKG_SCREEN_SPACING		= 24;
//# 		public static final byte BKG_SCREEN_Y			= 70;
//# 		public static final byte NUMBER_OF_SCREENS		= 7;
//#
//# 		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
//# 		public static final short SPOT_SIZE_SMALL	= 141;
//#
//# 		/** Dimensões do spot de luz da tela de jogo. Deve ser ímpar para casar com o fundo. */
//# 		public static final short SPOT_SIZE_BIG		= 241;
//#
//# 		// Velocidade para percorer o menu
//# 		public static final short TIME_GOTO_MENUITEM = 500;
//#
//# 		// Distancia entre o texto "Você ganhou" para o valor do premio
//# 		public static final byte WINSCREEN_TEXT_DISTANCE = 20;
//#
//# 		// Dados do emissor de particulas
//# 		public static final int MIN_WIDTH_PER_PARTICLE = 10;
//# 		public static final int MAX_WIDTH_PER_PARTICLE = 100;
//# 		public static final byte MAX_WIDTH_SPARKEMITTER = 115;
//#
//# 			/** Espaçamento vertical do título. */
//# 		public static final byte TITLE_Y_SPACING = 7;
//#
//# 		/** Offset em y na animação do brilho girando em relação ao logo. */
//# 		public static final byte SPLASH_SHINE_OFFSET_Y = -14;
//#
//#
//# 	//</editor-fold>
//#
//#
	//#endif

	public static final int WIDTH_PER_PARTICLE_RANGE = MAX_WIDTH_PER_PARTICLE - MIN_WIDTH_PER_PARTICLE;

}
