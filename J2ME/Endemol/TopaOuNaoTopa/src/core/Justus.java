/**
 * Justus.java
 *
 * Created on Aug 24, 2010 3:53:05 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import java.util.Stack;
import java.util.Vector;
import screens.GameMIDlet;
import screens.GameScreen;

/**
 *
 * @author peter
 */
public final class Justus extends UpdatableGroup implements Constants {

	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;

	public static final byte EXPRESSION_FELIZ = 0;
	public static final byte EXPRESSION_NORMAL = 1;
	public static final byte EXPRESSION_CELULAR = 2;
	public static final byte EXPRESSION_CELULAR_ASSUSTADO = 3;
	public static final byte EXPRESSION_VOCE = 4;
	public static final byte EXPRESSION_CALMA = 5;
	public static final byte EXPRESSION_QUEQUEISSO = 6;
	public static final byte EXPRESSION_REZANDO = 7;

	protected byte state;

	/** Duração da transição (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 620;

	/** Tempo mínimo que uma mensagem deve ser exibida antes de poder ser passada. */
	private static final short TIME_MESSAGE_MIN = 500;

	private long timeLastMessageShown;

	protected int accTime;

	private DrawableImage justus;
	private byte expression;
	
	private final RichLabel label;

	private int textTotalHeight;

	private final DrawableGroup textBox;

	private final DrawableImage boxLeft;
	private final DrawableImage boxRight;
	private final Pattern boxFill;
	private final UpdatableGroup labelGroup;

	/** "Tweener" da animação dos elementos na tela. Os pontos x são usados para a posição do Justus, e os pontos
	 * y para a posição da caixa de texto.
	 */
	private final BezierCurve tweener = new BezierCurve();

	private final Vector messages = new Vector();

	private final short textLimitBottom;
	private short textLimitTop;

	private static final byte SCROLL_STATE_NONE				= 0;
	private static final byte SCROLL_STATE_SCROLLING_UP		= 1;
	private static final byte SCROLL_STATE_SCROLLING_DOWN	= 2;
	private static final byte SCROLL_STATE_WAITING			= 3;

	private static final byte TIME_PER_CHAR	= 33;
	private int textAccTime;
    private String messageToShow;
    private boolean appearingMessage;

	//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 		private static final short SCROLL_WAIT_TIME = 1300;
	//#else
		private static final short SCROLL_WAIT_TIME = 1650;
	//#endif

	private byte scrollState;
	private short scrollAccTime;
	private final MUV scrollSpeed;

	/** Evita acessos indevidos à imagem do Justus durante as trocas de imagem. */
	private final Mutex mutex = new Mutex();

	private final Rectangle screenViewport;
	

	public Justus( Rectangle screenViewport ) throws Exception {
		super( 5 );

		this.screenViewport = screenViewport;
		
		textBox = new DrawableGroup( 5 );
		insertDrawable( textBox );

		boxLeft = new DrawableImage( PATH_IMAGES + "box_left.png" );
		textBox.insertDrawable( boxLeft );

		boxFill = new Pattern( new DrawableImage( PATH_IMAGES + "box_fill.png" ) );
		boxFill.setPosition( boxLeft.getWidth(), 0 );
		textBox.insertDrawable( boxFill );

		boxRight = new DrawableImage( boxLeft );
		boxRight.mirror( TRANS_MIRROR_H );
		textBox.insertDrawable( boxRight );

		labelGroup = new UpdatableGroup( 1 );
		labelGroup.setPosition( boxFill.getPosX() - 1, boxFill.getPosY() + 2 );
		textBox.insertDrawable( labelGroup );

		label = new RichLabel( FONT_TEXT, null, 0 );
		labelGroup.insertDrawable( label );

		textLimitBottom = 0;

		// em telas menores, é melhor o scroll ser mais rápido por causa do espaço reduzido
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			scrollSpeed = new MUV( label.getFont().getHeight() );
		//#else
			scrollSpeed = new MUV( label.getFont().getHeight() * 3 / 4 );
		//#endif

		justus = loadJustus(EXPRESSION_NORMAL);
		expression = EXPRESSION_NORMAL;
		insertDrawable( justus );

        appearingMessage = false;
        textAccTime = 0;

		setVisible(false);

		setState( STATE_HIDDEN );
	}

	
	private final DrawableImage loadJustus( byte expression ) throws Exception {
		return (new DrawableImage( PATH_IMAGES + "justus" + expression + ".png" ));
	}


	/**
	 * Troca a expressão do Justus.
	 */
	public final boolean changeExpression( byte expression ) {
		if( expression != this.expression ) {
			mutex.acquire();
			try {
				removeDrawable(justus);
				justus = null;
				justus = loadJustus(expression);
                justus.setPosition(getWidth() - justus.getWidth() , getHeight() - justus.getHeight() );
				tweener.control1.x = justus.getPosX();
				tweener.control2.x = tweener.control1.x;
				tweener.destiny.x = tweener.control1.x;
				refresh(false);
				insertDrawable(justus);
				this.expression = expression;

				autoSizeText();
			} catch( Exception ex ) {
				//#if DEBUG == "true"
					GameMIDlet.log( ex + ": " + ex.getMessage() );
					ex.printStackTrace();
				//#endif
				
				return false;
			} finally {
				mutex.release();
			}
		}
		return true;
	}

	
	public final boolean isHiding() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_HIDING:
				return true;

			default:
				return false;
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		mutex.acquire();
		textBox.setSize( width, TEXTBOX_HEIGHT );

		boxRight.setPosition( width - boxRight.getWidth(), 0 );
		boxFill.setSize( boxRight.getPosX() - boxFill.getPosX(), boxFill.getHeight() );

		label.setSize( width - justus.getWidth() - labelGroup.getPosX() + 2, textBox.getHeight() );
		labelGroup.setSize( label.getWidth(), boxFill.getHeight() - 2 );

		justus.setPosition( 0, height - justus.getHeight() );

		final int START_X = width;
		final int FINAL_X = width - justus.getWidth();

		mutex.release();

		final int START_Y = height;
		final int FINAL_Y = height - textBox.getHeight();

		tweener.origin.set( START_X, START_Y );
		tweener.control1.set( FINAL_X - ( ( FINAL_X - START_X ) >> 1 ), FINAL_Y - ( ( FINAL_Y - START_Y ) * 2 / 3 ) );
		tweener.control2.set( FINAL_X, FINAL_Y );
		tweener.destiny.set( FINAL_X, FINAL_Y );

		refresh();

		if ( messageToShow != null )
			setText( messageToShow );
	}


	public final int getBoxHeight() {
		return TEXTBOX_HEIGHT;
	}

	public final int getJustusWidth() {
		return justus.getHeight();
	}


	public final byte getState() {
		return state;
	}
	

	public final void hide() {
		if(!isHiding())
			setState(STATE_HIDING);
	}


	public final void show() {
		if(isHiding())
			setState(STATE_APPEARING);
	}

	
	private final void setState( int state ) {
		setState( state, true );

	}


	/**
	 * 
	 * @param state
	 * @param useMutex
	 */
	private final void setState( int state, final boolean useMutex ) {
		try {
			if ( useMutex )
				mutex.acquire();
			
			if ( state == this.state )
				return;

			switch ( state ) {
				case STATE_HIDDEN:
					setVisible( false );
					accTime = 0;
					refresh( false );
				break;

				case STATE_APPEARING:
					if ( this.state == STATE_SHOWN )
						return;

					setVisible( true );
				break;

				case STATE_SHOWN:
					setVisible( true );
					accTime = TRANSITION_TIME;
					refresh( false );
				break;

				case STATE_HIDING:
					if ( isHiding() )
						return;

					setVisible( true );
				break;
			}

			this.state = ( byte ) state;
		} finally {
			if ( useMutex )
				mutex.release();
		}
	}


	public final int getRemainingMessagesCount() {
		return messages.size();
	}


	public final boolean isLastMessageShown( boolean considerMinTime ) {
		return messages.size() <= 0 && ( !considerMinTime || ( isTimeElapsed() && !appearingMessage ) );
	}
	

	public final void update( int delta ) {
		super.update( delta );

		mutex.acquire();

		
		switch ( state ) {
			case STATE_APPEARING:
				accTime += delta;
				refresh( false );

				if ( accTime >= TRANSITION_TIME )
					setState( STATE_SHOWN, false );
			break;

			case STATE_SHOWN:
				switch ( scrollState ) {
					case SCROLL_STATE_SCROLLING_DOWN:
						label.move( 0, scrollSpeed.updateInt( delta ) );
						if ( label.getPosY() >= textLimitBottom ) {
							label.setPosition( label.getPosX(), textLimitBottom );
							scrollState = SCROLL_STATE_WAITING;
						}
					break;

					case SCROLL_STATE_SCROLLING_UP:
						label.move( 0, -scrollSpeed.updateInt( delta ) );
						if ( label.getPosY() <= textLimitTop ) {
							label.setPosition( label.getPosX(), textLimitTop );
							scrollState = SCROLL_STATE_WAITING;
						}
					break;

					case SCROLL_STATE_WAITING:
						scrollAccTime += delta;
						if ( scrollAccTime >= SCROLL_WAIT_TIME ) {
							scrollAccTime = 0;
							scrollState = ( label.getPosY() >= textLimitBottom ? SCROLL_STATE_SCROLLING_UP : SCROLL_STATE_SCROLLING_DOWN );
						}
					break;
				}
			break;

			case STATE_HIDING:
				accTime -= delta;
				refresh( false );

				if ( accTime <= 0 )
					setState( STATE_HIDDEN, false );
			break;
		}

        if(appearingMessage) {
            textAccTime += delta;
            if(textAccTime/TIME_PER_CHAR >= messageToShow.length()) {
                appearingMessage = false;
                setLabelText(messageToShow);
            }
            else
				setLabelText(messageToShow.substring(0,textAccTime/TIME_PER_CHAR));
        }

		mutex.release();
	}


	private final void refresh() {
		refresh(true);
	}


	private final void refresh(boolean useMutex) {
		final int fp_progress = NanoMath.clamp( NanoMath.divInt( accTime, TRANSITION_TIME ), 0, NanoMath.ONE );
		final Point p = new Point();

		screenViewport.y = Math.min( p.y, screenViewport.y );

		tweener.getPointAtFixed( p, fp_progress );
		textBox.setPosition( textBox.getPosX(), p.y );

		if(useMutex)
			mutex.acquire();

		justus.setPosition( p.x, justus.getPosY() );

		if(useMutex)
			mutex.release();
	}


	private final void setLabelText( String text) {
		label.setText( text );
    }
	

	private final void setText( String text ) {
        messageToShow = text;
        appearingMessage = true;
        textAccTime = 0;

		label.setText( text );
		textTotalHeight = label.getTextTotalHeight();
		autoSizeText();
		label.setText( "" );

        timeLastMessageShown = System.currentTimeMillis();
	}


	private final void autoSizeText() {
		// +2 porque em telas menores qualquer pixel é importante...
		label.setSize( getWidth() - justus.getWidth() - labelGroup.getPosX() + 2, textBox.getHeight() );
		labelGroup.setSize( label.getWidth(), labelGroup.getHeight() );
		
        textLimitTop = ( short ) Math.min( -textTotalHeight + textBox.getHeight() - textLimitBottom - 4, textLimitBottom );
        label.setPosition( label.getPosX(), textLimitBottom );
		label.setSize( label.getWidth(), textTotalHeight );

		if ( textLimitTop == textLimitBottom )
            scrollState = SCROLL_STATE_NONE;
        else
            scrollState = SCROLL_STATE_WAITING;
	}


	private final boolean isTimeElapsed() {
		return System.currentTimeMillis() - timeLastMessageShown >= TIME_MESSAGE_MIN;
	}


	public final void appendMessage( int textIndex ) {
		appendMessage( AppMIDlet.getText( textIndex ) );
	}


	public final void appendMessage( String message ) {
		messages.addElement( message );
	}


	public final void clearMessages( boolean clearCurrent ) {
		messages.removeAllElements();
		if ( clearCurrent ) {
			setText( "" );
		}
	}


	/**
	 * Mostra a próxima mensagem.
	 */
	public final void showNextMessage() {
		showNextMessage( false );
	}


	/**
	 * Mostra a próxima mensagem.
	 * @param force força a troca de mensagem, mesmo que a atual não tenha sido exibida pelo tempo mínimo.
	 */
	public final void showNextMessage( boolean force ) {
		mutex.acquire();
		if ( isTimeElapsed() || force ) {
			if ( appearingMessage && !force ) {
				textAccTime = Short.MAX_VALUE;
			} else if ( messages.size() > 0 ) {
				setText( ( String ) messages.elementAt( 0 ) );
				messages.removeElementAt( 0 );
			}
		}
		mutex.release();
	}


	//#if TOUCH == "true"
		public final boolean contains( int x, int y ) {
			return textBox.contains( x, y ) || justus.contains( x, y );
		}
	//#endif
	
}
