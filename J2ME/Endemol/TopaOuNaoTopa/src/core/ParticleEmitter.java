/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author caiolima
 */
public abstract class ParticleEmitter extends UpdatableGroup implements Constants {
	// <editor-fold defaultstate="collapsed" desc="Constantes Estáticas">
	/** Aceleração da gravidade. */
	private static final int FP_HALF_GRAVITY_ACC = PARTICLES_GRAVITY_SPEED >> 1;

    private final Sprite spriteParticle;
	private short activeParticles;
	// </editor-fold>   

	private final short MAX_PARTICLES;
	private final short NUMBER_OF_PARTICLES;
	private final Particle[] particles;
    private boolean isActive;
    private final boolean freeFall;

    private int accTime;
    private int timeBetweenParticles;

	public ParticleEmitter(int particlesPerSecond, boolean freeFall, Sprite sprt, int width, int height, int numberOfParticles, short maxParticles) throws Exception {
        super(1);
		setSize( width, height );

		MAX_PARTICLES = maxParticles;
		particles = new Particle[ MAX_PARTICLES ];

		NUMBER_OF_PARTICLES = (short)numberOfParticles;

        setParticlesPerSecond(particlesPerSecond);
        isActive = true;

        spriteParticle = sprt;
		for ( short i = 0; i < MAX_PARTICLES; ++i ) {
			particles[ i ] = new Particle(this);
		}
        //sort();

        this.freeFall = freeFall;
	}

	public final void setParticlesPerSecond(int nOfParticles) {
		if(nOfParticles!=0) timeBetweenParticles = 1000/nOfParticles;
		else timeBetweenParticles = 1000;
	}

	public final void update( int delta ) {
        super.update(delta);

        if(isActive) {
            accTime += delta;
            if(accTime>=timeBetweenParticles) {
				byte numberParticles = (byte)(accTime/timeBetweenParticles); // Calcula o numero de particulas a emitir
                accTime -= numberParticles*timeBetweenParticles; // Retira do tempo acumulado o intervalo entre n particulas
                while(numberParticles>0) { // Emite o numero de particulas desejadas
                    emit(); 
                    numberParticles--;
                }
            }
        }

//		final short previousActiveParticles = activeParticles;
		final int FP_DELTA = NanoMath.divInt( delta, 1000 );
		int i = 0;
		while (  i < activeParticles ) {
			particles[ i ].update( FP_DELTA );
			if(particles[ i ].visible) { ++i; }						// Se continuar visivel passa para proxima posição
			else {													// Senão
				--activeParticles;									// Decresce o número de particulas
				//particles[ i ] = particles[ activeParticles ];
				particles[ i ].set(particles[ activeParticles ]);	// E copia para posição atual a ultima particula do vetor
			}
		}

//		if ( previousActiveParticles != activeParticles )
//			sort();
	}

	public final void reset() {
		activeParticles = 0;
	}

	public void turnOff() {
		isActive = false;
	}
	public void turnOn() {
		isActive = true;
	}

//	private final void sort() {
//		short i = 0;
//		short j = ( short ) ( MAX_PARTICLES - 1 );
//		for ( ; i < MAX_PARTICLES; ++i ) {
//			if ( !particles[ i ].visible ) {
//				for ( ; j > i; --j ) {
//					if ( particles[ j ].visible ) {
//						final Particle temp = particles[ i ];
//						particles[ i ] = particles[ j ];
//						particles[ j ] = temp;
//						break;
//					}
//				}
//			}
//		}
//		activeParticles = j;
//	}

    public abstract Point RandSpeed();
    public abstract Point RandPosition();
    public abstract int RandLifeTime();

	public final void emit() {
	     if(activeParticles < (MAX_PARTICLES-1))
             particles[ activeParticles++ ].born(RandPosition(), RandSpeed(), RandLifeTime() );
	}
    
    public void draw(Graphics g) {

		if ( visible ) {
			if ( clipTest ) {
				pushClip( g );
				translate.addEquals( position );

				final Rectangle clip = clipStack[ currentStackSize ];
				clip.setIntersection( translate.x, translate.y, size.x, size.y );

				if ( clip.width > 0 && clip.height > 0 ) {
					g.setClip( clip.x, clip.y, clip.width, clip.height );

					paint( g );
					for ( short i = 0; i < activeParticles; ++i ) {
						particles[ i ].draw( g );
					}
				}

				translate.subEquals( position );
				popClip( g );
			} else {
				// desenha direto, ignorando o teste de interseção
				translate.addEquals( position );
				paint( g );
				translate.subEquals( position );
			}
		}
    }

	/**
	 * Classe interna que descreve uma part�cula de fogos de artif�cio.
	 */
	private static final class Particle {

		/** Tempo acumulado da part�cula. */
		private int fp_accTime;

		private int fp_speedX;
		private int fp_speedY;

		private final Point position = new Point();
		private final Point initialPos = new Point();

		private final ParticleEmitter parent;

        private static final int CYCLES_PER_SECOND = 3;
        private static final int MILISECONS_PER_CYCLES = 1000/CYCLES_PER_SECOND;

		private boolean visible;

		//private int radius;
		private int halfRadius;
		private byte seed;

        private int frameMod;
        private int lifeTime;

        private Particle(ParticleEmitter parent) {
            this.parent = parent;
            visible = false;
        }

		private final void set(Particle p) {
			fp_accTime = p.fp_accTime;
            frameMod = p.frameMod;

			fp_speedX = p.fp_speedX;
            fp_speedY = p.fp_speedY;
			visible = p.visible;
			seed = p.seed;
			lifeTime = p.lifeTime;

            halfRadius = p.halfRadius;//(byte)(NanoMath.toInt(fp_speedY) >> 1);

			initialPos.set( p.initialPos );
			
			updatePosition();
		}

		private final void born( Point bornPosition, Point bornSpeed, int LifeTime ) { 
			fp_accTime = 0;
            frameMod = 0;
			lifeTime = LifeTime;
            
			fp_speedX = bornSpeed.x;
            fp_speedY = bornSpeed.y;
			visible = true;
			seed = (byte)NanoMath.randInt(360);

            halfRadius = (byte)(NanoMath.toInt(fp_speedY) >> 1);

			//#if JAR == "min"
//# 		initialPos.set( bornPosition.sub( 2, 1 ) );
			//#else
			initialPos.set( bornPosition.sub( parent.spriteParticle.getSize().div( 2 ) ) );
			//#endif
			updatePosition();
		}

		public final void updatePosition() {
			if(parent.freeFall) {
				position.set( initialPos.x + NanoMath.toInt( NanoMath.mulFixed( fp_speedX, fp_accTime ) ),
						  initialPos.y + NanoMath.toInt( NanoMath.mulFixed( fp_speedY, fp_accTime ) + NanoMath.mulFixed( FP_HALF_GRAVITY_ACC, NanoMath.mulFixed( fp_accTime, fp_accTime ) ) ) );
			}
			else {
				position.set( initialPos.x + NanoMath.toInt(halfRadius*NanoMath.cosInt(seed+(fp_accTime/MILISECONS_PER_CYCLES))),
						  initialPos.y + NanoMath.toInt( NanoMath.mulFixed( fp_speedY, fp_accTime ) ) );
			}
		}

		public final void updateVisibility() {
			if(parent.freeFall) {
				if ( position.x <= 0 || position.x >= parent.getWidth() || position.y >= parent.getHeight() ) {
					visible = false;
				}
				if(fp_accTime > lifeTime) visible = false;
				else frameMod = NanoMath.min(NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed(fp_accTime,fp_accTime) , NanoMath.mulFixed(lifeTime,lifeTime) ) * parent.NUMBER_OF_PARTICLES ) , (parent.NUMBER_OF_PARTICLES - 1));
			}
			else {
				if ( position.y >= parent.getHeight() ) {
					visible = false;
                } 
				frameMod = NanoMath.sinInt(seed+(fp_accTime/MILISECONS_PER_CYCLES));
				if(frameMod>0)
					frameMod = 2;
				else {
					if(frameMod < -50000) frameMod = 1;
					else frameMod = 0;
				}	
			}
		}

		public final void update( int fp_delta ) {
			fp_accTime += fp_delta;

			updatePosition();
			updateVisibility();
		}

		public final void draw( Graphics g ) {
			if ( visible ) {
				//#if JAR == "min"
//# 					g.setColor( COLOR_DROP );
//# 					g.fillRect( translate.x + position.x, translate.y + position.y, 3, 2 );
				//#else
					parent.spriteParticle.setFrame(frameMod);
					parent.spriteParticle.setPosition( position );
					parent.spriteParticle.draw( g );
				//#endif
			} // fim if ( visible )
		}
	}
}