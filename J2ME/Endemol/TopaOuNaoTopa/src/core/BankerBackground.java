/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author caiolima
 */
public class BankerBackground extends UpdatableGroup implements Constants {

	private Pattern patternTop;
	private Pattern patternCenter;
	private Pattern patternBottom1;
	private Pattern patternBottom2;
	
	private DrawableImage bkg;
	private BasicAnimatedPattern heart;
	private Sprite banker;
	private Sprite graph;

	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
 		private DrawableGroup vault;
	//#endif

    private final Point[] lv = new Point[LINHAS_VERTICAIS];
    private final Point[] ph1 = new Point[LINHAS_HORIZONTAIS];
    private final Point[] ph2 = new Point[LINHAS_HORIZONTAIS];

	private static final int BAR_COLOR = 0x000000;
	private static final int BAR_LIGHT_COLOR_1 = 0x6b3b43;
	private static final int BAR_LIGHT_COLOR_2 = 0x57262e;
	private static final int BAR_LIGHT_COLOR_3 = 0x260006;

	private static final byte LINHAS_VERTICAIS = 4;
	private static final byte LINHAS_HORIZONTAIS = 4;
	
	private boolean bankerIsVisible;

	public BankerBackground() throws Exception {
		super(9);

		if ( GameMIDlet.isLowMemory() )
			patternTop = new Pattern( 0x7b5e64 );
		else
			patternTop = new Pattern( new DrawableImage( PATH_BANKER + "pattern_top.png" ) );
		
		patternTop.setSize( Short.MAX_VALUE, BANKER_PATTERN_TOP_HEIGHT );
		insertDrawable( patternTop );
		
		bankerIsVisible = false;
		showBanker();
		
        for(int i = 0; i < LINHAS_VERTICAIS ; ++i ) {
			lv[i] = new Point();
		}
        for(int i = 0; i < LINHAS_HORIZONTAIS ; ++i ) {
			ph1[i] = new Point();
			ph2[i] = new Point();
		}
	}

	public void setSize(int width, int height) {
		super.setSize(width, height);

		if(bankerIsVisible) {
			bkg.setRefPixelPosition( width >> 1, BANKER_BKG_Y );

			patternBottom2.setSize( Short.MAX_VALUE, height - patternBottom2.getPosY() );

			//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
			vault.setRefPixelPosition( width >> 1, patternBottom2.getPosY() + ( patternBottom2.getFill().getHeight() >> 1 ) );
			//#endif

			heart.setPosition( bkg.getPosX() + BANKER_HEART_X, bkg.getPosY() + BANKER_SCREENS_Y + ( ( graph.getHeight() - heart.getHeight() ) >> 1 ) );
			graph.setPosition( bkg.getPosX() + BANKER_GRAPH_X, bkg.getPosY() + BANKER_SCREENS_Y );

			banker.setRefPixelPosition( bkg.getRefPixelX(), bkg.getPosY() + bkg.getHeight() - 1 );

			for(int i = 0; i < LINHAS_VERTICAIS ; ++i ) {
				lv[i].set(bkg.getPosX() + ((bkg.getWidth() - BANKER_BARS_TOP_WIDTH)>>1)+(((i+1)*BANKER_BARS_TOP_WIDTH)/(LINHAS_VERTICAIS+1)) - (BANKER_BARS_LINE_DENSITY>>1), bkg.getPosY() - 1 );
			}

			if(LINHAS_HORIZONTAIS>0) {
				ph1[0].set( bkg.getPosX() + ((bkg.getWidth() - BANKER_BARS_TOP_WIDTH)>>1), bkg.getPosY() );
				ph2[0].set( bkg.getPosX() + ((bkg.getWidth() - BANKER_BARS_TOP_WIDTH)>>1) + BANKER_BARS_TOP_WIDTH, ph1[0].y );
			}
			for(int i = 1; i < LINHAS_HORIZONTAIS ; ++i ) {
				final int y = (bkg.getHeight()>>1) + (((i-1)*(bkg.getHeight()>>1))/(LINHAS_HORIZONTAIS-1));
				ph1[i].set( NanoMath.lerpInt( ph1[0].x , bkg.getPosX() , (y*100)/bkg.getHeight()  ) , bkg.getPosY() + y ); // ( ph1[0].x * ( bkg.getHeight() - y)  ) + ( bkg.getPosX() * y ) ) / bkg.getWidth() , bkg.getPosY() + y );
				ph2[i].set( NanoMath.lerpInt( ph2[0].x , (bkg.getPosX()+bkg.getWidth()) , (y*100)/bkg.getHeight()  ) , bkg.getPosY() + y ); //( ( ph2[0].x * ( bkg.getHeight() - y)  ) + ( ( bkg.getPosX() + bkg.getWidth() ) * y ) ) / bkg.getWidth() , bkg.getPosY() + y ) ;
			}
		}
	}
	
	public final void hideBanker() {
		bankerIsVisible = false;
		
		removeDrawable(patternCenter);
		removeDrawable(patternBottom1);
		removeDrawable(patternBottom2);
		removeDrawable(bkg);
		removeDrawable(graph);
		removeDrawable(heart);
		removeDrawable(banker);

		patternCenter = null;
		patternBottom1 = null;
		patternBottom2 = null;
		bkg = null;
		graph = null;
		heart.setFill(null);
		heart = null;
		banker = null;

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
			vault.removeAllDrawables();
			removeDrawable(vault);
			vault = null;
		//#endif
	}
	
	public final void showBanker() {
		if(!bankerIsVisible) {
			try {
				bankerIsVisible = true;

				patternCenter = new Pattern( new DrawableImage( PATH_BANKER + "pattern_center.png" ) );
				patternCenter.setSize( Short.MAX_VALUE, patternCenter.getFill().getHeight() );
				patternCenter.setPosition( 0, BANKER_PATTERN_CENTER_Y );
				insertDrawable( patternCenter );

				patternBottom1 = new Pattern( new DrawableImage( PATH_BANKER + "pattern_bottom1.png" ) );
				patternBottom1.setSize( Short.MAX_VALUE, patternBottom1.getFill().getHeight() );
				patternBottom1.setPosition( 0, patternTop.getPosY() + patternTop.getHeight() );
				insertDrawable( patternBottom1 );

				patternBottom2 = new Pattern( new DrawableImage( PATH_BANKER + "pattern_bottom2.png" ) );
				patternBottom2.setPosition( 0, patternBottom1.getPosY() + patternBottom1.getHeight() );
				insertDrawable( patternBottom2 );

				bkg = new DrawableImage( PATH_BANKER + "bkg.png" );
				bkg.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
				insertDrawable( bkg );

				graph = new Sprite( PATH_BANKER + "graph" );
				graph.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
				insertDrawable( graph );

				final DrawableImage heartImage = new DrawableImage( PATH_BANKER + "heart.png" );
				heart = new BasicAnimatedPattern( heartImage, heartImage.getWidth() >> 1, 0 );
				heart.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
				heart.setSize( BANKER_HEART_WIDTH, heart.getFill().getHeight() );
				insertDrawable( heart );

				banker = new Sprite( PATH_BANKER + "banker" );
				banker.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
				insertDrawable( banker );

				//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "GIANT"
					DrawableImage vaultImage = new DrawableImage( PATH_BANKER + "vault.png" );
					vault = new DrawableGroup( 4 );
					vault.setSize( vaultImage.getWidth() << 1, vaultImage.getHeight() << 1 );
					vault.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
					insertDrawable( vault );
					// lado esquerdo superior
					vault.insertDrawable( vaultImage );

					// lado direito superior
					vaultImage = new DrawableImage( vaultImage );
					vaultImage.mirror( TRANS_MIRROR_H );
					vaultImage.setPosition( vaultImage.getWidth(), 0 );
					vault.insertDrawable( vaultImage );

					// lado direito inferior
					vaultImage = new DrawableImage( vaultImage );
					vaultImage.mirror( TRANS_MIRROR_H | TRANS_MIRROR_V );
					vaultImage.setPosition( vaultImage.getSize() );
					vault.insertDrawable( vaultImage );

					// lado esquerdo inferior
					vaultImage = new DrawableImage( vaultImage );
					vaultImage.mirror( TRANS_MIRROR_V );
					vaultImage.setPosition( 0, vaultImage.getHeight() );
					vault.insertDrawable( vaultImage );
				//#endif
			}
			catch(Throwable e) {
				//#if DEBUG == "true"
				GameMIDlet.log( e.getClass() + " -> " + e.getMessage() );
				e.printStackTrace();
				//#endif
				hideBanker();
			}
		}
	}

    public final void paint(Graphics g) {
        super.paint(g);

		if(bankerIsVisible) {
			for(int i = 0; i < LINHAS_HORIZONTAIS ; ++i ) {
				g.setColor( BAR_COLOR );
				g.fillRect(translate.x + ph1[i].x, translate.y + ph1[i].y -(BANKER_BARS_LINE_DENSITY>>1), (ph2[i].x - ph1[i].x), Math.max( 1, (BANKER_BARS_LINE_DENSITY>>1) ));

				if(BANKER_BARS_LINE_DENSITY>1) {
					g.setColor( BAR_LIGHT_COLOR_3 );
					g.drawLine( translate.x + ph1[i].x, translate.y + ph1[i].y, translate.x + ph2[i].x-1, translate.y + ph2[i].y );
					if(BANKER_BARS_LINE_DENSITY>3) {
						g.setColor(BAR_LIGHT_COLOR_2);
						g.drawLine( translate.x + ph1[i].x, translate.y + ph1[i].y + 1, translate.x + ph2[i].x-1, translate.y + ph2[i].y + 1 );
						if(BANKER_BARS_LINE_DENSITY>5) {
							g.setColor(BAR_LIGHT_COLOR_1);
							g.fillRect( translate.x + ph1[i].x, translate.y + ph1[i].y + 2, (ph2[i].x - ph1[i].x), (BANKER_BARS_LINE_DENSITY>>1)-2 );
						}
					}
				}
			}

			g.setColor( BAR_COLOR );
			for(int i = 0; i < LINHAS_VERTICAIS ; ++i ) {
				g.fillRect(translate.x + lv[i].x, translate.y + lv[i].y, BANKER_BARS_LINE_DENSITY, (bkg.getHeight()));
			}
		}
	}
}
