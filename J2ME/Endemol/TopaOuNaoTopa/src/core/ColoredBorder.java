/**
 * Border.java
 * 
 * Created on Mar 2, 2009, 2:33:55 PM
 *
 */
//#if SCREEN_SIZE != "SMALL"
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;

/**
 *
 * @author Peter
 */
public final class ColoredBorder extends DrawableGroup implements Constants {

	private final Pattern patternLeft;
	private final Pattern patternRight;
	private final Pattern patternTopR;
	private final Pattern patternBottomR;
	private final Pattern patternTopL;
	private final Pattern patternBottomL;

	private final DrawableImage topLeft;
	private final DrawableImage top;
	private final DrawableImage bottom;
	private final DrawableImage topRight;
	private final DrawableImage bottomLeft;
	private final DrawableImage bottomRight;

	private static DrawableImage TOP_LEFT;
	private static DrawableImage TOP_LEFT_2;
	private static DrawableImage TOP_RIGHT;
	private static DrawableImage TOP;
	private static DrawableImage LEFT;
	private static DrawableImage RIGHT;
	private static DrawableImage TOP_RIGHT_2;
	private static DrawableImage BOTTOM_LEFT;
	private static DrawableImage BOTTOM_RIGHT;

	private Drawable fill;


	public ColoredBorder() throws Exception {
		super( 13 );

		if ( TOP_LEFT == null ) {
			LEFT = new DrawableImage( PATH_BORDER + "left.png" );
			RIGHT = new DrawableImage( PATH_BORDER + "right.png" );
			TOP_RIGHT_2 = new DrawableImage( PATH_BORDER + "h_2.png" );
			TOP_LEFT_2 = new DrawableImage( PATH_BORDER + "h_0.png" );
			TOP = new DrawableImage( PATH_BORDER + "h_1.png" );
			TOP_LEFT = new DrawableImage( PATH_BORDER + "tl.png" );
			BOTTOM_LEFT = new DrawableImage( PATH_BORDER + "bl.png" );
			TOP_RIGHT = new DrawableImage( PATH_BORDER + "tr.png" );
			BOTTOM_RIGHT = new DrawableImage( PATH_BORDER + "br.png" );
		}

		patternLeft = new Pattern( new DrawableImage( LEFT ) );
		patternLeft.setSize( patternLeft.getFill().getWidth(), 0 );
		insertDrawable( patternLeft );

		patternTopR = new Pattern( new DrawableImage( TOP_RIGHT_2 ) );
		patternTopR.setSize( 0, patternTopR.getFill().getHeight() );
		insertDrawable( patternTopR );

		patternRight = new Pattern( new DrawableImage( RIGHT ) );
		patternRight.setSize( patternRight.getFill().getWidth(), 0 );
		insertDrawable( patternRight );

		patternTopL = new Pattern( new DrawableImage( TOP_LEFT_2 ) );
		patternTopL.setSize( 0, patternTopL.getFill().getHeight() );
		insertDrawable( patternTopL );

		patternBottomR = new Pattern( new DrawableImage( ( DrawableImage ) patternTopR.getFill() ) );
		patternBottomR.setSize( 0, patternBottomR.getFill().getHeight() );
		insertDrawable( patternBottomR );

		patternBottomL = new Pattern( new DrawableImage( ( DrawableImage ) patternTopL.getFill() ) );
		patternBottomL.setSize( 0, patternBottomL.getFill().getHeight() );
		insertDrawable( patternBottomL );

		top = new DrawableImage( TOP);
		top.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( top );

		bottom = new DrawableImage( top );
		bottom.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		insertDrawable( bottom );

		topLeft = new DrawableImage( TOP_LEFT );
		insertDrawable( topLeft );

		bottomLeft = new DrawableImage( BOTTOM_LEFT );
		bottomLeft.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		insertDrawable( bottomLeft );
		
		topRight = new DrawableImage( TOP_RIGHT );
		topRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_TOP );
		insertDrawable( topRight);

		bottomRight = new DrawableImage( BOTTOM_RIGHT );
		bottomRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_BOTTOM );
		insertDrawable( bottomRight );

		patternTopR.setPosition( top.getPosX() + top.getWidth(), 0 );
		patternLeft.setPosition( 0, topLeft.getHeight() );
	}


	public final void setFill( Drawable fill ) {
		removeDrawable( this.fill );

		this.fill = fill;

		if ( fill != null ) {
			insertDrawable( fill, 0 );
			refreshFill();
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		bottomLeft.setRefPixelPosition( 0, height );
		topRight.setRefPixelPosition( width, 0 );
		bottomRight.setRefPixelPosition( width, height );
		
		bottom.setRefPixelPosition( (width>>1) , height );
		top.setRefPixelPosition( (width>>1) , 0 );

		patternTopR.setSize( NanoMath.max(topRight.getPosX() - (top.getPosX() + top.getWidth()),0) , patternTopR.getHeight() );
		patternTopL.setSize( NanoMath.max(top.getPosX() - (topLeft.getPosX() + topLeft.getWidth()),0) , patternTopL.getHeight() );

		patternBottomR.setSize( NanoMath.max(bottomRight.getPosX() - (bottom.getPosX() + bottom.getWidth()),0) , patternBottomR.getHeight() );
		patternBottomL.setSize( NanoMath.max(bottom.getPosX() - (bottomLeft.getPosX() + bottomLeft.getWidth()),0) , patternBottomL.getHeight() );
		patternTopR.setPosition( top.getPosX() + top.getWidth(), 0 );
		patternTopL.setPosition( topLeft.getPosX() + topLeft.getWidth(), 0 );

		patternBottomR.setPosition( bottom.getPosX() + bottom.getWidth(), height - patternBottomR.getHeight() );
		patternBottomL.setPosition( bottomLeft.getPosX() + bottomLeft.getWidth(), height - patternBottomL.getHeight() );

		/*patternBottomL.mirror( Pattern.TRANS_MIRROR_H );
		patternTopL.mirror( Pattern.TRANS_MIRROR_H );*/
		//patternTopL.getFill().setPosition( ( patternTopL.getWidth() % patternTopL.getFill().getWidth() ) , 0 );
		final int dxb = patternBottomL.getFill().getWidth() - (patternBottomL.getWidth() % patternBottomL.getFill().getWidth());
		patternBottomL.move( -dxb , 0 );
		patternBottomL.setSize( patternBottomL.getWidth() + dxb, patternBottomL.getHeight() );
		final int dxt = patternTopL.getFill().getWidth() - (patternTopL.getWidth() % patternTopL.getFill().getWidth());
		patternTopL.move( -dxt , 0 );
		patternTopL.setSize( patternTopL.getWidth() + dxt, patternTopL.getHeight() );
		
		patternLeft.setSize( patternLeft.getWidth(), height - patternLeft.getPosY() - bottomLeft.getHeight() );
		patternRight.setSize( patternRight.getWidth(), patternLeft.getHeight() );

		patternRight.setPosition( width - patternRight.getWidth(), patternLeft.getPosY() );

		refreshFill();
	}


	private final void refreshFill() {
		if ( fill != null ) {
			fill.setSize( bottomRight.getPosition().add( bottomRight.getSize() ) );
		}
	}

	public final int getBorderWidth(){
		return patternLeft.getWidth();
	}

	public final int getBorderHeight(){
		return patternTopR.getHeight();
	}

	public final void encapsulate( int x, int y, int width, int height ) {
		setPosition( x - patternLeft.getWidth(), y - patternTopR.getHeight() );
		setSize( width + ( patternTopR.getHeight() << 1 ), height + ( patternBottomR.getHeight() << 1 ) );
	}


	public final Rectangle getInternalRect() {
		return new Rectangle(patternLeft.getWidth(), patternTopR.getHeight(), patternRight.getPosX() - patternLeft.getWidth(), patternBottomR.getPosY() - patternTopR.getHeight() );
	}

}

//#endif