/**
 * Case.java
 *
 * Created on Aug 24, 2010 11:02:52 AM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Case extends DrawableGroup implements Constants {

	private final Sprite model;

	private int value;

	private static Sprite GIRLS;
	private static Sprite CASE;

	private static final byte GIRLS_TOTAL = 5;

	private static final byte SEQUENCE_STOPPED	= 0;
	private static final byte SEQUENCE_ANIMATED	= 1;

	private DrawableGroup caseImage;

	private int girlNameIndex;

	private static String[] girlsNames;
	private static boolean[] nameTaken;

	private static final short GIRLS_NAMES_TOTAL = 271;


	public Case() throws Exception {
		super( 4 );

		model = new Sprite( GIRLS );
		model.setPosition( CASE_GIRL_POSITION_X, CASE_GIRL_POSITION_Y );
		insertDrawable( model );

		setSize( model.getPosX() + model.getWidth(), model.getHeight() );

		defineReferencePixel( getWidth() >> 1, getHeight() + CASE_GIRL_REF_PIXEL_Y );
	}


	public final void set( int index, int value ) {
		this.value = value;
		if ( caseImage != null ) {
			removeDrawable( caseImage );
			caseImage = null;
		}
		caseImage = getCaseImage( index, false );
		caseImage.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_HCENTER );
		insertDrawable( caseImage );

		model.setFrame( NanoMath.randInt( GIRLS_TOTAL ) );

		caseImage.setPosition( CASE_CASE_POSITION_X, CASE_CASE_POSITION_Y - caseImage.getHeight() );

		int nameIndex = NanoMath.randInt( GIRLS_NAMES_TOTAL );
		while ( nameTaken[ nameIndex ] )
			nameIndex = NanoMath.randInt( GIRLS_NAMES_TOTAL );
		nameTaken[ nameIndex ] = true;
		girlNameIndex = nameIndex;

		setVisible( true );
	}


	public final String getGirlName() {
		return girlsNames[ girlNameIndex ];
	}


	public final Point getCenterPosition() {
		return new Point( getPosX() + ( getWidth() >> 1 ), getPosY() + ( getHeight() >> 1 ) );
	}


	public static final void resetGirlsNames() {
		if ( girlsNames == null ) {
			girlsNames = new String[ GIRLS_NAMES_TOTAL ];
			nameTaken = new boolean[ GIRLS_NAMES_TOTAL ];
			try {
				GameMIDlet.loadTexts( girlsNames, PATH_IMAGES + "girls.dat" );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					System.out.println( "Erro ao carregar nomes das modelos: " + e );
				//#endif
			}
		}

		for ( int i = 0; i < GIRLS_NAMES_TOTAL; ++i )
			nameTaken[ i ] = false;

		final int SORT_TRIES = GIRLS_NAMES_TOTAL << 1;
		for ( int i = 0; i < SORT_TRIES; ++i ) {
			final int randomIndex1 = NanoMath.randInt( GIRLS_NAMES_TOTAL );
			int randomIndex2;
			do {
				randomIndex2 = NanoMath.randInt( GIRLS_NAMES_TOTAL );
			} while ( randomIndex1 == randomIndex2 );

			final String temp = girlsNames[ randomIndex1 ];
			girlsNames[ randomIndex1 ] = girlsNames[ randomIndex2 ];
			girlsNames[ randomIndex2 ] = temp;
		}
		
		//#if DEBUG == "true"
			System.out.println( "GIRLS: " );
			for ( byte i = 0; i < CASES_TOTAL; ++i ) {
				System.out.println( ( i + 1 ) + " -> " + girlsNames[ i ] );
			}
		//#endif
	}


	public static final void loadImages() throws Exception {
		GIRLS = new Sprite( PATH_IMAGES + "girls" );
		CASE = new Sprite( PATH_IMAGES + ( GameMIDlet.isLowMemory() ? "case_low" : "case" ) );
	}


	public static final void unloadImages() {
		GIRLS = null;
		CASE = null;
	}


	public static final UpdatableGroup getCaseImage( final int index, boolean animated ) {
		final UpdatableGroup g = new UpdatableGroup( 2 );
		final Sprite caseImg = new Sprite( CASE );
		g.insertDrawable( caseImg );

		final Label number = new Label( FONT_CASE_SMALL );
		number.setText( String.valueOf( index ) );
		number.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
		number.setRefPixelPosition( (caseImg.getWidth() >> 1) + (CASE_SMALL_NUMBER_ERROR), CASE_SMALL_LABEL_Y );
		g.insertDrawable( number );

		g.setSize( caseImg.getSize() );

		if ( animated )
			caseImg.setSequence( SEQUENCE_ANIMATED );

		return g;
	}


	public final String getValueString() {
		return GameMIDlet.getCurrencyString( value );
	}


	public final int getValue() {
		return value;
	}


	public final void update( int delta ) {
		( ( Sprite ) caseImage.getDrawable( 0 ) ).update( delta );
	}


	public final void setActive( boolean active ) {
		( ( Sprite ) caseImage.getDrawable( 0 ) ).setSequence( active ? SEQUENCE_ANIMATED : SEQUENCE_STOPPED );
	}


	public final Point getCaseCenter() {
		return getPosition().add( caseImage.getRefPixelPosition() );
	}


	//#if TOUCH == "true"
		public final boolean contains( int x, int y ) {
			x -= getPosX();
			y -= getPosY();

			return caseImage.contains( x, y ) || caseImage.contains( x, y - TOUCH_TOLERANCE ) || caseImage.contains( x, y + TOUCH_TOLERANCE );
		}
	//#endif

}
