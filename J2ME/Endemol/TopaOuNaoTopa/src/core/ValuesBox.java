/**
 * ValuesBox.java
 *
 * Created on Aug 25, 2010 12:29:43 AM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import java.util.Vector;
import screens.GameMIDlet;
import screens.GameScreen;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public final class ValuesBox extends UpdatableGroup implements Constants, SpriteListener
	//#if TOUCH == "true"
		, PointerListener
	//#endif

{

	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;

	protected byte state;

	/** Duração da transição (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 620;

	private static final short TIME_ANIMATION = 600;

	private static final byte SEQUENCE_ACTIVE		= 0;
	private static final byte SEQUENCE_TRANSITION	= 1;
	private static final byte SEQUENCE_INACTIVE		= 2;

	protected int accTime;

	private final Label[] labels = new Label[ CASES_TOTAL ];

	private final ValueShine[] boxes = new ValueShine[ CASES_TOTAL ];
	private final Sprite[] shines = new Sprite[ CASES_TOTAL ];
	private final Label[] labelsTemp = new Label[ CASES_TOTAL ];
	private final short[] labelsAccTime = new short[ CASES_TOTAL ];

	/** "Tweener" da animação dos elementos na tela. Somente os valores de x são usados.
	 */
	private final BezierCurve tweener = new BezierCurve();

	private final boolean[] activeValues = new boolean[ CASES_TOTAL ];

	//#if SCREEN_SIZE != "SMALL"
		private final ColoredBorder border;
	//#endif

	private final Drawable logo;

	private final Rectangle backViewport;

	private final LedPattern pattern;

	//#if TOUCH == "true"
		/***/
		private final Point lastPointerPos = new Point();
	//#endif

	private final SpriteListener listener;

	private byte leftValues;

	
	public ValuesBox( SpriteListener listener, Rectangle backViewport ) throws Exception {
		super( ( CASES_TOTAL << 1 ) + 30 );
		GameMIDlet.log( "valuesBox.início" );

		this.listener = listener;
		this.backViewport = backViewport;

		pattern = new LedPattern();
		insertDrawable( pattern );

		GameMIDlet.log( "valuesBox.ledPattern" );

		//#if SCREEN_SIZE != "SMALL"
			border = new ColoredBorder();
			insertDrawable( border );
			GameMIDlet.log( "valuesBox.border" );
		//#endif

		final int[] values = GameScreen.VALUES;
		boxes[ 0 ] = new ValueShine();
		for ( int i = 0; i < CASES_TOTAL; ++i ) {
			final ValueShine box = new ValueShine( boxes[ 0 ].bar );
			boxes[ i ] = box;
			box.setListener( this, i );
			insertDrawable( box );

			final Label label = new Label( FONT_NUMBER_BLACK, GameMIDlet.getCurrencyString( values[ i ], false ) );
			labels[ i ] = label;
			insertDrawable( label );
		}
		GameMIDlet.log( "valuesBox.boxes" );

		leftValues = CASES_TOTAL;

		logo = TitleLabel.getLogo();
		logo.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( logo );
		GameMIDlet.log( "valuesBox.logo" );
	}


	public final void reset() throws Exception {
		leftValues = CASES_TOTAL;

		for ( byte i = 0; i < CASES_TOTAL; ++i )
			activeValues[ i ] = true;

		if ( !GameMIDlet.isLowMemory() ) {
			if ( shines[ 0 ] == null ) {
				//#if SCREEN_SIZE == "SMALL"
//# 					shines[ 0 ] = new Sprite( PATH_IMAGES + "shine2" );
				//#else
				shines[ 0 ] = new Sprite( PATH_IMAGES + "shine" );
				//#endif
			}
		}

		for ( int i = 0; i < CASES_TOTAL; ++i ) {
			removeDrawable( labelsTemp[ i ] );
			labelsTemp[ i ] = null;

			if ( !GameMIDlet.isLowMemory() ) {
				removeDrawable( shines[ i ] );
				shines[ i ] = new Sprite( shines[ 0 ] );
				shines[ i ].setListener( this, CASES_TOTAL + i );
			}
			
			labelsAccTime[ i ] = -1;
			boxes[ i ].setSequence( SEQUENCE_ACTIVE );
			labels[ i ].setFont( FONT_NUMBER_BLACK );
		}
		setState( STATE_HIDDEN );
	}


	public final int[] getValuesLeft() {
		final Vector v = new Vector();
		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			if ( activeValues[ i ] )
				v.addElement( new Integer( GameScreen.VALUES[ i ] ) );
		}

		final int[] valuesLeft = new int[ v.size() ];
		for ( byte i = 0; i < valuesLeft.length; ++i )
			valuesLeft[ i ] = ( ( Integer ) v.elementAt( i ) ).intValue();

		return valuesLeft;
	}


	/**
	 * 
	 * @return
	 */
	public final int getBiggestValueLeft() {
		for ( byte i = CASES_TOTAL - 1; i >= 0; --i ) {
			if ( activeValues[ i ] )
				return GameScreen.VALUES[ i ];
		}
		throw new IllegalStateException();
	}


	/**
	 * 
	 * @return
	 */
	public final int getLowValueLeft() {
		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			if ( activeValues[ i ] )
				return GameScreen.VALUES[ i ];
		}
		throw new IllegalStateException();
	}


	/**
	 * 
	 * @return
	 */
	public final int getNowBiggestLowValue() {
		int c = 2 + ( ( leftValues - 1 ) / 2 );
		for(int i = 0; i < CASES_TOTAL; ++i ) {
			if ( activeValues[ i ] ) { 
				c--;
				if(c==0)
					return GameScreen.VALUES[ i ];
			}
		}
		throw new IllegalStateException();
	}


	/**
	 * 
	 * @return
	 */
	public final int getNowBiggestMediumValue() {
		int c = 2 + ( ( leftValues - 1 ) / 3 );
		for(int i = CASES_TOTAL - 1; i >= 0; --i ) {
			if ( activeValues[ i ] ) { 
				c--;
				if(c==0)
					return GameScreen.VALUES[ i ];
			}
		}
		throw new IllegalStateException();
	}


	/**
	 * Calcula o menor dos valores "grandes" ainda pode ser ganho no jogo, e qual a chance do jogador ganhar isso ou mais.
	 * @return Point cujo x indica o menor dos maiores valores que pode ser ganho, e y indica a chance, em porcentagem.
	 */
	public final Point getBigValuesLeft() {
		int biggestValue = GameScreen.VALUES[CASES_TOTAL-1];
		byte bigValues = 0;
		
		for ( byte i = 19; i < CASES_TOTAL; ++i ) {
			if ( activeValues[ i ] ) {
				if ( GameScreen.VALUES[ i ] > GameScreen.getBiggestOverallMediumValue() ) {
					++bigValues;
					if ( biggestValue > GameScreen.VALUES[ i ] )
						biggestValue = GameScreen.VALUES[ i ];
				}
			}
		}
		
		if(bigValues <= 0) {
			for ( byte i = 18; i > 0; --i ) {
				if ( !activeValues[ i ] ) {
					biggestValue = GameScreen.VALUES[i];
				} else {
					break;
				}
			}
		}

		//#if DEBUG == "true"
			System.out.println("bigValues: " + bigValues + " | " + ( 100 * bigValues ) / ( leftValues + 1 ) + "%" );
		//#endif

		return new Point( biggestValue, ( 100 * bigValues ) / ( leftValues + 1 ) );
	}

	
	/**
	 *
	 * @return
	 */
	public final int calculateBankerOffer( int turn ) {
		//#if MILLION_VERSION == "true"
//# 			if(true) return 100000000; // para teste de tela de vitória com todo efeitos ligados
		//#endif
		
		turn = Math.min( turn, 10 );
		
		int value = 0;
		int count = 0;
		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			if ( activeValues[ i ] ) {
				value += GameScreen.VALUES[ i ];
				++count;
			}
		}
		//#if DEBUG == "true"
			System.out.println( "VALUE A: " + GameMIDlet.getCurrencyString( value, true ) + " -> " + count );
		//#endif
		if ( count > 0 )
			value /= count;

		value = value * turn / 10;
		//#if DEBUG == "true"
			System.out.println( "VALUE B: " + GameMIDlet.getCurrencyString( value, true ) );
			System.out.println( "OFFER: " + GameMIDlet.getCurrencyString( ( value + 100000 ) - ( value % 100000 ), true ) );
		//#endif

		value += (100-(value % 100));
		if(value > 100000) return ( value + 100000 ) - ( value % 100000 );
		else return value;
	}


	public final byte getValuesLeftCount() {
		// o total começa de -1 pois uma das maletas é a do jogador
		byte total = -1;

		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			if ( activeValues[ i ] )
				++total;
		}

		return total;
	}

	public final void eliminateValue( int value ) {
		for ( byte i = 0; i < CASES_TOTAL; ++i ) {
			if ( GameScreen.VALUES[ i ] == value ) {
				activeValues[ i ] = false;
				leftValues = getValuesLeftCount();

				//#if DEBUG == "true"
				int c = 0;
				for( int p = 0; p < CASES_TOTAL; ++p ) {
					if ( activeValues[ p ] ) {
						if ( GameScreen.VALUES[ p ]==getLowValueLeft() ) System.out.print( "( " );
						if ( c==1 ) System.out.print( "[ " );
						System.out.print( ""+GameScreen.VALUES[ p ]+" " ); ++c;
						if ( GameScreen.VALUES[ p ]==getNowBiggestMediumValue() ) System.out.print( "[ " );
						if ( GameScreen.VALUES[ p ]==getNowBiggestLowValue() ) System.out.print( "] " );
						if ( c==leftValues ) System.out.print( "] " );
						if ( GameScreen.VALUES[ p ]==getBiggestValueLeft() ) System.out.print( ") " );
					}
				}
				System.out.println( "" );
				System.out.println( "TotalLeft: " + leftValues );
				//#endif

				return;
			}
		}
	}


	public final byte getState() {
		return state;
	}


	public final void setState( int state ) {
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
				accTime = 0;
				setPosition( -getWidth(), 0 );
				refreshViewport();
				//if(state != this.state) listener.onSequenceEnded( LISTENER_ID_VALUES_BOX, state );
			break;

			case STATE_APPEARING:
				if ( this.state == STATE_SHOWN && getPosX() == tweener.destiny.x )
					return;

				accTime = 0;
				setTweener( getPosX(), 0 );
				refresh();
				setVisible( true );
			break;

			case STATE_SHOWN:
				setVisible( true );
				accTime = TRANSITION_TIME;
				refresh();

				for ( byte i = 0; i < CASES_TOTAL; ++i ) {
					switch ( boxes[ i ].getSequenceIndex() ) {
						case SEQUENCE_ACTIVE:
							if ( !activeValues[ i ] ) {
								MediaPlayer.play( SOUND_FLIP );
								boxes[ i ].setSequence( SEQUENCE_TRANSITION );
								labelsTemp[ i ] = new Label( FONT_NUMBER_WHITE, labels[ i ].getText() );
								labelsTemp[ i ].setPosition( labels[ i ].getPosition() );
								labelsTemp[ i ].setSize( labels[ i ].getSize() );
								labelsAccTime[ i ] = 0;
								insertDrawable( labelsTemp[ i ] );

								if ( !GameMIDlet.isLowMemory() )
									insertDrawable( shines[ i ] );
							}
						break;
					}
				}
			break;

			case STATE_HIDING:
				if ( ( this.state == STATE_HIDDEN && getPosX() == tweener.origin.x ) || state == this.state ) {
					return;
				}

				accTime = TRANSITION_TIME;
				setTweener( -getWidth(), getPosX() );
				refresh();
				setVisible( true );
				if ( this.state!=state )
					listener.onSequenceEnded( LISTENER_ID_VALUES_BOX, state );
			break;
		}

		this.state = ( byte ) state;
	}


	private final void setTweener( int originX, int destinationX ) {
		tweener.origin.set( originX, getPosY() );
		tweener.control1.set( ( originX + destinationX ) >> 1, getPosY() );
		tweener.control2.set( destinationX, getPosY() );
		tweener.destiny.set( destinationX, getPosY() );
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_APPEARING:
				accTime += delta;
				refresh();

				if ( accTime >= TRANSITION_TIME ) {
					setState( STATE_SHOWN );
					listener.onSequenceEnded( LISTENER_ID_VALUES_BOX, state );
				}
			break;

			case STATE_HIDING:
				final int previousTime = accTime;
				accTime -= delta;
				refresh();

				// garante que a tabela de valores está completamente escondida antes de avançar
				if ( accTime <= 0 && previousTime <= 0 ) {
					setState( STATE_HIDDEN );
					listener.onSequenceEnded( LISTENER_ID_VALUES_BOX, state );
				}
			break;

			case STATE_SHOWN:
				final Point p = new Point( getPosition() );
				for ( byte i = 0; i < CASES_TOTAL; ++ i ) {
					if ( labelsAccTime[ i ] >= 0 ) {
						labelsAccTime[ i ] += delta;
						labelsTemp[ i ].setViewport(
								new Rectangle( p.x + boxes[ i ].getPosX(), 0,
											   labelsAccTime[ i ] * boxes[ i ].getWidth() / TIME_ANIMATION,
											   getHeight() ) );
						if ( labelsAccTime[ i ] >= TIME_ANIMATION ) {
							labelsAccTime[ i ] = -1;
							if ( GameMIDlet.isLowMemory() )
								onSequenceEnded( i + CASES_TOTAL, 0 );
						}
					}
				}
			break;

			case STATE_HIDDEN:
			return;
		}

		super.update( delta );
	}


	private final void refresh() {
		final int fp_progress = NanoMath.clamp( NanoMath.divInt( accTime, TRANSITION_TIME ), 0, NanoMath.ONE );
		final Point p = new Point();

		tweener.getPointAtFixed( p, fp_progress );
		setPosition( p.x, 0 );
		refreshViewport();
	}


	private final void refreshViewport() {
		if ( state == STATE_HIDDEN ) {
			backViewport.x = 0;
			backViewport.width = getWidth();
		} else {
			backViewport.x = getPosX() + getWidth();
			backViewport.width = getWidth() - backViewport.x;
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		pattern.setSize( size );

		final byte TOTAL_COLUMNS = ( byte ) ( width > height ? 3 : 2 );

		final int TOTAL_ROWS = CASES_TOTAL / TOTAL_COLUMNS;
		final int remaining = CASES_TOTAL % TOTAL_COLUMNS;
		final int REAL_ROWS = (remaining==0) ? (TOTAL_ROWS) : (TOTAL_ROWS+1);

		//#if SCREEN_SIZE == "SMALL"
//# 			if ( (height - ( logo.getPosY() + logo.getHeight() ) ) > ( REAL_ROWS * VALUES_BOX_HEIGHT ) ) {
		//#else
			if ( (height - ( (logo.getPosY()<<1) + (border.getBorderHeight()<<1) ) ) > ( REAL_ROWS * VALUES_BOX_HEIGHT ) ) {
		//#endif
			logo.setRefPixelPosition( width >> 1, TITLE_Y_SPACING );
			logo.setVisible( true ); 
		} else {
			logo.setVisible( false );
		}
		
		final int bdWidth = NanoMath.min( TOTAL_COLUMNS == 2 ? VALUES_BOX_PORTRAIT_TOTAL_MAX_WIDTH : VALUES_BOX_LANDSCAPE_TOTAL_MAX_WIDTH , width);
		final int bdHeight = (logo.isVisible()) ? NanoMath.min( ( TOTAL_COLUMNS == 2 ) ? ( VALUES_BOX_PORTRAIT_TOTAL_MAX_HEIGHT ) : ( VALUES_BOX_LANDSCAPE_TOTAL_MAX_HEIGHT ) , ( height - ( (logo.getPosY()<<1) + logo.getHeight() ) ) ) : ( height );

		//#if SCREEN_SIZE == "SMALL"
//# 			final int INTERNAL_WIDTH = bdWidth;
//# 			final int INTERNAL_HEIGHT = bdHeight;
//# 
//# 			final int sumOfOffsetX = NanoMath.max( ( INTERNAL_WIDTH - ( TOTAL_COLUMNS * VALUES_BOX_WIDTH ) ) , 0 );
//# 			final int sumOfOffsetY = NanoMath.max( ( INTERNAL_HEIGHT - ( REAL_ROWS * VALUES_BOX_HEIGHT ) ) , 0 );
//# 			final int offsetX = sumOfOffsetX/(TOTAL_COLUMNS+1);
//# 			final int offsetY = sumOfOffsetY/(REAL_ROWS+1);
//# 
//# 			final int INITIAL_X = ( ( width - bdWidth ) >> 1 ) + offsetX + ((sumOfOffsetX%(TOTAL_COLUMNS+1))>>1);
//# 			final int INITIAL_Y = ( (logo.isVisible()) ? ( ( ( (logo.getPosY()<<1) + logo.getHeight() + TITLE_Y_SPACING ) + ( height - bdHeight ) ) >> 1 ) :
//#  						( ( height - bdHeight ) >> 1 ) ) +offsetY + ((sumOfOffsetY%(REAL_ROWS+1))>>1);
		//#else
			border.setSize( bdWidth, bdHeight );
			border.setPosition( ( ( width - bdWidth ) >> 1 ) ,
					(logo.isVisible()) ?
						( ( ( (logo.getPosY()<<1) + logo.getHeight() + TITLE_Y_SPACING ) + ( height - bdHeight ) ) >> 1 ) :
						( ( height - bdHeight ) >> 1 )
					);

			final int INTERNAL_WIDTH = bdWidth - (border.getBorderWidth()<<1);
			final int INTERNAL_HEIGHT = bdHeight - (border.getBorderHeight()<<1);
			final int sumOfOffsetX = NanoMath.max( ( INTERNAL_WIDTH - ( TOTAL_COLUMNS * VALUES_BOX_WIDTH ) ) , 0 );
			final int sumOfOffsetY = NanoMath.max( ( INTERNAL_HEIGHT - ( REAL_ROWS * VALUES_BOX_HEIGHT ) ) , 0 );
			final int offsetX = sumOfOffsetX/(TOTAL_COLUMNS+1);
			final int offsetY = sumOfOffsetY/(REAL_ROWS+1);

			final int INITIAL_X = border.getPosX() + border.getBorderWidth() + offsetX + ((sumOfOffsetX%(TOTAL_COLUMNS+1))>>1);
			final int INITIAL_Y = border.getPosY() + border.getBorderHeight() + offsetY + ((sumOfOffsetY%(REAL_ROWS+1))>>1);
		//#endif
		
		int y = INITIAL_Y;
		for ( int column = 0, x = INITIAL_X, current = 0; column < TOTAL_COLUMNS; ++column, x += VALUES_BOX_WIDTH + offsetX ) {
			y = INITIAL_Y;
			for ( byte row = 0; row < TOTAL_ROWS; ++row, ++current, y += VALUES_BOX_HEIGHT + offsetY ) {
				setBoxPosition( current, x, y );
			}
		}

		if ( remaining > 0 ) {
			//#if SCREEN_SIZE == "SMALL"
//# 				final int REMAINING_INITIAL_X = ( getWidth() >> 1 ) - ( ( ( remaining * VALUES_BOX_WIDTH ) + ( ( remaining - 1 ) * offsetX ) ) >> 1 );
			//#else
				final int REMAINING_INITIAL_X = border.getPosX() + ( border.getWidth() >> 1 ) - ( ( ( remaining * VALUES_BOX_WIDTH ) + ( ( remaining - 1 ) * offsetX ) ) >> 1 ); //( width - ( remaining * ( VALUES_BOX_WIDTH + ( VALUES_BOX_OFFSET << 1 ) ) - ( VALUES_BOX_OFFSET << 1 ) ) ) >> 1;
			//#endif
			for ( int i = CASES_TOTAL - remaining, x = REMAINING_INITIAL_X; i < CASES_TOTAL; ++i, x += VALUES_BOX_WIDTH + offsetX ) {
				setBoxPosition( i, x, y );
			}
		}
		
		refresh();
	}


	private final void setBoxPosition( int index, int x, int y ) {
		final Drawable box = boxes[ index ];
		final Drawable label = labels[ index ];
		box.setPosition( x, y );

		label.setPosition( box.getPosX() + (VALUES_BOX_WIDTH - (label.getWidth() + VALUES_BOX_TEXT_OFFSET)),
						   box.getPosY() + ((VALUES_BOX_HEIGHT - label.getHeight())>>1)); //- ( ( VALUES_BOX_HEIGHT - label.getHeight() ) >> 1 ) );

		if ( labelsTemp[ index ] != null )
			labelsTemp[ index ].setPosition( label.getPosition() );

		if ( shines[ index ] != null && !GameMIDlet.isLowMemory() ) {
			shines[ index ].setPosition( ( box.getPosX() + ( box.getWidth()>>1 ) ) - ( shines[ index ].getWidth() >> 1 ) ,
					( box.getPosY() + ( box.getHeight()>>1 ) ) - ( shines[ index ].getHeight() >> 1 ) );//box.getPosition() );
		}
	}


	public final boolean toggle() {
		switch ( state ) {
			case STATE_HIDING:
			case STATE_HIDDEN:
				setState( STATE_APPEARING );
			return true;

			case STATE_APPEARING:
			case STATE_SHOWN:
			default:
				setState( STATE_HIDING );
			return false;
		}
	}


	public final boolean isHiding() {
		switch ( state ) {
			case STATE_HIDDEN:
			case STATE_HIDING:
			return true;

			default:
				return false;
		}
	}


	public final void onSequenceEnded( int id, int sequence ) {
		if ( id < CASES_TOTAL ) {
			boxes[ id ].setSequence( SEQUENCE_INACTIVE );
		} else {
			id -= CASES_TOTAL;
			labels[ id ].setFont( FONT_NUMBER_WHITE );
			
			removeDrawable( labelsTemp[ id ] );
			labelsAccTime[ id ] = -1;
			labelsTemp[ id ] = null;

			if ( !GameMIDlet.isLowMemory() ) {
				removeDrawable( shines[ id ] );
				shines[ id ] = null;
			}
		}
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			switch ( state ) {
				case STATE_SHOWN:
					setPosition( Math.min( 0, getPosX() + x - lastPointerPos.x ), getPosY() );
					refreshViewport();
					lastPointerPos.set( x, y );
				break;
			}
		}


		public final void onPointerPressed( int x, int y ) {
			switch ( state ) {
				case STATE_SHOWN:
					lastPointerPos.set( x, y );
				break;
			}
		}


		public final void onPointerReleased( int x, int y ) {
			switch ( state ) {
				case STATE_SHOWN:
					setState( getPosX() > getWidth() / -10 ? STATE_APPEARING : STATE_HIDING );
				break;
			}
		}

	//#endif

	
	private static final class ValueShine extends UpdatableGroup {

		private final Sprite bar;

		//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "SMALL"
//# 			private final Sprite barRight;
		//#endif


		public ValueShine() throws Exception {
			this( new Sprite( PATH_IMAGES + "bar" ) );
		}


		public ValueShine( Sprite s ) throws Exception {
			super( 2 );

			bar = new Sprite( s );
			insertDrawable( bar );

			//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "SMALL"
//# 				barRight = new Sprite( bar );
//# 				barRight.mirror( TRANS_MIRROR_H );
//# 				insertDrawable( barRight );
			//#endif

			setSize( bar.getSize() );
		}


		public final void setSequence( int sequence ) {
			bar.setSequence( sequence );

			//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "SMALL"
//# 				barRight.setSequence( sequence );
			//#endif
		}


		public final void setListener( ValuesBox listener, int i ) {
			bar.setListener( listener, i );
		}


		public final byte getSequenceIndex() {
			return bar.getSequenceIndex();
		}
	}
}
