/**
 * LedPattern.java
 *
 * Created on Sep 16, 2010 7:30:19 PM
 *
 */
package core;


import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import core.Constants;

/**
 *
 * @author peter
 */
public final class LedPattern extends UpdatableGroup implements Constants {

	private final Pattern bkg;
	private final Pattern bkgAnimation;

	/** A posição x "real" da animação é armazenada em separado, pois a posição de desenho deve sempre ser arredondada
	 * de forma a cobrir os spots exatamente.
	 */
	private int bkgAnimationX;

	private final MUV bkgAnimSpeed = new MUV();


	public LedPattern() throws Exception {
		super( 2 );
		bkg = new Pattern( new DrawableImage( PATH_IMAGES + "off.png" ) );
		bkg.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		insertDrawable( bkg );

		bkgAnimation = new Pattern( new DrawableImage( PATH_IMAGES + "on.png" ) );
		bkgAnimation.setSize( bkgAnimation.getFill().getWidth(), Short.MAX_VALUE );
		insertDrawable( bkgAnimation );
	}


	public final void update( int delta ) {
		bkgAnimationX += bkgAnimSpeed.updateInt( delta );
		if ( bkgAnimationX >= ( getWidth() << 1 ) )
			bkgAnimationX = -bkgAnimation.getWidth();
		bkgAnimation.setPosition( bkgAnimationX - ( bkgAnimationX % BKG_ANIMATION_ROUND ), 0 );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		bkgAnimSpeed.setSpeed( Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) );
	}

}
