/**
 * GameScreen.java
 * ©2011 Nano Games.
 *
 * Created on Mar 2, 2011 2:07:41 PM.
 */

package screens;

import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Camera;
import core.Constants;
import core.GameBoard;
import core.GameMessage;
//#if TOUCH == "true"
import core.GestureManager;
import br.com.nanogames.components.userInterface.PointerListener;
import core.GestureListener;
//#endif
import core.HUD;
import core.Participant;
import core.Room;
import core.RoomMenu;
import core.TimeManager;
import javax.microedition.lcdui.Graphics;

/**
 * Tela principal do jogo.
 * @author Ygor
 */
public class GameScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
//#if TOUCH == "true"
	, PointerListener, GestureListener
//#endif
{
	//<editor-fold desc="Fields">

		/** GameMIDlet associado a essa screen. */
		private GameMIDlet midlet;

		/** Instância da classe de gerenciamento do tabuleiro. */
		private final GameBoard board;
		
		/** Instância da classe de gerenciamento da camera. */
		private static Camera camera;

		/** Elementos da interface grafica sobre o tabuleiro */
		private final HUD hud;

		/** Mensagem sendo atualmente apresentada na tela */
		private static GameMessage message;
		private static short messageIndex;
		private static RoomMenu roomMenu;

		//#if TOUCH == "true"
			private final GestureManager gestureManager;
		//#endif

		private static boolean messageIsOn() { return ( message != null || roomMenu != null ) && !GameBoard.isPlayerMoving(); }
	//</editor-fold>
    
    public static void load() {
        GameMIDlet.log("GameScreen foi chamado!");
        // TODO implementar para GameScreen
    }


	public static void setFocus( Participant p ) { if( camera != null ) camera.setCameraFocus( p.getPosition() ); }


    GameScreen(GameMIDlet midlet) throws Exception {
        super(4);
        this.midlet = midlet;

		TimeManager.reset();

		message = null;
		messageIndex = -1;
		roomMenu = null;

		board = new GameBoard( GameMIDlet.getPlayerId() );
		insertDrawable( board );
		
		camera = new Camera( board.getPosition(), board );
		camera.setMapSize( board.getSize() );
		board.setCamera( camera );

		hud = new HUD();
		insertDrawable( hud );
		hud.setPlayer( GameBoard.getPlayerParticipant() );

		// Faz do HUD o listener de eventos de alteracao de atributo
		// do participante do jogador
		GameBoard.getPlayerParticipant().setListener( hud );

        setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		//#if TOUCH == "true"
			gestureManager = new GestureManager( this );
		//#endif
    }


	void reset() {
	}


    public static void unload() {
		GameBoard.unload();
		camera = null;
		HUD.unload();
		message = null;
		messageIndex = -1;
		roomMenu = null;
		Participant.reset();
    }


	public final void paint( Graphics g ) {
		//#if DEBUG == "true"
			// Desenha retangulo para poder ser visto o frame rate
			g.fillRect( getWidth() - 80, 0, 100, 50 );
		//#endif
			
		super.paint( g );

		if( roomMenu != null ) roomMenu.draw( g );
	}


    public void sizeChanged(int width, int height) {
        setSize( width, height );

		board.sizeChanged( width, height );
		if( roomMenu != null )
			roomMenu.sizeChanged( width, height );
	}

	//<editor-fold desc="Event Handler">
		public void onSequenceEnded(int id, int sequence) {
			// TODO implementar para GameScreen
		}

		public void onFrameChanged(int id, int frameSequenceIndex) {
			// TODO implementar para GameScreen
		}


		public void hideNotify(boolean deviceEvent) {
			// TODO implementar para GameScreen
		}

		public void showNotify(boolean deviceEvent) {
			// TODO implementar para GameScreen
		}
	//</editor-fold> // end of Event Handler region


	public void update( int delta ) {
		super.update( delta );

		if( messageIsOn() ) {
			if( message != null ) {
				message.update( delta );
				if( message.isOver() ) {
					removeMessage();
				}
			}
			if( roomMenu != null ) {
				roomMenu.update( delta );
			}

		} else {
			camera.update( delta );
			board.update( delta );

			// caso a mensagem esteja aparecendo, vamos apresenta-la
			if( messageIsOn() &&  message != null )
				messageIndex = insertDrawable( message );
		}
	}

	//<editor-fold desc="Getters and Setters">
		int getScore() {
			// TODO implementar para GameScreen
			return 10;
		}

		int getLevel() {
			// TODO implementar para GameScreen
			return 1;
		}

		public void setSize( int width, int height ) {
			super.setSize( width, height );
			hud.setSize( width, height );
			camera.setSize( hud.getFreeArea().width, hud.getFreeArea().height );
			board.setPositionOffset( hud.getFreeArea().x, hud.getFreeArea().y, hud.getFreeArea().width, hud.getFreeArea().height );
			if( message != null ) message.setSize( width, height );
		}
	//</editor-fold> // end of Getters and Setters region

	//<editor-fold desc="Messages">
		public final void removeMessage() {
			removeDrawable( messageIndex );
			message.close();
			message = null;
		}

		/**
		 * Ativa uma mensagem na tela.
		 * @param gameMessage Mensagem a qual se deseja ativar.
		 * @param setHUDAsListener Determina se queremos ou não que o HUD seja um listener
		 * de eventos do participante.
		 */
		public void setMessage( GameMessage gameMessage, boolean setHUDAsListener ) {
			if( message != null ) {
				removeMessage();
			}

			message = gameMessage;
			if( gameMessage.abortActivities() ) {
				GameBoard.abortActivity();
			}

			if( setHUDAsListener )
				gameMessage.setListener( hud );
		}

		public final static void setRoomMenu( Room room, Participant player ) {
			roomMenu = new RoomMenu( room, player );
		}

		public final void removeRoomMenu() {
			roomMenu = null;
		}
		//public static GameMessage getMessage() { return message; }
	//</editor-fold>

	public int getPlayerParticipantAttribute( byte attributeId ) throws Exception {
		Participant playerParticipant = GameBoard.getPlayerParticipant();
		switch( attributeId ) {
			case ATTRIBUTE_BEAUTY:
				return playerParticipant.getBeaty();

			case ATTRIBUTE_CHARISMA:
				return playerParticipant.getCharisma();

			case ATTRIBUTE_KNOWLEDGE:
				return playerParticipant.getKnowledge();

			case ATTRIBUTE_RESISTANCE:
				return playerParticipant.getResistance();

			default:
				throw new Exception( "Id de atributo " + attributeId + " inválido.");
		}
	}

	//<editor-fold desc="Handle Input">
		public void keyPressed( int key ) {
			//TODO teste de tela de fim de jogo
			if( key == ScreenManager.KEY_NUM0 ) GameMIDlet.setScreen( SCREEN_WINSCREEN );
			else if( key == ScreenManager.KEY_POUND ) GameMIDlet.setScreen( SCREEN_GAME_OVER );
			else {
				if( messageIsOn() ) {
					if( message != null ) message.keyPressed( key );
					if( roomMenu != null ) roomMenu.keyPressed( key );
					return;
				} else if( !board.hasSelection() ) {
					hud.keyPressed( key );
					camera.keyPressed( key );
				}
				//tabuleiro.keyPressed( key, camera );
			}
		}

		public void keyReleased( int key ) {
			if( messageIsOn() ) {
				if( message != null ) { if( message.keyReleased( key ) ) { message.start(); hud.redrawAll(); } }
				if( roomMenu != null ) { if( roomMenu.keyReleased( key ) ) { removeRoomMenu(); hud.redrawAll(); } }
			} else {
				if( !board.hasSelection() ) {
					hud.keyReleased( key );
				}
				camera.keyReleased( key );
				board.keyReleased( key );
			}
		}


		//#if TOUCH == "true"
			public void onPointerDragged( int x, int y ) {
				gestureManager.onPointerDragged( x, y );
			}

			public void onPointerPressed( int x, int y ) {
				gestureManager.onPointerPressed( x, y );
			}

			public void onPointerReleased( int x, int y ) {
				gestureManager.onPointerReleased( x, y );
			}


			public void handleTap( int x, int y ) {
				if( messageIsOn() ) {
					if( message != null ) { if( message.handleTap( x, y ) ) { message.start(); hud.redrawAll(); } }
					if( roomMenu != null ) { if( roomMenu.handleTap( x, y ) ) { removeRoomMenu(); hud.redrawAll(); } }
				} else {
					if( hud.contains( x, y ) ) {
						hud.handleTap( x, y );
					} else {
						board.HandleTap( camera.getVirtualPosition( x, y ) );
					}
				}
			}

			public void handleFirstInteraction( int x, int y ) {
				if( messageIsOn() ) {
					if( message != null ) message.handleFirstInteraction( x, y );
					if( roomMenu != null ) roomMenu.handleFirstInteraction( x, y );
				} else {
					//if( hud.contains( x, y ) ) {
						hud.handleFirstInteraction( x, y );
					//} else {
						camera.onPointerPressed( x, y );
					//}
				}
			}

			public void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				if( messageIsOn() ) {
					if( message != null ) message.handleDrag( x, y, canBeTap, dx, dy );
					if( roomMenu != null ) roomMenu.handleDrag( x, y, canBeTap, dx, dy );
				} else {
					if( hud.contains( x, y ) ) {
						hud.handleDrag( x, y, canBeTap, dx, dy );
					} else {
						camera.onPointerDragged( x, y );
					}
				}
			}

			public void handleRelease( int x, int y ) {
				if( messageIsOn() ) {
					if( message != null ) message.handleRelease( x, y );
					if( roomMenu != null ) roomMenu.handleRelease( x, y );
				} else {
					if( hud.contains( x, y ) ) {
						hud.handleRelease( x, y );
					} else {
						camera.onPointerReleased( x, y );
					}
				}
			}

			public void gestureIsNotSpot( int x, int y ) {
				if( messageIsOn() ) {
					if( message != null ) message.gestureIsNotSpot( x, y );
					if( roomMenu != null ) roomMenu.gestureIsNotSpot( x, y );
				} else {
		//			if( hud.contains( x, y ) ) {
		//			} else {
		//			}
					board.GestureIsNotSpot( camera.getVirtualPosition( x, y ) );
					hud.gestureIsNotSpot( x, y );
				}
			}
		//#endif
	//</editor-fold> // end of Handle Input region
}
