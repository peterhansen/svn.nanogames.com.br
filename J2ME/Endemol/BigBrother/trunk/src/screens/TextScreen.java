package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.MediaPlayer;
import core.BasicScreen;
import javax.microedition.lcdui.Graphics;


/**
 * Tela para exibicao de texto, possuindo uma caixa de texto com barra de rolagem.
 * @author Caio, Ygor
 */
class TextScreen extends BasicScreen
{	
	private final ScrollRichLabel text;

	private boolean hasOkAction = false;
	
	//<editor-fold desc="Initialization">
		public TextScreen(	int slots,
							GameMIDlet midlet,
							int screen,
							boolean hasBorder,
							String textStr,
							boolean autoScroll ) throws Exception {
			this( slots, midlet, screen, textStr, autoScroll, null );
		}


		public TextScreen(	int slots,
							GameMIDlet midlet,
							int screen,
							String textStr,
							boolean autoScroll,
							Drawable[] specialChars ) throws Exception {

			super( slots + 2, midlet, screen, BARS_NONE, false, COLOR_BACKGROUND );

			if ( !autoScroll ) {
				final LineBorder lb = new LineBorder( COLOR_TOP_BAR_TOP, LineBorder.TYPE_ROUND_SIMPLE );
				lb.setFillColor( COLOR_TOP_BAR_TOP );
				lb.setSize( SCROLL_BAR_WIDTH, 50 );
				final Pattern dummy = new Pattern( COLOR_BACKGROUND );
				dummy.setSize( SCROLL_BAR_WIDTH, 50 );
				RichLabel label = new RichLabel( GameMIDlet.GetFont( FONT_TEXT_BLACK ), textStr + "\n\n\n" );
				label.setSpecialChars( specialChars );
				text = new ScrollRichLabel( label, dummy, lb );
			} else {
				text = new ScrollRichLabel( new RichLabel( GameMIDlet.GetFont( FONT_TEXT_BLACK ), textStr + "\n\n\n" ) );
			}
			insertDrawable( text );
			text.setAutoScroll( autoScroll );

			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}

		public TextScreen(	int slots,
							GameMIDlet midlet,
							int screen,
							String textStr,
							boolean autoScroll,
							Drawable[] specialChars,
							boolean hasOkAction ) throws Exception {
			this( slots, midlet, screen, textStr, autoScroll, specialChars );
			this.hasOkAction = hasOkAction;
		}
	//</editor-fold>

	public void setSize( int width, int height ) {
		super.setSize( width, height );

		if( text.getScrollPage() != null ) {
			text.getScrollPage().setSize( SCROLL_BAR_WIDTH, height );
			if( text.getScrollFull() != null ) {
				text.getScrollFull().setSize( text.getScrollPage().getSize() );
			}
		}

		text.setPosition( freeArea.x + SAFE_MARGIN, freeArea.y );
		text.setSize( freeArea.width - ( SAFE_MARGIN << 1 ), freeArea.height );
	}

	public void drawFrontLayer( Graphics g ) {
		(okButtonIsPressed? okButtonPressed: okButton).draw( g );
		(backButtonIsPressed? backButtonPressed: backButton).draw( g );
	}

	public void showNotify( boolean deviceEvent ) {
		super.showNotify( deviceEvent );
		if( !MediaPlayer.isPlaying() ) {
			MediaPlayer.play( SOUND_FIRST_MUSIC, MediaPlayer.LOOP_INFINITE );
		}
	}


	//<editor-fold desc="Handle Input">
		public void internalKeyReleased( int key ) {
			text.keyReleased( key );
		}

		public void internalKeyPressed( int key ) {
			text.keyPressed( key );
		}

		//#if TOUCH == "true"
			public void internalFirstInteraction( int x, int y ) {
				if( freeArea.contains( x, y ) )
					text.onPointerPressed( x, y );
			}


			public void internalHandleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				if( freeArea.contains( x, y ) )
					text.onPointerDragged( x, y );
			}


			public void internalHandleRelease( int x, int y ) {
				if( freeArea.contains( x, y ) )
					text.onPointerReleased( x, y );
			}
		//#endif
	//</editor-fold>

	//<editor-fold desc="OK and Back Actions">
		protected void okAction() {
			if( hasOkAction ) {
				midlet.onChoose( null, screenID, TEXT_OK );
			} else {
				backAction();
			}
		}

		protected void backAction() {
			exit();
		}
	//</editor-fold>
}
