package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import core.AttributeBar;
import core.BasicScreen;
import core.GameBoard;
import core.Participant;
import javax.microedition.lcdui.Graphics;

/**
 * Tela do dia da Eliminacao, no qual os dois participantes indicados
 * tem suas popularidades comparadas e o menos popular sai.
 * @author Caio, Ygor
 */
public class EliminationScreen extends BasicScreen implements Updatable {

	//<editor-fold desc="Constants">
		private static final int TIME_TO_SHOW = AUTOMATIC ? 1 : 5000;
	//</editor-fold>

	//<editor-fold desc="Variables">
		private final DrawableImage vsIcon;

		private final Sprite profilePhotos;

		private final Participant nomineeA;
		private final Participant nomineeB;

		private final DrawableImage eliminatedX;
		private final Pattern textBkg;
		private final RichLabel eliminatedText;

		private final AttributeBar percentageBar;

		private int currentPercentageA;
		private int currentPercentageB;

		private final Label nameA;
		private final Label nameB;

		private final boolean isNomineeA;
		private final boolean isFinal;
		private boolean hasReleasedResult;

		private BezierCurve percentageVariation = new BezierCurve();

		private int accTime = 0;
	//</editor-fold>


	//<editor-fold desc="Inicialization">
		public EliminationScreen( GameMIDlet midlet ) throws Exception {
			super( 9, midlet, SCREEN_ELIMINATION_SCREEN, BARS_MIDDLE_AND_SPECIAL, false, COLOR_BACKGROUND );

			this.isFinal = GameBoard.isLastWeek();
			this.nomineeA = GameBoard.getNomineeA();
			this.nomineeB = GameBoard.getNomineeB();
			
			//#if DEBUG == "true"
				System.out.println( "isFinal? " + isFinal );
			//#endif

			hasReleasedResult = false;
			
			textBkg = new Pattern( COLOR_OPTIONS_BACKGROUND );
			insertDrawable( textBkg );

			eliminatedText = new RichLabel( FONT_TEXT_BLACK );
			insertDrawable( eliminatedText );
			
			eliminatedX = new DrawableImage( PATH_CHARSELECTION_IMAGES + "x.png" );
			eliminatedX.defineReferencePixel( ANCHOR_CENTER );
			eliminatedX.setVisible( false );

			percentageBar = new AttributeBar( (byte)0, COLOR_CHARISMA, true, COLOR_RESISTANCE );
			insertDrawable( percentageBar );

			profilePhotos = new Sprite( PATH_CHARSELECTION_IMAGES + "participants" );
			profilePhotos.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );

			vsIcon = new DrawableImage( PATH_CHARSELECTION_IMAGES + "vs.png" );
			vsIcon.defineReferencePixel( ANCHOR_CENTER );
			insertDrawable( vsIcon );

			calculatePopularity();
			isNomineeA = currentPercentageA > currentPercentageB;

			nameA = new Label( FONT_MENU, nomineeA.getName().toUpperCase() );
			insertDrawable( nameA );
			nameB = new Label( FONT_MENU, nomineeB.getName().toUpperCase() );
			insertDrawable( nameB );

			percentageVariation.origin.set( 50, 50 );
			int temp = NanoMath.randInt( 100 );
			percentageVariation.control1.set( temp, 100 - temp );
			temp = NanoMath.randInt( 100 );
			percentageVariation.control2.set( temp, 100 - temp );
			percentageVariation.destiny.set( currentPercentageA, currentPercentageB );
		}
	//</editor-fold>
	

	//<editor-fold desc="Getters and Setters">
		public void setSize( int width, int height ) {
			super.setSize( width, height );

			vsIcon.setRefPixelPosition( middleBarArea.x + ( middleBarArea.width >> 1 ), middleBarArea.y + ( middleBarArea.height >> 1 ));
			
			textBkg.setSize( freeArea.width - ( BASIC_SCREEN_MARGIN << 1 ), freeArea.height - ( BASIC_SCREEN_MARGIN << 1 ) );
			textBkg.setPosition( freeArea.x + BASIC_SCREEN_MARGIN, freeArea.y + BASIC_SCREEN_MARGIN );

			eliminatedText.setSize( textBkg.getWidth() - ( BASIC_SCREEN_MARGIN << 1 ), textBkg.getHeight() - ( BASIC_SCREEN_MARGIN << 1 ) );
			eliminatedText.setPosition( textBkg.getPosX() + BASIC_SCREEN_MARGIN, textBkg.getPosY() + BASIC_SCREEN_MARGIN );

			int spacing = ( specialBarArea.height - ( nameA.getHeight() + percentageBar.getHeight() ) ) / 3;

			nameA.setPosition( centerLabelText( nameA, 0, width >> 1 ), specialBarArea.y + spacing );
			nameB.setPosition( centerLabelText( nameB, width >> 1, width ), specialBarArea.y + spacing );

			percentageBar.setSize( specialBarArea.width - ( BASIC_SCREEN_MARGIN << 1 ), percentageBar.getHeight() );
			percentageBar.setPosition( specialBarArea.x + ( ( specialBarArea.width - percentageBar.getWidth() ) >> 1 ), specialBarArea.y + specialBarArea.height - ( spacing + percentageBar.getHeight() + BAR_Y_OFFSET_BOTTOM ) );

			if( isNomineeA ^ isFinal ) eliminatedX.setRefPixelPosition( ( getWidth() >> 2 ), middleBarArea.y + ( middleBarArea.height >> 1 ) );
			else eliminatedX.setRefPixelPosition( ( ( 3 * getWidth() ) >> 2 ), middleBarArea.y + ( middleBarArea.height >> 1 ) );
		}


		public void setPercentageA( int percentage ) {
			percentage = NanoMath.clamp( percentage, 0, 100 );
			this.percentageBar.setValue( percentage );
			currentPercentageA = percentage;
		}

		public void setPercentageB( int percentage ) {
			percentage = NanoMath.clamp( percentage, 0, 100 );
			currentPercentageB = percentage;
		}
	//</editor-fold>


	public final void setEliminationText( String name ) {
		eliminatedText.setText( "<ALN_H>" + GameMIDlet.getText( ( isFinal ? TEXT_THE_WINNER_IS : TEXT_THE_ELIMINATED_IS ) ) + name, true );
	}
	
	
	public void calculatePopularity() {
		//#if DEBUG == "true"
			System.out.println( "nomineeA = " + nomineeA + " " + nomineeA.getName() );
			System.out.println( "nomineeB = " + nomineeB + " " + nomineeB.getName() );
		//#endif

		if( ALWAYS_WIN ) {
			//TODO teste de viória
			Participant player = GameBoard.getPlayerParticipant();
			if( player == nomineeA ) {
				if( isFinal ) {
					currentPercentageA = 99;
					currentPercentageB = 1;
				} else {
					currentPercentageA = 2;
					currentPercentageB = 98;
				}
				return;
			} else if( player == nomineeB ) {
				if( isFinal ) {
					currentPercentageA = 3;
					currentPercentageB = 97;
				} else {
					currentPercentageA = 96;
					currentPercentageB = 4;
				}
				return;
			}
		}
			
		int a = nomineeA.getPopularity();
		int b = nomineeB.getPopularity();
		
		if( isFinal ) {
			currentPercentageA = ( a * 100 ) / ( a + b );
			currentPercentageB = ( b * 100 ) / ( a + b );	
		} else {
			currentPercentageA = ( b * 100 ) / ( a + b );
			currentPercentageB = ( a * 100 ) / ( a + b );
		}
	}

	public void exit() {
		if( hasReleasedResult ) {
			Participant player = GameBoard.getPlayerParticipant();
			if( ( player == nomineeA ) && isNomineeA ) {
				System.out.println( "jogador o paredão como indicado A" );
				if( isNomineeA ) {
					if( isFinal ) {
						System.out.println( "jogador venceu" );
						midlet.onChoose( null, screenID, TEXT_WINNER );
					} else {
						System.out.println( "jogador saiu da casa" );
						midlet.onChoose( null, screenID, TEXT_GAME_OVER );
					}
					return;
				}
			} else if( ( player == nomineeB ) ) {
				System.out.println( "jogador o paredão como indicado B" );
				if( !isNomineeA ) {
					if( isFinal ) {
						System.out.println( "jogador venceu" );
						midlet.onChoose( null, screenID, TEXT_WINNER );
					} else {
						System.out.println( "jogador saiu da casa" );
						midlet.onChoose( null, screenID, TEXT_GAME_OVER );
					}
					return;
				}
			}
			if( isFinal ) {
				System.out.println( "jogo acabou" );
				midlet.onChoose( null, screenID, TEXT_GAME_OVER );
			} else {
				System.out.println( "o jogo segue" );
				midlet.onChoose( null, screenID, TEXT_EXIT );
			}
		}
	}

	//<editor-fold desc="Update and Draw">
		public void drawFrontLayer( Graphics g ) {
			profilePhotos.setRefPixelPosition( getWidth() >> 2, middleBarArea.y + middleBarArea.height );
			profilePhotos.setFrame( nomineeA.getPhotoId() );
			profilePhotos.setTransform( TRANS_MIRROR_H );
			profilePhotos.draw( g );

			profilePhotos.setRefPixelPosition( ( 3 * getWidth() ) >> 2, middleBarArea.y + middleBarArea.height );
			profilePhotos.setFrame( nomineeB.getPhotoId() );
			profilePhotos.setTransform( TRANS_NONE );
			profilePhotos.draw( g );
			
			eliminatedX.draw( g );

			( okButtonIsPressed ? okButtonPressed : okButton ).draw( g );
		}


		public void internalUpdate( int delta ) {
			if( eliminatedText.getTextTotalHeight() > eliminatedText.getHeight() )
				eliminatedText.setTextOffset( -NanoMath.toInt( ( ( NanoMath.cosInt( (int) ( 360 * System.currentTimeMillis() / 10000 ) ) + NanoMath.ONE ) / 2 ) * ( eliminatedText.getTextTotalHeight() - eliminatedText.getHeight() ) ) );
			else
				eliminatedText.setTextOffset( 0 );

			if( !hasReleasedResult ) {
				accTime += delta;
				if( accTime < TIME_TO_SHOW ) {
					setPercentageA( percentageVariation.getX( ( accTime * 100 ) / TIME_TO_SHOW ) );
					setPercentageB( percentageVariation.getY( ( accTime * 100 ) / TIME_TO_SHOW ) );

					setEliminationText( ( ( currentPercentageA > currentPercentageB ) ? nomineeA.getName() : nomineeB.getName() ) );

				} else {
					GameBoard.eliminate( isNomineeA ? nomineeA : nomineeB );
					hasReleasedResult = true;
					accTime = TIME_TO_SHOW;

					setPercentageA( percentageVariation.destiny.x );
					setPercentageB( percentageVariation.destiny.y );

					eliminatedX.setVisible( true );

					setEliminationText( ( ( isNomineeA ) ? nomineeA.getName() : nomineeB.getName() ) + "\n" + GameMIDlet.getText( ( isFinal ? TEXT_WINNER_TEXT : TEXT_ELIMINATION_TEXT ) ) );
				}
			}
		}
	//</editor-fold>

	//<editor-fold desc="OK and Back Actions">
		protected void okAction() {
			exit();
		}
	//</editor-fold>
}
