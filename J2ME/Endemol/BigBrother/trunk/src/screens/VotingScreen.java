package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import core.AttributeBar;
import core.BasicScreen;
import core.GameBoard;
//#if TOUCH == "true"
import core.GestureManager;
//#endif
import core.Participant;
import javax.microedition.lcdui.Graphics;

/**
 * Tela onde o jogador vota em um participante para leva-lo
 * para o dia de eliminacao. Os outros participantes tambem
 * fazem votacoes baseados em suas afinidades entre si.
 * @see Participant
 * @author Ygor
 */
public class VotingScreen extends BasicScreen {

	//<editor-fold desc="Constants">
		private static final byte STATE_VOTING = 0;
		private static final byte STATE_COUNTING = 1;
		private static final byte STATE_RESULTS = 2;

		private static final int VOTE_COUNT_BAR_SLOWNESS = AUTOMATIC ? 1 : 30;

		private static final int MAX_VOTE_BAR_SPEED = 2;
	//</editor-fold>

	//<editor-fold desc="Fields">
		private byte state = 0;
		private Participant[] participants;
		private Participant nomineeA = null;
		private Participant nomineeB = null;

		private int accumDelta = 0;
		private int accumBarValue = 0;
	//</editor-fold>

	//<editor-fold desc="Screen Components">
		private static Sprite profilePhotos;

		private final Label nameALabel;
		private final Label nameBLabel;

		private final DrawableImage vsIcon;

		private final Pattern flavorTextBkg;
		private final ScrollRichLabel flavorText;

		// TODO: renomear attribute bar para "GameBar"?
		private final AttributeBar voteCountBar;
	//</editor-fold>

	public VotingScreen( GameMIDlet midlet, int screenID, Participant[] participants ) throws Exception {
		super( 50, midlet, screenID, BARS_MIDDLE_AND_SPECIAL, true, COLOR_BACKGROUND );

		this.participants = participants;

		// a barra especial dessa tela e menor
		specialBarHDen = 3;

		profilePhotos = new Sprite(PATH_CHARSELECTION_IMAGES + "participants");
		profilePhotos.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );

		voteCountBar = new AttributeBar( (short) 0, COLOR_CHARISMA );
		voteCountBar.defineReferencePixel( ANCHOR_CENTER );

		nameALabel = new Label( FONT_MENU, "");
		insertDrawable( nameALabel );
		nameBLabel = new Label( FONT_MENU, "");
		insertDrawable( nameBLabel );

		vsIcon = new DrawableImage( PATH_CHARSELECTION_IMAGES + "vs.png" );
		vsIcon.defineReferencePixel( ANCHOR_CENTER );

		flavorText = new ScrollRichLabel( new RichLabel( FONT_TEXT_BLACK, "") );

		// verificar se precisa de autoscroll?
		flavorText.setAutoScroll( true );
		
		flavorTextBkg = new Pattern( COLOR_OPTIONS_BACKGROUND );
		insertDrawable( flavorTextBkg );
		insertDrawable( flavorText );

		maxIndex = participants.length;
		setIndex( 1 );

		changeState( STATE_VOTING );
	}

	public void setSize( int width, int height ) {
		super.setSize( width, height);

		flavorText.setPosition( SAFE_MARGIN, freeArea.y + ( SAFE_MARGIN * 3 ) / 4 );
		flavorText.setSize( freeArea.width - SAFE_MARGIN, freeArea.height - ( 3 * SAFE_MARGIN ) / 4);

		flavorTextBkg.setPosition( SAFE_MARGIN >> 1, freeArea.y + ( SAFE_MARGIN >> 1 ) );
		flavorTextBkg.setSize( freeArea.width - SAFE_MARGIN, freeArea.height - SAFE_MARGIN );

		int spacing;
		switch( state ) {
			case STATE_VOTING:
				profilePhotos.setRefPixelPosition( middleBarArea.width >> 1, middleBarArea.y + middleBarArea.height );

				spacing = specialBarArea.y + ( specialBarArea.height >> 1 ) - ( nameALabel.getFont().getHeight() >> 1 ) + SPECIAL_BAR_TEXT_OFFSET;
				nameALabel.setPosition( centerLabelText( nameALabel, 0, ScreenManager.SCREEN_WIDTH ), spacing );

				// posicao vertical das setas:
				// caso a tela seja horizontalizada, nao queremos cobrir o logo
				int arrowsY = middleBarArea.y + ( middleBarArea.height >> 1 );
				if( width > height )
					arrowsY = logo.getPosX() + logo.getHeight() + ( ( middleBarArea.y + middleBarArea.height - logo.getPosX() - logo.getHeight() ) >> 1 );

				arrowLeft.setRefPixelPosition( profilePhotos.getPosX() - ( SAFE_MARGIN << 1 ), arrowsY );
				arrowRight.setRefPixelPosition( profilePhotos.getPosX() + profilePhotos.getWidth() + ( SAFE_MARGIN << 1 ), arrowLeft.getRefPixelY() );
				arrowLeftPressed.setRefPixelPosition( arrowLeft.getRefPixelPosition() );
				arrowRightPressed.setRefPixelPosition( arrowRight.getRefPixelPosition() );
				
				//#if TOUCH == "true"
					GestureManager.setTouchArea( arrowLeftArea, arrowLeft.getPosition(), arrowLeft.getSize(), TOUCH_TOLERANCE );
					GestureManager.setTouchArea( arrowRightArea, arrowRight.getPosition(), arrowRight.getSize(), TOUCH_TOLERANCE );
				//#endif
				break;

				
			case STATE_COUNTING:

				spacing = ( specialBarArea.height - ( voteCountBar.getHeight() ) ) >> 1;
				voteCountBar.setSize( specialBarArea.width - ( BASIC_SCREEN_MARGIN << 1 ), voteCountBar.getHeight() );
				voteCountBar.setPosition( specialBarArea.x + ( ( specialBarArea.width - voteCountBar.getWidth() ) >> 1 ), specialBarArea.y + spacing/*specialBarArea.height - ( spacing + voteCountBar.getHeight() + BAR_Y_OFFSET_BOTTOM )*/ );
				break;

			case STATE_RESULTS:

				vsIcon.setRefPixelPosition(  middleBarArea.width >> 1, middleBarArea.y + ( middleBarArea.height >> 1 ) );
				
				spacing = specialBarArea.y + ( specialBarArea.height >> 1 ) - ( nameALabel.getFont().getHeight() >> 1);
				nameALabel.setPosition( centerLabelText( nameALabel, 0, width >> 1 ), spacing );
				nameBLabel.setPosition( centerLabelText( nameBLabel, width >> 1, width ), spacing );
				break;
		}
	}

	public void drawFrontLayer( Graphics g ) {

		switch( state ) {
			case STATE_VOTING:
				profilePhotos.draw( g );
				( arrowLeftIsPressed? arrowLeftPressed: arrowLeft ).draw( g );
				( arrowRightIsPressed? arrowRightPressed: arrowRight).draw( g );
				( okButtonIsPressed? okButtonPressed: okButton).draw( g );

				nameALabel.draw( g );
			break;

			case STATE_COUNTING:
				voteCountBar.draw( g );
			break;

			case STATE_RESULTS:

				vsIcon.draw( g );

				profilePhotos.mirror( Sprite.TRANS_MIRROR_H );
				profilePhotos.setRefPixelPosition( getWidth() >> 2, middleBarArea.y + middleBarArea.height );
				profilePhotos.setFrame( nomineeA.getPhotoId() );
				profilePhotos.draw( g );

				profilePhotos.setRefPixelPosition( ( 3 * getWidth() ) >> 2, middleBarArea.y + middleBarArea.height );
				profilePhotos.mirror( Sprite.TRANS_MIRROR_H );
				profilePhotos.setFrame( nomineeB.getPhotoId() );
				profilePhotos.draw( g );

				nameALabel.draw( g );
				nameBLabel.draw( g );

				( okButtonIsPressed? okButtonPressed: okButton ).draw( g );
			break;
		}
	}

	public void internalUpdate( int delta ) {
		if( state == STATE_COUNTING ) {
			accumDelta += delta;

			int instantDelta = accumDelta / VOTE_COUNT_BAR_SLOWNESS;
			
			if( instantDelta > 0 ) {
				accumBarValue += instantDelta;
				int newValue = accumBarValue;
				voteCountBar.setValue( newValue );
			
				if( newValue > 100 ) {
					changeState( STATE_RESULTS );
				}
			}
			accumDelta %= VOTE_COUNT_BAR_SLOWNESS;
		}
	}

	private void changeState( byte newState ) {
		state = newState;

		switch( newState ) {
			case STATE_RESULTS:
				accumBarValue = 0;
				calculateNominees();
				nameALabel.setText( nomineeA.getName().toUpperCase() );
				nameBLabel.setText( nomineeB.getName().toUpperCase() );
				String text = GameMIDlet.getText( TEXT_VOTING_RESULTS );

				// inserindo nomes dos participantes no texto
				flavorText.setText( GameMIDlet.insertParticipantNames( text, nomineeA, nomineeB ) );
				break;

			case STATE_VOTING:
				flavorText.setText( GameMIDlet.getText( TEXT_VOTING ) );
				break;
				
			case STATE_COUNTING:
				flavorText.setText( GameMIDlet.getText( TEXT_VOTING_COUNT ) );
				break;
		}

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	/**
	 * Realiza a votação, utilizando o voto do jogador mais os votos dos outros
	 * participantes baseados em suas afinidades. Em caso de empate, o criterio
	 * de desempate e uma comparacao das popularidades.
	 */
	private void calculateNominees() {

		// votos que cada participante recebe, indexados por seus indices
		byte[] votes = new byte[PARTICIPANTS_NUMBER];

		for( int i = 0; i < participants.length; i++ ) {
			Participant p = participants[ i ];
			if( p.getId() == GameMIDlet.getPlayerId() ) {
				// participante do jogador escolheu o participante do indice corrente
				votes[ participants[ currentIndex ].getId() ]++;
			} else {
				// opcao do participante p para o paredao:
				votes[ p.voteForElimination() ]++;
			}
		}

		//#if DEBUG == "true"
		/*	System.out.println( "VOTAÇÃO!!!" );
			for ( int i = 0; i < votes.length; i++ ) {
				for ( int j = 0; j < participants.length; j++ ) {
					if( participants[ j ].getId() == i ) {
						System.out.println( participants[ j ].getName() + " tem " + votes[ i ] + " votos" );
						break;
					}
				}
			}*/
		//#endif
		
		// todos os votos já foram computados,
		// vamos realizar a contagem
		byte votesNomineeA = -1, votesNomineeB = -1;
		for( int i = 0; i < votes.length; i++ ) {

			// procurando participante para esse indice
			for( int j = 0; j < participants.length; j++ ) {
				if( i == participants[j].getId() ) {
					Participant p = participants[j];

					// atualiza o primeiro e segundo lugares
					if( votes[ i ] > votesNomineeA || ( votes[ i ] == votesNomineeA && p.getPopularity() > nomineeA.getPopularity() ) ) {
						votesNomineeB = votesNomineeA;
						nomineeB = nomineeA;
						votesNomineeA = votes[ i ];
						nomineeA = p;
					} else if( votes[ i ] > votesNomineeB || ( votes[ i ] == votesNomineeB && p.getPopularity() > nomineeB.getPopularity() ) ) {
						votesNomineeB = votes[ i ];
						nomineeB = p;
					}
				}
			}
		}
	}

	//<editor-fold desc="Input Handles">
		//#if TOUCH == "true"
			public void handleRelease( int x, int y ) {
				okButtonIsPressed = arrowLeftIsPressed = arrowRightIsPressed = false;
			}
		//#endif
	//</editor-fold>

	public final void setIndex( int newIndex ) {
		int increment = newIndex - currentIndex;
		increment = increment > 0? 1: -1;

		currentIndex = findParticipant( increment );
		updateIndex();
	}

	/**
	 * Atualiza foto e nome do participante.
	 */
	protected final void updateIndex() {
		if( state == STATE_VOTING ) {
			profilePhotos.setFrame( participants[currentIndex].getPhotoId() );
			nameALabel.setText( participants[currentIndex].getName().toUpperCase() );
			nameALabel.setPosition( centerLabelText( nameALabel, 0, ScreenManager.SCREEN_WIDTH ), nameALabel.getPosY() );
		}
	}

	/**
	 * Procura índice de novo participante, diferente do jogador.
	 * @param increment step usado a cada iteração da busca.
	 * @return Índice do próximo participante. Caso não exista, retorna o índice atual.
	 */
	private int findParticipant( int increment ) {
		int newCurrentIndex = currentIndex;
		do {
			newCurrentIndex += increment;
			if( newCurrentIndex >= participants.length )
				newCurrentIndex -= participants.length;
			else if( newCurrentIndex < 0 )
				newCurrentIndex += participants.length;

		} while ( ( newCurrentIndex != currentIndex ) && participants[newCurrentIndex].getId() == GameMIDlet.getPlayerId() );
		return newCurrentIndex;
	}

	protected void okAction() {
		switch( state ) {
			case STATE_VOTING:
				changeState( STATE_COUNTING );
				break;
			case STATE_RESULTS:
				GameBoard.setNominees( nomineeA, nomineeB );
				GameMIDlet.setScreen( SCREEN_GAME );
		}
	}
}