package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
//#if TOUCH == "true"
import core.GestureManager;
import br.com.nanogames.components.util.Rectangle;
//#endif
import core.AttributeBar;
import core.BasicScreen;
import core.GameBoard;
import core.Participant;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
public class StatsScreen extends BasicScreen
{
	//<editor-fold desc="Fields">
		private final Label[] entryTitleLabel; // { name, hobby, food, charisma, beauty, resistance, knowlegde }
		private final Pattern[] entryBkgPattern; // { name, hobby, food, charisma, beauty, resistance, knowlegde }
		private final Drawable[] entryValue; // { name, hobby, food, charisma, beauty, resistance, knowlegde }
		private final Participant[] participants;
		private final Participant[] eliminateds;
		private final Participant nomineeA;
		private final Participant nomineeB;

		private final MarqueeLabel nominee;
		
		private final MarqueeLabel nameLabel;
		private final MarqueeLabel favoriteHobby;
		private final MarqueeLabel favoriteFood;
		
		// barras de atributos
		private final AttributeBar charismaBar;
		private final AttributeBar beautyBar;
		private final AttributeBar resistanceBar;
		private final AttributeBar knowledgeBar;
		
		private final Sprite profilePhotos;

		private final DrawableImage eliminatedX;

		//#if TOUCH == "true"
			private final Rectangle avatarArea = new Rectangle();
		//#endif
	//</editor-fold>


	public StatsScreen( GameMIDlet midlet, int screenID ) throws Exception {
		super( 50, midlet, screenID, BARS_MIDDLE, true, COLOR_BACKGROUND );

		this.participants = GameBoard.getParticipants();
		this.eliminateds = GameBoard.getEliminatedParticipants();
		this.nomineeA = GameBoard.getNomineeA();
		this.nomineeB = GameBoard.getNomineeB();

		// para a tela de stats, queremos que o botao de OK aja
		// como um botao de foco no participante
		okButton = new DrawableImage( PATH_GAME_IMAGES + "goto.png" );
		okButtonPressed = new DrawableImage( PATH_GAME_IMAGES + "gotop.png" );
		okButton.defineReferencePixel( ANCHOR_CENTER );
		okButtonPressed.defineReferencePixel( ANCHOR_CENTER );

		// imagens dos participantes
		profilePhotos = new Sprite(PATH_CHARSELECTION_IMAGES + "participants");
		profilePhotos.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
		insertDrawable(profilePhotos);

		eliminatedX = new DrawableImage( PATH_CHARSELECTION_IMAGES + "x.png" );
		eliminatedX.defineReferencePixel( ANCHOR_CENTER );
		eliminatedX.setVisible( false );
		insertDrawable( eliminatedX );

		nominee = new MarqueeLabel( FONT_MENU, TEXT_NOMINATED );
		nominee.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		nominee.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		insertDrawable( nominee );
		
		nameLabel = new MarqueeLabel( FONT_TEXT_BLACK, "xxxxxxxx" );
		nameLabel.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		nameLabel.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );

		// descrições do participante
		favoriteHobby = new MarqueeLabel( FONT_TEXT_BLACK, "xxxxxxxx" );
		favoriteHobby.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		favoriteHobby.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		favoriteFood = new MarqueeLabel( FONT_TEXT_BLACK, "xxxxxxxx" );
		favoriteFood.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		favoriteFood.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );		

		// attributos do participante
		charismaBar = new AttributeBar( (short) 0, COLOR_CHARISMA );
		beautyBar = new AttributeBar( (short) 0, COLOR_BEAUTY );
		resistanceBar = new AttributeBar( (short) 0, COLOR_RESISTANCE );
		knowledgeBar = new AttributeBar( (short) 0, COLOR_KNOWLEDGE );

		entryTitleLabel = new Label[7];
		entryBkgPattern = new Pattern[ entryTitleLabel.length ];
		int[] names = { TEXT_NAME, TEXT_HOBBY, TEXT_FOOD, TEXT_CHARISMA, TEXT_BEAUTY, TEXT_RESISTANCE, TEXT_KNOWLEDGE };
		entryValue = new Drawable[]{ nameLabel, favoriteHobby, favoriteFood, charismaBar, beautyBar, resistanceBar, knowledgeBar };
		for( int i = 0; i < entryTitleLabel.length; i++ ) {
			entryBkgPattern[i] = new Pattern( COLOR_OPTIONS_BACKGROUND );
			insertDrawable( entryBkgPattern[i] );
			entryTitleLabel[i] = new Label( FONT_MENU, names[i] );
			insertDrawable( entryTitleLabel[i] );
		}

		insertDrawable(nameLabel);
		insertDrawable(charismaBar);
		insertDrawable(beautyBar);
		insertDrawable(resistanceBar);
		insertDrawable(knowledgeBar);
		insertDrawable(favoriteFood);
		insertDrawable(favoriteHobby);

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		currentIndex = 0;
		maxIndex = participants.length + eliminateds.length;
		setIndex(currentIndex);
	}


	public void setSize(int width, int height) {
		super.setSize(width, height);

		profilePhotos.setRefPixelPosition( middleBarArea.width >> 1, middleBarArea.y + middleBarArea.height );
		eliminatedX.setRefPixelPosition( profilePhotos.getPosX() + ( profilePhotos.getWidth() >> 1 ), profilePhotos.getPosY() + ( profilePhotos.getHeight() >> 1 ));

		final int h = NanoMath.min( freeArea.height / entryTitleLabel.length, CHAR_SELECTION_TEXT_PATTERN ) ;
		final int w = NanoMath.min( freeArea.width - ( CHAR_SELECTION_ENTRY_MARGIN << 1 ), SCREEN_FREEAREA_MAX_WIDTH );
		final int offsetX = ( freeArea.width - w ) >> 1;
		final int spacing = ( freeArea.height - ( h * entryTitleLabel.length ) ) / ( entryTitleLabel.length + 1 );
		int y = freeArea.y + spacing;

		int maxW = CHAR_SELECTION_BAR_MAX_WIDTH;
		for( int i = 0; i < entryBkgPattern.length; i++ ) {
			entryBkgPattern[i].setSize( w, h );
			entryBkgPattern[i].setPosition( offsetX , y );
			entryTitleLabel[i].setPosition( entryBkgPattern[i].getPosX() + CHAR_SELECTION_TEXT_MARGIN, y + ( ( h - entryTitleLabel[i].getHeight() ) >> 1 ) );

			entryValue[i].setPosition( entryTitleLabel[i].getPosX() + entryTitleLabel[i].getWidth() + CHAR_SELECTION_TEXT_MARGIN, y + ( ( h - entryValue[i].getHeight() ) >> 1 ) );
			entryValue[i].setSize( entryBkgPattern[i].getWidth() - ( ( entryValue[i].getPosX() - entryBkgPattern[i].getPosX() ) + CHAR_SELECTION_ENTRY_MARGIN ), entryValue[i].getHeight() );

			if( entryValue[i] instanceof AttributeBar ) {
				maxW = NanoMath.min( entryValue[i].getWidth(), maxW );
			}

			y += h + spacing;
		}

		favoriteHobby.setTextOffset( favoriteHobby.getWidth() - favoriteHobby.getTextWidth() );
		favoriteFood.setTextOffset( favoriteFood.getWidth() - favoriteFood.getTextWidth() );
		nameLabel.setTextOffset( nameLabel.getWidth() - nameLabel.getTextWidth() );

		// Gambiarra para setar a largura das barras de acordo com a menor
		for( int i = 0; i < entryBkgPattern.length; i++ ) {
			if( entryValue[i] instanceof AttributeBar ) {
				entryValue[i].setPosition( entryBkgPattern[i].getPosX() + entryBkgPattern[i].getWidth() - ( maxW + CHAR_SELECTION_TEXT_MARGIN ), entryValue[i].getPosY() );
				entryValue[i].setSize( maxW, entryValue[i].getHeight() );
			}
		}

		// posicao vertical das setas:
		// caso a tela seja horizontalizada, nao queremos cobrir o logo
		int arrowsY = middleBarArea.y + ( middleBarArea.height >> 1 );
		if( width > height )
			arrowsY = logo.getPosX() + logo.getHeight() + ( ( middleBarArea.y + middleBarArea.height - logo.getPosX() - logo.getHeight() ) >> 1 );

		arrowLeft.setRefPixelPosition( profilePhotos.getPosX() - ( SAFE_MARGIN << 1 ), arrowsY );
		arrowRight.setRefPixelPosition( profilePhotos.getPosX() + profilePhotos.getWidth() + ( SAFE_MARGIN << 1 ), arrowLeft.getRefPixelY() );
		arrowLeftPressed.setRefPixelPosition( arrowLeft.getRefPixelPosition() );
		arrowRightPressed.setRefPixelPosition( arrowRight.getRefPixelPosition() );

		nominee.setPosition( arrowLeftPressed.getPosX() + arrowLeftPressed.getWidth(), profilePhotos.getPosY() + ( profilePhotos.getHeight() >> 1 ) );
		nominee.setSize( arrowRightPressed.getPosX() - nominee.getPosX(), nominee.getHeight() );
		nominee.setTextOffset( ( nominee.getWidth() - nominee.getTextWidth() ) >> 1 );

		//#if TOUCH == "true"
			GestureManager.setTouchArea( arrowLeftArea, arrowLeft.getPosition(), arrowLeft.getSize(), TOUCH_TOLERANCE );
			GestureManager.setTouchArea( arrowRightArea, arrowRight.getPosition(), arrowRight.getSize(), TOUCH_TOLERANCE );
			GestureManager.setTouchArea( avatarArea, profilePhotos.getPosition(), profilePhotos.getSize(), TOUCH_TOLERANCE );
		//#endif
	}
	
	
	public final void setName( String nome ) {
		nameLabel.setText( nome );
		nameLabel.setTextOffset( nameLabel.getWidth() - nameLabel.getTextWidth() );
	}


	public void drawFrontLayer( Graphics g ) {
		( arrowLeftIsPressed ? arrowLeftPressed : arrowLeft ).draw( g );
		( arrowRightIsPressed ? arrowRightPressed : arrowRight ).draw( g );
		( okButtonIsPressed ? okButtonPressed : okButton ).draw( g );
		( backButtonIsPressed ? backButtonPressed : backButton ).draw( g );
	}

	private Participant currentParticipant() {
		return ( currentIndex < participants.length ) ? participants[ currentIndex ] : eliminateds[ currentIndex - participants.length ];
	}
		

	/** 
	 * Atualiza os dados do participante selecionado.
	 */
	protected void updateIndex() {
		Participant p = currentParticipant();
		
		if( currentIndex < participants.length ) eliminatedX.setVisible( false );
		else eliminatedX.setVisible( true );

		if( p == nomineeA || p == nomineeB ) nominee.setVisible( true );
		else nominee.setVisible( false );

		profilePhotos.setFrame( p.getPhotoId() );
		setName( p.getName() );

		// atualizando dados do perfil
		charismaBar.setValue( p.getCharisma() * 100 / ATTRIBUTE_MAX );
		beautyBar.setValue( p.getBeaty() * 100 / ATTRIBUTE_MAX );
		resistanceBar.setValue( p.getResistance() * 100 / ATTRIBUTE_MAX );
		knowledgeBar.setValue( p.getKnowledge() * 100 / ATTRIBUTE_MAX );
		favoriteHobby.setText( GameMIDlet.getFavoriteHobby( p.getId() + 1 ) );
		favoriteFood.setText( GameMIDlet.getFavoriteFood( p.getId() + 1 ) );
		favoriteHobby.setTextOffset( favoriteHobby.getWidth() - favoriteHobby.getTextWidth() );
		favoriteFood.setTextOffset( favoriteFood.getWidth() - favoriteFood.getTextWidth() );
	}


	public void hideNotify(boolean deviceEvent) {
	}

	public void showNotify(boolean deviceEvent) {
	}
	

	//<editor-fold desc="Handle Input">
		public void okAction() {
			GameScreen.setFocus( currentParticipant() );
			midlet.onChoose( null, screenID, TEXT_EXIT );
		}

		public void backAction() {
			midlet.onChoose( null, screenID, TEXT_EXIT );
		}

		
		//#if TOUCH == "true"
			public final void internalHandleTap( int x, int y ) { }
		//#endif
	//</editor-fold>
}
