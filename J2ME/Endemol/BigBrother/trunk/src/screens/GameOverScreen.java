/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
//#if TOUCH == "true"
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.RankingScreen;
	import core.GestureListener;
	import core.GestureManager;
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.GameBoard;
import core.Participant;
import core.ParticleEmitter;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Caio
 */
public class GameOverScreen extends Drawable implements Constants, Updatable, KeyListener, ScreenListener
//#if TOUCH == "true"
		, PointerListener, GestureListener
//#endif
{
	private static final byte ANIM_OPPENING = 0;
	private static final byte ANIM_INFINITY_LOOP = 1;

	private final short ENTER_TIME;
	private static final short ENTER_TIME_WINNER = 2000;
	private static final short ENTER_TIME_LOOSER = 3000;
	private static final short LOOP_TIME = 2000;

	private static final short PLAYER_JUMPS_PER_LOOP = 5;
	
	private static final byte JANITOR_ANIM_WALKING = 0;
	private static final byte JANITOR_ANIM_SWEEPING = 1;
	
	private static final byte JANITOR_ANIMS = JANITOR_ANIM_SWEEPING + 1;

	private static final byte JANITOR_POSITIONS = 6;

	private static final short JANITOR_SWEEPING_CYCLE = 500;
	
	private static final short[] JANITOR_ANIM_TIMES = { 2000, 3000 };
	
	//<editor-fold desc="Fields">
		//<editor-fold desc="Layout Fields">	
			private final Pattern topBar;
			private final Pattern fill;

			private final Pattern bottom;
			private final Pattern left;
			private final Pattern right;
			private final Pattern middle;

			private final DrawableImage topLeft;
			private final DrawableImage topRight;
			private final DrawableImage bottomLeft;
			private final DrawableImage bottomRight;

			private final MarqueeLabel title;
			private final ScrollRichLabel text;
		//</editor-fold>

		//#if TOUCH == "true"
			private final GestureManager gestureManager;
		//#endif

		private final GameMIDlet midlet;
		private final int screenID;
		private final boolean isWinner;

		//<editor-fold desc="Scene Fields">
			private static int W, H, HALF_W;

			private final Rectangle sceneViewport = new Rectangle();
		
			private final Sprite crowd;
			
			private final Pattern bkg;
			private final Pattern crowdPttrn;
			private final Pattern panel;
			private final Pattern floor;

			private final DrawableImage empty;
			private final DrawableImage suitcase;

			private final DrawableImage presenter;
			private final Sprite janitor;
			
			private int anim = 0;
			private int janAnim = 0;

			// Prêmio dado ao jogador por ter completado o jogo.
			private int prize = 0;

			private byte animState = ANIM_OPPENING;
			private byte janAnimState = JANITOR_ANIM_WALKING;
			private byte janCurrentPosition = 0;
			
			private final Point[] janitorPositions;

			private final Point presenterInitialPosition = new Point();
			private final Point presenterFinalPosition = new Point();

			private final Image pins;
			private final Image pinsBW;

			private final Point[] positions = new Point[ 7 ];
			private final Point[] finalPositions = new Point[ 7 ];
			private final Point[] initialPositions = new Point[ 7 ];
			private final byte[] ids = new byte[ 7 ];

			private final Point playerDesiredPosition = new Point();
			private final Point playerPosition = new Point();
			private final byte playerId;

			private boolean hasToDraw = true;

			private final ParticleEmitter moneyRain;
		//</editor-fold>
	//</editor-fold>

	public GameOverScreen( GameMIDlet midlet, int screenID ) throws Exception {
		super();

		
		Participant[] participantsLeft = GameBoard.getParticipants();
		this.isWinner = participantsLeft.length == 1;
		
		ENTER_TIME = ( isWinner ) ? ENTER_TIME_LOOSER : ENTER_TIME_WINNER;

		if( GameBoard.getPlayerParticipant() != null ) {
			playerId = (byte)GameBoard.getPlayerParticipant().getPhotoId();
		} else {
			playerId = (byte)6; //GameBoard.getPlayerParticipant().getPhotoId();
		}
		for( int i = 0; i < playerId; i++ ) {
			ids[i] = (byte)i;
		}
		for( int i = playerId; i < ids.length; i++ ) {
			ids[i] = (byte)i;
		}

		this.screenID = screenID;
		this.midlet = midlet;

		String prizeFormatted = "";
		Customer currentCustomer = NanoOnline.getCurrentCustomer();
		if( currentCustomer != null &&  currentCustomer.isRegistered() ) {

			// calculando prêmio
			switch( participantsLeft.length ) {
				case 1:			prize = 1000000;	prizeFormatted = "1.000.000";	break;
				case 2:			prize =  500000;	prizeFormatted =   "500.000";	break;
				case 3:			prize =  100000;	prizeFormatted =   "100.000";	break;
				case 4:			prize =   50000;	prizeFormatted =    "50.000";	break;
				case 5:			prize =   10000;	prizeFormatted =    "10.000";	break;
				default:		prize =		  0;	prizeFormatted =	     "0";	break;
			}
			RankingScreen.setHighScore( RANKING_INDEX_ACC_SCORE, currentCustomer.getId(), prize );
		}
				
		DrawableImage img = new DrawableImage( PATH_IMAGES + "border.png" );
		
		topLeft = new DrawableImage( img );
		topRight = new DrawableImage( img );
		bottomLeft = new DrawableImage( img );
		bottomRight = new DrawableImage( img );

		topLeft.setTransform( TRANS_MIRROR_H );
		bottomRight.setTransform( TRANS_MIRROR_V );
		bottomLeft.setTransform( TRANS_MIRROR_V | TRANS_MIRROR_H );


		DrawableImage bdrh = new DrawableImage( PATH_IMAGES + "borderh.png" );
		bottom = new Pattern( bdrh );
		middle = new Pattern( bdrh );

		DrawableImage bdrv = new DrawableImage( PATH_IMAGES + "borderv.png" );
		left = new Pattern( bdrv );
		right = new Pattern( bdrv );

		left.setTransform( TRANS_MIRROR_V );


		topBar = new Pattern( new DrawableImage( GameMIDlet.getBar() ) );

		fill = new Pattern( isWinner ? new DrawableImage( PATH_IMAGES + "pattern_good.png" ) : new DrawableImage( PATH_IMAGES + "pattern_bad.png" ) );
		title = new MarqueeLabel( FONT_MENU_BIG, "" );
		title.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		title.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		title.setText( isWinner ? TEXT_WINNER_SCREEN_TITLE : TEXT_ELIMINATED_SCREEN_TITLE, false );

		text = new ScrollRichLabel( new RichLabel( isWinner ? FONT_TEXT_BLACK : FONT_TEXT_WHITE, 
			GameMIDlet.getText( isWinner ? TEXT_WINNER_SCREEN_TEXT: TEXT_ELIMINATED_SCREEN_TEXT ) +
			( prize > 0? GameMIDlet.getText( TEXT_PRIZE_WON ) + prizeFormatted : "" ) ) );
		text.setAutoScroll( true );

		//<editor-fold desc="Scene Elements">
			bkg = new Pattern( COLOR_END_BACKGROUND );
			
			for( int i = 0; i < finalPositions.length; i++ ) {
				initialPositions[i] = new Point();
				finalPositions[i] = new Point();
				positions[i] = new Point();
			}

			if( isWinner ) {
				pins = Image.createImage( PATH_GAME_IMAGES + "game_pins.png" );
				janitorPositions = null;
				crowd = new Sprite( PATH_END + "crowd" );
				crowdPttrn = new Pattern( crowd );
				empty = null;
				janitor = null;
				suitcase = null;			
				moneyRain = new ParticleEmitter( PARTICLES_SPEED, new Sprite( PATH_END + "money" ), ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, PARTICLES_MAX, (short)1000, 50, 100);
			} else {
				pins = null;
				janitorPositions = new Point[ JANITOR_POSITIONS ];
				for( int i = 0; i < janitorPositions.length; i++ ) janitorPositions[i] = new Point();
				janAnimState = JANITOR_ANIM_WALKING;
				crowd = null;
				moneyRain = null;
				janitor = new Sprite( PATH_END + "janitor" );
				janitor.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				suitcase = new DrawableImage( PATH_END + "suitcase.png" );
				suitcase.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				empty = new DrawableImage( PATH_END + "empty_crowd.png" );
				empty.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				crowdPttrn = new Pattern( new DrawableImage( PATH_END + "arquibancada.png" ) );
			}
				
			presenter = new DrawableImage( PATH_END + "apresentador.png" );
			presenter.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );

			pinsBW = Image.createImage( PATH_END + "pinsBW.png" );

			W = pinsBW.getWidth() / NUMBER_OF_PINS;
			HALF_W = W >> 1;

			H = pinsBW.getHeight();

			panel = new Pattern( new DrawableImage( PATH_END + "painel.png" ) );

			floor = new Pattern( COLOR_END_FLOOR );
		//</editor-fold>

		//#if TOUCH == "true"
			gestureManager = new GestureManager( this );
		//#endif
	}
	

	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		hasToDraw = true;

		title.setSize( getWidth() - ( topRight.getWidth() + topLeft.getWidth() + ( END_SAFE_MARGIN << 1 ) ), 0 );
		title.setSize( getWidth() - ( topRight.getWidth() + topLeft.getWidth() + ( END_SAFE_MARGIN << 1 ) ), title.getFont().getHeight() );

		title.setPosition( topRight.getWidth() + END_SAFE_MARGIN, END_TITLE_SAFE_MARGIN );
		title.setTextOffset( NanoMath.max( 0, ( title.getWidth() - title.getTextWidth() ) / 2 ) );

		text.setSize( getWidth() - ( topRight.getWidth() + topLeft.getWidth() + ( END_SAFE_MARGIN << 1 ) ), 0 );
		text.setSize( getWidth() - ( topRight.getWidth() + topLeft.getWidth() + ( END_SAFE_MARGIN << 1 ) ), NanoMath.min( text.getTextTotalHeight(), getHeight() / 3 ) );
		
		text.setPosition( topRight.getWidth() + END_SAFE_MARGIN, getHeight() - ( text.getHeight() + (0 * END_SAFE_MARGIN ) + bottom.getFill().getHeight() ) );
		
		final int diff = NanoMath.min( ( title.getHeight() + ( END_TITLE_SAFE_MARGIN << 1 ) + CHAR_SELECTION_PATTERN_BAR_BORDER ) - topBar.getFill().getHeight(), 0 );
		
		topLeft.setPosition( 0, diff );
		left.setPosition( 0, topLeft.getPosY() + topLeft.getHeight() );
		left.setSize( left.getFill().getWidth(), getHeight() - ( bottomLeft.getHeight() + left.getPosY() ) );
		bottomLeft.setPosition( 0, left.getPosY() + left.getHeight() );
		topBar.setPosition( topLeft.getWidth(), diff );
		topBar.setSize( getWidth() - ( topRight.getWidth() + topBar.getPosX() ), topBar.getFill().getHeight() );
		topRight.setPosition( topBar.getPosX() + topBar.getWidth(), diff );
		right.setPosition( topBar.getPosX() + topBar.getWidth(), topRight.getPosY() + topRight.getHeight() );
		right.setSize( right.getFill().getWidth(), getHeight() - ( bottomRight.getHeight() + right.getPosY() ) );
		bottomRight.setPosition( topBar.getPosX() + topBar.getWidth(), right.getPosY() + right.getHeight() );
		
		bottom.setPosition( bottomLeft.getPosX() + bottomLeft.getWidth(), getHeight() - bottom.getFill().getHeight() );
		bottom.setSize( topBar.getWidth(), bottom.getFill().getHeight() );
		fill.setSize( getWidth() - ( left.getWidth() + right.getWidth() ), NanoMath.min( text.getHeight() + ( END_SAFE_MARGIN << 1 ), getHeight() / 3 ) );
		fill.setPosition( left.getPosX() + left.getWidth(), bottom.getPosY() - fill.getHeight() );
		middle.setPosition( bottomLeft.getPosX() + bottomLeft.getWidth(), fill.getPosY() - middle.getFill().getHeight() );
		middle.setSize( topBar.getWidth(), bottom.getFill().getHeight() );
		
		sceneViewport.set( topBar.getPosX(), topBar.getPosY() + topBar.getHeight(), topBar.getWidth(), middle.getPosY() - ( topBar.getPosY() + topBar.getHeight() ) );

		floor.setPosition( sceneViewport.x, sceneViewport.y + ( sceneViewport.height >> 1 ) );
		floor.setSize( sceneViewport.width, ( sceneViewport.y + sceneViewport.height ) - ( floor.getPosY() ) );

		panel.setSize( sceneViewport.width, panel.getFill().getHeight() );
		panel.setPosition( sceneViewport.x, floor.getPosY() - panel.getHeight() );

		presenterInitialPosition.set( sceneViewport.x + ( sceneViewport.width >> 1 ) - ( ( presenter.getWidth() >> 1 ) + END_SAFE_MARGIN ), floor.getPosY() + ( floor.getHeight() >> 1 ) );
		presenterFinalPosition.set( sceneViewport.x - ( presenter.getWidth() ), floor.getPosY() + ( floor.getHeight() >> 1 ) );

		if( isWinner ) {
			moneyRain.setPosition( sceneViewport.x, sceneViewport.y );
			moneyRain.setSize( sceneViewport.width, sceneViewport.height );
			moneyRain.reset();
			playerDesiredPosition.set( sceneViewport.x + ( sceneViewport.width >> 1 ) + ( HALF_W ), floor.getPosY() + ( floor.getHeight() >> 1 ) );
			final int fwh = ( floor.getWidth() - END_SPOT_WIDTH ) >> 1;

			finalPositions[0].set( floor.getPosX() + ( ( fwh * 25 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 30 ) / 100 ) );
			finalPositions[1].set( floor.getPosX() + floor.getWidth() - ( ( fwh * 75 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 25 ) / 100 ) );
			finalPositions[2].set( floor.getPosX() + ( ( fwh * 70 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 35 ) / 100 ) );
			finalPositions[3].set( floor.getPosX() + floor.getWidth() - ( ( fwh * 30 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 40 ) / 100 ) );
			finalPositions[4].set( floor.getPosX() + ( ( fwh * 30 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 60 ) / 100 ) );
			finalPositions[5].set( floor.getPosX() + floor.getWidth() - ( ( fwh * 50 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 80 ) / 100 ) );
			finalPositions[6].set( floor.getPosX() + ( ( fwh * 60 ) / 100 ), floor.getPosY() + ( ( floor.getHeight() * 90 ) / 100 ) );

			if( animState == ANIM_INFINITY_LOOP ) {
				for( int i = 0; i < positions.length; i++ ) {
					positions[i].set( finalPositions[i] );
				}
			}

			initialPositions[0].set( floor.getPosX() - W, floor.getPosY() + ( ( floor.getHeight() * 20 ) / 100 ) );
			initialPositions[1].set( floor.getPosX() + floor.getWidth() + W, floor.getPosY() + ( ( floor.getHeight() * 15 ) / 100 ) );
			initialPositions[2].set( floor.getPosX() - W, floor.getPosY() + ( ( floor.getHeight() * 27 ) / 100 ) );
			initialPositions[3].set( floor.getPosX() + floor.getWidth() + W, floor.getPosY() + ( ( floor.getHeight() * 37 ) / 100 ) );
			initialPositions[4].set( floor.getPosX() - W, floor.getPosY() + ( ( floor.getHeight() * 63 ) / 100 ) );
			initialPositions[5].set( floor.getPosX() + floor.getWidth() + W, floor.getPosY() + ( ( floor.getHeight() * 95 ) / 100 ) );
			initialPositions[6].set( floor.getPosX() - W, floor.getPosY() + ( ( floor.getHeight() * 100 ) / 100 ) );
		} else {
			empty.setRefPixelPosition( sceneViewport.x + ( sceneViewport.width >> 1 ), panel.getPosY() - END_CROWD_DIFF );
			playerPosition.set( sceneViewport.x + ( sceneViewport.width >> 1 ), floor.getPosY() + ( floor.getHeight() >> 1 ) );

			for( int i = 0; i < janitorPositions.length; i++ ) {
				janitorPositions[i].set( sceneViewport.x + sceneViewport.width + ( janitor.getWidth() >> 1 ) - ( ( ( sceneViewport.width + ( janitor.getWidth() ) ) * i ) / ( janitorPositions.length - 1 ) ),
											floor.getPosY() + ( floor.getHeight() >> 2 ) );
			}

			suitcase.setRefPixelPosition( playerPosition.x + ( END_SPOT_WIDTH_HALF >> 1 ), playerPosition.y );
		}

		crowdPttrn.setSize( sceneViewport.width, crowdPttrn.getFill().getHeight() );
		crowdPttrn.setPosition( sceneViewport.x, panel.getPosY() - ( crowdPttrn.getHeight() + END_CROWD_DIFF ) );

		bkg.setPosition( sceneViewport.x, sceneViewport.y );
		bkg.setSize( sceneViewport.width, sceneViewport.height );

		update( 0 );
	}


	private final int getOffsetX( byte id ) { return id * W; }
	private final int getOffsetY( byte id ) { return 0;	}


	public final void drawParticipant( Graphics g, Image img, Point position, Point offset, byte id ) {
		final int beginX = position.x + offset.x - HALF_W;
		final int beginY = position.y + offset.y - H;
		final int endX = beginX + W;
		final int endY = beginY + H;

		if( endX > 0 && endY > 0  && beginX < ScreenManager.SCREEN_WIDTH && beginY < ScreenManager.SCREEN_HEIGHT ) {
			final int ox = g.getClipX();
			final int oy = g.getClipY();
			final int ow = g.getClipWidth();
			final int oh = g.getClipHeight();

			final Rectangle clip = new Rectangle( beginX, beginY, W, H );
			clip.setInsersection( sceneViewport );

			g.setClip( clip.x, clip.y, clip.width, clip.height );
			g.drawImage( img, beginX - getOffsetX( id ), beginY - getOffsetY( id ), 0 );

			g.setClip( ox, oy, ow, oh );
		}
	}
	
	
	public final void paint( Graphics g ) {
		//#if DEBUG == "true"
			// Desenha retangulo para poder ser visto o frame rate
			g.fillRect( getWidth() - 60, 20, 100, 25 );
		//#endif
			
		if( hasToDraw ) { 
			topLeft.draw( g );
			topRight.draw( g );
			bottomLeft.draw( g );
			bottomRight.draw( g );
			middle.draw( g );
			bottom.draw( g );
			left.draw( g );
			right.draw( g );
			hasToDraw = false;
		}

		// como esses elementos realizam scroll, sempre
		// precisamos redesenhá-los
		topBar.draw( g );
		title.draw( g );
		fill.draw( g );
		text.draw( g );
		
		g.setClip( translate.x + sceneViewport.x, translate.y + sceneViewport.y, sceneViewport.width, sceneViewport.height );
		
		
		bkg.draw( g );
		crowdPttrn.draw( g );
		if( empty != null ) empty.draw( g );
		panel.draw( g );
		floor.draw( g );
		final int scx = translate.x + ( getWidth() >> 1 );
		final int scy = translate.y + floor.getPosY() + ( floor.getHeight() >> 1 );
		
		if( janitor != null ) janitor.draw( g );

		// TODO tirar triangulo já que não podemos usar transparencia
		g.setColor( COLOR_END_LIGHT_2 );
		g.fillTriangle( scx, translate.y, scx - END_SPOT_WIDTH_HALF, scy, scx + END_SPOT_WIDTH_HALF, scy );
		
		// teste para fonte emissora de luz
//		g.setColor( 0xfff000 );
//		g.fillTriangle( scx, translate.y, scx - END_SPOT_WIDTH_HALF, scy, scx + END_SPOT_WIDTH_HALF, scy );
//		g.setColor( 0xfff888 );
//		g.fillTriangle( scx, translate.y, scx - END_SPOT_WIDTH_HALF + 7, scy, scx + END_SPOT_WIDTH_HALF - 7, scy );
//		g.setColor( 0xffffff );
//		g.fillTriangle( scx, translate.y, scx - END_SPOT_WIDTH_HALF + 14, scy, scx + END_SPOT_WIDTH_HALF - 14, scy );

		g.setColor( COLOR_END_LIGHT );
		g.fillArc( scx - END_SPOT_WIDTH_HALF, scy - END_SPOT_HEIGHT_HALF, END_SPOT_WIDTH, END_SPOT_HEIGHT, 0, 360 );

		presenter.draw( g );

		if( isWinner ) {
			for( int i = 0; i < finalPositions.length; i++ ) {
				drawParticipant( g, pinsBW, positions[i], translate, ids[i] );
			}
		}

		drawParticipant( g, ( pins != null ) ? pins : pinsBW, playerPosition, translate, playerId );

		if( suitcase != null ) suitcase.draw( g );

		if( moneyRain != null ) moneyRain.draw( g );
	}


	public void update( int delta ) {
		if( crowd != null ) crowd.update( delta );
		if( moneyRain != null ) moneyRain.update( delta );

		// como esses campos podem estar fazendo scroll
		title.update( delta );
		text.update( delta );

		if( janitor != null ) {
			if( janAnimState < JANITOR_ANIMS ) {
				janAnim += delta;

				while( janAnim >= JANITOR_ANIM_TIMES[ janAnimState ] ) {
					janAnim -= JANITOR_ANIM_TIMES[ janAnimState ];
					janAnimState++;
					if( janAnimState >= JANITOR_ANIMS ) {
						janAnimState = 0;
						janCurrentPosition++;
						if( janCurrentPosition >= ( janitorPositions.length - 1 ) ) {
							janCurrentPosition = 0;
						}

						// Proibe faxineiro de dançar MJ( Michael Jackson ), ou seja andar fazendo moonwalk
						if( janitorPositions[janCurrentPosition].x <= janitorPositions[ janCurrentPosition + 1 ].x ) janitor.setTransform( TRANS_MIRROR_H );
						else janitor.setTransform( TRANS_NONE );
					}
					janitor.setFrame( janAnimState );
				}

				switch( janAnimState ) {
					case JANITOR_ANIM_WALKING:
						janitor.setRefPixelPosition( goJumpping( janitorPositions[janCurrentPosition], janitorPositions[janCurrentPosition + 1], janAnim, JANITOR_ANIM_TIMES[ janAnimState ], WALK_JUMP_WIDTH, WALK_JUMP_HEIGHT ) );
					break;

					case JANITOR_ANIM_SWEEPING:
						if( ( (janAnim / JANITOR_SWEEPING_CYCLE) % 2 ) == 0 ) janitor.setTransform( TRANS_MIRROR_H );
						else janitor.setTransform(  TRANS_NONE );
						janitor.setRefPixelPosition( janitorPositions[janCurrentPosition+1] );
					break;
				}
			}
		}


		anim += delta;
		switch( animState ) {
			case ANIM_OPPENING:
				final int acc = ( anim > ENTER_TIME ) ? ( ENTER_TIME ) : ( anim );
				if( isWinner ) {
					playerPosition.set( playerDesiredPosition );
					if( positions != null ) {
						for( int i = 0; i < positions.length; i++ ) {
							positions[i].set( goJumpping( initialPositions[i], finalPositions[i], acc, ENTER_TIME, WALK_JUMP_WIDTH, WALK_JUMP_HEIGHT ) );
						}
					}
					presenter.setRefPixelPosition( presenterInitialPosition );
				} else {
					presenter.setTransform( TRANS_MIRROR_H );
					presenter.setRefPixelPosition( goJumpping( presenterInitialPosition, presenterFinalPosition, acc, ENTER_TIME, WALK_JUMP_WIDTH, WALK_JUMP_HEIGHT ) );
				}
				if( anim > ENTER_TIME ) {
					anim -= ENTER_TIME;
					animState++;
				}
			break;

			default:
				while( anim > LOOP_TIME ) anim -= LOOP_TIME;
				if( isWinner ) {
					presenter.setRefPixelPosition( presenterInitialPosition );
					playerPosition.set( playerDesiredPosition.x, playerDesiredPosition.y - NanoMath.toInt( NanoMath.abs( NanoMath.sinInt( ( 180 * anim * PLAYER_JUMPS_PER_LOOP ) / LOOP_TIME ) ) * WALK_JUMP_HEIGHT ) );
				} else {

				}
			break;
		}
	}


	public final Point goJumpping( Point intialPos, Point finalPos, int accTime, int totalTime, int width, int height ) {
		final Point diff = new Point( finalPos.x - intialPos.x, finalPos.y - intialPos.y );
		final int noOfJumps = diff.getModule() / width;
		return new Point( intialPos.x + ( ( ( diff.x ) * accTime ) / totalTime ),
				intialPos.y + ( ( ( diff.y ) * accTime ) / totalTime ) - NanoMath.toInt( NanoMath.abs( NanoMath.sinInt( ( 180 * accTime * noOfJumps ) / totalTime ) ) * height ) );
	}


	//<editor-fold desc="Handle Input">
		public void keyPressed( int key ) {
		}

		public void keyReleased( int key ) {
			if( animState == ANIM_INFINITY_LOOP ) {
				switch( key ) {
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_SOFT_MID:
					case ScreenManager.KEY_SOFT_LEFT:
						if( prize > 0 && NanoOnline.getCurrentCustomer() != null ) {
							midlet.onChoose( null, screenID, TEXT_OK );
						} else {
							midlet.onChoose( null, screenID, TEXT_CANCEL );
						}
					break;
				}
			}
		}


		//#if TOUCH == "true"
			// Eventos que apenas repassamos para o gerenciador de gestos
			public void onPointerDragged( int x, int y ) { gestureManager.onPointerDragged( x, y );	}
			public void onPointerPressed( int x, int y ) { gestureManager.onPointerPressed( x, y );	}
			public void onPointerReleased( int x, int y ) { gestureManager.onPointerReleased( x, y ); }


			public void handleTap( int x, int y ) {
				if( animState == ANIM_INFINITY_LOOP ) {
					midlet.onChoose( null, screenID, 0 );
				}
			}

			public void handleFirstInteraction( int x, int y ) {
			}

			public void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
			}

			public void handleRelease( int x, int y ) {
			}

			public void gestureIsNotSpot( int x, int y ) {
			}

		//#endif
	//</editor-fold>
	public void hideNotify( boolean deviceEvent ) {
	}
	public void showNotify( boolean deviceEvent ) {
		hasToDraw = true;
	}
}
