package screens;

import br.com.nanogames.components.Label;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.Constants;
import javax.microedition.lcdui.Graphics;


public final class LoadScreen extends UpdatableGroup implements Constants, Updatable, ScreenListener {
	
	//<editor-fold desc="Constants">
		private static final short CHANGE_TEXT_INTERVAL = 600; //Intervalo de atualizacao do texto.
		private static final byte MAX_DOTS = 4;
		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;
	//</editor-fold>

	//<editor-fold desc="Fields">
		private long lastUpdateTime;
		private byte dots;
		private Thread loadThread;
		private final LoadListener listener;
		private boolean painted;
		private boolean active = true;
		private final Label text;
		private short textId = TEXT_LOADING;
	//</editor-fold>

	//<editor-fold desc="Initialization">
		public LoadScreen( LoadListener listener ) throws Exception {
			super(1);
			text = new Label( FONT_MENU );
			text.setText( textId, true );
			insertDrawable(text);

			this.listener = listener;
			ScreenManager.setKeyListener( null );
			//#if TOUCH == "true"
				ScreenManager.setPointerListener( null );
			//#endif

			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}

		public LoadScreen( LoadListener listener, short textId ) throws Exception {
			this( listener );

			// reescolhendo texto adequado
			this.textId = textId;
			text.setText( textId, true );
		}

		public final void setSize( int width, int height ) {
			super.setSize(width, height);
			text.setPosition( ( getWidth() - text.getWidth() ) >> 1 , ( getHeight() - text.getHeight() ) >> 1 );
		}
	//</editor-fold>


	//<editor-fold desc="Update and Draw">
		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;
			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo sÃ£o carregados aqui para evitar sobrecarga do mÃ©todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					if ( painted ) {
						ScreenManager.setKeyListener( null );
						//#if TOUCH == "true"
							ScreenManager.setPointerListener( null );
						//#endif
						final LoadScreen loadScreen = this;
						loadThread = new Thread() {

							public final void run() {
								try {
									MediaPlayer.stop();
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
										e.printStackTrace();
										GameMIDlet.log( e.getMessage() );
									//#endif
									GameMIDlet.setScreen( SCREEN_MAIN_MENU );
								}
							}
						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();
					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( textId );
	//					AppMIDlet.gc();
	//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}
					text.setText( temp );
					text.setPosition( ( getWidth() - text.getWidth() ) >> 1 , ( getHeight() - text.getHeight() ) >> 1 );
					try {
						// permite que a thread de carregamento dos recursos continue sua execuÃ§Ã£o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do método update( int )


		public final void paint( Graphics g ) {
			super.paint( g );
			painted = true;
		}
	//</editor-fold>


	public final void setActive( boolean a ) {
		active = a;
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( size );
	}
}
