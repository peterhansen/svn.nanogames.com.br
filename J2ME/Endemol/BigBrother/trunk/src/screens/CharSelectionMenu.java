package screens;

import br.com.nanogames.components.Drawable;
//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import core.GestureManager;
//#endif
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.AttributeBar;
import core.BasicScreen;
import core.Participant;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Ygor
 */
public final class CharSelectionMenu extends BasicScreen
	//#if TOUCH == "true"
	implements EventListener
	//#endif
{
	//<editor-fold desc="Constants">
		private static final int MAX_NAME = 20;
	//</editor-fold>

	//<editor-fold desc="Fields">		
		private boolean participantSelected = false;
			
		private final Label[] entryTitleLabel; // { name, hobby, food, charisma, beauty, resistance, knowlegde }
		private final Pattern[] entryBkgPattern; // { name, hobby, food, charisma, beauty, resistance, knowlegde }
		private final Drawable[] entryValue; // { name, hobby, food, charisma, beauty, resistance, knowlegde }
		
		private final TextBox name;
		private final MarqueeLabel nameLabel;
		private final MarqueeLabel favoriteHobby;
		private final MarqueeLabel favoriteFood;
		
		// barras de atributos
		private final AttributeBar charismaBar;
		private final AttributeBar beautyBar;
		private final AttributeBar resistanceBar;
		private final AttributeBar knowledgeBar;
		
		private final Sprite profilePhotos;
		
		//#if TOUCH == "true"
			private final TouchKeyPad keyPad;
			private final Rectangle avatarArea = new Rectangle();
		//#endif
	//</editor-fold>

	public CharSelectionMenu(GameMIDlet midlet, int screenID, int participants) throws Exception {
		// TODO: 50???
		super( 50, midlet, screenID, BARS_MIDDLE, true, COLOR_BACKGROUND );

		// imagens dos participantes
		profilePhotos = new Sprite(PATH_CHARSELECTION_IMAGES + "participants");
		profilePhotos.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
		insertDrawable(profilePhotos);

		// informações do participante
		name = new TextBox( GameMIDlet.GetFont(FONT_TEXT_BLACK), "", MAX_NAME, TextBox.INPUT_MODE_ANY );
		name.setEnabled(true);

		// criando container do nome
		final Pattern caret = new Pattern( 0x005500 );
		caret.setSize( 1, GameMIDlet.GetFont(FONT_TEXT_BLACK).getHeight() );
		name.setCaret( caret );
		name.setPosition( 10, 0 );
		final LineBorder brd = new LineBorder( 0xffff00, LineBorder.TYPE_SIMPLE );
		brd.setFillColor( COLOR_BACKGROUND );
		name.setBorder( brd );
		
		nameLabel = new MarqueeLabel( FONT_TEXT_BLACK, "xxxxxxxx" );
		nameLabel.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		nameLabel.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );

		// descrições do participante
		favoriteHobby = new MarqueeLabel( FONT_TEXT_BLACK, "xxxxxxxx" );
		favoriteHobby.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		favoriteHobby.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		favoriteFood = new MarqueeLabel( FONT_TEXT_BLACK, "xxxxxxxx" );
		favoriteFood.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		favoriteFood.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );		

		// attributos do participante
		charismaBar = new AttributeBar( (short) 0, COLOR_CHARISMA );
		beautyBar = new AttributeBar( (short) 0, COLOR_BEAUTY );
		resistanceBar = new AttributeBar( (short) 0, COLOR_RESISTANCE );
		knowledgeBar = new AttributeBar( (short) 0, COLOR_KNOWLEDGE );

		entryTitleLabel = new Label[7];
		entryBkgPattern = new Pattern[ entryTitleLabel.length ];
		int[] names = { TEXT_NAME, TEXT_HOBBY, TEXT_FOOD, TEXT_CHARISMA, TEXT_BEAUTY, TEXT_RESISTANCE, TEXT_KNOWLEDGE };
		entryValue = new Drawable[]{ nameLabel, favoriteHobby, favoriteFood, charismaBar, beautyBar, resistanceBar, knowledgeBar };
		for( int i = 0; i < entryTitleLabel.length; i++ ) {
			entryBkgPattern[i] = new Pattern( COLOR_OPTIONS_BACKGROUND );
			insertDrawable( entryBkgPattern[i] );
			entryTitleLabel[i] = new Label( FONT_MENU, names[i] );
			insertDrawable( entryTitleLabel[i] );
		}

		insertDrawable(charismaBar);
		insertDrawable(beautyBar);
		insertDrawable(resistanceBar);
		insertDrawable(knowledgeBar);
		insertDrawable(favoriteFood);
		insertDrawable(favoriteHobby);
		
		//#if TOUCH == "true"
			if( ScreenManager.getInstance().hasPointerEvents() ) {
				final LineBorder lb = new LineBorder( COLOR_BACKGROUND, LineBorder.TYPE_ROUND_RAISED );
				lb.setFillColor( COLOR_OPTIONS_BACKGROUND );
				keyPad = new TouchKeyPad(GameMIDlet.GetFont(FONT_TEXT_BLACK), new DrawableImage(PATH_ONLINE + "clear.png"), new DrawableImage(PATH_ONLINE + "shift.png"), lb, COLOR_KEYPAD_BACKGROUD, COLOR_KEYPAD_BACKGROUN_TRANSITION_DECAL, COLOR_KEYPAD_BACKGROUN_DECAL );
				keyPad.setTextBox( null );
				keyPad.addEventListener( this );
			} else {
				keyPad = null;
			}
		//#endif

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

		currentIndex = 0;
		maxIndex = PARTICIPANTS_NUMBER;
		setIndex(currentIndex);
	}


	public void setSize(int width, int height) {
		super.setSize(width, height);

		profilePhotos.setRefPixelPosition( middleBarArea.width >> 1, middleBarArea.y + middleBarArea.height );

		final int h = NanoMath.min( freeArea.height / entryTitleLabel.length, CHAR_SELECTION_TEXT_PATTERN ) ;
		final int w = NanoMath.min( freeArea.width - ( CHAR_SELECTION_ENTRY_MARGIN << 1 ), SCREEN_FREEAREA_MAX_WIDTH );
		final int offsetX = ( freeArea.width - w ) >> 1;
		final int spacing = ( freeArea.height - ( h * entryTitleLabel.length ) ) / ( entryTitleLabel.length + 1 );
		int y = freeArea.y + spacing;

		int maxW = CHAR_SELECTION_BAR_MAX_WIDTH;
		for( int i = 0; i < entryBkgPattern.length; i++ ) {
			entryBkgPattern[i].setSize( w, h );
			entryBkgPattern[i].setPosition( offsetX , y );
			entryTitleLabel[i].setPosition( entryBkgPattern[i].getPosX() + CHAR_SELECTION_TEXT_MARGIN, y + ( ( h - entryTitleLabel[i].getHeight() ) >> 1 ) );

			entryValue[i].setPosition( entryTitleLabel[i].getPosX() + entryTitleLabel[i].getWidth() + ( CHAR_SELECTION_TEXT_MARGIN << 1 ), y + ( ( h - entryValue[i].getHeight() ) >> 1 ) );
			entryValue[i].setSize( entryBkgPattern[i].getWidth() - ( ( entryValue[i].getPosX() - entryBkgPattern[i].getPosX() ) + ( CHAR_SELECTION_ENTRY_MARGIN ) ), entryValue[i].getHeight() );

			if( entryValue[i] instanceof AttributeBar ) {
				maxW = NanoMath.min( entryValue[i].getWidth(), maxW );
			}

			y += h + spacing;
		}

		favoriteHobby.setTextOffset( favoriteHobby.getWidth() - favoriteHobby.getTextWidth() );
		favoriteFood.setTextOffset( favoriteFood.getWidth() - favoriteFood.getTextWidth() );

		// Gambiarra para setar a largura das barras de acordo com a menor
		for( int i = 0; i < entryBkgPattern.length; i++ ) {
			if( entryValue[i] instanceof AttributeBar ) {
				entryValue[i].setPosition( entryBkgPattern[i].getPosX() + entryBkgPattern[i].getWidth() - ( maxW + CHAR_SELECTION_TEXT_MARGIN ), entryValue[i].getPosY() );
				entryValue[i].setSize( maxW, entryValue[i].getHeight() );
			}
		}

		name.setSize( name.getSize() );
		//#if TOUCH == "true"
			if( keyPad != null ) {
				keyPad.setSize( getWidth(), NanoMath.min( TOUCH_KEYPAD_SIZE, getHeight() - nameLabel.getHeight() ) );
				keyPad.setPosition( 0, getHeight() - keyPad.getHeight() );

				GestureManager.setTouchArea( arrowLeftArea, arrowLeft.getPosition(), arrowLeft.getSize(), TOUCH_TOLERANCE );
				GestureManager.setTouchArea( arrowRightArea, arrowRight.getPosition(), arrowRight.getSize(), TOUCH_TOLERANCE );
				GestureManager.setTouchArea( avatarArea, profilePhotos.getPosition(), profilePhotos.getSize(), TOUCH_TOLERANCE );

				name.setPosition( nameLabel.getPosX(), ( ( nameLabel.getPosY() + nameLabel.getHeight() > keyPad.getPosY() ) && ScreenManager.getInstance().hasPointerEvents() ) ? ( keyPad.getPosY() - nameLabel.getHeight() ) : (nameLabel.getPosY() ) );
			} else {
				name.setPosition( nameLabel.getPosition() );
			}
		//#else
//# 			name.setPosition( name.getPosition() );
		//#endif
		nameLabel.setTextOffset( nameLabel.getWidth() - nameLabel.getTextWidth() );

		// posicao vertical das setas:
		// caso a tela seja horizontalizada, nao queremos cobrir o logo
		int arrowsY = middleBarArea.y + ( middleBarArea.height >> 1 );
		if( width > height )
			arrowsY = logo.getPosX() + logo.getHeight() + ( ( middleBarArea.y + middleBarArea.height - logo.getPosX() - logo.getHeight() ) >> 1 );

		arrowLeft.setRefPixelPosition( profilePhotos.getPosX() - ( SAFE_MARGIN << 1 ), arrowsY );
		arrowRight.setRefPixelPosition( profilePhotos.getPosX() + profilePhotos.getWidth() + ( SAFE_MARGIN << 1 ), arrowLeft.getRefPixelY() );
		arrowLeftPressed.setRefPixelPosition( arrowLeft.getRefPixelPosition() );
		arrowRightPressed.setRefPixelPosition( arrowRight.getRefPixelPosition() );
	}
	
	
	public final void setName( String nome ) {
		name.setText( nome );
		name.setSize( nameLabel.getWidth(), name.getHeight() );
		nameLabel.setText( nome );
		nameLabel.setTextOffset( nameLabel.getWidth() - nameLabel.getTextWidth() );
	}


	public final void hideTextBox() {
		name.setFocus( false );
		setName( name.getText() );
		//#if TOUCH == "true"
			if( keyPad != null && keyPad.getTextBox() != null ) {
				keyPad.setTextBox( null );
			}
		//#endif
	}

	public final void showTextBox() {
		setName( nameLabel.getText() );
		name.setFocus( true );
		//#if TOUCH == "true"
			if( keyPad != null && ( keyPad.getTextBox() != name ) ) {
				keyPad.setTextBox( name );
			}
		//#endif
	}

	public final void changeTextBoxFocus() {
		if( name.hasFocus() ) {
			hideTextBox();
		} else {
			showTextBox();
		}
	}


	public void drawFrontLayer( Graphics g ) {

		( arrowLeftIsPressed ? arrowLeftPressed : arrowLeft ).draw( g );
		( arrowRightIsPressed ? arrowRightPressed : arrowRight ).draw( g );
		( okButtonIsPressed ? okButtonPressed : okButton ).draw( g );
		( backButtonIsPressed ? backButtonPressed : backButton ).draw( g );

		if( name.hasFocus() ) { name.draw( g ); }
		else { nameLabel.draw( g ); }

		//#if TOUCH == "true"
			if( keyPad!= null && name.hasFocus() ) keyPad.draw( g );
		//#endif
	}

	//#if TOUCH == "true"
		public final void eventPerformed( Event evt ) {
			if( name.hasFocus() ) okAction();
			else hideTextBox();
		}
	//#endif
		

	/** 
	 * Atualiza os dados do participante selecionado.
	 */
	protected void updateIndex() {
		profilePhotos.setFrame( Participant.photosID(currentIndex) );
		setName( GameMIDlet.getName(currentIndex + 1) );

		// atualizando dados do perfil
		charismaBar.setValue(Participant.getInitialAttributes(currentIndex, ATTRIBUTE_CHARISMA));
		beautyBar.setValue(Participant.getInitialAttributes(currentIndex, ATTRIBUTE_BEAUTY));
		resistanceBar.setValue(Participant.getInitialAttributes(currentIndex, ATTRIBUTE_RESISTANCE));
		knowledgeBar.setValue(Participant.getInitialAttributes(currentIndex, ATTRIBUTE_KNOWLEDGE));
		favoriteHobby.setText( GameMIDlet.getFavoriteHobby(currentIndex + 1) );
		favoriteFood.setText( GameMIDlet.getFavoriteFood(currentIndex + 1) );
		favoriteHobby.setTextOffset( favoriteHobby.getWidth() - favoriteHobby.getTextWidth() );
		favoriteFood.setTextOffset( favoriteFood.getWidth() - favoriteFood.getTextWidth() );
	}


	public void hideNotify(boolean deviceEvent) {
	}

	public void showNotify(boolean deviceEvent) {
		if (!MediaPlayer.isPlaying()) {
			MediaPlayer.play(SOUND_FIRST_MUSIC, MediaPlayer.LOOP_INFINITE);
		}
	}
	

	//<editor-fold desc="Handle Input">
		public void okAction() {
			if( !name.hasFocus() ) {
				changeTextBoxFocus();
			} else {
				GameMIDlet.setPlayerId( currentIndex );
				midlet.onChoose(null, screenID, TEXT_CONTINUE);
			}
		}

		public void backAction() {
			if( !name.hasFocus() ) {
				midlet.onChoose( null, screenID, TEXT_BACK );
			} else {
				hideTextBox();
			}
		}
		
		
		public void keyPressed( int key ) { 
			if( name.hasFocus() ) {
				internalKeyPressed( key );
			} else {
				super.keyPressed( key );
			}
		}
		
		public void keyReleased( int key ) {
			if( name.hasFocus() ) {
				internalKeyReleased( key );
			} else {
				super.keyReleased( key );
			}
		}


		public void internalKeyPressed( int key ) {
			if( name.hasFocus() )
				name.keyPressed( key );
		}

		public void internalKeyReleased( int key ) {
			if( name.hasFocus() ) {
				name.keyReleased( key );
				switch( key ) {
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_SOFT_LEFT:
						okAction();
					break;

					case ScreenManager.KEY_UP:
					case ScreenManager.KEY_DOWN:
						changeTextBoxFocus();
					break;
				}
			} else {
				switch( key ) {
					case ScreenManager.KEY_UP:
					case ScreenManager.KEY_DOWN:
					case ScreenManager.KEY_NUM5:
					case ScreenManager.KEY_FIRE:
					case ScreenManager.KEY_SOFT_LEFT:
						changeTextBoxFocus();
					break;
				}
			}
		}

		//#if TOUCH == "true"
			public void internalHandleTap( int x, int y ) {
				if( keyPad!= null ) {
					if( !keyPad.contains( x, y ) && name.hasFocus() ) { hideTextBox(); }
					else if( !keyPad.contains( x, y ) || !name.hasFocus() ) {
						if( nameLabel.contains( x, y ) ) { showTextBox(); }
						else if( avatarArea.contains( x, y ) && !arrowLeftArea.contains( x, y ) && !arrowRightArea.contains( x, y ) ) {
							okAction();
						}
					}
				}
			}

			public void internalFirstInteraction( int x, int y ) {
				if( keyPad!= null ) {
					if( keyPad.getTextBox() != null && keyPad.contains( x, y ) ) {
						keyPad.onPointerPressed( x, y );
					} else {
						if( name.hasFocus() ) name.onPointerPressed( x, y );
					}
				}
			}

			public void internalHandleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				if( name.hasFocus() && keyPad.contains( x, y ) ) {
					keyPad.onPointerDragged( x, y );
				} else {
					if( name.hasFocus() ) name.onPointerDragged( x, y );
				}
			}

			public void internalHandleRelease( int x, int y ) {
				if( name.hasFocus() && keyPad.contains( x, y ) ) {
					keyPad.onPointerReleased( x, y );
				} else {
					if( name.hasFocus() ) name.onPointerReleased( x, y );
				}
			}

			public void internalGestureIsNotSpot( int x, int y ) {
			}
		//#endif
	//</editor-fold>
}
