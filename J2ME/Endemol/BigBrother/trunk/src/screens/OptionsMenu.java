package screens;

import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.BasicScreen;
import core.SubMenu;
import core.MenuLabel;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
class OptionsMenu extends BasicScreen implements MenuListener {

	//<editor-fold desc="Constants">
		public static final byte OPTIONS_0_100_DEFAULT_STEP = 25;
		public static final byte OPTIONS_TYPES = 3;
		public static final byte OPTIONS_ON_MAIN = 0;
		public static final byte OPTIONS_ON_GAME = 1;
		public static final byte OPTIONS_ON_HELP = 2;
		public static final byte OPTIONS_LOGIN = 3;
		public static final byte OPTIONS_NEW_RECORD = 4;
	//</editor-fold>

	private final SubMenu menu;
	private final MenuLabel[] labels;
	private final byte[] valueType;
	private final byte[] value;
	private final int[] options;
	private final int type;

	public OptionsMenu( GameMIDlet midlet, int screen, /* int titleTextId, */int type, Customer c ) throws Exception {
		super( 2, midlet, screen, BARS_NONE, false, COLOR_OPTIONS_BACKGROUND );

		this.type = type;
		final int selection = 0;

		switch ( type ) {
			case OPTIONS_ON_GAME:
				options = new int[]{ TEXT_SOUND, TEXT_VOLUME, TEXT_VIBRATION, TEXT_BACK_MENU, TEXT_EXIT_GAME };
				valueType = new byte[]{ OPTIONS_STATE_ON_OF, OPTIONS_STATE_0_100, OPTIONS_STATE_ON_OF };
			break;

			case OPTIONS_ON_HELP:
				options = new int[]{ TEXT_OPTION_HOW_TO_PLAY, TEXT_OPTION_ATTRIBUTES, TEXT_OPTION_ACTIVITIES, TEXT_OPTION_MINIGAMES, TEXT_OPTION_HAPPENINGS, TEXT_OPTION_CARDS };
				valueType = new byte[]{ };
			break;

			case OPTIONS_LOGIN:
				options = new int[]{ TEXT_PLAY_PROFILE, TEXT_CHOOSE_ANOTHER, TEXT_HELP };
				valueType = new byte[]{ OPTIONS_STATE_CURRENT_CUSTOMER };
			break;

			/*
			case OPTIONS_NEW_RECORD:
				selection = 0;
				options = new int[]{ TEXT_HIGH_SCORES, TEXT_RESTART, TEXT_BACK_MENU, TEXT_EXIT_GAME };
				valueType = new byte[]{ };
				labelProf = null;
			break;*/

			//default:
				/*selection = 0;
				options = new int[]{ TEXT_RESTART, TEXT_BACK_MENU, TEXT_EXIT_GAME };
				valueType = new byte[]{ };
				labelProf = null;*/
			//break;

			default:
				options = new int[]{ TEXT_SOUND, TEXT_VOLUME, TEXT_VIBRATION };
				valueType = new byte[]{ OPTIONS_STATE_ON_OF, OPTIONS_STATE_0_100, OPTIONS_STATE_ON_OF };
			break;
		}

		value = new byte[ valueType.length ];
		
		labels = new MenuLabel[ options.length ];
		for( int i = 0; i < options.length; i++ ) {
			updateValue( i );
			if( i < valueType.length ) {
				labels[i] = new MenuLabel( GameMIDlet.getText( options[i] ) + ":", getText( valueType[i], value[i] ) );
			} else {
				labels[i] = new MenuLabel( GameMIDlet.getText( options[i] ) );
			}
			refreshText( i );
		}

		menu = new SubMenu( this, screen, labels );
		menu.setCurrentIndex( selection );
		insertDrawable( menu );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	public final void updateValue( int id ) {
		switch ( options[id] ) {
			case TEXT_VOLUME:
				value[id] = MediaPlayer.getVolume();
			break;
			case TEXT_VIBRATION:
				value[id] = booleanToValue( MediaPlayer.isVibration() );
			break;
			case TEXT_SOUND:
				value[id] = booleanToValue( !MediaPlayer.isMuted() );
			break;
		}
	}

	//<editor-fold desc="Getters and Setters">
		public final void setBack() {
			menu.setCurrentIndex( options.length - 1 );
		}

		public final static String getText( byte valueType, byte value ) {
			switch ( valueType ) {
				case OPTIONS_STATE_ON_OF:
					if( ( value % 2 ) == 1 ) {
						return GameMIDlet.getText( TEXT_TURN_OFF );
					} else {
						return GameMIDlet.getText( TEXT_TURN_ON );
					}
				case OPTIONS_STATE_0_100:
					return value + "%";

				case OPTIONS_STATE_CURRENT_CUSTOMER:
					final Customer c = NanoOnline.getCurrentCustomer();
					return c.isRegistered() ? c.getNickname().toUpperCase() : GameMIDlet.getText( TEXT_NO_PROFILE );
			}
			return "";
		}

		public final void setSize( int width, int height ) {
			super.setSize( width, height );

			//if( type == OPTIONS_LOGIN ){
			//	labelProf.setSize( freeArea.width, FONT_WHITE_HEIGHT );
			//	labelProf.setPosition( freeArea.x, freeArea.y + ITEM_SPACING );
			//	labelProf.setTextOffset( ( freeArea.width - labelProf.getTextWidth() ) >> 1 );
			//	menu.setPosition( freeArea.x , labelProf.getPosY() + labelProf.getHeight() );
			//	menu.setSize( freeArea.width, freeArea.height - ( ITEM_SPACING + FONT_WHITE_HEIGHT ) );
			//} else {
			int maxWidth = NanoMath.min( SCREEN_FREEAREA_MAX_WIDTH, freeArea.width );
			menu.setSize( maxWidth, freeArea.height );
			int menuYPos = freeArea.y + NanoMath.max( ( freeArea.height - menu.getLabelsHeight() ) >> 1, 0 );
			menu.setPosition( freeArea.x + ( ( freeArea.width - maxWidth ) >> 1 ), menuYPos );
			//}
		}
	//</editor-fold>


	public static void save() {
		MediaPlayer.stop();
		MediaPlayer.saveOptions();
	}


	public final void drawFrontLayer( Graphics g ) {
		if( type == OPTIONS_ON_HELP )
		( okButtonIsPressed ? okButtonPressed : okButton ).draw( g );
		( backButtonIsPressed ? backButtonPressed : backButton ).draw( g );
	}
	
	
	public final void refreshText( int id ) {
		if( id < value.length )
			labels[id].setValueText( getText( valueType[id], value[id] ) );
	}


	private final static byte booleanToValue( boolean bool ) {
		return (byte) ( bool ? 1 : 0 ) ;
	}

	//<editor-fold desc="Handle Events">
		public void onChoose( Menu menu, int id, int index ) {
			if( index < value.length ) {

				switch ( options[index] ) {
					case TEXT_VOLUME:
						//#if DEBUG == "true"
							System.out.println( "MediaPlayer.getVolume() was " + MediaPlayer.getVolume() );
						//#endif

						MediaPlayer.setVolume( ( OPTIONS_0_100_DEFAULT_STEP + ( ( MediaPlayer.getVolume() ) % 100 ) ) );

						//#if DEBUG == "true"
							System.out.println( "MediaPlayer.setVolume( " + OPTIONS_0_100_DEFAULT_STEP + " + ( ( "+ MediaPlayer.getVolume() + " ) % 100 ) ) " );
							System.out.println( "MediaPlayer.getVolume() is " + MediaPlayer.getVolume() + " but should be " + ( OPTIONS_0_100_DEFAULT_STEP + ( ( MediaPlayer.getVolume() ) % 100 ) ) );
						//#endif

						MediaPlayer.play( SOUND_LOCK_OPEN );
					break;

					case TEXT_VIBRATION:
						MediaPlayer.setVibration( !MediaPlayer.isVibration() );
						MediaPlayer.vibrate( 500 );
					break;

					case TEXT_SOUND:
						MediaPlayer.setMute( !MediaPlayer.isMuted() );
						MediaPlayer.play( NanoMath.randInt( SOUND_COLD_SPICY ) );
					break;

					case TEXT_PLAY_PROFILE:
						midlet.onChoose( null, screenID, TEXT_NEW_GAME );
					break;
				}

				updateValue( index );
				refreshText( index );
			} else {
				midlet.onChoose( null, screenID, options[index] );
			}
		}

		public void onItemChanged( Menu menu, int id, int index ) {
			for( int i = 0; i < labels.length; i++ ) {
				if( i == index )
					labels[i].setFont( FONT_MENU );
				else
					labels[i].setFont( FONT_MENU_NO_FILL );
			}
		}
	//</editor-fold>


	//<editor-fold desc="Handle Input">
		public void internalKeyPressed( int key ) {
			if( key != ScreenManager.KEY_CLEAR ) menu.keyPressed( key );
		}

		public final void internalKeyReleased( int key ) {
			if( key != ScreenManager.KEY_CLEAR ) menu.keyReleased( key );
		}

		//#if TOUCH == "true"
			public void internalFirstInteraction( int x, int y ) {
				menu.onPointerPressed( x , y );
			}

			public void internalHandleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				menu.onPointerDragged( x , y );
			}

			public void internalHandleRelease( int x, int y ) {
				menu.onPointerReleased( x , y );
			}
		//#endif
	//</editor-fold>

	//<editor-fold desc="OK Action">
		protected void okAction() {
			if( type == OPTIONS_ON_HELP ) {
				onChoose( null, screenID, menu.getCurrentIndex() );
			} else {
				midlet.onChoose( null, screenID, TEXT_CONTINUE );
			}
		}

		protected void backAction() {
			midlet.onChoose( null, screenID, TEXT_EXIT );
		}
	//</editor-fold>
}
