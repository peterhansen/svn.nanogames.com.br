package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
public class GameSplashScreen extends DrawableGroup implements Constants, ScreenListener, Updatable, KeyListener 
//#if TOUCH == "true"
	, PointerListener
//#endif
{
	//<editor-fold desc="Constants">
		private final int SHOW_TIME_PERCENTAGE = 80;
		private final int BLINK_BY_LOOP = 1;
		private final int BLINK_LOOP = BAR_MOVE_LOOP / BLINK_BY_LOOP;
		private final int BLINK_TIME = ( BLINK_LOOP * SHOW_TIME_PERCENTAGE ) / 100;
		private final int GH2011_TIME = 1000;
		private final int GRAN_HERMANO_TIME = 1000;
	//</editor-fold>

	//<editor-fold desc="Fields">
		//#if VERSION == "GH"
//# 			private final DrawableImage gh;
//# 			private final DrawableImage t2011;
//# 			private final DrawableImage granHermano;
//# 			private final BezierCurve ghBezier = new BezierCurve();
//# 			private final BezierCurve t2011Bezier = new BezierCurve();
//# 			private final BezierCurve granHermanoBezier = new BezierCurve();
//# 			private final Point ghPos = new Point();
//# 			private final Point t2011Pos = new Point();
//# 			private final Point granHermanoPos = new Point();
//# 
//# 			private int gh2011AccTime;
//# 			private int granHermanoAccTime;
//# 
//# 			private final DrawableImage effectTL;
//# 			private final DrawableImage effectBR;	
		//#else
			private final Sprite blink;

			private final DrawableImage logo;

			private final DrawableImage roboVerde;
			private final DrawableImage roboLaranja;
			private final DrawableImage roboAzul;
		//#endif

		private final Pattern topBar;
		private final Pattern bottomBar;

		private int accTime;
		
		private final Pattern bkg;

		private final MarqueeLabel label;

		private boolean hasToDraw;

		private final GameMIDlet midlet;
		private int screenID;

		private boolean wantToClose;
	//</editor-fold>

	//<editor-fold desc="Constructor">
		public GameSplashScreen( GameMIDlet midlet, int screenID ) throws Exception {
			super( 6 );

			this.midlet = midlet;
			this.screenID = screenID;

			hasToDraw = true;
			wantToClose = false;
			accTime = 0;
				
			
			//#if VERSION == "GH"
//# 					bkg = new Pattern( COLOR_MAIN_MENU_BKG );
			//#else				
				//bkg = new Pattern( COLOR_MAIN_MENU_BKG );
				bkg = new Pattern( new DrawableImage( PATH_IMAGES + "pattern_good.png" ) );
			//#endif
			insertDrawable( bkg );

			//#if VERSION == "GH"
//# 				gh2011AccTime = 0;
//# 				granHermanoAccTime = 0;
//# 			
//# 				effectTL = new DrawableImage( PATH_IMAGES + "topFX.png" );
//# 				effectTL.setTransform( TRANS_MIRROR_H | TRANS_MIRROR_V );
//# 				effectBR = new DrawableImage( effectTL.getImage() );
//# 				insertDrawable( effectTL );
//# 				insertDrawable( effectBR );
//# 			
//# 				gh = new DrawableImage( PATH_SPLASH + "GH.png" );
//# 				t2011 = new DrawableImage( PATH_SPLASH + "2011.png" );
//# 				granHermano = new DrawableImage( PATH_SPLASH + "gran_hermano.png" );
//# 				insertDrawable( gh );
//# 				insertDrawable( t2011 );
//# 				insertDrawable( granHermano );
//# 
			//#else
				roboAzul = new DrawableImage( PATH_SPLASH + "robo1.png" );
				roboLaranja = new DrawableImage( PATH_SPLASH + "robo2.png" );
				roboVerde = new DrawableImage( PATH_SPLASH + "robo3.png" );

				roboAzul.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				roboLaranja.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
				roboVerde.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );

				insertDrawable( roboAzul );
				insertDrawable( roboLaranja );
				insertDrawable( roboVerde );

				logo = new DrawableImage( PATH_SPLASH + "logo_BBB.png" );
				logo.defineReferencePixel( ANCHOR_CENTER );
				insertDrawable( logo );

				blink = new Sprite( PATH_SPLASH + "olho_fechando" );
				blink.defineReferencePixel( ANCHOR_CENTER );
				insertDrawable( blink );
				blink.setSequence( 0 );
			//#endif

			DrawableImage img = new DrawableImage( GameMIDlet.getBar() );
			topBar = new Pattern( img );
			//topBar.getFill().setTransform( TRANS_MIRROR_H | TRANS_MIRROR_V );

			bottomBar = new Pattern( img );
			label = new MarqueeLabel( FONT_MENU, TEXT_PRESS_ANY_KEY );
		}
	//</editor-fold>


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		hasToDraw = true;
	
		topBar.setSize( getWidth() + topBar.getFill().getWidth(), topBar.getFill().getHeight() );

		//#if VERSION == "GH"
//# 		// garantindo que o logo não fica muito pequeno na tela
//# 		final int minimumHeight = NanoMath.max( gh.getHeight() + granHermano.getHeight(), height / 2 );
		//#else
			final int minimumHeight = logo.getHeight() + roboVerde.getHeight();
		//#endif
		
//		if( getHeight() <= getWidth() || ( getHeight() < ( topBar.getFill().getHeight() << 1 ) + minimumHeight + ( SAFE_MARGIN << 2 ) ) ) topBar.setPosition( 0, CHAR_SELECTION_PATTERN_BAR_BORDER - topBar.getHeight() );
//		else topBar.setPosition( 0, 0 );
//		
//		bottomBar.setSize( getWidth() + bottomBar.getFill().getWidth(), NanoMath.min ( bottomBar.getFill().getHeight(), NanoMath.max( getHeight() - ( ( topBar.getHeight() + topBar.getPosY() ) + minimumHeight ), label.getFont().getHeight() + CHAR_SELECTION_PATTERN_BAR_BORDER ) ) );
//		bottomBar.setPosition( -bottomBar.getFill().getWidth(), getHeight() - bottomBar.getHeight() );
		
		bottomBar.setSize( getWidth() + bottomBar.getFill().getWidth(), NanoMath.clamp( ( ( getHeight() - ( minimumHeight + SAFE_MARGIN ) ) >> 1 ), label.getFont().getHeight() + CHAR_SELECTION_PATTERN_BAR_BORDER, bottomBar.getFill().getHeight() ) );
		bottomBar.setPosition( -bottomBar.getFill().getWidth(), getHeight() - bottomBar.getHeight() );
		
		topBar.setPosition( 0, NanoMath.clamp( ( getHeight() - ( minimumHeight + SAFE_MARGIN ) ) - bottomBar.getHeight(), 0, topBar.getFill().getHeight() ) - topBar.getHeight() );
		
		//#if DEBUG == "true"
			System.out.println( "pt:" + topBar.getPosition() + " | tH:" + topBar.getHeight() );
			System.out.println( "minimumHeight:" + minimumHeight + " | getHeight():" + getHeight() + " | ( getHeight() - minimumHeight ) >> 1 ):" + ( ( getHeight() - minimumHeight ) >> 1 ) );
			System.out.println( "pb:" + bottomBar.getPosition() + " | bH:" + bottomBar.getHeight() );
		//#endif
		
		bkg.setPosition( 0, topBar.getPosY() + topBar.getHeight() );
		bkg.setSize( getWidth(), bottomBar.getPosY() - ( topBar.getPosY() + topBar.getHeight() ) );
		
		label.setPosition( bottomBar.getPosX() + bottomBar.getFill().getWidth(), bottomBar.getPosY() + ( ( ( getHeight() - bottomBar.getPosY() ) - label.getHeight() ) >> 1 ) );
		label.setSize( bottomBar.getWidth() - bottomBar.getFill().getWidth(), label.getFont().getHeight() );
		label.setTextOffset( ( label.getWidth() - label.getTextWidth() ) >> 1 );

		//#if VERSION == "GH"
//# 			effectTL.setPosition( bkg.getPosX(), bkg.getPosY() );
//# 			effectBR.setPosition( bkg.getWidth() - effectBR.getWidth(), bkg.getPosY() + bkg.getHeight() - effectBR.getHeight() );
//# 		
//# 			ghBezier.destiny.set( bkg.getPosX() + ( bkg.getWidth() >> 1 ) - ( ( gh.getWidth() + LOGO_GH_2011_DISTANCE + t2011.getWidth() ) >> 1 ) , bkg.getPosY() + ( bkg.getHeight() >> 1 ) - ( ( ( t2011.getHeight() + granHermano.getHeight() + LOGO_GH_AND_2011_GRAN_HERMANO_DISTANCE ) >> 1 ) + ( gh.getHeight() - t2011.getHeight() ) ) );
//# 			t2011Bezier.destiny.set( ghBezier.destiny.x + gh.getWidth() + LOGO_GH_2011_DISTANCE, ghBezier.destiny.y + gh.getHeight() - t2011.getHeight() );
//# 			granHermanoBezier.destiny.set( bkg.getPosX() + ( bkg.getWidth() >> 1 ) - ( granHermano.getWidth() >> 1 ), ghBezier.destiny.y+ gh.getHeight() + LOGO_GH_AND_2011_GRAN_HERMANO_DISTANCE );
//# 
//# 			ghBezier.origin.set( bkg.getPosX() + - ( gh.getWidth() ) , ghBezier.destiny.y );
//# 			t2011Bezier.origin.set( bkg.getPosX() + bkg.getWidth(), t2011Bezier.destiny.y );
//# 			granHermanoBezier.origin.set( granHermanoBezier.destiny.x, bkg.getPosY() + bkg.getHeight() );
//# 
//# 			ghBezier.control1.set( ghBezier.origin );
//# 			t2011Bezier.control1.set( t2011Bezier.origin );
//# 			granHermanoBezier.control1.set( granHermanoBezier.origin );
//# 
//# 			final int DIV_MOD = 2;
//# 
//# 			ghBezier.control2.set( ghBezier.destiny.x + ( ( ghBezier.destiny.x - ghBezier.origin.x ) / DIV_MOD ), ghBezier.destiny.y + ( ( ghBezier.destiny.y - ghBezier.origin.y ) / DIV_MOD ) );
//# 			t2011Bezier.control2.set( t2011Bezier.destiny.x + ( ( t2011Bezier.destiny.x - t2011Bezier.origin.x ) / DIV_MOD ), t2011Bezier.destiny.y + ( ( t2011Bezier.destiny.y - t2011Bezier.origin.y ) / DIV_MOD ) );
//# 			granHermanoBezier.control2.set( granHermanoBezier.destiny.x + ( ( granHermanoBezier.destiny.x - granHermanoBezier.origin.x ) / DIV_MOD ), granHermanoBezier.destiny.y + ( ( granHermanoBezier.destiny.y - granHermanoBezier.origin.y ) / DIV_MOD ) );
//# 
//# 			updateBarPosition();
//# 			updateTitleAnimation();
		//#else
			roboAzul.setRefPixelPosition( getWidth() >> 2, bottomBar.getPosY() );
			roboLaranja.setRefPixelPosition( getWidth() >> 1, bottomBar.getPosY() );
			roboVerde.setRefPixelPosition( ( 3 * getWidth() ) >> 2, bottomBar.getPosY() );

			logo.setRefPixelPosition( bkg.getPosX() + ( bkg.getWidth() >> 1 ), NanoMath.max( bkg.getPosY() + ( ( bkg.getHeight() - roboVerde.getHeight() ) >> 1 ), ( logo.getHeight() >> 1 ) ) );
			blink.setRefPixelPosition( logo.getRefPixelPosition() );
		//#endif
		updateBarPosition();
	}


	protected void paint( Graphics g ) {		
		if( hasToDraw ) { super.paint( g ); hasToDraw = false; }

		topBar.draw( g );
		bottomBar.draw( g );
		label.draw( g );
	}


	//<editor-fold desc="ScreenListener Methods">
		public void hideNotify( boolean deviceEvent ) {	}
		public void showNotify( boolean deviceEvent ) {	hasToDraw = true; }

		public void sizeChanged( int width, int height ) { setSize( width, height ); }
	//</editor-fold>

	
	//<editor-fold desc="Update Methods">
		public void updateBarPosition() {
			bottomBar.setPosition( ( ( bottomBar.getFill().getWidth() * accTime ) / BAR_MOVE_LOOP ) - bottomBar.getFill().getWidth() , bottomBar.getPosY() );
			topBar.setPosition( ( -( topBar.getFill().getWidth() * accTime ) / BAR_MOVE_LOOP ), topBar.getPosY() );
		}

		//#if VERSION == "GH"
//# 			private void updateTitleAnimation() {
//# 				ghBezier.getPointAt( ghPos, gh2011AccTime * 100 / GH2011_TIME );
//# 				gh.setPosition( ghPos );
//# 				t2011Bezier.getPointAt( t2011Pos, gh2011AccTime * 100 / GH2011_TIME );
//# 				t2011.setPosition( t2011Pos );
//# 				granHermanoBezier.getPointAt( granHermanoPos, granHermanoAccTime * 100 / GRAN_HERMANO_TIME );
//# 				granHermano.setPosition( granHermanoPos );
//# 				hasToDraw = true;
//# 			}
		//#endif

		public void update( int delta ) {
			accTime += delta;
			
			//#if VERSION == "BBB"
//# 				final int temp = blink.getCurrFrameIndex();
//# 				blink.update( delta );
//# 				if( temp != blink.getCurrFrameIndex() ) hasToDraw = true;
			//#endif

			while( accTime >= BAR_MOVE_LOOP ) {
				accTime -= BAR_MOVE_LOOP;
			}

			updateBarPosition();
			
			//#if VERSION == "GH"
//# 			if( gh2011AccTime < GH2011_TIME ) {
//# 				gh2011AccTime += delta;
//# 				if( gh2011AccTime > GH2011_TIME ) {
//# 					granHermanoAccTime += ( gh2011AccTime - GH2011_TIME );
//# 					gh2011AccTime = GH2011_TIME;
//# 				}
//# 				updateTitleAnimation();
//# 			} else if( granHermanoAccTime < GRAN_HERMANO_TIME ) {
//# 				granHermanoAccTime += delta;
//# 				if( granHermanoAccTime >= GRAN_HERMANO_TIME ) {
//# 					granHermanoAccTime = GRAN_HERMANO_TIME;
//# 					if( wantToClose ) exit();
//# 				}
//# 				updateTitleAnimation();
//# 			}
			//#endif

			if( accTime % BLINK_LOOP < BLINK_TIME ) {
				label.setVisible( true );
			} else {
				label.setVisible( false );
			}
		}
	//</editor-fold>

	private final void exit() {
		//#if VERSION == "GH"
//# 			if( granHermanoAccTime >= GRAN_HERMANO_TIME ) {
		//#endif
			midlet.onChoose( null, screenID, TEXT_EXIT );
		//#if VERSION == "GH"
//# 			} else {
//# 				wantToClose = true;
//# 			}
		//#endif
	}

	//<editor-fold desc="Handle Inputs">
		public void keyPressed( int key ) {	}

		public void keyReleased( int key ) { 
			exit();
		}
		

		public void onPointerDragged( int x, int y ) { }

		public void onPointerPressed( int x, int y ) { }

		public void onPointerReleased( int x, int y ) {	
			exit();
		}
	//</editor-fold>
}
