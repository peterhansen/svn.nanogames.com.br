
package core;

import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;

/**
 * Classe que representa as atividades de que um Comodo oferece,
 * e que podem ser executadas pelo participante do jogador, com um
 * custo de tempo e bonus em atributos associado a ela.
 * @author Caio, Ygor
 * @see Participant
 * @see Room
 */
public class Activity implements Constants {

	private byte id;
	private Room room;

	/** Quantidade máxima de minutos consumidos pela atividade */
	private final short activityTime;

	/** Atributo ao qual a atividade confere bonus */
	private final byte attribute;
	
	/** Bonus concecido ao participante por executar a atividade por minuto */
	private final int attBonus;

	public Activity( byte id, int activityTime, byte attribute ) {
		this.id = id;
		this.activityTime = (short)activityTime;
		this.attribute = attribute;
		this.attBonus = ATTRIBUTE_RAISE_MOD * activityTime / HOUR;
	}

	//<editor-fold desc="Getters and Setters">
		public final byte getId() { return id; }
		public short getActivityTime() { return activityTime; }
		public int getAttBonus() { return attBonus; }
		public byte getAttribute() { return attribute; }
		public Room getRoom() { return room; }
		public final String getActionName() { return GameMIDlet.getText(TEXT_ACTIVITY_SOCIALIZE + id); }
		public final void setRoom( Room room ) { this.room = room; }

	//</editor-fold>

	/**
	 * Tenta fazer com que o Participante execute atividade,
	 * consumindo a quantidade de horas adequadas e garantindo
	 * o bonus relacionado.
	 * @param p Participante que vai executar a atividade.
	 * @return Caso tenha sido possível executar a atividade.
	 */

	// TODO: por enquanto, esse método está desativado.
	// Precisamos determinar onde vamos lidar com esse controle - talvez
	// direto no TimeManager? Ele talvez seja mais capaz de determinar se há
	// tempo ou não para realizar uma atividade
	/*public final boolean doActivity( Participant p ) {

		/try {
			room.placeInFreeSpot(p);
		} catch (Exception e) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
			return false;
		}

		short freeTime = TimeManager.getFreeTime();

		// por enquanto, esse tempo de deslocamento e constante, logo,
		// nao precisamos ter de fato quais sao esses comodos
		short movementTime = GameBoard.timeBetweenRooms( null, null );

		// o tempo minimo para executarmos uma atividade é termos metade
		// do seu tempo maximo disponivel
		short minimumTime = ( short )( movementTime + ( activityTime / 2 ) );
		
		boolean skipToHappening = freeTime < minimumTime;
		if ( skipToHappening ) {
			// nao executamos atividade e pulamos para acontecimento
			//TimeManager.passTime( freeTime );
		} else {
			short availableTime = ( short )( NanoMath.min( freeTime, activityTime ) - movementTime );
			//TimeManager.passTime( availableTime );

			// TODO: aqui podemos adicionamos bônus de atributos, proporcional
			// ao tempo de atividade
		}

		return !skipToHappening;
	}
	*/
}
