package core;

import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;

/**
 * Relatorio diario com informacoes dos acontecimentos do dia e quaisquer penalidades.
 * @author Ygor
 */
public class ReportCard extends GameMessage {
	public ReportCard( byte happeningIndex, byte penaltyState ) {
		super( GameMIDlet.getText( TEXT_CARD_DAY_REPORT ), generateMessageText(happeningIndex, penaltyState), penaltyState == PENALTY_NOPENALTY );
		Participant player = GameBoard.getPlayerParticipant();

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	private static String generateMessageText( byte happeningIndex, byte penaltyState ) {
		String message = "";
		boolean happening = happeningIndex != TimeManager.HAPPENING_NOTHING;
		if( happening ) {
			message += GameMIDlet.getText( TEXT_CARD_HAPPENING_CALL ) +
					GameMIDlet.getText( TEXT_CARD_PARTY - 1 + happeningIndex );
		}

		if(	penaltyState != PENALTY_NOPENALTY ) {
			if( happening )
				message += "\n";

			message += GameMIDlet.getText( TEXT_CARD_PENALTY_WARNING ) +
					GameMIDlet.getText( TEXT_CARD_NO_FOOD -1 + penaltyState ) +
					GameMIDlet.getText( TEXT_CARD_SCOLDING_TEXT );
		}
		return message;
	}

	public void close() {
	}
}
