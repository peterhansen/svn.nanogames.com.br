/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableArc;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;


/**
 *
 * @author Caio
 */
public class MiniGame extends GameMessage {
	
	public static final byte MINIGAME_BUTTON_MASHING = 0;
	public static final byte MINIGAME_TARGET = 1;
	public static final byte MINIGAME_TYPES = 2;
	
	public static final byte MINIGAME_STATE_SHOW_MSG		= 0;
	public static final byte MINIGAME_STATE_COUNTING		= 1;
	public static final byte MINIGAME_STATE_PLAYING			= 2;
	public static final byte MINIGAME_STATE_FINISH_ANIM		= 3;
	public static final byte MINIGAME_STATE_SHOW_RESULTS	= 4;
	
	public static final byte MINIGAME_STATES = 5;
	
	public static final int MAX_VAR_VALUE = 50000;
	public static final int MAX_VAR_RANGE = MAX_VAR_VALUE >> 1;
	public static final int VAR_INCREASE = 1000;
	
	public static final int COUNT_TIME = 3500;
	public static final int MINIGAME_TIME = 10000;
	public static final int FINISH_ANIM_TIME = 2000;
	public static final int COUNT_ON_ACTION_MIN_TIME = 1000;
		
	private byte type;
	private byte state;
	private byte activityType;
	
	private int var;
	private int accTime;
	
	private int modX = 21;
	private int modY = 20;
	
	private MarqueeLabel countLabel;
	
	private AttributeBar bar;
	
	private Drawable drawable1;
	private Drawable drawable2;
	private Drawable drawable3;
	
	private Sprite sprite1;
	
	private Point point1 = new Point();
	private Point point2 = new Point();
	
	private static int lastResult;
	public static int getLastResult() { return lastResult;	}
	
	public MiniGame( String title, byte activityType ) {
		super( title, AppMIDlet.getText( TEXT_MINIGAME_BUTTON_MASING_TUT + typePerActivity( activityType ) ), true );
		
		setActivityType( activityType );
		setState( MINIGAME_STATE_SHOW_MSG );
		accTime = 0;
		
		try {
			bar = new AttributeBar( (short)( ( var * 100 ) / MAX_VAR_VALUE ), COLOR_RESISTANCE );
		} catch( Exception ex ) {
			//#if DEBUG == "true"
				ex.getMessage();
			//#endif
		}
		
		countLabel = new MarqueeLabel( FONT_MENU, "" );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
	
	
	public void setActivityType( byte activityType ) {
		this.activityType = activityType;
		type = typePerActivity( activityType );
		
		String img1 = null;
		int img1sz = 100;
		int img1anchor = ANCHOR_CENTER;
		int img1FailColor = 0xffffff;
		
		String img2 = null;
		int img2sz = 30;
		int img2anchor = ANCHOR_CENTER;
		int img2FailColor = 0xffff00;
		
		String img3 = null;
		int img3sz = 10;
		int img3anchor = ANCHOR_CENTER;
		int img3FailColor = 0xff0000;
		
		String sprt1 = null;
		int sprt1anchor = ANCHOR_CENTER;
		
		switch( activityType ) {
			case ACTIVITY_PREPARE_MEAL:				
				img1 = "carrot.png";
				img1anchor = ANCHOR_LEFT | ANCHOR_VCENTER;
				sprt1 = "carrotCutting";
				sprt1anchor = ANCHOR_LEFT | ANCHOR_VCENTER;
			break;
				
			case ACTIVITY_PLAY_SNOOKER:
				
				img1 = "ball.png";
				img2 = "splash.png";
				img3 = "cue.png";
				img3anchor = ANCHOR_HCENTER | ANCHOR_TOP;
			break;
				
			default:
//			case ACTIVITY_PLAY_VIDEO_GAME:
				sprt1 = "video_game";
				sprt1anchor = ANCHOR_CENTER;
			break;
		}
		
		if( img1 != null ) {
			try {
				drawable1 = new DrawableImage( PATH_MINIGAMES_IMAGES + img1 );
			} catch( Throwable t ) {
				drawable1 = new DrawableArc( img1FailColor );
				drawable1.setSize( img1sz, img1sz );
				//#if DEBUG == "true"
					t.getMessage();	
				//#endif
			}
			drawable1.defineReferencePixel( img1anchor );
		}

		if( img2 != null ) {
			try {
				drawable2 = new DrawableImage( PATH_MINIGAMES_IMAGES + img2 );
			} catch( Throwable t ) {
				drawable2 = new DrawableArc( img2FailColor );
				drawable2.setSize( img2sz, img2sz );
				//#if DEBUG == "true"
					t.getMessage();	
				//#endif
			}
			drawable2.defineReferencePixel( img2anchor );
		}

		if( img3 != null ) {
			try {
				drawable3 = new DrawableImage( PATH_MINIGAMES_IMAGES + img3 );
			} catch( Throwable t ) {
				drawable3 = new DrawableArc( img3FailColor );
				drawable3.setSize( img3sz, img3sz );
				//#if DEBUG == "true"
					t.getMessage();	
				//#endif
			}
			drawable3.defineReferencePixel( img3anchor );
		}

		if( sprt1 != null ) {
			try {
				sprite1 = new Sprite( PATH_MINIGAMES_IMAGES + sprt1 );
				sprite1.defineReferencePixel( sprt1anchor );
			} catch( Throwable t ) {
				//#if DEBUG == "true"
					t.getMessage();	
				//#endif
			}
		}
		
		switch( activityType ) {
			case ACTIVITY_PREPARE_MEAL:
			break;
				
			case ACTIVITY_PLAY_SNOOKER:
				drawable2.setVisible( false );
			break;
			
			default:
			break;
		}
	}
	
	
	public static byte typePerActivity( byte activity ) {
		switch( activity ) {				
			case ACTIVITY_PLAY_SNOOKER:
				return MINIGAME_TARGET;
			
			case ACTIVITY_PREPARE_MEAL:
			case ACTIVITY_PLAY_VIDEO_GAME:
				return MINIGAME_BUTTON_MASHING;
				
			default:
//				return MINIGAME_TARGET;
				return MINIGAME_BUTTON_MASHING;
		}
	}
	
	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		
		bar.setPosition( getPosX() + freeArea.x + SAFE_MARGIN + BAR_X_OFFSET, getPosY() + freeArea.y + freeArea.height - ( bar.getHeight() + SAFE_MARGIN ) );
		bar.setSize( freeArea.width - ( ( SAFE_MARGIN + BAR_X_OFFSET ) << 1 ), bar.getHeight() );
		
		countLabel.setSize( freeArea.width, countLabel.getFont().getHeight() );
		countLabel.setPosition( getPosX() + freeArea.x, getPosY() + freeArea.y + ( ( freeArea.height - countLabel.getHeight() ) >> 1 ) );
		
		
		
		switch( activityType ) {
			case ACTIVITY_PREPARE_MEAL:
				if( sprite1 != null && drawable1 != null ) {
					sprite1.setRefPixelPosition( getPosX() + text.getPosX() + ( ( text.getWidth() - ( sprite1.getWidth() + drawable1.getWidth() ) ) >> 1 ), getPosY() + text.getPosY() + ( text.getHeight() >> 1 ) );
					drawable1.setRefPixelPosition( sprite1.getRefPixelX() + sprite1.getWidth(), sprite1.getRefPixelY() );
				}
			break;
				
			case ACTIVITY_PLAY_SNOOKER:
				if( drawable1 != null ) drawable1.setRefPixelPosition( getPosX() + text.getPosX() + ( text.getWidth() >> 1 ), getPosY() + text.getPosY() + ( text.getHeight() >> 1 ) );
				if( drawable3 != null ) drawable3.setRefPixelPosition( drawable1.getRefPixelPosition() );
			break;
			
			default:
			//case ACTIVITY_PLAY_VIDEO_GAME:
				if( sprite1 != null ) sprite1.setRefPixelPosition( getPosX() + text.getPosX() + ( text.getWidth() >> 1 ), getPosY() + text.getPosY() + ( text.getHeight() >> 1 ) );
			break;
		}
		
		Rectangle rect = new Rectangle( getPosX() + fill.getPosX(), getPosY() + fill.getPosY(), fill.getWidth(), fill.getHeight());
		
		if( drawable1 != null ) drawable1.setViewport( rect );
		if( drawable2 != null ) drawable2.setViewport( rect );
		if( drawable3 != null ) drawable3.setViewport( rect );
		if( sprite1 != null ) sprite1.setViewport( rect );
	}
	
	
	public final void updateBar() { 
		bar.setValueFP( 10 * ( NanoMath.toFixed( var / 10 ) / MAX_VAR_VALUE ) );
	}
	
	
	private final void setVar( int newVar ) {
		var = newVar;
		if( var < 0 ) var = 0;
		else if( var > MAX_VAR_VALUE ) var = MAX_VAR_VALUE;
		//#if DEBUG == "true"
			System.out.println( "try to set var has " + newVar + ( ( var != newVar ) ?  ", but setted has " + var : "" ) );
		//#endif
		updateBar();
	}


	private void nextState() {		
		switch( state ) {
			case MINIGAME_STATE_SHOW_MSG:
				setState( MINIGAME_STATE_COUNTING );
			break;
				
			case MINIGAME_STATE_COUNTING:
				setState( MINIGAME_STATE_PLAYING );
			break;
				
			case MINIGAME_STATE_PLAYING:
				setState( MINIGAME_STATE_FINISH_ANIM );
			break;
				
			case MINIGAME_STATE_FINISH_ANIM:
				setState( MINIGAME_STATE_SHOW_RESULTS );
			break;
		}
	}
	
	
	public void setState( byte state ) {
		this.state = state;
		accTime = 0;
		
		switch( state ) {
			case MINIGAME_STATE_SHOW_MSG:
				text.setVisible( true );
			break;
				
			case MINIGAME_STATE_COUNTING:
			case MINIGAME_STATE_PLAYING:
				updateCrosshair();
				text.setVisible( false );
			break;
				
			case MINIGAME_STATE_SHOW_RESULTS:
				//#if DEBUG == "true"
					System.out.println( "Before SetText" );
				//#endif
				lastResult = getPercentage();
				setText( AppMIDlet.getText( TEXT_MINIGAME_RESULT ) + getStatus().toLowerCase() + AppMIDlet.getText( TEXT_MINIGAME_RESULT_FINISH ) + lastResult + "%" + ( ( lastResult == 100 ) ? ( AppMIDlet.getText( TEXT_BEST_ONE ) ) : ( ( lastResult == 0 ) ? ( AppMIDlet.getText( TEXT_WORST_ONE ) ) : ( "." ) ) ) + "</ALN_H>" );
				//#if DEBUG == "true"
					System.out.println( "After SetText" );
				//#endif
				text.setVisible( true );
			break;
		}
	}
	
	
	public int getPercentage() {
		return ( ( ( var * 100 ) + ( MAX_VAR_VALUE >> 1 ) ) / MAX_VAR_VALUE );
	}
	
	
	public String getStatus() {
		int pc = getPercentage();
		if( pc <= 20 ) return AppMIDlet.getText( TEXT_TERRIBLE );
		else if( pc <= 40 ) return AppMIDlet.getText( TEXT_BAD );
		else if( pc <= 60 ) return AppMIDlet.getText( TEXT_REGULAR );
		else if( pc <= 80 ) return AppMIDlet.getText( TEXT_GOOD );
		return AppMIDlet.getText( TEXT_EXCELENT );
	}
	
	
	public void updateCrosshair() {
		point2.set( NanoMath.cosInt( ( accTime * modX / 100 ) % 360 ), NanoMath.cosInt( ( accTime * modY / 100 ) % 360 ) );
		if( drawable1 != null ) point1.set( NanoMath.toInt( point2.x * ( drawable1.getWidth() >> 1 ) ), NanoMath.toInt( point2.y * ( drawable1.getHeight() >> 1 ) ) );
		if( drawable1 != null && drawable3 != null ) drawable3.setRefPixelPosition( drawable1.getRefPixelPosition().add( point1 ) );
		if( drawable1 != null && drawable2 != null ) drawable2.setRefPixelPosition( drawable1.getRefPixelPosition().add( point1 ) );
	}
	
	
	public void update( int delta ) {
		super.update( delta );
		
		accTime += delta;
		
		switch( state ) {
			case MINIGAME_STATE_SHOW_MSG:
			break;
				
			case MINIGAME_STATE_COUNTING:				
				int acc;
				
				if( accTime < 1000 ) { 
					countLabel.setText( "3" );
					acc = accTime;
				} else if( accTime < 2000 ) {
					countLabel.setText( "2" );
					acc = accTime - 1000;
				} else if( accTime < 3000 ) {
					countLabel.setText( "1" );
					acc = accTime - 2000;
				} else {
					if( accTime >= COUNT_TIME ) { 
						nextState();
						acc = 500;
					} else {
						//#if VERSION == "BBB"
//# 								countLabel.setText( AppMIDlet.getText( TEXT_GO ) );
						//#else 
							countLabel.setText( AppMIDlet.getText( TEXT_GO ).toUpperCase() );
						//#endif
						acc = accTime - 3000;
					}
				}
				
				acc = ( acc - 500 ) / 10;
				countLabel.setTextOffset( ( ( countLabel.getWidth() - countLabel.getTextWidth() ) >> 1 ) + ( acc * acc * acc * ( ( countLabel.getWidth() + countLabel.getTextWidth() ) >> 1 ) ) / ( 125000 ) );		
								
				//#if DEBUG == "true"
					System.out.println( "accTime = " + accTime + " | acc = " + acc + " | TextOffset = " + countLabel.getTextOffset() );
				//#endif
			break;
				
			case MINIGAME_STATE_PLAYING:
				switch( type ) {
					case MINIGAME_TARGET:
						updateCrosshair();
						setVar( ( MAX_VAR_VALUE + MAX_VAR_RANGE ) - NanoMath.clamp( 
								( NanoMath.sqrtInt( ( point1.x * point1.x ) + ( point1.y * point1.y ) ) * ( MAX_VAR_VALUE + MAX_VAR_RANGE ) / ( drawable1.getWidth() >> 1 ) ), 
								MAX_VAR_RANGE, MAX_VAR_VALUE + MAX_VAR_RANGE ) );
						//Sem nextState() pois não termina com o tempo
					break;
						
					case MINIGAME_BUTTON_MASHING:						
						if( sprite1 != null ) sprite1.update( delta );						
						if( accTime >= MINIGAME_TIME ) nextState();
						else setVar( var - delta );
					break;
						
					default:
						if( accTime >= MINIGAME_TIME ) nextState();
				}
			break;
				
			case MINIGAME_STATE_FINISH_ANIM:
				if( accTime >= FINISH_ANIM_TIME ) nextState();
				
				
				switch( activityType ) {
					case ACTIVITY_PLAY_SNOOKER:
						if( accTime <= 6 * FINISH_ANIM_TIME / 10 ) {
							if( drawable1 != null && drawable3 != null ) drawable3.setRefPixelPosition( drawable1.getRefPixelX() + point1.x, drawable1.getRefPixelY() + point1.y + ( MINIGAME_CUE_BACK_DIST * accTime / FINISH_ANIM_TIME ) );
						} else if( accTime <= 8 * FINISH_ANIM_TIME / 10 ) {
							if( drawable1 != null && drawable3 != null ) drawable3.setRefPixelPosition( drawable1.getRefPixelX() + point1.x, drawable1.getRefPixelY() + point1.y + MINIGAME_CUE_BACK_DIST );
						} else if( accTime <= 9 * FINISH_ANIM_TIME / 10 ) {
							if( drawable1 != null && drawable3 != null ) drawable3.setRefPixelPosition( drawable1.getRefPixelX() + point1.x, drawable1.getRefPixelY() + point1.y + ( MINIGAME_CUE_BACK_DIST * ( ( 9 * FINISH_ANIM_TIME / 10 ) - accTime ) / FINISH_ANIM_TIME ) );							
						} else {
							if( drawable1 != null && drawable3 != null ) drawable3.setRefPixelPosition( drawable1.getRefPixelPosition().add( point1 ) );
							drawable2.setVisible( var > 0 );
						}	
						
						if( accTime < 1000 ) { 
							//#if VERSION == "BBB"
//# 								countLabel.setText( getStatus() );
							//#else 
								countLabel.setText( getStatus().toUpperCase() );
							//#endif
							acc = ( accTime - 500 ) / 10;
							countLabel.setTextOffset( ( ( countLabel.getWidth() - countLabel.getTextWidth() ) >> 1 ) + ( acc * acc * acc * ( ( countLabel.getWidth() + countLabel.getTextWidth() ) >> 1 ) ) / ( 125000 ) );		
						}
					break;
						
					default:						
						if( accTime < 1000 ) { 
							//#if VERSION == "GH"
//# 								countLabel.setText( AppMIDlet.getText( TEXT_MINIGAME_OVER ).toUpperCase() );
							//#else
								countLabel.setText( AppMIDlet.getText( TEXT_MINIGAME_OVER ) );
							//#endif
							acc = ( accTime - 500 ) / 10;
							countLabel.setTextOffset( ( ( countLabel.getWidth() - countLabel.getTextWidth() ) >> 1 ) + ( acc * acc * acc * ( ( countLabel.getWidth() + countLabel.getTextWidth() ) >> 1 ) ) / ( 125000 ) );		
						} else if( accTime < 2000 ) { 
							//#if VERSION == "BBB"
//# 							countLabel.setText( getStatus() /*AppMIDlet.getText( TEXT_MINIGAME_OVER )*/ );
							//#else
								countLabel.setText( getStatus().toUpperCase() );
							//#endif
							acc = ( accTime - 1500 ) / 10;
							countLabel.setTextOffset( ( ( countLabel.getWidth() - countLabel.getTextWidth() ) >> 1 ) + ( acc * acc * acc * ( ( countLabel.getWidth() + countLabel.getTextWidth() ) >> 1 ) ) / ( 125000 ) );		
						}
				}
			break;
				
			case MINIGAME_STATE_SHOW_RESULTS:
			break;
		}
	}
	
	
	public void draw( Graphics g ) {
		super.draw( g );
		
		switch( state ) {
			case MINIGAME_STATE_SHOW_MSG:
			break;
				
			case MINIGAME_STATE_COUNTING:
				if( sprite1 != null ) sprite1.draw( g );
				if( drawable1 != null ) drawable1.draw( g );
				if( drawable2 != null ) drawable2.draw( g );
				if( drawable3 != null ) drawable3.draw( g );
				bar.draw( g );
				countLabel.draw( g );	
			break;
				
			case MINIGAME_STATE_PLAYING:
				if( sprite1 != null ) sprite1.draw( g );
				if( drawable1 != null ) drawable1.draw( g );
				if( drawable2 != null ) drawable2.draw( g );
				if( drawable3 != null ) drawable3.draw( g );
				if( accTime < COUNT_ON_ACTION_MIN_TIME ) countLabel.draw( g );
				bar.draw( g );
			break;
				
			case MINIGAME_STATE_FINISH_ANIM:
				if( sprite1 != null ) sprite1.draw( g );
				if( drawable1 != null ) drawable1.draw( g );
				if( drawable2 != null ) drawable2.draw( g );
				if( drawable3 != null ) drawable3.draw( g );
				countLabel.draw( g );
				bar.draw( g );
			break;
				
			case MINIGAME_STATE_SHOW_RESULTS:
			break;
		}
	}
	
	
	private final boolean okAction() {		
		switch( state ) {
			case MINIGAME_STATE_SHOW_MSG:
				nextState();
			break;

			case MINIGAME_STATE_COUNTING:
			break;

			case MINIGAME_STATE_PLAYING:
			break;
				
			case MINIGAME_STATE_FINISH_ANIM:
			break;

			case MINIGAME_STATE_SHOW_RESULTS:
			return true;
		}
		return false;
	}
	
	private final void gameAction( boolean touch ) {
		switch( state )  {
			case MINIGAME_STATE_SHOW_MSG:
			break;

			case MINIGAME_STATE_COUNTING:
				okButtonIsPressed = false;
			break;
				
			case MINIGAME_STATE_PLAYING:				
				switch( type ) {
					case MINIGAME_BUTTON_MASHING:
						setVar( var + ( ( VAR_INCREASE * ( touch ? 10 : 10 ) ) / 10 ) );
						if( sprite1 != null ) sprite1.setSequence( 1 );
					break;
						
					case MINIGAME_TARGET:
						nextState();
					break;
				}
			break;
				
			case MINIGAME_STATE_FINISH_ANIM:
			break;

			case MINIGAME_STATE_SHOW_RESULTS:
			break;
		}
	}

	//<editor-fold desc="Handdle Input">
		public final void keyPressed( int key ) {
			switch( key ) {
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
				case ScreenManager.KEY_FIRE:
				case ScreenManager.KEY_NUM5:
					okButtonIsPressed = true;
					gameAction( false );
				break;

				case ScreenManager.KEY_UP:
				case ScreenManager.KEY_NUM1:
					setMoveState( MOVE_STATE_MOVE_UP );
				break;

				case ScreenManager.KEY_DOWN:
				case ScreenManager.KEY_NUM8:
					setMoveState( MOVE_STATE_MOVE_DOWN );
				break;

				case ScreenManager.KEY_RIGHT:
				case ScreenManager.KEY_NUM6:
					moveText( -text.getHeight() );
				break;

				case ScreenManager.KEY_LEFT:
				case ScreenManager.KEY_NUM4:
					moveText( text.getHeight() );
				break;
			}
		}
		
		public final boolean keyReleased( int key ) {
			switch( key ) {
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
				case ScreenManager.KEY_FIRE:
				case ScreenManager.KEY_NUM5:
					okButtonIsPressed = false;
					return okAction();
			}

			return false;
		}

		//#if TOUCH == "true"
			public final boolean handleTap( int x, int y ) {
				if( okArea.contains( x - getPosX(), y - getPosY() ) ) {
					okButtonIsPressed = false;
					return okAction();
				}
				return false;
			}
			
			public final void gestureIsNotSpot( int x, int y ) {
				okButtonIsPressed = false;
			}
			
			public final void handleFirstInteraction( int x, int y ) {
				if( okArea.contains( x - getPosX(), y - getPosY() ) ) {
					okButtonIsPressed = true;
				}
				gameAction( true );
			}
			
			public final void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				if( state != MINIGAME_STATE_COUNTING ) okButtonIsPressed = ( okArea.contains( x - getPosX(), y - getPosY() ) && canBeTap );
				if( text.contains( x - getPosX(), y - getPosY() ) ) {
					moveText( dy );
				}
			}
			
			public final void handleRelease( int x, int y ) { 
				okButtonIsPressed = false;
			}
		//#endif
	//</editor-fold>

	public void close() {
	}
}
