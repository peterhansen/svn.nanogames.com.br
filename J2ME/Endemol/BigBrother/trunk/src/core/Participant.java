package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
/**
 *
 * @author Caio, Ygor
 */
public class Participant implements Constants {
	
	//<editor-fold desc="Constants">
		private final static byte STATE_STOPPED = 0;
		private final static byte STATE_MOVING = 1;
		private final static byte STATE_DOING_ACTIVITY = 2;
		//private static final byte STATE_STOPPING = 3;

		private final static int MIN_VELOCITY_TH = 60; // tiles por hora
		private final static int MIN_VELOCITY_PXH = MIN_VELOCITY_TH * TILE_SIZE; // pixles por hora
		private final static int MIN_VELOCITY_TH_FP = NanoMath.divInt( MIN_VELOCITY_TH, 1000 ); // tiles por hora
		private final static int MIN_VELOCITY_PXH_FP = MIN_VELOCITY_TH_FP * TILE_SIZE; // pixles por hora

		/** Tempo gasto no movimento entre os comodos */
		public static final int moveTime = HOUR >> 1;
	//</editor-fold> // end of Constants region

	//<editor-fold desc="Fields">

		//<editor-fold desc="Static Fields">
			private static Image pinsImg;
			private static Image baloonsImg;
			private static int bW;
			private static int bH;
			private static Image iconC;
			private static Image iconK;
			private static Image iconR;
			private static Image iconB;
			private static int W;
			private static int H;
			private static final int MAX_AFFINITY = Integer.MAX_VALUE;

			public static final byte[] ATTRIBUTES = {
				45, 40, 30, 45,
				45,	40,	20,	55,
				45,	40,	45,	30,
				50,	40,	35,	35,
				40,	45,	50,	25,
				40,	45,	55,	20,
				40,	55,	35,	30,
				40,	45,	35,	40
			};

			private static final byte[] ID_CHANGE = { 2, 4, 7, 1, 6, 0, 3, 5 };
			public static byte photosID( int id ) {
				return ID_CHANGE[id];
			}
		//</editor-fold> // end of Static Fields region

		private final String name;

		private Room room;
		private int spotId = -1;
		private int timeWithoutSleep = 0;
		private int timeWithoutEat = 0;
		
		private int[] attributes = new int[4];
		private int[] initialAttributes = new int[4];

		/** Comodos os quais o participante favorece quando escolhendo um novo */
		private Room[] favoriteRooms;

		/** Array de afinidades, indexada por id do participante */
		private int[] affinity = new int[PARTICIPANTS_NUMBER];

		/** Objeto o qual recebe eventos de alteracao de atributos do participante. Pode ser nulo. */
		private ParticipantListener listener = null;

		private final int id;
		private final byte photosID;
		private final Point position = new Point( -TILE_SIZE, -TILE_SIZE );
		
		//<editor-fold desc="State Variables">
			private BezierCurve moveRoute = new BezierCurve();
			private byte state = STATE_STOPPED;
			private int animAcc;

			// variaveis associadas a atividade e sua execução
			private Activity currentActivity;
			private Activity lastActivity;
			private int activityTime;
			private int attAdded;

			//* Degeneração acumulada, em minutos */
			private int accumDegeneration;
		//</editor-fold>

		private final Rectangle drawableArea = new Rectangle();
		private final Rectangle intersection = new Rectangle();
		
		private boolean isPlayerParticipant;
	//</editor-fold> // end of Fields region

	//<editor-fold desc="Initialization">
		public Participant( int id, String name ) throws Exception {
			this.name = name;

			if( pinsImg == null ) {
				pinsImg = Image.createImage( PATH_GAME_IMAGES + "game_pins.png" );
				W = pinsImg.getWidth() / NUMBER_OF_PINS;
				H = pinsImg.getHeight();
			}
			
			if( baloonsImg == null ) {
				baloonsImg = Image.createImage( PATH_GAME_IMAGES + "baloons.png" );
				bW = baloonsImg.getWidth() / 4;
				bH = baloonsImg.getHeight();
			}

			if( iconB == null ) iconB = Image.createImage( PATH_GAME_IMAGES + "icon_beauty.png" );
			if( iconR == null ) iconR = Image.createImage( PATH_GAME_IMAGES + "icon_resistance.png" );
			if( iconC == null ) iconC = Image.createImage( PATH_GAME_IMAGES + "icon_charisma.png" );
			if( iconK == null ) iconK = Image.createImage( PATH_GAME_IMAGES + "icon_knowlegde.png" );

			for( int i = 0; i < attributes.length; i++ ) {
				attributes[i] = ( getInitialAttributes( id, (byte) i ) * ATTRIBUTE_MAX ) / 100;
				initialAttributes[i] = attributes[i];
			}

			this.id = id;
			this.photosID = photosID( id );

			// gerando array de afinidades
			for ( int i = 0; i < PARTICIPANTS_NUMBER; i++ ) {
				affinity[ i ] = NanoMath.randInt( 50 );
			}
			
			isPlayerParticipant = false;
			
			
		}
	//</editor-fold>

	/**
	 * Libera participante de seu spot no comodo, caso ele
	 * esteja colocado em um.
	 */
	public final void freeSpot() {
		if( room != null )
			room.freeSpot( spotId );

		room = null;
		spotId = -1;
	}
	
	
	public final void setPlayerParticipant( boolean bool ) {
		isPlayerParticipant = bool;
	}

	//<editor-fold desc="Getters and Setters">
		
		/**
		 * Retorna valor inicial do atributo, indexados no Array ATTRIBUTES como
		 * ( ( indice_participante - 1 ) * 4 ) + indice_atributo.
		 * @param participantId Indice do participante.
		 * @param attributeId Indice do atributo.
		 * @return valor inicial do atributo.
		 */
		public static final byte getInitialAttributes( int participantId, byte attributeId ) {
		return ATTRIBUTES[ (participantId * 4) + attributeId ];
	}

		private final int getOffsetX( byte id ) { return id * W; }
		private final int getOffsetY( byte id ) { return 0;	}
		public final boolean isDone() { return state == STATE_STOPPED; }

		public final int getBeaty() { return attributes[ ATTRIBUTE_BEAUTY ]; }
		public final int getCharisma() { return attributes[ ATTRIBUTE_CHARISMA ];}
		public final int getKnowledge() { return attributes[ ATTRIBUTE_KNOWLEDGE ];}
		public final int getResistance() { return attributes[ ATTRIBUTE_RESISTANCE ]; }
		
		public final int getPopularity() {
			return ( getBeaty() + getCharisma() + getKnowledge() + getResistance() );
			// TODO testar bonificação por valores altos( fução quadratica )
//			int ret = 0;
//
//			int temp = NanoMath.divInt( getBeaty(), ATTRIBUTE_MAX );
//			ret += NanoMath.mulFixed( temp, temp );
//			temp = NanoMath.divInt( getCharisma(), ATTRIBUTE_MAX );
//			ret += NanoMath.mulFixed( temp, temp );
//			temp = NanoMath.divInt( getKnowledge(), ATTRIBUTE_MAX );
//			ret += NanoMath.mulFixed( temp, temp );
//			temp = NanoMath.divInt( getResistance(), ATTRIBUTE_MAX );
//			ret += NanoMath.mulFixed( temp, temp );
//
//			//#if DEBUG == "true"
//				System.out.println( NanoMath.toInt( W * ATTRIBUTE_MAX ) );
//			//#endif
//
//			return NanoMath.toInt( W * ATTRIBUTE_MAX );
		}

		public final String getName() {
			return name;
		}
		
		public final int getActivityTime() { return activityTime; }
		public final int getExecutedTime() { return isDoingActivity()? animAcc: 0; }

		public final int getId() { return id; }
		public final Point getPosition() { return position; }
		public final int getPhotoId() { return photosID; }

		public final Point getDestiny() { return moveRoute.destiny; }

		public final byte getPenaltyState() {
			byte penaltyState = PENALTY_NOPENALTY;
			if( getStarvationPenalty() > 0 )
				penaltyState |= PENALTY_NOFOOD;
			if( getInsomniaPenalty() > 0 )
				penaltyState |= PENALTY_NOSLEEP;
			return penaltyState;
		}

		/**
		 * Retorna o aumento percentual causado na degeneração
		 * por não comer e não dormir.
		 * @return Penalidade, como porcentagem.
		 */
		public final int getPenalty() {
			return NanoMath.min( 100 + ( ( getStarvationPenalty() + getInsomniaPenalty() ) * 50 ), 200 );
		}
		
		public final void setAttribute( byte id, int value ) {
			value = NanoMath.clamp( value, ( initialAttributes[id] / 2 ), ATTRIBUTE_MAX );
			attributes[id] = value;
		}

		public final void setListener( ParticipantListener listener ) { this.listener = listener; }

		private final int getStarvationPenalty() { return timeWithoutEat / MAX_TIME_WITHOUT_SLEEP; }
		private final int getInsomniaPenalty() { return timeWithoutSleep / MAX_TIME_WITHOUT_EAT; }
		
		public final void increaseAttribute( byte id, int value ) { setAttribute( id, attributes[id] + value ); }

		public final int getBonus() { return currentActivity.getAttBonus(); }

		public final boolean isDoingActivity() {
			return state  == STATE_DOING_ACTIVITY;
		}

		public final boolean isDoing( byte activityID ) {
			if( isDoingActivity() ) {
				if( currentActivity != null ) {
				return currentActivity.getAttribute() == activityID;
				}
			}
			return false;
		}

		public final Room getRoom() { return room; }

		/**
		 * Dá ordem de posicionamento o participante no comodo e pontos do comodo
		 * especificados. A posicao do ponto NAO é relativa ao comodo.
		 */
		public final void goTo( Room room, Point p, int spotId ) {
			this.room = room;
			this.spotId = spotId;

			moveRoute.origin.set( position.x, position.y );
			moveRoute.destiny.set( p.x, p.y );
			moveRoute.control1.set( moveRoute.origin );

			// TODO: colocar centro do mapa em algum lugar,
			// no GameBoard?
			moveRoute.control2.set( TILE_SIZE * 17, TILE_SIZE * 14 ); //TODO centro do mapa
			
			//#if DEBUG == "true"
				//System.out.println( " moveRoute from " + moveRoute.origin + " to " + moveRoute.destiny + " with " + moveRoute.control1 + " and " + moveRoute.control2 );
			//#endif

			animAcc = 0;
			state = STATE_MOVING;
		}

		/** Troca o estado do participante para "agindo". */
		//public final void setActingDirectly() {
		//	state = STATE_DOING_ACTIVITY;
		//	animAcc = 0;
		//}
		
	//</editor-fold> // end of Getters and Setters region

	//<editor-fold desc="Activity Handling">
		public final void setFavoriteRooms( Room[] rooms ) {
			// sempre setamos para que 50% das vezes tenhamos um comodo
			// favorito e no resto seja aleatorio
			favoriteRooms = new Room[ rooms.length * 2 ];
			for( int i = 0; i < rooms.length; i++ )
				favoriteRooms[ i ] = rooms[ i ];
		}

		public final Room getDesiredRoom() {
			return favoriteRooms[ NanoMath.randFixed( favoriteRooms.length ) ];
		}

		public final void assignActivity( Activity activity, int activityTime ) {
			this.activityTime = activityTime;

			if( listener != null && currentActivity != null )
				listener.attributeStoppedChanging( currentActivity.getAttribute() );
			
			currentActivity = activity;
			attAdded = 0;

			if( activity == null && state == STATE_DOING_ACTIVITY )
				state = STATE_STOPPED;
		}
	//</editor-fold>


	//<editor-fold desc="Affinity and Voting>"
	/**
	 * Retorna a opcao desse participante para eliminacao,
	 * a qual depende das afinidades desse participante.
	 * @return Indice do participante votado
	 */
	public final int voteForElimination() {
		int minAffinity = Integer.MAX_VALUE;
		int minId = -1;
		for( int i = 0; i < affinity.length; i++ ) {
			if( affinity[ i ] < minAffinity ) {
				minId = i;
				minAffinity = affinity[ i ];
			}
		}
		return minId;
	}
	
	/**
	 * Faz com que nao se vote mais nesse participante.
	 * @param participantId Participante o qual se quer ignorar na escolha de votos.
	 */
	public final void ignoreForVoting( int participantId ) {
		affinity[ participantId ] = Participant.MAX_AFFINITY;
	}
	//</editor-fold>

	public final int getAffinity( int participandId ) { return affinity[ participandId ]; }
	public final void setAffinity( int participantId, int value ) { affinity[ participantId ] = value; }
	
	//<editor-fold desc="Update and Draw">
		public void update( int time ) {
			// LEMBRETE: time do update é minutos (diferente de delta!)

			timeWithoutSleep += time;
			timeWithoutEat += time;

			// a degeneração acumulada precisa estar na mesma escala que
			// os fatores de degeneração:
			accumDegeneration += time * ATTRIBUTE_DEGEN_SCALE;
			final int degeneration = accumDegeneration / ATTRIBUTE_DEGENERATION_MOD;

			if( degeneration > 0 ) {
				final int degenerationRest = accumDegeneration % ATTRIBUTE_DEGENERATION_MOD;

				for( byte i = 0; i < attributes.length; i++ ) {
					if( !isDoing( i ) ) {
						//#if DEBUG == "true"
							//System.out.println( GameMIDlet.getText( TEXT_ACTIVITY_SOCIALIZE + i ) + " -= " + accumDegeneration );
						//#endif

						int penalty = getPenalty();
						//#if DEBUG == "true"
							//System.out.println( "penalidade: " + penalty );
						//#endif

						increaseAttribute( i, ( -degeneration * penalty ) / 100 );
						accumDegeneration = degenerationRest;
					}
				}
			}

			switch( state ) {
				case STATE_MOVING:
					animAcc += time;
					if( animAcc < moveTime ) {
						final int acc_fp = NanoMath.divInt( animAcc, moveTime );
						//position.set( GameOverScreen.goJumpping( moveRoute.origin, moveRoute.destiny, animAcc, moveTime, WALK_JUMP_WIDTH, WALK_JUMP_HEIGHT ) );
						position.set( moveRoute.getXFixed( acc_fp ), moveRoute.getYFixed( acc_fp ) );
					} else {
						position.set( moveRoute.destiny.x, moveRoute.destiny.y );
						animAcc = 0;
						state = currentActivity != null? STATE_DOING_ACTIVITY: STATE_STOPPED;
					}
				break;

				case STATE_DOING_ACTIVITY:
					animAcc += time;

					// TODO: mover esse código para Activity.java?
					if( animAcc < activityTime ) {
						if( currentActivity.getId() == ACTIVITY_EAT ) timeWithoutEat = 0;
						if( currentActivity.getId() == ACTIVITY_SLEEP ) timeWithoutSleep = 0;
						
						// avisa ao listener que esse atributo esta sendo alterado
						if( listener != null) {
							listener.attributeIsChanging( currentActivity.getAttribute() );
						}
						int att = ( ( ( ( animAcc * getBonus() * ( isPlayerParticipant ? ( MiniGame.getLastResult() << 1 ) : 100 ) ) / 100 ) ) / activityTime ) - attAdded;
						increaseAttribute( currentActivity.getAttribute() , att );
						attAdded += att;
						//#if DEBUG == "true"
							//System.out.println( photosID + ":   " +  GameMIDlet.getText( TEXT_CHARISMA + currentActivity.getAttribute() ) + " += " + att );
						//#endif

						if( currentActivity.getId() == ACTIVITY_SOCIALIZE ) {
							Participant[] participants = room.getParcipants();
							int numberOfParticipants = participants.length;

							//#if DEBUG == "true"
								// nao da para socializar na solidao
								//if( numberOfParticipants == 1 ) {
								//	System.out.println( name + " esta socializando na solidao... hhhmmm..." );
								//	numberOfParticipants++;
								//}
							//#endif

							int socialBonus = NanoMath.max( SOCIALIZATION_BONUS, (SOCIALIZATION_BONUS * 3) / ( numberOfParticipants - 1 ) );
							for( int i = 0; i < participants.length; i++ ) {
								if( i == id || participants[ i ] == null ) continue;
								participants[ i ].setAffinity( id, participants[ i ].getAffinity( id ) + socialBonus );
							}
						}

					} else {
						if( currentActivity.getId() == ACTIVITY_EAT ) timeWithoutEat = 0;
						if( currentActivity.getId() == ACTIVITY_SLEEP ) timeWithoutSleep = 0;

						int att = ( ( getBonus() * ( isPlayerParticipant ? ( MiniGame.getLastResult() << 1 ) : 100 ) ) / 100 ) - attAdded;
						increaseAttribute( currentActivity.getAttribute(), att );
						attAdded += att;
						//#if DEBUG == "true"
							//System.out.println( GameMIDlet.getText( TEXT_ACTIVITY_SOCIALIZE + currentActivity.getAttribute() ) + " += " + att );
						//#endif

						// avisa ao listener que esse atributo parou de ser alterado
						if( listener != null) {
							listener.attributeStoppedChanging( currentActivity.getAttribute() );
						}

						lastActivity = currentActivity;
						currentActivity = null;
						state = STATE_STOPPED;

						//#if DEBUG == "true"
							//for (int i = 0; i < affinity.length; i++) {
							//	System.out.print(affinity[i] + " - ");
							//}
							//System.out.println("");
						//#endif
					}
				break;
			}
		}

		public void draw( Graphics g, int offsetX, int offsetY, Rectangle area ) {
			//if( !isEliminated() ) {
//			final int beginX = position.x + offsetX - ( W >> 1 );
//			final int beginY = position.y + offsetY - ( ( H * 3 ) >> 2 );
//			final int endX = beginX + W;
//			final int endY = beginY + H;

			if( isDoingActivity() ) {
				final Activity activity = currentActivity;

//				//#if VERSION == "GH"
////# 					final int color;
////# 					switch( activity.getAttribute() ) {
////# 						case ATTRIBUTE_BEAUTY:
////# 							color = COLOR_BEAUTY;
////# 						break;
////# 						case ATTRIBUTE_CHARISMA:
////# 							color = COLOR_CHARISMA;
////# 						break;
////# 						case ATTRIBUTE_KNOWLEDGE:
////# 							color = COLOR_KNOWLEDGE;
////# 						break;
////# 						case ATTRIBUTE_RESISTANCE:
////# 							color = COLOR_RESISTANCE;
////# 						break;
////# 
////# 						default:
////# 							color = 0x000000;
////# 					}
////# 
////# 					g.setClip( area.x, area.y, area.width, area.height );
////# 					g.setColor( HUD.changeColorLightness( color, 33 ) );
////# 					g.fillTriangle( drawableArea.x + 3 * ( W >> 3 ), drawableArea.y - ( W >> 3 ), drawableArea.x + W - 3 * ( W >> 3 ), drawableArea.y - ( W >> 3 ), drawableArea.x + 3 * ( W >> 3 ), drawableArea.y + ( W >> 2 ) );
////# 					g.setColor( color );
////# 					g.drawLine( drawableArea.x + 3 * ( W >> 3 ), drawableArea.y - ( W >> 3 ), drawableArea.x + 3 * ( W >> 3 ), drawableArea.y + ( W >> 2 ) );
////# 					g.drawLine( drawableArea.x + W - 3 * ( W >> 3 ), drawableArea.y - ( W >> 3 ), drawableArea.x + 3 * ( W >> 3 ), drawableArea.y + ( W >> 2 ) );
////# 					g.setColor( color );
////# 					g.fillArc( drawableArea.x - ( W >> 2 ), drawableArea.y - W, W + ( W >> 1 ), W, 0, 360 );
////# 					g.setColor( HUD.changeColorLightness( color, 10 ) );
////# 					g.fillArc( drawableArea.x + 1 - ( W >> 2 ), drawableArea.y + 1 - W, W + ( W >> 1 ) - 2, W - 2, 0, 360 );
////# 					g.setColor( HUD.changeColorLightness( color, 30 ) );
////# 					g.fillArc( drawableArea.x + 2 - ( W >> 2 ), drawableArea.y + 2 - W, W + ( W >> 1 ) - 4, W - 4, 0, 360 );
////# 					g.setColor( HUD.changeColorLightness( color, 60 ) );
////# 					g.fillArc( drawableArea.x + 3 - ( W >> 2 ), drawableArea.y + 3 - W, W + ( W >> 1 ) - 6, W - 6, 0, 360 );
				////#else
					drawableArea.set( position.x + offsetX - ( bW >> 1 ), position.y + offsetY - ( ( ( H * 3 ) >> 2 ) + bH ), bW, bH );
					if( drawableArea.intersects( area ) ) {
						intersection.setIntersection( area, drawableArea );
						g.setClip( intersection.x, intersection.y, intersection.width, intersection.height );
						final int ds;
						g.drawImage( baloonsImg, drawableArea.x - ( bW * getBaloonOnSpriteID( activity.getAttribute() ) ), drawableArea.y, 0 );
					}
				////#endif
				
				switch( activity.getAttribute() ) {
					case ATTRIBUTE_CHARISMA:
						g.drawImage( iconC, drawableArea.x + ( bW >> 1 ), drawableArea.y + ( bH >> 1 ) + BALOON_CENTER_DIFF, Drawable.ANCHOR_CENTER );
					break;

					case ATTRIBUTE_BEAUTY:
						g.drawImage( iconB, drawableArea.x + ( bW >> 1 ), drawableArea.y + ( bH >> 1 ) + BALOON_CENTER_DIFF, Drawable.ANCHOR_CENTER );
					break;
						
					case ATTRIBUTE_RESISTANCE:
						g.drawImage( iconR, drawableArea.x + ( bW >> 1 ), drawableArea.y + ( bH >> 1 ) + BALOON_CENTER_DIFF, Drawable.ANCHOR_CENTER );
					break;

					case ATTRIBUTE_KNOWLEDGE:
						g.drawImage( iconK, drawableArea.x + ( bW >> 1 ), drawableArea.y + ( bH >> 1 ) + BALOON_CENTER_DIFF, Drawable.ANCHOR_CENTER );
					break;
				}
			}
			
			drawableArea.set( position.x + offsetX - ( W >> 1 ), position.y + offsetY - ( ( H * 3 ) >> 2 ), W, H);
			if( drawableArea.intersects( area ) ) {
				intersection.setIntersection( area, drawableArea );
				g.setClip( intersection.x, intersection.y, intersection.width, intersection.height );
				g.drawImage( pinsImg, drawableArea.x - getOffsetX( photosID ), drawableArea.y - getOffsetY( photosID ), 0 );
//				g.setClip( beginX, beginY, W, H );
//				g.drawImage( pinsImg, beginX - getOffsetX( photosID ), beginY - getOffsetY( photosID ), 0 );
			}
		}
		
		
		private final int getBaloonOnSpriteID( int id ) {
			switch( id ) {
				case ATTRIBUTE_BEAUTY: return 1;
					
				case ATTRIBUTE_KNOWLEDGE: return 2;
					
				case ATTRIBUTE_CHARISMA: return 3;
					
				case ATTRIBUTE_RESISTANCE: 
				default: return 0;
			}
		}


		/**
		 * Repete a execução da última atividade imediatamente.
		 * @param executedTime Tempo para o qual a atividade será executada.
		 */
		final void fastExecuteLastActivity( int executedTime ) {
			state = STATE_DOING_ACTIVITY;
			animAcc = 0;
			assignActivity( lastActivity, executedTime + 1 );
			update( executedTime );
		}
	//</editor-fold> // end of Update and Draw region

	//<editor-fold desc="Y Sorting">
		
		/** Lista de participantes, indexados por sua ordem em Y */
		public static Participant[] ySortedParticipants = new Participant[ PARTICIPANTS_NUMBER ];

		public static final void reset() {
			ySortedParticipants = new Participant[ PARTICIPANTS_NUMBER ];
		}

		public final void sortByY() {
			
			// procura indices
			Point thisDestiny = getDestiny();
			int oldIndex = -1;
			int newIndex = -1;
			int l = Participant.ySortedParticipants.length;
			for( int i = 0; i < l; i++ ) {
				if ( Participant.ySortedParticipants[ i ] == null ) {
					newIndex++;
					break;
				}

				if( Participant.ySortedParticipants[ i ].getDestiny().y <= thisDestiny.y )
					newIndex++;

				if( Participant.ySortedParticipants[ i ] == this )
					oldIndex = i;
			}

			// evitar o problema de newIndex == -1
			if( newIndex == -1 )
				newIndex = 0;

			// criamos ou pegamos esse participante para movê-lo na fila
			if( oldIndex == -1 ) {
				// é a primeira vez que esse participante entra na fila,
				// usamos o participante nulo do fim da fila (que deve estar vazio)
				oldIndex = ySortedParticipants.length - 1;
			}

			// Movemos na fila o score
			if ( oldIndex < newIndex ) {
				for ( int i = oldIndex; i < newIndex; ++i )
					ySortedParticipants[ i ] = ySortedParticipants[ i + 1 ];
			} else {
				for ( int i = oldIndex; i > newIndex; --i )
					ySortedParticipants[ i ] = ySortedParticipants[ i - 1 ];
			}

			// atualizamos a posição do participante no array
			ySortedParticipants[ newIndex ] = this;

			//#if DEBUG == "true"
			// Teste para saber se a ordenação está legal
			/*

			//System.out.println(" Ordenando... ");
			int iMax = -10;
			for (int i = 0; i < ySortedParticipants.length; i++) {
				if( ySortedParticipants[ i ] != null ) {
					int yPos = ySortedParticipants[i].getDestiny().y;
					if( yPos < iMax ) {
						System.out.println( "--- ERRRO!!!! ---" );
					}
					iMax = NanoMath.max( iMax, yPos );
					System.out.println(yPos);
				} else
					break;
			}
			*/
			//#endif
		}

		/**
		 * Remove participante da array ordenada por Y.
		 */
		public final void removeFromSortedArray() {
			Participant[] newSortedParticipants = new Participant[Participant.ySortedParticipants.length - 1];
			int offset = 0;
			for ( int i = 0; i < newSortedParticipants.length; i++ ) {
				if( Participant.ySortedParticipants[ i ] == this ) {
					// pulamos esse participante
					offset = 1;
				}

				newSortedParticipants[ i ] = Participant.ySortedParticipants[ i + offset ];
			}
			Participant.ySortedParticipants = newSortedParticipants;
		}
	//</editor-fold>
}
