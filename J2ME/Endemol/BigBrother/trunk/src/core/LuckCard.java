package core;

import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;


/**
 * Carta de sorte ou reves, a ser sorteada durante o jogo.
 * Quando o jogador tira uma carta, ele pode receber bonus ou penalidades
 * em seus atributos ou suas afinidades.
 * @author Ygor
 * @see Participant
 */
public class LuckCard extends GameMessage {

	private final byte attributeId;
	private final short attributeBonus;

	private int accumDelta = 0;

	protected LuckCard( String message, byte attributeId, short attributeBonus ) {
		super( GameMIDlet.getText( attributeBonus > 0? TEXT_CARD_LUCK: TEXT_CARD_BADLUCK),
				message, attributeBonus > 0 );

		this.attributeId = attributeId;
		this.attributeBonus = attributeBonus;

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}
	public final void close() {
		if( listener != null)
			listener.attributeStoppedChanging( attributeId );
	}

	public final void internalUpdate( int delta ) {
		delta += accumDelta;

		if( listener != null )
			listener.attributeIsChanging( attributeId );
		GameBoard.getPlayerParticipant().increaseAttribute( attributeId, attributeBonus * delta / EXECUTION_TIME );

		accumDelta = delta % EXECUTION_TIME;
	}

	/**
	 * Cria uma carta sorte/reves a partir do conteudo passado.
	 * @param content String contendo conteudo no formato message \ attributeId \ attributeBonus
	 */
	public static LuckCard createLuckCard( String content ) {
		int attributeIdIndex = content.indexOf( "@" );
		int attributeBonusIndex = content.indexOf( "@", attributeIdIndex + 1 );
		String[] parameters = new String[3];
		String message = content.substring( 0, attributeIdIndex );
		byte attributeId = Byte.parseByte( content.substring( attributeIdIndex + 1, attributeBonusIndex ) );
		short attributeBonus = Short.parseShort( content.substring( attributeBonusIndex + 1, content.length() ) );
		return new LuckCard( message, attributeId, attributeBonus );
	}
}
