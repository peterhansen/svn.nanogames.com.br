package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 * Mostra informacoes para o jogador durante o jogo.
 * @author Ygor
 */
public abstract class GameMessage extends DrawableGroup implements Constants, Updatable, ScreenListener
{
	//<editor-fold desc="Constants">
		protected static final int WIDTH_RATIO = 2;
		protected static final int HEIGHT_RATIO = 2;

		private static final byte STATE_STOPPED = 0;
		private static final byte STATE_STARTED = 1;
		private static final byte STATE_CLOSED = 2;

		protected static final byte MOVE_STATE_STOPPED = 0;
		protected static final byte MOVE_STATE_MOVE_UP = 1;
		protected static final byte MOVE_STATE_MOVE_DOWN = 2;

		/** Tempo (em deltas) total de execu��o da mensagem */
		protected static final int EXECUTION_TIME = 1800;

		protected static final int VELOCITY_TEXT_MOVE = 10; // px/ms
	//</editor-fold>

	//<editor-fold desc="Buttons">
		private Drawable okButton;
		private Drawable okButtonPressed;
		protected boolean okButtonIsPressed = false;
		//#if TOUCH == "true"
			protected final Rectangle okArea = new Rectangle();
		//#endif
	//</editor-fold>

	private final Pattern topBar;
	protected final Pattern fill;
	private final Pattern bottom;
	private final Pattern left;
	private final Pattern right;

	private final DrawableImage topLeft;
	private final DrawableImage topRight;
	private final DrawableImage bottomLeft;
	private final DrawableImage bottomRight;

	private final DrawableImage arrowUp;
	private final DrawableImage arrowDown;

	private Drawable patternFill;

	private byte moveState = MOVE_STATE_STOPPED;
	private int moveAccRest = 0;
	
	private MarqueeLabel title1;
	private MarqueeLabel title2;
	
	protected RichLabel text;
	private int maxTextOffset;
	private int minTextOffset;

	/** Caso a mensagem aborte as atividades quando e enviada ou nao */
	protected final boolean abortActivities = false;

	/** Caso a mensagem altere os atributos quando rodando */
	protected final boolean changeAttributes = false;

	/** Cor da carta (provavelmente sera removido) */
	protected int color = 0x000000;

	/** Tempo corrente total de execu��o da mensagem */
	private int time = 0;

	/** Estado atual da mensagem */
	protected byte state = STATE_STOPPED;

	/** Listener dos eventos dessa mensagem */
	protected ParticipantListener listener = null;
	
	protected Rectangle freeArea = new Rectangle();

	public GameMessage( String title, String message, boolean isGood ) {
		super( 12 );

		DrawableImage img;
		try {
			img = new DrawableImage( PATH_IMAGES + "border.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			img = null;
		}
		
		topLeft = new DrawableImage( img );
		topRight = new DrawableImage( img );
		bottomLeft = new DrawableImage( img );
		bottomRight = new DrawableImage( img );

		topLeft.setTransform( TRANS_MIRROR_H );
		bottomRight.setTransform( TRANS_MIRROR_V );
		bottomLeft.setTransform( TRANS_MIRROR_V | TRANS_MIRROR_H );

		insertDrawable( topLeft );
		insertDrawable( topRight );
		insertDrawable( bottomLeft );
		insertDrawable( bottomRight );

		Drawable imgBar;
		try {
			imgBar = new DrawableImage( GameMIDlet.getBar() );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			imgBar = new DrawableRect( 0xffff00 );
		}
		topBar = new Pattern( imgBar );
		
		Drawable imgBarH;
		try {
			imgBarH = new DrawableImage( PATH_IMAGES + "borderh.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			imgBarH = new DrawableRect( 0x00ff00 );
		}
		bottom = new Pattern( imgBarH );
		
		Drawable imgBarV;
		try {
			imgBarV = new DrawableImage( PATH_IMAGES + "borderv.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			imgBarV = new DrawableRect( 0xff0000 );
		}
		left = new Pattern( imgBarV );
		right = new Pattern( imgBarV );

		left.setTransform( TRANS_MIRROR_H );

		insertDrawable( topBar );
		insertDrawable( bottom );
		insertDrawable( left );
		insertDrawable( right );

		try {
			if( isGood ) patternFill = new DrawableImage( PATH_IMAGES + "pattern_good.png" );
			else patternFill = new DrawableImage( PATH_IMAGES + "pattern_bad.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			if( isGood ) patternFill = new DrawableRect( 0xffffff );
			else patternFill = new DrawableRect( 0x000000 );
		}

		fill = new Pattern( patternFill );
		insertDrawable( fill );

		final int titleCrop;
		int i = 0;
		for( i = title.length() - 1; i > 0; i-- ) {
			if( title.charAt( i ) == ' ' ) break;
		}
		titleCrop = i;
		
		try {
			title1 = new MarqueeLabel( FONT_MENU, title.substring( 0, titleCrop ).toUpperCase() );
			title1.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			title1.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			insertDrawable( title1 );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
		}
		try {
			title2 = new MarqueeLabel( FONT_MENU_BIG, title.substring( titleCrop + ( ( titleCrop > 0 ) ? 1 : 0 ), title.length() ).toUpperCase() );
			title2.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			title2.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			insertDrawable( title2 );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
		}

		try {
			text = new RichLabel( ( isGood ) ? ( FONT_TEXT_BLACK ) : ( FONT_TEXT_WHITE ), "<ALN_H>" + message + "</ALN_H>" );
			insertDrawable( text );
		}	catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			okButton = new DrawableRect( 0xff00ff );
		}

		try {
			okButton = new DrawableImage( PATH_CHARSELECTION_IMAGES + "ok.png");
		}	catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			okButton = new DrawableRect( 0xff00ff );
		}
		
		try {
			okButtonPressed = new DrawableImage( PATH_CHARSELECTION_IMAGES + "okp.png");
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			okButtonPressed = new DrawableRect( 0x880088 );
		}
		okButton.defineReferencePixel( ANCHOR_CENTER );
		okButtonPressed.defineReferencePixel( ANCHOR_CENTER );

		DrawableImage arrow;
		try {
			arrow = new DrawableImage( PATH_GAME_IMAGES + "arrow.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
			//#endif
			arrow = null;
		}

		arrowUp = new DrawableImage( arrow );
		arrowUp.setTransform( TRANS_MIRROR_V );
		arrowDown = new DrawableImage( arrow );
	}

	public void setSize( int width, int height ) {
		final int w = NanoMath.min( width, HUD_MAX_MESSAGE_WIDTH );
		final int h = NanoMath.min( height, HUD_MAX_MESSAGE_HEIGHT );
		final int x = ( ScreenManager.SCREEN_WIDTH - w ) >> 1;
		final int y = ( ScreenManager.SCREEN_HEIGHT - h ) >> 1;

		setPosition( x, y );
		super.setSize( w, h );

		topLeft.setPosition( 0, 0 );
		left.setPosition( 0, topLeft.getHeight() );
		left.setSize( left.getFill().getWidth(), getHeight() - ( bottomLeft.getHeight() + left.getPosY() ) );
		bottomLeft.setPosition( 0, left.getPosY() + left.getHeight() );
		topBar.setPosition( topLeft.getWidth(), 0 );
		topBar.setSize( getWidth() - ( topRight.getWidth() + topBar.getPosX() ), topBar.getFill().getHeight() );
		topRight.setPosition( topBar.getPosX() + topBar.getWidth(), 0 );
		right.setPosition( topBar.getPosX() + topBar.getWidth(), topRight.getHeight() );
		right.setSize( right.getFill().getWidth(), getHeight() - ( bottomRight.getHeight() + right.getPosY() ) );
		bottomRight.setPosition( topBar.getPosX() + topBar.getWidth(), right.getPosY() + right.getHeight() );
		bottom.setPosition( bottomLeft.getPosX() + bottomLeft.getWidth(), getHeight() - bottom.getFill().getHeight() );
		bottom.setSize( topBar.getWidth(), bottom.getFill().getHeight() );
		fill.setPosition( left.getPosX() + left.getWidth(), topBar.getPosY() + topBar.getHeight() );
		fill.setSize( getWidth() - ( right.getWidth() + fill.getPosX() ), getHeight() - ( topBar.getHeight() + bottom.getHeight() ) );
				
		okButton.setRefPixelPosition( topBar.getPosX() + ( topBar.getWidth() >> 1 ), topBar.getPosY() + topBar.getHeight() - ( CHAR_SELECTION_PATTERN_BAR_BORDER >> 1 ) );
		okButtonPressed.setRefPixelPosition( okButton.getRefPixelPosition() );
		//#if TOUCH == "true"
			GestureManager.setTouchArea( okArea, okButton.getPosition(), okButton.getSize(), TOUCH_TOLERANCE );
		//#endif
		
		final int sapcing = ( topBar.getHeight() - ( title1.getHeight() + title2.getHeight() + CHAR_SELECTION_PATTERN_BAR_BORDER ) ) / 2;
		title1.setPosition( topLeft.getWidth(), sapcing );
		title1.setSize( topBar.getWidth(), title1.getHeight() );
		title1.setTextOffset( ( title1.getWidth() - title1.getTextWidth() ) >> 1 );
		title2.setPosition( topLeft.getWidth(), title1.getPosY() + title1.getHeight() );
		title2.setSize( topBar.getWidth(), title2.getHeight() );
		title2.setTextOffset( ( title2.getWidth() - title2.getTextWidth() ) >> 1 );

		text.setPosition( fill.getPosX() + SAFE_MARGIN, fill.getPosY() );
		text.setSize( fill.getWidth() - ( SAFE_MARGIN << 1 ), fill.getHeight() - CHAR_SELECTION_PATTERN_BAR_BORDER - 5 );
		updateTextLimits();

		text.setTextOffset( maxTextOffset );

		arrowDown.setPosition( text.getPosX() + text.getWidth() - arrowDown.getWidth(), text.getPosY() + text.getHeight() - arrowDown.getHeight() );
		arrowUp.setPosition( text.getPosX(), text.getPosY() );
		
		freeArea.x = left.getPosX() + left.getWidth();
		freeArea.y = topBar.getPosY() + topBar.getHeight();
		freeArea.width = getWidth() - ( right.getWidth() + fill.getPosX() );
		freeArea.height = getHeight() - ( topBar.getHeight() + bottom.getHeight() );
	}
	
	
	protected final void setText( String t ) {
		text.setText( t, true );
		updateTextLimits();
	} 
	
	protected final void updateTextLimits() {
		maxTextOffset = okButton.getHeight() >> 1;
		minTextOffset = ( text.getHeight() < ( text.getTextTotalHeight() + text.getFont().getHeight() ) ) ? ( text.getHeight() - text.getTextTotalHeight() ) : ( maxTextOffset );
		text.setTextOffset( minTextOffset );
	}

	public final boolean abortActivities() { return abortActivities; }

	public void update( int delta ) {

		title1.update( delta );
		title2.update( delta );
		
		switch( state ) {
			case STATE_STOPPED:

				if( AUTOMATIC ) start();
				
				moveAccRest += delta;
				
				switch( moveState ) {
					case MOVE_STATE_MOVE_UP:
						moveText( moveAccRest / VELOCITY_TEXT_MOVE );
					break;

					case MOVE_STATE_MOVE_DOWN:
						moveText( -moveAccRest / VELOCITY_TEXT_MOVE );
					break;
				}

				moveAccRest = moveAccRest % VELOCITY_TEXT_MOVE;
			break;

			case STATE_STARTED:
				time += delta;
				internalUpdate( delta );
				if( time > EXECUTION_TIME ) {
					state = STATE_CLOSED;
				}
			break;
		}
	}

	/** M�todo de update interno da classe mensagem. O m�todo default
	 * fecha a mensagem imediatamente - deve-se sobrescrever esse m�todo
	 * para efeitos diferentes.
	 * @param delta	 */
	public void internalUpdate( int delta ) {
		// fecha janela direto
		state = STATE_CLOSED;
	}

	public void paint( Graphics g ) {
		super.paint( g );
		( okButtonIsPressed ? okButtonPressed : okButton ).draw( g );
		if( System.currentTimeMillis() % 1000 < 800 && text.isVisible() ) {
			if ( text.getTextOffset() != minTextOffset ) arrowDown.draw( g );
			if ( text.getTextOffset() != maxTextOffset ) arrowUp.draw( g );
		}
	}

	public abstract void close();

	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

	public void setMoveState( byte state ) {
		moveState = state;
		moveAccRest = 0;
	}
	
	public void moveText( int dy ) {
		text.setTextOffset( NanoMath.clamp( text.getTextOffset() + dy, minTextOffset, maxTextOffset ) );
	}

	public void hideNotify( boolean deviceEvent ) {
	}

	public void showNotify( boolean deviceEvent ) {
	}

	//<editor-fold desc="Handdle Input">
		public void keyPressed( int key ) {
			switch( key ) {
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
				case ScreenManager.KEY_FIRE:
					okButtonIsPressed = true;
				break;

				case ScreenManager.KEY_UP:
				case ScreenManager.KEY_NUM1:
					setMoveState( MOVE_STATE_MOVE_UP );
				break;

				case ScreenManager.KEY_DOWN:
				case ScreenManager.KEY_NUM8:
					setMoveState( MOVE_STATE_MOVE_DOWN );
				break;

				case ScreenManager.KEY_RIGHT:
				case ScreenManager.KEY_NUM6:
					moveText( -text.getHeight() );
				break;

				case ScreenManager.KEY_LEFT:
				case ScreenManager.KEY_NUM4:
					moveText( text.getHeight() );
				break;
			}
		}
		
		public boolean keyReleased( int key ) {
			setMoveState( MOVE_STATE_STOPPED );

			switch( key ) {
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
				case ScreenManager.KEY_FIRE:
					okButtonIsPressed = false;
					return true;
			}

			return false;
		}

		//#if TOUCH == "true"
			public boolean handleTap( int x, int y ) {
				if( okArea.contains( x - getPosX(), y - getPosY() ) ) {
					okButtonIsPressed = false;
					return true;
				}
				return false;
			}

			public void gestureIsNotSpot( int x, int y ) {
				okButtonIsPressed = false;
			}

			public void handleFirstInteraction( int x, int y ) {
				if( okArea.contains( x - getPosX(), y - getPosY() ) ) okButtonIsPressed = true;
			}

			public void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				okButtonIsPressed = ( okArea.contains( x - getPosX(), y - getPosY() ) && canBeTap );
				if( text.contains( x - getPosX(), y - getPosY() ) ) {
					moveText( dy );
				}
			}

			public void handleRelease( int x, int y ) {
				okButtonIsPressed = false;
			}
		//#endif
	//</editor-fold>

	public final void start() { state = STATE_STARTED; setVisible( false ); time = 0; }

	public final boolean isOver() { return state == STATE_CLOSED; }

	public final void setListener( ParticipantListener listenerA ) { this.listener = listenerA; }
}