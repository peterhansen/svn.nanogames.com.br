package core;

import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;

/**
 * Carta que anuncia o inicio de um acontecimento.
 * @author Ygor
 */
public class HappeningCard extends GameMessage {

	public HappeningCard( byte happeningIndex ) {
		super( GameMIDlet.getText( TEXT_CARD_HAPPENING_REPORT ), generateMessageText( happeningIndex ), true );
		color = 0x00ff0f;

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	private static String generateMessageText( byte happeningIndex ) {
		switch( happeningIndex ) {
			case TimeManager.HAPPENING_PARTY:
				return GameMIDlet.getText( TEXT_CARD_PARTY_CARD );
			
			case TimeManager.HAPPENING_VOTING:
				return GameMIDlet.getText( TEXT_CARD_VOTING_CARD );

			case TimeManager.HAPPENING_ELIMINATION:
				String message = GameMIDlet.getText( TEXT_CARD_ELIMINATION_CARD );
				return GameMIDlet.insertParticipantNames( message, GameBoard.getNomineeA(), GameBoard.getNomineeB());

			default: return "";
		}
	}

	public void close() {
		GameBoard.waitForInput();
		TimeManager.setHappeningScreen();
	}
}
