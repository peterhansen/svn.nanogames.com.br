/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;
import screens.GameScreen;

/**
 *
 * @author Caio
 */
public class RoomMenu extends DrawableGroup implements Constants, Updatable, ScreenListener
{
	private static final byte UNPRESSED_FONT = FONT_MENU;
	private static final byte PRESSED_FONT = FONT_MENU_NO_FILL;	
	
	private final Pattern topBar;
	private final Pattern left;
	private final Pattern right;

	private final DrawableImage topLeft;
	private final DrawableImage topRight;
	private final DrawableImage bottomLeft;
	private final DrawableImage bottomRight;

	private MarqueeLabel name;
	private MarqueeLabel activity;
	private final Pattern activityBkg;
	
	//<editor-fold desc="Buttons">
		private DrawableImage arrowLeft;
		private DrawableImage arrowLeftPressed;
		private static boolean arrowLeftIsPressed;
		private final Rectangle arrowLeftArea = new Rectangle();

		private DrawableImage arrowRight;
		private DrawableImage arrowRightPressed;
		private static boolean arrowRightIsPressed;
		private final Rectangle arrowRightArea = new Rectangle();
		
		private final Rectangle textArea = new Rectangle();
	//</editor-fold>
	
	private final Room room;
	private final Participant player;
	
	private int selection = 0;		

	/** Tela de jogo associada */
	private static GameScreen gameScreen = null;
	public static void setGameScreen( GameScreen gameScreen ) { RoomMenu.gameScreen = gameScreen; }

	public RoomMenu( Room room, Participant player ) {
		super( 10 );

		arrowLeftIsPressed = false;
		arrowRightIsPressed = false;
		
		this.room = room;
		this.player = player;

		DrawableImage img;
		try {
			img = new DrawableImage( PATH_IMAGES + "border.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
				System.out.println( "Rooms menu - Cant read border image" );
			//#endif
			img = null;
		}
		
		topLeft = new DrawableImage( img );
		topRight = new DrawableImage( img );
		bottomLeft = new DrawableImage( img );
		bottomRight = new DrawableImage( img );

		topLeft.setTransform( TRANS_MIRROR_H );
		bottomRight.setTransform( TRANS_MIRROR_V );
		bottomLeft.setTransform( TRANS_MIRROR_V | TRANS_MIRROR_H );

		insertDrawable( topLeft );
		insertDrawable( topRight );
		insertDrawable( bottomLeft );
		insertDrawable( bottomRight );
		
		

		Drawable imgBar;
		try {
			imgBar = new DrawableImage( GameMIDlet.getBar() );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
				System.out.println( "Rooms menu - Cant read bkg patter image" );
			//#endif
			imgBar = new DrawableRect( 0xffff00 );
		}
		topBar = new Pattern( imgBar );

		Drawable imgBarV;
		try {
			imgBarV = new DrawableImage( PATH_IMAGES + "borderv.png" );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
				System.out.println( "Rooms menu - Cant read vertical border image" );
			//#endif
			imgBarV = new DrawableRect( 0xff0000 );
		}
		left = new Pattern( imgBarV );
		right = new Pattern( imgBarV );


		insertDrawable( topBar );
		insertDrawable( left );
		insertDrawable( right );
		
		activityBkg = new Pattern( COLOR_OPTIONS_BACKGROUND );
		insertDrawable( activityBkg );
		
		try {
			name = new MarqueeLabel( FONT_MENU_NO_FILL, room.getName().toUpperCase() );
			name.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			name.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			activity = new MarqueeLabel( UNPRESSED_FONT, "XXXXXXX" );
			activity.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			activity.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			updateActivity();
		
			insertDrawable( name );
			insertDrawable( activity );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
				System.out.println( "Rooms menu - Cant read labels" );
			//#endif
		}

		try {
			arrowRight = new DrawableImage( GameMIDlet.getArrow() );
			arrowLeft = new DrawableImage( arrowRight );
			arrowRightPressed = new DrawableImage( GameMIDlet.getArrowPressed() );
			arrowLeftPressed = new DrawableImage( arrowRightPressed );

			arrowRight.mirror(TRANS_MIRROR_H);
			arrowRightPressed.mirror(TRANS_MIRROR_H);

			arrowRight.defineReferencePixel( ANCHOR_CENTER );
			arrowLeft.defineReferencePixel( ANCHOR_CENTER );
			arrowRightPressed.defineReferencePixel( ANCHOR_CENTER );
			arrowLeftPressed.defineReferencePixel( ANCHOR_CENTER );
		} catch( Throwable t ) {
			//#if DEBUG == "true"
				t.printStackTrace();
				System.out.println( "Rooms menu - Cant read arrows image" );
			//#endif			
		}

		setSize( NanoMath.min( ROOM_MENU_WIDTH, ScreenManager.SCREEN_WIDTH ), topBar.getHeight() );
	}
	
	
	public final void paint( Graphics g ) {
		super.paint( g );
		
		if( arrowLeftPressed != null && arrowLeft != null ) ( arrowLeftIsPressed ? arrowLeftPressed : arrowLeft ).draw( g );
		if( arrowRightPressed != null && arrowRight != null ) ( arrowRightIsPressed ? arrowRightPressed : arrowRight ).draw( g );
	}


	public final void setSize( int width, int height ) {
		super.setSize( NanoMath.min( ROOM_MENU_WIDTH, width ), height );
		
		defineReferencePixel( getWidth() >> 1, getHeight() >> 1 );
		setRefPixelPosition( ScreenManager.SCREEN_WIDTH >> 1, ScreenManager.SCREEN_HEIGHT >> 1 );

		topLeft.setPosition( 0, 0 );
		topRight.setPosition( getWidth() - topRight.getWidth(), topLeft.getPosY() );
		bottomLeft.setPosition( topLeft.getPosX(), getHeight() - bottomLeft.getHeight() );
		bottomRight.setPosition( topRight.getPosX(), bottomLeft.getPosY() );

		topBar.setSize( getWidth() - ( topRight.getWidth() + topLeft.getWidth() ), getHeight() );
		topBar.setPosition( topLeft.getPosX() + topLeft.getWidth(), topLeft.getPosY() );

		left.setPosition( topLeft.getPosX(), topLeft.getPosY() + topLeft.getHeight() );
		left.setSize( left.getFill().getWidth(), getHeight() - ( bottomLeft.getWidth() + topLeft.getWidth() ) );
		right.setPosition( topRight.getPosX(), topRight.getPosY() + topRight.getHeight() );
		right.setSize( right.getFill().getWidth(), getHeight() - ( topRight.getWidth() + bottomRight.getWidth() ) );

		activityBkg.setPosition( topBar.getPosX(), topBar.getPosY() + CHAR_SELECTION_PATTERN_BAR_BORDER + 35 ); //TODO passar para constants
		activityBkg.setSize( topBar.getWidth(), 43 ); //TODO passar para constants
		
		arrowLeft.setPosition( topBar.getPosX() + SAFE_MARGIN, activityBkg.getPosY() + ( ( activityBkg.getHeight() - arrowLeft.getHeight() ) >> 1 ) );
		arrowRight.setPosition( topBar.getPosX() + topBar.getWidth() - ( SAFE_MARGIN + arrowRight.getWidth() ), activityBkg.getPosY() + ( ( activityBkg.getHeight() - arrowRight.getHeight() ) >> 1 ) );
		arrowLeftPressed.setRefPixelPosition( arrowLeft.getRefPixelPosition() );
		arrowRightPressed.setRefPixelPosition( arrowRight.getRefPixelPosition() );
		//#if TOUCH == "true"
			GestureManager.setTouchArea( arrowLeftArea, arrowLeft.getPosition(), arrowLeft.getSize(), TOUCH_TOLERANCE );
			GestureManager.setTouchArea( arrowRightArea, arrowRight.getPosition(), arrowRight.getSize(), TOUCH_TOLERANCE );
		//#endif

		if( name != null ) {
			name.setPosition( topBar.getPosX(), topBar.getPosY() + CHAR_SELECTION_PATTERN_BAR_BORDER + ( ( 35 - name.getHeight() ) >> 1 ) );
			name.setSize( topBar.getWidth(), name.getHeight() );
			name.setTextOffset( ( name.getWidth() - name.getTextWidth() ) >> 1 );
		}

		if( activity != null ) {
			activity.setPosition( arrowLeft.getPosX() + arrowLeft.getWidth(), activityBkg.getPosY() + ( ( activityBkg.getHeight() - activity.getHeight() ) >> 1 ) );
			activity.setSize( arrowRight.getPosX() - activity.getPosX(), activity.getHeight() );
			activity.setTextOffset( ( activity.getWidth() - activity.getTextWidth() ) >> 1 );
		}
		
		//#if TOUCH == "true"
			GestureManager.setTouchArea( textArea, activity.getPosition(), activity.getSize(), TOUCH_TOLERANCE );
		//#endif
	}


	public final void update( int delta ) {
		activity.update( delta );
		name.update( delta );
	}


	public final void hideNotify( boolean deviceEvent ) {
	}

	public final void showNotify( boolean deviceEvent ) {
	}

	public final void sizeChanged( int width, int height ) {
		setSize( NanoMath.min( ROOM_MENU_WIDTH, ScreenManager.SCREEN_WIDTH ), topBar.getHeight() );
	}


	private void previousIndex() {
		selectIndex( selection - 1 );
	}

	private void nextIndex() {
		selectIndex( selection + 1 );
	}
	
	private void selectIndex( int index ) {
		final Activity[] acts = room.getAvailableActivities( true );
		
		if( index < 0 ) {
			index = acts.length - 1;
		} else if( index >= acts.length ) {
			index = 0;
		}
		
		selection = index;
		
		updateActivity();
	}


	private Activity getCurrentActivity() {
		final Activity[] acts = room.getAvailableActivities( true );
		return acts[ selection ];
	}


	private void updateActivity() {
		activity.setText( ' ' + getCurrentActivity().getActionName().toUpperCase() + ' ' );
		activity.setTextOffset( ( activity.getWidth() - activity.getTextWidth() ) >> 1 );
	}


	private final boolean doActivity() {
		GameBoard.setState( GameBoard.GAMESTATE_PLAYER_MOVING );
		Activity act = getCurrentActivity();
		act.getRoom().placeInFreeSpot( player );
		int activityTime = act.getActivityTime();
		player.sortByY();
		player.assignActivity( act, activityTime );
		//#if DEBUG == "true"
			System.out.println( "SetMessage -> MiniGame" );
		//#endif
		gameScreen.setMessage( new MiniGame( act.getActionName(), act.getId() ), false );
		return true;
	}
	

	//<editor-fold desc="Handle Input">
		public void keyPressed(int key) {
			switch (key) {
				case ScreenManager.KEY_LEFT:
				case ScreenManager.KEY_NUM4:
					arrowLeftIsPressed = true;
				break;


				case ScreenManager.KEY_RIGHT:
				case ScreenManager.KEY_NUM6:
					arrowRightIsPressed = true;
				break;

				case ScreenManager.KEY_FIRE:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_NUM5:
					activity.setFont( PRESSED_FONT );
				break;
			}
		}

		public boolean keyReleased(int key) {
			resetButtons();
			switch (key) {
				case ScreenManager.KEY_LEFT:
				case ScreenManager.KEY_NUM4:
					previousIndex();
				break;

				case ScreenManager.KEY_RIGHT:
				case ScreenManager.KEY_NUM6:
					nextIndex();
				break;

				case ScreenManager.KEY_FIRE:
				case ScreenManager.KEY_SOFT_MID:
				case ScreenManager.KEY_NUM5:
					return doActivity();
					
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_BACK:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_UP:
				case ScreenManager.KEY_DOWN:
				case ScreenManager.KEY_NUM1:
				case ScreenManager.KEY_NUM2:
				case ScreenManager.KEY_NUM3:
				case ScreenManager.KEY_NUM7:
				case ScreenManager.KEY_NUM8:
				case ScreenManager.KEY_NUM9:
				case ScreenManager.KEY_NUM0:
					return true;
			}
			return false;
		}
		
		private final void resetButtons() {
			arrowLeftIsPressed = arrowRightIsPressed = false;
			activity.setFont( UNPRESSED_FONT );
		}
	
		//#if TOUCH == "true"
			public final boolean handleTap( int x, int y ) {
				if( x > getPosX() && x < getPosX() + getWidth() && y > getPosY() && y < getPosY() + getHeight() ) {
					resetButtons();
					if( arrowRightArea.contains( x - getPosX(), y - getPosY() ) ) nextIndex();
					else if( arrowLeftArea.contains( x - getPosX(), y - getPosY() ) ) previousIndex();
					else if( textArea.contains( x - getPosX(), y - getPosY() ) ) return doActivity();
					return false;
				} else {
					return true;
				}
			}

			public final void handleFirstInteraction( int x, int y ) {
				if( arrowRightArea.contains( x - getPosX(), y - getPosY() ) ) arrowRightIsPressed = true;
				else if( arrowLeftArea.contains( x - getPosX(), y - getPosY() ) ) arrowLeftIsPressed = true;
				else if( textArea.contains( x - getPosX(), y - getPosY() ) ) activity.setFont( PRESSED_FONT );
			}

			public final void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				if( arrowRightArea.contains( x - getPosX(), y - getPosY() ) && canBeTap ) arrowRightIsPressed = true;
				else {
					arrowRightIsPressed = false;
					if( arrowLeftArea.contains( x - getPosX(), y - getPosY() ) && canBeTap ) arrowLeftIsPressed = true;
					else {
						arrowLeftIsPressed = false;
						if( textArea.contains( x - getPosX(), y - getPosY() ) && canBeTap ) activity.setFont( PRESSED_FONT );
						else activity.setFont( UNPRESSED_FONT );
					}
				}
			}

			public final void handleRelease( int x, int y ) {
				resetButtons();
			}

			public final void gestureIsNotSpot( int x, int y ) {
				resetButtons();
			}
		//#endif
	//</editor-fold>

}
