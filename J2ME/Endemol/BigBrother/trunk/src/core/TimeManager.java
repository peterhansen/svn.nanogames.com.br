/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;
import screens.GameScreen;

/**
 * Controla o passar do tempo do jogo, e também administra
 * os acontecimentos agendados. Todos os métodos dessa classe
 * lidam com o tempo em MINUTOS, a não ser que fique explícito
 * o contrário.
 * @author Ygor
 */
public class TimeManager implements Constants {

	//<editor-fold desc="Interruptions">
		public static final byte INTERRUPTION_NOTHING = 0;
		public static final byte INTERRUPTION_HAPPENING = 1;
		public static final byte INTERRUPTION_REPORT = 2;
		public static final byte INTERRUPTION_CARD = 3;
	//</editor-fold>

	//<editor-fold desc="Happening Constants">

		// tipos de acontecimentos
		public static final byte HAPPENING_NOTHING = 0;
		public static final byte HAPPENING_PARTY = 1;
		public static final byte HAPPENING_VOTING = 2;
		public static final byte HAPPENING_ELIMINATION = 3;

		// e suas horas de início, indexados por tipo de acontecimento
		private static final short[] HAPPENING_TIMES = { 0, 12 * HOUR, 20* HOUR, 20 * HOUR };
	//</editor-fold>

	/** Acontecimentos, indexados pela enumeração DAY_* */
	private static final byte[] HAPPENINGS = {
		HAPPENING_ELIMINATION,	// sunday
		HAPPENING_NOTHING,		// monday
		HAPPENING_NOTHING,		// tuesday
		HAPPENING_NOTHING,		// wednesday
		HAPPENING_PARTY,		// thursday
		HAPPENING_VOTING,		// friday
		HAPPENING_PARTY			// saturday
	};

	/** Tela de jogo associada */
	private static GameScreen gameScreen = null;

	// variáveis de calendário
	private static int minutes;
	private static byte dayOfWeek;
	private static byte week;
	private static int deltaRest;
	private static int deltaScaled;

	/** Informa caso o acontecimento do dia já acabou */
	private static boolean happeningStarted = false;

	//<editor-fold desc="Luck Cards Withdrawal">
		/** Define o tempo médio entre o sorteio de cartas, em minutos */
		public static final int CARD_RARITY = 300;
		public static final int CARD_RARITY_AT_PARTIES = 40;

		/** Minutos passados desde que a última carta foi tirada. Começa negativo
		 * para que não se sorteie uma carta de sorte ou revés tão cedo. */
		private static int minsToNextLuckCard;
	//</editor-fold>

	public static final void reset() {
		minutes = 8 * HOUR;
		dayOfWeek = DAY_MONDAY;
		week = 1;
		deltaRest = 0;
		deltaScaled = 0;
		happeningStarted = false;
		minsToNextLuckCard = getMinutesToNextCard();
	}

	//<editor-fold desc="Getters and Setters">
		public static int getHourOfDay() { return minutes / 60; }
		public static int getDayOfWeek() { return dayOfWeek; }
		public static int getWeek() { return week; }
		public static int getMinutes() { return minutes % 60; }
		public static void setGameScreen( GameScreen gameScreen ) { TimeManager.gameScreen = gameScreen; }
	//</editor-fold>

	/** Adiciona uma quantidade ao tempo atual. Não e possivel
	 * adicionar uma quantidade de tempo maior que um dia (parametro mins sofre clamp). */
	public static void passTime( int mins ) {

		// clamp no time, para evitar trocar mais
		// de um dia de uma vez
		mins = ( NanoMath.min( mins, DAY - 1 ) );
		minutes += mins;

		// caso tenhamos um acontecimento no dia atual e já chegou a hora dele
		if( !happeningStarted && HAPPENINGS[ dayOfWeek ] != HAPPENING_NOTHING && minutes >= HAPPENING_TIMES[ HAPPENINGS[ dayOfWeek ] ] ) {
			happeningStarted = true;
			//#if DEBUG == "true"
				System.out.println( "SetMessage -> HappeningCard" );
			//#endif
			gameScreen.setMessage( new HappeningCard( HAPPENINGS[ dayOfWeek ] ), false );
		} else if( GameBoard.isPlayerMoving() && minsToNextLuckCard <= 0 ) {
			//#if DEBUG == "true"
				System.out.println( "SetMessage -> LuckyCard" );
			//#endif
			gameScreen.setMessage( LuckCard.createLuckCard( GameMIDlet.getCardContent( NanoMath.randInt( NUMBER_OF_INGAME_CARDS ) ) ), true );
			minsToNextLuckCard = getMinutesToNextCard();
		} else {
			minsToNextLuckCard -= mins;
		}

		int nextDayMinutes = minutes - DAY;
		if ( nextDayMinutes >= 0 ) {
			// trocamos o dia
			dayOfWeek = ( byte ) ( ( dayOfWeek + 1 ) % 7 );
			happeningStarted = false;
			minutes = nextDayMinutes;

			// enviamos relatorio de inicio de dia para o jogador
			byte penaltyState = GameBoard.getPlayerParticipant().getPenaltyState();
			
			// so queremos informar o relatorio diario caso o participante
			// esteja com alguma penalidade ou caso seja um dia especial de acontecimento
			if( penaltyState != PENALTY_NOPENALTY || HAPPENINGS[ dayOfWeek ] != HAPPENING_NOTHING ) {
				//#if DEBUG == "true"
					System.out.println( "SetMessage -> ReportCard" );
				//#endif
				gameScreen.setMessage( new ReportCard( HAPPENINGS[ dayOfWeek ], penaltyState ), false );
			}
			
			if( dayOfWeek == DAY_SUNDAY ) {
				// trocamos a semana
				week += 1;
			}
		}

		HUD.updateTimeText();
	}

	public static final void updatePlayer( int delta, Participant participant, int deltaMod ) {
		delta += deltaRest;

		deltaScaled = delta / deltaMod;
		deltaRest = delta % deltaMod;

		participant.update( deltaScaled );
		passTime( deltaScaled );
	}

	public static final void updateParticipants(int delta, Participant[] participants, int deltaMod) {
		delta += deltaRest;

		deltaScaled = delta / deltaMod;
		for( int i = 0; i < participants.length; i++ )
			participants[i].update( deltaScaled );

		deltaRest = delta % deltaMod;
	}

	/** Invoca a tela associada ao acontecimento, caso o dia atual possua algum. */
	public static void setHappeningScreen() {
		switch( HAPPENINGS[ dayOfWeek ] ) {
			case TimeManager.HAPPENING_ELIMINATION:
				minutes += HOUR * 2;
				GameMIDlet.setScreen( SCREEN_ELIMINATION_SCREEN );
				HUD.updateTimeText();
				break;

			case TimeManager.HAPPENING_VOTING:
				minutes += HOUR * 2;
				GameMIDlet.setScreen( SCREEN_VOTING_SCREEN );
				HUD.updateTimeText();
				break;

			//case TimeManager.HAPPENING_PARTY:
				//forca retirada de carta de sorte/reves
				//minsToNextLuckCard = 0;
			//break;
		}
	}

	/** Gera a quantidade de minutos que deve se passar ate que uma nova
	 * carta sorte/reves seja sorteada para o participante. */
	private static int getMinutesToNextCard() {
		final int cardRarity = isPartyOn()? CARD_RARITY_AT_PARTIES: CARD_RARITY;
		return cardRarity + NanoMath.randInt( cardRarity*5 );
	}


	/** @return Caso uma festa esteja acontecendo atualmente. */
	public static boolean isPartyOn() {
		return HAPPENINGS[ dayOfWeek ] == HAPPENING_PARTY && happeningStarted;
	}
}
