/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

/**
 * Interface que deve ser implementada a fim de se receber eventos de alteracao
 * de valor de atributos de participantes.
 * @author Ygor
 */
public interface ParticipantListener {
	public abstract void attributeIsChanging( byte attributeId );
	public abstract void attributeStoppedChanging( byte attributeId );
}
