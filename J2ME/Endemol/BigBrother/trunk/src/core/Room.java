/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import screens.GameMIDlet;

/**
 * Representa um dos comodos da casa, contendo atividades e spots.
 * @author Caio, Ygor
 */
public final class Room implements Constants {

	//<editor-fold desc="Spot class">
		/**
		 * Classe interna que representa um local no qual os participantes
		 * podem ficar no comodo quando executam uma atividade.
		 */
		class Spot {
			private final int id;
			private final Point position;
			private Participant occupant;

			public Spot(int id, Room room, Point position) {
				this.id = id;
				this.position = position;
				occupant = null;
			}

			public final Participant getOccupant() { return occupant; }
			public final void setOccupant( Participant occupant ) { this.occupant = occupant; }
			public final int getId() { return id; }
			public final Point getPosition() { return position; }
		}
	//</editor-folder>

	//<editor-fold desc="Fields">
		private final byte id;
		private final Point center;
		private final Point firstTile;
		private final Point lastTile;
		private final Activity[] activities;

		/** Posicoes do comodo na qual os participantes podem ficar */
		private final Spot[] spots = new Spot[SPOTS_PER_ROOM];
	//</editor-fold>

	//<editor-fold desc="Initialization">
		public Room( byte id, Point[] tiles ) {
			this.id = id;

			if( tiles.length > 0 ) {
				this.firstTile = tiles[ 0 ];
				this.lastTile = tiles[ tiles.length - 1 ];
			} else {
				this.firstTile = new Point( 0, 0 );
				this.lastTile = new Point( 0, 0 );
			}

			this.center = new Point(
				( firstTile.x * TILE_SIZE ) + ( ( ( ( lastTile.x - firstTile.x ) + 1 ) * TILE_SIZE ) >> 1 ),
				( firstTile.y * TILE_SIZE ) + ( ( ( ( lastTile.y - firstTile.y ) + 1 ) * TILE_SIZE ) >> 1 )
			);

			//#if DEBUG == "true"
				System.out.println( "firstTile = " + firstTile + " | lastTlie = " + lastTile + " | center = " + center );
			//#endif

			switch( id ) {
				case ROOM_BEDROOM:
					activities = new Activity[] {
						new Activity( ACTIVITY_READ, 4 * HOUR, ATTRIBUTE_KNOWLEDGE ),
						new Activity( ACTIVITY_DRESS_UP, 4 * HOUR, ATTRIBUTE_BEAUTY ),
						new Activity( ACTIVITY_SOCIALIZE, 6 * HOUR, ATTRIBUTE_CHARISMA ),
						new Activity( ACTIVITY_SLEEP, 8 * HOUR, ATTRIBUTE_RESISTANCE )
					};

					spots[ 0 ] = new Spot(0, this, new Point(4, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 1 ] = new Spot(1, this, new Point(3, 5).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 2 ] = new Spot(2, this, new Point(5, 5).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 3 ] = new Spot(3, this, new Point(5, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 4 ] = new Spot(4, this, new Point(4, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 5 ] = new Spot(5, this, new Point(6, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 6 ] = new Spot(6, this, new Point(7, 5).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 7 ] = new Spot(7, this, new Point(8, 3).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 8 ] = new Spot(8, this, new Point(7, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 9 ] = new Spot(9, this, new Point(9, 6).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					break;

				case ROOM_LIVINGROOM:
					activities = new Activity[] {
						new Activity( ACTIVITY_WATCH_TV, 2 * HOUR, ATTRIBUTE_KNOWLEDGE ),
						new Activity( ACTIVITY_MIRROR_PRATICE, 2 * HOUR, ATTRIBUTE_BEAUTY ),
						new Activity( ACTIVITY_SOCIALIZE, 8 * HOUR, ATTRIBUTE_CHARISMA )
					};

					spots[ 0 ] = new Spot(0, this, new Point(12, 5).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 1 ] = new Spot(1, this, new Point(14, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 2 ] = new Spot(2, this, new Point(14, 6).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 3 ] = new Spot(3, this, new Point(18, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 4 ] = new Spot(4, this, new Point(16, 6).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 5 ] = new Spot(5, this, new Point(17, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 6 ] = new Spot(6, this, new Point(12, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 7 ] = new Spot(7, this, new Point(21, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 8 ] = new Spot(8, this, new Point(21, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 9 ] = new Spot(9, this, new Point(17, 8).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					break;

				case ROOM_POOL:
					activities = new Activity[] {
						new Activity( ACTIVITY_SWIM, 4 * HOUR, ATTRIBUTE_RESISTANCE ),
						new Activity( ACTIVITY_SUNBATHING, 6 * HOUR, ATTRIBUTE_BEAUTY ),
						new Activity( ACTIVITY_SOCIALIZE, 8 * HOUR, ATTRIBUTE_CHARISMA )
					};

					spots[ 0 ] = new Spot(0, this, new Point(15, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 1 ] = new Spot(1, this, new Point(17, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 2 ] = new Spot(2, this, new Point(18, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 3 ] = new Spot(3, this, new Point(20, 21).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 4 ] = new Spot(4, this, new Point(20, 23).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 5 ] = new Spot(5, this, new Point(11, 26).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 6 ] = new Spot(6, this, new Point(12, 26).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 7 ] = new Spot(7, this, new Point(18, 26).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 8 ] = new Spot(8, this, new Point(21, 23).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 9 ] = new Spot(9, this, new Point(22, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					break;

				case ROOM_GYM:
					activities = new Activity[] {
						new Activity( ACTIVITY_WORK_OUT, 6 * HOUR, ATTRIBUTE_RESISTANCE ),
						new Activity( ACTIVITY_PILATES, 4 * HOUR, ATTRIBUTE_BEAUTY ),
						new Activity( ACTIVITY_SOCIALIZE, 8 * HOUR, ATTRIBUTE_CHARISMA )
					};

					spots[ 0 ] = new Spot(0, this, new Point(25, 17).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 1 ] = new Spot(1, this, new Point(26, 17).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 2 ] = new Spot(2, this, new Point(27, 17).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 3 ] = new Spot(3, this, new Point(26, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 4 ] = new Spot(4, this, new Point(30, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 5 ] = new Spot(5, this, new Point(28, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 6 ] = new Spot(6, this, new Point(27, 21).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 7 ] = new Spot(7, this, new Point(28, 23).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 8 ] = new Spot(8, this, new Point(29, 23).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 9 ] = new Spot(9, this, new Point(31, 20).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					break;

				case ROOM_KITCHEN:
					activities = new Activity[] {
						new Activity( ACTIVITY_PREPARE_MEAL, 6 * HOUR, ATTRIBUTE_KNOWLEDGE ),
						new Activity( ACTIVITY_EAT, 4 * HOUR, ATTRIBUTE_RESISTANCE ),
						new Activity( ACTIVITY_SOCIALIZE, 8 * HOUR, ATTRIBUTE_CHARISMA )
					};
					
					spots[ 0 ] = new Spot(0, this, new Point(24, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 1 ] = new Spot(1, this, new Point(25, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 2 ] = new Spot(2, this, new Point(26, 7).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 3 ] = new Spot(3, this, new Point(25, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 4 ] = new Spot(4, this, new Point(27, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 5 ] = new Spot(5, this, new Point(27, 6).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 6 ] = new Spot(6, this, new Point(25, 8).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 7 ] = new Spot(7, this, new Point(23, 6).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 8 ] = new Spot(8, this, new Point(26, 4).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 9 ] = new Spot(9, this, new Point(25, 6).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					break;

				case ROOM_GAMEROOM:
					activities = new Activity[] {
						new Activity( ACTIVITY_PLAY_SNOOKER, 4 * HOUR, ATTRIBUTE_KNOWLEDGE ),
						new Activity( ACTIVITY_PLAY_VIDEO_GAME, 2 * HOUR, ATTRIBUTE_RESISTANCE ),
						new Activity( ACTIVITY_SOCIALIZE, 8 * HOUR, ATTRIBUTE_CHARISMA )
					};

					spots[ 0 ] = new Spot(0, this, new Point(2, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 1 ] = new Spot(1, this, new Point(4, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 2 ] = new Spot(2, this, new Point(6, 19).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 3 ] = new Spot(3, this, new Point(8, 20).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 4 ] = new Spot(4, this, new Point(6, 20).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 5 ] = new Spot(5, this, new Point(3, 21).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 6 ] = new Spot(6, this, new Point(4, 22).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 7 ] = new Spot(7, this, new Point(6, 22).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 8 ] = new Spot(8, this, new Point(8, 22).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );
					spots[ 9 ] = new Spot(9, this, new Point(7, 20).mul( TILE_SIZE ).add( TILE_HALF, TILE_HALF) );

					break;
				default:
					activities = new Activity[] {};
					break;
			}

			// registrando comodo das atividades
			for( int i = 0; i < activities.length; i++ )
				activities[i].setRoom( this );
		}
	//</editor-fold>

	//<editor-fold desc="Getters and Setters">
		public final Point getCenter() { return center; }
		public final int getCenterX() { return center.x; }
		public final int getCenterY() { return center.y; }

		public final Point getFirstTile() { return firstTile; }
		public final int getFirstTileX() { return firstTile.x; }
		public final int getFirstTileY() { return firstTile.y; }

		public final Point getLastTile() { return lastTile; }
		public final int getLastTileX() { return lastTile.x; }
		public final int getLastTileY() { return lastTile.y; }

		public final String getName() { return GameMIDlet.getText( TEXT_ROOM_BEDROOM + id ); }

		/**
		 * Retorna atividades praticaveis do comodo. Caso uma atividade
		 * nao possa ser praticada, ela nao e retornada no array.
		 * @param forPlayerParticipant Se as atividades disponiveis sao para o jogador ou para a IA
		 * @return Array com atividades que podem ser praticadas.
		 */
		 public final Activity[] getAvailableActivities( boolean forPlayerParticipant ) {
			Activity[] activitiesBuffer = new Activity[ activities.length ];
			int activitiesNumber = 0;

			

			// quando os participantes NPC escolhem as atividades,
			// eles já foram posicionados, logo, logo eles proprios não contam no quarto.
			// quando o participante esta no quarto onde quer, ele também nao conta
			int ammountOfParticipants = getParcipants().length;
			if( !forPlayerParticipant || GameBoard.getPlayerParticipant().getRoom() == this ) {
				ammountOfParticipants--;
			}
			
			for( int i = 0; i < activities.length; i++ ) {
				if( activities[ i ].getId() != ACTIVITY_SOCIALIZE || ammountOfParticipants > 0 ) {
					activitiesBuffer[ activitiesNumber ] = activities[ i ];
					activitiesNumber++;
				}
			}

			Activity[] availableActivities = new Activity[ activitiesNumber ];
			System.arraycopy( activitiesBuffer, 0, availableActivities, 0, activitiesNumber);
			return availableActivities;
		}
	//</editor-fold>
	
	/**
	 * Posiciona o participante em um spot livre do comodo, caso
	 * existam espaços livres disponiveis
	 * @param participant Participante o qual se deseja posicionar.
	 * @return false se ele já estiver nesse comodo
	 */
	public final boolean placeInFreeSpot( Participant participant ) {

		// se ja esta no comodo, não precisa realocar. o Participante, inclusive,
		// ja estara em modo de acao:
		//if( participant.getRoom() == this ) {
		//	participant.setActingDirectly();
		//	return false;
		//}

		// tenta posicionar participante em um lugar aleatorio do comodo
		int spotIndex = NanoMath.randFixed(SPOTS_PER_ROOM);
		if( spots[ spotIndex ].getOccupant() != null ) {
			// caso esse lugar nao esteja vazio,
			// procuramos um spot vazio nos vizinhos
			int newSpot = ( spotIndex + 1 ) % SPOTS_PER_ROOM;
			while( spots[ newSpot ].getOccupant() != null && newSpot != spotIndex )
				newSpot = ( newSpot + 1 ) % SPOTS_PER_ROOM;

			if( newSpot == spotIndex ) {
				// Não existe um lugar livre. Não deveria acontecer caso o número de lugares
				// por quarto seja maior que o número de participantes
				//#if DEBUG == "true"
					GameMIDlet.log( "Não foi possível encontrar lugar livre no comodo de id = " + id );
				//#endif
			} else {
				spotIndex = newSpot;
			}
		}

		// libera spot anterior e aloca no novo
		participant.freeSpot();
		spots[ spotIndex ].setOccupant( participant );
		participant.goTo( this, spots[spotIndex].getPosition(), spots[spotIndex].getId() );
		return true;
	}

	public final void freeSpot( int spotId ) {
		if( spots[spotId] != null )
			spots[spotId].setOccupant( null );
	}

	/**
	 * Procura nos spots do comodo por participantes e os retorna.
	 * @return Array contendo os participantes.
	 */
	public final Participant[] getParcipants() {
		Participant[] participantsBuffer = new Participant[spots.length];
		int pIndex = 0;
		
		for( int i = 0; i < spots.length; i++ ) {
			Participant p = spots[ i ].getOccupant();
			if( p != null ) {
				participantsBuffer[ pIndex ] = p;
				pIndex++;
			}
		}

		Participant[] participants = new Participant[pIndex];
		System.arraycopy(participantsBuffer, 0, participants, 0, pIndex );
		
		return participants;
	}
}
