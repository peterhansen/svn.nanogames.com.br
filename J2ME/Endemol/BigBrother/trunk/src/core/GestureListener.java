/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//#if TOUCH == "true"
package core;

/**
 *
 * @author Caio
 */
public interface GestureListener {
	public abstract void handleTap( int x, int y );
	public abstract void handleFirstInteraction( int x, int y );
	public abstract void handleDrag( int x, int y, boolean canBeTap, int dx, int dy );
	public abstract void handleRelease( int x, int y );
	public abstract void gestureIsNotSpot( int x, int y );
}
//#endif
