/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

/**
 *
 * @author Caio
 */
public class Camera implements Updatable, Constants
//#if TOUCH == "true"
	, PointerListener
//#endif
{
	//<editor-fold desc="Constants">
		private static final int CAM_VELOCITY_45 = NanoMath.toInt( CAM_VELOCITY * NanoMath.cosInt( 45 ) );
		private static final int CAMERA_MOVMENT_TIME = 500;
	//</editor-fold>


	//<editor-fold desc="Fields">
		private final Point lastTouchedPoint = new Point();

		private final Point focusPoint = new Point();
		/** Tamanho do mapa */
		private final Point MapSize = new Point();
		/** Area em pixel ocupada pela visualização camera */
		private final Point CameraArea = new Point();
		/** Posição do mapa na tela */
		private final Point mapPosition;
		/** Area navegavel da camera */
		private final Rectangle NavigableArea = new Rectangle();
		/** Limite inferior direito de posicionamento da camera */
		private final Point cameraLimit = new Point();

		private final Point CameraMovement = new Point();
		
		private final BezierCurve movementController = new BezierCurve();

		private final Point GoToPosition = new Point();
		private boolean needToGo = false;
		private int accTime = 0;

		private final Drawable map;
	//</editor-fold>

	//<editor-fold desc="Inicialization and Reset">
		public Camera( Point mapPosRef, Drawable map ) {
			mapPosition = mapPosRef;
			this.map = map;

			this.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}
	//</editor-fold> // end of Inicialization and Reset region


	//<editor-fold desc="Getters and Setters">
		public void setMapSize( Point size ){
			MapSize.set( size );
		}

		public final void setSize( int width, int height ) {
			//#if DEBUG == "true"
				System.out.println( "camera.setSize( " + width + ", " + height + " ) " );
			//#endif
			CameraArea.set( width, height );
			refresh();
		}


		public final int getWidth() { return CameraArea.x; }
		public final int getHeight() { return CameraArea.y; }
		public final Point getSize() { return CameraArea; }


		public void setCameraFocus( Point newFocus ) {
			focusPoint.x = NanoMath.clamp( newFocus.x, NavigableArea.x, cameraLimit.x );
			focusPoint.y = NanoMath.clamp( newFocus.y, NavigableArea.y, cameraLimit.y );
			map.setPosition( getRealPosition( new Point( 0, 0 ) ) );
//			mapPosition.x = ( CameraArea.x >> 1 ) - focusPoint.x;
//			mapPosition.y = ( CameraArea.y >> 1 ) - focusPoint.y;
//			map.setPosition( mapPosition );
//			//#if DEBUG == "true"
//				System.out.println( "mapPosition = " + mapPosition );
//			//#endif
		}


		public void setMovement( int x, int y ) {
		CameraMovement.set( x, y );
	}

		public void updateFocus() {
			setCameraFocus( focusPoint );
		}

		public Point getMapPosition() {
			return mapPosition;
		}

		public Point getFocusPoint() { return focusPoint; }
		public int getFocusPointX() { return focusPoint.x; }
		public int getFocusPointY() { return focusPoint.y; }

		public Point getRealPosition( int x, int y ) { return getRealPosition( new Point( x, y ) ); }
		public Point getRealPosition( Point virtualPosition ) {
			Point p = new Point( virtualPosition );
			p.subEquals( focusPoint );
			p.x += CameraArea.x >> 1;
			p.y += CameraArea.y >> 1;
			return p;
		}

		public Point getVirtualPosition( int vx, int vy ) { return getVirtualPosition( new Point( vx, vy ) ); }
		public Point getVirtualPosition( Point realPosition ) {
			Point p = new Point( realPosition );
			p.x -= CameraArea.x >> 1;
			p.y -= CameraArea.y >> 1;
			p.addEquals( focusPoint );
			return p;
		}

		public Point getRealTilePosition( int i, int j ) { return getRealTilePosition( new Point( i, j ) ); }
		public Point getRealTilePosition( Point tile ) {
			Point p = new Point( tile );
			p.mulEquals( TILE_SIZE );
			p.addEquals( TILE_HALF, TILE_HALF );
			return p;
		}
	//</editor-fold> // end of Getters and Setters region


	public void refresh() {
		NavigableArea.x = CameraArea.x >> 1;
		NavigableArea.y = CameraArea.y >> 1;
		NavigableArea.width = MapSize.x - CameraArea.x;
		NavigableArea.height = MapSize.y - CameraArea.y;
		cameraLimit.x = NavigableArea.x + NavigableArea.width;
		cameraLimit.y = NavigableArea.y + NavigableArea.height;
		updateFocus();
	}

	public void goTo( Point finalPosition ) {
//		movementController.control1.set( focusPoint );
		movementController.origin.set( focusPoint );
		movementController.destiny.set( finalPosition );
		movementController.control1.set( movementController.origin );
		movementController.control2.set( movementController.destiny );
		accTime = 0;
		needToGo = true;
	}

	public void stop() {
		CameraMovement.set( 0, 0 );
	}


	//<editor-fold desc="Handle Input">
		public void keyPressed(int key) {
			switch( key ) {
				case ScreenManager.KEY_LEFT:
				case ScreenManager.KEY_NUM4:
					setMovement( -CAM_VELOCITY, 0 );
				break;

				case ScreenManager.KEY_UP:
				case ScreenManager.KEY_NUM2:
					setMovement( 0, -CAM_VELOCITY );
				break;

				case ScreenManager.KEY_RIGHT:
				case ScreenManager.KEY_NUM6:
					setMovement( CAM_VELOCITY, 0 );
				break;

				case ScreenManager.KEY_DOWN:
				case ScreenManager.KEY_NUM8:
					setMovement( 0, CAM_VELOCITY );
				break;

				case ScreenManager.KEY_NUM1:
					setMovement( -CAM_VELOCITY_45, -CAM_VELOCITY_45 );
				break;

				case ScreenManager.KEY_NUM3:
					setMovement( CAM_VELOCITY_45, -CAM_VELOCITY_45 );
				break;

				case ScreenManager.KEY_NUM7:
					setMovement( -CAM_VELOCITY_45, CAM_VELOCITY_45 );
				break;

				case ScreenManager.KEY_NUM9:
					setMovement( CAM_VELOCITY_45, CAM_VELOCITY_45 );
				break;
			}
		}

		public void keyReleased( int key ) {
		stop();
	}


		//#if TOUCH == "true"
			public void onPointerDragged( int x, int y ) {
				setCameraFocus( focusPoint.add( lastTouchedPoint.sub( x, y ) ) );
				lastTouchedPoint.set( x, y );
			}

			public void onPointerPressed( int x, int y ) {
				lastTouchedPoint.set( x, y );
			}

			public void onPointerReleased( int x, int y ) {
			}
		//#endif
	//</editor-fold> // end of Handle Input region


	//<editor-fold desc="Update and Draw">
		public void update( int delta ) {
			if( needToGo ) {
				accTime += delta;
				if( accTime > CAMERA_MOVMENT_TIME ) {
					accTime = CAMERA_MOVMENT_TIME;
					needToGo = false;
				}
				movementController.getPointAtFixed( focusPoint, NanoMath.divInt( accTime, CAMERA_MOVMENT_TIME ) );
				setCameraFocus( focusPoint );
			} else {
				setCameraFocus( focusPoint.add( CameraMovement.mul( delta ).div( 1000 ) ) );
			}
		}
	//</editor-fold> // end of Update and Draw region
}