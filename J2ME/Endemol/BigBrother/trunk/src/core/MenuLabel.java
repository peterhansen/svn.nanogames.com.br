package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import screens.GameMIDlet;

/**
 * Label pertencente ao SubMenu, que pode ter um titulo e um estado.
 * @see SubMenu
 * @author Caio, Ygor
 */
public class MenuLabel extends UpdatableGroup implements Constants
{
	private static final byte TEXT_SPACING = 10;

	/**
	 * Classe utilizada pelo MenuLabel como Marquee que pode ser centralizada
	 * em sua largura.
	 * @author Caio, Ygor
	 */
	private class MenuMarqueeLabel extends UpdatableGroup implements Constants
	{
		private final MarqueeLabel label;

		public MenuMarqueeLabel( String text, int font ) throws Exception {
			super( 2 );

			label = new MarqueeLabel( GameMIDlet.GetFont( font ), text );
			label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			insertDrawable( label );
			setSize();
		}


		public void setFont( int fontId ) {
			label.setFont( fontId );
		}


		public void setText( int textId ){
			setText( GameMIDlet.getText( textId ) );
		}


		public void setText( String text ) {
			label.setText( text );
			label.setTextOffset( ( label.getWidth() - label.getTextWidth() ) >> 1 );
		}

		public final void setSize() {
			setSize( label.getWidth(), label.getFont().getHeight() );
		}


		public void setSize( int width, int height ) {
			super.setSize( width, height );
			defineReferencePixel( ANCHOR_CENTER );

			label.setText( label.getText(), true );
			label.setSize( getWidth(), label.getHeight() );
			label.setTextOffset( ( label.getWidth() - label.getTextWidth() ) >> 1 );
		}
	}

	private final MenuMarqueeLabel state;
	protected final Drawable title;
	protected final Pattern bkg;

	public MenuLabel( int titleText, int stateText ) throws Exception {
		this( GameMIDlet.getText( titleText ), GameMIDlet.getText( stateText ) );
	}

	public MenuLabel( String stateText ) throws Exception {
		this( null, stateText );
	}

	public MenuLabel( String titleText, String stateText ) throws Exception {
		super( 3 );

		bkg = new Pattern( COLOR_BACKGROUND );
		state = new MenuMarqueeLabel( stateText, FONT_MENU );
		insertDrawable( bkg );
		insertDrawable( state );

		if( titleText != null ) {
			title =  new MenuMarqueeLabel( titleText, FONT_MENU );
			insertDrawable( title );
		} else {
			title = null;
		}
		
		setSize( state.getWidth() + ( title != null ? title.getWidth() : 0 ) + ( ( title != null ? 3 : 2 ) * TEXT_SPACING ) , state.getHeight() );
	}


	public void setFont( int fontId ) {
		state.setFont( fontId );
	}

	public void setValueText( String text ) {
		state.setText( text );
	}

	public void setSize( int width, int height ) {		
		super.setSize( width, height );

		bkg.setSize( width, height );
		bkg.setPosition( 0, 0 );

		final int stateX;
		if( title != null ) {
			title.setPosition( TEXT_SPACING, ( height - title.getHeight() ) >> 1 );

			// TODO: por enquanto, labels com titulo tem para titulo e estado
			// cada metade da tela
			stateX = getWidth() >> 1;
		} else {
			stateX = 0;
		}
		
		state.setSize( getWidth() - stateX, state.getHeight() );
		state.setPosition( stateX , ( height - state.getHeight() ) >> 1 );
	}
}
