/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;

/**
 * Classe que representa o valor de um atributo como forma de barra, possuindo
 * uma Label que o descreve. Pode ser incluído em um Form.
 * @author Caio, Ygor
 */
public class AttributeBar extends Container implements Constants {

	public static final int LIGHT_EFFECT_THICKNESS = 2;

	private int value;
	private final Pattern fill;
	private final Pattern fillBkg;

	private final int fillColor1;
	private final int fillColor2;

	private final int fillBkgColor1;
	private final int fillBkgColor2;
	
	private final DrawableImage left;
	private final Pattern center;
	private final DrawableImage right;
	
	private final boolean hasBkgFill;
	private boolean showBkgFill;

	private int realBarMaxWidth;


	public AttributeBar( short value, int color ) throws Exception {
		this( value, color, false, 0 );
	}

	public AttributeBar( short value, int color, boolean hasBkgFill, int bkgFillColor ) throws Exception {
		super( 5 );
		
		this.hasBkgFill = hasBkgFill;
		showBkgFill = hasBkgFill;

		left =new DrawableImage( PATH_CHARSELECTION_IMAGES + "bar_left.png" );
		center = new Pattern( new DrawableImage( PATH_CHARSELECTION_IMAGES + "bar_center.png" ) );
		right =new DrawableImage( left );
		
		right.setTransform( TRANS_MIRROR_H );
		
		insertDrawable( center );
		insertDrawable( left );
		insertDrawable( right );

		fillColor1 = darken( color, 63 );
		fillColor2 = darken( color, 85 );
		
		fill = new Pattern( color );
		if( hasBkgFill ) {
			fillBkg = new Pattern( bkgFillColor );
			insertDrawable( fillBkg );

			fillBkgColor1 = darken( bkgFillColor, 63 );
			fillBkgColor2 = darken( bkgFillColor, 85 );
		} else {
			fillBkg = null;

			fillBkgColor1 = 0;
			fillBkgColor2 = 0;
		}
		insertDrawable( fill );

		this.value = NanoMath.clamp( value, 0, NanoMath.ONE );

		// o tamanho abaixo segue como dica para os métodos calcPreferredSize
		setSize( 200, left.getHeight() );
	}


	private final int darken( int color, int percentage ) {
		int r = ( ( ( color & 0xff0000 ) * percentage ) / 100 ) & 0xff0000;
		int g = ( ( ( color & 0x00ff00 ) * percentage ) / 100 ) & 0x00ff00;
		int b = ( ( ( color & 0x0000ff ) * percentage ) / 100 ) & 0x0000ff;
		return r | b | g;
	}


	private void updateBarSize() {
		fill.setSize( NanoMath.toInt( realBarMaxWidth * value ), center.getHeight() - ( BAR_Y_OFFSET_TOP + BAR_Y_OFFSET_BOTTOM + LIGHT_EFFECT_THICKNESS ) );
		if( hasBkgFill ) {
			fillBkg.setPosition( fill.getPosX() + fill.getWidth(), fill.getPosY() );
			fillBkg.setSize( realBarMaxWidth - fill.getWidth(), fill.getHeight() );
		}
	}

	public void paint( Graphics g ) {
		super.paint( g );

		g.setColor( fillColor1 );
		g.drawLine( translate.x + fill.getPosX(), translate.y + fill.getPosY() - 2, translate.x + fill.getPosX() + fill.getWidth() - 1, translate.y + fill.getPosY() - 2 );
		g.setColor( fillColor2 );
		g.drawLine( translate.x + fill.getPosX(), translate.y + fill.getPosY() - 1, translate.x + fill.getPosX() + fill.getWidth() - 1, translate.y + fill.getPosY() - 1 );

		if( hasBkgFill && showBkgFill ) {
			g.setColor( fillBkgColor1 );
			g.drawLine( translate.x + fillBkg.getPosX(), translate.y + fillBkg.getPosY() - 2, translate.x + fillBkg.getPosX() + fillBkg.getWidth() - 1, translate.y + fillBkg.getPosY() - 2 );
			g.setColor( fillBkgColor2 );
			g.drawLine( translate.x + fillBkg.getPosX(), translate.y + fillBkg.getPosY() - 1, translate.x + fillBkg.getPosX() + fillBkg.getWidth() - 1, translate.y + fillBkg.getPosY() - 1 );
		}
	}

	public int getValue() { return value; }

	public void setSize( int width, int height ) {
		super.setSize( width, height );
		realBarMaxWidth = getWidth() - ( ( left.getWidth() + BAR_X_OFFSET ) << 1 );
		int dy = ( getHeight() - center.getHeight() ) >> 1;
		left.setPosition( 0, dy );
		center.setPosition( left.getWidth(), dy );
		center.setSize( getWidth() - ( left.getWidth() + right.getWidth() ), center.getFill().getHeight() );
		right.setPosition( getWidth() - right.getWidth(), dy );
		fill.setPosition( left.getWidth() + BAR_X_OFFSET, dy + BAR_Y_OFFSET_TOP + LIGHT_EFFECT_THICKNESS );

		if( hasBkgFill ) {
			fillBkg.setPosition( fill.getPosX() + fill.getWidth(), fill.getPosY() );
		}

		updateBarSize();
	}

	public void setValue( int newValue ) {
		value = NanoMath.clamp( newValue * ( NanoMath.ONE / 100 ), 0, NanoMath.ONE );
		updateBarSize();
	}

	public void setValueFP( int newValue ) {
		value = NanoMath.clamp( newValue, 0, NanoMath.ONE );
		updateBarSize();
	}

	public final void setBkgFillVisible( boolean visible ) {
		showBkgFill = visible;
		fillBkg.setVisible( visible );
	}

	public final boolean isBkgFillVisible() { return showBkgFill && fillBkg.isVisible(); }
}
