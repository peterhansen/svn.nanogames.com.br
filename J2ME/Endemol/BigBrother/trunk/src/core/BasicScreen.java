package core;

import javax.microedition.lcdui.Graphics;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.Rectangle;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;


/**
 *
 * @author Caio
 */
public abstract class BasicScreen extends UpdatableGroup implements Constants, ScreenListener, KeyListener, Updatable //#if TOUCH == "true"
		, PointerListener, GestureListener //#endif
{


	/** Margem basica utilizada nas telas basicas */
	protected static final int BASIC_SCREEN_MARGIN = 5;
	protected final GameMIDlet midlet;
	protected final int screenID;
	protected final Pattern topBarLeft;
	protected final Pattern middleBar;
	protected final Pattern middleBarBottomBorder;
	protected final Pattern bkg;
	protected final Pattern bottomBar;
	protected final Pattern specialBar;
	private int accTime = 0;
	protected DrawableImage logo;
	protected final DrawableImage topBar;
	/** Modo de desenho de barras para essa tela. */
	private final byte barMode;
	/** Caso essa tela possua as setas sobre a barra intermediaria */
	private final boolean hasArrows;
	//<editor-fold desc="Botoes">
	protected DrawableImage okButton;
	protected DrawableImage okButtonPressed;
	protected boolean okButtonIsPressed = false;
	protected final DrawableImage backButton;
	protected final DrawableImage backButtonPressed;
	protected boolean backButtonIsPressed = false;
	protected final DrawableImage arrowLeft;
	protected final DrawableImage arrowLeftPressed;
	protected boolean arrowLeftIsPressed = false;
	protected final DrawableImage arrowRight;
	protected final DrawableImage arrowRightPressed;
	protected boolean arrowRightIsPressed = false;
	//#if TOUCH == "true"
	protected final Rectangle okArea = new Rectangle();
	protected final Rectangle backArea = new Rectangle();
	protected final Rectangle arrowLeftArea;
	protected final Rectangle arrowRightArea;
	//#endif
	//</editor-fold>

	/** Denominador da razao entre a altura da specialBar e a altura da middleBar */
	protected int specialBarHDen = 2;
	
	// atributos para lidar com o ciclo de opcoes para janela com setas
	protected int currentIndex = 0;
	protected int maxIndex;
	//#if TOUCH == "true"
	protected final GestureManager gestureManager;
	//#endif
	protected final Rectangle freeArea = new Rectangle();
	protected final Rectangle middleBarArea;
	protected final Rectangle specialBarArea;
	// modos de barra dessa tela
	public static final byte BARS_NONE = 0;
	public static final byte BARS_MIDDLE = 1;
	public static final byte BARS_MIDDLE_AND_SPECIAL = 2;


	public BasicScreen( int slots, GameMIDlet midlet, int screen, byte barMode, boolean hasArrows, int bkgColor ) throws Exception {
		super( slots + 5 );

		screenID = screen;
		this.midlet = midlet;
		this.barMode = barMode;

		bkg = new Pattern( bkgColor );
		insertDrawable( bkg );

		bottomBar = new Pattern( new DrawableImage( PATH_IMAGES + "blackbar_pattern.png" ) );

		topBar = new DrawableImage( PATH_IMAGES + "topFX.png" );
		topBarLeft = new Pattern( COLOR_CHAR_SELECTION_TOP );
		insertDrawable( topBarLeft );
		insertDrawable( topBar );

		if ( barMode != BARS_NONE ) {
			middleBar = new Pattern( new DrawableImage( GameMIDlet.getBar() ) );
			insertDrawable( middleBar );
			middleBarArea = new Rectangle();
		} else {
			// mesmo quando nao temos middleBar, utilizamos essa imagem
			// para desenhar a barra que separa o cabecalho das telas
			// de seu conteudo
			middleBar = null;
			middleBarArea = null;
		}
		middleBarBottomBorder = new Pattern( new DrawableImage( PATH_IMAGES + "borderh.png" ) );
		insertDrawable( middleBarBottomBorder );

		if ( barMode == BARS_MIDDLE_AND_SPECIAL ) {
			specialBar = new Pattern( COLOR_OPTIONS_BACKGROUND );
			specialBarArea = new Rectangle();
			insertDrawable( specialBar );
		} else {
			specialBarArea = null;
			specialBar = null;
		}

		logo = new DrawableImage( GameMIDlet.getLogo() );
		insertDrawable( logo );

		okButton = new DrawableImage( PATH_CHARSELECTION_IMAGES + "ok.png" );
		okButtonPressed = new DrawableImage( PATH_CHARSELECTION_IMAGES + "okp.png" );
		backButton = new DrawableImage( PATH_CHARSELECTION_IMAGES + "back.png" );
		backButtonPressed = new DrawableImage( PATH_CHARSELECTION_IMAGES + "backp.png" );

		okButton.defineReferencePixel( ANCHOR_CENTER );
		okButtonPressed.defineReferencePixel( ANCHOR_CENTER );
		backButton.defineReferencePixel( ANCHOR_CENTER );
		backButtonPressed.defineReferencePixel( ANCHOR_CENTER );

		this.hasArrows = hasArrows;
		if ( hasArrows ) {
			arrowRight = new DrawableImage( GameMIDlet.getArrow() );
			arrowLeft = new DrawableImage( arrowRight );
			arrowRightPressed = new DrawableImage( GameMIDlet.getArrowPressed() );
			arrowLeftPressed = new DrawableImage( arrowRightPressed );

			arrowRight.mirror( TRANS_MIRROR_H );
			arrowRightPressed.mirror( TRANS_MIRROR_H );

			arrowRight.defineReferencePixel( ANCHOR_CENTER );
			arrowLeft.defineReferencePixel( ANCHOR_CENTER );
			arrowRightPressed.defineReferencePixel( ANCHOR_CENTER );
			arrowLeftPressed.defineReferencePixel( ANCHOR_CENTER );

			//#if TOUCH == "true"
			arrowLeftArea = new Rectangle();
			arrowRightArea = new Rectangle();
			//#endif
		} else {
			arrowRight = arrowLeft = null;
			arrowRightPressed = arrowLeftPressed = null;
			//#if TOUCH == "true"
			arrowLeftArea = arrowRightArea = null;
			//#endif
		}

		//#if TOUCH == "true"
		gestureManager = new GestureManager( this );
		//#endif
	}

	//<editor-fold desc="Draw and Update Methods">

	public final void paint( Graphics g ) {
		super.paint( g );
		bottomBar.draw( g );
		drawFrontLayer( g );
	}


	public abstract void drawFrontLayer( Graphics g );


	protected void internalUpdate( int delta ) {
	}


	public final void update( int delta ) {
		super.update( delta );

		accTime += delta;

		while ( accTime >= BAR_MOVE_LOOP ) {
			accTime -= BAR_MOVE_LOOP;
		}

		updateBarPosition();
		internalUpdate( delta );
	}


	private final void updateBarPosition() {
		if( middleBar != null ) {
			middleBar.setPosition(
					( ( middleBar.getFill().getWidth() * accTime ) / BAR_MOVE_LOOP ) - middleBar.getFill().getWidth(),
								   middleBar.getPosY() );
		}
	}
	//</editor-fold>

	//<editor-fold desc="Handle Events">

	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		int topHeight = 0;
		if ( getHeight() > getWidth() || barMode == BARS_NONE ) {
			topHeight = NanoMath.min( topBar.getHeight(), getHeight() / 4 );
			topBar.setPosition( getWidth() - topBar.getWidth(), 0 );
			topBar.setSize( topBar.getWidth(), topHeight );
			topBarLeft.setSize( getWidth() - topBar.getWidth(), topHeight );
			//#if VERSION == "BBB"
//# 			removeDrawable( logo );
//# 			logo = new DrawableImage( GameMIDlet.getLogo() );
//# 			insertDrawable( logo );
//# 		} else {
//# 			removeDrawable( logo );
//# 			try { logo = new DrawableImage( PATH_IMAGES + "logo_Light.png" ); }
//# 			catch( Exception ex ) {}
//# 			insertDrawable( logo );
			//#endif
		}

		if( middleBar != null ) {
			middleBar.setPosition( 0, topHeight );
			
			// middle bar não pode ser maior que um terço da altura da tela
			final int middleBarHeight = NanoMath.min( ( getHeight() * 7 ) / 12, middleBar.getHeight() );
			middleBar.setSize( getWidth() + middleBar.getFill().getWidth(), middleBarHeight );

			middleBarBottomBorder.setSize( getWidth() + middleBarBottomBorder.getFill().getWidth(), middleBarBottomBorder.getHeight() );
			middleBarBottomBorder.setPosition( 0, middleBar.getPosY() + middleBar.getHeight() - middleBarBottomBorder.getHeight() );
			
		} else {
			middleBarBottomBorder.setPosition( 0, topHeight );
			middleBarBottomBorder.setSize( getWidth() + middleBarBottomBorder.getFill().getWidth(), middleBarBottomBorder.getHeight() );
		}

		if ( barMode != BARS_NONE ) {
			middleBarArea.set(
					middleBar.getPosX(), middleBar.getPosY() + CHAR_SELECTION_PATTERN_BAR_BORDER,
					middleBar.getWidth() - middleBar.getFill().getWidth(),
					middleBar.getHeight() - ( CHAR_SELECTION_PATTERN_BAR_BORDER << 1 ) );

			topHeight = middleBar.getPosY() + middleBar.getHeight();
			bkg.setPosition( 0, topHeight );
			bkg.setSize( getWidth(), getHeight() - ( bkg.getPosY() + 
					//#if VERSION == "GH"
//# 					CHAR_SELECTION_BAR_SIZE
					//#else
					bottomBar.getFill().getHeight()
					//#endif
					) );

			if ( barMode == BARS_MIDDLE ) {
				freeArea.set( bkg.getPosition(), bkg.getSize() );
			} else {

				// calculando altura da barra especial
				final int specialBarHeight = NanoMath.min( middleBar.getHeight() / ( 2 * specialBarHDen ), getHeight() / 10);

				specialBar.setSize( getWidth() - 2 * ( BASIC_SCREEN_MARGIN ), specialBarHeight );

									//middleBar.getHeight() / specialBarHDen );
				
				specialBar.setPosition( BASIC_SCREEN_MARGIN, topHeight + BASIC_SCREEN_MARGIN );
				specialBarArea.set( specialBar.getPosition(), specialBar.getSize() );
				freeArea.set( 0, specialBar.getPosY() + specialBar.getHeight(),
							  getWidth(), bkg.getHeight() - specialBar.getHeight() );
			}
		} else {
			bkg.setPosition( 0, topHeight + middleBarBottomBorder.getHeight() );
			bkg.setSize( getWidth(), getHeight() - ( bkg.getPosY() + 
					//#if VERSION == "GH"
//# 					CHAR_SELECTION_BAR_SIZE
					//#else
					bottomBar.getFill().getHeight()
					//#endif
					) );
			freeArea.set( bkg.getPosition(), bkg.getSize() );
		}

		logo.setPosition( GRAN_HERMANO_LOGO_X, GRAN_HERMANO_LOGO_Y );

		bottomBar.setPosition( 0, bkg.getPosY() + bkg.getHeight() );
		bottomBar.setSize( getWidth(), bottomBar.getHeight() );

		okButton.setRefPixelPosition( SAFE_MARGIN + ( okButton.getWidth() >> 1 ),
									  bottomBar.getPosY() + ( okButton.getHeight() / 4 ) - SAFE_MARGIN / 3 );
		okButtonPressed.setRefPixelPosition( okButton.getRefPixelPosition() );
		backButton.setRefPixelPosition( getWidth() - ( SAFE_MARGIN + ( backButton.getWidth() >> 1 ) ),
										bottomBar.getPosY() + ( backButton.getHeight() / 4 ) - SAFE_MARGIN / 3 );
		backButtonPressed.setRefPixelPosition( backButton.getRefPixelPosition() );

		//#if TOUCH == "true"
		GestureManager.setTouchArea( okArea, okButton.getPosition(), okButton.getSize(), TOUCH_TOLERANCE );
		GestureManager.setTouchArea( backArea, backButton.getPosition(), backButton.getSize(), TOUCH_TOLERANCE );
		//#endif

		updateBarPosition();
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

	public void exit() {
		midlet.onChoose( null, screenID, TEXT_EXIT );
	}
	//</editor-fold>

	//<editor-fold desc="Handle Input">

	protected void updateIndex() {
	}


	/**
	 * Altera o indice atual da tela, quando ela possui setas.
	 * @param newIndex
	 */
	public void setIndex( int newIndex ) {
		if ( hasArrows ) {
			currentIndex = newIndex >= 0 ? newIndex % maxIndex : maxIndex - ( NanoMath.abs( newIndex ) % maxIndex );
			updateIndex();
		}
	}


	protected void internalKeyPressed( int key ) {
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
				okButtonIsPressed = true;
				break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				backButtonIsPressed = true;
				break;
		}

		if ( hasArrows ) {
			switch ( key ) {
				case ScreenManager.KEY_NUM4:
				case ScreenManager.KEY_LEFT:
					arrowLeftIsPressed = true;
					break;

				case ScreenManager.KEY_NUM6:
				case ScreenManager.KEY_RIGHT:
					arrowRightIsPressed = true;
					break;
			}
		}

		// propaga evento
		internalKeyPressed( key );
	}


	protected void internalKeyReleased( int key ) {
	}


	public void keyReleased( int key ) {

		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
				if ( okButtonIsPressed ) {
					okButtonIsPressed = false;
					okAction();
				}
				break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				if ( backButtonIsPressed ) {
					backButtonIsPressed = false;
					backAction();
				}
				break;
		}

		if ( hasArrows ) {
			switch ( key ) {
				case ScreenManager.KEY_NUM4:
				case ScreenManager.KEY_LEFT:
					if ( arrowLeftIsPressed ) {
						arrowLeftIsPressed = false;
						setIndex( currentIndex - 1 );
					}
					break;

				case ScreenManager.KEY_NUM6:
				case ScreenManager.KEY_RIGHT:
					if ( arrowRightIsPressed ) {
						arrowRightIsPressed = false;
						setIndex( currentIndex + 1 );
					}
					break;
			}
		}

		// propaga evento para subclasse
		internalKeyReleased( key );
	}


	//#if TOUCH == "true"
	protected void internalHandleTap( int x, int y ) {
	}


	public final void handleTap( int x, int y ) {
		if ( okArea.contains( x, y ) ) {
			okAction();
			okButtonIsPressed = false;
		} else if ( backArea.contains( x, y ) ) {
			backAction();
			backButtonIsPressed = false;
		}

		if ( hasArrows ) {
			if ( arrowRightArea.contains( x, y ) ) {
				setIndex( currentIndex + 1 );
				arrowRightIsPressed = false;
			} else if ( arrowLeftArea.contains( x, y ) ) {
				setIndex( currentIndex - 1 );
				arrowLeftIsPressed = false;
			}
		}

		// propaga evento
		internalHandleTap( x, y );
	}


	protected void internalGestureIsNotSpot( int x, int y ) {
	}


	public final void gestureIsNotSpot( int x, int y ) {
		backButtonIsPressed = okButtonIsPressed = false;
		if ( hasArrows ) {
			arrowLeftIsPressed = arrowRightIsPressed = false;
		}
		internalGestureIsNotSpot( x, y );
	}


	protected void internalFirstInteraction( int x, int y ) {
	}


	public final void handleFirstInteraction( int x, int y ) {
		if ( okArea.contains( x, y ) ) {
			okButtonIsPressed = true;
		}
		if ( backArea.contains( x, y ) ) {
			backButtonIsPressed = true;
		}

		if ( hasArrows ) {
			if ( arrowRightArea.contains( x, y ) ) {
				arrowRightIsPressed = true;
			} else if ( arrowLeftArea.contains( x, y ) ) {
				arrowLeftIsPressed = true;
			}
		}

		// propaga
		internalFirstInteraction( x, y );
	}


	protected void internalHandleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
	}


	public final void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
		okButtonIsPressed = ( okArea.contains( x, y ) && canBeTap );
		backButtonIsPressed = ( backArea.contains( x, y ) && canBeTap );

		if ( hasArrows ) {
			arrowRightIsPressed = ( arrowRightArea.contains( x, y ) && canBeTap );
			arrowLeftIsPressed = ( arrowLeftArea.contains( x, y ) && canBeTap );
		}

		// propaga
		internalHandleDrag( x, y, canBeTap, dx, dy );
	}


	protected void internalHandleRelease( int x, int y ) {
	}


	public void handleRelease( int x, int y ) {
		internalHandleRelease( x, y );
	}


	public final void onPointerDragged( int x, int y ) {
		gestureManager.onPointerDragged( x, y );
	}


	public final void onPointerPressed( int x, int y ) {
		gestureManager.onPointerPressed( x, y );
	}


	public final void onPointerReleased( int x, int y ) {
		gestureManager.onPointerReleased( x, y );
	}
	//#endif
	//</editor-fold>

	//<editor-fold desc="Utils">

	/**
	 * Atualiza offset da MarqueeLabel para que ela fica centralizada
	 * em sua largura.
	 * @param label Label a qual se deseja centralizar
	 * @param x0 posicao x da esquerda
	 * @param x1 posicao x da direita
	 * @param Retorna posicao x onde a label ficaria centralizada
	 */
	protected final int centerLabelText( Label label, int x0, int x1 ) {
		return x0 + ( x1 - x0 >> 1 ) - ( label.getTextWidth() >> 1 );
	}
	//</editor-fold>

	//<editor-fold desc="Button Actions">

	protected void okAction() {
	}


	protected void backAction() {
	}
	//</editor-fold>
}