/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 *
 * @author Caio
 */
public final class HUD extends DrawableGroup implements Constants, ScreenListener, KeyListener, ParticipantListener
//#if TOUCH == "true"
	, PointerListener, GestureListener
//#endif
{
	private static Label weekLabel;
	//#if VERSION == "GH"
//# 		private static MarqueeLabel[] acronymLabel;
//# 		private static MarqueeLabel acronymSelected;
//# 
//# 		private final DrawableImage days_bar_left;
//# 		private final Pattern days_bar_center;
//# 		private final DrawableImage days_bar_right;
	//#else
		private final DrawableImage weeekDays;
		private final Pattern bottomBarBottom;
	//#endif
	private static int time;
	private static byte day;
	private static Participant participant;

	private final Pattern topBar;
	private final Pattern topBarTop;
	private final Pattern bottomBar;

	private final DrawableImage clock_bkg;
	private final DrawableImage clock_marks;
	private final Rectangle clock_fill_area = new Rectangle();

	private final DrawableImage beauty_icon;
	private final DrawableImage charisma_icon;
	private final DrawableImage knowlegde_icon;
	private final DrawableImage resistance_icon;

	private final AttributeBar beauty_bar;
	private final AttributeBar charisma_bar;
	private final AttributeBar knowlegde_bar;
	private final AttributeBar resistance_bar;

	private int lastBeauty;
	private int lastCharisma;
	private int lastKnowlegde;
	private int lastResistance;

	/** Array que diz quais atributos est�o sendo atualmente alterados */
	private final boolean[] attributesChanging = new boolean[NUMBER_OF_ATTRIBUTES];

	//<editor-fold desc="Buttons">
		private final DrawableImage statsButton;
		private final DrawableImage statsButtonPressed;
		private boolean statsButtonIsPressed = false;
		private final Rectangle statsArea = new Rectangle();

		private final DrawableImage pauseButton;
		private final DrawableImage pauseButtonPressed;
		private boolean pauseButtonIsPressed = false;
		private final Rectangle pauseArea = new Rectangle();

		private final Rectangle freeArea = new Rectangle();
	//</editor-fold>

	//#if TOUCH == "true"
		private final GestureManager gestureManager;
	//#endif

	private static boolean hasToRedrawTop;
	private static boolean hasToRedrawBottom;

	private static byte lastDay;
	private static int lastWeek;
	private static int lastShowedHour;

	private boolean lastDrawStatsIsPressed = false;
	private boolean lastDrawPauseIsPressed = false;

	//private final

	private static int WIDTH = 0;

	public HUD()  throws Exception {
		super( 0 );

		hasToRedrawTop = true;
		hasToRedrawBottom = true;

		lastDay = -1;

		time = 0;
		
		//#if VERSION == "GH"
//# 			days_bar_left =new DrawableImage( PATH_CHARSELECTION_IMAGES + "bar_left.png" );
//# 			days_bar_center = new Pattern( new DrawableImage( PATH_CHARSELECTION_IMAGES + "bar_center.png" ) );
//# 			days_bar_right =new DrawableImage( days_bar_left );
//# 			days_bar_right.setTransform( TRANS_MIRROR_H );
		//#else
			weeekDays = new DrawableImage( PATH_GAME_IMAGES + "dias_semanas.png" ); 
			bottomBarBottom = new Pattern( COLOR_TOP_BAR_TOP );
		//#endif

		topBar = new Pattern( new DrawableImage( PATH_IMAGES + "blackbar_pattern.png" ) );
		topBar.getFill().setTransform( TRANS_MIRROR_V );

		topBarTop = new Pattern( COLOR_TOP_BAR_TOP );

		bottomBar = new Pattern( new DrawableImage( PATH_IMAGES + "blackbar_pattern.png" ) );

		beauty_icon = new DrawableImage( PATH_GAME_IMAGES + "icon_beauty.png" );
		charisma_icon = new DrawableImage( PATH_GAME_IMAGES + "icon_charisma.png" );
		knowlegde_icon = new DrawableImage( PATH_GAME_IMAGES + "icon_knowlegde.png" );
		resistance_icon = new DrawableImage( PATH_GAME_IMAGES + "icon_resistance.png" );

		beauty_icon.defineReferencePixel( ANCHOR_CENTER );
		charisma_icon.defineReferencePixel( ANCHOR_CENTER );
		knowlegde_icon.defineReferencePixel( ANCHOR_CENTER );
		resistance_icon.defineReferencePixel( ANCHOR_CENTER );

		beauty_bar = new AttributeBar( (short)0, COLOR_BEAUTY, true, COLOR_CHANGING);
		charisma_bar = new AttributeBar( (short)0, COLOR_CHARISMA, true, COLOR_CHANGING);
		knowlegde_bar = new AttributeBar( (short)0, COLOR_KNOWLEDGE, true, COLOR_CHANGING);
		resistance_bar = new AttributeBar( (short)0, COLOR_RESISTANCE, true, COLOR_CHANGING);

		statsButton = new DrawableImage( PATH_GAME_IMAGES + "stats.png" );
		statsButtonPressed = new DrawableImage( PATH_GAME_IMAGES + "statsp.png" );
		statsButton.defineReferencePixel( ANCHOR_CENTER );
		statsButtonPressed.defineReferencePixel( ANCHOR_CENTER );

		pauseButton = new DrawableImage( PATH_GAME_IMAGES + "menu.png" );
		pauseButtonPressed = new DrawableImage( PATH_GAME_IMAGES + "menup.png" );
		pauseButton.defineReferencePixel( ANCHOR_CENTER );
		pauseButtonPressed.defineReferencePixel( ANCHOR_CENTER );

		clock_bkg = new DrawableImage( PATH_GAME_IMAGES + "clock_bkg.png" );
		clock_marks = new DrawableImage( PATH_GAME_IMAGES + "clock_marks.png" );

		weekLabel = new Label( FONT_WEEK, "Not Setted" );
		//dayLabel = new Label( FONT_TEXT_BLACK, "Not Setted" );

		//#if VERSION == "GH"
//# 			acronymLabel = new MarqueeLabel[ 7 ];
//# 			for( int i = 0; i < 7; i++ ) {
//# 				acronymLabel[i] = new MarqueeLabel( FONT_ACROMYN, TEXT_DAY_SUNDAY_ACRONYM + i );
//# 			}
//# 			acronymSelected = new MarqueeLabel( FONT_ACROMYN_SELECTED, "Not Setted" );
		//#endif

		//#if TOUCH == "true"
			gestureManager = new GestureManager( this );
		//#endif
	}


	public static final void unload() {
		participant = null;
		weekLabel = null;
		//#if VERSION == "GH"
//# 			acronymLabel = null;
//# 			acronymSelected = null;
		//#endif
	}


	public static final void setPlayer( Participant player ) {
		participant = player;
	}


	public static final void updateTimeText() {
		time = ( ( TimeManager.getHourOfDay() * HOUR ) + TimeManager.getMinutes() );
		day = (byte)TimeManager.getDayOfWeek();

		if( weekLabel != null && ( ( day != lastDay ) || ( TimeManager.getWeek() != lastWeek ) ) ) {
			weekLabel.setText( "Semana " + TimeManager.getWeek() );
			//#if VERSION == "GH"
//# 				acronymSelected.setText( acronymLabel[day].getText() );
//# 				acronymSelected.setPosition( acronymLabel[day].getPosition() );
//# 				acronymSelected.setSize( acronymLabel[day].getSize() );
//# 				acronymSelected.setTextOffset( acronymLabel[day].getTextOffset() );
			//#endif
			lastDay = day;
			lastWeek = TimeManager.getWeek();
			hasToRedrawBottom = true;
		}
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		//<editor-fold desc="Top Bar">
			topBarTop.setSize( getWidth(), ( HUD_TOP_BAR_DESIRED_HEIGHT - topBar.getFill().getHeight() ) + 1 );
			topBar.setPosition( 0, ( HUD_TOP_BAR_DESIRED_HEIGHT - topBar.getFill().getHeight() ) );
			topBar.setSize( getWidth(), topBar.getFill().getHeight() );
			clock_bkg.setPosition( ( topBar.getWidth() - clock_bkg.getWidth() ) >> 1, topBarTop.getPosY() );
			clock_marks.setPosition( clock_bkg.getPosX() + ( ( clock_bkg.getWidth() - clock_marks.getWidth() ) >> 1 ), clock_bkg.getPosY() + ( ( clock_bkg.getHeight() - clock_marks.getHeight() ) >> 1 ) );
			clock_fill_area.set( clock_bkg.getPosX() + ( ( clock_bkg.getWidth() - HUD_CLOCK_FILL_SIZE ) >> 1 ), clock_bkg.getPosY() + ( ( clock_bkg.getHeight() - HUD_CLOCK_FILL_SIZE ) >> 1 ), HUD_CLOCK_FILL_SIZE, HUD_CLOCK_FILL_SIZE );

			final int w = ( topBar.getWidth() - ( clock_bkg.getWidth() + ( ( HUD_CLOCK_BAR_DISTANCE + HUD_BAR_MARGIN ) << 1 ) ) ) >> 1;
			beauty_bar.setSize( w, beauty_bar.getHeight() );
			charisma_bar.setSize( w, charisma_bar.getHeight() );
			knowlegde_bar.setSize( w, knowlegde_bar.getHeight() );
			resistance_bar.setSize( w, resistance_bar.getHeight() );

			int spacing = ( topBarTop.getHeight() + topBar.getHeight() - ( beauty_bar.getHeight() + resistance_bar.getHeight() + 1 ) ) / 3;
			resistance_bar.setPosition( topBar.getPosX() + HUD_BAR_MARGIN, spacing );
			resistance_icon.setRefPixelPosition( topBar.getPosX() + ( HUD_BAR_MARGIN >> 1 ), resistance_bar.getPosY() + ( resistance_bar.getHeight() >> 1 )  );
			beauty_bar.setPosition( topBar.getPosX() + HUD_BAR_MARGIN, resistance_bar.getPosY() + resistance_bar.getHeight() + spacing );
			beauty_icon.setRefPixelPosition( topBar.getPosX() + ( HUD_BAR_MARGIN >> 1 ), beauty_bar.getPosY() + ( beauty_bar.getHeight() >> 1 )  );

			spacing = ( topBarTop.getHeight() + topBar.getHeight() - ( charisma_bar.getHeight() + knowlegde_bar.getHeight() + 1 ) ) / 3;
			knowlegde_bar.setPosition( topBar.getPosX() + topBar.getWidth() - (  knowlegde_bar.getWidth() + HUD_BAR_MARGIN ), spacing );
			knowlegde_icon.setRefPixelPosition( topBar.getPosX() + topBar.getWidth() - ( HUD_BAR_MARGIN >> 1 ), knowlegde_bar.getPosY() + ( knowlegde_bar.getHeight() >> 1 )  );
			charisma_bar.setPosition( topBar.getPosX() + topBar.getWidth() - (  charisma_bar.getWidth() + HUD_BAR_MARGIN ), knowlegde_bar.getPosY() + knowlegde_bar.getHeight() + spacing );
			charisma_icon.setRefPixelPosition( topBar.getPosX() + topBar.getWidth() - ( HUD_BAR_MARGIN >> 1 ), charisma_bar.getPosY() + ( charisma_bar.getHeight() >> 1 )  );
		//</editor-fold>

		//<editor-fold desc="Bottom Bar">
			bottomBar.setSize( getWidth(), bottomBar.getFill().getHeight() );
			bottomBar.setPosition( 0, ( getHeight() - bottomBar.getHeight() ) );

			statsButton.setRefPixelPosition( ( statsButton.getWidth() >> 1 ), getHeight() - ( ( statsButton.getWidth() >> 1 ) ) );
			statsButtonPressed.setRefPixelPosition( statsButton.getRefPixelPosition() );

			//#if TOUCH == "true"
				GestureManager.setTouchArea( statsArea, statsButton.getPosition(), statsButton.getSize(), TOUCH_TOLERANCE );
			//#endif

			pauseButton.setRefPixelPosition( bottomBar.getWidth() - ( ( pauseButton.getWidth() >> 1 ) ), getHeight() - ( ( pauseButton.getWidth() >> 1 ) ) );
			pauseButtonPressed.setRefPixelPosition( pauseButton.getRefPixelPosition() );
			//#if TOUCH == "true"
				GestureManager.setTouchArea( pauseArea, pauseButton.getPosition(), pauseButton.getSize(), TOUCH_TOLERANCE );
			//#endif

			//#if VERSION == "GH"
//# 				spacing = ( bottomBar.getHeight() - ( weekLabel.getHeight() + days_bar_left.getHeight() ) ) / 3;
//# 
//# 				weekLabel.setPosition( pauseArea.x - ( weekLabel.getWidth() + days_bar_right.getWidth() + BAR_X_OFFSET ), bottomBar.getPosY() + ( spacing )  + 2 );
//# 
//# 				spacing -= 2;
//# 
//# 				// não desejamos que o espaçamento da margem do calendário seja maior do que
//# 				// um desesseis avos da tela:
//# 				final int hudDayBarSpacing = NanoMath.min( HUD_MAX_DAY_BAR_SPACING,  width / 128 /*0*/ );
//# 
//# 				final int offset = ( width - 240 ) / 10;//( ( pauseButton.getPosX() + hudDayBarSpacing - ( days_bar_right.getWidth() + BAR_X_OFFSET ) ) - ( statsButton.getPosX() + statsButton.getWidth() + hudDayBarSpacing + days_bar_left.getWidth() + BAR_X_OFFSET ) ) % acronymLabel.length;
//# 
//# 				days_bar_left.setPosition( statsButton.getPosX() + statsButton.getWidth() + hudDayBarSpacing + ( offset >> 1 ), weekLabel.getPosY() + weekLabel.getHeight() + spacing );
//# 				days_bar_right.setPosition( pauseButton.getPosX() - ( days_bar_right.getWidth() + hudDayBarSpacing + ( ( offset >> 1 ) + ( offset | 1 ) ) ), days_bar_left.getPosY() );
//# 
//# 				days_bar_center.setPosition( days_bar_left.getPosX() + days_bar_left.getWidth(), days_bar_left.getPosY() );
//# 				days_bar_center.setSize( days_bar_right.getPosX() - ( days_bar_left.getPosX() + days_bar_left.getWidth() ), days_bar_center.getFill().getHeight() );
//# 
//# 				final int acroSize_fp = NanoMath.divInt( ( days_bar_center.getWidth() - ( BAR_X_OFFSET << 1 ) ), acronymLabel.length );
//# 				for( int i = 0; i < ( acronymLabel.length ); i++ ) {
//# 					acronymLabel[i].setPosition( days_bar_center.getPosX() + BAR_X_OFFSET + NanoMath.toInt( acroSize_fp * i ),
//# 							days_bar_center.getPosY() + BAR_Y_OFFSET_TOP + ( ( ( days_bar_center.getHeight() - ( BAR_Y_OFFSET_TOP + BAR_Y_OFFSET_BOTTOM ) ) - acronymLabel[i].getHeight() ) >> 1 ) );
//# 					acronymLabel[i].setSize( NanoMath.toInt( acroSize_fp ), acronymLabel[i].getHeight() );
//# 					acronymLabel[i].setTextOffset( ( acronymLabel[i].getWidth() - acronymLabel[i].getTextWidth() ) >> 1 );
//# 				}
//# 				acronymSelected.setPosition( acronymLabel[day].getPosition() );
//# 				acronymSelected.setSize( acronymLabel[day].getSize() );
//# 				acronymSelected.setTextOffset( acronymLabel[day].getTextOffset() );
			//#else
				bottomBar.setPosition( 0, ( getHeight() - HUD_BOTTOM_BAR_DESIRED_HEIGHT ) );
				spacing = ( HUD_BOTTOM_BAR_DESIRED_HEIGHT - ( weeekDays.getHeight() + weekLabel.getHeight() + HUD_BOTTOM_BAR_BORDER ) ) / 3;
				weeekDays.setPosition( bottomBar.getPosX() + ( ( bottomBar.getWidth() - weeekDays.getWidth() ) >> 1 ), bottomBar.getPosY() + HUD_BOTTOM_BAR_DESIRED_HEIGHT - ( spacing + weeekDays.getHeight() ) );
				weekLabel.setPosition( weeekDays.getPosX(), bottomBar.getPosY() + spacing + HUD_BOTTOM_BAR_BORDER );
				bottomBarBottom.setPosition( bottomBar.getPosX(), bottomBar.getPosY() + bottomBar.getHeight() );
				bottomBarBottom.setSize( bottomBar.getWidth(), HUD_BOTTOM_BAR_DESIRED_HEIGHT - bottomBar.getHeight() );
			//#endif
		//</editor-fold>

		freeArea.set( 0, topBar.getPosY() + topBar.getHeight(), getWidth(), bottomBar.getPosY() - ( topBar.getPosY() + topBar.getHeight() )  );

		redrawAll();

		cropArea( statsArea, bottomBar.getPosX(), bottomBar.getPosY(), bottomBar.getWidth(), bottomBar.getHeight() );
		cropArea( pauseArea, bottomBar.getPosX(), bottomBar.getPosY(), bottomBar.getWidth(), bottomBar.getHeight() );
	}


	public final void cropArea( Rectangle areaToBeCroped, Rectangle cropArea ) {
		cropArea( areaToBeCroped, cropArea.x, cropArea.y, cropArea.width, cropArea.height );
	}

	public final void cropArea( Rectangle area, int limitX, int limitY, int limitW, int limitH ) {
		if( area.x <= ( limitX + limitW ) &&
				( area.x + area.width ) >= limitX &&
				area.y <= ( limitY + limitH ) &&
				( area.y + area.height ) >= limitY ) {

			final int subX;
			final int subY;

			if( area.x < limitX ) {
				subX = limitX - area.x;
				area.x = limitX;
			} else {
				subX = 0;
			}


			if( area.y < limitY ) {
				subY =  limitY - area.y;
				area.y = limitY;
			} else {
				subY = 0;
			}

			if( area.x + area.width > limitX + limitW ) {
				area.width = ( limitX + limitW ) - area.x;
			} else {
				area.width -= subX;
			}

			if( area.y + area.height > limitY + limitH ) {
				area.height = ( limitY + limitH ) - area.y;
			} else {
				area.height -= subY;
			}
		} else {
			area.height = 0;
			area.width = 0;
		}
	}


	public final Rectangle getFreeArea() {
		return freeArea;
	}


	public final void paint( Graphics g ) {
		super.paint( g );

		//<editor-fold desc="TopBar">
			if( hasToRedrawTop ) {
				topBar.draw( g );
				topBarTop.draw( g );
				resistance_icon.draw( g );
				beauty_icon.draw( g );
				knowlegde_icon.draw( g );
				charisma_icon.draw( g );
			}

			if( hasToRedrawTop || time != lastShowedHour ) {
				clock_bkg.draw( g );
				g.setColor( COLOR_CLOCK );
				g.fillArc( clock_fill_area.x, clock_fill_area.y, clock_fill_area.width, clock_fill_area.height, 90, -( ( ( time * 360 ) / DAY ) ) );
				lastShowedHour = time;
				clock_marks.draw( g );
			}


			if( participant != null ) {
				if( hasToRedrawTop || ( resistance_bar.isBkgFillVisible() != attributesChanging[ATTRIBUTE_RESISTANCE] ) || lastResistance != participant.getResistance() * 100 / ATTRIBUTE_MAX ) {
					resistance_bar.setBkgFillVisible( attributesChanging[ATTRIBUTE_RESISTANCE] );
					lastResistance = participant.getResistance() * 100 / ATTRIBUTE_MAX;
					resistance_bar.setValue( lastResistance );
					resistance_bar.draw( g );
				}

				if( hasToRedrawTop || ( beauty_bar.isBkgFillVisible() != attributesChanging[ATTRIBUTE_BEAUTY] ) || lastBeauty != participant.getBeaty() * 100 / ATTRIBUTE_MAX ) {
					beauty_bar.setBkgFillVisible( attributesChanging[ATTRIBUTE_BEAUTY] );
					lastBeauty = participant.getBeaty() * 100 / ATTRIBUTE_MAX;
					beauty_bar.setValue( lastBeauty );
					beauty_bar.draw( g );
				}

				if( hasToRedrawTop || ( knowlegde_bar.isBkgFillVisible() != attributesChanging[ATTRIBUTE_KNOWLEDGE] ) || lastKnowlegde != participant.getKnowledge() * 100 / ATTRIBUTE_MAX ) {
					knowlegde_bar.setBkgFillVisible( attributesChanging[ATTRIBUTE_KNOWLEDGE] );
					lastKnowlegde = participant.getKnowledge() * 100 / ATTRIBUTE_MAX;
					knowlegde_bar.setValue( lastKnowlegde );
					knowlegde_bar.draw( g );
				}

				if( hasToRedrawTop || ( charisma_bar.isBkgFillVisible() != attributesChanging[ATTRIBUTE_CHARISMA] ) || lastCharisma != participant.getCharisma() * 100 / ATTRIBUTE_MAX ) {
					charisma_bar.setBkgFillVisible( attributesChanging[ATTRIBUTE_CHARISMA] );
					lastCharisma = participant.getCharisma() * 100 / ATTRIBUTE_MAX;
					charisma_bar.setValue( lastCharisma );
					charisma_bar.draw( g );
				}
			}

			if( hasToRedrawTop ) hasToRedrawTop = false;
		//</editor-fold>

		//<editor-fold desc="BottomBar">
			if( hasToRedrawBottom || ( pauseButtonIsPressed != lastDrawPauseIsPressed ) || ( lastDrawStatsIsPressed != statsButtonIsPressed )) {
				bottomBar.draw( g );
				//dayLabel.draw( g );

				//#if VERSION == "GH"
//# 					days_bar_left.draw( g );
//# 					days_bar_right.draw( g );
//# 					days_bar_center.draw( g );
//# 				
//# 					for( int i = 0; i < acronymLabel.length; i++ ) {
//# 						if( i != day ) acronymLabel[i].draw( g );
//# 					}
//# 					acronymSelected.draw( g );
//# 
//# 					for( int i = 0; i < acronymLabel.length - 1; i++ ) {
//# 						g.setColor( 0x929292 );
//# 						g.drawLine( translate.x + acronymLabel[ i ].getPosX() + acronymLabel[ i ].getWidth(),
//# 								translate.y + days_bar_center.getPosY() + BAR_Y_OFFSET_TOP,
//# 								translate.x + acronymLabel[ i ].getPosX() + acronymLabel[ i ].getWidth(),
//# 								translate.y + days_bar_center.getPosY() + days_bar_center.getHeight() - BAR_Y_OFFSET_BOTTOM );
//# 					}
//# 
//# 					g.setColor( 0xeb8f20 );
//# 					g.drawRect( translate.x + acronymLabel[ day ].getPosX() - 1, translate.y + days_bar_center.getPosY() + BAR_Y_OFFSET_TOP - 1, acronymLabel[ day ].getWidth() + 2, days_bar_center.getHeight() + 1 - ( BAR_Y_OFFSET_TOP + BAR_Y_OFFSET_BOTTOM ) );
//# 					g.setColor( 0xffd76b );
//# 					g.drawRect( translate.x + acronymLabel[ day ].getPosX(), translate.y + days_bar_center.getPosY() + BAR_Y_OFFSET_TOP - 2, acronymLabel[ day ].getWidth(), days_bar_center.getHeight() + 3 - ( BAR_Y_OFFSET_TOP + BAR_Y_OFFSET_BOTTOM ) );
				//#else
					bottomBarBottom.draw( g );
					weeekDays.draw( g );
					
					final int w = ( weeekDays.getWidth() / 7 );
					final int x = translate.x + weeekDays.getPosX() + ( ( ( day + 6 ) % 7 ) * w );
					final int y = translate.y + weeekDays.getPosY();
 					g.setColor( 0x0d3755 );
 					g.drawRect( x, y + 1, w + 1, weeekDays.getHeight() - 6 );
 					g.drawRect( x + 1, y, w - 1, weeekDays.getHeight() - 4);
				//#endif
					
				weekLabel.draw( g );

				( pauseButtonIsPressed ? pauseButtonPressed : pauseButton ).draw( g );
				lastDrawPauseIsPressed = pauseButtonIsPressed;
				( statsButtonIsPressed ? statsButtonPressed : statsButton ).draw( g );
				lastDrawStatsIsPressed = statsButtonIsPressed;
				
				hasToRedrawBottom = false;
			}
		//</editor-fold>
	}


	public void hideNotify( boolean deviceEvent ) {
	}

	public void showNotify( boolean deviceEvent ) {
		redrawAll();
	}

	public void redrawAll() {
		hasToRedrawTop = true;
		hasToRedrawBottom = true;
	}

	public final boolean contains( int x, int y ) {
		return !freeArea.contains( x, y );
	}


	private final void pauseMenuAction() {
		GameMIDlet.setScreen( SCREEN_MENU_GAME );
	}

	private final void statsMenuAction() {
		GameMIDlet.setScreen( SCREEN_STATS_MENU );
	}

	//<editor-folder desc="Participant Listener Implmentation">
		public void attributeIsChanging( byte attributeId ) {
			attributesChanging[attributeId] = true;
		}

		public void attributeStoppedChanging( byte attributeId ) {
			attributesChanging[attributeId] = false;
		}
	//</editor-folder>

	//<editor-fold desc="Handle Input">
		public void keyPressed( int key ) {
			switch( key ) {
				case ScreenManager.KEY_SOFT_LEFT:
					statsButtonIsPressed = true;
				break;

				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					pauseButtonIsPressed = true;
				break;
			}
		}

		public void keyReleased( int key ) {
			statsButtonIsPressed = false;
			pauseButtonIsPressed = false;
			switch( key ) {
				case ScreenManager.KEY_SOFT_LEFT:
					statsMenuAction();
				break;

				case ScreenManager.KEY_SOFT_RIGHT:
				case ScreenManager.KEY_CLEAR:
				case ScreenManager.KEY_BACK:
					pauseMenuAction();
				break;
			}
		}

		//#if TOUCH == "true"
			public final void onPointerDragged( int x, int y ) {
				gestureManager.onPointerDragged( x, y );
			}

			public final void onPointerPressed( int x, int y ) {
				gestureManager.onPointerPressed( x, y );
			}

			public final void onPointerReleased( int x, int y ) {
				gestureManager.onPointerReleased( x, y );
			}


			//<editor-fold desc="Gesture Listener">
				public void handleTap( int x, int y ) {
					if( statsArea.contains(x, y) ) {
						statsMenuAction();
						statsButtonIsPressed = false;
					}

					if( pauseArea.contains(x, y) ) {
						pauseMenuAction();
						pauseButtonIsPressed = false;
					}
				}

				public void gestureIsNotSpot( int x, int y ) {
					statsButtonIsPressed = false;
					pauseButtonIsPressed = false;
				}

				public void handleFirstInteraction( int x, int y ) {
					if( statsArea.contains(x, y) ) statsButtonIsPressed = true;
					if( pauseArea.contains(x, y) ) pauseButtonIsPressed = true;
				}

				public void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
					statsButtonIsPressed = ( statsArea.contains(x, y) && canBeTap );
					pauseButtonIsPressed = ( pauseArea.contains(x, y) && canBeTap );
				}

				public void handleRelease( int x, int y ) {
				}
			//</editor-fold>
		//#endif
	//</editor-fold>
}
