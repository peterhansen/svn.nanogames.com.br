package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;
import screens.GameScreen;

/**
 * O tabuleiro contem as pecas que o compoem (tiles), os comodos
 * e os participantes do jogo.
 * @author Caio, Ygor
 * @see Participant
 * @see Room
 */
public final class GameBoard extends Drawable implements Constants, ScreenListener {

	//<editor-fold desc="Constants">
		private static final byte DIRECTION_ANGLE_0		= 0;
		private static final byte DIRECTION_ANGLE_45	= 1;
		private static final byte DIRECTION_ANGLE_90	= 2;
		private static final byte DIRECTION_ANGLE_135	= 3;
		private static final byte DIRECTION_ANGLE_180	= 4;
		private static final byte DIRECTION_ANGLE_225	= 5;
		private static final byte DIRECTION_ANGLE_270	= 6;
		private static final byte DIRECTION_ANGLE_315	= 7;
		
		private static final int DELTA_DIVISION_ON_MOVE = ( AUTOMATIC ? 1 : 50 );
		private static final int DELTA_DIVISION_ON_ACTIVITY = ( AUTOMATIC ? 1 : 2 );

		// estados do tabuleiro
		public static final byte GAMESTATE_WAITING_INPUT		= 0;
		public static final byte GAMESTATE_PLAYER_MOVING		= 1;
		public static final byte GAMESTATE_PLAYER_ON_MINIGAME	= 2;
		public static final byte GAMESTATE_PLAYER_ACTING		= 3;
		public static final byte GAMESTATE_NONPLAYER_MOVING		= 4;
		public static final byte GAMESTATE_NONPLAYER_ACTING		= 5;
	//</editor-fold> // end of Constants region

	//<editor-fold desc="Fields">
		private static int TILES_PER_LINE;

		private final Image tilesImg;
		private final Image objectsImg;
		private byte[][] tileMap;
		private byte[][] objectsMap;
		private final RichLabel roomTitleLabel;
		private final Room[] rooms;

		//<editor-fold desc="Statics variable">
			/** Array com os participantes atualmente na casa */
			private static Participant[] participants;
			private static Participant[] eliminatedParticipants;

			/** Array com todos os participantes, mesmo os já eliminados */
			private static Participant[] allTimeParticipants;

			// participantes votados para o dia da eliminacao
			private static Participant nomineeA;		
			private static Participant nomineeB;

			/** Participante do jogador */
			private static Participant playerParticipant;
		
			private static byte state;
		//</editor-fold>

		private Camera cam;
		private int selectedRoom;

		private final Rectangle drawableElementArea = new Rectangle();
		private final Rectangle drawableArea = new Rectangle();
		private final Rectangle intersection = new Rectangle();		
	//</editor-fold> // end of Fields region
	

	//<editor-fold desc="Inicialization and Reset">
		public GameBoard( int playerId ) throws Exception {
			super();
			
			state = GAMESTATE_PLAYER_MOVING;

			roomTitleLabel = new RichLabel( FONT_MENU_BIG );
			roomTitleLabel.defineReferencePixel( ANCHOR_CENTER );

			//<editor-fold desc="Tile Map Creation">
			//#if DEBUG == "true"
				System.gc();
				long freeMem = Runtime.getRuntime().freeMemory();
			//#endif

			tilesImg = Image.createImage( PATH_GAME_IMAGES + "tiles.png" );
			TILES_PER_LINE = tilesImg.getWidth() / TILE_SIZE;

			objectsImg = Image.createImage( PATH_GAME_IMAGES + "tiles_obj.png" );

			//#if DEBUG == "true"
				System.gc();
				System.out.println("tiles.png ocupa: " + (freeMem - Runtime.getRuntime().freeMemory()));
				freeMem = Runtime.getRuntime().freeMemory();
			//#endif

			tileMap = readMapTiles( PATH_TEXT + "map.dat", -1 );
			objectsMap = readMapTiles( PATH_TEXT + "obj.dat", -113 ); // offset do tile map - (numeroDeTiles do TileMap1) - 1 = 112 - 1 = -113

			for( int j = 0; j < objectsMap[0].length; j++ ) {
				for( int i = 0; i < objectsMap.length; i++ ) {
					//#if DEBUG == "true"
						System.out.print( objectsMap[i][j] + ", " );
					//#endif
				}
				System.out.println();
			}

			setBoardSize(tileMap.length * TILE_SIZE, tileMap[0].length * TILE_SIZE);
			//</editor-fold> // end of Tile Map Creation region

			//<editor-fold desc="Rooms Creation">
			Point[][] roomsTiles = readRoomsTiles( PATH_TEXT + "rooms.dat" );
			rooms = new Room[ roomsTiles.length ];

			for (byte r = 0; r < this.rooms.length; r++) {
				this.rooms[r] = new Room( r, roomsTiles[r] );
			}
			//</editor-fold> end of Rooms Creation region

			//<editor-fold desc="Participants Creation">
				eliminatedParticipants = new Participant[ 0 ];
				participants = new Participant[ PARTICIPANTS_NUMBER ];
				for ( int i = 0; i < participants.length; i++ ) {
					participants[i] = new Participant( i, GameMIDlet.getName( i + 1 ) );

					// TODO: em qual quarto cada um deles começa?
					rooms[0].placeInFreeSpot(participants[i]);

					// quartos favoritos de cada participante
					switch( i ) {
						case PARTICIPANT_SAINTY:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_BEDROOM],
								rooms[ROOM_LIVINGROOM],
								rooms[ROOM_KITCHEN]
							});
						break;
						case PARTICIPANT_NERD:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_BEDROOM],
								rooms[ROOM_LIVINGROOM],
								rooms[ROOM_GAMEROOM]
							});
						break;
						case PARTICIPANT_HOUSEWIFE:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_BEDROOM],
								rooms[ROOM_KITCHEN],
								rooms[ROOM_LIVINGROOM]
							});
						break;
						case PARTICIPANT_GAY:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_POOL],
								rooms[ROOM_GAMEROOM],
								rooms[ROOM_BEDROOM]
							});
						break;
						case PARTICIPANT_HOTGIRL:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_GYM],
								rooms[ROOM_POOL],
								rooms[ROOM_LIVINGROOM]
							});
						break;
						case PARTICIPANT_GYMDUDE:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_GYM],
								rooms[ROOM_POOL],
								rooms[ROOM_LIVINGROOM]
							});
						break;
						case PARTICIPANT_PRETTY:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_KITCHEN],
								rooms[ROOM_POOL],
								rooms[ROOM_LIVINGROOM]
							});
						break;
						case PARTICIPANT_NORMAL:
							participants[i].setFavoriteRooms(new Room[] {
								rooms[ROOM_LIVINGROOM],
								rooms[ROOM_GAMEROOM],
								rooms[ROOM_BEDROOM]
							});
						break;
					}
				}

				// ordenação por Y
				for( int i = 0; i < participants.length; i++ )  {
					participants[i].sortByY();
				}
			//</editor-fold>

			// guardando array com referencias para todos participantes
			allTimeParticipants = new Participant[ PARTICIPANTS_NUMBER ];
			System.arraycopy( participants, 0, allTimeParticipants, 0, participants.length );

			// determinando participante do jogador
			playerParticipant = participants[ playerId ];
			playerParticipant.setPlayerParticipant( true );
			reset();
		}


		public static final byte[][] readMapTiles( String fileName, int offset ) throws Exception {
			TileMapReader tmr = new TileMapReader( (short)offset );
			
			GameMIDlet.openJarFile( fileName, tmr );
			
			return tmr.ret;
		}


		public static void unload() {
			nomineeA = null;
			nomineeB = null;
			playerParticipant = null;
			for( int i = 0; i < participants.length; i++ ) participants[i] = null;
			for( int i = 0; i < allTimeParticipants.length; i++ ) allTimeParticipants[i] = null;
			for( int i = 0; i < eliminatedParticipants.length; i++ ) eliminatedParticipants[i] = null;
		}
		
		private static final class TileMapReader implements Serializable {
				byte[][] ret = null;
				short offset;
					
				private final byte STATE_READ_WIDTH = 0;
				private final byte STATE_READ_HEIGHT = 1;
				private final byte STATE_READ_TILES = 2;

				public TileMapReader( short offset ) {
					this.offset = offset;
				}

				public void write( DataOutputStream output ) throws Exception {
				}

				public void read( DataInputStream input ) throws Exception {
					final StringBuffer b = new StringBuffer();
					boolean startedNumber = false;
					boolean reading = true;
					int x = 0, y = 0;
					int cx = 0, cy = 0;		
					
					byte state = STATE_READ_WIDTH;

					while ( reading ) {
						final char c = ( char ) input.readUnsignedByte();

						switch ( c ) {
							case '0': case '1': case '2': case '3': case '4':
							case '5': case '6': case '7': case '8': case '9':
								startedNumber = true;
								b.append( c );
							break;

							default:
								if ( startedNumber ) {
									short temp  = Short.parseShort( b.toString() );
									switch ( state ) {
										case STATE_READ_WIDTH:
											x = temp;
											state = STATE_READ_HEIGHT;
										break;
										
										case STATE_READ_HEIGHT:
											y = temp;
											ret = new byte[x][y];
											for( int i = 0; i < x; i++ ) {
												ret[i] = new byte[y];
											}
											state = STATE_READ_TILES;
										break;
										
										case STATE_READ_TILES:
											ret[cx++][cy] = (byte)( temp + offset );
											
											if( cx >= x ) {
												cy++; 
												if( cy >= y ) {
													reading = false;
												} else {
													cx = 0;
												}
											}
										break;
									}
									startedNumber = false;
									b.setLength( 0 );
								}
							break;
						}
					}
				}
		}
		

		public static final Point[][] readRoomsTiles( String fileName ) throws Exception {			
			RoomsTilesReader tmr = new RoomsTilesReader();
			
			GameMIDlet.openJarFile( fileName, tmr );
			
			return tmr.ret;
		}
		
		private static final class RoomsTilesReader implements Serializable {
				Point[][] ret = null;
					
				private final byte STATE_READ_QUANTITY = 0;
				private final byte STATE_READ_TILE_X = 1;
				private final byte STATE_READ_TILE_Y = 2;
				
				private final byte NUMBER_OF_POINTS = 2;

				public void write( DataOutputStream output ) throws Exception {
				}

				public void read( DataInputStream input ) throws Exception {
					final StringBuffer b = new StringBuffer();
					boolean startedNumber = false;
					boolean reading = true;
					int rooms = 0, r = 0, t = 0;
					
					byte state = STATE_READ_QUANTITY;

					while ( reading ) {
						final char c = ( char ) input.readUnsignedByte();

						switch ( c ) {
							case '0': case '1': case '2': case '3': case '4':
							case '5': case '6': case '7': case '8': case '9':
								startedNumber = true;
								b.append( c );
							break;

							default:
								if ( startedNumber ) {
									short temp  = Short.parseShort( b.toString() );
									switch ( state ) {
										case STATE_READ_QUANTITY:
											rooms = temp;
											ret = new Point[ rooms ][ NUMBER_OF_POINTS ];
											for( int i = 0; i < rooms; i++ ) {
												ret[ i ] = new Point[ NUMBER_OF_POINTS ];
												for(int j = 0; j < NUMBER_OF_POINTS; j++ ) {
													ret[i][j] = new Point();
												}
											}
											state = STATE_READ_TILE_X;
										break;
										
										case STATE_READ_TILE_X:
											ret[r][t] = new Point();
											//#if DEBUG == "true"
												//System.out.println( "ret[ "+r+", "+t+" ].x = " + temp );
											//#endif
											ret[r][t].x = temp;
											state = STATE_READ_TILE_Y;
										break;
										
										case STATE_READ_TILE_Y:
											//#if DEBUG == "true"
												System.out.println( "ret[ "+r+", "+t+" ].y = " + temp );
											//#endif

											ret[r][t++].y = temp;
											
											if( t >= NUMBER_OF_POINTS ) {
												r++; 
												if( r >= rooms ) {
													reading = false;
												} else {
													t = 0;
												}
											}
											state = STATE_READ_TILE_X;
										break;
									}
									startedNumber = false;
									b.setLength( 0 );
								}
							break;
						}
					}
				}
		}

		
		public final void reset() {
			selectedRoom = -1;
		}
	//</editor-fold> // end of Inicialization and Reset region

	//<editor-fold desc="Static Methods">

		/**
		 * Elimina participante do jogo, realizando todas as limpezas necessarias.
		 * @param eliminated Participante a ser eliminado.
		 */
		public static final void eliminate( Participant eliminated ) {
			
			// remover da ordenacao em Y
			eliminated.removeFromSortedArray();

			// apos eliminacao, atualizamos matrizes de afinidade
			// pra que ninguem mais vote nele
			for( int i = 0; i < participants.length; i++ ) {
				for ( int j = 0; j < PARTICIPANTS_NUMBER; j++ ) {
					participants[ i ].ignoreForVoting( eliminated.getId() );
				}
			}

			// liberamos o spot no qual o participante esta
			eliminated.freeSpot();

			Participant[] newEliminateds =  new Participant[ eliminatedParticipants.length + 1 ];
			newEliminateds[0] = eliminated;
			for( int i = 1; i <= eliminatedParticipants.length; i++ ) {
				newEliminateds[ i ] = eliminatedParticipants[ i - 1 ];
			}
			eliminatedParticipants = newEliminateds;
			
			nomineeA = null;
			nomineeB = null;

			Participant[] newParticipants = new Participant[ participants.length - 1 ];
			int offset = 0;
			for( int i = 0; i < newParticipants.length; i++ ) {
				if( participants[ i ] == eliminated ) {
					// pulamos esse participante
					offset = 1;
				}
				newParticipants[ i ] = participants[ i + offset ];
			}
			participants = newParticipants;
		}

		public static final boolean isLastWeek() { return ( getParticipants().length < 3 ); }
		public static final Participant getNomineeA() {	return nomineeA; }
		public static final Participant getNomineeB() {	return nomineeB; }
	//</editor-fold>

	//<editor-fold desc="Getters and Setters">
		public static boolean isPlayerMoving() {
			return state == GAMESTATE_PLAYER_MOVING;
		}

		public final void setPositionOffset( int x, int y, int w, int h ) {
			drawableArea.set( x, y, w, h );
		}

		public final void setBoardSize(int width, int height) {
			super.setSize(width, height);
		}

		public void setCamera(Camera camera) {
			cam = camera;
		}


		public static void setState( int state ) {
			GameBoard.state = (byte)state;
		}


		public final Room getCurrentRoom() {
			return hasSelection()? rooms[ selectedRoom ]: null;
		}

		public final void selectRoom( int id ) {
			if ( selectedRoom == id ) {
				openDialog();
				return;
			}

			selectedRoom = id;
			updateRoomTitleLabel();
			if (hasSelection()) {
				cam.goTo( getCurrentRoom().getCenter() );
			}
		}

		public final Activity[] getCurrentActivities() {
			return hasSelection()? getCurrentRoom().getAvailableActivities( true ): null;
		}

		public static final Participant[] getParticipants() {
			return participants;
		}

		public static final Participant[] getEliminatedParticipants() {
			return eliminatedParticipants;
		}

		public static final Participant getPlayerParticipant() {
			return playerParticipant;
		}

		public final boolean hasSelection() {
			return selectedRoom >= 0 && selectedRoom < rooms.length;
		}


		private final int getOffsetX(byte id) {
			return (id % TILES_PER_LINE) * TILE_SIZE;
		}

		private final int getOffsetY(byte id) {
		return (id / TILES_PER_LINE) * TILE_SIZE;
	}

	/**
	 * Determina participantes que devem ir para o dia da eliminacao.
	 * Esses participantes podem ser nulos.
	 * @param nomineeA Participante A.
	 * @param nomineeB Participante B.
	 */
	public final static void setNominees( Participant a, Participant b ) {
		nomineeA = a;
		nomineeB = b;
	}

	//</editor-fold> // end of Getters and Setters region

	private final void openDialog() {
		Room r = getCurrentRoom();

		//#if DEBUG == "true"
			//System.out.println("is null? " + (r == null));
		//#endif
		
		if (r != null) {
			unselectRoom();
			GameScreen.setRoomMenu( r, getPlayerParticipant() );
		}

		//unselectRoom();
	}

	private final void unselectRoom() {
		selectRoom(-1);
	}

	private final void updateRoomTitleLabel() {
		Room r = getCurrentRoom();
		if ( r != null ) {
			roomTitleLabel.setText( "<ALN_H>" + r.getName().toUpperCase() + "</ALN_H>");
			roomTitleLabel.setSize( NanoMath.min( ScreenManager.SCREEN_WIDTH - 20, ( r.getLastTileX() + 1 - r.getFirstTileX() ) * TILE_SIZE ), roomTitleLabel.getFont().getHeight() << 2 );
			roomTitleLabel.formatText( false );
			roomTitleLabel.setSize( roomTitleLabel.getWidth(), roomTitleLabel.getTextTotalHeight() );
			roomTitleLabel.defineReferencePixel( ANCHOR_CENTER );
			roomTitleLabel.setRefPixelPosition(r.getCenterX() + drawableArea.x, r.getCenterY() + drawableArea.y);
		}
	}

	//<editor-fold desc="Update and Draw">
		public final void update( int delta ) {
			Participant player;

			switch( state ) {
				case GAMESTATE_PLAYER_MOVING:
					player = getPlayerParticipant();
					TimeManager.updatePlayer( delta, player, DELTA_DIVISION_ON_MOVE );

					cam.setCameraFocus(player.getPosition());

					if( player.isDoingActivity() )
						//state = GAMESTATE_PLAYER_ON_MINIGAME;
						state = GAMESTATE_PLAYER_ACTING;
					else if( player.isDone() ) {
						state = GAMESTATE_NONPLAYER_MOVING;
					}
				break;
					
				case GAMESTATE_PLAYER_ON_MINIGAME:
					player = getPlayerParticipant();
					
					if( player.isDoingActivity() )
						//state = GAMESTATE_PLAYER_ON_MINIGAME;
						state = GAMESTATE_PLAYER_ACTING;
					else if( player.isDone() ) {
						state = GAMESTATE_NONPLAYER_MOVING;
					}
				break;

				case GAMESTATE_PLAYER_ACTING:
					player = getPlayerParticipant();

					TimeManager.updatePlayer( delta, player, DELTA_DIVISION_ON_ACTIVITY );
					if( player.isDone() ) {
						state = GAMESTATE_NONPLAYER_MOVING;
						assignActivities( player.getActivityTime() );
					}
				break;

				case GAMESTATE_NONPLAYER_MOVING:
					TimeManager.updateParticipants(delta, participants, DELTA_DIVISION_ON_MOVE);
					if( allNPCsAreActing() )
						state = GAMESTATE_NONPLAYER_ACTING;
					else if( allNPCsAreDone() )
						state = GAMESTATE_WAITING_INPUT;
					break;

				case GAMESTATE_NONPLAYER_ACTING:
					TimeManager.updateParticipants(delta, participants, DELTA_DIVISION_ON_ACTIVITY);
					if( allNPCsAreDone() )
						state = GAMESTATE_WAITING_INPUT;
					break;
			}

			// TODO escolhas automaticas
			if( AUTOMATIC && state == GAMESTATE_WAITING_INPUT ) {
				try {
					// TESTE: doActivity com atividade aleatoria
					state = GAMESTATE_PLAYER_MOVING;
					Room randomRoom = rooms[NanoMath.randInt( rooms.length )];
					Activity[] activities = randomRoom.getAvailableActivities( true );
					Activity act = activities[ NanoMath.randInt( activities.length ) ];

					try {
						randomRoom.placeInFreeSpot( getPlayerParticipant() );
						// atribui acao ao jogador e reordena
						getPlayerParticipant().assignActivity( act, act.getActivityTime() );
						getPlayerParticipant().sortByY();

					} catch (Exception e) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}

					unselectRoom();
				} catch( Throwable t ) {
					//#if DEBUG == "true"
						t.printStackTrace();
					//#endif
				}
			}
		}

		protected final void paint(Graphics g) {
			final Point pos = new Point( translate );
			pos.addEquals( drawableArea.x, drawableArea.y );
			
			final int initialI = NanoMath.max( (-getPosX() / TILE_SIZE) - 1, 0 );
			final int initialJ = NanoMath.max( (-getPosY() / TILE_SIZE) - 1, 0 );
			final int endI = NanoMath.min( initialI + ( ( getWidth() / TILE_SIZE ) + 2 ), tileMap.length );

			int i = initialI;
			for ( ; i < endI; i++) {

				final int endJ = NanoMath.min( initialJ + ( ( getHeight() / TILE_SIZE ) + 2 ), tileMap[i].length );

				for ( int j = initialJ; j < endJ; j++) {
					drawableElementArea.set( pos.x + (i * TILE_SIZE), pos.y + (j * TILE_SIZE), TILE_SIZE, TILE_SIZE );

					if ( drawableElementArea.intersects( drawableArea ) ) {
						intersection.setIntersection( drawableArea, drawableElementArea );
						g.setClip( intersection.x, intersection.y, intersection.width, intersection.height );
						g.drawImage(tilesImg, drawableElementArea.x - getOffsetX(tileMap[i][j]), drawableElementArea.y - getOffsetY(tileMap[i][j]), 0);
						if( objectsMap[i][j] >= 0  ) {
							g.drawImage(objectsImg, drawableElementArea.x - getOffsetX( (byte)(objectsMap[i][j]) ), drawableElementArea.y - getOffsetY( (byte)(objectsMap[i][j]) ), 0);
						}
					}
				}
			}
			g.setClip(drawableArea.x, drawableArea.y, drawableArea.width, drawableArea.height);

//			//#if DEBUG == "true"
//				for (i = 0; i < rooms.length; i++) {
//					final int firstX = pos.x + (rooms[i].getFirstTileX() * TILE_SIZE);
//					final int firstY = pos.y + (rooms[i].getFirstTileY() * TILE_SIZE);
//					final int lastX = pos.x + (rooms[i].getLastTileX() * TILE_SIZE);
//					final int lastY = pos.y + (rooms[i].getLastTileY() * TILE_SIZE);
//					if (lastX + TILE_SIZE > 0 && lastY + TILE_SIZE > 0 && firstX - TILE_SIZE < ScreenManager.SCREEN_WIDTH && firstY - TILE_SIZE < ScreenManager.SCREEN_HEIGHT) {
//						if (i == selectedRoom) {
//							g.setColor(rooms[i].getColor());
//							g.drawRect(firstX, firstY, (lastX - firstX) + TILE_SIZE, (lastY - firstY) + TILE_SIZE);
//						}
//					}
//				}
//			//#endif

			// desenhamos todos os participantes, ordenados pelo Y
			for (i = 0; i < participants.length; i++) {
				Participant p = Participant.ySortedParticipants[ i ];
				p.draw(g, pos.x, pos.y, drawableArea );
			}

			g.setClip(drawableArea.x, drawableArea.y, drawableArea.width, drawableArea.height);

			if (hasSelection()) {
				roomTitleLabel.draw( g );
			}
			// TODO: encapsular no HUD?
		}
	//</editor-fold> // end of Update and Draw region

	public final void hideNotify(boolean deviceEvent) {
	}

	public final void showNotify(boolean deviceEvent) {
	}

	public final void sizeChanged(int width, int height) {
		//super.setSize( width, height );
		updateRoomTitleLabel();
	}

	/**
	 * Verifica se todos os participantes que não pertencem
	 * ao jogador estão realizando alguma atividade.
	 */
	private final boolean allNPCsAreActing() {
		for (int i = 0; i < participants.length; i++) {
			if( participants[ i ] != playerParticipant && !participants[i].isDoingActivity() )
				return false;
		}
		return true;
	}

	/**
	 * Verifica se todos os participantes que não pertencem
	 * ao jogador terminaram suas atuvidades.
	 */
	private final boolean allNPCsAreDone() {
		for (int i = 0; i < participants.length; i++) {
			if( participants[ i ] != playerParticipant && !participants[i].isDone() )
				return false;
		}
		return true;
	}

	/**
	 * Finaliza atividade do participante do jogador e executa
	 * "de imediato" acao dos outros participantes (para que eles não fiquem
	 * defazados com relacao ao bonus de atributos)
	 */
	public static final void abortActivity() {
		int executedTime = getPlayerParticipant().getExecutedTime();
		getPlayerParticipant().assignActivity( null, 0 );

		if( state != GAMESTATE_PLAYER_MOVING ) {
			for (int i = 0; i < participants.length; i++) {
				if( participants[ i ] != null && participants[ i ] != getPlayerParticipant() ) {
					participants[ i ].fastExecuteLastActivity( executedTime );
				}
			}
		}
	}

	/**
	 * Escolhe atividades para os participantes não jogadores,
	 * baseados em suas preferências de comodo.
	 * @param activityTime Tempo que o participante do jogador consumiu
	 * em sua atividade.
	 */
	private final void assignActivities( int activityTime ) {

		// determinamos para quais comodos vao os participantes...
		for(int i = 0; i < participants.length; i++) {
			if ( participants[ i ] == playerParticipant) continue;
			
			try {
				Room desiredRoom;
				if( TimeManager.isPartyOn() ) {
					// durante as festas, os participantes ignoram
					// suas preferencias e optam pela piscina ou pela sala
					desiredRoom = rooms[ NanoMath.randFixed(2) == 1? ROOM_POOL: ROOM_LIVINGROOM];
				} else {
					// pega comodo desejado, e caso não se tenha preferencia,
					// no momento, escolhe-se um aleatorio
					desiredRoom = participants[i].getDesiredRoom();
					if( desiredRoom == null)
						desiredRoom = rooms[ NanoMath.randFixed( NUMBER_OF_ROOMS ) ];
				}
				
				desiredRoom.placeInFreeSpot( participants[i] );
				participants[ i ].sortByY();
				
			} catch (Exception e) {
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}
		}

		 // ...e depois escolhemos suas atividades. E importante que isso seja feito depois
		 // para que nao tentem socializar num comodo vazio:
		for( int i = 0; i < participants.length; i++) {
			if ( participants[ i ] == playerParticipant) continue;

			Room desiredRoom = participants[ i ].getRoom();

			// escolhe-se atividade aleatoria do comodo desejado
			Activity[] roomActivities = desiredRoom.getAvailableActivities( false );
			Activity activity = roomActivities[ NanoMath.randFixed( roomActivities.length ) ];

			// executa atividade
			participants[ i ].assignActivity( activity, activityTime );

			// TESTANDO IA
			//#if DEBUG == "true"
				/*System.out.println(
						(i == PARTICIPANT_SAINTY? "Santinha":
						i == PARTICIPANT_NERD? "Nerd":
						i == PARTICIPANT_HOUSEWIFE? "Dona de casa":
						i == PARTICIPANT_HOTGIRL? "Gostosa":
						i == PARTICIPANT_GYMDUDE? "Bombado":
						i == PARTICIPANT_PRETTY? "Bonita": "Normal") +
						" vai para " + desiredRoom.getName() +
						" " + activity.getActionName() );*/
			//#endif
		}
	}

	/**
	 * Atualiza o estado do tabuleiro para que ele esteja esperando
	 * pela interação do jogador.
	 */
	public static final void waitForInput() {
		getPlayerParticipant().assignActivity( null, 0 );
		state = GAMESTATE_WAITING_INPUT;
	}

	//<editor-fold desc="Handle Input">
		private final boolean isInRangeOfAngle(byte angle, int dx, int dy) {
			final int hip = NanoMath.sqrtInt(dx * dx + dy * dy);
			switch (angle) {
				case DIRECTION_ANGLE_0:
					if (dx > 0) { //cosInt( 23 ); range > 1/4
						return NanoMath.divInt(dx, hip) > NanoMath.divInt(70, 100);
					}
					break;

				case DIRECTION_ANGLE_45:
					if (dx > 0 && dy < 0) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
						final int cos = NanoMath.divInt(dy, hip);
						return cos < NanoMath.divInt(-38, 100) && cos > NanoMath.divInt(-92, 100);
					}
					break;

				case DIRECTION_ANGLE_90:
					if (dy < 0) { //.sinInt( 67 ); range > 1/4
						return NanoMath.divInt(dy, hip) < NanoMath.divInt(-70, 100);
					}
					break;

				case DIRECTION_ANGLE_135:
					if (dx < 0 && dy < 0) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
						final int cos = NanoMath.divInt(dy, hip);
						return cos < NanoMath.divInt(-38, 100) && cos > NanoMath.divInt(-92, 100);
					}
					break;

				case DIRECTION_ANGLE_180:
					if (dx < 0) { //NanoMath.cosInt( 157 ); range > 1/4
						return NanoMath.divInt(dx, hip) < NanoMath.divInt(-70, 100);
					}
					break;

				case DIRECTION_ANGLE_225:
					if (dx < 0 && dy > 0) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
						final int cos = NanoMath.divInt(dy, hip);
						return cos > NanoMath.divInt(38, 100) && cos < NanoMath.divInt(92, 100);
					}
					break;

				case DIRECTION_ANGLE_270:
					if (dy > 0) { //NanoMath.sinInt( 247 ); range > 1/4
						return NanoMath.divInt(dy, hip) > NanoMath.divInt(70, 100);
					}
					break;

				case DIRECTION_ANGLE_315:
					if (dx > 0 && dy > 0) { //.cosInt(23) > cos > .cosInt(67); range > 1/8
						final int cos = NanoMath.divInt(dy, hip);
						return cos > NanoMath.divInt(38, 100) && cos < NanoMath.divInt(92, 100);
					}
					break;
			}

			return false;
		}

		private final boolean moveCursorToDirection( byte angle, Camera cam ) {
			int bestDistance = Integer.MAX_VALUE;
			int best = -1;

			Room selected = getCurrentRoom();
			if (selected != null) {
				for (int i = 0; i < rooms.length; i++) {
					if ((rooms[i] != null) && (selected != rooms[i])) {
						final int dx = rooms[i].getCenterX() - selected.getCenterX();
						final int dy = rooms[i].getCenterY() - selected.getCenterY();
						if (isInRangeOfAngle(angle, dx, dy)) {
							final int dist = selected.getCenter().distanceTo(rooms[i].getCenter());
							if (dist < bestDistance) {
								bestDistance = dist;
								best = i;
							}
						}
					}
				}
				if (best != -1) {
					selectRoom(best);
				}
			}

			return false;
		}

		private final boolean selectBest(Camera cam) {
			int bestDistance = Integer.MAX_VALUE;
			int best = -1;

			for (int i = 0; i < rooms.length; i++) {
				if (rooms[i] != null) {
					final int dist = rooms[i].getCenter().distanceTo(cam.getFocusPoint());
					if (dist < bestDistance) {
						bestDistance = dist;
						best = i;
					}
				}
			}
			if (best != -1) {
				selectRoom(best);
			}

			return false;
		}

		public final void keyReleased(int key) {
			if (canHandleInput()) {
				if (hasSelection()) {
					switch (key) {
						case ScreenManager.KEY_SOFT_LEFT:
						case ScreenManager.KEY_SOFT_RIGHT:
						case ScreenManager.KEY_CLEAR:
						case ScreenManager.KEY_BACK:
							unselectRoom();
						break;

						case ScreenManager.KEY_NUM5:
						case ScreenManager.FIRE:
							openDialog();
						break;

						case ScreenManager.KEY_NUM6:
						case ScreenManager.KEY_RIGHT:
							moveCursorToDirection(DIRECTION_ANGLE_0, cam);
						break;

						case ScreenManager.KEY_NUM2:
						case ScreenManager.KEY_UP:
							moveCursorToDirection(DIRECTION_ANGLE_90, cam);
						break;

						case ScreenManager.KEY_NUM4:
						case ScreenManager.KEY_LEFT:
							moveCursorToDirection(DIRECTION_ANGLE_180, cam);
						break;

						case ScreenManager.KEY_NUM8:
						case ScreenManager.KEY_DOWN:
							moveCursorToDirection(DIRECTION_ANGLE_270, cam);
						break;
					}
				} else {
					switch (key) {
						case ScreenManager.KEY_NUM5:
						case ScreenManager.FIRE:
							selectBest(cam);
						break;
					}
				}
			}
		}

		public final boolean canHandleInput() {
			return state == GAMESTATE_WAITING_INPUT;
		}

		private final boolean roomContainsPoint(Room r, Point p) {
			final int firstX = (r.getFirstTileX() * TILE_SIZE);
			final int firstY = (r.getFirstTileY() * TILE_SIZE);
			final int lastX = (r.getLastTileX() * TILE_SIZE);
			final int lastY = (r.getLastTileY() * TILE_SIZE);
			return (lastX + TILE_SIZE > p.x && lastY + TILE_SIZE > p.y && firstX < p.x && firstY < p.y);
		}

		//#if TOUCH == "true"
			public final void HandleTap(Point p) {
				//p.subEquals( offset );
				if (canHandleInput()) {
					for (int i = 0; i < rooms.length; i++) {
						if (roomContainsPoint(rooms[i], p.sub( drawableArea.x, drawableArea.y ) )) {
							selectRoom(i);
							return;
						}
					}
					unselectRoom();
				}
			}

			public final void GestureIsNotSpot(Point p) {
				if (canHandleInput()) {
					unselectRoom();
				}
			}
	//</editor-fold> // end of Handle Input region
		//#endif
}
