package Original;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;
//==============================================================================
public class GameMIDlet extends AppMIDlet implements GameConstants, Serializable
{
	//#if DEBUG == "true"

	private static StringBuffer log;
	//#endif
	private static boolean lowMemory;
//------------------------------------------------------------------------------

	/**
	 * 
	 */
	public GameMIDlet()
	{
		super(-1, GAME_MAX_FRAME_TIME);
		//#if DEBUG == "true"
		log = new StringBuffer();
		//#endif
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param specialMapping
	 */
	public static final void setSpecialKeyMapping(boolean specialMapping)
	{
		try
		{
			ScreenManager.resetSpecialKeysTable();
			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

			int[][] keys = null;
			final int offset = 'a' - 'A';

			//#if BLACKBERRY_API == "true"
			//# 			switch ( Keypad.getHardwareLayout() ) {
			//# 				case ScreenManager.HW_LAYOUT_REDUCED:
			//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
			//# 					keys = new int[][] {
			//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
			//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
			//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
			//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
			//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
			//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
			//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
			//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
			//#
			//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
			//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
			//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
			//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
			//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
			//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
			//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
			//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
			//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
			//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
			//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
			//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
			//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
			//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
			//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
			//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
			//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
			//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
			//#
			//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
			//# 					 };
			//# 				break;
			//#
			//# 				default:
			//# 					if ( specialMapping ) {
			//# 						keys = new int[][] {
			//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
			//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
			//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
			//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
			//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
			//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
			//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
			//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
			//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
			//#
			//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
			//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
			//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
			//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
			//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
			//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
			//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
			//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
			//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
			//#
			//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
			//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
			//# 						 };
			//# 					} else {
			//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
			//# 							table.put( new Integer( c ), new Integer( c ) );
			//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
			//# 						}
			//#
			//# 						final int[] chars = new int[]
			//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
			//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
			//# 						};
			//#
			//# 						for ( byte i = 0; i < chars.length; ++i )
			//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			//# 					}
			//# 			}
			//#
			//#else

			if (specialMapping)
			{
				keys = new int[][]
						{
							{
								'q', ScreenManager.KEY_NUM1
							},
							{
								'Q', ScreenManager.KEY_NUM1
							},
							{
								'e', ScreenManager.KEY_NUM3
							},
							{
								'E', ScreenManager.KEY_NUM3
							},
							{
								'z', ScreenManager.KEY_NUM7
							},
							{
								'Z', ScreenManager.KEY_NUM7
							},
							{
								'c', ScreenManager.KEY_NUM9
							},
							{
								'C', ScreenManager.KEY_NUM9
							},
							{
								'w', ScreenManager.UP
							},
							{
								'W', ScreenManager.UP
							},
							{
								'a', ScreenManager.LEFT
							},
							{
								'A', ScreenManager.LEFT
							},
							{
								's', ScreenManager.FIRE
							},
							{
								'S', ScreenManager.FIRE
							},
							{
								'd', ScreenManager.RIGHT
							},
							{
								'D', ScreenManager.RIGHT
							},
							{
								'x', ScreenManager.DOWN
							},
							{
								'X', ScreenManager.DOWN
							},
							{
								'r', ScreenManager.KEY_NUM1
							},
							{
								'R', ScreenManager.KEY_NUM1
							},
							{
								'y', ScreenManager.KEY_NUM3
							},
							{
								'Y', ScreenManager.KEY_NUM3
							},
							{
								'v', ScreenManager.KEY_NUM7
							},
							{
								'V', ScreenManager.KEY_NUM7
							},
							{
								'n', ScreenManager.KEY_NUM9
							},
							{
								'N', ScreenManager.KEY_NUM9
							},
							{
								't', ScreenManager.KEY_NUM2
							},
							{
								'T', ScreenManager.KEY_NUM2
							},
							{
								'f', ScreenManager.KEY_NUM4
							},
							{
								'F', ScreenManager.KEY_NUM4
							},
							{
								'g', ScreenManager.KEY_NUM5
							},
							{
								'G', ScreenManager.KEY_NUM5
							},
							{
								'h', ScreenManager.KEY_NUM6
							},
							{
								'H', ScreenManager.KEY_NUM6
							},
							{
								'b', ScreenManager.KEY_NUM8
							},
							{
								'B', ScreenManager.KEY_NUM8
							},
							{
								10, ScreenManager.FIRE
							}, // ENTER

							{
								8, ScreenManager.KEY_CLEAR
							}, // BACKSPACE (Nokia E61)

							{
								'u', ScreenManager.KEY_STAR
							},
							{
								'U', ScreenManager.KEY_STAR
							},
							{
								'j', ScreenManager.KEY_STAR
							},
							{
								'J', ScreenManager.KEY_STAR
							},
							{
								'#', ScreenManager.KEY_STAR
							},
							{
								'*', ScreenManager.KEY_STAR
							},
							{
								'm', ScreenManager.KEY_STAR
							},
							{
								'M', ScreenManager.KEY_STAR
							},
							{
								'p', ScreenManager.KEY_STAR
							},
							{
								'P', ScreenManager.KEY_STAR
							},
							{
								' ', ScreenManager.KEY_STAR
							},
							{
								'$', ScreenManager.KEY_STAR
							},
						};
			} else
			{
				for (char c = 'A'; c <= 'Z'; ++c)
				{
					table.put(new Integer(c), new Integer(c));
					table.put(new Integer(c + offset), new Integer(c + offset));
				}

				final int[] chars = new int[]
				{
					' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for (byte i = 0; i < chars.length; ++i)
				{
					table.put(new Integer(chars[i]), new Integer(chars[i]));
				}
			}
			//#endif

			if (keys != null)
			{
				for (byte i = 0; i < keys.length; ++i)
				{
					table.put(new Integer(keys[i][ 0]), new Integer(keys[i][ 1]));
				}
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			log(e.getClass() + e.getMessage());
			e.printStackTrace();
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param screen
	 * @return
	 * @throws Exception
	 */
	protected int changeScreen(int screen) throws Exception
	{
		final GameMIDlet midlet = (GameMIDlet) instance;

		Drawable nextScreen = null;
		log("changeScreen " + screen);

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

//		bkg.setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

//		byte bkgType = BACKGROUND_TYPE_LED_PATTERN;

		byte indexSoftRight = SOFT_KEY_DONT_CHANGE;
		byte indexSoftLeft = SOFT_KEY_DONT_CHANGE;

		switch (screen)
		{
			case SCREEN_SPLASH_ENDEMOL:
			{
				nextScreen = new SplashEndemol();
			}
			break;

			case SCREEN_FEAR_FACTOR:
			{
				nextScreen = new FearFactorScreen();
			}
			break;
		}


		if (indexSoftLeft != SOFT_KEY_DONT_CHANGE)
		{
			setSoftKeyLabel(ScreenManager.SOFT_KEY_LEFT, indexSoftLeft);
		}

		if (indexSoftRight != SOFT_KEY_DONT_CHANGE)
		{
			setSoftKeyLabel(ScreenManager.SOFT_KEY_RIGHT, indexSoftRight);
		}

//				setBackground(bkgType);

		//#if DEBUG == "true"
		if (nextScreen == null)
		{
			throw new IllegalStateException("Warning: nextScreen null (" + screen + ").");
		}
		//#endif

		midlet.manager.setCurrentScreen(nextScreen);
		System.gc();
		return screen;
	}
//------------------------------------------------------------------------------

	/**
	 * Adiciona uma entrada no log. Não é necessário remover chamadas a esse método em versões de release, pois elas
	 * são descartadas pelo obfuscator.
	 * @param s
	 */
	public static final void log(String s)
	{
		//#if DEBUG == "true"
		System.gc();

		log.append(s);
		log.append(": ");
		final long freeMem = Runtime.getRuntime().freeMemory();
		log.append(freeMem);
		log.append(':');
		log.append(freeMem * 100 / Runtime.getRuntime().totalMemory());
		log.append("%\n");

		System.out.println(s + ": " + freeMem + ": " + (freeMem * 100 / Runtime.getRuntime().totalMemory()) + "%");
		//#endif
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @throws Exception
	 */
	protected final void loadResources() throws Exception
	{
		log("loadResources início");
		
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			Vector v = new Vector();
//# 			final int MEMORY_INCREASE = 170 * 1000; // size: 170kb
//# 			int total = 0;
//# 		    try {
//# 				while( Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT ) {
//# 					v.addElement(new byte[ MEMORY_INCREASE ]);
//# 					total += MEMORY_INCREASE;
//# 				}
//# 				lowMemory = false;
//# 			} catch( Throwable e ) {
//# 				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT && total < LOW_MEMORY_LIMIT;
//# 			} finally {
//# 				v = null;
//# 				System.gc();
//# 			}
//# 			//lowMemory = true; // TODO teste
		//#endif

		FONTS = new ImageFont[FONT_TYPES_TOTAL];

		for (byte i = 0; i < FONT_TYPES_TOTAL; ++i)
		{
			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//#  				switch ( i ) {
			//#if SCREEN_SIZE == "MEDIUM"
//#  						case FONT_NUMBER_BLACK:
//#  						case FONT_TITLE:
//#  							FONTS[ i ] = FONTS[ FONT_TEXT ];
//#  						break;
			//#endif
//#
			//#if SCREEN_SIZE == "SMALL"
//# 						case FONT_CASE_SMALL:
//# 						case FONT_TITLE:
			//#endif
//#  					case FONT_TEXT_WHITE:
//#  					case FONT_MILLION:
//#  					break;
//#
//#  					default:
//#  						FONTS[ i ] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);
//#  				}
			//#else
			if (i != FONT_TEXT_WHITE)
			{
				FONTS[i] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);
			}
			//#endif

			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
			//#if SCREEN_SIZE == "SMALL"
//# 					FONTS[ FONT_CASE_SMALL ] = FONTS[ FONT_TEXT ];
			//#endif
//# 				FONTS[ FONT_TITLE ] = FONTS[ FONT_TEXT ];
			//#endif

			switch (i)
			{
				case FONT_TITLE:
				//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 					break;
				//#endif
				case FONT_TEXT:
				case FONT_NUMBER_BLACK:
					FONTS[i].setCharExtraOffset(1);
					break;

				//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 				case FONT_MILLION:
				//#endif
				case FONT_TEXT_WHITE:
					break;

				default:
					FONTS[i].setCharExtraOffset(DEFAULT_FONT_OFFSET);
			}
			log("FONTE " + i);
		}

		log("database 0");
		//#if NANO_RANKING == "true"
//# 		NanoOnline.setRankingTypes(NanoOnline.RANKING_TYPES_GLOBAL_ONLY);
//# 		final int[] entries = new int[]
//# 		{
//# 			TEXT_RANKING_TOTAL_MONEY, TEXT_RANKING_MONEY_AVERAGE, TEXT_RANKING_MILLION, TEXT_RANKING_50_CENTS
//# 		};
//# 		RankingScreen.init(entries, this);
		//#endif

		log("database 1");
		// cria a base de dados do jogo
		try
		{
			createDatabase(DATABASE_NAME, DATABASE_TOTAL_SLOTS);

		} catch (Exception e)
		{
		}

		try
		{
			loadData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
		} catch (Exception e)
		{
			setLanguage((byte) 2); // NanoOnline.LANGUAGE_pt_BR
		}

		log("database 2");


		log("textos");

		setSpecialKeyMapping(true);
		log("loadResources fim");

		final String[] soundList = new String[SOUND_TOTAL];

		for (byte i = 0; i < SOUND_TOTAL; ++i)
		{
			soundList[i] = PATH_SOUNDS + i + ".mid";
		}

		try
		{
			MediaPlayer.init(DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList);
		}
		catch (Exception ex)
		{
//#if DEBUG == "true"
			System.out.println("algo deu errado na inicialização do MediaPlayers");
			ex.printStackTrace();
//#endif
		}

		final String prefix = PATH_IMAGES + "font_" + FONT_TEXT;
		final PaletteMap[] map = new PaletteMap[]
				{
				new PaletteMap(0x060a23, 0xffffff),
				new PaletteMap(0x000000, 0xffffff)
				};
		final PaletteChanger p = new PaletteChanger(prefix + ".png");
		final ImageFont font = ImageFont.createMultiSpacedFont(p.createImage(map), prefix + ".bin");
		font.setCharExtraOffset(getFont(FONT_TEXT).getCharExtraOffset());
		FONTS[FONT_WHITE_MEDIUM] = font;
		
		setScreen(SCREEN_SPLASH_ENDEMOL);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param language
	 */
	protected final void changeLanguage(byte language)
	{
		try
		{
			loadTexts(TEXT_TOTAL, PATH_IMAGES + "texts_" + language + ".dat");
		} catch (Exception ex)
		{
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	public final void destroy()
	{
		MediaPlayer.saveOptions();
		super.destroy();

		///TODO: decidir depois
//		if (gameScreen != null)
//		{
//			onGameOver(gameCanvas.getScore(), false);
//			gameScreen = null;
//		}

		notifyDestroyed();
	}
//------------------------------------------------------------------------------
//	private void onGameOver(int score, boolean b)
//	{
//
//	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @return
	 */
	public static final boolean isLowMemory()
	{
		return lowMemory;
	}
//------------------------------------------------------------------------------

	/**
	 * Define uma soft key a partir de um texto. Equivalente Ã  chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 *
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel(byte softKey, int textIndex)
	{
		setSoftKeyLabel(softKey, textIndex, 0);
	}
//------------------------------------------------------------------------------

	/**
	 * Define uma soft key a partir de um texto.
	 *
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visÃ­vel. Para o label estar sempre visÃ­vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel(byte softKey, int textIndex, int visibleTime)
	{
//		if ( textIndex < 0 ) {
		setSoftKey(softKey, null, true, 0);
//		} else {
//			try {
//				setSoftKey( softKey, new Label( getFont( FONT_INDEX_TEXT ), getText( textIndex ) ), true, visibleTime );
//			} catch ( Exception e ) {
//				//#if DEBUG == "true"
//					e.printStackTrace();
//				//#endif
//			}
//		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param softKey
	 * @param d
	 * @param changeNow
	 * @param visibleTime
	 */
	public static final void setSoftKey(byte softKey, Drawable d, boolean changeNow, int visibleTime)
	{
		ScreenManager.getInstance().setSoftKey(softKey, d);
	} // fim do método setSoftKey( byte, Drawable, boolean, int )
//------------------------------------------------------------------------------

	/**
	 *
	 * @return
	 */
	private static final String getVersion()
	{
		String version = instance.getAppProperty("MIDlet-Version");
		if (version == null)
		{
			version = "";
		}

		return "<ALN_H>" + getText(TEXT_VERSION) + version + "\n\n";
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param output
	 * @throws Exception
	 */
	public final void write(DataOutputStream output) throws Exception
	{
		output.writeByte(language);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param input
	 * @throws Exception
	 */
	public final void read(DataInputStream input) throws Exception
	{
		setLanguage(input.readByte());
	}
//------------------------------------------------------------------------------
}
//==============================================================================