package Original;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author monteiro
 */
public interface GameConstants
{    
    public static final int TITLE_SCREEN = 1;
    public static final int MAIN_MENU_SCREEN = 2;
    public static final int HELP_SCREEN = 3;
    public static final int ABOUT_SCREEN = 4;
    public static final int PAUSE_SCREEN = 5;
    public static final int PRE_GAME_INFO_SCREEN = 6;
    public static final int CURRENT_GAME_QUIT_CONFIRM_SCREEN = 7;
//	public static final int LEVEL_STATISTICS_SCREEN					= 8;
    public static final int SCORE_SUBMISSION_CONFIRM_SCREEN = 9;
    public static final int GAME_EXIT_CONFIRM_SCREEN = 10;
    public static final int INGAME_SCREEN = 11;
    public static final int INGAME_SUBMENU = 12;
    public static final int INGAME_ONE = 13;
    public static final int INGAME_TWO = 14;
    public static final int INGAME_THREE = 15;
    public static final int INGAME_FOUR = 16;
    public static final int INGAME_FIVE = 17;
    public static final int GAME_OVER = 18;
    public static final int HIGHSCORE_SCREEN = 19;
    public static final int CREDIT_SCREEN = 20;
    public static final int SOUNDENABLE_SCREEN = 21;
    // Text Array indices for Text Pages !! Check out ArraysToFile.java file !!
    public static final byte MAIN_MENU_TEXT = 0;
    public static final byte PAUSE_MENU_TEXT = 1;
    public static final byte PRE_GAME_INFO_TEXT = 2;
    public static final byte CURRENT_GAME_QUIT_TEXT = 3;
    public static final byte LEVEL_STATISTICS_TEXT = 4;
    public static final byte SCORE_SUBMIT_CONFIRM_TEXT = 5;
    public static final byte GAME_EXIT_TEXT = 6;
    public static final byte HELP_TEXT = 7;
    public static final byte ABOUT_TEXT = 8;
    public static final byte SUB_MAIN_MENU_TEXT = 9;
	// count the columns in your font image and set that value here !!
    public  final int CHAR_COLUMNS = 13;	
    public  final int KEY_EVENT_DELAY_FACTOR = 8;
    public static  final int LINE_GAP = 12;
    public static  final int TOTAL_LINES = 7;
    public static  final int HEADER_START_Y = 8;
    public static  final int TEXT_START_Y = 22;
    public static  final int ARROW_WIDTH = 7;
    public static  final int ARROW_HEIGHT = 7;
    public static  final int COMMAND_HEIGHT = 10;
    public static  final int SELECT = 1;
    public static  final int BACK = 2;
    public static  final int OK = 3;
    public static  final int YES = 4;
    public static  final int NO = 5;
    public static  final int PAUSE = 6;
    public static  final int VOTE = 7;
    public static  final int SUBMIT = 8;
    public static  final int TITLE_IMAGES = 6;
    public static  final int TITLENAMEANIMATIONFRAMES = 5;
    public static  final int CHAR_WIDTH = 0;
    public static  final int CHAR_HEIGHT = 1;
    // font images !! LARGE :- (7 x 7), 	MEDIUM:- (6 x 6), SMALL :- (4 x 6) !!
    public static  final int TOTAL_FONTS = 1;
    public static  final int FONT_BLACK_MEDIUM = 1;
	public static  final int FONT_GREEN_DIGITAL = 2;
    public static  final int FONT_WHITE_MEDIUM = 3;
    public static  final byte KEY_UP = 2;
    public  final byte KEY_DOWN = 3;
    public static  final byte KEY_LEFT = 4;
    public static  final byte KEY_RIGHT = 5;
    public static  final byte KEY_FIRE = 6;
    public static  final byte KEY_HASH = 9;
    public static  final byte KEY_STAR_ = 10;
    public static  final byte NUM_0 = 11;
    public static  final byte NUM_1 = 12;
    public static  final byte NUM_3 = 13;
    public static  final byte NUM_7 = 14;
    public static  final byte NUM_9 = 15;


    public static  final int FISH_ONE_X = 15;
    public static  final int FISH_ONE_W = 15;
    public static  final int FISH_ONE_H = 13;
    public static  final int FISH_TWO_X = 15;
    public static  final int FISH_TWO_W = 15;
    public static  final int FISH_TWO_H = 13;

//#if CONFIG == "BIG"
    public static  final int FISH_ONE_Y = 126;
    public static  final int FISH_TWO_Y = 150;
	public static  final int MAN_W =85;
	public static  final int MAN_H =102;
//#endif

//#if CONFIG == "MEDIUM"
//#     public static  final int FISH_ONE_Y = 100;
//#     public static  final int FISH_TWO_Y = 70;
//# 	public static  final int MAN_W =63;
//# 	public static  final int MAN_H =75;
//#endif

//#if CONFIG == "SMALL"
//#     public static  final int FISH_ONE_Y = 50;
//#     public static  final int FISH_TWO_Y = 70;
//#     public static  final int MAN_W =41;
//#     public static  final int MAN_H =45;
//#endif

//#if CONFIG == "BIG"

//#if ORIENTATION == "LANDSCAPE"
//#     public static  final int SNAKE_X = 156;
//#     public static  final int SNAKE_Y = 125;
//# 	public static final  int MAXLINES = 12;
//#endif

//#if ORIENTATION != "LANDSCAPE"
   public static  final int SNAKE_X = 76 + 28;
   public static  final int SNAKE_Y = 127 + 44;
   public static final  int MAXLINES = 18;
//#endif


    public static final int TEXTSTART_Y = 25;
	public static final int TEXTSTART_X = 20;
	public static final  int MAXWIDTH = 270;
//#endif

//#if CONFIG == "MEDIUM"
//#     public static final int SNAKE_X=76;
//#     public static final int SNAKE_Y=127;
//# 	public static final int TEXTSTART_Y = 25;
//# 	public static final  int MAXLINES = 10;
//# 	public static final  int MAXWIDTH = 195;
//# 	public static final int TEXTSTART_X = 15;
//#endif

//#if CONFIG == "SMALL"
//#     public static final int SNAKE_X=60;
//#     public static final int SNAKE_Y=84;
//#     public static final int TEXTSTART_Y = 18;
//# 	public static final int MAXLINES = 5;
//# 	public static final int TEXTSTART_X = 12;
//# 	public static final int MAXWIDTH =  (128 - TEXTSTART_X);
//#endif

    public static  final int LINEGAP = 14;
    public static final  int CHARW = 7;
    public static final  int CHARH = 9;
    
    
    




    ///DM
    public static final int MILISECOND = 1;
    public static final int MILISECONDS = MILISECOND;
    public static final int SECOND = 1000 * MILISECONDS;
	public static final int MILISECONDS_IN_A_SECOND = 1000;
    public static final int SECONDS = SECOND;
    public static final int SPLASHTIME = 2 * SECONDS;

    public static final int TEXT_MENU_PLAY = 0;
    public static final int TEXT_MENU_HELP = TEXT_MENU_PLAY +1;
    public static final int TEXT_MENU_SCORE = TEXT_MENU_HELP + 1;
    public static final int TEXT_MENU_SOUND = TEXT_MENU_SCORE +1;
    public static final int TEXT_MENU_ABOUT = TEXT_MENU_SOUND + 1;
    public static final int TEXT_MENU_CREDITS = TEXT_MENU_ABOUT + 1;
    public static final int TEXT_MENU_EXIT = TEXT_MENU_CREDITS + 1;


    public static final int TEXT_MINIGAMES_DAGGERS = TEXT_MENU_EXIT + 1;
    public static final int TEXT_MINIGAMES_CRAWLER = TEXT_MINIGAMES_DAGGERS + 1;
    public static final int TEXT_MINIGAMES_BALL = TEXT_MINIGAMES_CRAWLER + 1;
    public static final int TEXT_MINIGAMES_AQUA = TEXT_MINIGAMES_BALL + 1;
    public static final int TEXT_MINIGAMES_ACRO = TEXT_MINIGAMES_AQUA + 1;

    public static final int TEXT_PAUSEMENU_RESUME = TEXT_MINIGAMES_ACRO + 1;
    public static final int TEXT_PAUSEMENU_SOUND = TEXT_PAUSEMENU_RESUME + 1;
    public static final int TEXT_PAUSEMENU_MAINMENU = TEXT_PAUSEMENU_SOUND + 1;

    public static final int TEXT_SOUND_ON = TEXT_PAUSEMENU_MAINMENU + 1;
    public static final int TEXT_SOUND_OFF = TEXT_SOUND_ON + 1;

    public static final int TEXT_SK_SELECT = TEXT_SOUND_OFF+1;
    public static final int TEXT_SK_BACK = TEXT_SK_SELECT+1;

    public static final int TEXT_SOUNDMENU_QUESTION = TEXT_SK_BACK+1;
    public static final int TEXT_SOUNDMENU_YES = TEXT_SOUNDMENU_QUESTION+1;
    public static final int TEXT_SOUNDMENU_NO = TEXT_SOUNDMENU_YES+1;

    public static final int TEXT_QUIT_QUESTION = TEXT_SOUNDMENU_NO+1;
    public static final int TEXT_QUIT_YES = TEXT_QUIT_QUESTION+1;
    public static final int TEXT_QUIT_NO = TEXT_QUIT_YES+1;

    public static final int TEXT_CMD_OK = TEXT_QUIT_NO+1;
    public static final int TEXT_CMD_BACK = TEXT_CMD_OK+1;
    public static final int TEXT_CMD_VOTE = TEXT_CMD_BACK+1;
    public static final int TEXT_CMD_SUBMIT = TEXT_CMD_VOTE+1;
    public static final int TEXT_CMD_SELECT = TEXT_CMD_SUBMIT+1;
    public static final int TEXT_CMD_YES = TEXT_CMD_SELECT+1;
    public static final int TEXT_CMD_NO = TEXT_CMD_YES+1;
    public static final int TEXT_CMD_PAUSE = TEXT_CMD_NO+1;

    public static final int TEXT_MSG_WINNER = TEXT_CMD_PAUSE+1;
    public static final int TEXT_MSG_WIN = TEXT_MSG_WINNER+1;
    public static final int TEXT_MSG_GAMEOVER = TEXT_MSG_WIN+1;
    public static final int TEXT_SCORE = TEXT_MSG_GAMEOVER+1;

	public static final int TEXT_GAMES_DESC = TEXT_SCORE + 1;
	public static final int TEXT_ENDEMOL_CREDITS = TEXT_GAMES_DESC + 1;
	public static final int TEXT_INDIAGAMES_CREDITS = TEXT_ENDEMOL_CREDITS + 1;
	public static final int TEXT_CONTROLS = TEXT_INDIAGAMES_CREDITS + 1;

	public static final int TEXT_RULES = TEXT_CONTROLS + 1;
	public static final int TEXT_WHAT = TEXT_RULES + 1;
	public static final int TEXT_CONTROLS_DESC = TEXT_WHAT + 1;
	public static final int TEXT_RULES_DESC = TEXT_CONTROLS_DESC + 1;
	public static final int TEXT_TURN_SOUND_ON = TEXT_RULES_DESC + 1;
	public static final int TEXT_TURN_SOUND_OFF = TEXT_TURN_SOUND_ON + 1;
	public static final int TEXT_TURN_VIBRATION_ON = TEXT_TURN_SOUND_OFF + 1;
	public static final int TEXT_TURN_VIBRATION_OFF = TEXT_TURN_VIBRATION_ON + 1;
	public static final int TEXT_CANCEL = TEXT_TURN_VIBRATION_OFF + 1;
	public static final int TEXT_CONTINUE = TEXT_CANCEL + 1;
	public static final int TEXT_SPLASH_NANO = TEXT_CONTINUE + 1;
	public static final int TEXT_SPLASH_ENDEMOL = TEXT_SPLASH_NANO + 1;
	public static final int TEXT_LOADING = TEXT_SPLASH_ENDEMOL + 1;
	public static final int TEXT_SOUND_OPTIONS = TEXT_LOADING + 1;
	public static final int TEXT_BACK_TO_MENU = TEXT_SOUND_OPTIONS + 1;
	public static final int TEXT_BACK_TO_MENU_QUESTION = TEXT_BACK_TO_MENU + 1;
	public static final int TEXT_QUIT = TEXT_BACK_TO_MENU_QUESTION + 1;
	public static final int TEXT_QUIT_QUESTION_MARK = TEXT_QUIT + 1;
	public static final int TEXT_PRESS_ANY_KEY = TEXT_QUIT_QUESTION_MARK + 1;
	public static final int TEXT_GAMEOVER = TEXT_PRESS_ANY_KEY + 1;
	public static final int TEXT_TELL_ABOUT_IT = TEXT_GAMEOVER + 1;

	public static final int TEXT_CHOOSE_LANG = TEXT_TELL_ABOUT_IT + 1;
	public static final int TEXT_LANG_ENG = TEXT_CHOOSE_LANG + 1;
	public static final int TEXT_LANG_PT = TEXT_LANG_ENG + 1;
	public static final int TEXT_PROFILE = TEXT_LANG_PT + 1;
	public static final int TEXT_CHANGE_PROFILE = TEXT_PROFILE + 1;
	public static final int TEXT_APPLY = TEXT_CHANGE_PROFILE + 1;
	public static final int TEXT_NONE = TEXT_APPLY + 1;
	public static final int TEXT_DATE_FORMAT = TEXT_NONE + 1;
	public static final int TEXT_HIGHSCORES = TEXT_DATE_FORMAT + 1;
	public static final int TEXT_VOLUME = TEXT_HIGHSCORES + 1;
	public static final int TEXT_CONNECTED = TEXT_VOLUME + 1;
	public static final int TEXT_NO_SAVED_NEWS = TEXT_CONNECTED + 1;
	public static final int TEXT_NEWS_HELP_TEXT = TEXT_NO_SAVED_NEWS + 1;
	public static final int TEXT_UPDATE = TEXT_NEWS_HELP_TEXT + 1;
	public static final int TEXT_NEWS = TEXT_UPDATE + 1;
	public static final int TEXT_VERSION = TEXT_NEWS + 1;
	//textos de ajuda
	public static final int TEXT_JUMPKEYS = TEXT_VERSION +1;
	public static final int TEXT_JUMPTOUCH = TEXT_JUMPKEYS +1;

	public static final int TEXT_SWALLOWKEYS = TEXT_JUMPTOUCH +1;
	public static final int TEXT_SWALLOWTOUCH = TEXT_SWALLOWKEYS +1;

	public static final int TEXT_BALLKEYS = TEXT_SWALLOWTOUCH +1;
	public static final int TEXT_BALLTOUCH = TEXT_BALLKEYS +1;

	public static final int TEXT_LOCKKEYS = TEXT_BALLTOUCH +1;
	public static final int TEXT_LOCKTOUCH = TEXT_LOCKKEYS +1;


	public static final int TEXT_BALANCEKEYS = TEXT_LOCKTOUCH +1;
	public static final int TEXT_BALANCETOUCH = TEXT_BALANCEKEYS +1;

	public static final int TEXT_TOTAL = TEXT_BALANCETOUCH +1;

	
	public static final short GAME_MAX_FRAME_TIME = 180;
	public static final int DEFAULT_TEXT_HORIZONTAL_SPACING = 20;
		/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 4; //tmp

		/**Índice da fonte utilizada para textos. */
	public static final byte FONT_TITLE			= 0;
	public static final byte FONT_TEXT			= 1;
	public static final byte FONT_NUMBER_BLACK	= 2;
	public static final byte FONT_TEXT_WHITE	= 3;

	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	/** Offset da fonte padrão. */
	public static final byte DEFAULT_FONT_OFFSET = -1;
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";

	/**Índice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;

	public static final byte DATABASE_SLOT_LANGUAGE = 2;

	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 2;

	public static final int SOUND_TOTAL = 1;
	///era pra ser 1, mas esta consumindo muita memória...E no fim, mal faz
	///diferença
	public static final int SOUND_BG = 0;
	public static final int SOUND_TITLE = 0;


	public static final String PATH_SOUNDS = "/";


//#if CONFIG == "BIG"
	public static final int ballGirlFrameCount = 7;

//#if ORIENTATION == "LANDSCAPE"
//# 	public static final int ballGirlXOffset = 75;
//# 	public static final int ballGirlYOffset = 35;
//#endif

//#if ORIENTATION != "LANDSCAPE"
public static final int ballGirlXOffset = 35;
public static final int ballGirlYOffset = 25;
//#endif


	public static final int ballLockXOffset = 0;
	public static final int ballLockYOffset = 10 + 8;
//#endif

//#if CONFIG == "MEDIUM"
//# 	public static final int ballGirlFrameCount = 7;
//# 	public static final int ballGirlXOffset = 12;
//# 	public static final int ballGirlYOffset = 20;
//# 	public static final int ballLockXOffset = 0;
//# 	public static final int ballLockYOffset = 8;
//#endif





	public static final int SCREEN_SPLASH_ENDEMOL = 0;
	public static final int SCREEN_FEAR_FACTOR = 1;

	public static final int DESIRED_FRAMES_PER_SECOND = 20;

	public static final int BRIEF_HELP_TIME = 3;

	public static final String PATH_SPLASH = "/splash/";

//#if CONFIG == "BIG"
//#if ORIENTATION == "LANDSCAPE"
//# 	public static final int PREFERED_SIZE_X = 320;
//# 	public static final int PREFERED_SIZE_Y = 240;
//#endif

//#if ORIENTATION != "LANDSCAPE"
public static final int PREFERED_SIZE_X = 240;
public static final int PREFERED_SIZE_Y = 320;
//#endif

//#endif
	
//#if CONFIG == "MEDIUM"
//# 	public static final int PREFERED_SIZE_X = 176;
//# 	public static final int PREFERED_SIZE_Y = 208;
//#endif

//#if CONFIG == "SMALL"
//# 	public static final int PREFERED_SIZE_X = 128;
//# 	public static final int PREFERED_SIZE_Y = 128;
//#endif

}
