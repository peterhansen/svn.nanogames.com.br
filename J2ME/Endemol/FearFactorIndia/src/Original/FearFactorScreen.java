package Original;

///imports
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.MediaPlayer;
import javax.microedition.lcdui.*;
import java.io.*;
import java.util.Random;
import javax.microedition.rms.RecordStore;
//==============================================================================
public final class FearFactorScreen extends Drawable
		implements Updatable, GameConstants, KeyListener, ScreenListener
//#if TOUCH == "true"
		, PointerListener
//#endif
{

	private long timeSinceLastPass;
	private int highestLevel;
	private int counter_title = 0;
	private int highwireWalkerCenterX;
	private int highwireWalkerCenterY;
	private int textScrollPositionY;
	private int currentScreen;
	private int titleNameAniCurrentFrame = 0;
	private int titleNameDisplay = 0;
	private int backgroundArrow1X = 0;
	private int backgroundArrow2X = 60;
	private int backgroundArrow3X = 0;
	private int backgroundArrow4X = 0;
	private int menuSelectionAnimationX = -10;
	private int lockPinState[];
	private boolean performItemSelectionAnimation = false;
	private long[] timePerGame;
	/////////////////////// INGAME LOGIC SPECIFIC VARIABLES !!
	private int total_score;
	private int scrollCounter = 0;
	private int highwireWalkerTimer;
	private int knifeStartY = 0;
	private Random knifePositionRandomizer;
	private Image arrows_sub;
	private Image button;
	private Image button1;
	private Image buttonAni;
	private Image button2;
	private Image button3;
	private Image[] imgTitle;
	private Image[] image_gameone;
	private Image[] image_gametwo;
	private Image[] image_gamethree;
	private Image[] image_gamefour;
	private Image[] image_gamefive;
	private Sprite[] game5VisualJoystick;
	//******************************GAME ONE varible decleared ****************************//
	private boolean releasingKnife;
	//estado de vida da mocinha
	private boolean stabbedGirl;
	//Tecla esquerda pressionada
	private boolean leftKeyPressed;
	//Tecla direita pressionada
	private boolean rightKeyPressed;
	private boolean fireKeyPressed;
	//se a garota esta ou não olhando para a esquerda
	private boolean knifeGirlFacingLeft;
	private long counterKnifeTimer = 0;
	private int knifeDustAnim1 = 0;
	private int knifeDustAnim2 = 0;
	private int knifeDustAnim3 = 0;
	private int knifeDustAnim4 = 0;
	private long[] scorePerGame;
	private long seconds = 0;
	private long miliseconds;
	private int knife1StartPositionY = 28;
	private int knife2StartPositionY = 28;
	private int knife3StartPositionY = 28;
	private int knife4StartPositionY = 28;
	private int girl_clipX = 22;
	private int girl_clipY = 42;
	//de qual dos slots a faca vai cair
	private int randomKnife = 0;
	private int jumpingKnifeGirlFrame = 0;
	private int knifeGirlJumpStartPositionY = 45;
	private int girlStartPosition = 36;
	private int knifeGirlPositionY = 67;
	private int knifeGirlPositionX = 0;
	private int girlSpriteWidth = 22;
	private int girlSpriteHeight = 42;
	private int knifeSpriteWidth = 4;
	private int knifeSpriteHeight = 18;
//*****************************END of Game One VAR DEC *****************************//
	//*************************************** GAME TWO variables declared here*************************//
	private boolean eatingSnake = false;
	private boolean fearFlagSnake;
	private boolean snakeInHandAnimationEnded;
	private boolean lipsMoving;
	private boolean justWonTheMatch;
	private int snakeCount = 0;
	private int fearSnakeAmount = 64;
	private int snakeInHandAnimationFrame = 0;
	private int snakeInHandAnimationTimeCounter = 0;
	private int numberOfSnakes = 1;
	private int lipAnimationFrame = 0;
	private int snakeInMouthFrame = 0;
	private long gameStartTime = 0;
	private long Max_Time_G2 = 120;
//************************* END GAME TWO variables declared ***********************************//
	private byte currentGame;
	//controla posição dos canos
	private byte pipeHorizontalPosition = 0;
	private byte stop_counter = 60;
	private boolean ballGirlIsUnlocked = false;
	private boolean pipeforward0 = true;
	private boolean pipeforward1 = true;
	private boolean pipeforward2 = true;
	private boolean pipeforward3 = true;
	///trancas do terceiro jogo
	private boolean ballGirlLock1;
	private boolean ballGirlLock2;
	private boolean ballGirlLock3;
	private boolean ballGirlLock4;
	// oxigênio absoluto
	private int oxygenCount = 0;
	// oxigênio relativo
	private int oxygenControl = 0;
	///frame atual da garota se libertando ao fim do terceiro jogo
	private int ballGirlFreedFrame = 0;
	private int girls_lock_count = 0;
	private int pipe_counter0 = 0;
	private int pipe_counter1;
	private int pipe_counter2;
	private int pipe_counter3;
	private int pipeLockPositionY = 0;
	///posição real da corrente
	private int ironBallHeight = 0;
	//passagem de tempo no terceiro jogo
	private int chainProgression = 0;
	private long Max_Time_G4 = 120;
//***************************** game Five*************************
	private int skyPaintX;
	/*porting variables */
	private int clipWidth = 60;
	private int clipHeight = 58;
	private int highwireWalkerVerticalPosition;
	private int highwireWalkerFrameCounter;
	private int highwireWalkerBalance;
	private int const_factor = 15;
	///frames de animação de liberdade do jogador no jogo 4
	private int manFreeingFrame = 0;
	private int fish1HorizontalPosition = 15;
	private int fish2HorizontalPosition = 45;
	private int fish2SwimPaceController = 0;
	private int menuSel = 1;
	private int animationCounter = 0;
	private int subMenuSel = 1;
	private int fish1SwimPaceController;
	///DEPENDE DE RESOLUCAO!!!!////////
	private int bubble_x = 240 / 2;
	private int bubble_y = 320 / 3;
	////////////////////////////////////////////
	private int TotalScore = 0;
	///posição vareta de destravamento dos cadeados do jogo 4
	private int unlockingStickHorizontalPosition = 10;
	///quantos cadeados devem ser destravados na quarta fase
	private int locksToUnlock = 4;
	///velocidade de vai e vem da varinha de desarmar cadeados
	private int unlockingStickSpeed = 3;
	///flag para inicialização da quarta fase
	private boolean stage4JustStarted = true;
	///jogo 4 ainda em andamento?
	private boolean manIsFreed = false;
	private boolean unlockingStickMovingLeft = false;
	private boolean unlockingStickActive = false;
	private boolean highwireWalkerMovingRight;
	private boolean highwireWalkerIsFalling;
	private boolean highwireWalkerFell;
	private boolean flag5_gOver;
	private boolean fish1MovingRight;
	private boolean fish2MovingRight;
	///flag indicando que o jogador acabou de se libertar de um cadeado
	private boolean manJustReleasedLock;
	private long endGameTime = 0;
	private long startGameTime = 0;
	private long Max_Time_G5 = 250;
	private long Max_Time_G3 = 120;
	private int keyStatus;	// stores the status of all the keys !!
	private Random game4Random = new Random();
	private Random highwireWalkerBalanceRandomizer;
	private DrawableRect leftSoftKeyDrawable, rightSoftkeyDrawable;
	//private Image Green_Black, White_Font;
	private char[] char_array1 =
	{
		48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 64, 33, 59, 34, 58, 60, 61, 62, 40, 41, 96, 42, 36, 63, 91, 92, 93, 46, 39, 36, 35, 94, 45, 95, 43, 44, 47, 32
	};
	private byte[] char_width_array1 =
	{
		6, 3, 6, 5, 5, 6, 5, 5, //8  upto 7
		5, 5, 5, 6, 5, 6, 5, 4, //8  upto  f
		6, 7, 4, 3, 6, 4, 10, //7  upto m
		7, 5, 6, 6, 6, 5, 5, //7  upto gameThread
		7, 7, 10, 7, 7, 6, 7, //7 upto A
		6, 5, 6, 6, 6, 6, 7, 4, //8  upto I
		4, 7, 7, 9, 8, 5, 6, 5, //8    upto Q
		7, 5, 6, 7, 7, 9, 8, 8, //8   upto Y
		6, 11, 2, 3, 5, 2, 4, 5, //8   upto =
		4, 3, 3, 3, 5, 9, 6, 3, //8    upto [
		4, 3, 2, 3, 5, 7, 5, 3, //8   upto -
		5, 6, 3, 4, 4 - 2
	};
	///quantos pinos por cadeado na 4a fase
	private int pinsPerLock[] =
	{
		1, 1, 2, 2
	};
	private final String menuOption[] = new String[]
	{
		getText(TEXT_MENU_PLAY), getText(TEXT_MENU_HELP), getText(TEXT_MENU_SCORE), getText(TEXT_MENU_SOUND), getText(TEXT_MENU_ABOUT), getText(TEXT_MENU_EXIT)
	};
	private final String gameOption[] = new String[]
	{
		getText(TEXT_MINIGAMES_DAGGERS), getText(TEXT_MINIGAMES_CRAWLER), getText(TEXT_MINIGAMES_BALL), getText(TEXT_MINIGAMES_AQUA), getText(TEXT_MINIGAMES_ACRO)
	};
	private final String pausemenuOption[] = new String[]
	{
		getText(TEXT_PAUSEMENU_RESUME), getText(TEXT_PAUSEMENU_SOUND), getText(TEXT_PAUSEMENU_MAINMENU)
	};
	private final RichLabel richLabel;
	private final Label simpleWhiteLabel;
	private final Label digitalLabel;
	private final Label simpleBlackLabel;
	private int penXPos;
	private int penYPos;
	private boolean updating;
	private DrawableImage clockBoxMiddle;
	private DrawableImage clockBoxLeftCorner;
	private DrawableImage clockBoxRightCorner;
//------------------------------------------------------------------------------

	/**
	 *
	 * @param keyCode
	 */
	public void keyPressed(int keyCode)
	{
		switch (keyCode)
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				keyCode = ScreenManager.KEY_SOFT_RIGHT;
				break;
			case ScreenManager.KEY_NUM5:
			case ScreenManager.KEY_SOFT_LEFT:
				keyCode = ScreenManager.FIRE;
				break;
			case ScreenManager.KEY_NUM4:
				keyCode = ScreenManager.LEFT;
				break;
			case ScreenManager.KEY_NUM6:
				keyCode = ScreenManager.RIGHT;
				break;
			case ScreenManager.KEY_NUM8:
				keyCode = ScreenManager.DOWN;
				break;
			case ScreenManager.KEY_NUM2:
				keyCode = ScreenManager.UP;
				break;
		}

		keyStatus = keyCode;

		try
		{
			switch (currentScreen)
			{
				case INGAME_ONE:
				case INGAME_TWO:
				case INGAME_THREE:
				case INGAME_FOUR:
				case INGAME_FIVE:
				{
					if (keyStatus == (ScreenManager.KEY_SOFT_RIGHT))
					{
						subMenuSel = 0;
						changeGameState(PAUSE_SCREEN);
					}

				}
				break;
				case MAIN_MENU_SCREEN:
				{
					if (!performItemSelectionAnimation)
					{
						if (keyStatus == (ScreenManager.DOWN))
						{
							menuSel = (short) ((menuSel + 1) > 6 ? 1 : menuSel + 1);
							break;

						} else if (keyStatus == (ScreenManager.UP))
						{
							menuSel = (short) ((menuSel - 1) < 1 ? 6 : menuSel - 1);
							break;
						}

						if (keyStatus == (ScreenManager.FIRE))
						{
							clearAllKeys();
							performItemSelectionAnimation = true;
						}
					}

					clearAllKeys();
				}
				break;

				case SOUNDENABLE_SCREEN:
				{

					if (isKeyPressed(ScreenManager.FIRE))
					{
						MediaPlayer.setMute(false);
						MediaPlayer.play(SOUND_TITLE, MediaPlayer.LOOP_INFINITE);
						changeGameState(TITLE_SCREEN);
					} else if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						MediaPlayer.setMute(true);
						MediaPlayer.stop();
						changeGameState(TITLE_SCREEN);
					}

					clearAllKeys();
				}
				break;

				case TITLE_SCREEN:
				{
					if (isKeyPressed(ScreenManager.FIRE))
					{
						//sp.stopSounds();

						imgTitle[1] = null;
						imgTitle[2] = null;

						if (button == null)
						{
							button = ImageLoader.loadImage("/button.png");
						}

						if (button1 == null)
						{
							button1 = ImageLoader.loadImage("/button1.png");
						}

						if (buttonAni == null)
						{
							buttonAni = ImageLoader.loadImage("/buttonAni.png");
						}

						if (button2 == null)
						{
							button2 = ImageLoader.loadImage("/button2.png");
						}

						if (button3 == null)
						{
							button3 = ImageLoader.loadImage("/button3.png");
						}

						if (arrows_sub == null)
						{
							arrows_sub = ImageLoader.loadImage("/arrowssub.png");
						}

						changeGameState(MAIN_MENU_SCREEN);
					}

					clearAllKeys();
				}
				break;

				case GAME_OVER:
				{
					if (isKeyPressed(ScreenManager.FIRE))
					{
						if (arrows_sub == null)
						{
							arrows_sub = ImageLoader.loadImage("/arrowssub.png");
						}

						unloadAllInGameImages();

						if (currentGame == 1)
						{
							initGameTwo();
							resetGameTwoVariable();
							gameStartTime = System.currentTimeMillis();
							Max_Time_G2 = 120;
							changeGameState(INGAME_TWO);

							if (highestLevel == 0)
							{
								highestLevel = 1;
							}

							saveData();
						} else if (justWonTheMatch)
						{

							if (currentGame == 2)
							{
								initGameThree();
								resetGameThreeVariable();
								gameStartTime = System.currentTimeMillis();
								Max_Time_G3 = 120;
								changeGameState(INGAME_THREE);

								if (highestLevel == 1)
								{
									highestLevel = 2;
								}

								saveData();
							} else if (currentGame == 3)
							{
								initGameFour();
								resetGameFourVariable();
								gameStartTime = System.currentTimeMillis();
								Max_Time_G4 = 120;
								changeGameState(INGAME_FOUR);
								if (highestLevel == 2)
								{
									highestLevel = 3;
								}
								saveData();
							} else if (currentGame == 4)
							{
								highwireWalkerFell = false;
								initGameFive();
								resetGameFiveVariable();
								gameStartTime = System.currentTimeMillis();
								Max_Time_G5 = 250;
								changeGameState(INGAME_FIVE);
								if (highestLevel == 3)
								{
									highestLevel = 4;
								}
								saveData();
							} else
							{
								TotalScore = (int) (scorePerGame[0] + scorePerGame[1] + scorePerGame[2] + scorePerGame[3] + scorePerGame[4]);
								changeGameState(HIGHSCORE_SCREEN);
								saveData();
							}


						} else
						{
							changeGameState(MAIN_MENU_SCREEN);
						}
					}

					TotalScore = (int) (scorePerGame[0] + scorePerGame[1] + scorePerGame[2] + scorePerGame[3] + scorePerGame[4]);
					saveData();
					clearAllKeys();
				}

				break;

				case INGAME_SUBMENU:
				{
					if (!performItemSelectionAnimation)
					{
						if (isKeyPressed(ScreenManager.UP))
						{
							subMenuSel--;

							if (subMenuSel < 1)
							{
								subMenuSel = highestLevel + 1;
							}
							break;
						}

						if (isKeyPressed(ScreenManager.DOWN))
						{
							subMenuSel++;
							if (subMenuSel > highestLevel + 1)
							{
								subMenuSel = 1;
							}
							break;
						}

						if (isKeyPressed(ScreenManager.FIRE)) // pause the game !!
						{
							clearAllKeys();
							TotalScore = 0;
							scorePerGame[0] = 0;
							scorePerGame[1] = 0;
							scorePerGame[2] = 0;
							scorePerGame[3] = 0;
							scorePerGame[4] = 0;
							performItemSelectionAnimation = true;
						}

						if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
						{
							changeGameState(MAIN_MENU_SCREEN);	// main menu !!
							break;
						}
					}
					clearAllKeys();
				}
				break;


				//HELP SCREEN

				case HELP_SCREEN:
				{
					if (isKeyPressed(ScreenManager.UP))
					{
						scrollCounter--;

						if (textScrollPositionY > 0)
						{
							textScrollPositionY--;
						}

						clearAllKeys();
					} else if (isKeyPressed(ScreenManager.DOWN))
					{
						scrollCounter++;

						clearAllKeys();
					}

					if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						changeGameState(MAIN_MENU_SCREEN);
						clearAllKeys();
					}

					clearAllKeys();
				}
				break;
				//ABOUT SCREEN

				case ABOUT_SCREEN:
				{
					if (isKeyPressed(ScreenManager.UP))
					{
						clearAllKeys();
						scrollCounter--;

						if (textScrollPositionY > 0)
						{
							textScrollPositionY--;
						}

					} else if (isKeyPressed(ScreenManager.DOWN))
					{
						clearAllKeys();
						scrollCounter++;
					}

					if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						clearAllKeys();
						changeGameState(MAIN_MENU_SCREEN);
					}

					clearAllKeys();
				}
				break;

				case CREDIT_SCREEN:
				{
					if (isKeyPressed(ScreenManager.UP))
					{
						clearAllKeys();
						scrollCounter--;
						if (textScrollPositionY > 0)
						{
							textScrollPositionY--;
						}

					} else if (isKeyPressed(ScreenManager.DOWN))
					{
						clearAllKeys();
						scrollCounter++;
					}

					if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						clearAllKeys();
						changeGameState(MAIN_MENU_SCREEN);
					}

					clearAllKeys();
				}
				break;
				//HIGHSCORE SCREEN

				case HIGHSCORE_SCREEN:
				{
					if (isKeyPressed(ScreenManager.UP))
					{
						if (textScrollPositionY > 0)
						{
							textScrollPositionY--;
						}
					} 

					if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						changeGameState(MAIN_MENU_SCREEN);
					}

					clearAllKeys();
				}
				break;

				//PAUSE SCREEN
				case PAUSE_SCREEN:
				{
					// Scrolling on Pause menu !!
					if (isKeyPressed(ScreenManager.UP))
					{

						if (subMenuSel == 0)
						{
							subMenuSel = (byte) (pausemenuOption.length - 1);
						} else
						{
							subMenuSel--;
						}

						clearAllKeys();
					} else if (isKeyPressed(ScreenManager.DOWN))
					{

						if (subMenuSel == (pausemenuOption.length -  1))
						{
							subMenuSel = 0;
						} else
						{
							subMenuSel++;
						}

						clearAllKeys();
					}

					if (isKeyPressed(ScreenManager.FIRE))
					{
						clearAllKeys();

						switch (subMenuSel)
						{
							case 0:	// continue game !!
								switch (currentGame)
								{
									case 1://beware continue game
										gameStartTime = System.currentTimeMillis() - ((120 - gameStartTime) * 1000);
										changeGameState(INGAME_ONE);

										break;
									case 2:
										gameStartTime = System.currentTimeMillis() - ((120 - timePerGame[1]) * 1000);
										changeGameState(INGAME_TWO);

										break;
									case 3:
										gameStartTime = System.currentTimeMillis() - ((120 - timePerGame[2]) * 1000);
										changeGameState(INGAME_THREE);
										break;
									case 4:
										gameStartTime = System.currentTimeMillis() - ((120 - timePerGame[3]) * 1000);
										changeGameState(INGAME_FOUR);
										break;
									case 5:
										gameStartTime = System.currentTimeMillis() - ((250 - timePerGame[4]) * 1000);
										changeGameState(INGAME_FIVE);
										break;
								}


								break;


							case 1:
								MediaPlayer.setMute(!MediaPlayer.isMuted());
								if (!MediaPlayer.isMuted())
								{
									MediaPlayer.play(SOUND_TITLE, MediaPlayer.LOOP_INFINITE);
								}
								break;

							case 2:
								changeGameState(CURRENT_GAME_QUIT_CONFIRM_SCREEN);
								break;
						}
					}

					clearAllKeys();
				}
				break;

				//PRE GAME INFO SCREEN

				case PRE_GAME_INFO_SCREEN:
				{
					clearAllKeys();
				}
				break;

				//CURRENT GAME QUIT CONFIRM SCREEN
				case CURRENT_GAME_QUIT_CONFIRM_SCREEN:
				{
					GameMIDlet.log("quit?");
					if (isKeyPressed(ScreenManager.FIRE)) // yes
					{
						unloadAllInGameImages();

						changeGameState(MAIN_MENU_SCREEN);	// main menu !!
						subMenuSel = 1;

					} else if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						changeGameState(PAUSE_SCREEN);
					}

					//updateScreenWithDelay(menuDelay);
					//clearAllKeys();
				}
				break;

				//GAME EXIT CONFIRM SCREEN
				case GAME_EXIT_CONFIRM_SCREEN:
				{
					GameMIDlet.log("quit?");
					if (isKeyPressed(ScreenManager.FIRE))
					{
						unloadAllInGameImages();
						GameMIDlet.exit();
					}

					if (isKeyPressed(ScreenManager.KEY_SOFT_RIGHT))
					{
						changeGameState(MAIN_MENU_SCREEN);
					}

					clearAllKeys();
				}
				break;
			}
		} catch (Exception ex)
		{
		}
		GameMIDlet.log("key:" + String.valueOf(keyStatus));
//#if DEBUG == "true"
		highestLevel = 4;
//#endif
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param keyCode
	 */
	public void keyReleased(int keyCode)
	{
		clearAllKeys();
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @returns score total do jogo
	 */
	public int getScore()
	{
		return total_score;
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 */
	public FearFactorScreen() throws Exception
	{
		richLabel = new RichLabel(FONT_WHITE_MEDIUM, "");
		richLabel.setSize(ScreenManager.SCREEN_WIDTH, 0xFFFF);
		digitalLabel = new Label(FONT_GREEN_DIGITAL);
		digitalLabel.setSize(500, 500);
		simpleBlackLabel = new Label(FONT_BLACK_MEDIUM);
		simpleBlackLabel.setSize(500, 500);
		simpleWhiteLabel = new Label(FONT_WHITE_MEDIUM);
		simpleWhiteLabel.setSize(500, 500);
		timeSinceLastPass = 0;
		timePerGame = new long[5];
		scorePerGame = new long[5];

		highwireWalkerCenterX = getWidth() / 2 - 20;
		highwireWalkerCenterY = (getHeight() / 2);

		bubble_x = getWidth() / 2;
		bubble_y = getHeight() / 3;

		penXPos = getWidth() / 2;
		penYPos = getHeight() / 2;

		highwireWalkerBalanceRandomizer = new Random();
		lockPinState = new int[4];

		readData();

		sizeChanged(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);


		leftSoftKeyDrawable = new DrawableRect();
		rightSoftkeyDrawable = new DrawableRect();

		leftSoftKeyDrawable.setPosition(0, getHeight() - 13);
		leftSoftKeyDrawable.setSize(getWidth() / 2, 13);
		rightSoftkeyDrawable.setPosition(getWidth() / 2, getHeight() - 13);
		rightSoftkeyDrawable.setSize(getWidth() / 2, 13);

		changeGameState(TITLE_SCREEN);
	}
//------------------------------------------------------------------------------

	/**
	 * @note observe que mesmo que o joystick de tela do jogo 5 nao esteja
	 * @note alocado, o teste so acontece se houver toque. Só haverá toque caso
	 * @note os eventos de ponteiro sejam suportados, caso em que a imagem
	 * @note estara alocada.
	 * @param delta
	 */
	public void update(int delta)
	{

		updating = true;

//#if CONFIG == "BIG"
		int minSnakeFearMeterValue = 50;
		int maxSnakeFearMeterValue = 190;
//#endif

//#if CONFIG == "MEDIUM"
//# 		int minSnakeFearMeterValue = 40;
//# 		int maxSnakeFearMeterValue = 130;
//#endif

//#if CONFIG == "SMALL"
//# 		int minSnakeFearMeterValue = 30;
//# 		int maxSnakeFearMeterValue = 98;
//#endif

		///controle de velocidade de jogo
		int idealUpdateInterval = ((1 * SECOND) / DESIRED_FRAMES_PER_SECOND);

		if (idealUpdateInterval < 0)
		{
			idealUpdateInterval = 1;
		}

		timeSinceLastPass += delta;
		//GameMIDlet.log("timeSinceLastPass:"+timeSinceLastPass);

		if (timeSinceLastPass < idealUpdateInterval)
		{
			updating = false;
			return;
		} else
		{
			updating = true;
			//	GameMIDlet.log("update");
			//	GameMIDlet.log("idealUpdateInterval:"+idealUpdateInterval);
			timeSinceLastPass %= idealUpdateInterval;
		}

		//	GameMIDlet.log("delta:"+delta);

		// initial screen of the game !!		
		try
		{
			// set the initial currentScreen
			switch (currentScreen)
			{
				//TITLE SCREEN
				case INGAME_ONE:
				{
					endGameTime = System.currentTimeMillis();

					miliseconds = (endGameTime - startGameTime);

					timePerGame[0] = 120 - (System.currentTimeMillis() - gameStartTime) / MILISECONDS_IN_A_SECOND;

					///torna o tempo consistente
					if (timePerGame[0] < 0)
					{
						timePerGame[0] = 0;
					}

					if (miliseconds > 1000)
					{
						miliseconds -= 1000;
					}

					if (miliseconds < 0)
					{
						miliseconds = 0;
					}

					if ((endGameTime - startGameTime) / MILISECONDS_IN_A_SECOND >= 1)
					{
						seconds++;
						scorePerGame[0]++;
						startGameTime = System.currentTimeMillis();
					}

					//seleciona de onde sairá a faca
					if (knife1StartPositionY == knifeStartY && knife2StartPositionY == knifeStartY && knife3StartPositionY == knifeStartY && knife4StartPositionY == knifeStartY)
					{
						randomKnife = 1 + Math.abs(knifePositionRandomizer.nextInt()) % 4;
					}

					if (counterKnifeTimer == 0 && randomKnife == 2)
					{
						randomKnife = 3;
					}


					counterKnifeTimer += delta;

					///dispara a faca (1/2)
					if (counterKnifeTimer % 15 == 0)
					{
						releasingKnife = true;
					}

					if (!leftKeyPressed && !rightKeyPressed)
					{
						if (isKeyPressed(ScreenManager.LEFT))
						{
							leftKeyPressed = true;
							clearAllKeys();
						}

						if (isKeyPressed(ScreenManager.RIGHT))
						{
							rightKeyPressed = true;
							clearAllKeys();
						}
					}

					int getwidth = getWidth();
					if (leftKeyPressed)
					{

//#if CONFIG == "BIG"
						if (knifeGirlPositionX > -(getwidth / 4))
//#endif
//#if CONFIG == "MEDIUM"
//# 						if (knifeGirlPositionX > - 42)
//#endif
//#if CONFIG == "SMALL"
//# 						if (knifeGirlPositionX > - 30)
//#endif
						{
							knifeGirlPositionX -= getwidth / 24;
						}
					} else if (rightKeyPressed)
					{
						if (knifeGirlPositionX < getWidth() / 2)
						{
							knifeGirlPositionX += getWidth() / 24;
						}
					}

					if (stabbedGirl)
					{
						scorePerGame[0] *= 10;
						changeGameState(GAME_OVER);
					}

					///facas
					int _knife1X = 10 + 0 * getWidth() / 4;
					int _knife2X = 10 + 1 * getWidth() / 4;
					int _knife3X = 10 + 2 * getWidth() / 4;
					int _knife4X = 10 + 3 * getWidth() / 4;
					int _standKnifeCollisionY = getHeight() - image_gameone[6].getHeight() - image_gameone[2].getHeight();

					if (randomKnife == 1 || knife1StartPositionY > knifeStartY)
					{

						if (knife1StartPositionY < _standKnifeCollisionY)
						{
							knife1StartPositionY += 8;
						} else
						{
							if (knifeDustAnim1++ >= 4)
							{
								knife1StartPositionY = knifeStartY;
								knifeDustAnim1 = 0;
							}
						}

						if (knifeDustAnim1 == 0 && knife1StartPositionY < (_standKnifeCollisionY - girlSpriteHeight / 3) && collision(_knife1X + 8, knife1StartPositionY, knifeSpriteWidth, knifeSpriteHeight, girlStartPosition + knifeGirlPositionX, knifeGirlPositionY, girlSpriteWidth, girlSpriteHeight))
						{
							stabbedGirl = true;
						}
					}


					if (randomKnife == 2 || knife2StartPositionY > knifeStartY)
					{
						if (knife2StartPositionY < _standKnifeCollisionY)
						{
							knife2StartPositionY += 8;
						} else
						{
							if (knifeDustAnim2++ >= 4)
							{
								knife2StartPositionY = knifeStartY;
								knifeDustAnim2 = 0;
							}
						}

						if (knifeDustAnim2 == 0 && knife2StartPositionY < (_standKnifeCollisionY - girlSpriteHeight / 3) && collision(_knife2X + 8, knife2StartPositionY, knifeSpriteWidth, knifeSpriteHeight, girlStartPosition + knifeGirlPositionX, knifeGirlPositionY, girlSpriteWidth, girlSpriteHeight))
						{
							stabbedGirl = true;
						}
					}

					if (randomKnife == 3 || knife3StartPositionY > knifeStartY)
					{
						if (knife3StartPositionY < _standKnifeCollisionY)
						{
							knife3StartPositionY += 8;
						} else
						{
							if (knifeDustAnim3++ >= 4)
							{
								knife3StartPositionY = knifeStartY;
								knifeDustAnim3 = 0;
							}
						}

						if (knifeDustAnim3 == 0 && knife3StartPositionY < (_standKnifeCollisionY - girlSpriteHeight / 3) && collision(_knife3X + 8, knife3StartPositionY, knifeSpriteWidth, knifeSpriteHeight, girlStartPosition + knifeGirlPositionX, knifeGirlPositionY, girlSpriteWidth, girlSpriteHeight))
						{
							stabbedGirl = true;
						}
					}

					if (randomKnife == 4 || knife4StartPositionY > knifeStartY)
					{
						if (knife4StartPositionY < _standKnifeCollisionY)
						{
							knife4StartPositionY += 8;
						} else
						{
							if (knifeDustAnim4++ >= 4)
							{
								knife4StartPositionY = knifeStartY;
								knifeDustAnim4 = 0;
							}
						}

						if (knifeDustAnim4 == 0 && knife4StartPositionY < (_standKnifeCollisionY - girlSpriteHeight / 3) && collision(_knife4X + 8, knife4StartPositionY, knifeSpriteWidth, knifeSpriteHeight, girlStartPosition + knifeGirlPositionX, knifeGirlPositionY, girlSpriteWidth, girlSpriteHeight))
						{
							stabbedGirl = true;
						}
					}

					clearAllKeys();
				}
				break;

				case INGAME_TWO:
				{
					timePerGame[1] = 120 - (System.currentTimeMillis() - gameStartTime) / MILISECONDS_IN_A_SECOND;

					///torna o tempo consistente
					if (timePerGame[1] < 0)
					{
						timePerGame[1] = 0;
					}

					//game over condition
					if (timePerGame[1] <= 0)
					{
						justWonTheMatch = false;
						changeGameState(GAME_OVER);
					}

					if (isKeyPressed(ScreenManager.FIRE)) // action key !!
					{
//#if ORIENTATION == "LANDSCAPE"
//# 						int min = (getWidth() / 2 - 45);
//# 						int max = (getWidth() / 2 + 45);
//#endif

//#if ORIENTATION != "LANDSCAPE"
						int min = (getWidth() / 2 - 15);
						int max = (getWidth() / 2 + 15);
//#endif

//#if DEBUG == "true"
						min = 0;
						max = getWidth();
//#endif

						if (snakeInHandAnimationEnded)
						{
							if (fearSnakeAmount > min && fearSnakeAmount < max)
							{
								snakeCount++;
								lipsMoving = true;
							}

							if ((fearSnakeAmount > 0 && fearSnakeAmount < min) || (fearSnakeAmount > max && fearSnakeAmount < getWidth()))
							{
								if (snakeCount >= 0)
								{
									snakeCount--;
								}

								if (snakeCount <= 0)
								{
									justWonTheMatch = false;
									changeGameState(GAME_OVER);
								}
							}

							if (snakeCount == 6 && numberOfSnakes <= 5)
							{
								numberOfSnakes++;
								snakeCount = 0;
								snakeInHandAnimationEnded = false;
								eatingSnake = true;
							}

							if (numberOfSnakes == 6 && timePerGame[1] > 0)
							{
								justWonTheMatch = true;
								scorePerGame[1] = (Max_Time_G2 - (Max_Time_G2 - timePerGame[1])) * 10;
								changeGameState(GAME_OVER);
							}

							if (numberOfSnakes < 6 && timePerGame[1] < 0)
							{
								justWonTheMatch = false;
								changeGameState(GAME_OVER);
							}
						}

					}
					///desloca para um lado ou para o outro
					if (fearFlagSnake)
					{
						fearSnakeAmount += 6;
					} else
					{
						fearSnakeAmount -= 6;
					}


					///se passou dos limites pela direita
					if (fearSnakeAmount >= maxSnakeFearMeterValue)
					{
						fearFlagSnake = false;
					}

					//se passou dos limites pela esquerda
					if (fearSnakeAmount <= minSnakeFearMeterValue)
					{
						fearFlagSnake = true;
					}
				}
				break;

				case INGAME_THREE:
				{
					timePerGame[2] = 120 - (System.currentTimeMillis() - gameStartTime) / MILISECONDS_IN_A_SECOND;

					if (isKeyPressed(ScreenManager.FIRE))
					{
						///torna o tempo consistente
						if (timePerGame[2] < 0)
						{
							timePerGame[2] = 0;
						}

						if (timePerGame[2] <= 0 && !ballGirlIsUnlocked)
						{
							justWonTheMatch = false;
							changeGameState(GAME_OVER);
						}

						if (pipe_counter0 >= 4 && pipe_counter0 <= 8 && stop_counter > 45)
						{
							///tira a primeira tranca
							ballGirlIsUnlocked = false;
							pipeLockPositionY = 10;
							stop_counter = 45;
							ballGirlLock1 = true;
							pipe_counter0 = 6;
						} else if (pipe_counter1 >= 4 && pipe_counter1 <= 8 && stop_counter == 45)
						{
							///tira a segunda tranca
							ballGirlIsUnlocked = false;
//#if CONFIG != "SMALL"
							pipeLockPositionY = 27 + 8;
//#endif

//#if CONFIG == "SMALL"
//# 							pipeLockPositionY = 18;
//#endif

							stop_counter = 35;
							ballGirlLock2 = true;
							pipe_counter1 = 6;
						} else if (pipe_counter2 >= 4 && pipe_counter2 <= 8 && stop_counter == 35)
						{
							///tira a terceira tranca
							ballGirlIsUnlocked = false;
//#if CONFIG != "SMALL"
							pipeLockPositionY = 44 + 14;
//#endif

//#if CONFIG == "SMALL"
//# 							pipeLockPositionY = 30;
//#endif
							stop_counter = 25;
							ballGirlLock3 = true;
							pipe_counter2 = 6;
						} else if (pipe_counter3 >= 4 && pipe_counter3 <= 8 && stop_counter == 25)
						{
							//tira a quarta tranca e vence o minigame
//#if CONFIG != "SMALL"
							pipeLockPositionY = 61 + 16;
//#endif

//#if CONFIG == "SMALL"
//# 							pipeLockPositionY = 42;
//#endif
							stop_counter = 15;
							ballGirlIsUnlocked = true;
							ballGirlLock4 = true;
							justWonTheMatch = true;
							pipe_counter3 = 6;
						} else
						{
							switch (stop_counter)
							{
								case 15:
									pipe_counter3 = 0;
								case 25:
									pipe_counter2 = 0;
								case 35:
									pipe_counter1 = 0;
								case 45:
									pipe_counter0 = 0;
							}

							girls_lock_count = 0;
							pipeLockPositionY = 0;
							ballGirlIsUnlocked = false;
							stop_counter = 60;
						}

						clearAllKeys();
					}
					///passagem de tempo no terceiro jogo
					chainProgression++;



//#if CONFIG == "BIG"
					if (chainProgression % 25 == 0)
					{
						ironBallHeight += 10;
					}

//#if ORIENTATION != "LANDSCAPE"
					if (ironBallHeight > getHeight() / 2 + 30 && !ballGirlIsUnlocked)
//#endif
//#if ORIENTATION == "LANDSCAPE"
//# 					if (ironBallHeight > getHeight() / 2 + 10 && !ballGirlIsUnlocked)
//#endif
//#endif
//#if CONFIG == "MEDIUM"
//# 				if (chainProgression % 25 == 0)
//# 				{
//# 					ironBallHeight += 10;
//# 				}
//# 
//# 
//# 				if (ironBallHeight > getHeight() / 2 + 10 && !ballGirlIsUnlocked)
//#endif
//#if CONFIG == "SMALL"
//# 					if (chainProgression % 25 == 0)
//# 					{
//# 						ironBallHeight++;
//# 					}
//#
//# 					if (ironBallHeight > 55 && !ballGirlIsUnlocked)
//#endif
					{
						ironBallHeight = 0;
						justWonTheMatch = false;
						changeGameState(GAME_OVER);
					}


					///primeiro cano
					if (pipeHorizontalPosition >= 0 && stop_counter > 50)
					{


						///faz os movimento de ir e voltar dos canos
						if (pipeforward0)
						{
							pipe_counter0 += 2;
						} else
						{
							pipe_counter0 -= 2;
						}

						///controla os limites do movimento do cano
						if (pipe_counter0 >= 20)
						{
							pipeforward0 = false;
						}

						///controla os limites do movimento do cano
						if (pipe_counter0 <= -10)
						{
							pipeforward0 = true;
						}

						if (pipe_counter0 > 10 && pipeHorizontalPosition == 0)
						{
							pipeHorizontalPosition = 1;
						}
					}

					///segundo cano
					if (pipeHorizontalPosition >= 1 && stop_counter > 40)
					{
						if (pipeforward1)
						{
							pipe_counter1 += 2;

						} else
						{
							pipe_counter1 -= 2;
						}

						if (pipe_counter1 >= 20)
						{
							pipeforward1 = false;
						}

						if (pipe_counter1 <= -10)
						{
							pipeforward1 = true;
						}

						if (pipe_counter1 > 10 && pipeHorizontalPosition == 1)
						{
							pipeHorizontalPosition = 2;
						}
					}

					///terceiro cano
					if (pipeHorizontalPosition >= 2 && stop_counter > 30)
					{
						if (pipeforward2)
						{
							pipe_counter2 += 2;
						} else
						{
							pipe_counter2 -= 2;
						}

						if (pipe_counter2 >= 20)
						{
							pipeforward2 = false;
						}

						if (pipe_counter2 <= -10)
						{
							pipeforward2 = true;
						}

						if (pipe_counter2 > 10 && pipeHorizontalPosition == 2)
						{
							pipeHorizontalPosition = 3;
						}
					}

					///quarto cano
					if (pipeHorizontalPosition >= 3 && stop_counter > 20)
					{
						if (pipeforward3)
						{
							pipe_counter3 += 2;
						} else
						{
							pipe_counter3 -= 2;
						}

						if (pipe_counter3 >= 20)
						{
							pipeforward3 = false;
						}

						if (pipe_counter3 <= -10)
						{
							pipeforward3 = true;
						}
					}

					///garota liberta
					if (ballGirlFreedFrame > 6)
					{
						scorePerGame[2] = (Max_Time_G3 - (Max_Time_G3 - timePerGame[2])) * 10;
						changeGameState(GAME_OVER);
					}
				}
				break;

				case INGAME_FOUR:
				{
					timePerGame[3] = 120 - (System.currentTimeMillis() - gameStartTime) / MILISECONDS_IN_A_SECOND;

					if (timePerGame[3] < 0)
					{
						timePerGame[3] = 0;
					}
//#if CONFIG == "BIG"
					if (locksToUnlock != 0 && oxygenCount >= 129)
//#endif
//#if CONFIG == "MEDIUM"
//# 					if (locksToUnlock != 0 && oxygenCount >= 95)
//#endif
//#if CONFIG == "SMALL"
//# 					if (locksToUnlock != 0 && oxygenCount >= 65)
//#endif
					{
						justWonTheMatch = false;
						changeGameState(GAME_OVER);
					}

					if (isKeyPressed(ScreenManager.FIRE) /*&& !unlockingStickActive*/)
					{
						keyStatus = 0;
						unlockingStickActive = true;
						checkLockOpen();
						//	clearAllKeys();
					} else
					{
						unlockingStickActive = false;
					}

					///incrementa o uso de oxigênio
					oxygenControl++;

					///E incrementa a barra de acordo.
//#if CONFIG == "BIG"
					if (oxygenControl % 5 == 0)
					{
						oxygenCount++;
					}

					/// Não deixa passar do limite
					if (oxygenCount >= 129)
					{
						oxygenCount = 129;
					}

//#endif

//#if CONFIG == "MEDIUM"
//# 					if (oxygenControl % 5 == 0)
//# 					{
//# 						oxygenCount++;
//# 					}
//# 
//# 					/// Não deixa passar do limite
//# 					if (oxygenCount >= 95)
//# 					{
//# 						oxygenCount = 95;
//# 					}
//# 
//#endif

//#if CONFIG == "SMALL"
//# 					if (oxygenControl % 5 == 0)
//# 					{
//# 						oxygenCount++;
//# 					}
//#
//# 					/// Não deixa passar do limite
//# 					if (oxygenCount >= 65)
//# 					{
//# 						oxygenCount = 65;
//# 					}
//#
//#endif

					int total = 0;

					if (!manIsFreed)
					{
						if (stage4JustStarted) // for creation of random locks
						{
							int check;
							stage4JustStarted = false;

							///sorteia quais pinos precisam ser empurrados
							try
							{
								check = pinsPerLock[locksToUnlock - 1];

								for (int c = 0; c < check; c++)
								{
									int num = game4Random.nextInt() % 4;

									if (num < 0)
									{
										num = -num;
									}

									lockPinState[num] = 1;
								}
							} catch (Exception e)
							{
								//#if DEBUG == "true"
								System.out.println("error here " + e.toString());
								//#endif
							}
						}

//#if CONFIG == "BIG"
						int stickLimit = 80;
//#endif

//#if CONFIG == "MEDIUM"
//# 					int stickLimit = 30;
//#endif

//#if CONFIG == "SMALL"
//# 						int stickLimit = 10;
//#endif


						///ativa a vareta
						if (!unlockingStickActive)
						{
							if (!unlockingStickMovingLeft)
							{
								unlockingStickHorizontalPosition -= unlockingStickSpeed;

								if (unlockingStickHorizontalPosition <= -50)
								{
									unlockingStickMovingLeft = true;
								}
							} else
							{
								unlockingStickHorizontalPosition += unlockingStickSpeed;

								if (unlockingStickHorizontalPosition >= stickLimit)
								{
									unlockingStickMovingLeft = false;
								}
							}
						}

						//os quatro pinos do cadeado
						for (int c = 0; c < 4; c++) /// checking 4 all locks open
						{
							if (lockPinState[c] == 1)
							{
								total++;
							}
						}

						///destravou todas
						if (total == 4)
						{
							//creio que isso seja uma inicializacão prematura para uma proxima
							//partida...
							stage4JustStarted = true;

							manJustReleasedLock = true;

							for (int c = 0; c < 4; c++)
							{
								lockPinState[c] = 0;
							}

							if (locksToUnlock >= 1)
							{
								locksToUnlock--;

								if (locksToUnlock == 2 || locksToUnlock == 1)
								{
									unlockingStickSpeed += 2;
								}

								///destravou todos
								if (locksToUnlock == 0)
								{
									manIsFreed = true;
								}
							}
							total = 0;
						}
					}

					if (manFreeingFrame > 13)
					{
						manFreeingFrame = 0;
						manJustReleasedLock = false;

						if (timePerGame[3] > 0 && locksToUnlock == 0 && oxygenCount < 129)
						{
							justWonTheMatch = true;
							scorePerGame[3] = (Max_Time_G4 - (Max_Time_G4 - timePerGame[3])) * 10;
							changeGameState(GAME_OVER);
						}
						//justWonTheMatch condition of game four
					}

				}
				break;

				case INGAME_FIVE:
				{

					leftKeyPressed = isKeyPressed(ScreenManager.LEFT);
					fireKeyPressed = isKeyPressed(ScreenManager.FIRE);
					rightKeyPressed = isKeyPressed(ScreenManager.RIGHT);


					if (updating)
					{
						highwireWalkerTimer++;
					}

					if (highwireWalkerTimer > 1010)
					{
						highwireWalkerTimer = 0;
					}

					timePerGame[4] = Max_Time_G5 - (System.currentTimeMillis() - gameStartTime) / MILISECONDS_IN_A_SECOND;

					///fim de tempo == game over
					if (timePerGame[4] < 0)
					{
						justWonTheMatch = false;
						timePerGame[4] = 0;
						changeGameState(GAME_OVER);
					}

					if (!highwireWalkerFell)
					{
						highwireWalkerVerticalPosition = (3 * highwireWalkerCenterY) / 2;

						if (highwireWalkerCenterX > (getWidth() - image_gamefive[0].getWidth() - 12) && timePerGame[4] > 0)
						{
							justWonTheMatch = true;
							scorePerGame[4] = (Max_Time_G5 - (Max_Time_G5 - timePerGame[4])) * 10;
							changeGameState(GAME_OVER);
						}

						///faz com que jogador perca o equilibrio
						if (highwireWalkerTimer % 120 == 0)
						{
							highwireWalkerIsFalling = !highwireWalkerIsFalling;
							highwireWalkerFrameCounter = 0;
						}

						if (highwireWalkerTimer % highwireWalkerBalanceRandomizer.nextInt() % 60 == 0)
						{
							highwireWalkerMovingRight = !highwireWalkerMovingRight;
							highwireWalkerFrameCounter = 0;
							highwireWalkerBalance = 0;
						}

						if (highwireWalkerTimer % 3 == 0)
						{
							highwireWalkerFrameCounter++;
						}

						if (highwireWalkerMovingRight && highwireWalkerIsFalling)
						{
							if (updating)
							{
								highwireWalkerBalance += 3;
							}

							if (highwireWalkerFrameCounter > 2)
							{
								highwireWalkerFrameCounter = 0;
							}

						} else if (highwireWalkerIsFalling)// falling back
						{

							if (updating)
							{
								highwireWalkerBalance -= 3;
							}

							if (highwireWalkerFrameCounter > 2)
							{
								highwireWalkerFrameCounter = 0;
							}

						} else if (!highwireWalkerIsFalling)// walking straight
						{
							highwireWalkerBalance = 0;

							if (highwireWalkerFrameCounter > 3)
							{
								highwireWalkerFrameCounter = 0;
							}
						}

//						 Perdeu o equilibrio
						if (highwireWalkerBalance > 40 || highwireWalkerBalance < -40)
						{
							highwireWalkerFell = true;
							highwireWalkerFrameCounter = 0;
						}
					}

					if (!(highwireWalkerFrameCounter < 5)
							&& !(highwireWalkerFrameCounter < 10)
							&& !(highwireWalkerFrameCounter < 15))
					{
						justWonTheMatch = false;
						changeGameState(GAME_OVER);
					}

					if (!highwireWalkerFell)
					{
						if (leftKeyPressed)
						{
							if (highwireWalkerIsFalling && highwireWalkerBalance > 0)
							{
								highwireWalkerBalance -= 18;
							}

							clearAllKeys();
						}
						if (rightKeyPressed)
						{
							if (highwireWalkerIsFalling && highwireWalkerBalance < 0)
							{
								highwireWalkerBalance += 18;
							}

							clearAllKeys();
						}
					}

					if (fireKeyPressed) // action key !!
					{
						if (highwireWalkerCenterX == getWidth() / 2 && skyPaintX > -getWidth() * 2)
						{
							skyPaintX--;
						} else if (highwireWalkerCenterX < getWidth() / 2 || skyPaintX <= -getWidth() * 2)
						{
							highwireWalkerCenterX++;
						}

						clearAllKeys();
					}
				}
				clearAllKeys();
				break;
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("exception in run !! currentScreen : " + currentScreen + ", exc : " + e);
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	public void paint(Graphics g)
	{

		g.translate(translate.x, translate.y);

		try
		{

			switch (currentScreen)
			{
				case TITLE_SCREEN:
					drawTitleScreen(g);
					break;
				case SOUNDENABLE_SCREEN:
					drawSoundScreen(g);
					break;

				case MAIN_MENU_SCREEN:
					drawMainMenuScreen(g);
					break;

				case HELP_SCREEN:
					drawHelpScreen(g);
					break;

				case GAME_OVER:
					drawGameOverScreen(g);
					break;
				case ABOUT_SCREEN:
					drawAboutScreen(g);
					break;
				case HIGHSCORE_SCREEN:
					drawHighscoreScreen(g);
					break;
				case PAUSE_SCREEN:
					drawPauseScreen(g);
					break;

				case CURRENT_GAME_QUIT_CONFIRM_SCREEN:
					drawCurrentGameQuitConfirmScreen(g);
					break;

				case GAME_EXIT_CONFIRM_SCREEN:
					drawGameExitConfirmScreen(g);
					break;

				case INGAME_SCREEN:
					drawIngameScreen(g);
					break;

				case INGAME_SUBMENU:
					drawInGameSubMenu(g);
					break;

				case INGAME_ONE:
					currentGame = 1;
					drawGameOne(g);
					break;

				case INGAME_TWO:
					currentGame = 2;
					drawGameTwo(g);
					break;

				case INGAME_THREE:
					currentGame = 3;
					drawGameThree(g);
					break;

				case INGAME_FOUR:
					currentGame = 4;
					drawGameFour(g);
					break;

				case INGAME_FIVE:
					currentGame = 5;
					drawGameFive(g);
					break;
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("error while painting on page : " + currentScreen + " : " + e);
			e.printStackTrace();
			//#endif
		}

		g.translate(-translate.x, -translate.y);
		g.setClip(0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
		drawBorder(g);
//#if DEBUG == "true"
		leftSoftKeyDrawable.draw(g);
		rightSoftkeyDrawable.draw(g);
//#endif
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawIngameScreen(Graphics g)
	{
		drawInGameSubMenu(g);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void initGameOne()
	{
		knifePositionRandomizer = new Random();
		timePerGame[0] = 0;
		miliseconds = 0;
		seconds = 0;
//#if DEBUG == "false"
//# 		if (!MediaPlayer.isMuted())
//# 			MediaPlayer.play(SOUND_BG, MediaPlayer.LOOP_INFINITE);
//#endif
		try
		{
			image_gameone = new Image[10];

			for (int i = 0; i < 10; i++)
			{
				image_gameone[i] = ImageLoader.loadImage("/0/" + i + ".png");
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Error in creating image.....of game one" + e);
			//#endif
		}
		clockBoxLeftCorner = new DrawableImage(image_gameone[8]);
		clockBoxMiddle = new DrawableImage(image_gameone[9]);
		clockBoxRightCorner = new DrawableImage(image_gameone[8]);
		clockBoxRightCorner.mirror(DrawableImage.TRANS_MIRROR_H);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void initGameTwo()
	{
		unloadAllInGameImages();
		image_gametwo = new Image[10];
		timePerGame[1] = 0;
//#if DEBUG == "false"
//# 		if (!MediaPlayer.isMuted())
//# 			MediaPlayer.play(SOUND_BG, MediaPlayer.LOOP_INFINITE);
//#endif

		try
		{
			for (int i = 0; i < 10; i++)
			{
				image_gametwo[i] = ImageLoader.loadImage("/1/" + i + ".png");
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Error in creating image.....of game two" + e);
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void initGameThree()
	{
		unloadAllInGameImages();
		timePerGame[2] = 0;
		image_gamethree = new Image[12];
//#if DEBUG == "false"
//# 		if (!MediaPlayer.isMuted())
//# 			MediaPlayer.play(SOUND_BG, MediaPlayer.LOOP_INFINITE);
//#endif
		try
		{
			for (int i = 0; i < 12; i++)
			{
				if (i == 5 || i == 6)
				{
					continue;
				}
				image_gamethree[i] = ImageLoader.loadImage("/2/" + i + ".png");
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Error in creating image.....of game three");
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void initGameFour()
	{
		unloadAllInGameImages();
		image_gamefour = new Image[11];
		timePerGame[3] = 0;
		fish1HorizontalPosition = getWidth() / 2;
		fish2HorizontalPosition = fish1HorizontalPosition + 20;
//#if DEBUG == "false"
//# 		if (!MediaPlayer.isMuted())
//# 			MediaPlayer.play(SOUND_BG, MediaPlayer.LOOP_INFINITE);
//#endif
		try
		{
			for (int i = 0; i < 11; i++)
			{
				image_gamefour[i] = ImageLoader.loadImage("/3/" + i + ".png");
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Error in creating image.....of game four");
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void initGameFive()
	{
		unloadAllInGameImages();
		image_gamefive = new Image[11];
		timePerGame[4] = 0;
		
		
//#if DEBUG == "false"
//# 		if (!MediaPlayer.isMuted())
//# 			MediaPlayer.play(SOUND_BG, MediaPlayer.LOOP_INFINITE);
//#endif
		try
		{
			image_gamefive[0] = ImageLoader.loadImage("/4/" + 0 + ".png");
			image_gamefive[4] = ImageLoader.loadImage("/4/" + 4 + ".png");
			image_gamefive[5] = ImageLoader.loadImage("/4/" + 5 + ".png");
			image_gamefive[8] = ImageLoader.loadImage("/4/" + 8 + ".png");
			image_gamefive[9] = ImageLoader.loadImage("/4/" + 9 + ".png");

			if (ScreenManager.getInstance().hasPointerEvents())
			{
				game5VisualJoystick = new Sprite[3];
				game5VisualJoystick[0] = new Sprite("/4/b0");
				game5VisualJoystick[1] = new Sprite("/4/b1");
				game5VisualJoystick[2] = new Sprite("/4/b2");
			} else
			{
				game5VisualJoystick = null;
			}

		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Error in creating image.....of game five");
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void unloadAllInGameImages()
	{
		image_gameone = null;
		image_gametwo = null;
		image_gamethree = null;
		image_gamefour = null;
		image_gamefive = null;
		game5VisualJoystick = null;
		clockBoxLeftCorner = null;
		clockBoxMiddle = null;
		clockBoxRightCorner = null;
		System.gc();
	}
//------------------------------------------------------------------------------
////**************************GAME ONE FUNCTION ****************************/ 	

	/**
	 *
	 * @param g
	 */
	private void drawKnife(Graphics g)
	{
//#if CONFIG != "SMALL"
		int _knife1X = 10 + 0 * getWidth() / 4;
		int _knife2X = 10 + 1 * getWidth() / 4;
		int _knife3X = 10 + 2 * getWidth() / 4;
		int _knife4X = 10 + 3 * getWidth() / 4;
//#endif



//#if CONFIG == "SMALL"
//# 		int _knife1X = 0 * getWidth() / 4;
//# 		int _knife2X = 1 * getWidth() / 4;
//# 		int _knife3X = 2 * getWidth() / 4;
//# 		int _knife4X = 3 * getWidth() / 4;
//#endif
		int _standKnifeCollisionY = getHeight() - image_gameone[6].getHeight() - image_gameone[2].getHeight();

		if (randomKnife == 1 || knife1StartPositionY > knifeStartY)
		{
			if (knife1StartPositionY >= _standKnifeCollisionY)
			{
				if (knifeDustAnim1 < 4)
				{
					g.setClip(_knife1X + 9, _standKnifeCollisionY, image_gameone[7].getWidth() / 4, image_gameone[7].getHeight());
					g.drawImage(image_gameone[7], _knife1X + 9 - knifeDustAnim1 * image_gameone[7].getWidth() / 4, _standKnifeCollisionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}
			}

			if (knife1StartPositionY == knifeStartY + 8)
			{
				g.drawImage(image_gameone[1], _knife1X + 7, knifeStartY, Graphics.TOP | Graphics.LEFT);
			}

			if (knifeDustAnim1 == 0 && knife1StartPositionY > knifeStartY)
			{
				g.drawImage(image_gameone[2], _knife1X + 10, knife1StartPositionY, Graphics.TOP | Graphics.LEFT);
			}
		}


		if (randomKnife == 2 || knife2StartPositionY > knifeStartY)
		{
			if (knife2StartPositionY >= _standKnifeCollisionY)
			{
				if (knifeDustAnim2 < 4)
				{
					g.setClip(_knife2X + 9, _standKnifeCollisionY, image_gameone[7].getWidth() / 4, image_gameone[7].getHeight());
					g.drawImage(image_gameone[7], _knife2X + 9 - knifeDustAnim2 * image_gameone[7].getWidth() / 4, _standKnifeCollisionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}
			}

			if (knife2StartPositionY == knifeStartY + 8)
			{
				g.drawImage(image_gameone[1], _knife2X + 7, knifeStartY, Graphics.TOP | Graphics.LEFT);
			}

			if (knifeDustAnim2 == 0 && knife2StartPositionY > knifeStartY)
			{
				g.drawImage(image_gameone[2], _knife2X + 10, knife2StartPositionY, Graphics.TOP | Graphics.LEFT);
			}
		}

		if (randomKnife == 3 || knife3StartPositionY > knifeStartY)
		{
			if (knife3StartPositionY >= _standKnifeCollisionY)
			{
				if (knifeDustAnim3 < 4)
				{
					g.setClip(_knife3X + 9, _standKnifeCollisionY, image_gameone[7].getWidth() / 4, image_gameone[7].getHeight());
					g.drawImage(image_gameone[7], _knife3X + 9 - knifeDustAnim3 * image_gameone[7].getWidth() / 4, _standKnifeCollisionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}
			}

			if (knife3StartPositionY == knifeStartY + 8)
			{
				g.drawImage(image_gameone[1], _knife3X + 7, knifeStartY, Graphics.TOP | Graphics.LEFT);
			}

			if (knifeDustAnim3 == 0 && knife3StartPositionY > knifeStartY)
			{
				g.drawImage(image_gameone[2], _knife3X + 10, knife3StartPositionY, Graphics.TOP | Graphics.LEFT);
			}
		}

		if (randomKnife == 4 || knife4StartPositionY > knifeStartY)
		{
			if (knife4StartPositionY >= _standKnifeCollisionY)
			{
				if (knifeDustAnim4 < 4)
				{
					g.setClip(_knife4X + 9, _standKnifeCollisionY, image_gameone[7].getWidth() / 4, image_gameone[7].getHeight());
					g.drawImage(image_gameone[7], _knife4X + 9 - knifeDustAnim4 * image_gameone[7].getWidth() / 4, _standKnifeCollisionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}
			}

			if (knife4StartPositionY == knifeStartY + 8)
			{
				g.drawImage(image_gameone[1], _knife4X + 7, knifeStartY, Graphics.TOP | Graphics.LEFT);
			}

			if (knifeDustAnim4 == 0 && knife4StartPositionY > knifeStartY)
			{
				g.drawImage(image_gameone[2], _knife4X + 10, knife4StartPositionY, Graphics.TOP | Graphics.LEFT);
			}
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawStandingPlatforms(Graphics g)
	{
		int _standToDepress = -1;
//#if CONFIG ==  "BIG"
		switch (knifeGirlPositionX)
		{
			case -60:
				_standToDepress = 0;
				break;
			case 0:
				_standToDepress = 1;
				break;
			case 60:
				_standToDepress = 2;
				break;
			case 120:
				_standToDepress = 3;
				break;
		}
//#endif

//#if CONFIG ==  "MEDIUM"
//# 		switch (knifeGirlPositionX)
//# 		{
//# 			case -42:
//# 				_standToDepress = 0;
//# 				break;
//# 			case 0:
//# 				_standToDepress = 1;
//# 				break;
//# 			case 42:
//# 				_standToDepress = 2;
//# 				break;
//# 			case 84:
//# 				_standToDepress = 3;
//# 				break;
//# 		}
//#endif

//#if CONFIG ==  "SMALL"
//# 		switch (knifeGirlPositionX)
//# 		{
//# 			case -30:
//# 				_standToDepress = 0;
//# 				break;
//# 			case 0:
//# 				_standToDepress = 1;
//# 				break;
//# 			case 30:
//# 				_standToDepress = 2;
//# 				break;
//# 			case 60:
//# 				_standToDepress = 3;
//# 				break;
//# 		}
//#endif
		for (int i = 0; i < 4; i++)
		{
			g.drawImage(image_gameone[6], (getWidth() - image_gameone[6].getWidth() * 4) / 8 + getWidth() / 4 * i, getHeight() - image_gameone[6].getHeight() - 1 + (i == _standToDepress ? 1 : 0), Graphics.TOP | Graphics.LEFT);
		}
	}
//------------------------------------------------------------------------------

	/**
	 * desenha a moça das facas
	 * @param g
	 */
	private void drawKnifeGirlJumping(Graphics g)
	{
		if (leftKeyPressed)
		{
			knifeGirlFacingLeft = true;
			if (updating)
			{
				jumpingKnifeGirlFrame++;
			}

			switch (jumpingKnifeGirlFrame)
			{
				case 1:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[4], girlStartPosition + (-girl_clipX * 5) + knifeGirlPositionX, knifeGirlPositionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 2:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 3, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[4], girlStartPosition + (-girl_clipX * 4) + knifeGirlPositionX, knifeGirlPositionY - 3, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 3:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 8, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[4], girlStartPosition + (-girl_clipX * 3) + knifeGirlPositionX, knifeGirlPositionY - 8, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 4:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 15, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[4], girlStartPosition + (-girl_clipX * 2) + knifeGirlPositionX, knifeGirlPositionY - 15, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 5:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 8, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[4], girlStartPosition + (-girl_clipX * 1) + knifeGirlPositionX, knifeGirlPositionY - 8, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 6:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[4], girlStartPosition + (-girl_clipX * 0) + knifeGirlPositionX, knifeGirlPositionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					jumpingKnifeGirlFrame = 0;
					leftKeyPressed = false;
					break;
			}
		} else if (rightKeyPressed)
		{

			knifeGirlFacingLeft = false;
			if (updating)
			{
				jumpingKnifeGirlFrame++;
			}

			switch (jumpingKnifeGirlFrame)
			{
				case 1:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[3], (girlStartPosition) + (-girl_clipX * 0) + knifeGirlPositionX, knifeGirlPositionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 2:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 3, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[3], (girlStartPosition) + (-girl_clipX * 1) + knifeGirlPositionX, knifeGirlPositionY - 3, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 3:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 8, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[3], (girlStartPosition) + (-girl_clipX * 2) + knifeGirlPositionX, knifeGirlPositionY - 8, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 4:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 15, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[3], (girlStartPosition) + (-girl_clipX * 3) + knifeGirlPositionX, knifeGirlPositionY - 15, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 5:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY - 8, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[3], (girlStartPosition) + (-girl_clipX * 4) + knifeGirlPositionX, knifeGirlPositionY - 8, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 6:
					g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlJumpStartPositionY, girl_clipX, girl_clipY);
					g.drawImage(image_gameone[3], (girlStartPosition) + (-girl_clipX * 5) + knifeGirlPositionX, knifeGirlPositionY, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					jumpingKnifeGirlFrame = 0;
					rightKeyPressed = false;
					break;
			}
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param imgx1
	 * @param imgy1
	 * @param imgw1
	 * @param imgh1
	 * @param imgx2
	 * @param imgy2
	 * @param imgw2
	 * @param imgh2
	 * @return Se as duas imagens colidem ou nao
	 */
	private boolean collision(int imgx1, int imgy1, int imgw1, int imgh1, int imgx2, int imgy2, int imgw2, int imgh2)
	{
		return (((imgx1 < imgx2) && ((imgx1 + imgw1) > imgx2)) && ((imgy1 < imgy2) && ((imgy1 + imgh1) > imgy2)))
				|| (((imgx2 < imgx1) && ((imgx2 + imgw2) > imgx1)) && ((imgy2 < imgy1) && ((imgy2 + imgh2) > imgy1)));
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameOne(Graphics g)
	{
		///desenha o fundo da fase
		g.drawImage(image_gameone[0], 0, 0, Graphics.TOP | Graphics.LEFT);

		///desenha os lançadores de facas
		for (int i = 0; i < 4; i++)
		{
			g.drawImage(image_gameone[5], /*10*/ getWidth() / 24 + i * getWidth() / 4, 9, Graphics.TOP | Graphics.LEFT);
		}


		///desenha o piso da fase
		drawStandingPlatforms(g);

		int lastPositionX = 0;
		if (clockBoxLeftCorner != null)
		{
			int oldx = translate.x;
			int oldy = translate.y;

			translate.x -= oldx;
			translate.y -= oldy;

			clockBoxLeftCorner.setPosition(0, 0);
			clockBoxLeftCorner.setClipTest(false);
			clockBoxLeftCorner.setVisible(true);
			clockBoxLeftCorner.setSize(image_gameone[8].getWidth(), image_gameone[8].getHeight());
			clockBoxLeftCorner.draw(g);

			translate.x += oldx;
			translate.y += oldy;
		}


		if (clockBoxMiddle != null)
		{
			int oldx = translate.x;
			int oldy = translate.y;

			translate.x -= oldx;
			translate.y -= oldy;


			clockBoxMiddle.setClipTest(false);
			clockBoxMiddle.setVisible(true);
			clockBoxMiddle.setSize(image_gameone[9].getWidth(), image_gameone[9].getHeight());

			for (int c = 0; c < 5; ++c)
			{
				lastPositionX = image_gameone[8].getWidth() + image_gameone[9].getWidth() * c;
				clockBoxMiddle.setPosition(lastPositionX, 0);
				clockBoxMiddle.draw(g);
			}


			translate.x += oldx;
			translate.y += oldy;
		}

		if (clockBoxRightCorner != null)
		{
			int oldx = translate.x;
			int oldy = translate.y;

			translate.x -= oldx;
			translate.y -= oldy;


			clockBoxRightCorner.setPosition(image_gameone[9].getWidth() + lastPositionX, 0);
			clockBoxRightCorner.setClipTest(false);
			clockBoxRightCorner.setVisible(true);
			clockBoxRightCorner.setSize(image_gameone[8].getWidth(), image_gameone[8].getHeight());
			clockBoxRightCorner.draw(g);

			translate.x += oldx;
			translate.y += oldy;
		}
		String timeText = "" + seconds + "." + miliseconds;
		if (timeText.length() >= 7)
		{
			timeText = timeText.substring(0, 7);
		}
		//desenha o tempo
//#if CONFIG == "SMALL"
//# 		drawFontString(g, "" + seconds + "." + miliseconds, 8, 4, 20, 2);
//#endif

//#if CONFIG != "SMALL"
		drawFontString(g, timeText, 10, 7, 20, 2);
//#endif

		if (timePerGame[0] > 120 - BRIEF_HELP_TIME || ScreenManager.getInstance().hasPointerEvents())
		{
			leftSoftKeyDrawable.setPosition(0, getHeight() - 13);
			leftSoftKeyDrawable.setSize(getWidth() / 2, 13);
			rightSoftkeyDrawable.setPosition(getWidth() / 2, getHeight() - 13);
			rightSoftkeyDrawable.setSize(getWidth() / 2, 13);

			drawFontString(g, TEXT_SK_BACK, getWidth() - getLineWidth1(getText(TEXT_SK_BACK)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		}

		if (timePerGame[0] > 120 - BRIEF_HELP_TIME)
		{
			String msg = null;
			if (ScreenManager.getInstance().hasPointerEvents())
			{
				msg = getText(TEXT_JUMPTOUCH);
			} else
			{
				msg = getText(TEXT_JUMPKEYS);
			}
			richLabel.setText(msg);
			g.setColor(107, 0, 0);
			g.fillRect(0, 32, getWidth(), richLabel.getTextTotalHeight());
			autoText(msg, MAXWIDTH, 0, 0,32,20,g);
		}

		///dispara a faca (2/2)
		if (releasingKnife)
		{
			drawKnife(g);
		}

		///mata a garota...
		if (stabbedGirl)
		{
			drawInterfaceBG(g);
		} else ///caso a moça esteja pulando, desenha a animação de pulo
		if (leftKeyPressed || rightKeyPressed)
		{
			drawKnifeGirlJumping(g);
		} ///se ela estiver parada...
		else if (!leftKeyPressed && !rightKeyPressed)
		{
			g.setClip(girlStartPosition + knifeGirlPositionX, knifeGirlPositionY, image_gameone[4].getWidth() / 6, image_gameone[4].getHeight());
			if (knifeGirlFacingLeft) ///olhando para esquerda
			{
				g.drawImage(image_gameone[4], girlStartPosition + knifeGirlPositionX + (-girl_clipX * 5), knifeGirlPositionY, Graphics.TOP | Graphics.LEFT);
			} else ///ou olhando para a direita
			{
				g.drawImage(image_gameone[3], girlStartPosition + knifeGirlPositionX, knifeGirlPositionY, Graphics.TOP | Graphics.LEFT);
			}
		}



	}
//****************************************GAME ONE MAIN FUNCTION END **************************
//*************************GAME TWO  FUNCTION ************************************// 
//------------------------------------------------------------------------------

	/**
	 * desenha o triangulo indicando o ponto certo para se comer a cobra
	 * @param g
	 */
	private void drawSnakeEatingAim(Graphics g)
	{

//#if CONFIG == "BIG"
		int startX = 0;
//#if ORIENTATION == "LANDSCAPE"
//# 		startX = (getWidth() - image_gametwo[7].getWidth()) / 2;
//#endif
		g.drawImage(image_gametwo[7], startX, getHeight() - image_gametwo[7].getHeight(), Graphics.TOP | Graphics.LEFT);
		g.drawImage(image_gametwo[4], startX + fearSnakeAmount, getHeight() - 20, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "MEDIUM"
//# 		g.drawImage(image_gametwo[7], 0, getHeight() - image_gametwo[7].getHeight(), Graphics.TOP | Graphics.LEFT);
//# 		g.drawImage(image_gametwo[4], fearSnakeAmount, getHeight() - 15, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 		g.drawImage(image_gametwo[7], 0, getHeight() - image_gametwo[7].getHeight(), Graphics.TOP | Graphics.LEFT);
//# 		g.drawImage(image_gametwo[4], fearSnakeAmount, getHeight() - 10, Graphics.TOP | Graphics.LEFT);
//#endif

	}
//------------------------------------------------------------------------------

	/**
	 * desenha a animação da mão botando a cobra na boca
	 * @param g
	 */
	private void drawHandAnimation(Graphics g)
	{
		if (updating)
		{
			snakeInHandAnimationTimeCounter++;
		}

		if (snakeInHandAnimationTimeCounter == 10)
		{
			if (updating)
			{
				snakeInHandAnimationFrame++;
			}
			snakeInHandAnimationTimeCounter = 0;
		}

		if (snakeInHandAnimationFrame < 3)
		{
//#if CONFIG == "BIG"
//#if ORIENTATION == "LANDSCAPE"
//# 			g.setClip(140, SNAKE_Y - 11, 78, 102);
//# 			g.drawImage(image_gametwo[5], 140 + (-78 * snakeInHandAnimationFrame), SNAKE_Y - 11, Graphics.TOP | Graphics.LEFT);
//#endif
//#if ORIENTATION != "LANDSCAPE"
			g.setClip(61 + 20, 95 + 20 + 40, 96, 139);
			g.drawImage(image_gametwo[5], 60 + 20 + (-96 * snakeInHandAnimationFrame), 95 + 20 + 40, Graphics.TOP | Graphics.LEFT);
//#endif
//#endif

//#if CONFIG == "MEDIUM"
//# 			g.setClip(61,95+20,76,102);
//# 			g.drawImage(image_gametwo[5],60+(-76*snakeInHandAnimationFrame),95+20, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 			g.setClip(41, 73, 50, 59);
//# 			g.drawImage(image_gametwo[5], 41 + (-50 * snakeInHandAnimationFrame), 73, Graphics.TOP | Graphics.LEFT);
//#endif
			g.setClip(0, 0, getWidth(), getHeight());
		}

		if (snakeInHandAnimationFrame == 3)
		{
			snakeInHandAnimationFrame = 0;
			snakeInHandAnimationEnded = true;
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameTwo(Graphics g)
	{
		g.setColor(0);
		g.fillRect(0, 0, getWidth(), getHeight());
		int startX = 0;
//#if ORIENTATION == "LANDSCAPE"
//# 		startX = (getWidth() - image_gametwo[7].getWidth()) / 2;
//#endif

		//game over condition
		if (timePerGame[1] <= 0)
		{
			drawInterfaceBG(g);
		}

		///desenha o rosto da vesga

//#if CONFIG == "BIG"
		g.drawImage(image_gametwo[0], 0, -8, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "MEDIUM"
//# 		g.drawImage(image_gametwo[0],0,15-7+21, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 		g.drawImage(image_gametwo[0], 0, 15, Graphics.TOP | Graphics.LEFT);
//#endif


		///desenha a mão comendo a cobra
		if (!snakeInHandAnimationEnded)
		{
			drawHandAnimation(g);
		}


		//estado do movimento dos labios
		if (lipsMoving)
		{
			if (updating)
			{
				lipAnimationFrame++;
			}


//#if CONFIG == "BIG"
			if (lipAnimationFrame == 1)
			{
//#if ORIENTATION == "LANDSCAPE"
//# //146,131
//# 				g.drawImage(image_gametwo[1], 56 + 57, 146 - 40, Graphics.TOP | Graphics.LEFT);
//#endif
//#if ORIENTATION != "LANDSCAPE"
//112,187
				g.drawImage(image_gametwo[2], 56, 146, Graphics.TOP | Graphics.LEFT);
//#endif
			}

			if (lipAnimationFrame == 2)
			{
//#if ORIENTATION == "LANDSCAPE"
//# //146,131
//# 				g.drawImage(image_gametwo[2], 56 + 57, 146 - 40, Graphics.TOP | Graphics.LEFT);
//#endif
//#if ORIENTATION != "LANDSCAPE"
//112,187
				g.drawImage(image_gametwo[1], 56, 146, Graphics.TOP | Graphics.LEFT);
//#endif
			}
//#endif

//#if CONFIG == "MEDIUM"
//# 			if (lipAnimationFrame == 1)
//# 			{
//# 				g.drawImage(image_gametwo[2], 42, 100 + 7, 20);
//# 			}
//# 
//# 			if (lipAnimationFrame == 2)
//# 			{
//# 				g.drawImage(image_gametwo[1], 42, 100 + 7, 20);
//# 			}
//#endif

//#if CONFIG == "SMALL"
//# 			if (lipAnimationFrame == 1)
//# 			{
//# 				g.drawImage(image_gametwo[2], 42, 73, 20);
//# 			}
//# 			if (lipAnimationFrame == 2)
//# 			{
//# 				g.drawImage(image_gametwo[1], 42, 73, 20);
//# 			}
//#endif


			if (lipAnimationFrame > 2)
			{
				lipAnimationFrame = 0;
				lipsMoving = false;
			}
		}

//#if CONFIG == "BIG"
		g.drawImage(image_gametwo[7], startX, getHeight() - image_gametwo[7].getHeight(), Graphics.TOP | Graphics.LEFT);
		g.drawImage(image_gametwo[9], startX + 30, 8, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "MEDIUM"
//# 	    g.drawImage(image_gametwo[7],0,getHeight()-image_gametwo[7].getHeight(), Graphics.TOP | Graphics.LEFT);
//# 		g.drawImage(image_gametwo[9],30,8, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 		g.setClip(0, 0, getWidth(), getHeight() + 10);
//# 		g.drawImage(image_gametwo[7], 0, 105 - 3, Graphics.TOP | Graphics.LEFT);
//# 		g.drawImage(image_gametwo[9], 30, 8, Graphics.TOP | Graphics.LEFT);
//#endif


		if (snakeInHandAnimationEnded)
		{

//#if CONFIG == "BIG"
			if (snakeCount < 6)
			{
				int clipW = 61;
				int clipH = 167;

//#if ORIENTATION == "LANDSCAPE"
//# 				clipW = 41;
//# 				clipH = 110;
//#endif
				g.setClip(SNAKE_X, SNAKE_Y, clipW, SNAKE_Y + clipH);
				g.drawImage(image_gametwo[3], SNAKE_X + (-clipW * snakeCount), SNAKE_Y, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());
			} else
			{
				g.drawImage(image_gametwo[0], 0, -8, Graphics.TOP | Graphics.LEFT);
			}
//#endif

//#if CONFIG == "MEDIUM"
//# 	        if(snakeCount < 6)
//# 		      {
//# 					g.setClip(SNAKE_X,SNAKE_Y,41,111);
//# 					g.drawImage(image_gametwo[3],SNAKE_X+(-41*snakeCount),SNAKE_Y,20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 		      }
//# 		      else
//# 			      g.drawImage(image_gametwo[0],0,15,20);
//#endif

//#if CONFIG == "SMALL"
//# 			if (snakeCount < 6)
//# 			{
//# 				g.setClip(SNAKE_X, SNAKE_Y, 24, 43);
//# 				g.drawImage(image_gametwo[3], SNAKE_X + (-61 * snakeCount), SNAKE_Y, Graphics.TOP | Graphics.LEFT);
//# 				g.setClip(0, 0, getWidth(), getHeight());
//# 			} else
//# 			{
//# 				g.drawImage(image_gametwo[0], 0, -8, Graphics.TOP | Graphics.LEFT);
//# 			}
//#endif
		}

		///desenha barra de enfeites superior
		g.drawImage(image_gametwo[8], startX, 0, Graphics.TOP | Graphics.LEFT);


		//desenha o marcador do ponto certo de comer a cobra
		drawSnakeEatingAim(g);

		///os diferentes estagios de se colocar uma nova cobra na boca
		if (eatingSnake)
		{
			try
			{
				if (updating)
				{
					snakeInMouthFrame++;
				}

				int stride = - 53;
//#if ORIENTATION == "LANDSCAPE"
//# 				stride = - 38;
//#endif

				switch (snakeInMouthFrame)
				{
//#if CONFIG == "BIG"
					case 1:
						g.setClip(startX + 200, 15, 100 - startX, 10);
						g.drawImage(image_gametwo[6], startX + 200 + (stride * 1), 15, Graphics.TOP | Graphics.LEFT);
						g.setClip(0, 0, getWidth(), getHeight());
						break;

					case 2:
						g.setClip(startX + 150, 15, 100 - startX, 10);
						g.drawImage(image_gametwo[6], startX + 150 + (stride * 2), 15, Graphics.TOP | Graphics.LEFT);
						g.setClip(0, 0, getWidth(), getHeight());
						break;

					case 3:
						g.setClip(startX + 100, 15, 100 - startX, 10);
						g.drawImage(image_gametwo[6], startX + 100 + (stride * 3), 15, Graphics.TOP | Graphics.LEFT);
						g.setClip(0, 0, getWidth(), getHeight());
						break;

					case 4:
						g.setClip(startX + 50, 15, 100 - startX, 10);
						g.drawImage(image_gametwo[6], startX + 50 + (stride * 4), 15, Graphics.TOP | Graphics.LEFT);
						g.setClip(0, 0, getWidth(), getHeight());
						break;

					case 5:
						g.setClip(startX + 10, 15, 100 - startX, 10);
						g.drawImage(image_gametwo[6], startX + 10 + (stride * 5), 15, Graphics.TOP | Graphics.LEFT);
						g.setClip(0, 0, getWidth(), getHeight());
						break;
//#endif

//#if CONFIG == "MEDIUM"
//# 					case 1:
//# 						g.setClip( 140,11,53,7);
//# 						g.drawImage(image_gametwo[6],140+(-53*1),11,20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//# 
//# 					case 2:
//# 						g.setClip( 110,11,53,7);
//# 						g.drawImage(image_gametwo[6],110+(-53*2),11,20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//# 
//# 					case 3:
//# 						g.setClip( 80,11,53,7);
//# 						g.drawImage(image_gametwo[6],80+(-53*3),11,20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//# 
//# 					case 4:
//# 						g.setClip( 50,11,53,7);
//# 						g.drawImage(image_gametwo[6],50+(-53*4),11,20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//# 
//# 					case 5:
//# 						g.setClip( 10,11,53,7);
//# 						g.drawImage(image_gametwo[6],10+(-53*5),11,20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//#endif

//#if CONFIG == "SMALL"
//# 					case 1:
//# 						g.setClip(90, 7, 53, 7);
//# 						g.drawImage(image_gametwo[6], 90 + (-53 * 1), 7, 20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//#
//# 					case 2:
//# 						g.setClip(55, 7, 53, 7);
//# 						g.drawImage(image_gametwo[6], 55 + (-53 * 2), 7, 20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//#
//# 					case 3:
//# 						g.setClip(50, 7, 53, 7);
//# 						g.drawImage(image_gametwo[6], 10 + (-53 * 3), 7, 20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//#
//# 					case 4:
//# 						g.setClip(30, 7, 53, 7);
//# 						g.drawImage(image_gametwo[6], 30 + (-53 * 4), 7, 20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//#
//# 					case 5:
//# 						g.setClip(10, 7, 53, 7);
//# 						g.drawImage(image_gametwo[6], 10 + (-53 * 5), 7, 20);
//# 						g.setClip(0, 0, getWidth(), getHeight());
//# 						break;
//#endif


					case 6:
						snakeInMouthFrame = 1;
						eatingSnake = false;
						break;
				}

			} catch (Exception e)
			{
			}
		}

		//desenha o relógio
		///pelo visto, o ""+valor faz o cast de valor para string. vou manter
		///simplesmente pelo trabalho infernal que daria pra trocar tudo por uma
		///forma mais adequada.
//#if CONFIG == "BIG"
//#if ORIENTATION == "LANDSCAPE"
//# 		drawFontString(g, "" + timePerGame[1], startX + 7, 7, 20, FONT_WHITE_MEDIUM);
//#else
	drawFontString(g, "" + timePerGame[1], startX + 13, 11, 20, FONT_WHITE_MEDIUM);
//#endif
//#endif

//#if CONFIG == "MEDIUM"
//# 		drawFontString(g,""+timePerGame[1] ,6,8,20,FONT_WHITE_MEDIUM);
//#endif

//#if CONFIG == "SMALL"
//# 		drawFontString(g, "" + timePerGame[1], 3, 3, 20, FONT_WHITE_MEDIUM);
//#endif

		if (timePerGame[1] > 120 - BRIEF_HELP_TIME || ScreenManager.getInstance().hasPointerEvents())
		{
			leftSoftKeyDrawable.setPosition(0, getHeight() - 13);
			leftSoftKeyDrawable.setSize(getWidth() / 2, 13);
			rightSoftkeyDrawable.setPosition(getWidth() / 2, getHeight() - 13);
			rightSoftkeyDrawable.setSize(getWidth() / 2, 13);

			drawFontString(g, TEXT_SK_BACK, getWidth() - getLineWidth1(getText(TEXT_SK_BACK)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		}

		if (timePerGame[1] > 120 - BRIEF_HELP_TIME)
		{
			String msg = null;
			if (ScreenManager.getInstance().hasPointerEvents())
			{
				msg = getText(TEXT_SWALLOWTOUCH);
			} else
			{
				msg = getText(TEXT_SWALLOWKEYS);
			}
			richLabel.setText(msg);
			g.setColor(107, 0, 0);
			g.fillRect(0, 40, getWidth(), richLabel.getTextTotalHeight());
			autoText(msg, MAXWIDTH, 0, 3,40,20,g);
		}

	}
//------------------------------------------------------------------------------

	/**
	 * Desenha a garota amarrada na fase da bola de metal
	 * @param g
	 */
	private void drawBallGirl(Graphics g)
	{
//#if CONFIG == "BIG"
		int _girlClipW = image_gamethree[7].getWidth() / ballGirlFrameCount;
		int _girlClipH = image_gamethree[7].getHeight();
//#if ORIENTATION == "LANDSCAPE"
//# 		int ballGirlPositionY = getHeight() - _girlClipH - 55;
//#endif

//#if ORIENTATION != "LANDSCAPE"
		int ballGirlPositionY = getHeight() - _girlClipH - 55;
//#endif

//#endif

//#if CONFIG == "MEDIUM"
//# 		int _girlClipW = image_gamethree[7].getWidth() / ballGirlFrameCount;
//# 		int _girlClipH = image_gamethree[7].getHeight();
//# 		int ballGirlPositionY = getHeight() - _girlClipH - 35;
//#endif

//#if CONFIG == "SMALL"
//# 		int ballGirlPositionY = 67;
//#endif



//#if CONFIG != "SMALL"
		if (!ballGirlIsUnlocked)
		{
			//desenha garota
			g.setClip(girlStartPosition + 10 + ballGirlXOffset, ballGirlPositionY + 15 + ballGirlYOffset, _girlClipW, _girlClipH);
			g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 0) + 10 + ballGirlXOffset, ballGirlPositionY + 15 + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
			g.setClip(0, 0, getWidth(), getHeight());

			///se a primeira trava estiver ativa
			if (ballGirlLock1)
			{
				girls_lock_count++;

				if (girls_lock_count < 3)
				{
					g.setClip(girlStartPosition + 10 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 10 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}

				if (girls_lock_count == 3)
				{
					ballGirlLock1 = false;
				}

			} else if (girls_lock_count <= 0)
			{
				g.setClip(girlStartPosition + 10 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
				g.drawImage(image_gamethree[10], girlStartPosition + (-_girlClipW * 0) + 10 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());
			}

			///se a segunda trava estiver ativa
			if (ballGirlLock2)
			{
				girls_lock_count++;

				if (girls_lock_count < 6)
				{
					g.setClip(girlStartPosition + 17 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 17 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}

				if (girls_lock_count == 6)
				{
					ballGirlLock2 = false;
				}

			} else if (girls_lock_count <= 3)
			{
				g.setClip(girlStartPosition + 17 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
				g.drawImage(image_gamethree[10], girlStartPosition + (-_girlClipW * 0) + 17 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());
			}

			///se a terceira trava estiver ativa
			if (ballGirlLock3)
			{
				girls_lock_count++;

				if (girls_lock_count < 9)
				{
					g.setClip(girlStartPosition + 24 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 24 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}

				if (girls_lock_count == 9)
				{
					ballGirlLock3 = false;
				}

			} else if (girls_lock_count <= 6)
			{
				g.setClip(girlStartPosition + 24 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
				g.drawImage(image_gamethree[10], girlStartPosition + (-_girlClipW * 0) + 24 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());
			}

			///se a quarta trava estiver ativa
			if (ballGirlLock4)
			{
				girls_lock_count++;

				if (girls_lock_count < 12)
				{
					g.setClip(girlStartPosition + 32 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 32 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
				}

			} else if (girls_lock_count <= 9)
			{
				g.setClip(girlStartPosition + 32 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, 13, 12);
				g.drawImage(image_gamethree[10], girlStartPosition + (-_girlClipW * 0) + 32 + ballLockXOffset + ballGirlXOffset, ballGirlPositionY + 30 + ballLockYOffset + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());
			}

		} else
		{
			///animação da garota se levantando
			if (updating)
			{
				ballGirlFreedFrame++;
			}

			switch (ballGirlFreedFrame)
			{
				case 1:
					g.setClip(girlStartPosition + 5 + ballGirlXOffset, ballGirlPositionY + 5 + ballGirlYOffset, _girlClipW, _girlClipH);
					g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 1) + 5 + ballGirlXOffset, ballGirlPositionY + 5 + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 2:
					g.setClip(girlStartPosition - 6 + ballGirlXOffset, ballGirlPositionY + ballGirlYOffset, _girlClipW, _girlClipH);
					g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 2) - 6 + ballGirlXOffset, ballGirlPositionY + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 3:
					g.setClip(girlStartPosition - 8 + ballGirlXOffset, ballGirlPositionY + 4 + ballGirlYOffset, _girlClipW, _girlClipH);
					g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 3) - 8 + ballGirlXOffset, ballGirlPositionY + 4 + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 4:
					g.setClip(girlStartPosition - 10 + ballGirlXOffset, ballGirlPositionY + 12 + ballGirlYOffset, _girlClipW, _girlClipH);
					g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 4) - 10 + ballGirlXOffset, ballGirlPositionY + 12 + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 5:
					g.setClip(girlStartPosition - 15 + ballGirlXOffset, ballGirlPositionY + 16 + ballGirlYOffset, _girlClipW, _girlClipH);
					g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 5) - 15 + ballGirlXOffset, ballGirlPositionY + 16 + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;

				case 6:
					g.setClip(girlStartPosition - 20 + ballGirlXOffset, ballGirlPositionY + 20 + ballGirlYOffset, _girlClipW, _girlClipH);
					g.drawImage(image_gamethree[7], girlStartPosition + (-_girlClipW * 6) - 20 + ballGirlXOffset, ballGirlPositionY + 20 + ballGirlYOffset, Graphics.TOP | Graphics.LEFT);
					g.setClip(0, 0, getWidth(), getHeight());
					break;
			}
		}


//#endif


//#if CONFIG == "SMALL"
//# 		if (ballGirlIsUnlocked == false)
//# 		{
//# 			g.setClip(girlStartPosition + 10, ballGirlPositionY + 15, 42, 42);
//# 			g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 0) + 10, ballGirlPositionY + 15, 20);
//# 			g.setClip(0, 0, getWidth(), getHeight());
//#
//# 			if (ballGirlLock1 == true)
//# 			{
//# 				girls_lock_count++;
//#
//# 				if (girls_lock_count < 3)
//# 				{
//# 					g.setClip(girlStartPosition + 10, ballGirlPositionY + 30, 13, 12);
//# 					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 10, ballGirlPositionY + 30, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 				}
//# 				if (girls_lock_count == 3)
//# 				{
//# 					ballGirlLock1 = false;
//# 				}
//# 			} else if (girls_lock_count <= 0)
//# 			{
//# 				g.setClip(girlStartPosition + 10, ballGirlPositionY + 30, 13, 12);
//# 				g.drawImage(image_gamethree[10], girlStartPosition + (-42 * 0) + 10, ballGirlPositionY + 30, 20);
//# 				g.setClip(0, 0, getWidth(), getHeight());
//# 			}
//#
//# 			if (ballGirlLock2 == true)
//# 			{
//# 				girls_lock_count++;
//#
//# 				if (girls_lock_count < 6)
//# 				{
//# 					g.setClip(girlStartPosition + 17, ballGirlPositionY + 30, 13, 12);
//# 					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 17, ballGirlPositionY + 30, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 				}
//#
//# 				if (girls_lock_count == 6)
//# 				{
//#
//# 					ballGirlLock2 = false;
//# 				}
//#
//# 			} else if (girls_lock_count <= 3)
//# 			{
//# 				g.setClip(girlStartPosition + 17, ballGirlPositionY + 30, 13, 12);
//# 				g.drawImage(image_gamethree[10], girlStartPosition + (-42 * 0) + 17, ballGirlPositionY + 30, 20);
//# 				g.setClip(0, 0, getWidth(), getHeight());
//#
//# 			}
//#
//# 			if (ballGirlLock3 == true)
//# 			{
//# 				girls_lock_count++;
//#
//# 				if (girls_lock_count < 9)
//# 				{
//# 					g.setClip(girlStartPosition + 24, ballGirlPositionY + 30, 13, 12);
//# 					g.drawImage(image_gamethree[10], girlStartPosition + (-12 * girls_lock_count) + 24, ballGirlPositionY + 30, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 				}
//#
//# 				if (girls_lock_count == 9)
//# 				{
//# 					ballGirlLock3 = false;
//# 				}
//# 			} else if (girls_lock_count <= 6)
//# 			{
//# 				g.setClip(girlStartPosition + 24, ballGirlPositionY + 30, 13, 12);
//# 				g.drawImage(image_gamethree[10], girlStartPosition + (-42 * 0) + 24, ballGirlPositionY + 30, 20);
//# 				g.setClip(0, 0, getWidth(), getHeight());
//# 			}
//# 		} else
//# 		{
//#
//# 			if (updating)
//# 			{
//# 				ballGirlFreedFrame++;
//# 			}
//#
//# 			switch (ballGirlFreedFrame)
//# 			{
//# 				case 1:
//# 					g.setClip(girlStartPosition + 5, ballGirlPositionY + 5, 42, 42);
//# 					g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 1) + 5, ballGirlPositionY + 5, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 					break;
//#
//# 				case 2:
//# 					g.setClip(girlStartPosition - 6, ballGirlPositionY, 42, 42);
//# 					g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 2) - 6, ballGirlPositionY, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 					break;
//#
//# 				case 3:
//# 					g.setClip(girlStartPosition - 8, ballGirlPositionY + 4, 42, 42);
//# 					g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 3) - 8, ballGirlPositionY + 4, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 					break;
//#
//# 				case 4:
//# 					g.setClip(girlStartPosition - 10, ballGirlPositionY + 12, 42, 42);
//# 					g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 4) - 10, ballGirlPositionY + 12, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 					break;
//#
//# 				case 5:
//# 					g.setClip(girlStartPosition - 15, ballGirlPositionY + 16, 42, 42);
//# 					g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 5) - 15, ballGirlPositionY + 16, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 					break;
//#
//# 				case 6:
//# 					g.setClip(girlStartPosition - 20, ballGirlPositionY + 20, 42, 42);
//# 					g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 6) - 20, ballGirlPositionY + 20, 20);
//# 					g.setClip(0, 0, getWidth(), getHeight());
//# 					break;
//# 			}
//# 		}
//#endif

		///garota liberta
		if (ballGirlFreedFrame > 6)
		{
			g.setClip(girlStartPosition - 20, ballGirlPositionY + 20, 42, 42);
			g.drawImage(image_gamethree[7], girlStartPosition + (-42 * 6) - 20, ballGirlPositionY + 20, 20);
			g.setClip(0, 0, getWidth(), getHeight());
		}
	}
//------------------------------------------------------------------------------

	/**
	 * Animação + processamento da corrente + bola de ferro
	 * @param g
	 */
	private void drawBallAndChainAnimation(Graphics g)
	{
//#if CONFIG == "BIG"
		for (int i = 0; i < 15; i++)
		{
			g.drawImage(image_gamethree[8], getWidth() / 2 - 2, -120 - ((15 - 9) * 20) + ironBallHeight + i * 20, Graphics.TOP | Graphics.LEFT);
		}

		g.drawImage(image_gamethree[9], getWidth() / 2 - image_gamethree[9].getWidth() / 2, ironBallHeight + 30, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "MEDIUM"
//# 		for (int i = 0; i < 9; i++)
//# 		{
//# 			g.drawImage(image_gamethree[8], getWidth() / 2 - 2, -120 + ironBallHeight + i * 20, Graphics.TOP | Graphics.LEFT);
//# 		}
//# 
//# 		g.drawImage(image_gamethree[9], getWidth() / 2 - image_gamethree[9].getWidth() / 2, ironBallHeight + 30, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 		for (int i = 0; i < 4; i++)
//# 		{
//# 			g.drawImage(image_gamethree[8], getWidth() / 2 - 2, ironBallHeight - i * 20, Graphics.TOP | Graphics.LEFT);
//# 		}
//#
//# 		g.drawImage(image_gamethree[9], getWidth() / 2 - 12, ironBallHeight + 20, Graphics.TOP | Graphics.LEFT);
//#endif
	}
//------------------------------------------------------------------------------

	/**
	 * desenha a animacão dos canos
	 * @param g
	 */
	private void drawPipesAnimation(Graphics g)
	{

//#if CONFIG == "BIG"
		///tamanho em Y da bitola da trava do cano (porque precisa?)
		int _pipeYGap = 23;


		//calculo + animacão da corrente + bola de ferro
		drawBallAndChainAnimation(g);

		//desenha os canos em sí
		g.drawImage(image_gamethree[3], -21 - 11 + pipe_counter0, getHeight() / 2 + 5 + 25 + _pipeYGap * 0, Graphics.TOP | Graphics.LEFT);//left pipe 1
		g.drawImage(image_gamethree[2], getWidth() - 13 - 7 - 10 - pipe_counter0, getHeight() / 2 + 5 + 25 + _pipeYGap * 0, Graphics.TOP | Graphics.LEFT);//right pipe 1

		g.drawImage(image_gamethree[3], -21 - 11 + pipe_counter1, getHeight() / 2 + 5 + 25 + _pipeYGap * 1, Graphics.TOP | Graphics.LEFT);//left pipe 1
		g.drawImage(image_gamethree[2], getWidth() - 13 - 7 - 10 - pipe_counter1, getHeight() / 2 + 5 + 25 + _pipeYGap * 1, Graphics.TOP | Graphics.LEFT);//right pipe 1

		g.drawImage(image_gamethree[3], -21 - 11 + pipe_counter2, getHeight() / 2 + 5 + 25 + _pipeYGap * 2, Graphics.TOP | Graphics.LEFT);//left pipe 1
		g.drawImage(image_gamethree[2], getWidth() - 13 - 7 - 10 - pipe_counter2, getHeight() / 2 + 5 + 25 + _pipeYGap * 2, Graphics.TOP | Graphics.LEFT);//right pipe 1

		g.drawImage(image_gamethree[3], -21 - 11 + pipe_counter3, getHeight() / 2 + 5 + 25 + _pipeYGap * 3, Graphics.TOP | Graphics.LEFT);//left pipe 1
		g.drawImage(image_gamethree[2], getWidth() - 13 - 7 - 10 - pipe_counter3, getHeight() / 2 + 5 + 25 + _pipeYGap * 3, Graphics.TOP | Graphics.LEFT);//right pipe 1

		///desenha os suportes dos canos
		g.drawImage(image_gamethree[1], 3, getHeight() - image_gamethree[1].getHeight() - 10, Graphics.TOP | Graphics.LEFT);//vertical pipe
		g.drawImage(image_gamethree[1], getWidth() - image_gamethree[1].getWidth() - 3, getHeight() - image_gamethree[1].getHeight() - 10, Graphics.TOP | Graphics.LEFT);//verticle pipe


		///desenha as correntes que se ligam nos pinos de trava
		for (int i = 0; i < 12; i++)
		{
			g.drawImage(image_gamethree[8], 21, -100 + pipeLockPositionY + i * 20, Graphics.TOP | Graphics.LEFT);
			g.drawImage(image_gamethree[8], getWidth() - 15 - 8, -100 + pipeLockPositionY + i * 20, Graphics.TOP | Graphics.LEFT);
		}

		///desenha os pinos de trava nos canos
		g.drawImage(image_gamethree[4], 16, getHeight() / 2 - 90 + 16 + 12 + pipeLockPositionY, Graphics.TOP | Graphics.LEFT);
		g.drawImage(image_gamethree[4], getWidth() - 29, getHeight() / 2 - 90 + 16 + 12 + pipeLockPositionY, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "MEDIUM"
//# 			///tamanho em Y da bitola da trava do cano (porque precisa?)
//# 			int _pipeYGap = 23;
//# 
//# 			//calculo + animacão da corrente + bola de ferro
//# 	 		drawBallAndChainAnimation(g);
//# 			g.drawImage(image_gamethree[3],-21+pipe_counter0,getHeight()/2+5+_pipeYGap*0,20);//left pipe 1
//# 			g.drawImage(image_gamethree[2],getWidth()-13-10-pipe_counter0,getHeight()/2+5+_pipeYGap*0,20);//right pipe 1
//# 			g.drawImage(image_gamethree[3],-21+pipe_counter1,getHeight()/2+5+_pipeYGap*1,20);//left pipe 1
//# 			g.drawImage(image_gamethree[2],getWidth()-13-10-pipe_counter1,getHeight()/2+5+_pipeYGap*1,20);//right pipe 1
//# 			g.drawImage(image_gamethree[3],-21+pipe_counter2,getHeight()/2+5+_pipeYGap*2,20);//left pipe 1
//# 			g.drawImage(image_gamethree[2],getWidth()-13-10-pipe_counter2,getHeight()/2+5+_pipeYGap*2,20);//right pipe 1
//# 			g.drawImage(image_gamethree[3],-21+pipe_counter3,getHeight()/2+5+_pipeYGap*3,20);//left pipe 1
//# 			g.drawImage(image_gamethree[2],getWidth()-13-10-pipe_counter3,getHeight()/2+5+_pipeYGap*3,20);//right pipe 1
//# 			g.drawImage(image_gamethree[1],3,getHeight()-image_gamethree[1].getHeight()-10,20);//vertical pipe
//# 			g.drawImage(image_gamethree[1],getWidth()-image_gamethree[1].getWidth()-3,getHeight()-image_gamethree[1].getHeight()-10,20);//verticle pipe
//# 
//# 			for(int i=0;i<8;i++)
//# 			{
//# 			  g.drawImage(image_gamethree[8],21, -100+pipeLockPositionY+i*20,20);
//# 			  g.drawImage(image_gamethree[8],getWidth()-15-8,-100+pipeLockPositionY+i*20,20);
//# 			}
//# 
//# 			g.drawImage(image_gamethree[4],19, getHeight()/2-90+16+12+pipeLockPositionY,20);
//# 			g.drawImage(image_gamethree[4],getWidth()-15-11,getHeight()/2-90+16+12+pipeLockPositionY,20);
//#endif

//#if CONFIG == "SMALL"
//#
//# 		//calculo + animacão da corrente + bola de ferro
//# 		drawBallAndChainAnimation(g);
//# 		g.drawImage(image_gamethree[3], -18 + pipe_counter0, 72, 20);//left pipe 1
//# 		g.drawImage(image_gamethree[2], 105 - pipe_counter0, 72, 20);//right pipe 1
//#
//# 		g.drawImage(image_gamethree[3], -18 + pipe_counter1, 83, 20);//left pipe 1
//# 		g.drawImage(image_gamethree[2], 105 - pipe_counter1, 83, 20);//right pipe 1
//#
//# 		g.drawImage(image_gamethree[3], -18 + pipe_counter2, 95, 20);//left pipe 1
//# 		g.drawImage(image_gamethree[2], 105 - pipe_counter2, 95, 20);//right pipe 1
//#
//# 		g.drawImage(image_gamethree[3], -18 + pipe_counter3, 105, 20);//left pipe 1
//# 		g.drawImage(image_gamethree[2], 105 - pipe_counter3, 105, 20);//right pipe 1
//#
//# 		g.drawImage(image_gamethree[1], 3, 50, 20);//vertical pipe
//# 		g.drawImage(image_gamethree[1], 115, 50, 20);//verticle pipe
//#
//# 		for (int i = 0; i < 4; i++)
//# 		{
//# 			g.drawImage(image_gamethree[8], 20, 2 + pipeLockPositionY - i * 20, 20);
//# 			g.drawImage(image_gamethree[8], 107, 2 + pipeLockPositionY - i * 20, 20);
//# 		}
//# 		g.drawImage(image_gamethree[4], 17, 12 + pipeLockPositionY, 20);
//# 		g.drawImage(image_gamethree[4], 104, 12 + pipeLockPositionY, 20);
//#endif
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameThree(Graphics g)
	{
		//desenha o fundo da fase
		g.drawImage(image_gamethree[0], 0, 0, Graphics.TOP | Graphics.LEFT);

		///anima os canos
		drawPipesAnimation(g);

		///desenha a garota
		drawBallGirl(g);

		if (timePerGame[2] > 120 - BRIEF_HELP_TIME || ScreenManager.getInstance().hasPointerEvents())
		{
			leftSoftKeyDrawable.setPosition(0, getHeight() - 13);
			leftSoftKeyDrawable.setSize(getWidth() / 2, 13);
			rightSoftkeyDrawable.setPosition(getWidth() / 2, getHeight() - 13);
			rightSoftkeyDrawable.setSize(getWidth() / 2, 13);

			drawFontString(g, TEXT_SK_BACK, getWidth() - getLineWidth1(getText(TEXT_SK_BACK)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		}

		if (timePerGame[2] > 120 - BRIEF_HELP_TIME)
		{
			String msg = null;
			if (ScreenManager.getInstance().hasPointerEvents())
			{
				msg = getText(TEXT_BALLTOUCH);
			} else
			{
				msg = getText(TEXT_BALLKEYS);
			}
			richLabel.setText(msg);
			g.setColor(107, 0, 0);
			g.fillRect(0, 40, getWidth(), richLabel.getTextTotalHeight());
			autoText(msg, MAXWIDTH, 0, 3,40,20,g);
		}

	}
//------------------------------------------------------------------------------

	/**
	 * desenha (e processa) os peixes do jogo 4
	 * @param g
	 */
	private void drawFish(Graphics g)
	{
//#if ORIENTATION == "LANDSCAPE"
//# 		if ((fish1HorizontalPosition >= (getWidth() / 2) - 100) && (fish1HorizontalPosition <= (getWidth() / 2) + 100) && manJustReleasedLock)
//#else
		if ((fish1HorizontalPosition >= (getWidth() / 2) - 10 - 15) && (fish1HorizontalPosition <= (getWidth() / 2) - 10 + 15) && manJustReleasedLock)
//#endif
		{
			fish1SwimPaceController = 50;
		}

		if (fish1SwimPaceController > 0)
		{
			if (updating)
			{
				fish1SwimPaceController--;
			}
		}

		if (!manJustReleasedLock)
		{
			if (updating)
			{
				fish2SwimPaceController++;
			}

			if (fish1MovingRight)
			{
				if (fish1SwimPaceController == 0)
				{
					if (updating)
					{
						fish1HorizontalPosition++;
					}
					g.drawImage(image_gamefour[3], fish1HorizontalPosition, FISH_ONE_Y, Graphics.TOP | Graphics.LEFT);
				} else
				{
					if (fish1SwimPaceController % 2 == 0)
					{
						g.drawImage(image_gamefour[2], fish1HorizontalPosition, FISH_ONE_Y, Graphics.TOP | Graphics.LEFT);
					} else
					{
						g.drawImage(image_gamefour[3], fish1HorizontalPosition, FISH_ONE_Y, Graphics.TOP | Graphics.LEFT);
					}
				}
			} else
			{
				if (updating)
				{
					fish1HorizontalPosition--;
				}
				g.drawImage(image_gamefour[2], fish1HorizontalPosition, FISH_ONE_Y, Graphics.TOP | Graphics.LEFT);
			}
//#if CONFIG == "BIG"
//#if ORIENTATION == "LANDSCAPE"
//# 			if (fish1HorizontalPosition >= getWidth() - 100)
//#else
			if (fish1HorizontalPosition >= getWidth() - 60)
//#endif
//#endif
//#if CONFIG != "BIG"
//# 			if (fish1HorizontalPosition >= 95)
//#endif
			{
				fish1MovingRight = false;
			}

//#if ORIENTATION == "LANDSCAPE"
//# 			if (fish1HorizontalPosition <= 70)
//#else
			if (fish1HorizontalPosition <= 15)
//#endif
			{
				fish1MovingRight = true;
			}

			if (fish2MovingRight)
			{
				if (fish2SwimPaceController % 2 == 0)
				{
					if (updating)
					{
						fish2HorizontalPosition++;
					}
				}

				g.drawImage(image_gamefour[3], fish2HorizontalPosition, FISH_TWO_Y, Graphics.TOP | Graphics.LEFT);
			} else
			{
				if (updating)
				{
					fish2HorizontalPosition--;
				}
				g.drawImage(image_gamefour[2], fish2HorizontalPosition, FISH_TWO_Y, Graphics.TOP | Graphics.LEFT);
			}

			//gameover
//#if CONFIG == "BIG"
//#if ORIENTATION == "LANDSCAPE"
//# 			if (fish2HorizontalPosition >= getWidth() - 100)
//#else
			if (fish2HorizontalPosition >= getWidth() - 60)
//#endif
//#endif
//#if CONFIG != "BIG"
//# 			if (fish2HorizontalPosition >= getWidth() - 95)
//#endif
			{
				fish2MovingRight = false;
			}

			//gameover
//#if ORIENTATION == "LANDSCAPE"
//# 			if (fish2HorizontalPosition <= 70)
//#else
			if (fish2HorizontalPosition <= 15)
//#endif
			{
				fish2MovingRight = true;
			}
		}

		///movimenta (e reinicia) as bolhas
		if (bubble_y < 0)
		{
			bubble_y = getHeight() / 3;
			bubble_x = game4Random.nextInt() % 90;

			if (bubble_x < 20)
			{
				bubble_x = getWidth() / 2;
			}

		} else
		{
			bubble_y--;
		}

		//desenha a bolha
		g.drawImage(image_gamefour[10], bubble_x, bubble_y, Graphics.TOP | Graphics.LEFT);

	}
//------------------------------------------------------------------------------

	public void checkLockOpen()
	{
		for (int c = 0; c < 4; c++)
		{
//#if CONFIG == "BIG"
			if (unlockingStickHorizontalPosition + 71 >= 33 + (29 * c) && unlockingStickHorizontalPosition + 71 <= 40 + (29 * c) && lockPinState[c] == 0)
//#endif
//#if CONFIG == "MEDIUM"
//# 			if( unlockingStickHorizontalPosition+71 >= 24+(21*c) && unlockingStickHorizontalPosition+71<=31+(21*c) && lockPinState[c]==0)
//#endif
//#if CONFIG == "SMALL"
//# 			if (unlockingStickHorizontalPosition + 71 >= 20 + (17 * c) && unlockingStickHorizontalPosition + 71 <= 27 + (17 * c) && lockPinState[c] == 0)
//#endif
			{
				lockPinState[c] = 1;
			}
		}
	}
//------------------------------------------------------------------------------

	/**
	 * desenha + processa o interior do cadeado
	 * @param g
	 */
	private void drawLockInside(Graphics g)
	{
		if (!manIsFreed)
		{
			//desenha o envolucro do cadeado
//#if CONFIG == "BIG"
			g.drawImage(image_gamefour[4], 0, getHeight() - image_gamefour[4].getHeight(), Graphics.TOP | Graphics.LEFT);
			///ativa a vareta
			if (!unlockingStickActive)
			{
				g.setClip(unlockingStickHorizontalPosition, getHeight() - image_gamefour[5].getHeight() - 8, 71, 11);
				g.drawImage(image_gamefour[5], unlockingStickHorizontalPosition,/*100*/ getHeight() - image_gamefour[5].getHeight() - 8, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, getHeight() - image_gamefour[5].getHeight() - 8, unlockingStickHorizontalPosition, 11);

				if (unlockingStickHorizontalPosition > 67)
				{
					g.setClip(0, getHeight() - image_gamefour[5].getHeight() - 8, unlockingStickHorizontalPosition - 20, 11);
				}

				g.drawImage(image_gamefour[5], 0, getHeight() - image_gamefour[5].getHeight() - 8, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, getHeight() - image_gamefour[5].getHeight() - 8, unlockingStickHorizontalPosition, 11);
				g.drawImage(image_gamefour[5], 30, getHeight() - image_gamefour[5].getHeight() - 8, Graphics.TOP | Graphics.LEFT);

			} else
			{
				g.setClip(unlockingStickHorizontalPosition, getHeight() - image_gamefour[5].getHeight() - 8, 71, 10);
				g.drawImage(image_gamefour[5], unlockingStickHorizontalPosition - 71, getHeight() - image_gamefour[5].getHeight() - 8, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, getHeight() - image_gamefour[5].getHeight() - 8, unlockingStickHorizontalPosition, 11);

				if (unlockingStickHorizontalPosition > 67)
				{
					g.setClip(0, getHeight() - image_gamefour[5].getHeight() - 8, unlockingStickHorizontalPosition - 20, 11);
				}

				g.drawImage(image_gamefour[5], 0, getHeight() - image_gamefour[5].getHeight() - 8, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, getHeight() - image_gamefour[5].getHeight() - 8, unlockingStickHorizontalPosition, 11);
				g.drawImage(image_gamefour[5], 30, getHeight() - image_gamefour[5].getHeight() - 8, Graphics.TOP | Graphics.LEFT);
			}
			g.setClip(0, 0, getWidth(), getHeight());
//#endif

//#if CONFIG == "MEDIUM"
//# 	 			g.drawImage(image_gamefour[4], 0, getHeight() - image_gamefour[4].getHeight(), Graphics.TOP | Graphics.LEFT);
//# 
//# 				if(!unlockingStickActive)
//# 				{
//# 					g.setClip(unlockingStickHorizontalPosition,getHeight()-image_gamefour[5].getHeight()-5,71,11);
//# 					g.drawImage(image_gamefour[5],unlockingStickHorizontalPosition,getHeight()-image_gamefour[5].getHeight()-5,20);
//# 					g.setClip(0,unlockingStickHorizontalPosition-image_gamefour[5].getHeight()-5,unlockingStickHorizontalPosition,11);
//# 					g.drawImage(image_gamefour[5],0,getHeight()-image_gamefour[5].getHeight()-5,20);
//# 				}
//# 				else
//# 				{
//# 					g.setClip(unlockingStickHorizontalPosition,getHeight()-image_gamefour[5].getHeight()-5,71,10);
//# 					g.drawImage(image_gamefour[5],unlockingStickHorizontalPosition-71,getHeight()-image_gamefour[5].getHeight()-5,20);
//# 					g.setClip(0,unlockingStickHorizontalPosition-image_gamefour[5].getHeight()-5,unlockingStickHorizontalPosition,11);
//# 					g.drawImage(image_gamefour[5],0,getHeight()-image_gamefour[5].getHeight()-5,20);
//# 				}
//# 				g.setClip(0, 0, getWidth(), getHeight());
//#endif

//#if CONFIG == "SMALL"
//# 			g.drawImage(image_gamefour[4], 0, 90, 20);
//#
//# 			if (!unlockingStickActive)
//# 			{
//# 				g.setClip(unlockingStickHorizontalPosition, 100, 71, 10);
//# 				g.drawImage(image_gamefour[5], unlockingStickHorizontalPosition, 100, 20);
//# 			} else
//# 			{
//# 				g.setClip(unlockingStickHorizontalPosition, 100, 71, 10);
//# 				g.drawImage(image_gamefour[5], unlockingStickHorizontalPosition - 71, 100, 20);
//# 			}
//# 			g.setClip(0, 0, getWidth(), getHeight());
//#endif

			///desenha os pinos
			for (int c = 0; c < 4; c++)
			{
				if (lockPinState[c] == 0)
//#if CONFIG == "BIG"				
				{
					g.drawImage(image_gamefour[6], 33 + (29 * c), getHeight() - image_gamefour[4].getHeight() + 2 + 4, Graphics.TOP | Graphics.LEFT);
				} else
				{
					g.drawImage(image_gamefour[6], 33 + (29 * c), getHeight() - image_gamefour[4].getHeight() + 2, Graphics.TOP | Graphics.LEFT);
				}
			}

			///quantos cadeados faltam destravar
			g.setColor(0);
//#if ORIENTATION == "LANDSCAPE"
//# 			g.drawString("" + locksToUnlock, getWidth() - 128, getHeight() - 22, Graphics.TOP | Graphics.LEFT);
//#else
		g.drawString("" + locksToUnlock, getWidth() - 52, getHeight() - 22, Graphics.TOP | Graphics.LEFT);
//#endif
			g.setColor(1);
//#endif
//#if CONFIG == "MEDIUM"				
//# 					g.drawImage(image_gamefour[6],24+(21*c),getHeight()-image_gamefour[4].getHeight()+1+4,20);
//# 				else
//# 					g.drawImage(image_gamefour[6],24+(21*c),getHeight()-image_gamefour[4].getHeight()+1,20);
//# 			}
//# 
//# 		///quantos cadeados faltam destravar
//# 		g.setColor(0);
//# 		g.drawString(""+locksToUnlock,getWidth()-37,getHeight()-20, Graphics.TOP | Graphics.LEFT);
//# 		g.setColor(1);
//#endif
//#if CONFIG == "SMALL"
//# 				{
//# 					g.drawImage(image_gamefour[6], 21 + (17 * c), 95, Graphics.TOP | Graphics.LEFT);
//# 				} else
//# 				{
//# 					g.drawImage(image_gamefour[6], 21 + (17 * c), 92, Graphics.TOP | Graphics.LEFT);
//# 				}
//# 			}
//# 			g.setColor(0);
//# 			g.drawString("" + locksToUnlock, 110, 96, 20);
//# 			g.setColor(1);
//#endif
		}
	}
//-----------------------------------------------------------------------------

	/**
	 * Desenha medidor de oxigênio
	 * @param g
	 */
	private void drawOxygenMeter(Graphics g)
	{
//#if CONFIG == "BIG"
		g.setColor(0, 67, 82);
		g.fillRect(getWidth() - image_gamefour[1].getWidth() + 7, 51 + 49, 13, 1 + oxygenCount);
//#endif

//#if CONFIG == "MEDIUM"
//# 		g.setColor(0,67,82);
//# 		g.fillRect(getWidth()-image_gamefour[1].getWidth()+5,36+25+16,10,7+1+oxygenCount);
//#endif

//#if CONFIG == "SMALL"
//# 		g.setColor(255, 0, 0);
//# 		g.fillRect(116, 35, 5, 66);
//# 		g.setColor(192, 192, 192);
//# 		g.fillRect(116, 35, 5, 1 + oxygenCount);
//#endif
	}
//------------------------------------------------------------------------------

	/**
	 * desenha as frames dos homem se libertando dos cadeados
	 * @param g
	 */
	private void drawLockReleasingMan(Graphics g)
	{
		if (manJustReleasedLock)
		{
			if (updating)
			{
				manFreeingFrame++;
			}

//#if CONFIG == "BIG"
			if (manFreeingFrame <= 3 || locksToUnlock == 3 || locksToUnlock == 2 || locksToUnlock == 1)
			{
				switch (manFreeingFrame)
				{
					case 1:
						g.drawImage(image_gamefour[8], (getWidth() / 2) - 5 - 3, -10, Graphics.TOP | Graphics.LEFT);
						g.drawImage(image_gamefour[7], (getWidth() / 2) - 10 - 3, getHeight() / 3, Graphics.TOP | Graphics.LEFT);
						break;
					case 2:
						g.drawImage(image_gamefour[8], (getWidth() / 2) - 5 - 6, -10, Graphics.TOP | Graphics.LEFT);
						g.drawImage(image_gamefour[7], (getWidth() / 2) - 10 - 6, getHeight() / 3, Graphics.TOP | Graphics.LEFT);
						break;
					case 3:
					{
						g.drawImage(image_gamefour[8], (getWidth() / 2) - 5, -10, Graphics.TOP | Graphics.LEFT);
						g.drawImage(image_gamefour[7], (getWidth() / 2) - 10, (getHeight() / 3), Graphics.TOP | Graphics.LEFT);

						if (manFreeingFrame >= 3 && !manIsFreed)
						{
							manFreeingFrame = 0;
							manJustReleasedLock = false;
						}
					}
					break;
				}
			}

			///animação do homem se libertando
			if (manFreeingFrame > 3 || locksToUnlock == 0)
			{
				switch (manFreeingFrame)
				{
					case 4:
						g.drawImage(image_gamefour[8], (getWidth() / 2 - 5), -10, Graphics.TOP | Graphics.LEFT);
						g.setClip((getWidth() / 2 - 10), 40 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 - 10), 40 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 5:
						g.setClip((getWidth() / 2 - 10), 50 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 - 10) - 85, 50 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 6:
						g.setClip((getWidth() / 2 - 10), 65 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 - 10) - 85 * 2, 65 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 7:
						g.setClip((getWidth() / 2 - 15), 65 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 - 15) - 85 * 3, 65 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 8:
						g.setClip((getWidth() / 2), 70 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2) - 85 * 4, 70 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 9:
						g.setClip((getWidth() / 2 + 10), 60 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 + 10) - 85 * 5, 60 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 10:
						g.setClip((getWidth() / 2 + 10), 40 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 + 10) - 85 * 5, 40 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 11:
						g.setClip((getWidth() / 2 + 13), 20 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 + 13) - 85 * 5, 20 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 12:
						g.setClip((getWidth() / 2 + 23), 0 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 + 23) - 85 * 5, 0 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
					case 13:
						g.setClip((getWidth() / 2 + 30), -35 + 48 + 55, MAN_W, MAN_H);
						g.drawImage(image_gamefour[9], (getWidth() / 2 + 30) - 85 * 5, -35 + 48 + 55, Graphics.TOP | Graphics.LEFT);
						break;
				}
			}

			if (manFreeingFrame > 13)
			{
				if (timePerGame[3] > 0 && locksToUnlock == 0 && oxygenCount < 129)
//#endif
//#if CONFIG == "MEDIUM"
//# 	   		if (manFreeingFrame <= 3 || locksToUnlock == 3 || locksToUnlock == 2 || locksToUnlock == 1)
//# 			{
//# 				switch (manFreeingFrame)
//# 				{
//# 					case 1:
//# 						g.drawImage(image_gamefour[8],(getWidth()/2-5)-3,-10,Graphics.TOP | Graphics.LEFT);
//# 						g.drawImage(image_gamefour[7],(getWidth()/2-10)-3,getHeight()/3,Graphics.TOP | Graphics.LEFT);
//# 						break;
//# 					case 2:
//# 						g.drawImage(image_gamefour[8],(getWidth()/2-5)-6,-10,Graphics.TOP | Graphics.LEFT);
//# 						g.drawImage(image_gamefour[7],(getWidth()/2-10)-6,getHeight()/3,Graphics.TOP | Graphics.LEFT);
//# 						break;
//# 					case 3:
//# 					{
//# 						g.drawImage(image_gamefour[8],(getWidth()/2-5),-10,Graphics.TOP | Graphics.LEFT);
//# 						g.drawImage(image_gamefour[7],(getWidth()/2-10),getHeight()/3,Graphics.TOP | Graphics.LEFT);
//# 
//# 						if (manFreeingFrame >= 3 && !manIsFreed)
//# 						{
//# 							manFreeingFrame = 0;
//# 							manJustReleasedLock = false;
//# 						}
//# 					}
//# 					break;
//# 				}
//# 			}
//# 
//# 			///animação do homem se libertando
//# 			if (manFreeingFrame > 3 || locksToUnlock == 0)
//# 			{
//# 				switch (manFreeingFrame)
//# 				{
//# 					case 4:
//# 						g.drawImage(image_gamefour[8],(getWidth()/2-5),-10,20);
//# 						g.setClip((getWidth()/2-10),40+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2-10),40+48,20);
//# 						break;
//# 					case 5:
//# 						g.setClip((getWidth()/2-10),50+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2-10)-/*41*/63,50+48,20);
//# 						break;
//# 					case 6:
//# 						g.setClip((getWidth()/2-10),65+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2-10)-/*41*/63*2,65+48,20);
//# 						break;
//# 					case 7:
//# 						g.setClip((getWidth()/2-15),65+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2-15)-/*41*/63*3,65+48,20);
//# 						break;
//# 					case 8:
//# 						g.setClip((getWidth()/2),70+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2)-/*41*/63*4,70+48,20);
//# 						break;
//# 					case 9:
//# 						g.setClip((getWidth()/2+10),60+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2+10)-/*41*/63*5,60+48,20);
//# 						break;
//# 					case 10:
//# 						g.setClip((getWidth()/2+10),40+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2+10)-/*41*/63*5,40+48,20);
//# 						break;
//# 					case 11:
//# 						g.setClip((getWidth()/2+13),20+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2+13)-/*41*/63*5,20+48,20);
//# 						break;
//# 					case 12:
//# 						g.setClip((getWidth()/2+23),0+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2+23)-/*41*/63*5,0+48,20);
//# 						break;
//# 					case 13:
//# 						g.setClip((getWidth()/2+30),-35+48,MAN_W,MAN_H);
//# 						g.drawImage(image_gamefour[9],(getWidth()/2+30)-/*41*/63*5,-35+48,20);
//# 						break;
//# 				}
//# 			}
//# 
//# 			if (manFreeingFrame > 13)
//# 			{
//# 				if (timePerGame[3] > 0 && locksToUnlock == 0 && oxygenCount < 95)
//#endif
//#if CONFIG == "SMALL"
//# 			if (manFreeingFrame <= 3 || locksToUnlock == 3 || locksToUnlock == 2 || locksToUnlock == 1)
//# 			{
//# 				switch (manFreeingFrame)
//# 				{
//# 					case 1:
//# 						g.drawImage(image_gamefour[8], (getWidth() / 2 - 5) - 3, -10, 20);
//# 						g.drawImage(image_gamefour[7], (getWidth() / 2 - 10) - 3, 30, 20);
//# 						break;
//# 					case 2:
//# 						g.drawImage(image_gamefour[8], (getWidth() / 2 - 5) - 6, -10, 20);
//# 						g.drawImage(image_gamefour[7], (getWidth() / 2 - 10) - 6, 30, 20);
//# 						break;
//# 					case 3:
//# 					{
//# 						g.drawImage(image_gamefour[8], (getWidth() / 2 - 5), -10, 20);
//# 						g.drawImage(image_gamefour[7], (getWidth() / 2 - 10), 30, 20);
//#
//# 						if (manFreeingFrame >= 3 && !manIsFreed)
//# 						{
//# 							manFreeingFrame = 0;
//# 							manJustReleasedLock = false;
//# 						}
//# 					}
//# 					break;
//# 				}
//# 			}
//#
//# 			///animação do homem se libertando
//# 			if (manFreeingFrame > 3 || locksToUnlock == 0)
//# 			{
//# 				switch (manFreeingFrame)
//# 				{
//# 					case 4:
//# 						g.drawImage(image_gamefour[8], (getWidth() / 2 - 5), -10, 20);
//# 						g.setClip((getWidth() / 2 - 10), 40, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 - 10), 40, 20);
//# 						break;
//# 					case 5:
//# 						g.setClip((getWidth() / 2 - 10), 50, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 - 10) - 41, 50, 20);
//# 						break;
//# 					case 6:
//# 						g.setClip((getWidth() / 2 - 10), 65, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 - 10) - 41 * 2, 65, 20);
//# 						break;
//# 					case 7:
//# 						g.setClip((getWidth() / 2 - 15), 65, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 - 15) - 41 * 3, 65, 20);
//# 						break;
//# 					case 8:
//# 						g.setClip((getWidth() / 2), 70, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2) - 41 * 4, 70, 20);
//# 						break;
//# 					case 9:
//# 						g.setClip((getWidth() / 2 + 10), 60, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 + 10) - 41 * 5, 60, 20);
//# 						break;
//# 					case 10:
//# 						g.setClip((getWidth() / 2 + 10), 40, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 + 10) - 41 * 5, 40, 20);
//# 						break;
//# 					case 11:
//# 						g.setClip((getWidth() / 2 + 13), 20, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 + 13) - 41 * 5, 20, 20);
//# 						break;
//# 					case 12:
//# 						g.setClip((getWidth() / 2 + 23), 0, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 + 23) - 41 * 5, 0, 20);
//# 						break;
//# 					case 13:
//# 						g.setClip((getWidth() / 2 + 30), -35, MAN_W, MAN_H);
//# 						g.drawImage(image_gamefour[9], (getWidth() / 2 + 30) - 41 * 5, -35, 20);
//# 						break;
//# 				}
//# 			}
//#
//# 			if (manFreeingFrame > 13)
//# 			{
//# 				if (timePerGame[3] > 0 && locksToUnlock == 0 && oxygenCount < 65)
//#endif
				{
					drawInterfaceBG(g);
				}
				//justWonTheMatch condition of game four
			}
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameFour(Graphics g)
	{
//#if CONFIG == "BIG"
		if (locksToUnlock != 0 && oxygenCount >= 129)
//#endif
//#if CONFIG == "MEDIUM"
//# 		if (locksToUnlock != 0 && oxygenCount >= 95)
//#endif
//#if CONFIG == "SMALL"
//# 		if (locksToUnlock != 0 && oxygenCount >= 65)
//#endif
		{
			g.setColor(247, 189, 33);
			g.fillRect(0, 0, getWidth(), getHeight());
		}

		///desenha o fundo
		g.drawImage(image_gamefour[0], 0, 0, Graphics.TOP | Graphics.LEFT);

//#if CONFIG != "SMALL"
		g.drawImage(image_gamefour[1], getWidth() - image_gamefour[1].getWidth(), 49, Graphics.TOP | Graphics.LEFT);
		g.drawImage(image_gamefour[7], getWidth() / 2 - 10, getHeight() / 3, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 		g.drawImage(image_gamefour[1], 111, 14, Graphics.TOP | Graphics.LEFT);
//#endif
		/// desenha medidor de oxigênio
		drawOxygenMeter(g);

		/// desenha o corte do cadeado
		drawLockInside(g);

		// desenha o homem
		if (!(manJustReleasedLock || manIsFreed))
		{
//#if CONFIG != "SMALL"
			g.drawImage(image_gamefour[8], getWidth() / 2 - 5, -10, Graphics.TOP | Graphics.LEFT);
			g.drawImage(image_gamefour[7], getWidth() / 2 - 10, getHeight() / 3, Graphics.TOP | Graphics.LEFT);
//#endif

//#if CONFIG == "SMALL"
//# 			g.drawImage(image_gamefour[8], 60, -10, Graphics.TOP | Graphics.LEFT);
//# 			g.drawImage(image_gamefour[7], 55, 30, Graphics.TOP | Graphics.LEFT);
//#endif
		} ///HACK porco, eu sei, mas esse código é muito dificil de debuggar
		else if (manIsFreed)
		{
			g.drawImage(image_gamefour[0], 0, 0, Graphics.TOP | Graphics.LEFT);
		}


		///apesar de ser sempre chamada, só realmente desenha se o homem se libertar
		drawLockReleasingMan(g);

		///desenha os peixes
		drawFish(g);

		if (timePerGame[3] > 120 - BRIEF_HELP_TIME || ScreenManager.getInstance().hasPointerEvents())
		{
			int height = 0;

			if (!ScreenManager.getInstance().hasPointerEvents())
				height = getHeight() - 13;

			leftSoftKeyDrawable.setPosition(0, height);
			leftSoftKeyDrawable.setSize(getWidth() / 2, 20);
			rightSoftkeyDrawable.setPosition(getWidth() / 2, height);
			rightSoftkeyDrawable.setSize(getWidth() / 2, 20);

			drawFontString(g, TEXT_SK_BACK, getWidth() - getLineWidth1(getText(TEXT_SK_BACK)) - 3, height, 20, FONT_WHITE_MEDIUM);
		}

		if (timePerGame[3] > 120 - BRIEF_HELP_TIME)
		{
			String msg = null;
			if (ScreenManager.getInstance().hasPointerEvents())
			{
				msg = getText(TEXT_LOCKTOUCH);
			} else
			{
				msg = getText(TEXT_LOCKKEYS);
			}
			richLabel.setText(msg);
			g.setColor(107, 0, 0);
			g.fillRect(0, 40, getWidth(), richLabel.getTextTotalHeight());
			autoText(msg, MAXWIDTH, 0, 3,40,20,g);
		}

	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameFive(Graphics g)
	{
		///posiçao da corda superior
//#if CONFIG != "SMALL"
		int lowerSecurityWireHeight = 60;
		int wiresGap = 20;
		int lowerHighwirePositionY = highwireWalkerVerticalPosition - 20;
//#endif

//#if CONFIG == "SMALL"
//# 		int lowerSecurityWireHeight = 30;
//# 		int wiresGap = 20;
//# 		int lowerHighwirePositionY = highwireWalkerVerticalPosition - wiresGap;
//#endif




		///desenha o ceu
		for (int i = 0; i <= getWidth() / image_gamefive[4].getWidth(); i++)
		{
			g.drawImage(image_gamefive[4], (i * image_gamefive[4].getWidth()), 0, Graphics.TOP | Graphics.LEFT); // skyPaintX+getWidth()
		}

		//??
		g.setColor(0x6195ff);
		g.fillRect(0, image_gamefive[4].getHeight(), getWidth(), getHeight() - image_gamefive[4].getHeight());


//#if CONFIG != "SMALL"
		for (int j = 0; j < 3; j++)
//#endif
//#if CONFIG == "SMALL"
//# 		for (int j = 0; j < 6; j++)
//#endif
		{
			g.drawImage(image_gamefive[5], skyPaintX / 2 + 37 + (j * image_gamefive[5].getWidth()), getHeight() - image_gamefive[5].getHeight(), Graphics.TOP | Graphics.LEFT); // skyPaintX+getWidth()
		}


		///ultimo predio
		g.drawImage(image_gamefive[0], getWidth() * 3 - image_gamefive[0].getWidth() + skyPaintX, getHeight() - image_gamefive[0].getHeight(), Graphics.TOP | Graphics.LEFT);


		///desenha a bandeira vermelha ao fim da fase
		if (!flag5_gOver)
		{
			g.setColor(255, 0, 0);
			g.drawImage(image_gamefive[8], getWidth() * 3 - image_gamefive[0].getWidth() + skyPaintX - 18, highwireWalkerVerticalPosition - wiresGap - 3 - 15, Graphics.TOP | Graphics.LEFT);
			g.setColor(0);
			g.drawLine(getWidth() * 3 - image_gamefive[0].getWidth() + skyPaintX - 12, highwireWalkerVerticalPosition - wiresGap - 3 - 15, getWidth() * 3 - image_gamefive[0].getWidth() + skyPaintX, highwireWalkerVerticalPosition - wiresGap - 3);
		}

		//predio de partida
		g.drawImage(image_gamefive[0], skyPaintX, getHeight() - image_gamefive[0].getHeight(), Graphics.TOP | Graphics.LEFT);

		///desenha as cordas
		g.setColor(0);
		g.setStrokeStyle(Graphics.DOTTED);
		g.drawLine(skyPaintX, lowerSecurityWireHeight, getWidth() * 3, lowerSecurityWireHeight);
		g.setStrokeStyle(Graphics.SOLID);
		g.drawLine(skyPaintX, lowerHighwirePositionY, getWidth() * 3, lowerHighwirePositionY);

		if (!highwireWalkerFell)
		{
			highwireWalkerVerticalPosition = (3 * highwireWalkerCenterY) / 2;

			g.setClip(0, 0, 10000, 100000);
			g.setColor(0);

			if (highwireWalkerMovingRight && highwireWalkerIsFalling)
			{
				image_gamefive[1] = null;
				image_gamefive[3] = null;

				try
				{
					if (image_gamefive[2] == null)
					{
						image_gamefive[2] = ImageLoader.loadImage("/4/" + 2 + ".png");
					}
				} catch (Exception e)
				{
				}

				clipWidth = image_gamefive[2].getWidth() / 3;
				clipHeight = image_gamefive[2].getHeight();

				g.setColor(0, 0, 0);
				g.drawLine(highwireWalkerCenterX - 40, lowerSecurityWireHeight, highwireWalkerCenterX, highwireWalkerVerticalPosition - image_gamefive[2].getHeight());

				g.setClip(highwireWalkerCenterX - clipWidth / 2, highwireWalkerVerticalPosition - clipHeight + const_factor - 64 + 30, clipWidth, clipHeight);
				g.drawImage(image_gamefive[2], highwireWalkerCenterX - clipWidth / 2 - highwireWalkerFrameCounter * clipWidth, highwireWalkerVerticalPosition - clipHeight + const_factor - 64 + 30, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());

				g.setColor(0, 0, 0);
//				g.drawLine(highwireWalkerCenterX - 40, 60, highwireWalkerCenterX, highwireWalkerVerticalPosition - 80 + 20);

			} else if (highwireWalkerIsFalling)// falling back
			{

				image_gamefive[1] = null;
				image_gamefive[2] = null;

				try
				{
					if (image_gamefive[3] == null)
					{
						image_gamefive[3] = ImageLoader.loadImage("/4/" + 3 + ".png");
					}
				} catch (Exception e)
				{
				}

				clipWidth = image_gamefive[3].getWidth() / 3;
				clipHeight = image_gamefive[3].getHeight();

				g.setColor(0, 0, 0);
				g.drawLine(highwireWalkerCenterX - 40, lowerSecurityWireHeight, highwireWalkerCenterX - 10, highwireWalkerVerticalPosition - image_gamefive[3].getHeight());

				g.setClip(highwireWalkerCenterX - clipWidth / 2, highwireWalkerVerticalPosition - clipHeight + const_factor - 64 + 30, clipWidth, clipHeight);
				g.drawImage(image_gamefive[3], highwireWalkerCenterX - clipWidth / 2 - highwireWalkerFrameCounter * clipWidth, highwireWalkerVerticalPosition - clipHeight + const_factor - 64 + 30, Graphics.TOP | Graphics.LEFT);
				g.setColor(0, 0, 0);
//				g.drawLine(highwireWalkerCenterX - 40, 60, highwireWalkerCenterX, highwireWalkerVerticalPosition - 80 + 20);
			} else if (!highwireWalkerIsFalling)// walking straight
			{

				image_gamefive[2] = null;
				image_gamefive[3] = null;

				try
				{
					if (image_gamefive[1] == null)
					{
						image_gamefive[1] = ImageLoader.loadImage("/4/" + 1 + ".png");
					}
				} catch (Exception e)
				{
				}

				clipWidth = image_gamefive[1].getWidth() / 4;
				clipHeight = image_gamefive[1].getHeight();

				g.setColor(0, 0, 0);
				g.drawLine(highwireWalkerCenterX - 40, lowerSecurityWireHeight, highwireWalkerCenterX, highwireWalkerVerticalPosition - image_gamefive[1].getHeight());
				g.setClip(highwireWalkerCenterX - clipWidth / 2, highwireWalkerVerticalPosition - clipHeight + const_factor - 64 + 30, clipWidth, clipHeight);
				g.drawImage(image_gamefive[1], highwireWalkerCenterX - clipWidth / 2 - highwireWalkerFrameCounter * clipWidth, highwireWalkerVerticalPosition - clipHeight + const_factor - 64 + 30, Graphics.TOP | Graphics.LEFT);
				g.setClip(0, 0, getWidth(), getHeight());
				g.setColor(0, 0, 0);
//				g.drawLine(highwireWalkerCenterX - 40, 60, highwireWalkerCenterX, highwireWalkerVerticalPosition - 80 + 20);
			}

			// Perdeu o equilibrio
			if (highwireWalkerBalance > 40 || highwireWalkerBalance < -40)
			{
				g.setClip(0, 0, 10000, 100000);
			}

			g.setColor(255, 0, 0);
			g.setClip(0, 0, 10000, 100000);
		} else
		{
			drawGameFiveOver(g);
		}

		//desenha o topo branco da janela
		g.setColor(255, 255, 255);
		g.fillRect(0, 0, getWidth(), image_gamefive[9].getHeight());

		//desenha o tempo restante
		g.setColor(0);
		drawFontString(g, "" + timePerGame[4], 1, -4, 20, FONT_BLACK_MEDIUM);

		//desenha moldura do indicador de equilibrio
		g.drawImage(image_gamefive[9], getWidth() / 2, 0, 17);

		if (timePerGame[4] > Max_Time_G5 - BRIEF_HELP_TIME)
		{
			String msg = null;
			if (ScreenManager.getInstance().hasPointerEvents())
			{
				msg = getText(TEXT_BALANCETOUCH);
			} else
			{
				msg = getText(TEXT_BALANCEKEYS);
			}
			richLabel.setText(msg);
			g.setColor(107, 0, 0);
			g.fillRect(0, 15, getWidth(), richLabel.getTextTotalHeight());
			autoText(msg, MAXWIDTH, 0, 3,15,20,g);
		}


		//desenha os controles de equilibrio
		if (game5VisualJoystick != null)
		{
			int oldx = translate.x;
			int oldy = translate.y;

			translate.x -= oldx;
			translate.y -= oldy;

			game5VisualJoystick[0].setPosition((getWidth() - game5VisualJoystick[0].getFrame(0).width) / 2, getHeight() - game5VisualJoystick[0].getCurrentFrameImage().getHeight() - 20);
			game5VisualJoystick[1].setPosition(game5VisualJoystick[0].getPosX() - game5VisualJoystick[1].getFrame(0).width - 20, getHeight() - game5VisualJoystick[0].getCurrentFrameImage().getHeight() - 20);
			game5VisualJoystick[2].setPosition(game5VisualJoystick[0].getPosX() + game5VisualJoystick[0].getFrame(0).width + 20 , getHeight() - game5VisualJoystick[0].getCurrentFrameImage().getHeight() - 20);

			game5VisualJoystick[0].draw(g);
			game5VisualJoystick[1].draw(g);
			game5VisualJoystick[2].draw(g);

			translate.x += oldx;
			translate.y += oldy;

			game5VisualJoystick[0].setVisible(true);
			game5VisualJoystick[1].setVisible(true);
			game5VisualJoystick[2].setVisible(true);

			if (leftKeyPressed)
			{
				game5VisualJoystick[0].setFrame(0);
				game5VisualJoystick[1].setFrame(1);
				game5VisualJoystick[2].setFrame(0);
			} else if (rightKeyPressed)
			{
				game5VisualJoystick[0].setFrame(0);
				game5VisualJoystick[1].setFrame(0);
				game5VisualJoystick[2].setFrame(1);

			} else if (fireKeyPressed)
			{
				game5VisualJoystick[0].setFrame(1);
				game5VisualJoystick[1].setFrame(0);
				game5VisualJoystick[2].setFrame(0);

			} else
			{
				game5VisualJoystick[0].setFrame(0);
				game5VisualJoystick[1].setFrame(0);
				game5VisualJoystick[2].setFrame(0);

			}
		}

		//desenha o indicador de equilibrio
		if (highwireWalkerBalance <= 0)
		{
			if (highwireWalkerBalance == 0)
			{
				g.drawImage(image_gamefive[8], getWidth() / 2 + 1 + highwireWalkerBalance, 3, 17);
			} else if (highwireWalkerBalance >= -40)
			{
				g.drawImage(image_gamefive[8], getWidth() / 2 + 1 + highwireWalkerBalance, 3, 17);
			}
		} else if (highwireWalkerBalance <= 40)
		{
			g.drawImage(image_gamefive[8], getWidth() / 2 + 1 + highwireWalkerBalance, 3, 17);
		}

		if (timePerGame[4] > 120 - BRIEF_HELP_TIME || ScreenManager.getInstance().hasPointerEvents())
		{
			leftSoftKeyDrawable.setPosition(0, getHeight() - 13);
			leftSoftKeyDrawable.setSize(getWidth() / 2, 13);
			rightSoftkeyDrawable.setPosition(getWidth() / 2, getHeight() - 13);
			rightSoftkeyDrawable.setSize(getWidth() / 2, 13);

			drawFontString(g, TEXT_SK_BACK, getWidth() - getLineWidth1(getText(TEXT_SK_BACK)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		}


	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameFiveOver(Graphics g)
	{
//#if CONFIG != "SMALL"
		int topRopeY = 60;
//#endif


//#if CONFIG == "SMALL"
//# 		int topRopeY = 30;
//#endif

		image_gamefive[1] = null;
		image_gamefive[2] = null;
		image_gamefive[3] = null;

		try
		{
			image_gamefive[10] = ImageLoader.loadImage("/4/" + 10 + ".png");
		} catch (Exception e)
		{
		}

		if (updating)
		{
			highwireWalkerFrameCounter++;
		}

		if (highwireWalkerFrameCounter < 5)
		{
			g.drawLine(highwireWalkerCenterX - 40, topRopeY, highwireWalkerCenterX + image_gamefive[10].getWidth() / 2, highwireWalkerVerticalPosition - image_gamefive[10].getHeight() + 20 + 30);
			g.drawImage(image_gamefive[10], highwireWalkerCenterX, highwireWalkerVerticalPosition - (image_gamefive[10].getHeight() * 3) / 2 + 20 + 30, Graphics.TOP | Graphics.LEFT);
		} else if (highwireWalkerFrameCounter < 10)
		{
			g.drawLine(highwireWalkerCenterX - 40, topRopeY, highwireWalkerCenterX - 40, highwireWalkerVerticalPosition - image_gamefive[10].getHeight() + 20 + 20);
			g.drawImage(image_gamefive[10], highwireWalkerCenterX - 40 - image_gamefive[10].getWidth() / 2, highwireWalkerVerticalPosition - (image_gamefive[10].getHeight() * 3) / 2 + 20 + 20, Graphics.TOP | Graphics.LEFT);
		} else if (highwireWalkerFrameCounter < 15)
		{
			g.drawLine(highwireWalkerCenterX - 40, topRopeY, highwireWalkerCenterX - 40, highwireWalkerVerticalPosition - image_gamefive[10].getHeight() + 20 + 10);
			g.drawImage(image_gamefive[10], highwireWalkerCenterX - 40 - image_gamefive[10].getWidth() / 2, highwireWalkerVerticalPosition - (image_gamefive[10].getHeight() * 3) / 2 + 20 + 10, Graphics.TOP | Graphics.LEFT);
		} else
		{
			drawInterfaceBG(g);
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawInGameSubMenu(Graphics g)
	{
		drawMainMenuScreen(g);
		animationCounter++;

		if (animationCounter > 100)
		{
			animationCounter = 0;
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawTitleScreen(Graphics g)
	{
		if (updating)
		{
			counter_title++;
		}

		int x = 0;
		int y = 0;

		if (imgTitle[0].getWidth() >= getWidth())
		{
			x = 0;
		} else
		{
			x = (getWidth() - imgTitle[0].getWidth()) / 2;
		}

		if (imgTitle[0].getHeight() >= getHeight())
		{
			y = 0;
		} else
		{
			y = (getHeight() - imgTitle[0].getHeight()) / 2;
		}

		g.drawImage(imgTitle[0], x, y, Graphics.TOP | Graphics.LEFT);


		///tela de titulo
		if (currentScreen == TITLE_SCREEN)
		{

			y = getHeight() / 2 - (imgTitle[1].getHeight() / 2);
			if (imgTitle[1].getWidth() >= getWidth())
			{
				x = 0;
			} else
			{
				x = (getWidth() - imgTitle[1].getWidth()) / 2;
			}

			g.drawImage(imgTitle[1], x, y, Graphics.TOP | Graphics.LEFT);

			if (titleNameDisplay >= 1 && titleNameDisplay <= 3)
			{
				y = getHeight() / 2 - (imgTitle[1].getHeight() / 2) - 2;
			} else if (titleNameDisplay >= 4 && titleNameDisplay <= 6)
			{
				y = getHeight() / 2 - (imgTitle[1].getHeight() / 2) + 2;
			} else if (titleNameDisplay >= 7 && titleNameDisplay <= 9)
			{
				y = getHeight() / 2 - (imgTitle[1].getHeight() / 2) - 3;
			} else if (titleNameDisplay >= 10 && titleNameDisplay <= 12)
			{
				y = getHeight() / 2 - (imgTitle[1].getHeight() / 2) + 3;
			}

			g.drawImage(imgTitle[1], x, y, Graphics.TOP | Graphics.LEFT);

			if (titleNameAniCurrentFrame < TITLENAMEANIMATIONFRAMES)
			{
				clipImage(g, x, getHeight() / 2 - (imgTitle[2].getHeight() / 2), imgTitle[2], (imgTitle[2].getWidth() / TITLENAMEANIMATIONFRAMES) * titleNameAniCurrentFrame, 0, imgTitle[2].getWidth() / TITLENAMEANIMATIONFRAMES, imgTitle[2].getHeight());
			}
		} else
		{
			imgTitle[2] = null;
			imgTitle[1] = null;
			System.gc();
		}

		if (counter_title % 3 == 0)
		{
			titleNameAniCurrentFrame++;
		}


		if (titleNameAniCurrentFrame >= TITLENAMEANIMATIONFRAMES)
		{
			if (titleNameDisplay >= getWidth() / 4)
			{
				titleNameDisplay = 0;
				titleNameAniCurrentFrame = 0;
			} else
			{
				if (updating)
				{
					titleNameDisplay++;
				}
			}
		}

		///diferentes velocidades de setas
		if (updating)
		{
			backgroundArrow1X += 6;
			backgroundArrow2X += 5;
			backgroundArrow3X += 4;
			backgroundArrow4X += 3;
		}


		///faz o wrap da posicao das setas no fundo
		if (backgroundArrow1X > getWidth())
		{
			backgroundArrow1X = 0;
		}

		if (backgroundArrow2X > getWidth())
		{
			backgroundArrow2X = 0;
		}

		if (backgroundArrow3X > getWidth())
		{
			backgroundArrow3X = 0;
		}

		if (backgroundArrow4X > getWidth())
		{
			backgroundArrow4X = 0;
		}

		///setas que passam no fundo do menu
		g.drawImage(imgTitle[3], backgroundArrow1X, getHeight() / 4, Graphics.TOP | Graphics.LEFT);
		g.drawImage(imgTitle[3], backgroundArrow2X, getHeight() - (getHeight() / 4), Graphics.TOP | Graphics.LEFT);
		g.drawImage(imgTitle[4], backgroundArrow3X, getHeight() / 8, Graphics.TOP | Graphics.LEFT);
		g.drawImage(imgTitle[5], backgroundArrow4X, getHeight() - (getHeight() / 7), Graphics.TOP | Graphics.LEFT);
	}
//------------------------------------------------------------------------------

	/**
	 * desenha fundo dos menus
	 * @param g
	 */
	private void drawInterfaceBG(Graphics g)
	{
		g.setColor(247, 189, 33);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param s
	 */
	private void p(String s)
	{
		//#ifdef DEBUG
		System.out.println("" + s);
		//#endif
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawMainMenuScreen(Graphics g)
	{
//		int menuItemHeight = 25;				
//		int startX = 40;

//#if CONFIG == "BIG"
		int startX = getWidth() / 6;
		int startY = 0;
		int menuItemHeight = getHeight() / 13;
		int xOffset = 8;
//#endif

//#if CONFIG == "MEDIUM"
//# 		int startX = getWidth() / 16;
//# 		int startY = 0;
//# 		int menuItemHeight = getHeight() / 10;
//# 		int xOffset = 8;
//#endif

//#if CONFIG == "SMALL"
//# 		int startX = 0;
//# 		int startY = getHeight() / 16;
//# 		int menuItemHeight = getHeight() / 9;
//# 		int xOffset = 32;
//#endif


		int menuItemXOffset = 0;
		String soundOption = "";


		///desenha o fundo do menu
		drawTitleScreen(g);

		switch (currentScreen)
		{
			case MAIN_MENU_SCREEN:
				startY = getHeight() / 2 - menuItemHeight * menuOption.length / 2 - button2.getHeight();
				for (int menuItem = 0; menuItem < menuOption.length; menuItem++)
				{
					if (menuItem == 3)
					{
						if (!MediaPlayer.isMuted())
						{
							soundOption = getText(TEXT_SOUND_ON);
						} else
						{
							soundOption = getText(TEXT_SOUND_OFF);
						}
					} else
					{
						soundOption = "";
					}

					if (menuItem % 2 == 0)
					{
						menuItemXOffset = getWidth() / xOffset;
					} else
					{
						menuItemXOffset = 0;
					}

					if ((menuSel - 1) == menuItem)
					{
						g.drawImage(button2, startX + menuItemXOffset, startY + menuItem * menuItemHeight, 0);
						drawFontString(g, menuOption[menuItem] + soundOption, startX + menuItemXOffset + 15 + 49 - (getLineWidth1(menuOption[menuItem]) + getLineWidth1(soundOption)) / 2, startY + menuItem * menuItemHeight - 1, 20, FONT_WHITE_MEDIUM);

						if (performItemSelectionAnimation)
						{
							//	g.drawImage(buttonAni, menuSelectionAnimationX, startY + menuItem * menuItemHeight, 0);
						}

					} else
					{
						g.drawImage(button3, startX + menuItemXOffset, startY + menuItem * menuItemHeight, 0);
						drawFontString(g, menuOption[menuItem] + soundOption, startX + menuItemXOffset + 15 + 49 - (getLineWidth1(menuOption[menuItem]) + getLineWidth1(soundOption)) / 2, startY + menuItem * menuItemHeight - 1, 20, FONT_BLACK_MEDIUM);
					}
				}
				drawFontString(g, TEXT_SK_SELECT, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
				break;

			case INGAME_SUBMENU:
				startY = getHeight() / 2 - menuItemHeight * (highestLevel + 1) / 2 - button2.getHeight();

				for (int i = 0; i < highestLevel + 1; i++)
				{
					if (i % 2 == 0)
					{						
						menuItemXOffset = getWidth() / xOffset;
					}

					if ((subMenuSel - 1) == i)
					{
						g.drawImage(button2, startX + menuItemXOffset, startY + i * menuItemHeight, 0);
						drawFontString(g, gameOption[i], startX + menuItemXOffset + 15 + 49 - getLineWidth1(gameOption[i]) / 2, startY + i * menuItemHeight/*+2*/, 20, FONT_WHITE_MEDIUM);

						if (performItemSelectionAnimation)
						{
							//	g.drawImage(buttonAni, menuSelectionAnimationX, startY + i * menuItemHeight, 0);
						}
					} else
					{
						g.drawImage(button3, startX + menuItemXOffset, startY + i * menuItemHeight, 0);
						drawFontString(g, gameOption[i], startX + menuItemXOffset + 15 + 49 - getLineWidth1(gameOption[i]) / 2, startY + i * menuItemHeight/*+2*/, 20, FONT_BLACK_MEDIUM);
					}
				}
				drawFontString(g, TEXT_SK_SELECT, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
				drawFontString(g, TEXT_SK_BACK, getWidth() - getLineWidth1(getText(TEXT_SK_BACK)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
				break;

			case PAUSE_SCREEN:
				startY = getHeight() / 2 - menuItemHeight * pausemenuOption.length / 2 - button2.getHeight();
				for (int i = 0; i < pausemenuOption.length; i++)
				{
					if (i == 1)
					{
						if (!MediaPlayer.isMuted())
						{
							soundOption = getText(TEXT_SOUND_ON);
						} else
						{
							soundOption = getText(TEXT_SOUND_OFF);
						}
					} else
					{
						soundOption = "";
					}

					if (i % 2 == 0)
					{
						menuItemXOffset = getWidth() / 8;
					} else
					{
						menuItemXOffset = 0;
					}

					if ((subMenuSel) == i)
					{
						g.drawImage(button2, startX + menuItemXOffset, startY + i * menuItemHeight, 0);
						drawFontString(g, pausemenuOption[i] + soundOption, startX + menuItemXOffset + 13 + 49 - (getLineWidth1(pausemenuOption[i]) + getLineWidth1(soundOption)) / 2, startY + i * menuItemHeight - 1, 20, FONT_WHITE_MEDIUM);
					} else
					{
						g.drawImage(button3, startX + menuItemXOffset, startY + i * menuItemHeight, 0);
						drawFontString(g, pausemenuOption[i] + soundOption, startX + menuItemXOffset + 13 + 49 - (getLineWidth1(pausemenuOption[i]) + getLineWidth1(soundOption)) / 2, startY + i * menuItemHeight - 1, 20, FONT_BLACK_MEDIUM);
					}
				}

				drawFontString(g, TEXT_SK_SELECT, 8, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
				break;
		}

		///faz a animacão de seleção de item de menu
		if (updating)
		{
			if (performItemSelectionAnimation)
			{
//vou permitir esse ciclo acontecer uma vez, pois não sei quais outros
//efeitos colaterais acontecem nesse estado
				if (menuSelectionAnimationX < /*getWidth()*/ 15)
				{
					menuSelectionAnimationX += 15;
				} else
				{
					menuSelectionAnimationX = -10;
					performScreenChange();
					performItemSelectionAnimation = false;
				}
			}
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void performScreenChange()
	{
		//faz a troca de telas
		switch (currentScreen)
		{
			case MAIN_MENU_SCREEN:
				switch (menuSel)
				{
					case 1://new game
						changeGameState(INGAME_SUBMENU);
						subMenuSel = 1;
						break;

					case 2://help
						changeGameState(HELP_SCREEN);
						break;

					case 3://help
						changeGameState(HIGHSCORE_SCREEN);
						break;

					case 4://sound
						MediaPlayer.setMute(!MediaPlayer.isMuted());
						if (!MediaPlayer.isMuted())
						{
							MediaPlayer.play(SOUND_TITLE, MediaPlayer.LOOP_INFINITE);
						}
						break;

					case 5://about
						changeGameState(ABOUT_SCREEN);
						break;

					case 6://exit
						changeGameState(GAME_EXIT_CONFIRM_SCREEN);
						break;

///DM: a tela de creditos foi eliminada
//					case 6://credits
//						changeGameState(CREDIT_SCREEN);
//						break;
//
//					case 7://exit
//						changeGameState(GAME_EXIT_CONFIRM_SCREEN);
//						break;
				}
				break;

			///tela de escolha de jogos
			case INGAME_SUBMENU:
				switch (subMenuSel)
				{
					case 1:	// new game ONE!!
						gameStartTime = System.currentTimeMillis();
						initGameOne();
						resetGameOneVariable();
						changeGameState(INGAME_ONE);
						currentGame = 1;
						break;

					case 2: // new game TWO!!
						initGameTwo();
						resetGameTwoVariable();
						gameStartTime = System.currentTimeMillis();
						Max_Time_G2 = 120;
						changeGameState(INGAME_TWO);
						break;

					case 3: // new game THREE!!
						initGameThree();
						resetGameThreeVariable();
						gameStartTime = System.currentTimeMillis();
						Max_Time_G3 = 120;
						changeGameState(INGAME_THREE);
						break;

					case 4: // new game FOUR!!
						initGameFour();
						resetGameFourVariable();
						gameStartTime = System.currentTimeMillis();
						Max_Time_G4 = 120;
						changeGameState(INGAME_FOUR);
						break;

					case 5: // new game FIVE!!
						highwireWalkerFell = false;
						initGameFive();
						resetGameFiveVariable();
						gameStartTime = System.currentTimeMillis();
						Max_Time_G5 = 250;
						changeGameState(INGAME_FIVE);
						break;
				}
				break;
		}
	}
//------------------------------------------------------------------------------

	/**
	 * desenha o fundo para a janela de texto
	 */
	private void drawBGforText(Graphics g)
	{
		if (imgTitle[0] != null)
		{
			if (imgTitle[0].getWidth() < getWidth())
			{
				g.drawImage(imgTitle[0], 0, 0, Graphics.TOP | Graphics.LEFT);
			} else
			{
				g.drawImage(imgTitle[0], 0, 0, Graphics.TOP | Graphics.LEFT);
				g.drawImage(imgTitle[0], imgTitle[0].getWidth(), 0, Graphics.TOP | Graphics.LEFT);
			}
		}

		g.setColor(107, 0, 0);
		g.fillRect(8, 20, getWidth() - 16, getHeight() - 40);
		g.setColor(229, 34, 7);
		g.drawRect(8, 20, getWidth() - 16, getHeight() - 40);
	}
//------------------------------------------------------------------------------

	/**
	 * desenha a tela de escolha de som
	 * @param g
	 */
	private void drawSoundScreen(Graphics g)
	{
		g.setColor(0);
		g.fillRect(0, 0, getWidth(), getHeight());
		autoText(getText(TEXT_SOUNDMENU_QUESTION), MAXWIDTH, MAXLINES, TEXTSTART_X, getHeight() / 2 - 10, 17, g);
		drawFontString(g, TEXT_SOUNDMENU_YES, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		drawFontString(g, TEXT_SOUNDMENU_NO, getWidth() - getLineWidth1(getText(TEXT_SOUNDMENU_NO)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param buffer
	 * @param posx
	 * @param posy
	 * @param img
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */
	private void clipImage(Graphics buffer, int posx, int posy, Image img, int x, int y, int w, int h)
	{
		try
		{
			buffer.setClip(posx, posy, w, h);
			buffer.drawImage(img, posx - x, posy - y, Graphics.TOP | Graphics.LEFT);
			buffer.setClip(0, 0, getWidth(), getHeight());
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			p("error in clipImage " + e.toString());
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawHelpScreen(Graphics g)
	{
		g.setColor(247, 189, 33);
		g.fillRect(0, 0, getWidth(), getHeight());
		drawBGforText(g);
		autoText(getText(TEXT_GAMES_DESC), MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y, 17, g);
		drawFontString(g, TEXT_PAUSEMENU_MAINMENU, getWidth() - getLineWidth1(getText(TEXT_PAUSEMENU_MAINMENU)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawAboutScreen(Graphics g)
	{
		drawBGforText(g);
		autoText(getText(TEXT_ENDEMOL_CREDITS), MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y, 17, g);
		drawFontString(g, TEXT_PAUSEMENU_MAINMENU, getWidth() - getLineWidth1(getText(TEXT_PAUSEMENU_MAINMENU)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawHighscoreScreen(Graphics g)
	{
		drawBGforText(g);
		drawFontString(g, getText(TEXT_SCORE) + String.valueOf(TotalScore), /*(getWidth() - 18 * 6) / 2, getHeight() / 3, 20,*/ TEXTSTART_X, TEXTSTART_Y, 20, FONT_WHITE_MEDIUM);
		drawFontString(g, TEXT_PAUSEMENU_MAINMENU, getWidth() - getLineWidth1(getText(TEXT_PAUSEMENU_MAINMENU)) - 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameOverScreen(Graphics g)
	{
		long gameScore = 0;

		g.setColor(247, 189, 33);
		g.fillRect(0, 0, getWidth(), getHeight());
		drawBGforText(g);

		switch (currentGame)
		{
			case 1:
				gameScore = scorePerGame[0];
				justWonTheMatch = true;
				break;
			case 2:
				gameScore = scorePerGame[1];
				break;
			case 3:
				gameScore = scorePerGame[2];
				break;
			case 4:
				gameScore = scorePerGame[3];
				break;
			case 5:
				gameScore = scorePerGame[4];
				break;

			default:
				break;
		}

		if (justWonTheMatch)
		{
			if (currentGame == 1)
			{
				autoText(getText(TEXT_SCORE) + gameScore, MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y /*+ 30*/, 17, g);
			} else if (currentGame == 5)
			{
				autoText(getText(TEXT_MSG_WINNER) + gameScore, MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y /*+ 30*/, 17, g);
			} else
			{
				autoText(getText(TEXT_MSG_WIN) + gameScore, MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y /*+ 30*/, 17, g);
			}
		} else
		{
			autoText(getText(TEXT_MSG_GAMEOVER), MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y + 30, 17, g);
		}

		drawFontString(g, TEXT_CMD_OK, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawPauseScreen(Graphics g)
	{
		drawMainMenuScreen(g);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawCurrentGameQuitConfirmScreen(Graphics g)
	{
		drawBGforText(g);
		autoText(getText(TEXT_QUIT_QUESTION), MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y + 30, 17, g);
		drawFontString(g, TEXT_QUIT_YES, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		drawFontString(g, TEXT_QUIT_NO, getWidth() - getLineWidth1(getText(TEXT_QUIT_NO)) - 10, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}

//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 */
	private void drawGameExitConfirmScreen(Graphics g)
	{
		drawInterfaceBG(g);
		drawBGforText(g);
		autoText(getText(TEXT_QUIT_QUESTION), MAXWIDTH, MAXLINES, TEXTSTART_X, TEXTSTART_Y /*+ 30*/, 17, g);
		drawFontString(g, TEXT_QUIT_YES, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
		drawFontString(g, TEXT_QUIT_NO, getWidth() - getLineWidth1(getText(TEXT_QUIT_NO)) - 10, getHeight() - 15, 20, FONT_WHITE_MEDIUM);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 * @param str
	 * @param x
	 * @param y
	 * @param anchor
	 * @param color
	 */
	private void drawFontString(Graphics g, String str, int x, int y, int anchor, int color)
	{
		drawFontString_new(g, str, x, y, anchor, color);
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param g
	 * @param strId
	 * @param x
	 * @param y
	 * @param anchor
	 * @param color
	 */
	private void drawFontString(Graphics g, int strId, int x, int y, int anchor, int color)
	{
		drawFontString_new(g, getText(strId), x, y, anchor, color);
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 */
	private void clearAllKeys()
	{
		unlockingStickActive = false;
		keyStatus = 0;
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param KEY
	 * @return
	 */
	private boolean isKeyPressed(int KEY)
	{
		//return ((keyStatus & (1 << KEY)) == (1 << KEY));
		return keyStatus == KEY;
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param newScreen
	 */
	private void changeGameState(int newScreen)
	{
		if (!MediaPlayer.isPlaying())
			MediaPlayer.play(SOUND_TITLE, MediaPlayer.LOOP_INFINITE);
		
		scrollCounter = 0;
		loadBitmapsForScreen(newScreen);
		currentScreen = newScreen;
		clearAllKeys();
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param screen
	 */
	private void loadBitmapsForScreen(int screen)
	{
		// according to locksToUnlock, load & unload the objects
		try
		{
			switch (screen)
			{
				case INGAME_ONE:
				case INGAME_TWO:
				case INGAME_THREE:
				case INGAME_FOUR:
				case INGAME_FIVE:
					imgTitle = null;
					System.gc();
					break;

				default:
					imgTitle = new Image[TITLE_IMAGES];

					for (int i = 0; i < TITLE_IMAGES; i++)
					{
						imgTitle[i] = ImageLoader.loadImage("/title/" + i + ".png");
					}
					break;
			}

			switch (screen)
			{
				case GAME_OVER:
					unloadAllInGameImages();
					break;
				case MAIN_MENU_SCREEN:
					textScrollPositionY = 0;
					break;

				case HELP_SCREEN:
					textScrollPositionY = 0;
					break;

				case ABOUT_SCREEN:
					textScrollPositionY = 0;
					break;

				case CREDIT_SCREEN:
					textScrollPositionY = 0;
					break;


				case INGAME_SUBMENU:
					textScrollPositionY = 0;
					break;
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.print("\nError in loading images : " + e);
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void resetGameOneVariable()
	{
		knifeStartY = 9 + image_gameone[5].getHeight();
		knife1StartPositionY = knifeStartY;
		knife2StartPositionY = knifeStartY;
		knife3StartPositionY = knifeStartY;
		knife4StartPositionY = knifeStartY;
		randomKnife = 0;
		jumpingKnifeGirlFrame = 0;
		knifeGirlPositionX = 0;
		leftKeyPressed = false;
		rightKeyPressed = false;
		releasingKnife = false;
		stabbedGirl = false;
		seconds = 0;
		miliseconds = 0;
		endGameTime = 0;
		counterKnifeTimer = 0;
		startGameTime = 0;
		knifeGirlFacingLeft = false;
		girl_clipX = image_gameone[4].getWidth() / 6;
		girl_clipY = image_gameone[4].getHeight();
		girlStartPosition = (getWidth() - image_gameone[6].getWidth() * 4) / 8 + getWidth() / 4;
		knifeGirlJumpStartPositionY = getHeight() - image_gameone[6].getHeight() - image_gameone[4].getHeight();
		knifeGirlPositionY = getHeight() - image_gameone[6].getHeight() - image_gameone[4].getHeight();
		girlSpriteWidth = girl_clipX;
		girlSpriteHeight = girl_clipY;
		knifeSpriteWidth = image_gameone[2].getWidth();
		knifeSpriteHeight = image_gameone[2].getHeight();
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void resetGameTwoVariable()
	{
		eatingSnake = false;
		fearFlagSnake = false;
		snakeInHandAnimationEnded = false;
		snakeInHandAnimationFrame = 0;
		snakeCount = 0;
		numberOfSnakes = 1;
		lipsMoving = false;
		lipAnimationFrame = 0;
		gameStartTime = 0;
		timePerGame[1] = 0;
		scorePerGame[1] = 0;
		justWonTheMatch = false;
		snakeInMouthFrame = 0;
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void resetGameThreeVariable()
	{
		ballGirlFreedFrame = 0;
		ballGirlIsUnlocked = false;
		pipe_counter1 = 0;
		pipe_counter2 = 0;
		pipe_counter3 = 0;
		pipeforward1 = true;
		pipeforward2 = true;
		pipeforward3 = true;
		pipeHorizontalPosition = 0;
		stop_counter = 60;
		pipeLockPositionY = 0;
		ironBallHeight = 0;
		chainProgression = 0;
		pipeforward0 = true;
		pipe_counter0 = 0;
		ballGirlLock1 = false;
		ballGirlLock2 = false;
		ballGirlLock3 = false;
		ballGirlLock4 = false;
		girls_lock_count = 0;
		scorePerGame[2] = 0;
		girlStartPosition = 36;
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void resetGameFourVariable()
	{
		timePerGame[3] = 0;
		unlockingStickMovingLeft = false;
		unlockingStickActive = false;
		stage4JustStarted = true;
		locksToUnlock = 4;
		unlockingStickSpeed = 3;
		manIsFreed = false;
		manJustReleasedLock = false;
		manFreeingFrame = 0;
		oxygenCount = 0;
		oxygenControl = 0;
		scorePerGame[3] = 0;
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void resetGameFiveVariable()
	{
		highwireWalkerTimer = 1;
		highwireWalkerIsFalling = false;
		highwireWalkerBalance = 0;
		flag5_gOver = false;
		highwireWalkerFell = false;
		skyPaintX = 0;
		highwireWalkerCenterX = getWidth() / 2 - 20;
		highwireWalkerCenterY = (getHeight() / 2);
		scorePerGame[4] = 0;
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 * @param str
	 * @param GetX
	 * @param y
	 * @param anchor
	 * @param Color
	 */
	private void drawFontString_new(Graphics g, String str, int x, int y, int anchor, int Color)
	{
		Label currentLabel;

		switch (Color)
		{
			case FONT_GREEN_DIGITAL:
				currentLabel = digitalLabel;
				break;
			case FONT_BLACK_MEDIUM:
				currentLabel = simpleBlackLabel;
				break;
			default:
			case FONT_WHITE_MEDIUM:
				currentLabel = simpleWhiteLabel;
				break;
		}

		currentLabel.setText(str, false);
		currentLabel.setClipTest(false);
		currentLabel.setPosition(x, y);
		g.translate(-translate.x, -translate.y);
		currentLabel.draw(g);
		g.translate(translate.x, translate.y);
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param str1
	 * @return
	 */
	int getLineWidth1(String str1)
	{
		int len = 0;
		char str[] = str1.toCharArray();

		try
		{
			for (int i = 0; i < str.length; i++)
			{
				for (byte j = 0; j < 90; j++)
				{
					if (str[i] == '~')
					{
						str[i] = ' ';
					}

					if (char_array1[j] == str[i])
					{
						len += (char_width_array1[j]) + 1;
						break;
					}
				}
			}

		} catch (Exception e)
		{
			//#if DEBUG == "true"
			e.printStackTrace();
			System.out.println(" error in GetLineWidth  ");
			//#endif
		}
		return len;
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param g
	 * @param chr
	 * @param x
	 * @param y
	 * @param Color
	 * @return
	 */
	private int drawCharacter(Graphics g, char chr, int x, int y, int Color)
	{
		int nActualFontWidth = 0;  // font getWidth()
		int nActualFontHeight = 0;  // font getHeight()
		int offsetx = 0;
		int offsety = 0;
		int size = 0;

		if (chr == '~')
		{
			chr = ' ';
		}

		nActualFontWidth = 12;  // font getWidth()
		nActualFontHeight = 14;  // font getHeight()

		for (int i = 0; i < 90; i++)
		{
			try
			{
				if (char_array1[i] == chr)
				{
					if (i == 74)
					{
						i = 81;
					}

					if (i > 44)
					{
						offsetx = nActualFontWidth * (i - 45);
						offsety = nActualFontHeight;
					} else
					{
						offsetx = nActualFontWidth * i;
						offsety = 0;
					}

					size = char_width_array1[i];
					break;
				}
			} catch (Exception e)
			{
				//#if DEBUG == "true"
				e.printStackTrace();
				//#endif
			}
		}

		g.setClip(x, y, nActualFontWidth, nActualFontHeight);

		return size + 1;
	}
//------------------------------------------------------------------------------
	/**
	 *
	 * @param thestring
	 * @param max_width
	 * @param lines
	 * @param startX
	 * @param startY
	 * @param defaultAlign
	 */
	private void autoText(String thestring, int max_width, int lines, int startX, int startY, int defaultAlign, Graphics g)
	{
		int fontHeight = GameMIDlet.GetFont(3).getHeight();
		int currentScrollPosition = scrollCounter * fontHeight;
		int totalSize = 0;

		try
		{
			richLabel.setText(thestring);
			totalSize = richLabel.getTextTotalHeight();

			if (scrollCounter < 0)
			{
				scrollCounter = 0;
				return;
			}

			if (((totalSize / fontHeight) >= lines) && (scrollCounter > (totalSize / fontHeight) - lines))
			{
				scrollCounter = (totalSize / fontHeight) - lines;
			}

			richLabel.formatText(false);

			g.translate(-translate.x, -translate.y);

			GameMIDlet.log("" + (totalSize / fontHeight) + "," + scrollCounter);

			if (((totalSize / fontHeight) >= lines) && ((totalSize / fontHeight) > scrollCounter))
			{
				richLabel.setInitialLine(scrollCounter);
			} else
			{
				scrollCounter = 0;
				currentScrollPosition = 0;
			}

			richLabel.setPosition(startX, startY - currentScrollPosition);
			richLabel.setSize(getWidth() - 2 * startX, getHeight() - 2 * startY + currentScrollPosition);
			richLabel.draw(g);

			g.translate(translate.x, translate.y);

		} catch (Exception e)
		{
			if (((totalSize / fontHeight) >= lines))
			{
				scrollCounter = (totalSize / fontHeight) - lines;
			} else
			{
				scrollCounter = 0;
			}

			e.printStackTrace();
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void saveData()
	{
		ByteArrayOutputStream baos = null;
		DataOutputStream dos = null;
		try
		{
			baos = new ByteArrayOutputStream();
			dos = new DataOutputStream(baos);

			dos.writeInt(TotalScore);
			dos.writeInt(highestLevel);
			dos.flush();

			byte[] RMSData = baos.toByteArray();

			RecordStore recordscore;
			recordscore = RecordStore.openRecordStore("saveData", true);

			if (recordscore.getNumRecords() == 0)
			{
				recordscore.addRecord(RMSData, 0, RMSData.length);
			} else
			{
				recordscore.setRecord(1, RMSData, 0, RMSData.length);
			}

			recordscore.closeRecordStore();
			dos.close();
			dos = null;
			baos.close();
			baos = null;
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Exception in saving record store: " + e.toString());
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 */
	private void readData()
	{
		ByteArrayInputStream bais = null;
		DataInputStream dis = null;
		try
		{
			RecordStore recordscore;
			recordscore = RecordStore.openRecordStore("saveData", true);

			if (recordscore.getNumRecords() > 0)
			{

				byte[] RMSData = recordscore.getRecord(1);

				if (RMSData != null)
				{
					bais = new ByteArrayInputStream(RMSData);
					dis = new DataInputStream(bais);
					recordscore.closeRecordStore();
					TotalScore = dis.readInt();
					highestLevel = dis.readInt();

					dis.close();
					dis = null;
					bais.close();
					bais = null;
				} else
				{
					TotalScore = 0;
					highestLevel = 0;
				}
			}
		} catch (Exception e)
		{
			//#if DEBUG == "true"
			System.out.println("Exception in reading record store: " + e.toString());
			//#endif
		}
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param index
	 * @return
	 */
	public String getText(int index)
	{
		return GameMIDlet.getText(index);
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void onPointerDragged(int x, int y)
	{
		penXPos = x;
		penYPos = y;
		GameMIDlet.log("pointer dragged");
	}
//------------------------------------------------------------------------------
//#if TOUCH == "true"

	/**
	 *
	 * @param x
	 * @param y
	 */
	public void onPointerPressed(int px, int py)
	{
		penXPos = px;
		penYPos = py;


		GameMIDlet.log("pointer pressed");
		int menuItemXOffset = 0;
		int itemHeight = 0;
//#if CONFIG == "BIG"
		int startX = getWidth() / 6;
		int startY = 0;
		int menuItemHeight = getHeight() / 13;
		int xOffset = 8;
//#endif

//#if CONFIG == "MEDIUM"
//# 		int startX = getWidth() / 16;
//# 		int startY = 0;
//# 		int menuItemHeight = getHeight() / 10;
//# 		int xOffset = 8;
//#endif

//#if CONFIG == "SMALL"
//# 		int startX = 0;
//# 		int startY = getHeight() / 16;
//# 		int menuItemHeight =  getHeight() / 9;
//# 		int xOffset = 32;
//#endif

		//drawFontString(g, TEXT_SK_SELECT, 3, getHeight() - 15, 20, FONT_WHITE_MEDIUM);

		if (!contains(penXPos, penYPos))
		{
			return;
		}

		penXPos -= getPosX();
		penYPos -= getPosY();

		if (leftSoftKeyDrawable.contains(penXPos, penYPos))
		{
			keyPressed(ScreenManager.FIRE);
			return;
		}

		if (rightSoftkeyDrawable.contains(penXPos, penYPos))
		{
			keyPressed(ScreenManager.KEY_BACK);
			return;
		}

		switch (currentScreen)
		{
			case MAIN_MENU_SCREEN:
			{
				for (int menuItem = 0; menuItem <= menuOption.length; menuItem++)
				{

					if (menuItem % 2 == 0)
					{
						menuItemXOffset = 0;
					} else
					{
						menuItemXOffset = getWidth() / xOffset;
					}

					itemHeight = (menuItem + 1) * menuItemHeight;
					if ((penXPos > startX + menuItemXOffset) && (penXPos <= startX + menuItemXOffset + button3.getWidth())
							&& (penYPos > startY + itemHeight - menuItemHeight / 2) && (penYPos <= startY + itemHeight + 2 * menuItemHeight))
					{
						menuSel = menuItem;

						if (menuSel > menuOption.length)
							menuSel = menuOption.length;

						if (menuSel < 1)
							menuSel = 1;

						keyPressed(ScreenManager.FIRE);
						return;
					}
				}
			}
			break;

			case INGAME_SUBMENU:
			{
				startY = getHeight() / 2 - menuItemHeight * (highestLevel + 1) / 2 - button2.getHeight();

				for (int i = 0; i <= highestLevel; i++)
				{
					if (i % 2 == 0)
					{
						menuItemXOffset = getWidth() / xOffset;
					}

					itemHeight = (i) * menuItemHeight;
					if ((penXPos > startX + menuItemXOffset) && (penXPos <= startX + menuItemXOffset + button3.getWidth())
							&& (penYPos > startY + itemHeight) && (penYPos <= startY + itemHeight + menuItemHeight))
					{
						subMenuSel = i + 1;
						keyPressed(ScreenManager.FIRE);

						return;
					}
				}
			}
			break;

			case PAUSE_SCREEN:
			{
				startY = getHeight() / 2 - menuItemHeight * pausemenuOption.length / 2 - button2.getHeight();
				for (int i = 0; i < pausemenuOption.length; i++)
				{

					if (i % 2 == 0)
					{
						menuItemXOffset = getWidth() / 8;
					} else
					{
						menuItemXOffset = 0;
					}

					itemHeight = (i) * menuItemHeight;
					if ((penXPos > startX + menuItemXOffset) && (penXPos <= startX + menuItemXOffset + button3.getWidth())
							&& (penYPos > startY + itemHeight) && (penYPos <= startY + itemHeight + menuItemHeight))
					{
						subMenuSel = i;
						//faz com que os jogos nao tentem consumir o evento
						keyPressed(ScreenManager.FIRE);
						return;
					}

				}
			}
			break;
			case TITLE_SCREEN:
			case INGAME_TWO:
			case INGAME_THREE:
			case INGAME_FOUR:
			{
				keyPressed(ScreenManager.FIRE);
				break;
			}
			case HELP_SCREEN:
			case ABOUT_SCREEN:
			case CREDIT_SCREEN:
			case HIGHSCORE_SCREEN:
			{
				if (penYPos < getHeight() / 2)
				{
					keyPressed(ScreenManager.UP);
				} else
				{
					keyPressed(ScreenManager.DOWN);
				}
			}
			break;

			case INGAME_ONE:
			{
				if (penXPos < girlStartPosition + knifeGirlPositionX)
				{
					keyPressed(ScreenManager.LEFT);
				} else
				{
					keyPressed(ScreenManager.RIGHT);
				}
			}
			break;

			case INGAME_FIVE:
			{
				if (game5VisualJoystick[1].contains(penXPos, penYPos))
				{
					keyPressed(ScreenManager.LEFT);
				} else if (game5VisualJoystick[0].contains(penXPos, penYPos))
				{
					keyPressed(ScreenManager.FIRE);
				} else if (game5VisualJoystick[2].contains(penXPos, penYPos))
				{
					keyPressed(ScreenManager.RIGHT);
				}
			}
			break;

		}
	}
//------------------------------------------------------------------------------

	/**
	 *
	 * @param x
	 * @param y
	 */
	public void onPointerReleased(int x, int y)
	{
		keyReleased(ScreenManager.FIRE);
	}
//------------------------------------------------------------------------------
//#endif

	/**
	 * 
	 * @param deviceEvent
	 */
	public void hideNotify(boolean deviceEvent)
	{
		MediaPlayer.stop();

		if (currentScreen == INGAME_ONE || currentScreen == INGAME_TWO || currentScreen == INGAME_THREE || currentScreen == INGAME_FOUR || currentScreen == INGAME_FIVE)
		{
			changeGameState(PAUSE_SCREEN);
		}
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param deviceEvent
	 */
	public void showNotify(boolean deviceEvent)
	{
//#if DEBUG == "false"
//# 		MediaPlayer.play(SOUND_TITLE, MediaPlayer.LOOP_INFINITE);
//#endif

		switch (currentScreen)
		{
			case MAIN_MENU_SCREEN:
			{
				if (menuSel < 1 || menuSel >= menuOption.length)
				{
					subMenuSel = 1;
					menuSel = 1;
				}

			}
			break;

			case INGAME_SUBMENU:
			{
				if (menuSel < 1 || menuSel >= highestLevel)
				{
					subMenuSel = 1;
					menuSel = 1;
				}

			}
			break;

			case PAUSE_SCREEN:
			{
				if (subMenuSel < 1 || subMenuSel >= pausemenuOption.length)
				{
					subMenuSel = 1;
				}
			}
			break;
			default:
			{
				subMenuSel = 1;
				menuSel = 1;
			}
			break;
		}
	}
//------------------------------------------------------------------------------

	/**
	 * 
	 * @param getWidth()
	 * @param getHeight()
	 */
	public final void sizeChanged(int width, int height)
	{
		int localWidth = Math.min(width, PREFERED_SIZE_X);
		int localHeight = Math.min(height, PREFERED_SIZE_Y);


		if (localWidth != getWidth() || localHeight != getHeight())
		{
			setPosition((width - localWidth) / 2, (height - localHeight) / 2);
			setSize(localWidth, localHeight);
		}
	}
//------------------------------------------------------------------------------

	private final void drawBorder(Graphics g)
	{
		g.setColor(107, 0, 0);
		g.fillRect(0, 0, ScreenManager.SCREEN_WIDTH, translate.y);
		g.fillRect(0, translate.y, translate.x, ScreenManager.SCREEN_HEIGHT);
		g.fillRect(getWidth() + translate.x, translate.y, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
		g.fillRect(0, translate.y + getHeight(), ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

	}
//------------------------------------------------------------------------------
}
//==============================================================================

