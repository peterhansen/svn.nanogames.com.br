/**
 * GameScreen.java
 * ©2011 Nano Games.
 *
 * Created on Mar 2, 2011 2:07:41 PM.
 */
package screens;

import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import core.Camera;
import core.Character;
import core.Controller;
import core.Constants;
import core.Map;
import javax.microedition.lcdui.Graphics;

//#if TOUCH == "true"
import core.GestureManager;
import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.GestureListener;
import core.ParticleEmitter;
//#endif


/**
 * Tela principal do jogo.
 * @author Ygor
 */
public class GameScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener, SpriteListener
		//#if TOUCH == "true"
		, PointerListener, GestureListener
		//#endif
{	
	/** GameMIDlet associado a essa screen. */
	private GameMIDlet midlet;

	/** Instância da classe de gerenciamento da camera. */
	private static Camera camera;

	/** Instância da classe de gerenciamento da camera. */
	private static Map map;

	private Character character;
	private ParticleEmitter emitter;

	//#if TOUCH == "true"
	private final GestureManager gestureManager;
	//#endif

	GameScreen( GameMIDlet midlet ) throws Exception {
		super( 2 );

		map = new Map( 1 );
		character = new Character( map );
		map.setCharacter( character );

		insertDrawable( map );

		camera = new Camera( map.getPosition(), map );
		camera.setMapSize( map.getSize() );
		map.setCamera( camera );
		
		emitter = new ParticleEmitter( new Point( ScreenManager.SCREEN_WIDTH >> 1, ScreenManager.SCREEN_HEIGHT >> 1 ), 50, 85, 95, NanoMath.ONE * 300, NanoMath.ONE * 100, 5, 1000, 500, 2000 );
		insertDrawable( emitter );
		
//		Point p1 = new Point();
//		Point p2 = new Point( 100, 0 );
//		Point p3 = new Point();
//		for( int i = 0; i <= 360; i+=15 ) {
//			p3.set( NanoMath.toInt( 100 * NanoMath.cosInt( i ) ), NanoMath.toInt( 100 * NanoMath.sinInt( i ) ) );
//			final int a = p2.distanceTo( p1 );
//			final int b = p1.distanceTo( p3 );
//			final int c = p2.distanceTo( p3 );
//			final int cos = cosB( a, b, c );
//			//#if DEBUG == "true"
//				System.out.println( "---( "+i+"° )--- "+p1.toString()+" "+p2.toString()+" ) ( "+p3.toString()+" ) --- a="+a+", b="+b+" ,c="+c+" --- cos="+fixedToString(cos)+" ---- sin="+fixedToString(sinFromCos(cos))+" -" );
//			//#endif
//		}

		this.midlet = midlet;

		//bkg = new Pattern( COLOR_BACKGROUND );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		//#if TOUCH == "true"
		gestureManager = new GestureManager( this );
		//#endif
	}
	
	
	public final static String fixedToString( int fixed ) {
		int temp = NanoMath.toInt( fixed );
		String s = "" + temp;
		s+=',';
		fixed -= NanoMath.toFixed( temp );
		for( int i = 10; i <= 10000; i*=10 ) {
			temp = NanoMath.toInt( fixed * i );
			s += "" + temp;
			fixed -= NanoMath.divInt( temp, i );
		}
		return s;
	}


//	private final void updateRect() {
//		final Point p = rect.getRefPixelPosition();
//		rd = NanoMath.sqrtInt( rw * rw + rh * rh );
//		rect.setSize( rd, rd );
//		rect.defineReferencePixel( ANCHOR_CENTER );
//		rect.setRefPixelPosition( p );
//			
//		rect.p1.set( ( rd - NanoMath.toInt( rw * NanoMath.cosInt( ra ) + rh * NanoMath.sinInt( ra ) ) ) >> 1, ( rd + NanoMath.toInt( rw * NanoMath.sinInt( ra ) - rh * NanoMath.cosInt( ra ) ) ) >> 1 );
//		rect.p2.set( ( rd + NanoMath.toInt( rw * NanoMath.cosInt( ra ) - rh * NanoMath.sinInt( ra ) ) ) >> 1, ( rd - NanoMath.toInt( rw * NanoMath.sinInt( ra ) + rh * NanoMath.cosInt( ra ) ) ) >> 1 );
//		rect.p3.set( ( rd + NanoMath.toInt( rw * NanoMath.cosInt( ra ) + rh * NanoMath.sinInt( ra ) ) ) >> 1, ( rd + NanoMath.toInt( -rw * NanoMath.sinInt( ra ) + rh * NanoMath.cosInt( ra ) ) ) >> 1 );
//		rect.bottomLeft.set( ( rd + NanoMath.toInt( -rw * NanoMath.cosInt( ra ) + rh * NanoMath.sinInt( ra ) ) ) >> 1, ( rd + NanoMath.toInt( rw * NanoMath.sinInt( ra ) + rh * NanoMath.cosInt( ra ) ) ) >> 1 );
//	}
	
	
	public static final void load() {
	}


	public static final void unload() {
	}


	public final void paint( Graphics g ) {
		//bkg.draw( g );
		super.paint( g );
		
//		g.setColor( 0xff0000 );
//		g.drawLine( translate.x + circle.getRefPixelX(), translate.y + circle.getRefPixelY(), translate.x + rect.getPosX() + rect.p1.x, translate.y + rect.getPosY() + rect.p1.y);
//		g.drawLine( translate.x + circle.getRefPixelX(), translate.y + circle.getRefPixelY(), translate.x + rect.getPosX() + rect.p3.x, translate.y + rect.getPosY() + rect.p3.y);
//		g.drawLine( translate.x + circle.getRefPixelX(), translate.y + circle.getRefPixelY(), translate.x + rect.getPosX() + rect.p2.x, translate.y + rect.getPosY() + rect.p2.y);
//		g.drawLine( translate.x + circle.getRefPixelX(), translate.y + circle.getRefPixelY(), translate.x + rect.getPosX() + rect.bottomLeft.x, translate.y + rect.getPosY() + rect.bottomLeft.y);
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	//<editor-fold desc="Event Handler">
	public void onSequenceEnded( int id, int sequence ) {
		// TODO implementar para GameScreen
	}


	public void onFrameChanged( int id, int frameSequenceIndex ) {
		// TODO implementar para GameScreen
	}


	public void hideNotify( boolean deviceEvent ) {
		// TODO implementar para GameScreen
	}


	public void showNotify( boolean deviceEvent ) {
		// TODO implementar para GameScreen
	}
	//</editor-fold> // end of Event Handler region


	public void update( int delta ) {
		super.update( delta );
		
		map.update( delta );
		camera.update( delta );
	}

	//<editor-fold desc="Getters and Setters">

	public void setSize( int width, int height ) {
		super.setSize( width, height );

		// map resizing:
		// since the size of the map has nothing to do with the
		// screen (the camera is responsible for drawing it), we 
		// only resize its drawable area
		map.setPosition( 0, 0 );
		map.resizeDrawableArea( width, NanoMath.max( getHeight(), 0 ) );

		// camera resizing
		camera.setSize( getWidth(), getHeight() );
		camera.setCameraExtension( 0, NanoMath.toInt( NanoMath.mulFixed( FP_LEVEL_WATER_AREA_HEIGHT, Constants.FP_METER_TO_PIXEL_RATIO ) ) );
		camera.lookAhead( character.getPosition() );
	}
	//</editor-fold> // end of Getters and Setters region

	// TODO Me tire daqui!
	/**
	 *  Method to detect colision between a circle and a rectangle.
	 * @param edge p1 opposite to p3
	 * @param edge p2 opposite to p4
	 * @param edge p3 opposite to p1
	 * @param edge p4 opposite to p2
	 * @param circle center
	 * @param Size of radius of circle
	 */
	public final static boolean isColiding( final Point p1, final Point p2, final Point p3, final Point p4, final Point cc, final int r ) {
//		// para calcular colisão de retângulos rotacionados deve se calcular w,h como a distância entre pontos
//		final int w = p1.distanceTo( p2 );
//		final int h = p1.distanceTo( p4 );

		//        w
		//   p1-------p2
		//   |        |
		//  h|        |
		//   |        |
		//   p4-------p3

		final int d1 = p1.distanceTo( cc );
		final int d2 = p2.distanceTo( cc );
		final int d3 = p3.distanceTo( cc );
		final int d4 = p4.distanceTo( cc );

//		// para calcular colisão de retângulos rotacionados deve se calcular cx1,cy1,cx2,cy2 em cima da projeção dos segmentos de rect
//		final int cx1 = NanoMath.abs( p1.x - cc.x );
//		final int cy1 = NanoMath.abs( p1.y - cc.y );
//		final int cx2 = NanoMath.abs( cc.x - p3.x );
//		final int cy2 = NanoMath.abs( cc.y - p3.y );
//		
//		final int tx = cx1 + cx2 - w;
//		final int ty = cy1 + cy2 - h;

		final int tx = NanoMath.abs( p1.x - cc.x ) + NanoMath.abs( cc.x - p3.x ) - p1.distanceTo( p2 );
		final int ty = NanoMath.abs( p1.y - cc.y ) + NanoMath.abs( cc.y - p3.y ) - p1.distanceTo( p4 );

		return r > d1 || r > d2 || r > d3 || r > d4 || ( tx <= 2 * r && ty <= 2 * r && ( tx == 0 || ty == 0 ) )
			   || d1 < r || d2 < r || d3 < r || d4 < r;
	}
	
	
	public final static int cosB( int a, int b, int c ) {
		return ( a != 0 && c != 0 ) ? NanoMath.divInt( ( (a*a) + (c*c) - (b*b) ), ( 2*a*c ) ) : NanoMath.ONE;
	}
	
	
	public final static int sinFromCos( int cos ) {
		return NanoMath.sqrtFixed( NanoMath.ONE - NanoMath.mulFixed( cos, cos ) );
	}
	
	
	public final static Point projection( final Point p1, final Point p2, final Point p3 ) {
		final int a = p2.distanceTo( p1 );
		final int b = p1.distanceTo( p3 );
		final int c = p2.distanceTo( p3 );
		final int cos = cosB( a, b, c );
		final int sin = sinFromCos( cos );
		return new Point( NanoMath.toInt( c * cos ), NanoMath.toInt( c * sin ) );
	}
	


	public final static int projectionInX( final Point ptm, final Point p1, final Point p3 ) {
		return NanoMath.abs( p1.x - ptm.x ) + NanoMath.abs( ptm.x - p3.x );
	}
	
	
	public final static int projectionInY( final Point ptm, final Point p1, final Point p3 ) {
		return NanoMath.abs( p1.y - ptm.y ) + NanoMath.abs( ptm.y - p3.y );
	}
	
	
	/**
	 *  Method to detect colision between a rectangle and other a rectangle.
	 * @param edge of first rectangle p1 opposite to p3
	 * @param edge of first rectangle p2 opposite to p4
	 * @param edge of first rectangle p3 opposite to p1
	 * @param edge of first rectangle p4 opposite to p2
	 * @param edge of second rectangle p1 opposite to p3
	 * @param edge of second rectangle p2 opposite to p4
	 * @param edge of second rectangle p3 opposite to p1
	 * @param edge of second rectangle p4 opposite to p of second rectangle2
	 */
	public final static boolean isColiding( final Point r1p1, final Point r1p2, final Point r1p3, final Point r1p4, final Point r2p1, final Point r2p2, final Point r2p3, final Point r2p4 ) {
		int pjx, pjy;
		
		// Checks if some point of rectangle 2 is inside rectangle 1		
		pjx = NanoMath.abs( r1p1.x - r2p1.x ) + NanoMath.abs( r2p1.x - r1p3.x ) - r1p1.distanceTo( r1p2 );
		pjy = NanoMath.abs( r1p1.y - r2p1.y ) + NanoMath.abs( r2p1.y - r1p3.y ) - r1p1.distanceTo( r1p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		pjx = NanoMath.abs( r1p1.x - r2p2.x ) + NanoMath.abs( r2p2.x - r1p3.x ) - r1p1.distanceTo( r1p2 );
		pjy = NanoMath.abs( r1p1.y - r2p2.y ) + NanoMath.abs( r2p2.y - r1p3.y ) - r1p1.distanceTo( r1p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		pjx = NanoMath.abs( r1p1.x - r2p3.x ) + NanoMath.abs( r2p3.x - r1p3.x ) - r1p1.distanceTo( r1p2 );
		pjy = NanoMath.abs( r1p1.y - r2p3.y ) + NanoMath.abs( r2p3.y - r1p3.y ) - r1p1.distanceTo( r1p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		pjx = NanoMath.abs( r1p1.x - r2p4.x ) + NanoMath.abs( r2p4.x - r1p3.x ) - r1p1.distanceTo( r1p2 );
		pjy = NanoMath.abs( r1p1.y - r2p4.y ) + NanoMath.abs( r2p4.y - r1p3.y ) - r1p1.distanceTo( r1p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		
		// Checks if some point of rectangle 1 is inside rectangle 2
		pjx = NanoMath.abs( r2p1.x - r1p1.x ) + NanoMath.abs( r1p1.x - r2p3.x ) - r2p1.distanceTo( r2p2 );
		pjy = NanoMath.abs( r2p1.y - r1p1.y ) + NanoMath.abs( r1p1.y - r2p3.y ) - r2p1.distanceTo( r2p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		pjx = NanoMath.abs( r2p1.x - r1p2.x ) + NanoMath.abs( r1p2.x - r2p3.x ) - r2p1.distanceTo( r2p2 );
		pjy = NanoMath.abs( r2p1.y - r1p2.y ) + NanoMath.abs( r1p2.y - r2p3.y ) - r2p1.distanceTo( r2p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		pjx = NanoMath.abs( r2p1.x - r1p3.x ) + NanoMath.abs( r1p3.x - r2p3.x ) - r2p1.distanceTo( r2p2 );
		pjy = NanoMath.abs( r2p1.y - r1p3.y ) + NanoMath.abs( r1p3.y - r2p3.y ) - r2p1.distanceTo( r2p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		pjx = NanoMath.abs( r2p1.x - r1p4.x ) + NanoMath.abs( r1p4.x - r2p3.x ) - r2p1.distanceTo( r2p2 );
		pjy = NanoMath.abs( r2p1.y - r1p4.y ) + NanoMath.abs( r1p4.y - r2p3.y ) - r2p1.distanceTo( r2p4 );
		if( pjx == 0 &&  pjy == 0 ) return true;
		
		return false;
	}

	//<editor-fold desc="Handle Input">

	/**
	 * Updates character world position and update the camera 
	 * so that it precedes him/her.
	 */
	protected void setCharacterPosition( int x, int y ) {
		character.setWorldPosition( x, y );
		camera.lookAhead( character.getPosition() );
	}

	public void keyPressed( int key ) {
		//camera.keyPressed( key );

		Controller controller = character.getController();
		if( controller != null ) {
			controller.keyPressed( key );
		}

	}


	public void keyReleased( int key ) {
		switch( key ) {
			case ScreenManager.KEY_NUM0: 
			case ScreenManager.KEY_POUND: 
				GameMIDlet.reset();
			break;
				
			case ScreenManager.KEY_SOFT_RIGHT: 
			case ScreenManager.KEY_CLEAR: 
				GameMIDlet.exit(); 
			break;
				
			default:
				Controller controller = character.getController();
				if( controller != null ) {
					controller.keyReleased( key );
				}
			break;
		}
	}


	//#if TOUCH == "true"
		public void onPointerPressed( int x, int y ) {
			gestureManager.onPointerPressed( x, y );
			camera.onPointerPressed( x, y );
		}

		public void onPointerDragged( int x, int y ) {
			gestureManager.onPointerDragged( x, y );
			camera.onPointerDragged( x, y );
		}

		public void onPointerReleased( int x, int y ) {
			gestureManager.onPointerReleased( x, y );
			camera.onPointerReleased( x, y );
		}


		public void handleTap( int x, int y ) {
		}


		public void handleFirstInteraction( int x, int y ) {
		}


		public void handleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
		}


		public void handleRelease( int x, int y ) {
		}


		public void gestureIsNotSpot( int x, int y ) {
		}
	//#endif
	//</editor-fold> // end of Handle Input region
}
