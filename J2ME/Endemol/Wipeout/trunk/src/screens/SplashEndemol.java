/**
 * SplashEndemol.java
 *
 * Created on Aug 30, 2010 2:41:00 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.PaletteChanger;
import core.Constants;
import javax.microedition.lcdui.Image;

/**
 *
 * @author peter
 */
public final class SplashEndemol extends UpdatableGroup implements Constants, ScreenListener {
	//<editor-fold desc="Constants">
		private static final short TIME_LOGO = 3300;
	//</editor-fold>
	
	//<editor-fold desc="Fields">
		private final DrawableImage imgEndemol;
		private short accTime;
		private final RichLabel label;
	//</editor-fold>


	//<editor-fold desc="Inicialization">
		public SplashEndemol() throws Exception {
			super( 3 );
			imgEndemol = new DrawableImage( PATH_SPLASH + "endemol.png" );
			imgEndemol.defineReferencePixel( ANCHOR_CENTER );
			insertDrawable( imgEndemol );

			final Image fontImage = new PaletteChanger( PATH_SPLASH + "font_credits.png" ).createImage( 0xffffff, 0xffffff );
			final ImageFont font = ImageFont.createMultiSpacedFont( fontImage, PATH_SPLASH + "font_credits.bin" );
			font.setCharExtraOffset( 1 );
			label = new RichLabel( font, GameMIDlet.getText( TEXT_SPLASH_ENDEMOL ) );
			insertDrawable( label );

			setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		}


		public final void setSize( int width, int height ) {
			super.setSize( width, height );

			imgEndemol.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );

			label.setSize( ( size.x * 3 ) >> 2, 0 );
			label.formatText( false );
			label.setSize( label.getWidth(), label.getTextTotalHeight() );
			label.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			label.setRefPixelPosition( size.x >> 1, size.y );
		}
	//</editor-fold>


	//<editor-fold desc="Update and Draw">
		public final void update( int delta ) {
			super.update( delta );

			accTime += delta;
			//#if DEBUG == "true"
				accTime += delta << 1;
			//#endif
			if ( accTime >= TIME_LOGO )
				GameMIDlet.setScreen( SCREEN_SPLASH_GAME );
//				GameMIDlet.setScreen( SCREEN_MAIN_MENU );
		}
	//</editor-fold>


	public final void hideNotify( boolean deviceEvent ) {}

	public final void showNotify( boolean deviceEvent ) { }


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}
}
