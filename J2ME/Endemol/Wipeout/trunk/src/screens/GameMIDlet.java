/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import br.com.nanogames.components.userInterface.form.Form;

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;

import java.util.Hashtable;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener ,Serializable
{
	//<editor-fold desc="Constants">
		private static final short GAME_MAX_FRAME_TIME = 180;

		private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 0;
		private static final byte BACKGROUND_TYPE_BLACK			= 1;
		private static final byte BACKGROUND_TYPE_NONE			= 2;

		public static final byte OPTION_ENGLISH = 0;
		public static final byte OPTION_PORTUGUESE = 1;

		public static final byte OPTION_PLAY_SOUD = 0;
		public static final byte OPTION_NO_SOUND = 1;

		public static final byte CONFIRM_YES = 0;
		public static final byte CONFIRM_NO = 1;
		public static final byte CONFIRM_CANCEL = 2;
	//</editor-fold> // end of Constants region


	//<editor-fold desc="Fields">

		//<editor-fold desc="Imagens Estáticas">
			public static DrawableImage BRAND_LOGO;
			public static DrawableImage BAR_PATTERN;
			public static DrawableImage ARROW;
			public static DrawableImage ARROW_PRESSED;
		//</editor-fold> // end of Imagens Estáticas region

		/** Índice do participante escolhido pelo jogador */
		private static int playerId = 0;

		private static int helpOptionSelected = TEXT_OPTION_HOW_TO_PLAY;

		//<editor-fold desc="Textos Aleatórios">
			protected static String[] manNames;
			protected static String[] womanNames;
			protected static String[] foodNames;
			protected static String[] hobbyNames;
			//protected static String[] cardContents;
		//</editor-fold> // end of Textos Aleatórios region

		//#if NANO_RANKING == "true"
			private Form nanoOnlineForm;
		//#endif

		/** Referencia para a tela de jogo, para que seja possivel
		 * retornar a tela de jogo apos entrar na tela de pausa. */
		private GameScreen gameScreen;

		//<editor-fold desc="Static Fields">
			public static short maxReachedLevel; //TODO

			private static boolean lowMemory;

			private static LoadListener loader;
		//</editor-fold>
	//</editor-fold> // end of Fields region

	
	public GameMIDlet() {
		
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
			FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
		//#endif

		//#if DEBUG == "true"
			System.out.println( "sasd" );
		//#endif
		log("GameMIDlet antes");

//		System.out.println(Runtime.getRuntime().freeMemory());
	}

	
	public static final void setSpecialKeyMapping(boolean specialMapping) {
		try {
			ScreenManager.resetSpecialKeysTable();
			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

			int[][] keys = null;
			final int offset = 'a' - 'A';

			//#if BLACKBERRY_API == "true"
//# 				switch ( Keypad.getHardwareLayout() ) {
//# 					case ScreenManager.HW_LAYOUT_REDUCED:
//# 					case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 						keys = new int[][] {
//# 							{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 							{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 							{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 							{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 							{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 							{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 							{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 							{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//# 
//# 							{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 							{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 							{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 							{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 							{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 							{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 							{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 							{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 							{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 							{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//# 
//# 							{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 						 };
//# 					break;
//# 
//# 					default:
//# 						if ( specialMapping ) {
//# 							keys = new int[][] {
//# 								{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 								{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 								{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 								{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 								{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 								{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 								{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 								{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 								{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//# 
//# 								{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 								{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 								{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 								{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 								{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 								{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 								{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 								{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 								{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//# 
//# 								{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 								{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 							 };
//# 						} else {
//# 							for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 								table.put( new Integer( c ), new Integer( c ) );
//# 								table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 							}
//# 
//# 							final int[] chars = new int[]
//# 							{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 								'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 							};
//# 
//# 							for ( byte i = 0; i < chars.length; ++i )
//# 								table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 						}
//# 				}
//# 
			//#else

				if ( specialMapping ) {
					keys = new int[][] {
						{ 'q', ScreenManager.KEY_NUM1 },
						{ 'Q', ScreenManager.KEY_NUM1 },
						{ 'e', ScreenManager.KEY_NUM3 },
						{ 'E', ScreenManager.KEY_NUM3 },
						{ 'z', ScreenManager.KEY_NUM7 },
						{ 'Z', ScreenManager.KEY_NUM7 },
						{ 'c', ScreenManager.KEY_NUM9 },
						{ 'C', ScreenManager.KEY_NUM9 },
						{ 'w', ScreenManager.UP },
						{ 'W', ScreenManager.UP },
						{ 'a', ScreenManager.LEFT },
						{ 'A', ScreenManager.LEFT },
						{ 's', ScreenManager.FIRE },
						{ 'S', ScreenManager.FIRE },
						{ 'd', ScreenManager.RIGHT },
						{ 'D', ScreenManager.RIGHT },
						{ 'x', ScreenManager.DOWN },
						{ 'X', ScreenManager.DOWN },

						{ 'r', ScreenManager.KEY_NUM1 },
						{ 'R', ScreenManager.KEY_NUM1 },
						{ 'y', ScreenManager.KEY_NUM3 },
						{ 'Y', ScreenManager.KEY_NUM3 },
						{ 'v', ScreenManager.KEY_NUM7 },
						{ 'V', ScreenManager.KEY_NUM7 },
						{ 'n', ScreenManager.KEY_NUM9 },
						{ 'N', ScreenManager.KEY_NUM9 },
						{ 't', ScreenManager.KEY_NUM2 },
						{ 'T', ScreenManager.KEY_NUM2 },
						{ 'f', ScreenManager.KEY_NUM4 },
						{ 'F', ScreenManager.KEY_NUM4 },
						{ 'g', ScreenManager.KEY_NUM5 },
						{ 'G', ScreenManager.KEY_NUM5 },
						{ 'h', ScreenManager.KEY_NUM6 },
						{ 'H', ScreenManager.KEY_NUM6 },
						{ 'b', ScreenManager.KEY_NUM8 },
						{ 'B', ScreenManager.KEY_NUM8 },

						{ 10, ScreenManager.FIRE }, // ENTER
						{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

						{ 'u', ScreenManager.KEY_STAR },
						{ 'U', ScreenManager.KEY_STAR },
						{ 'j', ScreenManager.KEY_STAR },
						{ 'J', ScreenManager.KEY_STAR },
						{ '#', ScreenManager.KEY_STAR },
						{ '*', ScreenManager.KEY_STAR },
						{ 'm', ScreenManager.KEY_STAR },
						{ 'M', ScreenManager.KEY_STAR },
						{ 'p', ScreenManager.KEY_STAR },
						{ 'P', ScreenManager.KEY_STAR },
						{ ' ', ScreenManager.KEY_STAR },
						{ '$', ScreenManager.KEY_STAR },
					 };
				} else {
					for ( char c = 'A'; c <= 'Z'; ++c ) {
						table.put( new Integer( c ), new Integer( c ) );
						table.put( new Integer( c + offset ), new Integer( c + offset ) );
					}

					final int[] chars = new int[]
					{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
						'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
					};

					for ( byte i = 0; i < chars.length; ++i )
						table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
				}
			//#endif

			if ( keys != null ) {
				for ( byte i = 0; i < keys.length; ++i )
					table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				log( e.getClass() + e.getMessage() );
				e.printStackTrace();
			//#endif
		}
	}


	//#if DEBUG == "true"
		public final void start() {
			log( "start 1" );
			super.start();
			log( "start 2" );
		}
	//#endif

	public static final void setTextFontColor( int color ) {
		((GameMIDlet)getInstance()).setTextFontColor( color );
	}


	//<editor-fold desc="Load and Unload">
		protected final void loadResources() throws Exception {
			log("loadResources início");

			//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 	 			Vector v = new Vector();
//# 	 			final int MEMORY_INCREASE = 170 * 1000; // size: 170kb
//# 	 			int total = 0;
//# 	 		    try {
//# 	 				while( Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT ) {
//# 	 					v.addElement(new byte[ MEMORY_INCREASE ]);
//# 	 					total += MEMORY_INCREASE;
//# 	 				}
//# 	 				lowMemory = false;
//# 	 			} catch( Throwable e ) {
//# 	 				lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT && total < LOW_MEMORY_LIMIT;
//# 	 			} finally {
//# 	 				v = null;
//# 	 				System.gc();
//# 	 			}
			//#endif
	//	   lowMemory = true; // TODO forçar modo LowMemmory

			for (byte i = 0; i < FONT_TYPES_TOTAL; ++i) {
				//#if DEBUG == "true"
					System.out.println( "FONTE " + i );
				//#endif

				switch ( i ) {
						//#if VERSION == "GH"
//#  						case FONT_TEXT_WHITE:
//#  							PaletteMap[] pm = new PaletteMap[] { new PaletteMap( 0x000000, 0xffffff ) };
//#  							PaletteChanger pc = new PaletteChanger( PATH_IMAGES + "font_" + FONT_TEXT_BLACK + ".png" );
//#  							FONTS[ i ] = ImageFont.createMultiSpacedFont( pc.createImage( pm ), PATH_IMAGES + "font_" + FONT_TEXT_BLACK + ".bin" );
//#  						break;
						//#endif
						
//					case FONT_MENU:
//						FONTS[ i ] = ImageFont.createSpriteFont( PATH_IMAGES + "font_" + i );
//					break;

					default:
						//#if VERSION == "GH"
//# 							FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i );
						//#else
							FONTS[ i ] = ImageFont.createSpriteFont( PATH_IMAGES + "font_" + i );
						//#endif
				}

				if( isLowMemory() ) {
					FONTS[ FONT_TEXT_BLACK ] = FONTS[ FONT_MENU ];
				}

				switch ( i ) {
					case FONT_MENU:
					case FONT_MENU_NO_FILL:
						if ( FONTS[ i ] != null ) {
							//#if VERSION == "GH"
//# 						FONTS[ i ].setCharExtraOffset( 0 );
							//#else
								FONTS[ i ].setCharExtraOffset( 0 );
							//#endif
						}
					break;

					case FONT_MENU_BIG:
					case FONT_MENU_BIG_NO_FILL:
						if ( FONTS[ i ] != null ) {
							//#if VERSION == "GH"
//# 								FONTS[ i ].setCharExtraOffset( -2 );
							//#else
								FONTS[ i ].setCharExtraOffset( 4 );
							//#endif
						}
					break;

					default:
						if ( FONTS[ i ] != null ) {
							FONTS[ i ].setCharExtraOffset( DEFAULT_FONT_OFFSET );
						}
				}
			}

			BRAND_LOGO = new DrawableImage( PATH_IMAGES + "GH_logo.png" );
			log( "Brand logo" );

			BAR_PATTERN = new DrawableImage( PATH_IMAGES + "bar.png" );
			log( "BAR_PATTERN" );

			ARROW = new DrawableImage( PATH_CHARSELECTION_IMAGES + "arrow.png" );
			log( "ARROW" );

			ARROW_PRESSED = new DrawableImage( PATH_CHARSELECTION_IMAGES + "arrowp.png" );
			log( "ARROW_PRESSED" );

			language = 2; //nanoOnline.LANGUAGE_pt_BR;
			setLanguage( language );
			log( "textos" );

			log( "SoftButton" );

			setSpecialKeyMapping(true);
			log("loadResources fim");

			log( "create database" );
			// cria a base de dados do jogo
			try {
				createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
			} catch ( Exception e ) {
			}

			final String[] soundList = new String[SOUND_TOTAL];
			for (byte i = 0; i < SOUND_TOTAL; ++i) {
				soundList[i] = PATH_SOUNDS + i + ".mid";
			}
			MediaPlayer.init(DATABASE_NAME, DATABASE_SLOT_OPTIONS, soundList);
			loadOptions();

			setScreen( SCREEN_GAME );
		}

		protected final void loadRandomTexts(final int totalTexts, final String[] texts, String path) throws Exception {
			//#if TRY_CATCH_ALL == "true"
//#  		try {
			//#endif

			// escolhendo indices dos textos selecionados
			final int sizeOfTexts = texts.length;
			final int[] selectedIndexes = new int[sizeOfTexts];
			final int partitionSize = totalTexts / sizeOfTexts;
			final int randomIndexOffset = NanoMath.randFixed(sizeOfTexts);
			for (int i = 0; i < sizeOfTexts; i++) {
				selectedIndexes[i] = i*partitionSize + NanoMath.randFixed(partitionSize);
			}

			openJarFile( path, new Serializable() {

				public final void write( DataOutputStream output ) throws Exception {
				}

				public final void read( DataInputStream input ) throws Exception {
					final char[] buffer = new char[ 2048 ];
					int bufferIndex = 0;

					short currentString = 0;
					short randomIndex = 0;
					int descriptorsIndex = randomIndexOffset;

					try {
						while ( currentString < totalTexts ) {
							// se o texto estiver em UTF8, o converte
							byte b = input.readByte(); // TODO não ler de byte em byte para

							if ( b < 0 )
								b = ( byte ) ( b - 0x100 );

							char c = ( char ) b;

							if ( c >= 0xC0 ) {
								c = ( char ) ( ( ( c << 8 ) | input.readUnsignedByte() ) - 0xc2c0 );
							} else if ( c >= 0x80 ) {
								c = ( char ) ( c - 0xc200 );
							}

							switch ( c ) {
								case '\r':
									break;

								case '\n':
									// chegou ao final de uma string.
									// enquanto o índice da linha não for maior, não a queremos
									if ( currentString >= selectedIndexes[randomIndex] ) {
										// Caso a linha esteja vazia, utiliza uma string vazia (não
										// fazer esse teste lança uma exceção)
										descriptorsIndex = ( descriptorsIndex + 1 ) % sizeOfTexts;
										texts[descriptorsIndex] = ( bufferIndex > 0 ) ? new String( buffer, 0, bufferIndex ) : "";
										if( ++randomIndex >= selectedIndexes.length )
											return;
									}

									currentString++;
									bufferIndex = 0;
									break;

								case '\\':
									buffer[bufferIndex++] = '\n';
									break;

								default:
									buffer[bufferIndex++] = c;
							} // fim switch ( c )
						} // fim while ( currentString < TEXT_TOTAL )
					} catch ( EOFException eof ) {
						if ( currentString < totalTexts ) {
							// chegou ao final do arquivo antes de ler todos os textos
							//#if DEBUG == "true"
							throw new Exception( "erro ao ler textos: fim de arquivo detectado antes da leitura de todos os textos." );
							//#else
//# 							throw eof;
						//#endif
						} // fim if ( currentString < TOTAL_TEXTS )
					} // fim catch ( EOFException )
				} // fim do método read( DataInputStream )
			} );

		//#if TRY_CATCH_ALL == "true"
	//#  		} catch ( Throwable e ) {
	//#  			AppMIDlet.log( e , "53");
	//#  		}
		//#endif
		}

		private final void loadAllRandomTexts(){
			try {
				loadRandomTexts(TOTAL_DESCRIPTORS, manNames, PATH_TEXT + "boys_" + language + ".dat");
				loadRandomTexts(TOTAL_DESCRIPTORS, womanNames, PATH_TEXT + "girls_" + language + ".dat");
				loadRandomTexts(TOTAL_DESCRIPTORS, foodNames, PATH_TEXT + "food_" + language + ".dat");
				loadRandomTexts(TOTAL_DESCRIPTORS, hobbyNames, PATH_TEXT + "hobby_" + language + ".dat");
				//loadRandomTexts(NUMBER_OF_CARDS_TOTAL, cardContents, PATH_TEXT + "cards_" + language + ".dat");

				//#if DEBUG == "true"
					for (int i = 1; i <= PARTICIPANTS_NUMBER; i++) {
						System.out.println( "Participante: " + getName(i) );
						System.out.println( "Comida Favorita: " + getFavoriteFood(i) );
						System.out.println( "Hobby: " + getFavoriteHobby(i) );
					}
				//#endif
			} catch (Exception ex) {
				//#if DEBUG == "true"
				ex.printStackTrace(); 
				//#endif
			}
		}
		
		
		public static final void saveOptions() {
			try {
				AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA, ( ( GameMIDlet ) instance ) );
				MediaPlayer.saveOptions();
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		} // fim do método saveOptions()

		public final void loadOptions() {
			try {
				AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_GAME_DATA, this );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		} // fim do método loadOptions()
	//</editor-fold> // end of Load and Unload region


	protected final void changeLanguage(byte language) {
		GameMIDlet.log("changeLanguage início");
		//language = NanoOnline.LANGUAGE_pt_BR;
		try {
			// carregando textos da interface
			loadTexts(TEXT_TOTAL, PATH_TEXT + "texts_" + language + ".dat");

			manNames = new String[PARTICIPANTS_NUMBER / 2];
			womanNames = new String[PARTICIPANTS_NUMBER / 2];
			foodNames = new String[PARTICIPANTS_NUMBER];
			hobbyNames = new String[PARTICIPANTS_NUMBER];
			//cardContents = new String[NUMBER_OF_INGAME_CARDS];

			loadAllRandomTexts();
		} catch (Exception ex) {
			//#if DEBUG == "true"
			ex.printStackTrace();
			//#endif
		}
		GameMIDlet.log("changeLanguage fim");
	}
	
	public final void destroy(){
		saveOptions();

		super.destroy();
	}
	

	//#if DEBUG == "true"
		private void logScreen( int screen, String message ) {
			switch( screen ) {
				case SCREEN_CHOOSE_SOUND: message += "CHOOSE_SOUND"; break;
				case SCREEN_SPLASH_NANO: message += "SPLASH_NANO"; break;
				case SCREEN_SPLASH_BRAND: message += "SPLASH_BRAND"; break;
				case SCREEN_SPLASH_GAME: message += "SPLASH_GAME"; break;
				case SCREEN_MAIN_MENU: message += "MAIN_MENU"; break;
				case SCREEN_GAME: message += "NEW_GAME"; break;
				case SCREEN_OPTIONS: message += "OPTIONS"; break;
				case SCREEN_HELP: message += "HELP"; break;
				//case SCREEN_HELP_PROFILE: message += "HELP_PROFILE";	break;
				case SCREEN_CREDITS: message += "CREDITS"; break;
				case SCREEN_LOADING_GAME: message += "LOADING_GAME"; break;
				case SCREEN_CHOOSE_PROFILE: message += "CHOOSE_PROFILE"; break;
				case SCREEN_LOG: message += "LOG"; break;
				default: message += ""+screen;
			}
			log( message );
		}
	//#endif


	protected final int changeScreen(int screen) throws Exception {
		final GameMIDlet midlet = (GameMIDlet) instance;

		//#if DEBUG == "true"
			logScreen( screen, " ---=> Changed Screen: " );
		//#endif

		Drawable nextScreen = null;
		byte bkgType = BACKGROUND_TYPE_NONE; // padrão de fundo, se não for tem que especificar dependendo do caso no switch
		setSpecialKeyMapping( true );

		switch (screen) {
			case SCREEN_SPLASH_NANO:
				//#if SCREEN_SIZE != "SMALL"
					nextScreen = new BasicSplashNano(SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_MEDIUM,
															PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#else
//# 				nextScreen = new BasicSplashNano(SCREEN_SPLASH_BRAND, BasicSplashNano.SCREEN_SIZE_SMALL,
//# 														PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
				//#endif
				bkgType = BACKGROUND_TYPE_BLACK;
			break;

			case SCREEN_SPLASH_BRAND:
				nextScreen = new SplashEndemol();
				bkgType = BACKGROUND_TYPE_BLACK;
			break;

			case SCREEN_MENU_GAME:
				nextScreen = new OptionsMenu( this, screen, OptionsMenu.OPTIONS_ON_GAME, null );
			break;

			case SCREEN_OPTIONS:
				nextScreen = new OptionsMenu( this, screen, OptionsMenu.OPTIONS_ON_MAIN, null );
			break;

			//#if DEBUG == "true"
				case SCREEN_LOG:
				break;
			//#endif

			case SCREEN_HELP_MENU:
				nextScreen = new OptionsMenu( this, screen, OptionsMenu.OPTIONS_ON_HELP, null );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_HELP:
				Drawable[] images = ( helpOptionSelected != TEXT_OPTION_ATTRIBUTES && helpOptionSelected != TEXT_OPTION_ACTIVITIES ) ? null :
										new Drawable[]{ new DrawableImage( PATH_GAME_IMAGES + "icon_charisma.png"),
													  new DrawableImage( PATH_GAME_IMAGES + "icon_resistance.png"),
													  new DrawableImage( PATH_GAME_IMAGES + "icon_knowlegde.png"),
													  new DrawableImage( PATH_GAME_IMAGES + "icon_beauty.png") };
				nextScreen = new TextScreen( 1, this, SCREEN_HELP, getText( TEXT_HELP_HOW_TO_PLAY + helpOptionSelected - TEXT_OPTION_HOW_TO_PLAY ), false, images );
			break;

			//case SCREEN_CHOOSE_PROFILE:
			//break;

			case SCREEN_SPLASH_GAME:
//				nextScreen = new GameSplashScreen( this, screen );
//			break;
				screen = SCREEN_MAIN_MENU;

			case SCREEN_MAIN_MENU:
				nextScreen = new MainMenuScreen( this, screen );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_GAME:
				if( gameScreen == null ) {
					bkgType = BACKGROUND_TYPE_SOLID_COLOR;
					nextScreen = new LoadScreen( new LoadListener() {
						public final void load(final LoadScreen loadScreen) throws Exception {
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

							GameScreen.load();

							gameScreen = new GameScreen( midlet );
							GameMIDlet.log( "gameScreen" );

							loadScreen.setActive(false);
							setScreen( SCREEN_GAME );
						}
					} );
				} else {
					nextScreen = gameScreen;
				}
			break;

		} // fim switch ( screen )

		if( nextScreen != null ) nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		setBackground( bkgType );

		//#if DEBUG == "true"
			if ( nextScreen == null )
				throw new IllegalStateException( "Warning: nextScreen null (" + screen + ")." );
		//#endif

//		ScreenManager.getInstance().setCurrentScreen( nextScreen );
		midlet.manager.setCurrentScreen( nextScreen );

		//#if DEBUG == "true"
			logScreen( screen, " ---=> Exit Screen: " );
		//#endif

		return screen;
	} // fim do método changeScreen( int )
	
	public final static void reset() {
		((GameMIDlet)getInstance()).resetGame();
	}
	
	public final void resetGame() {
		gameScreen = null;
		setScreen( SCREEN_GAME );
	}


	//<editor-fold desc="Getters and Setters">
		//<editor-fold desc="Static Getters and Setters">
			public static DrawableImage getLogo() {	return BRAND_LOGO; }
			public static DrawableImage getBar() { return BAR_PATTERN; }
			public static DrawableImage getArrow() { return ARROW; }
			public static DrawableImage getArrowPressed() { return ARROW_PRESSED; }


			public static final String getRecommendURL() {
				final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
				if ( url == null )
					return URL_DEFAULT;

				return url;
			}
			private static final String getVersion() {
				String version = instance.getAppProperty( "MIDlet-Version" );
				if ( version == null )
					version = "";

				return "<ALN_H>" + getText( TEXT_VERSION ) + version + "\n\n";
			}

			public static final boolean isLowMemory() {
				return lowMemory;
			}

			private static final void setBackground( byte type ) {
		//		bkg.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_WIDTH);

				final GameMIDlet midlet = ( GameMIDlet ) instance;

				switch ( type ) {
					case BACKGROUND_TYPE_NONE:
						midlet.manager.setBackground( null, false );
						midlet.manager.setBackgroundColor( -1 );
					break;

					case BACKGROUND_TYPE_SOLID_COLOR:

						midlet.manager.setBackground( null, false );
						midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
					break;

					case BACKGROUND_TYPE_BLACK:
						midlet.manager.setBackground( null, false );
						midlet.manager.setBackgroundColor( 0 );
					default:
				}
			} // fim do método setBackground( byte )

			//<editor-fold desc="Participants Info Getters and Setters">

				public static final int getPlayerId() {
					return playerId;
				}

				/**
				 * Determina personagem que será utilizado pelo jogador.
				 * @param playerId Índice do participante escolhido pelo jogador.
				 */
				public static final void setPlayerId( int playerId ) {
					GameMIDlet.playerId = playerId;
				}


				public static final String getName( int id ) {
					if( id % 2 == 0 )
						// ids pares são os de homens
						return manNames[ ( id/2 ) - 1 ];
					else
						return womanNames[ ( id - 1 ) / 2 ];
				}

				public static final String getFavoriteFood( int id ) {
					return foodNames[ id - 1 ];
				}

				public static final String getFavoriteHobby( int id ) {
					return hobbyNames[ id - 1 ];
				}

				//public static final String getCardContent( int id ) {
				//	return cardContents[ id ];
				//}
			//</editor-fold>

		//</editor-fold> // end of Static Getters and Setters region

		protected final ImageFont getFont( int index ) {
			try {
				//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 					switch ( index ) {
//# 						case FONT_TEXT_WHITE:
//# 							if ( isLowMemory() ) {
//# 								final ImageFont font = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + index );
//# 								font.setCharExtraOffset( DEFAULT_FONT_OFFSET );
//# 								return font;
//# 							}
//# 						break;
//# 					}
				//#endif

				return FONTS[ index ];
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
					ex.printStackTrace();
				//#endif
				return null;
			}
		}
	//</editor-fold> // end of Getters and Setters region
			
	
	public final void write(DataOutputStream output) throws Exception {
		output.writeShort( maxReachedLevel );
//		output.writeBoolean( sound );
//		output.writeBoolean( vibration );

		//#if DEBUG == "true"
			System.out.println("------------write--------------");
			System.out.println("maxReachedLevel: " + maxReachedLevel);
//			System.out.println("sound: " + sound);
//			System.out.println("vibration: " + vibration);
			System.out.println("----------end-write------------");
		//#endif
	}

	public final void read( DataInputStream input ) throws Exception {
		maxReachedLevel = input.readShort();
//		sound = input.readBoolean();
//		vibration = input.readBoolean();
		
		//#if DEBUG == "true"
			System.out.println("-------------read--------------");
			System.out.println("maxReachedLevel: " + maxReachedLevel);
//			System.out.println("sound: " + sound);
//			System.out.println("vibration: " + vibration);
			System.out.println("-----------end-read------------");
		//#endif
	}
	
	
	//<editor-fold desc="Event Handler">
		/**
		 * Controla as trocas de tela do jogo.
		 * Essa função centraliza todas as trocas de tela do jogo, com o
		 * proposito de organizar o código.
		 * @param menu o Menu que requisitou a troca de tela. Caso seja NULL, a requisição
		 * nao veio de um Menu.
		 * @param id índice da tela
		 * @param index índice interno, específico da tela.
		 * @see Menu
		 */
		public void onChoose(Menu menu, int id, int index) {
			switch(id) {
				case SCREEN_MAIN_MENU: {
					switch(index) {
						case TEXT_NEW_GAME:
							setScreen( SCREEN_GAME );							
						break;
						//#if DEBUG == "true"
							case TEXT_LOADING:
								setScreen( SCREEN_LOG );
							break;
						//#endif

						case TEXT_OPTIONS:
							setScreen( SCREEN_OPTIONS );
						break;

						case TEXT_HELP:
							setScreen( SCREEN_HELP_MENU );
						break;

						case TEXT_CREDITS:
							setScreen( SCREEN_CREDITS );
						break;

						case TEXT_EXIT:
							exit();
						break;
					}
				}
				break;

				case SCREEN_CREDITS:
					setScreen( SCREEN_MAIN_MENU );
				break;

				case SCREEN_HELP:
					setScreen( SCREEN_HELP_MENU );
					break;

				case SCREEN_HELP_MENU:
					switch(index) {
						case TEXT_EXIT:
							setScreen( SCREEN_MAIN_MENU );
						break;

						default:
							helpOptionSelected = index;
							setScreen( SCREEN_HELP );
						break;
					}
					break;

				//#if DEBUG == "true"
					case SCREEN_LOG:
						MediaPlayer.stop();
						setScreen( SCREEN_MAIN_MENU );
						break;
				//#endif

				case SCREEN_OPTIONS:
					switch(index) {
						case TEXT_EXIT:
							OptionsMenu.save();
							setScreen( SCREEN_MAIN_MENU );
						break;
					}
				break;

				case SCREEN_MENU_GAME:
					switch(index) {
						case TEXT_EXIT:
							OptionsMenu.save();
							setScreen( SCREEN_GAME );
						break;

						case TEXT_BACK_MENU:
							OptionsMenu.save();
							setScreen( SCREEN_MAIN_MENU );
						break;
						
						case TEXT_EXIT_GAME:
							OptionsMenu.save();
							exit();
						break;
					}
				break;
					
				case SCREEN_GAME_OVER:
					setScreen( SCREEN_MAIN_MENU );
				break;

				case SCREEN_SPLASH_GAME:
					setScreen( SCREEN_MAIN_MENU );
				break;
			}
		}

		public void onItemChanged(Menu menu, int id, int index) {
		}
	//</editor-fold> // end of Event Handler region
}
