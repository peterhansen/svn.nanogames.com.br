package screens;

import javax.microedition.lcdui.Graphics;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.ScrollRichLabel;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
//#if TOUCH == "true"
import core.GestureManager;
//#endif
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.BasicScreen;

/**
 * Classe que simboliza a tela de menu principal do jogo.
 * @author Ygor
 */
public class MainMenuScreen extends BasicScreen implements MenuListener {

	private static final int PRESSED_FONT = FONT_MENU_BIG_NO_FILL;
	private static final int UNPRESSED_FONT = FONT_MENU_BIG;

	private final RichLabel menuOption;
	private final ScrollRichLabel menuOptionText;
	//#if VERSION == "GH"
//# 		private final DrawableImage bottomPattern;
	//#endif

	/** Tempo de espera para que o texto comece a fazer o scroll automático. */
	private static final int SCROLL_WAITING_TIME = 500;
	private static int scrollTime = 0;
	private static boolean isScrolling = false;
	
	
	private boolean menuIsPressed = false;
	
	private final Rectangle labelArea = new Rectangle();

	MainMenuScreen( GameMIDlet midlet, int screen ) throws Exception {
		super( 10, midlet, screen, BARS_MIDDLE, true, COLOR_MAIN_MENU_BKG );
		
		//#if VERSION == "GH"
//# 			bottomPattern = new DrawableImage( topBar );
//# 			bottomPattern.setTransform( TRANS_MIRROR_H );
//# 			bottomPattern.defineReferencePixel( ANCHOR_BOTTOM_LEFT );
//# 			insertDrawable( bottomPattern, 1 );
		//#endif

		menuOption = new RichLabel( FONT_MENU_BIG );
		menuOptionText = new ScrollRichLabel( new RichLabel( FONT_TEXT_WHITE ) );
		insertDrawable( menuOption );
		insertDrawable( menuOptionText );

		maxIndex = TEXT_EXIT - TEXT_NEW_GAME + 1;
		
		updateIndex();
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
	}
	

	public final void setSize(int width, int height) {
		super.setSize( width, height );

		final int arrowsY;
		if( width >= height ) {
			logo.setPosition( ( middleBarArea.width - logo.getWidth() ) >> 1, middleBarArea.y + ( 3 * SAFE_MARGIN >> 2 ) );
			arrowsY = (SAFE_MARGIN >> 1) + logo.getPosY() + logo.getHeight() + ( ( middleBarArea.height - logo.getPosY() - logo.getHeight() ) >> 1 );
		} else {
			arrowsY = middleBarArea.y + ( middleBarArea.height >> 1 );
		}

		menuOption.setSize( middleBarArea.width - 2 * ( SAFE_MARGIN + arrowLeft.getWidth() ), menuOption.getFont().getHeight() * 2 );
		menuOption.setPosition( middleBarArea.x + SAFE_MARGIN + arrowLeft.getWidth(), arrowsY - ( ( menuOption.getHeight() ) >> 1 ) );

		arrowLeft.setRefPixelPosition( menuOption.getPosX() - ( SAFE_MARGIN << 1 ), arrowsY );
		arrowRight.setRefPixelPosition( menuOption.getPosX() + menuOption.getWidth() + ( SAFE_MARGIN << 1 ), arrowLeft.getRefPixelY() );
		arrowLeftPressed.setRefPixelPosition( arrowLeft.getRefPixelPosition() );
		arrowRightPressed.setRefPixelPosition( arrowRight.getRefPixelPosition() );

		menuOptionText.setSize( NanoMath.min( freeArea.width - 2 * SAFE_MARGIN, SCREEN_FREEAREA_MAX_WIDTH ), freeArea.height );
		menuOptionText.setPosition( freeArea.x + ( ( freeArea.width - menuOptionText.getWidth() ) >> 1 ), freeArea.y );
		
		//#if VERSION == "GH"
//# 			bottomPattern.setRefPixelPosition( bottomBar.getPosX(), bottomBar.getPosY() );
		//#endif

		//#if TOUCH == "true"
			GestureManager.setTouchArea( arrowLeftArea, arrowLeft.getPosition(), arrowLeft.getSize(), TOUCH_TOLERANCE );
			GestureManager.setTouchArea( arrowRightArea, arrowRight.getPosition(), arrowRight.getSize(), TOUCH_TOLERANCE );
			GestureManager.setTouchArea( labelArea, menuOption.getPosition(), menuOption.getSize(), TOUCH_TOLERANCE );
		//#endif

		updateIndex();
	}
	

	public void internalUpdate( int delta ) {
		// atualiza situacao do scroll do texto de descricao da opcao
		if( !isScrolling ) {
			scrollTime += delta;
			if( scrollTime > SCROLL_WAITING_TIME ) {
				isScrolling = true;
				menuOptionText.setAutoScroll( menuOptionText.getTextTotalHeight() > menuOptionText.getHeight() );
			}
		}
	}
	

	public void drawFrontLayer( Graphics g ) {
		(arrowLeftIsPressed? arrowLeftPressed: arrowLeft).draw( g );
		(arrowRightIsPressed? arrowRightPressed: arrowRight).draw( g );
		(okButtonIsPressed? okButtonPressed: okButton).draw( g );
		(backButtonIsPressed? backButtonPressed: backButton).draw( g );
	}
	
	
	public void onChoose( Menu menu, int id, int index ) { }
	public void onItemChanged( Menu menu, int id, int index ) { }

	
	/** Atualiza textos da opçao do menu e de sua descricao. */
	protected void updateIndex() {
		menuOption.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_NEW_GAME + currentIndex ) + "</ALN_H>", true );
		if( menuOption.getLinesInfo().length == 1 ) {
			menuOption.setTextOffset( menuOption.getHeight() >> 2 );
		}

		scrollTime = 0;
		isScrolling = false;
		menuOptionText.setText( "<ALN_H>" + GameMIDlet.getText( TEXT_NEW_GAME_DESC + currentIndex ) + "</ALN_H>", true, false );
		
		//#if VERSION == "BBB"
//# 			if( TEXT_NEW_GAME + currentIndex == TEXT_CREDITS ) {
//# 				roboAzul.setVisible( false );
//# 				roboLaranja.setVisible( false );
//# 				roboVerde.setVisible( false );
//# 			} else {
//# 				roboAzul.setVisible( true );
//# 				roboLaranja.setVisible( true );
//# 				roboVerde.setVisible( true );
//# 			}
		//#endif
			
		menuOptionText.setAutoScroll( false );
	}
	

	//<editor-fold desc="Handle Input">
		protected void okAction() {
			switch ( TEXT_NEW_GAME + currentIndex ) {
				case TEXT_NEW_GAME:
					midlet.onChoose(null, SCREEN_MAIN_MENU, TEXT_NEW_GAME);
				break;

				case TEXT_OPTIONS:
					midlet.onChoose(null, SCREEN_MAIN_MENU, TEXT_OPTIONS);
				break;

				case TEXT_NANO_ONLINE:
					// TODO: Nano Online
				break;

				case TEXT_HELP:
					midlet.onChoose(null, SCREEN_MAIN_MENU, TEXT_HELP);
				break;

				case TEXT_EXIT:
					backAction();
				break;
			}
		}
		

		protected void backAction() {
			midlet.onChoose(null, SCREEN_MAIN_MENU, TEXT_EXIT);
		}


		public void internalKeyPressed( int key ) {
			switch( key ) {
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_FIRE:
					menuOption.setFont( PRESSED_FONT );
					menuIsPressed = true;
				break;
			}
		}


		public void internalKeyReleased( int key ) {
			switch( key ) {
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_FIRE:
					if( menuIsPressed ) {
						menuOption.setFont( UNPRESSED_FONT );
						okAction();
						menuIsPressed = false;
					}
				break;
			}
		}


		//#if TOUCH == "true"
			protected void internalHandleTap( int x, int y ) {
				if( ( labelArea.contains( x, y ) || menuOptionText.contains( x, y ) ) && ( !arrowLeftArea.contains( x, y ) ) && ( !arrowRightArea.contains( x, y ) ) ) { okAction(); }
			}

			protected void internalFirstInteraction( int x, int y ) {
				if( ( labelArea.contains( x, y ) || menuOptionText.contains( x, y ) ) && ( !arrowLeftArea.contains( x, y ) ) && ( !arrowRightArea.contains( x, y ) ) ) { menuOption.setFont( PRESSED_FONT ); }
			}

			protected void internalHandleDrag( int x, int y, boolean canBeTap, int dx, int dy ) {
				if( canBeTap && ( labelArea.contains( x, y ) || menuOptionText.contains( x, y ) ) && ( !arrowLeftArea.contains( x, y ) ) && ( !arrowRightArea.contains( x, y ) ) ) { menuOption.setFont( PRESSED_FONT ); }
			}

			protected void internalHandleRelease( int x, int y ) {
				menuOption.setFont( UNPRESSED_FONT );
			}

			protected void internalGestureIsNotSpot( int x, int y ) {
				menuOption.setFont( UNPRESSED_FONT );
			}
		//#endif
	//</editor-fold>
}