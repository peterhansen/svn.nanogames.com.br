/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author Caio
 */
public class InteractiveImage extends Drawable {
	private static final int ROTATION_TOLERANCE = 0; //4; // in degrees
	private static final int SCALE_TOLERANCE = 0;   // in percentage
	
	private Image originalImage;
	private Image moddedImage;
		
	private Point scale = new Point();
	private short rotation;
	
	private Point moddedScale = new Point();
	private Point moddedOrigin = new Point();
	private short moddedRotation;
		
	
	public InteractiveImage( Image image ) {
		super();
		
		this.originalImage = image;
		scale.set( NanoMath.ONE << 3, NanoMath.ONE << 3 );
		rotation = 0;
		moddedRotation = -360; //says that moddedImage do not exists
		updateModdedImage();
	}
	

	public final void paint( Graphics g ) {
		if( moddedImage != null ) {
			g.setColor( 0xff0000 );
			g.fillRect( translate.x + getRefPixelX() - moddedOrigin.x, translate.y + getRefPixelX() - moddedOrigin.y, moddedImage.getWidth(), moddedImage.getHeight() );
			g.drawImage( moddedImage, translate.x + getRefPixelX() - moddedOrigin.x, translate.y + getRefPixelX() - moddedOrigin.y, 0 );
		}
	}
	
	
	public static Image rotateImage( Image src, int angle, Point scale ) {
		final int sw = src.getWidth();
		final int sh = src.getHeight();
		int[] srcData = new int[ sw * sh ];
		
		src.getRGB( srcData, 0, sw, 0, 0, sw, sh );
		final int mw = NanoMath.toInt( NanoMath.mulFixed( NanoMath.abs( NanoMath.sinInt( angle ) * sh ) + NanoMath.abs( NanoMath.cosInt( angle ) * sw ), scale.x ) );
		final int mh = NanoMath.toInt( NanoMath.mulFixed( NanoMath.abs( NanoMath.sinInt( angle ) * sw ) + NanoMath.abs( NanoMath.cosInt( angle ) * sh ), scale.y ) );
		
//		//#if DEBUG == "true"
//			System.out.println( "( " + sw + ", " + sh + " ) -> ( " + mw + ", " + mh + " )" );
//		//#endif
			
		int[] dstData = new int[ mw * mh ];
		
		final int isa = NanoMath.toInt( NanoMath.sinInt( angle ) << 8 );
		final int ica = NanoMath.toInt( NanoMath.cosInt( angle ) << 8 );

		int my = - (mh >> 1);
		for(int i = 0; i < mh; i++) {
			int mx = - (mw  >> 1);
			for(int j = 0; j < mw; j++) {
				final int srcx = NanoMath.toInt( NanoMath.divFixed( NanoMath.toFixed( ( mx * ica + my * isa ) >> 8 ), scale.x ) ) + ( ( sw ) >> 1 );
				final int srcy = NanoMath.toInt( NanoMath.divFixed( NanoMath.toFixed( ( -mx * isa + my * ica ) >> 8 ), scale.y ) ) + ( ( sh ) >> 1 );

				if( srcx >= 0 && srcy >= 0 && srcx <= sw - 1 && srcy <= sh - 1 ) { 
					dstData[ j + i * mw ] = srcData[ srcx + srcy * sw ];
				}

				mx++;
			}
			my++;
		}

		return Image.createRGBImage( dstData, mw, mh, true );
	}
	
	
	private final void updateModdedOrigin() {
		final int x = originalImage.getWidth() >> 1;
		final int y = originalImage.getHeight() >> 1;
		moddedOrigin.x = NanoMath.toInt( NanoMath.mulFixed( NanoMath.abs( NanoMath.sinInt( rotation ) * y ) + NanoMath.abs( NanoMath.cosInt( rotation ) * x ) - ( NanoMath.abs( NanoMath.sinInt( rotation ) * originalImage.getHeight() ) + NanoMath.abs( NanoMath.cosInt( rotation ) * originalImage.getWidth() ) ), scale.x ) );
		moddedOrigin.y = NanoMath.toInt( NanoMath.mulFixed( NanoMath.abs( NanoMath.sinInt( rotation ) * x ) + NanoMath.abs( NanoMath.cosInt( rotation ) * y ) - ( NanoMath.abs( NanoMath.sinInt( rotation ) * originalImage.getWidth() ) + NanoMath.abs( NanoMath.cosInt( rotation ) * originalImage.getHeight() ) ), scale.y ) );
		//#if DEBUG == "true"
			System.out.println( "( " + x + ", " + y + ") -> (" + moddedOrigin.x + ", " + moddedOrigin.y + ")" );
		//#endif
	}
	
	
	private final void updateModdedImage() {
		if( NanoMath.abs( ( moddedScale.x - scale.x ) * 100 / scale.x ) > SCALE_TOLERANCE ||
			NanoMath.abs( ( moddedScale.y - scale.y ) * 100 / scale.y ) > SCALE_TOLERANCE || 
			NanoMath.abs( rotation - moddedRotation ) > ROTATION_TOLERANCE || moddedImage == null ) {
			
			if( originalImage != null ) {
				moddedImage = null;
				int WIDTH = NanoMath.toInt( originalImage.getWidth() * scale.x );
				int HEIGHT = NanoMath.toInt( originalImage.getHeight() * scale.y );
				if( WIDTH > 0 && HEIGHT > 0 && originalImage.getWidth() > 0 && originalImage.getHeight() > 0 ) {
					moddedImage = rotateImage( originalImage, rotation, scale );
					updateModdedOrigin();
				}
				
				defineReferencePixel( ANCHOR_CENTER );
				moddedScale.set( scale );
				moddedRotation = rotation;
			}
		}
	}
		
	
	public final short getRot() { return rotation; }
	public final int getScale() { return ( scale.x + scale.y ) >> 1; }
	
	public final void setRotation( short rotation ) { setTransform( rotation, scale.x, scale.y ); }
	
	public final void setScale( int fp_scale ) { setTransform( rotation, fp_scale ); }	
	public final void setScale( int fp_scaleX, int fp_scaleY ) { setTransform( rotation, fp_scaleX, fp_scaleY ); }



	/** Modifia escala e rotação da imagem atual, se necessário pois pode haver tolerância.
	 * @param angulo de rotação desejado entre 0 e 360.
	 * @param escala da imagem na notação de ponto fixo. */
	public final void setTransform( short rotation, int fp_scale ) { setTransform( rotation, fp_scale, fp_scale ); }
	
	
	/** Modifia escala e rotação da imagem atual, se necessário pois pode haver tolerância.
	 * @param angulo de rotação desejado entre 0 e 360.
	 * @param escala no eixo X da imagem na notação de ponto fixo.
	 * @param escala no eixo Y da imagem na notação de ponto fixo. */
	public final void setTransform( short rotation, int fp_scaleX, int fp_scaleY ) {
		this.rotation = (short)( rotation % 360 );
		scale.set( fp_scaleX, fp_scaleY );
		updateModdedImage();
	}
}
