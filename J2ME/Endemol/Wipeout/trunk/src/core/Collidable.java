/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

//#if DEBUG
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.util.NanoMath;
//#endif

/**
 * Represents a physical entity of the game world.
 * @author Ygor
 */
public abstract class Collidable extends DrawableGroup {

	/** The map where this collidable object belongs */
	protected Map map;

	/** Position of this collidable in world coordinates. Unit: meter */
	protected Point worldPosition;

	/** Size of this collidable in world coordinates. Unit: meter */
	protected Point worldSize;

	/** Collision box of this collidable, in world coordinates, relative to its world position.
	 * If this collision box is not set, then the Collidable will use its world position
	 * and size for its calculation.
	 */
	protected Rectangle collisionBox;

	//#if DEBUG
		/** Debug pattern for collision box. */
		protected Pattern debugCollisionPattern = new Pattern( 0x00ff00 );
	//#endif

	public Collidable( int slots, Map map ) {
		super( slots );
		this.map = map;

		//#if DEBUG
			debugCollisionPattern.setPosition( 0, 0 );
			insertDrawable( debugCollisionPattern );
		//#endif
	}

	public Collidable( Map map ) {
		this( 1, map );
	}

	public void setWorldPosition( int fixedX, int fixedY ) {
		worldPosition = new Point( fixedX, fixedY );
		refreshScreenPosition();
	}

	public void setWorldSize( int fixedX, int fixedY ) {
		worldSize = new Point( fixedX, fixedY );
		refreshScreenSize();
	}
	
	public void setMap( Map map ) {
		this.map = map;
	}

	public Point getWorldPosition() {
		return worldPosition;
	}

	public Point getWorldSize() {
		return worldSize;
	}

	/**
	 * Returns the collidable object collision box. If the collision box
	 * has not been explicitly set, then this method returns a Rectamgle
	 * with world position and size.
	 */
	public Rectangle getCollisionBox() {
		// TODO: this calculations will only remain correct
		// if the anchor of the collidable remains ANCHOR_BOTTOM
		if( collisionBox != null ) {
			return new Rectangle(
				worldPosition.x + collisionBox.x,
				worldPosition.y + collisionBox.y,
				collisionBox.width,
				collisionBox.height
			);
		} else {
			return new Rectangle( worldPosition.x, worldPosition.y, worldSize.x, worldSize.y );
		}
	}

	/** Tests whether the collision box of this object intersects with
	 * a specified rectangle.
	 */
	public boolean intersects( Rectangle otherCollisionBox ) {
		return getCollisionBox().intersects( otherCollisionBox );
	}

	/** Refreshes the position of the collidable on the screen.
	 * This method uses the reference pixel by default. */
	public void refreshScreenPosition() {
		Point screenPosition = map.getScreenPosition( worldPosition );
		setRefPixelPosition( screenPosition );
	}

	/** Refreshes the size of the collidable on the screen. 
	 * This method uses the reference pixel by default. */
	public void refreshScreenSize() {

		// TODO: for now, all collidables are drawn from below
		Point screenSize = map.getScreenSize( worldSize );
		defineReferencePixel( ANCHOR_BOTTOM );

		setSize( screenSize );

		//#if DEBUG
			debugCollisionPattern.setSize( collisionBox != null? map.getScreenSize( collisionBox.width, collisionBox.height ): size );
		//#endif
	}
}
