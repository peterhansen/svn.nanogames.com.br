package core;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Caio
 */
public class ParticleEmitter extends UpdatableGroup implements Constants {
	// <editor-fold defaultstate="collapsed" desc="Constantes Estáticas">
		private final Sprite spriteParticle;
		private short activeParticles;
	// </editor-fold>

	private final short MAX_PARTICLES;
	private final short NUMBER_OF_PARTICLES;
	private final Particle[] particles;
    private boolean isActive;

    private int accTime;
    private int timeBetweenParticles;
	
	private final int BORN_MAX_Y_DIFF;
	private final int BORN_MAX_HALF_Y_DIFF;

    private final byte ANGLE_MIN;
    private final byte ANGLE_DIFF;
    private final int SPEED_MIN;
    private final int SPEED_DIFF;
    private final int LIFE_MIN;
    private final int LIFE_DIFF;
    private final Point emissivePoint;

	public ParticleEmitter( Point emissivePoint, int sparksPerSecond, int minAngle, int maxAngle, int minSpeed, int speedRange, int dispersion, int minLife, int diffLife, int maxParticles ) throws Exception {
        super(1);
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		MAX_PARTICLES = (short)maxParticles;
		particles = new Particle[ MAX_PARTICLES ];

		NUMBER_OF_PARTICLES = (short)10;

        setParticlesPerSecond(sparksPerSecond);
        isActive = true;

        spriteParticle = new Sprite( PATH_IMAGES + "particle_spark" );
		for ( short i = 0; i < MAX_PARTICLES; ++i ) {
			particles[ i ] = new Particle(this);
		}

        this.emissivePoint = emissivePoint;

        ANGLE_MIN = (byte)minAngle;
        ANGLE_DIFF = (byte)( maxAngle - minAngle );
        SPEED_MIN = minSpeed;
        SPEED_DIFF = speedRange;
		LIFE_MIN = NanoMath.divInt( minLife, 1000 );
		LIFE_DIFF = NanoMath.divInt( diffLife, 1000 );

        BORN_MAX_Y_DIFF = dispersion;
        BORN_MAX_HALF_Y_DIFF = (dispersion >> 1);
	}

	public final void setParticlesPerSecond(int nOfParticles) {
		if(nOfParticles!=0) timeBetweenParticles = 1000/nOfParticles;
		else timeBetweenParticles = 1000;
	}

	public final void update( int delta ) {
        super.update(delta);

        if(isActive) {
            accTime += delta;
            if(accTime>=timeBetweenParticles) {
				byte numberParticles = (byte)(accTime/timeBetweenParticles); // Calcula o numero de particulas a emitir
                accTime -= numberParticles*timeBetweenParticles; // Retira do tempo acumulado o intervalo entre n particulas
                while(numberParticles>0) { // Emite o numero de particulas desejadas
                    emit();
                    numberParticles--;
                }
            }
        }

		final int delta_fp = NanoMath.divInt( delta, 1000 );
		int i = 0;
		while (  i < activeParticles ) {
			particles[ i ].update( delta_fp );
			if(particles[ i ].visible) { ++i; }						// Se continuar visivel passa para proxima posição
			else {													// Senão
				--activeParticles;									// Decresce o número de particulas
				//particles[ i ] = particles[ activeParticles ];
				particles[ i ].set(particles[ activeParticles ]);	// E copia para posição atual a ultima particula do vetor
			}
		}
	}

	public final void reset() {
		activeParticles = 0;
	}

	public final void setEmissivePosition( int x, int y ) {
		emissivePoint.set(x,y);
	}

	public void turnOff() {
		isActive = false;
	}
	public void turnOn() {
		isActive = true;
	}

    public Point RandSpeed() {
        final int speed = (NanoMath.randFixed(SPEED_DIFF) + SPEED_MIN);
        final int angle = (NanoMath.randInt(ANGLE_DIFF) + ANGLE_MIN);
        return new Point(NanoMath.mulFixed(NanoMath.cosInt(angle), speed) , -NanoMath.mulFixed(NanoMath.sinInt(angle), speed));
    }

    public Point RandPosition() {
        return emissivePoint.add(0, - BORN_MAX_HALF_Y_DIFF + NanoMath.randInt( BORN_MAX_Y_DIFF ));
    }

	public int RandLifeTime() {
        return ( NanoMath.randInt( LIFE_DIFF ) + LIFE_MIN );
	}


	public final void emit() {
	     if(activeParticles < (MAX_PARTICLES-1)) {
             particles[ activeParticles++ ].born(RandPosition(), RandSpeed(), RandLifeTime() );
		 }
	}

    public void draw(Graphics g) {

		if ( visible ) {
//			if ( clipTest ) {
				pushClip( g );
				translate.addEquals( position );

				final Rectangle clip = clipStack[ currentStackSize ];
				clip.setIntersection( translate.x, translate.y, size.x, size.y );

				if ( clip.width > 0 && clip.height > 0 ) {
					g.setClip( clip.x, clip.y, clip.width, clip.height );

					paint( g );
					for ( short i = 0; i < activeParticles; ++i ) {
						particles[ i ].draw( g );
					}
				}

				translate.subEquals( position );
				popClip( g );
//			} else {
//				// desenha direto, ignorando o teste de interseção
//				translate.addEquals( position );
//				paint( g );
//				translate.subEquals( position );
//			}
		}
    }

	/**
	 * Classe interna que descreve uma part�cula de fogos de artif�cio.
	 */
	private static final class Particle {

		//<editor-fold desc="Variables">
			/** Tempo acumulado da part�cula. */
			private int fp_accTime;

			private int fp_speedX;
			private int fp_speedY;

			private final Point position = new Point();
			private final Point initialPos = new Point();

			private final ParticleEmitter parent;

			private static final int CYCLES_PER_SECOND = 3;
			private static final int MILISECONS_PER_CYCLES = 1000 / CYCLES_PER_SECOND;
			private static final int FP_HALF_GRAVITY_ACC = PARTICLES_GRAVITY_SPEED >> 1;

			private boolean visible;

			private int halfRadius;
			private byte seed;

			private int frameMod;
			private int lifeTime;
		//</editor-fold>


		//<editor-fold desc="Constructor and Initializers">
			private Particle(ParticleEmitter parent) {
				this.parent = parent;
				visible = false;
			}

			private final void set(Particle p) {
				fp_accTime = p.fp_accTime;
				frameMod = p.frameMod;

				fp_speedY = p.fp_speedY;
				visible = p.visible;
				seed = p.seed;
				lifeTime = p.lifeTime;

				halfRadius = p.halfRadius;//(byte)(NanoMath.toInt(fp_speedY) >> 1);

				initialPos.set( p.initialPos );

				updatePosition();
			}

			private final void born( Point bornPosition, Point bornSpeed, int LifeTime ) {
				fp_accTime = 0;
				frameMod = 0;
				lifeTime = LifeTime;

				fp_speedX = bornSpeed.x;
				fp_speedY = bornSpeed.y;
				visible = true;
				seed = (byte)NanoMath.randInt(360);

				halfRadius = (byte)(NanoMath.toInt(fp_speedY) >> 1);

				//#if JAR == "min"
	//# 		initialPos.set( bornPosition.sub( 2, 1 ) );
				//#else
				initialPos.set( bornPosition.sub( parent.spriteParticle.getSize().div( 2 ) ) );
				//#endif
				updatePosition();
			}
		//</editor-fold>


		//<editor-fold desc="Update and Draw">
			public final void updatePosition() {
				position.set( initialPos.x + NanoMath.toInt( NanoMath.mulFixed( fp_speedX, fp_accTime ) ),
						  initialPos.y + NanoMath.toInt( NanoMath.mulFixed( fp_speedY, fp_accTime ) + NanoMath.mulFixed( FP_HALF_GRAVITY_ACC, NanoMath.mulFixed( fp_accTime, fp_accTime ) ) ) );
			}

			public final void update( int fp_delta ) {
				fp_accTime += fp_delta;

				updatePosition();

				//<editor-fold desc="Update Visibility">
					if ( position.x <= 0 || position.x >= parent.getWidth() || position.y >= parent.getHeight() ) {
						visible = false;
					}
					if(fp_accTime > lifeTime) visible = false;
					else frameMod = NanoMath.min(NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed(fp_accTime,fp_accTime) , NanoMath.mulFixed(lifeTime,lifeTime) ) * parent.NUMBER_OF_PARTICLES ) , (parent.NUMBER_OF_PARTICLES - 1));
				//</editor-fold>
			}

			public final void draw( Graphics g ) {
				if ( visible ) {
					//#if JAR == "min"
	//# 					g.setColor( COLOR_DROP );
	//# 					g.fillRect( translate.x + position.x, translate.y + position.y, 3, 2 );
					//#else
						parent.spriteParticle.setFrame(frameMod);
						parent.spriteParticle.setPosition( position );
						parent.spriteParticle.draw( g );
					//#endif
				} // fim if ( visible )
			}
		//</editor-fold>
	}
}