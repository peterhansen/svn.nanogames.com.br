/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 * Classe do personagem principal do jogador.
 * @author Ygor
 */
public class Character extends Collidable {
	
	protected final static int CHAR_STATE_PLAYING = 0;
	protected final static int CHAR_STATE_FALLING = 1;

	protected final static int FP_MIN_COLLISION_TIME = -800000000;
	
	protected int state;

	/** Responsible controller for character movenent.
	 * If the player is moving freely, the controller can be null.
	 */
	protected Controller controller;

	/** Gravity constant, in meters per second */
	protected final int FP_GRAVITY = NanoMath.divInt( -98, 10 );		// 9.8 meters/second^2

	/** Acummulated time, in fixed point seconds. */
	protected int fpAccTime = 0;

	/** World initial position of current movement.
	 * @see setSpeed
	 */
	protected Point fpP0 = new Point();

	/** World initial speed of current movement.
	 * @see setSpeed
	 */
	protected Point fpV0 = new Point();	

	public Character( Map map/*String prefixPath, PaletteMap[] palette*/ ) throws Exception {
		super( map );
		
		state = CHAR_STATE_PLAYING;

		// TODO: put standard height and width somewhere else in a constant
		setWorldSize( NanoMath.toFixed( 8 ) / 10, NanoMath.toFixed( 17 ) / 10 );

		// TODO: call something like map.getInitialPosition() in the near future
		setWorldPosition( NanoMath.toFixed( 1 ), NanoMath.toFixed( 6 ) );
		fpP0 = getWorldPosition();
	}
	
	public void push() {
		state = CHAR_STATE_FALLING;
	}
	
	public void update( int delta ) {
		if( controller != null ) {
			// controlled movement
			controller.update( delta );
			
		} else {

			// calculating fixed delta and accumulating it
			final int FP_DELTA = NanoMath.divInt( delta, Constants.SECOND );
			fpAccTime += FP_DELTA;

			// calculating new collision box for the current move
			Rectangle newCollisionBox = calculateNewPosition();

			if( state == CHAR_STATE_PLAYING ) {
				// testing the platforms for collisions

				// TODO: maybe we can iterate over only the visible ones???
				for( int i = 0; i < map.platforms.length; i++ ) {

					//final Rectangle platformColisionBox = map.platforms[ i ].getCollisionBox();
					if( map.platforms[ i ].intersects( newCollisionBox ) ) {
						// we have a collision!
						final Rectangle platformCollBox = map.platforms[ i ].getCollisionBox();

						// let's get over which axis it happens last
						int FP_TIME_FOR_X_COLLISION = FP_MIN_COLLISION_TIME;
						int FP_TIME_FOR_Y_COLLISION = FP_MIN_COLLISION_TIME;

						Point currentSpeed = new Point( fpV0.x, fpV0.y + NanoMath.mulFixed( FP_GRAVITY, fpAccTime ) );

						// looking for the time of the X axis collision:
						if( currentSpeed.x > 0 ) {
							FP_TIME_FOR_X_COLLISION = maxRoot( 0, fpV0.x, platformCollBox.x - ( fpP0.x + getWorldSize().x ) );
						} else if( currentSpeed.x < 0 ) {
							FP_TIME_FOR_X_COLLISION = maxRoot( 0, fpV0.x, platformCollBox.x + platformCollBox.width - fpP0.x );
						}

						// looking for the time of the Y axis collision:

						// TODO: for now, we haven't worried about collisions when you're
						// going up!
						/*if( currentSpeed.y > 0 ) {
							FP_TIME_FOR_Y_COLLISION = maxRoot(
									NanoMath.divFixed( FP_GRAVITY, NanoMath.ONE << 1 ),
									currentSpeed.y, //fpV0.y,
									platformCollBox.y - ( worldPosition.y + worldSize.y ) );
						} else */
						if( currentSpeed.y < 0 ) {
							FP_TIME_FOR_Y_COLLISION = maxRoot( NanoMath.divFixed( FP_GRAVITY, NanoMath.ONE << 1 ), fpV0.y,
																fpP0.y - ( platformCollBox.y + platformCollBox.height ) );
						}

						// the collision time is the biggest of the two:
						final int FP_COLLISION_TIME = NanoMath.max( FP_TIME_FOR_X_COLLISION, FP_TIME_FOR_Y_COLLISION );
						fpAccTime = FP_COLLISION_TIME - Constants.FP_COLLISION_SAFE_TIME;

						newCollisionBox = calculateNewPosition();
						setSpeed( 0, 0 );

						// Supõe que plataforma é um Rectangle
						if( platformCollBox.y + platformCollBox.height < newCollisionBox.y ) {
							
							Controller platformController = map.platforms[i].getController();
							if( platformController != null && platformController.intersects( newCollisionBox ) ) {//newCollisionBox.intersects( platformController.getCollisionBox() ) ) {
								
								platformController.setCharacter( this );
								setController( platformController );

								// since we are changing the character position, we also need to update his initial position in the controller
								controller.setInitialPos( newCollisionBox.x, newCollisionBox.y );
							}
						}

						break;
					}
				}
			}

			// updating character world position
			setWorldPosition( newCollisionBox.x, newCollisionBox.y );

		}

		// updating camera position so it follows the character
		map.getCamera().lookAhead( getPosition() );
	}

	/**
	 * Resets the character speed. This speed can be used both by
	 * the character when in free motion and by the controllers.
	 */
	public void setSpeed( int fpSpeedX, int fpSpeedY ) {
		fpP0.set( worldPosition );
		fpV0.set( fpSpeedX, fpSpeedY );
		fpAccTime = 0;
	}
	
	public void paint( Graphics g ) {
		super.paint( g );
		if( controller != null ) {
			controller.draw( g, debugCollisionPattern.getPosition().add( debugCollisionPattern.getSize() ) );
		}
	}

	/**
	 * Assigns the controller to the character, so that the character
	 * can delegate its movement do the controller.
	 * @param controller Can be null, if the character wants to be set to free movement.
	 */
	public void setController( Controller controller ) {
		this.controller = controller;
	}

	public final Controller getController() {
		return controller;
	}

	/**
	 * Auxiliary functions that calculates the new character position
	 * based on its current movement properties.
	 * @return a world coordinate rectangle with new position and size
	 */
	protected final Rectangle calculateNewPosition( ) {

		// (t^2)/2
		int FP_T2_OVER_TWO = NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpAccTime ), NanoMath.ONE << 1 );

		// calculating movement
		final Point FP_MOVEMENT = new Point(
			NanoMath.mulFixed( fpV0.x, fpAccTime ),
			NanoMath.mulFixed( fpV0.y, fpAccTime ) + NanoMath.mulFixed( FP_GRAVITY, FP_T2_OVER_TWO )
		);

		// calculating new position:
		// s = s0 + v0*t + (a*t^2)/2
		Point newPosition = new Point(
				fpP0.x + FP_MOVEMENT.x,
				fpP0.y + FP_MOVEMENT.y
				);

		Rectangle newCollisionBox = getCollisionBox();
		newCollisionBox.x = newPosition.x;
		newCollisionBox.y = newPosition.y;
		return newCollisionBox;
	}

	/** Auxiliary function for calculating the maximum root
	 * of the quadratic equation */
	protected int maxRoot( int FP_A, int FP_B, int FP_C ) {

		if( FP_A == 0 ) {
			return NanoMath.divFixed( FP_C, FP_B );
		} else {
			int FP_DET = NanoMath.mulFixed( FP_B, FP_B) - NanoMath.mulFixed( NanoMath.ONE * 4 , NanoMath.mulFixed( FP_A, FP_C) );
			int FP_SQRT_DET = NanoMath.sqrtFixed( FP_DET );
			int FP_DEN = NanoMath.mulFixed( NanoMath.ONE << 1, FP_A );
			int FP_R1 = NanoMath.divFixed( -FP_B + FP_SQRT_DET, FP_DEN );
			int FP_R2 = NanoMath.divFixed( -FP_B - FP_SQRT_DET, FP_DEN );
			return NanoMath.max( FP_R1, FP_R2 );
		}
	}
}
