package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;

/**
 * Represents a platform which the player character can walk over and/or collide.
 * @author Ygor
 */
public abstract class Platform extends Collidable implements Constants {
				
	public static final int PLATFORM_INITIAL		= 0;
	public static final int PLATFORM_FINAL			= 1;
	public static final int PLATFORM_INITIAL_LOW	= 2;
	public static final int PLATFORM_FINAL_LOW		= 3;
	public static final int PLATFORM_RED_BALL		= 4;
	public static final int PLATFORM_PIN			= 5;
	public static final int PLATFORM_STONE			= 6;
	public static final int PLATFORM_WATER_JET		= 7;
	public static final int PLATFORM_OLD_BOARD		= 8;
	protected int type;

	protected static final int FP_LEVEL_RED_BALLS_MIN_HEIGHT = 163840;		// 2.5 meters
	protected static final int FP_LEVEL_RED_BALLS_MAX_HEIGHT = 262144;		// 4 meters

	/** This method should be implemented with the initialization of
	 * each individual platform type.
	 */
	protected abstract void init();

	/** Platform controller. The controller world position
	 * is relative to the platform world position.
	 */
	protected Controller controller;

	private Platform( Map map, int slots ) {
		super( slots, map );

		init();

		//#if DEBUG
			// sets the size of the debug collision pattern to the collision box size
			// and draw it above all other drawables.
			debugCollisionPattern.setFillColor( 0xff00ff );
			if( collisionBox != null ) {
				debugCollisionPattern.setSize( map.getScreenSize( collisionBox.width, collisionBox.height ) );
			} else {
				debugCollisionPattern.setSize( map.getScreenSize( worldSize.x, worldSize.y ) );
			}
			setDrawableIndex( 0, drawables.length - 1 );
		//#endif

//		int platformHeight;
//		switch( type ) {
//			case PLATFORM_INITIAL:
//
//				// setting world size and collision box size
//				worldSize = new Point( NanoMath.toFixed( 7 ), NanoMath.toFixed( 5 ) );
//				collisionBox = new Rectangle( NanoMath.ONE, NanoMath.ONE, NanoMath.toFixed( 5 ), NanoMath.toFixed( 3 ) );
//
//				// creating initial platform controller
//				controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_AUTO );
//				controller.setRelativeWorldPos( 0, worldSize.y );
//				controller.setWorldSize( worldSize.x, NanoMath.ONE );
//				break;
//
//			case PLATFORM_FINAL:
//				worldSize = new Point( NanoMath.toFixed( 5 ), NanoMath.toFixed( 3 ) );
//				break;
//
//			case PLATFORM_RED_BALL:
//
//				// setting world size and collision box size
//				platformHeight = FP_LEVEL_RED_BALLS_MIN_HEIGHT + NanoMath.randFixed( FP_LEVEL_RED_BALLS_MAX_HEIGHT - FP_LEVEL_RED_BALLS_MIN_HEIGHT );
//				worldSize = new Point( NanoMath.divInt( 33, 10 ), platformHeight + NanoMath.toFixed( 1 ) );
//				collisionBox = new Rectangle( NanoMath.divInt( 75, 100 ), 0, NanoMath.divInt( 15, 10 ), platformHeight );
//
//				// creating red ball platform controller
//				controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_AUTO );
//				controller.setRelativeWorldPos( 0, worldSize.y );
//				controller.setWorldSize( worldSize.x, NanoMath.ONE );
//
//				// setting drawables positions and size
//				try {
//					DrawableImage redBall = new DrawableImage( Constants.PATH_IMAGES + "red_ball.png" );
//					insertDrawable( redBall );
//					redBall.setPosition( 0, 0 );
//
//					DrawableImage redBallStemFill = new DrawableImage( Constants.PATH_IMAGES + "red_ball_stem.png" );
//					Pattern redBallStem = new Pattern( redBallStemFill );
//					insertDrawable( redBallStem );
//					redBallStem.setPosition( 22, redBall.getHeight() );
//					redBallStem.setSize( redBallStem.getWidth(), 600 );
//
//				} catch ( Exception e ) {
//					//#if DEBUG
//						GameMIDlet.log( "Não foi possível carregar as imagens para o obstáculo das bolas vermelhas. ");
//						e.printStackTrace();
//					//#endif
//				}
//				break;
//
//			default:
//			case PLATFORM_OLD_BOARD:
//			case PLATFORM_WATER_JET:
//				// TODO: implement this!
//				platformHeight = FP_LEVEL_RED_BALLS_MIN_HEIGHT + NanoMath.randFixed( FP_LEVEL_RED_BALLS_MAX_HEIGHT - FP_LEVEL_RED_BALLS_MIN_HEIGHT );
//				worldSize = new Point( NanoMath.toFixed( 1 ), platformHeight );
//				collisionBox = new Rectangle( 0, 0, worldSize.x, platformHeight );
//
//				controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_AUTO );
//				controller.setRelativeWorldPos( 0, worldSize.y );
//				controller.setWorldSize( worldSize.x, NanoMath.ONE );
//				break;
//		}
	}

	public final Controller getController() {
		return controller;
	}

	public void update( int delta ) {
	}

	public void setWorldPosition( int fixedX, int fixedY ) {
		super.setWorldPosition( fixedX, fixedY );

		//#if DEBUG
			if( collisionBox != null ) {
				debugCollisionPattern.setPosition( 
						NanoMath.toInt( NanoMath.mulFixed( collisionBox.x, Constants.FP_METER_TO_PIXEL_RATIO ) ),
						NanoMath.toInt( NanoMath.mulFixed( worldSize.y - ( collisionBox.height + collisionBox.y ), FP_METER_TO_PIXEL_RATIO ) )
					);
						//getInternalPosition( new Point( collisionBox.x, collisionBox.y ), new Point( collisionBox.width, collisionBox.height ) ) );
			}
		//#endif

		// setting controller world position
		if( controller != null ) {
			Point relativePos = controller.getRelativeWorldPos();
			controller.setWorldPosition( worldPosition.x + relativePos.x, worldPosition.y + relativePos.y );
		}
	}

	public void refreshScreenSize() {
		super.refreshScreenSize();

		if( controller != null ) {
			controller.refreshScreenSize();
		}
	}
	
	public static Platform create( Map map, int type, final int id ) {
		switch( type ) {
			case PLATFORM_INITIAL:
			case PLATFORM_FINAL:
				return new Platform( map, 6 ) {

					protected final void init() {

						try {
							// loading images
							DrawableImage firstTile = new DrawableImage( Constants.PATH_IMAGES + "plataforma_1.png" );
							DrawableImage tilesFill = new DrawableImage( Constants.PATH_IMAGES + "plataforma_2.png" );
							DrawableImage lastTile = new DrawableImage( Constants.PATH_IMAGES + "plataforma_3.png" );

							int fpFirstTileWidth = NanoMath.divFixed( NanoMath.toFixed( firstTile.getWidth() ), Constants.FP_METER_TO_PIXEL_RATIO );
							int fpTilesWidth = NanoMath.divFixed( NanoMath.toFixed( tilesFill.getWidth() * 2 ), Constants.FP_METER_TO_PIXEL_RATIO );
							int fplLastTileWidth = NanoMath.divFixed( NanoMath.toFixed( lastTile.getWidth() ), Constants.FP_METER_TO_PIXEL_RATIO );
							
							insertDrawable( firstTile );
							firstTile.setPosition( 0, 0 );

							Pattern tiles = new Pattern( tilesFill );
							insertDrawable( tiles );
							tiles.setPosition( firstTile.getWidth(), 0 );
							tiles.setSize( map.getScreenSize( fpTilesWidth, 0 ).x, tilesFill.getHeight() );

							insertDrawable( lastTile );
							lastTile.setPosition( tiles.getPosX() + tiles.getWidth(), 0 );

							// setting world size and collision box size
							worldSize = new Point( fpFirstTileWidth + fpTilesWidth + fplLastTileWidth, NanoMath.divInt( 7, 2 ) );
							collisionBox = new Rectangle( NanoMath.ONE, 0, worldSize.x - ( NanoMath.ONE << 1 ), NanoMath.toFixed( 3 ) );

							// creating initial platform controller
							controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_AUTO );
							controller.setRelativeWorldPos( collisionBox.x, collisionBox.height );
							controller.setWorldSize( collisionBox.width, NanoMath.ONE );


						} catch ( Exception e ) {
							//#if DEBUG
								GameMIDlet.log( "Não foi possível carregar as imagens para o obstáculo das bolas vermelhas. ");
								e.printStackTrace();
							//#endif
						}
					}
				};

			case PLATFORM_INITIAL_LOW:
			case PLATFORM_FINAL_LOW:
				return new Platform( map, 3 ) {

					protected final void init() {

						// setting world size and collision box size
						worldSize = new Point( NanoMath.toFixed( 7 ), NanoMath.toFixed( 4 ) );
						collisionBox = new Rectangle( NanoMath.ONE, 0, NanoMath.toFixed( 5 ), NanoMath.toFixed( 1 ) );

						// creating initial platform controller
						controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_AUTO );
						controller.setRelativeWorldPos( collisionBox.x, collisionBox.height );
						controller.setWorldSize( collisionBox.width, NanoMath.ONE );
					}
				};
				
			case PLATFORM_RED_BALL: 
				return new Platform( map, 3 ) {
					protected final void init() {

						// setting world size and collision box size
						int platformHeight = FP_LEVEL_RED_BALLS_MIN_HEIGHT + NanoMath.randFixed( FP_LEVEL_RED_BALLS_MAX_HEIGHT - FP_LEVEL_RED_BALLS_MIN_HEIGHT );
						worldSize = new Point( NanoMath.divInt( 33, 10 ), platformHeight + NanoMath.toFixed( 1 ) );
						collisionBox = new Rectangle( NanoMath.divInt( 75, 100 ), 0, NanoMath.divInt( 15, 10 ), platformHeight );

						// creating red ball platform controller
						controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_JUMP );
						controller.setRelativeWorldPos( collisionBox.x, collisionBox.height );
						controller.setWorldSize( collisionBox.width, NanoMath.ONE );

						// setting drawables positions and size
						try {
							DrawableImage redBall = new DrawableImage( Constants.PATH_IMAGES + "red_ball.png" );
							insertDrawable( redBall );
							redBall.setPosition( 0, 0 );

							DrawableImage redBallStemFill = new DrawableImage( Constants.PATH_IMAGES + "red_ball_stem.png" );
							Pattern redBallStem = new Pattern( redBallStemFill );
							insertDrawable( redBallStem );
							redBallStem.setPosition( 22, redBall.getHeight() );
							redBallStem.setSize( redBallStem.getWidth(), 600 );

						} catch ( Exception e ) {
							//#if DEBUG
								GameMIDlet.log( "Não foi possível carregar as imagens para o obstáculo das bolas vermelhas. ");
								e.printStackTrace();
							//#endif
						}
					}
				};

			case PLATFORM_STONE:
				return new Platform( map, 1 ) {

					// TODO: está usando coisas do RED BALLS
					protected final void init() {

						// setting world size and collision box size
						worldSize = new Point( NanoMath.toFixed( 2 ), NanoMath.ONE );
						collisionBox = new Rectangle( NanoMath.divInt( 1, 3 ), 0, NanoMath.divInt( 4, 3 ), NanoMath.HALF );

						// creating red ball platform controller
						controller = Controller.create( map, this, Controller.CONTROLLER_TYPE_SINKING );
						controller.setRelativeWorldPos( collisionBox.x, collisionBox.height );
						controller.setWorldSize( collisionBox.width, NanoMath.ONE );
					}
				};
				
			case PLATFORM_PIN: 
				return new Platform( map, 1 ) {

					protected final void init() {
					}

//					protected final void paint( Graphics g ) {
//						//g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//						super.paint( g );
//
////						///Teste ball draw
////						g.setColor( 0x888888 );
////						g.setClip( translate.x + ( debugCollisionPattern.getWidth() - BALL_SIZE >> 1 ), translate.y - ( BALL_SIZE >> 2 ), BALL_SIZE, BALL_SIZE );
////						g.fillArc( translate.x + ( debugCollisionPattern.getWidth() - BALL_SIZE >> 1 ), translate.y - ( BALL_SIZE >> 2 ), BALL_SIZE, BALL_SIZE, 0, 360 );
////						g.setColor( 0xff0000 );
//					}
					
//					public boolean intersects( Rectangle rect ) {
//						return super.intersects( rect ) || false;
//					}
				};
				
//			case PLATFORM_WATER_JET:
//				return new Platform( map, type ) {
//					private static final int TIME_INTERVAL = 5000;
//					private static final int PASS_INTERVAL = 2000;
//					private final static int MAX_JET_SIZE = 80;
//
//					protected int accTime = ( -id * ( PASS_INTERVAL >> 1 ) ) % TIME_INTERVAL;
//
//
//					protected final void paint( Graphics g ) {
//						//g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//						super.paint( g );
//
//						// Teste ball draw
//
//						if( accTime < PASS_INTERVAL ) {
//							g.setColor( 0x0000aa );
//							int JET_SIZE = NanoMath.toInt( MAX_JET_SIZE * NanoMath.clamp( NanoMath.divInt( ( PASS_INTERVAL >> 1 ) - NanoMath.abs( accTime - ( PASS_INTERVAL >> 1 ) ), ( PASS_INTERVAL >> 2 ) ), 0, NanoMath.ONE ) );
//							//g.setClip( translate.x, translate.y - JET_SIZE, debugCollisionPattern.getWidth(), JET_SIZE );
//							g.fillTriangle( translate.x, translate.y - JET_SIZE, translate.x + debugCollisionPattern.getWidth(), translate.y - JET_SIZE, translate.x + ( debugCollisionPattern.getWidth() >> 1 ), translate.y  );
//						}
//					}
//
//					public void update( int delta ) {
//						accTime += delta;
//						if( accTime >= TIME_INTERVAL ) accTime -= TIME_INTERVAL;
//					}
//
////					public byte intersects( Character character, Rectangle rect ) {
////						int JET_SIZE = NanoMath.toInt( MAX_JET_SIZE * NanoMath.clamp( NanoMath.divInt( ( PASS_INTERVAL >> 1 ) - NanoMath.abs( accTime - ( PASS_INTERVAL >> 1 ) ), ( PASS_INTERVAL >> 2 ) ), 0, NanoMath.ONE ) );
////						return ( accTime > PASS_INTERVAL ) ? super.intersects( character, rect ) : ( ( rect.intersects( , id, id, id ) ) ? ( COLLISION_TYPE_PUSH ) : COLLISION_TYPE_NONE );
////					}
//				};
//
//			case PLATFORM_OLD_BOARD:
//				return new Platform( map, type ) {
//					private static final int TIME_INTERVAL = 5000;
//					private static final int PASS_INTERVAL = 2000;
//
//					protected int accTime = ( -id * ( PASS_INTERVAL >> 1 ) ) % TIME_INTERVAL;
//
//
//					protected final void paint( Graphics g ) {
//						//g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//						super.paint( g );
//
//						// Teste ball draw
//
//						if( accTime < PASS_INTERVAL ) {
//							g.setColor( 0x888800 );
//							final int MAX_JET_SIZE = 30;
//							int JET_SIZE = NanoMath.toInt( MAX_JET_SIZE * NanoMath.clamp( NanoMath.divInt( ( PASS_INTERVAL >> 1 ) - NanoMath.abs( accTime - ( PASS_INTERVAL >> 1 ) ), ( PASS_INTERVAL >> 2 ) ), 0, NanoMath.ONE ) );
//							g.fillRect( translate.x, translate.y, debugCollisionPattern.getWidth(), JET_SIZE );
//						}
//					}
//
//					public void update( int delta ) {
//						accTime += delta;
//						if( accTime >= TIME_INTERVAL ) accTime -= TIME_INTERVAL;
//					}
//
////					public boolean intersects( Rectangle rect ) {
////						return super.intersects( rect ) && ( accTime < PASS_INTERVAL );
////					}
//				};
				
			default: return null;
		}
	 }
}
