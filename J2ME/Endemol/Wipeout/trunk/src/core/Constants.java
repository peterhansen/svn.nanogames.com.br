/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */
package core;

/**
 *
 * @author Caio, Peter, Ygor
 */
public interface Constants {

	/** Nome curto do jogo, para submissão ao Nano Online. */
	public static final String APP_SHORT_NAME = "GRNH";
	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";
	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";
	public static final byte RANKING_INDEX_ACC_SCORE = 0;
	public static final byte RANKING_INDEX_MAX_SCORE = 1;
	public static final byte RANKING_INDEX_MAX_LEVEL = 2;

	// <editor-fold defaultstate="collapsed" desc="PATHS DOS ARQUIVOS">
	/** Caminho dos textos do jogo. */
	public static final String PATH_TEXT = "/";
	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	/** Caminho das imagens utilizadas nas telas de jogo. */
	public static final String PATH_GAME_IMAGES = PATH_IMAGES + "game/";
	/** Caminho das imagens utilizadas pela tela de seleção de personagens. */
	public static final String PATH_CHARSELECTION_IMAGES = PATH_IMAGES + "charselection/";
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";
	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_GIRLS = PATH_IMAGES + "girls/";
	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";
	/** Caminho das imagens usadas no Nano Online. */
	public static final String PATH_ONLINE = PATH_IMAGES + "online/";
	/** Caminho das imagens usadas nos minigames. */
	public static final String PATH_MINIGAMES_IMAGES = PATH_IMAGES + "minigames/";
	
	public static final String PATH_END = PATH_IMAGES + "end/";

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="DATABASE INFO">	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";
	/**Índice do slot de gravação das opções na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	public static final byte DATABASE_SLOT_GAME_DATA = 2;
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 2;
	/** Id da categoria de notícias do Topa ou Não Topa, no Nano Online. */
	public static final byte NEWS_CATEGORY_ID = 6;
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Game Constants">

		//<editor-fold desc="Cheats">
			public static final boolean AUTOMATIC = false;
			public static final boolean ALWAYS_WIN = false;
		//</editor-fold>

		public static final int BAR_MOVE_LOOP = 1000;

		// TODO: a constante abaixo era do GH.
		// Ver se ainda tem coisa sobrando do GH associada a ela.
		public static final int PARTICIPANTS_NUMBER = 8;


		//#if VERSION == "GH"
//# 			public static final int COLOR_CHAR_SELECTION_TOP = 0x000000;
//# 			public static final int COLOR_MAIN_MENU_BKG = 0x000000;
//# 
//# 			public static final int COLOR_BEAUTY = 0xd900ee;
//# 			public static final int COLOR_CHARISMA = 0xff9000;
//# 			public static final int COLOR_RESISTANCE = 0xee000d;
//# 			public static final int COLOR_KNOWLEDGE = 0x00b9d4;
//# 			public static final int COLOR_CHANGING = 0x9b9b9b;
//# 
//# 			public static final int COLOR_BACKGROUND = 0xFF7200;
//# 			public static final int COLOR_OPTIONS_BACKGROUND = 0xff8300;
//# 			
//# 			public static final int COLOR_CLOCK = 0x661a08;
//# 		
//# 			public static final int COLOR_TOP_BAR_TOP = 0x232729;
//# 			
//# 			public static final int COLOR_KEYPAD_BACKGROUD = 0x000000;
//# 			public static final int COLOR_KEYPAD_BACKGROUN_TRANSITION_DECAL = 0x491d00;
//# 			public static final int COLOR_KEYPAD_BACKGROUN_DECAL = 0xeb6300;
		//#else
			public static final int COLOR_CHAR_SELECTION_TOP = 0xb8d0e6;
			public static final int COLOR_MAIN_MENU_BKG = 0x064172;
			
			public static final int COLOR_BEAUTY = 0xfc6dfa;
			public static final int COLOR_CHARISMA = 0x9cc814;
			public static final int COLOR_RESISTANCE = 0xb6182e;
			public static final int COLOR_KNOWLEDGE = 0xfced4a;
			public static final int COLOR_CHANGING = 0x9b9b9b;

			public static final int COLOR_BACKGROUND = COLOR_MAIN_MENU_BKG;
			public static final int COLOR_OPTIONS_BACKGROUND = 0x154c7a;
			
			public static final int COLOR_CLOCK = 0x254f79;
			
			public static final int COLOR_TOP_BAR_TOP = 0xbdd2e6;
			
			public static final int COLOR_KEYPAD_BACKGROUD = 0x000000;
			public static final int COLOR_KEYPAD_BACKGROUN_TRANSITION_DECAL = 0x06131c;
			public static final int COLOR_KEYPAD_BACKGROUN_DECAL = 0x0a263e;
		//#endif

		public static final int COLOR_END_LIGHT = 0xfff000;
		public static final int COLOR_END_LIGHT_2 = 0xbbb000;
		public static final int COLOR_END_BACKGROUND = 0x01162e;
		public static final int COLOR_END_FLOOR = 0x483c34;
		
		public static final byte OPTIONS_STATE_NONE = 0;
		public static final byte OPTIONS_STATE_ON_OF = 1;
		public static final byte OPTIONS_STATE_0_100 = 2;
	// </editor-fold> // end of Game Constants region

	// <editor-fold defaultstate="collapsed" desc="Hardware Configuration">
	/** Duração da vibração ao atingir a trave, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 300;
	//#if SCREEN_SIZE == "SMALL"
	//# 		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
	//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#elif SCREEN_SIZE == "BIG"
		/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
		public static final int LOW_MEMORY_LIMIT = 1600000;
	//#else
//# 	/** Limite mínimo de memória para que comece a haver cortes nos recursos. */
//# 	public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Índices">

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS RANKINGS">
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES E DADOS DAS FONTES DE TEXTO">

		/** Offset da fonte padrão. */
		public static final byte DEFAULT_FONT_OFFSET = 1;

		/**Índice da fonte utilizada para textos. */
		public static final byte FONT_MENU = 0;
		public static final byte FONT_TEXT_BLACK = FONT_MENU + 1;
		public static final byte FONT_MENU_BIG = FONT_TEXT_BLACK + 1;
		public static final byte FONT_MENU_NO_FILL = FONT_MENU_BIG + 1;
		public static final byte FONT_MENU_BIG_NO_FILL = FONT_MENU_NO_FILL + 1;
		//#if VERSION == "GH"
//# 			public static final byte FONT_ACROMYN_SELECTED = FONT_MENU_BIG_NO_FILL + 1;
//# 			public static final byte FONT_ACROMYN = FONT_ACROMYN_SELECTED + 1;
//# 			public static final byte FONT_WEEK = FONT_ACROMYN + 1;
//# 				public static final byte FONT_TEXT_WHITE = FONT_WEEK + 1;
//# 
//# 			/** Total de tipos de fonte do jogo. */
//# 			public static final byte FONT_TYPES_TOTAL = FONT_TEXT_WHITE + 1;
		//#else
			public static final byte FONT_WEEK = FONT_MENU_BIG_NO_FILL + 1;
			public static final byte FONT_TEXT_WHITE = FONT_TEXT_BLACK;

			/** Total de tipos de fonte do jogo. */
			public static final byte FONT_TYPES_TOTAL = FONT_WEEK + 1;
		//#endif

		/**Índice da fonte padrão do jogo. */
		public static final byte FONT_INDEX_DEFAULT = FONT_TEXT_BLACK;
		
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS SONS">
	//índices dos sons do jogo
	public static final byte SOUND_KEY_COLLECTED = 0;
	public static final byte SOUND_LOCK_OPEN = SOUND_KEY_COLLECTED + 1;
	public static final byte SOUND_SEXY_SPECIAL = SOUND_LOCK_OPEN + 1;
	public static final byte SOUND_COLD_SPICY = SOUND_SEXY_SPECIAL + 1;
	public static final byte SOUND_HOT_SPICY = SOUND_COLD_SPICY + 1;
	public static final byte SOUND_RADIATED_SPICY = SOUND_HOT_SPICY + 1;
	public static final byte SOUND_MULTIPLE_LOCKS = SOUND_RADIATED_SPICY + 1;
	public static final byte SOUND_FIRST_MUSIC = SOUND_MULTIPLE_LOCKS + 1;
	public static final byte SOUND_GAME_OVER_MUSIC = SOUND_FIRST_MUSIC + 1;
	public static final byte SOUND_LAST_MUSIC = SOUND_GAME_OVER_MUSIC + 1;
	//total de sons
	public static final byte SOUND_TOTAL = SOUND_LAST_MUSIC + 1;
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS LISTENERS">
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="INDICES DOS TEXTOS">
		public static final short TEXT_OK = 0;
		public static final short TEXT_BACK = TEXT_OK + 1;
		public static final short TEXT_NAME = TEXT_BACK + 1;
		public static final short TEXT_HOBBY = TEXT_NAME + 1;
		public static final short TEXT_FOOD = TEXT_HOBBY + 1;
		public static final short TEXT_MUSIC = TEXT_FOOD + 1;
		public static final short TEXT_CHARISMA = TEXT_MUSIC + 1;
		public static final short TEXT_BEAUTY = TEXT_CHARISMA + 1;
		public static final short TEXT_RESISTANCE = TEXT_BEAUTY + 1;
		public static final short TEXT_KNOWLEDGE = TEXT_RESISTANCE + 1;

		//<editor-fold desc="Main Menu Entries and Descriptions">
			public static final short TEXT_NEW_GAME = TEXT_KNOWLEDGE + 1;
			public static final short TEXT_CHALLENGES = TEXT_NEW_GAME + 1;
			public static final short TEXT_OPTIONS = TEXT_CHALLENGES + 1;
			public static final short TEXT_NANO_ONLINE = TEXT_OPTIONS + 1;
			public static final short TEXT_HELP = TEXT_NANO_ONLINE + 1;
			public static final short TEXT_CREDITS = TEXT_HELP + 1;
			public static final short TEXT_EXIT = TEXT_CREDITS + 1;

			public static final short TEXT_NEW_GAME_DESC = TEXT_EXIT + 1;
			public static final short TEXT_OPTIONS_DESC = TEXT_NEW_GAME_DESC + 1;
			public static final short TEXT_NANO_ONLINE_DESC = TEXT_OPTIONS_DESC + 1;
			public static final short TEXT_HELP_DESC = TEXT_NANO_ONLINE_DESC + 1;
			public static final short TEXT_CREDITS_DESC = TEXT_HELP_DESC + 1;
			public static final short TEXT_EXIT_DESC = TEXT_CREDITS_DESC + 1;
		//</editor-fold>

		public static final short TEXT_PAUSE = TEXT_EXIT_DESC + 1;
		public static final short TEXT_CREDITS_TEXT = TEXT_PAUSE + 1;

		//<editor-fold desc="Help Contents and Menu Entries">
			public static final short TEXT_HELP_HOW_TO_PLAY = TEXT_CREDITS_TEXT + 1;
			public static final short TEXT_HELP_ATTRIBUTES = TEXT_HELP_HOW_TO_PLAY + 1;
			public static final short TEXT_HELP_ACTIVITIES = TEXT_HELP_ATTRIBUTES + 1;
			public static final short TEXT_HELP_MINIGAMES = TEXT_HELP_ACTIVITIES + 1;
			public static final short TEXT_HELP_HAPPENINGS = TEXT_HELP_MINIGAMES + 1;
			public static final short TEXT_HELP_CARDS = TEXT_HELP_HAPPENINGS + 1;

			public static final short TEXT_OPTION_HOW_TO_PLAY = TEXT_HELP_CARDS + 1;
			public static final short TEXT_OPTION_ATTRIBUTES = TEXT_OPTION_HOW_TO_PLAY + 1;
			public static final short TEXT_OPTION_ACTIVITIES = TEXT_OPTION_ATTRIBUTES + 1;
			public static final short TEXT_OPTION_MINIGAMES = TEXT_OPTION_ACTIVITIES + 1;
			public static final short TEXT_OPTION_HAPPENINGS = TEXT_OPTION_MINIGAMES + 1;
			public static final short TEXT_OPTION_CARDS = TEXT_OPTION_HAPPENINGS + 1;
		//</editor-fold>

		public static final short TEXT_TURN_OFF = TEXT_OPTION_CARDS + 1;
		public static final short TEXT_TURN_ON = TEXT_TURN_OFF + 1;
		public static final short TEXT_ON = TEXT_TURN_ON + 1;
		public static final short TEXT_OFF = TEXT_ON + 1;
		public static final short TEXT_SOUND = TEXT_OFF + 1;
		public static final short TEXT_VIBRATION = TEXT_SOUND + 1;
		public static final short TEXT_CANCEL = TEXT_VIBRATION + 1;
		public static final short TEXT_CONTINUE = TEXT_CANCEL + 1;
		public static final short TEXT_SPLASH_NANO = TEXT_CONTINUE + 1;
		public static final short TEXT_SPLASH_BRAND = TEXT_SPLASH_NANO + 1;
		public static final short TEXT_LOADING = TEXT_SPLASH_BRAND + 1;
		public static final short TEXT_DO_YOU_WANT_SOUND = TEXT_LOADING + 1;
		public static final short TEXT_YES = TEXT_DO_YOU_WANT_SOUND + 1;
		public static final short TEXT_NO = TEXT_YES + 1;
		public static final short TEXT_BACK_MENU = TEXT_NO + 1;
		public static final short TEXT_CONFIRM_BACK_MENU = TEXT_BACK_MENU + 1;
		public static final short TEXT_EXIT_GAME = TEXT_CONFIRM_BACK_MENU + 1;
		public static final short TEXT_CONFIRM_EXIT_1 = TEXT_EXIT_GAME + 1;
		public static final short TEXT_PRESS_ANY_KEY = TEXT_CONFIRM_EXIT_1 + 1;
		public static final short TEXT_GAME_OVER = TEXT_PRESS_ANY_KEY + 1;
		public static final short TEXT_RECOMMEND_TITLE = TEXT_GAME_OVER + 1;
		public static final short TEXT_PROFILE_SELECTION = TEXT_RECOMMEND_TITLE + 1;
		public static final short TEXT_CHOOSE_ANOTHER = TEXT_PROFILE_SELECTION + 1;
		public static final short TEXT_CONFIRM = TEXT_CHOOSE_ANOTHER + 1;
		public static final short TEXT_NO_PROFILE = TEXT_CONFIRM + 1;
		public static final short TEXT_TIME_FORMAT = TEXT_NO_PROFILE + 1;
		public static final short TEXT_HIGH_SCORES = TEXT_TIME_FORMAT + 1;
		public static final short TEXT_VOLUME = TEXT_HIGH_SCORES + 1;
		public static final short TEXT_VERSION = TEXT_VOLUME + 1;
		public static final short TEXT_PHOTO_GALLERY = TEXT_VERSION + 1;
		public static final short TEXT_LEVEL_CLEARED = TEXT_PHOTO_GALLERY + 1;
		public static final short TEXT_LEVEL_SHOW = TEXT_LEVEL_CLEARED + 1;
		public static final short TEXT_NEW_RECORD = TEXT_LEVEL_SHOW + 1;
		public static final short TEXT_RESTART = TEXT_NEW_RECORD + 1;
		public static final short TEXT_RECOMMEND_SENT = TEXT_RESTART + 1;
		public static final short TEXT_RECOMMEND_TEXT = TEXT_RECOMMEND_SENT + 1;
		public static final short TEXT_RECOMMENDED = TEXT_RECOMMEND_TEXT + 1;
		public static final short TEXT_RECOMMEND_SMS = TEXT_RECOMMENDED + 1;
		public static final short TEXT_RANKING_ACCUMULATED_POINTS = TEXT_RECOMMEND_SMS + 1;
		public static final short TEXT_RANKING_MAX_POINTS = TEXT_RANKING_ACCUMULATED_POINTS + 1;
		public static final short TEXT_RANKING_MAX_LEVEL = TEXT_RANKING_MAX_POINTS + 1;
		public static final short TEXT_CURRENT_PROFILE = TEXT_RANKING_MAX_LEVEL + 1;
		public static final short TEXT_SPLASH_ENDEMOL = TEXT_CURRENT_PROFILE + 1;
		public static final short TEXT_ACTIVITY_SOCIALIZE = TEXT_SPLASH_ENDEMOL + 1;
		public static final short TEXT_ACTIVITY_READ = TEXT_ACTIVITY_SOCIALIZE + 1;
		public static final short TEXT_ACTIVITY_DRESS_UP = TEXT_ACTIVITY_READ + 1;
		public static final short TEXT_ACTIVITY_SLEEP = TEXT_ACTIVITY_DRESS_UP + 1;
		public static final short TEXT_ACTIVITY_WATCH_TV = TEXT_ACTIVITY_SLEEP + 1;
		public static final short TEXT_ACTIVITY_MIRROR_PRATICE = TEXT_ACTIVITY_WATCH_TV + 1;
		public static final short TEXT_ACTIVITY_WORK_OUT = TEXT_ACTIVITY_MIRROR_PRATICE + 1;
		public static final short TEXT_ACTIVITY_PILATES = TEXT_ACTIVITY_WORK_OUT + 1;
		public static final short TEXT_ACTIVITY_SWIM = TEXT_ACTIVITY_PILATES + 1;
		public static final short TEXT_ACTIVITY_SUNBATHING = TEXT_ACTIVITY_SWIM + 1;
		public static final short TEXT_ACTIVITY_PREPARE_MEAL = TEXT_ACTIVITY_SUNBATHING + 1;
		public static final short TEXT_ACTIVITY_EAT = TEXT_ACTIVITY_PREPARE_MEAL + 1;
		public static final short TEXT_ACTIVITY_PLAY_SNOOKER = TEXT_ACTIVITY_EAT + 1;
		public static final short TEXT_ACTIVITY_PLAY_VIDEO_GAME = TEXT_ACTIVITY_PLAY_SNOOKER + 1;
		public static final short TEXT_ROOM_BEDROOM = TEXT_ACTIVITY_PLAY_VIDEO_GAME + 1;
		public static final short TEXT_ROOM_LIVINGROOM = TEXT_ROOM_BEDROOM + 1;
		public static final short TEXT_ROOM_GYM = TEXT_ROOM_LIVINGROOM + 1;
		public static final short TEXT_ROOM_POOL = TEXT_ROOM_GYM + 1;
		public static final short TEXT_ROOM_KITCHEN = TEXT_ROOM_POOL + 1;
		public static final short TEXT_ROOM_GAMEROOM = TEXT_ROOM_KITCHEN + 1;
		public static final short TEXT_DAY_SUNDAY = TEXT_ROOM_GAMEROOM + 1;
		public static final short TEXT_DAY_MONDAY = TEXT_DAY_SUNDAY + 1;
		public static final short TEXT_DAY_THUESDAY = TEXT_DAY_MONDAY + 1;
		public static final short TEXT_DAY_WEDNESDAY = TEXT_DAY_THUESDAY + 1;
		public static final short TEXT_DAY_THURSDAY = TEXT_DAY_WEDNESDAY + 1;
		public static final short TEXT_DAY_FRIDAY = TEXT_DAY_THURSDAY + 1;
		public static final short TEXT_DAY_SATURDAY = TEXT_DAY_FRIDAY + 1;
		public static final short TEXT_DAY_SUNDAY_ACRONYM = TEXT_DAY_SATURDAY + 1;
		public static final short TEXT_DAY_MONDAY_ACRONYM = TEXT_DAY_SUNDAY_ACRONYM + 1;
		public static final short TEXT_DAY_THUESDAY_ACRONYM = TEXT_DAY_MONDAY_ACRONYM + 1;
		public static final short TEXT_DAY_WEDNESDAY_ACRONYM = TEXT_DAY_THUESDAY_ACRONYM + 1;
		public static final short TEXT_DAY_THURSDAY_ACRONYM = TEXT_DAY_WEDNESDAY_ACRONYM + 1;
		public static final short TEXT_DAY_FRIDAY_ACRONYM = TEXT_DAY_THURSDAY_ACRONYM + 1;
		public static final short TEXT_DAY_SATURDAY_ACRONYM = TEXT_DAY_FRIDAY_ACRONYM + 1;
		public static final short TEXT_ELIMINATED = TEXT_DAY_SATURDAY_ACRONYM + 1;
		public static final short TEXT_THE_ELIMINATED_IS = TEXT_ELIMINATED + 1;
		public static final short TEXT_ELIMINATION = TEXT_THE_ELIMINATED_IS + 1;
		public static final short TEXT_VOTING = TEXT_ELIMINATION + 1;
		public static final short TEXT_VOTING_COUNT = TEXT_VOTING + 1;
		public static final short TEXT_VOTING_RESULTS = TEXT_VOTING_COUNT + 1;
		public static final short TEXT_ELIMINATION_TEXT = TEXT_VOTING_RESULTS + 1;
		public static final short TEXT_WINNER = TEXT_ELIMINATION_TEXT + 1;
		public static final short TEXT_THE_WINNER_IS = TEXT_WINNER + 1;
		public static final short TEXT_FINAL = TEXT_THE_WINNER_IS + 1;
		public static final short TEXT_WINNER_TEXT = TEXT_FINAL + 1;
		public static final short TEXT_CARD_DAY_REPORT = TEXT_WINNER_TEXT + 1;
		public static final short TEXT_CARD_HAPPENING_REPORT = TEXT_CARD_DAY_REPORT + 1;
		public static final short TEXT_CARD_HAPPENING_CALL = TEXT_CARD_HAPPENING_REPORT + 1;
		public static final short TEXT_CARD_PARTY = TEXT_CARD_HAPPENING_CALL + 1;
		public static final short TEXT_CARD_VOTING = TEXT_CARD_PARTY + 1;
		public static final short TEXT_CARD_ELIMINATION = TEXT_CARD_VOTING + 1;
		public static final short TEXT_CARD_PENALTY_WARNING = TEXT_CARD_ELIMINATION + 1;
		public static final short TEXT_CARD_NO_FOOD = TEXT_CARD_PENALTY_WARNING + 1;
		public static final short TEXT_CARD_NO_SLEEP = TEXT_CARD_NO_FOOD + 1;
		public static final short TEXT_CARD_NO_SLEEP_NO_FOOD = TEXT_CARD_NO_SLEEP + 1;
		public static final short TEXT_CARD_SCOLDING_TEXT = TEXT_CARD_NO_SLEEP_NO_FOOD + 1;
		public static final short TEXT_CARD_PARTY_CARD = TEXT_CARD_SCOLDING_TEXT + 1;
		public static final short TEXT_CARD_VOTING_CARD = TEXT_CARD_PARTY_CARD + 1;
		public static final short TEXT_CARD_ELIMINATION_CARD = TEXT_CARD_VOTING_CARD + 1;
		public static final short TEXT_CARD_LUCK = TEXT_CARD_ELIMINATION_CARD + 1;
		public static final short TEXT_CARD_BADLUCK = TEXT_CARD_LUCK + 1;
		public static final short TEXT_NOMINATED = TEXT_CARD_BADLUCK + 1;
		public static final short TEXT_WINNER_SCREEN_TITLE = TEXT_NOMINATED + 1;
		public static final short TEXT_ELIMINATED_SCREEN_TITLE = TEXT_WINNER_SCREEN_TITLE + 1;
		public static final short TEXT_WINNER_SCREEN_TEXT = TEXT_ELIMINATED_SCREEN_TITLE + 1;
		public static final short TEXT_ELIMINATED_SCREEN_TEXT = TEXT_WINNER_SCREEN_TEXT  + 1;
		public static final short TEXT_MINIGAME_BUTTON_MASING_TUT = TEXT_ELIMINATED_SCREEN_TEXT  + 1;
		public static final short TEXT_MINIGAME_TARGET_TUT = TEXT_MINIGAME_BUTTON_MASING_TUT  + 1;
		public static final short TEXT_MINIGAME_RESULT = TEXT_MINIGAME_TARGET_TUT  + 1;
		public static final short TEXT_TERRIBLE = TEXT_MINIGAME_RESULT  + 1;
		public static final short TEXT_BAD = TEXT_TERRIBLE  + 1;
		public static final short TEXT_REGULAR = TEXT_BAD  + 1;
		public static final short TEXT_GOOD = TEXT_REGULAR  + 1;
		public static final short TEXT_EXCELENT = TEXT_GOOD  + 1;
		public static final short TEXT_MINIGAME_RESULT_FINISH = TEXT_EXCELENT  + 1;
		public static final short TEXT_GO = TEXT_MINIGAME_RESULT_FINISH  + 1;
		public static final short TEXT_MINIGAME_OVER = TEXT_GO  + 1;
		public static final short TEXT_BEST_ONE = TEXT_MINIGAME_OVER + 1;
		public static final short TEXT_WORST_ONE = TEXT_BEST_ONE + 1;


		/** número total de textos do jogo */
		public static final short TEXT_TOTAL = TEXT_WORST_ONE + 1;

		//* numero total de descritores (nomes, comidas e hobbies) nos arquivos */
		public static final short TOTAL_DESCRIPTORS = 15;
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS TELAS DO JOGO">
	//	public static final byte SCREEN_CHOOSE_LANGUAGE				= 0;
	public static final byte SCREEN_CHOOSE_SOUND = 0;
	public static final byte SCREEN_SPLASH_NANO = SCREEN_CHOOSE_SOUND + 1;
	public static final byte SCREEN_SPLASH_BRAND = SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_SPLASH_GAME = SCREEN_SPLASH_BRAND + 1;
	public static final byte SCREEN_MAIN_MENU = SCREEN_SPLASH_GAME + 1;
	public static final byte SCREEN_GAME = SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_MENU_GAME = SCREEN_GAME + 1;
	public static final byte SCREEN_OPTIONS = SCREEN_MENU_GAME + 1;
	public static final byte SCREEN_HELP = SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_MENU = SCREEN_HELP + 1;
	public static final byte SCREEN_CREDITS = SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_LOADING_GAME = SCREEN_CREDITS + 1;
	public static final byte SCREEN_CHOOSE_PROFILE = SCREEN_LOADING_GAME + 1;
	public static final byte SCREEN_LOG = SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_GAME_OVER = SCREEN_LOG + 1;
	//#if DEBUG == "true"
	public static final byte SCREEN_ERROR_LOG = SCREEN_GAME_OVER + 1;
	//#endif
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE NOTÍCIAS">
	// </editor-fold>
	// </editor-fold>

	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA CADA TELA">
		//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA PEQUENA">
		//#if SCREEN_SIZE == "SMALL"
	//# 		//</editor-fold>
		//#
		//#
		//# 		//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA MÉDIA">
			//#elif SCREEN_SIZE == "MEDIUM"
//# 		
//# 				//</editor-fold>
//# 		
//# 		
//# 				//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE">
			//#elif SCREEN_SIZE == "BIG"
				/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
				public static final byte SAFE_MARGIN = 10;

				public static final int CAM_VELOCITY = 500;

				public static final byte TOUCH_TOLERANCE = 50;
				public static final byte TOUCH_TOLERANCE_HALF = TOUCH_TOLERANCE >> 1;

				public static final short SCROLL_BAR_WIDTH = 6;

				public static final byte SPLASH_EXTRA_KEY_THICKNESS = 14;
				public static final byte SPLASH_CHAIN_KEY_HORIZONTAL_POSITION_X = 56;
				public static final byte SPLASH_EXTRA_KEY_TO_CHAIN = 58;

				/** Grossura da borda que ronda o puzzle */
				public final byte TEXT_BKG_BORDER = 5;
				/** Distância entre a borda da tela e a caixa texto */
				public final static int TEXT_ABSOLUTE_MARGIN = TEXT_BKG_BORDER + SAFE_MARGIN;

				/** Distância mínima entre a borda da tela e o logo da SexyHot */
				public final static int SEXY_LOGO_MIN_MARGIN = 5;

				public final static int ITEM_SPACING = 10;
				public final static int FONT_WHITE_HEIGHT = 12;
				public final static int NUMBER_LABEL_HEIGHT = 27;
				public final static int KEYPAD_MINIMUM_SIZE = 120;

				public final static byte TILE_SIZE = 36;
				public final static byte TILE_HALF = TILE_SIZE >> 1;

				public static final byte ACTIVITIES_SPACING = 5;

				public static final byte CHAR_SELECTION_BAR_SIZE = 50;
				public static final byte CHAR_SELECTION_PATTERN_BAR_BORDER = 9;
				public static final byte CHAR_SELECTION_TEXT_PATTERN = 20;
				public static final byte CHAR_SELECTION_ENTRY_MARGIN = 7;
				public static final byte CHAR_SELECTION_TEXT_MARGIN = 4;
				public static final short CHAR_SELECTION_BAR_MAX_WIDTH = 80;

				public static final byte HUD_CLOCK_BAR_DISTANCE = 5;
				public static final byte HUD_BAR_MARGIN = 45;
				public static final byte HUD_CLOCK_FILL_SIZE = 56;
//				public static final short HUD_MAX_MESSAGE_WIDTH = 183;
//				public static final short HUD_MAX_MESSAGE_HEIGHT = 297;
				public static final short HUD_MAX_MESSAGE_WIDTH = 210;
				public static final short HUD_MAX_MESSAGE_HEIGHT = 340;
				public static final short HUD_DAY_BAR_SPACING = SAFE_MARGIN;

				public static final byte BAR_Y_OFFSET_TOP = 4;
				public static final byte BAR_Y_OFFSET_BOTTOM = 5;
				public static final byte BAR_X_OFFSET = -1;

				public static final byte END_FILL_HEIGHT = 85;
				public static final byte END_CROWD_DIFF = -7;
				public static final byte END_SPOT_WIDTH = 100;
				public static final byte END_SPOT_HEIGHT = 30;
				public static final byte END_SPOT_WIDTH_HALF = END_SPOT_WIDTH >> 1;
				public static final byte END_SPOT_HEIGHT_HALF = END_SPOT_HEIGHT >> 1;
				public static final byte END_SAFE_MARGIN = 6;
				public static final byte END_TITLE_SAFE_MARGIN = 12;
				public static final byte END_CROWD_ALPHA_HEIGHT = 18;

				public static final int WALK_JUMP_WIDTH = 20;
				public static final int WALK_JUMP_HEIGHT = 20;

				public static final short PARTICLES_MAX = 500;
				public static final byte PARTICLES_SPEED = 20;

				public static final short ROOM_MENU_WIDTH = 256;

				public static final short TOUCH_KEYPAD_SIZE = 150;

				public static final byte GRAN_HERMANO_LOGO_X = 10;
				public static final byte GRAN_HERMANO_LOGO_Y = 15;

				public static final short SCREEN_FREEAREA_MAX_WIDTH = 290;

				public static final int LOGO_GH_2011_DISTANCE = 3;
				public static final int LOGO_GH_AND_2011_GRAN_HERMANO_DISTANCE = 5;
	//</editor-fold>


	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GIGANTE">
				//#elif SCREEN_SIZE == "GIANT"
//# 				/** espaçamento entre a tela e o elementos alinhado na ponta da tela */
//# 				public static final byte SAFE_MARGIN = 10;
//# 
//# 				public static final int CAM_VELOCITY = 500;
//# 
//# 				public static final byte TOUCH_TOLERANCE = 50;
//# 				public static final byte TOUCH_TOLERANCE_HALF = TOUCH_TOLERANCE >> 1;
//# 
//# 				public static final short SCROLL_BAR_WIDTH = 6;
//# 
//# 				public static final byte SPLASH_EXTRA_KEY_THICKNESS = 14;
//# 				public static final byte SPLASH_CHAIN_KEY_HORIZONTAL_POSITION_X = 56;
//# 				public static final byte SPLASH_EXTRA_KEY_TO_CHAIN = 58;
//# 
//# 				/** Grossura da borda que ronda o puzzle */
//# 				public final byte TEXT_BKG_BORDER = 5;
//# 
//# 				/** Distância entre a borda da tela e a caixa texto */
//# 				public final static int TEXT_ABSOLUTE_MARGIN = TEXT_BKG_BORDER + SAFE_MARGIN;
//# 
//# 				/** Distância mínima entre a borda da tela e o logo da SexyHot */
//# 				public final static int SEXY_LOGO_MIN_MARGIN = 5;
//# 
//# 				public final static int ITEM_SPACING = 10;
//# 				public final static int FONT_WHITE_HEIGHT = 12;
//# 				public final static int NUMBER_LABEL_HEIGHT = 27;
//# 				public final static int KEYPAD_MINIMUM_SIZE = 120;
//# 
//# 				public final static byte TILE_SIZE = 36;
//# 				public final static byte TILE_HALF = TILE_SIZE >> 1;
//# 
//# 				public static final byte ACTIVITIES_SPACING = 5;
//# 
//# 				public static final byte CHAR_SELECTION_BAR_SIZE = 50;
//# 				public static final byte CHAR_SELECTION_TEXT_PATTERN = 20;
//# 				public static final byte CHAR_SELECTION_ENTRY_MARGIN = 7;
//# 				public static final byte CHAR_SELECTION_TEXT_MARGIN = 4;
//# 				public static final short CHAR_SELECTION_BAR_MAX_WIDTH = 80;
//# 
//# 				public static final byte HUD_CLOCK_BAR_DISTANCE = 5;
//# 				public static final byte HUD_BAR_MARGIN = 45;
//# 				public static final byte HUD_CLOCK_FILL_SIZE = 56;
//# //				public static final short HUD_MAX_MESSAGE_WIDTH = 183;
//# //				public static final short HUD_MAX_MESSAGE_HEIGHT = 297;
//# 				public static final short HUD_MAX_MESSAGE_WIDTH = 210;
//# 				public static final short HUD_MAX_MESSAGE_HEIGHT = 340;
//# 				public static final short HUD_DAY_BAR_SPACING = SAFE_MARGIN;
//# 
//# 				public static final byte BAR_Y_OFFSET_TOP = 4;
//# 				public static final byte BAR_Y_OFFSET_BOTTOM = 5;
//# 
//# 				final int PLATFORM_STANDARD_HEIGHT_OFFSET = 0;
//# 				
				//#if VERSION == "GH"
//# 					public static final byte BAR_X_OFFSET = -1;
//# 					public static final byte CHAR_SELECTION_PATTERN_BAR_BORDER = 9;
				//#else
//# 					public static final byte BAR_X_OFFSET = 0;
//# 					public static final byte HUD_BOTTOM_BAR_BORDER = 5;
//# 					public static final byte HUD_BOTTOM_BAR_DESIRED_HEIGHT = 60;
//# 					
//# 					public static final byte CHAR_SELECTION_PATTERN_BAR_BORDER = 6;
				//#endif
//# 					
//# 				public static final byte HUD_TOP_BAR_DESIRED_HEIGHT = 70;
//# 
//# 				public static final byte END_FILL_HEIGHT = 85;
//# 				public static final byte END_CROWD_DIFF = -7;
//# 				public static final byte END_SPOT_WIDTH = 100;
//# 				public static final byte END_SPOT_HEIGHT = 30;
//# 				public static final byte END_SPOT_WIDTH_HALF = END_SPOT_WIDTH >> 1;
//# 				public static final byte END_SPOT_HEIGHT_HALF = END_SPOT_HEIGHT >> 1;
//# 				public static final byte END_SAFE_MARGIN = 6;
//# 				public static final byte END_TITLE_SAFE_MARGIN = 12;
//# 				public static final byte END_CROWD_ALPHA_HEIGHT = 18;
//# 
//# 				public static final int WALK_JUMP_WIDTH = 20;
//# 				public static final int WALK_JUMP_HEIGHT = 20;
//# 
//# 				public static final short PARTICLES_MAX = 500;
//# 				public static final byte PARTICLES_SPEED = 20;
//# 
//# 				public static final short ROOM_MENU_WIDTH = 320;
//# 
//# 				public static final short TOUCH_KEYPAD_SIZE = 150;
//# 
				//#if VERSION == "GH"
//# 					public static final byte GRAN_HERMANO_LOGO_X = 10;
//# 					public static final byte GRAN_HERMANO_LOGO_Y = 15;
				//#else
//# 					public static final byte GRAN_HERMANO_LOGO_X = 10;
//# 					public static final byte GRAN_HERMANO_LOGO_Y = 8;
				//#endif
//# 
//# 				public static final short SCREEN_FREEAREA_MAX_WIDTH = 290;
//# 
//# 				public static final int LOGO_GH_2011_DISTANCE = 3;
//# 				public static final int LOGO_GH_AND_2011_GRAN_HERMANO_DISTANCE = 5;
//# 				
//# 				public static final int MAINMENU_FALL_PIXEL = 20;
//# 				
//# 				public static final int MINIGAME_CUE_BACK_DIST = 100;
//# 				
//# 				public static final int BALOON_CENTER_DIFF = -4;
//# 
//# 				/** Ratio of "deltas" per second */
//# 				public static final int SECOND = 1000;
//# 
//# 				public static final int FP_METER_TO_PIXEL_RATIO = 2293760;	// 35 pixels/meter
//# 				public static final int FP_LEVEL_WATER_AREA_HEIGHT = 32768;	// 0.5 meters
//# 
//# 				public static final int PANORAMA_NUMBER = 4;
//# 
//# 				public static final int FP_COLLISION_SAFE_TIME = 328;			// 0.005 seconds
//# 				public static final int FP_COLLISION_LANDING_THRESHOLD = 16384;	// 0.25 meters
//# 
//# 				public static final byte CONTROLLER_LINE_MAX_WIDTH = 100;
//# 				
//# 				public static final short PARTICLES_MAX_CONFETTIS = 500;
//# 				public static final byte PARTICLES_MIN_CONFETTI_SPEED = 30;
//# 				public static final byte PARTICLES_MAX_CONFETTI_SPEED = 60;
//# 				public static final short PARTICLES_MIN_SPARK_SPEED = 230;
//# 				public static final byte PARTICLES_SPARK_SPEED_RANGE = 50;
//# 				public static final int PARTICLES_GRAVITY_SPEED = 19800000;
//# 				public static final short PARTICLES_MAX_SPARKS = 120;
//# 				public static final short PARTICLES_SPARKS_PER_SECOND = 40;
				//#endif
		//</editor-fold>
	//</editor-fold>
}
