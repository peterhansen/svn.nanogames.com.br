package core;

import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;
import screens.GameMIDlet;

/**
 * Controls character movements when he/she is over its Platform.
 * @see Platform
 * @author Caio, Ygor
 */
public abstract class Controller extends Collidable implements KeyListener {

	private static final int FP_MAX_JUMP_SPEED = NanoMath.toFixed( 8 );
	private static final int FP_MIN_JUMP_SPEED = NanoMath.toFixed( 3 );
	private static final int FP_JUMP_SPEED_RANGE = FP_MAX_JUMP_SPEED - FP_MIN_JUMP_SPEED;
	
	// controller states
	private final static byte STATE_WAITING = 0;
	private final static byte STATE_MOVING = 1;
	private final static byte STATE_PREPARING_SPEED = 2;
	private final static byte STATE_PREPARING_ANGLE = 3;

	public static final byte CONTROLLER_TYPE_AUTO = 0;
	public static final byte CONTROLLER_TYPE_JUMP = 1;
	public static final byte CONTROLLER_TYPE_SINKING = 2;
	protected final byte type;

	/** Platform where the controller resides. */
	protected final Platform platform;

	/** Character currently associated with the controller.
	 * If the controller is not active, the character should be null.
	 */
	protected Character character;

	/** Initial position of the character. */
	protected Point fpP0;

	/** Position of this controller relative to its Platform world position. */
	protected final Point relativeWorldPos = new Point();

	public Controller( Map map, Platform platform, byte type ) {
		super( map );
		this.platform = platform;
		this.type = type;

		//#if DEBUG
			debugCollisionPattern.setFillColor( 0x000000 );
		//#endif
	}

	public final void setRelativeWorldPos( int fixedX, int fixedY ) {
		relativeWorldPos.set( fixedX, fixedY );
	}

	public final Point getRelativeWorldPos() {
		return relativeWorldPos;
	}
	
	public void setCharacter( Character character ) {
		this.character = character;
	}

	public void setInitialPos( int fpWorldX, int fpWorldY ) {
		fpP0.set( fpWorldX, fpWorldY );
	}

	/** Update method of the Controller. This method should be
	 * overloaded by each anonymous production in the factory
	 * method.
	 * @param delta
	 */
	public abstract void update( int delta );	
	
	public abstract void draw( Graphics g, Point center );	
	
	public abstract void resetController() ;

	/** Controller factory method. */
	public static Controller create( Map map, Platform platform, byte type ) {
		switch( type ) {

			//<editor-fold desc="Auto Controller">
			case CONTROLLER_TYPE_AUTO:
				return new Controller( map, platform, type ) {
					private byte state;

					/** Accumulated time, in seconds, since the controller was activated. */
					private int fpAccTime;

					/** Maximum time available for the user to pick the jump speed or angle. */
					private final int FP_MAX_CONTROL_TIME = NanoMath.toFixed( 5 );

					/** Jump speed, measured in meters/second. */
					private int fpJumpSpeed;

					/** Jump angle, measured in degrees. */
					private int jumpAngle;

					/** Time for the controller dials to return to zero.
					 * This is a parameter for controller difficulty.
					 */
					private int difficulty = 4 * Constants.SECOND;
					
					private final int fp_angleCyclePerSecond = NanoMath.divInt( 1, 2 );
					private final int fp_speedCyclePerSecond = NanoMath.divInt( 1, 1 );

					/** The time when the character started gathering speed for the jump. */
					private int fpBeginTime = 0;
							
					/** Width of the jump area, at the end of the platform, in meters. */
					private final int FP_JUMPING_AREA = NanoMath.divInt( 8, 10 );
					//private final int FP_JUMPING_AREA = NanoMath.ONE;

					/** Waiting time, in seconds, before the movement actually starts. */
					private final int FP_WAITING_TIME = NanoMath.HALF;

					/** Speed of the automatic movement, in meters per second. */
					private final int FP_AUTO_SPEED = NanoMath.ONE * 3;

					public void setCharacter( Character character ) {
						super.setCharacter( character );

						resetController();
						// resetting controller
						fpP0 = character.getWorldPosition();
					}
					
					public void resetController() {
						state = STATE_WAITING;
						fpJumpSpeed = fpBeginTime = fpAccTime = jumpAngle = 0;
					}
					
					public void update( int delta ) {
						final int FP_DELTA = NanoMath.divInt( delta, Constants.SECOND );
						fpAccTime += FP_DELTA;

						if( character.getWorldPosition().x <platform.getCollisionBox().x + platform.getCollisionBox().width - FP_JUMPING_AREA ) {
							if( fpAccTime > FP_WAITING_TIME ) {
								state = STATE_MOVING;
								character.setWorldPosition( fpP0.x + NanoMath.mulFixed( FP_AUTO_SPEED, fpAccTime - NanoMath.HALF ), fpP0.y );
							}
						} else {
							switch( state ) {
								case STATE_WAITING:
								case STATE_MOVING:
									state = STATE_PREPARING_SPEED;
									fpBeginTime = fpAccTime;
									jumpAngle = fpJumpSpeed = 0;
									
									//No breaks
								case STATE_PREPARING_SPEED:
									fpJumpSpeed = FP_MIN_JUMP_SPEED + NanoMath.mulFixed( NanoMath.sinInt( NanoMath.toInt( NanoMath.mulFixed( ( fpAccTime - fpBeginTime ) % NanoMath.divFixed( NanoMath.ONE, fp_speedCyclePerSecond ), fp_speedCyclePerSecond * 180 ) ) ), FP_JUMP_SPEED_RANGE );
								break;									
									
								case STATE_PREPARING_ANGLE:
									jumpAngle = 90 - NanoMath.abs( NanoMath.toInt( NanoMath.mulFixed( ( fpAccTime - fpBeginTime ) % NanoMath.divFixed( NanoMath.ONE, fp_angleCyclePerSecond ), fp_angleCyclePerSecond * 180 ) ) - 90 );
								break;
							}
						}
					}

					public void keyPressed( int key ) {
						switch( key ) {
							case ScreenManager.KEY_FIRE:
							case ScreenManager.KEY_NUM5:
							case ScreenManager.KEY_SOFT_MID:
								switch( state ) {
									case STATE_PREPARING_SPEED:
										state = STATE_PREPARING_ANGLE;
										fpBeginTime = fpAccTime;
									break;
										
									case STATE_PREPARING_ANGLE:
										character.setSpeed( NanoMath.mulFixed( fpJumpSpeed, NanoMath.cosInt( jumpAngle ) ), NanoMath.mulFixed( fpJumpSpeed, NanoMath.sinInt( jumpAngle ) ) );
										character.setController( null );
										//setCharacter( null );
										resetController();
									break;
								}
						}
					}

					public void keyReleased( int key ) {
					}
					
					public void draw( Graphics g, Point center ) {
						if( state == STATE_PREPARING_SPEED || state == STATE_PREPARING_ANGLE ) {
							g.setClip( 0,0, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
							
							final int cos = NanoMath.cosInt( jumpAngle );
							final int sin = NanoMath.sinInt( jumpAngle );
							final int dx = NanoMath.toInt( Constants.CONTROLLER_LINE_MAX_WIDTH * NanoMath.divFixed( NanoMath.mulFixed( fpJumpSpeed - FP_MIN_JUMP_SPEED, cos ), FP_JUMP_SPEED_RANGE ) );
							final int dy = -NanoMath.toInt( Constants.CONTROLLER_LINE_MAX_WIDTH * NanoMath.divFixed( NanoMath.mulFixed( fpJumpSpeed - FP_MIN_JUMP_SPEED, sin ), FP_JUMP_SPEED_RANGE ) );

							g.setColor( 0xffff00 );
							g.drawLine( translate.x + center.x, translate.y + center.y - 1, translate.x + center.x + dx, translate.y + center.y + dy - 1 );
							g.drawLine( translate.x + center.x, translate.y + center.y + 1, translate.x + center.x + dx, translate.y + center.y + dy + 1 );
							g.setColor( 0xff0000 );
							g.drawLine( translate.x + center.x, translate.y + center.y, translate.x + center.x + dx, translate.y + center.y + dy );
							
							g.setColor( 0xffff00 );
							g.fillArc( translate.x + center.x - 5, translate.y + center.y - 5, 10, 10, 0, 360 );
							g.setColor( 0xff0000 );
							g.fillArc( translate.x + center.x - 3, translate.y + center.y - 3, 6, 6, 0, 360 );
						}
					}
				};
			//</editor-fold>

			//<editor-fold desc="Jump Controller">
			case CONTROLLER_TYPE_JUMP:
			case CONTROLLER_TYPE_SINKING:
				return new JumpController( map, platform, type);
			//</editor-fold>

			default:
				//#if DEBUG
					GameMIDlet.log( "tentando criar controlador de tipo inexistente " + type );
				//#endif
				return new Controller( map, platform, type ) {
					public void resetController() {}
					public void update( int delta ) {}
					public void keyPressed( int key ) {}
					public void keyReleased( int key ) {}
					public void draw( Graphics g, Point center ) {}
				};
		}
	}

	private static class JumpController extends Controller {

		public JumpController( Map map, Platform platform, byte type ) {
			super( map, platform, type );
		}

		private byte state;

		private int fpAccTime;

		private int fpBeginTime;

		private int fpJumpSpeed;

		private int jumpAngle;

		private int fp_angleCyclePerSecond = NanoMath.divInt( 1, 2 );
		private int fp_speedCyclePerSecond = NanoMath.divInt( 1, 1 );

		private final int FP_MAX_CYCLES = NanoMath.divInt( 3, 1 );
		private final int FP_MIN_CYCLES = NanoMath.divInt( 1, 2 );
		private final int FP_CYCLES_RANGE = FP_MAX_CYCLES - FP_MIN_CYCLES;

		/** Speed used for jump controllers which control sinking platforms. Unit: meters/second. */ 
		private final int FP_SINKING_SPEED = NanoMath.divInt( -1, 25 );

		/** Initial position of the controller, before the character becomes attached to it */
		private Point fpPlatformP0;

		public void setCharacter( Character character ) {
			super.setCharacter( character );
			resetController();

			Rectangle charBox = character.getCollisionBox();
			Rectangle controllerBox = getCollisionBox();

			final int width = ( charBox.width + controllerBox.width ) >> 1;
			final int dist = NanoMath.abs( ( ( charBox.x + ( charBox.width >> 1 ) ) - ( controllerBox.x - ( charBox.width >> 1 ) ) ) - ( width ) );
			final int pc = FP_MIN_CYCLES + ( NanoMath.divFixed( NanoMath.mulFixed( FP_CYCLES_RANGE, NanoMath.clamp( dist, 0, width ) ), width ) );

			fp_angleCyclePerSecond = pc >> 1;
			fp_speedCyclePerSecond = pc;
			
			fpP0 = character.getWorldPosition();
			fpPlatformP0 = platform.getWorldPosition();
		}


		public void resetController() {
			state = STATE_PREPARING_SPEED;
			fpJumpSpeed = fpBeginTime = fpAccTime = jumpAngle = 0;
		}

		protected void failJump() {
			// TODO: calibrar isso direito
			character.push();
			character.setSpeed( 0, 100000 );
			character.setController( null );
		}

		public void update( int delta ) {
			final int FP_DELTA = NanoMath.divInt( delta, Constants.SECOND );
			fpAccTime += FP_DELTA;

			if ( type == CONTROLLER_TYPE_SINKING ) {
				platform.setWorldPosition( fpPlatformP0.x , fpPlatformP0.y + NanoMath.mulFixed( fpAccTime, FP_SINKING_SPEED ) );
				character.setWorldPosition( fpP0.x, fpP0.y + NanoMath.mulFixed( fpAccTime, FP_SINKING_SPEED ) );

				if( platform.getWorldPosition().y < fpPlatformP0.y - ( platform.getWorldSize().y >> 1 ) ) {
					failJump();
				}
			}
			
			switch ( state ) {
				case STATE_PREPARING_SPEED:
					fpJumpSpeed = FP_MIN_JUMP_SPEED + NanoMath.mulFixed( NanoMath.sinInt( NanoMath.toInt( NanoMath.mulFixed( ( fpAccTime - fpBeginTime ) % NanoMath.divFixed( NanoMath.ONE,
																																											  fp_speedCyclePerSecond ),
																															 fp_speedCyclePerSecond * 180 ) ) ),
																		 FP_JUMP_SPEED_RANGE );
					break;
				case STATE_PREPARING_ANGLE:
					jumpAngle = 90 - NanoMath.abs( NanoMath.toInt( NanoMath.mulFixed( ( fpAccTime - fpBeginTime ) % NanoMath.divFixed( NanoMath.ONE,
																																	   fp_angleCyclePerSecond ),
																					  fp_angleCyclePerSecond * 180 ) ) - 90 );
					break;
			}
		}


		public void keyPressed( int key ) {
			switch ( key ) {
				case ScreenManager.KEY_FIRE:
				case ScreenManager.KEY_NUM5:
				case ScreenManager.KEY_SOFT_MID:
					switch ( state ) {
						case STATE_PREPARING_SPEED:
							state = STATE_PREPARING_ANGLE;
							fpBeginTime = fpAccTime;
							break;
						case STATE_PREPARING_ANGLE:
							character.setSpeed( NanoMath.mulFixed( fpJumpSpeed,
																   NanoMath.cosInt( jumpAngle ) ),
												NanoMath.mulFixed( fpJumpSpeed,
																   NanoMath.sinInt( jumpAngle ) ) );
							character.setController( null );
							resetController();
							break;
					}
			}
		}


		public void keyReleased( int key ) {
		}


		public void draw( Graphics g, Point center ) {
			g.setClip( 0, 0, ScreenManager.SCREEN_WIDTH,
					   ScreenManager.SCREEN_HEIGHT );
			final int cos = NanoMath.cosInt( jumpAngle );
			final int sin = NanoMath.sinInt( jumpAngle );
			final int dx = NanoMath.toInt( Constants.CONTROLLER_LINE_MAX_WIDTH * NanoMath.divFixed( NanoMath.mulFixed( fpJumpSpeed - FP_MIN_JUMP_SPEED,
																													   cos ),
																									FP_JUMP_SPEED_RANGE ) );
			final int dy = -NanoMath.toInt( Constants.CONTROLLER_LINE_MAX_WIDTH * NanoMath.divFixed( NanoMath.mulFixed( fpJumpSpeed - FP_MIN_JUMP_SPEED,
																														sin ),
																									 FP_JUMP_SPEED_RANGE ) );
			g.setColor( 0x00ffff );
			g.drawLine( translate.x + center.x, translate.y + center.y - 1, translate.x + center.x + dx,
						translate.y + center.y + dy - 1 );
			g.drawLine( translate.x + center.x, translate.y + center.y + 1, translate.x + center.x + dx,
						translate.y + center.y + dy + 1 );
			g.setColor( 0xff0000 );
			g.drawLine( translate.x + center.x, translate.y + center.y, translate.x + center.x + dx,
						translate.y + center.y + dy );
			g.setColor( 0x00ffff );
			g.fillArc( translate.x + center.x - 5, translate.y + center.y - 5, 10, 10, 0, 360 );
			g.setColor( 0xff0000 );
			g.fillArc( translate.x + center.x - 3, translate.y + center.y - 3, 6, 6, 0, 360 );
		}
	}
}
