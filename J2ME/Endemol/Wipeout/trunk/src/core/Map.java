/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;


/**
 *
 * @author Caio
 */
public class Map extends Drawable implements Constants, ScreenListener {

	//<editor-fold desc="levels constants">
		public static final int FP_LEVEL_MAX_HEIGHT_IN_METERS = 1310720;			// 20 meters
		public static final int LEVEL_INDEX_RED_BALLS = 1;
		public static final int LEVEL_INDEX_STONES = 2;

		public static final int LEVEL_RED_BALLS_MIN_BALLS = 10;
		public static final int LEVEL_RED_BALLS_MAX_BALLS = 25;
		public static final int FP_LEVEL_RED_BALLS_MIN_DISTANCE = NanoMath.ONE;
		public static final int FP_LEVEL_RED_BALLS_MAX_DISTANCE = 131072;			// 2 meters

		public static final int LEVEL_STONES_MIN_STONES = 10;
		public static final int LEVEL_STONES_MAX_STONES = 25;
		public static final int FP_LEVEL_STONES_MIN_DISTANCE = NanoMath.HALF;
		public static final int FP_LEVEL_STONES_MAX_DISTANCE = 131072;				// 2 meters
	//</editor-fold>

	//<editor-fold desc="Panoramas">
		protected static final int PANORAMA_MAX_DISTANCE = 10;

		/** Background color to be displayed behind the panoramas */
		protected static final int PANORAMA_BACKGROUND_COLOR = 0x10B9FC;
		
		/** Height of the first panorama in meters */
		protected static final int PANORAMA_FIRST_PANORAMA_HEIGHT = NanoMath.toFixed( 10 );

		////#if VERSION == "giant"
			// The PANORAMA_HEIGHTS array should always have the same ammount
			// of entries as the PANORAMA_NUMBER constant
 			protected static final int[] PANORAMA_HEIGHTS = { 0, 161, 30, 81 };
		////#endif

		// TODO: to avoid class bloat, this three arrays stand for
		// a Panorama class
		protected final Pattern[] panoramas = new Pattern[ PANORAMA_NUMBER ];
		protected final int[] panoramaDistance = new int[ PANORAMA_NUMBER ];
	//</editor-fold>
		
	//<editor-fold desc="Stage attributes">
		private Camera camera;
		protected Platform[] platforms;
		private int firstVisiblePlatform;
		private int lastVisiblePlatform;
		private Character character;
	//</editor-fold>
		
	private final Rectangle drawableElementArea = new Rectangle();
	
	// TODO: the drawable area's purpose is to draw the map in a subspace
	// of the screen area. We might remove this in the future, if the HUD
	// we use makes this unnecessary.
	private final Rectangle drawableArea = new Rectangle();
	//private final Rectangle intersection = new Rectangle();

	public Map( int level ) {
		//this.character = character;
		
		// loading panorama backgrounds
		for( int i = 0; i < PANORAMA_NUMBER; i++ ) {
			try {
				panoramas[ i ] = new Pattern( new DrawableImage( Constants.PATH_IMAGES + "panorama_" + i + ".png" ) );

				// the farthest the panorama, the larger the distance
				panoramaDistance[ i ] = PANORAMA_MAX_DISTANCE / ( 2*i + 1 );
				
			} catch ( Exception e ) {
			//#if DEBUG
				AppMIDlet.log( "Não foi possível carregar o panorama " + i + " do mapa. " );
				e.printStackTrace();
			//#endif
			}
		}

		// generating platforms
		int levelWidthInMeters =  generatePlatforms( level );
		firstVisiblePlatform = 0;
		lastVisiblePlatform = platforms.length - 1;

		super.setSize(
				NanoMath.toInt( NanoMath.mulFixed( levelWidthInMeters, Constants.FP_METER_TO_PIXEL_RATIO ) ),
				NanoMath.toInt( NanoMath.mulFixed( FP_LEVEL_MAX_HEIGHT_IN_METERS, Constants.FP_METER_TO_PIXEL_RATIO ) )
				);
	}

	public void setCharacter( Character character ) {
		this.character = character;
	}

	public void setCamera( Camera camera ) {
		this.camera = camera;
	}

	public Camera getCamera() {
		return camera;
	}

	public void update( int delta ) {
		character.update( delta );
		for ( int i = 0; i < platforms.length; i++ ) {
			platforms[i].update( delta );
		}
	}

	public Rectangle getDrawableArea() {
		return drawableArea;
	}

	public final void resizeDrawableArea( int width, int height ) {
		drawableArea.set( position.x, position.y, width, height );

		//int fillWidth = parallaxBkg.getFill().getWidth();
		//parallaxBkg.setSize( ( ( width / fillWidth ) + 2 ) * fillWidth, parallaxBkg.getHeight() );

		for ( int i = 0; i < platforms.length; i++ ) {
			Platform platform = platforms[i];
			platform.refreshScreenSize();
			platform.refreshScreenPosition();
		}

		// panoramas position and size
		int accPanHeight = getScreenPosition( new Point( 0, PANORAMA_FIRST_PANORAMA_HEIGHT ) ).y;
		for( int i = 0; i < PANORAMA_NUMBER; i++ ) {
			panoramas[ i ].setSize( super.getWidth(), panoramas[ i ].getFill().getHeight() );
			accPanHeight += PANORAMA_HEIGHTS[ i ];
			panoramas[ i ].setPosition( 0, accPanHeight );
		}

		// character
		character.refreshScreenSize();
		character.refreshScreenPosition();
	}


	public Point getScreenPosition( Point worldPosition ) {
		return getScreenPosition( worldPosition.x, worldPosition.y );
	}

	public Point getScreenSize( Point worldSize ) {
		return getScreenSize(  worldSize.x, worldSize.y );
	}

	public Point getScreenPosition( int fpX, int fpY ) {
		int screenX = NanoMath.toInt( NanoMath.mulFixed( fpX, FP_METER_TO_PIXEL_RATIO ) );
		int screenY = NanoMath.toInt( NanoMath.mulFixed( FP_LEVEL_MAX_HEIGHT_IN_METERS - fpY, FP_METER_TO_PIXEL_RATIO ) );
		return new Point( screenX, screenY );
	}

	public Point getScreenSize( int fpWidth, int fpHeight ) {
		int screenX = NanoMath.toInt( NanoMath.mulFixed( fpWidth, FP_METER_TO_PIXEL_RATIO ) );
		int screenY = NanoMath.toInt( NanoMath.mulFixed( fpHeight, FP_METER_TO_PIXEL_RATIO ) );
		return new Point( screenX, screenY );
	}
	
	protected final void paint( Graphics g ) {
		final Point pos = new Point( translate );
		pos.addEquals( drawableArea.x, drawableArea.y );

		g.setClip( drawableArea.x, drawableArea.y, drawableArea.width, drawableArea.height );
		g.setColor( PANORAMA_BACKGROUND_COLOR );
		g.fillRect( drawableArea.x, drawableArea.y, drawableArea.width, drawableArea.height );

		// panorama drawing:
		// for the parallax we take a linear fraction of the camera X position
		int oldTranslateX = translate.x;
		for( int i = 0; i < PANORAMA_NUMBER; i++ ) {
			translate.x = oldTranslateX / panoramaDistance[ i ];
			panoramas[ i ].draw( g );
		}
		translate.x = oldTranslateX;

		if( firstVisiblePlatform > -1 && lastVisiblePlatform > -1 ) {
			for ( int i = firstVisiblePlatform; i <= lastVisiblePlatform; i++ ) {
				platforms[i].draw( g );
			}
		}

		g.setClip(drawableArea.x, drawableArea.y, drawableArea.width, drawableArea.height);
		
		character.draw( g );
	}

	/** This method is invoked by the camera whenever its focus point
	 * changes.
	 * @param newFocus Point where the camera is currently focusing.
	 */
	public void onCameraFocusMove( Point newFocus ) {
		// calculation of visible indexes of the platforms
		firstVisiblePlatform = -1;
		lastVisiblePlatform = -1;

		for( int i = 0; i < platforms.length; i++ ) {
			int platformLeftX = getPosX() + platforms[ i ].getPosX();
			int platformRightX = platformLeftX + platforms[ i ].getWidth();
			if( platformRightX >= drawableArea.x && platformLeftX <= drawableArea.x + drawableArea.width ) {
				// it is visible!
				if( firstVisiblePlatform == -1 ) {
					firstVisiblePlatform = i;
				}
				lastVisiblePlatform = i;
			}
		}
	}

	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void sizeChanged( int width, int height ) {
	}


	//<editor-fold desc="Level generation method">
		/**
		 * Fills the platform array with random platforms according to level.
		 * @param level
		 * @return The width of this level, in fixed point meters
		 */
		public final int generatePlatforms( int level ) {

			int numberOfPlatforms;
			int accDistance = 0;

			switch ( level ) {
				case LEVEL_INDEX_RED_BALLS:

					numberOfPlatforms = 2 + LEVEL_RED_BALLS_MIN_BALLS + NanoMath.randInt(
							LEVEL_RED_BALLS_MAX_BALLS - LEVEL_RED_BALLS_MIN_BALLS );
					platforms = new Platform[ numberOfPlatforms ];

					// initial platform
					platforms[ 0 ] = Platform.create( this, Platform.PLATFORM_INITIAL, 0 );
					platforms[ 0 ].setWorldPosition( 0, 0 );

					for ( int i = 1; i < numberOfPlatforms; i++ ) {
						accDistance += platforms[i - 1].getWorldSize().x;
						accDistance += FP_LEVEL_RED_BALLS_MIN_DISTANCE + NanoMath.randFixed( FP_LEVEL_RED_BALLS_MAX_DISTANCE - FP_LEVEL_RED_BALLS_MIN_DISTANCE );

						platforms[i] = Platform.create( this, ( i < numberOfPlatforms - 1 ) ? Platform.PLATFORM_RED_BALL : Platform.PLATFORM_FINAL, i );
						platforms[i].setWorldPosition( accDistance, 0 );
					}
					accDistance += platforms[ numberOfPlatforms - 1 ].getWorldSize().x;
					break;

				case LEVEL_INDEX_STONES:

					numberOfPlatforms = 2 + LEVEL_STONES_MIN_STONES + NanoMath.randInt(
							LEVEL_STONES_MAX_STONES - LEVEL_STONES_MIN_STONES );
					platforms = new Platform[ numberOfPlatforms ];

					// initial platform
					platforms[ 0 ] = Platform.create( this, Platform.PLATFORM_INITIAL_LOW, 0 );
					platforms[ 0 ].setWorldPosition( 0, 0 );

					for ( int i = 1; i < numberOfPlatforms; i++ ) {
						accDistance += platforms[i - 1].getWorldSize().x;
						accDistance += FP_LEVEL_STONES_MIN_DISTANCE + NanoMath.randFixed( FP_LEVEL_STONES_MAX_DISTANCE - FP_LEVEL_STONES_MIN_DISTANCE );

						platforms[i] = Platform.create( this, ( i < numberOfPlatforms - 1 ) ? Platform.PLATFORM_STONE : Platform.PLATFORM_FINAL_LOW, i );
						platforms[i].setWorldPosition( accDistance, 0 );
					}
					accDistance += platforms[ numberOfPlatforms - 1 ].getWorldSize().x;
					break;
				}

			return accDistance;
		}
	//</editor-fold>
}
