/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//#if TOUCH == "true"

package core;

import br.com.nanogames.components.userInterface.PointerListener;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

/**
 *	Classe de reconhecimento de gestos
 * @author Caio
 */
public class GestureManager implements PointerListener
{	
	//<editor-fold desc="Fields">
		/** Primeiro ponto da tela onde ocorreu o toque. */
		private final Point firstTouchPosition = new Point();
		/** Último ponto da tela onde ocorreu o toque. */
		private final Point lastTouchPosition = new Point();

		/** Distância máxima para reconhecimento de evento pontual. */
		private static final int TOUCH_DISTANCE_TOLERANCE = 10;
		
		private boolean canBeTap = false;
	//</editor-fold>
	private final GestureListener listener;

	public GestureManager( GestureListener listener ) {
		this.listener = listener;
	}


	public final boolean currentGestureCanBeTap() {
		return canBeTap;
	}


	//<editor-fold desc="Helpers">
		public static final void setTouchArea( Rectangle area, Point pos, Point size, int margin ) {
			setTouchArea( area, pos, size, margin, margin );
		}
		public static final void setTouchArea( Rectangle area, Point pos, Point size, int marginX, int marginY ) {
			area.set( pos.x - ( marginX >> 1 ), pos.y - ( marginY >> 1 ), size.x + marginX, size.y + marginY );
		}
	//</editor-fold>
	
	
	public void onPointerDragged( int x, int y ) {
		/** Ve se já pode desconsiderar os gestos pontuais */
		if( canBeTap && lastTouchPosition.distanceTo( firstTouchPosition ) > TOUCH_DISTANCE_TOLERANCE ) {
			canBeTap = false;
			listener.gestureIsNotSpot( x, y );
		}

		listener.handleDrag( x, y, canBeTap, x - lastTouchPosition.x, y - lastTouchPosition.y );

		lastTouchPosition.set( x, y );
	}

	public void onPointerPressed( int x, int y ) {
		firstTouchPosition.set( x, y );
		lastTouchPosition.set( x, y );
		listener.handleFirstInteraction( x, y );
		canBeTap = true;
	}

	public void onPointerReleased( int x, int y ) {
		if( canBeTap ) listener.handleTap( x, y );
		else listener.gestureIsNotSpot( x, y );
		listener.handleRelease( x, y );
	}
}
//#endif
