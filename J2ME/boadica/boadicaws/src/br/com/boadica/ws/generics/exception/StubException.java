/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.generics.exception;

/**
 *
 * @author Daniel Monteiro
 */
public class StubException extends Exception {

    public StubException(String string, Exception e) {
        super(string,e);
    }

}
