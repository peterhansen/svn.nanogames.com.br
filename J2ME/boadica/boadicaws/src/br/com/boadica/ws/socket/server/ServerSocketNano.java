/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.socket.server;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.net.httpserver.HttpServerImpl;

/**
 *
 * @author Fabiano
 */
public class ServerSocketNano {
private ServerSocket server;
    private int port = 7777;

    public ServerSocketNano() {
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   

   public void handleConnection() {
        try {
            System.out.println("Waiting for client message...");
            HttpServer http = HttpServer.create(new InetSocketAddress(7777), 2);
            http.start();
            //
            // The server do a loop here to accept all connection initiated by the
            // client application.
            //

        } catch (IOException ex) {
            Logger.getLogger(ServerSocketNano.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



   public static void main(String[] args) {
        try {
            HttpServer http = HttpServer.create(new InetSocketAddress(7777),200);

            HttpContext context = http.createContext("/nano", new ConnectionHandler());
            http.setExecutor(null); // creates a default executor
            http.start();
        } catch (IOException ex) {
            Logger.getLogger(ServerSocketNano.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
