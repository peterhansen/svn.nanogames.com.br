/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.facade;

import br.com.boadica.ws.client.MD5Util;
import br.com.boadica.ws.client.WSBoaDicaStub;
import br.com.boadica.ws.xml.parser.XMLParser;
import br.com.boadica.ws.xml.parser.entity.Categoria;
import br.com.boadica.ws.xml.parser.entity.Categorias;
import br.com.boadica.ws.xml.parser.entity.Lojas;
import br.com.boadica.ws.xml.parser.entity.Lojas.Loja;
import br.com.boadica.ws.xml.parser.entity.Ofertas;
import br.com.boadica.ws.xml.parser.entity.Ofertas.Oferta;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Monteiro
 */
public class BoaDicaFacade {

    public static final String URL_CATEGORIAS = "iphone/categorias";
    public static final String URL_CATEGORIA = "iphone/categoria";
    public static final String URL_LOJA = "iphone/loja";

    private static final XMLParser<Categorias> PARSE_CATEGORIA = new XMLParser<Categorias>(Categorias.class);
    private static final XMLParser<Lojas> PARSE_LOJA = new XMLParser<Lojas>(Lojas.class);
    private static final XMLParser<Ofertas> PARSE_OFERTAS = new XMLParser<Ofertas>(Ofertas.class);


    public static final Categorias buscaTodasCategorias() throws Exception{
        try{
            String _retorno = WSBoaDicaStub.communicate(URL_CATEGORIAS);
            Categorias categorias = PARSE_CATEGORIA.doParse(_retorno);

            return categorias;
        }catch (Exception e){
            throw e;
        }
    }


    public static final String[] buscarSubMenu(String menu){
        try {
            Categorias cat = buscaTodasCategorias();
             List<String> list = new ArrayList<String>();
            for (Categoria a : cat.getCategoria()) {
                    if (a.getNome().equals(menu)) {
                        list.add(a.getNome());
                        achou= true;

                    }
                }
            }
            return list.toArray(new String[0]);
            
        } catch (Exception ex) {
           return null;
        }
    }


    public static final String[] carregar(Categoria a){
        List<String> list = new ArrayList<String>();
        for (Categoria a : cat.getCategoria()) {
                if (a.getNome().equals(menu)) {
                    list.add(a.getNome());
                    achou= true;

                }
            }
        }
    return list.toArray(new String[0]);
    }


    public static final Ofertas buscaOferta(Categoria categoria, Integer pagina, Integer codLoja) throws Exception{
        try{
            if(categoria.getArgs() == null){
                return null;
            }
            
            String md5 = MD5Util.getHash(categoria.getArgs().replaceAll("=", "").replaceAll("&", "") + ParametrosEnum.PAGINA + (pagina== null?0:pagina) + ParametrosEnum.CODIGO_LOJA+(codLoja== null? 0:codLoja));
            String _retorno = WSBoaDicaStub.communicate(URL_CATEGORIA+"?" + categoria.getArgs() + "&" + ParametrosEnum.PAGINA + "=" + (pagina== null?0:pagina) + "&" + ParametrosEnum.CODIGO_LOJA+ "=" +(codLoja== null? 0:codLoja) + "&" + ParametrosEnum.ASSINATURA_MD5 + "=" +md5);
            Ofertas ofertas = PARSE_OFERTAS.doParse(_retorno);
            return ofertas;
            
        }catch (Exception e){
           throw e;
        }
    }

   public static final Ofertas buscaOferta(Categoria categoria, ParametrosEnum param) throws Exception{
        try{
            if(categoria.getArgs() == null){
                return null;
            }

            String md5 = MD5Util.getHash(categoria.getArgs().replaceAll("=", "").replaceAll("&", "") + param.name() + param  );
            String _retorno = WSBoaDicaStub.communicate(URL_CATEGORIA+"?" + categoria.getArgs() + "&" + param.name() + "=" + param + "&" + ParametrosEnum.ASSINATURA_MD5 + "=" +md5);
            Ofertas ofertas = PARSE_OFERTAS.doParse(_retorno);
            return ofertas;

        }catch (Exception e){
           throw e;
        }
    }

   public static final Ofertas buscaOferta(Categoria categoria) throws Exception{
        try{
            if(categoria.getArgs() == null){
                return null;
            }

            String md5 = MD5Util.getHash(categoria.getArgs().replaceAll("=", "").replaceAll("&", "") );
            String _retorno = WSBoaDicaStub.communicate(URL_CATEGORIA+"?" + categoria.getArgs() + "&" + ParametrosEnum.ASSINATURA_MD5 + "=" +md5);
            Ofertas ofertas = PARSE_OFERTAS.doParse(_retorno);
            return ofertas;

        }catch (Exception e){
           throw e;
        }
    }


   public static final Loja detalharLoja(String codloja) throws Exception{
         try{
            if(codloja == null || "".equals(codloja)){
                return null;
            }
            String _retorno = WSBoaDicaStub.communicate(URL_LOJA+"/" + codloja);
            Lojas loja = PARSE_LOJA.doParse(_retorno);
            return loja.getLoja().get(0);

        }catch (Exception e){
           throw e;
        }
   }

    public static Ofertas ler(List<Categoria> cat) throws Exception{
        for(Categoria a : cat){
             Ofertas ofertas = buscaOferta(a);
            
        }
        return null;
    }

    public static void lerl(List<Oferta> cat) throws Exception{
        for(Oferta a : cat){
            for(br.com.boadica.ws.xml.parser.entity.Ofertas.Oferta.Loja b: a.getLoja()){
                detalharLoja(b.getCodloja());
            }
        }

    }

    public static void lero(List<Loja> cat) throws Exception{
        for(Loja a : cat){
             detalharLoja(a.getCodloja());
        }
    }

    public static void main(String[] args ){
        try {
            Categorias c = buscaTodasCategorias();

            Ofertas a = ler(c.getCategoria());


        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

}
