/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.xml.parser;

import br.com.boadica.ws.generics.exception.ParserException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Daniel Monteiro
 */
public class XMLParser<E> {

    private JAXBContext jaxbContext =null;
    
    public XMLParser(Class<E> clazz){
        try {
            jaxbContext = JAXBContext.newInstance(clazz);
        } catch (JAXBException ex) {
            System.out.println(ex);
        }
    }



    public E doParse(String xml) throws ParserException{
        E element = null;
        try {
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            element = (E) unmarshaller.unmarshal(new BufferedReader(new StringReader(xml)));
        } catch (JAXBException ex) {
            ex.printStackTrace();
            throw new ParserException("Ocorreu uma exceção ao fazer o parser do XML",ex);
        }

        return element;
    }
}
