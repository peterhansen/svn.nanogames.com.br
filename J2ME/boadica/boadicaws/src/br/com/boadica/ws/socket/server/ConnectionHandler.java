/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.boadica.ws.socket.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Fabiano
 */
public class ConnectionHandler implements HttpHandler  {


    public void handle(HttpExchange he) throws IOException {
        try {
            //
            // Read a message sent by client application
            //
//            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
  //          String message = (String) ois.readObject();
    //        System.out.println("Message Received: " + message);

            //
            // Send a response information to the client application
            //

           String response = "This is the response";
           he.sendResponseHeaders(200, response.length());
           OutputStream os = he.getResponseBody();
           os.write(response.getBytes());
           os.close();

      //      ois.close();



            System.out.println("Waiting for client message...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
