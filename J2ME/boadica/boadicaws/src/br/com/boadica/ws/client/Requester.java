/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boadica.ws.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Daniel Monteiro
 */
public class Requester {

    /**
     * Metodo de comunicao com o webservice
     */
    private static final String GET = "GET";
    /**
     * Tamanho maximo para ser aberto em memoria na comunicacao com o ws
     */
    private static final int TAMANHO_BUFFER_WS = 8192;
    

    /**
     * Faz a chamada do webservice via GET
     * 
     * @param quiet
     * @param url
     * @param body
     * @return
     * @throws IOException
     */
    public static String request(URL url, InputStream body) throws IOException {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=ISO-8859-1");
            connection.setRequestMethod(GET);

            System.out.println(connection.getContentEncoding());
            // Escreve na saida somente se o objeto nao estiver nulo.
            if (body != null) {
                connection.setDoOutput(true);
                writeStream(body, connection.getOutputStream());
            }

            //Faz a conexao no WS
            connection.connect();

            //Le a resposta e retorna
            return readStream(connection.getInputStream());

        } finally {
            if (connection != null) //Fecha a conexao com o WS
            {
                connection.disconnect();
            }
        }

    }

    /**
     * Escreve na saida a chamada do input.
     * 
     * @param input
     * @param output
     * @throws IOException
     */
    private static final void writeStream(InputStream input, OutputStream output) throws IOException {
        byte buffer[] = new byte[TAMANHO_BUFFER_WS];
        int read = 0;
        while ((read = input.read(buffer)) != -1) {
            output.write(buffer, 0, read);
        }

    }

    /**
     * Faz a leitura da chamada do ws e retorna em uma String.
     * 
     * @param responseBodyStream
     * @return
     * @throws IOException
     */
    private static final String readStream(InputStream responseBodyStream) throws IOException {
        StringBuilder responseBody = new StringBuilder();

        int read = 0;
        byte buffer[] = new byte[TAMANHO_BUFFER_WS];
        while ((read = responseBodyStream.read( buffer)) != -1) {
            responseBody.append(new String(buffer, 0, read, "ISO-8859-1"));
        }

        return responseBody.toString();
    }
}
