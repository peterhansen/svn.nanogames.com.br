/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.boadica.ws.client;

import br.com.boadica.ws.generics.exception.StubException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Daniel Monteiro
 */
public class WSBoaDicaStub {

    private static final Map<String, String> CACHED_ELEMENT = new HashMap<String,String>();

    public static final String URL_BOA_DICA = "http://www.boadica.com.br/";

    public static String communicate(String url) throws StubException{
        try{
            if(CACHED_ELEMENT.containsKey(url)){
                return CACHED_ELEMENT.get(url);
            }
            
            String _return = Requester.request(montaCaminhoRelativo(url), null).replaceAll("&" , "&amp;");
            CACHED_ELEMENT.put(url, _return);
            return _return;
        } catch(Exception e){
            throw new StubException("Exceção ao conectar no Webservice", e);
        }
    }

    public static String encodeURL(String url) throws StubException {
        try{
            return URLEncoder.encode(url, "ISO-8859-1");
        } catch(Exception e){
            throw new StubException("Ocorreu uma exceção ao tentar codificar a URL ", e);
        }
    }


    public static URL montaCaminhoRelativo(String caminhoRelativo) throws StubException {
        try{
            URL caminho = new URL(URL_BOA_DICA);
            return new URL(caminho,caminhoRelativo);
        } catch(Exception e){
            throw new StubException("Ocorreu uma exceção ao tentar montar a URL ", e);
        }
    }

}
