package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.MenuLabel;
import javax.microedition.lcdui.Graphics;


public final class LoadScreen extends UpdatableGroup implements Constants, ScreenListener, KeyListener {

	/** Intervalo de atualizaÃ§Ã£o do texto. */
	private static final short CHANGE_TEXT_INTERVAL = 600;

	private long lastUpdateTime;

	private static final byte MAX_DOTS = 4;

	private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

	private byte dots;

	private Thread loadThread;

	private final LoadListener listener;

	private boolean painted;

	private boolean active = true;

	private final int id;

	private final DrawableImage owl;

	private final DrawableImage barBack;

	private final DrawableImage barFront;

	private final DrawableGroup barGroup;

	private final MarqueeLabel label;

	private String text = "";


	public LoadScreen( LoadListener listener, int id ) throws Exception {
		super( 5 );
		this.listener = listener;
		this.id = id;

		barBack = new DrawableImage( PATH_IMAGES + "loading_bar_0.png" );
		barBack.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( barBack );

		barGroup = new DrawableGroup( 1 );
		insertDrawable( barGroup );
		
		barFront = new DrawableImage( PATH_IMAGES + "loading_bar_1.png" );
		barFront.defineReferencePixel( ANCHOR_CENTER );
		barGroup.insertDrawable( barFront );

		label = new MarqueeLabel(
				//#if SCREEN_SIZE == "GIANT"
//# 					FONT_PANE
				//#else
				FONT_TEXT_BOLD
				//#endif
				, null );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		insertDrawable( label );

		owl = new DrawableImage( PATH_IMAGES + "coruja_search.png" );
		owl.defineReferencePixel( owl.getWidth() >> 1, owl.getHeight() + MENU_SPACING );
		insertDrawable( owl );

//		setProgress( -1 );
	}


	public final void update( int delta ) {
		final long interval = System.currentTimeMillis() - lastUpdateTime;
		if ( interval >= CHANGE_TEXT_INTERVAL ) {
			// os recursos do jogo sÃ£o carregados aqui para evitar sobrecarga do mÃ©todo loadResources, o que
			// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
			if ( loadThread == null ) {
				if ( painted && getPosX() == 0 ) {
					final LoadScreen loadScreen = this;
					loadThread = new Thread() {
						public final void run() {
							try {
								MediaPlayer.stop();
								AppMIDlet.gc();
								listener.load( loadScreen );
							} catch ( Throwable e ) {
								//#if DEBUG == "true"
								AppMIDlet.log( e, "a27" );
									e.printStackTrace();
									GameMIDlet.log( e.getMessage() );
								//#endif

//								GameMIDlet.setScreen( SCREEN_CATEGORIES ); //TODO: colocar mensagem de erro
								GameMIDlet.setScreen( SCREEN_ERROR_MESSAGE ); //TODO: colocar mensagem de erro
							}
						}
					};
					loadThread.start();
				}
			} else if ( active ) {
				lastUpdateTime = System.currentTimeMillis();
				dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
				String temp = text;
//					AppMIDlet.gc();
//					String temp = String.valueOf( Runtime.getRuntime().freeMemory() / 1000 );
				for ( byte i = 0; i < dots; ++i ) {
					temp += '.';
				}
				label.setText( temp );
				try {
					// permite que a thread de carregamento dos recursos continue sua execuÃ§Ã£o
					Thread.sleep( CHANGE_TEXT_INTERVAL );
				} catch ( Exception e ) {
					//#if DEBUG == "true"
					AppMIDlet.log( e, "a28" );
						e.printStackTrace();
					//#endif
				}
			}
		}
		label.update( delta );
	}


	// fim do mÃ©todo update( int )
	public final void paint( Graphics g ) {
		super.paint( g );
		painted = true;
	}

	
	public final void setActive( boolean a ) {
		active = a;
	}

	
	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		owl.setPosition( ( width - owl.getWidth() ) >> 1, Math.min( height - owl.getHeight() - barBack.getHeight() - label.getHeight(), Math.max( 0, ( height / 3 ) - ( owl.getHeight() >> 1 ) ) ) );

		barBack.setRefPixelPosition( width >> 1, owl.getRefPixelY() + barBack.getHeight() );
		barGroup.setPosition( barBack.getPosition() );

		label.setSize( width, label.getHeight() );
		label.setPosition( 0, barBack.getPosY() + barBack.getHeight() );
	}


	public final void setText( String text ) {
		this.text = text == null ? "" : text;
		label.setText( text );
		label.setTextOffset( Math.max( ( label.getWidth() - label.getTextWidth() ) >> 1, 0 ) );
	}


	/**
	 * 
	 * @param fp_progress
	 */
	public final void setProgress( int fp_progress ) {
		final int WIDTH = NanoMath.toInt( NanoMath.mulFixed( fp_progress, NanoMath.toFixed( barFront.getWidth() ) ) );
		barGroup.setSize( WIDTH, getHeight() );

		barBack.setVisible( fp_progress >= 0 );
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				setActive( false );
				( ( GameMIDlet ) GameMIDlet.getInstance() ).onChoose( null, id, 0 );
			break;
		}
	}


	public final void keyReleased( int key ) {
	}


	public boolean isActive() {
		return active;
	}
}
