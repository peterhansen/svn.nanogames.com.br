/**
 * GameMIDlet.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import core.MenuLabel;
import core.ScrollBar;
import java.io.DataInputStream;
import java.io.DataOutputStream;

//#if NANO_RANKING == "true"
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.Form;
//#endif

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.form.TouchKeyPad;
//#endif

import br.com.nanogames.components.userInterface.form.FormLabel;

import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.SmsSender;
import br.com.nanogames.components.util.SmsSenderListener;
import core.BoadicaButton;
import core.BoadicaData;
import core.Offer;
import core.PaneLabel;
import core.Store;
import core.Tab;
import core.TabbedPane;
import java.util.Hashtable;
import java.util.Vector;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener, Serializable, EventListener {
	
	private static final short GAME_MAX_FRAME_TIME = 180;
	
	private static final byte BACKGROUND_TYPE_LED_PATTERN	= 0;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 1;
	private static final byte BACKGROUND_TYPE_NONE			= 2;

	public static final byte OPTION_ENGLISH = 0;
	public static final byte OPTION_PORTUGUESE = 1;

	public static final byte OPTION_PLAY_SOUD = 0;
	public static final byte OPTION_NO_SOUND = 1;

	public static final byte CONFIRM_YES = 0;
	public static final byte CONFIRM_NO = 1;
	public static final byte CONFIRM_CANCEL = 2;

	private static final byte ID_BUTTON_MAP_CONFIRM						= 0;
	private static final byte ID_BUTTON_USER_MAP_CONFIRM				= 1;
	private static final byte ID_BUTTON_BACK							= 2;
	private static final byte ID_BUTTON_LOAD_YES						= 3;
	private static final byte ID_BUTTON_LOAD_NO							= 4;
	private static final byte ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_YES	= 5;
	private static final byte ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_NO	= 6;
	private static final byte ID_BUTTON_EXIT_YES						= 7;
	private static final byte ID_BUTTON_EXIT_NO							= 8;

	public static StringBuffer log = new StringBuffer();

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif
	
	private static boolean lowMemory;
	
	private static LoadListener loader;

	private WindowManager windowManager;

	private boolean moveRight = true;

	/** Opção do usuário: quantidade de páginas baixadas por acesso à internet. */
	private static short pagesPerSearch = PAGES_PER_SEARCH_DEFAULT;

	/** Opção do usuário: código da operadora. */
	private static String operatorCode = "";
	
	private static DrawableImage[] ICONS;

	private static TitledPane mainMenuFull;

	private static TitledPane mainMenuPartial;

	private static TitledPane optionsPane;

	private static String lastError = "";

	private final Pattern bkg = new Pattern( 0xffffff );

	private static String numberToCall;


	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
			super( -1, GAME_MAX_FRAME_TIME );
			FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
		//#endif

		log("GameMIDlet antes");
		bkg.setSize( 2000, 2000 );

//		System.out.println(Runtime.getRuntime().freeMemory());
	}

	
	public static final void setSpecialKeyMapping(boolean specialMapping) {
		try {
			ScreenManager.resetSpecialKeysTable();
			final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

			int[][] keys = null;
			final int offset = 'a' - 'A';

			//#if BLACKBERRY_API == "true"
//# 				switch ( Keypad.getHardwareLayout() ) {
//# 					case ScreenManager.HW_LAYOUT_REDUCED:
//# 					case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 						keys = new int[][] {
//# 							{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 							{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 							{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 							{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 							{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 							{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 							{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 							{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//# 
//# 							{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 							{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 							{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 							{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 							{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 							{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 							{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 							{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 							{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 							{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//# 
//# 							{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 						 };
//# 					break;
//# 
//# 					default:
//# 						if ( specialMapping ) {
//# 							keys = new int[][] {
//# 								{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 								{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 								{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 								{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 								{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 								{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 								{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 								{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 								{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//# 
//# 								{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 								{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 								{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 								{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 								{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 								{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 								{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 								{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 								{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//# 
//# 								{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 								{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 							 };
//# 						} else {
//# 							for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 								table.put( new Integer( c ), new Integer( c ) );
//# 								table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 							}
//# 
//# 							final int[] chars = new int[]
//# 							{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 								'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 							};
//# 
//# 							for ( byte i = 0; i < chars.length; ++i )
//# 								table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 						}
//# 				}
//# 
			//#else

				if ( specialMapping ) {
					keys = new int[][] {
						{ 'q', ScreenManager.KEY_NUM1 },
						{ 'Q', ScreenManager.KEY_NUM1 },
						{ 'e', ScreenManager.KEY_NUM3 },
						{ 'E', ScreenManager.KEY_NUM3 },
						{ 'z', ScreenManager.KEY_NUM7 },
						{ 'Z', ScreenManager.KEY_NUM7 },
						{ 'c', ScreenManager.KEY_NUM9 },
						{ 'C', ScreenManager.KEY_NUM9 },
						{ 'w', ScreenManager.UP },
						{ 'W', ScreenManager.UP },
						{ 'a', ScreenManager.LEFT },
						{ 'A', ScreenManager.LEFT },
						{ 's', ScreenManager.FIRE },
						{ 'S', ScreenManager.FIRE },
						{ 'd', ScreenManager.RIGHT },
						{ 'D', ScreenManager.RIGHT },
						{ 'x', ScreenManager.DOWN },
						{ 'X', ScreenManager.DOWN },

						{ 'r', ScreenManager.KEY_NUM1 },
						{ 'R', ScreenManager.KEY_NUM1 },
						{ 'y', ScreenManager.KEY_NUM3 },
						{ 'Y', ScreenManager.KEY_NUM3 },
						{ 'v', ScreenManager.KEY_NUM7 },
						{ 'V', ScreenManager.KEY_NUM7 },
						{ 'n', ScreenManager.KEY_NUM9 },
						{ 'N', ScreenManager.KEY_NUM9 },
						{ 't', ScreenManager.KEY_NUM2 },
						{ 'T', ScreenManager.KEY_NUM2 },
						{ 'f', ScreenManager.KEY_NUM4 },
						{ 'F', ScreenManager.KEY_NUM4 },
						{ 'g', ScreenManager.KEY_NUM5 },
						{ 'G', ScreenManager.KEY_NUM5 },
						{ 'h', ScreenManager.KEY_NUM6 },
						{ 'H', ScreenManager.KEY_NUM6 },
						{ 'b', ScreenManager.KEY_NUM8 },
						{ 'B', ScreenManager.KEY_NUM8 },

						{ 10, ScreenManager.FIRE }, // ENTER
						{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

						{ 'u', ScreenManager.KEY_STAR },
						{ 'U', ScreenManager.KEY_STAR },
						{ 'j', ScreenManager.KEY_STAR },
						{ 'J', ScreenManager.KEY_STAR },
						{ '#', ScreenManager.KEY_STAR },
						{ '*', ScreenManager.KEY_STAR },
						{ 'm', ScreenManager.KEY_STAR },
						{ 'M', ScreenManager.KEY_STAR },
						{ 'p', ScreenManager.KEY_STAR },
						{ 'P', ScreenManager.KEY_STAR },
						{ ' ', ScreenManager.KEY_STAR },
						{ '$', ScreenManager.KEY_STAR },
					 };
				} else {
					for ( char c = 'A'; c <= 'Z'; ++c ) {
						table.put( new Integer( c ), new Integer( c ) );
						table.put( new Integer( c + offset ), new Integer( c + offset ) );
					}

					final int[] chars = new int[]
					{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
						'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
					};

					for ( byte i = 0; i < chars.length; ++i )
						table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
				}
			//#endif

			if ( keys != null ) {
				for ( byte i = 0; i < keys.length; ++i )
					table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				log( e.getClass() + e.getMessage() );
				e.printStackTrace();
			//#endif
		}
	}


	/**
	 * Adiciona uma entrada no log. Não é necessário remover chamadas a esse método em versões de release, pois elas
	 * são descartadas pelo obfuscator.
	 * @param s
	 */
//	public static final void log(String s) {
//		//#if DEBUG == "true"
//			System.gc();
//
//			log.append(s);
//			log.append(": ");
//			final long freeMem = Runtime.getRuntime().freeMemory();
//			log.append( freeMem );
//			log.append( ':' );
//			log.append( freeMem * 100 / Runtime.getRuntime().totalMemory() );
//			log.append( "%\n" );
//
//			System.out.println( s + ": " + freeMem + ": " + ( freeMem * 100 / Runtime.getRuntime().totalMemory() ) + "%" );
//		//#endif
//	}


	//#if DEBUG == "true"
		public final void start() {
			log( "start 1" );
			super.start();
			log( "start 2" );
		}
	//#endif


	protected final void loadResources() throws Exception {
		log("loadResources início");

//	   lowMemory = true; // teste

		//#if SCREEN_SIZE == "SMALL"
//# 			FONTS[ 0 ] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_0");
//# 			FONTS[ 0 ].setCharExtraOffset( 1 );
		//#endif

		for (byte i = 0; i < FONT_TYPES_TOTAL; ++i) {
			//#if SCREEN_SIZE == "SMALL"
//# 				FONTS[ i ] = FONTS [ 0 ];
			//#else
			if ( i != FONT_TEXT_WHITE )
				FONTS[ i ] = ImageFont.createMultiSpacedFont(PATH_IMAGES + "font_" + i);

			switch ( i ) {
				case FONT_PANE:
				case FONT_TEXT_BOLD:
				case FONT_TEXT:
					FONTS[ i ].setCharExtraOffset( 1 );
				break;

				case FONT_TEXT_WHITE:
				break;


				default:
					FONTS[ i ].setCharExtraOffset( DEFAULT_FONT_OFFSET );
			}
			log( "FONTE " + i );
			//#endif
		}

		// cria a base de dados do jogo
		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
			AppMIDlet.log( e, "a14" );
		}
		
		log( "database 2" );

		try {
			loadData( DATABASE_NAME, DATABASE_SLOT_OPTIONS, this );
		} catch ( Exception e ) {
			AppMIDlet.log( e, "a15" );
			setLanguage( NanoOnline.LANGUAGE_pt_BR );
			pagesPerSearch = PAGES_PER_SEARCH_DEFAULT;
		}
		
		log( "textos" );

		NanoOnline.init( NanoOnline.LANGUAGE_pt_BR, APP_SHORT_NAME );
		BoadicaData.load();

		log("loadResources fim");
		setScreen( SCREEN_SPLASH_NANO );
	} // fim do mÃ©todo loadResources()


	/**
	 * 
	 * @param language
	 */
	protected final void changeLanguage( byte language ) {
		GameMIDlet.log( "changeLanguage início" );
		language = 2;// NanoOnline.LANGUAGE_pt_BR;
		try {
			loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts_" + language + ".dat" );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				AppMIDlet.log( ex, "a16" );
				ex.printStackTrace();
			//#endif
		}
		GameMIDlet.log( "changeLanguage fim" );
	}


	/**
	 * 
	 * @param phoneNumber
	 * @param addOperatorCode
	 * @return
	 */
	public static final String formatPhoneNumber( String phoneNumber, boolean addOperatorCode ) {
		// remove caracteres não numéricos do telefone (alguns aparelhos não fazem a ligação nesse caso)
		String number = ( addOperatorCode ? operatorCode : "" ) + phoneNumber;
		final char[] chars = number.toCharArray();
		int total = chars.length;
		for ( byte i = 0; i < total; ) {
			if ( !( chars[ i ] >= '0' && chars[ i ] <= '9' ) ) {
				for ( int j = i; j < total - 1; ++j )
					chars[ j ] = chars[ j + 1 ];

				--total;
			} else {
				++i;
			}
		}
		number = new String( chars, 0, total );

		if ( number.length() > 8 && number.indexOf( '0' ) != 0 )
			number = '0' + number;

		return number;
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getStorePhonesMenu( Store store ) throws Exception {
		final String[] phones = store.getPhones( false );

		//#if DEBUG == "true"
			for ( byte i = 0; i < phones.length; ++i )
				System.out.println( "PHONE[ " + i + "]: ->" + phones[ i ] + "<-" );
		//#endif
		
		final Form f = getForm();

		final Container c = new Container( 15, getLayout( false ) );
		final FormText label = new FormText( GetFont( FONT_PANE ), 3 );
		label.setText( getText( TEXT_CALL_INFO ) );
		c.insertDrawable( label );

		final EventListener listener = new EventListener() {
			public final void eventPerformed( Event evt ) {
				switch ( evt.eventType ) {
					case Event.EVT_BUTTON_CONFIRMED:
						final String number = formatPhoneNumber( phones[ evt.source.getId() ], true );
						if ( !callPhoneNumber( number, false ) ) {
							numberToCall = number;
							try {
								getTabbedPane().setPane( getExitConfirm( true ) , ICON_PHONE );
							} catch( Throwable t ) {
								//#if DEBUG == "true"
									AppMIDlet.log( t, "Dialogo de confirmação de saida para ligação" );
								//#endif
							}
						}
					break;

					case Event.EVT_KEY_PRESSED:
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_LEFT:
							case ScreenManager.KEY_NUM4:
								getTabbedPane().previousPane();
							break;

							case ScreenManager.KEY_RIGHT:
							case ScreenManager.KEY_NUM6:
								getTabbedPane().nextPane();
							break;
						}
					break;
				}
			}
		};



		BoadicaButton prev = null;
		BoadicaButton first = null;
		BoadicaButton last = null;
		
		for ( byte i = 0; i < phones.length; ++i ) {
			final BoadicaButton b = new BoadicaButton( phones[ i ] );
			b.setId( i );
			b.addEventListener( listener );
			c.insertDrawable( b );

			b.setNextFocusLeft( prev );
			b.setNextFocusUp( prev );

			if (prev != null){
				prev.setNextFocusDown( b );
				prev.setNextFocusRight( b );
			} else
				first = b;

			prev = b;
			last = b;
		}

		first.setNextFocusLeft( last );
		first.setNextFocusUp( last );
		last.setNextFocusDown( first );
		last.setNextFocusRight( first );

		f.setContentPane( c );

		return new TitledPane( f, getText( TEXT_PHONE ), true );
	}


	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getMapSubMenu( boolean userOffer ) throws Exception {
		final Form f = getForm();

		final Container c = new Container( 5, getLayout() );
		final FormText label = new FormText( GetFont( FONT_PANE ), 3 );
		label.setText( getText( TEXT_MAP_CONFIRM ) );
		c.insertDrawable( label );
		final BoadicaButton b = new BoadicaButton( TEXT_CONFIRM );
		b.setId( userOffer ? ID_BUTTON_USER_MAP_CONFIRM : ID_BUTTON_MAP_CONFIRM );
		b.addEventListener( ( GameMIDlet ) instance );
		c.insertDrawable( b );
		f.setContentPane( c );

		return new TitledPane( f, getText( TEXT_MAP ), false );
	}


	private static final LineBorder getBorder() throws Exception {
		final LineBorder b1 = new LineBorder( 0x1c3d30 );
		b1.setFillColor( 0xd7e9d4 );

		return b1;
	}


	/**
	 * 
	 * @return
	 * @param app 
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getRecommendScreen( final boolean app ) throws Exception {
		final Form f = getForm();
		
		final TextBox areaCode = new TextBox( GetFont( FONT_PANE ), null, 3, TextBox.INPUT_MODE_NUMBERS );
		areaCode.setCaret( getCaret() );
		areaCode.setBorder( getBorder() );

		final TextBox number = new TextBox( GetFont( FONT_PANE ), null, 12, TextBox.INPUT_MODE_NUMBERS );
		number.setCaret( getCaret() );
		number.setBorder( getBorder() ) ;

		final FormText label = new FormText( GetFont( FONT_PANE ), 20 );
		label.setText( getText( app ? TEXT_RECOMMEND_TEXT : TEXT_RECOMMEND_OFFER ), true, true );

		final BoadicaButton button = new BoadicaButton( TEXT_CONFIRM );

		final SmsSenderListener smsListener = new SmsSenderListener() {
			public final void onSMSSent( int id, boolean smsSent ) {
				label.setText( getText( smsSent? TEXT_RECOMMEND_SENT: TEXT_RECOMMEND_NOTSENT ) );
				areaCode.setText( null );
				number.setText( null );
				f.refreshLayout();
			}

			/** Fornece informações passo-a-passo das rotinas de envio de SMS */
			public final void onSMSLog( int id, String log ) {
				//#if DEBUG == "true"
					System.out.println( "onSMSLog: " + id + ", " + log );
				//#endif
			}
		};

		final EventListener listener = new EventListener() {
			public final void eventPerformed( Event evt ) {
				switch ( evt.eventType ) {
					case Event.EVT_BUTTON_CONFIRMED:
						final SmsSender sender = new SmsSender();
						final String text = app ? getText( TEXT_RECOMMEND_APP_SMS ) + getRecommendURL() :
									getText( TEXT_RECOMMEND_OFFER_SMS ) + OffersScreen.getCurrentOffer().getRecommendText();

						String n = number.getText();
						if ( areaCode.getText().length() > 1 ) {
							// usuário adicionou DDD
							if ( areaCode.getText().length() > 2 ) {
								// DDD do tipo "021" ou "031"
								n = areaCode.getText() + n;
							} else {
								// DDD do tipo "21" ou "31"
								n = '0' + areaCode.getText() + n;
							}
						}

						sender.send( n, text, smsListener );
					break;

					case Event.EVT_TEXTBOX_RIGHT_ON_END:
						if ( evt.source == areaCode || evt.source == number ) {
							evt.source.setHandlesInput( false );
						}
					break;

					case Event.EVT_TEXTBOX_LEFT_ON_START:
						if ( evt.source == number ) {
							evt.source.setHandlesInput( false );
						} else {
							getTabbedPane().keyReleased( 0 );
							getTabbedPane().previousPane();
						}
					break;

					case Event.EVT_KEY_PRESSED:
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_RIGHT:
								getTabbedPane().nextPane();
							break;
						}
					break;
				}
			}
		};

		areaCode.setNextFocusDown( number );
		areaCode.setNextFocusUp( button );

		number.setNextFocusUp( areaCode );
		number.setNextFocusDown( button );

		areaCode.setNextFocusRight( number );
		number.setNextFocusLeft( areaCode );
		number.setNextFocusRight( button );

		areaCode.setNextFocusLeft( button );
		number.setNextFocusLeft( areaCode );
		number.setNextFocusRight( button );

		button.setNextFocusUp( number );
		button.setNextFocusDown( areaCode );

		button.setNextFocusLeft( number );
		button.setNextFocusRight( areaCode );

		button.setNextFocusUp( number );
		button.setNextFocusDown( areaCode );

		number.addEventListener( listener );
		areaCode.addEventListener( listener );
		button.addEventListener( listener );

		final Container c = new Container( 8, getLayout() );
		c.insertDrawable( label );

		c.insertDrawable( areaCode );
		c.insertDrawable( number );

		c.insertDrawable( button );
		f.setContentPane( c );

		final TitledPane t = new TitledPane( f, getText( TEXT_RECOMMEND_TITLE ), true );
		t.setHasSpecialKeyMapping( true );
		return t;
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	private static final Drawable getCaret() {
		final Pattern caret = new Pattern( 0x005500 );
		caret.setSize( 1, GetFont( FONT_PANE ).getHeight() );
		return caret;
	}


	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getOfferMenu( final Offer offer ) throws Exception {
		final Form f = getForm();
		final Container c = new Container( 5, getLayout() );
		final FormText label = new FormText( GetFont( FONT_PANE ), 4 );

		label.setText( getText( offer.isSaved() ? TEXT_OFFER_ERASE : TEXT_OFFER_SAVE ) );
		c.insertDrawable( label );

		final TextBox t = new TextBox( GetFont( FONT_PANE ), null, 120, TextBox.INPUT_MODE_ANY );
		t.setCaret( getCaret() );
		t.setVisible( !offer.isSaved() );
		t.addEventListener( ( GameMIDlet ) instance );
		c.insertDrawable( t );
		
		final LineBorder b = new LineBorder( 0x1c3d30 );
		b.setFillColor( PANE_COLOR );
		t.setBorder( b ) ;
		final BoadicaButton button = new BoadicaButton( TEXT_CONFIRM );
		final TitledPane tp = new TitledPane( f, getText( offer.isSaved()? TEXT_ERASE : TEXT_SAVE ), true );
		tp.setHasSpecialKeyMapping( true );

		button.addEventListener( new EventListener() {
			public final void eventPerformed( Event evt ) {
				switch ( evt.eventType ) {

					case Event.EVT_KEY_PRESSED:
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_LEFT:
							case ScreenManager.KEY_NUM4: {
									getTabbedPane().previousPane();
									evt.consume();
								}
								break;

							case ScreenManager.KEY_RIGHT:
							case ScreenManager.KEY_NUM6: {
									getTabbedPane().nextPane();
									evt.consume();
								}
								break;
						}
						break;

	

					case Event.EVT_BUTTON_CONFIRMED:
						if ( offer.isSaved() ) {
							try {
								// apagou a oferta
								BoadicaData.removeUserOffer( offer );
								t.setText( null );
								t.setVisible( true );
								label.setText( getText( TEXT_OFFER_ERASED ) );
								tp.setTitle( getText( TEXT_SAVE ));
								final Tab tab = ( ( GameMIDlet ) instance ).windowManager.getTabbedPane().getTabByIndex( ICON_DELETE );
								tab.setIcon( ICON_SAVE );
								button.setText( TEXT_CONFIRM );
								refreshFavoritesPane();
							} catch ( Exception ex ) {
								//#if DEBUG == "true"
									AppMIDlet.log( ex, "a17" );
									ex.printStackTrace();
								//#endif
							}
						} else {
							if ( t.getText().length() > 0 )
								offer.setName( t.getText() );
							else
								offer.setName( offer.getShortName() );

							try {
								BoadicaData.addUserOffer( offer );
								label.setText( getText( TEXT_OFFER_SAVED ) );
								tp.setTitle( getText( TEXT_ERASE ));
								t.setVisible( false );
								final Tab tab = ( ( GameMIDlet ) instance ).windowManager.getTabbedPane().getTabByIndex( ICON_SAVE );
								tab.setIcon( ICON_DELETE );
								button.setText( TEXT_REMOVE );
								refreshFavoritesPane();
							} catch ( Exception ex ) {
								// TODO mostrar mensagem de erro
								//#if DEBUG == "true"
									AppMIDlet.log( ex, "a18" );
									ex.printStackTrace();
								//#endif
							}
						}
						f.refreshLayout();
						t.requestFocus();
					break;
				}
			}
		});

		button.setNextFocusDown( label );
		button.setNextFocusUp( label );

		c.insertDrawable( button );
		f.setContentPane( c );

		return tp;
	}


	private static final TabbedPane getTabbedPane() {
		return ( ( GameMIDlet ) instance ).windowManager.getTabbedPane();
	}


	private static final void refreshFavoritesPane() {
		try {
			getTabbedPane().setPane( getFavoritesMenu(), ICON_FAVORITES );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				AppMIDlet.log( e, "a19" );
				e.printStackTrace();
			//#endif
		}
	}


//	private static final void refreshFavoritesPane() {
//
//	}


	private static final Form getForm() throws Exception {
		final Form f = new Form();
		
		//#if TOUCH == "true"
			f.setTouchKeyPad( new TouchKeyPad( GetFont( FONT_TEXT ), new DrawableImage( PATH_ONLINE + "clear.png" ), new DrawableImage( PATH_ONLINE + "shift.png" ) ) );
		//#endif

		return f;
	}


	private static final FlowLayout getLayout() {
		return getLayout( true );
	}


	private static final FlowLayout getLayout( boolean horizontal ) {
		final FlowLayout layout = new FlowLayout( horizontal ? FlowLayout.AXIS_HORIZONTAL : FlowLayout.AXIS_VERTICAL );
		layout.start.y = 5;
		layout.gap.x = 5;
		layout.gap.y = 3;

		return layout;
	}


	private static final TitledPane getExitConfirm( boolean hasOperation ) throws Exception {
		final Form f = getForm();

		final Container c = new Container( 5, getLayout() );
		final FormText label = new FormText( GetFont( FONT_PANE ), 6 );
		label.setText( getText( hasOperation ? TEXT_EXIT_AND_EXECUTE_OPERATION : TEXT_EXIT_CONFIRMATION ) + ( hasOperation ? ( numberToCall.toString() + "?" ) : "" ) );
		label.setFocusable( false );
		c.insertDrawable( label );

		final BoadicaButton buttonYes = new BoadicaButton( TEXT_YES );
		buttonYes.setId( hasOperation ? ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_YES : ID_BUTTON_EXIT_YES );
		buttonYes.addEventListener( ( GameMIDlet ) instance );
		c.insertDrawable( buttonYes );

		final BoadicaButton buttonNo = new BoadicaButton( TEXT_NO );
		buttonNo.setId( hasOperation ? ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_NO : ID_BUTTON_EXIT_NO );
		buttonNo.addEventListener( ( GameMIDlet ) instance );
		c.insertDrawable( buttonNo );


		f.setContentPane( c );

		buttonNo.setNextFocusLeft( buttonYes );
		buttonYes.setNextFocusRight( buttonNo );

		return new TitledPane( f, getText( TEXT_EXIT ), true );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getFavoritesMenu() throws Exception {
		final Offer[] userOffers = BoadicaData.getUserOffers();

		if ( userOffers.length > 0 ) {
			final String[] entries = new String[ userOffers.length ];
			final int[] entriesIcons = new int[ userOffers.length ];
			for ( byte i = 0; i < entries.length; ++i ) {
				entries[ i ] = userOffers[ i ].getName();
				entriesIcons[ i ] = ICON_FAVORITES;
			}

			final BoadicaMenu favoritesMenu = new BoadicaMenu( SCREEN_USER_OFFERS, entries, TEXT_OPTIONS, entriesIcons );
			return new TitledPane( favoritesMenu, getText( TEXT_USER_OFFERS ), false );
		} else {
			final Form f = getForm();

			final Container c = new Container( 5, getLayout() );
			final FormText label = new FormText( GetFont( FONT_PANE ), 5 );
			label.setText( getText( TEXT_NO_USER_OFFERS_FOUND ) );
			c.insertDrawable( label );
			f.setContentPane( c );

			return new TitledPane( f, getText( TEXT_USER_OFFERS ), false );
		}
	}


	public static final TitledPane getBackPane( boolean back ) throws Exception {
		final Form f = getForm();

		final Container c = new Container( 5, getLayout() );
		final FormText label = new FormText( GetFont( FONT_PANE ), 4 );
		label.setText( getText( back ? TEXT_CONFIRM_BACK_TOTAL : TEXT_CONFIRM_EXIT_TOTAL ) );
		c.insertDrawable( label );

		final BoadicaButton button = new BoadicaButton( back ? TEXT_BACK : TEXT_EXIT );
		button.setId( ID_BUTTON_BACK );
		button.addEventListener( ( GameMIDlet ) instance );
		c.insertDrawable( button );


		f.setContentPane( c );

		return new TitledPane( f, getText( back ? TEXT_BACK : TEXT_EXIT ), false );
	}

	/**
	 * salva as opções do usuario
	 * @throws Exception
	 */
    public static final void saveOptions() throws Exception {
		saveData( DATABASE_NAME, DATABASE_SLOT_OPTIONS, ( GameMIDlet ) instance );
	}

	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getOptionsMenu() throws Exception {
		if ( optionsPane == null ) {
			final Form f = getForm();
			FlowLayout layout;
			layout = getLayout( false );

			if (ScreenManager.SCREEN_WIDTH < 240){
				layout.start.y = 0;
				layout.gap.x = 5;
				layout.gap.y = 0;
			}

			final Container c = new Container( 4, layout );
			
			final Container line1 = new Container(2, getLayout());

			layout = getLayout();
			if (ScreenManager.SCREEN_WIDTH < 240){
				layout.start.y = 2;
				layout.gap.x = 5;
				layout.gap.y = 0;
			}

			final Container line2 = new Container(1, layout);

			layout = getLayout();
			if (ScreenManager.SCREEN_WIDTH < 240){
				layout.start.y = 2;
				layout.gap.x = 5;
				layout.gap.y = 0;
			}

			final Container line3 = new Container(4, layout);
			final Container line4 = new Container(2, getLayout());
			
			//#if RECOMMEND_SCREEN == "true"
//# 				///segunda linha: codigo de operadora
//# 				//texto "codigo de operadora"
//# 				//TODO: procurar saber se tem alguma constante de texto referente a este label
//# 				final FormLabel carrierCodeLabel = new FormLabel( GetFont( FONT_PANE ), getText( TEXT_CARRIER_CODE ));
//# 				line1.insertDrawable( carrierCodeLabel );
//# 
//# 				//caixa de texto do codigo de operadora
//# 				final TextBox carrierCodeTextbox = new TextBox(GetFont( FONT_TEXT ), operatorCode, 3, TextBox.INPUT_MODE_NUMBERS );
//# 				carrierCodeTextbox.setActive( true );
//# 				carrierCodeTextbox.setBorder( getBorder(), true);
//# 				carrierCodeTextbox.setCaret( getCaret());
//# 				line1.insertDrawable( carrierCodeTextbox );
//# 				c.insertDrawable( line1 );
			//#endif
			
			///terceira linha: quantos resultados por pagina

			//texto "quantos resultados por pagina
			final FormLabel resultsPerPageLabel = new FormLabel( GetFont( FONT_PANE ), getText( TEXT_RESULTS_PER_SEARCH ));

			line2.insertDrawable( resultsPerPageLabel );
			c.insertDrawable( line2 );
			
			//botao -
			final BoadicaButton btnMinus = new BoadicaButton( TEXT_MINUS );
			line3.insertDrawable( btnMinus );
			btnMinus.setId( -1 );

			//caixa de texto: quantos resultados por pagina atualmente
			final TextBox resultsPerPageTextbox = new TextBox(GetFont( FONT_TEXT ), " ", NanoMath.max( getText(TEXT_ALL).length() + 1, (String.valueOf(PAGES_PER_SEARCH_MAX)).length() + 1), TextBox.INPUT_MODE_NUMBERS );
			resultsPerPageTextbox.setText( String.valueOf( pagesPerSearch));
			resultsPerPageTextbox.setBorder( getBorder(), true);
			resultsPerPageTextbox.setEnabled( false );
			resultsPerPageTextbox.setActive( false );
			line3.insertDrawable( resultsPerPageTextbox );

			//botao +
			final BoadicaButton btnPlus = new BoadicaButton( TEXT_PLUS );
			line3.insertDrawable( btnPlus );
			c.insertDrawable( line3 );
			btnPlus.setId( 1 );

			///ultima linha
			//texto explicando a "limpeza de dados"
			final FormText eraseDataLabel = new FormText( GetFont( FONT_PANE ), 100);
			eraseDataLabel.setText( getText( TEXT_CLEAR_RMS ), true, true);

			line4.insertDrawable( eraseDataLabel );

			//botão para limpar os dados
			final BoadicaButton btnErase = new BoadicaButton( TEXT_ERASE );

			btnErase.addEventListener( new EventListener() {
				public void eventPerformed( Event evt ) {
					if (evt.eventType ==  Event.EVT_BUTTON_CONFIRMED){
						BoadicaData.getInstance().clearRMS();
						setScreen(SCREEN_CATEGORIES_ROOT);
					}
				}
			});

			line4.insertDrawable( btnErase );
			c.insertDrawable( line4 );

			final EventListener listener = new EventListener() {

				public void eventPerformed( Event evt ) {
					if ( !evt.isConsumed() ) {
						switch ( evt.eventType ) {
							case Event.EVT_BUTTON_CONFIRMED: {
								pagesPerSearch = ( short ) ( pagesPerSearch + evt.source.getId() );

								if ( pagesPerSearch < PAGES_PER_SEARCH_MIN ) {
									pagesPerSearch = PAGES_PER_SEARCH_MAX;
								} else if ( pagesPerSearch > PAGES_PER_SEARCH_MAX ) {
									pagesPerSearch = PAGES_PER_SEARCH_MIN;
								}
							}
							// sem break

							case Event.EVT_FOCUS_GAINED:
							case Event.EVT_FOCUS_LOST: {
								//#if RECOMMEND_SCREEN == "true"
//# 									operatorCode = carrierCodeTextbox.getText();
								//#endif

								int maxlength = String.valueOf( PAGES_PER_SEARCH_MAX ).length();
								int length = String.valueOf( pagesPerSearch ).length() + 1;
								String padding = " ";

								for ( int c = 0; c <= ( maxlength - length ) >> 1; ++c ) {
									padding += " ";
								}

								resultsPerPageTextbox.setText( padding + String.valueOf( pagesPerSearch ) );
							}
							break;

							case Event.EVT_KEY_PRESSED:
								switch ( ( ( Integer ) evt.data ).intValue() ) {
									case ScreenManager.KEY_LEFT:
									case ScreenManager.KEY_NUM4:
										if ( evt.source.getId() < 0 ) {
											getTabbedPane().previousPane();
											evt.consume();
										}
									break;

									case ScreenManager.KEY_RIGHT:
									case ScreenManager.KEY_NUM6:
										if ( evt.source.getId() > 0 ) {
											getTabbedPane().nextPane();
											evt.consume();
										}
									break;
								}
								break;

							case Event.EVT_TEXTBOX_LEFT_ON_START:
								getTabbedPane().previousPane();
								evt.consume();
								break;

							case Event.EVT_TEXTBOX_RIGHT_ON_END:
								getTabbedPane().nextPane();
								evt.consume();
								break;

							case Event.EVT_TEXTBOX_CHAR_COMMITED:
							case Event.EVT_TEXTBOX_FILLED:
								//#if RECOMMEND_SCREEN == "true"
//# 									operatorCode = carrierCodeTextbox.getText();
								//#endif
							break;
						}
					}
				}
			};

			btnMinus.addEventListener( listener );
			btnPlus.addEventListener( listener );

			btnMinus.setNextFocusRight( btnPlus );
			btnPlus.setNextFocusLeft( btnMinus );
			btnMinus.setNextFocusDown( btnPlus );
			btnPlus.setNextFocusUp( btnMinus );

			btnPlus.setNextFocusDown( btnErase );
			btnErase.setNextFocusUp( btnPlus );

			//#if RECOMMEND_SCREEN == "true"
//# 				carrierCodeTextbox.addEventListener( listener );
			//#endif
			
			f.setContentPane( c );
			optionsPane =  new TitledPane( f, getText( TEXT_OPTIONS ), true );
		}

		optionsPane.setHasSpecialKeyMapping( true );

		return optionsPane;
	}


	public final void destroy() {
		//#if DEBUG == "true"
			BoadicaData.unload();
		//#endif
			
		try {
			saveOptions();
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				AppMIDlet.log( ex, "a20" );
				ex.printStackTrace();
			//#endif
		}
		super.destroy();
	}


	public static final void setLastError( String lastError ) {
		GameMIDlet.lastError = lastError;
	}
	

	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getMainMenu() throws Exception {
		return getMainMenu( true );
	}


	/**
	 * 
	 * @param full
	 * @return
	 * @throws java.lang.Exception
	 */
	private static final TitledPane getMainMenu( boolean full ) throws Exception {
		if ( full ) {
			if ( mainMenuFull == null ) {
				final int[] icons = new int[] {
					ICON_SEARCH,
					ICON_HELP,
					ICON_HELP,
					ICON_CREDITS,
					ICON_EXIT
				};

				mainMenuFull = new TitledPane( new BoadicaMenu( SCREEN_MAIN_MENU_FULL, new int[]{
									TEXT_PRICE_SEARCH,
									TEXT_HOW_TO_USE,
									TEXT_COSTS,
									TEXT_CREDITS,
									TEXT_EXIT,
								}, TEXT_MENU, icons ), getText( TEXT_MENU ), false );
			}

			return mainMenuFull;
		} else {
			if ( mainMenuPartial == null ) {
				final int[] icons = new int[] {
					ICON_HELP,
					ICON_HELP,
					ICON_CREDITS,
					ICON_EXIT
				};

				mainMenuPartial = new TitledPane( new BoadicaMenu( SCREEN_MAIN_MENU_PARTIAL, new int[]{
									TEXT_HOW_TO_USE,
									TEXT_COSTS,
									TEXT_CREDITS,
									TEXT_EXIT,
								}, TEXT_MENU, icons ), getText( TEXT_MENU ), false );
			}

			return mainMenuPartial;
		}
	}


	public static final DrawableImage getIcon( int index ) {
		if ( ICONS == null ) {
			ICONS = new DrawableImage[ ICONS_TOTAL ];
			try {
				for ( byte i = 0; i < ICONS_TOTAL; ++i )
					ICONS[ i ] = new DrawableImage( PATH_MENU + "icon_" + i + ".png" );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					AppMIDlet.log( e, "a21" );
					e.printStackTrace();
				//#endif
			}
		}

		return new DrawableImage( ICONS[ index ] );
	}


	protected final int changeScreen(int screen) throws Exception {



		//#if DEBUG == "true"
		switch( screen ) {
			case SCREEN_SPLASH_NANO	: { log( "SCREEN_SPLASH_NANO" ); } break;
			case SCREEN_SPLASH_BOADICA: { log( "SCREEN_SPLASH_BOADICA" ); } break;
			case SCREEN_MAIN_MENU_PARTIAL: { log( "SCREEN_MAIN_MENU_PARTIAL" ); } break;
			case SCREEN_MAIN_MENU_FULL: { log( "SCREEN_MAIN_MENU_FULL" ); } break;
			case SCREEN_OPTIONS	: { log( "SCREEN_OPTIONS" ); } break;
			case SCREEN_HELP_MENU: { log( "SCREEN_HELP_MENU" ); } break;
			case SCREEN_HELP_CONTROLS: { log( "SCREEN_HELP_CONTROLS" ); } break;
			case SCREEN_HELP_COSTS: { log( "SCREEN_HELP_COSTS" ); } break;
			case SCREEN_HELP_PROFILE: { log( "SCREEN_HELP_PROFILE" ); } break;
			case SCREEN_CREDITS		: { log( "SCREEN_CREDITS" ); } break;
			case SCREEN_LOADING_1	: { log( "SCREEN_LOADING_1" ); } break;
			case SCREEN_LOADING_2	: { log( "SCREEN_LOADING_2" ); } break;
			case SCREEN_CHOOSE_LANGUAGE: { log( "SCREEN_CHOOSE_LANGUAGE" ); } break;
			case SCREEN_NANO_RANKING_MENU: { log( "SCREEN_NANO_RANKING_MENU" ); } break;
			case SCREEN_NANO_RANKING_PROFILES : { log( "SCREEN_NANO_RANKING_PROFILES" ); } break;
			case SCREEN_LOADING_RECOMMEND_SCREEN : { log( "SCREEN_LOADING_RECOMMEND_SCREEN" ); } break;
			case SCREEN_ERROR_LOG	: { log( "SCREEN_ERROR_LOG" ); } break;
			case SCREEN_PRICES_NEW	: { log( "SCREEN_PRICES_NEW" ); } break;
			case SCREEN_PRICES		: { log( "SCREEN_PRICES" ); } break;
			case SCREEN_CATEGORIES_ROOT: { log( "SCREEN_CATEGORIES_ROOT" ); } break;
			case SCREEN_CATEGORIES	: { log( "SCREEN_CATEGORIES" ); } break;
			case SCREEN_LOAD_CATEGORIES_INFO : { log( "SCREEN_LOAD_CATEGORIES_INFO" ); } break;
			case SCREEN_LOAD_CATEGORIES: { log( "SCREEN_LOAD_CATEGORIES" ); } break;
			case SCREEN_USER_OFFER_DETAILS: { log( "SCREEN_USER_OFFER_DETAILS" ); } break;
			case SCREEN_OFFER_DETAILS: { log( "SCREEN_OFFER_DETAILS" ); } break;
			case SCREEN_LOAD_OFFERS	: { log( "SCREEN_LOAD_OFFERS" ); } break;
			case SCREEN_USER_OFFERS	: { log( "SCREEN_USER_OFFERS" ); } break;
			case SCREEN_USER_OFFERS_NEW: { log( "SCREEN_USER_OFFERS_NEW" ); } break;
			case SCREEN_MAP			: { log( "SCREEN_MAP" ); } break;
			case SCREEN_LOAD_MAP	: { log( "SCREEN_LOAD_MAP" ); } break;
			case SCREEN_LOAD_USER_MAP: { log( "SCREEN_LOAD_USER_MAP" ); } break;
			case SCREEN_OFFER_MENU	: { log( "SCREEN_OFFER_MENU" ); } break;
			case SCREEN_USER_MAP	: { log( "SCREEN_USER_MAP" ); } break;
			default: { log( "changedScreen " + screen ); }
		}
		//#endif

		final GameMIDlet midlet = (GameMIDlet) instance;

		log("changeScreen " + screen);

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = BACKGROUND_TYPE_LED_PATTERN;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;

		boolean useWindowManager = true;
		Drawable nextScreen = null;
		final Vector panes = new Vector();
		final Vector tabs = new Vector();
		boolean back = true;


		switch (screen) {
			case SCREEN_SPLASH_NANO:
				useWindowManager = false;

				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				nextScreen = new SplashNano();
			break;

			case SCREEN_SPLASH_BOADICA:
				useWindowManager = false;
				
				nextScreen = new SplashBoadica();
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
			break;

			case SCREEN_USER_MAP:
			case SCREEN_MAP:
				nextScreen = new MapScreen( screen == SCREEN_USER_MAP ? SCREEN_USER_OFFER_DETAILS : SCREEN_OFFER_DETAILS );

				panes.addElement( getMainMenu() );
				tabs.addElement( new Integer( ICON_MENU ) );
				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );
			break;

			case SCREEN_ERROR_MESSAGE:{
				int backTo = SCREEN_CATEGORIES_ROOT;
				
				if ( currentScreen == SCREEN_LOAD_MAP ){
					backTo = SCREEN_OFFER_DETAILS;
				} else if ( currentScreen == SCREEN_LOAD_USER_MAP ) {
					backTo = SCREEN_USER_OFFER_DETAILS;
				}
				nextScreen = new TextScreen( backTo , getText(TEXT_ERROR_TEXT) + "\n" + lastError, getText(TEXT_ERROR_TITLE), false, null);
			}
			break;

			case SCREEN_CATEGORIES_ROOT:
				BoadicaMenu.setCurrentCategory( null );
				
			case SCREEN_CATEGORIES:
				back = BoadicaMenu.getCurrentCategory() != null;

				OffersScreen.unload();
				nextScreen = BoadicaMenu.createCategoriesMenu();

				panes.addElement( getMainMenu( false ) );
				tabs.addElement( new Integer( ICON_MENU ) );

				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );

				panes.addElement( getOptionsMenu() );
				tabs.addElement( new Integer( ICON_OPTIONS ) );

				//#if RECOMMEND_SCREEN == "true"
//# 					if ( SmsSender.isSupported() ) {
//# 						panes.addElement( getRecommendScreen( true ) );
//# 						tabs.addElement( new Integer( ICON_MAIL ) );
//# 					}
				//#endif
			break;

			case SCREEN_LOAD_CATEGORIES_INFO:
				final Form f = new Form();
				final FormText text = new FormText( GetFont( FONT_TEXT ), 7 );
				text.setText( getText( TEXT_LOAD_CATEGORIES_INFO) );

				final FlowLayout l = getLayout( false );
				l.start.x = l.start.y;
				l.setAlignment( Drawable.ANCHOR_HCENTER );
				final Container c = new Container( 5, l );

				final Container buttonContainer = new Container( 5, getLayout() );
				final BoadicaButton buttonYes = new BoadicaButton( TEXT_YES );
				buttonYes.setId( ID_BUTTON_LOAD_YES );
				buttonYes.addEventListener( midlet );
				final BoadicaButton buttonNo = new BoadicaButton( TEXT_NO );
				buttonNo.setId( ID_BUTTON_LOAD_NO );
				buttonNo.addEventListener( midlet );
				
				buttonContainer.insertDrawable( buttonYes );
				buttonContainer.insertDrawable( buttonNo );

				c.insertDrawable( text );
				c.insertDrawable( buttonContainer );

				f.setContentPane( c );
				buttonYes.requestFocus();

				buttonYes.setNextFocusRight( buttonNo );
				buttonYes.setNextFocusLeft( buttonNo );
				buttonNo.setNextFocusLeft( buttonYes );
				buttonNo.setNextFocusRight( buttonYes );

				buttonYes.setNextFocusUp( buttonNo );
				buttonYes.setNextFocusDown( buttonNo );
				buttonNo.setNextFocusUp( buttonYes );
				buttonNo.setNextFocusDown( buttonYes );

				nextScreen = f;
			break;

			case SCREEN_LOAD_CATEGORIES:
				nextScreen = new LoadScreen( new LoadListener() {
					public final void load( final LoadScreen loadScreen ) throws Exception {
						try {
							BoadicaData.refreshCategories( loadScreen, SCREEN_CATEGORIES_ROOT );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
								AppMIDlet.log( e, "a22" );
								System.out.println( "" );
							//#endif
							loadScreen.setActive( false );
							GameMIDlet.setLastError( e.toString() );
							setScreen( SCREEN_ERROR_MESSAGE );
						}
					}
				}, screen );
			break;

			case SCREEN_PRICES_NEW:
				OffersScreen.load( SCREEN_CATEGORIES );
				// sem break
				
			case SCREEN_PRICES:
				nextScreen = OffersScreen.getInstance();

				panes.addElement( getMainMenu() );
				tabs.addElement( new Integer( ICON_MENU ) );
				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );

				panes.addElement( getOptionsMenu() );
				tabs.addElement( new Integer( ICON_OPTIONS ) );
			break;

			case SCREEN_USER_OFFERS_NEW:
				OffersScreen.load( SCREEN_CATEGORIES );
				// sem break

			case SCREEN_USER_OFFERS:
				OffersScreen.setOffers( BoadicaData.getUserOffers(), 0 );
				nextScreen = OffersScreen.getInstance();

				panes.addElement( getMainMenu() );
				tabs.addElement( new Integer( ICON_MENU ) );
				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );

				panes.addElement( getOptionsMenu() );
				tabs.addElement( new Integer( ICON_OPTIONS ) );
			break;

			case SCREEN_LOAD_OFFERS:
				nextScreen = new LoadScreen( new LoadListener() {
					public final void load( final LoadScreen loadScreen ) throws Exception {
						try {
							//#if DEBUG == "true"
								System.out.println( "LOAD OFFERS: " + OffersScreen.getInstance() );
							//#endif
							BoadicaData.getOffers( loadScreen, OffersScreen.getInstance() == null ? SCREEN_PRICES_NEW : SCREEN_PRICES, BoadicaMenu.getCurrentCategory().getId() );
						} catch ( Exception e )	{
							//#if DEBUG == "true"
									AppMIDlet.log( e, "a23" );
								e.printStackTrace();
							//#endif
							loadScreen.setActive( false );
							GameMIDlet.setLastError( e.toString() );
							setScreen( SCREEN_ERROR_MESSAGE );
						}
					}
				}, screen );
			break;

			case SCREEN_LOAD_USER_MAP:
			case SCREEN_LOAD_MAP:
				final int s = screen == SCREEN_LOAD_USER_MAP ? SCREEN_USER_MAP : SCREEN_MAP;
				nextScreen = new LoadScreen( new LoadListener() {
					public final void load( final LoadScreen loadScreen ) throws Exception {
						try {
							//#if DEBUG == "true"
								System.out.println( "LOAD MAP: " + OffersScreen.getInstance() );
							//#endif
							BoadicaData.loadMap( OffersScreen.getCurrentOffer().getStore(), loadScreen, s );
						} catch ( Exception e )	{
							//#if DEBUG == "true"
									AppMIDlet.log( e, "a24" );
								e.printStackTrace();
							//#endif
							loadScreen.setActive( false );
							GameMIDlet.setLastError( e.toString() );
							setScreen( SCREEN_ERROR_MESSAGE );
						}
					}
				}, screen );
			break;

			case SCREEN_USER_OFFER_DETAILS:
			case SCREEN_OFFER_DETAILS:
				final Offer currentOffer = OffersScreen.getCurrentOffer();
				nextScreen = new TextScreen( ( screen == SCREEN_USER_OFFER_DETAILS ) ? SCREEN_CATEGORIES_ROOT : SCREEN_PRICES, currentOffer.toFormattedString(), null, false, null );
				( ( TextScreen ) nextScreen ).setFonts( new ImageFont[] {
							GetFont( FONT_TEXT ),
							GetFont( FONT_TEXT ),
							GetFont( FONT_TEXT_BOLD ),
				} );

				panes.addElement( getMainMenu() );
				tabs.addElement( new Integer( ICON_MENU ) );
				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );

				panes.addElement( getOptionsMenu() );
				tabs.addElement( new Integer( ICON_OPTIONS ) );
				
				if ( currentOffer.isSaved() ) {
					panes.addElement( getOfferMenu( currentOffer ) );
					tabs.addElement( new Integer( ICON_DELETE ) );
				} else {
					panes.addElement( getOfferMenu( currentOffer ) );
					tabs.addElement( new Integer( ICON_SAVE ) );
				}

				panes.addElement( getStorePhonesMenu( currentOffer.getStore() ) );
				tabs.addElement( new Integer( ICON_PHONE ) );

				//#if RECOMMEND_SCREEN == "true"
//# 					if ( SmsSender.isSupported() ) {
//# 						panes.addElement( getRecommendScreen( false ) );
//# 						tabs.addElement( new Integer( ICON_MAIL ) );
//# 					}
				//#endif

				final Store store = currentOffer.getStore();
				if ( store != null && store.hasGeoInfo() ) {
					panes.addElement( getMapSubMenu( screen == SCREEN_USER_OFFER_DETAILS ) );
					tabs.addElement( new Integer( ICON_MAP ) );
				}
			break;

			case SCREEN_ERROR_LOG:
			case SCREEN_HELP_COSTS:
			case SCREEN_HELP_CONTROLS:
				String title = null;
				switch ( screen ) {
					case SCREEN_HELP_COSTS:
						title = getText( TEXT_COSTS );
					break;

					case SCREEN_HELP_CONTROLS:
						title = getText( TEXT_HOW_TO_USE );
					break;
				}

				if ( windowManager.getNextScreen() != null				 &&
 					 windowManager.getNextScreen() instanceof TextScreen &&
				   ( currentScreen == SCREEN_ERROR_LOG					 ||
					 currentScreen == SCREEN_HELP_COSTS					 ||
					 currentScreen == SCREEN_CREDITS					 ||
					 currentScreen == SCREEN_HELP_CONTROLS				 )) {

					currentScreen = ( byte ) ( ( TextScreen ) windowManager.getNextScreen() ).getBaseTextScreen().getNextScreenIndex();
				}

				//#if DEBUG == "true"
				nextScreen = new TextScreen( currentScreen, AppMIDlet.getLog(), title, false, null );
				//#else
//#  					nextScreen = new TextScreen( currentScreen, getText( TEXT_HELP_HOW_TO_USE + (screen - SCREEN_HELP_CONTROLS)) + getVersion() , title, false, null );
				//#endif
					
				indexSoftRight = TEXT_BACK;

				panes.addElement( getMainMenu() );
				tabs.addElement( new Integer( ICON_MENU ) );
				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );

				panes.addElement( getOptionsMenu() );
				tabs.addElement( new Integer( ICON_OPTIONS ) );

				//#if RECOMMEND_SCREEN == "true"
//# 					if ( SmsSender.isSupported() ) {
//# 						panes.addElement( getRecommendScreen( true ) );
//# 						tabs.addElement( new Integer( ICON_MAIL ) );
//# 					}
				//#endif
			break;

			case SCREEN_CREDITS:
				if ( windowManager.getNextScreen() != null				 &&
 					 windowManager.getNextScreen() instanceof TextScreen &&
				   ( currentScreen == SCREEN_ERROR_LOG					 ||
					 currentScreen == SCREEN_HELP_COSTS					 ||
					 currentScreen == SCREEN_CREDITS					 ||
					 currentScreen == SCREEN_HELP_CONTROLS				 )) {

					currentScreen = ( byte ) ( ( TextScreen ) windowManager.getNextScreen() ).getBaseTextScreen().getNextScreenIndex();
				}

				nextScreen = new TextScreen( currentScreen, getText( TEXT_CREDITS_TEXT ), getText( TEXT_CREDITS ), true, null );
				indexSoftRight = TEXT_BACK;

				panes.addElement( getMainMenu() );
				tabs.addElement( new Integer( ICON_MENU ) );
				panes.addElement( getFavoritesMenu() );
				tabs.addElement( new Integer( ICON_FAVORITES ) );

				panes.addElement( getOptionsMenu() );
				tabs.addElement( new Integer( ICON_OPTIONS ) );

				//#if RECOMMEND_SCREEN == "true"
//# 					if ( SmsSender.isSupported() ) {
//# 						panes.addElement( getRecommendScreen( true ) );
//# 						tabs.addElement( new Integer( ICON_MAIL ) );
//# 					}
				//#endif
			break;
		} // fim switch ( screen )
		
		setBackground( bkgType );

		//#if DEBUG == "true"
		if ( nextScreen == null )
			throw new IllegalStateException( "Warning: nextScreen null (" + screen + ")." );
		//#endif

		if ( useWindowManager ) {
			if ( windowManager == null ) {
				windowManager = new WindowManager();
			}

			windowManager.setPaneIcons( panes, tabs, back );
			windowManager.setScreen( nextScreen, midlet.moveRight );

			midlet.manager.setCurrentScreen( windowManager );
		} else {
			midlet.manager.setCurrentScreen( nextScreen );
		}

		bkg.setViewport( windowManager.getScreenViewport() );

		return screen;
	} // fim do mÃ©todo changeScreen( int )


	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_LED_PATTERN:
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( midlet.bkg, false );
			break;
		}
	} // fim do mÃ©todo setBackground( byte )

	
	public static final Drawable getScrollFull() throws Exception {
		return new ScrollBar( ScrollBar.TYPE_BACKGROUND );
	}


	public static final Drawable getScrollPage() throws Exception {
		return new ScrollBar( ScrollBar.TYPE_FOREGROUND );
	}


	public static final boolean isLowMemory() {
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			return true;
		//#else
			return lowMemory;
		//#endif
	}


	public static final void setMoveRight( boolean moveRight ) {
		( ( GameMIDlet ) instance ).moveRight = moveRight;
	}


	private static final void refreshResultsLabel( Menu menu, int index ) {
		if ( pagesPerSearch >= PAGES_PER_SEARCH_MIN )
			( ( PaneLabel ) menu.getDrawable( index ) ).setText( getText( TEXT_RESULTS_PER_SEARCH ) + pagesPerSearch );
		else
			( ( PaneLabel ) menu.getDrawable( index ) ).setText( getText( TEXT_RESULTS_PER_SEARCH ) + getText( TEXT_ALL ) );
	}


	public static final short getPagesPerSearch() {
		return pagesPerSearch;
	}


	public final void onChoose( Menu menu, int id, int index ) {
		//#if DEBUG == "true"
		System.out.println( "onChoose( " + menu + ", " + id + ", "+ index + " )" );
		//#endif

		moveRight = true;
		switch ( id ) {
			case SCREEN_MAIN_MENU_PARTIAL:
				++index;
			case SCREEN_MAIN_MENU_FULL:
				setSpecialKeyMapping(true);
//				//#if RECOMMEND_SCREEN == "false"
//					if ( index >= ENTRY_MAIN_MENU_RECOMMEND )
//						++index;
//				//#endif
				
				switch (index) {
					case ENTRY_MAIN_MENU_PRICE_SEARCH:
						BoadicaMenu.setCurrentCategory( null );
						setScreen( SCREEN_CATEGORIES ); 
					break;
					
					case ENTRY_MAIN_MENU_HELP_HOW_TO_USE:
						setScreen( SCREEN_HELP_CONTROLS );
					break;

					case ENTRY_MAIN_MENU_HELP_COSTS:
						setScreen( SCREEN_HELP_COSTS );
					break;
					
					case ENTRY_MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;
					
					case ENTRY_MAIN_MENU_EXIT:
						exit();
					break;
				} // fim switch ( index )
			break; // fim case SCREEN_MAIN_MENU

			case SCREEN_CATEGORIES:
				switch ( index ) {
					case ENTRY_CATEGORIES_BACK:
						moveRight = false;
						exit();
					break;
				}
			break;

			case SCREEN_LOAD_USER_MAP:
			case SCREEN_LOAD_MAP:{
				
				moveRight = false;
				int returnTo = ( id == SCREEN_LOAD_USER_MAP ? SCREEN_USER_OFFER_DETAILS : SCREEN_OFFER_DETAILS );
				BoadicaData.cancelConnection( returnTo );
				setScreen( returnTo );
			}
			break;

			case SCREEN_LOAD_CATEGORIES:
			case SCREEN_LOAD_OFFERS:

				moveRight = false;
				BoadicaData.cancelConnection( /*( id ==  SCREEN_LOAD_CATEGORIES ) ?  */SCREEN_CATEGORIES_ROOT /*: SCREEN_CATEGORIES*/);
				if ( id == SCREEN_LOAD_CATEGORIES )
					BoadicaData.getInstance().clearCategoriesCascading();
				else {
					BoadicaData.getInstance().clearOffers();
					BoadicaData.getInstance().clearStores();
				}

				setScreen( SCREEN_CATEGORIES_ROOT );
			break;

			case SCREEN_OPTIONS:
				switch ( index ) {
					case ENTRY_OPTIONS_RESULTS_PER_SEARCH:
						pagesPerSearch = ( short ) ( ( pagesPerSearch + PAGES_PER_SEARCH_STEP ) % ( PAGES_PER_SEARCH_MAX + PAGES_PER_SEARCH_STEP ) );
						refreshResultsLabel( menu, index );
					break;

					case ENTRY_OPTIONS_BACK:
						moveRight = false;
						setScreen( SCREEN_CATEGORIES );
					break;
				}
			break;

			case SCREEN_USER_OFFERS:
				final Offer[] userOffers = BoadicaData.getUserOffers();
				OffersScreen.setCurrentOffer( userOffers[ index ] );
				setScreen( SCREEN_USER_OFFER_DETAILS );
			break;

			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_COSTS );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						moveRight = false;
						setScreen( SCREEN_CATEGORIES );
					break;					
				}
			break;
		} // fim switch ( id )		
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
		try {
			for ( byte i = 0; i < menu.getTotalSlots(); ++i ) {
				if ( menu.getDrawable( i ) instanceof MenuLabel ) {
					( ( MenuLabel ) menu.getDrawable( i ) ).setCurrent( i == index );
				} else if ( menu.getDrawable( i ) instanceof PaneLabel ) {
					( ( PaneLabel ) menu.getDrawable( i ) ).setCurrent( i == index );
				}
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
		AppMIDlet.log( e, "a25" );
			e.printStackTrace();
			//#endif
		}
	}


	/**
	 * Define uma soft key a partir de um texto. Equivalente Ã  chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, 0 );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey Ã­ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visÃ­vel. Para o label estar sempre visÃ­vel, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( FONT_TEXT, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
			AppMIDlet.log( e, "a26" );
				e.printStackTrace();
				//#endif
			}
		}
	} // fim do mÃ©todo setSoftKeyLabel( byte, int )
	
	
	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		ScreenManager.getInstance().setSoftKey( softKey, d );
	} // fim do método setSoftKey( byte, Drawable, boolean, int )

	
	protected final ImageFont getFont( int index ) {
		return FONTS[ index ];
	}


	public static final String getCurrencyString( long value ) {
		return getCurrencyString( value, true );
	}


	public static final String getCurrencyString( long value, boolean useCurrency ) {
		final String currency = useCurrency ? getText( TEXT_CURRENCY ) : "";

		if ( value < 100 ) {
			// centavos
			return currency + "0," + value;
		} else {
			String s = String.valueOf( value );
			String res = "";
			for ( int i = s.length() - 1, count = 2; i >= 0; --i ) {
				if ( i >= s.length() - 2 ) {
					if ( i == s.length() - 2 )
						res = "," + s.charAt( i ) + res;
					else
						res = s.charAt( i ) + res;
				} else if ( count > 0 || i == 0 ) {
					--count;
					res = s.charAt( i ) + res;
				} else {
					count = 2;
					res = "." + s.charAt( i ) + res;
				}
			}
			return currency + res;
		}
	}

    
    private static final String getVersion() {
        String version = instance.getAppProperty( "MIDlet-Version" );
        if ( version == null )
            version = "";

        return "<ALN_H>" + getText( TEXT_VERSION ) + version + "\n\n";
    }    
	
	
	public final void write(DataOutputStream output) throws Exception {
		output.writeByte(language);
		output.writeShort( pagesPerSearch );
		output.writeUTF( operatorCode );
	}


	public final void read( DataInputStream input ) throws Exception {
		setLanguage( input.readByte() );
		pagesPerSearch = input.readShort();
		operatorCode = input.readUTF();
		log("operatorCode:"+operatorCode);
	}


	public final void eventPerformed( Event evt ) {
		//#if DEBUG == "true"
		System.out.println( "EVENT: " + evt.eventType + ", " + evt.source );
		AppMIDlet.log( "EVENT: " + evt.eventType + ", " + evt.source );
		//#endif
			
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( evt.source.getId() ) {
					case ID_BUTTON_MAP_CONFIRM:
					case ID_BUTTON_USER_MAP_CONFIRM:
						// TODO adicionar opção para salvar mapa
						moveRight = true;
						setScreen( evt.source.getId() == ID_BUTTON_MAP_CONFIRM ? SCREEN_LOAD_MAP : SCREEN_LOAD_USER_MAP );
					break;

					case ID_BUTTON_BACK:
						if ( BoadicaMenu.getCurrentCategory() != null ){
							windowManager.onBackPressed();
						} else {
							moveRight = false;
							exit();							
						}
					break;

					case ID_BUTTON_LOAD_YES:
						setScreen( SCREEN_LOAD_CATEGORIES );
					break;

					case ID_BUTTON_LOAD_NO:
						setMoveRight( true );
						setScreen( SCREEN_CATEGORIES_ROOT );
					break;

					case ID_BUTTON_EXIT_YES:
						exit();
					break;

					case ID_BUTTON_EXIT_NO:
						try {
							getTabbedPane().setPane( getBackPane( false ), ICON_EXIT );
						} catch( Throwable t ) {
							//#if DEBUG == "true"
								AppMIDlet.log( t, "Volta para pane de exit comum" );
							//#endif
						}
					break;

					case ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_YES:
						callPhoneNumber( numberToCall, true );
					break;

					case ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_NO:
						try {
							final Offer currentOffer = OffersScreen.getCurrentOffer();
							getTabbedPane().setPane( getStorePhonesMenu( currentOffer.getStore() ) , ICON_PHONE );
						} catch( Throwable t ) {
							//#if DEBUG == "true"
								AppMIDlet.log( t, "Volta para pane de telefones" );
							//#endif
						}
					break;
				}
			break;

			case Event.EVT_KEY_PRESSED:
				switch ( evt.source.getId() ) {
					case ID_BUTTON_MAP_CONFIRM:
					case ID_BUTTON_USER_MAP_CONFIRM:
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_LEFT:
							case ScreenManager.KEY_NUM4:
								getTabbedPane().previousPane();
							break;

							case ScreenManager.KEY_RIGHT:
							case ScreenManager.KEY_NUM6:
								getTabbedPane().nextPane();
							break;
						}
					break;

					case ID_BUTTON_LOAD_NO:
					case ID_BUTTON_LOAD_YES:
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_BACK:
								setMoveRight( true );
								setScreen( SCREEN_CATEGORIES_ROOT );
							break;
						}
					break;

					case ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_YES:
						//#if DEBUG == "true"
							System.out.println( "Yes we can button "+ evt.data );
						//#endif
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_LEFT:
							case ScreenManager.KEY_NUM4:
								getTabbedPane().previousPane();
								evt.consume();
							break;

							case ScreenManager.KEY_RIGHT:
							case ScreenManager.KEY_NUM6:
								evt.consume();
							break;
						}
					break;

					case ID_BUTTON_EXIT_AND_EXECUTE_OPERATION_NO:
						//#if DEBUG == "true"
							System.out.println( "NO button " + evt.data );
						//#endif
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_LEFT:
							case ScreenManager.KEY_NUM4:
								evt.consume();
							break;

							case ScreenManager.KEY_RIGHT:
							case ScreenManager.KEY_NUM6:
								getTabbedPane().nextPane();
								evt.consume();
							break;
						}
					break;
				}
			break;

			case Event.EVT_TEXTBOX_LEFT_ON_START:
				getTabbedPane().previousPane();
			break;

			case Event.EVT_TEXTBOX_RIGHT_ON_END:
				getTabbedPane().nextPane();
			break;
		}
	}


}
