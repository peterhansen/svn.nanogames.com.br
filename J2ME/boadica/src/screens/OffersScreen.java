/**
 * OffersScreen.java
 *
 * Created on Dec 4, 2010 9:11:35 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableRect;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import core.BoadicaData;
import core.Constants;
import core.Offer;
import javax.microedition.lcdui.Graphics;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener; 
//#endif
import core.TopBarRequester;

/**
 *
 * @author peter
 */
public final class OffersScreen extends DrawableGroup implements Constants, Updatable, KeyListener, TopBarRequester
//#if TOUCH == "true"
		, PointerListener
//#endif

{
	private static byte ROWS		= 10;
	private static final byte COLUMNS	= 3;


	private static final int TABLE_SIZE_PERCENT = 96;
	private int TABLE_WIDTH;
	private int OFFSET_X;
	private int CELL_HEIGHT;


	private static final byte[] COLUMNS_WIDTH_PERCENT = { 30, 50, 20 };

	private static final byte CURSOR_THICKNESS = 2;

	private static final byte COLUMN_VENDOR	= 0;
	private static final byte COLUMN_MODEL	= 1;
	private static final byte COLUMN_PRICE	= 2;


//	private static final byte COLUMN_INFO	= 3;

	private final Pattern headPattern;

	private final DrawableGroup cursor;

	private static Offer[] offers = new Offer[ 0 ];
	private static short totalPages;

	private final MarqueeLabel[][] cells;
	private final MarqueeLabel[] titles = new MarqueeLabel[ COLUMNS ];

	private byte currentIndex;

	private byte currentPage;

	private final Label pageLabel;
	private final Label nextPageLabel;
	private final Label previousPageLabel;

	private static Offer currentOffer;

	private static OffersScreen instance;

	private final RichLabel noOffersLabel;
	private static boolean iWantTopBar = true;

	//#if TOUCH == "true"
		private boolean draggingCells;
	//#endif

	private final int backIndex;


	public OffersScreen( int backIndex ) throws Exception {
		super( 25 + ( OFFERS_PER_PAGE * COLUMNS ) );


		//#if DEBUG == "true"
			System.out.println( "ScreenManager.SCREEN_HEIGHT " + ScreenManager.SCREEN_HEIGHT );
		//#endif
		if( ScreenManager.SCREEN_HEIGHT < 160 )
			ROWS = 5;
		else
			ROWS = 10;

		cells = new MarqueeLabel[ ROWS ][ COLUMNS ];

		this.backIndex = backIndex;
        headPattern = new Pattern( new DrawableImage( PATH_IMAGES + "head.png" ) );
		insertDrawable( headPattern );

		cursor = new DrawableGroup( 5 );
		insertDrawable( cursor );

		for ( int r = 0; r < ROWS; ++r ) {
			for ( byte c = 0; c < COLUMNS; ++c ) {
				final MarqueeLabel m = new MarqueeLabel( FONT_TEXT, null );
				cells[ r ][ c ] = m;
				m.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
				m.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				insertDrawable( m );
			}
		}

		final int[] TITLES = { TEXT_VENDOR, TEXT_MODEL, TEXT_CURRENCY, TEXT_INFO };
		for ( byte c = 0; c < COLUMNS; ++c ) {
			final MarqueeLabel m = new MarqueeLabel( FONT_TEXT, TITLES[ c ] );
			titles[ c ] = m;
			m.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
			m.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
			insertDrawable( m );
		}

		final Pattern p = new Pattern( 0xfeeac1 );
		p.setSize( 10000, 10000 );
		cursor.insertDrawable( p );
		DrawableRect r = new DrawableRect( 0xa6a6a6 );
		r.setPosition( 1, 1 );
		cursor.insertDrawable( r );

		r = new DrawableRect( 0xffa200 );
		cursor.insertDrawable( r );

		r = new DrawableRect( 0xffa200 );
		r.setPosition( 1, 1 );
		cursor.insertDrawable( r );

		pageLabel = new Label( FONT_TEXT_BOLD );
		insertDrawable( pageLabel );

		nextPageLabel = new Label( FONT_TEXT_BOLD, TEXT_NEXT );
		insertDrawable( nextPageLabel );

		previousPageLabel = new Label( FONT_TEXT_BOLD, TEXT_PREVIOUS );
		insertDrawable( previousPageLabel );

		noOffersLabel = new RichLabel( FONT_TEXT, TEXT_NO_OFFERS_FOUND );
		insertDrawable( noOffersLabel );

		refreshVisibility();

		//#if DEBUG == "true"
//			if ( offers == null || offers.length == 0 ) {
//				offers = new Offer[ 26 ];
//				for ( byte i = 0; i < offers.length; ++i ) {
//					offers[ i ] = Offer.getDummyOffer();
//				}
//			}
		//#endif
	}

	
	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final OffersScreen load( int backIndex ) throws Exception {
		//#if DEBUG == "true"
			System.out.println( "LOAD" );
		//#endif
		unload();
		instance = new OffersScreen( backIndex );

		return instance;
	}


	/**
	 * 
	 */
	public static final void unload() {
		//#if DEBUG == "true"
			System.out.println( "UNLOAD: " + instance );
		//#endif
			
		if ( instance != null ) {
			instance = null;
			offers = new Offer[ 0 ];
			totalPages = 0;
			currentOffer = null;
		}
	}


	/**
	 * 
	 * @return
	 */
	public static final OffersScreen getInstance() {
		return instance;
	}


	/**
	 * 
	 * @param offers
	 * @param page
	 */
	public static final void setOffers( Offer[] offers, int page ) {
		if ( page > 1 ) {
			final Offer[] oldOffers = OffersScreen.offers;
			OffersScreen.offers = new Offer[ oldOffers.length + offers.length ];
			System.arraycopy( oldOffers, 0, OffersScreen.offers, 0, oldOffers.length );
			System.arraycopy( offers, 0, OffersScreen.offers, oldOffers.length, offers.length );

			if ( instance != null )
				instance.setCurrentPage( instance.getCurrentPage() + 1 );
		} else {
			OffersScreen.offers = offers;
		}

		if ( instance != null ) {
			instance.refreshVisibility();
		}
	}


	private final void refreshVisibility() {
		noOffersLabel.setSize( size );
		noOffersLabel.setVisible( OffersScreen.offers == null || OffersScreen.offers.length == 0 );

		final boolean offersVisible = !noOffersLabel.isVisible();
		final int visibleRows = getVisibleRows();
		
		headPattern.setVisible( offersVisible );
		cursor.setVisible( offersVisible );

		for ( int r = 0; r < visibleRows; ++r ) {
			for ( byte c = 0; c < COLUMNS; ++c ) {
				cells[ r ][ c ].setVisible( offersVisible );
			}
		}

		for ( byte c = 0; c < COLUMNS; ++c ) {
			titles[ c ].setVisible( offersVisible );
		}

		refreshPageLabelPosition();
	}


	public static final void setTotalPages( short totalPages ) {
		// TODO calcular o total de paginas com a quantidade de ofertas da ultima paginas,
		// para casos de páginas com menos resultados do que o baixado do servidor,
		// EX.: última página pode não existir
		OffersScreen.totalPages = ( short ) ( (totalPages) * OFFERS_PER_PAGE / ROWS );
	}
	

	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		if( height < 160 )
			ROWS = 5;
		else
			ROWS = 10;

		TABLE_WIDTH = getWidth() * TABLE_SIZE_PERCENT / 100;
		OFFSET_X = ( getWidth() - TABLE_WIDTH ) >> 1;

		headPattern.setSize( TABLE_WIDTH, headPattern.getFill().getHeight() );
		headPattern.setPosition( OFFSET_X, MENU_LABEL_OFFSET_X );

		final int[] CELLS_WIDTH = new int[ COLUMNS ];
		for ( byte i = 0; i < COLUMNS; ++i )
			CELLS_WIDTH[ i ] = TABLE_WIDTH * COLUMNS_WIDTH_PERCENT[ i ] / 100;

		//final int CELL_HEIGHT = cells[ 0 ][ 0 ].getHeight();

		for ( int c = 0, x = OFFSET_X; c < COLUMNS; x += CELLS_WIDTH[ c ], ++c ) {
			titles[ c ].setSize( CELLS_WIDTH[ c ], titles[ c ].getHeight() );
			titles[ c ].setPosition( x, MENU_LABEL_OFFSET_X );
			titles[ c ].setTextOffset( ( CELLS_WIDTH[ c ] - titles[ c ].getFont().getTextWidth( titles[ c ].getText() ) ) >> 1 );
		}
		headPattern.setSize( headPattern.getWidth(), titles[0].getHeight() );

		//#if SCREEN_SIZE == "SMALL"
//# 		CELL_HEIGHT = ( height - ( PANE_HEIGHT_MAGICAL ) );
		//#else 
		CELL_HEIGHT = ( height - ( PANE_HEIGHT_MAGICAL << 1 ) );
		//#endif
		CELL_HEIGHT /=  ( cells.length + 2 );
		//#if DEBUG == "true"
			System.out.println( "height " + height );
		//#endif

		if( cells.length > 0 ) {
			CELL_HEIGHT = NanoMath.min(  CELL_HEIGHT, cells[ 0 ][ 0 ].getFont().getHeight() << 1 );
			if( CELL_HEIGHT < cells[ 0 ][ 0 ].getFont().getHeight() ){
				CELL_HEIGHT = cells[ 0 ][ 0 ].getFont().getHeight();
				iWantTopBar = false;
			} else {
				if( ( CELL_HEIGHT - cells[ 0 ][ 0 ].getFont().getHeight() ) * ( cells.length + 2 ) > TITLE_BAR_HEIGHT ) {
					iWantTopBar = true;
				}
			}

			for ( int r = 0, y = MENU_LABEL_OFFSET_X + titles[ 0 ].getHeight(); r < ROWS; ++r ) {
				for ( int c = 0, x = OFFSET_X; c < COLUMNS; x += CELLS_WIDTH[ c ], ++c ) {
					cells[ r ][ c ].setSize( CELLS_WIDTH[ c ], cells[ r ][ c ].getFont().getHeight() ); //cells[ r ][ c ].getHeight() );
					cells[ r ][ c ].setPosition( x, y + ( ( CELL_HEIGHT - cells[ r ][ c ].getHeight() ) >> 1 ) );

				}
				y += CELL_HEIGHT;
			}
		}

		cursor.setSize( TABLE_WIDTH, CELL_HEIGHT + ( CURSOR_THICKNESS << 1 ) );
		DrawableRect r = ( DrawableRect ) cursor.getDrawable( 1 );
		r.setSize( cursor.getSize().sub( 1, 1 ) );
		r = ( DrawableRect ) cursor.getDrawable( 2 );
		r.setSize( cursor.getSize() );
		r = ( DrawableRect ) cursor.getDrawable( 3 );
		r.setSize( cursor.getSize().sub( 2, 2 ) );

		noOffersLabel.setSize( width, height );
		
		setCurrentPage( currentPage );
		refreshPageLabelPosition();
		refreshVisibility();
	}


	/**
	 * 
	 * @return
	 */
	public final int getCurrentPage() {
		return currentPage;
	}


	/**
	 * 
	 * @return
	 */
	public final int getPageIndexToLoad() {
		return currentPage + 2;
	}


	/**
	 * 
	 * @return
	 */
	private final int getVisibleRows() {
		return NanoMath.clamp( offers.length - currentPage * ROWS, 0, ROWS );
	}


	/**
	 * 
	 */
	private final void refreshCells() {
		final int START = ( currentPage * ROWS );

		for ( int r = 0; r < ROWS; ++r) {
			if ( ( START + r ) < offers.length ) {
				final Offer offer = offers[ START + r ];
				cells[ r ][ COLUMN_VENDOR ].setText( offer.getVendor(), false );
				cells[ r ][ COLUMN_MODEL ].setText( offer.getModel(), false );
				cells[ r ][ COLUMN_PRICE ].setText( GameMIDlet.getCurrencyString( offer.getPrice(), false ), false );

				centerCellsText( r );
				for ( byte c = 0; c < COLUMNS; ++c ) {
					cells[ r ][ c ].setVisible( true );
				}
			} else {
				for ( byte c = 0; c < COLUMNS; ++c ) {
					cells[ r ][ c ].setVisible( false );
				}
			}
		}
	}


	public final void update( int delta ) {
		// só atualiza a célula escolhida
		for ( byte c = 0; c < COLUMNS; ++c )
			cells[ currentIndex ][ c ].update( delta );

		for ( byte c = 0; c < COLUMNS; ++c )
			titles[ c ].update( delta );
	}


	protected final void paint( Graphics g ) {
		super.paint( g );

		if ( !noOffersLabel.isVisible() ) {
			g.setColor( 0xf7c968 );

			//final int OFFSET_X = getWidth() * 5 / 100;

			final int[] CELLS_WIDTH = new int[ COLUMNS ];
			for ( byte i = 0; i < COLUMNS; ++i )
				CELLS_WIDTH[ i ] = TABLE_WIDTH * COLUMNS_WIDTH_PERCENT[ i ] / 100;

			final int visibleRows = getVisibleRows();

			for ( int i = 0; i < titles.length; ++i ) {
				g.drawRect( translate.x + titles[i].getPosX() - 1, translate.y + titles[i].getPosY() - 1, titles[i].getWidth(), titles[i].getHeight() );
			}

			for ( int r = 0, y = translate.y + MENU_LABEL_OFFSET_X + titles[ 0 ].getHeight(); r < visibleRows; ++r, y += CELL_HEIGHT ) {
				g.drawRect( translate.x + OFFSET_X, y, TABLE_WIDTH, CELL_HEIGHT );
			}

			final int TOTAL_HEIGHT = CELL_HEIGHT * ( visibleRows );
			for ( int c = 0, x = translate.x + OFFSET_X; c < ( COLUMNS ); x += CELLS_WIDTH[ c ], ++c ) {
				g.drawRect( x - 1, translate.y + MENU_LABEL_OFFSET_X + titles[ 0 ].getHeight() - 1, 0, TOTAL_HEIGHT );
			}
		}
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				setCurrentPage( currentPage - 1 );
			break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				setCurrentPage( currentPage + 1 );
			break;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setCurrentIndex( currentIndex - 1 );
			break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setCurrentIndex( currentIndex + 1 );
			break;

			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				GameMIDlet.setMoveRight( true );
				currentOffer = getSelectedOffer();
//				if ( BoadicaData.getStore( currentOffer.getStoreCode() ) == null ) {
//					// precisa buscar informações da loja antes de ver detalhes
//				} else {
					GameMIDlet.setScreen( SCREEN_OFFER_DETAILS );
//				}
			break;

			case ScreenManager.KEY_BACK:
//			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				GameMIDlet.setMoveRight( false );
				if ( BoadicaMenu.getCurrentCategory() != null )
					BoadicaMenu.setCurrentCategory( BoadicaMenu.getCurrentCategory().getParent() );
				
				GameMIDlet.setScreen( backIndex );
			break;
		}
	}


	public static final Offer getCurrentOffer() {
		return currentOffer;
	}


	public static final void setCurrentOffer( Offer offer ) {
		currentOffer = offer;
	}


	private final Offer getSelectedOffer() {
		return offers[ currentPage * ROWS + currentIndex ];
	}


	public final void keyReleased( int key ) {
	}


	/**
	 * 
	 * @param index
	 */
	private final void setCurrentIndex( int index ) {
		final int visibleRows = getVisibleRows();
		index = Math.min( visibleRows, index );

		centerCellsText( currentIndex );
		
		currentIndex = ( byte ) ( ( visibleRows + index ) % visibleRows );
		cursor.setPosition( OFFSET_X, cells[ currentIndex ][ 0 ].getPosY() - ( ( CURSOR_THICKNESS + CELL_HEIGHT - cells[ currentIndex ][ 0 ].getHeight() ) >> 1 ) );
	}


	/**
	 * 
	 * @param page
	 */
	private final void setCurrentPage( int page ) {
		page = NanoMath.clamp( page, 0, getTotalPages() - 1 );

		if ( page < getLoadedPages() ) {
			currentPage = ( byte ) page;
			pageLabel.setText( ( currentPage + 1 ) + "/" + getTotalPages() );
			refreshPageLabelPosition();
			refreshCells();
			setCurrentIndex( currentIndex );
		} else if ( offers.length > 0 ) {
			// página ainda não foi carregada; conecta ao servidor
			GameMIDlet.setMoveRight( true );
			GameMIDlet.setScreen( SCREEN_LOAD_OFFERS );
		}
	}


	/**
	 * 
	 */
	private final void refreshPageLabelPosition() {
		int height = 0;
		final int visibleRows = getVisibleRows();

		if (visibleRows > 0)
			height =  cells[ visibleRows - 1 ][ 0 ].getPosY() + CELL_HEIGHT;

		pageLabel.setPosition( ( getWidth() - pageLabel.getWidth() ) >> 1, height );

		previousPageLabel.setPosition( cells[ 0 ][ 0 ].getPosX(), pageLabel.getPosY() );
		previousPageLabel.setVisible( currentPage > 0 );

		nextPageLabel.setPosition( ( TABLE_WIDTH ) - nextPageLabel.getWidth(), previousPageLabel.getPosY() );
		nextPageLabel.setVisible( currentPage < getTotalPages() - 1 );
	}


	/**
	 * 
	 * @return
	 */
	private final int getTotalPages() {
		return Math.max( totalPages, ( offers.length + ROWS - 1 ) / ROWS );
	}


	/**
	 * 
	 * @return
	 */
	private final short getLoadedPages() {
		return ( short ) ( ( offers.length + ROWS - 1 ) / ROWS );
	}


	/**
	 * 
	 * @param r
	 */
	private final void centerCellsText( int r ) {
		for ( byte c = 0; c < COLUMNS; ++c ) {
			final int TEXT_WIDTH = cells[ r ][ c ].getFont().getTextWidth( cells[ r ][ c ].getText() );
			final int CELL_WIDTH = cells[ r ][ c ].getWidth();
			if ( TEXT_WIDTH < CELL_WIDTH )
				cells[ r ][ c ].setTextOffset( ( CELL_WIDTH - TEXT_WIDTH ) >> 1 );
			else
				cells[ r ][ c ].setTextOffset( 0 );
		}
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			final int clickedIndex = getRowAt( y );
			if ( draggingCells && clickedIndex >= 0 )
				setCurrentIndex( clickedIndex );
		}


		public final void onPointerPressed( int x, int y ) {
			if ( previousPageLabel.contains( x, y ) )
				keyPressed( ScreenManager.KEY_LEFT );
			else if ( nextPageLabel.contains( x, y ) )
				keyPressed( ScreenManager.KEY_RIGHT );
			else {
				final int clickedIndex = getRowAt( y );
				if ( clickedIndex >= 0 ) {
					draggingCells = true;

					if ( clickedIndex != currentIndex )
						setCurrentIndex( clickedIndex );
					else
						keyPressed( ScreenManager.KEY_FIRE );
				}
			}
		}


		public final void onPointerReleased( int x, int y ) {
			draggingCells = false;
		}


		private final int getRowAt( int y ) {
			final int index = ( y - ( cells[ 0 ][ 0 ].getPosY() - ( ( CELL_HEIGHT - cells[ 0 ][ 0 ].getHeight() ) >> 1 ) ) ) / CELL_HEIGHT; //  cells[ 0 ][ 0 ].getHeight();
			if ( index < 0 || index >= getVisibleRows() )
				return -1;

			return index;
		}

	//#endif


	public boolean TopBarIsRequested() {
		return iWantTopBar;
	}
}
