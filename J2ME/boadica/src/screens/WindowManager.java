/**
 * WindowManager.java
 *
 * Created on Dec 4, 2010 1:20:17 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.TabbedPane;
import core.TitleLabel;
import java.util.Enumeration;
import java.util.Vector;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import core.TopBarRequester;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public final class WindowManager extends UpdatableGroup implements Constants, KeyListener, ScreenListener
	//#if TOUCH == "true"
		, PointerListener
	//#endif
{

	/***/
	private static final short TIME_TRANSITION = 500;

	private Drawable currentScreen;

	private Drawable nextScreen;

	private final TitleLabel title;

	private boolean transition;

	private short accTime;

	private final BezierCurve bezierCurve = new BezierCurve();

	private final Mutex mutex = new Mutex();

	private boolean moveRight;

	private final TabbedPane tabbedPane;

    /** Pattern usado para obscurecer a área fora de foco atrás da TabbedPane. */
    private final Pattern focusShadow;

	private KeyListener keyListener;

	//#if TOUCH == "true"
		private PointerListener dragged;
	//#endif

	private final Rectangle screenViewport = new Rectangle();


	public void draw( Graphics g ) {
		try {
			super.draw( g );
		} catch ( Throwable th ) {
			AppMIDlet.log( th, "WindowManager::draw" );
		}
	}

	protected void paint( Graphics g ) {
		try {
			super.paint( g );
		} catch ( Throwable th ) {
			AppMIDlet.log( th, "WindowManager::paint" );
		}
	} // fim do método paint( Graphics )


	public final Rectangle getScreenViewport() {
		return screenViewport;
	}


	public final Drawable getNextScreen(){
		return currentScreen;
	}

	public WindowManager() throws Exception {
		super( 10 );

		//#if SCREEN_SIZE != "SMALL"
			title = new TitleLabel();
			insertDrawable( title );
		//#else
//# 			title = null;
		//#endif

        focusShadow = new Pattern( new DrawableImage( Constants.PATH_IMAGES + "splash/transparency.png" ) );
        focusShadow.setVisible( false );
        insertDrawable( focusShadow );

		tabbedPane = new TabbedPane( this, focusShadow );
		insertDrawable( tabbedPane );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
        focusShadow.setPosition( 0, 0 );
		tabbedPane.setPosition( 0, getHeight() );
	}


	public final void setTitleVisibility( boolean visibility ) {
		//#if SCREEN_SIZE != "SMALL"
			if( title.isVisible() != visibility ) {
				title.setVisible( visibility );
				if ( currentScreen != null )
					currentScreen.setSize( getWidth(), getHeight() - getTitleHeight() + tabbedPane.getPaneHeight() );
				if ( nextScreen != null )
					nextScreen.setSize( getWidth(), getHeight() - getTitleHeight() + tabbedPane.getPaneHeight() );
				refresh();
				refreshViewport();
				if ( nextScreen != null )
					nextScreen.setViewport( screenViewport );
				if ( currentScreen != null )
					currentScreen.setViewport( screenViewport );
			}
		//#endif
	}


	public final void setScreen( Drawable screen, boolean moveRight ) {
		//#if DEBUG == "true"
			GameMIDlet.log( "WindowManager.setScreen( " + screen + ", " + moveRight + " )" );
		//#endif

		if ( transition )
			switchScreens();

		transition = true;
		this.moveRight = moveRight;

		nextScreen = screen;

		if ( nextScreen != null ) {
			nextScreen.setViewport( screenViewport );
			insertDrawable( nextScreen, 0 );
			nextScreen.setSize( getWidth(), getHeight() - ( getTitleHeight() ) );
			if( nextScreen instanceof TopBarRequester ) {
				setTitleVisibility( ( (TopBarRequester)nextScreen ).TopBarIsRequested() );
			} else {
				setTitleVisibility( true );
			}
		}

        tabbedPane.hide();
		if ( nextScreen instanceof KeyListener )
			keyListener = ( KeyListener ) nextScreen;
		else
			keyListener = null;

		refresh();
	}


	public final void setPaneIcons( Vector entries, Vector tabs, boolean back ) throws Exception {
		tabbedPane.removeAllPanes();
		
		for ( Enumeration e = entries.elements(), t = tabs.elements(); e.hasMoreElements(); ) {
			tabbedPane.addPane( ( TitledPane ) e.nextElement(), ( ( Integer ) t.nextElement() ).intValue(), -1, false );
		}
		tabbedPane.addPane( GameMIDlet.getBackPane( back ), back ? ICON_BACK : ICON_EXIT, -1, false );

		tabbedPane.refreshPanes();

		if ( entries.size() > 0 )
            tabbedPane.hide();
	}


	private final void switchScreens() {
		switchScreens( true );
	}


	private final void switchScreens( final boolean useMutex ) {
		if ( currentScreen != null ) {
			removeDrawable( currentScreen );
			currentScreen = null;
		}
		
		currentScreen = nextScreen;
		nextScreen = null;

		accTime = 0;
		transition = false;

		refresh();
		refreshViewport();
	}


	public final TabbedPane getTabbedPane() {
		return tabbedPane;
	}


	public final void setTitle( int titleIndex ) {
		setTitle( GameMIDlet.getText( titleIndex ) );
	}


	public final void setTitle( String title ) {
		if ( this.title != null )
			this.title.setText( title, 0 );
	}


	private final void refreshViewport() {
		int dy = getTitleHeight(); //  title != null && title.isVisible() ) ? title.getHeight() : 0;

		screenViewport.set( 0, dy, getWidth(), tabbedPane.getPanePosY() - dy );
	}


	public final void update( int delta ) {
		super.update( delta );
		if ( transition ) {
			accTime += delta;
			refresh();

			if ( accTime >= TIME_TRANSITION ) {
				switchScreens( false );
			}
		}

		refreshViewport();
	}


	private final void refresh() {
		final int FP_PROGRESS = NanoMath.divInt( accTime, TIME_TRANSITION );
		final Point p = new Point();
		int dy = getTitleHeight(); //title != null && title.isVisible() ) ? title.getHeight() : 0;
		//#if DEBUG == "true"
			System.out.println( "dy = " + dy );
		//#endif
		bezierCurve.getPointAtFixed( p, FP_PROGRESS );
		if ( !moveRight ) {
			final int temp = p.x;
			p.x = getWidth() - p.y;
			p.y = p.x - getWidth();
		}
		
		if ( currentScreen != null )
			currentScreen.setPosition( p.x, dy );

		if ( nextScreen != null ) {
			nextScreen.setPosition( p.y, dy );
		}
	}

	public void onBackPressed() {
		if (currentScreen instanceof KeyListener)
			( ( KeyListener ) currentScreen ).keyPressed( ScreenManager.KEY_SOFT_RIGHT );
	}

	public final void keyPressed( int key ) {
		if ( !transition ) {
			switch ( key ) {
				case ScreenManager.KEY_POUND:
				case ScreenManager.KEY_STAR:
				case ScreenManager.KEY_SOFT_LEFT:
				case ScreenManager.KEY_SOFT_MID:
					if ( keyListener == tabbedPane ) {
                        tabbedPane.hide();
						if ( currentScreen instanceof KeyListener )
							keyListener = ( KeyListener ) currentScreen;
						else
							keyListener = null;
					} else {
                        tabbedPane.show();
						keyListener = tabbedPane;
					}
				break;

				default:
					if ( keyListener != null )
						keyListener.keyPressed( key );
			}
		}
	}


	public final void keyReleased( int key ) {
		if ( !transition && keyListener != null )
			keyListener.keyReleased( key );
	}


	public final void hideNotify( boolean deviceEvent ) {
		if ( !transition && currentScreen instanceof ScreenListener )
			( ( ScreenListener ) currentScreen ).hideNotify( deviceEvent );
	}


	public final void showNotify( boolean deviceEvent ) {
		if ( !transition && currentScreen instanceof ScreenListener )
			( ( ScreenListener ) currentScreen ).showNotify( deviceEvent );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		int dy = getTitleHeight(); // title != null  && title.isVisible() ) ? title.getHeight() : 0;

        focusShadow.setSize( width, height /*- TABBED_MANAGER_HEIGHT*/ );
		tabbedPane.setSize( width, TABBED_MANAGER_HEIGHT );

		if ( currentScreen != null ) {
			currentScreen.setSize( width, height - dy );
			if( currentScreen instanceof TopBarRequester ) {
				setTitleVisibility( ( (TopBarRequester)currentScreen ).TopBarIsRequested() );
			} else {
				setTitleVisibility( true );
			}
		}

		if ( nextScreen != null )
			nextScreen.setSize( width, height - dy );

		bezierCurve.origin.set( 0, width );
		bezierCurve.destiny.set( -width, 0 );
		
		bezierCurve.control1.set( bezierCurve.origin );
		bezierCurve.control2.set( -width, 0 );
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			setSize( width, height );
		}
	}


	public final int getTitleHeight() {
		return ( title != null  && title.isVisible() ) ? title.getHeight() : 0;
	}

	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			if ( !transition && dragged != null )
				dragged.onPointerDragged( x, ( dragged instanceof Component ? y : y - getTitleHeight() ) );
		}


		public final void onPointerPressed( int x, int y ) {
			if ( !transition ) {
				if ( tabbedPane.contains( x, y ) ) {
					dragged = tabbedPane;
					keyListener = tabbedPane;
                    focusShadow.setVisible( true );
					tabbedPane.onPointerPressed( x, y - tabbedPane.getPosY() );
					dragged.onPointerReleased( x, y - tabbedPane.getPosY() );
				} else if ( currentScreen instanceof PointerListener && currentScreen.contains( x, y ) ) {
					if ( tabbedPane.getState() != TabbedPane.STATE_VISIBLE ) {
						dragged = ( PointerListener ) currentScreen;
						if ( dragged instanceof Component )
							dragged.onPointerPressed( x, y );
						else
							dragged.onPointerPressed( x, y - getTitleHeight() );
					}
                    tabbedPane.hide();
				} else {
					dragged = null;
                    tabbedPane.hide();
				}
			}
		}


		public final void onPointerReleased( int x, int y ) {
			if ( !transition && dragged != null ) {
				if ( dragged == tabbedPane )
					dragged.onPointerReleased( x, y - tabbedPane.getPosY() );
				else if ( dragged instanceof Component )
					dragged.onPointerReleased( x, y );
				else
					dragged.onPointerReleased( x, y - getTitleHeight() );
			}

			dragged = null;
		}

	//#endif

}
