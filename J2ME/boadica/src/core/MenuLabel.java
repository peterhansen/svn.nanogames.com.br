/**
 * MenuLabel.java
 * 
 * Created on Mar 2, 2009, 3:14:44 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import screens.GameMIDlet;

/**
 *
 * @author Peter
 */
public final class MenuLabel extends UpdatableGroup implements Constants, SpriteListener {

	private static final byte SEQUENCE_UNSELECTED	= 0;
	private static final byte SEQUENCE_SELECTING	= 1;
	private static final byte SEQUENCE_SELECTED		= 2;
	private static final byte SEQUENCE_UNSELECTING	= 3;

	private static final byte ID_BAR				= 0;

	private static Sprite BAR;
	private static Sprite BAR_FILL;

	private final MarqueeLabel label;

	private boolean current;

	private final Sprite barLeft;
	private final Sprite barRight;

	private final Pattern barPattern;

	private static int PARENT_Y;

	private boolean selectable = true;

	private final DrawableImage categoryIcon;

	private static DrawableImage[] ICONS;

	private static byte ICON_BIGGEST_WIDTH;


	public MenuLabel( int textIndex ) throws Exception {
		this( GameMIDlet.getText( textIndex ) );
	}


	public MenuLabel( String text ) throws Exception {
		this( text, -1 );
	}

	
	public MenuLabel( String text, int iconIndex ) throws Exception {
		super( 7 );

		if ( BAR == null ) {
			BAR = new Sprite( PATH_IMAGES + "menubox" );
			BAR_FILL = new Sprite( PATH_IMAGES + "menubox_2" );
		}

		barLeft = new Sprite( BAR );
		barLeft.setListener( this, ID_BAR );
		insertDrawable( barLeft );

		barRight = new Sprite( BAR );
		barRight.mirror( TRANS_MIRROR_H );
		insertDrawable( barRight );

		barPattern = new Pattern( new Sprite( BAR_FILL ) );
		insertDrawable( barPattern );

		if ( iconIndex >= 0 ) {
			DrawableImage temp = null;
			try {
				//#if SCREEN_SIZE != "SMALL"
					if (ICONS == null) {
						ICONS = new DrawableImage[9];
						for (byte i = 0; i < ICONS.length; ++i) {
							ICONS[i] = new DrawableImage(PATH_ICONS + i + ".png");
							ICON_BIGGEST_WIDTH = (byte) Math.max(ICON_BIGGEST_WIDTH, ICONS[i].getWidth());
						}
					}

					temp = new DrawableImage(ICONS[iconIndex]);
					insertDrawable(temp);
				//#endif
			} catch ( Exception e ) {
				AppMIDlet.log( e, "a11" );
			}
			categoryIcon = temp;
		} else {
			categoryIcon = null;
		}

		label = new MarqueeLabel( FONT_TEXT_BOLD, text );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		insertDrawable( label );

		setSize( ScreenManager.SCREEN_WIDTH, barLeft.getHeight() );

		setText( text );
		setCurrent( false );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		barPattern.setSize( getWidth() - ( barLeft.getWidth() << 1 ), getHeight() );
		barPattern.setPosition( barLeft.getWidth(), 0 );
		barRight.setPosition( getWidth() - barRight.getWidth(), 0 );
		barPattern.defineReferencePixel( ANCHOR_CENTER );

		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		final int LABEL_X;
		if ( categoryIcon != null ) {
			categoryIcon.setPosition( MENU_LABEL_OFFSET_X + MENU_ICON_SPACING + ( ( ICON_BIGGEST_WIDTH - categoryIcon.getWidth() ) >> 1 ), ( getHeight() - categoryIcon.getHeight() ) >> 1 );
			LABEL_X = categoryIcon.getPosX() + ICON_BIGGEST_WIDTH + MENU_ICON_SPACING;
		} else {
			LABEL_X = ( MENU_LABEL_OFFSET_X << 1 );
		}

		label.setPosition( LABEL_X, ( barPattern.getHeight() - label.getHeight() ) >> 1 );
		label.setSize( getWidth() - ( MENU_LABEL_OFFSET_X ) - LABEL_X, label.getHeight() );
	}


	public static final void unload() {
		//#if DEBUG == "true"
			System.gc();
			final long memory = Runtime.getRuntime().freeMemory();
		//#endif

		BAR = null;

		//#if DEBUG == "true"
			System.gc();
			System.out.println( "MenuLabel.unload -> memória liberada: " + ( Runtime.getRuntime().freeMemory() - memory ) + " bytes." );
		//#endif
	}
	

	public final void setSelectable( boolean s ) {
		selectable = s;

		if ( !selectable ) {
			setBarSequence( SEQUENCE_SELECTED );
			refreshLabel();
		}
	}


	public final void setCurrent( boolean c ) {
		if ( selectable ) {
			this.current = c;
			switch ( barLeft.getSequenceIndex() ) {
				case SEQUENCE_SELECTED:
				case SEQUENCE_SELECTING:
					if ( !c ) {
						setBarSequence( SEQUENCE_UNSELECTING );
					}
				break;

				default:
					if ( c ) {
						setBarSequence( SEQUENCE_SELECTING );
					}
			}
			refreshLabel();
		}
	}


	private final void refreshLabel() {
		label.setViewport( new Rectangle( 0, 0, Short.MAX_VALUE, Short.MAX_VALUE ) );
		label.setScrollFrequency( ( current || !selectable ) ? MarqueeLabel.SCROLL_FREQ_IF_BIGGER : MarqueeLabel.SCROLL_FREQ_NONE );
		label.setTextOffset( 0 );

		if ( categoryIcon != null )
			categoryIcon.setViewport( label.getViewport() );
	}


	public final void setText( int textIndex ) {
		setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setText( String text ) {
		label.setText( text, false );
		refreshLabel();
	}


	public void update( int delta ) {
		super.update( delta );

		switch ( barLeft.getSequenceIndex() ) {
			case SEQUENCE_SELECTING:
			case SEQUENCE_UNSELECTING:
				final int percent;
				switch ( barLeft.getCurrFrameIndex() ) {
					case 0:
					case 12:
					default:
						percent = 100;
					break;

					case 1:
					case 11:
						percent = 80;
					break;

					case 2:
					case 10:
						percent = 60;
					break;

					case 3:
					case 9:
						percent = 40;
					break;

					case 4:
					case 8:
						percent = 20;
					break;

					case 5:
					case 6:
					case 7:
						percent = 0;
					break;
				}
				final Rectangle v = label.getViewport();

				if ( v != null ) {
					final int height = percent * label.getHeight() / 100;
					label.getViewport().height = height;
					label.getViewport().y = PARENT_Y + getPosY() + label.getPosY() + ( label.getHeight() >> 1 ) - ( height >> 1 );
				}
			break;
		}
	}


	public static final void setParentY( int y ) {
		PARENT_Y = y;
	}


	private final void setBarSequence( int sequence ) {
		barLeft.setSequence( sequence );
		barRight.setSequence( barLeft.getSequenceIndex() );
		( ( Sprite ) barPattern.getFill() ).setSequence( barLeft.getSequenceIndex() );
		
		//#if SCREEN_SIZE == "SMALL" || SCREEN_SIZE == "MEDIUM"
//# 			barRight.setSequence( sequence );
		//#endif
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( id ) {
			case ID_BAR:
				switch ( sequence ) {
					case SEQUENCE_SELECTING:
						setBarSequence( SEQUENCE_SELECTED );
						label.setViewport( null );
					break;

					case SEQUENCE_UNSELECTING:
						setBarSequence( SEQUENCE_UNSELECTED );
						label.setViewport( null );
					break;
				}
			break;
		}

		if ( categoryIcon != null )
			categoryIcon.setViewport( label.getViewport() );
	}


	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}



}
