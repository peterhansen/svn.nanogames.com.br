/**
 * BoadicaData.java
 *
 * Created on Jan 20, 2011 11:44:51 AM
 *
 */

package core;

import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Image;
import screens.GameMIDlet;
import screens.LoadListener;
import screens.LoadScreen;
import screens.MapScreen;
import screens.OffersScreen;

/**
 *
 * @author peter
 */
public final class BoadicaData implements Constants, ConnectionListener {

	//#if DEBUG == "true"
//		public static final String BOADICA_URL = "http://localhost:3000/boadica";
//		public static final String BOADICA_URL = "http://staging.nanogames.com.br/boadica";
		public static final String BOADICA_URL = "http://online.nanogames.com.br/boadica";
	//#else
//# 		public static final String BOADICA_URL = "http://online.nanogames.com.br/boadica";
	//#endif

	private static final String URL_GOOGLE_MAPS = "http://maps.google.com/maps/api/staticmap";

	/***/
	private static final int FP_PROGRESS_DATA_MIN = NanoMath.divInt( 40, 100 );
	private final static int CATEGORIES_PER_SLEEP = 100;

	private static final short KILOBYTE = 1 << 10;
	private static final int MEGABYTE = KILOBYTE << 10;

	private static final short HALF_KILOBYTE = KILOBYTE >> 1;

	private Offer[] offers = new Offer[ 0 ];

	private Offer[] userOffers = new Offer[ 0 ];

	private Category[] categories = new Category[ 0 ];

	private long categoriesTimestamp;

	private Store[] stores = new Store[ 0 ];

	private static BoadicaData instance;

	private LoadScreen listener;

	private int nextScreen;

	private int backScreen = -1;

	private static int activeConnection = -1;

	/** Total de bytes a baixar. */
	private int contentLengthTotal;

	/** bytes baixados até o momento. */
	private int contentLengthRead;

	private int fp_minProgress;


	private BoadicaData() {
	}


	public static final BoadicaData getInstance() {
		return instance;
	}


	public static final void load() {
		if ( instance == null ) {
			try {
				instance = new BoadicaData();

				GameMIDlet.log( "carregando categorias" );
				loadCategories();
				GameMIDlet.log( "carregando lojas" );
				loadStores();
				GameMIDlet.log( "carregando ofertas" );
				loadOffers( false );
				GameMIDlet.log( "carregando ofertas do usuário" );
				loadOffers( true );
				GameMIDlet.log( "final do carregamento" );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
					AppMIDlet.log( e, "a1" );
				//#endif

				instance.categoriesTimestamp = 0;
			}
		}
	}


	public static final void unload() {
		instance = null;
	}


	public static final void saveCategories( final Category[] categories ) throws Exception {
		final CategoriesManager manager = new CategoriesManager();
		instance.categories = categories;

		//#if DEBUG == "true"
			System.out.println( "CATEGORIAS: " );
			for ( short i = 0; i < categories.length; ++i )
				System.out.print( categories[ i ].getId() + ", " );
		//#endif

		manager.categories = categories;
		if ( instance != null ) {
			manager.listener = instance.listener;
			manager.timestamp = instance.categoriesTimestamp;
		}

		AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_CATEGORIES, manager );
	}


	public static final void loadCategories() throws Exception {
		final CategoriesManager manager = new CategoriesManager();
		AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_CATEGORIES, manager );
		instance.categories = manager.categories;
		instance.categoriesTimestamp = manager.timestamp;
	}


//	private static final void quickSort( Category[] categories, int low, int n ) {
//		int lo = low;
//		int hi = n;
//		if ( lo >= n ) {
//			return;
//		}
//		int mid = categories[ ( lo + hi ) >> 1 ].getId();
//		while ( lo < hi ) {
//			while ( lo < hi && categories[lo].getId() < mid ) {
//				lo++;
//			}
//			while ( lo < hi && categories[hi].getId() > mid ) {
//				hi--;
//			}
//			if ( lo < hi ) {
//				Category T = categories[lo];
//				categories[lo] = categories[hi];
//				categories[hi] = T;
//			}
//		}
//		if ( hi < lo ) {
//			int T = hi;
//			hi = lo;
//			lo = T;
//		}
//		quickSort( categories, low, lo );
//		quickSort( categories, lo == low ? lo + 1 : lo, n );
//	}


	/**
	 *
	 * @param s
	 */
	public static final void loadMap( Store s, LoadScreen listener, final int nextScreen ) {
		final StringBuffer buffer = new StringBuffer( URL_GOOGLE_MAPS );
		buffer.append( "?center=" );
		buffer.append( s.getLatitude() );
		buffer.append( ',' );
		buffer.append( s.getLongitude() );
		buffer.append( "&size=" );
		buffer.append( Math.min( GameMIDlet.isLowMemory() ? ScreenManager.SCREEN_WIDTH : ( ScreenManager.SCREEN_WIDTH * 3 / 2 ), 640 ) );
		buffer.append( 'x' );
		buffer.append( Math.min( GameMIDlet.isLowMemory() ? ScreenManager.SCREEN_HEIGHT : ( ScreenManager.SCREEN_HEIGHT * 3 / 2 ), 640 ) );
		buffer.append( "&format=png&sensor=true&zoom=17&markers=color:green|label:!|" );
		buffer.append( s.getLatitude() );
		buffer.append( ',' );
		buffer.append( s.getLongitude() );
		buffer.append( '|' );

		instance.listener = listener;
		instance.nextScreen = nextScreen;
		activeConnection =	NanoConnection.get( buffer.toString(), new ConnectionListener() {

				public final void processData( int id, byte[] data ) {
					if ( activeConnection < 0 ) {
						final int n = ( instance.nextScreen == SCREEN_USER_MAP ) ? SCREEN_USER_OFFERS : SCREEN_OFFER_DETAILS;
						instance.nextScreen = n;
						GameMIDlet.setScreen( n );
						return;
					}

					final Image map = Image.createImage( data, 0, data.length );

					if ( instance.listener != null  && activeConnection >= 0 ) {
						final LoadScreen l = instance.listener;
						final int n = nextScreen;
						instance.listener = null;
						instance.nextScreen = -1;

						l.setActive( false );
						MapScreen.setMap( map );
						GameMIDlet.setScreen( n );
					}
				}


				public final void onInfo( int id, int infoIndex, Object extraData ) {
					instance.onInfo( id, infoIndex, extraData );
				}


				public final void onError( int id, int errorIndex, Object extraData ) {
					instance.onError( id, errorIndex, extraData );
				}

			} );

	}


	/**
	 *
	 * @param offer
	 * @throws java.lang.Exception
	 */
	public static final void addUserOffer( Offer offer ) throws Exception {
		offer.setTimestamp( System.currentTimeMillis() );

		final Offer[] newUserOffers = new Offer[ instance.userOffers.length + 1 ];
		System.arraycopy( instance.userOffers, 0, newUserOffers, 0, instance.userOffers.length );
		newUserOffers[ instance.userOffers.length ] = offer;

		final OffersManager manager = new OffersManager();
		manager.offers = newUserOffers;
		manager.useTimestamp = true;
		
		instance.userOffers = newUserOffers;

		AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_USER_OFFERS, manager );

		//#if DEBUG == "true"
			System.out.println( "USER OFFER SAVED: " + offer );
		//#endif
	}


	/**
	 *
	 * @param offer
	 * @throws java.lang.Exception
	 */
	public static final void removeUserOffer( Offer offer ) throws Exception {
		final int index = getUserOfferIndex( offer );
		if ( index >= 0 ) {
			for ( int i = index; i < instance.userOffers.length - 1; ++i ) {
				instance.userOffers[ i ] = instance.userOffers[ i + 1 ];
			}

			final Offer[] newUserOffers = new Offer[ instance.userOffers.length - 1 ];
			System.arraycopy( instance.userOffers, 0, newUserOffers, 0, newUserOffers.length );

			final OffersManager manager = new OffersManager();
			manager.offers = newUserOffers;
			manager.useTimestamp = true;
			instance.userOffers = newUserOffers;

			AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_USER_OFFERS, manager );

			//#if DEBUG == "true"
				System.out.println( "USER OFFER REMOVED: " + offer );
			//#endif
		}
	}


	/**
	 *
	 * @param offers
	 * @param userOffers
	 * @throws java.lang.Exception
	 */
	public static final void saveOffers( Offer[] offers, boolean userOffers ) throws Exception {
		final OffersManager manager = new OffersManager();
		manager.offers = offers;
		manager.useTimestamp = userOffers;

		if ( userOffers )
			instance.userOffers = manager.offers;
		else
			instance.offers = manager.offers;

		AppMIDlet.saveData( DATABASE_NAME, userOffers ? DATABASE_SLOT_USER_OFFERS : DATABASE_SLOT_OFFERS, manager );
	}


	private static final void loadOffers( boolean userOffers ) throws Exception {
		final OffersManager manager = new OffersManager();
		//DM: sem settar essa flag, o manager nunca sabe que tem que ler o nome da oferta
		manager.useTimestamp = userOffers;

		AppMIDlet.loadData( DATABASE_NAME, userOffers ? DATABASE_SLOT_USER_OFFERS : DATABASE_SLOT_OFFERS, manager );

		if ( userOffers )
			instance.userOffers = manager.offers;
		else
			instance.offers = manager.offers;
	}


	private static final void addStores( Store[] newStores ) throws Exception {
		final Store[] currentStores = instance.stores;
		final Vector storesToAdd = new Vector( currentStores.length );

		for ( short i = 0; i < newStores.length; ++i ) {
			final Store s1 = newStores[ i ];

			boolean found = false;
			for ( short j = 0; j < currentStores.length; ++j ) {
				if ( s1.getCode() == currentStores[ j ].getCode() ) {
					found = true;
					break;
				}
			}

			if ( !found )
				storesToAdd.addElement( s1 );
		}

		newStores = new Store[ storesToAdd.size() ];
		storesToAdd.copyInto( newStores );
		final Store[] stores = new Store[ currentStores.length + newStores.length ];
		System.arraycopy( currentStores, 0, stores, 0, currentStores.length );
		System.arraycopy( newStores, 0, stores, currentStores.length, newStores.length );

		saveStores( stores );
	}


	public static final void saveStores( Store[] stores ) throws Exception {
		final StoresManager manager = new StoresManager();
		manager.stores = stores;
		instance.stores = manager.stores;

		AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_STORES, manager );
	}


	private static final void loadStores() throws Exception {
		final StoresManager manager = new StoresManager();
		AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_STORES, manager );
		instance.stores = manager.stores;
	}


	/**
	 *
	 * @param listener
	 * @param nextScreen
	 * @throws java.lang.Exception
	 */
	public static final void refreshCategories( LoadScreen listener, int nextScreen ) throws Exception {
		instance.listener = listener;
		instance.nextScreen = nextScreen;

		final ByteArrayOutputStream b = new ByteArrayOutputStream();
		final DataOutputStream out = new DataOutputStream( b );

		// escreve os dados globais
		NanoOnline.writeGlobalData( out );

		out.writeByte( PARAM_LAST_MODIFIED_DATE );
		out.writeLong( instance.categoriesTimestamp );
		//#if DEBUG == "true"
			System.out.println( "-----*-*-*-/*-/-*/--/*-*-/--*-*-*-**---" );
			System.out.println( "-----*-*-*-/*-/-*/--/*-*-/--*-*-*-**---" );
			System.out.println( "-----*-*-*-/*-/-*/--/*-*-/--*-*-*-**---" );
			System.out.println( instance.categoriesTimestamp + " -> " + GameMIDlet.formatTime( instance.categoriesTimestamp, GameMIDlet.getText( TEXT_TIME_FORMAT ) ) );
		//#endif
		out.writeByte( PARAM_DATA_END );

		out.flush();
		activeConnection = NanoConnection.post( BOADICA_URL + "/categories", b.toByteArray(), instance, false );
		instance.fp_minProgress = 0;
	}


	/**
	 *
	 */
	public static final void cancelConnection( int backScreen ) {

		//#if DEBUG == "true"
			if (activeConnection < 0)
				System.out.println( " tentou cancelar uma conexão inativa");

			System.out.println( "BoadicaData.cancelConnection("+ activeConnection +")");
		//#endif

		NanoConnection.cancel( activeConnection );
		activeConnection = -1;
		instance.backScreen = backScreen;
	}


	/**
	 *
	 * @param offer
	 * @return
	 */
	public static final int getUserOfferIndex( Offer offer ) {
		for ( short i = 0; i < instance.userOffers.length; ++i ) {
			if ( instance.userOffers[ i ].equals( offer ) )
				return i;
		}

		return -1;
	}


	/**
	 *
	 * @return
	 */
	public static final Offer[] getUserOffers() {
		return instance.userOffers;
	}


	/**
	 *
	 * @param offer
	 * @return
	 */
	public static final boolean isSaved( Offer offer ) {
		return getUserOfferIndex( offer ) >= 0;
	}


	/**
	 *
	 * @param storeCode
	 * @return
	 */
	public static final Store getStore( int storeCode ) {
		final Store[] stores = instance.stores;
		for ( short i = 0; i < stores.length; ++i ) {
			if ( stores[ i ].getCode() == storeCode )
				return stores[ i ];
		}

		return null;
	}


	/**
	 *
	 * @param storeCode
	 * @throws java.lang.Exception
	 */
	public static final void getStoreInfo( int storeCode ) throws Exception {
		final ByteArrayOutputStream b = new ByteArrayOutputStream();
		final DataOutputStream out = new DataOutputStream( b );

		// escreve os dados globais
		NanoOnline.writeGlobalData( out );

		out.writeByte( PARAM_STORE_CODE );
		out.writeInt( storeCode );

		out.flush();

		activeConnection = NanoConnection.post( BOADICA_URL + "/store", b.toByteArray(), instance, false );
		instance.fp_minProgress = 0;
	}


	/**
	 *
	 * @param listener
	 * @param nextScreen
	 * @param categoryId
	 * @throws java.lang.Exception
	 */
	public static final void getOffers(  LoadScreen listener, int nextScreen, int categoryId ) throws Exception {
		instance.listener = listener;
		instance.nextScreen = nextScreen;

		final ByteArrayOutputStream b = new ByteArrayOutputStream();
		final DataOutputStream out = new DataOutputStream( b );

		// escreve os dados globais
		NanoOnline.writeGlobalData( out );

		final Category category = getCategory( categoryId );

		out.writeByte( PARAM_LAST_MODIFIED_DATE );
		out.writeLong( instance.categoriesTimestamp );
		out.writeByte( PARAM_CATEGORY_ID );
		out.writeShort( category.getId() );
		out.writeByte( PARAM_OFFERS_PER_SEARCH );
		out.writeShort( GameMIDlet.getPagesPerSearch() * OFFERS_PER_PAGE );

		if ( OffersScreen.getInstance() != null ) {
			out.writeByte( PARAM_PAGE );
			out.writeShort( OffersScreen.getInstance().getPageIndexToLoad() );
		}

		out.flush();

		activeConnection = NanoConnection.post( BOADICA_URL + "/offers", b.toByteArray(), instance, false );
		instance.fp_minProgress = 0;
	}


	public static final Category[] getRootCategories() {
		if ( instance.categories.length > 0 ) {
			final Vector v = new Vector();
			for ( short i = 0; instance.categories[ i ].getParentId() == -1; ++i ) {
				v.addElement( instance.categories[ i ] );
			}

			final Category[] categories = new Category[ v.size() ];
			v.copyInto( categories );

			return categories;
		} else {
			// retorna as categorias padrão
			final byte ROOT_CATEGORIES = 9;
			final Category[] rootCategories = new Category[ ROOT_CATEGORIES ];

			for ( byte i = 0; i < ROOT_CATEGORIES; ++i ) {
				final Category c =  new Category();
				c.setName( GameMIDlet.getText( TEXT_CATEGORY_COMPUTER + i ) );
				rootCategories[ i ] = c;
			}

			return rootCategories;
		}
	}


	/**
	 *
	 * @param id
	 * @return
	 */
	public static final Category getCategory( int id ) {
		// como o array está ordenado, é só fazer uma busca binária
		final Category[] c = instance.categories;

		int max = instance.categories.length - 1;
		if ( max >= 0 && id >= 0 ) {
			int min = 0;
			int mid = 0;

			do {
				mid = min + ( ( max - min ) >> 1 );

				if ( id > c[ mid ].getId() )
					min = mid + 1;
				else
					max = mid - 1;
			} while ( c[ mid ].getId() != id && min <= max );

			return c[ mid ].getId() == id ? c[ mid ] : null;
		} else {
			return null;
		}
	}


	/**
	 *
	 * @param id
	 * @param data
	 */
	public final void processData( final int id, final byte[] data ) {
		final Thread loadThread = new Thread() {
			public final void run() {
				try {
					if ( activeConnection < 0 ) {
						onConnectionEnded();
						return;
					}
					final Hashtable t = NanoOnline.readGlobalData( data );

					switch ( ( ( Short ) t.get( new Byte( NanoOnline.ID_RETURN_CODE ) ) ).shortValue() ) {
						case NanoOnline.RC_APP_NOT_FOUND:
							if ( listener != null )
								listener.setText( NanoOnline.getText( NanoOnline.TEXT_ERROR_APP_NOT_FOUND ) );
						return;

						case NanoOnline.RC_SERVER_INTERNAL_ERROR:
							listener.setText( NanoOnline.getText( NanoOnline.TEXT_ERROR_SERVER_INTERNAL ) );
						return;

						case NanoOnline.RC_DEVICE_NOT_SUPPORTED:
							listener.setText( NanoOnline.getText( NanoOnline.TEXT_ERROR_DEVICE_NOT_SUPPORTED ) );
						return;
					}

					final byte[] specificData = ( byte[] ) t.get( new Byte( NanoOnline.ID_SPECIFIC_DATA ) );
					final DataInputStream input = new DataInputStream( new ByteArrayInputStream( specificData ) );

					byte paramId = -1;
					do {
						if ( activeConnection < 0) {
							break;
						}

						paramId = input.readByte();
						switch ( paramId ) {
							case PARAM_CATEGORIES_DATA:

								System.gc();
								final long freeMemory = Runtime.getRuntime().freeMemory();
								instance.categories = new Category[ 0 ];
								System.gc();
								//#if DEBUG == "true"
									System.out.println( "Memory dif: " + ( Runtime.getRuntime().freeMemory() - freeMemory ) );
								//#endif

								do {
									paramId = input.readByte();
									switch ( paramId ) {
										case PARAM_N_CATEGORIES:
											final int total = input.readInt();
											final Category[] newCategories = new Category[ total ];
											for ( int i = 0; i < total && BoadicaData.activeConnection >= 0; ++i ) {
												//#if DEBUG == "true"
													System.out.println( "categoria " + i + " / " + total );
												//#endif
												final Category c = new Category();
												newCategories[ i ] = c;
												c.read( input );

												if ( listener != null && ( ( i % CATEGORIES_PER_SLEEP ) == 0 ) ) {
													listener.setText( "Lendo categoria " + i + " / " + total );
													listener.setProgress( NanoMath.divInt( i, total ) );
													Thread.sleep( 1 );
												}
											}

											if (activeConnection >= 0) {
												try {
													saveCategories( newCategories );
												} catch ( Throwable th ) {
													//#if DEBUG == "true"
														th.printStackTrace();
														AppMIDlet.log( th, "a2" );
													//#endif
												}
											}
										break;

										case PARAM_LAST_MODIFIED_DATE:
												instance.categoriesTimestamp = input.readLong();
										break;
									}
								} while ( paramId != PARAM_DATA_END && BoadicaData.activeConnection >= 0 );
							break;

							case PARAM_OFFER_DATA:
								int page = -1;
								do {
									if ( activeConnection < 0 ) {
										break;
									}
									paramId = input.readByte();
									switch ( paramId ) {
										case PARAM_N_OFFERS:
											final int totalOffers = input.readShort();
											//#if DEBUG == "true"
												System.out.println( "LENDO " + totalOffers + " OFERTAS" );
											//#endif

											final Offer[] newOffers = new Offer[ totalOffers ];
											for ( int i = 0; i < totalOffers && BoadicaData.activeConnection >= 0; ++i ) {
												//#if DEBUG == "true"
													System.out.println( "OFERTA #" + i );
												//#endif

												final Offer offer = new Offer();
												offer.read( input );
												newOffers[ i ] = offer;
											}
											if (activeConnection >= 0) {
												try {
													saveOffers( newOffers, false );
												} catch ( Throwable th ) {
													//#if DEBUG == "true"
														th.printStackTrace();
														AppMIDlet.log( th, "a3" );
													//#endif
												}
												OffersScreen.setOffers( newOffers, page );
											}
										break;

										case PARAM_PAGE:
											page = input.readShort();
										break;

										case PARAM_TOTAL_PAGES:
											OffersScreen.setTotalPages( input.readShort() );
										break;

										case PARAM_TOTAL_STORES:
											if ( activeConnection < 0 ) {
												break;
											}
											final short totalStores = input.readShort();
											final Store[] newStores = new Store[ totalStores ];
											for ( short i = 0; i < totalStores && BoadicaData.activeConnection >= 0; ++i ) {
												final Store s = new Store();
												s.read( input );
												newStores[ i ] = s;
											}

											if (activeConnection >= 0)
												try {
													addStores( newStores );
												} catch ( Throwable th ) {
													//#if DEBUG == "true"
														th.printStackTrace();
														AppMIDlet.log( th, "a4" );
													//#endif
												}
										break;
									}
								} while ( paramId != PARAM_DATA_END && BoadicaData.activeConnection >= 0 );
							break;

							case PARAM_STORE_DATA:
								final Store s = new Store();
								s.read( input );
							break;

							case PARAM_DATA_END:
								// vai para o próximo paramId
							case PARAM_BOADICA_DATA_END:
							break;

							default:
								//#if DEBUG == "true"
									System.out.println( "ID INVÁLIDO: " + paramId );
								//#endif
						}

					} while ( paramId != PARAM_BOADICA_DATA_END && BoadicaData.activeConnection >= 0);
				} catch ( Exception e ) {
					//#if DEBUG == "true"
						e.printStackTrace();
						AppMIDlet.log( e, "a5" );
					//#endif
					GameMIDlet.setLastError( e.toString() );
					GameMIDlet.getInstance().setScreen( ( activeConnection >= 0 ) ? SCREEN_ERROR_MESSAGE : SCREEN_CATEGORIES_ROOT);
				} finally {
					onConnectionEnded();
                }
			}
		};
		loadThread.setPriority( Thread.MAX_PRIORITY );
		loadThread.start();
	}


	private final void onConnectionEnded() {
		//#if DEBUG == "true"
			System.out.println( "BoadicaData.onConnectionEnded: " + listener );
		//#endif

		if ( listener != null ) {
			final LoadScreen l = listener;
			final int n = nextScreen;
			listener = null;
			nextScreen = -1;

			if ( l.isActive() ) {
				l.setActive( false );
				if ( backScreen == -1 ) {
					GameMIDlet.setScreen( backScreen == -1 ? n : backScreen );
				} else {
					GameMIDlet.setScreen( backScreen );
					backScreen = -1;
				}
			} else {
//#if DEBUG == "true"
				System.out.println( "------------------------" );
//#endif
			}
		}
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		//#if DEBUG == "true"
			System.out.println( "onInfo: " + id + ", " + infoIndex + ", " + extraData );
		//#endif

		if ( activeConnection >= 0 ) {
			if ( listener != null ) {
				final StringBuffer buffer = new StringBuffer();
				switch ( infoIndex ) {
					case INFO_CONNECTION_OPENED:
						buffer.append( NanoOnline.getText( NanoOnline.TEXT_CONNECTION_OPENED ) );
						fp_minProgress = FP_PROGRESS_DATA_MIN / 5;
					break;

					case INFO_OUTPUT_STREAM_OPENED:
					case INFO_DATA_WRITTEN:
						buffer.append( NanoOnline.getText( NanoOnline.TEXT_SENDING_DATA ) );
						fp_minProgress = FP_PROGRESS_DATA_MIN / 4;
					break;

					case INFO_RESPONSE_CODE:
						buffer.append( NanoOnline.getText( NanoOnline.TEXT_RESPONSE_CODE ) );
						buffer.append( ( ( Integer ) extraData ).intValue() );

						fp_minProgress = FP_PROGRESS_DATA_MIN / 3;
					break;

					case INFO_INPUT_STREAM_OPENED:
						buffer.append( NanoOnline.getText( NanoOnline.TEXT_RECEIVING_DATA ) );

						fp_minProgress = FP_PROGRESS_DATA_MIN / 2;
					break;

					case INFO_CONTENT_LENGTH_TOTAL:
						contentLengthTotal = ( ( Integer ) extraData ).intValue();
						refreshLabel( buffer );
					break;

					case INFO_CONTENT_LENGTH_READ:
						contentLengthRead = ( ( Integer ) extraData ).intValue();

						buffer.append( NanoOnline.getText( NanoOnline.TEXT_RECEIVING_DATA ) );
						refreshLabel( buffer );
						fp_minProgress = FP_PROGRESS_DATA_MIN;
					break;

					case INFO_CONNECTION_ENDED:
					return;

					default:
						fp_minProgress = 0;
				}

				listener.setText( buffer.toString() );
			}
			refreshProgress();
		} else {
			onConnectionEnded();
		}
	}


	private final void refreshProgress() {
		if ( listener != null ) {
			if ( contentLengthTotal > 0 )
				listener.setProgress( fp_minProgress + NanoMath.mulFixed( NanoMath.divInt( contentLengthRead, contentLengthTotal ), NanoMath.ONE - FP_PROGRESS_DATA_MIN ) );
			else
				listener.setProgress( fp_minProgress );
		}
	}


	/**
	 *
	 * @param buffer
	 */
	private final void refreshLabel( StringBuffer buffer ) {
		buffer.append( ' ' );

		String unit = "";
		// primeiro atualiza o contador de progresso efetuado
		if ( contentLengthRead < KILOBYTE ) {
			buffer.append( contentLengthRead );
			unit = " bytes";
		} else if ( contentLengthTotal < MEGABYTE ) {
			buffer.append( contentLengthRead / KILOBYTE );
			unit = " KB";
		} else {
			buffer.append( contentLengthRead / MEGABYTE );
			unit = "  MB";
		}

		// agora atualiza o indicador do total de bytes, caso ele seja válido (nem sempre o tamanho total é conhecido)
		if ( contentLengthTotal > 0 ) {
			buffer.append( '/' );

			if ( contentLengthTotal < KILOBYTE ) {
				buffer.append( contentLengthTotal );
			} else if ( contentLengthTotal < NanoMath.MAX_INTEGER_VALUE ) {
				buffer.append( contentLengthTotal / KILOBYTE );
			} else {
				// ultrapassa o valor máximo permitido no fixedPoint, então arredonda o valor
				buffer.append( contentLengthRead / MEGABYTE );
			}
		}

		buffer.append( unit );

		//#if DEBUG == "true"
//			try {
//				Thread.sleep( 70 ); // reduz velocidade para poder acompanhar barra de download no emulador
//			} catch ( InterruptedException ex ) {
//			}
		//#endif
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		//#if DEBUG == "true"
		System.out.println( "onError: " + id + ", " + errorIndex + ", " + extraData );
		//#endif

		if ( listener != null && listener.isActive() ) {
			listener.setActive( false );
			GameMIDlet.setLastError( " (" + errorIndex + ')' );
			GameMIDlet.getInstance().setScreen(
					( activeConnection >= 0 ) ? SCREEN_ERROR_MESSAGE : SCREEN_CATEGORIES_ROOT );
		} else {
			//#if DEBUG == "true"
				System.out.println( " ******* treco  ******" );
			//#endif
		}

	}


	/**
	 * Limpa o RMS
	 */
	public final void clearRMS() {
		clearCategoriesCascading();
		clearUserOffers();
	}

	public final void clearCategoriesCascading(){
		try {
			clearCategories();
			clearOffers();
			clearStores();
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
				AppMIDlet.log( ex, "a6" );
			//#endif
		}
	}

	public final void clearCategories(){
		try {
			saveCategories( new Category[ 0 ] );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
				AppMIDlet.log( ex, "a7" );
			//#endif
		}
	}

	public final void clearOffers(){
		try {
			saveOffers( new Offer[ 0 ], false );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
				AppMIDlet.log( ex, "a8" );
			//#endif
		}
	}
	public final void clearUserOffers(){
		try {
			saveOffers( new Offer[ 0 ], true );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
				AppMIDlet.log( ex, "a9" );
			//#endif
		}
	}

	public final void clearStores(){
		try {
			saveStores( new Store[ 0 ] );
		} catch ( Exception ex ) {
			//#if DEBUG == "true"
				ex.printStackTrace();
				AppMIDlet.log( ex, "a10" );
			//#endif
		}
	}



	private static final class StoresManager implements Serializable {

		private Store[] stores = new Store[ 0 ];

		public final void write( DataOutputStream output ) throws Exception {
			output.writeShort( stores.length );
			for ( short i = 0; i < stores.length; ++i ) {
				stores[ i ].write( output );
			}
		}


		public final void read( DataInputStream input ) throws Exception {
			stores = new Store[ input.readShort() ];
			for ( short i = 0; i < stores.length; ++i ) {
				stores[ i ] = new Store();
				stores[ i ].read( input );
			}
		}
	}


	private static final class OffersManager implements Serializable {

		private Offer[] offers = new Offer[ 0 ];

		private boolean useTimestamp;


		public final void write( DataOutputStream output ) throws Exception {
			output.writeShort( offers.length );
			for ( short i = 0; i < offers.length; ++i ) {
				offers[ i ].write( output, useTimestamp );
			}
		}


		public final void read( DataInputStream input ) throws Exception {
			offers = new Offer[ input.readShort() ];
			for ( short i = 0; i < offers.length; ++i ) {
				offers[ i ] = new Offer();
				offers[ i ].read( input, useTimestamp );
			}
		}
	}


	private static final class CategoriesManager implements Serializable {

		private Category[] categories = new Category[ 0 ];

		private LoadScreen listener;

		private long timestamp;


		public final void write( DataOutputStream output ) throws Exception {
			//#if DEBUG == "true"
				System.out.println( "Gravando " + categories.length + " categorias" );
			//#endif
			output.writeLong( timestamp );
			output.writeShort( categories.length );
			for ( short i = 0; i < categories.length; ++i ) {
				if ( listener != null && ( i % CATEGORIES_PER_SLEEP == 0 ) ) {
					listener.setText( "Gravando categoria " + ( i + 1 ) + " / " + categories.length );
					listener.setProgress( NanoMath.divInt( i, categories.length ) );
					Thread.sleep( 1 );
				}

				categories[ i ].write( output );
				//#if DEBUG == "true"
					System.out.print( ( i % CATEGORIES_PER_SLEEP == 0 ) ? String.valueOf( i ) : "." );
				//#endif
			}
			//#if DEBUG == "true"
				System.out.println( "\nFim da gravação das categorias no RMS." );
			//#endif
		}


		public final void read( DataInputStream input ) throws Exception {
			//#if DEBUG == "true"
				System.out.println( "Carregando categorias" );
			//#endif

			timestamp = input.readLong();

			//#if DEBUG == "true"
				System.out.println( "Carregando categorias -> timestamp: " + timestamp );
			//#endif

			categories = new Category[ input.readShort() ];

			//#if DEBUG == "true"
				System.out.println( "Carregando categorias -> total: " + categories.length );
			//#endif
			for ( short i = 0; i < categories.length; ++i ) {
				categories[ i ] = new Category();
				categories[ i ].read( input );
			}
		}
	}


	// usando GPS do aparelho
//public void checkLocation() throws Exception
//    {
//        String string;
//        Location l;
//        LocationProvider lp;
//        Coordinates c;
//        // Set criteria for selecting a location provider:
//        // accurate to 500 meters horizontally
//        Criteria cr= new Criteria();
//        cr.setHorizontalAccuracy(500);
//
//        // Get an instance of the provider
//        lp= LocationProvider.getInstance(cr);
//
//        // Request the location, setting a one-minute timeout
//        l = lp.getLocation(60);
//        c = l.getQualifiedCoordinates();
//
//        if(c != null ) {
//          // Use coordinate information
//          double lat = c.getLatitude();
//          double lon = c.getLongitude();
//          string = "\nLatitude : " + lat + "\nLongitude : " + lon;
//
//        } else {
//            string ="Location API failed";
//        }
//        midlet.displayString(string);
//    }
//}

}