/**
 * TitleLabel.java
 *
 * Created on Sep 22, 2010 12:17:30 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class TitleLabel extends UpdatableGroup implements Constants {

	public static final byte STATE_SHOW_LOGO		= 0;
	public static final byte STATE_LABEL_APPEARING	= 1;
	public static final byte STATE_SHOW_LABEL		= 2;
	public static final byte STATE_LOGO_APPEARING	= 3;

	protected byte state;

	private final Drawable logo;

	private static DrawableImage[] LOGO;

	private final MUV muv;

	public static final short TIME_TOP_ANIMATION = 400;

	private final MarqueeLabel label;
	
	private final Pattern bar;

	private short waitTime;

	private String nextText;


	public TitleLabel() throws Exception {
		super( 4 );

		bar = new Pattern( new DrawableImage( PATH_IMAGES + "barra.png" ) );
		insertDrawable( bar );

		logo = getLogo();
		insertDrawable( logo );

		setSize( ScreenManager.SCREEN_WIDTH, logo.getHeight() );

		label = new MarqueeLabel( FONT_TEXT_BOLD, null );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		insertDrawable( label );

		bar.setSize( Short.MAX_VALUE, bar.getHeight() );
		muv = new MUV( getHeight() * 1000 / TIME_TOP_ANIMATION );
		setState( STATE_SHOW_LOGO );
	}

/**
 * 
 * @return
 * @throws Exception
 */
	public static final DrawableGroup getLogo() throws Exception {
		if ( LOGO == null ) {
			LOGO = new DrawableImage[ 1];
			LOGO[ 0 ] = new DrawableImage( PATH_IMAGES + "coruja_menu.png" );
		}
		final int HEIGHT = LOGO[ 0 ].getHeight();

		final DrawableGroup group = new DrawableGroup( 1 );
		int x = ScreenManager.SCREEN_WIDTH - LOGO [ 0 ].getWidth();
		final DrawableImage image = new DrawableImage( LOGO[ 0 ] );
		group.insertDrawable( image );
		image.setPosition( x, ( HEIGHT - image.getHeight() ) >> 1 );
		x += image.getWidth();
		group.setSize( x, LOGO[ 0 ].getHeight() );

		return group;
	}


	public final void setText( int textIndex, int waitTime ) {
		setText( GameMIDlet.getText( textIndex ), waitTime );
	}


	public final void setText( String text, int waitTime ) {
		nextText = text;
		this.waitTime = ( short ) waitTime;
	}


	public final void setFont( ImageFont font ) {
		label.setFont( font );
		refreshLabel();
	}


	private final void refreshLabel() {
		label.setSize( getWidth(), label.getFont().getHeight() );
		label.setPosition( 0, ( getHeight() - label.getHeight() ) >> 1 );
		final int WIDTH = label.getFont().getTextWidth( label.getText() );
		label.setTextOffset( WIDTH < getWidth() ? ( getWidth() - WIDTH >> 1 ) : 0 );
	}


	public final void update( int delta ) {
		super.update( delta );
		
		switch ( state ) {
			case STATE_LABEL_APPEARING:
				moveItems( muv.updateInt( delta ) );
				if ( bar.getPosY() <= 0 )
					setState( STATE_SHOW_LABEL );
			break;

			case STATE_LOGO_APPEARING:
				moveItems( muv.updateInt( delta ) );
				if ( logo.getPosY() >= 0 )
					setState( STATE_SHOW_LOGO );
			break;
		}

		if ( waitTime > 0 ) {
			waitTime -= delta;
			if ( waitTime <= 0 )
				setState( STATE_LABEL_APPEARING );
		}
	}


	public final void setState( int state ) {
		switch ( state ) {
			case STATE_SHOW_LOGO:
				moveItems( -logo.getPosY() );
			break;

			case STATE_SHOW_LABEL:
				moveItems( -logo.getHeight() - logo.getPosY() );
			break;

			case STATE_LABEL_APPEARING:
				muv.setSpeed( -Math.abs( muv.getSpeed() ) );
				label.setText( nextText, false );
				refreshLabel();
				// coloca o label na posição correta
				moveItems( 0 );
			break;

			case STATE_LOGO_APPEARING:
				muv.setSpeed( Math.abs( muv.getSpeed() ) );
			break;
		}

		this.state = ( byte ) state;
	}


	private final void moveItems( int dy ) {
		logo.move( 0, dy );
		label.setPosition( label.getPosX(), bar.getPosY() + ( ( getHeight() - label.getHeight() ) >> 1 ) );
	}
}
