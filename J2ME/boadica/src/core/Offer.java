/**
 * Offer.java
 *
 * Created on Dec 4, 2010 8:50:28 PM
 *
 */

package core;

import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Offer implements Constants, Serializable {

	/***/
	private String model;

	/***/
	private String specification;

	/***/
	private String vendor;

	/***/
	private int storeCode;

	/***/
	private int price;

	/***/
	private long timestamp;

	/***/
	private String name;


	public String getModel() {
		return model;
	}


	public void setModel( String model ) {
		this.model = model;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice( int price ) {
		this.price = price;
	}


	public String getSpecification() {
		return specification;
	}


	public void setSpecification( String specification ) {
		this.specification = specification;
	}


	public int getStoreCode() {
		return storeCode;
	}


	public final Store getStore() {
		return BoadicaData.getStore( getStoreCode() );
	}


	public void setStoreCode( int storeCode ) {
		this.storeCode = storeCode;
	}


	public String getVendor() {
		return vendor;
	}


	public void setVendor( String vendor ) {
		this.vendor = vendor;
	}


	public long getTimestamp() {
		return timestamp;
	}


	public void setTimestamp( long timestamp ) {
		this.timestamp = timestamp;
	}


	public final boolean isSaved() {
		return BoadicaData.getUserOfferIndex( this ) >= 0;
	}


	public final String getShortName() {
		final StringBuffer b = new StringBuffer();
		b.append( getVendor() );
		b.append( " - ");
		b.append( getModel() );
		b.append( " - ");
		b.append( getFormattedPrice() );

		return b.toString();
	}


	public final String getName() {
		return name;
	}


	public final void setName( String name ) {
		this.name = name;
	}


	public final boolean equals( Offer offer2 ) {
		return ( getPrice() == offer2.getPrice() ) &&
			   ( getStoreCode() == offer2.getStoreCode() ) &&
			   ( getVendor().compareTo( offer2.getVendor() ) == 0 ) &&
			   ( getModel().compareTo( offer2.getModel() ) == 0 ) &&
			   ( getSpecification().compareTo( offer2.getSpecification() ) == 0 );
	}


	public final String getRecommendText() {
		final Store s = getStore();
		final StringBuffer buffer = new StringBuffer( getModel() );
		buffer.append( " por " );
		buffer.append( getFormattedPrice() );
		buffer.append( " na " );
		buffer.append( s.getName() );
		buffer.append( ": " );
		buffer.append( s.getPhonesString( true ) );

		return buffer.toString();
	}


	//#if DEBUG == "true"
		public static final Offer getDummyOffer() {
			final Offer offer = new Offer();

			offer.setModel( "modelo " + NanoMath.randInt( 100 ) );
			offer.setPrice( 1 + NanoMath.randInt( 1999 * 100 ) );
			offer.setSpecification( "Processador Core 2 Duo E7300, 45nm, 2.66 GHz, Dual Core, FSB 1066 Mhz, Socket LGA, 3 MB Cache, In Box\nGarantia: 3 anos" );
			offer.setStoreCode( 1 + NanoMath.randInt( 100 ) );
			offer.setVendor( "ACME INDUSTRIES" + NanoMath.randInt( 1000 ) );

			return offer;
		}
	//#endif


	public final String getFormattedPrice() {
		return GameMIDlet.getCurrencyString( getPrice() );
	}


	public final String toFormattedString() {
		final StringBuffer s = new StringBuffer();

		s.append( "<FNT_0>" );
		s.append( getModel() );
		s.append( "\n<FNT_0>" );
		s.append( GameMIDlet.getText( TEXT_SPECIFICATIONS ) );
		s.append( ": <FNT_1>" );
		s.append( getSpecification() );
		s.append( "\n<FNT_0>" );
		s.append( GameMIDlet.getText( TEXT_PRICE ) );
		s.append( ": <FNT_1>" );
		s.append( getFormattedPrice() );

		if ( getTimestamp() > 0 ) {
			s.append( "\n<FNT_0>" );
			s.append( GameMIDlet.getText( TEXT_OFFER_SAVED_AT ) );
			s.append( ": <FNT_1>" );
			s.append( GameMIDlet.formatTime( getTimestamp(), GameMIDlet.getText( TEXT_TIME_FORMAT ) ) );
		}
		s.append( "\n\n" );
		
		final Store store = BoadicaData.getStore( getStoreCode() );
		if ( store == null )
			s.append( "DADOS DA LOJA NÃO ENCONTRADOS" ); // TODO dados da loja não encontrados
		else
			s.append( store.toFormattedString() );

		return s.toString();
	}


	public final void write( DataOutputStream out, boolean userOffer ) throws Exception {
		out.writeUTF( getModel() );
		out.writeUTF( getSpecification() );
		out.writeUTF( getVendor() );
		out.writeInt( getStoreCode() );
		out.writeInt( getPrice() );

		if ( userOffer ) {
			out.writeLong( getTimestamp() );
			out.writeUTF( getName() );
		}
	}


	public final void write( DataOutputStream out ) throws Exception {
		write( out, false );
	}


	public final void read( DataInputStream in, boolean userOffer ) throws Exception {
		setModel( in.readUTF() );
		setSpecification( in.readUTF() );
		setVendor( in.readUTF() );
		setStoreCode( in.readInt() );
		setPrice( in.readInt() );

		if ( userOffer ) {
			setTimestamp( in.readLong() );
			setName( in.readUTF() );
		}
		
		//#if DEBUG == "true"
			System.out.println( "READ OFFER: " + toFormattedString() );
		//#endif
	}


	public final void read( DataInputStream in ) throws Exception {
		read( in, false );
	}


}
