/**
 * Store.java
 *
 * Created on Dec 4, 2010 8:54:12 PM
 *
 */

package core;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Vector;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Store implements Serializable {
	
	public static final byte ID_NAME			= 0;
	public static final byte ID_CODE			= 1;
	public static final byte ID_LATITUDE		= 2;
	public static final byte ID_ADDRESS			= 3;
	public static final byte ID_ADDRESS_EXTRA	= 4;
	public static final byte ID_PHONES			= 5;
	public static final byte ID_FAX				= 6;
	public static final byte ID_CITY			= 7;
	public static final byte ID_DISTRICT		= 8;
	public static final byte ID_STATE			= 9;
	public static final byte ID_CEP				= 10;
	public static final byte ID_EMAIL			= 11;
	public static final byte ID_SINCE			= 12;
	public static final byte ID_REGION			= 13;

	public static final byte PARAM_CODE			= 0;

	/***/
	private String name;

	/***/
	private int code;

	/***/
	private String latitude;

	/***/
	private String longitude;

	/***/
	private String address;

	/***/
	private String addressExtra;

	/***/
	private String[] phones;

	/***/
	private String fax;

	/***/
	private String city;

	/***/
	private String district;

	/***/
	private String state;

	/***/
	private String cep;

	/***/
	private String email;

	/***/
	private String since;

	/***/
	private String region;


	public int getCode() {
		return code;
	}


	public final boolean hasGeoInfo() {
		return getLatitude() != null && getLongitude() != null;
	}


	public void setCode( int code ) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName( String name ) {
		this.name = name;
	}


	public String getRegion() {
		return region;
	}


	public void setRegion( String region ) {
		this.region = region;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress( String address ) {
		this.address = address;
	}


	public String getAddressExtra() {
		return addressExtra;
	}


	public void setAddressExtra( String addressExtra ) {
		this.addressExtra = addressExtra;
	}


	public String getCep() {
		return cep;
	}


	public void setCep( String cep ) {
		this.cep = cep;
	}


	public String getCity() {
		return city;
	}


	public void setCity( String city ) {
		this.city = city;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail( String email ) {
		this.email = email;
	}


	public String getFax() {
		return fax;
	}


	public void setFax( String fax ) {
		this.fax = fax;
	}


	/**
	 *
	 * @param all
	 * @return
	 */
	public final String[] getPhones( boolean all ) {
		if ( all ) {
			return phones;
		}

		final Vector v = new Vector();
		for ( byte i = 0; i < phones.length; ++i ) {
			final String phone = phones[ i ];
			if ( phone != null && phone.length() > 0 )
				v.addElement( phone.trim() );
		}

		final String[] ret = new String[ v.size() ];
		v.copyInto( ret );

		return ret;
	}


	public final String getPhonesString() {
		return getPhonesString( false );
	}


	public final String getPhonesString( boolean format ) {
		String s = "";
		for ( byte i = 0; i < phones.length; ++i ) {
			s += format ? GameMIDlet.formatPhoneNumber( phones[ i ], false ) : phones[ i ];
			if ( i < phones.length - 1 && phones[ i + 1 ] != null && phones[ i + 1 ].length() > 0 )
				s += ", ";
		}

		return s;
	}


	public void addPhone( String phone ) {
		
	}


	public void setPhones( String[] phones ) {
		this.phones = phones;
	}


	public String getSince() {
		return since;
	}


	public void setSince( String since ) {
		this.since = since;
	}


	public String getState() {
		return state;
	}


	public void setState( String state ) {
		this.state = state;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude( String latitude ) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude( String longitude ) {
		this.longitude = longitude;
	}


	public String getDistrict() {
		return district;
	}


	public void setDistrict( String district ) {
		this.district = district;
	}


	public final void write( DataOutputStream out ) throws Exception {
		out.writeUTF( getName() );
		out.writeInt( getCode() );
		out.writeUTF( getLatitude() );
		out.writeUTF( getLongitude() );
		out.writeUTF( getAddress() );
		out.writeUTF( getAddressExtra() );
		out.writeByte( phones.length );
		for ( byte i = 0; i < phones.length; ++i )
			out.writeUTF( phones[ i ] );
		out.writeUTF( getFax() );
		out.writeUTF( getCity() );
		out.writeUTF( getDistrict() );
		out.writeUTF( getState() );
		out.writeUTF( getCep() );
		out.writeUTF( getEmail() );
		out.writeUTF( getSince() );
		out.writeUTF( getRegion() );
	}


	public final void read( DataInputStream in ) throws Exception {
		setName( in.readUTF() );
		setCode( in.readInt() );
		setLatitude( in.readUTF() );
		setLongitude(in.readUTF() );
		setAddress( in.readUTF() );
		setAddressExtra( in.readUTF() );
		phones = new String[ in.readByte() ];
		for ( byte i = 0; i < phones.length; ++i )
			phones[ i ] = in.readUTF();
		setFax( in.readUTF() );
		setCity( in.readUTF() );
		setDistrict( in.readUTF() );
		setState( in.readUTF() );
		setCep( in.readUTF() );
		setEmail( in.readUTF() );
		setSince( in.readUTF() );
		setRegion( in.readUTF() );
		
		//#if DEBUG == "true"
			System.out.println( "Store.read: " + this );
		//#endif
	}

	
	public final String toFormattedString() {
		final StringBuffer b = new StringBuffer();
		b.append( "<FNT_0>" );
		b.append( "Nome: " );
		b.append( "<FNT_1>" );
		b.append( getName() );
		b.append( "<FNT_0>" );
		b.append( "\nEndereço: " );
		b.append( "<FNT_1>" );
		b.append( getAddress() );
		b.append( "<FNT_0>" );
		b.append( "\nComplemento: " );
		b.append( "<FNT_1>" );
		b.append( getAddressExtra() );
		b.append( "<FNT_0>" );
		b.append( "\nTelefones: " );
		b.append( "<FNT_1>" );
		b.append( getPhonesString() );
		b.append( "<FNT_0>" );
		b.append( "\nFax: " );
		b.append( "<FNT_1>" );
		b.append( getFax() );
		b.append( "<FNT_0>" );
		b.append( "\nCidade: " );
		b.append( "<FNT_1>" );
		b.append( getCity() );
		b.append( "<FNT_0>" );
		b.append( "\nBairro: " );
		b.append( "<FNT_1>" );
		b.append( getDistrict() );
		b.append( "<FNT_0>" );
		b.append( "\nEstado: " );
		b.append( "<FNT_1>" );
		b.append( getState() );
		b.append( "<FNT_0>" );
		b.append( "\nCEP: " );
		b.append( "<FNT_1>" );
		b.append( getCep() );
		b.append( "<FNT_0>" );
		b.append( "\nEmail: " );
		b.append( "<FNT_1>" );
		b.append( getEmail() );
		b.append( "<FNT_0>" );
		b.append( "\nNo Boadica desde: " );
		b.append( "<FNT_1>" );
		b.append( getSince() );
		b.append( "<FNT_0>" );
		b.append( "\nRegião: " );
		b.append( "<FNT_1>" );
		b.append( getRegion() );

		return b.toString();
	}

	//#if DEBUG == "true"
		public static final Store getDummyStore() {
			final Store s = new Store();

			s.setAddress( "Rua do Carmo, 7, 11º andar" );
			s.setAddressExtra( "Ao lado do Terminal Garagem Menezes Cortes" );
			s.setCep( "20011-020" );
			s.setCity( "Rio de Janeiro" );
			s.setCode( 1234 );
			s.setEmail( "contato@nanogames.com.br" );
			s.setFax( "(21) 2544-5573");
			s.setName( "InfoMalandro" );
			s.setPhones( new String[] { "(21) 2544-0500", "(21) 9333-5052" } );
			s.setSince( "31/02/1234" );
			s.setState( "RJ" );
			s.setRegion( "Centro" );

			return s;
		}


		public final String toString() {
			final StringBuffer b = new StringBuffer();
			b.append( "Nome: " );
			b.append( getName() );
			b.append( "\nCódigo: " );
			b.append( getCode() );
			b.append( "\nLatitude: " );
			b.append( getLatitude() );
			b.append( "\nLongitude: " );
			b.append( getLongitude() );
			b.append( "\nEndereço: " );
			b.append( getAddress() );
			b.append( "\nComplemento: " );
			b.append( getAddressExtra() );
			b.append( "\nTelefones: " );
			b.append( getPhonesString() );
			b.append( "\nFax: " );
			b.append( getFax() );
			b.append( "\nCidade: " );
			b.append( getCity() );
			b.append( "\nBairro: " );
			b.append( getDistrict() );
			b.append( "\nEstado: " );
			b.append( getState() );
			b.append( "\nCEP: " );
			b.append( getCep() );
			b.append( "\nEmail: " );
			b.append( getEmail() );
			b.append( "\nNo Boadica desde: " );
			b.append( getSince() );
			b.append( "\nRegião: " );
			b.append( getRegion() );

			return b.toString();
		}
	//#endif

}
