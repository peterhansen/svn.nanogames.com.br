/**
 * Tab.java
 *
 * Created on Feb 5, 2011 2:44:14 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Tab extends DrawableGroup implements Constants {

	private byte index;

	private Drawable icon;

	private static DrawableImage[] TAB_LEFT;
	private static DrawableImage[] TAB_RIGHT;
	private static DrawableImage[] TAB_FILL;

	private final DrawableImage[] tabLeft;
	private final DrawableImage[] tabRight;
	private final Pattern[] tabFill;

	private byte iconIndex = -1;

	
	public Tab( int iconIndex ) throws Exception {
		super( 12 );

		if ( TAB_FILL == null ) {
			TAB_FILL = new DrawableImage[ 2 ];
			TAB_LEFT = new DrawableImage[ 2 ];
			TAB_RIGHT = new DrawableImage[ 2 ];

			for ( byte i = 0; i < 2; ++i ) {
				TAB_FILL[ i ] = new DrawableImage( PATH_MENU + "tab_f_" + i + ".png" );
				TAB_RIGHT[ i ] = new DrawableImage( PATH_MENU + "tab_r_" + i + ".png" );
				TAB_LEFT[ i ] = new DrawableImage( PATH_MENU + "tab_l_" + i + ".png" );
			}
		}

		tabFill = new Pattern[ 2 ];
		tabLeft = new DrawableImage[ 2 ];
		tabRight = new DrawableImage[ 2 ];

		for ( byte i = 0; i < 2; ++i ) {
			tabFill[ i ] = new Pattern( new DrawableImage( TAB_FILL[ i ] ) );
			tabRight[ i ] = new DrawableImage( TAB_RIGHT[ i ] );
			tabLeft[ i ] = new DrawableImage( TAB_LEFT[ i ] );

			insertDrawable( tabFill[ i ] );
			insertDrawable( tabLeft[ i ] );
			insertDrawable( tabRight[ i ] );

			tabFill[ i ].setPosition( tabLeft[ i ].getWidth(), 0 );
		}

		setIcon( iconIndex );
	}


	public final void setIcon( int iconIndex ) {
		if ( this.icon != null ) {
			removeDrawable( this.icon );
			this.icon = null;
		}

		this.iconIndex = ( byte ) iconIndex;
		this.icon = GameMIDlet.getIcon( iconIndex );
		icon.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( icon );
		setSize( icon.getWidth() + ( TAB_ICON_OFFSET << 1 ) + ( tabLeft[ 0 ].getWidth() << 1 ), tabFill[ 0 ].getHeight() );
	}


	public final byte getIconIndex() {
		return iconIndex;
	}


	public final void setActive( boolean active ) {
		index = ( byte ) ( active ? 1 : 0 );
		
		for ( byte i = 0; i < 2; ++i ) {
			tabFill[ i ].setVisible( i == index );
			tabRight[ i ].setVisible( i == index );
			tabLeft[ i ].setVisible( i == index );
		}
	}


	public final boolean isActive() {
		return index == 1;
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		for ( byte i = 0; i < 2; ++i ) {
			tabFill[ i ].setSize( width - tabLeft[ i ].getWidth() - tabRight[ i ].getWidth(), tabFill[ i ].getHeight() );
			tabRight[ i ].setPosition( width - tabRight[ i ].getWidth(), 0 );
		}

		icon.setRefPixelPosition( tabFill[ 0 ].getPosX() + ( tabFill[ 0 ].getWidth() >> 1 ), height >> 1 );
	}

}
