/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package core;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Fabiano
 */
public final class Category implements Serializable {

	/***/

	/***/
	public static final byte ID_LEVEL		= 1;

	/***/
	public static final byte ID_ID			= 2;

	/***/
	public static final byte ID_PARENT_ID	= 3;

	/***/
    private String name = "";

	/***/
	private short id = -1;

	/***/
	private short parentId = -1;

	/***/
	private short[] childrenIds = new short[ 0 ];


	public short getId() {
		return id;
	}


	public void setId( short id ) {
		this.id = id;
	}


	public final Category getParent() {
		return BoadicaData.getCategory( getParentId() );
	}


	public short getParentId() {
		return parentId;
	}


	public void setParentId( short parentId ) {
		this.parentId = parentId;
	}


	protected final boolean hasChildrenInfo() {
		return childrenIds.length > 0;
	}


	public final Category[] getChildren() {
		final Category[] children = new Category[ hasChildrenInfo() ? childrenIds.length : 0 ];

		for ( short i = 0; i < childrenIds.length; ++i )
			children[ i ] = BoadicaData.getCategory( childrenIds[ i ] );

		return children;
	}


	public final boolean hasChildren() {
		return childrenIds.length > 0;
	}


    /**
     * @return the nome
     */
    public String getName() {
        return name;
    }

    /**private short[] childrenIds = {};
     * @param nome the nome to set
     */
    public void setName(String name) {
        this.name = name;
    }


	public final void write( DataOutputStream out ) throws Exception {
		write( out, true );
	}


	public final void write( DataOutputStream out, boolean writeChildren ) throws Exception {
		out.writeUTF( getName() );
		out.writeShort( getId() );
		out.writeShort( getParentId() );

		if ( writeChildren ) {
			out.writeShort( childrenIds.length );
			for ( short i = 0; i < childrenIds.length; ++i )
				out.writeShort( childrenIds[ i ] );
		}
	}


	public final void read( DataInputStream in ) throws Exception {
		read( in, true );
	}


	public final void read( DataInputStream in, boolean readChildren ) throws Exception {
		setName( in.readUTF() );
		setId( in.readShort() );
		setParentId( in.readShort() );

		if ( readChildren ) {
			final short total = in.readShort();
			final short[] c = new short[ total ];
			for ( short i = 0; i < total; ++i )
				c[ i ] = in.readShort();

			childrenIds = c;
		}

		//#if DEBUG == "true"
			System.out.println( "Category read: " + this );
		//#endif
	}

	
	//#if DEBUG == "true"
		public final String toString() {
			final StringBuffer b = new StringBuffer();
			b.append( "Category( name: " );
			b.append( getName() );
			b.append( ", id: " );
			b.append( getId() );
			b.append( ", parent: " );
			b.append( getParentId() );
			b.append( ", children: " );
			b.append( childrenIds == null ? -1 : childrenIds.length );
			b.append( " )" );

			return b.toString();
		}
	//#endif
}
