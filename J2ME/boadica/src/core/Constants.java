/**
 * Constants.java
 * ©2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:20:28 PM.
 */

package core;

/**
 *
 * @author Peter
 */
public interface Constants {

	public static final String APP_SHORT_NAME = "BOAD";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";

	/** ID de parâmetro de conexão: número de categorias. Formato dos dados: int */
	public static final byte PARAM_N_CATEGORIES				= 0;
	/** ID de parâmetro de conexão: id da loja. Formato dos dados: int */
	public static final byte PARAM_STORE_CODE				= 1;
	/** ID de parâmetro de conexão: início dos dados das categorias. Formato dos dados: variável */
	public static final byte PARAM_CATEGORIES_DATA			= 2;
	/** ID de parâmetro de conexão: início dos dados das ofertas. Formato dos dados: variável */
	public static final byte PARAM_OFFER_DATA				= 3;
	/** ID de parâmetro de conexão: início dos dados das lojas. Formato dos dados: variável */
	public static final byte PARAM_STORE_DATA				= 4;
	/** ID de parâmetro de conexão: fim dos dados atuais. Formato dos dados: nenhum */
	public static final byte PARAM_DATA_END					= 5;
	/** ID de parâmetro de conexão: . Formato dos dados: */
	public static final byte PARAM_LAST_MODIFIED_DATE		= 6;
	/** ID de parâmetro de conexão: fim dos dados atuais. Formato dos dados: nenhum */
	public static final byte PARAM_CATEGORY_LEVEL			= 7;
	// # ID de parâmetro de conexão: número da página. Formato dos dados: short
	public static final byte PARAM_PAGE						= 8;
	// # ID de parâmetro de conexão: . Formato dos dados: 
	public static final byte PARAM_CATEGORY_NAME			= 9;
	/** ID de parâmetro de conexão: quantidade de ofertas. Formato dos dados: short */
	public static final byte PARAM_N_OFFERS					= 10;
	/** ID de parâmetro de conexão: quantidade de ofertas. Formato dos dados: short */
	public static final byte PARAM_BOADICA_DATA_END			= 11;
	/** ID de parâmetro de conexão: quantidade total de páginas. Formato dos dados: short */
	public static final byte PARAM_TOTAL_PAGES				= 12;
	/** ID de parâmetro de conexão: quantidade total de lojas. Formato dos dados: short */
	public static final byte PARAM_TOTAL_STORES				= 13;
	/** ID de parâmetro de conexão: . Formato dos dados: */
	public static final byte PARAM_CATEGORY_ID				= 14;
	/** ID de parâmetro de conexão: número de resultados por pesquisa. Formato dos dados: short */
	public static final byte PARAM_OFFERS_PER_SEARCH		= 15;

	public static final byte OFFERS_PER_PAGE = 10;
	/***/
	public static final short PAGES_PER_SEARCH_DEFAULT	= 2;

	/***/
	public static final short PAGES_PER_SEARCH_MIN		= 1;

	/***/
	public static final short PAGES_PER_SEARCH_MAX		= 10;

	/***/
	public static final short PAGES_PER_SEARCH_STEP		= 1;

	/***/
	public static final byte STORE_DATA_MODE_TOTAL_MODES	= 3;



	/** Id da categoria de notícias do Boadica no Nano Online. */
//	public static final byte NEWS_CATEGORY_ID = 7;

	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";
	public static final String PATH_ICONS = PATH_IMAGES + "c_";

	/** Caminho das imagens utilizadas nas telas de splash. */
	public static final String PATH_SPLASH = PATH_IMAGES + "splash/";

	/** Caminho dos sons do jogo. */
	public static final String PATH_SOUNDS = "/";
	
	/** Caminho das imagens usadas no scroll. */
	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	public static final String PATH_ONLINE = PATH_IMAGES + "online/";
	public static final String PATH_MENU = PATH_IMAGES + "menu/";
	
	/** Nome da base de dados. */
	public static final String DATABASE_NAME = "N";

	/**Índice do slot de gravação das categorias na base de dados. */
	public static final byte DATABASE_SLOT_CATEGORIES = 1;
	
	/**Índice do slot de gravação das lojas na base de dados. */
	public static final byte DATABASE_SLOT_STORES = 2;
	
	/**Índice do slot de gravação das ofertas na base de dados. */
	public static final byte DATABASE_SLOT_OFFERS = 3;

	/**Índice do slot de gravação das ofertas do usuário na base de dados. */
	public static final byte DATABASE_SLOT_USER_OFFERS = 4;

	/**Índice do slot de gravação das ofertas do usuário na base de dados. */
	public static final byte DATABASE_SLOT_OPTIONS = 5;
	
	/** Quantidade total de slots da base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS = 5;

	/** Offset da fonte padrão. */
	public static final byte DEFAULT_FONT_OFFSET = -1;
	
	/**Índice da fonte padrão do jogo. */
	public static final byte FONT_INDEX_DEFAULT	= 0;
	
	/**Índice da fonte utilizada para textos. */
	public static final byte FONT_PANE			= 0;
	public static final byte FONT_TEXT_BOLD		= 1;
	public static final byte FONT_TEXT			= 2;
	public static final byte FONT_TEXT_WHITE	= 3;

	/** Total de tipos de fonte do jogo. */
	public static final byte FONT_TYPES_TOTAL	= 4;

	/** Duração da vibração ao atingir a trave, em milisegundos. */
	public static final short VIBRATION_TIME_DEFAULT = 300;

//#if SCREEN_SIZE == "SMALL"
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#elif SCREEN_SIZE == "BIG"
		public static final int LOW_MEMORY_LIMIT = 1600000;
    //#else
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif

	public static final byte ICON_BACK		= 0;
	public static final byte ICON_PHONE		= 1;
	public static final byte ICON_DELETE	= 2;
	public static final byte ICON_EXIT		= 3;
	public static final byte ICON_FAVORITES	= 4;
	public static final byte ICON_HELP		= 5;
	public static final byte ICON_MAIL		= 6;
	public static final byte ICON_MAP		= 7;
	public static final byte ICON_MENU		= 8;
	public static final byte ICON_ADD		= 9;
	public static final byte ICON_OPTIONS	= 10;
	public static final byte ICON_REFRESH	= 11;
	public static final byte ICON_RENAME	= 12;
	public static final byte ICON_SAVE		= 13;
	public static final byte ICON_CREDITS	= 14;
	public static final byte ICON_SEARCH	= 15;

	public static final byte ICONS_TOTAL	= ICON_SEARCH + 1;

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DOS TEXTOS">
	public static final int TEXT_OK								     	= 0;
	public static final int TEXT_BACK									= TEXT_OK + 1;
	public static final int TEXT_OPTIONS								= TEXT_BACK + 1;
	public static final int TEXT_HELP									= TEXT_OPTIONS + 1;
	public static final int TEXT_CREDITS								= TEXT_HELP + 1;
	public static final int TEXT_EXIT									= TEXT_CREDITS + 1;
	public static final int TEXT_CREDITS_TEXT							= TEXT_EXIT + 1;
	public static final int TEXT_HOW_TO_USE								= TEXT_CREDITS_TEXT + 1;
	public static final int TEXT_COSTS								    = TEXT_HOW_TO_USE + 1;
	public static final int TEXT_HELP_HOW_TO_USE						= TEXT_COSTS + 1;
	public static final int TEXT_HELP_COSTS								= TEXT_HELP_HOW_TO_USE + 1;
	public static final int TEXT_CANCEL									= TEXT_HELP_COSTS + 1;
	public static final int TEXT_CONTINUE								= TEXT_CANCEL + 1;
	public static final int TEXT_SPLASH_NANO							= TEXT_CONTINUE + 1;
	public static final int TEXT_SPLASH_PARTNER							= TEXT_SPLASH_NANO + 1;
	public static final int TEXT_LOADING								= TEXT_SPLASH_PARTNER + 1;
	public static final int TEXT_YES									= TEXT_LOADING + 1;
	public static final int TEXT_NO										= TEXT_YES + 1;
	public static final int TEXT_RECOMMEND_TITLE						= TEXT_NO + 1;
	public static final int TEXT_RECOMMEND_TEXT							= TEXT_RECOMMEND_TITLE + 1;
	public static final int TEXT_TIME_FORMAT							= TEXT_RECOMMEND_TEXT + 1;
	public static final int TEXT_CONNECTING						        = TEXT_TIME_FORMAT + 1;
	public static final int TEXT_VERSION						        = TEXT_CONNECTING + 1;
	public static final int TEXT_RECOMMEND_OFFER						= TEXT_VERSION + 1;
	public static final int TEXT_RECOMMEND_SENT							= TEXT_RECOMMEND_OFFER + 1;
	public static final int TEXT_RECOMMEND_NOTSENT						= TEXT_RECOMMEND_SENT + 1;
	public static final int TEXT_RECOMMEND_APP_SMS						= TEXT_RECOMMEND_NOTSENT + 1;
	public static final int TEXT_RECOMMEND_OFFER_SMS				    = TEXT_RECOMMEND_APP_SMS + 1;
	public static final int TEXT_PRICE_SEARCH						    = TEXT_RECOMMEND_OFFER_SMS + 1;
	public static final int TEXT_CATEGORY_COMPUTER				        = TEXT_PRICE_SEARCH + 1;
	public static final int TEXT_CATEGORY_CPU_MEMORY			        = TEXT_CATEGORY_COMPUTER + 1;
	public static final int TEXT_CATEGORY_STORAGE				        = TEXT_CATEGORY_CPU_MEMORY + 1;
	public static final int TEXT_CATEGORY_PRINTER				        = TEXT_CATEGORY_STORAGE + 1;
	public static final int TEXT_CATEGORY_MULTIMEDIA			        = TEXT_CATEGORY_PRINTER + 1;
	public static final int TEXT_CATEGORY_NETWORK				        = TEXT_CATEGORY_MULTIMEDIA + 1;
	public static final int TEXT_CATEGORY_PERIPHERALS			        = TEXT_CATEGORY_NETWORK + 1;
	public static final int TEXT_CATEGORY_SOFTWARE				        = TEXT_CATEGORY_PERIPHERALS + 1;
	public static final int TEXT_CATEGORY_OTHERS				        = TEXT_CATEGORY_SOFTWARE + 1;
	public static final int TEXT_VENDOR							        = TEXT_CATEGORY_OTHERS + 1;
	public static final int TEXT_MODEL							        = TEXT_VENDOR + 1;
	public static final int TEXT_PRICE							        = TEXT_MODEL + 1;
	public static final int TEXT_CURRENCY						        = TEXT_PRICE + 1;
	public static final int TEXT_INFO							        = TEXT_CURRENCY + 1;
	public static final int TEXT_NO_OFFERS_FOUND				        = TEXT_INFO + 1;
	public static final int TEXT_PREVIOUS						        = TEXT_NO_OFFERS_FOUND + 1;
	public static final int TEXT_NEXT									= TEXT_PREVIOUS + 1;
	public static final int TEXT_FILTER							        = TEXT_NEXT + 1;
	public static final int TEXT_INFORMATIONS					        = TEXT_FILTER + 1;
	public static final int TEXT_SPECIFICATIONS					        = TEXT_INFORMATIONS + 1;
	public static final int TEXT_ADDRESS						        = TEXT_SPECIFICATIONS + 1;
	public static final int TEXT_DISTRICT						        = TEXT_ADDRESS + 1;
	public static final int TEXT_CITY							        = TEXT_DISTRICT + 1;
	public static final int TEXT_STATE							        = TEXT_CITY + 1;
	public static final int TEXT_PHONE									= TEXT_STATE + 1;
	public static final int TEXT_BOADICA_SINCE					        = TEXT_PHONE + 1;
	public static final int TEXT_REGION							        = TEXT_BOADICA_SINCE + 1;
	public static final int TEXT_PRICE_MIN							    = TEXT_REGION + 1;
	public static final int TEXT_PRICE_MAX						        = TEXT_PRICE_MIN + 1;
	public static final int TEXT_LIST							        = TEXT_PRICE_MAX + 1;
	public static final int TEXT_HAS_SHARE_OPTION				        = TEXT_LIST + 1;
	public static final int TEXT_SELLS_OUTSIDE					        = TEXT_HAS_SHARE_OPTION + 1;
	public static final int TEXT_CLICK_DETAILS						    = TEXT_SELLS_OUTSIDE + 1;
	public static final int TEXT_CALL								    = TEXT_CLICK_DETAILS + 1;
	public static final int TEXT_LOAD_CATEGORIES_INFO				    = TEXT_CALL + 1;
	public static final int TEXT_USER_OFFERS						    = TEXT_LOAD_CATEGORIES_INFO + 1;
	public static final int TEXT_ERASE								    = TEXT_USER_OFFERS + 1;
	public static final int TEXT_MENU								    = TEXT_ERASE + 1;
	public static final int TEXT_SAVE								    = TEXT_MENU + 1;
    public static final int TEXT_MAP                                    = TEXT_SAVE + 1;
	public static final int TEXT_RESULTS_PER_SEARCH						= TEXT_MAP + 1;
	public static final int TEXT_STORE_DATA_MODE						= TEXT_RESULTS_PER_SEARCH + 1;
	public static final int TEXT_ECONOMIC								= TEXT_STORE_DATA_MODE + 1;
	public static final int TEXT_DEFAULT								= TEXT_ECONOMIC + 1;
	public static final int TEXT_COMPLETE								= TEXT_DEFAULT + 1;
	public static final int TEXT_OFFER_SAVED_AT							= TEXT_COMPLETE + 1;
	public static final int TEXT_CONFIRM								= TEXT_OFFER_SAVED_AT + 1;
	public static final int TEXT_MAP_CONFIRM							= TEXT_CONFIRM + 1;
	public static final int TEXT_MAP_TITLE								= TEXT_MAP_CONFIRM + 1;
	public static final int TEXT_OFFER_SAVE								= TEXT_MAP_TITLE + 1;
	public static final int TEXT_OFFER_SAVED							= TEXT_OFFER_SAVE + 1;
	public static final int TEXT_OFFER_ERASE							= TEXT_OFFER_SAVED + 1;
	public static final int TEXT_OFFER_ERASED							= TEXT_OFFER_ERASE + 1;
	public static final int TEXT_NO_USER_OFFERS_FOUND					= TEXT_OFFER_ERASED + 1;
	public static final int TEXT_ALL									= TEXT_NO_USER_OFFERS_FOUND + 1;
	public static final int TEXT_PLUS									= TEXT_ALL + 1;
	public static final int TEXT_MINUS									= TEXT_PLUS + 1;
	public static final int TEXT_CARRIER_CODE							= TEXT_MINUS + 1;
	public static final int TEXT_CALL_INFO								= TEXT_CARRIER_CODE + 1;
	public static final int TEXT_PAGES_PER_SEARCH						= TEXT_CALL_INFO + 1;
	public static final int TEXT_CLEAR_RMS								= TEXT_PAGES_PER_SEARCH + 1;
	public static final byte TEXT_CONFIRM_EXIT_TOTAL	 				= TEXT_CLEAR_RMS + 1;
	public static final byte TEXT_CONFIRM_BACK_TOTAL				    = TEXT_CONFIRM_EXIT_TOTAL + 1;
	public static final byte TEXT_REMOVE								= TEXT_CONFIRM_BACK_TOTAL + 1;
	public static final byte TEXT_ERROR_TITLE							= TEXT_REMOVE + 1;
	public static final byte TEXT_ERROR_TEXT							= TEXT_ERROR_TITLE + 1;
	public static final byte TEXT_EXIT_AND_EXECUTE_OPERATION			= TEXT_ERROR_TEXT + 1;
	public static final byte TEXT_EXIT_CONFIRMATION						= TEXT_EXIT_AND_EXECUTE_OPERATION + 1;
	public static final byte TEXT_CALL_TO								= TEXT_EXIT_CONFIRMATION + 1;
	/** número total de textos do jogo */
	public static final short TEXT_TOTAL = TEXT_CALL_TO + 1;
	
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS TELAS DO JOGO">
	
	public static final byte SCREEN_SPLASH_NANO					= 0;
	public static final byte SCREEN_SPLASH_BOADICA				= SCREEN_SPLASH_NANO + 1;
	public static final byte SCREEN_MAIN_MENU_PARTIAL			= SCREEN_SPLASH_BOADICA + 1;
	public static final byte SCREEN_MAIN_MENU_FULL				= SCREEN_MAIN_MENU_PARTIAL + 1;
	public static final byte SCREEN_OPTIONS						= SCREEN_MAIN_MENU_FULL + 1;
	public static final byte SCREEN_HELP_MENU					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_HELP_CONTROLS				= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_COSTS					= SCREEN_HELP_CONTROLS + 1;
	public static final byte SCREEN_HELP_PROFILE				= SCREEN_HELP_COSTS + 1;
	public static final byte SCREEN_CREDITS						= SCREEN_HELP_PROFILE + 1;
	public static final byte SCREEN_LOADING_1					= SCREEN_CREDITS + 1;
	public static final byte SCREEN_LOADING_2					= SCREEN_LOADING_1 + 1;
	public static final byte SCREEN_CHOOSE_LANGUAGE				= SCREEN_LOADING_2 + 1;
	public static final byte SCREEN_NANO_RANKING_MENU			= SCREEN_CHOOSE_LANGUAGE + 1;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_ERROR_LOG					= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_PRICES_NEW					= SCREEN_ERROR_LOG + 1;
	public static final byte SCREEN_PRICES						= SCREEN_PRICES_NEW + 1;
	public static final byte SCREEN_CATEGORIES_ROOT				= SCREEN_PRICES + 1;
	public static final byte SCREEN_CATEGORIES					= SCREEN_CATEGORIES_ROOT + 1;
	public static final byte SCREEN_LOAD_CATEGORIES_INFO		= SCREEN_CATEGORIES + 1;
	public static final byte SCREEN_LOAD_CATEGORIES				= SCREEN_LOAD_CATEGORIES_INFO + 1;
	public static final byte SCREEN_USER_OFFER_DETAILS			= SCREEN_LOAD_CATEGORIES + 1;
	public static final byte SCREEN_OFFER_DETAILS				= SCREEN_USER_OFFER_DETAILS + 1;
	public static final byte SCREEN_LOAD_OFFERS					= SCREEN_OFFER_DETAILS + 1;
	public static final byte SCREEN_USER_OFFERS					= SCREEN_LOAD_OFFERS + 1;
	public static final byte SCREEN_USER_OFFERS_NEW				= SCREEN_USER_OFFERS + 1;
	public static final byte SCREEN_MAP							= SCREEN_USER_OFFERS_NEW + 1;
	public static final byte SCREEN_LOAD_MAP					= SCREEN_MAP + 1;
	public static final byte SCREEN_LOAD_USER_MAP				= SCREEN_LOAD_MAP + 1;
	public static final byte SCREEN_OFFER_MENU					= SCREEN_LOAD_USER_MAP + 1;
	public static final byte SCREEN_USER_MAP					= SCREEN_OFFER_MENU + 1;
	public static final byte SCREEN_ERROR_MESSAGE				= SCREEN_USER_MAP + 1;	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU PRINCIPAL">

	// menu principal
	public static final byte ENTRY_MAIN_MENU_PRICE_SEARCH		= 0;
	public static final byte ENTRY_MAIN_MENU_HELP_HOW_TO_USE	= ENTRY_MAIN_MENU_PRICE_SEARCH + 1;
	public static final byte ENTRY_MAIN_MENU_HELP_COSTS			= ENTRY_MAIN_MENU_HELP_HOW_TO_USE + 1;
	public static final byte ENTRY_MAIN_MENU_CREDITS			= ENTRY_MAIN_MENU_HELP_COSTS + 1;
	public static final byte ENTRY_MAIN_MENU_EXIT				= ENTRY_MAIN_MENU_CREDITS + 1;

	//#if DEBUG == "true"
		public static final byte ENTRY_MAIN_MENU_ERROR_LOG	= ENTRY_MAIN_MENU_EXIT + 1;
	//#endif

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ÍNDICES DAS ENTRADAS DO MENU DE CATEGORIAS">

	// menu principal
	public static final byte ENTRY_CATEGORIES_COMPUTER		= 0;
	public static final byte ENTRY_CATEGORIES_CPU_MEMORY	= 1;
	public static final byte ENTRY_CATEGORIES_STORAGE		= 2;
	public static final byte ENTRY_CATEGORIES_PRINTER		= 3;
	public static final byte ENTRY_CATEGORIES_MULTIMEDIA	= 4;
	public static final byte ENTRY_CATEGORIES_NETWORK		= 5;
	public static final byte ENTRY_CATEGORIES_PERIPHERALS	= 6;
	public static final byte ENTRY_CATEGORIES_SOFTWARE		= 7;
	public static final byte ENTRY_CATEGORIES_OTHERS		= 8;
	public static final byte ENTRY_CATEGORIES_BACK			= -1;

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE AJUDA">
	
	public static final byte ENTRY_HELP_MENU_OBJETIVES					= 0;
	public static final byte ENTRY_HELP_MENU_CONTROLS					= 1;
	public static final byte ENTRY_HELP_MENU_BACK						= 2;
	
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE OPÇÕES">

	public static final byte ENTRY_OPTIONS_RESULTS_PER_SEARCH		= 0;
	public static final byte ENTRY_OPTIONS_BACK						= 1;

	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="�?NDICES DAS ENTRADAS DO MENU DE NOT�?CIAS">

	public static final byte ENTRY_NEWS_GENERAL		= 0;
	public static final byte ENTRY_NEWS_SCORERS		= 1;
	public static final byte ENTRY_NEWS_TABLE		= 2;
	//public static final byte ENTRY_NEWS_HELP		= 3;
	public static final byte ENTRY_NEWS_BACK		= 3;

	// </editor-fold>

	public static final byte SHINE_SEQUENCE_RIGHT	= 0;
	public static final byte SHINE_SEQUENCE_LEFT	= 1;
	public static final byte SHINE_SEUQENCE_STOPPED	= 2;

	public static final short TITLE_WAIT_TIME = 550;
	public static final int PANE_COLOR = 0xd7e9d4;

    //#if SCREEN_SIZE == "SMALL"
//#        int magicFactor = 3;
    //#else
        int magicFactor = 4;
    //#endif
	
	//#if SCREEN_SIZE == "SMALL"
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA PEQUENA" defaultstate="collapsed">
//# 
//# 		/** Espaçamento entre as entradas do menu. */
//# 		public static final int MENU_SPACING = 0;
//# 		public static final int TAB_MAX_SPACING = 1;
//# 		public static final int TAB_ICON_OFFSET = 0;
//# 			public static final int TITLE_BAR_HEIGHT = 1;
//# 
//# 		/***/
//# 		public static final byte MENU_LABEL_OFFSET_X = 3;
//# 
//# 		public static final byte TOUCH_TOLERANCE = 10;
//# 
//# 		// Velocidade para percorer o menu
//# 		public static final short TIME_GOTO_MENUITEM = 500;
//# 
//# 		/** Espaçamento vertical do título. */
//# 		public static final byte TITLE_Y_SPACING = 5;
//# 
//#   		public static final short BUTTON_MIN_WIDTH = 20;
//# 
//#  		public static final short TABBED_MANAGER_HEIGHT = 119;
//# 
//#   		public static final short PANE_HEIGHT_MAGICAL = 20;
//# 
//#  		public static final short SPLASH_OWL_BAR_OFFSET_Y = -14;
//# 
//#   		public static final byte MENU_ICON_SPACING = 5;
//# 
//#   		public static final byte TITLED_PANE_TITLE_Y = 3;
//# 
//# 
//# 	//</editor-fold>
//# 
	//#elif SCREEN_SIZE == "MEDIUM"
//#
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍ?FICAS PARA TELA MÉDIA" defaultstate="collapsed">
//#
//# 		/** Espaçamento entre as entradas do menu. */
//# 		public static final int MENU_SPACING = 0;
//# 		public static final int TAB_MAX_SPACING = 2;
//# 		public static final int TAB_ICON_OFFSET = -1;
//# 			public static final int TITLE_BAR_HEIGHT = 28;
//#
//# 		/***/
//# 		public static final byte MENU_LABEL_OFFSET_X = 4;
//#
//# 		public static final byte TOUCH_TOLERANCE = 10;
//#
//# 		// Velocidade para percorer o menu
//# 		public static final short TIME_GOTO_MENUITEM = 500;
//#
//# 		/** Espaçamento vertical do título. */
//# 		public static final byte TITLE_Y_SPACING = 5;
//#
//#   		public static final short BUTTON_MIN_WIDTH = 20;
//#
//#  		public static final short TABBED_MANAGER_HEIGHT = 160;
//#
//#   		public static final short PANE_HEIGHT_MAGICAL = 20;
//#
//#  		public static final short SPLASH_OWL_BAR_OFFSET_Y = -20;
//#
//#   		public static final byte MENU_ICON_SPACING = 5;
//#
//#   		public static final byte TITLED_PANE_TITLE_Y = 3;
//#
//#     //</editor-fold>
	//#elif SCREEN_SIZE == "BIG"

	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE" defaultstate="collapsed">

		/** Espaçamento entre as entradas do menu. */
		public static final int MENU_SPACING = 0;
		public static final int TAB_MAX_SPACING = 3;
		public static final int TAB_ICON_OFFSET = -2;
			public static final int TITLE_BAR_HEIGHT = 28;

		/***/
		public static final byte MENU_LABEL_OFFSET_X = 4;

		public static final byte TOUCH_TOLERANCE = 10;

		// Velocidade para percorer o menu
		public static final short TIME_GOTO_MENUITEM = 500;

		/** Espaçamento vertical do título. */
		public static final byte TITLE_Y_SPACING = 0;

		public static final byte EXTRA_HEIGHT = 50;

		/***/
		public static final short TABBED_MANAGER_HEIGHT = 180;

		public static final short PANE_HEIGHT_MAGICAL = 15;

		public static final short BUTTON_MIN_WIDTH = 42;

		public static final short SPLASH_OWL_BAR_OFFSET_Y = -31;

		public static final byte MENU_ICON_SPACING = 5;

		public static final byte TITLED_PANE_TITLE_Y = 3;

	//</editor-fold>

    //</editor-fold>
	//#elif SCREEN_SIZE == "GIANT"
//# 
//# 	//<editor-fold desc="DEFINIÇÕES ESPECÍFICAS PARA TELA GRANDE" defaultstate="collapsed">
//# 
//# 		/** Espaçamento entre as entradas do menu. */
//# 		public static final int MENU_SPACING = 0;
//# 		public static final int TAB_MAX_SPACING = 4;
//# 		public static final int TAB_ICON_OFFSET = -4;
//# 		public static final int TITLE_BAR_HEIGHT = 58;
//# 
//# 		/***/
//# 		public static final byte MENU_LABEL_OFFSET_X = 6;
//# 
//# 		public static final byte TOUCH_TOLERANCE = 10;
//# 
//# 		// Velocidade para percorer o menu
//# 		public static final short TIME_GOTO_MENUITEM = 500;
//# 
//# 		/** Espaçamento vertical do título. */
//# 		public static final byte TITLE_Y_SPACING = 5;
//# 
//#  		public static final short BUTTON_MIN_WIDTH = 42;
//# 
//# 		public static final short TABBED_MANAGER_HEIGHT = 300;
//# 		// DM: originalmente 30
//#  		public static final short PANE_HEIGHT_MAGICAL = 23;
//# 
//# 		public static final short SPLASH_OWL_BAR_OFFSET_Y = -31;
//# 
//#  		public static final byte MENU_ICON_SPACING = 5;
//# 
//#  		public static final byte TITLED_PANE_TITLE_Y = 3;
//# 	//</editor-fold>
//# 
//# 
	//#endif

}
