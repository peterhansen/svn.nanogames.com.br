/**
 * ScoreBar.java
 * ©2008 Nano Games.
 *
 * Created on 13/06/2008 12:53:08.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import core.Constants;
import core.GameMIDlet;
import core.LongMUV;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class ScoreBar extends UpdatableGroup implements Constants
{
	/** Label que exibe os pontos do jogador */
	private final Label scoreLabel;
	
	/** Pontuação atual */
	private long score;
	
	/** Pontuação que está sendo exibida no placar */
	private long scoreShown;

	/** Objeto para controlar a velocidade de atualização da pontuação */
	private final LongMUV scoreSpeed = new LongMUV();
	
	// CANCELED: Nesse jogo a pontuação não diminui
	/** Indica se os pontos estão subindo ou descendo. Auxiliar de animação */
	//private boolean scoreGoingUp;
	
	/** Distância do label dos pontos para as bordas da barra */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte LABEL_DIST_FROM_BORDER = 2;
	//#else
//# 		private final byte LABEL_DIST_FROM_BORDER = 1;
	//#endif
	
	/** Sombra que fica abaixo do label dos pontos */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte LABEL_SHADOW_HEIGHT = 1;
	//#else
//# 		private final byte LABEL_SHADOW_HEIGHT = 1;
	//#endif
	
	/** Espaçamento entre os caracteres da fonte de pontos */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		private final byte FONT_POINTS_CHAR_OFFSET = 0;
	//#else
//# 		private final byte FONT_POINTS_CHAR_OFFSET = -1;
	//#endif
	
	/** Quantos números possuímos na pontuação máxima */
	private final byte nNumbers = ( byte ) String.valueOf( MAX_SCORE ).length();
	
	//<editor-fold defaultstate="collapsed" desc="Índice dos elementos do grupo">
	
	public static final byte INDEX_SCORE_LABEL	= 0;
	
	//</editor-fold>
	
	/** Construtor */
	public ScoreBar() throws Exception
	{
		super( 4 );
		
		scoreLabel = new Label( GameMIDlet.getFont( FONT_POINTS ), GameMIDlet.formatNumStr( 0, nNumbers, '0', '.' ) );
		insertDrawable( scoreLabel );
		
		// Determina o tamanho do grupo e das patterns
		final int groupWidth = scoreLabel.getWidth() + ( LABEL_DIST_FROM_BORDER << 1 );
		final int bkgHeight = scoreLabel.getHeight() + ( LABEL_DIST_FROM_BORDER << 1 );
		setSize( groupWidth, bkgHeight + LABEL_SHADOW_HEIGHT );

		// Posiciona os elementos da coleção
		scoreLabel.setPosition( LABEL_DIST_FROM_BORDER, LABEL_DIST_FROM_BORDER );

	}
	
	public void changeScore( long points )
	{
		// Estourou a precisão, pois nesse jogo a pontuação nunca diminui
		if( points < 0 )
			return;
		
		// Estourou a precisão, pois nesse jogo a pontuação nunca diminui
		final long nextScore = score + points;
		if( nextScore < score )
		{
			score = MAX_SCORE;
		}
		else
		{
			score = nextScore;

			if( score > MAX_SCORE )
				score = MAX_SCORE;
			else if( score < 0 )
				score = 0;
		}

		final long diff = score - scoreShown;
		
		// Opa! Houve um erro...
		if( diff < 0 )
			return;
		
		scoreSpeed.setSpeed( diff, false );
	}

	public void update( int delta )
	{
		// Atualiza a pontuação
		updateScore( delta, false );
	}


    public long getScore()
	{
		return score;
	}
	
	/**
	 * Atualiza o label da pontuação
	 * @param delta Tempo decorrido desde a última chama ao método
	 * @param changeNow Indica se a pontuação mostrada deve ser automaticamente igualada à pontuação real
	 */
	public final void updateScore( int delta, boolean changeNow )
	{
		if( scoreShown != score )
		{
			if( changeNow )
			{
				scoreShown = score;
			}
			else
			{
				final long dp = scoreSpeed.update( delta );
				
				// Opa! Houve um erro...
				if( dp < 0 )
				{
					scoreShown = score;
				}
				else
				{
					final long nextScoreShown = scoreShown + dp;
					
					// CANCELED: Nesse jogo a pontuação não diminui
					//if( ( scoreGoingUp && ( scoreShown > score ) ) || ( !scoreGoingUp && ( scoreShown < score ) ) )
					if( ( nextScoreShown < scoreShown ) || ( nextScoreShown >= score ) )
						scoreShown = score;
					else
						scoreShown = nextScoreShown;
				}
			}
			scoreLabel.setText( GameMIDlet.formatNumStr( scoreShown, nNumbers, '0', '.' ) );
		}
	}
}
