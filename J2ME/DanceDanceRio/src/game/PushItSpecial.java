/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import core.Constants;
import core.GameMIDlet;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Screen;
import screens.PlayScreen;

/**
 *
 * @author Edson
 */
public final class PushItSpecial extends UpdatableGroup implements Constants, KeyListener {

    // protected DrawableGroup pushRoulette;
    /** Caixas que informam o que o usuário deve apertar */
    public final DrawableImage star,  bomb,  specialX4;
    private final Pattern bkg;
    public final MUV speedY;
    protected int velY;
    private final int TOP;
    public static short index;

	
    public PushItSpecial() throws Exception {
        super(4);

        velY = (PushItBar.getInstance().pushIt2.getHeight() * -8 / 3 );
        speedY = new MUV(velY);
        bkg = new Pattern(0x000000);
        star = new DrawableImage(ROOT_DIR + "s.png");
        bomb = new DrawableImage(ROOT_DIR + "bomba.png");
        specialX4 = new DrawableImage(ROOT_DIR + "x4.png");

        specialX4.setSize(star.getSize());
        bkg.setSize(star.getSize());

        bomb.setPosition(0, star.getPosY() + star.getHeight());
        specialX4.setPosition(0, bomb.getPosY() + bomb.getHeight());

        setSize(star.getWidth(), star.getHeight());
        TOP = getPosY() - (2 * getHeight());

        insertDrawable(bkg);
        insertDrawable(bomb);
        insertDrawable(specialX4);
        insertDrawable(star);
    }
	

    public final void update(int delta) {
        super.update(delta);
        setIndexImage();
        PushIt.setIndexRoulette(index);
		PushItBar.setIndexRoulette( index);

        final int diffY = speedY.updateInt(delta);
        star.move(0, diffY);
        specialX4.move(0, diffY);
        bomb.move(0, diffY);

        if (star.getPosY() < TOP) {
            star.setPosition(0, specialX4.getPosY() + getHeight());
        }
        if (bomb.getPosY() < TOP) {
            bomb.setPosition(0, star.getPosY() + getHeight());
        }
        if (specialX4.getPosY() < TOP) {
            specialX4.setPosition(0, bomb.getPosY() + getHeight());
        }
    }

	
   public final void setIndexImage() {
		if (star.getPosY() < getPosY() + getHeight() / 2 &&
				star.getPosY() + star.getHeight() > getPosY() + getHeight() / 2) {
			index = INDEX_STAR;
		}

		if (bomb.getPosY() < getPosY() + getHeight() / 2 &&
				bomb.getPosY() + bomb.getHeight() > getPosY() + getHeight() / 2) {
			index = INDEX_BOMB;
		}

		if (specialX4.getPosY() < getPosY() + getHeight() / 2 &&
				specialX4.getPosY() + specialX4.getHeight() > getPosY() + getHeight() / 2) {
			index = INDEX_BONUS;
		}
	}

   
    public final void keyPressed(int key) {
       
       PushIt.setIndexRoulette( index);
	   PushItBar.setIndexRoulette( index);
       
    }

	
    public final void keyReleased(int key) {
    }
}
