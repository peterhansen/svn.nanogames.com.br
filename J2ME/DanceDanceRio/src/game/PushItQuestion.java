/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import core.Constants;

/**
 *
 * @author Edson
 */
public class PushItQuestion extends UpdatableGroup implements Constants {

    private final Sprite question;

    public PushItQuestion() throws Exception {
        super(4);
        question = new Sprite(ROOT_DIR + "q");
        question.setSequence(0);
        question.setSize(question.getSize());
        insertDrawable(question);
        setSize(question.getSize());

    }
}

