/**
 * PushItBar.java
 * ©2008 Nano Games.
 *
 * Created on 04/06/2008 16:57:07.
 */

package game;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.GameMIDlet;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class PushItBar extends UpdatableGroup implements KeyListener, Constants, SpriteListener
{
	
	private static PushItBar instance;

	/** Caixas que informam o que o usuário deve apertar */
	public final DrawableImage pushIt2, pushIt4, pushIt6, pushIt8, pushItStar;

    /* */
    public final DrawableGroup pushRoulette;

	/** Rastros das caixas */
	public final DrawableImage trail2, trail4, trail6, trail8;
	
	/** Quanto do rastro fica por de trás da caixa */
	//#if   SCREEN_SIZE == "BIG"
		public static final byte PUSHIT_TRAIL_OFFSET = 3;
		private static final byte PUSHIT_DIST_FROM_BAR = 6;
        private final byte aimX = 25;
        private final byte aimY = 3;
        private final byte shineY = 8;
        private final byte shineX = 25;
	//#elif   SCREEN_SIZE == "SUPER_BIG"
//# 		public static final byte PUSHIT_TRAIL_OFFSET = 3;
//# 		private static final byte PUSHIT_DIST_FROM_BAR = 6;
//# 		private final byte aimX = 25;
//# 		private final byte aimY = 3;
//# 		private final byte shineY = 22;
//# 		private final byte shineX = 25;
	//#elif  SCREEN_SIZE == "MEDIUM"
//# 		public static final byte PUSHIT_TRAIL_OFFSET = 3;
//# 		private static final byte PUSHIT_DIST_FROM_BAR = 6;
//# 		private final byte aimX = 25;
//# 		private final byte aimY = 3;
//# 		private final byte shineY = 6;
//# 		private final byte shineX = 25;
	//#elif  SCREEN_SIZE == "SMALL"
//#
//# 		public static final byte PUSHIT_TRAIL_OFFSET = 2;
//# 		private static final byte PUSHIT_DIST_FROM_BAR = 6;
//# 		private final byte aimX = 20;
//#         private final byte aimY = 3;
//#         private final byte shineY = 4;
//#         private final byte shineX = 20;
	//#endif
	
	/** Número de pushIts na fase */
	private short nLevelPushIts;
	
	/** Número de pushIts acertáveis ao longo da fase */
	private short nHittablePushIts;
	
	/** Número de pushIts que já saíram da tela*/
	private short nInactivePushIts;

	/** Controla a animação dos pushIts */
	private final MUV pushItSpeedControl = new MUV();
	
	/** Mira */
	public static  Sprite aim;
	
	/** Brilho exibido quando o usuário tenta acertar um pushIt */
	private final Sprite shine;
	
	/** Objeto que receberá informações sobre a barra ao longo do jogo */
	private PushItBarListener listener;
	
	/** Tempo já decorrido do bônus da estrela */
	private int starBonusCounter;
	
	/** Tempo de duração do bônus da estrela */
	private int starBonusTime;
	
	/** Velocidade de movimentação dos pushIts com o bônus da estrela */
	private int starBonusSpeed;
	
	/** Velocidade de movimentação dos pushIts antes do bônus da estrela */
	private int speedBeforeStarBonus;
	
	/** PushIt que está sendo consumido no momento */
	private int currPushIt = -1;
	
	/** Controla de quanto em quanto tempo fornecemos pontos quando o jogador está acertando um rastro */
	private short trailCounter;
	
	/** Àrea de colisão da mira com os pushIts */
	private final Rectangle aimCollisionArea;
	
	//<editor-fold defaultstate="collapsed" desc="Índice dos elementos do grupo">
	
	public static final byte INDEX_FIRST_PUSHIT	= 0;
	public static final short INDEX_SHINE		= INDEX_FIRST_PUSHIT + MAX_PUSHITS;
	public static final short INDEX_AIM			= INDEX_SHINE + 1;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Tipos de acerto que podem ocorrer">
	
	public static final byte PUSHIT_HIT_KEEP_TRAIL	= -2;
	public static final byte PUSHIT_HIT_MISS		= -1;
	public static final byte PUSHIT_HIT_PERFECT		=  0;
	public static final byte PUSHIT_HIT_GREAT		=  1;
	public static final byte PUSHIT_HIT_GOOD		=  2;
	public static final byte PUSHIT_HIT_AVERAGE		=  3;
	public static final byte PUSHIT_HIT_BAD			=  4;
	
	public static final byte PUSHIT_N_TRUE_HIT_TYPES =  5;
	
	public static final byte PUSHIT_N_HIT_TYPES =  6;
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Versões da barra de pushIts">
	
	public static final byte PUSHIT_BAR_VERSION_FULL	= 0;
	public static final byte PUSHIT_BAR_VERSION_HELP	= 1;
    private static short index;
	
	//</editor-fold>
	
	
	/** Construtor */
	public PushItBar( PushItBarListener listener ) throws Exception
	{
		super( MAX_PUSHITS + 6 );
		
		instance = this;
		
		this.listener = listener;

        pushRoulette = new DrawableGroup(6);
		// Imagens originais
		final String PATH_2 = ROOT_DIR + "2" + IMG_EXT;
		pushIt2 = new DrawableImage( PATH_2 );

 		final String PATH_4 = ROOT_DIR + "4" + IMG_EXT;
		pushIt4 = new DrawableImage( PATH_4 );
		
		final String PATH_6 = ROOT_DIR + "6" + IMG_EXT;
		pushIt6 = new DrawableImage( PATH_6 );
		
		final String PATH_8 = ROOT_DIR + "8" + IMG_EXT;
		pushIt8 = new DrawableImage( PATH_8 );
		
		final String PATH_STAR = ROOT_DIR + "s" + IMG_EXT;
		pushItStar = new DrawableImage( PATH_STAR );
		
		// Rastros
	    String PATH_TRAIL_4 = ROOT_DIR+ "rastro4.png";
        String PATH_TRAIL_2 = ROOT_DIR+ "rastro2.png";

        trail2 = new DrawableImage(PATH_TRAIL_2);
		trail4 = new DrawableImage(PATH_TRAIL_4 );

		trail8 = new DrawableImage( ROOT_DIR+"rastro8.png" );
        trail6 = new DrawableImage( ROOT_DIR+"rastro6.png" );
		
		// Aloca e posiciona os elementos
		final int groupHeight = build( this, pushIt2, PUSHIT_BAR_VERSION_FULL );
		
		shine = ( Sprite )drawables[ INDEX_SHINE ];
		aim = ( Sprite )drawables[ INDEX_AIM ];
		
		aim.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		aim.setPosition( aim.getPosX() + aimX, aim.getPosY() );
		shine.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );

		//#if SCREEN_SIZE =="MEDIUM"
//# 		aim.setPosition(aim.getPosX(), aim.getPosY() - aimY );
		//#endif

		shine.setRefPixelPosition( aim.getRefPixelPosition() );

		final int pushItWidth = pushIt2.getWidth();
		final int aimAreaX = aim.getPosX() + ( ( aim.getWidth() - pushItWidth ) >> 1 );
		aimCollisionArea = new Rectangle( aimAreaX, pushIt2.getPosY(), pushItWidth, pushIt2.getHeight() );
	}

	
	private static final int build( UpdatableGroup group, Drawable basePushIt, byte version ) throws Exception
	{
		final int groupFinalWidth = version == PUSHIT_BAR_VERSION_FULL ? ScreenManager.SCREEN_WIDTH : ScreenManager.SCREEN_HALF_WIDTH;
			
		// Cria as bordas da barra
		final int barFrameHeight = 0;
		
		// Cria o fundo da barra
		final int barBkgHeight = basePushIt.getHeight() + ( PUSHIT_DIST_FROM_BAR << 1 ); 
		
		// Cria todos os pushIts
		if( version == PUSHIT_BAR_VERSION_FULL )
		{
			for( int i = 0 ; i < MAX_PUSHITS ; ++i )
			{
                final PushIt pushIt = new PushIt();
                group.insertDrawable( pushIt );
                pushIt.setVisible( false );
			}
		}
		else
		{
			group.insertDrawable( basePushIt );
		}
		
		// Cria o brilho
		final String PATH_SHINE = ROOT_DIR + "shine";
		final Sprite shineAux = new Sprite( PATH_SHINE + SPRITE_DESC_EXT, PATH_SHINE );
		group.insertDrawable( shineAux );
		shineAux.setVisible( false );

		if( group instanceof SpriteListener )
			shineAux.setListener( ( SpriteListener )group, INDEX_SHINE );
		
		// Cria a mira
		final String PATH_AIM = ROOT_DIR + "aim";
		final Sprite aimAux = new Sprite( PATH_AIM + SPRITE_DESC_EXT, PATH_AIM );
		group.insertDrawable( aimAux );
		
		// Determina o tamanho da coleção
		final int arrowHeight = 10; //arrow.getHeight();
		final int groupHeight = barBkgHeight + barFrameHeight + arrowHeight + 10;
		group.setSize( groupFinalWidth, groupHeight );

		// Posiciona os elementos da coleção

		aimAux.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		shineAux.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		if( version == PUSHIT_BAR_VERSION_FULL )
		{
			aimAux.setPosition( groupFinalWidth - aimAux.getWidth() - basePushIt.getWidth(), arrowHeight + (( barBkgHeight - aimAux.getHeight() ) >> 1 ) );
//			shineAux.setPosition( aimAux.getPosX() + (( aimAux.getWidth() - shineAux.getWidth() ) >> 1 ), aimAux.getPosY() + (( aimAux.getHeight() - shineAux.getHeight() ) >> 1 ));
			shineAux.setRefPixelPosition( aimAux.getRefPixelPosition() );
		} else {
			aimAux.setPosition( ( groupFinalWidth - aimAux.getWidth() ) >> 1, arrowHeight + (( barBkgHeight - aimAux.getHeight() ) >> 1 ) );
//			shineAux.setPosition( ( groupFinalWidth - shineAux.getWidth() ) >> 1, aimAux.getPosY() + (( aimAux.getHeight() - shineAux.getHeight() ) >> 1 ));
			shineAux.setRefPixelPosition( aimAux.getRefPixelPosition() );
			basePushIt.setPosition( -basePushIt.getWidth(), aimAux.getPosY() + (( aimAux.getHeight() - shineAux.getHeight() ) >> 1 ) );
		}

		return groupHeight;
	}

	
	/** Deve ser passado um grupo com no mínimo 7 slots */
	public static final void createHelpVersion( UpdatableGroup group ) throws Exception
	{
		final String path = ROOT_DIR + "2";
								
		final DrawableImage basePushIt = new DrawableImage( path + IMG_EXT );
		build( group, basePushIt, PUSHIT_BAR_VERSION_HELP );
	}
	
	
	/** Libera memória */
	public final void destruct()
	{	
		instance = null;
		shine.setListener( null, 0 );
		currPushIt = -1;
		listener = null;
		System.gc();
	}
	
	
	private static final UpdatableGroup createArrow() throws Exception
	{
		final UpdatableGroup group = new UpdatableGroup( 2 );
		
		final DrawableImage arrowBkg = new DrawableImage( ROOT_DIR + "arrowBkg" + IMG_EXT );
		group.insertDrawable( arrowBkg );
		
		final String path = ROOT_DIR + "arrow";
		final Sprite arrow = new Sprite( path + SPRITE_DESC_EXT, path );
		group.insertDrawable( arrow );

		final int groupWidth = arrowBkg.getWidth(), groupHeight = arrowBkg.getHeight();
		group.setSize( groupWidth, groupHeight );
		arrow.setPosition( ( groupWidth - arrow.getWidth() ) >> 1, groupHeight - arrow.getHeight() - 1 );
        arrow.setVisible(false);
        arrowBkg.setVisible(false);
		
		return group;
	}


	/** Configura a nova fase */
	public final void onNewLevel( int level ) throws Exception {
		// A dificuldade pára de crescer depois de MAX_DIFFICULT_LEVEL
		if ( level > MAX_DIFFICULTY_LEVEL ) {
			level = MAX_DIFFICULTY_LEVEL;		// Calcula a velocidade da fase
		}
		final int percentage = ( level * 100 ) / MAX_DIFFICULTY_LEVEL;
		final int currSpeed = NanoMath.lerpInt( MIN_PUSHITS_SPEED, MAX_PUSHITS_SPEED, percentage );
		pushItSpeedControl.setSpeed( currSpeed, true );

		// Calcula quantos pushIts teremos nesta fase
		nLevelPushIts = ( short ) NanoMath.lerpInt( MIN_PUSHITS, MAX_PUSHITS, percentage );
		byte nStars = ( byte ) NanoMath.lerpInt( MIN_STAR_PUSHITS, MAX_STAR_PUSHITS, percentage );
		byte nQuestionMarks = ( byte ) NanoMath.lerpInt( MIN_QM_PUSHITS, MAX_QM_PUSHITS, percentage );

		// Esse é invertido porque queremos ir diminuindo com o tempo
		byte nNones = ( byte ) NanoMath.lerpInt( MAX_NONE_PUSHITS, MIN_QM_PUSHITS, percentage );

		// Sorteia os pushIts
		nHittablePushIts = 0;
		int currX = -( currSpeed - DEFAULT_PUSHIT_SPACEMENT ), nNonesTogether = 0;
		final int max = nLevelPushIts + INDEX_FIRST_PUSHIT,  pushItsY = aim.getPosY() + ( ( aim.getHeight() - pushIt2.getHeight() ) >> 1 );
		for ( int i = INDEX_FIRST_PUSHIT; i < max; ++i ) {
			int code;
			// o seguinte loop é necessário pois o rastro de 1,5x o PushIt ficaria sob as setinhas
			do {
				code = PushIt.PUSHIT_HALF_NONE + NanoMath.randInt( PushIt.PUSHIT_SPECIAL - PushIt.PUSHIT_HALF_NONE + 1 );
			} while ( code == PushIt.PUSHIT_ONE_N_HALF_2 || code == PushIt.PUSHIT_ONE_N_HALF_4 || code == PushIt.PUSHIT_ONE_N_HALF_6 || code == PushIt.PUSHIT_ONE_N_HALF_8 );
			//code = PushIt.PUSHIT_ROULETTE;
			
			switch ( code ) {
				case PushIt.PUSHIT_QUESTION_MARK:
					if ( nQuestionMarks > 0 ) {
						--nQuestionMarks;
					} else {
						++nHittablePushIts;
						code = PushIt.PUSHIT_2 + NanoMath.randInt( PushIt.PUSHIT_TRIPLE_8 - PushIt.PUSHIT_2 + 1 );
					}
					break;

				case PushIt.PUSHIT_STAR:
					// Não faz sentido termos uma estrela como último pushIt da fase
					if ( ( nStars > 0 ) && ( i < max - 1 ) ) {
						--nStars;
					} else {
						++nHittablePushIts;
						code = PushIt.PUSHIT_2 + NanoMath.randInt( PushIt.PUSHIT_TRIPLE_8 - PushIt.PUSHIT_2 + 1 );
					}
					break;

				case PushIt.PUSHIT_TRIPLE_NONE:
				case PushIt.PUSHIT_DOUBLE_N_HALF_NONE:
				case PushIt.PUSHIT_DOUBLE_NONE:
				case PushIt.PUSHIT_ONE_N_HALF_NONE:
				case PushIt.PUSHIT_NONE:
				case PushIt.PUSHIT_HALF_NONE:
					if ( ( nNones > 0 ) && ( nNonesTogether < ( MAX_NONE_PUSHITS_TOGETHER * 10 ) ) && ( i > INDEX_FIRST_PUSHIT ) && ( i < max - 1 ) ) {
						--nNones;
						// Finjo que 5 vale 0.5
						nNonesTogether += ( ( code + 2 ) * 5 );
					} else {
						++nHittablePushIts;
						nNonesTogether = 0;
						code = PushIt.PUSHIT_2 + NanoMath.randInt( PushIt.PUSHIT_TRIPLE_8 - PushIt.PUSHIT_2 + 1 );
					}
					break;

				default:
					++nHittablePushIts;
				// Sem break mesmo
			}

			// 70% de chance de não possuir rastro
			if ( ( code > PushIt.PUSHIT_TRIPLE_NONE ) && ( NanoMath.randInt( 100 ) < 70 ) ) {
				code = PushIt.untrailCode( code );
			}
			( ( PushIt ) drawables[i] ).setPushIt( code );
			currX -= ( drawables[i].getWidth() + DEFAULT_PUSHIT_SPACEMENT );
			drawables[i].setPosition( currX, pushItsY );
		}

		// Reinicia as variáveis de controle
		currPushIt = -1;
		trailCounter = 0;

		nInactivePushIts = 0;

		starBonusTime = 0;
		starBonusCounter = 0;
		starBonusSpeed = 0;
		speedBeforeStarBonus = -1;

		// Reseta alguns estados
		shine.setVisible( false );
	}

	
	public synchronized void keyPressed( int key )
	{
		currPushIt = -1;
		
		shine.setSequence( 0 );
		shine.setVisible( true );
		shine.pause( false );
		checkPushItHit( key );
	}

	public static void setIndexRoulette(short index){
      PushItBar.index = index;
    }

	
	protected synchronized final void checkPushItHit( int keyPressed )
	{
		try{
			//GameMIDlet.log("checkPushItHit inicio");
		// Calcula a área da mira
		final int pushItWidth = pushIt2.getWidth();

		// Um pushIt será considerado sob a mira apenas se estiver no intervalo [-AIM_DIST_ACCEPTANCE, AIM_DIST_ACCEPTANCE]
		// em relação a aimArea. Logo, sempre teremos apenas um único pushIt sob a mira
		final int max = NanoMath.min( INDEX_FIRST_PUSHIT + nInactivePushIts + 4, INDEX_FIRST_PUSHIT + nLevelPushIts );
		
		int i;
		final int minIntersection = pushItWidth - AIM_DIST_ACCEPTANCE;
		Rectangle pushItArea = null;
		
		for( i = INDEX_FIRST_PUSHIT + nInactivePushIts ; i < max ; ++i )
		{
			//GameMIDlet.log("for( i = INDEX_FIRST_PUSHIT +  ... inicio");
			// Trata os pushIts vazios e já acertados
			if( drawables[i].isVisible() && !(( PushIt )drawables[i] ).getUsed() )
			{
				pushItArea = (( PushIt )drawables[i] ).getCollisionArea();
				final Rectangle intersection = new Rectangle();
				intersection.setIntersection( aimCollisionArea, pushItArea );

				// Se houve interseção, armazena
				if( intersection.width >= minIntersection )
					break;
			}
			//GameMIDlet.log("for( i = INDEX_FIRST_PUSHIT +  ... fim");
		}
		
		// Informa se acertou alguém
		if( i < max )
		{
				//GameMIDlet.log(" if( i < max )  ... inicio");
			final PushIt aux = ( PushIt )drawables[i];

			// Acertou o pushIt e seu código
			if( aux.hit( keyPressed ) )
			{
				if( aux.hasTrail() )
				{
					trailCounter = 0;
					currPushIt = i;
				}
				else if( aux.getCode() == PushIt.PUSHIT_STAR )
				{
					setStarBonus();
					// Não chama o listener, pois não iremos mudar o estado
					//listener.onHit();
					
					return;
				}
				
                if( aux.getCode() == PushIt.PUSHIT_ROULETTE){
                      listener.onHit( PushItBarListener.ON_HIT_TRUE_ROULETTE, PUSHIT_HIT_GOOD );
                 }

            if(aux.getCode() == PushIt.PUSHIT_SPECIAL){
				if (index == INDEX_STAR) {
					setStarBonus();
					return;
				}else if (index == INDEX_BOMB) {
					listener.onHit(PushItBarListener.ON_HIT_TRUE_SPECIAL_BOMB, PUSHIT_HIT_AVERAGE);
				} else if (index == INDEX_BONUS) {
						listener.onHit(PushItBarListener.ON_HIT_TRUE_SPECIAL_BONUS, PUSHIT_HIT_GREAT);
					}
				}

				// + 1 => O zero tb conta
				final byte hitTypeInterval = ( byte )( ( pushItWidth - minIntersection + 1 )/ PUSHIT_N_TRUE_HIT_TYPES );

				int errorMargin = aimCollisionArea.x - pushItArea.x;
				if( errorMargin < 0 )
					errorMargin = -errorMargin;
                  
				listener.onHit( aux.getCode() == PushIt.PUSHIT_QUESTION_MARK ? PushItBarListener.ON_HIT_TRUE_QM : PushItBarListener.ON_HIT_TRUE, ( byte )( errorMargin / hitTypeInterval ) );
			}
			// Acertou alguém, mas errou o código
			else {
				listener.onHit(aux.getCode() == PushIt.PUSHIT_QUESTION_MARK ? PushItBarListener.ON_HIT_FALSE_QM : PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS);
				if (aux.getCode() == PushIt.PUSHIT_ROULETTE) {
					listener.onHit(aux.getCode() == PushIt.PUSHIT_ROULETTE ? PushItBarListener.ON_HIT_FALSE_ROULETTE : PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS);
				}
				MediaPlayer.vibrate(DEFAULT_VIBRATION_TIME);
			}
	//	GameMIDlet.log(" if( i < max )  ... fim");
		}
		// Não acertou ninguém
		else {

			//#if BLACKBERRY_API == "false"
						MediaPlayer.vibrate(DEFAULT_VIBRATION_TIME);
			//#endif
			//GameMIDlet.log("else  ... ");
			// Verifica se o último já passou
			final PushIt lastPushIt = ( PushIt )drawables[ INDEX_FIRST_PUSHIT + nLevelPushIts - 1 ];
			if( lastPushIt.isVisible() && ( !lastPushIt.getUsed() || ( lastPushIt.getPosX() < ScreenManager.SCREEN_WIDTH ) ) )
					listener.onHit(PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS);
			}
		}
		catch (Throwable t){


		}
	}
	
	public synchronized final void keyReleased( int key )
	{
		currPushIt = -1;
	}
	
	/** Configura o bônus da estrela */
	protected void setStarBonus()
	{
		starBonusCounter = 0;
      
		// Soma pois pode estar acumulando duas estrelas
		starBonusTime += MIN_STAR_TIME + NanoMath.randInt( MAX_STAR_TIME - MIN_STAR_TIME + 1 );
		
		// Se estamos acumulando, não sobrescreve a velocidade original
		if( speedBeforeStarBonus < 0 )
			speedBeforeStarBonus = pushItSpeedControl.getSpeed();
		
		// Verifica a velocidade a ser aplicada
		starBonusSpeed = pushItSpeedControl.getSpeed() >> 1;

		final int minSpeed = pushIt2.getWidth() >> 2;
		if( starBonusSpeed < minSpeed )
			starBonusSpeed = minSpeed;
		
		final int speedDiff = speedBeforeStarBonus - starBonusSpeed;
		if( speedDiff > 0 )
			pushItSpeedControl.setSpeed( starBonusSpeed, false );
	}

	/** Atualiza o componente */
	public final void update( int delta )
	{
        super.update(delta);
		//#if SCREEN_SIZE == "SMALL"
//# 		cancelCurrPushIt();
		//#endif
		
		// Atualiza os componentes inseridos na coleção
		superDotUpdate( delta );
		
		// Verifica se está pressionando um pushIt de rastro
		if( currPushIt >= 0 )
		{
			trailCounter += delta;
			if( trailCounter >= TRAIL_POINTS_TIME )
			{
				final int max = trailCounter / TRAIL_POINTS_TIME;
				for( int i = 0 ; i < max ; ++i )
					listener.onHit( PushItBarListener.ON_HIT_TRUE_TRAIL, PUSHIT_HIT_KEEP_TRAIL );
				trailCounter %= TRAIL_POINTS_TIME;
			}
		}
		
		// Aplica o bônus da estrela
		if( starBonusTime > 0 )
		{
			starBonusCounter += delta;
			
			int timePercentage = ( starBonusCounter * 100 ) / starBonusTime;
			if( timePercentage >= 100 )
			{
				pushItSpeedControl.setSpeed( speedBeforeStarBonus, false );

				starBonusTime = 0;
				starBonusCounter = 0;
				starBonusSpeed = 0;
				speedBeforeStarBonus = -1;
			}
			else
			{
				int speed = starBonusSpeed + ((( speedBeforeStarBonus - starBonusSpeed ) * timePercentage ) / 100 );
				if( speed > speedBeforeStarBonus )
					speed = speedBeforeStarBonus;

				pushItSpeedControl.setSpeed( speed, false );
			}
		}

		// Move os pushIts
		final int dx = pushItSpeedControl.updateInt( delta );
		if( dx == 0 )
			return;

		final int max = nLevelPushIts + INDEX_FIRST_PUSHIT;
		for( int i = INDEX_FIRST_PUSHIT + nInactivePushIts; i < max ; ++i )
		{
			if( currPushIt == i )
			{
				if( !(( PushIt )drawables[i] ).reduceTrail( dx, aimCollisionArea.x ) )
				{
					currPushIt = -1;
					setNoShine();
				}
			}
			else
			{
				final PushIt aux = ( PushIt )drawables[i];
				aux.move( dx, 0 );
				
				// Verifica se o jogador perdeu um pushIt
				if( aux.isVisible()
					&& !aux.getUsed()
					&& ( aux.getCode() >= PushIt.PUSHIT_2 )
					&& ( aux.getCode() < PushIt.PUSHIT_QUESTION_MARK )
					&& ( aux.getCollisionArea().x >= aimCollisionArea.x + aimCollisionArea.width ) )
				{
					(( PushIt )drawables[i] ).setUsed();
					listener.onHit( PushItBarListener.ON_HIT_FALSE, PUSHIT_HIT_MISS );

                    currPushIt = -1;
				}
			
				if( aux.getPosX() >= ScreenManager.SCREEN_WIDTH )
				{
					aux.setVisible( false );
					++nInactivePushIts;

					if( nInactivePushIts == nLevelPushIts )
					{
						listener.onLevelEnded();
						return;
					}
				}
			}

		}
	
    }

	
	public short getNHitablePushIts()
	{
		return nHittablePushIts;
	}
	
	/** Otimiza o processo de atualização dos pushIts */
	private void superDotUpdate( int delta )
	{
		// CANCELED : PushIts não são updatables
		// Calcula quantos pushIts cabem na tela e acrescenta uma folga de 3
//		final short nPushItsOnScreen = ( short )( ( ScreenManager.SCREEN_WIDTH / pushIt2.getWidth() ) + 3 );
//		final short min = ( short )( INDEX_FIRST_PUSHIT + nInactivePushIts );
//		final short max = ( short )NanoMath.min( min + nPushItsOnScreen, INDEX_SHINE );
//		for( int i = min; i < max ; ++i )
//			(( PushIt )drawables[i] ).update( delta );
		
		// Atualiza os outros updatables inseridos no grupo
		shine.update( delta );
		aim.update( delta );
	}

	public synchronized void onSequenceEnded( int id, int sequence )
	{
		//#if SCREEN_SIZE == "SMALL"
//# 		// Tenta corrigir bug que só ocorre no Nokia 6822
//# 		if( currPushIt >= 0 )
//# 		{
//# 			cancelCurrPushIt();
//# 		}
//# 		else
//# 		{
//# 			currPushIt = -1;
//# 			setNoShine();
//# 		}
		//#else
		if( currPushIt < 0 )
			setNoShine();
		//#endif
	}
	
	//#if SCREEN_SIZE == "SMALL"
//# 	/** Tenta corrigir um bug que só ocorre no Nokia 6822 */
//# 	private synchronized void cancelCurrPushIt()
//# 	{
//# 		if( currPushIt < 0 )
//# 			return;
//#
//# 		if( ( currPushIt < INDEX_FIRST_PUSHIT + nInactivePushIts )
//# 				|| ( currPushIt >= INDEX_FIRST_PUSHIT + nLevelPushIts )
//# 				|| ( !drawables[ currPushIt ].isVisible() )
//# 				|| ( drawables[ currPushIt ].getPosX() > aimCollisionArea.x + aimCollisionArea.width )
//# 				|| ( drawables[ currPushIt ].getPosX() + drawables[ currPushIt ].getWidth() < aimCollisionArea.x )
//# 				// Não pode fazer este teste, pq "used" é setado no 1o hit!!!
//# 				//|| ( (( PushIt )drawables[ currPushIt ]).getUsed() )
//# 				|| ( !(( PushIt )drawables[ currPushIt ]).hasTrail() )
//# 			  )
//# 		{
//# 			currPushIt = -1;
//# 			setNoShine();
//# 		}
//# 	}
	//#endif

	
	private void setNoShine()
	{
		shine.pause( true );
		shine.setVisible( false );
	}

	
	public void onFrameChanged( int id, int frameSequenceIndex )
	{
	}
	
	
	public static final PushItBar getInstance() {
		return instance;
	}


	/** Otimiza o métdo de desenho */
	protected final void paint( Graphics g )
	{
		/** Otimiza o redesenho dos pushIts */
		int firstOnScreen = INDEX_FIRST_PUSHIT + nInactivePushIts;
		final int lastOnScreen = NanoMath.min( firstOnScreen + 1 + ( ScreenManager.SCREEN_WIDTH / ( pushIt2.getWidth() + DEFAULT_PUSHIT_SPACEMENT )), INDEX_FIRST_PUSHIT + nLevelPushIts - 1 );
		for( ; firstOnScreen <= lastOnScreen ; ++firstOnScreen )
		{
			if( drawables[ firstOnScreen ].isVisible() ) // Impede o desvio de chamada de função
				drawables[ firstOnScreen ].draw( g );
		}
		
		for( int i = INDEX_SHINE ; i <= INDEX_AIM ; ++i )
			drawables[i].draw( g );
	}
	
	
	public static final void removeListener() {
		instance.listener = null;
	}
	
}
