/**
 * ResultScreen.java
 * 
 * Created on 23/Nov/2009, 11:24:09
 *
 */
package screens;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import core.GameMIDlet;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif


/**
 *
 * @author Peter
 */
public final class ResultScreen extends UpdatableGroup implements Constants, KeyListener, ScreenListener //#if TOUCH == "true" , PointerListener //#endif
		
{

	private final Label[] labelsTitles = new Label[ PLAY_FEEDBACK_TYPES_TOTAL + 1 ];
	private final Label[] labelsScores = new Label[ PLAY_FEEDBACK_TYPES_TOTAL + 1 ];
	private final short[] results = new short[ PLAY_FEEDBACK_TYPES_TOTAL + 1 ];
	private static final byte STATE_NONE = -1;
	private static final byte STATE_SORT_PERFECT = STATE_NONE + 1;
	private static final byte STATE_SORT_GREAT = STATE_SORT_PERFECT + 1;
	private static final byte STATE_SORT_GOOD = STATE_SORT_GREAT + 1;
	private static final byte STATE_SORT_AVERAGE = STATE_SORT_GOOD + 1;
	private static final byte STATE_SORT_BAD = STATE_SORT_AVERAGE + 1;
	private static final byte STATE_SORT_MISSES = STATE_SORT_BAD + 1;
	private static final byte STATE_SORT_TOTAL = STATE_SORT_MISSES + 1;
	private static final byte STATE_SHOW_ALL = STATE_SORT_TOTAL + 1;
	private byte state;

	/*private final ImageFont fontMin ;
	private final Label textMin;
	private final Label valMin;
	private byte min;*/
	private static final short TIME_SORT = 630;
	private static final short TIME_WAIT = 1000;
	private short stateTime;
	private final byte LABEL_TOTAL_X_OFFSET;


	public ResultScreen() throws Exception {
		super( PLAY_FEEDBACK_TYPES_TOTAL * 5 );

		final ImageFont[] fonts = new ImageFont[ PLAY_FEEDBACK_TYPES_TOTAL + 1 ];
		for ( byte i = 0; i < fonts.length; ++i ) {
			fonts[i] = GameMIDlet.getFont( FONT_PERFECT + i );
		}
		final ImageFont fontPoints = GameMIDlet.getFont( FONT_POINTS );

		for ( byte i = 0; i < fonts.length; ++i ) {
			labelsTitles[i] = new Label( fonts[i], TEXT_MOVE_PERFECT + i );
			insertDrawable( labelsTitles[i] );

			labelsScores[i] = new Label( i == fonts.length - 1 ? fonts[fonts.length - 1] : fontPoints, null );
			insertDrawable( labelsScores[i] );
		}

		/*fontMin = GameMIDlet.getFont(FONT_TEXT_WHITE);
		textMin = new Label(fontMin, TEXT_MINUMUM);
		valMin = new Label(fontMin, "");
		insertDrawable(textMin);
		insertDrawable(valMin);*/

		LABEL_TOTAL_X_OFFSET = ( byte ) -labelsScores[labelsScores.length - 1].getFont().getTextWidth( "%" );




		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}


	private final String getScoreString( int score ) {
		if ( score < 10 ) {
			return "0" + String.valueOf( score );
		}

		return String.valueOf( score );
	}


	public final void increaseCounter( int playResultType ) {
		++results[playResultType];
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		stateTime = 0;

		switch ( state ) {
			case STATE_SORT_GREAT:
			case STATE_SORT_GOOD:
			case STATE_SORT_AVERAGE:
			case STATE_SORT_BAD:
			case STATE_SORT_MISSES:
			case STATE_SORT_TOTAL:
				labelsScores[state - 1].setText( getScoreString( results[state - 1] ) );
				break;

			case STATE_SHOW_ALL:
				final Label l = labelsScores[STATE_SORT_TOTAL];
				l.setText( getScoreString( getScorePercent() ) + '%' );
				l.setPosition( ( ( getWidth() * 3 ) >> 2 ) + labelsScores[state - 1].getWidth() - l.getWidth() + LABEL_TOTAL_X_OFFSET, l.getPosY() );
				break;
		}
	}


	public final void reset() {
		setState( STATE_SORT_PERFECT );
		for ( byte i = 0; i < results.length; ++i ) {
			results[i] = 0;
			labelsScores[i].setText( null );
		}
	//valMin.setVisible(false);
	}


	public final void update( int delta ) {
		switch ( state ) {
			case STATE_SORT_PERFECT:
			case STATE_SORT_GREAT:
			case STATE_SORT_GOOD:
			case STATE_SORT_AVERAGE:
			case STATE_SORT_BAD:
			case STATE_SORT_MISSES:
				stateTime += delta;
				labelsScores[state - STATE_SORT_PERFECT].setText( getScoreString( NanoMath.randInt( 100 ) ) );

				if ( stateTime >= TIME_SORT ) {
					setState( state + 1 );
				}
				break;

			case STATE_SORT_TOTAL:
				stateTime += delta;
				labelsScores[state - STATE_SORT_PERFECT].setText( getScoreString( NanoMath.randInt( 100 ) ) );
				labelsScores[state].setPosition( ( ( getWidth() * 3 ) >> 2 ) + labelsScores[state - 1].getWidth() - labelsScores[state].getWidth(), labelsScores[state].getPosY() );

				if ( stateTime >= TIME_SORT ) {
					setState( state + 1 );
				}
				break;

			case STATE_SHOW_ALL:
				if ( stateTime < TIME_WAIT ) {
					stateTime += delta;
				}
				break;
		}
	}


	public final int getScorePercent() {
		final short total = getTotalHits();

		final short[] scoreValues = new short[]{
			SCORE_PERFECT,
			SCORE_GREAT,
			SCORE_GOOD,
			SCORE_AVERAGE,
			SCORE_BAD,
			SCORE_ERROR, };

		int result = 0;
		for ( byte i = 0; i < scoreValues.length; ++i ) {
			result += results[i] * scoreValues[i];
		}
		//valMin.setVisible(true);
		return Math.max( 0, result * 100 / ( SCORE_PERFECT * total ) );
	}


	public void setMin( int min ) {
		//this.min = (byte) min;
	}


	public final short getTotalHits() {
		short total = 0;

		for ( byte i = 0; i < results.length; ++i ) {
			total += results[i];
		}
		//valMin.setText(String.valueOf(min + "%"));


		return total;
	}


	public final void setSize( int width, int height ) {
		setPosition( 0, 0 );
		//#if SCREEN_SIZE== "MEDIUM"
//# 			final int X_TITLE = width >> 4;
//# 			final int X_SCORE = ( width * 3 ) >> 2;
//#
		//#else
		final int X_TITLE = width >> 3;
		final int X_SCORE = ( width * 3 ) >> 2;
		//#endif

		int y = ( labelsTitles[ 0].getHeight() >> 1 ) + ( RESULTS_ITEMS_SPACING >> 1 );
		for ( byte i = 0; i < labelsTitles.length; ++i ) {
			labelsTitles[i].setPosition( X_TITLE, y - ( labelsTitles[i].getHeight() >> 1 ) );
			labelsScores[i].setPosition( X_SCORE - labelsScores[i].getWidth(), y - ( labelsScores[i].getHeight() >> 1 ) );
			y += labelsTitles[i].getHeight() + RESULTS_ITEMS_SPACING;
		}
		//textMin.setPosition(X_TITLE, labelsTitles[ PLAY_FEEDBACK_TYPES_TOTAL-1 ].getPosY() + labelsTitles[ PLAY_FEEDBACK_TYPES_TOTAL-1 ].getHeight() + 2);
		//valMin.setPosition(X_SCORE, textMin.getPosY());
		//	labelsTitles[ PLAY_FEEDBACK_TYPES_TOTAL].setPosition(X_TITLE, textMin.getPosY()+textMin.getHeight());
		labelsScores[PLAY_FEEDBACK_TYPES_TOTAL].setPosition( X_SCORE, labelsTitles[PLAY_FEEDBACK_TYPES_TOTAL].getPosY() );


		super.setSize( width, y + 5 );
		setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - getHeight() ) >> 1 );
	}


	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_SORT_PERFECT:
			case STATE_SORT_GREAT:
			case STATE_SORT_GOOD:
			case STATE_SORT_AVERAGE:
			case STATE_SORT_BAD:
			case STATE_SORT_MISSES:
			case STATE_SORT_TOTAL:
				while ( state < STATE_SHOW_ALL ) {
					setState( state + 1 );
				}
				break;

			case STATE_SHOW_ALL:
				if ( stateTime >= TIME_WAIT ) {
					GameMIDlet.setScreen( SCREEN_NEXT_LEVEL );
				}
				break;
		}
	}


	public final void keyReleased( int key ) {
	}


	//#if TOUCH == "true"
	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}

	//#endif

	public final void hideNotify() {
	}


	public final void showNotify() {
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			setSize( width, height );
		}
	}


}
