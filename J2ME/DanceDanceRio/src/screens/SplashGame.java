/**
 * SplashGame.java
 * ©2007 Nano Games
 */

package screens;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GameMIDlet;
import javax.microedition.lcdui.Graphics;

/**
 * @author Daniel L. Alves
 */

public final class SplashGame extends UpdatableGroup implements Constants, KeyListener
	//#if TOUCH == "true"
		// Não há aparelhos com limite de jar menor que 100 kb que suportam ponteiros (reduz o tamanho do código)
		, PointerListener
	//#endif
{
	private static final int COLOR_SKY_LIGHT = 0xfff605;
	private static final int COLOR_SKY_DARK = 0xfd9500;
	
	/** Quantos triângulos teremos rodando no background. OBS: 360 % N_TRIANGLES DEVE SER IGUAL A ZERO */
	private final byte N_TRIANGLES = 30;
	
	/** Ajudam a calcular os vetores na animação de girar o background */
	private final Point vecCalculator1 = new Point(), vecCalculator2 = new Point();
	
	/** Módulo utilizado por <code>vecCalculator1</code> e <code>vecCalculator2</code> no cálculo dos vetores */
	private final int vecModule;
	
	/** Ângulo a partir do qual começamos a calcular os triângulos */
	private short startAngle;
	
	/** Em quantos graus incrementamos <code>startAngle</code> a cada etapa da animação */
	private final byte START_ANGLE_INC = 5;
	
	/** Velocidade de animação do background */
	private final short BKG_ANIM_TIME = 125;
	
	/** Controla o incremento de ângulos da animação de girar o background ao longo do tempo */
	private final MUV animControl = new MUV();
	
	/** Controla a movimentação do label de "Pressione Qualquer Tecla" */
	private final MUV pressAnyKeyAnimControl = new MUV();

	private final Label pressAnyKeyLabel;

	private final DrawableImage logoDancin;
	private final DrawableImage logoRio;
	private final Sprite pandeiro;
	
	private final DrawableImage palmLeft;
	private final DrawableImage palmRight;
	private final Pattern archs;
	private final Pattern crowd;
	private final BasicAnimatedPattern bkgBottom;
	//#if SCREEN_SIZE =="SUPER_BIG"
//# 	private final Pattern sombra;
	//#endif

	/** Melhora o posicionamento do logo */
	//#if SCREEN_SIZE == "SMALL"
//# 		private final byte LOGO_OFFSET_X = 2;
	//#else
		private final byte LOGO_OFFSET_X = 0;
	//#endif
		
	private static final short TIME_PANDEIRO	= 2600;
	private static final short TIME_DANCIN		= 600;
	private static final short TIME_RIO			= 600;
		
	private static final byte STATE_NONE			= 0;
	private static final byte STATE_PANDEIRO_UP		= STATE_NONE + 1;
	private static final byte STATE_DANCIN_ENTERS	= STATE_PANDEIRO_UP + 1;
	private static final byte STATE_RIO_ENTERS		= STATE_DANCIN_ENTERS + 1;
	private static final byte STATE_PRESS_ANY_KEY	= STATE_RIO_ENTERS + 1;

	private byte state;
	
	private short stateTime;
	
		
	/** Construtor */
	public SplashGame( ImageFont font ) throws Exception
	{
		// Configura o grupo
		super( 10 );
		
		// Carrega o logo
		final String PATH_SPLASH_GAME = ROOT_DIR + "SplashGame/";
		
		pandeiro = new Sprite( PATH_SPLASH_GAME + "pandeiro" );
		pandeiro.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		insertDrawable( pandeiro );
		
		logoDancin = new DrawableImage( PATH_SPLASH_GAME + "d.png" );
		logoDancin.setPosition( -1000, -1000 );
		insertDrawable( logoDancin );
		logoRio = new DrawableImage( PATH_SPLASH_GAME + "r.png" );
		logoRio.setPosition( -1000, -1000 );
		insertDrawable( logoRio );
		
		bkgBottom = BorderedScreen.getAnimatedBkg();
		bkgBottom.setAnimation( BasicAnimatedPattern.ANIMATION_NONE );
		insertDrawable( bkgBottom );
		
		archs = new Pattern( BkgLapa.getArcos() );
		insertDrawable( archs );
		
		palmLeft = BkgLapa.getPalm();
		palmLeft.mirror( TRANS_MIRROR_H );
		insertDrawable( palmLeft );
		
		palmRight = BkgLapa.getPalm();
		insertDrawable( palmRight );
		
		crowd = new Pattern( BkgLapa.getCrowd() );
		insertDrawable( crowd );
		//#if SCREEN_SIZE =="SUPER_BIG"
//# 		sombra = new Pattern(new Sprite(PATH_SPLASH_GAME + "sombra"));
//# 		insertDrawable(sombra);
		//#endif
		
		pressAnyKeyLabel = new Label( font, AppMIDlet.getText( TEXT_PRESS_ANY_KEY ) );
		insertDrawable( pressAnyKeyLabel );

		// Calcula a maior dimensão da tela
		vecModule = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) << 1;
		
		// Calcula a velocidade da animação
		animControl.setSpeed( ( 1000 / BKG_ANIM_TIME ) * START_ANGLE_INC );
		pressAnyKeyAnimControl.setSpeed( ScreenManager.SCREEN_WIDTH >> 2 );

		

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		setState( STATE_PANDEIRO_UP );
	}
	
	
	private final void setState( int state ) {
		this.state = ( byte ) state;
		stateTime = 0;
		
		switch ( state ) {
			case STATE_PANDEIRO_UP:
				pandeiro.setRefPixelPosition( getWidth() >> 1, getHeight() );
			break;
			
			case STATE_DANCIN_ENTERS:
			break;
			
			case STATE_RIO_ENTERS:
			break;
			
			case STATE_PRESS_ANY_KEY:
			break;
		}
	}

	
	public final void update( int delta ) {
		super.update( delta );
		// Controla a animação de girar o background
		startAngle = ( short )( ( startAngle + animControl.updateInt( delta ) ) % 360 );
		
		stateTime += delta;
		
		switch ( state ) {
			case STATE_PANDEIRO_UP:
				pandeiro.setRefPixelPosition( getWidth() >> 1, NanoMath.lerpInt( getHeight(), getHeight() >> 2, stateTime * 100 / TIME_PANDEIRO ) );
				if ( stateTime >= TIME_PANDEIRO )
					setState( STATE_DANCIN_ENTERS );
			break;
			
			case STATE_DANCIN_ENTERS:
				//#if SCREEN_SIZE =="BIG" || SCREEN_SIZE =="SMALL"  || SCREEN_SIZE =="SUPER_BIG"
				logoDancin.setPosition( NanoMath.lerpInt( -logoDancin.getWidth(), ( getWidth() >> 1 ) + SPLASH_DANCIN_X_OFFSET, stateTime * 100 / TIME_DANCIN ), pandeiro.getPosY() );
				//#elif SCREEN_SIZE =="MEDIUM"
//# 				logoDancin.setPosition( NanoMath.lerpInt( -logoDancin.getWidth(), ( getWidth() >> 1 ) + SPLASH_DANCIN_X_OFFSET, stateTime * 100 / TIME_DANCIN )+ 8, pandeiro.getPosY() );
				//#endif
				if ( stateTime >= TIME_DANCIN )
					setState( STATE_RIO_ENTERS );
			break;
			
			case STATE_RIO_ENTERS:
				logoRio.setPosition( NanoMath.lerpInt( getWidth(), ( getWidth() >> 1 ) + SPLASH_RIO_X_OFFSET, stateTime * 100 / TIME_DANCIN ), pandeiro.getPosY() );
				if ( stateTime >= TIME_RIO )
					setState( STATE_PRESS_ANY_KEY );
			break;
			
			case STATE_PRESS_ANY_KEY:
				// Move o label de pressione qualquer tecla
				final int pressAnyKeyLabelEnd = pressAnyKeyLabel.getPosX() + pressAnyKeyLabel.getWidth();
				if( pressAnyKeyLabelEnd < 0 )
					pressAnyKeyLabel.setPosition( ScreenManager.SCREEN_WIDTH, pressAnyKeyLabel.getPosY() );
				else
					pressAnyKeyLabel.move( -pressAnyKeyAnimControl.updateInt( delta ), 0 );
			break;
		}
		
	}

	
	protected final void paint( Graphics g )
	{
		// Desenha o background. Ao invés de desenharmos triângulos alternando cores, pintamos toda a tela de
		// uma cor e depois os triângulos na outra cor. Assim otimizamos o método, já que o número de chamadas
		// a Graphics.fillTriangle cairá pela metade
		g.setColor( COLOR_SKY_LIGHT );
		g.fillRect( 0, 0, ScreenManager.SCREEN_WIDTH, bkgBottom.getPosY() );
		
		
		final int INC = 360 / N_TRIANGLES;
		final int DOUBLE_INC = INC << 1;
		final short max = ( short )( 360 + startAngle );
		final int POS_Y = pandeiro.getRefPixelY();
		
		for( short i = startAngle ; i < max ; i += DOUBLE_INC )
		{
			vecCalculator1.setVector( i, vecModule );
			vecCalculator1.addEquals( ScreenManager.SCREEN_HALF_WIDTH, POS_Y );
			
			vecCalculator2.setVector( i + INC, vecModule );
			vecCalculator2.addEquals( ScreenManager.SCREEN_HALF_WIDTH, POS_Y );
			
			g.setColor( COLOR_SKY_DARK );
			g.fillTriangle( ScreenManager.SCREEN_HALF_WIDTH, POS_Y, vecCalculator1.x, vecCalculator1.y, vecCalculator2.x, vecCalculator2.y );

			vecCalculator2.setVector( i + ( INC >> 2 ), vecModule );
			vecCalculator2.addEquals( ScreenManager.SCREEN_HALF_WIDTH, POS_Y );
			g.setColor( 0xffcc00 );
			g.fillTriangle( ScreenManager.SCREEN_HALF_WIDTH, POS_Y, vecCalculator1.x, vecCalculator1.y, vecCalculator2.x, vecCalculator2.y );
		}
		
		// Desenha as imagens
		super.paint( g );
	}

	
	public final void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.exit();
				break;

			default:
			//#if DEBUG == "true"
				callNextScreen();
			//#else
//# 				if ( state == STATE_PRESS_ANY_KEY )
//# 					callNextScreen();
			//#endif
		}
	}

	
	public final void keyReleased( int key )
	{
	}

	
	private final void callNextScreen()
	{
		AppMIDlet.setScreen( GameMIDlet.SCREEN_MAIN_MENU );
	}

	
	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y )
		{
		}

		public final void onPointerPressed( int x, int y )
		{
			keyPressed( 0 );
		}

		public final void onPointerReleased( int x, int y )
		{
		}
	//#endif


	public final void setSize( int width, int height ) {
		super.setSize( width, height );



		pressAnyKeyLabel.setPosition(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT - pressAnyKeyLabel.getHeight());
		//#if SCREEN_SIZE =="SUPER_BIG"
//# 		if (width < height) {
//# 			archs.setSize(getWidth(), archs.getFill().getHeight());
//# 			archs.setPosition(0, getHeight() >> 1);
//# 		} else {
//# 			int ajustach = 220;
//# 			archs.setSize(getWidth(), archs.getFill().getHeight());
//# 			archs.setPosition(0, getHeight() - ajustach);
//# 
//# 		}
		//#else
		archs.setSize(getWidth(), archs.getFill().getHeight());
		archs.setPosition(0, getHeight() >> 1);
		//#endif
		crowd.setSize(getWidth(), crowd.getFill().getHeight());
		crowd.setPosition(0, archs.getPosY() + (archs.getHeight() >> 1));

		palmLeft.setPosition(0, crowd.getPosY() + crowd.getHeight() - palmLeft.getHeight());
		palmRight.setPosition(getWidth() - palmRight.getWidth(), palmLeft.getPosY());

		bkgBottom.setPosition(0, archs.getPosY() + archs.getHeight());
		bkgBottom.setSize(getWidth(), getHeight() - bkgBottom.getPosY());

			//#if SCREEN_SIZE =="SUPER_BIG"
//# 		sombra.setSize(width, sombra.getHeight());
//# 		sombra.setPosition(-3, crowd.getPosY()+crowd.getHeight() - 2);
//# 
		//#endif

	}
}
