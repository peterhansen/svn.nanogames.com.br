/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GameMIDlet;

/**
 *
 * @author Edson
 */
//PETSEMATARY
public class BkgSematary extends UpdatableGroup implements Constants ,ScreenListener {

	private final Pattern bkgSky;
	private final Pattern bkgSkyTop;
	private final Pattern bkgBottom;
	private final Pattern muroEsquerda;
	private final DrawableImage portao;
	private final Pattern muroDireita;
	private final Pattern chao;
	private final DrawableImage arvore;

	private final DrawableImage cristo;
	private final DrawableImage morro;
	private final DrawableImage propaganda;
	private final Drawable predio;
	private final Drawable morcego;
	private final Drawable morcego2;
	private final Drawable morcego3;
	private final Drawable morcegoPlaca;
	private final Pattern monsters;
	private final DrawableImage lapide;
	public final MUV speedX;
	public final MUV speedY;
	private BezierCurve bezier;
	private Point posInicial;
	private Point posControle1;
	private Point posControle2;
	private Point posFinal;
	private final byte DIRECTION_1 = 1;
	private final byte DIRECTION_2 = 2;
	private byte direction = DIRECTION_1;
	private int timeBat = 0;
	private final int MAX_TIME = 10000;
	// pontos que receberão os valores dos pontos finais de cada movimento para serem os iniciais do proximo movimento
	private Point auxPosInicial1;
	private Point auxPosInicial2;


	//#if SCREEN_SIZE =="SUPER_BIG"
//# 	private final DrawableImage nuvem;
//# 	private  DrawableImage nuvem2;
//# 	private  DrawableImage nuvem3;
//# 	public final MUV speedXNuv;
//# 	private final Drawable morcegoPlaca2;
//# 	private Pattern auxChão ;
//# 	private  Pattern auxsky ;
	//#endif

	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM" || SCREEN_SIZE == "SUPER_BIG"
	private final DrawableImage lapide2;
	//#endif
	private static final int COLOR_SKY_DARK = 0x040837;
	private static final int COLOR_SKY_BOTTOM = 0x040837;


	BkgSematary() throws Exception {
		super(26);
		speedX = new MUV(15);
		speedY = new MUV(16);
		bkgSkyTop = new Pattern(COLOR_SKY_DARK);
		bkgBottom = new Pattern(COLOR_SKY_BOTTOM);

		if (GameMIDlet.isLowMemory()) {
			bkgSky = new Pattern(COLOR_SKY_DARK);
		} else {
			bkgSky = new Pattern(new DrawableImage(PATH_BKG + "pattern_sky.png"));
		}

		//#if SCREEN_SIZE == "SMALL"
//# 		if (GameMIDlet.isLowMemory()) {
//# 			chao = new Pattern(0x674515);
//# 		} else {
//# 			chao = new Pattern(new DrawableImage(PATH_BKG + "chao.png"));
//# 		}
		//#else
		chao = new Pattern(new DrawableImage(PATH_BKG + "chao.png"));

		//#endif
		muroEsquerda = new Pattern(new DrawableImage(PATH_BKG + "muro.png"));
		muroDireita = new Pattern(new DrawableImage(PATH_BKG + "muro.png"));
		portao = new DrawableImage(PATH_BKG + "portao.png");
	
		//#if SCREEN_SIZE == "SMALL"
//# 		if (GameMIDlet.isLowMemory()) {
//# 			cristo = null;
//# 			arvore = null;
//# 		} else {
//# 			cristo = new DrawableImage(PATH_BKG + "cristo.png");
//# 			arvore = new DrawableImage(PATH_BKG + "arvoreCemiterio.png");
//# 		}
		//#else
			cristo = new DrawableImage(PATH_BKG + "cristo.png");
			arvore = new DrawableImage(PATH_BKG + "arvoreCemiterio.png");
		//#endif
		if (!GameMIDlet.isLowMemory()) {
			morro = new DrawableImage(PATH_BKG + "morro.png");
		} else {
			morro = null;
		}
		bezier = new BezierCurve();
		if (!GameMIDlet.isLowMemory()) {
			propaganda = new DrawableImage(PATH_BKG + "placaNano.png");
		} else {
			propaganda = null;
		}

		//#if SCREEN_SIZE == "SMALL"
//# 		if (GameMIDlet.isLowMemory()) {
//# 			predio = null;
//# 		} else {
//# 			predio =  getBuilding();
//# 		}
		//#else
			predio = getBuilding();
		//#endif


		//#if SCREEN_SIZE == "SMALL"
//# 		if (GameMIDlet.isLowMemory()) {
//# 			morcego = null;
//# 			morcego2 = null;
//# 			morcego3 = null;
//# 		}else{
//# 			morcego = getBat();
//# 			morcego2 = getBat();
//# 			morcego3 = getBat();}
		//#else
			morcego = getBat();
			morcego2 = getBat();
			morcego3 = getBat();
		//#endif

		if (!GameMIDlet.isLowMemory()) {
			morcegoPlaca = new Sprite(PATH_BKG + "morcego");
		} else {
			morcegoPlaca = null;
		}
		//#if SCREEN_SIZE != "BIG"
//# 		if (!GameMIDlet.isLowMemory()) {
//# 			monsters = new Pattern(new Sprite(PATH_BKG + "monstros"));
//# 		} else {
//# 			monsters = new Pattern(new DrawableImage(PATH_BKG + "monstros_0.png"));
//# 		}
		//#else
		monsters = new Pattern(new Sprite(PATH_BKG + "monstros"));
		//#endif

		//#if SCREEN_SIZE == "SMALL"
//# 		if (GameMIDlet.isLowMemory()) {
//# 			lapide = null;
//# 		} else {
//# 			lapide = new DrawableImage(PATH_BKG + "lapide.png");
//# 		}
		//#else
				lapide = new DrawableImage(PATH_BKG + "lapide.png");
		//#endif

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM" || SCREEN_SIZE == "SUPER_BIG"
		if (!GameMIDlet.isLowMemory()) {
			lapide2 = new DrawableImage(PATH_BKG + "lapide2.png");
		} else {
			lapide2 = null;
		}
		//#endif


		
		insertDrawable(bkgSky);
		insertDrawable(bkgSkyTop);
		insertDrawable(bkgSky);
		//#if SCREEN_SIZE=="SUPER_BIG"
//# 		auxsky = new Pattern(0x27318d);
//# 		insertDrawable(auxsky);
		//#endif

		if (cristo != null) {
			insertDrawable(cristo);

		}
		insertDrawable(chao);

		
		if (morro != null) {
			insertDrawable(morro);
		}

		//#if SCREEN_SIZE == "SUPER_BIG"
//# 		nuvem = new DrawableImage(PATH_BKG + "nuvem1.png");
//# 		nuvem2 = new DrawableImage(PATH_BKG + "nuvem1.png");
//# 		nuvem3 = new DrawableImage(PATH_BKG + "nuvem1.png");
//# 		insertDrawable(nuvem);
//# 		insertDrawable(nuvem2);
//# 		insertDrawable(nuvem3);
//# 		morcegoPlaca2 = new Sprite(PATH_BKG + "morcego");
//# 		speedXNuv = new MUV( 5 );
//# 
		//#endif

		

		if (predio != null) {
			insertDrawable(predio);
		}
		insertDrawable(muroEsquerda);
		insertDrawable(muroDireita);
		insertDrawable(portao);
		insertDrawable(monsters);

		if (arvore != null) {
			insertDrawable(arvore);
		}
		if (morcego != null) {
			insertDrawable(morcego);
		}
		if (morcego2 != null) {
			insertDrawable(morcego2);
		}
		if (morcego3 != null) {
			insertDrawable(morcego3);
		}
		if (lapide != null) {
			insertDrawable(lapide);
		}

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM" || SCREEN_SIZE == "SUPER_BIG"
		if (lapide2 != null) {
			insertDrawable(lapide2);
		}
		//#endif
		if (!GameMIDlet.isLowMemory()) {
			insertDrawable(morcegoPlaca);
			//#if SCREEN_SIZE =="SUPER_BIG"
//# 			insertDrawable(morcegoPlaca2);
			//#endif
			insertDrawable(propaganda);
			setPosBat(DIRECTION_1);
		}

	}

	private static final Drawable getBuilding() throws Exception {

		if (GameMIDlet.isLowMemory()) {
			return new DrawableImage(PATH_BKG + "predioxx_0.png");
		}

		return new Sprite(PATH_BKG + "predioxx");
	}

	private static final Drawable getBat() throws Exception {

		if (GameMIDlet.isLowMemory()) {
			return new DrawableImage(PATH_BKG + "morcego_0.png");
		}

		return new Sprite(PATH_BKG + "morcego");
	}

	public static final Drawable getMonsters() throws Exception {

		if (GameMIDlet.isLowMemory()) {
			return new DrawableImage(PATH_BKG + "monstros_0.png");
		}
		return new Sprite(PATH_BKG + "monstros");

	}

	public final void setPosBat(int pos) {
		direction = (byte) pos;
//#if SCREEN_SIZE!= "SUPER_BIG"
		switch (pos) {
			case DIRECTION_1:
				if (posInicial == null) {
					posInicial = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
				} else {
					posInicial = auxPosInicial2;
				}
				posControle1 = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
				posControle2 = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
				posFinal = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
				auxPosInicial1 = posFinal;
				propaganda.setPosition(posInicial);
				morcegoPlaca.setPosition(propaganda.getPosX() + propaganda.getWidth() / 2 - morcegoPlaca.getWidth() / 2, propaganda.getPosY() - morcegoPlaca.getHeight());
				bezier.setValues(posInicial, posControle1, posControle2, posFinal);
				break;


			case DIRECTION_2:
				posInicial = auxPosInicial1;
				posControle1 = new Point(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH / 2), NanoMath.randInt(2 * ScreenManager.SCREEN_HALF_HEIGHT));
				posControle2 = new Point(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH / 2), NanoMath.randInt(ScreenManager.SCREEN_HEIGHT));
				posFinal = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
				auxPosInicial2 = posFinal;
				propaganda.setPosition(posInicial);
				morcegoPlaca.setPosition(propaganda.getPosX() + propaganda.getWidth() / 2 - morcegoPlaca.getWidth() / 2, propaganda.getPosY() - morcegoPlaca.getHeight());
				bezier.setValues(posInicial, posControle1, posControle2, posFinal);

				break;
		}
		//#else
//# 		switch (pos) {
//# 			case DIRECTION_1:
//# 				if (posInicial == null) {
//# 					posInicial = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
//# 				} else {
//# 					posInicial = auxPosInicial2;
//# 				}
//# 				posControle1 = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
//# 				posControle2 = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
//# 				posFinal = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
//# 				auxPosInicial1 = posFinal;
//# 				propaganda.setPosition(posInicial);
//# 				morcegoPlaca.setPosition(propaganda.getPosX() , propaganda.getPosY() - morcegoPlaca.getHeight());
//# 				morcegoPlaca2.setPosition(propaganda.getPosX()+propaganda.getWidth() - morcegoPlaca2.getWidth() , propaganda.getPosY() - morcegoPlaca2.getHeight());
//# 				bezier.setValues(posInicial, posControle1, posControle2, posFinal);
//# 				break;
//# 
//# 
//# 			case DIRECTION_2:
//# 				posInicial = auxPosInicial1;
//# 				posControle1 = new Point(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH / 2), NanoMath.randInt(2 * ScreenManager.SCREEN_HALF_HEIGHT));
//# 				posControle2 = new Point(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH / 2), NanoMath.randInt(ScreenManager.SCREEN_HEIGHT));
//# 				posFinal = new Point(NanoMath.randInt(-ScreenManager.SCREEN_WIDTH), NanoMath.randInt(-ScreenManager.SCREEN_HALF_HEIGHT));
//# 				auxPosInicial2 = posFinal;
//# 				propaganda.setPosition(posInicial);
//# 				morcegoPlaca.setPosition(propaganda.getPosX() , propaganda.getPosY() - morcegoPlaca.getHeight());
//# 				morcegoPlaca2.setPosition(propaganda.getPosX()+propaganda.getWidth() - morcegoPlaca2.getWidth() , propaganda.getPosY() - morcegoPlaca2.getHeight());
//# 				bezier.setValues(posInicial, posControle1, posControle2, posFinal);
//# 
//# 				break;
//# 		}
		//#endif
	}

	public final void update(int delta) {
		super.update(delta);
		switch (direction) {
			case DIRECTION_1:
				Point refPos = new Point();
				timeBat += delta;

				bezier.getPointAtFixed(refPos, NanoMath.divInt(timeBat, MAX_TIME));
				if (propaganda != null && morcegoPlaca != null) {
					propaganda.setRefPixelPosition(refPos);
					//#if SCREEN_SIZE!= "SUPER_BIG"
					morcegoPlaca.setPosition(propaganda.getPosX() + propaganda.getWidth() / 2 - morcegoPlaca.getWidth() / 2, propaganda.getPosY() - morcegoPlaca.getHeight());
					//#else
//# 					morcegoPlaca.setPosition(propaganda.getPosX(), propaganda.getPosY() - morcegoPlaca.getHeight());
//# 					morcegoPlaca2.setPosition(propaganda.getPosX() + propaganda.getWidth() - morcegoPlaca2.getWidth(), propaganda.getPosY() - morcegoPlaca2.getHeight());
					//#endif
					if (timeBat > MAX_TIME) {
						timeBat = 0;
						setPosBat(DIRECTION_2);
					}
				}
				break;
			case DIRECTION_2:
				refPos = new Point();
				timeBat += delta;
				bezier.getPointAtFixed(refPos, NanoMath.divInt(timeBat, MAX_TIME));
				if (propaganda != null && morcegoPlaca != null) {
					propaganda.setRefPixelPosition(refPos);
					//#if SCREEN_SIZE!= "SUPER_BIG"
					morcegoPlaca.setPosition(propaganda.getPosX() + propaganda.getWidth() / 2 - morcegoPlaca.getWidth() / 2, propaganda.getPosY() - morcegoPlaca.getHeight());
					//#else
//# 					morcegoPlaca.setPosition(propaganda.getPosX(), propaganda.getPosY() - morcegoPlaca.getHeight());
//# 					morcegoPlaca2.setPosition(propaganda.getPosX() + propaganda.getWidth() - morcegoPlaca2.getWidth(), propaganda.getPosY() - morcegoPlaca2.getHeight());
					//#endif
					if (timeBat > MAX_TIME) {
						timeBat = 0;
						setPosBat(DIRECTION_1);
					}
					break;
				}
			}
			
			//#if SCREEN_SIZE =="SUPER_BIG"
//# 			final int diffNuv = speedXNuv.updateInt(delta);
//# 			nuvem.move(diffNuv, 0);
//# 			if (nuvem.getPosX() > ScreenManager.SCREEN_WIDTH) {
//# 				nuvem.setPosition(-nuvem.getWidth(), nuvem.getPosY());
//# 			}
//# 			nuvem2.move(diffNuv, 0);
//# 			nuvem3.move(diffNuv, 0);
//# 			if (nuvem3.getPosX() > ScreenManager.SCREEN_WIDTH) {
//# 				nuvem3.setPosition(-nuvem3.getWidth(), nuvem3.getPosY());
//# 			}
//# 			if (nuvem3.getPosX() > ScreenManager.SCREEN_WIDTH) {
//# 				nuvem2.setPosition(-nuvem2.getWidth(), nuvem2.getPosY());
//# 			}
		//#endif
	}

	
	public final void setSize(int width, int height) {
		super.setSize(width, height);

		final byte arvoteAjust;
		final byte morcegoAjust;
		final byte morcego2Ajust;
		final byte lapideAjust;
		final byte morroAjust;
		final byte predioAjust;
		byte ajustMuro;


		chao.setSize( width, chao.getHeight() );
		//#if SCREEN_SIZE == "SMALL"
//# 		if(GameMIDlet.isLowMemory())
//# 	    chao.setSize( width,45 );
		//#endif


		//#if SCREEN_SIZE == "SUPER_BIG"
//# 			chao.setPosition( 0, getHeight() - chao.getHeight() + Math.max( ( 480 - height ) / 3, 0 ) );
		//#else
			chao.setPosition( 0, getHeight() - chao.getHeight() );
		//#endif
			
		muroEsquerda.setSize( ( width - portao.getWidth() ) >> 1, muroEsquerda.getHeight() );
		muroDireita.setSize( muroEsquerda.getSize() );
		muroEsquerda.setPosition( 0, chao.getPosY() - muroEsquerda.getHeight() );
		muroDireita.setPosition( getWidth() - muroDireita.getWidth(), muroEsquerda.getPosY() );
		portao.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_BOTTOM );
		portao.setRefPixelPosition( width >> 1, muroEsquerda.getPosY() + muroEsquerda.getHeight() );
		if(arvore!=null)
		arvore.setPosition(0, chao.getPosY() + chao.getHeight()/2 - arvore.getHeight());
		if (predio != null) {
			predio.setPosition(portao.getPosX(), chao.getPosY() - predio.getHeight());
		}
		bkgSky.setSize( width, bkgSky.getHeight() );
		if(cristo != null)
		cristo.setPosition(width-cristo.getWidth(), Math.max( 0, chao.getPosY() - cristo.getHeight() ) );

		//#if SCREEN_SIZE == "SUPER_BIG"
//# 			predio.setPosition(portao.getPosX()+portao.getWidth(), muroDireita.getPosY()- predio.getHeight() +  predio.getHeight()/2);
		//#else
			bkgSky.setPosition( 0, muroEsquerda.getPosY() - bkgSky.getHeight() );
		//#endif

		//#if SCREEN_SIZE == "BIG"
		if (width < height) {

			morcegoAjust = 30;
			morcego2Ajust = 2;
			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth(), arvore.getPosY() + morcegoAjust);
			morcego2.setPosition(morcego.getPosX() - morcego.getWidth(), arvore.getPosY() + morcego2.getHeight() / 2 - morcego2Ajust);
			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2 - morcego2Ajust);


			morroAjust = 10;
			lapideAjust = 10;
			if (morro != null) {
				morro.setPosition(-morroAjust, chao.getPosY() - morro.getHeight());
			}
			monsters.setSize(width, monsters.getHeight());
			monsters.setPosition(0, chao.getPosY() - monsters.getHeight() / 2);
			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);
			if (lapide2 != null) {
				lapide2.setPosition(0, height - lapide2.getHeight() - lapideAjust);
			}
		} else {

			morcegoAjust = 30;
			morcego2Ajust = 2;
			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth(), arvore.getPosY() + morcegoAjust);
			morcego2.setPosition(morcego.getPosX() - morcego.getWidth(), arvore.getPosY() + morcego2.getHeight() / 2 - morcego2Ajust);

			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2 - morcego2Ajust);

            if (morro != null) {
				morro.setPosition(0, chao.getPosY() - morro.getHeight());
			}
			predioAjust = 10;
			predio.setPosition(portao.getPosX() - predioAjust, chao.getPosY() - predio.getHeight());

			monsters.setSize(width, monsters.getHeight());
			monsters.setPosition(0, chao.getPosY() - monsters.getHeight() / 2);

			lapideAjust = 10;
			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);
			if (lapide2 != null) {
				lapide2.setPosition(lapide2.getWidth() / 4, height - lapide2.getHeight() - lapideAjust);
			}
		}

	//#elif SCREEN_SIZE == "SUPER_BIG"
//# 		if (width < height) {
//# 
//# 			morcegoAjust = 50;
//# 			morcego2Ajust = 10;
//# 			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth(), arvore.getPosY() + morcegoAjust);
//# 			morcego2.setPosition(morcego.getPosX() - morcego.getWidth()-15, arvore.getPosY() + morcego2.getHeight() / 2+5 );
//# 			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2 + morcego2Ajust);
//# 			morroAjust = 50;
//# 			lapideAjust = 10;
//# 			morro.setPosition(0, chao.getPosY() - morro.getHeight() -morroAjust );
//# 			if(height<600){
//# 				morro.setPosition(-10, chao.getPosY() - morro.getHeight() );
//# 
//# 			}
//# 			monsters.setSize(width, monsters.getHeight());
//# 			monsters.setPosition(0, chao.getPosY() - monsters.getHeight() / 2 - 15);
//# 			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);
//# 			if (lapide2 != null) {
//# 				lapide2.setPosition(0, height - lapide2.getHeight() - lapideAjust);
//# 			}
//# 
//# 			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
//# 			nuvem2.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
//# 			nuvem3.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
//# 
//# 			if(height > 600){
//# 				auxsky.setSize(width,muroDireita.getHeight());
//# 				auxsky.setPosition(0, bkgSky.getPosY()+bkgSky.getHeight()-3);
//# 				morro.setPosition(0, chao.getPosY() - morro.getHeight());
//# 			}
//# 
//# 
//# 		} else {
//# 
//# 			morcegoAjust = 50;
//# 			morcego2Ajust = 10;
//# 			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth(), arvore.getPosY() + morcegoAjust);
//# 			morcego2.setPosition(morcego.getPosX() - morcego.getWidth()-15, arvore.getPosY() + morcego2.getHeight() / 2+5 );
//# 			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2 + morcego2Ajust);
//# 			morro.setPosition(0, chao.getPosY() - morro.getHeight());
//# 			monsters.setSize(width, monsters.getHeight());
//# 			monsters.setPosition(0, chao.getPosY() - monsters.getHeight() +30);
//# 			lapideAjust = 10;
//# 			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);
//# 			lapide2.setPosition(lapide2.getWidth() / 4, height - lapide2.getHeight() - lapideAjust);
//# 
//# 			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT/2));
//# 			nuvem2.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT)/2);
//# 			nuvem3.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT)/2);
//# 		}
//# 
//# 
	//#elif SCREEN_SIZE == "MEDIUM"
//# 		if (width < height) {
//# 			morcegoAjust = 25;
//# 			morcego2Ajust = 5;
//# 			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth(), arvore.getPosY() + morcegoAjust);
//# 			morcego2.setPosition(morcego.getPosX() - morcego.getWidth() + morcego2Ajust, arvore.getPosY() + morcego2.getHeight() / 2 - morcego2Ajust);
//# 			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2 - morcego2Ajust);
//# 			morroAjust = 10;
//# 			lapideAjust = 5;
//# 			if (morro != null) {
//# 				morro.setPosition(-morroAjust, chao.getPosY() - morro.getHeight());
//# 			}
//# 			monsters.setSize(width, monsters.getHeight());
//# 			monsters.setPosition(0, chao.getPosY() - monsters.getHeight() / 2 - 10);
//# 			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);
//# 			if (lapide2 != null) {
//# 				lapide2.setPosition(0, height - lapide2.getHeight() - lapideAjust);
//# 			}
//# 		} else {
//#
//# 			morcegoAjust = 30;
//# 			morcego2Ajust = 2;
//# 			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth(), arvore.getPosY() + morcegoAjust);
//# 			morcego2.setPosition(morcego.getPosX() - morcego.getWidth(), arvore.getPosY() + morcego2.getHeight() / 2 - morcego2Ajust);
//# 			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2 - morcego2Ajust);
//#           if (morro != null) {
//# 				morro.setPosition(0, chao.getPosY() - morro.getHeight());}
//# 			predioAjust = 10;
//# 			predio.setPosition(portao.getPosX() - predioAjust, chao.getPosY() - predio.getHeight());
//#
//#
//# 			monsters.setSize(width, monsters.getHeight());
//# 			monsters.setPosition(0, chao.getPosY() - monsters.getHeight() / 2);
//#
//# 			lapideAjust = 7;
//# 			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);
//# 			if (lapide2 != null) {
//# 				lapide2.setPosition(lapide2.getWidth() / 4, height - lapide2.getHeight() - lapideAjust);
//# 			}
//# 		}
//#
//#
//#
	//#elif SCREEN_SIZE == "SMALL"
//# 			arvoteAjust = 20;
//# 			if (arvore != null) {
//# 			arvore.setPosition(0, chao.getPosY() - arvore.getHeight() / 2 - arvoteAjust);
//# 		}
//# 		morcegoAjust = 15;
//# 		morcego2Ajust = 5;
//# 		if (morcego != null) {
//# 			morcego.setPosition(arvore.getPosX() + arvore.getWidth() - morcego.getWidth() / 2, arvore.getPosY() + morcegoAjust);
//# 		}
//# 		if (morcego2 != null) {
//# 			morcego2.setPosition(morcego.getPosX() - morcego.getWidth() + morcego2Ajust, arvore.getPosY() + morcego2.getHeight() / 2 - morcego2Ajust);
//# 		}
//# 		if (morcego3 != null) {
//# 			morcego3.setPosition(arvore.getPosX(), arvore.getPosY() + 2 * morcego2.getHeight() - morcego2.getHeight() / 2);
//# 		}
//# 		if (cristo != null) {
//# 			cristo.setPosition(ScreenManager.SCREEN_WIDTH - cristo.getWidth(), muroDireita.getPosY() - cristo.getHeight());
//# 		}
//# 
//# 			morroAjust = 10;
//# 			lapideAjust = 5;
//# 			if (morro != null) {
//# 			morro.setPosition(-morroAjust, chao.getPosY() - morro.getHeight());
//# 		}
//# 			if(!GameMIDlet.isLowMemory())
//# 			predio.setPosition(portao.getPosX() , chao.getPosY() - predio.getHeight());
//# 
//# 
//# 		byte monternAjust = 15;
//# 		monsters.setSize(width, monsters.getHeight());
//# 		monsters.setPosition(0, chao.getPosY() - monsters.getHeight() + monternAjust);
//# 		if (lapide != null) {
//# 			lapide.setPosition(width - lapide.getWidth(), height - lapide.getHeight() - lapideAjust);}
//# 
	//#endif

//		// pode ser necessário preencher o topo da tela com uma cor sólida
	    bkgSkyTop.setSize(width, bkgSky.getPosY());
		bkgBottom.setPosition(0, bkgSky.getPosY()-bkgBottom.getHeight());
		bkgBottom.setSize(width, bkgBottom.getPosY() );
	}

	public final void hideNotify() {
	}

	public final void showNotify() {
	}

	public final void sizeChanged(int width, int height) {
		if ( width != getWidth() || height != getHeight() ){
			setSize(width, height);
		}
	}
}
