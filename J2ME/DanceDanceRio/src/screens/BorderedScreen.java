package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.GameMIDlet;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif
import javax.microedition.lcdui.Graphics;


public final class BorderedScreen extends UpdatableGroup implements KeyListener, Constants, ScreenListener
//#if TOUCH == "true"
	, PointerListener
//#endif
{

	private static final short TIME_TRANSITION = 500;

	public static final byte STATE_HIDDEN = 0;

	public static final byte STATE_APPEARING = 1;

	public static final byte STATE_SHOWN = 2;

	public static final byte STATE_HIDING = 3;

	private byte state;

	private short accTime;

	private KeyListener keyListener;

	//#if TOUCH == "true"
		private PointerListener pointerListener;
	//#endif

	private final BasicAnimatedPattern bkgTop;

	private final BasicAnimatedPattern bkgBottom;

	private final Pattern borderTop;

	private final Pattern borderBottom;

	private final Pattern fill;

	private final Rectangle contentViewport = new Rectangle();

	private final Rectangle bottomViewport = new Rectangle();

	private Drawable screen;

	private Drawable nextScreen;

	private boolean setScreenSize;

	private boolean bkgVisible = true;
	
	private static DrawableImage PATTERN_TOP;
	

	public BorderedScreen() throws Exception {
		super( 16 );

		borderTop = new Pattern( new DrawableImage( PATH_BKG + "pattern_tira.png" ) );
		borderTop.getFill().mirror( TRANS_MIRROR_V );
		borderBottom = new Pattern( new DrawableImage( ( DrawableImage ) borderTop.getFill() ) );

		bkgTop = getAnimatedBkg();
		bkgBottom = getAnimatedBkg();
		insertDrawable( bkgTop );
		insertDrawable( bkgBottom );

		bkgBottom.setViewport( bottomViewport );
		fill = new Pattern( new DrawableImage( PATH_BKG + "patternfundomenu.png" ) );
		fill.setViewport( contentViewport );
		insertDrawable( bkgTop );
		insertDrawable( bkgBottom );
		insertDrawable( fill );
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		insertDrawable( borderTop );
		insertDrawable( borderBottom );

		setState( STATE_HIDDEN );
	}
	
	
	public static final DrawableImage getPatternTop() throws Exception {
		if ( PATTERN_TOP == null )
			PATTERN_TOP = new DrawableImage( PATH_IMAGES + "Bkg/pattern_menu.png" );
		
		return new DrawableImage( PATTERN_TOP );
	}
	
	
	public static final BasicAnimatedPattern getAnimatedBkg() throws Exception {
		final BasicAnimatedPattern bkg = new BasicAnimatedPattern( getPatternTop(), ScreenManager.SCREEN_HALF_WIDTH / 5, 0 );
		bkg.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		
		return bkg;
	}


	public final void setNextScreen( Drawable nextScreen, boolean setScreenSize ) {
		this.nextScreen = nextScreen;
		this.setScreenSize = setScreenSize;
		
		setState( STATE_HIDING );
	}


	private final void switchScreens( boolean destroy ) {
		final Point borderSize = detectSize();

		if ( destroy && screen instanceof Form ) {
			( ( Form ) screen ).destroy();
		}
		removeDrawable( screen );
		screen = nextScreen;

		//#ifndef NO_RECOMMEND
			if ( !( screen instanceof Form ) && ( screen instanceof Container ) ) {
				try {
					final Form f = new Form( ( Container ) screen );
					screen = f;
				} catch ( Exception ex ) {
					//#if DEBUG == "true"
						ex.printStackTrace();
					//#endif
				}
			}
		//#endif
		
		if ( screen instanceof KeyListener ) {
			keyListener = ( KeyListener ) screen;
		} else {
			keyListener = null;
		}		

		//#if TOUCH == "true"
			if ( screen instanceof PointerListener )
				pointerListener = ( PointerListener ) screen;
			else
				pointerListener = null;
		//#endif

		if ( nextScreen != null ) {
			insertDrawable( nextScreen );
			//#ifdef NO_RECOMMEND
			//# 				handleKeys = true;
			//# 				if ( setScreenSize )
			//# 					screen.setSize( borderSize.x - ( BORDER_THICKNESS << 1 ) - BORDER_HALF_THICKNESS, borderSize.y - BORDER_THICKNESS );
			//# 				screen.setPosition( diff.x + ( BORDER_THICKNESS << 1 ), diff.y + BORDER_HALF_THICKNESS );
			//#else
				if ( screen instanceof Form ) {
					// gambiarra para tratar corretamente o scroll
					if ( setScreenSize ) {
						screen.setSize( borderSize.x - ( Constants.BORDER_THICKNESS << 1 ), borderSize.y - Constants.BORDER_THICKNESS );
					}
	//				screen.setPosition( diff.x + Constants.BORDER_THICKNESS, diff.y + Constants.BORDER_HALF_THICKNESS );
				} else {
					if ( setScreenSize ) {
						screen.setSize( borderSize.x - ( Constants.BORDER_THICKNESS << 1 ) - Constants.BORDER_HALF_THICKNESS, borderSize.y - Constants.BORDER_THICKNESS );
					}
	//				screen.setPosition( diff.x + ( Constants.BORDER_THICKNESS << 1 ), diff.y + Constants.BORDER_HALF_THICKNESS );
				}
			//#endif

			screen.setPosition( ( getWidth() - screen.getWidth() ) >> 1, ( getHeight() - screen.getHeight() ) >> 1 );
			screen.setViewport( contentViewport );
			setState( STATE_APPEARING );
		}

		nextScreen = null;
		
		ScreenManager.setKeyListener( this );
		ScreenManager.setScreenListener( this );
		
		//#if TOUCH == "true"
			ScreenManager.setPointerListener( this );
		//#endif
	}


	public final void setNextScreen( int textIndex, int nextScreen ) throws Exception {
		setNextScreen( textIndex, nextScreen, false );
	}


	public final void setNextScreen( int textIndex, int nextScreen, boolean autoScroll ) throws Exception {
		setNextScreen( loadFormText( AppMIDlet.getText( textIndex ), autoScroll ), true );
	}


	public final void setNextScreen( String text, int nextScreen, boolean autoScroll ) throws Exception {
		setNextScreen( loadFormText( text, autoScroll ), true );
	}


	public final void setNextScreen( String text, int nextScreen, boolean autoScroll, Drawable[] specialPieces ) throws Exception {
		setNextScreen( loadFormText( text, autoScroll, specialPieces ), true );
	}


	public static final FormText loadFormText( String text, boolean autoScroll ) throws Exception {
		return loadFormText( text, autoScroll, null );
	}


	public static final FormText loadFormText( String text, boolean autoScroll, Drawable[] specialChars ) throws Exception {
		final Point size = detectSize();
		final FormText textScreen = new FormText( AppMIDlet.getFont( Constants.FONT_TEXT_WHITE ), size.x, specialChars ) {

			public final void keyPressed( int key ) {
				switch ( key ) {
					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_SOFT_LEFT:
					case ScreenManager.KEY_SOFT_MID:
						GameMIDlet.setScreen( Constants.SCREEN_MAIN_MENU );
						break;
						
					default:
						super.keyPressed( key );
				}
			}
		};
		if ( autoScroll ) {
			textScreen.setAutoScroll( true );
			textScreen.setSize( size.x - ( Constants.BORDER_THICKNESS << 1 ), size.y - Constants.BORDER_THICKNESS );
			textScreen.setAutoScroll( true );
		} else {
			final ScrollBar scrollBar = GameMIDlet.getScrollBar();
			textScreen.setSize( size.x - Math.max( scrollBar.getWidth(), scrollBar.getWidth() ), size.y - Constants.BORDER_THICKNESS );
			textScreen.setScrollBarV( scrollBar );
		}
		textScreen.setText( text );
		return textScreen;
	}


	private static final Point detectSize() {
		//#if SCREEN_SIZE == "SMALL"
//# 				if ( ScreenManager.SCREEN_HEIGHT < 128 )
//# 					return new Point( ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
//# 				else
//# 					return new Point( ScreenManager.SCREEN_WIDTH <= ScreenManager.SCREEN_HEIGHT ? ScreenManager.SCREEN_WIDTH - ( BORDER_THICKNESS << 1 ) : ( ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2 ), ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
		//#else
			return new Point( ScreenManager.SCREEN_WIDTH, ( ScreenManager.SCREEN_HEIGHT * 3 ) >> 2 );
		//#endif
	}


	public final void update( int delta ) {
		super.update( delta );

		switch ( state ) {
			case STATE_APPEARING:
				accTime += delta;
				setVisibleArea( accTime * 100 / TIME_TRANSITION );
				if ( accTime >= TIME_TRANSITION ) {
					setState( STATE_SHOWN );
				}
				break;
			case STATE_HIDING:
				accTime -= delta;
				setVisibleArea( accTime * 100 / TIME_TRANSITION );
				if ( accTime <= 0 ) {
					setState( STATE_HIDDEN );
				}
				break;
		}
	}


	public final byte getState() {
		return state;
	}


	public final void setState( int state ) {
		this.state = ( byte ) state;
		
		switch ( state ) {
			case STATE_HIDDEN:
				setVisibleArea( 0 );
				if ( nextScreen != null ) {
					switchScreens( true );
				}
				break;

			case STATE_APPEARING:
				if ( bkgVisible ) {
					bkgTop.setVisible( true );
					bkgBottom.setVisible( true );
					fill.setVisible( true );
					borderBottom.setVisible( true );
					borderTop.setVisible( true );
				}

				accTime = 0;
				setVisibleArea( 0 );
			break;

			case STATE_SHOWN:
				if ( !bkgVisible ) {
					bkgTop.setVisible( false );
					bkgBottom.setVisible( false );
					fill.setVisible( false );
					borderBottom.setVisible( false );
					borderTop.setVisible( false );
				}
				accTime = TIME_TRANSITION;
				setVisibleArea( 100 );
				break;
				
			case STATE_HIDING:
				bkgTop.setVisible( true );
				bkgBottom.setVisible( true );
				fill.setVisible( true );
				borderBottom.setVisible( true );
				borderTop.setVisible( true );
				break;
		}
	}


	public final void setVisibleArea( int percent ) {
		percent = NanoMath.clamp( percent, 0, 100 );
		
		final int height = ( screen == null ? ( ( getHeight() * 3 ) >> 2 ) : Math.max( screen.getHeight(), getHeight() >> 2 ) ) * percent / 100;
		contentViewport.set( 0, ( ScreenManager.SCREEN_HEIGHT - height ) >> 1, getWidth(), height );
		bkgTop.setSize( getWidth(), contentViewport.y );
		bottomViewport.set( 0, contentViewport.y + contentViewport.height, getWidth(), getHeight() - ( contentViewport.y + contentViewport.height ) );

		borderTop.setPosition( 0, contentViewport.y - borderTop.getHeight() );
		borderBottom.setPosition( 0, contentViewport.y + contentViewport.height );
	}


	public final void keyPressed( int key ) {
		switch ( state ) {
			case STATE_APPEARING:
				setState( STATE_SHOWN );
				break;

			case STATE_HIDING:
				setState( STATE_HIDDEN );
				break;

			case STATE_SHOWN:
				if ( keyListener != null )
					keyListener.keyPressed( key );
			break;
		}
	}


	public final void keyReleased( int key ) {
		if ( state == STATE_SHOWN && keyListener != null ) {
			keyListener.keyReleased( key );
		}
	}


	public final void setBkgVisible( boolean visible ) {
		bkgVisible = visible;
	}


	public final void hideNotify() {
		if ( screen instanceof ScreenListener )
			( ( ScreenListener ) screen ).hideNotify();
	}


	public final void showNotify() {
		if ( screen instanceof ScreenListener )
			( ( ScreenListener ) screen ).showNotify();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		borderTop.setSize( getWidth(), borderTop.getFill().getHeight() );
		borderBottom.setSize( getWidth(), borderBottom.getFill().getHeight() );

		fill.setSize( size );
		bkgTop.setSize( size );
		bkgBottom.setSize( getWidth(), getHeight() >> 1 );
		bkgBottom.setPosition( 0, getHeight() >> 1 );
	}


	public final void sizeChanged( int width, int height ) {
		if ( width != getWidth() || height != getHeight() ) {
			setSize( width, height );

			if ( screen != null ) {
				if ( setScreenSize )
					screen.setSize( size );
				else if ( screen instanceof ScreenListener )
					( ( ScreenListener ) screen ).sizeChanged( width, height );
			}

			if ( nextScreen == null ) {
				nextScreen = screen;
			}
			switchScreens( false );
			setState( STATE_SHOWN );
		}
	}

	
	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
			if ( state == STATE_SHOWN && pointerListener != null )
				pointerListener.onPointerDragged( x, y );
		}


		public final void onPointerPressed( int x, int y ) {
			if ( state == STATE_SHOWN && pointerListener != null )
				pointerListener.onPointerPressed( x, y );
		}


		public final void onPointerReleased( int x, int y ) {
			if ( state == STATE_SHOWN && pointerListener != null )
				pointerListener.onPointerReleased( x, y );
		}
	//#endif

}
