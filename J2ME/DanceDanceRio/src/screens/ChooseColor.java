package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GameMIDlet;
import javax.microedition.lcdui.Image;
import br.com.nanogames.components.userInterface.ScreenListener;
//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;

//#endif
/**
 *
 * @author Edson
 */
public final class ChooseColor extends UpdatableGroup implements Constants, KeyListener,ScreenListener
//#if TOUCH == "true"
			, PointerListener
//#endif
{

	private static final byte COLORS_TOTAL = 6;
	//#if SCREEN_SIZE == "BIG"
	private final byte BOX_X = 12;
	private final byte BOX_Y = 10;
	//#elif SCREEN_SIZE == "SUPER_BIG"
//# 	private final byte BOX_X = 25;
//# 	private final byte BOX_Y = 10;
	//#elif SCREEN_SIZE == "MEDIUM"
//# 	private final byte BOX_X = 10;
//# 	private final byte BOX_Y = 10;
//#
	//#elif SCREEN_SIZE == "SMALL"
//# 	private final byte BOX_X = 8;
//# 	private final byte BOX_Y = 5;
	//#endif
    /*personagem*/
	private DrawableImage dancer;
	//private final RichLabel name;
	private final Sprite arrowLeft;
	private final Sprite arrowRight;
	private final RichLabel title;
	/*indices das partes a serem modificadas no personagem*/
	public static final byte INDEX_MENU_CABELO = 0,  INDEX_MENU_CAMISA = 1,  INDEX_MENU_CALCA = 2,  INDEX_MENU_PELE = 3;
	private byte positionIndexMenu = INDEX_MENU_CABELO;
	/*indices utilizados no metodo setColor */
	private byte indexColor;
	private byte AuxColorCabelo;
	private byte AuxColorCamisa;
	private byte AuxColorCalca;
	private byte AuxColorPele;
	private final DrawableImage box1;
	private final DrawableImage box2;
	private final DrawableImage box3;
	private final DrawableImage box4;
	private final int posX;
	private int posy;
	private final Drawable cursor;

	public ChooseColor(MenuListener listener, int id) throws Exception {
		super(12);

		final Point offset = new Point(0, (ScreenManager.SCREEN_HEIGHT - CHOOSE_CHARACTER_DEFAULT_HEIGHT) >> 1);

		switch (ChooseDancerScreen.getDancerIndex()) {
			case INDEX_DANCER_FUNK:
				dancer = new DrawableImage(ROOT_DIR + "Shu/funkeira_0.png");
				box1 = new DrawableImage(ROOT_DIR + "cabelo.png");
				box2 = new DrawableImage(ROOT_DIR + "camisa.png");
				box3 = new DrawableImage(ROOT_DIR + "calca.png");
				box4 = new DrawableImage(ROOT_DIR + "corpo.png");
				Thread.yield();
				dancer.defineReferencePixel(dancer.getWidth() >> 1, 0);
				dancer.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, SPLASH_GAME_FUNK_OFFSET_Y);
				dancer.move(offset);
				insertDrawable(dancer);
				posy = SPLASH_GAME_FUNK_OFFSET_Y;
				break;

			case INDEX_DANCER_SKULL:
				dancer = new DrawableImage(ROOT_DIR + "Caveira/caveirex_0.png");
				box1 = new DrawableImage(ROOT_DIR + "chapeu.png");
				box2 = new DrawableImage(ROOT_DIR + "camisa.png");
				box3 = new DrawableImage(ROOT_DIR + "acessorios.png");
				box4 = new DrawableImage(ROOT_DIR + "corpo.png");
				Thread.yield();
				dancer.defineReferencePixel(dancer.getWidth() >> 1, 0);
				dancer.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH + 10, SPLASH_GAME_SKULL_OFFSET_Y);
				dancer.move(offset);
				insertDrawable(dancer);
				posy = SPLASH_GAME_SKULL_OFFSET_Y;
				break;

			default:
				throw new IllegalArgumentException();
		}

		box1.defineReferencePixel(ANCHOR_VCENTER | ANCHOR_HCENTER);
		box2.defineReferencePixel(ANCHOR_VCENTER | ANCHOR_HCENTER);
		box3.defineReferencePixel(ANCHOR_VCENTER | ANCHOR_HCENTER);
		box4.defineReferencePixel(ANCHOR_VCENTER | ANCHOR_HCENTER);

		cursor = new Sprite(ROOT_DIR + "aim");
		cursor.defineReferencePixel(ANCHOR_VCENTER | ANCHOR_HCENTER);
		insertDrawable(cursor);

		insertDrawable(box1);
		insertDrawable(box2);
		insertDrawable(box3);
		insertDrawable(box4);

		title = new RichLabel(GameMIDlet.getFont(FONT_MENU), TEXT_CHOOSE_COLOR, ScreenManager.SCREEN_WIDTH);
		insertDrawable(title);
		/*//name = new RichLabel(GameMIDlet.getFont(FONT_MENU_SMALL), "",ScreenManager.SCREEN_WIDTH, null);
		name.setSize(ScreenManager.SCREEN_WIDTH, name.getHeight());
		name.setSize(ScreenManager.SCREEN_WIDTH, name.getFont().getImage().getHeight() << 1);
		name.defineReferencePixel(name.getWidth() >> 1, name.getHeight());
		name.setRefPixelPosition(ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HEIGHT - MENU_ITEMS_SPACING);
		insertDrawable(name);*/

		arrowLeft = new Sprite(ROOT_DIR + "right");
		arrowLeft.mirror(TRANS_MIRROR_H);
		arrowLeft.defineReferencePixel(arrowLeft.getWidth() + (MENU_ITEMS_SPACING << 1), arrowLeft.getHeight() >> 1);
		arrowLeft.setSequence(1);

		arrowRight = new Sprite(arrowLeft);
		arrowRight.defineReferencePixel(-MENU_ITEMS_SPACING << 1, arrowRight.getHeight() >> 1);
		arrowRight.setSequence(1);
		insertDrawable(arrowRight);
		insertDrawable(arrowLeft);
		posX = dancer.getPosX();

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
		setColor(NanoMath.randInt(COLORS_TOTAL), NanoMath.randInt(COLORS_TOTAL), NanoMath.randInt(COLORS_TOTAL), NanoMath.randInt(COLORS_TOTAL));
	}

	public final void keyPressed(int key) {
		switch (key) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.setScreen(SCREEN_CHOOSE_DANCER);
				return;

			case ScreenManager.KEY_SOFT_MID:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				GameMIDlet.setScreen(SCREEN_DIFFICULTY);
				break;

			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setCursorIndex(positionIndexMenu + 1);
				break;

			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setCursorIndex(positionIndexMenu - 1);
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				++indexColor;

				/*menu rotativo*/
				if (indexColor >= COLORS_TOTAL) {
					indexColor = 0;
				}
				try {

					switch (positionIndexMenu) {
						/*indexColor aumenta ou diminui de acordo com as teclad direira ou esquerda , e variavel auxiliar guarda o num do indice para ser usado na mudança de cor da proxima peça*/
						case INDEX_MENU_CABELO:
							setColor(indexColor, AuxColorCamisa, AuxColorCalca, AuxColorPele);
							break;

						case INDEX_MENU_CAMISA:
							setColor(AuxColorCabelo, indexColor, AuxColorCalca, AuxColorPele);
							break;

						case INDEX_MENU_CALCA:
							setColor(AuxColorCabelo, AuxColorCamisa, indexColor, AuxColorPele);
							break;
						case INDEX_MENU_PELE:
							setColor(AuxColorCabelo, AuxColorCamisa, AuxColorCalca, indexColor);
							break;
					}
				} catch (Exception ex) {
					//#if DEBUG == "true"
					ex.printStackTrace();
					//#endif
				}
				break;

			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				--indexColor;
				if (indexColor < 0) {
					indexColor = COLORS_TOTAL - 1;
				}

				try {
					switch (positionIndexMenu) {
						/*indexColor aumenta ou diminui de acordo com as teclad direira ou esquerda , e variavel auxiliar guarda o num do indice para ser usado na mudança de cor da proxima peça*/
						case INDEX_MENU_CABELO:
							setColor(indexColor, AuxColorCamisa, AuxColorCalca, AuxColorPele);
							break;

						case INDEX_MENU_CAMISA:
							setColor(AuxColorCabelo, indexColor, AuxColorCalca, AuxColorPele);
							break;

						case INDEX_MENU_CALCA:
							setColor(AuxColorCabelo, AuxColorCamisa, indexColor, AuxColorPele);
							break;
						case INDEX_MENU_PELE:
							setColor(AuxColorCabelo, AuxColorCamisa, AuxColorCalca, indexColor);
							break;
					}

				} catch (Exception ex) {
					//#if DEBUG == "true"
					ex.printStackTrace();
					//#endif
				}
				break;
		}
	}

	private final void setCursorIndex(int index) {
		positionIndexMenu = (byte) index;
		if (positionIndexMenu < INDEX_MENU_CABELO) {
			positionIndexMenu = INDEX_MENU_PELE;
		} else if (positionIndexMenu > INDEX_MENU_PELE) {
			positionIndexMenu = INDEX_MENU_CABELO;
		}

		Drawable box = null;
		switch (positionIndexMenu) {
			case INDEX_MENU_CABELO:
				box = box1;
				break;

			case INDEX_MENU_CAMISA:
				box = box2;
				break;

			case INDEX_MENU_CALCA:
				box = box3;
				break;

			case INDEX_MENU_PELE:
				box = box4;
				break;
		}

		if (box != null) {
			cursor.setRefPixelPosition(box.getRefPixelPosition());
		}
	}

	public final void keyReleased(int key) {
	}

	public final void setColor(int indexCabelo, int indexCamisa, int indexCalcas, int indexPele) throws Exception {
		removeDrawable(dancer);
		dancer = null;

		AuxColorCabelo = (byte) indexCabelo;
		AuxColorCamisa = (byte) indexCamisa;
		AuxColorCalca = (byte) indexCalcas;
		AuxColorPele = (byte) indexPele;

		//#if SCREEN_SIZE != "SUPER_BIG"

		switch (ChooseDancerScreen.getDancerIndex()) {
			case INDEX_DANCER_FUNK:
				final int[][] cabelos = new int[][]{
					// meio      brilho    contorno
					{0xfbd009, 0xfae276, 0xd6b000}, // loira
					{0xf73209, 0xf77f66, 0xd62500}, // ruiva linda do meu S2 *-*'
					{0x160901, 0x2c2a2a, 0x000000}, // preto
					{0x32be1a, 0x73ff5b, 0xe1c880a}, // verde
					{0xab27dd, 0xcd89e7, 0xe711196}, //
					{0x512406, 0x73370f, 0xe331603} // marrom
				};
				final int[][] camisas = new int[][]{
					// brilho     escuro
					{0xcb2d9c, 0x922172}, // rosa
					{0x1cdccf, 0x159e93}, // azul
					{0x494648, 0x272626}, // preta
					{0xcb3a2d, 0x922821},// vermelha
					{0x8527dd, 0x5a0c78},// roxo
					{0x0ae424, 0x0da61f} // verde
				};
				final int[][] calcas = new int[][]{
					// brilho  claro      escuro
					{0x788ad0, 0x5064b1, 0x3b487f}, // jeans
					{0x736f72, 0x494648, 0x272626}, // preta
					{0xfffffe, 0xe7eeff, 0xacb3bf}, // branca
					{0xfee4ff, 0xffca00, 0xba8105}, // amarela
					{0xfb8bd9, 0xfc3cc2, 0x910968}, // rosa
					{0xff6873, 0xdf2230, 0x891219}// vermelha
				};
				final int[][] pele = new int[][]{
					//contorno  pele meio  brilho/nariz
					{0xe39b6f, 0xffcbab, 0xffffff}, // branca
					{0xad5a41, 0xe78e72, 0xE9A792}, // india
					{0x808f7a, 0xc0cdbc, 0xE2E9E0}, // zoombie
					{0x3BBA63, 0xC0FDAD, 0xffffff}, // E.T.
					{0xe7ba58, 0xffe5ab, 0xF9ECCF}, // amarela
					{0x614431, 0x865f46, 0xAB7F63}, //
				};

				PaletteChanger p = new PaletteChanger(ROOT_DIR + "Shu/funkeira_0.png");

				final PaletteMap[] map = new PaletteMap[]{
					new PaletteMap(cabelos[ 0][ 0], cabelos[indexCabelo][ 0]),
					new PaletteMap(cabelos[ 0][ 1], cabelos[indexCabelo][ 1]),
					new PaletteMap(cabelos[ 0][ 2], cabelos[indexCabelo][ 2]),
					new PaletteMap(camisas[ 0][ 0], camisas[indexCamisa][ 0]),
					new PaletteMap(camisas[ 0][ 1], camisas[indexCamisa][ 1]),
					new PaletteMap(calcas[ 0][ 0], calcas[indexCalcas][ 0]),
					new PaletteMap(calcas[ 0][ 1], calcas[indexCalcas][ 1]),
					new PaletteMap(calcas[ 0][ 2], calcas[indexCalcas][ 2]),
					new PaletteMap(pele[ 0][ 0], pele[indexPele][ 0]),
					new PaletteMap(pele[ 0][ 1], pele[indexPele][ 1]),
					new PaletteMap(pele[ 0][ 2], pele[indexPele][ 2]),};

				p.createImage(map);
				final Image image = p.createImage(map);
				dancer = new DrawableImage(image);
				insertDrawable(dancer);
				PlayScreen.setDancerImage(map);

				break;

			case INDEX_DANCER_SKULL:
				final int[][] chapeu = new int[][]{
					//claro       sombra
					{0xfff3d9, 0xe5ba81},
					{0xef6fd3, 0xc9059e},//rosa
					{0xeaf8ff, 0x594abb8},//sinza
					{0xfff605, 0xedab1b}, // amarelo
					{0xd42a2a, 0x840808}, // vermelho
					{0x5a5a5a, 0x2d2c2c} // preto
				};
				final int[][] palito = new int[][]{
					// claro     sombra
					{0xfdf1d7, 0xe6bb83},
					{0xef6fd3, 0xc9059e},//rosa
					{0xeaf8ff, 0x594abb8},//sinza
					{0xfff605, 0xedab1b}, // amarelo
					{0xd42a2a, 0x840808}, // vermelho
					{0x5a5a5a, 0x2d2c2c} // preto
				};
				final int[][] flor = new int[][]{
					//claro    sombra
					{0xb80e0e, 0x7a0d0d}, // rosa
					{0xf8ffff, 0x94abb8}, // branco
					{0x8527dd, 0x5a0c78}, // roxo
					{0x0e9cb8, 0x0d687a}, // azul
					{0xff752a, 0x0d8330c}, // laranja
					{0xffca00, 0xba8106}, // dourada
				};
				final int[][] ossos = new int[][]{
					// claro    sombra   contorno
					{0xffffff, 0xbfbdbf, 0x818181}, // branca
					{0x77ccff, 0x068bda, 0x04516f}, // azul
					{0x817f7f, 0x484444, 0x302e2e}, // preta
					{0x7bf487, 0x3a9442, 0x1f6e26}, // verde
					{0xf8856e, 0xc73d22, 0x8d210c}, //vermelha
					{0xe5d091, 0xb2882f, 0x5a4319}, //amarelada
				};

				PaletteChanger p1 = new PaletteChanger(ROOT_DIR + "Caveira/caveirex_0.png");

				PaletteMap[] map1 = new PaletteMap[]{
					new PaletteMap(chapeu[ 0][ 0], chapeu[indexCabelo][ 0]),
					new PaletteMap(chapeu[ 0][ 1], chapeu[indexCabelo][ 1]),
					new PaletteMap(palito[ 0][ 0], palito[indexCamisa][ 0]),
					new PaletteMap(palito[ 0][ 1], palito[indexCamisa][ 1]),
					new PaletteMap(flor[ 0][ 0], flor[indexCalcas][ 0]),
					new PaletteMap(flor[ 0][ 1], flor[indexCalcas][ 1]),
					new PaletteMap(ossos[ 0][ 0], ossos[indexPele][ 0]),
					new PaletteMap(ossos[ 0][ 1], ossos[indexPele][ 1]),
					new PaletteMap(ossos[ 0][ 2], ossos[indexPele][ 2]),};

				p1.createImage(map1);
				final Image image1 = p1.createImage(map1);
				dancer = new DrawableImage(image1);
				insertDrawable(dancer);
				PlayScreen.setDancerImage(map1);
				break;
		}
		refreshDancerPosition();
		//#else
//# 		switch (ChooseDancerScreen.getDancerIndex()) {
//# 			case INDEX_DANCER_FUNK:
//# 				final int[][] cabelos = new int[][]{
//# 					// meio      brilho   contorno  claro    contorno2
//# 					{0xfbd009, 0xfae276, 0x977d05, 0xffdb32, 0xd6b000}, // loira
//# 					{0xdf0433, 0xff7693, 0x8b0723, 0xff345f, 0xc20931}, // ruiva linda do meu S2 *-*'
//# 					{0x131313, 0x444443, 0x000000, 0x1f1e1e, 0x0b0b0b}, // preto
//# 					{0x31ae1d, 0x6fe35e, 0x17650a, 0x45c82f, 0x1d7f0d}, // verde
//# 					{0x970bf0, 0xc771fe, 0x5d0297, 0xb441fe, 0x7c07c6}, // roxo
//# 					{0x512407, 0x583b0a, 0x2f1403, 0x5e2a07, 0x3b1b06} // marrom
//# 				};
//# 				final int[][] camisas = new int[][]{
//# 					// brilho   escuro  brilho     contorno
//# 					{0xcb2d9c, 0x922172, 0xed56c0, 0x610447}, // rosa
//# 					{0x8527dd, 0x6a15b9, 0xa65dea, 0x4d0d88}, // azul
//# 					{0x282728, 0x181718, 0x424042, 0x000000}, // preta
//# 					{0xcb3a2d, 0x922921, 0xe35f53, 0x641d0d},// vermelha
//# 					{0xe7c319, 0xe19503, 0xe19503, 0xb27500},// amarela
//# 					{0x29c93b, 0x0ea620, 0x58ea68, 0x057312} // verde
//# 				};
//# 				final int[][] calcas = new int[][]{
//# 					// brilho  claro      escuro    contorno
//# 					{0x788ad0, 0x5064b1, 0x3b487f, 0x142056}, // jeans
//# 					{0x424042, 0x282728, 0x181718, 0x000000}, // preta
//# 					{0xdce8ff, 0xbdd4ff, 0x83a3de, 0x5e80bf}, // branca
//# 					{0xffe270, 0xefc319, 0xe19503, 0xb27500}, // amarela
//# 					{0xff852d, 0xff58c2, 0xf42dab, 0x940463}, // rosa
//# 					{0xa65dea, 0x8527dd, 0x6a15b9, 0x4d0d88}// roxo
//# 				};
//# 				final int[][] pele = new int[][]{
//# 					//contorno pele meio bri/nariz bochechameio bochechacontorno contorno2   contorno3
//# 					{0xb96e41, 0xffcbab, 0xffffff, 0xfadcca,     0xfdd4bc,       0xe39b6f,   0xfab890},      // branca
//# 					{0x8a3820, 0xe78e72, 0xf5a58c, 0xfdb59f,     0xeb9c83,       0xad5a41,   0xd5795c}, // india
//# 					{0x5b7153, 0xa3bb9a, 0xE2E9E0, 0xbdd5b3,     0xafc7a5,       0x7790ce,   0x8ca583}, // zoombie
//# 					{0x126d2f, 0x1fe168, 0x7cffad, 0x52fd92,     0x33f3f8,       0x0c9743,   0x14c255}, // E.T.
//# 					{0x9c6a09, 0xffe5ab, 0xF9ECCF, 0xfdeac5,     0xf7ddac,       0xd89e2f,   0xfac868}, // amarela
//# 					{0x361d0e, 0x865f46, 0xc89c7f, 0xae8164,     0x9a6f53,       0x583620,   0x66412a}, //negra
//# 				};
//# 
//# 				PaletteChanger p = new PaletteChanger(ROOT_DIR + "Shu/funkeira_0.png");
//# 
//# 				final PaletteMap[] map = new PaletteMap[]{
//# 					new PaletteMap(cabelos[ 0][ 0], cabelos[indexCabelo][ 0]),
//# 					new PaletteMap(cabelos[ 0][ 1], cabelos[indexCabelo][ 1]),
//# 					new PaletteMap(cabelos[ 0][ 2], cabelos[indexCabelo][ 2]),
//# 					new PaletteMap(cabelos[ 0][ 3], cabelos[indexCabelo][ 3]),
//# 					new PaletteMap(cabelos[ 0][ 4], cabelos[indexCabelo][ 4]),
//# 					
//# 					new PaletteMap(camisas[ 0][ 0], camisas[indexCamisa][ 0]),
//# 					new PaletteMap(camisas[ 0][ 1], camisas[indexCamisa][ 1]),
//# 					new PaletteMap(camisas[ 0][ 2], camisas[indexCamisa][ 2]),
//# 					new PaletteMap(camisas[ 0][ 3], camisas[indexCamisa][ 3]),
//# 					
//# 					new PaletteMap(calcas[ 0][ 0], calcas[indexCalcas][ 0]),
//# 					new PaletteMap(calcas[ 0][ 1], calcas[indexCalcas][ 1]),
//# 					new PaletteMap(calcas[ 0][ 2], calcas[indexCalcas][ 2]),
//# 					new PaletteMap(calcas[ 0][ 3], calcas[indexCalcas][ 3]),
//# 
//# 					new PaletteMap(pele[ 0][ 0], pele[indexPele][ 0]),
//# 					new PaletteMap(pele[ 0][ 1], pele[indexPele][ 1]),
//# 					new PaletteMap(pele[ 0][ 2], pele[indexPele][ 2]),
//# 				    new PaletteMap(pele[ 0][ 3], pele[indexPele][ 3]),
//# 				    new PaletteMap(pele[ 0][ 4], pele[indexPele][ 4]),
//# 				    new PaletteMap(pele[ 0][ 5], pele[indexPele][ 5]),
//# 				    new PaletteMap(pele[ 0][ 6], pele[indexPele][ 6]),};
//# 
//# 				p.createImage(map);
//# 				final Image image = p.createImage(map);
//# 				dancer = new DrawableImage(image);
//# 				insertDrawable(dancer);
//# 				PlayScreen.setDancerImage(map);
//# 
//# 				break;
//# 
//# 			case INDEX_DANCER_SKULL:
//# 				final int[][] chapeu = new int[][]{
//# 					//claro     sombra   sombra2   contorno  brilho
//# 					{0xfff3d9, 0xe5ba81, 0xf4deae, 0x8f5304, 0xfefbf4},
//# 					{0x8958f8, 0x561ed5, 0x6e3ae5, 0x410bbd, 0xb396fb},//azul
//# 					{0xeaf8ff, 0x94abb8, 0xcce0eb, 0x5e7582, 0xfefbf4},//sinza
//# 					{0xfff605, 0xedab1b, 0xefc930, 0x985600, 0xf6f3ed}, // amarelo
//# 					{0xd42a2a, 0x840808, 0xb81c1c, 0x690404, 0xec5858}, // vermelho
//# 					{0x5a5a5a, 0x2d2c2c, 0x3e3d3d, 0x1c1b1b, 0x838181} // preto
//# 				};
//# 				final int[][] palito = new int[][]{
//# 					// claro     sombra  sombra2   contorno  brilho
//# 					{0xfff0d1, 0xe6bb83, 0xf2ddb0, 0x985600, 0xf6f3ed},
//# 					{0x8958f8, 0x561ed5, 0x6e3ae5, 0x410bbd, 0xb393fb},//azul
//# 					{0xeaf8ff, 0x94abb8, 0xcce0eb, 0x5e7582, 0xffffff},//sinza
//# 					{0xfff605, 0xedab1b, 0xefc930, 0x985600, 0xf6f3ed}, // amarelo
//# 					{0xd42a2a, 0x840808, 0xb81c1c, 0x690404, 0xec5858}, // vermelho
//# 					{0x5a5a5a, 0x2d2c2c, 0x3e3d3d, 0x1c1b1b, 0x838181} // preto
//# 				};
//# 				final int[][] flor = new int[][]{
//# 					//claro     sombra1  sombra2    sombra3    brilho
//# 					{0xb80e0e, 0x951111, 0x7a0d0d, 0x5f0000, 0xdc3535}, // vermelho
//# 					{0xf8ffff, 0xcce0eb, 0x94abb8, 0x5e7582, 0xffffff}, // branco
//# 					{0x0eb810, 0x119513, 0x5a0c78, 0x0d7a0e, 0x35dc37}, // verde
//# 					{0x0e9cb8, 0x112d95, 0x0d687a, 0x00145f, 0x3559dc}, // azul
//# 					{0xb80eb0, 0x95118e, 0x7a0d75, 0x5f005a, 0xdc3534}, // roxo
//# 					{0xfff605, 0xefc930, 0xedab1b, 0x985600, 0xffffff}, // dourada
//# 				};
//# 				final int[][] ossos = new int[][]{
//# 					// claro    sombra   sombra2   naréba/2  contorno  olhoFora
//# 					{0xffffff, 0xbfbdbf, 0x818181, 0xe4e2e2, 0x373737, 0x666363, 0x474747}, // branca
//# 					{0x77ccff, 0x068bda, 0x8e5d0c, 0x32a6ec, 0x181717, 0x068bda, 0x04516f}, // azul
//# 					{0x817f7f, 0x484444, 0x302e2e, 0x676666, 0x181717, 0x484444, 0x302e2e}, // preta
//# 					{0xd5fb83, 0x83c018, 0x1f6e26, 0xb2f045, 0x406204, 0x5e8d0c, 0x406204}, // verde
//# 					{0xf8856e, 0xdf482b, 0x8d210c, 0xf06245, 0x621304, 0x8d210c, 0x621304}, //vermelha
//# 					{0xe5d091, 0xb2882f, 0x5a4319, 0xd1b564, 0x402e0c, 0x745b12, 0x402e0c}, //amarelada
//# 				};
//# 
//# 				PaletteChanger p1 = new PaletteChanger(ROOT_DIR + "Caveira/caveirex_0.png");
//# 
//# 				PaletteMap[] map1 = new PaletteMap[]{
//# 					new PaletteMap(chapeu[ 0][ 0], chapeu[indexCabelo][ 0]),
//# 					new PaletteMap(chapeu[ 0][ 1], chapeu[indexCabelo][ 1]),
//# 					new PaletteMap(chapeu[ 0][ 2], chapeu[indexCabelo][ 2]),
//# 					new PaletteMap(chapeu[ 0][ 3], chapeu[indexCabelo][ 3]),
//# 					new PaletteMap(chapeu[ 0][ 4], chapeu[indexCabelo][ 4]),
//# 
//# 					new PaletteMap(palito[ 0][ 0], palito[indexCamisa][ 0]),
//# 					new PaletteMap(palito[ 0][ 1], palito[indexCamisa][ 1]),
//# 					new PaletteMap(palito[ 0][ 2], palito[indexCamisa][ 2]),
//# 					new PaletteMap(palito[ 0][ 3], palito[indexCamisa][ 3]),
//# 					new PaletteMap(palito[ 0][ 4], palito[indexCamisa][ 4]),
//# 
//# 					new PaletteMap(flor[ 0][ 0], flor[indexCalcas][ 0]),
//# 					new PaletteMap(flor[ 0][ 1], flor[indexCalcas][ 1]),
//# 					new PaletteMap(flor[ 0][ 2], flor[indexCalcas][ 2]),
//# 					new PaletteMap(flor[ 0][ 3], flor[indexCalcas][ 3]),
//# 					new PaletteMap(flor[ 0][ 4], flor[indexCalcas][ 4]),
//# 
//# 					new PaletteMap(ossos[ 0][ 0], ossos[indexPele][ 0]),
//# 					new PaletteMap(ossos[ 0][ 1], ossos[indexPele][ 1]),
//# 					new PaletteMap(ossos[ 0][ 2], ossos[indexPele][ 2]),
//# 					new PaletteMap(ossos[ 0][ 3], ossos[indexPele][ 3]),
//# 					new PaletteMap(ossos[ 0][ 4], ossos[indexPele][ 4]),
//# 					new PaletteMap(ossos[ 0][ 5], ossos[indexPele][ 5]),
//# 					new PaletteMap(ossos[ 0][ 6], ossos[indexPele][ 6]),};
//# 
//# 				p1.createImage(map1);
//# 				final Image image1 = p1.createImage(map1);
//# 				dancer = new DrawableImage(image1);
//# 				insertDrawable(dancer);
//# 				PlayScreen.setDancerImage(map1);
//# 				break;
//# 		}
//# 		refreshDancerPosition();
		//#endif
	}

	public final void setSize(int width, int height) {
		setPosition(0, 0);
		title.setSize(width * 9 / 10, height);
		title.formatText(false);
		title.setPosition((width - title.getWidth()) >> 1, Math.max(title.getPosY(), MENU_ITEMS_SPACING));

		final int BOX_INITIAL_Y = title.getPosY() + title.getTextTotalHeight() + (cursor.getHeight() >> 1);
		final int BOX_HEIGHT = cursor.getHeight();
		final int BOX_SPACING = BOX_HEIGHT - ((cursor.getHeight() - box1.getHeight()) >> 1);



		box1.setPosition(BOX_X, BOX_INITIAL_Y);

		if (BOX_INITIAL_Y + 4 * box1.getHeight() > height) {
			byte ajust = 13;
			title.setPosition(box1.getPosX() + box1.getWidth(), box1.getPosY());
			box1.setPosition(box1.getPosX(), box1.getPosY() - box1.getHeight()/2 - ajust);
		}
		
		box2.setPosition(BOX_X, box1.getPosY() + BOX_SPACING);
		box3.setPosition(BOX_X, box2.getPosY() + BOX_SPACING);
		box4.setPosition(BOX_X, box3.getPosY() + BOX_SPACING);

		super.setSize(width, box4.getPosY() + box4.getHeight() + (cursor.getHeight() - box4.getHeight()));

		setCursorIndex(positionIndexMenu);
		refreshDancerPosition();
	}


	//#if TOUCH == "true"
		public final void onPointerDragged( int x, int y ) {
		}


		public final void onPointerPressed( int x, int y ) {
			if ( box1.contains( x, y ) ) {
				setCursorIndex( 0 );
			} else if ( box2.contains( x, y ) ) {
				setCursorIndex( 1 );
			} else if ( box3.contains( x, y ) ) {
				setCursorIndex( 2 );
			} else if ( box4.contains( x, y ) ) {
				setCursorIndex( 3 );
			} else if ( x > box1.getPosX() + box1.getWidth() ) {
				if ( x < dancer.getPosX() ) {
					keyPressed( ScreenManager.LEFT );
				} else if ( x > dancer.getPosX() + dancer.getWidth() ) {
					keyPressed( ScreenManager.RIGHT );
				} else {
					keyPressed( ScreenManager.FIRE );
				}
			}
		}


		public final void onPointerReleased( int x, int y ) {
		}

	//#endif
	private final void refreshDancerPosition() {
		if (dancer != null) {
			dancer.defineReferencePixel(ANCHOR_HCENTER | ANCHOR_VCENTER);

			final int START_X = box1.getPosX() + box1.getWidth();
			final int START_Y = title.getPosY() + title.getTextTotalHeight() + MENU_ITEMS_SPACING;
			final int HEIGHT = getHeight() - START_Y;

			//#if SCREEN_SIZE =="BIG"
			switch (ChooseDancerScreen.getDancerIndex()) {
				case INDEX_DANCER_FUNK:
					dancer.setRefPixelPosition(START_X + ( ( getWidth() - START_X ) >> 1 ) + FUNKEIRA_OFFSET_X, START_Y + ( HEIGHT >> 1 ) );
					break;

				case INDEX_DANCER_SKULL:
					dancer.setRefPixelPosition(START_X + ( ( getWidth() - START_X ) >> 1 ), START_Y + ( HEIGHT >> 1 ) );
					break;
			}
			//#else
//# 			dancer.setRefPixelPosition(START_X + ((getWidth() - START_X) >> 1), START_Y + (HEIGHT >> 1));
			//#endif

			arrowLeft.setRefPixelPosition(dancer.getPosX(), dancer.getRefPixelY());
			arrowRight.setRefPixelPosition(dancer.getPosX() + dancer.getWidth(), arrowLeft.getRefPixelY());
		}
	}

	public final void hideNotify() {

	}

	public final void showNotify() {
	}

	public final void sizeChanged(int width, int height) {
		if ( width != getWidth() || height != getHeight() ){
			setSize(width, height);
		}
	}
}
