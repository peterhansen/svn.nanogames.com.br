//#if SCREEN_SIZE== "SUPER_BIG"
//# /*
//#  * To change this template, choose Tools | Templates
//#  * and open the template in the editor.
//#  */
//# package screens;
//# 
//# import br.com.nanogames.components.DrawableImage;
//# import br.com.nanogames.components.ImageFont;
//# import br.com.nanogames.components.Pattern;
//# import br.com.nanogames.components.RichLabel;
//# import br.com.nanogames.components.UpdatableGroup;
//# import br.com.nanogames.components.userInterface.KeyListener;
//# import br.com.nanogames.components.userInterface.ScreenManager;
//# import core.Constants;
//# import core.GameMIDlet;
//# 
//# /**
//#  *
//#  * @author Edson
//#  */
//# 
//# public class SplashNano extends UpdatableGroup implements Constants, KeyListener {
//# 
//# 	private DrawableImage imgNano;
//# 	private final ImageFont font;
//# 	private final RichLabel msg;
//# 	private final Pattern bkg = new Pattern(0x000000);
//# 	private final int TIME_TO_NEXT_SCREEN = 3000;
//# 	private int time;
//# 
//# 	public SplashNano() throws Exception {
//# 		super(6);
//# 		imgNano = new DrawableImage(ROOT_DIR + "splash/nano.png");
//# 		font = ImageFont.createMultiSpacedFont(ROOT_DIR + "splash/font_credits_big" );
//# 		msg = new RichLabel(font, TEXT_SPLASH_NANO , 0);
//# 		insertDrawable(bkg);
//# 		insertDrawable(imgNano);
//# 		insertDrawable(msg);
//# 		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
//# 	}
//# 	
//# 
//# 	public final void setSize(int width, int height) {
//# 		super.setSize(width, height);
//# 		bkg.setSize(width, height);
//# 		imgNano.setPosition(width / 2 - imgNano.getWidth() / 2, height / 2 - imgNano.getHeight() / 2);
//# 		msg.setPosition(width / 2 - msg.getWidth()/2, height - 50);
//# 	}
//# 
//# 	
//# 	public final void update(int delta) {
//# 		time += delta;
//# 
//# 		if (time > TIME_TO_NEXT_SCREEN) {
//# 			GameMIDlet.setScreen(SCREEN_SPLASH_GAME);
//# 		}
//# 		super.update(delta);
//# 	}
//# 
//# 
//# 	public final void keyPressed(int key) {
		//#if DEBUG == "true"
//# 			GameMIDlet.setScreen(SCREEN_SPLASH_GAME);
		//#endif
//# 	}
//# 
//# 	
//# 	public final void keyReleased(int key) {
//# 	}
//# }
//# 
//#endif
