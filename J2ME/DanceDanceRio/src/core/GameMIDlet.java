/**
 * GameMIDlet.java
 */
package core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import screens.BorderedScreen;
import screens.*;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedSoftkey;
import br.com.nanogames.components.basic.BasicConfirmScreen;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicOptionsScreen;
import br.com.nanogames.components.basic.BasicSplashBrand;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.online.Customer;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineScrollBar;
import br.com.nanogames.components.online.RankingEntry;
import br.com.nanogames.components.online.RankingFormatter;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.userInterface.Alert;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import br.com.nanogames.components.util.SmsSender;
import game.PushItBar;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDletStateChangeException;
import br.com.nanogames.components.util.Serializable;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.form.TouchKeyPad;

//#endif

//#if BLACKBERRY_API == "true"
//#  import net.rim.device.api.ui.Keypad;
//#endif

//#if LOG == "true"
//# import br.com.nanogames.components.util.Logger;
//#endif

//#if QUALCOMM_API == "true"
//# import com.qualcomm.qis.helper.LicenseString;
//# import java.util.Date;
//# import license.DecrementUseCallback;
//# import license.License;
//# import license.LicenseAgent;
//# import license.LicenseCallback;
//# import license.NullCallbackException;
//# import license.PurchaseResponse;
//#endif

/**
 * ©2007 Nano Games
 * @author Daniel L. Alves
 */

public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener ,Serializable
{	
	/** Referência para a tela de jogo */
	private PlayScreen playScreen;
	
	/** Fonte padrão do jogo */
	private static final byte BACKGROUND_TYPE_DEFAULT = 0;

	private static final byte BACKGROUND_TYPE_SOLID_COLOR = 1;

	private static final byte BACKGROUND_TYPE_NONE = 2;

	private static int softKeyRightText;

	/** gerenciador da animação da soft key esquerda */
	private static BasicAnimatedSoftkey softkeyLeft;

	/** gerenciador da animação da soft key direita */
	private static BasicAnimatedSoftkey softkeyRight;

	public static final byte DEFAULT_FONT_OFFSET = -1;
	//#if SCREEN_SIZE != "SMALL"
		private static final byte BIG_FONT_OFFSET = -2;

	//#else
//# 	private static final byte BIG_FONT_OFFSET = -0;
	//#endif
		
	//#if LOG == "true"
//# 	public static final byte SCREEN_LOG				= 21;
	//#endif

	public static StringBuffer log = new StringBuffer();
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu principal">
	
	public static final byte MAIN_MENU_NEW_GAME		= 0;
	public static final byte MAIN_MENU_OPTIONS		= 1;
	public static final byte MAIN_MENU_NANO_ONLINE	= 2;
	public static final byte MAIN_MENU_RECOMMEND	= 3;
	public static final byte MAIN_MENU_HELP			= 4;
	public static final byte MAIN_MENU_CREDITS		= 5;

	//#if QUALCOMM_API == "false"
		public static final byte MAIN_MENU_EXIT			= 6;
		public static final byte MAIN_MENU_LOG			= 7;
	//#else
//# 		public static final byte MAIN_MENU_LICENSE_INFO	= 6;
//# 		public static final byte MAIN_MENU_EXIT			= 7;
//# 		public static final byte MAIN_MENU_LOG			= 8;
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu de pausa">
	
	//#ifdef NO_SOUND
//# 		public static final byte PAUSE_MENU_CONTINUE			= 0;
//# 		public static final byte PAUSE_MENU_TOGGLE_VIBRATION	= 1;
//# 		public static final byte PAUSE_MENU_BACK_TO_MENU		= 2;	
//# 		public static final byte PAUSE_MENU_EXIT_GAME			= 3;
//# 
//# 		public static final byte PAUSE_MENU_NO_VIB_CONTINUE			= 0;
//# 		public static final byte PAUSE_MENU_NO_VIB_BACK_TO_MENU		= 1;	
//# 		public static final byte PAUSE_MENU_NO_VIB_EXIT_GAME		= 2;
	//#else
	public static final byte PAUSE_MENU_CONTINUE			= 0;
	public static final byte PAUSE_MENU_TOGGLE_SOUND		= 1;
	public static final byte PAUSE_MENU_TOGGLE_VIBRATION	= 2;
	public static final byte PAUSE_MENU_BACK_TO_MENU		= 3;	
	public static final byte PAUSE_MENU_EXIT_GAME			= 4;

	public static final byte PAUSE_MENU_NO_VIB_CONTINUE			= 0;
	public static final byte PAUSE_MENU_NO_VIB_TOGGLE_SOUND		= 1;
	public static final byte PAUSE_MENU_NO_VIB_BACK_TO_MENU		= 2;	
	public static final byte PAUSE_MENU_NO_VIB_EXIT_GAME		= 3;
	//#endif
	
	//</editor-fold>


     /*indices de opções menu de dificuldade*/
    public static final byte OPTIONS_DIFICULT_EASY = 1;
    public static final byte OPTIONS_DIFICULT_MEDIUM = 2;
    public static final byte OPTIONS_DIFICULT_HARD = 3;
    public static final byte MENU_DIFICULT_BACK_MAIN_MENU = 4;

    /* indices de opções menu escolha de personagem */
    /*indices de opções menu de dificuldade*/
    public static final byte OPTIONS_DANCER_1 = 1;
    public static final byte OPTIONS_DANCER_2 = 2;
    public static final byte OPTIONS_DANCER_3 = 3;
    public static final byte MENU_CHOOSE_DANCER_BACK_MAIN_MENU = 4;

    public static final byte LEVEL_RESULT_BACK_PLAYSCREEN      =5;

    public static final byte OPTION_ENGLISH= 1;
    public static final byte OPTION_PORTUGUESE= 2;



	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu de opções">
	
	//#ifdef NO_SOUND
//# 		public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 0;
//# 		public static final byte OPTIONS_MENU_ERASE_RECORDS		= 1;
//# 		public static final byte OPTIONS_MENU_BACK				= 2;
//# 
//# 		public static final byte OPTIONS_MENU_NO_VIB_ERASE_RECORDS		= 0;
//# 		public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;
	//#else
	public static final byte OPTIONS_MENU_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_TOGGLE_VIBRATION	= 1;
	public static final byte OPTIONS_MENU_BACK				= 2;

	public static final byte OPTIONS_MENU_NO_VIB_TOGGLE_SOUND		= 0;
	public static final byte OPTIONS_MENU_NO_VIB_BACK				= 1;
	//#endif
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Índices das opções do menu de ajuda">

	// CANCELED : Retiramos por falta de resposta da MTV
	//public static final byte HELP_MENU_INTRO			= 0;
	public static final byte HELP_MENU_RULES			= 0;
	public static final byte HELP_MENU_SPECIAL_ICONS	= 1;
	public static final byte HELP_MENU_CONTROLS			= 2;
	public static final byte HELP_MENU_BACK				= 3;
	
	//</editor-fold>

		/** Indica se o device possui pouca memória */
		private boolean lowMemory;
	
	/** Indica se os recursos do jogo já foram alocados */
	private boolean resourcesLoaded;

	private static BorderedScreen borderedScreen;

	//#if BLACKBERRY_API == "true"
//# 		private static boolean showRecommendScreen = true;
	//#endif

	//#if NANO_RANKING == "true"
		private Form nanoOnlineForm;
	//#endif

	//#ifndef NO_RECOMMEND
		private Drawable recommendScreen;
	//#endif
	
	/** Tempo máximo entre duas renderizações */
	private static final short LOW_FPS_FRAME_TIME = 130;
	
	/** Indica se devemos bloquear a mudança de telas. Com isso conseguiremos esperar os fades das telas
	 * de carregamento, por exemplo */ 
	private boolean blockScreenChange;


    public static long score;

	private static Sprite cursor;

	
	public GameMIDlet() throws Exception {
		//#ifdef SmallSagem
//# 		super( VENDOR_SAGEM_GRADIENTE, LOW_FPS_FRAME_TIME );
		//#elif VENDOR == "MOTOROLA"
//# 			super( VENDOR_MOTOROLA, LOW_FPS_FRAME_TIME );
		//#else
			super(-1, LOW_FPS_FRAME_TIME);
		//#endif

		FONTS = new ImageFont[FONT_TYPES_TOTAL];
	}


	public static final void log(String s) {
		//#if DEBUG == "true"
			System.gc();

			log.append(s);
			log.append(": ");
			log.append(Runtime.getRuntime().freeMemory());

			log.append("\n");
		//#endif
	}
	
	
	public final void destroy() {
		if (playScreen != null) {
			// Grava a pontuação mesmo que esteja no meio do jogo

			//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				RankingScreen.setHighScore(RANKING_TYPE_SCORE, c.getId(), playScreen.getScore(), true);
			} catch (Exception e) {
				//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
			}
		//#else
//# 				GameRecordsScreen.setScore( playScreen.getScore() );
//# 				resourcesLoaded = false;
//# 				playScreen = null;
			//#endif
		}
		
		//#if LOG == "true"
//# 		Logger.saveLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif
		
		super.destroy();
	}
	
	
	protected final void loadResources() throws Exception
	{

		//#if JAR == "min"
//# 			lowMemory = true;
		//#elifdef MEDIUM_MIN
//# 			lowMemory = true;
		//#elif SCREEN_SIZE == "BIG"
			/*switch (getVendor()) {
		case VENDOR_SAMSUNG:
		case VENDOR_LG:
		lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
		break;
		}*/
		//#elif SCREEN_SIZE == "SUPER_BIG"
//# 		       lowMemory = false;
		//#else
//# 			switch ( getVendor() ) {
//# 				case VENDOR_SONYERICSSON:
//# 				case VENDOR_SIEMENS:
//# 				case VENDOR_BLACKBERRY:
//# 				case VENDOR_HTC:
//# 				break;
//# 
//# 				default:
//# 					lowMemory = Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT;
//# 			}
		//#endif

           //lowMemory = true; // teste

		//#if BLACKBERRY_API == "true"
//# 			// em aparelhos antigos (e/ou com versões de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms não recebe os eventos de teclado corretamente, sumindo somente após a aplicação ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 			case ScreenManager.HW_LAYOUT_REDUCED:
//# 					showRecommendScreen = false;
//# 				break;
//# 			}
		//#endif

		try {
			createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
		}

		// inicializa as tabelas de ranking online
		//#if NANO_RANKING == "true"
			RankingScreen.init( new int[] { TEXT_RECORDS }, new RankingFormatter() {
				public String format( int type, long score ) {
					return String.valueOf( score );
				}

				public final void initLocalEntry( int type, RankingEntry entry, int index ) {
					entry.setScore( ( 20 - index ) * 10000 );
					final String[] names = new String[] { 
						"piteco",
						"tommy",
						"vivs",
						"weifols",
						"cammy",
						"eddie",
						"maxxx",
						"monty",
						"dimmy",
						"peta",
					};
					entry.setNickname( names[ index % names.length ] );
				}
		} );
		//#endif			

		// Carrega a fonte da aplicação
		final String PATH_FONTS = ROOT_DIR + "fonts/";
		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
			if ( isLowMemory() ) {
				if ( i >= FONT_PERFECT && i <= FONT_TOTAL_SCORE ) {
					FONTS[ i ] = getFont( FONT_MENU );
				} else {
					FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_FONTS + i );
				}
			} else {
				FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_FONTS + i );
				if ( i >= FONT_PERFECT && i <= FONT_TOTAL_SCORE )
					FONTS[ i ].setCharExtraOffset( -1 );
			}
		}
		FONTS[ FONT_MENU ].setCharExtraOffset( BIG_FONT_OFFSET );
		FONTS[ FONT_TEXT_WHITE ] .setCharExtraOffset( DEFAULT_FONT_OFFSET );

		//#if SCREEN_SIZE =="SUPER_BIG"
//# 			getFont(FONT_PERFECT).setCharExtraOffset(1);
//# 			getFont(FONT_GREAT).setCharExtraOffset(1);
//# 			getFont(FONT_GOOD).setCharExtraOffset(1);
//# 			getFont(FONT_REGULAR).setCharExtraOffset(1);
//# 			getFont(FONT_BAD).setCharExtraOffset(1);
//# 			getFont(FONT_MISS).setCharExtraOffset(1);
		//#endif
		
	    borderedScreen = new BorderedScreen();

		softkeyLeft = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_LEFT );
		softkeyRight = new BasicAnimatedSoftkey( ScreenManager.SOFT_KEY_RIGHT );
		
//		loadTexts(TEXT_TOTAL, ROOT_DIR + "en.dat" );

		manager.setSoftKey( ScreenManager.SOFT_KEY_LEFT, softkeyLeft );
		manager.setSoftKey( ScreenManager.SOFT_KEY_RIGHT, softkeyRight );
		
		// Determina uma cor de fundo
		manager.setBackgroundColor( DEFAULT_LIGHT_COLOR );
		
		//#if LOG == "true"
//# 		Logger.setRMS( DATABASE_NAME, DATABASE_SLOT_LOG );
//# 		Logger.loadLog( DATABASE_NAME, DATABASE_SLOT_LOG );
		//#endif

		try{
			AppMIDlet.loadData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
		} catch (Exception e) {
			language = NanoOnline.LANGUAGE_en_US;
		}

		//#if QUALCOMM_API == "false"
			setScreen(SCREEN_CHOOSE_LANGUAGE);
		//#endif
	}

	
	private final void loadImagesAndSounds() throws Exception
	{
		//#ifdef NO_SOUND
//# 		MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, null );
//# 		MediaPlayer.setMute( true );
		//#else
			final String[] SOUNDS = new String[SOUND_TOTAL];

			for( byte i = 0; i < SOUND_TOTAL; ++i )
				SOUNDS[i] = ROOT_DIR + i + ".mid";

			MediaPlayer.init( DATABASE_NAME, DATABASE_SLOT_OPTIONS, SOUNDS );
		//#endif

		resourcesLoaded = true;
	}


	public static synchronized void blockScreenChange( boolean block )
	{
		(( GameMIDlet )instance ).blockScreenChange = block;
	}

	
	public static final void setSoftKey( byte softKey, Drawable d, int visibleTime )
	{
		final GameMIDlet midlet = ( ( GameMIDlet ) instance );
		midlet.manager.setSoftKey( softKey, d );
	}


	public synchronized final void onChoose( Menu menu, int id, int index )
	{
		switch( id )
		{
			case SCREEN_MAIN_MENU:
				//#if BLACKBERRY_API == "true"
//# 					if ( !showRecommendScreen && index >= MAIN_MENU_RECOMMEND ) {
//#  						++index;
//#  					}
				//#endif

				//#ifndef RECOMMEND_SCREEN
					if ( index >= MAIN_MENU_RECOMMEND ) {
						++index;
					}
				//#endif

				//#ifndef NO_RECOMMEND
					//#if WEB_EMULATOR == "false"
//# 							if ( !SmsSender.isSupported() && index >= MAIN_MENU_RECOMMEND ) {
//# 								++index;
//# 							}
					//#endif
				//#endif
				switch( index )
				{
					case MAIN_MENU_NEW_GAME:
						//#if NANO_RANKING == "true"
							setScreen( SCREEN_CHOOSE_PROFILE );
						//#else
//# 							setScreen( SCREEN_CHOOSE_DANCER );
						//#endif
						break;

					case MAIN_MENU_OPTIONS:
						setScreen( SCREEN_OPTIONS );
						break;

					case MAIN_MENU_RECOMMEND:
						setScreen( SCREEN_LOADING_RECOMMEND_SCREEN );
					break;

					case MAIN_MENU_NANO_ONLINE:
						setScreen( SCREEN_LOADING_NANO_ONLINE );
						break;

					case MAIN_MENU_HELP:
						setScreen( SCREEN_HELP );
						break;

					case MAIN_MENU_CREDITS:
						setScreen( SCREEN_CREDITS );
					break;

					//#if QUALCOMM_API == "true"
//# 						case MAIN_MENU_LICENSE_INFO:
//# 							setScreen( SCREEN_QUALCOMM_LICENSE_INFO );
//# 						break;
					//#endif

					//#if LOG == "true"
//# 					case MAIN_MENU_LOG:
//# 						setScreen( SCREEN_LOG );
//# 						break;
					//#endif

					case MAIN_MENU_EXIT:
						exit();
						break;
				}
				break; // fim case SCREEN_MAIN_MENU
				
				//#if NANO_RANKING == "true"
					case SCREEN_CHOOSE_PROFILE:
						switch ( index ) {
							case BasicConfirmScreen.INDEX_YES:
								MediaPlayer.free();
								setScreen( SCREEN_CHOOSE_DANCER );
							break;

							case BasicConfirmScreen.INDEX_NO:
								setScreen( SCREEN_LOADING_PROFILES_SCREEN );
							break;

							case BasicConfirmScreen.INDEX_CANCEL:
								setScreen( SCREEN_MAIN_MENU );
							break;
						}
					break;
				//#endif				


				case SCREEN_DIFFICULTY:
					switch ( index ) {
						case OPTIONS_DIFICULT_EASY:
							PlayScreen.setDifficulty( DIFICULT_EASY );
							setScreen( SCREEN_LOAD_GAME );
							break;
						case OPTIONS_DIFICULT_MEDIUM:
							PlayScreen.setDifficulty( DIFICULT_MEDIUM );
							setScreen( SCREEN_LOAD_GAME );
							break;
						case OPTIONS_DIFICULT_HARD:
							PlayScreen.setDifficulty( DIFICULT_HARD );
							setScreen( SCREEN_LOAD_GAME );
							break;
						case MENU_DIFICULT_BACK_MAIN_MENU:
							setScreen( SCREEN_CHOOSE_COLORS );
							break;
					}
					break; // fim case SCREEN_DIFICULT
					
				case SCREEN_CHOOSE_LANGUAGE:
					try {
						switch (index) {

							case OPTION_ENGLISH:
								language = NanoOnline.LANGUAGE_en_US;
								loadTexts(TEXT_TOTAL, ROOT_DIR + "en.dat");
								setScreen(SCREEN_ENABLE_SOUNDS);
								break;
							case OPTION_PORTUGUESE:
								language = NanoOnline.LANGUAGE_pt_BR;
								loadTexts(TEXT_TOTAL, ROOT_DIR + "pt.dat");
								setScreen(SCREEN_ENABLE_SOUNDS);
								break;
						}
						AppMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_LANGUAGE, this);
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
						
						exit();
					}

                   break;

			//#if LOG == "true"
//# 			case SCREEN_LOG:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 				break;
			//#endif

			case SCREEN_SPLASH_BRAND:
				setScreen( SCREEN_SPLASH_GAME );
				break;

			case SCREEN_CREDITS:
				setScreen( SCREEN_MAIN_MENU );
				break;

			case SCREEN_HELP:
				switch( index )
				{
					case HELP_MENU_RULES:
						setScreen( SCREEN_HELP_RULES );
						break;
						
					case HELP_MENU_SPECIAL_ICONS:
						setScreen( SCREEN_HELP_SPECIAL_ICONS );
						break;
						
					case HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
						break;
						
					case HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
						break;
				}
				break;

			case SCREEN_PAUSE:
				if( MediaPlayer.isVibrationSupported() )
				{
					switch( index )
					{
						case PAUSE_MENU_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_UNPAUSE );
							break;

						//#ifndef NO_SOUND
						case PAUSE_MENU_TOGGLE_SOUND:
							MediaPlayer.setMute( !MediaPlayer.isMuted() );
						break;
						//#endif

						case PAUSE_MENU_TOGGLE_VIBRATION:
							MediaPlayer.setVibration( !MediaPlayer.isVibration() );
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
							break;

						case PAUSE_MENU_BACK_TO_MENU:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_BACK_TO_MENU );
							break;

						case PAUSE_MENU_EXIT_GAME:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_EXIT_GAME );
							break;
					}
				}
				else
				{
					switch( index )
					{
						case PAUSE_MENU_NO_VIB_CONTINUE:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_UNPAUSE );
							break;

						//#ifndef NO_SOUND
					case PAUSE_MENU_NO_VIB_TOGGLE_SOUND:
						MediaPlayer.setMute( !MediaPlayer.isMuted() );
						break;
						//#endif

						case PAUSE_MENU_NO_VIB_BACK_TO_MENU:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_BACK_TO_MENU );
							break;

						case PAUSE_MENU_NO_VIB_EXIT_GAME:
							GameMIDlet.setScreen( GameMIDlet.SCREEN_YN_EXIT_GAME );
							break;
					}
				}
				break; // fim case SCREEN_PAUSE												 

			case SCREEN_OPTIONS:
				if( MediaPlayer.isVibrationSupported() )
				{
					switch( index )
					{
						//#ifndef NO_SOUND
						case OPTIONS_MENU_TOGGLE_SOUND:
							break;
						//#endif
							
						case OPTIONS_MENU_TOGGLE_VIBRATION:
							MediaPlayer.vibrate( DEFAULT_VIBRATION_TIME );
							break;
							
						case OPTIONS_MENU_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				else
				{
					switch( index )
					{
						//#ifndef NO_SOUND
						case OPTIONS_MENU_NO_VIB_TOGGLE_SOUND:
							break;
						//#endif
							
						case OPTIONS_MENU_NO_VIB_BACK:
							MediaPlayer.saveOptions();
							setScreen( SCREEN_MAIN_MENU );
							break;
					}
				}
				break; // fim case SCREEN_OPTIONS


			case SCREEN_YN_BACK_TO_MENU:
				switch( index )
				{
					case BasicConfirmScreen.INDEX_YES:
						MediaPlayer.saveOptions();
						
						//#ifndef NO_SOUND
							//#if SAMSUNG_BAD_SOUND == "true"
//#									MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
							//#else
								MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
							//#endif
						//#endif

 						setScreen( SCREEN_MAIN_MENU );
						break;
 						
 					case BasicConfirmScreen.INDEX_NO:
						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_YN_EXIT_GAME:
				switch( index )
				{
					case BasicConfirmScreen.INDEX_YES:
 						GameMIDlet.exit();
						break;
 						
 					case BasicConfirmScreen.INDEX_NO:
 						setScreen( SCREEN_PAUSE );
						break;
				}
				break;

			case SCREEN_ENABLE_SOUNDS:
				MediaPlayer.setMute( index == BasicConfirmScreen.INDEX_NO );
				MediaPlayer.saveOptions();
				setScreen( SCREEN_SPLASH_NANO );
			break;
		} // fim switch ( id )
	}
	

	public void onItemChanged( Menu menu, int id, int index )
	{
	}


	public static final void gameOver( long score )
	{
		// Precisamos desalocar o away do jogo e alocar o away do fundo dos menus
		final GameMIDlet midlet = ( GameMIDlet )instance;
		midlet.playScreen.setState( PlayScreen.STATE_NONE );
		midlet.freePlayscreenMemory();

		//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				if ( score > 0 && RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true ) > 0 ) {
					setScreen( SCREEN_LOADING_HIGH_SCORES );
				} else {
					setScreen( SCREEN_MAIN_MENU );
				}
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				setScreen( SCREEN_MAIN_MENU );
			}
		//#else
//#
//# 			// Aloca o background do menu
//# 			midlet.manager.setBackground( midlet.createMenuBkg( true ), true );
//#
			//#ifndef SIEMENS
				//#ifndef NO_SOUND
					//#if SAMSUNG_BAD_SOUND == "true"
	//# 				MediaPlayer.play( SOUND_INDEX_THEME, SAMSUNG_LOOP_INFINITE );
					//#else
//# 						MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
					//#endif
				//#endif
			//#endif
//#
//# 			if( GameRecordsScreen.setScore( score ) )
//# 				setScreen( SCREEN_RECORDS );
//# 			else
//# 				setScreen( SCREEN_MAIN_MENU );
		//#endif
	}


	public static final byte isHighScore( long score ) {
		//#if NANO_RANKING == "true"
			try {
				final Customer c = NanoOnline.getCurrentCustomer();
				final byte typeRanking = RankingScreen.isHighScore( RANKING_TYPE_SCORE, c.getId(), score, true ) ;
				PlayScreen.setTyperanking(typeRanking);
				return typeRanking;
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
			return RankingScreen.SCORE_STATUS_NONE;
		//#else
//# 			return HighScoresScreen.isHighScore( score ) ;
		//#endif
	}


	/** Desaloca a tela de jogo e reseta as variáveis estáticas da classe PushItBar */
	private final void freePlayscreenMemory()
	{
		if( playScreen != null )
		{
			// - 1 => retiramos o Away da coleção do jogo
		     playScreen.getPushItBar().destruct();

			playScreen = null;
		}
		System.gc();
	}


	public static final boolean isLowMemory()
	{
		//#if ( LOW_JAR == "true" )
//# 		return true;
		//#else
			return (( GameMIDlet )getInstance() ).lowMemory;
		//#endif
	}
	
	/** Obtém o próximo inteiro múltiplo de m */
	public static final int nextMulOf( int n, int m )
	{
		final int aux = n % m;
		if( aux == 0 )
			return n;
		return n + ( m - aux );
	}

	
	public static final String formatNumStr( long value, int nChars, char filler, char separator )
	{
		final char[] valueStr = String.valueOf( value ).toCharArray();

		final boolean hasSeparator = ( separator != '\0' );
		final char[] s = new char[ !hasSeparator ? nChars : nChars + ( nextMulOf( nChars, 3 ) / 3 ) - 1 ];

		int i, sCounter = s.length - 1;
		for( i = 0 ; i < nChars ; ++i )
		{
			if( hasSeparator && ( i > 0 ) && ( i % 3 == 0 ) )
				s[ sCounter-- ] = separator;
			
			if( i < valueStr.length )
				s[ sCounter-- ] = valueStr[ valueStr.length - i - 1 ];
			else
				s[ sCounter-- ] = filler;
		}

		return new String( s );
	}

	
	public final void write(DataOutputStream output) throws Exception {
		output.writeByte(language);
		output.writeBoolean(MediaPlayer.isMuted());
	}

	
	public final void read(DataInputStream input) throws Exception {

		language = input.readByte();

		input.readBoolean();
	}


	//#if QUALCOMM_API == "true"
//# 
//# 		protected final void onLicenseMessage( byte type, final byte action, String message ) {
			//#if DEBUG == "true"
//# 				System.out.println( "onLicenseMessage( " + type + ", " + action + ", " + message + " )" );
			//#endif {
//# 
//# 			final Button[] buttons;
//# 
//# 			try {
//# 				switch ( type ) {
//# 					case MSG_TYPE_ERROR:
//# 						buttons = new Button[] {
//# 							new Button( getFont( FONT_TEXT_WHITE ), "OK" ),
//# 						};
//# 					break;
//# 
//# 					case MSG_TYPE_CONFIRMATION:
//# 					case MSG_TYPE_INFO:
//# 					case MSG_TYPE_WARNING:
//# 					default:
//# 						buttons = new Button[] {
//# 							new Button( getFont( FONT_TEXT_WHITE ), "OK" ),
//# 							new Button( getFont( FONT_TEXT_WHITE ), "CANCELAR" )
//# 						};
//# 					break;
//# 				}
//# 
//# 				for ( byte i = 0; i < buttons.length; ++i ) {
//# 					buttons[ i ].setBorder( getBorder() );
//# 					buttons[ i ].setSize( buttons[ i ].getSize().add( buttons[ i ].getBorder().getBorderSize() ) );
//# 				}
//# 
//# 				final LineBorder b = new LineBorder( 0x777777, LineBorder.TYPE_ROUND_RAISED );
//# 				b.setFillColor( 0x777777 );
//# 
//# 			
//# 				final Alert a = new Alert( Alert.TYPE_INFO, message, buttons, getFont( FONT_TEXT_WHITE ), b );
//# 				a.setListener( new MenuListener() {
//# 
//# 					public final void onChoose( Menu menu, int id, int index ) {
//# 						switch ( action ) {
//# 							case MSG_ACTION_EXIT:
//# 								exit();
//# 							break;
//# 
//# 							case MSG_ACTION_NONE:
//# 							break;
//# 
//# 							case MSG_ACTION_PURCHASE:
//# 								switch ( index ) {
//# 									case 0:
//# 										purchase();
//# 									break;
//# 
//# 									default:
//# 										exit();
//# 								}
//# 							break;
//# 
//# 							case MSG_ACTION_START:
//# 								setScreen( SCREEN_CHOOSE_LANGUAGE );
//# 							break;
//# 						}
//# 					}
//# 
//# 
//# 					public final void onItemChanged( Menu menu, int id, int index ) {
//# 					}
//# 
//# 				}, type );
//# 				a.show();
//# 			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 					ex.printStackTrace();
				//#endif
//# 			}
//# 
//# 		}
//# 
//# 
//# 		protected final void onValidLicense( License validLicense ) {
			//#if DEBUG == "true"
//# 				System.out.println( "onValidLicense( " + validLicense + " )" );
			//#endif
//# 			setScreen( SCREEN_CHOOSE_LANGUAGE );
//# 		}
//# 
//# 
	//#endif


	private static interface LoadListener {

		public void load( final LoadScreen loadScreen ) throws Exception;

	}


	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualização do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 600;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;

		private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;

		private boolean active = true;


		/**
		 *
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener ) throws Exception {
			super( AppMIDlet.getFont( FONT_MENU ), AppMIDlet.getText( TEXT_LOADING ) );

			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo são carregados aqui para evitar sobrecarga do método loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// só inicia a thread quando a tela atual for a ativa, para evitar possíveis atrasos em transições e
					// garantir que novos recursos só serão carregados quando a tela anterior puder ser desalocada por completo
					if ( borderedScreen.getState() == BorderedScreen.STATE_SHOWN && painted ) {
						//#if TOUCH == "true"
						//	ScreenManager.setPointerListener( null );
						//#endif

						final LoadScreen loadScreen = this;

						loadThread = new Thread() {

							public final void run() {
								try {
									AppMIDlet.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
										e.printStackTrace();
//										texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();
//
//										setScreen( SCREEN_ERROR_LOG );
										e.printStackTrace();
									GameMIDlet.exit();
								//#endif
								}
							} // fim do método run()

						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for ( byte i = 0; i < dots; ++i ) {
						temp += '.';
					}

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execução
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
							e.printStackTrace();
						//#endif
					}
				}
			}
		} // fim do método update( int )
		
		
		public final void paint( Graphics g ) {
			super.paint( g );
			painted = true;
		}


		private final void setActive( boolean a ) {
			active = a;
		}


	} // fim da classe interna LoadScreen

	
	private	final class HelpPushItBar extends UpdatableGroup implements SpriteListener
	{
		private final MUV animControl = new MUV( MIN_PUSHITS_SPEED );

		
		public HelpPushItBar() throws Exception
		{
			super( 7 );
			PushItBar.createHelpVersion( this );
		}

		
		public final void update( int delta )
		{
			super.update( delta );
			
			if( !drawables[ PushItBar.INDEX_FIRST_PUSHIT ].isVisible() )
				return;
			
			final int dx = animControl.updateInt( delta );
			if( dx == 0 )
				return;
			
			if( drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getPosX() + dx >= ( ( getWidth() - drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getWidth() ) >> 1 ) )
			{
				drawables[ PushItBar.INDEX_FIRST_PUSHIT ].setVisible( false );
				drawables[ PushItBar.INDEX_FIRST_PUSHIT ].setPosition( -drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getWidth(), drawables[ PushItBar.INDEX_FIRST_PUSHIT ].getPosY() );
				
				// Faz a animação do brilho
				final Sprite shine = ( Sprite )drawables[ PushItBar.INDEX_FIRST_PUSHIT + 1 ];
				shine.pause( false );
				shine.setVisible( true );
				shine.setSequence( 0 );
			}
			else
			{
				drawables[ PushItBar.INDEX_FIRST_PUSHIT ].move( dx, 0 );
			}
		}

		public final void onSequenceEnded( int id, int sequence )
		{
			final Sprite shine = ( Sprite )drawables[ PushItBar.INDEX_FIRST_PUSHIT + 1 ];
			shine.pause( true );
			shine.setVisible( false );
			drawables[ PushItBar.INDEX_FIRST_PUSHIT ].setVisible( true );
		}


		public final void onFrameChanged( int id, int frameSequenceIndex )
		{
		}
	}

	/**
	 * Define uma soft key a partir de um texto. Equivalente à chamada de <code>setSoftKeyLabel(softKey, textIndex, 0)</code>.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		setSoftKeyLabel( softKey, textIndex, ScreenManager.getInstance().hasPointerEvents() ? 0 : SOFT_KEY_VISIBLE_TIME );
	}


	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey índice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 * @param visibleTime tempo que o label permanece visível. Para o label estar sempre visível, basta utilizar zero.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex, int visibleTime ) {
		if ( softKey == ScreenManager.SOFT_KEY_RIGHT )
			softKeyRightText = textIndex;
		
		if ( textIndex < 0 ) {
			setSoftKey( softKey, null, true, 0 );
		} else {
			try {
				setSoftKey( softKey, new Label( FONT_TEXT_WHITE, textIndex ), true, visibleTime );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	} // fim do método setSoftKeyLabel( byte, int )

	
	protected final int changeScreen( int screen ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

			//#if SCREEN_SIZE == "MEDIUM"
//# 			// Alguém pode ter mudado o espaçamento da fonte por fora
//# 			 getFont( FONT_MENU ).setCharExtraOffset( GameMIDlet.DEFAULT_FONT_OFFSET );
//#
		    //#endif

		byte bkgType = BACKGROUND_TYPE_DEFAULT;
	//	log("changeScreen " + screen);
		boolean resizeScreen = false;


		if ( screen != currentScreen ) {
			Drawable nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexSoftLeft = SOFT_KEY_REMOVE;
			
			int softKeyVisibleTime = 0;
			
			try {
				switch (screen) {
					case SCREEN_ENABLE_SOUNDS:
						if (!resourcesLoaded) {
							loadImagesAndSounds();
						}

						nextScreen = new BasicConfirmScreen(midlet, screen, getFont(FONT_MENU), getCursor(), TEXT_DO_YOU_WANT_SOUND, TEXT_YES, TEXT_NO, -1, false);
						((BasicConfirmScreen) nextScreen).setEntriesAlignment(BasicConfirmScreen.ALIGNMENT_VERTICAL);
						((BasicConfirmScreen) nextScreen).setCurrentIndex(MediaPlayer.isMuted() ? 2 : 1);
						((BasicConfirmScreen) nextScreen).setSpacing(MENU_ITEMS_SPACING << 1, MENU_ITEMS_SPACING);

						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;
						break;

					case SCREEN_SPLASH_NANO:
                        bkgType = BACKGROUND_TYPE_NONE;

						if ( !resourcesLoaded ) {
							loadImagesAndSounds();
						}

						//#if SCREEN_SIZE == "SMALL"
//# 							nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
						//#elif SCREEN_SIZE != "SUPER_BIG"
							nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );
						//#else
//#
//# 							nextScreen = new SplashNano();
						//#endif

						//#ifndef NO_SOUND
                        	MediaPlayer.play( SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE );
						//#endif
						break;

					case SCREEN_SPLASH_GAME:
					
						nextScreen = new SplashGame( getFont( FONT_MENU ) );
                        bkgType = BACKGROUND_TYPE_NONE;
						break;

					case SCREEN_MAIN_MENU:
						freePlayscreenMemory();
						System.gc();
						
						//#if NANO_RANKING == "true"
							if ( nanoOnlineForm != null ) {
								NanoOnline.unload();
								nanoOnlineForm = null;
							}
						//#endif

						//#ifndef NO_RECOMMEND
							recommendScreen = null;
						//#endif

						// Se está vindo do splash do jogo...
						if ( currentScreen == SCREEN_YN_BACK_TO_MENU ) {
							// Precisamos alocar o Away do menu e desalocar o Away do jogo
							setBackground( BACKGROUND_TYPE_DEFAULT );
						}

						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new BasicMenu( midlet, screen, getFont( FONT_MENU ), new int[] {
									TEXT_NEW_GAME,
									TEXT_OPTIONS,
									TEXT_NANO_ONLINE,
									//#if RECOMMEND_SCREEN == "true"
//# 										TEXT_RECOMMEND_TITLE,
									//#endif
									TEXT_HELP,
									TEXT_CREDITS,
									//#if QUALCOMM_API == "true"
//# 										TEXT_LICENSE_INFO,
									//#endif
									TEXT_EXIT,
									//#if LOG == "true"
								//# 												  TEXT_LOG,
								//#endif
								}, MENU_ITEMS_SPACING, MAIN_MENU_NEW_GAME, MAIN_MENU_EXIT );
						( ( BasicMenu ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						bkgType = BACKGROUND_TYPE_DEFAULT;
						break;

					case SCREEN_LOAD_GAME:
                        bkgType = BACKGROUND_TYPE_NONE;
						nextScreen = new LoadScreen( new LoadListener() {

							public final void load( LoadScreen loadScreen ) throws Exception {
								// Precisamos desalocar o away do menu, pois iremos alocar o away do jogo
								freePlayscreenMemory();
								
								MediaPlayer.free();
								setSpecialKeyMapping( true );
								playScreen = new PlayScreen();
								loadScreen.setActive( false );
								setScreen( SCREEN_GAME );
							}
						} );
						break;

					case SCREEN_OPTIONS:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						if ( MediaPlayer.isVibrationSupported() ) {
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_MENU, new int[]{
//# 																 TEXT_TURN_VIBRATION_OFF,
//# 																 TEXT_BACK,
//# 															 }, OPTIONS_MENU_BACK, -1, -1, OPTIONS_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
							//#else
							nextScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU ), new int[] {
										TEXT_TURN_SOUND_OFF,
										TEXT_TURN_VIBRATION_OFF,
										TEXT_BACK }, 
										OPTIONS_MENU_BACK, OPTIONS_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, OPTIONS_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON, SOUND_INDEX_THEME, MediaPlayer.LOOP_INFINITE
									);
							//#endif
						} else {
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_MENU, new int[]{
//# 																 TEXT_RECORDS,
//# 																 TEXT_BACK,
//# 															 }, OPTIONS_MENU_NO_VIB_BACK, -1, -1, -1, -1 );
							//#else
							nextScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU ), new int[] {
										TEXT_TURN_SOUND_OFF,
										TEXT_RECORDS,
										TEXT_BACK, }, OPTIONS_MENU_NO_VIB_BACK, OPTIONS_MENU_NO_VIB_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1, SOUND_INDEX_THEME,
																 //#if SAMSUNG_BAD_SOUND == "true"
																 //#																	SAMSUNG_LOOP_INFINITE
																 //#else
																 MediaPlayer.LOOP_INFINITE
																 //#endif
									);
						//#endif
						}
						( ( BasicOptionsScreen ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_CREDITS:
						resizeScreen = true;
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, GameMIDlet.getFont( FONT_TEXT_WHITE ), TEXT_CREDITS_TEXT, true );
						break;

					case SCREEN_HELP:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new BasicMenu( midlet, screen, getFont( FONT_MENU ), new int[] {
									TEXT_HELP_RULES,
									TEXT_HELP_SPECIAL_ICONS,
									TEXT_HELP_CONTROLS,
									TEXT_BACK
								}, MENU_ITEMS_SPACING, HELP_MENU_RULES, HELP_MENU_BACK );

						( ( BasicMenu ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_HELP_RULES:
						resizeScreen = true;
						 {
							indexSoftRight = TEXT_BACK;
							indexSoftLeft = TEXT_OK;

							final Drawable[] specialCharsA = new Drawable[ PLAY_FEEDBACK_TYPES_TOTAL + 1 ];
							
							specialCharsA[0] = new HelpPushItBar();
							
							for ( byte i = 1; i < specialCharsA.length; ++i ) {
								specialCharsA[ i ] = new Label( getFont( FONT_PERFECT + i - 1 ), TEXT_MOVE_PERFECT + i - 1 );
							}

							nextScreen = new BasicTextScreen( SCREEN_HELP, GameMIDlet.getFont( FONT_TEXT_WHITE ), getText( TEXT_HELP_TEXT_RULES ) + getMIDletVersion(), false, specialCharsA );
							( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollFull() ).setFillColor( DEFAULT_DARK_COLOR );
							( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollPage() ).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
							nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
						}
						break;

					case SCREEN_HELP_SPECIAL_ICONS:
						resizeScreen = true;
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						final Drawable[] specialCharsB = new Drawable[ 4 ];

						specialCharsB[0] = new DrawableImage( ROOT_DIR + "s" + IMG_EXT );

						specialCharsB[1] = new DrawableImage( ROOT_DIR + "q_0" + IMG_EXT );

						specialCharsB[2] = new DrawableImage( ROOT_DIR + "x4" + IMG_EXT );

						specialCharsB[3] = new DrawableImage( ROOT_DIR + "bomba" + IMG_EXT );

						nextScreen = new BasicTextScreen( SCREEN_HELP, GameMIDlet.getFont( FONT_TEXT_WHITE ), getText( TEXT_HELP_TEXT_SPECIAL_ICONS ) + getMIDletVersion(), false, specialCharsB );
						( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollFull() ).setFillColor( DEFAULT_DARK_COLOR );
						( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollPage() ).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
						nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
						break;

					case SCREEN_HELP_CONTROLS:
						resizeScreen = true;
						 {
							indexSoftRight = TEXT_BACK;
							indexSoftLeft = TEXT_OK;

							final Drawable[] specialCharsC = new Drawable[ 5 ];

							for ( int i = 2; i <= 8; i += 2 ) {
								specialCharsC[( i >> 1 ) - 1] = new DrawableImage( ROOT_DIR + i + IMG_EXT );
							}

							specialCharsC[4] = new DrawableImage( ROOT_DIR + "s" + IMG_EXT );

				  		   // nextScreen = new BasicTextScreen( SCREEN_HELP, GameMIDlet.getFont( FONT_TEXT_WHITE ), shushu.toString(), false, specialCharsC );
							nextScreen = new BasicTextScreen( SCREEN_HELP, GameMIDlet.getFont( FONT_TEXT_WHITE ), getText( TEXT_HELP_TEXT_CONTROLS ) + getMIDletVersion(), false, specialCharsC );
							( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollFull() ).setFillColor( DEFAULT_DARK_COLOR );
							( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollPage() ).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
							nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
						}
						break;

					case SCREEN_PAUSE:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						if ( MediaPlayer.isVibrationSupported() ) {
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_MENU, new int[]{
//# 																 TEXT_CONTINUE,
//# 																 TEXT_TURN_VIBRATION_OFF,
//# 																 TEXT_BACK_MENU,
//# 																 TEXT_EXIT_GAME,
//# 															 }, PAUSE_MENU_CONTINUE, -1, -1, PAUSE_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON );
							//#else
							nextScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU ), new int[] {
										TEXT_CONTINUE,
										TEXT_TURN_SOUND_OFF,
										TEXT_TURN_VIBRATION_OFF,
										TEXT_BACK_MENU,
										TEXT_EXIT, }, PAUSE_MENU_CONTINUE, PAUSE_MENU_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, PAUSE_MENU_TOGGLE_VIBRATION, TEXT_TURN_VIBRATION_ON, -1, -1 );
						//#endif
						} else {
							//#ifdef NO_SOUND
//# 							nextScreen = new BasicOptionsScreen( midlet, screen, FONT_MENU, new int[]{
//# 																 TEXT_CONTINUE,
//# 																 TEXT_BACK_MENU,
//# 																 TEXT_EXIT_GAME,
//# 															 }, PAUSE_MENU_NO_VIB_CONTINUE, -1, -1, -1, -1 );
							//#else
							nextScreen = new BasicOptionsScreen( midlet, screen, getFont( FONT_MENU ), new int[] {
										TEXT_CONTINUE,
										TEXT_TURN_SOUND_OFF,
										TEXT_BACK_MENU,
										TEXT_EXIT, }, PAUSE_MENU_NO_VIB_CONTINUE, PAUSE_MENU_NO_VIB_TOGGLE_SOUND, TEXT_TURN_SOUND_ON, -1, -1, -1, -1 );
						//#endif
						}
						( ( BasicOptionsScreen ) nextScreen ).setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, BasicMenu.ANCHOR_LEFT | BasicMenu.ANCHOR_VCENTER );
						break;

					case SCREEN_YN_BACK_TO_MENU:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), getCursor(), TEXT_BACK_MENU_2, TEXT_YES, TEXT_NO, -1, false );
						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
						( ( BasicConfirmScreen ) nextScreen ).setSpacing( MENU_ITEMS_SPACING << 1, MENU_ITEMS_SPACING );
						break;

					case SCREEN_YN_EXIT_GAME:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), getCursor(), TEXT_EXIT_GAME, TEXT_YES, TEXT_NO, -1, false );
						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
						( ( BasicConfirmScreen ) nextScreen ).setSpacing( MENU_ITEMS_SPACING << 1, MENU_ITEMS_SPACING );
						break;

					//#if NANO_RANKING == "true"

					case SCREEN_RECORDS:
						setSpecialKeyMapping( false );

						nextScreen = nanoOnlineForm;
						bkgType = BACKGROUND_TYPE_NONE;
						break;

					case SCREEN_NANO_RANKING_MENU:
						nextScreen = nanoOnlineForm;
						bkgType = BACKGROUND_TYPE_NONE;
						break;

					case SCREEN_LOADING_NANO_ONLINE:
						nextScreen = new LoadScreen( new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								MediaPlayer.free();
								setSpecialKeyMapping( false );
								
								nanoOnlineForm = NanoOnline.load( language, APP_SHORT_NAME, SCREEN_MAIN_MENU );

								loadScreen.setActive( false );

								setScreen( SCREEN_NANO_RANKING_MENU );
							}

						} );

						break;

					case SCREEN_NANO_RANKING_PROFILES:
						nextScreen = nanoOnlineForm;
						bkgType = BACKGROUND_TYPE_NONE;
						break;

					case SCREEN_LOADING_PROFILES_SCREEN:
						nextScreen = new LoadScreen( new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								setSpecialKeyMapping( false );
								nanoOnlineForm = NanoOnline.load( language, APP_SHORT_NAME, SCREEN_CHOOSE_PROFILE, NanoOnline.SCREEN_PROFILE_SELECT );

								loadScreen.setActive( false );

								setScreen( SCREEN_NANO_RANKING_PROFILES );
							}


						} );
						break;

					case SCREEN_CHOOSE_PROFILE:
						setSpecialKeyMapping( false );
						
						final Customer c = NanoOnline.getCurrentCustomer();
						
						// a fonte só possui caracteres maiúsculos
						final String name = ( getText( TEXT_CURRENT_PROFILE ) + ( c.getId() == Customer.ID_NONE ? getText( TEXT_NO_PROFILE ) : c.getNickname() ) ).toUpperCase();
						
						final Label title = new RichLabel( FONT_MENU, name, ScreenManager.SCREEN_WIDTH * 9 / 10 );
						final Label labelConfirm = new Label( FONT_MENU, TEXT_CONFIRM );
						final Label labelChoose = new Label( FONT_MENU, TEXT_CHOOSE_ANOTHER );
						final Label labelBack = new Label( FONT_MENU, TEXT_BACK );

						nextScreen = new BasicConfirmScreen( midlet, screen, getFont( FONT_MENU ), getCursor(), 
															 title, labelConfirm, labelChoose, labelBack, false );
						( ( BasicConfirmScreen ) nextScreen ).setEntriesAlignment( BasicConfirmScreen.ALIGNMENT_VERTICAL );
						( ( BasicConfirmScreen ) nextScreen ).setCurrentIndex( BasicConfirmScreen.INDEX_YES );
						( ( BasicConfirmScreen ) nextScreen ).setSpacing( MENU_ITEMS_SPACING << 1, MENU_ITEMS_SPACING );
						indexSoftLeft = TEXT_OK;
					break;

					case SCREEN_LOADING_HIGH_SCORES:
						nextScreen = new LoadScreen( new LoadListener() {

							public final void load( final LoadScreen loadScreen ) throws Exception {
								MediaPlayer.free();

								setSpecialKeyMapping( false );
								playScreen = null;
								nanoOnlineForm = NanoOnline.load( language, APP_SHORT_NAME, SCREEN_MAIN_MENU, NanoOnline.SCREEN_NEW_RECORD );

								loadScreen.setActive( false );

								setScreen( SCREEN_RECORDS );
							}


						} );
						break;

					//#else
//#  					case SCREEN_RECORDS:
//# 						indexSoftRight = TEXT_BACK;
//# 						indexSoftLeft = TEXT_OK;
//#
//# 						nextScreen = GameRecordsScreen.createInstance( FONT_MENU );
//# 						break;
					//#endif

					//#if LOG == "true"
//# 					case SCREEN_LOG:
//# 						Logger.log( log );
//# 						nextScreen = Logger.getLogScreen( SCREEN_MAIN_MENU, GameMIDlet.getFont( FONT_TEXT_WHITE ) );
//# 						(( Pattern )(( BasicTextScreen )nextScreen).getScrollFull()).setFillColor( DEFAULT_DARK_COLOR );
//# 						(( Pattern )(( BasicTextScreen )nextScreen).getScrollPage()).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
//# 						break;
					//#endif

					// Vai do menu principal para o jogo
					case SCREEN_NEXT_LEVEL:
					  bkgType = BACKGROUND_TYPE_NONE;
						playScreen.onNewLevel();
					case SCREEN_GAME:
						//#if TOUCH == "true"
							if ( !ScreenManager.getInstance().hasPointerEvents() )
								softKeyVisibleTime = TIME_SOFT_KEY_VISIBLE;
						//#else
//# 							softKeyVisibleTime = TIME_SOFT_KEY_VISIBLE;
						//#endif
                        bkgType = BACKGROUND_TYPE_NONE;
	
						nextScreen = playScreen;

						indexSoftRight = TEXT_PAUSE;
						break;
						
					case SCREEN_RESULT:
						final ResultScreen resultScreen = playScreen.getResultScreen();
						nextScreen = resultScreen;
						
						indexSoftRight = TEXT_BACK;
					break;

					case SCREEN_DIFFICULTY:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						final int[] entries = new int[] {
							TEXT_DIFFICULTY_TITLE, // titulo do menu
							TEXT_DIFFICULTY_EASY,
							TEXT_DIFFICULTY_MEDIUM,
							TEXT_DIFFICULTY_HARD,
							TEXT_BACK };

						final BasicMenu difficulty = new BasicMenu( this, screen, getFont( FONT_MENU ), entries, 10, 1, entries.length - 1 );
						difficulty.setCurrentIndex( 2 );
						difficulty.setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_LEFT | Drawable.ANCHOR_VCENTER );
						nextScreen = difficulty;
						break;

					case SCREEN_CHOOSE_LANGUAGE:
						loadImagesAndSounds();
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						final String[] text = new String[] {
							"CHOOSE LANGUAGE", // titulo do menu
							"ENGLISH",
							"PORTUGUÊS" };
						final BasicMenu lang = new BasicMenu( this, screen, getFont( FONT_MENU ), text, MENU_ITEMS_SPACING, 1, 0, null );
						lang.setCursor( getCursor(), BasicMenu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_LEFT | Drawable.ANCHOR_VCENTER );
						if (language == NanoOnline.LANGUAGE_en_US) {
							lang.setCurrentIndex(OPTION_ENGLISH);
						} else {
							lang.setCurrentIndex(OPTION_PORTUGUESE);
						}
						nextScreen = lang;
						break;

					case SCREEN_CHOOSE_DANCER:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;

						nextScreen = new ChooseDancerScreen( midlet, screen );
						break;

					case SCREEN_CHOOSE_COLORS:
						indexSoftRight = TEXT_BACK;
						indexSoftLeft = TEXT_OK;
						
						nextScreen = new ChooseColor( midlet, screen );
					break;

					//#if QUALCOMM_API == "true"
//# 						case SCREEN_QUALCOMM_PURCHASE:
//# 							
//# 						break;
//# 
//# 						case SCREEN_QUALCOMM_LICENSE_INFO:
//# 							final StringBuffer licenseInfo = new StringBuffer();
//# 							licenseInfo.append( getText( TEXT_LICENSE_TYPE ) );
//# 							licenseInfo.append( LicenseString.getLicenseTypeString( license ) );
//# 							licenseInfo.append( '\n' );
//# 							licenseInfo.append( getText( TEXT_LICENSE_STATE ) );
//# 							licenseInfo.append( LicenseString.getLicenseValidity( license ) );
//# 							licenseInfo.append( '\n' );
//# 							if (license.isDateBased()) {
//# 								licenseInfo.append( getText( TEXT_LICENSE_EXPIRATION ) );
//# 								licenseInfo.append( new Date( license.getExpirationDate() ) );
//# 							}
//# 							if (license.isUseBased()) {
//# 								licenseInfo.append( getText( TEXT_LICENSE_USES_REMAINING ) );
//# 								licenseInfo.append( license.getUsesRemaining() );
//# 							}
//# 							nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_TEXT_WHITE ), licenseInfo.toString(), false );
//# 							indexSoftRight = TEXT_BACK;
//# 						break;
					//#endif

					//#if RECOMMEND_SCREEN == "true"
//#
//# 					case SCREEN_RECOMMEND_SENT:
//# 						resizeScreen = true;
//# 						nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, GameMIDlet.getFont( FONT_TEXT_WHITE ), getText( TEXT_RECOMMEND_SENT ) + getRecommendURL() + "\n\n", false );
//# 						( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollFull() ).setFillColor( DEFAULT_DARK_COLOR );
//# 						( ( Pattern ) ( ( BasicTextScreen ) nextScreen ).getScrollPage() ).setFillColor( DEFAULT_SCROLL_LIGHT_COLOR );
//# 						nextScreen.setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
//#
//# 						indexSoftRight = TEXT_BACK;
//# 						break;
//#
//# 					case SCREEN_RECOMMEND:
//# 						resizeScreen = true;
//# 						nextScreen = recommendScreen;
//#
//# 						indexSoftRight = TEXT_BACK;
//# 						break;
//#
//# 					case SCREEN_LOADING_RECOMMEND_SCREEN:
//# 						nextScreen = new LoadScreen( new LoadListener() {
//#
//# 							public final void load( final LoadScreen loadScreen ) throws Exception {
//# 								setSpecialKeyMapping( false );
//# 								final ScreenRecommend recommend = new ScreenRecommend( SCREEN_MAIN_MENU );
//# 								final Form f = new Form( recommend );
//#
								//#if SCREEN_SIZE == "BIG" && TOUCH == "true"
//# 									f.setTouchKeyPad( new TouchKeyPad( getFont( FONT_TEXT_WHITE ), new DrawableImage( PATH_IMAGES + "clear.png" ), new DrawableImage( "/online/shift.png" ), getBorder(), 0x002831, 0x428a9c, 0x215963 ) );
								//#endif
//#
//# 								recommendScreen = f;
//#
//# 								loadScreen.setActive( false );
//#
//# 								setScreen( SCREEN_RECOMMEND );
//# 							}
//# 						} );
//# 						break;
//#
					//#endif

					// Volta do menu principal para o jogo, logo precisa alocar o away do jogo novamente
					case SCREEN_CONTINUE:
						MediaPlayer.stop();

					// Sem break mesmo

					// Volta do menu de pause para o jogo
					case SCREEN_UNPAUSE:
						nextScreen = playScreen;
						playScreen.unpause();

						indexSoftRight = TEXT_PAUSE;
						break;

				} // fim switch ( screen )

				if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
					setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft, softKeyVisibleTime );

				if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
					setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight, softKeyVisibleTime );

				setBackground( bkgType );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif

				// Ocorreu um erro ao tentar trocar de tela, então sai do jogo
				exit();
			}

			if ( nextScreen != null ) {
				borderedScreen.setBkgVisible( bkgType != BACKGROUND_TYPE_NONE );
				borderedScreen.setNextScreen( nextScreen, resizeScreen );
				nextScreen = borderedScreen;

				midlet.manager.setCurrentScreen( nextScreen );

				return screen;
			} else {
				return -1;
			}
		} // fim if ( screen != currentScreen )
		return screen;
	}
	
	
	public static final Drawable getCursor() throws Exception {
		if ( cursor == null ) {
			cursor = new Sprite( ROOT_DIR + "cursorMenu" );
		}

		final Sprite c = new Sprite( cursor );
		c.defineReferencePixel( cursor.getWidth() + MENU_CURSOR_DIST_FROM_OPTS, cursor.getHeight() >> 1 );
		return c;
	}


	public static final ScrollBar getScrollBar() throws Exception {
		return new NanoOnlineScrollBar( NanoOnlineScrollBar.TYPE_VERTICAL, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT,
										COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Border getBorder() throws Exception {
		final LineBorder b = new LineBorder( 0xff7601, LineBorder.TYPE_ROUND_RAISED ) {
			public void setState( int state ) {
				super.setState( state );

				switch ( state ) {
					case STATE_PRESSED:
						setColor( 0xff4b00 );
						setFillColor( 0xff4b00 );
					break;

					case STATE_FOCUSED:
						setColor( 0xff8601 );
						setFillColor( 0xffbb00 );
					break;

					default:
						setColor( 0xfd9600 );
						setFillColor( 0xff7601 );
					break;
				}
			}
		};

		return b;
	}


	public static final void setSpecialKeyMapping( boolean specialMapping ) {
		ScreenManager.resetSpecialKeysTable();
		final Hashtable table = ScreenManager.SPECIAL_KEYS_TABLE;

		int[][] keys = null;
		final int offset = 'a' - 'A';

		//#if BLACKBERRY_API == "true"
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 				case ScreenManager.HW_LAYOUT_REDUCED_24:
//# 					keys = new int[][] {
//# 						{ 't', ScreenManager.KEY_NUM2 }, { 'T', ScreenManager.KEY_NUM2 },
//# 						{ 'y', ScreenManager.KEY_NUM2 }, { 'Y', ScreenManager.KEY_NUM2 },
//# 						{ 'd', ScreenManager.KEY_NUM4 }, { 'D', ScreenManager.KEY_NUM4 },
//# 						{ 'f', ScreenManager.KEY_NUM4 }, { 'F', ScreenManager.KEY_NUM4 },
//# 						{ 'j', ScreenManager.KEY_NUM6 }, { 'J', ScreenManager.KEY_NUM6 },
//# 						{ 'k', ScreenManager.KEY_NUM6 }, { 'K', ScreenManager.KEY_NUM6 },
//# 						{ 'b', ScreenManager.KEY_NUM8 }, { 'B', ScreenManager.KEY_NUM8 },
//# 						{ 'n', ScreenManager.KEY_NUM8 }, { 'N', ScreenManager.KEY_NUM8 },
//#
//# 						{ 'e', ScreenManager.KEY_NUM1 }, { 'E', ScreenManager.KEY_NUM1 },
//# 						{ 'r', ScreenManager.KEY_NUM1 }, { 'R', ScreenManager.KEY_NUM1 },
//# 						{ 'u', ScreenManager.KEY_NUM3 }, { 'U', ScreenManager.KEY_NUM3 },
//# 						{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 						{ 'c', ScreenManager.KEY_NUM7 }, { 'C', ScreenManager.KEY_NUM7 },
//# 						{ 'v', ScreenManager.KEY_NUM7 }, { 'V', ScreenManager.KEY_NUM7 },
//# 						{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 						{ 'g', ScreenManager.KEY_NUM5 }, { 'G', ScreenManager.KEY_NUM5 },
//# 						{ 'h', ScreenManager.KEY_NUM5 }, { 'H', ScreenManager.KEY_NUM5 },
//# 						{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 						{ 'w', ScreenManager.KEY_STAR }, { 'W', ScreenManager.KEY_STAR },
//# 						{ 's', ScreenManager.KEY_STAR }, { 'S', ScreenManager.KEY_STAR },
//# 						{ '*', ScreenManager.KEY_STAR }, { '#', ScreenManager.KEY_POUND },
//# 						{ 'l', ',' }, { 'L', ',' }, { ',', ',' },
//# 						{ 'o', '.' }, { 'O', '.' }, { 'p', '.' }, { 'P', '.' },
//# 						{ 'a', '?' }, { 'A', '?' }, { 's', '?' }, { 'S', '?' },
//# 						{ 'z', '@' }, { 'Z', '@' }, { 'x', '@' }, { 'x', '@' },
//#
//# 						{ '0', ScreenManager.KEY_NUM0 }, { ' ', ScreenManager.KEY_NUM0 },
//# 					 };
//# 				break;
//#
//# 				default:
//# 					if ( specialMapping ) {
//# 						keys = new int[][] {
//# 							{ 'w', ScreenManager.KEY_NUM1 }, { 'W', ScreenManager.KEY_NUM1 },
//# 							{ 'r', ScreenManager.KEY_NUM3 }, { 'R', ScreenManager.KEY_NUM3 },
//# 							{ 'z', ScreenManager.KEY_NUM7 }, { 'Z', ScreenManager.KEY_NUM7 },
//# 							{ 'c', ScreenManager.KEY_NUM9 }, { 'C', ScreenManager.KEY_NUM9 },
//# 							{ 'e', ScreenManager.KEY_NUM2 }, { 'E', ScreenManager.KEY_NUM2 },
//# 							{ 's', ScreenManager.KEY_NUM4 }, { 'S', ScreenManager.KEY_NUM4 },
//# 							{ 'd', ScreenManager.KEY_NUM5 }, { 'D', ScreenManager.KEY_NUM5 },
//# 							{ 'f', ScreenManager.KEY_NUM6 }, { 'F', ScreenManager.KEY_NUM6 },
//# 							{ 'x', ScreenManager.KEY_NUM8 }, { 'X', ScreenManager.KEY_NUM8 },
//#
//# 							{ 'y', ScreenManager.KEY_NUM1 }, { 'Y', ScreenManager.KEY_NUM1 },
//# 							{ 'i', ScreenManager.KEY_NUM3 }, { 'I', ScreenManager.KEY_NUM3 },
//# 							{ 'b', ScreenManager.KEY_NUM7 }, { 'B', ScreenManager.KEY_NUM7 },
//# 							{ 'm', ScreenManager.KEY_NUM9 }, { 'M', ScreenManager.KEY_NUM9 },
//# 							{ 'u', ScreenManager.UP }, { 'U', ScreenManager.UP },
//# 							{ 'h', ScreenManager.LEFT }, { 'H', ScreenManager.LEFT },
//# 							{ 'j', ScreenManager.FIRE }, { 'J', ScreenManager.FIRE },
//# 							{ 'k', ScreenManager.RIGHT }, { 'K', ScreenManager.RIGHT },
//# 							{ 'n', ScreenManager.DOWN }, { 'N', ScreenManager.DOWN },
//#
//# 							{ 'a', ScreenManager.KEY_STAR }, { 'A', ScreenManager.KEY_STAR },
//# 							{ 'q', ScreenManager.KEY_STAR }, { 'Q', ScreenManager.KEY_STAR },
//# 						 };
//# 					} else {
//# 						for ( char c = 'A'; c <= 'Z'; ++c ) {
//# 							table.put( new Integer( c ), new Integer( c ) );
//# 							table.put( new Integer( c + offset ), new Integer( c + offset ) );
//# 						}
//#
//# 						final int[] chars = new int[]
//# 						{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
//# 							'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
//# 						};
//#
//# 						for ( byte i = 0; i < chars.length; ++i )
//# 							table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
//# 					}
//# 			}
//#
		//#else

			if ( specialMapping ) {
				keys = new int[][] {
					{ 'q', ScreenManager.KEY_NUM1 },
					{ 'Q', ScreenManager.KEY_NUM1 },
					{ 'e', ScreenManager.KEY_NUM3 },
					{ 'E', ScreenManager.KEY_NUM3 },
					{ 'z', ScreenManager.KEY_NUM7 },
					{ 'Z', ScreenManager.KEY_NUM7 },
					{ 'c', ScreenManager.KEY_NUM9 },
					{ 'C', ScreenManager.KEY_NUM9 },
					{ 'w', ScreenManager.UP },
					{ 'W', ScreenManager.UP },
					{ 'a', ScreenManager.LEFT },
					{ 'A', ScreenManager.LEFT },
					{ 's', ScreenManager.FIRE },
					{ 'S', ScreenManager.FIRE },
					{ 'd', ScreenManager.RIGHT },
					{ 'D', ScreenManager.RIGHT },
					{ 'x', ScreenManager.DOWN },
					{ 'X', ScreenManager.DOWN },

					{ 'r', ScreenManager.KEY_NUM1 },
					{ 'R', ScreenManager.KEY_NUM1 },
					{ 'y', ScreenManager.KEY_NUM3 },
					{ 'Y', ScreenManager.KEY_NUM3 },
					{ 'v', ScreenManager.KEY_NUM7 },
					{ 'V', ScreenManager.KEY_NUM7 },
					{ 'n', ScreenManager.KEY_NUM9 },
					{ 'N', ScreenManager.KEY_NUM9 },
					{ 't', ScreenManager.KEY_NUM2 },
					{ 'T', ScreenManager.KEY_NUM2 },
					{ 'f', ScreenManager.KEY_NUM4 },
					{ 'F', ScreenManager.KEY_NUM4 },
					{ 'g', ScreenManager.KEY_NUM5 },
					{ 'G', ScreenManager.KEY_NUM5 },
					{ 'h', ScreenManager.KEY_NUM6 },
					{ 'H', ScreenManager.KEY_NUM6 },
					{ 'b', ScreenManager.KEY_NUM8 },
					{ 'B', ScreenManager.KEY_NUM8 },

					{ 10, ScreenManager.FIRE }, // ENTER
					{ 8, ScreenManager.KEY_CLEAR }, // BACKSPACE (Nokia E61)

					{ 'u', ScreenManager.KEY_STAR },
					{ 'U', ScreenManager.KEY_STAR },
					{ 'j', ScreenManager.KEY_STAR },
					{ 'J', ScreenManager.KEY_STAR },
					{ '#', ScreenManager.KEY_STAR },
					{ '*', ScreenManager.KEY_STAR },
					{ 'm', ScreenManager.KEY_STAR },
					{ 'M', ScreenManager.KEY_STAR },
					{ 'p', ScreenManager.KEY_STAR },
					{ 'P', ScreenManager.KEY_STAR },
					{ ' ', ScreenManager.KEY_STAR },
					{ '$', ScreenManager.KEY_STAR },
				 };
			} else {
				for ( char c = 'A'; c <= 'Z'; ++c ) {
					table.put( new Integer( c ), new Integer( c ) );
					table.put( new Integer( c + offset ), new Integer( c + offset ) );
				}

				final int[] chars = new int[]
				{	' ', ScreenManager.KEY_POUND, ScreenManager.KEY_STAR, '(', ')', '?', '!', ':', ';',
					'_', '-', '\'', '\"', '+', ',', '.', '@', '%', '$', '[', ']', '=', '&', '{', '}', 'ç', 'Ç'
				};

				for ( byte i = 0; i < chars.length; ++i )
					table.put( new Integer( chars[ i ] ), new Integer( chars[ i ] ) );
			}

		//#endif

		if ( keys != null ) {
			for ( byte i = 0; i < keys.length; ++i )
				table.put( new Integer( keys[ i ][ 0 ] ), new Integer( keys[ i ][ 1 ] ) );
		}
	}


	public static final String getRecommendURL() {
		final String url = GameMIDlet.getInstance().getAppProperty( APP_PROPERTY_URL );
		if ( url == null )
			return URL_DEFAULT;

		return url;
	}


	public static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;

		switch ( type ) {
			case BACKGROUND_TYPE_DEFAULT:
			case BACKGROUND_TYPE_NONE:
				borderedScreen.setBkgVisible( false );
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;

			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				borderedScreen.setBkgVisible( false );
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
			break;
		}
	} // fim do método setBackground( byte )


	public static final void setSoftKey( byte softKey, Drawable d, boolean changeNow, int visibleTime ) {
		switch ( softKey ) {
			case ScreenManager.SOFT_KEY_LEFT:
				if ( softkeyLeft != null ) {
					softkeyLeft.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;

			case ScreenManager.SOFT_KEY_RIGHT:
				if ( softkeyRight != null ) {
					softkeyRight.setNextSoftkey( d, visibleTime, changeNow );
				}
			break;
		}
	} // fim do método setSoftKey( byte, Drawable, boolean, int )


	public static final int getSoftKeyRightTextIndex() {
		return softKeyRightText;
	}
	
}
