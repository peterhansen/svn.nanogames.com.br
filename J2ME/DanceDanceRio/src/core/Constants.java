/**
 * Constants.java
 */

package core;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;

/**
 * ©2007 Nano Games
 * @author Daniel L. Alves
 */
public interface Constants {
	/** Nome curto do jogo, para submissão ao Nano Online. */
	public static final String APP_SHORT_NAME = "DRIO";

	/** URL passada por SMS ao indicar o jogo a um amigo. */
	public static final String APP_PROPERTY_URL = "DOWNLOAD_URL";

	public static final String URL_DEFAULT = "http://wap.nanogames.com.br";

	/***/
	public static final byte RANKING_TYPE_SCORE = 0;

	public static final int COLOR_BACKGROUND = 0x000000;
	public static final int COLOR_BACKGROUND_LIGHT = 0x174950;

	/***/
	public static final String HIGH_SCORE_DEFAULT_NICKNAME = "----";

	/** Caminho das imagens do jogo. */
	public static final String PATH_IMAGES = "/";

	/** Caminho das imagens das bordas. */
	public static final String PATH_BORDER = PATH_IMAGES + "border/";

	public static final String PATH_SCROLL = PATH_IMAGES + "scroll/";

	public static final String PATH_MENU = PATH_IMAGES + "menu/";

	public static final byte FONT_HITS			= 0;
	public static final byte FONT_MENU			= 1;
	/** Índice da fonte utilizada para textos. */
	public static final byte FONT_TEXT_WHITE	= 2;
	
	public static final byte FONT_PERFECT		= 3;
	public static final byte FONT_GREAT			= 4;
	public static final byte FONT_GOOD			= 5;
	public static final byte FONT_REGULAR		= 6;
	public static final byte FONT_BAD			= 7;
	public static final byte FONT_MISS			= 8;
	public static final byte FONT_TOTAL_SCORE	= 9;
	
	public static final byte FONT_POINTS		= 10;
	public static final byte FONT_BONUS			= 11;

	public static final byte FONT_TYPES_TOTAL = 12;

	/** Tempo que as soft keys ficam visíveis na tela */
	public static final short SOFT_KEY_VISIBLE_TIME = 1000;

	/** Número máximo de uma fase */
	public static final short MAX_LEVEL = 999;
		
	/** Fase na qual o jogo chega ao seu máximo de dificuldade */
	public static final byte MAX_DIFFICULTY_LEVEL = 20;
	
	/** Máxima pontuação possível */
	public static final long MAX_SCORE = 999999999999L;

	public static final short SCORE_PERFECT	= 6000;
	public static final short SCORE_GREAT	= 5000;
	public static final short SCORE_GOOD	= 4000;
	public static final short SCORE_AVERAGE	= 3000;
	public static final short SCORE_BAD		= 500;
	public static final short SCORE_ERROR	= 0;
		
	
	//#if DEBUG == "true"
		/** Número máximo de teclas que o jogador pode ter que apertar durante uma fase
		 * @see #MIN_PUSHITS
		 */
		public static final short MAX_PUSHITS = 40;

		public static final short MIN_PUSHITS = 3;
	//#else
//# 		/** Número máximo de teclas que o jogador pode ter que apertar durante uma fase
//# 		 * @see #MIN_PUSHITS
//# 		 */
//# 		public static final short MAX_PUSHITS = 80;
//# 		/** Número mínimo de teclas que o jogador pode ter que apertar durante uma fase
//# 		 * @see #MAX_PUSHITS
//# 		 */
//# 		public static final short MIN_PUSHITS = 10;
	//#endif
		
	/** Tempo máximo de duração do bônus da estrela (em milésimos)
	 * @see #MIN_STAR_TIME
	 */
	public static final short MAX_STAR_TIME = 15000;
	
	/** Tempo mínimo de duração do bônus da estrela (em milésimos)
	 * @see #MAX_STAR_TIME
	 */
	public static final short MIN_STAR_TIME = 10000;
	
	/** Número mínimo de pushIts de estrela que podem aparecer em uma fase
	 * @see #MAX_STAR_PUSHITS
	 */
	public static final byte MIN_STAR_PUSHITS = 0;
	
	/** Número máximo de pushIts de estrela que podem aparecer em uma fase
	 * @see #MIN_STAR_PUSHITS
	 */
	public static final byte MAX_STAR_PUSHITS = 12;
	
	/** Número mínimo de pushIts de interrogação que podem aparecer em uma fase
	 * @see #MAX_QM_PUSHITS
	 */
	public static final byte MIN_QM_PUSHITS = 0;
	
	/** Número máximo de pushIts de interrogação que podem aparecer em uma fase
	 * @see #MIN_QM_PUSHITS
	 */
	public static final byte MAX_QM_PUSHITS = 20;
	
	/** Número mínimo de pushIts vazios que podem aparecer em uma fase
	 * @see #MAX_NONE_PUSHITS
	 */
	public static final byte MIN_NONE_PUSHITS = 0;
	
	/** Número máximo de pushIts vazios que podem aparecer em uma fase
	 * @see #MIN_NONE_PUSHITS
	 */
	public static final byte MAX_NONE_PUSHITS = 10;
	
	/** Número máximo de pushIts vazios que podem aparecer juntos em uma fase */
	public static final byte MAX_NONE_PUSHITS_TOGETHER = 2;

    public static final short LIFE_MAX		= 32000;
	public static final short DANO_SEGUIDO	= LIFE_MAX / -8;
	public static final short DANO			= LIFE_MAX / -16;
	public static final short LIFE_UP		= LIFE_MAX / 30;

	/** De quanto em quanto tempo fornecemos pontos quando o jogador está acertando um rastro */
	public static final byte TRAIL_POINTS_TIME = 50;
	
	/** Até quando contabilizamos os hits */
	public static final short MAX_HIT_COUNT = 1000;
	
	/** Maior multiplicador bônus possível */
	public static final short MAX_BONUS_MULIPLIER = 999;
	
	/** Quanto o contador de bônus cresce ao receber um acerto PUSHIT_HIT_PERFECT */
	public static final byte PERFECT_BONUS_COUNTER_INC = 4;
	
	public static final byte PLAY_FEEDBACK_PERFECT		= 0;
	public static final byte PLAY_FEEDBACK_GREAT		= 1;
	public static final byte PLAY_FEEDBACK_GOOD			= 2;
	public static final byte PLAY_FEEDBACK_AVERAGE		= 3;
	public static final byte PLAY_FEEDBACK_BAD			= 4;
	public static final byte PLAY_FEEDBACK_MISS			= 5;
	
	public static final byte PLAY_FEEDBACK_TYPES_TOTAL	= PLAY_FEEDBACK_MISS + 1;
	
	/** Termo inicial da PA que regula o crescimento do multiplicador bônus */
	public static final byte BONUS_PA_A1 = 4;
	
	/** Razão da PA que regula o crescimento do multiplicador bônus */
	public static final byte BONUS_PA_R = 1;
	
	/** Tempo de visibilidade das imagens que indicam o tipo acerto */
	public static final short FEEDBACK_DUR = 500;
	
	/** Pontuação fornecida quando o jogador faz um acerto perfeito */
	public static final short PERFECT_HIT_N_POINTS = 587;
	
	/** Porcentagem de PERFECT_HIT_N_POINTS decrescida a cada categoria de acerto abaixo de PUSHIT_HIT_PERFECT */
	public static final byte UNDER_PERFECT_HIT_PERCENTAGE = 20;
	
    // tela de seleção de personagens
    public static final short CHOOSE_CHARACTER_DEFAULT_HEIGHT = 320;
	
	/** Tempo que o background leva para mudar completamente de cor nas telas de carregamento */
	public static final short LOAD_SCREEN_BKG_COLOR_CHANGE_TIME = 200;
			
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
		public static final byte MENU_AWAY_DIST_FROM_TITLE = 5;
		public static final byte MENU_CURSOR_DIST_FROM_OPTS = 3;
	//#else
//# 		public static final byte MENU_AWAY_DIST_FROM_TITLE = 5;
//# 		public static final byte MENU_CURSOR_DIST_FROM_OPTS = 1;
	//#endif
	
	/** Quantidade mínima de memória para que o jogo rode com sua capacidade máxima */
	//#if SCREEN_SIZE == "SMALL"
//# 		public static final int LOW_MEMORY_LIMIT = 900000;
	//#elif SCREEN_SIZE == "BIG"
		public static final int LOW_MEMORY_LIMIT = 1600000;
    //#else
//# 		public static final int LOW_MEMORY_LIMIT = 1200000;
	//#endif
	
	/** Tempo de duração do splash do jogo */
	public static final short SPLASH_GAME_DUR = 3000;
	
	/** Nome da base de dados */
	public static final String DATABASE_NAME = "n";
	
	/** Índice do slot de opções na base de dados */
	public static final byte DATABASE_SLOT_OPTIONS = 1;
	
	/** Índice do slot de recordes de pontos na base de dados */
	public static final byte DATABASE_SLOT_SCORES = 2;

	public static final byte DATABASE_SLOT_LANGUAGE = 3;

	
	//#if LOG == "true"
//# 	/** Índice do slot do log de erros */
//# 	public static final byte DATABASE_SLOT_LOG = 4;
	//#endif
	
	/** Número total de slots na base de dados */
	//#if LOG == "true"
//# 	public static final byte DATABASE_TOTAL_SLOTS = 4;
	//#else
	public static final byte DATABASE_TOTAL_SLOTS = 3;
	//#endif

	//<editor-fold defaultstate="collapsed" desc="Índices das músicas/sons do jogo">
	
	//#ifndef NO_SOUND

		/** Quantidade total de músicas / sons do jogo */
		public static final byte SOUND_TOTAL = 9;

		public static final byte SOUND_INDEX_THEME				= 0;
		public static final byte SOUND_INDEX_LEVEL_MUSIC_FUNK	= 1;
		public static final byte SOUND_INDEX_LEVEL_MUSIC_SAMBA	= 2;
		public static final byte SOUND_INDEX_EXPECTATION		= 3;
		public static final byte SOUND_INDEX_GAME_OVER			= 4;
		public static final byte SOUND_INDEX_NEW_LEVEL			= 5;
		public static final byte SOUND_INDEX_CHOOSE_CHARACTER	= 6;
		public static final byte SOUND_INDEX_LEVEL_MUSIC_FUNK_2	= 7;
		public static final byte SOUND_INDEX_LEVEL_MUSIC_SAMBA_2= 8;

		/** Número de músicas de cada personagem. */
		public static final byte SOUND_N_LEVEL_SOUNDS = 1;

		/** Velocidade mínima das músicas do jogo. */
		public static final int FP_SOUND_MIN_TEMPO_MULTIPLIER = NanoMath.ONE * 88 / 100;
		/** Velocidade máxima das músicas do jogo. */
		public static final int FP_SOUND_MAX_TEMPO_MULTIPLIER = NanoMath.ONE * 117 / 100;

	//#endif
	
	//</editor-fold>
		
	//<editor-fold defaultstate="collapsed" desc="Sequências do Away">

	public static final byte AWAY_SEQ_STATIC			= 0;
	public static final byte AWAY_SEQ_OPENING_LEGS		= 1;
	public static final byte AWAY_SEQ_TRAVOLTA_STYLE	= 2;
	public static final byte AWAY_SEQ_BRAND_NEW_THING	= 3;
	public static final byte AWAY_SEQ_VUPT				= 4;
	public static final byte AWAY_SEQ_BRAKE				= 5;
	public static final byte AWAY_SEQ_VANISH			= 6;
	//#if ( SCREEN_SIZE != "SMALL" ) && ( LOW_JAR == "false" )
	public static final byte AWAY_SEQ_APPEAR			= 7;
	//#endif
    /*Meu ingles não é lá essas coisas não mas eu acho que é isso =P*/
    public static final byte AWAY_SEQ_COMPLAINING	    = 8;
    public static final byte AWAY_SEQ_COMPLAINING_2	    = 9;
	
	/** Número de sequências utilizadas durante a fase */
	public static final byte AWAY_SEQ_N_LEVEL_SEQS		= 5;

	//</editor-fold>
	
	/** Strings compartilhadas pelo jogo inteiro */
	public static final String SPRITE_DESC_EXT = ".bin";
	public static final String FONT_DESC_EXT = ".dat";
	public static final String IMG_EXT = ".png";
	public static final String ROOT_DIR = "/";
	public static final String PATH_BKG = ROOT_DIR + "Bkg/";
			
	/** Caminho das imagens da tela de splash */
	public static final String PATH_SPLASH = "/splash/";
	
	/** Duração da vibração padrão */
	//#if SAMSUNG_BAD_SOUND == "true"
//#			public static final byte DEFAULT_VIBRATION_TIME = 80;
	//#else
		public static final short DEFAULT_VIBRATION_TIME = 300;
	//#endif
	
	//<editor-fold defaultstate="collapsed" desc="TEXTOS DO JOGO">

	public static final byte TEXT_BACK						= 0;
	public static final byte TEXT_NEW_GAME					= 1;
	public static final byte TEXT_EXIT						= 2;
	public static final byte TEXT_OPTIONS					= 3;
	public static final byte TEXT_CREDITS					= 4;
	public static final byte TEXT_CREDITS_TEXT				= 5;
	public static final byte TEXT_HELP						= 6;
	public static final byte TEXT_NEW_RECORD				= 7;
	public static final byte TEXT_HELP_TEXT_RULES			= 8;
	public static final byte TEXT_HELP_TEXT_SPECIAL_ICONS	= 9;
	public static final byte TEXT_HELP_TEXT_CONTROLS		= 10;
	public static final byte TEXT_TURN_SOUND_ON				= 11;
	public static final byte TEXT_TURN_SOUND_OFF			= 12;
	public static final byte TEXT_TURN_VIBRATION_ON			= 13;
	public static final byte TEXT_TURN_VIBRATION_OFF		= 14;
	public static final byte TEXT_CONTINUE					= 15;
	public static final byte TEXT_SPLASH_NANO				= 16;
	public static final byte TEXT_PAUSE						= 17;
	public static final byte TEXT_LOADING					= 18;
	public static final byte TEXT_DO_YOU_WANT_SOUND			= 19;
	public static final byte TEXT_YES						= 20;
	public static final byte TEXT_NO						= 21;
	public static final byte TEXT_EXIT_GAME					= 22;
	public static final byte TEXT_PRESS_ANY_KEY				= 23;
	public static final byte TEXT_RECORDS					= 24;
	public static final byte TEXT_ERASE_RECORDS				= 25;
	public static final byte TEXT_BACK_MENU					= 26;
	public static final byte TEXT_YOURE_DA_MAN				= 27;
	public static final byte TEXT_HITS						= 28;
	public static final byte TEXT_BONUS						= 29;
	public static final byte TEXT_RANKING					= 30;
	public static final byte TEXT_GAMEOVER					= 31;
	public static final byte TEXT_GET_READY					= 32;
	public static final byte TEXT_HELP_INTRO				= 33;
	public static final byte TEXT_HELP_RULES				= 34;
	public static final byte TEXT_HELP_SPECIAL_ICONS		= 35;
	public static final byte TEXT_HELP_CONTROLS				= 36;
	public static final byte TEXT_BACK_MENU_2				= 37;
	public static final byte TEXT_TOUCH_SCREEN_INTRO		= 38;
    public static final byte TEXT_CURRENT_PROFILE		    = TEXT_TOUCH_SCREEN_INTRO + 1;
    public static final byte TEXT_CHOOSE_ANOTHER		    = TEXT_CURRENT_PROFILE + 1;
    public static final byte TEXT_CONFIRM				    = TEXT_CHOOSE_ANOTHER + 1;
    public static final byte TEXT_NO_PROFILE				= TEXT_CONFIRM + 1;
    public static final byte TEXT_DIFFICULTY_EASY		    = TEXT_NO_PROFILE + 1;
    public static final byte TEXT_DIFFICULTY_MEDIUM		    = TEXT_DIFFICULTY_EASY + 1;
    public static final byte TEXT_DIFFICULTY_HARD		    = TEXT_DIFFICULTY_MEDIUM + 1;
    public static final byte TEXT_DIFFICULTY_TITLE		    = TEXT_DIFFICULTY_HARD + 1;
    public static final byte TEXT_FUNKERA		            = TEXT_DIFFICULTY_TITLE + 1;
    public static final byte TEXT_SKULL		                = TEXT_FUNKERA + 1;
    public static final byte TEXT_CHOOSE_DANCER	            = TEXT_SKULL + 1;
    public static final byte TEXT_CHOOSE_COLOR	            = TEXT_CHOOSE_DANCER + 1;
    public static final byte TEXT_RESULT                    = TEXT_CHOOSE_COLOR + 1;
    public static final byte TEXT_RECOMMEND_TITLE           = TEXT_RESULT + 1;
    public static final byte TEXT_RECOMMEND_TEXT            = TEXT_RECOMMEND_TITLE + 1;
    public static final byte TEXT_RECOMMEND_SMS             = TEXT_RECOMMEND_TEXT + 1;
    public static final byte TEXT_RECOMMEND_SENT            = TEXT_RECOMMEND_SMS + 1;
    public static final byte TEXT_NANO_ONLINE			    = TEXT_RECOMMEND_SENT + 1;
    public static final byte TEXT_OK			            = TEXT_NANO_ONLINE + 1;
    public static final byte TEXT_CLEAR						= TEXT_OK + 1;
    public static final byte TEXT_RESULT_TOTAL_ON_HIT_TRUE  = TEXT_CLEAR + 1;
    public static final byte TEXT_RESULT_TOTAL_ON_HIT_FALSE = TEXT_RESULT_TOTAL_ON_HIT_TRUE + 1;
	public static final byte TEXT_MOVE_PERFECT				= TEXT_RESULT_TOTAL_ON_HIT_FALSE + 1;
    public static final byte TEXT_MOVE_GREAT				= TEXT_MOVE_PERFECT + 1;
    public static final byte TEXT_MOVE_GOOD					= TEXT_MOVE_GREAT + 1;
    public static final byte TEXT_MOVE_AVERAGE				= TEXT_MOVE_GOOD + 1;
    public static final byte TEXT_MOVE_BAD					= TEXT_MOVE_AVERAGE + 1;
    public static final byte TEXT_MOVE_ERROR				= TEXT_MOVE_BAD + 1;
    public static final byte TEXT_RESULT_TOTAL			    = TEXT_MOVE_ERROR + 1;
	public static final byte TEXT_NEW_PERSONAL_RECORD            = TEXT_RESULT_TOTAL+1;
    public static final byte TEXT_NEW_PERSONAL_AND_LOCAL_RECORD  = TEXT_NEW_PERSONAL_RECORD+1;
	public static final byte TEXT_MINIMUM                        = TEXT_NEW_PERSONAL_AND_LOCAL_RECORD+1;
	public static final byte TEXT_LICENSE_TYPE                       = TEXT_MINIMUM+1;
	public static final byte TEXT_LICENSE_INFO                       = TEXT_LICENSE_TYPE + 1;
	public static final byte TEXT_LICENSE_STATE                       = TEXT_LICENSE_INFO + 1;
	public static final byte TEXT_LICENSE_USES_REMAINING          = TEXT_LICENSE_STATE + 1;
	public static final byte TEXT_LICENSE_EXPIRATION          = TEXT_LICENSE_USES_REMAINING + 1;

	//#if LOG == "true"
//# 	public static final byte TEXT_LOG					= 64;
	//#endif
		
	/** Número total de textos do jogo */
	public static final byte TEXT_TOTAL					        = TEXT_LICENSE_EXPIRATION + 2;

	//</editor-fold>

    /* indices dos personagens */
	public static final byte INDEX_DANCER_FUNK		= 0;
	public static final byte INDEX_DANCER_SKULL 	= 1;

	public static final byte TOTAL_DANCERS = 2;


    // variaveis de dificuldade do jogo
    public static final byte DIFICULT_EASY                   = 0;
    public static final byte DIFICULT_MEDIUM                 = MAX_DIFFICULTY_LEVEL >> 1;
    public static final byte DIFICULT_HARD                   = MAX_DIFFICULTY_LEVEL;

    public static final byte AWAY_STATIC                     = 0;
    public static final byte AWAY_MOVING                     = 1;

    public static final byte SPLASH_GAME_FUNK_OFFSET_X = 36;
	public static final short SPLASH_GAME_FUNK_OFFSET_Y = 80;
	public static final byte SPLASH_GAME_SKULL_OFFSET_X = 95;
	//#if SCREEN_SIZE == "BIG"
  	public static final short SPLASH_GAME_SKULL_OFFSET_Y = 80;
	//#elif SCREEN_SIZE == "SUPER_BIG"
//# 	public static final short SPLASH_GAME_SKULL_OFFSET_Y = 80;
	//#elif SCREEN_SIZE == "MEDIUM"
//# 	public static final short SPLASH_GAME_SKULL_OFFSET_Y = 140;
//# 
	//#elif  SCREEN_SIZE == "SMALL"
//# 	public static final short SPLASH_GAME_SKULL_OFFSET_Y = 45;
	//#endif


	public static final byte SPLASH_GAME_CAPOEIRA_OFFSET_X = 108;
	public static final short SPLASH_GAME_CAPOEIRA_OFFSET_Y = 110;

	public static final byte SPLASH_GAME_TITLE_OFFSET_X = 30;
	public static final byte SPLASH_GAME_TITLE_OFFSET_X_SPANISH = 0;
	public static final short SPLASH_GAME_TITLE_OFFSET_Y = 50;


    /*Índices das telas do jogo"*/

	public static final byte SCREEN_LOAD_RESOURCES				= 0;
	public static final byte SCREEN_ENABLE_SOUNDS				= 1;
	public static final byte SCREEN_CHANGE_BKG_COLOR			= 2;
	public static final byte SCREEN_SPLASH_NANO					= 3;
	public static final byte SCREEN_SPLASH_BRAND				= 4;
	public static final byte SCREEN_SPLASH_GAME					= 5;
	public static final byte SCREEN_MAIN_MENU					= 6;
	public static final byte SCREEN_LOAD_GAME					= 7;
	public static final byte SCREEN_OPTIONS						= 8;
	public static final byte SCREEN_CREDITS						= 9;
	public static final byte SCREEN_HELP						= 10;
	public static final byte SCREEN_HELP_INTRO					= 11;
	public static final byte SCREEN_HELP_RULES					= 12;
	public static final byte SCREEN_HELP_SPECIAL_ICONS			= 13;
	public static final byte SCREEN_HELP_CONTROLS				= 14;
	public static final byte SCREEN_RECORDS						= 15;
	public static final byte SCREEN_GAME						= 16;
	public static final byte SCREEN_PAUSE						= 17;
	public static final byte SCREEN_UNPAUSE						= 18;
	public static final byte SCREEN_CONTINUE					= 19;
	public static final byte SCREEN_GAMEOVER					= 20;
	public static final byte SCREEN_ENDING						= 21;
	public static final byte SCREEN_YN_EXIT_GAME				= 22;
	public static final byte SCREEN_YN_BACK_TO_MENU				= 23;
    public static final byte SCREEN_DIFFICULTY					= 24;
    public static final byte SCREEN_CHOOSE_DANCER				= 25;
    public static final byte SCREEN_CHOOSE_COLORS			    = 26;
    public static final byte SCREEN_CHOOSE_LANGUAGE			    = 27;
	public static final byte SCREEN_NANO_RANKING_MENU			= 28;
	public static final byte SCREEN_NANO_RANKING_PROFILES		= SCREEN_NANO_RANKING_MENU + 1;
	public static final byte SCREEN_LOADING_RECOMMEND_SCREEN	= SCREEN_NANO_RANKING_PROFILES + 1;
	public static final byte SCREEN_LOADING_NANO_ONLINE			= SCREEN_LOADING_RECOMMEND_SCREEN + 1;
	public static final byte SCREEN_LOADING_PROFILES_SCREEN		= SCREEN_LOADING_NANO_ONLINE + 1;
	public static final byte SCREEN_LOADING_HIGH_SCORES			= SCREEN_LOADING_PROFILES_SCREEN + 1;
	public static final byte SCREEN_CHOOSE_PROFILE				= SCREEN_LOADING_HIGH_SCORES + 1;
	public static final byte SCREEN_NO_PROFILE					= SCREEN_CHOOSE_PROFILE + 1;
	public static final byte SCREEN_RECOMMEND_SENT				= SCREEN_NO_PROFILE + 1;
	public static final byte SCREEN_RECOMMEND					= SCREEN_RECOMMEND_SENT + 1;
	public static final byte SCREEN_RESULT						= SCREEN_RECOMMEND + 1;
	public static final byte SCREEN_NEXT_LEVEL					= SCREEN_RESULT + 1;
	public static final byte SCREEN_QUALCOMM_CONNECTING			= SCREEN_NEXT_LEVEL + 1;
	public static final byte SCREEN_QUALCOMM_LICENSE_INVALID	= SCREEN_QUALCOMM_CONNECTING + 1;
	public static final byte SCREEN_QUALCOMM_PURCHASE			= SCREEN_QUALCOMM_LICENSE_INVALID + 1;
	public static final byte SCREEN_QUALCOMM_LICENSE_INFO		= SCREEN_QUALCOMM_PURCHASE + 1;

	// cores da barra de scroll
	public static final int COLOR_FULL_LEFT_OUT	= 0x000b0d;
	public static final int COLOR_FULL_FILL		= 0x32abca; //fundo barra
	public static final int COLOR_FULL_RIGHT	= 0x032026;
	public static final int COLOR_FULL_LEFT		= 0x245865;
	public static final int COLOR_PAGE_OUT		= 0x002b30;
	public static final int COLOR_PAGE_FILL		= 0x0F6981; //barra
	public static final int COLOR_PAGE_LEFT_1	= 0x32abca;
	public static final int COLOR_PAGE_LEFT_2	= 0x63a3ad; //sombra barra scroll

	
	
	public static final int DEFAULT_SCROLL_LIGHT_COLOR = COLOR_FULL_FILL;
	public static final int DEFAULT_LIGHT_COLOR = COLOR_FULL_FILL;
	public static final int DEFAULT_DARK_COLOR = COLOR_PAGE_FILL;	


    /*indices de imagens na roleta*/
    public static final byte ROULETTE_2                 = 2;
    public static final byte ROULETTE_4                 = 4;
    public static final byte ROULETTE_6                 = 6;
    public static final byte ROULETTE_8                 = 8;
	public static final byte ROULETTE_NONE              =10;

    public static final byte INDEX_STAR                 = 1;
    public static final byte INDEX_BOMB                 = 3;
    public static final byte INDEX_BONUS                = 5;

	/** Espessura da borda do tabuleiro. */
	
 	public static final byte BORDER_THICKNESS = 5;

	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "SUPER_BIG"
	 public static final byte AJUST_LIFE= 5;
	 //#elif SCREEN_SIZE == "MEDIUM"
//# 	 public static final byte AJUST_LIFE= 3;
	 //#elif SCREEN_SIZE == "SMALL"
//# 	 public static final byte AJUST_LIFE= 4;
	//#endif

	

	/** Espessura da borda do tabuleiro. */
	public static final int BORDER_HALF_THICKNESS = BORDER_THICKNESS >> 1;
	
	public static final int TIME_SOFT_KEY_VISIBLE = 4000;
	
	
	//#if SCREEN_SIZE == "BIG"

		public static final byte RESULTS_ITEMS_SPACING = 10;

		public static final short SCREEN_CHOOSE_DEFAULT_HEIGHT = 240;

		public static final byte SPLASH_DANCIN_X_OFFSET = -94;

		public static final byte SPLASH_RIO_X_OFFSET = 32;

		public static final byte LIFEBAR_START_X = 5;

		/** Folga que damos na mira para que o usuário não precise acertar o pushIt 100% */
		public static final byte AIM_DIST_ACCEPTANCE = 16;

		public static final byte CROWD_OFFSET_Y = -12;

		public static final short CHOOSE_PLAYER_CHARACTER_HEIGHT = 170;

		public static final byte FUNKEIRA_OFFSET_X = -8;

		public static final byte MENU_ITEMS_SPACING = 10;


	//#elif SCREEN_SIZE == "MEDIUM"
//# 		public static final byte RESULTS_ITEMS_SPACING = 5;
//# 
//# 		public static final short SCREEN_CHOOSE_DEFAULT_HEIGHT = 176;
//# 
//# 		public static final byte SPLASH_DANCIN_X_OFFSET = -94;
//# 
//# 		public static final byte SPLASH_RIO_X_OFFSET = 32;
//# 
//# 		public static final byte LIFEBAR_START_X = 3;
//# 
//# 		/** Folga que damos na mira para que o usuário não precise acertar o pushIt 100% */
//# 		public static final byte AIM_DIST_ACCEPTANCE = 12;
//# 
//# 		public static final byte CROWD_OFFSET_Y = -5;
//# 
//# 		public static final byte FUNKEIRA_Y_OFFSET = 25;
//# 
//# 		public static final short CHOOSE_PLAYER_CHARACTER_HEIGHT = 110;
//# 
//# 		public static final byte FUNKEIRA_OFFSET_X = -6;
//# 
//# 		public static final byte MENU_ITEMS_SPACING = 5;
//# 
	//#elif SCREEN_SIZE == "SMALL"
//# 		public static final byte RESULTS_ITEMS_SPACING = 4;
//#
//# 		public static final short SCREEN_CHOOSE_DEFAULT_HEIGHT = 128;
//#
//# 		public static final byte SPLASH_DANCIN_X_OFFSET = -47;
//#
//# 		public static final byte SPLASH_RIO_X_OFFSET = 15;
//#
//# 		public static final byte LIFEBAR_START_X = 2;
//#
//# 		/** Folga que damos na mira para que o usuário não precise acertar o pushIt 100% */
//# 		public static final byte AIM_DIST_ACCEPTANCE = 8;
//#
//# 		public static final byte CROWD_OFFSET_Y = 0;
//#
//# 		public static final byte FUNKEIRA_Y_OFFSET = 24;
//#
//#
//#
//# 		public static final short CHOOSE_PLAYER_CHARACTER_HEIGHT = 70;
//#
//# 		public static final byte FUNKEIRA_OFFSET_X = -3;
//#
//# 		public static final byte MENU_ITEMS_SPACING = 5;
//#
//# 		public static final byte LIFE_BAR_AJUST= 3;
		//#elif SCREEN_SIZE == "SUPER_BIG"
//# 			public static final byte RESULTS_ITEMS_SPACING = 10;
//# 			public static final short SCREEN_CHOOSE_DEFAULT_HEIGHT = 240;
//# 			public static final int SPLASH_DANCIN_X_OFFSET = -130;
//# 			public static final byte SPLASH_RIO_X_OFFSET = 60;
//# 			public static final byte LIFEBAR_START_X = 5;
//# 			/** Folga que damos na mira para que o usuário não precise acertar o pushIt 100% */
//# 			public static final byte AIM_DIST_ACCEPTANCE = 20;
//# 			public static final byte CROWD_OFFSET_Y = -12;
//# 			public static final short CHOOSE_PLAYER_CHARACTER_HEIGHT = 200;
//# 			public static final byte FUNKEIRA_OFFSET_X = -8;
//# 			public static final byte MENU_ITEMS_SPACING =15;
//# 
	//#endif

	//#if SCREEN_SIZE == "SUPER_BIG"
//# 	public static final int MIN_PUSHITS_SPEED = 50;
	//#elif SCREEN_SIZE == "BIG"
	public static final int MIN_PUSHITS_SPEED = 40;
	//#elif SCREEN_SIZE == "MEDIUM"
//# 	public static final int MIN_PUSHITS_SPEED = 30;
	//#elif SCREEN_SIZE == "SMALL"
//# 	public static final int MIN_PUSHITS_SPEED = 20;
//#
	//#endif
	public static final int MAX_PUSHITS_SPEED = MIN_PUSHITS_SPEED * 4;

	/** Espaçamento mínimo entre os pushIts */
	public static final byte DEFAULT_PUSHIT_SPACEMENT = AIM_DIST_ACCEPTANCE;
		
}