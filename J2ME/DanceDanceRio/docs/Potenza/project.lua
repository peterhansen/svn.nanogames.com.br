-- descritor para conversão de arquivos para o formato Potenza
-- Peter Hansen

-- exemplos de aparelhos-chefe com poucos caracteres
-- gradiente gf690: grgf690
-- motorola v3: mtv3
-- nokia n76: nkn76
-- samsung d820: sgd820
-- samsung c420: c420
-- lg me970: lgme970
-- lg mg155: lgmg155

-- nome do aplicativo/jogo
APP_NAME = [[GinasticaAway]]

-- caminho do arquivo original de especificação do aplicativo J2ME
PATH_APP_SPEC = [[326002-EST001-NanoGames.doc]]

-- caminho do formulário original de aprovação de aplicações Vivo Downloads
PATH_FAAVD = [[FAAVD_supportcomm.doc]]

-- caminho do arquivo original de solicitação de teste
PATH_SOL = [[326002-SOL001.01.1.xls]]

VERSIONS = {
	{
		-- big standard
		  ['jad'] = [[bs.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.3]]
		, ['device'] = [[sew910]]
	},
	{
		-- big icon 60x40
		  ['jad'] = [[b60x40.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.1]]
		, ['device'] = [[mta1200]]
	},
	{
		-- big samsung
		  ['jad'] = [[bsam.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.2]]
		, ['device'] = [[sgd836]]
	},
	{
		-- big flush graphics
		  ['jad'] = [[bfg.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.0]]
		, ['device'] = [[lgme970]]
	},
	{
		-- small standard
		  ['jad'] = [[ss.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.9]]
		, ['device'] = [[sew300]]
	},
	{
		-- small samsung
		  ['jad'] = [[ssam.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.8]]
		, ['device'] = [[sge256]]
	},
	{
		-- small nokia S40
		  ['jad'] = [[ss40.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.6]]
		, ['device'] = [[nk6061]]
	},
	{
		-- small low jar
		  ['jad'] = [[slj.jad]]
		, ['jar'] = [[]]
		, ['number'] = [[1.0.5]]
		, ['device'] = [[lgmg230]]
	},
}

