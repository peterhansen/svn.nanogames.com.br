using System;
using System.Collections.Generic;

namespace Utils {
	public class OneToManyBidirectionalSet< A, B > {
		
		private Dictionary< A, List< B > > directRelation;
		private Dictionary< B, A > reverseRelation;
		
		public OneToManyBidirectionalSet () {
			directRelation = new Dictionary<A, List<B>>();
			reverseRelation = new Dictionary< B, A >();
		}
		
				
		public List< B > GetValueForKey( A key ) {
			return directRelation[ key ];
		}
		 
		
		public void RemoveAllMappings( B mappedTo ) {
			List< B > values;
			
			foreach ( A key in directRelation.Keys ) {
				
				values = GetValueForKey( key );
				
				if ( values.Contains( mappedTo ) ) {
					values.Remove( mappedTo );
				}
			}
		}
		
		public void Set( A key, B mappedTo ) {
			///limpar todo A que ja tiver B
			RemoveAllMappings( mappedTo );
			
			///setar B no conjunto de A;
			directRelation[ key ].Add( mappedTo );
			reverseRelation[ mappedTo ] = key;
		}
		
		
	    /// <summary>
	    /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only. </exception>
	    public void Clear() {
	        directRelation.Clear();
			reverseRelation.Clear();
	    }
	
		
	    public bool Contains( A item ) {
			
			if ( directRelation.ContainsKey( item ) )
				return true;
			
			return false;
	    }

		public A GetValueByKey( B resultValue  ) {
			return reverseRelation[ resultValue ];
		}

		
	    /// <summary>
	    /// Gets the number of items contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <returns>
	    /// The number of items contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </returns>
	    public int Count {
	        get { return this.directRelation.Count; }
	    }
	
	    /// <summary>
	    /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
	    /// </summary>
	    /// <returns>
	    /// true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
	    /// </returns>
	    public bool IsReadOnly {
	        get {
	            return false;
	        }
	    }

		public override int GetHashCode() {
			return directRelation.GetHashCode();
		}
	}
}

