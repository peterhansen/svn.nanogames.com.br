using System;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class CharacterTargetMode {
		
		public enum Type { TROOP_LEADER };
		
		public Type type;
		public Faction faction;
		
		public CharacterTargetMode( Type type, Faction faction ) {
			this.type = type;
			this.faction = faction;
		}
	}
}

