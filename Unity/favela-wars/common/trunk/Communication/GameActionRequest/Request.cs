using System;

namespace GameCommunication {
	
	[Serializable]
	public abstract class Request {
		
		protected int playerID;
		
		public Request( int playerID ) {
			this.playerID = playerID;
		}
		
//		public void setPlayerID( int playerID ) {
//			this.playerID = playerID;
//		}
		
		public int GetPlayerID() {
			return playerID;
		}
		
	}
}

