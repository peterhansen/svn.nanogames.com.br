
using System;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class HollowBox : Box {
		
		protected Dictionary< Box, int > hollowBoxes = new Dictionary< Box, int >();
		
		
		public HollowBox( GameTransform gameTransform ) : base( gameTransform ) {
		}
		
		
		public void AddHoleBox( Box box ) {
			Box holeBox = new Box( box.GetGameTransform() );
			holeBox.GetGameTransform().position += GetPosition();
			
			hollowBoxes[ holeBox ] = 1;
		}
		
		
		public override RayBoxInterceptionReply Intercept( GameVector origin, GameVector target ) {
			RayBoxInterceptionReply reply = base.Intercept( origin, target );
			
			// Por enquanto, estamos assumindo que sempre que não há intersecoes com a caixa pai, nao precisamos nos preocupar.
			// Atencao: Esse tratamento inclusive exclui os casos específicos nos quais o raio se encontra totalmente dentro da caixa pai.
			if( reply.entry == null && reply.exit == null ) {
				return reply;
			}
				
			
			foreach( Box hollowBox in hollowBoxes.Keys ) {
				RayBoxInterceptionReply hollowReply = hollowBox.Intercept( origin, target );
				if( Merge( reply, hollowReply ) )
					// TODO: estamos assumindo atualmente que um raio só pode interceptar
					// uma hollowBox em uma holeBox (o que é errado)
					break;
			}
			return reply;
		}
		

		protected bool Merge( RayBoxInterceptionReply reply, RayBoxInterceptionReply hollowReply ) {
			if( hollowReply.entry == null && hollowReply.exit == null )
				return false;
			
			if( reply.entry != null && reply.exit != null ) {
				if( hollowReply.entry == null || hollowReply.exit == null )
					// nesse caso, implicitamente, hollowReply.exit também deve ser null. Ele ocorre quando a hollowBox não é atingida
					return false;			
				
				// caso padrão. vamos cropar o reply
				if( NanoMath.LessEquals( hollowReply.entry.t, reply.entry.t ) ) {
					reply.entry = hollowReply.exit;
				} else {
					reply.exit = hollowReply.entry;
				}
				
				if( NanoMath.Equals( reply.entry.t, reply.exit.t ) ) {
					// passamos batidos
					reply.entry = null;
					reply.exit = null;
				}
			} else if( reply.entry != null ) {
				// como a reta para dentro da caixa, precisamos essencialmente determinar se 
				// ele ficou o tempo todo na caixa
				if( NanoMath.LessEquals( hollowReply.entry.t, reply.entry.t ) ) {
					reply.entry = hollowReply.exit;
				} else {
					reply.exit = hollowReply.entry;
				}
			} else if( reply.exit != null ) {
				if( hollowReply.entry != null ) {
					reply.exit = hollowReply.entry;
				} else {
					reply.entry = hollowReply.exit;
				}
				
				if( NanoMath.Equals( reply.entry.t, reply.exit.t ) ) {
					// passamos batidos
					reply.entry = null;
					reply.exit = null;
				}
			}
			
			return true;
		}
		
		
		public override void DrawShadows( GameVector viewOrigin, Dictionary< Sector, SectorVisibilityStatus > sectors ) {
			List< ViewArea > hollowBoxesViewAreas = new List< ViewArea >();
			foreach( Box hollowBox in hollowBoxes.Keys ) {
				foreach( ViewArea viewArea in hollowBox.GenerateShadowViewAreas( viewOrigin ) ) {
					hollowBoxesViewAreas.Add( viewArea );
				}
			}
			
			convexHull.DrawShadows( viewOrigin, sectors, hollowBoxesViewAreas );
		}
		
		
		public override void CreateConvexHull( float xScale ) {
			base.CreateConvexHull( xScale );
			
			foreach( Box hollowBox in hollowBoxes.Keys )
				hollowBox.CreateConvexHull( xScale * 1.3f );
		}
	}
}

