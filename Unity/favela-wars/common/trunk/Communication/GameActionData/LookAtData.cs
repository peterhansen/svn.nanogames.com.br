using System;

namespace GameCommunication {
	
	public class LookAtData : GameActionData {
		
		public int characterID;
		
		public GameVector direction;
		
		public LookAtData( int characterID, GameVector direction ) {
			this.characterID = characterID;
			this.direction = direction;
		}
	}
}

