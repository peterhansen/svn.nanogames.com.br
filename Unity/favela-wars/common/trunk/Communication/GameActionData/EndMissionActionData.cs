using System;

using Utils;


namespace GameCommunication {
	
	[Serializable]
	public class EndMissionActionData : GameActionData {
		public Faction winningFaction;
		
		public EndMissionActionData( ObjectiveResult result ) {
			this.winningFaction = result.winningFaction;
		}
	}
}

