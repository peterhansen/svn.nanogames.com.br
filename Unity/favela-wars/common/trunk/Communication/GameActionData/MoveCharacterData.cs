using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	public class MoveCharacterData : GameActionData {
		
		public List< GameVector > nodes = new List< GameVector >();
		
		public VisibilityInfo visibilityInfo = new VisibilityInfo();
		
		public float percentualSpeed;
	
		public Stance movementStance;
		
		public Stance finalStance;
		
		public int characterWorldID;
		
		public GameVector finalDirection;

		
		public MoveCharacterData( int characterWorldID, List< GameVector > nodes, VisibilityInfo visibilityInfo, GameVector finalDirection ) {
			this.characterWorldID = characterWorldID;
			this.nodes = nodes;
			this.visibilityInfo = visibilityInfo;
			this.finalDirection = finalDirection;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MoveCharacterData );
		}
		
		
		public bool Equals( MoveCharacterData data ) {
			if ( data == null )
	            return false;
    
			return nodes.Equals( data.nodes );
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
	}
}

