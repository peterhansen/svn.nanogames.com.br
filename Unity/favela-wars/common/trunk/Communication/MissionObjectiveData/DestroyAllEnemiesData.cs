using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	[ Serializable ]
	public class DestroyAllEnemiesData : MissionObjectiveData {
		
		/// <summary>
		/// Número máximo de turnos para realizar a missão. 0 (zero) indica que não há limite.
		/// </summary>
		public int maxNumberOfTurns = 0;
		
		
		public DestroyAllEnemiesData() {
		}	
		
		
		public DestroyAllEnemiesData( int maxNumberOfTurns ) {
			this.maxNumberOfTurns = maxNumberOfTurns;
		}
		
	}
	
}

