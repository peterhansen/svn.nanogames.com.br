using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using Utils;
using SharpUnit;


namespace GameCommunication {
	
	[ Serializable ]
	public class MissionData {
		
		public string name = "";
		
		public Faction levelOwner = Faction.NONE;
		
//		public Utils.HashSet< SpawnAreaMissionData > spawnAreasData = new Utils.HashSet< SpawnAreaMissionData >();
		public Dictionary< SpawnAreaMissionData, int > spawnAreasData = new Dictionary< SpawnAreaMissionData, int >();
		
		public List< MissionObjectiveData > endMissionConditions = new List< MissionObjectiveData >();
		
		/// <summary>
		/// Lista de nomes dos assetbundles a serem carregados para esta missão.
		/// </summary>
		public string objectsBundlePath = "";
		
		public Dictionary< string, int > materialsAssetBundles = new Dictionary< string, int >();
		
		public Dictionary< string, int > meshesAssetBundles = new Dictionary< string, int >();
		
		
		public MissionData() {
		}
		
		
		public string GetMissionFilePath() {
			string dir = Common.Paths.SERVER + "/missions";
			if ( !Directory.Exists( dir ) )
                Directory.CreateDirectory( dir );
			
			return Path.Combine( dir, Util.GetCleanFilename( name ) + Common.MISSION_FILE_EXTENSION );
		}
		
		
		public string GetMapName() {
			return Util.GetCleanFilename( name );
		}
		
		
		public void ExportToFile() {
			Util.SerializeToFile( this, GetMissionFilePath() );
		}
		
		
		public static MissionData ImportFromFile( string filename ) {
			try {
				return Util.DeserializeFromFile< MissionData >( GetPath() + "/" + filename + Common.MISSION_FILE_EXTENSION );
			} catch( System.Exception e ) {
				throw new InvalidMissionFile( e.Message );
			}
		}		
		
		
		public static string GetPath() {
			return Common.Paths.SERVER + "/missions";
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MissionData );
		}
		
		
		public bool Equals( MissionData d ) {
			return d != null && name.Equals( d.name ) &&
				   spawnAreasData.Equals( d.spawnAreasData ) &&
				   endMissionConditions.Equals( d.endMissionConditions );
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		
	}
	
	
	#region Mission Generator Test Case
	internal class MissionDataTestCase : TestCase {
		
//		private MissionData data;
		
		public override void SetUp() {
			// TODO PETER
//			data = new MissionData();
//			data.name = "testando 1 2 3 bla bla";
//			
//			DestroyAllEnemies end1 = new DestroyAllEnemies( 66 );
//			data.endMissionConditions.Add( end1 );
//			
//			for ( int i = 0; i < 12; ++i ) {
//				SpawnAreaMissionData d = new SpawnAreaMissionData();
//				d.capacity = new Random().Next( 5, 10 );
//				d.faction = ( Faction ) ( i % 3 );
//				d.minObjects = 1;
//				d.Randomize();
//				data.spawnAreasData.Add( d );
//			}
		}
		
		
		public override void TearDown() {
		}
		
		
		[UnitTest]
		public void CanImportExportedMissionData() {
//			data.ExportToFile();
//			
//			MissionData loadedData = MissionData.ImportFromFile( data.name );
//			Assert.True( loadedData.Equals( data ) );
		}		
	}
	#endregion
}


public class InvalidMissionFile : Exception {
	
	public InvalidMissionFile() : this( "Arquivo de missão inválido." ) {
	}
	
	
	public InvalidMissionFile( string message ) : base( message ) {
	}
	
	
}