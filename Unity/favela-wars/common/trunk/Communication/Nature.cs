using System;
using System.Collections.Generic;

using Utils;

using Attribute = GameCommunication.Attribute;


namespace GameCommunication {
	
	public enum NatureType {
		GENERALIST,
		SPEEDSTER,
		AGRESSIVE,
		BRUISER,
		CEREBRAL,
		STRAIGHTFORWARD,
		TANK,
		ALERT
	}
	
	[ Serializable ]
	public class Nature : Editable {
			
		protected const float GROWTH_ALPHA = 0.4f;
		
		protected const float STARTING_RANDOMNESS = 10.0f;
		
		#region Valores máximos e mínimos dos atributos (os valores são readonly para que alterações em seu conteúdo sejam refletidos nos atributos que os usarem como limites)
		
		public static readonly Attribute AttackDefault = new Attribute( 50, 30, 100 );
		
		public static readonly Attribute DefenseDefault = new Attribute( 50, 30, 100 );
		
		public static readonly Attribute AgilityDefault = new Attribute( 50, 30, 100 );
		
		public static readonly Attribute TechniqueDefault = new Attribute( 50, 30, 100 );
		
		public static readonly Attribute IntelligenceDefault = new Attribute( 50, 30, 100 );
		
		public static readonly Attribute PerceptionDefault = new Attribute( 50, 30, 100 );
		
		public static readonly Attribute ActionPointsDefault = new Attribute( 100, 0, 100 );
		
		public static readonly Attribute HitPointsDefault = new Attribute( 100, 0, 100 );
		
		public static readonly Attribute MoralPointsDefault = new Attribute( 100, 0, 100 );
		
		#endregion
		
		public static Nature generalistNature = new Nature( 50, 50, 50, 50, 50, 50 );
		
		public static Nature speedsterNature = new Nature( 50, 30, 100, 50, 60, 80 );
		
		public static Nature agressiveNature = new Nature( 100, 50, 80, 50, 20, 50 );
		
		public static Nature bruiserNature = new Nature( 90, 100, 40, 50, 30, 50 );
		
		public static Nature straightforwardNature = new Nature( 60, 40, 30, 100, 60, 70 );
		
		public static Nature cerebralNature = new Nature( 20, 40, 40, 80, 100, 50 );
		
		public static Nature alertNature = new Nature( 40, 70, 30, 60, 60, 100 );
		
		protected NatureType type;
		
		private ModAttribute attackLevel;// = new ModAttribute( AttackDefault, AttackDefault );
		
		private ModAttribute defenseLevel;// = new ModAttribute( DefenseDefault, DefenseDefault );
		
		private ModAttribute agility;// = new ModAttribute( AgilityDefault, AgilityDefault );
		
		private ModAttribute technique;// = new ModAttribute( TechniqueDefault, TechniqueDefault );
		
		private ModAttribute intelligence;// = new ModAttribute( IntelligenceDefault, IntelligenceDefault );
		
		private ModAttribute perception;// = new ModAttribute( PerceptionDefault, PerceptionDefault );
		
		private ModAttribute actionPoints;// = new ModAttribute( ActionPointsDefault, ActionPointsDefault );
		
		private ModAttribute hitPoints;// = new ModAttribute( HitPointsDefault, HitPointsDefault );
		
		private ModAttribute moralPoints;// = new ModAttribute( MoralPointsDefault, MoralPointsDefault );
		
		
		#if UNITY_EDITOR
		// necessário para serializar a classe no editor
		public Nature() {
		}
		#endif
		
		
		private Nature( int maxAttack, int maxDefense, int maxAgility, int maxTechnique, int maxIntelligence, int maxPerception ) {
			attackLevel = new ModAttribute( maxAttack, 0.0f, maxAttack );
			defenseLevel = new ModAttribute( maxDefense, 0.0f, maxDefense );
			agility = new ModAttribute( maxAgility, 0.0f, maxAgility );
			technique = new ModAttribute( maxTechnique, 0.0f, maxTechnique );
			intelligence = new ModAttribute( maxIntelligence, 0.0f, maxIntelligence );
			perception = new ModAttribute( maxPerception, 0.0f, maxPerception );
			actionPoints = new ModAttribute( 100.0f, 0.0f, 100.0f );
			hitPoints = new ModAttribute( 100.0f, 0.0f, 100.0f );
			moralPoints = new ModAttribute( 100.0f, 0.0f, 100.0f );
		}
		
		
		private Nature( Nature other ) : this( 0, 0, 0, 0, 0, 0 ) {
			type = other.GetNatureType();
			attackLevel.Set( other.AttackLevel );
			defenseLevel.Set( other.DefenseLevel );
			agility.Set( other.Agility );
			technique.Set( other.Technique );
			intelligence.Set( other.Intelligence );
			perception.Set( other.Perception );
			actionPoints.Set( other.ActionPoints );
			hitPoints.Set( other.HitPoints );
			moralPoints.Set( other.MoralPoints );
		}
		
		
		public static Nature Instantiate( NatureType type ) {
			switch( type ) {
				case NatureType.GENERALIST:
					return new Nature( generalistNature );
		
				case NatureType.SPEEDSTER:
					return new Nature( speedsterNature );
					
				case NatureType.AGRESSIVE:
					return new Nature( agressiveNature );
				
				case NatureType.BRUISER:
					return new Nature( bruiserNature );
					
				case NatureType.STRAIGHTFORWARD:
					return new Nature( straightforwardNature );
					
				case NatureType.CEREBRAL:
					return new Nature( cerebralNature );
					
				case NatureType.ALERT:
					return new Nature( alertNature );
					
				default:
					throw new Exception( "Não encontrado NatureType adequado em Nature.Instantiate.");				
			}
		}
		
		
		public NatureType GetNatureType() {
			return type;
		}
		
		
		/// <summary>
		/// Indica a velocidade com a qual a Unidade se movimenta e realiza ações. Quanto maior o valor de Agilidade de uma Unidade, mais ações ela conseguirá realizar com seus pontos de Ação. A Agilidade também influencia a quantidade de terreno que uma Unidade consegue cobrir em seu turno. Unidades ágeis movimentam-se com velocidade e cobrem maiores distâncias. Por fim, a Agilidade também aumenta a chance de esquiva em combates corpo-a-corpo. O valor de Agilidade de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos.
		/// </summary>
		[ ToolTip( "Indica a velocidade com a qual a Unidade se movimenta e realiza ações. Quanto maior o valor de Agilidade de uma Unidade, mais ações ela conseguirá realizar com seus pontos de Ação. A Agilidade também influencia a quantidade de terreno que uma Unidade consegue cobrir em seu turno. Unidades ágeis movimentam-se com velocidade e cobrem maiores distâncias. Por fim, a Agilidade também aumenta a chance de esquiva em combates corpo-a-corpo. O valor de Agilidade de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos." ) ]
		public ModAttribute Agility {
			get { return agility; }
			set { agility = value; }
		}			
		
		
		/// <summary>
		/// Representa a capacidade de uma Unidade de infligir dano. Quanto maior o Ataque de uma Unidade, mais devastadora será a sua ofensiva. Esta capacidade de infligir dano pode estar relacionada tanto com treinamento e direcionamento tático da Unidade, como com sua própria índole. Ou seja, um alto valor de Ataque também está relacionado ao nível de agressividade de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos.
		/// </summary>
		[ ToolTip( "Representa a capacidade de uma Unidade de infligir dano. Quanto maior o Ataque de uma Unidade, mais devastadora será a sua ofensiva. Esta capacidade de infligir dano pode estar relacionada tanto com treinamento e direcionamento tático da Unidade, como com sua própria índole. Ou seja, um alto valor de Ataque também está relacionado ao nível de agressividade de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos." ) ]
		public ModAttribute AttackLevel {
			get { return attackLevel; }
			set { attackLevel = value; }
		}			
		
		
		/// <summary>
		/// Representa a resistência física de uma Unidade e está relacionada à capacidade de absorver ataques e de sobreviver a efeitos nocivos gerados por ataques especiais. A Defesa também tem influência direta no número de Pontos de Vida de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos.
		/// </summary>
		[ ToolTip( "Representa a resistência física de uma Unidade e está relacionada à capacidade de absorver ataques e de sobreviver a efeitos nocivos gerados por ataques especiais. A Defesa também tem influência direta no número de Pontos de Vida de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos." ) ]
		public ModAttribute DefenseLevel {
			get { return defenseLevel; }
			set { defenseLevel = value; }
		}			
		
		
		/// <summary>
		/// Está relacionada à concentração e destreza, e representa a capacidade da Unidade em realizar ações complexas no campo de batalha. Quanto maior a Técnica de uma Unidade, maior a chance de sucesso da execução de ações que exigem concentração e destreza, como disparos precisos, ações táticas, manobras corporais complexas, etc.
		/// </summary>
		[ ToolTip( "Representa a capacidade de aprendizado de uma Unidade e está mais relacionada ao Treinamento do que ao Campo de Batalha. Quanto mais Inteligente a Unidade, menos pontos de Experiência ela precisa investir no aprendizado de novas técnicas. Além disto, Unidades inteligentes também demoram menos para aprender." ) ]
		public ModAttribute Technique {
			get { return technique; }
			set { technique = value; }
		}			
		
		
		/// <summary>
		/// Representa a capacidade de aprendizado de uma Unidade e está mais relacionada ao Treinamento do que ao Campo de Batalha. Quanto mais Inteligente a Unidade, menos pontos de Experiência ela precisa investir no aprendizado de novas técnicas. Além disto, Unidades inteligentes também demoram menos para aprender.
		/// </summary>
		[ ToolTip( "Representa a capacidade de aprendizado de uma Unidade e está mais relacionada ao Treinamento do que ao Campo de Batalha. Quanto mais Inteligente a Unidade, menos pontos de Experiência ela precisa investir no aprendizado de novas técnicas. Além disto, Unidades inteligentes também demoram menos para aprender." ) ]
		public ModAttribute Intelligence {
			get { return intelligence; }
			set { intelligence = value; }
		}			
		
		
		/// <summary>
		/// Percepção é a sensibilidade que uma Unidade tem em relação ao espaço à sua volta, e está diretamente relacionada aos sentidos. Unidades com alta percepção são capazes de identificar inimigos se aproximando, avistar adversários escondidos e se prevenir quanto à emboscadas.
		/// </summary>
		[ ToolTip( "Percepção é a sensibilidade que uma Unidade tem em relação ao espaço à sua volta, e está diretamente relacionada aos sentidos. Unidades com alta percepção são capazes de identificar inimigos se aproximando, avistar adversários escondidos e se prevenir quanto à emboscadas." ) ]
		public ModAttribute Perception {
			get { return perception; }
			set { perception = value; }
		}			
		
		
		/// <summary>
		/// Durante os turnos dos Confrontos de Favela Wars, as Unidades realizam ações, como se locomover e atirar em seus oponentes. Quanto mais Pontos de Ação uma Unidade tiver, mais ações ela pode realizar em um mesmo turno. Os Pontos de Ação, ou PAs, são uma das características mais importantes de um Soldado.
		/// </summary>
		[ ToolTip( "Durante os turnos dos Confrontos de Favela Wars, as Unidades realizam ações, como se locomover e atirar em seus oponentes. Quanto mais Pontos de Ação uma Unidade tiver, mais ações ela pode realizar em um mesmo turno. Os Pontos de Ação, ou PAs, são uma das características mais importantes de um Soldado." ) ]
		public ModAttribute ActionPoints {
			get { return actionPoints; }
			set { actionPoints = value; }
		}			
		
		
		/// <summary>
		/// Os Pontos de Vida representam a energia vital das Unidades, ou seja, quanto dano físico elas podem suportar, originários de tiros, golpes corpo a corpo ou explosões. À medida que os Pontos de Vida de uma Unidade se aproximam de zero, esta Unidade se aproxima da morte.
		/// </summary>
		[ ToolTip( "Os Pontos de Vida representam a energia vital das Unidades, ou seja, quanto dano físico elas podem suportar, originários de tiros, golpes corpo a corpo ou explosões. À medida que os Pontos de Vida de uma Unidade se aproximam de zero, esta Unidade se aproxima da morte." ) ]
		public ModAttribute HitPoints {
			get { return hitPoints; }
			set { hitPoints = value; }
		}			
		
		
		/// <summary>
		/// A Moral é o mais importante indicador do estado psicológico de uma Unidade. De acordo com o rumo do combate, uma Unidade pode ganhar ou perder Pontos de Moral, o que irá influenciar diretamente a eficácia de suas ações. Unidades com baixa Moral tendem a errar e se desesperar com mais facilidade, enquanto Unidades com alta Moral realizam ações com mais segurança, reduzindo a chance de erros.
		/// </summary>
		[ ToolTip( "A Moral é o mais importante indicador do estado psicológico de uma Unidade. De acordo com o rumo do combate, uma Unidade pode ganhar ou perder Pontos de Moral, o que irá influenciar diretamente a eficácia de suas ações. Unidades com baixa Moral tendem a errar e se desesperar com mais facilidade, enquanto Unidades com alta Moral realizam ações com mais segurança, reduzindo a chance de erros." ) ]
		public ModAttribute MoralPoints {
			get { return moralPoints; }
			set { moralPoints = value; }
		}
		
		
		public ModAttribute GetAttribute( AttributeType type ) {
			switch (type) {
			case AttributeType.ATTACK:
				return AttackLevel;
				
			case AttributeType.DEFENSE:
				return DefenseLevel;
				
			case AttributeType.AGILITY:
				return Agility;
				
			case AttributeType.TECHNIQUE:
				return Technique;
				
			case AttributeType.INTELLIGENCE:
				return Intelligence;
				
			case AttributeType.PERCEPTION:
				return Perception;
				
			case AttributeType.ACTION_POINTS:
				return ActionPoints;
				
			case AttributeType.HIT_POINTS:
				return HitPoints;
				
			case AttributeType.MORAL_POINTS:
				return MoralPoints;
				
			default:
				throw new Exception( "Tipo de Atributo não mapeado no método CharacterInfoData.GetAttribute." );
			}
		}
		
		
		public void UpdateAttributes( float potential ) {
			foreach( AttributeType attributeType in Common.AttributeTypes ) {
				ModAttribute attribute = GetAttribute( attributeType );
				
				// fórmula para crescimento de atributos:
				// atributo( potencial ) = atributo.min + (atributo.max - atributo.min) * ( potencial ^ alfa ),
				// onde alfa deve estar entre 1 (crescimento linear) e 0.2
				attribute.Value = attribute.Min + ( attribute.Max - attribute.Min ) * (float) Math.Pow( (float) potential, (float) GROWTH_ALPHA );
			}
		}
		
				
		protected ModAttribute CreateInitialAttribute( int maxValue ) {
			int initial = ( int ) ( ( (float) maxValue ) * 0.1f + 
				NanoMath.Random( 0.0f, STARTING_RANDOMNESS ) - ( STARTING_RANDOMNESS * 0.5f ) );
			return new ModAttribute( initial, 0.0f, maxValue );
		}
	}
}

