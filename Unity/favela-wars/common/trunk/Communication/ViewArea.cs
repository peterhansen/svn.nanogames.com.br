using System;
using System.Collections;
using System.Collections.Generic;

using SharpUnit;
using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class ViewArea {
		
		private readonly GameVector center = new GameVector();
		
		private readonly GameVector endPoint1 = new GameVector();
		
		private readonly GameVector endPoint2 = new GameVector();
		
		
		public ViewArea() {
		}
		
		
		public ViewArea ( ViewArea another ) {
			center = new GameVector( another.GetCenter() );
			endPoint1 = new GameVector( another.GetEndPoint1() );
			endPoint2 = new GameVector( another.GetEndPoint2() );
		}
		
		public ViewArea( GameVector center, GameVector endPoint1, GameVector endPoint2 ) {
			this.center = center;
			this.endPoint1 = endPoint1;
			this.endPoint2 = endPoint2;
		}
		
		
		public void SetView( GameTransform gt ) {
			GameVector scaled = new GameVector( gt.direction );
			scaled.SetScale( Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT );
			center.Set( gt.position );
			
			float cos = ( float ) Math.Cos( Common.CharacterInfo.CHARACTER_FOV_RAD * 0.5f );
			float sin = ( float ) Math.Sin( Common.CharacterInfo.CHARACTER_FOV_RAD * 0.5f );
			
			endPoint1.x = center.x + ( scaled.x * cos ) - ( sin * scaled.z );
			endPoint1.z = center.z + ( scaled.x * sin ) + ( cos * scaled.z );
			
			cos = ( float ) Math.Cos( - Common.CharacterInfo.CHARACTER_FOV_RAD * 0.5f );
			sin = ( float ) Math.Sin( - Common.CharacterInfo.CHARACTER_FOV_RAD * 0.5f );
			endPoint2.x = center.x + ( scaled.x * cos ) - ( sin * scaled.z );
			endPoint2.z = center.z + ( scaled.x * sin ) + ( cos * scaled.z );
			
			endPoint1.y = endPoint2.y = center.y;
		}
		
		
		public override int GetHashCode() {
			///a ordem dos fatores não afeta o produto
			return string.Format( "{0}{1}{2}", center, endPoint1, endPoint2 ).GetHashCode();
		}
		
		
		public override string ToString() {
			return string.Format( "[ViewArea center={0} endpoint1={1} endpoint2={2}]", center, endPoint1, endPoint2 );
		}
		
		
		public GameVector GetCenter() {
			return center;
		}
		
		
		public GameVector GetEndPoint1() {
			return endPoint1;
		}
		
		
		public GameVector GetEndPoint2() {
			return endPoint2;
		}
		
		
		public void SetCenter( GameVector vec ) {
			center.Set( vec );
		}
		
		
		public void SetEndPoint1( GameVector vec ) {
			endPoint1.Set( vec );
		}
		
		
		public void SetEndPoint2( GameVector vec ) {
			endPoint2.Set( vec );
		}		
		
		
		public void SetView( GameVector p1, GameVector p2, GameVector c ) {
			
			SetEndPoint1( p1 );
			SetEndPoint2( p2 );
			SetCenter( c );
		}
	
		
		public void Draw() {
			Edge[] edges = new ViewArea( this ).GetEdges();
			edges[ 0 ].DebugDraw( 0xFF0000FF );
			edges[ 1 ].DebugDraw( 0x00FF00FF );
			edges[ 2 ].DebugDraw( 0x0000FFFF );
		} 
		
		
		// ADAPTADOS DE http://www.blackpawn.com/texts/pointinpoly/default.html
		private static bool PlanarSameSize( GameVector p1, GameVector p2, GameVector a, GameVector b ) {
		    GameVector cp1 = b.Sub( a ).Cross( p1.Sub(a ) );
		    GameVector cp2 = b.Sub( a ).Cross( p2.Sub(a ) );
			
			float dot = cp1.Dot( cp2 );
		    return NanoMath.GreaterEquals( dot, 0.0f );
		}
		
		
		private static bool PlanarPointInTriangle( GameVector p, GameVector a, GameVector b, GameVector c ) {
		    return ( PlanarSameSize(p,a, b,c) && PlanarSameSize(p,b, a,c) && PlanarSameSize(p,c, a,b) );
		}
		
		
		public bool IsInside( GameVector point ) {
			GameVector gvp = new GameVector( point );
			GameVector gva = new GameVector( center );
			GameVector gvb = new GameVector( endPoint1 );
			GameVector gvc = new GameVector( endPoint2 );
			gvp.y = 0.0f;
			gva.y = 0.0f;
			gvb.y = 0.0f;
			gvc.y = 0.0f;
			
			return PlanarPointInTriangle( gvp, gva, gvb, gvc );
		}
		
		
		public override bool Equals (object obj) {
			return IsCoincidant( obj as ViewArea );
		}
		
		
		public bool IsDegenerated() {			
			return endPoint1.Equals( endPoint2 ) || endPoint1.Equals( center ) || center.Equals( endPoint2 ) || !endPoint1.IsValid()|| !endPoint2.IsValid()|| !center.IsValid();
		}
		
		
		public bool IsCoincidant( ViewArea another ) {
			if ( another == null )
				return false;
			
			if ( another == this )
				return true;
			
			if ( center.Equals( another.center ) ) {				
				if ( endPoint1.Equals( another.endPoint1 ) && endPoint2.Equals( another.endPoint2 ) )
					return true;
				
				if ( endPoint1.Equals( another.endPoint2 ) && endPoint2.Equals( another.endPoint1 ) )
					return true;
			}

			if ( center.Equals( another.endPoint2 ) ) {				
				if ( endPoint1.Equals( center ) && endPoint2.Equals( another.endPoint1 ) )
					return true;
				
				if ( endPoint1.Equals( another.endPoint1 ) && endPoint2.Equals( center ) )
					return true;
			}

			if ( center.Equals( another.endPoint1 ) ) {				
				if ( endPoint1.Equals( another.endPoint2 ) && endPoint2.Equals(  center ) )
					return true;
				
				if ( endPoint1.Equals( center ) && endPoint2.Equals( another.endPoint2 ) )
					return true;
			}
			
			return false;
		}
		
		
		private void ProjectToLeast() {
			float len1 = endPoint1.Sub( center ).Norm();
			float len2 = endPoint2.Sub( center ).Norm();
			GameVector smaller;
			GameVector bigger;
			
			if ( len1 > len2 ) {
				smaller = endPoint2;
				bigger = endPoint1;
			} else if ( len1 < len2 ) {
				smaller = endPoint1;
				bigger = endPoint2;
			} else {
				return;
			}
			
			GameVector newVec = bigger.Sub( center );
			newVec.Normalize();
			float t = Math.Abs( smaller.Sub( center ).z / newVec.z );
			newVec.Scale( t );			
			bigger.Set( newVec.Add( center ) );
		}
		
		
		private void ProjectToMost() {
			float len1 = endPoint1.Sub( center ).Norm();
			float len2 = endPoint2.Sub( center ).Norm();
			GameVector smaller;
			GameVector bigger;
			
			if ( len1 > len2 ) {
				smaller = endPoint2;
				bigger = endPoint1;
			} else if ( len1 < len2 ) {
				smaller = endPoint1;
				bigger = endPoint2;
			} else {
				return;
			}
			
			GameVector newVec = smaller.Sub( center );
			newVec.Normalize();
			float t = Math.Abs( bigger.Sub( center ).z / newVec.z );
			newVec.Scale( t );			
			smaller.Set( newVec.Add( center ) );
		}
		
		
		private GameVector ProjectToMost( GameVector gv ) {
			float t1 = 0.0f;
			float t2 = 0.0f;
			GameVector toReturn = endPoint2.Sub( endPoint1 );
			Edge edge0 = new Edge( endPoint1.Sub( center ), endPoint2.Sub( center ) );
			Edge edge1 = new Edge( new GameVector(), gv.Sub( center ) );
			edge0.Intersect( edge1, out t1, out t2 );
			
			toReturn.Scale( t2 );
			
			if ( ( NanoMath.GreaterEquals( t2, 0.0f ) &&  NanoMath.LessEquals( t2, 1.0f ) ) )			
				return toReturn.Add( endPoint1 );
			else
				return gv;
		}
		
		
		/// <summary>
		/// Gets the edges.
		/// </summary>
		/// <returns>
		/// The edges.
		/// </returns>
		public Edge[] GetEdges() {
			Edge[] toReturn = new Edge[ 3 ];
			toReturn[ 0 ] = new Edge( center, endPoint1 );
			toReturn[ 1 ] = new Edge( center, endPoint2 );
			toReturn[ 2 ] = new Edge( endPoint1, endPoint2 );
			
			return toReturn;
		}
		
		
		/// <summary>
		/// Gets the intersecting points.
		/// </summary>
		/// <returns>
		/// The intersecting points.
		/// </returns>
		/// <param name='edge'>
		/// Edge.
		/// </param>
		/// <param name='gt'>
		/// Gt.
		/// </param>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		public static List< GameVector > GetIntersectingPoints( Edge edge, GameTransform gt, List< float > parameters ) {
			List< GameVector > toReturn = new List<GameVector>();
			Edge[] edges = gt.GetEdges();
			GameVector gv;
			float t1 = 0.0f;
			float t2 = 0.0f;
			
			for ( int c = 0; c < edges.Length; ++c ) {
			
				edge.Intersect( edges[ c ], out t1, out t2 );
				
				if ( NanoMath.GreaterEquals( t2, 0.0f ) && NanoMath.LessEquals( t2, 1.0f ) &&
					 NanoMath.GreaterEquals( t1, 0.0f ) && NanoMath.LessEquals( t1, 1.0f ) ) 
				{
					
					gv = edge.p1 + ( ( edge.p2 - edge.p1 ) * t2 );
					
					if ( !toReturn.Contains( gv ) ) {
						if ( parameters != null )
							parameters.Add( t2 );
						
						toReturn.Add( gv );
					}
				}
			}
			
			return toReturn;
		}
		
		
		/// <summary>
		/// Determines whether this instance is point in line the specified endPoint1 endPoint2 checkPoint.
		/// </summary>
		/// <returns>
		/// <c>true</c> if this instance is point in line the specified endPoint1 endPoint2 checkPoint; otherwise, <c>false</c>.
		/// </returns>
		/// <param name='endPoint1'>
		/// If set to <c>true</c> end point1.
		/// </param>
		/// <param name='endPoint2'>
		/// If set to <c>true</c> end point2.
		/// </param>
		/// <param name='checkPoint'>
		/// If set to <c>true</c> check point.
		/// </param>
		public static bool IsPointInLine( GameVector endPoint1, GameVector endPoint2, GameVector checkPoint ) {
		    return ( ( checkPoint.z - endPoint1.z ) / ( checkPoint.x - endPoint1.x ) ) == ( ( endPoint2.z - endPoint1.z ) / ( endPoint2.x - endPoint1.x ) );
		}
		
		
		public bool Intersect( GameTransform candidate ) {
			GameVector[] pointsInVolume = candidate.GetPoints();
			
			for ( int e = 0; e < pointsInVolume.Length; ++e )
				if ( IsInside( pointsInVolume[ e ] ) )
					return true;

			return false;
		}		
	
	
		public List< ViewArea > Split( GameVector point ) {
			List< ViewArea > toReturn = new List<ViewArea>();
			ViewArea p1;
			GameVector gv;
			
			if ( IsInside( point )  ) {
				gv = ProjectToMost( point );
				
				if ( gv != point ) {
					p1 = new ViewArea( this );
					p1.SetEndPoint2( gv );
					SetEndPoint1( gv );
			
					if ( !IsDegenerated() && !p1.IsDegenerated() ) {
						toReturn.Add( p1 );
					} else {
						SetEndPoint1( p1.endPoint1 );
					}
				}
			}  
			
			return toReturn;
		}
		
	}
}
