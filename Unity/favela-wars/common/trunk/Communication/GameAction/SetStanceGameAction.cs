using System;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class SetStanceGameAction : GameAction {
		
		public Stance currentStance;
		
		public SetStanceGameAction( Stance currentStance ) : base( ActionType.SET_STANCE ) {
			this.currentStance = currentStance;
		}
	}
}

