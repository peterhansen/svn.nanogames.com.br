using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class GameAction	{
			
		public enum ActionHand {
			NONE,
			RIGHT, 
			LEFT,  
		};
		
		protected GameAction parent;
		
		protected List< GameAction > children = new List< GameAction >();
		
		protected ActionType actionType = ActionType.NONE;
		
		protected bool isAvailable;
		public bool IsAvailable {
			get { return isAvailable; }
			set { isAvailable = value; }
		}
		
		protected ActionHand actionHand = ActionHand.NONE;
		public ActionHand Hand {
			get { return actionHand; }
			set { actionHand = value; }
		}
		
		
		public GameAction( ActionType type ) {
			isAvailable = true;
			actionType = type;
		}
		
		
		public ActionType GetActionType() {
			return actionType;
		}
		
		
		public void SetParent( GameAction parent ) {
			this.parent = parent;
		}
		
		
		public GameAction GetParent() {
			return parent;
		}
		
		
		public List< GameAction > GetChildren() {
			return children;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="a">
		/// A <see cref="Action"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Boolean"/>
		/// </returns>
		public bool IsChildOf( GameAction a ) {
			GameAction p = GetParent();
		
			while ( p != null && p != a )
				p = p.GetParent();
				
			return p != null;	
		}
		
		
		public void AddChild( GameAction child ) {
			child.SetParent( this );
			children.Add( child );
		}
		
		
		public bool HasChildren() {
			return children.Count > 0;
		}
		
		
		public GameAction GetChildOfType( ActionType type ) {
			foreach ( GameAction child in children ) {
				if ( child.GetActionType() == type )
					return child;
			}
			
			return null;
		}
		
	}
}

