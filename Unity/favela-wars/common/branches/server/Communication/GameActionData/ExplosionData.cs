using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	public class ExplosionData : GameActionData {
		
		/// <summary>
		/// Origem da explosão.
		/// </summary>
		public GameVector center;
		
		/// <summary>
		/// 
		/// </summary>
		// TODO implementar diferentes tipos de explosão (incendiária, gás, fumaça, etc.)
		public byte type;
		
		/// <summary>
		/// Raio da esfera da explosão.
		/// </summary>
		public float ray;
		
		
		public ExplosionData( GameVector center, float ray ) {
			this.center = center;
			this.ray = ray;
		}

		
		public override bool Equals( object obj ) {
			return Equals( obj as ExplosionData );
		}
		
		
		public bool Equals( ExplosionData other ) {
			return other != null && center.Equals( other.center ) && ray == other.ray && type == other.type;
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
		
	}
}

