using System;
using System.Text;
using System.Runtime.Serialization;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	/// <summary>
	/// Classe que encapsula as características vitais e atributos de um personagem. Maiores informações em:
	/// http://nanostudio.mobi/wikiFavela/CaracteristicasVitais
	/// http://nanostudio.mobi/wikiFavela/Atributos
	/// </summary>
	public class CharacterInfoData : Editable {
		
		private static uint count = 0;
		
		/// <summary>
		/// Nome do personagem.
		/// </summary>
		private string name = "Character #" + ( ++count );
		
		/// <summary>
		/// Natureza do personagem, a qual controla
		/// </summary>
		private Nature nature = Nature.Instantiate( NatureType.GENERALIST );
		
		/// <summary>
		/// Faccao do personagem.
		/// </summary>
		private Faction faction = Faction.NONE;
		
		/// <summary>
		/// Gênero do personagem (masculino/feminino).
		/// </summary>
		private Gender gender;

		private ExperienceInfo experienceInfo = new ExperienceInfo() ;
		
		
		public CharacterInfoData() {
		}
		
		
		public CharacterInfoData( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
		}		
		
		
		[ ToolTip( "Nome do personagem." ) ]
		public string Name {
			get { return name; }
			set { name = value; }
		}	
		
		
		[ ToolTip( "Gênero do personagem." ) ]
		public Gender Gender {
			get { return gender; }
			set { gender = value; }
		}
		
		
		[ ToolTip( "Faccao do personagem." ) ]
		public Faction Faction {
			get { return faction; }
			set { faction = value; }
		}
		
		
		public void AddTrainingPoints( int acquiredTPs ) {
			experienceInfo.totalTrainingPoints += acquiredTPs;
		}
		
		public int GetTrainingPoints(){
			return experienceInfo.totalTrainingPoints;
		}
		
		public NatureType GetNatureType(){
			return nature.GetNatureType();
		}
		
		
		/// <summary>
		/// Indica a velocidade com a qual a Unidade se movimenta e realiza ações. Quanto maior o valor de Agilidade de uma Unidade, mais ações ela conseguirá realizar com seus pontos de Ação. A Agilidade também influencia a quantidade de terreno que uma Unidade consegue cobrir em seu turno. Unidades ágeis movimentam-se com velocidade e cobrem maiores distâncias. Por fim, a Agilidade também aumenta a chance de esquiva em combates corpo-a-corpo. O valor de Agilidade de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos.
		/// </summary>
		[ ToolTip( "Indica a velocidade com a qual a Unidade se movimenta e realiza ações. Quanto maior o valor de Agilidade de uma Unidade, mais ações ela conseguirá realizar com seus pontos de Ação. A Agilidade também influencia a quantidade de terreno que uma Unidade consegue cobrir em seu turno. Unidades ágeis movimentam-se com velocidade e cobrem maiores distâncias. Por fim, a Agilidade também aumenta a chance de esquiva em combates corpo-a-corpo. O valor de Agilidade de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos." ) ]
		public ModAttribute Agility {
			get { return nature.Agility; }
			set { nature.Agility = value; }
		}			
		
		
		/// <summary>
		/// Representa a capacidade de uma Unidade de infligir dano. Quanto maior o Ataque de uma Unidade, mais devastadora será a sua ofensiva. Esta capacidade de infligir dano pode estar relacionada tanto com treinamento e direcionamento tático da Unidade, como com sua própria índole. Ou seja, um alto valor de Ataque também está relacionado ao nível de agressividade de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos.
		/// </summary>
		[ ToolTip( "Representa a capacidade de uma Unidade de infligir dano. Quanto maior o Ataque de uma Unidade, mais devastadora será a sua ofensiva. Esta capacidade de infligir dano pode estar relacionada tanto com treinamento e direcionamento tático da Unidade, como com sua própria índole. Ou seja, um alto valor de Ataque também está relacionado ao nível de agressividade de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos." ) ]
		public ModAttribute AttackLevel {
			get { return nature.AttackLevel; }
			set { nature.AttackLevel = value; }
		}			
		
		
		/// <summary>
		/// Representa a resistência física de uma Unidade e está relacionada à capacidade de absorver ataques e de sobreviver a efeitos nocivos gerados por ataques especiais. A Defesa também tem influência direta no número de Pontos de Vida de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos.
		/// </summary>
		[ ToolTip( "Representa a resistência física de uma Unidade e está relacionada à capacidade de absorver ataques e de sobreviver a efeitos nocivos gerados por ataques especiais. A Defesa também tem influência direta no número de Pontos de Vida de uma Unidade. O valor de Ataque de uma Unidade por ser modificado temporariamente por químicos ou definitivamente através de equipamento e Treinamentos." ) ]
		public ModAttribute DefenseLevel {
			get { return nature.DefenseLevel; }
			set { nature.DefenseLevel = value; }
		}			
		
		
		/// <summary>
		/// Está relacionada à concentração e destreza, e representa a capacidade da Unidade em realizar ações complexas no campo de batalha. Quanto maior a Técnica de uma Unidade, maior a chance de sucesso da execução de ações que exigem concentração e destreza, como disparos precisos, ações táticas, manobras corporais complexas, etc.
		/// </summary>
		[ ToolTip( "Representa a capacidade de aprendizado de uma Unidade e está mais relacionada ao Treinamento do que ao Campo de Batalha. Quanto mais Inteligente a Unidade, menos pontos de Experiência ela precisa investir no aprendizado de novas técnicas. Além disto, Unidades inteligentes também demoram menos para aprender." ) ]
		public ModAttribute Technique {
			get { return nature.Technique; }
			set { nature.Technique = value; }
		}			
		
		
		/// <summary>
		/// Representa a capacidade de aprendizado de uma Unidade e está mais relacionada ao Treinamento do que ao Campo de Batalha. Quanto mais Inteligente a Unidade, menos pontos de Experiência ela precisa investir no aprendizado de novas técnicas. Além disto, Unidades inteligentes também demoram menos para aprender.
		/// </summary>
		[ ToolTip( "Representa a capacidade de aprendizado de uma Unidade e está mais relacionada ao Treinamento do que ao Campo de Batalha. Quanto mais Inteligente a Unidade, menos pontos de Experiência ela precisa investir no aprendizado de novas técnicas. Além disto, Unidades inteligentes também demoram menos para aprender." ) ]
		public ModAttribute Intelligence {
			get { return nature.Intelligence; }
			set { nature.Intelligence = value; }
		}			
		
		
		/// <summary>
		/// Percepção é a sensibilidade que uma Unidade tem em relação ao espaço à sua volta, e está diretamente relacionada aos sentidos. Unidades com alta percepção são capazes de identificar inimigos se aproximando, avistar adversários escondidos e se prevenir quanto à emboscadas.
		/// </summary>
		[ ToolTip( "Percepção é a sensibilidade que uma Unidade tem em relação ao espaço à sua volta, e está diretamente relacionada aos sentidos. Unidades com alta percepção são capazes de identificar inimigos se aproximando, avistar adversários escondidos e se prevenir quanto à emboscadas." ) ]
		public ModAttribute Perception {
			get { return nature.Perception; }
			set { nature.Perception = value; }
		}			
		
		
		/// <summary>
		/// Durante os turnos dos Confrontos de Favela Wars, as Unidades realizam ações, como se locomover e atirar em seus oponentes. Quanto mais Pontos de Ação uma Unidade tiver, mais ações ela pode realizar em um mesmo turno. Os Pontos de Ação, ou PAs, são uma das características mais importantes de um Soldado.
		/// </summary>
		[ ToolTip( "Durante os turnos dos Confrontos de Favela Wars, as Unidades realizam ações, como se locomover e atirar em seus oponentes. Quanto mais Pontos de Ação uma Unidade tiver, mais ações ela pode realizar em um mesmo turno. Os Pontos de Ação, ou PAs, são uma das características mais importantes de um Soldado." ) ]
		public ModAttribute ActionPoints {
			get { return nature.ActionPoints; }
			set { nature.ActionPoints = value; }
		}			
		
		
		/// <summary>
		/// Os Pontos de Vida representam a energia vital das Unidades, ou seja, quanto dano físico elas podem suportar, originários de tiros, golpes corpo a corpo ou explosões. À medida que os Pontos de Vida de uma Unidade se aproximam de zero, esta Unidade se aproxima da morte.
		/// </summary>
		[ ToolTip( "Os Pontos de Vida representam a energia vital das Unidades, ou seja, quanto dano físico elas podem suportar, originários de tiros, golpes corpo a corpo ou explosões. À medida que os Pontos de Vida de uma Unidade se aproximam de zero, esta Unidade se aproxima da morte." ) ]
		public ModAttribute HitPoints {
			get { return nature.HitPoints; }
			set { nature.HitPoints = value; }
		}			
		
		
		/// <summary>
		/// A Moral é o mais importante indicador do estado psicológico de uma Unidade. De acordo com o rumo do combate, uma Unidade pode ganhar ou perder Pontos de Moral, o que irá influenciar diretamente a eficácia de suas ações. Unidades com baixa Moral tendem a errar e se desesperar com mais facilidade, enquanto Unidades com alta Moral realizam ações com mais segurança, reduzindo a chance de erros.
		/// </summary>
		[ ToolTip( "A Moral é o mais importante indicador do estado psicológico de uma Unidade. De acordo com o rumo do combate, uma Unidade pode ganhar ou perder Pontos de Moral, o que irá influenciar diretamente a eficácia de suas ações. Unidades com baixa Moral tendem a errar e se desesperar com mais facilidade, enquanto Unidades com alta Moral realizam ações com mais segurança, reduzindo a chance de erros." ) ]
		public ModAttribute MoralPoints {
			get { return nature.MoralPoints; }
			set { nature.MoralPoints = value; }
		}
		
		
		[ ToolTip( "Consumo atual de AP por metro deste personagem. (somente leitura)" ) ]
		public float ActionPointsPerMeter {
			// TODO: recriando toda hora esse atributo?
			get { 
				return new Attribute( 0, (int) Common.CharacterInfo.MIN_AP_PER_METER, (int) Common.CharacterInfo.MAX_AP_PER_METER ).Lerp( Agility ); 
			}
		}
		
		
		public float CounterAttackPerceptionBonus {
			get { return ( 1.0f + Perception.Percent ) / Common.CharacterInfo.COUNTERATTACK_PERCEPTION_INFLUENCE; }
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[CharacterInfoData name:" );
			sb.Append( Name );
			sb.Append( " gender:" );
			sb.Append( Gender );
			sb.Append( " HP:" );
			sb.Append( HitPoints );
			sb.Append( " AP:" );
			sb.Append( ActionPoints );
			sb.Append( " moral:" );
			sb.Append( MoralPoints );
			sb.Append( "]" );
			
			return sb.ToString();
		}
		
	}
	
}