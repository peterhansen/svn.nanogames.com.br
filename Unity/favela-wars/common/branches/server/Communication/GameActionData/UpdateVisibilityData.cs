using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	public class UpdateVisibilityData :  GameActionData {
	
		public int viewerId;
		
		public UpdateVisibilityData( int viewerId ) {
			this.viewerId = viewerId;
		}
		
		public override bool Equals( object obj ) {
			return Equals( obj as UpdateVisibilityData );
		}
		
		
		public bool Equals( UpdateVisibilityData data ) {
			return data != null && viewerId == data.viewerId;
		}
		
		
		public override int GetHashCode() {
        	return viewerId.GetHashCode();
		}
	}
}