using System;

namespace GameCommunication {
	
	public class GetCharacterMovementRequest : Request {
		
		public int characterWorldID;
		
		public GetCharacterMovementRequest( int playerID, int characterWorldID ) : base( playerID ) {
			this.characterWorldID = characterWorldID;
		}
	}
}

