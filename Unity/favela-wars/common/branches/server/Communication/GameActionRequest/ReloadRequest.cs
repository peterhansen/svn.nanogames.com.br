using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para recarregar uma arma.
	/// </summary>
	[ Serializable ]
	public class ReloadRequest : Request {
		
		public int characterWorldID;
		
		public int weaponID;
		
		
		public ReloadRequest( int playerID, int characterID, int weaponID ) : base( playerID ) {
			this.characterWorldID = characterID;
			this.weaponID = weaponID;
		}
	
	}
}

