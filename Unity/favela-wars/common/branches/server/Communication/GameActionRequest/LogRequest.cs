using System;

namespace GameCommunication
{
	[ Serializable ]
	public class LogRequest : Request {
		
		public string message;
		
		public LogLevel logLevel;
		

		public LogRequest( int playerID, LogLevel logLevel, string message ) : base( playerID ) {
			this.message = message;
			this.logLevel = logLevel;
		}
	}
}

