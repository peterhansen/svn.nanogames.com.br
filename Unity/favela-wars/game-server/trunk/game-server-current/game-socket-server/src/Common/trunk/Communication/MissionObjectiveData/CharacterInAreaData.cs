using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class CharacterInAreaData : MissionObjectiveData {
		
		public AreaMissionData area;
		
		public CharacterTargetMode target;
		
		public Faction winningFaction;
		
		public int targetedCharacterID;
		
		
		public CharacterInAreaData() {
		}
		
		
		public CharacterInAreaData( AreaMissionData area, CharacterTargetMode target, Faction winningFaction ) {
			this.area = area;
			this.target = target;
			this.winningFaction = winningFaction;
		}
		
	}
	
}

