using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	[ Serializable ]
	public abstract class MissionObjectiveData {
		
		public readonly List< MissionObjectiveData > objectivesToActivate = new List< MissionObjectiveData >();
		
		public readonly List< MissionObjectiveData > objectivesToDeactivate = new List< MissionObjectiveData >();
		
	}
	
}

