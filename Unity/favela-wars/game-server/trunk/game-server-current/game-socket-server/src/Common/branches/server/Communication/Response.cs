using System;
using System.Collections.Generic;

namespace GameCommunication {
	
	[ Serializable ]
	public class Response {
		
		protected List< GameActionData > gameActionsData = new List< GameActionData >();
		
		
		public void AddGameActionData( GameActionData gameActionData ) {
			if( gameActionData != null )
				gameActionsData.Add( gameActionData );
		}
		
		
		public List< GameActionData > GetGameActionsData() {
			return gameActionsData;
		}
		
	}
}

