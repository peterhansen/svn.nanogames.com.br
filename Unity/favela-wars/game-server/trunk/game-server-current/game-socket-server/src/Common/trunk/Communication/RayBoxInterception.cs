using System;
using System.Text;


namespace GameCommunication {
	
	public class RayBoxInterception {
		
		/// <summary>
		/// 
		/// </summary>
		public float t;
		
		public GameVector point;
		
		public GameVector normal;
		
		public int worldObjectID;
		
		
		public RayBoxInterception() {
		}
		
		
		public RayBoxInterception( float t, GameVector point, int worldObjectID ) {
			this.t = t;
			this.point = point;
			this.worldObjectID = worldObjectID;
		}
		
		
		public RayBoxInterception( float t, GameVector point, GameVector normal, int worldObjectID ) : this( t, point, worldObjectID ) {
			this.normal = normal;
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[" );
			sb.Append( "t:" );
			sb.Append( t );
			sb.Append( " point:" );
			sb.Append( point );
			sb.Append( " id:" );
			sb.Append( worldObjectID );
			sb.Append( "]" );
			
			return sb.ToString();
		}
		
	}
	

	public class RayBoxInterceptionReply {
		
		public RayBoxInterception entry;
		
		public RayBoxInterception exit;
		
		
		/// <summary>
		/// Indica se houve colisão, ou seja, se o raio entrou E/OU saiu da caixa.
		/// </summary>
		public bool HasIntercepted() {
			return entry != null || exit != null;
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[" );
			sb.Append( "entry:" );
			sb.Append( entry );
			sb.Append( " exit:" );
			sb.Append( exit );
			sb.Append( "]" );
			
			return sb.ToString();
		}
		
	}
	
}

