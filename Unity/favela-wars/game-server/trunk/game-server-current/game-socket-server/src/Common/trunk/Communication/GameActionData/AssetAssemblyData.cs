using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	// TODO: classe dummy, para simbolizar uma combinacao de assets
	// para um modelo
	[Serializable]
	public class AssetAssemblyData {
		
		/// <summary>
		/// Mapeamento de nomes das malhas com os filhos.
		/// </summary>
		public List< PartMap > meshes = new List< PartMap >();
		
		/// <summary>
		/// Mapeamento de materiais para cada parte (elemento) do objeto.
		/// </summary>
		public List< PartMap > materials = new List< PartMap >();
		
		
		public void AddMaterial( string partName, string materialName ) {
			AddPartMap( materials, partName, materialName );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="partName">
		/// A <see cref="System.String"/>
		/// </param>
		/// <param name="meshName">
		/// A <see cref="System.String"/>
		/// </param>
		public void AddMesh( string partName, string meshName ) {
			AddPartMap( meshes, partName, meshName );
		}
		
		
		private void AddPartMap( List< PartMap > list, string partName, string name ) {
			PartMap map = null;
			
			foreach ( PartMap m in list ) {
				if ( m.partName.Equals( partName ) ) {
					map = m;
					break;
				}
			}
				
			if ( map == null ) {
				map = new PartMap( partName );
				list.Add( map );	
			}
			
			map.names.Add( name );
		}
		
		
		public override string ToString () {
			StringBuilder sb = new StringBuilder();
			sb.Append("[AssetAssemblyData meshes:");
			foreach ( PartMap s in meshes ) {
				sb.Append( s.partName );
				sb.Append( "(" );
				foreach ( string m in s.names ) {
					sb.Append( m );
					sb.Append( ", " );
				}
				sb.Append( "), " );
			}
			sb.Append( " materials:" );
			foreach ( PartMap s in materials ) {
				sb.Append( s.partName );
				sb.Append( "(" );
				foreach ( string m in s.names ) {
					sb.Append( m );
					sb.Append( ", " );
				}
				sb.Append( "), " );
			}
			sb.Append( "]" );
			
			return sb.ToString();
		}		
		
		
	}
	
	
	[ Serializable ]
	public class PartMap {
		
		public string partName = "";
		
		public List< string > names = new List< string >();
		
		
		public PartMap( string partName ) {
			this.partName = partName;
		}
		
	}
				         
				         
}

