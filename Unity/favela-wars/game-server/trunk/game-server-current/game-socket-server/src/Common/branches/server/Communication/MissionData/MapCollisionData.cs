using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using SharpUnit;

using Utils;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe armazena os dados dos dados de colisão de um mapa. 
	/// </summary>
	[ Serializable ]
	public class MapCollisionData {
		
		private string missionName;
		
		private List< Box > boxes;
		
		private SectorsData sectorsData;
		
		private byte[] rawAstarData;
		
		
		public MapCollisionData() {
		}
		
		
		public MapCollisionData( string missionName, List< Box > boxes, SectorsData sectorsData, byte[] rawAstarData ) {
			this.missionName = missionName;
			this.sectorsData = sectorsData;
			this.boxes = boxes;
			this.rawAstarData = rawAstarData;
		}
		
				
		public override bool Equals( object obj ) {
			return Equals( ( ( MapCollisionData ) obj ) );
		}
		
		
		public bool Equals( MapCollisionData other ) {
			return other != null && missionName.Equals( other.GetMissionName() );
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode ();
		}
		
		
		public string GetMissionName() {
			return missionName;
		}

		
		public List< Box > GetBoxes() {
			return boxes;
		}
		
		
		public SectorsData GetSectorsData() {
			return sectorsData;
		}
		
		
		public byte[] GetRawAstarData() {
			return rawAstarData;
		}
		
		
		public string GetMapCollisionFilePath() {
			string path = GetPath();
			if ( !Directory.Exists( path ) )
                Directory.CreateDirectory( path );
			
			return Path.Combine( path, missionName + Common.LEVEL_FILE_EXTENSION );
		}
		
		
		public void ExportToFile() {
			Util.SerializeToFile( this, GetMapCollisionFilePath() );
		}
		
		
		public static MapCollisionData ImportFromFile( string filename ) {
			FileStream file = null;
			
			try {
				file = new FileStream( Path.Combine( GetPath(), filename + Common.LEVEL_FILE_EXTENSION ), FileMode.Open );
				BinaryFormatter bin = new BinaryFormatter();
				MapCollisionData mapCollisionData = ( MapCollisionData ) bin.Deserialize( file );
				return mapCollisionData;
			} catch( System.Exception e ) {
				throw new InvalidMissionFile( e.Message );
			} finally {
				if ( file != null )
					file.Close();
			}
		}				
		
		public static string GetPath() {
			return Path.Combine( Common.Paths.SERVER, "missions" );
		}
	}
	
	
}


public class InvalidMapCollisionFile : Exception {
	
	public InvalidMapCollisionFile() : this( "Arquivo descritor de colisões de mapa inválido." ) {
	}
	
	
	public InvalidMapCollisionFile( string message ) : base( message ) {
	}
	
}
