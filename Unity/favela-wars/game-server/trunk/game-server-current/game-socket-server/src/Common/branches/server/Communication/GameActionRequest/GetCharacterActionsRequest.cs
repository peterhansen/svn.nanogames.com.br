using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	[ Serializable ]
	public class GetCharacterActionsRequest : Request {
		
		public int characterWorldID;
		
		public GetCharacterActionsRequest( int playerID, int characterWorldID ) : base( playerID ) {
			this.characterWorldID = characterWorldID;
		}
	}
}

