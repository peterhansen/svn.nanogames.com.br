using System;

namespace GameCommunication {
	
	public enum NPCMovementType {
		FOLLOWING,
		FLEEING
	}
	
	[ Serializable ]
	public class NPCMovement {
			
		public int worldID;
		
		public NPCMovementType type;
		
		public GameVector target;
		
		public NPCMovement( int worldID, NPCMovementType type, GameVector target ) {
			this.worldID = worldID;
			this.type = type;
			this.target = target;
		}
	}
}

