using System;
using System.Collections.Generic;


namespace GameCommunication {
	public class VisibilityInfo {
		
		protected Dictionary< int, VisibilityChangeData > visibilityDataPerStep = new Dictionary< int, VisibilityChangeData >();
		
		protected VisibilityChangeData finalVisibility;
		
		public VisibilityChangeData FinalVisibility {
			get { 
				return finalVisibility; 
			}
			set {
				finalVisibility = value;
			}
		}
		
		public void SetVisibilityAtStep( int step, VisibilityChangeData visibilityChangeData ) {
			visibilityDataPerStep[ step ] = visibilityChangeData;
		}
		
		public bool ContainsStep( int step ) {
			return visibilityDataPerStep.ContainsKey( step );
		}
		
		public VisibilityChangeData GetVisibilityChangeData( int step ) {
			return visibilityDataPerStep[ step ];
		}
		
		public void RemoveVisibility( int step ) {
			visibilityDataPerStep.Remove( step );
		}
	}
}

