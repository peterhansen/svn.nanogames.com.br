using System;
using System.Collections;
using System.Collections.Generic;

namespace GameCommunication {

	
	[Serializable]
	public class CreateGameObjectData : GameActionData {
		
		public int worldObjectID;
		
		public int worldObjectLayer;
		
		public AssetAssemblyData assemblyData;
		
		public CharacterInfoData characterData;
		
		
		public CreateGameObjectData( int worldObjectID, int worldObjectLayer, CharacterInfoData characterdata, AssetAssemblyData assetCombinationData ) {
			this.worldObjectID = worldObjectID;
			this.assemblyData = assetCombinationData;
			this.worldObjectLayer = worldObjectLayer;
			this.characterData = characterdata;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as CreateGameObjectData );
		}
		
		
		public bool Equals( CreateGameObjectData other ) {
			return other != null && worldObjectID == other.worldObjectID;
		}
		
		
		public override int GetHashCode() {
        	return ( worldObjectID ).GetHashCode();
    	}
	}
}

