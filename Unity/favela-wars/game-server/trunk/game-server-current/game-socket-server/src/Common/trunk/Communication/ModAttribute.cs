using System;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class ModAttribute : Attribute {
		
		/// <summary>
		/// Modificador do atributo, como um power-up power-down.
		/// </summary>
		protected float modifier = 0;
		
		
		public ModAttribute() : base() {
		}
		
		
		public ModAttribute( Attribute a ) : base( a ) {
		}		
		
		
		public ModAttribute( Attribute a, Attribute absLimits ) : base( a, absLimits ) {
		}		
		
		
		public ModAttribute( ModAttribute a ) : base( a ) {
			modifier = a.modifier;
		}		
		
		
		public ModAttribute( float currentValue, float min, float max ) : base( currentValue, min, max ) {
		}
		
		
		public override void Set( Attribute other ) {
			min = other.Min;
			max = other.Max;
			Value = other.Value;
		}
		
		
		public void Set( ModAttribute other ) {
			Set( ( Attribute ) other );
			modifier = other.modifier;
		}
		
		
		[ ToolTip( "Modificador do valor atual do atributo. O valor atual exibido no editor já inclui o modificador." ) ]
		public float Modifier {
			get {
				return modifier;
			}
			
			set {
				modifier = value;
			}
		}		
		
		
		/// <summary>
		/// Valor atual (com modificadores).
		/// </summary>
		[ ToolTip( "Valor atual do atributo, já incluindo o valor do modificador." ) ]
		public override float Value {
			get {
				if ( absLimits == null )
					return current + Modifier;
				else
					return NanoMath.Clamp( current + Modifier, absLimits.Min, absLimits.Max );
			}
			set { 
				if ( absLimits == null )
					current = value; 
				else
					current = NanoMath.Clamp( value, absLimits.Min, absLimits.Max );
			}
		}
		
		
		public float ValueNoMod {
			get { return current; }
		}
		
		
		public float PercentNoMod {
			get { return NanoMath.Clamp( ( ValueNoMod - Min ) / ( Max - Min ), 0, 1 ); }
		}
		
		
		public override string ToString() {
			return string.Format("[Attribute: Min={0}, Max={1}, Value={2}, Modifier={3}]", Min, Max, Value, Modifier);
		}
		
	}
	
}

