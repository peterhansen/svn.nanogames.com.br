using System;
using System.Collections;

using Utils;

namespace GameCommunication {
	
	[Serializable]
	public class TurnData {
		
		public int turnNumber;
		
		public Faction faction;
		
		public DateTime initialTurnTime;
		
		
		public TurnData( int turnNumber, Faction faction ) {
			this.turnNumber = turnNumber;
			this.faction = faction;
		}
		
		
		public TurnData( TurnData turnData ) {
			turnNumber = turnData.turnNumber;
			faction = turnData.faction;
		}
		
	}
}

