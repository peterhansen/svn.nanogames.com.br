using System;


namespace GameCommunication {

	/// <summary>
	/// Log level for the Log Action
	/// </summary>
	public enum LogLevel {
		NORMAL,
		WARNING,
		ERROR
	};
	
	[Serializable]
	public class LogActionData : GameActionData {
		
		public LogLevel logLevel;
		public string message;
		
		
		public LogActionData( LogLevel level, string message ) {
			this.logLevel = level;
			this.message = message;
		}
		
		
		override public bool Equals( object obj ) {
			
			if ( obj == this )
				return true;
			
			if ( obj.GetType() != this.GetType() ) {
				return false;
			}
			else {
				return Equals( ( ( LogActionData ) obj ) );
			}
		}
		
		
		public bool Equals( LogActionData data ) {
			return ( logLevel == data.logLevel ) && ( message == data.message );
		}
		
		
		override public int GetHashCode() {
        	return ( logLevel.ToString() + message ).GetHashCode();
    	}
	}
}

