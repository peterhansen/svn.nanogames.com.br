using System;

using SharpUnit;
using GameCommunication;
using System.Collections.Generic;
using Utils;
using System.Text;


namespace GameCommunication {
	
	[ Serializable ]
	public class GameTransform {
		
		public GameVector position = new GameVector();
		
		public GameVector direction = new GameVector( GameVector.ZAxis );
		
		public GameVector scale = new GameVector( 1.0f, 1.0f, 1.0f );
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as GameTransform );
		}
		
		
		public bool Equals( GameTransform other ) {
			return other != null && position.Equals( other.position ) && 
				scale.Equals( other.scale ) && 
				direction.Equals( other.direction );
		}
		
		
		public bool Intersect( Edge edge ) {
			float t1 = 0.0f;
			float t2 = 0.0f;
			
			Intersect( edge, out t1, out t2 );
			
			return !( ( t1 >= 1.0f || t1 < 0.0f ) || ( t2 >= 1.0f || t2 < 0.0f ) );
		}
			
		
		public void Intersect( Edge edge, out float t1, out float t2 ) {
			float x1;
			float x2;
			float x3;
			float x4;
			float y1;
			float y2;
			float y3;
			float y4;
			float under;
			float candidate;
			
			t1 = float.MaxValue;
			t2 = float.MaxValue;
			
			Edge[] boxEdges = GetEdges();
			Edge currentEdge;
			
			for ( int c = 0; c <  boxEdges.Length; ++c ) {
				
				currentEdge = boxEdges[ c ];
				
				x1 = edge.p1.x;
				x3 = currentEdge.p1.x;
				y1 = edge.p1.z;
				y3 = currentEdge.p1.z;
				
				x2 = edge.p2.x;
				x4 = currentEdge.p2.x;
				y2 = edge.p2.z;
				y4 = currentEdge.p2.z;
				under = ( (y4-y3) * (x2-x1) - (x4-x3) * (y2-y1) );
				
				if ( NanoMath.Equals( under, 0.0f )  )
					continue;

				candidate = ( (x4-x3) * (y1-y3) - (y4-y3) * (x1-x3) ) / under;
				
				if ( candidate < t1 && candidate >= 0.0f )
					t1 = candidate;
				
				candidate = ( (x2-x1) * (y1-y3) - (y2-y1) * (x1-x3) ) / under;
				
				if ( candidate < t2 && candidate >= 0.0f )
					t2 = candidate;
			}
		}
		
		
		public void DebugDraw() {
			DebugDraw( 0x0000ffff );
		}
	
		
		public void DebugDraw( uint color ) {
			Edge[] edges = GetEdges();
			
			for ( int c = 0; c < edges.Length; ++c ) {
				edges[ c ].DebugDraw( color );
			}
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.Append( "GameTransform { pos" );
			sb.Append( position );
			sb.Append( " dir" );
			sb.Append( direction );
			sb.Append( " sca" );
			sb.Append( scale );
			sb.Append( " }" );
			
			return sb.ToString();
		}
		
		
		public GameVector[] GetPoints() {
			List< GameVector > pointsInVolume = new List< GameVector >();
			
			GameVector halfScale = scale * 0.5f;
			GameVector min = position - halfScale;
			GameVector max = position + halfScale;
			
			pointsInVolume.Add( new GameVector( min.x, min.y, min.z ) ); //0
			pointsInVolume.Add( new GameVector( min.x, min.y, max.z ) ); //1
			pointsInVolume.Add( new GameVector( max.x, min.y, min.z ) ); //2
			pointsInVolume.Add( new GameVector( max.x, min.y, max.z ) ); //3
			pointsInVolume.Add( new GameVector( max.x, max.y, min.z ) ); //4
			pointsInVolume.Add( new GameVector( max.x, max.y, max.z ) ); //5
			pointsInVolume.Add( new GameVector( min.x, max.y, min.z ) ); //6
			pointsInVolume.Add( new GameVector( min.x, max.y, max.z ) ); //7
			
			return pointsInVolume.ToArray();
		}


		public Edge[] GetEdges() {
			GameVector[] points = GetPoints();
			
			List<Edge> edgesInVolume = new List<Edge>();
			
			edgesInVolume.Add( new Edge( points[ 0 ], points[ 1 ] ) ); //0
			edgesInVolume.Add( new Edge( points[ 0 ], points[ 2 ] ) ); //1
			edgesInVolume.Add( new Edge( points[ 3 ], points[ 2 ] ) ); //2
			edgesInVolume.Add( new Edge( points[ 3 ], points[ 1 ] ) ); //3
			
			edgesInVolume.Add( new Edge( points[ 4 ], points[ 5 ] ) ); //4
			edgesInVolume.Add( new Edge( points[ 4 ], points[ 6 ] ) ); //5
			edgesInVolume.Add( new Edge( points[ 7 ], points[ 6 ] ) ); //6
			edgesInVolume.Add( new Edge( points[ 7 ], points[ 5 ] ) ); //7
			
			edgesInVolume.Add( new Edge( points[ 0 ], points[ 6 ] ) ); //8
			edgesInVolume.Add( new Edge( points[ 1 ], points[ 7 ] ) ); //9
			edgesInVolume.Add( new Edge( points[ 2 ], points[ 4 ] ) ); //10
			edgesInVolume.Add( new Edge( points[ 3 ], points[ 5 ] ) ); //11
			
			return  edgesInVolume.ToArray();
		}	
		
		
		public override int GetHashCode() {
			// TODO aqui ainda se obt�m o ToString() desses objetos, o que pode ser simplificado
			return string.Format( "{0}{1}{2}", position, direction, scale ).GetHashCode();
		}
	
		
		protected float FlipNegativeAngles( float angle ) {
			return angle < 0 ? 360.0f - ( Math.Abs( angle ) % 360.0f ) : angle;
		}
		
		
		public void Set( GameTransform gt ) {
			position.Set( gt.position );
			scale.Set( gt.scale );
			direction.Set( gt.direction );
		}
			
		
		public static GameTransform GetRandom() {
			GameTransform gt = new GameTransform();
			gt.Randomize();
			return gt;
		}
		
		
		public void Randomize() {
			position.Randomize();
			direction.Randomize();
			scale.Randomize();
		}
	}
}

