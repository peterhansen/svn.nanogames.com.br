using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Utils {
	
public static class Util {
		
		#region Amazon S3 info
		
		private const string awsAccessKeyId = "AKIAILVD4BVW4NQMA5VA";
		
        private const string awsSecretAccessKey = "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";
		
		private const string s3BucketName = "nanofwdev";
			
		private const string encryptionKey = "XPTOVTNC";
		
        private const string encryptionIV = "FDPCUPQP";
		
		#endregion
		
		
		/// <summary>
		/// Remove acentuação e caracteres especiais de uma string. "ação" é convertido para "acao", por exemplo.
		/// </summary>
		public static string RemoveDiacritics( string s ) {
			string stFormD = s.Normalize( NormalizationForm.FormD );
			StringBuilder sb = new StringBuilder();
		
			for ( int ich = 0; ich < stFormD.Length; ++ich ) {
				UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory( stFormD[ ich ] );
		    	if( uc != UnicodeCategory.NonSpacingMark ) {
		      		sb.Append( stFormD[ ich ] );
		    	}
		  	}
		
		  	return ( sb.ToString().Normalize( NormalizationForm.FormC ) );
		}
		
		
		/// <summary>
		/// Remove diacríticos do nome, e converte espaços em branco para '_'.
		/// </summary>
		public static string GetCleanFilename( string name ) {
			return RemoveDiacritics( name ).Replace( " ", "_" );
		}
		
		
		/// <summary>
		/// Gets the filename from path.
		/// </summary>
		/// <returns>
		/// The filename from path.
		/// </returns>
		/// <param name='fullPath'>
		/// Full path.
		/// </param>
		/// <param name='keepExtension'>
		/// Keep extension.
		/// </param>
		public static string GetFilenameFromPath( string fullPath, bool keepExtension ) {
			char[] separators = { '\\', '/' };
			int index = Math.Max( 0, fullPath.LastIndexOfAny( separators ) + 1 );
			
			string filename = fullPath.Substring( index );
			if ( keepExtension )
				return filename;
			
			index = filename.LastIndexOf( '.' );
			
			return index >= 0 ? filename.Substring( 0, index ) : filename;
		}
		
		
		/// <summary>
		/// Gets the base classes.
		/// </summary>
		/// <returns>
		/// The base classes.
		/// </returns>
		/// <param name='o'>
		/// O.
		/// </param>
		public static IEnumerable< Type > GetBaseClasses( object o ) {
			foreach( Type t in GetBaseClasses( o.GetType() ) )
				yield return t;
		}
		
		
		/// <summary>
		/// Gets the base classes.
		/// </summary>
		/// <returns>
		/// The base classes.
		/// </returns>
		/// <param name='type'>
		/// Type.
		/// </param>
		public static IEnumerable<Type > GetBaseClasses( Type type ) {
			while ( type != null ) {
				yield return type;
				type = type.BaseType;
			}
		}
		
		
		/// <summary>
		/// Serializes to XML file.
		/// </summary>
		/// <returns>
		/// The to XML file.
		/// </returns>
		/// <param name='o'>
		/// If set to <c>true</c> o.
		/// </param>
		/// <param name='path'>
		/// If set to <c>true</c> path.
		/// </param>
		public static bool SerializeToXMLFile( Object o, string path ) {
			FileStream file = null;
			try {
				file = new FileStream( path, FileMode.OpenOrCreate );
				StreamWriter writer = new StreamWriter( file, Encoding.UTF8 );
				Type[] baseTypes = new List< Type >( GetBaseClasses( o ) ).ToArray();
				XmlSerializer x = new XmlSerializer( o.GetType(), baseTypes );
				x.Serialize( writer, o );	
				
				return true;
			} catch ( Exception e ) {
				Debugger.LogError( "Error serializing " + o + " to XML file " + path + "." + e );
				
				return false;
			} finally {
				if ( file != null )
					file.Close();
			}
		}
		
		
		/// <summary>
		///  
		/// </summary>
		/// <param name="o">
		/// A <see cref="Object"/>
		/// </param>
		/// <param name="path">
		/// A <see cref="System.String"/>
		/// </param>
		public static bool SerializeToFile( Object o, string path ) {
			// TODO escrever testes de gravação e leitura dos serializadores
			FileStream file = null;
			try {
				file = new FileStream( path, FileMode.OpenOrCreate );
				BinaryFormatter bin = new BinaryFormatter();
				bin.Serialize( file, o );
				
				return true;
			} catch ( Exception e ) {
				Debugger.LogError( "Error serializing " + o + " to file " + path + "." + e );
				
				return false;
			} finally {
				if ( file != null )
					file.Close();
			}
		}	
		
		
		/// <summary>
		/// Obtém a representação em byte[] de um objeto serializável.
		/// </summary>
		public static byte[] ObjectToByteArray( Object o ) {
			MemoryStream memStream = new MemoryStream();
		    BinaryFormatter bin = new BinaryFormatter();
			
			bin.Serialize( memStream, o );
			
			return memStream.ToArray();
		}
		
		
		/// <summary>
		/// Deserializa um objeto a partir do caminho especificado. 
		/// </summary>
		/// <param name="path">
		/// Caminho do arquivo que contém o objeto serializado.
		/// </param>
		/// <returns>
		/// O objeto deserializado.
		/// </returns>
		public static object DeserializeFromFile( string path ) {
			FileStream file = null;
			BinaryFormatter bin = new BinaryFormatter();
			bin.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
			
			try {
				file = new FileStream( path, FileMode.Open );
				return bin.Deserialize( file );
			} catch ( Exception e ) {
				Debugger.LogError( "Error deserializing file " + path + ":\n" + e );
				return null;
			} finally {
				if ( file != null )
					file.Close();
			}			
		}
		
		
		/// <summary>
		/// Deserializes from XML file.
		/// </summary>
		/// <returns>
		/// The from XML file.
		/// </returns>
		/// <param name='path'>
		/// Path.
		/// </param>
		/// <param name='type'>
		/// Type.
		/// </param>
		public static object DeserializeFromXMLFile( string path, Type type ) {
			FileStream file = null;
			try {
				// os tipos-base são necessários porque o C# idiota não trata bem herança ao deserializar XML
				Type[] baseTypes = new List< Type >( GetBaseClasses( type ) ).ToArray();
				XmlSerializer x = new XmlSerializer( type, baseTypes );
				file = new FileStream( path, FileMode.Open );
				StreamReader reader = new StreamReader( file, Encoding.UTF8 );
				
				return x.Deserialize( file );
			} catch ( Exception e ) {
				Debugger.LogError( "Error deserializing XML file " + path + ":\n" + e );
				return null;
			} finally {
				if ( file != null )
					file.Close();
			}
		}
		
		
		/// <summary>
		/// Deserializes from XML file.
		/// </summary>
		/// <returns>
		/// The from XML file.
		/// </returns>
		/// <param name='path'>
		/// Path.
		/// </param>
		/// <typeparam name='T'>
		/// The 1st type parameter.
		/// </typeparam>
		public static T DeserializeFromXMLFile< T >( string path ) where T : class {
			try {
				Type t = typeof( T );
				return ( T ) Convert.ChangeType( DeserializeFromXMLFile( path, t ), t );
			} catch ( Exception e ) {
				Debugger.LogError( "Error deserializing file " + path + ":\n" + e );
				return null;
			}		
		}
		
		
		/// <summary>
		/// Deserializa um objeto a partir do caminho especificado. 
		/// </summary>
		/// <param name="path">
		/// Caminho do arquivo que contém o objeto serializado.
		/// </param>
		/// <returns>
		/// O objeto deserializado.
		/// </returns>
		public static T DeserializeFromFile< T >( string path ) where T : class {
			try {
				return ( T ) Convert.ChangeType( DeserializeFromFile( path ), typeof( T ) );
			} catch ( Exception e ) {
				Debugger.LogError( "Error deserializing file " + path + ":\n" + e );
				return null;
			}			
		}	
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="o">
		/// A <see cref="Object"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.String"/>
		/// </returns>
		public static string SerializeToBase64String( Object o ) {
			return System.Convert.ToBase64String( ObjectToByteArray( o ) );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="base64String">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="T"/>
		/// </returns>
		public static T DeserializeFromBase64String< T >( string base64String ) {
			return ( T ) Convert.ChangeType( DeserializeFromBase64String( base64String ), typeof( T ) );
		}		
		
		
		/// <summary>
		/// Deserializes from base64 string.
		/// </summary>
		/// <returns>
		/// The from base64 string.
		/// </returns>
		/// <param name='base64String'>
		/// Base64 string.
		/// </param>
		public static object DeserializeFromBase64String( string base64String ) {
	 		MemoryStream memStream = new MemoryStream( System.Convert.FromBase64String( base64String ) );
			BinaryFormatter bin = new BinaryFormatter();
			bin.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
			
			return bin.Deserialize( memStream );
		}		
		
		
		/// <summary>
		/// Saves to file.
		/// </summary>
		/// <param name='data'>
		/// Data.
		/// </param>
		/// <param name='path'>
		/// Path.
		/// </param>
		public static void SaveToFile( byte[] data, string path ) {
			FileStream file = null;
			try {
				file = new FileStream( path, FileMode.OpenOrCreate );
				file.Write( data, 0, data.Length );
			} catch ( Exception e ) {
				Debugger.LogError( "Error saving data to file " + path + ":\n" + e );
			} finally {
				if ( file != null )
					file.Close();
			}
		}
		
	}
	
	
}

