using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	public interface ISectorManager {
		
		Sector GetSectorFromGameVector( GameVector vector );
		
		List< Sector > Get3DSectorsFromViewAreaSorted( ViewArea v );
		
		IEnumerable< Sector > GetSectorsFromViewArea( ViewArea v );
		
		IEnumerable< Sector > Get3DSectorsFromViewArea( ViewArea v );
		
		IEnumerable< Sector > GetBoxSectors( Box box );
		
		
	}
}

