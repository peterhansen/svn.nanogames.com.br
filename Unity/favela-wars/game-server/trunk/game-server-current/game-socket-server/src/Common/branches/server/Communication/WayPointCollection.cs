using System;
using System.Collections.Generic;
using System.Collections;
using Utils;

namespace GameCommunication
{
	[ Serializable ]
	public class WayPointCollection
	{
		
		private Dictionary< WayPoint, bool > waypointCollection;
		
		public WayPointCollection ()
		{
			waypointCollection = new Dictionary< WayPoint, bool >();
		}
		
		~WayPointCollection(){
			clear();
			waypointCollection = null;
		}
		
		public void addWaypoint(float x, float y, float z){
			WayPoint w = new WayPoint( x, y, z );
			addWaypoint(w);
		}
		
		public void addWaypoint(GameVector v){
			WayPoint w = new WayPoint( v );
			addWaypoint(w);
		}
		
		public void addWaypoint(WayPoint v){
			this.waypointCollection.Add(v,false);
		}
		
		public void clear(){
			waypointCollection.Clear();
		}
		
		public void setOccupied(WayPoint w, bool occupied){
			if( waypointCollection.ContainsKey(w) ){
				waypointCollection[w] = occupied;
			}
		}
		
		public int count(){
			return waypointCollection.Count;
		}
		
		public WayPoint getRandomWayPoint(){
			List < WayPoint > temp = new List< WayPoint > ();
			foreach(KeyValuePair< WayPoint , bool > k in waypointCollection){
				if ( k.Value == false ){
					temp.Add(k.Key);
				}
			}
			if(temp.Count > 0){
				float randomNumber = NanoMath.Random(0f, (float)temp.Count);
				waypointCollection[ temp[(int)randomNumber] ] = true;
				return temp[(int)randomNumber];
			}
			
			return null;
			
		}
		
	}
}

