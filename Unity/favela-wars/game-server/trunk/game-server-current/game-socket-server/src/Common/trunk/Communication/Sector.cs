using System;
using System.Collections.Generic;


using SharpUnit;


namespace GameCommunication {
	
	[Serializable]
	public struct Sector {
		
		public const int NULL = ( MAX_I << SHIFT_I ) | ( MAX_J << SHIFT_J ) | MAX_K;
		
		/// <summary>
		/// Quantidade de bits usados para representar a componente 'i' (x) do setor.
		/// </summary>
		public const byte BITS_I = 8; // TODO 11
		
		/// <summary>
		/// Quantidade de bits usados para representar a componente 'j' (y) do setor.
		/// </summary>
		public const byte BITS_J = 8;
		
		/// <summary>
		/// Quantidade de bits usados para representar a componente 'k' (z) do setor.
		/// </summary>
		public const byte BITS_K = 8; // TODO 11
		
		/// <summary>
		/// Teto do valor da componente i (exclusivo, logo o valor máximo é MAX_I - 1).
		/// </summary>
		public const int CEIL_I = 1 << BITS_I;
		
		/// <summary>
		/// Teto do valor da componente j (exclusivo, logo o valor máximo é MAX_J - 1).
		/// </summary>
		public const int CEIL_J = 1 << BITS_J;
		
		/// <summary>
		/// Teto do valor da componente k (exclusivo, logo o valor máximo é MAX_K - 1).
		/// </summary>
		public const int CEIL_K = 1 << BITS_K;
		
		public const int MAX_I = CEIL_I - 1;
		public const int MAX_J = CEIL_J - 1;
		public const int MAX_K = CEIL_K - 1;
		
		private const int SHIFT_I = ( BITS_J + BITS_K );
		private const byte SHIFT_J = BITS_K;
		
		private const int MASK_I = MAX_I << SHIFT_I;
		private const int MASK_J = MAX_J << SHIFT_J;
		private const int MASK_K = MAX_K;
		
		public int sector;
		
		
		public int i {
			get {
				return ( sector & MASK_I ) >> SHIFT_I;
			}
		}
		
		
		public int j {
			get {
				return ( sector & MASK_J ) >> SHIFT_J;
			}
		}
		
		
		public int k {
			get {
				return ( sector & MASK_K );	
			}
		}
		
		
		public Sector( int sector) {
			this.sector = sector;
		}
		
		
		public Sector( int i, int j, int k ) {
			sector = ToSector( i, j, k );
		}
		
		
		public Sector( GameVector v ) {
			sector = ToSector( ( int ) v.x, ( int ) v.y, ( int ) v.z );
		}
		
		
		public static int ToSector( int i, int j, int k ) {
			if ( i < 0 || j < 0 || k < 0 )
				return NULL;
			
			return ( i << SHIFT_I ) | ( j << SHIFT_J ) | k;
		}
		
		
		public void Set( int i, int j, int k ) {
			sector = ToSector( i, j, k );
		}
		
		
		/// <summary>
		/// Representação de i, j e k num GameVector.
		/// </summary>
		public GameVector ToGameVector() {
			return new GameVector( i, j, k );	
		}
		
		
		public override bool Equals( Object obj ) {
			return ( obj is Sector ) && Equals( ( Sector ) obj );
		}
		
		
		public bool Equals( Sector other ) {
			return sector == other.sector;
		}
		
		
		public override string ToString() {
			return string.Format( "[Sector({0},{1},{2}]", i, j, k );
		}
		
		
		public override int GetHashCode() {
			return sector;
		}
		
		
		/// <summary>
		/// Indica se o setor é válido, ou seja, se é diferente do setor NULL.
		/// </summary>
		public bool IsValid() {
			return sector != NULL;
		}
		
				
		/// <param name='s'>
		/// Permite a conversão implícita de um setor para um inteiro.
		/// </param>
		public static implicit operator int ( Sector s ) {
		    return s.sector;
		}		
		
		
		/// <param name='i'>
		/// Permite a conversão implícita de um inteiro para um setor.
		/// </param>
		public static implicit operator Sector ( int i ) {
		    return new Sector( i );
		}		
		
		
		public static bool operator == ( Sector s1, Sector s2 ) {
			return s1.Equals( s2 );
		}
		
		
		public static bool operator != ( Sector s1, Sector s2 ) {
			return !s1.Equals( s2 );
		}
		
	}
	
	
	[Serializable]
	public class SectorEqualityComparer : IEqualityComparer< Sector > {
		public bool Equals( Sector x, Sector y ) {
			return x.Equals( y );
		}

		public int GetHashCode( Sector sector ) {
			return sector.GetHashCode();
		}
	}
	
	
	#region SectorTestCase
	public class SectorTestCase : TestCase {
		
		[UnitTest]
		public void TestRandomUValues() {
			Random r = new Random();
			
			for ( int times = 0; times < 20; ++times ) {
				int i = r.Next( Sector.CEIL_I );
				int j = r.Next( Sector.CEIL_J );
				int k = r.Next( Sector.CEIL_K );
				
				Sector s = new Sector( i, j, k );
				
				Assert.Equal( i, s.i );
				Assert.Equal( j, s.j );
				Assert.Equal( k, s.k );
			}
		}
		
		
		[UnitTest]
		public void TestNullValue() {
			Sector s = new Sector( Sector.MAX_I, Sector.MAX_J, Sector.MAX_K );
			Assert.True( s == Sector.NULL );
		}
		
	}
	#endregion
	
}

