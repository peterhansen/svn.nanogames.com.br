using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class ResistNumberOfTurnsData : MissionObjectiveData {
		
		public int numberOfTurns = 0;
	
		public Faction faction = Faction.NONE;
		
		
		public ResistNumberOfTurnsData() {
		}
		
		
		public ResistNumberOfTurnsData( Faction faction, int numberOfTurns ) {
			this.faction = faction;
			this.numberOfTurns = numberOfTurns;
		}
		
	}
	
}

