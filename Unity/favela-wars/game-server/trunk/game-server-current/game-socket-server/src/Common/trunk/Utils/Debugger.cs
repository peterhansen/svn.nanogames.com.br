using System;
using System.IO;
using GameCommunication;


namespace Utils {
	
	public delegate void ConsoleWriter( object message );
	
	public delegate void LineDrawer( GameVector origin, GameVector destination, uint color, float time );
	
	
	public static class Debugger {
		
		private static string path = null;
		
		private static StreamWriter fileWriter;
		
		private static ConsoleWriter regularWriter;
		
		private static ConsoleWriter warningWriter;
		
		private static ConsoleWriter errorWriter;
		
		private static LineDrawer lineDrawer;
		
		private static bool writeToFile = true;
		
		
		public static void SetRegularLogger( ConsoleWriter regular ) {
			regularWriter = regular;
		}
		
		
		public static bool HasRegularLogger() {
			return regularWriter != null;
		}
		
		
		public static void SetWarningLogger( ConsoleWriter warning ) {
			warningWriter = warning;
		}
		
		
		public static void SetErrorLogger( ConsoleWriter error ) {
			errorWriter = error;
		}
		
		
		public static void SetLineDrawer( LineDrawer ld ) {
			lineDrawer = ld;
		}
		
		
		public static void SetWriteToFile( bool w ) {
			writeToFile = w;
		}
		
		
		private static void init() {
			if ( path == null || fileWriter == null ) {
				string dir = Path.Combine( System.Environment.CurrentDirectory, "log" );
				
				if ( !Directory.Exists( dir ) )
                	Directory.CreateDirectory( dir );
				
				path = Path.Combine( dir, "log.txt" );
				fileWriter = new StreamWriter( path, true );
				
				// pula algumas linhas entre execuções
				fileWriter.WriteLine( "\n----- " + GetDateString() + "BEGIN -----\n" );
				fileWriter.Flush();
			}
		}
		
		
		public static void Release() {
			if ( fileWriter != null ) {
				fileWriter.WriteLine( "\n----- " + GetDateString() + "END -----\n" );
				fileWriter.Close();
				fileWriter = null;
			}
		}
	
		
		public static void Log( object message ) {
			init();
			message = GetDateString() + message;
			if ( writeToFile ) {
				fileWriter.WriteLine( message );
				fileWriter.Flush();
			}
			
			if ( regularWriter != null )
				regularWriter( message );
		}
		
		
		public static void LogWarning( object message ) {
			init();
			message = "[!] " + GetDateString() + message;
			if ( writeToFile ) {
				fileWriter.WriteLine( message );
				fileWriter.Flush();
			}
			
			if ( warningWriter != null )
				warningWriter( message );
		}
		
		
		public static void LogError( object message ) {
			init();
			message = "[E] " + GetDateString() + message;
			if ( writeToFile ) {
				fileWriter.WriteLine( message );
				fileWriter.Flush();
			}
			
			if ( errorWriter != null )
				errorWriter( message );
		}
		
		
		private static string GetDateString() {
			DateTime t = DateTime.Now;
			return t.Date.ToShortDateString() + " " + t.ToLongTimeString() + ": ";
		}
		
		
		public static void DrawLine( GameVector origin, GameVector destination, uint color, float time ) {
			if ( lineDrawer != null ) {
				lineDrawer( origin, destination, color, time );	
			}
		}
	
	}
}

