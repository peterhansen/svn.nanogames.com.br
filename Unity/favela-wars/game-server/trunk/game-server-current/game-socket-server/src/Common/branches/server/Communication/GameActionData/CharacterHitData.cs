using System;


namespace GameCommunication {
	
	[Serializable]
	public class CharacterHitData : GameActionData {
		
		public int person;
		public int damage;
		public bool isAlive;
		
		public CharacterHitData ( int hitPerson, int damage, bool isAlive ) {
			this.damage = damage;
			this.person = hitPerson;
			this.isAlive = isAlive;
		}
	}
}

