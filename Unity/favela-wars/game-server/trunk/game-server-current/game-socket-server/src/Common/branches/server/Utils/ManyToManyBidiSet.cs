using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Utils {
	
	[Serializable]
	public class ManyToManyBidiSet< A, B >  {
				
		private Dictionary< A, List< B > > mapAToB;
		private Dictionary< B, List< A > > mapBToA;
		
		public Dictionary< A, List< B > >.KeyCollection ASet() {
			return mapAToB.Keys;
		}
		
		public Dictionary< B, List< A > >.KeyCollection BSet() {
			return mapBToA.Keys;
		}
		
		public ManyToManyBidiSet() {
			mapAToB = new Dictionary< A, List< B > >();
			mapBToA = new Dictionary< B, List< A > >();
		}
		
		public ManyToManyBidiSet( IEqualityComparer<A> ASetEqualityComparer, IEqualityComparer<B> BSetEqualityComparer ) {
			mapAToB = new Dictionary< A, List< B > >( ASetEqualityComparer );
			mapBToA = new Dictionary< B, List< A > >( BSetEqualityComparer );
		}
		
		
		public List< B > Get( A elementOfSetA ) {
			return mapAToB[ elementOfSetA ];
		}
		
		public List< A > GetReverse( B elementOfSetB ) {
			return mapBToA[ elementOfSetB ];
		}
		
		public void Clear() {
			mapAToB.Clear();
			mapBToA.Clear();
		}
		
		public void AddToSetA( A elementOfSetA ) {
			if( !mapAToB.ContainsKey( elementOfSetA ) ) {
				mapAToB[ elementOfSetA ] = new List< B >();
			}
		}
		
		public void AddToSetB( B elementOfSetB ) {
			if( !mapBToA.ContainsKey( elementOfSetB ) ) {
				mapBToA[ elementOfSetB ] = new List< A >();
			}
			
		}
		
		public void Remove( A elementOfSetA ) {
			foreach ( B orphan in mapAToB[ elementOfSetA ] ) {
				mapBToA[ orphan ].Remove( elementOfSetA );
			}
				
			mapAToB.Remove( elementOfSetA );
		}
		
		public void Remove( B elementOfSetB ) {
			foreach ( A orphan in mapBToA[ elementOfSetB ] ) {
				mapAToB[ orphan ].Remove( elementOfSetB );
			}
				
			mapBToA.Remove( elementOfSetB );
		}
		
		public void ConnectElements( A elementOfSetA, B elementOfSetB ) {
			if( !mapAToB.ContainsKey( elementOfSetA ) ) {
				throw new ArgumentException( "Conjunto A não contem " + elementOfSetA );
			}
			
			if( !mapBToA.ContainsKey( elementOfSetB ) ) {
				throw new ArgumentException( "Conjunto B não contem " + elementOfSetB );
			}
			
			mapAToB[ elementOfSetA ].Add( elementOfSetB );
			mapBToA[ elementOfSetB ].Add( elementOfSetA );
		}
	}
}



