using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para obter o estado de uma partida.
	/// </summary>
	[ Serializable ]
	public class LoadSnapshotRequest : Request {
		
		public LoadSnapshotRequest( int playerID ) : base( playerID ){
		}
	
	}
}

