using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[Serializable]
	public class SectorsData {
		
		private int maxI;
		
		private int maxJ;
		
		private int maxK;
		
		protected SectorConnections sectorsToBoxes = new SectorConnections();
		
		private Box box = new Box();
		
		public GameVector minPoint = new GameVector();
		
		public GameVector maxPoint = new GameVector();	
		
		/// <summary>
		/// Gets the max i.
		/// </summary>
		/// <value>
		/// The max i.
		/// </value>
		public int MaxI {
			get {
				return maxI;
			}
		}
	
		/// <summary>
		/// Gets the max j.
		/// </summary>
		/// <value>
		/// The max j.
		/// </value>
		public int MaxJ {
			get {
				return maxJ;
			}
		}
	
		
		/// <summary>
		/// Gets the max k.
		/// </summary>
		/// <value>
		/// The max k.
		/// </value>
		public int MaxK {
			get {
				return maxK;
			}
		}
		
		
		public IEnumerable< Sector > GetAllSectors() {
			foreach( Sector s in sectorsToBoxes.GetAllSectors() ) {
				yield return s;
			}
		}	
		
		
		public void AddSector( Sector sector ) {
			sectorsToBoxes.AddSector( sector );
		}
		
		
		public Sector GetSector( Sector s ) {
			return sectorsToBoxes.GetSector( s );
		}
		
		
		public Sector GetSector( int i, int j, int k ) {
			return GetSector( new Sector( i, j, k ) );
		}
			
		
		public Box GetLevelBox() {
			return box;
		}
		
		
		public void Add( Box box ) {
			sectorsToBoxes.AddBox( box );
		}
		
		
		public void Connect( Sector sector, Box box ) {
			sectorsToBoxes.Connect( sector, box );
		}
		
		
		public void Disconnect( Box box ) {
			sectorsToBoxes.Disconnect( box );
		}
		
		
		public IEnumerable< Box > GetSectorBoxes( Sector sector ) {
			return sectorsToBoxes.GetSectorBoxes( sector );
		}
		
		
		public IEnumerable< Sector > GetBoxSectors( Box box ) {
			return sectorsToBoxes.GetBoxSectors( box );
		}
		
		
		public IEnumerable< Sector > CalculateBoxSectors( Box box ) {
			GameVector lowerPoint = box.GetNorthWestBottom();
			GameVector higherPoint = box.GetSouthEastTop();
			
			// Note que no higherSector o ponto y é acrescido de um nível, para garantir que um personagem 
			// parado sobre este objeto tenha um setor válido
			Sector lowerSector = CreateSectorFromGameVector( lowerPoint.x, lowerPoint.y, lowerPoint.z );
			Sector higherSector = CreateSectorFromGameVector( higherPoint.x, higherPoint.y + Common.LEVEL_LAYER_HEIGHT, higherPoint.z );
			
			for( int i = lowerSector.i; i <= higherSector.i; i++ ) {
				for( int j = lowerSector.j; j <= higherSector.j; j++ ) {
					for( int k = lowerSector.k; k <= higherSector.k; k++ ) {
						Sector s = new Sector( i, j, k );
						if( s.IsValid() )
							yield return s;
					}
				}
			}
		}
		
		
		/// <summary>
		/// Calculates the origin.
		/// </summary>
		/// <param name='boxes'>
		/// Boxes.
		/// </param>
		/// <param name='minPoint'>
		/// Minimum point.
		/// </param>
		/// <param name='maxPoint'>
		/// Max point.
		/// </param>
		public void CalculateMaxPoint( List< Box > boxes ) {
			minPoint = new GameVector( float.MaxValue, float.MaxValue, float.MaxValue );
			maxPoint = new GameVector( float.MinValue, float.MinValue, float.MinValue );
			
			foreach( Box box in boxes ) {
				// TODO: por enquanto, assume-se que todos as caixas estão alinhadas nos eixos
				minPoint.x = Math.Min( box.GetNorthWestBottom().x, minPoint.x );
				minPoint.y = Math.Min( box.GetNorthWestBottom().y, minPoint.y );
				minPoint.z = Math.Min( box.GetNorthWestBottom().z, minPoint.z );
				maxPoint.x = Math.Max( box.GetSouthEastTop().x, maxPoint.x );
				maxPoint.y = Math.Max( box.GetSouthEastTop().y, maxPoint.y );
				maxPoint.z = Math.Max( box.GetSouthEastTop().z, maxPoint.z );
			}
		}
		
		
		public void GenerateSectors( List< Box > boxes ) {
			// calcula o ponto máximo da missão a partir das caixas
			CalculateMaxPoint( boxes );
			CalculateLimits();
			
			// adiciona caixas, criando os setores os quais elas vao ocupar
			foreach( Box box in boxes ) {
				Add( box );
				CreateAndConnectBoxSectors( box, true );
			}
		}		
		
		
		/// <summary>
		/// Cria os setores referentes a uma caixa de colisão, e cria as conexões entre a caixa e estes setores.
		/// </summary>
		/// <param name='box'>
		/// Caixa a ser inserida.
		/// </param>
		/// <param name='fillUntilBottom'>
		/// Indica se, ao criar os setores da caixa, os setores abaixo dela devem também ser preenchidos, evitando
		/// "buracos" ao percorrer os setores na vertical.
		/// </param>
		public void CreateAndConnectBoxSectors( Box box, bool fillUntilBottom ) {
			foreach( Sector s in CalculateBoxSectors( box ) ) {
				if ( fillUntilBottom ) {
					int i = s.i;
					int k = s.k;
					
					// preenche para baixo até que haja um setor válido ou se atinja a camada mais baixa possível
					for ( int j = s.j - 1; j >= 0; --j ) {
						Sector s2 = new Sector( i, j, k );
						if ( GetSector( s2 ).IsValid() ) {
							break;
						} else {
							AddSector( s2 );
						}
					}
				}
				
				AddSector( s );
				Connect( s, box );
			}
		}		
		

		public Sector CreateSectorFromGameVector( float x, float y, float z ) {
			Sector s = new Sector( ( int ) ( ( x - minPoint.x ) / Common.LEVEL_SECTOR_WIDTH ), 
			                  	   ( int ) ( ( y - minPoint.y ) / Common.LEVEL_LAYER_HEIGHT ),
			                  	   ( int ) ( ( z - minPoint.z ) / Common.LEVEL_SECTOR_WIDTH ) );

			if ( s.IsValid() )
				sectorsToBoxes.AddSector( s );
			
			return s;
		}
		
		
		public void CalculateLimits() {
			maxI = Math.Max( 1, ( int ) ( Math.Ceiling( maxPoint.x - minPoint.x ) / Common.LEVEL_SECTOR_WIDTH ) );
			maxJ = Math.Max( 1, ( int ) ( Math.Ceiling( maxPoint.y - minPoint.y ) / Common.LEVEL_LAYER_HEIGHT ) );
			maxK = Math.Max( 1, ( int ) ( Math.Ceiling( maxPoint.z - minPoint.z ) / Common.LEVEL_SECTOR_WIDTH ) );
			
			box = new Box( 0, ( minPoint + maxPoint ) * 0.5f, ( maxPoint - minPoint ) );
		}
	}		
}

