using System;

using Utils;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para atirar com um personagem num ponto do mapa. 
	/// </summary>
	[ Serializable ]
	public class ShootRequest : Request {
		
		public int shooterWorldID;
		
		public int weaponID;
		
		public ItemUseMode useMode;
		
		public GameVector destination;
		
		
		public ShootRequest( int playerID, int shooterID, int weaponID, ItemUseMode useMode, GameVector destination ) : base( playerID ) {
			this.shooterWorldID = shooterID;
			this.weaponID = weaponID;
			this.useMode = useMode;
			this.destination = destination;
		}
	
	}
}

