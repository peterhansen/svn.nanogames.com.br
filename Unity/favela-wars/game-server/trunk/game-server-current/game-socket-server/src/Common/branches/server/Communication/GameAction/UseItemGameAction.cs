using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class UseItemGameAction : GameAction {
		
		public int itemID;
		
		/// <summary>
		/// Alcance do item. Valores menores que zero são desconsiderados no controller. Caso contrário, se a distância
		/// ao alvo for maior que o alcance, o controller não permite a tentativa de uso.
		/// </summary>
		public float range = -1;
		
		// TODO: ainda não está sendo usado
		public ItemUseMode itemUseMode = ItemUseMode.REGULAR;
		
		public ItemTargetMode itemTargetMode = ItemTargetMode.Any;
		
		
		public UseItemGameAction( int itemID, float range, ItemTargetMode targetMode ) : base( ActionType.USE_ITEM ) {
			this.itemID = itemID;
			this.itemTargetMode = targetMode;
			this.itemUseMode = ItemUseMode.REGULAR;
			this.range = range;
		}
	}
}

