using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	[ Serializable ]
	public class ShootData : GameActionData {
		
		/// <summary>
		/// Origem do(s) disparo(s).
		/// </summary>
		public GameVector shotOrigin;
		
		/// <summary>
		/// Pontos atingidos pelos projéteis ou fragmentos - algumas armas podem disparar rajadas ou tiros dispersos, como espingardas.
		/// </summary>
		public GameVector[] hitPoints;
		
		/// <summary>
		/// Instante de saída dos projéteis, em segundos, após o disparo.
		/// </summary>
		public float[] shotTimes;

		/// <summary>
		/// ID do atirador.
		/// </summary>
		public int shooterWorldID;
		
		public float speed = 20.0f;
		
		
		public ShootData( int shooterWorldID, GameVector shotOrigin, GameVector hitPoint ) : 
						  this( shooterWorldID, shotOrigin, new float[] { 0 }, new GameVector[] { hitPoint } )
		{
		}

		
		public ShootData( int shooterWorldID, GameVector shotOrigin, float[] shotTimes, GameVector[] hitPoints ) {
			this.shooterWorldID = shooterWorldID;
			this.shotOrigin = shotOrigin;
			this.hitPoints = hitPoints;
			this.shotTimes = shotTimes;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as ShootData );
		}
		
		
		public bool Equals( ShootData data ) {
			return data != null && shooterWorldID == data.shooterWorldID && hitPoints.Equals( data.hitPoints );
		}
		
		
		public override int GetHashCode() {
        	return hitPoints.GetHashCode() + shooterWorldID.GetHashCode();
		}
		
		
		
	}
}

