using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using SharpUnit;

using Utils;
using GameCommunication;


namespace GameModel {
				
	[ Serializable ]
	public class SectorManager : ISectorManager {

		private SectorsData data;
		
		public SectorManager( SectorsData data ) {
			this.data = data;
		}
		
		
		public SectorsData GetSectorsData() {
			return data;
		}
		
		
		public void OnBoxMoved( Box box ) {
			data.Disconnect( box );
			
			foreach( Sector sector in data.CalculateBoxSectors( box ) ) {
				data.Connect( sector, box );
			}
		}
		
		
		public Box GetLevelBox() {
			return data.GetLevelBox();
		}
		
		
		public int GetLayer( Box box ) {
			// adquirindo altura do chão do andar
			Sector s = GetSectorFromGameVector( 0.0f, box.GetY0(), 0.0f );
			float floorHeight = data.minPoint.y + s.j * Common.LEVEL_LAYER_HEIGHT;
			
			// descobrindo se é parede
			if( box.GetY1() > floorHeight + Common.LEVEL_GROUND_HEIGHT ) {
				// sim!
				return ( s.j + 1 ) * 2;
			} else {
				// não, é chão
				return ( s.j * 2 ) + 1;
			}
		}
		

		public IEnumerable< Sector > GetAllSectors () {
			foreach( Sector s in data.GetAllSectors() ) {
				yield return s;
			}
		}
		

		public void GetDimensionInSectors( out int missionWidthInSectors, out int missionHeightInSectors, out int missionLengthInSectors ) {
			missionWidthInSectors = data.MaxI;
			missionHeightInSectors = data.MaxJ;
			missionLengthInSectors = data.MaxK;
		} 
		
		
		public GameVector GetMinPoint() {
			return new GameVector( data.minPoint );
		}
		
		
		public GameVector GetMaxPoint() {
			return new GameVector( data.maxPoint );
		}
		
		
		/// <summary>
		/// Gets the boxes in ray sectors.
		/// </summary>
		/// <returns>
		/// The boxes in ray sectors.
		/// </returns>
		/// <param name='origin'>
		/// Origin.
		/// </param>
		/// <param name='target'>
		/// Target.
		/// </param>
		public IEnumerable< Box > GetBoxesInRaySectors( GameVector origin, GameVector target ) {
			Dictionary< Box, int > boxes = new Dictionary< Box, int >();
		
			foreach( Sector sector in EnumerateSectors( origin, target ) )
				foreach( Box box in data.GetSectorBoxes( sector ) )
					boxes[ box ] = 1;
			
			return boxes.Keys;
		}

		public IEnumerable< Box > GetSectorBoxes( Sector sector ) {
			foreach( Box box in data.GetSectorBoxes( sector ) )
				yield return box;
		}
		
		
		// TODO: refatorar, olhar 
		/// <summary>
		/// Gets the ray sectors.
		/// </summary>
		/// <returns>
		/// The ray sectors.
		/// </returns>
		/// <param name='origin'>
		/// Origin.
		/// </param>
		/// <param name='target'>
		/// Target.
		/// </param>
		public IEnumerable< Sector > GetRaySectors( GameVector origin, GameVector target ) {
			Dictionary< Sector, int > sectors = new Dictionary< Sector, int >();
			
			GameVector rightOrigin, rightTarget, leftOrigin, leftTarget;
			CollisionManager.GenerateSideRays( origin, target, Common.LEVEL_SECTOR_WIDTH * 2.0f, out rightOrigin, out rightTarget, out leftOrigin, out leftTarget );
			
			foreach ( int sector in EnumerateSectors( origin, target ) )
				sectors[ sector ] = 0;
			foreach ( int sector in EnumerateSectors( rightOrigin, rightTarget ) )
				sectors[ sector ] = 0;
			foreach ( int sector in EnumerateSectors( leftOrigin, leftTarget ) )
				sectors[ sector ] = 0;
			
			foreach( Sector sector in sectors.Keys ) {
				yield return sector;
			}
		}
		
		public GameTransform GetSectorTransform( int i, int j, int k ) {
			GameTransform sectorTransform = new GameTransform();
			sectorTransform.position.x = GetISectorCenter( i );
			sectorTransform.position.y = GetJSectorCenter( j );
			sectorTransform.position.z = GetKSectorCenter( k );
			sectorTransform.scale.x = Common.LEVEL_SECTOR_WIDTH;
			sectorTransform.scale.y = Common.LEVEL_LAYER_HEIGHT;
			sectorTransform.scale.z = Common.LEVEL_SECTOR_WIDTH;
			return sectorTransform;
		}
		
		public GameTransform GetSectorTransform( Sector sector ) {
			return GetSectorTransform( sector.i, sector.j, sector.k );
		}
		
		
		/// <summary>
		/// Obtém o setor presente na coordenada (i, j, k ).
		/// </summary>
		/// <returns>
		/// Setor da coordenada (i, j, k), ou Vector.NULL caso a coordenada seja inválida ou não haja setor com esta coordenada.
		/// </returns>
		public Sector GetSector( int i, int j, int k ) {
			return data.GetSector( i, j, k );
		}
		
		
		protected void CalculateBoundingBox( List< Box > boxes ) {
			data.minPoint = new GameVector( float.MaxValue, float.MaxValue, float.MaxValue );
			data.maxPoint = new GameVector( float.MinValue, float.MinValue, float.MinValue );
		}
		
		
		/// <summary>
		/// Calcula os limites da fase em setores.
		/// </summary>
		protected void CalculateSectorLimits() {	
			data.CalculateLimits();
		}
		
		
		/// <summary>
		/// Gets the box sectors.
		/// </summary>
		/// <returns>
		/// The box sectors.
		/// </returns>
		/// <param name='box'>
		/// Box.
		/// </param>
		public IEnumerable< Sector > GetBoxSectors( Box box ) {
			return data.GetBoxSectors( box );
		}
		
		
		/// <summary>
		/// Gets the boxes from game vector sector.
		/// </summary>
		/// <returns>
		/// The boxes from game vector sector.
		/// </returns>
		/// <param name='point'>
		/// Point.
		/// </param>
		public IEnumerable< Box > GetBoxesFromGameVectorSector( GameVector point ) {
			Sector sector = GetSectorFromGameVector( point );
			
			foreach( Box box in data.GetSectorBoxes( sector ) ) {
				yield return box;
			}
		}
		
		
		/// <summary>
		/// Obtém o setor referente ao GameVector <c>vector</c>.
		/// </summary>
		public Sector GetSectorFromGameVector( GameVector vector ) {
			return GetSectorFromGameVector( vector.x, vector.y, vector.z );
		}
		
		
		/// <summary>
		/// Gets the sector from game vector.
		/// </summary>
		/// <returns>
		/// The sector from game vector.
		/// </returns>
		/// <param name='vX'>
		/// A posição x no mundo.
		/// </param>
		/// <param name='vY'>
		/// A posição y no mundo.
		/// </param>
		/// <param name='vZ'>
		/// A posição z no mundo.
		/// </param>
		public Sector GetSectorFromGameVector( float vX, float vY, float vZ ) {
			return GetSector( 
				( int ) ( ( vX - data.minPoint.x ) / Common.LEVEL_SECTOR_WIDTH ), 
			    ( int ) ( ( vY - data.minPoint.y ) / Common.LEVEL_LAYER_HEIGHT ),
			    ( int ) ( ( vZ - data.minPoint.z ) / Common.LEVEL_SECTOR_WIDTH ) 
			);
		}
		
		
		// TODO PETER
//		public GameObject CreateCubeFromSector( Sector sector, string name ) {
//			return CreateCubeFromSector( sector, name, 0x0000ffff );
//		}
//		
		
		/// <summary>
		/// Creates the cube from sector.
		/// </summary>
		/// <param name='sector'>
		/// Sector.
		/// </param>
		
		// TODO PETER
		//		public GameObject CreateCubeFromSector( Sector sector, string name, uint color ) {
//			return CreateCubeFromSector( sector.i, sector.j, sector.k, name, color );
//		}
		
		
		/// <summary>
		/// Creates the cube from sector.
		/// </summary>
		/// <param name='i'>
		/// I.
		/// </param>
		/// <param name='j'>
		/// J.
		/// </param>
		/// <param name='k'>
		/// K.
		/// </param>
		/// <param name='name'>
		/// Name.
		/// </param>
		// TODO PETER
//		public GameObject CreateCubeFromSector( int i, int j, int k, string name, uint color ) {
//			GameObject cube = GameObject.CreatePrimitive( PrimitiveType.Cube );
//			GameTransform sectorTransform = GetSectorTransform( i, j, k );
//			
//			cube.transform.parent = ControllerUtils.GetHolder( "<sector_cubes>" ).transform;
//			cube.transform.position = ControllerUtils.CreateVector3( sectorTransform.position ); 
//			cube.transform.localScale = ControllerUtils.CreateVector3( sectorTransform.scale );
//			cube.renderer.material.shader = Shader.Find( "Transparent/Diffuse" );
//			cube.renderer.material.color = ControllerUtils.GetColor( color );
//
//			cube.name = string.Format( "{0} Sector({1},{2},{3})", name, i, j, k );
//			
//			return cube;
//		}
		
		
		public GameVector GetSectorCenter( int i, int j, int k ) {
			return new GameVector( GetISectorCenter( i ), GetJSectorCenter( j ), GetKSectorCenter( k ) );
		}
		
		
		protected float GetISectorCenter( int i ) {
			return data.minPoint.x + ( i * Common.LEVEL_SECTOR_WIDTH ) + ( Common.LEVEL_SECTOR_WIDTH * 0.5f );
		}
		
		
		protected float GetJSectorCenter( int j ) {
			return data.minPoint.y + ( j * Common.LEVEL_LAYER_HEIGHT) + ( Common.LEVEL_LAYER_HEIGHT * 0.5f );
		}
		
		
		protected float GetKSectorCenter( int k ) {
			return data.minPoint.z + ( k * Common.LEVEL_SECTOR_WIDTH) + ( Common.LEVEL_SECTOR_WIDTH * 0.5f );
		}
		
		
		public IEnumerable< RayBoxInterceptionReply > GetBoxInterceptions( Sector sector, GameVector origin, GameVector target ) {
			foreach( Box box in data.GetSectorBoxes( sector ) ) {
				GameTransform t = box.GetGameTransform();
				
				RayBoxInterceptionReply reply = box.Intercept( origin, target );
				if ( reply.entry != null ) {
					yield return reply;
				}
			}
		}
		
		
		/// <summary>
		/// Enumera os setores percorridos por uma parábola descrita pela ação da gravidade no vetor direction, a partir
		/// de origin.
		/// </summary>
		public IEnumerable< int > GetParabolaSectors( GameVector origin, GameVector direction ) {
			Parabola p = new Parabola( origin, direction );
			
			float finalT = -1;
			foreach ( float f in p.GetTimes( GetMinPoint().y ) )
				finalT = Math.Max( f, finalT );
			
			if ( finalT >= 0 ) {
				GameVector pixel = new GameVector( origin );
				
				int dx = ( int ) Math.Round( direction.x * finalT );
				int dz = ( int ) Math.Round( direction.z * finalT );
				
				if ( dx == 0 && dz == 0 ) {
					// arremesso vertical
//					int yMax = ( int ) p.yMax;
//					
//					for ( int y = ( int ) origin.y; y <= yMax; ++y )
//						yield return GetSectorFromGameVector( pixel.x, y, pixel.z );
//					
//					// descida
//					for ( int y = yMax - 1; y >= origin.y; --y )
//						yield return GetSectorFromGameVector( pixel.x, y, pixel.z );
				} else {
					int x_inc = ( direction.x < 0 ) ? -1 : 1;
					int l = Math.Abs( dx );
					int dx2 = l << 1;
					
					int z_inc = ( direction.z < 0 ) ? -1 : 1;
					int n = Math.Abs( dz );
					int dz2 = n << 1;
					
					bool traverseX = l >= n;
					int iteractions = Math.Max( l, n );
					
					int err = traverseX ? dz2 - l : dx2 - n;
					float y0 = origin.y;
					
					for ( int i = 0; i <= iteractions; ++i ) {
						float t = ( i + 1 ) * finalT / iteractions;
						float y1 = p.GetYAt( t );
						
						int diff = ( int ) ( y1 - y0 );
						int diffAbs = Math.Abs( diff );
						
						yield return GetSectorFromGameVector( pixel.x, ( int ) y0, pixel.z );
						
						if ( diffAbs > 0 ) {
							int y_inc = ( diff > 0 ? 1 : -1 );
							for ( int j = 0; j < diffAbs; ++j, y0 += y_inc ) {
								yield return GetSectorFromGameVector( pixel.x, (int) ( y0 + y_inc ), pixel.z );
							}
						}
						
						y0 = y1;
						if ( traverseX ) {
							if ( err > 0 ) {
								pixel.z += z_inc;
								err -= dx2;
							}
							err += dz2;
							pixel.x += x_inc;
						} else {
							if ( err > 0 ) {
								pixel.x += x_inc;
								err -= dz2;
							}
							err += dx2;
							pixel.z += z_inc;
						}
					}
				}
			}
		}
		
		
		/// <summary>
		/// Retorna a lista de setores percorridos por uma reta entre os pontos p1 e p2, utilizando o algoritmo de
		/// Bresenham para discretização.
		/// </summary>
		public List< Sector > GetSectors( GameVector p1, GameVector p2 ) {
			// TODO usar dictionary aqui e depois converter, para evitar repetições???
			Dictionary< Sector, int > sectors = new Dictionary< Sector, int >();
			foreach ( Sector s in EnumerateSectors( p1, p2 ) )
				sectors[ s ] = 0;
			
			return new List< Sector >( sectors.Keys );
		}
		
		
		/// <summary>
		/// Enumera os setores percorridos pelo segmento de reta definido por p1->p2.
		/// </summary>
		/// <returns>
		/// The sectors.
		/// </returns>
		/// <param name='p1'>
		/// P1.
		/// </param>
		/// <param name='p2'>
		/// P2.
		/// </param>
		public IEnumerable< Sector > EnumerateSectors( GameVector p1, GameVector p2 ) {
			// TODO evitar varrer áreas fora dos limites da fase
			GameVector diff = p2 - p1;
			int iterations = Math.Max( Math.Abs( ( int ) diff.x ), Math.Max( Math.Abs( ( int ) diff.y ), Math.Abs( ( int ) diff.z ) ) );
			
			GameVector inc = diff / iterations;
			
			GameVector pixel = new GameVector( p1 );
			for ( int i = 0; i <= iterations; ++i ) {
				Sector s = GetSectorFromGameVector( pixel );
				if ( s.IsValid() )
					yield return s;
	   
				pixel += inc;
	        }
		}
		
		
		/// <summary>
		/// Enumera os setores preenchidos por uma ViewArea.
		/// </summary>
		public IEnumerable< Sector > Get3DSectorsFromViewArea( ViewArea v ) {
//			v.Draw();
			
//			float t = Time.realtimeSinceStartup;
			float yDiffMax = Common.CharacterInfo.CHARACTER_V_VIEW_LIMIT;
			
			Dictionary< int, float[] > values = new Dictionary< int, float[] >();
			
			AddRanges( v.GetCenter(), v.GetEndPoint1(), values );
			AddRanges( v.GetCenter(), v.GetEndPoint2(), values );
			AddRanges( v.GetEndPoint1(), v.GetEndPoint2(), values );
			
			GameVector axis = ( ( v.GetEndPoint1() + v.GetEndPoint2() ) * 0.5f ) - v.GetCenter();
			float axisNorm = axis.Norm();
			axis.Normalize();
			
			float y0 = v.GetCenter().y - ( yDiffMax * 0.5f );
			float y1 = y0 + yDiffMax;
			GameVector temp = new GameVector();
			
			foreach ( int i in values.Keys ) {
				float x = i * Common.LEVEL_SECTOR_WIDTH;
				float mx = Math.Abs( x - v.GetCenter().x );
				
				float[] minMax = values[ i ];
				for ( float z = minMax[ 0 ]; z <= minMax[ 1 ]; z += Common.LEVEL_SECTOR_WIDTH ) {
					if ( mx + Math.Abs( z - v.GetCenter().z ) >= Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT )
						continue;
					
					for ( float y = y0; y < y1; y += Common.LEVEL_LAYER_HEIGHT ) {
						Sector s = GetSectorFromGameVector( x, y, z );
						if ( s.IsValid() ) {
							temp.Set( x, y, z );
							if ( IsInsideCone( temp, v.GetCenter(), axis, axisNorm, 30 ) )
								yield return s;
						}
					}
				}
			}
			
//			Debugger.Log( "T: " + ( Time.realtimeSinceStartup - t ) );
		}		
		
		
		/// <summary>
		/// Método auxiliar para Get3DSectorsFromViewArea, que indica se um ponto está dentro do volume do cone de visão.
		/// Diversas otimizações e simplificações foram feitas, e seu uso não é garantido em outro contexto pois 
		/// algumas condições foram assumidas.
		/// </summary>
		private bool IsInsideCone( GameVector point, GameVector apex, GameVector axisUnitary, float axisNorm, float angle ) {
			GameVector positionFromApex = point - apex;
			
			// ponto está no ápice do cone
			if ( positionFromApex.IsZero() )
				return true;
			
			float beta = positionFromApex.AngleBetween( axisUnitary );
			
			if ( NanoMath.LessEquals( beta, angle ) ) {
				float factor = (float) Math.Cos( beta * ( Math.PI / 180.0f ) );
				float positionFromApexNorm = positionFromApex.Norm();
				float component = factor * positionFromApexNorm;
				
				return ( NanoMath.LessEquals( component, axisNorm ) );
			}
			
			return false;
		}
		
		
		/// <summary>
		/// Enumera os setores preenchidos por uma ViewArea.
		/// </summary>
		public IEnumerable< Sector > GetSectorsFromViewArea( ViewArea v ) {
			Dictionary< int, float[] > values = new Dictionary< int, float[] >();
			
			AddRanges( v.GetCenter(), v.GetEndPoint1(), values );
			AddRanges( v.GetCenter(), v.GetEndPoint2(), values );
			AddRanges( v.GetEndPoint1(), v.GetEndPoint2(), values );
			
			float y = v.GetEndPoint1().y;
			
			foreach ( int i in values.Keys ) {
				float x = i * Common.LEVEL_SECTOR_WIDTH;
				
				float[] minMax = values[ i ];
				for ( float z = minMax[ 0 ]; z <= minMax[ 1 ]; z += Common.LEVEL_SECTOR_WIDTH ) {
					Sector s = GetSectorFromGameVector( x, y, z );
					if ( s.IsValid() )
						yield return s;
				}
			}
		}
				
		
		public List< Sector > Get3DSectorsFromViewAreaSorted( ViewArea v ) {
			SortedList< int, Sector > sectors = new SortedList< int, Sector >();
			
			Dictionary< int, int > distCount = new Dictionary< int, int >();
			
			Sector center = GetSectorFromGameVector( v.GetCenter() );
			foreach ( Sector s in Get3DSectorsFromViewArea( v ) ) {
				int dist = ( Math.Abs( s.i - center.i ) + Math.Abs( s.j - center.j ) + Math.Abs( s.k - center.k ) );
				
				if ( distCount.ContainsKey( dist ) )
					distCount[ dist ]++;
				else
					distCount[ dist ] = 0;
				
				try {
					sectors.Add( dist * 50 + distCount[ dist ], s );
				} catch ( Exception ) {
				}
			}
			
			return new List< Sector >( sectors.Values );
		}


		public List< Sector > GetSectorsFromViewAreaSorted( ViewArea v ) {
			Dictionary< int, float[] > values = new Dictionary< int, float[] >();
			SortedList< int, Sector > sectors = new SortedList<int, Sector>();
									
			AddRanges( v.GetCenter(), v.GetEndPoint1(), values );
			AddRanges( v.GetCenter(), v.GetEndPoint2(), values );
			AddRanges( v.GetEndPoint1(), v.GetEndPoint2(), values );
			
			float y = v.GetEndPoint1().y;
			int[] distCount = new int[140];
			Sector center = GetSectorFromGameVector( v.GetCenter() );
			
			foreach ( int i in values.Keys ) {
				float x = i * Common.LEVEL_SECTOR_WIDTH;
				
				float[] minMax = values[ i ];
				for ( float z = minMax[ 0 ]; z <= minMax[ 1 ]; z += Common.LEVEL_SECTOR_WIDTH ) {
					Sector s = GetSectorFromGameVector( x, y, z );
					if ( s.IsValid() ) {
						int dist = ( Math.Abs( s.i - center.i ) + Math.Abs( s.k - center.k ) );
						if ( dist < distCount.Length ) {
							
							distCount[ dist ]++;
							sectors.Add( dist * 20 + distCount[dist], s );
						}
					}
				}
			}
			
			return new List< Sector >( sectors.Values );
		}
		
		/// <summary>
		/// Método auxiliar de GetSectorsFromViewArea( ViewArea )
		/// </summary>
		private void AddRanges( GameVector p1, GameVector p2, Dictionary< int, float[] > values ) {
			foreach ( GameVector v in TraverseLine( p1, p2 ) ) {
				int index = ( int ) Math.Round( v.x / Common.LEVEL_SECTOR_WIDTH );
				if ( !values.ContainsKey( index ) )
					values[ index ] = new float[] { float.MaxValue, float.MinValue };
				
				if ( v.z > values[ index ][ 1 ] )
					values[ index ][ 1 ] = v.z;
				
				if ( v.z < values[ index ][ 0 ] )
					values[ index ][ 0 ] = v.z;
			}
		}
		
		
		/// <summary>
		/// Método auxiliar de GetSectorsFromViewArea( ViewArea )
		/// </summary>
		private IEnumerable< GameVector > TraverseLine( GameVector p1, GameVector p2 ) {
			// TODO evitar varrer áreas fora dos limites da fase
			GameVector diff = p2 - p1;
			int iterations = Math.Max( Math.Abs( ( int ) diff.x ), Math.Abs( ( int ) diff.z ) );
			
			GameVector inc = diff / iterations;
			
			GameVector pixel = new GameVector( p1 );
			for ( int i = 0; i <= iterations; ++i ) {
				yield return pixel;
				pixel += inc;
	        }
		}		
	}
}