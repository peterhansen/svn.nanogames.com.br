using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class Shotgun : FireArm {
		
		/// <summary>
		/// Porcentagem do ângulo de espalhamento vertical em relação ao horizontal.
		/// </summary>
		public const float VERTICAL_ANGLE_PERCENT = 0.6f;
		
		private float spreadAngle = 30;
		
		private int particles = 10;

				
		public Shotgun() : base( FireArmType.SHOTGUN ) {
			Size = ItemSize.Medium;
			Power = 120;
		}

		
		/// <summary>
		/// Quantidade de partículas expelidas pela arma.
		/// </summary>
		[ ToolTip( "Quantidade de partículas expelidas pela arma." ) ]
		public int Particles {
			get { return this.particles; }
			set { particles = value; }
		}
		
		
		/// <summary>
		/// Ângulo máximo de espalhamento das partículas da arma.
		/// </summary>
		[ ToolTip( "Ângulo máximo de espalhamento das partículas da arma." ) ]
		public float SpreadAngle {
			get { return this.spreadAngle; }
			set { spreadAngle = value; }
		}		
		
		
		public override float GetUseModeAPCost( ItemUseMode useMode ) {
			// TODO: parametrizar isso aqui
			return 4.0f;
		}
		
		
		public override IEnumerable< ItemUseMode > GetAvailableUseModes() {
			yield return ItemUseMode.REGULAR;
		}
		
		
		public override AttackReply Attack( GameVector origin, GameVector target, GameVector deviation, ItemUseMode useMode ) {
			AttackReply reply = new AttackReply();
			if( ammo.RemainingShots == 0 ) {
				reply.state = AttackState.OUT_OF_AMMO;
				return reply;
			}
			
			target += GameVector.GetRandom( -deviation, deviation );
			
			GameVector direction = target - origin;
			direction.SetScale( Range );
			
			// posição máxima na direção do alvo que a shotgun é capaz de alcançar
			GameVector rangeTarget = origin + direction;
			
			float powerPerParticle = Power / Particles;
			
			float totalWidth = Range * ( float ) Math.Sin( spreadAngle * Math.PI / 180.0f );
			float slotWidthRange = ( totalWidth / Particles );
			float slotHeightRange = slotWidthRange * VERTICAL_ANGLE_PERCENT;
			
			GameVector up = GetUpVector( direction );
			GameVector right = GetRightVector( direction );
			
			GameVector minRandomUp = new GameVector( up );
			minRandomUp.SetScale( -slotHeightRange * 0.8f );
			GameVector maxRandomUp = new GameVector( up );
			maxRandomUp.SetScale( slotHeightRange * 1.2f );
			
			GameVector randomH = new GameVector( right );
			randomH.SetScale( slotWidthRange );
			
			GameVector temp = -right;
			temp.SetScale( totalWidth * 0.5f );
			
			GameVector start = rangeTarget - temp;
			temp.SetScale( slotWidthRange );
			start += temp;
			
			// usa um contador separado para que cada faixa tenha 2 tiros, gerando uma 
			int i = 0; 
			while ( reply.shots.Count < Particles ) {
				GameVector diff = GameVector.GetRandom( minRandomUp, maxRandomUp ) + GameVector.GetRandom( -randomH, randomH );
				
				float mult = i * slotWidthRange * 2;
				GameVector t = start + diff + ( temp * mult );
				diff = t - origin;
				diff.SetScale( Range );
				t = origin + diff;
				
				ShotInfo info = new ShotInfo( origin, t, powerPerParticle );
				
				reply.shots.Add( info );
				
				if ( ( reply.shots.Count % 2 ) == 0 ) {
					++i;
				}
			}
			
			ammo.RemainingShots--;
			return reply;
		}
		
		
		protected GameVector GetRightVector( GameVector direction ) {
			GameVector pathUp = GameVector.YAxis - GameVector.YAxis.ProjectOn( direction );
			
			return direction.Cross( pathUp ).Unitary();
		}
		
		
		protected GameVector GetUpVector( GameVector direction ) {
			GameVector axis = Math.Abs( direction.x ) >= Math.Abs( direction.z ) ? new GameVector( 0, 0, -direction.z ) : new GameVector( -direction.x, 0, 0 );
			axis.Normalize();
			
			GameVector pathUp = axis - axis.ProjectOn( direction );
			
			return direction.Cross( pathUp ).Unitary();
		}
	}
}

