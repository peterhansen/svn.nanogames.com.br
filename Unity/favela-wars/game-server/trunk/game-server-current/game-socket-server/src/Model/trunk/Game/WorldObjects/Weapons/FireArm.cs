using System;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public abstract class FireArm : Weapon {
		
		protected float reloadTime = 10;
		
		protected int rateOfFire = 10;
		
		protected Ammo ammo;
		
		protected FireArmType firearmType;
		
		
		protected FireArm( FireArmType type ) : base() {
			this.firearmType = type;
		}
		
		
		/// <summary>
		/// Tipo da arma.
		/// </summary>
		public FireArmType FireArmType {
			get { return firearmType; }
		}
		
		
		/// <summary>
		/// Tempo de carregamento da arma, em segundos.
		/// </summary>
		[ ToolTip( "Tempo de carregamento da arma, em segundos." ) ]
		public float ReloadTime {
			get { return reloadTime; }
			set { reloadTime = value; }
		}
		
		
		/// <summary>
		/// Taxa de tiros da arma, em tiros por minuto.
		/// </summary>
		[ ToolTip( "Taxa de tiros da arma, em tiros por minuto." ) ]
		public int RateOfFire {
			get { return rateOfFire; }
			set { rateOfFire = value; }
		}
		
		
		/// <summary>
		/// Munição atual da arma.
		/// </summary>
		[ ToolTip( "Munição atual da arma." ) ]
		public Ammo Ammo {
			get { return ammo; }
			set { ammo = value; }
		}
		
		
		/// <summary>
		/// O percentual da munição atual da arma carregada.
		/// </summary>
		public float AmmoPercentage {
			get { return ammo == null? 0.0f: ( (float) ammo.RemainingShots / (float) ammo.MaxShots ); }
		}
		
		
		public bool CanShoot() {
			return ammo != null && !ammo.IsEmpty();
		}
		
		
		public void Reload() {
			// TODO recarregar arma
			ammo.RemainingShots = ammo.MaxShots;
		}
		
		
		public override GameAction GetPossibleActionsList() {
			GameAction toReturn = new GameAction( ActionType.SELECT_WEAPON );
			foreach( GameAction baseGameAction in base.GetPossibleActionsList().GetChildren() )
				toReturn.AddChild( baseGameAction );
			
			// ação de tiro
			AttackGameAction shootAction = new AttackGameAction( EquipID, ItemUseMode.REGULAR );
			if ( ammo == null || ammo.IsEmpty() )
				shootAction.IsAvailable = false;
				// TODO: onde colocar essa informação agora?
				//AttackGameAction shootAction = new AttackGameAction( "Tiro simples(" + ammo.RemainingShots + ")", EquipID, ItemUseMode.REGULAR );
			toReturn.AddChild( shootAction );
			
			return toReturn;
		}
		
		
//		/// <summary>
//		/// Calcula o gasto de AP para um determinado modo de ataque.
//		/// </summary>
//		public override float GetAPCost( UseMode useMode ) {
//			return rateOfFire / RATE_OF_FIRE_TO_AP_DIVIDER;
//		}
	}
}

