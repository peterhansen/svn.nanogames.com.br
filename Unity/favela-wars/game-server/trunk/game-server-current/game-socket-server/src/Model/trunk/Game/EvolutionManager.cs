using System;
using System.Collections.Generic;


using Utils;


namespace GameModel {
	
	public class EvolutionManager {
		
		protected Mission mission;
		
		
		public EvolutionManager( Mission mission ) {
			this.mission = mission;
		}
		
		public void OnCharacterShoot( Character character , bool hit = false ){
			character.experience.AddExperienceForShooting(hit);
		}
		
		public void OnCharacterThrowObject( Character character , bool hit  = false){
			character.experience.AddExperienceForThrowObject(hit);
		}
	
		
		public void OnCharacterKilled( Character killercharacter , Character deadcharacter ) {
			if( killercharacter != deadcharacter )
				killercharacter.experience.AddExperienceForKilling( deadcharacter );
		}
		
		public void OnMissionStart(){
			//Something to do here?
		}
		
		
		public void OnMissionEnded() {
			foreach( Character character in mission.GetCharacters() ) {
				int tpGained = character.experience.GetTPGained( character.IsAlive() );
				
				Debugger.Log( "Char: " + character.GetWorldID() + " just got " + tpGained + " Training Points" );
				//If the char is dead, it gives an error
				//character.Data.AddTrainingPoints( tpGained );
			}
		}
	}
}

