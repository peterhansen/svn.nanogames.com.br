using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public abstract class Weapon : EquippableItem {
		
		private bool twoHanded = false;
		
		private float power = 30;
		
		private float range = 30;
		
		protected float precision = 50;
		
		public Weapon() : base() {
		}
		
		/// <summary>
		/// Indica se a arma exige as 2 mãos para seu uso.
		/// </summary>
		[ ToolTip( "Indica se a arma exige as 2 mãos para seu uso." ) ]
		public bool TwoHanded {
			get { return this.twoHanded; }
			set { twoHanded = value; }
		}		
		
		
		/// <summary>
		/// Alcance da arma, em metros.
		/// </summary>
		[ ToolTip( "Alcance da arma, em metros." ) ]
		public float Range {
			get { return range; }
			set { range = value; }
		}
		
		
		/// <summary>
		/// Potência da arma.
		/// </summary>
		[ ToolTip( "Potência da arma." ) ]
		public float Power {
			get { return power; }
			set { power = value; }
		}
		
		
		/// <summary>
		/// Nível de precisão da arma.
		/// </summary>
		[ ToolTip( "Nível de precisão da arma." ) ]
		public float Precision {
			get { return precision; }
			set { precision = value; }
		}		
		
		//public abstract float GetAPCost( UseMode mode );
		
		public abstract AttackReply Attack( GameVector origin, GameVector target, GameVector deviation, ItemUseMode useMode );
	}
	
	public enum AttackState {
		SUCCESS,
		OUT_OF_AMMO
	}
	
	[ Serializable ]
	public class AttackReply {
		public AttackState state = AttackState.SUCCESS;
		public List< AttackInfo > shots = new List< AttackInfo >();
	}
}