using System.Collections;
using System.Collections.Generic;


namespace GameModel {	
	
	
	public class Troop {
	
		protected Dictionary< int, Soldier > soldiers = new Dictionary< int, Soldier >();
	
		protected int playerID;
		
		
		public Troop( int playerID ) {
			this.playerID = playerID;
		}
		
		
		public int GetPlayerID() {
			return playerID;
		}
		
		
		public void AddSoldier( Soldier s ) {
			soldiers.Add( s.GetWorldID(), s );
		}
			
		
		public Soldier GetSoldier( int worldID ) {
			return soldiers[ worldID ];
		}
	
	}
	
}
	
