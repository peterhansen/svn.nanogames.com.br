using System;

using Utils;
using GameCommunication;


namespace GameModel {
	
	[ Serializable ]
	public class DrugDealer : Soldier {
		
		#if UNITY_EDITOR
		public DrugDealer() {
			data.Faction = Faction.CRIMINAL;			
		}
		#endif
		
		public DrugDealer( int gameObjectID ) : base( gameObjectID, "ff0000ff" ) {
			Data.Faction = Faction.CRIMINAL;
		}
		
		
		public override GameAction GetPossibleActionsList () {
			GameAction toReturn = base.GetPossibleActionsList();
			return toReturn;
		}
	}
	
}