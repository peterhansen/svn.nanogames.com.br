using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class Medikit : ConsumableItem {
		
		protected float hitPoints = 50;
		
				
		public Medikit() : base() {
			Range = Common.LEVEL_SECTOR_DIAGONAL;
		}
		
		
		/// <summary>
		/// Pontos de energia recuperados ao se utilizar o Medikit.
		/// </summary>
		[ ToolTip( "Pontos de energia recuperados ao se utilizar o Medikit." ) ]
		public float HitPoints {
			get { return hitPoints; }
			set { hitPoints = value; }
		}
		
		
		protected override void Consume( GameVector target, Character character ) {
			Debugger.Log( string.Format( "{0} recovered {1} HP with a medikit.", character.Data.Name, hitPoints ) );
			character.Data.HitPoints.Value += hitPoints;
		}
		
		
		public override float GetUseModeAPCost( ItemUseMode useMode ) {
			return 10.0f;
		}
		
		
		public override IEnumerable< ItemUseMode > GetAvailableUseModes() {
			yield return ItemUseMode.REGULAR;
			yield break;
		}
	}
	
}

