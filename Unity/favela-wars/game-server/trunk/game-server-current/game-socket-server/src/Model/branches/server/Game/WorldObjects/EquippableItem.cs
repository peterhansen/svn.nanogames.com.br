using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class EquippableItem : Item {
		
		protected List< CharacterPart > validParts = new List< CharacterPart >();
		
		public EquippableItem() : base( 0, "" ) { // TODO
		}
		
		/// <summary>
		/// Enumera as partes do corpo de um personagem que este item ocupa ao ser equipado.
		/// </summary>
		public IEnumerable< CharacterPart > UsedParts() {
			foreach ( CharacterPart p in validParts )
				yield return p;
		}
		
	}
	
}

