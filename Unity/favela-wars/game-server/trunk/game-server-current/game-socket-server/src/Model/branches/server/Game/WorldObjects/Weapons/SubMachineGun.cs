using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class SubMachineGun : FireArm {
		
		protected int burstShots = 3;
		
		protected float burstShotInterval = 0.2f;
		
				
		public SubMachineGun() : base( FireArmType.SMG ) {
			Size = ItemSize.Medium;
			Power = 50;
		}

		
		/// <summary>
		/// Quantidade de tiros de uma rajada.
		/// </summary>
		[ ToolTip( "Quantidade de tiros de uma rajada." ) ]
		public int BurstShots {
			get { return this.burstShots; }
			set { burstShots = value; }
		}
		
		
		/// <summary>
		/// Intervalo em segundos entre os disparos de uma rajada.
		/// </summary>
		[ ToolTip( "Intervalo em segundos entre os disparos de uma rajada." ) ]
		public float BurstShotInterval {
			get { return this.burstShotInterval; }
			set { burstShotInterval = value; }
		}
		
				
		public override float GetUseModeAPCost( ItemUseMode useMode ) {
			// TODO: parametrizar
			switch( useMode ) {
			case ItemUseMode.REGULAR:
				return 4.0f;
			
			case ItemUseMode.BURST:
				return 10.0f;
			}
			
			throw new Item.UseModeNotAvailable( useMode );
		}
		
		
		public override IEnumerable< ItemUseMode > GetAvailableUseModes() {
			yield return ItemUseMode.REGULAR;
			yield return ItemUseMode.BURST;
		}
		
		
		public override AttackReply Attack( GameVector origin, GameVector target, GameVector deviation, ItemUseMode useMode ) {
			AttackReply reply = new AttackReply();
			if( ammo.RemainingShots == 0 ) {
				reply.state = AttackState.OUT_OF_AMMO;
				return reply;
			}
			
			int totalShots = Math.Min( ( useMode == ItemUseMode.BURST ) ? BurstShots : 1, ammo.RemainingShots );
			float time = 0;
			
			for ( int i = 0; i < totalShots; ++i ) {
				ShotInfo info = new ShotInfo( origin, target + GameVector.GetRandom( -deviation, deviation ), Power );
				info.TimeSinceFire = time;
				time += BurstShotInterval;
				
				reply.shots.Add( info );
			}
			
			ammo.RemainingShots -= totalShots;
			
			return reply;
		}
		
		
		public override GameAction GetPossibleActionsList() {
			GameAction toReturn = base.GetPossibleActionsList();
			
			if ( ammo != null && !ammo.IsEmpty() ) {
				// TODO: onde colocar essa info?
				//AttackGameAction shootAction = new AttackGameAction( "Tiro em rajada(" + ammo.RemainingShots + ")", EquipID, ItemUseMode.BURST );
				AttackGameAction shootAction = new AttackGameAction( EquipID, ItemUseMode.BURST );
				toReturn.AddChild( shootAction );
			}
			
			return toReturn;
		}		
		
	}
	
}

