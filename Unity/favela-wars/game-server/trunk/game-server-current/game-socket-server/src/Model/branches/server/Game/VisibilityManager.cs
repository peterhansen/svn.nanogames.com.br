using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	
	public enum CharacterVisibility {
		NOT_SEEN,
		SEEN_FIRST_THIS_TURN,
		SEEN_AGAIN_THIS_TURN,
		ALWAYS_SEEN
	};
	
	
	public class CharacterVisibilityInfo {
		
		public List< Sector > observedSectors = new List< Sector >();
		
		public List< int > observedCharactersIDs = new List< int >();
		
	}
		
	
	public class VisibilityManager {
		
		public class CharacterVisibilityState {
			
			public CharacterVisibility turnVisibility;
			
			public Dictionary< int, int > observers = new Dictionary< int, int >();
			
			public CharacterVisibilityState() {
				turnVisibility = CharacterVisibility.NOT_SEEN;
			}
			
			public CharacterVisibilityState( CharacterVisibility turnVisibility ) {
				this.turnVisibility = turnVisibility;
			}
			
		}
		
		
		protected class FactionVisibilityInfo {
			
			public Dictionary< int, CharacterVisibilityState > characters = new Dictionary< int, CharacterVisibilityState >();
			
			public Dictionary< Sector, int > sectorsKnown = new Dictionary< Sector, int >();
			
		}
		
		
		protected class SectorVisibilityInfo {
			
			public Dictionary< Character, int > observers = new Dictionary< Character, int >();
			
		}
		
		protected Mission mission;
		
		protected CollisionManager collisionManager;
		
		protected SectorManager sectorManager;
		
		protected List< Faction > factions = new List< Faction >();
		
		protected Dictionary< Faction, FactionVisibilityInfo > factionVisibility = new Dictionary< Faction, FactionVisibilityInfo >();
		
		protected Dictionary< Character, CharacterVisibilityInfo > characterVisibility = new Dictionary< Character, CharacterVisibilityInfo >();
		
		protected Dictionary< Sector, SectorVisibilityInfo > sectorsVisibilityInfo = new Dictionary< Sector, SectorVisibilityInfo >();
		
		protected Dictionary< Faction, Dictionary< int, bool > > knownCharacters = new Dictionary< Faction, Dictionary< int, bool > >();
		
		
		public VisibilityManager( Mission mission ) {
			this.mission = mission;
			this.collisionManager = mission.GetMap().GetCollisionManager();
			this.sectorManager = collisionManager.GetSectorManager();
			
			foreach( Faction faction in Common.PlayerFactions )
				factions.Add( faction );
			factions.Add( Faction.NEUTRAL );

			foreach( Faction faction in factions ) {
				factionVisibility[ faction ] = new FactionVisibilityInfo();
			}
				
			foreach( Faction faction in factions ) {
				foreach( Character character in mission.GetCharacters( faction ) ) {
					characterVisibility[ character ] = new CharacterVisibilityInfo();
					
					foreach( Faction otherFaction in factions ) {
						factionVisibility[ otherFaction ].characters[ character.GetWorldID() ] = new CharacterVisibilityState(
							otherFaction == character.Data.Faction? CharacterVisibility.ALWAYS_SEEN : CharacterVisibility.NOT_SEEN 
						);
					}
				}
			}
		}
		
		
		/// <summary>
		/// Obtém os dados de visibilidade de um determinado personagem.
		/// </summary>
		public CharacterVisibilityInfo GetCharacterVisibilityInfo( Character c ) {
			return characterVisibility[ c ];
		}

		
		// TODO: aqui criamos uma distincao entra a visibilidade do primeiro turno (a qual retorna como explorados tambem os setores
		// das spawn areas dos personagens) e dos outros turnos. Caso esse efeito seja eliminado, a necessidade desse parâmetro booleano
		// desaperece também.
		public VisibilityChangeData GetVisibilityFor( Faction faction, bool firstTurn ) {
			VisibilityChangeData visibilityChangeData = new VisibilityChangeData( faction );
			
			if ( knownCharacters.ContainsKey( faction ) ) {
			
				foreach ( int id in knownCharacters[ faction ].Keys ) {
						visibilityChangeData.characterList[ id ] = CharacterKnownStatus.SeenPreviously;
						visibilityChangeData.characterList.Alive[ id ] = mission.GetCharacter( id ).IsAlive();
						visibilityChangeData.characterList.Factions[ id ] = mission.GetCharacter( id ).Data.Faction;
				}
			}
			
			
			foreach( KeyValuePair< int, CharacterVisibilityState > kvp in factionVisibility[ faction ].characters ) {
			if( ( kvp.Value.turnVisibility == CharacterVisibility.ALWAYS_SEEN ) || 
				( kvp.Value.turnVisibility == CharacterVisibility.SEEN_FIRST_THIS_TURN ) || 
				( kvp.Value.turnVisibility == CharacterVisibility.SEEN_AGAIN_THIS_TURN ) )
					
//				visibilityChangeData.visibleWorldObjectIds[ kvp.Key ] = 1;
				visibilityChangeData.AddToVisible( kvp.Key );
				visibilityChangeData.characterList.Alive[ kvp.Key ] = ( mission.GetCharacter( kvp.Key ) ).IsAlive();
				visibilityChangeData.characterList.Factions[ kvp.Key ] = ( mission.GetCharacter( kvp.Key ) ).Data.Faction;
			}
			
			foreach( Character character in mission.GetCharacters( faction ) ) {
				// character.GetGameTransform().DebugDraw();
				// TODO: acho que aqui não estamos pegando exatamente os NEWLY visibles... aparentemente estamos pegando todos
				// TODO: newlyVisibleSectors nao é um dicionário, logo pode haver repeticoes aqui...
				visibilityChangeData.newlyVisibleSectors.AddRange( characterVisibility[ character ].observedSectors );
			}
			
			if( firstTurn ) {
				// calcula visibilidade de setores para as spawn areas das faccoes
				foreach( Box spawnAreaBox in mission.GetSpawnAreaBoxes( faction ) ) {
					Box extendedBox = new Box( spawnAreaBox.GetGameTransform() );
					
					// HACK: esse valor é 2.0f
					extendedBox.GetScale().Scale( 1.0f );
					foreach( Sector sector in sectorManager.GetSectorsData().CalculateBoxSectors( extendedBox ) ) {
						factionVisibility[ faction ].sectorsKnown[ sector ] = 1;
						
						// TODO: newlyVisibleSectors nao é um dicionário, logo pode haver repeticoes aqui...
						visibilityChangeData.newlyVisibleSectors.Add( sector );
					}
				}
			}
			
			return visibilityChangeData;
		}
		
		
		/// <summary>
		/// Calcula os setores visíveis para um determinado personagem.
		/// </summary>
		public VisibilityChangeData CalculateVisibilityFor( Character character ) {
			// criando informacao de visibilidade, contendo personagens ja visiveis no turno
			Faction characterFaction = character.Data.Faction;
			VisibilityChangeData visibilityData = new VisibilityChangeData( characterFaction );
			
			ViewArea v = new ViewArea();
			v.SetView( character.GetGameTransform() );
			
			visibilityData.newlyVisibleSectors = sectorManager.Get3DSectorsFromViewAreaSorted( v );
			
			Dictionary< Sector, SectorVisibilityStatus > visibleSectors = new Dictionary< Sector, SectorVisibilityStatus >();
			foreach ( Sector s in visibilityData.newlyVisibleSectors ) {
				visibleSectors[ s ] = SectorVisibilityStatus.VISIBLE;
//				sectorManager.CreateCubeFromSector( s, "original", 0x0000ff44 );
			}
			
			Dictionary< Box, int > verifiedBoxes = new Dictionary< Box, int >();
			
			foreach ( Sector s in visibilityData.newlyVisibleSectors ) {
				if ( visibleSectors[ s ] == SectorVisibilityStatus.NOT_VISIBLE )
					continue;
				
				foreach ( Box b in collisionManager.GetSectorBoxes( s ) ) {
					if ( verifiedBoxes.ContainsKey( b ) ) {
						continue;
					} else {
						verifiedBoxes[ b ] = 0;
					}
					
					// caixas de personagens e do chão não precisam ser verificadas
					if( b.IsCharacterBox() || b is GroundBox ) {
						continue;
					}
					
					b.DrawShadows( v.GetCenter(), visibleSectors );
				}
			}
			
			Sector viewSector = sectorManager.GetSectorFromGameVector( v.GetCenter() );
			visibleSectors[ viewSector ] = SectorVisibilityStatus.VISIBLE;
			
			visibilityData.newlyVisibleSectors = new List< Sector >();
			foreach ( Sector s in visibleSectors.Keys ) {
				if ( visibleSectors[ s ] != SectorVisibilityStatus.NOT_VISIBLE ) {
					visibilityData.newlyVisibleSectors.Add( s );
//					sectorManager.CreateCubeFromSector( s, "diff", 0x00ff0033 );
				}
			}
			
			// removendo setores previamente observados pelo personagem
			foreach( Sector sector in characterVisibility[ character ].observedSectors ) {
				sectorsVisibilityInfo[ sector ].observers.Remove( character );
			}
			
			// atualizando setores vistos pelo personagem
			characterVisibility[ character ].observedSectors = new List<Sector>( visibilityData.newlyVisibleSectors );
			foreach ( Sector sector in characterVisibility[ character ].observedSectors ) {
				if( !sectorsVisibilityInfo.ContainsKey( sector ) )
					sectorsVisibilityInfo[ sector ] = new SectorVisibilityInfo();
				
				sectorsVisibilityInfo[ sector ].observers[ character ] = 1;
			}
			
			// atualizando objetos da cena vistos pelos personagens
			UpdateCharacterVisibility( character, visibilityData );
			
			return visibilityData;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		public void OnBeforeTurnChange( TurnData turnData ) {
			// TODO: ainda nao sei pegar a faccao inimiga
			Faction nextFaction;
			switch( turnData.faction ) {
			case Faction.NEUTRAL:
				nextFaction = Faction.POLICE;
				break;
				
			case Faction.POLICE:
				nextFaction = Faction.CRIMINAL;
				break;
				
			case Faction.CRIMINAL:
				nextFaction = Faction.NEUTRAL;
				break;
				
			default:
				throw new Exception("nikasndiasnd");
			}
			
			FactionVisibilityInfo currentFactionVisibilityInfo = factionVisibility[ nextFaction ];
			
			foreach( Character enemyCharacter in mission.GetCharactersExcept( nextFaction ) ) {
				int enemyId = enemyCharacter.GetWorldID();
				currentFactionVisibilityInfo.characters[ enemyId ] = new CharacterVisibilityState( CharacterVisibility.NOT_SEEN );
				
				foreach( Sector sector in sectorManager.GetBoxSectors( enemyCharacter.GetBox() ) ) {
					if( sectorsVisibilityInfo.ContainsKey( sector ) ) {
						foreach( Character watchingCharacter in sectorsVisibilityInfo[ sector ].observers.Keys ) {
							if( watchingCharacter.Data.Faction == nextFaction ) {
								currentFactionVisibilityInfo.characters[ enemyId ].turnVisibility = CharacterVisibility.SEEN_AGAIN_THIS_TURN;
								currentFactionVisibilityInfo.characters[ enemyId ].observers[ watchingCharacter.GetWorldID() ] = 1;
							}
						}
					}
				}
				
				if( currentFactionVisibilityInfo.characters[ enemyId ].turnVisibility == CharacterVisibility.SEEN_AGAIN_THIS_TURN ) {
					enemyCharacter.GetGameTransform().DebugDraw( 0x00ff00ff );
					foreach( int observerId in currentFactionVisibilityInfo.characters[ enemyId ].observers.Keys ) {
						Debugger.Log( "enemyCharacter " + enemyCharacter.GetWorldID() + "visto por " + observerId );
					}
				} else {
					enemyCharacter.GetGameTransform().DebugDraw( 0xffff00ff );		
				}
			}
		}

		
		public CharacterVisibility GetCharacterTurnVisibility( Faction faction, Character character ) {
			return factionVisibility[ faction ].characters[ character.GetWorldID() ].turnVisibility;	
		}
		
		
		public void SetCharacterTurnVisibility( Faction faction, Character character, CharacterVisibility turnVisibility ) {
			factionVisibility[ faction ].characters[ character.GetWorldID() ].turnVisibility = turnVisibility;
		}
		
		
		public IEnumerable<Character> GetWatchers( Sector sector ) {
			if( !sectorsVisibilityInfo.ContainsKey( sector ) )
				yield break;
				
			foreach( Character character in sectorsVisibilityInfo[ sector ].observers.Keys ) {
				yield return character;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public void SpotCharacter( Character spottingCharacter, Character spottedCharacter ) {
			int spottedCharacterID = spottedCharacter.GetWorldID();
			Dictionary< int, CharacterVisibilityState > spottedCharacters = factionVisibility[ spottingCharacter.Data.Faction ].characters;
			
			if( !spottedCharacters.ContainsKey( spottedCharacterID ) ) {
				spottedCharacters[ spottedCharacterID ] = new CharacterVisibilityState();
			}
			
			CharacterVisibilityState spottedState = spottedCharacters[ spottedCharacterID ];
			spottedState.observers[ spottedCharacterID ] = 1;
			
			if ( !knownCharacters.ContainsKey( spottingCharacter.Data.Faction ) )
				knownCharacters[ spottingCharacter.Data.Faction ] = new Dictionary<int, bool>();
			
			knownCharacters[ spottingCharacter.Data.Faction ][ spottedCharacterID ] = true;
			
			switch( spottedState.turnVisibility ) {
			case CharacterVisibility.NOT_SEEN:				
				spottedState.turnVisibility = CharacterVisibility.SEEN_FIRST_THIS_TURN;
				break;
				
			case CharacterVisibility.SEEN_FIRST_THIS_TURN:
				spottedState.turnVisibility = CharacterVisibility.SEEN_AGAIN_THIS_TURN;
				break;
			}
			
			// vamos, por último, fazer visíveis os setores do personagem visto para a facção do personagem que o viu,
			// a fim de evitar diferenças entre a visibilidade dos setores e dos personagens
			foreach( Sector sector in sectorManager.GetBoxSectors( spottedCharacter.GetBox() ) ) {
				factionVisibility[spottingCharacter.Faction].sectorsKnown[ sector ] = 1;
			}
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		protected void UpdateCharacterVisibility( Character character, VisibilityChangeData vsd ) {
			bool previouslySeen = false;
			Faction characterFaction = character.Data.Faction;
			
			foreach ( Character c in mission.GetCharacters() ) {
				vsd.characterList.Factions[ c.GetWorldID() ] = c.Data.Faction;
				vsd.characterList.Alive[ c.GetWorldID() ] = ( c.Data.HitPoints > 0 );
				
				vsd.setAsInvisible( c.GetWorldID(), previouslySeen );
			}
			
			Dictionary< Box, int > alreadyTestedBoxes = new Dictionary< Box, int >();
			
			foreach ( Sector s in characterVisibility[ character ].observedSectors ) {
				foreach ( Box b in sectorManager.GetSectorBoxes( s ) ) {
					if( alreadyTestedBoxes.ContainsKey(b) )
						continue;
					
					alreadyTestedBoxes[ b ] = 1;
					int boxId = b.GetWorldObjectID();
					
					if( b.IsCharacterBox() ) {
						
						Character spottedCharacter = mission.GetCharacter( boxId );
						if( spottedCharacter.Data.Faction != characterFaction &&
							!vsd.visibleWorldObjectIds.ContainsKey( boxId ) ) {
						
							RaycastFilter filter = new RaycastFilter();
							foreach( Box rayBox in collisionManager.GetSectorManager().GetBoxesInRaySectors( character.GetPosition(), b.GetPosition() ) )
								filter.Boxes.Add( rayBox );
							filter.Boxes.Remove( character.GetBox() );
							filter.Boxes.Remove( b );
							
							if( collisionManager.Raycast( character.GetPosition(), b.GetPosition(), filter ) == RaycastResult.NO_INTERCEPTION ) {
							
								vsd.AddToVisible( boxId );
								SpotCharacter( character, spottedCharacter );
							}
						}
					}
				}
			}
			
			foreach ( Character c in mission.GetCharacters( characterFaction ) ) {	
				vsd.AddToVisible( c.GetWorldID() );
			}
			
			if ( knownCharacters.ContainsKey( characterFaction ) ) {
			
				foreach ( int id in knownCharacters[ characterFaction ].Keys ) {
						vsd.characterList[ id ] = CharacterKnownStatus.SeenPreviously;
						vsd.characterList.Alive[ id ] = mission.GetCharacter( id ).IsAlive();
						vsd.characterList.Factions[ id ] = mission.GetCharacter( id ).Data.Faction;
				}
			}
			
			
			foreach( KeyValuePair< int, CharacterVisibilityState > kvp in factionVisibility[ characterFaction ].characters ) {
				if ( kvp.Value.turnVisibility != CharacterVisibility.NOT_SEEN ) {
					//vsd.AddToVisible( kvp.Key );
					
					CharacterKnownStatus newStatus = CharacterKnownStatus.NotSeen;
					
					switch ( kvp.Value.turnVisibility ) {
						case CharacterVisibility.ALWAYS_SEEN:
						case CharacterVisibility.SEEN_AGAIN_THIS_TURN:
						case CharacterVisibility.SEEN_FIRST_THIS_TURN:
							newStatus = CharacterKnownStatus.CurrentlySeen;
						break;
						
						case CharacterVisibility.NOT_SEEN:
							if ( knownCharacters[ characterFaction ].ContainsKey( kvp.Key ) && knownCharacters[ characterFaction ][ kvp.Key ] ) {
								newStatus = CharacterKnownStatus.SeenPreviously;
							} else {
								newStatus = CharacterKnownStatus.NotSeen;
							}
						
							vsd.visibleWorldObjectIds.Remove( kvp.Key );
						break;

					}
					
					vsd.visibleWorldObjectIds[ kvp.Key ] = 1;
					vsd.characterList[ kvp.Key ] = newStatus;
					vsd.characterList.Alive[ kvp.Key ] = mission.GetCharacter( kvp.Key ).IsAlive();
					vsd.characterList.Factions[ kvp.Key ] = mission.GetCharacter( kvp.Key ).Data.Faction;
				}
			}
			
			characterVisibility[ character ].observedCharactersIDs = new List< int >( vsd.visibleWorldObjectIds.Keys );
		}
		
	}
}