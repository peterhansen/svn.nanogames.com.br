using System;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class CharacterInArea : MissionObjective {
		
		protected CharacterInAreaData data = new CharacterInAreaData();
		
		
		public CharacterInArea( CharacterInAreaData data ) {
			this.data = data;
		}
		
		
		public CharacterInArea( AreaMissionData area, CharacterTargetMode target, Faction winningFaction ) {
			data.area = area;
			data.target = target;
			data.winningFaction = winningFaction;
		}
		
		
		// TODO: IGUAL ao do KillCharacter
		public override void OnInit( Mission mission ) {
			if( data.target.type == CharacterTargetMode.Type.TROOP_LEADER ) {
				// TODO: ainda precisamos de uma forma de identificar os personagens
				// que nao dependa de seus ids gerados dinamicamente. Por enquanto, estamos
				// carteando que o cara de uma faccao com o menor ID é o lider
				int minId = int.MaxValue;
				foreach( Character character in mission.GetCharacters( data.target.faction ) ) {
					if( character.GetWorldID() < minId ) {
						minId = character.GetWorldID();
						data.targetedCharacterID = character.GetWorldID();
					}
				}
			}
		}
		
		
		protected override ObjectiveResult CheckWhenCharacterMoved( Mission mission, Character character ) {//Check( Mission mission ) {
			Character targetedCharacter = mission.GetCharacter( data.targetedCharacterID );
			
			if( targetedCharacter != null && character == targetedCharacter && targetedCharacter.GetBox().Intersects( data.area.box ) ) {
				return new ObjectiveResult( true, data.winningFaction );
			} else {
				return new ObjectiveResult();
			}
		}
		
	}
	
}

