using System;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections.Generic;
using System.IO.Compression;

using GameModel;
using Utils;
using GameCommunication;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;


namespace TCPServer {
	
   public class Server {
    
		private TcpListener tcpListener;
     
		private HttpListener httpListener;
		
		private TcpListener crossDomainListener;
		
		private Thread listenThread;
		
		private GameServer gameServer;
		
		private const int PORT = 3000;
		
		private const int CROSS_DOMAIN_PORT = 843;
		
//		private const string CROSSDOMAIN_XML = "<?xml version=\"1.0\"?><cross-domain-policy>\n\t<allow-access-from domain=\"*\" to-ports=\"500-9999\"/></cross-domain-policy>";
		private const string CROSSDOMAIN_XML = @"<?xml version='1.0'?>
<cross-domain-policy>
        <allow-access-from domain=""*"" to-ports=""1500-4000"" />
</cross-domain-policy>";
		
		private static readonly string[] PREFIXES = { 
//				"http://localhost/",
//				"http://127.0.0.1/",
				"http://localhost:" + PORT + "/",
				"http://127.0.0.1:" + PORT + "/",
				"http://localhost:" + CROSS_DOMAIN_PORT + "/",
				"http://127.0.0.1:" + CROSS_DOMAIN_PORT + "/",
		};

     
		public Server() {
//			FileStream file = null;
//			BinaryFormatter bin = new BinaryFormatter();
//			bin.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
//			MapCollisionData dsadsad = Util.DeserializeFromXMLFile< MapCollisionData >( @"C:\projetos\Unity\favela-wars\game-client\trunk\game-client-current\missions\TESTE_12345678.level" );
//			file = new FileStream( @"C:\projetos\Unity\favela-wars\game-client\trunk\game-client-current\missions\TESTE_12345678.level", FileMode.Open );
//			object morra = bin.Deserialize( file );
			
			gameServer = GameServer.GetInstance();
			
			httpListener = new HttpListener( );
			foreach ( string s in PREFIXES )
				httpListener.Prefixes.Add( s );
			
			listenThread = new Thread( new ThreadStart( ListenForClients2 ) );
			listenThread.Start();
			
//			crossDomainListener = new TcpListener( IPAddress.Any, CROSS_DOMAIN_PORT );
//			Thread crossDomainThread = new Thread( new ThreadStart( ServeCrossDomain ) );
//			crossDomainThread.Start();

//			this.tcpListener = new TcpListener(IPAddress.Any, PORT );
//			this.listenThread = new Thread( new ThreadStart( ListenForClients ) );
//			this.listenThread.Start();
     	}
		
		
		private void ServeCrossDomain() {
			crossDomainListener.Start( );
			Console.WriteLine( "Crossdomain served on HTTP port " + CROSS_DOMAIN_PORT + "..." );
			
			ASCIIEncoding encoder = new ASCIIEncoding( );
			byte[] buffer = encoder.GetBytes( CROSSDOMAIN_XML );
					
			
			while ( true ) {
				try {
					//blocks until a client has connected to the server
					TcpClient client = crossDomainListener.AcceptTcpClient( );
					NetworkStream clientStream = client.GetStream( );
					
					Console.WriteLine( "crossdomain.xml served" );
					
					clientStream.Write (buffer, 0, buffer.Length);
					clientStream.Flush();
					clientStream.Close();
				} catch ( Exception e ) {
					Console.WriteLine( e );
				}
			}
		}
		
		
		private void ListenForClients2() {
			Console.WriteLine( "Listening HTTP\n\t" + string.Join( "\n\t", PREFIXES ) );
			httpListener.Start();
			
			while ( true ) {
				// Note: The GetContext method blocks while waiting for a request. 
			    HttpListenerContext context = httpListener.GetContext();
			    HttpListenerRequest request = context.Request;
				
				// Obtain a response object.
				HttpListenerResponse response = context.Response;
				response.AddHeader( "Content-Encoding", "gzip" ); 
				
				Console.WriteLine( "-> Connection received on port " + request.LocalEndPoint.Port );
				
				Stream output = response.OutputStream;
				
				try {
					if ( request.LocalEndPoint.Port == CROSS_DOMAIN_PORT ) {
					    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(CROSSDOMAIN_XML);
					    // Get a response stream and write the response to it.
					    response.ContentLength64 = buffer.Length;
					    output.Write( buffer,0,buffer.Length );
					    // You must close the output stream.
					    output.Close();					
					} else {
						if ( request.HasEntityBody ) {
							Stream body = request.InputStream;
							
							Encoding encoding = request.ContentEncoding;
						    StreamReader reader = new StreamReader(body, encoding);
						    Console.WriteLine( "-> Client data content length {0}", request.ContentLength64 );
						
						    // Convert the data to a string and display it on the console.
						    string dataBase64 = reader.ReadToEnd();
							Request clientRequest = ( Request ) Util.DeserializeFromBase64String( dataBase64 );
							gameServer.PushRequest( clientRequest );
							gameServer.ProcessRequests();
						    
							List< Response > responses = new List<Response>( gameServer.EnumerateResponses() );
							dataBase64 = Util.SerializeToBase64String( responses );
							byte[] responseData = Encoding.ASCII.GetBytes( dataBase64 );
							output.Write( responseData, 0, responseData.Length );
							Console.WriteLine( "<- {0} response(s) returned ({1} bytes)", responses.Count, responseData.Length );
							
							response.AddHeader( "Content-Size", responseData.Length.ToString() ); 
							
//							GZipStream gzip = new GZipStream( output, CompressionMode.Compress, false );
							
//							gzip.Write( allData, 0, allData.Length );
//							gzip.Flush();
//							gzip.Close();
							
//							for ( int i = 0; i < allData.Length; i += 10 ) {
//								output.Write( allData, i, 10 );
//								output.Flush();
//								Console.WriteLine( i + " / " + allData.Length );
////								
//								Thread.Sleep( 1 );
//							}
							
							body.Close();
						    reader.Close();
						}
					}
				} catch ( Exception e ) {
					Console.WriteLine( e );
					
					byte[] exceptionData = Encoding.UTF8.GetBytes( e.Message );
					output.Write( exceptionData, 0, exceptionData.Length );
				} finally {
					if ( output != null ) {
						output.Flush();
						output.Close();
					}
				}
			}
		}
	
	
		private void ListenForClients() {
			Console.WriteLine("Waiting for clients on port " + PORT + "..." );
			this.tcpListener.Start();
		
			while (true) {
		     //blocks until a client has connected to the server
		     TcpClient client = this.tcpListener.AcceptTcpClient();
		
		     //create a thread to handle communication 
		     //with connected client
		     Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
		     clientThread.Start(client);
			}
		}	
		
		
		private void HandleClientComm( object client ) {
			Console.WriteLine ("Client connected: " + client);
		   	TcpClient tcpClient = (TcpClient)client;
		   	NetworkStream clientStream = tcpClient.GetStream();
		
		   	byte[] message = new byte[ 4096 ];
		   	int bytesRead;
			
			StringBuilder sb = new StringBuilder();
		
			while ( clientStream.DataAvailable ) {
				bytesRead = 0;
		
				try {
					//blocks until a client sends a message
					bytesRead = clientStream.Read( message, 0, message.Length );
				} catch ( Exception ) {
					//a socket error has occured
					break;
				}
		
				if ( bytesRead == 0 ) {
					//the client has disconnected from the server
		       		break;
		     	}
		
		     	//message has successfully been received
		     	ASCIIEncoding encoder = new ASCIIEncoding();
				string m = encoder.GetString(message, 0, bytesRead);
				string[] parts = m.Split( new char[] { '\n' } );
				
				StringBuilder sbTemp = new StringBuilder();
				for ( int i = 0; i < parts.Length; ++i ) {
					sbTemp.Append( i );
					sbTemp.Append( " -> " );
					sbTemp.Append( parts[ i ] );
					sbTemp.Append( '\n' );
					sb.Append( sbTemp.ToString() );
					Console.WriteLine( sbTemp.ToString() );
				}
		   }
			
			SendResponse( tcpClient, sb.ToString() );
		
		   	tcpClient.Close();
		}
		
		
		private void SendResponse( TcpClient tcpClient, string message ) {
			NetworkStream clientStream = tcpClient.GetStream ();
			ASCIIEncoding encoder = new ASCIIEncoding ();
			byte[] buffer = encoder.GetBytes ( message );
			
			Console.WriteLine( "Sending response: " + message );

			clientStream.Write (buffer, 0, buffer.Length);
			clientStream.Flush ();
		}
		
	}
	
}