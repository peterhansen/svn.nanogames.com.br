using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class GetCharacterInfoHandler : Handler {
		
		protected GetCharacterInfoRequest request;
		
		
		public GetCharacterInfoHandler( GetCharacterInfoRequest getCharacterInfoRequest ) {
			this.request = getCharacterInfoRequest;
		}


		public override IEnumerable< Response > GetResponses() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			if ( mission == null ) {
				Debugger.LogError( "Couldn't find or create mission for player " + request.GetPlayerID() );
				yield break;
			}
			
			Character characterWorldObj = ( Character ) mission.GetWorldObject( request.characterWorldID );
			
			if ( characterWorldObj == null ) {
				// no character corresponding to ID
				yield break;
			}
			
			Response r = new Response();
			r.AddGameActionData( new GetCharacterInfoData( characterWorldObj.Data, mission.GetPlayerId( characterWorldObj ) == request.GetPlayerID() ) );
			yield return r;
		}
		
	}
}

