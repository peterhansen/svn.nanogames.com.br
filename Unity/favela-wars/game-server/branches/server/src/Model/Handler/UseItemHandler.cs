using System;
using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;



namespace GameModel {
	
	public class UseItemHandler : Handler {
		
		private UseItemRequest request;
		
		
		public UseItemHandler( UseItemRequest useItemRequest ) {
			this.request = useItemRequest;
		}
		
		
		public override IEnumerable< Response > GetResponses() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetCharacter( request.characterID );
			 
			if( character == null || mission.GetPlayerId( character ) != request.GetPlayerID() ) {
				Debugger.LogWarning( "Jogador tentando usar item com personagem fora de sua tropa. PlayerID: " + request.GetPlayerID() + ", characterID: " + request.characterID );
				yield break;
			}
			
			ConsumableItem item = ( ConsumableItem ) character.GetItemByEquipID( request.itemID );
			if ( item == null ) {
				Debugger.LogError( "Invalid itemID: " + request.itemID );
				yield break;
			}
			
			Response r = new Response();
			
			// verifica se o personagem tem AP para usar o item; se tiver, desconta seu AP
			float apCost = item.GetAPCost( ItemUseMode.REGULAR );
			if ( apCost > character.Data.ActionPoints ) {
				AddInsufficientAPMessage( mission, r, request.GetPlayerID(), character.GetWorldID() );
				yield return r;
				yield break;
			}
			
			Character targetCharacter = null;
			switch ( item.Mode ) {
				case ItemTargetMode.Any:
				case ItemTargetMode.Character:
					targetCharacter = mission.GetCharacter( request.targetCharacterID );
					if ( targetCharacter == null ) {
						Debugger.LogError( "Invalid targetCharacterID: " + request.targetCharacterID );
						yield break;
					}
				break;

				case ItemTargetMode.Position:
					// TODO verificar ponto real de colisão no caso de itens que podem ser usados num ponto do cenário em vez de em um personagem
					// TODO implementar itens que podem ser usados numa posição, e não necessariamente num personagem
				break;

				case ItemTargetMode.Self:
					if ( request.characterID != request.targetCharacterID ) {
						Debugger.LogError( "Invalid target for self item: " + request.characterID + " -> " + request.targetCharacterID );
						yield break;
					}
				
					targetCharacter = character;
				break;
			}
			
			GameVector target = new GameVector( targetCharacter == null ? request.destination : targetCharacter.GetPosition() );
			float distance = ( target - character.GetPosition() ).Norm();
			if ( distance > item.Range ) {
				Debugger.LogError( "Target beyond item range." );
				yield break;
			}
			
			character.Data.ActionPoints.Value -= ( int ) Math.Round( apCost );
			
			if ( !item.Use( request.destination, targetCharacter ) ) {
				// item foi esgotado
				character.Inventory.RemoveItem( item.EquipID );
			}
			
			// mudando direcao do personagem, caso ele não esteja aplicando o item em si mesmo
			if ( item.Mode != ItemTargetMode.Self && request.characterID != request.targetCharacterID ) {
				GameVector origin = character.GetPosition();
				character.GetGameTransform().direction = ( request.destination - origin ).Unitary();
				r.AddGameActionData( new SetWorldObjectTransformData( request.characterID, character.GetGameTransform() ) );
				
				VisibilityChangeData visibilityData = mission.GetVisibilityManager().CalculateVisibilityFor( character );
				r.AddGameActionData( visibilityData );
			}
			
			// chamando animações de uso de item
			UseItemData useItemData = new UseItemData(  request.targetCharacterID, "Item usado com sucesso!" );
			r.AddGameActionData( useItemData );
			
			yield return r;
		}
	}
}

