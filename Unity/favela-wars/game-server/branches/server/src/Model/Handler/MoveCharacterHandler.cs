using System;
using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class MoveCharacterHandler : Handler {
		
		protected enum MovementInterruptionType {
			NO_INTERRUPTION,
			NO_AP_LEFT,
			ENEMY_SPOTTED,
			ENEMY_COUNTERATTACK,
			ENEMY_SPOTTED_AND_COUNTERATTACK
		}
		
		protected MoveCharacterRequest moveCharacterRequest;
		
		protected VisibilityManager visibilityManager;
		protected SectorManager sectorManager;
		
		protected Character counterAttackingCharacter;
		protected List< Character > spottedCharacters = new List<Character>();
		
		
		public MoveCharacterHandler( MoveCharacterRequest moveCharacterRequest ) {
			this.moveCharacterRequest = moveCharacterRequest;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		public override IEnumerable< Response > GetResponses() {
			int playerID = moveCharacterRequest.GetPlayerID();
			Mission mission = Mission.GetInstance( playerID );
			Character character = mission.GetCharacter( moveCharacterRequest.worldObjectID );
			visibilityManager = mission.GetVisibilityManager();
			sectorManager = mission.GetMap().GetCollisionManager().GetSectorManager();
			
			Response response = new Response( ResponseTarget.Requester );
			if ( !CheckIfCharacterIsUnderPlayerControl( mission, playerID, character ) ) {
				Debugger.Log( "Personagem não pertence à tropa do jogador." );
				yield break;
			}
			
			if ( character.Data.ActionPoints <= Common.CharacterInfo.INITIAL_AP_COST ) {
				response.Target = ResponseTarget.Requester;
				AddInsufficientAPMessage( mission, response, playerID, character.GetWorldID() );
				yield return response;
			} 
			
			List< GameVector > positions = moveCharacterRequest.path;
			if ( moveCharacterRequest.path == null ) {
				Debugger.Log( "caminho inválido" );
				response.Target = ResponseTarget.Requester;
				AddMessage( mission, response, playerID, character.GetWorldID(), "Caminho inválido." );
				yield return response;
				yield break;
			}
			
			// ajustando caminho para movimentar corretamente personagem em relacao ao topo de sua caixa (por enquanto, considerado
			// o centro da visão do personagem)
			foreach ( GameVector position in positions )
				position.y += character.GetBox().GetScale().y * 0.5f;
				
			MovementInterruptionType interruption;
			MoveCharacterData moveCharacterData = GenerateMoveCharacterData( mission, character, positions, out interruption );
			response.AddGameActionData( moveCharacterData );
			
			response.Target = ResponseTarget.All;
			yield return response;
			
			// atualiza posicao e direcao do personagem para posicao final do caminho
			mission.GetMap().OnWorldObjectMoved( character, mission.GetVisibilityManager() );
			
			switch ( interruption ) {
				case MovementInterruptionType.NO_AP_LEFT:
					AddInsufficientAPMessage( mission, response, playerID, character.GetWorldID() );
				break;
						
				case MovementInterruptionType.ENEMY_SPOTTED:
					AddMessage( mission, response, playerID, character.GetWorldID(), "Inimigo Encontrado!" );
				break;
					
				case MovementInterruptionType.ENEMY_SPOTTED_AND_COUNTERATTACK:
					AddMessage( mission, response, playerID, character.GetWorldID(), "Inimigo Encontrado!" );
				 	GenerateCounterAttack( character, response );
				break;
					
				case MovementInterruptionType.ENEMY_COUNTERATTACK:
					GenerateCounterAttack( character, response );					
				break;
					
				default:
					// em caso de não interrupcao, também vamos deixar o personagem ficar em sua stance final
					if ( moveCharacterRequest.finalStance != character.GetStance() ) {
						SetStanceRequest setStanceRequest = new SetStanceRequest( playerID, moveCharacterRequest.worldObjectID, moveCharacterRequest.finalStance );
						SetStanceHandler setStanceHandler = new SetStanceHandler( setStanceRequest );

						foreach ( Response setStanceHandlerResponse in setStanceHandler.GetResponses() ) {
							yield return setStanceHandlerResponse;
						}
					}
				break;
			}
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		protected MoveCharacterData GenerateMoveCharacterData( Mission mission, Character character, List< GameVector > nodes, out MovementInterruptionType interruption ) {			
			float apPerMeter = character.Data.ActionPointsPerMeter;
			float actionPointsTotal = Common.CharacterInfo.INITIAL_AP_COST;
			interruption = MovementInterruptionType.NO_INTERRUPTION;
			
			GameVector finalDirection = moveCharacterRequest.finalDirection;
			
			// ao longo do algoritmo para gerar um MoveCharacterData, vamos aproveitar e gerar nossa VisibilityInfo
			VisibilityInfo visibilityInfo = new VisibilityInfo();
			for( int i = 1; i < nodes.Count; ++i ) {
				float actionPointsToConsume = ( nodes[ i ] - nodes[ i - 1 ] ).Norm() * character.Data.ActionPointsPerMeter;
				float temp = actionPointsTotal + actionPointsToConsume;
				if ( temp > character.Data.ActionPoints ) {
					interruption = MovementInterruptionType.NO_AP_LEFT;
				}
				actionPointsTotal = temp;
				
				// a direção precisa ser ajustada antes da posição: primeiro se olha para onde se quer andar
				// TODO: embora esteja sendo trocada aqui, a direcao nao faz nenhuma diferenca para a colisão, por enquanto
				character.GetGameTransform().direction.Set( nodes[ i ].x - nodes[ i - 1 ].x, 0, nodes[ i ].z - nodes[ i - 1 ].z );
				character.GetGameTransform().direction.y = 0.0f;
				character.GetGameTransform().direction.Normalize();
				
				if ( ( i - 1 ) % Common.CharacterInfo.VISIBILITY_FREQUENCY == 0 ) {
					// TODO: extrair para um método a parte
					VisibilityChangeData vcd = visibilityManager.CalculateVisibilityFor( character );
					visibilityInfo.SetVisibilityAtStep( i, vcd );
					
					// verifica se algum personagem da outra tropa viu nosso personagem
					int enemiesSpottingCharacter = 0;
					foreach( Character watcherCharacter in visibilityManager.GetWatchers( sectorManager.GetSectorFromGameVector( nodes[ i ] ) ) ) {
						
						if ( watcherCharacter.Faction != character.Faction ) {
							enemiesSpottingCharacter++;
							if( watcherCharacter.ShouldCounterAttack( character ) ) {
								counterAttackingCharacter = watcherCharacter;
								
								visibilityManager.SpotCharacter( watcherCharacter, character );
								interruption = MovementInterruptionType.ENEMY_COUNTERATTACK;
							}
						}
					}
					
					if( enemiesSpottingCharacter == 0 ) {
						// TODO: por enquanto, ainda nao sei pegar a faccao inimiga da fase
						Faction enemyFaction = character.Faction == Faction.CRIMINAL? Faction.POLICE : Faction.CRIMINAL;
						CharacterVisibility characterTurnVisibility = visibilityManager.GetCharacterTurnVisibility( enemyFaction, character );
						
						if( characterTurnVisibility == CharacterVisibility.SEEN_FIRST_THIS_TURN ||
							characterTurnVisibility == CharacterVisibility.SEEN_AGAIN_THIS_TURN ) {
							
							// o personagem não está sendo mais visto nesse turno
							visibilityManager.SetCharacterTurnVisibility( enemyFaction, character, CharacterVisibility.NOT_SEEN );
						}		
					}
					
					// verifica se nosso personagem viu alguem da outra tropa nesse passo
					foreach( int spottedCharacterId in vcd.visibleWorldObjectIds.Keys ) {
						Character spottedCharacter = mission.GetCharacter( spottedCharacterId );
						
						if( spottedCharacter.Faction != Faction.NEUTRAL && spottedCharacter.Faction != character.Faction &&
							visibilityManager.GetCharacterTurnVisibility( character.Faction, spottedCharacter ) == CharacterVisibility.SEEN_FIRST_THIS_TURN ) {
							
							if( interruption == MovementInterruptionType.NO_INTERRUPTION )
								interruption = MovementInterruptionType.ENEMY_SPOTTED;
							else if( interruption == MovementInterruptionType.ENEMY_COUNTERATTACK )
								interruption = MovementInterruptionType.ENEMY_SPOTTED_AND_COUNTERATTACK;
						
							visibilityManager.SpotCharacter( character, spottedCharacter );
							spottedCharacters.Add( spottedCharacter );
						}
					}
				}
				
				character.GetGameTransform().position.Set( new GameVector( nodes[ i ] ) );
				
				if( interruption != MovementInterruptionType.NO_INTERRUPTION ) {
					i++;
					while( i < nodes.Count )
						nodes.RemoveAt( i );
					
					// recalculamos direcao final, em funcao da interrupcao
					finalDirection = ( nodes[ nodes.Count - 1 ] - nodes[ nodes.Count - 2 ] ).Normalized();
						
					character.Data.ActionPoints.Value -= ( int ) actionPointsTotal;
					return CreateMoveCharacterData( character, nodes, visibilityInfo, finalDirection );
				}
			}
			
			// caso o personagem nao tenha sido interrompido, vamos dar colocá-lo em sua direcao final, recalcular
			// essa última visibilidade antes de criar o MoveCharacterData
			// TODO: embora esteja sendo trocada aqui, a direcao nao faz nenhuma diferenca para a colisão, por enquanto
			character.GetGameTransform().direction.Set( finalDirection );
			character.GetGameTransform().direction.y = 0.0f;
			character.GetGameTransform().direction.Normalize();
			VisibilityChangeData finalVisibilityData = visibilityManager.CalculateVisibilityFor( character );
			visibilityInfo.FinalVisibility = finalVisibilityData;
			
			character.Data.ActionPoints.Value -= actionPointsTotal;
			
			return CreateMoveCharacterData( character, nodes, visibilityInfo, finalDirection );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		protected MoveCharacterData CreateMoveCharacterData( Character character, List< GameVector > nodes, VisibilityInfo visibilityInfo, GameVector finalDirection ) {
			foreach( GameVector node in nodes )
				node.y -= character.GetBox().GetScale().y * 0.5f;
			
			// Nota: não precisamos mandar para o cliente dados de visibilidade caso o personagem que está sendo movido é um NPC
			MoveCharacterData moveCharacterData = new MoveCharacterData( character.GetWorldID(), nodes, character.Faction == Faction.NEUTRAL? null : visibilityInfo, finalDirection );			// TODO: o personagem poderia tambem nao ir em sua velocidade total para passos curtos
			moveCharacterData.percentualSpeed = character.Data.Agility.Percent;
			moveCharacterData.movementStance = character.GetStance();
				
			return moveCharacterData;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		protected void GenerateCounterAttack( Character targetCharacter, Response response ) {
			int weaponId = targetCharacter.GetWeaponAt( CharacterPart.LeftHand ).EquipID;
			
			// TODO: como vamos fazer para tratar esse lance do opposingPlayerId (para executar tiros em nome de outra tropa )?
			int opposingPlayerId = moveCharacterRequest.GetPlayerID() == 1? 0: 1;
			ShootRequest shootRequest = new ShootRequest( opposingPlayerId, counterAttackingCharacter.GetWorldID(), weaponId, ItemUseMode.REGULAR, targetCharacter.GetGameTransform().position );
			ShootHandler counterAttackShootHandler = new ShootHandler( shootRequest );
		
			foreach ( Response shootResponse in counterAttackShootHandler.GetResponses() ) {
				// Não queremos repassar a visibilidade do inimigo!
				foreach ( GameActionData gameActionData in shootResponse.GetGameActionsData() ) {
					if ( !(gameActionData is VisibilityChangeData) )
						response.AddGameActionData( gameActionData );
				}
			}
		}
		
	}
}

