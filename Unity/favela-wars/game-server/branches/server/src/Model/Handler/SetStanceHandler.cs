using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class SetStanceHandler : Handler {
		
		protected SetStanceRequest request;
		
		public SetStanceHandler( SetStanceRequest request ) {
			this.request = request;
		}
		

		public override IEnumerable< Response > GetResponses() {
			Response r;
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetWorldObject( request.worldObjectID ) as Character;

			if( !CheckIfCharacterIsUnderPlayerControl( mission, request.GetPlayerID(), character ) ) {
				Debugger.LogWarning( "Jogador tentando modificar pose personagem fora de sua tropa. PlayerID: " + request.GetPlayerID() + ", characterID: " + request.worldObjectID );
				yield break;
			}
			
			r = new Response();
			character.SetStance( request.newStance );
			r.AddGameActionData( new SetStanceData( character.GetWorldID(), character.GetStance() ) );
			yield return r;
		}
	}
}