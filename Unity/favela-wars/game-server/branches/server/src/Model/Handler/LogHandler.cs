using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;


namespace GameModel {
	
	public class LogHandler : Handler {
		
		protected LogRequest logRequest;
		
		
		public LogHandler ( LogRequest logRequest ) {
			this.logRequest = logRequest;
		}
		
		
		public override IEnumerable< Response > GetResponses() {
			Response r = new Response( ResponseTarget.Requester );
			r.AddGameActionData( new LogActionData( logRequest.logLevel, logRequest.message ) );
			yield return r;
		}
	}
}

