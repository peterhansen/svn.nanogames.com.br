using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class LoadSnapshotHandler : Handler {
		
		protected LoadSnapshotRequest loadSnapshotRequest;
		
		
		public LoadSnapshotHandler( LoadSnapshotRequest loadSnapshotRequest ) {
			this.loadSnapshotRequest = loadSnapshotRequest;
		}
		
		
		public override IEnumerable< Response > GetResponses() {
			Response r = new Response( ResponseTarget.Requester );
			Mission m = Mission.GetInstance( loadSnapshotRequest.GetPlayerID() );
			
			foreach ( Character character in m.GetCharacters() ) {
				r.AddGameActionData( new SetWorldObjectTransformData( character.GetWorldID(), character.GetGameTransform() ) );
			}
			
			yield return r;
		}
		
	}
}

