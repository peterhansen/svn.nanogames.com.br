using System;
using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class ThrowObjectHandler : Handler {
		
		private ThrowObjectRequest request;
		
		public ThrowObjectHandler( ThrowObjectRequest throwRequest ) {
			request = throwRequest;
		}


		public override IEnumerable< Response > GetResponses() {
			// TODO passar para Handler alguns tratamentos gerais dos handlers (personagem, missão, facção, etc.)
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetCharacter( request.characterWorldID );
			
			if ( character == null || mission.GetPlayerId( character ) != request.GetPlayerID() ) {
				Debugger.LogWarning( "Jogador tentando arremessar objeto com personagem fora de sua tropa. PlayerID: " + request.GetPlayerID() + ", characterID: " + request.characterWorldID );
				yield break;
			}
			
			Response r = new Response( ResponseTarget.Requester );
			if ( character.Data.ActionPoints < Common.CharacterInfo.THROW_AP_COST ) {
				AddInsufficientAPMessage( mission, r, request.GetPlayerID(), character.GetWorldID() );
				yield return r;
				yield break;
			}
			
			Item item = null;
			foreach ( Item i in character.GetActionItems() ) {
				if ( i.EquipID == request.objectID ) {
					item = i;
					break;
				}
			}
			
			if ( item == null ) {
				Debugger.LogError( "Jogador tentando arremessar item não equipado. Character: " + character.GetWorldID() + ", itemID: " + request.objectID );
				yield break;
			}
			
			CollisionManager cm = mission.GetMap().GetCollisionManager();
			
			// limita a força do arremesso em função das características do personagem
			if ( request.direction.Norm() > Common.CharacterInfo.MAX_THROW_SPEED ) {
				float percent = NanoMath.PercentInRange( character.Data.DefenseLevel, Nature.DefenseDefault.Min, Nature.DefenseDefault.Max );
				float throwSpeed = NanoMath.Lerp( percent, Common.CharacterInfo.MIN_THROW_SPEED, Common.CharacterInfo.MAX_THROW_SPEED );
				request.direction.SetScale( Common.CharacterInfo.MAX_THROW_SPEED );
			}
			
			Grenade grenade = item as Grenade;
			
			int maxBounces = ( grenade == null ) ? int.MaxValue : ( grenade.MaxBounces > 0 ? grenade.MaxBounces : int.MaxValue );
			
			float throwDistance = request.direction.Norm();
			GameVector maxDiff = ShootHandler.GetDeviation( character, Common.CharacterInfo.THROW_PRECISION, throwDistance, throwDistance );
			GameVector diff = GameVector.GetRandom( -maxDiff, maxDiff );
			Debugger.Log( "THROW: " + request.direction + " / " + diff );
			
			List< ThrowInfo > throws;
			GameVector handPosition = CalculateHandPosition( character );
			throws = cm.ThrowObject( character.GetPosition() + handPosition, request.direction, request.characterWorldID, maxBounces, item.RestitutionRate );
			
			// mudando direcao do personagem
			GameVector direction = request.direction.Unitary();
			direction.y = 0.0f;
			character.GetGameTransform().direction = direction;
			LookAtData lookAtData = new LookAtData( character.GetWorldID(), direction );
			r.AddGameActionData( lookAtData );
			VisibilityChangeData visibilityData = mission.GetVisibilityManager().CalculateVisibilityFor( character );
			r.AddGameActionData( visibilityData );	
			
			character.Data.ActionPoints.Value -= Common.CharacterInfo.THROW_AP_COST;
			if ( grenade != null ) {
				float timeToExplode = grenade.TimeLimit <= 0 ? float.MaxValue : grenade.TimeLimit;
				float totalTime = 0;
				int i = 0;
				GameVector explosionPosition = null;
				
				for ( ; i < throws.Count; ++i ) {
					ThrowInfo throwInfo = throws[ i ];
					float temp = totalTime;
					totalTime = Math.Min( totalTime + throwInfo.timeLimit, timeToExplode );
					
					if ( totalTime >= timeToExplode || i >= maxBounces ) {
						throwInfo.timeLimit = totalTime - temp;
						explosionPosition = throwInfo.parabola.GetPointAt( throwInfo.timeLimit );
						throws.RemoveRange( i + 1, throws.Count - ( i + 1 ) );
						break;
					}
				}
				if ( explosionPosition == null ) {
					ThrowInfo t = throws[ throws.Count - 1 ];
					explosionPosition = t.parabola.GetPointAt( t.timeLimit );
				}
				
				// tempo que a granada leva para explodir após parar de se mover
				float timeToDestroy = ( totalTime < timeToExplode && grenade.TimeLimit > 0 ) ? timeToExplode - totalTime : 0;
				r.AddGameActionData( new ThrowObjectData( request.characterWorldID, throws, timeToDestroy ) );
				
				// se a granada chegou ao seu destino antes do tempo de explosão, aguarda o momento de explodi-la
				if ( totalTime < timeToExplode )
					r.AddGameActionData( new WaitActionData( ( int ) ( timeToDestroy * 1000 ) ) );
				
				// nesse ponto, já temos a explosão da granada
				HitInfo[] explosionHits = cm.Explode( explosionPosition, grenade.Range );
				
				//Adiciona a estatistica do personagem que ele jogou a granada
				mission.OnCharacterThrowObject( character , grenade , 1, (explosionHits.Length > 0));
				
				foreach ( HitInfo eh in explosionHits ) {
					Character characterHit = mission.GetCharacter( eh.worldObjectId );
					
					if ( characterHit.IsAlive() ) {
						float distance = ( characterHit.GetGameTransform().position - eh.position ).Norm();
						float percent = NanoMath.Clamp( 1.0f - ( distance / grenade.Range ), 0, 1 );
						int damage = ( int ) NanoMath.Lerp( percent, 0, grenade.Power );
						characterHit.Data.HitPoints.Value -= damage;
						
						Debugger.Log( "#" + characterHit.GetWorldID() + " perdeu " + damage + " HP; restam " + characterHit.Data.HitPoints + " HP" );
						if ( characterHit.Data.HitPoints <= 0 )
							mission.OnCharacterKilled( r, character, characterHit );
						
						r.AddGameActionData( new CharacterHitData( characterHit.GetWorldID(), damage, characterHit.IsAlive() ) );

					}
				}
				
				
				
				r.AddGameActionData( new ExplosionData( explosionPosition, grenade.Range ) );
			} else {
				// arremessou um outro tipo de objeto (não é granada)
				r.AddGameActionData( new ThrowObjectData( request.characterWorldID, throws, -1 ) );
			}
			
			// jogador descarta o item após seu arremesso, e equipa uma nova granada caso tenha usado a atual
			character.RemoveItem( item );
			if ( grenade != null )
				character.EquipNextGrenadeIfEmpty();
			
			yield return r;
		}
		
		
		protected GameVector CalculateHandPosition( Character character ) {
			GameVector dir = character.GetDirection().Normalized();
			
			// vamos encontrar o vetor lateral a nossa direção rodando-o em -90 graus no eixo y 
			// (estamos considerando que a caixa dos personagens continua alinhada ao Y com seu movimento)
			GameVector side = new GameVector( dir.z, dir.y, -dir.x ).Normalized();
			return
				dir * Common.CharacterInfo.BOX_HALF_DEPTH +
				side * Common.CharacterInfo.BOX_HALF_WIDTH +
				GameVector.YAxis * Common.CharacterInfo.HAND_POSITION_ON_THROW_Y;
		}
	}
	
}

