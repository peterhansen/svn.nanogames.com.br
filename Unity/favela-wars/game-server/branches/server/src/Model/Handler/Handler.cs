using System;
using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public abstract class Handler {
		
		
		public abstract IEnumerable< Response > GetResponses();	
		
	
		protected void AddMessage( Mission mission, Response response, int playerID, int characterID, string message ) {
			// jogador enviou mensagem em nome do servidor, logo, não precisa receber mensagem.
			if ( playerID == mission.GetTurnManager().DelegatedNPCPlayer.GetID() )
				return;
				
			response.AddGameActionData( new ShowMessageData( characterID, message ) );
		}		
		
		
		/// <summary>
		/// Adiciona a mensagem padrão de fim de AP do personagem.
		/// </summary>
		protected void AddInsufficientAPMessage( Mission mission, Response response, int playerID, int characterID ) {
			AddMessage( mission, response, playerID, characterID, "Pontos de ação insuficientes." );
		}
		
		
		protected bool CheckIfCharacterIsUnderPlayerControl( Mission mission, int playerID, Character character ) {
			if ( character == null ) {
				Debugger.LogWarning( "Jogador tentando mover personagem nulo" );
				return false;
			}
			
			// Só deixamos o jogador mover o personagem caso ele seja seu dono ou se ele esteja movendo um NPC e ele seja o responsável por movê-los
			int delegatedPlayerID = mission.GetTurnManager().DelegatedNPCPlayer.GetID();
			int characterPlayerID = mission.GetPlayerId( character );
			if( characterPlayerID == Common.NPCInfo.PLAYER_ID ) {
				if( playerID != delegatedPlayerID ) {
					Debugger.LogWarning( "Jogador " + playerID + " tentando mover NPC sem autoridade. O certo seria ter id = " + delegatedPlayerID );
					return false;
				}
			} else if( playerID != characterPlayerID ) {
				Debugger.LogWarning( "Jogador tentando mover personagem fora de sua tropa. PlayerID: " + playerID + ", characterID: " + characterPlayerID );
				return false;
			}

			return true;
		}
		
	}
}

