using System;
using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class UpdateVisibilityHandler : Handler {
		
		protected UpdateVisibilityRequest updateVisibilityRequest;
		
		
		public UpdateVisibilityHandler( UpdateVisibilityRequest updateVisibilityRequest ) {
			this.updateVisibilityRequest = updateVisibilityRequest;
		}
		

		public override IEnumerable< Response > GetResponses() {
			Response r = new Response( ResponseTarget.Requester );
			
			UpdateVisibilityData visibilityData = new  UpdateVisibilityData( updateVisibilityRequest.watcherId );
			r.AddGameActionData( visibilityData );
			
			yield return r;
		}
		
	}
}
