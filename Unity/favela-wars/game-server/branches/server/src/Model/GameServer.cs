using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using SharpUnit;

using GameCommunication;
using Utils;


namespace GameModel {	
	
	/// <summary>
	/// Classe que faz o roteamento de requisições e tratadores dessas requisições - efetivamente servindo de coodernador de regras de jogo.
	/// </summary>
	public class GameServer {
	
		
		/// <summary>
		/// Requisições pendentes
		/// </summary>
		private Queue pendingRequests = new Queue();
		
		/// <summary>
		/// Gerencia as respostas que ainda precisam ser enviadas para cada cliente.
		/// </summary>
		private Dictionary< int, Queue< Response > > outboundResponses = new Dictionary< int, Queue<Response > >();
		
		// relaciona os jogadores às missões ativas
		private readonly Dictionary< int, Mission > players = new Dictionary< int, Mission >();
		
		private static GameServer instance;
		
		
		/// <summary>
		/// Construtor - apenas inicializa a classe de modo que, em modo debug, ela não faça nada mas avise que foi 
		/// usada de forma não inicializada. Em modo release, ele não deve fazer nada.
		/// </summary>
		private GameServer() {
			Debugger.Log( "GameServer: STARTED" );
		}
		
		
		public static GameServer GetInstance() {
			if ( instance == null )
				instance = new GameServer();
			
			return instance;
		}
		
		
		public static void Unload() {
			Debugger.Log( "GameServer: STOPPED" );
			instance = null;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name='id'>
		/// 
		/// </param>
		/// <param name='mission'>
		/// 
		/// </param>
		public void AddPlayer( int id, Mission mission ) {
			if ( !players.ContainsKey( id ) ) {
				Debugger.Log( string.Format( "GameServer: player #{0} JOINED mission({1})", id, mission ) );
				
				players[ id ] = mission;
				
				if ( !outboundResponses.ContainsKey( id ) ) {
					outboundResponses[ id ] = new Queue< Response >();
				}
			} else if ( players[ id ] != mission ) {
				// TODO jogador estava em outra missão (situação não deveria ocorrer)
				// primeiro o remove da missão anterior
				RemovePlayer( id );
				
				// agora podemos adicioná-lo sem problemas
				AddPlayer( id, mission );
			}
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name='id'>
		/// 
		/// </param>
		public void RemovePlayer( int id ) {
			Mission mission = players.ContainsKey( id ) ? players[ id ] : null;
		
			players.Remove( id );
			outboundResponses.Remove( id );
			
			if ( mission != null ) {
				Debugger.Log( string.Format( "GameServer: player #{0} LEFT mission({1})", id, mission ) );
				
				// jogador estava numa missão
				// TODO informar à missão que jogador desconectou; se for o último jogador, encerra a missão
			}
		}
			
		
		/// <summary>
		/// Processa todos os requests enfileirados e processsa, preparando para a obtenção de resposta.
		/// </summary>
		public void ProcessRequests() {
			for ( int c = 0; c < pendingRequests.Count; ++c ) {
				Request request = ( Request ) pendingRequests.Dequeue();
				Type type = Type.GetType( request.GetType().ToString().Replace( "GameCommunication", "GameModel" ).Replace( "Request", "Handler" ), true );
				ConstructorInfo constructor = type.GetConstructor( new Type[] { request.GetType() } );
				
				if ( constructor != null ) {
					Handler handler = ( Handler ) constructor.Invoke( new object[] { request } );
					foreach ( Response response in handler.GetResponses() ) {
						PushResponse( request.GetPlayerID(), response );
					}
				}
			}
		}
		
		
		/// <summary>
		/// Enfileira requisição
		/// </summary>
		/// <param name="request">
		/// O <see cref="Request"/> é enfileirado e aguarda um <see cref="doProcessRequests"/> para que ele eventualmente
		/// crie uma <see cref="Response"/>, que deverá ser resgatada com <see cref="popResponse"/>
		/// </param>
		public void PushRequest( Request request ) {
			pendingRequests.Enqueue( request );
			ProcessRequests();
		}
		
		
		public IEnumerable< Response > PopAllResponses( int playerID ) {
			// TODO somente remover efetivamente as respostas da fila após garantia de que chegaram ao cliente
			
			if ( outboundResponses.ContainsKey( playerID ) ) {
				Queue< Response > pendingResponses = outboundResponses[ playerID ];
			
				Debugger.Log( string.Format( "GameServer: SENDING {0} messages to player #({1})", pendingResponses.Count, playerID ) );
				while ( pendingResponses.Count > 0 ) {
					yield return pendingResponses.Dequeue();
				}
			}
		}
		
		
		public void PushResponse( int requesterID, Response response ) {
			List< int > playerIDs = new List< int >();
			Mission mission = players[ requesterID ];
				
			switch ( response.Target ) {
				case ResponseTarget.All:
					PushResponse( mission.GetPlayers(), response );
					return;
				
				case ResponseTarget.Faction:
					// TODO obter decentemente a facção do jogador
					Faction f = requesterID == 0 ? Faction.POLICE : Faction.CRIMINAL;
					foreach ( Player p in mission.GetPlayers() ) {
						Faction f2 = p.GetID() == 0 ? Faction.POLICE : Faction.CRIMINAL;
						if ( f == f2 ) {
							playerIDs.Add( p.GetID() );
						}
					}
				break;
			
				case ResponseTarget.Requester:
					playerIDs.Add( requesterID );
				break;
			}
			
			PushResponse( playerIDs, response );
		}
		
		
		public void PushResponse( IEnumerable< Player > players, Response r ) {
			List< int > playerIDs = new List< int >();
			
			foreach ( Player p in players ) {
				playerIDs.Add( p.GetID() );
			}
			
			PushResponse( playerIDs, r );
		}
		
		
		public void PushResponse( List< int > playerIDs, Response r ) {
			foreach ( int playerID in playerIDs ) {
				outboundResponses[ playerID ].Enqueue( r );
			}
		}
		
	}
	
	
	#region testes
	
	
	/// <summary>
	/// Teste de caso básico: Se eu peço ao GameManager para tratar minha requisição de soma (devidamente registrada),
	/// Ele deve me responder o valor da soma realizada. 
	/// </summary>
	public class GameManagerTester : TestCase {
		
		/// <summary>
		/// Apenas constroi a requisição, mas ainda não configura
		/// </summary>
		public override void SetUp() {
		}
		
		
		/// <summary>
		/// Nada para desconstruir
		/// </summary>
		public override void TearDown() {
		}
		
		
		/// <summary>
		/// Caso de teste: Requisição deve obter o valor de resposta apropriado
		/// </summary>
		[UnitTest]
		public void RequestShouldGetApropriateResponse () {
//			SumResponse sumResponse;
//			GameManager gm;
//			
//			gm = new GameManager ();
//			
//			sumRequest.GetRequestDataMap ().Add ("param1", "1");
//			sumRequest.GetRequestDataMap ().Add ("param2", "4");
//			gm.AddRequestHandler (0, typeof(SumHandler));
//			gm.PushRequest (sumRequest);
//			gm.DoProcessRequests ();
//			sumResponse = (SumResponse)gm.PopResponse ();
//			
//			Assert.Equal ( sumResponse.GetReturnCode(), 5 );
		}
	}	
	
	#endregion
}
