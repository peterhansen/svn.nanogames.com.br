using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public enum EffectType {
		AttackLevel,
		DefenseLevel,
		Agility,
		Technique,
		Intelligence,
		Perception,
		HitPoints,
		MoralPoints,
	}
	
	
	[ Serializable ]
	public class EffectItem : ConsumableItem {
		
		protected float points = 50;
		
		protected int duration = 0;
		
		protected EffectType effectType;
		
				
		public EffectItem( EffectType type, int duration, float points ) : base() {
			Range = Common.LEVEL_SECTOR_DIAGONAL;
			EffectType = type;
			Duration = duration;
			Points = points;
		}
		
		
		/// <summary>
		/// Pontos de atributo modificados ao se usar o item (pode ser positivo ou negativo).
		/// </summary>
		[ ToolTip( "Pontos de atributo modificados ao se usar o item (pode ser positivo ou negativo)." ) ]
		public float Points {
			get { return points; }
			set { points = value; }
		}
		
		
		/// <summary>
		/// Duração do efeito, em turnos.
		/// </summary>
		[ ToolTip( "Duração do efeito, em turnos." ) ]
		public int Duration {
			get { return duration; }
			set { duration = Math.Max( 0, value ); }
		}
		
		
		/// <summary>
		/// Tipo de efeito causado pelo item.
		/// </summary>
		[ ToolTip( "Tipo de efeito causado pelo item." ) ]
		public EffectType EffectType {
			get { return effectType; }
			set { effectType = value; }
		}		
		
		
		public override IEnumerable<ItemUseMode> GetAvailableUseModes() {
			yield return ItemUseMode.REGULAR;
		}
		
		
		public override float GetUseModeAPCost( ItemUseMode useMode ) {
			return 10.0f;
		}
		
		
		protected override void Consume( GameVector target, Character character ) {
			character.AddEffect( new AttributeEffect( effectType, duration, points ) );
		}
	}
}

