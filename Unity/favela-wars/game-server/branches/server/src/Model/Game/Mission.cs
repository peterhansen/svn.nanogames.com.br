using System;
using System.Collections;
using System.Collections.Generic;

using SharpUnit;

using Utils;
using GameCommunication;
using System.Reflection;


namespace GameModel {
	
	using HashSetMissionCondition = Dictionary< MissionObjective, int >;
	
	using HashSetSpawnPoint = Dictionary< SpawnPoint, int >;
	
	
	public class Mission {
		
		/// <summary>
		/// Relaciona índice dos jogadores a uma instância de uma missão.
		/// </summary>
		protected static Dictionary< int, Mission > instances = new Dictionary< int, Mission >();
		
		protected Map map;
	
		/// <summary>
		/// Relaciona índices dos WorldObjects às suas instâncias
		/// </summary>
		//protected Dictionary< int, WorldObject > worldObjects = new Dictionary< int, WorldObject >();
		protected Dictionary< int, WorldObject > staticScenaryObjects = new Dictionary< int, WorldObject >();
		
		protected Dictionary< int, Character > characters = new Dictionary< int, Character >();
		
		protected Dictionary< int, int > charactersPerPlayer = new Dictionary< int, int >();	
		
		protected List< Player > players = new List< Player >();
		
		protected HashSetSpawnPoint spawnPoints = new HashSetSpawnPoint();	
		
		protected VisibilityManager visibilityManager;
		
		protected EvolutionManager evolutionManager;
		
		protected StatisticsManager statisticsManager;
				
		/// <summary>
		/// Informacoes relativas aos dados do turno corrente da missão.
		/// </summary>
		//protected TurnData turnData;
		protected TurnManager turnManager;
		
		protected MissionData data;
		
		protected List< MissionObjective > missionObjectives = new List< MissionObjective >();
		
		/// <summary>
		/// Jogador o qual recebe a responsabilidade de calcular os caminhos dos NPCs para esse turno.
		/// </summary>
		//protected Player npcMovePlayer;
		
		
		protected Mission( string levelPrefix, List< uint > soldierIDs ) {
			data = MissionData.ImportFromFile( levelPrefix );
			if ( data == null ) {
				throw new Exception( "Mission: error importing MissionData file." );
			}
			
			// TODO: por enquanto, comecamos com a policia
			turnManager = new TurnManager( this );
			map = new Map( data.GetMapName() );			
			
			// id dos objetos de cenário é negativo, começando em -1
			int worldID = -1;
			
			foreach( Box box in map.GetCollisionManager().GetBoxes() ) {
				WorldObject wo = new WorldObject( worldID, "-" );
				box.SetWorldObjectId( worldID );
				wo.SetBox( box );
				staticScenaryObjects.Add( worldID, wo );
				--worldID;
			}
			
			foreach ( SpawnAreaMissionData spawn in data.spawnAreasData.Keys ) {
				// inicializando spawn areas
				SpawnPoint p = new SpawnPoint( this.characters.Count, spawn );
				spawnPoints.Add( p, 0 );
			}
			
			List< Character > characters = new List< Character >();
			
			// id dos personagens é positivo
			worldID = 1;
			
			HashSetSpawnPoint npcSpawnPoints = GetValidSpawnPoints( Faction.NEUTRAL );
			foreach ( SpawnPoint p in npcSpawnPoints.Keys ) {
				int npcs = p.GetRandomQuantity();
				for ( int i = 0; i < npcs; ++i ) {
					NPC npc = new NPC( worldID++ );
					characters.Add( npc );
					charactersPerPlayer.Add( npc.GetWorldID(), -1 ); // TODO assumindo -1 como id do jogador de NPCs
				}
			}
			
			foreach ( int soldierID in soldierIDs ) {
				BOPE b = new BOPE( worldID++, soldierID.ToString() );
				characters.Add( b );
				charactersPerPlayer.Add( b.GetWorldID(), 0 ); // TODO assumindo 0 como id do jogador do BOPE
			}
			
			for ( int i = 0; i < 5; ++i ) {
				DrugDealer d = new DrugDealer( worldID++ );
				characters.Add( d );
				charactersPerPlayer.Add( d.GetWorldID(), 1 ); // TODO assumindo 1 como id do jogador dos traficantes
			}
			
			PopulateSpawnPoints( characters );
			visibilityManager = new VisibilityManager( this );
			
			// Agora que os personagens já foram adicionados, inicializamos 
			// o Evolution Manager os Mission Objectives
			evolutionManager = new EvolutionManager( this );
			
			//Statistics Manager
			statisticsManager = new StatisticsManager( this );
			
			foreach( MissionObjectiveData endMissionConditionData in data.endMissionConditions ) {
				Type type = Type.GetType( endMissionConditionData.GetType().ToString().Replace( "GameCommunication", "GameModel" ).Replace( "Data", "" ), true );
				ConstructorInfo constructor = type.GetConstructor( new Type[] { endMissionConditionData.GetType() } );
				
				if ( constructor != null ) {
					MissionObjective endMissionCondition = ( MissionObjective ) constructor.Invoke( new object[] { endMissionConditionData } );
					endMissionCondition.OnInit( this );
				} else {
					Debugger.LogError( "Mission objective constructor not found for " + endMissionConditionData );
				}
			}
		}
		
		
		/// <summary>
		/// Enumera todos os personagens da missão.
		/// </summary>
		public IEnumerable< Character > GetCharacters() {
			foreach ( Character ch in characters.Values )				
				yield return ch;
		}
	
		
		/// <summary>
		/// Enumera todos os personagens de uma determinada facção.
		/// </summary>
		public IEnumerable< Character > GetCharacters( Faction faction ) {
			foreach ( Character ch in characters.Values )
				if ( ch.Data.Faction == faction )
					yield return ch;
		}
		
		/// <summary>
		/// Enumera todos os personagens que NÃO são de uma determinada facção.
		/// </summary>
		public IEnumerable< Character > GetCharactersExcept( Faction faction ) {
			foreach ( Character ch in characters.Values )
				if ( ch.Data.Faction != faction )
					yield return ch;
		}
		
		
		/// <summary>
		/// Obtém o personagem com o id recebido, ou <c>null</c> caso não seja encontrado personagem com este id.
		/// </summary>
		public Character GetCharacter( int worldObjectId ) {
			try {
				return characters[ worldObjectId ];
			} catch ( Exception ) {
				return null;
			}
		}
		
		
		/// <summary>
		/// Enumera todos os objetos estáticos (de cenário) da missão.
		/// </summary>
		public IEnumerable< WorldObject > GetStaticObjects() {
			foreach ( WorldObject wo in staticScenaryObjects.Values )
				yield return wo;
		}
		
		
		public IEnumerable< Player > GetPlayers() {
			foreach ( Player p in players )
				yield return p;
		}
		
		
		public Player GetPlayer( int playerID ) {
			foreach( Player player in players ) {
				if( player.GetID() == playerID )
					return player;
			}
			
			return null;
		}
		
		
		public VisibilityManager GetVisibilityManager() {
			return visibilityManager;
		}
		
		
		public TurnManager GetTurnManager() {
			return turnManager;
		}
		
		
		public string GetName() {
			return data.name;
		}
		
		
		public MissionData GetData() {
			return data;
		}
		
		
		public byte[] GetRawAstarData() {
			return GetMap().GetCollisionManager().RawAStarData;
		}
		
		
		/// <summary>
		/// Obtém a facção rival a <c>faction</c>.
		/// </summary>
		public Faction GetEnemyFaction( Faction faction ) {
			switch ( faction ) {
				case Faction.POLICE:
					return Faction.CRIMINAL;
				
				case Faction.CRIMINAL:
					return Faction.POLICE;
				
				default:
					return Faction.NONE;
			}
		}
		
		
		/// <summary>
		/// Indica se duas facções são inimigas.
		/// </summary>
		public bool AreEnemies( Faction f1, Faction f2 ) {
			return GetEnemyFaction( f1 ) == f2;
		}
		
		
		public IEnumerable< Box > GetSpawnAreaBoxes( Faction faction ) {
			foreach( SpawnAreaMissionData spawnAreaData in data.spawnAreasData.Keys )
				if( spawnAreaData.faction == faction )
					yield return spawnAreaData.box;
		}
		
		
		public void OnBeforeTurnChange( TurnData turnData ) {
			foreach( Character character in GetCharacters( turnData.faction ) )
				character.PrepareToNewTurn();
			
			visibilityManager.OnBeforeTurnChange( turnData );
		}
		
		
		public void OnNewTurn( TurnData turnData ) {
			// verifica se algum dos objetivos da missão foi cumprido
			foreach ( MissionObjective objective in missionObjectives ) {
				ObjectiveResult result = objective.OnTurnChanged( this, turnData );
				
				if( result.missionIsOver ) {
					OnMissionEnded();
					
					Response endGameResponse = new Response( ResponseTarget.All );
					endGameResponse.AddGameActionData( new EndMissionActionData( result ) );
					GameServer.GetInstance().PushResponse( players, endGameResponse );
					
					return;
				}
			}
		}
		
		
		public void DelegateNPCMovement() {
			// TODO: quando formos fazer multiplayer, vamos precisar passar essa mensagem APENAS para o jogador sorteado
			Player newNPCPlayer = players[ (int) NanoMath.Random( players.Count ) ];
			turnManager.DelegatedNPCPlayer = newNPCPlayer;
			Debugger.Log( "Player escolhido para o calculo do caminho dos NPCs: " + turnManager.DelegatedNPCPlayer.GetID() );
			
			// criando resposta
			Response npcMovementResponse = new Response();
			MoveNPCActionData moveNPCActionData = new MoveNPCActionData();
			moveNPCActionData.playerID = turnManager.DelegatedNPCPlayer.GetID();
			
			foreach( Character npc in GetCharacters( Faction.NEUTRAL ) ) {
				if( NanoMath.Random( 0.0f, 1.0f ) < Common.NPCInfo.CHANCE_OF_MOVEMENT ) {
					WayPointCollection waypointCollection = GetWayPointCollection( Faction.NEUTRAL );
					WayPoint waypoint = waypointCollection.getRandomWayPoint();
					if(waypointCollection != null && waypointCollection.count() > 0 && waypoint != null){
						NPCMovement npcMovement = new NPCMovement( npc.GetWorldID(), NPCMovementType.FOLLOWING, waypoint.getPosition() );				
						//npcMovement.playerID = turnManager.DelegatedNPCPlayer.GetID();
						moveNPCActionData.npcMovements.Add( npcMovement );
					}
				}
			}
			
			npcMovementResponse.AddGameActionData( moveNPCActionData );
			GameServer.GetInstance().PushResponse( players, npcMovementResponse );
		}
		
		
		public void OnCharacterMoved( Response response, Character character ) {
			foreach ( MissionObjective condition in missionObjectives ) {
				ObjectiveResult result = condition.OnCharacterMoved( this, character );
				
				if ( result.missionIsOver ) {
					OnMissionEnded();
					
					response.AddGameActionData( new EndMissionActionData( result ) );

					return;
				}	
			}
		}
		
		public void OnCharacterThrowObject( Character character, Weapon weapon, int quantityOfAmmunition, bool hit ){
			statisticsManager.OnCharacterThrowObject( character , weapon , quantityOfAmmunition );
			evolutionManager.OnCharacterThrowObject( character , hit );
		}
		
		public void OnCharacterShoot( Character character, Weapon weapon, int quantityOfAmmunition, bool hit ){
			statisticsManager.OnCharacterShoot( character , weapon , quantityOfAmmunition );
			evolutionManager.OnCharacterShoot( character , hit );
		}
		
		
		public void OnCharacterKilled( Response response, Character killerCharacter, Character deadCharacter) {						
			evolutionManager.OnCharacterKilled( killerCharacter , deadCharacter );
			statisticsManager.OnCharacterKilled ( killerCharacter, deadCharacter );
				
			
			foreach ( MissionObjective condition in missionObjectives ) {
				ObjectiveResult result = condition.OnCharacterKilled( this, deadCharacter );
				
				if ( result.missionIsOver ) {
					OnMissionEnded();
					
					response.AddGameActionData( new EndMissionActionData( result ) );
					
					return;
				}	
			}
		}
		
		public void OnMissionStart(){
			statisticsManager.OnMissionStart();
			evolutionManager.OnMissionStart();
		}
		
		public void OnMissionEnded(){
			statisticsManager.OnMissionEnded();
			evolutionManager.OnMissionEnded();
		}
		
		private HashSetSpawnPoint GetValidSpawnPoints( Faction faction ) {
			HashSetSpawnPoint validSpawnPoints = new HashSetSpawnPoint();
			
			foreach ( SpawnPoint p in spawnPoints.Keys ) {
				if ( p.GetFaction() == faction )
					validSpawnPoints.Add( p, 0 );
			}
			
			return validSpawnPoints;
		}
		
		private WayPointCollection GetWayPointCollection( Faction faction ) {
			WayPointCollection wayPointCollection = new WayPointCollection();
			
			foreach ( SpawnAreaMissionData sa in data.spawnAreasData.Keys ) {
				if(sa.faction == faction){
					wayPointCollection = sa.waypointCollection;
					break;
				}
			}
			
			return wayPointCollection;
			//return null;
		}	
						
						
		private void PopulateSpawnPoints( List< Character > characters ) {
			Dictionary< Faction, HashSetSpawnPoint > validAreas = new Dictionary< Faction, HashSetSpawnPoint >();
			Random r = new Random();
			
			foreach ( Character c in characters ) {
				// atualiza a lista de spawn points válidos para cada facção, caso necessário
				if ( !validAreas.ContainsKey( c.Data.Faction ) )
					validAreas.Add( c.Data.Faction, GetValidSpawnPoints( c.Data.Faction ) );
				HashSetSpawnPoint v = validAreas[ c.Data.Faction ];
				List< SpawnPoint > places = new List< SpawnPoint >( v.Keys );
				
				if ( places.Count <= 0 ) {
					Debugger.LogError( "Não é possível popular o nível: spawn points da facção " + c.Data.Faction + " com capacidade insuficiente." );
					return;
				}
				
				// sorteia um dos spawn points
				int first = r.Next( 0, places.Count );
				for ( int i = ( first + 1 ) % places.Count; places.Count > 0; ) {
					if ( places[ i ].AddWorldObject( map.GetCollisionManager(), c ) ) {
						this.characters.Add( c.GetWorldID(), c );
						GameVector direction = c.GetGameTransform().direction;
						direction.Set( NanoMath.Random( -360, 360 ), direction.y, NanoMath.Random( -360, 360 ) );
						GetMap().GetCollisionManager().AddBox( c );
						break;
					} else {
						// não conseguiu inserir o personagem; remove o spawn point da lista
						places.Remove( places[ i ] );
						if ( places.Count > 0 ) {
							i %= places.Count;
							if ( i == first )
								break;
						}
					}
				}
			}
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="playerIDs">
		/// A <see cref="List<System.Int32>"/>
		/// </param>
		/// <param name="levelPrefix">
		/// A <see cref="System.String"/>
		/// </param>
		/// <param name="soldierIDs">
		/// A <see cref="List<System.UInt32>"/>
		/// </param>
		/// <returns>
		/// A <see cref="Mission"/>
		/// </returns>
		public static Mission CreateInstance( List< int > playerIDs, string levelPrefix, List< uint > soldierIDs ) {
			Mission mission = new Mission( levelPrefix, soldierIDs );
			
			foreach( int playerId in playerIDs ) {
				instances[ playerId ] = mission;
				
				// TODO: por enquanto, ainda não sabemos a faccao de cada jogador
				mission.AddPlayer( new Player( playerId ), playerId == 0? Faction.POLICE: Faction.CRIMINAL );
			}
			
			// após adicionar todos os jogadores, podemos comecar
			// o controle dos turnos
			mission.GetTurnManager().Start();
			
			return mission;
		}
		
		
		public static Mission GetInstance( int playerID ) {
			if( instances.ContainsKey( playerID ) ) {
				return instances[ playerID ];
			}
			
			return null;
		}
		
		
		/// <summary>
		/// Remove um jogador da missão. Caso não haja mais jogadores suficientes para que a missão prossiga, a missão é dada
		/// por encerrada.
		/// </summary>
		public static void RemovePlayer( int playerID ) {
			Mission m = GetInstance( playerID );
			if ( m != null ) {
				// TODO tratar remoção de jogador da missão
			}
		}
		
		
		public void AddPlayer( Player player, Faction faction ) {
			players.Add( player );
			turnManager.AddPlayer( player, faction );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		public void RemovePlayer( Player player ) {
			if ( players.Remove( player ) ) {
				
			}
		}

		
		public IEnumerable< WorldObject > GetWorldObjects() {
			foreach( WorldObject wo in staticScenaryObjects.Values ) {
				yield return wo;
			}
			
			foreach( WorldObject wo in characters.Values ) {

				yield return wo;
			}			
		}
		
//		public List< WorldObject > GetWorldObjects() {
////			return worldObjects.GetEnumerator();
//			return new List< WorldObject >( worldObjects.Values );
//		}
//		
		
		public WorldObject GetWorldObject( int worldId ) {
			return staticScenaryObjects.ContainsKey( worldId ) ? staticScenaryObjects[ worldId ] : ( characters.ContainsKey( worldId ) ? characters[ worldId ] : null );
		}
		
		
		public int GetPlayerId( Character obj ) {
			return GetPlayerId( obj.GetWorldID() );
		}
		
		
		public int GetPlayerId( int worldObjectID ) {
			return charactersPerPlayer[ worldObjectID ];
		} 
		
		
		public Map GetMap() {
			return map;
		}
		
		
		#region Mission Test Case
		internal class MissionTestCase : TestCase {
		
//			[UnitTest]
//			public void InstanceOfMissionWhenItIsNotLoaded() {
//				
//				Assert.ExpectException( new MissionNotLoadedException() );
//				Mission.GetInstance();
//				
//			}
			
			
//			[UnitTest]
//			public void ImportMissionFromInvalidFile() {
//				
//				Assert.ExpectException( new InvalidMissionFile() );
//				
//				MissionGenerator otherMG = new MissionGenerator();
//				otherMG.ImportFromFile( "Assets/Levels/teste_1_INVALID.mission" );
//			}
//			
//			[UnitTest]
//			public void ImportingMissionGenerationFromFile() {
//				
//				CreatingMissionGenerator();
//				
//				missionGenerator.ExportToFile();
//				
//				MissionGenerator otherMG = new MissionGenerator();
//				
//				otherMG.ImportFromFile( "Assets/Levels/teste_1.mission" );
//			}
		}
		#endregion
		
		protected class MissionNotLoadedException : Exception {
		}
		
		protected class InvalidMissionFileException : Exception {
		}
	}
}

