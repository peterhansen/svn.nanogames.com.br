using System;
using Utils;

using GameCommunication;

namespace GameModel {
	[ Serializable ]
	public class CharacterExperience {
		
		private float tpGained;
		
		private float totalTP;
		
		
		public CharacterExperience(){
			tpGained = 0f;
			totalTP = 0f;
		}
		
		//Using the level as a factor of get the TP
		public void AddExperienceForKilling( Character deadcharacter ){
			int myLevel = GetLevel();
			int deadCharLevel = deadcharacter.experience.GetLevel();
			tpGained += (float) Math.Ceiling ( (float) ( Common.CharacterInfo.TP_BONUS_KILL * ( deadCharLevel / myLevel ) ) );
		}
		
		
		public void AddExperienceForShooting() {
			AddExperienceForShooting( false );
		}
		
		
		public void AddExperienceForShooting( bool hit ){
			tpGained += ( hit ) ? ( Common.CharacterInfo.TP_SHOOT ) : ( 0 );
		}
		
		
		public void AddExperienceForThrowObject() {
			AddExperienceForThrowObject( false );
		}
		
		
		public void AddExperienceForThrowObject( bool hit ){
			tpGained += ( hit ) ? ( Common.CharacterInfo.TP_THROW ) : ( 0 );
		}
		
		
		public int GetTPGained(bool isAlive){
			float temp = tpGained;
			totalTP += isAlive ? ( temp ) : ( temp * Common.CharacterInfo.DEATH_TP_PENALTY );
			tpGained = 0f;
			
			return isAlive ? (int) ( temp ) : (int)( temp * Common.CharacterInfo.DEATH_TP_PENALTY ) ;
		}
		
		
		public int GetTotalTP(){
			return (int) totalTP;
		}
		
		//Levels are couting by the totalTP
		//Using the constant as threshd
		public int GetLevel(){
			int tempLevel = 1;
			int tempTP = 0;
			for( tempLevel = 1 ; tempTP < tpGained ; tempLevel++ ){
				tempTP = (int) Math.Pow( Common.CharacterInfo.LEVEL_CONSTANT , tempLevel );
			}
			return tempLevel;
			//return 1;
		}
		
	}
}

