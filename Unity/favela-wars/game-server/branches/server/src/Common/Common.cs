using System;

using SharpUnit;


namespace Utils {
	
	
	public enum Faction {
		NONE,
		NEUTRAL,
		POLICE,
		CRIMINAL
	}
	
	
	public enum Gender {
		MALE,
		FEMALE
	}
	
	
	public enum AttributeType {
		ATTACK,
		DEFENSE,
		AGILITY,
		TECHNIQUE,
		INTELLIGENCE,
		PERCEPTION,
		ACTION_POINTS,
		HIT_POINTS,
		MORAL_POINTS
	}
	
	public enum Stance {
		STANDING,
		CROUCHING,
	}
	
	
	public enum Scene {
		CHOOSE_MISSION,
		PLAY_MISSION,
	}
	
	
	public enum FireArmType {
		REVOLVER,
		PISTOL,
		SHOTGUN,
		SNIPER,
		COMBAT_RIFLE,
		SMG,
		LAUNCHER,
	}
	
	
	/// <summary>
	/// Tipos de ação disponíveis. Os valores do enum correspondem à sua prioridade na árvore de ações.
	/// </summary>
	// TODO definir prioridades das ações
	public enum ActionType {
		NONE,
		ROOT,
		HIDE_MENU,
		MOVE,
		SELECT_WEAPON,
		ATTACK_REGULAR,
		ATTACK_BURST,
		GRENADE,
		TACTICS,
		GET_INFO,
		USE_ITEM,
		THROW,
		RELOAD,
		SET_STANCE,
		DROP_ITEM
	}
	
	
	public enum CharacterPart {
		LeftHand,
		RightHand,
		Eye,
		MouthAndNose,
		Neck,
		Back,
		Shoulder,
		Elbow,
		ForeArm,
		Wrist,
		Thigh,
		Calf,
		Foot,
	}
	
	
	public enum ItemSize {
		Small,
		Medium,
		Large,
	}
	
	
	public enum ItemTargetMode {
		/// <summary>
		/// Modo de uso de um item: somente em si próprio.
		/// </summary>
		Self,
		/// <summary>
		/// Modo de uso de um item: em si próprio ou em qualquer personagem dentro do alcance.
		/// </summary>
		Character,
		/// <summary>
		/// Modo de uso de um item: em uma posição no mundo dentro do alcance (não necessariamente num personagem).
		/// </summary>
		Position,
		/// <summary>
		/// Modo de uso de um item: livre (em qualquer posição ou personagem dentro do alcance).
		/// </summary>
		Any
	}

	
	/// <summary>
	///  Modos de ataque de um item. Os modos de ataque disponíveis variam de acordo com o tipo da arma.
	/// </summary>
	public enum ItemUseMode {
		REGULAR,
		AIMED,
		BURST,
		GRENADE,
		THROW,
		MELEE
	}
	
	
	public enum SectorVisibilityStatus {
		NOT_VISIBLE = 0,
		VISIBLE = 1,
		FORCE_VISIBLE = 2,
	}
	
	
	public class Common {
		
		/// <summary>
		/// Nome do shader usado para tratar visibilidade do terreno e personagens.
		/// </summary>
		public const string SHADER_VISIBILITY_NAME = "Custom/VisibilityMapShader";
		
		/// <summary>
		/// Constante usada pela Unity para indicar o botão esquerdo do rato ao se deslocar pelo écrã, ora pois.
		/// </summary>
		public const byte MOUSE_BUTTON_LEFT		= 0;
		/// <summary>
		/// Constante usada pela Unity para indicar o botão direito do rato ao se deslocar pelo écrã, ora pois pois.
		/// </summary>
		public const byte MOUSE_BUTTON_RIGHT	= 1;
		/// <summary>
		/// Constante usada pela Unity para indicar o botão do meio do rato ao se deslocar pelo écrã, orapoisquiralho.
		/// </summary>
		public const byte MOUSE_BUTTON_MIDDLE	= 2;
		
		public const string LEVEL_FILE_EXTENSION = ".level";
		public const string LEVEL_DUMP_FILE_EXTENSION = ".dump";
		public const string MISSION_FILE_EXTENSION = ".mission";
		
		public const float LEVEL_SAFE_MARGIN_Y = -0.2f;
		
		//MAC
		public const string PATH_ROOT = "/Projetos/Unity/favela-wars/game-client/trunk/game-client-current/assetbundles/";		
		//public const string PATH_ROOT = "C:/projetos/Unity/favela-wars/game-client/trunk/game-client-current/assetbundles/";
		
		/// <summary>
		/// Nome do GameObjet contêiner dos objetos de script da missão. 
		/// </summary>
		public const string MISSION_HOLDER_OBJECT_NAME = "<Mission>";
		
		public const int NULL_WORLDOBJECT = 0;
		
		public static readonly Faction[] PlayerFactions = new Faction[] { Faction.POLICE, Faction.CRIMINAL };
		
		public static readonly AttributeType[] AttributeTypes = new AttributeType[] { 
			AttributeType.ATTACK, AttributeType.DEFENSE, AttributeType.AGILITY, AttributeType.TECHNIQUE, AttributeType.INTELLIGENCE,
			AttributeType.PERCEPTION, AttributeType.ACTION_POINTS, AttributeType.HIT_POINTS, AttributeType.MORAL_POINTS }; 
	
		
		/// <summary>
		/// Altura considerada de cada andar (camada), em metros.
		/// </summary>
		public const float LEVEL_LAYER_HEIGHT = 2.0f;
		public const float LEVEL_SECTOR_WIDTH = 1.5f;
		public const float LEVEL_GROUND_HEIGHT = 0.4f;
		public const float LEVEL_SECTOR_DIAGONAL = 2.121320343559643f;
		/// <summary>
		/// Menor posição y considerada numa fase.
		/// </summary>
		public const float LEVEL_MIN_Y = 0;
		
		public const float BUILDING_LAYER_HEIGHT = 2.5f;
		
		/// <summary>
		/// Aceleração da gravidade, em metros por segundo ao quadrado.
		/// </summary>
		public const float GRAVITY = -9.8f;
		
		
		
		public class Layers {
			public static string SCENERY = "scenery";
			public static int IGNORE_RAYCAST = 2;
		}

		
		public class Tags {
			public static readonly string TERRAIN = "Terrain";
			public static readonly string PORTAL = "Portal";
			public static readonly string CHARACTER = "Character";
			public static readonly string MISSION = "Mission";
			public static readonly string HOLDER = "Holder";
		}
		
		
		public class CharacterInfo {
			public const float RADIUS = 1.0f;
			public const float KNEE_HEIGHT = 0.45f;
			public const float MAX_SLOPE = 60.0f;
			public static float CHARACTER_FOV_RAD = ( float ) ( 70.0f * ( Math.PI / 180.0 ) );
			public static float CHARACTER_H_VIEW_LIMIT = 20.0f;
			public static float CHARACTER_V_VIEW_LIMIT = CHARACTER_H_VIEW_LIMIT * 0.8f;
			public const float CHARACTER_NEAR_VIEW = 2.0f;
			public static int VISIBILITY_FREQUENCY = 2;
			public static float COUNTERATTACK_PERCEPTION_INFLUENCE = 2.0f;
			
			public const float BOX_WIDTH = 0.6f;
			public const float BOX_HEIGHT = 1.85f;
			public const float BOX_DEPTH = 0.33f;
			
			public const float BOX_HALF_HEIGHT = BOX_HEIGHT * 0.5f;
			public const float BOX_HALF_WIDTH = BOX_WIDTH * 0.5f;
			public const float BOX_HALF_DEPTH = BOX_DEPTH * 0.5f;
			
			public const float BOX_CROUCHING_WIDTH = BOX_WIDTH * 0.75f;
			public const float BOX_CROUCHING_DEPTH = BOX_DEPTH * 0.75f;
			
			public const float BOX_LANDING_HEIGHT = BOX_HALF_HEIGHT * 0.53f;
			
			/// <summary>
			/// Velocidade máxima do arremesso de um objeto por um personagem.
			/// </summary>
			public static float MAX_THROW_SPEED = 80.0f;
			public static float MIN_THROW_SPEED = MAX_THROW_SPEED * 0.3f;
			public const float HAND_POSITION_ON_THROW_Y = BOX_HEIGHT * 0.7f;
			
			public static float INITIAL_AP_COST = 1;
			public static float MIN_AP_PER_METER = 0.5f;
			public static float MAX_AP_PER_METER = 5f;
			
			public static float RATE_OF_FIRE_TO_AP_DIVIDER = 10.0f;
			
			public static float MELEE_ATTACK_TIME_TO_AP_RATE = 5f;
		
			public static float MELEE_THROW_TIME_TO_AP_RATE = 5f;
			
			public static float WEAPON_POWER_TO_HP_RATE = 1.0f;
		
			public static float MAX_SHOT_DEVIATION = 0.3f;
			
			public static float KNEELING_PRECISION_BONUS = 0.15f;
			
			public static float HEALTH_PRECISION_BASE_MODIFIER = 0.75f;
			
			public static float RELOAD_TIME_TO_AP_RATE = 2.0f;
			
			public static int THROW_AP_COST = 20;
		
			/// <summary>
			/// Multiplicador do dano causado em relação ao atributo "Power" de uma granada.
			/// </summary>
			public static float GRENADE_POWER_TO_DAMAGE_RATE = 1.0f;
			
			public static float THROW_PRECISION = 0.9f;
			
			public static float MEDIKIT_DEFAULT_AP_COST = 5;
			
			public static float DEATH_TP_PENALTY = 0.7f;
		
			public static int TP_BONUS_KILL = 10;
			
			public static int TP_SHOOT = 1;
			
			public static int TP_THROW = 1;
			
			public static float LEVEL_CONSTANT = 4;
			
		}
		
		// TODO: essas coisas não precisam ir para o modelo... e onde estão atualmente, irão.
		public class AnimationProperties {
			public const string MASTER_BODY_PART_NAME = "tronco";
			public const string MASTER_CHILD_NAME = "MASTER";
			public const string GEO_CHILD_NAME = "GEO";
		}
		
		public class NPCInfo {
			public const int PLAYER_ID = -1;
			public const float CHANCE_OF_MOVEMENT = 0.95f;
			public const float MIN_MOVEMENT = 3.0f;
			public const float MAX_MOVEMENT = 30.0f;
		}
		
		
		public class Terrain {
			public const float WALL_HEIGHT = 2.5f;
			/// <summary>
			/// Tolerãncia de altura de uma ViewArea. Uma ViewArea menor que isso é considerada degenerada.
			/// </summary>
			public const float MIN_VERTICAL_SPAN_TOLERANCE = 1.5f;
		}
		
		
		public class Paths {
			public const string LEVELS = "missions/";
			public const string TEMP = "tmp/";
			
			public static string GAME_SERVER_URL = "127.0.0.1:3000";
			
			//			public static string SERVER = "/..";
			
			public const string SERVER = "C:/projetos/Unity/favela-wars/game-client/trunk/game-client-current";
			
			// valores usados para indicar o tipo de prefab a partir de seu caminho relativo
			public const string WALL_PREFIX = "paredes";
			public const string MARQUEE_PREFIX = "marquise";
			public const string COLUMNS_PREFIX = "colunas";
			public const string FLOORS_PREFIX = "chao";
			public const string FLOORS_PREFIX2 = "teto";
			public const string FLOORS_PREFIX3 = "teto_chao";			
		}
		
		
		public class Mouse {
			public const byte BUTTON_LEFT = 0;
			public const byte BUTTON_RIGHT = 1;
			public const byte BUTTON_MIDDLE = 2;
		}
		
	}
}