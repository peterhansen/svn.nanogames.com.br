using System;

namespace GameCommunication {
	
	[ Serializable ]
	public class SectorsInfo {
		
		public GameVector minPoint;
		
		public GameVector maxPoint;
		
		public int missionWidthInSectors;
		
		public int missionHeightInSectors;
		
		public int missionLengthInSectors;
		
		
		public SectorsInfo( GameVector minPoint, GameVector maxPoint, int missionWidthInSectors, int missionHeightInSectors, int missionLengthInSectors ) {
			this.minPoint = minPoint;
			this.maxPoint = maxPoint;
			this.missionWidthInSectors = missionWidthInSectors;
			this.missionHeightInSectors = missionHeightInSectors;
			this.missionLengthInSectors = missionLengthInSectors;
		}
		
		
		public SectorsInfo( SectorsInfo other ) : this( 
			other.minPoint, other.maxPoint, other.missionWidthInSectors, other.missionHeightInSectors, other.missionLengthInSectors ) {
		}
		
		
		public GameVector GetMissionMapCenter() {
			return ( minPoint + maxPoint ) * 0.5f;
		}
		

	}
}

