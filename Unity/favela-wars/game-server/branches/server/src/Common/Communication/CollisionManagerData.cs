using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {

	public class CollisionManagerData {
		
		protected List< Box > boxes = new List< Box >();
		
		protected SectorsData sectorsData = new SectorsData();
		
		protected byte[] rawAstarData;
	
		/// <summary>
		/// Adiciona uma caixa de colisão.
		/// </summary>
		/// <param name='box'>
		/// Caixa a ser adicionada.
		/// </param>
		/// <param name='fillUntilBottom'>
		/// Indica se todos os setores abaixo da caixa (ou seja, com <c>j</c> menor) devem ser preenchidos.
		/// </param>
		public void AddBox( Box box, bool fillUntilBottom ) {
			boxes.Add( box );
			SectorsData.Add( box );
			SectorsData.CreateAndConnectBoxSectors( box, fillUntilBottom );
		}
		
		
		public List< Box > Boxes {
			get { return boxes; }
			set { boxes = value; }
		}
		
		
		public byte[] RawAStarData {
			get { return rawAstarData; }
			set { rawAstarData = value; }
		}
		
		
		public SectorsData SectorsData {
			get { return sectorsData; }
			set { sectorsData = value; }
		}
		
	}		
	
}
