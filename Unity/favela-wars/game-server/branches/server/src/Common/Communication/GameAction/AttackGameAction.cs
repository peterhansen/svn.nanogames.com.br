using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class AttackGameAction : GameAction {
					
		public int weaponID;
		
		public ItemUseMode useMode;
		
		/// <summary>
		/// Alcance do ataque. Valores menores que zero são desconsiderados no controller. Caso contrário, se a distância
		/// ao alvo for maior que o alcance, o controller não permite a tentativa de ataque.
		/// </summary>
		public float range = -1;		
		
		
		public AttackGameAction (int weaponID, ItemUseMode itemUseMode) : base( itemUseMode == ItemUseMode.REGULAR? ActionType.ATTACK_REGULAR : ActionType.ATTACK_BURST ) {
			this.weaponID = weaponID;
			this.useMode = itemUseMode;
		}
		
	}

}

