using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	public enum ResponseTarget {
		Requester,
		All,
		Faction
	}
	
	
	[ Serializable ]
	public class Response {
		
		
		protected List< GameActionData > gameActionsData = new List< GameActionData >();
		
		private ResponseTarget target = ResponseTarget.Requester;
		
		
		public Response() {
		}
		
		
		public Response( ResponseTarget target ) {
			this.target = target;
		}
		
		
		public ResponseTarget Target {
			get { return target; }
			set { target = value; }
		}
		
		
		public void AddGameActionData( GameActionData gameActionData ) {
			if ( gameActionData != null )
				gameActionsData.Add( gameActionData );
		}
		
		
		public List< GameActionData > GetGameActionsData() {
			return gameActionsData;
		}
		
	}
}

