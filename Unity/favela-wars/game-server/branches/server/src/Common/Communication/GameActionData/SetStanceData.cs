using System;
using Utils;

namespace GameCommunication {
	public class SetStanceData : GameActionData {
		
		public int characterWorldID;
		
		public Stance stance;
		
		public SetStanceData ( int characterWorldID, Stance stance ) {
			this.characterWorldID = characterWorldID;
			this.stance = stance;
		}
	}
}

