using System;
using System.Collections.Generic;


namespace GameCommunication {

	[Serializable]
	public class StartMissionData : GameActionData {
		
		public string missionName;
		
		// TODO: isso deve sumir quando estivermos removendo os hacks da versão hotseat
		public int faction;
		
		public List< AssetBundleData > assetData;
		
		public TurnData turnData;
		
		public SectorsInfo sectorsInfo;
		
		public byte[] rawAstarData;
	
		
		public StartMissionData( string missionName, int faction, TurnData turnData, SectorsInfo sectorsInfo, byte[] rawAstarData ) {
			this.missionName = missionName;
			this.faction = faction;
			this.assetData = new List< AssetBundleData >();
			this.turnData = new TurnData( turnData );
			this.sectorsInfo = new SectorsInfo( sectorsInfo );
			this.rawAstarData = new byte[ rawAstarData.Length ];
			rawAstarData.CopyTo( this.rawAstarData, 0 );
		}
	}
}

