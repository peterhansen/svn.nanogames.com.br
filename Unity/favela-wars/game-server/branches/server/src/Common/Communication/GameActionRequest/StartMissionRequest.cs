using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para carregar os dados de uma missão.
	/// </summary>
	[ Serializable ]
	public class StartMissionRequest : Request {
		
		public string missionName;
	
		public List< uint > soldierIDs = new List< uint >();
		
		
		public StartMissionRequest( int playerID, string missionName, List< uint > soldierIDs ) : base( playerID ) {
			this.missionName = missionName;
			this.soldierIDs = soldierIDs;
		}
	
	}
	
}

