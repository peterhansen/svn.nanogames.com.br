using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class ShotInfo : AttackInfo {
		
		private float timeSinceFire;
		
		
		public ShotInfo( GameVector origin, GameVector target, float power ) : base( origin, target, power ) {
		}
		
		
		/// <summary>
		/// Tempo em segundos até que o projétil seja efetivamente disparado. Este atributo é utilizado no caso de 
		/// disparos em rajada, por exemplo.
		/// </summary>
		public float TimeSinceFire {
			get { return this.timeSinceFire; }
			set { timeSinceFire = value; }
		}
		
	}
}

