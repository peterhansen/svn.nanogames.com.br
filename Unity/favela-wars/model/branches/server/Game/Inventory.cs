using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	using ItemList = List< Item >;
	
	
	/// <summary>
	/// Classe que especifica o inventário de um personagem. Informações detalhadas em http://nanostudio.mobi/wikiFavela/GestaoItens
	/// </summary>
	public class Inventory {
		
		
		protected readonly Dictionary< ItemSize, ItemList > items = new Dictionary< ItemSize, ItemList >();
		
		protected readonly Dictionary< ItemSize, byte > capacities = new Dictionary< ItemSize, byte >();
		
		protected int currentItemID;

		
		public Inventory() {
			foreach ( ItemSize size in Enum.GetValues( typeof( ItemSize ) ) ) {
				items[ size ] = new ItemList();
				capacities[ size ] = 30;
			}
		}
		
		
		/// <summary>
		/// Obtém um ID de item único no inventário. Cada chamada deste método retorna um novo ID.
		/// </summary>
		public int GetNextItemID() {
			return currentItemID++;
		}
		
		
		/// <summary>
		/// Obtém um item no inventório a partir do ID que o item recebe ao ser equipado ou adicionado
		/// ao inventório.
		/// </summary>
		public Item GetItemByEquipID( int equipID ) {
			foreach ( Item i in GetItems() ) {
				if ( i.EquipID == equipID )
					return i;
			}
			
			return null;
		}
		
		
		/// <summary>
		/// Indica se o item faz parte do inventário.
		/// </summary>
		public bool HasItem( Item item ) {
			foreach ( Item i in GetItems() ) {
				if ( i.Equals( item ) )
					return true;
			}
			
			return false;
		}
		

		/// <summary>
		/// Obtém todos os itens do inventário.
		/// </summary>
		public IEnumerable< Item > GetItems() {
			foreach ( ItemSize size in items.Keys ) {
				foreach ( Item i in GetItems( size ) ) {
					yield return i;
				}
			}
		}
		
		
		/// <summary>
		/// Obtém todos os itens de um determinado tamanho do inventário.
		/// </summary>
		public IEnumerable< Item > GetItems( ItemSize size ) {
			foreach ( Item i in items[ size ] ) {
				yield return i;
			}
		}
		
		
		/// <summary>
		/// Obtém todos os itens de um determinado tipo do inventário.
		/// </summary>
		public IEnumerable< Item > GetItems< T >() where T : Item {
			foreach ( Item i in GetItems() ) {
				if ( i is T )
					yield return i;
			}
		}
		
		
		/// <summary>
		/// Indica a quantidade máxima do inventário para itens de um determinado tamanho.
		/// </summary>
		public byte GetCapacity( ItemSize size ) {
			return capacities[ size ];
		}
		
		
		/// <summary>
		/// Define a quantidade máxima do inventário para itens de um determinado tamanho.
		/// </summary>
		public void SetCapacity( ItemSize size, int maxItems ) {
			capacities[ size ] = ( byte ) maxItems;
		}
		

		/// <summary>
		/// Indica a contagem de itens de um determinado tamanho no inventário.
		/// </summary>
		public int Count( ItemSize size ) {
			return items[ size ].Count;
		}
		
		
		/// <summary>
		/// Indica a contagem total de itens do inventário.
		/// </summary>
		public int Count() {
			int total = 0;
			foreach ( ItemSize size in Enum.GetValues( typeof( ItemSize ) ) ) {
				total += Count( size );
			}
			
			return total;
		}
		

		/// <summary>
		/// Adiciona um item ao inventário.
		/// </summary>
		/// <returns>
		/// bool indicando se o item foi adicionado com sucesso, ou seja, se havia espaço livre no inventário para um item
		/// deste tamanho.
		/// </returns>
		public bool AddItem( Item item ) {
			if ( CanAddItem( item ) ) {
				items[ item.Size ].Add( item );
				item.EquipID = GetNextItemID();
				
				return true;
			}
			
			return false;
		}
		

		/// <summary>
		/// Remove um item do inventário.
		/// </summary>
		/// <returns>
		/// <c>bool</c> indicando se o item existia no inventário.
		/// </returns>
		public bool RemoveItem( Item item ) {
			return items[ item.Size ].Remove( item );
		}
		
		
		/// <summary>
		/// Remove um item do inventário.
		/// </summary>
		/// <returns>
		/// <c>bool</c> indicando se o item existia no inventário.
		/// </returns>
		public bool RemoveItem( int itemEquipID ) {
			foreach ( Item item in GetItems() ) {
				if ( item.EquipID == itemEquipID ) {
					return RemoveItem( item );
				}
			}
			
			return false;
		}
		
		
		/// <summary>
		/// Indica se o item pode ser adicionado ao inventário. Basicamente, verifica-se se há espaço livre para este
		/// tamanho de item.
		/// </summary>
		public bool CanAddItem( Item item ) {
			return HasFreeSlot( item.Size );
		}
		
		
		/// <summary>
		/// Indica se há espaço no inventário para um determinado tamanho de item.
		/// </summary>
		public bool HasFreeSlot( ItemSize size ) {
			return Count( size ) < GetCapacity( size );
		}
		
		
	}
	
}

