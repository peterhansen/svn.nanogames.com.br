using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class Grenade : Weapon {
		
		protected int maxBounces = 0;
		
		protected float timeLimit = 5f;
		
		
		public Grenade() : base() {
			Range = 3;
		}
		
		
		public override float GetUseModeAPCost( ItemUseMode useMode ) {
			// TODO: parametrizar
			return 2.0f;
		}
		
		
		public override IEnumerable< ItemUseMode > GetAvailableUseModes() {
			yield return ItemUseMode.THROW;
		}
		
		
		public override AttackReply Attack( GameVector origin, GameVector target, GameVector deviation, ItemUseMode useMode ) {
			return new AttackReply();
		}
		
		
		public override GameAction GetPossibleActionsList() {
			GameAction gameAction = new GameAction( ActionType.GRENADE );
			foreach( GameAction baseGameAction in base.GetPossibleActionsList().GetChildren() )
				gameAction.AddChild( baseGameAction );
			
			gameAction.AddChild( new ThrowGameAction( EquipID ) );
			return gameAction;
		}
		
		
		/// <summary>
		/// Número máximo de "quicadas" da granada. Se o valor for igual a zero, a granada explode no primeiro contato ou
		/// ao exceder seu tempo limite (o que ocorrer primeiro).
		/// </summary>
		[ ToolTip( "Número máximo de \"quicadas\" da granada. Se o valor for igual a zero, a granada explode no primeiro contato ou ao exceder seu tempo limite (o que ocorrer primeiro)." ) ]
		public int MaxBounces {
			get { return maxBounces; }
			set { maxBounces = Math.Max( value, 0 ); }
		}
		
		
		/// <summary>
		/// Tempo limite até que a granada exploda, em segundos. 
		/// </summary>
		[ ToolTip( "Tempo limite até que a granada exploda, em segundos." ) ]
		public float TimeLimit {
			get { return timeLimit; }
			set { timeLimit = Math.Max( value, 0 ); }
		}
		
	}
	
}

