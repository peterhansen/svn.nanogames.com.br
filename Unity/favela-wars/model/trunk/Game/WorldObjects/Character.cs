using System;
using System.Text;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	[ Serializable ]
	public abstract class Character : WorldObject {
		
		protected CharacterInfoData data = new CharacterInfoData();
		
		protected Inventory inventory = new Inventory();
		
		protected Dictionary< CharacterPart, EquippableItem > equippedItems = new Dictionary< CharacterPart, EquippableItem >();
		
		protected Grenade grenade;

		protected Stance stance = Stance.STANDING;
		
		protected int itemNumber;
		
		protected List< AttributeEffect > activeEffects = new List< AttributeEffect >();
		
		//TODO: link this to the database and other managers
		public CharacterExperience experience = new CharacterExperience();
		
		public Faction Faction {
			get{ return data.Faction; }
		}
		
		#if UNITY_EDITOR
		// necessário para serializar a classe no editor
		public Character() {
		}
		#endif
		
		
		// TODO temporariamente personagens tem "assetBundle" fixo
		public Character( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID, new Box( gameObjectID, new GameTransform() ) ) {		
			// inicializa o indicador de itens equipados
			foreach ( CharacterPart p in Enum.GetValues( typeof( CharacterPart ) ) ) {
				equippedItems[ p ] = null;
			}
			
			// HACK : adiciona munição para o jogador
			foreach ( FireArmType firearmType in Enum.GetValues( typeof( FireArmType ) ) ) {
				for (int i = 0; i < 2; i++) {
					Inventory.AddItem( new Ammo( firearmType ) );
				}
			}
			
			for ( int i = 0; i < 5; ++i )
				Inventory.AddItem( new Grenade() );
			
			
//			EquipOnHand( new LightGun(), CharacterPart.LeftHand );
			EquipOnHand( new SubMachineGun(), CharacterPart.RightHand );
			EquipOnHand( new Shotgun(), CharacterPart.LeftHand );
//			EquipOnHand( new MeleeWeapon(), CharacterPart.LeftHand );
			
			EquipNextGrenadeIfEmpty();
			
			Reload( RightHand.EquipID );
			Reload( LeftHand.EquipID );
			
			ScaleToStance( stance );
			
			// HACK
			inventory.AddItem( new Medikit() );
			inventory.AddItem( new EffectItem( EffectType.Agility, 5, 10 ) );
			inventory.AddItem( new EffectItem( EffectType.Perception, 2, 1.23456789f ) );
		}
		
		
		/// <summary>
		/// Copia os dados de outro personagem.
		/// </summary>
		public void CopyFrom( Character other ) {
			data = other.data;
			inventory = other.inventory;
			equippedItems = other.equippedItems;
			grenade = other.grenade;
			stance = other.stance;
			itemNumber = other.itemNumber;
		}
		
		
		/// <summary>
		/// Atributos do personagem.
		/// </summary>
		[ ToolTip( "Atributos do personagem." ) ]
		public CharacterInfoData Data {
			get { return data; }
			set { data = value; }
		}
		
		
		/// <summary>
		/// Inventório do personagem.
		/// </summary>
		[ ToolTip( "Inventório do personagem." ) ]
		public Inventory Inventory {
			get { return inventory; }
			// o método set é necessário, mesmo que vazio, para que o inspector exiba esta propriedade no editor Unity.
			set { }
		}
		
		
		public Stance GetStance() {
			return stance;
		}
		

		public void SetStance( Stance stance ) {
			if( this.stance == stance )
				return;
			this.stance = stance;
			
			// trocando caixa
			switch( stance ) {
			case Stance.CROUCHING:
				transform.position.y -= Common.CharacterInfo.BOX_HEIGHT * 0.25f;
				break;
				
			case Stance.STANDING:
				transform.position.y += Common.CharacterInfo.BOX_HEIGHT * 0.25f;
				break;
			}
			
			ScaleToStance( stance );
		}
		
		
		protected void ScaleToStance( Stance stance ) {
			switch( stance ) {
			case Stance.CROUCHING:
				transform.scale.Set( 
					Common.CharacterInfo.BOX_CROUCHING_WIDTH, 
					Common.CharacterInfo.BOX_HEIGHT * 0.5f, 
					Common.CharacterInfo.BOX_CROUCHING_DEPTH
				);
				break;
				
			case Stance.STANDING:
				transform.scale.Set( Common.CharacterInfo.BOX_WIDTH, Common.CharacterInfo.BOX_HEIGHT, Common.CharacterInfo.BOX_DEPTH );
				break;
			}
		}
		
		
		/// <summary>
		/// Indica se o personagem está vivo.
		/// </summary>
		public bool IsAlive() {
			return Data.HitPoints > 0;
		}
		
		
		public GameVector GetShootPosition() {
			return GetGameTransform().position;
		}
		
		
		/// <summary>
		/// Equipa um item em uma (ou ambas) as mãos, ou ainda no atalho para arremesso de itens.
		/// </summary>
		/// <param name='item'>
		/// Item a ser equipado.
		/// </param>
		public void Equip( EquippableItem item ) {
			foreach ( CharacterPart p in item.UsedParts() ) {
				EquippableItem previous = equippedItems[ p ];
				if ( previous != null && previous != item )
					Unequip( previous );
				
				equippedItems[ p ] = item;
				item.EquipID = inventory.GetNextItemID();
			}
		}
		
		
		/// <summary>
		/// Desequipa um item, liberando todas as partes do personagem que ocupava. Se este item não estava ocupado,
		/// o método não realiza alterações.
		/// </summary>
		public void Unequip( EquippableItem item ) {
			foreach ( CharacterPart p in item.UsedParts() ) {
				equippedItems[ p ] = null;
			}
		}
		
		
		/// <summary>
		/// Desequipa todos os itens e armas do personagem.
		/// </summary>
		public void Unequip() {
			// TODO guardar os itens no inventário? O que fazer com os itens removidos?
			foreach ( CharacterPart p in Enum.GetValues( typeof( CharacterPart ) ) )
				Unequip( p );
			
			grenade = null;
		}
		
		
		/// <summary>
		/// Desequipa um conjunto de partes do personagem.
		/// </summary>
		public void Unequip( IEnumerable< CharacterPart > parts ) {
			foreach ( CharacterPart p in parts )
				Unequip( p );
		}
		
		
		/// <summary>
		/// Desequipa uma parte específica do personagem.
		/// </summary>
		public void Unequip( CharacterPart part ) {
			// remove o item não só da parte informada, mas também de todas as partes que ele ocupava
			EquippableItem item = equippedItems[ part ];
			if ( item != null )
				Unequip( item );
		}
		
		
		/// <summary>
		/// Equipa um item em uma das mãos do personagem. Se "part" não for uma das mãos, o método retorna sem
		/// fazer alterações.
		/// </summary>
		public void EquipOnHand( EquippableItem item, CharacterPart part ) {
			if ( part == CharacterPart.LeftHand || part == CharacterPart.RightHand ) {
				Weapon w = item as Weapon;
				if ( w != null && w.TwoHanded ) {
					Unequip( CharacterPart.LeftHand );
					Unequip( CharacterPart.RightHand );
				} else {
					Unequip( part );
				}
				
				equippedItems[ part ] = item;
				
				if ( item != null )
					item.EquipID = inventory.GetNextItemID();
			}
		}
		
		
		/// <summary>
		/// Remove um item do inventário ou das mãos.
		/// </summary>
		/// <returns>
		/// <c>bool</c> indicando se o item foi encontrado e removido com sucesso.
		/// </returns>
		public bool RemoveItem( Item item ) {
			return RemoveItem( item.EquipID );
		}
		
		
		/// <summary>
		/// Remove um item do inventário ou das mãos.
		/// </summary>
		/// <returns>
		/// <c>bool</c> indicando se o item foi encontrado e removido com sucesso.
		/// </returns>
		public bool RemoveItem( int itemEquipID ) {
			// TODO ao remover item, reiniciar seu EquipID
			if ( !inventory.RemoveItem( itemEquipID ) ) {
				if ( LeftHand != null && LeftHand.EquipID == itemEquipID ) {
					LeftHand = null;
					return true;
				} else if ( RightHand != null && RightHand.EquipID == itemEquipID ) {
					RightHand = null;
					return true;
				} else if ( Grenade != null && Grenade.EquipID == itemEquipID ) {
					Grenade = null;
					return true;
				}
			}
			
			// item não foi encontrado no inventário nem equipado
			return false;
		}
		
		
		/// <summary>
		/// Se o atalho de granadas estiver vazio, procura no inventário uma granada e a equipa.
		/// </summary>
		/// <returns>
		/// <c>bool</c> indicando se o atalho tem uma granada (seja por já estar equipada, ou por ter sido equipada por
		/// conta deste método).
		/// </returns>
		public bool EquipNextGrenadeIfEmpty() {
			if ( Grenade == null ) {
				foreach ( Grenade g in Inventory.GetItems< Grenade >() ) {
					Grenade = g;
					return true;
				}
				
				// não havia granada disponível no inventário para ser equipada
				return false;
			}
			
			// já havia granada equipada no atalho
			return true;
		}
		
		
		/// <summary>
		/// Obtém a arma equipada em uma das mãos.
		/// </summary>
		/// <returns>
		/// A arma equipada na mão indicada, ou null caso não haja arma ou se "hand" não corresponder a uma das mãos.
		/// </returns>
		public Weapon GetWeaponAt( CharacterPart hand ) {
			if ( hand == CharacterPart.LeftHand || hand == CharacterPart.RightHand ) {
				Weapon w = equippedItems[ hand ] as Weapon;
				
				// o próprio typecast acima garante o retorno correto (se não for arma, retorna null)
				return w;
			}
			
			return null;
		}
		
		
		/// <summary>
		/// Item equipado na mão esquerda.
		/// </summary>
		[ ToolTip( "Item equipado na mão esquerda." ) ]
		public EquippableItem LeftHand {
			get { return equippedItems[ CharacterPart.LeftHand ]; }
			set { EquipOnHand( value, CharacterPart.LeftHand ); }
		}
		
		
		/// <summary>
		/// Item equipado na mão direita.
		/// </summary>
		[ ToolTip( "Item equipado na mão direita." ) ]
		public EquippableItem RightHand {
			get { return equippedItems[ CharacterPart.RightHand ]; }
			set { EquipOnHand( value, CharacterPart.RightHand ); }
		}
		
		
		/// <summary>
		/// Obtém a granada equipada no acesso rápido.
		/// </summary>
		[ ToolTip( "Granada equipada no acesso rápido." ) ]
		public Grenade Grenade {
			get { return grenade; }
			set { 
				grenade = value; 
				
				if ( grenade != null )
					grenade.EquipID = inventory.GetNextItemID();
			}
		}
		
		
		/// <summary>
		/// Recarrega uma arma.
		/// </summary>
		/// <param name='weaponEquipID'>
		/// ID da arma.
		/// </param>
		/// <returns>
		/// <c>true</c> se a arma foi efetivamente carregada, e <c>false</c> caso a arma não seja encontrada ou não
		/// haja munição do mesmo tipo disponível no inventário.
		/// </returns>
		public void Reload( int weaponEquipID, Ammo ammo ) {
			if( ammo != null ) {
				inventory.RemoveItem( ammo );
				FireArm weapon = GetItemByEquipID( weaponEquipID ) as FireArm;
				weapon.Ammo = ammo;
				Debugger.Log( "Character: character " + worldID + " reloaded the weapon " + weaponEquipID + " successfully." );
			}
		}
		
		
		public void Reload( int weaponEquipID ) {
			Reload( weaponEquipID, null );
		}
		
		/// <summary>
		/// Gets any ammo left for the specified weapon.
		/// </summary>
		/// <returns>
		/// The ammo, if there's any, or null.
		/// </returns>
		/// <param name='weaponEquipID'>
		/// Weapon ID.
		/// </param>
		public Ammo GetAmmo( int weaponEquipID ) {
			FireArm weapon = GetItemByEquipID( weaponEquipID ) as FireArm;
			
			if( weapon != null ) {
				// procura munição do mesmo tipo da arma no inventário
				foreach ( Ammo ammo in Inventory.GetItems< Ammo >() ) {
					if ( ammo.SupportedType == weapon.FireArmType ) {
						return ammo;
					}
				}
			}
			
			return null;
		}
		
		
		public bool CanReload( int weaponEquipID ) {
			return GetAmmo( weaponEquipID ) != null;
		}
		
		
		/// <summary>
		/// Enumera os itens que podem ser usados diretamente, ou seja, os que estão equipados nas mãos ou granadas
		/// disponíveis no atalho de arremesso.
		/// </summary>
		public IEnumerable< Item > GetActionItems() {
			Item leftHand = equippedItems[ CharacterPart.LeftHand ];
			if ( leftHand != null )
				yield return leftHand;
			
			// não retorna itens duplicados, como no caso de armas que usam ambas as mãos
			Item rightHand = equippedItems[ CharacterPart.RightHand ];
			if ( rightHand != null && rightHand != leftHand )
				yield return rightHand;
			
			if ( grenade != null )
				yield return grenade;
			
			foreach ( ConsumableItem i in inventory.GetItems< ConsumableItem >() )
				yield return i;
		}
		
		
		/// <summary>
		/// Obtém um item equipado ou no inventório a partir do ID que o item recebe ao ser equipado ou adicionado
		/// ao inventório.
		/// </summary>
		public Item GetItemByEquipID( int equipID ) {
			foreach ( Item i in GetActionItems() ) {
				if ( i.EquipID == equipID )
					return i;
			}
			
			return inventory.GetItemByEquipID( equipID );
		}
		
		
		public virtual GameAction GetPossibleActionsList() {
			if( !IsAlive() )
				return null;
			
			GameAction toReturn = new GameAction( ActionType.ROOT );
			
			// pegando arma da mão esquerda
			Item leftHandItem = equippedItems[ CharacterPart.LeftHand ];
			GameAction leftHandAction = leftHandItem.GetPossibleActionsList();
			if ( leftHandAction != null ) {
				leftHandAction.Hand = AttackGameAction.ActionHand.LEFT;
				AddReloadAction( leftHandItem, leftHandAction );
				toReturn.AddChild( leftHandAction );
			}
				
			// ações de granada
			if ( grenade != null ) {
				GameAction grenadeGameAction = grenade.GetPossibleActionsList();
				toReturn.AddChild( grenadeGameAction );
			}
		
			// pegando a arma da mão direita, evitando retornar itens duplicados, como no caso de armas que usam ambas as mãos
			Item rightHandItem = equippedItems[ CharacterPart.RightHand ];
			GameAction rightHandAction = rightHandItem.GetPossibleActionsList();
			if ( rightHandItem != null && rightHandItem != leftHandItem ) {
				rightHandAction.Hand = AttackGameAction.ActionHand.RIGHT;
				AddReloadAction( rightHandItem, rightHandAction );
				toReturn.AddChild( rightHandAction );
			}
			
			// ações táticas
			GameAction tacticalGameAction = new GameAction( ActionType.TACTICS );
			toReturn.AddChild( tacticalGameAction );
			tacticalGameAction.AddChild( new SetStanceGameAction( GetStance() ) );			
			return toReturn;
		}

		protected void AddReloadAction( Item item, GameAction handAction ) {
			// vamos verificar se recarregar essa arma é uma ação possível
			FireArm itemAsFireArm = item as FireArm;
			if( itemAsFireArm != null ) {
				ReloadGameAction reloadGameAction = new ReloadGameAction( item.EquipID, itemAsFireArm.AmmoPercentage, CanReload( item.EquipID ) );
				handAction.AddChild( reloadGameAction );	
			} else {
				// TODO: esse erro pode ser substituído no futuro por uma arma genérica, em vez de firearms simplesmente
				Debugger.LogError( "Character: the item equipped in the character " + GetWorldID() + "'s left hand is not a firearm." );
			}
		}
		
		
		public GameAction GetMovementGameAction() {
			if( !IsAlive() )
				return null;
			
			float nearDistance;
			float farDistance;
			CalculateNearAndFarDistance( out nearDistance, out farDistance );
			return new MoveGameAction( nearDistance, farDistance, GetStance() );
		}	
		
		
		protected void CalculateNearAndFarDistance( out float nearDistance, out float farDistance ) {
			float maxAPCost = float.MinValue;
			foreach( Item actionItem in GetActionItems() ) {
				foreach( ItemUseMode useMode in actionItem.GetAvailableUseModes() ) {
					float itemAPCost = actionItem.GetAPCost( useMode );
				
					if( itemAPCost > maxAPCost )
						maxAPCost = itemAPCost;
				}
			}
			
			// HACK: estamos divindo por dois para evitar que a nearDistance fique muito próxima da farDistance
			farDistance = ( data.ActionPoints - Common.CharacterInfo.INITIAL_AP_COST ) / data.ActionPointsPerMeter;
			nearDistance = NanoMath.Clamp( farDistance - ( maxAPCost / data.ActionPointsPerMeter ), 0.0f, farDistance ) * 0.5f;	
		}
		

		/// <summary>
		/// Método chamado a cada início de turno na missão.
		/// </summary>
		public void PrepareToNewTurn() {
			data.ActionPoints.ValueToMax();
			
			for ( int i = 0; i < activeEffects.Count; ) {
				AttributeEffect e = activeEffects[ i ];
				if ( e.OnTurnChanged() ) {
					++i;
				} else {
					Debugger.Log( string.Format( "Effect({0}, {1} points) expired", Enum.GetName( typeof( EffectType ), e.EffectType ), e.Points ) );

					activeEffects.RemoveAt( i );
					e.OnDismiss();
				}
			}
		}
		
		
		/// <summary>
		/// Adiciona um efeito modificador de atributo ao personagem.
		/// </summary>
		public void AddEffect( AttributeEffect effect ) {
			Debugger.Log( string.Format( "{0} under {1} points effect({2}) for {3} turns.", Data.Name, effect.Points, Enum.GetName( typeof( EffectType ), effect.EffectType ), effect.RemainingTurns ) );
			
			activeEffects.Add( effect );
			effect.OnActive( this );
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[Character data:" );
			sb.Append( data );
			sb.Append( "]" );
			
			return sb.ToString();
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		public bool ShouldCounterAttack( Character otherCharacter ) {
			if( Faction == Faction.NEUTRAL || otherCharacter.Faction == Faction.NEUTRAL || Faction == otherCharacter.Faction )
				// NPCs não contra-atacam e nem devem ser contratacados, assim como não se contra-ataca alguém da sua própria facção
				return false;
			
			
			// TODO: estou assumindo que a arma de contrataque é sempre a da direita
			float distance = ( GetPosition() - otherCharacter.GetPosition() ).Norm();
			FireArm counterAttackWeapon = GetWeaponAt( CharacterPart.LeftHand ) as FireArm;
			if( counterAttackWeapon == null || !counterAttackWeapon.CanShoot() || distance > Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT )
				return false;
			
			float k = Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT;
			float bonus = Data.CounterAttackPerceptionBonus;
			float counterAttackProbability = ( ( k - distance ) / k ) * bonus;
			return NanoMath.Random( 0.0f, 1.0f ) < counterAttackProbability;
		}
	}
	
}


	