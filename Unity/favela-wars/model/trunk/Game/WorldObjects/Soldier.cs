using System;

using Action = GameCommunication.GameAction;
using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public abstract class Soldier : Character {
	
		
		#if UNITY_EDITOR
		// necessário para serializar a classe no editor
		public Soldier() {
		}
		#endif
		
		
		public Soldier( int worldID, string assetBundleID ) : base( worldID, assetBundleID ) {
		}

	}
	
}

