using System;

using Utils;


namespace GameModel {
	
	/// <summary>
	/// Classe que descreve um cartucho de munição.
	/// </summary>
	[ Serializable ]
	public class Ammo : Item {
		
		protected int maxShots = 15;
		
		protected int remainingShots = 15;
		
		protected FireArmType supportedType;
		
		// HACK: esses -1 aqui estão marretados!
		public Ammo( FireArmType type ) : base( -1, "HACKHACKHACK" ) {
			supportedType = type;
			Size = ItemSize.Small;
		}
		
		
		public bool IsFull() {
			return remainingShots >= maxShots;
		}
		
		
		public bool IsEmpty() {
			return remainingShots <= 0;
		}
		
		
		/// <summary>
		/// Quantidade máxima de tiros.
		/// </summary>
		[ ToolTip( "Quantidade máxima de tiros." ) ]
		public int MaxShots {
			get { return this.maxShots; }
			set { maxShots = value; }
		}
		
		
		/// <summary>
		/// Quantidade restante de tiros.
		/// </summary>
		[ ToolTip( "Quantidade restante de tiros." ) ]
		public int RemainingShots {
			get { return this.remainingShots; }
			set { remainingShots = value; }
		}
		
		
		/// <summary>
		/// Tipo de arma que a munição suporta.
		/// </summary>
		[ ToolTip( "Tipo de arma que a munição suporta." ) ]
		public FireArmType SupportedType {
			get { return supportedType; }
			set { supportedType = value; }
		}
		
	}
	
}

