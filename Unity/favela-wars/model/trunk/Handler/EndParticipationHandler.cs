using System;

using Utils;
using GameCommunication;

namespace GameModel {
	
	public class EndParticipationHandler : Handler {
		
		protected EndParticipationRequest request;
		
		
		public EndParticipationHandler( EndParticipationRequest request ) {
			this.request = request;
		}
		
		
		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			TurnManager turnManager = mission.GetTurnManager();
			
			// TODO: onde são salvos os players?
			Player player = mission.GetPlayer( request.GetPlayerID() );
			Debugger.Log( "Player Id: " + request.GetPlayerID() );
			
			turnManager.EndParticipation( player );
			if( turnManager.UpdateTurnAndCheckForChange() ) {
				Response r = new Response();
				Debugger.Log( "The Turn Has Changed!" );
				
				TurnData turnData = turnManager.GetTurnData();
				r.AddGameActionData( new TurnChangeData( turnData ) );
				
				if( turnData.faction != Faction.NEUTRAL ) {
					VisibilityChangeData visibilityChangeData = mission.GetVisibilityManager().GetVisibilityFor( turnData.faction, turnData.turnNumber == 1 );
					r.AddGameActionData( visibilityChangeData );	
				}
				
				return r;
			}
			
			return null;
		}
		
	}
}

		