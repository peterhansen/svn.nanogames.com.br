using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;



namespace GameModel {
	
	public class ShootHandler : Handler {
		
		protected ShootRequest request;
		
		
		
		public ShootHandler( ShootRequest request ) {
			this.request = request;
		}


		public override Response GetResponse() {
			int playerID = request.GetPlayerID();
			Mission mission = Mission.GetInstance( playerID );
			Character character = mission.GetCharacter( request.shooterWorldID );
			
			if( character == null || mission.GetPlayerId( character ) != playerID ) {
				Debugger.LogWarning( "Jogador tentando atirar com personagem fora de sua tropa. PlayerID: " + playerID + ", characterID: " + request.shooterWorldID );
				return null;
			}
			
			CollisionManager cm = mission.GetMap().GetCollisionManager();
			
			Response r = new Response();
			List< HitInfo > hits = new List< HitInfo >();
			List< GameVector > hitPoints = new List< GameVector >();
			List< float > shotTimes = new List< float >();
			
			Weapon weapon = ( Weapon ) character.GetItemByEquipID( request.weaponID );
			if ( weapon == null ) {
				Debugger.LogError( "Invalid weaponID: " + request.weaponID );
				return null;
			}
			
			// verifica se o personagem tem AP para atacar; se tiver, desconta seu AP
			float apCost = weapon.GetAPCost( request.useMode );
			if ( apCost > character.Data.ActionPoints ) {
				AddInsufficientAPMessage( mission, r, playerID, character.GetWorldID() );
				return r;
			}
			
			character.Data.ActionPoints.Value -= ( int ) Math.Round( apCost );
			
			// mudando direcao do personagem
			GameVector origin = character.GetShootPosition();
			character.GetGameTransform().direction = ( request.destination - origin ).Unitary();
			VisibilityChangeData visibilityData = mission.GetVisibilityManager().CalculateVisibilityFor( character );
			r.AddGameActionData( visibilityData );
			
			// calcula precisão do tiro
			GameVector target = new GameVector( request.destination );
			float distance = ( target - origin ).Norm();
			GameVector diff = GetDeviation( character, weapon, distance );
			
			AttackReply attackReply;
			try {
				attackReply = weapon.Attack( origin, target, diff, request.useMode );
			} catch ( AttackOutOfRangeException ) {
				AddMessage( mission, r, playerID, character.GetWorldID(), "Ataque fora de alcance." );
				return r;
			}
			
			if( attackReply.state == AttackState.OUT_OF_AMMO ) {
				AddMessage( mission, r, playerID, character.GetWorldID(), "Sem municao para essa arma." );
				return r;
			}
			
			List< ShotInfo > shots = new List< ShotInfo >();
			foreach ( AttackInfo a in attackReply.shots )
				shots.Add( a as ShotInfo );
			
			shots.Sort( new ShotInfoComparer() );
			
			foreach ( ShotInfo shot in shots ) {
				HitInfo hit = new HitInfo();
				cm.Shoot( character, shot.Target, out hit );
				hits.Add( hit );
				hitPoints.Add( hit.position );
				shotTimes.Add( shot.TimeSinceFire );
			}
			
			GameVector[] hitPointsArray = hitPoints.ToArray();
			HitInfo[] hitsArray = hits.ToArray();
			float[] shotTimesArray = shotTimes.ToArray();
			
			r.AddGameActionData( new ShootData( request.shooterWorldID, origin, shotTimesArray, hitPointsArray ) );
			
			for ( int i = 0; i < hitsArray.Length; ++i ) {
				HitInfo hit = hitsArray[ i ];
				
				// TODO a animação do personagem atingido deve levar em consideração o instante do impacto
				
				if ( hit.worldObjectId != Common.NULL_WORLDOBJECT ) {
					Character targetCharacter = mission.GetCharacter( hit.worldObjectId );
					
					if ( targetCharacter != null ) {
						int damage = ( int ) Math.Round( attackReply.shots[ i ].Power * Common.CharacterInfo.WEAPON_POWER_TO_HP_RATE );
						DealDamageRequest dealDamageRequest = new DealDamageRequest( playerID, character.GetWorldID(), targetCharacter.GetWorldID(), damage );
						DealDamageHandler dealDamageHandler = new DealDamageHandler( dealDamageRequest );
						Response dealDamageResponse = dealDamageHandler.GetResponse();
						if( dealDamageResponse != null ) {
							foreach( GameActionData dealDamageActionData in dealDamageResponse.GetGameActionsData() )
								r.AddGameActionData( dealDamageActionData );
						}
					}
				}
			}
			
			//Adicionando os tiros as estatísticas
			mission.OnCharacterShoot( character , weapon , shots.Count , (hitsArray.Length > 0));
			
			return r;
		}
		
		
		public static GameVector GetDeviation( Character character, Weapon weapon, float distance ) {
			return GetDeviation( character, weapon.Precision, weapon.Range, distance );
		}
		
		
		public static GameVector GetDeviation( Character character, float weaponPrecision, float weaponRange, float distance ) {
			float stancePrecision = character.GetStance() == Stance.STANDING ? 1.0f : 1.0f + Common.CharacterInfo.KNEELING_PRECISION_BONUS;
			float healthPrecision = Common.CharacterInfo.HEALTH_PRECISION_BASE_MODIFIER + ( 1.0f - Common.CharacterInfo.HEALTH_PRECISION_BASE_MODIFIER ) * character.Data.HitPoints.Percent;
			float precision = character.Data.Technique.AbsPercent * ( weaponPrecision * 0.01f ) * stancePrecision * healthPrecision;
			float consideredDistance = Math.Min( distance, weaponRange );
			float currentMaxDeviation = consideredDistance * Common.CharacterInfo.MAX_SHOT_DEVIATION / weaponRange;
			
			// desvio máximo em relação ao alvo. O valor é metade pois o desvio pode ser negativo ou positivo
			float maxDeviation = ( 1.0f - precision ) * distance * currentMaxDeviation * 0.5f;
			
			return new GameVector( maxDeviation, maxDeviation, maxDeviation );
		}
		
	}
	
	internal class ShotInfoComparer : Comparer< ShotInfo > {
		public override int Compare( ShotInfo v1, ShotInfo v2 ) {
			if ( v1.TimeSinceFire < v2.TimeSinceFire )
				return -1;
			
			if ( v1.TimeSinceFire > v2.TimeSinceFire )
				return 1;
			
			return 0;
		}
	}

}

