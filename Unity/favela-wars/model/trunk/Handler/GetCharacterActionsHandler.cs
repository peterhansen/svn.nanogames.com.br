using System;
using System.Collections.Generic;

using GameCommunication;
using Action = GameCommunication.GameAction;
using Utils;


namespace GameModel {
	
	public class GetCharacterActionsHandler : Handler {
		
		protected GetCharacterActionsRequest request;
		
		public GetCharacterActionsHandler( GetCharacterActionsRequest getCharacterActionsRequest ) {
			this.request = getCharacterActionsRequest;
		}
		

		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetCharacter( request.characterWorldID );
			
			if ( character == null || mission.GetPlayerId( character ) != request.GetPlayerID() ) {
				Debugger.Log( "GetCharacterActionsHandler: jogador tentando acessar personagem fora de sua tropa." );
				return null;
			}
			
			GameAction root = character.GetPossibleActionsList();
			VisibilityManager visibilityManager = mission.GetVisibilityManager();
			Response r = new Response();
			
			
//			CharacterActionInfo characterActionInfo = new CharacterActionInfo();
//			characterActionInfo.percentualAP = ( character.Data.HitPoints.Max != 0f ) ? ( ( character.Data.HitPoints.Value / character.Data.HitPoints.Max ) * 100f ) : 0f ;
//			characterActionInfo.percentualHP = ( character.Data.ActionPoints.Max != 0f ) ? ( ( character.Data.ActionPoints.Value / character.Data.ActionPoints.Max ) * 100f) : 0f ;
//											 
			
			GetCharacterActionsData characterActionsData = 
					new GetCharacterActionsData( request.characterWorldID, root ,
												 ( character.Data.HitPoints.Max != 0f ) ? ( ( character.Data.HitPoints.Value / character.Data.HitPoints.Max ) * 100f ) : 0f,
												 ( character.Data.ActionPoints.Max != 0f ) ? ( ( character.Data.ActionPoints.Value / character.Data.ActionPoints.Max ) * 100f) : 0f ) ;
			
			// obtém a lista de inimigos atualmente na linha de tiro do personagem
			CharacterVisibilityInfo info = visibilityManager.GetCharacterVisibilityInfo( character );
			foreach ( int characterID in info.observedCharactersIDs ) {
				Character c = mission.GetCharacter( characterID );
				if( mission.AreEnemies( c.Data.Faction, character.Data.Faction ) )
					characterActionsData.visibleEnemiesIDs.Add( characterID );
			}
			
			r.AddGameActionData( characterActionsData );
			return r;
		}
	}
}
