using System;

using GameCommunication;


namespace GameModel {
	
	public class DealDamageHandler : Handler {
		
		protected DealDamageRequest request;
		
		protected Mission mission;
		
		public DealDamageHandler( DealDamageRequest request ) {
			this.request = request;
		}
		
		public override Response GetResponse () {
			Response response = new Response();
			int playerID = request.GetPlayerID();
			mission = Mission.GetInstance( playerID );
			Character damagingCharacter = mission.GetCharacter( request.damagingCharacterID );
			Character damagedCharacter = mission.GetCharacter( request.damagedCharacterID );
			
			damagedCharacter.Data.HitPoints.Value -= request.damage;
			if ( damagedCharacter.Data.HitPoints <= 0 )
				mission.OnCharacterKilled( response, damagingCharacter, damagedCharacter );
			
			response.AddGameActionData( new CharacterHitData( damagedCharacter.GetWorldID(), request.damage, damagedCharacter.IsAlive() ) );
			return response;
		}
	}
}

