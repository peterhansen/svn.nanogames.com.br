using System;

using UnityEngine;

using Squid;
using Squid.Controls;

using Utils;
using GameController;
using GameCommunication;


public class CharacterListNameEntry : Frame {

	public const int DefaultHeight = 25;
	
	private int id;
	
	private Label tx;
	
	private ImageControl eyeIcon;
	
	
	protected override void Initialize () {
		base.Initialize();
		
		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}

		Position = new Point( 0, 0 );
		OnMouseClick += new MouseClickEventHandler( MoveCameraToActor );
	}
	
	
	public CharacterListNameEntry( String name, Control parent, int id, CharacterKnownStatus initialState, int width ): base() {
		this.id = id;
		
		
		tx = new Label();
		tx.Text = name;
		
		Parent = parent;
		Size = new Point( width, DefaultHeight );
		eyeIcon = new Squid.Controls.ImageControl();
		switch ( initialState ) {
			case CharacterKnownStatus.CurrentlySeen:
				eyeIcon.Texture = "ListaInimigos/IconVisible.png";
			break;
			
			case CharacterKnownStatus.NotSeen:
			return;

			case CharacterKnownStatus.SeenPreviously:
				eyeIcon.Texture = "ListaInimigos/IconHidden.png";
			break;
			
		}
		eyeIcon.Parent = this;
		eyeIcon.Size = new Point( 21, 15 );
		eyeIcon.Position = new Point( ( width ) - 2 * ( eyeIcon.Size.x + 2 * TitledFrame.framePadding ), 2 );
		
		tx.Parent = this;
		tx.Position = new Point( 0, 0 );
		tx.Size = new Point( Size.x - eyeIcon.Size.x, Size.y );
		
		tx.OnMouseClick += new MouseClickEventHandler( MoveCameraToActor );
		eyeIcon.OnMouseClick += new MouseClickEventHandler( MoveCameraToActor );
	}
	
	
	private void MoveCameraToActor( Control sender ) {
		IsoCameraScript isoCamera = IsoCameraScript.GetInstance();
		isoCamera.MoveTo( WorldManager.GetInstance().GetGameObject( id ).transform.position );
	} 
	
}


public class CharacterListControl : FactionWindow {
	
	public VScrollbar Scrollbar { get; private set; }

	protected CharacterListData characterListData;
	
	private Button showBope;
	
	private Button showCriminal;
	
	private CheckBox showDead;
	
	private CheckBox showAlive;
	
	private CheckBox showOutOfSight;
	
	private Panel characterListContainer;
	
	private int currentElementsCount;
	
	
	public int Populate( ) {
		TitledFrame section; 
		int previousY = 0;
		
		foreach( Faction f in Common.PlayerFactions ) {
			section = new TitledFrame();
			section.Parent = this.characterListContainer.Content;
			section.Position = new Point( 0, previousY );
			currentElementsCount = 0;
			
			foreach ( int id in characterListData.Keys ) {
				if ( characterListData[ id ] != CharacterKnownStatus.NotSeen ) {
					if ( characterListData.Factions[ id ] != f )
						continue;

					if ( !showBope.Checked &&  characterListData.Factions[ id ] == Faction.POLICE )
						continue;
					
					if ( !showCriminal.Checked && characterListData.Factions[ id ] == Faction.CRIMINAL )
						continue;

				
//					if ( !showDead.Checked && showAlive.Checked && !characterListData.Alive[ id ] )
//						continue;
//					
//					if ( !showAlive.Checked && characterListData.Alive[ id ] )
//						continue;
//	
//					
//					if ( !showOutOfSight.Checked && characterListData[ id ] == CharacterKnownStatus.SeenPreviously )
//						continue;
					
					AddEntry( "soldado " + id, id, section, characterListData[ id ], characterListContainer.Size.x - characterListContainer.Position.x );
					++currentElementsCount;
				}
			}
			
			section.setTitleAndSize( f.ToString(), new Point( ( int )( this.characterListContainer.Content.Size.x ), ( TitledFrame.BottomPaddingY + TitledFrame.CaptionSizeY  + CharacterListNameEntry.DefaultHeight * currentElementsCount ) ) );
			previousY += section.Size.y;
		}			
		
		return previousY;
	}
	

	protected override void Initialize () {
		base.Initialize();
		
		this.characterListData = GameManager.GetInstance().GetCharacterListData();
		
		///////////TMP///////////////
//		this.characterListData = new CharacterListData();
//		for ( int c = 0; c < 30; ++c ) {
//			
//			characterListData[ c ] = CharacterKnownStatus.CurrentlySeen;
//			characterListData.Alive[ c ] = ( c % 2 ) == 0;
//			characterListData.Factions[ c ] = ( ( c % 2 ) == 0 ) ? Faction.POLICE : Faction.CRIMINAL;
//
//		}
		////////////////////////////
		
		Frame section; 

		section = new SectionFrame();		
		section.Parent = this.panel.Content;
		section.Position = new Point( 0, 0 );
		( section as SectionFrame ).SetSize( new Point( this.panel.Content.Size.x, 150 ) );
		
		Label tx = new Label();
		tx.Text = "Lista de unidades";
		tx.Parent = section;
		tx.Position = new Point( 20, 10 );
		tx.Size = new Point( 120, 20 );
		tx.Style = "FontTitle";
		
		showBope = new Button();
		showBope.Parent = section;
		showBope.Position = new Point( 35, 50 );
		showBope.Size = new Point( 150, 55 );
		showBope.Checked = true;
		showBope.CheckOnClick = true;
		showBope.Text = "BOPE";
		showBope.Style = "togglebutton";
		showBope.OnCheckedChanged += new CheckedChangedEventHandler( RebuildList );
		
		showCriminal = new Button();
		showCriminal.Parent = section;
		showCriminal.Position = new Point( 205, 50 );
		showCriminal.Size = new Point( 150, 55 );
		showCriminal.Checked = true;
		showCriminal.CheckOnClick = true;
		showCriminal.Text = "TRAFICO";
		showCriminal.Style = "togglebutton";
		showCriminal.OnCheckedChanged += new CheckedChangedEventHandler( RebuildList );
		
		showAlive = new CheckBox();
		showAlive.Parent = section;
		showAlive.Position = new Point( 42, 120 );
		showAlive.Size = new Point( 15, 15 );
		showAlive.Checked = true;
		showAlive.Style = "checkboxStyle";
		showAlive.OnCheckedChanged += new CheckedChangedEventHandler( RebuildList );
		
		tx = new Label();
		tx.Text = "Vivos";
		tx.Parent = section;
		tx.Position = new Point( 58, 115 );
		tx.Size = new Point( 40, 20 );		
		
		
		showDead = new CheckBox();
		showDead.Parent = section;
		showDead.Position = new Point( 127, 120 );
		showDead.Size = new Point( 15, 15 );
		showDead.Checked = true;
		showDead.Style = "checkboxStyle";
		showDead.OnCheckedChanged += new CheckedChangedEventHandler( RebuildList );
		
		tx = new Label();
		tx.Text = "Mortos";
		tx.Parent = section;
		tx.Position = new Point( 143, 115 );
		tx.Size = new Point( 40, 20 );		
		
		
		showOutOfSight = new CheckBox();
		showOutOfSight.Parent = section;
		showOutOfSight.Position = new Point( 229, 120 );
		showOutOfSight.Size = new Point( 15, 15 );
		showOutOfSight.Checked = true;
		showOutOfSight.Style = "checkboxStyle";
		showOutOfSight.OnCheckedChanged += new CheckedChangedEventHandler( RebuildList );
		
		tx = new Label();
		tx.Text = "Fora de visao";
		tx.Parent = section;
		tx.Position = new Point( 245, 115 );
		tx.Size = new Point( 40, 20 );
		///---------------------------------------------------------------
		section = new SectionFrame();		
		section.Parent = this.panel.Content;
		section.Position = new Point( 0, 151 );
		SectionFrame prev = ( SectionFrame ) section;
		Frame holder = new Frame();
		holder.Parent = section;
		holder.Position = new Point( 0, 20 );
		
		section = holder;
		characterListContainer = new Panel();
		characterListContainer.Parent = holder;
		characterListContainer.Position = new Point( 10, 0 );
		prev.SetSize( new Point( ( int )( this.panel.Content.Size.x ), this.panel.Content.Size.y - prev.Position.y ) );
		holder.Size = new Point( ( int )( prev.Size.x ), prev.Size.y - 2 * holder.Position.y );//( Size.y - section.Position.y - ( FactionWindowDecoration.DECORATION_REAL_HEIGHT ) ) );
		characterListContainer.Size = new Point( ( int )( holder.Size.x * 0.95f ), holder.Size.y );
		characterListContainer.Content.Size = characterListContainer.Size;

		characterListContainer.VScroll.ButtonUp.Style = "vsUp";
		characterListContainer.VScroll.ButtonDown.Style = "vsDown";
		characterListContainer.VScroll.Slider.Button.Style = "vsTrack";
		characterListContainer.VScroll.Style = "vsBg";
		
		int newSize = RebuildListHandler();
		characterListContainer.VScroll.Size = new Point( 12, characterListContainer.Size.y );
		characterListContainer.VScroll.ButtonDown.Size = new Point( 12, 12 );
		characterListContainer.VScroll.ButtonUp.Size = new Point( 12, 12 );
		///---------------------------------------------------------------
		Squid.Controls.ImageControl div = new Squid.Controls.ImageControl();
		div.Texture = "FactionWindow/BkgDivision.png";
		div.Size = new Point ( this.panel.Content.Size.x, 32 );
		div.Position = new Point ( 0, 150 - ( div.Size.y / 2 ) );
		div.Parent = this.panel.Content;
		
	}
	
	
	private void RebuildList( Control sender ) {
		RebuildListHandler();
	}
	
	
	public int RebuildListHandler() {
		Control v;
		
		for ( int c = 0; c < characterListContainer.Content.Controls.Count; ++c ) {
			v = characterListContainer.Content.Controls[ c ];
			v.Parent = null;
		}
		
		characterListContainer.Content.Controls.RemoveRange( 0, characterListContainer.Content.Controls.Count );
		return Populate();
	}
	
	
	public void AddEntry( String name, int id, Frame parent, CharacterKnownStatus state, int width ) {
		CharacterListNameEntry entry = new CharacterListNameEntry( name, parent, id, state, width );
		entry.Position = new Point( TitledFrame.CaptionPosX, TitledFrame.CaptionPosY + TitledFrame.CaptionSizeY + CharacterListNameEntry.DefaultHeight * currentElementsCount );
//		entry.Parent = parent;
	}
	
	
	private void Dismiss( Control sender ) {
		GUIManager.Reset();
	}

}