using System;

using Squid;

using GameController;


public class MenuOption {
	
	public IconType icon;
	
	public string stringCode;
	
	public object tag;
	
	public MouseClickEventHandler callback;
	
	public MenuOption( IconType icon, MouseClickEventHandler callback ) : this( icon, callback, null ) {
	}
	
	public MenuOption( IconType icon, MouseClickEventHandler callback, object tag ) {
		this.icon = icon;
		this.tag = tag;
		this.callback = callback;
		
		// TODO
		this.stringCode = Enum.GetName( typeof( IconType ), icon );
	}
}
