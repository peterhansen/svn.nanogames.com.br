using System;

using Squid;


public class OptionButton : Frame {
		
	protected MenuOption menuOption;
	
	public Button clickableArea = new Button();
	
	public OptionButton( MenuOption menuOption ) {
		this.menuOption = menuOption;
		
		clickableArea.Opacity = 0.0f;
		Controls.Add( clickableArea );
	}
	
	public virtual void AddButtonContents() {
	}
}


