using Utils;
using Squid;
using System;
using UnityEngine;
using GameController;
using GameCommunication;

public class FactionWindowDecoration : Frame {
	
	Squid.Controls.ImageControl highDecor;
	Squid.Controls.ImageControl lowDecor;
	
	public const int DECORATION_HEIGHT = 60;
	public const int DECORATION_REAL_HEIGHT = 20;
	
		protected override void Initialize () {
		
		base.Initialize();
		
		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}
	}
	
	protected override void DrawStyle (Style style, float opacity) {
		base.DrawStyle (style, opacity);
		
		int texId = GuiHost.Renderer.GetTexture("FactionWindow/spotlights.png");
		Squid.UVCoords coords;
		coords = new Squid.UVCoords( 0.0f, 0.0f, 1.0f, 1.0f );
		GuiHost.Renderer.DrawTexture( texId, 0, DECORATION_REAL_HEIGHT, 422, 168, coords, -1 );
		
		coords = new Squid.UVCoords( 1.0f, 1.0f, 0.0f, 0.0f );
		GuiHost.Renderer.DrawTexture( texId, 0, Size.y - 168 - DECORATION_REAL_HEIGHT, 422, 168, coords, -1 );
		
	}
	
	public void SetSizeForDecorations( Faction faction, Point size ) {;
		this.Size = size;
		
		Squid.Controls.ImageControl decor;
			
		decor = new Squid.Controls.ImageControl();
		decor.Parent = this;
		decor.Size = new Point( this.Parent.Size.x, 61 );
		decor.Position = new Point( 0, 0 );
		decor.Texture = ( faction == Faction.POLICE ) ? "FactionWindow/Adorno_bope_cima.png": "FactionWindow/Adorno_favela_cima.png";

		decor = new Squid.Controls.ImageControl();
		decor.Parent = this;
		decor.Size = new Point( this.Parent.Size.x, 61 );
		decor.Position = new Point( 0, Size.y - ( 61 ) );
		decor.Texture = ( faction == Faction.POLICE ) ? "FactionWindow/Adorno_bope_baixo.png": "FactionWindow/Adorno_favela_baixo.png";
	}
	
}


