using System;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using GameCommunication;
using GameController;
using Utils;


public class RadialMenuControl : OptionsMenu {
	
	public const int RADIAL_MENU_RADIUS = 200;
	
	private const float BUTTON_SLIDING_TIME = 1000.0f;
	
	public List< OptionButton > Buttons {
		get{ return buttons; }
	}
	
	
	public RadialMenuControl() {
		Size = new Point( 2 * (int) RADIAL_MENU_RADIUS, 2 * (int) RADIAL_MENU_RADIUS );
	}
	
	
	protected override OptionButton CreateButton( MenuOption option, object buttonData ) {		
		RadialMenuButton button = new RadialMenuButton( option );
		button.Position = new Point( (int) RADIAL_MENU_RADIUS, (int) RADIAL_MENU_RADIUS );
		return button;
	}
	
	
	public override void Show( Point position ) {
		Visible = true;
		Position = position - new Point( (int) RADIAL_MENU_RADIUS, (int) RADIAL_MENU_RADIUS );
		
		int numberOfOptions = buttons.Count;
		if( numberOfOptions == 0 ) {
			Debugger.LogError( "RadialMenuControl: radial menu with no options trying to be shown." );
			return;
		}
		
		// posicionando botão central do meio
		const int buttonHalfSize = ( RadialMenuButton.BUTTON_SIDE_SIZE >> 1 );
		
		Point centralPosition = new Point( RADIAL_MENU_RADIUS - buttonHalfSize, RADIAL_MENU_RADIUS - buttonHalfSize );
		//if( centralButton != null )
		//	centralButton.Animation.Custom( Slide( centralButton, buttonFinalPosition ) );
		
		// posicionando botões da borda
		float angle = 18.0f * Mathf.PI / 180.0f;
		float dAngle = ( 2.0f * Mathf.PI ) / (float) numberOfOptions;
		for( int i = 0; i < numberOfOptions; i++ ) {
			int dX = (int) ( ( RADIAL_MENU_RADIUS >> 1 ) * Mathf.Cos( angle ) );
			int dY = (int) ( ( RADIAL_MENU_RADIUS >> 1 ) * Mathf.Sin( angle ) );			
			Point targetPosition = centralPosition + new Point( -dX, -dY );
			
			RadialMenuButton button = buttons[ i ] as RadialMenuButton;
			Controls.Add( button );
			SlideButton( button, targetPosition );
			button.Quadrant = GetQuadrantFromAngle( angle );
			angle += dAngle;
		}
	}		
	
	
	private Quadrant GetQuadrantFromAngle( float angle ) {
		// modulando ângulo para que fique em graus...
		int angleInDegrees = (int) ( angle * 180.0f / Mathf.PI );
		
		// ... e no intervalo 0 ~ 360
		angleInDegrees = ( angleInDegrees < 0? 360 + angleInDegrees : angleInDegrees ) % 360;
		Quadrant quadrant = (Quadrant) ( ( angleInDegrees ) / 90 );
		return quadrant;
	}
	
	
	protected void SlideButton( RadialMenuButton button, Point targetPosition ) {
		button.Animation.Size( new Point( RadialMenuButton.BUTTON_SIDE_SIZE, RadialMenuButton.BUTTON_SIDE_SIZE ), BUTTON_SLIDING_TIME );
		button.Animation.Position( targetPosition, BUTTON_SLIDING_TIME );
	}
}

