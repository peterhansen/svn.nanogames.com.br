using System;

public enum Quadrant {
	NORTHWEST,
	NORTHEAST,
	SOUTHEAST,
	SOUTHWEST
}

