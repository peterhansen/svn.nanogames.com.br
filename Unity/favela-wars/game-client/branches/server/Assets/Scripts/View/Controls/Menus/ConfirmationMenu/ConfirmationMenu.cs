using System;

using Squid;

using Utils;


public class ConfirmationMenu : OptionsMenu {
	
	protected const int CONFIRMATION_MENU_MARGIN = 15;	
	
	public ConfirmationMenu() {
		Visible = false;
		Style = "ConfirmationBackground";
	}
	
	public override void Show( Point position ) {
		Visible = true;
		Size = new Point( 
			( buttons.Count - 1 ) * ( CONFIRMATION_MENU_MARGIN >> 1 ) + buttons.Count * ConfirmationButton.CONFIRMATION_BUTTON_SIZE_X + 2 * CONFIRMATION_MENU_MARGIN, 
			ConfirmationButton.CONFIRMATION_BUTTON_SIZE_Y + 2 * CONFIRMATION_MENU_MARGIN
		);
		
		for( int i = 0; i < buttons.Count; i++ ) {
			OptionButton button = buttons[ i ];
			Controls.Add( button );
			button.Position = new Point( 
				CONFIRMATION_MENU_MARGIN + ( ( CONFIRMATION_MENU_MARGIN >> 1 ) + ConfirmationButton.CONFIRMATION_BUTTON_SIZE_X ) * i, 
				CONFIRMATION_MENU_MARGIN 
			);
		}
		
		Position = position - new Point( Size.x >> 1, Size.y >> 1 );
	}
	
	protected override OptionButton CreateButton( MenuOption option, object buttonData ) {
		return new ConfirmationButton( option );
	}
}

