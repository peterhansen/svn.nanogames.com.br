using System;

using Squid;
using Squid.Controls;

using GameController;

	
public class DetailedMenuButton : OptionButton {
			
	public const int ICON_FRAME_HEIGHT = 50;
	
	protected const int ICON_SIDE_SIZE = 40;
	
	protected const int ICON_FRAME_WIDTH = 73;
	
	protected const int BUTTON_LINE_HEIGHT = 2;
	
	protected Label buttonText;
	
	protected ImageControl iconFrame;
	
	protected ImageControl buttonLine;
	
	public DetailedMenuButton( MenuOption option ) : base( option ) {
		iconFrame = new ImageControl();
		iconFrame.Texture = "GUI/ActionsMenu/ActionsSubMenu/IconBox";
		iconFrame.Size = new Point( ICON_FRAME_WIDTH, ICON_FRAME_HEIGHT );
		iconFrame.AllowFocus = false;
		iconFrame.NoEvents = true;
		
		buttonLine = new ImageControl();
		buttonLine.Texture = "GUI/ActionsMenu/ActionsSubMenu/IconBoxLine";
		buttonLine.Position = new Point( ICON_FRAME_WIDTH, 26 );
		buttonLine.AllowFocus = false;
		buttonLine.NoEvents = true;
				
		buttonText = new Label();
		buttonText.Text = L10n.Get( option.stringCode );
		buttonText.Position = new Point( ICON_FRAME_WIDTH, 9 );
		buttonText.AutoSize = AutoSize.HorizontalVertical;
		buttonText.AllowFocus = false;
		buttonText.NoEvents = true;
		
		Controls.Add( buttonText );
		Controls.Add( iconFrame );
		Controls.Add( buttonLine );
		
		OnSizeChanged += new SizeChangedEventHandler( OnSizeChangedHandler );
	}
	
	
	protected void OnSizeChangedHandler( Control sender ) {
		clickableArea.Size = Size;
		
		// TODO: como pegar o nome da fonte de um estilo específico?
		const string styleFontName = "Fonte_Nome01";
		Point textSize = GuiHost.Renderer.GetTextSize( buttonText.Text, GuiHost.Renderer.GetFont( styleFontName ) );
		buttonLine.Size = new Point( textSize.x, 7 );
	}
}