using System;
using System.Collections.Generic;

using Squid;


public abstract class OptionsMenu : Frame {
	
	protected List< OptionButton > buttons = new List< OptionButton >();
	
	public abstract void Show( Point position );
	
	protected abstract OptionButton CreateButton( MenuOption option, object buttonData );
	
	public OptionButton AddButton( MenuOption option ) {
		return AddButton( option, null );
	}
	
	public OptionButton AddButton( MenuOption option, object buttonData ) {
		OptionButton button = CreateButton( option, buttonData );
		button.AddButtonContents();
		button.clickableArea.OnMouseClick += new MouseClickEventHandler( option.callback );
		button.clickableArea.Tag = option.tag == null? option : option.tag;
		buttons.Add( button );
		return button;
	}
}


