using System;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;


public class CharacterInfoControl : FactionWindow {
	
	protected GetCharacterInfoInteractionController interactionController;
	protected GetCharacterInfoData getCharacterInfoData;
	
	protected Label nameLabel;
	protected Label charClassLabel;
	protected Label statusName;
	protected Label statusValue;
	protected Label karmaName;
	protected Label karmaValue;
	protected Label xpName;
	protected Label xpValue;	
	protected Gauge apBar;
	protected Gauge hpBar;
	protected Label apName;
	protected Label hpName;
	
	
	Squid.Controls.ImageControl mugshot;
	//ataque
	//defesa
	//agilidade
	protected Label attackNameLabel;
	protected Label attackValueLabel;
	protected Label defenseNameLabel;	
	protected Label defenseValueLabel;
	protected Label agilityNameLabel;	
	protected Label agilityValueLabel;

	//tecnica
	//percepcao
	//inteligencia
	protected Label tecniqueNameLabel;
	protected Label tecniqueValueLabel;
	protected Label perceptionNameLabel;	
	protected Label perceptionValueLabel;
	protected Label inteligenceNameLabel;	
	protected Label inteligenceValueLabel;
	
	
	protected Button closeButton;
	
	//// character camera
	//protected const int characterCameraWidth = 64;
	//protected const int characterCameraHeight = 64;
	//protected const int characterCameraDepth = 24;
	//protected readonly Vector3 cameraCloseUpPosition = new Vector3( 2.0f, 2.0f, 2.0f );
	//protected GameObject characterCamera;
	//protected RenderTexture cameraTexture = new RenderTexture( characterCameraWidth, characterCameraHeight, characterCameraDepth, RenderTextureFormat.Default, RenderTextureReadWrite.Default );
	


	
	public void init( GetCharacterInfoInteractionController interactionController, GetCharacterInfoData data ) {
		this.interactionController = interactionController;
		this.getCharacterInfoData = data;
		hpBar.Update( new Point( 100, 30 ), ( getCharacterInfoData.data.HitPoints.Value / getCharacterInfoData.data.HitPoints.Max ) );			
		apBar.Update( new Point( 100, 30 ), ( getCharacterInfoData.data.ActionPoints.Value / getCharacterInfoData.data.ActionPoints.Max ) );
	}
	
	protected override void Initialize() {
		base.Initialize();
	}
	
	public CharacterInfoControl( GetCharacterInfoInteractionController interactionController, GetCharacterInfoData data ) {
		// criando a câmera e realizando o close-up no personagem
		//characterCamera = new GameObject( "Character Camera" );
		//characterCamera.AddComponent< Camera >();
		//characterCamera.GetComponent< Camera >().targetTexture = cameraTexture;
		
		this.interactionController = interactionController;
		this.getCharacterInfoData = data;
		
		
		this.decor.SetSizeForDecorations( data.data.Faction, Size );

		Frame section; 
			
		section = new Frame();		
		section.Style = "frame";
		section.Parent = this.panel.Content;
		section.Position = new Point( 10, 10 );
		section.Size = new Point( Size.x, 250 );
		
		mugshot = new Squid.Controls.ImageControl();
		mugshot.Texture = "CharacterInfo/careca";
		mugshot.Parent = section;
		mugshot.Size = new Point( 145, 145 );
		mugshot.Position = new Point( 10, 10 );
		
		nameLabel = new Label();
		nameLabel.Size = new Point( 100, 30 );
		nameLabel.Position = new Point( 210, 0 );
		nameLabel.Parent = section;
		nameLabel.Text = getCharacterInfoData.data.Name;
		
		
		//TODO: criar campo  de classe de soldado
		charClassLabel = new Label();
		charClassLabel.Size = new Point( 100, 30 );
		charClassLabel.Position = new Point( 210, 20 );
		charClassLabel.Parent = section;
		
		NatureType natureType = getCharacterInfoData.data.GetNatureType();
		switch(natureType){
			case NatureType.AGRESSIVE:
			charClassLabel.Text = L10n.Get("NATURE_AGRESSIVE");
			break;
			
			case NatureType.ALERT:
			charClassLabel.Text = L10n.Get("NATURE_ALERT");
			break;
			
			case NatureType.BRUISER:
			charClassLabel.Text = L10n.Get("NATURE_BRUISER");
			break;
			
			case NatureType.CEREBRAL:
			charClassLabel.Text = L10n.Get("NATURE_CEREBRAL");
			break;
			
			case NatureType.GENERALIST:
			charClassLabel.Text = L10n.Get("NATURE_GENERALIST");
			break;
			
			case NatureType.SPEEDSTER:
			charClassLabel.Text = L10n.Get("NATURE_SPEEDSTER");
			break;
			
			case NatureType.STRAIGHTFORWARD:
			charClassLabel.Text = L10n.Get("NATURE_STRAIGHTFORWARD");
			break;
			
			case NatureType.TANK:
			charClassLabel.Text = L10n.Get("NATURE_TANK");
			break;
		}
		
		
		hpName = new Label();
		hpName.Size = new Point( 100, 30 );
		hpName.Position = new Point( 210, 50 );
		hpName.Parent = section;
		hpName.Text = L10n.Get("ATTRIBUTE_HP") + ": " + ( getCharacterInfoData.data.HitPoints.AbsPercent * 100 ).ToString( "0.00" ) + 
					  "% ( " + getCharacterInfoData.data.HitPoints.Value.ToString( "0.0" ) + "/" + getCharacterInfoData.data.HitPoints.Max.ToString( "0.0" ) + " )";
		
		
		if ( getCharacterInfoData != null && getCharacterInfoData.data != null  && getCharacterInfoData.data.HitPoints != null ) {
			
			hpBar = new Gauge();
			hpBar.Parent = section;
			hpBar.Position = new Point( 250, 50 );
			hpBar.Update( new Point( 100, 30 ), getCharacterInfoData.data.HitPoints.Percent );
		}
		
		apName = new Label();
		apName.Size = new Point( 100, 30 );
		apName.Position = new Point( 210, 70 );
		apName.Parent = section;
		apName.Text = L10n.Get("ATTRIBUTE_AP") + ": " + ( getCharacterInfoData.data.ActionPoints.AbsPercent * 100 ).ToString( "0.00" ) +
					  "% ( " + getCharacterInfoData.data.ActionPoints.Value.ToString( "0.0" ) + "/" + getCharacterInfoData.data.ActionPoints.Max.ToString( "0.0" ) + " )";
		
		if ( getCharacterInfoData != null && getCharacterInfoData.data != null && getCharacterInfoData.data.ActionPoints != null ) {
			
			apBar = new Gauge();
			apBar.Parent = section;
			apBar.Position = new Point( 250, 70 );
			apBar.Update( new Point( 100, 30 ), getCharacterInfoData.data.ActionPoints.Percent );
		}

		xpName = new Label();
		xpName.Size = new Point( 100, 30 );
		xpName.Position = new Point( 210, 90 );
		xpName.Parent = section;
		xpName.Text = L10n.Get("ATTRIBUTE_XP") + ": ";
		
		xpValue = new Label();
		xpValue.Size = new Point( 100, 30 );
		xpValue.Position = new Point( 250, 90 );
		xpValue.Parent = section;
		
		xpValue.Text = "" + getCharacterInfoData.data.GetTrainingPoints();
		
		
		statusName = new Label();
		statusName.Size = new Point( 100, 30 );
		statusName.Position = new Point( 210, 110 );
		statusName.Parent = section;
		statusName.Text = L10n.Get("ATTRIBUTE_STATUS");
		
		statusValue = new Label();
		statusValue.Size = new Point( 100, 30 );
		statusValue.Position = new Point( 250, 110 );
		statusValue.Parent = section;
		statusValue.Text = getCharacterInfoData.data.HitPoints > 0 ? "=-)" : "=-(";

		karmaName = new Label();
		karmaName.Size = new Point( 100, 30 );
		karmaName.Position = new Point( 210, 130 );
		karmaName.Parent = section;
		karmaName.Text = L10n.Get("ATTRIBUTE_MORAL");
		
		karmaValue = new Label();
		karmaValue.Size = new Point( 100, 30 );
		karmaValue.Position = new Point( 250, 130 );
		karmaValue.Parent = section;
		karmaValue.Text = "" + getCharacterInfoData.data.MoralPoints.Value;
		
		//////////stats/////////////
		
		section = new Frame();		
		section.Style = "frame";
		section.Parent = this.panel.Content;
		section.Position = new Point( 10, 170 );
		section.Size = new Point( Size.x, 170 );
		
		attackNameLabel = new Label();
		attackNameLabel.Size = new Point( 100, 30 );
		attackNameLabel.Position = new Point( 10, 50 );
		attackNameLabel.Parent = section;
		
		defenseNameLabel = new Label();
		defenseNameLabel.Size = new Point( 100, 30 );
		defenseNameLabel.Position = new Point( 10, 90 );
		defenseNameLabel.Parent = section;
		
		
		agilityNameLabel = new Label();
		agilityNameLabel.Size = new Point( 100, 30 );
		agilityNameLabel.Position = new Point( 10, 130 );
		agilityNameLabel.Parent = section;

		attackNameLabel.Text = L10n.Get("ATTRIBUTE_ATTACK");
		defenseNameLabel.Text = L10n.Get("ATTRIBUTE_DEFENSE");;
		agilityNameLabel.Text = L10n.Get("ATTRIBUTE_AGILITY");;
	
		
		attackValueLabel = new Label();
		attackValueLabel.Size = new Point( 100, 30 );
		attackValueLabel.Position = new Point( 100, 50 );
		attackValueLabel.Parent = section;
		
		defenseValueLabel = new Label();
		defenseValueLabel.Size = new Point( 100, 30 );
		defenseValueLabel.Position = new Point( 100, 90 );
		defenseValueLabel.Parent = section;
		
		
		agilityValueLabel = new Label();
		agilityValueLabel.Size = new Point( 100, 30 );
		agilityValueLabel.Position = new Point( 100, 130 );
		agilityValueLabel.Parent = section;
		
		attackValueLabel.Text = "" + getCharacterInfoData.data.AttackLevel.Value;
		defenseValueLabel.Text = "" + getCharacterInfoData.data.DefenseLevel.Value;
		agilityValueLabel.Text = "" + getCharacterInfoData.data.Agility.Value;

		//------------------------
		//tecnica
		//percepcao
		//inteligencia
		
		tecniqueNameLabel = new Label();
		tecniqueNameLabel.Size = new Point( 100, 30 );
		tecniqueNameLabel.Position = new Point( 210, 50 );
		tecniqueNameLabel.Parent = section;
		
		perceptionNameLabel = new Label();
		perceptionNameLabel.Size = new Point( 100, 30 );
		perceptionNameLabel.Position = new Point( 210, 90 );
		perceptionNameLabel.Parent = section;
		
		inteligenceNameLabel = new Label();
		inteligenceNameLabel.Size = new Point( 100, 30 );
		inteligenceNameLabel.Position = new Point( 210, 130 );
		inteligenceNameLabel.Parent = section;

		tecniqueNameLabel.Text = L10n.Get("ATTRIBUTE_TECHNIQUE");
		perceptionNameLabel.Text = L10n.Get("ATTRIBUTE_PERCEPTION");
		inteligenceNameLabel.Text = L10n.Get("ATTRIBUTE_INTELLIGENCE");
	
		
		tecniqueValueLabel = new Label();
		tecniqueValueLabel.Size = new Point( 100, 30 );
		tecniqueValueLabel.Position = new Point( 310, 50 );
		tecniqueValueLabel.Parent = section;
		
		perceptionValueLabel = new Label();
		perceptionValueLabel.Size = new Point( 100, 30 );
		perceptionValueLabel.Position = new Point( 310, 90 );
		perceptionValueLabel.Parent = section;
		
		
		inteligenceValueLabel = new Label();
		inteligenceValueLabel.Size = new Point( 100, 30 );
		inteligenceValueLabel.Position = new Point( 310, 130 );
		inteligenceValueLabel.Parent = section;
		
		tecniqueValueLabel.Text = "" + getCharacterInfoData.data.Technique.Value;
		perceptionValueLabel.Text = "" + getCharacterInfoData.data.Perception.Value;
		inteligenceValueLabel.Text = "" + getCharacterInfoData.data.Intelligence.Value;
		
		//////////
		
		closeButton = new Button();
		closeButton.Size = new Point( 100, 30 );
		closeButton.Position = new Point( 10, 370 );
		closeButton.Text = L10n.Get("ACTION_CLOSE");
		closeButton.OnMouseClick += new MouseClickEventHandler( Close );
		closeButton.Parent = section;
	}
	
	protected void Close( Control sender ) {
		GUIManager.Reset();
	}
//	public bool RenderGUI() {
//		bool done = false;
//		
//		GUI.BeginGroup( new Rect( 20.0f, 50.0f, 300.0f, 300.0f ) );
//		GUI.Box( new Rect( 0.0f, 0.0f, 300.0f, 300.0f ), "info" );
//		
//		GUI.Label( new Rect( 30.0f, 30.0f, 230.0f, 20.0f ), "nome: " + data.character.GetName() );
//		GUI.Label( new Rect( 30.0f, 60.0f, 230.0f, 20.0f ), "facção: " + data.character.GetFaction() );
//		GUI.Label( new Rect( 30.0f, 90.0f, 230.0f, 20.0f ), "HP: " + data.character.GetHitPoints() );
//		GUI.Label( new Rect( 30.0f, 120.0f, 230.0f, 20.0f ), "AP: " + data.character.GetActionPoints() );
//		GUI.Label( new Rect( 30.0f, 150.0f, 230.0f, 20.0f ), "moral: " + data.character.GetMoralPoints() );
//		
//		//GUI.DrawTexture( new Rect( 200.0f, 30.0f, 64.0f, 64.0f ), cameraTexture );
//		
//		
//		if ( GUI.Button( new Rect( 150.0f, 240.0f, 100.0f, 50.0f ), "ocultar" ) ) {
//			interactionController.OnDone();
//			done = true;
//		}
//		
//		GUI.EndGroup();
//		
//		return done;
//	}
}


