using System;
using Squid;

public class SectionFrame : Frame {
	public Squid.Controls.ImageControl gradient;
//	protected override void DrawStyle (Style style, float opacity) {
//		base.DrawStyle (style, opacity);
//		
//		int texId = GuiHost.Renderer.GetTexture("FactionWindow/fundo_degrade.png");
//		Squid.UVCoords coords;
//		coords = new Squid.UVCoords( 0.0f, 0.0f, 1.0f, 1.0f );
//		GuiHost.Renderer.DrawTexture( texId, Position.x, Position.y, Size.x, Size.y, coords, -1 );
//	}
	
	public void SetSize( Point size ) {
		Size = size;
		gradient.Size = size;
	}
	
	protected override void Initialize () {
		base.Initialize ();
		
		Style = "holoPanel";
		
		gradient = new Squid.Controls.ImageControl();
		gradient.Parent = this;
		gradient.Texture = "FactionWindow/fundo_degrade.png";
		gradient.Position = new Point( 0, 0 );
		gradient.Size = Size;

	}
}
