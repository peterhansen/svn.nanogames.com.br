using System;
using System.Collections.Generic;

using Squid;
using Squid.Controls;

using GameController;


public class RadialSubMenuControl : OptionsMenu {
		
	protected const int BORDER_IMAGE_SIDE_SIZE_X = 61;
	
	protected const int BORDER_IMAGE_SIDE_SIZE_Y = 64;
	
	protected const int BORDER_IMAGE_SIDE_SIZE_BIG = 128;
	
	protected const string BORDER_TOP_RIGHT_IMAGE_SMALL = "GUI/ActionsMenu/ActionsSubMenu/LightBorder";
	
	protected const string BORDER_TOP_RIGHT_IMAGE = "GUI/ActionsMenu/ActionsSubMenu/LightBorderBig";
	
	protected OrientedImageControl bottomBorder;
	
	protected OrientedImageControl topBorder;
	
	protected Quadrant quadrant;
	public Quadrant Quadrant {
		get { return quadrant; }
		set { quadrant = value; }
	}
	
	
	public RadialSubMenuControl() {

		bottomBorder = new OrientedImageControl();
		bottomBorder.NoEvents = true;
		bottomBorder.AllowFocus = false;
		
		topBorder = new OrientedImageControl();
		topBorder.NoEvents = true;
		topBorder.AllowFocus = false;
	}
	
	
	protected override OptionButton CreateButton( MenuOption option, object buttonData ) {
		// TODO: usando tipo do ÍCONE para teste?? Podrão...
		if( option.icon == IconType.RELOAD ) {
			return new ReloadSubMenuButton( option, buttonData );	
		} else {
			return new RadialSubMenuButton( option );
		}
	}
	
	
	public void Clear() {
		foreach( Control button in buttons )
			button.Parent = null;
			
		buttons.Clear();
		topBorder.Parent = null;
		bottomBorder.Parent = null;
	}
	
	public override void Show( Point position ) {
		Visible = true;
		
		Size = new Point( RadialSubMenuButton.BUTTON_WIDTH, ( buttons.Count - 1 ) * RadialSubMenuButton.BUTTON_PADDING );
		int totalButtonsHeight = 0;
		foreach( OptionButton button in buttons ) {
			totalButtonsHeight += button.Size.y;
		}
		Size += new Point( 0, totalButtonsHeight );
			 
		if( buttons.Count <= 2 ) {
			bottomBorder.Texture = BORDER_TOP_RIGHT_IMAGE_SMALL;
			topBorder.Texture = BORDER_TOP_RIGHT_IMAGE_SMALL;
			bottomBorder.Size = new Point( BORDER_IMAGE_SIDE_SIZE_X, BORDER_IMAGE_SIDE_SIZE_Y );
			topBorder.Size = new Point( BORDER_IMAGE_SIDE_SIZE_X, BORDER_IMAGE_SIDE_SIZE_Y );
		} else {
			bottomBorder.Texture = BORDER_TOP_RIGHT_IMAGE;
			topBorder.Texture = BORDER_TOP_RIGHT_IMAGE;
			bottomBorder.Size = new Point( BORDER_IMAGE_SIDE_SIZE_BIG, BORDER_IMAGE_SIDE_SIZE_BIG );
			topBorder.Size = new Point( BORDER_IMAGE_SIDE_SIZE_BIG, BORDER_IMAGE_SIDE_SIZE_BIG );
		}
		
		switch( quadrant ) {
		case Quadrant.NORTHEAST:
			Position = position - new Point( 0, Size.y );
			break;
			
		case Quadrant.NORTHWEST:
			Position = position - Size;
			break;
			
		case Quadrant.SOUTHEAST:
			Position = position;
			break;
			
		case Quadrant.SOUTHWEST:
			Position = position - new Point( Size.x, 0 );
			break;
		}
		
		switch( quadrant ) {
		case Quadrant.NORTHEAST:
		case Quadrant.SOUTHWEST:
			topBorder.Position = new Point( Size.x, 0 ) - new Point( topBorder.Size.x, 0 ) + new Point( 8, -8 );
			bottomBorder.Position = new Point( 0, Size.y ) - new Point( 0, bottomBorder.Size.y ) + new Point( -8, 8 );
			topBorder.Orientation = OrientedImageControl.ImageOrientation.NO_ROTATION;
			bottomBorder.Orientation = OrientedImageControl.ImageOrientation.ROTATE_180_DEGREES;
			break;
			
		case Quadrant.NORTHWEST:
		case Quadrant.SOUTHEAST:
			topBorder.Position = new Point( 0, 0 ) + new Point( -8, -8 );
			bottomBorder.Position = new Point( Size.x, Size.y ) - new Point( topBorder.Size.x, bottomBorder.Size.y ) + new Point( 8, 8 );
			topBorder.Orientation = OrientedImageControl.ImageOrientation.ROTATE_270_DEGREES;
			bottomBorder.Orientation = OrientedImageControl.ImageOrientation.ROTATE_90_DEGREES;
			break;
		}
		
		
		for( int i = 0; i < buttons.Count; i++ ) {
			buttons[ i ].Position = new Point( 0, i * ( RadialSubMenuButton.BUTTON_HEIGHT + RadialSubMenuButton.BUTTON_PADDING ) );
			Controls.Add( buttons[ i ] );
		}
		
		Controls.Add( bottomBorder );
		Controls.Add( topBorder );
	}	
	
	public void Close() {
		Visible = false;
		Clear();
	}
}
