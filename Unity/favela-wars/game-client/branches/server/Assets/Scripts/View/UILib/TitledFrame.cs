using System;
using Utils;
using Squid;
using UnityEngine;

public class TitledFrame : Frame {
	
	private Label label;
	private Squid.Controls.ImageControl framingLeft;
//	private Squid.Controls.ImageControl framingRight;
	private Squid.Controls.ImageControl framingStroke;
	
	
	public const int CaptionSizeY = 20;
	public const int CaptionSizeX = 70;
	public const int CaptionPosY = 0;
	public const int CaptionPosX = 25;
	public const int BottomPaddingY = 15;
	public const int ScrollWidth = 15;
	public const int FramePosY = 10;
	public const int FramePosX = 5;
	public const int ScrollHeight = 5;
	public const int framePadding = 10;
	public const int textMargin = 10;
	public const int spaceBetweenFrameAndScroll = 7;
//	protected override void DrawStyle (Style style, float opacity) {
//		base.DrawStyle (style, opacity);
//		
//		int texId = GuiHost.Renderer.GetTexture( "ListaInimigos/Division.png" );
//		Squid.UVCoords coords;
//		coords = new Squid.UVCoords( 0.0f, 0.0f, 1.0f, 1.0f );
//		GuiHost.Renderer.DrawTexture( texId, 10, 10, 255, 32, coords, -1 );
//		
//		coords = new Squid.UVCoords( 1.0f, 1.0f, 0.0f, 0.0f );
//		GuiHost.Renderer.DrawTexture( texId, 0, Size.y - 128 - 21, 512, 128, coords, -1 );
//		
//	}
	
	public void setTitleAndSize( String title, Point newSize ) {
		
		Size = newSize;
		label.Text = title;
		label.Size = new Point( framePadding + textMargin, framePadding );
		
		framingLeft.Texture = "ListaInimigos/Division.png";
		framingLeft.Size = new Point( ( Size.x ) - framingLeft.Position.x - 2 * framePadding - spaceBetweenFrameAndScroll, 37 );
		
//		framingRight.Texture = "ListaInimigos/Division.png";
//		framingRight.Position = new Point( ( - Size.x ) - 10, 15 );
//		framingRight.Size = new Point( ( ( Size.x * 2 ) - framingLeft.Position.x ), 32 );
		
		framingStroke.Texture = "ListaInimigos/DivisionLine.png";
		//framepadding * 2 = espaço antes do texto + espaço depois do texto.
		framingStroke.Position = new Point( CaptionPosX + 2 * framePadding + GuiHost.Renderer.GetTextSize( label.Text, GuiHost.Renderer.GetFont("Fonte_Nome01") ).x, FramePosY );
		//framepadding * 3 = espaço antes do texto + espaço depois do texto.+ espaço depois do framing.
		framingStroke.Size = new Point( Size.x - framingStroke.Position.x - 3 * framePadding , 2 );
	}
	
	protected override void Initialize () {
		base.Initialize ();
		
		framingLeft = new Squid.Controls.ImageControl();
		framingLeft.Parent = this;
		framingLeft.Position = new Point( FramePosX, FramePosY );
		
//		framingRight = new Squid.Controls.ImageControl();
//		framingRight.Parent = this;
//		framingRight.Position = new Point( 10, 15 );
		
		framingStroke = new Squid.Controls.ImageControl();
		framingStroke.Parent = this;
		framingStroke.Position = new Point( framePadding, FramePosY );
		
		label = new Label();
		label.Parent = this;
		label.AutoSize = AutoSize.HorizontalVertical;
		label.Position = new Point( CaptionPosX, CaptionPosY );
		label.Style = "FontFrameTitle";
	}	
}

