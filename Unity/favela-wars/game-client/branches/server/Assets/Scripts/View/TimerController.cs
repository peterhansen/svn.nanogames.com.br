using UnityEngine;
using System.Collections;
using System;
using Utils;
using GameController;
using GameCommunication;

public class TimerController : MonoBehaviour {
	
	public GUIStyle timerStyle;
	
	public float countDownSeconds;
	
	private Vector3 buttonScale;
    private Vector3 maxScale;
	public float scaleAnimationFactor;
	private bool shouldAnimate;
	
	private float startTime;
	private float restSeconds;
	private float roundedRestSeconds;
	private float displaySeconds;
	private float displayMinutes;
	
	private int displayedLines;
	private float proportion;
	private float actualFOV;
	
	public bool countTimer;
	private bool timerSelected;
	
	public static float TurnTimeInSeconds = 60f;
	
	// Use this for initialization
	void Start () {
		timerSelected = false;
		countTimer = true;
		countDownSeconds = TimerController.TurnTimeInSeconds;
		startTime = Time.time;
		
		scaleAnimationFactor = 10f;
		
		proportion = transform.localScale.x / IsoCameraScript.GetInstance().camera.orthographicSize;
		
		buttonScale = new Vector3( proportion * actualFOV, 
								   proportion * actualFOV, 
								   proportion * actualFOV);
		
		maxScale = new Vector3( gameObject.transform.localScale.x + scaleAnimationFactor, 
								gameObject.transform.localScale.y + scaleAnimationFactor, 
								gameObject.transform.localScale.z + scaleAnimationFactor);
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.position = IsoCameraScript.GetInstance().PickCameraRightCorner();
		actualFOV = IsoCameraScript.GetInstance().camera.orthographicSize;
		
		buttonScale = new Vector3( proportion * actualFOV, 
								   proportion * actualFOV, 
								   proportion * actualFOV);
		
		transform.localScale = buttonScale;
		transform.localRotation = IsoCameraScript.GetInstance().camera.transform.localRotation;
		
		
		renderer.material.SetFloat("_Cutoff", Mathf.InverseLerp(0, countDownSeconds, restSeconds));
		
		if(shouldAnimate){
			gameObject.transform.localScale = new Vector3( gameObject.transform.localScale.x + 1f, 
														   gameObject.transform.localScale.y + 1f, 
														   gameObject.transform.localScale.z + 1f);
			
			if(gameObject.transform.localScale.x >= maxScale.x){
				shouldAnimate = false;
			}
			
		}
		
		if(countTimer){
			//Timer Stuff
			float guiTime = Time.time - startTime;
			
			restSeconds = ( ( countDownSeconds - (guiTime) ) > 0f) ? ( countDownSeconds - (guiTime) ) : 0f;
			
			if(restSeconds == 60f){
				Debugger.Log ("One Minute Left");
			}
			
			if(restSeconds <= 0f){
				Debugger.Log ("Time is Over");
			}
			
			roundedRestSeconds = Mathf.CeilToInt(restSeconds);
			displaySeconds = roundedRestSeconds % 60;
			displayMinutes = roundedRestSeconds / 60;
		
			if(restSeconds <= 0f){
				OnTimerEnd();
			}
		}
		
		if (Input.GetMouseButtonUp(0)) {
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hitInfo;
			
			Debug.Log ("Left click");
        	if (Physics.Raycast( ray, out hitInfo )){
				if(hitInfo.transform == transform){
					Debugger.Log ("Hit it!");
					if(!timerSelected){
						timerSelected = true;
						shouldAnimate = true;
					}
					else{
						OnTimerEnd();
					}
				}
			}
			else{
				timerSelected = false;
				gameObject.transform.localScale = buttonScale;
			}
		}
	}
	
//	void OnGUI(){
//		string text = string.Format("{0:00}:{1:00}", displayMinutes, displaySeconds);
//		//Debug
//		GUI.Label(new Rect(0f, 0f, 100, 30), text);
//	}
	
	public void OnTimerEnd(){
		roundedRestSeconds = 0f;
		countDownSeconds = 0f;
		countTimer = false;
		
		GameManager.GetInstance().SendRequest( new EndParticipationRequest( GameManager.GetInstance().GetLocalPlayer() ) );
	}
	
	public void OnTimerSelected(){
		
	}
	
	public void ResetTimer(){
		countDownSeconds = TimerController.TurnTimeInSeconds;
		startTime = Time.time;
		countTimer = true;
		timerSelected = false;
		shouldAnimate = false;
		restSeconds = countDownSeconds;
		gameObject.transform.localScale = buttonScale;
	}
	
	public void StopTimer(){
		countTimer = false;
	}
	
	public void RestartTimer(){
		startTime = Time.time - restSeconds;
		countTimer = true;
	}
	
	public void HideTimer(){
		Renderer[] renderers = gameObject.GetComponentsInChildren< Renderer > ();
		foreach(Renderer r in renderers){
			r.renderer.enabled = false;
		}
		
	}
	
	public void ShowTimer(){
		Renderer[] renderers = gameObject.GetComponentsInChildren< Renderer > ();
		foreach(Renderer r in renderers){
			r.renderer.enabled = true;
		}
	}
}
