using System;

using Squid;

using GameController;


public class RadialSubMenuButton : OptionButton {
	
	public const int BUTTON_WIDTH = 200;
	
	public const int BUTTON_HEIGHT = 38;
	
	public const int BUTTON_PADDING = 3;
	
	public const int BUTTON_TEXT_LEFT_MARGIN = 20;
	
	
	public RadialSubMenuButton( MenuOption option ) : base( option ) {
		Size = new Point( BUTTON_WIDTH, BUTTON_HEIGHT );
		Padding = new Margin( BUTTON_PADDING, BUTTON_PADDING, BUTTON_PADDING, BUTTON_PADDING );
		
		clickableArea.Style = "RadialSubMenuButton";
		clickableArea.TextAlign = Alignment.MiddleLeft;
		clickableArea.Opacity = 1.0f;
		clickableArea.Size = Size;
	}
	
	
	public override void AddButtonContents() {
		Label text = new Label();
		text.Text = L10n.Get( menuOption.stringCode );
		text.AutoSize = AutoSize.Horizontal;
		text.Size = new Point( text.Size.x - BUTTON_TEXT_LEFT_MARGIN, Size.y - 2 * BUTTON_PADDING );
		text.TextAlign = Alignment.MiddleLeft;
		text.Position = new Point( BUTTON_TEXT_LEFT_MARGIN, 0 );
		text.AllowFocus = false;
		text.NoEvents = true;
		Controls.Add( text );
	}
}

