using System;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;


public class ShowMessageControl : Frame {
	
	protected ShowMessageData data;
	
	protected Label textContent;
	protected Button closeButton;
	
	private InteractionController parent;
	
	

	
	protected override void Initialize () {
		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}
		
		Position = new Point( 10, 10 );
		Size = new Point( 300, 300 );
		Parent = guiManager.Desktop;
		
		textContent = new Label();
		textContent.Position = new Point( 10, 10 );
		textContent.Size = new Point( 190, 190 );
		Controls.Add( textContent );
		Style = "holoPanel";
		closeButton = new Button();
		closeButton.Position = new Point( 200, 200 );
		closeButton.Size = new Point( 100, 30 );
		closeButton.Text = "Fechar";
		closeButton.OnMouseClick += new MouseClickEventHandler( Close );
		Controls.Add( closeButton );
	}
	
	public ShowMessageControl( InteractionController parent, ShowMessageData data ) {
		this.data = data;
		this.parent = parent;
		textContent.Text = data.GetMessage();
		//parent.controls.Add( this );
	}
	
	protected void Close( Control Sender ) {
		GUIManager.SetInteractionController( parent );
		//parent.OnDone();
		//parent.OnDone();
	}
	
//	public void StartRenderer() {
//	}
	
//	public bool RenderGUI() {
//		bool done = false;
//		
//		GUI.BeginGroup( new Rect( 20.0f, 50.0f, 300.0f, 300.0f ) );
//		GUI.Box( new Rect( 0.0f, 0.0f, 300.0f, 300.0f ), "info" );
//		
//		GUI.Label( new Rect( 30.0f, 30.0f, 230.0f, 20.0f ), data.GetMessage() );
//		
//		if ( GUI.Button( new Rect( 150.0f, 240.0f, 100.0f, 50.0f ), "ocultar" ) ) {
//			if ( parent != null ) {
//				parent.OnDone();
//			} 
//			done = true;
//		}
//		
//		GUI.EndGroup();
//		
//		return done;
//	}
}


