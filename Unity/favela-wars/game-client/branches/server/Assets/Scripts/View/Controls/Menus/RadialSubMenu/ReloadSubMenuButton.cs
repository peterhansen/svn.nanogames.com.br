using System;

using Squid;

using Utils;
using GameCommunication;


public class ReloadSubMenuButton : RadialSubMenuButton {
	
	protected const int PADDING = 10;
			
	protected const string RELOAD_BUTTON_IMAGE = "GUI/ActionsMenu/ActionsSubMenu/BtReload";
	
	protected const int RELOAD_BUTTON_SIZE = 42;
	
	protected ReloadGameAction reloadGameAction;
	
	protected OrientedImageControl buttonImage;
	
	public ReloadSubMenuButton( MenuOption option, object buttonData ): base( option ) {
		reloadGameAction = buttonData as ReloadGameAction;
		if( reloadGameAction == null )
			Debugger.Log( "ReloadSubMenuButton: no reload info." );
	}
	
	public override void AddButtonContents() {
		// adicionando barra de munição
		AmmoBar ammoBar = new AmmoBar( 100 );
		if( reloadGameAction != null )
			ammoBar.SetCurrentValue( (int) ( reloadGameAction.ammoPercentage * 100.0f ) );
		Controls.Add( ammoBar );
		
		// adicionando imagem do botão
		buttonImage = new OrientedImageControl();
		buttonImage.Texture = RELOAD_BUTTON_IMAGE;
		buttonImage.Size = new Point( RELOAD_BUTTON_SIZE, RELOAD_BUTTON_SIZE );
		buttonImage.Position = new Point( BUTTON_WIDTH - PADDING - RELOAD_BUTTON_SIZE, ( BUTTON_HEIGHT - RELOAD_BUTTON_SIZE ) >> 1 );
		Controls.Add( buttonImage );
	}
	
	protected class AmmoBar : Frame {
		
		protected const int PADDING_X = 9;
		
		protected const int PADDING_Y = 8;
		
		protected const int AMMO_BAR_SIZE_X = 103;
		
		protected const int AMMO_BAR_SIZE_Y = 34;
		
		protected const int AMMO_BAR_FILL_SIZE_Y = 19;
				
		protected int maxValue;
		
		protected int currentValue = 0;
		
		protected Frame fill;
		
		public AmmoBar( int maxValue ) {
			this.maxValue = maxValue;
						
			Style = "AmmoBarBackground";
			
			fill = new Frame();
			fill.Style = "AmmoBarFill";
			fill.Position = new Point( PADDING_X, PADDING_Y );
			Controls.Add( fill );
						
			OnSizeChanged += new SizeChangedEventHandler( OnSizeChangedHandler );
			Size = new Point( AMMO_BAR_SIZE_X, AMMO_BAR_SIZE_Y );
		}
		
		public void SetCurrentValue( int currentValue ) {
			this.currentValue = currentValue;
			UpdateFillSize();
		}
		
		protected void OnSizeChangedHandler( Control sender ) {
			UpdateFillSize();
		}
		
		protected void UpdateFillSize() {
			fill.Size = new Point( ( ( currentValue * ( AMMO_BAR_SIZE_X - 2 * PADDING_X + 1 ) ) / maxValue ), AMMO_BAR_FILL_SIZE_Y );
		}
	}
}


