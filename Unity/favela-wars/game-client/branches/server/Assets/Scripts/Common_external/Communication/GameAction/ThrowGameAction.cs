using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class ThrowGameAction : GameAction {
		
		public int itemID;
		
		
		public ThrowGameAction( int itemID ) : base( ActionType.THROW ) {
			this.itemID = itemID;
		}
		
	}

}

