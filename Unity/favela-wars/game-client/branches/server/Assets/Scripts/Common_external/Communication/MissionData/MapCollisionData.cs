using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using SharpUnit;

using Utils;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe armazena os dados dos dados de colisão de um mapa. 
	/// </summary>
	[ Serializable ]
	public class MapCollisionData {
		
		private string missionName;
		
		private List< Box > boxes;
		
		private SectorsData sectorsData;
		
		private byte[] rawAstarData;
		
		
		public MapCollisionData() {
		}
		
		
		public MapCollisionData( string missionName, List< Box > boxes, SectorsData sectorsData, byte[] rawAstarData ) {
			this.missionName = missionName;
			this.sectorsData = sectorsData;
			this.boxes = boxes;
			this.rawAstarData = rawAstarData;
		}
		
				
		public override bool Equals( object obj ) {
			return Equals( ( ( MapCollisionData ) obj ) );
		}
		
		
		public bool Equals( MapCollisionData other ) {
			return other != null && missionName.Equals( other.GetMissionName() );
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode ();
		}
		
		
		public string GetMissionName() {
			return missionName;
		}

		
		public List< Box > GetBoxes() {
			return boxes;
		}
		
		
		public SectorsData GetSectorsData() {
			return sectorsData;
		}
		
		
		public byte[] GetRawAstarData() {
			return rawAstarData;
		}
		
		
		public string GetMapCollisionFilePath( bool fullPath ) {
			string path;
			
			if ( fullPath ) {
				path = GetPath( missionName );
				Directory.CreateDirectory( path );
			} else {
				path = string.Format( "missions/{0}", missionName );
			}
			
			return Path.Combine( path, Common.LEVEL_FILENAME );
		}
		
		
		public void ExportToFile() {
			string localPath = GetMapCollisionFilePath( true );
			string s3Path = GetMapCollisionFilePath( false );
			
			Util.SerializeToFile( this, localPath );
			Util.SaveToS3( localPath, s3Path );
		}
		
		
		public static MapCollisionData ImportFromFile( string filename ) {
			string s3Path = string.Format( "missions/{0}/{1}", filename, Common.LEVEL_FILENAME );
			return Util.DeserializeFromS3< MapCollisionData >( s3Path );
		}
		
		
		public static string GetPath( string missionName ) {
			return Path.Combine( Path.Combine( Common.Paths.SERVER, "missions" ), missionName );
		}
		
	}
	
	
}


public class InvalidMapCollisionFile : Exception {
	
	public InvalidMapCollisionFile() : this( "Arquivo descritor de colisões de mapa inválido." ) {
	}
	
	
	public InvalidMapCollisionFile( string message ) : base( message ) {
	}
	
}
