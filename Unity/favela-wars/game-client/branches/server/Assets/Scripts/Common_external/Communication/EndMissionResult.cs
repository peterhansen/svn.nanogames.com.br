using System;

using Utils;

namespace GameCommunication {
	
	[Serializable]
	public class EndMissionResult {
		
		public bool isDone;
		public Faction winningFaction;
		
		
		public EndMissionResult() {
			this.isDone = false;
			this.winningFaction = Faction.NONE;
		}
		
		
		public EndMissionResult( bool isDone, Faction winningFaction ) {
			this.isDone = isDone;
			this.winningFaction = winningFaction;
		}
	}
}

