using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class ReloadGameAction : GameAction {
		
		public int weaponID;
		
		public float ammoPercentage;
		
		public ReloadGameAction( int weaponID, float ammoPercentage ) : base( ActionType.RELOAD ) {
			this.weaponID = weaponID;
			this.ammoPercentage = ammoPercentage;
		}
	}
}

