using System;
using System.Collections;
using System.Collections.Generic;

using Utils;

namespace GameCommunication {
	
	[ Serializable ]
	public class SpawnAreaMissionData : AreaMissionData {
		
		public int minObjects = 0;
	
		public int capacity = 1;
	
		public Faction faction = Faction.NEUTRAL;
		
		public WayPointCollection waypointCollection = new WayPointCollection();
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as SpawnAreaMissionData );
		}
		
		
		public bool Equals( SpawnAreaMissionData other ) {
			return other != null && minObjects == other.minObjects && capacity == other.capacity &&
				   faction == other.faction;
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
			
		
		public void Randomize() {
			box.GetGameTransform().Randomize();
		}
		
	}
}

