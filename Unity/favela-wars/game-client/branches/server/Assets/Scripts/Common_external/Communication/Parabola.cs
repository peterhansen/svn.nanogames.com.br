using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	/// <summary>
	/// Classe que descreve uma parábola no formato y = at² + bt + c.
	/// </summary>
	public class Parabola {
		
		public GameVector origin;
		
		public GameVector direction;
		
		
		/// <summary>
		/// Gets a.
		/// </summary>
		/// <value>
		/// A.
		/// </value>
		public float a {
			get {
				return Common.GRAVITY;
			}
		}
		
		
		/// <summary>
		/// Gets the b.
		/// </summary>
		/// <value>
		/// The b.
		/// </value>
		public float b {
			get {
				return direction.y;
			}
		}
		
		
		/// <summary>
		/// Gets the c.
		/// </summary>
		/// <value>
		/// The c.
		/// </value>
		public float c {
			get {
				return origin.y;
			}
		}
		
		
		/// <summary>
		/// Gets the delta.
		/// </summary>
		/// <value>
		/// The delta.
		/// </value>
		public float delta {
			get {
				return b * b - ( 4 * a * c );
			}
		}
		
		
		/// <summary>
		/// Gets the y max.
		/// </summary>
		/// <value>
		/// The y max.
		/// </value>
		public float yMax {
			get {
				// -delta / 4a
				return -delta / ( 4 * a );
			}
		}
		
		
		/// <summary>
		/// Obtém o instante de tempo do ápice da parábola (ponto de inflexão)
		/// </summary>
		public float tMax {
			get {
				return b / ( a * -2.0f );
			}
		}
			
		
		public Parabola( GameVector origin, GameVector direction ) {
			this.origin = origin;
			this.direction = direction;
		}
		
		
		public Parabola( Parabola p ) : this( p.origin, p.direction ) {
		}
		
		
		/// <summary>
		/// Obtém o(s) instante(s) em que a curva toca ou cruza o ponto y. Podem ser retornados 0, 1 ou 2 instantes,
		/// dependendo dos parâmetros da parábola.
		/// </summary>
		public List< float > GetTimes( float y ) {
			List< float > roots = new List< float >();
			
			float c2 = c - y;
			
			float deltaSquare = ( float ) Math.Sqrt( b * b - 4.0f * a * c2 );
			
			float a2 = a * 2.0f;
			float T1 = ( -b + deltaSquare ) / a2;
			float T2 = ( -b - deltaSquare ) / a2;
			
			if ( !float.IsNaN( T1 ) )
				roots.Add( T1 );
			
			if ( !float.IsNaN( T2 ) )
				roots.Add( T2 );
				
			return roots;
		}
		
		
		/// <summary>
		/// Gets the time.
		/// </summary>
		/// <returns>
		/// The time.
		/// </returns>
		/// <param name='y'>
		/// Y.
		/// </param>
		public float GetTime( float y ) {
			List< float > times = GetTimes( y );
			
			foreach ( float t in times ) {
				if ( t > 0 )
					return t;
			}
			
			return -1;
		}
		
		
		/// <summary>
		/// Obtém a posição y no instante "time" segundos.
		/// </summary>
		public float GetYAt( float time ) {
			return ( a * time * time ) + ( b * time ) + c;
		}
		
		
		/// <summary>
		/// Obtém o ponto da parábola no instante "time" segundos.
		/// </summary>
		public GameVector GetPointAt( float time ) {
			return new GameVector( origin.x + ( direction.x * time ), 
								   GetYAt( time ),
								   origin.z + ( direction.z * time ) );
		}
		
		
		/// <summary>
		/// Obtém a velocidade y no instante "time".
		/// </summary>
		public float GetYSpeedAt( float time ) {
			return b + a * 2.0f * time;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>
		/// 
		/// </returns>
		/// <param name='time'>
		/// 
		/// </param>
		public GameVector GetSpeedAt( float time ) {
			return new GameVector( direction.x, GetYSpeedAt( time ), direction.z );
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[Parabola origin:" );
			sb.Append( origin );
			sb.Append( " direction:" );
			sb.Append( direction );
			sb.Append( "]" );
			
			return sb.ToString();
		}
		
	}
}

