using System;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class SetCharacterInfoRequest : Request {
		
		public int worldObjectID;
		
		public CharacterInfoData characterData;
		
		
		public SetCharacterInfoRequest( int playerID, int worldObjectID, CharacterInfoData characterData ) : base( playerID ) {
			this.worldObjectID = worldObjectID;
			this.characterData = characterData;
		}
		
	}
}

