using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para obter o estado de uma partida.
	/// </summary>
	[ Serializable ]
	public class GetStateRequest {
		
		public GetStateRequest() {
		}
	
	}
}

