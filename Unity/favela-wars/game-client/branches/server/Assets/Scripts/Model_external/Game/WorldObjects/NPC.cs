using System;

using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class NPC : Character {
		
		
		#if UNITY_EDITOR
		// necessário para serializar a classe no editor
		public NPC() {
		}
		#endif
		
		
		public NPC( int gameObjectID ) : this( gameObjectID, "ffff00ff" ) {
			Data.Faction = Faction.NEUTRAL;
		}
		
		
		public NPC( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID ) {
		}
		
	}
	
}

