using System;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class GetCharacterMovementHandler : Handler {
		
		protected GetCharacterMovementRequest request;
		
		public GetCharacterMovementHandler( GetCharacterMovementRequest request ) {
			this.request = request;
		}

		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetCharacter( request.characterWorldID );
			
			if ( character == null || mission.GetPlayerId( character ) != request.GetPlayerID() ) {
				Debugger.Log( "GetCharacterActionsHandler: jogador tentando mover personagem fora de sua tropa." );
				return null;
			}
			
			Response response = new Response();
			GameAction movementGameAction = character.GetMovementGameAction();
			if( movementGameAction == null )
				return null;
			
			response.AddGameActionData( new GetCharacterActionsData( request.characterWorldID, movementGameAction ) );
			return response;
		}
	}
}

