using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;



namespace GameModel {
	
	public class ReloadHandler : Handler {
		
		
		protected ReloadRequest request;
		
		
		public ReloadHandler( ReloadRequest request ) {
			this.request = request;
		}


		public override Response GetResponse() {
			int playerID = request.GetPlayerID();
			Mission mission = Mission.GetInstance( playerID );
			Character character = mission.GetCharacter( request.characterWorldID );
			
			if( !CheckIfCharacterIsUnderPlayerControl( mission, playerID, character ) ) {
				Debugger.LogWarning( "Jogador tentando atirar com personagem fora de sua tropa. PlayerID: " + playerID + ", characterID: " + request.characterWorldID );
				return null;
			}
			
			FireArm weapon = ( FireArm ) character.GetItemByEquipID( request.weaponID );
			if ( weapon == null ) {
				if ( weapon is Weapon )
					Debugger.LogError( "Wrong weapon type to reload: " + character.GetItemByEquipID( request.weaponID ) );
				
				Debugger.LogError( "Invalid weaponID: " + request.weaponID );
				return null;
			}
			
			// verifica se o personagem tem AP para atacar; se tiver, desconta seu AP
			float apCost = weapon.ReloadTime * Common.CharacterInfo.RELOAD_TIME_TO_AP_RATE;
			
			Response r = new Response();
			if ( apCost > character.Data.ActionPoints ) {
				AddInsufficientAPMessage( mission, r, playerID, character.GetWorldID() );
				return r;
			}
			
			if ( character.Reload( weapon.EquipID ) ) {
				character.Data.ActionPoints.Value -= ( int ) Math.Round( apCost );
			} else {
				AddMessage( mission, r, playerID, character.GetWorldID(), "Não há munição disponível para esta arma no inventário." );
			}
			
			return r;
		}
	}
	

}

