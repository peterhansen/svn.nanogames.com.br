using System.Collections;
using GameCommunication;
using Utils;


namespace GameModel {
	public class SetStanceHandler : Handler {
		
		protected SetStanceRequest request;
		
		public SetStanceHandler( SetStanceRequest request ) {
			this.request = request;
		}

		public override Response GetResponse() {
			Response r;
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetWorldObject( request.worldObjectID ) as Character;

			if( !CheckIfCharacterIsUnderPlayerControl( mission, request.GetPlayerID(), character ) ) {
				Debugger.LogWarning( "Jogador tentando modificar pose personagem fora de sua tropa. PlayerID: " + request.GetPlayerID() + ", characterID: " + request.worldObjectID );
				return null;
			}
			
			r = new Response();
			character.SetStance( request.newStance );
			r.AddGameActionData( new SetStanceData( character.GetWorldID(), character.GetStance() ) );
			return r;
		}
	}
}