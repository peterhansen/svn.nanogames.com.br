using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;

namespace GameModel {
	
	public class UpdateVisibilityHandler : Handler {
		
		protected UpdateVisibilityRequest updateVisibilityRequest;
		
		public UpdateVisibilityHandler( UpdateVisibilityRequest updateVisibilityRequest ) {
			this.updateVisibilityRequest = updateVisibilityRequest;
		}

		public override Response GetResponse() {
			Response r = new Response();
			
			UpdateVisibilityData visibilityData = new  UpdateVisibilityData( updateVisibilityRequest.watcherId );
			r.AddGameActionData( visibilityData );
			return r;
		}
	}
}
