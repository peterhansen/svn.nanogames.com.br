using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;

namespace GameModel {
	
	public class Map {
		
		protected Dictionary< SpawnPoint, int > spawnPoints = new Dictionary< SpawnPoint, int >();
		
		protected CollisionManager collisionManager;
		
		
		public Map( string levelName ) {
			collisionManager = new CollisionManager( levelName );
		}
		
		
		public CollisionManager GetCollisionManager() {
			return collisionManager;
		}
		

		public void OnWorldObjectMoved( WorldObject obj, VisibilityManager hint ) {
			collisionManager.OnBoxMoved( obj.GetBox() );
		}
		
		
		public SectorsInfo GetSectorsInfo() {
			int missionWidthInSectors, missionHeightInSectors, missionLengthInSectors;
			collisionManager.GetSectorManager().GetDimensionInSectors( out missionWidthInSectors, out missionHeightInSectors, out missionLengthInSectors );
			
			return new SectorsInfo( 
				collisionManager.GetSectorManager().GetMinPoint(), 
				collisionManager.GetSectorManager().GetMaxPoint(), 
				missionWidthInSectors, missionHeightInSectors, missionLengthInSectors );
		}
	}
}

