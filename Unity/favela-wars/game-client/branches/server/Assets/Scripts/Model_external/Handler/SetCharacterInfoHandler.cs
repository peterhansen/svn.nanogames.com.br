using System.Collections;
using GameCommunication;
using Utils;


namespace GameModel {
	
	public class SetCharacterInfoHandler : Handler {
	
		protected SetCharacterInfoRequest request;
		
		
		public SetCharacterInfoHandler( SetCharacterInfoRequest request ) {
			this.request = request;
		}

		
		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			Character character = mission.GetCharacter( request.worldObjectID );

			if ( character == null ) {
				// personagem não encontrado, ou não pode ser mexido pelo jogador
				Debugger.LogWarning( "Personagem não encontrado: " + request.worldObjectID );
				return null;
			}
			
			character.Data = request.characterData;
			
			return null;
		}
	}
}