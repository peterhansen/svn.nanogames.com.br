using System;
using System.Collections.Generic;

using Utils;

using GameCommunication;


namespace GameModel {
	
	public class StartMissionHandler : Handler {
		
		protected StartMissionRequest request;
		
		
		public StartMissionHandler( StartMissionRequest startMissionRequest ) {
			this.request = startMissionRequest;
		}
		
		
		public override Response GetResponse() {
			Response response = new Response();
			Debugger.Log( "Creating Mission: " + request.missionName );
			
			// TODO: por enquanto, apenas com esses dois jogadores marretados.
			List< int > ids = new List<int>();
			ids.Add( request.GetPlayerID() );
			ids.Add( request.GetPlayerID() + 1 );
			
			Mission mission = Mission.CreateInstance( ids, request.missionName, request.soldierIDs );

			StartMissionData startMissionData = new StartMissionData( 
				mission.GetName(), 
				( int ) mission.GetData().levelOwner,
				mission.GetTurnManager().GetTurnData(), 
				mission.GetMap().GetSectorsInfo(),
				mission.GetRawAstarData()
			);
			MissionData missionData = mission.GetData();
			
			// aqui iremos adicionar todos os Base Asset bundles usados na cena.
			// Esses Asset Bundles contém dentro deles os descritores que vão dizer quais
			// são os outros asset bundles que precisam ser baixados.
			foreach ( string assetBundlePath in missionData.meshesAssetBundles.Keys )
				startMissionData.assetData.Add( new AssetBundleData( assetBundlePath, assetBundlePath, 1 ) ); // TODO gerenciar os nomes, caminhos e versões dos asset bundles gerados pelo exportador
			
			foreach ( string assetBundlePath in missionData.materialsAssetBundles.Keys )
				startMissionData.assetData.Add( new AssetBundleData( assetBundlePath, assetBundlePath, 1 ) ); // TODO gerenciar os nomes, caminhos e versões dos asset bundles gerados pelo exportador
			
			startMissionData.assetData.Add( new AssetBundleData( missionData.objectsBundlePath, missionData.objectsBundlePath, 1 ) ); // TODO gerenciar os nomes, caminhos e versões dos asset bundles gerados pelo exportador
			response.AddGameActionData( startMissionData );
			
			VisibilityManager visibilityManager = mission.GetVisibilityManager();
			Faction firstTurnFaction = mission.GetTurnManager().GetTurnData().faction;
			
			foreach( Character character in mission.GetCharacters() ) {
				CharacterInfoData characterInfoData = new CharacterInfoData();
				characterInfoData.Faction = character.Data.Faction;
				response.AddGameActionData( new CreateGameObjectData( character.GetWorldID(), character.GetLayer(), characterInfoData, new AssetAssemblyData() ) );
				
				// calcula a visibilidade inicial do personagem
				switch ( character.Data.Faction ) {
					case Faction.POLICE:
					case Faction.CRIMINAL:
						visibilityManager.CalculateVisibilityFor( character );
					break;
				}
			}
			
//			
			VisibilityChangeData visibilityChangeData = visibilityManager.GetVisibilityFor( firstTurnFaction, true );		
			response.AddGameActionData( visibilityChangeData );
			
			return response;
		}	
	}
	
}

