using System;

using Utils;
using GameCommunication;


namespace GameModel {
	public class DestroyAllEnemies : MissionObjective {
		
		protected DestroyAllEnemiesData data = new DestroyAllEnemiesData();
		
		
		public DestroyAllEnemies( DestroyAllEnemiesData data ) {
			this.data = data;
		}
		
		
		public DestroyAllEnemies( int maxNumberOfTurns ) {
			data.maxNumberOfTurns = maxNumberOfTurns;
		}
		
		
		protected override ObjectiveResult CheckWhenCharacterKilled( Mission mission, Character killedCharacter ) {
			int policemenCount = 0;
			int criminalsCount = 0;
			//int civilianCount = 0;
			
			foreach( Character character in mission.GetCharacters() ) {
				if( !character.IsAlive() )
					continue;
				
				switch ( character.Data.Faction ) {
				case Faction.POLICE	:
					++policemenCount;
					break;
	//			case Faction.NEUTRAL:
	//				++civilianCount;
	//				break;
				case Faction.CRIMINAL:
					++criminalsCount;
					break;
				}
			}
			
			ObjectiveResult result = new ObjectiveResult();
			if( policemenCount == 0 && criminalsCount == 0 ) {
				result.missionIsOver = true;
				result.winningFaction = Faction.NONE;
			} else if( policemenCount == 0 ) {
				result.missionIsOver = true;
				result.winningFaction = Faction.CRIMINAL;
			} else if( criminalsCount == 0 ) {
				result.missionIsOver = true;
				result.winningFaction = Faction.POLICE;
			}
			
			return result;
		}
		
		
		protected override ObjectiveResult CheckWhenTurnChanged( Mission mission, TurnData turnData ) {
			ObjectiveResult result = new ObjectiveResult();
			
			if( MaxNumberOfTurnsReached( mission ) ) {
				result.missionIsOver = true;
				result.winningFaction = Faction.NONE;
			}
			
			return result;
		}
		
		
		protected bool MaxNumberOfTurnsReached( Mission mission ) {
			return mission.GetTurnManager().GetTurnData().turnNumber > data.maxNumberOfTurns && data.maxNumberOfTurns != 0;
		}
		
	}
	
}
