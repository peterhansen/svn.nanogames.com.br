using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameCommunication;
using GameController;


public abstract class SpawnArea : Area {
	
	public int capacity = 1;
	
	public int minObjects = 0;
	
	public List< GameObject > waypoints;
	
	
	public SpawnArea() {
	}
	
	
	public abstract Faction GetFaction();
	
	
	public new SpawnAreaMissionData GetMissionData() {
		SpawnAreaMissionData d = new SpawnAreaMissionData();
		
		// faz ajustes nas dimensões caso a área esteja rotacionada
		GameTransform gt = GetGameTransform();
		Bounds b = ControllerUtils.GetBox( gameObject, false );
		gt.scale.x = b.size.x;
		gt.scale.y = b.size.y;
		gt.scale.z = b.size.z;
		
		d.box.SetGameTransform( gt );
		d.faction = GetFaction();
		d.capacity = capacity;
		d.minObjects = minObjects;
		
		WayPointCollection wpCollection = new WayPointCollection();
		foreach(GameObject gameObj in waypoints){
			Vector3 vec3 = gameObj.transform.position;
			GameVector gv = new GameVector(vec3.x, vec3.y, vec3.z);
			WayPoint w = new WayPoint(gv);
			wpCollection.addWaypoint(w);
		}
		
		d.waypointCollection = wpCollection;
		
		return d;
	}
	
	
	public override bool Equals( object obj ) {
		return Equals( obj as SpawnArea );
	}
	
	
	public bool Equals( SpawnArea area ) {
		return area != null && capacity == area.capacity && minObjects == area.minObjects && 
			   GetFaction() == area.GetFaction();
	}
	
	
	public override int GetHashCode() {
		return ( capacity.ToString() + minObjects.ToString() + GetFaction().ToString() ).GetHashCode();
	}
	
}
