using System;
using System.Collections.Generic;


using UnityEngine;


namespace GameController {
	
	public class Portal {
		
		public Bounds bounds;
		
		public bool isOpen = true;
		
		
		public Portal( Bounds bounds ) {
			this.bounds = bounds;
		}
		
	}
}

