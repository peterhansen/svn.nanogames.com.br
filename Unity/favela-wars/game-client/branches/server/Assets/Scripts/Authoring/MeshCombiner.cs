using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameController;
using Utils;


public static class MeshCombiner {
	
	
	public static void CombineMeshes( IEnumerable< GameObject > list, GameObject combinedRoot ) {
		foreach ( GameObject go in EnumerateCombinedMeshes( list, combinedRoot ) ) {
		}
	}
	

	/// <summary>
	/// 
	/// </summary>
	public static IEnumerable< GameObject > EnumerateCombinedMeshes( IEnumerable< GameObject > list, GameObject combinedRoot ) {
		BuildingInfo.PrepareScene();
		
		LayerMaterialInfo[] layerMaterialInfo = new LayerMaterialInfo[ BuildingInfo.MaxLayer + 1 ];
		for ( int i = 0; i < layerMaterialInfo.Length; ++i )
			layerMaterialInfo[ i ] = new LayerMaterialInfo();
		
		int objectsCount = 0;
			
		foreach ( GameObject go in list ) {
			yield return go;
			
			objectsCount++;
			
			foreach ( MeshRenderer r in go.GetComponentsInChildren< MeshRenderer >() ) {
				if ( r.gameObject.tag.Equals( Common.Tags.MISSION ) )
					continue;
				
				int layer = BuildingInfo.GetLayer( r.gameObject );
				LayerMaterialInfo info = layerMaterialInfo[ layer ];
				
				foreach ( Material m in r.sharedMaterials ) {
					string name = ( ( m == null ) ? "null" : m.name );
					info.materialByName[ name ] = m;
					
					if ( !info.filtersPerMaterial.ContainsKey( name ) )
						info.filtersPerMaterial[ name ] = new List< MeshFilter >();
					
					info.filtersPerMaterial[ name ].AddRange( r.gameObject.GetComponents< MeshFilter >() );
				}
			}
		}
		
		
		for ( int layer = 0; layer < layerMaterialInfo.Length; ++layer ) {
			GameObject layerFolder = ControllerUtils.GetHolder( string.Format( "[{0}]", layer ) );
			layerFolder.isStatic = true;
			layerFolder.tag = Common.Tags.HOLDER;
			layerFolder.transform.parent = combinedRoot.transform;
			
			if ( layer > 0 ) {
				BuildingInfo b = layerFolder.AddComponent< BuildingInfo >();
				b.childrenLayer = layer;
			}
			
			LayerMaterialInfo info = layerMaterialInfo[ layer ];
				
			foreach ( string materialName in info.materialByName.Keys ) {
				Material material = info.materialByName[ materialName ];
				MeshFilter[] meshFilters = info.filtersPerMaterial[ materialName ].ToArray();
				
				if ( meshFilters.Length > 0 ) {
					CombineInstance[] combine = new CombineInstance[ meshFilters.Length ];
					
					for ( int i = 0; i < meshFilters.Length; i++ ) {
						MeshFilter f = meshFilters[ i ];
						
						Transform parent = f.transform.parent;
						if ( parent == null )
							parent = f.transform;
						
						combine[ i ].mesh = f.sharedMesh;
	//					Matrix4x4 myTransform = parent.worldToLocalMatrix;
	//					combine[ i ].transform = myTransform * f.transform.localToWorldMatrix;
						combine[ i ].transform = f.transform.localToWorldMatrix;
	//					f.gameObject.renderer.enabled = false; // TODO remover a mesh e/ou renderer????
					}
					
					GameObject combined = new GameObject( material == null ? "no_material" : ( material.name + "_combined" ) );
					combined.isStatic = true;
					combined.tag = Common.Tags.TERRAIN;
					combined.transform.parent = layerFolder.transform;
					
					MeshFilter filter = combined.AddComponent< MeshFilter >();
					MeshRenderer renderer = combined.AddComponent< MeshRenderer >();
					combined.AddComponent< MeshCollider >();
					
					renderer.sharedMaterial = material;
					combined.transform.localPosition = new Vector3();
					combined.transform.localScale = new Vector3( 1, 1, 1 );
					combined.transform.localEulerAngles = new Vector3();
					
					filter.sharedMesh = new Mesh();
					filter.sharedMesh.CombineMeshes( combine, true );
					filter.sharedMesh.name = "combined_mesh";
					filter.sharedMesh.Optimize();
					
					MeshRebuilder meshRebuilder = combined.AddComponent< MeshRebuilder >();
					meshRebuilder.SetMesh( filter.sharedMesh );
				}
			}
		}
	}
	
	
	internal class LayerMaterialInfo {
		
		public Dictionary< string, List< MeshFilter > > filtersPerMaterial = new Dictionary< string, List< MeshFilter > >();
		
		public Dictionary< string, Material > materialByName = new Dictionary< string, Material >();
		
	}
	
}
