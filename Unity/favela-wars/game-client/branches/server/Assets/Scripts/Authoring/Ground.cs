using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameController;
using GameCommunication;
using Utils;


[ RequireComponent( typeof( MeshFilter ) ) ]
public class Ground : StakeGenerator {
	
	public int numberOfQuadsInX = 8;
	public int numberOfQuadsInZ = 8;
	
	protected List< GroundBox > groundBoxes = new List< GroundBox >();
	
	private GameVector minPoint;
	
	public GameVector GetMissionMinPoint() {
		return minPoint;
	}
	
	
	private float GetSmallerY() {
		Object[] objs = GameObject.FindObjectsOfType( typeof( GameObject )  );
		GameObject gobj;
		float smaller = 0.0f;
		float current = 0.0f;
		
		foreach ( Object obj in objs ) {
			gobj = ( GameObject ) obj;
			current = gobj.transform.position.y - ( gobj.transform.localScale.y * 0.5f );
			
			if ( NanoMath.LessThan( current, smaller ) )
				smaller = current;
		}
		
		return smaller;
	}
	
	
	public float SetAsMultiple( float x ) {
		float toReturn = Mathf.Round( x / Common.LEVEL_SECTOR_WIDTH );
		toReturn *= Common.LEVEL_SECTOR_WIDTH;
		return toReturn;
	}
	
	
	public bool GenerateGroundBoxes() {		
		tag = Common.Tags.TERRAIN;
		SelfTagger tagger = gameObject.AddComponent< SelfTagger >();
		tagger.selfTag = tag;
		
		Bounds meshBounds = ControllerUtils.GetBox( gameObject, false );
		minPoint = ControllerUtils.CreateGameVector( meshBounds.min );
		
		float terrainMeshMarginX = percentualMargin.x * meshBounds.size.x;
		float terrainMeshMarginZ = percentualMargin.y * meshBounds.size.z;
		
		float boxWidth = ( meshBounds.size.x - 2 * terrainMeshMarginX ) / numberOfQuadsInX;
		float boxLength = ( meshBounds.size.z - 2 * terrainMeshMarginZ ) / numberOfQuadsInZ;
		
		MeshCollider meshCollider = GetComponent< MeshCollider >();
		if( meshCollider == null )
			meshCollider = gameObject.AddComponent< MeshCollider >();
		
		Vector3[,] vertices = new Vector3[ numberOfQuadsInX + 1, numberOfQuadsInZ + 1 ];
		for (int i = 0; i < numberOfQuadsInX + 1; i++) {
			for (int k = 0; k < numberOfQuadsInZ + 1; k++) {
				
				Vector3 origin = new Vector3( minPoint.x + i * boxWidth, meshBounds.max.y + 1.0f, minPoint.z + k * boxLength );
				
				origin.x += i < numberOfQuadsInX? terrainMeshMarginX : -terrainMeshMarginX;
				origin.z += k < numberOfQuadsInZ? terrainMeshMarginZ : -terrainMeshMarginZ;
				
				RaycastHit hit;
				Ray ray = new Ray( origin, Vector3.down );
				
				if( meshCollider.Raycast( ray, out hit, 1000.0f ) ) {
					vertices[ i, k ] = hit.point;
					
					//GameObject hitSphere = GameObject.CreatePrimitive( PrimitiveType.Sphere );
					//hitSphere.transform.position = hit.point;
					//hitSphere.name = "ground check";
					//hitSphere.transform.localScale = new Vector3( 1.0f, 1.0f, 1.0f );
				} else {
					// Temos um erro!
					//GameObject errorSphere = GameObject.CreatePrimitive( PrimitiveType.Sphere );
					//errorSphere.transform.position = origin;
					//errorSphere.transform.localScale = new Vector3( 1.0f, 1.0f, 1.0f );
					//errorSphere.name = "error!";
					Debug.LogError( "Não foi possível gerar a caixa do terreno (" + i + ", " + k + "). Verifique a posicao da malha e garanta que ela não possui rotacao." );
					return false;
				}
			}
		}
			
		for (int c = 1; c < numberOfQuadsInX + 1; c++) {
			for (int d = 1; d < numberOfQuadsInZ + 1; d++) {
				
				Vector3 mid = new Vector3();
				Vector3 scale = new Vector3();
				float[] heights = new float[ 4 ];
				heights[ 0 ] = vertices[ c - 1, d - 1 ].y;
				heights[ 1 ] = vertices[ c - 1, d ].y;
				heights[ 2 ] = vertices[ c, d - 1 ].y;
				heights[ 3 ] = vertices[ c, d ].y;
				
				float max = float.MinValue;
				float min = float.MaxValue;
				
				for ( int e = 0; e < heights.Length; ++e ) {

					if ( max < heights[ e ] )
						max = heights[ e ];
					
					if ( min > heights[ e ] )
						min = heights[ e ];
				}
				
				min += Common.LEVEL_SAFE_MARGIN_Y;

				
				scale.x = vertices[ c, d ].x - vertices[ c - 1, d - 1 ].x;
				scale.y = max - min;
				scale.z = vertices[ c, d ].z - vertices[ c - 1, d - 1 ].z;
				
				mid.x = vertices[ c, d ].x - scale.x * 0.5f;
				mid.y = ( scale.y * 0.5f ) + min;
				mid.z = vertices[ c, d ].z - scale.z * 0.5f;
				
				GroundBox gbox = new GroundBox();
				GameTransform gt = new GameTransform();
				gt.position = GameController.ControllerUtils.CreateGameVector( mid );
				gt.scale = GameController.ControllerUtils.CreateGameVector( scale );
				gbox.SetGameTransform( gt );
					
				gbox.SetHeights( vertices[ c - 1, d - 1 ].y, vertices[ c - 1, d ].y, vertices[ c, d ].y, vertices[ c, d - 1 ].y );
				gbox.SetMiddleIndices( 1, 2 );
				groundBoxes.Add( gbox );
				
//				// DEBUG
//				GameObject createdBox = GameObject.CreatePrimitive( PrimitiveType.Cube );
//				createdBox.transform.position = mid;
//				createdBox.transform.localScale = scale;
//				createdBox.name = "Ground Box ( " + c + ", " + d + ")";
			}
		}
		
		return true;
	}
	
	
	public IEnumerable<GroundBox> GetGroundBoxes() {
		foreach( GroundBox gbox in groundBoxes )
			yield return gbox;
	}
	
}
