using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameController;

using Object = UnityEngine.Object;

#if UNITY_EDITOR
[ ExecuteInEditMode ]
public class Tiler : MonoBehaviour {
	
	public int rows = 1;
	
	public int columns = 1;
	
	protected int savedRows = 1;
	
	protected int savedColumns = 1;
	
	public float offsetInRow;
	
	public float offsetInColumn;
	
	protected float savedOffsetInRow;
	
	protected float savedOffsetInColumn;
	
	protected GameObject original;
	
	/// <summary>
	/// Lista auxiliar de filhos gerados dinamicamente. Esta lista � importante para evitar mistura dos filhos gerados
	/// dinamicamente com os filhos originais do GameObject.
	/// </summary>
	protected List< GameObject > children = new List< GameObject >();
	
	
	/// <summary>
	/// M�todo chamado quando o script � removido de um GameObject.
	/// </summary>
	public void OnDestroy() {
		Object.DestroyImmediate( original );
		original = null;
	}
	
	
	private void RemoveAllChildren() {
		foreach ( GameObject o in children ) {
			Object.DestroyImmediate( o );
		}
		children.Clear();
	}
	
	
	// Update is called once per frame
	public void Update() {
		if ( Application.isEditor && !Application.isPlaying && HasChanged() ) {
			if ( original == null ) {
				original = ( GameObject ) GameObject.Instantiate( gameObject );
				original.name = gameObject.name;
				if ( original.renderer != null )
					original.renderer.enabled = false;
				
				original.transform.parent = transform;
				
				foreach ( Component tiler in original.GetComponents< Tiler >() ) {
					Object.DestroyImmediate( tiler );
				}
				
				// misturar Tiler e ObjectSnapper n�o funciona bem pois ambos interferem na posi��o dos objetos
				foreach ( Component snapper in original.GetComponents< ObjectSnapper >() ) {
					Object.DestroyImmediate( snapper );
				}
			}
			
			// se n�o houver altera��es no n�mero de elementos, n�o � necess�rio recri�-los
			bool create = rows != savedRows || columns != savedColumns;
			if ( create ) {
				RemoveAllChildren();
				
				savedRows = rows;
				savedColumns = columns;
			}
			
			savedOffsetInColumn = offsetInColumn;
			savedOffsetInRow = offsetInRow;
			
			float dx;
			float dz;
			
			original.transform.eulerAngles = new Vector3();
			
			Bounds b = ControllerUtils.GetBox( original, false );
			dx = b.size.x;
			dz = b.size.z;
			
			dx += offsetInRow;
			dz += offsetInColumn;
			
			Vector3 scaleInverse = new Vector3( 1.0f / transform.localScale.x, 1.0f / transform.localScale.y, 1.0f / transform.localScale.z );
			
			for ( int r = 0; r < rows; ++r ) {
				for ( int c = 0; c < columns; ++c ) {
					GameObject copy;
					// o primeiro objeto � o pr�prio gameObject deste script
					if ( r == 0 && c == 0 )
						continue;
					
					if ( create ) {
						copy = ( GameObject ) GameObject.Instantiate( original );
						copy.name = gameObject.name + '_' + r + '_' + c;
						copy.transform.parent = transform;
						children.Add( copy );
						
						if ( copy.renderer != null )
							copy.renderer.enabled = true;
					} else {
						// o �ndice tem 1 a menos porque o 1� � o pr�prio
						copy = children[ r * columns + c - 1 ];
					}
					
					copy.transform.localEulerAngles = new Vector3();
					copy.transform.localPosition = new Vector3( dx * r * scaleInverse.x, 0, dz * c * scaleInverse.z );
					copy.transform.localScale = new Vector3( 1, 1, 1 );
				}
			}
		}
	}
	
	
	private bool HasChanged() {
		return rows != savedRows || columns != savedColumns || savedOffsetInRow != offsetInRow || savedOffsetInColumn != offsetInColumn;
	}
}
#endif