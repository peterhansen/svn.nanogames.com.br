#if UNITY_EDITOR

using System;

using UnityEngine;

using Utils;


[ ExecuteInEditMode ]
public abstract class GenericEditor : MonoBehaviour {
	
	protected object data;
	
	
	public static string DEFAULT_PATH {
		get { return Application.dataPath + "/Options/"; }
	}
	
	
	public object GetData() {
		return data;
	}
	
	
	public void SetData( Editable data ) {
		this.data = data;
	}
	
	
	public virtual void Awake() {
	}
	
	
	[ ToolTip( "Caminho do arquivo de opções default deste inspetor." ) ]
	public virtual string DefaultPath {
		get { return DEFAULT_PATH + Util.GetCleanFilename( GetTitle() ) + ".options"; }
	}
	
	
	public abstract string GetTitle();
	
}

#endif