using UnityEngine;
using System.Collections;

using GameController;

[ ExecuteInEditMode ]
public class SelfDestructBehaviour : MonoBehaviour, TimerScriptListener {
	
	// TODO valor não é usado - permitir que seja editável e que inicie automaticamente com start e/ou awake
	public int timeInSeconds = 5;
	
	
	public void Awake() {
		TimerScript.AddTimer( timeInSeconds * 1000, this );
	}

	
	public void StartTimer( int iteration ) {
		TimerScript.AddTimer( iteration * 1000, this );
	}
	
	
	public void OnTimerEnded( int timerID ) {
		Destroy( this.gameObject );
	}
}
