#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using UnityEditor;
using UnityEngine;

using Attribute = GameCommunication.Attribute;
using GameCommunication;
using GameController;
    
using Utils;

    
public enum ExposedType {
	Generic,
	Integer,
	Float,
	Boolean,
	String,
	Vector2,
	Vector3,
	GameVector,
	Enum,
	Array,
}



public static class ExposeProperties {
	
	
    public static bool Expose( IEnumerable< PropertyField > properties, bool firstRun ) {
		
        GUILayoutOption[] options = new GUILayoutOption[0];
        
        EditorGUILayout.BeginVertical( options );
		
		bool dirty = false;
        
        foreach ( PropertyField field in properties ) {
			EditorGUILayout.BeginHorizontal( options );
			
			object previousValue = field.GetValue();
			GUIContent content = new GUIContent( field.Name, field.ToolTip );
	        
	        switch ( field.Type ) {
				case ExposedType.Integer:
	                field.SetValue( EditorGUILayout.IntField( content, (int)field.GetValue(), options ) ); 
	            break;
	            
	        	case ExposedType.Float:
	                field.SetValue( EditorGUILayout.FloatField( content, (float)field.GetValue(), options ) );
	            break;
	            
	        	case ExposedType.Boolean:
	                field.SetValue( EditorGUILayout.Toggle( content, (bool)field.GetValue(), options ) );
	            break;
	        
	        	case ExposedType.String:
	                field.SetValue( EditorGUILayout.TextField( content, (String)field.GetValue(), options ) );
	            break;
	            
	        	case ExposedType.Vector2:
	                field.SetValue( EditorGUILayout.Vector2Field( content.text, (Vector2)field.GetValue(), options ) );
	            break;
	            
	        	case ExposedType.Vector3:
	                field.SetValue( EditorGUILayout.Vector3Field( content.text, (Vector3)field.GetValue(), options ) );
	            break;
	
	        	case ExposedType.GameVector:
	                field.SetValue( ControllerUtils.CreateGameVector( EditorGUILayout.Vector3Field( content.text, (Vector3) ( ControllerUtils.CreateVector3( ( GameVector ) field.GetValue() ) ), options ) ) );
	            break;
	
	        	case ExposedType.Enum:
	           		field.SetValue(EditorGUILayout.EnumPopup( content, (Enum)field.GetValue(), options));
	            break;
				
				case ExposedType.Array:
					// TODO expor corretamente um array
//					Array a = ( Array ) field.GetValue();
//					EditorGUILayout.EndHorizontal();
//					Type t = a.GetType().GetElementType();
//					foreach ( object o in ( Array ) field.GetValue() ) {
//						EditorGUILayout.BeginHorizontal( emptyOptions );
//						EditorGUILayout.IntField( "Morra " + o, (int) o, emptyOptions );
//						EditorGUILayout.EndHorizontal();
//					}
				break;
	            
	        	default:
	            break;
	        }
	        
	        EditorGUILayout.EndHorizontal();
	        
			if ( !firstRun ) {
				try {
					if ( !dirty && !field.GetValue().Equals( previousValue ) )
						dirty = true;
				} catch ( Exception ) {
				}
			}		
        }
        
        EditorGUILayout.EndVertical();
		
		return dirty;
    }
	
	
	private static void ExposeField( PropertyField field ) {
		
	}
	
	
	public static PropertyField[] GetProperties( System.Object obj ) {
		if ( obj == null )
			return new PropertyField[ 0 ];
		
		return GetProperties( obj, obj.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance ) );
	}
	
    
    public static PropertyField[] GetProperties( System.Object obj, PropertyInfo[] infos ) {
		List< PropertyField > fields = new List<PropertyField>();
        
        foreach ( PropertyInfo info in infos ) {
            if ( ! (info.CanRead && info.CanWrite) )
                continue;
            
            object[] attributes = info.GetCustomAttributes( true );
            
            bool isExposed = true;
            
            foreach( object o in attributes ) {
                if ( o.GetType() == typeof( HideInInspector ) ) {
                    isExposed = false;
                    break;
                }
            }
            
            if ( !isExposed )
                continue;
            
            ExposedType type = ExposedType.Integer;
            
            if( PropertyField.GetPropertyType( info, out type ) ) {
                PropertyField field = new PropertyField( obj, info, type );
                fields.Add( field );
            }
        }
        return fields.ToArray();
    }
}


public class PropertyField {
	
    public System.Object m_Instance;
    
	PropertyInfo m_Info;
    
	ExposedType m_Type;
	
    MethodInfo m_Getter;
    
	MethodInfo m_Setter;
	
	private string toolTip = "";
    
	
    public ExposedType Type {
        get { return m_Type; }
    }
	
                
    public String Name {   
        get { return ObjectNames.NicifyVariableName( m_Info.Name ); }
    }
	
	
	public string ToolTip {
		get { return toolTip; }
		set { toolTip = value; }
	}
	
	
    public PropertyField( System.Object instance, PropertyInfo info, ExposedType type ) {   
        m_Instance = instance;
        m_Info = info;
        m_Type = type;
        
		object[] toolTipAttributes = m_Info.GetCustomAttributes( typeof( ToolTipAttribute ), false );
		if ( toolTipAttributes.Length > 0 ) {
			ToolTipAttribute a = toolTipAttributes[ 0 ] as ToolTipAttribute;
			if ( a != null )
				ToolTip = a.ToolTipText;
		}
        m_Getter = m_Info.GetGetMethod();
        m_Setter = m_Info.GetSetMethod();
    }
	
    
    public System.Object GetValue() {
        return m_Getter.Invoke( m_Instance, null );
    }
	
    
    public void SetValue( System.Object value ) {
		if ( m_Setter != null )
        	m_Setter.Invoke( m_Instance, new System.Object[] { value } );
    }
	
	
	public override bool Equals (object obj) {
		return Equals( obj as PropertyField );
	}
	
	
	public bool Equals( PropertyField other ) {
		return other != null && m_Instance == other.m_Instance && m_Type == other.m_Type && m_Getter == other.m_Getter && m_Setter == other.m_Setter;
	}
	
	
	public override int GetHashCode() {
		return base.GetHashCode();
	}
	
    
    public static bool GetPropertyType( PropertyInfo info, out ExposedType propertyType ) {
        propertyType = ExposedType.Generic;
        
        Type type = info.PropertyType;
        
        if ( type == typeof( int ) ) {
            propertyType = ExposedType.Integer;
            return true;
        }
        
        if ( type == typeof( float ) ) {
            propertyType = ExposedType.Float;
            return true;
        }
        
        if ( type == typeof( bool ) ) {
            propertyType = ExposedType.Boolean;
            return true;
        }
        
        if ( type == typeof( string ) ) {
            propertyType = ExposedType.String;
            return true;
        }   
        
        if ( type == typeof( Vector2 ) ) {
            propertyType = ExposedType.Vector2;
            return true;
        }
        
        if ( type == typeof( Vector3 ) ) {
            propertyType = ExposedType.Vector3;
            return true;
        }
		
        if ( type == typeof( GameVector ) ) {
            propertyType = ExposedType.GameVector;
            return true;
        }
		
        if ( type.IsEnum ) {
            propertyType = ExposedType.Enum;
            return true;
        }
		
		if ( type.IsArray ) {
			propertyType = ExposedType.Array;
			return true;
		}
        
        return false;
        
    }
    
}

#endif