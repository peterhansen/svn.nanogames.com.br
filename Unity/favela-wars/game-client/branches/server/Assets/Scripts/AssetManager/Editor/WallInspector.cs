using System;

using UnityEditor;
using UnityEngine;

using GameCommunication;


[ CustomEditor( typeof( ObjectSnapper ) ) ]
public class WallInspector : Editor {
	
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		
		ObjectSnapper script = target as ObjectSnapper;
	
		if ( script.snapMode == ObjectSnapper.SnapMode.Wall )
			script.WallDrawMode = ( ObjectSnapper.WallMode ) EditorGUILayout.EnumPopup( "Wall Mode", script.WallDrawMode );
	}
	
}