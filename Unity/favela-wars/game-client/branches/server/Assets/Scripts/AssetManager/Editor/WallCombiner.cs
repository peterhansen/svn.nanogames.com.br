using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Utils;

using Object = UnityEngine.Object;


public class WallCombiner  {
	
	private const string TAG_UV_MAP = "vt";
	
	private const string TAG_OBJECT_NAME = "# object ";
	
	private const double MARGIN = 0;//0.002;
	
	private const double WALL_PERCENT = ( 480.0 / 512.0 ) - MARGIN;
	
	private const double OTHER_PERCENT = 1.0 - WALL_PERCENT;
	
	private static readonly Regex validExtensions = new Regex( @"(\A.*\.(png|jpg|jpeg)+\z)" );
	
	private const float LOAD_TEXTURES_PERCENT = 0.3f;
	
	private const float SAVE_TEXTURES_PERCENT = 1.0f - LOAD_TEXTURES_PERCENT;
	
	private const string TITLE = AssetGenerator.FIXES_TITLE;
	
	
	private enum ReadMode {
		NONE,
		READ_WALL_UV,
		READ_OTHER_UV,
	}
	
	
	[ MenuItem( TITLE + "/Adjust walls UV" ) ]
	public static void CombineWalls() {
		string path = EditorUtility.OpenFolderPanel( "Load OBJ wall files from directory", Application.dataPath, "" );
		if ( path == null || path.Length == 0 )
			return;
		
		string[] filenames = Directory.GetFiles( path );
		foreach ( string filename in filenames ) {
			if ( !filename.ToLower().EndsWith( ".obj" ) || filename.Contains( "_MOD" ) )
				continue;
			
			ReadMode readMode = ReadMode.NONE;
			StringBuilder sb = new StringBuilder();
			
			try {
				string[] lines = File.ReadAllLines( filename );
				foreach ( string line in lines ) {
					string lineToAdd = line;
					
					if ( line.StartsWith( TAG_UV_MAP ) ) {
						if ( readMode == ReadMode.NONE )
							continue;
						
						string[] parts = line.Split( ' ' );
						double u = double.Parse( parts[ 1 ] );
						switch ( readMode ) {
							case ReadMode.READ_WALL_UV:
								u = MARGIN + u * WALL_PERCENT;
							break;
							
							case ReadMode.READ_OTHER_UV:
								u = WALL_PERCENT + ( u * OTHER_PERCENT );
							break;
						}
						parts[ 1 ] = u.ToString();
						lineToAdd = string.Join( " ", parts );
					} else if ( line.StartsWith( TAG_OBJECT_NAME ) ) {
						string objectName = line.Substring( TAG_OBJECT_NAME.Length );
						readMode = objectName.ToLower().Contains( "parede" ) ? ReadMode.READ_WALL_UV : ReadMode.READ_OTHER_UV;
					}
					
					sb.Append( lineToAdd );
					sb.Append( '\n' );
				}
				StreamWriter writer = new StreamWriter( filename.Replace( ".obj", "_MOD.obj" ) );
				writer.Write( sb.ToString() );
				writer.Flush();
				writer.Close();
			} catch ( Exception e ) {
				Debugger.LogError( e );
			}
		}
	}
	
	
	[ MenuItem( TITLE + "/Combine Textures" ) ]
	public static void CombineTextures() {
		List< Texture2D > columnsTextures = null;
		Dictionary< string, CompositeTexture > walls = null;
		Dictionary< string, CompositeTexture > columns = null;
		
		try {
			string wallsRootPath = EditorUtility.OpenFolderPanel( "Choose WALLS textures root path", "C:/Users/peter/Desktop/zzzzzzzzzzzz/paredes", "" );
			//			string wallsRootPath = EditorUtility.OpenFolderPanel( "Choose WALLS textures root path", Application.dataPath, "" );
			if ( wallsRootPath == null || wallsRootPath.Length == 0 )
				return;
			
			string columnsRootPath = EditorUtility.OpenFolderPanel( "Choose COLUMNS textures root path", "C:/Users/peter/Desktop/zzzzzzzzzzzz/colunas", "" );
			//			string columnsRootPath = EditorUtility.OpenFolderPanel( "Choose COLUMNS textures root path", Application.dataPath, "" );
			if ( columnsRootPath == null || columnsRootPath.Length == 0 )
				return;
			
			string outPath = wallsRootPath + "_COMBINED";
			Directory.CreateDirectory( outPath );
			
			columnsTextures = LoadTextures( columnsRootPath );
			
			walls = ComposeTextures( LoadTextures( wallsRootPath ) );
			
			float wallPercent = SAVE_TEXTURES_PERCENT / walls.Keys.Count;
			int wallCount = 0;
			foreach ( KeyValuePair< string, CompositeTexture > wallInfo in walls ) {
				++wallCount;
				
				CompositeTexture wall = wallInfo.Value;
				
				if ( wall.MainTexture == null )
					continue;
				
				float wallProgress = LOAD_TEXTURES_PERCENT + wallCount * wallPercent;
					
				columns = ComposeTextures( columnsTextures );
				float columnPercent = wallPercent / columns.Keys.Count;
				int columnCount = 0;
				
				string title = "Combining " + wall.name + " maps";
				
				foreach ( KeyValuePair< string, CompositeTexture > columnInfo in columns ) {
					++columnCount;
					
					CompositeTexture column = columnInfo.Value;
					
					float progress = wallProgress + columnCount * columnPercent;
					
					if ( column.MainTexture == null )
						continue;
					
					CombineAndSaveTexture( wall.MainTexture, wall.name, column.MainTexture, column.name, outPath, AssetGenerator.MAP_SUFFIX_DIFFUSE_ALT );
					EditorUtility.DisplayProgressBar( title, "Diffuse map", progress );
					
					CombineAndSaveTexture( wall.NormalMap, wall.name, column.NormalMap, column.name, outPath, AssetGenerator.MAP_SUFFIX_NORMAL );
					EditorUtility.DisplayProgressBar( title, "Normal map", progress );
					
					CombineAndSaveTexture( wall.LightMap, wall.name, column.LightMap, column.name, outPath, AssetGenerator.MAP_SUFFIX_SPECULAR );
					EditorUtility.DisplayProgressBar( title, "Specular map", progress );
				}
				
				wall.Free();
				Resources.UnloadUnusedAssets();
				EditorUtility.UnloadUnusedAssets();
				System.GC.Collect();
			}
		} catch ( Exception e ) {
			Debugger.LogError( e );
		} finally {
			EditorUtility.ClearProgressBar();
			
			if ( columnsTextures != null ) {
				foreach ( Texture2D t in columnsTextures )
					Object.DestroyImmediate( t );
				
				columnsTextures = null;
			}
			
			if ( walls != null ) {
				foreach ( CompositeTexture t in walls.Values )
					t.Free();
				
				walls = null;
			}
			
			if ( columns != null ) {
				foreach ( CompositeTexture t in columns.Values )
					t.Free();
				
				columns = null;
			}
			
			Resources.UnloadUnusedAssets();
			EditorUtility.UnloadUnusedAssets();
			System.GC.Collect();
		}		
	}
	
	
	private static void CombineAndSaveTexture( Texture2D wall, string wallName, Texture2D column, string columnName, string path, string map ) {
		if ( wall == null || column == null )
			return;
		
		Texture2D main = Combine( wall, column );
		Util.SaveToFile( main.EncodeToPNG(), string.Format( "{0}/{1}_{2}_{3}.png", path, wallName, columnName, map ) );
		
		Object.DestroyImmediate( main );
	}
	
	
	/// <summary>
	/// Combina uma textura de parede com uma coluna. A textura retornada tem as mesmas dimensões da textura de parede.
	/// </summary>
	private static Texture2D Combine( Texture2D wall, Texture2D column ) {
		Texture2D newWall = new Texture2D( wall.width, wall.height );
		
		Color[] wallColors = wall.GetPixels( 0, 0, wall.width - column.width, wall.height );
		Color[] columnColors = column.GetPixels();
		newWall.SetPixels( 0, 0, wall.width - column.width, wall.height, wallColors );
		newWall.SetPixels( wall.width - column.width, 0, column.width, wall.height, columnColors );
		
		return newWall;
	}
	
	
	/// <summary>
	/// Carrega todas as texturas de um determinado diretório e de seus subdiretórios.
	/// </summary>
	private static List< Texture2D > LoadTextures( string rootPath ) {
		List< Texture2D > textures = new List< Texture2D >();
		
		if ( Directory.Exists( rootPath ) ) {
			List< string > folders = new List<string>();
			folders.Add( rootPath );
			folders.AddRange( Directory.GetDirectories( rootPath ) );

			float folderPercent = LOAD_TEXTURES_PERCENT / folders.Count;
			int count = 0;
			foreach ( string path in folders ) {
				++count;
				
				string[] filenames = Directory.GetFiles( path );
				float filePercent = folderPercent / filenames.Length;
				int fileCount = 0;
				foreach( string filename in filenames ) {
					++fileCount;
					float progress = count * folderPercent + fileCount * filePercent;
					EditorUtility.DisplayProgressBar( "Loading textures", "Loading textures from " + path + "...", progress );
					
					if ( validExtensions.IsMatch( filename ) ) {
						FileStream file = null;
						try {
							file = new FileStream( filename, FileMode.Open );
							byte[] imageData = new byte[ file.Length ];
							int total = 0;
							while ( total < file.Length ) {
								total += file.Read( imageData, 0, ( int ) file.Length - total );
							}
							Texture2D texture = new Texture2D( 1, 1 );
							if ( texture.LoadImage( imageData ) ) {
								texture.name = Util.GetFilenameFromPath( filename, false );
								textures.Add( texture );
							} else {
								Debugger.LogError( "Couldn't load texture from file " + filename );
							}
						} catch ( Exception e ) {
							Debugger.LogError( e );
						} finally {
							if ( file != null )
								file.Close();
						}
					}
				}
			}
		}
		
		return textures;
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	private static Dictionary< string, CompositeTexture > ComposeTextures( List< Texture2D > textures ) {
		Dictionary< string, CompositeTexture > compositeTextures = new Dictionary< string, CompositeTexture >();
		
		// compõe todas as texturas (difusa + normal + altura + ...)
		foreach ( Texture2D t in textures ) {
			string cleanName;
			int index = t.name.LastIndexOf( '_' );
			
			if ( index < 0 )
				cleanName = t.name;
			else
				cleanName = t.name.Substring( 0, index );
			
			if ( !compositeTextures.ContainsKey( cleanName ) )
				compositeTextures.Add( cleanName, new CompositeTexture( cleanName ) );
			
			compositeTextures[ cleanName ].Add( t );
		}
		
		return compositeTextures;
	}	

	
}
