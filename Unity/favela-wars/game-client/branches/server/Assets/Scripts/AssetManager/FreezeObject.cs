using System;
using UnityEngine;
using System.Collections;


[ ExecuteInEditMode ]
public class FreezeObject : MonoBehaviour {
  
	public Space space = Space.World;
    public bool FreezePosition = false;
    public bool FreezeRotation = false;
	public bool FreezeScaleY = false;

    private Space m_OldSpace = Space.World;
    private bool m_OldFreezePosition = false;
    private bool m_OldFreezeRotation = false;
	private bool m_OldFreezeScaleY = false;

    private Vector3 m_Position = Vector3.zero;
    private Quaternion m_Rotation = Quaternion.identity;
	private float m_ScaleY;

	
    void Awake() {
        if ( Application.isPlaying )
            Destroy( this );
    }
	
	
    void Update() {
        if ( !Application.isEditor ) {
            Destroy( this );
            return;
        }
		
		// muitos dos testes abaixo não são estritamente necessários, mas evitam que a Unity considere que a cena
		// foi alterada em função das atribuições

        if ( FreezePosition ) {
            // Save current position if enabled
            if ( ( FreezePosition != m_OldFreezePosition ) || ( space != m_OldSpace ) ) {
                m_Position = (space == Space.World) ? transform.position : transform.localPosition;
	            
				// Freeze the position
	            if ( space == Space.World ) {
					if ( transform.localRotation != m_Rotation )
	                	transform.position = m_Position;
				} else {
					if ( transform.localPosition != m_Position )
	                	transform.localPosition = m_Position;
				}
			}
        }
		
        if ( FreezeRotation ) {
            // Save current rotation if enabled
            if ( ( FreezeRotation != m_OldFreezeRotation ) || ( space != m_OldSpace ) ) 
                m_Rotation = ( space == Space.World ) ? transform.rotation : transform.localRotation;
			
			// Freeze the rotation
			if ( space == Space.World ) {
				if ( transform.rotation != m_Rotation )
					transform.rotation = m_Rotation;
			} else {
				if ( transform.localRotation != m_Rotation )
					transform.localRotation = m_Rotation;
			}
        }
		
		if ( FreezeScaleY ) {
			if ( ( FreezeScaleY != m_OldFreezeScaleY ) || ( space != m_OldSpace ) )
				m_ScaleY = transform.localScale.y;
			
			if ( transform.localScale.y != m_ScaleY )
			    transform.localScale = new Vector3( transform.localScale.x, m_ScaleY, transform.localScale.z );
		}
		
		if ( m_OldSpace != space )
        	m_OldSpace = space;
		if ( m_OldFreezePosition != FreezePosition )
        	m_OldFreezePosition = FreezePosition;
        if ( m_OldFreezeRotation != FreezeRotation )
			m_OldFreezeRotation = FreezeRotation;
		if ( m_OldFreezeScaleY != FreezeScaleY )
			m_OldFreezeScaleY = FreezeScaleY;
    }
}

