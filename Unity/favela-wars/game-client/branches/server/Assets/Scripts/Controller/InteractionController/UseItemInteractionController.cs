using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;


namespace GameController {
	
	public class UseItemInteractionController : ActionInteractionController {
		
		private Dictionary< GameObject, GameObject > targets = new Dictionary< GameObject, GameObject >();
		
		private GameVector target;
		
		private GameObject currentTargetCharacter;
		
		private UseItemGameAction useItemAction;
		
		private ConfirmationMenu confirmationMenu;
		
		public UseItemInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			if ( !( action is UseItemGameAction ) )
				throw new Exception( "Action is not UseItemGameAction - " + action );
			
			useItemAction = action as UseItemGameAction;
			
			GameVector origin = ControllerUtils.CreateGameVector( selectedCharacter.transform.position );
			
			switch ( useItemAction.itemTargetMode ) {
				case ItemTargetMode.Any:
				case ItemTargetMode.Position:
				break;
					
				case ItemTargetMode.Character:
					AddTarget( selectedCharacter );
				
					// TODO fazer raycasts para garantir que não há obstáculos entre eles
					foreach ( GameObject o in WorldManager.GetInstance().EnumerateCharacters() )  {
						if ( CheckFreeArea( o ) ) {
							if ( useItemAction.range < 0 ) {
								AddTarget( o );
							} else {
								GameVector pos = ControllerUtils.CreateGameVector( o.transform.position );
								if ( ( pos - origin ).Norm() <= useItemAction.range ) {
									AddTarget( o );
								}
							}
						}
					}
				break;			
					
				case ItemTargetMode.Self:
					AddTarget( selectedCharacter );
					currentTargetCharacter = selectedCharacter;
				break;
			}
			
			confirmationMenu = new ConfirmationMenu();
			controls.Add( confirmationMenu );
		}
		
		
		private bool CheckFreeArea( GameObject o ) {
			Bounds b1 = ControllerUtils.GetBox( selectedCharacter, false );
			Bounds b2 = ControllerUtils.GetBox( o, false );
			
			Ray ray = new Ray( b1.center, b2.center - b1.center );
			RaycastHit hitInfo;
				
			if ( Physics.Raycast( ray, out hitInfo ) ) {
				if ( hitInfo.collider != null ) {
					GameObject hitObject = hitInfo.collider.gameObject;
					Transform t = hitObject.transform;
					while ( t != null ) {
						if ( t.gameObject == o )
							return true;
						
						t = t.parent;
					}
				}
			}
			
			return false;
		}
		
		
		private void AddTarget( GameObject target ) {
			GameObject clone = ( GameObject ) GameObject.Instantiate( target );
			foreach ( Renderer r in clone.GetComponentsInChildren< Renderer >() ) {
				Material m = r.material;
				if ( m != null ) {
					m.shader = Shader.Find( "Transparent/Diffuse" );
				}
			}
			SetCharacterChosen( clone, false );
				
			targets[ clone ] = target;
			target.active = false;
		}
		
		
		public override void Update() {
			GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
			Desktop desktop = guiManager.Desktop;
			
			if ( desktop.HotControl == desktop && Input.GetMouseButtonDown( Common.MOUSE_BUTTON_LEFT ) ) {
				Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
				RaycastHit hitInfo;
				
				if ( Physics.Raycast( ray, out hitInfo ) ) {
					GameVector destination;
					
					if ( useItemAction.range > 0 ) {
						GameVector origin = ControllerUtils.CreateGameVector( selectedCharacter.transform.position );
						destination = ControllerUtils.CreateGameVector( hitInfo.collider.gameObject.transform.position );
						
						float distance = ( destination - origin ).Norm();
						
						if ( distance > useItemAction.range ) {
							Debugger.LogWarning( "Uso fora do alcance do item!!!" );
							SetCurrentCharacter( null );
							return;
						}
					} else {
						destination = ControllerUtils.CreateGameVector( hitInfo.point );
					}
				
					target = destination;
					
					GameObject chosenTarget = hitInfo.collider.gameObject;
					//Debugger.Log( "OBJECT ESCOLHIDO: " + chosenTarget.name );
					
					SetCurrentCharacter( chosenTarget );
					
					switch ( useItemAction.itemTargetMode ) {
						case ItemTargetMode.Any:
						case ItemTargetMode.Position:
							ShowRadialMenu();
							break;
							
						case ItemTargetMode.Character:
							if( currentTargetCharacter != null )
								ShowRadialMenu();
							break;
					}
				}
			}
		}
		
		
		protected void ShowRadialMenu() {
			confirmationMenu.AddButton( new MenuOption( IconType.OK, UseItem ) );
			confirmationMenu.AddButton( new MenuOption( IconType.CANCEL, Cancel ) );
			confirmationMenu.Show( ControllerUtils.GetScreenCenter() );
			IsoCameraScript.GetInstance().MoveTo( ControllerUtils.CreateVector3( target ) );
			IsoCameraScript.GetInstance().userInputIsBlocked = true;
		}
		
		
		public void UseItem( Control sender ) {
			int playerID = GameManager.GetInstance().GetLocalPlayer();
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			int targetCharacterID = currentTargetCharacter == null ? -1 : WorldManager.GetInstance().GetWorldID( targets[ currentTargetCharacter ] );
			
			GameManager.GetInstance().SendRequest( new UseItemRequest( playerID, characterID, useItemAction.itemID, target, targetCharacterID ) );
			OnDone();
		}
		
		
		protected void Cancel( Control sender ) {
			ResetTargets();
			GUIManager.SetInteractionController( parent );
		}
		
		
		public override void OnDone() {
			ResetTargets();
			base.OnDone();
			GUIManager.Reset();
		}
		
		
		
		public override void OnRemoved() {
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
		}
		
	
		private void SetCurrentCharacter( GameObject character ) {
			if ( currentTargetCharacter != null )
				SetCharacterChosen( currentTargetCharacter, false );
			
			currentTargetCharacter = null;
		
			if ( character != null ) {
				foreach ( GameObject t in targets.Keys ) {
					if ( t == character ) {
						Debugger.Log( "OK!!!!" );
						currentTargetCharacter = character;
						SetCharacterChosen( currentTargetCharacter, true );
						break;
					}
				}
			}
		}
		
		
		private void SetCharacterChosen( GameObject character, bool active ) {
			foreach ( Renderer r in character.GetComponentsInChildren< Renderer >() ) {
				Material m = r.material;
				if ( m != null ) {
					m.color = active ? Color.red : Color.green;
				}
			}	
		}
		
		
		private void ResetTargets() {
			foreach ( GameObject target in targets.Keys ) {
				GameObject original = targets[ target ];
				original.active = true;
				
				GameObject.Destroy( target );
			}
			targets.Clear();
		}
		
		
	}
}

