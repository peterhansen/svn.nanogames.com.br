using System;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class MoveInteractionController : ActionInteractionController {
		
		#region Character Data
		protected int characterID;
		
		protected Seeker characterSeeker;

		protected float nearDistance;
		
		protected float farDistance;
		#endregion
		
		#region Movement Data
		protected VisibilityMap visibilityMap;
		
		protected List< Vector3 > points;
		
		protected CoverData coverData;
		
		protected bool pathIsDecided = false;
		


		protected MovementTrail trail;
		
		protected PathfinderListener pathListener;
		#endregion
		
		#region Interface Elements
		protected ConfirmationMenu confirmationMenu;
		#endregion
		
	
		public MoveInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			// TODO: singleton?
			visibilityMap = VisibilityMap.GetInstance();
			
			characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			characterSeeker = selectedCharacter.GetComponent< Seeker >();
			if( characterSeeker == null ) {
				Debugger.Log( "Personagem sem um Seeker associado. Esse personagem não poderá se mover na cena." );
				OnDone();
			}
			
			MoveGameAction moveGameAction = action as MoveGameAction;
			if( moveGameAction == null ) {
				Debugger.Log( "Game Action recebida não corresponde ao tipo do Interaction Controller." );
				OnDone();
			}
			nearDistance = moveGameAction.nearDistance;
			farDistance = moveGameAction.farDistance;
			
			confirmationMenu = new ConfirmationMenu();
			controls.Add( confirmationMenu );
						
		}
		
		public override void Update() {
			InteractionUpdate();
		}
		
		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			if( pathIsDecided || inputEvent.gameObject == null || inputEvent.gameObject.CompareTag( Common.Tags.CHARACTER ) )
				return;
			
			Vector3 destination = new Vector3( inputEvent.selectionPosition.x, inputEvent.selectionPosition.y, inputEvent.selectionPosition.z );
			
			MoveGameAction moveAction = action as MoveGameAction;
			if( !inputEvent.singleClick )
				pathListener = new PathfinderListener( AskMoveConfirmation );
			else
				pathListener = new PathfinderListener( ConfirmMoveWithoutAsking );
			
			characterSeeker.StartPath( selectedCharacter.transform.position, destination, pathListener.StartPathCallback );
		}
			
		

		
		
		protected void ConfirmMovement( Control sender ) {
			// convertemos caminho em uma lista de gameVectors para enviar para o servidor
			List< GameVector > gameVectorPoints = new List< GameVector >();
			foreach( Vector3 point in points )
				gameVectorPoints.Add( ControllerUtils.CreateGameVector( point ) );
			
			// enviando caminho para o servidor
			MoveCharacterRequest request = new MoveCharacterRequest( 
				GameManager.GetInstance().GetLocalPlayer(), 
				characterID, 
				gameVectorPoints, 
				ControllerUtils.CreateGameVector( pathListener.finalDirection ), 
				pathListener.finalStance
			);
			GameManager.GetInstance().SendRequest( request );

			OnDone();
			GUIManager.Reset();
		}
		
		
		protected void ConfirmMoveWithoutAsking( List< Vector3 > points, CoverData coverData ) {
			// TODO: dar um jeito de sumir com esses caras aqui? (dependências temporais)
			this.points = points;
			this.coverData = coverData;
			
			if( coverData != null ) {
				TakeCoverAndConfirmMovement( null );
			} else {
				ConfirmMovement( null );
			}
		}
		
		
		protected void AskMoveConfirmation( List< Vector3 > points, CoverData coverData ) {
			trail = new MovementTrail( visibilityMap, GameManager.GetInstance().GetTurnFaction(), nearDistance, farDistance );
			trail.GenerateTrail( points );
			
			// TODO: dar um jeito de sumir com esses caras aqui (dependência temporal)
			this.points = points;
			this.coverData = coverData;
			confirmationMenu.AddButton( new MenuOption( IconType.OK, ConfirmMovement ) );
			confirmationMenu.AddButton( new MenuOption( IconType.CANCEL, CancelMovement ) );
				
			if( coverData != null )
				confirmationMenu.AddButton( new MenuOption( IconType.TAKE_COVER, TakeCoverAndConfirmMovement ) );
				
			confirmationMenu.Show( new Point( Screen.width / 2, Screen.height / 2 ) );
			IsoCameraScript.GetInstance().userInputIsBlocked = true;
			IsoCameraScript.GetInstance().FocusOn( trail.DestinationObject );
		}
		
		
		protected void TakeCoverAndConfirmMovement( Control sender ) {
			pathListener.finalStance = Stance.CROUCHING;
			
			for( int i = 0; i < points.Count; i++ ) {
				Vector3 distanceToCover = points[ i ] - ( coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT );
				
				distanceToCover.y = 0.0f;
				
				// estamos aproximando nosso personagem por um cilindro aqui
				if( distanceToCover.magnitude < Common.CharacterInfo.BOX_HALF_WIDTH ) {
					Vector3 currDistanceToWall = Vector3.Project( distanceToCover, coverData.normal );
					Vector3 correction = currDistanceToWall * ( Common.CharacterInfo.BOX_HALF_WIDTH / currDistanceToWall.magnitude );
					correction.y = 0.0f;
					points[ i ] = coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT + correction;
				}
			}
			
			// primeiro, vamos calcular a direcao de cobertura, a qual é tangente ao plano de cobertura)
			Vector3 moveDir = points[ points.Count - 1 ] - points[ points.Count - 2 ];
			Vector3 projectedCoverDir = Vector3.Project( moveDir, coverData.normal );
			pathListener.finalDirection = moveDir - projectedCoverDir;
			pathListener.finalDirection.y = 0.0f;
			pathListener.finalDirection.Normalize();
			
			if( NanoMath.Equals( pathListener.finalDirection.magnitude, 0.0f ) )
				pathListener.finalDirection = coverData.normal;
			
			ConfirmMovement( null );
		}
		
		
		protected void CancelMovement( Control sender ) {
			OnDone();
		}
		
		
		public override void OnDone() {
			if( trail != null )
				trail.Destroy();
			
			base.OnDone();
		}
			
		public override void OnRemoved() {
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
		}
	}
}