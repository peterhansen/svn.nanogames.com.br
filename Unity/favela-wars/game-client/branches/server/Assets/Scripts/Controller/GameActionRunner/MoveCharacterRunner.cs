using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameCommunication;
using Utils;

namespace GameController {
	
	public class MoveCharacterRunner : GameActionRunner {
		
		public MoveCharacterData data;
	
		public MoveCharacterRunner( MoveCharacterData moveCharacterData ) : base( true ) {
			this.data = moveCharacterData;
		}
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.characterWorldID );
			
			if ( gameObject == null ) {
				Debugger.LogError( "MoveCharacterRunner: worldObjectID not found: " + data.characterWorldID );
			} else {
				PathFollowerScript pfs = gameObject.GetComponent< PathFollowerScript >();
				pfs.Initialize( this, data );
			}
		}
	}
}

