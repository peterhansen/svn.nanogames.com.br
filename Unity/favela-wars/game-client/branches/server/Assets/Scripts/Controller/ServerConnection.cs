using System;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameCommunication;
using GameController;
using Utils;


[ ExecuteInEditMode ]
public class ServerConnection : MonoBehaviour {
	
	private const string OBJECT_NAME = "ServerConnectionScript";
	
	
	private static readonly List< ServerRequest > pendingRequests = new List< ServerRequest >();
	
	
	public static void SendData( object data, ServerConnectionListener listener ) {
		GameObject serverScript = GameObject.Find( OBJECT_NAME );
		if ( serverScript == null ) {
			serverScript = new GameObject( OBJECT_NAME );
			serverScript.AddComponent< ServerConnection >();
		}
		
		string request64 = Util.SerializeToBase64String( data );
		byte[] requestData = Encoding.ASCII.GetBytes( request64 );
		
		WWW www = new WWW( Common.Paths.GAME_SERVER_URL, requestData );
		
		ServerRequest request = new ServerRequest( www, data, listener );
		pendingRequests.Add( request );
	}
	
	
	public void Awake() {
		DontDestroyOnLoad( this );
	}
	
	
	public void Update() {
		for ( int i = 0; i < pendingRequests.Count; ) {
			ServerRequest request = pendingRequests[ i ];
			
			if ( request.CheckResponse() ) {
				pendingRequests.RemoveAt( i );
			} else {
				++i;
			}
		}
	}	
	
	
	/// <summary>
	/// Classe auxiliar para gerenciar conexões pendentes.
	/// </summary>
	private class ServerRequest {
		
		public object data;
		
		public ServerConnectionListener listener;
		
		public WWW www;
		
		
		public ServerRequest( WWW www, object data, ServerConnectionListener listener ) {
			this.www = www;
			this.data = data;
			this.listener = listener;
		}
		
		
		/// <summary>
		/// Verifica se houve resposta do servidor, e em caso positivo chama a callback.
		/// <returns><c>bool</c> indicando se a conexão foi encerrada.</returns>
		/// </summary>
		public bool CheckResponse() {
			if ( www.isDone ) {
				Debugger.Log( "Response size: " + www.bytes.Length );
				
				try {
					List< Response > responses = Util.DeserializeFromBase64String< List< Response > >( www.text ); 
					listener.OnServerResponse( responses );
				} catch ( Exception e ) {
					listener.OnServerError( e );
					Debugger.LogError( "Error deserializing server data.\n" + e );
				}
				
				return true;
			} else {
				return false;
			}
		}
		
	}	
	
}

