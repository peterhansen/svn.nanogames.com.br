using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class CharacterHitRunner : GameActionRunner {
	
		private CharacterHitData data;
		
		public CharacterHitRunner( CharacterHitData hitData ) : base( false ) {
			this.data = hitData;
		}
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.person );
			
			if ( !data.isAlive ) {
				gameObject.transform.RotateAround( Vector3.right, 90.0f );
			}
		}
	}
}

