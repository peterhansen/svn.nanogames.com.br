using System;
using UnityEngine;
using GameCommunication;

namespace GameController {
	
	public class TurnChangeRunner : GameActionRunner {
		
		protected TurnData data;
		
		
		public TurnChangeRunner( TurnChangeData turnChangeData ) : base( false ) {			
			this.data = turnChangeData.turnData;
		}
		
		
		protected override void Execute() {	
			GameManager.GetInstance().SetTurnData( data );
			VisibilityMap visibilityMap = VisibilityMap.GetInstance();
			
			if ( visibilityMap != null && data.faction != Utils.Faction.NEUTRAL ){
				visibilityMap.UpdateVisibilityBuffer( data.faction );
				
				//Atualizando o DateTime
				data.initialTurnTime = DateTime.Now;
				
			}
		}	
	}
}

