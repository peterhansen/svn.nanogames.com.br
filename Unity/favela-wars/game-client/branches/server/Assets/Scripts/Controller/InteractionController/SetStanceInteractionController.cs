using System;

using UnityEngine;

using Utils;
using GameController;
using GameCommunication;


namespace GameController {
	
	public class SetStanceInteractionController : ActionInteractionController {
		
		protected Stance currentStance;
		
		public SetStanceInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			currentStance = ( action as SetStanceGameAction ).currentStance;
		}
		
		public override void Initialize() {
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			GameManager.GetInstance().SendRequest( new SetStanceRequest( GameManager.GetInstance().GetLocalPlayer(), characterID, currentStance == Stance.CROUCHING? Stance.STANDING : Stance.CROUCHING ) );
			GUIManager.Reset();
		}
	}
}

