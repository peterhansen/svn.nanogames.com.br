using System;

using UnityEngine;


/// <summary>
/// Workaround para restrição da Unity, que não mantém a tag ao gerar um prefab.
/// </summary>
[ ExecuteInEditMode ]
public class SelfTagger : MonoBehaviour {

	public string selfTag;
	
	
	public void Update(){
		this.gameObject.tag = selfTag;
		DestroyImmediate( this );
	}
	
}
