using System;

using UnityEngine;


namespace GameController {
	
	public class CoverData {
		public Vector3 position;
		public Vector3 normal;
	}
	
}

