using System;
using UnityEngine;
using GameCommunication;
using Utils;

namespace GameController {
	public class SetStanceRunner : GameActionRunner {
		private SetStanceData data;
		
	
		public SetStanceRunner( SetStanceData setStanceData ) : base( false ) {
			this.data = setStanceData;
		}
		
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.characterWorldID );
			if ( gameObject == null ) {
				Debugger.LogError( "MoveCharacterRunner: worldObjectID not found: " + data.characterWorldID );
				return;
			}
			
			CharacterInfoScript characterInfo = gameObject.GetComponent< CharacterInfoScript >();
			if( characterInfo == null ) {
				Debugger.LogError( "MoveCharacterRunner: not possible to change stance of character " + data.characterWorldID );
				return;
			}
			characterInfo.stance = data.stance;
			
			AnimationController animationController = gameObject.GetComponent< AnimationController >();
			if( animationController == null ) {
				Debugger.LogError( "MoveCharacterRunner: not possible to play stance change animation for character " + data.characterWorldID );
				return;
			}
			animationController.Play( data.stance == Stance.CROUCHING? AnimationType.CROUCH : AnimationType.RISE );
		}
	}
}