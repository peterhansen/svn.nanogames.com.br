using System;
using System.Collections;
using System.Collections.Generic;


using GameCommunication;


namespace GameController {
	
	public interface ServerConnectionListener {
		
		
		void OnServerResponse( List< Response > responses );
		
		void OnServerError( Exception e );
		
		
	}
	
}

