using System;
using System.Collections.Generic;
using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class ExplosionRunner : GameActionRunner {
		
		private ExplosionData data;
		
		private byte currentShot;
		
	
		public ExplosionRunner( ExplosionData explosionData ) : base( true ) {
			this.data = explosionData;
		}
		
		
		protected override void Execute() {
			ExplosionScript.Explode( data.center, data.ray, this );
		}
		
		
	}
}

