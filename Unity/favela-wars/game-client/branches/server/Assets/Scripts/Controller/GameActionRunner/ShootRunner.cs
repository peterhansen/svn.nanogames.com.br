using System;
using System.Collections.Generic;
using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class ShootRunner : GameActionRunner, TimerScriptListener {
		
		private ShootData data;
		
		private byte currentShot;
	
		public ShootRunner( ShootData shootData ) : base( true ) {
			this.data = shootData;
		}
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.shooterWorldID );
			// TODO obter informações da arma e munição do soldado ao atirar (visualização do projétil, emissão de partículas, etc.)
			
			//Debugger.Log( "ShootRunner.Execute: " + data.shooterWorldID );
			if ( gameObject == null ) {
				Debugger.LogError( "ShootRunner: shooter worldID not found: " + data.shooterWorldID );
			} else {
				gameObject.transform.LookAt( ControllerUtils.CreateVector3( data.hitPoints[ 0 ] ) );
				gameObject.transform.eulerAngles = new Vector3( 0.0f, gameObject.transform.eulerAngles.y, 0.0f );
				
				// agenda a execução dos tiros (os tiros já são ordenados pelo momento do disparo)
				foreach ( float time in data.shotTimes ) {
					TimerScript.AddTimer( ( int ) ( time * 1000 ), this );
				}
			}
		}
		
		public void ShootNext() {
			if ( currentShot < data.hitPoints.Length ) {
				ProjectileScript.Shoot( data.shotOrigin, data.hitPoints[ currentShot ], data.speed, ( currentShot == data.hitPoints.Length - 1 ) ? this : null );
				++currentShot;
			}
		}
		
		public void OnTimerEnded ( int timerID ) {
			ShootNext();
		}
	}
}

