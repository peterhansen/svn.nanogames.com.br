using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameCommunication;


namespace GameController {
	
	public class ActionInteractionController : InteractionController {
		
		protected GameObject selectedCharacter;
		
		protected GameAction action;
		
		/// <summary>
		/// Lista de inimigos na linha de tiro do personagem. Este controle permite ciclar rapidamente entre os alvos,
		/// sendo exibido de forma homogênea independentemente do tipo de controle que necessite desta interface.
		/// </summary>
		protected EnemyListControl enemyListControl = new EnemyListControl();
		
		
		public ActionInteractionController( GameObject selectedCharacter, GameAction action ) {
			this.selectedCharacter = selectedCharacter;
			this.action = action;
		}
		
		
		public void SetEnemiesList( List< int > enemiesList ) {
			enemyListControl.SetEnemiesList( enemiesList );
		}
		
		
		protected void AddEnemyListControl() {
			controls.Add( enemyListControl );
		}
		
	}
}

