using System;
using GameCommunication;
using UnityEngine;


namespace GameController {

	public class ProjectileScript : MonoBehaviour {
		
		private float speed;
		
		private float accTime;
		
		private float totalTime;
		
		private GameVector diff;
		
		private GameVector origin;
		
		private GameObject projectile;
		
		private static int count = 0;
		
		private ShootRunner runner;
		
		private Texture2D tex;
		
		
		public static void Shoot( GameVector origin, GameVector destination, float speed, ShootRunner runner ) {
			//Debug.DrawLine( ControllerUtils.CreateVector3( origin ), ControllerUtils.CreateVector3( destination ), Color.red, 10.0f );
			GameObject o = GameObject.CreatePrimitive( PrimitiveType.Sphere );
			o.transform.localScale = new Vector3( 0.3f, 0.3f, 0.3f );
			o.transform.position = new Vector3( origin.x, origin.y, origin.z );
			o.AddComponent( typeof( ProjectileScript ) );
			o.name = "Projectile #" + count++;
			
			ControllerUtils.ApplyVisibilityMap( o, VisibilityMap.GetInstance() );
			
			ProjectileScript s = ( ProjectileScript ) o.GetComponent( typeof( ProjectileScript ) );
			s.projectile = o;
			s.runner = runner;
			s.origin = origin;
			s.diff = destination - origin;
			s.speed = speed;
			
			s.totalTime = s.diff.Norm() / s.speed;
			
			if ( s.tex == null ) {
				s.tex = new Texture2D( 1, 1 );
				s.tex.SetPixel( 0, 0, Color.red );
				s.tex.Apply();
			}
			o.renderer.sharedMaterial.SetTexture( "_MainTex", s.tex );
			o.renderer.sharedMaterial.color = Color.white;
			
		}
		
		
		public void Update() {
			accTime += Time.deltaTime;
			
			GameVector pos;
			if ( accTime < totalTime ) {
				pos = origin + ( diff * ( accTime / totalTime ) );
			} else {
				// atingiu o alvo
				pos = origin + diff;
				Destroy( projectile );
				
				if ( runner != null )
					runner.OnDone();
			}
			transform.position = new Vector3( pos.x, pos.y, pos.z );
		}
		
	}

}

