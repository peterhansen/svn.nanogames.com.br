using System;

using UnityEngine;

using Utils;
using System.Collections.Generic;


namespace GameController {
	
	public class MovementTrail {
		
		protected const float TRAIL_MESH_WIDTH = 0.04f;
	
		protected const float TRAIL_HEIGHT = 0.2f;
		
		protected VisibilityMap visibilityMap;

		protected GameObject trailObject;
		
		protected MeshFilter meshFilter;
		
		protected Mesh trailMesh;
		
		protected GameObject destinationObject;
		public GameObject DestinationObject {
			get { return destinationObject; }
		}
		
		protected float nearDistance;
		
		protected float farDistance;
		
		protected Texture2D factionTexture;
		
		protected bool active;
		public bool Active {
			get {
				return active;
			}
			set {
				active = value;
				destinationObject.active = active;
				trailObject.active = active;
			}
		}
		
		
		public MovementTrail( VisibilityMap visibilityMap, Faction faction, float nearDistance, float farDistance ) {
			this.visibilityMap = visibilityMap;
			this.nearDistance = nearDistance;
			this.farDistance = farDistance;
			
			factionTexture = visibilityMap.GetTexture( faction );
			
			// criando cilindro de destino
			destinationObject = GameObject.CreatePrimitive( PrimitiveType.Cylinder );
			destinationObject.layer = Common.Layers.IGNORE_RAYCAST;
			destinationObject.renderer.material = new Material( Shader.Find( "Transparent/Bumped Diffuse" ) );
			destinationObject.transform.localScale = new Vector3( 0.7f, 0.3f, 0.7f );
			destinationObject.name = "Movement Destination";
			
			// criando malha dummy
			Texture2D dummyTexture = new Texture2D( 1, 1 );
			dummyTexture.SetPixel( 0, 0, Color.white );
			
			trailObject = new GameObject( "Trail Object" );
			MeshRenderer meshRenderer = trailObject.AddComponent< MeshRenderer >();
			meshRenderer.material = new Material( Shader.Find( "Particles/VertexLit Blended" ) );
			meshRenderer.material.SetTexture( "_MainTex", dummyTexture );
			meshFilter = trailObject.AddComponent< MeshFilter >();
			
			trailMesh = new Mesh();
			meshFilter.mesh = trailMesh;
		}
	
		
		public void GenerateTrail( List< Vector3 > points ) {
			Vector3[] meshVertices = new Vector3[ points.Count * 2 ];
			Vector3[] meshNormals = new Vector3[ points.Count * 2 ];
			Color[] meshColors = new Color[ points.Count * 2 ];
			
			Vector3 dir = new Vector3();
			Color vertexColor = new Color();
			float currentDistance = 0.0f;
			
			for( int i = 0; i < points.Count; i++ ) {
				Vector3 currentPoint = points[ i ] + Vector3.up * TRAIL_HEIGHT;
				
				// acumulando distância total
				if( i > 0 )
					currentDistance += ( points[ i ] - points[ i - 1 ] ).magnitude;

				if( i + 1 < points.Count ) {
					Vector3 newDir = ( points[ i + 1 ] - currentPoint ).normalized;
					if( i - 1 >= 0 ) {
						float beta = Vector3.Angle( newDir, points[ i ] - points[ i - 1 ] );
						if( NanoMath.GreaterThan( Mathf.Abs( beta ), 10.0f ) ) {
							newDir = ( newDir + dir ) * 0.5f;
						}
					}
					dir = newDir.normalized;
				} else {
					dir = ( points[ i ] - points[ i - 1 ] ).normalized;
				}
				Vector3[] currentSideVectors = GetSideVectors( currentPoint, dir );
			
				meshVertices[ i * 2 ] = currentSideVectors[ 0 ];
				meshVertices[ ( i * 2 ) + 1 ] = currentSideVectors[ 1 ];
				
				meshNormals[ i * 2 ] = Vector3.up;
				meshNormals[ ( i * 2 ) + 1 ] = Vector3.up;
				
				vertexColor = CalculateVertexColor( currentPoint, currentDistance );
				meshColors[ i * 2 ] = vertexColor;
				meshColors[ (i * 2) + 1 ] = vertexColor;
			}
			
			// vamos colorir o destinationObject de forma que ele fique na mesma cor do fim da trailMesh
			destinationObject.renderer.material.SetColor( "_Color", vertexColor );
			
			// multiplicamos por 6 = 2 * 3 (dois triângulos por plano, cada triângulo com 3 vértices)
			int[] meshTriangles = new int[ ( points.Count - 1 ) * 6 ];
			for( int i = 0; i < points.Count - 1; i++ ) {
				// triangulo 1 do plano
				meshTriangles[ i * 6 ] = i * 2;
				meshTriangles[ ( i * 6 ) + 1 ] = ( i * 2 ) + 1;
				meshTriangles[ ( i * 6 ) + 2 ] = ( i * 2 ) + 3;
				
				// triangulo 2 do plano
				meshTriangles[ ( i * 6 ) + 3 ] = meshTriangles[ ( i * 6 ) ];
				meshTriangles[ ( i * 6 ) + 4 ] = meshTriangles[ ( i * 6 ) + 2 ];
				meshTriangles[ ( i * 6 ) + 5 ] = ( i * 2 ) + 2;
			}
			trailMesh.vertices = meshVertices;
			trailMesh.normals = meshNormals;
			trailMesh.triangles = meshTriangles;
			trailMesh.colors = meshColors;
			
			destinationObject.transform.position = points[ points.Count - 1 ];
		}
		
		
		protected Vector3[] GetSideVectors( Vector3 point, Vector3 dir ) {
			Vector3 sideVector = Vector3.Cross( dir, Vector3.up ) * TRAIL_MESH_WIDTH;
			return new Vector3[]{ point + sideVector, point - sideVector };
		}
		
		
		protected Color CalculateVertexColor( Vector3 vertex, float distance ) {
			// calculando cor
			float safeColorPercentage = 0.0f;
			if( distance > farDistance )
				safeColorPercentage = 1.0f;
			else if( distance > nearDistance )
				safeColorPercentage = ( distance - nearDistance ) / ( farDistance - nearDistance );
			
			// calculando alpha
			float alpha = CalculateVertexAlpha( vertex );
			return new Color( safeColorPercentage, 1.0f - safeColorPercentage, 0.0f, alpha * 0.7f );
		}
		
		
		// ATENÇÃO: esse método precisa ser um reflexo do shader de visibilidade
		protected float CalculateVertexAlpha( Vector3 vertex ) {			
			Vector3 voxelCoord = new Vector3( ( vertex.x - visibilityMap.minPoint.x ) / visibilityMap.sectorSize.x,
				( vertex.y - visibilityMap.minPoint.y ) / visibilityMap.sectorSize.y,
				( vertex.z - visibilityMap.minPoint.z ) / visibilityMap.sectorSize.z
			);
		
			float sectorJ = Mathf.Floor( voxelCoord.y );
		
			float DW = 1.0f / ( visibilityMap.numberOfSectors.y * ( visibilityMap.numberOfSectors.x + 2 ) );
			Vector2 sectorUV = new Vector2( 
				( sectorJ * ( visibilityMap.numberOfSectors.x + 2.0f ) + voxelCoord.x + 1.0f ) * DW, 
				( voxelCoord.z + 1.0f ) / ( visibilityMap.numberOfSectors.z + 2.0f ) );
			
			float dist = 2.0f * ( voxelCoord.y - sectorJ - 0.5f );
			
			float neighborSectorJ = Mathf.Max( sectorJ + Mathf.Sign( dist ), 0.0f );
			Vector2 neighborSectorUV = new Vector2( ( neighborSectorJ * ( visibilityMap.numberOfSectors.x + 2.0f ) + voxelCoord.x ) * DW, sectorUV.y );
			
			int sectorX = (int) (sectorUV.x * visibilityMap.visibilityBuffer[0].width);
			int sectorY = (int) (sectorUV.y * visibilityMap.visibilityBuffer[0].height);
			int neighborSectorX = (int) (neighborSectorUV.x * visibilityMap.visibilityBuffer[0].width);
			int neighborSectorY = (int) (neighborSectorUV.y * visibilityMap.visibilityBuffer[0].height);
			
			return Mathf.Max( factionTexture.GetPixel( sectorX, sectorY ).r,
				              factionTexture.GetPixel( neighborSectorX, neighborSectorY ).r * Mathf.Abs( dist ) );
		}
		
		public void Destroy() {
			GameObject.DestroyImmediate( trailObject );
			GameObject.DestroyImmediate( destinationObject );
		}
	}

}

