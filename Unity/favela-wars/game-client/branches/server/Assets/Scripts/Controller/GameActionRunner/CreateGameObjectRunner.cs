using System;

using UnityEngine;

using GameCommunication;

using Utils;


namespace GameController {
	
	public class CreateGameObjectRunner : GameActionRunner {
	
		protected CreateGameObjectData data;
	
		
		public CreateGameObjectRunner( CreateGameObjectData createGameObjectData ) : base( false ) {
			this.data = createGameObjectData;
		}
		
		
		protected override void Execute() {
			WorldManager.GetInstance().AddCharacter( data );
		}
	}
}

