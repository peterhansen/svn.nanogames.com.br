using System;
using System.Collections.Generic;

using UnityEngine;


namespace GameController {
	
	public enum AnimationParts {
		HEAD,
		TRUNK,
		LEGS,
		FEET
	}
	
	public enum AnimationType {
		IDLE,
		WALK,
		CROUCH,
		RISE,
		THROW
	}
	
	public class AnimationController : MonoBehaviour {
			
		protected const float CROSS_FADE_TIME = 0.1f;
	
		public static readonly string[] BODY_PART_NAMES = { "cabeca", "tronco", "pernas", "botas" };
		
		public static readonly string[] ANIMATION_NAMES = { "idle", "anda", "agacha", "levanta", "lanca" };
		
		public static readonly WrapMode[] ANIMATION_WRAP_MODES = { 
			WrapMode.Loop, 
			WrapMode.Loop, 
			WrapMode.ClampForever, 
			WrapMode.ClampForever, 
			WrapMode.ClampForever
		};
		
		protected GameObject[] animatedParts;
		
		protected List< string > animationNames = new List< string >();
		
		public void SetAnimatedParts( GameObject[] animatedParts ) {
			this.animatedParts = animatedParts;
			Stop();
		}
		
		public void Play( AnimationType animation ) {
			foreach( GameObject animatedPart in animatedParts )
				animatedPart.animation.CrossFade( ANIMATION_NAMES[ (int) animation ], CROSS_FADE_TIME );
		}
	
		
		public void Stop() {
			Play( AnimationType.IDLE );
		}
	}
}

