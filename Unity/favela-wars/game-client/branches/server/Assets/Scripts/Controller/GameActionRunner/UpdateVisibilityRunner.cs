using System;
using GameCommunication;

namespace GameController {
	
	public class UpdateVisibilityRunner : GameActionRunner {
		
		protected UpdateVisibilityData data;
	
		
		public UpdateVisibilityRunner( UpdateVisibilityData data ) : base( false ) {
			this.data = data;
		}
		
		
		protected override void Execute() {
		}
		
	}
}
