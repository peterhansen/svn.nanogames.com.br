using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GameController;
using Utils;

using UnityEngine;

using Object = UnityEngine.Object;


[ ExecuteInEditMode ]
public class BuildingInfo : MonoBehaviour {
	
	private static int layer;
	
	private static int maxLayer;
	
	private static Dictionary< GameObject, int > objectsLayers = new Dictionary< GameObject, int >();
	
	/// <summary>
	/// 
	/// </summary>
	public int childrenLayer;
	
	
	public static void PrepareScene() {
		objectsLayers = new Dictionary< GameObject, int >();
		maxLayer = 0;
		
		// ordena os objetos pela sua posição y, de forma que, ao verificar qual a camada do piso de um objeto,
		// as camadas inferiores já tenham sido calculadas.
		Object[] tempObjects = GameObject.FindSceneObjectsOfType( typeof( GameObject ) );
		GameObject[] sceneObjects = new GameObject[ tempObjects.Length ];
		for ( int i = 0; i < tempObjects.Length; ++i )
			sceneObjects[ i ] = tempObjects[ i ] as GameObject;
		
		Array.Sort( sceneObjects, new TransformYComparer() );
		
		foreach ( GameObject obj in sceneObjects ) {
			Transform rootTransform = obj.transform;
			Bounds rootBounds = new Bounds();
			
			BuildingInfo buildInfo = null;
			Transform t = rootTransform.parent;
			while ( buildInfo == null && t != null ) {
				buildInfo = t.GetComponent< BuildingInfo >();
				if ( buildInfo != null ) {
					rootBounds = ControllerUtils.GetBox( t.gameObject, false );
					break;
				}
				t = t.parent;
			}
			
//			ObjectSnapper snapper = obj.GetComponent< ObjectSnapper >();
			string lower = obj.name.ToLower();
			bool isWall = lower.StartsWith( "parede" ) || lower.StartsWith( "coluna" );//snapper != null && ( snapper.snapMode == ObjectSnapper.SnapMode.Wall || snapper.snapMode == ObjectSnapper.SnapMode.Column );
			
			if ( buildInfo == null ) {
				// se o objeto não está numa hierarquia que indique sua camada, a obtém por raycast
				AddObjectInLayer( GetGroundLayer( obj ), obj );
			} else {
				Bounds b = ControllerUtils.GetBox( obj, false );
				float diff = b.min.y - rootBounds.min.y;
				int objectLayer = 2 + ( ( int ) Math.Round( diff / Common.BUILDING_LAYER_HEIGHT ) << 1 ) + ( isWall ? 1 : 0 );
				
				AddObjectInLayer( objectLayer, obj );
			}
		}
		
		layer = maxLayer;
	}
	
	
	public static void RebuildScene() {
		foreach ( GameObject go in GameObject.FindGameObjectsWithTag( Common.Tags.TERRAIN ) ) {
			BuildingInfo b = go.GetComponent< BuildingInfo >();
			if ( b != null ) {
				b.ResetChildren();
				
				if ( b.childrenLayer > maxLayer )
					maxLayer = b.childrenLayer;
			}
		}
		
		layer = maxLayer;
	}
	
	
	private void ResetChildren() {
		SetChildrenLayer( childrenLayer );
	}
	
	
	public void SetChildrenLayer( int layer ) {
		foreach ( Transform t in gameObject.GetComponentsInChildren< Transform >( true ) ) {
			AddObjectInLayer( layer, t.gameObject );
		}
	}
	
	
	public static int MaxLayer {
		get { return maxLayer; }
	}
	
	
	private static void AddObjectInLayer( int layer, GameObject gameObject ) {
		objectsLayers[ gameObject ] = layer;
		if ( layer > maxLayer )
			maxLayer = layer;
//		Debugger.Log( gameObject.name + " -> " + objectsLayers[ gameObject ] );
	}
	
	
	/// <summary>
	/// Obtém a camada do piso sob o objeto <c>obj</c>. O menor valor retornado é 0 (zero).
	/// </summary>
	public static int GetGroundLayer( GameObject obj ) {
		RaycastHit hit;
		Bounds b = ControllerUtils.GetBox( obj, false );
		Ray ray = new Ray( b.center, Vector3.down );
		if ( Physics.Raycast( ray, out hit, Mathf.Infinity ) ) {
			try {
				return objectsLayers[ hit.collider.gameObject ];
			} catch ( Exception ) {
			}
		}
		
		return 0;
	}
	
	
	/// <summary>
	/// Atualiza a informação de camada do objeto <c>obj</c>, de acordo com o piso onde se encontra.
	/// </summary>
	public static void RefreshLayer( GameObject obj ) {
		objectsLayers[ obj ] = GetGroundLayer( obj );
//		Debugger.Log( obj.name + " / " + objectsLayers[ obj ] );
	}
	
	
	/// <summary>
	/// Obtém a camada do objeto <c>obj</c>. Caso não haja informação sobre sua camada, é retornado <c>0</c> (zero).
	/// </summary>
	public static int GetLayer( GameObject obj ) {
		foreach ( GameObject o in objectsLayers.Keys ) {
			if ( o == obj ) {
				return objectsLayers[ o ];
			}
		}
		
		return 0;
	}
	
	
	/// <summary>
	/// Define a camada de visibilidade de acordo com a camada do personagem selecionado.
	/// </summary>
	public static void SetLayerByCharacter( GameObject character ) {
		SetVisibleLayer( GetLayer( character ) );
	}
	
	
	/// <summary>
	/// Indica se o objeto é andável, ou seja, se é algum tipo de piso ou terreno.
	/// </summary>
	public static bool IsWalkable( GameObject obj ) {
		return ( GetLayer( obj ) & 1 ) == 0;
	}
	
	
	public static void IncreaseVisibleLayer() {
		if ( CanIncreaseLayer() )
			SetVisibleLayer( layer + 1 );
	}
	
	
	public static void ReduceVisibleLayer() {
		if ( CanDecreaseLayer() )
			SetVisibleLayer( layer - 1 );
	}
	
	
	public static void SetVisibleLayer( int l ) {
		layer = NanoMath.Clamp( l, 0, maxLayer );
		
		foreach ( GameObject obj in objectsLayers.Keys ) {
			SetObjectActive( obj, objectsLayers[ obj ] <= layer );
		}		
	}
	
	
	public static bool CanIncreaseLayer() {
		return layer < maxLayer;
	}
	
	
	public static bool CanDecreaseLayer() {
		return layer > 0;
	}
	
	
	/// <summary>
	/// Método que ativa/desativa a visibilidade e colisão de objetos. O objeto como um todo não é ativado/desativado
	/// pois isso causa problemas ao tentar localizá-los futuramente com métodos como GameObject.Find().
	/// </summary>
	private static void SetObjectActive( GameObject o, bool active ) {
		foreach ( Renderer r in o.GetComponentsInChildren< Renderer >() ) {
			r.enabled = active;
		}
		foreach ( Collider c in o.GetComponentsInChildren< Collider >() ) {
			c.enabled = active;
		}
	}
	
	
	internal class TransformYComparer : IComparer< GameObject > {
		public int Compare( GameObject g1, GameObject g2 ) {
			Bounds b1 = ControllerUtils.GetBox( g1, false );
			Bounds b2 = ControllerUtils.GetBox( g2, false );
			
			return b1.min.y <= b2.min.y ? -1 : 1;
		}
		
	}
	
}
