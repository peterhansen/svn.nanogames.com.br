using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameController;
using GameCommunication;


using Utils;
using LitJson;
//using Newtonsoft.Json;
using System.Xml;

	
public class ChooseMission : MonoBehaviour {
	
	public void Start() {
		Caching.CleanCache(); // TODO teste - limpa o cache antes de come�ar uma miss�o	
		Application.runInBackground = true;
		
		try {
			string s = Util.GetUrlDataAsString( "http://192.168.1.32:9292/maps.json" );
			Debugger.Log( "raw:\n" + s );
			
//			XmlDocument o = JsonConvert.DeserializeXmlNode(s);

			
			JsonReader o = new JsonReader( s );
			JsonData maps = JsonMapper.ToObject( o );
//			for ( int i = 0; i < maps.Count; ++i ) {
////				Debugger.Log( string.Format( "Map({0}): {1}", i, maps[ i ] ) );
//				Debugger.Log( maps[ "map" ][ i ] );
//			}
			
			Debugger.Log( "MORRA:\n" + JsonToXml( s ) );
			
//			foreach ( string mapName in data[ "maps" ] ) {
//				Debugger.Log( "->" + mapName );
//			}
//			foreach ( object bla in morra ) {
//				Debugger.Log( "ADDSA" );
//			}
//			Debugger.Log( "Maps: " + data[ "maps" ] );
//			Debugger.Log( "Total: " + data[ "total" ] );
//			Debugger.Log( "Map 0: " + data[ "maps" ][ 0 ] );
//			while ( o.Read() ) {
//				Debugger.Log( o.Token + ", " + o.Value );
//			}
//			object o = JsonConvert.DeserializeObject( s );
			
			Debugger.Log( "convert:\n" + o );
		} catch ( Exception e ) {
			Debugger.LogError( e );
		}
	}
	

	public void OnGUI() {
		string[] missions = Directory.GetDirectories( Path.Combine( Common.Paths.SERVER, "missions" ) );
	
		for ( int i = 0; i < missions.Length; ++i ) {
			string mission = Path.GetFileNameWithoutExtension( missions[ i ] );
			bool even = ( i & 1 ) == 0;
			int y = 10 + ( i >> 1 ) * 40;
			
			if( GUI.Button( new Rect( even ? 10 : 200, y, 100, 30 ), mission ) ) {
				GameManager gc = GameManager.GetInstance();
				
				List< uint > soldierIDs = new List< uint > { 0x00ff00ff, 0x00ffffff, 0xffff00ff, 0x000000ff, 0x00ff0000 };
				
				gc.SendRequest( new StartMissionRequest( gc.GetLocalPlayer(), mission, soldierIDs ) );
				gc.SendRequest( new LoadSnapshotRequest( gc.GetLocalPlayer() ) );
				
				Application.LoadLevel( "Mission Screen" );
				this.enabled = false;
			}	
		}
	}
	
	
	public static XmlDocument JsonToXml(string json)
    {
        XmlNode newNode = null;
        XmlNode appendToNode = null;
        XmlDocument returnXmlDoc = new XmlDocument();
        returnXmlDoc.LoadXml("<Document />");
        XmlNode rootNode = returnXmlDoc.SelectSingleNode("Document");
        appendToNode = rootNode;

        string[] arrElementData;
        string[] arrElements = json.Split('\r');
        foreach (string element in arrElements)
        {
            string processElement = element.Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
            if ((processElement.IndexOf("}") > -1 || processElement.IndexOf("]") > -1) &&
                appendToNode != rootNode)
            {
                appendToNode = appendToNode.ParentNode;
            }
            else if (processElement.IndexOf("[") > -1)
            {
                processElement = processElement.Replace(":", "").Replace("[", "").Replace("\"", "").Trim();
                newNode = returnXmlDoc.CreateElement(processElement);
                appendToNode.AppendChild(newNode);
                appendToNode = newNode;
            }
            else if (processElement.IndexOf("{") > -1 && processElement.IndexOf(":") > -1)
            {
                processElement = processElement.Replace(":", "").Replace("{", "").Replace("\"", "").Trim();
                newNode = returnXmlDoc.CreateElement(processElement);
                appendToNode.AppendChild(newNode);
                appendToNode = newNode;
            }
            else
            {
                if (processElement.IndexOf(":") > -1)
                {
                    arrElementData = processElement.Replace(": \"", ":").Replace("\",", "").Replace("\"", "").Split(':');
                    newNode = returnXmlDoc.CreateElement(arrElementData[0]);
                    for (int i = 1; i < arrElementData.Length; i++)
                    { newNode.InnerText += arrElementData[i]; }
                    appendToNode.AppendChild(newNode);
                }
            }
        }

        return returnXmlDoc;
    }
	
}
