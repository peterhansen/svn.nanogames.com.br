Shader "Custom/VisibilityMapShader" {
	
	Properties {
		_MinPoint ( "Min Point", Vector ) = ( 0.0, 0.0, 0.0, 1.0 )
		_NumberOfSectors( "Number Of Sectors", Vector ) = ( 1.0, 1.0, 1.0, 1.0 )
		_SectorSize( "Sector Dimensions", Vector ) = ( 1.0, 1.0, 1.0, 1.0 )
		_DW( "Delta W", Float ) = 1.0
		_NumberOfSectorsXPlusTwo( "Number Of X Sectors Plus Two", Float ) = 1.0
		_NumberOfSectorsYMinusOne( "Number Of Y Sectors Minus One", Float ) = 1.0
		_NumberOfSectorsZPlusTwo( "Number of Z Sector Plus Two", Float ) = 1.0
		_NumberOfSectorsZPlusOne( "Number of Z Sector Plus One", Float ) = 1.0
		_MainTex( "Color Map", 2D ) = "black" {}
		_BumpMap( "Normal Map", 2D ) = "bump" {}
		_VisibilityMap ("Visibility Map", Rect) = "white" {}
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 300

		CGPROGRAM
	    #pragma surface surf Lambert
	    
	    struct Input {
			float3 worldPos;
			float2 uv_MainTex;
        	float2 uv_BumpMap;
		};
	      
		float4 _MinPoint;
		float4 _NumberOfSectors;
		float4 _SectorSize;
		// TODO verificar uso de outros tipos de valor fora float (ponto fixo, half, etc.)
		half _DW;
		half _NumberOfSectorsXPlusTwo;
		half _NumberOfSectorsYMinusOne;
		half _NumberOfSectorsZPlusTwo;
		half _NumberOfSectorsZPlusOne;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _VisibilityMap;
			
		void surf (Input IN, inout SurfaceOutput o) {	  
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
				
			// TODO se impedirmos o objeto de sair do mapa, não é necessário fazer clamp
			float3 voxelCoord = float3( 
//				clamp( ( IN.worldPos.x - _MinPoint.x ) / _SectorSize.x, 0.0f, _NumberOfSectorsXPlusTwo ),
				( IN.worldPos.x - _MinPoint.x ) / _SectorSize.x,
				( IN.worldPos.y - _MinPoint.y ) / _SectorSize.y,
				( IN.worldPos.z - _MinPoint.z ) / _SectorSize.z
			);
		
			half sectorJ = floor( voxelCoord.y );
		
			float2 sectorUV = float2( 
				( sectorJ * _NumberOfSectorsXPlusTwo + voxelCoord.x + 1.0f ) * _DW, 
				( voxelCoord.z + 1.0f ) / _NumberOfSectorsZPlusTwo );
			
			half dist = 2.0f * ( voxelCoord.y - sectorJ - 0.5f );
			
			half neighborSectorJ = max( sectorJ + sign( dist ), 0.0f );
			float2 neighborSectorUV = float2( ( neighborSectorJ * _NumberOfSectorsXPlusTwo + voxelCoord.x ) * _DW, sectorUV.y );
			o.Albedo *= tex2D(_MainTex, IN.uv_MainTex).a * max( tex2D(_VisibilityMap, sectorUV ).r, tex2D( _VisibilityMap, neighborSectorUV ).r * abs( dist ) );
			
//			o.Albedo *= tex2D(_MainTex, IN.uv_MainTex).a * tex2D(_VisibilityMap, sectorUV ).r;
		}
		ENDCG
	}
	Fallback "Diffuse"
}