Shader "Custom/terrain_splat_basic" {
	Properties {
		// Common
		_TileBase ("Tile base (RGB)", 2D) = "white" {}
		_Tile01 ("Tile 01 (RGB)", 2D) = "white" {}
		_Tile02 ("Tile 02 (RGB)", 2D) = "white" {}
		_Tile03 ("Tile 03 (RGB)", 2D) = "white" {}
		_DarkMap ("Dark map (RGB)", 2D) = "white" {}
		
		// Programmable pipeline
		_SplatMap ("Splat map (RGBA)", 2D) = "white" {}
		
		// fog of war
		_MinPoint ( "Min Point", Vector ) = ( 0.0, 0.0, 0.0, 1.0 )
		_NumberOfSectors( "Number Of Sectors", Vector ) = ( 1.0, 1.0, 1.0, 1.0 )
		_SectorSize( "Sector Dimensions", Vector ) = ( 1.0, 1.0, 1.0, 1.0 )
		_DW( "Delta W", Float ) = 1.0
		_NumberOfSectorsXPlusTwo( "Number Of X Sectors Plus Two", Float ) = 1.0
		_NumberOfSectorsYMinusOne( "Number Of Y Sectors Minus One", Float ) = 1.0
		_NumberOfSectorsZPlusTwo( "Number of Z Sector Plus Two", Float ) = 1.0
		_NumberOfSectorsZPlusOne( "Number of Z Sector Plus One", Float ) = 1.0
		//_MainTex( "Color Map", 2D ) = "black" {}
		//_BumpMap( "Normal Map", 2D ) = "bump" {}
		_VisibilityMap ("Visibility Map", Rect) = "white" {}
		
		// Fixed function?
		//_TileAlphaBase ("Tile alpha base", 2D) = "white" {}
		//_TileAlpha01 ("Tile alpha 01", 2D) = "white" {}
		//_TileAlpha02 ("Tile alpha 02", 2D) = "white" {}
		//_TileAlpha03 ("Tile alpha 03", 2D) = "white" {}
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf IBL_simple
		
		half4 LightingIBL_simple (SurfaceOutput s, half3 lightDir, half atten) {
			return half4(s.Albedo, 1.0);
		}

		// fog of war
		float4 _MinPoint;
		float4 _NumberOfSectors;
		float4 _SectorSize;
		half _DW;
		half _NumberOfSectorsXPlusTwo;
		half _NumberOfSectorsYMinusOne;
		half _NumberOfSectorsZPlusTwo;
		half _NumberOfSectorsZPlusOne;

		sampler2D _VisibilityMap;
		sampler2D _SplatMap;
		sampler2D _TileBase;
		sampler2D _DarkMap;

		struct Input {
			float2 uv_SplatMap : TEXCOORD0;
			float2 uv_TileBase : TEXCOORD1;
			float2 uv_Tile01 : TEXCOORD2;
			float2 uv_Tile02 : TEXCOORD3;
			float2 uv_Tile03 : TEXCOORD4;
			
			// fog of war
			float3 worldPos;
			//float2 uv_MainTex;
        	//float2 uv_BumpMap;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 splat_combined = tex2D(_SplatMap, IN.uv_SplatMap);
			
			half4 base_color = tex2D(_TileBase, IN.uv_TileBase);
			half4 tile01_color = tex2D(_TileBase, IN.uv_Tile01);
			half4 tile02_color = tex2D(_TileBase, IN.uv_Tile02);
			half4 tile03_color = tex2D(_TileBase, IN.uv_Tile03);
			
			// Base color
			o.Albedo = base_color * splat_combined.r;
			o.Albedo += tile01_color * splat_combined.g;
			o.Albedo += tile02_color * splat_combined.b;
			o.Albedo += tile03_color * splat_combined.a;

			// Dark mapping
			o.Albedo *= tex2D(_DarkMap, IN.uv_SplatMap);
		
			o.Alpha = 1.0;
		
			// hack 
			o.Albedo *= 0.5f;
			
			
			// fog of war
			float3 voxelCoord = float3( 
				( IN.worldPos.x - _MinPoint.x ) / _SectorSize.x,
				( IN.worldPos.y - _MinPoint.y ) / _SectorSize.y,
				( IN.worldPos.z - _MinPoint.z ) / _SectorSize.z
			);
		
			half sectorJ = floor( voxelCoord.y );
		
			float2 sectorUV = float2( 
				( sectorJ * _NumberOfSectorsXPlusTwo + voxelCoord.x + 1.0f ) * _DW, 
				( voxelCoord.z + 1.0f ) / _NumberOfSectorsZPlusTwo );
			
			half dist = 2.0f * ( voxelCoord.y - sectorJ - 0.5f );
			
			 half neighborSectorJ = max( sectorJ + sign( dist ), 0.0f );
			float2 neighborSectorUV = float2( ( neighborSectorJ * _NumberOfSectorsXPlusTwo + voxelCoord.x ) * _DW, sectorUV.y );
			o.Albedo *= max( tex2D(_VisibilityMap, sectorUV ).r, tex2D( _VisibilityMap, neighborSectorUV ).r * abs( dist ) );
		}
		ENDCG
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		Lighting OFF
		
		Pass {
			SetTexture[_DarkMap]
			SetTexture[_TileBase]
			{
				combine previous * texture
			}
			SetTexture[_TileAlphaBase]
			{
				combine previous * texture
			}
		}
		
		Pass {
			Blend One One
			
			SetTexture[_DarkMap]
			SetTexture[_Tile01]
			{
				combine previous * texture
			}
			SetTexture[_TileAlpha01]
			{
				combine previous * texture
			}
		}	
		
		Pass {
			Blend One One
			
			SetTexture[_DarkMap]
			SetTexture[_Tile02]
			{
				combine previous * texture
			}
			SetTexture[_TileAlpha02]
			{
				combine previous * texture
			}
		}	
		
		Pass {
			Blend One One
			
			SetTexture[_DarkMap]
			SetTexture[_Tile03]
			{
				combine previous * texture
			}
			SetTexture[_TileAlpha03]
			{
				combine previous * texture
			}
		}	
	}
	
	FallBack "Diffuse"
}
