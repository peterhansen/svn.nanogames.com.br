Shader "Custom/VisibilityMapShader" {
	
	Properties {
		_MinPoint ( "Min Point", Vector ) = ( 0.0, 0.0, 0.0, 1.0 )
		_NumberOfSectors( "Number Of Sectors", Vector ) = ( 1.0, 1.0, 1.0, 1.0 )
		_SectorSize( "Sector Dimensions", Vector ) = ( 1.0, 1.0, 1.0, 1.0 )
		_DW( "Delta W", Float ) = 1.0
		_NumberOfSectorsXPlusTwo( "Number Of X Sectors Plus Two", Float ) = 1.0
		_NumberOfSectorsYMinusOne( "Number Of Y Sectors Minus One", Float ) = 1.0
		_NumberOfSectorsZPlusTwo( "Number of Z Sector Plus Two", Float ) = 1.0
		_NumberOfSectorsZPlusOne( "Number of Z Sector Plus One", Float ) = 1.0
		_MainTex( "Color Map", 2D ) = "black" {}
		_BumpMap( "Normal Map", 2D ) = "bump" {}
		_VisibilityMap ("Visibility Map", Rect) = "white" {}
		
		// SH Lighting
		_SH_scale ("SH scaling factor", Float) = 1.0
		_L00 ("L00 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_1_1 ("L_1_1 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_10 ("L_10 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_11 ("L_11 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_2_2 ("L_2_2 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_2_1 ("L_2_1 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_20 ("L_20 coeffcient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_21 ("L_21 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		_L_22 ("L_22 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
		// SH Lighting
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 300

		CGPROGRAM
	    //#pragma surface surf Lambert
	    #pragma surface surf NullLight vertex:vert
	   
		// Constants
		#define C1 0.429043
		#define C2 0.511664
		#define C3 0.743125
		#define C4 0.886227
		#define C5 0.247708
		#define SCALED(coeff) (_SH_scale * float3(coeff) * 12.2 )

	    half4 LightingNullLight (SurfaceOutput s, half3 lightDir, half atten) {
			return half4(s.Albedo, 1.0);
		}
	   
	    struct Input {
			float3 worldPos;
			float2 uv_MainTex;
        	float2 uv_BumpMap;
        	
        	float4 sh_color;
		};
	      
		float4 _MinPoint;
		float4 _NumberOfSectors;
		float4 _SectorSize;
		// TODO verificar uso de outros tipos de valor fora float (ponto fixo, half, etc.)
		half _DW;
		half _NumberOfSectorsXPlusTwo;
		half _NumberOfSectorsYMinusOne;
		half _NumberOfSectorsZPlusTwo;
		half _NumberOfSectorsZPlusOne;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _VisibilityMap;
		
		// SH Lighting
		float _SH_scale;
		float4 _L00;
		float4 _L_1_1;
		float4 _L_10;
		float4 _L_11;
		float4 _L_2_2;
		float4 _L_2_1;
		float4 _L_20;
		float4 _L_21;
		float4 _L_22;
		
		void vert (inout appdata_full v, out Input o)
		{
		    // HACK
		    //float3 N = v.normal;
		    float3 N = float3(v.normal.x, v.normal.y, v.normal.z);
		    N = mul(float3x3(_Object2World), N);
		    
		    // Irradiance
			float3 E = C1 * SCALED(_L_22) * ((N.x * N.x) - (N.y * N.y));
			E += C3 * SCALED(_L_20) * (N.z * N.z);
			E += C4 * SCALED(_L00);
			E += C5 * SCALED(_L_20);
			E += 2.0 * C1 * (SCALED(_L_2_2) * (N.x * N.y) + SCALED(_L_21) * (N.x * N.z) + SCALED(_L_2_1) * (N.y * N.z));
			E += 2.0 * C2 * (SCALED(_L_11) * N.x + SCALED(_L_1_1) * N.y + SCALED(_L_10) * N.z);
		
			o.sh_color = float4(E, 1.0);
		    
		    return;
		}
		// SH Lighting
			
		void surf (Input IN, inout SurfaceOutput o) {	  
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
				
			// TODO se impedirmos o objeto de sair do mapa, não é necessário fazer clamp
			float3 voxelCoord = float3( 
//				clamp( ( IN.worldPos.x - _MinPoint.x ) / _SectorSize.x, 0.0f, _NumberOfSectorsXPlusTwo ),
				( IN.worldPos.x - _MinPoint.x ) / _SectorSize.x,
				( IN.worldPos.y - _MinPoint.y ) / _SectorSize.y,
				( IN.worldPos.z - _MinPoint.z ) / _SectorSize.z
			);
		
			half sectorJ = floor( voxelCoord.y );
		
			float2 sectorUV = float2( 
				( sectorJ * _NumberOfSectorsXPlusTwo + voxelCoord.x + 1.0f ) * _DW, 
				( voxelCoord.z + 1.0f ) / _NumberOfSectorsZPlusTwo );
			
			half dist = 2.0f * ( voxelCoord.y - sectorJ - 0.5f );
			
			half neighborSectorJ = max( sectorJ + sign( dist ), 0.0f );
			float2 neighborSectorUV = float2( ( neighborSectorJ * _NumberOfSectorsXPlusTwo + voxelCoord.x ) * _DW, sectorUV.y );
			float4 fog_of_war = tex2D(_MainTex, IN.uv_MainTex).a * max( tex2D(_VisibilityMap, sectorUV ).r, tex2D( _VisibilityMap, neighborSectorUV ).r * abs( dist ) );
			
			o.Albedo *= fog_of_war;// * IN.sh_color;
			//o.Albedo = IN.sh_color;
		}
		ENDCG
	}
	Fallback "Diffuse"
}