#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

using Utils;
using GameController;
using GameCommunication;

using Object = UnityEngine.Object;


// alias para facilitar a geração de variações de combinações de materiais
using Combination = System.Collections.Generic.List< CompositeTexture >;


public class AssetGenerator {
	
	public const string WINDOW_TITLE = "Asset Manager";
	
	private const string UTILS_TITLE = WINDOW_TITLE + "/Util";
	
	public const string FIXES_TITLE = WINDOW_TITLE + "/Fixes";
	
	public const string PATH_PREFABS = "Assets/Prefabs/";
	
	public const string PATH_MATERIALS = "Assets/Materials/Generated/";
	
	public const string PATH_ASSETS = "Assets/Assets/";
	
	public static readonly string PATH_BUNDLES_MATERIALS = Common.Paths.SERVER + "/assetbundles/_materials/";
	
	public static readonly string PATH_BUNDLES_MESHES = Common.Paths.SERVER + "/assetbundles/_meshes/";
	
	private const string MENU_ITEM = "Asset Manager/Generate Assets";
	
	private const string ROOT_PART_NAME = "_root";
	
	private static readonly Regex validExtensions = new Regex( @"(\A.*\.(png|jpg|jpeg|bmp|tif)+\z)" );
	
	private static readonly Regex wallsRegex = new Regex( @"(paredes|colunas)" );
	
	private static readonly Regex floorRegex = new Regex( @"(teto_chao|teto|chao)" );
	
	private static readonly Regex marqueeRegex = new Regex( @"(marquise)" );
	
	private static readonly Regex portalRegex = new Regex( @"/(paredeporta|paredegrande|paredejanelao|paredejanela|paredejanelagrande|paredebasculante|paredejanelagrandemeio|paredejanelameio|paredeportameio)/" );
	
	private const string terrainPrefix = "assets/terreno";
	
	public const string MAP_SUFFIX_DIFFUSE = "";
	public const string MAP_SUFFIX_DIFFUSE_ALT = "diffuse";
	public const string MAP_SUFFIX_DIFFUSE_ALT2 = "diffusemap";
	
	public const string MAP_SUFFIX_SPECULAR = "specular";
	public const string MAP_SUFFIX_SPECULAR_ALT = "specularmap";
	
	public const string MAP_SUFFIX_LIGHTING = "lighting";
	
	public const string MAP_SUFFIX_NORMAL = "normal";
	public const string MAP_SUFFIX_NORMAL_ALT = "normals";
	public const string MAP_SUFFIX_NORMAL_ALT2 = "normalsmap";
	
	public static bool overwriteFiles;
	
	public static Dictionary< string, bool > generatedMaterials = new Dictionary< string, bool >();
	
	// TODO gerar assets para todos os targets
	private static readonly BuildTarget[] buildTargets = { BuildTarget.WebPlayer, BuildTarget.Android, BuildTarget.iPhone };
	
	
	/// <summary>
	///  
	/// </summary>
    [ MenuItem( MENU_ITEM ) ]
    public static void Execute() {
		Directory.CreateDirectory( PATH_BUNDLES_MATERIALS );
		Directory.CreateDirectory( PATH_BUNDLES_MESHES );
		Directory.CreateDirectory( PATH_MATERIALS );
		
		overwriteFiles = false;
		generatedMaterials = new Dictionary< string, bool >();
		
        int createdMaterials = 0;
		List< string > createdCombinations = new List< string >();
		
        foreach ( Object o in Selection.GetFiltered( typeof( Object ), SelectionMode.DeepAssets ) ) {
			GameObject assetFbx = o as GameObject;
            if ( assetFbx == null || assetFbx.name.Contains( "@" ) )
				continue;
			
			string assetPath = AssetRoot( assetFbx );
			
			// evita que alguém execute o script num objeto que esteja fora da pasta correta
			if ( !assetPath.StartsWith( PATH_ASSETS ) )
				continue;
			
			string tag = null;
			bool markAsStatic = false;
			
			if ( assetPath.ToLower().Contains( terrainPrefix ) ) {
				tag = Common.Tags.TERRAIN;
				markAsStatic = true;
			}
			
			string assetRelativePathShort = assetPath.Replace( PATH_ASSETS, "" );
			string assetRelativePath = assetRelativePathShort + assetFbx.name;
			List< Renderer > defaultMaterialChildren = new List< Renderer >();
			
			Dictionary< string, Combination > variations = new Dictionary< string, List< CompositeTexture > >();

            // obtém todas as texturas do diretório do objeto
			List< Texture2D > textures = LoadTextures( assetPath );
			variations[ ROOT_PART_NAME ] = SaveMaterials( textures, OutputPath( assetRelativePath ), ROOT_PART_NAME );
			
			// verifica se há texturas comuns, e gera materiais para elas
			textures = LoadTextures( Directory.GetParent( assetPath ) + "_comum" );
			variations[ ROOT_PART_NAME ].AddRange( SaveMaterials( textures, OutputPath( assetRelativePath ), ROOT_PART_NAME ) );
			
			// grava as malhas em assetbundles
			Dictionary< string, string > meshesNames = new Dictionary< string, string >();
			foreach ( MeshFilter meshFilter in assetFbx.GetComponentsInChildren< MeshFilter >( true ) ) {
				Mesh mesh = meshFilter.sharedMesh;
				
				string meshPath = GetBundlePath( PATH_BUNDLES_MESHES, assetRelativePath + meshFilter.name + '_' + mesh.name );
				BuildPipeline.BuildAssetBundle( mesh, null, meshPath, BuildAssetBundleOptions.CollectDependencies );
				
				meshesNames.Add( meshFilter.gameObject == assetFbx ? ROOT_PART_NAME : meshFilter.gameObject.name, meshPath );
			}
			
            // cria materiais para os filhos
            foreach ( Renderer smr in assetFbx.GetComponentsInChildren< Renderer >( true ) ) {
				// verifica se o filho tem texturas específicas
				string childPath = Path.Combine( assetPath, smr.name.EndsWith( "_interior" ) ? "_interior" :  smr.name );
				
				if ( Directory.Exists( childPath ) ) {
					textures = LoadTextures( childPath );
					
					if ( !variations.ContainsKey( smr.name ) )
						variations[ smr.name ] = new List< CompositeTexture >();
					variations[ smr.name ].AddRange( SaveMaterials( textures, OutputPath( assetRelativePath, smr.name ), smr.name ) );
				} else {
					defaultMaterialChildren.Add( smr );
				}
            }
			
			List< Combination > combinations = Build( new List<Combination>(), 0, new List< Combination >( variations.Values ) );
			
			foreach ( List< CompositeTexture > list in variations.Values ) {
				createdMaterials += list.Count;
			}
			
			string pathLowerCase = assetRelativePath.ToLower();
			MatchCollection wallMatch = wallsRegex.Matches( pathLowerCase );
			MatchCollection floorMatch = floorRegex.Matches( pathLowerCase );
			MatchCollection marqueeMatch = marqueeRegex.Matches( pathLowerCase );
			
			// corrige a posição inicial do objeto importado, e normaliza a posição de seus componentes internos
			GameObject normalizedObject = ( GameObject ) GameObject.Instantiate( assetFbx );
			normalizedObject.name = assetFbx.name;
			normalizedObject.transform.position = new Vector3();
			Bounds boundingBox = ControllerUtils.GetBox( normalizedObject, true );
			foreach ( Transform t in normalizedObject.transform ) {
				if ( t.gameObject == normalizedObject )
					continue;
				
				t.localPosition = t.localPosition - boundingBox.center;
			}
			float width = boundingBox.size.x * normalizedObject.transform.localScale.x;
			
			// grava um prefab para cada combinação
			foreach ( Combination c in combinations ) {
				GameObject copy = ( GameObject ) GameObject.Instantiate( normalizedObject );
				copy.name = normalizedObject.name;
				
				// marcar como estático permite que a Unity crie batches de renderização
				if ( markAsStatic )
					copy.isStatic = true;
				
				copy.AddComponent( typeof( PrefabDescriptor ) );
				PrefabDescriptor descriptor = ( PrefabDescriptor ) copy.GetComponent( typeof( PrefabDescriptor ) );
				AssetAssemblyData data = descriptor.data;
				
				foreach ( string s in meshesNames.Keys )
					data.AddMesh( s, meshesNames[ s ] );
				
				// se for parede, piso ou coluna...
				if ( wallMatch.Count > 0 || floorMatch.Count > 0 || marqueeMatch.Count > 0 ) {
					// adiciona o snapper para facilitar a edição de casas, barracos e prédios
					ObjectSnapper snapper = copy.AddComponent< ObjectSnapper >();
					
					if ( wallMatch.Count > 0 )
						snapper.wallWidth = Mathf.Max( width, snapper.wallWidth ) - ObjectSnapper.DEFAULT_WALL_THICKNESS;
					
					snapper.SetCenter( -boundingBox.center );
					
					if ( wallMatch.Count > 0 ) {
						snapper.SetSnapMode( wallMatch[ 0 ].Value );
					} else if ( floorMatch.Count > 0 ) {
						snapper.SetSnapMode( floorMatch[ 0 ].Value );
						copy.AddComponent( typeof( PartSelector ) );
					} else if ( marqueeMatch.Count > 0 ) {
						snapper.SetSnapMode( marqueeMatch[ 0 ].Value );
						copy.AddComponent( typeof( MarqueePartSelector ) );
					}
					
					// se for uma parede, verifica se tem porta, para adicionar o portal correspondente
					if ( snapper.snapMode == ObjectSnapper.SnapMode.Wall ) {
						MatchCollection portalMatch = portalRegex.Matches( pathLowerCase );
						
						if ( portalMatch.Count > 0 ) {
							string cleanMatch = portalMatch[ 0 ].Value.Replace( "/", "" );
							// parede tem porta
							HollowDescriptor portalDescriptor = copy.AddComponent< HollowDescriptor >();
  							portalDescriptor.SetWallTypeName( cleanMatch );
						}
					}
					
					// adiciona caixa para detectar colisão contra o cenário
					// TODO: por enquanto, estamos assumindo que todas as caixas estão
					// orientadas nos eixos
					copy.AddComponent< BoxCollider >();
					BoxCollider boxCollider = copy.GetComponent< BoxCollider >();
					boxCollider.size = boundingBox.size;
				}
				
				try {
					foreach ( CompositeTexture t in c ) {
						if ( t.parentName.Equals( ROOT_PART_NAME ) ) {
							// atribui o material para todas as partes que não tem material específico
							foreach ( Renderer r in defaultMaterialChildren ) {
								GameObject part = ControllerUtils.FindChild( copy, r.name );
								if ( part == null ) {
									copy.renderer.sharedMaterial = t.GetMaterial();
									data.AddMaterial( ROOT_PART_NAME, t.GetMaterialBundlePath() );
								} else {
									part.renderer.sharedMaterial = t.GetMaterial();
									data.AddMaterial( r.name, t.GetMaterialBundlePath() );
								}
							}
						} else {
							GameObject part = ControllerUtils.FindChild( copy, t.parentName );
							if ( part != null )
								part.renderer.sharedMaterial = t.GetMaterial();
							else
								Debugger.LogError( "Part not found: " + t.parentName );
							
							data.AddMaterial( t.parentName, t.GetMaterialBundlePath() );
						}
					}
					
					// workaround para permitir a exportação da tag no prefab
					if ( tag != null ) {
						copy.AddComponent( typeof( SelfTagger ) );
						SelfTagger selfTagger = ( SelfTagger ) copy.GetComponent( typeof( SelfTagger ) );
						selfTagger.selfTag = tag;
					}
					
					descriptor.originalPrefabPath = Path.Combine( PATH_PREFABS, assetRelativePathShort + copy.name + GetCombinationName( c ) + ".prefab" );
					
					if ( CreatePrefab( copy, descriptor.originalPrefabPath ) )
						createdCombinations.Add( descriptor.originalPrefabPath );
				} catch ( Exception e ) {
					Debugger.LogError( e );
					Object.DestroyImmediate( copy );
				}
			}
			
			GameObject.DestroyImmediate( normalizedObject );
        }
		
		AssetDatabase.Refresh();
		
		// corrige eventuais conexões com prefabs perdidas na cena
		FixScenePrefabs();
		
		EditorUtility.DisplayDialog( WINDOW_TITLE, "Assets gerados:\n\nmateriais: " + createdMaterials + "\nprefabs: " + createdCombinations.Count, "Ok");
    }
	
	
	/// <summary>
	/// Valida o item de menu. 
	/// </summary>
	[ MenuItem ( MENU_ITEM, true ) ]
    public static bool ValidateGenerateAssets() {
		foreach ( Object o in Selection.GetFiltered( typeof( Object ), SelectionMode.DeepAssets ) ) {
			GameObject assetFbx = o as GameObject;
            if ( assetFbx == null || assetFbx.name.Contains( "@" ) )
				continue;
			
			string assetPath = AssetRoot( assetFbx );
			
			// se tiver pelo menos um objeto na pasta válida, o script pode ser executado
			if ( assetPath.StartsWith( PATH_ASSETS ) )
				return true;
		}
		
		return false;
    }
	
	
	[ MenuItem ( UTILS_TITLE + "/Rotate Object Left %q" ) ]
	public static void RotateLeft() {
		GameObject go = Selection.activeGameObject;
		Vector3 rot = go.transform.localEulerAngles;
		go.transform.localEulerAngles = new Vector3( rot.x, ( 360 + rot.y - 90 ) % 360, rot.z );
	}
	
	
	[ MenuItem ( UTILS_TITLE + "/Rotate Object Left %q", true ) ]
	public static bool ValidateRotateLeft() {
		return HasGameObjectSelected();
	}
		
	
	[ MenuItem ( UTILS_TITLE + "/Rotate Object Right %w" ) ]
	public static void RotateRight() {
		GameObject go = Selection.activeGameObject;
		Vector3 rot = go.transform.localEulerAngles;
		go.transform.localEulerAngles = new Vector3( rot.x, rot.y + 90, rot.z );
	}
	
	
	[ MenuItem ( UTILS_TITLE + "/Rotate Object Right %w", true ) ]
	public static bool ValidateRotateRight() {
		return HasGameObjectSelected();
	}
	
	
	[ MenuItem ( UTILS_TITLE + "/Land Object %e" ) ]
	public static void LandObjectMenu() {
		LandObject( Selection.activeGameObject );
	}
	
	
	public static void LandObject( GameObject go ) {
		RaycastHit hit;
		Bounds b = ControllerUtils.GetBox( go, false );
		Vector3 origin = new Vector3( b.center.x, b.min.y, b.center.z );
		Ray ray = new Ray( origin, Vector3.down );
		if ( !Physics.Raycast( ray, out hit, 1000 ) ) {
			return;
		}
		
		Vector3 diff = b.center - go.transform.position;
		
		go.transform.position = new Vector3( go.transform.position.x, hit.point.y - diff.y + ( b.size.y * 0.5f ), go.transform.position.z );
	}
	
	
	[ MenuItem ( UTILS_TITLE + "/Land Object %e", true ) ]
	public static bool ValidateLandObject() {
		return HasGameObjectSelected();
	}
	

	private static bool HasGameObjectSelected() {
		GameObject go = Selection.activeGameObject;
		return go != null && go.transform != null;
	}
	
	
	[ MenuItem ( UTILS_TITLE + "/Show bounding box" ) ]
	public static void AddBoundingBox() {
		GameObject go = Selection.activeGameObject;
		Bounds b = ControllerUtils.GetBox( go, false );
		GameObject box = GameObject.CreatePrimitive( PrimitiveType.Cube );
		box.name = go.name + " bounding box";
		box.transform.position = b.center;
		box.transform.localScale = b.size;
	}
	
	
	[ MenuItem ( UTILS_TITLE + "/Show bounding box", true ) ]
	public static bool ValidateShowBoundingBox() {
		return HasGameObjectSelected();
	}	
	
	
	[ MenuItem ( FIXES_TITLE + "/Fix object or prefab" ) ]
	public static void FixPrefab() {
		foreach ( Object o in Selection.gameObjects ) {
			GameObject go = o as GameObject;
			if ( go == null )
				continue;
			
			FixPrefab( go );
		}
	}
	
	
	[ MenuItem ( FIXES_TITLE + "/Fix All Scene Objects" ) ]
	public static void FixScenePrefabs() {
		try {
			Object[] all = GameObject.FindObjectsOfType( typeof( GameObject ) );
			int count = 0;
			float total = all.Length;
			
			foreach ( Object o in all ) {
				++count;
				EditorUtility.DisplayProgressBar( "Fix All Scene Objects", "Fixing " + o.name + " prefabs connections", count / total );
				
				GameObject go = o as GameObject;
				if ( go.transform.parent != null )
					continue;
				
				FixPrefab( go );
			}
		} finally {
			EditorUtility.ClearProgressBar();
		}
	}
	
	
	public static void FixPrefab( GameObject original ) {
		PrefabType prefabType = PrefabUtility.GetPrefabType( original );
		
		bool isPrefab = false;
		
		GameObject gameObject;
		switch ( prefabType ) {
			case PrefabType.Prefab:
			case PrefabType.ModelPrefab:
				gameObject = ( GameObject ) PrefabUtility.InstantiatePrefab( original );
				gameObject.name = original.name;
				isPrefab = true;
			break;
			
			case PrefabType.PrefabInstance:
			case PrefabType.ModelPrefabInstance:
				Object prefab = PrefabUtility.GetPrefabParent( original );
			
				gameObject = original;
				original = ( GameObject ) prefab;
			
				foreach ( Transform t in gameObject.transform ) {
					PrefabUtility.DisconnectPrefabInstance( t.gameObject );
				}
			break;
			
			default:
				gameObject = original;
			
				foreach ( Transform t in gameObject.transform ) {
					PrefabUtility.DisconnectPrefabInstance( t.gameObject );
				}
			break;
		}
		
		foreach ( Transform t in gameObject.transform ) {
			GameObject child = t.gameObject;
			if ( child == gameObject )
				continue;
			
			FixPrefab( child );
		}
		
		PrefabDescriptor descriptor = gameObject.GetComponent< PrefabDescriptor >();
		if ( descriptor == null )
			return;
		
		string rootPath = "file://" + Application.dataPath + "/../";
		
		try {
			AssetAssemblyData data = descriptor.data;
			GameObject child = descriptor.gameObject;
			
			GameObject originalChild = ( GameObject ) Resources.LoadAssetAtPath( descriptor.originalPrefabPath, typeof( GameObject ) );
		
			if ( originalChild == null ) {
				foreach ( PartMap meshMap in data.meshes ) {
					try {
						GameObject meshPart = GameObjectBuilder.GetPart( child, meshMap.partName );
						MeshFilter meshFilter = ( MeshFilter ) meshPart.GetComponent( typeof( MeshFilter ) );
						if ( meshFilter == null ) {
//							Debugger.LogWarning( "part not found: " + meshMap.partName );
							continue;
						}
						
						WWW www = new WWW( rootPath + meshMap.names[ 0 ] );
						for ( int i = 0; i < 20; ++i ) {
							if ( www.isDone ) {
								break;
							}
						}
						
						AssetBundle aaa = www.assetBundle;
						if ( aaa == null )
							continue;
						
						aaa.LoadAll();
						Mesh mesh2 = Object.Instantiate( aaa.mainAsset ) as Mesh;
						meshFilter.mesh = mesh2;
//							aaa.Unload( false );
					} catch ( Exception e ) {
//						Debugger.LogError( "part not found: " + meshMap.partName + "\n" + e );
					}
				}
	
				// associa os materiais às partes corretas
				foreach ( PartMap materialMap in data.materials ) {
					List< Material > materials = new List< Material >();
					foreach ( string materialSource in materialMap.names ) {
						try {
							WWW www = new WWW( rootPath + materialMap.names[ 0 ] );
							for ( int i = 0; i < 20; ++i ) {
								if ( www.isDone ) {
									break;
								}
							}
							
							AssetBundle aaa = www.assetBundle;
							if ( aaa == null )
								continue;
							
							aaa.LoadAll();
							Material material2 = Object.Instantiate( aaa.mainAsset ) as Material;
							materials.Add( material2 );
//								aaa.Unload( false );
						} catch ( Exception e ) {
							Debugger.LogError( e );
						}
					}
					
					GameObject part = GameObjectBuilder.GetPart( child, materialMap.partName );
					if ( part != null && part.renderer != null ) {
						part.renderer.sharedMaterials = materials.ToArray();
					}
				}					
			} else {
				foreach ( Renderer r in originalChild.GetComponentsInChildren< Renderer >( true ) ) {
					Renderer r2 = ControllerUtils.FindComponentByName< Renderer >( child, r.name );
					if ( r2 != null )
						r2.sharedMaterials = r.sharedMaterials;
//					else
//						Debugger.LogError( "Renderer not found in " + child.name + ": " + r.name );
				}
				
				foreach ( MeshFilter f in originalChild.GetComponentsInChildren< MeshFilter >( true ) ) {
					MeshFilter f2 = ControllerUtils.FindComponentByName< MeshFilter >( child, f.name );
					if ( f2 != null )
						f2.sharedMesh = f.sharedMesh;
//					else
//						Debugger.LogError( "MeshFilter not found in " + child.name + ": " + f.name );
				}
			}
			
			if ( isPrefab )
				PrefabUtility.ReplacePrefab( gameObject, original );
		} catch ( Exception e ) {
			Debugger.LogError( e );
		} finally {
			if ( isPrefab )
				GameObject.DestroyImmediate( gameObject );
				
			AssetDatabase.Refresh();
		}
	}	
	
	
	[ MenuItem ( "Asset Manager/Combine meshes" ) ]
	public static void CombineMeshesMenuItem() {
//		object[] objectList = Selection.GetFiltered( typeof( Object ), SelectionMode.DeepAssets );
		GameObject[] objects = Selection.gameObjects;
		List< GameObject > gameObjectList = new List< GameObject >();
		for ( int i = 0; i < objects.Length; ++i ) {
			GameObject parent = objects[ i ];
			foreach ( Transform t in parent.transform )
				gameObjectList.Add( t.gameObject );
		}
		
		MeshCombiner.CombineMeshes( gameObjectList, ControllerUtils.GetHolder( "<combined>" ) );
	}
	
	
	[ MenuItem ( "Asset Manager/Static batch" ) ]
	public static void StaticBatch() {
		foreach ( GameObject go in Selection.gameObjects ) {
//			List< GameObject > bla = new List<GameObject>();
			foreach ( Transform t in go.transform ) {
				if ( t.gameObject != go )
//					bla.Add( t.gameObject );
				StaticBatchingUtility.Combine( t.gameObject );
			}
//			StaticBatchingUtility.Combine( bla.ToArray(), ControllerUtils.GetHolder( "___" + go.name ) );
			StaticBatchingUtility.Combine( go );
			go.isStatic = true;
		}
//		GameObject root = ControllerUtils.GetHolder( "<root>" );
//		if ( root != null ) {
//			StaticBatchingUtility.Combine( root );
//		}
	}
	
	
	/// <summary>
	/// Gera todas as combinações de materiais para as partes que compõem um objeto. O algoritmo é recursivo, indo até a
	/// última lista de variações de uma parte recebida, e gera as novas combinações (acumulando-as em list) conforme
	/// as chamadas vão sendo retornadas.
	/// </summary>
	private static List< Combination > Build( List< Combination > list, int index, List< Combination > variations ) {
		List< Combination > ret = new List< Combination >();
		if ( index >= variations.Count - 1 ) {
			foreach ( CompositeTexture c in variations[ index ] ) {
				Combination combo = new Combination();
				combo.Add( c );
				ret.Add( combo );
			}
			return ret;
		}
		
		List< Combination > childrenCombinations = Build( list, index + 1, variations );
		
		foreach ( CompositeTexture ct in variations[ index ] ) {
			foreach ( Combination combination in childrenCombinations ) {
				Combination c2 = new Combination();
				c2.Add( ct );
				c2.AddRange( combination );
				ret.Add( c2 );
			}
		}
		// evita problemas caso o objeto SÓ tenha partes personalizadas
		if ( index != 0 )
			list.Clear();
		list.AddRange( ret );
		
		return list;
	}
	

	/// <summary>
	/// Método auxiliar para efetivamente realizar a gravação do prefab.
	/// </summary>
	private static bool CreatePrefab( GameObject obj, string path ) {
		Directory.CreateDirectory( Path.GetDirectoryName( path ) );
		
		// se o arquivo existir, pede a ação do usuário
		if ( CanSaveFile( path ) ) {
			PrefabUtility.CreatePrefab( path, obj, ReplacePrefabOptions.ReplaceNameBased );
			GameObject.DestroyImmediate( obj );
			
			return true;
		}
		
		return false;
    }
	
	
	/// <summary>
	/// Carrega todas as texturas de um determinado diretório.
	/// </summary>
	private static List< Texture2D > LoadTextures( string path ) {
		List< Texture2D > textures = new List< Texture2D >();
		
		if ( Directory.Exists( path ) ) {
			string[] files = Directory.GetFiles( path );
			for ( int i = 0; i < files.Length; ++i ) {
				if ( validExtensions.IsMatch( files[ i ] ) )
					textures.Add( ( Texture2D ) AssetDatabase.LoadAssetAtPath( files[ i ], typeof( Texture2D ) ) );
			}
		}
		
		return textures;
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	private static List< CompositeTexture > SaveMaterials( List< Texture2D > textures, string path, string parentName ) {
		Dictionary< string, CompositeTexture > compositeTextures = new Dictionary< string, CompositeTexture >();
		List< CompositeTexture > variations = new List< CompositeTexture >();
		
		// compõe todas as texturas (difusa + normal + altura + ...)
		string materialsPath = path;
		foreach ( Texture2D t in textures ) {
			string cleanName;
			int index = t.name.LastIndexOf( '_' );
			
//			if ( index < 0 || CompositeTexture.IsDiffuse( t.name ) )
			if ( index < 0 )
				cleanName = t.name;
			else
				cleanName = t.name.Substring( 0, index );
			
			if ( !compositeTextures.ContainsKey( cleanName ) )
				compositeTextures.Add( cleanName, new CompositeTexture( materialsPath, cleanName, parentName ) );
			
			compositeTextures[ cleanName ].Add( t );
		}
		
		foreach ( CompositeTexture ct in compositeTextures.Values ) {
			ct.Save();
			// adiciona à lista de variações, mesmo que não tenha sido salvo (o arquivo já poderia existir)
			variations.Add( ct );
		}
		
		return variations;
	}
	
	
	// Returns the path to the directory that holds the specified FBX.
    private static string AssetRoot( GameObject asset ) {
        string root = AssetDatabase.GetAssetPath( asset );
        return root.Substring( 0, root.LastIndexOf( '/' ) + 1 );
    }

	
	private static string OutputPath( string assetRelativePath ) {
		return PATH_MATERIALS + assetRelativePath;
	}
	
	
	private static string OutputPath( string assetRelativePath, string assetName ) {
		return PATH_MATERIALS + assetRelativePath + assetName + "/";
	}
	
	
	private static string GetCombinationName( Combination c ) {
		StringBuilder sb = new StringBuilder();
		
		foreach ( CompositeTexture t in c ) {
			sb.Append( '_' );
			sb.Append( t.name );
		}
		
		return sb.ToString();
	}
		
		
	public static bool CanSaveFile( string path ) {
		if ( !AssetGenerator.overwriteFiles && File.Exists( path ) ) {
			switch ( EditorUtility.DisplayDialogComplex( AssetGenerator.WINDOW_TITLE, "Foi encontrado um arquivo em \"" + path + "\".", "Substituir todos", "Substituir este", "Manter original" ) ) {
				case 0:
					AssetGenerator.overwriteFiles = true;
					return true;
					
				case 1:
					return true;
					
				case 2:
				default:
					return false;
			}
		}
			
		return true;
	}
	
	
	public static string GetBundlePath( string path, string name ) {
		return path + name.Replace( AssetGenerator.PATH_PREFABS, "" ).Replace( '/', '_' ) + ".assetbundle";
	}
	
	
}
#endif
