using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using UnityEditor;
using UnityEngine;

using GameCommunication;
using Utils;

using Attribute = GameCommunication.Attribute;
using Object = UnityEngine.Object;


[ CustomEditor( typeof( GenericEditor ) ) ]
public class AttributeEditor : Editor {
	
	public static readonly string DEFAULT_PATH = GenericEditor.DEFAULT_PATH;
	
	private static Dictionary< GenericEditor, string > lastPath = new Dictionary< GenericEditor, string >();

    private object data;
	
	private string title;
	
	private List< AttributeInspector > inspectors = new List< AttributeInspector >();
	
	private static Dictionary< GenericEditor, object > instances = new Dictionary< GenericEditor, object >();
	
	private bool firstRun = true;
	
	private bool changed;
	
	
	public object Data {
		get { return data; }
		set {
			data = value;
			OnEnable();
		}
	}
	
    
    public virtual void OnEnable() {
		if ( !Directory.Exists( DEFAULT_PATH ) )
			Directory.CreateDirectory( DEFAULT_PATH );
		
		GenericEditor editor = ( GenericEditor ) target;
		
		if ( !instances.ContainsKey( editor ) ) {
			if ( editor.DefaultPath != null && editor.DefaultPath.Length > 0 ) {
				lastPath[ editor ] = editor.DefaultPath;
				
				Load( editor.DefaultPath, editor.GetData().GetType() );
			}
			
			if ( data == null )
				data = editor.GetData();
		} else {
			data = instances[ editor ];
		}
		
		Init();
		
		instances[ editor ] = data;
    }
	
	
	private void Init() {
		GenericEditor editor = ( GenericEditor ) target;
		title = editor.GetTitle();
		changed = false;
		
		inspectors.Clear();
		
		if ( data != null ) {
			// o objeto da Unity é passado para que alterações em qualquer nível da hierarquia reflitam no setter mais amplo,
			foreach ( AttributeInspector g in GetInspectorGroups( editor ) ) {
				inspectors.Add( g );
			}
		}
	}
	
	
	private string GetLastPath() {
		GenericEditor e = ( GenericEditor ) target;
		if ( !lastPath.ContainsKey( e ) ) {
			lastPath[ e ] = ( e.DefaultPath.Length <= 0 ? DEFAULT_PATH : e.DefaultPath );
		}
		
		return lastPath[ e ];
	}
	
	
	private IEnumerable< AttributeInspector > GetInspectorGroups( object o ) {
		List< PropertyInfo > infos;
		
		if ( o is GenericEditor ) 
			infos = new List< PropertyInfo >( o.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly ) );
		else if ( o != null )
			infos = new List< PropertyInfo >( o.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance ) );
		else
			yield break;
		
		infos.Sort( new PropertyInfoComparer() );
			
		foreach ( PropertyInfo info in infos ) {
			MethodInfo getter = info.GetGetMethod();
			
			object got = getter.Invoke( o, null );
			Attribute a = got as Attribute;
			if ( a == null ) {
				// se for um tipo básico, adiciona o inspetor
				ExposedType type = ExposedType.Generic;
				if ( PropertyField.GetPropertyType( info, out type ) ) {
					yield return new AttributeInspector( info, o, got, new PropertyField( o, info, type ) );
				} else {
					// se for um tipo desconhecido (normalmente uma classe), adiciona seus filhos
					AttributeInspector g = new AttributeInspector( info, o, got );
					g.Title = info.Name;
					
					foreach ( AttributeInspector i in GetInspectorGroups( got ) )
						g.AddChild( i );
					
					yield return g;
				}
			} else {
				AttributeInspector inspector = new AttributeInspector( info, o, got );
				yield return inspector;
			}
		}
	}
	
	
    public override void OnInspectorGUI () {
        if ( data == null )
            return;
        
        this.DrawDefaultInspector();
		
		foreach ( AttributeInspector g in inspectors ) {
			changed |= g.Render( firstRun );
		}
		
		if ( GUILayout.Button( "Load" ) ) {
			string path = EditorUtility.OpenFilePanel( title + " load", GetLastPath(), "options" );
			if ( path.Length > 0 ) {
				Load( path, data.GetType() );
			}
		}
		
		if ( GUILayout.Button( "Save" ) ) {
			string path = EditorUtility.SaveFilePanel( title + " save", GetLastPath(), Util.GetCleanFilename( title ), "options" );
			if ( path.Length > 0 && Util.SerializeToXMLFile( data, path ) ) {
				changed = false;
				lastPath[ ( GenericEditor ) target ] = path;
			}
		}
		
		firstRun = false;
    }
	
	
	protected bool Load( string path, Type dataType ) {
		Editable e = ( Editable ) Util.DeserializeFromXMLFile( path, dataType );
		if ( e != null ) {
			data = e;
			lastPath[ ( GenericEditor ) target ] = path;
			Init();
			
			return true;
		} else {
			Debugger.LogWarning( "Error loading options from " + path );
			return false;
		}	
	}
	
	
	internal class AttributeInspector {
		
		private string title;
		
		public readonly List< AttributeInspector > children = new List< AttributeInspector >();
		
		public List< PropertyField > fields;
		
		public bool show;
		
		private bool composite = true;
		
		public MethodInfo setterMethod;
		
		public object setterParent;
		
		public object setterParam;
		
		private GUIContent content;
		
		
		public AttributeInspector( PropertyInfo info, object parent, object o, PropertyField field ) {
			Title = info.Name;
			
			fields = new List< PropertyField >();
			fields.Add( field );
			show = true;
			composite = false;
			
			setterParent = parent;
			setterParam = o;
			setterMethod = info.GetSetMethod();
			
			content = new GUIContent( Title );
		}		
		
		
		public AttributeInspector( PropertyInfo info, object parent, object o ) {
			Title = info.Name;
			this.fields = new List< PropertyField >( ExposeProperties.GetProperties( o ) );
			fields.Sort( new PropertyFieldComparer() );
			
			object[] toolTipAttributes = info.GetCustomAttributes( typeof( ToolTipAttribute ), false );
			string tooltip = "";
			if ( toolTipAttributes.Length > 0 ) {
				ToolTipAttribute a = toolTipAttributes[ 0 ] as ToolTipAttribute;
				if ( a != null )
					tooltip = a.ToolTipText;
			}
			content = new GUIContent( Title, tooltip );
			
			setterParent = parent;
			setterParam = o;
			setterMethod = info.GetSetMethod();
		}
		
		
		public string Title {
			get { return title; }
			set { title = ObjectNames.NicifyVariableName( value ); }
		}
		
		
		public void AddChild( AttributeInspector i ) {
			// verifica se o AttributeInspector a ser adicionado é na verdade um campo já existente
			if ( i.fields != null && i.fields.Count == 1 ) {
				PropertyField field = i.fields[ 0 ];
				
				foreach ( PropertyField f in fields ) {
					if ( f.Equals( field ) )
						return;
				}
			}
			
			show = true;
			children.Add( i );
		}
		
		
		public bool Render( bool firstRun ) {
			if ( composite ) {
				content.text = Title;
				show = EditorGUILayout.Foldout( show, content );
			}
			
			bool changed = false;
			
			if ( show ) {
				if ( fields != null )
					changed = ExposeProperties.Expose( fields, firstRun );
				
				if ( children.Count > 0 ) {
					EditorGUILayout.Space();
					
					foreach ( AttributeInspector a in children ) {
						changed |= a.Render( firstRun );
					}
				}
			}
			
			if ( changed && setterMethod != null )
				setterMethod.Invoke( setterParent, new object[] { setterParam } );
			
			if ( children.Count > 0 ) {
				EditorGUILayout.Space();
				EditorGUILayout.Space();
			}
			
			return changed;
		}
		
	}
	
	
	internal class PropertyInfoComparer : IComparer< PropertyInfo > {
		public int Compare ( PropertyInfo p1, PropertyInfo p2 ) {
			return p1.Name.ToLower().CompareTo( p2.Name.ToLower() );
		}
		
	}
	
			
	internal class PropertyFieldComparer : IComparer< PropertyField > {
		public int Compare ( PropertyField p1, PropertyField p2 ) {
			return p1.Name.ToLower().CompareTo( p2.Name.ToLower() );
		}
		
	}
}


