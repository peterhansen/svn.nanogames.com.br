using System;

using Squid;

using GameController;


public class MenuOption {
	
	public IconType icon;
	
	public string stringCode;
	
	public object tag;
	
	public MouseClickEventHandler callback;
	
	public bool available;
	
	public MenuOption( IconType icon, MouseClickEventHandler callback ) : this( icon, callback, null, true ) {
	}
	
	public MenuOption( IconType icon, MouseClickEventHandler callback, object tag, bool available ) {
		this.icon = icon;
		this.tag = tag;
		this.callback = callback;
		this.available = available;
		
		// TODO
		this.stringCode = Enum.GetName( typeof( IconType ), icon );
	}
}
