using UnityEngine;
using System.Collections;

public class SelectedGUIController : MonoBehaviour {
	
	public float apIndicator = 0f;
	public float lifeIndicator = 0f;
	
	private const float maxIndicator = 100f;
	private Vector3 childScale;
	
	// Use this for initialization
	void Start () {
		foreach(Transform child in transform){
			childScale = child.localScale;
		}
		
		Texture tex = gameObject.renderer.material.mainTexture;
		
	}
	
	// Update is called once per frame
	void Update () {
		float realAp = apIndicator * 0.85f;
		Mathf.Clamp(realAp, 0f, 85f);
		renderer.material.SetFloat("_Cutoff", Mathf.InverseLerp(0, 100f, Mathf.CeilToInt( maxIndicator - realAp ) ) );
		foreach(Transform child in transform){
			child.localScale = new Vector3( childScale.x * ( lifeIndicator / maxIndicator ) ,
											childScale.y , 
											childScale.z * ( lifeIndicator / maxIndicator ));
		}
	}
}
