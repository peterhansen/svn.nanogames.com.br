using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;

public class TurnInfoControl : Frame {
	
	protected Frame toolbar;
	protected bool collapsed = true;
	
	protected Button expandCollapseButton;
	protected Button changeTurnButton;
	protected Button characterListButton;
	protected Button cameraControlButton;
	protected Button rotateLeftButton;
	protected Button rotateRightButton;
	
	protected Button decreaseVisibleLayerButton;
	protected Button increaseVisibleLayerButton;
	
	protected Button nextCharacterButton;
	
	protected Label frameLabel;
	
	protected Frame cameraControlWindow;
	
	protected OrientedImageControl rightUpperCornerBorder;
	protected OrientedImageControl rightDownCornerBorder;
	protected OrientedImageControl separetedUpperBorder;
	protected OrientedImageControl separetedDownBorder;
	
		
	
	protected override void Initialize() {
		base.Initialize();

		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}
	
		Desktop desktop = guiManager.Desktop;
		
		Position = new Point( 0, 0 );
		Size = new Point( 1000, 1000 );
		Parent = desktop;
		desktop.Controls.Add( this );
		
		toolbar = new Frame();
		toolbar.Size = new Point( 247, 66 );
		toolbar.Position = new Point( 0, 0 );
		toolbar.Scissor = true;
		toolbar.Style = "camera_toolbar";
		toolbar.Parent = this;
		
		
//		expandCollapseButton = new Button();
//		expandCollapseButton.Size = new Point( 48, 67 );
//		expandCollapseButton.Position = new Point( 200, 0 );
//		expandCollapseButton.Parent = toolbar;
////		expandCollapseButton.Opacity = 0.0f;
//		expandCollapseButton.OnMouseClick += new MouseClickEventHandler( ExpandCollapseCameraToolbar );
		
		decreaseVisibleLayerButton = new Button();
		decreaseVisibleLayerButton.Size = new Point( 150, 20 );
		decreaseVisibleLayerButton.Position = new Point( 10, 30 );
		decreaseVisibleLayerButton.Parent = this;
		decreaseVisibleLayerButton.Text =  L10n.Get("ACTION_LAYER_DOWN");
		decreaseVisibleLayerButton.OnMouseClick += new MouseClickEventHandler( ReduceVisibleLayer );

		increaseVisibleLayerButton = new Button();
		increaseVisibleLayerButton.Size = new Point( 150, 20 );
		increaseVisibleLayerButton.Position = new Point( 10, 60 );
		increaseVisibleLayerButton.Parent = this;
		increaseVisibleLayerButton.Text = L10n.Get("ACTION_LAYER_UP");
		increaseVisibleLayerButton.OnMouseClick += new MouseClickEventHandler( IncreaseVisibleLayer );
		
		cameraControlButton = new Button();
		cameraControlButton.Size = new Point( 42, 42 );
		cameraControlButton.Position = new Point( 20, 90 );
		cameraControlButton.Parent = this;
		//cameraControlButton.Text = L10n.Get("ACTION_CAMERA_CONTROL");
		cameraControlButton.OnMouseClick += new MouseClickEventHandler( CameraControl );
		cameraControlButton.Style = "cameracontrolbutton";
		
		cameraControlWindow = new SectionFrame();
        cameraControlWindow.Size = new Squid.Point(160, 100);
        cameraControlWindow.Position = new Squid.Point(170, 90);
        cameraControlWindow.Style = "frame";
        cameraControlWindow.Parent = this;
		
		rightUpperCornerBorder = new OrientedImageControl();
		rightUpperCornerBorder.Texture = "GUI/CameraControl/lightBorderRightSmall.png";
		rightUpperCornerBorder.Size = new Squid.Point(61, 64);
        rightUpperCornerBorder.Position = new Squid.Point(108, -10);
        rightUpperCornerBorder.Parent = cameraControlWindow;
		
		separetedDownBorder = new OrientedImageControl();
		separetedDownBorder.Texture = "GUI/CameraControl/LightBorderLeft_separated_hor.png";
		separetedDownBorder.Size = new Squid.Point(128, 16);
        separetedDownBorder.Position = new Squid.Point(-25, 95);
        separetedDownBorder.Parent = cameraControlWindow;
		
		rightDownCornerBorder = new OrientedImageControl();
		rightDownCornerBorder.Texture = "GUI/CameraControl/lightBorderRightSmallFlipped.png";
		rightDownCornerBorder.Size = new Squid.Point(61, 64);
        rightDownCornerBorder.Position = new Squid.Point(108, 45);
        rightDownCornerBorder.Parent = cameraControlWindow;
		
		separetedUpperBorder = new OrientedImageControl();
		separetedUpperBorder.Texture = "GUI/CameraControl/LightBorderLeft_separated_hor_Flipped.png";
		separetedUpperBorder.Size = new Squid.Point(128, 16);
        separetedUpperBorder.Position = new Squid.Point(-25, -10);
        separetedUpperBorder.Parent = cameraControlWindow;
		
		frameLabel = new Label();
        frameLabel.Text = L10n.Get("ACTION_CAMERA_CONTROL").ToUpper();
        frameLabel.Size = new Squid.Point(160, 40);
        frameLabel.Position = new Squid.Point(0, 0);
        frameLabel.Parent = cameraControlWindow;
		frameLabel.TextAlign = Alignment.MiddleCenter;
		frameLabel.Style = "FontTitle";
		
		rotateLeftButton = new Button();
		rotateLeftButton.Size = new Point( 50, 40 );
		rotateLeftButton.Position = new Point( 20, 50 );
		rotateLeftButton.Parent = cameraControlWindow;
		//rotateLeftButton.Text = "R";
		rotateLeftButton.OnMouseClick += new MouseClickEventHandler( RotateLeft );
		rotateLeftButton.Style = "cameracontrolleftbutton";
		
		rotateRightButton = new Button();
		rotateRightButton.Size = new Point( 50, 40 );
		rotateRightButton.Position = new Point( 160 - 20 - 50, 50);
		rotateRightButton.Parent = cameraControlWindow;
		//rotateRightButton.Text = "L" ; 
		rotateRightButton.OnMouseClick += new MouseClickEventHandler( RotateRight );
		rotateRightButton.Style = "cameracontrolrightbutton";
		
		cameraControlWindow.Visible = false;
		
		changeTurnButton = new Button();
		changeTurnButton.Size = new Point( 150, 20 );
		changeTurnButton.Position = new Point( 10, 150 );
		changeTurnButton.Parent = this;
		changeTurnButton.Text = L10n.Get("ACTION_CHANGE_TURN");
		changeTurnButton.OnMouseClick += new MouseClickEventHandler( ChangeTurn );
		
		characterListButton = new Button();
		characterListButton.Size = new Point( 150, 20 );
		characterListButton.Position = new Point( 10, 180 );
		characterListButton.Parent = this;
		characterListButton.Text = L10n.Get("ACTION_CHARACTER_LIST");
		characterListButton.OnMouseClick += new MouseClickEventHandler( ListCharacters );

		nextCharacterButton = new Button();
		nextCharacterButton.Size = new Point( 150, 20 );
		nextCharacterButton.Position = new Point( 10, 250 );
		nextCharacterButton.Parent = this;
		nextCharacterButton.Text = L10n.Get("ACTION_NEXT_SOLDIER");
		nextCharacterButton.OnMouseClick += new MouseClickEventHandler( NextSoldier );
	
	}
	
	
	private void ExpandCollapseCameraToolbar( Control sender ) {
		collapsed = !collapsed;
		toolbar.Animation.Stop();
		toolbar.Animation.Position( new Point( collapsed? -199 : 0, 0 ), 500 );
	}
	
	private void CameraControl( Control sender ) {
		cameraControlWindow.Visible = !cameraControlWindow.Visible;
	}
	
	
	private void RotateLeft( Control sender ) {
		IsoCameraScript isoCamera = IsoCameraScript.GetInstance();
		isoCamera.RotateLeft();
	}
		
	
	private void RotateRight( Control sender ) {
		IsoCameraScript isoCamera = IsoCameraScript.GetInstance();
		isoCamera.RotateRight();
	}
	
	
	private void ChangeTurn( Control sender ) {
		GameManager.GetInstance().SendRequest( new EndParticipationRequest( GameManager.GetInstance().GetLocalPlayer() ) );
	}
	
	
	private void ListCharacters( Control sender ) {		
		CharactersListInteractionController clic = new CharactersListInteractionController();
		GUIManager.SetInteractionController( clic );
	}
	
	
	private void ReduceVisibleLayer( Control sender ) {
		BuildingInfo.ReduceVisibleLayer();
		RefreshLayerButtons();
	}
	
	
	private void IncreaseVisibleLayer( Control sender ) {
		BuildingInfo.IncreaseVisibleLayer();
		RefreshLayerButtons();
	}
	
	
	private void RefreshLayerButtons() {
		decreaseVisibleLayerButton.Enabled = BuildingInfo.CanDecreaseLayer();
		increaseVisibleLayerButton.Enabled = BuildingInfo.CanIncreaseLayer();
	}
	
	
	/// <summary>
	/// Cicla a câmera entre os soldados do jogador. Caso não haja um último soldado selecionado, o primeiro soldado da
	/// lista é selecionado.
	/// </summary>
	private void NextSoldier( Control sender ) {
		GameManager gameManager = GameManager.GetInstance();
		WorldManager worldManager = WorldManager.GetInstance();
		
		List< GameObject > validCharacters = worldManager.GetFactionCharacters( gameManager.GetTurnFaction() );
		
		int index = -1;
		for ( int i = 0; i < validCharacters.Count; ++i ) {
			if ( gameManager.LastSelectedSoldier == validCharacters[ i ] ) {
				index = i;
				break;
			}
		}
		GameObject soldier = validCharacters[ ( index + 1 ) % validCharacters.Count ];
		gameManager.LastSelectedSoldier = soldier;
		
		IsoCameraScript.GetInstance().MoveTo( soldier.transform.position );
	}
	
}
