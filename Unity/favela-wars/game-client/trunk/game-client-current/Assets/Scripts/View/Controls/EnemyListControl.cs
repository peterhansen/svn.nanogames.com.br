using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;

public class EnemyListControl : Frame {
	
	protected List< int > enemiesWorldIDs = new List< int >();
	
	protected Button nextEnemyButton;
	
	protected int lastEnemySelectedID;
	
	
	public void SetEnemiesList( List< int > enemiesWorldIDs ) {
		this.enemiesWorldIDs = enemiesWorldIDs;
		nextEnemyButton.Visible = enemiesWorldIDs.Count > 0;
	}
	
	
	protected override void Initialize() {
		base.Initialize();

		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "EnemyListControl Error: No GUIManager found!" );
			return;
		}
	
		Desktop desktop = guiManager.Desktop;
		
		Position = new Point( 0, 0 );
		Size = new Point( 1000, 1000 );
		Parent = desktop;
		desktop.Controls.Add( this );
		
		nextEnemyButton = new Button();
		nextEnemyButton.Size = new Point( 150, 20 );
		nextEnemyButton.Position = new Point( 10, 200 );
		nextEnemyButton.Parent = this;
		nextEnemyButton.Text = "Snake!!!!!";
		nextEnemyButton.OnMouseClick += new MouseClickEventHandler( OnNextEnemyClick );
	}
	
	
	protected void OnNextEnemyClick( Control sender ) {
		int index = -1;
		for ( int i = 0; i < enemiesWorldIDs.Count; ++i ) {
			if ( lastEnemySelectedID == enemiesWorldIDs[ i ] ) {
				index = i;
				break;
			}
		}
		int id = (enemiesWorldIDs.Count != 0)? enemiesWorldIDs[ ( index + 1 ) % enemiesWorldIDs.Count ] : 0;
		GameObject enemy = WorldManager.GetInstance().GetGameObject( id );
		lastEnemySelectedID = id;
		
		IsoCameraScript.GetInstance().MoveTo( enemy.transform.position );
	}
	
}
