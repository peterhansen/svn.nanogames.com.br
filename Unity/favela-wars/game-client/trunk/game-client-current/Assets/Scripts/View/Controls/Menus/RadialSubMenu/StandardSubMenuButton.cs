using System;

using Squid;

using GameController;


public class StandardSubMenuButton : RadialSubMenuButton {
	
	protected Label text;
	
	public StandardSubMenuButton( MenuOption option ) : base( option ) {
	}
	
	public override void AddButtonContents( bool available ) {
		text = new Label();		
		text.AutoSize = AutoSize.Horizontal;
		text.Size = new Point( text.Size.x - BUTTON_TEXT_LEFT_MARGIN, Size.y - 2 * BUTTON_PADDING );
		text.TextAlign = Alignment.MiddleLeft;
		text.Position = new Point( BUTTON_TEXT_LEFT_MARGIN, 0 );
		text.AllowFocus = false;
		text.NoEvents = true;
		
		text.Text = L10n.Get( menuOption.stringCode );
		if( !available )
			
			text.Text = "[color=" + GUIManager.DISABLED_TINT_COLOR_STRING + "]" + text.Text + "[/color]";
		
		Controls.Add( text );	
	}
}

