using System;

using Squid;

using GameController;


public class RadialSubMenuButton : OptionButton {
	
	public const int BUTTON_WIDTH = 200;
	
	public const int BUTTON_HEIGHT = 61;
	
	public const int BUTTON_PADDING = 3;
	
	public const int BUTTON_TEXT_LEFT_MARGIN = 20;
	
	public RadialSubMenuButton( MenuOption option ) : base( option ) {
		Size = new Point( BUTTON_WIDTH, BUTTON_HEIGHT );
		Padding = new Margin( BUTTON_PADDING, BUTTON_PADDING, BUTTON_PADDING, BUTTON_PADDING );
		
		clickableArea.Style = "RadialSubMenuButton";
		clickableArea.TextAlign = Alignment.MiddleLeft;
		clickableArea.Opacity = 1.0f;
		clickableArea.Size = Size;
	}
}

