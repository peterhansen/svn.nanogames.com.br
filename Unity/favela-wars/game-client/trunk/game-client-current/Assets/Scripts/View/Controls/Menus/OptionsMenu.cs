using System;
using System.Collections.Generic;

using Squid;

using Utils;


public abstract class OptionsMenu : Frame {
	
	protected List< OptionButton > buttons = new List< OptionButton >();
	
	public abstract void Show( Point position );
	
	protected abstract OptionButton CreateButton( MenuOption option, object buttonData );
	
	public OptionButton AddButton( MenuOption option ) {
		return AddButton( option, null );
	}
	
	public OptionButton AddButton( MenuOption option, object buttonData ) {
		OptionButton button = CreateButton( option, buttonData );
		button.AddButtonContents( option.available );
		button.clickableArea.OnMouseClick += new MouseClickEventHandler( option.available? option.callback : NullCallback );
		button.clickableArea.Tag = option.tag == null? option : option.tag;
		buttons.Add( button );
		return button;
	}
	
	protected void NullCallback( Control sender ) {
		Debugger.Log( "Opção não disponível." );
	}
}


