using UnityEngine;
using System.Collections;

public class WorldLabel : MonoBehaviour {
	
	public float cost;
	
	void OnGUI() {
		Vector3 screenPos = Camera.main.WorldToScreenPoint( transform.position );
		GUI.Label( new Rect( screenPos.x, screenPos.y, 100, 40 ), "Cost: " + cost );
	}
}
