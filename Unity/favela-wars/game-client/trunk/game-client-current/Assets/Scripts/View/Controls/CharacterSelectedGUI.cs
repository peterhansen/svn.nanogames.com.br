using UnityEngine;

using GameController;
using GameCommunication;

public class CharacterSelectedGUI
{
	
	private GameObject selectedCharacter;
	private CharacterActionInfo characterActionInfo;
	private GameObject selectionGUI;
	
	public static CharacterSelectedGUI instance;
	
	public CharacterSelectedGUI( GameObject selectedCharacter, Vector3 normal, CharacterActionInfo characterActionInfo){
		this.selectedCharacter = selectedCharacter;
		this.characterActionInfo = characterActionInfo;
		CharacterSelectedGUI.instance = this;
	}
	
	public void DrawStats(){
		Vector3 charBoundPos = selectedCharacter.transform.collider.bounds.min;
		Vector3 charPos = selectedCharacter.transform.position;
		Vector3 charScale = selectedCharacter.transform.localScale;
		
		DestroyGUI();
		
		selectionGUI = Resources.Load( "SelectedGUI" ) as GameObject;
		selectionGUI.transform.position = new Vector3 ( charPos.x, charBoundPos.y + 0.01f, charPos.z );
		
		//MARRETA para testes!
		SelectedGUIController controller = selectionGUI.GetComponent<SelectedGUIController>();
		if(characterActionInfo != null) {
			controller.apIndicator = characterActionInfo.percentualAP;
			controller.lifeIndicator = characterActionInfo.percentualHP;
		}
		
		GameObject clone = GameObject.Instantiate(selectionGUI) as GameObject;
		clone.transform.parent = selectedCharacter.transform;
		selectionGUI = clone;
	}
	
	public void DestroyGUI(){
		if(selectionGUI != null)
			GameObject.Destroy(selectionGUI);
	}
}


