using System;

using Squid;

using Utils;
using GameCommunication;


public class ReloadSubMenuButton : RadialSubMenuButton {
	
	protected const int PADDING = 10;
			
	protected const string RELOAD_BUTTON_IMAGE = "GUI/ActionsMenu/ActionsSubMenu/BtReload";
	
	protected const int RELOAD_BUTTON_SIZE = 42;
	
	protected const int AMMO_BAR_SIZE_Y = 34;
	
	protected ReloadGameAction reloadGameAction;
	
	protected AmmoBar ammoBar;
	
	protected OrientedImageControl buttonImage;
	
	public ReloadSubMenuButton( MenuOption option, object buttonData ): base( option ) {
		reloadGameAction = buttonData as ReloadGameAction;
		if( reloadGameAction == null )
			Debugger.Log( "ReloadSubMenuButton: no reload info." );
	}
	
	public override void AddButtonContents( bool available ) {
		// adicionando barra de munição
		ammoBar = new AmmoBar( 100, available );
		if( reloadGameAction != null )
			ammoBar.SetCurrentValue( (int) ( reloadGameAction.ammoPercentage * 100.0f ) );
		Controls.Add( ammoBar );
		
		// adicionando imagem do botão
		buttonImage = new OrientedImageControl();
		buttonImage.AllowFocus = false;
		buttonImage.NoEvents = true;
		buttonImage.Texture = RELOAD_BUTTON_IMAGE;
		if( !available )
			buttonImage.TintColor = GUIManager.DISABLED_TINT_COLOR;
		Controls.Add( buttonImage );
		
		Size = new Point( BUTTON_WIDTH, BUTTON_HEIGHT );
		OnSizeChanged += new SizeChangedEventHandler( OnResize );
		OnResize( this );
	}
	
	
	protected void OnResize( Control sender ) {
		buttonImage.Size = new Point( RELOAD_BUTTON_SIZE, RELOAD_BUTTON_SIZE );
		buttonImage.Position = new Point( BUTTON_WIDTH - PADDING - RELOAD_BUTTON_SIZE, ( BUTTON_HEIGHT - RELOAD_BUTTON_SIZE ) >> 1 );
		ammoBar.Size = new Point( buttonImage.Position.x - PADDING * 2, AMMO_BAR_SIZE_Y );
		ammoBar.Position = new Point( PADDING, ( Size.y - ammoBar.Size.y ) >> 1 );
	}
	
		
	protected class AmmoBar : Frame {
		
		protected const int PADDING_X = 9;
		
		protected const int PADDING_Y = 8;
		
		protected const int AMMO_BAR_FILL_SIZE_Y = 19;
				
		protected int maxValue;
		
		protected int currentValue = 0;
		
		protected Frame fill;
		
		public AmmoBar( int maxValue, bool available ) {
			this.maxValue = maxValue;
						
			Style = "AmmoBarBackground";
			Enabled = available;
			AllowFocus = false;
			
			fill = new Frame();
			fill.Style = "AmmoBarFill";
			fill.Position = new Point( PADDING_X, PADDING_Y );
			fill.AllowFocus = false;
			fill.Enabled = available;
			Controls.Add( fill );
						
			OnSizeChanged += new SizeChangedEventHandler( OnSizeChangedHandler );
		}
		
		public void SetCurrentValue( int currentValue ) {
			this.currentValue = currentValue;
			UpdateFillSize();
		}
		
		protected void OnSizeChangedHandler( Control sender ) {
			UpdateFillSize();
		}
		
		protected void UpdateFillSize() {
			fill.Size = new Point( ( ( currentValue * ( Size.x - 2 * PADDING_X + 1 ) ) / maxValue ) - 1, AMMO_BAR_FILL_SIZE_Y - 1 );
		}
	}
}


