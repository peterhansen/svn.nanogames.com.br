using Utils;
using Squid;
using System;
using UnityEngine;
using GameController;
using GameCommunication;

public class HolographicPanel : Panel {
	
	protected override void Initialize () {
		
		base.Initialize();
		
		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}

//		this.Style = "holoPanel";
	}
	
//	protected override void DrawStyle (Style style, float opacity) {
//		base.DrawStyle (style, opacity);
//		
//		int texId = GuiHost.Renderer.GetTexture("FactionWindow/fundo_degrade.png");
//		Squid.UVCoords coords;
//		coords = new Squid.UVCoords( 0.0f, 0.0f, 1.0f, 1.0f );
//		GuiHost.Renderer.DrawTexture( texId, Position.x + 5, Position.y, Size.x, Size.y, coords, -1 );
//	}
	
	public void CenterAround( int centerX, int posY, int x, int y ) {
		
		Position = new Point( centerX - ( x / 2 ), posY );
		Size = new Point( x, y - 2 * posY );
		
		VScrollbar vs = this.VScroll;
		vs.ButtonUp.Style = "vsUp";
		vs.ButtonDown.Style = "vsDown";
		vs.Slider.Style = "vsTrack";
		vs.Size = new Point( 15, y - posY );
		vs.ButtonDown.Size = new Point( 15, 15 );
		vs.ButtonUp.Size = new Point( 15, 15 );
		
//		vs.ButtonDown.Position = new Point( 0, 0 );
//		vs.ButtonUp.Position = new Point( 0, 15 );
//		vs.Slider.Position = new Point( 0, 30 );
//		vs.Slider.Size = new Point( 30, 30 );		

//		Squid.Controls.ImageControl decor;
//		decor = new Squid.Controls.ImageControl();
//		decor.Parent = this;
//		decor.Size = Size;
//		decor.Position = new Point( ( x / 2 ) - centerX, posY );
//		decor.Texture = "FactionWindow/fundo_sembordas.png";
		
		
		
//		decor = new Squid.Controls.ImageControl();
//		decor.Parent = this;
//		decor.Size = new Point( this.Parent.Size.x, -63 );
//		decor.Position = new Point( centerX - ( x / 2 ), Size.y - ( posY ) );
//		decor.Texture = "spotlights.png";		
	}
}


