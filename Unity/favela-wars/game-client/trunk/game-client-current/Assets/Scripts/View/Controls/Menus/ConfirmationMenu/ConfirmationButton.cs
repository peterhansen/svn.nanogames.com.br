using System;

using Squid;

using Utils;


public class ConfirmationButton : OptionButton {
	
	public const int CONFIRMATION_BUTTON_SIZE_X = 51;
	
	public const int CONFIRMATION_BUTTON_SIZE_Y = 50;
	
	public const int CONFIRMATION_ICON_SIZE = 32;
	
	protected const string CONFIRMATION_ICONS_PATH = "GUI/ActionsMenu/ConfirmationMenu/";
		
	public ConfirmationButton( MenuOption option ) : base( option ) {
		Style = "ConfirmationButton";
		
		OrientedImageControl icon = new OrientedImageControl();
		icon.Texture = GetConfirmationIcon( option.icon );
		icon.AllowFocus = false;
		icon.NoEvents = true;
		
		Size = new Point( CONFIRMATION_BUTTON_SIZE_X, CONFIRMATION_BUTTON_SIZE_Y );
		clickableArea.Size = Size;
		icon.Size = new Point( CONFIRMATION_ICON_SIZE, CONFIRMATION_ICON_SIZE );
		icon.Position = new Point( ( Size.x - icon.Size.x ) >> 1 , ( Size.y - icon.Size.y ) >> 1 );
		
		Controls.Add( icon );
	}
	
	protected static string GetConfirmationIcon( IconType type ) {
		switch( type ) {
		case IconType.OK:
			return CONFIRMATION_ICONS_PATH + "IconConfirm";
			
		case IconType.CANCEL:
			return CONFIRMATION_ICONS_PATH + "IconCancel";
			
		case IconType.TAKE_COVER:
			return CONFIRMATION_ICONS_PATH + "IconProtect";
			
		default:
			Debugger.LogWarning( "ConfimationButton: no confirmation icon for IconType " + Enum.GetName( typeof( IconType ), type ) );
			return "";
		}
	}
}
