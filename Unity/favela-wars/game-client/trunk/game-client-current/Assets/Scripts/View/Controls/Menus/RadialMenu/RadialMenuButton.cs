using System;

using UnityEngine;

using Squid;
using Squid.Controls;

using Utils;
using GameCommunication;


public class RadialMenuButton : OptionButton {
	
	public const int BUTTON_SIDE_SIZE = 78;
	
	public const int BUTTON_ICON_SIDE_SIZE = 46;
	
	public const int BUTTON_ICON_SELECTED_SIDE_SIZE = 58;
			
	protected const float BUTTON_CLICK_ANIMATION_TIME = 200.0f;
	
	protected const float FLOATING_REFRESH = Mathf.PI * 4.0f;
	
	protected const float FLOATING_SPEED = 5.0f;
	
	protected const int FLOATING_MAX_HEIGHT = 5;
	
	protected const string ICONS_PATH = "GUI/ActionsMenu/Icons/";
	
	public enum RadialButtonType {
		DEFAULT,
		RIGHT,
		LEFT
	}
	
	//protected float floatingHeight;
	
	protected bool isOn = false;
	
	protected RadialButtonType type;
	
	public OrientedImageControl buttonImage;
	
	public OrientedImageControl pressedButtonImage;
	
	public OrientedImageControl icon;
	
	// usado para a flutuada do botão
	//protected float accTime = 0.0f;
	
	protected Quadrant quadrant;
	public Quadrant Quadrant {
		get { return quadrant; }
		set { quadrant = value; }
	}
	
	public RadialMenuButton( MenuOption option ) : base( option ) {
		type = RadialButtonType.DEFAULT;
		if( option.tag != null ) {
			GameAction optionGameAction = option.tag as GameAction;
			switch( optionGameAction.Hand ) {
			case GameAction.ActionHand.LEFT:
				type = RadialButtonType.LEFT;
				break;
				
			case GameAction.ActionHand.RIGHT:
				type = RadialButtonType.RIGHT;
				break;
			}
		}
		
		// configurando fundo do botão
		clickableArea.Size = new Point( BUTTON_SIDE_SIZE, BUTTON_SIDE_SIZE );
		
		buttonImage = new OrientedImageControl();
		buttonImage.AllowFocus = false;
		buttonImage.NoEvents = true;
		
		pressedButtonImage = new OrientedImageControl();
		pressedButtonImage.AllowFocus = false;
		pressedButtonImage.NoEvents = true;
		
		switch( type ) {
		case RadialButtonType.DEFAULT:
			buttonImage.Texture = "GUI/ActionsMenu/BtDefault";
			break;
			
		case RadialButtonType.LEFT:
			buttonImage.Texture = "GUI/ActionsMenu/BtLeft";
			break;
			
		case RadialButtonType.RIGHT:
			buttonImage.Texture = "GUI/ActionsMenu/BtRight";
			break;
		}
		pressedButtonImage.Texture = "GUI/ActionsMenu/BtGlow";
		pressedButtonImage.Opacity = 0.0f;
		
		Controls.Add( clickableArea );
		Controls.Add( buttonImage );
		Controls.Add( pressedButtonImage );
		
		// adicionando ícone, caso ele exista
		string iconName = GetIconPath( option.icon );
		icon = new OrientedImageControl();
		icon.AllowFocus = false;
		icon.NoEvents = true;
		Controls.Add( icon );
		if( iconName == null ) {
			Debugger.LogWarning( "RadialMenuButton: no radial menu button icon found for \"" + iconName + "." );
		} else {
			icon.Texture = iconName;
		}
	
		clickableArea.OnMouseClick += new MouseClickEventHandler( Switch );
		OnSizeChanged += new SizeChangedEventHandler( OnResize );
	}
	
	
	protected void OnResize( Control sender ) {
		buttonImage.Size = Size;
		pressedButtonImage.Size = Size;
		
		if( icon != null ) {
			icon.Size = new Point( ( sender.Size.x * BUTTON_ICON_SIDE_SIZE ) / BUTTON_SIDE_SIZE, ( sender.Size.y * BUTTON_ICON_SIDE_SIZE ) / BUTTON_SIDE_SIZE );
			icon.Position = new Point( ( sender.Size.x - icon.Size.x ) / 2,  ( sender.Size.y - icon.Size.y ) / 2 );
		}
	}
	
	
	protected void Switch( Control sender ) {
		Turn( !isOn );
	}

	public void Turn( bool isOn ) {
		this.isOn = isOn;
		if( isOn ) {
			if( icon != null ) {
				icon.Animation.Size( new Point( BUTTON_ICON_SELECTED_SIDE_SIZE, BUTTON_ICON_SELECTED_SIDE_SIZE ), BUTTON_CLICK_ANIMATION_TIME );
				icon.Animation.Position( new Point( ( BUTTON_SIDE_SIZE - BUTTON_ICON_SELECTED_SIDE_SIZE ) / 2,  ( BUTTON_SIDE_SIZE - BUTTON_ICON_SELECTED_SIDE_SIZE ) / 2 ), BUTTON_CLICK_ANIMATION_TIME );
			}
		
			// trocando imagem de fundo para iluminar o botão
			buttonImage.Animation.Opacity( 0.0f, BUTTON_CLICK_ANIMATION_TIME );
			pressedButtonImage.Animation.Opacity( 1.0f, BUTTON_CLICK_ANIMATION_TIME );
		} else {
			// fazendo o ícone diminuir
			if( icon != null ) {
				icon.Animation.Size( new Point( BUTTON_ICON_SIDE_SIZE, BUTTON_ICON_SIDE_SIZE ), BUTTON_CLICK_ANIMATION_TIME );
				icon.Animation.Position( new Point( ( BUTTON_SIDE_SIZE - BUTTON_ICON_SIDE_SIZE ) / 2,  ( BUTTON_SIDE_SIZE - BUTTON_ICON_SIDE_SIZE ) / 2 ), BUTTON_CLICK_ANIMATION_TIME );
			}
			
			// trocando imagem de fundo para escurecer o botão
			buttonImage.Animation.Opacity( 1.0f, BUTTON_CLICK_ANIMATION_TIME );
			pressedButtonImage.Animation.Opacity( 0.0f, BUTTON_CLICK_ANIMATION_TIME );
		}
	}
	
	
	protected string GetIconPath( IconType radialMenuIcon ) {
		switch( radialMenuIcon ) {
		case IconType.CHARACTER_INFO:
			return ICONS_PATH + "Profile.png";
			
		case IconType.TACTICS:
			return ICONS_PATH + "Tactics.png";
			
		case IconType.HAND_LEFT:
			return ICONS_PATH + "HandLeft.png";
			
		case IconType.HAND_RIGHT:
			return ICONS_PATH + "HandRight.png";
			
		case IconType.GRENADE:
			return ICONS_PATH + "Object.png";
			
		default:
			// TODO: carregar aqui imagem default, se é que isso vai existir
			return null;
		}
	}
}

