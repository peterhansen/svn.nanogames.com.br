using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;


public enum GUIElementID {
	GET_TURN_NUMBER_BUTTON
}

public struct InputEventGUI {
	public GUIElementID elementID;
}

// TODO: essa classe pode se transformar num InteractionEvent gen�rico
public struct InputEventScene {
	public int mouseButton;
	public bool singleClick;
	public GameObject gameObject;
	public Vector3 selectionPosition;
	public Vector3 selectionNormal;
}


public class GUIManager : GuiRenderer {
		
	public static int DISABLED_TINT_COLOR = ColorInt.RGBA( 1.0f, 1.0f, 1.0f, 0.4f );

	public static string DISABLED_TINT_COLOR_STRING = "64ffffff";
	
	GameObject selectedCharacter;
	
	public static Desktop desktop;
	protected static Skin skin;
	
	private static InteractionController interactionController;
	
	protected static List< Control > controls = new List<Control>();
	
	protected static List< FloatingText > floatingTexts = new List< FloatingText >();
	
	
	public void Awake() {
		Application.runInBackground = true;
		DontDestroyOnLoad( this );
	}
	
	protected override void Start () {
		base.Start();
		
		desktop = Desktop;
		skin = Skin;
		
		// Desktop and Skin are inherited fields
		Desktop.ShowCursor = true;
		Skin.DefaultFont = "Fonte_Texto12";
		GuiHost.SetSkin(Skin);
        #region create skin
		
        ControlStyle itemStyle = new ControlStyle();
        itemStyle.Tiling = TextureMode.Grid;
        itemStyle.Grid = new Margin(6);
        itemStyle.Texture = "button_hot.dds";
        itemStyle.Default.Texture = "button_default.dds";
        itemStyle.Pressed.Texture = "button_down.dds";
        itemStyle.SelectedPressed.Texture = "button_down.dds";
        itemStyle.Focused.Texture = "button_down.dds";
        itemStyle.SelectedFocused.Texture = "button_down.dds";
        itemStyle.Selected.Texture = "button_down.dds";
        itemStyle.SelectedHot.Texture = "button_down.dds";
        itemStyle.TextPadding = new Margin(10, 0, 10, 0);

        ControlStyle inputStyle = new ControlStyle();
        inputStyle.Texture = "input_default.dds";
        inputStyle.Hot.Texture = "input_focused.dds";
        inputStyle.Focused.Texture = "input_focused.dds";
        inputStyle.TextPadding = new Margin(8);
        inputStyle.Tiling = TextureMode.Grid;
        inputStyle.Focused.Tint = ColorInt.RGBA(1, 0, 0, 1);
        inputStyle.Grid = new Margin(6);

        ControlStyle holoPanelStyle = new ControlStyle();
        holoPanelStyle.Texture = "FactionWindow/fundo_mid.png";
		holoPanelStyle.Tiling = TextureMode.Repeat;
		
		ControlStyle radialSubMenuButton = new ControlStyle();
		radialSubMenuButton.Texture = "FactionWindow/fundo.png";
		
        ControlStyle titledFrame = new ControlStyle();
        titledFrame.Texture = "ListaInimigos/Division.png";
		titledFrame.Tiling = TextureMode.Grid;
		
        ControlStyle buttonStyle = new ControlStyle();
        buttonStyle.Texture = "button_default.dds";
        buttonStyle.Hot.Texture = "button_hot.dds";
        buttonStyle.Focused.Texture = "button_hot.dds";
        buttonStyle.Pressed.Texture = "button_down.dds";
        buttonStyle.Checked.Texture = "button_down.dds";
        buttonStyle.CheckedHot.Texture = "button_down.dds";
        buttonStyle.TextAlign = Alignment.MiddleCenter;
        buttonStyle.Tiling = TextureMode.Grid;
        buttonStyle.Grid = new Margin(6);

		ControlStyle radialMenuButtonStyle = new ControlStyle();
        radialMenuButtonStyle.Texture = "GUI/ActionsMenu/BtDefault.png";
		
		ControlStyle radialMenuLeftButtonStyle = new ControlStyle();
        radialMenuLeftButtonStyle.Texture = "GUI/ActionsMenu/BtLeft.png";
		
		ControlStyle radialMenuRightButtonStyle = new ControlStyle();
        radialMenuRightButtonStyle.Texture = "GUI/ActionsMenu/BtRight.png";

        ControlStyle frameStyle = new ControlStyle();
        frameStyle.Texture = "FactionWindow/fundo_sembordas.png";

        ControlStyle frameStyle2 = new ControlStyle();
        frameStyle2.CheckedHot.Tiling = TextureMode.Grid;
        frameStyle2.Tiling = TextureMode.Grid;
        frameStyle2.Grid = new Margin(8);
        frameStyle2.Texture = "grid2.dds";
        frameStyle2.TextPadding = new Margin(8);

		ControlStyle ammoBarBackground = new ControlStyle();
		ammoBarBackground.Texture = "GUI/ActionsMenu/ActionsSubMenu/ProgressBarBkg.png";
		ammoBarBackground.Disabled.Tint = ColorInt.RGBA( 0.0f, 1.0f, 1.0f, 0.4f );
		
		ControlStyle ammoBarFill = new ControlStyle();
		ammoBarFill.Texture = "GUI/ActionsMenu/ActionsSubMenu/ProgressBarFill.png";
		ammoBarFill.Tiling = TextureMode.Repeat;
		ammoBarFill.Disabled.TextColor = ColorInt.RGBA( 0.0f, 1.0f, 1.0f, 0.4f );
		 
		ControlStyle confirmationButton = new ControlStyle();
		confirmationButton.Texture = "GUI/ActionsMenu/ConfirmationMenu/IconBox.png";
		
		ControlStyle confirmationBackground = new ControlStyle();
		confirmationBackground.Texture = "GUI/ActionsMenu/ConfirmationMenu/BgConfirm.png";
		
		ControlStyle toggleButton = new ControlStyle();
        toggleButton.Texture = "ListaInimigos/Button.png";
        toggleButton.Hot.Texture = "ListaInimigos/Button.png";
        toggleButton.Focused.Texture = "ListaInimigos/Button.png";
        toggleButton.Pressed.Texture = "ListaInimigos/Button.png";
        toggleButton.Checked.Texture = "ListaInimigos/ButtonSelected.png";
        toggleButton.CheckedHot.Texture = "ListaInimigos/ButtonSelected.png";
		toggleButton.Font = "Fonte_Titulo14";
		toggleButton.Default.Font = "Fonte_Titulo14";

		toggleButton.TextAlign = Alignment.MiddleCenter;
		
		ControlStyle eyeButton = new ControlStyle();
        eyeButton.Texture = "ListaInimigos/IconVisible.png";
        eyeButton.Hot.Texture = "ListaInimigos/IconVisible.png";		
        eyeButton.Focused.Texture = "ListaInimigos/IconVisible.png";
        eyeButton.Pressed.Texture = "ListaInimigos/IconVisible.png";
        eyeButton.Checked.Texture = "ListaInimigos/IconHidden.png";
        eyeButton.CheckedHot.Texture = "ListaInimigos/IconHidden.png";
		eyeButton.TextAlign = Alignment.MiddleCenter;

		ControlStyle vsTrack = new ControlStyle();
		vsTrack.Default.Texture = "GUI/ScrollBar/Button.png";
		vsTrack.Hot.Texture = "GUI/ScrollBar/Button.png";
		vsTrack.Pressed.Texture = "GUI/ScrollBar/Button.png";
		vsTrack.Focused.Texture = "GUI/ScrollBar/Button.png";
		
		ControlStyle vsBg = new ControlStyle();
		vsBg.Default.Texture = "GUI/ScrollBar/Scroll.png";
		vsBg.Hot.Texture = "GUI/ScrollBar/Scroll.png";
		vsBg.Pressed.Texture = "GUI/ScrollBar/Scroll.png";
		vsBg.Focused.Texture = "GUI/ScrollBar/Scroll.png";

        ControlStyle hscrollTrackStyle = new ControlStyle();
        hscrollTrackStyle.Tiling = TextureMode.Grid;
        hscrollTrackStyle.Grid = new Margin(3, 0, 3, 0);
        hscrollTrackStyle.Texture = "hscroll_track.dds";

        ControlStyle hscrollButtonStyle = new ControlStyle();
        hscrollButtonStyle.Tiling = TextureMode.Grid;
        hscrollButtonStyle.Grid = new Margin(4, 0, 4, 0);
        hscrollButtonStyle.Texture = "hscroll_button.dds";
        hscrollButtonStyle.Hot.Texture = "hscroll_button_hot.dds";
        hscrollButtonStyle.Pressed.Texture = "hscroll_button_down.dds";

        ControlStyle hscrollUp = new ControlStyle();
        hscrollUp.Default.Texture = "hscrollUp_default.dds";
        hscrollUp.Hot.Texture = "hscrollUp_hot.dds";
        hscrollUp.Pressed.Texture = "hscrollUp_down.dds";
        hscrollUp.Focused.Texture = "hscrollUp_hot.dds";


        ControlStyle checkboxStyle = new ControlStyle();
        checkboxStyle.Default.Texture = "ListaInimigos/Box.png";
        checkboxStyle.Hot.Texture = "ListaInimigos/Box.png";
        checkboxStyle.Pressed.Texture = "ListaInimigos/Box.png";
        checkboxStyle.Checked.Texture = "ListaInimigos/BoxChecked.png";
        checkboxStyle.CheckedFocused.Texture = "ListaInimigos/BoxChecked.png";
        checkboxStyle.CheckedHot.Texture = "ListaInimigos/BoxChecked.png";
        checkboxStyle.CheckedPressed.Texture = "ListaInimigos/BoxChecked.png";
		
        checkboxStyle.Default.Tiling = TextureMode.Repeat;
        checkboxStyle.Hot.Tiling = TextureMode.Repeat;
        checkboxStyle.Pressed.Tiling = TextureMode.Repeat;
        checkboxStyle.Checked.Tiling = TextureMode.Repeat;
        checkboxStyle.CheckedFocused.Tiling = TextureMode.Repeat;
        checkboxStyle.CheckedHot.Tiling = TextureMode.Repeat;
        checkboxStyle.CheckedPressed.Tiling = TextureMode.Repeat;
		
        ControlStyle checkButtonStyle = new ControlStyle();
        checkButtonStyle.Default.Texture = "checkbox_default.dds";
        checkButtonStyle.Hot.Texture = "checkbox_hot.dds";
        checkButtonStyle.Pressed.Texture = "checkbox_down.dds";
        checkButtonStyle.Checked.Texture = "checkbox_checked.dds";
        checkButtonStyle.CheckedFocused.Texture = "checkbox_checked_hot.dds";
        checkButtonStyle.CheckedHot.Texture = "checkbox_checked_hot.dds";
        checkButtonStyle.CheckedPressed.Texture = "checkbox_down.dds";
		
        ControlStyle comboLabelStyle = new ControlStyle();
        comboLabelStyle.TextPadding = new Margin(10, 0, 0, 0);
        comboLabelStyle.Default.Texture = "combo_default.dds";
        comboLabelStyle.Hot.Texture = "combo_hot.dds";
        comboLabelStyle.Pressed.Texture = "combo_down.dds";
        comboLabelStyle.Focused.Texture = "combo_hot.dds";
        comboLabelStyle.Tiling = TextureMode.Grid;
        comboLabelStyle.Grid = new Margin(6, 0, 0, 0);

        ControlStyle comboButtonStyle = new ControlStyle();
        comboButtonStyle.Default.Texture = "combo_button_default.dds";
        comboButtonStyle.Hot.Texture = "combo_button_hot.dds";
        comboButtonStyle.Pressed.Texture = "combo_button_down.dds";
        comboButtonStyle.Focused.Texture = "combo_button_hot.dds";

        ControlStyle labelStyle = new ControlStyle();
        labelStyle.TextAlign = Alignment.TopRight;
        labelStyle.TextPadding = new Squid.Margin(8);
		
		ControlStyle toolbarStyle = new ControlStyle();
		toolbarStyle.Texture = "camera_toolbar";
		
		
		ControlStyle fontFrameTitle = new ControlStyle();
		fontFrameTitle.Font = "Fonte_Nome01";
		fontFrameTitle.Default.Font = "Fonte_Nome01";
		
		ControlStyle fontTitle = new ControlStyle();
		fontTitle.Font = "Fonte_Titulo14";
		fontTitle.Default.Font = "Fonte_Titulo14";
		
		ControlStyle cameraControlLeftButtonStyle = new ControlStyle();
        cameraControlLeftButtonStyle.Texture = "GUI/CameraControl/BtCamEsquerda.png";
		
		ControlStyle cameraControlRightButtonStyle = new ControlStyle();
        cameraControlRightButtonStyle.Texture = "GUI/CameraControl/BtCamDireita.png";
		
		ControlStyle cameraControlButtonStyle = new ControlStyle();
        cameraControlButtonStyle.Texture = "GUI/CameraControl/CameraIcon.png";
		
		

		Skin.Styles.Add("holoPanel", holoPanelStyle );
        Skin.Styles.Add("item", itemStyle);
        Skin.Styles.Add("textbox", inputStyle);
        Skin.Styles.Add("button", buttonStyle);
        Skin.Styles.Add("frame", frameStyle);
        Skin.Styles.Add("frame2", frameStyle2);
        Skin.Styles.Add("checkBox", checkButtonStyle);
        Skin.Styles.Add("comboLabel", comboLabelStyle);
        Skin.Styles.Add("comboButton", comboButtonStyle);
        Skin.Styles.Add("hscrollTrack", hscrollTrackStyle);
        Skin.Styles.Add("hscrollButton", hscrollButtonStyle);
        Skin.Styles.Add("hscrollUp", hscrollUp);
        Skin.Styles.Add("multiline", labelStyle);
		Skin.Styles.Add("checkboxStyle", checkboxStyle);
		Skin.Styles.Add("camera_toolbar", toolbarStyle );
		Skin.Styles.Add("togglebutton", toggleButton );
		Skin.Styles.Add("eyebutton", eyeButton );
		Skin.Styles.Add("actionsmenubutton", radialMenuButtonStyle );
		Skin.Styles.Add("actionsmenurightbutton", radialMenuRightButtonStyle );
		Skin.Styles.Add("actionsmenuleftbutton", radialMenuLeftButtonStyle );
		Skin.Styles.Add("titledFrame", titledFrame );
		Skin.Styles.Add("FontFrameTitle", fontFrameTitle );
		Skin.Styles.Add("FontTitle", fontTitle );
		Skin.Styles.Add( "ConfirmationButton", confirmationButton );
		Skin.Styles.Add( "ConfirmationBackground", confirmationBackground );
		Skin.Styles.Add( "RadialSubMenuButton", radialSubMenuButton );
		Skin.Styles.Add( "AmmoBarBackground", ammoBarBackground );
		Skin.Styles.Add( "AmmoBarFill", ammoBarFill );

		Skin.Styles.Add("vsTrack", vsTrack );
		Skin.Styles.Add("vsBg", vsBg );
		Skin.Styles.Add("cameracontrolrightbutton", cameraControlRightButtonStyle );
		Skin.Styles.Add("cameracontrolleftbutton", cameraControlLeftButtonStyle );
		Skin.Styles.Add("cameracontrolbutton", cameraControlButtonStyle );
		
        #endregion
	}
	
	
	public void Update() {
		foreach( FloatingText floatingText in floatingTexts ) {
			if( floatingText.IsDone )
				floatingText.Parent = null;
		}
		
		if ( GameActionRunner.IsBusy() )
			return;
		
		if ( interactionController != null )
			interactionController.Update();
	}
	
	public static void Reset() {
		SetInteractionController( new SelectCharacterInteractionController() );		
	}
	
	
	public static void SetInteractionController( InteractionController newController ) {
		if ( interactionController != null ) {
			RemoveRenderers( interactionController );
			interactionController.OnRemoved();
		}
		
		if ( newController == null ) {
			throw new System.Exception( "NULL interactionController (previous = " + interactionController + ")" );
		}
		
		interactionController = newController;
		
		AddRenderers( newController );
		newController.Initialize();
	}
	
	
	public static InteractionController GetInteractionController() {
		return interactionController;
	}
	
		
	public static void ClearAllRenderables() {
		foreach( Control control in controls )
			control.Parent = null;
		controls.Clear();
	}
	
	protected static void AddControl( Control c ) {
		controls.Add( c );
		c.Parent = desktop;
	}
	
	protected static void RemoveControl( Control c ) {
		controls.Remove( c );
		c.Parent = null;
	}
	
	
	protected static void RemoveRenderers( InteractionController interactionController ) {
		foreach( Control control in interactionController.controls ) {
			RemoveControl( control );
		}
	}
	
	
	public static void AddRenderers( InteractionController interactionController ) {
		foreach( Control control in interactionController.controls ) {
			AddControl( control );
		}
	}
	
	
	public static void AddFloatingText( string message, int x, int y ) {
		floatingTexts.Add( new FloatingText( message, x, y ) );
	}
	
	//Hack: depois retirar essa marreta do nome do timer
	public static void HideTimer(){
		GameObject countDownTimer = GameObject.Find("CountDownTimer(Clone)");
		if(countDownTimer != null){
			TimerController tc = countDownTimer.GetComponent< TimerController > ();
			tc.StopTimer();
			tc.HideTimer();
		}
	}
	
	public static void ResetTimer(){
		GameObject countDownTimer = GameObject.Find("CountDownTimer(Clone)");
		if(countDownTimer != null){
			TimerController tc = countDownTimer.GetComponent< TimerController > ();
			tc.ShowTimer();
			tc.ResetTimer();
		}
	}
	
	public class FloatingText : Label {
			
		protected const int FLOATING_DISTANCE = -50;
		
		protected const int TOTAL_RUNNING_TIME = 3000;
		
		protected bool isDone = false;
		public bool IsDone {
			get { return isDone; }	
		}
		
		public FloatingText( string text, int x, int y ) : base() {
			Text = text;
			Position = new Point( x, y );
		}
		
		protected override void Initialize() {
			base.Initialize();
			
			Size = new Point( 40, 20 );
			Parent = GUIManager.desktop;
			Animation.Custom(Float(this));
		}
		
		private System.Collections.IEnumerator Float( FloatingText label ) {
			Animation.Position( Position + new Point( 0, FLOATING_DISTANCE ), TOTAL_RUNNING_TIME );
			yield return Animation.Opacity( 0.0f, TOTAL_RUNNING_TIME );
			
			Animation.Stop();
			isDone = true;
	    }
	}
}