using System;
using GameCommunication;
using System.Collections.Generic;

using Utils;

namespace GameModel {
	
	public class LoadSnapshotHandler : Handler {
		
		protected LoadSnapshotRequest loadSnapshotRequest;
		
		
		public LoadSnapshotHandler( LoadSnapshotRequest loadSnapshotRequest ) {
			this.loadSnapshotRequest = loadSnapshotRequest;
		}
		
		
		public override Response GetResponse() {
			Response r = new Response();
			Mission m = Mission.GetInstance( loadSnapshotRequest.GetPlayerID() );
			
			foreach ( Character character in m.GetCharacters() ) {
				r.AddGameActionData( new SetWorldObjectTransformData( character.GetWorldID(), character.GetGameTransform() ) );
			}
			
			return r;
		}
		
	}
}

