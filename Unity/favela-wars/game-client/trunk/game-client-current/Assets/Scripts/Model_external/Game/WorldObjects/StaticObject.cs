using System;
using System.Text;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	[ Serializable ]
	public class StaticObject : WorldObject {
		
		public StaticObject( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID ) {
		}
	}
}

