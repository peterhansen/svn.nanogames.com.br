using System;

using Utils;
using GameCommunication;


namespace GameModel {

	[ Serializable ]
	public class BOPE : Soldier {
		
		#if UNITY_EDITOR
		// necessário para serializar a classe no editor
		public BOPE() {
		}
		#endif
		
		
		public BOPE( int gameObjectID ) : base( gameObjectID, "0000ffff" ) {
			Data.Faction = Faction.POLICE; 
			data.Faction = Faction.POLICE;
		}
		
		
		public BOPE( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID ) {
			Data.Faction = Faction.POLICE; 
		}
		
		
		public override GameAction GetPossibleActionsList () {
			GameAction toReturn = base.GetPossibleActionsList();
			
			return toReturn;
		}		
		
	}
	
}