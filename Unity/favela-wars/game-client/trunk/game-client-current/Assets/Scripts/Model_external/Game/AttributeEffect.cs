using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class AttributeEffect {
		
		protected ModAttribute attribute;
		
		protected EffectType effectType;
		
		protected int remainingTurns;
		
		protected float points;
		
		
		public AttributeEffect( EffectType e, int turns, float points ) {
			this.effectType = e;
			this.remainingTurns = turns;
			this.points = points;
		}
		
		
		public EffectType EffectType {
			get { return effectType; }
			set { effectType = value; }
		}
		
		
		public int RemainingTurns {
			get { return remainingTurns; }
			set { remainingTurns = value; }
		}
		
		
		public float Points {
			get { return points; }
			set { points = value; }
		}
		
		
		public void OnActive( Character c ) {
			switch ( effectType ) {
				case EffectType.Agility:
					attribute = c.Data.Agility;
				break;
				
				case EffectType.AttackLevel:
					attribute = c.Data.AttackLevel;
				break;
				
				case EffectType.HitPoints:
					attribute = c.Data.HitPoints;
				break;
				
				case EffectType.Intelligence:
					attribute = c.Data.Intelligence;
				break;
			
				case EffectType.MoralPoints:
					attribute = c.Data.MoralPoints;
				break;
				
				case EffectType.Perception:
					attribute = c.Data.Perception;
				break;
				
				case EffectType.Technique:
					attribute = c.Data.Technique;
				break;
				
				default:
					throw new System.Exception( "Invalid effectType: " + effectType );
			}
			
			attribute.Modifier += points;
		}
		
		
		/// <summary>
		/// Atualiza o efeito após a passagem de um turno.
		/// </summary>
		/// <returns>
		/// <c>bool</c> indicando se o efeito ainda está ativo.
		/// </returns>
		public bool OnTurnChanged() {
			if ( remainingTurns > 0 ) {
				--remainingTurns;
				
				switch ( effectType ) {
					case EffectType.HitPoints:
						// regenera a energia a cada turno
						attribute.Value += points;
					break;
				}
				
				if ( remainingTurns <= 0 )
					OnDismiss();
			}
			
			return remainingTurns > 0;
		}
		
		
		/// <summary>
		/// Método chamado quando o efeito expira.
		/// </summary>
		public void OnDismiss() {
			switch ( effectType ) {
				case EffectType.HitPoints:
				break;
				
				default:
					attribute.Modifier -= points;
				break;
			}
		}
		
		
	}
	
	
}


