using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameController;
using GameCommunication;


using Utils;
	
	
public class ChooseMission : MonoBehaviour {
	
	public void Start() {
		Caching.CleanCache(); // TODO teste - limpa o cache antes de come�ar uma miss�o	
		Application.runInBackground = true;
	}
	
	

	public void OnGUI() {
		string[] missions = Directory.GetFiles( Path.Combine( Common.Paths.SERVER, "missions" ), "*.mission" );
	
		for ( int i = 0; i < missions.Length; ++i ) {
			string mission = Path.GetFileNameWithoutExtension( missions[ i ] );
			bool even = ( i & 1 ) == 0;
			int y = 10 + ( i >> 1 ) * 40;
			
			if( GUI.Button( new Rect( even ? 10 : 200, y, 100, 30 ), mission ) ) {
				GameManager gc = GameManager.GetInstance();
				
				List< uint > soldierIDs = new List< uint > { 0x00ff00ff, 0x00ffffff, 0xffff00ff, 0x000000ff, 0x00ff0000 };
				
				gc.SendRequest( new StartMissionRequest( gc.GetLocalPlayer(), mission, soldierIDs ) );
				gc.SendRequest( new LoadSnapshotRequest( gc.GetLocalPlayer() ) );
				
				Application.LoadLevel( "Mission Screen" );
				this.enabled = false;
			}	
		}
	}
	
}
