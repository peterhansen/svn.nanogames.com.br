/**
 * @file TestRunner.cs
 * 
 * Unity3D unit test runner.
 * Sets up the unit testing suite and executes all unit tests.
 * Drag this onto an empty GameObject to run tests.
 */

using UnityEngine;
using System.Collections;
using System.Reflection;
using SharpUnit;

public class Unity3D_TestRunner : MonoBehaviour 
{
    /**
     * Initialize class resources.
     */
	void Start() 
    {
        // Create test suite
        TestSuite suite = new TestSuite();
		
		System.Type[] types = Assembly.GetExecutingAssembly().GetTypes();
		foreach (System.Type type in types)
		{
		    if (type.IsSubclassOf(typeof(TestCase)))
		    {
		        //Find all Constructors with no arguments (Type.EmptyTypes) and call them - with no arguments (null)
		       	TestCase tc = (TestCase)type.GetConstructor(System.Type.EmptyTypes).Invoke(null);
				suite.AddAll( tc );
		    }
		}
		

        // Run the tests
        TestResult res = suite.Run(null);

        // Report results
        Unity3D_TestReporter reporter = new Unity3D_TestReporter();
        reporter.LogResults(res);
	}
}
