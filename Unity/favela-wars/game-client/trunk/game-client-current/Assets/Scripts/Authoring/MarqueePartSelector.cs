using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

using GameController;
using Utils;


[ ExecuteInEditMode ]
public class MarqueePartSelector : MonoBehaviour {
	
	protected bool oldShowFloor1 = true;
	protected bool oldShowFloor2 = true;
	protected bool oldShowFloor3 = true;
	protected bool oldShowEdge1 = true;
	protected bool oldShowEdge2 = true;
	protected bool oldShowEdge3 = true;
	
	public bool showFloor1 = true;
	public bool showFloor2 = true;
	public bool showFloor3 = true;
	public bool showEdge1 = true;
	public bool showEdge2 = true;
	public bool showEdge3 = true;
	
	
	public void Update() {
		bool changed = false;
		
		if ( showFloor1 != oldShowFloor1 ) {
			changed = true;
			oldShowFloor1 = showFloor1;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "marquise_1" )
					childTransform.gameObject.active = showFloor1;
		}

		if ( showFloor2 != oldShowFloor2 ) {
			changed = true;
			oldShowFloor2 = showFloor2;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "marquise_2" )
					childTransform.gameObject.active = showFloor2;
		}
		
		if ( showFloor3 != oldShowFloor3 ) {
			changed = true;
			oldShowFloor3 = showFloor3;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "marquise_3" )
					childTransform.gameObject.active = showFloor3;
		}
		
		if ( showEdge1 != oldShowEdge1 ) {
			changed = true;
			oldShowEdge1 = showEdge1;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "coluna_marquise_1" )
					childTransform.gameObject.active = showEdge1;	
		}
		
		if ( showEdge2 != oldShowEdge2 ) {
			changed = true;
			oldShowEdge2 = showEdge2;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "coluna_marquise_2" )
					childTransform.gameObject.active = showEdge2;	
		}
		
		if ( showEdge3 != oldShowEdge3 ) {
			changed = true;
			oldShowEdge3 = showEdge3;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "coluna_marquise_3" )
					childTransform.gameObject.active = showEdge3;
		}
		
		// atualiza o BoxCollider de acordo com as partes ativas
		if ( changed ) {
			BoxCollider c = GetComponent< BoxCollider >();
			if ( c != null ) {
				Bounds b = ControllerUtils.GetBox( gameObject, true );
				c.center = b.center;
				c.size = b.size;
			}
		}
	}
}

#endif