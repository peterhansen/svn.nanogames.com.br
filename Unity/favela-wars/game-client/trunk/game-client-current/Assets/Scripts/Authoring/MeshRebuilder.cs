using System;

using UnityEngine;

using Object = UnityEngine.Object;


/// <summary>
/// Classe utilitária para que se possa serializar uma malha de polígonos gerada (usando MeshCombiner, por exemplo).
/// </summary>
public class MeshRebuilder : MonoBehaviour {
	
	public int[] triangles;

	public Vector3[] vertices;
	
	public Vector3[] normals;
	
	public Color[] colors;
	
	public Vector2[] uv;
	
	public Vector2[] uv1;
	
	public Vector4[] tangents;
	
	
	public void SetMesh( Mesh m ) {
		triangles    = m.triangles;
        vertices     = m.vertices;
        normals      = m.normals;
        colors       = m.colors;
        uv           = m.uv;
        uv1          = m.uv1;
        tangents     = m.tangents;
	}
	
	
	public void Awake() {
		MeshFilter f = GetComponent< MeshFilter >();
		if ( f == null )
			f = gameObject.AddComponent< MeshFilter >();
		
		Mesh m = f.sharedMesh;
		if ( m == null ) {
			m = new Mesh();
			m.vertices     = vertices;
			m.normals      = normals;
			m.colors       = colors;
			m.uv           = uv;
			m.uv1          = uv1;
			m.tangents     = tangents;
			m.triangles    = triangles;
			
			m.RecalculateNormals();
			m.RecalculateBounds();
			m.Optimize();
			
			f.sharedMesh = m;
		}
		
		Object.Destroy( this );
	}
	
}

