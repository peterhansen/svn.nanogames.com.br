using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


public class SpawnAreaBOPE : SpawnArea {
	
	public SpawnAreaBOPE() : base() {
	}
	
	
	public override Faction GetFaction() {
		return Faction.POLICE;
	}
	
}
