using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameCommunication;


public class HitAndRunGameMode : GameMode {
	
	public Area area;
	
	
	public override IEnumerable< MissionObjectiveData > GetGameModeMissionObjectivesData() {
//		KillCharacterScript killCharacterScript = new KillCharacterScript();
//		killCharacterScript.characterType = CharacterTargetMode.Type.TROOP_LEADER;
//		killCharacterScript.targetFaction = Faction.CRIMINAL;
//		killCharacterScript.winningFaction = 
		KillCharacterData killCharacterData = new KillCharacterData( Faction.POLICE, new CharacterTargetMode( CharacterTargetMode.Type.TROOP_LEADER, Faction.CRIMINAL ) );
		CharacterInAreaData characterInAreaData = new CharacterInAreaData( area.GetMissionData(), new CharacterTargetMode( CharacterTargetMode.Type.TROOP_LEADER, Faction.POLICE ), Faction.POLICE );
		
		killCharacterData.objectivesToActivate.Add( characterInAreaData );
		// TODO PETER o que a linha abaixo fazia???
//		characterInAreaData.isActive = false;
		
		yield return killCharacterData;
		yield return characterInAreaData;
	}
}

