#if UNITY_EDITOR

using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

using GameCommunication;
using GameController;
using Utils;
using SharpUnit;

using Attribute = GameCommunication.Attribute;


/// <summary>
/// Esta classe permite a edição em tempo de execução dos parâmetros gerais de game design, como velocidades máximas
/// e mínimas dos personagens, consumo de AP para cada tipo de ação, etc.
/// Para adicionar novos campos ao editor, basta criar as propriedades get/set correspondentes, com classes que herdam
/// de Editable.
/// </summary>
public class CharacterEditor : GenericEditor {
	
	private CharacterInfoData characterData;
	
	private int characterWorldID;
	
	
	public override string GetTitle() {
		return "Character Editor " + characterData.Name;
	}
	
	
	public int CharacterWorldID {
		get { return characterWorldID; }
		set { characterWorldID = value; }
	}
	
	
	public CharacterInfoData CharacterData {
		get { return characterData; }
		set { 
			characterData = value; 
			data = characterData;
		
			if ( Application.isPlaying ) {
				GameManager gm = GameManager.GetInstance();
				if ( gm != null )
					gm.SendRequest( new SetCharacterInfoRequest( gm.GetLocalPlayer(), characterWorldID, characterData ) );
			}
		}
	}
	
	
}

#endif