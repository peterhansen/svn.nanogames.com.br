using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Utils;

using GameCommunication;
using GameController;


#if UNITY_EDITOR
using UnityEditor;

public class HollowDescriptor : MonoBehaviour {
	
	/// <summary>
	/// Tipos de parede, no que diz respeito à configuração de portas e janelas.
	/// </summary>
	public enum WallType {
		Regular,
		Door,
		Window,
		DoorAndWindows,
		BigWindow,
		FullWindow,
		FullWindowColumn,
		SwivelWindow,
		BigMiddleWindow,
		MiddleWindow,
		MiddleDoor
	}
	
	/// <summary>
	/// Tipos de portas e janelas que podem estar presentes numa parede.
	/// </summary>
	private enum HollowType {
		Door,
		WindowCenter,
		WindowLeft,
		WindowRight,
		BigWindow,
		FullWindow,
		FullWindowColumn,
		SwivelWindow
	}
	
	/// <summary>
	/// Mapeia os nomes dos diretórios de paredes aos seus tipos de configuração porta/janela.
	/// </summary>
	private static Dictionary< string, WallType > wallTypeNames = new Dictionary<string, WallType>() {
		{ "paredeporta", WallType.Door },
		{ "paredegrande", WallType.DoorAndWindows },
		{ "paredejanelao", WallType.FullWindow },
		{ "coluna_janelao", WallType.FullWindowColumn },
		{ "paredejanela", WallType.Window },
		{ "paredejanelagrande", WallType.BigWindow },
		{ "paredebasculante", WallType.SwivelWindow },
		{ "paredejanelagrandemeio", WallType.BigMiddleWindow },
		{ "paredejanelameio", WallType.MiddleWindow },
		{ "paredeportameio", WallType.MiddleDoor },
		{ "", WallType.Regular }
	};
	
	
	private static readonly Vector3 size = new Vector3( Common.LEVEL_SECTOR_WIDTH, 2.0f, Common.LEVEL_SECTOR_WIDTH );
	
	private static readonly Vector3 position = new Vector3( 0, -0.25f, 0 );
	
	public WallType wallType;
	
	
	public Portal Generate() {
		switch( wallType ) {
			case WallType.Door:
			case WallType.DoorAndWindows:
			case WallType.MiddleDoor:
				Bounds portalBounds = new Bounds( transform.position + position, size );
				return new Portal( portalBounds );
				
			default:
				return null;
		}
	}
	
	
	public void SetWallTypeName( string typename ) {
		wallType = wallTypeNames[ typename ];
	}
	
	
	public IEnumerable< Box > GenerateHoleBoxes() {
		return EnumerateBoxes( wallType );
	}
	
	
	//public IEnumerable< ConvexHull > GenerateConvexHulls( Box box ) {
	//	return EnumerateConvexHulls( wallType, box );	
	//}
	
	
	private IEnumerable< Box > EnumerateBoxes( WallType type ) {
		switch ( type ) {
			case WallType.BigWindow:
			case WallType.BigMiddleWindow:
				yield return GetBox( HollowType.BigWindow );
			break;
			
			case WallType.Door:
			case WallType.MiddleDoor:
				yield return GetBox( HollowType.Door );
			break;
			
			case WallType.DoorAndWindows:
				yield return GetBox( HollowType.Door );
				yield return GetBox( HollowType.WindowLeft );
				yield return GetBox( HollowType.WindowRight );
			break;
			
			case WallType.FullWindow:
				yield return GetBox( HollowType.FullWindow );
			break;
			
			case WallType.FullWindowColumn:
				yield return GetBox( HollowType.FullWindowColumn );
			break;
			
			case WallType.Window:
			case WallType.MiddleWindow:
				yield return GetBox( HollowType.WindowCenter );
			break;
			
			case WallType.SwivelWindow:
				yield return GetBox( HollowType.SwivelWindow );
			break;
		}
		
		yield break;
	}
	
	
	/*private IEnumerable< ConvexHull > EnumerateConvexHulls( WallType type, Box box ) {
		float dx;
		
		switch ( type ) {
			case WallType.BigWindow:
			case WallType.BigMiddleWindow:
			case WallType.Door:
			case WallType.MiddleDoor:
			case WallType.Window:
			case WallType.MiddleWindow:
				dx = box.GetDX() * 0.15f;
			
				GameTransform leftHullTransform = new GameTransform();
				leftHullTransform.position = new GameVector( -dx, 0.0f, 0.0f );
				leftHullTransform.scale = new GameVector( dx * 2.0f, Common.LEVEL_LAYER_HEIGHT, ObjectSnapper.DEFAULT_WALL_THICKNESS );
				ConvexHull leftHull = new ConvexHull( box, leftHullTransform );
				yield return leftHull;
			
				GameTransform rightHullTransform = new GameTransform();
				rightHullTransform.position = new GameVector( dx, 0.0f, 0.0f );
				rightHullTransform.scale = new GameVector( dx * 2.0f, Common.LEVEL_LAYER_HEIGHT, ObjectSnapper.DEFAULT_WALL_THICKNESS );
				ConvexHull rightHull = new ConvexHull( box, rightHullTransform );
				yield return rightHull;
			break;
			
			case WallType.DoorAndWindows:
				dx = box.GetDX() * 0.1f;
				for( int i = -3; i <= 3; i+=2 ) {
					GameTransform hullTransform = new GameTransform();
					hullTransform.position = new GameVector( i * dx, 0.0f, 0.0f );
					hullTransform.scale = new GameVector( dx * 2.0f, Common.LEVEL_LAYER_HEIGHT, ObjectSnapper.DEFAULT_WALL_THICKNESS );
					ConvexHull hull = new ConvexHull( box, hullTransform );
					yield return hull;
				}	
			break;
			
			case WallType.SwivelWindow:
				ConvexHull swivelHull = new ConvexHull( box, box.GetGameTransform() );
				swivelHull.Position = new GameVector();
				yield return swivelHull;
			break;
		}
		yield break;
	}*/
	
	
	private Box GetBox( HollowType t ) {
		GameTransform gt = new GameTransform();
		switch ( t ) {
			case HollowType.Door:
				gt.position.Set( 0, -0.25f, 0 );
				gt.scale.Set( 0.8f, 2.0f, ObjectSnapper.DEFAULT_WALL_THICKNESS );
			break;
			
			case HollowType.WindowCenter:
				gt.position.Set( 0, 0.315f, 0 );
				gt.scale.Set( 0.618f, 0.869f, ObjectSnapper.DEFAULT_WALL_THICKNESS );			
			break;
			
			case HollowType.WindowLeft:
				gt.position.Set( 1.475f, 0.39f, 0 );
				gt.scale.Set( 0.490f, 0.720f, ObjectSnapper.DEFAULT_WALL_THICKNESS );			
			break;
			
			case HollowType.WindowRight:
				gt.position.Set( -1.475f, 0.315f, 0 );
				gt.scale.Set( 0.618f, 0.869f, ObjectSnapper.DEFAULT_WALL_THICKNESS );
			break;
			
			case HollowType.BigWindow:
				gt.position.Set( 0, 0.315f, 0 );
				gt.scale.Set( 1.092f, 0.869f, ObjectSnapper.DEFAULT_WALL_THICKNESS );			
			break;
			
			case HollowType.FullWindow:
				gt.position.Set( 0, 0.17f, 0 );
				gt.scale.Set( ObjectSnapper.DEFAULT_WALL_WIDTH, 1.06f, ObjectSnapper.DEFAULT_WALL_THICKNESS );			
			break;
			
			case HollowType.FullWindowColumn:
				gt.position.Set( 0, 0.17f, 0 );
				gt.scale.Set( ObjectSnapper.DEFAULT_WALL_THICKNESS, 1.06f, ObjectSnapper.DEFAULT_WALL_THICKNESS );
			break;
			
			case HollowType.SwivelWindow:
				gt.position.Set( 0, 0.825754f, 0 );
				gt.scale.Set( 1.09f, 0.345f, ObjectSnapper.DEFAULT_WALL_THICKNESS );
			break;
		}
		
		// trocando a escala da caixa, em caso de rotacao no eixo Y
		if( NanoMath.Equals( transform.eulerAngles.y, 90.0f ) || NanoMath.Equals( transform.eulerAngles.y, 270.0f ) ) {
			float gtScaleZ = gt.scale.z;
			gt.scale.z = gt.scale.x;
			gt.scale.x = gtScaleZ;
		}
		return new Box( gt );
	}	
}

#endif