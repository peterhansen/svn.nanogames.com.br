using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


public class SpawnAreaNPC : SpawnArea {
	
	public SpawnAreaNPC() : base() {
	}
	
	
	public override Faction GetFaction() {
		return Faction.NEUTRAL;
	}
	
}
