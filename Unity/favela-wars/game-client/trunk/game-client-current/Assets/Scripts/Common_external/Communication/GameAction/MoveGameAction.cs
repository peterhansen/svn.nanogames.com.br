using System.Collections;

using Utils;


namespace GameCommunication {
	
	public class MoveGameAction : GameAction {
		
		public float nearDistance;
		
		public float farDistance;
		
		public Stance currentStance;
		
		public MoveGameAction( float nearDistance, float farDistance, Stance currentStance ) : base( ActionType.MOVE ) {
			this.nearDistance = nearDistance;
			this.farDistance = farDistance;
			this.currentStance = currentStance;
		}
	}
}