using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {

	[ Serializable ]
	public class CharacterActionInfo {
		public float percentualHP;
		public float percentualAP;
		
		public CharacterActionInfo(float percentualHP , float percentualAP){
			this.percentualHP = percentualHP;
			this.percentualAP = percentualAP;
		}
		
		public override bool Equals( object obj ) {
			return Equals( obj as CharacterActionInfo );
		}
		
		
		public bool Equals( CharacterActionInfo other ) {
			return other != null && percentualHP == other.percentualHP && percentualAP == other.percentualAP;
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
	}
}

