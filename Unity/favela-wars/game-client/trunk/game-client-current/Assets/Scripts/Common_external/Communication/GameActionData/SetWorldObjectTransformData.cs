using System;


namespace GameCommunication {

	
	[Serializable]
	public class SetWorldObjectTransformData : GameActionData {
		
		public int worldObjectID;
		public GameTransform transform = new GameTransform();
		
		public SetWorldObjectTransformData( int worldObjectID, GameTransform transform ) {//GameVector position, GameVector angle, GameVector scale ) {
			this.worldObjectID = worldObjectID;
			this.transform.Set( transform );
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as SetWorldObjectTransformData );
		}
		
		
		public bool Equals( SetWorldObjectTransformData data ) {
			if ( data == null )
	            return false;
    
			return worldObjectID == data.worldObjectID &&
					transform.position.Equals( data.transform.position ) && 
					transform.direction.Equals( data.transform.direction ) && 
					transform.scale.Equals( data.transform.scale );
		}
		
		
		public override int GetHashCode() {
        	return worldObjectID; // TODO implementar GetHashCode() decente
    	}
	}
}

