using System;
using System.Collections.Generic;


namespace GameCommunication {
	
	[ Serializable ]
	public class MoveNPCActionData : GameActionData {
		public int playerID;
		public List< NPCMovement > npcMovements = new List< NPCMovement >();
	}
}

