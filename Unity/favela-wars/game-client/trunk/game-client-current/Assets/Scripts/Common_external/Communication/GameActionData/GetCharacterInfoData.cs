using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {

	[Serializable]
	public class GetCharacterInfoData : GameActionData {
		
		public CharacterInfoData data;
		
		public bool fullInfo;
		
		public GetCharacterInfoData( CharacterInfoData data, bool fullInfo ) {
			this.data = data;
			this.fullInfo = fullInfo;
		}
		
		
	}
}

