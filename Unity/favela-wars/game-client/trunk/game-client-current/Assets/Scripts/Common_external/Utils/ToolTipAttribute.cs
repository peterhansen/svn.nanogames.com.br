using System;
using System.Collections;

[AttributeUsage( AttributeTargets.Property )]
public class ToolTipAttribute : Attribute {
	
	private string text;
	
    
	public ToolTipAttribute( string text ) {
		ToolTipText = text;
	}
	
	
	public string ToolTipText {
		get { return text; }
		set { text = value; }
	}
}