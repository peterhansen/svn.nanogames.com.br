using System;

namespace GameCommunication {
	
	[Serializable]
	public class AssetBundleData {
		
		
		public string name;
		
		public string path;
		public int version;
		
		public AssetBundleData() {
		}
		
		
		public AssetBundleData( string name, string path, int version ) {
			this.name = name;
			this.path = path;
			this.version = version;
		}
	}
}

