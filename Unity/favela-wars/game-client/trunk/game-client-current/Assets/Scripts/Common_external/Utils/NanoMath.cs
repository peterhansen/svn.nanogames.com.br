using System;
using GameCommunication;


namespace Utils {
	
	public class NanoMath {
		
		public const float EPSILON = 0.0001f;
		
		private static Random r = new Random();

		
		public static float Random( float min, float max ) {
			return min + ( float ) ( r.NextDouble() * ( max - min ) );
		}
		
		
		public static float Random( float max ) {
			return Random( 0, max );
		}
		
		
		public static bool Equals( float a, float b ) {
			return (float) ( Math.Abs( a - b ) ) < EPSILON;
		}
		
		
		public static bool GreaterThan( float lhs, float rhs ) {
			return !Equals( lhs, rhs ) && lhs > rhs;
		}
		
		
		public static bool LessThan( float lhs, float rhs ) {
			return !Equals( lhs, rhs ) && lhs < rhs;
		}
		
		
		public static bool GreaterEquals( float lhs, float rhs ) {
			return Equals( lhs, rhs ) || lhs > rhs;
		}
		
		
		public static bool LessEquals( float lhs, float rhs ) {
			return Equals( lhs, rhs ) || lhs < rhs;
		}
		
		
		public static bool Equals( float a, float b, float epsilon ) {
			return (float) ( Math.Abs( a - b ) ) < epsilon;
		}		
		
		
		/// <summary>
		/// Lerp the specified progress, min and max.
		/// </summary>
		/// <param name='progress'>
		/// Progresso entre 0 e 1.
		/// </param>
		/// <param name='min'>
		/// 
		/// </param>
		/// <param name='max'>
		/// 
		/// </param>
		public static float Lerp( float progress, float min, float max ) {
			return min + ( ( max - min ) * Clamp( progress, 0, 1 ) );
		}
		
		
		public static float Clamp( float value, float min, float max ) {
			return ( value <= min ) ? min : ( value >= max ? max : value );
		}
		
		
		public static int Clamp( int value, int min, int max ) {
			return ( value <= min ) ? min : ( value >= max ? max : value );
		}
		
		/// <summary>
		/// Calcula em que porcentagem da faixa (min/max) o valor se situa.
		/// </summary>
		public static float PercentInRange( float value, float min, float max ) {
			return ( value - min ) / ( max - min );
		}
		
		
		public static bool IsInLine( GameVector origin, GameVector point, GameVector destiny ) {
			GameVector bigger = destiny.Sub( origin );
			GameVector candidate = point.Sub( origin );
			
			float rel = ( candidate.Norm() / bigger.Norm() );
			
			if ( rel > 1.0f )
				return false;
			
			bigger.Normalize();
			candidate.Normalize();
			
			return bigger.Equals( candidate, 0.01f );
		}
	}
}

