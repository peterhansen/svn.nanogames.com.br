using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	[Serializable]
	public class GetCharacterActionsData : GameActionData {
		
		public int characterId;
		
		public GameAction actionTree;
		
		public CharacterActionInfo info;
		
		/// <summary>
		/// Ids no mundo de inimigos vistos pelo personagem (a lista pode estar vazia).
		/// </summary>
		public readonly List< int > visibleEnemiesIDs = new List< int >();

		
		public GetCharacterActionsData( int characterId, GameAction actionTree ) {
			this.characterId = characterId;
			this.actionTree = actionTree;
		}
		
		public GetCharacterActionsData( int characterId, GameAction actionTree , CharacterActionInfo info) {
			this.characterId = characterId;
			this.actionTree = actionTree;
			this.info = info;
		}
		
		public GetCharacterActionsData( int characterId, GameAction actionTree , float hp, float ap) {
			this.characterId = characterId;
			this.actionTree = actionTree;
			this.info = new CharacterActionInfo ( hp, ap );
		}
		
		public override bool Equals( object obj ) {
			return Equals( obj as GetCharacterActionsData );
		}
		
		
		public bool Equals( GetCharacterActionsData other ) {
			return other != null && characterId == other.characterId && actionTree.Equals( other.actionTree );
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
	}
}

