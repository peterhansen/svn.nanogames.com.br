using System;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[Serializable]
	public class ShowMessageData : GameActionData {
		
		public string message;
		
		public int character;
		
		public ShowMessageData( int id, string message ) {
			this.message = message;
			this.character = id;
		}
		
		public String GetMessage() {
			return message;
		}
	}
}

