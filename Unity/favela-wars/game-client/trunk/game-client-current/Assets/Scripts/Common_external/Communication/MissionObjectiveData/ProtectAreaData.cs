using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	[ Serializable ]
	public class ProtectAreaData : MissionObjectiveData {
		
		public AreaMissionData area;
		
		public int numberOfTurns;
		
		public int policeTurnsInArea = 0;
		
		public int criminalTurnsInArea = 0;
		
		
		public ProtectAreaData() {
		}
		
		
		public ProtectAreaData( AreaMissionData area, int numberOfTurns ) {
			this.area = area;
			this.numberOfTurns = numberOfTurns;
		}
		
	}
	
}

