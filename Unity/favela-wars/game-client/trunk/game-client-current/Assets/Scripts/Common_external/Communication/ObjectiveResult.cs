using System;

using Utils;

namespace GameCommunication {
	
	[Serializable]
	public class ObjectiveResult {
		
		//public static readonly Null = new ObjectiveResult( false, Faction.NONE );
		
		public bool missionIsOver;
		public Faction winningFaction;
		
		public ObjectiveResult() {
			this.missionIsOver = false;
			this.winningFaction = Faction.NONE;
		}
		
		
		public ObjectiveResult( bool isDone, Faction winningFaction ) {
			this.missionIsOver = isDone;
			this.winningFaction = winningFaction;
		}
	}
}

