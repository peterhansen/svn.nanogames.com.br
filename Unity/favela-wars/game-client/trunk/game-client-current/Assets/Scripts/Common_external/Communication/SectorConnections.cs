using System;
using System.Collections.Generic;

using Utils;

namespace GameCommunication {

	[Serializable]
	public class SectorConnections {
		
		protected Dictionary< Sector, BoxSet > sectors = new Dictionary< Sector, BoxSet >();
		
		protected Dictionary< Box, SectorSet > boxes = new Dictionary< Box, SectorSet >();
		
		
		public void AddSector( Sector sector ) {
			if( !sectors.ContainsKey( sector ) ) {
				sectors[ sector ] = new BoxSet();
			}
		}
		
		
		public void AddBox( Box box ) {
			if( !boxes.ContainsKey( box ) ) {
				boxes[ box ] = new SectorSet();
			}
		}
		
		
		public bool HasSector( Sector sector ) {
			return sectors.ContainsKey( sector );
		}
		
		
		public IEnumerable< Sector > GetAllSectors() {
			foreach( Sector s in sectors.Keys ) {
				yield return s;
			}
		}
		
		
		
		public Sector GetSector( int i, int j, int k ) {
			return GetSector( new Sector( i, j, k ) );
		}
		
		
		public Sector GetSector( Sector sector ) {
			if ( sectors.ContainsKey( sector ) )
				return sector;
			
			return Sector.NULL;
		}
		
			
		public void Disconnect( Box box ) {
			if( !boxes.ContainsKey( box ) ) {
				throw new ArgumentException();
			}
			
			foreach( Sector sector in boxes[ box ].GetSectors() ) {
				sectors[ sector ].Remove( box );
			}
			boxes[ box ] = new SectorSet();
		}
		
		
		public IEnumerable< Box > GetSectorBoxes( Sector sector ) {
			if( sectors.ContainsKey( sector ) ) {
				foreach ( Box b in sectors[ sector ].GetBoxes() )
					yield return b;
			} else {
				yield break;
			}
		}
		
		
		public IEnumerable< Sector > GetBoxSectors( Box box ) {
			if ( boxes.ContainsKey( box ) ) {
				foreach ( Sector s in boxes[ box ].GetSectors() )
					yield return s;
			} else  {
				yield break;
			}
		}
		
		
		public void Connect( Sector sector, Box box ) {
			if( !sectors.ContainsKey( sector ) ) {
				if ( !sector.IsValid() )
					return;
				
				AddSector( sector );
			}
			
			if( !boxes.ContainsKey( box ) ) {
				throw new ArgumentException( box + " box not found " );
			}
			
			sectors[ sector ].Add( box );
			boxes[ box ].Add( sector );
			
			// HACK
			//Debugger.Log( "Conectando " + sector + " com caixa " + box.GetWorldObjectID() );
		}
		
	}
	
	[Serializable]
	public class BoxSet {
		
		protected Dictionary< Box, int > boxes = new Dictionary< Box, int >();
		
		public IEnumerable< Box > GetBoxes() {
			return boxes.Keys;
		}
		
		
		public void Add( Box box ) {
			boxes[ box ] = 1;
		}
		
		
		public void Remove( Box box ) {
			boxes.Remove( box );
		}
		
	}
	
	
	[Serializable]
	public class SectorSet {
		
		//protected Utils.HashSet<Sector> sectors = new Utils.HashSet<Sector>();
		protected Dictionary< Sector, int > sectors = new Dictionary< Sector, int >();
		
		public IEnumerable< Sector > GetSectors() {
			return sectors.Keys;
		}
		
		
		public void Add( Sector sector ) {
			sectors[ sector ] = 1;
		}
		
	}
}

