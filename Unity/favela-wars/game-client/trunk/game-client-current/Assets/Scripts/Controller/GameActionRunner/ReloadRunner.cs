using System;
using System.Collections.Generic;
using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class ReloadRunner : GameActionRunner {
		
		private ReloadData data;
		
		private byte currentShot;
		
	
		public ReloadRunner( ReloadData reloadData ) : base( false ) {
			this.data = reloadData;
		}
		
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.characterWorldID );
			
			if ( gameObject == null ) {
				Debugger.LogError( "ReloadRunner: reloader worldID not found: " + data.characterWorldID );
			} else {
			}
		}
		
	}
}

