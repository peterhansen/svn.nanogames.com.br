using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using GameCommunication;
using GameController;
using Utils;


public class IsoCameraScript : MonoBehaviour {
	
	/// <summary>
	/// Quantidade de posições anteriores do mouse usados para fazer o scroll cinético.
	/// http://www.nbilyk.com/examples/kineticScrollExample/srcview/index.html
	/// </summary>
	private const byte KINETIC_HISTORY_LENGTH = 10;
	
	public float kineticDecayRate = 0.92f;
	
	public int moveDecayPercent = 3;
	
	public enum CameraState {
		IDLE,
		PANNING,
		ROTATING,
		SNAPPING_TO_ANGLES,
		MOVING,
	}
	
//	public enum BlockState {
//		SCROLL = 1,
//		ROTATION = 2,
//		PANNING = 4,
//		TRANSLATION = 8
//	}
	
	protected Vector3 clickedWorldPos = new Vector3();
	
	protected Vector2 clickedScreenPos = new Vector2();
	
    private Vector2 velocity = new Vector2();
	
	private Vector3 speed = new Vector3();
	
	private const float MAX_VELOCITY = 500.0f;
	
	/// <summary>
	/// Distância mínima da câmera para a cena.
	/// </summary>
	public int minDistance = 1;
	
	/// <summary>
	/// Distância máxima da câmera para a cena.
	/// </summary>
	public int maxDistance = 90;
	
	/// <summary>
	/// Velocidade do zoom através do scroll do mouse.
	/// </summary>
	public int zoomScrollSpeed = 35;
	
	/// <summary>
	/// Multiplicador da rotação da cena com o mouse. Quanto maior o valor, mais sensível e rápida é a rotação.
	/// </summary>
	public float mouseRotationMultiplier = 0.5f;
	
	/// <summary>
	/// Variação mínima de ângulo a cada frame quando a câmera está girando automaticamente.
	/// </summary>
	public float minRotationStep = 0.01f;
	
	/// <summary>
	/// Variação mínima de ângulo a cada frame quando a câmera está girando automaticamente.
	/// </summary>
	public float maxRotationStep = 3.3f;
	
	/// <summary>
	/// Velocidade padrão de rotação da câmera.
	/// </summary>
	public float rotationSpeed = 0.15f;
	
	/// <summary>
	/// 
	/// </summary>
	public float translateMultiplier = 0.1f;
	
	public CameraState state = CameraState.IDLE;
	
	protected float initialAngle;
	
	protected int finalAngle = 45;
	
	public float cameraSpeed = 4.0f;
	
	public bool showControls = true;
	
	public bool userInputIsBlocked = false;
	
	//public int blockedInput = 0;
	
	private float finalZoom;
	
	public GUIStyle textStyle;
	
	protected Camera c;
	
	/// <summary>
	/// Porcentagem da tela em que a rolagem pela posição do mouse está ativa (1 = 100%).
	/// </summary>
	public float scrollBorderPercent = 0.1f;
	
	/// <summary>
	/// Velocidade máxima de scroll da cena ao usar as bordas de rolagem.
	/// </summary>
	public float maxBorderScrollSpeed = 1.0f;
	
	private Vector3 min;
	
	private Vector3 max;
	
	private const float farClipPlane = 600.0f;
	

	public void Start() {
		Prepare();
	}
	
	
	public void Prepare() {
		c = Camera.main;
		Vector3 angle = new Vector3( 35.264f, 45.0f, 0 );
		c.transform.eulerAngles = angle;
		
		VisibilityMap map = VisibilityMap.GetInstance();
		if ( map == null )
			return;
		
		// obtém o centro do mapa
		Vector3 center = ( map.minPoint + map.maxPoint ) * 0.5f;
		center.y = map.minPoint.y;
		
		// posiciona a câmera no local inicial
		Vector3 pos = center - c.transform.forward.normalized * farClipPlane * 0.5f;
		c.transform.position = pos;
		
		c.farClipPlane = farClipPlane;
		c.orthographic = true;
		c.nearClipPlane = 3;
		c.orthographicSize = maxDistance;
		finalZoom = minDistance + ( maxDistance - minDistance ) * 0.4f;
		recalculateLimits();
	}
	
	
	public void Update () {
		switch( state ) {
			case CameraState.IDLE:
				IdleFixedUpdate();
				UpdateKinetic();
				CheckBounds();
			break;
			
			case CameraState.PANNING:
				PanFixedUpdate();
				CheckBounds();
			break;
			
			case CameraState.ROTATING:
				RotateFixedUpdate();
				UpdateKinetic();
			break;
			
			case CameraState.SNAPPING_TO_ANGLES:
				SnappingToAnglesFixedUpdate();
				UpdateKinetic();
			break;
			
			case CameraState.MOVING:
				MovingUpdate();
				CheckBounds();
			break;
			
			default:
				CheckBounds();
			break;
		}
	}
	
	
	private void UpdateKinetic() {
		if ( speed.x != 0 && speed.z != 0 ) {
			// terminando movimento de pan (cineticamente)
			c.transform.position += speed;
			// fator de decaimento do cinetico
			speed *= NanoMath.Clamp( kineticDecayRate, 0, 1 );
		}
	}
	
	
	public static IsoCameraScript GetInstance() {
		// TODO singleton decente da câmera
		GameObject go = GameObject.FindGameObjectWithTag( "MainCamera" );
		return go.GetComponent< IsoCameraScript >();
	}
	
	
	public void MoveTo( Vector3 target ) {
		Vector3 v = c.WorldToViewportPoint( target );
		v.z = 0;
		
		Vector3 p = c.ViewportToWorldPoint( v );
		Vector3 cameraCenterAtWorld = c.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, 0 ) );
		Vector3 d = GetDiffY0( p - cameraCenterAtWorld );
		clickedWorldPos = c.transform.position + d;
		SetState( CameraState.MOVING );
	}
	
	
	public void FocusOn( GameObject gameObject ) {
		GameManager gameManager = GameManager.GetInstance();
		WorldManager worldManager = WorldManager.GetInstance();
		
		if( gameObject == null )
			return;
		
		// TODO: apenas dos personagens eu tiro informação de camadas?
		if( gameObject.CompareTag( Common.Tags.CHARACTER ) ) {
			gameManager.LastSelectedSoldier = gameObject;
			BuildingInfo.SetLayerByCharacter( gameObject );
		}
			
		IsoCameraScript.GetInstance().MoveTo( gameObject.transform.position );
	}

	
	protected void IdleFixedUpdate() {
		//if( ( ( blockedInput & (int)BlockState.SCROLL ) ) != (int)BlockState.SCROLL  )
		CheckWheelScroll();
		
		//if( ( ( blockedInput & (int)BlockState.TRANSLATION ) ) == (int)BlockState.TRANSLATION  )
		if(userInputIsBlocked)
			return;		
		
		// verificando scroll da borda da fase
		//CheckBorderScroll();
		
		if ( Input.GetMouseButtonDown( Common.MOUSE_BUTTON_MIDDLE ) ) {
			clickedWorldPos = c.ScreenToWorldPoint( Input.mousePosition );
			Vector3 cameraCenterAtWorld = c.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, 0 ) );
			
			Vector3 d = GetDiffY0( clickedWorldPos - cameraCenterAtWorld );
			
			clickedScreenPos = c.transform.position + d;
			SetState( CameraState.PANNING );
		} else if( Input.GetMouseButtonDown( Common.MOUSE_BUTTON_RIGHT ) ) {
			clickedWorldPos = c.ScreenToWorldPoint( Input.mousePosition );
			clickedScreenPos = Input.mousePosition;
			SetState( CameraState.ROTATING );
		}
	}
	
	
	private void recalculateLimits() {
		VisibilityMap map = VisibilityMap.GetInstance();
		if ( map == null )
			return;
		
		float y = map.minPoint.y;
		
		Vector3[] points = new Vector3[] {
			new Vector3( map.minPoint.x, y, map.minPoint.z ),
			new Vector3( map.minPoint.x, y, map.maxPoint.z ),
			new Vector3( map.maxPoint.x, y, map.minPoint.z ),
			new Vector3( map.maxPoint.x, y, map.maxPoint.z ) 
		};
		
		min = new Vector3( float.MaxValue, y, float.MaxValue );
		max = new Vector3( float.MinValue, y, float.MinValue );
		
		foreach ( Vector3 p in points ) {
			// obtém a posição do mundo no viewport
			Vector3 temp = c.WorldToScreenPoint( p );
			temp.z = 0;
			
			// converte para a posição que a câmera estaria ao centralizar este ponto
			Vector3 v = c.ScreenToWorldPoint( temp );
			// zera a variação em y retornada pelo cálculo da câmera, evitando problemas com z-near
			v += GetDiffY0( new Vector3( 0, v.y - c.transform.position.y, 0 ) );
			
			min.x = Math.Min( min.x, v.x );
			min.z = Math.Min( min.z, v.z );

			max.x = Math.Max( max.x, v.x );
			max.z = Math.Max( max.z, v.z );
		}
//		Debugger.Log( "MIN: " + min + ", MAX: "+ max );
	}
	
	
	private void CheckBounds() {
		if ( min.x == 0 && min.z == 0 ) {
			recalculateLimits();
		}
		
		Vector3 pos = c.transform.position;
		Vector3 finalPos = new Vector3( NanoMath.Clamp( pos.x, min.x, max.x ), pos.y, NanoMath.Clamp( pos.z, min.z, max.z ) );
		
		c.transform.position = finalPos;
	}
	
	
	private void CheckWheelScroll() {
		// tratando zoom da câmera baseado na wheel do mouse
		float diff = Input.GetAxis( "Mouse ScrollWheel" ) * -Mathf.Abs( zoomScrollSpeed );
		finalZoom = Mathf.Clamp( finalZoom + diff, minDistance, maxDistance );
		float speed = finalZoom - c.orthographicSize;
		// suaviza a animação do zoom
        c.orthographicSize += speed * 0.15f;
	}
	
	
	/// <summary>
	/// Obtém a translação equivalente a <c>diff</c>, porém utilizando somente as coordenadas x e z (d.y = 0);
	/// </summary>
	private Vector3 GetDiffY0( Vector3 diff ) {
		float sinX = ( float ) Math.Sin( c.transform.eulerAngles.y * Math.PI / 180 );
		float cosX = ( float ) Math.Cos( c.transform.eulerAngles.y * Math.PI / 180 );
		
		return new Vector3( diff.x + ( sinX * diff.y ), 0, diff.z + ( cosX * diff.y ) );
	}
	
	
	protected void PanFixedUpdate() {
		Vector3 releasedPoint = c.ScreenToWorldPoint( Input.mousePosition );
		speed = GetDiffY0( clickedWorldPos - releasedPoint );
		c.transform.position += speed;
				
		if ( !Input.GetMouseButton( Common.MOUSE_BUTTON_MIDDLE ) ) {
			SetState( CameraState.IDLE );
		}
	}
	
	
	protected void MovingUpdate() {
		CheckWheelScroll();
		
		Vector3 cameraCenterAtWorld = c.ViewportToWorldPoint( new Vector3( 0.5f, 0.5f, 0 ) );
		Vector3 distance = clickedWorldPos - cameraCenterAtWorld;
		speed = distance * NanoMath.Clamp( moveDecayPercent / 100.0f, 0, 0.9f );
		
		if ( distance.magnitude > 1.15f ) {
			c.transform.position += speed;
		} else {
			SetState( CameraState.IDLE );
		}
	}
	

	protected void RotateFixedUpdate() {
		RaycastHit hit = new RaycastHit();
		Ray ray = c.ViewportPointToRay( new Vector3( 0.5f, 0.5f, 0 ) );
		
		if( Input.GetMouseButton( Common.MOUSE_BUTTON_RIGHT ) ) {
			if ( Physics.Raycast( ray, out hit ) ) {
				c.transform.RotateAround( hit.point, Vector3.up, ( Input.mousePosition.x - clickedScreenPos.x ) * mouseRotationMultiplier );
				
				clickedScreenPos.Set( Input.mousePosition.x, Input.mousePosition.y );
				clickedWorldPos = hit.point;
			}
		} else {
			if ( Math.Abs( c.transform.eulerAngles.y - finalAngle ) > 1.0f ) {	
				// encontra o ângulo mais próximo (45, 135, 225 ou 315)
				initialAngle = c.transform.eulerAngles.y;
				float mod = initialAngle % 90;
				int nextMultiple = ( int ) ( ( initialAngle + 45 ) / 90 ) * 90;
				finalAngle = ( int ) ( nextMultiple + ( mod < 45 ? 45 : -45 ) ) % 360;
				
				SetState( CameraState.SNAPPING_TO_ANGLES );
			} else {
				SetState( CameraState.IDLE );
				recalculateLimits();
			}
		}
	}
	

	protected void SnappingToAnglesFixedUpdate() {
		float normalFinalAngle = 360 + ( finalAngle % 360 );
		float normalAngle = ( normalFinalAngle - c.transform.eulerAngles.y < 180 ) ? c.transform.eulerAngles.y : 360 + c.transform.eulerAngles.y;
		float angle = Mathf.Lerp( normalAngle, normalFinalAngle, rotationSpeed );
		
		float diff = Mathf.Clamp( Mathf.Abs( angle - normalAngle ), minRotationStep, maxRotationStep );
		
		c.transform.RotateAround( clickedWorldPos, Vector3.up, angle >= normalAngle ? diff : -diff );
		
		normalAngle = 360 + ( c.transform.eulerAngles.y % 360 );
		
		if( Mathf.Abs( ( normalAngle - normalFinalAngle ) % 360 ) <= 1.0f ) {
			SetState( CameraState.IDLE );
			recalculateLimits();
		}
	}
	
	
	protected void Stop() {
		velocity.Set( 0, 0 );
	}
	
	
	public void TranslateCamera( float dx, float dy ) {
		c.transform.Translate( dx, dy, 0, c.transform );
	}
	
	
	protected Vector3 PickMouse() {
		RaycastHit hit = new RaycastHit();
		Ray ray = c.ScreenPointToRay( Input.mousePosition );
		Physics.Raycast( ray, out hit );
		return hit.point;
	}
	
	
	protected Vector3 PickCameraCenter() {
		RaycastHit hit = new RaycastHit();
		Ray ray = c.ViewportPointToRay( new Vector3( 0.5f, 0.5f, 0 ) );
		Physics.Raycast( ray, out hit );
		return hit.point;
	}
	
	public Vector3 PickCameraRightCorner() {
		Vector3 v = c.ViewportToWorldPoint( new Vector3( 0.9f, 0.15f, 0 ) );
		v += c.transform.forward.normalized * 20;
		return v;
	}
	
	
	public void RotateLeft() {
		if( IsAvailable() ) {
			finalAngle = ( int ) ( c.transform.eulerAngles.y + 90 );
			clickedWorldPos = PickCameraCenter();
			SetState( CameraState.SNAPPING_TO_ANGLES );
		}
	}
	
	public void RotateRight() {
		if( IsAvailable() ) {
			finalAngle = ( int ) ( c.transform.eulerAngles.y - 90 );
			clickedWorldPos = PickCameraCenter();
			SetState( CameraState.SNAPPING_TO_ANGLES );
		}
	}
	
	
	public bool IsAvailable() {
		return state == CameraState.IDLE && !Input.GetMouseButton( Common.MOUSE_BUTTON_RIGHT );
	}
	
	
	/// <summary>
	/// Verifica se o mouse está na área de scroll das bordas da tela, e faz o scroll caso positivo.
	/// </summary>
	private void CheckBorderScroll() {
		// não faz scroll pelas bordas enquanto o scroll cinético ou rotação estão ativos
		if ( !IsAvailable() )
			return;
		
		float x = Input.mousePosition.x;
		float y = Input.mousePosition.y;
		
		// trata situações em que o ponteiro do mouse está fora da tela (normalmente, no editor da Unity)
		if ( x < 0 || x > c.GetScreenWidth() || y < 0 || y > c.GetScreenHeight() )
			return;
		
		float limitLeft = c.GetScreenWidth() * scrollBorderPercent;
		float limitRight = c.GetScreenWidth() - limitLeft;
		float diffX = 0;
		
		float limitTop = c.GetScreenHeight() * scrollBorderPercent;
		float limitBottom = c.GetScreenHeight() - limitTop;
		float diffY = 0;
		
		bool isInTop = y > limitBottom;
		
		if ( ( x < limitLeft && !isInTop ) || x > limitRight ) {
			float percent = Mathf.Min( Mathf.Abs( limitLeft - x ), Mathf.Abs( x - limitRight ) ) / limitLeft;
			diffX = x < limitLeft ? -percent : percent * maxBorderScrollSpeed;
		}
		
		// TODO: por enquanto, estamos assumindo que a barra de ferramentas da câmera
		// ocupa 40% da parte superior da tela.
		if ( y < limitTop || ( isInTop && x > c.GetScreenWidth() * 0.4f ) ) {
			float percent = Mathf.Min( Mathf.Abs( limitTop - y ), Mathf.Abs( y - limitBottom ) ) / limitTop;
			diffY = y < limitTop ? -percent : percent * maxBorderScrollSpeed;
		}		
		
		TranslateCamera( diffX, diffY );
	}
	
	
	protected void SetState( CameraState state ) {
		this.state = state;
	
		switch( state ) {
			case CameraState.IDLE:
//			Debugger.Log( "Camera is Idle" );
			break;
			
			case CameraState.PANNING:
//			Debugger.Log( "Camera is Panning" );
			break;
		
			case CameraState.ROTATING:
//			Debugger.Log( "Camera is Rotating" );
			break;
			
			case CameraState.SNAPPING_TO_ANGLES:
//			Debugger.Log( "Camera is Snapping to Angles" );
			break;
			
			case CameraState.MOVING:
			break;
		}
	}
	
	
}
