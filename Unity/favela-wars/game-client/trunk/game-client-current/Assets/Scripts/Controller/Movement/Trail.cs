using System.Collections.Generic;

using UnityEngine;

using Utils;


public class Trail : MonoBehaviour {
	
	public bool glows;
	
	public const float GLOW_TIME = 0.6f;
	
	public float glowMaxDistance;
	
	public float glowSpeed;
	
	protected float length = 0.0f;
	public float Length {
		get { return length; }
	}
	
	protected List< Vector3 > points = new List< Vector3 >();
	
	public List< Vector3 > GetPoints() {
		return points;
	}
	
	public void SetPoints( List< Vector3 > points ) {
		this.points = points;
		length = 0.0f;
		
		for( int i = 0; i < points.Count - 1; i++ ) {
			length += ( points[ i + 1 ] - points[ i ] ).magnitude;
		}
	}
		
	protected float currDistance = 0.0f;
		
	// Update is called once per frame
	void Update () {
		currDistance += Time.deltaTime * glowSpeed;
		if( currDistance > glowMaxDistance ) {
			currDistance -= glowMaxDistance;	
		}
		
		MeshFilter meshFilter = GetComponent< MeshFilter >();
		meshFilter.renderer.material.SetFloat( "_GlowTime", currDistance );
	}
}
