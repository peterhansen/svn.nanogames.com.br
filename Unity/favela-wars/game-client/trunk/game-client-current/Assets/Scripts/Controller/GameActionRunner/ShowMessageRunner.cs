using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameCommunication;
using Utils;

namespace GameController {
	
	public class ShowMessageRunner : GameActionRunner {
		
		private ShowMessageData data;
	
		public ShowMessageRunner( ShowMessageData showMessageData ) : base( false ) {
			this.data = showMessageData;
		}
		
		protected override void Execute() {			
			ShowMessageInteractionController interactionController = new ShowMessageInteractionController( GUIManager.GetInteractionController(), data );
			Debugger.Log( data.message );
			GUIManager.SetInteractionController( interactionController );
		}	
	}
}


