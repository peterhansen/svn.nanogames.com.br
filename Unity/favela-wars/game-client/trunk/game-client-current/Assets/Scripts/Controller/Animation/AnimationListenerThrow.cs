using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;
using GameController;


public class AnimationListenerThrow : MonoBehaviour {
	
	public ThrowObjectRunner runner;
	
	public ThrowObjectData data;
	
	public void OnThrow() {
		if( data == null || runner == null ) {
			Debugger.LogError( "AnimationListenerThrow: Throw Animation Listener is invalid." );
			return;
		}
		
		ThrowScript ts = ThrowScript.Throw( data.throwsAndHits, null, runner );
		ts.timeToDestroyGameObject = data.timeToDestroy;
	}
	
	public void OnThrowEnded() {
		AnimationController animationController = transform.parent.transform.parent.gameObject.GetComponent< AnimationController >();
		animationController.Stop();
	}
}
