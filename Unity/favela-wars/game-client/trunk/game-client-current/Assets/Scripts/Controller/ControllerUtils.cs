using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Object = UnityEngine.Object;

using Squid;

using GameCommunication;
using Utils;


namespace GameController {
	
	public static class ControllerUtils {
		
		public const string GROUND_OBJECT_NAME = "Ground Object";		
		
		/// <summary>
		/// Valor utilizado para acelerar cálculos de divisão por 255 (multiplicação é mais rápida)
		/// </summary>
		private const float inv255 = 1.0f / 255.0f;
		
		
		public static Color GetColor( uint color ) {
			return new Color( ( ( color & 0xff000000 ) >> 24 ) * inv255,
			                  ( ( color & 0x00ff0000 ) >> 16 ) * inv255, 
		                      ( ( color & 0x0000ff00 ) >>  8 ) * inv255,
			                    ( color & 0x000000ff )         * inv255 );
		}
		
		public static GameVector CreateGameVector( Vector3 original ) {
			return new GameVector( original.x, original.y, original.z );
		}
		
		
		public static Vector3 CreateVector3( GameVector original ) {
			return new Vector3( original.x, original.y, original.z );
		}
		
		
		public static GameTransform CreateGameTransform( Transform t ) {
			GameTransform ret = new GameTransform();
			ret.direction = CreateGameVector( t.eulerAngles );
			ret.position = CreateGameVector( t.localPosition );
			ret.scale = CreateGameVector( t.localScale );
			
			return ret;
		}	
		
		
		/// <summary>
		/// Obtém a instância de um singleton da cena, mas NÃO o cria caso não exista. 
		/// </summary>
		/// <param name="instanceName">
		/// O nome do GameObject.
		/// </param>
		/// <returns>
		/// A instância do GameObject, ou null caso não exista.
		/// </returns>
		public static GameObject GetInstance( string instanceName ) {
			return GameObject.Find( instanceName );
		}
		
		
		/// <summary>
		/// Retorna a instância de um GameObject singleton da cena. Caso ela não exista, é criada e retornada.
		/// </summary>
		/// <param name="instanceName">
		/// O nome do GameObject na cena. Esse parâmetro é usado como identificação pela Unity para se obter a instância.
		/// </param>
		/// <param name="componentClassName">
		/// O nome da classe de componente a adicionar ao GameObject, caso ele precise ser criado. Se a instância
		/// já existir, não é verificada a consistência entre a lista de componentes do objeto e o componente passado por
		/// parâmetro.
		/// </param>
		/// <returns>
		/// A instância do GameObject.
		/// </returns>
		public static GameObject GetInstance( string instanceName, string componentClassName ) {
			return GetInstance( instanceName, new List< string >() { componentClassName } );
		}
		
		
		/// <summary>
		/// Retorna a instância de um GameObject singleton da cena. Caso ela não exista, é criada e retornada.
		/// </summary>
		/// <param name="instanceName">
		/// O nome do GameObject na cena. Esse parâmetro é usado como identificação pela Unity para se obter a instância.
		/// </param>
		/// <param name="componentsClassNames">
		/// O(s) nome(s) da(s) classe(s) de componente a adicionar ao GameObject, caso ele precise ser criado. Se a instância
		/// já existir, não é verificada a consistência entre a lista de componentes passados por parâmetro e os componentes
		/// do objeto.
		/// </param>
		/// <returns>
		/// A instância do GameObject.
		/// </returns>
		public static GameObject GetInstance( string instanceName, List< string > componentsClassNames ) {
			GameObject instance = GameObject.Find( instanceName );
			
			if( instance == null ) {
				instance = new GameObject();
				instance.name = instanceName;
				foreach ( string className in componentsClassNames )
					instance.AddComponent( className );
				Object.DontDestroyOnLoad( instance );
				
				instance.transform.parent = GetMissionHolderObject().transform;
			}	
			
			return instance;
		}
		
		
		/// <summary>
		/// Obtém o filho de um GameObject pelo seu nome. 
		/// </summary>
		/// <param name="parent">
		/// A <see cref="GameObject"/>
		/// </param>
		/// <param name="name">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="GameObject"/>
		/// </returns>
		public static GameObject FindChild( GameObject parent, string name ) {
			Transform transformChild = parent.transform.Find( name );
			if ( transformChild != null )
		   		return transformChild.gameObject;
			
			return null;
		}
		
		
		public static T FindComponentByName< T >( GameObject parent, string name ) where T : Component {
			foreach ( Component c in parent.GetComponentsInChildren< T >( true ) ) {
				if ( c.name.Equals( name ) )
					return ( T ) c;
			}
			return default( T );
		}
		

		/// <summary>
		/// Aplica o shader de visibilidade a todos os materiais de um GameObject.
		/// </summary>
		public static void ApplyVisibilityMap( GameObject go, VisibilityMap visibilityMap ) {
			foreach( Renderer renderer in go.GetComponentsInChildren< Renderer >( true ) ) {
				foreach( Material material in renderer.materials ) {
					
					// HACK MALDITO! 
					if( !go.name.Equals( GROUND_OBJECT_NAME ) )
						material.shader = Shader.Find( Common.SHADER_VISIBILITY_NAME );
					else {
						material.shader = Shader.Find( "Custom/terrain_splat_basic" );
					}
					
					material.SetTexture( "_VisibilityMap", visibilityMap.GetTexture( GameManager.GetInstance().GetTurnFaction() ) );
					material.SetVector( "_MinPoint", new Vector4( visibilityMap.minPoint.x, visibilityMap.minPoint.y,  visibilityMap.minPoint.z, 1.0f ) );
					material.SetVector( "_SectorSize", new Vector4( Common.LEVEL_SECTOR_WIDTH, Common.LEVEL_LAYER_HEIGHT, Common.LEVEL_SECTOR_WIDTH, 1.0f ) );
					material.SetVector( "_NumberOfSectors", new Vector4( visibilityMap.numberOfSectors.x, visibilityMap.numberOfSectors.y, visibilityMap.numberOfSectors.z, 1.0f ) );
					material.SetFloat( "_NumberOfSectorsXPlusTwo", visibilityMap.numberOfSectors.x + 2.0f );
					material.SetFloat( "_NumberOfSectorsYMinusOne", visibilityMap.numberOfSectors.y - 1.0f );
					material.SetFloat( "_NumberOfSectorsZPlusTwo", visibilityMap.numberOfSectors.z + 2.0f );
					material.SetFloat( "_NumberOfSectorsZPlusOne", visibilityMap.numberOfSectors.z + 1.0f );
					material.SetFloat( "_DW", 1.0f / ( visibilityMap.numberOfSectors.y * ( visibilityMap.numberOfSectors.x + 2 ) ) );
				}
			}			
		}
		
		
		public static Bounds GetBox( GameObject o, bool localSpace ) {
			return GetBox( o, localSpace, false );
		}
		
		
		/// <summary>
		/// Calcula uma caixa que envolva o objeto e todos os seus filhos, alinhada no eixo padrão.
		/// </summary>
		public static Bounds GetBox( GameObject o, bool localSpace, bool includeInactive ) {
			if ( localSpace ) {
				Transform previousParent = o.transform.parent;
				o.transform.parent = null;
				
				GameTransform old = CreateGameTransform( o.transform );
				o.transform.localScale = new Vector3( 1, 1, 1 );
				o.transform.position = new Vector3();
				o.transform.eulerAngles = new Vector3();
				
				Bounds b = new Bounds( new Vector3( float.MinValue, 0, 0 ), new Vector3() );
				
				foreach ( Transform t in o.GetComponentsInChildren< Transform >( includeInactive ) ) {
					Renderer r = ( Renderer ) t.gameObject.GetComponent( typeof( Renderer ) );
					if ( r != null  ) {
						if ( b.center.x == float.MinValue )
							b.center = ( r.bounds.center - o.transform.position );
						
						b.Encapsulate( r.bounds );
						continue;
					}
				}
				
				o.transform.localScale = CreateVector3( old.scale );
				o.transform.localPosition = CreateVector3( old.position );
				o.transform.eulerAngles = CreateVector3( old.direction );
				o.transform.parent = previousParent;
				
				return b;
			} else {
				Bounds b = new Bounds( o.transform.position, new Vector3() );
				
				foreach ( Transform t in o.GetComponentsInChildren< Transform >( includeInactive ) ) {
					Renderer r = ( Renderer ) t.gameObject.GetComponent( typeof( Renderer ) );
					if ( r != null  ) {
						b.Encapsulate( r.bounds );
						continue;
					}
				}
				
				return b;				
			}
		}
		
		
		public static GameVector RotateCollisionBoxSize( GameVector angles, GameVector size ) {
			if ( NanoMath.Equals( angles.x, 270f ) )
				return new GameVector( size.x, size.z, size.y );
				
			if ( NanoMath.Equals( angles.y, 90.0f ) || NanoMath.Equals( angles.y, 270.0f ) )
				return new GameVector( size.z, size.y, size.x );
			
			return new GameVector( size.x, size.y, size.z );
		}

		
		/// <summary>
		/// Obtém (criando, caso necessário) a instância do GameObject que serve de contêiner para os objetos de missão. 
		/// </summary>
		/// <returns>
		/// A instância do contêiner para os objetos de missão.
		/// </returns>
		public static GameObject GetMissionHolderObject() {
			return GetHolder( Common.MISSION_HOLDER_OBJECT_NAME );
		}
		
		
		/// <summary>
		/// Gets the holder.
		/// </summary>
		/// <returns>
		/// The holder.
		/// </returns>
		/// <param name='name'>
		/// Name.
		/// </param>
		public static GameObject GetHolder( string name ) {
			GameObject instance = GameObject.Find( name );
			
			if ( instance == null ) {
				instance = new GameObject( name );
				instance.tag = Common.Tags.HOLDER;
				Object.DontDestroyOnLoad( instance );
			}	
			
			return instance;			
		}
		
		public static Point GetScreenCenter() {
			return new Point( Screen.width >> 1, Screen.height >> 1 );
		}
	}
}

