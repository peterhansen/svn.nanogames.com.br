using System;

using UnityEngine;

using System.Collections.Generic;
using System.Collections;

using GameController;
using GameCommunication;
using Utils;
using Action = GameCommunication.GameAction;


namespace GameController {
	
	public class GetCharacterActionsRunner : GameActionRunner {
		
		private GetCharacterActionsData data;
		
		public GetCharacterActionsRunner( GetCharacterActionsData getCharacterActionsData ) : base( true ) {
			this.data = getCharacterActionsData;
		}
		
		protected override void Execute() {
			GameObject selectedCharacter = WorldManager.GetInstance().GetGameObject( data.characterId );
			
			// verifica se o que foi selecionada foi uma ação genérica de pegar todas as ações ou uma de movimento
			ActionInteractionController interactionController;
			
			
			if( data.actionTree == null ) {
				// nenhuma ação para o personagem
				Debugger.Log( "O personagem não possui nenhuma ação disponível." );
				OnDone();
				return;
			}
			
			// TODO: criar uma classe específica para o runner de movimento?
			if( data.actionTree.GetActionType() == ActionType.MOVE ) {
				interactionController = new MoveInteractionController( selectedCharacter, data.actionTree );
			} else {
				interactionController = new ActionsMenuInteractionController( selectedCharacter, data );
			}
			interactionController.SetCharacterActionInfo( data.info );
			
			interactionController.SetParent( GUIManager.GetInteractionController() );
			GUIManager.SetInteractionController( interactionController );
			OnDone();
		}
	}
}

