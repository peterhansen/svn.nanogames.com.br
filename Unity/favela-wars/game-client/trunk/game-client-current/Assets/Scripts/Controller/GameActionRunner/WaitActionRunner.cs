using System;
using System.Timers;
using UnityEngine;
using GameCommunication;
using System.Collections;

using Utils;


namespace GameController {
	
	public class WaitActionRunner : GameActionRunner, TimerScriptListener {
		
		private WaitActionData data;
		
		private Timer timer;
		
		
		public WaitActionRunner( WaitActionData data ) : base( true ) {
			this.data = data;
		}
		
		
		protected override void Execute() {
			TimerScript.AddTimer( data.getWaitTime(), this );
		}
		
		
		public void OnTimerEnded( int timerID ) {
			OnDone();
		}
		

	}
}

