using System;

using UnityEngine;

using GameCommunication;


namespace GameController {
	
	public class ReloadInteractionController : ActionInteractionController {
		
		public ReloadInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			if ( !( action is ReloadGameAction ) )
				throw new Exception( "Action is not ReloadGameAction - " + action );
		}
		
		public override void Initialize() {
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			ReloadGameAction reloadAction = action as ReloadGameAction;
			
			GameManager.GetInstance().SendRequest( new ReloadRequest( GameManager.GetInstance().GetLocalPlayer(), characterID, reloadAction.weaponID ) );
			OnDone();
		}
		
		public override void OnDone() {
			GUIManager.Reset();
		}
	}
}

