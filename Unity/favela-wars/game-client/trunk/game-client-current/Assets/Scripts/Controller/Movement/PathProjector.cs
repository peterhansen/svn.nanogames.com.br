using System;
using System.Collections.Generic;

using UnityEngine;

//DEBUG
using Utils;


namespace GameController {
	
	public class PathProjector {
		
		protected const int MAX_LAND_ATTEMPTS = 7;
		
		protected const float UP_STEP_FOR_EACH_ATTEMP = 0.9f;
		
		protected const float UP_SAFE_DISTANCE = 0.05f;
				
		public void CalculatePathPoints( Pathfinding.Path path, out List< Vector3 > points, out Vector3 finalDirection ) {
			finalDirection = new Vector3();
			if( path.vectorPath.Length < 2 ) {
				points = null;
				return;
			}
			
			points = new List< Vector3 >();
			for( int i = 0; i < path.vectorPath.Length - 1; i++ ) {
				Vector3 p0 = path.vectorPath[ i ];
				Vector3 p1 = path.vectorPath[ i + 1 ];
				Vector3 dir = p1 - p0;
				float dt = 1.5f / dir.magnitude;
				float t = 0.0f;
				do {				
					AttempCast( p0 + dir * t, points );
					t += dt;
				} while( t < 1.0f );
			}
			AttempCast( path.vectorPath[ path.vectorPath.Length - 1 ], points );
			
//			// DEBUG
//			for( int i = 0; i < points.Count - 1; i++ ) {
//				Debugger.DrawLine( 
//					ControllerUtils.CreateGameVector( points[i] ), 
//					ControllerUtils.CreateGameVector( points[i+1] ), 
//					i % 2 == 0? 0xff0000ff: 0x0000ffff, 
//					10.0f
//				);
//			}
			
			if( points.Count < 2 ) {
				points = null;
				return;
			}
			
			finalDirection = ( points[ points.Count -1 ] - points[ points.Count - 2 ] ).normalized;
		}
		
		protected void AttempCast( Vector3 p, List< Vector3 > points ) {
			RaycastHit hitInfo;
			for( int j = 0; j < MAX_LAND_ATTEMPTS; j++ ) {
				p += Vector3.up * UP_STEP_FOR_EACH_ATTEMP * j;
				if( Physics.Raycast( p, Vector3.down, out hitInfo, Common.CharacterInfo.BOX_HEIGHT ) ) {
					points.Add( hitInfo.point + Vector3.up * UP_SAFE_DISTANCE );
					return;
				}
			}
		}
	}
}

