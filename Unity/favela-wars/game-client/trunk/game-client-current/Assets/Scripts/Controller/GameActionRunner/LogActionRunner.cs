using System;
using UnityEngine;
using Utils;
using GameCommunication;


namespace GameController {
	
	public class LogActionRunner : GameActionRunner {
		
		private LogActionData data;
		
	
		public LogActionRunner( LogActionData logActionData ) : base( false ) {
			this.data = logActionData;
		}
		
		
		override protected void Execute() {
			switch ( data.logLevel ) {
				case LogLevel.WARNING:
					Debugger.LogWarning( data.message );
				break;
				
				case LogLevel.ERROR:
					Debugger.LogError( data.message );
				break;
			
				case LogLevel.NORMAL:
				default:
					Debugger.Log( data.message );
				break;
			}
		}
		
		
	}
}

