using System;

using UnityEngine;


using GameCommunication;
using Utils;


namespace GameController {
	
	public class ActionsMenuInteractionController : ActionInteractionController {
		
		protected ActionsMenuControl actionsMenu;
		
		public ActionsMenuInteractionController( GameObject selectedCharacter, GetCharacterActionsData data ) : base( selectedCharacter, null ) {					
			actionsMenu = new ActionsMenuControl( this, selectedCharacter, data.actionTree, data.visibleEnemiesIDs );
			controls.Add( actionsMenu );
			
			SetEnemiesList( data.visibleEnemiesIDs );
			AddEnemyListControl();
		}
		
		
		public override void Update() {
			InteractionUpdate();
		}
		
		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			// caso tenha recebido esse evento, é porque não foi a interface quem recebeu o clique
			OnDone();
		}
		
		
		public override void Initialize() {
			IsoCameraScript.GetInstance().userInputIsBlocked = true;
			IsoCameraScript.GetInstance().FocusOn( selectedCharacter );
		}
		
		public override void OnRemoved() {
			base.OnRemoved();
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
			characterSelectedGUI.DestroyGUI();
		}
	}
}
