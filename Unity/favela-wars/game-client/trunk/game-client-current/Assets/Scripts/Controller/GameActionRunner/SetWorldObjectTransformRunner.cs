using System;
using UnityEngine;
using Utils;
using GameCommunication;


namespace GameController {
	
	/// <summary>
	/// 
	/// </summary>
	public class SetWorldObjectTransformRunner : GameActionRunner {
		
		private SetWorldObjectTransformData data;
		
	
		public SetWorldObjectTransformRunner( SetWorldObjectTransformData transformData ) : base( false ) {
			this.data = transformData;
		}
		
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.worldObjectID );
			
			if ( gameObject == null ) {
				Debugger.LogError( "SetWorldObjectTransformRunner: worldObjectID not found: " + data.worldObjectID );
			} else {
//				gameObject.transform.localScale = ControllerUtils.CreateVector3( data.transform.scale );
				gameObject.transform.position = ControllerUtils.CreateVector3( data.transform.position );
				
				// HACK!!!
				if ( gameObject.transform.childCount < 2 ) {
					gameObject.transform.forward = ControllerUtils.CreateVector3( data.transform.direction );
					gameObject.transform.eulerAngles += new Vector3( 0, 180, 180 );
				} else {
					gameObject.transform.forward = ControllerUtils.CreateVector3( data.transform.direction );
				}
			}
		}
	}
}

