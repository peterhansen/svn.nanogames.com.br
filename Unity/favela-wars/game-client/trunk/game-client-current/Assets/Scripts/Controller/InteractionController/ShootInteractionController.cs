using System;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;


namespace GameController {
	
	public class ShootInteractionController : ActionInteractionController {
		
		private ConfirmationMenu confirmationMenu;
		
		private GameVector target;
		
		
		public override void Update() {
			InteractionUpdate();
		}
		
		
		public ShootInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			if ( !( action is AttackGameAction ) )
				throw new Exception( "Action is not AttackGameAction - " + action );
			
			if ( action == null ) {
				// quer dizer que não foi escolhida nenhuma arma.
				Debugger.LogError( "Para atirar, selecione uma arma." );
				GUIManager.Reset();
				throw new Exception( "Invalid attack action - " + action );
			}
			
			confirmationMenu = new ConfirmationMenu();			
			controls.Add( confirmationMenu );
			
			AddEnemyListControl();
		}
		
		
		protected void CancelAttack( Control sender ) {
			OnDone();
		}
		
		
		protected void ConfirmAttack( Control sender ) {
			GameManager gameManager = GameManager.GetInstance();
			WorldManager worldManager = WorldManager.GetInstance();
			
			int characterID = worldManager.GetWorldID( selectedCharacter );
			AttackGameAction attackAction = action as AttackGameAction;
			
			gameManager.SendRequest( new ShootRequest( gameManager.GetLocalPlayer(), characterID, attackAction.weaponID, attackAction.useMode, target ) );
			OnDone();
			GUIManager.Reset();
		}
		
		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			if( target == null && inputEvent.mouseButton == 0 && inputEvent.gameObject != null ) {
				GameManager gameManager = GameManager.GetInstance();
				WorldManager worldManager = WorldManager.GetInstance();
				
				target = ControllerUtils.CreateGameVector( inputEvent.selectionPosition );
				int characterID = worldManager.GetWorldID( selectedCharacter );
				AttackGameAction attackAction = action as AttackGameAction;
				
				if( attackAction.range > 0 ) {
					GameVector origin = ControllerUtils.CreateGameVector( worldManager.GetGameObject( characterID ).transform.position );
					float distance = ( target - origin ).Norm();
					if ( distance > attackAction.range ) {
						Debugger.Log( "Ataque fora do alcance." );
						return;
					}
				}
				
				if( inputEvent.singleClick ) {
					confirmationMenu.AddButton( new MenuOption( IconType.OK, ConfirmAttack ) );
					confirmationMenu.AddButton( new MenuOption( IconType.CANCEL, CancelAttack ) );
					confirmationMenu.Show( ControllerUtils.GetScreenCenter() );
					IsoCameraScript.GetInstance().MoveTo( ControllerUtils.CreateVector3( target ) );
					IsoCameraScript.GetInstance().userInputIsBlocked = true;
				} else {
					ConfirmAttack( null );
				}
			}
		}
		
		
		public override void OnRemoved() {
			base.OnRemoved();
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
		}
		
		
	}
}

