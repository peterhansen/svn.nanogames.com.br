using System.Collections.Generic;

using UnityEngine;

using Squid;
using SharpUnit;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class InteractionController {
				
		public List< Control > controls = new List< Control >();
		
		public InteractionController parent;
		
		protected ClickEvent clickEvent;

		
		public virtual void OnDone() {
			GUIManager.SetInteractionController( parent );
		}
		
		
		public virtual void OnRemoved() {
		}
		
		
		public void SetParent( InteractionController parent ) {
			this.parent = parent;
		}
		
		
		public InteractionController GetParent() {
			return parent;
		}
		
		
		public virtual void Initialize() {
		}
		
		
		/// <summary>
		/// Método chamado automaticamente pelo GUIManager a cada iteração do loop principal.
		/// </summary>
		public virtual void Update() {
		}
		
		
		public virtual void ReceiveInputEvent( InputEventScene inputEvent ) {			
		}
		
		
		protected void InteractionUpdate() {	
			// TODO criar singleton
			GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
			Desktop desktop = guiManager.Desktop;
			
			if( clickEvent != null && Time.realtimeSinceStartup - clickEvent.startTime > ClickEvent.CLICK_TIME ) {
				// acabou o tempo disponível para os clique: vamos fazer o picking
				ScreenPick( desktop );
				clickEvent = null;
			} else if( desktop.HotControl == desktop && Input.GetMouseButtonDown( Common.MOUSE_BUTTON_LEFT ) ) {
				if( clickEvent == null ) { 
					clickEvent = new ClickEvent();
				} else {
					clickEvent.clickCount++;
				}
			}
		}
		
		
		protected void ScreenPick( Desktop desktop ) {
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hitInfo;
			
			InputEventScene ie = new InputEventScene();
			ie.mouseButton = Common.MOUSE_BUTTON_LEFT;
			ie.singleClick = clickEvent.clickCount == 1;
			
			if ( Physics.Raycast( ray, out hitInfo ) ) {
				ie.gameObject = hitInfo.transform.gameObject;
				ie.selectionPosition = hitInfo.point;
				ie.selectionNormal = hitInfo.normal;
			} else {
				ie.gameObject = null;
				ie.selectionPosition = Vector3.zero;
			}
			
			ReceiveInputEvent( ie );
		}
		
		
		protected class ClickEvent {
			
			/// <summary>
			/// Tempo máximo entre os cliques de uma interação de clique.
			/// </summary>
			public const float CLICK_TIME = 0.3f;
			
			public float startTime;
			
			public int clickCount;
			
			// TODO: por enquanto, estou assumindo que só cliques com o botão direito fazem efeito no jogo
			//public int mouseButton;
			
			public ClickEvent() {
				clickCount = 1;
				startTime = Time.realtimeSinceStartup;
			}
		}
	}
}
