using System;
using System.Collections.Generic;
using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class ThrowObjectRunner : GameActionRunner {
		
		private ThrowObjectData data;
		
	
		public ThrowObjectRunner( ThrowObjectData shootData ) : base( true ) {
			this.data = shootData;
		}
		
		
		protected override void Execute() {
			GameObject character = WorldManager.GetInstance().GetGameObject( data.characterWorldID );
			if( character == null ) {
				Debugger.LogError( "Throw Object Runner: Character " + data.characterWorldID + " not found." );
				return;
			}
			
			AnimationListenerThrow throwListener = GetThrowListener( character );
			if( throwListener == null ) {
				Debugger.LogError( "Throw Object Runner: Character " + data.characterWorldID + " lacking Throw Animation Listener component." );
				return;
			}
			
			throwListener.data = data;
			throwListener.runner = this;
			AnimationController animationController = character.GetComponent< AnimationController >();
			if( animationController == null ) {
				Debugger.LogError( "Throw Object Runner: Animation Controller not found for character " + data.characterWorldID );
				ThrowScript ts = ThrowScript.Throw( data.throwsAndHits, null, this );
				ts.timeToDestroyGameObject = data.timeToDestroy;
				return;
			}
			
			animationController.Play( AnimationType.THROW );
		}
		
		protected AnimationListenerThrow GetThrowListener( GameObject character ) {
			Transform bodyPartChild = character.transform.FindChild( Common.AnimationProperties.MASTER_BODY_PART_NAME );
			Transform masterChild = bodyPartChild.transform.FindChild( Common.AnimationProperties.MASTER_CHILD_NAME );
			return masterChild.GetComponent< AnimationListenerThrow >();
		}
	}
}

