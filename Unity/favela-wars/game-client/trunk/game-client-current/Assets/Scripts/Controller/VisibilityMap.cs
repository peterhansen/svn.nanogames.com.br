using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameCommunication;
using GameController;


public class VisibilityMap : MonoBehaviour {
	
	private static readonly Color DARK_COLOR = new Color( 0.03f, 0.03f, 0.03f );
	
	public Texture2D[] visibilityBuffer;
	
	public Vector3 minPoint;
	
	public Vector3 maxPoint;
	
	public Vector3 sectorSize;
	
	public Vector3 numberOfSectors;
	
	protected int iMax;
	
	protected int jMax;
	
	protected int kMax;
	
	
	public void Awake() {
		DontDestroyOnLoad( this );
	}
	
	
	public void Instantiate( SectorsInfo sectorsInfo, int owner ) {
		int tWidth = ( sectorsInfo.missionWidthInSectors + 2 ) * sectorsInfo.missionHeightInSectors;
		int tHeight = 2 + sectorsInfo.missionLengthInSectors;
		
		visibilityBuffer = new Texture2D[ Common.PlayerFactions.Length ];
		
		foreach( Faction faction in Common.PlayerFactions ) {
			int c = faction - Common.PlayerFactions[ 0 ];
			visibilityBuffer[ c ] = new Texture2D( tWidth, tHeight, TextureFormat.RGB24, false, true );
			visibilityBuffer[ c ].wrapMode = TextureWrapMode.Clamp;

			for( int i = 0; i < tWidth; i++ ) {
				for( int j = 0; j < tHeight; j++ ) {
					visibilityBuffer[ c ].SetPixel( i, j, owner == (int) faction ? Color.white : DARK_COLOR );
				}
			}
			visibilityBuffer[ c ].Apply();
		}

		minPoint = new Vector3( sectorsInfo.minPoint.x, sectorsInfo.minPoint.y, sectorsInfo.minPoint.z );
		maxPoint = new Vector3( sectorsInfo.maxPoint.x, sectorsInfo.maxPoint.y, sectorsInfo.maxPoint.z );
		sectorSize = new Vector3( Common.LEVEL_SECTOR_WIDTH, Common.LEVEL_LAYER_HEIGHT, Common.LEVEL_SECTOR_WIDTH );
		numberOfSectors = new Vector3( sectorsInfo.missionWidthInSectors, sectorsInfo.missionHeightInSectors, sectorsInfo.missionLengthInSectors );
			
		iMax = (int) numberOfSectors.x;
		jMax = (int) numberOfSectors.y; 
		kMax = (int) numberOfSectors.z;
	}
	
	
	public static VisibilityMap GetInstance() {
		// TODO singleton decente de VisibilityMap
		return GameObject.Find( "Visibility Map" ).GetComponent< VisibilityMap >();
	}
	
	
	public void UpdateVisibilityBuffer( Faction faction ) {
		// TODO deixar refer�ncia est�tica em algum ponto do c�digo, em vez de GameObject.Find()
		// TODO n�o � necess�rio verificar a inst�ncia aqui, este j� � um m�todo de inst�ncia!
		Texture2D factionTexture = GetTexture( faction );
		foreach ( GameObject go in GameObject.FindSceneObjectsOfType( typeof ( GameObject ) ) ) {
			// TODO n�o temos como compartilhar uma textura, alterando assim somente uma textura e refletindo em todos os
			// objetos que a usam? Varrer todos os materiais da cena � um processo MUITO lento!
			foreach( Renderer renderer in go.GetComponentsInChildren< Renderer >( true ) ) {
				foreach( Material material in renderer.materials ) {
					material.SetTexture( "_VisibilityMap", factionTexture );
				}
			}
		}
	}
	
	
	public void SetVisible( List< Sector > sectors, bool visible, Faction faction ) {
		Texture2D factionTexture = GetTexture( faction );
		int multiplierJ = (int) numberOfSectors.x + 2;
		
		foreach( Sector s in sectors ) {
			int pixelX = 1 + s.i + multiplierJ * s.j;
			int pixelY = 1 + s.k;
			
//			if( NanoMath.Equals( visibilityBuffer[ factionIndex ].GetPixel( pixelX, pixelY ).r, 0.0f ) ) {
//				Vector3 position = new Vector3( minPoint.x, minPoint.y, minPoint.z );
//				position.x += sectorSize.x * ( s.i + 0.5f );
//				position.y += sectorSize.y * ( s.j + 0.5f );
//				position.z += sectorSize.z * ( s.k + 0.5f );
//				
//				GameObject cubao = GameObject.CreatePrimitive( PrimitiveType.Cube );
//				cubao.transform.position = position;
//				cubao.transform.localScale = new Vector3( sectorSize.x, sectorSize.y, sectorSize.z );
//				cubao.name = "sector( " + s.i + ", " + s.j + ", " + s.k + " )";
//			}
			
			factionTexture.SetPixel( pixelX, pixelY, visible? Color.white : Color.black );
		}
		factionTexture.Apply();
	}
	
	public Texture2D GetTexture( Faction faction ) {
		return visibilityBuffer[ ( int )( faction - Common.PlayerFactions[ 0 ] ) ];
	}
}