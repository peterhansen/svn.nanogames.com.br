using System;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class MoveInteractionController : ActionInteractionController {
		
		#region Character Data
		protected int characterID;
		
		protected Seeker characterSeeker;

		protected float nearDistance;
		
		protected float farDistance;
		#endregion
		
		#region Movement Data
		protected VisibilityMap visibilityMap;
		
		protected List< Vector3 > points;
		
		protected CoverData coverData;
		
		protected bool pathChosen = false;
		


		protected MovementTrail trail;
		
		protected PathfinderListener pathListener;
		#endregion
		
		#region Interface Elements
		protected ConfirmationMenu confirmationMenu;
		#endregion
		
	
		public MoveInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			// TODO: singleton?
			visibilityMap = VisibilityMap.GetInstance();
			
			characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			characterSeeker = selectedCharacter.GetComponent< Seeker >();
			if( characterSeeker == null ) {
				Debugger.Log( "Personagem sem um Seeker associado. Esse personagem não poderá se mover na cena." );
				OnDone();
			}
			
			MoveGameAction moveGameAction = action as MoveGameAction;
			if( moveGameAction == null ) {
				Debugger.Log( "Game Action recebida não corresponde ao tipo do Interaction Controller." );
				OnDone();
			}
			nearDistance = moveGameAction.nearDistance;
			farDistance = moveGameAction.farDistance;
			
			confirmationMenu = new ConfirmationMenu();
			controls.Add( confirmationMenu );
						
		}
		
		public override void Update() {
			InteractionUpdate();
		}
		
		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			if( pathChosen || inputEvent.gameObject == null || inputEvent.gameObject.CompareTag( Common.Tags.CHARACTER ) )
				return;

			MoveGameAction moveAction = action as MoveGameAction;
			if( !inputEvent.singleClick )
				pathListener = new PathfinderListener( AskMoveConfirmation, inputEvent.selectionNormal );
			else
				pathListener = new PathfinderListener( ConfirmMoveWithoutAsking, inputEvent.selectionNormal );
			
			Vector3 destination = new Vector3( inputEvent.selectionPosition.x, inputEvent.selectionPosition.y, inputEvent.selectionPosition.z );
			characterSeeker.StartPath( selectedCharacter.transform.position, destination, pathListener.StartPathCallback );
		}
			
		

		
		
		protected void ConfirmMovement( Control sender ) {
			// convertemos caminho em uma lista de gameVectors para enviar para o servidor
			List< GameVector > gameVectorPoints = new List< GameVector >();
			foreach( Vector3 point in points )
				gameVectorPoints.Add( ControllerUtils.CreateGameVector( point ) );
			
			// enviando caminho para o servidor
			MoveCharacterRequest request = new MoveCharacterRequest( 
				GameManager.GetInstance().GetLocalPlayer(), 
				characterID, 
				gameVectorPoints, 
				ControllerUtils.CreateGameVector( pathListener.finalDirection ), 
				pathListener.finalStance
			);
			GameManager.GetInstance().SendRequest( request );

			OnDone();
			GUIManager.Reset();
		}
		
		
		protected void ConfirmMoveWithoutAsking( List< Vector3 > points, Vector3 destinationNormal, CoverData coverData ) {
			// TODO: dar um jeito de sumir com esses caras aqui? (dependências temporais)
			this.points = points;
			this.coverData = coverData;
			
			if( coverData != null ) {
				TakeCoverAndConfirmMovement( null );
			} else {
				ConfirmMovement( null );
			}
		}
	
		
		
		protected void AskMoveConfirmation( List< Vector3 > points, Vector3 destinationNormal, CoverData coverData ) {
			pathChosen = true;
			
			// TODO: quando removermos o hotseat, essa visibilidade da façção dinâmica tem que vazar...
			trail = new MovementTrail( visibilityMap, GameManager.GetInstance().GetTurnFaction() );
			
			// fazer essa parada dentro do trail
			trail.DestinationObject.transform.position = points[ points.Count - 1 ] + Vector3.up * 0.1f;
			trail.DestinationObject.transform.up = destinationNormal;			
			
			// vamos encontrar cada uma das partes da trilha
			// TODO: esse código está ligeiramente podre. REFACTOLYPSE NOW!
			int stage = 1;
			Dictionary< int, List< Vector3 > > trailParts = new Dictionary< int, List< Vector3 > >();
			trailParts[ stage ] = new List< Vector3 >();
			List< Vector3 > currentStagePath = trailParts[ stage ];		
			currentStagePath.Add( points[ 0 ] );
			float totalDistance = 0.0f;
			
			for( int i = 0; i < points.Count - 1; i++ ) {
				currentStagePath.Add( points[ i ] );
				totalDistance += ( points[ i + 1 ] - points[ i ] ).magnitude;
				
				switch( stage ) {
				case 1:
					if( totalDistance > nearDistance ) {
						stage++;
						trailParts[ stage ] = new List< Vector3 >();
						i--;
						currentStagePath = trailParts[ stage ];
					}
					break;
					
				case 2:
					if( totalDistance > farDistance ) {
						stage++;
						trailParts[ stage ] = new List< Vector3 >();
						currentStagePath = trailParts[ stage ];	
						i--;
					}
					break;
				}
			}
			
			float glowDistance = 0.0f;
			List< Vector3 > trailPartPoints = null;
			foreach( KeyValuePair< int, List< Vector3 > > kvp in trailParts ) {
				int trailPartStage = kvp.Key;
				trailPartPoints = kvp.Value;
				
				Trail trailPart = trail.GenerateTrail( trailPartStage, trailPartPoints, glowDistance );
				if( trailPart.glows )
					glowDistance += trailPart.Length;
			}
			
			foreach( GameObject trailObject in trail.trailObjects )
				ApplyGlowParameters( trailObject, glowDistance );
			
			MeshFilter meshFilter = trail.DestinationObject.GetComponent< MeshFilter >();
			Color[] meshColors = new Color[ meshFilter.mesh.vertexCount ];
			for( int i = 0; i < meshFilter.mesh.vertexCount; i++ ) {
				meshColors[ i ] = trail.CalculateVertexColor( trail.DestinationObject.transform.position + meshFilter.mesh.vertices[ i ], glowDistance );
			}
			meshFilter.mesh.colors = meshColors;
			if( stage < 3 ) {				
				trail.DestinationObject.renderer.material.SetColor( "_TintColor", MovementTrail.DESTINATION_GLOW_COLOR );
				
				// ... e atribuindo velocidade e distância máxima do glow para o objeto de destino
				ApplyGlowParameters( trail.DestinationObject, glowDistance );
			}
			
			this.points = points;
			this.coverData = coverData;
			confirmationMenu.AddButton( new MenuOption( IconType.OK, ConfirmMovement ) );
			confirmationMenu.AddButton( new MenuOption( IconType.CANCEL, CancelMovement ) );
				
			if( coverData != null )
				confirmationMenu.AddButton( new MenuOption( IconType.TAKE_COVER, TakeCoverAndConfirmMovement ) );
				
			confirmationMenu.Show( new Point( Screen.width >> 1, Screen.height / 3 ) );
			IsoCameraScript.GetInstance().userInputIsBlocked = true;
			IsoCameraScript.GetInstance().FocusOn( trail.DestinationObject );
		}
	

		protected void ApplyGlowParameters( GameObject gameObject, float glowDistance ) {
			Trail trailGlow = gameObject.GetComponent< Trail >();
			if( trailGlow != null ) {
				trailGlow.glowMaxDistance = glowDistance;
				trailGlow.glowSpeed = glowDistance / Trail.GLOW_TIME;
			}
		}
		
		
		protected void TakeCoverAndConfirmMovement( Control sender ) {
			pathListener.finalStance = Stance.CROUCHING;
			
			for( int i = 0; i < points.Count; i++ ) {
				Vector3 distanceToCover = points[ i ] - ( coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT );
				
				distanceToCover.y = 0.0f;
				
				// estamos aproximando nosso personagem por um cilindro aqui
				if( distanceToCover.magnitude < Common.CharacterInfo.BOX_HALF_WIDTH ) {
					Vector3 currDistanceToWall = Vector3.Project( distanceToCover, coverData.normal );
					Vector3 correction = currDistanceToWall * ( Common.CharacterInfo.BOX_HALF_WIDTH / currDistanceToWall.magnitude );
					correction.y = 0.0f;
					points[ i ] = coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT + correction;
				}
			}
			
			// primeiro, vamos calcular a direcao de cobertura, a qual é tangente ao plano de cobertura)
			Vector3 moveDir = points[ points.Count - 1 ] - points[ points.Count - 2 ];
			Vector3 projectedCoverDir = Vector3.Project( moveDir, coverData.normal );
			pathListener.finalDirection = moveDir - projectedCoverDir;
			pathListener.finalDirection.y = 0.0f;
			pathListener.finalDirection.Normalize();
			
			if( NanoMath.Equals( pathListener.finalDirection.magnitude, 0.0f ) )
				pathListener.finalDirection = coverData.normal;
			
			ConfirmMovement( null );
		}
		
		
		protected void CancelMovement( Control sender ) {
			OnDone();
		}
		
		
		public override void OnDone() {
			if( trail != null )
				trail.Destroy();
			
			base.OnDone();
		}
			
		public override void OnRemoved() {
			base.OnRemoved();
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
			characterSelectedGUI.DestroyGUI();
		}
	}
}