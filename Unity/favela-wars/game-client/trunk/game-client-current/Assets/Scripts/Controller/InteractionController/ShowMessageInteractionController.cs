using System;

using UnityEngine;

using Utils;
using GameController;
using GameCommunication;

namespace GameController {
	
	public class ShowMessageInteractionController : InteractionController {
		
		public ShowMessageInteractionController( InteractionController previous, ShowMessageData data ) {
			this.SetParent( previous );
			controls.Add( new ShowMessageControl( GetParent(), data ) );
		}
	}
}