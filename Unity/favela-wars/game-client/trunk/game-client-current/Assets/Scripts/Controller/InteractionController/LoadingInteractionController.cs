using System;


namespace GameController {
	
	public class LoadingInteractionController : InteractionController {
		
		protected LoadResourceControl control = new LoadResourceControl();
		
		public LoadingInteractionController() {
			controls.Add( control );
		}
		
		public void UpdateProgress( float progress ) {
			control.Tick( progress );
			
			if( progress > 1.0f ) {
				GUIManager.SetInteractionController( new SelectCharacterInteractionController() );
			}
		}
	}
}

