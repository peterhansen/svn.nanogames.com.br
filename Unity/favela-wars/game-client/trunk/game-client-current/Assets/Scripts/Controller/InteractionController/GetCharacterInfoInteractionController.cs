using System;

using UnityEngine;

using GameCommunication;


namespace GameController {
	
	public class GetCharacterInfoInteractionController : ActionInteractionController {
		
		GetCharacterInfoData characterData;

		
		public GetCharacterInfoInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
		}
		
		
		public override void Initialize() {
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			GameManager.GetInstance().SendRequest( new GetCharacterInfoRequest( GameManager.GetInstance().GetLocalPlayer(), characterID ) );
		}
		
		
		public void SetData( GetCharacterInfoData data ) {
			this.characterData = data;
			controls.Add( new CharacterInfoControl( this, characterData ) );
			GUIManager.AddRenderers( this );
		}
		
	}
}

