using System;

using UnityEngine;

using Utils;
using System.Collections.Generic;


namespace GameController {
	
	public class MovementTrail {
		
		public static readonly Color DESTINATION_GLOW_COLOR = new Color( 0.22f, 0.55f, 0.7f, 1.0f );

		protected const float DESTINATION_OBJECT_RADIUS = 1.0f;
		
		protected const float DESTINATION_OBJECT_HEIGHT = 0.000001f;
		
		protected const int NUMBER_OF_TRAIL_STAGES = 3;
		
		protected const float TRAIL_MESH_WIDTH = 0.08f;
	
		protected const float TRAIL_HEIGHT = 0.1f;
		
		protected const float TRAIL_STANDARD_GLOW = 0.25f;
		
		
		protected VisibilityMap visibilityMap;
		
		public GameObject[] trailObjects;

		protected MeshFilter[] meshFilters;
		
		protected Mesh[] trailMeshes;
		
		protected List< Trail > trailParts = new List< Trail >();
		
		
		protected GameObject destinationObject;
		public GameObject DestinationObject {
			get { return destinationObject; }
		}
		
		protected Texture2D factionTexture;
		
		protected bool active;
		public bool Active {
			get { return active; }
			set {
				active = value;
				destinationObject.active = active;
				foreach( GameObject trailObject in trailObjects )
					trailObject.active = active;
			}
		}
		
		
		public MovementTrail( VisibilityMap visibilityMap, Faction faction ) {
			this.visibilityMap = visibilityMap;
			
			factionTexture = visibilityMap.GetTexture( faction );
			
			// criando cilindro de destino
			destinationObject = GameObject.CreatePrimitive( PrimitiveType.Cylinder );
			destinationObject.layer = Common.Layers.IGNORE_RAYCAST;
			destinationObject.renderer.material = new Material( Shader.Find( "Custom/MovementTrailShader" ) );
			destinationObject.renderer.material.SetTexture( "_MainTex", Resources.Load( "GUI/MovementTrail/Cursor_Move" ) as Texture2D );
			destinationObject.renderer.material.SetColor( "_TintColor", new Color( TRAIL_STANDARD_GLOW, TRAIL_STANDARD_GLOW, TRAIL_STANDARD_GLOW, 1.0f ) );
			destinationObject.transform.localScale = new Vector3( DESTINATION_OBJECT_RADIUS, DESTINATION_OBJECT_HEIGHT, DESTINATION_OBJECT_RADIUS );
			destinationObject.AddComponent< Trail >();
			destinationObject.name = "Movement Destination";
			
			// criando malhas de trilha
			trailObjects = new GameObject[ NUMBER_OF_TRAIL_STAGES ];
			trailMeshes = new Mesh[ NUMBER_OF_TRAIL_STAGES ];
			meshFilters = new MeshFilter[ NUMBER_OF_TRAIL_STAGES ];
			
			for( int i = 0; i < NUMBER_OF_TRAIL_STAGES; i++ ) {
				int stage = i + 1;
				trailObjects[ i ] = new GameObject( "Trail Object - Stage " + stage );

				MeshRenderer meshRenderer = trailObjects[ i ].AddComponent< MeshRenderer >();
				meshRenderer.material = new Material( Shader.Find( "Custom/MovementTrailShader" ) );
				Texture2D trailTexture = Resources.Load( "GUI/MovementTrail/Stage0" + stage ) as Texture2D;
				meshRenderer.material.SetTexture( "_MainTex", trailTexture );
				meshRenderer.material.SetColor( "_TintColor", new Color( TRAIL_STANDARD_GLOW, TRAIL_STANDARD_GLOW, TRAIL_STANDARD_GLOW, 1.0f ) );
				trailObjects[ i ].AddComponent< Trail >();
				
				trailMeshes[ i ] = new Mesh();
				meshFilters[ i ] = trailObjects[ i ].AddComponent< MeshFilter >();
				meshFilters[ i ].mesh = trailMeshes[ i ];
			}
		}
	
		
		public Trail GenerateTrail( int stage, List< Vector3 > points, float previousTrailPartDistance ) {		
			// resampleando a lista de pontos para ganhar mais precisão para a geração da malha
//			const float SAMPLE_RATIO = 0.f;
//			List< Vector3 > resampledPoints = new List< Vector3 >();
//			for( int i = 0; i < points.Count - 1; i++ ) {
//				for( float j = 0.0f; j < 1.0f; j += SAMPLE_RATIO ) {
//					Vector3 newPoint = points[i] * ( 1.0f - j ) + points[i] * j;
//					resampledPoints.Add( newPoint );
//				}
//			}
			
			int stageIndex = stage - 1;			
			
			// fazendo uma interpolação de bezier na curva de pontos
			const float dt = 0.1f;
			Vector3 p0 = new Vector3();
			Vector3 p1 = new Vector3();
			Vector3 p2 = new Vector3();
			List< Vector3 > interpolatedPoints = new List< Vector3 >();
			interpolatedPoints.Add( points[ 0 ] );
			for( int i = 0; i < points.Count; i++ )
			{
				p0 = i == 0? points[ i ] : interpolatedPoints[ interpolatedPoints.Count - 1 ];
				p1 = points[ Math.Min( i + 1, points.Count - 1 ) ];
				p2 = points[ Math.Min( i + 2, points.Count - 1 ) ];
				
				for( float t = 0.1f; t < 0.6f; t += dt ) {
					Vector3 interpolatedPoint = BezierInterpolation( p0, p1, p2, t );
					interpolatedPoints.Add( interpolatedPoint );
				}
			}
			Vector3 finalInterpolatedPoint = BezierInterpolation( p0, p1, p2, 0.6f );
			interpolatedPoints.Add( finalInterpolatedPoint );
			
			Vector3[] meshVertices = new Vector3[ interpolatedPoints.Count * 2 ];
			Vector3[] meshNormals = new Vector3[ interpolatedPoints.Count * 2 ];
			Vector2[] meshUVs = new Vector2[ interpolatedPoints.Count * 2 ];
			Color[] meshColors = new Color[ interpolatedPoints.Count * 2 ];
			
			float totalDistance = previousTrailPartDistance;
			for( int i = 0; i < interpolatedPoints.Count - 1; i++ )
				totalDistance += ( interpolatedPoints[ i + 1 ] - interpolatedPoints[ i ] ).magnitude;
			
			Vector3 dir = new Vector3();
			Color vertexColor = Color.white;			
			
			float distance = previousTrailPartDistance;
			for( int i = 0; i < interpolatedPoints.Count; i++ ) {
				Vector3 currentPoint = interpolatedPoints[ i ] + Vector3.up * TRAIL_HEIGHT;
				
				// acumulando distância total
				if( i > 0 )
					distance += ( interpolatedPoints[ i ] - interpolatedPoints[ i - 1 ] ).magnitude;

				if( i + 1 < interpolatedPoints.Count ) {
					Vector3 newDir = ( interpolatedPoints[ i + 1 ] - currentPoint ).normalized;
					if( i - 1 >= 0 ) {
						float beta = Vector3.Angle( newDir, interpolatedPoints[ i ] - interpolatedPoints[ i - 1 ] );
						if( NanoMath.GreaterThan( Mathf.Abs( beta ), 10.0f ) ) {
							newDir = ( newDir + dir ) * 0.5f;
						}
					}
					dir = newDir.normalized;
				} else {
					dir = ( interpolatedPoints[ i ] - interpolatedPoints[ i - 1 ] ).normalized;
				}
				Vector3[] currentSideVectors = GetSideVectors( currentPoint, dir );
			
				meshVertices[ ( i * 2 ) + 1 ] = currentSideVectors[ 0 ];
				meshVertices[ i * 2 ] = currentSideVectors[ 1 ];
				
				meshNormals[ i * 2 ] = Vector3.up;
				meshNormals[ ( i * 2 ) + 1 ] = Vector3.up;
				meshUVs[ i * 2 ] = i % 2 == 0? new Vector2( 0.0f, 0.0f ) : new Vector2( 0.0f, 1.0f );
				meshUVs[ (i * 2) + 1 ] = i % 2 == 0? new Vector2( 1.0f, 0.0f ) : new Vector2( 1.0f, 1.0f );
				
				vertexColor = CalculateVertexColor( currentPoint, distance, previousTrailPartDistance, totalDistance, stage == 1, stage == 3 );
				meshColors[ i * 2 ] = vertexColor;
				meshColors[ (i * 2) + 1 ] = vertexColor;
			}

			// vamos colorir o destinationObject de forma que ele fique na mesma cor do fim da trailMesh
			
			// multiplicamos por 6 = 2 * 3 (dois triângulos por plano, cada triângulo com 3 vértices)
			int[] meshTriangles = new int[ ( interpolatedPoints.Count - 1 ) * 6 ];
			for( int i = 0; i < interpolatedPoints.Count - 1; i++ ) {
				// triangulo 1 do plano
				meshTriangles[ i * 6 ] = i * 2;
				meshTriangles[ ( i * 6 ) + 1 ] = ( i * 2 ) + 1;
				meshTriangles[ ( i * 6 ) + 2 ] = ( i * 2 ) + 3;
				
				// triangulo 2 do plano
				meshTriangles[ ( i * 6 ) + 3 ] = meshTriangles[ ( i * 6 ) ];
				meshTriangles[ ( i * 6 ) + 4 ] = meshTriangles[ ( i * 6 ) + 2 ];
				meshTriangles[ ( i * 6 ) + 5 ] = ( i * 2 ) + 2;
			}
			trailMeshes[ stageIndex ].vertices = meshVertices;
			trailMeshes[ stageIndex ].normals = meshNormals;
			trailMeshes[ stageIndex ].triangles = meshTriangles;
			trailMeshes[ stageIndex ].uv = meshUVs;
			trailMeshes[ stageIndex ].colors = meshColors;
			
			Trail trail = trailObjects[ stageIndex ].GetComponent< Trail >();
			trail.SetPoints( points );
			trail.glows = stage < 3;
			trailParts.Add( trail );
			return trail;
		}

		public void PositionDestinationObject() {
			List< Vector3 > trailPartPoints = trailParts[ trailParts.Count - 1 ].GetPoints();
			destinationObject.transform.position = trailPartPoints[ trailPartPoints.Count - 1 ] + Vector3.up * TRAIL_HEIGHT;
		}
		
		
		protected Vector3[] GetSideVectors( Vector3 point, Vector3 dir ) {
			Vector3 sideVector = Vector3.Cross( dir, Vector3.up ).normalized * TRAIL_MESH_WIDTH;
			return new Vector3[]{ point + sideVector, point - sideVector };
		}
		
		
		// HACK: isso aqui vai poder deixar de ser publico...
		public Color CalculateVertexColor( Vector3 vertex, float distance ) {//bool fadeNearDestination ) {
			return new Color( distance / 100.0f, CalculateVertexAlpha( vertex ), 1.0f );
		}
		
		// HACK: isso aqui não deveria ser público
		public Color CalculateVertexColor( 
		   	 Vector3 vertex, float distance, 
			 float nearBorderDistance, float farBorderDistance, 
			 bool nearBigMargin, bool farBigMargin
			 ) {
			float alpha = CalculateVertexAlpha( vertex );
			
			float distanceToNearBorder = distance - nearBorderDistance;
			if( !nearBigMargin )
				distanceToNearBorder *= 2.5f;
			
			float distanceToFarBorder = farBorderDistance - distance;
			if( !farBigMargin )
				distanceToFarBorder *= 2.5f;
					
			alpha *= Mathf.Min( 1.0f, Mathf.Min( distanceToNearBorder, distanceToFarBorder ) / DESTINATION_OBJECT_RADIUS );
			
			return new Color( distance / 100.0f, alpha, 1.0f );
		}
		
		
		// ATENÇÃO: esse método precisa ser um reflexo do shader de visibilidade
		protected float CalculateVertexAlpha( Vector3 vertex ) {			
			Vector3 voxelCoord = new Vector3( ( vertex.x - visibilityMap.minPoint.x ) / visibilityMap.sectorSize.x,
				( vertex.y - visibilityMap.minPoint.y ) / visibilityMap.sectorSize.y,
				( vertex.z - visibilityMap.minPoint.z ) / visibilityMap.sectorSize.z
			);
		
			float sectorJ = Mathf.Floor( voxelCoord.y );
		
			float DW = 1.0f / ( visibilityMap.numberOfSectors.y * ( visibilityMap.numberOfSectors.x + 2 ) );
			Vector2 sectorUV = new Vector2( 
				( sectorJ * ( visibilityMap.numberOfSectors.x + 2.0f ) + voxelCoord.x + 1.0f ) * DW, 
				( voxelCoord.z + 1.0f ) / ( visibilityMap.numberOfSectors.z + 2.0f ) );
			
			float dist = 2.0f * ( voxelCoord.y - sectorJ - 0.5f );
			
			float neighborSectorJ = Mathf.Max( sectorJ + Mathf.Sign( dist ), 0.0f );
			Vector2 neighborSectorUV = new Vector2( ( neighborSectorJ * ( visibilityMap.numberOfSectors.x + 2.0f ) + voxelCoord.x ) * DW, sectorUV.y );
			
			int sectorX = (int) (sectorUV.x * visibilityMap.visibilityBuffer[0].width);
			int sectorY = (int) (sectorUV.y * visibilityMap.visibilityBuffer[0].height);
			int neighborSectorX = (int) (neighborSectorUV.x * visibilityMap.visibilityBuffer[0].width);
			int neighborSectorY = (int) (neighborSectorUV.y * visibilityMap.visibilityBuffer[0].height);
			
			return Mathf.Max( factionTexture.GetPixel( sectorX, sectorY ).r,
				              factionTexture.GetPixel( neighborSectorX, neighborSectorY ).r * Mathf.Abs( dist ) );
		}
		
		public void Destroy() {
			for( int i = 0; i < NUMBER_OF_TRAIL_STAGES; i++ )
				GameObject.DestroyImmediate( trailObjects[ i ] );
			
			GameObject.DestroyImmediate( destinationObject );
		}
		
		protected Vector3 BezierInterpolation( Vector3 p0, Vector3 p1, Vector3 p2, float t ) {
			return ( ( 1.0f - t ) * ( ( 1.0f - t ) * p0 + t * p1 ) ) + ( t * ( ( 1.0f - t ) * p1 + t * p2 ) ); 
		}
	}

}

