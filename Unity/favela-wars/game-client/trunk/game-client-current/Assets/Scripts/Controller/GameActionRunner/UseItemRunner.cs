using System;

using UnityEngine;

using Squid;

using GameCommunication;
using Utils;


namespace GameController {
	
	public class UseItemRunner : GameActionRunner {
			
		protected UseItemData data;
		
		public UseItemRunner( UseItemData useItemData ) : base( false ) {
			this.data = useItemData;
		}

		protected override void Execute() {
			// pegando posicao na tela do personagem algo do item
			GameObject character = WorldManager.GetInstance().GetGameObject( data.characterID );
			Vector3 screenPos = Camera.mainCamera.WorldToScreenPoint( character.transform.position );
			
			GUIManager.AddFloatingText( data.useItemMessage, (int)screenPos.x, (int)screenPos.y );
		}
	}
}

