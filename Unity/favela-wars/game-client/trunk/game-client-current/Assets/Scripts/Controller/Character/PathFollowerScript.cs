using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Pathfinding;

using Utils;
using GameController;
using GameCommunication;


public class PathFollowerScript : Seeker {
	
	#region Variáveis estáticas (usadas pelo game design editor)
	public static float MIN_STANDING_MOVE_SPEED = 1f;
	public static float MAX_STANDING_MOVE_SPEED = 3f;
	public static float MIN_CROUCH_MOVE_SPEED = 0.3f;
	public static float MAX_CROUCH_MOVE_SPEED = 0.8f;
	#endregion
	
	#region Referências de classe
	protected MoveCharacterData data;
	
	protected MoveCharacterRunner runner;
	
	protected AnimationController animationController;
	#endregion
	
	#region Controle de movimento
	private bool isRunning;
	
	protected int currentStep;
	
	protected float accTime;

	protected float totalTime;
	
	protected float speed;
	
	protected GameVector origin;
	
	protected GameVector destination;
	#endregion
	
	public PathFollowerScript() {
		isRunning = false;
	}
	
	public void Initialize( MoveCharacterRunner runner, MoveCharacterData data ) {
		this.runner = runner;
		
		this.data = data;
		if( data == null ) {
			Debugger.LogError( "PathFollowerScript: it is not possible to initialize a PathFollowerScript without a MoveCharacterData instance." );
			OnDone();
			return;
		}
		
		animationController = GetComponent< AnimationController >();
		if( animationController == null ) {
			Debugger.LogWarning( "PathFollowerScript: initializing a PathFollowerScript without an Animation Controller. Animations will not work for the character " + data.characterWorldID );
			OnDone();
			return;
		}		
		
		accTime = 0.0f;
		totalTime = 0.0f;
		currentStep = 1;
		
		switch ( data.movementStance ) {
		case Stance.STANDING:
			speed = NanoMath.Lerp( data.percentualSpeed, MIN_STANDING_MOVE_SPEED, MAX_STANDING_MOVE_SPEED );
			break;
			
		case Stance.CROUCHING:
			speed = NanoMath.Lerp( data.percentualSpeed, MIN_CROUCH_MOVE_SPEED, MAX_CROUCH_MOVE_SPEED );
			break;
		} 
		
		// libera nós previamente ocupados por esse personagem e comeca o movimento
//		oldBounds = GameController.ControllerUtils.GetBox( gameObject, false );
//		SetGraphNodesWalkability( true );
		isRunning = true;
		
		if( animationController != null )
			animationController.Play( AnimationType.WALK );
		
		Step();
	}	

	
	public void FixedUpdate() {
		if( !isRunning )
			return;
		
		if( NanoMath.GreaterEquals( accTime, totalTime ) ) {
			transform.position = ControllerUtils.CreateVector3( destination );
			if( Step() )
				// caso o movimento tenha acabado, vamos querer encerrar o movimento por aqui
				return;
		}
	
		accTime += Time.fixedDeltaTime;
		GameVector d = origin + ( destination - origin ) * ( accTime / totalTime );
		transform.position = ControllerUtils.CreateVector3( d );
		
		// TODO conforme personagem anda, atualizar informação sobre sua layer
	}
	
	
	private bool Step() {
		isRunning = data.nodes.Count > 0;
		
		if ( isRunning ) {
			origin = new GameVector( transform.position.x, transform.position.y, transform.position.z );
			destination = data.nodes[ 0 ] + GameVector.YAxis * Common.CharacterInfo.BOX_LANDING_HEIGHT;
			Debugger.DrawLine( origin, destination, 0xff0000ff, 10.0f );
			
			LookAt( ControllerUtils.CreateVector3( destination ) );
		
			accTime -= totalTime;
			totalTime = origin.DistanceTo( destination ) / speed;
			data.nodes.RemoveAt( 0 );
			
			if( data.visibilityInfo != null && data.visibilityInfo.ContainsStep( currentStep ) )
				RunVisibilityChangeData( data.visibilityInfo.GetVisibilityChangeData( currentStep ) );
			
			currentStep++;
		} else {
			
			// verificamos se temos uma visibilidade final para executá-la
			if( data.visibilityInfo != null ) {
				VisibilityChangeData visibilityData = data.visibilityInfo.FinalVisibility;
				if( visibilityData != null ) {
					LookAt( ControllerUtils.CreateVector3( destination + data.finalDirection ) );
					RunVisibilityChangeData( visibilityData );
				}
			}
			
			/// atualiza posicao no grafo de navegacao
//			Bounds newBounds = GameController.ControllerUtils.GetBox( gameObject, false );
//			SetGraphNodesWalkability( false );
//			//oldBounds = newBounds;
			
			if( animationController != null )
				animationController.Stop();
			OnDone();
		}
		
		// atualiza a informação da camada do personagem
		BuildingInfo.RefreshLayer( gameObject );
		return !isRunning;
	}
	
	
	// TODO: esse método vai poder deixar de ser public, na versao nao hotseat
	public void SetGraphNodesWalkability( bool walkability ) {
		Bounds bounds = GameController.ControllerUtils.GetBox( gameObject, false );	
		
		// DEBUG
//		GameObject cubao = GameObject.CreatePrimitive( PrimitiveType.Cube );
//		cubao.transform.position = bounds.center;
//		cubao.transform.localScale = bounds.extents;
//		cubao.name = gameObject.name + " bounds";
		
		GraphUpdateObject guo = new GraphUpdateObject( bounds );
		guo.modifyWalkability = true;
		guo.updatePhysics = true;
		guo.setWalkability = walkability;
		
		GameObject aStar = GameObject.Find ( "A*" );
		AstarPath aStarPath = aStar.GetComponent< AstarPath >();
		aStarPath.UpdateGraphs( guo );
	}
	
	
	protected void RunVisibilityChangeData( VisibilityChangeData visibilityData ) {
		VisibilityChangeRunner visibilityRunner = new VisibilityChangeRunner( visibilityData );
		visibilityRunner.Run();
		
		if( data.visibilityInfo != null )
			data.visibilityInfo.RemoveVisibility( currentStep );
	}
	
	
	protected void LookAt( Vector3 targetPoint ) {
		transform.LookAt( targetPoint );
		transform.eulerAngles = new Vector3( 0.0f, transform.eulerAngles.y, 0.0f );
	}
	
	
	protected void OnDone() {
		if( runner != null )
			runner.OnDone();
	}
	

	protected void ConsolidateDestination( Vector3 destination ) {
		transform.position = destination;
		LandObject( gameObject );
	}
	
	
	public static void LandObject( GameObject go ) {
		RaycastHit hit;
		Bounds b = ControllerUtils.GetBox( go, false );
		Vector3 origin = new Vector3( b.center.x, b.min.y, b.center.z );
		Ray ray = new Ray( origin, Vector3.down );
		if ( !Physics.Raycast( ray, out hit, b.size.y ) )
			return;
		
//		Debugger.DrawLine(
//			ControllerUtils.CreateGameVector( go.transform.position ), 
//			ControllerUtils.CreateGameVector( hit.point ), 
//			0xff00ffff, 
//			10.0f
//		);
		
		go.transform.position = hit.point;
	}
}
