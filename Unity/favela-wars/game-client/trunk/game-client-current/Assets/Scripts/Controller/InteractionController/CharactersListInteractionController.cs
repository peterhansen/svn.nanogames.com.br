using System;

using GameCommunication;


namespace GameController {
	
	public class CharactersListInteractionController : InteractionController {
		
		public CharactersListInteractionController() {
			
			CharacterListControl clc = new CharacterListControl();
			clc.Populate();
			controls.Add( clc );
		}
	}
}

