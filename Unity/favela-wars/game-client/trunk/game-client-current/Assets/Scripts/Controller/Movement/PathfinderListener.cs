using System;
using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class PathfinderListener {
			
		public bool confirmationNeeded;
		
		public delegate void SendPathCallback( List< Vector3 > points, Vector3 normal, CoverData coverData );
		
		protected SendPathCallback callback;
		
		protected List< Vector3 > points = new List< Vector3 >();
		
		public Vector3 finalDirection;
		
		public Vector3 finalNormal;
		
		public Stance finalStance;
		
		public float currentDistance;
		
		protected float accumulatedDistance;
		
		protected PathProjector projector = new PathProjector();
		
		public PathfinderListener( SendPathCallback callback, Vector3 finalNormal ) {
			this.callback = callback;
			this.finalNormal = finalNormal;
		}
		
		public void StartPathCallback( Pathfinding.Path path ) {	
			if( !path.error )
				OnSuccess( path );
			else
				OnFailure();
		}
		
		protected void OnSuccess( Pathfinding.Path path ) {
			projector.CalculatePathPoints( path, out points, out finalDirection );
			CoverData coverData = GenerateCoverData( points[ points.Count - 1 ] );
			callback( points, finalNormal, coverData );
		}
		
		protected void OnFailure() {
			Debugger.Log( "Movimento inválido!" );
		}
		
		protected CoverData GenerateCoverData( Vector3 position ) {
			const int numberOfRays = 6;
			const float maxCoverDistance = 1.0f;
			
			CoverData coverData = null;
			float smallestDistance = float.MaxValue;
			
			for (int i = 0; i < numberOfRays; i++) {
				float angle = i * 360.0f / (float) numberOfRays;
				Vector3 dir = new Vector3( Mathf.Cos( angle ), 0.0f, Mathf.Sin( angle ) );
				Vector3 origin = position + Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT;
				Ray ray = new Ray( origin, dir );
				
				RaycastHit hit;
				if( Physics.Raycast( ray, out hit, maxCoverDistance ) ) {
					if( hit.distance < smallestDistance ) {
						smallestDistance = hit.distance;
						
						coverData = new CoverData();
						coverData.position = hit.point;
						coverData.normal = hit.normal;
					}
				}
			}
			
			return coverData;
		}	
	}
}

