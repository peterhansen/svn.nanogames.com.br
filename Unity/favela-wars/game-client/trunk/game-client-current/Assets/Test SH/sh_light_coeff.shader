Shader "sh_light_coeff_TEST" {

Properties {
	_SH_scale ("SH scaling factor", Float) = 1.0
	_L00 ("L00 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_1_1 ("L_1_1 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_10 ("L_10 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_11 ("L_11 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_2_2 ("L_2_2 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_2_1 ("L_2_1 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_20 ("L_20 coeffcient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_21 ("L_21 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
	_L_22 ("L_22 coefficient", Vector) = (0.0, 0.0, 0.0, 0.0)
}

SubShader {
    Pass {

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

// Constants
#define C1 0.429043
#define C2 0.511664
#define C3 0.743125
#define C4 0.886227
#define C5 0.247708

// HACK
//#define vec3 float3
// Env map test
//#define L_00 vec3(2.525627, 2.323453, 2.326265)
//#define L_1_1 vec3(-0.527202, -0.214542, 0.115904)
//#define L_10 vec3(0.047339, 0.066496, 0.083035)
//#define L_11 vec3(-0.003062, 0.009533, 0.017440)
//#define L_2_2 vec3(0.023733, 0.016762, 0.013869)
//#define L_2_1 vec3(-0.061234, -0.050576, -0.024582)
//#define L_20 vec3(-0.145934, -0.122050, -0.079847)
//#define L_21 vec3(0.010150, 0.001560, -0.008679)
//#define L_22 vec3(0.048304, 0.056962, 0.054762)

#define SCALED(coeff) (_SH_scale * float3(coeff) )
//#define SCALED(coeff) (float3(coeff))

float _SH_scale;
float4 _L00;
float4 _L_1_1;
float4 _L_10;
float4 _L_11;
float4 _L_2_2;
float4 _L_2_1;
float4 _L_20;
float4 _L_21;
float4 _L_22;

struct v2f {
    float4 pos : SV_POSITION;
    float3 color : COLOR0;
};

v2f vert (appdata_base v)
{
    v2f o;
    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
    
    // HACK
    //float3 N = v.normal;
    float3 N = float3(v.normal.x, v.normal.y, v.normal.z);
    N = mul(float3x3(_Object2World), N);
    N = normalize(N);
    
    // Irradiance
	float3 E = C1 * SCALED(_L_22) * ((N.x * N.x) - (N.y * N.y));
	E += C3 * SCALED(_L_20) * (N.z * N.z);
	E += C4 * SCALED(_L00);
	E += C5 * SCALED(_L_20);
	E += 2.0 * C1 * (SCALED(_L_2_2) * (N.x * N.y) + SCALED(_L_21) * (N.x * N.z) + SCALED(_L_2_1) * (N.y * N.z));
	E += 2.0 * C2 * (SCALED(_L_11) * N.x + SCALED(_L_1_1) * N.y + SCALED(_L_10) * N.z);

	o.color = float4(E, 1.0);

    
    return o;
}

half4 frag (v2f i) : COLOR
{
    return half4 (i.color, 1);
}
ENDCG

    }
}
Fallback "VertexLit"
}
