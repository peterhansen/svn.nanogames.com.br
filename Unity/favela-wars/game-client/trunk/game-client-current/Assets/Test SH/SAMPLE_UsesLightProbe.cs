using UnityEngine;

using System.Collections;


public class SAMPLE_UsesLightProbe : MonoBehaviour {
	
	public Vector3 origin;
	public Vector3 destination;
	
	public float accTime = 0.0f;
	public const float totalTime = 2.0f;	
	
	class LightProbeWithSqDistance : System.IComparable {
		public GameObject _lightProbe;
		public float _sqDistance;
		
		public LightProbeWithSqDistance(GameObject lprobe, float sqdist) {
			_lightProbe = lprobe;
			_sqDistance = sqdist;
		}
		
		public int CompareTo(object obj) {
			if(obj is LightProbeWithSqDistance)
				return this._sqDistance.CompareTo((obj as LightProbeWithSqDistance)._sqDistance);
			
			throw new System.ArgumentException("Trying to compare LightProbeWithSqDistance with unknown object!");
		}
		
		public static Vector3[] interpolate(LightProbeWithSqDistance probe_a, LightProbeWithSqDistance probe_b) {
			Vector3[] sh_coeffs = new Vector3[9];
			float sqdistance_sum = probe_a._sqDistance + probe_b._sqDistance;
			float alpha_a, alpha_b;
			SAMPLE_LightProbe light_probe_a, light_probe_b;
			
			alpha_a = probe_b._sqDistance / sqdistance_sum;
			alpha_b = 1.0f - alpha_a;
			light_probe_a = probe_a._lightProbe.GetComponent("SAMPLE_LightProbe") as SAMPLE_LightProbe;
			light_probe_b = probe_b._lightProbe.GetComponent("SAMPLE_LightProbe") as SAMPLE_LightProbe;

			for(int i = 0; i < sh_coeffs.Length; ++i)
				sh_coeffs[i] = (light_probe_a.mSHCoeffs[i] * light_probe_a.mSHScale * alpha_a) + (light_probe_b.mSHCoeffs[i] * light_probe_b.mSHScale * alpha_b);
			
			return sh_coeffs;
		}
	};
	
	// Use this for initialization
	void Start () {
		origin = new Vector3(0.0f, 0.0f, 0.0f);	
		destination = new Vector3(10.0f, 0.0f, 10.0f);
	}
	
	// Update is called once per frame
	void Update () {
		accTime += Time.deltaTime;
		if( accTime > totalTime )
			accTime = 0.0f;
		
//		transform.position = origin + (destination - origin) * (accTime / totalTime);
		
		OnPositionChange();
	}
	
	void OnPositionChange() {
		GameObject[] lightProbes = GameObject.FindGameObjectsWithTag("SH_LightProbe") as GameObject[];
		LightProbeWithSqDistance[] lightProbesSqDist = new LightProbeWithSqDistance[lightProbes.Length];
		
		for(int i = 0; i < lightProbes.Length; ++i) {
			Vector3 to_probe = lightProbes[i].transform.position - transform.position;
			
			lightProbesSqDist[i] = new LightProbeWithSqDistance(lightProbes[i], to_probe.sqrMagnitude);
		}
		
		System.Array.Sort(lightProbesSqDist);
		
		foreach( Renderer renderer in GetComponentsInChildren< Renderer >() ) {
			if(renderer.material.HasProperty("_SH_scale")) {
				switch(lightProbesSqDist.Length) {
					//case 0:
					//	break;
						//return;
					
					case 1:
						SAMPLE_LightProbe lightProbe_01 = lightProbesSqDist[0]._lightProbe.GetComponent("SAMPLE_LightProbe") as SAMPLE_LightProbe;
						SAMPLE_LightProbe.setMaterialSHParams(lightProbe_01.mSHCoeffs, lightProbe_01.mSHScale, renderer.material);
						break;
						//return;
					
					default:
						Vector3[] sh_coeffs = LightProbeWithSqDistance.interpolate(lightProbesSqDist[0], lightProbesSqDist[1]);
						SAMPLE_LightProbe.setMaterialSHParams(sh_coeffs, 1.0f, renderer.material);
						break;
						//return;
				}
			}
//			
		}
	}
}
