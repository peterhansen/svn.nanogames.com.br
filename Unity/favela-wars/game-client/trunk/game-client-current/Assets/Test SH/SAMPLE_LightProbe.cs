using UnityEngine;
using System.Collections;

public class SAMPLE_LightProbe : MonoBehaviour {
	
	public string lightProbeName;
	
	// SH indexes
	public const int L00 = 0;
	public const int L_1_1 = 1;
	public const int L_10 = 2;
	public const int L_11 = 3;
	public const int L_2_2 = 4;
	public const int L_2_1 = 5;
	public const int L_20 = 6;
	public const int L_21 = 7;
	public const int L_22 = 8;
	
	public Vector3[] mSHCoeffs;
	public float mSHScale = 1.0f;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () 
	{	
	}
	
	public void initSHCoeffs() {
		mSHCoeffs = new Vector3[9];
	}
	
	public static void setMaterialSHParams(Vector3[] sh_coeffs, float sh_scale, Material material) {
		material.SetVector("_L00", sh_coeffs[L00]);
		material.SetVector("_L_1_1", sh_coeffs[L_1_1]);
		material.SetVector("_L_10", sh_coeffs[L_10]);
		material.SetVector("_L_11", sh_coeffs[L_11]);
		material.SetVector("_L_2_2", sh_coeffs[L_2_2]);
		material.SetVector("_L_2_1", sh_coeffs[L_2_1]);
		material.SetVector("_L_20", sh_coeffs[L_20]);
		material.SetVector("_L_21", sh_coeffs[L_21]);
		material.SetVector("_L_22", sh_coeffs[L_22]);
		
		material.SetFloat("_SH_scale", sh_scale);
	}
}
