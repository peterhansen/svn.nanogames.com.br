using UnityEngine;
using System.Collections;

public class LightProbe_Tron : SAMPLE_LightProbe {
	
	// Use this for initialization
	void Start () {
		initSHCoeffs();
		
		mSHCoeffs[L00] = new Vector3(2.148558f, 1.594839f, 0.459398f);
		mSHCoeffs[L_1_1] = new Vector3(-0.074691f, -0.172616f, 0.150108f);
		mSHCoeffs[L_10] = new Vector3(-0.004045f, -0.002721f, 0.000106f);
		mSHCoeffs[L_11] = new Vector3(-0.036128f, -0.029902f, -0.004000f);
		mSHCoeffs[L_2_2] = new Vector3(0.060444f, 0.054934f, 0.006866f);
		mSHCoeffs[L_2_1] = new Vector3(-0.004353f, -0.005138f, -0.001286f);
		mSHCoeffs[L_20] = new Vector3(0.027252f, -0.011730f, 0.005241f);
		mSHCoeffs[L_21] = new Vector3(0.001309f, 0.001121f, 0.000096f);
		mSHCoeffs[L_22] = new Vector3(0.054823f, -0.012417f, 0.009306f)	;
		
		mSHScale = 0.30f;
	}
	
	// Update is called once per frame
	void Update () {	
	}
}
