using UnityEngine;
using System.Collections;

public class LightProbe_Sample : SAMPLE_LightProbe {
	
	// Use this for initialization
	void Start () {
		initSHCoeffs();
				
		mSHCoeffs[L00] = new Vector3(2.118955f, 1.869027f, 1.469673f);
		mSHCoeffs[L_1_1] = new Vector3(0.587161f, 0.706083f, 0.509435f);
		mSHCoeffs[L_10] = new Vector3(-0.046473f, -0.068327f, -0.063319f);
		mSHCoeffs[L_11] = new Vector3(0.060276f, 0.056137f, 0.053587f);
		mSHCoeffs[L_2_2] = new Vector3(0.025518f, 0.031309f, 0.028960f);
		mSHCoeffs[L_2_1] = new Vector3(-0.188751f, -0.229618f, -0.229592f);
		mSHCoeffs[L_20] = new Vector3(-0.065442f, -0.063322f, -0.043219f);
		mSHCoeffs[L_21] = new Vector3(-0.027673f, -0.046185f, -0.047141f);
		mSHCoeffs[L_22] = new Vector3(0.077694f, 0.028042f, 0.063944f);
		
		mSHScale = 0.30f;
	}
	
	// Update is called once per frame
	void Update () {	
	}
}
