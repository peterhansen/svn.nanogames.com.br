using UnityEngine;
using System.Collections;

public class LightProbe_Canyon : SAMPLE_LightProbe {
	
	// Use this for initialization
	void Start () {
		initSHCoeffs();
		
		mSHCoeffs[L00] = new Vector3(2.525627f, 2.323453f, 2.326265f);
		mSHCoeffs[L_1_1] = new Vector3(-0.527202f, -0.214542f, 0.115904f);
		mSHCoeffs[L_10] = new Vector3(0.047339f, 0.066496f, 0.083035f);
		mSHCoeffs[L_11] = new Vector3(-0.003062f, 0.009533f, 0.017440f);
		mSHCoeffs[L_2_2] = new Vector3(0.023733f, 0.016762f, 0.013869f);
		mSHCoeffs[L_2_1] = new Vector3(-0.061234f, -0.050576f, -0.024582f);
		mSHCoeffs[L_20] = new Vector3(-0.145934f, -0.122050f, -0.079847f);
		mSHCoeffs[L_21] = new Vector3(0.010150f, 0.001560f, -0.008679f);
		mSHCoeffs[L_22] = new Vector3(0.048304f, 0.056962f, 0.054762f);
		
		mSHScale = 0.30f;
	}
	
	// Update is called once per frame
	void Update () {	
	}
}
