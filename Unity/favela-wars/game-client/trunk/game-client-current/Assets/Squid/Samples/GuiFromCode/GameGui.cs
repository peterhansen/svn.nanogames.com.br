using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Squid;
using System;
using System.Reflection;

public class GameGui : GuiRenderer
{
	
	protected override void Start ()
	{
		base.Start ();
		
		// Desktop and Skin are inherited fields
		Desktop.ShowCursor = true;
		Skin.DefaultFont = "segoe_ui_bold_8";
		GuiHost.SetSkin(Skin);
		
		if(Wysiwyg == null)
		{
	        #region create skin
		
        ControlStyle itemStyle = new ControlStyle();
        itemStyle.Tiling = TextureMode.Grid;
        itemStyle.Grid = new Margin(6);
        itemStyle.Texture = "button_hot.dds";
        itemStyle.Default.Texture = "button_default.dds";
        itemStyle.Pressed.Texture = "button_down.dds";
        itemStyle.SelectedPressed.Texture = "button_down.dds";
        itemStyle.Focused.Texture = "button_down.dds";
        itemStyle.SelectedFocused.Texture = "button_down.dds";
        itemStyle.Selected.Texture = "button_down.dds";
        itemStyle.SelectedHot.Texture = "button_down.dds";
        itemStyle.TextPadding = new Margin(10, 0, 10, 0);

        ControlStyle inputStyle = new ControlStyle();
        inputStyle.Texture = "input_default.dds";
        inputStyle.Hot.Texture = "input_focused.dds";
        inputStyle.Focused.Texture = "input_focused.dds";
        inputStyle.TextPadding = new Margin(8);
        inputStyle.Tiling = TextureMode.Grid;
        inputStyle.Focused.Tint = ColorInt.RGBA(1, 0, 0, 1);
        inputStyle.Grid = new Margin(6);

        ControlStyle buttonStyle = new ControlStyle();
        buttonStyle.Texture = "button_default.dds";
        buttonStyle.Hot.Texture = "button_hot.dds";
        buttonStyle.Focused.Texture = "button_hot.dds";
        buttonStyle.Pressed.Texture = "button_down.dds";
        buttonStyle.Checked.Texture = "button_down.dds";
        buttonStyle.CheckedHot.Texture = "button_down.dds";
        buttonStyle.TextAlign = Alignment.MiddleCenter;
        buttonStyle.Tiling = TextureMode.Grid;
        buttonStyle.Grid = new Margin(6);

        ControlStyle frameStyle = new ControlStyle();
        frameStyle.Tiling = TextureMode.Grid;
        frameStyle.Grid = new Margin(8);
        frameStyle.Texture = "grid.dds";

        ControlStyle frameStyle2 = new ControlStyle();
        frameStyle2.CheckedHot.Tiling = TextureMode.Grid;
        frameStyle2.Tiling = TextureMode.Grid;
        frameStyle2.Grid = new Margin(8);
        frameStyle2.Texture = "grid2.dds";
        frameStyle2.TextPadding = new Margin(8);

        ControlStyle vscrollTrackStyle = new ControlStyle();
        vscrollTrackStyle.Tiling = TextureMode.Grid;
        vscrollTrackStyle.Grid = new Margin(0, 3, 0, 3);
        vscrollTrackStyle.Texture = "vscroll_track.dds";

        ControlStyle vscrollButtonStyle = new ControlStyle();
        vscrollButtonStyle.Tiling = TextureMode.Grid;
        vscrollButtonStyle.Grid = new Margin(0, 4, 0, 4);
        vscrollButtonStyle.Texture = "vscroll_button.dds";
        vscrollButtonStyle.Hot.Texture = "vscroll_button_hot.dds";
        vscrollButtonStyle.Pressed.Texture = "vscroll_button_down.dds";

        ControlStyle vscrollUp = new ControlStyle();
        vscrollUp.Default.Texture = "vscrollUp_default.dds";
        vscrollUp.Hot.Texture = "vscrollUp_hot.dds";
        vscrollUp.Pressed.Texture = "vscrollUp_down.dds";
        vscrollUp.Focused.Texture = "vscrollUp_hot.dds";

        ControlStyle hscrollTrackStyle = new ControlStyle();
        hscrollTrackStyle.Tiling = TextureMode.Grid;
        hscrollTrackStyle.Grid = new Margin(3, 0, 3, 0);
        hscrollTrackStyle.Texture = "hscroll_track.dds";

        ControlStyle hscrollButtonStyle = new ControlStyle();
        hscrollButtonStyle.Tiling = TextureMode.Grid;
        hscrollButtonStyle.Grid = new Margin(4, 0, 4, 0);
        hscrollButtonStyle.Texture = "hscroll_button.dds";
        hscrollButtonStyle.Hot.Texture = "hscroll_button_hot.dds";
        hscrollButtonStyle.Pressed.Texture = "hscroll_button_down.dds";

        ControlStyle hscrollUp = new ControlStyle();
        hscrollUp.Default.Texture = "hscrollUp_default.dds";
        hscrollUp.Hot.Texture = "hscrollUp_hot.dds";
        hscrollUp.Pressed.Texture = "hscrollUp_down.dds";
        hscrollUp.Focused.Texture = "hscrollUp_hot.dds";


        ControlStyle checkButtonStyle = new ControlStyle();
        checkButtonStyle.Default.Texture = "checkbox_default.dds";
        checkButtonStyle.Hot.Texture = "checkbox_hot.dds";
        checkButtonStyle.Pressed.Texture = "checkbox_down.dds";
        checkButtonStyle.Checked.Texture = "checkbox_checked.dds";
        checkButtonStyle.CheckedFocused.Texture = "checkbox_checked_hot.dds";
        checkButtonStyle.CheckedHot.Texture = "checkbox_checked_hot.dds";
        checkButtonStyle.CheckedPressed.Texture = "checkbox_down.dds";

        ControlStyle comboLabelStyle = new ControlStyle();
        comboLabelStyle.TextPadding = new Margin(10, 0, 0, 0);
        comboLabelStyle.Default.Texture = "combo_default.dds";
        comboLabelStyle.Hot.Texture = "combo_hot.dds";
        comboLabelStyle.Pressed.Texture = "combo_down.dds";
        comboLabelStyle.Focused.Texture = "combo_hot.dds";
        comboLabelStyle.Tiling = TextureMode.Grid;
        comboLabelStyle.Grid = new Margin(6, 0, 0, 0);

        ControlStyle comboButtonStyle = new ControlStyle();
        comboButtonStyle.Default.Texture = "combo_button_default.dds";
        comboButtonStyle.Hot.Texture = "combo_button_hot.dds";
        comboButtonStyle.Pressed.Texture = "combo_button_down.dds";
        comboButtonStyle.Focused.Texture = "combo_button_hot.dds";

        ControlStyle labelStyle = new ControlStyle();
        labelStyle.TextAlign = Alignment.TopRight;
        labelStyle.TextPadding = new Squid.Margin(8);

        Skin.Styles.Add("item", itemStyle);
        Skin.Styles.Add("textbox", inputStyle);
        Skin.Styles.Add("button", buttonStyle);
        Skin.Styles.Add("frame", frameStyle);
        Skin.Styles.Add("frame2", frameStyle2);
        Skin.Styles.Add("checkBox", checkButtonStyle);
        Skin.Styles.Add("comboLabel", comboLabelStyle);
        Skin.Styles.Add("comboButton", comboButtonStyle);
        Skin.Styles.Add("vscrollTrack", vscrollTrackStyle);
        Skin.Styles.Add("vscrollButton", vscrollButtonStyle);
        Skin.Styles.Add("vscrollUp", vscrollUp);
        Skin.Styles.Add("hscrollTrack", hscrollTrackStyle);
        Skin.Styles.Add("hscrollButton", hscrollButtonStyle);
        Skin.Styles.Add("hscrollUp", hscrollUp);
        Skin.Styles.Add("multiline", labelStyle);

        #endregion
		}
		
		// create auto-atlas
		// note: size parameter will go away next version (autom. size)
		Texture2D atlas = CreateAtlas(Skin, "guiAtlas01", 2, 1024) as Texture2D;

		// optional: save the returned texture as png
		// var bytes = atlas.EncodeToPNG();
		// System.IO.File.WriteAllBytes("F:\\atlas.png", bytes);
		
		if(Wysiwyg == null)
			CreateGui();
	}

    void CreateGui()
    {
        #region cursors

        Point cursorSize = new Point(32, 32);
        Point halfSize = cursorSize / 2;

        Desktop.AddCursor(Cursors.Default, new Cursor { Texture = "Cursors/Arrow.png", Size = cursorSize, HotSpot = Point.Zero });
        Desktop.AddCursor(Cursors.Link, new Cursor { Texture = "Cursors/Link.png", Size = cursorSize, HotSpot = Point.Zero });
        Desktop.AddCursor(Cursors.Move, new Cursor { Texture = "Cursors/Move.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.Select, new Cursor { Texture = "Cursors/Select.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.SizeNS, new Cursor { Texture = "Cursors/SizeNS.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.SizeWE, new Cursor { Texture = "Cursors/SizeWE.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.HSplit, new Cursor { Texture = "Cursors/SizeNS.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.VSplit, new Cursor { Texture = "Cursors/SizeWE.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.SizeNESW, new Cursor { Texture = "Cursors/SizeNESW.png", Size = cursorSize, HotSpot = halfSize });
        Desktop.AddCursor(Cursors.SizeNWSE, new Cursor { Texture = "Cursors/SizeNWSE.png", Size = cursorSize, HotSpot = halfSize });

        #endregion
				
        #region sample window 1 - Anchoring, DropDown, Modal Dialog

        SampleWindow window1 = new SampleWindow();
        window1.Size = new Squid.Point(440, 340);
        window1.Position = new Squid.Point(40, 40);
        window1.Style = "frame";
        window1.Titlebar.Text = "Anchoring, [color=ffcc0000]DropDown, Modal Dialog[/color]";
        window1.Resizable = true;
        window1.Parent = Desktop;

        Label label1 = new Label();
        label1.Text = "username:";
        label1.Size = new Squid.Point(122, 35);
        label1.Position = new Squid.Point(60, 100);
        label1.Parent = window1;
        label1.OnMousePress += new MouseDownEventHandler(label1_OnMouseDown);

        TextBox textbox1 = new TextBox { Name = "textbox" };
        textbox1.Text = "username";
        textbox1.Size = new Squid.Point(222, 35);
        textbox1.Position = new Squid.Point(180, 100);
        textbox1.Style = "textbox";
        textbox1.Parent = window1;
        textbox1.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;

        Label label2 = new Label();
        label2.Text = "password:";
        label2.Size = new Squid.Point(122, 35);
        label2.Position = new Squid.Point(60, 140);
        label2.Parent = window1;

        TextBox textbox2 = new TextBox { Name = "textbox" };
        textbox2.PasswordChar = char.Parse("*");
        textbox2.Text = "password";
        textbox2.Size = new Squid.Point(222, 35);
        textbox2.Position = new Squid.Point(180, 140);
        textbox2.Style = "textbox";
        textbox2.Parent = window1;
        textbox2.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;

        Button button = new Button();
        button.Size = new Squid.Point(157, 35);
        button.Position = new Squid.Point(437 - 192, 346 - 52);
        button.Text = "login";
        button.Style = "button";
        button.Parent = window1;
        button.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        button.Cursor = Cursors.Link;
        button.OnMouseClick += new MouseClickEventHandler(button_OnMouseClick);

        DropDownList combo = new DropDownList();
        combo.Size = new Squid.Point(222, 35);
        combo.Position = new Squid.Point(180, 180);
        combo.Parent = window1;
        combo.Label.Style = "comboLabel";
        combo.Button.Style = "comboButton";
        combo.Listbox.Margin = new Margin(0, 6, 0, 0);
        combo.Listbox.Style = "frame2";
        combo.Listbox.ClipFrame.Margin = new Margin(8, 8, 8, 8);
        combo.Listbox.Scrollbar.Margin = new Margin(0, 4, 4, 4);
        combo.Listbox.Scrollbar.Size = new Squid.Point(14, 10);
        combo.Listbox.Scrollbar.ButtonUp.Style = "vscrollUp";
        combo.Listbox.Scrollbar.ButtonUp.Size = new Squid.Point(10, 20);
        combo.Listbox.Scrollbar.ButtonDown.Style = "vscrollUp";
        combo.Listbox.Scrollbar.ButtonDown.Size = new Squid.Point(10, 20);
        combo.Listbox.Scrollbar.Slider.Margin = new Margin(0, 2, 0, 2);
        combo.Listbox.Scrollbar.Slider.Style = "vscrollTrack";
        combo.Listbox.Scrollbar.Slider.Button.Style = "vscrollButton";
        combo.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;

        for (int i = 0; i < 10; i++)
        {
            ListBoxItem item = new ListBoxItem();
            item.Text = "listboxitem";
            item.Size = new Squid.Point(100, 35);
            item.Margin = new Margin(0, 0, 0, 4);
            item.Style = "item";
            combo.Items.Add(item);
        }

        CheckBox box = new CheckBox();
        box.Size = new Squid.Point(157, 26);
        box.Position = new Squid.Point(180, 220);
        box.Text = "remember me";
        box.Parent = window1;
        box.Button.Style = "checkBox";
        box.Button.Size = new Squid.Point(26, 26);
        box.Button.Cursor = Cursors.Link;

        #endregion

        #region sample window 2 - SplitContainer, TreeView, ListBox

        SampleWindow window2 = new SampleWindow();
        window2.Size = new Squid.Point(440, 340);
        window2.Position = new Squid.Point(500, 40);
        window2.Style = "frame";
        window2.Titlebar.Text = "SplitContainer, TreeView, ListBox";
        window2.Resizable = true;
        window2.Parent = Desktop;

        SplitContainer split = new SplitContainer();
        split.Dock = DockStyle.Fill;
        split.Parent = window2;
        split.SplitButton.Style = "button";
        split.SplitFrame1.Size = new Point(10, 10);
        split.SplitFrame2.Size = new Point(30, 10);

        Squid.Panel pnl = new Squid.Panel();
        pnl.Dock = Squid.DockStyle.Fill;
        pnl.ClipFrame.Margin = new Squid.Margin(8);
        pnl.ClipFrame.Style = "textbox";

        pnl.VScroll.Margin = new Squid.Margin(0, 8, 8, 8);
        pnl.VScroll.Size = new Squid.Point(14, 10);
        pnl.VScroll.Slider.Style = "vscrollTrack";
        pnl.VScroll.Slider.Button.Style = "vscrollButton";
        pnl.VScroll.ButtonUp.Style = "vscrollUp";
        pnl.VScroll.ButtonUp.Size = new Squid.Point(10, 20);
        pnl.VScroll.ButtonDown.Style = "vscrollUp";
        pnl.VScroll.ButtonDown.Size = new Squid.Point(10, 20);
        pnl.VScroll.Slider.Margin = new Squid.Margin(0, 2, 0, 2);
        pnl.Parent = split.SplitFrame2;

        ListBox listbox1 = new ListBox();
        listbox1.Dock = DockStyle.Fill;
        listbox1.Size = new Squid.Point(200, 290);
        listbox1.Style = "frame2";
        listbox1.Parent = split.SplitFrame2;
        listbox1.ClipFrame.Margin = new Margin(8, 8, 8, 8);
        listbox1.Scrollbar.Margin = new Margin(0, 8, 8, 8);
        listbox1.Scrollbar.Size = new Squid.Point(14, 10);
        listbox1.Scrollbar.Slider.Style = "vscrollTrack";
        listbox1.Scrollbar.Slider.Button.Style = "vscrollButton";
        listbox1.Scrollbar.ButtonUp.Style = "vscrollUp";
        listbox1.Scrollbar.ButtonUp.Size = new Squid.Point(10, 20);
        listbox1.Scrollbar.ButtonDown.Style = "vscrollUp";
        listbox1.Scrollbar.ButtonDown.Size = new Squid.Point(10, 20);
        listbox1.Scrollbar.Slider.Margin = new Margin(0, 2, 0, 2);

        for (int i = 0; i < 30; i++)
        {
            ListBoxItem item = new ListBoxItem();
            item.Text = "listboxitem";
            item.Size = new Squid.Point(100, 70);
            item.Margin = new Margin(0, 0, 0, 4);
            item.Style = "item";
            item.Tooltip = "This is a\r\nmultiline tooltip";
            item.OnMouseEnter += new MouseEnterEventHandler(item_OnMouseEnter);
            item.OnMouseLeave += new MouseLeaveEventHandler(item_OnMouseLeave);
            listbox1.Items.Add(item);
        }

        TreeView treeview = new TreeView();
        treeview.Size = new Squid.Point(200, 330);
        treeview.Dock = DockStyle.Fill;
        treeview.Style = "frame2";
        treeview.Parent = split.SplitFrame1;
        treeview.ClipFrame.Margin = new Margin(8, 8, 8, 8);
        treeview.Scrollbar.Margin = new Margin(0, 8, 8, 8);
        treeview.Scrollbar.Size = new Squid.Point(14, 10);
        treeview.Scrollbar.Slider.Style = "vscrollTrack";
        treeview.Scrollbar.Slider.Button.Style = "vscrollButton";
        treeview.Scrollbar.ButtonUp.Style = "vscrollUp";
        treeview.Scrollbar.ButtonUp.Size = new Squid.Point(10, 20);
        treeview.Scrollbar.ButtonDown.Style = "vscrollUp";
        treeview.Scrollbar.ButtonDown.Size = new Squid.Point(10, 20);
        treeview.Scrollbar.Slider.Margin = new Margin(0, 2, 0, 2);

        for (int i = 0; i < 30; i++)
        {
            TreeNodeLabel node = new TreeNodeLabel();
            node.Button.Style = "item";
            node.Label.Text = "node level 1";
            node.Size = new Squid.Point(100, 35);
            node.Margin = new Margin(0, 0, 0, 4);
            node.Style = "item";
            node.Tooltip = node.Label.Text;
            treeview.Nodes.Add(node);

            for (int i2 = 0; i2 < 3; i2++)
            {
                TreeNodeLabel sub1 = new TreeNodeLabel();
                sub1.Button.Style = "item";
                sub1.Label.Text = "node level 2";
                sub1.Size = new Squid.Point(100, 35);
                sub1.Margin = new Margin(8, 0, 0, 4);
                sub1.Style = "item";
                sub1.Tooltip = sub1.Label.Text;
                node.Nodes.Add(sub1);

                for (int i3 = 0; i3 < 3; i3++)
                {
                    TreeNodeLabel sub2 = new TreeNodeLabel();
                    sub2.Button.Style = "item";
                    sub2.Label.Text = "node level 3";
                    sub2.Size = new Squid.Point(100, 35);
                    sub2.Margin = new Margin(16, 0, 0, 4);
                    sub2.Style = "item";
                    sub2.Tooltip = sub2.Label.Text;
                    sub1.Nodes.Add(sub2);
                }
            }
        }

        #endregion

        #region sample window 3 - Custom Control (Inheritance)

        SampleWindow window3 = new SampleWindow();
        window3.Size = new Point(440, 340);
        window3.Position = new Point(40, 400);
        window3.Resizable = true;
        window3.Style = "frame";
        window3.Titlebar.Text = "Custom Control (Inheritance)";
        window3.Parent = Desktop;

        ChatBox chatbox = new ChatBox();
        chatbox.Dock = DockStyle.Fill;
        window3.Controls.Add(chatbox);

        chatbox.Style = "frame2";
        chatbox.Input.Style = "textbox";
        chatbox.Input.Margin = new Margin(8, 0, 8, 8);
        chatbox.Output.Margin = new Margin(8, 8, 8, 8);
        //chatbox.Output.Style = "textbox";
        chatbox.Scrollbar.Margin = new Margin(0, 8, 8, 8);
        chatbox.Scrollbar.Size = new Squid.Point(14, 10);
        chatbox.Scrollbar.Slider.Style = "vscrollTrack";
        chatbox.Scrollbar.Slider.Button.Style = "vscrollButton";
        chatbox.Scrollbar.ButtonUp.Style = "vscrollUp";
        chatbox.Scrollbar.ButtonUp.Size = new Squid.Point(10, 20);
        chatbox.Scrollbar.ButtonDown.Style = "vscrollUp";
        chatbox.Scrollbar.ButtonDown.Size = new Squid.Point(10, 20);
        chatbox.Scrollbar.Slider.Margin = new Margin(0, 2, 0, 2);

        #endregion

        #region sample window 4 - TabControl, TextAlign

        SampleWindow window4 = new SampleWindow();
        window4.Size = new Point(440, 340);
        window4.Position = new Point(500, 400);
        window4.Resizable = true;
        window4.Style = "frame";
        window4.Titlebar.Text = "TabControl, TextAlign";
        window4.Parent = Desktop;

        TabControl tabcontrol = new TabControl();
        tabcontrol.Dock = DockStyle.Fill;
        tabcontrol.Parent = window4;
        tabcontrol.ButtonFrame.Dock = DockStyle.Bottom;

        for (int i = 0; i < 6; i++)
        {
            TabPage tabPage = new TabPage();
            tabPage.Style = "frame2";
            tabPage.Margin = new Margin(0, -1, 0, 0);
            tabPage.Button.Style = "button";
            tabPage.Button.Text = "page" + i;
            tabPage.Button.Tooltip = "hmyeah";
            tabPage.Button.Margin = new Margin(0, 0, -1, 0);
            tabcontrol.TabPages.Add(tabPage);

            Label lbl = new Label();
            lbl.Dock = DockStyle.Fill;
            lbl.Parent = tabPage;
            lbl.TextWrap = true;
            lbl.Text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam\r\n nonumy eirmod tempor invidunt ut labore [color=ffcc0000][url=testurl]click \r\n meh![/url][/color] et dolore magna aliquyam erat, sed diam voluptua.\r\n At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
            lbl.Style = "multiline";
            lbl.OnLinkClicked += new Label.LinkClickedEventHandler(lbl_OnLinkClicked);
        }

        #endregion

        #region sample window 5 - Panel, TextBox

        SampleWindow window5 = new SampleWindow();
        window5.Size = new Point(440, 340);
        window5.Position = new Point(960, 40);
        window5.Resizable = true;
        window5.Style = "frame";
        window5.Titlebar.Text = "Panel, TextBox";
        window5.Parent = Desktop;

        Panel panel = new Panel();
        panel.Style = "frame2";
        panel.Dock = DockStyle.Fill;
        panel.Parent = window5;

        panel.ClipFrame.Margin = new Margin(8);
        panel.ClipFrame.Style = "textbox";

        panel.VScroll.Margin = new Margin(0, 8, 8, 8);
        panel.VScroll.Size = new Squid.Point(14, 10);
        panel.VScroll.Slider.Style = "vscrollTrack";
        panel.VScroll.Slider.Button.Style = "vscrollButton";
        panel.VScroll.ButtonUp.Style = "vscrollUp";
        panel.VScroll.ButtonUp.Size = new Squid.Point(10, 20);
        panel.VScroll.ButtonDown.Style = "vscrollUp";
        panel.VScroll.ButtonDown.Size = new Squid.Point(10, 20);
        panel.VScroll.Slider.Margin = new Margin(0, 2, 0, 2);

        panel.HScroll.Margin = new Margin(8, 0, 8, 8);
        panel.HScroll.Size = new Squid.Point(10, 14);
        panel.HScroll.Slider.Style = "hscrollTrack";
        panel.HScroll.Slider.Button.Style = "hscrollButton";
        panel.HScroll.ButtonUp.Style = "hscrollUp";
        panel.HScroll.ButtonUp.Size = new Squid.Point(20, 10);
        panel.HScroll.ButtonDown.Style = "hscrollUp";
        panel.HScroll.ButtonDown.Size = new Squid.Point(20, 10);
        panel.HScroll.Slider.Margin = new Margin(2, 0, 2, 0);

        for (int i = 0; i < 10; i++)
        {
            Label label = new Label();
            label.Text = "label control:";
            label.Size = new Point(100, 35);
            label.Position = new Point(10, 10 + 45 * i);
            panel.Content.Controls.Add(label);

            TextBox txt = new TextBox();
            //txt.Mode = TextBoxMode.Numeric;
            txt.Text = "lorem ipsum";
            txt.Size = new Squid.Point(222, 35);
            txt.Position = new Point(110, 10 + 45 * i);
            txt.Style = "textbox";
            txt.AllowDrop = true;
            txt.TabIndex = 1 + i;
            txt.OnDragDrop += new DragDropEventHandler(txt_OnDragDrop);
            panel.Content.Controls.Add(txt);
        }

        #endregion

        #region sample window 6 - Misc

        SampleWindow window6 = new SampleWindow();
        window6.Size = new Point(440, 340);
        window6.Position = new Point(960, 400);
        window6.Resizable = true;
        window6.Style = "frame";
        window6.Titlebar.Text = "Misc";
        window6.Parent = Desktop;

        int bpos = 2;
        foreach (Alignment align in Enum.GetValues(typeof(Alignment)))
        {
            Button b1 = new Button();
            b1.Size = new Point(100, 20);
            b1.Text = align.ToString();
            b1.Position = new Point(10, bpos * 24);
            b1.Parent = window6;
            b1.Tag = align;
            b1.Style = "button";
            b1.OnMouseClick += new MouseClickEventHandler(b1_OnMouseClick);
            bpos++;
        }

        #endregion
    }	
	
	#region event handler
	
	void item_OnMouseLeave(Control sender)
    {
        sender.Animation.Stop();
        sender.Animation.Size(new Point(100, 70), 250);
    }

    void item_OnMouseEnter(Control sender)
    {
        sender.Animation.Stop();
        sender.Animation.Size(new Point(100, 140), 250);
    }

    void b1_OnMouseClick(Control sender)
    {
        ControlStyle style = GuiHost.GetStyle("multiline");
        style.TextAlign = (Alignment)sender.Tag;
    }

    void lbl_OnLinkClicked(string href)
    {
        MessageBox dialog = MessageBox.Show(new Point(300, 200), "Message Box", href, MessageBoxButtons.OKCancel, Desktop);
        dialog.OnDialogResult += new Dialog.DialogResultEventHandler(dialog_OnDialogResult);
        dialog.Animation.Custom(WalkSquare(dialog));
    }

    private System.Collections.IEnumerator WalkSquare(MessageBox dialog)
    {
        yield return dialog.Animation.Position(new Point(10, 10), 1000);
        yield return dialog.Animation.Position(new Point(1000, 10), 1000);
        yield return dialog.Animation.Position(new Point(1000, 600), 1000);
        yield return dialog.Animation.Position(new Point(10, 600), 1000);
    }

    void label1_OnMouseDown(Control sender)
    {
        Button btn = new Button();
        btn.Size = new Squid.Point(157, 26);
        btn.Text = "remember me";
        btn.Position = sender.Location;
        Desktop.DoDragDrop(btn);
    }

    void txt_OnDragDrop(Control sender, DragDropEventArgs e)
    {
        ((TextBox)sender).Text = ((Button)e.DraggedControl).Text;
    }

    void button_OnMouseClick(Control sender)
    {
        MessageBox dialog = MessageBox.Show(new Point(300, 200), "Message Box", "This is a modal dialog\r\nClick any button to close.", MessageBoxButtons.OKCancel, Desktop);
        dialog.OnDialogResult += new Dialog.DialogResultEventHandler(dialog_OnDialogResult);
    }

    void dialog_OnDialogResult(Dialog sender, DialogResult result)
    {
        // do something
    }
	
	#endregion
}
	