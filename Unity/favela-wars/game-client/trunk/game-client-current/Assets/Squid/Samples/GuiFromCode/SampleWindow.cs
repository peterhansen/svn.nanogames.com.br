﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;

public class SampleWindow : Window
{
    public TitleBar Titlebar { get; private set; }

    protected override void Initialize()
    {
        base.Initialize();

        AllowDragOut = true;
        Padding = new Margin(7);

        Titlebar = new TitleBar();
        Titlebar.Dock = DockStyle.Top;
        Titlebar.Size = new Squid.Point(122, 35);
        Titlebar.OnMouseDown += delegate(Control sender) { StartDrag(); };
        Titlebar.OnMouseUp += delegate(Control sender) { StopDrag(); };
        Titlebar.Cursor = Cursors.Move;
        Titlebar.Style = "frame2";
        Titlebar.Margin = new Margin(0, 0, 0, -1);
        Titlebar.Button.OnMouseClick += Button_OnMouseClick;
		
		Resizable = true;
        AllowDragOut = false;

        Controls.Add(Titlebar);
    }

    void Button_OnMouseClick(Control sender)
    {
        Animation.Custom(FadeAndClose());
    }

    private System.Collections.IEnumerator FadeAndClose()
    {
        Enabled = false;

        Animation.Size(new Point(Size.x, 10), 500);
        
        yield return Animation.Opacity(0, 500);

        Close();
    }
}

public class TitleBar : Label
{
    public Button Button { get; private set; }

    protected override void Initialize()
    {
        Button = new Button();
        Button.Size = new Point(30, 30);
        Button.Style = "button";
        Button.Tooltip = "Close Window";
        Button.Dock = DockStyle.Right;
        Button.Margin = new Margin(0, 8, 8, 8);
        Elements.Add(Button);
    }
}

