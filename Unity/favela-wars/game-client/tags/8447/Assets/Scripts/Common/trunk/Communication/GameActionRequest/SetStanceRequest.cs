using System;
using Utils;

namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para mudar a postura de um personagem.
	/// </summary>
	[ Serializable ]
	public class SetStanceRequest : Request {
		
		public int worldObjectID;
		
		public Stance newStance;
		
		public SetStanceRequest( int playerID, int worldObjectID, Stance stance ) : base( playerID ) {
			this.worldObjectID = worldObjectID;
			this.newStance = stance;
		}
	}
}

