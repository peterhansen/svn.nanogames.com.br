using System;

namespace GameCommunication {
	
	[ Serializable ]
	public class EndParticipationRequest : Request {
		
		public EndParticipationRequest( int playerID ) : base( playerID ) {
		}
	}
}