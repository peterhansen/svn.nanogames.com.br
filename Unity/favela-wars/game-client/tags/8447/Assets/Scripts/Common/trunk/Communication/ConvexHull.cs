using System;
using System.Text;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {	
	
	[ Serializable ]
    public class ConvexHull {
		
		private const int VERTEX_COUNT = 4;
		
		private Box box;
		private GameVector[] vertices;
        
		private int[] indices;

        private bool[] backFacing;
        
		private GameVector[] shadowVertices;
		
		private ISectorManager sm;

		private GameVector position;
		
		
		private enum ConvexType {
			Obstacle,
			Floor,
			Ceil,
		}
		
		
        public GameVector Position {
            get { return position; }
            set { position = value; }
        }
		
        
        public ConvexHull( Box box, GameVector[] points, GameVector position ) {
            this.box = box;
			this.position = position;

            vertices = new GameVector[ VERTEX_COUNT + 1 ];
            GameVector center = new GameVector();

            for ( int i = 0; i < VERTEX_COUNT; i++ ) {
                vertices[ i ] = points[ i ] + position;
                center += points[ i ];
            }            
            center /= points.Length;

            vertices[ VERTEX_COUNT ] = center;

            indices = new int[ VERTEX_COUNT + 2 ];

			indices[ 0 ] = VERTEX_COUNT;
			for ( int i = 0; i < vertices.Length; i++ ) {
                indices[ i + 1 ] = i;
            }
            indices[ VERTEX_COUNT + 1 ] = 0;

            backFacing = new bool[ VERTEX_COUNT ];
        }
		
		
		public void SetISectorManager( ISectorManager sm ) {
			 this.sm = sm;
		}
		
		
		public IEnumerable< ViewArea > GenerateShadowViewAreas( GameVector viewOrigin ) {
			viewOrigin = new GameVector( viewOrigin );
			viewOrigin.y = position.y;
			
            //compute facing of each edge, using N*L
            for ( int i = 0; i < VERTEX_COUNT; i++ ) {
                GameVector firstVertex = vertices[ i ];
                int secondIndex = ( i + 1 ) % VERTEX_COUNT;
                GameVector secondVertex = vertices[ secondIndex ];
                GameVector middle = ( firstVertex + secondVertex ) * 0.5f;

                GameVector L = viewOrigin - middle;

                GameVector N = new GameVector();
                N.x = - ( secondVertex.z - firstVertex.z );
                N.z = secondVertex.x - firstVertex.x;
               
                if( N.Dot( L ) > 0.0f )
                    backFacing[ i ] = false;
                else {
                    backFacing[ i ] = true;
				}
            }
            
            //find beginning and ending vertices which belong to the shadow
            int startingIndex = 0;
            int endingIndex = 0;
            for ( int i = 0; i < VERTEX_COUNT; i++ ) {
                int currentEdge = i;
                int nextEdge = ( i + 1 ) % VERTEX_COUNT;

                if ( backFacing[ currentEdge ] && !backFacing[ nextEdge ] )
                    endingIndex = nextEdge;

                if ( !backFacing[ currentEdge ] && backFacing[ nextEdge ] )
                    startingIndex = nextEdge;
            }

            int shadowVertexCount;

            //nr of vertices that are in the shadow
            if ( endingIndex > startingIndex )
                shadowVertexCount = endingIndex - startingIndex + 1;
            else
                shadowVertexCount = VERTEX_COUNT + 1 - startingIndex + endingIndex ;

            shadowVertices = new GameVector[ shadowVertexCount * 2 ];

            //create a triangle strip that has the shape of the shadow
            int currentIndex = startingIndex;
            int svCount = 0;
            while ( svCount != shadowVertexCount * 2 ) {
                GameVector vertexPos = vertices[ currentIndex ];
                
                //one vertex on the hull
                shadowVertices[svCount] = vertexPos;

                //one extruded by the light direction
				GameVector L2P = vertexPos - viewOrigin;
                L2P.Normalize();
                shadowVertices[ svCount + 1 ] = viewOrigin + L2P * Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT * 2.0f;
                
                svCount += 2;
                currentIndex = ( currentIndex + 1 ) % VERTEX_COUNT;
            }
			
			for ( int i = 0; i < shadowVertexCount * 2 - 2; i++ ) {
				yield return new ViewArea( shadowVertices[ i ], shadowVertices[ i + 1 ], shadowVertices[ i + 2 ] );			
			}
		}
		
		
		public void DrawShadows( GameVector viewOrigin, Dictionary< Sector, SectorVisibilityStatus > viewAreaSectors, IEnumerable< ViewArea > ignoredViewAreas ) {
			// TODO verificar corretamente se � piso
			
			Sector viewSector = sm.GetSectorFromGameVector( viewOrigin );
			
			Dictionary< Sector, int > ignoredSectors = null;
			
			ConvexType t;
			if ( box.GetScale().y < 0.4f ) {
				if ( position.y > viewOrigin.y )
					t = ConvexType.Ceil;
				else
					t = ConvexType.Floor;
			} else {
				t = ConvexType.Obstacle;
				
				if ( ignoredViewAreas != null ) {
					ignoredSectors = new Dictionary< Sector, int >();
					foreach( ViewArea ignoredViewArea in ignoredViewAreas ) {
						foreach( Sector sector in sm.Get3DSectorsFromViewArea( ignoredViewArea ) ) {
							ignoredSectors[ sector ] = 0;
						}
					}
				}
			}
			
			Dictionary< Sector, int > boxSectors = new Dictionary< Sector, int >();
			foreach( Sector s in sm.GetBoxSectors( box ) )
				boxSectors[ s ] = 0;
			
			float verticalLimit = Common.CharacterInfo.CHARACTER_V_VIEW_LIMIT;
			float verticalInit = 6;
			float invertedViewLimit = 1 / Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT;
			
			foreach ( ViewArea va in GenerateShadowViewAreas( viewOrigin ) ) {
				foreach( Sector s in sm.GetSectorsFromViewArea( va ) ) {
					int distance = Math.Abs( s.i - viewSector.i ) + Math.Abs( s.k - viewSector.k );
					
					int height = ( int ) Math.Round( NanoMath.Lerp( distance * invertedViewLimit, verticalInit, verticalLimit ) );
					
					switch ( t ) {
						case ConvexType.Floor:
							FillShadowSectors( s, Math.Max( 0, s.j - height ), s.j, viewAreaSectors );
						break;
						
						case ConvexType.Ceil:
							FillShadowSectors( s, s.j, s.j + height, viewAreaSectors );
							foreach ( Sector bs in boxSectors.Keys ) {
								if ( !viewAreaSectors.ContainsKey( bs ) || viewAreaSectors[ bs ] != SectorVisibilityStatus.FORCE_VISIBLE ) {
									viewAreaSectors[ bs ] = SectorVisibilityStatus.NOT_VISIBLE;
								}
							}
						break;
						
						case ConvexType.Obstacle:
							FillShadowSectorsIgnored( s, Math.Max( 0, s.j - height ), s.j + height, viewAreaSectors, ignoredSectors );
						
							foreach ( Sector bs in boxSectors.Keys ) {
								if ( viewAreaSectors.ContainsKey( bs ) )
									viewAreaSectors[ bs ] = SectorVisibilityStatus.FORCE_VISIBLE;
							}
						break;
					}
				}
			}
		}	
		
		
		private void FillShadowSectors( Sector s, int initialJ, int finalJ, Dictionary< Sector, SectorVisibilityStatus > viewAreaSectors ) {
			// varre os setores verticais ao setor 's'. Como n�o h� buracos entre setores na vertical, ao 
			// encontrar o primeiro setor inv�lido podemos interromper o loop.
			for ( int j = initialJ; j < finalJ; ++j ) {
				Sector s2 = new Sector( s.i, j, s.k );
				if ( s2.IsValid() ) {
					viewAreaSectors[ s2 ] = SectorVisibilityStatus.NOT_VISIBLE;
				} else {
					break;
				}
			}
		}	
		
		
		private void FillShadowSectors( Sector s, int initialJ, int finalJ, Dictionary< Sector, SectorVisibilityStatus > viewAreaSectors, IEnumerable< ViewArea > ignoredViewAreas ) {
			if( ignoredViewAreas == null ) {
				FillShadowSectors( s, initialJ, finalJ, viewAreaSectors );
			} else {
				Dictionary< Sector, int > ignoredSectors = new Dictionary< Sector, int >();
				foreach( ViewArea ignoredViewArea in ignoredViewAreas ) {
					ignoredViewArea.Draw();
					foreach( Sector sector in sm.Get3DSectorsFromViewArea( ignoredViewArea ) ) {
						ignoredSectors[ sector ] = 0;
					}
				}
				
				// varre os setores verticais ao setor 's'. Como n�o h� buracos entre setores na vertical, ao 
				// encontrar o primeiro setor inv�lido podemos interromper o loop.
				for ( int j = initialJ; j < finalJ; ++j ) {
					Sector s2 = new Sector( s.i, j, s.k );
					if ( s2.IsValid() ) {
						if ( !ignoredSectors.ContainsKey( s2 ) ) {
							viewAreaSectors[ s2 ] = SectorVisibilityStatus.NOT_VISIBLE;
						}
					} else {
						break;
					}
				}
			}
		}
		
		
		private void FillShadowSectorsIgnored( Sector s, int initialJ, int finalJ, Dictionary< Sector, SectorVisibilityStatus > viewAreaSectors, Dictionary< Sector, int > ignoredSectors ) {
			if( ignoredSectors == null ) {
				FillShadowSectors( s, initialJ, finalJ, viewAreaSectors );
			} else {
				// varre os setores verticais ao setor 's'. Como n�o h� buracos entre setores na vertical, ao 
				// encontrar o primeiro setor inv�lido podemos interromper o loop.
				for ( int j = initialJ; j < finalJ; ++j ) {
					Sector s2 = new Sector( s.i, j, s.k );
					if ( s2.IsValid() ) {
						if ( !ignoredSectors.ContainsKey( s2 ) ) {
							viewAreaSectors[ s2 ] = SectorVisibilityStatus.NOT_VISIBLE;
						}
					} else {
						break;
					}
				}
			}		
		}
		
		
	}
	
}
