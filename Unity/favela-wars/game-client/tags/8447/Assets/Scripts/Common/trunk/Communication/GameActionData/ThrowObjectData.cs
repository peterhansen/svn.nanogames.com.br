using System;
using System.Text;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	public class ThrowObjectData : GameActionData {
		
		public List< ThrowInfo > throwsAndHits = new List< ThrowInfo >();
		
		public int characterWorldID;
		
		public float timeToDestroy;
		
		
		public ThrowObjectData( int characterWorldID, List< ThrowInfo > throwsAndHits ) : this( characterWorldID, throwsAndHits, -1 ) {
		}
		
		
		public ThrowObjectData( int characterWorldID, List< ThrowInfo > throwsAndHits, float timeToDestroy ) {
			this.characterWorldID = characterWorldID;
			this.throwsAndHits = throwsAndHits;
			this.timeToDestroy = timeToDestroy;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as ThrowObjectData );
		}
		
		
		public bool Equals( ThrowObjectData data ) {
			return data != null && characterWorldID == data.characterWorldID && throwsAndHits.Equals( data.throwsAndHits );
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
	}
	
	
	public class ThrowInfo {
		
		/// <summary>
		/// Parábola descrita pelo arremesso.
		/// </summary>
		public Parabola parabola;
		
		/// <summary>
		/// Limite de tempo da parábola. Após este tempo, o script de arremesso avançará para a próxima parábola, ou
		/// encerrará a execução caso seja a última. Normalmente, esse valor indica o tempo em que o objeto arremessado
		/// colidirá pela próxima vez, mas pode ser mais curto, indicando a explosão de uma granada no ar, por exemplo.
		/// </summary>
		public float timeLimit;
		
		
		public ThrowInfo( Parabola parabola, float timeLimit ) {
			this.parabola = parabola;
			this.timeLimit = timeLimit;
		}
		
		
		public override bool Equals( Object obj ) {
			return Equals( ( ThrowInfo ) obj );
		}
		
		
		public bool Equals( ThrowInfo t ) {
			try {
				return timeLimit == t.timeLimit && parabola.Equals( t.parabola );
			} catch ( NullReferenceException ) {
				return false;
			}
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		

		public static bool operator ==( ThrowInfo s1, ThrowInfo s2 ) {
			try {
				return s1.Equals( s2 );
			} catch ( NullReferenceException ) {
				return s2 == null;
			}
		}
		
		
		public static bool operator !=( ThrowInfo s1, ThrowInfo s2 ) {
			return !( s1 == s2 );
		}		
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[ThrowInfo parabola:" );
			sb.Append( parabola );
			sb.Append( " time:" );
			sb.Append( timeLimit );
			sb.Append( "]" );
			
			return sb.ToString();
		}
		
	}	
}

