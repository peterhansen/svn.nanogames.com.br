using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GameCommunication {
	
	[ Serializable ]
	public enum CharacterKnownStatus {
		NotSeen,
		SeenPreviously,
		CurrentlySeen
	}
	
	
	[ Serializable ]
	public class CharacterListData : Dictionary< int, CharacterKnownStatus > {
		
		public Dictionary< int, Utils.Faction > Factions = new Dictionary< int, Utils.Faction >();
		
		public Dictionary< int, bool > Alive = new Dictionary< int, bool >();
		
		// Mapeia um WorldObjectId com um bool que diz se ele já foi visto antes
		// ou está sendo visto agora
		//public  charactersKnown = new Dictionary< int , CharacterKnownStatus >();
		
		
		public CharacterListData() {
		}
		
		
		public CharacterListData( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
		}			
		
	}
}

