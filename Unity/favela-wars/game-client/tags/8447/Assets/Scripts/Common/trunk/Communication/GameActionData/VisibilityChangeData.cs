using System;
using System.Collections.Generic;
using Utils;

namespace GameCommunication {
	
	[Serializable]
	public class VisibilityChangeData : GameActionData {
		
		public Faction faction;
		
		public List< Sector > newlyVisibleSectors = new List< Sector >();
		
		public Dictionary< int, int > visibleWorldObjectIds = new Dictionary< int, int >();
		
		public CharacterListData characterList = new CharacterListData();
					
		
		public VisibilityChangeData( Faction faction ) {
			this.faction = faction;
		}
		
		
		public VisibilityChangeData( VisibilityChangeData other ) : this( other.faction ) {
			foreach( int id in other.visibleWorldObjectIds.Keys ) {
				if ( other.characterList[ id ] != CharacterKnownStatus.NotSeen ) {
					visibleWorldObjectIds[ id ] = 1;
				}
				
				characterList[ id ] = other.characterList[ id ];
			}
			
			newlyVisibleSectors = new List< Sector >( other.newlyVisibleSectors );
		}
		
		
		public bool IsVisible( int id ) {
			return visibleWorldObjectIds.ContainsKey( id );
		}
		
		
		public void setAsInvisible( int id, bool seenBefore ) {
			if ( visibleWorldObjectIds.ContainsKey( id ) )
				visibleWorldObjectIds.Remove( id );
			
			if ( seenBefore )
				characterList[ id ] = CharacterKnownStatus.SeenPreviously;
			else
				characterList[ id ] = CharacterKnownStatus.NotSeen;
		}
		
		
		public void AddToVisible( int id ) {
			visibleWorldObjectIds[ id ] = 1;
			characterList[ id ] = CharacterKnownStatus.CurrentlySeen;
		}
		
	}
	
}
