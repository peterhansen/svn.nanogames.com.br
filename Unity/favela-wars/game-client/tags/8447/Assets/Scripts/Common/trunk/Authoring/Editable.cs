using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Utils;


[ Serializable ]
public abstract class Editable : ISerializable {
	
	private const string KEY_LIST_STRING = "_keys";
	
	
	public Editable() {
	}
	
	
	public Editable( SerializationInfo info, StreamingContext ctxt ) {
		List< string > keys = ( List< string > ) info.GetValue( KEY_LIST_STRING, typeof( List< string > ) );
		
		Type type = GetType();
		foreach ( string key in keys ) {
			PropertyInfo p = type.GetProperty( key );
			MethodInfo setter = p.GetSetMethod();
			
			if ( setter != null ) {
				try {
					string serializedObject = info.GetString( key );
					object deserializedObject = Util.DeserializeFromBase64String( serializedObject );
					
					setter.Invoke( this, new object[] { deserializedObject } );
				} catch ( Exception e ) {
					Debugger.LogError( key + " -> " + setter.Name + "\n" + e );
					throw e;
				}
			} else {
				Debugger.LogWarning( "Setter not found: " + key );
			}
		}
	}
	        
	
	//Serialization function.
	public virtual void GetObjectData( SerializationInfo info, StreamingContext ctxt ) {
		PropertyInfo[] infos = GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance );
		
		List< string > keys = new List< string >();
			
		foreach ( PropertyInfo p in infos ) {
			MethodInfo getter = p.GetGetMethod();
			keys.Add( p.Name);
			
			object got = getter.Invoke( this, null );
			string serializedValue = Util.SerializeToBase64String( got );
			info.AddValue( p.Name, serializedValue );
		}
		
		info.AddValue( KEY_LIST_STRING, keys );
	}
	
}

