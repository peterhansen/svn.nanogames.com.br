using System;

namespace GameCommunication {
	
	public class DealDamageRequest : Request {
		
		public int damagingCharacterID;
		
		public int damagedCharacterID;
		
		public int damage;
		
		public DealDamageRequest( int playerID, int damagingCharacterID, int damagedCharacterID, int damage ) : base( playerID ) {
			this.damagingCharacterID = damagingCharacterID;
			this.damagedCharacterID = damagedCharacterID;
			this.damage = damage;
		}
	}
}

