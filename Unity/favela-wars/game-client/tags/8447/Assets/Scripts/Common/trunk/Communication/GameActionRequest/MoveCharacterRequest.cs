using System;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para movimentar um personagem. 
	/// </summary>
	[ Serializable ]
	public class MoveCharacterRequest : Request {
		
		public int worldObjectID;
		
		public List< GameVector > path;
		
		public GameVector finalDirection;
		
		public Stance finalStance;
		
		public MoveCharacterRequest( int playerID, int worldObjectID, List< GameVector > path, GameVector finalDirection, Stance finalStance ) : base( playerID ) {
			this.worldObjectID = worldObjectID;
			this.path = path;
			this.finalDirection = finalDirection;
			this.finalStance = finalStance;
		}
	
	}
}

