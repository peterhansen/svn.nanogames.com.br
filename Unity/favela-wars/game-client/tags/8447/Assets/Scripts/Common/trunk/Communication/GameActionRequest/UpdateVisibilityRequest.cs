using System;


namespace GameCommunication {
	
	[ Serializable ]
	public class UpdateVisibilityRequest : Request {
		
		public int watcherId;
		
		public UpdateVisibilityRequest( int playerID, int watcherId ) : base( playerID ) {
			this.watcherId = watcherId;
		}
	}
}
