using System;

namespace GameCommunication
{
	[ Serializable ]
	public class WayPoint
	{
	
		private GameVector position;
		
		public WayPoint (){
			this.position = new GameVector( 0f , 0f , 0f );
		}
		
		public WayPoint (float x, float y, float z){
			this.position = new GameVector( x , y , z );
		}
		
		public WayPoint (GameVector v){
			this.position = new GameVector( v );
		}
		
		~WayPoint(){
			position = null;
		}
		
		public void setPosition(float x, float y, float z){
			this.position = new GameVector( x , y , z );
		}
		
		public GameVector getPosition(){
			return new GameVector(this.position);
		}
	}
}

