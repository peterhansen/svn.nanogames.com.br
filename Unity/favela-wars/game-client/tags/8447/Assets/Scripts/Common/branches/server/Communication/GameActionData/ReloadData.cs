using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	[ Serializable ]
	public class ReloadData : GameActionData {
		
		/// <summary>
		/// ID do personagem.
		/// </summary>
		public int characterWorldID;
		
		
		public ReloadData( int characterWorldID ) {
			this.characterWorldID = characterWorldID;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as ReloadData );
		}
		
		
		public bool Equals( ReloadData data ) {
			return data != null && characterWorldID == data.characterWorldID;
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
		
	}
}

