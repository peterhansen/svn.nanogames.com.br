using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para arremessar um objeto.
	/// </summary>
	[ Serializable ]
	public class ThrowObjectRequest : Request {
		
		public int characterWorldID;
		
		public int objectID;
		
		public GameVector direction;
		
		public ThrowObjectRequest( int playerID, int shooterID, int objectID, GameVector direction ) : base( playerID ) {
			this.characterWorldID = shooterID;
			this.objectID = objectID;
			this.direction = direction;
		}
	
	}
}

