using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameCommunication {
	
	[ Serializable ]
	public class GroundBox : Box {
		
		float[] heights = new float[ 4 ];
		int[] middleIndexes = new int[ 2 ];
		
		public void SetHeights( float h0, float h1, float h2, float h3 ) {
			
			heights[ 0 ] = h0;
			heights[ 1 ] = h1;
			heights[ 2 ] = h2;
			heights[ 3 ] = h3;
		}
		
		public float[] GetHeights() {
			return heights;
		}
		
		public int[] GetMiddleIndexes() {
			return middleIndexes;
		}
		
		public void SetMiddleIndices( int i0, int i1 ) {
			
			middleIndexes[ 0 ] = i0;
			middleIndexes[ 1 ] = i1;
		}
		
		public void DebugDraw() {
			GameTransform gt = GetGameTransform();
			GameVector[] points = gt.GetPoints();
			GameVector gv1 = new GameVector( points[ middleIndexes[ 0 ] ] );
			GameVector gv2 = new GameVector( points[ middleIndexes[ 1 ] ] );
			gv1.y = heights[ 1 ];
			gv2.y = heights[ 3 ];
			gt.DebugDraw();
			
			
			GameVector o;
			GameVector v;
			GameVector u;

			o = new GameVector( points[ 0 ] );
			v = points[ middleIndexes[ 1 ] ];
			u = points[ middleIndexes[ 0 ] ];
			o.y = heights[ 0 ];
			u.y = heights[ 1 ];
			v.y = heights[ 3 ];			
			Debugger.DrawLine( o, v, 0xFFFF00FF, 10.0f );
			Debugger.DrawLine( o, u, 0xFF00FFFF, 10.0f );
			Debugger.DrawLine( v, u, 0xFF0F0FFF, 10.0f );
			
			o = new GameVector( points[ 3 ] );
			v = points[ middleIndexes[ 0 ] ];
			u = points[ middleIndexes[ 1 ] ];
			o.y = heights[ 2 ];
			u.y = heights[ 3 ];
			v.y = heights[ 1 ];			
			Debugger.DrawLine( o, v, 0xFFFF00FF, 10.0f );
			Debugger.DrawLine( o, u, 0xFF00FFFF, 10.0f );
			Debugger.DrawLine( v, u, 0xFF0F0FFF, 10.0f );
		}
		
		
		
		private RayBoxInterceptionReply CheckWithTriangle( GameVector origin, GameVector target, int index ) {
			
			RayBoxInterceptionReply rbir = base.Intercept( origin, target );
			RayBoxInterceptionReply nullAnswer = new RayBoxInterceptionReply();
			nullAnswer.entry = null;
			nullAnswer.exit = null;
			
			GameTransform gt = GetGameTransform();
			GameVector[] points = gt.GetPoints();
			
			GameVector o, u, v;
			if( index == 0 ) {
				o = new GameVector( points[ 0 ] );
				v = points[ middleIndexes[ 1 ] ];
				u = points[ middleIndexes[ 0 ] ];
				o.y = heights[ 0 ];
				u.y = heights[ 1 ];
				v.y = heights[ 3 ];			
			} else {
				o = new GameVector( points[ 3 ] );
				v = points[ middleIndexes[ 0 ] ];
				u = points[ middleIndexes[ 1 ] ];
				o.y = heights[ 2 ];
				u.y = heights[ 3 ];
				v.y = heights[ 1 ];		
			}
			v = v - o;
			u = u - o;
			
			
			GameVector v0 = origin;
			GameVector d = target - v0;
			GameVector vt;
			GameVector k;
			
			GameVector normal = u.Cross( v );
			
			float t;
			float A;
			float B;
			float C;
			float D;
			A = normal.x;
			B = normal.y;
			C = normal.z;
			D = - ( A * o.x + B * o.y + C * o.z ); 
			
			float tUnder = ( A * d.x + B * d.y + C * d.z ); 
			
			// TODO: caso essa hipótese falhe, nosso vetor está "paralelo" ao plano. Talvez valha a pena testar caso ele faca parte do plano (nesse caso, 
			// ele nao so intercepta em um ponto como em todos).
			if( !NanoMath.Equals( tUnder, 0.0f ) ) {
				
				t = ( -D - A * v0.x - B * v0.y - C * v0.z ) / tUnder;
				vt = v0 + d * t;
				k = vt - o;
				

				float det = v.x * u.y - v.y * u.x;
				if( !NanoMath.Equals( det, 0.0f ) ) {
					float a = ( u.y * k.x - u.x * k.y ) / det;
					float b = ( v.x * k.y - v.y * k.x ) / det;
					
					if ( rbir.entry == null )
						rbir.entry = new RayBoxInterception();
					
					if ( rbir.exit == null )
						rbir.exit = new RayBoxInterception();
					
					if ( NanoMath.GreaterEquals( b, 0.0f ) && NanoMath.GreaterEquals( a, 0.0f ) && NanoMath.LessEquals( a + b, 1.1f ) ) {
						
						//preenche a resposta adequada
						rbir.entry.point = vt;
						rbir.entry.normal = normal;
						rbir.entry.t = t;
					} else {
						rbir.entry = null;
						rbir.exit = null;
					}
				}
				else {
					det = v.x * u.z - v.z * u.x;
					
					if( !NanoMath.Equals( det, 0.0f ) ) {
						float a = ( u.z * k.x - u.x * k.z ) / det;
						float b = ( v.x * k.z - v.z * k.x ) / det;
						
						if ( rbir.entry == null )
							rbir.entry = new RayBoxInterception();
						
						if ( rbir.exit == null )
							rbir.exit = new RayBoxInterception();
						
						if ( NanoMath.GreaterEquals( b, 0.0f ) && NanoMath.GreaterEquals( a, 0.0f ) && NanoMath.LessEquals( a + b, 1.1f ) ) {
							
							//preenche a resposta adequada
							rbir.entry.point = vt;
							rbir.entry.normal = normal;
							rbir.entry.t = t;
						} else {
							rbir.entry = null;
							rbir.exit = null;
						}
					}
					else {
						throw new ArithmeticException( "Sistema para detectar colisão com Ground Box sem solucao única!");
					}
				}
			}
			
			return rbir;
		}
		
		public override RayBoxInterceptionReply Intercept( GameVector origin, GameVector target ) {
			
			RayBoxInterceptionReply rbir = base.Intercept( origin, target );
			
			RayBoxInterceptionReply prism0;
			RayBoxInterceptionReply prism1;
			
			
			prism0 = CheckWithPrism( origin, target, 0 );
			prism1 = CheckWithPrism( origin, target, 1 );
			
			if ( prism0.entry != null && prism1.entry != null ) {
				
				if ( prism0.entry.t <= prism1.entry.t ) {
				
					rbir.entry = prism0.entry;
					rbir.exit = prism1.entry;
				} else {

					rbir.entry = prism1.entry;
					rbir.exit = prism0.entry;
				}
			} else {
				
				if ( prism0.entry != null && prism1.entry == null ) {

					rbir.entry = prism0.entry;
				} else if ( prism0.entry == null && prism1.entry != null ) {

					rbir.entry = prism1.entry;
				} else {
					
					rbir.entry = null;
					rbir.exit = null;
				}
			}
			
			
			return rbir;
		}
		
		public RayBoxInterceptionReply CheckWithPrism( GameVector origin, GameVector target, int index ) {
			
			RayBoxInterceptionReply rbir = base.Intercept( origin, target );
			RayBoxInterceptionReply nullAnswer = new RayBoxInterceptionReply();
			nullAnswer.entry = null;
			nullAnswer.exit = null;
			float A;
			float B;
			float C;
			float D;
			GameTransform gt = GetGameTransform();
			GameVector[] points = gt.GetPoints();

			GameVector o, u, v;
			if( index == 0 ) {
				o = new GameVector( points[ 0 ] );
				v = points[ middleIndexes[ 1 ] ];
				u = points[ middleIndexes[ 0 ] ];
				o.y = heights[ 0 ];
				u.y = heights[ 1 ];
				v.y = heights[ 3 ];			
			} else {
				o = new GameVector( points[ 3 ] );
				v = points[ middleIndexes[ 0 ] ];
				u = points[ middleIndexes[ 1 ] ];
				o.y = heights[ 2 ];
				u.y = heights[ 3 ];
				v.y = heights[ 1 ];		
			}
			
			v = v - o;
			u = u - o;
			
			GameVector normal = u.Cross( v );
			A = normal.x;
			B = normal.y;
			C = normal.z;
			D = - ( A * o.x + B * o.y + C * o.z ); 
			
			GameVector hitEntry = rbir.entry == null ? origin : rbir.entry.point;
			GameVector hitExit = rbir.exit == null ? target : rbir.exit.point;
			
			float y0 = ( A * hitEntry.x + B * hitEntry.y + C * hitEntry.z + D );
			float y1 = ( A * hitExit.x + B * hitExit.y + C * hitExit.z + D );
			
			
			
			if ( NanoMath.GreaterThan( y0, 0.0f ) && NanoMath.GreaterThan( y1, 0.0f ) ) {
				return nullAnswer;
			} else if ( NanoMath.LessThan( y0, 0.0f ) && NanoMath.LessThan( y1, 0.0f ) ) {
				return rbir;
			} else 
				return CheckWithTriangle( origin, target, index );
		}
		
	}
}