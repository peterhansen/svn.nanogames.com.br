using System;

using Utils;
using SharpUnit;


namespace GameCommunication {
	
	[ Serializable ]
	public class Attribute : object {
		
		/// <summary>
		/// Valor mínimo do atributo. Caso haja um modificador do atributo, o valor atual pode ficar abaixo do mínimo.
		/// </summary>
		protected float min = 0;
		
		/// <summary>
		/// Valor máximo do atributo. Caso haja um modificador do atributo, o valor atual pode exceder o máximo.
		/// </summary>
		protected float max = 100;
		
		/// <summary>
		/// Valor atual (sem modificadores).
		/// </summary>
		protected float current;
		
		/// <summary>
		/// Limites absolutos do atributo.
		/// </summary>
		protected Attribute absLimits;
		
		
		public Attribute() {
		}
		
		
		public Attribute( Attribute a ) : this( a.current, a.min, a.max ) {
		}		
		
		
		public Attribute( Attribute a, Attribute absLimits ) : this( a ) {
			this.absLimits = absLimits;
		}		
		
		
		public Attribute( float currentValue, float min, float max ) {
			this.min = min;
			this.max = max;
			Value = currentValue;
		}
		
		
		public virtual void Set( Attribute other ) {
			min = other.min;
			max = other.max;
			current = other.current;
		}
		
		
		/// <summary>
		/// Calcula o valor proporcional deste atributo em função da porcentagem de outro atributo. Este método é usado
		/// em casos de atributos relacionados, como técnica e precisão (50% de técnica = 50% da precisão máxima).
		/// </summary>
		public float Lerp( Attribute a ) {
			return Min + ( a.AbsPercent * ( Max - Min ) );
		}
		
		
		/// <summary>
		/// Valor mínimo do atributo. Caso haja um modificador do atributo, o valor atual pode ficar abaixo do mínimo.
		/// </summary>
		[ ToolTip( "Valor mínimo do atributo." ) ]
		public float Min {
			get {
				return min;
			}
			
			set {
				min = Math.Min( value, max );
				CheckRange();
			}
		}
		
		
		/// <summary>
		/// Valor máximo do atributo. Caso haja um modificador do atributo, o valor atual pode exceder o máximo.
		/// </summary>
		[ ToolTip( "Valor máximo do atributo." ) ]
		public float Max {
			get {
				return max;
			}
			
			set {
				max = Math.Max( value, min );
				CheckRange();
			}
		}		
		
		
		/// <summary>
		/// Valor atual (sem modificadores).
		/// </summary>
		[ ToolTip( "Valor atual do atributo." ) ]
		public virtual float Value {
			get {
				return current;
			}
			
			set {
				current = value;
				CheckRange();
			}
		}	
		
		
		public float Percent {
			get { return NanoMath.Clamp( ( Value - Min ) / ( Max - Min ), 0, 1 ); }
		}
		
		
		public float AbsPercent {
			get {
				return absLimits == null ? Percent : ( ( Value - absLimits.Min ) / ( absLimits.Max - absLimits.Min ) );
			}
		}
		
		
		/// <summary>
		/// Stevia sintático para atribuir Value para o valor máximo permitido.
		/// </summary>
		public void ValueToMax() {
			Value = Max;
		}
		
		
		/// <summary>
		/// ZeroCal sintático para atribuir Value para o valor mínimo permitido.
		/// </summary>
		public void ValueToMin() {
			Value = Max;
		}
		
		
		protected void CheckRange() {
			if ( current < min )
				current = min;
			else if ( current > max )
				current = max;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as Attribute );
		}
		
		
		public bool Equals( Attribute a ) {
			return a != null && min == a.min && max == a.max && Value == a.Value;
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		
		
		public override string ToString() {
			return string.Format("[Attribute: Min={0}, Max={1}, Value={2}]", Min, Max, Value );
		}
		
		
		/// <summary>
		/// </summary>
		public static implicit operator float ( Attribute a ) {
		    return a.Value;
		}		
		
		
		public static Attribute operator + ( Attribute a, float i ) {
			a.Value += i;
			return a;
		}
		
		
		public static Attribute operator + ( float i, Attribute a ) {
			return a + i;
		}
	
		
		public static Attribute operator - ( Attribute a, float i ) {
			a.Value -= i;
			return a;
		}
		
		
		public static Attribute operator - ( float i, Attribute a ) {
			return a - i;
		}

		
		public static bool operator == ( Attribute a, float i ) {
			return a.Value == i;
		}
		
		
		public static bool operator == ( Attribute a, Attribute b ) {
			try {
				return a.Value == b.Value;
			} catch ( NullReferenceException ) {
				return ( ( ( object ) a ) == null ) == ( ( ( object ) b ) == null );
			}
		}		
		
		
		public static bool operator != ( Attribute a, Attribute b ) {
			try {
				return !( a.Value == b.Value );
			} catch ( NullReferenceException ) {
				return ( ( ( object ) a ) == null ) != ( ( ( object ) b ) == null );
			}
		}		
		
		
		public static bool operator >= ( Attribute a, Attribute b ) {
			return a.Value >= b.Value;
		}
		
		
		public static bool operator <= ( Attribute a, Attribute b ) {
			return a.Value <= b.Value;
		}
		
		
		public static bool operator > ( Attribute a, Attribute b ) {
			return a.Value > b.Value;
		}
		
		
		public static bool operator < ( Attribute a, Attribute b ) {
			return a.Value < b.Value;
		}
		
		
		public static bool operator == ( float i, Attribute a ) {
			return a.Value == i;
		}
	
		
		public static bool operator != ( Attribute a, float i ) {
			return !( a == i );
		}
		
		
		public static bool operator != ( float i, Attribute a ) {
			return !( a == i );
		}
	
		
		public static bool operator >= ( Attribute a, float i ) {
			return a.Value >= i;
		}
		

		public static bool operator <= ( Attribute a, float i ) {
			return a.Value <= i;
		}	
		
		
		public static bool operator > ( Attribute a, float i ) {
			return a.Value > i;
		}
		

		public static bool operator < ( Attribute a, float i ) {
			return a.Value < i;
		}
		

		public static float operator * ( Attribute a, float i ) {
			return a.Value * i;
		}
		
		
		public static float operator / ( Attribute a, float i ) {
			return a.Value / i;
		}
		

		public static float operator % ( Attribute a, float i ) {
			return a.Value % i;
		}

	}
	
	
	internal class AttributeTestCase : TestCase {
		
		private const int MIN_VALUE = -1000000;
		
		private const int MAX_VALUE = 1000000;
		
		private Attribute a;
		
		private Attribute b;
		
		private Random r = new Random();
		
		
		public override void SetUp() {
			
		}
	
		
		public override void TearDown() {
		}
		
		
		[UnitTest]
		public void TestRanges() {
			for ( int i = 0; i < 10; ++i ) {
				a = new Attribute( r.Next( MIN_VALUE, 0 ), r.Next( 1, MAX_VALUE ), r.Next() );
				Assert.True( a.Value >= a.Min && a.Value <= a.Max );
			}
		}
		
		
		[UnitTest]
		public void TestConvertions() {
			a = new Attribute( 56, 3, 90 );
			Assert.True( a == 56 );
			Assert.True( a <= 56 );
			Assert.True( a >= 56 );
			Assert.False( a < 56 );
			Assert.False( a > 56 );

			Assert.False( a == 50 );
			Assert.True( a < 60 );
			Assert.True( a > 50 );
		}	
		
		
		[UnitTest]
		public void TestComparisons() {
			a = new Attribute( 50, 8, 300 );
			b = new Attribute( 50, 8, 300 );
			Assert.True( a == b );
			Assert.True( b == a );
			Assert.True( a.Equals( b ) );
			Assert.True( b.Equals( a ) );
			Assert.False( a != b );
			Assert.False( b != a );
			
			b = new Attribute( 123, 8, 300 );
			Assert.False( a == b );
			Assert.False( b == a );
			Assert.False( a.Equals( b ) );
			Assert.False( b.Equals( a ) );
			Assert.True( a != b );
			Assert.True( b != a );
			
			// os valores atuais são iguais; == deve ser true, Equals deve ser false
			b = new Attribute( 50, -248, 300 );
			Assert.True( a == b );
			Assert.True( b == a );
			Assert.False( a.Equals( b ) );
			Assert.False( b.Equals( a ) );
			Assert.False( a != b );
			Assert.False( b != a );

			// os valores atuais são iguais; == deve ser true, Equals deve ser false
			b = new Attribute( 50, 8, 875 );
			Assert.True( a == b );
			Assert.True( b == a );
			Assert.False( a.Equals( b ) );
			Assert.False( b.Equals( a ) );
			Assert.False( a != b );
			Assert.False( b != a );
		}
		
		
		[UnitTest]
		public void TestSerialization() {
			a = new Attribute( 321, 123, 500 );
			b = new Attribute( 321, 123, 500 );
			
			string sa = Util.SerializeToBase64String( a );
			string sb = Util.SerializeToBase64String( b );
			
			Assert.True( sa.Equals( sb ) );
			
			for ( int i = 0; i < 10; ++i ) {
				a = new Attribute( r.Next( MIN_VALUE, 0 ), r.Next( 1, MAX_VALUE ), r.Next() );
				sa = Util.SerializeToBase64String( a );
				b = Util.DeserializeFromBase64String< Attribute >( sa );
				
				Assert.True( a.Equals( b ) );
			}
		}
		
	}
	
}

