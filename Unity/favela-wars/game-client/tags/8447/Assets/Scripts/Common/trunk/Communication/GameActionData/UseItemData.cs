using System;

namespace GameCommunication {
	
	[Serializable]
	public class UseItemData : GameActionData {
		
		public int characterID;
		
		public string useItemMessage;
		
		public UseItemData( int characterID, string useItemMessage ) {
			this.characterID = characterID;
			this.useItemMessage = useItemMessage;
		}
	}
}

