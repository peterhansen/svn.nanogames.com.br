using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public abstract class MissionObjective {
		
		public bool isActive = true;
		
		// TODO: precisamos ser capazes de configurar essa quantia
		protected int remainingTurns = 10;
		
		public List< MissionObjective > objectivesToActivate = new List< MissionObjective >();
		
		public List< MissionObjective > objectivesToDeactivate = new List< MissionObjective >();
		
		
		public virtual void OnInit( Mission mission ) {
		}
		
		
		public ObjectiveResult OnTurnChanged( Mission mission, TurnData turnData ) {
			return isActive? ProcessResult( CheckWhenTurnChanged( mission, turnData ) ) : new ObjectiveResult();
		}
		
		
		public ObjectiveResult OnCharacterMoved( Mission mission, Character character ) { 
			return isActive? ProcessResult( CheckWhenCharacterMoved( mission, character ) ) : new ObjectiveResult();
		}
		
		
		public ObjectiveResult OnCharacterKilled( Mission mission, Character character ) { 
			return isActive? ProcessResult( CheckWhenCharacterKilled( mission, character ) ) : new ObjectiveResult();
		}
		
		protected ObjectiveResult ProcessResult( ObjectiveResult result ) {
			// Caso o resultado seja de fim de missão, mas tenhamos algum objetivo
			// ainda para ativar ou desativar...
			if( result.missionIsOver && ( objectivesToActivate.Count > 0 || objectivesToDeactivate.Count > 0 ) ) {
				
				// ..."absorvemos" o fim da partida, e, na verdade, vamos ativá-los.
				result.missionIsOver = false;
				
				foreach( MissionObjective objective in objectivesToActivate ) {
					objective.isActive = true; 
				}
			
				foreach( MissionObjective objective in objectivesToDeactivate ) {
					objective.isActive = false; 
				}
			}
			
			return result;
		}
		
		
		protected virtual ObjectiveResult CheckWhenTurnChanged( Mission mission, TurnData turnData ) { 
			return new ObjectiveResult(); 
		}
	
		
		protected virtual ObjectiveResult CheckWhenCharacterMoved( Mission mission, Character character ) { 
			return new ObjectiveResult(); 
		}
		
		
		protected virtual ObjectiveResult CheckWhenCharacterKilled( Mission mission, Character character ) { 
			return new ObjectiveResult(); 
		}
		
	}
	
}
