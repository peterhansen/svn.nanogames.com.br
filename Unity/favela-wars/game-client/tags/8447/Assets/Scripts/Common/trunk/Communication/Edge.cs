using System;
using Utils;



namespace GameCommunication {
	public class Edge {
		
		public class Span {
			
			public float x1;
			public float x2;
			
			public Span( float x1, float x2 ) {
		        if( x1 < x2 ) {
	                this.x1 = x1;
	                this.x2 = x2;
		        } else {
	                this.x1 = x2;
	                this.x2 = x1;
		        }
			}
		}
		
		public GameVector p1;
		public GameVector p2;
		public Edge connectedToP1;
		public Edge connectedToP2;
		
		public Edge( GameVector p1, GameVector p2 ) {
			this.p1 = new GameVector( p1 );
			this.p2 = new GameVector( p2 );
			connectedToP1 = null;
			connectedToP2 = null;
		}
		
		
	
		public void Intersect( Edge edge, out float t1, out float t2 ) {
			float x1;
			float x2;
			float x3;
			float x4;
			float y1;
			float y2;
			float y3;
			float y4;
			float under;
			
			t1 = float.MaxValue;
			t2 = float.MaxValue;
			
			x1 = edge.p1.x;
			x3 = p1.x;
			y1 = edge.p1.z;
			y3 = p1.z;
			
			x2 = edge.p2.x;
			x4 = p2.x;
			y2 = edge.p2.z;
			y4 = p2.z;
			
			under = ( (y4-y3) * (x2-x1) - (x4-x3) * (y2-y1) );
			
			if ( NanoMath.Equals( under, 0.0f )  )
				return;

			t1 = ( (x4-x3) * (y1-y3) - (y4-y3) * (x1-x3) ) / under;
			t2 = ( (x2-x1) * (y1-y3) - (y2-y1) * (x1-x3) ) / under;
			
		}		
		
		public void DebugDraw( uint color ) {
			Debugger.DrawLine( p1, p2, color, 10.0f );
		}
		
		
		public static Edge EdgeWithClosestFirst( GameVector p1, GameVector p2 ) {
			
			Edge edge;
			
	        if( p1.z < p2.z ) {
				edge = new Edge( p1, p2 );
	        } else {
				edge = new Edge( p2, p1 );
	        }			
			
			return edge;
		}
	}
}

