using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para usar um item.
	/// </summary>
	[ Serializable ]
	public class UseItemRequest : Request {
		
		public int characterID;
		
		public int itemID;
		
		public GameVector destination;
		
		public int targetCharacterID = -1;
		
		
		public UseItemRequest( int playerID, int characterID, int itemID, GameVector destination, int targetCharacterID ) : base( playerID ) {
			this.characterID = characterID;
			this.itemID = itemID;
			this.destination = destination;
			this.targetCharacterID = targetCharacterID;
		}
	
	}
}

