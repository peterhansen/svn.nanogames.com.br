using System;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameCommunication {
	
	[ Serializable ]
	public class KillCharacterData : MissionObjectiveData {
		
		public CharacterTargetMode target;
		
		public Faction winningFaction;
		
		public int targetedCharacterID;
		
		
		public KillCharacterData() {
		}
		
		
		public KillCharacterData( Faction winningFaction, CharacterTargetMode target ) {
			this.target = target;
			this.winningFaction = winningFaction;
		}
		
	}
	
}

