using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	[ Serializable ]
	public class Action	{
	
		protected string title;
		
		protected Action parent;
		
		protected List< Action > children = new List< Action >();
		
		protected Type interactionControllerClass;
		
		public Action( string title ) {
			this.title = title;
		}
		
		public Type getInteractionControllerClass() {
			return interactionControllerClass;
		}

		
		public void SetInteractionControllerClass( Type handlerClass ) {
			interactionControllerClass = handlerClass;
		}
		
		public string GetTitle() {
			return title;
		}
		
		
		public void SetParent( Action parent ) {
			this.parent = parent;
		}
		
		
		public Action GetParent() {
			return parent;
		}
		
		
		public List< Action > GetChildren() {
			return children;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="a">
		/// A <see cref="Action"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Boolean"/>
		/// </returns>
		public bool IsChildOf( Action a ) {
			Action p = GetParent();
		
			while ( p != null && p != a )
				p = p.GetParent();
				
			return p != null;	
		}
		
		
		public void AddChild( Action child ) {
			child.SetParent( this );
			children.Add( child );
		}
		
		
		public bool HasChildren() {
			return children.Count > 0;
		}
		
		public Action GetChildWith( String title ) {
			foreach ( Action child in children ) {
				if ( child.GetTitle().Equals( title ) )
					return child;
			}
			
			return null;
		}

		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[ Action title:" );
			sb.Append( title );
			sb.Append( " children(" );
			sb.Append( children.Count );
			sb.Append( ")" );
			foreach ( Action a in children ) {
				sb.Append( a );
			}
			sb.Append( " ]" );
			
			return sb.ToString();
		}
		
		
	}
}

