using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using GameCommunication;
using GameController;
using Utils;


public class ActionsMenuControl : RadialMenuControl {
	
	protected Desktop desktop;
	
	protected GameAction rootAction;
	
	protected GameObject selectedCharacter;
	
	protected ActionMenuInteractionController interactionController;
	
	protected List< int > enemiesWorldIDs;
	
	
	public ActionsMenuControl ( 
			ActionMenuInteractionController interactionController, 
		 	GameObject selectedCharacter, 
		 	GameAction rootAction, 
		 	List< int > enemiesWorldIDs ) : base( new Point( Screen.width / 2, Screen.height / 2 ) ) {
		this.interactionController = interactionController;
		this.selectedCharacter = selectedCharacter;
		this.enemiesWorldIDs = enemiesWorldIDs;
		
		// criamos nossa ação raiz a partir das ações recebidas do servidor
		this.rootAction = new GameAction( ActionType.ROOT );
		foreach( GameAction characterAction in rootAction.GetChildren() )
			this.rootAction.AddChild( characterAction );
		
		this.rootAction.AddChild( new GameAction( ActionType.GET_INFO ) );
		this.rootAction.AddChild( new GameAction( ActionType.HIDE_MENU ) );
		SelectAction( this.rootAction );
	}
	
	
	public void AddAction( GameAction action ) {
		foreach ( GameAction childGameAction in action.GetChildren() )
			AddButton( new RadialMenuOption( GetActionIcon( childGameAction ), ClickOnAction, childGameAction ) );
		
		GameAction parentAction = action.GetParent();
		if( parentAction != null && parentAction != action )
			AddCentralButton( new RadialMenuOption( GetActionIcon( action ), ClickOnAction, parentAction ) );
	}
	
	
	protected void ClickOnAction( Control sender ) {
		GameAction action = sender.Tag as GameAction;
		
		switch ( action.GetActionType() ) {
			// TODO: aqui, bastaria saber se a Game Action possui filhos?	
			case ActionType.NONE:
			case ActionType.ROOT:
			case ActionType.SELECT_WEAPON:
			case ActionType.TACTICS:
			case ActionType.GRENADE:
				SelectAction( action );
				break;
			
			case ActionType.HIDE_MENU:
				GUIManager.Reset();
				break;
			
			default:
				object[] parameters = new object[] { selectedCharacter, action };
				Type[] types = new Type[] { typeof( GameObject ), typeof( GameAction ) };
				
				// obtém a classe tratadora. Botões que não tem ação específica apenas fecham o menu
				Type t = GetInteractionControllerType( action.GetActionType() );
				if ( t != null ) {
					ActionInteractionController newIC = ( ActionInteractionController ) t.GetConstructor( types ).Invoke( parameters );
					newIC.SetParent( interactionController );
					newIC.SetEnemiesList( enemiesWorldIDs );
				
					GUIManager.SetInteractionController( newIC );
				}
				break;
		}
	}
	
	
	private Type GetInteractionControllerType( ActionType t ) {
		switch ( t ) {
			case ActionType.GET_INFO:
				return typeof( GetCharacterInfoInteractionController );
				
			case ActionType.MOVE:
				return typeof( MoveInteractionController );
				
			case ActionType.RELOAD:
				return typeof( ReloadInteractionController );
				
			case ActionType.ATTACK_REGULAR:
			case ActionType.ATTACK_BURST:
				return typeof( ShootInteractionController );
				
			case ActionType.THROW:
				return typeof( ThrowInteractionController );
				
			case ActionType.SET_STANCE:
				return typeof( SetStanceInteractionController );

			case ActionType.USE_ITEM:
				return typeof( UseItemInteractionController );

			case ActionType.DROP_ITEM:
				return null; // TODO implementar ação de descartar item
			
			default:
				return null;
		}
	}
	
	
	protected RadialMenuButton.RadialMenuIconType GetActionIcon( GameAction gameAction ) {
		switch( gameAction.GetActionType() ) {
		case ActionType.GET_INFO:
			return RadialMenuButton.RadialMenuIconType.CHARACTER_INFO;
			
		// TODO: ainda não está completo. Precisamos especificar ícones para cada tipo de arma
		case ActionType.SELECT_WEAPON:
			return gameAction.Hand == AttackGameAction.ActionHand.LEFT? RadialMenuButton.RadialMenuIconType.HAND_LEFT : RadialMenuButton.RadialMenuIconType.HAND_RIGHT;
			
		case ActionType.HIDE_MENU:
			return RadialMenuButton.RadialMenuIconType.CLOSE_MENU;
			
		case ActionType.TACTICS:
			return RadialMenuButton.RadialMenuIconType.TACTICS;
			
		case ActionType.ATTACK_REGULAR:
			return RadialMenuButton.RadialMenuIconType.ATTACK_REGULAR;
			
		case ActionType.ATTACK_BURST:
			return RadialMenuButton.RadialMenuIconType.ATTACK_BURST;
			
		case ActionType.RELOAD:
			return RadialMenuButton.RadialMenuIconType.RELOAD;
			
		case ActionType.GRENADE:
			return RadialMenuButton.RadialMenuIconType.GRENADE;
			
		case ActionType.THROW:
			return RadialMenuButton.RadialMenuIconType.THROW;
			
		case ActionType.DROP_ITEM:
			return RadialMenuButton.RadialMenuIconType.DROP_ITEM;
			
		case ActionType.SET_STANCE:
			return RadialMenuButton.RadialMenuIconType.CHANGE_STANCE;
		
		default:
			return RadialMenuButton.RadialMenuIconType.NONE;
		}
	}
	
	
	protected void SelectAction( GameAction gameAction ) {
		Clear();
		AddAction( gameAction );
		Show();
	}
}
