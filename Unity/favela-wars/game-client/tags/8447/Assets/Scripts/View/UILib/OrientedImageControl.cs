using System;

using Squid;


public class OrientedImageControl : Squid.Controls.ImageControl {
   
	public enum ImageOrientation {
		NO_ROTATION,
		ROTATE_90_DEGREES,
		ROTATE_180_DEGREES,
		ROTATE_270_DEGREES
	}
	
	protected readonly UVCoords[] orientationUVs = {
		new UVCoords( 0, 0, 1, 1 ),
		new UVCoords( 0, 1, 1, 0 ),
		new UVCoords( 1, 1, 0, 0 ),
		new UVCoords( 1, 0, 0, 1 )
	};
	
	protected ImageOrientation imageOrientation = ImageOrientation.NO_ROTATION;
	public ImageOrientation Orientation {
		get {
			return imageOrientation;
		}
		set {
			imageOrientation = value;
		}
	}

    protected override void DrawStyle( Style style, float opacity ) {
        int texture = GuiHost.Renderer.GetTexture(Texture);
        int color = ColorInt.FromArgb(opacity, -1);
		
        if( texture > -1 )
            GuiHost.Renderer.DrawTexture(texture, Location.x, Location.y, Size.x, Size.y, orientationUVs[ ( int ) imageOrientation ], color);
    }
}

