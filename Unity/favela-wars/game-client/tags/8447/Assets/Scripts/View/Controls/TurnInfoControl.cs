using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;

public class TurnInfoControl : Frame {
	
	protected Frame toolbar;
	protected bool collapsed = true;
	
	protected Button expandCollapseButton;
	protected Button changeTurnButton;
	protected Button characterListButton;
	
	protected Button decreaseVisibleLayerButton;
	protected Button increaseVisibleLayerButton;
	
	protected Button nextCharacterButton;
	
		
	
	protected override void Initialize() {
		base.Initialize();

		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}
	
		Desktop desktop = guiManager.Desktop;
		
		Position = new Point( 0, 0 );
		Size = new Point( 1000, 1000 );
		Parent = desktop;
		desktop.Controls.Add( this );
		
		toolbar = new Frame();
		toolbar.Size = new Point( 247, 66 );
		toolbar.Position = new Point( 0, 0 );
		toolbar.Scissor = true;
		toolbar.Style = "camera_toolbar";
		toolbar.Parent = this;
		
		
		expandCollapseButton = new Button();
		expandCollapseButton.Size = new Point( 48, 67 );
		expandCollapseButton.Position = new Point( 200, 0 );
		expandCollapseButton.Parent = toolbar;
//		expandCollapseButton.Opacity = 0.0f;
		expandCollapseButton.OnMouseClick += new MouseClickEventHandler( ExpandCollapseCameraToolbar );
		
		changeTurnButton = new Button();
		changeTurnButton.Size = new Point( 150, 20 );
		changeTurnButton.Position = new Point( 10, 100 );
		changeTurnButton.Parent = this;
		changeTurnButton.Text = "Passar o Turno";
		changeTurnButton.OnMouseClick += new MouseClickEventHandler( ChangeTurn );
		
		characterListButton = new Button();
		characterListButton.Size = new Point( 150, 20 );
		characterListButton.Position = new Point( 10, 130 );
		characterListButton.Parent = this;
		characterListButton.Text = "Lista de Personagens";
		characterListButton.OnMouseClick += new MouseClickEventHandler( ListCharacters );
		
		decreaseVisibleLayerButton = new Button();
		decreaseVisibleLayerButton.Size = new Point( 150, 20 );
		decreaseVisibleLayerButton.Position = new Point( 10, 30 );
		decreaseVisibleLayerButton.Parent = this;
		decreaseVisibleLayerButton.Text = "Descer camada";
		decreaseVisibleLayerButton.OnMouseClick += new MouseClickEventHandler( ReduceVisibleLayer );

		increaseVisibleLayerButton = new Button();
		increaseVisibleLayerButton.Size = new Point( 150, 20 );
		increaseVisibleLayerButton.Position = new Point( 10, 60 );
		increaseVisibleLayerButton.Parent = this;
		increaseVisibleLayerButton.Text = "Subir camada";
		increaseVisibleLayerButton.OnMouseClick += new MouseClickEventHandler( IncreaseVisibleLayer );

		nextCharacterButton = new Button();
		nextCharacterButton.Size = new Point( 150, 20 );
		nextCharacterButton.Position = new Point( 10, 200 );
		nextCharacterButton.Parent = this;
		nextCharacterButton.Text = "Próximo soldado";
		nextCharacterButton.OnMouseClick += new MouseClickEventHandler( NextSoldier );
	
	}
	
	
	private void ExpandCollapseCameraToolbar( Control sender ) {
		collapsed = !collapsed;
		toolbar.Animation.Stop();
		toolbar.Animation.Position( new Point( collapsed? -199 : 0, 0 ), 500 );
	}
	
	
	private void RotateLeft( Control sender ) {
		IsoCameraScript isoCamera = IsoCameraScript.GetInstance();
		isoCamera.RotateLeft();
	}
		
	
	private void RotateRight( Control sender ) {
		IsoCameraScript isoCamera = IsoCameraScript.GetInstance();
		isoCamera.RotateRight();
	}
	
	
	private void ChangeTurn( Control sender ) {
		GameManager.GetInstance().SendRequest( new EndParticipationRequest( GameManager.GetInstance().GetLocalPlayer() ) );
	}
	
	
	private void ListCharacters( Control sender ) {		
		CharactersListInteractionController clic = new CharactersListInteractionController();
		GUIManager.SetInteractionController( clic );
	}
	
	
	private void ReduceVisibleLayer( Control sender ) {
		BuildingInfo.ReduceVisibleLayer();
		RefreshLayerButtons();
	}
	
	
	private void IncreaseVisibleLayer( Control sender ) {
		BuildingInfo.IncreaseVisibleLayer();
		RefreshLayerButtons();
	}
	
	
	private void RefreshLayerButtons() {
		decreaseVisibleLayerButton.Enabled = BuildingInfo.CanDecreaseLayer();
		increaseVisibleLayerButton.Enabled = BuildingInfo.CanIncreaseLayer();
	}
	
	
	/// <summary>
	/// Cicla a câmera entre os soldados do jogador. Caso não haja um último soldado selecionado, o primeiro soldado da
	/// lista é selecionado.
	/// </summary>
	private void NextSoldier( Control sender ) {
		GameManager gameManager = GameManager.GetInstance();
		WorldManager worldManager = WorldManager.GetInstance();
		
		List< GameObject > validCharacters = worldManager.GetFactionCharacters( gameManager.GetTurnFaction() );
		
		int index = -1;
		for ( int i = 0; i < validCharacters.Count; ++i ) {
			if ( gameManager.LastSelectedSoldier == validCharacters[ i ] ) {
				index = i;
				break;
			}
		}
		GameObject soldier = validCharacters[ ( index + 1 ) % validCharacters.Count ];
		gameManager.LastSelectedSoldier = soldier;
		
		IsoCameraScript.GetInstance().MoveTo( soldier.transform.position );
	}
	
}
