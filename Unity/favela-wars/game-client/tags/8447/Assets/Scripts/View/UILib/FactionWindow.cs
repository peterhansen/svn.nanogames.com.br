using Utils;
using Squid;
using System;
using UnityEngine;
using GameController;
using GameCommunication;

public class FactionWindow : Window {
	
	private Faction faction;
	protected FactionWindowDecoration decor;
	protected HolographicPanel panel;
	
	
	public FactionWindow() {
		
	}
	
	private void Dismiss( Control sender ) {
		GUIManager.Reset();
	}
	
	protected override void Initialize () {
		base.Initialize();
		
		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}

		Desktop desktop = guiManager.Desktop;
		
		
		Size = new Point( 422, desktop.Size.y );
		Position = new Point( 5, 0 );
		Parent = Desktop;
		desktop.Controls.Add( this );
		
		panel = new HolographicPanel();
		panel.Parent = this;
		panel.CenterAround( Size.x / 2, 43, 395, Size.y );
		panel.Content.Size = panel.Size;
//		panel.Position = panel.Position;
//		panel.Content.Size = new Point( ( int )( panel.Size.x * 0.4 ), panel.Size.y );
//		panel.Content.Style = "holoPanel";
//		HolographicPanel panel2;
//		panel2 = new HolographicPanel();
//		panel2.Parent = this;
//		panel2.CenterAround( Size.x / 2, 43, 380, Size.y );
//		panel2.Content.Size = new Point( ( int )( panel2.Size.x * 0.95 ), panel2.Size.y );
//		panel2.Style = "holoPanel";
//		
//		panel = panel2;
//		
		
		decor = new FactionWindowDecoration();
		decor.Parent = this;
		decor.SetSizeForDecorations( Faction.POLICE, Size );		
		
		Button btn = new Button();
		btn.Parent = this;
		btn.OnMouseClick += new MouseClickEventHandler( Dismiss );
		btn.Size = new Point( 100, 30 );
		btn.Text = "Fechar";	
	}
	
//	public void Dismiss( Control sender ) {
//		GUIManager.Reset();
//	}
}


