using System;

using GameController;


public class RadialMenuOption {
	
	public RadialMenuButton.RadialMenuIconType icon;
	
	public object tag;
	
	public RadialMenuControl.RadialMenuControlOnClick callback;
	
	public RadialMenuOption( RadialMenuButton.RadialMenuIconType icon, RadialMenuControl.RadialMenuControlOnClick callback ) : this( icon, callback, null ) {
	}
	
	public RadialMenuOption( RadialMenuButton.RadialMenuIconType icon, RadialMenuControl.RadialMenuControlOnClick callback, object tag ) {
		this.icon = icon;
		this.tag = tag;
		this.callback = callback;
	}
}
