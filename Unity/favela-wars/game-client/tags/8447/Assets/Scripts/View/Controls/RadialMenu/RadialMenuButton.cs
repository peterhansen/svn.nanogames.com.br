using System;

using UnityEngine;

using Squid;
using Squid.Controls;


public class RadialMenuButton : Frame {
	
	public const int BUTTON_SIDE_SIZE = 78;
	
	protected const int ICON_SIDE_SIZE = 58;
	
	protected const float FLOAT_REFRESH = Mathf.PI * 4.0f;
	
	protected const string ICONS_PATH = "GUI/ActionsMenu/Icons/";
	
	public enum RadialButtonType {
		DEFAULT,
		RIGHT,
		LEFT
	}
	
	public enum RadialMenuIconType {
		NONE,
		OK,
		CANCEL,
		BACK,
		CHARACTER_INFO,
		TACTICS,
		TAKE_COVER,
		CLOSE_MENU,
		HAND_LEFT,
		HAND_RIGHT,
		GRENADE,
		ATTACK_REGULAR,
		ATTACK_BURST,
		RELOAD,
		THROW,
		USE_ITEM,
		DROP_ITEM,
		CHANGE_STANCE
	}
	
	protected RadialButtonType type;
	
	public Button content;
	
	//protected ImageControl icon;
	// TODO: por enquanto, não temos todos os itens, então precisamos criar uma abstração para a idéia de ícone.
	// vamos usar strings quando ele não existir ainda
	public Control icon;
	
	// usado para a flutuada do botão
	protected float accTime = 0.0f;
	
	public RadialMenuButton( RadialButtonType type, RadialMenuIconType iconType ) {
		this.type = type;
		Size = new Point( BUTTON_SIDE_SIZE, BUTTON_SIDE_SIZE );
		
		// configurando fundo do botão
		content = new Button();
		switch( type ) {
		case RadialButtonType.DEFAULT:
			content.Style = "actionsmenubutton";
			break;
			
		case RadialButtonType.LEFT:
			content.Style = "actionsmenuleftbutton";
			break;
			
		case RadialButtonType.RIGHT:
			content.Style = "actionsmenurightbutton";
			break;
		}
		Controls.Add( content );
		
		// adicionando ícone, caso ele exista
		string iconName = GetIconPath( iconType );
		if( iconName != null ) {
			icon = new ImageControl();
			icon.AllowFocus = false;
			icon.NoEvents = true;
			ImageControl iconAsImage = icon as ImageControl;
			iconAsImage.Texture = iconName;
			Controls.Add( icon );
		} else {
			content.Text = Enum.GetName( typeof( RadialMenuIconType ), iconType );
			content.TextAlign = Alignment.MiddleCenter;
		}
	}
	
	
	protected override void OnUpdate() {
		base.OnUpdate();
		
		// controlando leve flutuada
		if( accTime > FLOAT_REFRESH )
			accTime -= FLOAT_REFRESH;
		
		// fazer o floating...
	}
	
	
	
	protected string GetIconPath( RadialMenuIconType radialMenuIcon ) {
		switch( radialMenuIcon ) {
		case RadialMenuIconType.CHARACTER_INFO:
			return ICONS_PATH + "Profile.png";
			
		case RadialMenuIconType.TACTICS:
			return ICONS_PATH + "Tactics.png";
			
		case RadialMenuIconType.HAND_LEFT:
			return ICONS_PATH + "HandLeft.png";
			
		case RadialMenuIconType.HAND_RIGHT:
			return ICONS_PATH + "HandRight.png";
			
		case RadialMenuIconType.GRENADE:
			return ICONS_PATH + "Object.png";
			
		default:
			// TODO: carregar aqui imagem default
			return null;
		}
	}
}

