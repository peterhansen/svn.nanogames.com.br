using System;
using System.Collections;

using UnityEngine;

using Squid;

using Utils;
using GameCommunication;


public class LoadResourceControl : TextBox {

	

	
	protected int percent = 0;
	
	protected override void Initialize () {
		base.Initialize();
		
		GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
		if( guiManager == null ) {
			Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
			return;
		}
		Desktop desktop = guiManager.Desktop;
		
		Size = new Point( 120, 40 );
		Position = new Point( 100, 100 );
		Text = "Loading...";
		Parent = Desktop;
	}
	
//	public bool RenderGUI () {
//		GUI.BeginGroup( new Rect( 20.0f, 50.0f, 200.0f, 100.0f ) );
//		GUI.Box( new Rect( 0.0f, 0.0f, 200.0f, 100.0f ), "Loading: " + percent + "%" );
//		GUI.Button( new Rect( 50.0f, 50.0f, percent, 20.0f ), " " );
//		GUI.EndGroup();
//		
//		return false;
//	}
	
	
	public void Tick( float progress ) {
		percent = ( int ) ( 100.0f * progress );
		
		Text = "   Loading... " + percent + "%";
	}
}
