using System;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using GameCommunication;
using GameController;
using Utils;


public class RadialMenuControl : Frame {
	
	private const float RADIAL_MENU_RADIUS = 300.0f;
	
	private const float BUTTON_SLIDING_TIME = 1000.0f;
	
	public delegate void RadialMenuControlOnClick( Control sender );
	
	protected List< RadialMenuButton > buttons = new List< RadialMenuButton >();
	
	protected RadialMenuButton centralButton;
	
	
	public List< RadialMenuButton > Buttons {
		get{ return buttons; }
	}
	
	
	public RadialMenuControl( Point position ) {
		Position = position - new Point( (int) RADIAL_MENU_RADIUS, (int) RADIAL_MENU_RADIUS );
		Size = new Point( 2 * (int) RADIAL_MENU_RADIUS, 2 * (int) RADIAL_MENU_RADIUS );
	}
	
	
	/// <summary>
	/// Adiciona um botão ao Menu Radial correspondente a uma Radial Menu Option. Lembre-se de chamar o método
	/// Show para posicionar os botões corretamente após todas as opções terem sido inseridas.
	/// </summary>
	/// <returns>
	/// O Button que foi criado a partir da Radial Menu Option.
	/// </returns>
	/// <param name='option'>
	/// A Radial Menu Option
	/// </param>
	/// <seealso cref="Show"/>
	public RadialMenuButton AddButton( RadialMenuOption option ) {
		RadialMenuButton button = CreateButton( option );
		buttons.Add( button );
		return button;
	}
	
	
	public RadialMenuButton AddCentralButton( RadialMenuOption option ) {
		centralButton = CreateButton( option );
		return centralButton;
	}
	
	
	protected RadialMenuButton CreateButton( RadialMenuOption option ) {
		RadialMenuButton.RadialButtonType radialButtonType = RadialMenuButton.RadialButtonType.DEFAULT;
		if( option.tag != null ) {
			GameAction optionGameAction = option.tag as GameAction;
			switch( optionGameAction.Hand ) {
			case GameAction.ActionHand.LEFT:
				radialButtonType = RadialMenuButton.RadialButtonType.LEFT;
				break;
				
			case GameAction.ActionHand.RIGHT:
				radialButtonType = RadialMenuButton.RadialButtonType.RIGHT;
				break;
			}
		}
		
		RadialMenuButton button = new RadialMenuButton( radialButtonType, option.icon );
		button.Position = new Point( (int) RADIAL_MENU_RADIUS, (int) RADIAL_MENU_RADIUS );
		button.content.Tag = option.tag == null? option : option.tag;
		button.content.OnMouseClick += new MouseClickEventHandler( option.callback );
		Controls.Add( button );
		return button;
	}
	
	
	public void Show() {
		Visible = true;
		int numberOfOptions = buttons.Count;
		if( numberOfOptions == 0 ) {
			Debugger.LogError( "RadialMenuControl: radial menu with no options trying to be shown." );
			return;
		}
		
		// posicionando botão central do meio
		const int buttonHalfSize = ( RadialMenuButton.BUTTON_SIDE_SIZE >> 1 );
		
		Point buttonFinalPosition = new Point( 
			(int) RADIAL_MENU_RADIUS - buttonHalfSize, 
			(int) RADIAL_MENU_RADIUS - buttonHalfSize 
		);
		
		if( centralButton != null )
			centralButton.Animation.Custom( Slide( centralButton, buttonFinalPosition ) );
		
		// posicionando botões da borda
		float angle = Mathf.PI / 2.0f;
		float dAngle = ( 2.0f * Mathf.PI ) / (float) numberOfOptions;
		for( int i = 0; i < numberOfOptions; i++ ) {
			int dX = (int) ( ( RADIAL_MENU_RADIUS / 2.0f ) * Mathf.Cos( angle ) );
			int dY = (int) ( ( RADIAL_MENU_RADIUS / 2.0f ) * Mathf.Sin( angle ) );
			angle += dAngle;
			
			Point targetPosition = new Point( (int) RADIAL_MENU_RADIUS + dX - buttonHalfSize, (int) RADIAL_MENU_RADIUS + dY - buttonHalfSize );
			buttons[ i ].Animation.Custom( Slide( buttons[ i ], targetPosition ) );
		}
	}
	
	
	public void Clear() {
		if( centralButton != null ) {
			centralButton.Parent = null;
			centralButton = null;
		}
		
		foreach ( RadialMenuButton b in buttons )
			b.Parent = null;
		
		buttons.Clear();
	}
	
	
	private System.Collections.IEnumerator Slide( RadialMenuButton button, Point targetPosition ) {
		button.content.Animation.Size( new Point( RadialMenuButton.BUTTON_SIDE_SIZE, RadialMenuButton.BUTTON_SIDE_SIZE ), BUTTON_SLIDING_TIME );
		if( button.icon != null )
			button.icon.Animation.Size( new Point( RadialMenuButton.BUTTON_SIDE_SIZE, RadialMenuButton.BUTTON_SIDE_SIZE ), BUTTON_SLIDING_TIME );
		yield return button.Animation.Position( targetPosition, BUTTON_SLIDING_TIME );
		Animation.Stop();
	}
}

