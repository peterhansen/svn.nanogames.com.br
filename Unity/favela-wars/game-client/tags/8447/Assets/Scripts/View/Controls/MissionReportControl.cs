using System;

using UnityEngine;

using Squid;

using Utils;
using GameController;


public class MissionReportControl : Window {	
			
	private const float HEADER_SCREEN_HEIGHT_FRACTION = 0.20f;
	
	private const float CONTENTS_SCREEN_HEIGHT_FRACTION = 0.65f;
	
	private const float FOOTER_SCREEN_HEIGHT_FRACTION = 0.145f;
	
	private BorderPanel headerPanel;
	
	private ContentPanel contentPanel;
	
	private BorderPanel footerPanel;
	
	protected override void Initialize() {
		base.Initialize();
		
		// configurando tamanho da janela
		Position = new Point( ( Screen.width - BorderPanel.BAR_WIDTH ) >> 1, 0 );
		Size = new Point( BorderPanel.BAR_WIDTH, Screen.height );
		
		// adicionando controles ao layout
		headerPanel = new HeaderPanel();
		Controls.Add( headerPanel );
		
		contentPanel = new ContentPanel();
		Controls.Add( contentPanel );
		
		footerPanel = new FooterPanel();
		Controls.Add( footerPanel );
		
		const int verticalSpacing = 2;
		int h = Position.y;
		
		headerPanel.Position = new Point( 0, h );
		headerPanel.Size = new Point( Size.x, (int) ( Size.y * HEADER_SCREEN_HEIGHT_FRACTION ) );
		
		h += verticalSpacing + headerPanel.Size.y;
		contentPanel.Position = new Point( 0, h );
		contentPanel.Size = new Point( Size.x, (int) ( Size.y * CONTENTS_SCREEN_HEIGHT_FRACTION ) );
		
		h += verticalSpacing + contentPanel.Size.y;
		footerPanel.Position = new Point( 0, h );
		footerPanel.Size = new Point( Size.x, (int) ( Size.y * FOOTER_SCREEN_HEIGHT_FRACTION ) );
	}	
		
	
	protected class ContentPanel : SectionFrame {
	}
	
	
	protected class BorderPanel : Frame {	
		
		public const int BAR_WIDTH = 754;
		
		public const int BAR_HEIGHT = 25;
		
		private const int CONTENT_PANEL_PADDING = 10;
		
		private bool topPanel;
		
		private OrientedImageControl bar;
		
		private SectionFrame contentPanel;
		
		
		public BorderPanel( bool topPanel ) {
			this.topPanel = topPanel;
						
			contentPanel = new SectionFrame();
			contentPanel.Position = new Point( CONTENT_PANEL_PADDING, 0 );
			Controls.Add( contentPanel );
			
			bar = new OrientedImageControl();
			bar.Texture = "GUI/MissionReportScreen/TopBar_EndGame";
			
			if( !topPanel )
				bar.Orientation = OrientedImageControl.ImageOrientation.ROTATE_180_DEGREES;
			Controls.Add( bar );
			
			OnSizeChanged += new SizeChangedEventHandler( OnResize );
		}
		
		protected virtual void OnResize( Control sender ) {
			bar.Size = new Point( BAR_WIDTH, BAR_HEIGHT );
			if( !topPanel )
				bar.Position = new Point( 0, Size.y - BAR_HEIGHT );

			contentPanel.Size = new Point( sender.Size.x - 2 * CONTENT_PANEL_PADDING, sender.Size.y );
		}
	}
	
	protected class HeaderPanel : BorderPanel {
		
		public HeaderPanel() : base( true ) {
		}
	}
	
	protected class FooterPanel : BorderPanel {
		
		private const int BUTTON_VERTICAL_PADDING = 5;
		
		private Label label;
		
		private Button button;
		
		public FooterPanel() : base( false ) {
			label = new Label();
			label.Text = L10n.Get( "MISSION_REPORT_FOOTER" );
			
			button = new Button();
			button.Text = L10n.Get( "EXIT" );
			// TODO: de onde pegar a fonte?
			
			OnSizeChanged += new SizeChangedEventHandler( OnResize );
		}
		
		protected override void OnResize( Control sender ) {
			//button.Size = new Point( GuiHost.Renderer.GetTextSize( button.Text,  ).x * 2, sender.Size.y - 2 * BUTTON_VERTICAL_PADDING );
		}
	}
}