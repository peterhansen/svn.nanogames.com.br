using Utils;
using Squid;
using System;
using UnityEngine;


public class Gauge : Frame {
	
	Squid.Controls.ImageControl framing;
	Squid.Controls.ImageControl filling;
	
	protected override void Initialize () {
		
		base.Initialize();
		
		framing = new Squid.Controls.ImageControl();
		framing.Parent = this;
		framing.Size = Size;
		framing.Position = new Point( 0, 0 );
		framing.Texture = "barra_stats.png";

		filling = new Squid.Controls.ImageControl();
		filling.Parent = this;
		filling.Size = Size;
		filling.Position = new Point( 0, 0 );
		filling.Texture = "barra_stats_preenchimento.png";
	}	
	
	public void Update( Point newSize, float percent ) {
		
		Size = newSize;
		framing.Size = Size;
		filling.Size = new Point( ( int ) ( Size.x * percent ), Size.y );
	}
}


