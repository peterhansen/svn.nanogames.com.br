#if UNITY_EDITOR
using System;

using UnityEditor;
using UnityEngine;

using GameCommunication;


[ CustomEditor( typeof( GameDesignEditor ) ) ]
public class GameDesignInspector : AttributeEditor {
	
	
	public override void OnEnable() {
		base.OnEnable();
	}
	
	
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
    }
	
}
#endif