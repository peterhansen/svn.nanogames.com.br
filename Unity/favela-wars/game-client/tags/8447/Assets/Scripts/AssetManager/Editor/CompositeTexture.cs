using System;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

using Utils;

using Object = UnityEngine.Object;


/// <summary>
/// Classe auxiliar para criar o material correto a partir da presença ou não dos mapas de textura (normal, altura, etc.) 
/// </summary>
internal class CompositeTexture {
	
	protected Texture2D mainTexture;
	
	protected Texture2D normalMap;
	
	protected Texture2D heightMap;
	
	protected Texture2D lightMap;
	
	protected Material material;
	
	public string name;
	
	protected string path;
	
	public string parentName;
	
	private string bundlePath;
	
	
	public CompositeTexture( string path, string name, string parentName ) : this( name, parentName ) {
//		this.path = path + name + ( AssetGenerator.count++ ) + ".mat";
		this.path = path + '_' + name + ".mat";
		bundlePath = AssetGenerator.GetBundlePath( AssetGenerator.PATH_BUNDLES_MATERIALS, path.Replace( AssetGenerator.PATH_MATERIALS, "" ) + '_' + name );
	}	
	
	
	public CompositeTexture( string name, string parentName ) : this( name ) {
		this.parentName = parentName;
	}
	
	
	public CompositeTexture( string name ) {
		this.name = name;
	}

	
	public Texture2D HeightMap {
		get { return this.heightMap; }
	}
	
	
	public Texture2D LightMap {
		get { return this.lightMap; }
	}

	
	public Texture2D MainTexture {
		get { return this.mainTexture; }
	}

	
	public Texture2D NormalMap {
		get { return this.normalMap; }
	}	
	
	
	public void Add( Texture2D t ) {
		string type = t.name.Substring( t.name.LastIndexOf( '_' ) + 1 ).ToLower();
		
		switch ( type ) {
			case AssetGenerator.MAP_SUFFIX_NORMAL:
			case AssetGenerator.MAP_SUFFIX_NORMAL_ALT:
			case AssetGenerator.MAP_SUFFIX_NORMAL_ALT2:
				normalMap = t;
			break;
				
			case AssetGenerator.MAP_SUFFIX_SPECULAR:
			case AssetGenerator.MAP_SUFFIX_SPECULAR_ALT:
				heightMap = t;
			break;
			
			case AssetGenerator.MAP_SUFFIX_LIGHTING:
				lightMap = t;
			break;
			
			case AssetGenerator.MAP_SUFFIX_DIFFUSE:
			case AssetGenerator.MAP_SUFFIX_DIFFUSE_ALT:
			case AssetGenerator.MAP_SUFFIX_DIFFUSE_ALT2:
			default:
				mainTexture = t;
			break;
		}
	}
	
	
	public static bool IsDiffuse( string textureName ) {
		string type = textureName.Substring( textureName.LastIndexOf( '_' ) + 1 ).ToLower();
		
		switch ( type ) {
			case AssetGenerator.MAP_SUFFIX_NORMAL:
			case AssetGenerator.MAP_SUFFIX_NORMAL_ALT:
			case AssetGenerator.MAP_SUFFIX_NORMAL_ALT2:
			case AssetGenerator.MAP_SUFFIX_SPECULAR:
			case AssetGenerator.MAP_SUFFIX_SPECULAR_ALT:
			case AssetGenerator.MAP_SUFFIX_LIGHTING:
				
			return false;
			
			case AssetGenerator.MAP_SUFFIX_DIFFUSE:
			case AssetGenerator.MAP_SUFFIX_DIFFUSE_ALT:
			case AssetGenerator.MAP_SUFFIX_DIFFUSE_ALT2:
			default:
				return true;
		}		
	}
	
	
	public Shader GetShader() {
		return Shader.Find( Common.SHADER_VISIBILITY_NAME );
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <returns>
	/// A <see cref="Material"/>
	/// </returns>
	public Material GetMaterial() {
		if ( material == null ) {
			// Create the Material
			material = new Material( GetShader() );
			material.SetTexture( "_MainTex", mainTexture );
			material.name = name;
			
			if ( normalMap != null )
				material.SetTexture( "_BumpMap", normalMap );
			
		}
		
		return material;
	}
	
	
	public void Free() {
		if ( mainTexture != null ) {
			Object.DestroyImmediate( mainTexture );
			mainTexture = null;
		}
		if ( normalMap != null ) {
			Object.DestroyImmediate( normalMap );
			normalMap = null;
		}
		if ( lightMap != null ) {
			Object.DestroyImmediate( lightMap );
			lightMap = null;
		}
		if ( heightMap != null ) {
			Object.DestroyImmediate( heightMap );
			heightMap = null;
		}
		if ( material != null ) {
			Object.DestroyImmediate( material );
			material = null;
		}
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public bool Save() {
		if ( AssetGenerator.CanSaveFile( path ) ) {
			// gera o prefab que vai ser usado no editor Unity
			Directory.CreateDirectory( path.Substring( 0, path.LastIndexOf( '/' ) + 1 ) );
			Material m = GetMaterial();
			AssetDatabase.CreateAsset( m, path );
			
			// TODO OPÇÃO PARA EVITAR PROBLEMA DOS PREFABS PERDEREM CONEXÃO: GERAR 1 MATERIAL PARA CADA PREFAB, PORÉM SÓ EXPORTAR 1 MATERIAL PARA TODOS AO GERAR MISSÃO
			
			// gera também um bundle, para ser importado depois no jogo
			if ( !AssetGenerator.generatedMaterials.ContainsKey( bundlePath ) ) {
				BuildPipeline.BuildAssetBundle( m, null, bundlePath, BuildAssetBundleOptions.CollectDependencies );
				AssetGenerator.generatedMaterials.Add( bundlePath, true );
			}
			
			return true;
		}
		
		return false;
	}
	
	
	public string GetMaterialPath() {
		return path;
	}
	
	
	public string GetMaterialBundlePath() {
		return bundlePath;
	}
	
	
	public override string ToString() {
		StringBuilder sb = new StringBuilder( "[CompositeTexture name:" );
		
		sb.Append( name );
		//		sb.Append( " main:" );
		//		sb.Append( mainTexture );
		//		sb.Append( " normal:" );
		//		sb.Append( normalMap );
		//		sb.Append( " height:" );
		//		sb.Append( heightMap );
		sb.Append( "]" );
		
		return sb.ToString();
	}
	
}
