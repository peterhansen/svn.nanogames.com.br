#if UNITY_EDITOR
using System;

using UnityEditor;
using UnityEngine;

using GameCommunication;


/// <summary>
/// Classe utilizada para que a Unity associe corretamente o inspector ao script, pois ela não aceita herança de tipos
/// em CustomEditor.
/// </summary>
[ CustomEditor( typeof( ItemEditor ) ) ]
public class ItemInspector : AttributeEditor {
	
	
	public override void OnEnable() {
		( ( ItemEditor ) target ).Start();
		base.OnEnable();
	}
	
	
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
    }
	
}
#endif