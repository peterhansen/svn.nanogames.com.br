using System;

using UnityEditor;
using UnityEngine;

using GameCommunication;


/// <summary>
/// Classe utilizada para que a Unity associe corretamente o inspector ao script, pois ela não aceita herança de tipos
/// em CustomEditor.
/// </summary>
[ CustomEditor( typeof( CharacterEditor ) ) ]
public class CharacterInspector : AttributeEditor {
	
	public override void OnEnable() {
		base.OnEnable();
	}
	
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
    }
	
}
