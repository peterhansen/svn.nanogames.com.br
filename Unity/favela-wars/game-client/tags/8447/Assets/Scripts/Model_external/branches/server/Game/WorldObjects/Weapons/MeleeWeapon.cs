using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class MeleeWeapon : Weapon {
		
		protected bool isThrowable;
		
		protected float attackTime = 5;
		
		protected float throwTime = 5;

				
		public MeleeWeapon() : base() {
			Range = Common.LEVEL_SECTOR_DIAGONAL;
		}
		
		
		/// <summary>
		/// Indica se esta arma de combate corpo-a-corpo pode ser arremessada para causar danos a inimigos, como
		/// facas de arremesso, martelos, shurikens e palavras de baixo calão.
		/// </summary>
		[ ToolTip( "Indica se esta arma de combate corpo-a-corpo pode ser arremessada para causar danos a inimigos, como facas de arremesso, martelos, shurikens e palavras de baixo calão." ) ]
		public bool IsThrowable {
			get { return this.isThrowable; }
			set { isThrowable = value; }
		}	
		
		
		/// <summary>
		/// Tempo do ataque padrão, em segundos.
		/// </summary>
		[ ToolTip( "Tempo do ataque padrão, em segundos." ) ]
		public float AttackTime {
			get { return attackTime; }
			set { attackTime = value; }
		}
		
		
		/// <summary>
		/// Tempo do ataque de arremesso, em segundos.
		/// </summary>
		[ ToolTip( "Tempo do ataque de arremesso, em segundos." ) ]
		public float ThrowTime {
			get { return throwTime; }
			set { throwTime = value; }
		}

		public override AttackReply Attack( GameVector origin, GameVector target, GameVector deviation, ItemUseMode useMode ) {
			AttackReply reply = new AttackReply();
			
			GameVector direction = target - origin;
			
			if ( direction.Norm() > Range )
				throw new AttackOutOfRangeException();
			
			reply.shots.Add( new ShotInfo( origin, target, Power ) );
			return reply;
		}
		
		
		public override GameAction GetPossibleActionsList() {
			GameAction toReturn = base.GetPossibleActionsList();
			
			AttackGameAction a = new AttackGameAction( EquipID, ItemUseMode.MELEE );
			a.range = Range;
			toReturn.AddChild( a );
			
			if ( IsThrowable ) {
				GameAction throwAction = new AttackGameAction( EquipID, ItemUseMode.THROW );
				toReturn.AddChild( throwAction );
			}
			
			return toReturn;
		}
		
		
//		public override float GetAPCost( UseMode useMode ) {
//			switch ( mode ) {
//				case UseMode.THROW:
//					return ThrowTime * THROW_TIME_TO_AP_RATE;
//				
//				default:
//					return AttackTime * ATTACK_TIME_TO_AP_RATE;
//			}
//		}
	}
	
}

