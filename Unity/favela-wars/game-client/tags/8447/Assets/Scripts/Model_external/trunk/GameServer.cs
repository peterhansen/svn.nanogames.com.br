using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using SharpUnit;

using GameCommunication;
// HACK
using GameController;


namespace GameModel {	
	
	/// <summary>
	/// Classe que faz o roteamento de requisições e tratadores dessas requisições - efetivamente servindo de coodernador de regras de jogo.
	/// </summary>
	public class GameServer {
	
		
		/// <summary>
		/// Requisições pendentes
		/// </summary>
		private Queue pendingRequests;
		/// <summary>
		/// Respostas que ainda precisam ser retiradas
		/// </summary>
		private Queue< Response > outboundResponses;
		
		private static GameServer instance;
		
		
		
		/// <summary>
		/// Construtor - apenas inicializa a classe de modo que, em modo debug, ela não faça nada mas avise que foi 
		/// usada de forma não inicializada. Em modo release, ele não deve fazer nada.
		/// </summary>
		private GameServer () {
			pendingRequests = new Queue();
			outboundResponses = new Queue< Response >();
		}
		
		
		public static GameServer GetInstance() {
			if ( instance == null )
				instance = new GameServer();
			
			return instance;
		}
		
		
		public static void Unload() {
			instance = null;
		}
			
		
		/// <summary>
		/// Processa todos os requests enfileirados e processsa, preparando para a obtenção de resposta.
		/// </summary>
		public void ProcessRequests() {
			for ( int c = 0; c < pendingRequests.Count; ++c ) {
				Request request = ( Request ) pendingRequests.Dequeue();
				Type type = Type.GetType( request.GetType().ToString().Replace( "GameCommunication", "GameModel" ).Replace( "Request", "Handler" ), true );
				ConstructorInfo constructor = type.GetConstructor( new Type[] { request.GetType() } );
				
				if ( constructor != null ) {
					Handler handler = ( Handler ) constructor.Invoke( new object[] { request } );
					Response response = handler.GetResponse();
					
					if( response != null ) {
						EnqueueResponse( response );
					}
				}
			}
			
			// TODO por enquanto, retorna todas as requisicoes
			while( outboundResponses.Count > 0 ) {
				// TODO PETER enviar resposta
				GameManager.GetInstance().ReceiveResponse( PopResponse() );
			}
		}
		
		
		public void EnqueueResponse( Response response ) {
			outboundResponses.Enqueue( response );
		}
		
		
		/// <summary>
		/// Enfileira requisição
		/// </summary>
		/// <param name="request">
		/// O <see cref="Request"/> é enfileirado e aguarda um <see cref="doProcessRequests"/> para que ele eventualmente
		/// crie uma <see cref="Response"/>, que deverá ser resgatada com <see cref="popResponse"/>
		/// </param>
		public void PushRequest( Request request ) {
			// TODO temporário: sempre trata a requisição assim que chega
			pendingRequests.Enqueue (request);
			ProcessRequests();
		}
		
		
//		/// <summary>
//		/// Obtem o resultado de alguma requisição. Não existe uma correspondência 1:1 entre uma requisição enfileirada 
//		/// e uma resposta
//		/// </summary>
//		/// <returns>
//		/// Um <see cref="Response"/>.
//		/// </returns>
		public Response PopResponse() {
			return ( Response ) outboundResponses.Dequeue ();
		}
		
		
		public IEnumerable< Response > EnumerateResponses() {
			while( outboundResponses.Count > 0 ) {
				yield return outboundResponses.Dequeue();
			}
		}
		
		
		public void PushResponse( Response response ) {
			outboundResponses.Enqueue( response );
		}
	}
	
	
	#region testes
	
	
	/// <summary>
	/// Teste de caso básico: Se eu peço ao GameManager para tratar minha requisição de soma (devidamente registrada),
	/// Ele deve me responder o valor da soma realizada. 
	/// </summary>
	public class GameManagerTester : TestCase
	{
		/// <summary>
		/// Apenas constroi a requisição, mas ainda não configura
		/// </summary>
		public override void SetUp() {
		}
		
		
		/// <summary>
		/// Nada para desconstruir
		/// </summary>
		public override void TearDown() {
		}
		
		
		/// <summary>
		/// Caso de teste: Requisição deve obter o valor de resposta apropriado
		/// </summary>
		[UnitTest]
		public void RequestShouldGetApropriateResponse () {
//			SumResponse sumResponse;
//			GameManager gm;
//			
//			gm = new GameManager ();
//			
//			sumRequest.GetRequestDataMap ().Add ("param1", "1");
//			sumRequest.GetRequestDataMap ().Add ("param2", "4");
//			gm.AddRequestHandler (0, typeof(SumHandler));
//			gm.PushRequest (sumRequest);
//			gm.DoProcessRequests ();
//			sumResponse = (SumResponse)gm.PopResponse ();
//			
//			Assert.Equal ( sumResponse.GetReturnCode(), 5 );
		}
	}	
	#endregion
}
