using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Utils;

using SharpUnit;
using GameCommunication;


namespace GameModel {
	
	public enum RaycastResult {
		NO_INTERCEPTION,
		INTERCEPTION
	};
	
	
	public class CollisionManager {
		
		protected CollisionManagerData data = new CollisionManagerData();
	
		protected SectorManager sectorManager;
		
		protected const float SHOT_WIDTH = 0.005f;
		
		
		public CollisionManager( string filename ) : this() {
			LoadMap( filename );
		}
		
		
		/// <summary>
		/// Construtor padr�o, para quando se vai usar a classe para gera��o de caixas envolventes, e n�o apenas para sua consulta.
		/// </summary>
		public CollisionManager() {
			sectorManager = new SectorManager( data.SectorsData );
		}
		
		
		public List< Box > GetBoxes() {
			return data.Boxes;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name='wo'>
		/// 
		/// </param>
		public void AddBox( WorldObject wo ) {
			if ( wo.GetBox() != null )
				data.AddBox( wo.GetBox(), false );
		}
		
		
		public byte[] RawAStarData {
			get { return data.RawAStarData; }
		}
		
		
//		/// <summary>
//		/// Adiciona uma caixa de colis�o.
//		/// </summary>
//		/// <param name='box'>
//		/// Caixa a ser adicionada.
//		/// </param>
//		/// <param name='fillUntilBottom'>
//		/// Indica se todos os setores abaixo da caixa (ou seja, com <c>j</c> menor) devem ser preenchidos.
//		/// </param>
		// TODO PETER
//		public void AddBox( Box box, bool fillUntilBottom ) {
//			boxes.Add( box );
//			
//			// HACK: mudar isso, est� muito feio...
//			if( sectorManager != null && ( !Application.isEditor || Application.isPlaying ) ) {
//				sectorManager.AddBox( box, fillUntilBottom );
//			}
//		}			
		
		
		public void RemoveBox( Box box ) {
			data.Boxes.Remove( box );
		}
		
		
		public Box GetBoxAt( GameVector point ) {		
			if( sectorManager != null ) {
				foreach( Box b in sectorManager.GetBoxesFromGameVectorSector( point ) ) {
					if( b.Contains( point ) ) {
						return b;
					}
				}
			} else {
				foreach( Box b in data.Boxes ) {
					if( b.Contains( point ) ) {
						return b;
					}
				}
			}
			
			return null;
		}
				
		
		public bool IsOccupied( GameVector point ) {
			return GetBoxAt( point ) != null;
		}
		
		
		public void OnBoxMoved( Box box ) {
			UpdateBoxLayer( box );
			sectorManager.OnBoxMoved( box );
		}
		
		
		// TODO PETER
//		public void GenerateSectors( GameVector minPoint ) {
//			// gera setores a partir das caixas e os cadastra no sector manager
//			sectorManager.GetSectorsData().GenerateSectors( minPoint, data.Boxes );
//			
//			foreach( Box box in data.Boxes )
//				UpdateBoxLayer( box );
//		}
		

		public void UpdateBoxLayer( Box box ) {
			box.SetLayer( sectorManager.GetLayer( box ) );
		}
		
		
		public IEnumerable< Sector > GetBoxSectors( Box objBox ) {
			foreach( Sector sector in sectorManager.GetBoxSectors( objBox ) ) {
				yield return sector;
			}
		}
		
		
		public IEnumerable<Box> GetSectorBoxes( Sector sector ) {
			foreach( Box box in sectorManager.GetSectorBoxes( sector ) ) {
				yield return box;
			}
		}
		
		
		public IEnumerable<int> GetRaySectors( GameVector origin, GameVector target ) {
			foreach( int sector in sectorManager.GetRaySectors( origin, target ) ) {
				yield return sector;
			}
		}
		
		
		public SectorManager GetSectorManager() {
			return sectorManager;
		}
		
		
		public RaycastResult Shoot( Character shooter, GameVector target, out HitInfo hitInfo ) {
			RaycastFilter filter = new RaycastFilter();
			foreach( Box box in sectorManager.GetBoxesInRaySectors( shooter.GetGameTransform().position, target ) )
				if( box != shooter.GetBox() )
					filter.Boxes.Add( box );
			
			RaycastResult result = Raycast( shooter.GetGameTransform().position, target, out hitInfo, filter );
			//Debugger.DrawLine( shooter.GetGameTransform().position, hitInfo.position, result == RaycastResult.NO_INTERCEPTION? 0x00ff00ff: 0xff0000ff , 10.0f );
			return result;
		}
		
		
		/// <summary>
		/// Throws the object.
		/// </summary>
		/// <returns>
		/// The object.
		/// </returns>
		/// <param name='origin'>
		/// Origin.
		/// </param>
		/// <param name='direction'>
		/// Direction.
		/// </param>
		public List< ThrowInfo > ThrowObject( GameVector origin, GameVector direction, int characterWorldID, int maxBounces, float restitutionRate ) {
			List< ThrowInfo > throws = new List< ThrowInfo >();
			
			foreach ( ParabolaHit reply in GetThrowHits( origin, direction, characterWorldID ) ) {
				ThrowInfo t = new ThrowInfo( new Parabola( origin, direction ), reply.time );
				throws.Add( t );
				
				if ( reply.direction.IsZero() )
					break;
				
				GameVector newDirection = reply.direction * restitutionRate;
				
				if ( ( maxBounces > 0 && throws.Count > maxBounces ) || newDirection.Norm() < 1.9f )
					break;
				
				throws.AddRange( ThrowObject( reply.position, newDirection, characterWorldID, maxBounces - 1, restitutionRate ) );
				break;
			}
			
			return throws;
		}
		
		
		/// <summary>
		/// Enumera os pontos que a par�bola de um arremesso atinge. A acelera��o considerada � da gravidade.
		/// </summary>
		/// <returns>
		/// Enumera��o dos setores que a par�bola percorre.
		/// </returns>
		/// <param name='origin'>
		/// 
		/// </param>
		/// <param name='direction'>
		/// </param>
		public IEnumerable< ParabolaHit > GetThrowHits( GameVector origin, GameVector direction, int characterWorldID ) {
			Parabola p = new Parabola( origin, direction );
			
			float finalT = -1;
			foreach ( float f in p.GetTimes( sectorManager.GetMinPoint().y ) )
				finalT = Math.Max( f, finalT );
			
			if ( finalT >= 0 ) {
				bool hit = false;
				
				int dx = ( int ) ( direction.x > 0 ? Math.Ceiling( direction.x * finalT ) : Math.Floor( direction.x * finalT ) );
				int dz = ( int ) ( direction.z > 0 ? Math.Ceiling( direction.z * finalT ) : Math.Floor( direction.z * finalT ) );
				
				int iteractions = Math.Max( 2, Math.Max( Math.Abs( dx ), Math.Abs( dz )) );
				float y0 = origin.y;
				
				GameVector entryPoint = new GameVector( origin );
				GameVector currentDirection;
				List< RayBoxInterceptionReply > replies = new List< RayBoxInterceptionReply >();
				List< Sector > sectors;
				
				for ( int i = 0; i <= iteractions; ++i ) {
					float t = ( i + 1 ) * finalT / iteractions;
					GameVector currentPoint = p.GetPointAt( t );
					float y1 = currentPoint.y;
					
					int diff = ( int ) ( y1 - y0 );
					int diffAbs = Math.Abs( diff );
					
					currentDirection = ( currentPoint - entryPoint );
					
					GameVector ret = new GameVector( currentDirection );
					ret.SetScale( Common.LEVEL_SECTOR_DIAGONAL );
					
					sectors = sectorManager.GetSectors( entryPoint - ret, currentPoint + ret );
					
					Debugger.DrawLine( entryPoint, currentPoint, 0xffff0088, 5 );
					
					replies = new List< RayBoxInterceptionReply >();
					foreach ( Sector s in sectors )
						replies.AddRange( sectorManager.GetBoxInterceptions( s, entryPoint, currentPoint ) );
					
					// se houver mais de 1 interse��o com caixas, procura a que ocorreu primeiro
					if ( replies.Count > 0 ) {
						RayBoxInterceptionReply bestReply = null;
						foreach ( RayBoxInterceptionReply reply in replies ) {
							reply.entry.normal.Normalize();
							Debugger.DrawLine( entryPoint, currentPoint + new GameVector( 0.001f, 0.001f, 0.001f ), 0x00ffffff, 5 );
							Debugger.DrawLine( reply.entry.point, reply.entry.point + reply.entry.normal, 0xff0000ff, 5 );
							if ( ( bestReply == null || reply.entry.t < bestReply.entry.t ) && ( reply.entry.worldObjectID != characterWorldID && currentDirection.AngleBetween( reply.entry.normal ) > 90 ) )
								bestReply = reply;
						}
						// a resposta pode ser nula caso a �nica colis�o tenha ocorrido com a caixa do personagem
						if ( bestReply != null ) {
							Debugger.DrawLine( entryPoint, currentPoint, 0xff0000aa, 5 );
							yield return GetHit( p, bestReply );
							hit = true;
						}
					}
					
					if ( diffAbs > 0 ) {
						int y_inc = ( diff > 0 ? 1 : -1 );
						for ( int j = 0; j < diffAbs; ++j, y0 += y_inc )
							replies.AddRange( sectorManager.GetBoxInterceptions( sectorManager.GetSectorFromGameVector( entryPoint.x, y0 + y_inc, entryPoint.z ), entryPoint, currentPoint ) );
							
						if ( replies.Count > 0 ) {
							RayBoxInterceptionReply bestReply = null;
							foreach ( RayBoxInterceptionReply reply in replies ) {
								reply.entry.normal.Normalize();
								Debugger.DrawLine( entryPoint, currentPoint + new GameVector( 0.001f, 0.001f, 0.001f ), 0x00ffffff, 5 );
								Debugger.DrawLine( reply.entry.point, reply.entry.point + reply.entry.normal, 0xff0000ff, 5 );
								if ( ( bestReply == null || reply.entry.t < bestReply.entry.t ) && ( reply.entry.worldObjectID != characterWorldID && currentDirection.AngleBetween( reply.entry.normal ) > 90 ) )
									bestReply = reply;
							}
							// a resposta pode ser nula caso a �nica colis�o tenha ocorrido com a caixa do personagem
							if ( bestReply != null ) {
								Debugger.DrawLine( entryPoint, currentPoint, 0xff0000aa, 5 );
								yield return GetHit( p, bestReply );
								hit = true;
							}
						}
					}
					
					y0 = y1;
					entryPoint.Set( currentPoint );
				}
				
				// tratamento especial para o caso de parar fora da �rea do n�vel
				if ( !hit )
					yield return new ParabolaHit( finalT, p.GetPointAt( finalT ), new GameVector() );
			}
		}
		
		
		/// <summary>
		/// M�todo auxiliar para criar um objeto com informa��es de colis�o de uma par�bola.
		/// </summary>
		private static ParabolaHit GetHit( Parabola p, RayBoxInterceptionReply reply ) {
			Debugger.Log( reply.entry + ", " + reply.entry.normal );
			GameVector minDistance = new GameVector( reply.entry.normal );
			minDistance.SetScale( 0.1f );
			reply.entry.point += minDistance;
			
			float touchT = 9999999;
			List< float > times = p.GetTimes( reply.entry.point.y );
			
			GameVector finalDirection = null;
			float bestDistance = 9999;
			foreach ( float f in times ) {
				if ( f > 0 ) {
					float dist = p.GetPointAt( f ).DistanceTo( reply.entry.point );
					if ( dist < bestDistance ) {
						touchT = f;
						finalDirection = p.GetSpeedAt( f );
						bestDistance = dist;
					}
				}
			}
			
			if ( finalDirection == null ) {
				// HACK evita retornar null, por�m pode fazer com que objeto "grude" em algo (como uma parede)
				return new ParabolaHit( 0, new GameVector( p.origin ), new GameVector() );
			}
			
			float finalSpeed = finalDirection.Norm();
			
			GameVector returnPosition = new GameVector( finalDirection );
			returnPosition.SetScale( 0.1f );
			
			// posi��o da colis�o
			GameVector hitPoint = p.GetPointAt( touchT );
			// posi��o do objeto no momento da colis�o
			GameVector objectHitCenter = hitPoint - returnPosition;
			// dire��o de sa�da do objeto
			GameVector objectOutCenter = ( hitPoint * 2 ) - objectHitCenter + ( reply.entry.normal * ( objectHitCenter - hitPoint ).Dot( reply.entry.normal ) ) * 2;
			
			GameVector exitVector = objectOutCenter - hitPoint;
			exitVector.SetScale( finalDirection.Norm() );
			
			Debugger.DrawLine( hitPoint, hitPoint + exitVector, 0x00ff00ff, 5 );
			
			return new ParabolaHit( touchT, hitPoint, exitVector );
		}
		
		
		/// <summary>
		/// Verifica todos os objetos atingidos por uma explos�o.
		/// </summary>
		/// <param name='center'>
		/// Centro da explos�o.
		/// </param>
		/// <param name='ray'>
		/// Raio da esfera da explos�o.
		/// </param>
		/// <returns>
		/// Lista de pontos atingidos (pode haver at� 9 pontos por personagem).
		/// </returns>
		public HitInfo[] Explode( GameVector center, float ray ) {
			// TODO amortizar impacto caso personagem esteja no caminho de outro
			List< Box > sectorBoxes = data.Boxes; // TODO filtrar caixas pelo volume da esfera
			List< HitInfo > hitsInfo = new List< HitInfo >();
			
			foreach ( Box b in sectorBoxes ) {
				// se n�o for um personagem, n�o verifica
				if ( b.IsSceneryBox() )
					continue;
				
				GameTransform gt = b.GetGameTransform();
				float maxDistance = ray + ( gt.scale.Norm() * 0.5f );
				
				// verifica com o centro da caixa
				if ( gt.position.DistanceTo( center ) <= maxDistance ) {
					// a caixa est� dentro da �rea da explos�o - verifica se ela pode efetivamente ser atingida
					HitInfo hitInfo = new HitInfo();
					if ( Raycast( center, gt.position, out hitInfo ) == RaycastResult.INTERCEPTION && hitInfo.worldObjectId == b.GetWorldObjectID() ) {
						hitsInfo.Add( hitInfo );
					}
				}
				
				// verifica com os outros pontos da caixa
				foreach ( GameVector p in b.GetVertexes() ) {
					if ( p.DistanceTo( center ) <= maxDistance ) {
						// a caixa est� dentro da �rea da explos�o - verifica se ela pode efetivamente ser atingida
						HitInfo hitInfo = new HitInfo();
						if ( Raycast( center, p, out hitInfo ) == RaycastResult.INTERCEPTION && hitInfo.worldObjectId == b.GetWorldObjectID() ) {
							hitsInfo.Add( hitInfo );
						}
					}
				}
			}
			
			return hitsInfo.ToArray();
		}
		

		public RaycastResult Raycast( GameVector origin, GameVector target ) {
			return Raycast( origin, target, null );
		}
	
				
		public RaycastResult Raycast( GameVector origin, GameVector target, float width ) {
			return Raycast( origin, target, width, null );
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, out HitInfo hitInfo ) {
			return Raycast( origin, target, out hitInfo, null );
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width, out HitInfo hitInfo ) {
			return Raycast( origin, target, width, out hitInfo, null );
		}
		
		
		public RaycastResult RaycastAnyOfTheRays( GameVector origin, GameVector target, float width, RaycastFilter filter ) {
			return RaycastSideRays( origin, target, width, filter, false );
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width, RaycastFilter filter ) {
			return RaycastSideRays( origin, target, width, filter, true );
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, RaycastFilter filter ) {
			if( RayIsNull( origin, target ) )
				return RaycastResult.NO_INTERCEPTION;
			
			if( filter != null ) {
				foreach( Box box in filter.Boxes ) {
					RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
					
					if( interceptionReply.exit != null || interceptionReply.entry != null ) {
						return RaycastResult.INTERCEPTION;
					}
				}
			} else {
				foreach( Box box in sectorManager.GetBoxesInRaySectors( origin, target ) ) {		
					RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
					
					if( interceptionReply.exit != null || interceptionReply.entry != null ) {
						return RaycastResult.INTERCEPTION;
					}
				}
			}

			return RaycastResult.NO_INTERCEPTION;
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, out HitInfo hitInfo, RaycastFilter filter ) {
			GameVector direction;
			if( RayIsNull( origin, target, out direction, out hitInfo ) ) {
				return RaycastResult.NO_INTERCEPTION;
			}
			
			RayBoxInterception entryInterception = null;
			RayBoxInterception exitInterception = null;
			
			if( filter != null ) {
				foreach( Box box in filter.Boxes ) {		
					RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
									
					entryInterception = UpdateInterception( entryInterception, interceptionReply.entry );
					exitInterception = UpdateInterception( exitInterception, interceptionReply.exit );
				}
			} else {
				foreach( Box box in sectorManager.GetBoxesInRaySectors( origin, target ) ) {		
					RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
									
					entryInterception = UpdateInterception( entryInterception, interceptionReply.entry );
					exitInterception = UpdateInterception( exitInterception, interceptionReply.exit );
				}
			}	
			
			
			hitInfo = new HitInfo();
			
			if( entryInterception != null ) {
				hitInfo.position = entryInterception.point;
				hitInfo.worldObjectId = entryInterception.worldObjectID;
			} else if( exitInterception != null ) {
				hitInfo.position = exitInterception.point;
				hitInfo.worldObjectId = exitInterception.worldObjectID;
			} else {
				hitInfo.position = target;
			}
			
			return ( entryInterception == null && exitInterception == null )? RaycastResult.NO_INTERCEPTION : RaycastResult.INTERCEPTION;
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width, out HitInfo hitInfo, RaycastFilter filter ) {
			GameVector direction = target - origin;
			
			GameVector rightOrigin, rightDestination;
			GameVector leftOrigin, leftDestination;
			GenerateSideRays( origin, target, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
			
			HitInfo hitRight;
			RaycastResult rightPathResult = Raycast( rightOrigin, rightDestination, out hitRight, filter );
			
			HitInfo hitLeft;
			RaycastResult leftPathResult = Raycast( leftOrigin, leftDestination, out hitLeft, filter );
			
			hitInfo = new HitInfo();
			if( rightPathResult == RaycastResult.NO_INTERCEPTION && leftPathResult == RaycastResult.NO_INTERCEPTION ) {
				
				hitInfo.position = target;
				hitInfo.worldObjectId = Common.NULL_WORLDOBJECT;
				return RaycastResult.NO_INTERCEPTION;
			} else {
				// pegando caminho que colidiu primeiro
				float rightNorm = ( hitRight.position - rightOrigin ).Norm();
				float leftNorm = ( hitLeft.position - leftOrigin ).Norm();
				
				HitInfo closerHit = rightNorm < leftNorm? hitRight: hitLeft;

				hitInfo.worldObjectId = closerHit.worldObjectId;
				float norm = Math.Min( rightNorm, leftNorm );
				hitInfo.position = origin + direction.Unitary() * norm;
				
				return RaycastResult.INTERCEPTION;
			}
		}
		
		
		// TODO esse m�todo ainda nao retorna um filtro... deveria retornar
		public List< Box > CreateFilterCone( GameVector apex, GameVector axis, float angle ) {
			List< Box > objects = new List< Box >();
			
			foreach( Box box in data.Boxes ) {
				GameVector positionFromApex = box.GetPosition() - apex;
				
				// caixa est� no �pice do cone
				if( positionFromApex.IsZero() ) {
					objects.Add( box );
					continue;
				}
				
				bool sameDirection;
				GameVector projectedPosition = positionFromApex.ProjectOn( axis, out sameDirection );
				if( sameDirection && 
				   NanoMath.LessEquals( projectedPosition.Norm(), axis.Norm() ) &&
				   NanoMath.LessEquals( positionFromApex.AngleBetween( axis ), angle ) ) {
					objects.Add( box );
				}
			}
			
			return objects.Count > 0 ? objects: null;
		}
		
		
		/// <summary>
		/// Salva mapa para o disco. C�digo ja existia, mas agora a ideia � unificar, para facilitar testagem.
		/// </summary>
		/// <param name="path"> 
		/// � <see cref="System.String"/> relativa � raiz do projeto.
		/// </param>
		public void SaveMap( string missionName ) {
			MapCollisionData collisionData = new MapCollisionData( missionName, data.Boxes, sectorManager.GetSectorsData(), data.RawAStarData );
			collisionData.ExportToFile();
		}	
		
		
		protected void LoadMap( string filename ) {
			MapCollisionData collisionData = MapCollisionData.ImportFromFile( filename );
			data.Boxes = collisionData.GetBoxes();
			data.SectorsData = collisionData.GetSectorsData();
			sectorManager = new SectorManager( data.SectorsData );
			
			foreach( Box b in collisionData.GetBoxes() )
				b.SetISectorManager( sectorManager );
			
			data.RawAStarData = collisionData.GetRawAstarData();
		}
		
		
		protected bool RayIsNull( GameVector origin, GameVector target ) {
			return ( target - origin ).IsZero();
		}
		
		
		protected bool RayIsNull( GameVector origin, GameVector target, out GameVector direction, out HitInfo hitInfo ) {
			hitInfo = new HitInfo();
			direction = target - origin;
			bool isNull = direction.IsZero();
			
			if( isNull ) {
				hitInfo.position = origin;
				hitInfo.worldObjectId = Common.NULL_WORLDOBJECT;
			} 
			return isNull;
		}
		
		
		protected RaycastResult RaycastSideRays( GameVector origin, GameVector target, float width, RaycastFilter filter, bool tryBothRays ) {
			if( RayIsNull( origin, target ) )
				return RaycastResult.NO_INTERCEPTION;
			
			GameVector rightOrigin, rightDestination;
			GameVector leftOrigin, leftDestination;
			GenerateSideRays( origin, target, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
			
			if( tryBothRays ) { 
				if( Raycast( rightOrigin, rightDestination, filter ) == RaycastResult.INTERCEPTION || 
				   Raycast( leftOrigin, leftDestination, filter ) == RaycastResult.INTERCEPTION) {
					return RaycastResult.INTERCEPTION;
				}
			} else {
				if( Raycast( rightOrigin, rightDestination, filter ) == RaycastResult.INTERCEPTION && 
				   Raycast( leftOrigin, leftDestination, filter ) == RaycastResult.INTERCEPTION) {
					return RaycastResult.INTERCEPTION;
				}
			}
			
			return RaycastResult.NO_INTERCEPTION;
		}
		
		protected RayBoxInterception UpdateInterception( RayBoxInterception current, RayBoxInterception candidate ) {
			if( candidate != null ) {
				if( current == null || candidate.t < current.t ) {
					return candidate;
				}
			}
			return current;
		}
		
		
		// TODO: mover para Utils!
		/// <summary>
		/// Gera raios paralelos ao raio (target - origin), no eixo horizontal.
		/// </summary>
		public static void GenerateSideRays( GameVector origin, GameVector target, float width,
											 out GameVector rightOrigin, out GameVector rightDestination, 
											 out GameVector leftOrigin, out GameVector leftDestination ) 
		{
			
			GenerateParallelRays( origin, target, GameVector.YAxis, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
		}
		
		

		public static void GenerateVerticalRays( GameVector origin, GameVector target, float width,
												 out GameVector topOrigin, out GameVector topDestination, 
												 out GameVector bottomOrigin, out GameVector bottomDestination ) 
		{
			
			GenerateParallelRays( origin, target, GameVector.XAxis, width, out topOrigin, out topDestination, out bottomOrigin, out bottomDestination );
		}
		
		
		private static void GenerateParallelRays( GameVector origin, GameVector target, GameVector axis, float width,
												  out GameVector rightOrigin, out GameVector rightDestination, 
												  out GameVector leftOrigin, out GameVector leftDestination ) 
		{
			
			GameVector direction = target - origin;
			GameVector pathUp = axis - axis.ProjectOn( direction );
			if( pathUp.IsZero() ) {
				pathUp = axis.Equals( GameVector.YAxis ) ? GameVector.XAxis : GameVector.YAxis;
			}
				
			GameVector rightDir = direction.Cross( pathUp ).Unitary();
			rightDir *= width * 0.5f;
			
			GameVector leftDir = -rightDir;
			
			rightOrigin = origin + rightDir;
			rightDestination = target + rightDir;
			leftOrigin = origin + leftDir;
			leftDestination = target + leftDir;
		}
		
		
		public override int GetHashCode() { // TODO
			return ToString().GetHashCode();
		}		
		
		
		public bool Land( GameVector position, Box characterBox, out HitInfo hitInfo, RaycastFilter filter ) {
			GameVector target = new GameVector( position );
			target -= GameVector.YAxis * 2.0f * characterBox.GetGameTransform().scale.y;
						
			bool works = Raycast( position, target, out hitInfo, filter ) == RaycastResult.INTERCEPTION;
			return works;
		}
		
		
		public bool LandBox( Box box, RaycastFilter filter ) {
			/*GameVector origin = box.GetGameTransform().position;
			
			GameVector target = new GameVector( origin );
			target -= GameVector.YAxis * 2.0f * box.GetGameTransform().scale.y;
			if ( target.y < sectorManager.GetSectorsData().minPoint.y )
				target.y = sectorManager.GetSectorsData().minPoint.y;
		
			HitInfo hit;
			if( Raycast( origin, target, out hit, filter ) == RaycastResult.INTERCEPTION ) {
				Debugger.DrawLine( origin, hit.position, 0xFF0000FF, 10.0f );
				box.GetGameTransform().position = hit.position + GameVector.YAxis * box.GetGameTransform().scale.y * 0.5f;
				return true;
			} else {
				return false;
			}*/			
			HitInfo hit;
			if( Land( box.GetGameTransform().position, box, out hit, filter ) ) {
				box.GetGameTransform().position = hit.position + GameVector.YAxis * box.GetGameTransform().scale.y * 0.5f;
				return true;
			} else {
				return false;
			}
		}
		
		
		
		public bool LandBox( Box box ) {
			return LandBox( box, null );
		}
		
		
		public Box GetBoxByWorldObjectId( int worldObjectID ) {
			foreach ( Box b in data.Boxes ) {
				if ( b.GetWorldObjectID() == worldObjectID )
					return b;
			}
			
			return null;
		}
		
	}
	

	/// <summary>
	/// Caso de teste
	/// </summary>
//	public class CollisionManagerTestLevel1TestCase : TestCase {
//		
//		protected const string NONEXISTANT_LEVEL = "Assets/Levels/world_file_which_does_not_exists" + Common.LEVEL_FILE_EXTENSION;
//				
//		protected const string LEVEL_NAME = "teste_1";
//		protected const string LEVEL_NAME_WHICH_DOES_NOT_EXISTS = "world_file_which_does_not_exists";
//
//		protected const string TEST_LEVEL = "Assets/Levels/teste_1" + Common.LEVEL_FILE_EXTENSION;
//		protected const int NUMBER_OF_SECTORS = 8;
//		protected CollisionManager collisionManager;
//		
//		public override void SetUp() {
//			collisionManager = new CollisionManager( TEST_LEVEL ); 
//		}
//		
//		
//		[UnitTest]
//		public void NotFindindWorldFile() {
//			Assert.ExpectException( new FileNotFoundException() );
//			new CollisionManager( NONEXISTANT_LEVEL );
//		}
//		
//		[UnitTest]
//		public void CorrectNumberOfSectors() {
//			Assert.True( collisionManager.GetTotalSectors() == NUMBER_OF_SECTORS + 1 );
//		}
//		
//		
//		[ UnitTest ]
//		public void TestHardProjection() {
//			LevelSector sector = LevelSector.MakeSectorAt( -69.57163f, -69.57163f + 0.2976303f, -15.0f,-15.0f + 30.0f, 138.4975f, 138.4975f + 2.475281f );
//			Assert.CheckEquals( new GameVector( -69.57163f, -14.7f, 139.1895f ), sector.GetProjection( new GameVector( -155.4f, -14.7f, 137.0f ), new GameVector( -69.274f, -14.7f, 139.1971f ) ) );
//		}
//
//	
//	
//			
//		[ UnitTest ]
//		public void TestAddCollisionCell() {
//			CollisionManager cm;
//			LevelSector sector;
//
//			cm = new CollisionManager();
//			Assert.Equal( 0, cm.GetTotalSectors() );
//			cm.SaveMap( "tmp" + Common.LEVEL_FILE_EXTENSION, true );
//			cm = new CollisionManager( "tmp" + Common.LEVEL_FILE_EXTENSION );
//			Assert.Equal( 1, cm.GetTotalSectors() );
//			cm = new CollisionManager();
//			sector = new LevelSector();
//			sector.SetShape( 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f );
//			cm.AddNavigableCell( sector );
//		}
//		
//		
//		[ UnitTest ]
//		public void TestGetSectorFromGameVector() {
//			CollisionManager cm;
//			
//			cm = new CollisionManager();
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 0.0f, 2.0f, 0.0f, 2.0f ) );
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 2.0f, 2.0f, 0.0f, 2.0f, 0.0f, 2.0f ) );
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 2.0f, 2.0f, 0.0f, 2.0f ) );
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 0.0f, 2.0f, 2.0f, 2.0f ) );	
//		}
//
//		
//		[ UnitTest ]
//		public void TestRayInterceptionMustHaveSameResultBothWays() {
//		
//			GameVector origin = new GameVector( 5.768162f, -1.8f, 2.921593f );
//			GameVector destiny = new GameVector( 5.134628f, -1.8f, 2.9f );
//			int sec1 = collisionManager.GetSectorFromGameVector( origin );
//			int sec2 = collisionManager.GetSectorFromGameVector( destiny );
//			
//			List< int > path1 = collisionManager.GetPath( origin, destiny );
//			Assert.Equal( sec2, path1[ path1.Count - 1 ] );
//			List< int > path2 = collisionManager.GetPath( destiny, origin );
//			Assert.Equal( sec1, path2[ path2.Count - 1 ] );
//			
//			Assert.Equal( path1.Count, path2.Count );
//		}
//		
//		
//		[UnitTest]
//		public void LevelSectorPathShouldBeBlocked() {
//			GameVector destiny = new GameVector( 5.0f, -1.8f, 2.458286f );
//			GameVector origin = new GameVector( 0.0f, -1.8f, -1.541715f );
//			
////			GameVector hit = collisionManager.GetHitPoint( origin, destiny );
////			
////			UnityEngine.Vector3 originVec = new UnityEngine.Vector3( origin.x, origin.y, origin.z );
////			UnityEngine.Vector3 destinyVec = new UnityEngine.Vector3( destiny.x, destiny.y, destiny.z );
////			UnityEngine.Debug.DrawLine( originVec, destinyVec, UnityEngine.Color.yellow, 10 );
//			
//			List< int > path = collisionManager.GetPath( origin, destiny );
//			int finalSector = collisionManager.GetSectorFromGameVector( destiny );
//			Assert.Equal( 2, path.Count );
//			Assert.False ( finalSector == path[ path.Count - 1 ] );
//		}
//		
//		
//		[ UnitTest ]
//		public void ShouldMoveDirectly() {
//			
//			GameVector gv1 = new GameVector( 0.3f, -1.99089f, 2.158286f );
//			GameVector gv2 = new GameVector( 5.229355f, -1.8f, 2.643562f );
//			int s1;
//			int s2;
//
//			s1 = collisionManager.GetSectorFromGameVector( gv1 );
//			s2 = collisionManager.GetSectorFromGameVector( gv2 );
//			List< int > path = collisionManager.GetPath( gv1, gv2 );
//			Assert.CheckEquals( path[ path.Count - 1 ], s2 ); 
//			Assert.CheckEquals( path[ 0 ], s1 ); 
//		}
//		
//
//		protected List< int > getPathForTestBetweenSectors( int s1, int s2 ) {
//			GameVector gv1 = collisionManager.GetSector( s1 ).GetCenter();
//			GameVector gv3 = collisionManager.GetSector( s2 ).GetCenter();
//			return collisionManager.GetPath( gv1, gv3 );
//		}
//		
//		//testado E
//		[ UnitTest ]
//		public void TestParametricProjectionAt() {
//			List< int > path = getPathForTestBetweenSectors( 1, 3 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 1, path[ 0 ] );
//			Assert.Equal( 3, path[ 1 ] );
//		}		
//		
//		//testado E
//		[ UnitTest ]
//		public void TestAnotherParametricProjectionAt() {
//			List< int > path = getPathForTestBetweenSectors( 6, 8 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 6, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//		}	
//		
//		//testado W
//		[ UnitTest ]
//		public void TestWhatGoesAroundComesAround() {
//			List< int > path = getPathForTestBetweenSectors( 8, 6 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 8, path[ 0 ] );
//			Assert.Equal( 6, path[ 1 ] );
//		}			
//		
//		//testado W
//		[ UnitTest ]
//		public void TestYetAnotherPath() {
//			List< int > path = getPathForTestBetweenSectors( 7, 8 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 7, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//		}	
//		
//		//testado N
//		[ UnitTest ]
//		public void TestFinallyANewPath() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 4, 7 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 4, path[ 0 ] );
//			Assert.Equal( 7, path[ 1 ] );
//		}
//		
//		//testado N
//		[ UnitTest ]
//		public void PathBetweenLevelSectors1and6() {
//			List< int >	path = getPathForTestBetweenSectors( 1, 6 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 1, path[ 0 ] );
//			Assert.Equal( 5, path[ 1 ] );
//			Assert.Equal( 6, path[ 2 ] );
//		}
//		
//		//testado S
//		[ UnitTest ]
//		public void TestPathFrom4to2() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 4, 2 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 4, path[ 0 ] );
//			Assert.Equal( 2, path[ 1 ] );
//		}		
//		
//		//testado S
//		[ UnitTest ]
//		public void TestPathFrom6to1() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 6, 1 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 6, path[ 0 ] );
//			Assert.Equal( 5, path[ 1 ] );
//			Assert.Equal( 1, path[ 2 ] );
//		}		
//
//		//testado E
//		[ UnitTest ]
//		public void TestPathFrom1o2() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 1, 2 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 1, path[ 0 ] );
//			Assert.Equal( 3, path[ 1 ] );
//			Assert.Equal( 2, path[ 2 ] );
//		}		
//
//		
//		//testado W
//		[ UnitTest ]
//		public void TestPathFrom2to1() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 2, 1 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 2, path[ 0 ] );
//			Assert.Equal( 3, path[ 1 ] );
//			Assert.Equal( 1, path[ 2 ] );
//		}				
//		
//		//testado E
//		[ UnitTest ]
//		public void TestPathFrom7to6() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 7, 6 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 7, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//			Assert.Equal( 6, path[ 2 ] );
//		}		
//
//		
//		//testado W
//		[ UnitTest ]
//		public void TestPathFrom6to7() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 6, 7 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 6, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//			Assert.Equal( 7, path[ 2 ] );
//		}
//		
//
//		[ UnitTest ]
//		public void TestPathFrom3to7() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 3, 7 );
//			Assert.Equal( 1, path.Count );
//			Assert.Equal( 3, path[ 0 ] );
//		}
//		
//		[ UnitTest ]
//		public void TestPathFrom5to7() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 5, 7 );
//			Assert.Equal( 1, path.Count );
//			Assert.Equal( 5, path[ 0 ] );
//		}
//	}	
//
//			
//	public class CollisionManagerTestLevel3TestCase : TestCase {
//			
//			protected const string LEVEL_NAME = "teste_3";
//			protected CollisionManager collisionManager;
//			
//			public override void SetUp() {
//				collisionManager = new CollisionManager( Path.Combine( Common.PATH_LEVEL_FILE, LEVEL_NAME + Common.LEVEL_FILE_EXTENSION ) ); 
//			}
//			
//			[ UnitTest ]
//			public void PathShouldBeAvailable() {
//				GameVector a = new GameVector( -57.4f, -14.7f, 139.5f );
//				GameVector b = new GameVector( -155.4f, -14.7f, 137.0f );
//				List< int > path = collisionManager.GetPath( a, b );
//				Assert.NotNull( path );
//				Assert.Equal( 6, path.Count );
//			}
//		
//		 
//
//			[UnitTest]
//			public void PathShouldBeAvailable3() {
//				GameVector a = new GameVector( -155.5933f, -14.7f, 137.3793f );
//				GameVector b = new GameVector( -69.03856f, -14.32582f, -23.95451f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetPath( a, b, out hit );
//				Assert.NotNull( hit.position );
//			}
//
//		
//			[UnitTest]
//			public void PathShouldBeAvailable2() {
//				GameVector a = new GameVector( -68.40066f, -14.7f, 101.9524f );
//				GameVector b = new GameVector( -132.9193f, -14.32582f, 41.47237f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetPath( a, b, out hit );
//				Assert.NotNull( hit.position );
//			}
//		
//			[UnitTest]
//			public void PathShouldBeAvailable4() {
//				GameVector a = new GameVector( -72.69505f, -14.32582f, 58.67208f );
//				GameVector b = new GameVector( -99.24321f, -14.32582f, 46.78305f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetPath( a, b, out hit );
//			
//				Assert.NotNull( path );
//				Assert.Equal( 2, path.Count );
//				Assert.Equal( collisionManager.GetSectorFromGameVector( a ), path[ 0 ] );
//				Assert.Equal( collisionManager.GetSectorFromGameVector( b ), path[ 1 ] );
//				Assert.CheckEquals( b, hit.position );
//			}
//		
//			[UnitTest]
//			public void CantShootThroughAVerySmallHole() {
//				GameVector origin = new GameVector( -46.11366f, -14.32582f, 84.98982f );
//				GameVector target = new GameVector( -78.83429f, -14.32582f, 51.41371f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				int expected = path[ path.Count - 1 ];
//				int got = collisionManager.GetSectorFromGameVector( hit.position );
//				
//				Assert.False( expected == got, "Sectors " + expected + " and " + got + " should be different" );
//			}
//		}
//	
//	public class CollisionManagerTrajectoryTestCase : TestCase {
//		
//			protected const string LEVEL_NAME = "teste_1";
//			protected CollisionManager collisionManager;
//			
//			
//			public override void SetUp() {
//				collisionManager = new CollisionManager( Path.Combine( Common.PATH_LEVEL_FILE, LEVEL_NAME + Common.LEVEL_FILE_EXTENSION ) );
//			}
//			
//			[UnitTest]
//			public void NullTrajectory() {
//				
//				GameVector origin = new GameVector( 5.0f, -1.29089f, -1.541715f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, origin, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 1, path.Count );
//				Assert.Equal( 2, path[ 0 ] );
//				
//				Assert.CheckEquals( origin, hit.position );
//				Assert.Equal( Common.NULL_WORLDOBJECT, hit.worldObjectId );
//			}
//			
//			[UnitTest]
//			public void SimpleTrajectory() {
//				GameVector origin = new GameVector( 5.0f, -1.29089f, -1.541715f );
//				GameVector target = new GameVector( 5.0f, -1.29089f, 2.458286f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 3, path.Count );
//				
//				Assert.Equal( 2, path[ 0 ] );
//				Assert.Equal( 4, path[ 1 ] );
//				Assert.Equal( 7, path[ 2 ] );
//				
//				Assert.CheckEquals( target, hit.position );
//				Assert.Equal( Common.NULL_WORLDOBJECT, hit.worldObjectId );
//			}
//			
//			[UnitTest]
//			public void HaltedTrajectory() {
//				GameVector origin = new GameVector( 2.5f, -1.29089f, -1.541715f );
//				GameVector target = new GameVector( 2.5f, -1.29089f, 0.45f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 1, path.Count );
//				
//				Assert.Equal( 3, path[ 0 ] );
//				Assert.Equal( Common.WALL_WORLDOBJECT, hit.worldObjectId );
//				
//				GameVector hitOnWall = new GameVector( 2.5f, -1.29089f, -0.541715f );			
//				Assert.CheckEquals( hitOnWall, hit.position );
//			}
//			
//			[UnitTest]
//			public void TrajectoryHittingAnotherCharacter() {
//				
//				// pretending that the sector is a character
//				int mockWorldObjectId = 4;
//				LevelSector characterSector = collisionManager.GetSector( 6 );
//				List< int > connectedSectors = new List< int >();
//				
//				foreach( LevelSector.Direction dir in LevelSector.Directions ) {
//					int sectorIndex = characterSector.GetLink( dir );
//					if( sectorIndex != 0 ) {
//						connectedSectors.Add( sectorIndex );
//					}
//				}
//				
//				foreach( int sectorIndex in connectedSectors ) {
//					LevelSector sector = collisionManager.GetSector( sectorIndex );
//					foreach( LevelSector.Direction dir in LevelSector.Directions ) {
//						
//						if( sector.GetLink( dir ) == characterSector.GetId() ) {
//							sector.SetLink( dir, -mockWorldObjectId );
//						}
//					}
//				}
//				
//			
//				collisionManager.AddCollisionCell( mockWorldObjectId, characterSector );
//			
//				
//				GameVector origin = new GameVector( 5.0f, -1.29089f, 2.458286f );
//				GameVector target = new GameVector( 0.0f, -1.29089f, 2.458286f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 2, path.Count );
//				
//				Assert.Equal( 7, path[ 0 ] );
//				Assert.Equal( 8, path[ 1 ] );
//				
//				Assert.Equal( mockWorldObjectId, hit.worldObjectId );
//				
//				GameVector hitOnCharacter = new GameVector( 1.0f, -1.29089f, 2.458286f );				
//				Assert.CheckEquals( hitOnCharacter, hit.position );
//			}
//		}
//	
//public class CollisionManagerTestLevel4TestCase : TestCase {
//
//		protected const string TEST_LEVEL = "Assets/Levels/teste_5" + Common.LEVEL_FILE_EXTENSION;
//		
//		protected CollisionManager collisionManager;
//		
//		public override void SetUp() {
//			collisionManager = new CollisionManager( TEST_LEVEL );
//		}
//		
//		[ UnitTest ]
//		public void TestSaveStateAndRestoreState() {
//			LevelSector s1;
//			LevelSector s2;
//			LevelSector s3;
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 0.0f, 0.0f, 2.0f, 2.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 1.0f, 1.0f, 1.0f, 2.0f, 2.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 1.0f, 1.0f, 1.0f, 3.0f, 3.0f, 3.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 3.0f, 3.0f, 3.0f, 5.0f, 5.0f, 5.0f ) );
//			
//			
//			collisionManager.GetSector( 0 ).SetFixedShape( true );
//			collisionManager.GetSector( 1 ).SetFixedShape( true );
//			collisionManager.GetSector( 2 ).SetFixedShape( true );
//			
//			s1 = new LevelSector( collisionManager.GetSector( 0 ) );
//			s2 = new LevelSector( collisionManager.GetSector( 1 ) );
//			s3 = new LevelSector( collisionManager.GetSector( 2 ) );
//			
//			
//			collisionManager.SaveState();
//			
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 1.0f, 0.0f, 2.0f, 4.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 1.0f, 14.0f, 1.0f, 2.0f, 20.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 11.0f, 1.0f, 1.0f, 113.0f, 3.0f, 3.0f ) );
//			
//			collisionManager.Wipe();
//			
//			collisionManager.RestoreState();
//			
//			Assert.CheckEquals( 3, collisionManager.GetTotalSectors() );
//			
//			Assert.CheckEquals( s1, collisionManager.GetSector( 0 ) );
//			Assert.CheckEquals( s2, collisionManager.GetSector( 1 ) );
//			Assert.CheckEquals( s3, collisionManager.GetSector( 2 ) );
//			
//			Assert.False( s1 == collisionManager.GetSector( 0 ) );
//			Assert.False( s2 == collisionManager.GetSector( 1 ) );
//			Assert.False( s3 == collisionManager.GetSector( 2 ) );
//		}
//		
//		[ UnitTest ]
//		public void TestRecalculateBoxesTouching() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 2.0f, 5.0f, 0.0f, 9.0f, 4.0f, 5.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 5, collisionManager.GetTotalSectors() );
//		}
//		
//		[ UnitTest ]
//		public void TestRecalculateBoxesOneInsideAnother() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 4.0f, 5.0f, 4.0f, 5.0f, 4.0f, 5.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 28, collisionManager.GetTotalSectors() );
//		}
//
//		
//		[ UnitTest ]
//		public void TestRecalculateBoxCrossingToAnother() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 4.0f, 5.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 37, collisionManager.GetTotalSectors() );
//		}
//
//		[ UnitTest ]
//		public void TestRecalculateBoxOn4() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 8.0f, 10.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 49, collisionManager.GetTotalSectors() );
//		}	
//			
//		[ UnitTest ]
//		public void TestEnumeratorForFixedSectorsOnly() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 255.0f, 0.0f, 255.0f, 0.0f, 255.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 8.0f, 10.0f ) );
//			
//			int counter = 0;
//			
//			foreach ( LevelSector sector in collisionManager ) {
//				Assert.CheckEquals( collisionManager.GetSector( counter ), sector );
//				++counter;
//			}
//		}
//		
//		[ UnitTest ]
//		public void TestEnumerator() {
//			collisionManager.Wipe();
//			LevelSector collidible;
//			
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 255.0f, 0.0f, 255.0f, 0.0f, 255.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			
//			collidible = LevelSector.MakeSectorAt( 4.0f, 5.0f, 4.0f, 5.0f, 4.0f, 5.0f );
//			collisionManager.AddCollisionCell( 0, collidible );
//			
//			int id = 0;
//			int counter = 0;
//			
//			foreach ( LevelSector sector in collisionManager ) {
//
//				if ( id < collisionManager.GetTotalSectors() ) {
//					Assert.CheckEquals( collisionManager.GetSector( id ), sector );
//				} else {
//					Assert.CheckEquals( collidible, sector );
//				}
//				
//				++id;
//				++counter;
//			}
//			
//			Assert.CheckEquals( 3, id );
//			Assert.CheckEquals( 3, counter );
//			Assert.CheckEquals( 2, collisionManager.GetTotalSectors() );
//		}
//		
//		[ UnitTest ]
//		public void TestIdPreservationOnBoxRecalculation() {
//			collisionManager.Wipe();
//			LevelSector collidible;
//			
//			
//			
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//
//			collidible = LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 8.0f, 10.0f );
//			
//			collisionManager.AddCollisionCell( 1, collidible );
//			PortalizerWrapper.Recalculate( collisionManager );
//			Assert.CheckEquals( 45, collisionManager.GetTotalSectors() );
//		}		
//	}	

	public class CollisionManagerRaycastTestCase : TestCase {
	
		protected CollisionManager cm;
		
		public override void SetUp() {
			cm = new CollisionManager();
			// TODO PETER
//			cm.AddBox( new Box( new GameTransform() ), false );
		}
		
		[UnitTest]
		public void MissingSimpleBox() {
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( -1.0f, 1.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.NO_INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void HittingSimpleBox() {	
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 1.0f, 0.0f, 0.0f );
			
			HitInfo hit;
			cm.Raycast( a, b, out hit );
		}
		
		
		[UnitTest]
		public void AnotherSimpleHitTest() {	
			GameVector a = new GameVector( -0.2f, 1.0f, 0.0f );
			GameVector b = new GameVector( -0.2f, -1.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( -0.2f, 0.5f, 0.0f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void HittingOnTheEdge() {	
			GameVector a = new GameVector( 1.0f, 1.0f, 0.0f );
			GameVector b = new GameVector( -1.0f, -1.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( 0.5f, 0.5f, 0.0f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void HittingOnTheCorner() {	
			GameVector a = new GameVector( -1.0f, -1.0f, -1.0f );
			GameVector b = new GameVector( 1.0f, 1.0f, 1.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( -0.5f, -0.5f, -0.5f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void TargetIsInBoxFace() {
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( -0.5f, 0.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void TargetIsInBoxCorner() {
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( -0.5f, 0.5f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void OriginAndTargetAreInBoxFace() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.5f );
			GameVector b = new GameVector( 0.0f, 0.2f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como ambos est�o na face,
			// o algoritmo assume que o primeiro ponto entra no cubo
			// e o segundo sai
			Assert.CheckEquals( a, hit.position );
		}
		
		[UnitTest]
		public void OriginAndTargetAreInOppositeBoxFaces() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.5f );
			GameVector b = new GameVector( 0.0f, 0.0f, -0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como ambos est�o em faces do cubo face,
			// o algoritmo assume que o primeiro ponto entra no cubo
			// e o segundo sai
			Assert.CheckEquals( a, hit.position );
		}
		
		[UnitTest]
		public void OriginAndTargetAreInBoxCorners() {
			GameVector a = new GameVector( -0.5f, -0.5f, -0.5f );
			GameVector b = new GameVector( -0.5f, -0.5f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como ambos est�o em uma mesma face,
			// (a que compartilha os cantos), o algoritmo assume que o primeiro ponto 
			// entra no cubo e o segundo sai
			Assert.CheckEquals( a, hit.position );
		}
		
		[UnitTest]
		public void RaycastDoesNotLeaveTheBox() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 0.1f, 0.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.NO_INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void RaycastFromInside() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 1.0f, 0.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( 0.5f, 0.0f, 0.0f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void RaycastFromCornerToOutsideTheBox() {
			GameVector a = new GameVector( 0.5f, 0.5f, 0.5f );
			GameVector b = new GameVector( 0.1f, 0.5f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como a est� na face e b est� fora,
			// o algoritmo assume que o primeiro ponto sai do cubo e � a primeira
			// interception
			Assert.CheckEquals( a, hit.position );
		}
	}
	
	public class CollisionManagerImportingTestCase : TestCase {
		
		[UnitTest]
		public void Importing() {
			CollisionManager cm = new CollisionManager( "Test1" );
			
			Assert.Equal( 3, cm.GetBoxes().Count );
		}
	}
}