using System;
using System.Collections.Generic;
using Utils;
using GameCommunication;


namespace GameModel {
	
	public class StatisticsManager{
		
		protected Mission mission;
		private DateTime startTime;
		private DateTime endTime;
		private int turns;
		
		Dictionary< Character, BattleEvent > characterStatistics;
		
		
		public StatisticsManager ( Mission mission ){
			characterStatistics = new Dictionary<Character, BattleEvent> ();
			
			this.mission = mission;
			
			foreach( Character character in mission.GetCharacters() )
				RegisterCharacter( character );
			
			turns = 0;
			
		}
		
		protected void RegisterCharacter( Character character ){
			characterStatistics.Add( character , new BattleEvent() );
		}
		
		
		public void AddBattleEvent( Character character, BattleEventType battleEventType, int val ){
			if( characterStatistics.ContainsKey( character ) ){
				characterStatistics[ character ].AddValue( val , battleEventType);
			}
		}
		
		public void ClearAllStatistics(){
			foreach(KeyValuePair< Character, BattleEvent > k in characterStatistics){
				k.Value.ResetAllValues();
			}
			turns = 0;
		}
		
		public void OnCharacterKilled( Character killerCharacter, Character deadCharacter , bool headshot = false ){
			if( characterStatistics.ContainsKey( killerCharacter ) &&  characterStatistics.ContainsKey( deadCharacter ) ){
				
				if( killerCharacter != deadCharacter){
					characterStatistics[ killerCharacter ].AddValue( 1 , BattleEventType.KILLS );
				}
				
				characterStatistics[ deadCharacter ].AddValue( 1 , BattleEventType.DEATHS );
				
				if(deadCharacter.Faction == Faction.NEUTRAL){
					characterStatistics[ killerCharacter ].AddValue( 1, BattleEventType.NPCKILLS );
				}
				
				if(headshot){
					characterStatistics[ killerCharacter ].AddValue( 1, BattleEventType.HEADSHOTS );
				}
			}
		}
		
		public void OnCharacterShoot( Character character, Weapon weapon , int quantityOfAmmunition ){
			if( characterStatistics.ContainsKey( character ) ){
				characterStatistics[ character ].AddValue(quantityOfAmmunition , BattleEventType.SHOOTSGIVEN);
				characterStatistics[ character ].updateWeapon(weapon, quantityOfAmmunition);
			}
		}
		
		public void OnCharacterThrowObject( Character character, Weapon weapon , int quantityOfAmmunition){
			if( characterStatistics.ContainsKey( character ) ){
				characterStatistics[ character ].AddValue(quantityOfAmmunition , BattleEventType.THROWOBJECTS);
				characterStatistics[ character ].updateTrowObject(weapon, quantityOfAmmunition);
			}
		}
		
		public void OnMissionStart(){
			this.startTime = DateTime.Now;
		}
		
		//Calculate Statics
		public void OnMissionEnded() {
			//Luiz - When the code reaches this part, you have all the numbers necessary to create the end of game GUI
			this.endTime = DateTime.Now;
			TurnData turndata = this.mission.GetTurnManager ().GetTurnData ();
			turns = turndata.turnNumber;
			//Returns something from here ?
			
			//DebugStuff -- Luiz
			foreach (KeyValuePair< Character, BattleEvent > k in characterStatistics) {
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " Deaths: " + characterStatistics [k.Key].GetValue (BattleEventType.DEATHS));
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " Finalizations: " + characterStatistics [k.Key].GetValue (BattleEventType.FINALIZATIONS));
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " Headshots: " + characterStatistics [k.Key].GetValue (BattleEventType.HEADSHOTS));
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " Kills: " + characterStatistics [k.Key].GetValue (BattleEventType.KILLS));
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " NPCKills: " + characterStatistics [k.Key].GetValue (BattleEventType.NPCKILLS));
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " Shoots Given: " + characterStatistics [k.Key].GetValue (BattleEventType.SHOOTSGIVEN));
				Debugger.Log (" ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID () + " Throw Objects: " + characterStatistics [k.Key].GetValue (BattleEventType.THROWOBJECTS));
				Debugger.Log ( " ( " + k.Key.Faction + " ) " + "Character " + k.Key.GetWorldID() + " Shoots Taken: " + characterStatistics[ k.Key ].GetValue(BattleEventType.SHOOTSTAKEN) );
			}
		}
		
		
	}
}

