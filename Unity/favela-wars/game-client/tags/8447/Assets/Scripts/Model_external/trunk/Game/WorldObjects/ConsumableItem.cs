using System;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public abstract class ConsumableItem : Item {
		
		private ItemTargetMode mode = ItemTargetMode.Character;
		
		private float range = 3;
		
		private int usesLeft = 1;
		
		public ConsumableItem() : base( 0, "" ) {
		}
		
		
		/// <summary>
		/// Alcance do item, em metros.
		/// </summary>
		[ ToolTip( "Alcance do item, em metros." ) ]
		public float Range {
			get { return range; }
			set { range = value; }
		}
		
		
		[ ToolTip( "Modo de uso do item." ) ]
		public ItemTargetMode Mode {
			get { return mode; }
			set { mode = value; }
		}
		

		
		[ ToolTip( "Quantidade de usos restantes do item." ) ]
		public int UsesLeft {
			get { return usesLeft; }
			set { usesLeft = value; }
		}
		
		
		public override GameAction GetPossibleActionsList() {
			GameAction toReturn = base.GetPossibleActionsList();
			
			if ( usesLeft > 0 ) {
				UseItemGameAction a = new UseItemGameAction( EquipID, Range, mode );
				a.range = Range;
				toReturn.AddChild( a );
			}
			
			return toReturn;
		}
		
		
		/// <summary>
		/// Método que realiza as ações necessárias quando efetivamente consumimos o item.
		/// </summary>
		protected abstract void Consume( GameVector target, Character character );
		
		
		/// <summary>
		/// Usa o item num local ou personagem.
		/// </summary>
		/// <param name='target'>
		/// Local onde o item será usado. Caso o item seja de uso específico em um personagem, esse valor pode ser <c>null</c>.
		/// </param>
		/// <param name='character'>
		/// Personagem no qual o item será usado. Caso o item seja de uso específico em um local, esse valor pode ser <c>null</c>.
		/// </param>
		/// <returns>
		/// <c>bool</c> indicando se há cargas restantes do item.
		/// </returns>
		public bool Use( GameVector target, Character character ) {
			if ( usesLeft > 0 ) {
				Consume( target, character );
				--usesLeft;
			}
			
			return UsesLeft > 0;
		}
	
	}
	
}

