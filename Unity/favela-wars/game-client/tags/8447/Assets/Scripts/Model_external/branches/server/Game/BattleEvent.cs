using System;
using System.Collections.Generic;

namespace GameModel
{
	
	public enum BattleEventType{
			SHOOTSGIVEN = 0,
			SHOOTSTAKEN,
			THROWOBJECTS,
			HEADSHOTS,
			DEATHS,
			KILLS,
			FINALIZATIONS,
			NPCKILLS
	}
	
	
	
	public class BattleEvent{
		
		private Dictionary< BattleEventType , int > battleEvents;
		private Dictionary< Weapon , int > weaponCount;
		private Dictionary< Weapon , int > trowObjectCount;
		
		public BattleEvent(){
			//Init
			battleEvents = new Dictionary< BattleEventType , int > ();
			weaponCount = new Dictionary<Weapon, int> ();
			trowObjectCount = new Dictionary<Weapon, int> ();
			AddKeys();
			//ResetAllValues();
		}
		
		public void AddKeys(){
			//Todo: melhorar isso aqui!
			battleEvents.Add( BattleEventType.SHOOTSGIVEN, 0 );
			battleEvents.Add( BattleEventType.SHOOTSTAKEN, 0 );
			battleEvents.Add( BattleEventType.THROWOBJECTS, 0 );
			battleEvents.Add( BattleEventType.HEADSHOTS, 0 );
			battleEvents.Add( BattleEventType.DEATHS, 0 );
			battleEvents.Add( BattleEventType.KILLS, 0 );
			battleEvents.Add( BattleEventType.FINALIZATIONS, 0 );
			battleEvents.Add( BattleEventType.NPCKILLS, 0 );
		}
		
		public void AddValue(int val, BattleEventType type) {
			battleEvents[ type ] += val;
		}
		
		public int GetValue(BattleEventType type){
			return battleEvents[ type ];
		}
		
		public void ResetValue(BattleEventType type){
			battleEvents[ type ] = 0;
		}
		
		public void ResetAllValues(){
			foreach(KeyValuePair < BattleEventType , int > k in battleEvents){
				battleEvents[ k.Key ] = 0;
			}
			weaponCount.Clear();
			trowObjectCount.Clear();
		}
		
		public void updateWeapon( Weapon weapon , int quantityOfAmmunition ){
			if(weaponCount.ContainsKey(weapon) == false){
				weaponCount.Add(weapon, 0);
			}
			weaponCount[ weapon ] += quantityOfAmmunition;
		}
		
		public void updateTrowObject( Weapon weapon , int quantityOfAmmunition ){
			if(trowObjectCount.ContainsKey(weapon) == false){
				trowObjectCount.Add(weapon, 0);
			}
			trowObjectCount[ weapon ] += quantityOfAmmunition;
		}
		
		public float CalculateAccuracy(){
			return (battleEvents[ BattleEventType.KILLS ] / (float)battleEvents[ BattleEventType.SHOOTSGIVEN ]) * 100f ;
		}
	}
	
}

