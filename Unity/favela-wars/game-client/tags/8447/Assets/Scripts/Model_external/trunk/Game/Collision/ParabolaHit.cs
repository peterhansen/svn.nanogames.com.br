using System;

using GameCommunication;


namespace GameModel {
	
	public class ParabolaHit {
		
		public float time;
		
		public GameVector position;
		
		public GameVector direction;
		
		
		public ParabolaHit( float time, GameVector position, GameVector direction ) {
			this.time = time;
			this.position = position;
			this.direction = direction;
		}
	
	}
}

