using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class LightGun : FireArm {

				
		public LightGun() : base( FireArmType.PISTOL ) {
		}
		
		
		public override AttackReply Attack( GameVector origin, GameVector target, GameVector deviation, ItemUseMode useMode ) {
			AttackReply reply = new AttackReply();

			target += GameVector.GetRandom( -deviation, deviation );

			reply.shots.Add( new ShotInfo( origin, target, Power ) );
			return reply;
		}
		
		
//		public override float GetAPCost( UseMode useMode ) {
//			throw new NotImplementedException ();
//		}
		
		public override float GetUseModeAPCost( ItemUseMode useMode ) {
			// TODO: parametrizar isso aqui
			return 10.0f;
		}
		
		
		public override IEnumerable< ItemUseMode > GetAvailableUseModes() {
			yield return ItemUseMode.REGULAR;
		}
	}
	
}

