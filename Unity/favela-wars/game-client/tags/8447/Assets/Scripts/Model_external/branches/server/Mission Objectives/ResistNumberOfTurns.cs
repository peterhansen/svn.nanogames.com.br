using System;

using Utils;
using GameCommunication;


namespace GameModel {
	public class ResistNumberOfTurns : MissionObjective {
		
		protected ResistNumberOfTurnsData data;
		
		
		public ResistNumberOfTurns( ResistNumberOfTurnsData data ) {
			this.data = data;
		}
		
		
		public ResistNumberOfTurns( Faction faction, int numberOfTurns ) {
			data.faction = faction;
			data.numberOfTurns = numberOfTurns;
		}
		
		
		protected override ObjectiveResult CheckWhenTurnChanged( Mission mission, TurnData turnData ) {
			ObjectiveResult result = new ObjectiveResult();
			
			// Como toda missão possui um DestrouAllEnemies, estamos assumindo
			// aqui que o DestroyAllEnemies ainda gerou um EndMissionResult.
			// Só precisamos então contar os turnos
			if( MaxNumberOfTurnsReached( mission ) ) {
				result.missionIsOver = true;
				result.winningFaction = data.faction;
			}
			
			return result;
		}
		
		// TODO: refatorar com DestroyAllEnemies? (tem um método muito parecido)
		protected bool MaxNumberOfTurnsReached( Mission mission ) {
			return mission.GetTurnManager().GetTurnData().turnNumber > data.numberOfTurns;
		}
	}
}
