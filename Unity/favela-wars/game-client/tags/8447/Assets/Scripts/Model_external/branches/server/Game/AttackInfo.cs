using System;
using System.Collections;
using System.Collections.Generic;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class AttackInfo {
		
		private float power;
		
		private GameVector origin;
		
		private GameVector target;
		
		// TODO efeito do disparo ? (normal, incendiário, explosivo, venenoso, etc)
		
		
		public AttackInfo( GameVector origin, GameVector target, float power ) {
			this.origin = origin;
			this.target = target;
			this.power = power;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		public GameVector Origin {
			get { return this.origin; }
			set { origin = value; }
		}
		

		/// <summary>
		/// 
		/// </summary>
		public float Power {
			get { return this.power; }
			set { power = value; }
		}
		
		
		/// <summary>
		/// Posição final do disparo. Alguns projéteis têm alcance limitado, logo o projétil pode "morrer" antes do
		/// alvo efetivo do tiro.
		/// </summary>
		public GameVector Target {
			get { return this.target; }
			set { target = value; }
		}
		
	}
	
	
	public class AttackOutOfRangeException : Exception {
		
		
		
	}
}

