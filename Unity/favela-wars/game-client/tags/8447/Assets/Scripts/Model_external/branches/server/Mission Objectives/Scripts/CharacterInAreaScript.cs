//using System.Collections;
//
//using UnityEngine;
//
//using Utils;
//using GameCommunication;
//
//
//public class CharacterInAreaScript : MissionObjectiveScript {
//
//	public Area area;
//	public Faction winningFaction;
//	public Faction targetFaction;
//	public CharacterTargetMode.Type type;
//	
//	
//	public override MissionObjective GetMissionObjective() {
//		return new CharacterInArea( area.GetMissionData(), new CharacterTargetMode( type, targetFaction ), winningFaction );
//	}
//}