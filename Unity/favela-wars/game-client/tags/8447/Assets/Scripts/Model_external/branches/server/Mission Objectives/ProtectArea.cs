using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class ProtectArea : MissionObjective {
		
		protected ProtectAreaData data = new ProtectAreaData();
		
		
		public ProtectArea( ProtectAreaData data ) {
			this.data = data;
		}
		
		
		public ProtectArea( AreaMissionData area, int numberOfTurns ) {
			data.area = area;
			data.numberOfTurns = numberOfTurns;
		}
		
	
		protected override ObjectiveResult CheckWhenTurnChanged( Mission mission, TurnData turnData ) {
			//Dictionary< Faction, bool > isInArea = new Dictionary< Faction, bool >();
			bool policeIsInArea = false;
			bool criminalIsInArea = false;
			
			foreach ( Character character in mission.GetWorldObjects() ) {
				if( character.GetBox().Intersects( data.area.box ) ) {
					if( character.Data.Faction == Faction.POLICE ) {
						policeIsInArea = true;
					} else if( character.Data.Faction == Faction.CRIMINAL ) {
						criminalIsInArea = true;
					}
				}
			}
			
			data.policeTurnsInArea = policeIsInArea ? data.policeTurnsInArea + 1: 0;
			data.criminalTurnsInArea = criminalIsInArea ? data.criminalTurnsInArea + 1: 0;
			
			Debugger.Log( "Police turns in area: " + data.policeTurnsInArea );
			Debugger.Log( "Criminal turns in area: " + data.criminalTurnsInArea );
			
			// caso os dois já consigam satisfazer a condicao de vitória ou caso
			// nenhum dos dois tenha satisfeito, nada acontece
			if( data.policeTurnsInArea > data.numberOfTurns && data.criminalTurnsInArea <= data.numberOfTurns ) {
				return new ObjectiveResult( true, Faction.POLICE );
			} else if( data.criminalTurnsInArea > data.numberOfTurns && data.policeTurnsInArea <= data.numberOfTurns ) {
				return new ObjectiveResult( true, Faction.CRIMINAL );
			} else {
				return new ObjectiveResult();
			}
		}
		
	}
	
}
