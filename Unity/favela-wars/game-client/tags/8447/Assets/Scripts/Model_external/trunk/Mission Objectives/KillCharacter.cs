using System;

using Utils;
using GameCommunication;


namespace GameModel {
	public class KillCharacter : MissionObjective {
		
		protected KillCharacterData data;
		
		
		public KillCharacter( KillCharacterData data ) {
			this.data = data;
		}
		
		
		public KillCharacter( Faction winningFaction, CharacterTargetMode target ) {
			data.target = target;
			data.winningFaction = winningFaction;
		}
		
		
		public override void OnInit( Mission mission ) {
			if( data.target.type == CharacterTargetMode.Type.TROOP_LEADER ) {
				// TODO: ainda precisamos de uma forma de identificar os personagens
				// que nao dependa de seus ids gerados dinamicamente. Por enquanto, estamos
				// carteando que o cara de uma faccao com o menor ID é o lider
				int minId = int.MaxValue;
				foreach( Character character in mission.GetCharacters( data.target.faction ) ) {
					if( character.GetWorldID() < minId ) {
						minId = character.GetWorldID();
						data.targetedCharacterID = character.GetWorldID();
					}
				}
			}
		}
		
		
		protected override ObjectiveResult CheckWhenCharacterKilled( Mission mission, Character character ) {
			Character targetedCharacter =  mission.GetCharacter( data.targetedCharacterID );
			
			if( targetedCharacter != null && character == targetedCharacter ) {
				return new ObjectiveResult( true, data.winningFaction );
			} else {
				return new ObjectiveResult();
			}
		}
	}

}
