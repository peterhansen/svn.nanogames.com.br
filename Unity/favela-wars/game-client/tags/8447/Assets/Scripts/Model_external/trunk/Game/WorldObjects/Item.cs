using System;
using System.ComponentModel;
using System.Collections.Generic;

using GameCommunication;

using Utils;


namespace GameModel {
	
	[ Serializable ]
	public abstract class Item : WorldObject {
		
		public class UseModeNotAvailable : System.Exception {
			public UseModeNotAvailable( ItemUseMode useMode ) : base( "Modo de uso " + useMode + " não implementado." ) {
			}
		}
		
		protected ItemSize size;
		
		protected int equipID;
		
		protected float restitutionRate = 0.6f;


		public Item( int worldObjectID, string assetBundleID ) : base( worldObjectID, assetBundleID ) {
		}
		
		
		public float GetAPCost( ItemUseMode useMode ) {
			if( !IsUseModeAvailable( useMode ) )
				throw new UseModeNotAvailable( useMode );
			
			return GetUseModeAPCost( useMode );
		}
		
		
		public virtual float GetUseModeAPCost( ItemUseMode useMode ) {
			return 0.0f;
		}
		
		
		public virtual IEnumerable< ItemUseMode > GetAvailableUseModes() {
			yield break;
		}
		
		
		public bool IsUseModeAvailable( ItemUseMode useMode ) {
			foreach( ItemUseMode availableUseMode in GetAvailableUseModes() )
				if( useMode == availableUseMode )
					return true;
			
			return false;
		}
		
		
		/// <summary>
		/// Tamanho do item.
		/// </summary>
		[ ToolTip( "Tamanho do item." ) ]
		public ItemSize Size {
			get { return this.size; }
			set { size = value; }
		}	
		
		
		// TODO EquipID é a melhor forma de identificar unicamente os itens dos usuários?
		/// <summary>
		/// Identificação única deste item junto ao personagem. 
		/// </summary>
		[ ToolTip( "Identificação única deste item junto ao personagem." ) ]
		public int EquipID {
			get { return this.equipID; }
			set { equipID = value; }
		}
		
		
		/// <summary>
		/// Coeficiente de restituição do item ao ser arremessado. Os valores válidos estão na faixa [0,1] (inclusive).
		/// </summary>
		[ ToolTip( "Coeficiente de restituição do objeto ao ser arremessado. Os valores válidos estão na faixa [0,1]. Quanto maior o valor, mais o objeto conserva sua velocidade após colidir com algum outro objeto." ) ]
		public float RestitutionRate {
			get { return restitutionRate; }
			set { restitutionRate = NanoMath.Clamp( value, 0, 1 ); }
		}
		
		
		/// <summary>
		/// Obtém a lista de ações disponíveis para o item.
		/// </summary>
		public virtual GameAction GetPossibleActionsList() {
			GameAction toReturn = new GameAction( Utils.ActionType.NONE );
			
			// ações comuns a todos os itens entram aqui
			
			return toReturn;
		}
		
	}
	
}

