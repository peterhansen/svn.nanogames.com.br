using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using Utils;
using GameCommunication;


public class SpawnAreaDrug : SpawnArea {
	
	public SpawnAreaDrug() : base() {
	}
	
	
	public override Faction GetFaction() {
		return Faction.CRIMINAL;
	}
	
}
