using UnityEngine;

using System;
using System.Collections;

using Utils;


//
#if UNITY_EDITOR
using UnityEditor;

[ ExecuteInEditMode ]
public class ObjectSnapper : MonoBehaviour {
	
	public enum SnapMode {
		Wall,
		Column,
		Floor,
		Marquee
	}
	
	public enum WallMode {
		WallAndColumn,
		WallOnly,
		ColumnOnly,
	}
	
	/// <summary>
	/// Valor usado para separar levemente o ch�o das superf�cies inferiores, evitando assim z-fight.
	/// </summary>
	private const float FLOOR_EPSILON = 0.0111f;
	
	public const float DEFAULT_WALL_WIDTH = 2.5f;
	
	public float wallWidth = DEFAULT_WALL_WIDTH;
	
	public const float DEFAULT_WALL_THICKNESS = 0.1f;
	
	private Vector3 lastPosition = new Vector3();
	
	private float totalWidth;
	
	protected Vector3 center = new Vector3();
	
	public SnapMode snapMode = SnapMode.Wall;
	
	private int lastSnapAngle;
	
	private bool horizontal;
	
	private WallMode wallMode = WallMode.WallAndColumn;
	
	
	public void SetCenter( Vector3 c ) {
		center = c;
	}
	
	
	public void Update() {
		if ( PrefabUtility.GetPrefabType( gameObject ) == PrefabType.Prefab )
			return;
		
		Update( false );
	}
	
	
	public void Awake() {
		WallDrawMode = wallMode;
		RefreshRotation();
	}
	
	
	public WallMode WallDrawMode {
		get { return wallMode; }
		set { 
			wallMode = value; 
			
			// evita exce��es e lentid�o do editor caso o objeto selecionado seja ainda um prefab (na aba Project), 
			// pois n�o � poss�vel alterar o estado "active" do objeto neste caso
			if ( PrefabUtility.GetPrefabType( gameObject ) == PrefabType.Prefab )
				return;
			
			foreach ( Transform t in transform ) {
				switch ( wallMode ) {
					case WallMode.ColumnOnly:
						t.gameObject.active = t.name.ToLower().StartsWith( "coluna" );
					break;

					case WallMode.WallOnly:
						t.gameObject.active = !t.name.ToLower().StartsWith( "coluna" );
					break;
					
					case WallMode.WallAndColumn:
					default:
						t.gameObject.active = true;
					break;
				}
			}
		}
	}
	
	
	public void Update( bool force ) {
		Transform t = transform;
		Vector3 p = t.localPosition;
		if ( force || ( p.Equals( lastPosition ) && t.localEulerAngles.y == lastSnapAngle ) )
			return;
		
		// posiciona os filhos corretamente
		if ( GetComponent< PrefabDescriptor >() == null ) {
			Renderer[] renderers = ( Renderer[] ) GetComponentsInChildren< Renderer >();
			foreach( Renderer r in renderers ) {
				if ( r != null && r.gameObject != gameObject ) {
					r.gameObject.transform.localPosition = new Vector3( center.x, center.y, center.z );
				}
			}
		}
		
		totalWidth = wallWidth + DEFAULT_WALL_THICKNESS;
		float halfWidth = totalWidth * 0.5f;
		
		switch ( snapMode ) {
			case SnapMode.Column:
				p.x = Round( p.x, halfWidth );
				p.z = Round( p.z, 0.0f );
				p.y = Round( p.y, DEFAULT_WALL_WIDTH * 0.5f, DEFAULT_WALL_WIDTH );
			break;
			
			case SnapMode.Wall:
				if ( t.localEulerAngles.y != lastSnapAngle )
					RefreshRotation();
				
				float initial = 0.0f; 
				if ( ( (int) ( wallWidth / DEFAULT_WALL_WIDTH ) ) % 2 == 0 )
					initial = ( DEFAULT_WALL_THICKNESS + DEFAULT_WALL_WIDTH ) * -0.5f;
			
				if ( !p.Equals( lastPosition ) ) {
					if ( horizontal ) {
						p.x = initial + Round( p.x, halfWidth, DEFAULT_WALL_WIDTH + DEFAULT_WALL_THICKNESS );
						p.z = Round( p.z, halfWidth, DEFAULT_WALL_WIDTH + DEFAULT_WALL_THICKNESS );
					} else {
						p.x = ( DEFAULT_WALL_THICKNESS * 0.5f ) + initial + Round( p.x, 0.0f, DEFAULT_WALL_WIDTH + ( DEFAULT_WALL_THICKNESS * 1f ) );
						p.z = ( DEFAULT_WALL_THICKNESS * 0.5f ) + Round( p.z, 0.0f, DEFAULT_WALL_WIDTH + DEFAULT_WALL_THICKNESS );
					}
				
					// faz o snap em y
					p.y = Round( p.y, DEFAULT_WALL_WIDTH * 0.5f, DEFAULT_WALL_WIDTH );
				}
			break;
			
			case SnapMode.Floor:
				if ( t.localEulerAngles.y != lastSnapAngle )
					RefreshRotation();
				
				if ( !p.Equals( lastPosition ) ) {
					p.x = -DEFAULT_WALL_THICKNESS * 0.5f + Round( p.x, 0 );
				
					// ajusta a altura em fun��o de pisos com diferentes alturas (volumes)
					Bounds b = GameController.ControllerUtils.GetBox( gameObject, false, true );
					p.y = ( b.extents.y > 0 ? 0 : FLOOR_EPSILON ) + Round( p.y, 0, DEFAULT_WALL_WIDTH ) + b.extents.y;
					p.z = Round( p.z, halfWidth );
				}
			break;

			case SnapMode.Marquee:
				if ( t.localEulerAngles.y != lastSnapAngle )
					RefreshRotation();
				
				if ( !p.Equals( lastPosition ) ) {
					p.x = -DEFAULT_WALL_THICKNESS * 0.5f + Round( p.x, 0 );
				
					// ajusta a altura em fun��o de pisos com diferentes alturas (volumes)
					Bounds b = GameController.ControllerUtils.GetBox( gameObject, false );
					p.y = Round( p.y, 0, DEFAULT_WALL_WIDTH ) - b.extents.y;
					p.z = -DEFAULT_WALL_THICKNESS * 0.5f + Round( p.z, halfWidth );
				}
			break;
		}
		
		t.localPosition = p;
		lastPosition = p;
	}
	
	
	private void RefreshRotation() {
		lastSnapAngle = ( int ) ( Mathf.Round( transform.localEulerAngles.y * 4 / 360 ) * 90 ) % 360;
		horizontal = ( lastSnapAngle % 180 ) != 0;
		transform.localEulerAngles = new Vector3( transform.localEulerAngles.x, lastSnapAngle, transform.localEulerAngles.z );
	}
	
	
	public void SetSnapMode( string s ) {
		switch ( s ) {
			case Common.Paths.COLUMNS_PREFIX:
				SetSnapMode( SnapMode.Column );
			break;

			case Common.Paths.WALL_PREFIX:
				SetSnapMode( SnapMode.Wall );
			break;
			
			case Common.Paths.MARQUEE_PREFIX:
				SetSnapMode( SnapMode.Marquee );
			break;
			
			case Common.Paths.FLOORS_PREFIX:
			case Common.Paths.FLOORS_PREFIX2:
			case Common.Paths.FLOORS_PREFIX3:
				SetSnapMode( SnapMode.Floor );
			break;
		}
	}
	
	
	public void SetSnapMode( SnapMode s ) {
		snapMode = s;
		Update( true );
	}
	
	
	private float Round( float input, float initial ) {
		return Round( input, initial, totalWidth );
	}
	
	
	private float Round( float input, float initial, float total ) {
        float snappedValue;

        snappedValue = initial + total * Mathf.Round( ( ( input - initial ) / total ) );

        return snappedValue;
    }
	
	
	private Vector2 GetCell( Vector3 pos ) {
		return new Vector2( ( int ) ( Math.Floor( pos.x ) / totalWidth ), ( int ) ( Math.Floor( pos.z ) / totalWidth ) );
	}
	
	
}
#endif