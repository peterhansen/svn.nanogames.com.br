#if UNITY_EDITOR
using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

using GameCommunication;
using Utils;
using SharpUnit;

using Attribute = GameCommunication.Attribute;


/// <summary>
/// Esta classe permite a edição em tempo de execução dos parâmetros gerais de game design, como velocidades máximas
/// e mínimas dos personagens, consumo de AP para cada tipo de ação, etc.
/// Para adicionar novos campos ao editor, basta criar as propriedades get/set correspondentes, com classes que herdam
/// de Editable.
/// </summary>
[ ExecuteInEditMode ]
public class ItemEditor : GenericEditor {
	
	// TODO PETER ajustar ItemEditor após alterações
	
//	private List< Item > items = new List< Item >();
	
	
	public ItemEditor() {
//		data = items;
	}
	
	
	public void Start() {
		string path = Path.Combine( Application.absoluteURL, "items" );
		Directory.CreateDirectory( path );
		string[] files = Directory.GetFiles( path, "*.item" );
	
		for ( int i = 0; i < files.Length; ++i ) {
			string itemPath = Path.GetFileNameWithoutExtension( files[ i ] );
//			Item item = Util.DeserializeFromXMLFile( itemPath );
		}
	}
	
	
	public override string GetTitle() {
		return "Item Editor";
	}
	
	
}
#endif
