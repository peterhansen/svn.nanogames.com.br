using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

using Pathfinding;

using SharpUnit;
using GameCommunication;
using Utils;
using GameController;

using Object = UnityEngine.Object;
using Debugger = Utils.Debugger;
using HashSetSpawnArea = System.Collections.Generic.Dictionary< SpawnArea, int >;
using HashSetString = System.Collections.Generic.Dictionary< string, int >;

#if UNITY_EDITOR
using UnityEditor;


public class MissionGenerator : MonoBehaviour {
	
	private const string MENU_ITEM = "Mission Editor/Export Mission %g";
	
	// TODO o nome da missão deve estar posteriormente disponível em diversos idiomas
	public string missionName = "";
	
	/// <summary>
	/// Facção "dona da casa", que tem como principal vantagem já enxergar todo o mapa desde o começo.
	/// </summary>
	public Faction levelOwner = Faction.NONE;
	
	protected string sceneName;
	
	protected CollisionManagerData collisionManagerData;
	
	protected HashSetSpawnArea spawnAreas = new HashSetSpawnArea();
	
	protected List< MissionObjectiveData > missionObjectives;
	
	private List< GameObject > sceneObjects = new List< GameObject >();

	protected HashSetString sceneMeshes = new HashSetString();
	
	protected HashSetString sceneMaterials = new HashSetString();
	
	protected Ground groundObject;
	
	protected AstarPath pathfinder;
	public RecastGraph aStarGraph;
	
	protected StakeGenerator stakeHolder = new StakeGenerator();
	
	
	[ MenuItem( MENU_ITEM ) ]
	public static void Execute() {
		try {			
			MissionGenerator missionGenerator = ( MissionGenerator ) Object.FindObjectOfType( typeof( MissionGenerator ) );
			missionGenerator.groundObject = GameObject.FindObjectOfType( typeof( Ground ) ) as Ground;
			
			missionGenerator.pathfinder = Object.FindObjectOfType( typeof( AstarPath ) ) as AstarPath;
			if( missionGenerator.pathfinder == null  ) {
				CreatePathfinder( missionGenerator );
			}
			
			if ( missionGenerator == null ) {
				EditorUtility.DisplayDialog( "Mission Editor", "A cena deve ter um script " + typeof( MissionGenerator ) + " associado a um de seus objetos.", "Ok");
			} else {
				if ( missionGenerator.groundObject != null ) {
					missionGenerator.Start();
					missionGenerator.ExportToFile();
					EditorUtility.DisplayDialog( "Mission Editor", "Missão \"" + missionGenerator.missionName + "\" exportada com sucesso. " + 
						missionGenerator.sceneObjects.Count + " objetos, " + 
						missionGenerator.sceneMeshes.Count + " malhas e " + 
						missionGenerator.sceneMaterials.Count + " materiais da cena foram exportados.", "Ok" );
				} else {
					EditorUtility.DisplayDialog( "Mission Editor", "Erro ao exportar missão: " + "Missão não possui um terreno!", "Ok");	
				}
			}
			
		} catch ( Exception e ) {
			Debugger.LogError( e.ToString() );
			EditorUtility.DisplayDialog( "Mission Editor", "Erro ao exportar missão: " + e.Message, "Ok");
		}
	}
	
	
	/// <summary>
	/// É importante (re)inicializar os atributos aqui pois o script é instanciado uma só vez no editor, porém
	/// a cada execução o método Start é chamado. Sem isso, listas ficam cheias de valores antigos, por exemplo.
	/// </summary>
	public void Start() {
		missionObjectives = new List< MissionObjectiveData >();
		sceneMaterials = new HashSetString();
		sceneObjects = new List< GameObject >();
		sceneName = "";		
	}
	
	
	// Validate the menu item.
    // The item will be disabled if no transform is selected.
	/// <summary>
	/// Valida o item de menu para exportar missão, deixando-o desabilitado caso não haja um objeto com a tag "Mission"
	/// na cena.
	/// </summary>
	/// <returns>
	/// Um <see cref="System.Boolean"/> indicando se o item de menu pode ser utilizado ou não.
	/// </returns>
    [ MenuItem ( MENU_ITEM, true ) ]
    public static bool ValidateExportMission() {
		return Object.FindObjectOfType( typeof( MissionGenerator ) ) != null;
    }
	
	
	public void ExportToFile() {
		ValidateMissionName();
		
		LoadSpawnAreas();
		CheckForSpawnAreas();
		
		LoadMissionObjectives();
		
		GenerateAssetBundles();	
		
		MissionData missionData = new MissionData();
		foreach( SpawnArea sa in spawnAreas.Keys ) {
			missionData.spawnAreasData.Add( sa.GetMissionData(), 0 );
		}
		
		missionData.endMissionConditions = missionObjectives;
		
		missionData.name = missionName;
		missionData.levelOwner = levelOwner;
		missionData.objectsBundlePath = GetSceneBundlePath( false );
		missionData.materialsAssetBundles = sceneMaterials;
		missionData.meshesAssetBundles = sceneMeshes;
		
		GenerateCollisionManager();
		GenerateAStarGraph();
		GenerateLevelFile();
		
		missionData.ExportToFile();
	}
	
	 
	protected void LoadSpawnAreas() {
		SpawnArea[] spawnAreasArray = GameObject.FindObjectsOfType( typeof( SpawnArea ) ) as SpawnArea[];
		spawnAreas = new HashSetSpawnArea();
		
		foreach( SpawnArea sa in spawnAreasArray ) {
			spawnAreas.Add( sa, 0 );
		}
	}
	
	
	protected void LoadMissionObjectives() {
		GameMode gameMode = GetMissionGameMode();
		
		foreach( MissionObjectiveData missionObjectiveData in gameMode.GetMissionObjectivesData() ) {
			missionObjectives.Add( missionObjectiveData );
		}
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	protected void GenerateAssetBundles() {
		Directory.CreateDirectory( AssetbundlePath );
		
		sceneName = EditorApplication.currentScene;
		sceneName = sceneName.Replace( ".unity", "" );
		sceneName = sceneName.Substring( sceneName.LastIndexOf( '/' ) + 1 );
		
		string path = AssetbundlePath + "/" + sceneName;
		FileUtil.DeleteFileOrDirectory( path );
		Directory.CreateDirectory( path );
		
		GameObject rootFolder = ControllerUtils.GetHolder( "_root" );
		GameObject combinedFolder = rootFolder;
		
		List< GameObject > all = new List< GameObject >();
		List< GameObject > filteredObjects = new List< GameObject >();
		
		foreach ( GameObject obj in GameObject.FindObjectsOfType( typeof( GameObject ) ) ) {			
			if ( obj.GetComponent< Light >() != null ) {
				filteredObjects.Add( obj );
			} else {
				if ( obj.transform.parent != null || obj.tag.Contains( Common.Tags.MISSION ) ||
					 obj.name.Contains( "@" ) || obj.tag.Equals( "MainCamera" ) || obj == rootFolder ) {
					
					continue;
				}
					
				all.Add( obj );
			}
		}
		
		
		foreach ( GameObject o in MeshCombiner.EnumerateCombinedMeshes( all, combinedFolder ) ) {
		}
		
		filteredObjects.Add( combinedFolder );
		
		Object rootPrefab = null;
		try {
			foreach ( GameObject obj in filteredObjects )
				AddGameObject( obj, false );
			
			foreach ( GameObject obj in sceneObjects ) {
				if ( obj.transform.parent == null ) {
					if ( !obj.name.Equals( "_root" ) )
						obj.transform.parent = rootFolder.transform;
				} else {
					Debugger.LogWarning( "Non-root scene object: " + obj );
				}
			}
			rootPrefab = GetPrefab( rootFolder, rootFolder.name );
			
			BuildPipeline.BuildAssetBundle( rootPrefab, null, GetSceneBundlePath( true ), BuildAssetBundleOptions.CollectDependencies );
		} catch ( Exception e ) {
			Debugger.LogError( e );
		} finally {
			foreach ( Object o in sceneObjects )
				Object.DestroyImmediate( o );
			
			
			Object.DestroyImmediate( rootFolder );
			Object.DestroyImmediate( combinedFolder );
			
			// é necessário remover o prefav após ele ter sido adicionado ao bundle da fase
			AssetDatabase.DeleteAsset( AssetDatabase.GetAssetPath( rootPrefab ) );
		}
    }
	
	
	/// <summary>
	/// Obtém o caminho do bundle com as informações dos objetos da cena.
	/// </summary>
	private string GetSceneBundlePath( bool fullPath ) {
		return ( fullPath ? AssetbundlePath : "assetbundles/" ) + sceneName + "/objects.assetbundle";		
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	private void AddGameObject( GameObject obj, bool isChild ) {
//		if ( obj.tag.Contains( Common.Tags.MISSION ) )
//			return;
				
		PrefabDescriptor prefabDescriptor = ( PrefabDescriptor ) obj.GetComponent( typeof( PrefabDescriptor ) );
		if ( prefabDescriptor == null ) {
			if ( isChild ) {
				AddBundleToList( obj, true, false );
			} else {
				GameObject assetClone = ( GameObject ) Object.Instantiate( obj );
				assetClone.name = Util.GetCleanFilename( obj.name );
				AddBundleToList( assetClone, true, true );
			}
		} else {
			// o objeto foi gerado a partir de um prefab, logo as informações de malhas e materiais já foram processadas
			if ( isChild ) {
				CleanObject( obj );
			} else {
				GameObject copy = GetCleanCopy( obj );
				copy.AddComponent( typeof( PrefabDescriptor ) );
				PrefabDescriptor d = ( PrefabDescriptor ) copy.GetComponent( typeof( PrefabDescriptor ) );
				d.CopyFrom( prefabDescriptor );
				
				AddBundleToList( copy, false, true );
			} 
			
			foreach ( PartMap m in prefabDescriptor.data.materials ) {
				foreach ( string s in m.names )
					sceneMaterials.Add( s, 0 );
//				sceneMaterials.AddRange( m.names );
			}
			
			foreach ( PartMap m in prefabDescriptor.data.meshes ) {
				foreach ( string s in m.names )
					sceneMeshes.Add( s, 0 );
//				sceneMeshes.AddRange( m.names );
			}
		}
	}
	
	
	/// <summary>
	/// Removes the unused scripts.
	/// </summary>
	/// <param name='copy'>
	/// Copy.
	/// </param>
	private void RemoveUnusedScripts( GameObject copy ) {
		List< Component > toRemove = new List< Component >();
		toRemove.AddRange( copy.GetComponentsInChildren< ObjectSnapper >( true ) );
		toRemove.AddRange( copy.GetComponentsInChildren< Tiler >( true ) );
		toRemove.AddRange( copy.GetComponentsInChildren< HollowDescriptor >( true ) );
		
		foreach ( Component c in toRemove )
			Object.DestroyImmediate( c );
	}
	
	
	/// <summary>
	/// Determines whether this instance is a holder.
	/// </summary>
	/// <returns>
	/// <c>true</c> if this instance is holder the specified o; otherwise, <c>false</c>.
	/// </returns>
	/// <param name='o'>
	/// If set to <c>true</c> o.
	/// </param>
	private bool IsHolder( GameObject o ) {
		return o.tag.Equals( Common.Tags.HOLDER ) || ( o.transform.childCount > 0 && 
				   o.GetComponent< Renderer >() == null && 
				   o.GetComponent< Light >() == null && 
				   o.GetComponent< Camera >() == null && 
				   o.GetComponent< Animation >() == null );
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="assetClone">
	/// A <see cref="GameObject"/>
	/// </param>
	/// <param name="generateChildrenBundles">
	/// A <see cref="System.Boolean"/>
	/// </param>
	private void AddBundleToList( GameObject gameObject, bool addDescriptor, bool addToList ) {
		// se o objeto for somente um contêiner de outros objetos, verifica se seus filhos tem descritores, se são pastas ou objetos comuns
		if ( IsHolder( gameObject ) ) {
			Transform[] children = gameObject.GetComponentsInChildren< Transform >();
			
			foreach ( Transform child in children ) {
				// evita inserir o próprio transform, e também os netos, bisnetos, etc.
				if ( child.gameObject != gameObject )
					AddGameObject( child.gameObject, true );
			}
		}
		
		RemoveUnusedScripts( gameObject );
		
		if ( addToList )
			sceneObjects.Add( gameObject );
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="go">
	/// A <see cref="GameObject"/>
	/// </param>
	/// <returns>
	/// A <see cref="GameObject"/>
	/// </returns>
	public static GameObject GetCleanCopy( GameObject go ) {
		GameObject copy = ( GameObject ) GameObject.Instantiate( go );
		copy.name = go.name;
		
		CleanObject( copy );
		
		return copy;
	}
	
	
	public static void CleanObject( GameObject obj ) {
		foreach ( Renderer renderer in obj.GetComponentsInChildren( typeof ( Renderer ) ) ) {
			renderer.materials = new Material[ 0 ];
		}
		
		foreach ( MeshFilter meshFilter in obj.GetComponentsInChildren( typeof ( MeshFilter ) ) ) {
			meshFilter.mesh = null;
		}
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="go">
	/// A <see cref="GameObject"/>
	/// </param>
	/// <param name="name">
	/// A <see cref="System.String"/>
	/// </param>
	/// <returns>
	/// A <see cref="Object"/>
	/// </returns>
    public static Object GetPrefab( GameObject go, string name ) {
        Object tempPrefab = PrefabUtility.CreateEmptyPrefab( "Assets/" + name + ".prefab" );
        tempPrefab = PrefabUtility.ReplacePrefab( go, tempPrefab );
		tempPrefab.name = go.name;
		
        return tempPrefab;
    }	
			
			
	private string GetBundleName( GameObject go ) {
		return go.name + "_" + go.GetInstanceID();
	}
			
			
	private string GetBundlePath( string bundleName ) {
		return AssetbundlePath + sceneName + "/" + bundleName + ".assetbundle";
	}
	
	
    protected string AssetbundlePath {
        get {
			return Common.Paths.SERVER + "/assetbundles" + System.IO.Path.DirectorySeparatorChar; 
		}
    }
	
	
	protected void ValidateMissionName() {
		if ( missionName == null || missionName.Trim().Length <= 0 ) {
			throw new UnnamedMissionException();
		}
	}	
	
	
	protected GameMode GetMissionGameMode() {
		GameMode[] gameModes = Object.FindObjectsOfType( typeof( GameMode ) ) as GameMode[];
		
		if( gameModes.Length != 1 ) {
			throw new InvalidNumberOfGameModesException();
		} else {
			return gameModes[ 0 ];
		}
	}
		
	
	protected void GenerateCollisionManager() {
		collisionManagerData = new CollisionManagerData();
		GameObject[] terrainElements = GameObject.FindGameObjectsWithTag( Common.Tags.TERRAIN );
		
		List< HollowDescriptor > portalDescriptors = new List< HollowDescriptor >();
		
		
		// adiciona primeiro as caixas que compõem o terreno (chão) do mapa
		if( !groundObject.GenerateGroundBoxes() ) {
			Debugger.LogError( "Aconteceu um problema durante a geracao das caixas do chao. Sua missao pode estar incompleta. ");
			return;
		}		
		foreach ( GroundBox groundBox in groundObject.GetGroundBoxes() )
			collisionManagerData.AddBox( groundBox, false );		
		
		for ( int c = 0; c < terrainElements.Length; ++c ) {
			GameObject go = terrainElements[ c ];
			
			if ( go.collider != null && go.collider is BoxCollider ) {
				BoxCollider boxCollider = go.collider as BoxCollider;
				
				GameTransform boxTransform = new GameTransform();
				boxTransform.position = ControllerUtils.CreateGameVector( go.transform.position );
				
				GameVector size = new GameVector( boxCollider.size.x * go.transform.localScale.x, boxCollider.size.y * go.transform.localScale.y, boxCollider.size.z * go.transform.localScale.z );
				boxTransform.scale = ControllerUtils.RotateCollisionBoxSize( ControllerUtils.CreateGameVector( go.transform.eulerAngles ), size );
				
				HollowDescriptor p = go.GetComponent< HollowDescriptor >();
				if ( p == null ) {
					Box box = new Box( boxTransform );
					collisionManagerData.AddBox( box, true );
				} else {
					HollowBox box = new HollowBox( boxTransform );
					foreach( Box holeBox in p.GenerateHoleBoxes() )
						box.AddHoleBox( holeBox );
					collisionManagerData.AddBox( box, true );
					
					portalDescriptors.Add( p );
				}
			} 
		}
		
		// gerando lista de setores da missão
		List< Box > groundBoxes = new List< Box >();
		foreach ( Box b in groundObject.GetGroundBoxes() ) {
			groundBoxes.Add( b );
		}
		collisionManagerData.SectorsData.GenerateSectors( groundBoxes );
		
		// gerando lista de portais da missão, removemos as estacas dentro delas
		foreach ( HollowDescriptor p in portalDescriptors ) {
			Portal portal = p.Generate();
			if( portal != null )
				StakeGenerator.AddPortal( portal );
		}
	}
	
	
	protected void GenerateAStarGraph() {
		AstarPath astar = GameObject.Find( "A*" ).GetComponent< AstarPath >();
		byte[] rawAstarData = astar.astarData.data;
		
		if( rawAstarData == null )
			Debugger.LogWarning( "MissionGenerator: AstarData não foi serializado corretamente." );
		
		collisionManagerData.RawAStarData = rawAstarData;
	}
	
	
	protected void GenerateLevelFile() {
		MapCollisionData collisionData = new MapCollisionData( Util.GetCleanFilename( missionName ), collisionManagerData.Boxes, collisionManagerData.SectorsData, collisionManagerData.RawAStarData );
		collisionData.ExportToFile();
	}

	
	public override bool Equals( object obj ) {
		return Equals( obj as MissionGenerator );
	}
	
	
	public bool Equals( MissionGenerator mg ) {
		return mg != null && missionName.Equals( mg.missionName ) && spawnAreas.Equals( mg.spawnAreas );
	}
	
	
	public override int GetHashCode() {
		return base.GetHashCode();
	}
	
	
	protected void CheckForSpawnAreas() {
		if ( spawnAreas.Count == 0 ) {
			throw new SpawnAreasMissingException();
		}
		
		bool policeFactionHasSpawnArea = false;
		bool criminalFactionHasSpawnArea = false;
		
		foreach ( SpawnArea spawnArea in spawnAreas.Keys ) {
			if ( spawnArea.GetFaction() == Faction.POLICE ) {
				policeFactionHasSpawnArea = true;
			} else if ( spawnArea.GetFaction() == Faction.CRIMINAL ) {
				criminalFactionHasSpawnArea = true;
			}
		}
		
		if ( !policeFactionHasSpawnArea ) {
			throw new SpawnAreasMissingException( "Não existe nenhuma Spawn Area para a faccao dos policiais." );
		}
		
		if ( !criminalFactionHasSpawnArea ) {
			throw new SpawnAreasMissingException( "Não existe nenhuma Spawn Area para a faccao dos traficantes." );
		}
	}
	
	protected static void CreatePathfinder( MissionGenerator missionGenerator ) {
		
		// criando pathfinder
		GameObject aStarObject = new GameObject("A*");
		missionGenerator.pathfinder = aStarObject.AddComponent< AstarPath >();
		missionGenerator.pathfinder.Awake();
		
		// configurando grafo do tipo recast
		missionGenerator.aStarGraph = missionGenerator.pathfinder.astarData.AddGraph( typeof( RecastGraph ) ) as RecastGraph;
		missionGenerator.aStarGraph.useCRecast = false;
		missionGenerator.aStarGraph.cellSize = 0.2f;
		missionGenerator.aStarGraph.cellSize = 0.8f;
		missionGenerator.aStarGraph.walkableHeight = 1.0f;
		missionGenerator.aStarGraph.walkableClimb = 0.5f;
		missionGenerator.aStarGraph.characterRadius = 0.1f;
		missionGenerator.aStarGraph.maxSlope = 46.0f;
		missionGenerator.aStarGraph.maxEdgeLength = 10.0f;
		missionGenerator.aStarGraph.contourMaxError = 0.001f;
		missionGenerator.aStarGraph.regionMinSize = 40;
		
		missionGenerator.aStarGraph.SnapForceBoundsToScene();
	}
}
	
//	#region Mission Generator Test Case
//	internal class MissionGeneratorTestCase : TestCase {
//	
//		protected const string MISSION_NAME = "mission_generator_test";
//		
//		protected GameObject mission;
//		protected GameObject policeArea;
//		protected GameObject criminalArea;
//		
//		protected UnityEngine.Object spawnAreaBOPEPrefab = Resources.Load( "Spawn Area BOPE" );
//		protected UnityEngine.Object spawnAreaNPCPrefab = Resources.Load( "Spawn Area NPC" );
//		protected UnityEngine.Object spawnAreaDrugPrefab = Resources.Load( "Spawn Area Drug" );
//		
//		
//		protected MissionGenerator missionGenerator;
//		
//		
//		public override void SetUp() {
//			mission = (GameObject) GameObject.Instantiate( Resources.Load( "Mission Generator Object" ) );
//			missionGenerator = mission.GetComponent< MissionGenerator >();
//		}
//	
//		public override void TearDown() {
//			GameObject.Destroy( mission );
//		}
//		
//		
//		[UnitTest]
//		public void MissionObjectivesMissing() {
//			
//			Assert.ExpectException( new NoMissionObjectivesException() );
//			missionGenerator.ExportToFile();
//		}
//		
//		[UnitTest]
//		public void SpawnAreasMissing() {
//
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			Assert.ExpectException( new SpawnAreasMissingException() );
//			missionGenerator.ExportToFile();
//		}
//		
//		[UnitTest]
//		public void NoSpawnAreasForOneOfTheFactions() {
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			policeArea = GameObject.Instantiate( spawnAreaBOPEPrefab ) as GameObject;
//			
//			Assert.ExpectException( new SpawnAreasMissingException() );
//			try {
//				missionGenerator.ExportToFile();
//			} catch( Exception e ) {
//				throw e;
//			} finally {
//				Destroy( policeArea );
//			}
//		}
//		
//		[UnitTest]
//		public void NamelessMission() {
//			Assert.ExpectException( new UnnamedMissionException() );
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			CreateAndExportMission();
//		}		
//		
//		[UnitTest]
//		public void MissionWithoutMapName() {
//			Assert.ExpectException( new UnnamedMapException() );
//			
//			missionGenerator.missionName = MISSION_NAME;
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			CreateAndExportMission();
//		}
//		
//		[UnitTest]
//		public void MissionWithoutSceneName() {
//			Assert.ExpectException( new UnnamedSceneException() );
//			
//			missionGenerator.missionName = MISSION_NAME;
//			missionGenerator.mapName = MISSION_NAME;
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			CreateAndExportMission();
//		}		
//		
//		[UnitTest]
//		public void CorrectMissionGeneration() {
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			missionGenerator.missionName = MISSION_NAME;
//			missionGenerator.mapName = MISSION_NAME;
//			missionGenerator.sceneName = "Test Scene";
//			CreateAndExportMission();
//			
//			Assert.True( File.Exists( missionGenerator.GetMissionFilePath() ), "O arquivo de missão não foi gerado corretamente." );
//			Assert.True( File.Exists( missionGenerator.GetLevelFilePath() ), "O arquivo de level não foi gerado corretamente." );
//		}		
//		
//		protected void CreateAndExportMission() {
//			AddingBothFactionAreas();
//			try {
//				missionGenerator.ExportToFile();
//			} catch( Exception e ) {
//				throw e;	
//			} finally {
//				CleaningUpFactionAreas();
//			}
//		}
//
//		protected void AddingBothFactionAreas() {
//			policeArea = GameObject.Instantiate( spawnAreaBOPEPrefab ) as GameObject;
//			criminalArea = GameObject.Instantiate( spawnAreaDrugPrefab ) as GameObject;
//		}
//		
//		protected void CleaningUpFactionAreas() {
//			Destroy( policeArea );
//			Destroy( criminalArea );
//		}
//	}
//	#endregion
//}
//

#region Excecoes
public class SpawnAreasMissingException : Exception {
	
	public SpawnAreasMissingException() {
	}
	
	public SpawnAreasMissingException( string message ) : base( message ) {
	}
	
}

public class InvalidNumberOfGameModesException : Exception {
	
	public InvalidNumberOfGameModesException() : this( "Sua deve ter um (e apenas um) modo de jogo!" ) {
	}
	
	
	public InvalidNumberOfGameModesException( string message ) : base( message ) {
	}
	
}

public class UnnamedMissionException : Exception {
	
	public UnnamedMissionException() : this( "Nome inválido da missão." ) {
	}
	
	
	public UnnamedMissionException( string message ) : base( message ) {
	}
	
}

public class UnnamedMapException : Exception {
	
	public UnnamedMapException() : this( "Nome do mapa inválido." ) {
	}
	
	
	public UnnamedMapException( string message ) : base( message ) {
	}
	
}

public class UnnamedSceneException : Exception {
	
	public UnnamedSceneException() : this( "Nome da cena inválido." ) {
	}
	
	
	public UnnamedSceneException( string message ) : base( message ) {
	}
	
}
#endregion

#endif
