using UnityEngine;

using System.Collections.Generic;

using GameCommunication;


public abstract class GameMode : MonoBehaviour {
	
	public int maxNumberOfTurns;
	
	
	public IEnumerable< MissionObjectiveData > GetMissionObjectivesData() {
		yield return new DestroyAllEnemiesData( maxNumberOfTurns );
		
		foreach( MissionObjectiveData objectiveData in GetGameModeMissionObjectivesData() ) {
			yield return objectiveData;
		}
	}
	
	
	public virtual IEnumerable< MissionObjectiveData > GetGameModeMissionObjectivesData() {
		yield break;
	}
	
}
