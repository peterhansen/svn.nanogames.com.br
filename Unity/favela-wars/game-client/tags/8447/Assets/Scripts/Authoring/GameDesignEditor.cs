#if UNITY_EDITOR
using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

using GameCommunication;
using Utils;
using SharpUnit;

using Attribute = GameCommunication.Attribute;


/// <summary>
/// Esta classe permite a edição em tempo de execução dos parâmetros gerais de game design, como velocidades máximas
/// e mínimas dos personagens, consumo de AP para cada tipo de ação, etc.
/// Para adicionar novos campos ao editor, basta criar as propriedades get/set correspondentes, com classes que herdam
/// de Editable.
/// </summary>
public class GameDesignEditor : GenericEditor {
	
	private GameDesignData gameDesignData = new GameDesignData();
	
	
	public GameDesignEditor() {
		data = gameDesignData;
	}
	
	
	public override string GetTitle() {
		return "Game Design Editor";
	}
	
	
	[ ToolTip( "Atributos editáveis de game design do Favela Wars." ) ]
	public GameDesignData GameDesignData {
		get { return gameDesignData; }
		set { gameDesignData = value; }
	}	
	
}


/// <summary>
/// Classe que encapsula os dados usados pelo editor de game design - necessária pois o editor não pode ser serializável,
/// já que herda de MonoBehaviour.
/// </summary>
[ Serializable ]
public class GameDesignData : Editable {
	
	private CharacterAttributes characterAttributes = new CharacterAttributes();
	
	private MoveAttributes moveAttributes = new MoveAttributes();
	
	private VisibilityAttributes visibilityAttributes = new VisibilityAttributes();
	
	private ThrowAttributes throwAttributes = new ThrowAttributes();
	
	private AttackAttributes attackAttributes = new AttackAttributes();
	
	private ItemAttributes itemAttributes = new ItemAttributes();
	
	private EvolutionAttributes evolutionAttributes = new EvolutionAttributes();
	
	public GameDesignData() {
	}
	
	
	public GameDesignData( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Atributos comuns a todos os personagens, como faixas de valores válidos para atributos, movimentação e ataque." ) ]
	public CharacterAttributes CharacterAttributes {
		get { return characterAttributes; }
		set { characterAttributes = value; }
	}
	
	
	[ ToolTip( "Atributos usados na movimentação de personagens." ) ]
	public MoveAttributes MoveAttributes {
		get { return moveAttributes; }
		set { moveAttributes = value; }
	}
	
	
	[ ToolTip( "Atributos usados para cálculo de visibilidade dos personagens." ) ]
	public VisibilityAttributes VisibilityAttributes {
		get { return visibilityAttributes; }
		set { visibilityAttributes = value; }
	}
	
	
	[ ToolTip( "Atributos referentes à ação de arremesso de um objeto por um personagem." ) ]
	public ThrowAttributes ThrowAttributes {
		get { return throwAttributes; }
		set { throwAttributes = value; }
	}
	
	
	[ ToolTip( "Atributos referentes às ações de ataque dos personagens." ) ]
	public AttackAttributes AttackAttributes {
		get { return attackAttributes; }
		set { attackAttributes = value; }
	}

	
	[ ToolTip( "Atributos referentes ao uso de itens." ) ]
	public ItemAttributes ItemAttributes {
		get { return itemAttributes; }
		set { itemAttributes = value; }
	}
	
	[ ToolTip( "Atributos referentes a evolucao do personagem." ) ]
	public EvolutionAttributes EvolutionAttributes {
		get { return evolutionAttributes; }
		set { evolutionAttributes = value; }
	}
	
}


#region MoveAttributes

[ Serializable ]
public class MoveAttributes : Editable {
	
	
	public MoveAttributes() {
	}
		
	
	public MoveAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Custo inicial de AP para um personagem mover-se (como uma \"bandeirada\" de um táxi). Este valor deve ser maior ou igual a zero." ) ]
	public float InitialAPCost {
		get { return Common.CharacterInfo.INITIAL_AP_COST; }
		set { Common.CharacterInfo.INITIAL_AP_COST = Math.Max( value, 0 ); }
	}
	
	
	[ ToolTip( "Custo mínimo de AP para um soldado mover-se um metro. O custo mínimo é relativo a um soldado com o máximo de agilidade." ) ]
	public float MinAPCostPerMeter {
		get { return Common.CharacterInfo.MIN_AP_PER_METER; }
		set { Common.CharacterInfo.MIN_AP_PER_METER = value; }
	}
	
	
	[ ToolTip( "Custo máximo de AP para um soldado mover-se um metro. O custo máximo é relativo a um soldado com o mínimo de agilidade." ) ]
	public float MaxAPCostPerMeter {
		get { return Common.CharacterInfo.MAX_AP_PER_METER; }
		set { Common.CharacterInfo.MAX_AP_PER_METER = value; }
	}
	
	
	[ ToolTip( "Velocidade de movimentação de um soldado em pé, em metros por segundo. Este atributo é somente visual, não tendo relação com o consumo de AP do soldado." ) ]
	public Attribute StandingMovementSpeed {
		get { return new Attribute( 0, ( int ) PathFollowerScript.MIN_STANDING_MOVE_SPEED, ( int ) PathFollowerScript.MAX_STANDING_MOVE_SPEED ); }
		set { 
			PathFollowerScript.MIN_STANDING_MOVE_SPEED = value.Min;
			PathFollowerScript.MIN_STANDING_MOVE_SPEED = value.Max;
		}
	}

	
	[ ToolTip( "Velocidade de movimentação de um soldado agachado, em metros por segundo. Este atributo é somente visual, não tendo relação com o consumo de AP do soldado." ) ]
	public Attribute CrouchingMovementSpeed {
		get { return new Attribute( 0, ( int ) PathFollowerScript.MIN_CROUCH_MOVE_SPEED, ( int ) PathFollowerScript.MAX_CROUCH_MOVE_SPEED ); }
		set { 
			PathFollowerScript.MIN_CROUCH_MOVE_SPEED = value.Min;
			PathFollowerScript.MAX_CROUCH_MOVE_SPEED = value.Max;
		}
	}

}

#endregion


#region CharacterAttributes

[ Serializable ]
public class CharacterAttributes : Editable {
	
	public CharacterAttributes() {
	}
	
	
	public CharacterAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Limites para pontos de ataque de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute Attack {
		get { return Nature.AttackDefault; }
		set { Nature.AttackDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para pontos de defesa de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute Defense {
		get { return Nature.DefenseDefault; }
		set { Nature.DefenseDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para agilidade de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute Agility {
		get { return Nature.AgilityDefault; }
		set { Nature.AgilityDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para técnica de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute Tecnique {
		get { return Nature.TechniqueDefault; }
		set { Nature.TechniqueDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para inteligência de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute Intelligence {
		get { return Nature.IntelligenceDefault; }
		set { Nature.IntelligenceDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para percepção de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute Perception {
		get { return Nature.PerceptionDefault; }
		set { Nature.PerceptionDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para pontos de ação de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute ActionPoints {
		get { return Nature.ActionPointsDefault; }
		set { Nature.ActionPointsDefault.Set( value ); }
	}

	
	[ ToolTip( "Limites para pontos de energia de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute HitPoints {
		get { return Nature.HitPointsDefault; }
		set { Nature.HitPointsDefault.Set( value ); }
	}
	
	
	[ ToolTip( "Limites para pontos de moral de personagens. Informações detalhadas no Favela Wiki." ) ]
	public Attribute MoralPoints {
		get { return Nature.MoralPointsDefault; }
		set { Nature.MoralPointsDefault.Set( value ); }
	}

}

#endregion


#region VisibilityAttributes

[ Serializable ]
public class VisibilityAttributes : Editable {
	
	public VisibilityAttributes() {
	}
	
	
	public VisibilityAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Ângulo de abertura do campo de visão de um soldado." ) ]
	public float ViewAngle {
		get { return ( float ) ( Common.CharacterInfo.CHARACTER_FOV_RAD / ( Math.PI / 180.0 ) ); }
		set { Common.CharacterInfo.CHARACTER_FOV_RAD = ( float ) ( NanoMath.Clamp( value, 0, 360 ) * ( Math.PI / 180.0 ) ); }
	}
	
	
	[ ToolTip( "Distância máxima do campo de visão de um soldado, em metros." ) ]
	public float ViewDistance {
		get { return Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT; }
		set { Common.CharacterInfo.CHARACTER_H_VIEW_LIMIT = NanoMath.Clamp( value, 0, 200 ); }
	}
	
		
	[ ToolTip( "Frequência de passos na qual o personagem recalcula sua visibilidade." ) ]
	public int VisibilityFrequency {
		get { return Common.CharacterInfo.VISIBILITY_FREQUENCY; }
		set { Common.CharacterInfo.VISIBILITY_FREQUENCY = NanoMath.Clamp( value, 1, 10 ); }
	}
	
	
	[ ToolTip( "O quanto a percepcao do personagem ajuda na realizacao de contra-ataques." ) ]
	public float CounterChanceBonus {
		get { return Common.CharacterInfo.COUNTERATTACK_PERCEPTION_INFLUENCE; }
		set { Common.CharacterInfo.COUNTERATTACK_PERCEPTION_INFLUENCE = NanoMath.Clamp( value, 1.0f, 5.0f ); }
	}
}

#endregion

#region ShootAttributes

[ Serializable ]
public class AttackAttributes : Editable {
	
	public AttackAttributes() {
	}
	
	
	public AttackAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Taxa de conversão da taxa de tiros de uma arma (em tiros por minuto) para consumo de AP.\nA quantidade de AP gasta é dada por (taxa_de_tiros_da_arma / divisor), ou seja, quanto MAIOR o divisor, MENOR o consumo de AP. Os valores válidos são maiores que zero." ) ]
	public float RateOfFireToAPDivider {
		get { return Common.CharacterInfo.RATE_OF_FIRE_TO_AP_DIVIDER; }
		set { Common.CharacterInfo.RATE_OF_FIRE_TO_AP_DIVIDER = Math.Max( value, 0.1f ); }
	}

	
	[ ToolTip( "Taxa de conversão do tempo de recarga de uma arma (em segundos) para consumo de AP. O valor 1 significa que 1 segundo = 1 AP. Os valores válidos são maiores ou iguais a zero." ) ]
	public float ReloadTimeToAPRate {
		get { return Common.CharacterInfo.RELOAD_TIME_TO_AP_RATE; }
		set { Common.CharacterInfo.RELOAD_TIME_TO_AP_RATE = Math.Max( value, 0 ); }
	}
	
	
	[ ToolTip( "Multiplicador do gasto de AP para a velocidade do ataque corpo-a-corpo de uma arma. O gasto de AP é dado por (tempo_do_ataque_da_arma * taxa)." ) ]
	public float MeleeAttackToAPRate {
		get { return Common.CharacterInfo.MELEE_ATTACK_TIME_TO_AP_RATE; }
		set { Common.CharacterInfo.MELEE_ATTACK_TIME_TO_AP_RATE = Math.Max( value, 0 ); }
	}
	
	
	[ ToolTip( "Multiplicador do gasto de AP para a velocidade do arremesso de uma ARMA (não granada) corpo-a-corpo, como facas, shurikens ou outras armas de arremesso. O gasto de AP é dado por (tempo_do_arremesso_da_arma * taxa)." ) ]
	public float MeleeThrowToAPRate {
		get { return Common.CharacterInfo.MELEE_THROW_TIME_TO_AP_RATE; }
		set { Common.CharacterInfo.MELEE_THROW_TIME_TO_AP_RATE = Math.Max( value, 0 ); }
	}	

	
	[ ToolTip( "Multiplicador do dano máximo causado por um tiro em relação ao atributo \"Power\" da arma. 1x significa que 1 Power causa dano de 1HP." ) ]
	public float WeaponPowerToHPRate {
		get { return Common.CharacterInfo.WEAPON_POWER_TO_HP_RATE; }
		set { Common.CharacterInfo.WEAPON_POWER_TO_HP_RATE = Math.Max( value, 0 ); }
	}
	

	[ ToolTip( "Multiplicador do dano máximo causado por uma granada em relação ao atributo \"Power\" da mesma. 1x significa que 1 Power causa dano de 1HP." ) ]
	public float GrenadePowerToHPRate {
		get { return Common.CharacterInfo.GRENADE_POWER_TO_DAMAGE_RATE; }
		set { Common.CharacterInfo.GRENADE_POWER_TO_DAMAGE_RATE = Math.Max( value, 0 ); }
	}
	
	
	[ ToolTip( "Desvio máximo proporcional de um tiro em relação ao centro da mira. O valor é proporcional à distância do alvo ao ponto de disparo. Um valor de 0.5 indica que um disparo a 30m pode ter, no pior caso (precisão ZERO), um desvio de 15m." ) ]
	public float MaxShotDeviatonPercent {
		get { return Common.CharacterInfo.MAX_SHOT_DEVIATION; }
		set { Common.CharacterInfo.MAX_SHOT_DEVIATION = NanoMath.Clamp( value, 0, 1 ); }
	}
	
	
	[ ToolTip( "Bônus na precisão de um disparo caso o soldado esteja agachado. Os valores variam entre 0 e 1 (0% e 100%)" ) ]
	public float KneelingPrecisionBonus {
		get { return Common.CharacterInfo.KNEELING_PRECISION_BONUS; }
		set { Common.CharacterInfo.KNEELING_PRECISION_BONUS = NanoMath.Clamp( value, 0, 1 ); }
	}
	
	
	[ ToolTip( "Percentual base do modificador de precisão em relação ao HP do soldado.\nOs valores válidos ficam na faixa [0,1]. Quanto maior o valor, menor o impacto do HP na precisão de um tiro.\nPor exemplo: um modificador de 0.8 (80%) faz um soldado com metade da energia tenha uma penalidade de 10% ((100% - 80%) * 50%) na precisão de um disparo." ) ]
	public float HealthPrecisionBaseModifier {
		get { return Common.CharacterInfo.HEALTH_PRECISION_BASE_MODIFIER; }
		set { Common.CharacterInfo.HEALTH_PRECISION_BASE_MODIFIER = NanoMath.Clamp( value, 0, 1 ); }
	}
	
	
}

#endregion

#region ThrowAttributes

[ Serializable ]
public class ThrowAttributes : Editable {
	
	public ThrowAttributes() {
	}
	
	
	public ThrowAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Custo de AP (fixo) para a ação de arremesso de um item." ) ]
	public int ThrowActionPointsCost {
		get { return Common.CharacterInfo.THROW_AP_COST; }
		set { Common.CharacterInfo.THROW_AP_COST = Math.Max( 0, value ); }
	}
	
	
	[ ToolTip( "Velocidade mínima de arremesso de um item. Quanto maior o valor, mais longe um item pode ser arremessado." ) ]
	public float MinThrowSpeed {
		get { return Common.CharacterInfo.MIN_THROW_SPEED; }
		set { Common.CharacterInfo.MIN_THROW_SPEED = NanoMath.Clamp( value, 0, MaxThrowSpeed ); }
	}
	
	
	[ ToolTip( "Velocidade máxima de arremesso de um item. Quanto maior o valor, mais longe um item pode ser arremessado." ) ]
	public float MaxThrowSpeed {
		get { return Common.CharacterInfo.MAX_THROW_SPEED; }
		set { Common.CharacterInfo.MAX_THROW_SPEED = Math.Max( value, MinThrowSpeed ); }
	}	
	
	
	[ ToolTip( "Precisão padrão do arremesso de um objeto. Os valores válidos estão na faixa [0,1]." ) ]
	public float ThrowPrecision {
		get { return Common.CharacterInfo.THROW_PRECISION; }
		set { Common.CharacterInfo.THROW_PRECISION = NanoMath.Clamp( value, 0, 1 ); }
	}	
	
	
}

#endregion

#region ItemAttributes

[ Serializable ]
public class ItemAttributes : Editable {
	
	public ItemAttributes() {
	}
	
	
	public ItemAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Custo de AP (fixo) para o uso de um medikit." ) ]
	public float MedikitActionPointsCost {
		get { return Common.CharacterInfo.MEDIKIT_DEFAULT_AP_COST; }
		set { Common.CharacterInfo.MEDIKIT_DEFAULT_AP_COST = value; }
	}
	
	
}

#endregion

#region EvolutionAttributes

[ Serializable ]
public class EvolutionAttributes : Editable {
	
	public EvolutionAttributes() {
	}
	
	
	public EvolutionAttributes( SerializationInfo info, StreamingContext ctxt ) : base( info, ctxt ) {
	}
	
	
	[ ToolTip( "Penalidade por morte de um personagem" ) ]
	public float DeathPenalty {
		get { return Common.CharacterInfo.DEATH_TP_PENALTY; }
		set { Common.CharacterInfo.DEATH_TP_PENALTY = NanoMath.Clamp( value, 0, 1 ); }
	}
	
	[ ToolTip( "Constante que define o quanto de TP um soldado precisa para passar de nível." ) ]
	public float LevelConstant {
		get { return Common.CharacterInfo.LEVEL_CONSTANT; }
		set { Common.CharacterInfo.LEVEL_CONSTANT = Math.Max( 0, value ); }
	}
	
	[ ToolTip( "Bônus que um soldado recebe ao matar outro soldado." ) ]
	public int KillBonus {
		get { return Common.CharacterInfo.TP_BONUS_KILL; }
		set { Common.CharacterInfo.TP_BONUS_KILL = Math.Max( 0, value ); }
	}
	
	[ ToolTip( "TP que um soldado recebe ao dar um tiro e acertar alguém." ) ]
	public int tpShoot {
		get { return Common.CharacterInfo.TP_SHOOT; }
		set { Common.CharacterInfo.TP_SHOOT = Math.Max( 0, value ); }
	}
	
	[ ToolTip( "TP que um soldado recebe ao jogar algo e acertar alguém" ) ]
	public int tpThrow {
		get { return Common.CharacterInfo.TP_THROW; }
		set { Common.CharacterInfo.TP_THROW = Math.Max( 0, value ); }
	}
	
	
}

#endregion


public class GameDesignEditorTestCase : TestCase {
	
	
	public override void SetUp() {
	}
	
	
	[UnitTest]
	public void TestSerialization() {
//		GameObject go = new GameObject();
//		
//		try {
//			CharacterInfoData.ATTACK_DEFAULT_MIN = -9999;
//			
//			GameDesignEditor editor = go.AddComponent< GameDesignEditor >();
//			GameDesignData dataToSave = editor.Data;
//			dataToSave.CharacterAttributes.Defense = new Attribute( -1, -666, 666 );
//			string serialized = Util.SerializeToBase64String( dataToSave );
//			
//			CharacterInfoData.ATTACK_DEFAULT_MIN = 123;
//			
//			GameDesignData loadedData = Util.DeserializeFromBase64String< GameDesignData >( serialized );
//		} finally {
//			GameObject.DestroyImmediate( go );
//		}
	}
	
}
#endif