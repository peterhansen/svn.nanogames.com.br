#if UNITY_EDITOR

using System.Collections;

using UnityEngine;

using GameCommunication;


public class NatureEditor : GenericEditor {
	
	private NatureData natureData = new NatureData();
	
	
	public NatureEditor() {
		data = natureData;
	}
	
	
	[ ToolTip( "Atributos editáveis da natureza de personagens." ) ]
	public NatureData NatureData {
		get { return natureData; }
		set { natureData = value; }
	}	

	
	public override string GetTitle() {
		return "Nature Editor";
	}
	
	
}


public class NatureData : Editable {
	
	[ ToolTip( "Atributos padrão da natureza \"Agressiva\" de personagens." ) ]
	public Nature AgressiveNature {
		get { return Nature.agressiveNature; }
		set { Nature.agressiveNature = value; }
	}
	
	
	[ ToolTip( "Atributos padrão da natureza \"Alerta\" de personagens." ) ]
	public Nature AlertNature {
		get { return Nature.alertNature; }
		set { Nature.alertNature = value; }
	}
	
	
	[ ToolTip( "Atributos padrão da natureza \"Montanha\" de personagens." ) ]
	public Nature BruiserNature {
		get { return Nature.bruiserNature; }
		set { Nature.bruiserNature = value; }
	}


	[ ToolTip( "Atributos padrão da natureza \"Cerebral\" de personagens." ) ]
	public Nature CerebralNature {
		get { return Nature.cerebralNature; }
		set { Nature.cerebralNature = value; }
	}
	
	
	[ ToolTip( "Atributos padrão da natureza \"Generalista\" de personagens." ) ]
	public Nature GeneralistNature {
		get { return Nature.generalistNature; }
		set { Nature.generalistNature = value; }
	}
	
	
	[ ToolTip( "Atributos padrão da natureza \"Velocista\" de personagens." ) ]
	public Nature SpeedsterNature {
		get { return Nature.speedsterNature; }
		set { Nature.speedsterNature = value; }
	}
	
	
	[ ToolTip( "Atributos padrão da natureza \"Objetiva\" de personagens." ) ]
	public Nature StraightforwardNature {
		get { return Nature.straightforwardNature; }
		set { Nature.straightforwardNature = value; }
	}
	
}

#endif