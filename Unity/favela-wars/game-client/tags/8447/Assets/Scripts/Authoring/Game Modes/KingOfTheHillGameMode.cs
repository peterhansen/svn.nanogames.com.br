using System.Collections.Generic;

using UnityEngine;

using GameCommunication;


public class KingOfTheHillGameMode : GameMode {
	
	public Area area;
	
	public int turnsProtectingArea;
	
	
	public override IEnumerable< MissionObjectiveData > GetGameModeMissionObjectivesData() {
		yield return new ProtectAreaData( area.GetMissionData(), turnsProtectingArea );
	}
	
}
