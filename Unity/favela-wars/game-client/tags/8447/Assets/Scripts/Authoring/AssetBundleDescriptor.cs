using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.IO;
using System;

using GameCommunication;
using Utils;


[ Serializable ]
public class AssetBundleDescriptor : ScriptableObject {
	
	[ NonSerializedAttribute ]
	public AssetAssemblyData data = new AssetAssemblyData();
	
	/// <summary>
	/// Dados serializados numa string base 64.
	/// </summary>
	public string serializedData;
	
	
	public void Serialize() {
		serializedData = Util.SerializeToBase64String( data );
	}
	
	
	public AssetAssemblyData Deserialize() {
		return Util.DeserializeFromBase64String< AssetAssemblyData >( serializedData );
	}
	
	
	public void Merge( AssetBundleDescriptor  d ) {
		foreach ( PartMap m in d.data.meshes ) {
			foreach ( string s in m.names ) {
				AddMesh( m.partName, s );
			}
		}
		
		foreach ( PartMap m in d.data.materials ) {
			foreach ( string s in m.names ) {
				AddMaterial( m.partName, s );
			}
		}
		
	}
	
	
	public void AddMesh( string childName, string meshName ) {
		data.AddMesh( childName, meshName );
	}
	
	
	public void AddMaterial( string partName, string materialName ) {
		data.AddMaterial( partName, materialName );
	}
	
	
	public override string ToString() {
		return data.ToString();
	}

}
	
