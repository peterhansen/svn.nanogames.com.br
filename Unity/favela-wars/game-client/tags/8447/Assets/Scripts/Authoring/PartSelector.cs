using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

using GameController;
using Utils;


[ ExecuteInEditMode ]
public class PartSelector : MonoBehaviour {
	
	protected bool oldShowFloor = true;
	protected bool oldShowCorner = true;
	protected bool oldShowEdge1 = true;
	protected bool oldShowEdge2 = true;
	
	public bool showFloor = true;
	public bool showCorner = true;
	public bool showEdge1 = true;
	public bool showEdge2 = true;
	
	
	public void Update() {
		bool changed = false;
		
		if ( showFloor != oldShowFloor ) {
			changed = true;
			oldShowFloor = showFloor;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "chao" )
					childTransform.gameObject.active = showFloor;
		}
		
		if ( showCorner != oldShowCorner ) {
			changed = true;
			oldShowCorner = showCorner;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "chao_quina" )
					childTransform.gameObject.active = showCorner;	
		}
		
		if ( showEdge1 != oldShowEdge1 ) {
			changed = true;
			oldShowEdge1 = showEdge1;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "chao_lateral_01" )
					childTransform.gameObject.active = showEdge1;	
		}
		
		if ( showEdge2 != oldShowEdge2 ) {
			changed = true;
			oldShowEdge2 = showEdge2;
			foreach( Transform childTransform in transform )
				if( childTransform.name == "chao_lateral_02" )
					childTransform.gameObject.active = showEdge2;	
		}
		
		// atualiza o BoxCollider de acordo com as partes ativas
		if ( changed ) {
			BoxCollider c = GetComponent< BoxCollider >();
			if ( c != null ) {
				Bounds b = ControllerUtils.GetBox( gameObject, true );
				c.center = b.center;
				c.size = b.size;
			}
		}
	}
}

#endif