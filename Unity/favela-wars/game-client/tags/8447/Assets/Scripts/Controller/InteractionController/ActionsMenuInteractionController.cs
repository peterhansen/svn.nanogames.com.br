using System;

using UnityEngine;


using GameCommunication;
using Utils;


namespace GameController {
	
	public class ActionMenuInteractionController : ActionInteractionController {
		
		protected ActionsMenuControl actionsMenu;
		
		public ActionMenuInteractionController( GameObject selectedCharacter, GetCharacterActionsData data ) : base( selectedCharacter, null ) {					
			actionsMenu = new ActionsMenuControl( this, selectedCharacter, data.actionTree, data.visibleEnemiesIDs );
			controls.Add( actionsMenu );
			
			SetEnemiesList( data.visibleEnemiesIDs );
			AddEnemyListControl();
		}
		
		public override void Initialize() {
			IsoCameraScript.GetInstance().userInputIsBlocked = true;
			IsoCameraScript.GetInstance().FocusOn( selectedCharacter );
			
		}
		
		public override void OnRemoved() {
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
		}
	}
}
