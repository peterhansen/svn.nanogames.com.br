using System;
using System.Collections.Generic;

using UnityEngine;

using Pathfinding;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class MoveNPCActionRunner : GameActionRunner {
	
		protected const int MAX_FLEE_DISTANCE_TIMES_100 = 6000;
		
		public MoveNPCActionData moveNPCActionData;
		
		protected GameManager gameManager;
		
		protected Dictionary< OnPathIsDoneListener, int > pathListeners = new Dictionary< OnPathIsDoneListener, int >();
		
		public int pathsToBeCalculated;
		
		
		public MoveNPCActionRunner( MoveNPCActionData moveNPCActionData ) : base( false ) {
			gameManager = GameManager.GetInstance();
			this.moveNPCActionData = moveNPCActionData;
			pathsToBeCalculated = moveNPCActionData.npcMovements.Count;
		}
		
		
		protected override void Execute() {
			Debugger.Log( "Calculando caminho dos NPCs" );
			
			if( pathsToBeCalculated == 0 ) {
				OnAllPathsAreCalculated();
				return;
			}
			
			foreach( NPCMovement npcMovement in moveNPCActionData.npcMovements ) {
				GameObject npc = WorldManager.GetInstance().GetGameObject( npcMovement.worldID );
				if( npc == null ) {
					Debugger.Log( "MoveNPCActionRunner tentando mover NPC de id = " + npcMovement.worldID + "." );
					continue;
				}
				
				Seeker seeker = npc.GetComponent< Seeker >();
				if( seeker == null ) {
					Debugger.Log( "NPC de id = " + npcMovement.worldID + " sem um seeker para realizar seu movimento." );
					continue;
				}
					
				OnPathIsDoneListener listener = new OnPathIsDoneListener( this, npcMovement );
				pathListeners[ listener ] = 1;
				
				switch( npcMovement.type ) {
				case NPCMovementType.FOLLOWING:
					seeker.StartPath( npc.transform.position, ControllerUtils.CreateVector3( npcMovement.target ), listener.OnStartPathIsDone );
					break;
					
				case NPCMovementType.FLEEING:
					// TODO: ainda não sei exatamente como fazer
					//path = new Pathfinding.FleePath( npc.transform.position, ControllerUtils.CreateVector3( npcMovement.target ), MAX_FLEE_DISTANCE_TIMES_100, OnStartPathIsDone );
					break;
				}				
			}
		}
		
		
		
		
		protected void OnAllPathsAreCalculated() {
			GameManager.GetInstance().SendRequest( new EndParticipationRequest( moveNPCActionData.playerID ) );
		}
		
		protected class OnPathIsDoneListener {
			
			public MoveNPCActionRunner runner;
			
			public NPCMovement npcMovement;
			
			protected PathProjector projector = new PathProjector();
			
			public OnPathIsDoneListener( MoveNPCActionRunner runner, NPCMovement npcMovement ) {
				this.runner = runner;
				this.npcMovement = npcMovement;
			}
			
			public void OnStartPathIsDone( Pathfinding.Path path ) {
				runner.pathsToBeCalculated--;
				
				if( path.error ) {
					Debugger.Log( "Ocorreu um erro ao calcular o caminho do NPC de id = " + npcMovement.worldID );
				} else {
					// TODO: aqui precisamos tratar o caminho da mesma forma que o tratamos no MoveInteractionController
					List< Vector3 > vectorPath;
					Vector3 finalDirection;
					
					projector.CalculatePathPoints( path, out vectorPath, out finalDirection );
					if( vectorPath != null ) {
						List< GameVector > gameVectorPath = new List<GameVector>();
						foreach( Vector3 node in vectorPath )
							gameVectorPath.Add( ControllerUtils.CreateGameVector( node ) );
					
						// TODO: isso deve ser removido quando a rede estiver ok. Só o jogador certo vai rodar esse runner.
						MoveCharacterRequest request = new MoveCharacterRequest( runner.moveNPCActionData.playerID, npcMovement.worldID, gameVectorPath, ControllerUtils.CreateGameVector( finalDirection ), Stance.STANDING );
						GameManager.GetInstance().SendRequest( request );
					}
				}
				
				if( runner.pathsToBeCalculated == 0 )
					runner.OnAllPathsAreCalculated();
			}
		}
	}
}

