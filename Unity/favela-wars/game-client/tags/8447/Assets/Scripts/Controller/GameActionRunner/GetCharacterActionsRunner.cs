using System;

using UnityEngine;

using System.Collections.Generic;
using System.Collections;

using GameController;
using GameCommunication;
using Utils;
using Action = GameCommunication.GameAction;


namespace GameController {
	
	public class GetCharacterActionsRunner : GameActionRunner {
		
		private GetCharacterActionsData data;
		
		
		public GetCharacterActionsRunner( GetCharacterActionsData getCharacterActionsData ) : base( true ) {
			this.data = getCharacterActionsData;
		}
		
		
		protected override void Execute() {
			GameObject selectedCharacter = WorldManager.GetInstance().GetGameObject( data.characterId );
			GUIManager.SetInteractionController( new ActionMenuInteractionController( selectedCharacter, data ) );
			OnDone();
		}
		
	}
}

