using System;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class MoveInteractionController : ActionInteractionController {
		
		protected class CoverData {
			public Vector3 position;
			public Vector3 normal;
		}
		
		#region Character Data
		protected int characterID;
		
		protected Seeker characterSeeker;

		protected float nearDistance;
		
		protected float farDistance;
		
		#endregion
		
		#region Movement Data
		protected VisibilityMap visibilityMap;
		
		protected List< Vector3 > points = new List< Vector3 >();
		
		protected Vector3 finalDirection;
		
		protected Stance finalStance;
		
		protected Vector3 penultimateTrailPoint;
		
		protected bool pathIsDecided = false;
		
		protected CoverData coverData;
		
		protected PathProjector projector = new PathProjector();
		
		protected float refreshFactor = 1.0f;
		
		protected float timeSinceLastPath = 0.0f;
		
		protected float currentDistance = 0.0f;
		
		protected float accumulatedDistance = 0.0f;
		
		protected MovementTrail trail; 
		#endregion
		
		
		#region Interface Elements
		protected RadialMenuControl radialMenu;
		#endregion
		
	
		public MoveInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			// TODO: singleton?
			visibilityMap = VisibilityMap.GetInstance();
			
			characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			characterSeeker = selectedCharacter.GetComponent< Seeker >();
			if( characterSeeker == null ) {
				Debugger.Log( "Personagem sem um Seeker associado. Esse personagem não poderá se mover na cena." );
				OnDone();
			}
			
			MoveGameAction moveGameAction = action as MoveGameAction;
			if( moveGameAction == null ) {
				Debugger.Log( "Game Action recebida não corresponde ao tipo do Interaction Controller." );
				OnDone();
			}
			nearDistance = moveGameAction.nearDistance;
			farDistance = moveGameAction.farDistance;
			accumulatedDistance = 0.0f;
			
			radialMenu = new RadialMenuControl( new Point( Screen.width / 2, Screen.height / 2 ) );
			radialMenu.Visible = false;
			controls.Add( radialMenu );
						
			trail = new MovementTrail( visibilityMap, GameManager.GetInstance().GetTurnFaction(), nearDistance, farDistance );			
		}
		

		public override void Update() {
			if( Input.GetMouseButtonDown( Common.Mouse.BUTTON_LEFT ) ) { 
				if( !pathIsDecided ) {
					Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
					RaycastHit hitInfo;
						
					if ( Physics.Raycast( ray, out hitInfo ) && 
						 Vector3.Angle( hitInfo.normal, Vector3.up ) < Common.CharacterInfo.MAX_SLOPE &&
						 BuildingInfo.IsWalkable( hitInfo.collider.gameObject ) ) {
	
						trail.SetPosition( hitInfo.point );
						
						// verificando se existe algum objeto que oferece cobertura para o personagem
						CheckForCoverObject( hitInfo.point );
						
						InputEventScene ies = new InputEventScene();
						ies.selectionPosition = hitInfo.point;
						ies.gameObject = hitInfo.collider == null? null: hitInfo.collider.gameObject;
						ReceiveInputEvent( ies );
						
						DecidePath( true );
					} else {
						trail.Active = false;
					}
				}
			}
			
			// TODO: aqui poderíamos empregar um método para corrigir o fim da malha, de forma a ficar sempre colado ao destinationObject.
			// O método abaixo existe, mas deixa bem feio. Talvez possa ser melhorado no futuro.
			// CorrectTrailMesh();
		}
		
		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			if( inputEvent.gameObject == null || inputEvent.gameObject.CompareTag( Common.Tags.CHARACTER ) )
				return;
			
			Vector3 destination = new Vector3( inputEvent.selectionPosition.x, inputEvent.selectionPosition.y, inputEvent.selectionPosition.z );		
			characterSeeker.StartPath( selectedCharacter.transform.position, destination, StartPathCallback );
		}
			
		
		protected void DecidePath( bool pathIsDecided ) {
			this.pathIsDecided = pathIsDecided;
			
			if( pathIsDecided ) {
				radialMenu.AddButton( new RadialMenuOption( RadialMenuButton.RadialMenuIconType.OK, ConfirmMovement ) );
				radialMenu.AddButton( new RadialMenuOption( RadialMenuButton.RadialMenuIconType.CANCEL, CancelMovement ) );
				
				if( coverData != null )
					radialMenu.AddButton( new RadialMenuOption( RadialMenuButton.RadialMenuIconType.TAKE_COVER, ConfirmMovementAndTakeCover ) );
				
				radialMenu.Show();
				IsoCameraScript.GetInstance().userInputIsBlocked = true;
				IsoCameraScript.GetInstance().FocusOn( trail.DestinationObject );
			}
		}
		
		
		protected void CheckForCoverObject( Vector3 position ) {
			const int numberOfRays = 6;
			const float maxCoverDistance = 1.0f;
			
			coverData = null;
			float smallestDistance = float.MaxValue;
			
			for (int i = 0; i < numberOfRays; i++) {
				float angle = i * 360.0f / (float) numberOfRays;
				Vector3 dir = new Vector3( Mathf.Cos( angle ), 0.0f, Mathf.Sin( angle ) );
				Vector3 origin = position + Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT;
				Ray ray = new Ray( origin, dir );
				
				RaycastHit hit;
				if( Physics.Raycast( ray, out hit, maxCoverDistance ) ) {
					if( hit.distance < smallestDistance ) {
						smallestDistance = hit.distance;
						
						coverData = new CoverData();
						coverData.position = hit.point;
						coverData.normal = hit.normal;
					}
				}
			}
		}
		
		
		protected void StartPathCallback( Pathfinding.Path path ) {	
			if( !path.error )
				OnSuccess( path );
			else
				OnFailure();
		}
		
		
		protected void OnSuccess( Pathfinding.Path path ) {
			trail.Active = true;
			trail.SetPosition( path.vectorPath[ path.vectorPath.Length - 1 ] );
			
			projector.CalculatePathPoints( path, out points, out finalDirection );
			
			// desejaremos ter o penúltimo ponto do movimento para recalcular o ponto final
			penultimateTrailPoint = points[ points.Count - 2 ];
			
			currentDistance = accumulatedDistance;
			trail.GenerateTrail( points, ref currentDistance );
			
			// atribuindo Stance final
			MoveGameAction moveGameAction = action as MoveGameAction;
			if( moveGameAction == null )
				Debugger.LogError( "MoveInteractionController recebeu uma GameAction que não é um MoveGameAction" );
			finalStance = moveGameAction.currentStance;
		}
		
//		protected void CorrectTrailMesh() {
//			Vector3[] newVertices = trailMesh.vertices;
//			int n = newVertices.Length;
//			if( n < 2 )
//				return;
//			
//			Vector3 dir = ( destinationObject.transform.position - penultimateTrailPoint ).normalized;
//			
//			Vector3[] finalSideVectors = GetSideVectors( destinationObject.transform.position, dir );
//			newVertices[ n - 2 ] = finalSideVectors[0];
//			newVertices[ n - 1 ] = finalSideVectors[1];
//			trailMesh.vertices = newVertices;
//		}
		
		
		protected void OnFailure() {
			Debugger.Log( "Movimento inválido!" );
			trail.Active = false;
		}
		
		
		
		protected void ConfirmMovement( Control sender ) {
			accumulatedDistance = currentDistance;
			trail.Active = false;
			
			// convertemos caminho em uma lista de gameVectors para enviar para o servidor
			List< GameVector > gameVectorPoints = new List< GameVector >();
			foreach( Vector3 point in points )
				gameVectorPoints.Add( ControllerUtils.CreateGameVector( point ) );
			
			GameManager.GetInstance().SendRequest( new MoveCharacterRequest( GameManager.GetInstance().GetLocalPlayer(), characterID, gameVectorPoints, ControllerUtils.CreateGameVector( finalDirection ), finalStance ) );
			DecidePath( false );
			GUIManager.Reset();
		}
		
		
		protected void ConfirmMovementAndTakeCover( Control sender ) {
			finalStance = Stance.CROUCHING;
			
			for( int i = 0; i < points.Count; i++ ) {
				Vector3 distanceToCover = points[ i ] - ( coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT );
				
//				Debugger.DrawLine( 
//					ControllerUtils.CreateGameVector( points[ i ] ),
//					ControllerUtils.CreateGameVector( coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT ), 
//					0xff0000ff, 
//					10.0f 
//				);
				
				distanceToCover.y = 0.0f;
				
				// estamos aproximando nosso personagem por um cilindro aqui
				if( distanceToCover.magnitude < Common.CharacterInfo.BOX_HALF_WIDTH ) {
					Vector3 currDistanceToWall = Vector3.Project( distanceToCover, coverData.normal );
					Vector3 correction = currDistanceToWall * ( Common.CharacterInfo.BOX_HALF_WIDTH / currDistanceToWall.magnitude );
					correction.y = 0.0f;
					
//					Debugger.DrawLine( 
//						ControllerUtils.CreateGameVector( points[ i ] ),
//						ControllerUtils.CreateGameVector( coverData.position + correction ), 
//						0x00ffffff, 
//						10.0f 
//					);
					points[ i ] = coverData.position - Vector3.up * Common.CharacterInfo.BOX_HALF_HEIGHT + correction;
					
					
				}
			}
			
			// primeiro, vamos calcular a direcao de cobertura, a qual é tangente ao plano de cobertura)
			Vector3 moveDir = points[ points.Count - 1 ] - points[ points.Count - 2 ];
			Vector3 projectedCoverDir = Vector3.Project( moveDir, coverData.normal );
			finalDirection = moveDir - projectedCoverDir;
			finalDirection.y = 0.0f;
			finalDirection.Normalize();
			
			if( NanoMath.Equals( finalDirection.magnitude, 0.0f ) ) {
				finalDirection = coverData.normal;
			}
			
			
			ConfirmMovement( sender );
		}
		
		
		protected void CancelMovement( Control sender ) {
			OnDone();
		}
		
		
		public override void OnDone() {
			trail.Destroy();
			base.OnDone();
		}
			
		public override void OnRemoved() {
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
		}
	}
}