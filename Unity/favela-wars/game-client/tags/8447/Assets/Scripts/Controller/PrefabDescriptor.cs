using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameCommunication;


public class PrefabDescriptor : MonoBehaviour {
	
	public string originalPrefabPath;

	public AssetAssemblyData data = new AssetAssemblyData();
	
	
	public void CopyFrom( PrefabDescriptor other ) {
		originalPrefabPath = other.originalPrefabPath;
		data = other.data;
	}
	
}
	

