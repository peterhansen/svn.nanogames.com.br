using System;

using UnityEngine;

using GameCommunication;
using Utils;


namespace GameController {
	
	public class SelectCharacterInteractionController : InteractionController {
		
		private TurnInfoControl turnInfoControl;
		
		private GameManager gameManager;
		
		private WorldManager worldManager;

		public SelectCharacterInteractionController() : this( false ) {
		}
		
	
		public SelectCharacterInteractionController( bool moveToLastSelectedSoldier ) {
			gameManager = GameManager.GetInstance();
			worldManager = WorldManager.GetInstance();
			
			turnInfoControl = new TurnInfoControl();
			controls.Add( turnInfoControl );
			
			// TODO: desliguei isso durante a elaboração do Radial Menu
			// se for começo de turno, seleciona um soldado do jogador
			// if ( moveToLastSelectedSoldier )
			//	SelectCharacter( gameManager.LastSelectedSoldier );
		}
		
		
		public override void Update() {
			GenerateInputEvent();
		}
		

		public override void ReceiveInputEvent( InputEventScene inputEvent ) {			
			GameObject go = inputEvent.gameObject;
			SelectCharacter( go );
		}		
		
		
		private void SelectCharacter( GameObject character ) {
			gameManager.SendRequest( new GetCharacterActionsRequest( gameManager.GetLocalPlayer(), worldManager.GetWorldID( character ) ) );
		}	
	}
}
