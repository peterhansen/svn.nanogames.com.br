using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Object = UnityEngine.Object;

using GameController;
using GameCommunication;
using Utils;


namespace GameController {
	
	public class ThrowScript : MonoBehaviour {
		
		public delegate void OnDoneMethod();
		
		private float accTime;
		
		private List< ThrowInfo > throws = new List< ThrowInfo >();
		
		private static int count = 0;
		
		private GameActionRunner runner;
		
		private OnDoneMethod onDone;
		
		private ThrowInfo currentThrow;
		
		public float timeMultiplier = 1.0f;
		
		public float timeToDestroyGameObject = -1;
		
		private static Texture2D tex;
		
		
		public static ThrowScript Throw( List< ThrowInfo > throws ) {
			return Throw( throws, null, null, null );
		}
		
		
		public static ThrowScript Throw( List< ThrowInfo > throws, GameObject obj, OnDoneMethod onDone ) {
			return Throw( throws, obj, null, onDone );
		}
		
		
		public static ThrowScript Throw( List< ThrowInfo > throws, GameObject obj, GameActionRunner runner ) {
			return Throw( throws, obj, runner, null );
		}
		
		
		private static ThrowScript Throw( List< ThrowInfo > throws, GameObject obj, GameActionRunner runner, OnDoneMethod onDone ) {
			if ( ( throws == null || throws.Count <= 0 ) && onDone == null )
				return null;
			
			if ( obj == null ) {
				obj = GameObject.CreatePrimitive( PrimitiveType.Sphere );
				obj.name = "Throw #" + count++;
				obj.transform.localScale = new Vector3( 0.3f, 0.3f, 0.3f );
				
				if ( tex == null ) {
					tex = new Texture2D( 1, 1 );
					tex.SetPixel( 0, 0, Color.green );
					tex.Apply();
				}
					
				obj.renderer.sharedMaterial.SetTexture( "_MainTex", tex );
				obj.renderer.sharedMaterial.color = Color.white;
				
				obj.transform.localPosition = new Vector3( 0.0f, Common.CharacterInfo.BOX_HEIGHT, Common.CharacterInfo.BOX_HALF_WIDTH );
				
				ControllerUtils.ApplyVisibilityMap( obj, VisibilityMap.GetInstance() );
			}
			

			ThrowScript s = obj.AddComponent< ThrowScript >();
			s.runner = runner;
			s.throws = throws;
			s.onDone = onDone;
			
			s.Step();
			
			return s;
		}
		
		
		public void Update() {
			if ( currentThrow != null ) {
				Refresh( Time.deltaTime );
			} else {
				Step();
			}
		}
		
		
		private void Refresh( float delta ) {
			Parabola p = currentThrow.parabola;
			accTime = Mathf.Min( accTime + ( delta * timeMultiplier ), currentThrow.timeLimit );
			
			transform.position = ControllerUtils.CreateVector3( p.GetPointAt( accTime ) );
			
//			float percent = accTime / currentThrow.timeLimit;
//			if ( percent <= 0.1f || percent >= 0.9f )
//				Debugger.Log( percent + " -> " + currentThrow.timeLimit + ", " + transform.position.y );
			
			if ( accTime >= currentThrow.timeLimit )
				Step();
		}
		
		
		private void Step() {
			if ( throws.Count > 0 ) {
				currentThrow = throws[ 0 ];
				accTime = 0;
				throws.RemoveAt( 0 );
				
				Refresh( 0 );
			} else {
				enabled = false;
				
				if ( timeToDestroyGameObject >= 0 ) {
					TimerScript.AddTimer( ( int ) ( timeToDestroyGameObject * 1000 ), new DestroyObjectListener( gameObject ) );
				}
				
				if ( runner != null )
					runner.OnDone();
				
				if ( onDone != null )
					onDone();
				
				Object.DestroyImmediate( this );
			}
		}		
		
		
		internal class DestroyObjectListener : TimerScriptListener {
			
			GameObject target;
			
			
			public DestroyObjectListener( GameObject target ) {
				this.target = target;
			}
			
		
			public void OnTimerEnded (int timerID) {
				Object.DestroyImmediate( target );
			}
			
		}
		
	}
	
}
