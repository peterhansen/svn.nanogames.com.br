using System;
using System.Collections.Generic;

using UnityEngine;


namespace GameController {
	
	/// <summary>
	/// Classe estática de localização e internacionalização.
	/// </summary>
	/// <seealso cref="Get"/>
	public static class L10n {
		
		private static LanguagePackage currentLanguagePackage;
		
		public static List< LanguagePackage > languagePackages = new List< LanguagePackage >();
		
		
		/// <summary>
		/// Carrega todos as strings de localização já presentes no sistema.
		/// </summary>
		public static void LoadAll() {
			// TODO: ler de um asset bundle?
			string[] contents = {
				"ERROR_NO_AP: O personagem não possui APs para realizar essa ação.",
				"ERROR_NO_AMMUNITION: O personagem não possui munição suficiente para realizar essa ação.",
				
				"ATTRIBUTE_HP: HP",
				"ATTRIBUTE_AP: AP",
				"ATTRIBUTE_XP: XP",
				
				"ATTRIBUTE_ATTACK: Ataque",
				"ATTRIBUTE_DEFENSE: Defesa",
				"ATTRIBUTE_AGILITY: Agilidade",
				"ATTRIBUTE_TECHNIQUE: Tecnica",
				"ATTRIBUTE_PERCEPTION: Percepcao",
				"ATTRIBUTE_INTELLIGENCE: Inteligencia",
				"ATTRIBUTE_STATUS: Estado",
				"ATTRIBUTE_MORAL: Moral",
				
				"NATURE_AGRESSIVE: Agressivo",
				"NATURE_ALERT: Alerta",
				"NATURE_BRUISER: Objetivo",
				"NATURE_CEREBRAL: Cerebral",
				"NATURE_GENERALIST: Generalista",
				"NATURE_SPEEDSTER: Velocista",
				"NATURE_STRAIGHTFORWARD: Agressivo",
				"NATURE_TANK: Montanha",
				
				"ACTION_CLOSE: Fechar"
				
			};
			LanguagePackage languagePackage = LanguagePackage.Create( "PT", contents );
			languagePackages.Add(languagePackage);
		} 
		
		
		/// <summary>
		/// Seleciona qual pacote de linguagem será usado pelo sistema.
		/// </summary>
		/// <param name='name'>
		/// Nome do pacote de linguagem.
		/// </param>
		public static void SelectLanguagePackage( string name ) {
			foreach( LanguagePackage languagePackage in languagePackages ) {
				if( languagePackage.Name.Equals( name ) ) {
					currentLanguagePackage = languagePackage;
					break;
				}
			}
		}
		
		/// <summary>
		/// Retorna a string de acordo com o pacote de linguagem corrente. Caso ele não possua uma string para esse string code,
		/// ele retorna uma string vazia.
		/// </summary>
		/// <param name='stringCode'>
		/// String code usado para identificar a string.
		/// </param>
		public static string Get( string stringCode ) {
			return currentLanguagePackage[ stringCode ];
		}
	}
}

