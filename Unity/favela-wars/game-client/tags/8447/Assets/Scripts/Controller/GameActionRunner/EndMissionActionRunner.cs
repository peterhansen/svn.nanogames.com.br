using System;

using UnityEngine;

using Utils;
using GameCommunication;

namespace GameController {
	
	public class EndMissionActionRunner : GameActionRunner {
		
		protected EndMissionActionData endMissionData;
	
		public EndMissionActionRunner( EndMissionActionData endMissionData ) : base( false ) {
			this.endMissionData = endMissionData;
		}
		
		protected override void Execute() {
			
			if( endMissionData.winningFaction == Faction.NONE ) {
				Debugger.Log( "Empate! Nenhuma das faccoes saiu vitoriosa..." );
			} else if( endMissionData.winningFaction == Faction.POLICE ) {
				// TODO: por enquanto estou assumindo que o personagem é um policial
				Debugger.Log( "Você venceu!" );
			} else if( endMissionData.winningFaction == Faction.CRIMINAL ) {
				Debugger.Log( "Sua tropa foi derrotada..." );
			}
			
			GUIManager.ClearAllRenderables();
			Application.LoadLevel( ( int ) Utils.Scene.CHOOSE_MISSION );
		}
	}
}

