using System;
using GameCommunication;
using UnityEngine;


namespace GameController {

	public class ExplosionScript : MonoBehaviour {
		
		public const float EXPLOSION_TIME = 0.6f;
		
		private float ray;
		
		private float accTime;
		
		private GameVector diff;
		
		private GameVector center;
		
		private GameObject explosion;
		
		private static int count = 0;
		
		private ExplosionRunner runner;
		
		
		public static void Explode( GameVector center, float ray, ExplosionRunner runner ) {
			GameObject explosion = GameObject.CreatePrimitive( PrimitiveType.Sphere );
			explosion.transform.localScale = new Vector3();
			explosion.transform.position = ControllerUtils.CreateVector3( center );
			explosion.renderer.material.color = Color.white;
			explosion.AddComponent( typeof( ExplosionScript ) );
			explosion.name = "Explosion #" + count++;
			
			ExplosionScript s = ( ExplosionScript ) explosion.GetComponent( typeof( ExplosionScript ) );
			s.explosion = explosion;
			s.runner = runner;
			s.center = center;
			// as dimensões de uma esfera na Unity referem-se ao diâmetro, não ao raio
			s.ray = ray * 2.0f;
		}
		
		
		public void Update() {
			accTime += Time.deltaTime;
			
			if ( accTime < EXPLOSION_TIME ) {
				float size = ray * ( accTime / EXPLOSION_TIME );
				explosion.transform.localScale = new Vector3( size, size, size );
				explosion.renderer.material.shader = Shader.Find( "Transparent/Bumped Diffuse" );
				explosion.renderer.material.color = new Color( 1, 0.8f, 0.6f, 1.0f - ( accTime / EXPLOSION_TIME ) );
			} else {
				// terminou a explosão
				Destroy( explosion );
				
				if ( runner != null )
					runner.OnDone();
			}
		}
		
	}

}

