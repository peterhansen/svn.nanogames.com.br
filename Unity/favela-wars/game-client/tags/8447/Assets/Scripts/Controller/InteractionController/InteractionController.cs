using System.Collections.Generic;

using UnityEngine;

using Squid;
using SharpUnit;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class InteractionController {
				
		public List< Control > controls = new List< Control >();
		
		public InteractionController parent;
		
		
		public virtual void OnDone() {
			GUIManager.SetInteractionController( parent );
		}
		
		
		public virtual void OnRemoved() {
		}
		
		
		public void SetParent( InteractionController parent ) {
			this.parent = parent;
		}
		
		
		public InteractionController GetParent() {
			return parent;
		}
		
		
		public virtual void Initialize() {
		}
		
		
		/// <summary>
		/// Método chamado automaticamente pelo GUIManager a cada iteração do loop principal.
		/// </summary>
		public virtual void Update() {
		}
		
		
		public virtual void ReceiveInputEvent( InputEventScene inputEvent ) {			
		}
		
		
		/// <summary>
		/// Gera um evento de input da tela. Esse método pode ser usado dentro do Update() para que sejam enviados eventos para o método ReceiveInputEvent.
		/// </summary>
		protected void GenerateInputEvent() {
			// TODO criar singleton
			GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
			Desktop desktop = guiManager.Desktop;
			
			if ( desktop.HotControl == desktop && Input.GetMouseButtonDown( Common.MOUSE_BUTTON_LEFT ) ) {
				Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
				RaycastHit hitInfo;
				
				InputEventScene ie = new InputEventScene();
				ie.mouseButton = Common.MOUSE_BUTTON_LEFT;
				
				if ( Physics.Raycast( ray, out hitInfo ) ) {
					ie.gameObject = hitInfo.transform.gameObject;
					ie.selectionPosition = hitInfo.point;
				} else {
					ie.gameObject = null;
					ie.selectionPosition = Vector3.zero;
				}
				
				ReceiveInputEvent( ie );
			}
		}
	}
}
