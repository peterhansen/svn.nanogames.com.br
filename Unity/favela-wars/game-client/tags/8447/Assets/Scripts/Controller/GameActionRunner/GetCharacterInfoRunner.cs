using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameCommunication;
using Utils;

namespace GameController {
	
	public class GetCharacterInfoRunner : GameActionRunner {
		
		private GetCharacterInfoData data;
		
		public GetCharacterInfoRunner( GetCharacterInfoData getCharacterInfoData ) : base( true ) {
			this.data = getCharacterInfoData;
		}
		
		protected override void Execute() {
			GetCharacterInfoInteractionController ic = GUIManager.GetInteractionController() as GetCharacterInfoInteractionController;
			if( ic != null ) {
				ic.SetData( data );
			}
			
			OnDone();
		}
	}
}

