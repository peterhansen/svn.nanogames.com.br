using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameCommunication;
using GameController;
using Utils;

//using PartMap = GameCommunication.AssetAssemblyData.PartMap;
using Object = UnityEngine.Object;


namespace GameController {
	
	public static class GameObjectBuilder {
		
		private static readonly Dictionary< string, Mesh > cachedMeshes = new Dictionary< string, Mesh >();
		
		private static readonly Dictionary< string, Material > cachedMaterials = new Dictionary< string, Material >();
	
		
		public static bool IsInCache( string url ) {
			return cachedMeshes.ContainsKey( url ) || cachedMaterials.ContainsKey( url );
		}
		
		
		public static void AddAsset( string name, AssetBundle a ) {
			Object[] objects = a.LoadAll();
			
			foreach ( Object o in objects ) {
				HandleObject( a, o, name );
			}
		}
		
		
		private static void HandleObject( AssetBundle a, Object obj, string name ) {
			if ( obj is Mesh ) {
				if ( !cachedMeshes.ContainsKey( name ) )
					cachedMeshes.Add( name, ( Mesh ) obj );
			} else if ( obj is Material ) {
				if ( !cachedMaterials.ContainsKey( name ) )
					cachedMaterials.Add( name, ( Material ) obj );
			} else if ( obj is GameObject ) {
				GameObject go = ( GameObject ) obj;
				if ( go.name.Equals( "_root" ) )
					go.name = "<root>";
				
				CreateGameObject( go, true );
			}
		}
		
		
		public static GameObject CreateGameObject( GameObject original, bool createCopy ) {
			GameObject gameObject;
			if ( createCopy ) {
				gameObject = ( GameObject ) GameObject.Instantiate( original );
				gameObject.name = original.name;
			} else {
				gameObject = original;
			}
			
			// verifica se o gameobject tem um descritor de como remontá-lo
			PrefabDescriptor descriptor = gameObject.GetComponent< PrefabDescriptor >();
			if ( descriptor == null ) {
				foreach ( Transform child in gameObject.transform ) {
					if ( child.gameObject != gameObject )
						CreateGameObject( child.gameObject, false );
				}
				return gameObject;
			}
			
			AssetAssemblyData data = descriptor.data;
			
			//criar components
			foreach ( PartMap meshMap in data.meshes ) {
				try {
					GameObject meshPart = GetPart( gameObject, meshMap.partName );
					MeshFilter meshFilter = ( MeshFilter ) meshPart.GetComponent( typeof( MeshFilter ) );
					if ( meshFilter == null ) {
						Debugger.LogWarning( "part not found: " + meshMap.partName );
						continue;
					}
					
					Mesh mesh = cachedMeshes[ meshMap.names[ 0 ] ];
					meshFilter.mesh = mesh;
				} catch ( Exception e ) {
					Debugger.LogError( "part not found: " + meshMap.partName + "\n" + e );
					return gameObject;
				}
			}

			// associa os materiais às partes corretas
			foreach ( PartMap materialMap in data.materials ) {
				List< Material > materials = new List< Material >();
				Debugger.Log( "->" + materialMap.names.Count );
				foreach ( string materialSource in materialMap.names ) {
					try {
						Material material = cachedMaterials[ materialSource ];
						materials.Add( material );
					} catch ( Exception e ) {
						Debugger.LogError( e );
					}
				}
				
				GameObject part = GetPart( gameObject, materialMap.partName );
				if ( part != null && part.renderer != null ) {
//					part.renderer.sharedMaterials = materials.ToArray();
					part.renderer.material = materials[ 0 ]; // HACK
				}
			}
			
			if ( createCopy )
				GameObject.DestroyImmediate( descriptor );
			
			return gameObject;
		}
		
		
		public static GameObject GetPart( GameObject o, string partName ) {
			if ( partName.Equals( "_root" ) || partName.Equals( o.name ) )
				return o;
			else
				return ControllerUtils.FindChild( o, partName );
		}
		
	}
	
	
}
