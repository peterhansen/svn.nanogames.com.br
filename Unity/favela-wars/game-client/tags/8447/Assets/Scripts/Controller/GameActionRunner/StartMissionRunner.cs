using System;
using System.IO;

using UnityEngine;

using Pathfinding;

using Utils;
using GameCommunication;

using Object = UnityEngine.Object;


namespace GameController {
	
	/// <summary>
	/// 
	/// </summary>
	public class StartMissionRunner : GameActionRunner, TimerScriptListener {
		
		protected const int CAMERA_HEIGHT_FACTOR = 16;
		
		protected StartMissionData data;
		
		protected int currentAssetBundle = -1;
		protected WWW assetDownloader;
		
		protected float startTime;
		protected LoadingInteractionController intContLoading;
		
		protected Camera camera;
		
		protected float step;
		
		protected float progress;
		
		
		public StartMissionRunner( StartMissionData data ) : base( true ) {
			this.data = data;
			step = 1.0f / data.assetData.Count;
			
			GameObject o = GameObject.Find( "Visibility Map" );
			if ( o == null )
				throw new Exception( "Visibility map not found." );
			
			// instanciando mapa de visibilidade	
			o.GetComponent< VisibilityMap >().Instantiate( data.sectorsInfo, data.faction );
		
			intContLoading = new LoadingInteractionController();
			GUIManager.SetInteractionController( intContLoading );
		}
		
		
		public void OnTimerEnded( int timerID ) {
			if ( !Application.loadedLevelName.Equals( "Mission Screen" ) ) {
				TimerScript.AddTimer( 1, this );
				return;
			} else if( camera == null ) {
				camera = Camera.main;
				
				// NOTA: não podemos desligar a câmera... do contrário, não podemos mostrar um GuiRenderer.
				//SetCameraActive( false );
			}
			
			if ( assetDownloader.error != null ) {
				Debugger.LogError( "StartMissionRunner download error: " + assetDownloader.error );
				// TODO: por enquanto, estamos seguindo para o outro download quando um deles falha.
				// Essa medida é muito drástica, talvez queiramos tentar novamente.
				LoadNextAsset();
				return;
			}
			
			intContLoading.UpdateProgress( progress + assetDownloader.progress * step );
			
			if( !assetDownloader.isDone || assetDownloader.progress < 1.0f ) {
				TimerScript.AddTimer( 1, this );
			} else {
				GameObjectBuilder.AddAsset( GetCurrentAssetBundle().name, assetDownloader.assetBundle );
				LoadNextAsset();
			}
		}
		
		
		protected override void Execute() {
			startTime = Time.realtimeSinceStartup;
			GameManager.GetInstance().SetTurnData( data.turnData );
			LoadNextAsset();
		}
		
		
		protected void LoadNextAsset() {
			currentAssetBundle++;
			progress = ( currentAssetBundle / (float) data.assetData.Count );
			intContLoading.UpdateProgress( progress );
			
			if ( currentAssetBundle >= data.assetData.Count ) {
				// garante que objetos de missão estejam na pasta correta durante a execução
				GameObject missionHolder = ControllerUtils.GetMissionHolderObject();
				GameObject[] missionObjects = GameObject.FindGameObjectsWithTag( Common.Tags.MISSION );
				foreach ( GameObject o in missionObjects )
					o.transform.parent = missionHolder.transform;
			
				// TODO necessário estar aqui também?
//				GUIManager.SetInteractionController( new SelectCharacterInteractionController() );
				Debugger.Log( "Mission " + data.missionName + " loaded in " + ( Time.realtimeSinceStartup - startTime ) + " seconds." );
				SetCameraActive( true );
				
				OnDone();
				
				VisibilityMap visibilityMap = VisibilityMap.GetInstance();
				if ( visibilityMap != null ) {
					// atualizando textura de visibilidade
					foreach ( GameObject go in GameObject.FindGameObjectsWithTag( Common.Tags.TERRAIN ) ) {
						ControllerUtils.ApplyVisibilityMap( go, visibilityMap );
						
						MeshCollider mc = go.GetComponent< MeshCollider >();
						if ( mc != null ) {
							MeshFilter mf = go.GetComponent< MeshFilter >();
							if ( mf != null )
								mc.sharedMesh = mf.sharedMesh;
						}
					}
					
					visibilityMap.UpdateVisibilityBuffer( Faction.POLICE );
				}
				
				// criando AstarPath
				GameObject aStar = new GameObject();
				aStar.name = "A*";
				AstarPath aStarPath = aStar.AddComponent< AstarPath >();
				aStarPath.Initialize();
				AstarPath.active = aStarPath;
				aStarPath.astarData.DeserializeGraphs( new AstarSerializer( aStarPath ), data.rawAstarData );
				// HACK
//				aStarPath.astarData.graphs[0].Scan();
				AstarPath.active.showGraphs = false;
				AstarPath.active.showNavGraphs = false;
				
				// TODO: hack temporário para a versão hotseat
				GameManager.GetInstance().UpdateGraph( Faction.POLICE );
				BuildingInfo.RebuildScene();
				
				foreach ( GameObject character in WorldManager.GetInstance().EnumerateCharacters() )
					BuildingInfo.RefreshLayer( character );
				
				IsoCameraScript.GetInstance().Prepare();
			} else {
				AssetBundleData asset = data.assetData[ currentAssetBundle ];
				if ( GameObjectBuilder.IsInCache( asset.path ) ) {
					// asset já havia sido baixado
					LoadNextAsset();
				} else {
//					assetDownloader = WWW.LoadFromCacheOrDownload( "file://" + Application.dataPath + "/../" + asset.path, asset.version );
					string bla = @"file://" + ( Common.Paths.SERVER + @"\" + asset.path );
					Debugger.Log( "DOWNLOADING: " + bla );
					assetDownloader = new WWW( bla );
					TimerScript.AddTimer( 1, this );
				}
			}
		}
		

		protected AssetBundleData GetCurrentAssetBundle() {
			return data.assetData[ currentAssetBundle ];
		}
		
		
		protected void SetCameraActive( bool active ) {
			if ( camera != null ) {
				camera.enabled = active;
				camera.transform.position = ControllerUtils.CreateVector3( data.sectorsInfo.GetMissionMapCenter() ) - data.sectorsInfo.missionHeightInSectors * Common.LEVEL_LAYER_HEIGHT * CAMERA_HEIGHT_FACTOR * camera.transform.forward;
			}
		}
	}
}