using System;
using SharpUnit;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using GameCommunication;
using Utils;
using System.Threading;

using GameModel;
using System.Text;


namespace GameController {
	
	public class GameManager : ServerConnectionListener {
		
		/// <summary>
		/// Instância do GameController (singleton).
		/// </summary>
		private static GameManager instance;
		
		private int localPlayer;
	
		private TurnData turnData;
	
		private CharacterListData characterListData;
		
		private GameObject lastSelectedSoldier;
		

		private GameManager() {
			// carregando textos de localização
			L10n.LoadAll();
			L10n.SelectLanguagePackage( "PT" );
		}
		
		
		/// <summary>
		/// Obtém a instância do GameController, criando-a caso necessário.
		/// </summary>
		/// <returns>
		/// A <see cref="GameController"/>
		/// </returns>
		public static GameManager GetInstance() {
			if ( instance == null ) {
				instance = new GameManager();
			}
			
			return instance;
		}
				
		
		public void SetTurnData( TurnData turnData ) {
			// TODO: isso é feito só durante o HotSeat
			localPlayer = turnData.faction == Faction.POLICE? 0 : 1;
			
			foreach( GameObject gameObject in WorldManager.GetInstance().GetGameObjects() ) {
				PathFollowerScript pathFollowerScript = gameObject.GetComponent< PathFollowerScript >();
				// TODO pathFollowerScript.SetGraphNodesWalkability
//				pathFollowerScript.SetGraphNodesWalkability( pathFollowerScript.Faction == turnData.faction );
			}
			
			this.turnData = new TurnData( turnData );
			
			GUIManager.SetInteractionController( new SelectCharacterInteractionController( false ) );
		}
		
		
		public void UpdateGraph( Faction faction ) {
			bool hasGameObjects = false;
			foreach( GameObject gameObject in WorldManager.GetInstance().GetGameObjects() ) {
				PathFollowerScript pathFollowerScript = gameObject.GetComponent< PathFollowerScript >();
				CharacterInfoScript characterInfo = gameObject.GetComponent< CharacterInfoScript >();
				pathFollowerScript.SetGraphNodesWalkability( characterInfo.faction == faction );
				hasGameObjects = true;
			}
				
			// atualizamos o grafo e chamamos um caminho dummy nele, só para que ele se atualize imediatamente
			if( hasGameObjects ) {
				GameObject dummyFollower = new GameObject();
				dummyFollower.AddComponent< PathFollowerScript >().StartPath( Vector3.zero, Vector3.right );
			}
		}
		
		
		public void SetCharacterListData( CharacterListData characterListData ) {
			this.characterListData = characterListData;
		}
		
		
		public CharacterListData GetCharacterListData() {
			return characterListData;
		}
		
		
		public int GetTurnCount() {
			return turnData.turnNumber;
		}
		
		
		public Faction GetTurnFaction() {
			return turnData.faction;
		}
		
		
		/// <summary>
		/// Descarrega a instância do GameController.
		/// </summary>
		public static void Unload() {
			instance = null;
		}
		
		
		public int GetLocalPlayer() {
			return localPlayer;
		}
		
		
		public void SetLocalPlayer( int id ) {
			this.localPlayer = id;
		}
		
		
		/// <summary>
		/// Último soldado selecionado pelo jogador.
		/// </summary>
		public GameObject LastSelectedSoldier {
			get { return lastSelectedSoldier; }
			set { 
				WorldManager worldManager = WorldManager.GetInstance();
				if ( GetTurnFaction() == worldManager.GetCharacterFaction( value ) )
					lastSelectedSoldier = value;
			}
		}
		
		
		/// <summary>
		/// Envia requisição para o modelo de jogo
		/// </summary>
		public void SendRequest( Request request ) {
			ServerConnection.SendData( request, this );
		}
		
		
		public void OnServerResponse( List< Response > responses ) {
			foreach ( Response r in responses ) {
				GameActionRunner.ProcessRequests( r.GetGameActionsData() );
			}
		}
		

		public void OnServerError( Exception e ) {
		}
		
	}
}

