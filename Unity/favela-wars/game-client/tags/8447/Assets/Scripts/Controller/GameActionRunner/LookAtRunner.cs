using System;

using UnityEngine;

using GameCommunication;


namespace GameController {
	
	public class LookAtRunner : GameActionRunner {
		
		private LookAtData data;
		
		public LookAtRunner( LookAtData data ) : base( false ) {
			this.data = data;
		}
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.characterID );
			gameObject.transform.LookAt( gameObject.transform.position + ControllerUtils.CreateVector3( data.direction ) );
		}
	}
}

