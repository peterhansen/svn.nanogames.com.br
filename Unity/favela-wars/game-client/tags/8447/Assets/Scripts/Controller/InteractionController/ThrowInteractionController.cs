using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Squid;

using Utils;
using GameController;
using GameCommunication;


namespace GameController {
	
	public class ThrowInteractionController : ActionInteractionController {
		
		private static readonly float MAX_DIRECTION_Y = Common.CharacterInfo.MAX_THROW_SPEED;
		
		private static readonly float MIN_DIRECTION_Y = 8;
		
		private static readonly float DEFAULT_DIRECTION_Y = 30;
		
		public enum State {
			NONE,
			CHOOSE_TARGET,
			CHOOSE_HEIGHT,
			DONE
		}
		
		private State state;
		
		private static GameObject target;
		
		private static GameObject projectile;
		
		private static GameObject trailObject;
		
		private Parabola parabola;
		
		private float parabolaTime;
		
		private GameVector destination;
		
		private ThrowScript currentThrow;
		
		private float lastMouseY = -1;
		
		/// <summary>
		/// Multiplicador da velocidade de arrasto do controle de altura do arremesso.
		/// </summary>
		public float heightMultiplier = 0.5f;
		
		/// <summary>
		/// Direção y do próximo indicador de arremesso.
		/// </summary>
		private float directionY = 15;
		
		/// <summary>
		/// Indica se houve um evento prévio de click do mouse, para evitar problemas de recebimento de eventos
		/// de mouse up, quando o evento de mouse down foi efetuado com outro interactionController ativo.
		/// </summary>
		private bool mouseLeftHold;
		
		public static Parabola lastParabola;
		
		
		public ThrowInteractionController( GameObject selectedCharacter, GameAction action ) : base( selectedCharacter, action ) {
			if ( !( action is ThrowGameAction ) )
				throw new Exception( "ThrowInteractionController requires a ThrowGameAction" );
			
			if ( target == null ) {
				target = GameObject.CreatePrimitive( PrimitiveType.Cylinder );
				GameObject.DestroyImmediate( target.GetComponent< Collider >() );
				target.transform.localScale = new Vector3( 3.5f, 0.2f, 3.5f );
				target.renderer.material.shader = Shader.Find( "Transparent/Bumped Diffuse" );
				target.renderer.material.color = new Color( 0, 1, 0, 0.5f );
				target.name = "Throw controller target";
				target.renderer.enabled = false;
			}
			
			if ( projectile == null ) {
				projectile = GameObject.CreatePrimitive( PrimitiveType.Sphere );
				GameObject.DestroyImmediate( projectile.GetComponent< Collider >() );
				projectile.name = "Throw controller projectile";
				projectile.layer = LayerMask.NameToLayer( "Ignore Raycast" );
				projectile.transform.localScale = new Vector3( 0.5f, 0.5f, 0.5f );
			}
			
			controls.Add( new ThrowControl( this ) );
			SetState( State.CHOOSE_TARGET );
		}
		
		
		public override void Update() {
			if ( Input.GetMouseButtonDown( Common.MOUSE_BUTTON_LEFT ) ) {
				mouseLeftHold = true;
				lastMouseY = Input.mousePosition.y;
			}
			
			switch ( state ) {
				case State.CHOOSE_TARGET:
					if ( mouseLeftHold && Input.GetMouseButtonUp( Common.MOUSE_BUTTON_LEFT ) ) {
						//Debugger.Log( "CLICK 1" );
						RefreshTarget();
						SetState( State.CHOOSE_HEIGHT );
					}
				break;
				
				case State.CHOOSE_HEIGHT:
					if ( Input.GetMouseButtonUp( Common.MOUSE_BUTTON_LEFT ) ) {
						//Debugger.Log( "CLICK 2" );
					} else if ( mouseLeftHold && Input.GetMouseButton( Common.MOUSE_BUTTON_LEFT ) ) {
						directionY = Mathf.Clamp( directionY + ( Input.mousePosition.y - lastMouseY ) * heightMultiplier, MIN_DIRECTION_Y, MAX_DIRECTION_Y );
						lastMouseY = Input.mousePosition.y;
					}
				break;
			}
			
			if ( Input.GetMouseButtonUp( Common.MOUSE_BUTTON_LEFT ) ) {
				mouseLeftHold = false;
			}
		}
		
		
		private void RefreshTarget() {
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hitInfo;
			
			if ( Physics.Raycast( ray, out hitInfo ) ) {
				target.transform.position = hitInfo.point;
				target.renderer.enabled = true;
				projectile.renderer.enabled = true;
				
				GameVector origin = ControllerUtils.CreateGameVector( selectedCharacter.transform.position );
				destination = ControllerUtils.CreateGameVector( hitInfo.point );
				GameVector direction = destination - origin;
				direction.Scale( 0.3f );
				directionY = GetInitialAngle( new Parabola( new GameVector( origin ), new GameVector( direction ) ) );
				direction.y = directionY;
				lastMouseY = Input.mousePosition.y;
			
				parabola = new Parabola( new GameVector( origin ), new GameVector( direction ) );
				
				parabolaTime = Math.Min( direction.Norm(), parabola.GetTime( 0 ) );
			
				OnParabolaEnd();
			}
		}
		
		
		private void AddTrail() {
			if ( trailObject != null ) {
				GameObject.DestroyImmediate( trailObject );
			}
			
			trailObject = new GameObject();
			trailObject.name = projectile.name + " trail";
			trailObject.layer = projectile.layer;
			trailObject.AddComponent( typeof( TrailRenderer ) );
			trailObject.transform.parent = projectile.transform;
			trailObject.transform.localPosition = Vector3.zero;
			
			TrailRenderer t = ( TrailRenderer ) trailObject.GetComponent( typeof( TrailRenderer ) );
			t.time = parabolaTime;
			t.startWidth = 2.0f;
			t.endWidth = 3.2f;	
			t.material.shader = Shader.Find( "Transparent/Bumped Diffuse" );
			t.material.color = new Color( 0.2f, 1, 0, 0.45f );
		}
		
		
		public void OnParabolaEnd() {
			switch ( state ) {
				case State.CHOOSE_TARGET:
				case State.CHOOSE_HEIGHT:
					// recalcula os parâmetros da parábola de forma que o alvo continue o mesmo, ainda que haja
					// mudança na altura do arremesso
					parabola.direction.y = directionY;
					float time = parabola.GetTime( Math.Min( 0, parabola.origin.y ) );
					parabola.direction.x = ( destination.x - parabola.origin.x ) / time;
					parabola.direction.z = ( destination.z - parabola.origin.z ) / time;
					
					if ( parabola.direction.Norm() > Common.CharacterInfo.MAX_THROW_SPEED ) {
						parabola.direction.SetScale( Common.CharacterInfo.MAX_THROW_SPEED );
					}
					float collisionTime = CheckParabolaCollision( parabola, parabolaTime );
					parabolaTime = collisionTime > 0 ? collisionTime : parabola.GetTime( Mathf.Min( 0, parabola.origin.y ) );
					
					currentThrow = ThrowScript.Throw( new List< ThrowInfo > { new ThrowInfo( parabola, parabolaTime ) }, projectile, OnParabolaEnd );
					currentThrow.timeMultiplier = 3;
					AddTrail();
				break;
			}
		}
		
		
		/// <summary>
		/// Calcula uma velocidade y inicial que seja suficiente para transpor um eventual obstáculo no caminho.
		/// Caso não seja encontrado um arremesso válido, retorna a velocidade y inicial padrão.
		/// </summary>
		private float GetInitialAngle( Parabola p ) {
			for ( float y = MIN_DIRECTION_Y; y < MAX_DIRECTION_Y; y += 5 ) {
				p.direction.y = y;
				float time1 = p.GetTime( 0 );
				p.direction.x = ( destination.x - p.origin.x ) / time1;
				p.direction.z = ( destination.z - p.origin.z ) / time1;
				
				float time2 = CheckParabolaCollision( p, time1 );
				if ( time2 < 0 || ( time2 / time1 ) >= 0.9f )
					return y;
			}
			
			return DEFAULT_DIRECTION_Y;
		}
		
		
		private float CheckParabolaCollision( Parabola p, float maxTime ) {
			float STEP = 0.1f;
			Vector3 previousPos = ControllerUtils.CreateVector3( p.GetPointAt( 0 ) );
			
			for ( float t = STEP; t <= maxTime; t += STEP ) {
				Vector3 pos = ControllerUtils.CreateVector3( p.GetPointAt( t ) );
				Vector3 direction = pos - previousPos;
				
				if ( Physics.Raycast( pos, direction, direction.magnitude ) ) {
					return t;
				}
				
				previousPos = pos;
			}
			
			return -1;
		}
		
		
		public State GetState() {
			return state;
		}
		
		
		public void Throw( Control sender ) {
			lastParabola = parabola;
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			ThrowObjectRequest t = new ThrowObjectRequest( GameManager.GetInstance().GetLocalPlayer(), characterID, ( ( ThrowGameAction ) action ).itemID, parabola.direction );
			GameManager.GetInstance().SendRequest( t );
			OnDone();
		}
		
		
		public void Repeat( Control sender ) {
			parabola = lastParabola;
			Throw( sender );
		}
		
		
		public void SetState( State state ) {
			//Debugger.Log( "ThrowInteractionController state: " + state );
			this.state = state;
			ThrowControl control = controls[ 0 ] as ThrowControl;
			control.radialMenu.Visible = false;
			
			switch ( state ) {
				case State.NONE:
					GameObject.DestroyImmediate( trailObject );
					if ( projectile != null )
						projectile.renderer.enabled = false;
					
					target.renderer.enabled = false;
				break;
				
				case State.CHOOSE_TARGET:
					target.renderer.enabled = false;
					projectile.renderer.enabled = false;
				break;
				
				case State.CHOOSE_HEIGHT:
					control.radialMenu.AddButton( new RadialMenuOption( RadialMenuButton.RadialMenuIconType.OK, Throw ) );
					control.radialMenu.AddButton( new RadialMenuOption( RadialMenuButton.RadialMenuIconType.CANCEL, Cancel ) );
					control.radialMenu.Show();
					IsoCameraScript.GetInstance().FocusOn( target );
					IsoCameraScript.GetInstance().userInputIsBlocked = true;
				break;
					
				case State.DONE:
				break;
			}
		}
		
		
		public override void OnRemoved() {
			IsoCameraScript.GetInstance().userInputIsBlocked = false;
		}
		
		
		public override void OnDone() {
			SetState( State.NONE );
			GUIManager.Reset();
//			base.OnDone();
		}
		
		
		public void Cancel( Control sender ) {
			SetState( State.NONE );
			GUIManager.SetInteractionController( parent );
		}
	}
	
	internal class ThrowControl : Frame {
		
		ThrowInteractionController controller;
		
		protected const int BUTTON_WIDTH = 120;
		
		public RadialMenuControl radialMenu;
		
		public ThrowControl( ThrowInteractionController controller ) {
			this.controller = controller;
		}
		
		protected override void Initialize () {
			base.Initialize ();
			
			Position = new Point( 0, 0 );
			Size = new Point( 1000,1000 );
			
			GUIManager guiManager = GameObject.FindObjectOfType( typeof( GUIManager ) ) as GUIManager;
			if( guiManager == null ) {
				Debugger.LogError( "TurnInfoGUIRenderer Error: No GUIManager found!" );
				return;
			}
			
			guiManager.Desktop.Controls.Add( this );
			
			radialMenu = new RadialMenuControl( new Point( Screen.width / 2, Screen.height / 2 ) );
			radialMenu.Visible = false;
			Controls.Add( radialMenu );
		}
		
		private void Throw( Control sender ) {
			Event.current.Use();
			controller.Throw( sender );
		}
		
		private void Cancel( Control sender ) {
			Event.current.Use();
			Debugger.Log( "CANCEL" );
			controller.Cancel( sender );
		}
		
	}
	
}

