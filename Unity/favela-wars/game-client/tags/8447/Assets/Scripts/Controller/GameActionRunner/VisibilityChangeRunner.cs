using System;
using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class VisibilityChangeRunner : GameActionRunner {
		
		protected VisibilityChangeData data;
	
		
		public VisibilityChangeRunner( VisibilityChangeData data ) : base( false ) {
			this.data = data;
		}
		
		
		protected override void Execute() {
			VisibilityMap visibilityMap = VisibilityMap.GetInstance();
			visibilityMap.SetVisible( new List< Sector >( data.newlyVisibleSectors ), true, data.faction );
			
			foreach( GameObject gameObject in WorldManager.GetInstance().GetGameObjects() ) {
				if( gameObject != null && gameObject.renderer != null ) {
					gameObject.renderer.enabled = data.visibleWorldObjectIds.ContainsKey( WorldManager.GetInstance().GetWorldID( gameObject ) );
				}
			}
			GameManager.GetInstance().SetCharacterListData( data.characterList );
		}
	}
}