using System;
using System.Collections.Generic;


namespace GameController {
	
	[ Serializable ]
	public class LanguagePackage {
		
		private Dictionary< string, string > strings = new Dictionary< string, string >();
		
		private string name;
		public string Name {
			get { return name; }
		}
		
		private string errorMessage;
		public string ErrorMessage {
			get { return errorMessage; }
		}
		
		
		private LanguagePackage( string name, string[] lines ) {
			this.name = name;
			foreach( string line in lines ) {
				int i = line.IndexOf(':');
				string stringCode = line.Substring( 0, i );
				string stringContent = line.Substring( i + 2, line.Length - i - 2 );
				
				if( strings.ContainsKey( stringCode ) ) {
					errorMessage += "Erro: o stringCode \"" + stringCode + "\" possui entradas repetidas. \n";
					continue;
				}
				
				strings[ stringCode ] = stringContent;
			}
		}
		
		
		public override string ToString () {
			return name + ( errorMessage.Length == 0 ? "": "(COM ERROS)" );
		}
		
		
		public string this[ string stringCode ] {
		    get {
				return strings.ContainsKey( stringCode )? strings[ stringCode ] : ""; 
			}
		}
		
		
		public static LanguagePackage Create( StringHolder holder ) {			
			return LanguagePackage.Create( holder.name, holder.content );
		}
		
		
		public static LanguagePackage Create( string name, string[] contents ) {
			return new LanguagePackage( name, contents );
		}
	}
}

