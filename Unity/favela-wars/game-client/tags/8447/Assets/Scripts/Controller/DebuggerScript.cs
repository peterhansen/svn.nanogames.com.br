using UnityEngine;
using System.Collections;

using Utils;
using GameCommunication;
using GameController;


[ ExecuteInEditMode ]
public class DebuggerScript : MonoBehaviour {
	
	public bool writeToFile = true;
	
	public bool writeToConsole = true;
	
	private bool _writeToFile;
	
	private bool _writeToConsole;
	
	
	public void Awake() {
		Init();
	}
	
	
	public void Start() {
		Init();
	}
	

	public void OnEnable() {
		Init();
	}
	
	
	private void Init() {
		if ( _writeToFile != writeToFile || _writeToConsole != writeToConsole || ( writeToConsole != Debugger.HasRegularLogger() ) ) {
			Debugger.SetWriteToFile( writeToFile );
		
			if ( writeToConsole ) {
				Debugger.SetRegularLogger( Debug.Log );
				Debugger.SetWarningLogger( Debug.LogWarning );
				Debugger.SetErrorLogger( Debug.LogError );
			} else {
				Debugger.SetRegularLogger( null );
				Debugger.SetWarningLogger( null );
				Debugger.SetErrorLogger( null );
			}
			
			Debugger.SetLineDrawer( DrawLine );
		
			_writeToFile = writeToFile;
			_writeToConsole = writeToConsole;
		
			Debugger.Log( "Debugger status: file(" + (writeToFile ? "on" : "off" ) + ") console(" + (writeToConsole ? "on)" : "off)" ));
		}
	}
	
	
	public void Update() {
		Init();
	}
	
	
	public void OnApplicationQuit() {
		Debugger.Release();
	}
	
	
	public void DrawLine( GameVector origin, GameVector destination, uint color, float time ) {
		Vector3 o = new Vector3( origin.x, origin.y, origin.z );
		Vector3 d = new Vector3( destination.x, destination.y, destination.z );
		Color c = ControllerUtils.GetColor( color );
		
		Debug.DrawLine( o, d, c, time );
	}
	
}
