using System;


namespace GameCommunication {

	
	[Serializable]
	public class StartMissionData : GameActionData {
		
		public string sceneName;
		public TurnData turnData;
		
		
		public StartMissionData( string sceneName, TurnData turnData ) {
			this.sceneName = sceneName;
			this.turnData = turnData;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as StartMissionData );
		}
		
		
		public bool Equals( StartMissionData data ) {
			return data != null && sceneName.Equals( data.sceneName );
		}
		
		
		public override int GetHashCode() {
        	return sceneName.GetHashCode();
    	}
		
	}
}

