using System;
using System.Collections.Generic;

namespace GameCommunication {
	
	public class Response {
		
		protected List< GameActionData > gameActionsData = new List< GameActionData >();
		
		
		public void AddGameActionData( GameActionData gameActionData ) {
			gameActionsData.Add( gameActionData );
		}
		
		
		public List< GameActionData > GetGameActionsData() {
			return gameActionsData;
		}
	}
}

