using System;

namespace GameCommunication {
	
	[Serializable]
	public abstract class Request {
		
		protected int playerID;
		
		public void setPlayerID( int playerID ) {
			this.playerID = playerID;
		}
		
		public int GetPlayerID() {
			return playerID;
		}
		
	}
}

