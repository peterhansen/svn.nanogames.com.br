using System;
using Utils;

namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para mudar a postura de um personagem.
	/// </summary>
	[ Serializable ]
	public class SetStanceRequest : Request {
		
		public int worldObjectID;
		
		public Stance newStance;
		
		public SetStanceRequest( int worldObjectID, Stance stance ) {
			this.worldObjectID = worldObjectID;
			this.newStance = stance;
		}
	}
}

