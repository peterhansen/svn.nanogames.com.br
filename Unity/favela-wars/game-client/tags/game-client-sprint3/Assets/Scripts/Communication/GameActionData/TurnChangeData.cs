using System;

using Utils;

namespace GameCommunication {
	
	[Serializable]
	public class TurnChangeData : GameActionData {
		
//		public int turnNumber;
//		public Faction faction;
		public TurnData turnData;
		
		public TurnChangeData( int turnNumber, Faction faction ) {
			turnData = new TurnData( turnNumber, faction );
		}
		
		public TurnChangeData( TurnData turnData ) {
			this.turnData = turnData;
		}
		
		public TurnChangeData( TurnChangeData turnChangeData ) {
			turnData = new TurnData( turnChangeData.turnData );
		}
		
		public override bool Equals (object obj) {
			return Equals( obj as TurnChangeData );
		}
		
		public bool Equals( TurnChangeData other ) {
			return other != null && turnData == other.turnData;
		}
		
		public override string ToString () {
			return "TurnData( " + turnData.ToString() + " )"; 
		}
		
		public override int GetHashCode() {
			return ToString().GetHashCode();
		}
	}
}

