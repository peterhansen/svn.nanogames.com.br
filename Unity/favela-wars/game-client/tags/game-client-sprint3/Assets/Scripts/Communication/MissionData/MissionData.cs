using System;
using System.IO;
//using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;

using Utils;
using SharpUnit;



namespace GameCommunication {
	
	[ Serializable ]
	public class MissionData {
		
		public string missionName = "";
		
		public string mapName = "";
		
		public string sceneName = "";
		
		public HashSet< SpawnAreaMissionData > spawnAreasData = new HashSet< SpawnAreaMissionData >();
		
		public HashSet< EndMissionCondition > endMissionConditions = new HashSet< EndMissionCondition >();
		
		
		public MissionData() {
		}
		
		
		public string GetMissionFilePath() {
			string dir = Path.Combine( System.Environment.CurrentDirectory, "missions" );
			if ( !Directory.Exists( dir ) )
                Directory.CreateDirectory( dir );
			
			return Path.Combine( dir, missionName + Common.MISSION_FILE_EXTENSION );
		}
		
		
		public void ExportToFile() {
			FileStream file = null;
			try {
				file = new FileStream( GetMissionFilePath(), FileMode.Create );
				BinaryFormatter bin = new BinaryFormatter();
				bin.Serialize( file, this );
			} finally {
				if ( file != null )
					file.Close();
			}
		}
		
		
		public static MissionData ImportFromFile( string filename ) {
			FileStream file = null;
			BinaryFormatter bin = new BinaryFormatter();
			
			try {
				file = new FileStream( Path.Combine( GetPath(), filename + Common.MISSION_FILE_EXTENSION ), FileMode.Open );
				MissionData missionData = ( MissionData ) bin.Deserialize( file );
				return missionData;
			} catch( System.Exception e ) {
				throw new InvalidMissionFile( e.Message );
			} finally {
				if ( file != null )
					file.Close();
			}
		}		
		
		
		public static string GetPath() {
			return Path.Combine( System.Environment.CurrentDirectory, "missions" );
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MissionData );
		}
		
		
		public bool Equals( MissionData d ) {
			return d != null && missionName.Equals( d.missionName ) && mapName.Equals( d.mapName ) &&
				   sceneName.Equals( d.sceneName ) && spawnAreasData.Equals( d.spawnAreasData ) &&
				   endMissionConditions.Equals( d.endMissionConditions );
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		
	}
	
	
	#region Mission Generator Test Case
	
	internal class MissionDataTestCase : TestCase {
		
		private MissionData data;
		
		public override void SetUp() {
			data = new MissionData();
			data.mapName = "áéíóú";
			data.sceneName = "ãç²";
			data.missionName = "testando 1 2 3 bla bla";
			
			DestroyAllEnemies end1 = new DestroyAllEnemies();
			end1.maxNumberOfTurns = 66;
			data.endMissionConditions.Add( end1 );
			
			for ( int i = 0; i < 12; ++i ) {
				SpawnAreaMissionData d = new SpawnAreaMissionData();
				d.capacity = new Random().Next( 5, 10 );
				d.faction = ( Faction ) ( i % 3 );
				d.minObjects = 1;
				d.areaBox.Randomize();
				data.spawnAreasData.Add( d );
			}
		}
		
		
		public override void TearDown() {
		}
		
		
		[UnitTest]
		public void CanImportExportedMissionData() {
			data.ExportToFile();
			
			MissionData loadedData = MissionData.ImportFromFile( data.missionName );
			Assert.True( loadedData.Equals( data ) );
		}
		
	}
	
	
	#endregion
}


public class InvalidMissionFile : Exception {
	
	public InvalidMissionFile() : this( "Arquivo de missão inválido." ) {
	}
	
	
	public InvalidMissionFile( string message ) : base( message ) {
	}
	
	
}