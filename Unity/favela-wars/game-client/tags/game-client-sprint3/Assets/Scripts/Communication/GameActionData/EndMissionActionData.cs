using System;

namespace GameCommunication {
	
	[Serializable]
	public class EndMissionActionData : GameActionData {
		String reason;
		
		public EndMissionActionData ( EndMissionCondition reason ) {
			this.reason = reason.ToString();
		}
	}
}

