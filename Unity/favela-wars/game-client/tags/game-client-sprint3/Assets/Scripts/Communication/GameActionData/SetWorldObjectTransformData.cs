using System;


namespace GameCommunication {

	
	[Serializable]
	public class SetWorldObjectTransformData : GameActionData {
		
		public int worldObjectID;
		
		public GameVector position;
		
		public GameVector angle;
		
		public GameVector scale;
		
		
		public SetWorldObjectTransformData( int worldObjectID, GameVector position, GameVector angle, GameVector scale ) {
			this.worldObjectID = worldObjectID;
			
			this.position = position;
			this.angle = angle;
			this.scale = scale;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as SetWorldObjectTransformData );
		}
		
		
		public bool Equals( SetWorldObjectTransformData data ) {
			if ( data == null )
	            return false;
    
			return worldObjectID == data.worldObjectID && position.Equals( data.position ) && 
					angle.Equals( data.angle ) && scale.Equals( data.scale );
		}
		
		
		public override int GetHashCode() {
        	return worldObjectID; // TODO implementar GetHashCode() decente
    	}
	}
}

