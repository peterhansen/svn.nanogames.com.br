using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	public class GetCharacterActionsData : GameActionData {
		
		public Action actionTree;

		
		public GetCharacterActionsData( Action actionTree ) {
			this.actionTree = actionTree;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MoveCharacterData );
		}
		
		
		public bool Equals( GetCharacterActionsData data ) {
			return data != null && actionTree.Equals( data.actionTree );
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
	}
}

