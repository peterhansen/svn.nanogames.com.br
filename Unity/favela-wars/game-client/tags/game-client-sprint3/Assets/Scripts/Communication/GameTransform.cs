using System;

using SharpUnit;

using Utils;
using System.Text;


namespace GameCommunication {
	
	[ Serializable ]
	public class GameTransform {
		
		public GameVector position = new GameVector();
		
		public GameVector direction = new GameVector( GameVector.ZAxis );
		
		public GameVector scale = new GameVector( 1.0f, 1.0f, 1.0f );
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as GameTransform );
		}
		
		
		public bool Equals( GameTransform other ) {
			return other != null && position.Equals( other.position ) && 
				scale.Equals( other.scale ) && 
				direction.Equals( other.direction );
		}
		
		
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append( "GameTransform { pos" );
			sb.Append( position );
			sb.Append( " dir" );
			sb.Append( direction );
			sb.Append( " sca" );
			sb.Append( scale );
			sb.Append( " }" );
			
			return sb.ToString();
		}
		
		
		public override int GetHashCode () {
			return ToString().GetHashCode();
		}
	
		
		protected float FlipNegativeAngles( float angle ) {
			return angle < 0? 360.0f - ( Math.Abs( angle ) % 360.0f ) : angle;
		}
		
		
		public void Set( GameTransform gt ) {
			position.Set( gt.position );
			scale.Set( gt.scale );
			direction.Set( gt.direction );
		}
			
		
		public static GameTransform GetRandom() {
			GameTransform gt = new GameTransform();
			gt.Randomize();
			return gt;
		}
		
		
		public void Randomize() {
			position.Randomize();
			direction.Randomize();
			scale.Randomize();
		}
	}
}

