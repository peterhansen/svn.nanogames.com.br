using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para carregar um asset (modelo 3D,
	/// textura, cenário, etc.).
	/// </summary>
	[ Serializable ]
	public class LoadAssetRequest : Request {
		
		public LoadAssetRequest() {
		}
	
	}
}

