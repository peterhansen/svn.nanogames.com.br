using System;
using System.Collections;
using System.Collections.Generic;

// TODO: Não pode usar GAME MODEL!!!
using GameModel;


namespace GameCommunication {

	
	[Serializable]
	public class GetCharacterInfoData : GameActionData {
		
		public Character character;
		
		public bool fullInfo;
	
		
		public GetCharacterInfoData( Character character, bool fullInfo ) {
			this.character = character;
			this.fullInfo = fullInfo;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as GetCharacterInfoData );
		}
		
		
		public bool Equals( GetCharacterInfoData data ) {
			return data != null && character.Equals( data.character ) && fullInfo == data.fullInfo;
		}
		
		
		public override int GetHashCode() {
        	return character.GetHashCode();
    	}
	}
}

