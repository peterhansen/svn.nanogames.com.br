using System;
using System.Text;


namespace GameCommunication {
	
	[ Serializable ]
	public class HitInfo {
		public GameVector position;
		public int worldObjectId;
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.Append( "[HitInfo] pos:" );
			sb.Append( position );
			sb.Append( " id:" );
			sb.Append( worldObjectId );
			
			return sb.ToString();
		}
	}
}

