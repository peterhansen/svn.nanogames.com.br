using System;

namespace GameCommunication {
	
	[ Serializable ]
	public class WaitRequest : Request {
		
		public int waitTime;
		

		public WaitRequest( int waitTime ) {
			this.waitTime = waitTime;
		}
		
	}
}

