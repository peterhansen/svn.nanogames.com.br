using System;
using System.Collections.Generic;


namespace GameCommunication {
	
	[Serializable]
	public class VisibilityChangeData : GameActionData {
		
		public List< int > visibleWorldObjectIds = new List< int >();
		public List< int > invisibleWorldObjectIds = new List< int >();
		
		public static VisibilityChangeData operator+ ( VisibilityChangeData a, VisibilityChangeData b ) {
			VisibilityChangeData tmp = new VisibilityChangeData();
			tmp.CopyFrom( a );
			tmp.Combine( b );
			return tmp;
		}
		
		public void CopyFrom( VisibilityChangeData vcd ) {
			foreach( int id in vcd.visibleWorldObjectIds ) {
				visibleWorldObjectIds.Add( id );
			}
			
			foreach( int id in vcd.invisibleWorldObjectIds ) {
				invisibleWorldObjectIds.Add( id );
			}
		}
		
		public void MakeVisible( int id ) {
			if ( !visibleWorldObjectIds.Contains( id ) )
				visibleWorldObjectIds.Add( id );
			
			while ( invisibleWorldObjectIds.Contains( id ) )
				invisibleWorldObjectIds.Remove( id );
		}
		
		public void Combine( VisibilityChangeData vcd ) {
			
			foreach( int id in vcd.visibleWorldObjectIds ) {
				visibleWorldObjectIds.Add( id );
			}
			
			foreach( int id in vcd.invisibleWorldObjectIds ) {
				invisibleWorldObjectIds.Add( id );
			}
			
			foreach( int id in visibleWorldObjectIds ) {
				while ( invisibleWorldObjectIds.Contains( id ) )
					invisibleWorldObjectIds.Remove( id );
			}
		}
	}
}
