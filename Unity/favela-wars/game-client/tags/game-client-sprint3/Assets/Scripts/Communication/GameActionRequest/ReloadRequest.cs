using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para recarregar uma arma.
	/// </summary>
	[ Serializable ]
	public class ReloadRequest : Request {
		
		public ReloadRequest() {
		}
	
	}
}

