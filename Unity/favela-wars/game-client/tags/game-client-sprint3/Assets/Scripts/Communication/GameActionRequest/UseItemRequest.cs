using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para usar um item.
	/// </summary>
	[ Serializable ]
	public class UseItemRequest : Request {
		
		public UseItemRequest() {
		}
	
	}
}

