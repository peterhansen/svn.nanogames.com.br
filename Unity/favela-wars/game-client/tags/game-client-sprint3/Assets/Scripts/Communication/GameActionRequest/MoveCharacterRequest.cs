using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para movimentar um personagem. 
	/// </summary>
	[ Serializable ]
	public class MoveCharacterRequest : Request {
		
		public int worldObjectID;
		
		public GameVector destination;
		
		
		public MoveCharacterRequest( int worldObjectID, GameVector destination ) {
			this.worldObjectID = worldObjectID;
			this.destination = destination;
		}
	
	}
}

