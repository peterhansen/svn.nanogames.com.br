using System;

namespace GameCommunication
{
	[ Serializable ]
	public class LogRequest : Request {
		
		public string message;
		
		public LogLevel logLevel;
		

		public LogRequest( LogLevel logLevel, string message ) {
			this.message = message;
			this.logLevel = logLevel;
		}
	}
}

