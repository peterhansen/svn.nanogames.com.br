using System;
using System.Collections.Generic;
using System.Collections;


namespace GameCommunication {
	
	public class ShootData : GameActionData {
		
		public GameVector shotOrigin;
		
		public GameVector hitPoint;
		
		public int shooterWorldID;
		
		public int weaponID;

		
		public ShootData( int shooterWorldID, int weaponID, GameVector shotOrigin, GameVector hitPoint ) {
			this.shooterWorldID = shooterWorldID;
			this.weaponID = weaponID;
			this.shotOrigin = shotOrigin;
			this.hitPoint = hitPoint;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MoveCharacterData );
		}
		
		
		public bool Equals( ShootData data ) {
			return data != null && shooterWorldID == data.shooterWorldID && weaponID == data.weaponID && hitPoint.Equals( data.hitPoint );
		}
		
		
		public override int GetHashCode() {
        	return hitPoint.GetHashCode() + ( shooterWorldID + ", " + weaponID ).GetHashCode();
		}
		
		
	}
}

