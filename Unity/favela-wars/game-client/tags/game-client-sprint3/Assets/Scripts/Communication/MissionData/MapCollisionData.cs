using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using SharpUnit;

using Utils;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe armazena os dados dos dados de colisão de um mapa. 
	/// </summary>
	[ Serializable ]
	public class MapCollisionData {
		
		private List< Box > boxes;
		
		private string missionName;
		
		
		public MapCollisionData( string missionName, List< Box > boxes ) {
			this.missionName = missionName;
			this.boxes = boxes;
		}
		
				
		public override bool Equals( object obj ) {
			return Equals( ( ( MapCollisionData ) obj ) );
		}
		
		
		public bool Equals( MapCollisionData other ) {
			// TODO: não adianta testar só o nome da missão - implementar Equals de lista de caixas
			return other != null && missionName.Equals( other.GetMissionName() );
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		
		
		public string GetMissionName() {
			return missionName;
		}

		
		public List< Box > GetBoxes() {
			return boxes;
		}
		
		public string GetMapCollisionFilePath() {
			string path = GetPath();
			if ( !Directory.Exists( path ) )
                Directory.CreateDirectory( path );
			
			return Path.Combine( path, missionName + Common.LEVEL_FILE_EXTENSION );
		}
		
		
		public void ExportToFile() {
			FileStream file = null;
			
			try {
				file = new FileStream( GetMapCollisionFilePath(), FileMode.Create );
				BinaryFormatter bin = new BinaryFormatter();
				bin.Serialize( file, this );
			} finally {
				if ( file != null )
					file.Close();
			}
		}
		
		
		public static MapCollisionData ImportFromFile( string filename ) {
			FileStream file = null;
			
			try {
				file = new FileStream( Path.Combine( GetPath(), filename + Common.LEVEL_FILE_EXTENSION ), FileMode.Open );
				BinaryFormatter bin = new BinaryFormatter();
				MapCollisionData mapCollisionData = ( MapCollisionData ) bin.Deserialize( file );
				return mapCollisionData;
			} catch( System.Exception e ) {
				throw new InvalidMissionFile( e.Message );
			} finally {
				if ( file != null )
					file.Close();
			}
		}				
		
		public static string GetPath() {
			return Path.Combine( System.Environment.CurrentDirectory, "missions" );
		}
	}
	
	
	#region Mission Generator Test Case
	
	internal class MapCollisionDataTestCase : TestCase {
	
		public override void SetUp() {
		}
	
		public override void TearDown() {
		}
		
		[UnitTest]
		public void CanImportExportedMapCollisionData( ) {
			List< Box > boxes = new List< Box >();
			MapCollisionData data = new MapCollisionData( "testando 1 2 3 bla bla", boxes );
			
			data.ExportToFile();
			
			MapCollisionData loadedData = MapCollisionData.ImportFromFile( data.GetMissionName() );
			Assert.True( loadedData.Equals( data ) );
		}
	}
	
	#endregion
	
}


public class InvalidMapCollisionFile : Exception {
	
	public InvalidMapCollisionFile() : this( "Arquivo descritor de colisões de mapa inválido." ) {
	}
	
	
	public InvalidMapCollisionFile( string message ) : base( message ) {
	}
	
}
