using System;
using System.Collections;
using System.Collections.Generic;


namespace GameCommunication {
	
	
	[ Serializable ]
	public class GetCharacterInfoRequest : Request {
		
		public int characterWorldID;
		
		
		public GetCharacterInfoRequest( int characterWorldID ) {
			this.characterWorldID = characterWorldID;
		}
	
	}
}

