using System;
using SharpUnit;
using Utils;
using System.Text;


namespace GameCommunication {
	
	[ Serializable ]
	public class GameVector {
		
		
		#region campos
		public float x;
		public float y;
		public float z;
		#endregion
		
		
		#region métodos		
		
		
		public static readonly GameVector Zero = new GameVector();
		
		public static readonly GameVector XAxis = new GameVector( 1.0f, 0.0f, 0.0f );
		public static readonly GameVector YAxis = new GameVector( 0.0f, 1.0f, 0.0f );
		public static readonly GameVector ZAxis = new GameVector( 0.0f, 0.0f, 1.0f );
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="x">
		/// A <see cref="System.Single"/>
		/// </param>
		/// <param name="y">
		/// A <see cref="System.Single"/>
		/// </param>
		/// <param name="z">
		/// A <see cref="System.Single"/>
		/// </param>
		public GameVector( float x, float y, float z ) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		
		/// <summary>
		/// Construtor padrão - inicializa todos os campos com 0
		/// </summary>
		public GameVector() {
			this.x = 0.0f;
			this.y = 0.0f;
			this.z = 0.0f;
		}

		
		/// <summary>
		/// Copy Constructor. 
		/// </summary>
		/// <param name="v">
		/// A <see cref="GameVector"/>
		/// </param>
		public GameVector( GameVector v ) {
			x = v.x;
			y = v.y;
			z = v.z;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x">
		/// A <see cref="System.Single"/>
		/// </param>
		/// <param name="y">
		/// A <see cref="System.Single"/>
		/// </param>
		/// <param name="z">
		/// A <see cref="System.Single"/>
		/// </param>
		public void Set( float x, float y, float z ) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		
		public void Set( GameVector gv ) {
			Set( gv.x, gv.y, gv.z );
		}
		
		
		/// <summary>
		/// Compara dois vetores.
		/// </summary>
		/// <param name="vec">
		/// O outro <see cref="GameVector"/> a ser comparado com o atual
		/// </param>
		/// <returns>
		/// Retorna um <see cref="System.Boolean"/> indicando se dois vetores são iguais
		/// </returns>
		public bool Equals( GameVector vec ) {
			return vec != null && NanoMath.Equals( this.x, vec.x ) && NanoMath.Equals( this.y, vec.y ) && NanoMath.Equals( this.z, vec.z );
		}
		
		
		public bool Equals( GameVector vec, float epsilon ) {
			return vec != null && NanoMath.Equals( this.x, vec.x, epsilon ) && NanoMath.Equals( this.y, vec.y, epsilon ) && NanoMath.Equals( this.z, vec.z, epsilon );
		}

		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj">
		/// A <see cref="System.Object"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Boolean"/>
		/// </returns>
		public override bool Equals( object obj ) {
			return Equals( obj as GameVector );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/>
		/// </returns>
		public override string ToString () {
			StringBuilder sb = new StringBuilder();
		
			sb.Append( "( " );
			sb.Append( x );
			sb.Append( ", " );
			sb.Append( y );
			sb.Append( ", " );
			sb.Append( z );
			sb.Append( " )" );
			
			return sb.ToString();
		}
		
		
		public bool IsZero() {
			return NanoMath.Equals( 0.0f, Norm() );
		}
		
		
		/// <summary>
		/// Soma dois vetores e retorna num terceiro
		/// </summary>
		/// <param name="other">
		/// O outro <see cref="GameVector"/> a ser somado com esse
		/// </param>
		/// <returns>
		/// Um <see cref="GameVector"/> contendo a soma dos dois
		/// </returns>
		public GameVector Add( GameVector other ) {
			return new GameVector( x + other.x, y + other.y, z + other.z );
		}
		
		
		/// <summary>
		/// Subtrai dois vetores e retorna num terceiro
		/// </summary>
		/// <param name="other">
		/// O outro <see cref="GameVector"/> a ser subtraido desse
		/// </param>
		/// <returns>
		/// Um <see cref="GameVector"/> contendo a subtração dos dois
		/// </returns>
		public GameVector Sub( GameVector other ) {
			return new GameVector( x - other.x, y - other.y, z - other.z );
		}
		
		
		/// <summary>
		/// Sobrecarga de operador servindo de açucar sintático para <see cref="Add"/> 
		/// </summary>
		/// <param name="a">
		/// A <see cref="GameVector"/>
		/// </param>
		/// <param name="b">
		/// A <see cref="GameVector"/>
		/// </param>
		/// <returns>
		/// A <see cref="GameVector"/>
		/// </returns>
		public static GameVector operator+ ( GameVector a, GameVector b ) {
			return a.Add( b );
		}
		
		
		/// <summary>
		/// Sobrecarga de operador servindo de açucar sintático para <see cref="Sub"/>  
		/// </summary>
		/// <param name="a">
		/// A <see cref="GameVector"/>
		/// </param>
		/// <param name="b">
		/// A <see cref="GameVector"/>
		/// </param>
		/// <returns>
		/// A <see cref="GameVector"/>
		/// </returns>
		public static GameVector operator- ( GameVector a, GameVector b ) {
			return a.Sub( b );
		}
		
		public static GameVector operator- ( GameVector a ) {
			return new GameVector( a ) * -1.0f;
		}
		
		/// <summary>
		/// Retorna a norma do vetor
		/// </summary>
		/// <returns>
		/// A <see cref="System.Single"/>
		/// </returns>
		public float Norm() {
			return (float) Math.Sqrt( x * x + y * y + z * z );
		}
		
		
		public static GameVector operator* ( GameVector a, float f ) {
			return new GameVector( a.x * f, a.y * f, a.z * f );
		}
		
		
		/// <summary>
		/// A distância entre dois vetores 
		/// </summary>
		/// <param name="vec">
		/// A <see cref="GameVector"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Single"/>
		/// </returns>
		public float DistanceTo( GameVector vec ) {
			return Sub( vec ).Norm();
		}
		
		
		/// <summary>
		/// Normaliza o vetor. Não faz checagem para vetor nulo.
		/// Deve voar se usada num vetor construido com a inicialização padrão?
		/// </summary>
		public void Normalize() {
			float abs = Norm();
			x /= abs;
			y /= abs;
			z /= abs;
		}
		
		
		public GameVector Unitary() {
			GameVector result = new GameVector( this );
			result.Normalize();
			return result;
		}
		
		
		/// <summary>
		/// Escalonamento do vetor por um escalar numérico 
		/// </summary>
		/// <param name="s">
		/// A <see cref="System.Single"/>
		/// </param>
		public void Scale( float s ) {
			x *= s;
			y *= s;
			z *= s;
		}
		
		
		public GameVector Cross( GameVector other ) {
			return new GameVector( y * other.z - other.y * z, z * other.x - x * other.z, x * other.y - y * other.x );
		}
		
		
		public static GameVector operator* ( GameVector a, GameVector b ) {
			return a.Cross( b );
		}
		
		
		public float Dot( GameVector other ) {
			return ( x * other.x ) + ( y * other.y ) + ( z * other.z );
		}
		
		/// <summary>
		/// Retorna o ângulo entre os dois vetores.
		/// </summary>
		/// <param name="other">
		/// A <see cref="GameVector"/>
		/// </param>
		/// <returns>
		/// Ângulo em graus.
		/// </returns>
		public float AngleBetween( GameVector other ) {
			if( IsZero() ||  other.IsZero() ) {
				throw new ArithmeticException();
			}
			
			return (float) ( ( 180.0f / Math.PI ) * Math.Acos( ( float ) this.Unitary().Dot( other.Unitary() ) ) );
		}
		
		
		public GameVector Project( GameVector other ) {
			if( other.IsZero() ) {
				throw new ArithmeticException();
			}
			
			float beta = AngleBetween( other );
			float factor = (float) Math.Cos( beta * ( Math.PI / 180.0f ) );
			float component = factor * Norm();
			
			return other.Unitary() * component;
		}
		
		
		public GameVector Project( GameVector other, out bool sameDirection ) {
			GameVector projected = Project( other );
			
			// verificando se as cordenadas não nulas tem sinal trocado
			if( !NanoMath.Equals( projected.x, 0.0f ) ) {
				sameDirection = NanoMath.GreaterEquals( projected.x * other.x,  0.0f );
			} else if( !NanoMath.Equals( projected.y, 0.0f ) ) {
				sameDirection = NanoMath.GreaterEquals( projected.y * other.y,  0.0f );
			} else {
				sameDirection = NanoMath.GreaterEquals( projected.z * other.z,  0.0f );
			}
			
			return projected;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>
		/// A <see cref="System.Int32"/>
		/// </returns>
		public override int GetHashCode () {
			return ToString().GetHashCode();
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>
		/// A <see cref="System.Boolean"/>
		/// </returns>
		public bool IsValid() {
			
			if ( float.IsNaN( x ) || float.IsNegativeInfinity( x ) || float.IsPositiveInfinity( x ) || float.IsInfinity( x ) )
				return false;
				
			if ( float.IsNaN( y ) || float.IsNegativeInfinity( y ) || float.IsPositiveInfinity( y ) || float.IsInfinity( y ) )
				return false;

			if ( float.IsNaN( z ) || float.IsNegativeInfinity( z ) || float.IsPositiveInfinity( z ) || float.IsInfinity( z ) )
				return false;
			
			return true;
		}
		
		
		public static GameVector GetRandom() {
			GameVector gv = new GameVector();
			gv.Randomize();
			return gv;
		}
		
		
		public void Randomize() {
			Random r = new Random();
			x = r.Next();
			y = r.Next();
			z = r.Next();
		}
		
		// TODO implementar os muitos métodos de conta com vetores (produto escalar, vetorial)
		#endregion
	}
	
	
	#region GameVector Test Case
	public class GameVectorTestCase : TestCase {

		[UnitTest]
		public void NormalizationTest() {
			GameVector v = new GameVector( 3.4f, -7.01f, 1.89f );
			v.Normalize();
			Assert.Equal( 1.0f, v.Norm() );
			Assert.CheckEquals( new GameVector( 0.4240991f, -0.8743926f, 0.2357492f ), v );
		}
		
		[UnitTest]
		public void CrossProductTrivial() {
			GameVector a = new GameVector( 1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 0.0f, 1.0f, 0.0f );
			
			GameVector product = a * b;
			GameVector expected = new GameVector( 0.0f, 0.0f, 1.0f );
			
			Assert.CheckEquals( expected, product );
		}
		
		[UnitTest]
		public void CrossProductNull() {
			GameVector a = new GameVector( 8.1f, -91.0652f, 3.14f );
			GameVector b = new GameVector( 0.0f, 0.0f, 0.0f );
			
			GameVector product = a * b;
			GameVector expected = new GameVector( 0.0f, 0.0f, 0.0f );
			
			Assert.CheckEquals( expected, product );
		}
		
		[UnitTest]
		public void CrossProductAxis() {
			GameVector a = new GameVector( 1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 0.0f, 1.0f, 0.0f );
			
			GameVector product = a * b;
			GameVector expected = new GameVector( 0.0f, 0.0f, 1.0f );
			
			Assert.CheckEquals( expected, product );
		}
		
		[UnitTest]
		public void CrossProduct() {
			GameVector a = new GameVector( 3.0f, -3.0f, 1.0f );
			GameVector b = new GameVector( 4.0f, 9.0f, 2.0f );
			
			GameVector product = a * b;
			GameVector expected = new GameVector( -15.0f, -2.0f, 39.0f );
			
			Assert.CheckEquals( expected, product );
		}
		
		[UnitTest]
		public void AngleBetweenNullTargetGameVector() {
			
			Assert.ExpectException( new ArithmeticException() );
			
			GameVector a = new GameVector( 1.0f, 1.0f, 1.0f );
			GameVector b = new GameVector();
			
			a.AngleBetween( b );
		}
		
		[UnitTest]
		public void AngleBetweenNullGameVector() {
			
			Assert.ExpectException( new ArithmeticException() );
			
			GameVector a = new GameVector();
			GameVector b = new GameVector( 1.0f, 0.0f, 0.0f );
			
			a.AngleBetween( b );
		}
		
		[UnitTest]
		public void AngleBetweenTheSameVector() {			
			GameVector a = new GameVector( -3.44f, 45.119f, -1.111f );
			Assert.Equal( 0.0f, a.AngleBetween( a ) );
		}
		
		[UnitTest]
		public void AngleBetweenVectorsIsBidirectional() {			
			GameVector a = new GameVector( -3.44f, 45.119f, -1.111f );
			GameVector b = new GameVector( 40.44f, 0.000002f, -8.949f );
			
			Assert.Equal( b.AngleBetween( a ), a.AngleBetween( b ) );
		}
		
		[UnitTest]
		public void AngleBetweenPerpendicularVectors() {
			GameVector a = new GameVector( 0.0f, 0.0f, 1.0f );
			GameVector b = new GameVector( 0.0f, 1.0f, 0.0f );
			GameVector c = new GameVector( 1.0f, 0.0f, 0.0f );
			
			Assert.Equal( 90.0f, a.AngleBetween( b ) );
			Assert.Equal( 90.0f, a.AngleBetween( c ) );
			Assert.Equal( 90.0f, b.AngleBetween( c ) );
		}
		
		[UnitTest]
		public void AngleBetweenVectors() {			
			GameVector a = new GameVector( 2.0f, 3.0f, 4.0f );
			GameVector b = new GameVector( 1.0f, -2.0f, 3.0f );
			
			Assert.Equal( 66.60715f, a.AngleBetween( b ) );
		}
		
		[UnitTest]
		public void ProjectionOnNullVector() {
			Assert.ExpectException( new ArithmeticException() );
			
			GameVector a = new GameVector( 1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 0.0f, 0.0f, 0.0f );
			
			a.Project( b );
		}
				
		[UnitTest]
		public void SameVectorProjection() {
			GameVector a = new GameVector( 2.0f, 3.0f, 4.0f );
			Assert.CheckEquals( a, a.Project( a ) );
		}
		
		[UnitTest]
		public void PerpendicularProjection() {
			GameVector a = new GameVector( 0.0f, 0.0f, 1.0f );
			GameVector b = new GameVector( 0.0f, 1.0f, 0.0f );
			
			Assert.CheckEquals( new GameVector(), a.Project( b ) );
		}
		
		[UnitTest]
		public void Projection() {
			GameVector a = new GameVector( 1.0f, 1.0f, 0.0f );
			GameVector b = new GameVector( 2.0f, 0.0f, 0.0f );
			
			GameVector expected = new GameVector( 1.0f, 0.0f, 0.0f );
			
			Assert.CheckEquals( expected, a.Project( b ) );
		}

	}
	#endregion
}

