using System;
using System.Collections.Generic;
using System.Collections;

namespace GameCommunication {
	
	public class MoveCharacterData : GameActionData {
		
		public List< GameVector > nodes = new List< GameVector >();
		public float speed;
		public int characterWorldID;

		
		public MoveCharacterData( int characterWorldID, List< GameVector > nodes ) {
			this.characterWorldID = characterWorldID;
			this.nodes = nodes;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MoveCharacterData );
		}
		
		
		public bool Equals( MoveCharacterData data ) {
			if( data == null )
	            return false;
    
			return nodes.Equals( data.nodes );
		}
		
		
		public override int GetHashCode() {
        	return base.GetHashCode();
		}
		
		
	}
}

