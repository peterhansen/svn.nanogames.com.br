using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para atirar com um personagem num ponto do mapa. 
	/// </summary>
	[ Serializable ]
	public class ShootRequest : Request {
		
		public int shooterWorldID;
		
		public int weaponID;
		
		public GameVector destination;
		
		
		public ShootRequest( int shooterID, int weaponID, GameVector destination ) {
			this.shooterWorldID = shooterID;
			this.weaponID = weaponID;
			this.destination = destination;
		}
	
	}
}

