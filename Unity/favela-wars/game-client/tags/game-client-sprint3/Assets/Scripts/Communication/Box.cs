using System;
using System.Collections.Generic;

using Utils;
using SharpUnit;
using GameModel;


namespace GameCommunication {
	
	[ Serializable ]
	public class Box {
		
		public enum Face {
			NORTH,
			EAST,
			SOUTH,
			WEST,
			CEILING,
			FLOOR
		}
		
		public static readonly Face[] Faces = new Face[] { Face.NORTH, Face.EAST, Face.SOUTH, Face.WEST, Face.CEILING, Face.FLOOR };
		
		private GameTransform transform = new GameTransform();
		
		private int worldObjectID;
		
				
		public Box( int worldObjectID, GameVector center, GameVector size ) {
			this.worldObjectID = worldObjectID;
			transform.position.Set( center );
			transform.direction.Set( GameVector.ZAxis );
			transform.scale.Set( size );
		}
		
		
		public Box( WorldObject obj ) {
			this.worldObjectID = obj.GetWorldID();
			this.transform = obj.GetGameTransform();
		}
		
		
		public Box( GameTransform transform ) : this( Common.NULL_WORLDOBJECT, transform ) {
		}
		
		
		public Box( int worldObjectID, GameTransform gt ) {
			this.worldObjectID = worldObjectID;
			transform.Set( gt );
		}
		
		
		public bool Coincidant( Box box  ) {
			return transform.Equals( box.GetGameTransform() );
		}
		
		
		public bool Equals( Box box ) {
			return box != null && box.worldObjectID == worldObjectID && Coincidant( box );
		}
		
		
		public override bool Equals (object other ) {
			return Equals( other as Box );
		}	
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		
		
		public GameTransform GetGameTransform() {
			return transform;
		}
		
		
		public GameVector GetPosition() {
			return transform.position;
		}
		
		
		public GameVector GetDirection() {
			return transform.direction;
		}
		
		
		public float GetX0() {
			return transform.position.x - ( transform.scale.x * 0.5f );
		}
		
		
		public float GetX1() {
			return transform.position.x + ( transform.scale.x * 0.5f );
		}
		
		
		public float GetY0() {
			return transform.position.y - ( transform.scale.y * 0.5f );
		}
		
		
		public float GetY1() {
			return transform.position.y + ( transform.scale.y * 0.5f );
		}
		
		
		public float GetZ0() {
			return transform.position.z - ( transform.scale.z * 0.5f );
		}
		
		
		public float GetZ1() {
			return transform.position.z + ( transform.scale.z * 0.5f );
		}
		
		
		public float GetDY() {
			return this.transform.scale.y;
		}
		
		
		public float GetDX() {
			return this.transform.scale.x;
		}
		
		
		public float GetDZ() {
			return this.transform.scale.z;
		}
		
		
		public int GetWorldObjectID() {
			return worldObjectID;
		}
		
		
		public bool Contains( GameVector point ) {	
			return NanoMath.LessEquals( point.x, GetX1() ) && NanoMath.GreaterEquals( point.x, GetX0() ) && 
				   NanoMath.LessEquals( point.y, GetY1() ) && NanoMath.GreaterEquals( point.y, GetY0() ) && 
				   NanoMath.LessEquals( point.z, GetZ1() ) && NanoMath.GreaterEquals( point.z, GetZ0() );
		}
		
		
		public GameVector GetNorthEastBottom() {
			return new GameVector( GetX1(), GetY0(), GetZ0() );
		}
		
		
		public GameVector GetNorthWestBottom() {
			return new GameVector( GetX0(), GetY0(), GetZ0() );
		}
		
		
		public GameVector GetSouthEastBottom() {
			return new GameVector( GetX1(), GetY0(), GetZ1() );
		}
		
		
		public GameVector GetSouthWestBottom() {
			return new GameVector( GetX0(), GetY0(), GetZ1() );
		}	
		
		
		public GameVector GetNorthEastTop() {
			return new GameVector( GetX1(), GetY1(), GetZ0() );
		}
		
		
		public GameVector GetNorthWestTop() {
			return new GameVector( GetX0(), GetY1(), GetZ0() );
		}
		
		
		public GameVector GetSouthEastTop() {
			return new GameVector( GetX1(), GetY1(), GetZ1() );
		}
		
		
		public GameVector GetSouthWestTop() {
			return new GameVector( GetX0(), GetY1(), GetZ1() );
		}
		
		
		public bool Intersects( Box box ) {
			float x0 = GetX0();
			float y0 = GetY0();
			float z0 = GetZ0();
			float x1 = GetX1();
			float y1 = GetY1();
			float z1 = GetZ1();
			
			float boxx0 = box.GetX0();
			float boxy0 = box.GetY0();
			float boxz0 = box.GetZ0();
			float boxx1 = box.GetX1();
			float boxy1 = box.GetY1();
			float boxz1 = box.GetZ1();
			
						
			if ( ( boxx0 >= x0 && boxx0 <= x1 ) || ( boxx1 >= x0 && boxx1 <= x1 ) ){
				if ( ( boxy0 >= y0 && boxy0 <= y1 ) || ( boxy1 >= y0 && boxy1 <= y1 ) ) {
					if ( ( boxz0 > z0 && boxz0 < z1) || ( boxz1 > z0 && boxz1 < z1 ) ) {
						return true;
					} else {
						//x0 ou x1 esta dentro. y0 ou y1 esta dentro. Pode estar todo incluido ( por dentro ou por fora )
						//ou todo excluido
						if ( ( boxz0 >= z0 && boxz1 <= z1 ) || ( boxz0 <= z0 && boxz1 >= z1 ) )
							return true;
					}
				} else {
					//x0 ou x1 esta dentro. y0 e y1 estão fora. Nada pode ser afirmado sobre z0 ou z1.
					//y0 e y1 tem que estar totalmente dentro. z pode estar em qualquer situação
					if ( ( boxy0 >= y0 && boxy1 <= y1 ) || ( boxy0 <= y0 && boxy1 >= y1 ) ) {
						if ( ( boxz0 >= z0 && boxz0 <= z1) || ( boxz1 >= z0 && boxz1 <= z1 ) )
							return true;
	
						if ( ( boxz0 >= z0 && boxz1 <= z1 ) || ( boxz0 <= z0 && boxz1 >= z1 ) ) 
							return true;

					} 
				}
			} else {
				//x0 e x1 estão fora. podem estar totalmente dentro também. nada pode ser afirmado sobre os outros...
				if ( ( boxx0 >= x0 && boxx1 <= x1 ) || ( boxx0 <= x0 && boxx1 >= x1 ) )
					if ( ( boxy0 >= y0 && boxy0 <= y1 ) || ( boxy1 >= y0 && boxy1 <= y1 ) ) {
							if ( ( boxz0 >= z0 && boxz0 <= z1) || ( boxz1 >= z0 && boxz1 <= z1 ) )
								return true;
	
							if ( ( boxz0 >= z0 && boxz1 <= z1 ) || ( boxz0 <= z0 && boxz1 >= z1 ) ) 
								return true;
					} else {
						if ( ( boxy0 >= y0 && boxy1 <= y1 ) || ( boxy0 <= y0 && boxy1 >= y1 ) ) {
							if ( ( boxz0 > z0 && boxz0 < z1) || ( boxz1 > z0 && boxz1 < z1 ) )
								return true;
	
							if ( ( boxz0 >= z0 && boxz1 <= z1 ) || ( boxz0 <= z0 && boxz1 >= z1 ) ) 
								return true;
						}
					}
				}
				

			return false;
		}
		
		
		public void BounceBackIfIntersects( Box box ) {
			BounceBackIfIntersects( box, null );
		}
		
		
		public bool Inside( Box box ) {
			return box.GetX0() >= GetX0() && box.GetX1() <= GetX1() && 
				box.GetY0() >= GetY0() && box.GetY1() <= GetY1() && 
					box.GetZ0() >= GetZ0() && box.GetZ1() <= GetZ1();
		}
		
		
		public void BounceBackIfIntersects( Box box, GameVector direction ) {
			float lesser;
			GameVector intersection0 = new GameVector();
			GameVector intersection1 = new GameVector();
			
			if ( direction != null && ( Coincidant( box ) || Inside( box ) ) ) {
				GameVector reverse = new GameVector( direction );
				reverse = reverse * -1.0f;
				box.GetGameTransform().position += reverse;
			}
			
			for ( int c = 0; c < 6; ++c ) {
				
				if ( !Intersects( box ) )
					return;
				
				
				intersection0.x = box.GetX0() - GetX0();
				intersection0.y = box.GetY0() - GetY0();
				intersection0.z = box.GetZ0() - GetZ0();
				
				intersection1.x = GetX1() - box.GetX1();
				intersection1.y = GetY1() - box.GetY1();
				intersection1.z = GetZ1() - box.GetZ1();
				
				lesser = intersection1.x;
				lesser = Math.Min( lesser, intersection1.y );
				lesser = Math.Min( lesser, intersection1.z );
				
				lesser = Math.Min( lesser, intersection0.x );
				lesser = Math.Min( lesser, intersection0.y );
				lesser = Math.Min( lesser, intersection0.z );

				
				if ( NanoMath.Equals( lesser, intersection0.x ) ) {
					box.GetGameTransform().position.x = GetX0() - ( box.GetDX() / 1.5f );
				} 
				
				if ( NanoMath.Equals( lesser, intersection0.z ) ) {
					box.GetGameTransform().position.z = GetZ0() - ( box.GetDZ() / 1.5f );
				}				
				
				if ( NanoMath.Equals( lesser, intersection1.x ) ) {
					box.GetGameTransform().position.x = GetX1() + ( box.GetDX() / 1.5f );
				} 
				
				
				if ( NanoMath.Equals( lesser, intersection0.y ) ) {
					box.GetGameTransform().position.y = GetY0() - ( box.GetDY() / 1.5f );
				} 				
				
				if ( NanoMath.Equals( lesser, intersection1.y ) ) {
					box.GetGameTransform().position.y = GetY1() + ( box.GetDY() / 1.5f );
				} 
				
				
				if ( NanoMath.Equals( lesser, intersection1.z ) ) {
					box.GetGameTransform().position.z = GetZ1() + ( box.GetDZ() / 1.5f );
				}
			}
			
			
			
		}
		
		
		public RayBoxInterceptionReply Intercept( GameVector origin, GameVector target ) {
			
			List< RayBoxInterception > interceptions = new List< RayBoxInterception >();
						
			foreach( Face face in Faces ) {
				AddFaceInterceptions( interceptions, face, origin, target );
			}
			
			MergeInterceptions( interceptions );
			
			RayBoxInterceptionReply reply = new RayBoxInterceptionReply();
			
			if( interceptions.Count == 2 ) {
			
				if( interceptions[ 0 ].t < interceptions[ 1 ].t ) {
					reply.entry = interceptions[ 0 ];
					reply.exit = interceptions[ 1 ];
				} else {
					reply.entry = interceptions[ 1 ];
					reply.exit = interceptions[ 0 ];
				}
			} else if( interceptions.Count == 1 ) {
			
				// entry or exit?
				if( Contains( origin ) ) {
					reply.exit = interceptions[ 0 ];
				} else {
					reply.entry = interceptions[ 0 ];
				}
			}
			
			return reply;
		}
		
		
		protected void AddFaceInterceptions( List< RayBoxInterception > interceptions, Face face, GameVector origin, GameVector target ) {			
			GameVector direction = target - origin;
			
			float t = 0;
			float delta2;
			
			switch( face ) {
				case Face.EAST:
					delta2 = GetX1() - origin.x;
					t = delta2 / direction.x;
					break;
					
				case Face.NORTH:
					delta2 = GetZ0() - origin.z;
					t = delta2 / direction.z;
					break;
				
				case Face.SOUTH:
					delta2 = GetZ1() - origin.z;
					t = delta2 / direction.z;
					break;				
				
				case Face.WEST:
					delta2 = GetX0() - origin.x;
					t = delta2 / direction.x;
					break;
					
				case Face.FLOOR:
					delta2 = GetY0() - origin.y;
					t = delta2 / direction.y;
					break;
					
				case Face.CEILING:
					delta2 = GetY1() - origin.y;
					t = delta2 / direction.y;
					break;
			}
			
			if( t < 0.0f || t > 1.0f ) {
				// intercessão se encontra anterior ou posterior aos pontos
				// que definem a semirreta origin-target
				return;
				
			} else if( float.IsNaN( t ) ) {
				// caso t seja NaN, isso significa que delta2 sobre a cordenada relevante
				// equivale a zero / zero. Nesse caso, a trajetória testada está completamente
				// sobre o plano em questão. Testamos os dois pontos, para t == 0 e t == 1:
				
				if( IsPointWithinFace( origin, face ) ) {				
					interceptions.Add( new RayBoxInterception( 0.0f, origin, worldObjectID ) );
				}
				if( IsPointWithinFace( target, face ) ) {
					interceptions.Add( new RayBoxInterception( 1.0f, target, worldObjectID ) );
				}
				
			} else {
				// caso normal, só precisamos testar o t encontrado
				
				GameVector interceptionPoint = origin + ( target - origin ) * t;
				if( IsPointWithinFace( interceptionPoint, face ) ) {				
					interceptions.Add( new RayBoxInterception( t, interceptionPoint, worldObjectID )  );
				}
			}
		}
		
		protected bool IsPointWithinFace( GameVector interceptionPoint, Face face ) {
			switch( face ) {
				
			case Face.EAST:
			case Face.WEST:
				if( NanoMath.GreaterThan( interceptionPoint.y, GetY1() ) || NanoMath.LessThan( interceptionPoint.y, GetY0() ) ||
				   	NanoMath.GreaterThan( interceptionPoint.z, GetZ1() ) || NanoMath.LessThan( interceptionPoint.z, GetZ0() ) ) {
					return false;
				}
				break;
					
			case Face.NORTH:
			case Face.SOUTH:
				if( NanoMath.GreaterThan( interceptionPoint.y, GetY1() ) || NanoMath.LessThan( interceptionPoint.y, GetY0() ) ||
				   	NanoMath.GreaterThan( interceptionPoint.x, GetX1() ) || NanoMath.LessThan( interceptionPoint.x, GetX0() ) ) {
					return false;
				}
				break;
				
			case Face.FLOOR:
			case Face.CEILING:
				if( NanoMath.GreaterThan( interceptionPoint.z, GetZ1() ) || NanoMath.LessThan( interceptionPoint.z, GetZ0() ) ||
				   	NanoMath.GreaterThan( interceptionPoint.x, GetX1() ) || NanoMath.LessThan( interceptionPoint.x, GetX0() ) ) {
					return false;
					}
				break;
			}

			return true;
		}

		
		protected void MergeInterceptions( List< RayBoxInterception > interceptions ) {
			for( int i = 0; i < interceptions.Count; i++ ) {
				for( int j = i + 1; j < interceptions.Count; j++ ) {
					if( NanoMath.Equals( interceptions[ i ].t, interceptions[ j ].t ) ) {
						interceptions.RemoveAt( j );
						i--;
						break;
					}
				}
			}
		}
	}
	
	public class BoxTestCase : TestCase {
		
		[ UnitTest ]
		public void TestBumpIfIntersects() {
			GameTransform gt1;
			GameTransform gt2;

			Box box1;
			Box box2;
			
			///testa caixa em (0,0,0)-(10,10,10) e outra em (5,5,8)-(7,7,10) sendo empurrada para (5,5,10)-(5,5,12)
			gt1 = new GameTransform();
			gt2 = new GameTransform();
			gt1.position.x = 5;
			gt1.position.y = 5;
			gt1.position.z = 5;
			
			gt1.scale.x = 10;
			gt1.scale.y = 10;
			gt1.scale.z = 10;
			
			box1 = new Box( gt1 );

			
			gt2.position.x = 6;
			gt2.position.y = 6;
			gt2.position.z = 9;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( 6.0f, 6.0f, 11.333333f ), box2.GetGameTransform().position );
			
			///testa caixa em (0,0,0)-(10,10,10) e outra em (5,8,5)-(7,10,7) sendo empurrada para (5,10,5)-(5,12,5)
			gt2.position.x = 6;
			gt2.position.y = 9;
			gt2.position.z = 6;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( 6.0f, 11.33333f, 6.0f ), box2.GetGameTransform().position );
			
			///testa caixa em (0,0,0)-(10,10,10) e outra em (8,5,5)-(10,7,7) sendo empurrada para (10,5,5)-(12,5,5)
			gt2.position.x = 9;
			gt2.position.y = 6;
			gt2.position.z = 6;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( 11.33333f, 6.0f, 6.0f ), box2.GetGameTransform().position );
			///testa caixa em (0,0,0)-(10,10,10) e outra em (5,5,5)-(7,7,7) sendo empurrada para (10,10,10)-(12,12,12)
			gt2.position.x = 6;
			gt2.position.y = 6;
			gt2.position.z = 6;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( 11.33333f, 11.33333f, 11.33333f ), box2.GetGameTransform().position );			

			///testa caixa em (0,0,0)-(10,10,10) e outra em (-1,-1,-1)-(1,1,1) sendo empurrada para (-2,-2,-2)-(0,0,0)
			
			gt2.position.x = 0;
			gt2.position.y = 0;
			gt2.position.z = 0;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( -1.33333f, -1.33333f, -1.33333f ), box2.GetGameTransform().position );			
			
			///testa caixa em (0,0,0)-(10,10,10) e outra em ( 0, 3, 3)-( 2, 5, 5) sendo empurrada para (-2, 3, 3)-( 0, 5, 5)
			gt2.position.x = 1;
			gt2.position.y = 4;
			gt2.position.z = 4;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( -1.33333f,  4.0f,  4.0f ), box2.GetGameTransform().position );			
			
			///testa caixa em (0,0,0)-(10,10,10) e outra em (-1,-1,-1)-(1,1,1) sendo empurrada para (-2,-2,-2)-(0,0,0)
			gt2.position.x = 7;
			gt2.position.y = 4;
			gt2.position.z = 4;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector( 11.33333f,  4.0f,  4.0f ), box2.GetGameTransform().position );			
			///testa caixa em (0,0,0)-(10,10,10) e outra em (-1,-1,-1)-(1,1,1) sendo empurrada para (-2,-2,-2)-(0,0,0)
			gt2.position.x = 5;
			gt2.position.y = 5;
			gt2.position.z = 2;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector(  5.0f,  5.0f,  -1.33333f ), box2.GetGameTransform().position );			
			///testa caixa em (0,0,0)-(10,10,10) e outra em (-1,-1,-1)-(1,1,1) sendo empurrada para (-2,-2,-2)-(0,0,0)
			gt2.position.x = 5;
			gt2.position.y = 2;
			gt2.position.z = 5;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector(  5.0f,  -1.33333f,  5.0f ), box2.GetGameTransform().position );
			///testa caixa em (0,0,0)-(10,10,10) e outra em (-1,-1,-1)-(1,1,1) sendo empurrada para (-2,-2,-2)-(0,0,0)
			gt2.position.x = 5;
			gt2.position.y = 7;
			gt2.position.z = 5;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			box1.BounceBackIfIntersects( box2 );
			Assert.CheckEquals( new GameVector(  5.0f,  11.33333f,  5.0f ), box2.GetGameTransform().position );
		}
		
		[ UnitTest ]
		public void TestBoxWithBoxIntersection() {
			GameTransform gt1;
			GameTransform gt2;
			GameTransform gt3;
			Box box1;
			Box box2;
			Box box3;
			
			gt1 = new GameTransform();
			
			gt1.position.x = 5;
			gt1.position.y = 5;
			gt1.position.z = 5;
			
			gt1.scale.x = 10;
			gt1.scale.y = 10;
			gt1.scale.z = 10;
			
			box1 = new Box( gt1 );
			
			gt2 = new GameTransform();
			
			gt2.position.x = 7;
			gt2.position.y = 7;
			gt2.position.z = 9;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt3 = new GameTransform();
			
			gt3.position.x = 17;
			gt3.position.y = 17;
			gt3.position.z = 19;
			
			gt3.scale.x = 2;
			gt3.scale.y = 2;
			gt3.scale.z = 2;
			
			box3 = new Box( gt3 );
			Assert.CheckEquals( false, box1.Intersects( box3 ) );
			Assert.CheckEquals( false, box3.Intersects( box1 ) );
			Assert.CheckEquals( false, box2.Intersects( box3 ) );
			Assert.CheckEquals( false, box3.Intersects( box2 ) );
			
			gt1.position.x = 5;
			gt1.position.y = 5;
			gt1.position.z = 5;
			
			gt1.scale.x = 10;
			gt1.scale.y = 10;
			gt1.scale.z = 10;
			
			box1 = new Box( gt1 );
			
			
			gt2.position.x = 6;
			gt2.position.y = 6;
			gt2.position.z = 9;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );

			gt2.position.x = 6;
			gt2.position.y = 9;
			gt2.position.z = 6;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 9;
			gt2.position.y = 6;
			gt2.position.z = 6;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 6;
			gt2.position.y = 6;
			gt2.position.z = 6;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 0;
			gt2.position.y = 0;
			gt2.position.z = 0;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 1;
			gt2.position.y = 4;
			gt2.position.z = 4;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 7;
			gt2.position.y = 4;
			gt2.position.z = 4;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 5;
			gt2.position.y = 5;
			gt2.position.z = 2;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 5;
			gt2.position.y = 2;
			gt2.position.z = 5;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			
			gt2.position.x = 5;
			gt2.position.y = 7;
			gt2.position.z = 5;
			
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );
			gt2.position.x = 5;
			gt2.position.y = 12;
			gt2.position.z = 5;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;

			box2 = new Box( gt2 );
			Assert.CheckEquals( false, box1.Intersects( box2 ) );
			Assert.CheckEquals( false, box2.Intersects( box1 ) );
			gt2.position.x = 5;
			gt2.position.y = -5;
			gt2.position.z = 5;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( false, box1.Intersects( box2 ) );
			Assert.CheckEquals( false, box2.Intersects( box1 ) );
			gt2.position.x = 12;
			gt2.position.y = 5;
			gt2.position.z = 5;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( false, box1.Intersects( box2 ) );
			Assert.CheckEquals( false, box2.Intersects( box1 ) );
			gt2.position.x = -5;
			gt2.position.y = 5;
			gt2.position.z = 5;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( false, box1.Intersects( box2 ) );
			Assert.CheckEquals( false, box2.Intersects( box1 ) );			
			gt2.position.x = 5;
			gt2.position.y = 5;
			gt2.position.z = 12;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( false, box1.Intersects( box2 ) );
			Assert.CheckEquals( false, box2.Intersects( box1 ) );
			
			
			gt2.position.x = 5;
			gt2.position.y = 5;
			gt2.position.z = -5;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box2 = new Box( gt2 );
			Assert.CheckEquals( false, box1.Intersects( box2 ) );
			Assert.CheckEquals( false, box2.Intersects( box1 ) );			
			
			gt2.position.x = 5;
			gt2.position.y = 5;
			gt2.position.z = -5;
			gt2.scale.x = 2;
			gt2.scale.y = 2;
			gt2.scale.z = 2;
			
			box1 = new Box( gt2 );
			box2 = new Box( gt2 );
			Assert.CheckEquals( true, box1.Intersects( box2 ) );
			Assert.CheckEquals( true, box2.Intersects( box1 ) );			
			
		}
	}

}