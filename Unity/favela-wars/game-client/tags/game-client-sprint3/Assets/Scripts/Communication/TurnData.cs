using System;

using Utils;

namespace GameCommunication {
	
	public class TurnData {
		
		public int turnNumber;
		public Faction faction;
		
		public TurnData( int turnNumber, Faction faction ) {
			this.turnNumber = turnNumber;
			this.faction = faction;
		}
		
		public TurnData( TurnData turnData ) {
			turnNumber = turnData.turnNumber;
			faction = turnData.faction;
		}
	}
}

