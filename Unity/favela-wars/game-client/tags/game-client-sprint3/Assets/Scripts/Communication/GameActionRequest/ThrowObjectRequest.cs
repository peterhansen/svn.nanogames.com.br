using System;


namespace GameCommunication {
	
	/// <summary>
	/// Essa classe descreve as informações de um pedido feito ao GameManager, para arremessar um objeto.
	/// </summary>
	[ Serializable ]
	public class ThrowObjectRequest : Request {
		
		public ThrowObjectRequest() {
		}
	
	}
}

