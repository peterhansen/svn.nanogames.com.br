using System;


namespace GameCommunication {
	
	[ Serializable() ]
	public class WaitActionData : GameActionData {

		private int waitTime;
		
		
		public WaitActionData( int waitTime ) {
			this.waitTime = waitTime;
		}
		
		
		public int getWaitTime() {
			return waitTime;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as WaitActionData );
		}
		
		
		public bool Equals( WaitActionData data ) {
			return data != null && ( waitTime == data.waitTime );
		}
		
		
		public override int GetHashCode() {
        	return waitTime;
		}
		
	}
}

