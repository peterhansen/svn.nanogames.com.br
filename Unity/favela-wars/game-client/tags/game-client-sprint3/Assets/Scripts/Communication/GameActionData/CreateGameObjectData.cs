using System;
using System.Collections;
using System.Collections.Generic;

namespace GameCommunication {

	
	[Serializable]
	public class CreateGameObjectData : GameActionData {
		
		public int worldObjectID;
		
		public string assetBundleID;
		
		
		public CreateGameObjectData( int worldObjectID, string assetBundleID ) {
			this.worldObjectID = worldObjectID;
			this.assetBundleID = assetBundleID;
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as CreateGameObjectData );
		}
		
		
		public bool Equals( CreateGameObjectData data ) {
			return data != null && worldObjectID == data.worldObjectID && assetBundleID.Equals( data.assetBundleID );
		}
		
		
		public override int GetHashCode() {
        	return ( assetBundleID + worldObjectID ).GetHashCode();
    	}
	}
}

