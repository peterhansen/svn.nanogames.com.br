using System;

using Utils;

namespace GameCommunication {
	
	[ Serializable ]
	public class SpawnAreaMissionData : AreaMissionData {
		
		public int minObjects = 0;
	
		public int capacity = 1;
	
		public Faction faction = Faction.NEUTRAL;
		
		public HashSet< string > allowedClasses = new HashSet< string >();
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as SpawnAreaMissionData );
		}
		
		
		public bool Equals( SpawnAreaMissionData other ) {
			return other != null && minObjects == other.minObjects && capacity == other.capacity &&
				   faction == other.faction && allowedClasses.Equals( other.allowedClasses );
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
			
		
	}
}

