using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using SharpUnit;


namespace GameCommunication {
	
	/// <summary>
	/// Classe abstrata que contém os dados das ações de jogo vindas do GameManager.
	/// </summary>
	[ Serializable ]
	public abstract class GameActionData {
	
		
		public GameActionData () {
		}
		
		
		public static byte[] SerializeData( List< GameActionData > data ) {
			MemoryStream memStream = new MemoryStream();
		    BinaryFormatter bin = new BinaryFormatter();
			foreach ( GameActionData request in data ) {
				bin.Serialize( memStream, request );
			}
			
			return memStream.ToArray();
		}
		
		
		public static List< GameActionData > DeserializeData( byte[] data ) {
			List< GameActionData > list = new List< GameActionData >();
			MemoryStream memStream = new MemoryStream( data );
		    BinaryFormatter bin = new BinaryFormatter();
			
			while ( memStream.Position < memStream.Length )
				list.Add( ( GameActionData ) bin.Deserialize( memStream ) );
			
			return list;
		}
	}
	
	
	#region Classe de testes
		
	public class GameActionDataTester : TestCase {
		
		List< GameActionData > list1;
		List< GameActionData > list2;
			

		public override void SetUp() {
			list1 = new List< GameActionData >() {
				new LogActionData( LogLevel.ERROR, "request de log 1" ),
				new WaitActionData( 2000 ),
				new LogActionData( LogLevel.NORMAL, "request de log 2" ),
				new WaitActionData( 4000 ),
				new LogActionData( LogLevel.WARNING, "request de log 3" ),
			};
		}
			
			
		/// <summary>
		/// Nada para desconstruir
		/// </summary>
		public override void TearDown() {
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		[UnitTest]
		public void SerializeDeserialize() {
			byte[] serializedData = GameActionData.SerializeData( list1 );
			
			list2 = GameActionData.DeserializeData( serializedData );
			
			Assert.True( list1.Count == list2.Count );
			for ( int i = 0; i < list1.Count; ++i ) {
				Assert.CheckEquals( list1[ i ], list2[ i ] );
			}
		}
	}
	
	#endregion
}

