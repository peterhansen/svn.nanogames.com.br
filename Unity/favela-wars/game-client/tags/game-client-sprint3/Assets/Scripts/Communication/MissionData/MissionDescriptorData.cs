using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;

using Utils;

using SharpUnit;


namespace GameCommunication {
	
	[ Serializable ]
	public class MissionDescriptorData {
		
		public string missionName;
		
		public string mapName;
		
		public string sceneName;
		
		public int version;
		
		private static readonly Regex regex = new Regex( @"([a-zA-Z_]+)_(\d+).mission" );
		
		
		public MissionDescriptorData() {
		}
		
		
		public static List< MissionDescriptorData > GetMissionList() {
			List< MissionDescriptorData > missionList = new List< MissionDescriptorData >();
			
			// TODO tentar ler o arquivo da lista de missões
			
			string[] missions = Directory.GetFiles( Path.Combine( System.Environment.CurrentDirectory, "missions" ), Common.MISSION_FILE_EXTENSION );
	
			for ( int i = 0; i < missions.Length; ++i ) {
				string missionWithVersion = Path.GetFileNameWithoutExtension( missions[ i ] );
				
				if ( !regex.IsMatch( missionWithVersion ) )
				    continue;
			}
			
			// TODO gravar em arquivo a lista de missões
			
			return missionList;
		}
		
		
		public string GetMissionFilePath() {
			string dir = Path.Combine( System.Environment.CurrentDirectory, "missions" );
			if ( !Directory.Exists( dir ) )
                Directory.CreateDirectory( dir );
			
			return Path.Combine( dir, missionName + Common.MISSION_FILE_EXTENSION );
		}
		
		
		public void ExportToFile() {
			FileStream file = null;
			try {
				file = new FileStream( GetMissionFilePath(), FileMode.Create );
				BinaryFormatter bin = new BinaryFormatter();
				bin.Serialize( file, this );
			} finally {
				if ( file != null )
					file.Close();
			}
		}
		
		
		public static MissionDescriptorData ImportFromFile( string missionName ) {
			FileStream file = null;
			
			try {
				file = new FileStream( Path.Combine( GetPath(), missionName + ".mission" ), FileMode.Open );
				BinaryFormatter bin = new BinaryFormatter();
				MissionDescriptorData missionData = ( MissionDescriptorData ) bin.Deserialize( file );
				return missionData;
			} catch( System.Exception e ) {
				throw new InvalidMissionFile( e.Message );
			} finally {
				if ( file != null )
					file.Close();
			}
		}		
		
		
		public static string GetPath() {
			return Path.Combine( System.Environment.CurrentDirectory, "missions" );
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as MissionDescriptorData );
		}
		
		
		public bool Equals( MissionDescriptorData data ) {
			return data != null && missionName.Equals( data.missionName ) && sceneName.Equals( data.sceneName ) &&
					mapName.Equals( data.mapName ) && version == data.version;
		}
		
		
		public override int GetHashCode() {
        	return ( missionName + sceneName + mapName + version ).GetHashCode();
		}
		
		
	}
	
	
	#region Mission Generator Test Case
	
	internal class MissionDescriptorDataTestCase : TestCase {
	
		
		public override void SetUp() {
		}
	
		public override void TearDown() {
		}
		
		
		[UnitTest]
		public void CanImportExportedMissionDescriptor() {
			MissionDescriptorData data = new MissionDescriptorData();
			data.mapName = "áéíóú";
			data.sceneName = "ãç²";
			data.missionName = "testando 1 2 3 bla bla";
			data.version = 1234;
			
			data.ExportToFile();
			
			MissionDescriptorData loadedData = MissionDescriptorData.ImportFromFile( data.missionName );
			Assert.True( loadedData.Equals( data ) );
		}
		
	}
	
	#endregion
	
}

