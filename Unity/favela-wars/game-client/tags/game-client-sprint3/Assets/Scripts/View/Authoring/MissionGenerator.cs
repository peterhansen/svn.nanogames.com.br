using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using Object = UnityEngine.Object;
using Debugger = Utils.Debugger;

using UnityEngine;
using UnityEditor;

using SharpUnit;
using GameCommunication;
using Utils;
using GameController;

// TODO: CollisionManager deveria ir para o Common??
using GameModel;


public class MissionGenerator : MonoBehaviour {
	
	// TODO o nome da missÃ£o deve estar posteriormente disponÃ­vel em diversos idiomas
	public string missionName = "";
	
	public string mapName = "";
	
	public string sceneName = "";
	
	protected HashSet< SpawnArea > spawnAreas = new HashSet< SpawnArea >();
	
	protected HashSet< EndMissionCondition > endMissionConditions = new HashSet< EndMissionCondition >();
	
	protected CollisionManager collisionManager;
	
	
	[ MenuItem( "Mission Editor/Export Mission" ) ]
	public static void Execute() {
		try {			
			MissionGenerator mg = ( MissionGenerator ) Object.FindObjectOfType( typeof( MissionGenerator ) );
		
			if ( mg == null ) {
				EditorUtility.DisplayDialog( "Mission Editor", "A cena deve ter um script " + typeof( MissionGenerator ) + " associado a um de seus objetos.", "Ok");
			} else {
				mg.ExportToFile();
				EditorUtility.DisplayDialog( "Mission Editor", "MissÃ£o \"" + mg.missionName + "\" exportada com sucesso.", "Ok" );
			}
		} catch ( Exception e ) {
			Debugger.LogError( e.ToString() );
			EditorUtility.DisplayDialog( "Mission Editor", "Erro ao exportar missÃ£o: " + e.Message, "Ok");
		}
	}
	
	
	// Validate the menu item.
    // The item will be disabled if no transform is selected.
	/// <summary>
	/// Valida o item de menu para exportar missÃ£o, deixando-o desabilitado caso nÃ£o haja um objeto com a tag "Mission"
	/// na cena.
	/// </summary>
	/// <returns>
	/// Um <see cref="System.Boolean"/> indicando se o item de menu pode ser utilizado ou nÃ£o.
	/// </returns>
    [ MenuItem ( "Mission Editor/Export Mission", true ) ]
    public static bool ValidateExportMission() {
		return Object.FindObjectOfType( typeof( MissionGenerator ) ) != null;
    }
	
	
	[ MenuItem( "Mission Editor/Test" ) ]
	public static void Test() {
		Object[] selection = Selection.objects;
		
		if ( selection == null || selection.Length <= 0 ) {
			EditorUtility.DisplayDialog( "Test", "Nenhum objeto selecionado.", "Ok");
		} else {
			Object[] dependencies = EditorUtility.CollectDependencies( selection );
			Debugger.Log( "Scene dependencies( " + dependencies.Length + " )" );
			foreach ( Object obj in dependencies ) {
//				if ( obj is GameObject ) {
					Debugger.Log( "-->" + obj.name + "(" + obj.GetInstanceID() + ") " + obj.GetType() );
//				}
			}
		}
	}
	
	
	public void ExportToFile() {
		LoadSpawnAreas();
		CheckForSpawnAreas();
		
		LoadEndMissionConditions();
		CheckForEndMissionConditions();
		
		ValidateMissionName();
		ValidateMapName();
		ValidateSceneName();
		
		MissionData missionData = new MissionData();
		foreach( SpawnArea sa in spawnAreas ) {
			missionData.spawnAreasData.Add( sa.GetMissionData() );
		}
		
		missionData.endMissionConditions = endMissionConditions;
		
		missionData.missionName = missionName;
		missionData.mapName = mapName;
		missionData.sceneName = sceneName;
		
		GenerateCollisionManager();
		GenerateLevelFile();
		
		missionData.ExportToFile();
	}
	
	 
	protected void LoadSpawnAreas() {
		SpawnArea[] spawnAreasArray = GameObject.FindObjectsOfType( typeof( SpawnArea ) ) as SpawnArea[];
		spawnAreas = new HashSet< SpawnArea >();
		
		foreach( SpawnArea sa in spawnAreasArray ) {
			spawnAreas.Add( sa );
		}
	}
	
	
	protected void LoadEndMissionConditions() {
		EndMissionScript[] endMissionScriptsArray = GameObject.FindObjectsOfType( typeof( EndMissionScript ) ) as EndMissionScript[];
		endMissionConditions = new HashSet< EndMissionCondition >();
		
		foreach( EndMissionScript e in endMissionScriptsArray ) {
			endMissionConditions.Add( e.GetEndMissionCondition() );
		}
	}
	
	
	protected void ValidateMissionName() {
		if ( missionName == null || missionName.Trim().Length <= 0 ) {
			throw new UnnamedMissionException();
		}
	}	
	
	
	protected void ValidateMapName() {
		if ( mapName == null || mapName.Trim().Length <= 0 ) {
			throw new UnnamedMapException();
		}
	}
	
	
	protected void ValidateSceneName() {
		if ( sceneName == null || sceneName.Trim().Length <= 0 ) {
			throw new UnnamedSceneException();
		}
	}

	
	protected void CheckForEndMissionConditions() {
		if ( Object.FindObjectOfType( typeof( EndMissionScript ) ) == null )
			throw new NoEndMissionConditionsException();
	}
		
	
	protected void GenerateCollisionManager() {	
		collisionManager = new CollisionManager();
		WorldManager worldManager = WorldManager.GetInstance();
		GameObject[] sceneElements = GameObject.FindGameObjectsWithTag( Common.Tags.TERRAIN );
		
		for ( int c = 0; c < sceneElements.Length; ++c ) {
			GameObject go = sceneElements[ c ];
			
			if ( go.collider != null && go.collider is BoxCollider ) {
				collisionManager.AddBox( new Box( worldManager.GetWorldID( go ), ControllerUtils.CreateGameVector( go.transform.position ), ControllerUtils.CreateGameVector( go.collider.transform.localScale ) ) );
			} 
		}
	}
	
	
	protected void GenerateLevelFile() {
		collisionManager.SaveMap( missionName );
	}

	
	public override bool Equals( object obj ) {
		return Equals( obj as MissionGenerator );
	}
	
	
	public bool Equals( MissionGenerator mg ) {
		return mg != null && missionName.Equals( mg.missionName ) && mapName.Equals( mg.mapName ) && spawnAreas.Equals( mg.spawnAreas );
	}
	
	
	public override int GetHashCode() {
		return ( mapName + missionName + sceneName ).GetHashCode();
	}
	
	
	protected void CheckForSpawnAreas() {
		if ( spawnAreas.Count == 0 ) {
			throw new SpawnAreasMissingException();
		}
		
		bool policeFactionHasSpawnArea = false;
		bool criminalFactionHasSpawnArea = false;
		
		foreach ( SpawnArea spawnArea in spawnAreas ) {
			if ( spawnArea.GetFaction() == Faction.POLICE ) {
				policeFactionHasSpawnArea = true;
			} else if ( spawnArea.GetFaction() == Faction.CRIMINAL ) {
				criminalFactionHasSpawnArea = true;
			}
		}
		
		if ( !policeFactionHasSpawnArea ) {
			throw new SpawnAreasMissingException( "NÃ£o existe nenhuma Spawn Area para a faccao dos policiais." );
		}
		
		if ( !criminalFactionHasSpawnArea ) {
			throw new SpawnAreasMissingException( "NÃ£o existe nenhuma Spawn Area para a faccao dos traficantes." );
		}
	}
	
}
	
//	#region Mission Generator Test Case
//	internal class MissionGeneratorTestCase : TestCase {
//	
//		protected const string MISSION_NAME = "mission_generator_test";
//		
//		protected GameObject mission;
//		protected GameObject policeArea;
//		protected GameObject criminalArea;
//		
//		protected UnityEngine.Object spawnAreaBOPEPrefab = Resources.Load( "Spawn Area BOPE" );
//		protected UnityEngine.Object spawnAreaNPCPrefab = Resources.Load( "Spawn Area NPC" );
//		protected UnityEngine.Object spawnAreaDrugPrefab = Resources.Load( "Spawn Area Drug" );
//		
//		
//		protected MissionGenerator missionGenerator;
//		
//		
//		public override void SetUp() {
//			mission = (GameObject) GameObject.Instantiate( Resources.Load( "Mission Generator Object" ) );
//			missionGenerator = mission.GetComponent< MissionGenerator >();
//		}
//	
//		public override void TearDown() {
//			GameObject.Destroy( mission );
//		}
//		
//		
//		[UnitTest]
//		public void EndMissionConditionsMissing() {
//			
//			Assert.ExpectException( new NoEndMissionConditionsException() );
//			missionGenerator.ExportToFile();
//		}
//		
//		[UnitTest]
//		public void SpawnAreasMissing() {
//
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			Assert.ExpectException( new SpawnAreasMissingException() );
//			missionGenerator.ExportToFile();
//		}
//		
//		[UnitTest]
//		public void NoSpawnAreasForOneOfTheFactions() {
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			policeArea = GameObject.Instantiate( spawnAreaBOPEPrefab ) as GameObject;
//			
//			Assert.ExpectException( new SpawnAreasMissingException() );
//			try {
//				missionGenerator.ExportToFile();
//			} catch( Exception e ) {
//				throw e;
//			} finally {
//				Destroy( policeArea );
//			}
//		}
//		
//		[UnitTest]
//		public void NamelessMission() {
//			Assert.ExpectException( new UnnamedMissionException() );
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			CreateAndExportMission();
//		}		
//		
//		[UnitTest]
//		public void MissionWithoutMapName() {
//			Assert.ExpectException( new UnnamedMapException() );
//			
//			missionGenerator.missionName = MISSION_NAME;
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			CreateAndExportMission();
//		}
//		
//		[UnitTest]
//		public void MissionWithoutSceneName() {
//			Assert.ExpectException( new UnnamedSceneException() );
//			
//			missionGenerator.missionName = MISSION_NAME;
//			missionGenerator.mapName = MISSION_NAME;
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			
//			CreateAndExportMission();
//		}		
//		
//		[UnitTest]
//		public void CorrectMissionGeneration() {
//			mission.AddComponent( typeof( DestroyAllEnemies ) );
//			missionGenerator.missionName = MISSION_NAME;
//			missionGenerator.mapName = MISSION_NAME;
//			missionGenerator.sceneName = "Test Scene";
//			CreateAndExportMission();
//			
//			Assert.True( File.Exists( missionGenerator.GetMissionFilePath() ), "O arquivo de missÃ£o nÃ£o foi gerado corretamente." );
//			Assert.True( File.Exists( missionGenerator.GetLevelFilePath() ), "O arquivo de level nÃ£o foi gerado corretamente." );
//		}		
//		
//		protected void CreateAndExportMission() {
//			AddingBothFactionAreas();
//			try {
//				missionGenerator.ExportToFile();
//			} catch( Exception e ) {
//				throw e;	
//			} finally {
//				CleaningUpFactionAreas();
//			}
//		}
//
//		protected void AddingBothFactionAreas() {
//			policeArea = GameObject.Instantiate( spawnAreaBOPEPrefab ) as GameObject;
//			criminalArea = GameObject.Instantiate( spawnAreaDrugPrefab ) as GameObject;
//		}
//		
//		protected void CleaningUpFactionAreas() {
//			Destroy( policeArea );
//			Destroy( criminalArea );
//		}
//	}
//	#endregion
//}
//

#region Excecoes
public class SpawnAreasMissingException : Exception {
	
	public SpawnAreasMissingException() {
	}
	
	public SpawnAreasMissingException( string message ) : base( message ) {
	}
	
}

public class NoEndMissionConditionsException : Exception {
	
	public NoEndMissionConditionsException() : this( "Sua missÃ£o deve ter pelo menos uma condiÃ§Ã£o de tÃ©rmino." ) {
	}
	
	
	public NoEndMissionConditionsException( string message ) : base( message ) {
	}
	
}

public class UnnamedMissionException : Exception {
	
	public UnnamedMissionException() : this( "Nome invÃ¡lido da missÃ£o." ) {
	}
	
	
	public UnnamedMissionException( string message ) : base( message ) {
	}
	
}

public class UnnamedMapException : Exception {
	
	public UnnamedMapException() : this( "Nome do mapa invÃ¡lido." ) {
	}
	
	
	public UnnamedMapException( string message ) : base( message ) {
	}
	
}

public class UnnamedSceneException : Exception {
	
	public UnnamedSceneException() : this( "Nome da cena invÃ¡lido." ) {
	}
	
	
	public UnnamedSceneException( string message ) : base( message ) {
	}
	
}
#endregion
