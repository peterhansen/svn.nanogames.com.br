using System.Collections;

using UnityEngine;

using SharpUnit;

using GameController; 
using GameCommunication;


public class Area : MonoBehaviour {
	
	
	public void Start() {
		// TODO deixar visível em modo debug
		if( renderer != null ) {
			renderer.enabled = false;
		}
	}
	
	
	public AreaMissionData GetMissionData() {
		AreaMissionData d = new AreaMissionData();
		d.areaBox = GetGameTransform();
		return d;
	}
	
	
	protected GameTransform GetGameTransform() {
		GameTransform gt = new GameTransform();
		gt.position = ControllerUtils.CreateGameVector( transform.position );
		gt.direction = ControllerUtils.CreateGameVector( transform.forward );
		gt.scale = ControllerUtils.CreateGameVector( transform.localScale );
		
		return gt;
	}
	
	
	#region Area Test Case
	internal class AreaTestCase : TestCase {
		
		[UnitTest]
		public void TransformToGameTransform() {
			
			GameObject area = new GameObject();
			
			float posX = 1.34f;
			float posY = -6.12f;
			float posZ = 100.3333334f;
			float rotX = 20.0f;
			float rotY = -66.0f;
			float rotZ = 0.31f;
			float scaleX = 1.11f;
			float scaleY = 3.0f;
			float scaleZ = -5.0f;
			
			area.transform.position = new Vector3( posX, posY, posZ );
			area.transform.Rotate( new Vector3( rotX, rotY, rotZ ) );
			area.transform.localScale = new Vector3( scaleX, scaleY, scaleZ );
			
			area.AddComponent( typeof( Area ) );
			
			GameTransform gameObjectGT = area.GetComponent< Area >().GetGameTransform();
			
			GameTransform expectedGT = new GameTransform();
			expectedGT.position = new GameVector( posX, posY, posZ );
			expectedGT.direction = ControllerUtils.CreateGameVector( area.transform.forward );
			expectedGT.scale = new GameVector( scaleX, scaleY, scaleZ );
			
			Assert.CheckEquals( expectedGT, gameObjectGT );
		}
	}
	#endregion
}
