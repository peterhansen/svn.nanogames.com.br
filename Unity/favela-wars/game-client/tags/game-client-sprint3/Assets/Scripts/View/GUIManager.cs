using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameController;
using GameCommunication;



public enum GUIElementID {
	GET_TURN_NUMBER_BUTTON
}

public struct InputEventGUI {
	public GUIElementID elementID;
}

public struct InputEventScene {
	public int mouseButton;
	public GameObject gameObject;
	public Vector3 selectionPosition;
}


public class GUIManager : MonoBehaviour {

	protected string missionName = "";
	GameObject selectedCharacter;

	public static InteractionController baseInteractionController = new InteractionController();
	
	public static InteractionController interactionController;

	private static List< GUIRenderer > renderers = new List< GUIRenderer >();
	
	private static GUIRenderer[] renderersArray = new GUIRenderer[ 0 ];
	
	
	public static void CheckSingleton() {
		ControllerUtils.GetInstance( "<GUI Manager>", "GUIManager" );
	}
	
	
	public static void SetInteractionController( InteractionController newController ) {
		if ( newController == null )
			interactionController = baseInteractionController;
		else {
			newController.SetStateFromOther( interactionController );
			interactionController = newController;
			newController.Flush();
		}
	} 
	
	public void Awake() {
		SetInteractionController( null );
	}
	
	public void OnGUI() {
		selectedCharacter = interactionController.GetSelectedCharacter();
		GUI.Label( new Rect( 10, 10, 200, 40 ), selectedCharacter == null? "Nenhum personagem selecionado": selectedCharacter.ToString() ); 
	
		for ( int i = 0; i < renderersArray.Length; ++i ) {
			renderersArray[ i ].OnGUI();
		}
		
		if ( GUI.Button( new Rect( 10, 395, 200, 30 ), "fim de turno" ) ) {
			GameController.GameController.GetInstance().SendRequest( new EndParticipationRequest() );
			GUIManager.ClearAllRenderables();
			SetInteractionController( null );
		}
		
		GUI.Label( new Rect( 10, 420, 200, 40 ), "Turno " + GameController.GameController.GetInstance().GetTurnCount() + " - " + GameController.GameController.GetInstance().GetTurnFaction() );
	}
	
	
	public static void ClearAllRenderables() {
		renderers = new List<GUIRenderer>();
		RefreshGUIRenrederesArray();
	}
	
	
	public void Update() {
		if ( renderers.Count > 0 )
			return;
		
		if ( GameActionRunner.IsBusy() )
			return;

		
		int mouseButton = -1;
		if( Input.GetMouseButtonDown( 0 ) ) {
			mouseButton = 0;
		} else if( Input.GetMouseButtonDown( 1 ) ) {
			mouseButton = 1;
		} else if( Input.GetMouseButtonDown( 2 ) ) {
			mouseButton = 2;
		}		
		
		if( mouseButton > -1 ) {
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hitInfo;
			
			InputEventScene ie = new InputEventScene();
			ie.mouseButton = mouseButton;
			
			if ( Physics.Raycast( ray, out hitInfo ) ) {
				ie.gameObject = hitInfo.transform.gameObject;
				ie.selectionPosition = hitInfo.point;
			} else {
				ie.gameObject = null;
				ie.selectionPosition = Vector3.zero;
			}
			
			interactionController.ReceiveInputEvent( ie );
		}
	}
	
	
	public static void AddGUIRenderer( GUIRenderer r ) {
		renderers.Add( r );
		RefreshGUIRenrederesArray();
	}
	
	
	public static void RemoveGUIRenderer( GUIRenderer r ) {
		renderers.Remove( r );
		RefreshGUIRenrederesArray();
	}
	
	
	private static void RefreshGUIRenrederesArray() {
		renderersArray = renderers.ToArray();
	}
	
}
