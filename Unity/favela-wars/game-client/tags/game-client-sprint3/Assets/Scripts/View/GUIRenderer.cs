using System;

	
/// <summary>
/// Esta interface deve ser implementada por métodos que precisem desenhar algo na tela usando métodos GUI. Objetos que
/// implementem a interface devem ser passados para GUIManager.AddRenderer().
/// </summary>
public interface GUIRenderer {
	
	void OnGUI();
	
}
	

