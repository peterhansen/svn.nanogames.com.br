using UnityEngine;
using System.Collections;
using System;
using Utils;
using GameCommunication;

public class LoadResourceGUIRenderer : GUIRenderer, GameController.TimerScriptListener {
	int percent;
	
	public void OnGUI () {
		GUI.BeginGroup( new Rect( 20.0f, 50.0f, 200.0f, 100.0f ) );
		GUI.Box( new Rect( 0.0f, 0.0f, 200.0f, 100.0f ), "Loading: " + percent + "%" );
		GUI.Button( new Rect( 50.0f, 50.0f, percent, 20.0f ), " " );
		GUI.EndGroup();
	}

	public void Tick( float progress ) {
		percent = ( int ) ( 100.0f * progress );
	}
	
	public void Start() {
		percent = 0;
		GUIManager.AddGUIRenderer( this );
		HideLevel();
	}
	
	public void OnTimerEnded( int timerID ) {
		GUIManager.RemoveGUIRenderer( this );
		ShowLevel();
	}
	
	public void Dismiss() {
		percent = 100;
		TimerScript.AddTimer( 1000, this );
	}
	
	public void ShowLevel() {
		foreach ( GameObject g in GameObject.FindObjectsOfType( typeof( GameObject ) ) as GameObject[] ) {
			g.active = true;
		}
	}
	
	public void HideLevel() {
		foreach ( GameObject g in GameObject.FindObjectsOfType( typeof( GameObject ) ) as GameObject[] ) {
			g.active = false;
		}
	}
}
