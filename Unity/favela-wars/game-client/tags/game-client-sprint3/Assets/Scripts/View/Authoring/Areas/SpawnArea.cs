using UnityEngine;

using System.Collections;

using Utils;
using GameCommunication;


public abstract class SpawnArea : Area {
	
	public int capacity = 1;
	
	public int minObjects = 0;
	
	protected Faction faction = Faction.NEUTRAL;
	
	public HashSet< string > allowedClasses = new HashSet< string >() { "", "", "" };
	
	
	public SpawnArea( Faction faction ) {
		this.faction = faction;
	}
	
	
	public Faction GetFaction() {
		return faction;
	}
	
	
	public new SpawnAreaMissionData GetMissionData() {
		SpawnAreaMissionData d = new SpawnAreaMissionData();
		d.areaBox = GetGameTransform();
		d.faction = faction;
		d.capacity = capacity;
		d.minObjects = minObjects;
		d.allowedClasses = allowedClasses;
		
		return d;
	}
	
	
	public override bool Equals( object obj ) {
		return Equals( obj as SpawnArea );
	}
	
	
	public bool Equals( SpawnArea area ) {
		return area != null && capacity == area.capacity && minObjects == area.minObjects && 
			   faction == area.faction && allowedClasses.Equals( area.allowedClasses );
	}
	
	
	public override int GetHashCode() {
		return ( capacity.ToString() + minObjects.ToString() + faction.ToString() + allowedClasses ).GetHashCode();
	}
	
}
