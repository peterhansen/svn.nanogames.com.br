using UnityEngine;
using System.Collections;

public class EndMissionScript : MonoBehaviour {
	
	protected EndMissionCondition endMissionCondition;
	
	
	public EndMissionCondition GetEndMissionCondition() {
		return endMissionCondition;
	}
	
}
