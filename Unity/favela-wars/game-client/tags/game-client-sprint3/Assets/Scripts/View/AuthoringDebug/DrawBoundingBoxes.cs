using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameModel; //uso GameModel porque este é um behaviour porque é para a autoração.
using GameCommunication;
using GameController;
using Utils;


/// <summary>
/// Desenha as bouding boxes de um dito CollisionManager.
/// </summary>
public class DrawBoundingBoxes : MonoBehaviour {

	#region campos
	public string level;
	
	public bool drawBoundingBoxes = true;
	
	public bool drawAStarNodes = true;
	
	private CollisionManager loader;
	protected AStarGraph graph;
	#endregion

	#region métodos
/// <summary>
/// A pasta passada para o construtor é uma pasta sugerida. Como poderiamos fazer melhor?
/// </summary>
	public void Awake() {
		loader = new CollisionManager( level );
		AStarGraphGenerator wgg = new AStarGraphGenerator( loader );
		graph = wgg.GenerateGraph();
	}	

	
	/// <summary>
	/// 
	/// </summary>
	/// <returns>
	/// A <see cref="CollisionManager"/>
	/// </returns>
	public CollisionManager GetCollisionManager() {
		return loader;
	}
	
	
/// <summary>
/// Pega o level carregado e cria GameObjects para exibir melhor a geometria das bounding boxes.
/// </summary>
	void Start () {
		if ( loader == null ) {
			Debugger.LogError( "Map collision data not found." );
			return;
		}
		
		Box box;
		GameObject cube;
		Material material;
		List< Box > boxes = new List< Box >( loader.GetBoxes() );
		
		if ( drawBoundingBoxes ) {
			///para cada setor, cria um cubo equivalente
			for ( int i = 0; i < boxes.Count; ++i ) {
				box = boxes[ i ];
				GameTransform gt = box.GetGameTransform();
				material = new Material( Shader.Find( "Transparent/Diffuse" ) );
				material.color = new Color( 0.0f, 0.0f, 1.0f, 0.5f );
				
				cube = GameObject.CreatePrimitive( PrimitiveType.Cube );
				cube.name = "Cube " + i;
				cube.transform.position = ControllerUtils.CreateVector3( gt.position );
				cube.transform.localScale = ControllerUtils.CreateVector3( gt.scale );
				cube.renderer.material = material;
			}
		}
	
		if ( drawAStarNodes ) {
			material = new Material( Shader.Find( "Transparent/Diffuse" ) );
			material.color = new Color( 0.0f, 0.0f, 0.8f, 0.5f );
			
			foreach( AStarNode node in graph.GetNodes() ) {
				GameObject nodeGO = GameObject.CreatePrimitive( PrimitiveType.Cube );
				GameVector pos = node.GetPosition();
				nodeGO.name = "Node " + graph.GetNodeIndex( node );
				nodeGO.transform.position = new Vector3( pos.x, pos.y, pos.z );
				nodeGO.transform.localScale = new Vector3( 2.2f, 2.2f, 2.2f );
				nodeGO.renderer.material = material;
			}
		}
	}
	#endregion
}