using UnityEngine;
using System.Collections;
using Utils;
using System;

[ Serializable ]
public class DestroyAllEnemiesScript : EndMissionScript {
	
	public int maxNumberOfTurns = 0;
	public Faction FactionToBeDestroyed;
	
	public DestroyAllEnemiesScript() {
		Init();
	}
	
	
	// Use this for initialization
	void Start() {
		Init();
	}
	
	
	protected void Init() {
		if ( endMissionCondition == null )
			endMissionCondition = new DestroyAllEnemies();
	}
	
	
}
