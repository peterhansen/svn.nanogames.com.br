using UnityEngine;

using System;
using System.Collections;
using GameModel;

[ Serializable ]
public abstract class EndMissionCondition {
	public abstract bool Check( Mission mission );
	public abstract bool Outcome( Mission mission );
}
