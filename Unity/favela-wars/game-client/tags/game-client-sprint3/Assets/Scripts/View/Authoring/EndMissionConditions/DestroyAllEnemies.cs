using System;
using UnityEngine;
using GameModel;
using Utils;

[ Serializable ]
public class DestroyAllEnemies : EndMissionCondition {
	
	/// <summary>
	/// Número máximo de turnos para realizar a missão. 0 (zero) indica que não há limite.
	/// </summary>
	public int maxNumberOfTurns = 0;
	public Faction FactionToBeDestroyed;
	
	public DestroyAllEnemies() : base() {
		FactionToBeDestroyed = Faction.CRIMINAL;
		maxNumberOfTurns = 5;
	}
	
	public override bool Equals( object obj ) {
		return Equals( obj as DestroyAllEnemies );
	}
	
	
	public bool Equals( DestroyAllEnemies d ) {
		return d != null && maxNumberOfTurns == d.maxNumberOfTurns;
	}
	
	
	public override int GetHashCode() {
		return base.GetHashCode();
	}
	
	public override bool Outcome (Mission mission) {
		int policemenCount = 0;
		int drugDealersCount = 0;
		int civilianCount = 0;
		
		foreach ( Character character in mission.GetWorldObjects()  ) {
			
			if ( character.GetHitPoints() <= 0 )
				continue;
			
			switch ( character.GetFaction() ) {
			case Faction.POLICE	:
				++policemenCount;
				break;
			case Faction.NEUTRAL:
				++civilianCount;
				break;
			case Faction.CRIMINAL:
				++drugDealersCount;
				break;
			}
			
		}
		
		bool inTime = ( mission.GetTurnManager().GetTurnData().turnNumber >= maxNumberOfTurns || maxNumberOfTurns == 0 );
		
		return ( policemenCount == 0 && FactionToBeDestroyed == Faction.POLICE && inTime ) || 
			( drugDealersCount == 0 && FactionToBeDestroyed == Faction.CRIMINAL && inTime );
	}
	
	public override bool Check (Mission mission) {
		int policemenCount = 0;
		int drugDealersCount = 0;
		int civilianCount = 0;
		
		foreach ( Character character in mission.GetWorldObjects()  ) {
			
			if ( character.GetHitPoints() <= 0 )
				continue;
			
			switch ( character.GetFaction() ) {
			case Faction.POLICE	:
				++policemenCount;
				break;
			case Faction.NEUTRAL:
				++civilianCount;
				break;
			case Faction.CRIMINAL:
				++drugDealersCount;
				break;
			}
			
		}
		
		return ( policemenCount == 0 && FactionToBeDestroyed == Faction.POLICE) || 
			( drugDealersCount == 0 && FactionToBeDestroyed == Faction.CRIMINAL ) || 
				( mission.GetTurnManager().GetTurnData().turnNumber >= maxNumberOfTurns && maxNumberOfTurns != 0 );
	}
}

