using System;
using UnityEngine;
using GameCommunication;

public class CharacterInfoGUIRenderer : GUIRenderer {
	private GetCharacterInfoData data;
	
	public CharacterInfoGUIRenderer ( GetCharacterInfoData data ) {
		this.data = data;
	}
	
	public void OnGUI () {
		bool shouldRemove = false;
		GUI.BeginGroup( new Rect( 20.0f, 50.0f, 300.0f, 300.0f ) );
		GUI.Box( new Rect( 0.0f, 0.0f, 300.0f, 300.0f ), "info" );
		
		GUI.Label( new Rect( 30.0f, 30.0f, 230.0f, 20.0f ), "nome: " + data.character.GetName() );
		GUI.Label( new Rect( 30.0f, 60.0f, 230.0f, 20.0f ), "facção: " + data.character.GetFaction() );
		GUI.Label( new Rect( 30.0f, 90.0f, 230.0f, 20.0f ), "HP: " + data.character.GetHitPoints() );
		GUI.Label( new Rect( 30.0f, 120.0f, 230.0f, 20.0f ), "AP: " + data.character.GetActionPoints() );
		GUI.Label( new Rect( 30.0f, 150.0f, 230.0f, 20.0f ), "moral: " + data.character.GetMoralPoints() );
		
		
		if ( GUI.Button( new Rect( 150.0f, 240.0f, 100.0f, 50.0f ), "ocultar" ) ) {
			shouldRemove = true;
		}
		
		GUI.EndGroup();
		
		if ( shouldRemove ) {
			GUIManager.RemoveGUIRenderer( this );
		}
	}
}


