using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using SharpUnit;


namespace Utils {
	
	[ Serializable ]
	public class HashSet< T > : ICollection< T >, ISerializable, IDeserializationCallback {
	  
		private readonly Dictionary< T, object > dict;
	
	    public HashSet() {
	        dict = new Dictionary<T, object>();
	    }
		
		
		public HashSet( SerializationInfo info, StreamingContext context ) {
			this.dict = ( Dictionary< T, object > ) info.GetValue( "Dictionary", typeof ( Dictionary< T, object > ) );
		}
		
		
		public void GetObjectData( SerializationInfo info, StreamingContext ctxt ) {
		    info.AddValue( "Dictionary", dict );
		}
		
	
	    public HashSet(IEnumerable<T> items) : this() {
	        if (items == null) {
	            return;
	        }
	
	        foreach (T item in items) {
	            Add(item);
	        }
	    }
		
		
	    public HashSet<T> NullSet { get { return new HashSet<T>(); } }
	
	    #region ICollection<T> Members
	
	    public void Add( T item ) {
	        if ( null == item ) {
	            throw new ArgumentNullException("item");
	        }
	
			if ( !dict.ContainsKey( item ) ) {
				T t = GetObjectByValue( item );
				if ( t == null )
		        	dict[ item ] = null;
			}
	    }
	
	    /// <summary>
	    /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only. </exception>
	    public void Clear() {
	        dict.Clear();
	    }
	
		
	    public bool Contains( T item ) {
			if ( dict.ContainsKey( item ) )
				return true;
			
			// se não conseguiu achar a chave pela referência, tenta pelos valores
			return GetObjectByValue( item ) != null;
	    }
		
		
		protected T GetObjectByValue( T item ) {
			foreach ( T t in this ) {
				if ( t.Equals( item ) )
					return t;
			}
			
			return default( T );
		}
	
		
	    /// <summary>
	    /// Copies the items of the <see cref="T:System.Collections.Generic.ICollection`1"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
	    /// </summary>
	    /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the items copied from <see cref="T:System.Collections.Generic.ICollection`1"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param><param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param><exception cref="T:System.ArgumentNullException"><paramref name="array"/> is null.</exception><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="arrayIndex"/> is less than 0.</exception><exception cref="T:System.ArgumentException"><paramref name="array"/> is multidimensional.-or-<paramref name="arrayIndex"/> is equal to or greater than the length of <paramref name="array"/>.-or-The number of items in the source <see cref="T:System.Collections.Generic.ICollection`1"/> is greater than the available space from <paramref name="arrayIndex"/> to the end of the destination <paramref name="array"/>.-or-Type T cannot be cast automatically to the type of the destination <paramref name="array"/>.</exception>
	    public void CopyTo( T[] array, int arrayIndex ) {
	        if ( array == null )
				throw new ArgumentNullException("array");
			
	        if ( arrayIndex < 0 || arrayIndex >= array.Length || arrayIndex >= Count ) {
	            throw new ArgumentOutOfRangeException("arrayIndex");
	        }
	
	        dict.Keys.CopyTo( array, arrayIndex );
	    }
	
	    /// <summary>
	    /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <returns>
	    /// true if <paramref name="item"/> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false. This method also returns false if <paramref name="item"/> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </returns>
	    /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param><exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.</exception>
	    public bool Remove( T item ) {
			if ( dict.Remove( item ) )
				return true;
			
			T t = GetObjectByValue( item );
			if ( t != null )
	        	return dict.Remove( t );
			
			return false;
	    }
	
		
	    /// <summary>
	    /// Gets the number of items contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <returns>
	    /// The number of items contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </returns>
	    public int Count {
	        get { return dict.Count; }
	    }
	
	    /// <summary>
	    /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
	    /// </summary>
	    /// <returns>
	    /// true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
	    /// </returns>
	    public bool IsReadOnly {
	        get {
	            return false;
	        }
	    }
	
	    #endregion
	
	    public HashSet< T > Union( HashSet< T > set ) {
	        HashSet<T> unionSet = new HashSet<T>(this);
	
	        if (null == set) {
	            return unionSet;
	        }
	
	        foreach ( T item in set ) {
	            if ( unionSet.Contains( item ) ) {
	                continue;
	            }
	
	            unionSet.Add( item );
	        }
	
	        return unionSet;
	    }
	
		
	    public HashSet< T > Subtract( HashSet< T > set ) {
	        HashSet<T> subtractSet = new HashSet<T>(this);
	
	        if (null == set) {
	            return subtractSet;
	        }
	
	        foreach (T item in set) {
	            if (!subtractSet.Contains(item)) {
	                continue;
	            }
	
	            subtractSet.dict.Remove( item );
	        }
	
	        return subtractSet;
	    }
	
	    public bool IsSubsetOf(HashSet<T> set)
	    {
	        HashSet<T> setToCompare = set ?? NullSet;
	
	        foreach ( T item in this ) {
	            if ( !setToCompare.Contains( item ) ) {
	                return false;
	            }
	        }
	
	        return true;
	    }
	
	    public HashSet<T> Intersection(HashSet<T> set) {
	        HashSet<T> intersectionSet = NullSet;
	
	        if ( null == set ) {
	            return intersectionSet;
	        }
	
	        foreach (T item in this) {
	            if ( !set.Contains( item ) ) {
	                continue;
	            }
	
	            intersectionSet.Add(item);
	        }
	
	        foreach (T item in set) {
	            if (!Contains(item) || intersectionSet.Contains(item)) {
	                continue;
	            }
	
	            intersectionSet.Add(item);
	        }
	
	        return intersectionSet;
	    }
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as HashSet< T > );
		}
		
		
		public bool Equals( HashSet< T > other ) {
			if ( other == null )
				return false;
			
			int otherCount = other.Count;
			if ( otherCount != Count ) {
				return false;
			}
			
			// dois HashSets vazios são considerados iguais
			if ( otherCount == 0 ) {
				return true;
			}
			
			T[] otherArray = new T[ otherCount ];
			other.CopyTo( otherArray, 0 );
			
			// primeiro verifica se todos os objetos existem em other
			foreach ( T obj in this ) {
				int initialCount = otherCount;
				for ( int i = 0; i < otherCount; ) {
					if ( obj.Equals( otherArray[ i ] ) ) {
						otherArray[ i ] = otherArray[ otherCount - 1 ];
						--otherCount;
					} else {
						++i;
					}
				}
				
				// se não houve mudança no total de itens, é porque não houve igualdade
				if ( initialCount == otherCount )
					return false;
			}
			
			// o outro HashSet só pode ter itens ainda se for diferente deste
			return otherCount == 0;
	    }
		
		
		public bool EqualsReference( HashSet< T > other ) {
			return other != null && IsSubsetOf( other ) && other.IsSubsetOf( this );
		}
		
		
		public override int GetHashCode() {
			return dict.GetHashCode();
		}
		
		
	    public bool IsProperSubsetOf( HashSet< T > set ) {
	        HashSet<T> setToCompare = set ?? NullSet;
	
	        // A is a proper subset of a if the b is a subset of a and a != b
	        return ( IsSubsetOf( setToCompare ) && !setToCompare.IsSubsetOf( this ) );
	    }
		
		
	    public bool IsSupersetOf( HashSet<T> set ) {
	        HashSet<T> setToCompare = set ?? NullSet;
	
	        foreach (T item in setToCompare)
	        {
	            if (!Contains(item))
	            {
	                return false;
	            }
	        }
	
	        return true;
	    }
		
	
	    public bool IsProperSupersetOf(HashSet<T> set)
	    {
	        HashSet<T> setToCompare = set ?? NullSet;
	
	        // B is a proper superset of a if b is a superset of a and a != b
	        return (IsSupersetOf(setToCompare) && !setToCompare.IsSupersetOf(this));
	    }
	
	    public List<T> ToList()
	    {
	        return new List<T>(this);
	    }
	
	    #region Implementation of ISerializable
	
	    /// <summary>
	    /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
	    /// </summary>
	    /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data. </param><param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization. </param><exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
	//    TODO teste... public void GetObjectData(SerializationInfo info, StreamingContext context)
	//    {
	//        if (info == null) 
	//			throw new ArgumentNullException("info");
	//        dict.GetObjectData(info, context);
	//    }
	
	    #endregion
	
	    #region Implementation of IDeserializationCallback
	
	    /// <summary>
	    /// Runs when the entire object graph has been deserialized.
	    /// </summary>
	    /// <param name="sender">The object that initiated the callback. The functionality for this parameter is not currently implemented. </param>
	    public void OnDeserialization(object sender)
	    {
	        dict.OnDeserialization(sender);
	    }
	
	    #endregion
	 
	    #region Implementation of IEnumerable
	
	    /// <summary>
	    /// Returns an enumerator that iterates through the collection.
	    /// </summary>
	    /// <returns>
	    /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
	    /// </returns>
	    /// <filterpriority>1</filterpriority>
	    public IEnumerator<T> GetEnumerator() {
	        return dict.Keys.GetEnumerator();
	    }
	
	    /// <summary>
	    /// Returns an enumerator that iterates through a collection.
	    /// </summary>
	    /// <returns>
	    /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
	    /// </returns>
	    /// <filterpriority>2</filterpriority>
	    IEnumerator IEnumerable.GetEnumerator() {
	        return GetEnumerator();
	    }
	
	    #endregion
		
		
	}
	
	#region Mission Generator Test Case
	
	internal class HashSetTestCase : TestCase {
		
		HashSet< string > hash1 = new HashSet< string >();
		
		HashSet< string > hash2 = new HashSet< string >();
		
		HashSet< int > hash3 = new HashSet< int >();
		
		
		public override void SetUp() {
			for ( int i = 0; i < 50; ++i ) {
				hash1.Add( i.ToString() );
				hash2.Add( i.ToString() );
				hash3.Add( i );
			}
		}
		
		
		public override void TearDown() {
		}
		
		
		[UnitTest]
		public void CheckRemove() {
			int total = hash1.Count;
			for ( int i = 0; i < total; ++i ) {
				int previous = hash1.Count;
				hash1.Remove( i.ToString() );
				Assert.True( hash1.Count == previous - 1 );
			}
		}
		
		
		[UnitTest]
		public void CheckEquals() {
			Assert.False( hash1.Equals( hash3 ) );
			Assert.True( hash1.Equals( hash1 ) );
			Assert.True( hash1.Equals( hash2 ) );
			Assert.True( hash2.Equals( hash1 ) );
			Assert.True( hash2.Equals( hash2 ) );
			
			hash1.Add( "123" );
			
			Assert.False( hash1.Equals( hash2 ) );
			Assert.False( hash1.Equals( hash3 ) );
			
			Assert.False( hash3.Equals( hash1 ) );
			Assert.False( hash3.Equals( hash2 ) );
			Assert.False( hash2.Equals( hash3 ) );
		}
		
		
		[UnitTest]
		public void CheckUniqueness() {
			Assert.True( hash1.Equals( hash1 ) );
			Assert.True( hash1.Equals( hash2 ) );
			
			int previousCount = hash1.Count;
			
			for ( int i = 0; i < 50; ++i ) {
				hash1.Add( i.ToString() );
			}
			
			Assert.True( previousCount == hash1.Count );
			
			Assert.True( hash1.Equals( hash2 ) );
			Assert.True( hash2.Equals( hash1 ) );
		}
	}
	
	
	#endregion
}