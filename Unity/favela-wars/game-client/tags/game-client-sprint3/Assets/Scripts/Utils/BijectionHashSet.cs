using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using SharpUnit;


namespace Utils {
	
	[ Serializable ]
	public class BijectionHashSet< A, B > {
	  
		private readonly Dictionary< A, B > dict;
		private readonly Dictionary< B, A > dictReverse;
	
	    public BijectionHashSet() {
	        dict = new Dictionary< A, B >();
			dictReverse = new Dictionary< B, A >();
	    }
		
		
		public void Set( A key, B mappedTo ) {
			dict[ key ] = mappedTo;
			dictReverse[ mappedTo ] = key;
		}
		
		public B Get( A key) {
			return dict[ key ];
		}

		public A GetReverse( B mappedTo ) {
			return dictReverse[ mappedTo ];
		}
		
		
		public BijectionHashSet( SerializationInfo info, StreamingContext context ) {
			this.dict = ( Dictionary< A, B > ) info.GetValue( "Dictionary", typeof ( Dictionary< A, B > ) );
			this.dictReverse = ( Dictionary< B, A > ) info.GetValue( "ReverseDictionary", typeof ( Dictionary< B, A > ) );
		}
		
		
		public void GetObjectData( SerializationInfo info, StreamingContext ctxt ) {
			info.AddValue( "Dictionary", dict );
			info.AddValue( "ReverseDictionary", dictReverse );
		}
		
		    
		public void Add( A item ) {
	        if ( null == item ) {
	            throw new ArgumentNullException("item");
	        }
	
			if ( !dict.ContainsKey( item ) ) {
				A t = GetObjectByValue( item );
				if ( t == null )
		        	dict[ item ] = default( B );
			}
	    }
		
	    public BijectionHashSet(IEnumerable<A> items) : this() {
	        if (items == null) {
	            return;
	        }
	
	        foreach (A item in items) {
	            Add(item);
	        }
	    }
		
		public BijectionHashSet< A, B > NullSet { get { return new BijectionHashSet< A, B >(); } }
	
		
		
	    /// <summary>
	    /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only. </exception>
	    public void Clear() {
	        dict.Clear();
			dictReverse.Clear();
	    }
	
		
	    public bool Contains( A item ) {
			
			if ( dict.ContainsKey( item ) )
				return true;
			
			return false;
	    }

		
		protected A GetObjectByValue( A item ) {
			foreach ( object t in dict ) {
				if ( t.Equals( item ) )
					return (A)t;
			}
			
			return default( A );
		}
		
		protected B GetValueByValue( B obj ) {
			foreach ( object o in dictReverse ) {
				if ( o.Equals( obj ) )
					return (B)o;
			}
			
			return default( B );
		}
		

		
	    /// <summary>
	    /// Gets the number of items contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </summary>
	    /// <returns>
	    /// The number of items contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
	    /// </returns>
	    public int Count {
	        get { return dict.Count; }
	    }
	
	    /// <summary>
	    /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
	    /// </summary>
	    /// <returns>
	    /// true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
	    /// </returns>
	    public bool IsReadOnly {
	        get {
	            return false;
	        }
	    }

		public override int GetHashCode() {
			return dict.GetHashCode();
		}
		

	}
	
		
	#region Test Case
	
	internal class BijectionHashSetTestCase : TestCase {
				
		BijectionHashSet< int, int > hash2 = new BijectionHashSet< int, int >();
		
		
		public override void SetUp() {
			for ( int i = 0; i < 50; ++i ) {				
				hash2.Add( i );
			}
			
			
			for ( int i = 0; i < 25; ++i ) {
				hash2.Set( i, i + 25 );
			}

		}
		
		
		public override void TearDown() {
		}
		
		
		[UnitTest]
		public void CheckIsConsistent() {
			for ( int i = 0; i < 25; ++i ) {
				Assert.CheckEquals( i + 25, hash2.Get( i ) );
			}
		}
	}
	#endregion
}

