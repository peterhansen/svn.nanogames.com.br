using System;


namespace Utils {
	
	public static class NanoRandom {
		
		private static Random r = new Random();
		
		
		public static float NextFloat( float min, float max ) {
			return min + ( float ) ( r.NextDouble() * ( max - min ) );
		}
		
	}
}

