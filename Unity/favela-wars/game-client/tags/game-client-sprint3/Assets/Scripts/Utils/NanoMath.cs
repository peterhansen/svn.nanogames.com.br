using System;
using GameCommunication;
using Utils;


namespace Utils {
	
	public class NanoMath {
		
		public const float EPSILON = 0.0001f;
		
		
		public static bool Equals( float a, float b ) {
			return (float) ( Math.Abs( a - b ) ) < EPSILON;
		}
		
		
		public static bool GreaterThan( float lhs, float rhs ) {
			return !Equals( lhs, rhs ) && lhs > rhs;
		}
		
		
		public static bool LessThan( float lhs, float rhs ) {
			return !Equals( lhs, rhs ) && lhs < rhs;
		}
		
		
		public static bool GreaterEquals( float lhs, float rhs ) {
			return Equals( lhs, rhs ) || lhs > rhs;
		}
		
		
		public static bool LessEquals( float lhs, float rhs ) {
			return Equals( lhs, rhs ) || lhs < rhs;
		}
		
		
		public static bool Equals( float a, float b, float epsilon ) {
			return (float) ( Math.Abs( a - b ) ) < epsilon;
		}		
		
		
		public static bool IsInLine( GameVector origin, GameVector point, GameVector destiny ) {
			GameVector bigger = destiny.Sub( origin );
			GameVector candidate = point.Sub( origin );
			
			float rel = ( candidate.Norm() / bigger.Norm() );
			
			if ( rel > 1.0f )
				return false;
			
			bigger.Normalize();
			candidate.Normalize();
			
			return bigger.Equals( candidate, 0.01f );
		}
	}
}

