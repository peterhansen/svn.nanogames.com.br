using System;

namespace Utils {
	
	public enum Faction {
		NEUTRAL,
		POLICE,
		CRIMINAL
	}
	
	public enum Gender {
		MALE,
		FEMALE
	}
	
	
	public enum Stance {
		STANDING,
		CROUCHING,
	}
	
	
	public enum Scene {
		CHOOSE_MISSION,
		PLAY_MISSION,
	}
	
	public class Common {
		
		public const string LEVEL_FILE_EXTENSION = ".level";
		public const string LEVEL_DUMP_FILE_EXTENSION = ".dump";
		public const string MISSION_FILE_EXTENSION = ".mission";
		
		public const string PATH_LEVEL_FILE = "missions/";
		public const string PATH_TEMP_FOLDER = "tmp/";
		
		/// <summary>
		/// Nome do GameObjet contêiner dos objetos de script da missão. 
		/// </summary>
		public const string MISSION_HOLDER_OBJECT_NAME = "<Mission>";
		
		public const int NULL_WORLDOBJECT = -1;
		
		public static readonly Faction[] PlayerFactions = new Faction[] { Faction.POLICE, Faction.CRIMINAL };
		
		/// <summary>
		/// Altura considerada de cada andar (camada), em metros.
		/// </summary>
		public const float LEVEL_LAYER_HEIGHT = 3.0f;
		
		
		internal class Layers {
			public static string SCENERY = "scenery";
		}
		
		internal class Tags {
			public static string TERRAIN = "Terrain";
			public static string CHARACTER = "Character";
			public static string MISSION = "Mission";
			public static string HOLDER = "Holder";
		}
		
		internal class ActionTypes {
			public const string HIDE_MENU = "HIDE_MENU";
			public const string ROOT = "ROOT";
			public const string MOVE = "MOVE";
			public const string SHOOT = "SHOOT";
			public const string USE_ITEM = "USE_ITEM";
			public const string THROW = "THROW";
			public const string GET_INFO = "GET_INFO";
			public const string RELOAD = "RELOAD";
			public const string SET_STANCE = "SET_STANCE";
		}
		
		internal class Character {
			public const float RADIUS = 1.0f;
			public const float KNEE_HEIGHT = 0.3f;
		}
	}
}

