using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Utils;
using GameModel;


namespace GameController {
	
	public class WorldManager {
		
		/// <summary>
		/// Dicionário que mapeia GameObjectIds (presentes nos WorldObjects) em GameObjects.
		/// </summary>
		protected BijectionHashSet< int, GameObject > gameObjectsForWorldIds;
		
		protected static WorldManager instance;
		
		
		protected WorldManager() {
			gameObjectsForWorldIds = new BijectionHashSet< int, GameObject >();
		}
		
		
		public static WorldManager GetInstance() {
			if ( instance == null )
				instance = new WorldManager();
			
			return instance;
		}
		
		
		public static void Unload() {
			instance = null;
		}
		
		
		public void AddWorldObject( int worldObjectID, string assetBundleID ) {
			GameObject gameObject = ( GameObject ) GameObject.CreatePrimitive( PrimitiveType.Cube );
			gameObject.name = "GameObject #" + worldObjectID + " (" + assetBundleID + ")";
			
			// TODO temporariamente, o assetbundleID indica a cor do elemento no mapa
			uint color = 0x0000ffff;
			try {
				color = UInt32.Parse( assetBundleID, System.Globalization.NumberStyles.HexNumber );
			} catch ( Exception ) {
			}
			
			gameObject.renderer.material.color = ControllerUtils.GetColor( color );
			gameObject.tag = "Character";
			gameObject.AddComponent( "PathFollowerScript" );
			gameObject.transform.parent = ControllerUtils.GetHolder( "<Characters> " ).transform;
			
			gameObjectsForWorldIds.Set( worldObjectID, gameObject );
		}
		
		
		/// <summary>
		/// Obtém um objeto do jogo a partir de seu ID no mundo. 
		/// </summary>
		/// <param name="worldObjectID">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <returns>
		/// O GameObject associado, ou null caso não seja encontrado.
		/// </returns>
		public GameObject GetGameObject( int worldObjectID ) {
			try {
				return gameObjectsForWorldIds.Get( worldObjectID );
			} catch ( Exception ) { 
				return null;
			}
		}
		
		
		public int GetWorldID( GameObject gameObject ) {
			try {
				return gameObjectsForWorldIds.GetReverse( gameObject );
			} catch ( Exception ) {
				return Common.NULL_WORLDOBJECT;
			}
		}
	}
}

