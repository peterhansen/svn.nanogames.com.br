using System;
using UnityEngine;
using Utils;
using GameCommunication;
using Action = GameCommunication.Action;

namespace GameController
{
	public class MoveInteractionController : InteractionController {
		
		public MoveInteractionController() {
			state = InteractionControllerState.WAITING_FOR_INTERACTION;
		}

		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			base.ReceiveInputEvent( inputEvent );
			
			switch ( state ) {	
				
				case InteractionControllerState.CHARACTER_FROM_PLAYER_TEAM_SELECTED:
					
					switch( inputEvent.mouseButton ) {
						case 0:
							if ( inputEvent.gameObject != null && !inputEvent.gameObject.CompareTag( Common.Tags.CHARACTER ) ) { // TODO passar tags usadas para Common
								// TODO: fazer com que o movimento nÃ£o possa ser realizado caso o personagem
								//state = InteractionControllerState.CHARACTER_ATTEMPTING_MOVEMENT;
								
								GameVector pos = new GameVector( inputEvent.selectionPosition.x, inputEvent.selectionPosition.y + Common.Character.KNEE_HEIGHT, inputEvent.selectionPosition.z );
								
								int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
		
								GameController.GetInstance().SendRequest( new MoveCharacterRequest( characterID, pos ) );
							} 
						break;
					}
				break;
			}
		}
	}
}

