using System;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class StartMissionRunner : GameActionRunner, TimerScriptListener {
		
		private StartMissionData data;
		
		private AsyncOperation load;
		
		float startTime;
		
		
		public StartMissionRunner( StartMissionData data ) : base( true ) {
			this.data = data;
		}
		
		
		protected override void Execute() {
			Debugger.Log( "Loading level " + data.sceneName + "..." );
			
			startTime = Time.realtimeSinceStartup;
			WorldManager.Unload();
			
			GameController.GetInstance().SetTurnData( data.turnData );
			
			load = Application.LoadLevelAsync( data.sceneName );
			startTime = Time.realtimeSinceStartup;
			TimerScript.AddTimer( 100, this );
		}
		
		
		public void OnTimerEnded( int timerID ) {
			if ( load.progress < 1.0 ) {
				Debugger.Log( "Load Level progress: " + load.progress );
				TimerScript.AddTimer( 100, this );
				StartMissionRequest.loadingBar.Tick( load.progress );
			} else {
				Debugger.Log( "Level " + Application.loadedLevel + " loaded in " + ( Time.realtimeSinceStartup - startTime ) + " seconds." );
				StartMissionRequest.loadingBar.Dismiss();
				// garante que todos os singletons necessários à execução da missão estejam carregados
				TimerScript.CheckSingleton();
				GUIManager.CheckSingleton();
				
				// garante que objetos de missão estejam na pasta correta durante a execução
				GameObject missionHolder = ControllerUtils.GetMissionHolderObject();
				GameObject[] missionObjects = GameObject.FindGameObjectsWithTag( Common.Tags.MISSION );
				foreach ( GameObject o in missionObjects )
					o.transform.parent = missionHolder.transform;
				
				OnDone();
			}
		}
		
		
	}
}

