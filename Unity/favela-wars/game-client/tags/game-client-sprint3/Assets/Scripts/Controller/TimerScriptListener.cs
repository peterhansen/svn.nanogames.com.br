using System;

namespace GameController {

	public interface TimerScriptListener {
		
		void OnTimerEnded( int timerID );
		
	}

}

