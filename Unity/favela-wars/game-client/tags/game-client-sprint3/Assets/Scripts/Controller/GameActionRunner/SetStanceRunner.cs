using System;
using UnityEngine;
using GameCommunication;
using Utils;

namespace GameController {
	public class SetStanceRunner : GameActionRunner {
		private SetStanceData data;
		
	
		public SetStanceRunner( SetStanceData setStanceData ) : base( false ) {
			this.data = setStanceData;
		}
		
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.characterWorldID );
			
			//Debugger.Log( "MoveCharacterRunner.Execute: " + data.characterWorldID );
			if ( gameObject == null ) {
				Debugger.LogError( "MoveCharacterRunner: worldObjectID not found: " + data.characterWorldID );
			} 
		}
	}
}