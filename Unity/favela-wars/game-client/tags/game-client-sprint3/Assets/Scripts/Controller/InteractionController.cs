using UnityEngine;
using SharpUnit;
using System.Collections.Generic;
using Utils;
using GameCommunication;


namespace GameController {
	
	public enum InteractionControllerState {
		WAITING_FOR_INTERACTION,
		CHARACTER_FROM_PLAYER_TEAM_SELECTED,
		CHARACTER_FROM_ENEMY_TEAM_SELECTED,
		CHARACTER_ATTEMPTING_ACTION
	};
	
	public class InteractionController {
		
		protected InteractionControllerState state;
		protected GameObject selectedCharacter;
		protected GameObject selectedTarget;
		
		public void SetStateFromOther( InteractionController other ) {
			
			if ( other == null )
				return;
			
			SetState( other.GetState() );
			SelectCharacter( other.GetSelectedCharacter() );
			SelectTarget( other.GetSelectedTarget() );
		}
		
		
		public InteractionController() {
			state = InteractionControllerState.WAITING_FOR_INTERACTION;
		}
		
		
		public GameObject GetSelectedCharacter() {
			return selectedCharacter;
		}
		
		public GameObject GetSelectedTarget() {
			return selectedTarget;
		}
		
		public void SelectCharacter( GameObject go ) {
			selectedCharacter = go;
		}
		
		public void SelectTarget( GameObject go ) {
			selectedTarget = go;
		}

		
		public InteractionControllerState GetState() {
			return state;
		}
		
		protected void SetState( InteractionControllerState newState ) {
			state = newState;
		}
		
		
		public void ReceiveInputEvent( InputEventGUI inputEvent ) {
		}
		
		public virtual void Flush() {
			
		}
		
			       
		public virtual void ReceiveInputEvent( InputEventScene inputEvent ) {

			switch ( state ) {	
				case InteractionControllerState.WAITING_FOR_INTERACTION:
					if ( inputEvent.gameObject != null && inputEvent.gameObject.CompareTag( Common.Tags.CHARACTER ) ) {
						selectedCharacter = inputEvent.gameObject;
						state = InteractionControllerState.CHARACTER_FROM_PLAYER_TEAM_SELECTED;
						GameController.GetInstance().SendRequest( new GetCharacterActionsRequest( WorldManager.GetInstance().GetWorldID( selectedCharacter ) ) );
					}
				break;
				
				case InteractionControllerState.CHARACTER_FROM_PLAYER_TEAM_SELECTED:
					
					switch( inputEvent.mouseButton ) {
						case 0:
							if ( inputEvent.gameObject == null ) {
								selectedCharacter = inputEvent.gameObject;
								state = InteractionControllerState.WAITING_FOR_INTERACTION;
							} else if ( inputEvent.gameObject.CompareTag( Common.Tags.CHARACTER ) ) {
								selectedCharacter = inputEvent.gameObject;
								GameController.GetInstance().SendRequest( new GetCharacterActionsRequest( WorldManager.GetInstance().GetWorldID( selectedCharacter ) ) );
							}
						break;
					}
				break;
			}
		}
	}

	
	#region Interaction Controller Test Case
	public class InteractionControllerTestCase : TestCase {
		
		protected GameObject character1;
		protected GameObject character2;
		protected GameObject terrain;
		protected InteractionController interactionController;
			
		
		override public void SetUp() {
			List< int > list = new List<int>();
			List< uint > list2 = new List<uint>();
			list.Add( 0 );
			list2.Add( 0 );
			list2.Add( 1 );
			GameModel.Mission.CreateInstance( list, "test_auto", list2 );
			interactionController = new InteractionController();
			character1 = new GameObject( "Character 1" );
			character1.tag = Common.Tags.CHARACTER;
			character2 = new GameObject( "Character 2" );
			character2.tag = Common.Tags.CHARACTER;
			terrain = new GameObject( "Terrain" );
			terrain.tag = Common.Tags.TERRAIN;
		}
		
		
		override public void TearDown() {
			GameObject.Destroy( character1 );
			GameObject.Destroy( character2 );
			GameObject.Destroy( terrain );
		}
		
		
		[UnitTest]
		public void InteractionControllerStartingState() {
			Assert.True( interactionController.GetState() == InteractionControllerState.WAITING_FOR_INTERACTION );
		}
			
		[UnitTest]
		public void SelectingCharacter() {
			InputEventScene ie;
			
			ie.gameObject = character1;
			ie.selectionPosition = new Vector3( 0, 0, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			Assert.True( interactionController.GetState() == InteractionControllerState.CHARACTER_FROM_PLAYER_TEAM_SELECTED );
			Assert.True( interactionController.GetSelectedCharacter() == character1 );
		}
		
		[UnitTest]
		public void ChangingCharacter() {
			InputEventScene ie;
			
			ie.gameObject = character1;
			ie.selectionPosition = new Vector3( 0, 0, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			ie.gameObject = character2;
			ie.selectionPosition = new Vector3( 0, 0, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			Assert.True( interactionController.GetState() == InteractionControllerState.CHARACTER_FROM_PLAYER_TEAM_SELECTED );
			Assert.True( interactionController.GetSelectedCharacter() == character2 );
		}
		
		[UnitTest]
		public void SelectingNullCharacter() {
			InputEventScene ie;
			
			ie.gameObject = null;
			ie.selectionPosition = new Vector3( 0, 0, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			Assert.True( interactionController.GetState() == InteractionControllerState.WAITING_FOR_INTERACTION );
		}
		
		[UnitTest]
		public void DeselectingCharacter() {
			InputEventScene ie;
			
			ie.gameObject = character1;
			ie.selectionPosition = new Vector3( 0, 0, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			ie.gameObject = null;
			ie.selectionPosition = new Vector3( 0, 0, 0 );
			interactionController.ReceiveInputEvent( ie );
			
			Assert.True( interactionController.GetState() == InteractionControllerState.WAITING_FOR_INTERACTION );
			Assert.True( interactionController.GetSelectedCharacter() == null );
		}
		
		//[UnitTest]
		// TODO: reativar para testar que a interface entre em modo de "espera"
		// enquanto o personagem esteja atirando
		public void AttemptingMovementWithCharacter() {
			InputEventScene ie;
			
			ie.gameObject = character1;
			ie.selectionPosition = new Vector3( 0, -1.8f, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			ie.gameObject = terrain;
			ie.selectionPosition = new Vector3( 4.7f, -1.8f, 2.158286f );

			interactionController.ReceiveInputEvent( ie );
			
			Assert.True( interactionController.GetState() == InteractionControllerState.CHARACTER_ATTEMPTING_ACTION );
		}
		
		// TODO: precisamos alterar o estado para esperar um tiro
		//[UnitTest]
		public void AttemptingAShot() {
			InputEventScene ie;
			
			// selecting character
			ie.gameObject = character1;
			ie.selectionPosition = new Vector3( 0, -1.8f, 0 );
			ie.mouseButton = 0;
			interactionController.ReceiveInputEvent( ie );
			
			// shooting at floor
			ie.gameObject = terrain;
			ie.mouseButton = 2;
			ie.selectionPosition = new Vector3( 4.7f, -1.8f, 2.158286f );

			interactionController.ReceiveInputEvent( ie );
			
			Assert.True( interactionController.GetState() == InteractionControllerState.CHARACTER_ATTEMPTING_ACTION );
		}
	}
	
	#endregion
	
}
