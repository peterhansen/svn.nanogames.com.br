using System;
using UnityEngine;
using Utils;
using GameCommunication;


namespace GameController {
	
	/// <summary>
	/// 
	/// </summary>
	public class SetWorldObjectTransformRunner : GameActionRunner {
		
		private SetWorldObjectTransformData data;
		
	
		public SetWorldObjectTransformRunner( SetWorldObjectTransformData transformData ) : base( false ) {
			this.data = transformData;
		}
		
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.worldObjectID );
			
			//Logger.Log( "SetWorldObjectTransformRunner.Execute: " + data.worldObjectID );
			if ( gameObject == null ) {
				Debugger.LogError( "SetWorldObjectTransformRunner: worldObjectID not found: " + data.worldObjectID );
			} else {
				gameObject.transform.localScale = new Vector3( data.scale.x, data.scale.y, data.scale.y );
				gameObject.transform.position = new Vector3( data.position.x, data.position.y, data.position.z );
				gameObject.transform.localEulerAngles = new Vector3( data.angle.x, data.angle.y, data.angle.z );
			}
		}
		
		
	}
}

