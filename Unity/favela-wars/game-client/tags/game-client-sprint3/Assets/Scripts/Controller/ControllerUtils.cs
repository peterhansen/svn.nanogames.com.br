using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Object = UnityEngine.Object;

using GameCommunication;
using Utils;


namespace GameController {
	
	public static class ControllerUtils {
		
		public static Color GetColor( uint color ) {
			return new Color( ( ( color & 0xff000000 ) >> 24 ) / 255.0f,
			                  ( ( color & 0x00ff0000 ) >> 16 ) / 255.0f, 
		                      ( ( color & 0x0000ff00 ) >>  8 ) / 255.0f,
			                    ( color & 0x000000ff )         / 255.0f );
		}
		
		public static GameVector CreateGameVector( Vector3 original ) {
			return new GameVector( original.x, original.y, original.z );
		}
		
		public static Vector3 CreateVector3( GameVector original ) {
			return new Vector3( original.x, original.y, original.z );
		}
		
		
		/// <summary>
		/// Obtém a instância de um singleton da cena, mas NÃO o cria caso não exista. 
		/// </summary>
		/// <param name="instanceName">
		/// O nome do GameObject.
		/// </param>
		/// <returns>
		/// A instância do GameObject, ou null caso não exista.
		/// </returns>
		public static GameObject GetInstance( string instanceName ) {
			return GameObject.Find( instanceName );
		}
		
		
		/// <summary>
		/// Retorna a instância de um GameObject singleton da cena. Caso ela não exista, é criada e retornada.
		/// </summary>
		/// <param name="instanceName">
		/// O nome do GameObject na cena. Esse parâmetro é usado como identificação pela Unity para se obter a instância.
		/// </param>
		/// <param name="componentClassName">
		/// O nome da classe de componente a adicionar ao GameObject, caso ele precise ser criado. Se a instância
		/// já existir, não é verificada a consistência entre a lista de componentes do objeto e o componente passado por
		/// parâmetro.
		/// </param>
		/// <returns>
		/// A instância do GameObject.
		/// </returns>
		public static GameObject GetInstance( string instanceName, string componentClassName ) {
			return GetInstance( instanceName, new List< string >() { componentClassName } );
		}
		
		
		/// <summary>
		/// Retorna a instância de um GameObject singleton da cena. Caso ela não exista, é criada e retornada.
		/// </summary>
		/// <param name="instanceName">
		/// O nome do GameObject na cena. Esse parâmetro é usado como identificação pela Unity para se obter a instância.
		/// </param>
		/// <param name="componentsClassNames">
		/// O(s) nome(s) da(s) classe(s) de componente a adicionar ao GameObject, caso ele precise ser criado. Se a instância
		/// já existir, não é verificada a consistência entre a lista de componentes passados por parâmetro e os componentes
		/// do objeto.
		/// </param>
		/// <returns>
		/// A instância do GameObject.
		/// </returns>
		public static GameObject GetInstance( string instanceName, List< string > componentsClassNames ) {
			GameObject instance = GameObject.Find( instanceName );
			
			if ( instance == null ) {
				instance = GameObject.CreatePrimitive( PrimitiveType.Cube );
				instance.name = instanceName;
				instance.renderer.enabled = false;
				instance.tag = Common.Tags.MISSION;
				foreach ( string className in componentsClassNames )
					instance.AddComponent( className );
				Object.DontDestroyOnLoad( instance );
				
				instance.transform.parent = GetMissionHolderObject().transform;
			}	
			
			return instance;
		}
		
		
		/// <summary>
		/// Obtém o filho de um GameObject pelo seu nome. 
		/// </summary>
		/// <param name="parent">
		/// A <see cref="GameObject"/>
		/// </param>
		/// <param name="name">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="GameObject"/>
		/// </returns>
		public static GameObject FindChild( GameObject parent, string name ) {
			Transform transformChild = parent.transform.Find( name );
			if ( transformChild != null )
		   		return transformChild.gameObject;
			
			return null;
		}
		
		
		/// <summary>
		/// Obtém (criando, caso necessário) a instância do GameObject que serve de contêiner para os objetos de missão. 
		/// </summary>
		/// <returns>
		/// A instância do contêiner para os objetos de missão.
		/// </returns>
		public static GameObject GetMissionHolderObject() {
			return GetHolder( Common.MISSION_HOLDER_OBJECT_NAME );
		}
		
		
		public static GameObject GetHolder( string name ) {
			GameObject instance = GameObject.Find( name );
			
			if ( instance == null ) {
				instance = GameObject.CreatePrimitive( PrimitiveType.Cube );
				instance.name = name;
				instance.renderer.enabled = false;
				instance.tag = Common.Tags.HOLDER;
				Object.DontDestroyOnLoad( instance );
			}	
			
			return instance;			
		}
		
	}
}

