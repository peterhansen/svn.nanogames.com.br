using System;
using System.Collections.Generic;
using System.Collections;

using UnityEngine;

using Utils;
using GameCommunication;


namespace GameController {
	
	public class ShootRunner : GameActionRunner {
		
		private ShootData data;
		
	
		public ShootRunner( ShootData shootData ) : base( true ) {
			this.data = shootData;
		}
		
		
		protected override void Execute() {
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.shooterWorldID );
			// TODO obter informações da arma e munição do soldado ao atirar (visualização do projétil, emissão de partículas, etc.)
			
			//Debugger.Log( "ShootRunner.Execute: " + data.shooterWorldID );
			if ( gameObject == null ) {
				Debugger.LogError( "ShootRunner: shooterWorldID not found: " + data.shooterWorldID );
			} else {
				gameObject.transform.LookAt( ControllerUtils.CreateVector3( data.hitPoint ) );
				ProjectileScript.Shoot( data.shotOrigin, data.hitPoint, 45.0f, this );
			}
		}
		
	}
}

