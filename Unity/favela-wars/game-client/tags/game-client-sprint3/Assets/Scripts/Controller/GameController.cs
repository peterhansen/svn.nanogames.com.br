using System;
using SharpUnit;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using GameCommunication;
using Utils;


// if singleplayer
using GameModel;
// endif


namespace GameController {
	
	public class GameController {
		
		/// <summary>
		/// Instância do GameController (singleton).
		/// </summary>
		private static GameController instance;
		
		protected int localPlayer;
	
		protected TurnData turnData;
			
		/// <summary>
		/// Construtor - apenas inicializa a classe de modo que, em modo debug, ela não faça nada mas avise que foi 
		/// usada de forma não inicializada. Em modo release, ele não deve fazer nada.
		/// </summary>
		private GameController() {
		}
		
		
		/// <summary>
		/// Obtém a instância do GameController, criando-a caso necessário.
		/// </summary>
		/// <returns>
		/// A <see cref="GameController"/>
		/// </returns>
		public static GameController GetInstance() {
			if ( instance == null ) {
				instance = new GameController();
			}
			
			return instance;
		}
				
		
		public void SetTurnData( TurnData turnData ) {
			
			// TODO: por enquanto, estou fingindo que sou outro jogador
			localPlayer = turnData.faction == Faction.POLICE? 0: 1;
			
			this.turnData = new TurnData( turnData );
		}
		
		
		public int GetTurnCount() {
			return turnData.turnNumber;
		}
		
		
		public Faction GetTurnFaction() {
			return turnData.faction;
		}
		
		
		/// <summary>
		/// Descarrega a instância do GameController.
		/// </summary>
		public static void Unload() {
			instance = null;
		}
		
		public void ReceiveResponse( Response response ) {
			GameActionRunner.ProcessRequests( response.GetGameActionsData() );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="d">
		/// A <see cref="Dictionary<System.Int16, System.Object>"/>
		/// </param>
		public void MoveCharacter( Dictionary< short, object > d ) {
			foreach ( short key in d.Keys ) {
				Debugger.Log( key + " -> " + d[ key ] );
			}
		}
		
		
		public void SetLocalPlayer( int id ) {
			this.localPlayer = id;
		}
		
		/// <summary>
		/// Envia requisição para o modelo de jogo
		/// </summary>
		public void SendRequest( Request request ) {
			request.setPlayerID( localPlayer ); // TODO playerID da request por enquanto é sempre 0
			GameManager.GetInstance().PushRequest( request );
		}
		
	}
	
	
	#region Classe de testes
		
		public class GameControllerTester : TestCase {
		}
	#endregion
}

