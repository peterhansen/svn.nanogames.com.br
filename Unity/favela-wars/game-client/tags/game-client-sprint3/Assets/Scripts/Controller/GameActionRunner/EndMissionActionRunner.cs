using System;
using GameCommunication;
using UnityEngine;

namespace GameController {
	public class EndMissionActionRunner : GameActionRunner {
		private EndMissionActionData endMissionData;
	
		public EndMissionActionRunner( EndMissionActionData endMissionData ) : base( false ) {
			this.endMissionData = endMissionData;
		}
		
		
		protected override void Execute() {
			UnityEngine.Debug.Log( "fim de jogo!" );
			GUIManager.ClearAllRenderables();
			Application.LoadLevel( ( int ) Utils.Scene.CHOOSE_MISSION );
		}
	}
}

