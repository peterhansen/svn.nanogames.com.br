using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using UnityEngine;
using GameCommunication;
using Utils;


namespace GameController {
	
	
	public abstract class GameActionRunner {
		
		protected bool asynchronous;
		
		protected bool autoCallNextActionRunner;
		
		protected GameActionRunner nextGameActionRunner;
		
		protected bool done = false;
		
		private static readonly List< GameActionRunner > gameActionRunners = new List< GameActionRunner >();
		
		private float time;
	
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="asynchronous">
		/// A <see cref="System.Boolean"/>
		/// </param>
		/// <param name="callNextAction">
		/// A <see cref="System.Boolean"/>
		/// </param>
		protected GameActionRunner( bool asynchronous ) {
			this.asynchronous = asynchronous;
			autoCallNextActionRunner = !asynchronous;
		}
		
		public static bool IsBusy() {
			return gameActionRunners.Count > 0;
		}
		
		
		public static void ProcessRequests( List< GameActionData > requests ) {
			try {
				bool alreadyRunning = gameActionRunners.Count > 0;
				GameActionRunner previous = alreadyRunning ? gameActionRunners[ gameActionRunners.Count - 1 ] : null;
				
				foreach ( GameActionData request in requests ) {
					Type type = Type.GetType( request.GetType().ToString().Replace( "GameCommunication", "GameController" ).Replace( "Data", "Runner" ), true );
					ConstructorInfo constructor = type.GetConstructor( new Type[] { request.GetType() } );
					GameActionRunner runner = ( GameActionRunner ) constructor.Invoke( new object[] { request } );
					if ( previous != null )
						previous.SetNextGameActionRunner( runner );
					previous = runner;
					
					gameActionRunners.Add( runner );
				}
				
				// TODO execucao atualmente e feita apos cada recebimento de requests
				if( !alreadyRunning ) {
					ProcessQueue();
				}
			} catch ( Exception e ) {
				Debugger.LogError( "GameActionManager.ProcessRequests: " + e.Message );
			}
		}
		
		
		/// <summary>
		/// Executa a ação do jogo. Caso ela já tenha sido executada, este método não faz mais nada. 
		/// </summary>
		public void Run() {
			if ( !done ) {
				time = Time.realtimeSinceStartup;
				Execute();
				done = true;
				
				if ( !asynchronous ) {
					LogTime();
					gameActionRunners.Remove( this );
					
					if ( autoCallNextActionRunner && nextGameActionRunner != null )
						nextGameActionRunner.Run();
				}
			}
		}
		
		
		private void LogTime() {
			//Debugger.Log( "Game Action executed in " + ( Time.realtimeSinceStartup - time ) + " seconds. " + gameActionRunners.Count + " remaining." );
		}
		
		
		/// <summary>
		/// Método a ser chamado quando as ações de jogo assíncronas, como esperar um timer ou exibir uma animação,
		/// tiverem sido encerradas.
		/// </summary>
		public void OnDone() {
			if ( asynchronous ) {
				LogTime();
				gameActionRunners.Remove( this );
				
				if ( nextGameActionRunner != null )
					nextGameActionRunner.Run();
			}
		}
		
		
		public void SetAutoCallNextAction( bool autoCallNextAction ) {
			this.autoCallNextActionRunner = autoCallNextAction;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="nextGameAction">
		/// A <see cref="GameActionRequest"/>
		/// </param>
		public void SetNextGameActionRunner( GameActionRunner nextGameAction ) {
			this.nextGameActionRunner = nextGameAction;
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>
		/// A <see cref="GameActionRequest"/>
		/// </returns>
		public GameActionRunner GetNextGameAction() {
			return nextGameActionRunner;
		}
		
		
		/// <summary>
		/// Processa a fila de ações. 
		/// </summary>
		public static void ProcessQueue() {
			if ( gameActionRunners.Count > 0 ) {
				gameActionRunners[ 0 ].Run();
			}
		}
		
		
		/// <summary>
		///  
		/// </summary>
		protected abstract void Execute();
		
		
	}
}

