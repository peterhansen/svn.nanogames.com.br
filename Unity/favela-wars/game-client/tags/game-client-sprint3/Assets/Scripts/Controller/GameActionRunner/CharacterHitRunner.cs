using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using UnityEngine;
using GameCommunication;
using Utils;


namespace GameController {
	
	public class CharacterHitRunner : GameActionRunner {
	
		private CharacterHitData data;
		
		public CharacterHitRunner( CharacterHitData hitData ) : base( false ) {
			this.data = hitData;
			
			GameObject gameObject = WorldManager.GetInstance().GetGameObject( data.person );
			
			if ( !data.isAlive ) {
				gameObject.transform.RotateAround( Vector3.right, 90.0f );
			}
		}
		
		protected override void Execute() {
		}
	}
}

