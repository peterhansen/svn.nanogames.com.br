using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using GameController;
using GameCommunication;

public class PathFollowerScript : MonoBehaviour {
	
	bool isRunning;
	
	protected List< GameVector > nodes = new List< GameVector >();
	
	public float speed;
	
	protected float accTime;
	protected float totalTime;
	
	protected GameVector origin;
	protected GameVector destination;
	
	protected MoveCharacterRunner runner;
	
	
	public PathFollowerScript() {
		isRunning = false;
	}
	
	
	
	
	public void SetNodes( MoveCharacterRunner runner, List< GameVector > nodes ) {
		this.runner = runner;
		this.nodes = nodes;
	}
	

	// Use this for initialization
	public void Start() {
		speed = 42;
	}
	
	// Update is called once per frame
	public void Update() {
			if( !isRunning ) {
				isRunning = true;
				accTime = 0;
				Step();
			} else {
				if( accTime < totalTime ) {
					accTime += Time.deltaTime;
					
					if( accTime < totalTime ) {
						GameVector d = origin + ( destination - origin ) * ( accTime / totalTime );
						transform.position = ControllerUtils.CreateVector3( d );
					}
				} else {
					transform.position = ControllerUtils.CreateVector3( destination );
					Step();
				}
			}
	}
	
	
	private void Step() {
		if( nodes.Count > 0 ) {
			origin = new GameVector( transform.position.x, transform.position.y, transform.position.z );
			destination = nodes[ 0 ];
			transform.LookAt( ControllerUtils.CreateVector3( destination ) );
			
			accTime = 0;
			totalTime = origin.DistanceTo( destination ) / speed;
			
			nodes.RemoveAt( 0 );
		} else {
			isRunning = false;
			if( runner != null ) {
				runner.OnDone();
			}
		}
	}
}
