using System;
using GameController;
using GameCommunication;
using UnityEngine;
using Utils;
using Action = GameCommunication.Action;

namespace GameController {
	public class SetStanceInteractionController : InteractionController {
		
		public override void Flush() {
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			
			
			
			GameController.GetInstance().SendRequest( new SetStanceRequest( characterID, Stance.CROUCHING ) );
		}
	}
}

