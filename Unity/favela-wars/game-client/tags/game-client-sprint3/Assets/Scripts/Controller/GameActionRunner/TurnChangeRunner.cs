using System;

using GameCommunication;

namespace GameController {
	
	public class TurnChangeRunner : GameActionRunner {
		
		protected TurnData data;
		
		public TurnChangeRunner( TurnChangeData turnChangeData ) : base( true ) {			
			this.data = turnChangeData.turnData;
		}
		
		protected override void Execute() {	
			GameController.GetInstance().SetTurnData( data );
			OnDone();
		}	
	}
}

