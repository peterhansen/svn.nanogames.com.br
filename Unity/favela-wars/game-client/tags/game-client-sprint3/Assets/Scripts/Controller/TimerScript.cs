using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameController;
using System.Threading;
using Utils;


public class TimerScript : MonoBehaviour {
	
	private static List< NanoTimer > activeTimers = new List< NanoTimer >();
	
	
	protected TimerScript() {
	}
	
	
	public static void CheckSingleton() {
		ControllerUtils.GetInstance( "<TimerObject>", "TimerScript" );
	}

	
	public static int AddTimer( int milliseconds, TimerScriptListener listener ) {
		CheckSingleton();
		
		NanoTimer t = new NanoTimer( activeTimers.Count, milliseconds, listener );
		activeTimers.Add( t );
		
		return t.id;
	}
	
	
	public void Start() {
	}
	
	
	public void Update() {
		int timeMS = ( int ) ( Time.deltaTime * 1000 );
		
		List< NanoTimer > timersToRemove = new List< NanoTimer >();
		NanoTimer[] temp = activeTimers.ToArray();
		for ( int i = 0; i < temp.Length; ++i ) {
			NanoTimer t = temp[ i ];
			t.remainingTime -= timeMS;
		
			if ( t.remainingTime <= 0 ) {
				timersToRemove.Add( t );
				t.listener.OnTimerEnded( t.id );
			}
		}
		
		foreach ( NanoTimer t in timersToRemove ) {
			activeTimers.Remove( t );
		}
		
//		if ( activeTimers.Count <= 0 )
//			enabled = false;
	}
	
}



internal class NanoTimer {
	
	public int id;
	
	public int remainingTime;
	
	public TimerScriptListener listener;
	
	
	public NanoTimer( int id, int milliseconds, TimerScriptListener listener ) {
		this.id = id;
		this.remainingTime = milliseconds;
		this.listener = listener;
	}
	
	
}

