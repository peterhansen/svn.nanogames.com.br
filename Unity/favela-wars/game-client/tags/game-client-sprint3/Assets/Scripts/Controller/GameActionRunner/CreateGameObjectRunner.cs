using System;
using UnityEngine;

using GameCommunication;

using Utils;


namespace GameController {
	
	public class CreateGameObjectRunner : GameActionRunner {
		
		private CreateGameObjectData data;
		
	
		public CreateGameObjectRunner( CreateGameObjectData createGameObjectData ) : base( false ) {
			this.data = createGameObjectData;
		}
		
		
		protected override void Execute() {	
			WorldManager.GetInstance().AddWorldObject( data.worldObjectID, data.assetBundleID );
		}			
		
	}
}

