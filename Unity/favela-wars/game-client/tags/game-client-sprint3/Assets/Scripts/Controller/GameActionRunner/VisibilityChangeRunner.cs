using System;

using GameCommunication;

namespace GameController {
	
	public class VisibilityChangeRunner : GameActionRunner {
		
		protected VisibilityChangeData data;
	
		public VisibilityChangeRunner( VisibilityChangeData data ) : base( false ) {
			this.data = data;
		}
		
		protected override void Execute() {
			foreach( int i in data.visibleWorldObjectIds ) {
				WorldManager.GetInstance().GetGameObject( i ).renderer.enabled = true;
			}
			
			foreach( int i in data.invisibleWorldObjectIds ) {
				WorldManager.GetInstance().GetGameObject( i ).renderer.enabled = false;
			}
		}
	}
}

