using System;
using GameController;
using GameCommunication;
using UnityEngine;
using Utils;
using Action = GameCommunication.Action;
namespace GameController
{
	public class ShootInteractionController : InteractionController {
		
		public ShootInteractionController() {
			state = InteractionControllerState.WAITING_FOR_INTERACTION;
		}

		
		public override void ReceiveInputEvent( InputEventScene inputEvent ) {
			base.ReceiveInputEvent( inputEvent );
		
			
			switch ( state ) {	
				case InteractionControllerState.CHARACTER_FROM_PLAYER_TEAM_SELECTED:
					
					switch( inputEvent.mouseButton ) {
						case 0:
							if ( inputEvent.gameObject != null ) {
								// TODO
								//state = InteractionControllerState.CHARACTER_ATTEMPTING_TO_SHOOT;
								
								GameVector target = new GameVector( inputEvent.selectionPosition.x, inputEvent.selectionPosition.y, inputEvent.selectionPosition.z );
								
								// HACK
								Vector3 targetVec = ControllerUtils.CreateVector3( target );
								Debug.DrawLine( selectedCharacter.transform.position, targetVec, Color.blue, 10.0f );
								
								int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
		
								GameController.GetInstance().SendRequest( new ShootRequest( characterID, 0, target ) );
							}
						break;
							
					}
				break;
			}
		}
	}
}

