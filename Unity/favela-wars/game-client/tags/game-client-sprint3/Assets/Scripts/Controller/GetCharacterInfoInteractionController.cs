using System;
using GameCommunication;

namespace GameController {
	public class GetCharacterInfoInteractionController : InteractionController {
		
		public override void Flush () {
			int characterID = WorldManager.GetInstance().GetWorldID( selectedCharacter );
			GameController.GetInstance().SendRequest( new GetCharacterInfoRequest( characterID ) );
		}
	}
}

