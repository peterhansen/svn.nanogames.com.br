using UnityEngine;
using System.Collections;

using Utils;
using GameCommunication;
using GameController;


public class DebuggerScript : MonoBehaviour {
	
	public bool writeToFile = true;
	
	public bool writeToConsole = true;
	
	
	public void OnGUI() {
		Init();
	}
	

	// Use this for initialization
	public void Awake() {
		Init();
	}
	
	
	private void Init() {
		Debugger.SetWriteToFile( writeToFile );
		
		if ( writeToConsole ) {
			Debugger.SetRegularLogger( Debug.Log );
			Debugger.SetWarningLogger( Debug.LogWarning );
			Debugger.SetErrorLogger( Debug.LogError );
		}
		
		Debugger.SetLineDrawer( DrawLine );
		enabled = false;
	}
	
	
	public void OnApplicationQuit() {
		Debugger.Release();
	}
	
	
	public void DrawLine( GameVector origin, GameVector destination, uint color, float time ) {
		Vector3 o = new Vector3( origin.x, origin.y, origin.z );
		Vector3 d = new Vector3( destination.x, destination.y, destination.z );
		Color c = ControllerUtils.GetColor( color );
		
		Debug.DrawLine( o, d, c, time );
	}
	
}
