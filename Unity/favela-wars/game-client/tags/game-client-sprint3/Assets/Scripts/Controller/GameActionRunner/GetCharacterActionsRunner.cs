using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameController;
using GameCommunication;
using Utils;
using Action = GameCommunication.Action;


namespace GameController {
	
	public class GetCharacterActionsRunner : GameActionRunner {
		
		private GetCharacterActionsData data;
		
	
		public GetCharacterActionsRunner( GetCharacterActionsData getCharacterActionsData ) : base( true ) {
			this.data = getCharacterActionsData;
		}
		
		
		protected override void Execute() {
			// TODO por enquanto, GetCharacterActionsRunner só exibe no console os dados do personagem
			Debugger.Log( "Actions available: " + data.actionTree.ToString() );
			ActionsMenu menu = new ActionsMenu( data.actionTree );
			GUIManager.AddGUIRenderer( menu );
			OnDone();
		}
		
	}
	
	
	internal class ActionsMenu : GUIRenderer {
		
		private Action actionTree;
		
		
		public ActionsMenu( Action actionTree ) {
			this.actionTree = actionTree;
		}
		
		
		public void OnGUI() {
			GUI.Box( new Rect( 0,0, 500, 500 ), "ações" );
			DrawAction( actionTree, 40, 40, 80 );
		}
		
		
		private void DrawAction( Action a, int x, int y, int yDiff ) {
			
			foreach ( Action c in a.GetChildren() ) {
				if ( GUI.Button( new Rect( x, y, 15 * c.GetTitle().Length, 30 ), c.GetTitle() ) ) {
					GUIManager.RemoveGUIRenderer( this );
					
					if ( c.getInteractionControllerClass() != null )
						GUIManager.SetInteractionController( ( InteractionController ) c.getInteractionControllerClass().GetConstructor( Type.EmptyTypes).Invoke(null) );
					else
						GUIManager.SetInteractionController( null );
				}
				
				DrawAction( c, x + 120, y, yDiff >> 1 );
				y += yDiff;
				
			}
		}
		
	}
}

