using System;
using System.Collections.Generic;

using GameCommunication;

namespace GameModel {
	
	public abstract class RaycastFilter {
		public abstract List< Box > FilterBoxes( List< Box > boxes );
	}
	
	public class RaycastFilterIgnore : RaycastFilter {
		
		List< Box > ignoredBoxes = new List< Box >();
		
		public void ClearIgnoreList() {
			ignoredBoxes.Clear();
		}
		
		public override List<Box> FilterBoxes( List< Box > boxes ) {
			List< Box > filtered = new List< Box >( boxes );
			
			foreach( Box b in ignoredBoxes ) {
				filtered.Remove( b );
			}
			return filtered;
		}
		
		public void Ignore( Box box ) {
			ignoredBoxes.Add( box );
		}
	}
	
	public class RaycastFilterReplace : RaycastFilter {
		
		List< Box > filterBoxes = new List< Box >();
		
		public void ClearFilteredList() {
			filterBoxes.Clear();
		}

		
		public override List<Box> FilterBoxes( List< Box > boxes ) {
			return filterBoxes;
		}
		
		public void Add( Box box ) {
			filterBoxes.Add( box );
		}
	}
}

