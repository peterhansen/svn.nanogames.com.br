using System;
using System.Text;
using System.Collections.Generic;
using Action = GameCommunication.Action;
using Utils;
using GameController;

namespace GameModel {
	
	[ Serializable ]
	public abstract class Character : WorldObject {
		
		/// <summary>
		/// 
		/// </summary>
		protected string name = "";
		
		/// <summary>
		/// 
		/// </summary>
		protected Gender gender;
		
		/// <summary>
		/// 
		/// </summary>
		protected int attackLevel;
		
		/// <summary>
		/// 
		/// </summary>
		protected int defenseLevel;
		
		/// <summary>
		/// 
		/// </summary>
		protected int agility;
		
		/// <summary>
		/// 
		/// </summary>
		protected int technique;
		
		/// <summary>
		/// 
		/// </summary>
		protected int intelligence;
		
		/// <summary>
		/// 
		/// </summary>
		protected int perception;
		
		/// <summary>
		/// 
		/// </summary>
		protected int actionPoints;
		
		/// <summary>
		/// 
		/// </summary>
		protected int hitPoints;
		
		/// <summary>
		/// 
		/// </summary>
		protected int moralPoints;
		
		/// <summary>
		/// 
		/// </summary>
		protected int experiencePoints;
		
		
		
		// TODO temporariamente personagens tem "assetBundle" fixo
		public Character( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID ) {
			//TODO: teste de tiros...
			SetHitPoints( 10 );
		}
		
		
		public abstract Faction GetFaction();
		
		
		public void SetName( string name ) {
			this.name = name;
		}
		
		
		public string GetName() {
			return name;
		}
		
		
		public void setGender( Gender g ) {
			this.gender = g;
		}
		
		
		public Gender GetGender() {
			return gender;
		}
		
		
		public void SetAttackLevel( int attackLevel ) {
			this.attackLevel = attackLevel;
		}
		
		
		public int GetAttackLevel() {
			return attackLevel;
		}
		
		
		public void SetDefenseLevel( int defenseLevel ) {
			this.defenseLevel = defenseLevel;
		}
		
		
		public int GetDefenseLevel() {
			return defenseLevel;
		}
		
		
		public void SetAgility( int agility ) {
			this.agility = agility;
		}
		
		
		public int GetAgility() {
			return agility;
		}
		
		
		public void SetTechnique( int technique ) {
			this.technique = technique;
		}
		
		
		public int GetTechnique() {
			return technique;
		}
		
		
		public void SetIntelligence( int intelligence ) {
			this.intelligence = intelligence;
		}
		
		
		public int GetIntelligence() {
			return intelligence;
		}
		
		
		public void SetPerception( int perception ) {
			this.perception = perception;
		}
		
		
		public void SetActionPoints( int actionPoints ) {
			this.actionPoints = actionPoints;
		}
		
		
		public int GetActionPoints() {
			return actionPoints;
		}
		
		
		public void SetHitPoints( int hitPoints ) {
			this.hitPoints = hitPoints;
		}
		
		
		public int GetHitPoints() {
			return hitPoints;
		}
		
		
		public void SetMoralPoints( int moralPoints ) {
			this.moralPoints = moralPoints;
		}
		
		
		public int GetMoralPoints() {
			return moralPoints;
		}
		
		
		public void SetExperiencePoints( int experiencePoints ) {
			this.experiencePoints = experiencePoints;
		}
		
		
		public int GetExperiencePoints() {
			return experiencePoints;
		}
		
		//TODO: não sei se a estrutura vai ser bem assim...
		public virtual Action GetPossibleActionsList() {
			Action toReturn;
			toReturn = new Action( "Actions" );
			toReturn.SetInteractionControllerClass( typeof( InteractionController ) );
			return toReturn;
		}
		
		
		public override string ToString() {
			StringBuilder sb = new StringBuilder( "[" );
			sb.Append( GetType().ToString().Replace( "GameModel.", "" ) );
			sb.Append( " name:" );
			sb.Append( GetName() );
			sb.Append( " gender:" );
			sb.Append( GetGender() );
			sb.Append( " HP:" );
			sb.Append( GetHitPoints() );
			sb.Append( " AP:" );
			sb.Append( GetActionPoints() );
			sb.Append( " moral:" );
			sb.Append( GetMoralPoints() );
			sb.Append( " ]" );
			
			return sb.ToString();
		}
		
		
		public override bool Equals( object obj ) {
			return Equals( obj as Character );
		}
		
		
		public bool Equals( Character c ) {
			return c != null && name.Equals( c.name ) && gender.Equals( c.gender ) && attackLevel == c.attackLevel &&
				   defenseLevel == c.defenseLevel && agility == c.agility && technique == c.technique &&
				   intelligence == c.intelligence && perception == c.perception && actionPoints == c.actionPoints;
		}
		
		
		public override int GetHashCode() {
			return base.GetHashCode();
		}
		
	}
	
	
}


	