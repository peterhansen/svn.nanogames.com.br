using System.Collections;
using System.Collections.Generic;

using SharpUnit;


namespace GameModel {	
	
	public class Player {
	
		protected int id;
		protected string nickname;
		
		public Player( int id ) {
			this.id = id;
		}
		
		public int GetID() {
			return id;
		}
		
		public override bool Equals( object obj ) {
			return Equals( obj as Player );
		}
		
		public bool Equals( Player other ) {
			if( other == null ) {
				return false;
			}
			
			return id == other.GetID();
		}
		
		public override string ToString() {
			return "Player( " + id + " )";
		}
		
		public override int GetHashCode() {
			return ToString().GetHashCode();
		}
		
		public class Comparer : IEqualityComparer< Player > {
			public bool Equals( Player x, Player y ) {
				return x.Equals( y );
			}
				
			public int GetHashCode( Player obj ) {
				return obj.GetHashCode();
			}
		}
		
		internal class PlayerComparerTest : TestCase {
			
			[UnitTest]
			public void AddingTwoEqualPlayers() {
				Player player1 = new Player( 1 );
				Player player2 = new Player( 1 );
				
				Dictionary< Player, int > d = new Dictionary< Player, int >( new Player.Comparer() );
				d[ player1 ] = 44;
				d[ player2 ] = -13;
				
				Assert.Equal( 1, d.Keys.Count );
				Assert.Equal( -13, d[ player1 ] );
				Assert.Equal( -13, d[ player2 ] );
			}
		}
	}
}