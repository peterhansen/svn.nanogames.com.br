using System;
using GameCommunication;


namespace GameModel {
	public class LogHandler : Handler {
		
		protected LogRequest logRequest;
		
		public LogHandler ( LogRequest logRequest )
		{
			this.logRequest = logRequest;
			
		}
		
		public override Response GetResponse() {
			Response r = new Response();
			r.AddGameActionData( new LogActionData( logRequest.logLevel, logRequest.message ) );
			return r;
		}
	}
}

