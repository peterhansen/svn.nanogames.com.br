using UnityEngine;
using System.Collections;
using GameCommunication;
using Utils;
namespace GameModel {
	public class SetStanceHandler : Handler {
		protected SetStanceRequest stanceRequest;
		
		
		public SetStanceHandler( SetStanceRequest stanceRequest ) {
			this.stanceRequest = stanceRequest;
		}

		public override Response GetResponse() {
			Response r;
			Mission mission = Mission.GetInstance( stanceRequest.GetPlayerID() );
			Soldier characterWorldObj = mission.GetWorldObjectByID( stanceRequest.worldObjectID ) as Soldier;

			if ( characterWorldObj == null || 
			     mission.GetWorldObjectPlayerID( characterWorldObj ) != stanceRequest.GetPlayerID() ) 
			{
				// personagem não encontrado, ou não pode ser mexido pelo jogador
				Debugger.LogWarning( "Jogador tentando modificar pose personagem fora de sua tropa. PlayerID: " + stanceRequest.GetPlayerID() + ", characterID: " + stanceRequest.worldObjectID );
				return null;
			}
			
			r = new Response();
			
			characterWorldObj.SetStance( stanceRequest.newStance );
			
			r.AddGameActionData( new SetStanceData( characterWorldObj.GetWorldID(), characterWorldObj.GetStance() ) );
			
			return r;
		}
	}
}