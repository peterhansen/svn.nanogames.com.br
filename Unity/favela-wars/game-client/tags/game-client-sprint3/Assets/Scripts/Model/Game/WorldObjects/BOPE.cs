using System;
using Utils;
using Action = GameCommunication.Action;

namespace GameModel {

	[ Serializable ]
	public class BOPE : Soldier {
		public BOPE( int gameObjectID ) : base( gameObjectID, "0000ffff" ) {
		}
		
		
		public BOPE( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID ) {
		}
		
		
		public override Faction GetFaction() {
			return Faction.POLICE;
		}
		
		public override Action GetPossibleActionsList () {
			Action toReturn;
			Action shoot;
			toReturn = base.GetPossibleActionsList ();
			shoot = toReturn.GetChildWith( Common.ActionTypes.SHOOT );
			//TODO: valores temporários
			shoot.AddChild( new Action( ".9mm" ) );
			shoot.AddChild( new Action( "fuzil" ) );
			
			
			toReturn.AddChild( new Action( "Prender" ) );
			toReturn.AddChild( new Action( "Pedir reforco" ) );
			
			return toReturn;
		}		
		
	}
	
}

