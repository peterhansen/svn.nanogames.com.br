using System;
using System.Collections.Generic;


namespace GameModel {
	
	public class GameWorld {
		
		protected Dictionary< int, WorldObject > worldObjects;
		
		private CollisionManager collisionManager;
		
		
		public GameWorld() {
			worldObjects = new Dictionary< int, WorldObject >();
		}
		
		
		public void LoadStage( string stageName ) {
			collisionManager = new CollisionManager( stageName );
		}
		
		
		public CollisionManager GetCollisionManager() {
			return collisionManager;
		}
}
}

