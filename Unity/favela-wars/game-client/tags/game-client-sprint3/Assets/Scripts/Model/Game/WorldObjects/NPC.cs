using System;

using Utils;


namespace GameModel {
	
	[ Serializable ]
	public class NPC : Character {
		
		public NPC( int gameObjectID ) : this( gameObjectID, "ffff00ff" ) {
		}
		
		
		public NPC( int gameObjectID, string assetBundleID ) : base( gameObjectID, assetBundleID ) {
		}
		
		
		public override Faction GetFaction() {
			return Faction.NEUTRAL;
		}
		
	}
	
}

