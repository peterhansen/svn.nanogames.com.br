using System;
using System.Collections;
using System.Collections.Generic;

using SharpUnit;

using Utils;
using GameCommunication;


namespace GameModel {
	
	using HashSetMissionCondition = Utils.HashSet< EndMissionCondition >;
	using HashSetSpawnPoint = Utils.HashSet< SpawnPoint >;
	
	public class Mission {
		
		/// <summary>
		/// Relaciona índice dos jogadores a uma instância de uma missão.
		/// </summary>
		protected static Dictionary< int, Mission > instances = new Dictionary< int, Mission >();
		
		protected Map map;
	
		/// <summary>
		/// Relaciona índices dos WorldObjects às suas instâncias
		/// </summary>
		protected Dictionary< int, WorldObject > worldObjects = new Dictionary< int, WorldObject >();
		
		/// <summary>
		/// 
		/// </summary>
		protected HashSetSpawnPoint spawnPoints = new HashSetSpawnPoint();
		
		/// <summary>
		/// 
		/// </summary>
		protected HashSetMissionCondition endMissionConditions = new HashSetMissionCondition();
		
		/// <summary>
		/// 
		/// </summary>
		protected List< Player > players = new List< Player >();
		
		private VisibilityManager visibilityManager;
		/// <summary>
		/// 
		/// </summary>
		protected Dictionary< int, int > charactersPerPlayer = new Dictionary< int, int >();
		
		/// <summary>
		/// 
		/// </summary>
		protected string name;
		
		/// <summary>
		/// 
		/// </summary>
		protected string sceneName;
		
		/// <summary>
		/// Informacoes relativas aos dados do turno corrente da missão.
		/// </summary>
		//protected TurnData turnData;
		protected TurnManager turnManager;
	
	
		protected Mission( string levelPrefix, List< uint > soldierIDs ) {
			MissionData missionData = MissionData.ImportFromFile( levelPrefix );
			
			this.name = missionData.missionName;
			this.sceneName = missionData.sceneName;
			this.endMissionConditions = missionData.endMissionConditions;
			
			// TODO: por enquanto, comecamos com a policia
			turnManager = new TurnManager( this );
			map = new Map( missionData.mapName );
			
			foreach ( SpawnAreaMissionData spawn in missionData.spawnAreasData ) {
				SpawnPoint p = new SpawnPoint( worldObjects.Count, spawn );
				spawnPoints.Add( p );
			}
			
			List< Character > characters = new List< Character >();
			int worldID = 1;
			
			HashSetSpawnPoint npcSpawnPoints = GetValidSpawnPoints( Faction.NEUTRAL );
			foreach ( SpawnPoint p in npcSpawnPoints ) {
				int npcs = p.GetRandomQuantity();
				for ( int i = 0; i < npcs; ++i ) {
					NPC npc = new NPC( worldID++ );
					characters.Add( npc );
					charactersPerPlayer.Add( npc.GetWorldID(), -1 ); // TODO assumindo -1 como id do jogador de NPCs
				}
			}
			
			foreach ( int soldierID in soldierIDs ) {
				BOPE b = new BOPE( worldID++, soldierID.ToString() );
				characters.Add( b );
				charactersPerPlayer.Add( b.GetWorldID(), 0 ); // TODO assumindo 0 como id do jogador do BOPE
			}
			
			for ( int i = 0; i < 5; ++i ) {
				DrugDealer d = new DrugDealer( worldID++ );
				characters.Add( d );
				charactersPerPlayer.Add( d.GetWorldID(), 1 ); // TODO assumindo 1 como id do jogador dos traficantes
			}
			
			PopulateSpawnPoints( characters );
			visibilityManager = new VisibilityManager();
		}
		
		public void BuildVisibilityMap( List<Character> visibilityResult, Faction faction ) {
			List<Character> visibles = new List<Character>();

			if ( visibilityResult != null ) {
				visibles.AddRange( visibilityResult );
			} else {
				foreach ( Character c in worldObjects.Values )
					if ( c.GetFaction() == faction )
						visibles.Add( c );
			}
			
			
			foreach ( Character a in visibles ) {
				RaycastFilterIgnore filter = new RaycastFilterIgnore();
				
				foreach ( Character b in visibles ) {
					if ( a == b ) {
						visibilityManager.SetVisibility( a.GetWorldID(), a.GetWorldID(), true );
						continue;
					}
					
					if ( a.GetFaction() == b.GetFaction() ) {
						visibilityManager.SetVisibility( a.GetWorldID(), a.GetWorldID(), true );
						continue;
					}

					
					if ( visibilityManager.GetVisibility( a.GetWorldID(), b.GetWorldID() ) )
						continue;
					
					filter.ClearIgnoreList();
					filter.Ignore( b.GetBox() );
					filter.Ignore( a.GetBox() );
					
					foreach( Character m in visibles ) {
						if ( m.GetFaction() == faction )
							filter.Ignore( m.GetBox() );
					}
					 
					bool visible = false;
					visible = map.GetAStarGraph().CheckSight( a.GetGameTransform().position, b.GetGameTransform().position, filter );
					visibilityManager.SetVisibility( a.GetWorldID(), b.GetWorldID(), visible );
				}
			}
		}
		
		public VisibilityManager GetVisibilityManager() {
			return visibilityManager;
		}
		
		public TurnManager GetTurnManager() {
			return turnManager;
		}
		
		
		public void CheckEndMissionConditions() {
	
			foreach ( EndMissionCondition condition in  endMissionConditions ) {
				
				if ( condition.Check( this ) ) {
			 
					Response endGame = new Response();
					endGame.AddGameActionData( new EndMissionActionData( condition  ) );
					GameManager.GetInstance().EnqueueResponse( endGame );
					return;
				}
			}
		}
		
		
		private HashSetSpawnPoint GetValidSpawnPoints( Faction faction ) {
			HashSetSpawnPoint validSpawnPoints = new HashSetSpawnPoint();
			
			foreach ( SpawnPoint p in spawnPoints ) {
				if ( p.GetFaction() == faction )
					validSpawnPoints.Add( p );
			}
			
			return validSpawnPoints;
		}
						
						
		private void PopulateSpawnPoints( List< Character > characters ) {
			Dictionary< Faction, HashSetSpawnPoint > validAreas = new Dictionary< Faction, HashSetSpawnPoint >();
			Random r = new Random();
			
			foreach ( Character c in characters ) {
				// atualiza a lista de spawn points válidos para cada facção, caso necessário
				if ( !validAreas.ContainsKey( c.GetFaction() ) )
					validAreas.Add( c.GetFaction(), GetValidSpawnPoints( c.GetFaction() ) );
				HashSetSpawnPoint v = validAreas[ c.GetFaction() ];
				List< SpawnPoint > places = new List< SpawnPoint >( v );
				
				if ( places.Count <= 0 ) {
					Debugger.LogError( "Não é possível popular o nível: spawn points da facção " + c.GetFaction() + " com capacidade insuficiente." );
					return;
				}
				
				// sorteia um dos spawn points
				int first = r.Next( 0, places.Count );
				for ( int i = ( first + 1 ) % places.Count; places.Count > 0; ) {
					if ( places[ i ].AddWorldObject( c ) ) {
						worldObjects.Add( c.GetWorldID(), c );
						GetMap().GetCollisionManager().AddBox( c );
						GetMap().UpdateNavigationGraph( c, visibilityManager );
						break;
					} else {
						// não conseguiu inserir o personagem; remove o spawn point da lista
						places.Remove( places[ i ] );
						if ( places.Count > 0 ) {
							i %= places.Count;
							if ( i == first )
								break;
						}
					}
				}
			}
		}
		
		public void DisableCharacter( Character corpse ) {
			Debugger.Log( "morto: " + corpse );
			CheckEndMissionConditions();
		}
		
		
		public static Mission CreateInstance( List< int > playerIDs, string levelPrefix, List< uint > soldierIDs ) {
			
			Mission mission = new Mission( levelPrefix, soldierIDs );
			
			foreach( int playerId in playerIDs ) {
				instances[ playerId ] = mission;
				
				// TODO: por enquanto, ainda não sabemos a faccao de cada jogador
				mission.AddPlayer( new Player( playerId ), playerId == 0? Faction.POLICE: Faction.CRIMINAL );
			}
			
			// após adicionar todos os jogadores, podemos comecar
			// o controle dos turnos
			mission.GetTurnManager().Start();
			
			return mission;
		}
		
		
		public static Mission GetInstance( int playerID ) {
			if( instances.ContainsKey( playerID ) ) {
				return instances[ playerID ];
			}
			
			return null;
		}
		
		public void AddPlayer( Player player, Faction faction ) {
			players.Add( player );
			turnManager.AddPlayer( player, faction );
		}

		
//		public static Mission LoadMission( string missionName ) {
//			if( instance == null ) {
//				instance = 
//			}
//			
//		}
		
		
		public string GetSceneName() {
			return sceneName;
		}
		
		
		public List< WorldObject > GetWorldObjects() {
//			return worldObjects.GetEnumerator();
			return new List< WorldObject >( worldObjects.Values );
		}
		
		
		public WorldObject GetWorldObjectByID( int worldId ) {
			return worldObjects.ContainsKey( worldId ) ? worldObjects[ worldId ] : null;
		}
		
		
		public int GetWorldObjectPlayerID( WorldObject obj ) {
			return GetWorldObjectPlayerID( obj.GetWorldID() );
		}
		
		
		public int GetWorldObjectPlayerID( int worldObjectID ) {
			return charactersPerPlayer[ worldObjectID ];
		} 
		
		
		public Map GetMap() {
			return map;
		}
		
		
		#region Mission Test Case
		internal class MissionTestCase : TestCase {
		
//			[UnitTest]
//			public void InstanceOfMissionWhenItIsNotLoaded() {
//				
//				Assert.ExpectException( new MissionNotLoadedException() );
//				Mission.GetInstance();
//				
//			}
			
			
//			[UnitTest]
//			public void ImportMissionFromInvalidFile() {
//				
//				Assert.ExpectException( new InvalidMissionFile() );
//				
//				MissionGenerator otherMG = new MissionGenerator();
//				otherMG.ImportFromFile( "Assets/Levels/teste_1_INVALID.mission" );
//			}
//			
//			[UnitTest]
//			public void ImportingMissionGenerationFromFile() {
//				
//				CreatingMissionGenerator();
//				
//				missionGenerator.ExportToFile();
//				
//				MissionGenerator otherMG = new MissionGenerator();
//				
//				otherMG.ImportFromFile( "Assets/Levels/teste_1.mission" );
//			}
		}
		#endregion
		
		protected class MissionNotLoadedException : Exception {
		}
		
		protected class InvalidMissionFileException : Exception {
		}
	}
}

