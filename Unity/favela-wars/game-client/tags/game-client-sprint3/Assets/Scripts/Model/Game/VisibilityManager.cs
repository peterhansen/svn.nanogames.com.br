using System;
using Utils;
using GameCommunication;
using System.Collections.Generic;

namespace GameModel {
	
	public class VisibilityManager {
		
		private class MutuallyVisibleCharacters {
			public int A;
			public int B;
			
			public override int GetHashCode ()
			{
				return ToString().GetHashCode ();
			}
			
			public override bool Equals (object obj) {
				return Equals( obj as MutuallyVisibleCharacters );
			}
			
			public bool Equals( MutuallyVisibleCharacters obj ) {
				
				if ( obj == null ) 
					return false;
				
				return ( A == obj.A && B == obj.B ) || ( obj.A == B && obj.B == A );
			}
			
			public override string ToString ()
			{
				return string.Format ("[MutuallyVisibleCharacters]" + A + "," + B );
			}
		}
	
		public void Reset() {
			visibilityMap.Clear();
		}
		
		private BijectionHashSet< MutuallyVisibleCharacters, bool > visibilityMap;
		
		public VisibilityManager () {
			visibilityMap = new BijectionHashSet<MutuallyVisibleCharacters, bool>();
		}
		
		public void SetVisibility( int A, int B, bool status ) {
			MutuallyVisibleCharacters pair1 = new MutuallyVisibleCharacters();
			MutuallyVisibleCharacters pair2 = new MutuallyVisibleCharacters();
			pair1.A = pair2.B = A;
			pair1.B = pair2.A = B;
			
			visibilityMap.Set( pair1, status );
			visibilityMap.Set( pair2, status );
			
		}
		
		public bool Contains( int A, int B ) {
			MutuallyVisibleCharacters pair1 = new MutuallyVisibleCharacters();
			MutuallyVisibleCharacters pair2 = new MutuallyVisibleCharacters();
			pair1.A = pair2.B = A;
			pair1.B = pair2.A = B;

			return visibilityMap.Contains( pair1 ) || visibilityMap.Contains( pair2 );
		}
		
		
		public bool GetVisibility( int A, int B ) {
			MutuallyVisibleCharacters pair1 = new MutuallyVisibleCharacters();
			MutuallyVisibleCharacters pair2 = new MutuallyVisibleCharacters();
			pair1.A = pair2.B = A;
			pair1.B = pair2.A = B;

			if (  visibilityMap.Contains( pair1 ) )
				return visibilityMap.Get( pair1 );
			else if ( visibilityMap.Contains( pair2 ) )
				return visibilityMap.Get( pair2 );
			else
				return false;
			
		}
		
	public static VisibilityChangeData GetVisibilityFor( Faction faction, Mission mission ) {
			
			VisibilityChangeData visibilityData = new VisibilityChangeData();

			foreach ( Character c in mission.GetWorldObjects() ) {
				
				if ( c.GetFaction() != faction ) {
					visibilityData.invisibleWorldObjectIds.Add( c.GetWorldID() );
				}
			}			
			
			foreach ( Character c in mission.GetWorldObjects() ) {
				
				if ( c.GetFaction() == faction ) {
					visibilityData.Combine( GetVisibilityFor( c, mission ) );
					visibilityData.MakeVisible( c.GetWorldID() );
				}
			}
			
			return visibilityData;
		}
		
		public static VisibilityChangeData GetVisibilityFor( WorldObject characterWorldObj, Mission mission ) {
					// TODO: o uso do VisibilityChangeData deve ser feito em outro lugar?
				GameTransform charGT = characterWorldObj.GetGameTransform();
				CollisionManager cm = mission.GetMap().GetCollisionManager();
				
				float coneHeight = 60.0f;
				GameVector leftSide = charGT.direction.Cross( GameVector.YAxis ).Unitary();
				Debugger.Log( "left side = " + leftSide );
				Debugger.DrawLine( charGT.position, charGT.position + ( charGT.direction * coneHeight ) + ( leftSide * coneHeight ), 0x0000ffff, 10.0f );
				Debugger.DrawLine( charGT.position, charGT.position + ( charGT.direction * coneHeight ) + ( -leftSide * coneHeight ), 0x0000ffff, 10.0f );
				
				VisibilityChangeData visibilityData = new VisibilityChangeData();
				
				List< Character > visibilityResult = new List<Character>();
				
				
				foreach( Box box in cm.CreateFilterCone( charGT.position, charGT.direction * coneHeight, 45.0f ) ) {
					int boxId = box.GetWorldObjectID();
					if( boxId > 0 )
						visibilityData.visibleWorldObjectIds.Add( boxId );
					
					Debugger.DrawLine( box.GetPosition(), box.GetPosition() + box.GetDirection() * 2.0f, 0x00ff00ff, 10.0f );
				}
				
				foreach( Box box in cm.GetBoxes() ) {
					int boxId = box.GetWorldObjectID();
					if( boxId > 0 && !visibilityData.visibleWorldObjectIds.Contains( boxId ) ) {
						visibilityData.invisibleWorldObjectIds.Add( boxId );
					}
				}

				foreach ( int id in visibilityData.visibleWorldObjectIds ) {
					if ( mission.GetWorldObjectByID( id ) is Character )
					visibilityResult.Add( mission.GetWorldObjectByID( id ) as Character );
				}
				
				
				Character b;
				Character a = characterWorldObj as Character;
				List< int > toBeRemoved = new List<int>();
			
				mission.BuildVisibilityMap( visibilityResult, a.GetFaction() );
			
				VisibilityManager vm = mission.GetVisibilityManager();
				
				if ( a != null ) {
					foreach ( int id in visibilityData.visibleWorldObjectIds ) {
						
						if ( mission.GetWorldObjectByID( id ) is Character ) {
							
							b = mission.GetWorldObjectByID( id ) as Character;
							
							if ( b != a && !vm.GetVisibility( a.GetWorldID(), b.GetWorldID() ) ) {
								toBeRemoved.Add(  b.GetWorldID() );
							}
						}
					}
				}

				foreach ( int id in toBeRemoved ) {
					visibilityData.visibleWorldObjectIds.Remove( id );
					visibilityData.invisibleWorldObjectIds.Add( id );
				}
			
			return visibilityData;
		}		
	}
}

