using System;

using Utils;
using GameCommunication;

namespace GameModel {
	
	public class EndParticipationHandler : Handler {
		
		protected EndParticipationRequest request;
		
		public EndParticipationHandler( EndParticipationRequest request ) {
			this.request = request;
		}
		
		public override Response GetResponse() {

			
			
			TurnManager turnManager = Mission.GetInstance( request.GetPlayerID() ).GetTurnManager();
			
			// TODO: onde são salvos os players?
			Player player = new Player( request.GetPlayerID() );
			Debugger.Log( "Player Id: " + request.GetPlayerID() );
			
			turnManager.EndParticipation( player );
			
			if( turnManager.HasTurnChanged() ) {
				Debugger.Log( "The Turn Has Changed!" );
				Response r = new Response();
				Mission mission = Mission.GetInstance( request.GetPlayerID() );
				r.AddGameActionData( new TurnChangeData( turnManager.GetTurnData() ) );
				VisibilityChangeData visibilityData = VisibilityManager.GetVisibilityFor( turnManager.GetTurnData().faction, mission );
				r.AddGameActionData( visibilityData );
				return r;
			}
			
			return null;
		}
	}
}

		