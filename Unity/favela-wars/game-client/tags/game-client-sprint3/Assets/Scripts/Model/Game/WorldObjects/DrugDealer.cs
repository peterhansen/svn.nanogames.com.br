using System;
using Utils;
using Action = GameCommunication.Action;


namespace GameModel {
	
	[ Serializable ]
	public class DrugDealer : Soldier {
	
		public DrugDealer( int gameObjectID ) : base( gameObjectID, "ff0000ff" ) {
		}
		
		
		public override Faction GetFaction() {
			return Faction.CRIMINAL;
		}
		
		public override Action GetPossibleActionsList () {
			Action toReturn;
			Action shoot;
			toReturn = base.GetPossibleActionsList ();
			shoot = toReturn.GetChildWith( Common.ActionTypes.SHOOT );
			//TODO: valores temporários
			shoot.AddChild( new Action( "Desert Eagle" ) );
			shoot.AddChild( new Action( "UZI" ) );

			toReturn.AddChild( new Action( "Esculachar" ) );
			toReturn.AddChild( new Action( "Ouvir pancadão" ) );
			
			return toReturn;
		}
	}
	
}

