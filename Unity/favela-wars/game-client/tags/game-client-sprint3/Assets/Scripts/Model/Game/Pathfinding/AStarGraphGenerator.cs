using System;
using System.Collections.Generic;
using SharpUnit;
using GameCommunication;

using Utils;


namespace GameModel {
	
	using HashSetBox = Utils.HashSet< Box >;
	
	
	public class AStarGraphGenerator {
		
		
		protected class NodeSet {
			public AStarNode northEastNode;
			public AStarNode northWestNode;
			public AStarNode southEastNode;
			public AStarNode southWestNode;
			
			public List< AStarNode > GetNodes() {
				List< AStarNode > nodes = new List< AStarNode >();
				
				if( northEastNode != null ) {
					nodes.Add( northEastNode );
				}
				
				if( northWestNode != null ) {
					nodes.Add( northWestNode );
				}
				
				if( southEastNode != null ) {
					nodes.Add( southEastNode );
				}
				
				if( southWestNode != null ) {
					nodes.Add( southWestNode );
				}
				
				return nodes;
			}
		}
		
		protected struct AStarArc {
			public AStarNode origin;
			public AStarNode target;
		}
		
		protected CollisionManager collisionManager;
		
		// Acceleration structures
		Dictionary< Box, NodeSet > nodeSets = new Dictionary< Box, NodeSet >();
		//Dictionary< AStarNode, List< AStarNode > > availableNodes = new Dictionary< AStarNode, List< AStarNode > >();
		
		Dictionary< Box, List< AStarNode > > blockedNodes = new Dictionary< Box, List< AStarNode > >();
		
		Dictionary< Box, List< AStarArc > > blockedArcs = new Dictionary< Box, List< AStarArc > >();
		Dictionary< AStarArc, int > numberOfBoxesBlockingArc = new Dictionary< AStarArc, int >();
		
		public AStarGraphGenerator( CollisionManager collisionManager ) {
			this.collisionManager = collisionManager;
		}
		
		
		public AStarGraph GenerateGraph() {
			AStarGraph graph = new AStarGraph( collisionManager );
			GeneratingNodeSets( graph );
			
			

			
			foreach( AStarNode node in graph.GetNodes() ) {
				foreach( AStarNode candidateNode in graph.GetNodes() ) {
					ConnectNodes( graph, node, candidateNode );
				}
			}			
			return graph;
		}
		
		
		public AStarGraph UpdateGraph( AStarGraph graph, WorldObject obj, VisibilityManager hints ) {
			Box objBox = obj.GetBox();
			
//			// atualiza os nós bloqueados pelo WorldObject
//			List< AStarNode > newBlockedNodes = new List< AStarNode >();
//			foreach( AStarNode node in graph.GetNodes() ) {
//				if( objBox.Contains( node.GetPosition() ) ) {
//					
//					Debugger.Log( "node blocked!" );
//					node.SetAccessibility( false );
//					newBlockedNodes.Add( node );
//				} else {
//					if( !blockedNodes.ContainsKey( objBox ) ) {
//						blockedNodes[ objBox ] = new List< AStarNode >();
//					}
//					
//					if( blockedNodes[ objBox ].Contains( node ) ) {
//						// assume-se que nós só podem ser bloqueados por
//						// uma caixa de cada vez
//						Debugger.Log( "released!" );
//						node.SetAccessibility( true );
//					}
//				}
//			}
//			blockedNodes[ objBox ] = newBlockedNodes;
				
			// verifica os arcos que eram bloqueados pela caixa do WorldObject
			if( blockedArcs.ContainsKey( objBox ) ) {
				foreach( AStarArc arc in blockedArcs[ objBox ] ) {
					numberOfBoxesBlockingArc[ arc ]--;
					if( numberOfBoxesBlockingArc[ arc ] == 0 ) {
						arc.origin.AddChild( arc.target );
						arc.target.AddChild( arc.origin );
					}
					//{
					//	availableNodes[ arc.origin ].Add( arc.target );
					//	availableNodes[ arc.target ].Add( arc.origin );
					//}
				}
			}
			blockedArcs[ objBox ] = new List< AStarArc >();
			
			// verifica os novos arcos bloqueados
			RaycastFilterReplace filter = new RaycastFilterReplace();
			filter.Add( objBox );
			
			foreach( AStarNode node in graph.GetNodes() ) {
				List< AStarNode > childrenToBeRemoved = new List<AStarNode>();
				foreach( AStarNode childNode in node.GetChildren() ) {
					if( !graph.CheckClearPath( node.GetPosition(), childNode.GetPosition(), filter ) ) {
						AStarArc arc = new AStarArc();
						arc.origin = node;
						arc.target = childNode;
						blockedArcs[ objBox ].Add( arc );
						
						if( !numberOfBoxesBlockingArc.ContainsKey( arc ) ) {
							numberOfBoxesBlockingArc[ arc ] = 0;
						}
						numberOfBoxesBlockingArc[ arc ]++;
						
						childrenToBeRemoved.Add( childNode );
						childNode.RemoveChild( node );
					}
				}
				
				foreach( AStarNode child in childrenToBeRemoved ) {
					node.RemoveChild( child );
				}
			}
			
			
			// atualiza nós da caixa
 			bool newNodeSet = !nodeSets.ContainsKey( objBox );
			nodeSets[ objBox ] = CalculateNodeSet( objBox );
			
			if( newNodeSet ) {
				AddNodeSet( graph, nodeSets[ objBox ], objBox );
			}
			
			
			int id1;
			int id2;
			
			foreach( AStarNode node in nodeSets[ objBox ].GetNodes() ) {
				id1 = node.getObjID();
				foreach( AStarNode candidateNode in graph.GetNodes() ) {
					id2 = candidateNode.getObjID();
					if( !ConnectNodes( graph, node, candidateNode ) ) {	
						node.RemoveChild( candidateNode );
						candidateNode.RemoveChild( node );
						//Debugger.DrawLine( node.GetPosition(), candidateNode.GetPosition(), 0x00ff00ff, 8.0f );
					} else {
						if ( hints != null && id1 != -1 && id2 != -1 )
							hints.SetVisibility( id1, id2, true );
					//	Debugger.DrawLine( node.GetPosition(), candidateNode.GetPosition(), 0xff0000ff, 8.0f );
					}
				}
			}
			return graph;
		}
		
		
		protected void GeneratingNodeSets( AStarGraph graph ) {
			nodeSets.Clear();
			IList< Box > boxes = collisionManager.GetBoxes();
			
			for ( int i = 0; i < boxes.Count; ++i ) {
				nodeSets[ boxes[ i ] ] = CalculateNodeSet( boxes[ i ] );
			}
			
			// verificando nós existentes
			foreach ( KeyValuePair< Box, NodeSet > boxNodeSet in nodeSets ) {
				AddNodeSet( graph, boxNodeSet.Value, boxNodeSet.Key );
			}
		}

		
		protected NodeSet CalculateNodeSet( Box box ) {
			
			GameVector nodePos = new GameVector();
			
			NodeSet nodeSet;
			if( nodeSets.ContainsKey( box ) ) {
				nodeSet = nodeSets[ box ];
				foreach( AStarNode node in nodeSet.GetNodes() ) {
					node.GetChildren().Clear();
				}
			} else {
				nodeSet = new NodeSet();
			}
			
			GameVector safeDistance = new GameVector(); 
			
			// TODO: esses testes podem ser refatorados

			// testing for a corner in the southeast direction
			nodePos = box.GetSouthEastTop();
			safeDistance.Set( Common.Character.RADIUS, 0.0f, Common.Character.RADIUS );
			nodePos += safeDistance;
			
			if ( !collisionManager.IsOccupied( nodePos ) ) {
				if( nodeSet.southEastNode == null ) {
					nodeSet.southEastNode = new AStarNode( nodePos );
				} else {
					nodeSet.southEastNode.SetPosition( nodePos );
				}
			}
			else {
				nodeSet.southEastNode = null;
			}
				
			// testing for a corner in the soutwest direction
			nodePos = box.GetSouthWestTop();
			safeDistance.Set( -Common.Character.RADIUS, 0.0f, Common.Character.RADIUS );
			nodePos += safeDistance;
			
			if ( !collisionManager.IsOccupied( nodePos ) ) {
				if( nodeSet.southWestNode == null ) {
					nodeSet.southWestNode = new AStarNode( nodePos );
				} else {
					nodeSet.southWestNode.SetPosition( nodePos );
				}
			} else {
				nodeSet.southWestNode = null;
			}
			
			// testing for a corner in the northeast direction
			nodePos = box.GetNorthEastTop();
			safeDistance.Set( Common.Character.RADIUS, 0.0f, -Common.Character.RADIUS );
			nodePos += safeDistance;
			
			if ( !collisionManager.IsOccupied( nodePos ) ) {
				if( nodeSet.northEastNode == null ) {
					nodeSet.northEastNode = new AStarNode( nodePos );
				} else {
					nodeSet.northEastNode.SetPosition( nodePos );
				}
			} else {
				nodeSet.northEastNode = null;
			}
				
			// testing for a corner in the northwest direction
			nodePos = box.GetNorthWestTop();
			safeDistance.Set( -Common.Character.RADIUS, 0.0f, -Common.Character.RADIUS );
			nodePos += safeDistance;
			if ( !collisionManager.IsOccupied( nodePos ) ) {
				if( nodeSet.northWestNode == null ) {
					nodeSet.northWestNode = new AStarNode( nodePos );		
				} else {
					nodeSet.northWestNode.SetPosition( nodePos );
				}
			} else {
				nodeSet.northWestNode = null;
			}
			
			return nodeSet;
		}
		
		protected void AddNodeSet( AStarGraph graph, NodeSet nodeSet, Box box ) {
			AddNode( graph, nodeSet.northEastNode, box );
			AddNode( graph, nodeSet.northWestNode, box );
			AddNode( graph, nodeSet.southEastNode, box );
			AddNode( graph, nodeSet.southWestNode, box );
		}

		protected void AddNode( AStarGraph graph, AStarNode node, Box box ) {
			float floorY;
			if( node != null && ShouldAddNodeInPosition( box, node.GetPosition(), out floorY ) ) {
				node.GetPosition().y = floorY;
				graph.AddNode( node );
			}

		}

		protected bool ConnectNodes( AStarGraph graph, AStarNode node, AStarNode candidateNode ) {
			if( node != candidateNode && 
			   candidateNode.IsAccessible() && 
			   graph.VectorsAreUnderMaxHeight( node.GetPosition(), candidateNode.GetPosition() ) &&
			   !node.HasChildren( candidateNode ) && 
			   graph.CheckClearPath( node.GetPosition(), candidateNode.GetPosition() ) ) {
				
				node.AddChild( candidateNode );
				candidateNode.AddChild( node );
				return true;
			}
			return false;
		}

		/// <summary>
		/// Indica se um nó deve ser adicionado a uma posição.
		/// </summary>
		/// <param name="box">
		/// A caixa envolvente do obstáculo.
		/// </param>
		/// <param name="position">
		/// A posição de onde partirá o raio. Sua posição y deve ser equivalente ao topo da caixa.
		/// </param>
		/// <param name="floorY">
		/// Valor retornado para a posição efetiva do nó a ser adicionado.
		/// </param>
		/// <returns>
		/// Um <see cref="System.Boolean"/> indicando se um nó deve ser inserido nesta posição.
		/// </returns>
		protected bool ShouldAddNodeInPosition( Box box, GameVector position, out float floorY ) {
			floorY = position.y;
			GameVector target = new GameVector( position.x, position.y - box.GetGameTransform().scale.y - Common.Character.KNEE_HEIGHT, position.z );
			HitInfo hitInfo;
			RaycastResult result = collisionManager.Raycast( position, target, out hitInfo );
			
			if ( result == RaycastResult.INTERCEPTION ) {
				floorY = hitInfo.position.y + Common.Character.KNEE_HEIGHT;
				return true;
			}
			
			return false;
		}
		
		
		#region A Star Graph Generator Test Cases
		
//		public class AStarGraphGenerationTestCase : TestCase {
//			
//			protected const string LEVEL_PATH = "Assets/Levels/teste_1" + Common.LEVEL_FILE_EXTENSION;
//			protected const int SECTOR_WITH_NO_NODE = 3;
//			protected const int SECTOR_WITH_SOUTHEAST_NODE = 1;
//			protected GameVector POSITION_OF_SOUTHEAST_CORNER = new GameVector( 1.0f, -2.290899f, -0.541715f );
//			protected const int SECTOR_WITH_NORTHWEST_NODE = 7;
//			protected GameVector POSITION_OF_NORTHWEST_CORNER = new GameVector( 4.0f, -2.290899f, 1.458286f );
//
//			protected GameWorld world;
//			protected CollisionManager collManager;
//			protected AStarGraphGenerator aStarGraphGen;
//			
//			
//			override public void SetUp() {
//				world = new GameWorld();
//				world.LoadStage( LEVEL_PATH );
//				collManager = world.GetCollisionManager();
//				aStarGraphGen = new AStarGraphGenerator( collManager );
//			}
//			
//			
//			[UnitTest]
//			public void GeneratingNodeSets() {
//				LevelSector sector =  collManager.GetSector( SECTOR_WITH_SOUTHEAST_NODE );
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//				Assert.NotNull( ns );
//			}
//			
//			[UnitTest]
//			public void SectorWithNoNode() {
//				LevelSector sector =  collManager.GetSector( SECTOR_WITH_NO_NODE );
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//				Assert.Null( ns.northEastNode );
//				Assert.Null( ns.northWestNode );
//				Assert.Null( ns.southEastNode );
//				Assert.Null( ns.southWestNode );
//			}
//			
//			[UnitTest]
//			public void SectorWithSoutheastNode() {
//				LevelSector sector =  collManager.GetSector( SECTOR_WITH_SOUTHEAST_NODE );
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//				
//				Assert.Null( ns.northEastNode );
//				Assert.Null( ns.northWestNode );
//				Assert.Null( ns.southWestNode );
//				
//				// southeast node 
//				Assert.NotNull( ns.southEastNode );
//				GameVector correctNodePos = POSITION_OF_SOUTHEAST_CORNER + new GameVector( -NODE_CORNER_DISTANCE, Common.CHARACTER_KNEE_HEIGHT, -NODE_CORNER_DISTANCE );
//				Assert.CheckEquals( correctNodePos, ns.southEastNode.GetPosition() );
//			}
//			
//			[UnitTest]
//			public void SectorWithNorthestNode() {
//				LevelSector sector =  collManager.GetSector( SECTOR_WITH_NORTHWEST_NODE );
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ws = aStarGraphGen.GetSectorNodeSet( sector );
//				
//				// null nodes
//				Assert.Null( ws.northEastNode );
//				Assert.Null( ws.southWestNode );
//				Assert.Null( ws.southEastNode );
//				
//				// northwest node 
//				Assert.NotNull( ws.northWestNode );
//				
//				GameVector correctNodePos = POSITION_OF_NORTHWEST_CORNER + new GameVector( NODE_CORNER_DISTANCE, Common.CHARACTER_KNEE_HEIGHT, NODE_CORNER_DISTANCE );
//				Assert.CheckEquals( correctNodePos, ws.northWestNode.GetPosition() );
//			}
//		}
//		
//		public class AStarGraphGeneratorNodeGenerationTestCase : TestCase {
//			
//			protected const string LEVEL_PATH = "Assets/Levels/teste_2" + Common.LEVEL_FILE_EXTENSION;
//			
//			protected GameWorld world;
//			protected CollisionManager collManager;
//			protected AStarGraphGenerator aStarGraphGen;
//			
//			
//			override public void SetUp() {
//				world = new GameWorld();
//				world.LoadStage( LEVEL_PATH );
//				collManager = world.GetCollisionManager();
//				aStarGraphGen = new AStarGraphGenerator( collManager );
//			}
//			
//			[UnitTest]
//			public void SectorsWithoutNodes() {
//				
//				int[] sectorsWithoutNodes = { 2, 3, 21, 47, 51, 61 };
//				
//				foreach( int sectorId in sectorsWithoutNodes ) {
//					LevelSector sector = collManager.GetSector( sectorId );
//				
//					aStarGraphGen.GenerateSectorNodeSet( sector );
//					NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//					
//					Assert.Null( ns.northEastNode );
//					Assert.Null( ns.northWestNode );
//					Assert.Null( ns.southEastNode );
//					Assert.Null( ns.southWestNode );
//				}
//			}
//			
//			[UnitTest]
//			public void Sector23Nodes() {
//				LevelSector sector = collManager.GetSector( 22 );
//				
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//				
//				Assert.NotNull( ns.northEastNode );
//				Assert.NotNull( ns.southWestNode );
//				
//				Assert.Null( ns.northWestNode );
//				Assert.Null( ns.southEastNode );
//			}
//			
//			[UnitTest]
//			public void Sector24Nodes() {
//				LevelSector sector = collManager.GetSector( 24 );
//				
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//				
//				Assert.NotNull( ns.northWestNode );
//				
//				Assert.Null( ns.northEastNode );
//				Assert.Null( ns.southWestNode );
//				Assert.Null( ns.southEastNode );
//			}
//			
//			[UnitTest]
//			public void Sector26Nodes() {
//				LevelSector sector = collManager.GetSector( 16 );
//				
//				aStarGraphGen.GenerateSectorNodeSet( sector );
//				NodeSet ns = aStarGraphGen.GetSectorNodeSet( sector );
//				
//				Assert.NotNull( ns.northEastNode );
//				Assert.NotNull( ns.southWestNode );
//				
//				Assert.Null( ns.southEastNode );
//				Assert.Null( ns.northWestNode );
//
//			}
//			
//			// TODO: teste de evitar nós no ar? (deve ser usada outra fase?)
//		}
//		
//		public class AStarGraphGenerationNodeVisibilityTestCase : TestCase {
//		
//			protected const string LEVEL_PATH = "Assets/Levels/teste_3" + Common.LEVEL_FILE_EXTENSION;
//			
//			protected GameWorld world;
//			protected CollisionManager collManager;
//			protected AStarGraphGenerator aStarGraphGen;
//			
//			
//			override public void SetUp() {
//				world = new GameWorld();
//				world.LoadStage( LEVEL_PATH );
//				collManager = world.GetCollisionManager();
//				aStarGraphGen = new AStarGraphGenerator( collManager );
//			}
//			
//			[UnitTest]
//			public void NodesConnectedToNodeZero() {
//				AStarGraph graph = aStarGraphGen.GenerateGraph();
//				List< AStarNode > nodes = graph.GetNodes();
//				AStarNode node = nodes[ 0 ];
//				
//				int[] connectedNodes = { 1, 6, 7, 8, 9, 10 };
//				
//				Assert.Equal( connectedNodes.Length, node.GetChildren().Count );
//
//				foreach( AStarNode childNode in node.GetChildren() ) {					
//					bool hasTheNode = false;
//					int nodeIndex = graph.GetNodeIndex( childNode );
//					
//					foreach( int index in connectedNodes ) {
//						if( index == nodeIndex ) {
//							hasTheNode = true;
//						}
//					}
//					Assert.True( hasTheNode, "Node 0 should not be connected to node " + nodeIndex );
//				}
//			}
//			
//			[UnitTest]
//			public void NodesConnectedToNodeSix() {
//				AStarGraph graph = aStarGraphGen.GenerateGraph();
//				List< AStarNode > nodes = graph.GetNodes();
//				Assert.CheckEquals( 12, nodes.Count );
//				AStarNode node = nodes[ 6 ];
//				
//				int[] connectedNodes = { 0, 2, 4, 5, 8, 9, 7 };
//				
//				Assert.Equal( connectedNodes.Length, node.GetChildren().Count );
//
//				foreach( AStarNode childNode in node.GetChildren() ) {
//					
//					bool hasTheNode = false;
//					int nodeIndex = graph.GetNodeIndex( childNode );
//					
//					foreach( int index in connectedNodes ) {
//						if( index == nodeIndex ) {
//							hasTheNode = true;
//						}
//					}
//					Assert.True( hasTheNode, "Node 6 should not be connected to node " + nodeIndex );
//				}
//			}
//			
//			[UnitTest]
//			public void NodesWhichShouldNotConnect() {
//				AStarGraph graph = aStarGraphGen.GenerateGraph();
//				
//				List< AStarNode > nodes = graph.GetNodes();
//				
//				foreach( AStarNode childNode in nodes[ 1 ].GetChildren() ) {
//					Assert.False( childNode == nodes[ 2 ] );
//				}
//			}
//		}
		
		#endregion
	}
}

