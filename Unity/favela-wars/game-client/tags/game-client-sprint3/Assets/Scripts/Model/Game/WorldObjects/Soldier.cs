using System;
using Action = GameCommunication.Action;
using GameController;
using GameCommunication;
using Utils;

namespace GameModel {
	
	[ Serializable ]
	public abstract class Soldier : Character {
	
		private Stance stance;
		
		public Stance GetStance() {
			return stance;
		}

		public void SetStance( Stance stance ) {
			///TODO: por enquanto, vamos fazer um toggle...
			//this.stance = stance;
			if ( this.stance == Stance.STANDING )
				this.stance = Stance.CROUCHING;
			else
				this.stance = Stance.STANDING;
		}
		
		public Soldier( int worldID, string assetBundleID ) : base( worldID, assetBundleID ) {
			stance = Stance.STANDING;
			//TODO: para testar o tiro
			SetHitPoints( 20 );
			SetAttackLevel( 2 );
		}
		
		public override Action GetPossibleActionsList () {
			Action toReturn;
			toReturn = base.GetPossibleActionsList ();
			//TODO: valores temporários
			Action newAction;
			newAction = new Action( Common.ActionTypes.MOVE );
			newAction.SetInteractionControllerClass( typeof( MoveInteractionController ) );
			toReturn.AddChild( newAction );
			
			newAction = new Action( Common.ActionTypes.SHOOT );
			newAction.SetInteractionControllerClass( typeof( ShootInteractionController ) );
			toReturn.AddChild( newAction );
			
			newAction = new Action( Common.ActionTypes.SET_STANCE );
			newAction.SetInteractionControllerClass( typeof( SetStanceInteractionController ) );
			toReturn.AddChild( newAction );			

			return toReturn;
		}

	}
	
}

