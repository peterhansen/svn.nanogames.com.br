using System;
using GameCommunication;


namespace GameModel {
	
	public class WaitHandler : Handler {
		
		protected WaitRequest waitRequest;
		
		
		public WaitHandler ( WaitRequest waitRequest ) {
			this.waitRequest = waitRequest;
		}
		
		
		public override Response GetResponse() {
			Response r = new Response();
			r.AddGameActionData( new WaitActionData( waitRequest.waitTime ) );
			return r;
		}
		
	}
}

