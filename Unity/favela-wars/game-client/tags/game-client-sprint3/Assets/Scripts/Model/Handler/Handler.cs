using System;
using GameCommunication;

namespace GameModel
{
	public abstract class Handler
	{
		public abstract Response GetResponse();
	}
}

