using System;
using System.IO;
using System.Collections.Generic;

using SharpUnit;

using GameCommunication;
using Utils;


namespace GameModel {
	
	public class AStarGraph {
		
		protected const int ASTAR_CUTOFF = 100;
		
		protected List< AStarNode > nodes = new List< AStarNode >();
		
		protected CollisionManager collisionManager;
	
		protected bool newGoalNode;
		protected AStarNode goalNode;
		
		protected bool newInitialNode;
		protected AStarNode initialNode;
		
		protected List< AStarNode > openNodes = new List< AStarNode >();
		protected List< AStarNode > closedNodes = new List< AStarNode >();
		
		protected List< GameVector > bestPath = new List< GameVector >();
		
		public AStarGraph ( CollisionManager collisionManager ) {
			this.collisionManager = collisionManager;
		}
		
		
		public void AddNode( AStarNode node ) {
			nodes.Add( node );
		}
	
		
		public List< AStarNode > GetNodes() {
			return nodes;
		}
		
		
		public int GetNodeIndex( AStarNode node ) {
			return nodes.IndexOf( node );
		}
		
		
		public List< GameVector > FindBestPath( WorldObject obj, GameVector goalPosition ) {
			GameVector initialPosition = obj.GetGameTransform().position;
			
			if( initialPosition.Equals( goalPosition ) ) {
				return null;
			}
			
			try {
				SetUp( obj, goalPosition );
			} catch( NodeNotMergedException ) {
				// TODO: informar para o usuário que o movimento é inválido
				Debugger.Log( "Movimento inválido!" );
				CleanUpMergedNodes();
				return null;
			}

			int iterations;
			for ( iterations = 0; iterations < ASTAR_CUTOFF; iterations++ ) {
				if( !ExecuteAStar() ) {
					break;
				}
			}
			
			if( bestPath == null ) {
				return null;
			} 
			
			List< GameVector > currentBestPath = new List<GameVector>();
			
			if ( bestPath == null )
				return null;
				
			
			foreach( GameVector node in bestPath ) {
				currentBestPath.Add( new GameVector( node ) );
			}
			
			return currentBestPath;
		}
		
		
		protected void SetUp( WorldObject obj, GameVector goalPosition ) {
			openNodes.Clear();
			closedNodes.Clear();
			
			bestPath = null;
			
			foreach( AStarNode node in nodes ) {
				node.SetParent( null );
			}
			
			goalNode = MergeNodeToGraph( goalPosition, null, out newGoalNode );
			initialNode = MergeNodeToGraph( obj.GetGameTransform().position, obj, out newInitialNode );
			
			initialNode.SetGAndH( 0, initialNode.GetPosition().DistanceTo( goalNode.GetPosition() ) );
			openNodes.Add( initialNode );
		}
				
		
		protected bool ExecuteAStar() {
			
			AStarNode bestNode = GetBestNodeFromOpenList();
		
			if( bestNode == null ) {
				// Não há nós na lista aberta. Isso quer dizer que nosso
				// A* não conseguiu encontrar um caminho.
				return false;
			}
			
			if( bestNode == goalNode ) {
				// Encontramos o melhor caminho!
				GenerateBestPath();
				return false;
			}
			
			GameVector bestNodePos = bestNode.GetPosition();
			
			foreach( AStarNode childNode in bestNode.GetChildren() ) {
				if( !childNode.IsAccessible() ) {
					continue;
				}
				
				GameVector childNodePos = childNode.GetPosition();
				
				float g = bestNode.GetG() + bestNodePos.DistanceTo( childNodePos );
				float h = childNodePos.DistanceTo( goalNode.GetPosition() );
				float f = g + h;
				
				int indexInOpen = openNodes.IndexOf( childNode );
				int indexInClosed = closedNodes.IndexOf( childNode );
				
				if( indexInOpen > -1 || indexInClosed > -1 ) {
					if( f < childNode.GetF() ) {
						childNode.SetGAndH( g, h );
						childNode.SetParent( bestNode );
					}
				}
				else {
					childNode.SetParent( bestNode );
					childNode.SetGAndH( g, h );
					openNodes.Add( childNode );
				}
			}
			
			openNodes.Remove( bestNode );
			closedNodes.Add( bestNode );
			
			return true;
		}
		
		public bool CheckSight( GameVector origin, GameVector target, RaycastFilterIgnore filter ) {
			if( !VectorsAreUnderMaxHeight( origin, target ) ) {
				return false;
			}
			
			RaycastResult result = collisionManager.RaycastAnyOfTheRays( origin, target, Common.Character.KNEE_HEIGHT * 0.3f, filter );
			return result == RaycastResult.NO_INTERCEPTION;
		}

		
		
		public bool CheckClearPath( GameVector origin, GameVector target ) {
			if( !VectorsAreUnderMaxHeight( origin, target ) ) {
				return false;
			}
			
			RaycastResult result = collisionManager.Raycast( origin, target, Common.Character.KNEE_HEIGHT * 0.3f );
			return result == RaycastResult.NO_INTERCEPTION;
		}
		
		
		public bool CheckClearPath( GameVector origin, GameVector target, RaycastFilter filter ) {
			if( !VectorsAreUnderMaxHeight( origin, target ) ) {
				return false;
			}
			
			RaycastResult result = collisionManager.Raycast( origin, target, Common.Character.KNEE_HEIGHT * 0.3f, filter );
			return result == RaycastResult.NO_INTERCEPTION;
		}
		
		
		public bool VectorsAreUnderMaxHeight( GameVector origin, GameVector target ) {
			return Math.Abs( origin.y - target.y ) < Common.LEVEL_LAYER_HEIGHT;
		}
		
		
		protected AStarNode MergeNodeToGraph( GameVector position, WorldObject filteredObj, out bool isNewNode ) {
			isNewNode = true;
			AStarNode newNode = null;
			
			foreach ( AStarNode node in nodes ) {
				if ( node.GetPosition().Equals( position ) ) {
					isNewNode = false;
					newNode = node;
					break;
				}
			}
			
			if( isNewNode ) {
				newNode = new AStarNode( position, ( filteredObj != null ) ? filteredObj.GetWorldID() : -1 );
				nodes.Add( newNode );
				
				bool isConnected = false;
				
				foreach( AStarNode node in nodes ) {
					
					RaycastFilterIgnore filter = null;
					if( filteredObj != null ) {
						filter = new RaycastFilterIgnore();
						filter.Ignore( filteredObj.GetBox() );
					}
					
					if( !node.Equals( newNode ) && CheckClearPath( node.GetPosition(), newNode.GetPosition(), filter ) ) {
						node.AddChild( newNode );
						newNode.AddChild( node );						
						isConnected = true;
					}
				}
				
				if( !isConnected ) {
					// não foi possível adicionar esse nó ao grafo
					throw new NodeNotMergedException();
				}
			}
			
			return newNode;
		}

		
		protected AStarNode GetBestNodeFromOpenList() {
			float minCost = float.MaxValue;
			AStarNode minCostNode = null;
			
			foreach( AStarNode node in openNodes ) {
				
				if( node.GetF() < minCost ) {
					minCost = node.GetF();
					minCostNode = node;
				}
			}
	
			return minCostNode;
		}
		
		
		protected void GenerateBestPath() {
			bestPath = new List< GameVector >();
			
			AStarNode node = goalNode;
			
			while( node.GetParent() != null ) {
				bestPath.Add( node.GetPosition() );
				node = node.GetParent();
			}
			
			bestPath.Add( node.GetPosition() );
			bestPath.Reverse();

			CleanUpMergedNodes();
		}
		
		protected void CleanUpMergedNodes() {
			if( newInitialNode ) {
				nodes.Remove( initialNode );
			}
			
			if( newGoalNode ) {
				nodes.Remove( goalNode );
			}
		}
}
	
	#region AStar Graph Test Case
	
	/*
	
	public class AStarGraphTestCase : TestCase {
		
		protected readonly string LEVEL_NAME = "teste_1";
		protected const int NUMBER_OF_NODES = 4;
		protected const int NUMBER_OF_ARCS_PER_NODE = 2;
		
		protected GameVector outsideLevelSectorPosition = new GameVector( 200.0f, -100.0f, 23.0f );
		
		protected GameWorld world;
		protected CollisionManager collManager;
		protected AStarGraphGenerator aStarGraphGen;
		protected AStarGraph aStarGraph;
		
		
		override public void SetUp() {
			world = new GameWorld();
			world.LoadStage( LEVEL_NAME );
			collManager = world.GetCollisionManager();
			aStarGraphGen = new AStarGraphGenerator( collManager );
			aStarGraph = aStarGraphGen.GenerateGraph();
		}
			
			
		override public void TearDown() {
		}
		
		
		[UnitTest]
		public void NodeNumber() {
			Assert.Equal( NUMBER_OF_NODES, aStarGraph.GetNodes().Count );
		}
				
		[UnitTest]
		public void NumberOfArcsPerNode() {
			foreach( AStarNode node in aStarGraph.GetNodes() ) {
				Assert.Equal( NUMBER_OF_ARCS_PER_NODE, node.GetChildren().Count );
			}
		}
		
		[UnitTest]
		public void CantHaveNodesInTheSamePosition() {
			
			foreach( AStarNode node1 in aStarGraph.GetNodes() ) {
				foreach( AStarNode node2 in aStarGraph.GetNodes() ) {
				
					if( node1 != node2 ){
						Assert.False( node1.GetPosition().Equals( node2.GetPosition() ) );
					}
				}
			}
		}
		
		[UnitTest]
		public void NodesCorrectConnection() {
			
			foreach( AStarNode node in aStarGraph.GetNodes() ) {
				
				List< AStarNode > connectedNodes = node.GetChildren();
					
				switch( aStarGraph.GetNodeIndex( node ) ) {
					
				case 0:
				case 3:
					Assert.True( connectedNodes.Count > 1 );
					Assert.Equal( 1, aStarGraph.GetNodeIndex( connectedNodes[ 0 ] ) );
					Assert.Equal( 2, aStarGraph.GetNodeIndex( connectedNodes[ 1 ] ) );
					break;
					
				case 1:
				case 2:
					Assert.Equal( 3, aStarGraph.GetNodeIndex( connectedNodes[ 1 ] ) );
					Assert.Equal( 0, aStarGraph.GetNodeIndex( connectedNodes[ 0 ] ) );
					break;
				
				}
			}
		}
		
		[UnitTest]
		public void CantMoveOutsideLevelSector() {
			
			List< AStarNode > nodes = aStarGraph.GetNodes();
			List< GameVector > bestPath = aStarGraph.FindBestPath( nodes[ 0 ].GetPosition(), outsideLevelSectorPosition );
			
			Assert.Null( bestPath );
		}
		
		[UnitTest]
		public void CantMoveToCurrentPosition() {
			
			List< AStarNode > nodes = aStarGraph.GetNodes();
			List< GameVector > bestPath = aStarGraph.FindBestPath( nodes[ 0 ].GetPosition(), nodes[ 0 ].GetPosition() );
			
			Assert.Null( bestPath );
		}
		
		[UnitTest]
		public void SimpleTwoNodePath() {
			
			List< AStarNode > nodes = aStarGraph.GetNodes();
			List< GameVector > bestPath = aStarGraph.FindBestPath( nodes[ 0 ].GetPosition(), nodes[ 1 ].GetPosition() );
			
			Assert.NotNull( bestPath );
			Assert.Equal( 2, bestPath.Count );
			Assert.CheckEquals( nodes[ 0 ].GetPosition(), bestPath[ 0 ] );
			Assert.CheckEquals( nodes[ 1 ].GetPosition(), bestPath[ 1 ] );
		}
		
		[UnitTest]
		public void PathAroundTheCorner() {
			
			List< AStarNode > nodes = aStarGraph.GetNodes();
			
			GameVector deltaV = new GameVector( -0.2f, 0.0f, 0.25f );
			List< GameVector > bestPath = aStarGraph.FindBestPath( nodes[ 0 ].GetPosition() + deltaV, nodes[ 3 ].GetPosition() );
			
			Assert.NotNull( bestPath );
			Assert.Equal( 3, bestPath.Count );
			Assert.CheckEquals( nodes[ 0 ].GetPosition() + deltaV, bestPath[ 0 ] );
			Assert.CheckEquals( nodes[ 2 ].GetPosition(), bestPath[ 1 ] );
			Assert.CheckEquals( nodes[ 3 ].GetPosition(), bestPath[ 2 ] );
		}
	}
	
	public class AStarGraphTestCase2 : TestCase {
		
		protected readonly string LEVEL_PATH = Path.Combine( Common.PATH_LEVEL_FILE, "teste_3" + Common.LEVEL_FILE_EXTENSION );
		
		protected GameWorld world;
		protected CollisionManager collManager;
		protected AStarGraphGenerator aStarGraphGen;
		protected AStarGraph aStarGraph;
		
		public override void SetUp() {
			world = new GameWorld();
			world.LoadStage( LEVEL_PATH );
			collManager = world.GetCollisionManager();
			aStarGraphGen = new AStarGraphGenerator( collManager );
			aStarGraph = aStarGraphGen.GenerateGraph();
		}
			
		public override void TearDown() {
		}
		
		[UnitTest]
		public void PathShouldBeClear() {
			GameVector a = new GameVector( -72.69505f, -14.32582f, 58.67208f );
			GameVector b = new GameVector( -99.24321f, -14.32582f, 46.78305f );
			
			Assert.True( aStarGraph.CheckClearPath( a, b ) );
			Assert.True( aStarGraph.CheckClearPath( b, a ) );
		}
	}
	
	*/
	#endregion
}

#region Excecoes
public class NodeNotMergedException : Exception {
	
	public NodeNotMergedException() : base( "Could not merge node to A* Graph" ) {
	}
}

#endregion