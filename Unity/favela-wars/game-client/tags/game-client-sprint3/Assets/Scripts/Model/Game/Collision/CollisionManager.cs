using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Utils;
using SharpUnit;
using GameCommunication;


namespace GameModel {
	
	public enum RaycastResult {
		NO_INTERCEPTION,
		INTERCEPTION
	};
	
	/// <summary>
	/// Centralizador de tratamento de colis�es
	/// </summary>
	public class CollisionManager {
		
		private List< Box > boxes = new List< Box >();
		
		protected const float SHOT_WIDTH = 0.005f;
		
		
		public CollisionManager( string filename ) {
			LoadMap( filename );
		}
		
		
		/// <summary>
		/// Construtor padr�o, para quando se vai usar a classe para gera��o de caixas envolventes, e n�o apenas para sua consulta.
		/// </summary>
		public CollisionManager() {
		}
		
		
		public List< Box > GetBoxes() {
			return boxes;
		}
		
		
		public void AddBox( WorldObject wo ) {
			AddBox( wo.GetBox() );
		}
		
		
		public Box GetBoxAt( GameVector point ) {
			foreach ( Box b in boxes ) {
				if ( b.Contains( point ) )
				    return b;
			}
			
			return null;
		}
				
		
		public bool IsOccupied( GameVector point ) {
			return GetBoxAt( point ) != null;
		}
		
		
		public void AddBox( Box box ) {
			boxes.Add( box );
		}
		

		public RaycastResult Shoot( WorldObject shooter, GameVector target, out HitInfo hitInfo ) {
			RaycastFilterIgnore filter = new RaycastFilterIgnore();
			filter.Ignore( shooter.GetBox() );
			return Raycast( shooter.GetGameTransform().position, target, SHOT_WIDTH, out hitInfo, filter );
		}
		

		public RaycastResult Raycast( GameVector origin, GameVector target ) {
			foreach( Box box in boxes ) {		
				RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
				
				if( interceptionReply.exit != null || interceptionReply.entry != null ) {
					return RaycastResult.INTERCEPTION;
				}
			}

			return RaycastResult.NO_INTERCEPTION;
		}
	
		public RaycastResult RaycastAnyOfTheRays( GameVector origin, GameVector target, float width, RaycastFilterIgnore filter ) {
			
			if( (target - origin).IsZero() ) {
				return RaycastResult.NO_INTERCEPTION;
			}
			
			GameVector rightOrigin, rightDestination;
			GameVector leftOrigin, leftDestination;
			GenerateSideRays( origin, target, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
			
			if( Raycast( rightOrigin, rightDestination, filter ) == RaycastResult.INTERCEPTION  && 
			   Raycast( leftOrigin, leftDestination, filter ) == RaycastResult.INTERCEPTION ) {
				return RaycastResult.INTERCEPTION;
			}
			
			return RaycastResult.NO_INTERCEPTION;
		}		
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width ) {
			if( (target - origin).IsZero() ) {
				return RaycastResult.NO_INTERCEPTION;
			}
			
			GameVector rightOrigin, rightDestination;
			GameVector leftOrigin, leftDestination;
			GenerateSideRays( origin, target, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
			
			if( Raycast( rightOrigin, rightDestination ) == RaycastResult.INTERCEPTION ) {
				return RaycastResult.INTERCEPTION;
			}
				
			if( Raycast( leftOrigin, leftDestination ) == RaycastResult.INTERCEPTION ) {
				return RaycastResult.INTERCEPTION;
			}
			
			return RaycastResult.NO_INTERCEPTION;
		}
		
		public RaycastResult Raycast( GameVector origin, GameVector target, RaycastFilter filter ) {
			
			List< Box > filteredBoxes = filter == null? boxes: filter.FilterBoxes( boxes );
			
			foreach( Box box in filteredBoxes ) {		
				RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
				
				if( interceptionReply.exit != null || interceptionReply.entry != null ) {
					return RaycastResult.INTERCEPTION;
				}
			}

			return RaycastResult.NO_INTERCEPTION;
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width, RaycastFilter filter ) {
			if( (target - origin).IsZero() ) {
				return RaycastResult.NO_INTERCEPTION;
			}
			
			GameVector rightOrigin, rightDestination;
			GameVector leftOrigin, leftDestination;
			GenerateSideRays( origin, target, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
			
			if( Raycast( rightOrigin, rightDestination, filter ) == RaycastResult.INTERCEPTION || 
			   Raycast( leftOrigin, leftDestination, filter ) == RaycastResult.INTERCEPTION) {
				return RaycastResult.INTERCEPTION;
			}
			
			return RaycastResult.NO_INTERCEPTION;
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, out HitInfo hitInfo ) {
			return Raycast( origin, target, out hitInfo, null );
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width, out HitInfo hitInfo ) {
			return Raycast( origin, target, width, out hitInfo, null );
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, out HitInfo hitInfo, RaycastFilter filter ) {
			RayBoxInterception entryInterception = null;
			RayBoxInterception exitInterception = null;
			
			List< Box > filteredBoxes = filter == null? boxes: filter.FilterBoxes( boxes );
			
			foreach( Box box in filteredBoxes ) {		
				RayBoxInterceptionReply interceptionReply = box.Intercept( origin, target );
								
				entryInterception = UpdateInterception( entryInterception, interceptionReply.entry );
				exitInterception = UpdateInterception( exitInterception, interceptionReply.exit );
			}
			
			hitInfo = new HitInfo();
			
			if( entryInterception != null ) {
				hitInfo.position = entryInterception.point;
				hitInfo.worldObjectId = entryInterception.worldObjectID;
			} else if( exitInterception != null ) {
				hitInfo.position = exitInterception.point;
				hitInfo.worldObjectId = exitInterception.worldObjectID;
			} else {
				hitInfo.position = target;
			}
			
			return ( entryInterception == null && exitInterception == null )? RaycastResult.NO_INTERCEPTION : RaycastResult.INTERCEPTION;
		}
		
		
		public RaycastResult Raycast( GameVector origin, GameVector target, float width, out HitInfo hitInfo, RaycastFilter filter ) {
			
			GameVector direction = (target - origin);
			if( direction.IsZero() ) {
				hitInfo = new HitInfo();
				hitInfo.position = origin;
				hitInfo.worldObjectId = Common.NULL_WORLDOBJECT;
				return RaycastResult.INTERCEPTION;
			}
			
			GameVector rightOrigin, rightDestination;
			GameVector leftOrigin, leftDestination;
			GenerateSideRays( origin, target, width, out rightOrigin, out rightDestination, out leftOrigin, out leftDestination );
			
			HitInfo hitRight;
			RaycastResult rightPathResult = Raycast( rightOrigin, rightDestination, out hitRight, filter );
			
			HitInfo hitLeft;
			RaycastResult leftPathResult = Raycast( leftOrigin, leftDestination, out hitLeft, filter );
			
			hitInfo = new HitInfo();
			if( rightPathResult == RaycastResult.NO_INTERCEPTION && leftPathResult == RaycastResult.NO_INTERCEPTION ) {
				
				hitInfo.position = target;
				hitInfo.worldObjectId = Common.NULL_WORLDOBJECT;
				return RaycastResult.NO_INTERCEPTION;
			} else {
				// pegando caminho que colidiu primeiro
				float rightNorm = ( hitRight.position - rightOrigin ).Norm();
				float leftNorm = ( hitLeft.position - leftOrigin ).Norm();
				
				HitInfo closerHit = rightNorm < leftNorm? hitRight: hitLeft;

				hitInfo.worldObjectId = closerHit.worldObjectId;
				float norm = Math.Min( rightNorm, leftNorm );
				hitInfo.position = origin + direction.Unitary() * norm;
				
				return RaycastResult.INTERCEPTION;
			}
		}
		
		
		// TODO esse m�todo ainda nao retorna um filtro... deveria retornar
		public List< Box > CreateFilterCone( GameVector apex, GameVector axis, float angle ) {
			List< Box > objects = new List< Box >();
			
			foreach( Box box in boxes ) {
				GameVector positionFromApex = box.GetPosition() - apex;
				
				// caixa est� no �pice do cone
				if( positionFromApex.IsZero() ) {
					objects.Add( box );
					continue;
				}
				
				bool sameDirection;
				GameVector projectedPosition = positionFromApex.Project( axis, out sameDirection );
				if( sameDirection && 
				   NanoMath.LessEquals( projectedPosition.Norm(), axis.Norm() ) &&
				   NanoMath.LessEquals( positionFromApex.AngleBetween( axis ), angle ) ) {
					objects.Add( box );
				}
			}
			
			return objects.Count > 0? objects: null;
		}
		
		
		/// <summary>
		/// Salva mapa para o disco. C�digo ja existia, mas agora a ideia � unificar, para facilitar testagem.
		/// </summary>
		/// <param name="path"> 
		/// � <see cref="System.String"/> relativa � raiz do projeto.
		/// </param>
		public void SaveMap( string missionName ) {
			MapCollisionData collisionData = new MapCollisionData( missionName, boxes );
			collisionData.ExportToFile();
		}	
		
		
		protected void LoadMap( string filename ) {
			MapCollisionData collisionData = MapCollisionData.ImportFromFile( filename );
			this.boxes = collisionData.GetBoxes();
		}
		
	
		protected RayBoxInterception UpdateInterception( RayBoxInterception current, RayBoxInterception candidate ) {
			if( candidate != null ) {
				if( current == null || candidate.t < current.t ) {
					return candidate;
				}
			}
			return current;
		}
		
		protected void GenerateSideRays( 
		                                GameVector origin, GameVector target, float width,
		                                out GameVector rightOrigin, out GameVector rightDestination, 
		                                out GameVector leftOrigin, out GameVector leftDestination ) {
			GameVector direction = target - origin;
			
			GameVector pathUp = new GameVector();
			GameVector rightDir = new GameVector();
			
			pathUp = GameVector.YAxis - GameVector.YAxis.Project( direction );
				
			rightDir = direction.Cross( pathUp ).Unitary();
			rightDir *= width * 0.5f;
			
			GameVector leftDir = rightDir * -1.0f;
			
			rightOrigin = origin + rightDir;
			rightDestination = target + rightDir;
			leftOrigin = origin + leftDir;
			leftDestination = target + leftDir;
		}
		
		
		public override int GetHashCode() { // TODO
			return ToString().GetHashCode();
		}		
		
		
		public bool CorrectDestination( Box worldObjectBox, GameVector destination, out GameVector correctedDestination ) {
			
			correctedDestination = new GameVector();
			
			Box destinationBox = new Box( worldObjectBox.GetGameTransform() );
			destinationBox.GetGameTransform().position.Set( destination );
			
			RemoveBoxFromCollision( destinationBox );
			
			if( LandBox( destinationBox ) ) {
				correctedDestination.Set( destinationBox.GetGameTransform().position );
				return true;
			}
			
			return false;
		}

		
		protected void RemoveBoxFromCollision( Box box ) {
			RaycastFilterIgnore filter = new RaycastFilterIgnore();
			filter.Ignore( box );
			
			foreach ( Box b in filter.FilterBoxes( boxes ) ) {
				if ( b.Intersects( box ) ) {
						b.BounceBackIfIntersects( box );
				}
			}
		}
		
		
		protected bool LandBox( Box box ) {
			GameVector origin = box.GetGameTransform().position;
			
			GameVector target = new GameVector( origin );
			target -= GameVector.YAxis * 2.0f * box.GetGameTransform().scale.y;
		
			HitInfo hit;
			if( Raycast( origin, target, out hit ) == RaycastResult.INTERCEPTION ) {
				box.GetGameTransform().position = hit.position + GameVector.YAxis * box.GetGameTransform().scale.y * 0.5f;
				return true;
			} else {
				return false;
			}
		}
		
		
		public Box GetBoxByWorldObjectId( int worldObjectID ) {
			foreach ( Box b in boxes ) {
				if ( b.GetWorldObjectID() == worldObjectID )
					return b;
			}
			
			return null;
		}
		
	}
	

	/// <summary>
	/// Caso de teste
	/// </summary>
//	public class CollisionManagerTestLevel1TestCase : TestCase {
//		
//		protected const string NONEXISTANT_LEVEL = "Assets/Levels/world_file_which_does_not_exists" + Common.LEVEL_FILE_EXTENSION;
//				
//		protected const string LEVEL_NAME = "teste_1";
//		protected const string LEVEL_NAME_WHICH_DOES_NOT_EXISTS = "world_file_which_does_not_exists";
//
//		protected const string TEST_LEVEL = "Assets/Levels/teste_1" + Common.LEVEL_FILE_EXTENSION;
//		protected const int NUMBER_OF_SECTORS = 8;
//		protected CollisionManager collisionManager;
//		
//		public override void SetUp() {
//			collisionManager = new CollisionManager( TEST_LEVEL ); 
//		}
//		
//		
//		[UnitTest]
//		public void NotFindindWorldFile() {
//			Assert.ExpectException( new FileNotFoundException() );
//			new CollisionManager( NONEXISTANT_LEVEL );
//		}
//		
//		[UnitTest]
//		public void CorrectNumberOfSectors() {
//			Assert.True( collisionManager.GetTotalSectors() == NUMBER_OF_SECTORS + 1 );
//		}
//		
//		
//		[ UnitTest ]
//		public void TestHardProjection() {
//			LevelSector sector = LevelSector.MakeSectorAt( -69.57163f, -69.57163f + 0.2976303f, -15.0f,-15.0f + 30.0f, 138.4975f, 138.4975f + 2.475281f );
//			Assert.CheckEquals( new GameVector( -69.57163f, -14.7f, 139.1895f ), sector.GetProjection( new GameVector( -155.4f, -14.7f, 137.0f ), new GameVector( -69.274f, -14.7f, 139.1971f ) ) );
//		}
//
//	
//	
//			
//		[ UnitTest ]
//		public void TestAddCollisionCell() {
//			CollisionManager cm;
//			LevelSector sector;
//
//			cm = new CollisionManager();
//			Assert.Equal( 0, cm.GetTotalSectors() );
//			cm.SaveMap( "tmp" + Common.LEVEL_FILE_EXTENSION, true );
//			cm = new CollisionManager( "tmp" + Common.LEVEL_FILE_EXTENSION );
//			Assert.Equal( 1, cm.GetTotalSectors() );
//			cm = new CollisionManager();
//			sector = new LevelSector();
//			sector.SetShape( 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f );
//			cm.AddNavigableCell( sector );
//		}
//		
//		
//		[ UnitTest ]
//		public void TestGetSectorFromGameVector() {
//			CollisionManager cm;
//			
//			cm = new CollisionManager();
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 0.0f, 2.0f, 0.0f, 2.0f ) );
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 2.0f, 2.0f, 0.0f, 2.0f, 0.0f, 2.0f ) );
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 2.0f, 2.0f, 0.0f, 2.0f ) );
//			cm.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 0.0f, 2.0f, 2.0f, 2.0f ) );	
//		}
//
//		
//		[ UnitTest ]
//		public void TestRayInterceptionMustHaveSameResultBothWays() {
//		
//			GameVector origin = new GameVector( 5.768162f, -1.8f, 2.921593f );
//			GameVector destiny = new GameVector( 5.134628f, -1.8f, 2.9f );
//			int sec1 = collisionManager.GetSectorFromGameVector( origin );
//			int sec2 = collisionManager.GetSectorFromGameVector( destiny );
//			
//			List< int > path1 = collisionManager.GetPath( origin, destiny );
//			Assert.Equal( sec2, path1[ path1.Count - 1 ] );
//			List< int > path2 = collisionManager.GetPath( destiny, origin );
//			Assert.Equal( sec1, path2[ path2.Count - 1 ] );
//			
//			Assert.Equal( path1.Count, path2.Count );
//		}
//		
//		
//		[UnitTest]
//		public void LevelSectorPathShouldBeBlocked() {
//			GameVector destiny = new GameVector( 5.0f, -1.8f, 2.458286f );
//			GameVector origin = new GameVector( 0.0f, -1.8f, -1.541715f );
//			
////			GameVector hit = collisionManager.GetHitPoint( origin, destiny );
////			
////			UnityEngine.Vector3 originVec = new UnityEngine.Vector3( origin.x, origin.y, origin.z );
////			UnityEngine.Vector3 destinyVec = new UnityEngine.Vector3( destiny.x, destiny.y, destiny.z );
////			UnityEngine.Debug.DrawLine( originVec, destinyVec, UnityEngine.Color.yellow, 10 );
//			
//			List< int > path = collisionManager.GetPath( origin, destiny );
//			int finalSector = collisionManager.GetSectorFromGameVector( destiny );
//			Assert.Equal( 2, path.Count );
//			Assert.False ( finalSector == path[ path.Count - 1 ] );
//		}
//		
//		
//		[ UnitTest ]
//		public void ShouldMoveDirectly() {
//			
//			GameVector gv1 = new GameVector( 0.3f, -1.99089f, 2.158286f );
//			GameVector gv2 = new GameVector( 5.229355f, -1.8f, 2.643562f );
//			int s1;
//			int s2;
//
//			s1 = collisionManager.GetSectorFromGameVector( gv1 );
//			s2 = collisionManager.GetSectorFromGameVector( gv2 );
//			List< int > path = collisionManager.GetPath( gv1, gv2 );
//			Assert.CheckEquals( path[ path.Count - 1 ], s2 ); 
//			Assert.CheckEquals( path[ 0 ], s1 ); 
//		}
//		
//
//		protected List< int > getPathForTestBetweenSectors( int s1, int s2 ) {
//			GameVector gv1 = collisionManager.GetSector( s1 ).GetCenter();
//			GameVector gv3 = collisionManager.GetSector( s2 ).GetCenter();
//			return collisionManager.GetPath( gv1, gv3 );
//		}
//		
//		//testado E
//		[ UnitTest ]
//		public void TestParametricProjectionAt() {
//			List< int > path = getPathForTestBetweenSectors( 1, 3 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 1, path[ 0 ] );
//			Assert.Equal( 3, path[ 1 ] );
//		}		
//		
//		//testado E
//		[ UnitTest ]
//		public void TestAnotherParametricProjectionAt() {
//			List< int > path = getPathForTestBetweenSectors( 6, 8 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 6, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//		}	
//		
//		//testado W
//		[ UnitTest ]
//		public void TestWhatGoesAroundComesAround() {
//			List< int > path = getPathForTestBetweenSectors( 8, 6 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 8, path[ 0 ] );
//			Assert.Equal( 6, path[ 1 ] );
//		}			
//		
//		//testado W
//		[ UnitTest ]
//		public void TestYetAnotherPath() {
//			List< int > path = getPathForTestBetweenSectors( 7, 8 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 7, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//		}	
//		
//		//testado N
//		[ UnitTest ]
//		public void TestFinallyANewPath() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 4, 7 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 4, path[ 0 ] );
//			Assert.Equal( 7, path[ 1 ] );
//		}
//		
//		//testado N
//		[ UnitTest ]
//		public void PathBetweenLevelSectors1and6() {
//			List< int >	path = getPathForTestBetweenSectors( 1, 6 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 1, path[ 0 ] );
//			Assert.Equal( 5, path[ 1 ] );
//			Assert.Equal( 6, path[ 2 ] );
//		}
//		
//		//testado S
//		[ UnitTest ]
//		public void TestPathFrom4to2() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 4, 2 );
//			Assert.Equal( 2, path.Count );
//			Assert.Equal( 4, path[ 0 ] );
//			Assert.Equal( 2, path[ 1 ] );
//		}		
//		
//		//testado S
//		[ UnitTest ]
//		public void TestPathFrom6to1() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 6, 1 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 6, path[ 0 ] );
//			Assert.Equal( 5, path[ 1 ] );
//			Assert.Equal( 1, path[ 2 ] );
//		}		
//
//		//testado E
//		[ UnitTest ]
//		public void TestPathFrom1o2() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 1, 2 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 1, path[ 0 ] );
//			Assert.Equal( 3, path[ 1 ] );
//			Assert.Equal( 2, path[ 2 ] );
//		}		
//
//		
//		//testado W
//		[ UnitTest ]
//		public void TestPathFrom2to1() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 2, 1 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 2, path[ 0 ] );
//			Assert.Equal( 3, path[ 1 ] );
//			Assert.Equal( 1, path[ 2 ] );
//		}				
//		
//		//testado E
//		[ UnitTest ]
//		public void TestPathFrom7to6() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 7, 6 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 7, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//			Assert.Equal( 6, path[ 2 ] );
//		}		
//
//		
//		//testado W
//		[ UnitTest ]
//		public void TestPathFrom6to7() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 6, 7 );
//			Assert.Equal( 3, path.Count );
//			Assert.Equal( 6, path[ 0 ] );
//			Assert.Equal( 8, path[ 1 ] );
//			Assert.Equal( 7, path[ 2 ] );
//		}
//		
//
//		[ UnitTest ]
//		public void TestPathFrom3to7() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 3, 7 );
//			Assert.Equal( 1, path.Count );
//			Assert.Equal( 3, path[ 0 ] );
//		}
//		
//		[ UnitTest ]
//		public void TestPathFrom5to7() {
//			List< int > path;
//			path = getPathForTestBetweenSectors( 5, 7 );
//			Assert.Equal( 1, path.Count );
//			Assert.Equal( 5, path[ 0 ] );
//		}
//	}	
//
//			
//	public class CollisionManagerTestLevel3TestCase : TestCase {
//			
//			protected const string LEVEL_NAME = "teste_3";
//			protected CollisionManager collisionManager;
//			
//			public override void SetUp() {
//				collisionManager = new CollisionManager( Path.Combine( Common.PATH_LEVEL_FILE, LEVEL_NAME + Common.LEVEL_FILE_EXTENSION ) ); 
//			}
//			
//			[ UnitTest ]
//			public void PathShouldBeAvailable() {
//				GameVector a = new GameVector( -57.4f, -14.7f, 139.5f );
//				GameVector b = new GameVector( -155.4f, -14.7f, 137.0f );
//				List< int > path = collisionManager.GetPath( a, b );
//				Assert.NotNull( path );
//				Assert.Equal( 6, path.Count );
//			}
//		
//		 
//
//			[UnitTest]
//			public void PathShouldBeAvailable3() {
//				GameVector a = new GameVector( -155.5933f, -14.7f, 137.3793f );
//				GameVector b = new GameVector( -69.03856f, -14.32582f, -23.95451f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetPath( a, b, out hit );
//				Assert.NotNull( hit.position );
//			}
//
//		
//			[UnitTest]
//			public void PathShouldBeAvailable2() {
//				GameVector a = new GameVector( -68.40066f, -14.7f, 101.9524f );
//				GameVector b = new GameVector( -132.9193f, -14.32582f, 41.47237f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetPath( a, b, out hit );
//				Assert.NotNull( hit.position );
//			}
//		
//			[UnitTest]
//			public void PathShouldBeAvailable4() {
//				GameVector a = new GameVector( -72.69505f, -14.32582f, 58.67208f );
//				GameVector b = new GameVector( -99.24321f, -14.32582f, 46.78305f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetPath( a, b, out hit );
//			
//				Assert.NotNull( path );
//				Assert.Equal( 2, path.Count );
//				Assert.Equal( collisionManager.GetSectorFromGameVector( a ), path[ 0 ] );
//				Assert.Equal( collisionManager.GetSectorFromGameVector( b ), path[ 1 ] );
//				Assert.CheckEquals( b, hit.position );
//			}
//		
//			[UnitTest]
//			public void CantShootThroughAVerySmallHole() {
//				GameVector origin = new GameVector( -46.11366f, -14.32582f, 84.98982f );
//				GameVector target = new GameVector( -78.83429f, -14.32582f, 51.41371f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				int expected = path[ path.Count - 1 ];
//				int got = collisionManager.GetSectorFromGameVector( hit.position );
//				
//				Assert.False( expected == got, "Sectors " + expected + " and " + got + " should be different" );
//			}
//		}
//	
//	public class CollisionManagerTrajectoryTestCase : TestCase {
//		
//			protected const string LEVEL_NAME = "teste_1";
//			protected CollisionManager collisionManager;
//			
//			
//			public override void SetUp() {
//				collisionManager = new CollisionManager( Path.Combine( Common.PATH_LEVEL_FILE, LEVEL_NAME + Common.LEVEL_FILE_EXTENSION ) );
//			}
//			
//			[UnitTest]
//			public void NullTrajectory() {
//				
//				GameVector origin = new GameVector( 5.0f, -1.29089f, -1.541715f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, origin, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 1, path.Count );
//				Assert.Equal( 2, path[ 0 ] );
//				
//				Assert.CheckEquals( origin, hit.position );
//				Assert.Equal( Common.NULL_WORLDOBJECT, hit.worldObjectId );
//			}
//			
//			[UnitTest]
//			public void SimpleTrajectory() {
//				GameVector origin = new GameVector( 5.0f, -1.29089f, -1.541715f );
//				GameVector target = new GameVector( 5.0f, -1.29089f, 2.458286f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 3, path.Count );
//				
//				Assert.Equal( 2, path[ 0 ] );
//				Assert.Equal( 4, path[ 1 ] );
//				Assert.Equal( 7, path[ 2 ] );
//				
//				Assert.CheckEquals( target, hit.position );
//				Assert.Equal( Common.NULL_WORLDOBJECT, hit.worldObjectId );
//			}
//			
//			[UnitTest]
//			public void HaltedTrajectory() {
//				GameVector origin = new GameVector( 2.5f, -1.29089f, -1.541715f );
//				GameVector target = new GameVector( 2.5f, -1.29089f, 0.45f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 1, path.Count );
//				
//				Assert.Equal( 3, path[ 0 ] );
//				Assert.Equal( Common.WALL_WORLDOBJECT, hit.worldObjectId );
//				
//				GameVector hitOnWall = new GameVector( 2.5f, -1.29089f, -0.541715f );			
//				Assert.CheckEquals( hitOnWall, hit.position );
//			}
//			
//			[UnitTest]
//			public void TrajectoryHittingAnotherCharacter() {
//				
//				// pretending that the sector is a character
//				int mockWorldObjectId = 4;
//				LevelSector characterSector = collisionManager.GetSector( 6 );
//				List< int > connectedSectors = new List< int >();
//				
//				foreach( LevelSector.Direction dir in LevelSector.Directions ) {
//					int sectorIndex = characterSector.GetLink( dir );
//					if( sectorIndex != 0 ) {
//						connectedSectors.Add( sectorIndex );
//					}
//				}
//				
//				foreach( int sectorIndex in connectedSectors ) {
//					LevelSector sector = collisionManager.GetSector( sectorIndex );
//					foreach( LevelSector.Direction dir in LevelSector.Directions ) {
//						
//						if( sector.GetLink( dir ) == characterSector.GetId() ) {
//							sector.SetLink( dir, -mockWorldObjectId );
//						}
//					}
//				}
//				
//			
//				collisionManager.AddCollisionCell( mockWorldObjectId, characterSector );
//			
//				
//				GameVector origin = new GameVector( 5.0f, -1.29089f, 2.458286f );
//				GameVector target = new GameVector( 0.0f, -1.29089f, 2.458286f );
//				
//				HitInfo hit;
//				List< int > path = collisionManager.GetShotTrajectory( origin, target, out hit );
//				
//				Assert.NotNull( path );
//				Assert.Equal( 2, path.Count );
//				
//				Assert.Equal( 7, path[ 0 ] );
//				Assert.Equal( 8, path[ 1 ] );
//				
//				Assert.Equal( mockWorldObjectId, hit.worldObjectId );
//				
//				GameVector hitOnCharacter = new GameVector( 1.0f, -1.29089f, 2.458286f );				
//				Assert.CheckEquals( hitOnCharacter, hit.position );
//			}
//		}
//	
//public class CollisionManagerTestLevel4TestCase : TestCase {
//
//		protected const string TEST_LEVEL = "Assets/Levels/teste_5" + Common.LEVEL_FILE_EXTENSION;
//		
//		protected CollisionManager collisionManager;
//		
//		public override void SetUp() {
//			collisionManager = new CollisionManager( TEST_LEVEL );
//		}
//		
//		[ UnitTest ]
//		public void TestSaveStateAndRestoreState() {
//			LevelSector s1;
//			LevelSector s2;
//			LevelSector s3;
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 0.0f, 0.0f, 2.0f, 2.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 1.0f, 1.0f, 1.0f, 2.0f, 2.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 1.0f, 1.0f, 1.0f, 3.0f, 3.0f, 3.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 3.0f, 3.0f, 3.0f, 5.0f, 5.0f, 5.0f ) );
//			
//			
//			collisionManager.GetSector( 0 ).SetFixedShape( true );
//			collisionManager.GetSector( 1 ).SetFixedShape( true );
//			collisionManager.GetSector( 2 ).SetFixedShape( true );
//			
//			s1 = new LevelSector( collisionManager.GetSector( 0 ) );
//			s2 = new LevelSector( collisionManager.GetSector( 1 ) );
//			s3 = new LevelSector( collisionManager.GetSector( 2 ) );
//			
//			
//			collisionManager.SaveState();
//			
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 1.0f, 0.0f, 2.0f, 4.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 1.0f, 14.0f, 1.0f, 2.0f, 20.0f, 2.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 11.0f, 1.0f, 1.0f, 113.0f, 3.0f, 3.0f ) );
//			
//			collisionManager.Wipe();
//			
//			collisionManager.RestoreState();
//			
//			Assert.CheckEquals( 3, collisionManager.GetTotalSectors() );
//			
//			Assert.CheckEquals( s1, collisionManager.GetSector( 0 ) );
//			Assert.CheckEquals( s2, collisionManager.GetSector( 1 ) );
//			Assert.CheckEquals( s3, collisionManager.GetSector( 2 ) );
//			
//			Assert.False( s1 == collisionManager.GetSector( 0 ) );
//			Assert.False( s2 == collisionManager.GetSector( 1 ) );
//			Assert.False( s3 == collisionManager.GetSector( 2 ) );
//		}
//		
//		[ UnitTest ]
//		public void TestRecalculateBoxesTouching() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 2.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 2.0f, 5.0f, 0.0f, 9.0f, 4.0f, 5.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 5, collisionManager.GetTotalSectors() );
//		}
//		
//		[ UnitTest ]
//		public void TestRecalculateBoxesOneInsideAnother() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 4.0f, 5.0f, 4.0f, 5.0f, 4.0f, 5.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 28, collisionManager.GetTotalSectors() );
//		}
//
//		
//		[ UnitTest ]
//		public void TestRecalculateBoxCrossingToAnother() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 4.0f, 5.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 37, collisionManager.GetTotalSectors() );
//		}
//
//		[ UnitTest ]
//		public void TestRecalculateBoxOn4() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 8.0f, 10.0f ) );
//			PortalizerWrapper.Recalculate( collisionManager );
//			
//			Assert.CheckEquals( 49, collisionManager.GetTotalSectors() );
//		}	
//			
//		[ UnitTest ]
//		public void TestEnumeratorForFixedSectorsOnly() {
//			collisionManager.Wipe();
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 255.0f, 0.0f, 255.0f, 0.0f, 255.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 8.0f, 10.0f ) );
//			
//			int counter = 0;
//			
//			foreach ( LevelSector sector in collisionManager ) {
//				Assert.CheckEquals( collisionManager.GetSector( counter ), sector );
//				++counter;
//			}
//		}
//		
//		[ UnitTest ]
//		public void TestEnumerator() {
//			collisionManager.Wipe();
//			LevelSector collidible;
//			
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 255.0f, 0.0f, 255.0f, 0.0f, 255.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			
//			collidible = LevelSector.MakeSectorAt( 4.0f, 5.0f, 4.0f, 5.0f, 4.0f, 5.0f );
//			collisionManager.AddCollisionCell( 0, collidible );
//			
//			int id = 0;
//			int counter = 0;
//			
//			foreach ( LevelSector sector in collisionManager ) {
//
//				if ( id < collisionManager.GetTotalSectors() ) {
//					Assert.CheckEquals( collisionManager.GetSector( id ), sector );
//				} else {
//					Assert.CheckEquals( collidible, sector );
//				}
//				
//				++id;
//				++counter;
//			}
//			
//			Assert.CheckEquals( 3, id );
//			Assert.CheckEquals( 3, counter );
//			Assert.CheckEquals( 2, collisionManager.GetTotalSectors() );
//		}
//		
//		[ UnitTest ]
//		public void TestIdPreservationOnBoxRecalculation() {
//			collisionManager.Wipe();
//			LevelSector collidible;
//			
//			
//			
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 0.0f, 9.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 0.0f, 9.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//			collisionManager.AddNavigableCell( LevelSector.MakeSectorAt( 9.0f, 18.0f, 0.0f, 9.0f, 9.0f, 18.0f ) );
//
//			collidible = LevelSector.MakeSectorAt( 8.0f, 10.0f, 4.0f, 5.0f, 8.0f, 10.0f );
//			
//			collisionManager.AddCollisionCell( 1, collidible );
//			PortalizerWrapper.Recalculate( collisionManager );
//			Assert.CheckEquals( 45, collisionManager.GetTotalSectors() );
//		}		
//	}	

	public class CollisionManagerRaycastTestCase : TestCase {
	
		protected CollisionManager cm;
		
		public override void SetUp() {
			cm = new CollisionManager();
			cm.AddBox( new Box( new GameTransform() ) );
		}
		
		[UnitTest]
		public void MissingSimpleBox() {
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( -1.0f, 1.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.NO_INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void HittingSimpleBox() {	
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 1.0f, 0.0f, 0.0f );
			
			HitInfo hit;
			cm.Raycast( a, b, out hit );
		}
		
		
		[UnitTest]
		public void AnotherSimpleHitTest() {	
			GameVector a = new GameVector( -0.2f, 1.0f, 0.0f );
			GameVector b = new GameVector( -0.2f, -1.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( -0.2f, 0.5f, 0.0f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void HittingOnTheEdge() {	
			GameVector a = new GameVector( 1.0f, 1.0f, 0.0f );
			GameVector b = new GameVector( -1.0f, -1.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( 0.5f, 0.5f, 0.0f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void HittingOnTheCorner() {	
			GameVector a = new GameVector( -1.0f, -1.0f, -1.0f );
			GameVector b = new GameVector( 1.0f, 1.0f, 1.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( -0.5f, -0.5f, -0.5f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void TargetIsInBoxFace() {
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( -0.5f, 0.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void TargetIsInBoxCorner() {
			GameVector a = new GameVector( -1.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( -0.5f, 0.5f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void OriginAndTargetAreInBoxFace() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.5f );
			GameVector b = new GameVector( 0.0f, 0.2f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como ambos est�o na face,
			// o algoritmo assume que o primeiro ponto entra no cubo
			// e o segundo sai
			Assert.CheckEquals( a, hit.position );
		}
		
		[UnitTest]
		public void OriginAndTargetAreInOppositeBoxFaces() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.5f );
			GameVector b = new GameVector( 0.0f, 0.0f, -0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como ambos est�o em faces do cubo face,
			// o algoritmo assume que o primeiro ponto entra no cubo
			// e o segundo sai
			Assert.CheckEquals( a, hit.position );
		}
		
		[UnitTest]
		public void OriginAndTargetAreInBoxCorners() {
			GameVector a = new GameVector( -0.5f, -0.5f, -0.5f );
			GameVector b = new GameVector( -0.5f, -0.5f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como ambos est�o em uma mesma face,
			// (a que compartilha os cantos), o algoritmo assume que o primeiro ponto 
			// entra no cubo e o segundo sai
			Assert.CheckEquals( a, hit.position );
		}
		
		[UnitTest]
		public void RaycastDoesNotLeaveTheBox() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 0.1f, 0.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.NO_INTERCEPTION, rayHit );
			Assert.CheckEquals( b, hit.position );
		}
		
		[UnitTest]
		public void RaycastFromInside() {
			GameVector a = new GameVector( 0.0f, 0.0f, 0.0f );
			GameVector b = new GameVector( 1.0f, 0.0f, 0.0f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			GameVector expected = new GameVector( 0.5f, 0.0f, 0.0f );
			Assert.CheckEquals( expected, hit.position );
		}
		
		[UnitTest]
		public void RaycastFromCornerToOutsideTheBox() {
			GameVector a = new GameVector( 0.5f, 0.5f, 0.5f );
			GameVector b = new GameVector( 0.1f, 0.5f, 0.5f );
			
			HitInfo hit;
			RaycastResult rayHit = cm.Raycast( a, b, out hit );
			
			Assert.EnumEqual( RaycastResult.INTERCEPTION, rayHit );
			
			// resultado deve ser "a" pois, como a est� na face e b est� fora,
			// o algoritmo assume que o primeiro ponto sai do cubo e � a primeira
			// interception
			Assert.CheckEquals( a, hit.position );
		}
	}
	
	public class CollisionManagerImportingTestCase : TestCase {
		
		[UnitTest]
		public void Importing() {
			CollisionManager cm = new CollisionManager( "Test1" );
			
			Assert.Equal( 3, cm.GetBoxes().Count );
		}
	}
	
	public class CollisionManagerConeInterceptionTestCase : TestCase {
		
		protected WorldObject character = new WorldObject( 1, "" );
		protected Box characterBox;
		protected CollisionManager cm;
			
		public override void SetUp() {
			cm = new CollisionManager();
			cm.AddBox( character );
			characterBox = character.GetBox();
		}
		
		[UnitTest]
		public void CharacterOutsideCone() {
			characterBox.GetPosition().Set( new GameVector( 300.0f, -200.0f, 100.0f ) );
			List< Box > boxes = cm.CreateFilterCone( GameVector.Zero, characterBox.GetDirection() * 10.0f, 30.0f );
			Assert.Null( boxes );
		}
		
		[UnitTest]
		public void PointOnTheConeApex() {
			List< Box > boxes = cm.CreateFilterCone( characterBox.GetPosition(), characterBox.GetDirection() * 10.0f, 30.0f );
			Assert.Equal( 1, boxes.Count );
			Assert.CheckEquals( boxes[ 0 ], character.GetBox() );
		}
		
		[UnitTest]
		public void OneBoxInsideAndAnotherOutsideCone() {
			WorldObject character2 = new WorldObject( 2, "" );
			cm.AddBox( character2.GetBox() );
			character2.GetBox().GetGameTransform().position.Set( 0.0f, 0.0f, 11.0f );
			characterBox.GetPosition().Set( new GameVector( 0.0f, 0.0f, 5.0f ) );

			List< Box > boxes = cm.CreateFilterCone( GameVector.Zero, characterBox.GetDirection() * 10.0f, 30.0f );
			Assert.Equal( 1, boxes.Count );
			Assert.CheckEquals( boxes[ 0 ], character.GetBox() );
		}
		
		[UnitTest]
		public void BoxOnTheConeSurface() {
			float tan10 = 0.176326981f;
			characterBox.GetPosition().Set( new GameVector( 6.0f * tan10, 6.0f, 0.0f ) );

			List< Box > boxes = cm.CreateFilterCone( GameVector.Zero, GameVector.YAxis * 10.0f, 10.0f );
			Assert.Equal( 1, boxes.Count );
			Assert.CheckEquals( boxes[ 0 ], character.GetBox() );
		}
	}
}