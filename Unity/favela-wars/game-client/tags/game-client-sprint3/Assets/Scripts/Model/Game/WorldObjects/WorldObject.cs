using System;
using GameCommunication;


namespace GameModel {
	
	/// <summary>
	/// Esta classe descreve as informações gerais de um objeto do mundo, como personagens e itens visíveis no mapa. 
	/// </summary>
	public class WorldObject {
		
		/// <summary>
		/// ID deste objeto no mundo.
		/// </summary>
		protected int worldID;
		
		/// <summary>
		/// Identificação do bundle de assets (modelo 3D, animação, texturas, etc.) deste objeto.
		/// </summary>
		protected string assetBundleID;
		
		/// <summary>
		/// Informações de posição deste objeto no mundo.
		/// </summary>
		protected GameTransform transform = new GameTransform();
		
		protected Box box;
		
		
		public WorldObject( int worldID, string assetBundleID ) {
			this.worldID = worldID;
			this.assetBundleID = assetBundleID;
			this.box = new Box( this );
		}
		
		
		public int GetWorldID() {
			return worldID;
		}
		
		
		public string GetAssetBundleID() {
			return assetBundleID;
		}
		
		
		public GameTransform GetGameTransform() {
			return transform;
		}
		
		
		public Box GetBox() {
			return box;
		}
		
	}
}

