using System;
using GameCommunication;
using System.Collections.Generic;

namespace GameModel {
	
	public class LoadSnapshotHandler : Handler {
		
		protected LoadSnapshotRequest loadSnapshotRequest;
		
		
		public LoadSnapshotHandler( LoadSnapshotRequest loadSnapshotRequest ) {
			this.loadSnapshotRequest = loadSnapshotRequest;
		}
		
		
		public override Response GetResponse() {
			Response r = new Response();
			Mission m = Mission.GetInstance( loadSnapshotRequest.GetPlayerID() );
			
			List< WorldObject > worldObjects = m.GetWorldObjects();
			
			foreach ( WorldObject w in worldObjects ) {
				GameTransform t = w.GetGameTransform();
				r.AddGameActionData( new SetWorldObjectTransformData( w.GetWorldID(), t.position, t.direction, t.scale ) );
				//r.AddGameActionData( new WaitActionData( 1000 ) ); // TODO teste
			}
			
			return r;
		}
		
	}
}

