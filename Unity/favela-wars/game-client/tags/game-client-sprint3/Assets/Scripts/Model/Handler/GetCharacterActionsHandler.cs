using System;
using System.Collections.Generic;

using GameCommunication;
using Action = GameCommunication.Action;
using Utils;
using GameController;

namespace GameModel {
	
	public class GetCharacterActionsHandler : Handler {
		
		protected GetCharacterActionsRequest request;
		
		
		public GetCharacterActionsHandler( GetCharacterActionsRequest getCharacterActionsRequest ) {
			this.request = getCharacterActionsRequest;
		}


		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			
			Character characterWorldObj = ( Character ) mission.GetWorldObjectByID( request.characterWorldID );
			Action root = new Action( Common.ActionTypes.ROOT );
			Action hideAction = new Action( Common.ActionTypes.HIDE_MENU );
			root.AddChild( hideAction );
			Action getInfoAction = new Action( Common.ActionTypes.GET_INFO );
			getInfoAction.SetInteractionControllerClass( typeof( GetCharacterInfoInteractionController ) );
			root.AddChild( getInfoAction );
			
			
			if ( characterWorldObj == null ) {
				// no character corresponding to ID
				return null;
			} else {
				// soldado é do jogador
				if ( mission.GetWorldObjectPlayerID( characterWorldObj ) == request.GetPlayerID() ) {
					//monta lista de possíveis ações:
					root.AddChild( characterWorldObj.GetPossibleActionsList() );
				}
			}
			
			
			Response r = new Response();
			r.AddGameActionData( new GetCharacterActionsData( root ) );
			return r;
		}
		
	}
}

