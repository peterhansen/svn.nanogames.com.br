using System;
using System.Collections.Generic;
using GameCommunication;
using System.Text;


namespace GameModel {
	
	public class AStarNode {
	
		protected float g = 0.0f;
		protected float h = 0.0f;
		
		protected bool isAccessible = true;
		
		protected AStarNode parent = null;
		
		protected HashSet< AStarNode > children = new HashSet< AStarNode >();
		
		protected GameVector position;
		
		private int objId;

		public AStarNode( GameVector position ) {
			this.parent = null;
			this.position = position;
			this.objId = -1;
		}
		
		
		public AStarNode( GameVector position, int objId ) {
			this.parent = null;
			this.position = position;
			this.objId = objId;
		}
		
		public int getObjID() {
			return objId;
		}
		
		public GameVector GetPosition() {
			return position;
		}
		
		public void SetPosition( GameVector position ) {
			this.position.Set( position );
		}
		
		public void SetGAndH( float g, float h ) {
			this.g = g;
			this.h = h;
		}
		
		public void SetParent( AStarNode parent ) {
			this.parent = parent;
		}
		
		public AStarNode GetParent() {
			return parent;
		}
		
		public float GetF() {
			return g + h;
		}
		
		public float GetG() {
			return g;
		}
		
		public float GetH() {
			return h;
		}
		
		public HashSet< AStarNode > GetChildren() {
			return children;
		}
		
		public void AddChild( AStarNode child ) {
			children.Add( child );
		}
		
		public void RemoveChild( AStarNode child  ) {
			if( children.Contains( child ) ) {
				children.Remove( child );
			}
		}
		
		public bool HasChildren( AStarNode node ) {
			return children.Contains( node );
		}
		
		public bool IsAccessible() {
			return isAccessible;
		}
		
		public void SetAccessibility( bool isAccessible ) {
			this.isAccessible = isAccessible;
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.Append( "[AStarNode pos=" );
			sb.Append( position );
			sb.Append( " g=" );
			sb.Append( g );
			sb.Append( " h=" );
			sb.Append( h );
			sb.Append( " ]" );
			
			return sb.ToString();
		}
	}
}

