using System;
using System.Collections.Generic;

using GameCommunication;
using Utils;

namespace GameModel {
	
	public class MoveCharacterHandler : Handler {
		
		protected MoveCharacterRequest moveCharacterRequest;
		
		
		public MoveCharacterHandler( MoveCharacterRequest moveCharacterRequest ) {
			this.moveCharacterRequest = moveCharacterRequest;
		}
		

		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( moveCharacterRequest.GetPlayerID() );
			
			WorldObject characterWorldObj = mission.GetWorldObjectByID( moveCharacterRequest.worldObjectID );
			
			if ( characterWorldObj == null || 
			     mission.GetWorldObjectPlayerID( characterWorldObj ) != moveCharacterRequest.GetPlayerID() ) 
			{
				// personagem não encontrado, ou não pode ser movido pelo jogador
				Debugger.LogWarning( "Jogador tentando mover personagem fora de sua tropa. PlayerID: " + moveCharacterRequest.GetPlayerID() + ", characterID: " + moveCharacterRequest.worldObjectID );
				return null;
			}
			
			GameVector correctedDestination;
			if( !CorrectDestination( mission.GetMap().GetCollisionManager(), characterWorldObj.GetBox(), out correctedDestination ) ) {
				return null;
			}
			
			List< GameVector> nodes = mission.GetMap().GetAStarGraph().FindBestPath( characterWorldObj, correctedDestination );
			if( nodes != null ) {
				Response r = new Response();
				
				// atualiza posicao e direcao do personagem para posicao final do caminho
				ConsolidateFinalPosition( characterWorldObj, nodes, mission );
				
				// TODO: a velocidade é passada pelo servidor?
				// Não deveria ser somente a stance do personagem, no cliente, que determina isso?
				MoveCharacterData data = new MoveCharacterData( moveCharacterRequest.worldObjectID, nodes );
				if ( characterWorldObj is Soldier )
					data.speed = ( characterWorldObj as Soldier ).GetStance() == Stance.STANDING ? 42.0f : 4.0f;
				else
					data.speed = 10.0f;
						
				r.AddGameActionData( data );
				
				VisibilityChangeData visibilityData = VisibilityManager.GetVisibilityFor( ( characterWorldObj as Character ).GetFaction(), mission );
				
				r.AddGameActionData( visibilityData );
				return r;
			} else {
				return null;
			}
		}
		
		protected bool CorrectDestination( CollisionManager collManager, Box characterBox, out GameVector correctedDestination ) {
			return collManager.CorrectDestination( characterBox, moveCharacterRequest.destination, out correctedDestination );
		}
		
		protected void ConsolidateFinalPosition( WorldObject characterWorldObj, List< GameVector > nodes, Mission mission) {
			GameTransform characterTransform = characterWorldObj.GetGameTransform();
			characterTransform.position.Set( nodes[ nodes.Count - 1 ] );
			characterTransform.direction.Set( ( nodes[ nodes.Count - 1 ] - nodes[ nodes.Count - 2 ] ).Unitary() );
			mission.GetMap().OnWorldObjectMoved( characterWorldObj, mission.GetVisibilityManager() );
		}
	}
}

