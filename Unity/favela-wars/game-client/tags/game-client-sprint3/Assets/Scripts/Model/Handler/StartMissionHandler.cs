using System;
using GameCommunication;
using System.Collections.Generic;

using Utils;


namespace GameModel {
	
	public class StartMissionHandler : Handler {
		
		protected StartMissionRequest request;
		
		public StartMissionHandler( StartMissionRequest startMissionRequest ) {
			this.request = startMissionRequest;
		}
		
		public override Response GetResponse() {
			Response r = new Response();
			
			Debugger.Log( "Creating Mission: " + request.missionName );
			
			List< int > ids = new List<int>();
			///TODO: por enquanto, apenas com esses dois jogadores marretados.
			ids.Add( request.GetPlayerID() );
			ids.Add( request.GetPlayerID() + 1 );
			
			Mission m = Mission.CreateInstance( ids, request.missionName, request.soldierIDs );
			
			List< WorldObject > worldObjects = m.GetWorldObjects();
			
			r.AddGameActionData( new StartMissionData( m.GetSceneName(), m.GetTurnManager().GetTurnData() ) );
			foreach ( WorldObject w in worldObjects ) {
				r.AddGameActionData( new CreateGameObjectData( w.GetWorldID(), w.GetAssetBundleID() ) );
			}
			
			VisibilityChangeData vis = VisibilityManager.GetVisibilityFor( m.GetTurnManager().GetTurnData().faction, m );
			
			r.AddGameActionData( vis );
			
			return r;
		}	
	}
}

