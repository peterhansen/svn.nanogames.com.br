using System;
using System.Collections.Generic;

using SharpUnit;

using Utils;
using GameCommunication;


namespace GameModel {
	
	public class TurnManager {
		
		protected enum PlayerTurnState {
			NOT_HIS_TURN,
			CAN_PARTICIPATE,
			ENDED_PARTICIPATION
		}
		
		protected Mission mission;
		
		// turn information
		protected int factionIndex;
		protected int turnNumber;
		
		protected Dictionary< Player, Faction > playerFactions = new Dictionary< Player, Faction >( new Player.Comparer() );
		protected Dictionary< Player, PlayerTurnState > playerState = new Dictionary< Player, PlayerTurnState >( new Player.Comparer() );
		protected Dictionary< Faction, Utils.HashSet< Player > > factionPlayers = new Dictionary< Faction, Utils.HashSet< Player > >();

		public TurnManager( Mission mission ) {
			this.mission = mission;
		}
		
		public TurnData GetTurnData() {
			return new TurnData( turnNumber, Common.PlayerFactions[ factionIndex ] );
		}
		
		public void Start() {
			// TODO: vamos sempre comecar pela mesma faccao?
			turnNumber = 0;
			factionIndex = Common.PlayerFactions.Length - 1;
			ChangeTurn();
		}
		
		public void ChangeTurn() {
			factionIndex++;
			
			if( factionIndex >= Common.PlayerFactions.Length ) {
				factionIndex = 0;
				turnNumber++;
			}
			
			foreach( KeyValuePair< Player, Faction > factionPerPlayer in playerFactions ) {
				playerState[ factionPerPlayer.Key ] = ( factionPerPlayer.Value == GetCurrentFaction() )? PlayerTurnState.CAN_PARTICIPATE: PlayerTurnState.NOT_HIS_TURN;
			}
			
			mission.GetVisibilityManager().Reset();
			mission.BuildVisibilityMap( null, Common.PlayerFactions[ factionIndex ] );
			mission.CheckEndMissionConditions();
		}
		
		public void AddPlayer( Player player, Faction faction ) {
			if( !factionPlayers.ContainsKey( faction ) ) {
				factionPlayers[ faction ] = new Utils.HashSet< Player >();
			}
			factionPlayers[ faction ].Add( player );
			playerFactions[ player ] = faction;
		}
		
		public void RemovePlayer( Player player ) {
			if( playerFactions.ContainsKey( player ) ) {
				playerFactions.Remove( player );
			}
		}
		
		public void EndParticipation( Player player ) {
			if( playerState[ player ] == PlayerTurnState.CAN_PARTICIPATE ) {
				playerState[ player ] = PlayerTurnState.ENDED_PARTICIPATION;
			}
		}
		
		public bool HasTurnChanged() {
			foreach( Player player in factionPlayers[ GetCurrentFaction() ] ) {
				if( playerState[ player ] != PlayerTurnState.ENDED_PARTICIPATION ) {
					return false;
				}
			}
			
			ChangeTurn();
			return true;
		}
		
		protected Faction GetCurrentFaction() {
			return Common.PlayerFactions[ factionIndex ];	
		}
	}
}

