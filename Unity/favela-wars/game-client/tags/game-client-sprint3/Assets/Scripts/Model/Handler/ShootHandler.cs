using System;
using System.Collections.Generic;

using Utils;
using GameCommunication;



namespace GameModel {
	
	public class ShootHandler : Handler {
		
		protected ShootRequest shootRequest;
		
		
		public ShootHandler( ShootRequest shootRequest ) {
			this.shootRequest = shootRequest;
		}


		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( shootRequest.GetPlayerID() );
			
			WorldObject characterWorldObj = mission.GetWorldObjectByID( shootRequest.shooterWorldID );
			
			if( characterWorldObj == null || mission.GetWorldObjectPlayerID( characterWorldObj ) != shootRequest.GetPlayerID() ) {
				Debugger.LogWarning( "Jogador tentando atirar com personagem fora de sua tropa. PlayerID: " + shootRequest.GetPlayerID() + ", characterID: " + shootRequest.shooterWorldID );
				return null;
			}
			
			Response r = new Response();
			HitInfo hit;
			mission.GetMap().GetCollisionManager().Shoot( characterWorldObj, shootRequest.destination, out hit );
			
			if ( hit.worldObjectId != Common.NULL_WORLDOBJECT ) {
				
				Character target = mission.GetWorldObjectByID( hit.worldObjectId ) as Character;
				if ( target != null ) {
					int damage = ( int ) NanoRandom.NextFloat( 0.1f, ( characterWorldObj as Soldier).GetAttackLevel() );
					target.SetHitPoints( target.GetHitPoints() -  damage );
					
					Debugger.Log( "dano - sobrou: " + target.GetHitPoints() );
					if ( target.GetHitPoints() <= 0 ) {
						mission.DisableCharacter( target );
					} 
					r.AddGameActionData( new CharacterHitData( target.GetWorldID(), damage, target.GetHitPoints() > 0 ) );
				}
			}
				
				
			//Debugger.Log( "HIT: " + hit.position + " -> " + hit.worldObjectId );
			//if( hit.worldObjectId != Common.NULL_WORLDOBJECT ) {
			r.AddGameActionData( new ShootData( shootRequest.shooterWorldID, shootRequest.weaponID, characterWorldObj.GetGameTransform().position, hit.position ) );
			return r;
		}
	}
}

