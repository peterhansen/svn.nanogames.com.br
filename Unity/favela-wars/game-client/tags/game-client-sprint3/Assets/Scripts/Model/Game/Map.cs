using System;
using System.IO;

using GameCommunication;
using Utils;

namespace GameModel {
	
	public class Map {
		
		protected HashSet< SpawnPoint > spawnPoints = new HashSet< SpawnPoint >();
		
		protected CollisionManager collisionManager;
		
		protected AStarGraph aStarGraph;
		protected AStarGraphGenerator aStarGenerator;
		
		
		public Map( string levelName ) {
			collisionManager = new CollisionManager( levelName );
			aStarGenerator = new AStarGraphGenerator( collisionManager );
			aStarGraph = aStarGenerator.GenerateGraph();
		}
		
		
		public AStarGraph GetAStarGraph() { 
			return aStarGraph;
		}
		
		
		public CollisionManager GetCollisionManager() {
			return collisionManager;
		}
		
		
		public void UpdateNavigationGraph( WorldObject obj, VisibilityManager hint ) {
			aStarGraph = aStarGenerator.UpdateGraph( aStarGraph, obj, hint );
		}
		
		
		public void OnWorldObjectMoved( WorldObject obj, VisibilityManager hint ) {
			UpdateNavigationGraph( obj, hint );
		}
	}

}

