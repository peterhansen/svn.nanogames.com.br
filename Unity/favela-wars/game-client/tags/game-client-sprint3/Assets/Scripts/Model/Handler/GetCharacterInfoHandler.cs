using System;
using System.Collections.Generic;

using GameCommunication;
using Utils;

namespace GameModel {
	
	public class GetCharacterInfoHandler : Handler {
		
		protected GetCharacterInfoRequest request;
		
		
		public GetCharacterInfoHandler( GetCharacterInfoRequest getCharacterInfoRequest ) {
			this.request = getCharacterInfoRequest;
		}


		public override Response GetResponse() {
			Mission mission = Mission.GetInstance( request.GetPlayerID() );
			if ( mission == null ) {
				Debugger.LogError( "Couldn't find or create mission for player " + request.GetPlayerID() );
				return null;
			}
				
			
			Character characterWorldObj = ( Character ) mission.GetWorldObjectByID( request.characterWorldID );
			
			if ( characterWorldObj == null ) {
				// no character corresponding to ID
				return null;
			}
			
			Response r = new Response();
			r.AddGameActionData( new GetCharacterInfoData( characterWorldObj, mission.GetWorldObjectPlayerID( characterWorldObj ) == request.GetPlayerID() ) );
			return r;
		}
		
	}
}

