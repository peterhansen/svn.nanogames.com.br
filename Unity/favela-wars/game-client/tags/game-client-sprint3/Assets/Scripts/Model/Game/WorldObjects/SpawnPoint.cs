using System;
using GameCommunication;
using System.Collections;
using System.Collections.Generic;

using Utils;


namespace GameModel {
	
	public class SpawnPoint : WorldObject {
		
		protected int minObjects;
		
		protected int capacity;
		
		protected List< WorldObject > objects = new List< WorldObject >();
		
		protected Faction faction;
		
		
		public SpawnPoint( int worldID ) : base( worldID, "ffffffff" ) {
			transform.scale.Set( 1, 1, 1 );
			transform.position.Set( 0, transform.scale.y * 0.5f, 0 );
		}
		
		
		public SpawnPoint( int worldID, SpawnAreaMissionData data ) : base( worldID, "ffffffff" ) {
			transform.scale.Set( data.areaBox.scale );
			transform.position.Set( data.areaBox.position );
			transform.direction.Set( data.areaBox.direction );
			capacity = data.capacity;
			minObjects = data.minObjects;
			faction = data.faction;
		}
		
		
		public bool AddWorldObject( WorldObject obj ) {
			if ( objects.Count < capacity ) {
				float halfWidth = transform.scale.x * 0.5f;
				float halfDepth = transform.scale.z * 0.5f;
				
				// evita loop infinito caso a área esteja muito apertada para tantos objetos
				int tries = 100;
				do {
					obj.GetGameTransform().position.Set( NanoRandom.NextFloat( transform.position.x - halfWidth, transform.position.x + halfWidth ),
					                                     transform.position.y,
					                                     NanoRandom.NextFloat( transform.position.z - halfDepth, transform.position.z + halfDepth ) );
					--tries;
				} while ( tries > 0 && CheckObjectsCollision( obj ) );
				
				objects.Add( obj );
				
				return true;
			}
			
			return false;
		}
		
		
		protected bool CheckObjectsCollision( WorldObject obj ) {
			float objRay = GetHalfDiagonal( obj );
			
			foreach ( WorldObject o in objects ) {
				float contactDistance = objRay + GetHalfDiagonal( o );
				float distance = o.GetGameTransform().position.Sub( obj.GetGameTransform().position ).Norm();
				
				if ( distance <= contactDistance )
					return true;
			}
			
			return false;
		}
		
		
		protected static float GetHalfDiagonal( WorldObject obj ) {
			return ( float ) ( Math.Sqrt( ( obj.GetGameTransform().scale.x * obj.GetGameTransform().scale.x ) +
			             	  		( obj.GetGameTransform().scale.z * obj.GetGameTransform().scale.z ) ) * 0.5f );
		}
		
		
		public Faction GetFaction() {
			return faction;
		}
		
		
		public int GetRandomQuantity() {
			return ( int ) NanoRandom.NextFloat( minObjects, capacity );
		}
		
		
	}
}

