using System;

using GameCommunication;

namespace GameModel {
	
	public class RayBoxInterception {
		
		public float t;
		public GameVector point;
		public int worldObjectID;
		
		public RayBoxInterception() {
		}
		
		public RayBoxInterception( float t, GameVector point, int worldObjectID ) {
			this.t = t;
			this.point = point;
			this.worldObjectID = worldObjectID;
		}
	}

	public class RayBoxInterceptionReply {
		public RayBoxInterception entry;
		public RayBoxInterception exit;
	}
}

