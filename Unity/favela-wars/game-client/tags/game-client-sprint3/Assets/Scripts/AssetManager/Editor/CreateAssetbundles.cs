using System.IO;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

using Utils;

public class CreateAssetbundles {
	
    // This method creates an assetbundle of each SkinnedMeshRenderer found in any selected character
	// fbx, and adds any materials that are intended to be used by the specific SkinnedMeshRenderer.
    [ MenuItem( "Asset Manager/Create Assetbundles" ) ]
    static void Execute() {
        bool createdBundle = false;
        // TODO teste foreach ( Object o in Selection.GetFiltered( typeof ( Object ), SelectionMode.DeepAssets ) ) {
		foreach ( Object o in EditorUtility.CollectDependencies( Selection.objects ) ) {
            if ( !( o is GameObject ) )
				continue;
			
            if ( o.name.Contains( "@" ) )
				continue;
			
            GameObject assetFBX = ( GameObject ) o;
            string name = assetFBX.name.ToLower();
           
            Debugger.Log( "******* Creating assetbundles for: " + name + " *******" );

            // Create a directory to store the generated assetbundles.
            if ( !Directory.Exists( AssetbundlePath ) )
                Directory.CreateDirectory( AssetbundlePath );


            // Delete existing assetbundles for current character.
            string[] existingAssetbundles = Directory.GetFiles( AssetbundlePath );
            foreach ( string bundle in existingAssetbundles ) {
                if ( bundle.EndsWith( ".assetbundle" ) && bundle.Contains( "/assetbundles/" + name ) )
                    File.Delete(bundle);
            }

            // Save bones and animations to a seperate assetbundle. Any possible combination of 
			// CharacterElements will use these assets as a base. As we can not edit assets we
			// instantiate the fbx and remove what we dont need. As only assets can be added to
			// assetbundles we save the result as a prefab and delete it as soon as the
			// assetbundle is created.
            GameObject assetClone = ( GameObject ) Object.Instantiate( assetFBX );

			// postprocess animations: we need them animating even offscreen
			foreach ( Animation anim in assetClone.GetComponentsInChildren< Animation >() )
                anim.cullingType = AnimationCullingType.AlwaysAnimate;

            foreach ( SkinnedMeshRenderer smr in assetClone.GetComponentsInChildren< SkinnedMeshRenderer >())
                Object.DestroyImmediate( smr.gameObject );
			
            assetClone.AddComponent< SkinnedMeshRenderer >();
            Object characterBasePrefab = GetPrefab( assetClone, "base" ); // TODO usar somente "base" ou "assetbase"
            string path = AssetbundlePath + name + "_base.assetbundle";
            BuildPipeline.BuildAssetBundle( characterBasePrefab, null, path, BuildAssetBundleOptions.CollectDependencies );
            AssetDatabase.DeleteAsset( AssetDatabase.GetAssetPath( characterBasePrefab ) );

            // Collect materials.
            List< Material > materials;
			string materialsPath = ""; // TODO GenerateMaterials.MaterialsPath( assetFBX );
			if ( Directory.Exists( materialsPath ) )
				materials = EditorHelpers.CollectAll< Material >( materialsPath );
			else
				materials = new List< Material >();

            // Create assetbundles for each SkinnedMeshRenderer.
            foreach ( Renderer renderer in assetFBX.GetComponentsInChildren< Renderer >( true ) ) {
				// TODO necessário tratar outros tipos de Renderer exceto MeshRenderer e SkinnedMeshRenderer?
                List< Object > toinclude = new List< Object >();

                // Save the current SkinnedMeshRenderer as a prefab so it can be included in the assetbundle.
				// As instantiating part of an fbx results in the entire fbx being instantiated, we have to
				// dispose of the entire instance after we detach the SkinnedMeshRenderer in question.
                GameObject rendererClone = ( GameObject ) EditorUtility.InstantiatePrefab( renderer.gameObject );
				if ( rendererClone.transform.parent != null ) {
	                GameObject rendererParent = rendererClone.transform.parent.gameObject;
	                rendererClone.transform.parent = null;
	                Object.DestroyImmediate( rendererParent );
				}
                Object rendererPrefab = GetPrefab( rendererClone, "rendererobject" );
                toinclude.Add( rendererPrefab );

                // Collect applicable materials.
                foreach ( Material m in materials ) {
                    if ( m.name.Contains( renderer.name.ToLower() ) )
						toinclude.Add(m);
				}

                // When assembling a character, we load SkinnedMeshRenderers from assetbundles,
                // and as such they have lost the references to their bones. To be able to
                // remap the SkinnedMeshRenderers to use the bones from the characterbase assetbundles,
                // we save the names of the bones used.
                List< string > boneNames = new List< string >();
				if ( renderer is SkinnedMeshRenderer ) {
					// somente SkinnedMeshRenderer tem bones
					SkinnedMeshRenderer smr = ( SkinnedMeshRenderer ) renderer;
	                foreach ( Transform t in smr.bones )
	                    boneNames.Add( t.name );
				}
                string stringholderpath = "Assets/bonenames.asset";
				
				StringHolder holder = ScriptableObject.CreateInstance< StringHolder > ();
				holder.content = boneNames.ToArray();
                AssetDatabase.CreateAsset( holder, stringholderpath );
                toinclude.Add( AssetDatabase.LoadAssetAtPath( stringholderpath, typeof ( StringHolder ) ) );

                // Save the assetbundle.
                string bundleName = name + "_" + renderer.name.ToLower();
                path = AssetbundlePath + bundleName + ".assetbundle";
                BuildPipeline.BuildAssetBundle( null, toinclude.ToArray(), path, BuildAssetBundleOptions.CollectDependencies );
                Debugger.Log( "Saved " + bundleName + " with " + ( toinclude.Count - 2 ) + " materials" );

                // Delete temp assets.
                AssetDatabase.DeleteAsset( AssetDatabase.GetAssetPath( rendererPrefab ) );
                AssetDatabase.DeleteAsset( stringholderpath );
                createdBundle = true;
            }
        }

        if ( createdBundle )
            UpdateAssetElementDatabase.Execute();
        else
            EditorUtility.DisplayDialog( "Asset Manager", "Nenhum asset bundle criado. Selecione a pasta do asset no Project pane para processar todos os assets. Selecione subpastas para processar assets específicos.", "Ok" );
    }
	

    private static Object GetPrefab( GameObject go, string name ) {
        Object tempPrefab = EditorUtility.CreateEmptyPrefab( "Assets/" + name + ".prefab" );
        tempPrefab = EditorUtility.ReplacePrefab( go, tempPrefab );
        Object.DestroyImmediate( go );
		
        return tempPrefab;
    }
	

    public static string AssetbundlePath {
        get {
			return "assetbundles" + Path.DirectorySeparatorChar; 
		}
    }
}