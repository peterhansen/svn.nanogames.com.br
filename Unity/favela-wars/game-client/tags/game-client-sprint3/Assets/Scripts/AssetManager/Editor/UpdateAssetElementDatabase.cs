﻿using System.IO;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

using Utils;

public class UpdateAssetElementDatabase {
    // This method collects information about all available
    // CharacterElements stores it in the CharacterElementDatabase
    // assetbundle. Which CharacterElements are available is 
    // determined by checking the generated materials.
    [ MenuItem( "Asset Manager/Update Asset Element Database" ) ]
    public static void Execute() {
        List< AssetElement > assetElements = new List< AssetElement >();

        // As a AssetElement needs the name of the assetbundle that contains its assets, we go through
		// all assetbundles to match them to the materials we find.
        string[] assetbundles = Directory.GetFiles( CreateAssetbundles.AssetbundlePath );
        string[] materials = Directory.GetFiles( "Assets/Models", "*.mat", SearchOption.AllDirectories );
        foreach ( string material in materials ) {
            foreach ( string bundle in assetbundles ) {
                FileInfo bundleFI = new FileInfo(bundle);
                FileInfo materialFI = new FileInfo(material);
                string bundleName = bundleFI.Name.Replace(".assetbundle", "");
                
				if ( !materialFI.Name.StartsWith( bundleName ) ) 
					continue;
                
				if ( !material.Contains( "TextureMaterials" ) ) 
					continue;
                
				assetElements.Add( new AssetElement( materialFI.Name.Replace( ".mat", "" ), bundleFI.Name ) );
                break;
            }
        }

        // After collecting all CharacterElements we store them in an
        // assetbundle using a ScriptableObject.

        // Create a ScriptableObject that contains the list of CharacterElements.
		AssetElementHolder t = ScriptableObject.CreateInstance<AssetElementHolder> ();
		t.content = assetElements;

        // Save the ScriptableObject and load the resulting asset so it can 
        // be added to an assetbundle.
        string p = "Assets/AssetElementDatabase.asset";
        AssetDatabase.CreateAsset(t, p);
		Object o = AssetDatabase.LoadAssetAtPath(p, typeof(AssetElementHolder));

        // Build the CharacterElementDatabase assetbundle.
		BuildPipeline.BuildAssetBundle(o, null, CreateAssetbundles.AssetbundlePath + "AssetElementDatabase.assetbundle");

        // Delete the ScriptableObject.
        AssetDatabase.DeleteAsset(p);

        Debugger.Log("******* Updated Asset Element Database, added " + assetElements.Count + " elements *******");
    }
}