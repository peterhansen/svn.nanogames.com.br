﻿using UnityEditor;
using UnityEngine;

public class FBXPostprocessor : AssetPostprocessor {
    // This method is called just before importing an FBX.
    void OnPreprocessModel() {
        ModelImporter mi = ( ModelImporter ) assetImporter;
        mi.globalScale = 1;
        mi.animationCompression = ModelImporterAnimationCompression.Off;

        // Materials for characters are created using the GenerateMaterials script.
        mi.generateMaterials = 0;
    }
	

    // This method is called immediately after importing an FBX.
    void OnPostprocessModel( GameObject go ) {
        // Assume an animation FBX has an @ in its name, to determine if an fbx is a character or an animation.
        if ( assetPath.Contains( "@" ) ) {
            // For animation FBX's all unnecessary Objects are removed.
            // This is not required but improves clarity when browsing assets.

            // Remove SkinnedMeshRenderers and their meshes.
			// TODO necessário fazer o loop também para outros tipos de renderer?
            foreach ( SkinnedMeshRenderer smr in go.GetComponentsInChildren< SkinnedMeshRenderer >() ) {
                Object.DestroyImmediate( smr.sharedMesh, true );
                Object.DestroyImmediate( smr.gameObject );
            }

            // Remove the bones.
//            foreach (Transform o in go.transform) TODO necessário? (gera erro de compilação na Unity)
//                Object.DestroyImmediate(o);
        }
    }
}