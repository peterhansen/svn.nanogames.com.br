using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEngine;

using Utils;
using GameController;

using Object = UnityEngine.Object;


// alias para facilitar a geração de variações de combinações de materiais
using Combination = System.Collections.Generic.List< CompositeTexture >;


public class GenerateResources {
	
	public static string WINDOW_TITLE = "Gerador de Assets";
	
	public static string PATH_PREFABS = "Assets/Prefabs/";
	
	public static string PATH_ASSETS = "Assets/Assets/";
	
	private const string MENU_ITEM = "Asset Manager/Generate Assets";
	
	private static readonly Regex validExtensions = new Regex( @"(\A.*\.(png|jpg|jpeg|bmp|tif)+\z)" );
	
	public static bool overwriteFiles;
	
	
    [ MenuItem( MENU_ITEM ) ]
    public static void Execute() {
		overwriteFiles = false;
		
        int createdMaterials = 0;
		int createdCombinations = 0;
		
        foreach ( GameObject assetFbx in Selection.GetFiltered( typeof( GameObject ), SelectionMode.DeepAssets ) ) {
            if ( assetFbx.name.Contains( "@" ) )
				continue;
			
			string assetPath = AssetRoot( assetFbx );
			
			// evita que alguém execute o script num objeto que esteja fora da pasta correta
			if ( !assetPath.StartsWith( PATH_ASSETS ) )
				continue;
			
			string assetRelativePath = assetPath.Replace( PATH_ASSETS, "" );
			List< Renderer > defaultMaterialChildren = new List< Renderer >();
			
			Dictionary< string, Combination > variations = new Dictionary< string, List< CompositeTexture > >();

            // obtém todas as texturas do diretório do objeto
			List< Texture2D > textures = LoadTextures( assetPath );
			variations[ "_root" ] = SaveMaterials( textures, OutputPath( assetRelativePath, assetFbx.name ), "_root" );
			
			// verifica se há texturas comuns, e gera materiais para elas
			textures = LoadTextures( Directory.GetParent( assetPath ) + "_comum" );
			variations[ "_root" ].AddRange( SaveMaterials( textures, OutputPath( assetRelativePath, assetFbx.name ), "_root" ) );
			
            // cria materiais para os filhos
            foreach ( Renderer smr in assetFbx.GetComponentsInChildren< Renderer >( true ) ) {
				// verifica se o filho tem texturas específicas
				string childPath = Path.Combine( assetPath, smr.name );
				if ( Directory.Exists( childPath ) ) {
					textures = LoadTextures( childPath );
					
					if ( !variations.ContainsKey( smr.name ) )
						variations[ smr.name ] = new List< CompositeTexture >();
					variations[ smr.name ].AddRange( SaveMaterials( textures, OutputPath( assetRelativePath, assetFbx.name + "/" + smr.name ), smr.name ) );
				} else {
					defaultMaterialChildren.Add( smr );
				}
            }
			
			List< Combination > combinations = Build( new List<Combination>(), 0, new List< Combination >( variations.Values ) );
			
			createdMaterials += variations.Values.Count;
			
			foreach ( Combination c in combinations ) {
				GameObject copy = ( GameObject ) GameObject.Instantiate( assetFbx );
				copy.name = assetFbx.name;
			
				try {
					foreach ( CompositeTexture t in c ) {
						if ( t.parentName.Equals( "_root" ) ) {
							// atribui o material para todas as partes que não tem material específico
							foreach ( Renderer r in defaultMaterialChildren ) {
								GameObject part = ControllerUtils.FindChild( copy, r.name );
								if ( part == null )
									copy.renderer.material = t.GetMaterial();
									else
								part.renderer.material = t.GetMaterial();
							}
						} else {
							GameObject part = ControllerUtils.FindChild( copy, t.parentName );
							if ( part != null )
								part.renderer.material = t.GetMaterial();
							else
								Debugger.LogError( "Part not found: " + t.parentName );
						}
					}
					if ( CreatePrefab( copy, Path.Combine( assetRelativePath, copy.name + GetCombinationName( c ) ) ) )
						++createdCombinations;
				} catch ( Exception e ) {
					Debugger.LogError( e );
				}
			}
        }
		
		EditorUtility.DisplayDialog( WINDOW_TITLE, "Assets gerados:\n\nmateriais: " + createdMaterials + "\nprefabs: " + createdCombinations, "Ok");
    }
	
	
	/// <summary>
	/// Valida o item de menu. 
	/// </summary>
	[ MenuItem ( MENU_ITEM, true ) ]
    public static bool ValidateGenerateAssets() {
		Object[] selected = Selection.GetFiltered( typeof( GameObject ), SelectionMode.DeepAssets );
		if ( selected.Length <= 0 )
			return false;
		
		foreach ( GameObject assetFbx in selected ) {
            if ( assetFbx.name.Contains( "@" ) )
				continue;
			
			string assetPath = AssetRoot( assetFbx );
			
			// evita que alguém execute o script num objeto que esteja fora da pasta correta
			if ( !assetPath.StartsWith( PATH_ASSETS ) )
				return false;
		}
		
		return true;
    }
	
	
	/// <summary>
	/// Gera todas as combinações de materiais para as partes que compõem um objeto. O algoritmo é recursivo, indo até a
	/// última lista de variações de uma parte recebida, e gera as novas combinações (acumulando-as em list) conforme
	/// as chamadas vão sendo retornadas.
	/// </summary>
	private static List< Combination > Build( List< Combination > list, int index, List< Combination > variations ) {
		List< Combination > ret = new List< Combination >();
		if ( index >= variations.Count - 1 ) {
			foreach ( CompositeTexture c in variations[ index ] ) {
				Combination combo = new Combination();
				combo.Add( c );
				ret.Add( combo );
			}
			return ret;
		}
		
		List< Combination > childrenCombinations = Build( list, index + 1, variations );
		
		foreach ( CompositeTexture ct in variations[ index ] ) {
			foreach ( Combination combination in childrenCombinations ) {
				Combination c2 = new Combination();
				c2.Add( ct );
				c2.AddRange( combination );
				ret.Add( c2 );
			}
		}
		list.Clear();
		list.AddRange( ret );
		
		return list;
	}
	
	
	private static bool CreatePrefab( GameObject obj, string name ) {
		string path = Path.Combine( PATH_PREFABS, name + ".prefab" );
		Directory.CreateDirectory( Path.GetDirectoryName( path ) );
		
		if ( CanSaveFile( path ) ) {
	        Object prefab = EditorUtility.CreateEmptyPrefab( path );
	        EditorUtility.ReplacePrefab( obj, prefab );
	        AssetDatabase.Refresh();
			GameObject.DestroyImmediate( obj );
			
			return true;
		}
		
		return false;
    }
	
	
	private static List< Texture2D > LoadTextures( string path ) {
		if ( Directory.Exists( path ) ) {
			string[] files = Directory.GetFiles( path );
			List< Texture2D > textures = new List< Texture2D >();
			for ( int i = 0; i < files.Length; ++i ) {
				if ( validExtensions.IsMatch( files[ i ] ) )
					textures.Add( ( Texture2D ) AssetDatabase.LoadAssetAtPath( files[ i ], typeof( Texture2D ) ) );
			}
			
			return textures;
		}
		
		return new List< Texture2D >();
	}
	
	
	private static List< CompositeTexture > SaveMaterials( List< Texture2D > textures, string path, string parentName ) {
		Dictionary< string, CompositeTexture > compositeTextures = new Dictionary< string, CompositeTexture >();
		List< CompositeTexture > variations = new List< CompositeTexture >();
		
		// compõe todas as texturas (difusa + normal + altura + ...)
		foreach ( Texture2D t in textures ) {
			string cleanName;
			int index = t.name.LastIndexOf( '_' );
			
			if ( index >= 0 )
				cleanName = t.name.Substring( 0, index );
			else
				cleanName = t.name;
			
			if ( !compositeTextures.ContainsKey( cleanName ) )
				compositeTextures.Add( cleanName, new CompositeTexture( path + "_materiais/", cleanName, parentName ) );
			
			compositeTextures[ cleanName ].Add( t );
		}
		
		foreach ( CompositeTexture ct in compositeTextures.Values ) {
			ct.Save();
			// adiciona à lista de variações, mesmo que não tenha sido salvo (o arquivo já poderia existir)
			variations.Add( ct );
		}
		
		return variations;
	}
	
	
	// Returns the path to the directory that holds the specified FBX.
    private static string AssetRoot( GameObject asset ) {
        string root = AssetDatabase.GetAssetPath( asset );
        return root.Substring( 0, root.LastIndexOf( '/' ) + 1 );
    }

	
	private static string OutputPath( string assetRelativePath, string assetName ) {
		return PATH_PREFABS + assetRelativePath + assetName + "/";
	}
	
	
	private static string GetCombinationName( Combination c ) {
		StringBuilder sb = new StringBuilder();
		
		foreach ( CompositeTexture t in c ) {
			sb.Append( '_' );
			sb.Append( t.name );
		}
		
		return sb.ToString();
	}
		
		
	public static bool CanSaveFile( string path ) {
		if ( !GenerateResources.overwriteFiles && File.Exists( path ) ) {
			switch ( EditorUtility.DisplayDialogComplex( GenerateResources.WINDOW_TITLE, "Foi encontrado um arquivo em \"" + path + "\".", "Substituir todos", "Substituir este", "Manter original" ) ) {
				case 0:
					GenerateResources.overwriteFiles = true;
					return true;
					
				case 1:
					return true;
					
				case 2:
				default:
					return false;
			}
		}
			
		return true;
	}
	
	
}

/// <summary>
/// Classe auxiliar para criar o material correto a partir da presença ou não dos mapas de textura (normal, altura, etc.) 
/// </summary>
class CompositeTexture {
	
	protected Texture2D mainTexture;
	
	protected Texture2D normalMap;
	
	protected Texture2D heightMap;
	
	protected Material material;
	
	public string name;
	
	protected string path;
	
	public string parentName;
	
	
	public CompositeTexture( string path, string name, string parentName ) {
		this.path = path + name + ".mat";
		this.name = name;
		this.parentName = parentName;
	}
	
	
	public void Add( Texture2D t ) {
		if ( !t.name.Contains( "_" ) ) {
			// não é uma textura com mapa especial
			mainTexture = t;
		} else {
			string type = t.name.Substring( t.name.LastIndexOf( '_' ) + 1 );
			
			switch ( type ) {
			case "normal":
				normalMap = t;
				break;
				
			case "altura":
				heightMap = t;
				break;
			}
		}
	}
	
	
	public Shader GetShader() {
		string s = null;
		if ( normalMap == null )
			s = "Specular";
		else
			s = "Bumped Specular";
		
		if ( s != null )
			return Shader.Find( s );
		
		return null;
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <returns>
	/// A <see cref="Material"/>
	/// </returns>
	public Material GetMaterial() {
		if ( material == null ) {
			// Create the Material
			material = new Material( GetShader() );
			material.SetTexture( "_MainTex", mainTexture );
			material.name = name;
			
			if ( normalMap != null )
				material.SetTexture( "_BumpMap", normalMap );
			
			if ( heightMap != null )
				material.SetTexture( "_HeightMap", heightMap );
		}
		
		return material;
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public bool Save() {
		if ( GenerateResources.CanSaveFile( path ) ) {
			Directory.CreateDirectory( path.Substring( 0, path.LastIndexOf( '/' ) + 1 ) );
			
			AssetDatabase.CreateAsset( GetMaterial(), path );
			return true;
		}
		
		return false;
	}
	
	
	public override string ToString() {
		StringBuilder sb = new StringBuilder( "[CompositeTexture name:" );
		
		sb.Append( name );
		//		sb.Append( " main:" );
		//		sb.Append( mainTexture );
		//		sb.Append( " normal:" );
		//		sb.Append( normalMap );
		//		sb.Append( " height:" );
		//		sb.Append( heightMap );
		sb.Append( "]" );
		
		return sb.ToString();
	}
	
}

