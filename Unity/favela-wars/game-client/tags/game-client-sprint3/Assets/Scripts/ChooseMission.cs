using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using GameController;
using GameCommunication;


using Utils;
	
	
public class ChooseMission : MonoBehaviour {

	public void OnGUI() {
		string[] missions = Directory.GetFiles( Path.Combine( Application.absoluteURL, "missions" ), "*.mission" );
	
		for ( int i = 0; i < missions.Length; ++i ) {
			string mission = Path.GetFileNameWithoutExtension( missions[ i ] );
			bool even = ( i & 1 ) == 0;
			int y = 10 + ( i >> 1 ) * 40;
			
			if( GUI.Button( new Rect( even ? 10 : 200, y, 100, 30 ), mission ) ) {
				GameController.GameController gc = GameController.GameController.GetInstance();
				
				List< uint > soldierIDs = new List< uint > { 0x00ff00ff, 0x00cc00ff, 0x008800ff, 0x004400ff }; // TODO teste
				StartMissionRequest loadRequest = new StartMissionRequest( mission, soldierIDs );
				gc.SendRequest( loadRequest );
				
				gc.SendRequest( new LoadSnapshotRequest() );
			}	
		}
	}
	
}
