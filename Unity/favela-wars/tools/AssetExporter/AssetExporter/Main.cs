using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using Affirma.ThreeSharp.Model;
using Affirma.ThreeSharp.Query;
using Affirma.ThreeSharp;


namespace AssetExporter {

	public class MainClass {
		
		// Insert your AWS access keys here.
        private static readonly string awsAccessKeyId = "AKIAILVD4BVW4NQMA5VA";
        private static readonly string awsSecretAccessKey = "9dTau4St6wH2SxalEkPWxLjfqCNpH/HNbKH1ucoj";
		
		
		private const int ARG_INDEX_BUCKET_NAME	= 0;
		private const int ARG_INDEX_FILE_LIST	= 1;
		
		
		public static void Main( string[] args ) {
			Dictionary< string, string > paths = null;
			
			string bucketName = args[ ARG_INDEX_BUCKET_NAME ];
			
			Console.WriteLine( "Writing to Amazon S3 bucket (" + bucketName + ")" );
			
			try {
				paths = DeserializeFromBase64String< Dictionary< string, string > >( args[ ARG_INDEX_FILE_LIST ] );
			} catch ( Exception e ) {
				Console.WriteLine( "Error deserializing paths:\n" + e );
				return;
			}
			
			ThreeSharpConfig config = new ThreeSharpConfig();
			config.AwsAccessKeyID = awsAccessKeyId;
			config.AwsSecretAccessKey = awsSecretAccessKey;
			config.IsSecure = false;
			
			IThreeSharp service = new ThreeSharpQuery(config);
			
			
			foreach ( string filename in paths.Keys ) {
				string s3Path = paths[ filename ];
				Console.Write( "Writing {0} to {1}... ", filename, s3Path );
				
				try {
					using ( ObjectAddRequest request = new ObjectAddRequest( bucketName, s3Path ) ) {
		                request.LoadStreamWithFile( filename );
		                using ( ObjectAddResponse response = service.ObjectAdd( request ) ) { 
						}
		            }
					
					Console.WriteLine( "OK" );
				} catch ( Exception e ) {
					Console.WriteLine( "ERROR:\n" + e );
				}
			}
		}
		
		// TODO integrar com common no SVN (remover métodos abaixo)
		
		/// <summary>
		/// Gets the filename from path.
		/// </summary>
		/// <returns>
		/// The filename from path.
		/// </returns>
		/// <param name='fullPath'>
		/// Full path.
		/// </param>
		/// <param name='keepExtension'>
		/// Keep extension.
		/// </param>
		public static string GetFilenameFromPath( string fullPath, bool keepExtension ) {
			char[] separators = { '\\', '/' };
			int index = Math.Max( 0, fullPath.LastIndexOfAny( separators ) + 1 );
			
			string filename = fullPath.Substring( index );
			if ( keepExtension )
				return filename;
			
			index = filename.LastIndexOf( '.' );
			
			return index >= 0 ? filename.Substring( 0, index ) : filename;
		}
		
		
		public static string GetDirectoryFromPath( string fullPath ) {
			char[] separators = { '\\', '/' };
			int index = Math.Max( 0, fullPath.LastIndexOfAny( separators ) );
			
			return index >= 0 ? fullPath.Substring( 0, index ) : fullPath;
		}		
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="o">
		/// A <see cref="Object"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.String"/>
		/// </returns>
		public static string SerializeToBase64String( object o ) {
			return System.Convert.ToBase64String( ObjectToByteArray( o ) );
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="base64String">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="T"/>
		/// </returns>
		public static T DeserializeFromBase64String< T >( string base64String ) {
			return ( T ) Convert.ChangeType( DeserializeFromBase64String( base64String ), typeof( T ) );
		}		
		
		
		/// <summary>
		/// Deserializes from base64 string.
		/// </summary>
		/// <returns>
		/// The from base64 string.
		/// </returns>
		/// <param name='base64String'>
		/// Base64 string.
		/// </param>
		public static object DeserializeFromBase64String( string base64String ) {
	 		MemoryStream memStream = new MemoryStream( System.Convert.FromBase64String( base64String ) );
			BinaryFormatter bin = new BinaryFormatter();
			bin.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
			
			return bin.Deserialize( memStream );
		}
		
		
		/// <summary>
		/// Obtém a representação em byte[] de um objeto serializável.
		/// </summary>
		public static byte[] ObjectToByteArray( object o ) {
			MemoryStream memStream = new MemoryStream();
		    BinaryFormatter bin = new BinaryFormatter();
			
			bin.Serialize( memStream, o );
			
			return memStream.ToArray();
		}		
		
	}
}
