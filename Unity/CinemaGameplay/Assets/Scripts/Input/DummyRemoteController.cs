using UnityEngine;
using System.Collections;

public class DummyRemoteController : MonoBehaviour {
	public string ThrottleButton;
	public string BrakeButton;
	public string LeftButton;
	public string RightButton;
	public string ExitButton;
	public string IdentifyButton;
	
	
	private bool entered;
	private string code;
	private static int staticCode = 0;
	
	private float lastDir;
	private float lastAcc;
	
	private RemoteControllerManager manager;
	
	private static string getNewCode() {
		return (staticCode++).ToString();
	}
	
	void Awake () {
		code = getNewCode();
	}
	
	// Use this for initialization
	void Start () {
		getNewCode();
		manager = GetComponent<RemoteControllerManager>();
	}
	
	// Update is called once per frame
	void Update () {
		float acc = 0;
		float dir = 0;
		
		bool t = Input.GetButton( ThrottleButton );
		bool b = Input.GetButton( BrakeButton );
		bool r = Input.GetButton( RightButton );
		bool l = Input.GetButton( LeftButton );
		
		if( t || b || r || l ) {
			if( !entered ) {
				entered = manager.Call( RemoteActions.EnterGame, code, code );
			}
			if( t ) {
				acc = 1;
			}
			if( b ) {
				acc -= 1;
			}			
			
			if( l ^ r ) {
				if( l ) {
					dir = -1;
				}
				if( r ) {
					dir = 1;
				}
			}
		}
		
		if( Input.GetButtonDown( IdentifyButton ) ) {
			manager.Call( RemoteActions.Identify, code, code );
		}
			
		
		if( Input.GetButtonDown( ExitButton ) ) {
			manager.Call( RemoteActions.ExitGame, code );
			entered = false;
		} else {
			if( lastAcc != acc || lastDir != dir ) {
				if( manager.Call( RemoteActions.ChangeState, code, acc.ToString(), dir.ToString() ) ){
					lastAcc = acc;
					lastDir = dir;
				}
			}
		}
	}
}
