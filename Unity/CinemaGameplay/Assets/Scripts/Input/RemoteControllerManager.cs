using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>Type of actions that can be called by the web service</summary>
public enum RemoteActions {
	/// <summary>Action executed when a new player enter the game, Come with:
	/// <list type="bullet">
	/// <item> <term>ID;</term> </item>
	/// <item> <term>Name of Player.</term> </item>
	/// </list> </summary>
	EnterGame,
	/// <summary>Action to set new state of input of a player, Come with:
	/// <list type="bullet">
	/// <item> <term>ID;</term> </item>
	/// <item> <term>Acceleration;</term> </item>
	/// <item> <term>Direction.</term> </item>
	/// </list> </summary>
	ChangeState,
	/// <summary>Action to eliminate a player of the game, Come with:
	/// <list type="bullet">
	/// <item> <term>ID.</term> </item>
	/// </list> </summary>
	ExitGame,
	/// <summary>Action to indentify who is the player on screen, Come with:
	/// <list type="bullet">
	/// <item> <term>ID.</term> </item>
	/// </list> </summary>
	Identify,
	/// <summary>Action to change a name of a player, Come with:
	/// <list type="bullet">
	/// <item> <term>ID;</term> </item>
	/// <item> <term>Name of Player.</term> </item>
	/// </list> </summary>
	Rename
}

public class RemoteControllerManager : MonoBehaviour {
	private Dictionary< string, GameObject > cars = new Dictionary<string, GameObject>();
	public GameObject bumperCar;
	
	public Transform spawTransform;
	
	public static System.Random rand = new System.Random();

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	public bool Call( RemoteActions action, string id, params string[] actionParams ) {
		Debug.Log( action.ToString() + " " + id );
		switch( action ) {
			case RemoteActions.EnterGame: {
//				Debug.Log( "EnterGame " + id );
				if( !cars.ContainsKey( id ) ) {
//					Debug.Log( id + " not Exist" );
					GameObject obj = (GameObject)Instantiate( bumperCar, spawTransform.position, spawTransform.rotation );
					cars.Add( id, obj );
					CarCustomizer[] customs = obj.GetComponentsInChildren< CarCustomizer >();
					foreach( CarCustomizer c in customs ) {
						c.color1 = new Color( (float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble(), 1 );
						c.color2 = new Color( (float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble(), 1 );
					}
					CarController[] controllers = obj.GetComponentsInChildren< CarController >();
					foreach( CarController c in controllers ) {
						c.Rename( actionParams[0] );
					}
				}
				return true;
			}
			
			case RemoteActions.ExitGame: {
				if( cars.ContainsKey( id ) ) {
					GameObject obj = cars[id];
					cars.Remove( id );
					DestroyImmediate( obj );
					return true;
				}
				return false;
			}
			
			case RemoteActions.ChangeState: {
				if( actionParams != null && actionParams.Length > 1 && cars.ContainsKey( id ) ) {
					GameObject obj = cars[ id ];
					CarController[] controllers = obj.GetComponentsInChildren< CarController >();
					foreach( CarController c in controllers ) {
						c.setState( (float)Convert.ToDouble( actionParams[0] ), (float)Convert.ToDouble( actionParams[1] ) );
					}
					return true;
				}
				return false;
			}
			
			
			case RemoteActions.Rename: {
				if( actionParams != null && actionParams.Length > 0 && cars.ContainsKey( id ) ) {
					GameObject obj = cars[ id ];
					CarController[] controllers = obj.GetComponentsInChildren< CarController >();
					foreach( CarController c in controllers ) {
						c.Rename( actionParams[0] );
					}
					return true;
				}
				return false;
			}
			
			case RemoteActions.Identify: {
				if( cars.ContainsKey( id ) ) {
					GameObject obj = cars[ id ];
					CarController[] controllers = obj.GetComponentsInChildren< CarController >();
					foreach( CarController c in controllers ) {
						c.Show();
					}
					return true;
				}
				return false;
			}
		}
		return false;
	}
}
