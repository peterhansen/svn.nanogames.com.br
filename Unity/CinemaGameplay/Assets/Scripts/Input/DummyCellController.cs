using UnityEngine;
using System.Collections;

public class DummyCellController : MonoBehaviour {
	public string ThrottleButton;
	public string BrakeButton;
	public string LeftButton;
	public string RightButton;
	public string ExitButton;
	public string IdentifyButton;
	
	
	private bool entered;
	private string code;
	private static int staticCode = 0;
	
	private float lastDir;
	private float lastAcc;
	
	private RemoteControllerManager manager;
	
	private static string getNewCode() {
		return (staticCode++).ToString();
	}
	
	void Awake () {
		code = getNewCode();
	}
	
	// Use this for initialization
	void Start () {
		getNewCode();
		manager = GetComponent<RemoteControllerManager>();
	}
	
	// Update is called once per frame
	void Update () {
		float acc = 0;
		float dir = 0;
		if( Input.isGyroAvailable ) {
			Gyroscope gyro = Input.gyro;
//			 gyro.gravity
		}
		
		bool t = Input.GetButton( ThrottleButton );
		bool b = Input.GetButton( BrakeButton );
		bool r = Input.GetButton( RightButton );
		bool l = Input.GetButton( LeftButton );
		
		if( t || b || r || l ) {
			if( !entered ) {
				entered = manager.Call( RemoteActions.EnterGame, code );
			}
			if( t ) {
				acc = 1;
			}
			if( b ) {
				acc -= 1;
			}			
			
			if( l ^ r ) {
				if( l ) {
					dir = -1;
				}
				if( r ) {
					dir = 1;
				}
			}
		}
		
		if( Input.GetButtonDown( IdentifyButton ) ) {
			manager.Call( RemoteActions.Identify, code, code );
		}
			
		
		if( Input.GetButtonDown( ExitButton ) ) {
			entered = !manager.Call( RemoteActions.ExitGame, code );
			Debug.Log( entered );
		} else {
			if( lastAcc != acc || lastDir != dir ) {
				if( manager.Call( RemoteActions.ChangeState, code, acc.ToString(), dir.ToString() ) ){
					lastAcc = acc;
					lastDir = dir;
				}
			}
		}
	}
}
