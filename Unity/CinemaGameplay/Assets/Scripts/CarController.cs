using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class CarController : MonoBehaviour {
	#region Constants
	#endregion
	
	
	#region Statics
	#endregion
	
	
	#region Variables	
		#region Public(Inspector Can See)
			public WheelCollider wheelFR;
		    public WheelCollider wheelFL;
		    public WheelCollider wheelBR;
		    public WheelCollider wheelBL;
	
			public LightWithHalo lightFR;
		    public LightWithHalo lightFL;
		    public LightWithHalo lightBR;
		    public LightWithHalo lightBL;
			
			public LightController controller;
			
			public Transform labelPosition;
			
			public float maxSteerAngle;
			public float maxTorque;
			
			public float accel;
			public float steer;
			
			public float motorTorque = 20;
			
			public string name = "X";
			public Color textColor;
			public float showTime = 3;
			public float fontInitialSize = 60;
			public float fontFinalSize = 2;
		#endregion
		
		#region Private
			private float accTime;
			private GUIStyle style = new GUIStyle();
			private Rect rect;
		#endregion
	#endregion
	
	
	#region Properties
	#endregion
	
	
	#region Methods
		#region MonoBehavior Methods
			public void Start() {
			}
			
			
			public void Update() {
		        float delta = Time.deltaTime;
				
				if( accTime != 1 ) {
					float colorDelta = delta / showTime;
					if( accTime + colorDelta < 1 ) {
						accTime += colorDelta;
					} else {
						accTime = 1;
					}
				}
				
				style.normal.textColor = Color.Lerp( textColor, new Color( textColor.r, textColor.g, textColor.b, 0 ), accTime );
				style.fontSize = (int)Mathf.Lerp( fontInitialSize, fontFinalSize, accTime );
				
				wheelFL.steerAngle = wheelFR.steerAngle = (float)( steer * maxSteerAngle );
				wheelBL.motorTorque = wheelBR.motorTorque = motorTorque * accel;
				
				if( accel != 0 ) {
					controller.TurnOn();
					if( accel > 0 ) {
						lightBL.TurnOff();
						lightBR.TurnOff();
					} else {
						lightBL.TurnOn();
						lightBR.TurnOn();
					}
				} else {
					controller.TurnOff();
					lightBL.TurnOn();
					lightBR.TurnOn();
				}
			}
			
			
			public void OnGUI() {
		        Camera cam = Camera.current;
				
				if( cam != null ) {
					Vector2 size = style.CalcSize( new GUIContent( name ) );
					Vector2 pos = cam.WorldToScreenPoint( labelPosition.position );
					rect.width = size.x;
					rect.height = size.y;
					rect.x = Mathf.Clamp( pos.x - rect.width / 2, 0, Screen.width - rect.width );
					rect.y = Mathf.Clamp( Screen.height - ( pos.y + rect.height / 2 ), 0, Screen.height - rect.height );
					
					GUI.Label( rect, name, style );
				}
			}
		#endregion
		
		
		public void setState( float acc, float dir ) {
			accel = Mathf.Clamp( acc, -1, 1 );
			steer = Mathf.Clamp( dir, -1, 1 );
		}
		
		
		public void Show() {
			accTime = 0;
		}
		
		
		public void Rename( string newName ) {
			name = newName;
			Show();
		}
	#endregion
}