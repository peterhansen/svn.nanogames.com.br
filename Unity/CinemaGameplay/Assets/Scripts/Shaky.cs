using UnityEngine;
using System.Collections;

public class Shaky : MonoBehaviour {
	public Transform targetPosition;
	public Transform body;
	public Vector3 movementCubeSize;
	public float movementMinimumLoopTime = .1f;
	public float movementLoopTimeVariance = .2f;
	private RandomSequence xSequence = new RandomSequence();
	private RandomSequence ySequence = new RandomSequence();
	private RandomSequence zSequence = new RandomSequence();
	private Vector3 pos = new Vector3();
	

	// Use this for initialization
	void Start () {
		
	}
	
	
	void OnDrawGizmos() {
		float delta = Time.deltaTime;
				
		xSequence.desiredValue = targetPosition.position.x;
		ySequence.desiredValue = targetPosition.position.y;
		zSequence.desiredValue = targetPosition.position.z;
		
		xSequence.valueVariance = movementCubeSize.x;
		ySequence.valueVariance = movementCubeSize.y;
		zSequence.valueVariance = movementCubeSize.z;
		
		xSequence.minimumLoopTime = ySequence.minimumLoopTime = zSequence.minimumLoopTime = movementMinimumLoopTime; //movementMinimumLoopTime cant be zero
		xSequence.loopTimeVariance = ySequence.loopTimeVariance = zSequence.loopTimeVariance = movementLoopTimeVariance;
		
		xSequence.Update( delta );
		ySequence.Update( delta );
		zSequence.Update( delta );
		
		pos.x = xSequence.CurrentValue(); 
		pos.y = ySequence.CurrentValue(); 
		pos.z = zSequence.CurrentValue() ;
		
		body.position = pos;
	}
	                     
	
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime;
				
		xSequence.desiredValue = targetPosition.position.x;
		ySequence.desiredValue = targetPosition.position.y;
		zSequence.desiredValue = targetPosition.position.z;
		
		xSequence.valueVariance = movementCubeSize.x;
		ySequence.valueVariance = movementCubeSize.y;
		zSequence.valueVariance = movementCubeSize.z;
		
		xSequence.minimumLoopTime = ySequence.minimumLoopTime = zSequence.minimumLoopTime = movementMinimumLoopTime; //movementMinimumLoopTime cant be zero
		xSequence.loopTimeVariance = ySequence.loopTimeVariance = zSequence.loopTimeVariance = movementLoopTimeVariance;
		
		xSequence.Update( delta );
		ySequence.Update( delta );
		zSequence.Update( delta );
		
		pos.x = xSequence.CurrentValue(); 
		pos.y = ySequence.CurrentValue(); 
		pos.z = zSequence.CurrentValue() ;
		
		body.position = pos;
	}
}
