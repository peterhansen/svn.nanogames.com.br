using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour {
	public ParticleAnimator animator;
	public ParticleEmitter emitter;
	public Light light;
	public Color color;
	public float desireLightIntensity;
	public float intensityDecayVelocity = 1;
	private float lightIntensityMultiplier;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime;
		
		if( light != null ) light.color = color;
		
		float step = ( intensityDecayVelocity / desireLightIntensity ) * delta;
		
		if( emitter != null ) {
			if( !emitter.emit ) {
				if( lightIntensityMultiplier - step > 0 ) {
					lightIntensityMultiplier -= step;
				} else {
					lightIntensityMultiplier = 0;
				}
			} else {
				if( lightIntensityMultiplier + step < 1 ) {
					lightIntensityMultiplier += step;
				} else {
					lightIntensityMultiplier = 1;
				}
			}
		}
		
		light.intensity = desireLightIntensity * lightIntensityMultiplier;
	}
	
	public void TurnOn() {
		if( emitter != null ) {
			emitter.emit = true;
		}
	}
	
	public void TurnOff() {
		if( emitter != null ) {
			emitter.emit = false;
		}
	}
}
