using UnityEngine;
using System.Reflection;
using System.Collections;

public class DebugAllChildren : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UnityEngine.Component[] objs = transform.GetComponentsInChildren< UnityEngine.Component >();
		
		foreach( UnityEngine.Component obj in objs ) {
//			if( obj is Behaviour ) {
//				FieldInfo[] myFields = obj.GetType().GetFields();
//				Debug.Log( "Fields " + myFields.Length );
//				foreach( FieldInfo f in myFields ) {
//					Debug.Log( f );
//				}
//				PropertyInfo[] properties = obj.GetType().GetProperties();
//				Debug.Log( "Properties " + properties.Length );
//				foreach( PropertyInfo p in properties ) {
//					Debug.Log( p );
//				}
//			}
			Debug.Log( obj.GetType().ToString() );
		}
	}
}