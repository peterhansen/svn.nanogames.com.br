using UnityEngine;
using System.Collections;

public class LightWithHalo : MonoBehaviour {
	public Behaviour whiteHalo;
	public Behaviour coloredHalo;
	public Light light;
	

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	
	public void TurnOn() {
		if( !whiteHalo.enabled || !coloredHalo.enabled || !light.enabled ) {	
			whiteHalo.enabled = true;
			coloredHalo.enabled = true;
			light.enabled = true;
		}
	}
	
	
	public void TurnOff() {
		if( whiteHalo.enabled || coloredHalo.enabled || light.enabled ) {	
			whiteHalo.enabled = false;
			coloredHalo.enabled = false;
			light.enabled = false;
		}
	}
}
