using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugFPS : MonoBehaviour {
	public GUIText text;
	
	private float fps;
	private float accTime;
	private Queue< float > times = new Queue<float>();

	// Use this for initialization
	void Start () {
		times.Clear();
	}
	
	
	// Update is called once per frame
	void OnGUI() {
		float delta = Time.deltaTime;
		accTime += delta;
		times.Enqueue( delta );
		while( accTime > 1 ) {
			if( times.Count > 0 ) {
				accTime -= times.Dequeue();
			} else {
				accTime = 0;
			}
		}
		fps = ( accTime > 0 ) ? ( times.Count / accTime ) : 0;
		text.text = string.Format("{0:0.0}", fps) + " fps";
	}
}
