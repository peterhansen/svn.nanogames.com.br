using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class RandomSequence {
	public float desiredValue = 1;
	public float valueVariance = 2;
	public float minimumLoopTime = 1;
	public float loopTimeVariance = 2;
	private float finalValue = 0;
	private float currentTime = 0;
	private float initialValue = 0;
	private float currentValue = 0;
	private float currentLoopTime = 0;
	private static System.Random random = new System.Random();
	
	
	public float CurrentValue() {
		return currentValue;
	}
	
	
	public void Update( float delta ) {
		currentTime += delta;
		while( currentTime > currentLoopTime && ( minimumLoopTime > 0 ) && ( ( minimumLoopTime + loopTimeVariance ) > 0 ) ) {
			currentTime -= currentLoopTime;
			currentLoopTime = minimumLoopTime + (float)random.NextDouble() * loopTimeVariance;
			initialValue = finalValue;
			finalValue = desiredValue + ( ( (float)random.NextDouble() - .5f ) * valueVariance );			
		}
		currentValue = initialValue + ( ( currentTime / currentLoopTime ) * ( finalValue - initialValue ) );
	}
}
