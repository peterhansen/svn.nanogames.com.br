using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class CarCustomizer : MonoBehaviour {
	
	public MeshRenderer mesh1;
	public MeshRenderer mesh2;
	
	public Color color1;
	public Color color2;
	

	// Use this for initialization
	void Start () {
		mesh1.material.color = color1;
		mesh2.material.color = color2;
	}
	
	// Update is called once per frame
	void Update () {
		mesh1.material.color = color1;
		mesh2.material.color = color2;
	}
}