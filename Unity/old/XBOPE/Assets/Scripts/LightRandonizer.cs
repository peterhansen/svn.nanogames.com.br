using UnityEngine;
using System.Collections;

public class LightRandonizer : MonoBehaviour {
	public Transform center;
	public new Light light;
	public Vector3 movementCubeSize;
	public float movementMinimumLoopTime = .1f;
	public float movementLoopTimeVariance = .2f;
	public RandomSequence intensitySequence;
	private RandomSequence xSequence = new RandomSequence();
	private RandomSequence ySequence = new RandomSequence();
	private RandomSequence zSequence = new RandomSequence();
	

	// Use this for initialization
	void Start () {
		
	}
	
	
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime;
		
		intensitySequence.Update( delta );
		light.intensity = intensitySequence.CurrentValue();
		
		xSequence.desiredValue = center.position.x;
		ySequence.desiredValue = center.position.y;
		zSequence.desiredValue = center.position.z;
		
		xSequence.valueVariance = movementCubeSize.x;
		ySequence.valueVariance = movementCubeSize.y;
		zSequence.valueVariance = movementCubeSize.z;
		
		xSequence.minimumLoopTime = ySequence.minimumLoopTime = zSequence.minimumLoopTime = movementMinimumLoopTime;
		xSequence.loopTimeVariance = ySequence.loopTimeVariance = zSequence.loopTimeVariance = movementLoopTimeVariance;
		
		xSequence.Update( delta );
		ySequence.Update( delta );
		zSequence.Update( delta );
		
		light.transform.position = new Vector3( xSequence.CurrentValue(), ySequence.CurrentValue(), zSequence.CurrentValue() );
		
//		light.transform.position.x = xSequence.CurrentValue();
//		light.transform.position.y = ySequence.CurrentValue();
//		light.transform.position.z = zSequence.CurrentValue();
	}
}
