﻿namespace OiTcpDebugger
{
    partial class frmApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btReplay = new System.Windows.Forms.Button();
            this.lbSelectSearchCriterias = new System.Windows.Forms.Label();
            this.lbResults = new System.Windows.Forms.Label();
            this.lbIpClient = new System.Windows.Forms.Label();
            this.lbDate = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.mtbIpClient = new System.Windows.Forms.MaskedTextBox();
            this.lbIpHost = new System.Windows.Forms.Label();
            this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpTimeBegin = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.btSearch = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 277);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(748, 276);
            this.dataGridView1.TabIndex = 4;
            // 
            // btReplay
            // 
            this.btReplay.Location = new System.Drawing.Point(664, 568);
            this.btReplay.Name = "btReplay";
            this.btReplay.Size = new System.Drawing.Size(96, 29);
            this.btReplay.TabIndex = 5;
            this.btReplay.Text = "&Replay";
            this.btReplay.UseVisualStyleBackColor = true;
            this.btReplay.Click += new System.EventHandler(this.onReplaySession);
            // 
            // lbSelectSearchCriterias
            // 
            this.lbSelectSearchCriterias.AutoSize = true;
            this.lbSelectSearchCriterias.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSelectSearchCriterias.Location = new System.Drawing.Point(11, 9);
            this.lbSelectSearchCriterias.Name = "lbSelectSearchCriterias";
            this.lbSelectSearchCriterias.Size = new System.Drawing.Size(246, 17);
            this.lbSelectSearchCriterias.TabIndex = 2;
            this.lbSelectSearchCriterias.Text = "Determine os seus critérios de busca:";
            // 
            // lbResults
            // 
            this.lbResults.AutoSize = true;
            this.lbResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbResults.Location = new System.Drawing.Point(11, 256);
            this.lbResults.Name = "lbResults";
            this.lbResults.Size = new System.Drawing.Size(83, 17);
            this.lbResults.TabIndex = 3;
            this.lbResults.Text = "Resultados:";
            // 
            // lbIpClient
            // 
            this.lbIpClient.AutoSize = true;
            this.lbIpClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIpClient.Location = new System.Drawing.Point(6, 28);
            this.lbIpClient.Name = "lbIpClient";
            this.lbIpClient.Size = new System.Drawing.Size(91, 17);
            this.lbIpClient.TabIndex = 4;
            this.lbIpClient.Text = "IP do Cliente:";
            // 
            // lbDate
            // 
            this.lbDate.AutoSize = true;
            this.lbDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDate.Location = new System.Drawing.Point(6, 26);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(117, 17);
            this.lbDate.TabIndex = 5;
            this.lbDate.Text = "Início do Período:";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(9, 46);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 0;
            // 
            // mtbIpClient
            // 
            this.mtbIpClient.Location = new System.Drawing.Point(9, 106);
            this.mtbIpClient.Mask = "##0\\.##0\\.##0\\.##0\\:####0";
            this.mtbIpClient.Name = "mtbIpClient";
            this.mtbIpClient.Size = new System.Drawing.Size(159, 20);
            this.mtbIpClient.TabIndex = 1;
            this.mtbIpClient.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onIPTextBoxKeyDown);
            this.mtbIpClient.Leave += new System.EventHandler(this.onLeaveIPTextBox);
            this.mtbIpClient.Enter += new System.EventHandler(this.onEnterIPTextBox);
            // 
            // lbIpHost
            // 
            this.lbIpHost.AutoSize = true;
            this.lbIpHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIpHost.Location = new System.Drawing.Point(6, 86);
            this.lbIpHost.Name = "lbIpHost";
            this.lbIpHost.Size = new System.Drawing.Size(77, 17);
            this.lbIpHost.TabIndex = 8;
            this.lbIpHost.Text = "IP do Host:";
            // 
            // dtpDateEnd
            // 
            this.dtpDateEnd.CustomFormat = "hh:mm";
            this.dtpDateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpDateEnd.Location = new System.Drawing.Point(6, 106);
            this.dtpDateEnd.Name = "dtpDateEnd";
            this.dtpDateEnd.Size = new System.Drawing.Size(76, 20);
            this.dtpDateEnd.TabIndex = 1;
            // 
            // dtpTimeBegin
            // 
            this.dtpTimeBegin.CustomFormat = "hh:mm";
            this.dtpTimeBegin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTimeBegin.Location = new System.Drawing.Point(6, 46);
            this.dtpTimeBegin.Name = "dtpTimeBegin";
            this.dtpTimeBegin.Size = new System.Drawing.Size(76, 20);
            this.dtpTimeBegin.TabIndex = 0;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(9, 106);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 556);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Número de resultados:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Fim do Período";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Fim do Intervalo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "Início do Intervalo:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbDate);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 147);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data de Acesso";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpTimeBegin);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dtpDateEnd);
            this.groupBox2.Location = new System.Drawing.Point(247, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(136, 147);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Intervalo de Acesso";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.maskedTextBox1);
            this.groupBox3.Controls.Add(this.lbIpHost);
            this.groupBox3.Controls.Add(this.lbIpClient);
            this.groupBox3.Controls.Add(this.mtbIpClient);
            this.groupBox3.Location = new System.Drawing.Point(399, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(361, 147);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Outros Critérios";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(9, 48);
            this.maskedTextBox1.Mask = "##0\\.##0\\.##0\\.##0\\:####0";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(159, 20);
            this.maskedTextBox1.TabIndex = 0;
            this.maskedTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.onIPTextBoxKeyDown);
            this.maskedTextBox1.Leave += new System.EventHandler(this.onLeaveIPTextBox);
            this.maskedTextBox1.Enter += new System.EventHandler(this.onEnterIPTextBox);
            // 
            // btSearch
            // 
            this.btSearch.Location = new System.Drawing.Point(293, 202);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(187, 29);
            this.btSearch.TabIndex = 3;
            this.btSearch.Text = "&Buscar Resultados";
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.onSearchResults);
            // 
            // frmApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 609);
            this.Controls.Add(this.btSearch);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbResults);
            this.Controls.Add(this.lbSelectSearchCriterias);
            this.Controls.Add(this.btReplay);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmApp";
            this.Text = "Oi TCP Debugger";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btReplay;
        private System.Windows.Forms.Label lbSelectSearchCriterias;
        private System.Windows.Forms.Label lbResults;
        private System.Windows.Forms.Label lbIpClient;
        private System.Windows.Forms.Label lbDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.MaskedTextBox mtbIpClient;
        private System.Windows.Forms.Label lbIpHost;
        private System.Windows.Forms.DateTimePicker dtpDateEnd;
        private System.Windows.Forms.DateTimePicker dtpTimeBegin;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}

