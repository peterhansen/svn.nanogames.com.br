﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace OiTcpDebugger
{
    public partial class frmApp : Form
    {
        // Variáveis da classe
        Process emulatorProcess;

        // Construtor
        public frmApp()
        {
            InitializeComponent();

            emulatorProcess = null;
        }

        private void onLeaveIPTextBox( object sender, EventArgs e )
        {
            // http://social.msdn.microsoft.com/Forums/en-US/csharpgeneral/thread/27d1e86e-8561-4379-b5c5-401f15c24c97

            MaskedTextBox tb = ( MaskedTextBox )sender;

            // Resets the cursor when we leave the textbox
            tb.SelectionStart = 0;

            // Enable the tabstop property so we can cycle through the form controls again  
            foreach( Control c in this.Controls )  
                c.TabStop = true;  
        }

        private void onEnterIPTextBox( object sender, EventArgs e )
        {
            MaskedTextBox tb = ( MaskedTextBox )sender;

            this.BeginInvoke((MethodInvoker)delegate()
            {
                int pos = tb.SelectionStart;

                if( pos > tb.Text.Length )
                    pos = tb.Text.Length;

                tb.Select(pos, 0);
            });

            // Resets the cursor when we enter the textbox  
            //tb.Select(0, 0);
            //tb.SelectionStart = 0;
            //tb.SelectionLength = 3;

            // Disable the TabStop property to prevent the form and its controls to catch the Tab key  
            foreach( Control c in this.Controls ) 
                c.TabStop = false; 
        }

        private void onIPTextBoxKeyDown( object sender, KeyEventArgs e )
        {
            // Cycle through the mask fields  
            if( e.KeyCode == Keys.Tab )
            {
                MaskedTextBox tb = ( MaskedTextBox )sender;

                int pos = tb.SelectionStart;  
                int max = (tb.MaskedTextProvider.Length - tb.MaskedTextProvider.EditPositionCount);  
                int nextField = 0;  
 
                for( int i = 0; i < tb.MaskedTextProvider.Length; ++i )
                {  
                    if( !tb.MaskedTextProvider.IsEditPosition(i) && ( pos + max ) >= i )
                        nextField = i;  
                }  
                ++nextField;
 
                // We're done, enable the TabStop property again  
                if( pos == nextField )
                    onLeaveIPTextBox( tb, e );
 
                tb.SelectionStart = nextField;
            } 
        }

        private void onSearchResults(object sender, EventArgs e)
        {
            // http://forums.mysql.com/read.php?38,115063,115063

            // Executa as queries no banco de dados
    //            *
    //  First, you need to install the mysql connector/net, it is located at: http://dev.mysql.com/downloads/connector/net/1.0.html
    //*
    //  Next create a new project
    //*
    //  Next add reference to: MySql.Data
    //*
    //  Next add "using MySql.Data.MySqlClient;"
    //*
    //  Finally add the following code to your application:


            string MyConString = "SERVER=localhost;" +
    "DATABASE=mydatabase;" +
    "UID=testuser;" +
    "PASSWORD=testpassword;";
            MySqlConnection connection = new MySqlConnection(MyConString);
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            command.CommandText = "select * from mycustomers";
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                string thisrow = "";
                for (int i = 0; i < Reader.FieldCount; i++)
                    thisrow += Reader.GetValue(i).ToString() + ",";
                listBox1.Items.Add(thisrow);
            }
            connection.Close();
        }

        // Exibe a sessão selecionada através do emulador
        private void onReplaySession(object sender, EventArgs e)
        {
            // Roda o emulador
            if( startEmulator() )
            {
                // Inicia o servidor TCP
                startServer();
            }
            else
            {
                Console.WriteLine( "Could not run emulator" );
            }
        }

        private void startServer()
        {
            // - http://msdn.microsoft.com/en-us/library/system.net.sockets.socket%28VS.90%29.aspx
            // http://stackoverflow.com/questions/199528/c-console-receive-input-with-pipe
            // http://www.codeguru.com/csharp/csharp/cs_misc/reflection/article.php/c7261
            // http://www.switchonthecode.com/tutorials/csharp-tutorial-simple-threaded-tcp-server
            // http://msdn.microsoft.com/en-us/library/system.net.sockets.socket%28VS.90%29.aspx
            // http://msdn.microsoft.com/en-us/library/system.net.sockets.tcpclient.aspx
        }

        private bool startEmulator()
        {
            ProcessStartInfo psi = new ProcessStartInfo( @"C:\listfiles.bat" );
            psi.CreateNoWindow = false;
            psi.WindowStyle = ProcessWindowStyle.Normal;
            psi.RedirectStandardOutput = false;
            psi.UseShellExecute = false;

            try
            {
                emulatorProcess = Process.Start( psi );
                return true;
            }
            catch( Exception ex )
            {
                Console.WriteLine( "Exception caught in startEmulator: " + ex.Message );
                return false;
            }
        }
    }
}
