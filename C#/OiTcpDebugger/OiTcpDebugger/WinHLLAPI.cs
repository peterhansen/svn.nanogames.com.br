﻿using System;
using System.Runtime.InteropServices;

namespace OiTN3270Debugger
{
	static class WinHLLAPI
	{
		#region Windows HLLAPI Return Codes

		public const int WHLLOK				=	  0;	// SUCCESSFUL
		public const int WHLLNOTCONNECTED	=	  1;	// NOT CONNECTED TO PRESENTATION SPACE
		public const int WHLLBLOCKNOTAVAIL	=	  1;	// REQUESTED SIZE IS NOT AVAILABLE
		public const int WHLLPARAMETERERROR	=	  2;	// PARAMETER ERROR/INVALID FUNCTION
		public const int WHLLBLOCKIDINVALID	=	  2;	// INVALID BLOCK ID WAS SPECIFIED
		public const int WHLLFTXCOMPLETE	=	  3;	// FILE TRANSFER COMPLETE
		public const int WHLLFTXSEGMENTED	=	  4;	// FILE TRANSFER COMPLETE / SEGMENTED
		public const int WHLLBUSY			=	  4;	// PRESENATION SPACE IS BUSY
		public const int WHLLINHIBITED		=	  5;	// INHIBITED / KKEYBOARD LOCKED
		public const int WHLLTRUNCATED		=	  6;	// DATA TRUNCATED
		public const int WHLLPOSITIONERROR	=	  7;	// INVALID PRESENTATION SPACE POSITION
		public const int WHLLNOTAVAILABLE	=	  8;	// UNAVAILABLE OPERATION
		public const int WHLLSYSERROR		=	  9;	// SYSTEM ERROR
		public const int WHLLNOTSUPPORTED	=	 10;	// FUNCTION NOT SUPPORTED
		public const int WHLLUNAVAILABLE	=	 11;	// RESOURCE IS UNAVAILABLE
		public const int WHLLPSENDED		=	 12;	// THE SESSION WAS STOPPED
		public const int WHLLUNDEFINEDKEY	=	 20;	// UNDEFINED KEY COMBINATION
		public const int WHLLOIAUPDATE		=	 21;	// OIA UPDATED
		public const int WHLLPSUPDATE		=	 22;	// PS UPDATED
		public const int WHLLBOTHUPDATE		=	 23;	// BOTH PS AND OIA UPDATED
		public const int WHLLNOFIELD		=	 24;	// NO SUCH FIELD FOUND
		public const int WHLLNOKEYSTROKES	=	 25;	// NO KEYSTROKES ARE AVAILABLE
		public const int WHLLPSCHANGED		=	 26;	// PS OR OIA CHANGED
		public const int WHLLFTXABORTED		=	 27;	// FILE TRANSFER ABORTED
		public const int WHLLZEROLENFIELD	=	 28;	// FIELD LENGTH IS ZERO
		public const int WHLLKEYOVERFLOW	=	 31;	// KEYSTROKE OVERFLOW
		public const int WHLLSFACONN		=	 32;	// OTHER APPLICATION ALREADY CONNECTED
		public const int WHLLTRANCANCLI		=	 34;	// MESSAGE SENT INBOUND TO HOST CANCELLED
		public const int WHLLTRANCANCL		=	 35;	// OUTBOUND TRANS FROM HOST CANCELLED
		public const int WHLLHOSTCLOST		=	 36;	// CONTACT WITH HOST WAS LOST
		public const int WHLLOKDISABLED		=	 37;	// THE FUNCTION WAS SUCCESSFUL
		public const int WHLLNOTCOMPLETE	=	 38;	// THE REQUESTED FN WAS NOT COMPLETED
		public const int WHLLSFDDM			=	 39;	// ONE DDM SESSION ALREADY CONNECTED
		public const int WHLLSFDPEND		=	 40;	// DISCONNECTED W ASYNC REQUESTS PENDING
		public const int WHLLBUFFINUSE		=	 41;	// SPECIFIED BUFFER CURRENTLY IN USE
		public const int WHLLNOMATCH		=	 42;	// NO MATCHING REQUEST FOUND
		public const int WHLLLOCKERROR		=	 43;	// API ALREADY LOCKED OR UNLOCKED

		public const int WHLLINVALIDFUNCTIONNUM	= 301;	// INVALID FUNCTION NUMBER
		public const int WHLLFILENOTFOUND		= 302;	// FILE NOT FOUND
		public const int WHLLACCESSDENIED		= 305;	// ACCESS DENIED
		public const int WHLLMEMORY				= 308;	// INSUFFICIENT MEMORY
		public const int WHLLINVALIDENVIRONMENT	= 310;	// INVALID ENVIRONMENT
		public const int WHLLINVALIDFORMAT		= 311;	// INVALID FORMAT

		public const int WHLLINVALIDPSID		= 9998;	// INVALID PRESENTATION SPACE ID
		public const int WHLLINVALIDRC			= 9999;	// INVALID ROW OR COLUMN CODE

		#endregion

		#region Windows HLLAPI Extentions Return Codes

		public const int WHLLALREADY         = 0xF000;	/* An async call is already outstanding */
		public const int WHLLINVALID         = 0xF001;	/* Async Task ID is invalid */
		public const int WHLLCANCEL          = 0xF002;	/* Blocking call was cancelled */
		public const int WHLLSYSNOTREADY     = 0xF003;	/* Underlying subsystem not started */
		public const int WHLLVERNOTSUPPORTED = 0xF004;	/* Application version not supported */

		#endregion

		#region Windows HLLAPI Session Option Definitions

		public const int PARM_STRLEN					= 1;
		public const int PARM_STREOT					= 2;
		public const int PARM_SRCHALL					= 1;
		public const int PARM_SRCHFROM					= 2;
		public const int PARM_SRCHFRWD					= 1;
		public const int PARM_SRCHBKWD					= 2;
		public const int PARM_NOATTRB					= 1;
		public const int PARM_ATTRB						= 2;
		public const int PARM_FPAUSE					= 1;
		public const int PARM_IPAUSE					= 2;
		public const int PARM_NOQUIET					= 1;
		public const int PARM_QUIET						= 2;
		public const int PARM_AUTORESET					= 1;
		public const int PARM_NORESET					= 2;
		public const int PARM_TWAIT						= 1;
		public const int PARM_LWAIT						= 2;
		public const int PARM_NWAIT						= 3;
		public const int PARM_TROFF						= 1;
		public const int PARM_TRON						= 2;
		public const int PARM_NOEAB						= 2;
		public const int PARM_EAB						= 1;
		public const int PARM_XLATE						= 1;
		public const int PARM_NOXLATE					= 2;
		public const int PARM_CONLOG					= 1;
		public const int PARM_CONPHYS					= 2;
		public const int PARM_OLDOIA					= 1;
		public const int PARM_NEWOIA					= 2;
		public const int PARM_NOCFGSIZE					= 1;
		public const int PARM_CFGSIZE					= 2;
		public const int PARM_DISPLAY					= 1;
		public const int PARM_NODISPLAY					= 2;
		public const int PARM_WRITE_SUPER				= 1;
		public const int PARM_WRITE_WRITE				= 2;
		public const int PARM_WRITE_READ				= 3;
		public const int PARM_WRITE_NONE				= 4;
		public const int PARM_SUPER_WRITE				= 5;
		public const int PARM_READ_WRITE				= 6;
		public const int PARM_RETRY						= 1;
		public const int PARM_NORETRY					= 2;

		#endregion

		#region SetSessionParameters Values

		public const int WHLL_SSP_NEWRET      = 0x00000001;
		public const int WHLL_SSP_OLDRET      = 0x00000002;
		public const int WHLL_SSP_ATTRB       = 0x00000004;
		public const int WHLL_SSP_NOATTRB     = 0x00000008;
		public const int WHLL_SSP_NWAIT       = 0x00000010;
		public const int WHLL_SSP_LWAIT       = 0x00000020;
		public const int WHLL_SSP_TWAIT       = 0x00000040;
		public const int WHLL_SSP_EAB         = 0x00000080;
		public const int WHLL_SSP_NOEAB       = 0x00000100;
		public const int WHLL_SSP_AUTORESET   = 0x00000200;
		public const int WHLL_SSP_NORESET     = 0x00000400;
		public const int WHLL_SSP_SRCHALL     = 0x00001000;
		public const int WHLL_SSP_SRCHFROM    = 0x00002000;
		public const int WHLL_SSP_SRCHFRWD    = 0x00004000;
		public const int WHLL_SSP_SRCHBKWD    = 0x00008000;
		public const int WHLL_SSP_FPAUSE      = 0x00010000;
		public const int WHLL_SSP_IPAUSE      = 0x00020000;

		#endregion

		#region Convert Row or Column Values

		public const int WHLL_CONVERT_POSITION = 'P';
		public const int WHLL_CONVERT_ROW      = 'R';

		#endregion

		#region Storage Manager Sub-Function Values

		public const int WHLL_GETSTORAGE        = 1;
		public const int WHLL_FREESTORAGE       = 2;
		public const int WHLL_FREEALLSTORAGE    = 3;
		public const int WHLL_QUERYFREESTORAGE  = 4;

		#endregion

		#region Change PS Name Values

		public const int WHLL_CHANGEPSNAME_SET			= 0x01;
		public const int WHLL_CHANGEPSNAME_RESET		= 0x02;

		#endregion

		#region Window Status Values

		public const int WHLL_WINDOWSTATUS_SET			= 0x01;
		public const int WHLL_WINDOWSTATUS_QUERY		= 0x02;
		public const int WHLL_WINDOWSTATUS_EXTQUERY		= 0x03;

		public const int WHLL_WINDOWSTATUS_NULL			= 0x0000;
		public const int WHLL_WINDOWSTATUS_SIZE			= 0x0001;
		public const int WHLL_WINDOWSTATUS_MOVE			= 0x0002;
		public const int WHLL_WINDOWSTATUS_ZORDER		= 0x0004;
		public const int WHLL_WINDOWSTATUS_SHOW			= 0x0008;
		public const int WHLL_WINDOWSTATUS_HIDE			= 0x0010;
		public const int WHLL_WINDOWSTATUS_ACTIVATE		= 0x0080;
		public const int WHLL_WINDOWSTATUS_DEACTIVATE	= 0x0100;
		public const int WHLL_WINDOWSTATUS_MINIMIZE		= 0x0400;
		public const int WHLL_WINDOWSTATUS_MAXIMIZE		= 0x0800;
		public const int WHLL_WINDOWSTATUS_RESTORE		= 0x1000;

		public const int WHLL_WINDOWSTATUS_FRONT		= 0x00000003;
		public const int WHLL_WINDOWSTATUS_BACK			= 0x00000004;

		#endregion

		#region Lock API Values

		public const int WHLL_LOCKAPI_LOCK			= 'L';
		public const int WHLL_LOCKAPI_UNLOCK		= 'U';
		public const int WHLL_LOCKAPI_RETURN		= 'R';
		public const int WHLL_LOCKAPI_QUEUE			= 'Q';

		#endregion

		#region Windows HLLAPI Session Function Names

		public const int OEMFUNCTION = 0;  /* OEM Function */
		public const int CONNECTPS                =   1;  /* Connect Presentation Space */
		public const int DISCONNECTPS             =   2;  /* Disconnect Presentation Space */
		public const int SENDKEY                  =   3;  /* Send Key */
		public const int WAIT                     =   4;  /* Wait */
		public const int COPYPS                   =   5;  /* Copy Presentation Space */
		public const int SEARCHPS                 =   6;  /* Search Presentation Space */
		public const int QUERYCURSORLOC           =   7;  /* Query Cursor Location */
		public const int COPYPSTOSTR              =   8;  /* Copy Presentation Space To String */
		public const int SETSESSIONPARAMETERS     =   9;  /* Set Session Parameters */
		public const int QUERYSESSIONS            =  10;  /* Query Sessions */
		public const int RESERVE                  =  11;  /* Reserve */
		public const int RELEASE                  =  12;  /* Release */
		public const int COPYOIA                  =  13;  /* Copy OIA Information */
		public const int QUERYFIELDATTRIBUTE      =  14;  /* Query Field Attribute */
		public const int COPYSTRTOPS              =  15;  /* Copy String To Presentation Space */
		public const int STORAGEMGR               =  17;  /* Storage Manager */
		public const int PAUSE                    =  18;  /* Pause */
		public const int QUERYSYSTEM              =  20;  /* Query System */
		public const int RESETSYSTEM              =  21;  /* Reset System */
		public const int QUERYSESSIONSTATUS       =  22;  /* Query Session Status */
		public const int STARTHOSTNOTIFICATION    =  23;  /* Start Host Notification */
		public const int QUERYHOSTUPDATE          =  24;  /* Query Host Update */
		public const int STOPHOSTNOTIFICATION     =  25;  /* Stop Host Notification */
		public const int SEARCHFIELD              =  30;  /* Search Field */
		public const int FINDFIELDPOSITION        =  31;  /* Find Field Position */
		public const int FINDFIELDLENGTH          =  32;  /* Find Field Length */
		public const int COPYSTRINGTOFIELD        =  33;  /* Copy String To Field */
		public const int COPYFIELDTOSTRING        =  34;  /* Copy String To Field */
		public const int SETCURSOR                =  40;  /* Set Cursor */
		public const int STARTCLOSEINTERCEPT      =  41;  /* Start Close Intercept */
		public const int QUERYCLOSEINTERCEPT      =  42;  /* Query Close Intercept */
		public const int STOPCLOSEINTERCEPT       =  43;  /* Stop Close Intercept */
		public const int STARTKSINTERCEPT         =  50;  /* Start Keystroke Intercept */
		public const int GETKEY                   =  51;  /* Get Key */
		public const int POSTINTERCEPTSTATUS      =  52;  /* Post Intercept Status */
		public const int STOPKSINTERCEPT          =  53;  /* Stop  Keystroke Intercept */
		public const int LOCKPSAPI                =  60;  /* Lock Presentation Space API */
		public const int LOCKWSAPI                =  61;  /* Lock Window Services API */
		public const int SENDFILE                 =  90;  /* Send File */
		public const int RECEIVEFILE              =  91;  /* Receive File */
		public const int CONVERT                  =  99;  /* Convert Position or RowCol */
		public const int CONNECTWINDOWSERVICES    = 101;  /* Connect Window Services */
		public const int DISCONNECTWINDOWSERVICES = 102;  /* Disconnect Window Services */
		public const int QUERYWINDOWCOORDINATES   = 103;  /* Query or Set Window Coordinates */
		public const int WINDOWSTATUS             = 104;  /* Query or Set Window Status */
		public const int CHANGEPSNAME             = 105;  /* Change Presentation Space Name */
		public const int CONNECTSTRFLDS           = 120;  /* Connect Structured Fields */
		public const int DISCONSTRFLDS            = 121;  /* Disconnect Structured Fields */
		public const int QUERYCOMMBUFSIZ          = 122;  /* Query Communications Buffer Size */
		public const int ALLOCCOMMBUFF            = 123;  /* Allocate Communications Buffer */
		public const int FREECOMMBUFF             = 124;  /* Free Communications Buffer */
		public const int GETREQUESTCOMP           = 125;  /* Get Request Completion */
		public const int READSTRFLDS              = 126;  /* Read Structured Fields */
		public const int WRITESTRFLDS             = 127;  /* Write Structured Fields */

		#endregion

		#region Windows HLLAPI Types

		public const int WHLLDESCRIPTION_LEN	=  127;

		public unsafe struct WHLLAPIDATA
		{
			// public int wVersion;
			public fixed byte szDescription[ WHLLDESCRIPTION_LEN + 1 ];
		};

		#endregion

		/// <summary>
		/// Interface function with the HLLAPI DLL
		/// </summary>
		/// <param name="func">
		/// Specifies the number of the HLLAPI function you wish to call.
		/// </param>
		/// <param name="str">
		/// The data string you pass to the HLLAPI function.  This string must contain certain information depending
		/// on which function is to be called. It may contain information upon return from the HLLAPI function call. 
		/// For details, please refer to the Windows HLLAPI Specifications.
		/// </param>
		/// <param name="length">
		/// Specifies the length of the data string passed to the HLLAPI function but it is occassionally be used for
		/// other purposes depending on which function number is being called.  For details on each function, please
		/// refer to the Windows HLLAPI Specifications.
		/// </param>
		/// <param name="ret">
		/// Usually contains the error/success code upon return from the HLLAPI function call.  You can check this value
		/// to obtain information on the success or failure of the HLLAPI function call.  This parameter can also used for
		/// other purposes. Please refer to the Windows HLLAPI Specifications for details. 
		/// </param>
		/// <returns>
		/// This function returns nothing.
		/// </returns>
		[DllImport( "whllapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall )]
		public extern static unsafe void HLLAPI( int* func, byte* str, int* length, int* ret );

		[DllImport( "whllapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall )]
		public extern static unsafe int WinHLLAPIStartup( int wVersionRequired, WHLLAPIDATA* lpData );

		[DllImport( "whllapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall )]
		public extern static bool WinHLLAPICleanup();
	}
}
