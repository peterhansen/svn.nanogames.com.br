﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OiTN3270Debugger
{
	static class Utils
	{
		private const Byte DOT_KEY_VALUE = 190;
		private const Byte DOT_NUMPAD_KEY_VALUE = 194;
		public static bool IsValidIPInput( KeyEventArgs e )
		{
			if( !( e.KeyCode == Keys.D0 )
				&& !( e.KeyCode == Keys.D1 )
				&& !( e.KeyCode == Keys.D2 )
				&& !( e.KeyCode == Keys.D3 )
				&& !( e.KeyCode == Keys.D4 )
				&& !( e.KeyCode == Keys.D5 )
				&& !( e.KeyCode == Keys.D6 )
				&& !( e.KeyCode == Keys.D7 )
				&& !( e.KeyCode == Keys.D8 )
				&& !( e.KeyCode == Keys.D9 )
				&& !( e.KeyCode == Keys.NumPad0 )
				&& !( e.KeyCode == Keys.NumPad1 )
				&& !( e.KeyCode == Keys.NumPad2 )
				&& !( e.KeyCode == Keys.NumPad3 )
				&& !( e.KeyCode == Keys.NumPad4 )
				&& !( e.KeyCode == Keys.NumPad5 )
				&& !( e.KeyCode == Keys.NumPad6 )
				&& !( e.KeyCode == Keys.NumPad7 )
				&& !( e.KeyCode == Keys.NumPad8 )
				&& !( e.KeyCode == Keys.NumPad9 )
				&& !( e.KeyCode == Keys.Back )
				&& !( e.KeyCode == Keys.Delete )
				&& !( e.KeyCode == Keys.Left )
				&& !( e.KeyCode == Keys.Right )
				&& !( e.KeyCode == Keys.Up )
				&& !( e.KeyCode == Keys.Down )
				&& !( e.KeyCode == Keys.End )
				&& !( e.KeyCode == Keys.Home )
				&& !( e.KeyValue == DOT_KEY_VALUE )
				&& !( e.KeyValue == DOT_NUMPAD_KEY_VALUE )
				&& !( e.KeyCode == Keys.LShiftKey )
				&& !( e.KeyCode == Keys.RShiftKey )
				&& !( e.KeyCode == Keys.ShiftKey )
				&& !(( e.KeyCode == Keys.C ) && e.Control )
				&& !(( e.KeyCode == Keys.X ) && e.Control )
				&& !(( e.KeyCode == Keys.V ) && e.Control ))
			{
				return false;
			}
			return true;
		}

		public static int GetAIDCode( string aid )
		{
			int ret = 0;
			switch( aid.ToUpper() )
			{
				case "ENTER":
					ret = 0x7D;
					break;
				case "PF1":
					ret = 0xF1;
					break;
				case "PF2":
					ret = 0xF2;
					break;
				case "PF3":
					ret = 0xF3;
					break;
				case "PF4":
					ret = 0xF4;
					break;
				case "PF5":
					ret = 0xF5;
					break;
				case "PF6":
					ret = 0xF6;
					break;
				case "PF7":
					ret = 0xF7;
					break;
				case "PF8":
					ret = 0xF8;
					break;
				case "PF9":
					ret = 0xF9;
					break;
				case "PF10":
					ret = 0x7A;
					break;
				case "PF11":
					ret = 0x7B;
					break;
				case "PF12":
					ret = 0x7C;
					break;
				case "PF13":
					ret = 0xC1;
					break;
				case "PF14":
					ret = 0xC2;
					break;
				case "PF15":
					ret = 0xC3;
					break;
				case "PF16":
					ret = 0xC4;
					break;
				case "PF17":
					ret = 0xC5;
					break;
				case "PF18":
					ret = 0xC6;
					break;
				case "PF19":
					ret = 0xC7;
					break;
				case "PF20":
					ret = 0xC8;
					break;
				case "PF21":
					ret = 0xC9;
					break;
				case "PF22":
					ret = 0x4A;
					break;
				case "PF23":
					ret = 0x4B;
					break;
				case "PF24":
					ret = 0x4C;
					break;
				case "PA1":
					ret = 0x6C;
					break;
				case "PA2":
					ret = 0x6E;
					break;
				case "PA3":
					ret = 0x6B;
					break;
				case "CLEAR":
					ret = 0x6D;
					break;
				case "SYSREQ":
					ret = 0xF0;
					break;
			}
			return ret;
		}

		public static string GetAIDStr( byte aid )
		{
			string ret;
			switch( aid )
			{
				case 0x00:
					ret = "";
					break;

				case 0x7D:
					ret = "Enter";
					break;

				case 0xF1:
					ret = "PF1";
					break;

				case 0xF2:
					ret = "PF2";
					break;

				case 0xF3:
					ret = "PF3";
					break;

				case 0xF4:
					ret = "PF4";
					break;

				case 0xF5:
					ret = "PF5";
					break;

				case 0xF6:
					ret = "PF6";
					break;

				case 0xF7:
					ret = "PF7";
					break;

				case 0xF8:
					ret = "PF8";
					break;

				case 0xF9:
					ret = "PF9";
					break;

				case 0x7A:
					ret = "PF10";
					break;

				case 0x7B:
					ret = "PF11";
					break;

				case 0x7C:
					ret = "PF12";
					break;

				case 0xC1:
					ret = "PF13";
					break;

				case 0xC2:
					ret = "PF14";
					break;

				case 0xC3:
					ret = "PF15";
					break;

				case 0xC4:
					ret = "PF16";
					break;

				case 0xC5:
					ret = "PF17";
					break;

				case 0xC6:
					ret = "PF18";
					break;

				case 0xC7:
					ret = "PF19";
					break;

				case 0xC8:
					ret = "PF20";
					break;

				case 0xC9:
					ret = "PF21";
					break;

				case 0x4A:
					ret = "PF22";
					break;

				case 0x4B:
					ret = "PF23";
					break;

				case 0x4C:
					ret = "PF24";
					break;

				case 0x6C:
					ret = "PA1";
					break;

				case 0x6E:
					ret = "PA2";
					break;

				case 0x6B:
					ret = "PA3";
					break;

				case 0x6D:
					ret = "Clear";
					break;

				case 0xF0:
					ret = "Sysreq";
					break;

				case 0x41:
					ret = "Write";
					break;
				
				default:
					ret = "Unkown";
					break;
			}
			return ret;
		}

		public static void GetLineAndColumn( int cursorX, int cursorY, out int line, out int column )
		{
			try
			{
				int cx = Utils.EBCDICToDecimal( ( byte )cursorX );
				int cy = Utils.EBCDICToDecimal( ( byte )cursorY );
				int sum = ( cx * 64 ) + cy;
				line = ( sum / 80 ) + 1;
				column = ( sum % 80 ) + 1;
			}
			catch
			{
				line = -1;
				column = -1;
			}
		}

		public static byte EBCDICToDecimal( byte ebcdicCode )
		{
			// 0, 16, 32
			if( ( ebcdicCode == 0x40 ) || ( ebcdicCode == 0x50 ) || ( ebcdicCode == 0x60 ) )
				return ( byte )( (( ebcdicCode >> 4 ) - 0x4 ) * 16 );

			// [1,15]
			if( ( ebcdicCode >= 0xC1 && ebcdicCode <= 0xC9 ) || ( ebcdicCode >= 0x4A && ebcdicCode <= 0x4F ) )
				return ( byte )( ebcdicCode & 0x0F );

			// [17,31]
			if( ( ebcdicCode >= 0xD1 && ebcdicCode <= 0xD9 ) || ( ebcdicCode >= 0x5A && ebcdicCode <= 0x5F ) )
				return ( byte )( 16 + ( 0x0F & ebcdicCode ) );

			// [33, 47]
			if( ( ebcdicCode == 0x61 ) || ( ebcdicCode >= 0xE2 && ebcdicCode <= 0xE9 ) || ( ebcdicCode >= 0x6A && ebcdicCode <= 0x6F ) )
				return ( byte )( 32 + ( 0x0F & ebcdicCode ) );

			// [48, 63]
			if( ( ebcdicCode >= 0xF0 && ebcdicCode <= 0xF9 ) || ( ebcdicCode >= 0x7A && ebcdicCode <= 0x7F ) )
				return ( byte )( 48 + ( 0x0F & ebcdicCode ) );

			throw new ArgumentException( "Invalid EBCDIC code" );
		}

		public static String ByteArrayToHexString( byte[] b )
		{
			StringBuilder sb = new StringBuilder( b.Length * 2 );
			for( int i = 0 ; i < b.Length ; i++ )
			{
				byte v = b[ i ];
				if( v < 16 )
				{
					sb.Append( '0' );
				}

				sb.Append( v.ToString( "X" ) );
			}
			return sb.ToString();
		}

		public static string ConvertEBCDICtoASCII( byte[] strEBCDICString )
		{
			byte[] e2a = new byte[ 256 ]
            {
                32, 1, 2, 3,156, 9,134,127,151,141,142, 11, 12, 13, 14, 15,
                16, 17, 18, 19,157,133, 8,135, 24, 25,146,143, 28, 29, 30, 31,
                128,129,130,131,132, 10, 23, 27,136,137,138,139,140, 5, 6, 7,
                144,145, 22,147,148,149,150, 4,152,153,154,155, 20, 21,158, 26,
                32,160,161,162,163,164,165,166,167,168, 91, 46, 60, 40, 43, 33,
                38,169,170,171,172,173,174,175,176,177, 93, 36, 42, 41, 59, 94,
                45, 47,178,179,180,181,182,183,184,185,124, 44, 37, 95, 62, 63,
                186,187,188,189,190,191,192,193,194, 96, 58, 35, 64, 39, 61, 34,
                195, 97, 98, 99,100,101,102,103,104,105,196,197,198,199,200,201,
                202,106,107,108,109,110,111,112,113,114,203,204,205,206,207,208,
                209,126,115,116,117,118,119,120,121,122,210,211,212,213,214,215,
                216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,
                123, 65, 66, 67, 68, 69, 70, 71, 72, 73,232,233,234,235,236,237,
                125, 74, 75, 76, 77, 78, 79, 80, 81, 82,238,239,240,241,242,243,
                92,159, 83, 84, 85, 86, 87, 88, 89, 90,244,245,246,247,248,249,
                48, 49, 50, 51, 52, 53, 54, 55, 56, 57,250,251,252,253,254,255
            };

			StringBuilder sb = new StringBuilder();
			for( int i = 0 ; i < strEBCDICString.Length ; ++i )
				sb.Append( Convert.ToChar( e2a[ strEBCDICString[ i ] ] ) );

			return sb.ToString();
		}

		public static string ConvertASCIItoEBCDICHex( string asciiStr )
		{
			byte[] temp = ConvertASCIItoEBCDIC( asciiStr );

			StringBuilder sb = new StringBuilder();
			for( int i = 0 ; i < temp.Length ; ++i )
				sb.Append( temp[ i ].ToString( "X" ) );

			return sb.ToString();
		}

		public static byte[] ConvertASCIItoEBCDIC( string asciiStr )
		{
			// http://shop.alterlinks.com/ascii-table/ascii-ebcdic-us.php
			byte[] a2e = new byte[ 256 ]
            {
                0x00, 0x01, 0x02, 0x03, 0x37, 0x2D, 0x2E, 0x2F, 0x16, 0x05, 0x25, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
				0x10, 0x11, 0x12, 0x13, 0x3C, 0x3D, 0x32, 0x26, 0x18, 0x19, 0x3F, 0x27, 0x1C, 0x1D, 0x1E, 0x1F,
				0x40, 0x4F, 0x7F, 0x7B, 0x5B, 0x6C, 0x50, 0x7D, 0x4D, 0x5D, 0x5C, 0x4E, 0x6B, 0x60, 0x4B, 0x61,
				0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0x7A, 0x5E, 0x4C, 0x7E, 0x6E, 0x6F,
				0x7C, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6,
				0xD7, 0xD8, 0xD9, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0x4A, 0xE0, 0x5A, 0x5F, 0x6D,
				0x79, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96,
				0x97, 0x98, 0x99, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xC0, 0x6A, 0xD0, 0xA1, 0x07,
				0x20, 0x21, 0x22, 0x23, 0x24, 0x15, 0x06, 0x17, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x09, 0x0A, 0x1B,
				0x30, 0x31, 0x1A, 0x33, 0x34, 0x35, 0x36, 0x08, 0x38, 0x39, 0x3A, 0x3B, 0x04, 0x14, 0x3E, 0xE1,
				0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57,
				0x58, 0x59, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75,
				0x76, 0x77, 0x78, 0x80, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x90, 0x9A, 0x9B, 0x9C, 0x9D, 0x9E,
				0x9F, 0xA0, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7,
				0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xBE, 0xBF, 0xCA, 0xCB, 0xCC, 0xCD, 0xCE, 0xCF, 0xDA, 0xDB,
				0xDC, 0xDD, 0xDE, 0xDF, 0xEA, 0xEB, 0xEC, 0xED, 0xEE, 0xEF, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF
            };

			byte[] ret = new byte[ asciiStr.Length ];
			for( int i = 0 ; i < asciiStr.Length ; ++i )
				ret[ i ] = a2e[ asciiStr[ i ] ];

			return ret;
		}

		public static DateTime EpochTimeToLocalTime( double epochTime )
		{
			DateTime d = new DateTime( 1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc );
			return d.AddSeconds( epochTime ).ToLocalTime();
		}
	}
}