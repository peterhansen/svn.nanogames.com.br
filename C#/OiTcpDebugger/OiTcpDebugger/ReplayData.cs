﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace OiTN3270Debugger
{
	class ReplayData
	{
		Int32 currPacket;
		DataTable packetsTable;

		public Int32 NPackets
		{
			get
			{
				return PacketsTable == null ? 0 : PacketsTable.Rows.Count;
			}
		}
		public Int32 CurrPacket
		{
			get
			{
				return currPacket;
			}
			set
			{
				if( value > NPackets )
					currPacket = NPackets;
				else
					currPacket = value;
			}
		}
		public DataTable PacketsTable
		{
			get
			{
				return packetsTable;
			}
			set
			{
				packetsTable = value;
				CurrPacket = 0;
			}
		}
		public ReplayData( DataTable packetsDataTable )
		{
			currPacket = 0;
			packetsTable = packetsDataTable;
		}
	}
}
