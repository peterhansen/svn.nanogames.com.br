﻿using System;
using System.Runtime.InteropServices;

namespace OiTN3270Debugger
{
	static class Win32API
	{
		// WINDOWS: http://msdn.microsoft.com/en-us/library/ff468925%28v=VS.85%29.aspx
		// GDI: http://msdn.microsoft.com/en-us/library/dd145203%28v=VS.85%29.aspx
		// GDI+: http://msdn.microsoft.com/en-us/library/ms533798%28VS.85%29.aspx
		// GDI PRINT API: http://msdn.microsoft.com/en-us/library/ff686799%28v=VS.85%29.aspx

		#region Windows constants

		public const int PW_CLIENTONLY = 0x01;

		//values from Winuser.h in Microsoft SDK.
		/// <summary>
		/// Windows NT/2000/XP: Installs a hook procedure that monitors low-level mouse input events.
		/// </summary>
		public const int WH_MOUSE_LL = 14;
		/// <summary>
		/// Windows NT/2000/XP: Installs a hook procedure that monitors low-level keyboard  input events.
		/// </summary>
		public const int WH_KEYBOARD_LL = 13;

		/// <summary>
		/// Installs a hook procedure that monitors mouse messages. For more information, see the MouseProc hook procedure. 
		/// </summary>
		public const int WH_MOUSE = 7;
		/// <summary>
		/// Installs a hook procedure that monitors keystroke messages. For more information, see the KeyboardProc hook procedure. 
		/// </summary>
		public const int WH_KEYBOARD = 2;

		/// <summary>
		/// The WM_MOUSEMOVE message is posted to a window when the cursor moves. 
		/// </summary>
		public const int WM_MOUSEMOVE = 0x200;
		/// <summary>
		/// The WM_LBUTTONDOWN message is posted when the user presses the left mouse button 
		/// </summary>
		public const int WM_LBUTTONDOWN = 0x201;
		/// <summary>
		/// The WM_RBUTTONDOWN message is posted when the user presses the right mouse button
		/// </summary>
		public const int WM_RBUTTONDOWN = 0x204;
		/// <summary>
		/// The WM_MBUTTONDOWN message is posted when the user presses the middle mouse button 
		/// </summary>
		public const int WM_MBUTTONDOWN = 0x207;
		/// <summary>
		/// The WM_LBUTTONUP message is posted when the user releases the left mouse button 
		/// </summary>
		public const int WM_LBUTTONUP = 0x202;
		/// <summary>
		/// The WM_RBUTTONUP message is posted when the user releases the right mouse button 
		/// </summary>
		public const int WM_RBUTTONUP = 0x205;
		/// <summary>
		/// The WM_MBUTTONUP message is posted when the user releases the middle mouse button 
		/// </summary>
		public const int WM_MBUTTONUP = 0x208;
		/// <summary>
		/// The WM_LBUTTONDBLCLK message is posted when the user double-clicks the left mouse button 
		/// </summary>
		public const int WM_LBUTTONDBLCLK = 0x203;
		/// <summary>
		/// The WM_RBUTTONDBLCLK message is posted when the user double-clicks the right mouse button 
		/// </summary>
		public const int WM_RBUTTONDBLCLK = 0x206;
		/// <summary>
		/// The WM_RBUTTONDOWN message is posted when the user presses the right mouse button 
		/// </summary>
		public const int WM_MBUTTONDBLCLK = 0x209;
		/// <summary>
		/// The WM_MOUSEWHEEL message is posted when the user presses the mouse wheel. 
		/// </summary>
		public const int WM_MOUSEWHEEL = 0x020A;

		/// <summary>
		/// The WM_KEYDOWN message is posted to the window with the keyboard focus when a nonsystem 
		/// key is pressed. A nonsystem key is a key that is pressed when the ALT key is not pressed.
		/// </summary>
		public const int WM_KEYDOWN = 0x100;
		/// <summary>
		/// The WM_KEYUP message is posted to the window with the keyboard focus when a nonsystem 
		/// key is released. A nonsystem key is a key that is pressed when the ALT key is not pressed, 
		/// or a keyboard key that is pressed when a window has the keyboard focus.
		/// </summary>
		public const int WM_KEYUP = 0x101;
		/// <summary>
		/// The WM_SYSKEYDOWN message is posted to the window with the keyboard focus when the user 
		/// presses the F10 key (which activates the menu bar) or holds down the ALT key and then 
		/// presses another key. It also occurs when no window currently has the keyboard focus; 
		/// in this case, the WM_SYSKEYDOWN message is sent to the active window. The window that 
		/// receives the message can distinguish between these two contexts by checking the context 
		/// code in the lParam parameter. 
		/// </summary>
		public const int WM_SYSKEYDOWN = 0x104;
		/// <summary>
		/// The WM_SYSKEYUP message is posted to the window with the keyboard focus when the user 
		/// releases a key that was pressed while the ALT key was held down. It also occurs when no 
		/// window currently has the keyboard focus; in this case, the WM_SYSKEYUP message is sent 
		/// to the active window. The window that receives the message can distinguish between 
		/// these two contexts by checking the context code in the lParam parameter. 
		/// </summary>
		public const int WM_SYSKEYUP = 0x105;

		public const byte VK_SHIFT = 0x10;
		public const byte VK_CAPITAL = 0x14;
		public const byte VK_NUMLOCK = 0x90;

		#endregion

		#region Windows structure definitions

		/// <summary>
		/// The RECT structure defines the coordinates of the upper-left and lower-right corners of a rectangle.
		/// </summary>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/a5ch4fda%28VS.80%29.aspx
		/// </remarks>
		[StructLayout( LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 2)]
		public struct RECT
		{
			/// <summary>
			/// Specifies the x-coordinate of the upper-left corner of a rectangle.
			/// </summary>
			public int left;

			/// <summary>
			/// Specifies the y-coordinate of the upper-left corner of a rectangle.
			/// </summary>
			public int top;

			/// <summary>
			/// Specifies the x-coordinate of the lower-right corner of a rectangle.
			/// </summary>
			public int right;
			
			/// <summary>
			/// Specifies the y-coordinate of the lower-right corner of a rectangle.
			/// </summary>
			public int bottom;
		}

		/// <summary>
		/// The POINT structure defines the x- and y- coordinates of a point. 
		/// </summary>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/gdi/rectangl_0tiq.asp
		/// </remarks>
		[StructLayout( LayoutKind.Sequential )]
		public class POINT
		{
			/// <summary>
			/// Specifies the x-coordinate of the point. 
			/// </summary>
			public int x;
			/// <summary>
			/// Specifies the y-coordinate of the point. 
			/// </summary>
			public int y;
		}

		/// <summary>
		/// The MOUSEHOOKSTRUCT structure contains information about a mouse event passed to a WH_MOUSE hook procedure, MouseProc. 
		/// </summary>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/hooks/hookreference/hookstructures/cwpstruct.asp
		/// </remarks>
		[StructLayout( LayoutKind.Sequential )]
		public class MouseHookStruct
		{
			/// <summary>
			/// Specifies a POINT structure that contains the x- and y-coordinates of the cursor, in screen coordinates. 
			/// </summary>
			public POINT pt;
			/// <summary>
			/// Handle to the window that will receive the mouse message corresponding to the mouse event. 
			/// </summary>
			public int hwnd;
			/// <summary>
			/// Specifies the hit-test value. For a list of hit-test values, see the description of the WM_NCHITTEST message. 
			/// </summary>
			public int wHitTestCode;
			/// <summary>
			/// Specifies extra information associated with the message. 
			/// </summary>
			public int dwExtraInfo;
		}

		/// <summary>
		/// The MSLLHOOKSTRUCT structure contains information about a low-level keyboard input event. 
		/// </summary>
		[StructLayout( LayoutKind.Sequential )]
		public class MouseLLHookStruct
		{
			/// <summary>
			/// Specifies a POINT structure that contains the x- and y-coordinates of the cursor, in screen coordinates. 
			/// </summary>
			public POINT pt;
			/// <summary>
			/// If the message is WM_MOUSEWHEEL, the high-order word of this member is the wheel delta. 
			/// The low-order word is reserved. A positive value indicates that the wheel was rotated forward, 
			/// away from the user; a negative value indicates that the wheel was rotated backward, toward the user. 
			/// One wheel click is defined as WHEEL_DELTA, which is 120. 
			///If the message is WM_XBUTTONDOWN, WM_XBUTTONUP, WM_XBUTTONDBLCLK, WM_NCXBUTTONDOWN, WM_NCXBUTTONUP,
			/// or WM_NCXBUTTONDBLCLK, the high-order word specifies which X button was pressed or released, 
			/// and the low-order word is reserved. This value can be one or more of the following values. Otherwise, mouseData is not used. 
			///XBUTTON1
			///The first X button was pressed or released.
			///XBUTTON2
			///The second X button was pressed or released.
			/// </summary>
			public int mouseData;
			/// <summary>
			/// Specifies the event-injected flag. An application can use the following value to test the mouse flags. Value Purpose 
			///LLMHF_INJECTED Test the event-injected flag.  
			///0
			///Specifies whether the event was injected. The value is 1 if the event was injected; otherwise, it is 0.
			///1-15
			///Reserved.
			/// </summary>
			public int flags;
			/// <summary>
			/// Specifies the time stamp for this message.
			/// </summary>
			public int time;
			/// <summary>
			/// Specifies extra information associated with the message. 
			/// </summary>
			public int dwExtraInfo;
		}

		/// <summary>
		/// The KBDLLHOOKSTRUCT structure contains information about a low-level keyboard input event. 
		/// </summary>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/hooks/hookreference/hookstructures/cwpstruct.asp
		/// </remarks>
		[StructLayout( LayoutKind.Sequential )]
		public class KeyboardHookStruct
		{
			/// <summary>
			/// Specifies a virtual-key code. The code must be a value in the range 1 to 254. 
			/// </summary>
			public int vkCode;
			/// <summary>
			/// Specifies a hardware scan code for the key. 
			/// </summary>
			public int scanCode;
			/// <summary>
			/// Specifies the extended-key flag, event-injected flag, context code, and transition-state flag.
			/// </summary>
			public int flags;
			/// <summary>
			/// Specifies the time stamp for this message.
			/// </summary>
			public int time;
			/// <summary>
			/// Specifies extra information associated with the message. 
			/// </summary>
			public int dwExtraInfo;
		}

		#endregion

		#region Windows function imports

		/// <summary>
		/// The GetDC function retrieves a handle to a device context (DC) for the client area of a specified window
		/// or for the entire screen. You can use the returned handle in subsequent GDI functions to draw in the DC.
		/// The device context is an opaque data structure, whose values are used internally by GDI.
		///
		// The GetDCEx function is an extension to GetDC, which gives an application more control over how and whether
		/// clipping occurs in the client area.
		/// </summary>
		/// <param name="hWnd">
		/// [in] A handle to the window whose DC is to be retrieved. If this value is NULL, GetDC retrieves the DC for the entire screen.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is a handle to the DC for the specified window's client area.
		/// If the function fails, the return value is NULL.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/dd144871%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static IntPtr GetDC( IntPtr hWnd );

		/// <summary>
		/// The ReleaseDC function releases a device context (DC), freeing it for use by other applications. The effect
		/// of the ReleaseDC  function depends on the type of DC. It frees only common and window DCs. It has no effect on class or public DCs.
		/// </summary>
		/// <param name="hWnd">
		/// [in] A handle to the window whose DC is to be released.
		/// </param>
		/// <param name="hDC">
		/// [in] A handle to the DC to be released.
		/// </param>
		/// <returns>
		/// The return value indicates whether the DC was released. If the DC was released, the return value is 1.
		/// If the DC was not released, the return value is zero.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/dd144871%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static int ReleaseDC( IntPtr hWnd, IntPtr hDC );

		/// <summary>
		/// The DeleteDC function deletes the specified device context (DC).
		/// </summary>
		/// <param name="hDC">
		/// [in] A handle to the device context.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/dd183533%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static bool DeleteDC( IntPtr hDC );

		/// <summary>
		/// The CreateDC function creates a device context (DC) for a device using the specified name.
		/// </summary>
		/// <param name="lpszDriver">
		/// A pointer to a null-terminated character string that specifies either DISPLAY or the name of a specific display
		/// device or the name of a print provider, which is usually WINSPOOL.
		/// </param>
		/// <param name="lpszDevice">
		/// [in] A pointer to a null-terminated character string that specifies the name of the specific output device being used, as 
		/// shown by the Print Manager (for example, Epson FX-80). It is not the printer model name. The lpszDevice parameter must
		/// be used.
		/// 
		/// To obtain valid names for displays, call EnumDisplayDevices.
		/// If lpszDriver is DISPLAY or the device name of a specific display device, then lpszDevice must be NULL or that same device 
		/// name. If lpszDevice is NULL, then a DC is created for the primary display device.
		/// 
		/// If there are multiple monitors on the system, calling CreateDC(TEXT("DISPLAY"),NULL,NULL,NULL) will create a DC covering all the monitors.
		/// </param>
		/// <param name="lpszOutput">
		/// This parameter is ignored and should be set to NULL. It is provided only for compatibility with 16-bit Windows.
		/// </param>
		/// <param name="lpInitData">
		/// [in]  A pointer to a DEVMODE structure containing device-specific initialization data for the device driver. The 
		/// DocumentProperties function retrieves this structure filled in for a specified device. The lpInitData  parameter
		/// must be NULL if the device driver is to use the default initialization (if any) specified by the user.
		///
		/// If lpszDriver is DISPLAY, then lpInitData must be NULL. The display device's current DEVMODE is used.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is the handle to a DC for the specified device.
		/// If the function fails, the return value is NULL. The function will return NULL for a DEVMODE structure other than
		/// the current DEVMODE.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/dd183490%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static IntPtr CreateDC( IntPtr lpszDriver, IntPtr lpszDevice, IntPtr lpszOutput );
		// TODOO public extern static IntPtr CreateDC( IntPtr lpszDriver, __in  LPCTSTR lpszDevice, LPCTSTR lpszOutput, __in  const DEVMODE *lpInitDat );

		/// <summary>
		/// The PrintWindow function copies a visual window into the specified device context (DC), typically a printer DC.
		/// 
		/// This is a blocking or synchronous function and might not return immediately. How quickly this function returns
		/// depends on run-time factors such as network status, print server configuration, and printer driver implementation—factors
		/// that are difficult to predict when writing an application. Calling this function from a thread that manages interaction with
		/// the user interface could make the application appear to be unresponsive.
		/// 
		/// This function is similar to WM_PRINT. Before calling PrintWindow, first select a bitmap into hdcBlt.
		/// </summary>
		/// <param name="hWnd">
		/// [in] A handle to the window that will be copied.
		/// </param>
		/// <param name="hdcBlt">
		/// [in] A handle to the device context.
		/// </param>
		/// <param name="nFlags">
		/// [in] The drawing options. It can be one of the following values:
		/// PW_CLIENTONLY: Only the client area of the window is copied to hdcBlt. By default, the entire window is copied.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. To get extended error information, call GetLastError. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/dd162869%28VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static bool PrintWindow( IntPtr hWnd, IntPtr hdcBlt, uint nFlags );

		/// <summary>
		/// Retrieves the dimensions of the bounding rectangle of the specified window. The dimensions are given in
		/// screen coordinates that are relative to the upper-left corner of the screen.
		/// </summary>
		/// <param name="hWnd">
		/// [in] A handle to the window.
		/// </param>
		/// <param name="lpRect">
		/// [out] A pointer to a RECT structure that receives the screen coordinates of the upper-left and lower-right corners of the window.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. To get extended error information, call GetLastError. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/ms633519%28VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static unsafe bool GetWindowRect( IntPtr hWnd, RECT* lpRect );

		/// <summary>
		/// Retrieves the coordinates of a window's client area. The client coordinates specify the upper-left and lower-right
		/// corners of the client area. Because client coordinates are relative to the upper-left corner of a window's client area,
		/// the coordinates of the upper-left corner are (0,0).
		/// </summary>
		/// <param name="hWnd">
		/// [in] A handle to the window whose client coordinates are to be retrieved.
		/// </param>
		/// <param name="lpRect">
		/// [out] A pointer to a RECT structure that receives the client coordinates. The left and top  members are zero. The right
		/// and bottom  members contain the width and height of the window. 
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. To get extended error information, call GetLastError. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/ms633503%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static unsafe bool GetClientRect( IntPtr hWnd, RECT* lpRect );

		/// <summary>
		/// Brings the specified window to the top of the Z order. If the window is a top-level window,
		/// it is activated. If the window is a child window, the top-level parent window associated with
		/// the child window is activated.
		/// </summary>
		/// <param name="hWnd">
		/// [in] A handle to the window to bring to the top of the Z order.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. To get extended error information, call GetLastError. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/ms632673%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "User32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static bool BringWindowToTop( IntPtr hWnd );

		/// <summary>
		/// The GetPixel function retrieves the red, green, blue (RGB) color value of the pixel at
		/// the specified coordinates.
		/// </summary>
		/// <param name="hDC">
		/// [in] A handle to the device context.
		/// </param>
		/// <param name="nXPos">
		/// [in] The x-coordinate, in logical units, of the pixel to be examined.
		/// </param>
		/// <param name="nYPos">
		/// [in] The y-coordinate, in logical units, of the pixel to be examined.
		/// </param>
		/// <returns>
		/// The return value is the RGB value of the pixel. If the pixel is outside of the current clipping
		/// region, the return value is CLR_INVALID.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/en-us/library/dd144909%28v=VS.85%29.aspx
		/// </remarks>
		[DllImport( "Gdi32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static int GetPixel( IntPtr hDC, int nXPos, int nYPos );

		// http://msdn.microsoft.com/en-us/library/dd144877%28v=VS.85%29.aspx
		[DllImport( "Gdi32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public extern static int GetDeviceCaps( IntPtr hDC, int nIndex );

		public const int BLACKNESS = 0x42;
		public const int DSTINVERT = 0x550009;
		public const int MERGECOPY = 0xC000CA;
		public const int MERGEPAINT = 0xBB0226;
		public const int NOTSRCCOPY = 0x330008;
		public const int NOTSRCERASE = 0x1100A6;
		public const int PATCOPY = 0xF00021;
		public const int PATINVERT = 0x5A0049;
		public const int PATPAINT = 0xFB0A09;
		public const int SRCAND = 0x8800C6;
		public const int SRCCOPY = 0xCC0020;
		public const int SRCERASE = 0x440328;
		public const int SRCINVERT = 0x660046;
		public const int SRCPAINT = 0xEE0086;
		public const int WHITENESS = 0xFF0062;
		[DllImport( "Gdi32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public static extern bool BitBlt( IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop );

		/// <summary>
		/// The SetWindowsHookEx function installs an application-defined hook procedure into a hook chain. 
		/// You would install a hook procedure to monitor the system for certain types of events. These events 
		/// are associated either with a specific thread or with all threads in the same desktop as the calling thread. 
		/// </summary>
		/// <param name="idHook">
		/// [in] Specifies the type of hook procedure to be installed. This parameter can be one of the following values.
		/// </param>
		/// <param name="lpfn">
		/// [in] Pointer to the hook procedure. If the dwThreadId parameter is zero or specifies the identifier of a 
		/// thread created by a different process, the lpfn parameter must point to a hook procedure in a dynamic-link 
		/// library (DLL). Otherwise, lpfn can point to a hook procedure in the code associated with the current process.
		/// </param>
		/// <param name="hMod">
		/// [in] Handle to the DLL containing the hook procedure pointed to by the lpfn parameter. 
		/// The hMod parameter must be set to NULL if the dwThreadId parameter specifies a thread created by 
		/// the current process and if the hook procedure is within the code associated with the current process. 
		/// </param>
		/// <param name="dwThreadId">
		/// [in] Specifies the identifier of the thread with which the hook procedure is to be associated. 
		/// If this parameter is zero, the hook procedure is associated with all existing threads running in the 
		/// same desktop as the calling thread. 
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is the handle to the hook procedure.
		/// If the function fails, the return value is NULL. To get extended error information, call GetLastError.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/hooks/hookreference/hookfunctions/setwindowshookex.asp
		/// </remarks>
		[DllImport( "user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public static extern int SetWindowsHookEx( int idHook, HookProc lpfn, IntPtr hMod, int dwThreadId );

		/// <summary>
		/// The UnhookWindowsHookEx function removes a hook procedure installed in a hook chain by the SetWindowsHookEx function. 
		/// </summary>
		/// <param name="idHook">
		/// [in] Handle to the hook to be removed. This parameter is a hook handle obtained by a previous call to SetWindowsHookEx. 
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. To get extended error information, call GetLastError.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/hooks/hookreference/hookfunctions/setwindowshookex.asp
		/// </remarks>
		[DllImport( "user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true )]
		public static extern int UnhookWindowsHookEx( int idHook );

		/// <summary>
		/// The CallNextHookEx function passes the hook information to the next hook procedure in the current hook chain. 
		/// A hook procedure can call this function either before or after processing the hook information. 
		/// </summary>
		/// <param name="idHook">Ignored.</param>
		/// <param name="nCode">
		/// [in] Specifies the hook code passed to the current hook procedure. 
		/// The next hook procedure uses this code to determine how to process the hook information.
		/// </param>
		/// <param name="wParam">
		/// [in] Specifies the wParam value passed to the current hook procedure. 
		/// The meaning of this parameter depends on the type of hook associated with the current hook chain. 
		/// </param>
		/// <param name="lParam">
		/// [in] Specifies the lParam value passed to the current hook procedure. 
		/// The meaning of this parameter depends on the type of hook associated with the current hook chain. 
		/// </param>
		/// <returns>
		/// This value is returned by the next hook procedure in the chain. 
		/// The current hook procedure must also return this value. The meaning of the return value depends on the hook type. 
		/// For more information, see the descriptions of the individual hook procedures.
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/hooks/hookreference/hookfunctions/setwindowshookex.asp
		/// </remarks>
		[DllImport( "user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall )]
		public static extern int CallNextHookEx( int idHook, int nCode, int wParam, IntPtr lParam );

		/// <summary>
		/// The CallWndProc hook procedure is an application-defined or library-defined callback 
		/// function used with the SetWindowsHookEx function. The HOOKPROC type defines a pointer 
		/// to this callback function. CallWndProc is a placeholder for the application-defined 
		/// or library-defined function name.
		/// </summary>
		/// <param name="nCode">
		/// [in] Specifies whether the hook procedure must process the message. 
		/// If nCode is HC_ACTION, the hook procedure must process the message. 
		/// If nCode is less than zero, the hook procedure must pass the message to the 
		/// CallNextHookEx function without further processing and must return the 
		/// value returned by CallNextHookEx.
		/// </param>
		/// <param name="wParam">
		/// [in] Specifies whether the message was sent by the current thread. 
		/// If the message was sent by the current thread, it is nonzero; otherwise, it is zero. 
		/// </param>
		/// <param name="lParam">
		/// [in] Pointer to a CWPSTRUCT structure that contains details about the message. 
		/// </param>
		/// <returns>
		/// If nCode is less than zero, the hook procedure must return the value returned by CallNextHookEx. 
		/// If nCode is greater than or equal to zero, it is highly recommended that you call CallNextHookEx 
		/// and return the value it returns; otherwise, other applications that have installed WH_CALLWNDPROC 
		/// hooks will not receive hook notifications and may behave incorrectly as a result. If the hook 
		/// procedure does not call CallNextHookEx, the return value should be zero. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/hooks/hookreference/hookfunctions/callwndproc.asp
		/// </remarks>
		public delegate int HookProc( int nCode, int wParam, IntPtr lParam );

		/// <summary>
		/// The ToAscii function translates the specified virtual-key code and keyboard 
		/// state to the corresponding character or characters. The function translates the code 
		/// using the input language and physical keyboard layout identified by the keyboard layout handle.
		/// </summary>
		/// <param name="uVirtKey">
		/// [in] Specifies the virtual-key code to be translated. 
		/// </param>
		/// <param name="uScanCode">
		/// [in] Specifies the hardware scan code of the key to be translated. 
		/// The high-order bit of this value is set if the key is up (not pressed). 
		/// </param>
		/// <param name="lpbKeyState">
		/// [in] Pointer to a 256-byte array that contains the current keyboard state. 
		/// Each element (byte) in the array contains the state of one key. 
		/// If the high-order bit of a byte is set, the key is down (pressed). 
		/// The low bit, if set, indicates that the key is toggled on. In this function, 
		/// only the toggle bit of the CAPS LOCK key is relevant. The toggle state 
		/// of the NUM LOCK and SCROLL LOCK keys is ignored.
		/// </param>
		/// <param name="lpwTransKey">
		/// [out] Pointer to the buffer that receives the translated character or characters. 
		/// </param>
		/// <param name="fuState">
		/// [in] Specifies whether a menu is active. This parameter must be 1 if a menu is active, or 0 otherwise. 
		/// </param>
		/// <returns>
		/// If the specified key is a dead key, the return value is negative. Otherwise, it is one of the following values. 
		/// Value Meaning 
		/// 0 The specified virtual key has no translation for the current state of the keyboard. 
		/// 1 One character was copied to the buffer. 
		/// 2 Two characters were copied to the buffer. This usually happens when a dead-key character 
		/// (accent or diacritic) stored in the keyboard layout cannot be composed with the specified 
		/// virtual key to form a single character. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/keyboardinput/keyboardinputreference/keyboardinputfunctions/toascii.asp
		/// </remarks>
		[DllImport( "user32" )]
		public static extern int ToAscii( int uVirtKey, int uScanCode, byte[] lpbKeyState, byte[] lpwTransKey, int fuState );

		/// <summary>
		/// The GetKeyboardState function copies the status of the 256 virtual keys to the 
		/// specified buffer. 
		/// </summary>
		/// <param name="pbKeyState">
		/// [in] Pointer to a 256-byte array that contains keyboard key states. 
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. To get extended error information, call GetLastError. 
		/// </returns>
		/// <remarks>
		/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/keyboardinput/keyboardinputreference/keyboardinputfunctions/toascii.asp
		/// </remarks>
		[DllImport( "user32" )]
		public static extern int GetKeyboardState( byte[] pbKeyState );

		[DllImport( "user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall )]
		public static extern short GetKeyState( int vKey );

		#endregion
	}
}
