﻿namespace OiTN3270Debugger
{
    partial class FrmApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( FrmApp ) );
			this.tipAppHelper = new System.Windows.Forms.ToolTip( this.components );
			this.tbPacketString = new System.Windows.Forms.TextBox();
			this.ckbPacketString = new System.Windows.Forms.CheckBox();
			this.ckbPacketDate = new System.Windows.Forms.CheckBox();
			this.dtpBeginDate = new System.Windows.Forms.DateTimePicker();
			this.nudClientPort = new System.Windows.Forms.NumericUpDown();
			this.ckbClientPort = new System.Windows.Forms.CheckBox();
			this.ckbTerminalName = new System.Windows.Forms.CheckBox();
			this.ckbTerminalType = new System.Windows.Forms.CheckBox();
			this.ckbSessionHost = new System.Windows.Forms.CheckBox();
			this.ckbSessionClient = new System.Windows.Forms.CheckBox();
			this.tbTerminalName = new System.Windows.Forms.TextBox();
			this.tbTerminalType = new System.Windows.Forms.TextBox();
			this.btExportToAVI = new System.Windows.Forms.Button();
			this.btReplay = new System.Windows.Forms.Button();
			this.btSearch = new System.Windows.Forms.Button();
			this.rtbActions = new System.Windows.Forms.RichTextBox();
			this.btPauseReplay = new System.Windows.Forms.Button();
			this.btStopReplay = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbDatabases = new System.Windows.Forms.ComboBox();
			this.tbIpClient = new System.Windows.Forms.TextBox();
			this.tbIpHost = new System.Windows.Forms.TextBox();
			this.picbArrow = new System.Windows.Forms.PictureBox();
			this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
			this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
			this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
			this.nudReplayDelay = new System.Windows.Forms.NumericUpDown();
			this.cbPacketsStrMatch = new System.Windows.Forms.ComboBox();
			this.mnMainMenu = new System.Windows.Forms.MenuStrip();
			this.arquivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.configurarConexãoComOBDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ferramentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tcTabsController = new System.Windows.Forms.TabControl();
			this.tabSearch = new System.Windows.Forms.TabPage();
			this.gbPacketFilter = new System.Windows.Forms.GroupBox();
			this.ckbPacketDirection = new System.Windows.Forms.CheckBox();
			this.gbPacketDirection = new System.Windows.Forms.GroupBox();
			this.rbPacketDirBoth = new System.Windows.Forms.RadioButton();
			this.rbPacketDirHTC = new System.Windows.Forms.RadioButton();
			this.rbPacketDirCTH = new System.Windows.Forms.RadioButton();
			this.gbPacketDate = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lbDate = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.gbResultsPanel = new System.Windows.Forms.GroupBox();
			this.lbNResults = new System.Windows.Forms.Label();
			this.dgResults = new System.Windows.Forms.DataGridView();
			this.tabReplay = new System.Windows.Forms.TabPage();
			this.gbReplayPanel = new System.Windows.Forms.GroupBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lbReplayProgress = new System.Windows.Forms.Label();
			this.progbReplay = new System.Windows.Forms.ProgressBar();
			this.tabExport = new System.Windows.Forms.TabPage();
			this.lbExportProgress = new System.Windows.Forms.Label();
			this.progbExport = new System.Windows.Forms.ProgressBar();
			this.tbSufixoBaseHexa = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			( ( System.ComponentModel.ISupportInitialize )( this.nudClientPort ) ).BeginInit();
			this.groupBox1.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.picbArrow ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.nudReplayDelay ) ).BeginInit();
			this.mnMainMenu.SuspendLayout();
			this.tcTabsController.SuspendLayout();
			this.tabSearch.SuspendLayout();
			this.gbPacketFilter.SuspendLayout();
			this.gbPacketDirection.SuspendLayout();
			this.gbPacketDate.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.gbResultsPanel.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.dgResults ) ).BeginInit();
			this.tabReplay.SuspendLayout();
			this.gbReplayPanel.SuspendLayout();
			this.tabExport.SuspendLayout();
			this.SuspendLayout();
			// 
			// tipAppHelper
			// 
			this.tipAppHelper.ToolTipTitle = "Oi TN3270 Debugger";
			// 
			// tbPacketString
			// 
			this.tbPacketString.Enabled = false;
			this.tbPacketString.Location = new System.Drawing.Point( 355, 157 );
			this.tbPacketString.Name = "tbPacketString";
			this.tbPacketString.Size = new System.Drawing.Size( 159, 20 );
			this.tbPacketString.TabIndex = 5;
			this.tipAppHelper.SetToolTip( this.tbPacketString, resources.GetString( "tbPacketString.ToolTip" ) );
			// 
			// ckbPacketString
			// 
			this.ckbPacketString.AutoSize = true;
			this.ckbPacketString.Location = new System.Drawing.Point( 353, 134 );
			this.ckbPacketString.Name = "ckbPacketString";
			this.ckbPacketString.Size = new System.Drawing.Size( 331, 17 );
			this.ckbPacketString.TabIndex = 4;
			this.ckbPacketString.Text = "Filtrar por String ( Sequência de Caracteres na Tela do Terminal )";
			this.tipAppHelper.SetToolTip( this.ckbPacketString, "Habilita ou desabilita o filtro de pacote String (sequência de caracteres)" );
			this.ckbPacketString.UseVisualStyleBackColor = true;
			this.ckbPacketString.CheckedChanged += new System.EventHandler( this.OnPacketFilterChanged );
			// 
			// ckbPacketDate
			// 
			this.ckbPacketDate.AutoSize = true;
			this.ckbPacketDate.Location = new System.Drawing.Point( 6, 27 );
			this.ckbPacketDate.Name = "ckbPacketDate";
			this.ckbPacketDate.Size = new System.Drawing.Size( 154, 17 );
			this.ckbPacketDate.TabIndex = 0;
			this.ckbPacketDate.Text = "Filtrar Data de Transmissão";
			this.tipAppHelper.SetToolTip( this.ckbPacketDate, "Habilita ou desabilita o filtro de pacote Data de Transmissão" );
			this.ckbPacketDate.UseVisualStyleBackColor = true;
			this.ckbPacketDate.CheckedChanged += new System.EventHandler( this.OnPacketFilterChanged );
			// 
			// dtpBeginDate
			// 
			this.dtpBeginDate.CustomFormat = "dd/MM/yyyy";
			this.dtpBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpBeginDate.Location = new System.Drawing.Point( 9, 35 );
			this.dtpBeginDate.Name = "dtpBeginDate";
			this.dtpBeginDate.Size = new System.Drawing.Size( 149, 20 );
			this.dtpBeginDate.TabIndex = 0;
			this.tipAppHelper.SetToolTip( this.dtpBeginDate, resources.GetString( "dtpBeginDate.ToolTip" ) );
			// 
			// nudClientPort
			// 
			this.nudClientPort.Enabled = false;
			this.nudClientPort.Location = new System.Drawing.Point( 180, 46 );
			this.nudClientPort.Maximum = new decimal( new int[] {
            65535,
            0,
            0,
            0} );
			this.nudClientPort.Name = "nudClientPort";
			this.nudClientPort.Size = new System.Drawing.Size( 89, 20 );
			this.nudClientPort.TabIndex = 3;
			this.tipAppHelper.SetToolTip( this.nudClientPort, "\r\nFiltro de Sessão - Porta do Cliente\r\n\r\nQuando este campo é utilizado, somente s" +
					"erão retornadas conversas cuja porta da conexão do cliente seja igual ao valor e" +
					"specificado." );
			// 
			// ckbClientPort
			// 
			this.ckbClientPort.AutoSize = true;
			this.ckbClientPort.Location = new System.Drawing.Point( 180, 26 );
			this.ckbClientPort.Name = "ckbClientPort";
			this.ckbClientPort.Size = new System.Drawing.Size( 129, 17 );
			this.ckbClientPort.TabIndex = 2;
			this.ckbClientPort.Text = "Filtrar Porta do Cliente";
			this.tipAppHelper.SetToolTip( this.ckbClientPort, "Habilita ou desabilita o filtro de sessão Porta do Cliente" );
			this.ckbClientPort.UseVisualStyleBackColor = true;
			this.ckbClientPort.CheckedChanged += new System.EventHandler( this.OnSessionFilterChanged );
			// 
			// ckbTerminalName
			// 
			this.ckbTerminalName.AutoSize = true;
			this.ckbTerminalName.Location = new System.Drawing.Point( 202, 198 );
			this.ckbTerminalName.Name = "ckbTerminalName";
			this.ckbTerminalName.Size = new System.Drawing.Size( 140, 17 );
			this.ckbTerminalName.TabIndex = 10;
			this.ckbTerminalName.Text = "Filtrar Nome do Terminal";
			this.tipAppHelper.SetToolTip( this.ckbTerminalName, "Habilita ou desabilita o filtro de sessão Nome do Terminal" );
			this.ckbTerminalName.UseVisualStyleBackColor = true;
			this.ckbTerminalName.CheckedChanged += new System.EventHandler( this.OnSessionFilterChanged );
			// 
			// ckbTerminalType
			// 
			this.ckbTerminalType.AutoSize = true;
			this.ckbTerminalType.Location = new System.Drawing.Point( 6, 198 );
			this.ckbTerminalType.Name = "ckbTerminalType";
			this.ckbTerminalType.Size = new System.Drawing.Size( 133, 17 );
			this.ckbTerminalType.TabIndex = 8;
			this.ckbTerminalType.Text = "Filtrar Tipo do Terminal";
			this.tipAppHelper.SetToolTip( this.ckbTerminalType, "Habilita ou desabilita o filtro de sessão Tipo do Terminal" );
			this.ckbTerminalType.UseVisualStyleBackColor = true;
			this.ckbTerminalType.CheckedChanged += new System.EventHandler( this.OnSessionFilterChanged );
			// 
			// ckbSessionHost
			// 
			this.ckbSessionHost.AutoSize = true;
			this.ckbSessionHost.Location = new System.Drawing.Point( 9, 96 );
			this.ckbSessionHost.Name = "ckbSessionHost";
			this.ckbSessionHost.Size = new System.Drawing.Size( 121, 17 );
			this.ckbSessionHost.TabIndex = 4;
			this.ckbSessionHost.Text = "Filtrar IP do Servidor";
			this.tipAppHelper.SetToolTip( this.ckbSessionHost, "Habilita ou desabilita o filtro de sessão IP do Servidor" );
			this.ckbSessionHost.UseVisualStyleBackColor = true;
			this.ckbSessionHost.CheckedChanged += new System.EventHandler( this.OnSessionFilterChanged );
			// 
			// ckbSessionClient
			// 
			this.ckbSessionClient.AutoSize = true;
			this.ckbSessionClient.Location = new System.Drawing.Point( 9, 26 );
			this.ckbSessionClient.Name = "ckbSessionClient";
			this.ckbSessionClient.Size = new System.Drawing.Size( 114, 17 );
			this.ckbSessionClient.TabIndex = 0;
			this.ckbSessionClient.Text = "Filtrar IP do Cliente";
			this.tipAppHelper.SetToolTip( this.ckbSessionClient, "Habilita ou desabilita o filtro de sessão IP do Cliente" );
			this.ckbSessionClient.UseVisualStyleBackColor = true;
			this.ckbSessionClient.CheckedChanged += new System.EventHandler( this.OnSessionFilterChanged );
			// 
			// tbTerminalName
			// 
			this.tbTerminalName.Enabled = false;
			this.tbTerminalName.Location = new System.Drawing.Point( 202, 218 );
			this.tbTerminalName.Name = "tbTerminalName";
			this.tbTerminalName.Size = new System.Drawing.Size( 178, 20 );
			this.tbTerminalName.TabIndex = 11;
			this.tipAppHelper.SetToolTip( this.tbTerminalName, "\r\nFiltro de Sessão - Nome do Terminal\r\n\r\nQuando este campo é utilizado, somente s" +
					"erão retornadas conversas cujo terminal possua nome especificado." );
			// 
			// tbTerminalType
			// 
			this.tbTerminalType.Enabled = false;
			this.tbTerminalType.Location = new System.Drawing.Point( 6, 218 );
			this.tbTerminalType.Name = "tbTerminalType";
			this.tbTerminalType.Size = new System.Drawing.Size( 178, 20 );
			this.tbTerminalType.TabIndex = 9;
			this.tipAppHelper.SetToolTip( this.tbTerminalType, "\r\nFiltro de Sessão - Tipo do Terminal\r\n\r\nQuando este campo é utilizado, somente s" +
					"erão retornadas conversas cujo terminal seja do tipo especificado." );
			// 
			// btExportToAVI
			// 
			this.btExportToAVI.Location = new System.Drawing.Point( 893, 310 );
			this.btExportToAVI.Name = "btExportToAVI";
			this.btExportToAVI.Size = new System.Drawing.Size( 142, 29 );
			this.btExportToAVI.TabIndex = 1;
			this.btExportToAVI.Text = "Ex&portar para AVI";
			this.tipAppHelper.SetToolTip( this.btExportToAVI, "Exporta a conversa selecionada para vídeo no formato AVI" );
			this.btExportToAVI.UseVisualStyleBackColor = true;
			this.btExportToAVI.Click += new System.EventHandler( this.OnExportToAVI );
			// 
			// btReplay
			// 
			this.btReplay.Location = new System.Drawing.Point( 1041, 310 );
			this.btReplay.Name = "btReplay";
			this.btReplay.Size = new System.Drawing.Size( 96, 29 );
			this.btReplay.TabIndex = 2;
			this.btReplay.Text = "&Replay";
			this.tipAppHelper.SetToolTip( this.btReplay, "Inicia o replay da conversa selecionada na tabela" );
			this.btReplay.UseVisualStyleBackColor = true;
			this.btReplay.Click += new System.EventHandler( this.OnReplaySession );
			// 
			// btSearch
			// 
			this.btSearch.Location = new System.Drawing.Point( 487, 280 );
			this.btSearch.Name = "btSearch";
			this.btSearch.Size = new System.Drawing.Size( 187, 29 );
			this.btSearch.TabIndex = 4;
			this.btSearch.Text = "&Buscar Resultados";
			this.tipAppHelper.SetToolTip( this.btSearch, "Busca, no banco de dados,  por conversas que atendem aos filtros especificados" );
			this.btSearch.UseVisualStyleBackColor = true;
			this.btSearch.Click += new System.EventHandler( this.OnSearchResults );
			// 
			// rtbActions
			// 
			this.rtbActions.Location = new System.Drawing.Point( 9, 166 );
			this.rtbActions.Name = "rtbActions";
			this.rtbActions.ReadOnly = true;
			this.rtbActions.Size = new System.Drawing.Size( 1128, 203 );
			this.rtbActions.TabIndex = 3;
			this.rtbActions.TabStop = false;
			this.rtbActions.Text = "";
			this.tipAppHelper.SetToolTip( this.rtbActions, "Exibe as ações realizadas pelo usuário que realizou a conversa" );
			this.rtbActions.WordWrap = false;
			// 
			// btPauseReplay
			// 
			this.btPauseReplay.Location = new System.Drawing.Point( 981, 376 );
			this.btPauseReplay.Name = "btPauseReplay";
			this.btPauseReplay.Size = new System.Drawing.Size( 75, 29 );
			this.btPauseReplay.TabIndex = 4;
			this.btPauseReplay.Text = "&Pause";
			this.tipAppHelper.SetToolTip( this.btPauseReplay, "Pausa o replay que está sendo exibido ou reinicia a exibição caso o replay já est" +
					"eja pausado" );
			this.btPauseReplay.UseVisualStyleBackColor = true;
			this.btPauseReplay.Click += new System.EventHandler( this.OnReplayPause );
			// 
			// btStopReplay
			// 
			this.btStopReplay.Location = new System.Drawing.Point( 1062, 376 );
			this.btStopReplay.Name = "btStopReplay";
			this.btStopReplay.Size = new System.Drawing.Size( 75, 29 );
			this.btStopReplay.TabIndex = 5;
			this.btStopReplay.Text = "&Stop";
			this.tipAppHelper.SetToolTip( this.btStopReplay, "Pára a reprodução do replay atual" );
			this.btStopReplay.UseVisualStyleBackColor = true;
			this.btStopReplay.Click += new System.EventHandler( this.OnReplayStop );
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add( this.label7 );
			this.groupBox1.Controls.Add( this.tbSufixoBaseHexa );
			this.groupBox1.Controls.Add( this.cbDatabases );
			this.groupBox1.Location = new System.Drawing.Point( 8, 4 );
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size( 404, 96 );
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Base de Dados";
			this.tipAppHelper.SetToolTip( this.groupBox1, "Selecione a base dados sobre a qual as buscas serão executadas" );
			// 
			// cbDatabases
			// 
			this.cbDatabases.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbDatabases.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbDatabases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDatabases.Items.AddRange( new object[] {
            "<Não há Bases de Dados>"} );
			this.cbDatabases.Location = new System.Drawing.Point( 9, 24 );
			this.cbDatabases.Name = "cbDatabases";
			this.cbDatabases.Size = new System.Drawing.Size( 333, 21 );
			this.cbDatabases.Sorted = true;
			this.cbDatabases.TabIndex = 0;
			this.tipAppHelper.SetToolTip( this.cbDatabases, "Selecione a base de dados sobre a qual as buscas deverão ser realizadas" );
			this.cbDatabases.SelectedIndexChanged += new System.EventHandler( this.OnDatabaseSelectedChanged );
			// 
			// tbIpClient
			// 
			this.tbIpClient.Enabled = false;
			this.tbIpClient.Location = new System.Drawing.Point( 9, 46 );
			this.tbIpClient.MaxLength = 15;
			this.tbIpClient.Name = "tbIpClient";
			this.tbIpClient.Size = new System.Drawing.Size( 159, 20 );
			this.tbIpClient.TabIndex = 1;
			this.tipAppHelper.SetToolTip( this.tbIpClient, "\r\nFiltro de Sessão - IP do Cliente\r\n\r\nQuando este campo é utilizado, somente serã" +
					"o retornadas conversas cujo endereço IP do cliente seja igual ao valor especific" +
					"ado." );
			this.tbIpClient.KeyDown += new System.Windows.Forms.KeyEventHandler( this.OnIPTextBoxKeyDown );
			// 
			// tbIpHost
			// 
			this.tbIpHost.Enabled = false;
			this.tbIpHost.Location = new System.Drawing.Point( 9, 116 );
			this.tbIpHost.MaxLength = 15;
			this.tbIpHost.Name = "tbIpHost";
			this.tbIpHost.Size = new System.Drawing.Size( 159, 20 );
			this.tbIpHost.TabIndex = 5;
			this.tipAppHelper.SetToolTip( this.tbIpHost, "\r\nFiltro de Sessão - IP do Servidor\r\n\r\nQuando este campo é utilizado, somente ser" +
					"ão retornadas conversas cujo endereço IP do servidor seja igual ao valor especif" +
					"icado." );
			this.tbIpHost.KeyDown += new System.Windows.Forms.KeyEventHandler( this.OnIPTextBoxKeyDown );
			// 
			// picbArrow
			// 
			this.picbArrow.Cursor = System.Windows.Forms.Cursors.Hand;
			this.picbArrow.Image = global::OiTN3270Debugger.Properties.Resources.UpArrow;
			this.picbArrow.InitialImage = global::OiTN3270Debugger.Properties.Resources.UpArrow;
			this.picbArrow.Location = new System.Drawing.Point( 2, 126 );
			this.picbArrow.Name = "picbArrow";
			this.picbArrow.Size = new System.Drawing.Size( 16, 10 );
			this.picbArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.picbArrow.TabIndex = 6;
			this.picbArrow.TabStop = false;
			this.tipAppHelper.SetToolTip( this.picbArrow, "Clique e arraste a seta para avançar / retroceder o replay" );
			this.picbArrow.WaitOnLoad = true;
			this.picbArrow.MouseDown += new System.Windows.Forms.MouseEventHandler( this.onReplayArrowStartDrag );
			this.picbArrow.MouseUp += new System.Windows.Forms.MouseEventHandler( this.onReaplayArrowDrop );
			// 
			// dtpEndTime
			// 
			this.dtpEndTime.CustomFormat = "HH:mm:ss";
			this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpEndTime.Location = new System.Drawing.Point( 519, 35 );
			this.dtpEndTime.Name = "dtpEndTime";
			this.dtpEndTime.ShowUpDown = true;
			this.dtpEndTime.Size = new System.Drawing.Size( 149, 20 );
			this.dtpEndTime.TabIndex = 3;
			this.tipAppHelper.SetToolTip( this.dtpEndTime, resources.GetString( "dtpEndTime.ToolTip" ) );
			// 
			// dtpBeginTime
			// 
			this.dtpBeginTime.CustomFormat = "HH:mm:ss";
			this.dtpBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpBeginTime.Location = new System.Drawing.Point( 179, 35 );
			this.dtpBeginTime.Name = "dtpBeginTime";
			this.dtpBeginTime.ShowUpDown = true;
			this.dtpBeginTime.Size = new System.Drawing.Size( 149, 20 );
			this.dtpBeginTime.TabIndex = 1;
			this.tipAppHelper.SetToolTip( this.dtpBeginTime, resources.GetString( "dtpBeginTime.ToolTip" ) );
			// 
			// dtpEndDate
			// 
			this.dtpEndDate.CustomFormat = "dd/MM/yyyy";
			this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpEndDate.Location = new System.Drawing.Point( 349, 35 );
			this.dtpEndDate.Name = "dtpEndDate";
			this.dtpEndDate.Size = new System.Drawing.Size( 149, 20 );
			this.dtpEndDate.TabIndex = 2;
			this.tipAppHelper.SetToolTip( this.dtpEndDate, resources.GetString( "dtpEndDate.ToolTip" ) );
			// 
			// nudReplayDelay
			// 
			this.nudReplayDelay.Increment = new decimal( new int[] {
            5,
            0,
            0,
            0} );
			this.nudReplayDelay.Location = new System.Drawing.Point( 9, 48 );
			this.nudReplayDelay.Maximum = new decimal( new int[] {
            2000,
            0,
            0,
            0} );
			this.nudReplayDelay.Minimum = new decimal( new int[] {
            50,
            0,
            0,
            0} );
			this.nudReplayDelay.Name = "nudReplayDelay";
			this.nudReplayDelay.Size = new System.Drawing.Size( 89, 20 );
			this.nudReplayDelay.TabIndex = 0;
			this.tipAppHelper.SetToolTip( this.nudReplayDelay, "Determina o tempo de espera adicionado entre cada frame (tela) exibido no replay." +
					" Os valores permitidos vão de 50 a 2000 milissegundos." );
			this.nudReplayDelay.Value = new decimal( new int[] {
            1000,
            0,
            0,
            0} );
			this.nudReplayDelay.ValueChanged += new System.EventHandler( this.OnReplayDelayValueChanged );
			// 
			// cbPacketsStrMatch
			// 
			this.cbPacketsStrMatch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbPacketsStrMatch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbPacketsStrMatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbPacketsStrMatch.Enabled = false;
			this.cbPacketsStrMatch.Items.AddRange( new object[] {
            "<Não há Bases de Dados>"} );
			this.cbPacketsStrMatch.Location = new System.Drawing.Point( 212, 47 );
			this.cbPacketsStrMatch.Name = "cbPacketsStrMatch";
			this.cbPacketsStrMatch.Size = new System.Drawing.Size( 170, 21 );
			this.cbPacketsStrMatch.Sorted = true;
			this.cbPacketsStrMatch.TabIndex = 1;
			this.tipAppHelper.SetToolTip( this.cbPacketsStrMatch, "Selecione a base de dados sobre a qual as buscas deverão ser realizadas" );
			this.cbPacketsStrMatch.SelectedIndexChanged += new System.EventHandler( this.OnSkipToFrame );
			// 
			// mnMainMenu
			// 
			this.mnMainMenu.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.arquivoToolStripMenuItem,
            this.editarToolStripMenuItem,
            this.ferramentasToolStripMenuItem} );
			this.mnMainMenu.Location = new System.Drawing.Point( 0, 0 );
			this.mnMainMenu.Name = "mnMainMenu";
			this.mnMainMenu.Size = new System.Drawing.Size( 1168, 24 );
			this.mnMainMenu.TabIndex = 6;
			this.mnMainMenu.Text = "menuStrip1";
			// 
			// arquivoToolStripMenuItem
			// 
			this.arquivoToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.sairToolStripMenuItem} );
			this.arquivoToolStripMenuItem.Name = "arquivoToolStripMenuItem";
			this.arquivoToolStripMenuItem.Size = new System.Drawing.Size( 56, 20 );
			this.arquivoToolStripMenuItem.Text = "&Arquivo";
			// 
			// sairToolStripMenuItem
			// 
			this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
			this.sairToolStripMenuItem.Size = new System.Drawing.Size( 103, 22 );
			this.sairToolStripMenuItem.Text = "Sair";
			this.sairToolStripMenuItem.Click += new System.EventHandler( this.OnMenuOptExit );
			// 
			// editarToolStripMenuItem
			// 
			this.editarToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.configurarConexãoComOBDToolStripMenuItem} );
			this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
			this.editarToolStripMenuItem.Size = new System.Drawing.Size( 47, 20 );
			this.editarToolStripMenuItem.Text = "&Editar";
			// 
			// configurarConexãoComOBDToolStripMenuItem
			// 
			this.configurarConexãoComOBDToolStripMenuItem.Name = "configurarConexãoComOBDToolStripMenuItem";
			this.configurarConexãoComOBDToolStripMenuItem.Size = new System.Drawing.Size( 229, 22 );
			this.configurarConexãoComOBDToolStripMenuItem.Text = "Configurar Conexão com o BD";
			this.configurarConexãoComOBDToolStripMenuItem.Click += new System.EventHandler( this.OnMenuOptConfigBDConn );
			// 
			// ferramentasToolStripMenuItem
			// 
			this.ferramentasToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.configuraçõesToolStripMenuItem} );
			this.ferramentasToolStripMenuItem.Name = "ferramentasToolStripMenuItem";
			this.ferramentasToolStripMenuItem.Size = new System.Drawing.Size( 80, 20 );
			this.ferramentasToolStripMenuItem.Text = "&Ferramentas";
			// 
			// configuraçõesToolStripMenuItem
			// 
			this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
			this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size( 291, 22 );
			this.configuraçõesToolStripMenuItem.Text = "Alterar Configurações Padrão do Programa";
			this.configuraçõesToolStripMenuItem.Click += new System.EventHandler( this.OnMenuOptConfigDefaultValues );
			// 
			// tcTabsController
			// 
			this.tcTabsController.Controls.Add( this.tabSearch );
			this.tcTabsController.Controls.Add( this.tabReplay );
			this.tcTabsController.Controls.Add( this.tabExport );
			this.tcTabsController.Location = new System.Drawing.Point( 0, 27 );
			this.tcTabsController.Name = "tcTabsController";
			this.tcTabsController.SelectedIndex = 0;
			this.tcTabsController.Size = new System.Drawing.Size( 1174, 689 );
			this.tcTabsController.TabIndex = 7;
			// 
			// tabSearch
			// 
			this.tabSearch.Controls.Add( this.groupBox1 );
			this.tabSearch.Controls.Add( this.btSearch );
			this.tabSearch.Controls.Add( this.gbPacketFilter );
			this.tabSearch.Controls.Add( this.groupBox3 );
			this.tabSearch.Controls.Add( this.gbResultsPanel );
			this.tabSearch.Location = new System.Drawing.Point( 4, 22 );
			this.tabSearch.Name = "tabSearch";
			this.tabSearch.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabSearch.Size = new System.Drawing.Size( 1166, 663 );
			this.tabSearch.TabIndex = 0;
			this.tabSearch.Text = "Busca";
			this.tabSearch.UseVisualStyleBackColor = true;
			// 
			// gbPacketFilter
			// 
			this.gbPacketFilter.Controls.Add( this.tbPacketString );
			this.gbPacketFilter.Controls.Add( this.ckbPacketString );
			this.gbPacketFilter.Controls.Add( this.ckbPacketDirection );
			this.gbPacketFilter.Controls.Add( this.ckbPacketDate );
			this.gbPacketFilter.Controls.Add( this.ckbTerminalName );
			this.gbPacketFilter.Controls.Add( this.gbPacketDirection );
			this.gbPacketFilter.Controls.Add( this.gbPacketDate );
			this.gbPacketFilter.Controls.Add( this.ckbTerminalType );
			this.gbPacketFilter.Controls.Add( this.tbTerminalName );
			this.gbPacketFilter.Controls.Add( this.tbTerminalType );
			this.gbPacketFilter.Location = new System.Drawing.Point( 418, 4 );
			this.gbPacketFilter.Name = "gbPacketFilter";
			this.gbPacketFilter.Size = new System.Drawing.Size( 734, 268 );
			this.gbPacketFilter.TabIndex = 2;
			this.gbPacketFilter.TabStop = false;
			this.gbPacketFilter.Text = "Filtros de Pacotes";
			// 
			// ckbPacketDirection
			// 
			this.ckbPacketDirection.AutoSize = true;
			this.ckbPacketDirection.Location = new System.Drawing.Point( 6, 115 );
			this.ckbPacketDirection.Name = "ckbPacketDirection";
			this.ckbPacketDirection.Size = new System.Drawing.Size( 168, 17 );
			this.ckbPacketDirection.TabIndex = 2;
			this.ckbPacketDirection.Text = "Filtrar Direção de Transmissão";
			this.ckbPacketDirection.UseVisualStyleBackColor = true;
			this.ckbPacketDirection.CheckedChanged += new System.EventHandler( this.OnPacketFilterChanged );
			// 
			// gbPacketDirection
			// 
			this.gbPacketDirection.Controls.Add( this.rbPacketDirBoth );
			this.gbPacketDirection.Controls.Add( this.rbPacketDirHTC );
			this.gbPacketDirection.Controls.Add( this.rbPacketDirCTH );
			this.gbPacketDirection.Enabled = false;
			this.gbPacketDirection.Location = new System.Drawing.Point( 6, 129 );
			this.gbPacketDirection.Name = "gbPacketDirection";
			this.gbPacketDirection.Size = new System.Drawing.Size( 326, 48 );
			this.gbPacketDirection.TabIndex = 3;
			this.gbPacketDirection.TabStop = false;
			// 
			// rbPacketDirBoth
			// 
			this.rbPacketDirBoth.AutoSize = true;
			this.rbPacketDirBoth.Checked = true;
			this.rbPacketDirBoth.Location = new System.Drawing.Point( 6, 17 );
			this.rbPacketDirBoth.Name = "rbPacketDirBoth";
			this.rbPacketDirBoth.Size = new System.Drawing.Size( 57, 17 );
			this.rbPacketDirBoth.TabIndex = 0;
			this.rbPacketDirBoth.TabStop = true;
			this.rbPacketDirBoth.Text = "Ambas";
			this.rbPacketDirBoth.UseVisualStyleBackColor = true;
			// 
			// rbPacketDirHTC
			// 
			this.rbPacketDirHTC.AutoSize = true;
			this.rbPacketDirHTC.Location = new System.Drawing.Point( 194, 17 );
			this.rbPacketDirHTC.Name = "rbPacketDirHTC";
			this.rbPacketDirHTC.Size = new System.Drawing.Size( 123, 17 );
			this.rbPacketDirHTC.TabIndex = 2;
			this.rbPacketDirHTC.Text = "Servidor para Cliente";
			this.rbPacketDirHTC.UseVisualStyleBackColor = true;
			// 
			// rbPacketDirCTH
			// 
			this.rbPacketDirCTH.AutoSize = true;
			this.rbPacketDirCTH.Location = new System.Drawing.Point( 67, 17 );
			this.rbPacketDirCTH.Name = "rbPacketDirCTH";
			this.rbPacketDirCTH.Size = new System.Drawing.Size( 123, 17 );
			this.rbPacketDirCTH.TabIndex = 1;
			this.rbPacketDirCTH.Text = "Cliente para Servidor";
			this.rbPacketDirCTH.UseVisualStyleBackColor = true;
			// 
			// gbPacketDate
			// 
			this.gbPacketDate.Controls.Add( this.label2 );
			this.gbPacketDate.Controls.Add( this.dtpEndDate );
			this.gbPacketDate.Controls.Add( this.label4 );
			this.gbPacketDate.Controls.Add( this.dtpEndTime );
			this.gbPacketDate.Controls.Add( this.label3 );
			this.gbPacketDate.Controls.Add( this.dtpBeginTime );
			this.gbPacketDate.Controls.Add( this.lbDate );
			this.gbPacketDate.Controls.Add( this.dtpBeginDate );
			this.gbPacketDate.Enabled = false;
			this.gbPacketDate.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
			this.gbPacketDate.Location = new System.Drawing.Point( 6, 41 );
			this.gbPacketDate.Name = "gbPacketDate";
			this.gbPacketDate.Size = new System.Drawing.Size( 721, 64 );
			this.gbPacketDate.TabIndex = 1;
			this.gbPacketDate.TabStop = false;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
			this.label2.Location = new System.Drawing.Point( 345, 18 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 64, 13 );
			this.label2.TabIndex = 21;
			this.label2.Text = "Data de Fim";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
			this.label4.Location = new System.Drawing.Point( 175, 18 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 75, 13 );
			this.label4.TabIndex = 19;
			this.label4.Text = "Hora de Início";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
			this.label3.Location = new System.Drawing.Point( 516, 18 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 64, 13 );
			this.label3.TabIndex = 18;
			this.label3.Text = "Hora de Fim";
			// 
			// lbDate
			// 
			this.lbDate.AutoSize = true;
			this.lbDate.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
			this.lbDate.Location = new System.Drawing.Point( 6, 18 );
			this.lbDate.Name = "lbDate";
			this.lbDate.Size = new System.Drawing.Size( 75, 13 );
			this.lbDate.TabIndex = 5;
			this.lbDate.Text = "Data de Início";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add( this.tbIpHost );
			this.groupBox3.Controls.Add( this.tbIpClient );
			this.groupBox3.Controls.Add( this.nudClientPort );
			this.groupBox3.Controls.Add( this.ckbClientPort );
			this.groupBox3.Controls.Add( this.ckbSessionHost );
			this.groupBox3.Controls.Add( this.ckbSessionClient );
			this.groupBox3.Location = new System.Drawing.Point( 8, 110 );
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size( 404, 162 );
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Filtros da Sessão";
			// 
			// gbResultsPanel
			// 
			this.gbResultsPanel.Controls.Add( this.btExportToAVI );
			this.gbResultsPanel.Controls.Add( this.btReplay );
			this.gbResultsPanel.Controls.Add( this.lbNResults );
			this.gbResultsPanel.Controls.Add( this.dgResults );
			this.gbResultsPanel.Enabled = false;
			this.gbResultsPanel.Location = new System.Drawing.Point( 8, 311 );
			this.gbResultsPanel.Name = "gbResultsPanel";
			this.gbResultsPanel.Size = new System.Drawing.Size( 1144, 345 );
			this.gbResultsPanel.TabIndex = 5;
			this.gbResultsPanel.TabStop = false;
			this.gbResultsPanel.Text = "Painel de Resultados de Busca";
			// 
			// lbNResults
			// 
			this.lbNResults.AutoSize = true;
			this.lbNResults.Location = new System.Drawing.Point( 6, 310 );
			this.lbNResults.Name = "lbNResults";
			this.lbNResults.Size = new System.Drawing.Size( 93, 13 );
			this.lbNResults.TabIndex = 1;
			this.lbNResults.Text = "Nenhum resultado";
			// 
			// dgResults
			// 
			this.dgResults.AllowUserToAddRows = false;
			this.dgResults.AllowUserToDeleteRows = false;
			this.dgResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgResults.Location = new System.Drawing.Point( 8, 20 );
			this.dgResults.MultiSelect = false;
			this.dgResults.Name = "dgResults";
			this.dgResults.ReadOnly = true;
			this.dgResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgResults.Size = new System.Drawing.Size( 1129, 284 );
			this.dgResults.TabIndex = 0;
			this.dgResults.TabStop = false;
			// 
			// tabReplay
			// 
			this.tabReplay.Controls.Add( this.gbReplayPanel );
			this.tabReplay.Location = new System.Drawing.Point( 4, 22 );
			this.tabReplay.Name = "tabReplay";
			this.tabReplay.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabReplay.Size = new System.Drawing.Size( 1166, 663 );
			this.tabReplay.TabIndex = 1;
			this.tabReplay.Text = "Replay";
			this.tabReplay.UseVisualStyleBackColor = true;
			// 
			// gbReplayPanel
			// 
			this.gbReplayPanel.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
			this.gbReplayPanel.Controls.Add( this.label6 );
			this.gbReplayPanel.Controls.Add( this.cbPacketsStrMatch );
			this.gbReplayPanel.Controls.Add( this.label1 );
			this.gbReplayPanel.Controls.Add( this.nudReplayDelay );
			this.gbReplayPanel.Controls.Add( this.picbArrow );
			this.gbReplayPanel.Controls.Add( this.label5 );
			this.gbReplayPanel.Controls.Add( this.lbReplayProgress );
			this.gbReplayPanel.Controls.Add( this.rtbActions );
			this.gbReplayPanel.Controls.Add( this.progbReplay );
			this.gbReplayPanel.Controls.Add( this.btPauseReplay );
			this.gbReplayPanel.Controls.Add( this.btStopReplay );
			this.gbReplayPanel.Location = new System.Drawing.Point( 8, 4 );
			this.gbReplayPanel.Name = "gbReplayPanel";
			this.gbReplayPanel.Size = new System.Drawing.Size( 1144, 416 );
			this.gbReplayPanel.TabIndex = 7;
			this.gbReplayPanel.TabStop = false;
			this.gbReplayPanel.Text = "Painel de Controle do Replay";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point( 209, 31 );
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size( 182, 13 );
			this.label6.TabIndex = 11;
			this.label6.Text = "Frames que possuem match de string";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 7, 31 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 178, 13 );
			this.label1.TabIndex = 9;
			this.label1.Text = "Delay Entre Frames do Replay ( ms )";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point( 6, 150 );
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size( 76, 13 );
			this.label5.TabIndex = 2;
			this.label5.Text = "Log de Ações:";
			// 
			// lbReplayProgress
			// 
			this.lbReplayProgress.AutoSize = true;
			this.lbReplayProgress.Location = new System.Drawing.Point( 6, 85 );
			this.lbReplayProgress.Name = "lbReplayProgress";
			this.lbReplayProgress.Size = new System.Drawing.Size( 108, 13 );
			this.lbReplayProgress.TabIndex = 0;
			this.lbReplayProgress.Text = "Progresso do Replay:";
			// 
			// progbReplay
			// 
			this.progbReplay.Location = new System.Drawing.Point( 9, 101 );
			this.progbReplay.Name = "progbReplay";
			this.progbReplay.Size = new System.Drawing.Size( 1128, 23 );
			this.progbReplay.Step = 1;
			this.progbReplay.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progbReplay.TabIndex = 2;
			// 
			// tabExport
			// 
			this.tabExport.Controls.Add( this.lbExportProgress );
			this.tabExport.Controls.Add( this.progbExport );
			this.tabExport.Location = new System.Drawing.Point( 4, 22 );
			this.tabExport.Name = "tabExport";
			this.tabExport.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabExport.Size = new System.Drawing.Size( 1166, 663 );
			this.tabExport.TabIndex = 2;
			this.tabExport.Text = "Exportação";
			this.tabExport.UseVisualStyleBackColor = true;
			// 
			// lbExportProgress
			// 
			this.lbExportProgress.AutoSize = true;
			this.lbExportProgress.Location = new System.Drawing.Point( 18, 266 );
			this.lbExportProgress.Name = "lbExportProgress";
			this.lbExportProgress.Size = new System.Drawing.Size( 129, 13 );
			this.lbExportProgress.TabIndex = 2;
			this.lbExportProgress.Text = "Progresso da Exportação:";
			// 
			// progbExport
			// 
			this.progbExport.Location = new System.Drawing.Point( 21, 282 );
			this.progbExport.Name = "progbExport";
			this.progbExport.Size = new System.Drawing.Size( 1128, 23 );
			this.progbExport.Step = 1;
			this.progbExport.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progbExport.TabIndex = 0;
			// 
			// tbSufixoBaseHexa
			// 
			this.tbSufixoBaseHexa.Location = new System.Drawing.Point( 123, 56 );
			this.tbSufixoBaseHexa.Name = "tbSufixoBaseHexa";
			this.tbSufixoBaseHexa.Size = new System.Drawing.Size( 100, 20 );
			this.tbSufixoBaseHexa.TabIndex = 1;
			this.tbSufixoBaseHexa.Text = "x";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point( 6, 59 );
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size( 111, 13 );
			this.label7.TabIndex = 2;
			this.label7.Text = "Sufixo Base TcpData:";
			// 
			// FrmApp
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 1168, 713 );
			this.Controls.Add( this.mnMainMenu );
			this.Controls.Add( this.tcTabsController );
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.MainMenuStrip = this.mnMainMenu;
			this.MaximizeBox = false;
			this.Name = "FrmApp";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Oi TN3270 Debugger";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.OnFormClosing );
			this.Shown += new System.EventHandler( this.OnShown );
			( ( System.ComponentModel.ISupportInitialize )( this.nudClientPort ) ).EndInit();
			this.groupBox1.ResumeLayout( false );
			this.groupBox1.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.picbArrow ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.nudReplayDelay ) ).EndInit();
			this.mnMainMenu.ResumeLayout( false );
			this.mnMainMenu.PerformLayout();
			this.tcTabsController.ResumeLayout( false );
			this.tabSearch.ResumeLayout( false );
			this.gbPacketFilter.ResumeLayout( false );
			this.gbPacketFilter.PerformLayout();
			this.gbPacketDirection.ResumeLayout( false );
			this.gbPacketDirection.PerformLayout();
			this.gbPacketDate.ResumeLayout( false );
			this.gbPacketDate.PerformLayout();
			this.groupBox3.ResumeLayout( false );
			this.groupBox3.PerformLayout();
			this.gbResultsPanel.ResumeLayout( false );
			this.gbResultsPanel.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.dgResults ) ).EndInit();
			this.tabReplay.ResumeLayout( false );
			this.gbReplayPanel.ResumeLayout( false );
			this.gbReplayPanel.PerformLayout();
			this.tabExport.ResumeLayout( false );
			this.tabExport.PerformLayout();
			this.ResumeLayout( false );
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.ToolTip tipAppHelper;
		private System.Windows.Forms.MenuStrip mnMainMenu;
		private System.Windows.Forms.ToolStripMenuItem arquivoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem configurarConexãoComOBDToolStripMenuItem;
		private System.Windows.Forms.TabControl tcTabsController;
		private System.Windows.Forms.TabPage tabSearch;
		private System.Windows.Forms.TabPage tabReplay;
		private System.Windows.Forms.Button btSearch;
		private System.Windows.Forms.GroupBox gbPacketFilter;
		private System.Windows.Forms.TextBox tbPacketString;
		private System.Windows.Forms.CheckBox ckbPacketString;
		private System.Windows.Forms.CheckBox ckbPacketDirection;
		private System.Windows.Forms.CheckBox ckbPacketDate;
		private System.Windows.Forms.GroupBox gbPacketDirection;
		private System.Windows.Forms.RadioButton rbPacketDirBoth;
		private System.Windows.Forms.RadioButton rbPacketDirHTC;
		private System.Windows.Forms.RadioButton rbPacketDirCTH;
		private System.Windows.Forms.GroupBox gbPacketDate;
		private System.Windows.Forms.Label lbDate;
		private System.Windows.Forms.DateTimePicker dtpBeginDate;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.NumericUpDown nudClientPort;
		private System.Windows.Forms.CheckBox ckbClientPort;
		private System.Windows.Forms.CheckBox ckbTerminalName;
		private System.Windows.Forms.CheckBox ckbTerminalType;
		private System.Windows.Forms.CheckBox ckbSessionHost;
		private System.Windows.Forms.CheckBox ckbSessionClient;
		private System.Windows.Forms.TextBox tbTerminalName;
		private System.Windows.Forms.TextBox tbTerminalType;
		private System.Windows.Forms.GroupBox gbResultsPanel;
		private System.Windows.Forms.Button btExportToAVI;
		private System.Windows.Forms.Button btReplay;
		private System.Windows.Forms.Label lbNResults;
		private System.Windows.Forms.DataGridView dgResults;
		private System.Windows.Forms.GroupBox gbReplayPanel;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lbReplayProgress;
		private System.Windows.Forms.RichTextBox rtbActions;
		private System.Windows.Forms.ProgressBar progbReplay;
		private System.Windows.Forms.Button btPauseReplay;
		private System.Windows.Forms.Button btStopReplay;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox cbDatabases;
		private System.Windows.Forms.TextBox tbIpHost;
		private System.Windows.Forms.TextBox tbIpClient;
		private System.Windows.Forms.PictureBox picbArrow;
		private System.Windows.Forms.TabPage tabExport;
		private System.Windows.Forms.Label lbExportProgress;
		private System.Windows.Forms.ProgressBar progbExport;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtpEndDate;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DateTimePicker dtpEndTime;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker dtpBeginTime;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown nudReplayDelay;
		private System.Windows.Forms.ToolStripMenuItem ferramentasToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox cbPacketsStrMatch;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox tbSufixoBaseHexa;
    }
}

