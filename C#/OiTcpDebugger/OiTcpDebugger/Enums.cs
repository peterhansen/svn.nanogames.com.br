﻿namespace OiTN3270Debugger
{
    enum EmulatorStyle
    {
        STYLE_24x80,
        STYLE_32x80,
        STYLE_43x80,
        STYLE_27x132
    };
}
