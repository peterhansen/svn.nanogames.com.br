﻿namespace OiTN3270Debugger
{
	partial class FrmBDConfig
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( FrmBDConfig ) );
			this.btCancel = new System.Windows.Forms.Button();
			this.btOk = new System.Windows.Forms.Button();
			this.tbBDConnUser = new System.Windows.Forms.TextBox();
			this.tbBDConnPass = new System.Windows.Forms.TextBox();
			this.nudBDConnPort = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tbBDConnIP = new System.Windows.Forms.TextBox();
			( ( System.ComponentModel.ISupportInitialize )( this.nudBDConnPort ) ).BeginInit();
			this.SuspendLayout();
			// 
			// btCancel
			// 
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.Location = new System.Drawing.Point( 216, 111 );
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size( 96, 29 );
			this.btCancel.TabIndex = 4;
			this.btCancel.Text = "&Cancelar";
			this.btCancel.UseVisualStyleBackColor = true;
			this.btCancel.Click += new System.EventHandler( this.OnClose );
			// 
			// btOk
			// 
			this.btOk.Location = new System.Drawing.Point( 318, 111 );
			this.btOk.Name = "btOk";
			this.btOk.Size = new System.Drawing.Size( 96, 29 );
			this.btOk.TabIndex = 5;
			this.btOk.Text = "&Ok";
			this.btOk.UseVisualStyleBackColor = true;
			this.btOk.Click += new System.EventHandler( this.OnDBConfigChanged );
			// 
			// tbBDConnUser
			// 
			this.tbBDConnUser.Location = new System.Drawing.Point( 12, 29 );
			this.tbBDConnUser.Name = "tbBDConnUser";
			this.tbBDConnUser.Size = new System.Drawing.Size( 245, 20 );
			this.tbBDConnUser.TabIndex = 0;
			// 
			// tbBDConnPass
			// 
			this.tbBDConnPass.Location = new System.Drawing.Point( 269, 29 );
			this.tbBDConnPass.Name = "tbBDConnPass";
			this.tbBDConnPass.PasswordChar = '*';
			this.tbBDConnPass.Size = new System.Drawing.Size( 144, 20 );
			this.tbBDConnPass.TabIndex = 1;
			this.tbBDConnPass.UseSystemPasswordChar = true;
			// 
			// nudBDConnPort
			// 
			this.nudBDConnPort.Location = new System.Drawing.Point( 189, 78 );
			this.nudBDConnPort.Maximum = new decimal( new int[] {
            65535,
            0,
            0,
            0} );
			this.nudBDConnPort.Name = "nudBDConnPort";
			this.nudBDConnPort.Size = new System.Drawing.Size( 89, 20 );
			this.nudBDConnPort.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 12, 13 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 89, 13 );
			this.label1.TabIndex = 0;
			this.label1.Text = "Nome do Usuário";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point( 266, 13 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 38, 13 );
			this.label2.TabIndex = 10;
			this.label2.Text = "Senha";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point( 186, 62 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 32, 13 );
			this.label3.TabIndex = 11;
			this.label3.Text = "Porta";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point( 12, 62 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 66, 13 );
			this.label4.TabIndex = 13;
			this.label4.Text = "Endereço IP";
			// 
			// tbBDConnIP
			// 
			this.tbBDConnIP.Location = new System.Drawing.Point( 12, 78 );
			this.tbBDConnIP.MaxLength = 15;
			this.tbBDConnIP.Name = "tbBDConnIP";
			this.tbBDConnIP.Size = new System.Drawing.Size( 164, 20 );
			this.tbBDConnIP.TabIndex = 2;
			this.tbBDConnIP.KeyDown += new System.Windows.Forms.KeyEventHandler( this.OnIPTextBoxKeyDown );
			// 
			// FrmBDConfig
			// 
			this.AcceptButton = this.btOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size( 426, 152 );
			this.Controls.Add( this.tbBDConnIP );
			this.Controls.Add( this.label4 );
			this.Controls.Add( this.label3 );
			this.Controls.Add( this.label2 );
			this.Controls.Add( this.label1 );
			this.Controls.Add( this.nudBDConnPort );
			this.Controls.Add( this.tbBDConnPass );
			this.Controls.Add( this.tbBDConnUser );
			this.Controls.Add( this.btOk );
			this.Controls.Add( this.btCancel );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.MaximizeBox = false;
			this.Name = "FrmBDConfig";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Configuração da Conexão com o Banco de Dados";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler( this.OnFormClosed );
			( ( System.ComponentModel.ISupportInitialize )( this.nudBDConnPort ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btCancel;
		private System.Windows.Forms.Button btOk;
		private System.Windows.Forms.TextBox tbBDConnUser;
		private System.Windows.Forms.TextBox tbBDConnPass;
		private System.Windows.Forms.NumericUpDown nudBDConnPort;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbBDConnIP;

	}
}