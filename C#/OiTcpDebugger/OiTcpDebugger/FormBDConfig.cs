﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace OiTN3270Debugger
{
	// Tipos dos delegates utilizados
	public delegate void OnDBConnConfigured( bool configChanged, IPAddress ip, UInt16 port, string user, string password );

	public partial class FrmBDConfig : Form
	{
		// Indica se a configuração oi alterada. Em caso positivo, chamaremos a callback recebida como parâmetro quando
		// o formulário for fechado
		private bool configChanged;

		// Callback que será chamada quando este formulário estiver prestes a ser fechado
		private OnDBConnConfigured callback;

		public FrmBDConfig( OnDBConnConfigured onFormClosed, IPAddress ip, UInt16 port, string user, string password )
		{
			InitializeComponent();

			IP = ip;
			Port = port;
			User = user;
			Password = password;

			callback = onFormClosed;

			configChanged = false;
		}

		public string Password
		{
			get
			{
				return tbBDConnPass.Text;
			}
			set
			{
				tbBDConnPass.Text = value;
			}
		}

		public string User
		{
			get
			{
				return tbBDConnUser.Text;
			}
			set
			{
				tbBDConnUser.Text = value;
			}
		}

		public IPAddress IP
		{
			get
			{
				if( tbBDConnIP.Text.Length == 0 )
					return new IPAddress( new byte[] { 127, 0, 0, 1 } );
				return IPAddress.Parse( tbBDConnIP.Text );
			}
			set
			{
				tbBDConnIP.Text = value.ToString();
			}
		}

		public UInt16 Port
		{
			get
			{
				return Convert.ToUInt16( nudBDConnPort.Value );
			}
			set
			{
				nudBDConnPort.Value = value;
			}
		}

		private void OnDBConfigChanged( object sender, EventArgs e )
		{
			// Verifica se o valor do IP está válido
			try
			{
				IPAddress temp = this.IP;
			}
			catch
			{
				MessageBox.Show( this, "O valor fornecido não é um endereço IP válido", "IP Inválido" );
				return;
			}

			configChanged = true;
			this.Close();
		}

		private void OnClose( object sender, EventArgs e )
		{
			configChanged = false;
			this.Close();
		}

		private void OnFormClosed( object sender, FormClosedEventArgs e )
		{
			if( configChanged )
				callback( true, IP, Port, User, Password );
			else
				callback( false, null, 0, null, null );
		}

		private void OnIPTextBoxKeyDown( object sender, KeyEventArgs e )
		{
			if( !Utils.IsValidIPInput( e ) )
			{
				e.Handled = true;
				e.SuppressKeyPress = true;
			}
		}
	}
}
