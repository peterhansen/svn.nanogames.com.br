﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Diagnostics;
using OiTN3270Debugger.Properties;
using MySql.Data.MySqlClient;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.IO;
using System.Collections;
using System.Globalization;

namespace OiTN3270Debugger
{
	public partial class FrmApp : Form
	{
		private Process emulatorProcess;
		private TN3270Server tn3270Server;
		private ReplayData replayData;

		private bool replayPaused;

		private bool windowHandleCreated;

		private bool draggingReplayArrow;

		private int dragOffsetX;

		private IPAddress dbIP;
		private UInt16 dbPort;
		private string dbUser, dbPassword, dbDatabase;

		private bool bbmModeOn;

		private static string ClientIpstr = "10.11.12.13";

		private int framesDelayMS;

		private UserActivityHook mouseHook;

		TabPage tabAfterEmulatorStarts;

		const Int32 TN3270_SERVER_PORT = 23;
		byte[] TN3270_SERVER_IP = new byte[] { 127, 0, 0, 1 };

		const byte CLIENT_TO_HOST = 1;
		const byte HOST_TO_CLIENT = 0;

		const byte TN3270_START_FIELD_BYTE = 0x1D;
		const byte TN3270_START_FIELD_EX_BYTE = 0x29;
		const byte TN3270_START_FIELD_EX_CNTL = 0xC0;

		const byte DIST_FROM_REPLAY_BAR_TO_ARROW = 2;

		private bool ReplayPaused
		{
			get
			{
				return replayPaused;
			}
			set
			{
				replayPaused = value;

				this.BeginInvoke( new MethodInvoker( delegate()
				{
					if( replayPaused )
						btPauseReplay.Text = "&Play";
					else
						btPauseReplay.Text = "&Pause";
				} ) );
			}
		}

		public FrmApp()
		{
			InitializeComponent();

			Settings.Default.Reload();

			string bli = Settings.Default.DB_DATABASE;

			DateTime today = DateTime.Now;
			dtpBeginTime.Value = new DateTime( today.Year, today.Month, today.Day, 0, 0, 0, 0 );
			dtpEndTime.Value = new DateTime( today.Year, today.Month, today.Day, 23, 59, 59, 999 );

			tn3270Server = new TN3270Server( new IPAddress( TN3270_SERVER_IP ), TN3270_SERVER_PORT, OnServerStarted, this, HandleReplayConn, this );

			emulatorProcess = null;
			replayData = null;

			replayPaused = false;

			if( String.Equals( Settings.Default.DB_SERVER, "localhost", StringComparison.OrdinalIgnoreCase ) )
				dbIP = new IPAddress( new byte[] { 127, 0, 0, 1 } );
			else
				dbIP = IPAddress.Parse( Settings.Default.DB_SERVER );

			dbPort = Convert.ToUInt16( Settings.Default.DB_PORT );
			dbUser = Settings.Default.DB_USER;
			dbPassword = Settings.Default.DB_PASSWORD;
			dbDatabase = Settings.Default.DB_DATABASE;

			framesDelayMS = Settings.Default.REPLAY_FRAMES_DELAY_MS;
			nudReplayDelay.Value = framesDelayMS;
			nudReplayDelay.Minimum = Settings.Default.REPLAY_MIN_FRAMES_DELAY;
			nudReplayDelay.Maximum = Settings.Default.REPLAY_MAX_FRAMES_DELAY;

			this.HandleCreated += new EventHandler( OnHanleCreated );
			this.HandleDestroyed += new EventHandler( OnHanleDestroyed );

			windowHandleCreated = false;

			draggingReplayArrow = false;
			dragOffsetX = 0;

			bbmModeOn = false;

			picbArrow.Location = new Point( progbReplay.Location.X - ( picbArrow.Size.Width >> 1 ), progbReplay.Location.Y + progbReplay.Size.Height + DIST_FROM_REPLAY_BAR_TO_ARROW );

			mouseHook = new UserActivityHook( true, false );
			mouseHook.OnMouseActivity += new MouseEventHandler( OnMouseMoved );
		}

		private void OnHanleCreated( object sender, EventArgs e )
		{
			windowHandleCreated = true;
		}

		private void OnHanleDestroyed( object sender, EventArgs e )
		{
			windowHandleCreated = false;
		}

		private bool IsPlayingReplay()
		{
			return ( replayData != null ) && ( emulatorProcess != null ) && ( !emulatorProcess.HasExited ) && ( tabReplay.Enabled );
		}


		private string GetSessionFiltersQueryStr()
		{
			string sessionFiltersStr = "";

			if( ckbSessionClient.Checked && tbIpClient.Text.Length > 0 )
			{
				string ip = tbIpClient.Text.Replace( " ", String.Empty );

				IPAddress ipC = IPAddress.Parse( ip );
				sessionFiltersStr += "AND p.ClientIp = " + toIpSql( ipC ) + " ";

			}

			if( ckbClientPort.Checked )
			{
				sessionFiltersStr += "AND p.ClientPort = " + nudClientPort.Value + " ";
			}

			if( ckbSessionHost.Checked && tbIpHost.Text.Length > 0 )
			{
				string ip = tbIpHost.Text.Replace( " ", String.Empty );
				IPAddress ipH = IPAddress.Parse( ip );
				sessionFiltersStr += "AND ((p.Host - 23) / 100000) = " + toIpSql( ipH ) + " ";

			}

			return sessionFiltersStr;
		}

		private string GetPacketFiltersQueryStr()
		{
			string packetFiltersStr = "";

			if( ckbPacketDate.Checked )
			{
				DateTime beginDate = dtpBeginDate.Value;
				DateTime endDate = dtpEndDate.Value;

				DateTime beginTime = dtpBeginTime.Value;
				DateTime endTime = dtpEndTime.Value;

				DateTime temp = new DateTime( beginDate.Year, beginDate.Month, beginDate.Day, beginTime.Hour, beginTime.Minute, beginTime.Second, 0, DateTimeKind.Local );
				string begin = temp.ToUniversalTime().ToString( "yyyy-MM-dd HH:mm:ss" );

				temp = new DateTime( endDate.Year, endDate.Month, endDate.Day, endTime.Hour, endTime.Minute, endTime.Second, 999, DateTimeKind.Local );
				string end = temp.ToUniversalTime().ToString( "yyyy-MM-dd HH:mm:ss" );

				packetFiltersStr += "AND conversations.Inicio >= '" + begin + "' ";
				packetFiltersStr += "AND conversations.Fim <= '" + end + "' ";
			}

			if( ckbPacketDirection.Checked && !rbPacketDirBoth.Checked )
			{
				packetFiltersStr += "AND p.Direction = " + ( rbPacketDirCTH.Checked ? CLIENT_TO_HOST : HOST_TO_CLIENT ) + " ";
			}

			if( ckbPacketString.Checked && tbPacketString.Text.Length > 0 )
			{
				packetFiltersStr += "AND UPPER( p.FieldData ) LIKE '%" + tbPacketString.Text.ToUpper() + "%' ";
			}

			if( ckbTerminalType.Checked && tbTerminalType.Text.Length > 0 )
			{
				packetFiltersStr += "AND ( p.FieldData ) LIKE '%TTyp:" + tbTerminalType.Text.ToUpper() + "*%' ";
			}

			if( ckbTerminalName.Checked && tbTerminalName.Text.Length > 0 )
			{
				packetFiltersStr += "AND ( p.FieldData ) LIKE '%LUnm:" + tbTerminalName.Text.ToUpper() + "*%' ";
			}


			return packetFiltersStr;
		}

		private UInt64 toIpSql( IPAddress ipDot )
		{

			Byte[] bytes = ipDot.GetAddressBytes();
			UInt64 ipSql = ( UInt64 )( ( UInt64 )( bytes[ 0 ] & 0xff ) * 1000000000Lu ) +
							( UInt64 )( ( bytes[ 1 ] & 0xff ) * 1000000 ) +
							( UInt64 )( ( bytes[ 2 ] & 0xff ) * 1000 ) +
							( UInt64 )( ( bytes[ 3 ] & 0xff ) );

			return ipSql;
		}

		private string GetQueryForFilters()
		{
			string packetsFiltersStr = GetPacketFiltersQueryStr();
			string sessionFiltersStr = GetSessionFiltersQueryStr();
			string queryString = " SELECT DISTINCT "
								+ "ClientIp, ClientPort, (Host - 23) / 100000 as Host, "
								+ "epochTimeInSecsToDate(MIN(EpochTime)) Inicio, "
								+ "epochTimeInSecsToDate(MAX(EpochTime)) Fim, "
								+ "MIN(EpochTime) as InicioD, "
								+ "MAX(EpochTime) as FimD, "
								+ "MAX(EpochTime) - MIN(EpochTime) as TempoTotal, "
								+ "Count(TcpLength > 0) as Pacotes, "
								+ "CEIL(SUM(TcpSyn) / 2) as Conexoes, SUM(TcpLength) as Bytes "
								+ "FROM TcpPackets p WHERE 1 = 1 ";

			if( packetsFiltersStr.Trim().Length > 0 )
			{
				queryString += packetsFiltersStr;
			}

			if( sessionFiltersStr.Trim().Length > 0 )
			{
				queryString += sessionFiltersStr;
			}

			queryString += "GROUP BY p.ClientIP, p.ClientPort, p.Host LIMIT " + Settings.Default.MAX_QUERY_RESULTS;
			queryString += " ;";
			return queryString;

		}

		private bool ValidateFilters()
		{
			if( tbIpClient.Text.Length > 0 )
			{
				try
				{
					IPAddress.Parse( tbIpClient.Text );
				}
				catch
				{
					MessageBox.Show( this, "O valor fornecido para o filtro 'IP do Cliente' não é um endereço IP válido.", "IP Inválido" );
					return false;
				}
			}

			if( tbIpHost.Text.Length > 0 )
			{
				try
				{
					IPAddress.Parse( tbIpHost.Text );
				}
				catch
				{
					MessageBox.Show( this, "O valor fornecido para o filtro 'IP do Servidor' não é um endereço IP válido.", "IP Inválido" );
					return false;
				}
			}

			return true;
		}

		private void OnSearchResults( object sender, EventArgs e )
		{
			if( !ValidateFilters() )
				return;

			MySqlConnection connection = null;
			MySqlDataAdapter dataAdapter = null;

			try
			{
				connection = GetMySQLConn( dbDatabase );
				dataAdapter = new MySqlDataAdapter( GetQueryForFilters(), connection );

				DataSet ds = new DataSet();
				connection.Open();
				dataAdapter.Fill( ds );
				connection.Close();

				if( ds.Tables.Count > 0 )
				{
					DataTable tbl = ds.Tables[ 0 ];
					dgResults.DataSource = tbl;

					ds.EnforceConstraints = false;

					dgResults.Columns[ "InicioD" ].Visible = false;
					dgResults.Columns[ "FimD" ].Visible = false;
					dgResults.Columns[ "ClientIp" ].DefaultCellStyle.Format = "##0'.'##0'.'##0'.'##0";
					dgResults.Columns[ "Host" ].DefaultCellStyle.Format = "##0'.'##0'.'##0'.'##0";
					dgResults.Columns[ "TempoTotal" ].DefaultCellStyle.Format = "###,###,###,##0";
					dgResults.Columns[ "Bytes" ].DefaultCellStyle.Format = "###,###,###,##0";
					dgResults.Columns[ "Pacotes" ].DefaultCellStyle.Format = "###,###,###,##0";
					int nRows = tbl.Rows.Count;

					foreach( DataColumn column in tbl.Columns )
					{
						dgResults.Columns[ column.Ordinal ].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
						dgResults.Columns[ column.Ordinal ].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
					}




					int[] columnsIndexes = { tbl.Columns[ "Inicio" ].Ordinal, tbl.Columns[ "Fim" ].Ordinal };
					DataGridViewColumn[] dateTimeColumns = { dgResults.Columns[ columnsIndexes[ 0 ] ], dgResults.Columns[ columnsIndexes[ 1 ] ] };

					for( int i = 0 ; i < dateTimeColumns.Length ; ++i )
						dateTimeColumns[ i ].DefaultCellStyle.Format = "dd/MM/yyyy HH:mm:ss";

					for( int row = 0 ; row < nRows ; ++row )
					{
						DataRow temp = tbl.Rows[ row ];
						temp.BeginEdit();
						for( int i = 0 ; i < columnsIndexes.Length ; ++i )
							temp[ columnsIndexes[ i ] ] = ( ( DateTime )temp[ columnsIndexes[ i ] ] ).ToLocalTime();
					}

					tbl.AcceptChanges();
					ds.EnforceConstraints = true;

					lbNResults.Text = "Número de resultados: " + ds.Tables[ 0 ].Rows.Count;
					gbResultsPanel.Enabled = true;
				}
				else
				{
					lbNResults.Text = "Nenhum resultado";
					gbResultsPanel.Enabled = false;
				}
			}
			catch( Exception ex )
			{
				dgResults.DataSource = null;
				MessageBox.Show( "Error while searching for data: " + ex.Message );
			}
			finally
			{
				if( dataAdapter != null )
				{
					dataAdapter.Dispose();
				}

				if( connection != null )
				{
					connection.Close();
					connection.Dispose();
				}
			}
		}

		private void OnExportToAVI( object sender, EventArgs e )
		{
			tabAfterEmulatorStarts = tabExport;
			tn3270Server.SetConnEstablishedCallback( HandleExportConn );
			TabReplayStart();
		}

		private void OnReplaySession( object sender, EventArgs e )
		{
			tabAfterEmulatorStarts = tabReplay;
			tn3270Server.SetConnEstablishedCallback( HandleReplayConn );
			TabReplayStart();
		}

		private void TabReplayStart()
		{
			if( dgResults.SelectedRows.Count == 1 )
			{
				DataRowView selectedRow = ( DataRowView )dgResults.SelectedRows[ 0 ].DataBoundItem;
				UInt64 clientIp = Convert.ToUInt64( selectedRow[ "ClientIp" ] );
				UInt64 clientPort = Convert.ToUInt64( selectedRow[ "ClientPort" ] );
				UInt64 host = Convert.ToUInt64( selectedRow[ "Host" ] ) * 100000 + 23;

				double ini = Convert.ToDouble( selectedRow[ "InicioD" ] );
				double fim = Convert.ToDouble( selectedRow[ "FimD" ] );

				if( GetConversationPackets( clientIp, clientPort, host, ini, fim ) )
				{
					if( ckbPacketString.Checked && tbPacketString.Text.Length > 0 )
					{
						cbPacketsStrMatch.Items.Clear();
						cbPacketsStrMatch.Enabled = false;

						MySqlConnection connection = null;
						MySqlDataAdapter dataAdapter = null;
						try
						{
							connection = GetMySQLConn( dbDatabase );
							UInt64 ihost = Convert.ToUInt64( selectedRow[ "Host" ] ) * 100000 + 23;
							dataAdapter = new MySqlDataAdapter( "SELECT EpochTime Hora FROM TcpPackets WHERE ClientIp = "
							   + selectedRow[ "ClientIp" ]
							   + " AND ClientPort = " + selectedRow[ "ClientPort" ]
							   + " AND Host = " + ihost
							   + " AND FieldData LIKE \"%" + tbPacketString.Text
							   + "%\"", connection );
							DataSet ds = new DataSet();
							connection.Open();
							dataAdapter.Fill( ds );
							connection.Close();


							if( ds.Tables.Count > 0 )
							{
								cbPacketsStrMatch.Items.Add( "<Selecione o Frame>" );

								foreach( DataRow row in ds.Tables[ 0 ].Rows )
									cbPacketsStrMatch.Items.Add( Utils.EpochTimeToLocalTime( ( double )row[ "Hora" ] ).ToString( "dd/MM/yyyy HH:mm:ss" ) );

								cbPacketsStrMatch.Enabled = true;
								cbPacketsStrMatch.SelectedItem = cbPacketsStrMatch.Items[ 0 ];
							}
						}
						catch( Exception ex )
						{
							MessageBox.Show( "Exception on MySql \n" + ex.Message );
						}
						finally
						{
							if( dataAdapter != null )
							{
								dataAdapter.Dispose();
							}
							if( connection != null )
							{
								connection.Close();
								connection.Dispose();
							}
						}
					}
					StartServer();
				}
				else
				{
					MessageBox.Show( "No packets found for this conversation in the DB.", "Conversation Empty" );
				}
			}
		}

		private void SetCurrentTab( TabPage nextTab )
		{
			try
			{
				if( !windowHandleCreated )
					return;

				this.BeginInvoke( new MethodInvoker( delegate()
				{
					foreach( TabPage tab in tcTabsController.TabPages )
						tab.Enabled = false;

					nextTab.Enabled = true;
					tcTabsController.SelectTab( nextTab );
				} ) );
			}
			catch( Exception ex )
			{
				MessageBox.Show( "Could not access interface thread: " + ex.Message );
			}
		}

		private MySqlConnection GetMySQLConn( string database )
		{
			if( database.Length > 0 )
				return new MySqlConnection( "SERVER=" + dbIP + ";UID=" + dbUser + ";PASSWORD=" + dbPassword + ";PORT=" + dbPort + ";DATABASE=" + database + ";default command timeout=3600;" );

			return new MySqlConnection( "SERVER=" + dbIP + ";UID=" + dbUser + ";PASSWORD=" + dbPassword + ";PORT=" + dbPort + ";default command timeout=3600;" );
		}

		private static void AppendPackets( DataTable packetsTable )
		{
			double defaultEpochTime = ( double )packetsTable.Rows[ 0 ][ "EpochTime" ];

			DataRow row;

			row = packetsTable.NewRow();
			row[ "Direction" ] = HOST_TO_CLIENT;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfa, 0x28, 0x03, 0x07, 0x02, 0x00, 0x04, 0xff, 0xf0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x31, 0x01, 0x03, 0x03, 0x91, 0x90, 0x30, 0x80, 0x00, 0x84, 0x87, 0xf8, 0x80, 0x00, 0x02, 0x80, 0x00, 0x00, 0x00, 0x00, 0x18, 0x50, 0x00, 0x00, 0x7e, 0x00, 0x00, 0x06, 0xe3, 0xc5, 0xd3, 0xd5, 0xc5, 0xe3, 0x00, 0xff, 0xef, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x76, 0x13 };
			packetsTable.Rows.InsertAt( row, 0 );

			row = packetsTable.NewRow();
			row[ "Direction" ] = CLIENT_TO_HOST;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfa, 0x28, 0x03, 0x07, 0x02, 0x00, 0x04, 0xff, 0xf0 };
			packetsTable.Rows.InsertAt( row, 0 );

			row = packetsTable.NewRow();
			row[ "Direction" ] = HOST_TO_CLIENT;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfa, 0x28, 0x02, 0x04, 0x49, 0x42, 0x4d, 0x2d, 0x33, 0x32, 0x37, 0x38, 0x2d, 0x32, 0x2d, 0x45, 0x01, 0x2a, 0x2a, 0x2a, 0x2a, 0x2a, 0x2a, 0x2a, 0x2a, 0xff, 0xf0 };
			packetsTable.Rows.InsertAt( row, 0 );

			row = packetsTable.NewRow();
			row[ "Direction" ] = CLIENT_TO_HOST;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfa, 0x28, 0x02, 0x07, 0x49, 0x42, 0x4d, 0x2d, 0x33, 0x32, 0x37, 0x38, 0x2d, 0x32, 0x2d, 0x45, 0xff, 0xf0 };
			packetsTable.Rows.InsertAt( row, 0 );

			row = packetsTable.NewRow();
			row[ "Direction" ] = HOST_TO_CLIENT;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfa, 0x28, 0x08, 0x02, 0xff, 0xf0 };
			packetsTable.Rows.InsertAt( row, 0 );

			row = packetsTable.NewRow();
			row[ "Direction" ] = CLIENT_TO_HOST;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfb, 0x28 };
			packetsTable.Rows.InsertAt( row, 0 );

			row = packetsTable.NewRow();
			row[ "Direction" ] = HOST_TO_CLIENT;
			row[ "EpochTime" ] = defaultEpochTime;
			row[ "TcpData" ] = new byte[] { 0xff, 0xfd, 0x28 };
			packetsTable.Rows.InsertAt( row, 0 );
		}

		private bool GetConversationPackets( UInt64 clientIp, UInt64 clientPort, UInt64 host, double ini, double fim )
		{
			DataSet conversationPackets = null;
			MySqlConnection connection = null;
			MySqlDataAdapter dataAdapter = null;

			try
			{
				CultureInfo enUS = new CultureInfo( "en-US" );

				connection = GetMySQLConn( dbDatabase + tbSufixoBaseHexa.Text.Trim() );
				string select = "SELECT EpochTime, TcpData, Direction, TcpSyn FROM TcpDatax WHERE ClientIp = " + clientIp +
													" AND ClientPort = " + clientPort + " AND Host = " + host + " ORDER BY EpochTime ";
				dataAdapter = new MySqlDataAdapter( select, connection );

				conversationPackets = new DataSet();

				connection.Open();
				dataAdapter.Fill( conversationPackets );
				connection.Close();

				if( ( conversationPackets.Tables.Count > 0 ) && ( conversationPackets.Tables[ 0 ].Rows.Count > 0 ) )
				{

					Int64 icliente = Convert.ToInt64( clientIp );
					long ip4 = icliente % 1000;
					long ip3 = ( ( icliente - ip4 ) % 1000000 ) / 1000;
					long ip1 = ( icliente - ( icliente % 1000000000L ) ) / 1000000000L;
					long ip2 = ( icliente - ( ip1 * 1000000000L ) ) / 1000000;

					ClientIpstr = ip1 + "." + ip2 + "." + ip3 + "." + ip4 + ":" + clientPort;

					replayData = new ReplayData( conversationPackets.Tables[ 0 ] );
					conversationPackets = null;

					if( ( ulong )replayData.PacketsTable.Rows[ 0 ][ "TcpSyn" ] != 1 )
					{

						AppendPackets( replayData.PacketsTable );

					}

					return true;
				}

				replayData = null;
				return false;
			}
			catch( Exception ex )
			{
				MessageBox.Show( "Could not get conversation packets: " + ex.Message );
				return false;
			}
			finally
			{
				if( dataAdapter != null )
				{
					dataAdapter.Dispose();
				}

				if( connection != null )
				{
					connection.Close();
					connection.Dispose();
				}

				if( conversationPackets != null )
				{
					conversationPackets.Clear();
					conversationPackets.Dispose();
				}
			}
		}

		private bool StartServer()
		{
			try
			{
				tn3270Server.Start();
			}
			catch( Exception ex )
			{
				MessageBox.Show( "Could not start telnet server: " + ex.Message );
				return false;
			}
			return true;
		}

		private void OnServerStarted( object param )
		{
			if( StartEmulator( LocateStyle() ) )
			{
				this.BeginInvoke( new MethodInvoker( delegate()
				{
					if( tabAfterEmulatorStarts == tabReplay )
					{
						EmptyLog();

						progbReplay.Value = 0;
						progbReplay.Minimum = 0;
						progbReplay.Maximum = replayData.NPackets;

						lbReplayProgress.Text = "Progresso do Replay:";

						ReplayPaused = false;
					}
					else
					{
						progbExport.Value = 0;
						progbExport.Minimum = 0;
						progbExport.Maximum = replayData.NPackets;

						lbExportProgress.Text = "Progresso da Exportação:";
					}
					SetCurrentTab( tabAfterEmulatorStarts );
				} ) );
			}
		}

		private static bool ReplayPacket( ReplayData replayData, NetworkStream emulatorStream, RichTextBox log, bool showHiddenFields, bool found00 )
		{
			bool emulatorScreenUpdated = false;

			byte[] packetData;
			byte[] packetData1 = ( byte[] )( replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "TcpData" ] );
			if( packetData1.Length > 0 )
			{
				byte[] dataToWrite = null;
				byte aid = 0, cursorX = 0, cursorY = 0;
				byte packetDir = Convert.ToByte( replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "Direction" ] );
				byte packetNext = 2;
				bool packetIncrement = false;
				if( replayData.NPackets > replayData.CurrPacket + 1 )
				{
					packetNext = Convert.ToByte( replayData.PacketsTable.Rows[ replayData.CurrPacket + 1 ][ "Direction" ] );
				}

				if( packetDir == packetNext )
				{
					byte[] packetData2 = ( byte[] )( replayData.PacketsTable.Rows[ replayData.CurrPacket + 1 ][ "TcpData" ] );

					packetData = new byte[ packetData1.Length + packetData2.Length ];

					packetData1.CopyTo( packetData, 0 );
					packetData2.CopyTo( packetData, packetData1.Length );

					packetIncrement = true;
				}
				else
				{
					packetData = new byte[ packetData1.Length ];
					packetData1.CopyTo( packetData, 0 );
				}

				if( packetDir == HOST_TO_CLIENT )
				{
					if( showHiddenFields )
						ShowNoDisplayFields( packetData );
					if( log != null )
						LogData( log, Utils.EpochTimeToLocalTime( ( double )replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "EpochTime" ] ), replayData.CurrPacket, 0x41, 0xFF, 0xFF, packetData, showHiddenFields, null );
					dataToWrite = packetData;
				}
				else
				{
					bool test = ( packetData[ 0 ] == 0x00 ) || ( !found00 && packetData[ 0 ] != 0xff );
					dataToWrite = ( test ? ChangeReadToWrite( packetData, out aid, out cursorX, out cursorY, found00 ) : null );
				}

				if( dataToWrite != null )
				{
					emulatorStream.Write( dataToWrite, 0, dataToWrite.Length );
					emulatorScreenUpdated = true;
				}

				if( packetDir == CLIENT_TO_HOST )
				{
					ArrayList noDisplayFields = new ArrayList();
					if( !showHiddenFields )
					{


						DateTime CurTitle = Utils.EpochTimeToLocalTime( ( double )replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "EpochTime" ] );
						string CurPacknum = "IP:"
								+ ClientIpstr
								+ "   "
								+ CurTitle.ToString( "dd/MM/yyyy HH:mm:ss" ) + "  "
								+ replayData.CurrPacket.ToString()
								+ " / "
								+ replayData.NPackets.ToString()
								;

						GetNoDisplayfields( noDisplayFields, CurPacknum );
					}
					if( log != null )
					{
						NoDisplayField[] temp = new NoDisplayField[ noDisplayFields.Count ];
						for( int i = 0 ; i < noDisplayFields.Count ; ++i )
							temp[ i ] = ( NoDisplayField )noDisplayFields[ i ];

						LogData( log, Utils.EpochTimeToLocalTime( ( double )replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "EpochTime" ] ), replayData.CurrPacket, aid, cursorX, cursorY, packetData, showHiddenFields, temp );
					}
				}
				if( packetIncrement )
				{
					++replayData.CurrPacket;
				}
			}
			return emulatorScreenUpdated;
		}

		private struct NoDisplayField
		{
			public NoDisplayField( int ind, int dataLength )
			{
				index = ind;
				data = new byte[ dataLength ];
			}

			public int index;
			public byte[] data;
		}

		private static unsafe bool GetNoDisplayfields( ArrayList noDisplayFields, String CurTitle )
		{
			byte[] str, ps;
			int func, length, ret;

			func = WinHLLAPI.CONNECTPS;
			str = Encoding.ASCII.GetBytes( Settings.Default.HLLAPI_SESSION_ID + "   \0" );
			length = 4;
			fixed( byte* pTemp = str )
				WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

			if( ret != WinHLLAPI.WHLLOK )
			{
				func = WinHLLAPI.DISCONNECTPS;
				str = Encoding.ASCII.GetBytes( Settings.Default.HLLAPI_SESSION_ID + "   \0" );
				length = str.Length;
				fixed( byte* pTemp = str )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );
				return false;
			}

			bool ok = true;
			try
			{
				func = WinHLLAPI.SETSESSIONPARAMETERS;
				str = Encoding.ASCII.GetBytes( "NOCFGSIZE,NODISPLAY\0" );
				length = 2;
				fixed( byte* pTemp = str )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

				if( ret != WinHLLAPI.WHLLOK )
					throw new Exception( "HLLAPI Error" );

				func = WinHLLAPI.QUERYSESSIONS;
				str = new byte[ 12 ];
				length = str.Length;
				fixed( byte* pTemp = str )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

				if( ret != WinHLLAPI.WHLLOK )
					throw new Exception( "HLLAPI Error" );

				int psSize = ( ( ( int )str[ 11 ] ) << 8 ) | str[ 10 ];
				ps = new byte[ psSize ];

				func = WinHLLAPI.CONNECTWINDOWSERVICES;
				str = Encoding.ASCII.GetBytes( Settings.Default.HLLAPI_SESSION_ID + "\0" );

				length = str.Length;
				fixed( byte* pTemp = str )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

				func = WinHLLAPI.CHANGEPSNAME;
				byte[] strx = new byte[ 63 ];
				strx[ 0 ] = 0x5a;
				strx[ 1 ] = 0x01;
				str = Encoding.ASCII.GetBytes( CurTitle
					+ "\0" );
				str.CopyTo( strx, 2 );
				length = strx.Length;
				fixed( byte* pTemp = strx )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

				func = WinHLLAPI.DISCONNECTWINDOWSERVICES;
				str = Encoding.ASCII.GetBytes( Settings.Default.HLLAPI_SESSION_ID + "\0" );

				length = str.Length;
				fixed( byte* pTemp = str )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

				func = WinHLLAPI.COPYPS;
				length = 0;
				fixed( byte* pTemp = ps )
					WinHLLAPI.HLLAPI( &func, pTemp, &psSize, &ret );

				if( ret != WinHLLAPI.WHLLOK )
					throw new Exception( "HLLAPI Error" );

				int firstZeroIndex = Array.FindIndex( ps, byteValue => byteValue == 0x00 );
				if( firstZeroIndex != -1 )
				{
					for( int i = firstZeroIndex ; i < psSize ; ++i )
					{
						if( ps[ i ] == 0 )
						{
							int j;
							for( j = i ; j < psSize ; ++j )
							{
								if( ps[ j ] != 0 )
									break;
							}
							NoDisplayField hf = new NoDisplayField( i, j - i );
							noDisplayFields.Add( hf );
							i = j;
						}
					}

					func = WinHLLAPI.SETSESSIONPARAMETERS;
					str = Encoding.ASCII.GetBytes( "DISPLAY\0" );
					length = 1;
					fixed( byte* pTemp = str )
						WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );

					if( ret != WinHLLAPI.WHLLOK )
						throw new Exception( "HLLAPI Error" );

					func = WinHLLAPI.COPYPS;
					length = 0;
					fixed( byte* pTemp = ps )
						WinHLLAPI.HLLAPI( &func, pTemp, &psSize, &ret );

					if( ret != WinHLLAPI.WHLLOK )
						throw new Exception( "HLLAPI Error" );

					foreach( NoDisplayField field in noDisplayFields )
						Array.Copy( ps, field.index, field.data, 0, field.data.Length );


				}
			}
			catch
			{
				noDisplayFields.Clear();
				ok = false;
			}
			finally
			{
				func = WinHLLAPI.DISCONNECTPS;
				str = Encoding.ASCII.GetBytes( Settings.Default.HLLAPI_SESSION_ID + "   \0" );
				length = str.Length;
				fixed( byte* pTemp = str )
					WinHLLAPI.HLLAPI( &func, pTemp, &length, &ret );
			}
			return ok;
		}

		private void HandleReplayConn( object param )
		{
			try
			{
				while( !tabReplay.Enabled )
					Thread.Sleep( 100 );

				int extraWaitTime = 0;
				const int BUSY_WAIT_TIME_MILLIS = 200;

				bool found00 = isExtended();

				while( true )
				{
					lock( this )
					{
						if( !IsPlayingReplay() )
						{
							return;
						}

						if( !ReplayPaused && ( replayData.CurrPacket < replayData.NPackets ) )
						{
							bool emulatorScreenUpdated = ReplayPacket( replayData, tn3270Server.GetTcpClientStream(), rtbActions, bbmModeOn, found00 );

							++replayData.CurrPacket;

							this.BeginInvoke( new MethodInvoker( delegate()
{
	progbReplay.Value = replayData.CurrPacket;
	double percent = Convert.ToDouble( replayData.CurrPacket ) / Convert.ToDouble( replayData.NPackets );
	Int32 aux = Convert.ToInt32( percent * 100 );
	lbReplayProgress.Text = "Progresso do Replay: " + ( aux > 100 ? 100 : aux ) + "% ( " + replayData.CurrPacket + " / " + replayData.NPackets + " )";

	picbArrow.Location = new Point( progbReplay.Location.X + Convert.ToInt32( progbReplay.Size.Width * percent ) - ( picbArrow.Size.Width >> 1 ), picbArrow.Location.Y );
} ) );

							extraWaitTime = ( emulatorScreenUpdated ? framesDelayMS : 0 );
						}
					}
					Thread.Sleep( extraWaitTime > 0 ? extraWaitTime : BUSY_WAIT_TIME_MILLIS );
				}
			}
			catch( DllNotFoundException ex )
			{
				MessageBox.Show( ex.Message + "\n\nUnable to continue.", "DLL not Found" );
			}
			catch( Exception ex )
			{
				MessageBox.Show( ex.Message, "Unexpected Error" );
			}
			finally
			{
				OnReplayStop( btStopReplay, new EventArgs() );
			}
		}

		private bool isExtended()
		{
			if( replayData.NPackets < 20 ) return true;

			bool found00 = false;
			int limit = 5;
			while( ( replayData.CurrPacket < replayData.NPackets ) && ( !found00 ) && ( limit > 0 ) )
			{
				byte packetDir = Convert.ToByte( replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "Direction" ] );

				if( packetDir == CLIENT_TO_HOST )
				{

					byte[] packetData = ( byte[] )( replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "TcpData" ] );

					if( ( packetData != null ) && ( packetData.Length > 0 ) && ( packetData[ 0 ] != 0xFF ) )
					{
						if( packetData[ 0 ] == 0x00 )
						{
							found00 = true;
						}
						else
						{
							--limit;
						}
					}
				}
				++replayData.CurrPacket;
			}

			replayData.CurrPacket = 0;
			return found00;
		}

		private EmulatorStyle LocateStyle()
		{
			EmulatorStyle TermType = EmulatorStyle.STYLE_24x80;
			if( replayData.NPackets < 20 )
				return EmulatorStyle.STYLE_24x80;

			bool found00 = false;
			int limit = 10;
			while( ( replayData.CurrPacket < replayData.NPackets ) && ( !found00 ) && ( limit > 0 ) )
			{
				byte packetDir = Convert.ToByte( replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "Direction" ] );

				if( packetDir == CLIENT_TO_HOST )
				{

					byte[] packetData = ( byte[] )( replayData.PacketsTable.Rows[ replayData.CurrPacket ][ "TcpData" ] );

					if( packetData.Length > 0 && packetData[ 0 ] == 0xFF )
					{
						int i = packetData.Length;
						int max = i - 12;

						for( int j = 1 ; j < max && !found00 ; j++ )
						{
							while( ( packetData[ j ] == 0xFA ) && ( packetData[ j + 1 ] == 0x28 ) && ( !found00 ) )
							{
								for( int k = j + 2 ; k < max ; k++ )
									if( packetData[ k ] == 0x02 && packetData[ k + 1 ] == 0x07 && ( !found00 ) )
									{
										found00 = true;
										TermType = EmulatorStyle.STYLE_24x80 + ( packetData[ k + 11 ] - '2' );
										k = i;
									}
							}
							while( ( packetData[ j ] == 0xFA ) && ( packetData[ j + 1 ] == 0x18 ) && ( !found00 ) )
							{
								for( int k = j + 2 ; k < max ; k++ )
								{
									if( ( packetData[ k ] == 0x00 ) && ( !found00 ) )
									{
										TermType = EmulatorStyle.STYLE_24x80 + ( packetData[ k + 10 ] - '2' );
										found00 = true;
										k = i;
									}
								}
							}
						}
					}
					else
					{
						--limit;
					}
				}
				++replayData.CurrPacket;
			}

			replayData.CurrPacket = 0;
			return TermType;
		}

		private void HandleExportConn( object param )
		{
			IntPtr hWnd = IntPtr.Zero;
			IntPtr hDC = IntPtr.Zero;

			try
			{
				hWnd = emulatorProcess.MainWindowHandle;

				hDC = Win32API.GetDC( hWnd );
				if( hDC == IntPtr.Zero )
					throw new Win32Exception( "Couldn't get emulator's window's device context" );

				Win32API.RECT r;
				unsafe
				{
					Win32API.GetWindowRect( hWnd, &r );
				}

				uint nSavedImgs = 0;
				int wndWidth = r.right - r.left;
				int wndHeight = r.bottom - r.top;

				Int32 nPackets = replayData.NPackets;
				NetworkStream stream = tn3270Server.GetTcpClientStream();

				string dirPath = Application.StartupPath + Settings.Default.TEMP_DIR_PATH;
				if( !Directory.Exists( dirPath ) )
					Directory.CreateDirectory( dirPath );

				bool found00 = isExtended();

				for( Int32 i = 0 ; ( ( i < nPackets ) && ( replayData.CurrPacket < nPackets ) ) ; ++i )
				{
					try
					{
						ReplayPacket( replayData, stream, null, bbmModeOn, found00 );

						Graphics g = Graphics.FromHdc( hDC );
						Bitmap img = new Bitmap( wndWidth, wndHeight, g );
						Graphics memoryGraphics = Graphics.FromImage( img );
						IntPtr dc = memoryGraphics.GetHdc();
						bool success = Win32API.PrintWindow( hWnd, dc, 0 );
						memoryGraphics.ReleaseHdc( dc );
						img.Save( dirPath + "tmp_" + nSavedImgs + ".png" );
						img.Dispose();
						g.Dispose();


						++nSavedImgs;
					}
					catch( Exception ex )
					{
						MessageBox.Show( "Error trying to save temp image at" + dirPath + "\n" + ex.GetType().ToString() + ": " + ex.Message, "Could not Save Image" );
						return;
					}

					++replayData.CurrPacket;

					this.BeginInvoke( new MethodInvoker( delegate()
					{
						progbExport.Value = i;
						double percent = Convert.ToDouble( i ) / Convert.ToDouble( nPackets );
						Int32 aux = Convert.ToInt32( percent * 100 );
						lbExportProgress.Text = "Progresso da Exportação: " + ( aux > 100 ? 100 : aux ) + "% ( " + i + " / " + nPackets + " )";
					} ) );
				}

				CloseEmulatorProcess();

				this.Invoke( new MethodInvoker( delegate()
{
	SaveFileDialog sfdAvi = new SaveFileDialog();
	sfdAvi.DefaultExt = "avi";
	sfdAvi.InitialDirectory = Environment.GetFolderPath( Environment.SpecialFolder.CommonVideos );
	sfdAvi.FileName = "movie";
	sfdAvi.Filter = "AVI files|*.avi";
	sfdAvi.Title = "Selecione o local para salvar o arquivo AVI";
	if( sfdAvi.ShowDialog( this ) == DialogResult.OK )
	{
		string bleble = sfdAvi.FileName;

		AviWriter temp = new AviWriter();
		temp.Open( sfdAvi.FileName, 2, wndWidth, wndHeight );
		for( uint i = 0 ; i < nSavedImgs ; ++i )
		{
			Bitmap frameTonsert = new Bitmap( Bitmap.FromFile( dirPath + "tmp_" + i + ".png" ) );
			temp.AddFrame( frameTonsert );
			frameTonsert.Dispose();
		}
		temp.Close();
	}
	sfdAvi.Dispose();
} ) );
			}
			catch( Exception ex )
			{
				MessageBox.Show( ex.Message, "Unexpected Error" );
			}
			finally
			{
				if( hDC != IntPtr.Zero )
					Win32API.ReleaseDC( hWnd, hDC );

				RemoveTempScreenshots();

				OnReplayStop( btStopReplay, new EventArgs() );
			}
		}

		private void RemoveTempScreenshots()
		{
			string dirPath = Directory.GetCurrentDirectory() + Settings.Default.TEMP_DIR_PATH;
			if( Directory.Exists( dirPath ) )
				Directory.Delete( dirPath, true );
		}

		private static void LogData( RichTextBox log, DateTime timeStamp, Int32 CurPkt, byte aid, byte cursorX, byte cursorY, byte[] packetData, bool showNoDisplayFields, NoDisplayField[] noDisplayFields )
		{
			string textToAppend = timeStamp.ToString( "dd/MM/yyyy HH:mm:ss" ) + "  "
				+ CurPkt.ToString( "####0" )
				+ "\t", translatedAID = Utils.GetAIDStr( aid );

			if( translatedAID.Length > 0 )
				textToAppend += translatedAID;

			int line, column;
			Utils.GetLineAndColumn( cursorX, cursorY, out line, out column );
			textToAppend += "\t" + ( line == -1 ? "    " : ( "x = " + line.ToString( "000" ) ) ) + "\t" + ( column == -1 ? "   " : ( "y = " + column.ToString( "000" ) ) ) + "\n";

			if( packetData.Length > 0 && aid != 0x41 )
			{
				int totalBytes = packetData.Length - 2;
				for( int i = 8 ; i < totalBytes ; ++i )
				{
					if( packetData[ i ] == 0x11 )
					{
						try
						{
							i++;
							byte fieldCX = packetData[ i++ ];
							byte fieldCY = packetData[ i++ ];

							int j;
							for( j = i ; j < totalBytes ; ++j )
							{
								if( packetData[ j ] == 0x11 )
									break;
							}

							byte[] temp = new byte[ j - i ];
							Array.Copy( packetData, i, temp, 0, temp.Length );
							i = j - 1;

							Utils.GetLineAndColumn( fieldCX, fieldCY, out line, out column );
							textToAppend += "\t\t\t\tx = " + line.ToString( "000" ) + "\ty = " + column.ToString( "000" );

							int fieldIndex = ( Utils.EBCDICToDecimal( fieldCX ) * 64 ) + Utils.EBCDICToDecimal( fieldCY );
							if( ( noDisplayFields.Length == 0 ) || ( Array.FindIndex( noDisplayFields, field => field.index == fieldIndex ) == -1 ) || showNoDisplayFields )
								textToAppend += "\t" + Utils.ConvertEBCDICtoASCII( temp );

						}
						catch
						{
							i = totalBytes;
						}
						finally
						{
							textToAppend += "\n";
						}
					}
				}
			}

			log.BeginInvoke( new MethodInvoker( delegate()
			{
				log.Text += textToAppend;
				log.SelectionStart = log.Text.Length;
				log.ScrollToCaret();
				log.Refresh();
			} ) );
		}

		private static byte[] ChangeReadToWrite( byte[] packetData, out byte aid, out byte cursorX, out byte cursorY, bool found00 )
		{
			aid = 0;
			cursorX = 0;
			cursorY = 0;

			int offset = 5;
			if( !found00 )
			{
				offset = 0;
			}
			try
			{
				aid = packetData[ offset ];
				if( aid == 0x88 )
				{
					aid = 0x00;
					return null;
				}

				cursorX = packetData[ offset + 1 ];
				cursorY = packetData[ offset + 2 ];
				byte[] headerE = new byte[] { 0x00, 0x00, 0x00, 0x01, 0x03, 0x01, 0xC3 };
				byte[] headerN = new byte[] { 0x01, 0xC3 };

				Int32 resLength = packetData.Length + 3;

				byte[] res = new byte[ resLength ];
				if( found00 )
				{
					headerE.CopyTo( res, 0 );
				}
				else
				{
					headerN.CopyTo( res, 0 );
				}

				int i = headerE.Length;
				if( !found00 )
				{
					i = headerN.Length;
				}
				for( int j = ( offset + 3 ) ; j < ( packetData.Length - 2 ) ; ++i, ++j )
					res[ i ] = packetData[ j ];

				res[ i++ ] = 0x11;
				res[ i++ ] = cursorX;
				res[ i++ ] = cursorY;
				res[ i++ ] = 0x13;
				res[ i++ ] = 0xFF;
				res[ i++ ] = 0xEF;

				return res;
			}
			catch
			{
				return null;
			}
		}

		private static void ShowNoDisplayFields( byte[] packetData )
		{
			for( Int32 i = 0 ; i < packetData.Length ; ++i )
			{
				if( ( ( packetData[ i ] == TN3270_START_FIELD_BYTE ) && ( ( i + 1 ) < packetData.Length ) )
	|| ( ( ( i + 3 ) < packetData.Length )
			&& ( packetData[ i ] == TN3270_START_FIELD_EX_BYTE )
			&& ( packetData[ i + 2 ] == TN3270_START_FIELD_EX_CNTL ) )
	)
				{
					if( packetData[ i + 2 ] == TN3270_START_FIELD_EX_CNTL ) { i += 2; }
					++i;
					switch( packetData[ i ] )
					{
						case 0x4C:
							packetData[ i ] = 0xC8;
							break;

						case 0x4D:
							packetData[ i ] = 0xC9;
							break;

						case 0x5C:
							packetData[ i ] = 0xD8;
							break;

						case 0x5D:
							packetData[ i ] = 0xD9;
							break;

						case 0x6C:
							packetData[ i ] = 0xE8;
							break;

						case 0x6D:
							packetData[ i ] = 0xE9;
							break;
					}
				}
			}
		}

		private bool StartEmulator( EmulatorStyle style )
		{
			try
			{
				ProcessStartInfo psi = new ProcessStartInfo( Settings.Default.TN3270_EMULATOR_PATH );
				psi.CreateNoWindow = false;
				psi.WindowStyle = ProcessWindowStyle.Normal;
				psi.RedirectStandardOutput = false;
				psi.UseShellExecute = false;

				switch( style )
				{
					case EmulatorStyle.STYLE_27x132:
						psi.Arguments = "-zoi27x132";
						break;

					case EmulatorStyle.STYLE_32x80:
						psi.Arguments = "-zoi32x80";
						break;

					case EmulatorStyle.STYLE_43x80:
						psi.Arguments = "-zoi43x80";
						break;

					case EmulatorStyle.STYLE_24x80:
					default:
						psi.Arguments = "-zoi24x80";
						break;
				}

				unsafe
				{
					int ret;
					int func = WinHLLAPI.RESETSYSTEM;
					WinHLLAPI.HLLAPI( &func, null, null, &ret );
					if( ret > 0 )
					{
						MessageBox.Show( "Could not ResetSystem - HLLAPI" );
					};
					if( !WinHLLAPI.WinHLLAPICleanup() )
					{
						MessageBox.Show( "Could not Cleanup HLLAPI" );
					};


				}

				emulatorProcess = Process.Start( psi );
				return true;
			}
			catch( Exception ex )
			{
				MessageBox.Show( ex.Message, "Could not Start Emulator" );
				return false;
			}
		}

		private void OnReplayPause( object sender, EventArgs e )
		{
			lock( this )
			{
				if( IsPlayingReplay() )
					ReplayPaused = !ReplayPaused;
			}
		}

		unsafe private void CloseEmulatorProcess()
		{
			lock( this )
			{
				tn3270Server.CloseTcpClient();

				Thread.Sleep( 500 );

				if( emulatorProcess != null )
				{
					if( !emulatorProcess.HasExited )
					{
						int ret;
						int func = WinHLLAPI.RESETSYSTEM;
						WinHLLAPI.HLLAPI( &func, null, null, &ret );
						if( ret > 0 )
						{
							MessageBox.Show( "Could not ResetSystem - HLLAPI" );
						};
						if( !WinHLLAPI.WinHLLAPICleanup() )
						{
							MessageBox.Show( "Could not Cleanup HLLAPI" );
						};

						emulatorProcess.CloseMainWindow();
						emulatorProcess.Kill();
						emulatorProcess.WaitForExit();
					}

					emulatorProcess.Close();
					emulatorProcess.Dispose();
					emulatorProcess = null;
				}
			}
		}

		private void OnReplayStop( object sender, EventArgs e )
		{
			lock( this )
			{
				ReplayPaused = false;
				replayData = null;

				CloseEmulatorProcess();

				this.BeginInvoke( new MethodInvoker( delegate()
				{
					btPauseReplay.Text = "Pause";
				} ) );

				SetCurrentTab( tabSearch );
			}
		}

		private void OnFormClosing( object sender, FormClosingEventArgs e )
		{
			OnReplayStop( btStopReplay, new EventArgs() );
		}



		private void OnPacketFilterChanged( object sender, EventArgs e )
		{
			CheckBox ckb = ( CheckBox )sender;

			if( ckb == ckbPacketDate )
			{
				gbPacketDate.Enabled = ckb.Checked;
			}
			else if( ckb == ckbPacketDirection )
			{
				gbPacketDirection.Enabled = ckb.Checked;
			}
			else if( ckb == ckbPacketString )
			{
				tbPacketString.Enabled = ckb.Checked;
			}
		}

		private void OnSessionFilterChanged( object sender, EventArgs e )
		{
			CheckBox ckb = ( CheckBox )sender;

			if( ckb == ckbSessionClient )
			{
				tbIpClient.Enabled = ckbSessionClient.Checked;
			}
			else if( ckb == ckbSessionHost )
			{
				tbIpHost.Enabled = ckbSessionHost.Checked;
			}
			else if( ckb == ckbTerminalType )
			{
				tbTerminalType.Enabled = ckbTerminalType.Checked;
			}
			else if( ckb == ckbTerminalName )
			{
				tbTerminalName.Enabled = ckbTerminalName.Checked;
			}
			else if( ckb == ckbClientPort )
			{
				nudClientPort.Enabled = ckbClientPort.Checked;
			}
		}

		private void OnMenuOptExit( object sender, EventArgs e )
		{
			this.Close();
		}

		private void OnDBConfigChanged( bool configChanged, IPAddress ip, UInt16 port, string user, string password )
		{
			if( configChanged )
			{
				dbIP = ip;
				dbPort = port;
				dbUser = user;
				dbPassword = password;

				OnLoadDatabases();

				MySqlConnection connection = null;
				try
				{
					connection = GetMySQLConn( "" );
					connection.Open();
					connection.Close();

					bbmModeOn = dbPassword.EndsWith( "_bbm", StringComparison.OrdinalIgnoreCase );
				}
				catch
				{
				}
				finally
				{
					if( connection != null )
					{
						connection.Close();
						connection.Dispose();
					}
				}
			}
			this.Enabled = true;
		}

		private void OnMenuOptConfigBDConn( object sender, EventArgs e )
		{
			this.Enabled = false;

			FrmBDConfig temp = new FrmBDConfig( OnDBConfigChanged, dbIP, dbPort, dbUser, dbPassword );
			temp.Show( this );
		}

		private void OnShown( object sender, EventArgs e )
		{
			SetCurrentTab( tabSearch );

			OnLoadDatabases();
		}

		private void OnLoadDatabases()
		{
			MySqlConnection connection = null;
			MySqlDataAdapter dataAdapter = null;

			try
			{
				cbDatabases.Items.Clear();
				cbDatabases.Items.Add( "<Não há Bases de Dados>" );

				connection = GetMySQLConn( "" );
				connection.Open();

				dataAdapter = new MySqlDataAdapter( "SHOW DATABASES", connection );

				DataSet ds = new DataSet();
				dataAdapter.Fill( ds );

				connection.Close();


				if( ( ds.Tables.Count > 0 ) && ( ds.Tables[ 0 ].Rows.Count > 0 ) )
				{
					cbDatabases.Items.Clear();
					cbDatabases.Items.Add( "<Selecione a Base de Dados>" );
					foreach( DataRow row in ds.Tables[ 0 ].Rows )
						cbDatabases.Items.Add( row[ "Database" ] );

					bool foundDefaultDatabase = false;
					foreach( object obj in cbDatabases.Items )
					{
						if( String.Equals( ( string )obj, Settings.Default.DB_DATABASE, StringComparison.Ordinal ) )
						{
							foundDefaultDatabase = true;
							cbDatabases.SelectedItem = obj;
							break;
						}
					}

					if( !foundDefaultDatabase )
					{
						cbDatabases.SelectedIndex = 0;
					}
				}
			}
			catch( Exception ex )
			{
				cbDatabases.SelectedIndex = 0;
				MessageBox.Show( ex.Message, "Could not Load Databases" );
			}
			finally
			{
				if( dataAdapter != null )
				{
					dataAdapter.Dispose();
				}
				if( connection != null )
				{
					connection.Close();
					connection.Dispose();
				}
			}
		}

		private void OnDatabaseSelectedChanged( object sender, EventArgs e )
		{
			if( !String.Equals( dbDatabase, ( string )cbDatabases.Items[ cbDatabases.SelectedIndex ], StringComparison.OrdinalIgnoreCase ) )
			{
				dgResults.DataSource = null;

				if( cbDatabases.SelectedIndex > 0 )
					dbDatabase = ( string )cbDatabases.SelectedItem;
				else
					dbDatabase = "";
			}
		}

		private void OnIPTextBoxKeyDown( object sender, KeyEventArgs e )
		{
			if( !Utils.IsValidIPInput( e ) )
			{
				e.Handled = true;
				e.SuppressKeyPress = true;
			}
		}

		private void onReplayArrowStartDrag( object sender, MouseEventArgs e )
		{
			draggingReplayArrow = true;
			dragOffsetX = e.X;

			lock( this )
				ReplayPaused = true;
		}

		private void OnMouseMoved( object sender, MouseEventArgs e )
		{
			lock( this )
			{
				if( !IsPlayingReplay() )
					return;

				if( draggingReplayArrow && ( picbArrow.Cursor == Cursors.Hand ) )
				{
					int arrowHalfWidth = picbArrow.Size.Width >> 1;
					Rectangle collArea = new Rectangle( progbReplay.Location.X - arrowHalfWidth,
														progbReplay.Location.Y,
														progbReplay.Size.Width + picbArrow.Size.Width,
														progbReplay.Size.Height + ( picbArrow.Size.Height << 1 ) + DIST_FROM_REPLAY_BAR_TO_ARROW );

					if( gbReplayPanel.RectangleToScreen( collArea ).Contains( e.Location ) )
					{
						Point nextPos = new Point( gbReplayPanel.PointToClient( e.Location ).X - dragOffsetX, picbArrow.Location.Y );

						if( nextPos.X < collArea.X )
						{
							nextPos.X = collArea.X;
						}
						else
						{
							int progBarEnd = progbReplay.Location.X + progbReplay.Size.Width - arrowHalfWidth;
							if( nextPos.X > progBarEnd )
								nextPos.X = progBarEnd;
						}

						if( nextPos.X != picbArrow.Location.X )
						{
							picbArrow.Location = nextPos;
							replayData.CurrPacket = ( Int32 )( replayData.NPackets * ( ( double )nextPos.X / ( double )progbReplay.Size.Width ) );
						}
					}
					else
					{
						picbArrow.Cursor = Cursors.Arrow;

						SetReplayAtCurrPacket();
						ReplayPaused = false;
					}
				}
			}
		}

		private void onReaplayArrowDrop( object sender, MouseEventArgs e )
		{
			draggingReplayArrow = false;
			picbArrow.Cursor = Cursors.Hand;

			lock( this )
			{
				SetReplayAtCurrPacket();
				ReplayPaused = false;
			}
		}

		private void SetReplayAtCurrPacket()
		{
			EmptyLog();

			Int32 finalPacket = replayData.CurrPacket;

			bool found00 = isExtended();

			Int32 j = 10;

			if( finalPacket > 20 ) j = finalPacket - 10;

			for( Int32 i = j ; i < finalPacket ; ++i )
			{
				replayData.CurrPacket = i;
				ReplayPacket( replayData, tn3270Server.GetTcpClientStream(), rtbActions, bbmModeOn, found00 );
			}
		}

		private void EmptyLog()
		{
			this.BeginInvoke( new MethodInvoker( delegate()
			{
				rtbActions.Text = "";
				rtbActions.SelectionStart = rtbActions.Text.Length;
				rtbActions.ScrollToCaret();
				rtbActions.Refresh();
			} ) );
		}

		private void OnReplayDelayValueChanged( object sender, EventArgs e )
		{
			framesDelayMS = ( int )nudReplayDelay.Value;
		}

		private void OnMenuOptConfigDefaultValues( object sender, EventArgs e )
		{
			this.Enabled = false;

			FormAppSettings temp = new FormAppSettings();
			temp.Show( this );
		}

		private void OnSkipToFrame( object sender, EventArgs e )
		{
			if( cbPacketsStrMatch.SelectedIndex > 0 )
			{
				int curpk = 0;
				for( curpk = 0 ; curpk <= replayData.NPackets - 1 ; curpk++ )
				{
					String tempotab = Utils.EpochTimeToLocalTime( ( double )replayData.PacketsTable.Rows[ curpk ][ "EpochTime" ] ).ToString( "dd/MM/yyyy HH:mm:ss" );
					String temposel = cbPacketsStrMatch.SelectedItem.ToString();
					if( string.Equals( temposel, tempotab ) )
					{
						break;
					}
				}

				if( curpk > 5 ) curpk -= 4;
				replayData.CurrPacket = Convert.ToInt32( curpk );
				SetReplayAtCurrPacket();
				ReplayPaused = false;
			}
		}



	}
}
