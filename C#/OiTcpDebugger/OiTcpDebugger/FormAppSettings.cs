﻿using System.Windows.Forms;
using OiTN3270Debugger.Properties;
using System.Xml;
using System;
using System.Net;
using System.IO;

namespace OiTN3270Debugger
{
	public partial class FormAppSettings : Form
	{
		// Endereço do banco de dados
		private IPAddress dbIP;

		public FormAppSettings()
		{
			// Inicializa a interface gráfica
			InitializeComponent();

			// Esses dois valores não mudam nunca
			nudReplayDelay.Minimum = Settings.Default.REPLAY_MIN_FRAMES_DELAY;
			nudReplayDelay.Maximum = Settings.Default.REPLAY_MAX_FRAMES_DELAY;

			// Carrega os valores de configuração atuais
			nudReplayDelay.Value = Settings.Default.REPLAY_FRAMES_DELAY_MS;

			tbEmulatorPath.Text = Settings.Default.TN3270_EMULATOR_PATH;
			cbSessionID.SelectedItem = Settings.Default.HLLAPI_SESSION_ID.ToString();

			tbDatabase.Text = Settings.Default.DB_DATABASE;
			tbDBUser.Text = Settings.Default.DB_USER;
			nudDBPort.Value = Settings.Default.DB_PORT;
			nudMaxQueryResults.Value = Settings.Default.MAX_QUERY_RESULTS;

			if( String.Equals( Settings.Default.DB_SERVER, "localhost", StringComparison.OrdinalIgnoreCase ) )
				dbIP = new IPAddress( new byte[] { 127, 0, 0, 1 } );
			else
				dbIP = IPAddress.Parse( Settings.Default.DB_SERVER );

			tbDBIp.Text = dbIP.ToString();
		}

		private void OnIPTextBoxKeyDown( object sender, KeyEventArgs e )
		{
			if( !Utils.IsValidIPInput( e ) )
			{
				e.Handled = true;
				e.SuppressKeyPress = true;
			}
		}

		private void OnCancel( object sender, System.EventArgs e )
		{
			Close();
		}

		private void OnApply( object sender, System.EventArgs e )
		{
			// Verifica se os campos obrigatórios foram preenchidos
			if( ( tbDBIp.Text.Length == 0 ) || ( tbEmulatorPath.Text.Length == 0 ) )
			{
				MessageBox.Show( this, "Nem todos os campos obrigatórios (*) foram preenchidos", "Campo não Preenchido" );
				return;
			}

			// Verifica se a path é válida (o usuário pode ter digitado ao invés de selecionado pelo OpenFileDialog)
			if( !File.Exists( tbEmulatorPath.Text ) )
			{
				MessageBox.Show( this, "O emulador informado não existe", "Caminho Inválido" );
				return;
			}

			// Verifica se o IP fornecido é válido
			try
			{
				IPAddress temp = IPAddress.Parse( tbDBIp.Text );
			}
			catch
			{
				MessageBox.Show( this, "O valor fornecido não é um endereço IP válido", "IP Inválido" );
				return;
			}

			Settings.Default.TN3270_EMULATOR_PATH = tbEmulatorPath.Text;
			Settings.Default.HLLAPI_SESSION_ID = cbSessionID.SelectedItem.ToString()[0];

			Settings.Default.DB_USER = tbDBUser.Text;
			Settings.Default.DB_SERVER = tbDBIp.Text;
			Settings.Default.DB_PORT = Convert.ToUInt16( nudDBPort.Value );
			Settings.Default.DB_DATABASE = tbDatabase.Text;

			Settings.Default.REPLAY_FRAMES_DELAY_MS = Convert.ToInt32( nudReplayDelay.Value );

			Settings.Default.MAX_QUERY_RESULTS = Convert.ToUInt16( nudMaxQueryResults.Value );

			Settings.Default.Save();
			Settings.Default.Reload();

			// Fecha o formulário
			Close();
		}

		private void OnFormClosed( object sender, FormClosedEventArgs e )
		{
			Owner.Enabled = true;
		}

		private void OnSearchPath( object sender, EventArgs e )
		{
			Enabled = false;

			if( File.Exists( tbEmulatorPath.Text ) )
				fileDialogEmulator.InitialDirectory = tbEmulatorPath.Text.Substring( 0, tbEmulatorPath.Text.LastIndexOf( '\\' ) );
			else
				fileDialogEmulator.InitialDirectory = Environment.GetFolderPath( Environment.SpecialFolder.Desktop );

			if( fileDialogEmulator.ShowDialog() == DialogResult.OK )
				tbEmulatorPath.Text = fileDialogEmulator.FileName;

			Enabled = true;
		}

		private void FormAppSettings_Load( object sender, EventArgs e )
		{

		}

		private void fileDialogEmulator_FileOk( object sender, System.ComponentModel.CancelEventArgs e )
		{

		}
	}
}
