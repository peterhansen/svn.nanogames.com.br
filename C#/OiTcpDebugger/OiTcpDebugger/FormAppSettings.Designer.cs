﻿namespace OiTN3270Debugger
{
	partial class FormAppSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( FormAppSettings ) );
			this.tbEmulatorPath = new System.Windows.Forms.TextBox();
			this.tbDBUser = new System.Windows.Forms.TextBox();
			this.tbDatabase = new System.Windows.Forms.TextBox();
			this.btCancel = new System.Windows.Forms.Button();
			this.btApply = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.cbSessionID = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.nudDBPort = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.tbDBIp = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.nudReplayDelay = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.nudMaxQueryResults = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.fileDialogEmulator = new System.Windows.Forms.OpenFileDialog();
			this.groupBox1.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.nudDBPort ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.nudReplayDelay ) ).BeginInit();
			this.groupBox2.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.nudMaxQueryResults ) ).BeginInit();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// tbEmulatorPath
			// 
			this.tbEmulatorPath.Location = new System.Drawing.Point( 8, 41 );
			this.tbEmulatorPath.Name = "tbEmulatorPath";
			this.tbEmulatorPath.Size = new System.Drawing.Size( 314, 20 );
			this.tbEmulatorPath.TabIndex = 0;
			// 
			// tbDBUser
			// 
			this.tbDBUser.Location = new System.Drawing.Point( 9, 42 );
			this.tbDBUser.Name = "tbDBUser";
			this.tbDBUser.Size = new System.Drawing.Size( 253, 20 );
			this.tbDBUser.TabIndex = 0;
			// 
			// tbDatabase
			// 
			this.tbDatabase.Location = new System.Drawing.Point( 9, 91 );
			this.tbDatabase.Name = "tbDatabase";
			this.tbDatabase.Size = new System.Drawing.Size( 253, 20 );
			this.tbDatabase.TabIndex = 3;
			// 
			// btCancel
			// 
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.Location = new System.Drawing.Point( 397, 282 );
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size( 75, 29 );
			this.btCancel.TabIndex = 3;
			this.btCancel.Text = "&Cancelar";
			this.btCancel.UseVisualStyleBackColor = true;
			this.btCancel.Click += new System.EventHandler( this.OnCancel );
			// 
			// btApply
			// 
			this.btApply.Location = new System.Drawing.Point( 478, 282 );
			this.btApply.Name = "btApply";
			this.btApply.Size = new System.Drawing.Size( 75, 29 );
			this.btApply.TabIndex = 4;
			this.btApply.Text = "&Aplicar";
			this.btApply.UseVisualStyleBackColor = true;
			this.btApply.Click += new System.EventHandler( this.OnApply );
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add( this.button1 );
			this.groupBox1.Controls.Add( this.label7 );
			this.groupBox1.Controls.Add( this.cbSessionID );
			this.groupBox1.Controls.Add( this.label1 );
			this.groupBox1.Controls.Add( this.tbEmulatorPath );
			this.groupBox1.Location = new System.Drawing.Point( 5, 2 );
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size( 548, 72 );
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Emulador";
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button1.Location = new System.Drawing.Point( 328, 35 );
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size( 75, 29 );
			this.button1.TabIndex = 1;
			this.button1.Text = "&Buscar";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler( this.OnSearchPath );
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point( 420, 24 );
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size( 71, 13 );
			this.label7.TabIndex = 13;
			this.label7.Text = "ID da Sessão";
			// 
			// cbSessionID
			// 
			this.cbSessionID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbSessionID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbSessionID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSessionID.FormattingEnabled = true;
			this.cbSessionID.Items.AddRange( new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"} );
			this.cbSessionID.Location = new System.Drawing.Point( 423, 40 );
			this.cbSessionID.Name = "cbSessionID";
			this.cbSessionID.Size = new System.Drawing.Size( 121, 21 );
			this.cbSessionID.Sorted = true;
			this.cbSessionID.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 5, 24 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 165, 13 );
			this.label1.TabIndex = 3;
			this.label1.Text = "Caminho do Emulador TN3270 (*)";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point( 6, 26 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 141, 13 );
			this.label2.TabIndex = 4;
			this.label2.Text = "Usuário do Banco de Dados";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point( 6, 75 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 117, 13 );
			this.label3.TabIndex = 5;
			this.label3.Text = "Base de Dados Padrão";
			// 
			// nudDBPort
			// 
			this.nudDBPort.Location = new System.Drawing.Point( 455, 42 );
			this.nudDBPort.Maximum = new decimal( new int[] {
            65535,
            0,
            0,
            0} );
			this.nudDBPort.Name = "nudDBPort";
			this.nudDBPort.Size = new System.Drawing.Size( 89, 20 );
			this.nudDBPort.TabIndex = 2;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point( 452, 26 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 92, 13 );
			this.label4.TabIndex = 7;
			this.label4.Text = "Porta da Conexão";
			// 
			// tbDBIp
			// 
			this.tbDBIp.Location = new System.Drawing.Point( 278, 42 );
			this.tbDBIp.MaxLength = 15;
			this.tbDBIp.Name = "tbDBIp";
			this.tbDBIp.Size = new System.Drawing.Size( 159, 20 );
			this.tbDBIp.TabIndex = 1;
			this.tbDBIp.KeyDown += new System.Windows.Forms.KeyEventHandler( this.OnIPTextBoxKeyDown );
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point( 275, 26 );
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size( 164, 13 );
			this.label5.TabIndex = 9;
			this.label5.Text = "Endereço do Banco de Dados (*)";
			// 
			// nudReplayDelay
			// 
			this.nudReplayDelay.Location = new System.Drawing.Point( 8, 42 );
			this.nudReplayDelay.Maximum = new decimal( new int[] {
            2000,
            0,
            0,
            0} );
			this.nudReplayDelay.Minimum = new decimal( new int[] {
            50,
            0,
            0,
            0} );
			this.nudReplayDelay.Name = "nudReplayDelay";
			this.nudReplayDelay.Size = new System.Drawing.Size( 89, 20 );
			this.nudReplayDelay.TabIndex = 0;
			this.nudReplayDelay.Value = new decimal( new int[] {
            1000,
            0,
            0,
            0} );
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point( 6, 26 );
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size( 173, 13 );
			this.label6.TabIndex = 11;
			this.label6.Text = "Delay Entre Frames (milissegundos)";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add( this.nudMaxQueryResults );
			this.groupBox2.Controls.Add( this.label8 );
			this.groupBox2.Controls.Add( this.label2 );
			this.groupBox2.Controls.Add( this.tbDatabase );
			this.groupBox2.Controls.Add( this.label5 );
			this.groupBox2.Controls.Add( this.tbDBUser );
			this.groupBox2.Controls.Add( this.tbDBIp );
			this.groupBox2.Controls.Add( this.label3 );
			this.groupBox2.Controls.Add( this.label4 );
			this.groupBox2.Controls.Add( this.nudDBPort );
			this.groupBox2.Location = new System.Drawing.Point( 5, 80 );
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size( 548, 119 );
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Conexão com o Banco de Dados";
			// 
			// nudMaxQueryResults
			// 
			this.nudMaxQueryResults.Location = new System.Drawing.Point( 278, 91 );
			this.nudMaxQueryResults.Maximum = new decimal( new int[] {
            65535,
            0,
            0,
            0} );
			this.nudMaxQueryResults.Name = "nudMaxQueryResults";
			this.nudMaxQueryResults.Size = new System.Drawing.Size( 89, 20 );
			this.nudMaxQueryResults.TabIndex = 11;
			this.nudMaxQueryResults.Value = new decimal( new int[] {
            1000,
            0,
            0,
            0} );
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point( 275, 75 );
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size( 205, 13 );
			this.label8.TabIndex = 10;
			this.label8.Text = "Número Máximo de Resultados por Busca";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add( this.label6 );
			this.groupBox3.Controls.Add( this.nudReplayDelay );
			this.groupBox3.Location = new System.Drawing.Point( 5, 205 );
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size( 548, 71 );
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Replay";
			// 
			// fileDialogEmulator
			// 
			this.fileDialogEmulator.DefaultExt = "exe";
			this.fileDialogEmulator.Filter = "Executable Files|*.exe|All Files|*.*";
			this.fileDialogEmulator.Title = "Selecione o Emulador";
			this.fileDialogEmulator.FileOk += new System.ComponentModel.CancelEventHandler( this.fileDialogEmulator_FileOk );
			// 
			// FormAppSettings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size( 559, 317 );
			this.Controls.Add( this.groupBox3 );
			this.Controls.Add( this.groupBox2 );
			this.Controls.Add( this.groupBox1 );
			this.Controls.Add( this.btCancel );
			this.Controls.Add( this.btApply );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.MaximizeBox = false;
			this.Name = "FormAppSettings";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Configurações Padrão";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler( this.OnFormClosed );
			this.Load += new System.EventHandler( this.FormAppSettings_Load );
			this.groupBox1.ResumeLayout( false );
			this.groupBox1.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.nudDBPort ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.nudReplayDelay ) ).EndInit();
			this.groupBox2.ResumeLayout( false );
			this.groupBox2.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.nudMaxQueryResults ) ).EndInit();
			this.groupBox3.ResumeLayout( false );
			this.groupBox3.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.TextBox tbEmulatorPath;
		private System.Windows.Forms.TextBox tbDBUser;
		private System.Windows.Forms.TextBox tbDatabase;
		private System.Windows.Forms.Button btCancel;
		private System.Windows.Forms.Button btApply;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown nudDBPort;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbDBIp;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown nudReplayDelay;
		private System.Windows.Forms.ComboBox cbSessionID;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.OpenFileDialog fileDialogEmulator;
		private System.Windows.Forms.NumericUpDown nudMaxQueryResults;
		private System.Windows.Forms.Label label8;
	}
}