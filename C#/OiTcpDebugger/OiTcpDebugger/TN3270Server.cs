﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Windows.Forms;

namespace OiTN3270Debugger
{
	public delegate void OnServerRunningCallbackType( object param );
	public delegate void OnConnEstabilishedCallbackType( object param );

    class TN3270Server
    {
		        private TcpClient tcpClient;
		        private TcpListener tcpListener;
				private IPAddress _ip;
				private Int32 _port;
		        private Thread listenThread;
				private ManualResetEvent tcpClientConnected;
				private OnServerRunningCallbackType serverRunningCallback;
				private object serverRunningCallbackParam;
				private OnConnEstabilishedCallbackType connectionCallback;
				private object connectionCallbackParam;
        public TN3270Server( IPAddress ip, Int32 port, OnServerRunningCallbackType serverCallback, object serverCallbackParam, OnConnEstabilishedCallbackType connCallback, object connCallbackParam )
        {
            tcpClient = null;
            tcpListener = null;
            listenThread = null;
			tcpClientConnected = new ManualResetEvent( false );
            _ip = ip;
            _port = port;
			serverRunningCallback = serverCallback;
			serverRunningCallbackParam = serverCallbackParam;
			connectionCallback = connCallback;
			connectionCallbackParam = connCallbackParam;
        }

        public void Start()
        {
            Stop();
            listenThread = new Thread( new ThreadStart( ListenForClients ) );
            listenThread.Start();
        }

        public void Stop()
        {
            if( tcpListener != null )
            {
                tcpListener.Stop();
                tcpListener = null;
            }

            if( listenThread != null )
            {
                if( listenThread.IsAlive )
                    listenThread.Abort();
                listenThread = null;
            }
        }

		public void CloseTcpClient()
		{
			if( tcpClient != null )
			{
				tcpClient.Close();
				tcpClient = null;
			}
		}

		public NetworkStream GetTcpClientStream()
		{
			return tcpClient == null ? null : tcpClient.GetStream();
		}

		public void SetConnEstablishedCallback( OnConnEstabilishedCallbackType callback, object param = null )
		{
			connectionCallback = callback;
			connectionCallbackParam = param;
		}

		public void SetServerStartedCallback( OnServerRunningCallbackType callback, object param = null )
		{
			serverRunningCallback = callback;
			serverRunningCallbackParam = param;
		}

		private void ListenForClients()
		{
						if( tcpListener != null )
				tcpListener.Stop();
						tcpClientConnected.Reset();
						tcpListener = new TcpListener( _ip, _port );
			try
			{
				tcpListener.Start( 0 );
				tcpListener.BeginAcceptTcpClient( onClientConnectionAttempt, this );
								serverRunningCallback( serverRunningCallbackParam );
								tcpClientConnected.WaitOne();
				connectionCallback( connectionCallbackParam );
			}
			catch( SocketException ex )
			{
				MessageBox.Show( "Socket Exception\n\n" + ex.Message, "TCP Error" );
			}
			catch( Exception ex )
			{
				MessageBox.Show( "Could not establish TCP connection\n\n" + ex.Message, "TCP Error" );
			}
		}

				private static void onClientConnectionAttempt( IAsyncResult ar )
		{
						TN3270Server tn3270Server = ( TN3270Server )ar.AsyncState;
						tn3270Server.tcpClient = tn3270Server.tcpListener.EndAcceptTcpClient( ar );
						tn3270Server.tcpClientConnected.Set();
		}
    }
}
