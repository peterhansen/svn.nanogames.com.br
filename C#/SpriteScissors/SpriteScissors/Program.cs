using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Collections;
using System.Runtime.InteropServices;
using SpriteScissors.Properties;
using System.Diagnostics;
using System.Threading;

namespace SpriteScissors {
    class Program {
        // Fun��o de entrada do programa
        static void Main( string[] args ) {
            #region Exibe a ajuda do programa caso n�o tenha recebido par�metros

            if( args.Length == 0 ) {
                PrintHelp();
                return;
            }

            #endregion

            // Faz o parse dos par�metros recebidos pela linha de comando
            int nFrames;
            bool createTexture, ESTOU_LENDO_FONTES_NINJA, POTENCIA_DE_2;
            string imagePath, outputPath, offsetFilePath, fontChars;

            Color colorKey;
            ImageFormat selectedFormat;

            //TODO tirar for�a��o de barra
            //args = new string[] { "-i", "font_0.png", "-t", "-c" };
            ReadParams( ref args, out imagePath, out nFrames, out colorKey, out outputPath, out selectedFormat, out offsetFilePath, out createTexture, out fontChars, out ESTOU_LENDO_FONTES_NINJA, out POTENCIA_DE_2 );

            // Verifica se os par�metros obrigat�rios foram informados

            // Arquivo de entrada
            Bitmap image;

            string path;
            string imageName;

            #region Verifica se o usu�rio passou a path de uma imagem v�lida

            if( imagePath == null ) {
                Console.WriteLine( "Voc� deve especificar o arquivo de entrada" );
                return;
            }

            try {
                if( !File.Exists( imagePath ) ) {
                    Console.WriteLine( "O arquivo de entrada especificado � inexistente" );
                    return;
                }

                int lastBarIndex = imagePath.LastIndexOf( '\\' ) + 1;
                path = imagePath.Substring( 0, lastBarIndex );
                imageName = imagePath.Substring( lastBarIndex );
                imageName = imageName.Remove( imageName.LastIndexOf( '.' ) );
            } catch( Exception ex ) {
                Console.WriteLine( "N�o foi poss�vel reconhecer " + imagePath + " como o nome de um arquivo\n" + ex.GetType() + ": " + ex.Message );
                return;
            }

            // Se n�o forneceu a path da imagem de sa�da
            if( outputPath == null )
                outputPath = path;

            if( offsetFilePath == null )
                offsetFilePath = outputPath;

            #region Carrega a imagem especificada

            try {
                image = (Bitmap)Bitmap.FromFile( imagePath );
            } catch( Exception ex ) {
                Console.WriteLine( "N�o foi poss�vel carregar a imagem " + imagePath + "\n" + ex.GetType().ToString() + ": " + ex.Message );
                return;
            }

            string temp = "__spriteScissorsTemp.bmp";
            image.Save( temp, ImageFormat.Bmp );
            byte[] imageBytes = File.ReadAllBytes( temp );
            File.Delete( temp );

            #endregion

            #endregion

            #region Verifica se o n�mero de frames � v�lido

            if( ESTOU_LENDO_FONTES_NINJA ) {
                Console.WriteLine( "Gerador de fontes" );

                // Cria uma array auxiliar para conter as cores existentes em um frame
                ArrayList paletteColors = new ArrayList();

                // Cria um array auxiliar para conter os dados que ser�o escritos no arquivo
                ArrayList descFileEntries = new ArrayList();

                // Cria um array auxiliar para conter as paths das imagens geradas
                ArrayList framesImgsPaths = new ArrayList();

                // Percorre os frames da imagem
                TextWriter txtWriter = null;
                int textureWidth = 0, textureHeight = 0, maxCharWidth = 0, maxCharHeight = image.Height;
                string offsetFileName = offsetFilePath + imageName + ".bin";

                try {
                    txtWriter = File.CreateText( offsetFileName );

                    if( fontChars != null ) txtWriter.WriteLine( fontChars ); // Escreve os caracteres contidos na fonte 

                    int totalChars = ( fontChars != null ) ? fontChars.Length : Int32.MaxValue;
                    int readedChars = 0;

                    for( int accX = 0; ( accX < image.Width ) && ( readedChars < totalChars ); accX++ ) {
                        int minY = Int32.MaxValue, maxY = 0;

                        //find first valid line
                        bool finded = false;
                        int firstPixel = accX;
                        for( ; firstPixel < image.Width && !finded; firstPixel++ ) {
                            for( int accY = 0; accY < image.Height; accY++ ) {
                                if( image.GetPixel( firstPixel, accY ).A > 0 ) {
                                    finded = true;
                                    if( minY > accY ) minY = accY;
                                    if( maxY < accY ) maxY = accY;
                                }
                            }
                        }

                        //look until have valid line
                        for( accX = firstPixel; accX < image.Width && finded; accX++ ) {
                            bool lineHasValidPixel = false;
                            for( int accY = 0; accY < image.Height; accY++ ) {
                                if( image.GetPixel( accX, accY ).A > 0 ) {
                                    lineHasValidPixel = true;
                                    if( minY > accY ) minY = accY;
                                    if( maxY < accY ) maxY = accY;
                                }
                            }
                            if( !lineHasValidPixel ) break;
                        }

                        if( accX != firstPixel ) {
                            Rectangle area = new Rectangle( firstPixel, minY, ( ( accX - 1 ) - firstPixel ), ( maxY - minY ) );

                            // Calcula as dimens�es que ser�o necess�rias para uma poss�vel gera��o de textura
                            textureWidth += area.Width;
                            if( area.Height > textureHeight )  textureHeight = area.Height;

                            if( area.Width > maxCharWidth ) maxCharWidth = area.Width;
                            if( area.Height > maxCharHeight ) maxCharHeight = area.Height;

                            Bitmap scissoredFrame = new Bitmap( ( ( accX - 1 ) - firstPixel ), ( maxY - minY ), image.PixelFormat );
                            for( int x = 0; x < area.Width; x++ ) {
                                for( int y = 0; y < area.Height; y++ ) {
                                    scissoredFrame.SetPixel( x, y, image.GetPixel( (area.X + x), (area.Y + y) ) );
                                }
                            }

                            // Salva o bitmap criado
                            string saveName;
                            if( nFrames == 1 ) {
                                saveName = outputPath + imageName;

                                // J� que poderemos estar sobrescrevendo a imagem original, cancela a refer�ncia para
                                // a mesma, sen�o ir� disparar uma exce��o
                                image.Dispose();
                            } else {
                                saveName = outputPath + imageName + @"\" + imageName + "_" + readedChars;
                            }

                            if( !Directory.Exists( outputPath + imageName ) ) Directory.CreateDirectory( outputPath + imageName );
                            saveName += "." + selectedFormat.ToString().ToLower();

                            scissoredFrame.Save( saveName, selectedFormat );

                            Console.WriteLine( "Generate " + selectedFormat.ToString() );
                            framesImgsPaths.Add( saveName );

                            // Armazena o texto que dever� ser escrito no arquivo descritor de offsets
                            descFileEntries.Add( area.Y.ToString() );

                            // Limpa o array auxiliar
                            paletteColors.Clear();

                            // Fornece um feedback para o usu�rio
                            Console.WriteLine( "Char em " + area.X + " de tamanho " + area.Width + ": Sucesso - Total de Cores: " + scissoredFrame.Palette.Entries.Length );

                            readedChars++;
                        }
                    }

                    //txtWriter.WriteLine( readedChars.ToString() );
                    txtWriter.WriteLine( maxCharWidth + " " + maxCharHeight );
                    txtWriter.WriteLine( ( maxCharWidth / 3 ) );

                    // Preenche o arquivo descritor
                    if( createTexture ) {
                        // Fornece um feedback para o usu�rio
                        Console.WriteLine( "Gerando a textura e seu arquivo descritor" );

                        // TODO string textureName = outputPath + imageName + "_tex.png";
                        string textureName = outputPath + imageName + "_tex." + selectedFormat.ToString().ToLower();
                        Bitmap texture = CreateTexture( txtWriter, framesImgsPaths, descFileEntries, textureWidth, textureHeight, colorKey, POTENCIA_DE_2, short.MaxValue );
                        texture.Save( textureName, selectedFormat );

                        // TODO
                        //texture.Save( textureName, ImageFormat.Png );

                        //// Melhora a compress�o do PNG utilizando o programa PNGOut
                        //if( selectedFormat == ImageFormat.Png )
                        //    RunPngOut( textureName, "c6" );
                    } else {
                        // Fornece um feedback para o usu�rio
                        Console.WriteLine( "Gerando o arquivo descritor dos frames" );

                        for( int i = 0; i < descFileEntries.Count; ++i )
                            txtWriter.WriteLine( descFileEntries[ i ] );
                    }
                    descFileEntries.Clear();
                    framesImgsPaths.Clear();

                    // Fecha o arquivo descritor de offsets
                    txtWriter.Close();

                    // Informa que tudo ocorreu bem
                    Console.WriteLine( "Opera��o realizada com sucesso" );
                } catch( Exception ex ) {
                    // N�o cria o arquivo descritor em casos de erro
                    if( txtWriter != null ) {
                        txtWriter.Close();
                        File.Delete( offsetFileName );
                    }

                    // Exibe o erro para o usu�rio
                    Console.WriteLine( "Erro durante o processamento de dados\n" + ex.GetType() + ": " + ex.Message );
                } finally {
                    // Libera a imagem original
                    image.Dispose();
                }

            } else {

                if( nFrames == 0 ) {
                    Console.WriteLine( "Voc� deve especificar o n�mero de frames da imagem" );
                    return;
                }

                // Verifica se o tamanho dos frames est� proporcional
                if( image.Width % nFrames != 0 ) {
                    Console.WriteLine( "Os frames da imagem " + imagePath + " n�o s�o proporcionais" );
                    return;
                }

            #endregion

                // Recorta os frames ( TODO : Threads ? )
                int frameWidth = image.Width / nFrames;

                // Cria uma array auxiliar para conter as cores existentes em um frame
                ArrayList paletteColors = new ArrayList();

                // Cria um array auxiliar para conter os dados que ser�o escritos no arquivo
                ArrayList descFileEntries = new ArrayList();

                // Cria um array auxiliar para conter as paths das imagens geradas
                ArrayList framesImgsPaths = new ArrayList();

                // Percorre os frames da imagem
                TextWriter txtWriter = null;
                int textureWidth = 0, textureHeight = 0;
                string offsetFileName = offsetFilePath + imageName + ".bin";

                try {
                    // Cria o arquivo descritor de offsets
                    txtWriter = File.CreateText( offsetFileName );

                    // Escreve os caracteres contidos na fonte
                    if( fontChars != null )
                        txtWriter.WriteLine( fontChars );

                    // Escreve o tamanho original do frame
                    txtWriter.WriteLine( frameWidth.ToString() + " " + image.Height.ToString() );

                    // Escreve o n�mero de frames da imagem
                    txtWriter.WriteLine( nFrames.ToString() );

                    // Recorta os frames da imagem
                    for( int currFrame = 0; currFrame < nFrames; ++currFrame ) {
                        // Calcula o offset do frame atual
                        int offset = currFrame * frameWidth;

                        // Obt�m a �rea do frame que deve ser recortada
                        Rectangle area;
                        GetScissoredArea( imageBytes, image.Width, image.Height, colorKey, offset, frameWidth, paletteColors, out area );

                        // Calcula as dimens�es que ser�o necess�rias para uma poss�vel gera��o de textura
                        textureWidth += area.Width;
                        if( area.Height > textureHeight )
                            textureHeight = area.Height;

                        // Verifica se a cor de transpar�ncia est� na �rea calculada
                        Bitmap scissoredFrame;
                        bool hasColorKey = false;
                        if( !createTexture ) {

                            if( hasColorKey = ColorKeySearch( image, colorKey, area ) )
                                AddPaletteColor( paletteColors, Color.FromArgb( 0, colorKey ) );

                            scissoredFrame = BitmapMaker.CreatePalettedBitmapFrom( image, area, paletteColors, colorKey );
                        } else {
                            scissoredFrame = BitmapMaker.CreateTrueColorBitmapFrom( image, area, colorKey );
                        }

                        /* Bug do .Net - Transforma em imagem true color al�m de n�o manter a cor de transpar�ncia correta...
                        if( hasColorKey )
                            scissoredFrame.MakeTransparent( colorKey );*/

                        // Salva o bitmap criado
                        string saveName;
                        if( nFrames == 1 ) {
                            saveName = outputPath + imageName;

                            // J� que poderemos estar sobrescrevendo a imagem original, cancela a refer�ncia para
                            // a mesma, sen�o ir� disparar uma exce��o
                            image.Dispose();
                        } else {
                            saveName = outputPath + imageName + "_" + currFrame;
                        }

                        scissoredFrame.Save( saveName + ".bmp", ImageFormat.Bmp );

                        Console.WriteLine( "Generate " + selectedFormat.ToString() );
                        if( ( selectedFormat == ImageFormat.Png ) && !createTexture ) {
                            // Converte o BMP para PNG utilizando o programa PNGOut
                            ToPNG( ref saveName, hasColorKey );

                            // Armazena a path da imagem
                            framesImgsPaths.Add( saveName + ".png" );
                        } else {
                            // Armazena a path da imagem
                            framesImgsPaths.Add( saveName + ".bmp" );
                        }

                        // Armazena o texto que dever� ser escrito no arquivo descritor de offsets
                        descFileEntries.Add( ( area.X - offset ).ToString() + " " + area.Y.ToString() );

                        // Limpa o array auxiliar
                        paletteColors.Clear();

                        // Fornece um feedback para o usu�rio
                        Console.WriteLine( "Frame " + currFrame + ": Sucesso - Total de Cores: " + scissoredFrame.Palette.Entries.Length );
                    } // fim for

                    // Preenche o arquivo descritor
                    if( createTexture ) {
                        // Fornece um feedback para o usu�rio
                        Console.WriteLine( "Gerando a textura e seu arquivo descritor" );

                        // TODO string textureName = outputPath + imageName + "_tex.png";
                        string textureName = outputPath + imageName + "_tex.bmp";
                        Bitmap texture = CreateTexture( txtWriter, framesImgsPaths, descFileEntries, textureWidth, textureHeight, colorKey, POTENCIA_DE_2 );
                        texture.Save( textureName, ImageFormat.Bmp );

                        // TODO
                        //texture.Save( textureName, ImageFormat.Png );

                        //// Melhora a compress�o do PNG utilizando o programa PNGOut
                        //if( selectedFormat == ImageFormat.Png )
                        //    RunPngOut( textureName, "c6" );
                    } else {
                        // Fornece um feedback para o usu�rio
                        Console.WriteLine( "Gerando o arquivo descritor dos frames" );

                        for( int i = 0; i < descFileEntries.Count; ++i )
                            txtWriter.WriteLine( descFileEntries[ i ] );
                    }
                    descFileEntries.Clear();
                    framesImgsPaths.Clear();

                    // Fecha o arquivo descritor de offsets
                    txtWriter.Close();

                    // Informa que tudo ocorreu bem
                    Console.WriteLine( "Opera��o realizada com sucesso" );
                } catch( Exception ex ) {
                    // N�o cria o arquivo descritor em casos de erro
                    if( txtWriter != null ) {
                        txtWriter.Close();
                        File.Delete( offsetFileName );
                    }

                    // Exibe o erro para o usu�rio
                    Console.WriteLine( "Erro durante o processamento de dados\n" + ex.GetType() + ": " + ex.Message );
                } finally {
                    // Libera a imagem original
                    image.Dispose();
                }
            }
            //Console.ReadKey();
        }

        // Exibe a ajuda do programa na tela de console
        public static void PrintHelp() {
            Console.WriteLine( "\n------- Sprite Scissors -------\n" );
            Console.WriteLine( "O programa pode receber os seguintes par�metros:\n" );
            Console.WriteLine( "-i (OBRIGAT�RIO): Arquivo de entrada. Os formatos permitidos s�o: BMP, PNG, GIF, JPEG e TIFF\n" );
            Console.WriteLine( "-n (OBRIGAT�RIO): N�mero de frames da imagem\n" );
            Console.WriteLine( "-c (ATERNATIVO): Cria a SpriteFont\n" );
            Console.WriteLine( "-k : Cor de transpar�ncia do arquivo de entrada. As cores devem ser especificadas no seguinte formato: R,G,B,A. N�o deve haver espa�os entre os valores e as v�rgulas. O valor padr�o � Magenta(255,0,255,255)\n" );
            Console.WriteLine( "-o : Diret�rio de sa�da das imagens. Se n�o for especificado, as imagens criadas ser�o salvas no mesmo diret�rio do arquivo de entrada\n" );
            Console.WriteLine( "-f : Formato das imagens geradas. Os valores permitidos s�o: BMP e PNG, sendo PNG o default. Imagens PNG s�o repassadas para o programa PNGout, cujo caminho deve estar no arquivo de configura��o do programa\n" );
            Console.WriteLine( "-d : Diret�rio de sa�da do arquivo descritor de offsets. Se n�o for especificado, o arquivo � criado no mesmo diret�rio especificado por -o\n" );
            Console.WriteLine( "-t : Cria uma textura composta pelos frames gerados. Suas dimens�es ser�o pot�ncias de 2\n" );
            Console.WriteLine( "-q : Suas dimens�es ser�o pot�ncias de 2\n" );
            Console.WriteLine( "-e : Cria a textura de uma fonte, composta pelos frames gerados. Suas dimens�es ser�o pot�ncias de 2. O usu�rio deve passar uma string contendo os caracteres da fonte na mesma ordem em que estes aparecem na imagem\n" );
            Console.WriteLine( "Exemplo:\n" );
            Console.WriteLine( "- Processar o arquivo C:\\MinhasImagens\\exemplo.bmp que possui 4 frames e cuja cor de transpar�ncia � R=0, G=0, B=0\n" );
            Console.WriteLine( "  SpriteScissors -i \"C:\\MinhasImagens\\exemplo.bmp\" -n 4 -k 0,0,0,0\n" );
            Console.WriteLine( "  SpriteScissors -i \"C:\\MinhasImagens\\exemplo.bmp\" -t -e\n" );
            Console.WriteLine( "Os espa�os entre os especificadores (-i, -n, ...) e seus valores s�o obrigat�rios\n" );
            Console.WriteLine( "Os valores de -o e -d devem possuir uma '\\' como �ltimo caractere\n" );
        }

        // Converte o BMP para PNG utilizando o programa PNGOut
        public static void ToPNG( ref string saveName, bool hasColorKey ) {
            if( Settings.Default.PNG_OUT_PATH.Length > 0 ) {
                // Utiliza o programa PNGout para salvar a imagem aproveitando a codifica��o utilizada
                // OBS: Componente n�o salva imagens PNG com paleta de cores
                RunPngOut( saveName + ".bmp", "/kp" );

                // Acrescenta a informa��o de transpar�ncia
                if( hasColorKey )
                    PNGHandler.InsertTransparency( saveName + ".png" );

                // Roda o PNGout novamente sobre a imagem para garantir compress�o m�xima
                RunPngOut( saveName + ".png", Settings.Default.PNG_OUT_USER_PARAMS );
            } else {
                throw new Exception( "O diret�rio do programa PNGout n�o foi informado no arquivo de configura��o do programa" );
            }
        }

        // Retorna a �rea do frame que deve ser recortada
        public static void GetScissoredArea( byte[] image, int imageWidth, int imageHeight, Color colorKey, int frameOffset, int frameWidth, ArrayList paletteColors, out Rectangle area ) {
            // Inicializa as vari�veis auxiliares
            int scissoredX1 = frameWidth;
            int scissoredY1 = imageHeight;
            int scissoredX2 = 0;
            int scissoredY2 = 0;

            // Percorre as linhas do frame
            for( int y = 0; y < imageHeight; ++y ) {
                // Percorre as colunas do frame
                for( int x = 0; x < frameWidth; ++x ) {
                    // Obt�m a cor do pixel
                    Color pixelColor = BitmapMaker.GetPixel( image, imageWidth, imageHeight, frameOffset + x, y );

                    // Se n�o � a cor de transpar�ncia
                    if( ( pixelColor.ToArgb() != colorKey.ToArgb() ) && ( pixelColor.A > 0 ) ) {
                        // Verifica se � um pixel limite da �rea que desejamos recortar
                        
                        if( x < scissoredX1 ) scissoredX1 = x;
                        if( y < scissoredY1 ) scissoredY1 = y;
                        if( x > scissoredX2 ) scissoredX2 = x;
                        if( y > scissoredY2 ) scissoredY2 = y;

                        // Armazena a cor no array auxiliar
                        AddPaletteColor( paletteColors, pixelColor );
                    }
                }
            }
            // Retorna a �rea calculada
            area = new Rectangle( frameOffset + scissoredX1, scissoredY1, scissoredX2 - scissoredX1 + 1, scissoredY2 - scissoredY1 + 1 );
        }

        // Adiciona uma cor � paleta de cores
        public static void AddPaletteColor( ArrayList paletteColors, Color pixelColor ) {
            // Verifica se a cor j� est� inserida no array
            int index = paletteColors.BinarySearch( pixelColor, new ColorComparer() );
            if( index < 0 )
                paletteColors.Insert( -( index + 1 ), pixelColor );
        }

        // Verifica se a �rea recortada do frame possui a cor de transpar�ncia recebida como par�metro
        public static bool ColorKeySearch( Bitmap image, Color colorKey, Rectangle area ) {
            // Percorre a �rea procurando a cor de transpar�ncia
            for( int y = 0; y < area.Height; ++y ) {
                for( int x = 0; x < area.Width; x++ ) {
                    if( image.GetPixel( area.X + x, area.Y + y ).ToArgb() == colorKey.ToArgb() )
                        return true;
                }
            }
            return false;
        }

        // Faz o parse dos par�metros contidos em args
        public static void ReadParams( ref string[] args, out string imagePath, out int nFrames, out Color colorKey, out string outputPath, out ImageFormat selectedFormat, out string offsetFilePath, out bool createTexture, out string fontChars, out bool charReader, out bool usePowerOf2 ) {
            // Inicializa os par�metros com seus valores default
            nFrames = 0;
            imagePath = null;
            outputPath = null;
            offsetFilePath = null;
            colorKey = Color.FromArgb( 255, 255, 0, 255 );
            selectedFormat = ImageFormat.Png;
            createTexture = false;
            fontChars = null;
            charReader = false;
            usePowerOf2 = false;

            // Faz o parse dos par�metros
            char? opt = null;
            for( int i = 0; i < args.Length; ++i ) {
                // Se o primeiro caracter � '-', ent�o � um especificador
                if( args[ i ][ 0 ] == '-' ) {
                    // Obt�m o especificador do par�metro
                    opt = args[ i ][ 1 ];

                    // Trata os especificadores que n�o possuem par�metros
                    switch( opt ) {
                        case 't':
                            createTexture = true;
                        break;

                        case 'c':
                            charReader = true;
                        break;

                        case 'q':
                            usePowerOf2 = true;
                        break;

                        default:
                            continue;
                    }

                    // Indica que est� esperando um novo especificador
                    opt = null;
                }
                    // Trata os especificadores que possuem par�metros
                else if( opt != null ) {
                    // Verifica o que o par�metro est� informando
                    switch( opt ) {
                        // Path da imagem original
                        case 'i':
                            imagePath = args[ i ];
                            break;

                        // N�mero de frames da imagem original
                        case 'n':
                            try {
                                nFrames = Convert.ToInt32( args[ i ] );
                            } catch {
                                nFrames = 0;
                            }
                            break;

                        // Cor de transpar�ncia da imagem original
                        case 'k': {
                                string[] aux = args[ i ].Split( ',', '.' );
                                if( aux.Length >= 4 ) {
                                    try {
                                        colorKey = Color.FromArgb( Convert.ToInt32( aux[ 3 ] ), Convert.ToInt32( aux[ 0 ] ), Convert.ToInt32( aux[ 1 ] ), Convert.ToInt32( aux[ 2 ] ) );
                                    } catch {
                                        colorKey = Color.FromArgb( 255, 255, 0, 255 );
                                    }
                                }
                            }
                            break;

                        // Path da imagem de sa�da
                        case 'o':
                            outputPath = args[ i ];
                            break;

                        // Formato da imagem de sa�da
                        case 'f':
                            switch( args[ i ].ToUpper() ) {
                                case "BMP":
                                    selectedFormat = ImageFormat.Bmp;
                                    break;

                                case "PNG":
                                    selectedFormat = ImageFormat.Png;
                                    break;

                                /* N�o permite por causa de bugs do componente .NET
                                case "GIF":
                                    selectedFormat = ImageFormat.Gif;
                                    break;

                                case "JPG":
                                case "JPEG":
                                    selectedFormat = ImageFormat.Jpeg;
                                    break;

                                case "TIFF":
                                    selectedFormat = ImageFormat.Tiff;
                                    break;
                                */
                            }
                            break;

                        case 'd':
                            offsetFilePath = args[ i ];
                        break;

                        case 'e':
                            createTexture = true;
                            fontChars = args[ i ];
                        break;
                    }
                    // Indica que est� esperando um novo especificador
                    opt = null;
                }
            }
        }

        // Executa o programa PNGout sobre o arquivo recebido como par�metro
        // http://advsys.net/ken/utils.htm#pngout
        // http://advsys.net/ken/util/pngout.htm
        public static void RunPngOut( string inputFileName, string extraParams ) {
            Process pngOut = null;

            Console.WriteLine( "Try PNG" );
            
            try {
                if( File.Exists( @"pngout.exe" ) ) {
                    // Nome do arquivo de sa�da do PNGout
                    string fileName = inputFileName.Remove( inputFileName.LastIndexOf( '.' ) );
                    string tempFileName = fileName + "_outed.png";

                    // Configura o processo que ir� executar o programa PNGout
                    pngOut = new Process();
                    pngOut.StartInfo.CreateNoWindow = true;
                    pngOut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    pngOut.StartInfo.FileName = @"pngout.exe";
                    pngOut.StartInfo.Arguments = "/y /q " + extraParams + " " + inputFileName + " " + tempFileName;

                    // Executa o processo
                    pngOut.Start();

                    // Espera o PNGout terminar o processamento
                    while( !pngOut.HasExited )
                        Thread.Sleep( 10 );

                    // Verifica se o arquivo tempor�rio foi criado
                    if( File.Exists( tempFileName ) ) {
                        // Apaga o arquivo original
                        File.Delete( inputFileName );

                        // Garante que n�o h� um arquivo com o nome que vamos utilizar
                        string outputFileName = fileName + ".png";
                        if( File.Exists( outputFileName ) )
                            File.Delete( outputFileName );

                        // Renomeia o arquivo gerado
                        File.Move( tempFileName, outputFileName );
                    } else {
                        // Se n�o terminou com sucesso, dispara uma exce��o
                        throw new Exception( "N�o foi poss�vel gerar a imagem. C�digo de erro do PNGout = " + pngOut.ExitCode );
                    }
                }
            } catch( Exception ex ) {
                // Informa o erro ao usu�rio
                Console.WriteLine( "Erro ao executar o programa PNGout para este frame\n" + ex.GetType() + ": " + ex.Message );
            } finally {
                // Libera os recursos alocados
                if( pngOut != null ) {
                    pngOut.Close();
                    pngOut.Dispose();
                }
            }
        }


        // Cria uma �nica imagem, com dimens�es em pot�ncias de 2, contendo todos os frames recortados
        // TODO : Otimizar esta rotina!!!!!!!!!!!!
        // TODO : Melhorar organiza��o dos frames na textura (guardar blocos livres de pixels. Come�ar com as maiores em �rea e avan�ar para as menores. Sort num vetor por �rea)
        // TODO : Verificar o tamanho �timo do BMP !!!
        // TODO : Criar as op��es -h e -w para o usu�rio poder determinar a largura e a altura m�xima da textura gerada
        // TODO : Gerar mais de uma textura caso n caiba nas especifica��es dos par�metros -w e -h (pode ser uma outra op��o)
        public static Bitmap CreateTexture( TextWriter txtWriter, ArrayList framesImgsPaths, ArrayList descFileEntries, int totalWidth, int maxHeight, Color colorKey, bool usePowerOf2, int maxTextureWidth = 1024 ) {
            int MAX_TEXTURE_WIDTH = maxTextureWidth;
            const int MAX_TEXTURE_HEIGHT = 1024;

            // Converte a largura e a altura da textura para n�meros pot�cias de 2
            int nLines = 1;
            int textureWidth = usePowerOf2 ? NextPowOf2( totalWidth ) : totalWidth;
            if( textureWidth > MAX_TEXTURE_WIDTH ) {
                nLines = textureWidth / MAX_TEXTURE_WIDTH;
                textureWidth = MAX_TEXTURE_WIDTH;
            }

            int textureHeight = usePowerOf2 ? NextPowOf2( nLines * maxHeight ) : nLines * maxHeight;
            if( textureHeight > MAX_TEXTURE_HEIGHT )
                throw new Exception( "N�o � poss�vel arrumar todos os frames em uma textura 1024 x 1024" );

            // Cria a textura
            // TODO Bitmap texture = new Bitmap( textureWidth, textureHeight, PixelFormat.Format32bppArgb );
            Bitmap texture = new Bitmap( textureWidth, textureHeight );

            // Preenche a textura
            int currWidth = 0, currLine = 0;
            int nFrames = descFileEntries.Count;

            Bitmap currFrameImg;
            Console.Write( "Reading frame(s): " );
            for( int currFrame = 0; currFrame < nFrames; ++currFrame ) {
                Console.Write( currFrame.ToString() + ( ( ( currFrame + 1 ) != nFrames ) ? "," : ".\n" ) );

                // Carrega a imagem do frame que iremos copiar na textura
                currFrameImg = (Bitmap)Bitmap.FromFile( framesImgsPaths[ currFrame ].ToString() );
                int imageWidth = currFrameImg.Width, imageHeight = currFrameImg.Height;

                // Verifica se devemos descer para a pr�xima "linha" de imagens
                if( currWidth + imageWidth > MAX_TEXTURE_WIDTH ) {
                    currWidth = 0;
                    ++currLine;
                }

                // Copia o frame
                int currLineY = ( currLine * maxHeight );
                for( int y = 0; y < imageHeight; ++y ) {
                    for( int x = 0; x < imageWidth; ++x ) {
                        Color c = currFrameImg.GetPixel( x, y );
                        texture.SetPixel( currWidth + x, currLineY + y, c );
                    }
                }

                currFrameImg.Dispose();

                // Preenche o arquivo descritor
                txtWriter.WriteLine( currWidth.ToString() + " " + currLineY.ToString() + " " + imageWidth.ToString() + " " + imageHeight.ToString() + " " + descFileEntries[ currFrame ] );

                // Incrementa o contador da coordenada x atual
                currWidth += imageWidth;

                // Deleta o frame
                //File.Delete( framesImgsPaths[ currFrame ].ToString() );
            }

            // Retorna a textura criada
            return texture;
        }

        // Retorna a pr�xima pot�ncia de 2 depois de n, ou o pr�prio n caso este j� o seja 
        public static int NextPowOf2( int n ) {
            --n;
            n |= n >> 1;
            n |= n >> 2;
            n |= n >> 4;
            n |= n >> 8;
            n |= n >> 16;
            return ++n;
        }
    }
}