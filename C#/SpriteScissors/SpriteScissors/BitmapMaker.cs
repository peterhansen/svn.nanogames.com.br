using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Collections;
using System.Runtime.InteropServices;

namespace SpriteScissors
{
	// Informa��es do arquivo bitmap
	[StructLayout( LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 2 )]
	struct BITMAPFILEHEADER
	{
		public ushort bfType;
		public uint bfSize;
		public ushort bfReserved1;
		public ushort bfReserved2;
		public uint bfOffBits; 
	};

	// Informa��es da imagem bitmap
	[StructLayout( LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4 )]
	struct BITMAPINFOHEADER
	{
		public uint biSize;
		public int biWidth;
		public int biHeight;
		public ushort biPlanes;
		public ushort biBitCount;
		public uint biCompression;
		public uint biSizeImage;
		public int biXPelsPerMeter;
		public int biYPelsPerMeter;
		public uint biClrUsed;
		public uint biClrImportant; 
	};

	// Classe necess�ria para fazermos a compara��o entre as cores na lista ordenada da paleta de cores
	class ColorComparer : IComparer
	{
		// Compara duas cores conforme seus valores R, G, B
		public int Compare( Object x, Object y )
		{
			Color c1 = ( Color )x, c2 = ( Color )y;

			return -( c1.ToArgb() - c2.ToArgb() );
		}
	}

	// Cria um bitmap atrav�s das informa��es recebidas como par�metro
	static class BitmapMaker
	{
		// Defini��es �teis
		public const uint BI_RGB = 0;

		// Calcula o tamanho em bytes de uma linha do bitmap ( deve ser sempre m�ltiplo de 4 )
		public static int GetStride( int width, ushort bpp, out int widthInBytes )
		{
			widthInBytes = ( int )Math.Ceiling( width * ( ( float )bpp / 8 ) );
			return ( widthInBytes + 3 ) & ~3;
		}

		// Converte a estrutura BITMAPFILEHEADER em um array de bytes
		public static unsafe byte[] BmpFileHeaderToByteArray( BITMAPFILEHEADER header )
		{
			byte[] aux = new byte[ sizeof( BITMAPFILEHEADER ) ];
			fixed( byte *pAux = aux )
			{
				*( ( BITMAPFILEHEADER* )pAux ) = header;
			}
			return aux;
		}

		// Converte a estrutura BITMAPINFOHEADER em um array de bytes
		public static unsafe byte[] BmpInfoHeaderToByteArray( BITMAPINFOHEADER header )
		{
			byte[] aux = new byte[ sizeof( BITMAPINFOHEADER ) ];
			fixed( byte* pAux = aux )
			{
				*( ( BITMAPINFOHEADER* )pAux ) = header;
			}
			return aux;
		}

		// Cria e retorna um novo bitmap a partir da imagem original
		public static unsafe Bitmap CreatePalettedBitmapFrom( Bitmap image, Rectangle area, ArrayList palette, Color colorKey )
		{
			// Calcula quantos bits ser�o necess�rios por pixel da imagem
			byte bpp;
			if( palette.Count <= 2 )
				bpp = 1;
			/* N�o funciona
			else if( palette.Count <= 4 )
				bpp = 2;*/
			else if( palette.Count <= 16 )
				bpp = 4;
			else
				bpp = 8;

			// Podemos ignorar as cores n�o utilizadas, por isso o trecho abaixo est� comentado
			// Calcula o n�mero total de cores na paleta de cores
			// int nColors = 2 << ( bpp - 1 );
			int nColors = palette.Count;

			// Obt�m o stride do bitmap e de quantos bytes sua largura necessita
			int widthInBytes;
			int stride = GetStride( area.Width, bpp, out widthInBytes );

			// Preenche os cabe�alhos do bitmap
			BITMAPINFOHEADER infoHeader;
			infoHeader.biSize = ( uint )sizeof( BITMAPINFOHEADER );
			infoHeader.biWidth = area.Width;
			infoHeader.biHeight = -area.Height;
			infoHeader.biPlanes = 1;
			infoHeader.biBitCount = bpp;
			infoHeader.biCompression = BI_RGB;
			infoHeader.biSizeImage = ( uint )( area.Height * stride );
			infoHeader.biXPelsPerMeter = 0;
			infoHeader.biYPelsPerMeter = 0;
			infoHeader.biClrUsed = ( uint )nColors;
			infoHeader.biClrImportant = 0;

			BITMAPFILEHEADER fileHeader;
			fileHeader.bfType = ( ( ( ushort )'M' ) << 8 ) | ( ( ushort )'B' );
			fileHeader.bfReserved1 = 0;
			fileHeader.bfReserved2 = 0;
			fileHeader.bfOffBits = ( uint )sizeof( BITMAPFILEHEADER ) + ( uint )sizeof( BITMAPINFOHEADER ) + ( uint )( 4 * nColors );
			fileHeader.bfSize = fileHeader.bfOffBits + infoHeader.biSizeImage;

			// Ponteiro para a �rea de mem�ria onde iremos criar o arquivo bitmap
			uint counter = 0;
			byte[] bmpBytes = new byte[ fileHeader.bfSize ];

			// Escreve o cabe�alho do arquivo
			byte[] fhArray = BmpFileHeaderToByteArray( fileHeader );

			for( int i = 0 ; i < fhArray.Length ; ++i )
				bmpBytes[ counter++ ] = fhArray[ i ];

			// Escreve o cabe�alho da imagem
			byte[] ihArray = BmpInfoHeaderToByteArray( infoHeader );

			for( int i = 0 ; i < ihArray.Length ; ++i )
				bmpBytes[ counter++ ] = ihArray[ i ];

			// Escreve a paleta de cores
			for( int i = 0 ; i < palette.Count ; ++i )
			{
				bmpBytes[ counter++ ] = ( ( Color )palette[ i ] ).B;
				bmpBytes[ counter++ ] = ( ( Color )palette[ i ] ).G;
				bmpBytes[ counter++ ] = ( ( Color )palette[ i ] ).R;
				bmpBytes[ counter++ ] = ( ( Color )palette[ i ] ).A;
			}

			// Podemos ignorar as cores n�o utilizadas, por isso o trecho abaixo est� comentado
			// Preenche o resto da paleta
			//for( int i = 0 ; i < ( nColors - palette.Count ) ; ++i )
			//{
			//    bmpBytes[ counter++ ] = 0;
			//    bmpBytes[ counter++ ] = 0;
			//    bmpBytes[ counter++ ] = 0;
			//    bmpBytes[ counter++ ] = 0;
			//}

			// Verifica se iremos percorrer os bytes por completo. Quando n�o, iremos for�ar a contagem
			// do �ltimo byte com informa��o da imagem de cada linha	
			int extra = area.Width % ( 8 / bpp ) != 0 ? 1 : 0;

			// Escreve os pixels
			for( int y = 0 ; y < area.Height ; ++y )
			{
				for( int x = 0 ; x < area.Width ; ++x )
				{
					// Verifica qual � a cor do pixel
					Color color = image.GetPixel( area.X + x, area.Y + y );

					// Verifica qual o �ndice desta cor na paleta de cores
					byte colorIndex = 0;

					if( ( color.ToArgb() & 0x00FFFFFF ) != ( colorKey.ToArgb() & 0x00FFFFFF ) )
						colorIndex = ( byte )palette.BinarySearch( color, new ColorComparer() );

					// Preenche a imagem com o �ndice da cor na paleta de cores
					switch( bpp )
					{
						case 1:
							bmpBytes[ counter ] |= ( byte )( colorIndex << ( 7 - ( x % 8 ) ) );
							break;

						/* N�o Funciona
						case 2:
							bmpBytes[ counter ] |= ( byte )( colorIndex << ( 6 - ( ( x % 4 ) << 1 ) ) );
							break; */

						case 4:
							if( x % 2 == 0 )
								bmpBytes[ counter ] = ( byte )( colorIndex << 4 );
							else
								bmpBytes[ counter ] = ( byte )( ( bmpBytes[ counter ] & 0xF0 ) | ( colorIndex & 0x0F ) );
							break;

						case 8:
							bmpBytes[ counter ] = colorIndex;
							break;
					}

					// Aponta para a pr�xima coluna
					counter += ( uint )( ( ( x + 1 ) % ( 8 / bpp ) ) == 0 ? 1 : 0 );
				}

				// Aponta para a pr�xima linha
				counter += ( uint )( stride - widthInBytes + extra );
			}

			// Cria o bitmap
			return new Bitmap( new MemoryStream( bmpBytes ) );
		}

        public static unsafe Bitmap CreateTrueColorBitmapFrom( Bitmap image, Rectangle area, Color colorKey)
        {
            // Calcula quantos bits ser�o necess�rios por pixel da imagem
            // TODO : Por enquanto, considera que nunca teremos uma cor de transpar�ncia
            //byte bpp = 24;
            byte bpp = 32;

            // Obt�m o stride do bitmap e de quantos bytes sua largura necessita
            int widthInBytes;
            int stride = GetStride(area.Width, bpp, out widthInBytes);

            // Preenche os cabe�alhos do bitmap
            BITMAPINFOHEADER infoHeader;
            infoHeader.biSize = (uint)sizeof(BITMAPINFOHEADER);
            infoHeader.biWidth = area.Width;
            infoHeader.biHeight = -area.Height;
            infoHeader.biPlanes = 1;
            infoHeader.biBitCount = bpp;
            infoHeader.biCompression = BI_RGB;
            infoHeader.biSizeImage = (uint)(area.Height * stride);
            infoHeader.biXPelsPerMeter = 0;
            infoHeader.biYPelsPerMeter = 0;
            infoHeader.biClrUsed = 0;
            infoHeader.biClrImportant = 0;

            BITMAPFILEHEADER fileHeader;
            fileHeader.bfType = (((ushort)'M') << 8) | ((ushort)'B');
            fileHeader.bfReserved1 = 0;
            fileHeader.bfReserved2 = 0;
            fileHeader.bfOffBits = (uint)sizeof(BITMAPFILEHEADER) + (uint)sizeof(BITMAPINFOHEADER);
            fileHeader.bfSize = fileHeader.bfOffBits + infoHeader.biSizeImage;

            // Ponteiro para a �rea de mem�ria onde iremos criar o arquivo bitmap
            uint counter = 0;
            byte[] bmpBytes = new byte[fileHeader.bfSize];

            // Escreve o cabe�alho do arquivo
            byte[] fhArray = BmpFileHeaderToByteArray(fileHeader);

            for (int i = 0; i < fhArray.Length; ++i)
                bmpBytes[counter++] = fhArray[i];

            // Escreve o cabe�alho da imagem
            byte[] ihArray = BmpInfoHeaderToByteArray(infoHeader);

            for (int i = 0; i < ihArray.Length; ++i)
                bmpBytes[counter++] = ihArray[i];

            // Escreve os pixels
            for (int y = 0; y < area.Height; ++y)
            {
                for (int x = 0; x < area.Width; ++x)
                {
                    // Verifica qual � a cor do pixel
                    Color color = image.GetPixel(area.X + x, area.Y + y);

                    if( color.A != 0 )
                    {
                        bmpBytes[counter++] = color.B;
                        bmpBytes[counter++] = color.G;
                        bmpBytes[counter++] = color.R;
                        bmpBytes[counter++] = color.A;
                    }
                    else
                    {
                        bmpBytes[counter++] = 0;
                        bmpBytes[counter++] = 0;
                        bmpBytes[counter++] = 0;
                        bmpBytes[counter++] = 0;
                    }
                }

                // Aponta para a pr�xima linha
                counter += (uint)(stride - widthInBytes);
            }

            // Cria o bitmap
            return new Bitmap(new MemoryStream(bmpBytes));
        }

        public static unsafe byte[] CreateEmptyTrueColorBitmap(int width, int height)
        {
            byte bpp = 32;

            // Obt�m o stride do bitmap e de quantos bytes sua largura necessita
            int widthInBytes;
            int stride = GetStride(width, bpp, out widthInBytes);

            // Preenche os cabe�alhos do bitmap
            BITMAPINFOHEADER infoHeader;
            infoHeader.biSize = (uint)sizeof(BITMAPINFOHEADER);
            infoHeader.biWidth = width;
            infoHeader.biHeight = -height;
            infoHeader.biPlanes = 1;
            infoHeader.biBitCount = bpp;
            infoHeader.biCompression = BI_RGB;
            infoHeader.biSizeImage = (uint)(height * stride);
            infoHeader.biXPelsPerMeter = 0;
            infoHeader.biYPelsPerMeter = 0;
            infoHeader.biClrUsed = 0;
            infoHeader.biClrImportant = 0;

            BITMAPFILEHEADER fileHeader;
            fileHeader.bfType = (((ushort)'M') << 8) | ((ushort)'B');
            fileHeader.bfReserved1 = 0;
            fileHeader.bfReserved2 = 0;
            fileHeader.bfOffBits = (uint)sizeof(BITMAPFILEHEADER) + (uint)sizeof(BITMAPINFOHEADER);
            fileHeader.bfSize = fileHeader.bfOffBits + infoHeader.biSizeImage;

            // Ponteiro para a �rea de mem�ria onde iremos criar o arquivo bitmap
            uint counter = 0;
            byte[] bmpBytes = new byte[fileHeader.bfSize];

            // Escreve o cabe�alho do arquivo
            byte[] fhArray = BmpFileHeaderToByteArray(fileHeader);

            for (int i = 0; i < fhArray.Length; ++i)
                bmpBytes[counter++] = fhArray[i];

            // Escreve o cabe�alho da imagem
            byte[] ihArray = BmpInfoHeaderToByteArray(infoHeader);

            for (int i = 0; i < ihArray.Length; ++i)
                bmpBytes[counter++] = ihArray[i];

            // Cria o bitmap
            return bmpBytes;
        }

        public static unsafe Color GetPixel( byte[] fileData, int width, int height, int x, int y )
        {
             byte bpp = 32;

            // Obt�m o stride do bitmap e de quantos bytes sua largura necessita
            int widthInBytes;
            int stride = GetStride(width, bpp, out widthInBytes);

            uint offset = (uint)sizeof(BITMAPFILEHEADER) + (uint)sizeof(BITMAPINFOHEADER);
            offset += (uint)(((height - y - 1) * stride) + ( x * 4 ) );
            return Color.FromArgb( fileData[ offset + 3], fileData[ offset + 2] , fileData[ offset + 1], fileData[ offset ] );
        }

        public static unsafe void SetPixel( byte[] fileData, int width, int height, int x, int y, Color c)
        {
             byte bpp = 32;

            // Obt�m o stride do bitmap e de quantos bytes sua largura necessita
            int widthInBytes;
            int stride = GetStride(width, bpp, out widthInBytes);

            uint offset = (uint)sizeof(BITMAPFILEHEADER) + (uint)sizeof(BITMAPINFOHEADER);
            offset += (uint)((y * stride) + ( x * 4 ) );
            fileData[ offset    ] = c.B;
            fileData[ offset + 1] = c.G;
            fileData[ offset + 2] = c.R;
            fileData[ offset + 3] = c.A;
        }
	}
}