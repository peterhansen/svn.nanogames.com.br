using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Net;

namespace SpriteScissors
{
    // Classe para inserir a informa��o de transpar�ncia na imagem PNG
	// Maiores informa��es sobre formato PNG podem ser encontradas nos sites:
	// - http://www.libpng.org/pub/png/spec/
	// - http://www.libpng.org/pub/png/libpng.html
	// - http://www.zlib.net/
    class PNGHandler
    {
        // Tabela de CRCs
        static uint[] crcTable = null;

		// Insere a informa��o de transpar�ncia na imagem PNG recebida como par�metro
		// TODO : Verificar se possui chunk PLTE. Se n�o possuir, retorna sem fazer nada
        public static unsafe void InsertTransparency( string fileName )
        {
            // Obt�m o arquivo como um array de bytes
            byte[] fileData = File.ReadAllBytes( fileName );

            // Cria um array maior para conter as modifica��es desejadas
			byte[] modifiedFileData = new byte[ fileData.Length + 13 ];

            // Os primeiros oito bytes s�o a assinatura do arquivo PNG e possuem os valores decimais:
            // 137 80 78 71 13 10 26 10. O primeiro chunk, IHDR, vem logo em seguida.
			uint count = 8 + 25;
			for( uint i = 0 ; i < count ; ++i )
                modifiedFileData[ i ] = fileData[ i ];

            // Obt�m o tamanho do chunk PLTE
			// OBD: PNG � um formato de armazenamento para internet, logo temos que converter
			uint chunkLength = 12;
			fixed( byte* pAux = &fileData[ count ] )
				chunkLength += ( uint )( IPAddress.NetworkToHostOrder( *( ( uint* )pAux ) ) >> 32 );

			// Copia o chunk PLTE
			for( uint i = count ; i < count + chunkLength ; ++i )
				modifiedFileData[ i ] = fileData[ i ];

			count += chunkLength;

			// Insere o chunk tRNS com as informa��es de transpar�ncia
			uint countBeforetRNS = count;
			fixed( byte* pChunk = &modifiedFileData[ count ] )
			{
				modifiedFileData[ count++ ] = 0x00;
				modifiedFileData[ count++ ] = 0x00;
				modifiedFileData[ count++ ] = 0x00;
				modifiedFileData[ count++ ] = 0x01;
				modifiedFileData[ count++ ] = ( byte )'t';
				modifiedFileData[ count++ ] = ( byte )'R';
				modifiedFileData[ count++ ] = ( byte )'N';
				modifiedFileData[ count++ ] = ( byte )'S';
				modifiedFileData[ count++ ] = 0;

				byte* pAux = pChunk + 4; // N�o inclui o campo 'length' no c�lculo do crc
				uint crc = GetCRC( pAux, 5 );
				pAux = ( byte* )&crc;

				modifiedFileData[ count++ ] = pAux[ 0 ];
				modifiedFileData[ count++ ] = pAux[ 1 ];
				modifiedFileData[ count++ ] = pAux[ 2 ];
				modifiedFileData[ count++ ] = pAux[ 3 ];
			}

			// Copia o resto do arquivo
			for( uint i = countBeforetRNS ; i < fileData.Length ; ++i )
				modifiedFileData[ count++ ] = fileData[ i ];

            // Salva o arquivo
            File.WriteAllBytes( fileName, modifiedFileData );
        }

        // Calcula o CRC do chunk contido em chunkData
        public static unsafe uint GetCRC( byte* pChunkData, int length )
        {
            if( crcTable == null )
                makeCRCTable();

            uint crc = 0xFFFFFFFF;
			for( int i = 0 ; i < length ; ++i )
				crc = crcTable[ ( crc ^ pChunkData[ i ] ) & 0xFF ] ^ ( crc >> 8 );

            return crc ^ 0xFFFFFFFF;
        }

        // Pr�-calcula a tabela de CRC para agilizar c�lculos futuros
        private static void makeCRCTable()
        {
            crcTable = new uint[256];

            uint c;
            for( int i = 0 ; i < crcTable.Length ; ++i )
            {
                c = ( uint )i;
                for( int j = 0 ; j < 8 ; ++j )
                {
                    if( ( c & 1 ) != 0 )
                        c = 0xedb88320 ^ ( c >> 1 );
                    else
                        c >>= 1;
                }
                crcTable[ i ] = c;
            }
        }
    };
}
