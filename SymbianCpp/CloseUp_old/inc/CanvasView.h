/*
 * CanvasView.h
 *
 *  Created on: 26/04/2010
 *      Author: Daniel
 */

#ifndef CANVASVIEW_H_
#define CANVASVIEW_H_

#include <e32base.h>
#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>

#include "ImageFont.h"
//#include "Label.h"

class CanvasView : public CBase
	{
public:
	enum {UP,RIGHT,DOWN,LEFT,ENTER};
	class CanvasDisplaylet
		{
		public:
		TBuf16<100>* addText()
					{
					TBuf16<100> *ptr=new (ELeave)TBuf16<100>();
					iText.AppendL(ptr);
					return ptr;
					}

		
		CanvasDisplaylet()
			{
			iRectArea=TRect(0,0,200,200);
			iFillColor=TRgb(0,0,0,0);
			iOutlineColor=TRgb(0,0,0);
			iImage=NULL;
			iImageMask=NULL;
			iGoto=-1;
			iState=0;
			}
		
		
		
		
	//	private:
		TInt32 iGoto;
		TInt32 iState;
		TRect iRectArea;
		TRgb iFillColor;
		TRgb iOutlineColor;
		CFbsBitmap *iImage;
		CFbsBitmap *iImageMask;		
		RArray<TBuf16<100>*> iText;
		};
	CanvasView(TInt aXRes,TInt aYRes);
	void Draw(CWindowGc &aGc);
	TInt32 getXRes();
	TInt32 getYRes();
	void setXRes(TInt32 aXRes);
	void setYRes(TInt32 aYRes);
	CFbsBitmap *LoadBMP(TInt aIndex);
	void addDisplaylet(CanvasDisplaylet *displaylet);
	void setGoto(TInt32 aGoto);
	TInt32 getGoto();
	void setCurrentDisplaylet(TInt32 aCurrentDisplaylet);
	
	CanvasDisplaylet *getDisplaylet(TInt32 aIndex)
		{
		return (*iDisplaylet)[aIndex];		
		}
	
	TInt32 getTotalDisplaylets()
		{
		return iDisplaylet->Count();		
		}
	
	TInt32 getCurrentDisplaylet();
	void sendKey(TInt32 aKey);
	void hiding();
	void showing();
	void Click(TPoint aPos);
	virtual ~CanvasView();
	void Centralize(CanvasDisplaylet *ptr);
	void CentralizeHorizontal(CanvasDisplaylet *ptr);
	void CentralizeVertical(CanvasDisplaylet *ptr);
	void ExpandToImage(CanvasDisplaylet *ptr);
	
	
	virtual void SendSignal(int aSignal)
		{
		setGoto(-1);
		}
	//ImageFont *iFont;
	//Label *lbl;
	TInt32 iXRes;
	TInt32 iYRes;
	TInt32 iGoto;
	TInt32 iCurrentDisplaylet;
	RArray<CanvasDisplaylet*> *iDisplaylet;
	CFont *iFont;
	};

#endif /* CANVASVIEW_H_ */
