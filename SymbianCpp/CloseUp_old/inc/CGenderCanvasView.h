/*
 * CGenderCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CGENDERCANVASVIEW_H_
#define CGENDERCANVASVIEW_H_

#include "CanvasView.h"

class CGenderCanvasView : public CanvasView
	{
public:
	CGenderCanvasView(TInt aXRes,TInt aYRes);
	virtual ~CGenderCanvasView();
	virtual void SendSignal(int aSignal);
	};

#endif /* CGENDERCANVASVIEW_H_ */
