/*
 * CGLRender.h
 *
 *  Created on: 19/04/2010
 *      Author: monteiro
 */

#ifndef CGLRENDER_H_
#define CGLRENDER_H_

#include <e32base.h>
#include <w32std.h>
 
#include <GLES/egl.h>
#include <GLES/gl.h>

class CGLRender: public CBase
{
  public:
 
    // m�todo para se criar uma inst�ncia da classe    
    static CGLRender* NewL (RDrawableWindow & aWindow);
    void RenderScene();
  public:
 
    // destrutor
    ~CGLRender ();
 
    // double buffering
    void SwapBuffers ();
    void EnforceContext();
  private:
 
     // construtor
     CGLRender (RDrawableWindow& aWindow);	
     
     // segunda parte do construtor em duas fases
     void ConstructL();
 
  private:
 
     RDrawableWindow     iWindow;
 
     EGLDisplay  iEglDisplay; 	
     EGLConfig   iEglConfig;   	
     EGLContext  iEglContext; 	
     EGLSurface  iEglSurface;	
};
#endif /* CGLRENDER_H_ */
