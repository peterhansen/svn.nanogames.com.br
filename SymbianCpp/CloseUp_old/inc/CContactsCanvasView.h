/*
 * CContactsCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CCONTACTSCANVASVIEW_H_
#define CCONTACTSCANVASVIEW_H_

#include "CanvasView.h"

class CContactsCanvasView : public CanvasView
	{
public:
	class CCloseUpContact
		{
	public:
		TBuf16<100> iName;
		TBuf16<100> iSurname;
		TBuf16<100> iNumber;	
		};
	TBuf16<100> iName;
	TBuf16<100> iNumber;
	RArray<CCloseUpContact*> *iContacts;
	RArray<int> iSelectedContacts;
	CContactsCanvasView(TInt aXRes,TInt aYRes);
	virtual ~CContactsCanvasView();
	virtual void SendSignal(int aSignal)
		{
			iSelectedContacts.Append(-aSignal-2);
			setGoto(4);
			TBuf16<100> num;
			num.AppendNum(-aSignal-2);
			User::InfoPrint(num);
			getDisplaylet(-aSignal+2)->iState=!getDisplaylet(-aSignal+2)->iState;
			//for (int c=0;c<getTotalDisplaylets();c++)
				
		}
	};

#endif /* CCONTACTSCANVASVIEW_H_ */
