/*
 * CMainCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CMAINCANVASVIEW_H_
#define CMAINCANVASVIEW_H_

#include "CanvasView.h"

class CMainCanvasView : public CanvasView
	{
public:
	CMainCanvasView(TInt aXRes,TInt aYRes);
	virtual ~CMainCanvasView();
	void SendSignal(int aSignal){}
	};

#endif /* CMAINCANVASVIEW_H_ */
