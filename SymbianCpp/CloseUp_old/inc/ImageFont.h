#ifndef IMAGEFONT_H
#define IMAGEFONT_H
#include <e32base.h>
#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>
/** N�mero de caracteres dispon�veis. */
#define CHAR_TABLE_SIZE 256

/** Tipo de caracter: qualquer tipo. */
#define CHAR_TYPE_ALL 0
/** Tipo de caracter: acentuados com caixa baixa. */
#define CHAR_TYPE_ACCENT_LOWER_CASE 1
/** Tipo de caracter: acentuados com caixa alta. */
#define CHAR_TYPE_ACCENT_UPPER_CASE 2
/** Tipo de caracter: especiais @#$%�&*()/-+="',.;:[]{}!? etc. */
#define CHAR_TYPE_SPECIAL 3
/** Tipo de caracter: comum (A-Z, a-z) e �. */
#define CHAR_TYPE_REGULAR 4
/** Tipo de caracter: num�rico (0-9). */
#define CHAR_TYPE_NUMERIC 5


class ImageFont {

	public:

	virtual ~ImageFont();

    ImageFont( CFbsBitmap *pImage, TBuf16<255> *characters   );
	//static ImageFont *createMultiSpacedFont( CFbsBitmap *pImage, TBuf16<255> &fontDataFile);

	void calculateOffsets( TBuf16<255> *characters, int length );
	int getTextWidth( TBuf16<255> *text );	
	int getCharWidth( TChar c );
	CFbsBitmap* getImage();
	int getHeight();
	void setCharExtraOffset( int extraOffset );
	int getCharExtraOffset();
	int getCharOffset( TChar c );

//	protected:

	ImageFont( CFbsBitmap *pImage );

	void calculateOffsets( TBuf16<255> *characters );
	
	/** Imagem contendo os caracteres da fonte. */
	CFbsBitmap *pImage;
	
	/** Largura de cada caracter. */
	int charsWidths[ CHAR_TABLE_SIZE ];
	
	/** Posi��o inicial (x) de cada caracter na imagem. */
    int charsOffsets[ CHAR_TABLE_SIZE ];
	
	/** Offset extra entre os caracteres (utilizado para se aumentar ou reduzir o espa�amento padr�o entre eles). */
	int charExtraOffset;

};

#endif
