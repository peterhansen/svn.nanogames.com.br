#ifndef LABEL_H
#define LABEL_H


#include "ImageFont.h"

class Label 
	{

	public:
	//friend class ImageFont;
	Label( ImageFont* pFont, TBuf16<255> *text );
	Label( ImageFont* pFont, TBuf16<255> *text, bool setTextSize );
	virtual ~Label();

	void setText( TBuf16<255> *text );
	void setText( TBuf16<255> *text, bool setSize );
	TBuf16<255> *getCharBuffer();
	TBuf16<255> *getText();
	ImageFont* getFont();
	void setFont( ImageFont* pFont );
	void drawString( CWindowGc* pG, TBuf16<255> *text, int x, int y );
	void drawString( CWindowGc* pG, TBuf16<255>* text, int textLength, int x, int y );
	virtual void Draw(CWindowGc &aGc);
	//protected:

	/** fonte utilizada para desenhar o texto */
	ImageFont* pFont;
	
	/** array de caracteres que representa o texto do label */
	TBuf16<255> *charBuffer;


};

#endif
