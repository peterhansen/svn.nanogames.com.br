/*
 ============================================================================
 Name		: VibraController.h
 Author	  : 
 Version	 : 1.0
 Copyright   : Your copyright notice
 Description : CVibraController declaration
 ============================================================================
 */

#ifndef VIBRACONTROLLER_H
#define VIBRACONTROLLER_H

// INCLUDES
#include <e32std.h>
#include <e32base.h>
#include <hwrmvibra.h> 


// CLASS DECLARATION

class CVibraController : public CBase, MHWRMVibraObserver
	{
public:
	// Constructors and destructor
	~CVibraController();
	static CVibraController* NewL();
	static CVibraController* NewLC();

private:
	CVibraController();
	void ConstructL();
	
private:
	enum
	{
	EVibraStateUnKnown = 0,
	EVibraStateRunning,
	EVibraStateStopped
	};	
	
public:
	void StartVibrationL();
	void StopVibrationL();
	
	TBool IsRunning() { return (iState == EVibraStateRunning); }
	
private:  // from MHWRMVibraObserver
    virtual void VibraModeChanged(CHWRMVibra::TVibraModeState aState);
    virtual void VibraStatusChanged(CHWRMVibra::TVibraStatus aStatus);


private:
	CHWRMVibra*	iVibra;
	TInt iState;
	TInt iIntensity;
	};

#endif // VIBRACONTROLLER_H
