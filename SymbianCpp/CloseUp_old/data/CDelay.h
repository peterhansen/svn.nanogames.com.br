#ifndef __DELAY_H__
#define __DELAY_H__
 
#include <e32base.h>
 
class MDelayObserver
{
public:
    virtual void DelayDoneL() = 0;
};
 
class CDelay : public CActive
{
public:
    static CDelay* NewL(MDelayObserver &aObserver);
    ~CDelay();
 
    void Delay(TTimeIntervalMicroSeconds32 aPeriod);
 
private:
    CDelay(MDelayObserver &aObserver);
    void ConstructL();
    void RunL();
    void DoCancel();
 
private:
    RTimer iTimer;
    TTimeIntervalMicroSeconds32 iPeriod;
    MDelayObserver &iObserver;
};
 
#endif
