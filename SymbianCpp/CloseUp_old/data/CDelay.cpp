#include "Delay.h"
 
CDelay* CDelay::NewL(MDelayObserver &aObserver)
{
    CDelay* self=new(ELeave) CDelay(aObserver);
    CleanupStack::PushL(self);
    self->ConstructL();
    CleanupStack::Pop(self);
    return self;
}
 
CDelay::CDelay(MDelayObserver &aObserver): CActive(0), iPeriod(0), iObserver(aObserver)
{
    CActiveScheduler::Add(this);
}
 
void CDelay::ConstructL()
{
    User::LeaveIfError(iTimer.CreateLocal());
}
 
CDelay::~CDelay()
{
    Cancel();
    iTimer.Close();
}
 
void CDelay::Delay(TTimeIntervalMicroSeconds32 aPeriod)
{
    iTimer.After(iStatus, aPeriod);
    SetActive();
 
 
void CDelay::RunL()
{
    iObserver.DelayDoneL();
}
 
void CDelay::DoCancel()
{
    iTimer.Cancel();
}
