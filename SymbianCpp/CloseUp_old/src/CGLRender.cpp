/*
 * CGLRender.cpp
 *
 *  Created on: 19/04/2010
 *      Author: monteiro
 */

#include "CGLRender.h"
#ifdef PVRT_FIXED_POINT_ENABLE
#define VERTTYPE		GLfixed
#define VERTTYPEENUM	GL_FIXED
#define f2vt(x)			((int)((x)*65536))
#define myglLoadMatrix	glLoadMatrixx
#define myglClearColor	glClearColorx
#else
#define VERTTYPE		GLfloat
#define VERTTYPEENUM	GL_FLOAT
#define f2vt(x)			(x)
#define myglLoadMatrix	glLoadMatrixf
#define myglClearColor	glClearColor
#endif
//_____________________________________________________________________________
 
CGLRender::CGLRender (RDrawableWindow & aWindow)
 : iWindow (aWindow)
{}
 
//_____________________________________________________________________________
 
CGLRender::~CGLRender()
{
 eglMakeCurrent (iEglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
 eglDestroySurface (iEglDisplay, iEglSurface);
 eglDestroyContext (iEglDisplay, iEglContext);
 eglTerminate (iEglDisplay);
}
 
//_____________________________________________________________________________
 
CGLRender* CGLRender::NewL (RDrawableWindow & aWindow)
{
 CGLRender* instance = new (ELeave) CGLRender (aWindow);
 CleanupStack::PushL (instance);
 
 instance->ConstructL();
 CleanupStack::Pop();
 
 return instance;
}
 
//_____________________________________________________________________________
 
void CGLRender::ConstructL()
{
 // lista de atributos
 EGLint attribList [] = 
 { 
   EGL_BUFFER_SIZE, 0,
   EGL_DEPTH_SIZE, 15,
   EGL_NONE 
 };	
 
 
 // recuperar modo de cores do aparelho
 switch (iWindow.DisplayMode() )
 {
   case EColor4K:
    attribList [1] = 12; 
    break; 
 
   case EColor64K:
    attribList [1] = 16; 
    break;
 
   case EColor16M:
    attribList [1] = 24; 
    break; 
 
   default: 
    attribList [1] = 32; 
 }
 
 // passo 1
 iEglDisplay = eglGetDisplay (EGL_DEFAULT_DISPLAY);
 
 if (iEglDisplay == EGL_NO_DISPLAY)
  User::Panic (_L("Unable to find a  suitable EGLDisplay"), 0);
 
 // passo 2
 if (!eglInitialize (iEglDisplay, 0, 0) )
  User::Panic (_L("Unable to initialize EGL"), 0);
 
 // passo 3
 EGLint numConfigs;
 
 if (!eglChooseConfig (iEglDisplay, attribList, &iEglConfig, 1, &numConfigs) )
  User::Panic (_L("Unable to choose EGL config"), 0);
 
 // passo 4
 iEglContext = eglCreateContext (iEglDisplay, iEglConfig, EGL_NO_CONTEXT, 0);
 
 if (iEglContext == 0)
  User::Panic (_L("Unable to create EGL context"), 0);	
 
 // passo 5
 iEglSurface = eglCreateWindowSurface (iEglDisplay, iEglConfig, &iWindow, 0);	
 
 if (iEglSurface == NULL)
  User::Panic (_L("Unable to create EGL surface"), 0);
 
 // passo 6
 eglMakeCurrent (iEglDisplay, iEglSurface, iEglSurface, iEglContext);	
}
 
//_____________________________________________________________________________
 
void CGLRender::EnforceContext ()
{
 eglMakeCurrent (iEglDisplay, iEglSurface, iEglSurface, iEglContext);	
}
 

void CGLRender::RenderScene ()
{
	User::InfoPrint(_L("render"));
	
	VERTTYPE pfVertices[] = {	f2vt(-.4f),f2vt(-.4f),f2vt(0),
								f2vt(+.4f),f2vt(-.4f),f2vt(0),
								f2vt(-.2f),f2vt(0),f2vt(0)
								};
	VERTTYPE pfColors[] = {	f2vt(0.9f), f2vt(0.0f), f2vt(0.0f), f2vt(1.0f),
							f2vt(0.0f), f2vt(0.9f), f2vt(0.0f), f2vt(1.0f),
							f2vt(0.0f), f2vt(0.0f), f2vt(0.9f), f2vt(1.0f)
	};
	
		glClearColor(f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(1.0f));
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
	  glLoadIdentity();  
	  
	
	  glTranslatex(0, 0, f2vt(-10));
	  //Enable the vertices array  
	  glEnableClientState(GL_VERTEX_ARRAY);
	  glVertexPointer(3, GL_FIXED, 0, pfVertices);
	  //3 = XYZ coordinates, GL_SHORT = data type, 0 = 0 stride bytes

	  //Enable the vertex color array
	  glEnableClientState(GL_COLOR_ARRAY);
	  glColorPointer(4,GL_FIXED, 0, pfColors);
	  //4 = RGBA format, GL_UNSIGNED_BYTE = data type,0=0 stide    bytes

	  glDrawArrays(GL_TRIANGLES, 0, 3);
	  /*We want draw triangles, 0 = first element(vertice), 3 = number of 
	    items (vertices) to draw from the array*/  
}
//_____________________________________________________________________________
 
void CGLRender::SwapBuffers ()
{
	User::InfoPrint(_L("swap buffers"));
 eglSwapBuffers (iEglDisplay, iEglSurface);
}
