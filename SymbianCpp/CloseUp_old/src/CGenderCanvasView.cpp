/*
 * CGenderCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CGenderCanvasView.h"
#include <CloseUp.mbg>
CGenderCanvasView::CGenderCanvasView(TInt aXRes,TInt aYRes):CanvasView(aXRes,aYRes)
	{
	// TODO Auto-generated constructor stub
	CanvasView::CanvasDisplaylet *let;
	TBuf16<100> *ptr;
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(64,64,64);
	let->iRectArea=TRect(0,0,getXRes(),getYRes());
	let->iImage=LoadBMP(EMbmCloseupBg);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(128,128,128);
	let->iRectArea=TRect(0,0.6f*getYRes(),getXRes(),getYRes());
	let->iImage=LoadBMP(EMbmCloseupPessoas);	
	let->iImageMask=LoadBMP(EMbmCloseupPessoas_mask);
	addDisplaylet(let);

	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);	
	let->iImage=LoadBMP(EMbmCloseupLogo);
	let->iImageMask=LoadBMP(EMbmCloseupLogo_mask);
	let->iRectArea=TRect(0.3f*getXRes(),0.1f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);
	ExpandToImage(let);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(255,255,255);
	let->iRectArea=TRect(0.2f*getXRes(),0.20f*getYRes(),0.8f*getXRes(),0.4f*getYRes());//(80,180,160,220);
 	ptr=let->addText(); 	
 	ptr->Copy(_L("EM QUEM VOCE"));	
	ptr=let->addText();
	ptr->Append(_L("QUER DAR UM UP?"));	
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(255,0,255);
	let->iRectArea=TRect(0.1f*getXRes(),0.45f*getYRes(),0.47f*getXRes(),0.7f*getYRes());//(20,20,110,170);
	
	let->iImage=LoadBMP(EMbmCloseupNeles);
	let->iImageMask=LoadBMP(EMbmCloseupNeles_mask);
	let->iGoto=-2;
	ExpandToImage(let);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,255,255);
	let->iRectArea=TRect(0.52f*getXRes(),0.45f*getYRes(),0.9f*getXRes(),0.7f*getYRes());//(120,20,220,170);
	let->iGoto=-3;
	let->iImage=LoadBMP(EMbmCloseupNelas);
	let->iImageMask=LoadBMP(EMbmCloseupNelas_mask);
	ExpandToImage(let);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(255,255,255);
	let->iRectArea=TRect(0.4f*getXRes(),0.75f*getYRes(),0.6f*getXRes(),0.9f*getYRes());//(80,180,160,220);
	let->iGoto=3;
	addDisplaylet(let);
	let->iImage=LoadBMP(EMbmCloseupEscolher_contatos);
	let->iImageMask=LoadBMP(EMbmCloseupEscolher_contatos_mask);
	CentralizeHorizontal(let);	
	setCurrentDisplaylet(0);
	}

CGenderCanvasView::~CGenderCanvasView()
	{
	// TODO Auto-generated destructor stub
	}

void CGenderCanvasView::SendSignal(int aSignal)
	{
	switch (aSignal)
		{
		case -2:
		getDisplaylet(4)->iState=1;
		getDisplaylet(5)->iState=0;
		User::InfoPrint(_L("eles"));		
		break;
		case -3:
		getDisplaylet(4)->iState=0;
		getDisplaylet(5)->iState=1;
		User::InfoPrint(_L("elas"));		
		break;
		}
	
	}
