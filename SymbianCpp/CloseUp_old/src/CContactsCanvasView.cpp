/*
 * CContactsCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CContactsCanvasView.h"
#include <CloseUp.mbg>

CContactsCanvasView::CContactsCanvasView(TInt aXRes,TInt aYRes):CanvasView(aXRes,aYRes)
	{
	    CCloseUpContact *contact;
	    TBuf16<100> *ptr;
	    iContacts=new RArray<CCloseUpContact*>();
	// TODO Auto-generated constructor stub
	    /*
	 {
			  
	    	
	    	  CContactDatabase* contactsDb = CContactDatabase::OpenL();
	    	  TContactIter iter(*contactsDb);
	    	  
	    	  TContactItemId cardId;
	    	  TInt pField;
	    	  TInt nField;
	    	  TInt fField;
	    	  TBuf16<100>  text;
	    	  
	    	  while ((cardId = iter.NextL()) != KNullContactId)
	    	  {		
				  contact=new CCloseUpContact();
	    		  CContactItem* card = contactsDb->ReadContactL(cardId);
	    		//  CleanupStack::PushL(card);
	    	
	    		  pField = card->CardFields().Find(KUidContactFieldPhoneNumber);
	    		  nField = card->CardFields().Find(KUidContactFieldGivenName);
	    		  fField = card->CardFields().Find(KUidContactFieldFamilyName);
	    		  if (nField != KErrNotFound && fField != KErrNotFound && pField
	    		  != KErrNotFound)
	    		  {
					  User::InfoPrint(_L("error!"));
	    		  }
	    		  
	    		  {
	    		  CContactItemFieldSet &set=card->CardFields();
	    		  CContactItemField& field= set[nField];
	    		  text.Copy(field.TextStorage()->Text());	    
	    		  contact->iName.Copy(text);
	    		  }

	    		  {
	    		  CContactItemFieldSet &set=card->CardFields();
	    		  CContactItemField& field= set[fField];
	    		  text.Copy(field.TextStorage()->Text());	    
	    		  contact->iSurname.Copy(text);
	    		  }  
	    		  
  

	    		  
	    		  
	    		  iContacts->AppendL(contact);	    		
	    		  contactsDb->CloseContactL(card->Id());
	    		 // CleanupStack::PopAndDestroy(); // card
	    	  }
	    	}
		 */
	    	
	    contact=new CCloseUpContact();	    	
	    contact->iName.Copy(_L("Daniel"));
		contact->iSurname.Copy(_L("Monteiro"));
		iContacts->AppendL(contact);	     
		  

	    contact=new CCloseUpContact();	    	
	    contact->iName.Copy(_L("Chuchu"));
		contact->iSurname.Copy(_L("Queiroz"));
		iContacts->AppendL(contact);	     

		
	    contact=new CCloseUpContact();	    	
	    contact->iName.Copy(_L("Max"));
		contact->iSurname.Copy(_L("Nogueira"));
		iContacts->AppendL(contact);	     

	    contact=new CCloseUpContact();	    	
	    contact->iName.Copy(_L("Vivi"));
		contact->iSurname.Copy(_L("Mesquita"));
		iContacts->AppendL(contact);	     
		
		  
	    contact=new CCloseUpContact();	    	
	    contact->iName.Copy(_L("Peter"));
		contact->iSurname.Copy(_L("Hansen"));
		iContacts->AppendL(contact);	     
		  
	 	    	
	    	
		CanvasView::CanvasDisplaylet *let;
		let=new CanvasView::CanvasDisplaylet();
		let->iFillColor=TRgb(64,64,64);
		let->iRectArea=TRect(0,0,getXRes(),getYRes());//(0,0,320,240);
		let->iImage=LoadBMP(EMbmCloseupBg);
		addDisplaylet(let);
		
		let=new CanvasView::CanvasDisplaylet();
		let->iFillColor=TRgb(128,128,128);
		let->iRectArea=TRect(0,0.6f*getYRes(),getXRes(),getYRes());
		let->iImage=LoadBMP(EMbmCloseupPessoas);	
		let->iImageMask=LoadBMP(EMbmCloseupPessoas_mask);
		addDisplaylet(let);
		
		
		let=new CanvasView::CanvasDisplaylet();
		let->iFillColor=TRgb(0,0,255);	
		let->iImage=LoadBMP(EMbmCloseupLogo);
		let->iImageMask=LoadBMP(EMbmCloseupLogo_mask);
		let->iRectArea=TRect(0.3f*getXRes(),0.1f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);
		ExpandToImage(let);		
		addDisplaylet(let);
		
		
		let=new CanvasView::CanvasDisplaylet();
		let->iFillColor=TRgb(128,128,128);
		let->iRectArea=TRect(0.1*getXRes(),0.3f*getYRes(),getXRes(),getYRes());
		ptr=let->addText();
		ptr->Copy(_L("QUERO DAR UM UP EM:"));
		addDisplaylet(let);
		
		for (int c=0;c<iContacts->Count()*2;c++)
			{
			if (c%2) continue;
			
			for (int d=0;d<iSelectedContacts.Count();d++)
				if (iSelectedContacts[d]==c/2 )
					continue;
			
				ptr=new (ELeave)TBuf16<100>();
			let=new CanvasView::CanvasDisplaylet();
			let->iFillColor=TRgb(255,128,64);
			let->iRectArea=TRect(0.1*getXRes(),(0.4*getYRes())+c*0.05*getYRes(),getXRes(),(0.4*getYRes())+(c+1)*0.05*getYRes());
			//let->iGoto=4;			
			let->iGoto=-2-((c/2));
			ptr->Copy((*iContacts)[c/2]->iName);
			ptr->Append(' ');
			ptr->Append((*iContacts)[c/2]->iSurname);
			let->iText.Append(ptr);
			addDisplaylet(let);
			}
		setCurrentDisplaylet(0);
	}

CContactsCanvasView::~CContactsCanvasView()
	{
	// TODO Auto-generated destructor stub
	}
