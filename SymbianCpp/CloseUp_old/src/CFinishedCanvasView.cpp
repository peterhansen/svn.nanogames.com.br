/*
 * CFinishedCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CFinishedCanvasView.h"
#include <CloseUp.mbg>

CFinishedCanvasView::CFinishedCanvasView(TInt aXRes,TInt aYRes):CanvasView(aXRes,aYRes)
	{
	// TODO Auto-generated constructor stub
	CanvasView::CanvasDisplaylet *let;
	TBuf16<100> *ptr;
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(64,64,64);
	let->iRectArea=TRect(0,0,getXRes(),getYRes());//(0,0,320,240);
	let->iImage=LoadBMP(EMbmCloseupBg);
	addDisplaylet(let);

	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(128,128,128);
	let->iRectArea=TRect(0,0.6f*getYRes(),getXRes(),getYRes());
	let->iImage=LoadBMP(EMbmCloseupPessoas);	
	let->iImageMask=LoadBMP(EMbmCloseupPessoas_mask);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);	
	let->iImage=LoadBMP(EMbmCloseupLogo);
	let->iImageMask=LoadBMP(EMbmCloseupLogo_mask);
	let->iRectArea=TRect(0.3f*getXRes(),0.1f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);
	ExpandToImage(let);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(255,255,255);
	let->iRectArea=TRect(0.2f*getXRes(),0.40f*getYRes(),0.8f*getXRes(),0.8f*getYRes());//(80,180,160,220);
  	ptr=let->addText(); 
  	ptr->Copy(_L("TUDO CERTO."));
  	ptr=let->addText(); 	
  	ptr=let->addText(); 	
  	ptr->Copy(_L("A magica acontece"));
  	ptr=let->addText(); 	
  	ptr->Copy(_L("quando um deles te ligar."));
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(255,0,255);
	let->iRectArea=TRect(0.1f*getXRes(),0.75f*getYRes(),0.47f*getXRes(),0.9f*getYRes());//(20,20,110,170);	
	let->iImage=LoadBMP(EMbmCloseupMais_contatos);
	let->iGoto=3;
	let->iImageMask=LoadBMP(EMbmCloseupMais_contatos_mask);
	ExpandToImage(let);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,255,255);
	let->iRectArea=TRect(0.52f*getXRes(),0.75f*getYRes(),0.9f*getXRes(),0.9f*getYRes());//(120,20,220,170);	
	let->iImage=LoadBMP(EMbmCloseupVoltar_ao_inicio);
	let->iImageMask=LoadBMP(EMbmCloseupVoltar_ao_inicio_mask);
	let->iGoto=1;
	ExpandToImage(let);
	addDisplaylet(let);
	

	setCurrentDisplaylet(0);
	}

CFinishedCanvasView::~CFinishedCanvasView()
	{
	// TODO Auto-generated destructor stub
	}
