/*
 * CanvasView.cpp
 *
 *  Created on: 26/04/2010
 *      Author: Daniel
 */
#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>
#include "CanvasView.h"
//#include "Label.h"
#include <CloseUp.mbg>

void CanvasView::Draw( CWindowGc &aGc)
	{
	
	
	
	while (iCurrentDisplaylet<iDisplaylet->Count() && ((*iDisplaylet)[iCurrentDisplaylet])->iGoto==-1)
		iCurrentDisplaylet++;
	
	int x0,y0,x1,y1;
	CanvasView::CanvasDisplaylet *displaylet=NULL;
	for (int c=0;c<iDisplaylet->Count();c++)
		{		
		displaylet=(*iDisplaylet)[c];
		/*
		if (displaylet->iGoto!=-1)
		{			
		aGc.SetPenColor(TRgb(0,255,0));
		aGc.SetBrushStyle(CGraphicsContext::ENullBrush);
		TRect rect=displaylet->iRectArea;
		rect.Move(-3,-3);
		rect.Grow(8,8);
		aGc.DrawRect(rect);	
		}
	*/
		aGc.SetPenStyle(CGraphicsContext::EDashedPen);
		if (displaylet->iState)
		{			
			aGc.SetPenColor(TRgb(0,0,255));

			aGc.SetBrushStyle(CGraphicsContext::ENullBrush);
			TRect rect=displaylet->iRectArea;
			aGc.DrawRect(rect);	
		}		
		
		aGc.SetBrushColor(displaylet->iFillColor);
		aGc.SetPenColor(displaylet->iFillColor);
		aGc.SetBrushStyle(CGraphicsContext::ESquareCrossHatchBrush);
			
		if (displaylet->iImage!=NULL)
			{
		TSize imageSize(displaylet->iImage->SizeInPixels());
		TRect area(displaylet->iRectArea.iTl.iX,displaylet->iRectArea.iTl.iY,displaylet->iRectArea.iTl.iX+imageSize.iWidth,displaylet->iRectArea.iTl.iY+imageSize.iHeight);
		
			if (iXRes>=imageSize.iWidth)
				{
				x0=0;
				x1=displaylet->iRectArea.iTl.iX+imageSize.iWidth;
				}
			else
				{
				x0=(imageSize.iWidth-iXRes)/2;
				x1=(2*(imageSize.iWidth-iXRes));
				}

			if (iYRes>=imageSize.iHeight)
				{
				y0=0;
				y1=imageSize.iHeight;
				}
			else
				{
				y0=(imageSize.iHeight-iYRes)/2;
				y1=(2*(imageSize.iHeight-iYRes));
				}
			
			TRect cliparea(x0,y0,x1,y1);
		
			if (displaylet->iImageMask==NULL)
				aGc.BitBlt(displaylet->iRectArea.iTl,displaylet->iImage,cliparea);				
			else
				aGc.DrawBitmapMasked(area,displaylet->iImage,displaylet->iImage->SizeInPixels(), displaylet->iImageMask,ETrue );				

			}
		else
			if (displaylet->iText.Count()==0)				
				aGc.DrawRect(displaylet->iRectArea);
		
		aGc.SetPenColor(TRgb(255,255,255));
		//aGc.DrawText(displaylet->iText,
			//	TPoint(displaylet->iRectArea.iTl.iX,displaylet->iRectArea.iTl.iY+(displaylet->iRectArea.iBr.iY-displaylet->iRectArea.iTl.iY)/2)
				//);
		TBuf16<255> tmp;
			for (int c=0;c<displaylet->iText.Count();c++)	
				{
			//		tmp.Copy(*displaylet->iText[c]);
					aGc.DrawText(*displaylet->iText[c],	TPoint(	displaylet->iRectArea.iTl.iX,displaylet->iRectArea.iTl.iY+(c+1)*20));
			//		lbl->drawString(&aGc,&tmp,(int)displaylet->iRectArea.iTl.iX,(int)displaylet->iRectArea.iTl.iY+(c+1)*20);
				}
			

			

		}
	aGc.SetPenStyle(CGraphicsContext::EDottedPen);
	if (iDisplaylet->Count()>0)
		{
		displaylet=(*iDisplaylet)[iCurrentDisplaylet];	
		aGc.SetPenColor(TRgb(255,0,0));
		
		aGc.SetBrushStyle(CGraphicsContext::ENullBrush);
		TRect rect=displaylet->iRectArea;
		rect.Move(-1,-1);
		rect.Grow(5,5);
		aGc.DrawRect(rect);	
		}
	
	}

void CanvasView::setGoto(TInt32 aGoto)
	{
	iGoto=aGoto;	
	}

TInt32 CanvasView::getGoto()
	{
	return iGoto;	
	}

void CanvasView::setCurrentDisplaylet(TInt32 aCurrentDisplaylet)
	{
	iCurrentDisplaylet=aCurrentDisplaylet;	
	}

TInt32 CanvasView::getCurrentDisplaylet()
	{
	return iCurrentDisplaylet;	
	}

void CanvasView::showing()
	{
		
	}

void CanvasView::hiding()
	{
		
	}

TInt32 CanvasView::getYRes()
	{
		return iYRes;
	}
TInt32 CanvasView::getXRes()
	{
		return iXRes;
	}

void CanvasView::setXRes(TInt32 aXRes)
	{		
		 iXRes=aXRes;
	}
void CanvasView::setYRes(TInt32 aYRes)
	{
		 iYRes=aYRes;
	}


void CanvasView::addDisplaylet(CanvasDisplaylet *displaylet)
	{
		iDisplaylet->AppendL(displaylet);
		setGoto(-1);
	}

CanvasView::CanvasView(TInt aXRes,TInt aYRes)
	{
	// TODO Auto-generated constructor stub
		iDisplaylet=new RArray<CanvasDisplaylet*>();	
		iCurrentDisplaylet=0;
		iXRes=aXRes;
		iYRes=aYRes;		
		/*
		TBuf16<255> *chars=new TBuf16<255>();		
		chars->Copy(_L("abcdefghijklmpqrstuvwxyz"));
		//chars->Append(_L("0123456789���"));
	//	chars->Append(_L("��������."));
		//chars->Append(_L(",:-+?!/@�%*\"\'()"));	
		iFont=new ImageFont(CanvasView::LoadBMP(EMbmCloseupFonte_destaque),chars);
		chars=new TBuf16<255>();
		chars->Copy(_L("Daniel"));
		lbl=new Label(iFont,chars,true);	
				*/
		
	}

CanvasView::~CanvasView()
	{
	// TODO Auto-generated destructor stub
	}

void CanvasView::sendKey(TInt32 aKey)
	{
	switch(aKey)
		{
		case UP:
			
			do
			iCurrentDisplaylet--;
			while (iCurrentDisplaylet>0 &&((*iDisplaylet)[iCurrentDisplaylet])->iGoto==-1);
				
			break;
		case DOWN:
			
			do
			iCurrentDisplaylet++;
			while (iCurrentDisplaylet<iDisplaylet->Count() && ((*iDisplaylet)[iCurrentDisplaylet])->iGoto==-1);
			
			break;
		case ENTER:
			setGoto(((*iDisplaylet)[iCurrentDisplaylet])->iGoto);
			break;			
			
		}
	if (iCurrentDisplaylet<0)
		iCurrentDisplaylet=0;
	if (iCurrentDisplaylet>=iDisplaylet->Count())
		iCurrentDisplaylet=iDisplaylet->Count()-1;
	}

	CFbsBitmap *CanvasView::LoadBMP(TInt aIndex)
	{	
	_LIT(KMBMFileName,"\\resource\\apps\\CloseUp.mbm");
	CFbsBitmap *ptr=NULL;
	
	TFindFile imageFile(CCoeEnv::Static()->FsSession());
	
	TInt errcode=imageFile.FindByDir(KMBMFileName,KNullDesC);
	if (KErrNone==errcode)
		{
		ptr= new (ELeave) CFbsBitmap();
		ptr->Load(imageFile.File(),aIndex);
		return ptr;
		}
	else		
		return NULL;
				
	
	return ptr;

	}


void CanvasView::Click(TPoint aPoint)
	{
	
	CanvasView::CanvasDisplaylet *displaylet=NULL;
	for (int c=0;c<iDisplaylet->Count();c++)
		{		
		displaylet=(*iDisplaylet)[c];
		if (displaylet->iRectArea.Contains(aPoint))
			setGoto(displaylet->iGoto);
		}
	}

void CanvasView::Centralize(CanvasView::CanvasDisplaylet *displaylet)
	{
	CentralizeHorizontal(displaylet);
	CentralizeVertical(displaylet);	
	}

void CanvasView::CentralizeHorizontal(CanvasView::CanvasDisplaylet *displaylet)
	{
	int x0,y0,x1,y1;
	TSize imageSize(displaylet->iImage->SizeInPixels());
	TRect area(displaylet->iRectArea.iTl.iX,
				displaylet->iRectArea.iTl.iY,
				displaylet->iRectArea.iTl.iX+imageSize.iWidth,
				displaylet->iRectArea.iTl.iY+imageSize.iHeight);
	
	x0=(iXRes-imageSize.iWidth)/2;
	x1=x0+imageSize.iWidth;
	y0=displaylet->iRectArea.iTl.iY;
	y1=displaylet->iRectArea.iTl.iY+imageSize.iHeight;
		
	displaylet->iRectArea= TRect(x0,y0,x1,y1);	
	
	}

void CanvasView::CentralizeVertical(CanvasView::CanvasDisplaylet *displaylet)
	{
	int x0,y0,x1,y1;
	TSize imageSize(displaylet->iImage->SizeInPixels());
	TRect area(displaylet->iRectArea.iTl.iX,
				displaylet->iRectArea.iTl.iY,
				displaylet->iRectArea.iTl.iX+imageSize.iWidth,
				displaylet->iRectArea.iTl.iY+imageSize.iHeight);
	
	x0=displaylet->iRectArea.iTl.iX;
	x1=displaylet->iRectArea.iTl.iX+imageSize.iWidth;
	y0=(iYRes-imageSize.iHeight)/2;
	y1=y0+imageSize.iHeight;
		
	displaylet->iRectArea= TRect(x0,y0,x1,y1);	
	
	}

void CanvasView::ExpandToImage(CanvasView::CanvasDisplaylet *displaylet)
	{
	if (displaylet->iImage==NULL) return;
	
	
	TSize imageSize(displaylet->iImage->SizeInPixels());
	TRect area(
				displaylet->iRectArea.iTl.iX,
				displaylet->iRectArea.iTl.iY,
				displaylet->iRectArea.iTl.iX+imageSize.iWidth,
				displaylet->iRectArea.iTl.iY+imageSize.iHeight
	);
		
	displaylet->iRectArea= area;	
	
	}

