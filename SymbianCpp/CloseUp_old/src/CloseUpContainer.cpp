/*
========================================================================
 Name        : CloseUpContainer.cpp
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
// [[[ begin generated region: do not modify [Generated System Includes]

#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>
#include <aknutils.h> 
// ]]] end generated region [Generated System Includes]

#include "CSplashCanvasView.h"
#include "CMainCanvasView.h"
#include "CGenderCanvasView.h"
#include "CContactsCanvasView.h"
#include "CFinishedCanvasView.h"
#include "CAboutCanvasView.h"
// [[[ begin generated region: do not modify [Generated User Includes]
#include "CloseUpContainer.h"
#include "CloseUpContainerView.h"
#include "CloseUp.hrh"
// ]]] end generated region [Generated User Includes]

// [[[ begin generated region: do not modify [Generated Constants]
// ]]] end generated region [Generated Constants]
#include <CloseUp.mbg>
#include "ImageFont.h"
#include "Label.h"

CanvasView* CCloseUpContainer::getCanvasView(TInt32 aId)
	{
	return (*iViews)[aId];	
	}

/**
 * First phase of Symbian two-phase construction. Should not 
 * contain any code that could leave.
 */
CCloseUpContainer::CCloseUpContainer()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	
	}
/** 
 * Destroy child controls.
 */
CCloseUpContainer::~CCloseUpContainer()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	
	}
				
/**
 * Construct the control (first phase).
 *  Creates an instance and initializes it.
 *  Instance is not left on cleanup stack.
 * @param aRect bounding rectangle
 * @param aParent owning parent, or NULL
 * @param aCommandObserver command observer
 * @return initialized instance of CCloseUpContainer
 */
CCloseUpContainer* CCloseUpContainer::NewL( 
		const TRect& aRect, 
		const CCoeControl* aParent, 
		MEikCommandObserver* aCommandObserver )
	{
	CCloseUpContainer* self = CCloseUpContainer::NewLC( 
			aRect, 
			aParent, 
			aCommandObserver );
	CleanupStack::Pop( self );
	return self;
	}

/**
 * Construct the control (first phase).
 *  Creates an instance and initializes it.
 *  Instance is left on cleanup stack.
 * @param aRect The rectangle for this window
 * @param aParent owning parent, or NULL
 * @param aCommandObserver command observer
 * @return new instance of CCloseUpContainer
 */
CCloseUpContainer* CCloseUpContainer::NewLC( 
		const TRect& aRect, 
		const CCoeControl* aParent, 
		MEikCommandObserver* aCommandObserver )
	{
	CCloseUpContainer* self = new ( ELeave ) CCloseUpContainer();
	CleanupStack::PushL( self );
	self->ConstructL( aRect, aParent, aCommandObserver );
	return self;
	}

void CCloseUpContainer::SwapBuffers()
	{
	//User::InfoPrint(_L("tick"));

	if (getCanvasView(iCurrentView)->getGoto()!=-1)
		{
		if (getCanvasView(iCurrentView)->getGoto()>0)
			{
			TInt32 oldid=iCurrentView;
			getCanvasView(iCurrentView)->hiding();	
			iCurrentView=getCanvasView(iCurrentView)->getGoto();
			getCanvasView(oldid)->setGoto(-1);
			getCanvasView(iCurrentView)->showing();
			}
		else
			{
			getCanvasView(iCurrentView)->SendSignal(getCanvasView(iCurrentView)->getGoto());
			//getCanvasView(iCurrentView)->setGoto(-1);
			}
		}
	
	DrawDeferred();
	}


TInt CCloseUpContainer::GLCallBack(TAny* aAny)
	{
	CCloseUpContainer* self = static_cast<CCloseUpContainer*>( aAny );
 
        // TODO: The below call is a sample function,
        // you may change it to your requirement.
	self->SwapBuffers();
 
        // Cancel the timer when the callback should not be called again.
        // Call: self->iPeriodicTimer->Cancel();
 
	return KErrNone; // Return value ignored by CPeriodic
	}	

/**
 * Construct the control (second phase).
 *  Creates a window to contain the controls and activates it.
 * @param aRect bounding rectangle
 * @param aCommandObserver command observer
 * @param aParent owning parent, or NULL
 */ 
void CCloseUpContainer::ConstructL( 
		const TRect& aRect, 
		const CCoeControl* aParent, 
		MEikCommandObserver* aCommandObserver )
	{
	if ( aParent == NULL )
	    {
		CreateWindowL();
	    }
	else
	    {
	    SetContainerWindowL( *aParent );
	    }
	iFocusControl = NULL;
	iCommandObserver = aCommandObserver;
	InitializeControlsL();
	SetRect( aRect );
	SetExtentToWholeScreen();
	
	ActivateL();
	
	   CGraphicsDevice* iDevice=iCoeEnv->ScreenDevice();
	   TFileName iFileName;
	   TBuf16<50> filename;
	   filename.Copy(_L("\\resource\\apps\\font.gdr"));
	   iFileName.Copy(filename);
	   TFindFile findfile(CCoeEnv::Static()->FsSession());
	   if (KErrNone == findfile.FindByDir(filename,KNullDesC))
		   User::InfoPrint(_L("nao encontrado"));
	   else
		   User::InfoPrint( findfile.File() );
	   

	   CWsScreenDevice *iScrDevice = iCoeEnv->ScreenDevice();
	   TInt aid=10001;
	   iScrDevice->AddFile(iFileName,aid);
	   _LIT(KFontName,"Ultramagnetic Bold");
	   TFontSpec myFontSpec;
	   myFontSpec.iTypeface.iName=KFontName;
	   myFontSpec.iHeight=150;   
	   iDevice->GetNearestFontInTwips(iFont,myFontSpec);
			   
	// [[[ begin generated region: do not modify [Post-ActivateL initializations]
	// ]]] end generated region [Post-ActivateL initializations]
	iPeriodicTimer=CPeriodic::NewL(CActive::EPriorityUserInput);
    iPeriodicTimer->Start(500000, 500000,TCallBack(GLCallBack, this));
    CanvasView *ptr;
    
   iViews=new RArray<CanvasView*>();
   CWsScreenDevice* screenDevice = CEikonEnv::Static()->ScreenDevice();
   TInt mode = screenDevice->CurrentScreenMode();
   TPixelsAndRotation pixelsAndRotation;
   screenDevice->GetScreenModeSizeAndRotation( mode, pixelsAndRotation );
   TApaTask *task=new TApaTask(this->iCoeEnv->WsSession());
   task->SetWgId(CEikonEnv::Static()->RootWin().Identifier());
   ptr=new CSplashCanvasView(screenDevice->SizeInPixels().iWidth,screenDevice->SizeInPixels().iHeight,task);
   ptr->iFont=iFont;
   iViews->AppendL(ptr);   
   
   ptr=(new CMainCanvasView(screenDevice->SizeInPixels().iWidth,screenDevice->SizeInPixels().iHeight));
   ptr->iFont=iFont;
   iViews->AppendL(ptr);
   
   ptr=(new CGenderCanvasView(screenDevice->SizeInPixels().iWidth,screenDevice->SizeInPixels().iHeight));
   ptr->iFont=iFont;
   iViews->AppendL(ptr);
   
   ptr=(new CContactsCanvasView(screenDevice->SizeInPixels().iWidth,screenDevice->SizeInPixels().iHeight));
   ptr->iFont=iFont;
   iViews->AppendL(ptr);    
  
   ptr=(new CFinishedCanvasView(screenDevice->SizeInPixels().iWidth,screenDevice->SizeInPixels().iHeight));
   ptr->iFont=iFont;
   iViews->AppendL(ptr);    
   ptr=(new CAboutCanvasView(screenDevice->SizeInPixels().iWidth,screenDevice->SizeInPixels().iHeight));
   ptr->iFont=iFont;
   iViews->AppendL(ptr); 
   
   



   iCurrentView=0;
   
    

   
   //iDevice->GetNearestFontInTwips(iFont,myFontSpec);
   
	}
			
/**
* Return the number of controls in the container (override)
* @return count
*/
TInt CCloseUpContainer::CountComponentControls() const
	{
	return ( int ) ELastControl;
	}
				
/**
* Get the control with the given index (override)
* @param aIndex Control index [0...n) (limited by #CountComponentControls)
* @return Pointer to control
*/
CCoeControl* CCloseUpContainer::ComponentControl( TInt aIndex ) const
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	switch ( aIndex )
		{
		}
	// ]]] end generated region [Generated Contents]
	
	// handle any user controls here...
	
	return NULL;
	}
				
/**
 *	Handle resizing of the container. This implementation will lay out
 *  full-sized controls like list boxes for any screen size, and will layout
 *  labels, editors, etc. to the size they were given in the UI designer.
 *  This code will need to be modified to adjust arbitrary controls to
 *  any screen size.
 */				
void CCloseUpContainer::SizeChanged()
	{
	CCoeControl::SizeChanged();
	LayoutControls();
	// [[[ begin generated region: do not modify [Generated Contents]
			
	// ]]] end generated region [Generated Contents]
	}
				
// [[[ begin generated function: do not modify
/**
 * Layout components as specified in the UI Designer
 */
void CCloseUpContainer::LayoutControls()
	{
	}
// ]]] end generated function

/**
 *	Handle key events.
 */				
TKeyResponse CCloseUpContainer::OfferKeyEventL(const TKeyEvent& aKeyEvent,TEventCode aType )
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	
	// ]]] end generated region [Generated Contents]
	
	if ( iFocusControl != NULL
		&& iFocusControl->OfferKeyEventL( aKeyEvent, aType ) == EKeyWasConsumed )
		{
	
		return EKeyWasConsumed;
		}
	if( aType == EEventKeyDown )	 
	{	 
	  // Process EEventKeyDown	 
	}	 
	 
	if( aType == EEventKey )	 
	{	 
	  switch(aKeyEvent.iScanCode)	 
		  {
	  case EStdKeyDownArrow:	 
	  case EStdKeyRightArrow:
		  //User::InfoPrint(_L("down"));
		  getCanvasView(iCurrentView)->sendKey(CanvasView::DOWN);
		  break;
	  case EStdKeyUpArrow:
	  case EStdKeyLeftArrow:
		  //User::InfoPrint(_L("up"));
		  getCanvasView(iCurrentView)->sendKey(CanvasView::UP);
		  break;
		  ///ainda vou encontrar o c�digo de tecla "enter". N�o � EStdKeyEnter!!
	  case /*EStdKeyLeftArrow*/167:
	  case 3:
		  //User::InfoPrint(_L("enter"));
		  getCanvasView(iCurrentView)->sendKey(CanvasView::ENTER);
		  break;	  	 
	  default:	 
	  break;	 
		  }	 
	}
	if( aType == EEventKeyUp )	 
	{
	  // Process EEventKeyUp 	 
	}
		


	return CCoeControl::OfferKeyEventL( aKeyEvent, aType );
	}
				
// [[[ begin generated function: do not modify
/**
 *	Initialize each control upon creation.
 */				
void CCloseUpContainer::InitializeControlsL()
	{
	
	}
// ]]] end generated function

/** 
 * Handle global resource changes, such as scalable UI or skin events (override)
 */
void CCloseUpContainer::HandleResourceChange( TInt aType )
	{
	CCoeControl::HandleResourceChange( aType );
	SetRect( iAvkonViewAppUi->View( TUid::Uid( ECloseUpContainerViewId ) )->ClientRect() );
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
    if ( aType==KEikDynamicLayoutVariantSwitch )
          {			
			
          TRect rect;
          // Ask where container's rectangle should be
          // EMainPane equals to area returned by
          // CEikAppUi::ClientRect()
          AknLayoutUtils::LayoutMetricsRect(AknLayoutUtils::EMainPane,rect);
          // Set new screen rect
          SetRect(rect);
          
          for (int c=0;c<iViews->Count();c++)
        	  {
        	  getCanvasView(c)->setXRes(this->iCoeEnv->ScreenDevice()->SizeInPixels().iWidth);
        	  getCanvasView(c)->setYRes(this->iCoeEnv->ScreenDevice()->SizeInPixels().iHeight);
        	  }
        	  
          }	
	}
				
/**
 *	Draw container contents.
 */				
void CCloseUpContainer::Draw( const TRect& aRect ) const
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	
	CWindowGc& gc = SystemGc();
	//gc.UseFont(CEikonEnv::Static()->DenseFont());
	gc.UseFont(iFont);
	
	//User::InfoPrint(_L("draw"));
	gc.Clear( aRect );
	(*iViews)[iCurrentView]->Draw(gc);	
//	if (iCurrentView==0)		(*iViews)[iCurrentView]->setGoto(1);
	gc.DiscardFont();
	}


void CCloseUpContainer::HandlePointerEventL(const TPointerEvent& aPointerEvent)
	{
    // Remember to call base class implementation. Then your child controls
    // receive pointer events.
    CCoeControl::HandlePointerEventL(aPointerEvent);
 
    // Rest of your code 
    if (aPointerEvent.iType == TPointerEvent::EButton1Down)        
		getCanvasView(iCurrentView)->Click(aPointerEvent.iPosition);
        
	
	}

