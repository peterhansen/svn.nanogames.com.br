#include "Label.h"

 
/**
 * Cria um novo label.
 * 
 * @param font fonte utilizada para desenhar os caracteres do label.
 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
 * @throws java.lang.Exception caso a fonte seja nula.
 */
Label::Label( ImageFont* pFont, TBuf16<255> *text ) 
	{
	Label( pFont, text, true );
	}


/**
 * Cria um novo label.
 * 
 * @param font fonte utilizada para desenhar os caracteres do label.
 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
 * @param setTextSize indica se a largura do label deve ser redefinida de acordo com o texto. <i>true</i> redimensiona
 * o label de forma que tenha a mesma largura que o texto, e <i>false</i> mantém as dimensões atuais do label, 
 * independentemente do texto definido.
 * @throws java.lang.Exception caso a fonte seja nula.
 */
Label::Label( ImageFont* pFont, TBuf16<255> *text, bool setTextSize ) 
	{
	if ( pFont == NULL ) 
		{
	User::InfoPrint(_L("fudeu: fonte nao carregada"));
		return; ///EVIL UNLEASHED!
	}
	
	setFont( pFont );
	setText( text, setTextSize );		
	}


Label::~Label() {
	delete pFont;
	delete charBuffer;
}


/**
 * Define o texto do label. Equivalente à chamada de <code>setText( text, true )</code>.
 * 
 * @param text texto do label.
 * @see #setText(String, boolean)
 * @see #setText(int)
 */
void Label::setText( TBuf16<255> *text ) {
	setText( text, true );
}


/**
 * Define o texto do label.
 * 
 * @param text texto do label.
 * @param setSize indica se as dimensões do label devem ser atualizadas de forma a conter todo o texto.
 * @see #setText(String)
 * @see #setText(int)
 */
void Label::setText( TBuf16<255> *text, bool aSetSize ) {
	charBuffer = text;
	
	//if ( aSetSize )		setSize( pFont->getTextWidth( text ), pFont->getHeight() );		
}
 

/**
 * 
 * @return
 */
TBuf16<255>* Label::getCharBuffer() {
	return charBuffer;
}


/**
 * 
 * @return
 */
TBuf16<255> *Label::getText() {
	return charBuffer;
}
 

ImageFont* Label::getFont() {
	return pFont;
}


void Label::setFont( ImageFont* pFont ) {
	this->pFont = pFont;
}


/**
 * Desenha um texto num Graphics. 
 * 
 * @param g Graphics onde será desenhado o texto.
 * @param text texto a ser desenhado.
 * @param x posição x (esquerda) onde será desenhado o texto.
 * @param y posição y (topo) onde será desenhado o texto.
 */
void Label::drawString( CWindowGc *g, TBuf16<255> *text, int x, int y ) {
	drawString( g, text,text->Length(), x, y );
}


void Label::Draw(CWindowGc &pG)
	{
	User::InfoPrint(_L("desenhou"));
	TBuf16<255> *text=new TBuf16<255>();
	text->Copy(_L("Daniel estatico"));
	drawString(&pG,text,20,20);
	}

/**
 * Desenha um texto num Graphics. 
 * 
 * @param g Graphics onde será desenhado o texto.
 * @param text texto a ser desenhado.
 * @param x posição x (esquerda) onde será desenhado o texto.
 * @param y posição y (topo) onde será desenhado o texto.
 */
void Label::drawString( CWindowGc *pG, TBuf16<255>* text, int textLength, int x, int y ) {
	// não desenha caracteres que estejam fora da área de clip horizontal
	int limitLeft = 0;//this->iRectArea.iTl.iX;
	int limitRight = 640;//this->iRectArea.iBr.iX;//limitLeft + this->iRectArea.iWidth;

	int i = 0;
	for ( ; i < textLength && x < limitRight; ++i ) {
		// o teste da largura do caracter é usado para evitar chamadas desnecessárias de funções no caso
		// de caracteres não presentes na fonte (são ignorados)
		int c = (*text)[ i ];
		// aqui a largura tem que ser a do caracter somente (sem o offset extra)
		int charWidth = pFont->charsWidths[ c ];
		
		if ( charWidth > 0 && ( x + charWidth >= limitLeft ) ) {
			// faz a interseção da área de clip do caracter com a área de clip do texto todo
			//pushClip( pG );
			
			TRect rect( x, y, charWidth, pFont->getHeight() );
			TPoint point(x - pFont->getCharOffset( c ), y);
			
			pG->BitBlt(point,pFont->getImage(),rect);
			
			// restaura a área de clip total
			//popClip( pG );
		} // fim if ( charWidth > 0 && ( x + charWidth >= limitLeft ) )
		
		x += pFont->getCharWidth( c );
	} // fim for ( int i = 0; i < textArray.length && x < limitRight; ++i )
} // fim do método drawString( Graphics, String, int, int )


