/*
========================================================================
 Name        : CloseUpDocument.cpp
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
// [[[ begin generated region: do not modify [Generated User Includes]
#include "CloseUpDocument.h"
#include "CloseUpAppUi.h"
// ]]] end generated region [Generated User Includes]

/**
 * @brief Constructs the document class for the application.
 * @param anApplication the application instance
 */
CCloseUpDocument::CCloseUpDocument( CEikApplication& anApplication )
	: CAknDocument( anApplication )
	{
	}

/**
 * @brief Completes the second phase of Symbian object construction. 
 * Put initialization code that could leave here.  
 */ 
void CCloseUpDocument::ConstructL()
	{
	}
	
/**
 * Symbian OS two-phase constructor.
 *
 * Creates an instance of CCloseUpDocument, constructs it, and
 * returns it.
 *
 * @param aApp the application instance
 * @return the new CCloseUpDocument
 */
CCloseUpDocument* CCloseUpDocument::NewL( CEikApplication& aApp )
	{
	CCloseUpDocument* self = new ( ELeave ) CCloseUpDocument( aApp );
	CleanupStack::PushL( self );
	self->ConstructL();
	CleanupStack::Pop( self );
	return self;
	}

/**
 * @brief Creates the application UI object for this document.
 * @return the new instance
 */	
CEikAppUi* CCloseUpDocument::CreateAppUiL()
	{
	return new ( ELeave ) CCloseUpAppUi;
	}
				
