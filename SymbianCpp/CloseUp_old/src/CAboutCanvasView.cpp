/*
 * CAboutCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CAboutCanvasView.h"
#include <CloseUp.mbg>

CAboutCanvasView::CAboutCanvasView(TInt aXRes,TInt aYRes):CanvasView(aXRes,aYRes)
	{
	// TODO Auto-generated constructor stub
	CanvasView::CanvasDisplaylet *let;
	TBuf16<100> *ptr;
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(64,64,64);
	let->iRectArea=TRect(0,0,getXRes(),getYRes());//(0,0,320,240);
	let->iImage=LoadBMP(EMbmCloseupBg);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(128,128,128);
	let->iRectArea=TRect(0,0.6f*getYRes(),getXRes(),getYRes());
	let->iImage=LoadBMP(EMbmCloseupPessoas);	
	let->iImageMask=LoadBMP(EMbmCloseupPessoas_mask);
	addDisplaylet(let);
		
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);	
	let->iImage=LoadBMP(EMbmCloseupLogo);
	let->iImageMask=LoadBMP(EMbmCloseupLogo_mask);
	let->iRectArea=TRect(0.3f*getXRes(),0.1f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);
	ExpandToImage(let);	
	addDisplaylet(let);
		

		
		let=new CanvasView::CanvasDisplaylet();
		let->iFillColor=TRgb(0,0,255);
		let->iRectArea=TRect(0.1f*getXRes(),0.3f*getYRes(),getXRes(),0.6f*getYRes());
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("O aplicativo de um up"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("e para garotos e garotas"));
	  	ptr=let->addText(); 	
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("Ele customiza o toque dos"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("contatos que voce quiser e ainda"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("ativa algumas funcoes do celular"));	
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("na hora em que receber a ligacao"));
	  	ptr=let->addText(); 	
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("Alem do aplicativo, voce tem"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("wallpapers, videos, mp3 e"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("ringtones do Edu K"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("Copacabana Club para"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("personalizar ainda mais"));
	  	ptr=let->addText(); 	
	  	ptr->Copy(_L("o seu aparelho."));	









	  	
	  	
		addDisplaylet(let);
		
		
		let=new CanvasView::CanvasDisplaylet();
		let->iFillColor=TRgb(0,255,0);
		let->iRectArea=TRect(0.45f*getXRes(),0.85f*getYRes(),0.65f*getXRes(),0.95f*getYRes());
		let->iGoto=1;	
		let->iImage=LoadBMP(EMbmCloseupVoltar_ao_inicio);
		let->iImageMask=LoadBMP(EMbmCloseupVoltar_ao_inicio_mask);
		//if (let->iImage==NULL || let->iImageMask==NULL)
			//let->iText.Copy(_L("Erro ao carregar imagem"));
		CentralizeHorizontal(let);
		addDisplaylet(let);


		
		setCurrentDisplaylet(0);
	}

CAboutCanvasView::~CAboutCanvasView()
	{
	// TODO Auto-generated destructor stub
	}
