/*
 * CSplashCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CSplashCanvasView.h"
#include <CloseUp.mbg>

CSplashCanvasView::CSplashCanvasView(TInt aXRes,TInt aYRes,TApaTask *aTask):CanvasView(aXRes,aYRes)
	{
	iTask=aTask;
	// TODO Auto-generated constructor stub
	CanvasView::CanvasDisplaylet *let;
	TBuf16<100> *ptr;
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(128,128,128);
	let->iRectArea=TRect(0,0,getXRes(),getYRes());
	let->iImage=LoadBMP(EMbmCloseupBg_resultado);
	let->iGoto=-2;
	addDisplaylet(let);
	

	

	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);
	let->iRectArea=TRect(0.1f*getXRes(),0.2f*getYRes(),0.9f*getXRes(),0.4f*getYRes());
	ptr=let->addText();
	ptr->Append(_L("DE UM UP"));	
	ptr=let->addText();
	ptr=let->addText();
	ptr->Append(_L("com Edu K e Copacabana club"));
	let->iGoto=-2;
//	CentralizeHorizontal(let);
	addDisplaylet(let);
	
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);	
	let->iImage=LoadBMP(EMbmCloseupBocas);
	let->iRectArea=TRect(0.0f*getXRes(),0.1f*getYRes(),getXRes(),getYRes());
	let->iImageMask=LoadBMP(EMbmCloseupBocas_mask);
	Centralize(let);
	let->iGoto=-2;
	addDisplaylet(let);
	setCurrentDisplaylet(0);
	}

CSplashCanvasView::~CSplashCanvasView()
	{
	// TODO Auto-generated destructor stub
	}
