/*
========================================================================
 Name        : CloseUpAppUi.cpp
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
// [[[ begin generated region: do not modify [Generated System Includes]
#include <eikmenub.h>
#include <akncontext.h>
#include <akntitle.h>
#include <CloseUp.rsg>
#include <eikbtgpc.h>
#include <avkon.rsg>
// ]]] end generated region [Generated System Includes]

// [[[ begin generated region: do not modify [Generated User Includes]
#include "CloseUpAppUi.h"
#include "CloseUp.hrh"
#include "CloseUpContainerView.h"
// ]]] end generated region [Generated User Includes]

// [[[ begin generated region: do not modify [Generated Constants]
// ]]] end generated region [Generated Constants]

/**
 * Construct the CCloseUpAppUi instance
 */ 
CCloseUpAppUi::CCloseUpAppUi()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	
	}

/** 
 * The appui's destructor removes the container from the control
 * stack and destroys it.
 */
CCloseUpAppUi::~CCloseUpAppUi()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	
	}

void CCloseUpAppUi::SwitchToViewL(int aIndex)
	{
		//SetDefaultViewL( *iCloseUpContainerView );
	//	ActivateViewL(iCloseUpContainerView->);
	TUid Id=iCloseUpContainerView->Id();
//	ActivateLocalViewL(Id);
	SetDefaultViewL(*iCloseUpContainerView);
	User::InfoPrint(_L("switch"));
	StatusPane()->MakeVisible(EFalse);
	Cba()->MakeVisible(EFalse);

	}

// [[[ begin generated function: do not modify
void CCloseUpAppUi::InitializeContainersL()
	{
	iCloseUpContainerView = CCloseUpContainerView::NewL();
	AddViewL( iCloseUpContainerView );
	SetDefaultViewL( *iCloseUpContainerView );
	StatusPane()->MakeVisible(EFalse);
	Cba()->MakeVisible(EFalse);

	}
// ]]] end generated function

/**
 * Handle a command for this appui (override)
 * @param aCommand command id to be handled
 */
void CCloseUpAppUi::HandleCommandL( TInt aCommand )
	{
	StatusPane()->MakeVisible(EFalse);
	Cba()->MakeVisible(EFalse);

	// [[[ begin generated region: do not modify [Generated Code]
	TBool commandHandled = EFalse;
	switch ( aCommand )
		{ // code to dispatch to the AppUi's menu and CBA commands is generated here
		default:
			break;
		}
	
		
	if ( !commandHandled ) 
		{
		if ( aCommand == EAknSoftkeyExit || aCommand == EEikCmdExit )
			{
			Exit();
			}
		}
	// ]]] end generated region [Generated Code]
	
	}

/** 
 * Override of the HandleResourceChangeL virtual function
 */
void CCloseUpAppUi::HandleResourceChangeL( TInt aType )
	{
	CAknViewAppUi::HandleResourceChangeL( aType );
	// [[[ begin generated region: do not modify [Generated Code]
	// ]]] end generated region [Generated Code]
	StatusPane()->MakeVisible(EFalse);
	Cba()->MakeVisible(EFalse);
	}
				
/** 
 * Override of the HandleKeyEventL virtual function
 * @return EKeyWasConsumed if event was handled, EKeyWasNotConsumed if not
 * @param aKeyEvent 
 * @param aType 
 */
TKeyResponse CCloseUpAppUi::HandleKeyEventL(
		const TKeyEvent& aKeyEvent,
		TEventCode aType )
	{
	// The inherited HandleKeyEventL is private and cannot be called
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	Cba()->SetCommandSetL(R_AVKON_SOFTKEYS_EMPTY);
	return EKeyWasNotConsumed;
	}

/** 
 * Override of the HandleViewDeactivation virtual function
 *
 * @param aViewIdToBeDeactivated 
 * @param aNewlyActivatedViewId 
 */
void CCloseUpAppUi::HandleViewDeactivation( 
		const TVwsViewId& aViewIdToBeDeactivated, 
		const TVwsViewId& aNewlyActivatedViewId )
	{
	CAknViewAppUi::HandleViewDeactivation( 
			aViewIdToBeDeactivated, 
			aNewlyActivatedViewId );
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	
	}

/**
 * @brief Completes the second phase of Symbian object construction. 
 * Put initialization code that could leave here. 
 */ 
void CCloseUpAppUi::ConstructL()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	
	BaseConstructL( EAknEnableSkin ); 
	InitializeContainersL();
	// ]]] end generated region [Generated Contents]
	StatusPane()->MakeVisible(EFalse);
	Cba()->MakeVisible(EFalse);
	}

void CCloseUpAppUi::HandleForegroundEventL(TBool aForeground)
	{
	CAknAppUi::HandleForegroundEventL(aForeground);
	if (aForeground)
		{	
		iCloseUpContainerView->StopDisplayingMenuBar();
		iCloseUpContainerView->StopDisplayingToolbar();
		User::InfoPrint(_L("SUPERSIZE-ME!"));
		StatusPane()->MakeVisible(EFalse);
		Cba()->MakeVisible(EFalse);
		this->SetFullScreenApp(true);	
		}
	
	}

