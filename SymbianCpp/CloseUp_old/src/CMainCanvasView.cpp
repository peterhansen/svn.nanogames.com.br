/*
 * CMainCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CMainCanvasView.h"
#include <CloseUp.mbg>



CMainCanvasView::CMainCanvasView(TInt aXRes,TInt aYRes):CanvasView(aXRes,aYRes)
	{
	
	


	
	// TODO Auto-generated constructor stub
	CanvasView::CanvasDisplaylet *let;
	TBuf16<100> *ptr;
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(128,128,128);
	let->iRectArea=TRect(0,0,getXRes(),getYRes());
	let->iImage=LoadBMP(EMbmCloseupBg);	
	addDisplaylet(let);
	
	
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(128,128,128);
	let->iImage=LoadBMP(EMbmCloseupPessoas);	
	let->iImageMask=LoadBMP(EMbmCloseupPessoas_mask);
	let->iRectArea=TRect(0,0.6f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);	
	addDisplaylet(let);
	
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);	
	let->iImage=LoadBMP(EMbmCloseupLogo);
	let->iImageMask=LoadBMP(EMbmCloseupLogo_mask);
	let->iRectArea=TRect(0.3f*getXRes(),0.1f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);
	ExpandToImage(let);
	addDisplaylet(let);
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,0,255);
	let->iRectArea=TRect(0.05f*getXRes(),0.3f*getYRes(),0.9f*getXRes(),0.6f*getYRes());
	let->iGoto=2;
 	ptr=let->addText(); 	
 	ptr->Copy(_L("Nao da pra perder a ligacao")); 	
	ptr=let->addText();
	ptr->Append(_L("de alguem que voce esta afim."));	
	ptr=let->addText();
	ptr=let->addText();
	ptr->Append(_L("ENTAO SELECIONE SEUS"));
	ptr=let->addText();
	ptr->Append(_L(" CONTATOS QUENTES."));
	ptr=let->addText();
	ptr=let->addText();
	ptr->Append(_L("quando um deles ligar, o celular"));
	ptr=let->addText();
	ptr->Append(_L(" vai vibrar tanto quanto voce e"));
	ptr=let->addText();
	ptr->Append(_L("     ativar varias funcoes"));
	addDisplaylet(let);
	
	
	let=new CanvasView::CanvasDisplaylet();
	let->iFillColor=TRgb(0,255,0);
	let->iGoto=5;	
	let->iImage=LoadBMP(EMbmCloseupSobre);
	let->iImageMask=LoadBMP(EMbmCloseupSobre_mask);
	let->iRectArea=TRect(0.45f*getXRes(),0.85f*getYRes(),let->iImage->SizeInPixels().iWidth,let->iImage->SizeInPixels().iHeight);
	CentralizeHorizontal(let);

	addDisplaylet(let);



	
	
	setCurrentDisplaylet(0);
	
	}

CMainCanvasView::~CMainCanvasView()
	{
	// TODO Auto-generated destructor stub
	}




