/*
 ============================================================================
 Name		: VibraController.cpp
 Author	  : 
 Version	 : 1.0
 Copyright   : Your copyright notice
 Description : CVibraController implementation
 ============================================================================
 */

#include "VibraController.h"



#define KMaxVibraTime 10000
#define KVibraInterval 3000

CVibraController::CVibraController(): iState(EVibraStateUnKnown)
	{
	// No implementation required
	}

CVibraController::~CVibraController()
	{
	if(iVibra)
		{
		delete iVibra;
		iVibra = NULL;
		}
	}

CVibraController* CVibraController::NewLC()
	{
	CVibraController* self = new (ELeave) CVibraController();
	CleanupStack::PushL(self);
	self->ConstructL();
	return self;
	}

CVibraController* CVibraController::NewL()
	{
	CVibraController* self = CVibraController::NewLC();
	CleanupStack::Pop(); // self;
	return self;
	}

void CVibraController::ConstructL()
	{
	iVibra = CHWRMVibra::NewL(this);
	iIntensity = 100;
	}

void CVibraController::StartVibrationL()
	{
	iState = EVibraStateUnKnown;
	iVibra->StartVibraL(KMaxVibraTime, iIntensity);
	}

void CVibraController::StopVibrationL()
	{
	iState = EVibraStateUnKnown;
	iVibra->StopVibraL();
	}

void CVibraController::VibraModeChanged(CHWRMVibra::TVibraModeState aState)
	{
	
	}

void CVibraController::VibraStatusChanged(CHWRMVibra::TVibraStatus aStatus)
	{
	
	switch(aStatus)
		{
		case CHWRMVibra::EVibraStatusStopped:
			{
			if(iVibra)
				{
				if(iIntensity == 100)
					{
					iIntensity = 10;
					}
				else
					{
					iIntensity = 100;
					}
				if(iState != EVibraStateUnKnown)
					{
					iVibra->StartVibraL(KMaxVibraTime, iIntensity);
					}
				}
			}
			break;
		case CHWRMVibra::EVibraStatusOn:
			{
			iState = EVibraStateRunning;
			}
			break;
		default:
			break;
		}
	}
