#include "CSplashCanvasView.h"
#include "CloseUpContainer.h"
#include <CloseUp.mbg>

CSplashCanvasView::CSplashCanvasView( TInt aXRes, TInt aYRes) : CanvasView( aXRes, aYRes )
{
	CanvasDisplaylet *pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea = TRect( 0, 0, getXRes(), getYRes() );

		if( getScreenSize()==SS_SMALL )
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP( EMbmCloseupQvgasplash ) );
		else
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP( EMbmCloseupSplash ) );

		pDisplaylet->iGoto = SIGN_NOP - 1;
		addDisplaylet( pDisplaylet );
	}
	CleanupStack::Pop( pDisplaylet );

	setCurrentDisplaylet( 0 );
	
	setGoto( CV_MAIN );
	
}

CSplashCanvasView::~CSplashCanvasView()
{
}

void CSplashCanvasView::SendSignal( int aSignal )
{
	
	if(!getTask() )
	{
		//n�o mais...
	//	getTask()->SendToBackground();
		setGoto( CV_MAIN );
	}
	
}


