#include "CanvasDisplaylet.h"

CanvasDisplaylet::CanvasDisplaylet( void )
				: iEnabled( true ), iVisible( true ), iGoto( 1 ), iState( 0 ), iRectArea( 0, 0, 200, 200 ), iClipArea(),
				  iFillColor( 0, 0, 0, 0 ), iSelectedFont( BF_NORMALTEXT ), iOutlineColor( 255, 255, 255, 255 ), iSelectedColor( 64, 64, 64 ),
				  pImage( NULL ), pImageMask( NULL ), pSelectedImage( NULL ), pSelectedImageMask( NULL ), iText()
{
}
	
CanvasDisplaylet::~CanvasDisplaylet( void )
{
	delete pImage;
	delete pImageMask;

	delete pSelectedImage;
	delete pSelectedImageMask;
	
	iText.ResetAndDestroy();
}
	
TBuf16< 255 >* CanvasDisplaylet::addText( void )
{
	TBuf16< 255 > *ptr = new (ELeave) TBuf16< 255 >();
	CleanupStack::PushL( ptr );
	iText.AppendL( ptr );
	CleanupStack::Pop( ptr );
	return iText[ iText.Count() - 1 ];
}


void CanvasDisplaylet::setDefaultImage( CFbsBitmap* pImg )
{
	if( pImage != NULL )
		delete pImage;
	
	pImage = pImg;
}

void CanvasDisplaylet::setDefaultImageMask( CFbsBitmap* pMask )
{
	if( pImageMask != NULL )
		delete pImageMask;

	pImageMask = pMask;
}

void CanvasDisplaylet::setSelectedImage( CFbsBitmap* pImg )
{
	if( pSelectedImage != NULL )
		delete pSelectedImage;
	
	pSelectedImage = pImg;
}

void CanvasDisplaylet::setSelectedImageMask( CFbsBitmap* pMask )
{
	if( pSelectedImageMask != NULL )
		delete pSelectedImageMask;
	
	pSelectedImageMask = pMask;
}
