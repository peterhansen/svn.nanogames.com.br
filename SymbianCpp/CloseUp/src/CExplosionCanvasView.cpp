/*
 * CExplosionCanvasView.cpp
 *
 *  Created on: 20/05/2010
 *      Author: monteiro
 */

#include "CExplosionCanvasView.h"
#include "CloseUpContainer.h"
#include <CloseUp.mbg>

CExplosionCanvasView::CExplosionCanvasView( TInt aXRes, TInt aYRes )
					: CanvasView( aXRes, aYRes ), iSelection( 0 )
{
	
	
	
	

	CanvasDisplaylet *pDisplaylet=new (ELeave) CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0,0,getXRes(),getYRes());
		addDisplaylet( pDisplaylet );
	}
	CleanupStack::Pop( pDisplaylet );

	pDisplaylet=new (ELeave) CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0.45f*getXRes(),0.85f*getYRes(),0.65f*getXRes(),0.95f*getYRes());
		pDisplaylet->iGoto=CV_MAIN;	
		
		if (getScreenSize()==SS_SMALL)
			{
				pGender[0]=CCloseUpContainer::LoadBMP(EMbmCloseupQvgaeles);
				pGender[1]=CCloseUpContainer::LoadBMP(EMbmCloseupQvgaelas);
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio) );
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_mask) );				
			}
		else
			{
				pGender[0]=CCloseUpContainer::LoadBMP(EMbmCloseupEles);	
				pGender[1]=CCloseUpContainer::LoadBMP(EMbmCloseupElas);
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio) );
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_mask) );
			}
		
		
		
		
		
		
		
		CentralizeHorizontal( pDisplaylet );
		addDisplaylet( pDisplaylet );
	}
	CleanupStack::Pop( pDisplaylet );

	setCurrentDisplaylet( 1 );
}

CExplosionCanvasView::~CExplosionCanvasView()
{
	delete pTask;
	delete pGender[0];
	delete pGender[1];	
}

void CExplosionCanvasView::SendSignal( int aSignal )
{
	CanvasView::SendSignal(aSignal);
	getDisplaylet(0)->setDefaultImage(pGender[aSignal]);
	setGoto(SIGN_NOP);
}

