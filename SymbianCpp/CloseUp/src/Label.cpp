#include "Label.h"

 
/**
 * Cria um novo label.
 * 
 * @param font fonte utilizada para desenhar os caracteres do label.
 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
 * @throws java.lang.Exception caso a fonte seja nula.
 */
Label::Label( ImageFont* pFontt ) 
	{
	Label( pFont,  true );
	}


/**
 * Cria um novo label.
 * 
 * @param font fonte utilizada para desenhar os caracteres do label.
 * @param text texto inicial do label. Utilizar <i>null</i> define um texto vazio.
 * @param setTextSize indica se a largura do label deve ser redefinida de acordo com o texto. <i>true</i> redimensiona
 * o label de forma que tenha a mesma largura que o texto, e <i>false</i> mantém as dimensões atuais do label, 
 * independentemente do texto definido.
 * @throws java.lang.Exception caso a fonte seja nula.
 */
Label::Label( ImageFont* pFont,  bool setTextSize ) 
	{
	if ( pFont == NULL ) 
		{
	User::InfoPrint(_L("fudeu: fonte nao carregada"));

		return; ///EVIL UNLEASHED!
	}
	
	setFont( pFont );
			
	}


Label::~Label() {
	delete pFont;
	pFont=NULL;
}





 

ImageFont* Label::getFont() {
	return pFont;
}


void Label::setFont( ImageFont* pFont ) {
	this->pFont = pFont;
}


/**
 * Desenha um texto num Graphics. 
 * 
 * @param g Graphics onde será desenhado o texto.
 * @param text texto a ser desenhado.
 * @param x posição x (esquerda) onde será desenhado o texto.
 * @param y posição y (topo) onde será desenhado o texto.
 */
void Label::drawString( CGraphicsContext *g, TBuf16<255> *text, int x, int y ,TRect aClippingRect) {
	drawString( g, text,text->Length(), x, y,aClippingRect );
}


void Label::Draw(CGraphicsContext &pG)
	{
	
	}

/**
 * Desenha um texto num Graphics. 
 * 
 * @param g Graphics onde será desenhado o texto.
 * @param text texto a ser desenhado.
 * @param x posição x (esquerda) onde será desenhado o texto.
 * @param y posição y (topo) onde será desenhado o texto.
 */
void Label::drawString( CGraphicsContext *pG, TBuf16<255>* text, int textLength, int x, int y,TRect aClippingRect ) {
	// não desenha caracteres que estejam fora da área de clip horizontal		
	text->LowerCase();
	int i = 0;
	TBuf16<1> tmp;
	int charWidth;
	int charHeight;

	
	TRect clip;
	TRect dstclip;
	for ( ; i < text->Length() /*&& x < limitRight*/; ++i ) {
		// o teste da largura do caracter é usado para evitar chamadas desnecessárias de funções no caso
		// de caracteres não presentes na fonte (são ignorados)
		
		TInt64 c = (*text)[ i ];
	//	if (c==' ' || c==',' || c=='.' || c=='!' || c=='?')
		//if (c <0 && c>255)

		tmp.Zero();
		tmp.Append((TChar)c);
	//	if (this->pFont->iChars.Find(tmp)==KErrNotFound)
		if (c==' ')
			{
			x +=pFont->charsWidths[ 'b' ];
			continue;		
			}
		else
	//	tmp.Append(c);
	//	User::InfoPrint(tmp);		
		

		// aqui a largura tem que ser a do caracter somente (sem o offset extra)		
			
		//if ( charWidth > 0 /*&& ( x + charWidth >= limitLeft )*/ ) 
		{	
			int charWidth = pFont->charsWidths[ c ];
			int charHeight=pFont->getHeight();
			int width=x+charWidth;
			int height=y+charHeight;
			
			clip=aClippingRect;
			clip.Move(-clip.iTl);
			
			
			
			// faz a interseção da área de clip do caracter com a área de clip do texto todo
			TRect rect(pFont->getCharOffset( c)+ pFont->iTrailingSpace  /* +charWidth/2*/ , 0,pFont->getCharOffset( c )+pFont->iTrailingSpace/*+charWidth/2*/+ charWidth, charHeight );
			TRect dstrect(x,y,width,height);
			
			
			dstclip=dstrect;
			dstclip.Move(-aClippingRect.iTl);
			dstclip.Intersection(clip);
			dstclip.Move(-dstclip.iTl);
			dstclip.Move(rect.iTl);
			
			
						
			pG->DrawBitmapMasked(dstrect,pFont->getImage(),dstclip,pFont->pImageMask,ETrue);			
			x += pFont->iSpacing+ charWidth;//pFont->getCharWidth( c );
		} // fim if ( charWidth > 0 && ( x + charWidth >= limitLeft ) )
		

	} // fim for ( int i = 0; i < textArray.length && x < limitRight; ++i )
	//User::InfoPrint(tmp);
} // fim do método drawString( Graphics, String, int, int )


