/*
 * CFinishedCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CFinishedCanvasView.h"
#include "CloseUpContainer.h"
#include <CloseUp.mbg>

CFinishedCanvasView::CFinishedCanvasView(TInt aXRes,TInt aYRes):CanvasView(aXRes,aYRes)
{
	CanvasDisplaylet *pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0,0,getXRes(),getYRes());//(0,0,320,240);
		
		if (getScreenSize()==SS_SMALL)
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgafinished) );
		else
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupFinished) );
		
		addDisplaylet(pDisplaylet);
		pDisplaylet->iEnabled=false;
	}
	CleanupStack::Pop( pDisplaylet );

	pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0.1f*getXRes(),0.75f*getYRes(),0.47f*getXRes(),0.9f*getYRes());//(20,20,110,170);	
		pDisplaylet->iGoto=CV_CONTACTS;
		
		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgamais_contatos) );		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgamais_contatos_mask) );		
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgamais_contatos_over) );			
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgamais_contatos_mask) );			
			}
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupMais_contatos) );
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupMais_contatos_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupMais_contatos_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupMais_contatos_mask) );
			}
		
		
		ExpandToImage(pDisplaylet);
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );

	pDisplaylet=new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0.52f*getXRes(),0.75f*getYRes(),0.9f*getXRes(),0.9f*getYRes());//(120,20,220,170);	
		

		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio) );		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_mask) );		
			}
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio) );
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_mask) );
			}	
		
		pDisplaylet->iGoto=CV_MAIN;
		ExpandToImage(pDisplaylet);
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );
	
	setCurrentDisplaylet( 1 );
}

CFinishedCanvasView::~CFinishedCanvasView()
{
}
