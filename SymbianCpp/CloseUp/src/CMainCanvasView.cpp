/*
 * CMainCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */
#include <aknnotewrappers.h>

#include "CMainCanvasView.h"
#include "CloseUpContainer.h"
#include <CloseUp.mbg>
		


CMainCanvasView::CMainCanvasView( TInt aXRes, TInt aYRes)
				: CanvasView( aXRes, aYRes ), iState( 0 ),pCallMonitor( NULL ), pContacts( NULL )
{
	
	pCallMonitor = CCallMonitor::NewLC( *this );
	CleanupStack::Pop( pCallMonitor );
	
	CanvasDisplaylet *pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0,0,getXRes(),getYRes());

		if (getScreenSize()==SS_SMALL)
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP( EMbmCloseupQvgamain ) );
		else
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP( EMbmCloseupMain ) );

		addDisplaylet( pDisplaylet );
	}
	CleanupStack::Pop( pDisplaylet );
////////////////////////////////////////////////////
	pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iGoto=CV_GENDER;
		
		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgainiciar) );		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgainiciar_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgainicar_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgainiciar_mask) );				
			}
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupIniciar) );		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupIniciar_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupInicar_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupIniciar_mask) );
			}
		
		
		
		pDisplaylet->iRectArea=TRect(0.45f*getXRes(),0.75f*getYRes(),pDisplaylet->getDefaultImage()->SizeInPixels().iWidth,pDisplaylet->getDefaultImage()->SizeInPixels().iHeight);
		CentralizeHorizontal(pDisplaylet);
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );
///////////////////////////////////////////////	
	pDisplaylet=new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iGoto=SIGN_NOP-1;
		
		
		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaminimizar));		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaminimizar_mask));
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaminimizar_over));
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaminimizar_over_mask));			
			}
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupMinimizar));
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupMinimizar_mask));
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupMinimizar_over));
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupMinimizar_mask));
			}
		
		pDisplaylet->iRectArea=TRect(0.45f*getXRes(),0.85f*getYRes(),pDisplaylet->getDefaultImage()->SizeInPixels().iWidth,pDisplaylet->getDefaultImage()->SizeInPixels().iHeight);
		CentralizeHorizontal(pDisplaylet);
		pDisplaylet->iRectArea.Move(-pDisplaylet->iRectArea.iTl);
		pDisplaylet->iRectArea.Move(0.1f*getXRes(),0.85f*getYRes());
		addDisplaylet( pDisplaylet );
	}
	CleanupStack::Pop( pDisplaylet );
////////////////////////////////////////////////
	pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iGoto=CV_ABOUT;

		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgasobre));
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgasobre_mask));
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgasobre_over));		
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgasobre_over_mask));		
			}		
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupSobre));
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupSobre_mask));
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupSobre_over));
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupSobre_mask));		
			}
		
		pDisplaylet->iRectArea=TRect(0.45f*getXRes(),0.85f*getYRes(),pDisplaylet->getDefaultImage()->SizeInPixels().iWidth,pDisplaylet->getDefaultImage()->SizeInPixels().iHeight);
		CentralizeHorizontal(pDisplaylet);
		pDisplaylet->iRectArea.Move(-pDisplaylet->iRectArea.iTl);
		pDisplaylet->iRectArea.Move(0.65f*getXRes(),0.85f*getYRes());
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );
////////////////////////////////////////
	setCurrentDisplaylet( 1 );
}

CMainCanvasView::~CMainCanvasView( void )
{
	pContacts->ResetAndDestroy();
	delete pContacts;
	
	if (pCallMonitor->IsActive())
		pCallMonitor->Cancel();
	delete pCallMonitor;
}

void CMainCanvasView::SendSignal( int aSignal )
{
	switch (aSignal)
		{
		case SIGN_NOP-1:
			if( getTask() )
			{
				getTask()->SendToBackground();

			}
			break;
		case SIGN_NOP-2:
			{
				CAknInformationNote* infor = new (ELeave) CAknInformationNote;
				infor->ExecuteLD(_L("Importe seus contatos para o aparelho antes!"));
			}
			break;
		}
	setGoto(SIGN_NOP );	
}
											  
/*
 * void CMainCanvasView::sendKey(int aKey)
	{
		switch(aKey)
			{
			case CanvasView::LEFT:
				setCurrentDisplaylet(DL_BTN_MINIMIZAR);
				break;

			case CanvasView::RIGHT:
				setCurrentDisplaylet(DL_BTN_SOBRE);
				break;

			case CanvasView::UP:
				if (getCurrentDisplaylet()==DL_BTN)
					setCurrentDisplaylet(DL_ELES+iSelection);				
				break;

			case CanvasView::DOWN:
				if (getCurrentDisplaylet()!=DL_BTN)
					setCurrentDisplaylet(DL_BTN);
				break;
			
			case CanvasView::ENTER:
				CanvasView::sendKey(aKey);		

			}
	
	}
 * */
											  
void CMainCanvasView::CallStatusChangedL( CTelephony::TCallStatus& aStatus, TInt aError )
{
	if( aError == KErrNone )
	{
		CTelephony *tel = pCallMonitor->GetTelephony();
		
		CTelephony::TRemotePartyInfoV1 RemInfoUse;
		CTelephony::TCallInfoV1		   CallInfoUse;
		CTelephony::TCallSelectionV1   CallSelectionUse;

		CTelephony::TRemotePartyInfoV1Pckg 	RemParty(RemInfoUse);
		CTelephony::TCallInfoV1Pckg 		CallInfo(CallInfoUse);
		CTelephony::TCallSelectionV1Pckg 	CallSelection(CallSelectionUse);

		CTelephony::TCallStatusV1 CallStatus;
		CTelephony::TCallStatusV1Pckg CallStatusPkg(CallStatus);			

		CallSelectionUse.iLine=CTelephony::EVoiceLine;
		CallSelectionUse.iSelect=CTelephony::EInProgressCall;		

		tel->GetCallInfo( CallSelection,CallInfo,RemParty );
		
		getTask()->BringToForeground();

		TBuf16<3*NUMCHARSTOCOMPARE> numCaller;
		TBuf16<3*NUMCHARSTOCOMPARE> numContact;
		
		numCaller.Zero();
		numCaller.Copy(RemInfoUse.iRemoteNumber.iTelNumber.Right(NUMCHARSTOCOMPARE));
		int nContacts=pContacts->Count();
		for( TInt c = 0 ; c < nContacts ; ++c )	
			{
			
			if ((*pContacts)[c]->iSelected==0) continue;
			
			numContact.Zero();
			numContact.Copy((*pContacts)[c]->iNumber.Right(NUMCHARSTOCOMPARE));

			if (numContact.Compare(numCaller) == 0)
				{
				getTask()->BringToForeground();
				iState = 1;
				return;
				}
			}
		
		
	}
}
