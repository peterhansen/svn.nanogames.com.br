/*
 * CGenderCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CGenderCanvasView.h"
#include "CloseUpContainer.h"
#include <CloseUp.mbg>

		
#define DL_BG 0
#define DL_ELES 1
#define DL_ELAS 2
#define DL_BTN 3

		
CGenderCanvasView::CGenderCanvasView( TInt aXRes, TInt aYRes )
				 : CanvasView( aXRes, aYRes ), iSelection( 0 )
{
	CanvasDisplaylet *pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0,0,getXRes(),getYRes());
		
		if (getScreenSize()==SS_SMALL)
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgagender) );
		else
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupGender) );

		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );
	
	pDisplaylet=new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0.1f*getXRes(),0.45f*getYRes(),0.47f*getXRes(),0.7f*getYRes());//(20,20,110,170);
		pDisplaylet->iSelectedColor=TRgb(0,0,255);
		pDisplaylet->iGoto=SIGN_NOP-1;
		pDisplaylet->iState=1;
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );

	pDisplaylet=new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0.52f*getXRes(),0.45f*getYRes(),0.9f*getXRes(),0.7f*getYRes());//(120,20,220,170);
		pDisplaylet->iSelectedColor=TRgb(255,0,0);
		pDisplaylet->iGoto=SIGN_NOP-2;
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );
	
	pDisplaylet=new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0.4f*getXRes(),0.75f*getYRes(),0.6f*getXRes(),0.93f*getYRes());//(80,180,160,220);

		pDisplaylet->iGoto=CV_CONTACTS;
		
		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaescolher_contatos) );		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaescolher_contatos_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaescolher_contatos_over) );		
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaescolher_contatos_mask) );
			}
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupEscolher_contatos) );
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupEscolher_contatos_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupEscolher_contatos_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupEscolher_contatos_mask) );
			}
	
		
		

		addDisplaylet(pDisplaylet);
		CentralizeHorizontal(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );
	
	setCurrentDisplaylet(DL_BG);
	if (!iSelection)
		SendSignal(SIGN_NOP-1);
	else
		SendSignal(SIGN_NOP-2);
	setCurrentDisplaylet( 1 );
}

CGenderCanvasView::~CGenderCanvasView()
{
}

/*
void CGenderCanvasView::sendKey(int aKey)
	{
		switch(aKey)
			{
			case CanvasView::LEFT:
				setCurrentDisplaylet(DL_ELES);
				break;

			case CanvasView::RIGHT:
				setCurrentDisplaylet(DL_ELAS);
				break;

			case CanvasView::UP:
				if (getCurrentDisplaylet()==DL_BTN)
					setCurrentDisplaylet(DL_ELES+iSelection);				
				break;

			case CanvasView::DOWN:
				if (getCurrentDisplaylet()!=DL_BTN)
					setCurrentDisplaylet(DL_BTN);
				break;
			
			case CanvasView::ENTER:
				CanvasView::sendKey(aKey);		

			}
	
	}
*/
void CGenderCanvasView::SendSignal(int aSignal)
	{
		   
	switch (aSignal)
		{
		case -2:
		iSelection=0;		
		getDisplaylet(1)->iState=1;
		getDisplaylet(2)->iState=0;
		break;
		
		case -3:
		iSelection=1;
		getDisplaylet(1)->iState=0;
		getDisplaylet(2)->iState=1;
		break;
		}	       
			if (getScreenSize()==SS_SMALL)
				setGoto(CV_CONTACTS);
			else		   
				setGoto(SIGN_NOP);
	}

