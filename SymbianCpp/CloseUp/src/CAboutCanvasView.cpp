/*
 * CAboutCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include "CAboutCanvasView.h"
#include "CloseUpContainer.h"
#include <CloseUp.mbg>

CAboutCanvasView::CAboutCanvasView( TInt aXRes, TInt aYRes )
				: CanvasView( aXRes, aYRes )
{
	CanvasDisplaylet *pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{
		pDisplaylet->iRectArea=TRect(0,0,getXRes(),getYRes());
		if (getScreenSize()==SS_SMALL)
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgaabout));
		else
			pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupAbout));
		addDisplaylet(pDisplaylet);
	}
	CleanupStack::Pop( pDisplaylet );

	pDisplaylet = new (ELeave)CanvasDisplaylet();
	CleanupStack::PushL( pDisplaylet );
	{		
		pDisplaylet->iRectArea=TRect(0.45f*getXRes(),0.85f*getYRes(),0.65f*getXRes(),0.95f*getYRes());
		pDisplaylet->iGoto=CV_MAIN;	
		
		if (getScreenSize()==SS_SMALL)
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio) );		
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupQvgavoltar_ao_inicio_mask) );
			}
		
		else
			{
				pDisplaylet->setDefaultImage( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio) );
				pDisplaylet->setDefaultImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_mask) );
				pDisplaylet->setSelectedImage( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_over) );
				pDisplaylet->setSelectedImageMask( CCloseUpContainer::LoadBMP(EMbmCloseupVoltar_ao_inicio_mask) );
			}
		
		
		
		CentralizeHorizontal( pDisplaylet );
		addDisplaylet( pDisplaylet );
	}
	CleanupStack::Pop( pDisplaylet );

	setCurrentDisplaylet( 1 );
}

CAboutCanvasView::~CAboutCanvasView()
{
}
