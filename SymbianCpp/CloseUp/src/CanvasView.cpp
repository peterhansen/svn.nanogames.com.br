/*
 * CanvasView.cpp
 *
 *  Created on: 26/04/2010
 *      Author: Daniel
 */
#include <aknviewappui.h>
#include <coemain.h>
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikappui.h>
#include <eikenv.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <e32math.h>
#include <e32des8.h>
#include <fbs.h>
#include <flogger.h>
#include <f32file.h>  //RFs
#include <gdi.h>
#include <s32file.h>  //RFileReadStream
#include <w32std.h>

#include <CloseUp.mbg>
#include "CanvasView.h"
#include "CanvasDisplaylet.h"




void CanvasView::Draw( CGraphicsContext &aGc)
	{
	
	CanvasDisplaylet *displaylet=NULL;	
	int to=iDisplaylet->Count();
	CNanoFont *font=NULL;
	int previousheight=0;
	
	TRect rect;
	TSize imageSize;
	TRect area;					
	TRect clip;
	int textcount=0;	
	for (int c=0;c<to;++c)
		{		
			displaylet=(*iDisplaylet)[c];
			font=iFont[displaylet->iSelectedFont];			
			aGc.SetPenStyle(CGraphicsContext::EDashedPen);
			
			if (displaylet->iState)
			{			
				aGc.SetPenColor(displaylet->iSelectedColor);	
				aGc.SetBrushStyle(CGraphicsContext::ENullBrush);
				rect=displaylet->iRectArea;				
				rect.Intersection(displaylet->iClipArea);
				aGc.DrawRect(rect);	
			}		
			
			aGc.SetBrushColor(displaylet->iFillColor);
			aGc.SetPenColor(displaylet->iFillColor);
			aGc.SetBrushStyle(CGraphicsContext::ESquareCrossHatchBrush);
				
			if (displaylet->getDefaultImage()!=NULL)
				{
					imageSize=displaylet->getDefaultImage()->SizeInPixels();
					area=TRect(displaylet->iRectArea.iTl.iX,displaylet->iRectArea.iTl.iY,displaylet->iRectArea.iTl.iX+imageSize.iWidth,displaylet->iRectArea.iTl.iY+imageSize.iHeight);					
					clip=displaylet->iClipArea;
					clip.Intersection(area);
					
					if (displaylet->getDefaultImageMask()==NULL)						
						aGc.DrawBitmap(area,displaylet->getDefaultImage(),clip);				
					else
						aGc.DrawBitmapMasked(clip,displaylet->getDefaultImage(),clip.Size(), displaylet->getDefaultImageMask(),ETrue );				
	
				}
				
			
				aGc.SetPenColor(TRgb(255,255,255));
				aGc.SetBrushStyle(CGraphicsContext::ENullBrush);
				previousheight=0;
				textcount=displaylet->iText.Count();
				
				for (int d=0;d<textcount;++d)
					{
						font->drawString(&aGc,displaylet->iText[d],displaylet->iRectArea.iTl.iX,displaylet->iRectArea.iTl.iY+previousheight+(d)*font->pFont->getHeight(),displaylet->iClipArea);
						previousheight=font->pFont->getHeight();
					}
				
				if (iCurrentDisplaylet==c)
					{
					if (displaylet->getSelectedImage()==NULL )
						{
						if ( getScreenSize()==SS_SMALL )
							{
							aGc.SetPenStyle(CGraphicsContext::EDottedPen);
							aGc.SetPenColor(displaylet->iOutlineColor);				
							aGc.SetBrushStyle(CGraphicsContext::ENullBrush);
							rect=displaylet->iRectArea;
							rect.Move(-1,-1);
							rect.Grow(5,5);
							rect.Intersection(displaylet->iClipArea);
							aGc.DrawRect(rect);
							}
						}
					else
						{
						
						imageSize=displaylet->getSelectedImage()->SizeInPixels();
						area=TRect(displaylet->iRectArea.iTl.iX,displaylet->iRectArea.iTl.iY,displaylet->iRectArea.iTl.iX+imageSize.iWidth,displaylet->iRectArea.iTl.iY+imageSize.iHeight);						
						clip=displaylet->iClipArea;
						clip.Intersection(area);
							if (displaylet->getSelectedImageMask()==NULL)
								aGc.DrawBitmap(area,displaylet->getSelectedImage(),clip);				
							else
								aGc.DrawBitmapMasked(clip,displaylet->getSelectedImage(),clip.Size(), displaylet->getSelectedImageMask(),ETrue );				
						
						}
					}
				
		}

	}

void CanvasView::setGoto(TInt32 aGoto)
	{
	iGoto=aGoto;	
	}

TInt32 CanvasView::getGoto()
	{
	return iGoto;	
	}

void CanvasView::removeDisplaylet(TInt32 aDisplaylet)
	{
		iDisplaylet->Remove(aDisplaylet);
		//agora aDisplaylet aponta para o displaylet seguinte.
		setCurrentDisplaylet(aDisplaylet);
	}


void CanvasView::setCurrentDisplaylet(TInt32 aCurrentDisplaylet)
	{
	if (aCurrentDisplaylet<getTotalDisplaylets())
		iCurrentDisplaylet=aCurrentDisplaylet;
	else 
		iCurrentDisplaylet=getTotalDisplaylets()-1;
	}

TInt32 CanvasView::getCurrentDisplaylet()
	{
	return iCurrentDisplaylet;	
	}

void CanvasView::showing()
	{
		
	}

void CanvasView::hiding()
	{
		
	}

TInt32 CanvasView::getYRes()
	{
		return iYRes;
	}
TInt32 CanvasView::getXRes()
	{
		return iXRes;
	}

void CanvasView::setXRes(TInt32 aXRes)
	{		
	User::InfoPrint(_L("recalc X"));	
		CanvasDisplaylet *displaylet=NULL;
		for (int c=0;c<iDisplaylet->Count();c++)
			{		
				displaylet=(*iDisplaylet)[c];
				displaylet->iRectArea.iTl.iX*=aXRes;
				displaylet->iRectArea.iTl.iX/=iXRes;
				displaylet->iRectArea.iBr.iX*=aXRes;
				displaylet->iRectArea.iBr.iX/=iXRes;				
				
			}
		iXRes=aXRes;	 
	}
void CanvasView::setYRes(TInt32 aYRes)
	{
//	User::InfoPrint(_L("recalc Y"));
	CanvasDisplaylet *displaylet=NULL;
	for (int c=0;c<iDisplaylet->Count();c++)
		{		
			displaylet=(*iDisplaylet)[c];
			displaylet->iRectArea.iTl.iY*=aYRes;
			displaylet->iRectArea.iTl.iY/=iYRes;
			displaylet->iRectArea.iBr.iY*=aYRes;
			displaylet->iRectArea.iBr.iY/=iYRes;				
			
		}

		 iYRes=aYRes;
	}


void CanvasView::addDisplaylet(CanvasDisplaylet *displaylet)
	{
		iDisplaylet->AppendL(displaylet);		
		displaylet->iClipArea=TRect(0,0,iXRes,iYRes);
	}

CanvasView::CanvasView(TInt aXRes,TInt aYRes)
	{
		setVisible(true);
		
		for (int c=0;c<NUM_FONTS;c++)		
			setFont(c,NULL);	
		
		iDisplaylet=new RPointerArray<CanvasDisplaylet>();	
		iCurrentDisplaylet=0;
		iXRes=aXRes;
		iYRes=aYRes;
		iGoto=-1;
		if (iXRes < 360)
			iScreenSize=SS_SMALL;
		else
			iScreenSize=SS_BIG;
			
	}

 void CanvasView::SendSignal(int aSignal)
	{
	setGoto(SIGN_NOP);
	}


CanvasView::~CanvasView()
	{
	iDisplaylet->ResetAndDestroy();
	delete iDisplaylet;
	iDisplaylet=NULL;
	}

void CanvasView::sendKey(TInt32 aKey)
	{	
	int to=iDisplaylet->Count();
	
	switch(aKey)
		{
		case UP:
			
			do
			iCurrentDisplaylet--;
			while (iCurrentDisplaylet>0 &&!((*iDisplaylet)[iCurrentDisplaylet])->iEnabled &&((*iDisplaylet)[iCurrentDisplaylet])->iGoto==SIGN_NOP);
				
			break;
		case DOWN:
			
			do
			iCurrentDisplaylet++;
			while (iCurrentDisplaylet<iDisplaylet->Count() &&!((*iDisplaylet)[iCurrentDisplaylet])->iEnabled && ((*iDisplaylet)[iCurrentDisplaylet])->iGoto==SIGN_NOP);
			
			break;
		case ENTER:
			setGoto(((*iDisplaylet)[iCurrentDisplaylet])->iGoto);
			break;			
			
		}
	if (iCurrentDisplaylet<0)
		iCurrentDisplaylet=0;
	if (iCurrentDisplaylet>=to)
		iCurrentDisplaylet=to-1;
	
	while (iCurrentDisplaylet<to && ((*iDisplaylet)[iCurrentDisplaylet])->iGoto==SIGN_NOP)
		iCurrentDisplaylet++;

	}




void CanvasView::Click(TPoint aPoint)
	{
	int emptyspace=0;
	CanvasDisplaylet *displaylet=NULL;
	TRect rect;
	for (int c=0;c<iDisplaylet->Count();c++)
		{		
		displaylet=(*iDisplaylet)[c];
		if (!displaylet->iVisible)
			{
			//	emptyspace+=displaylet->iRectArea.Height();//(displaylet->iRectArea.iBr.iY-displaylet->iRectArea.iTl.iY);
				emptyspace+=64;
				continue;
			}
		
		rect=displaylet->iRectArea;
		rect.Move(0,-emptyspace);
		if (rect.Contains(aPoint))
			setGoto(displaylet->iGoto);
		}
	}

void CanvasView::Centralize(CanvasDisplaylet *displaylet)
	{
	CentralizeHorizontal(displaylet);
	CentralizeVertical(displaylet);	
	}

void CanvasView::CentralizeHorizontal(CanvasDisplaylet *displaylet)
	{
	int x0,y0,x1,y1;
	TSize imageSize(displaylet->getDefaultImage()->SizeInPixels());
	TRect area(displaylet->iRectArea.iTl.iX,
				displaylet->iRectArea.iTl.iY,
				displaylet->iRectArea.iTl.iX+imageSize.iWidth,
				displaylet->iRectArea.iTl.iY+imageSize.iHeight);
	
	x0=(iXRes-imageSize.iWidth)/2;
	x1=x0+imageSize.iWidth;
	y0=displaylet->iRectArea.iTl.iY;
	y1=displaylet->iRectArea.iTl.iY+imageSize.iHeight;
		
	displaylet->iRectArea= TRect(x0,y0,x1,y1);	
	
	}

void CanvasView::CentralizeVertical(CanvasDisplaylet *displaylet)
	{
	int x0,y0,x1,y1;
	TSize imageSize(displaylet->getDefaultImage()->SizeInPixels());
	TRect area(displaylet->iRectArea.iTl.iX,
				displaylet->iRectArea.iTl.iY,
				displaylet->iRectArea.iTl.iX+imageSize.iWidth,
				displaylet->iRectArea.iTl.iY+imageSize.iHeight);
	
	x0=displaylet->iRectArea.iTl.iX;
	x1=displaylet->iRectArea.iTl.iX+imageSize.iWidth;
	y0=(iYRes-imageSize.iHeight)/2;
	y1=y0+imageSize.iHeight;
		
	displaylet->iRectArea= TRect(x0,y0,x1,y1);	
	
	}

void CanvasView::ExpandToImage(CanvasDisplaylet *displaylet)
	{
	if (displaylet->getDefaultImage()==NULL) return;	
	
	TSize imageSize(displaylet->getDefaultImage()->SizeInPixels());
	TRect area(
				displaylet->iRectArea.iTl.iX,
				displaylet->iRectArea.iTl.iY,
				displaylet->iRectArea.iTl.iX+imageSize.iWidth,
				displaylet->iRectArea.iTl.iY+imageSize.iHeight
	);
		
	displaylet->iRectArea= area;	
	
	}

