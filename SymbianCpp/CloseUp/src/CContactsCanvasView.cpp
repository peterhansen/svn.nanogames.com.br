/*
 * CContactsCanvasView.cpp
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#include <CloseUp.mbg>
#include <e32base.h>

#include <cntdb.h>
#include <cntitem.h>
#include <cntfldst.h>
#include <cpbkcontactengine.h>

#include "CContactsCanvasView.h"
#include "CloseUpContainer.h"

// quantidade de contatos visíveis ao mesmo tempo na tela
#define FONT_BIG 3
#define FONT_MEDIUM 2
#define FONT_SMALL 1
#define FONT_VERY_SMALL 4

// quantidade de contatos vis�veis ao mesmo tempo na tela
#define VISIBLE_CONTACTS 7

#define ENTRY_HEIGHT ( 0.075f * getYRes() )

#define ENTRIES_BOX_Y_TOP ( 0.4f * getYRes() )
#define ENTRIES_BOX_HEIGHT ( ENTRY_HEIGHT * VISIBLE_CONTACTS )
#define ENTRIES_BOX_Y_BOTTOM ( ENTRIES_BOX_Y_TOP + ENTRIES_BOX_HEIGHT )

#define SCROLL_LIMIT ( ENTRIES_BOX_Y_TOP - ( ENTRY_HEIGHT * pContacts->Count() ) )

CContactsCanvasView::CContactsCanvasView(TInt aXRes, TInt aYRes) :
	CanvasView(aXRes, aYRes), iScrolled(0), iSelection(0), iChanged(false),
			pContacts(NULL)
	{
	// TODO: Este construtor não está à prova de exceções!!! Temos um exemplo logo abaixo. Se o 2o new falhar, não iremos liberar a memória alocada
	// pelo 1o new, afinal, o C++ só chama os destrutores de objetos criados com sucesso. Este objeto ainda não foi completamente criado (estamos em
	// seu construtor), logo, em casos de exceções, seu destrutor não será chamado. Para resolver estes problemas temos que utilizar smart pointers!!!
	// Ver Herb Sutter e Scott Meyers como referência de literatura
	pContacts = new (ELeave) RPointerArray<CCloseUpContact> ();
	CanvasDisplaylet *pDisplaylet = new CanvasDisplaylet();
	CleanupStack::PushL(pDisplaylet);
		{
		pDisplaylet->iRectArea = TRect(0, 0, getXRes(), getYRes());

		if (getScreenSize() == SS_SMALL)
			pDisplaylet->setDefaultImage(CCloseUpContainer::LoadBMP(
					EMbmCloseupQvgacontacts));
		else
			pDisplaylet->setDefaultImage(CCloseUpContainer::LoadBMP(
					EMbmCloseupContacts));

		pDisplaylet->iEnabled = false;
		addDisplaylet(pDisplaylet);
		}
	CleanupStack::Pop(pDisplaylet);
	ImportContactsFromAddressBook();
	}

CContactsCanvasView::~CContactsCanvasView()
	{
	pContacts->ResetAndDestroy();
	delete pContacts;
	}

void CContactsCanvasView::changeContactsDisplayState()
	{
	// Torna visivel o displaylet sob o cursor + 4
	CanvasDisplaylet *pAux;
	if (getCurrentDisplaylet() - 2 >= 1)
		{
		pAux = getDisplaylet(getCurrentDisplaylet() - 2);
		pAux->iSelectedFont = BF_SMALL;
		pAux->iVisible = true;
		}

	if (getCurrentDisplaylet() - 1 >= 1)
		{
		pAux = getDisplaylet(getCurrentDisplaylet() - 1);
		pAux->iSelectedFont = BF_MEDIUM;
		pAux->iVisible = true;
		}

	pAux = getDisplaylet(getCurrentDisplaylet());
	pAux->iSelectedFont = BF_BIG;
	pAux->iVisible = true;

	if (getCurrentDisplaylet() + 1 < getTotalDisplaylets())
		{
		pAux = getDisplaylet(getCurrentDisplaylet() + 1);
		pAux->iSelectedFont = BF_MEDIUM;
		pAux->iVisible = true;
		}

	if (getCurrentDisplaylet() + 2 < getTotalDisplaylets())
		{
		pAux = getDisplaylet(getCurrentDisplaylet() + 2);
		pAux->iSelectedFont = BF_SMALL;
		pAux->iVisible = true;
		}
	

	}

void CContactsCanvasView::setCurrentContact(int index)
	{

	if (index < 0)
		index = 0;
	else if (index >= pContacts->Count())
		index = pContacts->Count() - 1;

	int relativeY = (index * ENTRY_HEIGHT) + iScrolled;
	
	TBuf16<40> num;
	num.AppendNum(relativeY);
	num.Append(',');
	num.AppendNum(ENTRIES_BOX_HEIGHT);
	User::InfoPrint(num);

	if (relativeY < 0)
		ScrollY(-relativeY, false);
	else if (relativeY > ENTRIES_BOX_HEIGHT - ENTRY_HEIGHT)
		ScrollY(ENTRIES_BOX_HEIGHT - ENTRY_HEIGHT - relativeY, false);
	else
		ScrollY(0, false);
	}

void CContactsCanvasView::sendKey(TInt32 aKey)
	{
	CanvasView::sendKey(aKey);

	TInt currentDisplaylet = getCurrentDisplaylet();
	switch (aKey)
		{
		case CanvasView::UP:
		case CanvasView::LEFT:
			if (getCurrentDisplaylet() <= 1)
				setCurrentContact(0);		
			break;
/*
		case CanvasView::DOWN:
		case CanvasView::RIGHT:
			if (getCurrentDisplaylet() == getTotalDisplaylets() - 1)
				setCurrentContact(0);
			else
				setCurrentDisplaylet(getCurrentDisplaylet()+1);

			setCurrentContact(getCurrentDisplaylet()-1);
			break;
			*/
		}
	
	setCurrentContact(getCurrentDisplaylet()-1);
	changeContactsDisplayState();
	}

TFileName CContactsCanvasView::getPictureName(int aSelection)
	{
	TFileName filename;
	TFindFile findfile(CCoeEnv::Static()->FsSession());
	if (!iSelection)
		filename.Copy(_L("\\resource\\apps\\eles.bmp"));
	else
		filename.Copy(_L("\\resource\\apps\\elas.bmp"));

	if (KErrNone == findfile.FindByDir(filename, KNullDesC))
		return findfile.File();
	else
		return _L16("");

	}

TFileName CContactsCanvasView::getRingToneName(int aSelection)
	{
	TFileName filename;
	TFindFile findfile(CCoeEnv::Static()->FsSession());
	if (!iSelection)
		filename.Copy(_L("\\resource\\apps\\de1up_edu_120.mp3"));
	else
		filename.Copy(_L("\\resource\\apps\\de1up_copa_120.mp3"));

	if (KErrNone == findfile.FindByDir(filename, KNullDesC))
		return findfile.File();
	else
		return _L16("");

	}


void CContactsCanvasView::showing()
	{/*
		setVisible(false);			
		
		
		pContacts->Reset();
		//ImportContactsFromAddressBook();
		setVisible(true);
		*/
	}

void CContactsCanvasView::ToggleContact(int index)
	{
	TRgb gender[2];
	gender[0] = TRgb(0, 0, 255);
	gender[1] = TRgb(255, 0, 0);
	TFileName mp3file=getRingToneName(iSelection);
	//TFileName bmpfile=getPictureName(iSelection);
	CanvasDisplaylet *pDisplaylet = getDisplaylet(index + 1);
	
	if (!((*pContacts)[index])->iSelected)
		{
			pDisplaylet->iState = 1;			
			pDisplaylet->iSelectedColor = gender[iSelection];
			((*pContacts)[index])->iSelected=iSelection+1;
			
			AddRingtoneForContactL(mp3file,
					((*pContacts)[index])->iContactId);
			
			/*
			CFbsBitmap *ptr=CCloseUpContainer::LoadBMP(EMbmCloseupEles);
			RWriteStream stream;
			RReadStream read;
			ptr->ExternalizeL(stream);
			RArray<TInt8> rawbytes;
			stream.CommitL();
			read.ReadL(stream);
		//	stream.Release();
			
			
			AddPictureForContactL(read,
					((*pContacts)[index])->iContactId);
			//read.Close();
			//read.Release();
			 * 
			 */
			 
		}
	else
		{
			pDisplaylet->iState = 0;
			((*pContacts)[index])->iSelected=0;
			RemoveRingtoneForContactL(((*pContacts)[index])->iContactId);	
		//	RemovePictureForContactL(((*pContacts)[index])->iContactId);
		}
	}

void CContactsCanvasView::SendSignal(int aSignal)
	{
	TInt index = SIGN_NOP - aSignal - 1;
	if (index < 0)
		return;
	setGoto(CV_FINISHED);
	ToggleContact(index);
	}

void CContactsCanvasView::ScrollY(int aDelta, bool setCurrentIndex)
	{
	int previousScroll = iScrolled;

	iScrolled += aDelta;

	if (iScrolled > 0)
		iScrolled = 0;
	else if (iScrolled < SCROLL_LIMIT)
		iScrolled = SCROLL_LIMIT;

	TInt nContacts = pContacts->Count();
	for (int c = 0; c < nContacts; c++)
		getDisplaylet(c + 1)->iRectArea.Move(0, iScrolled - previousScroll);

	int index = getCurrentDisplaylet() - 1;
	if (index < 0)
		index = 0;
	else if (index >= pContacts->Count())
		index = pContacts->Count() - 1;

	for (int i = 0; i < pContacts->Count(); ++i)
		{
		int diff = i - index;
		if (diff < 0)
			diff = -diff;

		switch (diff)
			{
			case 0:
				getDisplaylet(i + 1)->iSelectedFont = FONT_BIG;
				break;

			case 1:
				getDisplaylet(i + 1)->iSelectedFont = FONT_MEDIUM;
				break;

			case 2:
				getDisplaylet(i + 1)->iSelectedFont = FONT_SMALL;
				break;

			default:
				getDisplaylet(i + 1)->iSelectedFont = FONT_VERY_SMALL;
				break;
			}
		}

	if (setCurrentIndex)
		{
		int firstIndex = -iScrolled / ENTRY_HEIGHT;
		setCurrentContact(firstIndex + (VISIBLE_CONTACTS / 2));
		setCurrentDisplaylet(firstIndex + (VISIBLE_CONTACTS / 2) + 1);
		}

	}

void CContactsCanvasView::AddRingtoneForContactL(const TDesC& aRingtone,
		TContactItemId aId)
	{
	CContactDatabase* ContactDb = CContactDatabase::OpenL();
	CleanupStack::PushL(ContactDb);

	CContactItem* SelItem = ContactDb->OpenContactL(aId);
	CleanupStack::PushL(SelItem);

	TInt RIndex = SelItem->CardFields().Find(KUidContactFieldRingTone);
	if (RIndex != KErrNotFound)
		SelItem->RemoveField(RIndex);

	CContactItemField* contactField = CContactItemField::NewLC(
			KStorageTypeText, KUidContactFieldRingTone);
	contactField->TextStorage()->SetTextL(aRingtone);
	contactField->SetLabelL(_L("tone"));
	contactField->SetMapping(KUidContactFieldVCardMapUnknown);
	contactField->SetId(2);

	SelItem->AddFieldL(*contactField);
	CleanupStack::Pop();//ContactField		
	ContactDb->CommitContactL(*SelItem);

	CleanupStack::PopAndDestroy(2);//ContactDb,SelItem
	}

void CContactsCanvasView::RemoveRingtoneForContactL(TContactItemId aId)
	{
	CContactDatabase* ContactDb = CContactDatabase::OpenL();
	CleanupStack::PushL(ContactDb);

	CContactItem* SelItem = ContactDb->OpenContactL(aId);
	CleanupStack::PushL(SelItem);

	TInt RIndex = SelItem->CardFields().Find(KUidContactFieldRingTone);
	if (RIndex != KErrNotFound)
		SelItem->RemoveField(RIndex);

	ContactDb->CommitContactL(*SelItem);

	CleanupStack::PopAndDestroy(2);//ContactDb,SelItem
	}


void CContactsCanvasView::AddPictureForContactL( RReadStream &aPicture,
		TContactItemId aId)
	{
	   CContactDatabase* contactsDb = CContactDatabase::OpenL();
	   CleanupStack::PushL( contactsDb );
	 
	   CContactItem* contactItem = contactsDb->OpenContactL( aId );
	   CleanupStack::PushL( contactItem );
	   TInt index = contactItem->CardFields().Find( KUidContactFieldPicture );
	   if ( index != KErrNotFound )
	     {
	     contactItem->RemoveField( index );
	     }
	 
	   CContactItemField* field;
	   field = CContactItemField::NewLC( KStorageTypeStore, 
	                                     KUidContactFieldPicture );
	   field->SetMapping( KUidContactFieldVCardMapPHOTO );
	   field->AddFieldTypeL( KUidContactFieldVCardMapBMP );
	   field->ResetStore();
	 
	   // aData contains the thumbnails image data.
	   
	   field->StoreStorage()->InternalizeL(aPicture);
	   contactItem->AddFieldL( *field );
	   CleanupStack::Pop( field );
	 
	   // Save and close the contacts database
	   contactsDb->CommitContactL( *contactItem );
	   contactsDb->CloseContactL( aId );
	   CleanupStack::PopAndDestroy(2); // contactItem, contactsDb
	}

void CContactsCanvasView::RemovePictureForContactL(TContactItemId aId)
	{
	CContactDatabase* ContactDb = CContactDatabase::OpenL();
	CleanupStack::PushL(ContactDb);

	CContactItem* SelItem = ContactDb->OpenContactL(aId);
	CleanupStack::PushL(SelItem);

	TInt RIndex = SelItem->CardFields().Find(KUidContactFieldPicture);
	if (RIndex != KErrNotFound)
		SelItem->RemoveField(RIndex);

	ContactDb->CommitContactL(*SelItem);

	CleanupStack::PopAndDestroy(2);//ContactDb,SelItem
	}



void CContactsCanvasView::ImportContactsFromAddressBook()
	{
	// Ordena os contatos
		CanvasDisplaylet *pDisplaylet;
		CContactDatabase* contactsDb = CContactDatabase::OpenL();
		contactsDb->SetDbViewContactType(KUidContactItem);
		CArrayFixFlat<CContactDatabase::TSortPref>* aSortOrder =
				new (ELeave) CArrayFixFlat<CContactDatabase::TSortPref> (1);
		CleanupStack::PushL(aSortOrder);
			{
			CContactDatabase::TSortPref check;
			check.iFieldType = KUidContactFieldFamilyName;
			check.iOrder = CContactDatabase::TSortPref::EAsc;

			CContactDatabase::TSortPref check1;
			check1.iFieldType = KUidContactFieldGivenName;
			check.iOrder = CContactDatabase::TSortPref::EAsc;

			aSortOrder->AppendL(check1);
			aSortOrder->AppendL(check);

			contactsDb->SortL(aSortOrder);
			}
		CleanupStack::Pop(aSortOrder);

		// Itera pela lista de contatos
		TContactIter iter(*contactsDb);
		TContactItemId cardId;
		TBuf16<255> text;
		bool naotem;

		while ((cardId = iter.NextL()) != KNullContactId)
			{
			naotem=true;
			CCloseUpContact *contact = new CCloseUpContact();
			CleanupStack::PushL(contact);
				{
				CContactItem* card = contactsDb->ReadContactL(cardId);
				contact->iContactId = cardId;

				TInt pField = card->CardFields().Find(KUidContactFieldPhoneNumber);
				TInt nField = card->CardFields().Find(KUidContactFieldGivenName);
				TInt fField = card->CardFields().Find(KUidContactFieldFamilyName);
				TInt rField = card->CardFields().Find(KUidContactFieldRingTone);
				
				
				
				
				if (nField != KErrNotFound)
					{
					CContactItemFieldSet &set = card->CardFields();
					CContactItemField& field = set[nField];
					text.Copy(field.TextStorage()->Text());
					text.Trim();
					contact->iName.Copy(text);
					}

				if (pField != KErrNotFound)
					{
					CContactItemFieldSet &set = card->CardFields();
					CContactItemField& field = set[pField];
					text.Copy(field.TextStorage()->Text());
					text.Trim();
					contact->iNumber.Copy(text);
					}

				if (fField != KErrNotFound)
					{
					CContactItemFieldSet &set = card->CardFields();
					CContactItemField& field = set[fField];
					text.Copy(field.TextStorage()->Text());
					text.Trim();
					contact->iSurname.Copy(text);
					}

				if (rField != KErrNotFound)
					{
						CContactItemFieldSet &set = card->CardFields();
						CContactItemField& field = set[rField];
						text.Copy(field.TextStorage()->Text());
						text.Trim();
						if (text.Find(_L16("de1up_copa_120.mp3"))!=KErrNotFound)
							contact->iSelected=2;
						else
						if (text.Find(_L16("de1up_edu_120.mp3"))!=KErrNotFound)
							contact->iSelected=1;
						else
							contact->iSelected=0;
					}		
				else
					contact->iSelected=0;
				/*
				CContactItemFieldSet &set = card->CardFields();
				for (int c=0;c<card->CardFields().Count();c++)					
					{				
					CContactItemField& field = set[c];
					if (field.TextStorage()->Text().Length()>0)
						{
						contact->iSurname.Append(field.Label());
						contact->iSurname.Append('=');
						contact->iSurname.Append(field.);
						}
				
					}
				*/
				
				
		
				
				
				if (contact->iNumber.Length() > 0 && naotem)
					{
						pContacts->AppendL(contact);
					}
				else
					{
					// Acho que não tem problema deletarmos o ponteiro e depois
					// passarmos seu endereço para CleanupStack::Pop(). Afinal,
					// tal método só utiliza o argumento para uma checagem no
					// modo debug
					delete contact;
					}
				contactsDb->CloseContactL(card->Id());
				}
			CleanupStack::Pop(contact);

			}

		contactsDb->CloseTables();

		TInt nContacts = pContacts->Count();
		for (TInt c = 0; c < nContacts; ++c)
			{
			pDisplaylet = new (ELeave) CanvasDisplaylet();
			CleanupStack::PushL(pDisplaylet);
				{
				pDisplaylet->iGoto = SIGN_NOP - (1 + c);

				TBuf16<255> *ptr = new (ELeave) TBuf16<255> ();
				CleanupStack::PushL(ptr);
					{
					ptr->Copy((*pContacts)[c]->iName);
					ptr->Append(' ');
					ptr->Append((*pContacts)[c]->iSurname);
					pDisplaylet->iText.Append(ptr);

					}
				CleanupStack::Pop(ptr);
				addDisplaylet(pDisplaylet);
				pDisplaylet->iClipArea = TRect(0, ENTRIES_BOX_Y_TOP, getXRes(),
						ENTRIES_BOX_Y_BOTTOM);
				pDisplaylet->iRectArea = TRect(0.1 * getXRes(), ENTRIES_BOX_Y_TOP
						+ c * ENTRY_HEIGHT, getXRes(), ENTRIES_BOX_Y_TOP + (c + 1)
						*ENTRY_HEIGHT);
				}
			CleanupStack::Pop(pDisplaylet);
			}
		setCurrentDisplaylet(VISIBLE_CONTACTS / 2);
		setCurrentContact(VISIBLE_CONTACTS / 2);
		
		changeContactsDisplayState();
		
		
		///torna os contatos consistentes
			{
			TRgb gender[2];
			gender[0] = TRgb(0, 0, 255);
			gender[1] = TRgb(255, 0, 0);
		
			int oldSelection=iSelection;
			nContacts=pContacts->Count();
			for (int c=0;c<nContacts;++c)		
				if ((*pContacts)[c]->iSelected)
					{	
						pDisplaylet=getDisplaylet(c+1);
						pDisplaylet->iState=1;
						pDisplaylet->iSelectedColor=gender[(*pContacts)[c]->iSelected-1];					
					}
			iSelection=oldSelection;
			}
	}


/*
void CContactsCanvasView::SaveSelectedContactsL()
	{
	TBuf8<0xFF> buffer; ///TODO: numero magico->FORA!
	TFileName filename;
	RFile file;
	RFs &fs = CCoeEnv::Static()->FsSession();
	TInt32 count = pSelectedContacts->Count();

	for (int c = 0; c < count; ++c)
		{
		for (int d = 0; d
				< ((*pContacts)[(*pSelectedContacts)[c]])->iNumber.Length(); ++d)
			buffer.Append(
					(TChar) ((*pContacts)[(*pSelectedContacts)[c]])->iNumber[d]);
		buffer.Append('\n');
		}

	filename.Copy(_L("D:\\"));
	filename.Append(_L("closeup.num"));
	User::LeaveIfError(file.Replace(fs, filename, EFileWrite));
	User::LeaveIfError(file.Create(fs, filename, EFileWrite | EFileShareAny));
	CleanupClosePushL(file);
	User::LeaveIfError(file.Write(buffer, buffer.Length()));
	CleanupStack::PopAndDestroy(&file);
	}

void CContactsCanvasView::LoadSelectedContactsL()
	{
	TFileName filename;
	TBuf8<0xFF> numbers;
	TBuf16<0xFF> data16;///TODO: numero magico->FORA!
	TBuf16<3 * NUMCHARSTOCOMPARE> number;
	RFile file;
	TInt fileSize;
	int numcontacts;

	filename.Copy(_L("D:\\"));
	filename.Append(_L("closeup.num"));
	TFindFile findfile(CCoeEnv::Static()->FsSession());

	if (findfile.FindByDir(filename, KNullDesC) == KErrNone)
		{
		data16.Zero();
		numbers.Zero();
		User::LeaveIfError(file.Open(CCoeEnv::Static()->FsSession(),
				findfile.File(), EFileRead));

		CleanupClosePushL(file);
			{
			User::LeaveIfError(file.Size(fileSize));
			if (numbers.MaxLength() < fileSize)
				User::Leave(KErrOverflow);
			User::LeaveIfError(file.Read(numbers, fileSize));
			}
		CleanupStack::PopAndDestroy(&file);

		data16.Copy(numbers);
		User::InfoPrint(data16);
		
		numcontacts = pContacts->Count();
		for (int c = 0; c < numcontacts; ++c)
			{
			number.Zero();
			number.Copy(((*pContacts)[c])->iNumber);

			if (number.Length() > 0 && data16.Find(number) != KErrNotFound)
				ToggleContact(c);
			}

		}

	}
*/
/**
 * void AssignPicturePath( const TDesC& aImagePath, TContactItemId aId )
 *
 * Updates a specified contact item (aId) with a full-size image path
 * (aImagePath)
 *
 * TODO não testado! Retirado de http://wiki.forum.nokia.com/index.php/TSS001074_-_Updating_contact%27s_images_in_S60_3rd_Ed,_FP2_devices
 */
/*
 void CContactsCanvasView::AssignPicturePath( const TDesC& aImagePath, TContactItemId aId )
 {
 // create pbkengine.
 CPbkContactEngine* pbkContactEng = CPbkContactEngine::NewL( &iCoeEnv->FsSession());
 CleanupStack::PushL( pbkContactEng );
 // Get image field info if there is no image assigned initially
 const CPbkFieldsInfo& fieldsInfo = pbkContactEng->FieldsInfo();
 CpbkFieldInfo* pbkFieldInfo = fieldsInfo.Find( EPbkFieldIdCodImageID );
 // Open the contact item specified by aId
 CPbkContactItem* aContactItem = pbkContactEng->OpenContactL( aId );
 CleanupStack::PushL( aContactItem );
 // Get the image field info
 TPbkContactItemField* info = aContactItem->FindField( EPbkFieldIdCodImageID );
 if ( !info )
 {
 // Add the image field if it was not found
 TPbkContactItemField info = aContactItem->AddFieldL( *pbkFieldInfo );
 if( KStorageTypeText == info.StorageType() )
 {
 // set path for the new image
 CContactTextField* textstore = info.TextStorage();
 textstore->SetTextL( aImagePath );   
 }           
 }
 else
 {
 if( KStorageTypeText == info->StorageType() )
 {
 // Update with the new image path
 CContactTextField* textstore = info->TextStorage();
 textstore->SetTextL( aImagePath );   
 }     
 }
 
 // Save and close the contacts database
 pbkContactEng->CommitContactL( *aContactItem, EFalse );
 pbkContactEng->CloseContactL( aId );
 CleanupStack::PopAndDestroy( 2 ); // aContactItem, pbkContactEng
 }
 */
