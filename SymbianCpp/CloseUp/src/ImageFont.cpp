#include "ImageFont.h"

ImageFont::ImageFont( CFbsBitmap *pImage ) {
	this->pImage = pImage;
}


ImageFont::~ImageFont() {
	delete pImage;
}


/**
 * Cria uma nova inst�ncia de fonte monoespa�ada.
 * @param image imagem que cont�m os caracteres da fonte.
 * @param characters caracteres presentes na imagem.
 * @return refer�ncia para a fonte criada, ou null caso haja erro ao alocar recursos ou par�metros inv�lidos.
 * @throws java.lang.Exception 
 */
  ImageFont::ImageFont(int aTrailingSpace,int aSpacing, CFbsBitmap *aImage,CFbsBitmap *aImageMask, TBuf16<255> *characters )  
			{
	  iChars.Copy(*characters);
	  iTrailingSpace=aTrailingSpace;
	  iSpacing=aSpacing;
	pImage=aImage;
	pImageMask=aImageMask;
	int IMAGE_WIDTH = pImage->SizeInPixels().iWidth;
/*
	if ( IMAGE_WIDTH % characters->Length() != 0 ) 
		{
		User::InfoPrint(_L("fudeu:nao carregou imagem direito"));
	return;
		}
*/
	// pr�-calcula a largura dos caracteres
	int charWidth = ( IMAGE_WIDTH / characters->Length() );
//	char chars[ characters.Length() ];
//	characters.getChars( 0, characters.length(), chars, 0 ); // copia pro array

	// preenche o array de tamanhos de caracteres
	for ( int i = 0; i < characters->Length(); ++i )		
		charsWidths[ (*characters)[ i ] ] = charWidth;
		
	calculateOffsets( characters );


} // fim do m�todo createMonoSpacedFont()


/**
 * Cria uma nova inst�ncia de fonte multiespa�ada.
 * @param image imagem que cont�m os caracteres da fonte.
 * @param fontDataFile endere�o do arquivo bin�rio que descreve a tira de fonte.
 * @return refer�ncia para a fonte criada, ou null caso haja erro ao alocar recursos ou par�metros inv�lidos.
 * @throws java.lang.Exception 
 */

  ImageFont::ImageFont(int aTrailingSpace,int aSpacing, CFbsBitmap* aImage,CFbsBitmap *aImageMask, TBuf16<255> *characters,RArray<int> *fontData ) 
	{
	  iSpacing=aSpacing;
	  iTrailingSpace=aTrailingSpace;
	  pImage=aImage;
	  pImageMask=aImageMask;
		// obt�m o n�mero total de caracteres da fonte
		int numberOfChars = fontData->Count();
		int c;
		
		// armazena as larguras de cada caracter
		for ( int i = 0; i < numberOfChars; ++i ) {
			c = (*characters)[i];
			if (c<0 || c>CHAR_TABLE_SIZE)
				User::Leave(-128);
			charsWidths[ c ] = (*fontData)[ i ];
		}
		
		calculateOffsets( characters );	
		
	}
 // fim do m�todo createMultiSpacedFont()


/**
 *	M�todo auxiliar para calcular e atualizar os offsets de cada caracter na fonte.
 * 
 * @param characters 
 */
void ImageFont::calculateOffsets( TBuf16<255> *characters) 
	{
	int offset = 0;
	
	for ( int i = 0; i < characters->Length(); ++i ) 
		{
	
		charsOffsets[ (*characters)[ i ] ] = offset;
		offset += charsWidths[ (*characters)[ i ] ];
		}
	
	} 


/**
 * Retorna a largura em pixels de um texto.
 * 
 * @param text texto cuja largura ser� calculada.
 * @return largura do texto em pixels.
 * @throws NullPointerException caso text seja null.
 * @see #getTextWidth(char[])
 * @see #getTextWidth(int)
 */
int ImageFont::getTextWidth( TBuf16<255> *text ) 
	{
	int width = 0;

	for ( int i = 0; i < text->Length(); ++i )
		width += getCharWidth( (*text)[ i ] );

	// a largura � corrigida para que o �ltimo caracter seja exibido corretamente
	return width > 0 ? width - charExtraOffset : 0;
} // fim do m�todo getTextWidth( String )


/**
 * Retorna a largura de um caracter da fonte.
 * @param c caracter cuja largura ser� retornada.
 * @return largura do caracter em pixels.
 */
int ImageFont::getCharWidth( TChar c ) {
	// n�o � necess�rio verificar se � menor que zero pois char � unsigned
	return ( ( c < CHAR_TABLE_SIZE && charsWidths[c] > 0 ) ? charsWidths[ c ] + charExtraOffset : 0 );
} // fim do m�todo getCharWidth( char )	 


CFbsBitmap* ImageFont::getImage() {
	return pImage;
}


/**
 * Obt�m a altura da fonte.
 * 
 * @return inteiro indicando a altura dos caracteres da fonte.
 */
int ImageFont::getHeight() {
	return pImageMask->SizeInPixels().iHeight;
}


/**
 *
 * @param offset
 */
void ImageFont::setCharExtraOffset( TInt extraOffset ) {
	charExtraOffset = extraOffset;
}


/**
 * 
 * @return
 */
int ImageFont::getCharExtraOffset() {
	return charExtraOffset;
}

int ImageFont::getCharOffset( TChar c ) {
	return c < CHAR_TABLE_SIZE ? charsOffsets[ c ] : 0;
}
