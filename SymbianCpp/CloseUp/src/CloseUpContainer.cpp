/*
 ========================================================================
 Name        : CloseUpContainer.cpp
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
 ========================================================================
 */
// [[[ begin generated region: do not modify [Generated System Includes]

#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>
#include <aknutils.h>

#include <eikenv.h>
// ]]] end generated region [Generated System Includes]

#include "CSplashCanvasView.h"
#include "CMainCanvasView.h"
#include "CGenderCanvasView.h"
#include "CContactsCanvasView.h"
#include "CFinishedCanvasView.h"
#include "CAboutCanvasView.h"
#include "CExplosionCanvasView.h"
// [[[ begin generated region: do not modify [Generated User Includes]
#include "CloseUpContainer.h"
#include "CloseUpContainerView.h"
#include "CloseUp.hrh"
// ]]] end generated region [Generated User Includes]

// [[[ begin generated region: do not modify [Generated Constants]
// ]]] end generated region [Generated Constants]
#include <CloseUp.mbg>
#include "ImageFont.h"
#include "Label.h"

// tolerancia no scroll
#define SCROLL_Y_TOLERANCE 15

CanvasView* CCloseUpContainer::getCanvasView(TInt32 aId)
	{
	return (*iViews)[aId];
	}

/**
 * First phase of Symbian two-phase construction. Should not 
 * contain any code that could leave.
 */
CCloseUpContainer::CCloseUpContainer()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]

	}
/** 
 * Destroy child controls.
 */
CCloseUpContainer::~CCloseUpContainer()
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	iPeriodicTimer->Cancel();
	delete iPeriodicTimer;
	for (int c = 0; c < iViews->Count(); c++)
		delete (*iViews)[c];
	delete iViews;

	// Release double buffering classes
	if (iBackBufferContext)
		{
		delete iBackBufferContext;
		iBackBufferContext = NULL;
		}
	if (iBackBufferDevice)
		{
		delete iBackBufferDevice;
		iBackBufferDevice = NULL;
		}
	if (iBackBuffer)
		{
		delete iBackBuffer;
		iBackBuffer = NULL;
		}
	iBackBufferSize = TSize(0, 0);

	}

/**
 * Construct the control (first phase).
 *  Creates an instance and initializes it.
 *  Instance is not left on cleanup stack.
 * @param aRect bounding rectangle
 * @param aParent owning parent, or NULL
 * @param aCommandObserver command observer
 * @return initialized instance of CCloseUpContainer
 */
CCloseUpContainer* CCloseUpContainer::NewL(const TRect& aRect,
		const CCoeControl* aParent, MEikCommandObserver* aCommandObserver)
	{
	CCloseUpContainer* self = CCloseUpContainer::NewLC(aRect, aParent,
			aCommandObserver);
	CleanupStack::Pop(self);
	return self;
	}

/**
 * Construct the control (first phase).
 *  Creates an instance and initializes it.
 *  Instance is left on cleanup stack.
 * @param aRect The rectangle for this window
 * @param aParent owning parent, or NULL
 * @param aCommandObserver command observer
 * @return new instance of CCloseUpContainer
 */
CCloseUpContainer* CCloseUpContainer::NewLC(const TRect& aRect,
		const CCoeControl* aParent, MEikCommandObserver* aCommandObserver)
	{
	CCloseUpContainer* self = new (ELeave) CCloseUpContainer();
	CleanupStack::PushL(self);
	self->ConstructL(aRect, aParent, aCommandObserver);
	return self;
	}

  void CCloseUpContainer::PrepareForFocusLossL()
	{
	  iVisible=false;
	
	}

  void CCloseUpContainer::PrepareForFocusGainL()
	  {
	  iVisible=true;	  
	  }


void CCloseUpContainer::Tick()
	{
	if (!IsFocused()) 
		return;
	
	CExplosionCanvasView *ptr1 = (CExplosionCanvasView*) getCanvasView(
			CV_EXPLOSION);
	CGenderCanvasView *ptr2 = (CGenderCanvasView*) getCanvasView(CV_GENDER);
	CMainCanvasView *ptr3 = (CMainCanvasView*) getCanvasView(CV_MAIN);
	CContactsCanvasView *ptr4 = (CContactsCanvasView*) getCanvasView(
			CV_CONTACTS);

	ptr4->setSelection(ptr2->getSelection());
	ptr3->setContacts(ptr4->getContacts());
	if (ptr3->getState())
		{		
		ptr1->setSelection(ptr2->getSelection());
		ptr1->SendSignal(ptr2->getSelection());
		getCanvasView(iCurrentView)->setGoto(CV_EXPLOSION);
		ptr3->setState(0);
		}

	if (getCanvasView(iCurrentView)->getGoto() != SIGN_NOP)
		{
		if (getCanvasView(iCurrentView)->getGoto() > 0)
			{
			TInt32 oldid = iCurrentView;
			getCanvasView(iCurrentView)->hiding();
			iCurrentView = getCanvasView(iCurrentView)->getGoto();
			getCanvasView(oldid)->setGoto(SIGN_NOP);
			getCanvasView(iCurrentView)->showing();
			}
		else			
			getCanvasView(iCurrentView)->SendSignal(
					getCanvasView(iCurrentView)->getGoto());			
		}

	DrawDeferred();
	}

TInt CCloseUpContainer::CallBack(TAny* aAny)
	{
	CCloseUpContainer* self = static_cast<CCloseUpContainer*> (aAny);

	// TODO: The below call is a sample function,
	// you may change it to your requirement.
	self->Tick();

	// Cancel the timer when the callback should not be called again.
	// Call: self->iPeriodicTimer->Cancel();

	return KErrNone; // Return value ignored by CPeriodic
	}

void CCloseUpContainer::initFonts(TScreenSizes aScreenSize)
	{
	_LIT16(KChars,"abcdefghijklmnopqrstuvwxyz0123456789áâàãéêíóôõú,:-+?!/@©%*'\"()ç");
	TBuf16<255> *tmp = NULL;
	RArray<int> *widths = NULL;

	switch (aScreenSize)
		{
		case SS_SMALL:
			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(7);
			(*widths)['b' - 'a'] = 6;
			(*widths)['c' - 'a'] = 6;
			(*widths)['d' - 'a'] = 6;
			(*widths)['e' - 'a'] = 5;
			(*widths)['f' - 'a'] = 5;
			(*widths)['g' - 'a'] = 6;
			(*widths)['h' - 'a'] = 6;
			(*widths)['i' - 'a'] = 2;
			(*widths)['j' - 'a'] = 4;
			(*widths)['k' - 'a'] = 7;
			(*widths)['l' - 'a'] = 5;
			(*widths)['m' - 'a'] = 8;
			(*widths)['n' - 'a'] = 7;
			(*widths)['o' - 'a'] = 6;
			(*widths)['p' - 'a'] = 6;
			(*widths)['q' - 'a'] = 6;
			(*widths)['r' - 'a'] = 6;
			(*widths)['s' - 'a'] = 7;
			(*widths)['t' - 'a'] = 6;
			(*widths)['u' - 'a'] = 6;
			(*widths)['v' - 'a'] = 9;
			(*widths)['w' - 'a'] = 10;
			(*widths)['x' - 'a'] = 7;
			(*widths)['y' - 'a'] = 8;
			(*widths)['z' - 'a'] = 6;
			(*widths)['z' - 'a'] = 6;

			(*widths)[26] = 6;
			(*widths)[27] = 4;
			(*widths)[28] = 6;
			(*widths)[29] = 6;
			(*widths)[30] = 7;
			(*widths)[31] = 6;
			(*widths)[32] = 6;
			(*widths)[33] = 6;
			(*widths)[34] = 6;
			(*widths)[35] = 6;

			(*widths)[36] = 7;
			(*widths)[37] = 7;
			(*widths)[38] = 7;
			(*widths)[39] = 7;
			(*widths)[40] = 5;
			(*widths)[41] = 5;
			(*widths)[42] = 3;
			(*widths)[43] = 6;
			(*widths)[44] = 6;
			(*widths)[45] = 6;
			(*widths)[46] = 6;
			
			(*widths)[47] = 4;
			(*widths)[48] = 2;
			(*widths)[49] = 6;
			(*widths)[50] = 6;
			(*widths)[51] = 6;
			(*widths)[52] = 2;
			(*widths)[53] = 5;
			(*widths)[54] = 11;
			(*widths)[55] = 9;
			(*widths)[56] = 9;
			(*widths)[57] = 5;
			(*widths)[58] = 3;
			(*widths)[59] = 4;
			(*widths)[60] = 3;			
			(*widths)[61] = 3;			
			(*widths)[60] = 6;			

			(*widths)[61] = 6;	
			(*widths)[62] = 6;	
			(*widths)[63] = 6;	
			
			
			
			iFontes[1] = new Label(new ImageFont(5, 1, LoadBMP(
					EMbmCloseupQvgafont_small), LoadBMP(
					EMbmCloseupQvgafont_small_mask), tmp, widths), true);

			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(7);
			(*widths)['b' - 'a'] = 7;
			(*widths)['c' - 'a'] = 7;
			(*widths)['d' - 'a'] = 7;
			(*widths)['e' - 'a'] = 6;
			(*widths)['f' - 'a'] = 6;
			(*widths)['g' - 'a'] = 7;
			(*widths)['h' - 'a'] = 7;
			(*widths)['i' - 'a'] = 3;
			(*widths)['j' - 'a'] = 5;
			(*widths)['k' - 'a'] = 8;
			(*widths)['l' - 'a'] = 6;
			(*widths)['m' - 'a'] = 10;
			(*widths)['n' - 'a'] = 8;
			(*widths)['o' - 'a'] = 7;
			(*widths)['p' - 'a'] = 7;
			(*widths)['q' - 'a'] = 7;
			(*widths)['r' - 'a'] = 7;
			(*widths)['s' - 'a'] = 8;
			(*widths)['t' - 'a'] = 7;
			(*widths)['u' - 'a'] = 7;
			(*widths)['v' - 'a'] = 7;
			(*widths)['w' - 'a'] = 12;
			(*widths)['x' - 'a'] = 9;
			(*widths)['y' - 'a'] = 9;
			(*widths)['z' - 'a'] = 7;

			(*widths)[26] = 7;
			(*widths)[27] = 4;
			(*widths)[28] = 7;
			(*widths)[29] = 7;
			(*widths)[30] = 8;
			(*widths)[31] = 7;
			(*widths)[32] = 7;
			(*widths)[33] = 7;
			(*widths)[34] = 7;
			(*widths)[35] = 7;

			(*widths)[36] = 7;
			(*widths)[37] = 7;
			(*widths)[38] = 7;
			(*widths)[39] = 7;
			(*widths)[40] = 6;
			(*widths)[41] = 6;
			(*widths)[42] = 4;
			(*widths)[43] = 7;
			(*widths)[44] = 7;
			(*widths)[45] = 7;
			(*widths)[46] = 7;

			
			(*widths)[47] = 4; //ú
			(*widths)[48] = 2; //ú
			(*widths)[49] = 7; //ú
			(*widths)[50] = 6; //ú
			(*widths)[51] = 7; //ú
			(*widths)[52] = 3; //ú
			(*widths)[53] = 5; //ú
			(*widths)[54] = 13; //ú
			
			(*widths)[55] = 10; //ú
			(*widths)[56] = 14; //ú
			(*widths)[57] = 8; //ú
			(*widths)[58] = 3; //ú
			(*widths)[59] = 4; //ú
			(*widths)[60] = 8; //ú
			(*widths)[61] = 4; //ú
			(*widths)[62] = 7; //ú
			
			
			iFontes[2] = new Label(new ImageFont(6, 1, LoadBMP(
					EMbmCloseupQvgafont_medium), LoadBMP(
					EMbmCloseupQvgafont_medium_mask), tmp, widths), true);

			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(9);
			(*widths)['b' - 'a'] = 8;
			(*widths)['c' - 'a'] = 8;
			(*widths)['d' - 'a'] = 8;
			(*widths)['e' - 'a'] = 7;
			(*widths)['f' - 'a'] = 7;
			(*widths)['g' - 'a'] = 8;
			(*widths)['h' - 'a'] = 8;
			(*widths)['i' - 'a'] = 3;
			(*widths)['j' - 'a'] = 5;
			(*widths)['k' - 'a'] = 8;
			(*widths)['l' - 'a'] = 7;
			(*widths)['m' - 'a'] = 11;
			(*widths)['n' - 'a'] = 9;
			(*widths)['o' - 'a'] = 8;
			(*widths)['p' - 'a'] = 8;
			(*widths)['q' - 'a'] = 8;
			(*widths)['r' - 'a'] = 8;
			(*widths)['s' - 'a'] = 9;
			(*widths)['t' - 'a'] = 9;
			(*widths)['u' - 'a'] = 8;
			(*widths)['v' - 'a'] = 9;
			(*widths)['w' - 'a'] = 13;
			(*widths)['x' - 'a'] = 9;
			(*widths)['y' - 'a'] = 9;
			(*widths)['z' - 'a'] = 8;

			(*widths)[26] = 8; //0
			(*widths)[27] = 5;
			(*widths)[28] = 8;
			(*widths)[29] = 8;
			(*widths)[30] = 8;
			(*widths)[31] = 9; //5
			(*widths)[32] = 8;
			(*widths)[33] = 9;
			(*widths)[34] = 8;
			(*widths)[35] = 8;

			(*widths)[36] = 10; //á
			(*widths)[37] = 10;
			(*widths)[38] = 10;
			(*widths)[39] = 10;
			(*widths)[40] = 7; //é
			(*widths)[41] = 7;
			(*widths)[42] = 4; //í
			(*widths)[43] = 8; //ó
			(*widths)[44] = 8;
			(*widths)[45] = 8;
			(*widths)[46] = 8; //ú
			
			
			(*widths)[47] = 3; //ú
			(*widths)[48] = 3; //ú			
			(*widths)[49] = 7; //ú			
			(*widths)[50] = 8; //ú			
			(*widths)[51] = 8; //ú				
			(*widths)[52] = 3; //ú			
			(*widths)[53] = 6; //ú		
			(*widths)[54] = 14; //ú			
			(*widths)[55] = 14; //ú
			(*widths)[56] = 13; //ú
			(*widths)[57] = 7; //ú
			(*widths)[58] = 3; //ú
			(*widths)[59] = 6; //ú
			(*widths)[60] = 4; //ú
			(*widths)[61] = 4; //ú
			(*widths)[62] = 8; //ú
			
			
			


			iFontes[3] = new Label(new ImageFont(7, 1, LoadBMP(
					EMbmCloseupQvgafont_big), LoadBMP(
					EMbmCloseupQvgafont_big_mask), tmp, widths), true);

			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(7);
			(*widths)['b' - 'a'] = 6;
			(*widths)['c' - 'a'] = 6;
			(*widths)['d' - 'a'] = 6;
			(*widths)['e' - 'a'] = 5;
			(*widths)['f' - 'a'] = 5;
			(*widths)['g' - 'a'] = 6;
			(*widths)['h' - 'a'] = 6;
			(*widths)['i' - 'a'] = 2;
			(*widths)['j' - 'a'] = 4;
			(*widths)['k' - 'a'] = 7;
			(*widths)['l' - 'a'] = 5;
			(*widths)['m' - 'a'] = 8;
			(*widths)['n' - 'a'] = 7;
			(*widths)['o' - 'a'] = 6;
			(*widths)['p' - 'a'] = 6;
			(*widths)['q' - 'a'] = 6;
			(*widths)['r' - 'a'] = 6;
			(*widths)['s' - 'a'] = 7;
			(*widths)['t' - 'a'] = 6;
			(*widths)['u' - 'a'] = 6;
			(*widths)['v' - 'a'] = 9;
			(*widths)['w' - 'a'] = 10;
			(*widths)['x' - 'a'] = 7;
			(*widths)['y' - 'a'] = 8;
			(*widths)['z' - 'a'] = 6;

			(*widths)[26] = 6;
			(*widths)[27] = 4;
			(*widths)[28] = 6;
			(*widths)[29] = 6;
			(*widths)[30] = 7;
			(*widths)[31] = 6;
			(*widths)[32] = 6;
			(*widths)[33] = 6;
			(*widths)[34] = 6;
			(*widths)[35] = 6;

			(*widths)[36] = 7;
			(*widths)[37] = 7;
			(*widths)[38] = 7;
			(*widths)[39] = 7;
			(*widths)[40] = 5;
			(*widths)[41] = 5;
			(*widths)[42] = 3;
			(*widths)[43] = 6;
			(*widths)[44] = 6;
			(*widths)[45] = 6;
			(*widths)[46] = 6;
			
			(*widths)[47] = 4;
			(*widths)[48] = 2;
			(*widths)[49] = 6;
			(*widths)[50] = 6;
			(*widths)[51] = 6;
			(*widths)[52] = 2;
			(*widths)[53] = 5;
			(*widths)[54] = 11;
			(*widths)[55] = 9;
			(*widths)[56] = 9;
			(*widths)[57] = 5;
			(*widths)[58] = 3;
			(*widths)[59] = 4;
			(*widths)[60] = 3;
			(*widths)[61] = 3;
			(*widths)[62] = 6;
			iFontes[4] = new Label(new ImageFont(5, 1, LoadBMP(
					EMbmCloseupQvgafont_text), LoadBMP(
					EMbmCloseupQvgafont_text_mask), tmp, widths), true);
			break;
		case SS_BIG:
			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);
			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(9);
			(*widths)['b' - 'a'] = 8;
			(*widths)['c' - 'a'] = 8;
			(*widths)['d' - 'a'] = 8;
			(*widths)['e' - 'a'] = 7;
			(*widths)['f' - 'a'] = 7;
			(*widths)['g' - 'a'] = 8;
			(*widths)['h' - 'a'] = 8;
			(*widths)['i' - 'a'] = 3;
			(*widths)['j' - 'a'] = 5;
			(*widths)['k' - 'a'] = 8;
			(*widths)['l' - 'a'] = 7;
			(*widths)['m' - 'a'] = 11;
			(*widths)['n' - 'a'] = 9;
			(*widths)['o' - 'a'] = 8;
			(*widths)['p' - 'a'] = 8;
			(*widths)['q' - 'a'] = 8;
			(*widths)['r' - 'a'] = 8;
			(*widths)['s' - 'a'] = 9;
			(*widths)['t' - 'a'] = 9;
			(*widths)['u' - 'a'] = 8;
			(*widths)['v' - 'a'] = 9;
			(*widths)['w' - 'a'] = 13;
			(*widths)['x' - 'a'] = 9;
			(*widths)['y' - 'a'] = 9;
			(*widths)['z' - 'a'] = 8;

			(*widths)[26] = 8;
			(*widths)[27] = 5;
			(*widths)[28] = 8;
			(*widths)[29] = 8;
			(*widths)[30] = 9;
			(*widths)[31] = 7;
			(*widths)[32] = 8;
			(*widths)[33] = 7;
			(*widths)[34] = 8;
			(*widths)[35] = 8;

			(*widths)[36] = 11;
			(*widths)[37] = 11;
			(*widths)[38] = 11;
			(*widths)[39] = 11;
			(*widths)[40] = 9;
			(*widths)[41] = 9;
			(*widths)[42] = 5;
			(*widths)[43] = 8;
			(*widths)[44] = 8;
			(*widths)[45] = 8;
			(*widths)[46] = 8;
			
			(*widths)[47] = 6;
			(*widths)[48] = 3;
			(*widths)[49] = 7;
			(*widths)[50] = 9;
			(*widths)[51] = 8;
			(*widths)[52] = 3;
			(*widths)[53] = 6;
			(*widths)[54] = 14;
			(*widths)[55] = 14;
			(*widths)[56] = 13;
			(*widths)[57] = 6;
			(*widths)[58] = 5;
			(*widths)[59] = 6;
			(*widths)[60] = 4;
			(*widths)[61] = 4;
			(*widths)[62] = 8;

			

			iFontes[1] = new Label(new ImageFont(6, 1, LoadBMP(
					EMbmCloseupFonte_nomes_pq), LoadBMP(
					EMbmCloseupFonte_nomes_pq_mask), tmp, widths), true);

			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(11);
			(*widths)['b' - 'a'] = 10;
			(*widths)['c' - 'a'] = 11;
			(*widths)['d' - 'a'] = 12;
			(*widths)['e' - 'a'] = 9;
			(*widths)['f' - 'a'] = 9;
			(*widths)['g' - 'a'] = 11;
			(*widths)['h' - 'a'] = 10;
			(*widths)['i' - 'a'] = 4;
			(*widths)['j' - 'a'] = 7;
			(*widths)['k' - 'a'] = 11;
			(*widths)['l' - 'a'] = 9;
			(*widths)['m' - 'a'] = 14;
			(*widths)['n' - 'a'] = 11;
			(*widths)['o' - 'a'] = 11;
			(*widths)['p' - 'a'] = 11;
			(*widths)['q' - 'a'] = 11;
			(*widths)['r' - 'a'] = 11;
			(*widths)['s' - 'a'] = 11;
			(*widths)['t' - 'a'] = 10;
			(*widths)['u' - 'a'] = 10;
			(*widths)['v' - 'a'] = 11;
			(*widths)['w' - 'a'] = 16;
			(*widths)['x' - 'a'] = 12;
			(*widths)['y' - 'a'] = 12;
			(*widths)['z' - 'a'] = 10;

			(*widths)[26] = 10;
			(*widths)[27] = 6;
			(*widths)[28] = 10;
			(*widths)[29] = 10;
			(*widths)[30] = 11;
			(*widths)[31] = 10;
			(*widths)[32] = 10;
			(*widths)[33] = 10;
			(*widths)[34] = 10;
			(*widths)[35] = 10;

			(*widths)[36] = 11;
			(*widths)[37] = 11;
			(*widths)[38] = 11;
			(*widths)[39] = 11;
			(*widths)[40] = 9;
			(*widths)[41] = 9;
			(*widths)[42] = 6;
			(*widths)[43] = 11;
			(*widths)[44] = 11;
			(*widths)[45] = 11;
			(*widths)[46] = 11;
			
			(*widths)[47] = 6;
			(*widths)[48] = 3;
			(*widths)[49] = 9;
			(*widths)[50] = 9;
			(*widths)[51] = 9;
			(*widths)[52] = 3;
			(*widths)[53] = 8;
			(*widths)[54] = 16;
			(*widths)[55] = 17;
			(*widths)[56] = 18;
			(*widths)[57] = 8;
			(*widths)[58] = 4;
			(*widths)[59] = 6;
			(*widths)[60] = 10;
			(*widths)[60] = 5;
			(*widths)[60] = 5;
			(*widths)[60] = 11;
			

			iFontes[2] = new Label(new ImageFont(8, 1, LoadBMP(
					EMbmCloseupFonte_nomes_md), LoadBMP(
					EMbmCloseupFonte_nomes_md_mask), tmp, widths), true);

			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(15);
			(*widths)['b' - 'a'] = 14;
			(*widths)['c' - 'a'] = 14;
			(*widths)['d' - 'a'] = 14;
			(*widths)['e' - 'a'] = 12;
			(*widths)['f' - 'a'] = 12;
			(*widths)['g' - 'a'] = 14;
			(*widths)['h' - 'a'] = 14;
			(*widths)['i' - 'a'] = 5;
			(*widths)['j' - 'a'] = 9;
			(*widths)['k' - 'a'] = 15;
			(*widths)['l' - 'a'] = 12;
			(*widths)['m' - 'a'] = 19;
			(*widths)['n' - 'a'] = 16;
			(*widths)['o' - 'a'] = 13;
			(*widths)['p' - 'a'] = 14;
			(*widths)['q' - 'a'] = 14;
			(*widths)['r' - 'a'] = 14;
			(*widths)['s' - 'a'] = 14;
			(*widths)['t' - 'a'] = 15;
			(*widths)['u' - 'a'] = 14;
			(*widths)['v' - 'a'] = 15;
			(*widths)['w' - 'a'] = 22;
			(*widths)['x' - 'a'] = 16;
			(*widths)['y' - 'a'] = 16;
			(*widths)['z' - 'a'] = 13;

			(*widths)[26] = 14;
			(*widths)[27] = 8;
			(*widths)[28] = 13;
			(*widths)[29] = 13;
			(*widths)[30] = 15;
			(*widths)[31] = 13;
			(*widths)[32] = 13;
			(*widths)[33] = 13;
			(*widths)[34] = 13;
			(*widths)[35] = 13;

			(*widths)[36] = 15;
			(*widths)[37] = 15;
			(*widths)[38] = 15;
			(*widths)[39] = 15;
			(*widths)[40] = 12;
			(*widths)[41] = 12;
			(*widths)[42] = 8;
			(*widths)[43] = 14;
			(*widths)[44] = 14;
			(*widths)[45] = 14;
			(*widths)[46] = 14;

			(*widths)[47] = 8;
			(*widths)[48] = 4;
			(*widths)[49] = 13;
			(*widths)[50] = 13;
			(*widths)[51] = 13;
			(*widths)[52] = 4;
			(*widths)[53] = 12;
			(*widths)[54] = 23;
			(*widths)[55] = 21;
			(*widths)[56] = 25;
			(*widths)[57] = 11;
			(*widths)[58] = 6;
			(*widths)[59] = 8;
			(*widths)[60] = 9;
			(*widths)[61] = 9;
			(*widths)[62] = 14;
			
			iFontes[3] = new Label(new ImageFont(12, 1, LoadBMP(
					EMbmCloseupFonte_nomes_gr), LoadBMP(
					EMbmCloseupFonte_nomes_gr_mask), tmp, widths), true);

			widths = new RArray<int> ();
			tmp = new TBuf16<255> ();
			tmp->Copy(KChars);

			for (int c = 0; c < tmp->Length(); c++)
				widths->Append(9);
			(*widths)['b' - 'a'] = 8;
			(*widths)['c' - 'a'] = 8;
			(*widths)['d' - 'a'] = 8;
			(*widths)['e' - 'a'] = 7;
			(*widths)['f' - 'a'] = 7;
			(*widths)['g' - 'a'] = 8;
			(*widths)['h' - 'a'] = 8;
			(*widths)['i' - 'a'] = 3;
			(*widths)['j' - 'a'] = 5;
			(*widths)['k' - 'a'] = 8;
			(*widths)['l' - 'a'] = 7;
			(*widths)['m' - 'a'] = 11;
			(*widths)['n' - 'a'] = 9;
			(*widths)['o' - 'a'] = 8;
			(*widths)['p' - 'a'] = 8;
			(*widths)['q' - 'a'] = 8;
			(*widths)['r' - 'a'] = 8;
			(*widths)['s' - 'a'] = 9;
			(*widths)['t' - 'a'] = 9;
			(*widths)['u' - 'a'] = 8;
			(*widths)['v' - 'a'] = 9;
			(*widths)['w' - 'a'] = 13;
			(*widths)['x' - 'a'] = 9;
			(*widths)['y' - 'a'] = 9;
			(*widths)['z' - 'a'] = 8;

			(*widths)[26] = 8;
			(*widths)[27] = 5;
			(*widths)[28] = 8;
			(*widths)[29] = 8;
			(*widths)[30] = 8;
			(*widths)[31] = 8;
			(*widths)[32] = 8;
			(*widths)[33] = 7;
			(*widths)[34] = 8;
			(*widths)[35] = 8;

			(*widths)[36] = 11;
			(*widths)[37] = 11;
			(*widths)[38] = 11;
			(*widths)[39] = 11;
			(*widths)[40] = 9;
			(*widths)[41] = 9;
			(*widths)[42] = 5;
			(*widths)[43] = 8;
			(*widths)[44] = 8;
			(*widths)[45] = 8;
			(*widths)[46] = 8;
			
			(*widths)[47] = 6;
			(*widths)[48] = 3;
			(*widths)[49] = 7;
			(*widths)[50] = 9;
			(*widths)[51] = 8;
			(*widths)[52] = 3;
			(*widths)[53] = 6;
			(*widths)[54] = 14;
			(*widths)[55] = 14;
			(*widths)[56] = 13;
			(*widths)[57] = 6;
			(*widths)[58] = 5;
			(*widths)[59] = 6;
			(*widths)[60] = 5;
			(*widths)[61] = 4;
			(*widths)[62] = 8;
			iFontes[4] = new Label(new ImageFont(6, 1, LoadBMP(
					EMbmCloseupFonte_texto), LoadBMP(
					EMbmCloseupFonte_texto_mask), tmp, widths), true);
			break;
		}

	}

/**
 * Construct the control (second phase).
 *  Creates a window to contain the controls and activates it.
 * @param aRect bounding rectangle
 * @param aCommandObserver command observer
 * @param aParent owning parent, or NULL
 */
void CCloseUpContainer::ConstructL(const TRect& aRect,
		const CCoeControl* aParent, MEikCommandObserver* aCommandObserver)
	{
	if (aParent == NULL)
		{
		CreateWindowL();
		}
	else
		{
		SetContainerWindowL(*aParent);
		}
	iFocusControl = NULL;
	iCommandObserver = aCommandObserver;
	InitializeControlsL();
	SetRect(aRect);
	SetExtentToWholeScreen();
	ActivateL();
	EnableDragEvents();
	DrawableWindow()->EnableFocusChangeEvents();
	iVisible=true;
	// [[[ begin generated region: do not modify [Post-ActivateL initializations]
	// ]]] end generated region [Post-ActivateL initializations]	
	iViews = new RPointerArray<CanvasView> ();
	CanvasView *ptr = NULL;
	TPixelsAndRotation pixelsAndRotation;
	TSize screensize;
	CWsScreenDevice* screenDevice = CEikonEnv::Static()->ScreenDevice();
	TInt mode = screenDevice->CurrentScreenMode();
	iCurrentView = 0;

	screenDevice->GetScreenModeSizeAndRotation(mode, pixelsAndRotation);
	screensize = screenDevice->SizeInPixels();
	if (screensize.iWidth < 360)
		initFonts(SS_SMALL);
	else
		initFonts(SS_BIG);

	TApaTask *pTask = new TApaTask(this->iCoeEnv->WsSession());
	pTask->SetWgId(CEikonEnv::Static()->RootWin().Identifier());

	////////////////////////criando o backbuffer //////////
	iBackBuffer = new (ELeave) CFbsBitmap;

	User::LeaveIfError(iBackBuffer->Create(Size(),
			iEikonEnv->DefaultDisplayMode()));

	// Create back buffer graphics context
	iBackBufferDevice = CFbsBitmapDevice::NewL(iBackBuffer);
	User::LeaveIfError(iBackBufferDevice->CreateContext(iBackBufferContext));
	iBackBufferContext->SetPenStyle(CGraphicsContext::ESolidPen);
	iBackBufferSize = iBackBuffer->SizeInPixels();
	//////////////////////////////
	ptr = new CSplashCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	/////////////////////////////////////
	ptr = new CMainCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	CMainCanvasView *ptr2 = (CMainCanvasView*) ptr;
	/////////////////////////////////////
	ptr = new CGenderCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	/////////////////////////////////////
	ptr = new CContactsCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	ptr2->setContacts(((CContactsCanvasView*) ptr)->getContacts());
	/////////////////////////////////////
	ptr = new CFinishedCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	/////////////////////////////////////
	ptr = new CAboutCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	/////////////////////////////////////
	ptr = new CExplosionCanvasView(screensize.iWidth, screensize.iHeight);
	for (int c = 0; c < 4; c++)
		ptr->setFont(c + 1, iFontes[c + 1]);
	ptr->setTask(pTask);
	CleanupStack::PushL(ptr);
	iViews->AppendL(ptr);
	CleanupStack::Pop(ptr);

	/////////////////////////////////////
	iPeriodicTimer = CPeriodic::NewL(CActive::EPriorityUserInput);
	iPeriodicTimer->Start(0, 30000, TCallBack(CallBack, this));

	}

/**
 *	Draw container contents.
 */
void CCloseUpContainer::Draw(const TRect& aRect) const
	{
	// [[[ begin generated region: do not modify [Generated Contents]

	CWindowGc& gc = SystemGc();
	gc.Clear(aRect);
	gc.SetBrushStyle(CWindowGc::ESolidBrush);
	gc.SetBrushColor(TRgb(0, 0, 0));
	gc.DrawRect(aRect);

	if (!iBackBufferContext)
		return;
	
	if ((*iViews)[iCurrentView]->getVisible())
		(*iViews)[iCurrentView]->Draw(*iBackBufferContext);

	gc.BitBlt(TPoint(0, 0), iBackBuffer);
	}

/**
 * Return the number of controls in the container (override)
 * @return count
 */
TInt CCloseUpContainer::CountComponentControls() const
	{
	return (int) ELastControl;
	}

/**
 * Get the control with the given index (override)
 * @param aIndex Control index [0...n) (limited by #CountComponentControls)
 * @return Pointer to control
 */
CCoeControl* CCloseUpContainer::ComponentControl(TInt aIndex) const
	{
	// [[[ begin generated region: do not modify [Generated Contents]
	switch (aIndex)
		{
		}
	// ]]] end generated region [Generated Contents]

	// handle any user controls here...

	return NULL;
	}

/**
 *	Handle resizing of the container. This implementation will lay out
 *  full-sized controls like list boxes for any screen size, and will layout
 *  labels, editors, etc. to the size they were given in the UI designer.
 *  This code will need to be modified to adjust arbitrary controls to
 *  any screen size.
 */
void CCloseUpContainer::SizeChanged()
	{
	CCoeControl::SizeChanged();
	LayoutControls();
	// [[[ begin generated region: do not modify [Generated Contents]

	// ]]] end generated region [Generated Contents]
	}

// [[[ begin generated function: do not modify
/**
 * Layout components as specified in the UI Designer
 */
void CCloseUpContainer::LayoutControls()
	{
	}
// ]]] end generated function

/**
 *	Handle key events.
 */
TKeyResponse CCloseUpContainer::OfferKeyEventL(const TKeyEvent& aKeyEvent,
		TEventCode aType)
	{
	// [[[ begin generated region: do not modify [Generated Contents]

	// ]]] end generated region [Generated Contents]

	if (iFocusControl != NULL
			&& iFocusControl->OfferKeyEventL(aKeyEvent, aType)
					== EKeyWasConsumed)
		{

		return EKeyWasConsumed;
		}
	if (aType == EEventKeyDown)
		{
		// Process EEventKeyDown	 
		//	iVibraController->StartVibrationL();
		}

	if (aType == EEventKey)
		{
		switch (aKeyEvent.iScanCode)
			{
			case EStdKeyDownArrow:
			case EStdKeyRightArrow:
				//User::InfoPrint(_L("down"));
				getCanvasView(iCurrentView)->sendKey(CanvasView::DOWN);
				break;
			case EStdKeyUpArrow:
			case EStdKeyLeftArrow:
				//User::InfoPrint(_L("up"));
				getCanvasView(iCurrentView)->sendKey(CanvasView::UP);
				break;
				///ainda vou encontrar o c�digo de tecla "enter". N�o � EStdKeyEnter!!
			case /*EStdKeyLeftArrow*/167:
			case 3:
				//User::InfoPrint(_L("enter"));

				getCanvasView(iCurrentView)->sendKey(CanvasView::ENTER);
				break;
			default:
				break;
			}
		}
	if (aType == EEventKeyUp)
		{
		//iVibraController->StopVibrationL();
		// Process EEventKeyUp 	 
		}

	return CCoeControl::OfferKeyEventL(aKeyEvent, aType);
	}

// [[[ begin generated function: do not modify
/**
 *	Initialize each control upon creation.
 */
void CCloseUpContainer::InitializeControlsL()
	{

	}
// ]]] end generated function

/** 
 * Handle global resource changes, such as scalable UI or skin events (override)
 */
void CCloseUpContainer::HandleResourceChange(TInt aType)
	{
	CCoeControl::HandleResourceChange(aType);
	SetRect(
			iAvkonViewAppUi->View(TUid::Uid(ECloseUpContainerViewId))->ClientRect());
	// [[[ begin generated region: do not modify [Generated Contents]
	// ]]] end generated region [Generated Contents]
	if (aType == KEikDynamicLayoutVariantSwitch)
		{

		TRect rect;
		// Ask where container's rectangle should be
		// EMainPane equals to area returned by
		// CEikAppUi::ClientRect()
		AknLayoutUtils::LayoutMetricsRect(AknLayoutUtils::EMainPane, rect);
		// Set new screen rect
		SetRect(rect);

		for (int c = 0; c < iViews->Count(); c++)
			{
			getCanvasView(c)->setXRes(
					this->iCoeEnv->ScreenDevice()->SizeInPixels().iWidth);
			getCanvasView(c)->setYRes(
					this->iCoeEnv->ScreenDevice()->SizeInPixels().iHeight);
			}

		}
	}

void CCloseUpContainer::HandlePointerEventL(const TPointerEvent& aPointerEvent)
	{
	// Remember to call base class implementation. Then your child controls
	// receive pointer events.
	CCoeControl::HandlePointerEventL(aPointerEvent);

	// Rest of your code
	TBuf16<20> text;
	switch (aPointerEvent.iType)
		{
		case TPointerEvent::EDrag:

			int modulus = aPointerEvent.iPosition.iY - point[0].iY;
			int sign = modulus;
			if (modulus < 0)
				modulus = -modulus;
/*
			text.AppendNum(point[0].iY);
			text.Append(_L( " -> " ));
			text.AppendNum(aPointerEvent.iPosition.iY);
			User::InfoPrint(text);
*/
			if (iCurrentView == 3)
				((CContactsCanvasView*) getCanvasView(iCurrentView))->ScrollY(
						aPointerEvent.iPosition.iY - point[0].iY);
			break;

		case TPointerEvent::EButton1Down:
			point[1] = aPointerEvent.iPosition;
			viewClick = iCurrentView;
			break;

		case TPointerEvent::EButton1Up:
			// evita que um click na tela anterior reflita na atual
			if (viewClick == iCurrentView)
				{
			//	User::InfoPrint(_L("tap up"));
				int dy = (point[1].iY - aPointerEvent.iPosition.iY);
				if (dy < 0)
					dy = -dy;
				bool scrolling = dy > SCROLL_Y_TOLERANCE;

				if (!scrolling)
					getCanvasView(iCurrentView)->Click(aPointerEvent.iPosition);
				}
			break;
		}
	point[0] = aPointerEvent.iPosition;
	}

CFbsBitmap* CCloseUpContainer::LoadBMP(TInt aIndex)
	{
	TFindFile imageFile(CCoeEnv::Static()->FsSession());

	if (KErrNone == imageFile.FindByDir(_L16("\\resource\\apps\\CloseUp.mbm"),
			KNullDesC))
		{
		CFbsBitmap *ptr = new (ELeave) CFbsBitmap();
		CleanupStack::PushL(ptr);
		ptr->Load(imageFile.File(), aIndex);
		CleanupStack::Pop(ptr);
		return ptr;
		}

	return NULL;
	}

