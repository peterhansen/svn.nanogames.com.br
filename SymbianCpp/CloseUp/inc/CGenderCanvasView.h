/*
 * CGenderCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CGENDERCANVASVIEW_H_
#define CGENDERCANVASVIEW_H_

#include "CanvasView.h"

class CGenderCanvasView : public CanvasView
	{
	
public:	
	CGenderCanvasView(TInt aXRes,TInt aYRes);
	virtual ~CGenderCanvasView();
	virtual void SendSignal(int aSignal);
	//virtual void sendKey(int aKey);
	void setSelection(TInt32 aSelection);
	TInt32 getSelection() const;
	
private:

	TInt32 iSelection;
	
	};

inline TInt32 CGenderCanvasView::getSelection() const
	{
		return iSelection;	
	}
	
inline void CGenderCanvasView::setSelection(TInt32 aSelection)
	{
		iSelection=aSelection;
	}

#endif /* CGENDERCANVASVIEW_H_ */
