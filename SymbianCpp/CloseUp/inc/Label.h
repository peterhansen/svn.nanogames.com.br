#ifndef LABEL_H
#define LABEL_H


#include "ImageFont.h"

class Label 
	{

	public:
	//friend class ImageFont;
	Label( ImageFont* pFont);
	Label( ImageFont* pFont, bool setTextSize );
	virtual ~Label();


	ImageFont* getFont();
	void setFont( ImageFont* pFont );
	void drawString( CGraphicsContext* pG, TBuf16<255> *text, int x, int y,TRect aClippingRect );
	void drawString( CGraphicsContext* pG, TBuf16<255>* text, int textLength, int x, int y,TRect aClippingRect );
	virtual void Draw(CGraphicsContext &aGc);
	//protected:

	/** fonte utilizada para desenhar o texto */
	ImageFont* pFont;



};

/// mais bonito...
typedef Label CNanoFont;

#endif
