#define NUM_FONTS 5
#define NUMCHARSTOCOMPARE 8

#define DL_BTN_INICIAR 1
#define DL_BTN_MINIMIZAR 2
#define DL_BTN_SOBRE 3

#define SIGN_NOP -1


#ifndef CONFIG_H
#define CONFIG_H

enum TCanvasViewId {CV_SPLASH,CV_MAIN,CV_GENDER,CV_CONTACTS,CV_FINISHED,CV_ABOUT,CV_EXPLOSION};
enum TBitmapedFontsType {BF_NOTUSED,BF_SMALL,BF_MEDIUM,BF_BIG,BF_NORMALTEXT};
enum TScreenSizes{ SS_SMALL,SS_BIG};

#endif
