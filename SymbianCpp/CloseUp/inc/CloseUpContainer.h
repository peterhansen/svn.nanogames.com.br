/*
========================================================================
 Name        : CloseUpContainer.h
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
#ifndef CLOSEUPCONTAINER_H
#define CLOSEUPCONTAINER_H
#include "CanvasView.h"
// [[[ begin generated region: do not modify [Generated Includes]
#include <coecntrl.h>		
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>
#include <VibraController.h>
#include <ImageFont.h>
#include <Label.h>
// ]]] end generated region [Generated Includes]

// [[[ begin [Event Handler Includes]
// ]]] end [Event Handler Includes]

// [[[ begin generated region: do not modify [Generated Forward Declarations]
class MEikCommandObserver;		
// ]]] end generated region [Generated Forward Declarations]

/**
 * Container class for CloseUpContainer
 * 
 * @class	CCloseUpContainer CloseUpContainer.h
 */
class CCloseUpContainer : public CCoeControl
	{
private:
    void HandlePointerEventL(const TPointerEvent& aPointerEvent);
public:
	// constructors and destructor
	CCloseUpContainer();
	static CCloseUpContainer* NewL( 
		const TRect& aRect, 
		const CCoeControl* aParent, 
		MEikCommandObserver* aCommandObserver );
	static CCloseUpContainer* NewLC( 
		const TRect& aRect, 
		const CCoeControl* aParent, 
		MEikCommandObserver* aCommandObserver );
	static CFbsBitmap*  LoadBMP(TInt aIndex);	
	
	void ConstructL( 
		const TRect& aRect, 
		const CCoeControl* aParent, 
		MEikCommandObserver* aCommandObserver );
	virtual ~CCloseUpContainer();
	void initFonts(TScreenSizes aScreenSize);
	void LoadSelectedContactsL();
public:
	virtual IMPORT_C void PrepareForFocusLossL();
	virtual IMPORT_C void PrepareForFocusGainL();
	
	Label *iFontes[5];
	// from base class CCoeControl
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl( TInt aIndex ) const;
	TKeyResponse OfferKeyEventL( 
			const TKeyEvent& aKeyEvent, 
			TEventCode aType );
	void HandleResourceChange( TInt aType );
	static TInt CallBack(TAny* aAny);

	CPeriodic *iPeriodicTimer;	
	bool iVisible;
protected:
	// from base class CCoeControl
	void SizeChanged();

private:
	RPointerArray<CanvasView> *iViews;
	
	TInt32 iCurrentView;		
	TPoint point[2];
    CFbsBitmap*                     iBackBuffer;
    CFbsBitmapDevice*               iBackBufferDevice;
    CFbsBitGc*                      iBackBufferContext;
    TSize                           iBackBufferSize;	
	
	// view ativa no momento de um click down
	int viewClick;
	CVibraController *iVibraController;
	void GetFileType(const TDesC& aFileName, TDes8& aFileType);
	
	void Tick();
	// from base class CCoeControl
	void Draw( const TRect& aRect ) const;	
	CanvasView* getCanvasView(TInt32 aId);
private:

	void InitializeControlsL();
	void LayoutControls();
	CCoeControl* iFocusControl;
	MEikCommandObserver* iCommandObserver;
	// [[[ begin generated region: do not modify [Generated Methods]
public: 
	// ]]] end generated region [Generated Methods]
	
	// [[[ begin generated region: do not modify [Generated Type Declarations]
public: 
	// ]]] end generated region [Generated Type Declarations]
	
	// [[[ begin generated region: do not modify [Generated Instance Variables]
private: 
	// ]]] end generated region [Generated Instance Variables]
	
	
	// [[[ begin [Overridden Methods]
protected: 
	// ]]] end [Overridden Methods]
	
	
	// [[[ begin [User Handlers]
protected: 
	// ]]] end [User Handlers]
	
public: 
	enum TControls
		{
		// [[[ begin generated region: do not modify [Generated Contents]
		
		// ]]] end generated region [Generated Contents]
		
		// add any user-defined entries here...
		
		ELastControl
		};
	};
				
#endif // CLOSEUPCONTAINER_H
