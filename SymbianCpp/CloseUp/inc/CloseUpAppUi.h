/*
========================================================================
 Name        : CloseUpAppUi.h
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
#ifndef CLOSEUPAPPUI_H
#define CLOSEUPAPPUI_H

// [[[ begin generated region: do not modify [Generated Includes]
#include <aknviewappui.h>
// ]]] end generated region [Generated Includes]

// [[[ begin generated region: do not modify [Generated Forward Declarations]
class CCloseUpContainerView;
// ]]] end generated region [Generated Forward Declarations]

/**
 * @class	CCloseUpAppUi CloseUpAppUi.h
 * @brief The AppUi class handles application-wide aspects of the user interface, including
 *        view management and the default menu, control pane, and status pane.
 */
class CCloseUpAppUi : public CAknViewAppUi
	{
public: 
	// constructor and destructor
	CCloseUpAppUi();
	virtual ~CCloseUpAppUi();
	void ConstructL();

public:
	// from CCoeAppUi
	TKeyResponse HandleKeyEventL(
				const TKeyEvent& aKeyEvent,
				TEventCode aType );

	// from CEikAppUi
	void HandleCommandL( TInt aCommand );
	void HandleResourceChangeL( TInt aType );
	void HandleForegroundEventL(TBool aForeground);
	// from CAknAppUi
	void HandleViewDeactivation( 
			const TVwsViewId& aViewIdToBeDeactivated, 
			const TVwsViewId& aNewlyActivatedViewId );

	void HandleWsEventL (const TWsEvent &aEvent, CCoeControl *aDestination);
private:
	CAknView *iViews[7];
	
	void InitializeContainersL();
	// [[[ begin generated region: do not modify [Generated Methods]
public: 
	// ]]] end generated region [Generated Methods]
	void SwitchToViewL(int aIndex);	
	// [[[ begin generated region: do not modify [Generated Instance Variables]
private: 
	CCloseUpContainerView* iCloseUpContainerView;
	// ]]] end generated region [Generated Instance Variables]
	
	
	// [[[ begin [User Handlers]
protected: 
	// ]]] end [User Handlers]
	
	};

#endif // CLOSEUPAPPUI_H			
