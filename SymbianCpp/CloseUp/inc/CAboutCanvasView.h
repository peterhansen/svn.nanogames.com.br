/*
 * CAboutCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CABOUTCANVASVIEW_H_
#define CABOUTCANVASVIEW_H_

#include "CanvasView.h"

class CAboutCanvasView : public CanvasView
{
	public:
		CAboutCanvasView( TInt aXRes, TInt aYRes );
		virtual ~CAboutCanvasView( void );
		virtual void SendSignal( int aSignal ){};
};

#endif /* CABOUTCANVASVIEW_H_ */
