/*
 * CContactsCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CCONTACTSCANVASVIEW_H_
#define CCONTACTSCANVASVIEW_H_

#include "CanvasView.h"


class CContactsCanvasView : public CanvasView
{
	public:
		CContactsCanvasView( TInt aXRes,TInt aYRes );
		virtual ~CContactsCanvasView();
		virtual void showing();		
		virtual void sendKey( TInt32 aKey );
		virtual void SendSignal(int aSignal);		
		void RemoveRingtoneForContactL(TContactItemId aId);
		void AddRingtoneForContactL(const TDesC& aRingtone,TContactItemId aId);

		void RemovePictureForContactL(TContactItemId aId);
		void AddPictureForContactL(RReadStream &aPicture,TContactItemId aId);

		
		void ScrollY(int aDelta, bool setCurrentIndex=true);
		void ToggleContact(int aContactIndex);
		TFileName getRingToneName(int aSelection);
		TFileName getPictureName(int aSelection);
		
		
		void ImportContactsFromAddressBook();		
		struct CCloseUpContact
		{
			TBuf16<255> iName;
			TBuf16<255> iSurname;
			TBuf16<255> iNumber;	
			TContactItemId iContactId;
			TInt iSelected;
		};

		void setSelection(TInt32 aSelection);
		TInt32 getSelection() const;
		
		void setUnchanged();
		bool getChanged() const;
		void setCurrentContact( int index );
	//	void LoadSelectedContactsL();
	//	void SaveSelectedContactsL();
		RPointerArray< CCloseUpContact > * getContacts() const;
	private:
		int iScrolled;
		bool iChanged;
		void changeContactsDisplayState();
		int iSelection;
		RPointerArray< CCloseUpContact > *pContacts;
};

inline RPointerArray< CContactsCanvasView::CCloseUpContact > * CContactsCanvasView::getContacts() const
	{
		return pContacts;
	}

inline bool CContactsCanvasView::getChanged() const
	{
		return iChanged;
	}

inline void CContactsCanvasView::setUnchanged()
	{
		iChanged=false;
	}

inline TInt32 CContactsCanvasView::getSelection() const
	{
		return iSelection;	
	}
	
inline void CContactsCanvasView::setSelection(TInt32 aSelection)
	{
		iSelection=aSelection;
	}

#endif /* CCONTACTSCANVASVIEW_H_ */
