
#ifndef CLOSEUP_PAN_H
#define CLOSEUP_PAN_H

/** CloseUp application panic codes */
enum TCloseUpPanics
	{
	ECloseUpUi = 1
	// add further panics here
	};

inline void Panic(TCloseUpPanics aReason)
	{
	_LIT(applicationName,"CloseUp");
	User::Panic(applicationName, aReason);
	}

#endif // CLOSEUP_PAN_H
