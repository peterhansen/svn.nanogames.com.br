/*
 *  CanvasDisplaylet.h
 *  CloseUp
 *
 *  Created by Daniel Lopes Alves on 5/24/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <e32base.h>
#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <w32std.h>

#include "config.h"

#ifndef CANVAS_DISPLAYLET_H
#define CANVAS_DISPLAYLET_H

class CanvasDisplaylet: public CBase
{
	public:
		CanvasDisplaylet( void );	
		~CanvasDisplaylet( void );
		TBuf16< 255 >* addText( void );
		
		void setDefaultImage( CFbsBitmap* pImg );
		const CFbsBitmap* getDefaultImage( void ) const;

		void setDefaultImageMask( CFbsBitmap* pMask );
		const CFbsBitmap* getDefaultImageMask( void ) const;
	
		void setSelectedImage( CFbsBitmap* pImg );
		const CFbsBitmap* getSelectedImage( void ) const;

		void setSelectedImageMask( CFbsBitmap* pMask );
		const CFbsBitmap* getSelectedImageMask( void ) const;

//	private:
		bool iEnabled;
		bool iVisible;
		TInt32 iGoto;
		TInt32 iState;
		TRect iRectArea;
		TRect iClipArea;
		TRgb iFillColor;
		TInt iSelectedFont;
		TRgb iOutlineColor;
		TRgb iSelectedColor;
		RPointerArray< TBuf16< 255 > > iText;
	private:
		CFbsBitmap *pImage;
		CFbsBitmap *pImageMask;

		CFbsBitmap *pSelectedImage;
		CFbsBitmap *pSelectedImageMask;

		
};

inline const CFbsBitmap* CanvasDisplaylet::getDefaultImage( void ) const
{
	return pImage;
}

inline const CFbsBitmap* CanvasDisplaylet::getDefaultImageMask( void ) const
{
	return pImageMask;
}

inline const CFbsBitmap* CanvasDisplaylet::getSelectedImage( void ) const
{
	return pSelectedImage;
}

inline const CFbsBitmap* CanvasDisplaylet::getSelectedImageMask( void ) const
{
	return pSelectedImageMask;
}

#endif
