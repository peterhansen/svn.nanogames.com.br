/*
 * CanvasView.h
 *
 *  Created on: 26/04/2010
 *      Author: Daniel
 */

#ifndef CANVASVIEW_H_
#define CANVASVIEW_H_

#include <e32base.h>
#include <aknviewappui.h>
#include <eikappui.h>
#include <e32cmn.h>   //TUid
#include <e32std.h>   //User
#include <e32base.h>  //CArrayPtr, CleanupStack
#include <e32def.h>   //TBool
#include <s32file.h>  //RFileReadStream
#include <f32file.h>  //RFs
#include <cntdb.h>    //CContactDatabase
#include <cntdef.h>   //CContactIdArray
#include <cntfilt.h>  //CCntFilter
#include <cntitem.h>
#include <cntfield.h>
#include <cntitem.h>
#include <cntfield.h>
#include <cntfldst.h>
#include <eikenv.h>
#include <e32des8.h>
#include <e32def.h>
#include <fbs.h>
#include <gdi.h>
#include <e32base.h>
#include <coemain.h>
#include <flogger.h>
#include <e32math.h>
#include <e32base.h>
#include <w32std.h>

#include "config.h" 
#include "ImageFont.h"
#include "Label.h"
#include "CanvasDisplaylet.h"




class CanvasView : public CBase
{
	public:
		enum {UP,RIGHT,DOWN,LEFT,ENTER};

		CanvasView( TInt aXRes, TInt aYRes );
		virtual ~CanvasView( void );
	
	virtual void Draw(CGraphicsContext &aGc);
	virtual void sendKey(TInt32 aKey);
	virtual void hiding();
	virtual void showing();
	virtual void Click(TPoint aPos);
	virtual void SendSignal(int aSignal);
	
	TInt32 getXRes();
	TInt32 getYRes();
	void setXRes(TInt32 aXRes);
	void setYRes(TInt32 aYRes);
	
	void removeDisplaylet(TInt32 aIndex);
	void addDisplaylet(CanvasDisplaylet *displaylet);
	CanvasDisplaylet *getDisplaylet(TInt32 aIndex);
	TInt32 getTotalDisplaylets();
			
	void setGoto(TInt32 aGoto);
	TInt32 getGoto();
	
	void setCurrentDisplaylet(TInt32 aCurrentDisplaylet);	
	TInt32 getCurrentDisplaylet();
	
	
	void Centralize(CanvasDisplaylet *ptr);
	void CentralizeHorizontal(CanvasDisplaylet *ptr);
	void CentralizeVertical(CanvasDisplaylet *ptr);
	void ExpandToImage(CanvasDisplaylet *ptr);
	
		
	void setTask( TApaTask* pNewTask );
	TApaTask* getTask( void );
	
	void setFont(int aSlot,CNanoFont *aFont);
	CNanoFont *getFont(int aSlot) const;

	void setScreenSize(TScreenSizes aScreenSize);
	TScreenSizes getScreenSize() const;
	void setVisible(bool aVisible)
		{
			iVisible=aVisible;		
		}
	
	bool getVisible()
		{
			return iVisible;
		}
	
	void Reset();
	private:
	bool iVisible;
	TApaTask *pTask;	
	RPointerArray<CanvasDisplaylet> *iDisplaylet;
	Label *iFont[5];
	TInt32 iXRes;
	TInt32 iYRes;
	TInt32 iGoto;
	TScreenSizes iScreenSize;
	TInt32 iCurrentDisplaylet;	
	
	RPointerArray<CanvasDisplaylet> * getArray()
			{
			return iDisplaylet;
		
			}
	
};

inline void CanvasView::Reset()
	{
	//nao eh muito legal, mas eh o que da pra fazer no tempo dado
	iDisplaylet=new RPointerArray<CanvasDisplaylet>();
	//iDisplaylet->Reset();	
	}

inline void CanvasView::setFont(int aSlot,Label *aFont)
	{
		if (aSlot<NUM_FONTS && aSlot>=0)
			iFont[aSlot]=aFont;
	}

inline Label *CanvasView::getFont(int aSlot) const
		{
		if (aSlot<NUM_FONTS && aSlot>=0)
			return iFont[aSlot];
		else
			return NULL;
		}

inline void CanvasView::setScreenSize(TScreenSizes aScreenSize)
	{
		iScreenSize=aScreenSize;
	}

inline TScreenSizes CanvasView::getScreenSize() const
	{
		return iScreenSize;
	}


inline CanvasDisplaylet *CanvasView::getDisplaylet(TInt32 aIndex)
	{
	return (*iDisplaylet)[aIndex];		
	}

inline TInt32 CanvasView::getTotalDisplaylets()
	{
	return iDisplaylet->Count();		
	}

inline void CanvasView::setTask( TApaTask* pNewTask )
{
	if( pTask != NULL )
		delete pTask;
	pTask = pNewTask;
}

inline TApaTask* CanvasView::getTask( void ) 
{
	return pTask;
}



#endif /* CANVASVIEW_H_ */
