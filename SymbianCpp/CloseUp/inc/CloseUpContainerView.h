/*
========================================================================
 Name        : CloseUpContainerView.h
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
#ifndef CLOSEUPCONTAINERVIEW_H
#define CLOSEUPCONTAINERVIEW_H

// [[[ begin generated region: do not modify [Generated Includes]
#include <aknview.h>
// ]]] end generated region [Generated Includes]


// [[[ begin [Event Handler Includes]
// ]]] end [Event Handler Includes]

// [[[ begin generated region: do not modify [Generated Constants]
// ]]] end generated region [Generated Constants]

// [[[ begin generated region: do not modify [Generated Forward Declarations]
class CCloseUpContainer;
// ]]] end generated region [Generated Forward Declarations]

/**
 * Avkon view class for CloseUpContainerView. It is register with the view server
 * by the AppUi. It owns the container control.
 * @class	CCloseUpContainerView CloseUpContainerView.h
 */						
			
class CCloseUpContainerView : public CAknView
	{
	
	
	// [[[ begin [Public Section]
public:
	// constructors and destructor
	CCloseUpContainerView();
	static CCloseUpContainerView* NewL();
	static CCloseUpContainerView* NewLC();        
	void ConstructL();
	virtual ~CCloseUpContainerView();
						
	// from base class CAknView
	TUid Id() const;
	void HandleCommandL( TInt aCommand );
	
	// [[[ begin generated region: do not modify [Generated Methods]
	CCloseUpContainer* CreateContainerL();	
	// ]]] end generated region [Generated Methods]
	
	CCloseUpContainer* container;
	// ]]] end [Public Section]
	
	
	// [[[ begin [Protected Section]
protected:
	// from base class CAknView
	void DoActivateL(
		const TVwsViewId& aPrevViewId,
		TUid aCustomMessageId,
		const TDesC8& aCustomMessage );
	void DoDeactivate();
	void HandleStatusPaneSizeChange();
	
	// [[[ begin generated region: do not modify [Overridden Methods]
	// ]]] end generated region [Overridden Methods]
	
	
	// [[[ begin [User Handlers]
	// ]]] end [User Handlers]
	
	// ]]] end [Protected Section]
	
	
	// [[[ begin [Private Section]
private:
	void SetupStatusPaneL();
	void CleanupStatusPane();
	
	// [[[ begin generated region: do not modify [Generated Instance Variables]
	CCloseUpContainer* iCloseUpContainer;
	// ]]] end generated region [Generated Instance Variables]
	
	// [[[ begin generated region: do not modify [Generated Methods]
	// ]]] end generated region [Generated Methods]
	
	// ]]] end [Private Section]
	
	};

#endif // CLOSEUPCONTAINERVIEW_H
