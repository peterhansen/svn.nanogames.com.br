/*
 * CMainCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CMAINCANVASVIEW_H_
#define CMAINCANVASVIEW_H_

#include "Config.h"
#include "CanvasView.h"
#include "CallsMonitor.h"
#include "CContactsCanvasView.h"

class CMainCanvasView : public CanvasView,  MCallCallBack
{
	public:
		CMainCanvasView( TInt aXRes, TInt aYRes);
		virtual ~CMainCanvasView( void );
		
		virtual void SendSignal( int aSignal );
	
		int getState() const;
		void setState(int aState);
		
		void setContacts(RPointerArray< CContactsCanvasView::CCloseUpContact > *aContacts);

	private:
		void CallStatusChangedL( CTelephony::TCallStatus& aStatus, TInt aError );
		int iState;		
		CCallMonitor *pCallMonitor;	
		RPointerArray< CContactsCanvasView::CCloseUpContact > *pContacts;
};


inline int CMainCanvasView::getState() const
	{
	return iState;		
	}

inline void CMainCanvasView::setState(int aState)
	{
		iState=aState;
	}


inline void CMainCanvasView::setContacts(RPointerArray< CContactsCanvasView::CCloseUpContact > *aContacts)
	{	
		pContacts=aContacts;
		if (pContacts->Count()==0)
			getDisplaylet(DL_BTN_INICIAR)->iGoto=SIGN_NOP-2;
	}
	
#endif /* CMAINCANVASVIEW_H_ */
