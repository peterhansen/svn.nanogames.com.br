/*
 * CExplosionCanvasView.h
 *
 *  Created on: 20/05/2010
 *      Author: monteiro
 */

#ifndef CEXPLOSIONCANVASVIEW_H_
#define CEXPLOSIONCANVASVIEW_H_

#include "CanvasView.h"
		
class CExplosionCanvasView : public CanvasView
{
	public:
		CExplosionCanvasView(TInt aXRes,TInt aYRes);
		virtual ~CExplosionCanvasView();

		virtual void SendSignal( int aSignal );
		void setSelection(TInt32 aSelection);
		TInt32 getSelection() const;
	private:
		TApaTask *pTask;
		TInt32 iSelection;
		CFbsBitmap *pGender[2];	
		
};

inline TInt32 CExplosionCanvasView::getSelection() const
	{
		return iSelection;	
	}
	
inline void CExplosionCanvasView::setSelection(TInt32 aSelection)
	{
		iSelection=aSelection;
	}

#endif /* CEXPLOSIONCANVASVIEW_H_ */
