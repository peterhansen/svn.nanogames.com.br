/*
 * CFinishedCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CFINISHEDCANVASVIEW_H_
#define CFINISHEDCANVASVIEW_H_

#include "CanvasView.h"

class CFinishedCanvasView : public CanvasView
{
	public:
		CFinishedCanvasView(TInt aXRes,TInt aYRes);
		virtual ~CFinishedCanvasView();
		virtual void SendSignal(int aSignal){}
};

#endif /* CFINISHEDCANVASVIEW_H_ */
