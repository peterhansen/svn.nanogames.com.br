/*
========================================================================
 Name        : CloseUpDocument.h
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
#ifndef CLOSEUPDOCUMENT_H
#define CLOSEUPDOCUMENT_H

#include <akndoc.h>
		
class CEikAppUi;

/**
* @class	CCloseUpDocument CloseUpDocument.h
* @brief	A CAknDocument-derived class is required by the S60 application 
*           framework. It is responsible for creating the AppUi object. 
*/
class CCloseUpDocument : public CAknDocument
	{
public: 
	// constructor
	static CCloseUpDocument* NewL( CEikApplication& aApp );

private: 
	// constructors
	CCloseUpDocument( CEikApplication& aApp );
	void ConstructL();
	
public: 
	// from base class CEikDocument
	CEikAppUi* CreateAppUiL();
	};
#endif // CLOSEUPDOCUMENT_H
