/*
========================================================================
 Name        : CloseUpApplication.h
 Author      : Daniel
 Copyright   : 2010� Nano Games - Todos os direitos reservados
 Description : 
========================================================================
*/
#ifndef CLOSEUPAPPLICATION_H
#define CLOSEUPAPPLICATION_H

// [[[ begin generated region: do not modify [Generated Includes]
#include <aknapp.h>
// ]]] end generated region [Generated Includes]

// [[[ begin generated region: do not modify [Generated Constants]
const TUid KUidCloseUpApplication = { 0x20030B10 };
// ]]] end generated region [Generated Constants]

/**
 *
 * @class	CCloseUpApplication CloseUpApplication.h
 * @brief	A CAknApplication-derived class is required by the S60 application 
 *          framework. It is subclassed to create the application's document 
 *          object.
 */
class CCloseUpApplication : public CAknApplication
	{
private:
	TUid AppDllUid() const;
	CApaDocument* CreateDocumentL();
	
	};
			
#endif // CLOSEUPAPPLICATION_H		
