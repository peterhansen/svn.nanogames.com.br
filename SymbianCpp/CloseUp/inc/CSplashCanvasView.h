/*
 * CSplashCanvasView.h
 *
 *  Created on: 27/04/2010
 *      Author: Daniel
 */

#ifndef CSPLASHCANVASVIEW_H_
#define CSPLASHCANVASVIEW_H_

#include "CanvasView.h"

class CSplashCanvasView : public CanvasView
{
	public:
		CSplashCanvasView( TInt aXRes, TInt aYRes);
		virtual ~CSplashCanvasView();		
		virtual void SendSignal( int aSignal );	
};



#endif /* CSPLASHCANVASVIEW_H_ */
