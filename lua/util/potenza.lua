-- Gerador de documenta��o para Buongiorno
-- Peter Hansen
DEBUG = true

-- separador de diret�rios no Windows
DIR_SEP = '\\'

require "lfs"
dofile( "functions.lua" )

if # arg < 1 then
	print( "Uso: " .. arg[ 0 ] .. " arquivo.lua" )
	os.exit()
end

-- quantidade m�xima de caracteres no nome do arquivo (16, excluindo o '.jar' e o 'v', para indicar a vers�o)
MAX_NAME_LENGTH = 11

-- prefixo do arquivo de solicita��o de teste
PREFIX_SOL = [[326002-SOL001 � NanoGames_]]
-- prefixo do arquivo de espefica��o de aplica��o
PREFIX_SPEC = [[326002-EST001 � NanoGames_]]
-- subdiret�rio da documenta��o
DIR_DOCS = [[Documenta��o]]

-- armazena o nome do arquivo descritor
FILE_LUA = arg[ 1 ]

local index = nil
local start = 0
repeat 
	local temp = string.find( FILE_LUA, DIR_SEP, start )
	if ( temp ~= nil ) then
		start = temp + 1
		index = temp
	end
until ( temp == nil )

if ( index ~= nil ) then
	lfs.chdir( string.sub( FILE_LUA, 0, index ) )
end

-- diret�rio atual
local CUR_DIR = lfs.currentdir() .. DIR_SEP

print( 'current dir: ' .. CUR_DIR )

INFO_URL = [[MIDlet-Jar-URL: ]]
MATCH = [[MIDlet%-Jar%-URL:% ]]

dofile( FILE_LUA )

if DEBUG == true then
	print( 'table size: ' .. # VERSIONS )
	print( 'app name: ' .. APP_NAME )
	print( 'path spec: ' .. PATH_APP_SPEC)
	print( 'path faavd: ' .. PATH_FAAVD )
	print( 'path sol: ' .. PATH_SOL )
end

-- l� todas as vers�es da tabela
for i, version in ipairs( VERSIONS ) do
	if ( DEBUG == true ) then
		print( 'i: ' .. i )
		print( '\tjad: ' .. version[ 'jad' ] )
		print( '\tjar: ' .. version[ 'jar' ] )
		print( '\tnumber: ' .. version[ 'number' ] )
		print( '\tdevice: ' .. version[ 'device' ] .. '\n' )
	end
	
	if ( version[ 'jad' ] ~= nil and version[ 'device' ] ~= nil ) then
		-- se o caminho do arquivo .jar estiver vazio, utiliza o mesmo nome do .jad, trocando apenas a extens�o
		if ( version[ 'jar' ] == nil or string.len( version[ 'jar' ] ) == 0 ) then
			version[ 'jar' ] = string.gsub( version[ 'jad' ], "%.jad", ".jar" )
		end
		
		-- "limpa" o n�mero da vers�o, removendo o caracter '.' (ponto)
		local versionClean = string.gsub( version[ 'number' ], '%.', '' )
		local length = string.len( version[ 'device' ] ) + string.len( versionClean )
		
		local maxLength = math.max( MAX_NAME_LENGTH - length, 1 )
		
		-- prefixo do nome do aplicativo
		local appName = string.sub( string.lower( APP_NAME ), 0, maxLength ) .. version[ 'device' ] .. 'v' .. versionClean
		
		if ( DEBUG == true ) then
			print( 'version clean: ' .. versionClean )
			print( 'length: ' .. length )
			print( 'max length: ' .. maxLength )
			print( 'app name: ' .. appName )
		end
		
		-- cria o diret�rio de documenta��o
		local path = CUR_DIR .. appName
		local ret, msg = chkmkdir( path )
		path = path .. DIR_SEP
		
		if ( ret == true ) then
			ret, msg = chkmkdir( path .. DIR_DOCS )
		end
		
		local pathJadJar = path .. appName
		if ( ret == true ) then
			ret, msg = chkmkdir( pathJadJar )
		end
		pathJadJar = pathJadJar .. DIR_SEP
		
		if ( ret == true ) then
			ret, msg = copyFile( CUR_DIR .. PATH_APP_SPEC, path .. DIR_DOCS .. DIR_SEP .. PREFIX_SPEC .. appName .. '.doc' )
			if ( ret == false ) then
				print( 'error: ' .. msg .. '\n' )
			end
			
			ret, msg = copyFile( CUR_DIR .. PATH_SOL, path .. DIR_DOCS .. DIR_SEP .. PREFIX_SOL .. appName .. '.xls' )
			if ( ret == false ) then
				print( 'error: ' .. msg .. '\n' )
			end
			
			-- antes de copiar o arquivo .jad, troca o caminho para o arquivo .jar
			local jadLines, jadPathIndex = prepareJad( CUR_DIR .. version[ 'jad' ], MATCH )
			ret, msg = createJad( jadLines, pathJadJar .. appName .. '.jad', jadPathIndex, INFO_URL .. appName .. '.jar' )
			if ( ret == false ) then
				print( 'error: ' .. msg .. '\n' )
			end
			
			ret, msg = copyFile( CUR_DIR .. version[ 'jar' ], pathJadJar .. appName .. '.jar' )
			if ( ret == false ) then
				print( 'error: ' .. msg .. '\n' )
			end
		else
			print( 'erro ao criar diret�rio: ' .. msg )
		end
	end
end

