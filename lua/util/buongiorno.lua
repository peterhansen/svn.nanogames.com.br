-- Gerador de documenta��o para Buongiorno
-- Peter Hansen
require "lfs"
dofile( "functions.lua" )

if # arg < 2 then
	print( "Uso: " .. arg[ 0 ] .. " nome_do_jogo lista.csv lista_saida" )
	os.exit()
end


-- armazena o nome do aplicativo e dos arquivos
APP_NAME = arg[ 1 ]
LIST = arg[ 2 ]
OUTPUT = arg[ 3 ]
if ( OUTPUT == nil ) then
	OUTPUT = APP_NAME .. ".xls"
end

INFO_URL = [[MIDlet-Jar-URL: ]]
MATCH = [[MIDlet%-Jar%-URL:% ]]

previousJad = ""
jarOriginalFilename = ""
jadFile = {}
jadURLIndex = -1

lineInfo = {}
VENDOR = 0
MODEL = 1
JAD_NAME = 2

-- abre o arquivo de sa�da para escrita
outputFile = io.open( OUTPUT, "w+" )

-- l� todas as linhas da lista
for line in io.lines( LIST ) do
	local i = VENDOR
	for info in string.gmatch( line, "[%w%s()%.%-%_]*[^,]" ) do
		lineInfo[ i ] = info
		i = i + 1
	end
	
	if ( lineInfo[ JAD_NAME ] ~= previousJadName ) then
		-- garante que o nome do arquivo termina com .jad
		if ( string.find( lineInfo[ JAD_NAME ], ".jad" ) == nil ) then
			lineInfo[ JAD_NAME ] = lineInfo[ JAD_NAME ] .. ".jad"
		end
		
		jadFile, jadURLIndex = prepareJad( lineInfo[ JAD_NAME ], MATCH )
		previousJadName = lineInfo[ JAD_NAME ]
		jarOriginalFilename = string.gsub( previousJadName, "%.jad", ".jar" ) 
	end
	
	-- "limpa" os nomes dos arquivos, primeiro removendo nomes comerciais (como RAZR)
	lineInfo[ MODEL ] = string.gsub( lineInfo[ MODEL ], " %(.+%)", "" )
	-- remove poss�veis espa�os antes do ponto e espa�os duplos
	lineInfo[ MODEL ] = string.gsub( lineInfo[ MODEL ], "  ", "" )
	lineInfo[ MODEL ] = string.gsub( lineInfo[ MODEL ], " %.", "" )
	lineInfo[ MODEL ] = string.gsub( lineInfo[ MODEL ], " %.", "" )
	-- agora troca espa�os por '_'
	lineInfo[ MODEL ] = string.gsub( lineInfo[ MODEL ], " ", "_" )
	
	
	-- novo nome dos arquivos (sem as extens�es .jad e .jar)
	local newFilename = APP_NAME .. "_" .. lineInfo[ VENDOR ] .. "_" .. lineInfo[ MODEL ]
	local newJadFilename = newFilename .. ".jad"
	local newJarFilename = newFilename .. ".jar"

	createJad( jadFile, newJadFilename, jadURLIndex, INFO_URL .. newJarFilename )
	copyJar( jarOriginalFilename, newJarFilename )
	
	outputFile:write( lineInfo[ VENDOR ] .. "\t" .. lineInfo[ MODEL ] .. "\t" .. newJadFilename .. "\n" )
end

-- fecha o arquivo de sa�da
outputFile:close()

