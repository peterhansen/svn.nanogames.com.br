-- fun��o que carrega o jad especificado, trocando a linha que cont�m o endere�o do jar por uma linha vazia
-- retorna 2 valores: uma tabela com as linhas do arquivo jad e o �ndice da linha que cont�m a informa��o do endere�o do jar
function prepareJad( jadFilename, match )
	-- tabela com as linhas do arquivo jad
	local jadFile = {}
	
	local currentLine = 1;
	-- �ndice da linha que cont�m a informa��o do endere�o do jar
	local indexJarUrl = 0
	
	for line in io.lines( jadFilename ) do 
		-- verifica se � a linha que cont�m o nome do arquivo .jar
		if ( string.match( line, match ) ~= nil ) then
			-- armazena o �ndice da linha que cont�m a informa��o da url do arquivo .jar
			table.insert( jadFile, "" )
			indexJarUrl = currentLine
		else
			table.insert( jadFile, line .. "\n" )
			
			currentLine = currentLine + 1
		end
	end
	
	return jadFile, indexJarUrl
	
end



function createJad( jadLines, filename, index, url )
	local file, msg = io.open( filename, "w" )
	
	if ( file == nil ) then
		return false, msg
	else
		for i, line in ipairs( jadLines ) do
			if ( i == index ) then
				file:write( url .. "\n" )
			else
				file:write( line )
			end
		end	
		
		file:close()
	end
	
	return true
end


function copyFile( oldPath, newPath )
	print( "copying file \"" .. oldPath .. '\" to \"' .. newPath .. '\"' )
	local old, msg = io.open( oldPath, "rb" )
	
	if ( old == nil ) then
		--print( "OLD == nil: " .. oldPath )
		return false, 'error opening file: ' .. msg
		--os.exit()
	end
	
	local data = old:read( "*a" )
	
	local new
	new, msg = io.open( newPath, "w+b" )
	if ( new == nil ) then
		return false, 'error creating file: ' .. msg
	end
	new:write( data )
	
	old:close()
	new:close()
	
	return true
end


function chkmkdir( dir )
	local ret, msg = lfs.attributes( dir )
	if ( ret == nil or ret.mode ~= "directory" ) then
		return lfs.mkdir( dir )
	end
	
	return true
end
