class JadFile
  
  def initialize( filename )
    @info = {}
    
    file = File.open( filename, 'r' )
    lines = file.readlines()
    
    for line in lines
      parts = line.split( /(: )/ )
      #parts[ 1 ] � igual ao separador ': '
      @info[ parts[ 0 ] ] = parts[ 2 ].chomp()
    end
  end
  
  
  def save_to( filename )
    File.open( filename, 'w+' ) do |output|
      @info.each { |key, value| 
        output.puts( "#{key}: #{value}" )
      }
    end
  end


  def data()
    data = StringIO.new( "", 'w+' )
    @info.sort.each { |entry|
#      data.puts "#{key}: #{value}"
      data.puts "#{entry[ 0 ]}: #{entry[ 1 ]}"
    }
    data.close()

    return data.string
  end
  
  
  def info
    @info
  end


  ##
  # Varre todas as entradas do arquivo jad referentes a arquivos de dados do aplicativo (jar ou cod).
  # Retorna um bloco com par |key, value| a cada entrada encontrada.
  ##
  def file_paths
    @info.each { |key, value|
      if ( key.match( /(MIDlet-Jar-URL)|(RIM-COD-URL)/ ) )
        yield( key, value )
      end
    }
  end
  
end