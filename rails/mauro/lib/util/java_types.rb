module JavaTypes
  require 'bit-struct'
  
  class EOFException < Exception
  end
  
  class UTFDataFormatException < Exception
  end
  
   EULER = 2.718281828
  
  ##
  #
  ##
  class InputStream
    
    # posi��o de leitura atual (vai de zero a @string.length - 1)
    @pos
    # string que representa os bytes do stream
    @string
    
    def initialize( s )
      @pos = 0
      @string = s
    end
    
    ##
    # L� um byte (sem sinal) do stream.
    ##
    def read_ubyte()
      byte = @string[ @pos ]
      @pos += 1
      
      if ( @pos > @string.length )
        raise EOFException
      end
      
      return byte
    end
    
    
    ##
    # L� um byte (com sinal) do stream
    ##
    def read_byte()
      b = read_ubyte()
      return ( b & 0xff ) > 0x7f ? b - 0x100 : b # TODO melhor algoritmo?
    end
    
    
    ##
    #
    ##
    def read_char()
      return ( ( read_byte() << 8 ) | ( read_byte() & 0xff ) )
    end
    
    
    ##
    #
    ##
    def read_short()
      return ( ( read_byte() << 8 ) | ( read_ubyte() ) )
    end
    
    
    ##
    #
    ##
    def read_ushort()
      return ( ( ( read_byte() & 0xff ) << 8 ) | ( read_byte() & 0xff ) )
    end
    
    
    ##
    #
    ##
    def read_int()
      return ( ( ( read_byte()  ) << 24 ) | 
               ( ( read_ubyte()  ) << 16 ) | 
               ( ( read_ubyte()  ) <<  8 ) | 
                 ( read_ubyte()  ) )
    end
    
    
    ##
    #
    ##
    def read_uint() # TODO corrigir
      i = read_int()
      return ( ~i ) + 1
    end
    
    
    ##
    #
    ##
    def read_long()
      return  ( ( ( read_byte() ) << 56 ) |
                ( ( read_ubyte() ) << 48 ) |
                ( ( read_ubyte() ) << 40 ) |
                ( ( read_ubyte() ) << 32 ) |
                ( ( read_ubyte() ) << 24 ) |
                ( ( read_ubyte() ) << 16 ) |
                ( ( read_ubyte() ) <<  8 ) |
                ( ( read_ubyte() ) ) )
    end
    
    
    ##
    #
    ##
    def read_float()
      # TODO testar grava��o e leitura de floats/doubles
      bits = read_uint()
      s = ( ( bits >> 31 ) == 0) ? 1 : -1
      e = ( ( bits >> 23 ) & 0xff )
      m = ( e == 0 ) ? ( bits & 0x7fffff ) << 1 : ( bits & 0x7fffff ) | 0x800000

      return s * m * ( 2** ( EULER - 150 ) ) # TODO confirmar se algoritmo est� ok
    end
    
    
    ##
    #
    ##
    def read_double()
      bits = read_long()
      s = ( ( bits >> 63 ) == 0 ) ? 1 : -1
      e = ( ( bits >> 52 ) & 0x7ff )
      long m = ( e == 0 ) ? ( bits & 0xfffffffffffff ) << 1 : ( bits & 0xfffffffffffff ) | 0x10000000000000
      
      return s * m * ( 2** ( EULER - 1075 ) ) # TODO confirmar algoritmo
    end
    

    ##
    # L� uma string no formato Java e retorna uma string unicode.
    ##
    def read_string()
      return read_str( false )
    end
    
    
    ##
    # L� uma string no formato Java e retorna uma string tamb�m no formato Java.
    ##    
    def read_utf()
      return read_str( true )
    end
    
    
    ##
    # Avan�a 'n' bytes no stream.
    ##
    def skip( n )
      @pos += n
    end
    
    
    ##
    #
    ##
    def reset()
      @pos = 0
    end
    
    
    ##
    # L� um boolean do stream.
    ##
    def read_boolean()
      return read_ubyte() != 0
    end
    
    
    ##
    #
    ##
    def available()
      return @string.length - @pos
    end
    
    ##
    #
    ##
    def pos()
      return @pos
    end
    
    
    ##
    # 
    ##
    def read_available()
      return @string.slice( @pos..@string.length )
    end
    
    
    ############################################################################
    # M�TODOS PRIVADOS                                                         #
    ############################################################################    
    private
    
    ##
    # L� uma string formatada no padr�o Java. Se add_length for true, retorna uma string
    # tamb�m no padr�o Java (2 bytes indicando o tamanho, e ent�o 'n' caracteres
    # unicode). Caso seja false, retorna a string sem o indicador de tamanho no
    # in�cio.
    ##
    def read_str( add_length )
      length = read_ushort()

      utf = ""
      if ( add_length )
        utf << length
      end
      
      # TODO verificar se algoritmo de caracteres unicode est� ok - n�o � poss�vel testar no momento devido ao bug de strings com acentos...
      1.upto( length ) {
        char = read_ubyte()

        if ( ( char & 0x10000000 ) == 0 )
          # caracter de 1 byte
          utf << char
        elsif ( ( char & 0x11000000 ) == 0x11000000 )
          # caracter de 2 bytes
          utf << ( ( char & 0x1f ) << 6 )
          utf << ( read_char() & 0x3f )
        elsif ( ( char & 0x11100000 ) == 0x11100000 )
          # caracter de 3 bytes
          utf << ( ( char & 0x0f ) << 12 )
          utf << ( ( read_char() & 0x3f ) << 6 )
          utf << ( read_char() & 0x3f )
        elsif ( ( ( char & 0x11110000 ) == 0x11110000 ) || ( ( char & 0x10000000 ) == 0x10000000 ) )
          # lan�a UTFDataFormatException  
          raise UTFDataFormatException
        end
      }
      
#      puts( "RETORNOU #{utf}")
      return utf
    end    
    
  end
  
  
  ##
  #
  ##
  class OutputStream
    
    #
    @string
    
    
    def initialize()
      @string = ""
    end    
    
    
    ##
    #
    ##
    def write_byte( byte )
      write_java_byte( Byte.new( byte ) )
    end
    
    
    ##
    #
    ##
    def write_java_byte( jbyte )
      @string << jbyte.to_s()
    end
    
    
    ##
    #
    ##
    def write_char( char )
      write_java_char( Char.new( char ) )
    end
    
    
    ##
    #
    ##
    def write_java_char( jchar )
      @string << jchar.to_s()
    end
    
    
    ##
    #
    ##
    def write_short( short )
      write_java_short( Short.new( short ) )
    end
    
    
    ##
    #
    ##
    def write_java_short( jshort )
      @string << jshort.to_s()
    end
    
    
    ##
    #
    ##
    def write_int( int )
      write_java_int( Integer.new( int ) )
    end
    
    
    ##
    #
    ##
    def write_java_int( jint )
      @string << jint.to_s()
    end
    
    
    ##
    #
    ##
    def write_long( long )
      write_java_long( Long.new( long ) )
    end
    
    
    ##
    #
    ##
    def write_java_long( jlong )
      @string << jlong.to_s()
    end
    
    
    ##
    #
    ##
    def write_float( float )
      write_java_float( Float.new( float ) )
    end
    
    
    ##
    #
    ##
    def write_java_float( jfloat )
      @string << jfloat.to_s()
    end
    
    
    ##
    #
    ##
    def write_double( double )
      write_java_float( Double.new( double ) )
    end
    
    
    ##
    #
    ##
    def write_java_double( jdouble )
      @string << jdouble.to_s()
    end
    
    
    ##
    # Escreve uma string j� no formato java no stream.
    ##
    def write_utf( utf )
      @string << utf.to_s()
    end
    
    
    ##
    #
    ##
    def write_java_string( jstring )
      write_utf( jstring )
    end
    
    
    ##
    # Escreve uma string no padr�o Ruby no stream, convertendo-a para o formato Java.
    ##
    def write_string( string )
      write_utf( String.new( string ) )
    end
    
    
    ##
    # Retorna o array de bytes contendo todos os dados escritos, no formato Java.
    ##
    def flush()
      return @string
    end
    
    
    ##
    # Reinicia o stream, descartando todos os dados j� gravados.
    ##
    def reset()
      @string = ""
    end
    
    
    ##
    # Escreve os erros presentes num objeto que deriva de ActiveRecord.
    ##
    def write_errors( obj )
      obj.errors.each_full { |msg|
#        logger.info( "MSG: #{msg}")
        write_byte( ID_RETURN_CODE )
        write_short( extract_error_code( msg ) )
        write_byte( ID_ERROR_MESSAGE )
        write_string( msg )
      }     
    end
    
  end
  
  
  
  # TODO usar jcode?

  # FIXME utilizar caracteres acentuados �S VEZES causa problema do Ruby se perder em rela��o
  # �s linhas de c�digo (syntax error, unexpected tIDENTIFIER, expecting kEND). O problema
  # � intermitente: copiar o c�digo num novo arquivo Ruby ou no iRb funciona...
  
  ##
  #
  ##
  class Char < BitStruct
    text  :char, 16, "char"

    initial_value.char = ""
    
    def initialize( c )
      super()
      
      temp = ""

      c = c[ 0 ]
      if ( c >= 0xC0 )
        temp << 0xC3
        temp << ( c - 0x40 )
      elsif ( c >= 0x80 )
        temp << 0xC2
        temp << c
      else
        temp << 0x0
        temp << c
      end
      
      self.char = temp
    end
  end


  ##
  #
  ##
  class Byte < BitStruct
    signed  :byte,  8,  "byte"
    
    initial_value.byte = 0

    def initialize( b )
      super()
      
      self.byte = b
    end
  end


  ##
  #
  ##
  class Short < BitStruct
    signed  :short, 16, "short", :endian => :big

    initial_value.short = 0
    
    def initialize( s )
      super()
      
      self.short = s
    end
  end


  ##
  #
  ##
  class Integer < BitStruct
    signed  :int, 32, "int", :endian => :big
    
    initial_value.int = 0

    def initialize( i )
      super()
      
      self.int = i
    end
  end


  ##
  #
  ##
  class Long < BitStruct
    signed  :long,  64, "long", :endian => :big
    
    initial_value.long = 0

    def initialize( l )
      super()
      
      self.long = l
    end
  end
  
  
  ##
  #
  ##
  class Float < BitStruct
    float :float, 32, "float"
    
    initial_value.float = 0.0
    
    def initialize( f )
      super()
      
      self.float = f
    end
  end

  
  ##
  #
  ##
  class Double < BitStruct
    float :double, 64, "double"
    
    initial_value.double = 0.0
    
    def initialize( d )
      super()
      
      self.double = d
    end
  end


  ##
  #
  ##
  class String < BitStruct
    signed    :length,     16,     "length"
    rest      :content,            "content"

    note "classe que representa uma string unicode no formato Java: 2 bytes indicando o tamanho, e ent�o 'n' caracteres UTF-8"

    initial_value.length = 0
    initial_value.content = ""
    
    def initialize( s )
      super()
      
      temp = ""
      diff = 0

      # converte cada caracter para unicode. fonte: http://www.intertwingly.net/stories/2004/04/14/i18n.html
      s.each_byte { |char|
        if ( char >= 0xC0 )
          temp << 0xC3
          temp << ( char - 0x40 )

          # adicionou mais um byte
          diff = diff + 1
        elsif ( char >= 0x80 )
          temp << 0xC2
          temp << char

          # adicionou mais um byte
          diff = diff + 1
        else
          temp << char
        end
      }

      # o comprimento final � o n�mero de bytes, que n�o necessariamente corresponde
      # ao n�mero de caracteres (bytes >= chars)
      self.length = s.length + diff
      self.content = temp
    end
  end 

  
  # teste nada otimizado para conferir se grava��o e leitura est�o corretos
#  output = OutputStream.new()
#  
##  limit = 18446744073709551614 # unsigned long
#  limit = 10
#  
#  steps = 0
#  step = 1 # limit / 100
#  current = -limit - 1
#  i = current
#  while ( i <= limit )
#    output.reset()
#    output.write_float( i )
#    
#    input = InputStream.new( output.flush )
#    puts( "#{i}: #{input.read_float()}" )
#    r = input.read_float()
#    if ( r != i )
#      puts( "ERRO: #{i} !=#{r}" )
#      break
#    end
    
#    if ( i >= current )
#      if ( steps % 2 == 0 )
#        puts( "#{steps / 2}: #{i}" )
#      end
#      
#      current += step
#      steps += 1
#    end
#    
##    i += step / 9001
#    i += 1
#  end
  
end
