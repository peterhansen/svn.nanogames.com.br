require 'mongrel_cluster/recipes'

# be sure to change these
set :user, 'peter'
set :domain, 'online.nanogames.com.br'
set :application, 'nano_online'

# the rest should be good
set :deploy_to, "/var/www/rails/#{application}" 
set :deploy_via, :export
set :repository, "http://svn.nanogames.com.br/rails/nano_online/"
set :scm_username, "nano_online_svn"
set :scm_password, 'nAnO_SvN123'
set :scm_verbose, true
set :chmod755, "app config db lib public vendor script script/* public/disp*"

set :mongrel_config, "/etc/mongrel_cluster/#{application}.yml"

default_run_options[ :pty ] = true
# Your web server, e.g. nginx
role :web, "#{domain}"
# Your application server, e.g. mongrel
role :app, "#{domain}"
# Your database server, e.g. MySQL
role :db, "#{domain}", :primary => true

# You need this in order to get prompted for password.
namespace :deploy do
  [:start, :stop, :restart].each do |t|
    desc "#{t.to_s.capitalize} the mongrel app server"
    task t do
      sudo "mongrel_rails cluster::#{t.to_s} -C #{mongrel_config}"
    end
  end
end


desc "Link shared files"
task :before_symlink do
  run "ln -s #{shared_path}/files #{release_path}/files"
end



################################################################################
###################### CONFIGURAÇÕES DO DREAMHOST ##############################
################################################################################
#default_run_options[:pty] = true
#
## be sure to change these
#set :user, 'piteco'
#set :domain, 'online.nanogames.com.br'
#set :application, 'nano_online'
#
## the rest should be good
##set :repository,  "#{user}@#{domain}:git/#{application}.git" 
#set :deploy_to, "/home/#{user}/#{domain}" 
#set :deploy_via, :remote_cache
#set :repository, "http://svn.nanogames.com.br/rails/nano_online/"
#set :scm_username, "nano_online_svn"
##set( :scm_password ) { Capistrano::CLI.password_prompt }
#set :scm_password, 'nAnO_SvN123'
#set :scm_verbose, true
#set :use_sudo, false
#set :chmod755, "app config db lib public vendor script script/* public/disp*"
#
#
#server domain, :app, :web
#role :db, domain, :primary => true
#
#namespace :deploy do
#  task :restart do
#    run "touch #{current_path}/tmp/restart.txt" 
#  end
#end
#
#
#desc "Make spin script executable"
#task :make_spin_script_executable, :roles => :app do
#  run "chmod +x #{current_path}/script/spin"
#end
#before "deploy:start", "make_spin_script_executable" 
#
#desc "Tasks to execute after code update" 
#task :after_update_code, :roles => [:app, :db, :web] do
#  # fix permissions
#  run "chmod +x #{release_path}/script/process/reaper" 
#  run "chmod +x #{release_path}/script/process/spawner" 
#  run "chmod 755 #{release_path}/public/dispatch.*" 
#end










################################################################################
######################### ARQUIVO ANTIGO ABAIXO ################################
################################################################################

## Introductory SwitchTower config for Dreamhost.
##
## ORIGINAL BY: Jamis Buck
##
## Docs: http://manuals.rubyonrails.com/read/chapter/98
##
## HIGHLY MODIFIED BY: Geoffrey Grosenbach boss@topfunky.com
##
## Docs: http://nubyonrails.com/pages/shovel_dreamhost
##
## USE AT YOUR OWN RISK! THIS SCRIPT MODIFIES FILES, MAKES DIRECTORIES, AND STARTS
## PROCESSES. FOR ADVANCED OR DARING USERS ONLY!
##
## DESCRIPTION
##
## This is a customized recipe for easily deploying web apps to a shared host.
##
## You also need to modify Apache's document root using Dreamhost's web control panel.
##
## For full details, see http://nubyonrails.com/pages/shovel_dreamhost
##
## To setup lighty, first edit this file for your primary Dreamhost account.
##
## Then run:
##   rake remote:exec ACTION=setup
##
## This will create all the necessary directories for running Switchtower.
##
## From then, you can deploy your application with Switchtower's standard
##   rake deploy
##
## Or rollback with
##   rake rollback
## 
## This defines a deployment "recipe" that you can feed to switchtower
## (http://manuals.rubyonrails.com/read/book/17). It allows you to automate
## (among other things) the deployment of your application.
##
## NOTE: When editing on Windows, be sure to save with Unix line endings (LF).
#
#set :user, 'piteco'
#set :application, "online.nanogames.com.br"
#set :repository, "http://svn.nanogames.com.br/rails/nano_online/"
#set :svn_username, "peter"
#set( :svn_password ) { Capistrano::CLI.password_prompt }
## NOTE: If file:/// doesn't work for you, try this:
##set :repository, "svn+ssh://home/#{user}/svn/#{application}"
#
#
## =============================================================================
## You shouldn't have to modify the rest of these
## =============================================================================
#
#role :web, application
#role :app, application
#role :db,  application, :primary => true
#
#set :deploy_to, "/home/#{user}/#{application}"
## set :svn, "/path/to/svn"       # defaults to searching the PATH
#set :use_sudo, false
#set :restart_via, :run
#set :checkout, "export" 
#
#
#desc "Restart the FCGI processes on the app server as a regular user."
#task :restart, :roles => :app do
#  #run "killall -9 ruby"
#  run "ruby #{current_path}/script/process/reaper --dispatcher=dispatch.fcgi"
#end
#
#task :after_symlink, :roles => [:web, :app] do
#
#  # Force production enviroment by replacing a line in environment.rb
#  #run "perl -i -pe \"s/#ENV\\['RAILS_ENV'\\] \\|\\|= 'production'/ENV['RAILS_ENV'] ||= 'production'/\" #{current_path}/config/environment.rb"
#
#  # Make dispatcher executable
#  run "chmod a+x #{current_path}/public/dispatch.fcgi"
#  
#  # Ensure Rails in the vendor dir is used by replacing a line in fcgi_handler.rb
#  run "perl -i -pe \"s/require 'dispatcher'/require '#{current_path.gsub(/\//, '\\/')}\\/vendor\\/rails\\/railties\\/lib\\/dispatcher'/\" #{current_path}/vendor/rails/railties/lib/fcgi_handler.rb"
#end
#
#desc "Make spin script executable"
#task :make_spin_script_executable, :roles => :app do
#  run "chmod +x #{current_path}/script/spin"
#end
#before "deploy:start", "make_spin_script_executable" 
#
#desc "Tasks to execute after code update" 
#task :after_update_code, :roles => [:app, :db, :web] do
#  # fix permissions
#  run "chmod +x #{release_path}/script/process/reaper" 
#  run "chmod +x #{release_path}/script/process/spawner" 
#  run "chmod 755 #{release_path}/public/dispatch.*" 
#end
#
#desc "Restarting after deployment" 
#task :after_deploy, :roles => [:app, :db, :web] do
#  run "touch #{release_path}/public/dispatch.fcgi" 
#end
#
#desc "Restarting after rollback" 
#task :after_rollback, :roles => [:app, :db, :web] do
#  run "touch #{release_path}/public/dispatch.fcgi" 
#end
#
#
##desc "Link shared files"
##task :before_symlink do
##  run "rm -drf #{release_path}/public/files/uploads"
##  run "ln -s #{shared_path}/uploads #{release_path}/public/files/uploads"
##end