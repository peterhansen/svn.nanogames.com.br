ActionController::Routing::Routes.draw do |map|
  
  map.connect 'customers/login', :controller => 'customers', :action => 'login', :conditions => { :method => :post }
  
  map.resources :devices, :vendors, :families, :midp_versions, :cldc_versions, :apps, 
                :app_categories, :app_versions, :bands, :data_services, :device_bugs,
                :optional_apis, :operators, :integrators, :submissions, :customers,
                :ranking_entries, :download_codes, :user_roles, :partners, :users
                # :midlet_reports, :downloads

#  map.resources :customers #, :except => :show
#  map.resources :customers, :member => { :login => [:get, :post] }

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.


  map.connect 'download/:id', :controller => 'downloads', :action => 'download_file_by_id'
  map.connect 'downloads/', :controller => 'downloads', :action => 'select_app'
  map.connect 'test/', :controller => 'test', :action => 'detect'

  #endere�os acessados pelos aparelhos ap�s efetuar instala��o ou exclus�o de um aplicativo
  map.connect 'notify/install/:id', :controller => 'midlet_reports', :action => 'install'
  map.connect 'notify/delete/:id', :controller => 'midlet_reports', :action => 'delete'

  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'

  # no caso de qualquer p�gina inv�lida, envia para a p�gina principal da Nano Games TODO caso o :controller seja v�lido,
  # n�o funciona (testar se usu�rio est� logado - for�ar SEMPRE acesso inicial a /login)
  map.connect "*anything",
              :controller => 'login',
              :action => 'redirect_to_nano_games'
end
