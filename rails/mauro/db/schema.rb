# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20090326195008) do

  create_table "app_categories", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "app_categories_apps", :id => false, :force => true do |t|
    t.integer "app_id",          :limit => 11, :null => false
    t.integer "app_category_id", :limit => 11, :null => false
  end

  add_index "app_categories_apps", ["app_id", "app_category_id"], :name => "index_app_categories_apps_on_app_id_and_app_category_id"
  add_index "app_categories_apps", ["app_category_id"], :name => "index_app_categories_apps_on_app_category_id"

  create_table "app_versions", :force => true do |t|
    t.integer  "app_id",        :limit => 11,  :null => false
    t.text     "release_notes"
    t.text     "file_jad"
    t.text     "file_jar"
    t.text     "number",        :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "app_versions", ["app_id"], :name => "app_id"

  create_table "app_versions_customers", :id => false, :force => true do |t|
    t.integer "app_version_id", :limit => 11, :null => false
    t.integer "customer_id",    :limit => 11, :null => false
  end

  add_index "app_versions_customers", ["app_version_id", "customer_id"], :name => "index_app_versions_customers_on_app_version_id_and_customer_id"
  add_index "app_versions_customers", ["customer_id"], :name => "index_app_versions_customers_on_customer_id"

  create_table "app_versions_families", :id => false, :force => true do |t|
    t.integer "app_version_id", :limit => 11, :null => false
    t.integer "family_id",      :limit => 11, :null => false
  end

  add_index "app_versions_families", ["app_version_id", "family_id"], :name => "index_app_versions_families_on_app_version_id_and_family_id"
  add_index "app_versions_families", ["family_id"], :name => "index_app_versions_families_on_family_id"

  create_table "app_versions_submissions", :id => false, :force => true do |t|
    t.integer "app_version_id", :limit => 11, :null => false
    t.integer "submission_id",  :limit => 11, :null => false
  end

  add_index "app_versions_submissions", ["app_version_id", "submission_id"], :name => "ver_subm_id"
  add_index "app_versions_submissions", ["submission_id"], :name => "index_app_versions_submissions_on_submission_id"

  create_table "apps", :force => true do |t|
    t.text     "name",               :limit => 255, :null => false
    t.text     "name_short",         :limit => 255, :null => false
    t.text     "description_short"
    t.text     "description_medium"
    t.text     "description_long"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "apps_partners", :force => true do |t|
    t.integer "app_id",     :limit => 11, :null => false
    t.integer "partner_id", :limit => 11, :null => false
  end

  add_index "apps_partners", ["partner_id", "app_id"], :name => "index_apps_partners_on_partner_id_and_app_id"
  add_index "apps_partners", ["app_id"], :name => "index_apps_partners_on_app_id"

  create_table "bands", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.integer  "frequency",  :limit => 11,  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bands_devices", :id => false, :force => true do |t|
    t.integer "device_id", :limit => 11, :null => false
    t.integer "band_id",   :limit => 11, :null => false
  end

  add_index "bands_devices", ["device_id", "band_id"], :name => "index_bands_devices_on_device_id_and_band_id"
  add_index "bands_devices", ["band_id"], :name => "index_bands_devices_on_band_id"

  create_table "bands_operators", :id => false, :force => true do |t|
    t.integer "operator_id", :limit => 11, :null => false
    t.integer "band_id",     :limit => 11, :null => false
  end

  add_index "bands_operators", ["operator_id"], :name => "operator_id"
  add_index "bands_operators", ["band_id"], :name => "band_id"

  create_table "cldc_versions", :force => true do |t|
    t.text     "version",     :limit => 255, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", :force => true do |t|
    t.text "name",       :limit => 255, :null => false
    t.text "iso_name_2", :limit => 255, :null => false
    t.text "iso_name_3", :limit => 255, :null => false
    t.text "iso_number", :limit => 255, :null => false
  end

  create_table "customers", :force => true do |t|
    t.text     "nickname",        :limit => 255, :null => false
    t.text     "first_name",      :limit => 255
    t.text     "last_name",       :limit => 255
    t.text     "email",           :limit => 255
    t.text     "phone_number",    :limit => 255
    t.date     "birthday"
    t.string   "genre",           :limit => 1
    t.string   "hashed_password"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers_devices", :id => false, :force => true do |t|
    t.integer "device_id",   :limit => 11, :null => false
    t.integer "customer_id", :limit => 11, :null => false
  end

  add_index "customers_devices", ["device_id", "customer_id"], :name => "index_customers_devices_on_device_id_and_customer_id"
  add_index "customers_devices", ["customer_id"], :name => "index_customers_devices_on_customer_id"

  create_table "data_services", :force => true do |t|
    t.text     "name",       :limit => 255
    t.integer  "max_speed",  :limit => 11
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "data_services_devices", :id => false, :force => true do |t|
    t.integer "device_id",       :limit => 11, :null => false
    t.integer "data_service_id", :limit => 11, :null => false
  end

  add_index "data_services_devices", ["device_id", "data_service_id"], :name => "index_data_services_devices_on_device_id_and_data_service_id"
  add_index "data_services_devices", ["data_service_id"], :name => "index_data_services_devices_on_data_service_id"

  create_table "device_bugs", :force => true do |t|
    t.text     "name",        :limit => 255, :null => false
    t.text     "description",                :null => false
    t.text     "workaround"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "device_integrators", :force => true do |t|
    t.integer  "device_id",     :limit => 11,  :null => false
    t.integer  "integrator_id", :limit => 11,  :null => false
    t.text     "identifier",    :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "device_integrators", ["device_id", "integrator_id"], :name => "index_device_integrators_on_device_id_and_integrator_id"
  add_index "device_integrators", ["integrator_id"], :name => "index_device_integrators_on_integrator_id"

  create_table "devices", :force => true do |t|
    t.integer  "midp_version_id",       :limit => 11
    t.integer  "cldc_version_id",       :limit => 11
    t.integer  "vendor_id",             :limit => 11,  :null => false
    t.integer  "family_id",             :limit => 11
    t.text     "commercial_name",       :limit => 255
    t.text     "model",                 :limit => 255, :null => false
    t.integer  "screen_width",          :limit => 2
    t.integer  "screen_height_partial", :limit => 2
    t.integer  "screen_height_full",    :limit => 2
    t.integer  "jar_size_max",          :limit => 2
    t.integer  "memory_heap",           :limit => 4
    t.integer  "memory_image",          :limit => 4
    t.text     "user_agent",            :limit => 255
    t.boolean  "pointer_events"
    t.integer  "rms_total_size",        :limit => 4
    t.integer  "rms_max_record_size",   :limit => 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "devices", ["vendor_id"], :name => "index_devices_on_vendor_id"
  add_index "devices", ["midp_version_id"], :name => "index_devices_on_midp_version_id"
  add_index "devices", ["cldc_version_id"], :name => "index_devices_on_cldc_version_id"
  add_index "devices", ["family_id"], :name => "index_devices_on_family_id"

  create_table "devices_device_bugs", :id => false, :force => true do |t|
    t.integer "device_id",     :limit => 11, :null => false
    t.integer "device_bug_id", :limit => 11, :null => false
  end

  add_index "devices_device_bugs", ["device_id", "device_bug_id"], :name => "index_devices_device_bugs_on_device_id_and_device_bug_id"
  add_index "devices_device_bugs", ["device_bug_id"], :name => "index_devices_device_bugs_on_device_bug_id"

  create_table "devices_optional_apis", :id => false, :force => true do |t|
    t.integer "device_id",       :limit => 11, :null => false
    t.integer "optional_api_id", :limit => 11, :null => false
  end

  add_index "devices_optional_apis", ["device_id", "optional_api_id"], :name => "index_devices_optional_apis_on_device_id_and_optional_api_id"
  add_index "devices_optional_apis", ["optional_api_id"], :name => "index_devices_optional_apis_on_optional_api_id"

  create_table "devices_submissions", :id => false, :force => true do |t|
    t.integer "device_id",     :limit => 11, :null => false
    t.integer "submission_id", :limit => 11, :null => false
  end

  add_index "devices_submissions", ["device_id", "submission_id"], :name => "index_devices_submissions_on_device_id_and_submission_id"
  add_index "devices_submissions", ["submission_id"], :name => "index_devices_submissions_on_submission_id"

  create_table "download_codes", :force => true do |t|
    t.text     "code",        :limit => 255, :null => false
    t.text     "file",                       :null => false
    t.datetime "used_at"
    t.integer  "download_id", :limit => 11,  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "download_codes", ["download_id"], :name => "index_download_codes_on_download_id"

  create_table "downloads", :force => true do |t|
    t.text     "user_agent",                    :null => false
    t.text     "phone_number",   :limit => 255
    t.integer  "customer_id",    :limit => 11
    t.integer  "app_version_id", :limit => 11,  :null => false
    t.integer  "device_id",      :limit => 11
    t.text     "ip_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "downloads", ["app_version_id"], :name => "app_version_id"
  add_index "downloads", ["customer_id", "app_version_id", "device_id"], :name => "index_downloads_on_customer_id_and_app_version_id_and_device_id"
  add_index "downloads", ["customer_id", "app_version_id"], :name => "index_downloads_on_customer_id_and_app_version_id"
  add_index "downloads", ["device_id"], :name => "index_downloads_on_device_id"

  create_table "families", :force => true do |t|
    t.text     "name",        :limit => 255,                    :null => false
    t.boolean  "ignored",                    :default => false, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "integrators", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "integrators_operators", :id => false, :force => true do |t|
    t.integer "integrator_id", :limit => 11, :null => false
    t.integer "operator_id",   :limit => 11, :null => false
  end

  add_index "integrators_operators", ["integrator_id", "operator_id"], :name => "index_integrators_operators_on_integrator_id_and_operator_id"
  add_index "integrators_operators", ["operator_id"], :name => "index_integrators_operators_on_operator_id"

  create_table "midlet_reports", :force => true do |t|
    t.integer  "status_code",  :limit => 2
    t.text     "user_agent",                  :null => false
    t.text     "phone_number", :limit => 255
    t.integer  "download_id",  :limit => 11
    t.integer  "app_id",       :limit => 11
    t.text     "ip_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "midlet_reports", ["download_id", "app_id"], :name => "index_midlet_reports_on_download_id_and_app_id"
  add_index "midlet_reports", ["app_id"], :name => "index_midlet_reports_on_app_id"

  create_table "midp_versions", :force => true do |t|
    t.text     "version",     :limit => 255, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "operators", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "optional_apis", :force => true do |t|
    t.text     "name",        :limit => 255, :null => false
    t.text     "description"
    t.integer  "jsr_index",   :limit => 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "optional_apis", ["jsr_index"], :name => "index_optional_apis_on_jsr_index"

  create_table "partners", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ranking_entries", :force => true do |t|
    t.integer  "customer_id",    :limit => 11,                :null => false
    t.integer  "app_version_id", :limit => 11,                :null => false
    t.integer  "device_id",      :limit => 11
    t.integer  "score",          :limit => 8,                 :null => false
    t.integer  "sub_type",       :limit => 1,  :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ranking_entries", ["customer_id", "app_version_id", "device_id"], :name => "by_customer_id_appver_id_and_dev_id"
  add_index "ranking_entries", ["customer_id", "app_version_id"], :name => "index_ranking_entries_on_customer_id_and_app_version_id"
  add_index "ranking_entries", ["app_version_id"], :name => "index_ranking_entries_on_app_version_id"
  add_index "ranking_entries", ["device_id"], :name => "index_ranking_entries_on_device_id"
  add_index "ranking_entries", ["score"], :name => "index_ranking_entries_on_score"
  add_index "ranking_entries", ["sub_type"], :name => "index_ranking_entries_on_sub_type"

  create_table "submissions", :force => true do |t|
    t.integer  "integrator_id", :limit => 11, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "submissions", ["integrator_id"], :name => "index_submissions_on_integrator_id"

  create_table "undetected_user_agents", :force => true do |t|
    t.text     "user_agent",                             :null => false
    t.integer  "accesses",   :limit => 3, :default => 1, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_roles", :force => true do |t|
    t.text     "name",       :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "hashed_password"
    t.string   "salt"
    t.integer  "partner_id",      :limit => 11
    t.integer  "user_role_id",    :limit => 11, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["partner_id"], :name => "partner_id"
  add_index "users", ["user_role_id"], :name => "user_role_id"

  create_table "vendors", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "app_categories_apps", ["app_id"], "apps", ["id"], :name => "app_categories_apps_ibfk_1"
  add_foreign_key "app_categories_apps", ["app_category_id"], "app_categories", ["id"], :name => "app_categories_apps_ibfk_2"

  add_foreign_key "app_versions", ["app_id"], "apps", ["id"], :name => "app_versions_ibfk_1"

  add_foreign_key "app_versions_customers", ["app_version_id"], "app_versions", ["id"], :name => "app_versions_customers_ibfk_1"
  add_foreign_key "app_versions_customers", ["customer_id"], "customers", ["id"], :name => "app_versions_customers_ibfk_2"

  add_foreign_key "app_versions_families", ["app_version_id"], "app_versions", ["id"], :name => "app_versions_families_ibfk_1"
  add_foreign_key "app_versions_families", ["family_id"], "families", ["id"], :name => "app_versions_families_ibfk_2"

  add_foreign_key "app_versions_submissions", ["app_version_id"], "app_versions", ["id"], :name => "app_versions_submissions_ibfk_1"
  add_foreign_key "app_versions_submissions", ["submission_id"], "submissions", ["id"], :name => "app_versions_submissions_ibfk_2"

  add_foreign_key "apps_partners", ["app_id"], "apps", ["id"], :name => "apps_partners_ibfk_1"
  add_foreign_key "apps_partners", ["partner_id"], "partners", ["id"], :name => "apps_partners_ibfk_2"

  add_foreign_key "bands_devices", ["device_id"], "devices", ["id"], :name => "bands_devices_ibfk_1"
  add_foreign_key "bands_devices", ["band_id"], "bands", ["id"], :name => "bands_devices_ibfk_2"

  add_foreign_key "bands_operators", ["operator_id"], "operators", ["id"], :name => "bands_operators_ibfk_1"
  add_foreign_key "bands_operators", ["band_id"], "bands", ["id"], :name => "bands_operators_ibfk_2"

  add_foreign_key "customers_devices", ["device_id"], "devices", ["id"], :name => "customers_devices_ibfk_1"
  add_foreign_key "customers_devices", ["customer_id"], "customers", ["id"], :name => "customers_devices_ibfk_2"

  add_foreign_key "data_services_devices", ["device_id"], "devices", ["id"], :name => "data_services_devices_ibfk_1"
  add_foreign_key "data_services_devices", ["data_service_id"], "data_services", ["id"], :name => "data_services_devices_ibfk_2"

  add_foreign_key "device_integrators", ["device_id"], "devices", ["id"], :name => "device_integrators_ibfk_1"
  add_foreign_key "device_integrators", ["integrator_id"], "integrators", ["id"], :name => "device_integrators_ibfk_2"

  add_foreign_key "devices", ["midp_version_id"], "midp_versions", ["id"], :name => "devices_ibfk_1"
  add_foreign_key "devices", ["cldc_version_id"], "cldc_versions", ["id"], :name => "devices_ibfk_2"
  add_foreign_key "devices", ["vendor_id"], "vendors", ["id"], :name => "devices_ibfk_3"
  add_foreign_key "devices", ["family_id"], "families", ["id"], :name => "devices_ibfk_4"

  add_foreign_key "devices_device_bugs", ["device_id"], "devices", ["id"], :name => "devices_device_bugs_ibfk_1"
  add_foreign_key "devices_device_bugs", ["device_bug_id"], "device_bugs", ["id"], :name => "devices_device_bugs_ibfk_2"

  add_foreign_key "devices_optional_apis", ["device_id"], "devices", ["id"], :name => "devices_optional_apis_ibfk_1"
  add_foreign_key "devices_optional_apis", ["optional_api_id"], "optional_apis", ["id"], :name => "devices_optional_apis_ibfk_2"

  add_foreign_key "devices_submissions", ["device_id"], "devices", ["id"], :name => "devices_submissions_ibfk_1"
  add_foreign_key "devices_submissions", ["submission_id"], "submissions", ["id"], :name => "devices_submissions_ibfk_2"

  add_foreign_key "download_codes", ["download_id"], "downloads", ["id"], :name => "download_codes_ibfk_1"

  add_foreign_key "downloads", ["customer_id"], "customers", ["id"], :name => "downloads_ibfk_1"
  add_foreign_key "downloads", ["app_version_id"], "app_versions", ["id"], :name => "downloads_ibfk_2"
  add_foreign_key "downloads", ["device_id"], "devices", ["id"], :name => "downloads_ibfk_3"

  add_foreign_key "integrators_operators", ["integrator_id"], "integrators", ["id"], :name => "integrators_operators_ibfk_1"
  add_foreign_key "integrators_operators", ["operator_id"], "operators", ["id"], :name => "integrators_operators_ibfk_2"

  add_foreign_key "midlet_reports", ["download_id"], "downloads", ["id"], :name => "midlet_reports_ibfk_1"
  add_foreign_key "midlet_reports", ["app_id"], "apps", ["id"], :name => "midlet_reports_ibfk_2"

  add_foreign_key "ranking_entries", ["customer_id"], "customers", ["id"], :name => "ranking_entries_ibfk_1"
  add_foreign_key "ranking_entries", ["app_version_id"], "app_versions", ["id"], :name => "ranking_entries_ibfk_2"
  add_foreign_key "ranking_entries", ["device_id"], "devices", ["id"], :name => "ranking_entries_ibfk_3"

  add_foreign_key "submissions", ["integrator_id"], "integrators", ["id"], :name => "submissions_ibfk_1"

  add_foreign_key "users", ["partner_id"], "partners", ["id"], :name => "users_ibfk_1"
  add_foreign_key "users", ["user_role_id"], "user_roles", ["id"], :name => "users_ibfk_2"

end
