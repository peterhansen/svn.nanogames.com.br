class CreateDataServices < ActiveRecord::Migration
  def self.up
    create_table :data_services do |t|
      t.column :name, :text, :limit => 20
      t.column :max_speed, :integer 
      
      t.timestamps
    end

    #cria as tabelas de relacionamento N-N
    create_table :data_services_devices, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :data_service_id, :integer, :null => false
    end

    add_index :data_services_devices, [ :device_id, :data_service_id ]
    add_index :data_services_devices, :data_service_id
  end

  def self.down
    remove_index :data_services_devices, :data_service_id    
    remove_index :data_services_devices, [ :device_id, :data_service_id ]
        
    drop_table :data_services_devices
    drop_table :data_services
  end
end
