class CreateCustomers < ActiveRecord::Migration
  def self.up

    #cria a tabela de pa�ses
    create_table :countries do |t|
      t.column :name, :text, :limit => 40, :null => false
      t.column :iso_name_2, :text, :limit => 2, :null => false
      t.column :iso_name_3, :text, :limit => 3, :null => false
      t.column :iso_number, :text, :limit => 3, :null => false
    end

  end

  
  def self.down
    drop_table :countries
  end
end
