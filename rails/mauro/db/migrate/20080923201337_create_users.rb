class CreateUsers < ActiveRecord::Migration
  def self.up
    # parceiros 
    create_table :partners do |t|
      t.column :name, :text, :limit => 30, :null => false
      t.timestamps
    end

    # relacionamento parceiros x aplicativos
    create_table :apps_partners, :id => false do |t|
      t.column :app_id, :integer, :null => false
      t.column :partner_id, :integer, :null => false
    end


    # tabela de tipos de usu�rios
    create_table :user_roles do |t|
      t.column :name, :text, :limit => 30

      t.timestamps
    end

    # tabela de usu�rios em si
    create_table :users do |t|
      t.column :name, :string
      t.column :hashed_password, :string
      t.column :salt, :string
      t.column :partner_id, :integer
      t.column :user_role_id, :integer, :null => false

      t.timestamps
    end

    # �ndices
#    add_index :partners, [ :name ]
    add_index :apps_partners, [ :partner_id, :app_id ]
    add_index :apps_partners, [ :app_id ]
#    add_index :user_roles, [ :name ]
  end

  
  def self.down
#    remove_index :user_roles, [ :name ]
    remove_index :apps_partners, [ :app_id ]
    remove_index :apps_partners, [ :partner_id, :app_id ]
#    remove_index :partners, [ :name ]

    drop_table :users
    drop_table :user_roles
    drop_table :apps_partners
    drop_table :partners
  end
end
