class CreateUndetectedUserAgents < ActiveRecord::Migration
  def self.up
    create_table :undetected_user_agents do |t|
      t.column :user_agent, :text, :null => false
      t.column :accesses, :integer, :limit => 3, :default => 1, :null => false
      
      t.timestamps
    end
  end

  def self.down
    drop_table :undetected_user_agents
  end
end
