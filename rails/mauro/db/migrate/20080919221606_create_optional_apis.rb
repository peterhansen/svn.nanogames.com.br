class CreateOptionalApis < ActiveRecord::Migration
  def self.up
    create_table :optional_apis do |t|
      t.column :name,  :text, :limit => 30, :null => false
      t.column :description,   :text, :limit => 100
      t.column :jsr_index,   :integer, :limit => 2
      
      t.timestamps
    end
    
    add_index :optional_apis, :jsr_index
    
    create_table :devices_optional_apis, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :optional_api_id, :integer, :null => false
    end
    
    add_index :devices_optional_apis, [ :device_id, :optional_api_id ]
    add_index :devices_optional_apis, :optional_api_id
  end

  def self.down
    remove_index :devices_optional_apis, :optional_api_id
    remove_index :devices_optional_apis, [ :device_id, :optional_api_id ]    

    remove_index :optional_apis, :jsr_index
    
    drop_table :devices_optional_apis
    drop_table :optional_apis
  end
end
