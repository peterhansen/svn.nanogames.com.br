class CreateDeviceIntegrators < ActiveRecord::Migration
  def self.up
    create_table :device_integrators do |t|
      t.column :device_id, :integer, :null => false
      t.column :integrator_id, :integer, :null => false
      t.column :identifier, :text, :limit => 30
      t.timestamps
    end
    
    add_index :device_integrators, [ :device_id, :integrator_id ]
    add_index :device_integrators, [ :integrator_id ]
  end

  def self.down
    remove_index :device_integrators, [ :integrator_id ]
    remove_index :device_integrators, [ :device_id, :integrator_id ]
    
    drop_table :device_integrators
  end
end
