class CreateSubmissions < ActiveRecord::Migration
  def self.up
    create_table :submissions do |t|
      t.column :integrator_id, :integer, :null => false
      
      t.timestamps
    end
    add_index :submissions, [ :integrator_id ]    
    
    #cria as tabelas de relacionamento NxN
    create_table :devices_submissions, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :submission_id, :integer, :null => false
    end     
    add_index :devices_submissions, [ :device_id, :submission_id ]
    add_index :devices_submissions, :submission_id      
    
    
    create_table :app_versions_submissions, :id => false do |t|
      t.column :app_version_id, :integer, :null => false
      t.column :submission_id, :integer, :null => false
    end
    # o nome do pr�ximo �ndice � necess�rio para n�o exceder o limite do nome de um �ndice
    add_index :app_versions_submissions, [ :app_version_id, :submission_id ], :name => 'ver_subm_id'
    add_index :app_versions_submissions, :submission_id
    
  end

  def self.down
    remove_index :app_versions_submissions, [ :app_version_id, :submission_id ]
    remove_index :app_versions_submissions, :submission_id      
    remove_index :devices_submissions, [ :device_id, :submission_id ]
    remove_index :devices_submissions, :submission_id      
    remove_index :submissions, [ :integrator_id ]    
    
    drop_table :app_versions_submissions
    drop_table :devices_submissions
    drop_table :submissions
  end
end
