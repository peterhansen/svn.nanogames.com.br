class CreateAppVersions < ActiveRecord::Migration
  def self.up
    create_table :app_versions do |t|
      t.column :app_id,  :integer, :null => false
      t.column :release_notes,  :text, :limit => 100
      t.column :file_jad, :text
      t.column :file_jar, :text
      t.column :number,  :text, :limit => 10, :null => false
      
      t.timestamps
    end
    
  
    #cria as tabelas de relacionamento NxN
    create_table :app_versions_families, :id => false do |t|
      t.column :app_version_id, :integer, :null => false
      t.column :family_id, :integer, :null => false
    end     
    add_index :app_versions_families, [ :app_version_id, :family_id ]
    add_index :app_versions_families, :family_id           
    
  end

  def self.down
    remove_index :app_versions_families, [ :app_version_id, :family_id ]
    remove_index :app_versions_families, :family_id           
    
    drop_table :app_versions_families
    drop_table :app_versions
  end
end
