class CreateIntegrators < ActiveRecord::Migration
  def self.up
    create_table :integrators do |t|
      t.column :name, :text, :limit => 20, :null => false
      t.timestamps
    end
    
    #cria as tabelas de relacionamento N-N
    create_table :integrators_operators, :id => false do |t|
      t.column :integrator_id, :integer, :null => false
      t.column :operator_id, :integer, :null => false
    end
    
    add_index :integrators_operators, [ :integrator_id, :operator_id ]
    add_index :integrators_operators, [ :operator_id ]
  end

  def self.down
    remove_index :integrators_operators, [ :integrator_id, :operator_id ]
    remove_index :integrators_operators, [ :operator_id ]
    
    drop_table :integrators
  end
end
