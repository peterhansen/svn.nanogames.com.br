class CreateAppCategories < ActiveRecord::Migration
  def self.up
    create_table :app_categories do |t|
      t.column :name,  :text, :limit => 20, :null => false
      
      t.timestamps
    end
    
    create_table :app_categories_apps, :id => false do |t|
      t.column :app_id, :integer, :null => false
      t.column :app_category_id, :integer, :null => false
    end 
    
    add_index :app_categories_apps, [ :app_id, :app_category_id ]
    add_index :app_categories_apps, :app_category_id    
  end

  def self.down
    remove_index :app_categories_apps, :app_category_id
    remove_index :app_categories_apps, [ :app_id, :app_category_id ]
    
    drop_table :app_categories_apps
    drop_table :app_categories
  end
end
