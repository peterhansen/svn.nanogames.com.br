class Country < ActiveRecord::Base
  validates_length_of :name, :maximum => 40
  validates_length_of :iso_name_2, :is => 2
  validates_length_of :iso_name_3, :is => 3
  validates_length_of :iso_number, :in => 1..3
  
  validates_uniqueness_of :name
  validates_uniqueness_of :iso_name_2
  validates_uniqueness_of :iso_name_3
  validates_uniqueness_of :iso_number
  
end
