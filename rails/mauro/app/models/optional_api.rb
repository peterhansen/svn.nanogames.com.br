class OptionalApi < ActiveRecord::Base
  has_and_belongs_to_many :devices
  
  validates_presence_of :name
  validates_length_of :name, :in => 5..30
  validates_length_of :description, :in => 5..100, :allow_nil => true
  validates_numericality_of :jsr_index, :allow_nil => true
  
end
