class Band < ActiveRecord::Base
  has_and_belongs_to_many :devices
  has_and_belongs_to_many :operators
  
  validates_presence_of :name, :frequency
  validates_numericality_of :frequency
end
