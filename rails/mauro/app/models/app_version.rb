require 'find'

class AppVersion < ActiveRecord::Base
  belongs_to :app
  has_and_belongs_to_many :families
  has_and_belongs_to_many :submissions
 # has_and_belongs_to_many :customers
 # has_many :ranking_entries
 # has_many :downloads
  
  has_many :integrators, :through => :submissions
  
  upload_column :file_jad, 
                :store_dir => proc{|record| "#{record.app_version_number_clean}/"}

  upload_column :file_jar, 
                :store_dir => proc{|record| "#{record.app_version_number_clean}/"}
  
  validates_presence_of :app_id
  validates_presence_of :number
  validates_length_of :number, :maximum => 10

  # valida o formato da vers�o (xx.yy ou xx.yy.zz)
  validates_format_of :number, :with => /\A\d{1,2}\.\d{1,2}\.\d{1,2}\z/, :message => "must be in format 'xx.yy.zz'"
  
  validates_length_of :release_notes, :maximum => 100, :allow_nil => true 

  # TODO jun��es auto-referenciais para vers�es que substituem uma outra vers�o (ou s�o substitu�das)
  
  
  def devices()
    devices = []
    
    families.each() do |family|
      devices << family.devices
    end
    
    return devices
  end  
  
  def app_version_number
    app.name + ' ' + number
  end
  
  def app_version_number_clean
    app_version_number().gsub( ' ', '_' )
  end
  

  #apaga todos os arquivos do diret�rio que n�o sejam os arquivos .jad ou .jar atuais
  def erase_old_files()
    dir = ''
    path_jad = ''
    path_jar = ''
    
    if ( file_jad )
      dir = file_jad.dir
      path_jad = file_jad.path
    end
    
    if ( file_jar )
      dir = file_jar.dir
      path_jar = file_jar.path
    else
      return unless ( file_jad )
    end
    
    Find.find( dir ) do |path|
      unless ( FileTest.directory?( path ) )
        if ( !path.eql?( path_jad ) && !path.eql?( path_jar ) )
          begin
            File.delete( path )
          rescue Exception => e
          end
        end
      end
    end
  end
  
end
