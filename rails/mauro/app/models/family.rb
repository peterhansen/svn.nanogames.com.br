class Family < ActiveRecord::Base
  has_many :devices, :dependent => :nullify
  has_and_belongs_to_many :app_versions
  
  validates_presence_of :name
  validates_uniqueness_of :name
  validates_length_of :name, :in => 5..30
  validates_length_of :description, :maximum => 100, :allow_nil => true
end
