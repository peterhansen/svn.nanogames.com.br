class App < ActiveRecord::Base
  has_and_belongs_to_many :app_categories
  has_many :app_versions
#  has_many :ranking_entries, :through => :app_versions # TODO necessário?
#  has_many :downloads, :through => :app_versions
  
  validates_presence_of :name, :name_short
  
  validates_uniqueness_of :name, :name_short
  
  validates_length_of :name, :in => 3..50
  validates_length_of :name_short, :is => 4
  validates_length_of :description_short, :maximum => 150, :allow_nil => true
  validates_length_of :description_medium, :maximum => 300, :allow_nil => true
  validates_length_of :description_long, :maximum => 900, :allow_nil => true

  def name_clean()
    return name.gsub( ' ', '_' )
  end

end
