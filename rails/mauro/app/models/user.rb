require 'digest/sha1'

class User < ActiveRecord::Base
  belongs_to :user_role
  
  validates_presence_of    :name
  validates_uniqueness_of  :name
  validates_presence_of    :user_role

  attr_accessor :password_confirmation 
  validates_confirmation_of :password


  def validate
    errors.add_to_base( 'Missing password' ) if hashed_password.blank?
  end


  def password
    @password
  end

  
  def password=(pwd)
    @password = pwd
    create_new_salt
    self.hashed_password = User.encrypted_password( self.password, self.salt )
  end


  def self.authenticate( name, password, key )
    user = self.find_by_name( name )
    
    if ( user )
      expected_password = encrypted_password( password, user.salt )
      if ( user.hashed_password != expected_password )
      #      if ( user.hashed_password != expected_password || !validate_key( key ) )
        user = nil
      end
    end
    
    return user
  end
  
  
  def after_destroy
    if ( User.count.zero? )
      raise "Can't delete: last user."
    end
  end
  
  
  private
  
  def self.encrypted_password( password, salt )
    string_to_hash = password + 'besteira aleat�ria' + salt #dificulta a adivinha��o da senha
    Digest::SHA1.hexdigest( string_to_hash )
  end


  def self.validate_key( key )
    if ( !key.nil? )
      t = Time.now().utc()
      return Digest::SHA1.hexdigest( "P#{t.hour.to_s}E*'#{t.day.to_s}T09id_\&saj#{t.year.to_s}E$�V#{t.month.to_s}R__1\@V#{t.min.to_s}H=\##{t.yday.to_s}" ) == key
    end

    return false
  end

  
  def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
  end
  
end
