class MidpVersion < ActiveRecord::Base
  has_many :devices, :dependent => :nullify
  
  validates_uniqueness_of :version
end
