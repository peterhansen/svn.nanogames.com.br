# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

URL_NANO_ONLINE = 'http://online.nanogames.com.br'

# caminho dos arquivos privados
PATH_FILES = "#{RAILS_ROOT}/files"

PATH_SUBMISSIONS = "#{PATH_FILES}/submissions"

PATH_RELEASES = "#{PATH_FILES}/releases"

# Id de jogo/aplicativo: dummy (usado para testes e debug) 
ID_APP_DUMMY                            = "DMMY"

# Id de chave global: aplicativo/jogo. Tipo do valor: string 
ID_APP					= -128

# Id de chave global: vers�o do aplicativo/jogo. Tipo do valor: string 
ID_APP_VERSION			= ID_APP + 1

# Id de chave global: idioma atual do aplicativo. Tipo do valor: byte 
ID_LANGUAGE			= ID_APP_VERSION + 1

# Id de chave global: vers�o do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string 
ID_NANO_ONLINE_VERSION	= ID_LANGUAGE + 1

# Id de chave global: c�digo de retorno. Tipo do valor: short 
ID_RETURN_CODE			= ID_NANO_ONLINE_VERSION + 1

# Id de chave global: usu�rio. Tipo do valor: int 
ID_CUSTOMER_ID				= ID_RETURN_CODE + 1

# Id de chave global: mensagem de erro. Tipo do valor: string 
ID_ERROR_MESSAGE		= ID_CUSTOMER_ID + 1

#Id de chave global: in�cio de dados espec�ficos. Tipo do valor: vari�vel
ID_SPECIFIC_DATA		= ID_ERROR_MESSAGE + 1


# assim como as chaves, os valores de retorno globais possuem valores negativos para que n�o haja confus�o com os
# valores utilizados internamente em cada servi�o. A chave 0 (zero) � considerada OK (RC_OK)

# Valor de retorno global: sem erros. 
RC_OK			  = 0

# Valor de retorno global: erro interno do servidor. 
RC_SERVER_INTERNAL_ERROR  = RC_OK - 1

# Valor de retorno global: aparelho n�o suportado/detectado. 
RC_DEVICE_NOT_SUPPORTED   = RC_SERVER_INTERNAL_ERROR - 1

# Valor de retorno global: aplicativo n�o encontrado (id de aplicativo inv�lido). 
RC_APP_NOT_FOUND	  = RC_DEVICE_NOT_SUPPORTED - 1


@@default_content_types = {
  'jad' => 'text/vnd.sun.j2me.app-descriptor',
  'jar' => 'application/java-archive',
  'cod' => 'application/vnd.rim.cod',
  'sis' => 'application/vnd.symbian.install',
  'sisx' => 'x-epoc/x-sisx-app',
}


  ##
  # Tenta detectar o aparelho do usu�rio. Caso seja detectado com sucesso, adiciona
  # a chave :device ao hash params, para que o aparelho de origem esteja acess�vel
  # para os pr�ximos m�todos. Se essa chave j� estiver definida nos par�metros, 
  # a verifica��o s� � feita caso <code>force_check</code> seja true.
  ##
  def get_device( force_check = false )
    logger.info( "get_device( #{force_check} )")
    if ( force_check || !params[ :device ] ) 
      # tenta detectar o aparelho do usu�rio (caso n�o seja encontrado, j� armazena seu user-agent no banco de dados)
      @device, matches = Device.parse_user_agent( request.user_agent )
      logger.info( "\tdevice found: #{ @device == nil ? "NONE -> #{request.user_agent}" : @device }" )
      
      params[ :device_vendor ] = matches[ :VENDOR ]

      # tenta extrair o n�mero de telefone do usu�rio a partir do HTTP request
      @phone_number = request.env[ 'HTTP_MSISDN' ] ||
                      request.env[ 'MSISDN' ] ||
                      request.env[ 'HTTP_X_MSP_WAP_CLIENT_ID' ] ||
                      request.env[ 'HTTP_X_HTS_CLID' ] ||
                      request.env[ 'HTTP_X_FH_MSISDN' ] ||
                      request.env[ 'HTTP_X_MSISDN' ] ||
                      request.env[ 'HTTP_X_NOKIA_MSISDN' ] ||
                      request.env[ 'HTTP_X_UP_CALLING_LINE_ID' ] ||
                      request.env[ 'HTTP_X_WAP_NETWORK_CLIENT_MSISDN' ] ||
                      request.env[ 'HTTP_X_NETWORK_INFO' ]

      logger.info( "phone number: #{@phone_number}" )

      # se detectou o aparelho ou pelo menos o fabricante, tenta ler os par�metros
      if ( matches )
        # os par�metros s�o obtidos a partir da leitura do corpo do request
        if ( request.post? || request.put? )
          read_binary_params()
        end

        unless ( @phone_number )
          logger.info( "phone number not detected. request parameters: ")
          temp = ""
          for item in request.env
            temp << "#{item[ 0 ]}\t"
          end
          logger.info( temp )
        end
      end
      
      params[ :phone_number ] = @phone_number
      params[ :device ] = @device
    end
    logger.info( "get_device( #{force_check} ) FIM: #{params[ :device ] }")

    return params[ :device ]
  end  
  
  
  ##
  # Varre uma string (array de bytes) representando uma sequ�ncia de dados no formato
  # key (1 byte) value (tamanho vari�vel). Este m�todo cria um bloco, com 2 chaves:
  # |stream, id|. A stream � a refer�ncia para o array em si, e o id � um byte
  # identificador da chave. A leitura dos dados � feita utilizando-se os m�todos
  # da classe JavaTypes::JavaStream. Caso o m�todo chamador n�o queira tratar um 
  # determinado par key/value, deve pular os bytes correspondentes no array, para
  # que a leitura da pr�xima chave n�o seja inv�lida, o que acaba por invalidar
  # todo o restante da leitura do stream.
  # 
  # O m�todo encerra quando todos os bytes s�o lidos e/ou pulados.
  ##
  def parse_data( data )
    begin
#      stream = JavaTypes::JavaStream.new( request.body.string )
      stream = JavaTypes::InputStream.new( data )
#      params = Hash.new()
      
      # varre o corpo da mensagem, preenchendo a hashtable com os par�metros encontrados,
      # at� que todos os par�metros sejam lidos (EOF) ou que o id de dados espec�ficos
      # seja encontrado, quando a stream � ent�o passada ao tratador adequado.
      while ( stream.available() > 0 )
        id = stream.read_byte()
        yield stream, id
      end
      
    rescue JavaTypes::EOFException => e
    rescue JavaTypes::UTFDataFormatException => e
      logger.error( "UTFDataFormatException" )
    rescue => e
      logger.error( e.message )
    end
  end
  
  
  ##
  # Renderiza um conte�do bin�rio, no formato Java, para uma aplica��o Nano. Esse
  # m�todo retorna um bloco com uma refer�ncia para o OutputStream. S� � necess�rio
  # realizar as chamadas de grava��o no stream - a abertura e fechamento s�o feitas
  # dentro deste m�todo.
  ##
  def render_binary()
    begin
      out = JavaTypes::OutputStream.new()
      
      # permite que o m�todo chamador realize as opera��es de escrita no stream
      yield out
      
      logger.info( "->render_binary: #{out.flush.length} bytes: #{out.flush}")
      
      # retorna as informa��es gravadas pelo m�todo chamador
      render( :inline => out.flush() )
    end
  end
  
  
  ##
  # L� os par�metros bin�rios recebidos (armazenados em request.raw_post) e os adiciona
  # como chaves no hash params. A leitura � interrompida quando todo o request.body
  # � lido, ou ent�o quando o id ID_SPECIFIC_DATA � encontrado (e seu valor armazenado
  # em params[ ::ID_SPECIFIC_DATA ])
  ##
  def read_binary_params()
#    parse_data( request.body.string ) { |stream, id|
    # TODO adicionar header inicial para evitar tentativa de leitura de par�metros bin�rios
    # de clientes que n�o est�o acessando por uma aplica��o Nano (ex.: header inicial do PNG)
    parse_data( request.raw_post ) { |stream, id|
      case ( id )
        when ID_APP
          params[ id ] = stream.read_string()
          # tenta achar a aplica��o no BD
          params[ :app ] = App.find_by_name_short( params[ id ] )
          
          # se j� tiver lido a vers�o do aplicativo, armazena a vers�o
          if ( params[ ID_APP_VERSION ] )
            params[ :app_version ] = AppVersion.find_by_app_id_and_number( params[ :app ].id, params[ id ] )
          end          
        when ID_CUSTOMER_ID
          params[ id ] = stream.read_int()
          params[ :customer ] = Customer.find_by_id( params[ id ] )
        when ID_RETURN_CODE
          params[ id ] = stream.read_short()
        when ID_LANGUAGE
          params[ id ] = stream.read_byte()
        when ID_APP_VERSION
          params[ id ] = stream.read_string()
          
          # se j� tiver lido qual o aplicativo de origem, busca a vers�o
          if ( params[ :app ] )
            params[ :app_version ] = AppVersion.find_by_app_id_and_number( params[ :app ].id, params[ id ] )
          end
        when ID_NANO_ONLINE_VERSION
          params[ id ] = stream.read_string()
        when ID_ERROR_MESSAGE
          params[ id ] = stream.read_string()
        when ID_SPECIFIC_DATA
          # inicia a leitura dos dados espec�ficos
          params[ id ] = stream.read_available()
      end      
    }
    logger.info( "PARAMETROS LIDOS: ")
    params.each() { |key, value|
      logger.info "#{key} => #{value}"
    }    
  end
  
  
  ##
  #
  ##
  def extract_error_code( message )
    m = message.match( /\(\d+\)/ )
    if ( m && m.size > 0 )
      return m[ m.size - 1 ].match( /\d+/ )[ 0 ].to_i()
    end
    
    return 0
  end


  def default_content_type( extension )
    return @@default_content_types[ extension.delete( '.' ) ]
  end


  def default_page_layout
#    return ( get_device() ? 'wml' : 'simple' ) TODO teste
    return 'simple'
  end
  
  

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time

  before_filter :get_device

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  # TODO teste 
  # TODO protect_from_forgery #:secret => '050e52b0800808bc993ff8cf33dcd657'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  # filter_parameter_logging :password

  
  # TODO http://www.railsdev.ws/blog/10/using-subdomains-in-rails-apps/
#  @current_subdomain = (self.request.subdomains.join('.'))

  
  
  ##############################################################################
  # M�TODOS PROTEGIDOS                                                         #
  ##############################################################################
  protected

  
  ##############################################################################
  # M�TODOS PRIVADOS                                                           #
  ##############################################################################
  private

  
  ##
  #
  ##
  def authorize
    unless ( User.find_by_id( session[ :user_id ] ) )
      if ( params[ :device_vendor ] )
        # conex�o veio de um aparelho - verifica a origem
        if ( params[ :app ] )
          # conex�o veio de um aparelho n�o detectado, por�m rodando uma aplica��o
          # Nano - responde atrav�s do m�todo bin�rio
          render_binary() { |out|
            out.write_byte( ID_RETURN_CODE )
            out.write_short( RC_DEVICE_NOT_SUPPORTED )
            out.write_byte( ID_ERROR_MESSAGE )
            out.write_string( "aparelho n�o detectado. fabricante: #{params[ :device_vendor ]}" )
          }
        else
          # conex�o veio de um aparelho n�o detectado, atrav�s do navegador ou de
          # outro aplicativo - responde em html
          # TODO resposta HTML padr�o
          logger.warn( "retornou resposta vazia para cliente n�o detectado: #{params[ :device_vendor ]}" )
          redirect_to( 'http://www.nanogames.com.br' )
        end
      else
        logger.info( "Acesso n�o autorizado - user agent: #{request.user_agent}" )
#        session[ :original_uri ] = request.request_uri
#        flash[ :notice ] = 'Please log in'
#        redirect_to( :controller => 'login', :action => 'login' )
        redirect_to( 'http://www.nanogames.com.br' )
      end
    end
  end
end
