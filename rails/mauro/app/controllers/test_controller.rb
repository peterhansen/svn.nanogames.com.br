class TestController < ApplicationController
  
  # GET /undetected_user_agent
  # GET /undetected_user_agents.xml
  def detect
    @device = get_device( true )
    logger.info( "CONEXAO RECEBIDA" )
    headers = request.env.select {|k,v| k.match(".*") }
    for header in headers
      logger.info( "#{header}")
    end
#    @device = Device.parse_user_agent( request.user_agent )

    render :layout => default_page_layout
  end
  
end
