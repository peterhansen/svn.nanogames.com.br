class AppsController < ApplicationController

  before_filter :authorize
  
  layout 'admin'
  
  # GET /apps
  # GET /apps.xml
  def index
    @apps = App.find( :all,
                      :order => 'name',
                      :page => { :size => 30, :current => params[ :page ] } )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @apps }
    end
  end

  # GET /apps/1
  # GET /apps/1.xml
  def show
    begin
      @app = App.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid app id #{params[:id]}" )
      flash[:notice] = "Invalid app"
      redirect_to :action => :index
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @app }
      end
    end
    
  end

  # GET /apps/new
  # GET /apps/new.xml
  def new
    @app = App.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @app }
    end
  end

  # GET /apps/1/edit
  def edit
    @app = App.find(params[:id])
  end

  # POST /apps
  # POST /apps.xml
  def create
    @app = App.new(params[:app])

    respond_to do |format|
      if @app.save
        flash[:notice] = "App #{ @app.name } was successfully created."
        format.html { redirect_to(@app) }
        format.xml  { render :xml => @app, :status => :created, :location => @app }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @app.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /apps/1
  # PUT /apps/1.xml
  def update
    params[ :app ][ :app_category_ids ] ||= []
    @app = App.find(params[:id])

    respond_to do |format|
      if @app.update_attributes(params[:app])
        flash[:notice] = "App #{ @app.name } was successfully updated."
        format.html { redirect_to(@app) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @app.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /apps/1
  # DELETE /apps/1.xml
  def destroy
    @app = App.find(params[:id])
    @app.destroy

    respond_to do |format|
      flash[:notice] = "App #{ @app.name } was successfully destroyed."
      format.html { redirect_to(apps_url) }
      format.xml  { head :ok }
    end
  end
  
end
