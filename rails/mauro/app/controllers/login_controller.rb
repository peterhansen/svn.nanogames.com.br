class LoginController < ApplicationController
  layout 'admin', :except => :login
  
  before_filter :authorize, :except => :login

  
  def add_user
    @user = User.new( params[ :user ] )
    if ( request.post? and @user.save )
      flash.now[ :notice ] = "User #{ @user.name } created."
      @user = User.new
    end
  end


  def login
    session[ :user_id ] = nil
    if ( request.post? )
      user = User.authenticate( params[ :name ], params[ :password ], params[ :key ] )
      
      if ( user )
        session[ :user_id ] = user.id
        
        #redireciona o usu�rio � p�gina que tentou acessar antes
        uri = session[ :original_uri ]
        session[ :original_uri ] = nil
        
        redirect_to( uri || { :action => 'index' } )
      else
        flash[ :notice ] = 'Invalid user/password/key combination'
      end
    end
  end


  def logout
    session[ :user_id ] = nil
    flash[ :notice ] = 'Logged out.'
    redirect_to( :action => 'login' )
  end


  def index
  end


  def redirect_to_nano_games
    redirect_to( 'http://www.nanogames.com.br' )
  end

  
  def delete_user
    if ( request.post? )
      user = User.find( params[ :id ] )
      
      begin
        user.destroy
        flash[ :notice ] = "User #{ user.name } successfully deleted."
      rescue Exception => e
        flash[ :notice ] = e.message
      end
    end
    
    redirect_to( :action => :list_users )
  end

  
  def list_users
    @all_users = User.find( :all )
  end

end
