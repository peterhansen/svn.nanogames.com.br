class AppVersionsController < ApplicationController
  
  before_filter :authorize
  
  layout 'admin'
  
  # GET /app_versions
  # GET /app_versions.xml
  def index
    @app_versions = AppVersion.find( :all,
                                     :order => 'app_id, number', # TODO ordenar pelo nome da aplica��o
                                     :page => { :size => 30, :current => params[ :page ] } )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @app_versions }
    end
  end

  # GET /app_versions/1
  # GET /app_versions/1.xml
  def show
    begin
      @app_version = AppVersion.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid app_version id #{params[:id]}" )
      flash[:notice] = "Invalid app_version"
      redirect_to :action => :index
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @app_version }
      end
    end
    
  end

  # GET /app_versions/new
  # GET /app_versions/new.xml
  def new
    @app_version = AppVersion.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @app_version }
    end
  end

  # GET /app_versions/1/edit
  def edit
    @app_version = AppVersion.find(params[:id])
  end

  # POST /app_versions
  # POST /app_versions.xml
  def create
    @app_version = AppVersion.new(params[:app_version])
    app_versions = AppVersion.find_all_by_app_id( @app_version.app_id )

    biggest = [ 0, 0, 0 ]
    app_versions.each { |a|
      temp = a.number.split( "\." )

      for i in ( 0..2 ) do
        temp[ i ] = temp[ i ].to_i
      end

      for i in ( 0..2 ) do
        if ( temp[ i ].to_i > biggest[ i ] )
          biggest = temp
          break
        end
      end
    }

    case ( params[ :release_type ] )
      when "Major release"
        biggest[ 0 ] += 1
      when "Minor release"
        biggest[ 1 ] += 1
      when "Bug fix"
        biggest[ 2 ] += 1
    end
    
    @app_version.number = "#{ biggest[ 0 ] }.#{ biggest[ 1 ] }.#{ biggest[ 2 ] }"

    respond_to do |format|
      if @app_version.save
        flash[:notice] = "App version #{ @app_version.number } was successfully created."
        format.html { redirect_to(@app_version) }
        format.xml  { render :xml => @app_version, :status => :created, :location => @app_version }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @app_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /app_versions/1
  # PUT /app_versions/1.xml
  def update
    params[ :app_version ][ :family_ids ] ||= []
    @app_version = AppVersion.find(params[:id])

    respond_to do |format|
      if @app_version.update_attributes(params[:app_version])
        @app_version.erase_old_files()
        
        flash[:notice] = "App version #{ @app_version.number } was successfully updated."
        format.html { redirect_to(@app_version) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @app_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /app_versions/1
  # DELETE /app_versions/1.xml
  def destroy
    @app_version = AppVersion.find(params[:id])
    @app_version.destroy

    respond_to do |format|
      flash[:notice] = "App version #{ @app_version.number } was successfully destroyed."
      format.html { redirect_to(app_versions_url) }
      format.xml  { head :ok }
    end
  end
  
  
end
