jQuery(function() {
  var gameId = jQuery('#game_id').val();
  var userName = jQuery('#user_name').val();
  var color = jQuery('#color').val();
  var mainPane = jQuery('#main_panel');
  var scrollPane = jQuery('.jspPane');
  var api = mainPane.data('jsp');
  var charCount = 0;

  var faye = new Faye.Client('http://' + window.location.hostname + ':9292/faye', {
    timeout: 120
  });

  faye.subscribe('/games/489861889', function(data) {
    displayMessage(data);
    scrollToBottom();
  });

  jQuery('#input_message').focus(clearInputField);

  jQuery('#input_message').live('keyup', function(event) {
    if ((event.keyCode == 13) && (event.ctrlKey)) {
      jQuery('#chat_button').click();
      resetCharCounter();
    }

    if (charLimitReached()) {
      restrictContent();
      return false;
    }

    updateCharCounter();
  });

  function charLimitReached() {
    return charCount > 300;
  }

  function restrictContent() {
    jQuery('#input_message').val(jQuery('#input_message').val().substring(0, 300));
  }

  function resetCharCounter() {
    setCharCountTo(0);
  }

  function updateCharCounter() {
    setCharCountTo(jQuery('#input_message').val().length);
  }

  function setCharCountTo(value) {
    charCount = value;
    jQuery('#char_counter').text((Math.max(300 - value, 0)).toString());
  }

  jQuery('#chat_button').click(function() {
    if (inputFieldIsEmpty())
      return false;

    faye.publish('/games/489861889', {
      user_name:  userName,
      color:      color,
      text:       jQuery('#input_message').val()
    });

    clearInputField();
  });

  function displayMessage(data) {
    var text = "<p><strong class='" + data.color + "'>" + data.user_name + "</strong>" + data.text + "</p>";
    jQuery('.jspPane').append(text);
  }

  function scrollToBottom() {
    setTimeout(function() {
      api.scrollTo(0, scrollPane.height());
    }, 500);
  }

  function inputFieldIsEmpty() {
    return jQuery('#input_message').val() == '';
  }

  function clearInputField() {
    jQuery('#input_message').val('');
  }
});