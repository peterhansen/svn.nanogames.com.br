function newQuestion() {
    if (jQuery(".question:hidden").first().slideDown("slow").size() == 0) {
        alert("Não é possivel adicionar mais questões.");
    }
}

function cleanUp(element) {
    //zera o titulo
    element.children()[1] = '';
    //zera as opções
    for (var i = 0; i < 4; i++) {
        if (jQuery("#edit")) {
//            console.log(element.children()[2].children[1].children[0].children[i]);
        } else {
            element.children()[2].children[1].children[i].children[0].value = '';
        }
    }
}

function removeQuestion(id) {
    var element = jQuery("#" + id);
    var question_element;
    element.parent().parent().slideUp();
    cleanUp(element.parent());
    question_element = element.parent().parent();
    element.parent().parent().remove();
    jQuery("#question_parent").children().first().append(question_element);
}
