function maxLength() {
    var max_length = 165;
    if (jQuery("#notification_body").val().length > max_length) {
        jQuery("#notification_body").val(jQuery("#notification_body").val().substr(0,max_length));   
    }
    jQuery(".char_count").text(max_length - jQuery("#notification_body").val().length);
}


jQuery(".char_count").ready(function () {
    var max_length = 165;
    jQuery(".char_count").text(max_length - jQuery("#notification_body").val().length);
});
