var $timer = '';
var $timer_start = '';
var $calls = 0;
var $timeRemaining = 0;
var $timeout = 0;


function getStateUpdate(game_turn, game_id) {
  var state = "";
  new Ajax.Request("/games/state_update/?game_turn=" + game_turn + "&game_id=" + game_id, {
    method: 'get',
    asynchronous:false,
    onSuccess: function(data) {
      state = data.responseText.evalJSON();
    }
  });
  return state;
}

function replay(previous_game_id) {
  new Ajax.Request("/games/replay_game?previous_game_id=" + previous_game_id, {
    method: 'get',
    asynchronous:false,
    onSuccess: function (data) {
      window.location = '/games/' + data.responseText;
    }
  })
}

function forceExitRoom() {
  new Ajax.Request("/games/exit", {method: 'get'})
}

function forceExitGame(room_id) {
  window.location = '/rooms/' + room_id;
}

function setTurnHTML(new_turn) {
  jQuery(".indicador_jogadorAtivo").each(function(display, index) {
    if (display.getStyle("display") == "inline") {
      display.setStyle({display: "none"});
    }
  });

  if ($("turn_" + new_turn) != null) {
    $("turn_" + new_turn).setStyle({display: "inline"});
  }
}

function sendGameState(game_state) {
  state_of_game = game_state.evalJSON()
  if ($('flash') != null) {
    $('flash').newQuestion(state_of_game);
  }
}

function reportError(message) {
  new Ajax.Request("/error?what=" + message)
}

function requestUsersList() {
  var users_list;
  new Ajax.Request("/games/users_list", {
    asynchronous:false,
    method: 'get',
    onSuccess: function(data) {
      users_list = data.responseText.evalJSON();
    }
  });
  return users_list;
}

function setUserListHTML(users) {
  player_list = $('lista_jogadores')
  for (index in users) {
    player_list.append("<li class='" + users[index].color + "' id='jogador_" + users[index].id + "'>");
    player_list.append("<img src='" + users[index].thumb + "' alt='" + users[index].nickname + "' class='img_jogador'>");
    player_list.append("<div class='info_jogador'>");
    player_list.append("<h5>" + users[index].nickname + "</h5>");
    player_list.append("<p class='info'><span>" + users[index].ranking_position + "<sup>o</sup> lugar</span> - <span>" + users[index].score + "pontos</span></p>");
    player_list.append("</div>");
    player_list.append("</li>");
  }
}

function getMyID() {
  tag_elem_with_id = document.getElementById("my_id");

  my_id_to_return = tag_elem_with_id.getAttribute("rel");
  return my_id_to_return;
}

function getLocation() {
  return window.location.protocol + '//' + window.location.host;
}

function gameReady() {
  new Ajax.Request("/games/game_ready", {
    asynchronous:false,
    method: "get"
  })
}

function leave(args) {
  params = args.evalJSON()

  if ($('jogador_' + params.id) != null) {
    $('jogador_' + params.id).remove();
  }
}

function getAnswer(answer, time) {
  new Ajax.Request("/games/verify_answer?answer=" + answer + "&time=" + time, {
    method: 'get',
    asynchronous:false
  });
}

function sendAnswerResponse(response, answer) {
  if ($('flash') != null) {
    $('flash').sendAnswerResponse(response, answer);
  }
}

function imReady(game_id, user_id) {
  setPlayerReady(user_id)
  new Ajax.Request("/games/set_user?id=" + game_id, {
    method: 'get',
    asynchronous:true,
    onSuccess: function() {
      if (jQuery('.bt_pronto')[0] != null) {
        jQuery('.bt_pronto')[0].remove();
      }
    }
  });
}

function unsetUserLobby(user_id) {
  if ($('lobby_user_' + user_id) != null) {
    $('lobby_user_' + user_id).remove();
  }
}

function setPlayerReady(user_id) {
  if ($('lobby_user_' + user_id) != null) {
    $('lobby_user_' + user_id).childElements()[0].innerHTML = "Está pronto"
    $('lobby_user_' + user_id).className = "pronto"
  }
}

function startTimer(game_id) {
  if ($timer == "") {
    if ($('lobby_timer') != null) {
      $('lobby_timer').setStyle({"display": "block"})
    }
    new Ajax.Request("/games/get_timer?id=" + game_id, {
      method: 'get',
      asynchronous:false,
      onSuccess: function(data) {
        $timer_start = parseInt(data.responseText);
      }
    });
    $timeout = new Date().getTime() + ( ( 30 - $timer_start ) * 1000 );

    new PeriodicalExecuter(function(timer) {
      $timer = timer;
      $calls = ($calls + 1) % 10;
      if ($calls == 0) {
        new Ajax.Request("/games/remaining_time?game_id=" + game_id, {
          method: 'get',
          asynchronous:false,
          onSuccess: function(data) {
            $timeRemaining = parseInt(data.responseText);
            $timeout = new Date().getTime() + ( $timeRemaining * 1000 );
          }
        })
      }
      else {
        $timeRemaining = Math.max(0, ( $timeout - new Date().getTime() ) / 1000);
      }
      if ($timeRemaining <= 0) {
        stopTimer();
        startGame(game_id);
        new Ajax.Request("/games/start_game/" + game_id, {
          method: 'get',
          asynchronous: false
        });
      }
      else {
        if ($('timer')) {
          timeText = parseInt($timeRemaining)
          if (timeText == "1")
            $('timer').innerHTML = "1 SEGUNDO";
          else if (timeText == "0")
            $('timer').innerHTML = "AGORA!";
          else
            $('timer').innerHTML = timeText + " SEGUNDOS";
        }
      }
    }, 1);
  }
  ;
}

function stopTimer() {
  if ($timer != "") {
    if ($('lobby_timer')) {
      $('lobby_timer').setStyle({"display": "none"});
    }
    $timer.stop();
    $timer = "";
    $timer_start = 60
  }
}

function startGame(game_id) {
  if ($timer != '') {
    $timer.stop();
  }

  $('jogo_flash').setStyle({"visibility" : "visible"});
  $('bt_sairPartida').setStyle({"visibility" : "visible"});

  if ($('lightbox')) {
    $('lightbox').remove();
  }

  clearInterval($lobby_timer);
}

function set_user_lobby(user_id, user_name) {
  if (( $('lobby_list_users') != null ) && ( $('lobby_user_' + user_id) == null)) {
    li = new Element('li');
    $('lobby_list_users').appendChild(li);
    li.setAttribute("id", "lobby_user_" + user_id);
    li.addClassName("nao_pronto");
    li.innerHTML = "" + user_name + " - <span>Não está pronto</span>";
  }
}

function getBackgroundURL() {
  var image_url = ""
  new Ajax.Request("/games/get_background_image", {
    method: 'get',
    asynchronous:false,
    onSuccess: function(image) {
      if (image.responseText != "null") {
        image_url = "http://" + window.location.host + image.responseText
      }
      else {
        image_url = null
      }
    }
  });
  return image_url;
}

function hostname() {
  host = 'http://' + window.location.host
  return host;
}

function endGame() {
  new Ajax.Request("/games/exit", {
    method: 'get',
    asynchronous:false
  })
}

function exitGame() {
  new Ajax.Request("/games/exit_and_redirect", {
    method: 'get',
    asynchronous:false,
    onSuccess: function (data) {
      window.location = '/rooms/' + data.responseText;
    }
  })
}