function updateGameList( id, players, num_players ){
    if ( $( "users_in_" + id ) != null ){
        players_in_game = document.getElementById('users_in_' + id)
        list_player = document.getElementById('list_player_' + id)
        players_in_game.innerHTML = num_players;
        list_player.innerHTML = players;
    }
}

function setStartedButton(game_id, text){
  if( $("game_status_" + game_id) != null){
    $("game_status_" + game_id).innerHTML =  "<a href='#' class='jogo_iniciado texthide'><span>"+ text +"</span></a>";
  }
}

window.onload = function(){
  new Ajax.Request("/games/exit", {
    asynchronous:true,
    method: 'get'
  });
  setGameNumbers();
}
