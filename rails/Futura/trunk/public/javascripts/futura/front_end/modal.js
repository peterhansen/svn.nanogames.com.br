var modal;
function constructModal(id, url) {
  closeModal(id);

  if (!document.getElementById(id)) {
    if ($('lobby')) {
      $('lobby').remove();
      $('overlay').remove();
      errorModal(id, url);
    }
    var bod = $('site');
    var elem = document.createElement('a');
    bod.appendChild(elem);
    elem.id = id;
    elem.href = url;

    $(id).setStyle("display: none;");
  }

  showModal(id);
}

function showModal(id) {
  modal = new lightbox($(id));
  modal.activate();
}

function errorModal(id, url) {
  bod = document.getElementsByTagName('body')[0];
  overlay = document.createElement('div');
  overlay.id = 'overlay';
  lb = document.createElement('div');
  lb.id = 'lightbox';
  lb.className = 'loading';
  lb.innerHTML = '<div id="lbLoadMessage">' + '</div>';
  bod.appendChild(overlay);
  bod.appendChild(lb);
  constructModal(id, url);
}

function createGame(selected_value, room_id) {
  var url = "/games/new?id=" + room_id + "&tipo_mesa=" + selected_value;
  var id = "create_game_" + selected_value;
  constructModal(id, url);
}

function closeModal(id) {
  try {
    $(id).remove();
    modal.deactivate();
  }
  catch(err) {
    error = "Modal is null."
  }
}

function forgetPassword() {
  closeModal('login_modal');
  constructModal('password_modal', '/users/password/new');
}
