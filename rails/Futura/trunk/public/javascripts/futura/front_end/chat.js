$text_area_input_value=0;
$char_count_value=0;

jQuery(function() {
  var jug = new Juggernaut;

  jug.subscribe("channel_" + jQuery("#game_id").val().toString(), function(data){
    showChatMessage(data);
  });
  
  jQuery('#input_message').live('keyup', function(event) {
    var message = jQuery('#input_message').val();
    
    if ((event.keyCode == Event.KEY_RETURN) && (event.ctrlKey)) {
      if ((message != '') && (message != "\n")) {
        sendChatMessage(message);
      }
    }
  });

  jQuery('#invite_message_button').click( function() {
    sendChatMessage(jQuery('#input_message').val());
  });
});

function sendChatMessage(message_body) {
  var message = message_body.gsub("\n", "<br>");

  new Ajax.Request("/chat/publish", {
    method: 'post',
    postBody: 'message=' + message + '&game_id=' + jQuery('#game_id').val().toString() + "&user_id=" + jQuery('#user_id').val().toString(),
    asynchronous:true,
    onSuccess: function(){
      jQuery('#input_message').val("");
      jQuery('#char_counter').html($char_count_value.toString());
    }
  });
}

function showChatMessage(data){
  var nickname = data.nickname;
  var color = data.color;
  var message_body = data.message;

  show_area = $("show_area_message");
  p_element = new Element('p');
  strong_element = new Element('strong');
  strong_element.className = color;
  strong_element.appendChild( document.createTextNode( nickname ) );
  p_element.appendChild( strong_element );
  p_element = setMessage(message_body, p_element);
  show_area.appendChild( p_element );
  show_area.scrollTop = show_area.scrollHeight
}

function setMessage(message_body, p_element){
  message_splited = message_body.strip().split('<br>');
  message_size = message_splited.size() - 1;
  message_splited.each(function(m, i){
    p_element.appendChild( document.createTextNode( m ) );
    if( i < message_size){
      br_element = new Element('br');
      p_element.appendChild(br_element);
    }
  });
  return p_element;
}

function clearTextArea(){
  if($('input_message').value == $text_area_input_value){
    $('input_message').value = '';
  }
}

function fieldMaxLength(field, char_count){
  if($(char_count)){
    char_count_element = $(char_count)
    if( field.value.length > $char_count_value ){
      char_count_element.setStyle({'color': 'red'})
      $('invite_message_button').disable();
      $('invite_message_button').setStyle({'cursor': 'default'})
      $('invite_message_button').setAttribute('src', '/images/comum/bt_enviarMensagem_disabled.png');
      //field.value = field.value.substring(0, max_length)
    }
    else{
      if( char_count_element.getStyle('color') == 'red'){
        char_count_element.setStyle({'color': 'blue'})
        $('invite_message_button').enable();
        $('invite_message_button').setAttribute('src', '/images/comum/bt_enviarMensagem.png');
        $('invite_message_button').setStyle({'cursor': 'pointer'})
      }
    }
    char_count_element.innerHTML = $char_count_value - field.value.length
  }
}

