var $content_length = 0;

function pollServer(path, timer, element, callback) {
  return window.setInterval(function() {
    new Ajax.Request(path, {
      method: 'get',
      asynchronous: false,
      onSuccess: function(data, transport) {
        if (data.status == 200) {
          if (element) {
            document.getElementById(element).innerHTML = data.responseText;
          }
        }

        if (callback) {
          callback.call(data.responseText);
        }

        // o tamanho pode ser maior ou menor, porque pode haver notificações removidas
        if ((element == "notifications") && ($content_length != data.getHeader("Content-Length"))) {
          if ($content_length != 0) {
            pulsateNotification(30000);
            play();
          }

          $content_length = data.getHeader("Content-Length");
        }
      }
    });
  }, timer);
}
