document.observe("dom:loaded", function() {
    $('pais').observe('change', function() { setSelectDisable(); });
    if($('user_agreed_terms') != null){
      $('user_agreed_terms').observe('change', function() {setSubmitState()});
    }
});
  
function setSubmitState(){
    if($('user_agreed_terms') != null){
      if(document.getElementById('user_agreed_terms').checked == true){
//        $('user_submit').setAttribute("class", "bt_continuar");
        $('user_submit').className = "bt_continuar";
        $('user_submit').enable();
      }
      else{
//        $('user_submit').setAttribute("class", "bt_continuar_disabled");
        $('user_submit').className = "bt_continuar_disabled";
        $('user_submit').disable();
      }
    }
}

function setSelectDisable(){
    if ($('pais').options[25].selected){
        $('estado').enable();
        //$('.estado').show()
    }
    else{
        $('estado').disable();
        //$('.estado').hide()
    }
}
  
function checkNumberSize( element, size ){
  if ( element.value.length >= size ) {
    //element.value = element.value.substring(0, size);
    if(element.id == "dia") {
      document.getElementById('mes').focus();
    } else if(element.id == "mes") {
      document.getElementById('ano').focus();
    } else if(element.id == "ano") {
      document.getElementById('user_show_age').focus();
    }
  } 
  else if ( isNaN( element.value ) ) {
    element.value = element.value.substring(0, element.value.length - 1)
  }
}

function enablePassword(){
    $('user_password').enable();
    $('user_password_confirmation').enable();
}

