jQuery.noConflict();

Cufon.replace('h2');
Cufon.replace('em');

jQuery(function() {
  jQuery.ajax({
    url: '/set_user_online',
    async: false
  });
});

jQuery(window).unload(function() {
  jQuery.ajax({
    url: '/set_user_offline',
    async: false
  });
});

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-20443707-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script');
  ga.type = 'text/javascript';
  ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(ga, s);
})();