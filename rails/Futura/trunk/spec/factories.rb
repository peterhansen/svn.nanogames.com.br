FactoryGirl.define do
  sequence :name do |n|
    "Object_#{n}"
  end

  factory :authentication do
    provider      { 'Facebook' }
    uid           { '1234' }
    oauth_token   { '123456789' }
    oauth_secret  { '987654321' }
    user
  end

  factory :user do
    name          { FactoryGirl.generate :name }
    email         { "email_#{name}@example.com" }
    encrypted_password { name }
    password_salt { name }
    patent        { name }
    password      "senha123"
    post_name     { name }
    born_day      10
    born_month    10
    born_year     1970
    city          "Rio de Janeiro"
    nickname      { name }
    agreed_terms  true
  end

  factory :participation do
    user { FactoryGirl.create :user, :name => FactoryGirl.generate(:name) }
  end

  factory :game do
    room_id "Room #{FactoryGirl.generate :name}"
  end

  factory :game_with_players, :class => Game do
    room_id "Room #{FactoryGirl.generate :name}"
    participations {[
      FactoryGirl.create(:participation),
      FactoryGirl.create(:participation)      
    ]}
  end

  factory :room do
    description "Description"
    name {"name #{FactoryGirl.generate :name}"}
  end

  factory :theme do
    name {"Theme #{FactoryGirl.generate :name}"}
  end
end