require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe "Authenticatimg" do
  context "inside Facebook" do
    describe "GET /index" do
      context "inside Facebook" do
        before(:each) do
          host! 'facebook.nanostudio.com.br'
          #@request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:twitter]
        end

        it "should be on the facebook subdomain" do
          get '/auth/facebook/callback'
          @request.subdomain(2).should == 'facebook'
        end

        it "should set session[:facebook_uid] if signed_request is present" do
          Authentication.should_receive(:find_by_uid).at_least(1).times.and_return(stub(:user => 'User'))
          get '/auth/facebook/callback'
          @controller.session[:facebook_uid].should be_present
          @controller.logged_user.should == 'User'
        end

        it "should redirect to /home/index" do
          get '/auth/facebook/callback'
          response.should redirect_to '/'
        end

        context "if the user never accessed via facebook" do
          it "should create an User" do
            User.delete_all
            Authentication.delete_all
            
            lambda do
              get '/auth/facebook/callback'
            end.should change(User, :count).by(1)
          end
          
          it "should create an Authentication" do
            User.delete_all
            Authentication.delete_all

            lambda do
              get '/auth/facebook/callback'
            end.should change(Authentication, :count).by(1)
          end
        end

        context "if the user have accessed via facebook" do
          it "should set the session[:facebook_uid] to his authentication uid" do
            user = FactoryGirl.create :user
            auth = FactoryGirl.create :authentication, :uid => '123545', :provider => 'facebook'

            get '/auth/facebook/callback'
            @controller.session[:facebook_uid].should == '123545'
          end
        end
      end
    end
  end
end
