require File.dirname(__FILE__) + '/../spec_helper'

describe "As a not logged in user" do
  describe "When I visit '/'" do
    it "I should see the home screen" do
      get '/'
      response.should redirect_to(new_user_session_path)
    end
  end
end