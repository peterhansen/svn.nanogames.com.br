# == Schema Information
#
# Table name: games
#
#  id               :integer(4)      not null, primary key
#  room_id          :integer(4)
#  user_ids         :integer(4)
#  started          :boolean(1)      default(FALSE)
#  private          :boolean(1)      default(FALSE)
#  min_users        :integer(4)      default(2)
#  max_users        :integer(4)      default(6)
#  permited_users   :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  difficulty_id    :integer(4)
#  turn             :integer(4)      default(0)
#  identifier       :integer(4)
#  status           :integer(4)
#  started_at       :datetime
#  cicle            :integer(4)
#  quick_start      :boolean(1)      default(FALSE)
#  previous_game_id :integer(4)
#

require File.dirname(__FILE__) + '/../spec_helper'

describe Game do
  it { should have_many(:users).through(:participations) }
  it { should have_many(:participations) }
  it { should have_many(:cicles) }
  it { should belong_to(:room) }
  it { should have_one :owner }
  it { should validate_presence_of :room_id }
  it { should validate_numericality_of :min_users }
  it { should validate_numericality_of :max_users }

  describe "#current_user" do
    context "when game is finished" do
      it "returns nil" do
        FactoryGirl.build(:game, :status => 5).current_user.should be_nil
      end
    end

    context "when game is running" do
      it "returns nil if there is no active participations" do
        FactoryGirl.build(:game, :status => 4).current_user.should be_nil
      end

      # it "returns the current user if there is active participations"
    end
  end

  describe "#first_user" do
    it "returns the first user" do
      g = FactoryGirl.create :game_with_players
      g.first_user.should eq(g.participations.first.user)
    end
  end

  describe "#next_user" do
    let(:game) do
      FactoryGirl.create :game_with_players
    end

    it "returns the next user" do
      game.should_receive(:current_user).at_least(1).times.and_return(game.participations.first.user)
      game.next_user.should eq(game.participations.last.user)
    end

    it "returns the first player if the last player is playing" do
      game.should_receive(:current_user).at_least(1).times.and_return(game.participations.last.user)
      game.next_user.should eq(game.participations.first.user)
    end
  end

  describe "#active_participations" do
    it "returns nil if game is finished" do
      FactoryGirl.create(:game, :status => 5).active_participations.should == []
    end

    it "return the active participations if game is running" do
      game = FactoryGirl.create(:game_with_players)
      game.active_participations.should == game.participations
    end
  end

  describe "#full?" do
    it "returns true if game is full" do
      FactoryGirl.create(:game_with_players, :max_users => 2).full?.should be_true
    end

    it "returns false if game is not full" do
      FactoryGirl.create(:game).full?.should_not be_true
    end
  end
end
