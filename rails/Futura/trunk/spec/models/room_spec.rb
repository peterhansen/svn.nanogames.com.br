# == Schema Information
#
# Table name: rooms
#
#  id                      :integer(4)      not null, primary key
#  name                    :string(255)
#  created_at              :datetime
#  updated_at              :datetime
#  image_file_name         :string(255)
#  image_content_type      :string(255)
#  image_file_size         :integer(4)
#  image_updated_at        :datetime
#  capacity                :integer(4)      default(100)
#  sponsor_id              :integer(4)
#  game_ids                :integer(4)
#  background_file_name    :string(255)
#  background_content_type :string(255)
#  background_file_size    :integer(4)
#  background_updated_at   :datetime
#  description             :string(255)     default(""), not null
#

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Room do
  before(:each) do
    @room = FactoryGirl.create(:room)
  end

  # after(:each) do
  #   Room.delete_all
  # end
  
  it "requires the presence of name" do
    @room.should validate_presence_of :name
  end
  
  it "has and belongs to many themes" do
    @room.should have_and_belong_to_many :themes
  end
  
  it "belongs to sponsor" do
    @room.should belong_to :sponsor
  end

  it "has many games" do
    @room.should have_many :games
  end

  it "has many users" do
    @room.should have_many :users
  end

  it "has and belongs to many histories" do
    @room.should have_and_belong_to_many :histories
  end
end
