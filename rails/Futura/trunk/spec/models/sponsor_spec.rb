# == Schema Information
#
# Table name: sponsors
#
#  id         :integer(4)      not null, primary key
#  room_ids   :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

require File.dirname(__FILE__) + '/../spec_helper'

describe Sponsor do
  it { should have_many :rooms }
end
