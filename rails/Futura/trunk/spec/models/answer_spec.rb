# == Schema Information
#
# Table name: answers
#
#  id          :integer(4)      not null, primary key
#  question_id :integer(4)
#  a           :string(255)
#  b           :string(255)
#  c           :string(255)
#  d           :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

require File.dirname(__FILE__) + '/../spec_helper'

describe Answer do
  it { should be_invalid }
  it "should be valid with correct attributes" do
    # Factory(:answer).should be_valid
  end
  
  it { should validate_presence_of(:a) }
  it { should validate_presence_of(:b) }
  
  it { should belong_to(:question) }
end
