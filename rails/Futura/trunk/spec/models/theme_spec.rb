# == Schema Information
#
# Table name: themes
#
#  id                :integer(4)      not null, primary key
#  parent_id         :integer(4)
#  children_count    :integer(4)
#  ancestors_count   :integer(4)
#  descendants_count :integer(4)
#  hidden            :boolean(1)
#  name              :string(255)
#  description       :string(255)
#  name_with_level   :string(255)
#  position          :integer(4)
#  created_at        :datetime
#  updated_at        :datetime
#  requested_times   :integer(4)      default(0)
#

require File.dirname(__FILE__) + '/../spec_helper'

describe Theme do
  after(:each) do
    Theme.delete_all
  end
  
  it { should have_many(:questions) }
  it { should have_many(:suggested_questions) }
  it { should have_and_belong_to_many(:rooms) }
  it { should validate_presence_of(:name) }
  # it { should validate_uniqueness_of(:name) }

  it "should be valid with correct attributes" do
    FactoryGirl.create(:theme).should be_valid
  end
  
  it "should have 1 levels in the name_with_level" do
    theme = FactoryGirl.create(:theme, :name => "Filho")
    theme.update_attribute(:parent, FactoryGirl.create(:theme))
    theme.name_with_level.should eql("> Filho")
  end

  describe "Adding Parent" do
    let(:dad) { FactoryGirl.create(:theme) }
    let(:son) { FactoryGirl.create(:theme, :parent => dad) }

    it "should have parent id" do
      son.parent.should eql(dad)
    end
    
    it "should have ancestours_count of 1" do
      son.ancestors_count.should eql(1)
    end
  end

  describe "Adding Children" do
    let(:dad) { FactoryGirl.create(:theme) }
    let(:son) { FactoryGirl.create(:theme, :parent => dad) }
    let(:daughter) { FactoryGirl.create(:theme, :parent => dad) }

    before(:each) { son; daughter; }

    it "should have children_count of 2" do
      dad.children_count.should eql 2
    end
    
    it "should have descendants_count of 2" do
      dad.descendants_count.should eql 2
    end
    
    it "should have 2 descendents" do
      dad.descendants.size.should eql 2
    end
  end

  describe "Adding Children and Grandchildren" do
    let(:dad) { FactoryGirl.create(:theme) }
    let(:son) { FactoryGirl.create(:theme, :parent => dad) }
    let(:grandson) { FactoryGirl.create(:theme, :parent => son) }

    before(:each) { son; grandson; }
    
    it "should have children_count of 1" do
      dad.children.size.should eql 1
    end
    
    it "should have descendants_count of 2" do
      dad.descendants_count.should eql 2
    end
    
    it "should have 2 descendants" do
      dad.descendants.size.should eql 2
    end
    
    it "grandson should have 2 ascendents" do
      grandson.ancestors_count.should eql 2
      grandson.ancestors.size.should eql 2
    end
  end
end