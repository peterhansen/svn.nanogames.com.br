# == Schema Information
#
# Table name: users
#
#  id                    :integer(4)      not null, primary key
#  email                 :string(255)     default(""), not null
#  encrypted_password    :string(128)     default(""), not null
#  password_salt         :string(255)     default(""), not null
#  confirmation_token    :string(255)
#  confirmed_at          :datetime
#  confirmation_sent_at  :datetime
#  reset_password_token  :string(255)
#  remember_token        :string(255)
#  remember_created_at   :datetime
#  sign_in_count         :integer(4)      default(0)
#  current_sign_in_at    :datetime
#  last_sign_in_at       :datetime
#  current_sign_in_ip    :string(255)
#  last_sign_in_ip       :string(255)
#  failed_attempts       :integer(4)      default(0)
#  unlock_token          :string(255)
#  locked_at             :datetime
#  created_at            :datetime
#  updated_at            :datetime
#  avatar_file_name      :string(255)
#  avatar_content_type   :string(255)
#  avatar_file_size      :integer(4)
#  avatar_updated_at     :datetime
#  blocked               :boolean(1)      default(FALSE)
#  game_id               :integer(4)
#  room_id               :integer(4)
#  name                  :string(255)
#  post_name             :string(255)
#  marital_status_id     :integer(4)
#  city                  :string(255)
#  about                 :string(255)
#  state_id              :integer(4)
#  country_id            :integer(4)
#  gender_id             :integer(4)
#  born_day              :string(255)
#  born_month            :string(255)
#  born_year             :string(255)
#  nickname              :string(255)
#  show_email            :boolean(1)
#  show_age              :boolean(1)
#  receive_newsletter    :boolean(1)
#  agreed_terms          :boolean(1)
#  show_marital_status   :boolean(1)      default(FALSE)
#  room_user_id          :string(255)     default("0")
#  game_player_id        :string(255)     default("0")
#  color                 :string(255)
#  turn                  :integer(4)
#  orkut_id              :string(255)
#  complete_name         :string(255)
#  my_game_id            :integer(4)
#  skill                 :integer(4)      default(500)
#  digest_avatar         :string(255)
#  score                 :integer(4)      default(0)
#  inviter_id            :integer(4)
#  current_session_id    :string(255)
#  enter_in_game_at      :datetime
#  skill_month_variation :integer(4)      default(0)
#  skill_week_variation  :integer(4)      default(0)
#  questions_skipped     :integer(4)
#  patent                :string(255)     default("Calouro"), not null
#  users_count           :integer(4)      default(0)
#

require File.dirname(__FILE__) + '/../spec_helper'

describe User do
  it { should be_invalid }

  it "should be valid with correct attributes" do
    FactoryGirl.create(:user).should be_valid
  end
  
  it { should belong_to(:my_game) }
  it { should belong_to(:room) }
  it { should have_and_belong_to_many(:histories) }
end
