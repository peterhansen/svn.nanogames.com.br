require File.dirname(__FILE__) + '/../spec_helper'

describe 'Omniauth Authentication' do
  before(:each) do
    User.delete_all
    Authentication.delete_all
    @user = FactoryGirl.create :user
  end

  describe "#add_omniauth_authentication" do
    it "adds a new authentication to the customer" do
      lambda do
        @user.add_authentication('provider' => 'qualquer', 'uid' => 'coisa', 'credentials' => { 'token' => '1234', 'secret' => '4321' })
      end.should change(@user.authentications, :size).by(1)
    end

    it "returns an Authentication object" do
      @user.add_authentication('provider' => 'qualquer', 
        'uid' => 'coisa', 
        'credentials' => { 'token' => '1234', 'secret' => '4321' }).class.should == Authentication
    end
  end

  describe "#create_omniauth_authentication" do
    it "creates a new customer" do
      User.should_receive(:create!)
      User.create_with_omniauth('user_info' => { 'name' => 'qualquer', 'email' => 'qualquer@coisa.com'}, 
        'provider' => 'qualquer', 
        'uid' => 'coisa', 
        'credentials' => { 'token' => '1234', 'secret' => '4321' })
    end

    it "create a new authentication" do
      lambda do
        User.create_with_omniauth('user_info' => { 'name' => 'qualquer', 'email' => 'qualquer@coisa.com'}, 
          'provider' => 'qualquer', 
          'uid' => 'coisa', 
          'credentials' => { 'token' => '1234', 'secret' => '4321' })
      end.should change(Authentication, :count).by(1)
    end
  end

  describe "#authenticate_omniauth" do
    context "with omniauth_hash" do
      it "fetches authentication" do
        Authentication.should_receive(:find_by_provider_and_uid).and_return(stub(:user => '123'))
        User.authenticate_omniauth(omniauth_hash: 'alguma coisa')
      end
    end

    context "with username and password" do
      it "fetches the customer" do
        User.should_receive(:authenticate)
        User.authenticate_omniauth(user_hash: {nickname: 'zeh', password: 'senha123'})
      end
    end
  end

  describe "#has_provider" do
    it "returns true if the customer have authenticated with the given provider" do
      auth = FactoryGirl.create :authentication
      auth.user.has_provider?(auth.provider.to_sym).should be_true
    end

    it "returns false if the customer have not authenticated with the given provider" do
      auth = FactoryGirl.create :authentication
      auth.user.has_provider?(:outro).should be_false
    end
  end

  describe "#merge" do
    it "adds the merged customer's authentications into the current customer" do
      user1 = FactoryGirl.create :user
      user2 = FactoryGirl.create :user
      FactoryGirl.create :authentication, :user => user1
      FactoryGirl.create :authentication, :user => user2
      user1.merge user2
      user1.authentications.count.should == 2
    end

  #   it "marks the merged customer as deactivated"
  end
end