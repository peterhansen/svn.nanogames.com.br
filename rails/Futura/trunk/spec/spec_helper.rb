require 'rubygems'
require 'spork'

Spork.prefork do
  ENV["RAILS_ENV"] ||= 'test'

  require File.expand_path(File.join(File.dirname(__FILE__),'..','config','environment'))
  require 'rspec/rails'
  #require 'capybara/rspec'

  Dir[File.expand_path(File.join(File.dirname(__FILE__),'support','**','*.rb'))].each {|f| require f}

  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:facebook] = {
    'provider' => 'facebook',
    'uid' => '123545',
    'user_info' => {
      'name' => 'Ze',
      'email' => 'ze@exemplo.com'
    },
    'credentials' => {
      'token' => '1234',
      'secret' => '4321'
    }
  }

  module Devise::TestHelpers
    def sign_in_user(user)
      user.confirm!
      sign_in user
      controller.session[:session_id] = '42'
    end
  end

  RSpec.configure do |config|
    # config.fixture_path = "#{Rails.root}/spec/fixtures/"
    # config.global_fixtures = :all
    # config.use_transactional_fixtures = true

    config.include Devise::TestHelpers, :type => :controller
    config.mock_with :rspec
  end
end

Spork.each_run do
end