#require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')
require 'spec_helper'

describe HomeController do
  describe "GET /index" do
    context "new user" do
      #it "should redirect user to the login page" do
      #  get :index
      #  should redirect_to(new_user_session_path)
      #end
      
    end

    context "facebook user" do
      it "should redirect to the authentication path" do
        @request.env['HTTP_HOST'] = 'facebook.nanogames.com.br'
        get :index
        response.should redirect_to('https://graph.facebook.com/oauth/authorize?client_id=190475304357156&redirect_uri=http://localhost:3000/auth/facebook_canvas/')
      end
    end
  end
end