require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe GamesController do
  before(:each) do
    User.delete_all
    sign_in_user(FactoryGirl.create :user)
    Game.delete_all
  end

  after(:each) do
    User.delete_all
  end

  describe "GET /index" do
    it "assigns @game" do
      controller.should_receive(:current_user).at_least(1).times.and_return(FactoryGirl.create :user)
      Game.should_receive(:first).and_return('game')
      get :index
      assigns(:game).should eql('game')
    end
  end
 
  describe "#show (via GET)" do
    before(:each) do
      controller.should_receive(:current_user).at_least(1).times.and_return(FactoryGirl.create :user)
    end

    describe "when the game exists and" do
      context "it is full" do
        let(:game) { FactoryGirl.create(:game_with_players, :max_users => 2) }

        it "redirects to room#show" do
          Game.should_receive(:find_by_id).and_return(game)
          get :show, :id => '42'
          response.should redirect_to(room_path(game.room))
        end
      end

      context "it is not full" do
        let(:game) { FactoryGirl.create :game }

        it "render show template" do
          Game.should_receive(:find_by_id).and_return(game)
          get :show, :id => '42'
          response.should redirect_to(room_path(game.room))
        end
      end

      context "it is initiated" do
        let(:game) { FactoryGirl.create :game, :status => 4 }

        it "redirects to the room#show" do
          Game.should_receive(:find_by_id).and_return(game)
          get :show, :id => '42'
          response.should redirect_to(room_path(game.room))
        end
      end

      context "it is over" do
        let(:game) { FactoryGirl.create :game, :status => 5 }

        it "redirects to the room#show" do
          Game.should_receive(:find_by_id).and_return(game)
          get :show, :id => '42'
          response.should redirect_to(room_path(game.room))
        end
      end
    end

    context "when the the game doesn't exists" do
      it "redirects to room#index" do
        Game.should_receive(:find_by_id).and_return(nil)
        get :show, :id => '42'
        response.should redirect_to(rooms_path)
      end
    end
  end
end