Futura::Application.configure do
  config.cache_classes                                  = false
  config.whiny_nils                                     = true
  config.action_controller.consider_all_requests_local  = true
  config.action_view.debug_rjs                          = true
  config.action_controller.perform_caching              = false
  config.action_mailer.delivery_method                  = :test
  # config.action_mailer.sendmail_settings                = {:location => '/usr/sbin/sendmail',
                                                           # :arguments => '-i -t'}
  config.action_mailer.perform_deliveries               = false
  config.action_mailer.raise_delivery_errors            = false
  config.action_mailer.default_url_options              = { :host => 'localhost:3000' }
  ActionMailer::Base.smtp_settings[:enable_starttls_auto] = false

  config.active_support.deprecation = :log
  config.cache_store = :mem_cache_store
  
  config.facebook_app_id = "165039626892116"
  config.facebook_app_secret = "2ca9a41062f5bd7618ecdcda535412b4"
  config.facebook_app_name = "nanotestapp"
end