Futura::Application.routes.draw do
  devise_for :admins
  devise_for :users, :controllers => {:sessions => "sessions"}

  match '/' => 'home#index', :constraints => { :subdomain => /facebook/ }
  root :to => 'home#welcome'

  resources :oauth_consumers do
    member do
      get :callback
    end
  end

  match '/rooms/get_room_partial/:id' => 'rooms#get_room_partial'
  match '/rooms/get_game_info/:id' => 'rooms#get_game_info'
  post 'rooms', :to => 'rooms#index'
  resources :rooms do
    member do
      get :get_room_partial
    end

    collection do
      get :exit
      get :get_game_info
      get :set_user
      get :get_player_list
    end
  end

  match '/games/update_player_list/:id' => 'games#update_player_list'
  match '/games/start_game/:id' => 'games#start_game'
  match '/games/lobby/:id' => 'games#lobby'
  resources :games do
    member do
      get :start_game
    end

    collection do
      get :replay_game
      get :lobby
      get :exit_and_redirect
      get :exit
      get :users_list
      get :confirm_exit
      get :state_update
      get :set_user
      get :verify_answer
      get :load_flash
      get :update_player_list
      get :new_turn
      get :get_timer
      get :get_velocity
      get :get_background_image
      get :quick_start
      get :remaining_time
      get :request_user_list
      get :game_ready
      get :get_question
    end
  end

  resources :players do
    collection do
      get :all
      get :friends
      get :ranking
      get :search
    end
  end

  resources :challenges do
    collection do
      get :send_invites
      get :get_themes
      get :accept_challenge
      get :send_invites_ranking
      get :invite_challenge
      get :index_ranking
      get :index
      get :remove_challenge
    end
  end

  resources :profiles do
    collection do
      get :add_friend
    end
  end

  resources :invitations do
    collection do
      get :orkut_invitation
      get :facebook
      post :send_facebook_invites
      get :show_captcha
      get :captcha
      post :send_orkut_invitation
      get :orkut
      get :search_ajax
      get :register_orkut
    end
  end

  resources :polls do
    collection do
      get :success
    end

    member do
      post :respond
    end
  end

  match '/registrations/new' => 'registrations#new'
  match "/futura_registrations/social_media" => 'futura_registrations#social_media'
  match "/futura_registrations/activate_account" => 'futura_registrations#activate_account'
  match "/futura_registrations/email_success_message" => 'futura_registrations#email_success_message'

  # devise_scope :user do
  match '/auth/:provider/callback' => 'authentications#create'
  match '/auth/failure' => 'authentications#failure'
  match '/auth/facebook_canvas' => 'authentications#facebook_canvas'
  match '/auth/destroy/:id' => 'authentications#destroy'
  # end

  match '/share' => 'pages#share'
  match '/pages/:action' => 'pages'
  match '/welcome' => 'home#welcome', :as => :welcome
  post '/home' => 'home#index', :via => :get
  match '/users' => 'home#index', :via => :get
  match '/noticias' => 'home#news', :as => :noticias
  match '/polling' => 'home#polling', :as => :pull_test
  match '/get_time' => 'home#get_time', :as => :get_time
  match '/home/suggest_question_answer' => 'home#suggest_question_answer'
  match '/games/quick_start' => 'games#quick_start'
  match '/rankings' => 'rankings#index', :as => :rankings
  match '/rankings/monthly' => 'rankings#monthly', :as => :monthly_rankings
  match '/rankings/all_time' => 'rankings#all_time', :as => :all_time_rankings
  match '/rankings/weekly' => 'rankings#weekly', :as => :weekly_rankings
  match '/rankings/friends' => 'rankings#friends', :as => :friend_rankings
  match '/notifications/set_disable' => 'notifications#set_disable', :as => :notifications_disable
  match '/notifications/get_notifications' => 'notifications#get_notifications'
  match '/set_user_online' => 'application#set_user_online'
  match '/set_user_offline' => 'application#set_user_offline'
  match '/suggest_question' => 'home#suggest_question'
  match '/orkut_index' => 'pages#index_orkut'
  match '/exit' => 'game_sessions#exit', :as => :game_sessions
  match '/home/submit_question' => 'home#submit_question'
  match '/avatar/index' => 'avatar#index'
  match '/avatar/new' => 'avatar#new'
  match '/avatar/image' => 'avatar#image'
  match '/avatar/temp_image' => 'avatar#temp_image'
  match '/avatar/crop' => 'avatar#crop'

  namespace :admin do
    root :to => 'themes#index'

    resources :questions do
      collection do
        post :create_question_from_csv
        post :create_question_from_txt
        get :import_csv
      end

      member do
        delete :destroy, :as => :destroy
      end
    end

    resources :themes do
      collection do
        get :get_childrens
      end
    end

    resources :rooms do
      collection do
        get :get_descendants
        get :get_themes_for_select
      end
    end

    resources :users do
      collection do
        get :mailing
        put :update_max_online_users
        get :config_users
        post :send_mailing, :as => :send_mailing
      end
    end

    resources :admins
    resources :notifications

    match "/news" => 'news#create', :via => :post, :as => :create_news
    resources :news

    resources :polls do
      collection do
        get :report
        post :update_active
        post :send_mailing
      end
    end

    resources :suggested_questions do
      collection do
        get :toggle_suggest_question
      end
    end
  end
end