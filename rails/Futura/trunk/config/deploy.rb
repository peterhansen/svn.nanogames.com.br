require "bundler/capistrano"

set :user, "rails"
set :application, "futura"
set :deploy_via, :export
set :repository,  "http://svn.nanogames.com.br/rails/Futura/trunk/"
set :scm_username, "nano_online_svn"
set :scm_password, 'nAnO_SvN123'
set :scm_verbose, true
set :chmod775, "app config db lib public vendor script script/* public/disp* log/*"

namespace :deploy do
  set(:base_ruby_path)    {'/usr/local'}
  set(:shared_path)       {"#{deploy_to}/shared"}
  set(:path_to_current)   {"#{deploy_to}/current"}

  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  task :set_sphinx do
    run "cd #{current_path}"
    run "rake ts:config RAILS_ENV=production"
    run "rake ts:index RAILS_ENV=production"
    run "rake ts:start RAILS_ENV=production"
  end

  
  task :create_symlinks do
    create_symlink_for_file("config/database.yml", "config/database.yml")
    create_symlink_for_file("config/environments/development.rb")
    create_symlink_for_directory("public/images")
    create_symlink_for_directory("public/design")
    create_symlink_for_directory("public/img")
    create_symlink_for_directory("public/system")
    create_symlink_for_directory("public/swf")
    create_symlink_for_directory("log")
    create_symlink_for_directory("tmp")
  end

  def create_symlink_for_file(origin, destiny = nil)
    run "sudo ln -sf #{shared_path}/#{origin} #{path_to_current}/#{destiny || origin}"
  end

  def create_symlink_for_directory(path)
    run "sudo ln -sf #{shared_path}/#{path} #{path_to_current}/#{path}"
  end

  task :seed do
    run "cd #{deploy_to}/current && sudo rake db:seed RAILS_ENV=production" do |channel, stream, text|
      logger.info text
    end
  end
end

namespace :foreman do
  desc "Export the Procfile to Ubuntu's upstart scripts"
  task :export, :roles => :app do
    run "cd #{deploy_to}/current && sudo bundle exec foreman export upstart /etc/init -a #{application} -u #{user} -l #{deploy_to}/shared/log -f Procfile.production"
  end

  desc "Start the application services"
  task :start, :roles => :app do
    sudo "start #{application}"
  end

  desc "Stop the application services"
  task :stop, :roles => :app do
    sudo "stop #{application}"
  end

  desc "Restart the application services"
  task :restart, :roles => :app do
    run "sudo start #{application} || sudo restart #{application}"
  end
end

after "deploy", "deploy:create_symlinks"
after "deploy", "deploy:cleanup"
after "deploy:update", "foreman:export"
after "deploy:update", "foreman:restart"

set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"
