@env = { :PATH => '/usr/local/bin:/usr/bin:/bin',
         :SHELL => '/bin/bash',
         :RAILS_ENV => ENV['RAILS_ENV'] }

every :monday, :at => '4:00am' do
  rake "ranking:reset_week"
end

every 1.month, :at => 'start of the month at 4:00am' do
  rake "ranking:reset_month"
end