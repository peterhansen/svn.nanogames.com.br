require File.expand_path('../boot', __FILE__)

require 'rails/all'
require File.dirname(__FILE__) + '/../lib/custom_logger.rb'

# If you have a Gemfile, require the gems listed there, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env) if defined?(Bundler)

module Futura
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.


    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    config.autoload_paths += Dir["#{config.root}/lib/**/", "#{config.root}/app/concerns"]
    config.action_view.javascript_expansions[:defaults] = %w(jquery-1.4.2.min cufon-yui DIN_Alternate_Medium_500.font jquery.Jcrop futura/front_end/poll futura/front_end/share_on_orkut )
    config.encoding = "utf-8"
    config.filter_parameters += [:password]
    config.time_zone = 'UTC'
    config.i18n.default_locale = :'pt-br'

    config.generators do |g|
      g.orm :active_record
      g.template_engine :erb
      g.test_framework :rspec, :fixture => true
    end

    config.middleware.swap Rails::Rack::Logger, CustomLogger, :silenced => ["/notifications/get_notifications",
                                                                            "/rooms/get_room_partial/",
                                                                            "/rooms/get_game_info/",
                                                                            "/set_user_online",
                                                                            "/set_user_offline",
                                                                            "/games/state_update/",
                                                                            "/games/lobby/",
                                                                            "/games/update_player_list/"]
  end
end
