  role :web, "futura.nanogames.com.br"
  role :app, "futura.nanogames.com.br"
  role :db, "futura.nanogames.com.br", :primary => true
  set(:deploy_to) {"/var/www/rails/#{application}"}
  set :stage, 'production'
