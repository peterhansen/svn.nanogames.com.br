#role :web, "184.106.93.206"
#role :app, "184.106.93.206"
#role :db, "184.106.93.206", :primary => true
#set(:deploy_to) {"/home/rails/www.cdf.org.br"}
role :web, "staging.nanogames.com.br"
role :app, "staging.nanogames.com.br"
role :db, "staging.nanogames.com.br", :primary => true
set(:deploy_to) {"/home/rails/www.cdf.org.br"}
set :stage, "staging"
