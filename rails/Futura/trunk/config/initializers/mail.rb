Futura::Application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.default :charset => 'UTF-8'
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.smtp_settings = {
      :enable_starttls_auto => false,
      :address        => 'mail.nanogames.com.br',
      :authentication => :login,
      :domain         => 'nanogames.com.br',
      :user_name      => 'noreply@nanogames.com.br',
      :password       => 'J2J3Q6aR',
  }
end