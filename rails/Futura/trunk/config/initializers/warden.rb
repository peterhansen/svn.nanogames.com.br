  # Setup Session Serialization
class Warden::SessionSerializer
  def serialize(record)
    [record.class, record.id]
  end

  def deserialize(keys)
    klass, id = keys
    klass.find(:first, :conditions => { :id => id })
  end
end

Warden::Strategies.add(:orkut_sign) do
  def authenticate!
    user = User.find_all_by_orkut_id( params[:ouid] ).last
    user.nil? ? fail!("Couldn't log in") : success!(u)
  end
end

 Warden::Strategies.add(:site) do

    def authenticate!
      user = User.find_by_email_or_nickname(params[:user][:email]) if params[:user]
      if user and user.encrypted_password == user.send("password_digest", params[:user][:password])
        success!(user)
        user.update_attribute(:current_session_id, session[:session_id])
      else
        fail!("Couldn't log in")
      end
    end
 end