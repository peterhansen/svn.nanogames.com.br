Futura::Application.configure do
  config.session_store :mem_cache_store, :key => '99d590ebcd668c51e0743f9bf2bf49627dce65e85fe4f2c00b41e077f822657d4a894e464bc092ca687e65b723b891084e55fd4ba96042f2d4dd1b09d8665d17'
  config.action_controller.session = {
      :key => '_futura',
      :cache => Rails.cache,
      :secret => '99d590ebcd668c51e0743f9bf2bf49627dce65e85fe4f2c00b41e077f822657d4a894e464bc092ca687e65b723b891084e55fd4ba96042f2d4dd1b09d8665d17'
  }
end