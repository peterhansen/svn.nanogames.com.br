Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook,
           Rails.application.config.facebook_app_id,
           Rails.application.config.facebook_app_secret,
           :iframe => true,
           :scope  => 'publish_stream,offline_access,email'
end