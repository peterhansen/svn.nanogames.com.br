require 'faye'
require 'logger'

LOGGER = Logger.new STDOUT

faye_server = Faye::RackAdapter.new(:mount => '/faye', :timeout => 45)

faye_server.bind(:handshake) do |client_id|
  LOGGER.add(Logger::INFO, "Client #{client_id} connected.")
end

faye_server.bind(:subscribe) do |client_id, channel|
  LOGGER.add(Logger::INFO, "Client #{client_id} subscribed to channel #{channel}.")
end

faye_server.bind(:publish) do |client_id, channel, message|
  LOGGER.add(Logger::INFO, "Client #{client_id} published message #{message} to channel #{channel}.")
end

faye_server.listen(9292)