class CustomLogger < Rails::Rack::Logger
  def initialize(app, opts = {})
    @app = app
    @opts = opts
    @opts[:silenced] ||= []

    logger = Logger.new($stdout)
    logger.level = Logger::ERROR
    ActiveRecord::Base.logger = logger
  end

  def call(env)
    if env['X-SILENCE-LOGGER'] || matches(@opts[:silenced], env['PATH_INFO'])
      Rails.logger.info env['PATH_INFO']
      Rails.logger.silence do
        @app.call(env)
      end
    else
      super(env)
    end
  end

  def matches(array, value)
    array.each do |string|
      if value.match string
        return true
      end
    end

    false
  end
end