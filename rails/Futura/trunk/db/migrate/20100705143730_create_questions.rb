class CreateQuestions < ActiveRecord::Migration
  def self.up
    create_table :questions do |t|
      t.string :title
      t.integer :answer_id
      t.integer :theme_id
      t.string :correct
      t.timestamps
    end
  end

  def self.down
    drop_table :questions
  end
end
