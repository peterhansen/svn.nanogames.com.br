class AddSponsorColumnToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :sponsor_id, :integer
  end

  def self.down
  end
end
