class AddColumnToStaticDataGame < ActiveRecord::Migration
  def self.up
    add_column :static_data_games, :max_online_users, :integer, :default => 100
  end

  def self.down
    remove_column :static_data_games, :max_online_users
  end
end
