class AddMyGameColumnToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :my_game_id, :integer
  end

  def self.down
    remove_column :users, :my_game_id
  end
end
