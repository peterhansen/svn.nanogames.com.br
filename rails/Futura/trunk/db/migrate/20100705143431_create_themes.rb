class CreateThemes < ActiveRecord::Migration
  def self.up
    create_table :themes do |t|
      t.integer :parent_id, :children_count, :ancestors_count, :descendants_count
      t.boolean :hidden
      t.string :name, :description, :name_with_level
      t.integer :position
      t.timestamps
    end
  end

  def self.down
    drop_table :themes
  end
end
