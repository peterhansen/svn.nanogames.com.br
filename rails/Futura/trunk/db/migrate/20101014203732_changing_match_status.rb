class ChangingMatchStatus < ActiveRecord::Migration
  def self.up
    rename_column :matches, :active, :status
    change_column :matches, :status, :integer, :default => 0
  end

  def self.down
    change_column :matches, :status, :boolean, :defaul => true
    rename_column :matches, :status, :active
  end
end
