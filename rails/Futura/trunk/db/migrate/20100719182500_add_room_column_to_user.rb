class AddRoomColumnToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :room_id, :integer
  end

  def self.down
    remove_column :users, :room_id
  end
end
