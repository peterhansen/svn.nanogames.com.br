class CreateStaticDataGames < ActiveRecord::Migration
  def self.up
    create_table :static_data_games do |t|
      t.boolean :suggest_question_active

      t.timestamps
    end
    StaticDataGame.create(:suggest_question_active => true)
  end

  def self.down
    drop_table :static_data_games
  end
end
