class AddDifficultyIdToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :difficulty_id, :integer
  end

  def self.down
    remove_column :games, :difficulty_id
  end
end
