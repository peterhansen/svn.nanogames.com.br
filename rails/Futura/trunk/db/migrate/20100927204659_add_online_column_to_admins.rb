class AddOnlineColumnToAdmins < ActiveRecord::Migration
  def self.up
    add_column :admins, :online, :boolean
  end

  def self.down
    remove_column :admins, :online
  end
end
