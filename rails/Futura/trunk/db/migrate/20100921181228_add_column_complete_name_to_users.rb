class AddColumnCompleteNameToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :complete_name, :string
  end

  def self.down
    remove_column :users, :complete_name
  end
end
