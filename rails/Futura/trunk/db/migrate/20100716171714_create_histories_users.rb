class CreateHistoriesUsers < ActiveRecord::Migration
  def self.up
    create_table "histories_users", :id => false do |t|
      t.integer :user_id
      t.integer :history_id
    end
  end

  def self.down
  end
end
