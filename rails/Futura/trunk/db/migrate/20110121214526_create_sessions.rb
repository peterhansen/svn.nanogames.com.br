class CreateSessions < ActiveRecord::Migration
  def self.up
    create_table :sessions do |t|
      t.string :sessid
      t.string :data

      t.timestamps
    end
    add_index :sessions, ["sessid"], :name => 'sessid'
  end

  def self.down
    drop_table :sessions
  end
end
