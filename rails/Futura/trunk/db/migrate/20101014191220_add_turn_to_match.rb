class AddTurnToMatch < ActiveRecord::Migration
  def self.up
    add_column :matches, :turn, :integer, :default => 0
  end

  def self.down
    remove_column :matches, :turn
  end
end
