class AddStartedAtToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :started_at, :time
  end

  def self.down
    remove_column :games, :started_atp
  end
end
