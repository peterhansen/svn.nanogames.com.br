class ChangeTurnsTableToCicles < ActiveRecord::Migration
  def self.up
    rename_table :turns, :cicles
  end

  def self.down
    rename_table :cicles, :turn
  end
end
