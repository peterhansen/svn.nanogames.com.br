class AddQuestionsSkippedToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :questions_skipped, :integer
  end

  def self.down
    remove_column :users, :questions_skipped
  end
end
