class CreateOrkutProfiles < ActiveRecord::Migration
  def self.up
    create_table :orkut_profiles do |t|
      t.integer :user_id
      t.string :profile_id
      t.string :name
      t.string :thumb_url
      t.integer :status, :default => 1
      t.boolean :selected, :default => false

      t.timestamps
    end
  end

  def self.down
    drop_table :orkut_profiles
  end
end
