class AddDescriptionToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :description, :string, :null => false, :default => ''
  end

  def self.down
    remove_column :rooms, :description
  end
end
