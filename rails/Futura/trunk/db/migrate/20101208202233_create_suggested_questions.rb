class CreateSuggestedQuestions < ActiveRecord::Migration
  def self.up
    create_table :suggested_questions do |t|
      t.string :title
      t.string :answer_one
      t.string :answer_two
      t.string :answer_three
      t.string :answer_four
      t.integer :theme_id
      t.boolean :approved, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :suggested_questions
  end
end
