class AddShowMaritalStatusToUser < ActiveRecord::Migration
  def self.up
    rename_table  :matrials,          :marital_statuses
    rename_column :marital_statuses,  :status, :name
    rename_column :users,             :matrial_id, :marital_status_id
    
    add_column    :users,             :show_marital_status, :boolean, :default => false
  end

  def self.down
    remove_column :users, :show_marital_status
    
    rename_column :users, :marital_status_id, :matrial_id
    rename_column :marital_statuses, :name, :status
    rename_table  :marital_statuses, :matrials    
  end
end
