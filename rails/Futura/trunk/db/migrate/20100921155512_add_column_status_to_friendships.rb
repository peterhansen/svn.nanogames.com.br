class AddColumnStatusToFriendships < ActiveRecord::Migration
  def self.up
    add_column :friendships, :friendship_status_id, :integer, :default => 1
  end

  def self.down
    remove_column :friendships, :friendship_status_id
  end
end
