class AddKindToCicles < ActiveRecord::Migration
  def self.up
    add_column :cicles, :kind, :integer
  end

  def self.down
    remove_column :cicles, :kind
  end
end
