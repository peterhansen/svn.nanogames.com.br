class AddSkillToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :skill, :integer, :default => 500
  end

  def self.down
    remove_column :users, :skill
  end
end
