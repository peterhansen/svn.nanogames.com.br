class AddPatentToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :patent, :string, :default => "Calouro", :null => false
  end

  def self.down
    remove_column :users, :patent
  end
end
