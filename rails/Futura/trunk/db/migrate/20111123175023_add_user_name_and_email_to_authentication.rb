class AddUserNameAndEmailToAuthentication < ActiveRecord::Migration
  def self.up
    add_column :authentications, :username, :string
    add_column :authentications, :email, :string
  end

  def self.down
    remove_column :authentications, :email
    remove_column :authentications, :username
  end
end
