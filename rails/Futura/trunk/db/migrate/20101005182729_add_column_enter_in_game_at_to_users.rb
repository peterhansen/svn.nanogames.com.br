class AddColumnEnterInGameAtToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :enter_in_game_at, :datetime
  end

  def self.down
    add_column :users, :enter_in_game_at
  end
end
