class AddTimeToCicles < ActiveRecord::Migration
  def self.up
    add_column :cicles, :time, :integer
  end

  def self.down
    remove_column :cicles, :time
  end
end
