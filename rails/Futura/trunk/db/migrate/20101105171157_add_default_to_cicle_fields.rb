class AddDefaultToCicleFields < ActiveRecord::Migration
  def self.up
    change_column :cicles, :points, :integer, :default => 0
    change_column :cicles, :correct, :boolean, :default => false
    change_column :cicles, :choosed_answer, :string, :default => nil
  end

  def self.down
    change_column :cicles, :points, :integer
    change_column :cicles, :correct, :boolean
    change_column :cicles, :choosed_answer, :string
  end
end
