class RemoveOnlineFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :online
  end

  def self.down
    add_column :users, :online, :boolean, :default => false
  end
end
