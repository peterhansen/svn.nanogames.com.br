class AddCurrentSessionIdToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :current_session_id, :string
  end

  def self.down
    remove_column :users, :current_session_id
  end
end
