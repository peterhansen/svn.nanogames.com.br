class CreateChallenges < ActiveRecord::Migration
  def self.up
    create_table :challenges, :id => false do |t|
      t.column :user_id, :integer          # source of the relationship
      t.column :user_id_target, :integer   # target of the relationship
      t.column :status, :integer, :default => 1
      t.timestamp :challenged_at
    end
     execute "ALTER TABLE  `challenges` CHANGE  `challenged_at`  `challenged_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP"

  end

  def self.down
    drop_table :challenges
  end
end
