class CreateRoomsThemes < ActiveRecord::Migration
  def self.up
    create_table "rooms_themes", :id => false do |t|
      t.integer :room_id
      t.integer :theme_id
    end
  end

  def self.down
  end
end
