class AddTurnColumnToGame < ActiveRecord::Migration
  def self.up
    add_column :games, :turn, :integer, :defualt => 0
  end

  def self.down
    remove_column :games, :turn
  end
end
