class AddDefaultToParticipationRightAnswers < ActiveRecord::Migration
  def self.up
    change_column :participations, :right_answers, :integer, :default => 0
  end

  def self.down
    change_column :participations, :right_answers, :integer
  end
end
