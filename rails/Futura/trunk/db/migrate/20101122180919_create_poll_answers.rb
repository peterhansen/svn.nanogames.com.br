class CreatePollAnswers < ActiveRecord::Migration
  def self.up
    create_table :poll_answers, :force => true do |t|
      t.integer :poll_question_id
      t.text :content
      t.timestamps
    end
  end

  def self.down
    drop_table :poll_answers
  end
end
