class AddStateToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :state, :integer
  end

  def self.down
    remove_column :games, :state
  end
end
