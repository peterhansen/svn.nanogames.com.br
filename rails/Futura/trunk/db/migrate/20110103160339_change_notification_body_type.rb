class ChangeNotificationBodyType < ActiveRecord::Migration
  def self.up
    change_column :notifications, :body, :text, :limit => 512
  end

  def self.down
    change_column :notifications, :body, :string
  end
end
