class CreateGames < ActiveRecord::Migration
  def self.up
    create_table :games do |t|
      t.integer :room_id
      t.integer :user_ids
      t.boolean :started, :default=>false
      t.boolean :private, :default=>false
      t.integer :min_users, :default=>2
      t.integer :max_users, :default=>6
      t.string  :permited_users
      t.timestamps
    end
  end

  def self.down
    drop_table :games
  end
end
