class AddActiveToTurns < ActiveRecord::Migration
  def self.up
    add_column :turns, :active, :boolean, :default => true
  end

  def self.down
    remove_column :turns, :active
  end
end
