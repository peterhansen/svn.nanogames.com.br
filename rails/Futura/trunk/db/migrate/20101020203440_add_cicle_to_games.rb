class AddCicleToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :cicle, :integer
  end

  def self.down
    remove_column :games, :cicle
  end
end
