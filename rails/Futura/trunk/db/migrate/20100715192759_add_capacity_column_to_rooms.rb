class AddCapacityColumnToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :capacity, :integer, :default=>100
  end

  def self.down
  end
end
