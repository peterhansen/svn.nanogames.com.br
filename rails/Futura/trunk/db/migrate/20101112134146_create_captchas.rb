class CreateCaptchas < ActiveRecord::Migration
  def self.up
    create_table :captchas do |t|
      t.integer :orkut_id
      t.string :captcha_url
      t.string :captcha_token

      t.timestamps
    end
  end

  def self.down
    drop_table :captchas
  end
end
