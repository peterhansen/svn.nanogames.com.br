class AddOnlineToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :online, :boolean
  end

  def self.down
    remove_column :users, :online
  end
end
