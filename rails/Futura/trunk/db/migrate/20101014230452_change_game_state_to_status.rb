class ChangeGameStateToStatus < ActiveRecord::Migration
  def self.up
    rename_column :games, :state, :status
  end

  def self.down
    rename_column :games, :status, :state
  end
end
