# encoding: UTF-8

Admin.delete_all
Answer.delete_all
Authentication.delete_all
Cicle.delete_all
Country.delete_all
Friendship.delete_all
Game.delete_all
Gender.delete_all
MaritalStatus.delete_all
Participation.delete_all
Question.delete_all
Room.delete_all
State.delete_all
Theme.delete_all
User.delete_all

countries_file = File.open("db/files/countries.txt", 'r').readlines
states_file    = File.open("db/files/states.txt", 'r').readlines

countries_file.each do |line|
  Country.create(:name => line.split("\n")[0])
end

br = Country.find_by_name('Brasil')
states_file.each do |line|
  state = State.create(:name => line.split(',').first, :abv => line.split(',').last, :country => br)
end

MaritalStatus.create([{ :name=>'Namorando' }, { :name=>'Casado(a)' }, { :name=>'Solteiro(a)' }, { :name => 'Viúvo(a)' }])

Gender.create([{ :name => "Masculino" }, { :name => "Feminino" }])

Admin.create([{ :email => "admin@nanogames.com.br", :password => "senha123" }])

@users = [{ :email => "werther@nano.com", :nickname => "Patinho", :name =>"Tio", :post_name => "Beto" },
          { :email => "dimas@nano.com", :nickname => "Silas", :name =>"Dimas", :post_name => "Cyriaco" },
          { :email => "peter@nano.com", :nickname => "Tio Pedro", :name =>"Piteco", :post_name => "da Silva Sauro" },
          { :email => "monteiro@nano.com", :nickname => "Gaiteiro", :name =>"Monteiro", :post_name => "Cordeiro" },
          { :email => "yuri@nano.com", :nickname => "Yuri", :name =>"Yuri", :post_name => "Mello" }]

Theme.create([{ :name => "Futebol" }, { :name => "Outra coisa" }])
Theme.create({ :name => "Filho de alguma coisa", :parent => Theme.first })

Room.create({ :name => "Teste sala 1", :themes => Theme.all })

Answer.create([{ :a=>"Vasco", :b => "Fluminense", :c => "Flamengo", :d => "Botafogo" },
               { :a=>"Vasco", :b => "Fluminense", :c => "Flamengo", :d => "Botafogo" },
               { :a=>"Vasco", :b => "Fluminense", :c => "Flamengo", :d => "Botafogo" },
               { :a=>"Alessandro!", :b => "Fluminense", :c => "Flamengo", :d => "Botafogo" }])


answers = Answer.all
Question.create([{ :title => "Qual o melhor time do Brasil?", :correct => 'c', :theme => Theme.first, :answer => answers[0] },
                 { :title => "Quem é Leovegildo Lins da Gama?", :correct => 'b', :theme => Theme.first, :answer => answers[1] },
                 { :title => "Qual o time que mais cedeu jogadores à Seleção Brasileira em Copas do Mundo?", :correct => 'd', :theme => Theme.first, :answer => answers[2] },
                 { :title => "Quem não toca bem e não cruza bem?", :correct => 'a', :theme => Theme.first, :answer => answers[3] }])

Answer.create([{ :a=>"Vasco", :b => "Fluminense", :c => "Flamengo", :d => "Botafogo", :question => Question.all[0] },
               { :a=>"Tiririca", :b => "Júnior", :c => "Beckenbauer", :d => "Bozo", :question => Question.all[1] },
               { :a=>"XV de Jaú", :b => "Ferroviário", :c => "Ibis", :d => "Botafogo", :question => Question.all[2] },
               { :a=>"Alessandro!", :b => "Dimas", :c => "Peter", :d => "Beto", :question => Question.all[3] }])

@users.each do |user|
  User.create(
      user.merge({
                     :password           => 'senha123',
                     :born_day           => '01',
                     :born_month         => '01',
                     :born_year          => '1970',
                     :marital_status     => MaritalStatus.first,
                     :state              => State.first,
                     :country            => State.first.country,
                     :gender             => Gender.first,
                     :city               => 'Rio de Janeiro',
                     :agreed_terms       => true,
                     :receive_newsletter => true }
      )
  ).confirm!
end

Game.create :room                      => Room.first,
            :status                    => Game::ENDED,
            :participations_attributes => [{ :user   => User.first,
                                             :status => Participation::FINISHED,
                                             :hits   => 10,
                                             :score  => 100 },
                                           { :user   => User.last,
                                             :status => Participation::FINISHED,
                                             :hits   => 15,
                                             :score  => 200 }]

StaticDataGame.create(suggest_question_active: true)