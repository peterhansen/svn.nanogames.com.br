# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111123175023) do

  create_table "admins", :force => true do |t|
    t.string   "email",                               :default => "",    :null => false
    t.string   "encrypted_password",   :limit => 128, :default => "",    :null => false
    t.string   "password_salt",                       :default => "",    :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "blocked",                             :default => false
    t.boolean  "online"
  end

  add_index "admins", ["confirmation_token"], :name => "index_admins_on_confirmation_token", :unique => true
  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "answers", :force => true do |t|
    t.integer  "question_id"
    t.string   "a"
    t.string   "b"
    t.string   "c"
    t.string   "d"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "answers", ["question_id"], :name => "index_answers_on_question_id"

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.string   "oauth_secret"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "email"
  end

  add_index "authentications", ["provider", "uid"], :name => "provider_uid_index"
  add_index "authentications", ["user_id"], :name => "user_id_index"

  create_table "captchas", :force => true do |t|
    t.integer  "orkut_id"
    t.string   "captcha_url"
    t.string   "captcha_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "captchas", ["orkut_id"], :name => "index_captchas_on_orkut_id"

  create_table "challenges", :id => false, :force => true do |t|
    t.integer  "user_id"
    t.integer  "user_id_target"
    t.integer  "status",         :default => 1
    t.datetime "challenged_at"
  end

  add_index "challenges", ["status"], :name => "index_challenges_on_status"
  add_index "challenges", ["user_id"], :name => "index_challenges_on_user_id"
  add_index "challenges", ["user_id_target"], :name => "index_challenges_on_user_id_target"

  create_table "cicles", :force => true do |t|
    t.integer  "game_id"
    t.integer  "user_id"
    t.integer  "points",         :default => 0
    t.boolean  "correct",        :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",         :default => true
    t.integer  "question_id"
    t.string   "choosed_answer"
    t.integer  "time",           :default => 0
    t.integer  "kind"
  end

  add_index "cicles", ["active"], :name => "index_cicles_on_active"
  add_index "cicles", ["game_id"], :name => "index_cicles_on_game_id"
  add_index "cicles", ["question_id"], :name => "index_cicles_on_question_id"
  add_index "cicles", ["user_id"], :name => "index_cicles_on_user_id"

  create_table "consumer_tokens", :force => true do |t|
    t.integer  "user_id"
    t.string   "type",             :limit => 30
    t.string   "token"
    t.string   "secret"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "orkut_id"
    t.string   "return_http_body"
    t.string   "request_params"
  end

  add_index "consumer_tokens", ["token"], :name => "index_consumer_tokens_on_token", :unique => true
  add_index "consumer_tokens", ["user_id"], :name => "index_consumer_tokens_on_user_id"

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "friendship_statuses", :force => true do |t|
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "friendship_status_id", :default => 1
  end

  add_index "friendships", ["friend_id"], :name => "index_friendships_on_friend_id"
  add_index "friendships", ["friendship_status_id"], :name => "index_friendships_on_friendship_status_id"
  add_index "friendships", ["user_id", "friend_id"], :name => "index_friendships_on_user_id_and_friend_id"

  create_table "games", :force => true do |t|
    t.integer  "room_id"
    t.integer  "user_ids"
    t.boolean  "started",          :default => false
    t.boolean  "private",          :default => false
    t.integer  "min_users",        :default => 2
    t.integer  "max_users",        :default => 6
    t.string   "permited_users"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "difficulty_id"
    t.integer  "turn",             :default => 0
    t.integer  "identifier"
    t.integer  "status"
    t.datetime "started_at"
    t.integer  "cicle"
    t.boolean  "quick_start",      :default => false
    t.integer  "previous_game_id"
  end

  add_index "games", ["cicle"], :name => "index_games_on_cicle"
  add_index "games", ["difficulty_id"], :name => "index_games_on_difficulty_id"
  add_index "games", ["private"], :name => "index_games_on_private"
  add_index "games", ["room_id"], :name => "index_games_on_room_id"
  add_index "games", ["started"], :name => "index_games_on_started"
  add_index "games", ["status"], :name => "index_games_on_status"

  create_table "games_questions", :id => false, :force => true do |t|
    t.integer "game_id"
    t.integer "question_id"
  end

  add_index "games_questions", ["game_id", "question_id"], :name => "index_games_questions_on_game_id_and_question_id"
  add_index "games_questions", ["game_id"], :name => "index_games_questions_on_game_id"
  add_index "games_questions", ["question_id"], :name => "index_games_questions_on_question_id"

  create_table "genders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "histories", :force => true do |t|
    t.boolean  "started",          :default => false
    t.boolean  "private",          :default => false
    t.string   "password"
    t.integer  "min_users",        :default => 2
    t.integer  "max_users",        :default => 6
    t.datetime "game_created_at"
    t.datetime "game_finished_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "histories_rooms", :id => false, :force => true do |t|
    t.integer "room_id"
    t.integer "history_id"
  end

  create_table "histories_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "history_id"
  end

  create_table "invitations", :force => true do |t|
    t.integer  "user_id"
    t.string   "friend_email"
    t.string   "token"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invitations", ["status"], :name => "index_invitations_on_status"
  add_index "invitations", ["user_id"], :name => "index_invitations_on_user_id"

  create_table "marital_statuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "summary",    :limit => 50
    t.boolean  "active",                   :default => true
  end

  add_index "news", ["active"], :name => "index_news_on_active"

  create_table "newsletter_contacts", :force => true do |t|
    t.string   "name"
    t.string   "post_name"
    t.string   "email"
    t.string   "city"
    t.integer  "state_id"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", :force => true do |t|
    t.text     "body"
    t.boolean  "active",     :default => true
    t.boolean  "to_all"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
  end

  add_index "notifications", ["active"], :name => "index_notifications_on_active"
  add_index "notifications", ["to_all"], :name => "index_notifications_on_to_all"

  create_table "notifications_users", :id => false, :force => true do |t|
    t.integer "notification_id"
    t.integer "user_id"
  end

  add_index "notifications_users", ["notification_id", "user_id"], :name => "index_notifications_users_on_notification_id_and_user_id"
  add_index "notifications_users", ["notification_id"], :name => "index_notifications_users_on_notification_id"
  add_index "notifications_users", ["user_id"], :name => "index_notifications_users_on_user_id"

  create_table "orkut_profiles", :force => true do |t|
    t.integer  "user_id"
    t.string   "profile_id"
    t.string   "name"
    t.string   "thumb_url"
    t.integer  "status",     :default => 1
    t.boolean  "selected",   :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "orkut_profiles", ["profile_id"], :name => "index_orkut_profiles_on_profile_id"
  add_index "orkut_profiles", ["status"], :name => "index_orkut_profiles_on_status"
  add_index "orkut_profiles", ["user_id"], :name => "index_orkut_profiles_on_user_id"

  create_table "orkuts", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "return_http_body"
    t.text     "request_params"
  end

  create_table "participations", :force => true do |t|
    t.integer  "points"
    t.integer  "hits",             :default => 0
    t.integer  "user_id"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",           :default => 0
    t.integer  "turn",             :default => 0
    t.boolean  "ready",            :default => false
    t.integer  "score",            :default => 0
    t.integer  "misses",           :default => 0
    t.integer  "color"
    t.datetime "enter_in_game_at"
  end

  add_index "participations", ["game_id"], :name => "index_participations_on_game_id"
  add_index "participations", ["ready"], :name => "index_participations_on_ready"
  add_index "participations", ["status"], :name => "index_participations_on_status"
  add_index "participations", ["user_id"], :name => "index_participations_on_user_id"

  create_table "poll_answers", :force => true do |t|
    t.integer  "poll_question_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "count",            :default => 0
  end

  add_index "poll_answers", ["poll_question_id"], :name => "index_poll_answers_on_poll_question_id"

  create_table "poll_questions", :force => true do |t|
    t.integer  "poll_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "poll_questions", ["poll_id"], :name => "index_poll_questions_on_poll_id"

  create_table "polls", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "active",       :default => false
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "notification"
  end

  add_index "polls", ["active"], :name => "index_polls_on_active"
  add_index "polls", ["notification"], :name => "index_polls_on_notification"

  create_table "polls_users", :id => false, :force => true do |t|
    t.integer "poll_id"
    t.integer "user_id"
  end

  add_index "polls_users", ["poll_id", "user_id"], :name => "index_polls_users_on_poll_id_and_user_id"
  add_index "polls_users", ["poll_id"], :name => "index_polls_users_on_poll_id"
  add_index "polls_users", ["user_id"], :name => "index_polls_users_on_user_id"

  create_table "questions", :force => true do |t|
    t.string   "title"
    t.integer  "answer_id"
    t.integer  "theme_id"
    t.string   "correct"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "questions", ["answer_id"], :name => "index_questions_on_answer_id"
  add_index "questions", ["theme_id"], :name => "index_questions_on_theme_id"

  create_table "rooms", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "capacity",                :default => 100
    t.integer  "sponsor_id"
    t.integer  "game_ids"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.string   "description",             :default => "",  :null => false
  end

  create_table "rooms_themes", :id => false, :force => true do |t|
    t.integer "room_id"
    t.integer "theme_id"
  end

  add_index "rooms_themes", ["room_id", "theme_id"], :name => "index_rooms_themes_on_room_id_and_theme_id"
  add_index "rooms_themes", ["room_id"], :name => "index_rooms_themes_on_room_id"
  add_index "rooms_themes", ["theme_id"], :name => "index_rooms_themes_on_theme_id"

  create_table "sessions", :force => true do |t|
    t.string   "sessid"
    t.string   "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["sessid"], :name => "sessid"

  create_table "skill_variations", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsors", :force => true do |t|
    t.integer  "room_ids"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.string   "abv"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "static_data_games", :force => true do |t|
    t.boolean  "suggest_question_active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "flash_version",           :default => "0.1"
    t.integer  "max_online_users",        :default => 100
  end

  create_table "suggested_questions", :force => true do |t|
    t.string   "title"
    t.string   "answer_one"
    t.string   "answer_two"
    t.string   "answer_three"
    t.string   "answer_four"
    t.integer  "theme_id"
    t.boolean  "approved",     :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "correct"
  end

  add_index "suggested_questions", ["approved"], :name => "index_suggested_questions_on_approved"
  add_index "suggested_questions", ["theme_id"], :name => "index_suggested_questions_on_theme_id"
  add_index "suggested_questions", ["user_id"], :name => "index_suggested_questions_on_user_id"

  create_table "themes", :force => true do |t|
    t.integer  "parent_id"
    t.integer  "children_count"
    t.integer  "ancestors_count"
    t.integer  "descendants_count"
    t.boolean  "hidden"
    t.string   "name"
    t.string   "description"
    t.string   "name_with_level"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "requested_times",   :default => 0
  end

  add_index "themes", ["parent_id"], :name => "index_themes_on_parent_id"

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "",        :null => false
    t.string   "encrypted_password",    :limit => 128, :default => "",        :null => false
    t.string   "password_salt",                        :default => "",        :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                      :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "blocked",                              :default => false
    t.integer  "game_id"
    t.integer  "room_id"
    t.string   "name"
    t.string   "post_name"
    t.integer  "marital_status_id"
    t.string   "city"
    t.string   "about"
    t.integer  "state_id"
    t.integer  "country_id"
    t.integer  "gender_id"
    t.string   "born_day"
    t.string   "born_month"
    t.string   "born_year"
    t.string   "nickname"
    t.boolean  "show_email"
    t.boolean  "show_age"
    t.boolean  "receive_newsletter"
    t.boolean  "agreed_terms"
    t.boolean  "show_marital_status",                  :default => false
    t.string   "room_user_id",                         :default => "0"
    t.string   "game_player_id",                       :default => "0"
    t.string   "color"
    t.integer  "turn"
    t.string   "orkut_id"
    t.string   "complete_name"
    t.integer  "my_game_id"
    t.integer  "skill",                                :default => 500
    t.string   "digest_avatar"
    t.integer  "score",                                :default => 0
    t.integer  "inviter_id"
    t.string   "current_session_id"
    t.datetime "enter_in_game_at"
    t.integer  "skill_month_variation",                :default => 0
    t.integer  "skill_week_variation",                 :default => 0
    t.integer  "questions_skipped"
    t.string   "patent",                               :default => "Calouro", :null => false
    t.integer  "users_count",                          :default => 0
    t.boolean  "online"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["current_session_id"], :name => "index_users_on_current_session_id", :length => {"current_session_id"=>40}
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["inviter_id"], :name => "index_users_on_inviter_id"
  add_index "users", ["last_sign_in_at"], :name => "index_users_on_last_sign_in_at"
  add_index "users", ["my_game_id"], :name => "index_users_on_my_game_id"
  add_index "users", ["name"], :name => "index_users_on_name", :length => {"name"=>30}
  add_index "users", ["nickname"], :name => "index_users_on_nickname", :length => {"nickname"=>30}
  add_index "users", ["orkut_id"], :name => "index_users_on_orkut_id", :length => {"orkut_id"=>40}
  add_index "users", ["post_name"], :name => "index_users_on_post_name", :length => {"post_name"=>30}
  add_index "users", ["questions_skipped"], :name => "index_users_on_questions_skipped"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["score"], :name => "index_users_on_score"
  add_index "users", ["skill"], :name => "index_users_on_skill"
  add_index "users", ["skill_month_variation"], :name => "index_users_on_skill_month_variation"
  add_index "users", ["skill_week_variation"], :name => "index_users_on_skill_week_variation"

end
