module Categorizable
  extend ActiveSupport::Concern

  included do
    # attr_accessor :parent, :children

    belongs_to :parent, :class_name => self.to_s
    has_many :children, :class_name => self.to_s, :foreign_key => 'parent_id'
  end

  module InstanceMethods
    def ancestors_count
      ancestors.size
    end

    def ancestors
      node, nodes = self, []
      nodes << node = node.parent while node.parent
      nodes
    end

    def name_with_level
      "#{">" * ancestors_count} #{name}"
    end

    def descendants
      descendants = []
      self.children.each { |child|
        descendants += [child] unless child.hidden?
        descendants += child.descendants
      } unless self.children.empty?
      descendants 
    end
    
    def descendants_count
      descendants.size
    end

    def children_count
      children.size
    end

    def save_questions
      if parent_id
        questions.each do |question|
          question.theme = parent
          question.save
        end
      else
        questions.each do |question|
          question.destroy
        end
      end
    end
  end
end