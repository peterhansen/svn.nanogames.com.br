class ExternalAuthenticationFailed < StandardError; end
class CustomerNotFound < StandardError; end

module OmniauthAuthenticatable
  extend ActiveSupport::Concern

  included do
    has_many :authentications,  :dependent => :destroy
    accepts_nested_attributes_for :authentications
    attr_accessible :authentications_attributes
  end

  module InstanceMethods
    def add_authentication(omniauth_hash)
      self.authentications.create({
        username: omniauth_hash['user_info']['name'],
        email: omniauth_hash['user_info']['email'],
        provider: omniauth_hash['provider'],
        uid: omniauth_hash['uid'],
        oauth_token: omniauth_hash['credentials']['token'],
        oauth_secret: omniauth_hash['credentials']['secret']
      })
    end

    def has_provider?(provider)
      self.authentications.inject(false) { |result, auth| result || (auth.provider == provider.to_s) }
    end

    def merge(user)
      user.authentications.each do |auth|
        auth.update_attributes :user => self
      end
    end
  end

  module ClassMethods
    def authenticate_omniauth(options)
      omniauth_hash, user_hash = options[:omniauth_hash], options[:user_hash]

      if omniauth_hash
        provider, uid = omniauth_hash['provider'], omniauth_hash['uid']
        authentication = Authentication.find_by_provider_and_uid( provider, uid )

        if authentication
          return authentication.user
        else
          raise ExternalAuthenticationFailed
        end
      elsif user_hash
        nickname, password = user_hash[:nickname], user_hash[:password]
        return User.authenticate(nickname, password)
      end
    end

    def create_with_omniauth(omniauth_hash)
      if user = User.find_by_email(omniauth_hash['user_info']['email'])
        user.add_authentication(omniauth_hash)
        return
      end
      
      user = User.create({
        nickname: omniauth_hash['user_info']['name'],
        name: omniauth_hash['user_info']['name'],
        post_name: 'not specified',
        born_day: '01',
        born_month: '01',
        born_year: '1970',
        city: 'not specified',
        agreed_terms: true,
        email: omniauth_hash['user_info']['email'],
        password: User.generate_password,
        authentications_attributes: [{
          username: omniauth_hash['user_info']['name'],
          email: omniauth_hash['user_info']['email'],
          provider: omniauth_hash['provider'],
          uid: omniauth_hash['uid'],
          oauth_token: omniauth_hash['credentials']['token'],
          oauth_secret: omniauth_hash['credentials']['secret']
        }]
      })

      user.confirm!

      while user.errors.on :nickname
        user.update_attributes :nickname => user.nickname + rand(9999).to_s
      end
    end
  end
end