class SessionsController < Devise::SessionsController
  prepend_view_path "app/views/devise"
  after_filter :set_session_id, :on => :create

  def new
    super
  end

  def create
    super
  end

  def set_session_id; end

  def update
    super
  end
end