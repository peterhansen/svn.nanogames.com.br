# encoding: UTF-8

class HomeController < ApplicationController
  before_filter :set_user_online_on_page, :only => [:news]
  before_filter :ensure_user_is_logged, :except => [:welcome]

  def index
    exit_room_and_game
    redirect_to "/orkut_index" and return if session[:ouid]
    redirect_to '/rooms' if logged_user
  end

  def news
    @news = New.where(active: true ).order("created_at DESC").paginate(:page => params[:page], :per_page => 4)
  end

  def polling
    @time = Time.now
  end

  def get_time
    render :text => Time.now
  end

  def suggest_question
    @suggested_question = SuggestedQuestion.new
  end

  def submit_question
    @suggested_question = logged_user.suggest_question(params[:suggest_question])

    if @suggested_question.errors.empty?
      flash[:message] = "Pergunta submetida com sucesso!"
      Emailer.delay.deliver_suggest_question_to_admin(@suggested_question.id, request.host_with_port)
      redirect_to :action => 'suggest_question_answer'
    else
      flash[:message] = "Ocorreram um ou mais erros na submissão de pergunta."
      respond_to do |format|
        format.xml { render :xml => @suggested_questions.errors }
        format.html { render :action => 'suggest_question' }
      end
    end
  end

  def welcome
    redirect_to :rooms and return if logged_user
    render :index
  end

  def suggest_question_answer; end
end