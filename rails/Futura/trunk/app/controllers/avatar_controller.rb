class AvatarController < ApplicationController
  layout 'avatar'
  
  def index; end
  def new; end

  def temp_image
    render :new and return if params[:avatar].blank?

    logged_user.avatar = params[:avatar][:image]
    logged_user.save
    redirect_to :action=>'image'
  end

  def image
    @image_url = logged_user.avatar.url(:to_crop)
  end

  def crop
    x = params[:x1].to_i
    y = params[:y1].to_i
    width = params[:width].to_i
    height = params[:height].to_i

    image = Magick::Image.read(logged_user.avatar.path(:to_crop)).first
    
    cropped_image = image.crop!(x, y, width, height)
    cropped_image.write("/tmp/#{logged_user.avatar.original_filename}")
    image_file = File.open("/tmp/#{logged_user.avatar.original_filename}", "r")
    old_name = "/tmp/#{logged_user.avatar.original_filename}"
    logged_user.avatar = image_file
    image_file.close
    system("rm #{old_name}")
    logged_user.save

    render :text => "<script>top.window.location='/profiles'</script>"  
  end
end