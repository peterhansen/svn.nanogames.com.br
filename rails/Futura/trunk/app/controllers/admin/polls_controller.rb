# encoding: UTF-8

class Admin::PollsController < Admin::AdminController
  def index
    @polls = Poll.all.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @active_poll = Poll.active
    @poll = Poll.new
    20.times do
      question = @poll.poll_questions.build
      4.times do
        question.poll_answers.build
      end
    end
  end

  def create
    @poll = Poll.new(params[:poll])

    if ( active = Poll.active ) and @poll.active
      active.update_attribute(:active, false)

      if notification = Notification.find_by_id(active.notification)
        notification.update_attribute(:active, false)
      end
    end

    if @poll.save
      notification = Notification.create(:body => "Nova Enquete disponível. <a href='/polls/#{@poll.id}'>Clique aqui para respondê-la.</a>")
      notification.update_attribute(:to_all, true)
      @poll.update_attribute(:notification, notification.id)

      redirect_to :action => :index
    else
      render :action => :new
    end
  end

  def edit
    @active_poll = Poll.active
    @poll = Poll.find(params[:id])
    @qtd = @poll.poll_questions.size

    (20 - @poll.poll_questions.size).times do
      question = @poll.poll_questions.build
      4.times do
        question.poll_answers.build
      end
    end
  end

  def update
    @poll = Poll.find(params[:id])

    Poll.active.update_attribute(:active, false) if (params[:poll][:active] == "1") and Poll.active.present?

    if @poll.update_attributes(params[:poll])

      if @poll.active?
        Notification.find(@poll.notification).update_attribute(:active, true)
      else
        Notification.find(@poll.notification).update_attribute(:active, false)
      end

      redirect_to :action => :index
    else
      render :action => :new
    end
  end

  def destroy
    if poll = Poll.find(params[:id])
      if notification = Notification.find_by_id(poll.notification)
        notification.update_attribute(:active, false)
      end
      poll.destroy
    end
    redirect_to :action => :index
  end

  def report
    @poll = Poll.find(params[:id])
  end

  def update_active
    if active = Poll.active
      active.update_attribute(:active, false)
      if notification = Notification.find_by_id(active.notification)
        notification.update_attribute(:active, false)
      end
    end

    if params[:active]
      poll = Poll.find(params[:id].split("_")[1])
      poll.update_attribute(:active, active_parameter(params[:active]))

      if notification = Notification.find_by_id(poll.notification)
        notification.update_attribute(:active, active_parameter(params[:active]))
      end
      render :text => 'success'
    else
      render :text => 'failure'
    end
  end

private
  def active_parameter(param)
    param == "true" ? false : true
  end
end
