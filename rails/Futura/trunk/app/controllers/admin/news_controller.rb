# encoding: UTF-8

class Admin::NewsController < Admin::AdminController
  before_filter :authenticate_admin!
  def index
    @news = New.find_all_by_active(params[:active] == "true" ? true : false, {:order => "created_at DESC"}).paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @news = New.new
  end

  def create
    @news = New.new(params[:new])

    if @news.save
      flash[:result] = I18n.t( :saved )
      flash[:class] = "sucesso"
      redirect_to :action => 'index', :active => @news.active.to_s
    else
      flash[:result] = I18n.t( :not_saved )
      flash[:class] = "erro"
      render :action => 'new'
    end
  end

  def show; end
    
  def edit
    @news = New.find(params[:id])
  end
  
  def update
    @news = New.find(params[:id])
    if @news.update_attributes(params[:new])
      flash[:result] = I18n.t( :updated )
      flash[:class] = "sucesso"
      redirect_to :action => 'index', :active => @news.active
    else
      flash[:result] = I18n.t( :not_updated )
      flash[:class] = "erro"
      render :action=>'edit'
    end
  end
  
  def destroy
    @news = New.find(params[:id])
    active = @news.active
    if @news.destroy
      flash[:result] = I18n.t( :deleted_with_success )
      flash[:class] = "sucesso"
    else
      flash[:result] = I18n.t( :not_deleted )
      flash[:class] ="erro"
    end
    redirect_to :action => 'index', :active => active
  end
end
