# encoding: UTF-8

class Admin::NotificationsController < Admin::AdminController
  include ERB::Util
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::TextHelper
  
  def index
      @notifications = Notification.find_all_by_active_and_to_all(params[:active] == "true" ? true : false, true, {:order => "created_at DESC"}).paginate(:page => params[:page], :per_page => 10)
  end
  
  def show
    @notification = Notification.find(params[:id])
  end
  
  def new
    @notification = Notification.new
  end
  
  def create
    @notification = Notification.new(params[:notification])
    @notification.to_all = true
    if @notification.save
      flash[:result] = I18n.t( :saved )
      flash[:class] = "sucesso"
      redirect_to :action => 'index', :active => @notification.active.to_s
    else
      flash[:result] = I18n.t( :not_saved )
      flash[:class] = "erro"
      respond_to do |format|
        format.xml { render :xml=>@notification.errors }
        format.html { render :action=>'new' }
      end
    end
  end
  
  def edit
    @notification = Notification.find(params[:id])
  end
  
  def update
    @notification = Notification.find(params[:id])
    if @notification.update_attributes(params[:notification])
      flash[:result] = I18n.t( :updated )
      flash[:class] = "sucesso"
      redirect_to :action => 'index', :active => @notification.active
    else
      flash[:result] = I18n.t( :not_updated )
      flash[:class] = "erro"
      respond_to do |format|
        format.xml { render :xml=>@notification.errors }
        format.html { render :action=>'edit' }
      end
    end
  end
  
  def destroy
    @notification = Notification.find(params[:id])
    active = @notification.active
    if @notification.destroy
      flash[:result] = I18n.t( :deleted_with_success )
      flash[:class] = "sucesso"
    else
      flash[:result] = I18n.t( :not_deleted )
      flash[:class] ="erro"
    end
    redirect_to :action => 'index', :active => active
  end
  
end
