class Admin::AdminController < ApplicationController
  before_filter :sign_admin
  layout "admin"
  def index
    redirect_to :controller=>'admin/questions'
  end
  def make_theme_box( id )
    roots = Theme.find(:all, :conditions=>{:parent_id=>nil})
    roots_array = []
    roots.each do |root|
      if ( ( root.id != id ) or (id == "not_theme"))
        roots_array << root
      end
      root.descendants.each do |children|
        if ( ( children.id != id ) or (id == "not_theme"))
          roots_array << children
        end
      end
    end
    return roots_array
  end
  
  def sign_admin
    if current_admin and Admin.find_by_id(current_admin.id).nil?
      warden.logout(:admin)
    end
    authenticate_admin!
  end
  
  
end
