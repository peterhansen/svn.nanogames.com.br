# encoding: UTF-8

class Admin::QuestionsController < Admin::AdminController
 
  def index
    if params[:search].blank?
      @questions = Question.all.paginate( :per_page=>10, :page=>params[:page] )
    else
      @questions = Question.where("title like ?", "%#{params[:search]}%").paginate( :per_page=>10, :page=>params[:page] )
    end
    
  end

  def new
    if Theme.count > 0
      @question = Question.new
      @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}
    else
      flash[:class] = "padrao"
      flash[:result] = I18n.t(:atention)
      flash[:description] = I18n.t(:you_have_no_theme)
    end
  end

  def edit
    if Theme.count > 0
      @question = Question.find( params[:id] )
      @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}
    else
      flash[:class] = "padrao"
      flash[:result] = I18n.t(:atention)
      flash[:description] = I18n.t(:you_have_no_theme)
    end
  end

  def create
    @question = Question.new( params[:question] )
    @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}
    @answer = Answer.new( params[:answer] )
    if @answer.save
      @question.answer = @answer
      if @question.save
        flash[:result] = I18n.t( :saved )
        flash[:class] = "sucesso"
        redirect_to :action=>'index'
      else
        flash[:result] = I18n.t( :not_saved )
        flash[:class] = "erro"
        respond_to do |format|
          format.xml { render :xml=>@question.errors }
          format.html { render :action=>'new' }
        end
      end
    else
      flash[:result] = I18n.t( :not_saved )
      flash[:class] = "erro"
      respond_to do |format|
        format.xml { render :xml=>@answer.errors }
        format.html { render :action=>'new' }
      end
    end
  end

  def update
    @question = Question.find( params[:id] )
    @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}

    @answer = Answer.find( @question.answer_id )
    if @answer.update_attributes( params[:answer] )
      if @question.update_attributes( params[:question] )
        flash[:result] = I18n.t( :updated )
        flash[:class] = "sucesso"
        redirect_to :action=>"index"
      else
        flash[:result] = I18n.t( :not_updated )
        flash[:class] = "erro"
        respond_to do |format|
          format.xml { render :xml=>@question.errors }
          format.html { render :action=>'edit' }
        end
      end
    else
      flash[:result] = I18n.t( :not_updated )
      respond_to do |format|
        format.xml { render :xml=>@answer.errors }
        format.html { render :action=>'edit' }
      end
    end
  end

  def destroy
    question = Question.find( params[:id] )
    
    if question.destroy
      flash[:result] = I18n.t( :deleted_with_success )
      flash[:class]  = "sucesso"
    else
      flash[:result] = I18n.t( :not_deleted )
      flash[:class]  ="erro"
    end
    
    redirect_to :action => "index"
  end

  def import_csv
    if Theme.count > 0
      @select_options = make_theme_box( "not_theme" ).collect{ |theme| [theme.name_with_level, theme.id] }
      @select_options_doc = make_theme_box( "not_theme" ).collect{ |theme| [theme.name_with_level, theme.id] }
    else
      flash[:class] = "padrao"
      flash[:result] = I18n.t(:invalid_operation)
      flash[:description] = flash[:description] = I18n.t(:you_have_no_theme)
    end
  end

  def create_question_from_csv
    @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}
    file_to_import = params[:csv][:file].read
    if Question.import_from_csv(file_to_import, params[:csv][:theme]) 
      flash[:result] = I18n.t(:csv_imported)
      flash[:class] = "sucesso"
    else
      flash[:result] = "O arquivo precisa estar com o econding: 'UTF-8' e seguir a formatação dos campos."
      flash[:class] = "erro"
    end  
    redirect_to :action=>'index'
  end

  def create_question_from_txt
    @select_options_doc = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}
    file_to_import = params[:doc][:file].read


    imported_ok, total_imported = Question.import_from_text(file_to_import, params[:doc][:theme]) 
    if imported_ok
      flash[:result] = I18n.t(:doc_imported) + "#{total_imported} perguntas importadas"
      flash[:class] = "sucesso"
    else
      flash[:result] = "O arquivo não possui a formatação correta. Por favor confira se todas as perguntas e respostas possuem \";\" como separador, e não hífen."
      flash[:class] = "erro"
    end  
    redirect_to :action=>'index'
  end
end
