# encoding: UTF-8

class ContactsController < ApplicationController
  before_filter :validate_mail_form, :only => :create
  def index
    redirect_to :action => :new
  end
  
  def success
  end
  
  def new
    @states = State.all.collect {|s| [s.abv, s.id]}
    @selected_state = State.find_by_name("Rio de Janeiro").id
    @countries = Country.all.collect {|s| [s.name, s.id]}
    @selected_country = Country.find_by_name("Brasil").id
  end
  
  def create
    if params[:receive_news_letter]
      if newsletter_contact = NewsletterContact.find_by_email(params[:user_info][:email])
        newsletter_contact.update_attributes(params[:user_info])
      else
        newsletter_contact = NewsletterContact.new(params[:user_info])
        newsletter_contact.save
      end
    end
#    Delayed::Job.enqueue(BackgroundJob.new('Emailer', 'deliver_contact_us', params[:user_info], params[:message]))
    Emailer.delay.deliver_contact_us( params[:user_info], params[:message] )
    redirect_to :action => 'success'
  end
  
  protected
  def validate_mail_form
    flash[:message] = ''
    flash[:errors] = {}
    unless params[:user_info][:email] =~ /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i 
      flash[:message] += "<li>Email inválido.</li>"
      flash[:errors][:email] = 'user_info_email'
    end
    if params[:user_info][:name].empty?
      flash[:message] += "<li>Nome em branco.</li>"
      flash[:errors][:name] = 'user_info_name'
    elsif params[:user_info][:name] =~ /[0-9]/
      flash[:message] += "<li>Nome pode conter apenas caracteres.</li>"
      flash[:errors][:name] = 'user_info_name'
    end 
    if params[:user_info][:post_name] =~ /[0-9]/
      flash[:message] += "<li>Sobrenome pode conter apenas caracteres.</li>"
      flash[:errors][:post_name] = 'user_info_post_name'
    end
    if params[:message].empty? 
      flash[:message] += "<li>Mensagem em branco.</li>"
      flash[:errors][:message] = 'message'
    elsif params[:message].length >= 230
      flash[:message] += "<li>A mensagem pode ter até apenas 230 caracteres.</li>"
      flash[:errors][:message] = 'message'
    end
    unless flash[:message].empty?
      redirect_to :action => 'new'
    end
  end
end
