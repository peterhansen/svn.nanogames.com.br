# encoding: UTF-8

class ProfilesController < ApplicationController
  before_filter :ensure_user_is_logged
  before_filter :exit_room_and_game, :only => :index
  
  def index
    inviteds = logged_user.inviteds
    inviteds.blank? ? @inviteds = 0 : @inviteds = inviteds.size

    @poll = logged_user.unanswered_poll
  end
  
  def show
    @user = User.find(params[:id])
    render :layout => false
  end
  
  def add_friend
    logged_user.add_friend( User.find(params[:friend_id]) )
    expire_profiles_cache( params[:friend_id] )
     
    redirect_to :controller => 'players'
  end
  
  def update
    if params[:user][:password] == "" and params[:user][:password_confirmation] == ""
      user_params = {}
      params[:user].each do |key, value| 
        if key != "password"  and  key != "password_confirmation"
          user_params[key.to_sym] = value 
        end
      end
    else
      user_params = params[:user]
    end
    
    if logged_user.update_attributes(user_params)
      expire_profiles_cache()
      
      logged_user.complete_name = "#{params[:user][:name]} #{params[:user][:post_name]}" 
      logged_user.show_marital_status = (user_params["show_marital_status"] != "0")
      logged_user.show_email = (user_params["show_email"] != "0")
      logged_user.show_age = (user_params["show_age"] != "0")
      logged_user.save
      flash[:notice] = "Informações salvas com sucesso!"
      redirect_to :controller => 'profiles'
    else 
      flash[:error] = "Ocorreram alguns erros atualizando o seu perfil!"
      respond_to do |format|
        format.xml { render :xml => logged_user.errors }
        format.html { render :action => 'index' }
      end
    end
  end
  
  private
  
  def expire_profiles_cache( other_id = nil )
    expire_fragment "user_profile_#{ logged_user.id }" 
    expire_fragment "user_profile_#{ other_id }" if other_id
  end
  
end
