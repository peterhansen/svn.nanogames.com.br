# encoding: UTF-8

class SocialMediasController < ApplicationController
  before_filter :ensure_user_is_logged

  def index
    if session[ :registering ]
      user = User.find(session[:user_id])
    else
      user = logged_user
    end
    if user.config_orkut_id
      flash[:notice] = "Seus amigos foram importados do Orkut."

      if session[:callback_orkut]
        orkut_page = session[:callback_orkut]
        session[:callback_orkut] = nil
        redirect_to orkut_page
      elsif session[ :registering ]
        flash[:notice] = "Conta do orkut configurada."
        redirect_to :controller => 'futura_registrations', :action => 'email_success_message'
      else
        flash[:notice] = "Sua conta foi associada ao Orkut com sucesso. Agora você pode convidar seus amigos para jogar."
        redirect_to :controller => 'invitations', :action => 'orkut'
      end
    else
      flash[:notice] = "Um erro impediu que seu orkut fosse associado com sua conta no Futura CDF. Tente novamente mais tarde."
    end
  end

  def show
    redirect_to :controller => 'oauth_consumers', :action => 'google'
  end

  def add_friends_from_orkut
    friends = logged_user.orkut_friends

    params[:friends].each do |key, value|
      if value == 1
        #logged_user.add_friend(key)
      end
    end
  end

protected
  def registered(contacts)
    contacts_to_return = []
    for contact in contacts
      if contact.include?("emails")
        contact_for_array = User.find(:first, :conditions=>{:email=>contact["emails"][0]["value"]})
      else
        contact_for_array = nil
      end
      if not contact_for_array.nil?
        contacts_to_return << contact_for_array
      end
    end
    return contacts_to_return
  end
end

