class PlayersController < ApplicationController
  before_filter :ensure_user_is_logged
  before_filter :exit_room_and_game
  
  FRIENDS_PER_PAGE = 20

  def index
    redirect_to :action => 'friends'
  end

  def friends
    conditions = "nickname like '%#{params[:search][:nickname]}%'" if params[:search]

    set_challenges
    @users = PlayersController.sort_and_paginate_users( logged_user.friends.online.where(conditions), params[:show_selecteds], logged_user, params[ :page ] )
    @active_tab_friends = "aba_ativa"
  end

  def all
    conditions = "nickname like '%#{params[:search][:nickname]}%'" if params[:search]
    
    set_challenges
    @active_tab_players = "aba_ativa"
    @users = PlayersController.sort_and_paginate_users( User.online.where(conditions) - [logged_user], params[:show_selecteds], logged_user, params[ :page ] )
  end

private
  
  def set_challenges
    @challenges_made = Challenge.all( :conditions => [ "user_id_target = ? AND status = ?", logged_user, Challenge::INVITED ] )
  end
  
  ##
  # Ordena uma lista de jogadores primeiro pelo seu estado (livre/jogando), e então pelo apelido (case insensitive)
  # TODO Esse metodo devia estar em user.rb
  ##
  def self.sort_and_paginate_users( users, selected, logged_user, page = 1 )
    users ||= []

    # TODO extrair para User#<=>
    users.sort! do |a,b|
      if ( a.game.nil? == b.game.nil? )
        a.nickname.casecmp( b.nickname )
      else
        a.game ? 1 : -1
      end
    end
    
    if selected
      return users.select{|u| u.challenges_out_ids.include?(logged_user.id)}.paginate(:per_page => FRIENDS_PER_PAGE, :page => page )
    else  
      return users.paginate(:per_page => FRIENDS_PER_PAGE, :page => page )
    end
  end
  
end
