module Devise
  class RegistrationsController < ApplicationController
    prepend_before_filter :require_no_authentication, :only => [:new, :create]
    prepend_before_filter :authenticate_scope!, :only => [:edit, :update, :destroy]
    include Devise::Controllers::InternalHelpers

    def new
      build_resource
      session[:invitation_token] = params[:ft]
      render_with_scope :new
    end

    def create
      build_resource

      resource.complete_name = "#{resource.name} #{resource.post_name}"

      if session[:invitation_token]
        if inviter_user = User.find(session[:invitation_token])
          resource.inviter_id = inviter_user.id
          inviter_user.score  += 100
          inviter_user.save
        end
      end

      if resource.save
        resource.confirm_invite_from_orkut!(session[:ouid]) unless session[:invitation_token]
        if session[:callback_orkut]
          resource.confirm!
          session[:registering]       = true
          session[:user_id]           = resource.id
          resource.current_session_id = session[:session_id]
          resource.save
          redirect_to "/oauth_consumers/google"
        else
          resource.add_friend(inviter_user.id) if inviter_user
          set_flash_message :notice, :signed_up
          session[:registering] = true
          session[:user_id]     = resource.id
          redirect_to '/futura_registrations/social_media'
        end
      else
        render_with_scope :new
      end
    end

    def edit
      render_with_scope :edit
    end

    def update
      if self.resource.update_with_password(params[resource_name])
        set_flash_message :notice, :updated
        redirect_to after_sign_in_path_for(self.resource)
      else
        render_with_scope :edit
      end
    end

    def destroy

      self.resource.destroy
      set_flash_message :notice, :destroyed
      sign_out_and_redirect(self.resource)
    end

    protected

    def authenticate_scope!
      send(:"authenticate_#{resource_name}!")
      self.resource = send(:"current_#{resource_name}").dup
    end
  end
end