# encoding: UTF-8

class ChallengesController < ApplicationController
  def invite_challenge
    user = User.find(params[:user_id])
    game = logged_user.my_game
    if game and game.max_users > 2
      max_users = game.max_users - 1
    elsif game and game.max_users == 2
      max_users = 5
    elsif game and not game.private?
      max_users = game.max_users - 1
    else
      max_users = 5
    end

    if params[:ctrl] == "ranking"
      max_users = User.count
    end

    value = false
    logged_user.challenges_in << user
    value = true


    render :json => { :value      => value, :max_users => max_users,
                      :challenges => logged_user.challenges_in.size }
  end

  def remove_challenge
    user = User.find(params[:user_id])
    logged_user.challenges_in.delete(user)
    render :json => { :value => true, :challenges => logged_user.challenges_in.size }
  end

  def index
    @users        = logged_user.challenges_in
    @rooms        = Room.all(:order => 'name')
    @themes       = @rooms.first.themes.uniq.collect { |theme| theme.name }
    @difficulties = Difficulty.all
    render :layout => false
  end

  def index_ranking
    @users        = logged_user.challenges_in
    @rooms        = Room.all(:order => 'name')
    @themes       = @rooms.first.themes.collect { |theme| theme.name }
    @difficulties = Difficulty.all
    render :layout => false
  end

  def send_invites_ranking
    users = logged_user.challenges_in
    users.each do |user|
      logged_user.set_invited_challenge(user.id)

      room         = Room.find(params[:room_id])
      speed        = params[:difficulty_id] == 1 ? "normal" : "turbo"
      notification = Notification.create :body => 'body', :users => [user], :active => true
      url_game     = "/challenges/accept_challenge?room_id=#{params[:room_id]}&difficulty_id=#{params[:difficulty_id]}&user_from_id=#{logged_user.id}&notification_id=#{notification.id}"

      notification.update_attribute :body, "#{logged_user.profile_url_modal} está te desafiando! Sala #{room.name} - Velocidade #{speed}.\n<a href='#{url_game}'>Aceito!</a>"
    end
    redirect_to :controller => 'rankings', :action => 'all_time'
  end

  def send_invites
    users       = logged_user.challenges_in
    game_params = { :difficulty_id => params[:difficulty_id], :room_id => params[:room_id], :private => true }
    game        = logged_user.last_created_game || logged_user.create_game(game_params)

    game.update_attribute :max_users, users.size + 1

    users.each do |user|
      notification         = Notification.create do |n|
        n.body   = 'body'
        n.users  = [user]
        n.active = true
      end
      notification_message = "#{logged_user.profile_url_modal} está te desafiando! Sala #{game.room.name} - Velocidade #{game.difficulty_id == 1 ? "normal" : "turbo"}.\n<a href='/games/#{game.id}?notification_id=#{notification.id}'>Aceito!</a>"
      notification.update_attribute :body, notification_message
    end

    logged_user.cleanup_challenges
    redirect_to :controller => "games", :action =>"show", :id => game.id
  end

  def get_themes
    room = Room.find(params[:room_id])
    render :json => room.themes.uniq.collect { |theme| theme.name }
  end

  def accept_challenge
    if params[:notification_id]
      notification_challenge = Notification.find(params[:notification_id])
      notification_challenge.disable!
    end

    game_params    = { :difficulty_id => params[:difficulty_id], :room_id => params[:room_id], :private => true }
    game           = logged_user.create_game(game_params)
    game.max_users = 2
    game.save

    user = User.find(params[:user_from_id])
    user.challenges_in.delete(logged_user)

    notification_message = "#{logged_user.nickname} aceitou seu desafio. Para jogar <a href='/games/#{game.id}?notification_id=#{notification.id}'>clique aqui</a>"
    Notification.create do |n|
      n.body   = notification_message
      n.users  = [user]
      n.active = true
    end

    redirect_to :controller => "games", :action =>"show", :id => game.id
  end

end
