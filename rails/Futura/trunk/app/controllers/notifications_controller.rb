# encoding: UTF-8

class NotificationsController < ApplicationController
  def set_disable
    notification = Notification.find(params[:notification_id])
    notification.active = false
    notification.save
    render :nothing => true
  end

  def get_notifications
    if logged_user
      when_fragment_expired "notifications_#{logged_user.id}", 9.seconds.from_now do
        render :partial => 'notification_box'
      end
    else
      head :ok
    end
  end
end
