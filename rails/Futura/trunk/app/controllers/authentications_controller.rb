class AuthenticationsController < ApplicationController
  layout nil, :only => :facebook_canvas

  def create
    omniauth_hash = request.env["omniauth.auth"]
    user          = User.authenticate_omniauth({omniauth_hash: omniauth_hash,
                                                user_hash: { nickname: params[:username],
                                                             password: params[:password] } })

    session["#{omniauth_hash['provider']}_uid".to_sym] = omniauth_hash['uid']

    associate_facebook_account user

    redirect_to "http://apps.facebook.com/#{Rails.application.config.facebook_app_name}" # and return if request.env["omniauth.origin"].match('apps.facebook.com')
    #redirect_to rooms_path and return
  rescue ExternalAuthenticationFailed
    if user = logged_user
      user.add_authentication(omniauth_hash)
    else
      User.create_with_omniauth(omniauth_hash)
    end

    redirect_to root_path and return
    session["#{omniauth_hash['provider']}_uid".to_sym] = omniauth_hash['uid']
  rescue CustomerNotFound
    flash[:notice] = 'Incorrect nickname or password.'
    render :new and return
  end

  #def destroy
  #  logout_customer
  #  redirect_to new_session_url
  #end

  def failure
    render :text => params['message']
  end

  def facebook_canvas;
  end

  def destroy
    auth = Authentication.find(params[:id])
    if logged_user.facebook_account.first == auth
      auth.destroy
    end
    redirect_to '/profiles'
  end

  private

  # @param user [User]
  def associate_facebook_account user
    if logged_user != user
      user.facebook_account.first.update_attribute :user, logged_user
    end
  end
end