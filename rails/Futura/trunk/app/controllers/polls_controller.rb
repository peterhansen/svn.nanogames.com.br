class PollsController < ApplicationController  
  before_filter :ensure_user_is_logged
  before_filter :exit_room_and_game
  
  def show
    @poll = Poll.active
    if ( logged_user.polls && logged_user.polls.include?( @poll ) )
      redirect_to :controller => :rooms
    end
  end

  def respond
    @poll = Poll.find(params[:id])

    if @poll && !logged_user.polls.include?( @poll )
      if params[:answers].blank? 
        flash[:notice] = "Resposta em branco"
        render :show
      else
        @poll.answered_by logged_user

        params[:answers].each_pair do |key, value|
          PollQuestion.find(key).answered_option(value)
        end
        render :success, :id => @poll.id
      end
    end
  end

  def success
    unless @poll = Poll.find(param[:id])
      redirect_to profiles_path
    end
  end
end
