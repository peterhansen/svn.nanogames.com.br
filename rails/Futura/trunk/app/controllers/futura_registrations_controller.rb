class FuturaRegistrationsController < ApplicationController
  #before_filter :authenticate_user!
  #before_filter :authenticate_admin!
  before_filter :authenticate_not_registered, :except => [:index, :activate_account]
  
  def index; end

  def social_media; end

  def email_success_message    
    session[:registering] = false
    session[:user_id] = false
  end

  def activate_account
    if user = User.find_by_confirmation_token(params[:token])
      user.confirm!

      if invitation = Invitation.find_by_token(params[:ft])
        invitation.confirm!
        user.inviter = User.find(invitation.user_id)
        user.save
      end
    end
  end
  
protected
  def authenticate_not_registered
    if user_signed_in? || session[:registering].blank?
      redirect_to :root
    end
  end
end
