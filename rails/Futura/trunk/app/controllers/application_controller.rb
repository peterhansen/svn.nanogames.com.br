# encoding: UTF-8

class ApplicationController < ActionController::Base
  APP_ID = Rails.application.config.facebook_app_id
  APP_SECRET= Rails.application.config.facebook_app_secret

  protect_from_forgery
  before_filter :set_header_for_ie
  helper_method :logged_user, :logged_user_id, :logged_in_facebook?

  # @return [User]
  def logged_user
    @logged_user ||= devise_user || facebook_user || orkut_user
  end

  # @return [Numeric]
  def logged_user_id
    @logged_user_id ||= (devise_user.id if devise_user) || facebook_user_id
  end

  # @return [nil]
  def ensure_user_is_logged
    if inside_facebook? and not logged_in_facebook?
      redirect_to '/auth/facebook'
    else
      logged_user.present? || registering? || log_user_in
    end
  end

  # @return [nil]
  def set_user_online_on_page
    if devise_user
      ensure_user_is_logged unless logged_user
      exit_room_and_game
    end
  end

  # @return [nil]
  def set_user_online
    logger.silence do
      logged_user.is_online! if logged_user
      head :ok
    end
  end

  # @return [nil]
  def set_user_offline
    logger.silence do
      logged_user.is_offline! if logged_user
      head :ok
    end
  end

  # @return [nil]
  def orkut_sign_in
    user = User.find_all_by_orkut_id(params[:ouid]).last
    redirect_to '/orkut_index' unless user

    if user.confirmed_at
      session[:ouid] = params[:ouid]
      session[:callback_orkut] = "http://www.orkut.com.br/Main#Application?appId=#{params[:appid]}"
      warden.logout(:user)
      warden.default_strategies :orkut_sign
      warden.authenticate!
      warden.set_user(user, :scope => :user)
      user.current_session_id = session[:session_id]
      user.save
    else
      session[:confirmed_error_message] = "Você está tentando entrar com um endereço de e-mail que ainda não foi confirmado. Por favor verifique sua conta de e-mail e confirme sua conta, ou insira seu e-mail abaixo novamente para reenviar a mensagem."
      redirect_to :controller => :home
    end
  end

  # @return [nil]
  def decide_strategy
    unless logged_user
      if params[:ouid]
        orkut_sign_in
      else
        session[:ouid] = nil
        ensure_user_is_logged
      end
    end
  end

  # @return [User]
  def my_user
    return logged_user if logged_user

    if session[:current_user_id]
      User.find(session[:current_user_id])
    else
      User.where(:current_session_id => session[:session_id]).first
    end
  end

  # @return [nil]
  def exit_game
  #noinspection RailsChecklist01
    user = my_user

    if user
      game = user.current_game || user.games.last

      if game
        if game.reload.started?
          participation = user.active_participation

          if participation
            game.next_cicle! if participation.is_my_cicle?
            game.end! if game.reload.active_participations.size <= 1
          end

          user.exit_game
        elsif game.initialized? || game.aborted?
          user.exit_game
        end
      end
    end
  end

  # @return [nil]
  def exit_room
    if user = my_user
      room = user.room
      if room
        user.exit_room
      end
    end
  end

  # @return [nil]
  def exit_room_and_game
    exit_game
    exit_room if params[:creating_game].nil?
  end

  # @return [nil]
  def exit_site
    user = my_user
    user.exit_site if user
  end

  private

  # @return [User] The user logged through devise
  def devise_user
    current_user
  end

  # @return [User] The user logged through Facebook
  def facebook_user
    if logged_in_facebook?
      auth = Authentication.where(:uid => session['facebook_uid']).includes(:user)

      unless auth.blank?
        GC.start
        return auth.first.user
      else
        session['facebook_uid'] = nil
        return nil
      end
    end
  end

  # @return [User] The user logged through Facebook
  def facebook_user_id
    if logged_in_facebook?
      auth = Authentication.where(:uid => session['facebook_uid']).select(:user_id).first

      if auth
        return auth.user_id
      end
    end
  end

  def orkut_user
    User.find_all_by_orkut_id(params[:ouid]).last if params[:ouid]
  end

  # @return [Boolean]
  def logged_in_facebook?
    inside_facebook? && (has_signed_request? || session['facebook_uid'].present?)
  end

  # @return [Boolean]
  def registering?
    session[:registering].present?
  end

  # @return [nil]
  def log_user_in
    if inside_facebook?
      redirect_to '/auth/facebook_canvas' and return unless inside_facebook? and has_user_id?
    else
      authenticate_user!
    end
  end

  # @return [Boolean]
  def inside_facebook?
    !request.subdomain(2).match('facebook').nil?
  end

  # @return [Boolean]
  def has_user_id?
    has_signed_request? || has_code?
  end

  # @return [Numeric]
  def has_signed_request?
    if params['signed_request']
      session['facebook_uid'] = parse_signed_request(params['signed_request'])['user_id']
    end
  end

  # @return [Numeric]
  def has_code?
    if params['code']
      access_token = oauth.get_access_token(params['code'])
      uid = graph(access_token).get_object('me')['id']
      session['facebook_uid'] = uid
    end
  end

  # @param [String]
  # @return [Numeric]
  def parse_signed_request(input)
    encoded_sig, encoded_envelope = input.split('.', 2)
    envelope = MultiJson.decode(base64_url_decode(encoded_envelope))
    return envelope
  end

  # @param [String]
  # @return [Numeric]
  def base64_url_decode(str)
    str += '=' * (4 - str.length.modulo(4))
    Base64.decode64(str.tr('-_', '+/'))
  end

  # @return [Koala::Facebook::OAuth]
  def oauth
    Koala::Facebook::OAuth.new(APP_ID, APP_SECRET, callback_url)
  end

  # @return [Koala::Facebook::GraphAPI]
  def graph(access_token)
    Koala::Facebook::API.new(access_token)
  end

  # @return [String]
  def callback_url
    'http://facebook.nanogames.lvh.me:3000/auth/facebook_canvas/'
  end

  # @return [nil]
  def set_header_for_ie
    response.headers['P3P'] = 'CP="CAO PSA OUR"'
  end
end