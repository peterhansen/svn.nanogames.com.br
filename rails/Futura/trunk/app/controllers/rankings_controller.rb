class RankingsController < ApplicationController
  before_filter :ensure_user_is_logged
  before_filter :exit_room_and_game
  
  before_filter :set_challenges
  
  def index
    redirect_to :action => :all_time
  end
  
  def all_time
    @ranking_type = 'a'
    @users = User.ranking.paginate({:page => params[:page], :per_page => 10})
    @player_position = logged_user.position_in_all_ranking
  end
  
  def monthly
    @ranking_type = 'm'
    @users = User.monthly_ranking.paginate({:page => params[:page], :per_page => 10})
    @player_position = logged_user.position_in_month_ranking
  end
  
  def weekly
    @ranking_type = 'w'
    @users = User.weekly_ranking.paginate({:page => params[:page], :per_page => 10})
    @player_position = logged_user.position_in_week_ranking
  end
  
  def friends
    @ranking_type = 'a'
    @users = ( logged_user.friends + [ logged_user ] ).sort { |a,b| b <=> a }
    
    @users.each_with_index do |u, i|
      u.skill_month_variation = u.score
      
      if ( u == logged_user )
        @player_position = ( i + 1 )
      end
    end
    
    @player_position ||= @users.length + 1
    @users = @users.paginate({:page => params[:page], :per_page => 10})
  end
  
  
  private
  
  def set_challenges
    @challenges_made = Challenge.all( :conditions => [ "user_id_target = ? AND status = ?", logged_user, Challenge::INVITED ] )
  end
end
