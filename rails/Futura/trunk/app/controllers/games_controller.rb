class GamesController < ApplicationController
  before_filter :ensure_user_is_logged, :except => [:state_update, :update_player_list, :lobby]
  before_filter :check_if_playing, :only => [:show, :set_user, :create, :quick_start]

  def index
    @game = Game.first
  end

  def show
    if @game = Game.find_by_id(params[:id])
      # TODO falta testar isso
      Notification.find(params[:notification_id]).disable! if params[:notification_id]

      @url_modal = "/rooms/#{@game.room_id}"
      @type      = "mesa de jogo"
      @users     = @game.users_playing

      redirect_to room_path(@game.room) unless logged_user.enter_lobby(@game)
    else
      redirect_to rooms_path
    end
  end

  def state_update
    user = logged_user || User.find_by_id(params[:user_id])
    # TODO user.current_game pode ser diferente do jogo atual dele!!!!!!!!

    if (game = Game.find_by_id(params[:game_id])) || (user && game = user.current_game)
      game.check_turn_timeout #unless ( user && user.is_my_cicle? )
      state = game.reload.state(params[:game_turn])
      render :json => state
    else
      render :json => nil
    end
  end

  def update_player_list
    game = Game.find(params[:id])

    if game.status != Game::ENDED
      render :partial => '/games/player_info', :locals => { :users => game.users_playing, :game => game, :participations => game.participations }
    else
      render :partial => '/games/player_info', :locals => { :users => game.final_users_for_list, :game => game, :participations => game.participations }
    end
  end

  def lobby
    update       = params[:update] == 'true'
    @game        = Game.find(params[:id])
    @users       = @game.users_in_lobby_or_playing
    @ready_users = @game.users_playing.size
    @start       = @ready_users == @game.max_users

    if update
      if @start
        render :update do |page|
          page.call "startGame", @game.id
        end
      else
        render :partial => 'lobby_list', :locals => { :users       => @users,
                                                      :game        => @game,
                                                      :ready_users => @ready_users,
                                                      :timer       => get_remaining_time(@game) }
      end
    else
      render :layout => false
    end
  end

  def confirm_exit
    @game = Game.find(params[:id])
    render :layout => false
  end

  def remaining_time
    game = Game.find(params[:game_id])
    render :text => get_remaining_time(game)
  end

  def get_remaining_time(game)
    if game.started_at
      temp = 30 - (Time.now - game.started_at).to_i
      return (temp < 0 ? 0 : temp)
    else
      return 30
    end
  end

  def new
    @game         = Game.new
    @private      = params[:tipo_mesa] == "Particular"
    @difficulties = Difficulty.all
    render :layout => false
  end

  def create
    @game = logged_user.create_game(params[:game])

    if @game.save
      if @game.private?
        redirect_to :controller=>'players', :action => 'all', :creating_game => true
      else
        redirect_to :controller=>'games', :action=>'show', :id => @game.id
      end
    else
      @private      = params[:tipo_mesa] == "Particular"
      @difficulties = Difficulty.all
      render :action =>'new'
    end
  end

  def quick_start
    @difficulty = Difficulty.find_by_name(params[:d])
    unless @game = logged_user.find_quick_game(@difficulty, params[:r])
      @game = logged_user.create_game(:room_id       => params[:r],
                                      :difficulty_id => @difficulty.id,
                                      :identifier    => Game.first_available_identifier(params[:r]),
                                      :quick_start   => true,
                                      :private       => false)

      @game.save
    end
    redirect_to :controller=>'games', :action=>'show', :id => @game.id
  end

  def game_ready
    render :nothing => true
  end

  def users_list
    if game = logged_user.current_game
      users_in_game = game.users_playing
      users         = users_in_game.collect { |user| { :id    => user.id.to_s, :nickname => user.nickname,
                                                       :color => game.colors.index(user.color) + 1 } }
    end
    render :json => users
  end

  def send_game_state(game)
    render :json => game.state
  end

  def verify_answer
    if logged_user.is_my_cicle?
      if question = logged_user.current_cicle.question
        game = logged_user.current_game
        game.check_turn_timeout

        if logged_user.current_cicle.is_bonus_cicle? || question.correct_answer?(params[:answer])
          logged_user.answer_is_correct(Game::POINTS_PER_QUESTION * game.multiplier, params[:answer], params[:time])
        elsif logged_user.current_cicle.is_penalty_cicle?
          logged_user.answer_is_wrong(0, "-", params[:time])
        elsif params[:answer] != "-" && question.wrong_answer?(params[:answer])
          logged_user.answer_is_wrong(0, params[:answer], 0)
        end

        game.next_cicle!
      end
    end
    head :ok
  end

  def set_user
    unless logged_user.game
      @game = Game.find(params[:id])
      if @game.started?
        redirect_to :controller => 'rooms', :action => 'show', :id => @game.room_id if @game.started?
      else
        logged_user.enter_game(@game)
        logged_user.i_am_ready!

        @game.reload

        if @game.active_participations.size >= @game.max_users
          @game.start!
        elsif @game.users_playing_count >= @game.min_users
          @game.start_timer
        end

        render :nothing => true
      end
    end
  end

  def start_game
    Game.find(params[:id]).start!
    render :nothing => true
  end

  def load_flash
    game = Game.find(params[:id])

    if game.active_participations.size == 1
      redirect_to :controller => 'rooms', :action => 'show', :id => game.room_id
    else
      render :update do |page|
        page.replace_html "jogo_flash", :partial => 'game_flash'
        page.replace_html "chat_div", :partial => 'shared/chat'
      end
    end
  end

  def get_timer
    @game      = Game.find(params[:id])
    started_at = @game.started_at || Time.now
    timer      = (Time.now - started_at).to_i
    render :text => timer.to_s
  end

  def request_user_list
    game = Game.find(params[:id])
    if params[:users_ids] != ""
      users_already_displayed = User.find(params[:users_ids].split("_"))
      users_playing           = game.users_playing
      users                   = users_playing - users_already_displayed
    else
      users = game.users_playing
    end
    if users
      render :update do |page|
        for user in users
          page.insert_html :bottom, 'lista_jogadores', :partial => 'player_info', :locals => { :user => user }
        end
      end
    else
      render :text => "no_users_to_display"
    end
  end


  def replay_game
    new_game = logged_user.replay_game(params[:previous_game_id])
    render :text => new_game.id
  end

  def get_background_image
    if logged_user.current_game and logged_user.current_game.room.background_file_name
      render :text => logged_user.current_game.room.background.url(:large)
    else
      render :text => "null"
    end
  end

  def exit
    exit_game
    expire_page :action => :update_player_list
    render :nothing => true
  end

  def exit_and_redirect
    render :text => logged_user.last_cicle.game.room.id
  end

  private

  def verify_invite
    game = Game.find(params[:id])
    if game.private?
      owner = User.find(game.owner_id)
      unless owner.challenges_in.include?(logged_user)
        redirect_to :back
      end
    end
  end

  def check_if_playing
    if participation = logged_user.active_participation
      participation.status = Participation::ABANDONED
      participation.save
    end
  end
end
