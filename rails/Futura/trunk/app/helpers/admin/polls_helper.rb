module Admin::PollsHelper
  def percent(question, answer)
    if  question.participations > 0
      ((answer.count.to_f / question.participations.to_f) * 100).round
    else
      0
    end
  end
end

