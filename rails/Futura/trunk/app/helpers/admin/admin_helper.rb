module Admin::AdminHelper
  def text_length
    if @notification.body
      length = @notification.body.length
    else
      length = 0
    end
    165 - length
  end
  
  def enable_or_disable
   StaticDataGame.first.suggest_question_active ? "Desativar" : "Ativar"
  end
  
 
end
