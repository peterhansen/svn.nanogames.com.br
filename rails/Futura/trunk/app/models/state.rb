# == Schema Information
#
# Table name: states
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  abv        :string(255)
#  country_id :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class State < ActiveRecord::Base
  has_many :users
  belongs_to :country
  
  def self.my_state_name(id)
   if state = State.find_by_id(id)
     return state.name
   else
     return ""
   end
  end
  
end
