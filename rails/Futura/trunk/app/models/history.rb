# == Schema Information
#
# Table name: histories
#
#  id               :integer(4)      not null, primary key
#  started          :boolean(1)      default(FALSE)
#  private          :boolean(1)      default(FALSE)
#  password         :string(255)
#  min_users        :integer(4)      default(2)
#  max_users        :integer(4)      default(6)
#  game_created_at  :datetime
#  game_finished_at :datetime
#  created_at       :datetime
#  updated_at       :datetime
#

class History < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_and_belongs_to_many :rooms
end
