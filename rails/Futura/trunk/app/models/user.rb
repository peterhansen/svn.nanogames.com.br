# encoding: UTF-8

# == Schema Information
#
# Table name: users
#
#  id                    :integer(4)      not null, primary key
#  email                 :string(255)     default(""), not null
#  encrypted_password    :string(128)     default(""), not null
#  password_salt         :string(255)     default(""), not null
#  confirmation_token    :string(255)
#  confirmed_at          :datetime
#  confirmation_sent_at  :datetime
#  reset_password_token  :string(255)
#  remember_token        :string(255)
#  remember_created_at   :datetime
#  sign_in_count         :integer(4)      default(0)
#  current_sign_in_at    :datetime
#  last_sign_in_at       :datetime
#  current_sign_in_ip    :string(255)
#  last_sign_in_ip       :string(255)
#  failed_attempts       :integer(4)      default(0)
#  unlock_token          :string(255)
#  locked_at             :datetime
#  created_at            :datetime
#  updated_at            :datetime
#  avatar_file_name      :string(255)
#  avatar_content_type   :string(255)
#  avatar_file_size      :integer(4)
#  avatar_updated_at     :datetime
#  blocked               :boolean(1)      default(FALSE)
#  game_id               :integer(4)
#  room_id               :integer(4)
#  name                  :string(255)
#  post_name             :string(255)
#  marital_status_id     :integer(4)
#  city                  :string(255)
#  about                 :string(255)
#  state_id              :integer(4)
#  country_id            :integer(4)
#  gender_id             :integer(4)
#  born_day              :string(255)
#  born_month            :string(255)
#  born_year             :string(255)
#  nickname              :string(255)
#  show_email            :boolean(1)
#  show_age              :boolean(1)
#  receive_newsletter    :boolean(1)
#  agreed_terms          :boolean(1)
#  show_marital_status   :boolean(1)      default(FALSE)
#  room_user_id          :string(255)     default("0")
#  game_player_id        :string(255)     default("0")
#  color                 :string(255)
#  turn                  :integer(4)
#  orkut_id              :string(255)
#  complete_name         :string(255)
#  my_game_id            :integer(4)
#  skill                 :integer(4)      default(500)
#  digest_avatar         :string(255)
#  score                 :integer(4)      default(0)
#  inviter_id            :integer(4)
#  current_session_id    :string(255)
#  enter_in_game_at      :datetime
#  skill_month_variation :integer(4)      default(0)
#  skill_week_variation  :integer(4)      default(0)
#  questions_skipped     :integer(4)
#  patent                :string(255)     default("Calouro"), not null
#  users_count           :integer(4)      default(0)
#

#noinspection RubyClassMethodNamingConvention
class User < ActiveRecord::Base
  devise :registerable, :database_authenticatable, :recoverable,
         :rememberable, :trackable, :validatable, :confirmable,
         :lockable, :encryptable, :encryptor => :sha512

  include Comparable, OmniauthAuthenticatable
  attr_accessor :result

  FIELDS_USED_IN_RANKING        = 'id, nickname, skill_month_variation, skill_week_variation, score, avatar_file_name, patent'
  ONLINE_USERS_COUNT_INTERVAL   = 10.seconds
  SET_DEAD_INTERVAL             = 70.seconds
  RANKING_UPDATE_INTERVAL       = 1.minute
  HIGHEST_SCORE_UPDATE_INTERVAL = 5.minutes

  PATENT_LEVEL = ["Gênio", "Sábio", "Mestre", "Especialista", "Veterano", "Monitor", "Aspirante",
                  "Aprendiz", "Novato", "Calouro"]

  PATENT_LEVEL_RATE = 1.07
  PATENT_LEVELS     = 40
  PATENT_LEVEL_MIN  = 0

  before_save :set_complete_name

  attr_accessible :email, :password, :password_confirmation, :avatar, :name, :post_name,
                  :marital_status_id, :city, :about, :state_id, :country_id, :gender_id,
                  :born_day, :born_month, :born_year, :nickname, :show_email, :show_age,
                  :agreed_terms, :receive_newsletter, :about, :patent, :remember_me

#  scope :ranking, lambda {{:order => 'score desc, created_at'}}
#  scope :weekly_ranking, lambda {{:order => 'skill_week_variation desc, created_at'}}
#  scope :monthly_ranking, lambda {{:order => 'skill_month_variation desc, created_at'}}

  belongs_to :room
  belongs_to :my_game, :class_name => 'Game'
  belongs_to :marital_status
  belongs_to :state
  belongs_to :country
  belongs_to :gender
  has_many :participations
  has_many :games, :through => :participations
  has_many :cicles
  has_many :invitations
  has_many :friendships
  has_many :friends, :through => :friendships, :class_name => "User"
  has_many :skill_variations
  belongs_to :inviter, :class_name => "User"
  has_many :inviteds, :class_name => "User", :foreign_key => 'inviter_id'
  has_and_belongs_to_many :histories
  has_and_belongs_to_many :notifications
  has_and_belongs_to_many :polls
#has_one :google, :class_name => "GoogleToken", :dependent=>:destroy
  has_many :orkut_friends, :class_name => "OrkutProfile"
  has_many :suggested_questions

  acts_as_network :challenges, :join_table => :challenges

  has_attached_file :avatar,
                    :styles      => { :to_crop    => '425x400!',
                                      :medium     => "100x100!",
                                      :thumb      => "66x66!",
                                      :small_thumb=>"26x26!" },
                    :default_url => '/avatars/:style/missing.gif'


  validates_attachment_size :avatar, :less_than => 5.megabytes
  validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png', 'image/gif',
                                                               "image/pjpeg", "image/x-png"]
  validates_presence_of :name, :post_name, :born_day, :born_month, :born_year, :city, :nickname, :agreed_terms
  validates_length_of :name, :maximum=> 20
  validates_length_of :post_name, :maximum=> 20
  validates_length_of :nickname, :maximum=> 20
  validates_uniqueness_of :nickname
  validates_length_of :born_day, :is=> 2, :message=>"#{I18n.t(:size_must_have)} 2 #{I18n.t(:digits)}"
  validates_length_of :born_month, :is=> 2, :message=>"#{I18n.t(:size_must_have)} 2 #{I18n.t(:digits)}"
  validates_length_of :born_year, :is=> 4, :message=>"#{I18n.t(:size_must_have)} 4 #{I18n.t(:digits)}"
  validates_length_of :city, :maximum=> 50

  scope :online, where(online: true)

  def self.find_by_email_or_nickname(param)
    User.find(:first, :conditions => ['email = ? or nickname = ?', param, param])
  end

  def self.how_many_online
    online.count
  end

  def self.find_all_by_online(online = true)
    where('online is true')
  end

  def confirm_invite_from_orkut!(ouid)
    if inviter_user_id = OrkutProfile.find_all_by_status_and_profile_id(OrkutProfile::INVITED, ouid).last
      inviter_user    = User.find(inviter_user_id)
      self.inviter_id = inviter_user_id
      self.save
      # TODO adicionar pontos em outro lugar
      inviter_user.score += 100
      inviter_user.save
      inviter_user
    end
  end

  def online?
    self.reload.online
  end

  def how_many_online_friends
    online_friends.size
  end

  def online_friends(param = true)
    self.friends.select { |friend| friend if friend.online == param }
  end

  def suggest_question(params)
    suggested_question = SuggestedQuestion.new(params)
    if suggested_question.save
      self.suggested_questions << suggested_question
    end
    return suggested_question
  end


  def how_many_approved_questions
    self.suggested_questions.find_all_by_approved(true).size
  end

  def colors
    ["verde", "amarelo", "azul", "laranja", "roxo", "vermelho"]
  end

  def find_quick_game(dif, room_id)
    game = Game.find(:first,
                     :conditions => { :quick_start => true, :status => Game::INITIALIZED, :room_id => room_id, :difficulty_id => dif.id },
                     :order      => ["created_at DESC"])

    game if game and (game.users_in_lobby_or_playing.size < 6)
  end

  def replay_game(previous_game_id)
    unless game_to_replay = Game.find_by_previous_game_id(previous_game_id)
      previous_game  = Game.find(previous_game_id)
      game_to_replay = self.create_game(previous_game.params_for_replay)
    end
    game_to_replay
  end

  def last_created_game
    game = self.games.find(:all, :readonly => false).last
    if game.present? && game.status == Game::INITIALIZED
      return game
    end
  end

  def create_notification_for_patent
    full_message = "Você acabou de mudar para a patente: <strong>#{patent}</strong><br /><a href='/share'>Compartilhar</a>"
    notifications.create(:body => full_message, :active => true)
  end

  def cleanup_challenges
    self.challenges_in = []
  end

  def color
    colors[self.active_participation.color - 1] if active_participation
  end

  def color_for_game(game)
    if participation = game.participations.find_all_by_user_id(self.id).first
      colors[(participation.color || 1) - 1]
    end
  end


  def id_and_name
    { :futura_id => self.id, :display_name => self.nickname }
  end

  def age
    born_date = birthday_date
    today     = Date.today
    user_age  = today.year - born_date.year - (born_date.change(:year => today.year) > today ? 1 : 0)
    user_age
  end

  def birthday_display
    I18n.l birthday_date, :format => :birthday
  end

  def is_my_friend?(friend_id)
    if self.friend_ids.include?(friend_id)
      my_friendship = Friendship.find(:first, :conditions => { :user_id   => self.id,
                                                               :friend_id => friend_id })
      my_friendship.present?
    else
      false
    end
  end

  def set_complete_name
    self.complete_name = "#{self.name} #{self.post_name}"
  end

  def birthday_date
    if Rails.env.develpment?
      "#{self.born_day}.#{self.born_month}.#{self.born_year}".to_date
    else
      "#{self.born_month}.#{self.born_day}.#{self.born_year}".to_date
    end
  end


  def cleanup_notifications!
    self.notifications = []
    self.save
  end

  def cleanup_challenges!
    self.challenges_in  = []
    self.challenges_out = []
    self.save
  end

  def cleanup_notifications_and_challenges!
    cleanup_challenges!
  end

  def <=>(player)
    self.score <=> player.score
  end

  def add_skill_variation(amount)
    # TODO não usado no momento SkillVariation.create!(:amount => amount, :user => self)
  end

  def right_answer!
    participation      = self.active_participation
    participation.hits += 1
    participation.save
  end

  def wrong_answer!
    participation        = self.active_participation
    participation.misses += 1
    participation.save
  end

  def refresh_patent!(players_count, position = nil)
    patent_level, patent_sublevel = get_patent_level(players_count)
    patent_text                   = "#{patent_level}_#{patent_sublevel}"

    unless self.patent == patent_text
      self.set_patent(patent_text)
      self.create_notification_for_patent
    end
  end

  def patent_to_s
    self.patent.parameterize
  end

  def unanswered_poll
    if poll = Poll.active
      return poll unless self.responded_to_poll(poll)
    end
  end

  def responded_to_poll(poll)
    self.polls.include?(poll)
  end

  def exit_game
    if active_participation
      if active_participation.game and active_participation.game.started?
        end_cicle! if is_my_cicle?
        active_participation.update_attribute(:status, Participation::ABANDONED)
      else
        active_participation.update_attribute(:status, Participation::KICKED)
      end
    end
    update_attribute(:color, nil)
  end

  def exit_room
    # TODO necessário persistir em banco a sala do jogador?
    # Resposta: a sala do jogador deveria ser um atributo da Participation.
    update_attributes(:room => nil, :room_user_id => nil)
  end

  def exit_site
    is_offline!
  end

  def current_month_score
    skill_month_variation
  end

  def last_week_score
    skill_week_variation
  end

  def self.ranking
    all_time_ranking
  end

  def self.all_time_ranking
    ranking_all           = Rails.cache.read('ranking_all') || nil
    last_all_ranking_time = Rails.cache.read('last_all_ranking_time') || 1.day.ago

    if (ranking_all.nil? || (Time.now - last_all_ranking_time >= RANKING_UPDATE_INTERVAL))
      Rails.cache.write('last_all_ranking_time', Time.now)
      if (ranking_all.nil?)
        User.refresh_all_ranking
      else
        User.delay.refresh_all_ranking
      end
    end

    return ranking_all
  end

  def self.reset_monthly_ranking
    all_p          = Participation.all(:conditions => ["created_at > '?'", Date.today.beginning_of_month], :select => 'sum(score) sum_score, user_id', :group => :user_id)
    non_zero_users = []

    all_p.each do |p|
      user = User.find_by_id(p.user_id)
      if user
        non_zero_users << user
        user.skill_month_variation = p.sum_score
        user.save
      end
    end

    other_users = User.all(:conditions => ["id NOT IN (?)", non_zero_users])
    other_users.each do |user|
      user.skill_month_variation = 0
      user.save
    end

    Rails.cache.write('last_month_ranking_reset_time', Time.now)
  end

  def self.reset_week_scores
    all_participations = Participation.all(:conditions => ["created_at > ?", Date.today.beginning_of_week], :select => 'sum(score) sum_score, user_id', :group => :user_id)
    non_zero_users     = []

    all_participations.each do |participation|
      user = User.find_by_id(participation.user_id)
      if user
        non_zero_users << user
        user.skill_week_variation = participation.sum_score
        user.save
      end
    end

    other_users = User.all(:conditions => ["id NOT IN (?)", non_zero_users])
    other_users.each do |user|
      user.skill_week_variation = 0
      user.save
    end
    Rails.cache.write('last_week_ranking_reset_time', Time.now)
  end

  def refresh_score
    update_attributes(
        score:                 score_per_period + polls_score + invitations_score,
        skill_month_variation: score_per_period(Date.today.beginning_of_month),
        skill_week_variation:  score_per_period(Date.today.beginning_of_week)
    )
  end

  def polls_score
    self.polls.count(select: 'id') * Poll::POINTS
  end

  def invitations_score
    Invitation.count(:conditions => { :user_id => self, :status => Invitation::ACCEPTED }, :select => 'id') * Invitation::POINTS
  end

  # @return [Array]
  def self.monthly_ranking
    User.delay.reset_monthly_ranking unless monthly_ranking_reseted?

    if monthly_ranking_in_cache? and monthly_ranking_cache_valid?
      ranking = Rails.cache.read('ranking_month')
      User.delay.refresh_month_ranking
    else
      ranking = User.refresh_month_ranking
    end

    ranking
  end

  # @return [Boolean]
  def self.monthly_ranking_in_cache?
    Rails.cache.read('ranking_month').present?
  end

  # @return [Boolean]
  def self.monthly_ranking_cache_valid?
    Time.now - (Rails.cache.read('last_month_ranking_time') || 1.day.ago) < RANKING_UPDATE_INTERVAL
  end

  # @return [Boolean]
  def self.monthly_ranking_reseted?
    (Rails.cache.read('last_month_ranking_reset_time') || 2.months.ago) > Date.today.beginning_of_month
  end

  # @return [Array]
  def self.weekly_ranking
    User.delay.reset_week_scores unless weekly_ranking_reseted?

    if weekly_ranking_in_cache? and weekly_ranking_cache_valid?
      User.delay.refresh_week_ranking
      weekly_ranking = Rails.cache.read('ranking_week')
    else
      weekly_ranking = User.refresh_week_ranking
    end

    weekly_ranking
  end

  # @return [Boolean]
  def self.weekly_ranking_reseted?
    (Rails.cache.read('last_week_ranking_reset_time') || 1.month.ago) < Date.today.beginning_of_week
  end

  # @return [Boolean]
  def self.weekly_ranking_cache_valid?
    Time.now - (Rails.cache.read('last_week_ranking_time') || 1.day.ago) >= RANKING_UPDATE_INTERVAL
  end

  # @return [Boolean]
  def self.weekly_ranking_in_cache?
    Rails.cache.read('ranking_week').present?
  end

  ##
  # Obtém a pontuação acumulada do jogador num determinado período.
  # start_date data de início da pesquisa. Se for nulo, não filtra as entradas mais antigas.
  # end_date data de fim da pesquisa. Se for nulo, não filtra as entradas mais recentes.
  ##
  def score_per_period(start_date = nil, end_date = nil)
    if start_date
      if end_date
        all_p = Participation.all(:conditions => ["user_id = ? AND created_at BETWEEN ? and ?", self, start_date, end_date], :select => 'sum(score) score')
      else
        all_p = Participation.all(:conditions => ["user_id = ? AND created_at > ?", self, start_date], :select => 'sum(score) score')
      end
    else
      if end_date
        all_p = Participation.all(:conditions => ["user_id = ? AND created_at < ?", self, end_date], :select => 'sum(score) score')
      else
        all_p = Participation.all(:conditions => ["user_id = ?", self], :select => 'sum(score) score')
      end
    end

    if all_p
      total = 0
      all_p.each { |p|
        total += p.score if (p.score)
      }

      return total
    else
      return 0
    end
  end

  def position_in_all_ranking
    if ranking_all = User.all_time_ranking
      ranking_all.each_with_index do |user, index|
        if user.id == self.id
          return index + 1
        end
      end
      ranking_all.size || 0
    end
    #User.count(:conditions => "id != #{self.id} and (score > #{self.score} or (score = #{self.score} and created_at <= \"#{self.created_at}\"))", :select => 'id' ) + 1
  end

  def position_in_week_ranking
    ranking_week = User.weekly_ranking
    ranking_week.each_with_index { |u, i|
      if (u.id == self.id)
        return i + 1
      end
    }
    ranking_week.size
  end

  def position_in_month_ranking
    ranking_month = User.monthly_ranking
    ranking_month.each_with_index { |u, i|
      if (u.id == self.id)
        return i + 1
      end
    }
    ranking_month.size
  end

  def already_invited?(email)
    if User.find_by_email(email)
      not Invitation.find(:first, :conditions => "user_id = #{self.id} and friend_email = \"#{email}\" and created_at >= \"#{(Time.now - 1.days).to_date}\"").blank?
    else
      return false
    end
  end

  def send_invitation(email, message)
    token      = self.id.to_s
    invitation = Invitation.new(:user_id => self.id, :friend_email => email.strip, :token => token, :status => 1)

    if invitation.save
#      Delayed::Job.enqueue(BackgroundJob.new( 'Emailer', 'deliver_invitation', self, email, message, token))
      Emailer.delay.deliver_invitation(self, email, message, token)
    end

    invitation
  end

  def active_notifications
    # TODO como fazer cache das notificações de cada jogador, e só expirá-las quando houver novidades?
    notifications = self.notifications.where("active = true AND created_at >= ?", 1.hour.ago)

    notifications_to_all = Notification.find(:all, :conditions => "active = true AND to_all = true")

    if poll = Poll.active
      if self.polls.include? poll
        notifications_to_all.delete_if { |n| n.id == poll.notification }
      end
    end
    notifies = notifications + notifications_to_all.to_a
    notifies.sort_by { |n| n.created_at }.reverse
  end


  def config_orkut_id
    if self.google
      message = { "method" => 'people.get', "params" => { "userId" => ["@me"], "groupId" => "@self",
                                                          "fields" => ["id"] } }.to_json
      ouid    = orkut_connection("http://orkut.gmodules.com/social/rpc/people", message)["id"]

      User.transaction do
        users = User.find_all_by_orkut_id(ouid)
        users.each { |u| u.orkut_id = nil; u.google = nil; u.save }
        self.orkut_id = ouid
        self.save
      end
    else
      return nil
    end
  end

  def captcha_image
    orkut.captcha_image(orkut.captchas.last.captcha_url) unless orkut.captchas.empty?
  end

  def orkut_header
    { 'Content-Type' => 'application/json' }
  end

  def orkut_token
    self.google.client
  end

  def import_orkut_friends
    if self.orkut_id
      message = { "method" => 'people.get', "params" => { "userId" => ["@me"], "groupId" => "@friends", "fields" => ["displayName", "emails",
                                                                                                                     "thumbnailUrl"] } }.to_json

      friends = orkut_connection("http://orkut.gmodules.com/social/rpc/people", message)

      friends.each do |friend|
        if friend['emails'] && user = User.find_by_email(friend["emails"].first["value"])
          self.add_friend(user.id)
        else
          # TODO send_orkut_invitation(friend["emails"].first["value"]) unless friend["email"].blank?
        end
      end
    end
  end

  def profile_url_modal
    "<a href='#' onclick=\"constructModal('profile_#{self.id}', '/profiles/#{self.id}');\">#{self.nickname}</a>"
  end

  def add_friend(friend)
    if Friendship.create(user: self, friend: friend)
      Notification.create(users: [friend], body: "#{profile_url_modal} te adicionou como amigo.")
      Notification.create(users: [self], body: "Você acabou de adicionar #{friend.profile_url_modal} como amigo.")
    end
  end

  def orkut_friends_to_invite
    self.orkut_friends.find_all_by_status(OrkutProfile::PENDING)
  end

  def invited_challenge_for?(user_id)
    challenge_invited = Challenge.find(:first, :conditions => { :user_id => user_id, :user_id_target => self.id, :status => Challenge::INVITED })
    return ((!challenge_invited.nil?) and challenge_invited.challenged_at > DateTime.yesterday)
  end

  def set_invited_challenge(user_id)
    Challenge.connection.execute("UPDATE challenges SET status = '#{Challenge::INVITED}' WHERE user_id = '#{user_id}' AND user_id_target = '#{self.id}'")
  end

  def time_in_game(game)
    cicles = self.cicles.find_all_by_game_id(game.id)
    cicles.inject(0) { |sum, t| sum + t.time }
  end

  def last_participation
    participations.find(:first, :order => "created_at DESC")
  end

  def right_answers
    self.active_participation.hits
  end

  def create_game(params)
    game        = Game.new(params)
    game.turn   = 0 # TODO porque isso não é o valor padrão?
    game.cicle  = 0
    game.status = Game::INITIALIZED

    if game.save
      self.enter_lobby(game)
      return game
    else
      return nil
    end
  end

  # @param game [Game]
  # @return [Boolean]
  def enter_lobby(game)
    return false if game.status != Game::INITIALIZED or game.active_participations.size >= game.max_users

    unless participation = game.participations.find_all_by_user_id(self.id).first
      participation = self.participations.build(:game_id => game.id, :color => game.free_colors.first)
    end

    participation.status = Participation::IN_LOBBY
    self.room = game.room if self.room.nil?
    participation.save
    game.reload
    self.save
  end

  # @param game [Game]
  # @return [Boolean]
  def enter_game(game)
    if participation = game.participations.find_all_by_user_id(self.id).first
      User.transaction do
        #participation.update_attribute(:color, game.free_colors.first)
        participation.update_attribute(:status, Participation::ACTIVE)
        game.reload
        participation.update_attribute(:turn, game.users_playing_count - 1)
        participation.update_attribute(:enter_in_game_at, Time.now)
        update_attribute(:challenges_out, [])
        update_attribute(:questions_skipped, 0)
      end
    else
      return false
    end
  end

  def i_am_ready!
    if participation = self.active_participation
      participation.update_attribute :ready, true
    end
  end

  # @param game [Game]
  # @return [Boolean]
  def new_cicle_for_game(game)
    cicles.create!(:game_id => game.id)
  end

  def confirmed_in_lobby?(game)
    # TODO Tell, don't ask.
    participations.find_all_by_game_id(game.id).last.status == Participation::ACTIVE
  end

  def active_participation
    self.participations.where(:status => [Participation::ACTIVE, Participation::IN_LOBBY]).order('created_at DESC').first
  end

  # @return [Cicle]
  def current_cicle
    # TODO O game deveria ter um atributeo current_cycle
    self.cicles.where(:active => true).order('created_at desc').first
  end

  def last_cicle
    self.cicles.order('created_at desc').first
  end

  # @return [Game]
  def current_game
    active_participation.game if active_participation
  end

  def answer_is_correct(points, answer, time)
    if is_my_cicle?
      User.transaction do
        self.right_answer!
        update_attribute(:questions_skipped, 0)

        cicle                = self.current_cicle
        cicle.correct        = true
        cicle.choosed_answer = answer
        cicle.points         = points
        cicle.time           = time
        cicle.save

        add_skill_variation(points)

        participation       = self.active_participation
        participation.score += points
        participation.save
        self.save
        #self.end_cicle!
      end
    end
  end

  def answer_is_wrong(points, answer, time)
    if is_my_cicle?
      User.transaction do
        self.wrong_answer!
        update_attribute(:questions_skipped, 0)
        cicle                = self.current_cicle
        cicle.correct        = false
        cicle.choosed_answer = answer
        cicle.points         = points
        cicle.time           = time
        add_skill_variation(points)

        cicle.save

        active_participation.update_attribute(:score, self.active_participation.score + points)
        #self.end_cicle!
      end
    end
  end

  def did_not_answer(time_elapsed)
    User.transaction do
      self.wrong_answer!
      update_attribute(:questions_skipped, questions_skipped + 1)
      cicle         = self.current_cicle
      cicle.correct = false
      cicle.time    = time_elapsed
      cicle.save
    end
  end

  def leaved_by_timeout?
    questions_skipped >= Game::TURNS_BEFORE_TIMEOUT
  end

  def is_my_cicle?
    self.active_participation.is_my_cicle? if self.active_participation
  end

  # @return [Boolean]
  def end_cicle!
    if cycle = current_cicle
      cycle.update_attribute(:active, false)
    end
  end

  def creating_game?
    return self.games.last && self.games.last.status == Game::INITIALIZED
  end

  def game
    current_game
  end

  def orkut
    Orkut.find(self.google.orkut_id) if self.google
  end

  def orkut_connection(uri, message)
    response              = orkut_token.post(uri, message, orkut_header)
    response_decoded_body = ActiveSupport::JSON.decode(response.body)
    response_decoded_body["data"]
  end

  def get_patent_level(players_count)
    if score <= 0
      return PATENT_LEVEL.last, 0
    end

    patent_ranges = User.get_ranges(User::PATENT_LEVELS, User::PATENT_LEVEL_MIN, players_count, User::PATENT_LEVEL_RATE)
    position     = players_count - self.position_in_all_ranking
    index_patent = patent_ranges.index(patent_ranges.select { |p| p if p.include? position }.first)

    if index_patent
      level_name     = PATENT_LEVEL[index_patent / 4]
      sub_level_name = 3 - (index_patent % 4)
    else
      level_name     = PATENT_LEVEL.last
      sub_level_name = 0
    end

    return level_name, sub_level_name
  end

  def patent_icon_url
    patent, level = get_patent_level(User.count(:conditions => 'score >0'))
    "/images/ranking/ic_#{patent.parameterize}_#{level}.png"
  end

  def patent_name
    self.patent.split("_").first
  end

  def self.refresh_all_ranking(refresh_patents = true)
    User.transaction do
      ranking_all = User.all(:select => FIELDS_USED_IN_RANKING, :order => 'score DESC')
      Rails.cache.write('ranking_all', ranking_all)

      if refresh_patents
        players_count = User.count(:conditions => "score > 0")
        ranking_all.each_with_index do |user, index|
          user.delay.refresh_patent!(players_count, (index + 1))
        end
      end
    end
  end


  # @return [Array]
  def self.refresh_month_ranking
    ranking_month = User.all(:select => FIELDS_USED_IN_RANKING, :order => 'skill_month_variation DESC')
    Rails.cache.write('last_month_ranking_time', Time.now)
    Rails.cache.write('ranking_month', ranking_month)
    ranking_month
  end


  # @return [Array]
  def self.refresh_week_ranking
    ranking_week = User.all(:select => FIELDS_USED_IN_RANKING, :order => 'skill_week_variation DESC')
    Rails.cache.write('ranking_week', ranking_week)
    Rails.cache.write('last_week_ranking_time', Time.now)
    ranking_week
  end

  def self.refresh_online_users_list
    users = User.where('online is true and updated_at < "?"', 30.minutes.ago)
    users.each do |user|
      user.is_offline!
    end
  end

  def set_patent(new_patent)
    #	reload é necessário para evitar	problemas em save() caso o objeto não tenha carregado todos os dados do banco
    self.reload
    self.update_attribute(:patent, new_patent)
  end

  ##
  # Obtém faixas de valores cujas proporções entre si sejam definidas por rate.
  ##
  def self.get_ranges(n_levels, min = 0.0, max = 1.0, rate = 1.61803399)
    levels   = []
    total    = 0
    acc      = max
    previous = max

    n_levels.times do |i|
      total += rate ** i
    end

    n_levels.times do |i|
      if (i == n_levels - 1)
        acc = min
      else
        acc -= (rate ** i) * (max - min) / total
      end
      levels[i] = acc ... previous
      previous  = acc
    end

    levels
  end

  def self.refresh_patents
    User.all.each do |user|
      patent, sub_level = user.get_patent_level(User.count(:conditions => 'score >0'))
      user.set_patent(patent + "_" + sub_level.to_s)
    end
  end


  def send_victory_notification
    share_message = "#{nickname} mostrou que sabe tudo e venceu uma partida no CDF – Clube Desafio Futura!"
    notifications.create(:body => "Você acabou de ganhar uma partida!<br /><a href='/share?message=#{share_message}'> Compartilhar</a>")
  end


  def self.generate_password(length = 7)
    charset      = %w{2 3 4 6 7 8 9 A C D E F G H J K L M N P Q R T V W X Y Z}
    new_password = ""

    while !(new_password =~ /\d/)
      new_password = (0...length).map { charset.to_a[rand(charset.size)] }.join
    end
    new_password
  end

  def is_online!
    update_attribute :online, true
  end

  def is_offline!
    update_attribute :online, false
  end

  def has_facebook?
    facebook_account.any?
  end

  def facebook_account
    authentications.where(provider: 'facebook')
  end
end
