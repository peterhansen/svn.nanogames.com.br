# == Schema Information
#
# Table name: friendship_statuses
#
#  id         :integer(4)      not null, primary key
#  status     :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class FriendshipStatus < ActiveRecord::Base
  has_many :friendships
end
