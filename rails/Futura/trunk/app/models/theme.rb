# == Schema Information
#
# Table name: themes
#
#  id                :integer(4)      not null, primary key
#  parent_id         :integer(4)
#  children_count    :integer(4)
#  ancestors_count   :integer(4)
#  descendants_count :integer(4)
#  hidden            :boolean(1)
#  name              :string(255)
#  description       :string(255)
#  name_with_level   :string(255)
#  position          :integer(4)
#  created_at        :datetime
#  updated_at        :datetime
#  requested_times   :integer(4)      default(0)
#

class Theme < ActiveRecord::Base
  include Categorizable

  has_and_belongs_to_many :rooms
  has_many :questions
  has_many :suggested_questions
  
  before_destroy :save_questions
  
  validates :name, :presence => true, :uniqueness => true

  scope :root, :conditions => {:ancestors_count => 0}
end