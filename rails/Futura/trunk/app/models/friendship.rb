# == Schema Information
#
# Table name: friendships
#
#  id                   :integer(4)      not null, primary key
#  user_id              :integer(4)
#  friend_id            :integer(4)
#  created_at           :datetime
#  updated_at           :datetime
#  friendship_status_id :integer(4)      default(1)
#

class Friendship < ActiveRecord::Base
  PENDING = 1
  ACCEPTED = 2

  belongs_to :user
  belongs_to :friend, :class_name => "User", :foreign_key => "friend_id"
  validates_uniqueness_of :friend_id, :scope => :user_id
  validate :self_friendship

  def self_friendship
    errors.add(:base, I18n.t(:cannot_add_yourself)) if self.user_id == self.friend_id
  end
end
