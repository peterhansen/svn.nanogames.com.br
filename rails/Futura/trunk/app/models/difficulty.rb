class Difficulty
  attr_accessor :id, :name
  
  def self.all
    [{:id => 1, :name => "Normal"}, {:id => 2, :name => "Turbo"}].map {|d| new(d)}
  end
  
  def initialize(params)
    @id = params[:id]
    @name = params[:name]
  end
  
  def self.first
    all.first
  end
  
  def self.last
    all.last
  end
  
  def self.find(id)
    all.detect { |d| d.id == id }
  end
  
  def self.find_by_name(name)
    all.detect { |d| d.name == name }
  end
  
  def games
    Game.find_all_by_difficulty_id(self.id)
  end
end
