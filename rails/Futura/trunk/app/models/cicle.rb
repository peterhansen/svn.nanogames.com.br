# == Schema Information
#
# Table name: cicles
#
#  id             :integer(4)      not null, primary key
#  game_id        :integer(4)
#  user_id        :integer(4)
#  points         :integer(4)      default(0)
#  correct        :boolean(1)      default(FALSE)
#  created_at     :datetime
#  updated_at     :datetime
#  active         :boolean(1)      default(TRUE)
#  question_id    :integer(4)
#  choosed_answer :string(255)
#  time           :integer(4)      default(0)
#  kind           :integer(4)
#

class Cicle < ActiveRecord::Base
  belongs_to :game
  belongs_to :user
  belongs_to :question

  def is_bonus_cicle?
    kind > 0
  end

  def is_penalty_cicle?
    kind < 0
  end
end
