# == Schema Information
#
# Table name: answers
#
#  id          :integer(4)      not null, primary key
#  question_id :integer(4)
#  a           :string(255)
#  b           :string(255)
#  c           :string(255)
#  d           :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Answer < ActiveRecord::Base
  validates_presence_of :a, :b
  belongs_to :question
end
