# == Schema Information
#
# Table name: questions
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)
#  answer_id  :integer(4)
#  theme_id   :integer(4)
#  correct    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Question < ActiveRecord::Base
  
  #CSV Import Constants for csv array
  TITLE =           0
  ANSWER_A =        1
  ANSWER_B =        2
  ANSWER_C =        3
  ANSWER_D =        4
  CORRECT_ANSWER =  5
  
  belongs_to :answer
  belongs_to :theme
  has_one :cicle
  has_and_belongs_to_many :used_in_games, :class_name => "Game"
  
  validates_presence_of :title, :answer, :theme, :correct
  validates_uniqueness_of :title
  
  def self.import_from_text( content, theme_id )
    # total_questions = 0
    # saved_questions = 0

    # encoding = CharDet.detect( content )["encoding"].downcase
    # return false, "#{saved_questions} / #{total_questions}" if encoding != "utf-8" and (encoding =~ /ISO-8859-/i) != 0
    # if encoding =~ /ISO-8859-2/i
    #   content = Iconv.iconv("UTF-8", "WINDOWS-1252", content)
    #   content = content.first
    # elsif encoding =~ /ISO-8859-/i
    #   content = Iconv.iconv("UTF-8", "ISO-8859-1", content)
    #   content = content.first
    # end
    
    # unless @theme = Theme.find_by_id( theme_id )
    #   return false, "#{saved_questions} / #{total_questions}"
    # end

    # question = Question.new
    # answer =  Answer.new

    # begin
    #   content.each_line do |line|

    #     line.strip!
    #     # atenção: as expressões regulares usadas possuem caracteres que parecem espaços, mas não são! E são importantes para evitar erros no parser.
    #     if line.length > 0
    #       if index = line.gsub!( /^(\d|\.|"){1,7} *(-|–|;) */, '' )
    #         # leu pergunta
    #         question.title = line
    #         total_questions += 1
    #         # despreza possíveis respostas anteriores (provavelmente falha na pergunta anterior)
    #         answer = Answer.new
    #       elsif index = line.gsub( /^( |\*| )*(a|b|c|d) {0,3}(-|–|;|\)) *\)?(\t| | )*/i, '' )
    #         # leu opção de resposta
    #         index = line.index( /(a|b|c|d)/i )
    #         if ( index && question.title )
    #           option = line[ index..index ].downcase
    #           question.correct = option if ( line.index( '*' ) )
   
    #           line.gsub!( /^( |\*| )*(a|b|c|d) {0,3}(-|–|;|\))(\t| | )*/i, '' )
    #           case option
    #             when 'a'
    #               answer.a = line
    #             when 'b'
    #               answer.b = line
    #             when 'c'
    #               answer.c = line
    #             when 'd'
    #               answer.d = line
    #           end
    #         end
    #       end

    #       if answer and answer.a and answer.b and answer.c
    #         question.theme = @theme
    #         answer.save
    #         question.answer = answer if question and answer
    #         answer = Answer.new
    #         question.save if question
    #         saved_questions += 1
    #         question = Question.new
    #       end
    #     end 
    #   end

    #   return true, "#{saved_questions} / #{total_questions}"
    # rescue Exception => e
    #   return false, "#{saved_questions} / #{total_questions}"
    # end
  end
  
  def requested_times
    return self.used_in_game_ids.size
  end
  
  def self.import_from_csv(file_read, theme_id)
    encoding = CharDet.detect( file_read )["encoding"]
    return false if encoding != "utf-8" and (encoding =~ /ISO-8859-/) != 0
    if encoding =~ /ISO-8859-/
      text_read = Iconv.iconv("UTF-8", "ISO-8859-1", file_read)
      file_read = text_read.first
    end

    FasterCSV.parse(file_read, :quote_char => '"', :col_sep => ';', :row_sep => :auto) do |row| 
      if ((not row[0] =~ /^(tit[uloes]*\s*)$/i) and (not row[0] =~ /^\s*$/))
        question = Question.new
        answer = Answer.new
        theme = Theme.find(theme_id)
        answer.a = row[ANSWER_A]
        answer.b = row[ANSWER_B]
        answer.c = row[ANSWER_C]
        answer.d = row[ANSWER_D]
        answer.save
        question.title = row[TITLE]
        question.correct = row[CORRECT_ANSWER].downcase
        question.answer = answer
        question.theme = theme
        question.save
      end
    end
    return true
  end

  class << self
    def create_from_suggestion(suggestion)
      answer = Answer.create(:a => suggestion.answer_one, 
                             :b => suggestion.answer_two, 
                             :c => suggestion.answer_three, 
                             :d => suggestion.answer_four )
      
      question = Question.new( :title => suggestion.title, 
                               :theme => suggestion.theme, 
                               :correct => suggestion.correct, 
                               :answer => answer )
      question.save
    end
  end

  def correct_answer?(answer)
    self.correct == answer.downcase
  end

  def wrong_answer?(answer)
    !correct_answer?(answer)
  end
end
