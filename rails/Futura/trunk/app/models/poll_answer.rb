# == Schema Information
#
# Table name: poll_answers
#
#  id               :integer(4)      not null, primary key
#  poll_question_id :integer(4)
#  content          :text
#  created_at       :datetime
#  updated_at       :datetime
#  count            :integer(4)      default(0)
#

class PollAnswer < ActiveRecord::Base
  belongs_to :poll_question
  validates_presence_of :content
end
