# == Schema Information
#
# Table name: games
#
#  id               :integer(4)      not null, primary key
#  room_id          :integer(4)
#  user_ids         :integer(4)
#  started          :boolean(1)      default(FALSE)
#  private          :boolean(1)      default(FALSE)
#  min_users        :integer(4)      default(2)
#  max_users        :integer(4)      default(6)
#  permited_users   :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  difficulty_id    :integer(4)
#  turn             :integer(4)      default(0)
#  identifier       :integer(4)
#  status           :integer(4)
#  started_at       :datetime
#  cicle            :integer(4)
#  quick_start      :boolean(1)      default(FALSE)
#  previous_game_id :integer(4)
#

class Game < ActiveRecord::Base
  INITIALIZED = 3
  STARTED = 4
  ENDED = 5
  ABORTED = 6

  QUESTIONS_PER_GAME = 24
  POINTS_PER_QUESTION = 10
  POINTS_PER_GAME = 200
  TURNS_BEFORE_TIMEOUT = 2
  K = 32

  SPEED_NORMAL = 1
  SPEED_TURBO = 0

  # número de segundos extras no começo do turno, para evitar desvantagens dos jogadores
  EXTRA_TIME = 5

  validates_presence_of :room_id
  has_many :participations #, :include => :user
  has_many :users, :through => :participations
  has_many :cicles
  belongs_to :room
  has_one :owner, :class_name => "User", :foreign_key => 'my_game_id'
  # has_and_belongs_to_many :used_questions, :class_name => "Question"

  validates_numericality_of :min_users, :greater_than_or_equal_to => 2, :less_than => 7
  validates_numericality_of :max_users, :greater_than_or_equal_to => 2, :less_than => 7

  # scope :public, :conditions => "private = 0"
  # scope :not_ended, :conditions => "status != #{ABORTED} and status != #{ENDED}"

  attr_accessor :participations_attributes

  accepts_nested_attributes_for :participations, :allow_destroy => true, :reject_if => proc { |attributes| attributes['user'].blank? }

  def initialized?
    status == INITIALIZED
  end

  def started?
    status == STARTED
  end

  def ended?
    status == ENDED
  end

  def aborted?
    status == ABORTED
  end

  def multiplier
    difficulty_id == SPEED_TURBO ? 1.5 : 1
  end

  def difficulty=(dif)
    self.difficulty_id = dif.id
  end

  def difficulty
    Difficulty.find(self.difficulty_id)
  end

  def full?
    max_users == participations.count(:conditions => { :status => Participation::ACTIVE })
  end

  def active_participations
    participations.find(:all, :conditions => { :status => Participation::ACTIVE }, :include => :user, :order => 'enter_in_game_at')
  end

  def max_cicle_time
    difficulty_id == SPEED_NORMAL ? 60 : 30
  end

  def current_user
    if started?
      unless active_participations.blank?
        if cicle = cicles.find(:first, :order => 'created_at DESC')
          return cicle.user
        else
          return active_participations.first.user
        end
      end
    end
  end

  # @return [User]
  def next_user
    # TODO Era para ser:
    # users.find(current_user_id)

    current_participation = self.current_user.active_participation
    act_part = active_participations

    if current_user && current_participation
      user_index = act_part ? act_part.index(current_participation) : 0

      if next_participation = act_part[user_index + 1]
        next_participation.user
      else
        first_user
      end
    else
      first_user
    end
  end

  def first_user
    active_participations.first.user
  end

  def last_cicle
    cicles.find(:all, :order => 'created_at DESC', :limit => 2).last
  end

  def previous_user
    last_cicle.user if last_cicle
  end

  def last_answer
    if last_cicle && previous_user.last_cicle && self.previous_user.last_cicle.game && self.previous_user.last_cicle.game.id == self.id
      choosed = last_cicle.choosed_answer || "-"
      return self.previous_user.id, last_cicle.correct, choosed
    else
      return nil, nil, nil
    end
  end

  # Return the next cicle
  # @return [Cicle]
  def next_cicle!
    end_cycle
    
    if end_of_game?
      end!
    else
      new_cycle
      return current_cicle
    end
  end

  def end_of_game?
    cicles.where('correct is true').size >= QUESTIONS_PER_GAME
  end

  # @return [NilClass]
  def end_cycle
    cicles.where(:active => true).each { |c| c.update_attribute :active, false }
  end

  # @return [Cicle]
  def new_cycle
    next_user.cicles.create!(:game_id => self.id)
    new_question
  end

  def new_question
    #questions = cicles.map{|c| c.question_id}.compact
    themes = room.theme_ids.uniq

    #conditions = "theme_id in (#{themes.join(',')}) #{ questions.blank? ? '' : 'and id not in(' + questions.join(',') + ')'}"
    conditions = "theme_id in (#{themes.join(',')})"
    total_candidates = Question.count(:conditions => conditions, :select => 'id')
    question = Question.find(:all,
                             :conditions => conditions,
                             :limit => "#{ (rand * total_candidates).to_i }, 1")

    kind = pick_question_kind
    cicle = current_cicle
    cicle.question = question[0]

    cicle.kind = kind
    cicle.save
  end

  def ready?
    self.active_participations.inject(true) { |status, participant| status && participant.ready }
  end

  def self.first_available_identifier(room_id)
    if room_id
      used = Game.find(:all, :conditions => "room_id = #{room_id}").map { |g| g.identifier }.compact

      all = []

      if used.first
        1.upto(used.first.size) { |n| all << n }
        availables = all - used
      else
        availables = [1]
      end

      unless availables.blank?
        return availables.first
      else
        return used.compact.sort.last + 1
      end
    end
  end

  def colors
    @@colors ||= ["verde", "amarelo", "azul", "laranja", "roxo", "vermelho"]
  end

  def color_ids
    (1..6).to_a
  end

  # @return [Array]
  def free_colors
    used_colors = participations_in_lobby_or_playing.collect { |p| p.color } - [nil]
    color_ids - used_colors
  end

  def start!
    if initialized?
      update_attribute :status, STARTED

      participations_in_lobby.each { |p| p.update_attribute(:status, Participation::KICKED) }

      current_user.new_cicle_for_game(self)
      new_question
    end
  end

  def current_cicle
    if user = self.current_user
      user.current_cicle
    end
  end

  def end!
    self.clean_notification

    if started?
      update_attribute(:status, ENDED)
      self.reload
      winner = self.winner

      winner.send_victory_notification if winner

      # faz só um commit com todas as alterações
      Game.transaction do
        self.participations.each do |p|
          if p.status == Participation::ACTIVE
            p.status = Participation::FINISHED
            # adiciona o bônus de pontos por vitória
            p.score = p.score + ((POINTS_PER_GAME * self.multiplier * total_hits) / QUESTIONS_PER_GAME) if p.user == winner
            p.save
          end
          # atualiza a pontuação total do jogador
          p.user.refresh_score
        end

        self.cicles.each do |c|
          c.update_attribute(:active, false)
        end
      end

    elsif initialized?
      # abort!
      update_attribute(:status, ABORTED)
      nil
    else
      nil
    end
  end

  def clean_notification
    Notification.cleanup!(self)
  end

  # @return [Boolean]
  def recalculate_winning_participation
    if participation = self.winning_participation
      new_score = (participation.hits * POINTS_PER_QUESTION * self.multiplier) + ((POINTS_PER_GAME * self.multiplier * total_hits) / QUESTIONS_PER_GAME)
      participation.update_attribute(:score, new_score)
    end
  end

  # return [Numeric]
  def total_hits
    participations.inject(0) { |sum, participation| sum + participation.hits }
  end

  # params [Boolean]
  # return [Participation]
  def winning_participation(final_p = nil)
    final_p ||= self.final_participations(false)

    top_score = final_p.map { |p| p.score }.max
    winners = final_p.find_all { |p| p.score == top_score }

    if winners.blank?
      return nil
    elsif winners.size <= 1
      winners.first
    else
      winners.min { |p1, p2| p1.user.time_in_game(self) <=> p2.user.time_in_game(self) }
    end
  end

  # Obtém o vencedor da partida. Opcionalmente, pode ser passada a lista de participações finais, para evitar busca ao banco.
  # final_p participações finais do jogo (pode ser nulo - nesse caso, será obtido do banco).
  #
  # @param [Boolean]
  # @return [User]
  def winner(final_p = nil)
    winning_participation(final_p).user
  end

  def final_participations_for_list
    self.participations.find(:all, :conditions => { :status => Participation::FINISHED })
  end

  def final_users_for_list
    final_participations_for_list.map { |p| p.user }
  end

  def final_participations(consider_abandomned = true)
    if consider_abandomned
      self.participations.find(:all,
                               :conditions => ["status not in (?, ?)", Participation::KICKED, Participation::IN_LOBBY],
                               :include => :user)
    else
      self.participations.find(:all,
                               :conditions => ["status not in (?, ?, ?)", Participation::KICKED, Participation::ABANDONED, Participation::IN_LOBBY],
                               :include => :user)
    end
  end

  def params_for_replay
    { :previous_game_id => self.id, :difficulty_id => self.difficulty_id,
      :private => self.private, :room_id => self.room_id }
  end

  def final_users
    final_participations.map { |p| p.user }
  end

  def users_playing
    active_participations.select { |participation| participation.status == Participation::ACTIVE }.map { |participation| participation.user }
  end

  def users_in_lobby_or_playing_count
    participations.count(:select => 'id', :conditions => ["status in (?, ?)", Participation::IN_LOBBY, Participation::ACTIVE])
  end

  def users_in_lobby_or_playing
    participations_in_lobby_or_playing.map { |participation| participation.user }
  end

  def participations_in_lobby_or_playing
    self.participations.find(:all,
                             :select => "id, user_id, color",
                             :conditions => ["status in (?, ?)", Participation::IN_LOBBY, Participation::ACTIVE],
                             :include => :user)
  end

  def participations_in_lobby
    # TODO passar para um scope
    self.participations.find(:all, :conditions => "status = #{Participation::IN_LOBBY}")
  end

  def users_in_lobby
    participations_in_lobby.map { |participation| participation.user }
  end

  def users_playing_count
    users_playing.size
  end

  def hits
    self.participations.inject(0) { |sum, participation| sum += participation.hits }
  end

  def start_timer
    self.started_at = Time.now
    self.save
  end

  def self.questions_per_game
    QUESTIONS_PER_GAME
  end


  def pick_question_kind
    case rand(100)
      when 0...2 then
        (rand * -1000000).to_i
      when 2...4 then
        (rand * 1000000).to_i
      else
        0
    end
  end

  def board_state
    self.cicles.where(:correct => true).order('created_at').all.map { |c| c.user.participations.find(:first, :order => "created_at desc").color }
  end

  def state(cycle_id = nil)
    current_player = self.current_user ? self.current_user.id : nil

    if (cycle_id && cycle_id.to_i > 0)
      cycle = Cicle.find(cycle_id)
      choosed = cycle.choosed_answer || "-"
      last_player, correct, option = previous_user.id, cycle.correct, choosed if previous_user
    else
      last_player, correct, option = nil, nil, nil # TODO last_answer é confiável?????? self.last_answer
    end

    question = question_state if self.current_user and (self.status == STARTED)
    board_state = self.board_state

    final_p = self.final_participations

    if ended?
      end_game = true
      game_winner = self.winner(final_p)
    else
      end_game = false
      game_winner = nil
    end

    cicle = time_elapsed = "-"

    unless current_cicle.blank?
      time_elapsed = (Time.now - current_cicle.created_at - EXTRA_TIME).to_i
      cicle = current_cicle.id
    end

    players = final_p.collect do |p|
      if (p.status == Participation::ACTIVE || p.status == Participation::FINISHED)
        score = p.hits * (POINTS_PER_QUESTION * self.multiplier)
      else
        score = 0
      end

      if game_winner && p.user_id == game_winner.id
        bonus = ((POINTS_PER_GAME * self.multiplier * total_hits) / QUESTIONS_PER_GAME)

        p.user.id_and_name.merge({ :color_id => p.color,
                                   :active => p.active?,
                                   :score => score,
                                   :hits => p.hits,
                                   :misses => p.misses,
                                   :bonus => bonus,
                                   :time => p.user.time_in_game(self) })
      else
        p.user.id_and_name.merge({ :color_id => p.color,
                                   :active => p.active?,
                                   :score => score,
                                   :hits => p.hits,
                                   :misses => p.misses,
                                   :time => p.user.time_in_game(self) })
      end
    end

    answer_response = { :player_id => last_player,
                        :answer => option,
                        :correct => correct } if self.status == STARTED

    game_state = { :players => sort_player_list(players),
                   :game_speed => self.difficulty.name.downcase,
                   :turn_id => cicle,
                   :time_elapsed => time_elapsed,
                   :game_id => self.id,
                   :current_player_id => current_player,
                   :answer_response => answer_response,
                   :new_question => question,
                   :table_state => board_state,
                   :end_game => end_game,
                   :current_turn => self.cicles.size }

    game_state
  end

  def question_state
    if self.current_cicle && question = self.current_cicle.question
      question_json = { :title => question.title,
                        :kind => self.current_cicle.kind,
                        :options => { :a => question.answer.a,
                                      :b => question.answer.b,
                                      :c => question.answer.c,
                                      :d => question.answer.d } }
    end
  end

  def sort_player_list(players)
    players.sort do |first, last|
      if first[:hits] > last[:hits]
        -1
      elsif first[:hits] < last[:hits]
        1
      elsif first[:misses] < last[:misses]
        -1
      elsif first[:misses] > last[:misses]
        1
      elsif first[:time] < last[:time]
        -1
      elsif first[:time] > last[:time]
        1
      else
        0
      end
    end #.each_with_index {|value, index| value[:local_ranking] = index + 1}
  end

  def check_turn_timeout
    if current_cicle
      time_elapsed = (Time.now - current_cicle.created_at).to_i

      timeout = difficulty_id == SPEED_NORMAL ? 63 : 33

      if time_elapsed >= timeout
        user = current_user

        user.did_not_answer(max_cicle_time)
        if user.reload.leaved_by_timeout?
          user.exit_game
        end
        next_cicle!
      end
    end
  end
end