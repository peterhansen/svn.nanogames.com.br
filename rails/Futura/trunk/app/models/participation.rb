# == Schema Information
#
# Table name: participations
#
#  id               :integer(4)      not null, primary key
#  points           :integer(4)
#  hits             :integer(4)      default(0)
#  user_id          :integer(4)
#  game_id          :integer(4)
#  created_at       :datetime
#  updated_at       :datetime
#  status           :integer(4)      default(0)
#  turn             :integer(4)      default(0)
#  ready            :boolean(1)      default(FALSE)
#  score            :integer(4)      default(0)
#  misses           :integer(4)      default(0)
#  color            :integer(4)
#  enter_in_game_at :datetime
#

class Participation < ActiveRecord::Base
  belongs_to :user
  belongs_to :game

  ACTIVE    = 0
  FINISHED  = 1
  ABANDONED = 2
  IN_LOBBY  = 3
  KICKED    = 4
  
  def is_my_cicle?
    if game and game.status == Game::STARTED
      user.id == game.current_user.id
    end
  end

  def active?
    # TODO participação KICKED conta como ativa?
    unless self.status == ABANDONED or self.status == IN_LOBBY
      true
    else
      false
    end
  end
end
