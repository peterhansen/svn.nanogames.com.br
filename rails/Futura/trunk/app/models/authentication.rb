class Authentication < ActiveRecord::Base
  attr_accessible :provider, :uid, :oauth_token, :oauth_secret, :user, :username, :email
  belongs_to :user
end