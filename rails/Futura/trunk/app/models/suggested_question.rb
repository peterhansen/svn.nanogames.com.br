# == Schema Information
#
# Table name: suggested_questions
#
#  id           :integer(4)      not null, primary key
#  title        :string(255)
#  answer_one   :string(255)
#  answer_two   :string(255)
#  answer_three :string(255)
#  answer_four  :string(255)
#  theme_id     :integer(4)
#  approved     :boolean(1)      default(FALSE)
#  created_at   :datetime
#  updated_at   :datetime
#  user_id      :integer(4)
#  correct      :string(255)
#

class SuggestedQuestion < ActiveRecord::Base
  belongs_to :theme
  belongs_to :user
  
  validates_presence_of :title
  validates_presence_of :answer_one
  validates_presence_of :answer_two
  validates_length_of   :title,    :maximum=> 250, :allow_nil => true
  validates_length_of   :answer_one,    :maximum=> 100, :allow_nil => true
  validates_length_of   :answer_two,    :maximum=> 100, :allow_nil => true
  validates_length_of   :answer_three,    :maximum=> 100, :allow_nil => true
  validates_length_of   :answer_four,    :maximum=> 100, :allow_nil => true

  scope :approved, :conditions => {:approved => true}
  scope :not_approved, :conditions => {:approved => false}
end
