# == Schema Information
#
# Table name: static_data_games
#
#  id                      :integer(4)      not null, primary key
#  suggest_question_active :boolean(1)
#  created_at              :datetime
#  updated_at              :datetime
#  flash_version           :string(255)     default("0.1")
#  max_online_users        :integer(4)      default(100)
#

class StaticDataGame < ActiveRecord::Base

end
