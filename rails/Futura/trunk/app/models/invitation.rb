# == Schema Information
#
# Table name: invitations
#
#  id           :integer(4)      not null, primary key
#  user_id      :integer(4)
#  friend_email :string(255)
#  token        :string(255)
#  status       :integer(4)
#  created_at   :datetime
#  updated_at   :datetime
#

class Invitation < ActiveRecord::Base
  PENDING = 1
  ACCEPTED = 2

  POINTS = 100

  belongs_to :user

  def confirm!
    if user = User.find_by_confirmation_token(self.token)
      user.inviter = User.find(self.user_id)
      user.save
    end
  end
end
