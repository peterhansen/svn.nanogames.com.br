# == Schema Information
#
# Table name: sponsors
#
#  id         :integer(4)      not null, primary key
#  room_ids   :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class Sponsor < ActiveRecord::Base
  has_many :rooms
end
