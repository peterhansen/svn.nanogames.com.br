# == Schema Information
#
# Table name: notifications
#
#  id         :integer(4)      not null, primary key
#  body       :text
#  active     :boolean(1)      default(TRUE)
#  to_all     :boolean(1)
#  created_at :datetime
#  updated_at :datetime
#  title      :string(255)
#

class Notification < ActiveRecord::Base
  has_and_belongs_to_many :users
  validates_presence_of :body

  def self.active_to(user_id)
    notifications = find_by_sql("SELECT * FROM notifications INNER JOIN notifications_users ON notifications.id = notifications_users.notification_id WHERE (notifications.active = true AND notifications.created_at >= '#{1.hour.ago.to_s(:db)}' AND ( notifications_users.user_id = #{user_id} OR to_all = true ))")
    #notifications = where("active = true AND created_at >= ? AND (user_id = ? OR to_all = true )", 1.hour.ago, user_id)

    if poll = Poll.active
      user_polls = Poll.find_by_sql("SELECT * FROM polls INNER JOIN polls_users ON polls.id = polls_users.poll_id WHERE (polls_users.user_id = #{user_id} )")

      if user_polls.include? poll
        notifications.delete_if { |n| n.id == poll.notification }
      end
    end
    return notifications.sort_by { |n| n.created_at }.reverse
  end

  def disable!
    self.active = false
    save
  end

  def self.cleanup!(game)
    notifications = game.users.map { |u| u.notifications.where("active = true AND created_at >= ?", 1.hour.ago) }.flatten!

    # desabilita desafios para jogos não mais válidos
    Notification.transaction do
      notifications.each do |n|
        sub = n.body[/<a\s+href='\/games\/(\d+)/i] ||
            n.body[/<a\s+href='\/challenges\/accept_challenge\?room_id=(\d+)/i]

        if (sub && game_index = sub[/\d+/])
          g = Game.find_by_id(game_index)

          if (g && (g.ended? || g.aborted?))
            n.active = false
            n.save
          end
        end
      end
    end
  end
end
