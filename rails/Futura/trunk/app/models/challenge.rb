# == Schema Information
#
# Table name: challenges
#
#  user_id        :integer(4)
#  user_id_target :integer(4)
#  status         :integer(4)      default(1)
#  challenged_at  :timestamp
#

class Challenge < ActiveRecord::Base
   PENDING = 1
   INVITED = 2
   
   before_create :set_date
   before_save :set_date
   before_update :set_date
   belongs_to :user                    # the source of the invite
   belongs_to :user_target,            # the target of the invite
    :class_name => 'User', 
    :foreign_key => 'user_id_target'
    
    def set_date
      self.challenged_at = DateTime.now
    end
end
