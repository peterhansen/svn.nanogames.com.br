# == Schema Information
#
# Table name: marital_statuses
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class MaritalStatus < ActiveRecord::Base
  has_many :users
end
