# == Schema Information
#
# Table name: skill_variations
#
#  id         :integer(4)      not null, primary key
#  user_id    :integer(4)      not null
#  amount     :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class SkillVariation < ActiveRecord::Base
  belongs_to :user

  scope :last_week_for_player, lambda { |user|
    { :conditions => "user_id = #{user.id} and created_at > \"#{(Time.now - 7.days).to_date}\"" }
  }

  scope :last_month_for_player, lambda { |user|
    { :conditions => "user_id = #{user.id} and created_at > \"#{(Time.now - 1.month).to_date}\"" }
  }
end
