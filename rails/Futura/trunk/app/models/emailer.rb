# encoding: UTF-8

class Emailer < ActionMailer::Base
  default :from => "Futura CDF <no-reply@cdf.org.br>", :content_type => "text/html", :reply_to => "no-reply@cdf.org.br"

  def invitation(user, recipients, message, token)
    @message = message
    @token   = token

    mail( :subject      => "#{user.complete_name.strip} está te convidando para participar do Futura CDF",
          :recipients   => recipients )
  end

  def contact_us(user_information, message)
    mail( :subject      => "Contato de #{user_information[:name]} #{user_information[:post_name]} através do Futura CD",
          :from         => "#{user_information[:name]} #{user_information[:post_name]} <#{user_information[:email]}>",
          :recipients   => "cdf@futura.org.br",
          :reply_to     => "#{user_information[:email]}",
          :body         => message )
  end

  def suggested_question(user, question)
    @user         = user
    @question     = question

    mail( :subject      => "Sua pergunta foi aprovada!",
          :recipients   => user.email )
  end

  
  def suggest_question_to_admin(question_id, host)
    @question_id  = question_id
    @host         = host

    mail( :subject      => "Futura CDF: nova sugestão de pergunta",
          :recipients   => Admin.all.collect{|a| a.email}.join(",") )
  end
end
