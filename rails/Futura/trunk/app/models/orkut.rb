# == Schema Information
#
# Table name: orkuts
#
#  id               :integer(4)      not null, primary key
#  created_at       :datetime
#  updated_at       :datetime
#  return_http_body :text
#  request_params   :text
#

class Orkut < ActiveRecord::Base
   has_one :consumer_token
   has_many :captchas

   def get_captcha(orkut_user_ids)
    params = []

    orkut_user_ids.each do |ouid|
      params << build_scrap_params(nil, ouid)
    end

    make_post_request(params)
  end

  def current_user
    User.find(self.consumer_token.user_id)
  end

  def access_token
    self.consumer_token.client
  end

  def send_scrap(captcha_answer = nil, message_body = "")
    params = []
    orkut_friends = current_user.orkut_friends.find_all_by_selected_and_status(true, OrkutProfile::PENDING)

    orkut_friends.each do |o_f|
      params << build_scrap_params(o_f.profile_id, message_body)
    end

    if captcha_answer or not captcha_answer.blank?
      captcha_token = self.captchas.last.captcha_token
      captcha_param = build_captcha_params(captcha_answer, captcha_token)
      params = [captcha_param].concat(params)
    end

    make_post_request(params)
  end

  def clear_request!
    if self.request_params
      self.request_params = nil
      self.return_http_body = nil
    end
    self.save
  end

  def last_return_body
    ActiveSupport::JSON.decode(self.return_http_body) if self.return_http_body
  end

  def last_request_params
    ActiveSupport::JSON.decode(self.return_http_body) if self.request_params
  end

  def friends
    make_post_request(build_friend_params)
  end

  def clear_captcha!
    self.captchas = []
    self.save
  end

  def make_post_request(params = [])
    ret = access_token.post(rpc_endpoint, params.to_json, json_header).body
    ret_decoded = ActiveSupport::JSON.decode(ret)
    if params.class == Array and params.first["method"] == "messages.create"
      if ret_decoded.first["error"]
        Captcha.create(:orkut_id => self.id, :captcha_url => ret_decoded.first["error"]["data"]["captchaUrl"],
                       :captcha_token => ret_decoded.first["error"]["data"]["captchaToken"])
        return false
      end
    elsif  params.class != Array and params["method"] == "people.get"
      if not ret_decoded["data"]
        self.update_attribute(:return_http_body, ret)
      else
        OrkutProfile.add_friends!(self.consumer_token.user_id, ret_decoded["data"]["list"]) 
      end
    elsif params.class == Array and params.first["method"] == "captcha.answer"
      if ret_decoded[1]["error"]
        Captcha.create(:orkut_id => self.id, :captcha_url => ret_decoded[1]["error"]["data"]["captchaUrl"],
                       :captcha_token => ret_decoded[1]["error"]["data"]["captchaToken"])
        return false
      end
    end
    return true
  end

  def rpc_endpoint
    "http://sandbox.orkut.gmodules.com/social/rpc"
  end


  def captcha_image(captcha_url)
    access_token.get("http://sandbox.orkut.gmodules.com#{captcha_url}").body
  end

  
  def build_invitation(ouid)
    token = "#{self.user.id}#{ouid.hash}"
    invitation = Invitation.new(:user_id => self.user.id,:token => token , :status => 1)
    invitation.save
    return "http://www.cdf.org.br/users/sign_up?ft=#{token}"
  end

  def build_friend_params(fields = [])
    {
      "method" => 'people.get',
      "params" =>
      {
        "userId" => ["@me"],
        "groupId" => "@friends",
        "fields" =>  fields || ["displayName", "emails", "thumbnailUrl"],
        "count" => "10000"
      }
    }
  end

  def build_scrap_params(orkut_user_id, message_body)
    {
      "method" => 'messages.create',
      "id" => orkut_user_id,
      "params" =>
      {
        "messageType"=>"public_message",
        "userId"=> orkut_user_id,
        "groupId"=>"@friends",
        "message"=>
        {
          "title"=>"sended at: #{DateTime.now}",
          "body"=> "#{message_body}",
          "recipients"=>[1]
        }
      }
    }
  end

  def build_captcha_params(captcha_answer, captcha_token)
    {
      "method" => "captcha.answer",
      "params" =>
      {
        "userId" => ["@me"],
        "groupId" => "@self",
        "captchaAnswer" => captcha_answer,
        "captchaToken" => captcha_token
      }
    }
  end

  def json_header
    {'Content-Type' => 'application/json' }
  end

end

