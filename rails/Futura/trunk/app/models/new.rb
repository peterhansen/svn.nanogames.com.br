# == Schema Information
#
# Table name: news
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#  summary    :string(50)
#  active     :boolean(1)      default(TRUE)
#

class New < ActiveRecord::Base
  validates_presence_of :title, :summary, :body
  scope :last_two, :conditions => {:active=> true}, :order => "created_at DESC", :limit => 2
  
end
