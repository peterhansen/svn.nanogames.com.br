# == Schema Information
#
# Table name: newsletter_contacts
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  post_name  :string(255)
#  email      :string(255)
#  city       :string(255)
#  state_id   :integer(4)
#  country_id :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class NewsletterContact < ActiveRecord::Base
end
