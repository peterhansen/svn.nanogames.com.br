# == Schema Information
#
# Table name: captchas
#
#  id            :integer(4)      not null, primary key
#  orkut_id      :integer(4)
#  captcha_url   :string(255)
#  captcha_token :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Captcha < ActiveRecord::Base
  belongs_to :orkut
end
