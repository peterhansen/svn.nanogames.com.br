#language: pt

Funcionalidade: Login pelo site
  Para poder usar o Futura CDF
  Como um usuário
  Eu quero poder fazer login no site

  Cenário: Tentando acessar o site sem ter feito login
    Dado que eu não estou logado no site
    Quando eu vou para a página inicial
    Então eu devo estar na página de login

  Cenário: Acessando o site depois de ter feito login
    Dado que eu estou logado
    Quando eu vou para a página inicial
    Então eu devo estar na página inicial