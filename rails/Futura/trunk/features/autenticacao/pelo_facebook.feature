#language: pt

Funcionalidade: Login pelo Facebook
  Para poder usar o Futura CDF
  Como um usuário
  Eu quero poder logar pelo facebook

  Cenário: Acessando o site pelo Facebook
    Dado que eu não estou logado no site
    E eu não estou logado no facebook
    Quando eu vou para a página inicial
    Então eu devo estar na página de login do facebook