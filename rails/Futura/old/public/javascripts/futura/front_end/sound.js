$sound=0;
soundManager.url = '/swf/soundmanager2.swf'; // directory where SM2 .SWFs live
soundManager.useFlashBlock = false;
soundManager.debugMode = false;   
soundManager.onload = function() {
    var mySound = soundManager.createSound({
        id: 'aSound',
        url: '/sounds/alerta_notificacao.mp3',
        autoLoad: true,
        autoPlay: false
        // onload: [ event handler function object ],
        // other options here..
    });
    setGlobal(mySound)
}
function setGlobal(my_sound){
    $sound = my_sound;
}
      
function play(){
    $sound.play();
}
