var $j = jQuery.noConflict();
function showTooltip(element, game_id){
  /* Captura a posicao do elemento clicado*/
  var offset = jQuery(element).offset();
  id = jQuery(element).attr('id').split("_")[2];

  /* Centraliza o tooltip capturando a largura do elemento e dividindo pela metade*/
  var tooltipWidth = jQuery(id).width();
  var tooltipCenter = Math.round(tooltipWidth/2);
  var tooltipLeft = offset.left - tooltipCenter + "px";

  /* Margem do tooltip em relacao ao cursor do mouse*/
  var tooltipTop = offset.top + 10 + "px";
  
  new Ajax.Request("/rooms/get_player_list/" + game_id, {
      method: 'get',
      asynchronous:false,
      onSuccess: function(data){
        write_tooltip(data.responseText);
      }
    });

  /* Exibicao do tooltip*/
  jQuery("#jogadores_mesa").css("left", tooltipLeft);
  jQuery("#jogadores_mesa").css("top", tooltipTop);
  jQuery("#jogadores_mesa").show();
}

function hideTooltip() {
  jQuery(".jogadores_mesa").hide();
}

function write_tooltip(response) {
  response_obj = response.evalJSON()
    ul_element = $("list_player");
    ul_element.innerHTML = "";
  if(response_obj.size() <= 0){
    element = new Element("li");
    element.innerHTML = "Nenhum";
  }
  else{
    response_obj.each(function(name, index){ 
      element = new Element("li");
      element.innerHTML = name;
      ul_element.appendChild(element);
    });
  };
}
