// Aplica regra de alinhamento a direita somente aos resultados pares
$timeout_search = null;
document.observe("dom:loaded", function(){
  $$(".resultados_busca li").each(function(li, index){
    result_mod = index % 2;
    if(result_mod != 0){
      li.setStyle("float: right;");
    }
  });

  $$(".toggle_button").each(function(button, index2){
    button.observe("click", toggle_button_callback)
  });

  if ($('search_from') != null){
    from = $('search_from').value
    new Ajax.Autocompleter("query", "autocomplete_choices", "/players/search_ajax?from=" + from, {
      tokens: ',' ,
      paramName: "q",
      minChars: 3,
      frequency: 0.5
    });
  }
    setAttributestoGame();
});


function toggle_button_callback(element){

    type_toggle = this.className.split(" ")[0];
    user_id = this.up().up().id
    if( type_toggle == "bt_desafiar" ){
        inviteChallenge(user_id)
    }
    else{
        removeChallenge(user_id)
    }
}

function setModal(){
    if(window.location.pathname.split("/")[1] == "rankings"){
        constructModal('challenge_list', '/challenges/index_ranking');
    }
    else{
        constructModal('challenge_list', '/challenges');
    }
}

function inviteChallenge( user_id ){
    obj_toggle = toggleRequestChallenge(user_id, "invite")
    //if( obj_toggle["return"] ){
    $('' + user_id).addClassName("selecionado");
    if($('ranking') == null){
        div = $('' + user_id).childElements()[1];
    }
    else {
        div = $('' + user_id).childElements().last();
    }
    bt = div.childElements().last();
    bt.innerHTML = "Desfazer";
    bt.className = "bt_desfazer"

    if( $("invite_button").className.split(" ")[0] == "bt_enviarDesafio_disabled"){
        $("invite_button").className = "bt_enviarDesafio texthide toggle_button";
        $("invite_button").observe("click", setModal);
    }
  /*}
  else{
    alert("Você pode desafiar no máximo " + obj_toggle["max_users"] + " jogadores.");
  }*/
}

function removeChallenge( user_id ){
  toggle = toggleRequestChallenge(user_id, "remove");
  if( toggle["return"] ){
    if( $('' + user_id) != null ){
        $('' + user_id).removeClassName("selecionado");
        if($('ranking') == null){
            div = $('' + user_id).childElements()[1];
        }
        else{
            div = $('' + user_id).childElements().last();
        }
        bt = div.childElements().last();
        bt.innerHTML = "Desafiar";
        bt.className = "bt_desafiar"
    }

    if( toggle["challenges"] == 0 ){
       if(window.location.pathname.split('/')[1] != 'invitations'){
           $("invite_button").className = "bt_enviarDesafio_disabled texthide toggle_button";
           $("invite_button").stopObserving('click');
       }
    }
  }
}

function toggleRequestChallenge(user_id, type)  {
  var return_bool = false;
  var max_users = 0;
  var controller = "players"
  if($('ranking') != null){
    controller = "ranking"
  }
  if ($("orkut_invitation") == null){
    url_toogle = "/challenges/"+ type + "_challenge?user_id=" + user_id + "&ctrl=" + controller ;
  }
  else{
    url_toogle = "/invitations/orkut_invitation?profile_id=" + user_id
  }
  new Ajax.Request(url_toogle, {
    asynchronous:false,
    method: 'get',
    onSuccess: function(data){
      if ($("orkut_invitation") == null){
        data_json = data.responseText.evalJSON();
        challenges = data_json.challenges
        return_bool = data_json.value;
        if( data_json.max_users != null ){

          max_users = data_json.max_users
        }
      }
    }
  });
  if ($("orkut_invitation") != null){
    return_bool = true;
    max_users = 10000;
    challenges = '';
  }
  if(challenges){
    return {"return": return_bool, "max_users": max_users, "challenges": challenges};
  }
  else{
    return {"return": return_bool, "max_users": max_users, "challenges": ''};
  }

}

function removeUserChallengeList(user_id){
  $("challenge_" + user_id).remove();
  if( $$('.challenges_list')[0].childElements().size() < 1 ){
    closeModal($('pop_mesaParticular'))
  }
  removeChallenge( user_id );
}

function pop_user_from_list(user_id){
  if($('ranking') == null){
    if( $(user_id) != null ){
      pos_float = $(user_id).getStyle("float")
      next_element = $(user_id);
      $(user_id).remove();
      if( pos_float == "left" ){
        next_element.setStyle("float: left");
      }
    }
  }
}
function setRoom(room_id){
  setAttributestoGame();
  new Ajax.Request("/challenges/get_themes?room_id=" + room_id, {
    asynchronous:false,
    method: 'get',
    onSuccess: function(themes){
      themes_json = themes.responseText.evalJSON();
      $("themes").innerHTML = themes_json.join(", ")
    }
  })
}

function setAttributestoGame(){
   if($("room_select") != null){
        button = $$('.bt_enviarConvites')[0]
        room_id = $('room_select').value
        difficulty_id = $('difficulty_select').value
        ctrl = "players"
        if($('ranking') != null){
            ctrl = "ranking"
        }
       button.setAttribute("href", "/challenges/send_invites?room_id=" + room_id + "&difficulty_id=" + difficulty_id + "&ctrl=" + ctrl)
   }
}


function filterOrkutFriends(element){
    if($timeout_search){
        clearTimeout($timeout_search)
    }
    $timeout_search = setTimeout("ajaxSearchOrkutFriends('"+ element.value +"')", 0.5 * 1000);
}

function ajaxSearchOrkutFriends(query){
    if((query.length >= 3) || (query.length <= 0)){
        requestSearch(query)
    }
}

function setTriggers(){

    $$(".resultados_busca li").each(function(li, index){
        result_mod = index % 2;
        if(result_mod != 0){
            li.setStyle("float: right;");
        }
    });

    $$(".toggle_button").each(function(button, index2){
        button.observe("click", toggle_button_callback);
    });
}

function requestSearch(query){

    new Ajax.Request("/invitations/search_ajax?query=" + query, {
        method: 'get',
        onSuccess: function(data){
            $$(".resultados_busca")[0].innerHTML = data.responseText;
            setTriggers();
        }
     })

}

/*jQuery(document).ready(function(){
    // Toggle do botão de mostrar selecionados/todos

    var showAll = true; // Inicia mostrando a lista de pessoas completa

    jQuery('.bt_mostrar_toggle').click(function() {
        if (showAll){
            // Mostrar apenas as pessoas selecionadas
            jQuery(this).removeClass('mostrar_selecionados').addClass('mostrar_todos');
            showAll = false;
        }
        else{
            // Mostrar a lista completa
            jQuery(this).removeClass('mostrar_todos').addClass('mostrar_selecionados');
            showAll = true;
        }
    });
});
*/
