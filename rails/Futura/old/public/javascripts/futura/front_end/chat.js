$text_area_input_value=0;
$char_count_value=0;
function startObserveInput(){
    $text_area_input_value = $("input_message").value
    $char_count_value = parseInt($("char_counter").innerHTML);
    Event.observe('input_message', 'keyup', function(e){
        if ((e.keyCode == Event.KEY_RETURN) && (e.ctrlKey)){
            if(($('input_message').value != "\n") && ($('input_message').value != "")){
                sendChatMessage();
            }
        }
    })
}

function sendChatMessage(){
    message_body = $('input_message');
    if(message_body.value.length > $char_count_value){
        return false;
    }
    value_message = message_body.value.gsub("\n", "<br>");
    new Ajax.Request("/chat/message", {
    method: 'post',
    postBody: 'message_body=' + value_message,
    asynchronous:true,
    onCreate: function(){
        message_body.value = '';
        $('char_counter').innerHTML = $char_count_value.toString();
    }
  });
}

function showChatMessage( message_body, nickname, color ){
    show_area = $("show_area_message");
    p_element = new Element('p');
    strong_element = new Element('strong');
    strong_element.className = color;
    strong_element.appendChild( document.createTextNode( nickname ) );
    p_element.appendChild( strong_element );
    p_element = setMessage(message_body, p_element);
    show_area.appendChild( p_element );
    show_area.scrollTop = show_area.scrollHeight
}

function setMessage(message_body, p_element){
    message_splited = message_body.strip().split('<br>');
    message_size = message_splited.size() - 1;
    message_splited.each(function(m, i){
       p_element.appendChild( document.createTextNode( m ) );  
       if( i < message_size){
           br_element = new Element('br');   
           p_element.appendChild(br_element);
        }
    })
    return p_element;
}

function clearTextArea(){
    if($('input_message').value == $text_area_input_value){
        $('input_message').value = '';
    }
}

function fieldMaxLength(field, char_count){
    if($(char_count)){
        char_count_element = $(char_count)
        if( field.value.length > $char_count_value ){
            char_count_element.setStyle({'color': 'red'})
            $('invite_message_button').disable();
            $('invite_message_button').setStyle({'cursor': 'default'})
            $('invite_message_button').setAttribute('src', '/images/comum/bt_enviarMensagem_disabled.png');
            //field.value = field.value.substring(0, max_length)
        }
        else{
            if( char_count_element.getStyle('color') == 'red'){
               char_count_element.setStyle({'color': 'blue'})
               $('invite_message_button').enable();
               $('invite_message_button').setAttribute('src', '/images/comum/bt_enviarMensagem.png');
               $('invite_message_button').setStyle({'cursor': 'pointer'})
            }
        }
        char_count_element.innerHTML = $char_count_value - field.value.length
    }
}

