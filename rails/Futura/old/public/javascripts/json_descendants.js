function checkDescendants(id){
var checked=''
if($('#checkbox_' + id).attr('checked')){
    checked='true'
}
    $.ajax({
        url: '/admin/rooms/get_descendants',
        dataType: 'json',
        data: "theme_id=" + id,
        async: false,
        success: function(descendants){
            for(var index in descendants ){

                var descendant_id = descendants[index].theme.id;
                $('#checkbox_' + descendant_id ).attr('checked', checked)
            }

        }
    });
}