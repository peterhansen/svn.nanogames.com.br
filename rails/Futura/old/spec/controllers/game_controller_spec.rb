require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe GamesController do
  before(:each) do
    sign_in_user(users(:user_not_playing))
  end

  describe "GET /index" do
    it "assigns @game" do
      controller.should_receive(:current_user).at_least(1).times.and_return(users(:user_not_playing))
      Game.should_receive(:first).and_return('game')
      get :index
      assigns(:game).should eql('game')
    end
  end

  describe "#show (via GET)" do
    before(:each) do
      controller.should_receive(:current_user).at_least(1).times.and_return(users(:user_not_playing))
    end
    
    describe "when the game exists and" do
      context "it is full" do
        before(:each) do
          @game = games(:game_full)
        end

        it "redirects to room#show" do
          Game.should_receive(:find_by_id).and_return(@game)
          get :show
          response.should redirect_to(room_path(@game.room))
        end
      end

      context "it is not full" do
        before(:each) do
          @game = games(:game_not_full)
        end

        it "render show template" do
          Game.should_receive(:find_by_id).and_return(@game)
          get :show
          response.should redirect_to(room_path(@game.room))
        end
      end

      context "it is initiated" do
        before(:each) do
          @game = games(:game_initiated)
        end
        
        it "redirects to the room#show" do
          Game.should_receive(:find_by_id).and_return(@game)
          get :show
          response.should redirect_to(room_path(@game.room))
        end
      end

      context "it is over" do
        before(:each) do
          @game = games(:game_finished)
        end

        it "redirects to the room#show" do
          Game.should_receive(:find_by_id).and_return(@game)
          get :show
          response.should redirect_to(room_path(@game.room))
        end
      end
    end

    context "when the the game doesn't exists" do
      it "redirects to room#index" do
        Game.should_receive(:find_by_id).and_return(nil)
        get :show
        response.should redirect_to(rooms_path)
      end
    end
  end
end