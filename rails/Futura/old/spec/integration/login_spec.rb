require File.dirname(__FILE__) + '/../spec_helper'

context "As a not logged in user" do
  before :each do
    visit '/'
  end

  context "When I visit '/'" do
    it "I should see the home screen" do
      current_path.should == '/'
    end

    it "I should see 'jogar pelo site'" do
      page.should have_xpath('//img', :tittle => 'Jogar Pelo Site') #have_content 'Jogar Pelo Site'
    end
  end

  context "When i click 'Jogar Pelo Site'" do
    it "I should see the login form" do
      click_link 'Jogar Pelo Site'
      save_and_open_page
      page.should have_content('E-mail ou apelido')
    end
  end
end
