require File.dirname(__FILE__) + '/../spec_helper'

describe Question do
  it { should belong_to :answer }
  it { should belong_to :theme }
  
  it { should validate_presence_of :title }
  it { should validate_presence_of :answer }
  it { should validate_presence_of :theme }
  it { should validate_presence_of :correct }
  
  it {} # Factory(:question); should validate_uniqueness_of :title }
end
