require File.dirname(__FILE__) + '/../spec_helper'

describe User do
  fixtures :users

  it { should be_invalid }

  it "should be valid with correct attributes" do
    users(:user_2).should be_valid
  end
  
  should_belong_to :my_game
  should_belong_to :room
  should_have_and_belong_to_many :histories
end
