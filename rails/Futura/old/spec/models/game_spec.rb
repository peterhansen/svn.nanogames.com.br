require File.dirname(__FILE__) + '/../spec_helper'

describe Game do
  it "has many users" do
    should have_many(:users, :through => :participations)
  end

  it "has many participations" do
    should have_many(:participations)
  end

  it "has many cicles" do
    should have_many(:cicles)
  end

  it "belongs to room" do
    should belong_to(:room)
  end

  it "has one owner" do
    should have_one :owner
  end

  it "requires room to be set" do
    should validate_presence_of :room_id
  end

  it "requires min_users to be a number" do
    should validate_numericality_of :min_users
  end

  it "requires max_users to be a number" do
    should validate_numericality_of :max_users
  end

  describe "#current_user" do
    context "when game is finished" do
      it "returns nil" do
        games(:game_finished).current_user.should be_nil
      end
    end

    context "when game is running" do
      it "returns nil if there is no active participations" do
        games(:game_initiated).current_user.should be_nil
      end

      it "returns the current user if there is active participations"
    end
  end

  describe "#next_user" do
    before :each do
      @game = games(:game_with_two_players)
    end
    it "returns the next user" do
      @game.should_receive(:current_user).at_least(1).times.and_return(users(:ze))
      @game.next_user.id.should == users(:mane).id
    end

    it "returns the first player if the last player is playing" do
      @game.should_receive(:current_user).at_least(1).times.and_return(users(:mane))
      @game.next_user.id.should == users(:ze).id
    end
  end

  describe "#active_participations" do
    it "returns nil if game is finished" do
      games(:game_finished).active_participations.should == []
    end

    it "return the active participations if game is running" do
      games(:game_with_two_players).active_participations.should == participations(:ze_participation, :mane_participation)
    end
  end

  describe "#full?" do
    it "returns true if game is full" do
      games(:game_full).full?.should be_true
    end

    it "returns false if game is not full" do
      games(:game_not_full).full?.should be_false
    end
  end
end
