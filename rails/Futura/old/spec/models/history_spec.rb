require File.dirname(__FILE__) + '/../spec_helper'
#  has_and_belongs_to_many :users
#  has_and_belongs_to_many :rooms

describe History do
  it { should be_valid }
  
  it { should have_and_belong_to_many :users }
  it { should have_and_belong_to_many :rooms }
end
