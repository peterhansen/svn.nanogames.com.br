require File.dirname(__FILE__) + '/../spec_helper'

describe Answer do
  it { should be_invalid }
  it "should be valid with correct attributes" do
    # Factory(:answer).should be_valid
  end
  
  it { should validate_presence_of(:a) }
  it { should validate_presence_of(:b) }
  
  it { should belong_to(:question) }
end
