require File.dirname(__FILE__) + '/../spec_helper'

describe Theme do
  
  it { should have_many(:questions) }
  it { should have_and_belong_to_many(:rooms) }
  
  it { should_not be_valid }
  it { should validate_presence_of :name }
  it "should validate uniqueness of Name" do
    # Factory(:theme, :name => Factory.next(:name))
    should validate_uniqueness_of :name
  end

  it "should be valid with correct attributes" do
    # Factory.create(:theme, :name => Factory.next(:name)).should be_valid
  end
  
  it "should have 1 levels in the name_with_level" do
    # theme = Factory.create(:theme, :name => "Filho")
    # theme.parent = Factory.create(:theme, :name => "Pai")
    # theme.save
    # theme.reload
    
    # theme.name_with_level.should eql("> Filho")
  end
  
end

describe "Adding Parent" do
  before do
    # @son = Factory.create(:theme, :name => "Son")
    # @dad = Factory.create(:theme, :name => "Dad")
    # @son.parent = @dad
    # @son.save
    # @son.reload
  end
  
  it "should have parent id" do
    # @son.parent_id.should_not be_nil
    # @son.parent_id.should eql(@dad.id)
  end
  
  it "should have ancestours_count of 1" do
    # @son.ancestors_count.should eql(1)
  end
  
  it "should have 1 ancestor" do
    # @son.ancestors.size.should eql(1)
  end
  
end

describe "Adding Children" do

  before do
    # @dad = Factory.create(:theme, :name => "Dad")
    # @son = Factory.create(:theme, :name => "Son")
    # @daughter = Factory.create(:theme, :name => "Daughter")
    
    # @son.parent = @dad
    # @son.save
    
    # @daughter.parent = @dad
    # @daughter.save
    
    # @dad.reload # Sem esse reload os atributos virtuais ficam desatualizados!
  end
  
  it "should have children_count of 2" do
    # @dad.children.size.should eql 2
  end
  
  it "should have descendants_count of 2" do
    # @dad.descendants.size.should eql 2
  end
  
  it "should have 2 descendents" do
    # @dad.descendants.size.should eql 2
  end
  
end

describe "Adding Children and Grandchildren" do
  
  before do
    # @dad = Factory.create(:theme, :name => "Dad")
    # @son = Factory.create(:theme, :name => "Son")
    # @grandson = Factory.create(:theme, :name => "Daughter")
    
    # @son.parent = @dad
    # @son.save
    
    # @grandson.parent = @son
    # @grandson.save
    
    # @dad.reload # Sem esse reload os atributos ficam desatualizados!
  end
  
  it "should have children_count of 1" do
    # @dad.children.size.should eql 1
  end
  
  it "should have descendants_count of 2" do
    # @dad.descendants_count.should eql 2
  end
  
  it "should have 2 descendants" do
    # @dad.descendants.size.should eql 2
  end
  
  it "grandson should have 2 ascendents" do
    # @grandson.ancestors_count.should eql 2
    # @grandson.ancestors.size.should eql 2
  end
  
end
