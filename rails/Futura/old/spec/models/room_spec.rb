require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Room do
  fixtures :rooms, :themes

  before(:each)do
    @room = rooms(:history)
  end
  
  it "requires the presence of name" do
    @room.should validate_presence_of :name
  end
  
  it "has and belongs to many themes" do
    @room.should have_and_belong_to_many :themes
  end
  
  it "belongs to sponsor" do
    @room.should belong_to :sponsor
  end

  it "has many games" do
    @room.should have_many :games
  end

  it "has many users" do
    @room.should have_many :users
  end

  it "has and belongs to many histories" do
    @room.should have_and_belong_to_many :histories
  end
end
