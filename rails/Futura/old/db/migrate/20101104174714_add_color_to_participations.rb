class AddColorToParticipations < ActiveRecord::Migration
  def self.up
    add_column :participations, :color, :integer
  end

  def self.down
    remove_column :participations, :color
  end
end
