class AddSkillMonthVariationAndSkillWeekVariationToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :skill_month_variation, :integer, :default => 0
    add_column :users, :skill_week_variation, :integer, :default => 0
  end

  def self.down
    remove_column :users, :skill_week_variation
    remove_column :users, :skill_month_variation
  end
end
