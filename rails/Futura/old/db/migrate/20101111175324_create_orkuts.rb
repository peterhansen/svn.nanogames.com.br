class CreateOrkuts < ActiveRecord::Migration
  def self.up
    create_table :orkuts do |t|
      t.string :return_http_body
      t.string :request_params
      t.timestamps
    end
  end

  def self.down
    drop_table :orkuts
  end
end
