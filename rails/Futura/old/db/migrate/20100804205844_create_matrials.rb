class CreateMatrials < ActiveRecord::Migration
  def self.up
    create_table :matrials do |t|
      t.string :status

      t.timestamps
    end
  end

  def self.down
    drop_table :matrials
  end
end
