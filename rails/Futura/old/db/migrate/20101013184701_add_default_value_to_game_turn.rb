class AddDefaultValueToGameTurn < ActiveRecord::Migration
  def self.up
    change_column :games, :turn, :integer, :default => 0
  end

  def self.down
    change_column :games, :turn, :integer
  end
end
