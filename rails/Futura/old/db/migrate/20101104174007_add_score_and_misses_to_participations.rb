class AddScoreAndMissesToParticipations < ActiveRecord::Migration
  def self.up
    rename_column :participations, :right_answers, :hits
    add_column :participations, :score, :integer, :default => 0
    add_column :participations, :misses, :integer, :default => 0
  end

  def self.down
    remove_column :participations, :misses
    remove_column :participations, :score
    rename_column :participations, :hits, :right_answers
  end
end
