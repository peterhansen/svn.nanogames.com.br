class AddDigestAvatarColumnToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :digest_avatar, :string
  end

  def self.down
    remove_column :users, :digest_avatar
  end
end
