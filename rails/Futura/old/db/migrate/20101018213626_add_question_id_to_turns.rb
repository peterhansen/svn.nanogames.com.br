class AddQuestionIdToTurns < ActiveRecord::Migration
  def self.up
    add_column :turns, :question_id, :integer
  end

  def self.down
    remove_column :turns, :question_id
  end
end
