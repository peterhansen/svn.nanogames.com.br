class AddCountToPollAnswers < ActiveRecord::Migration
  def self.up
    add_column :poll_answers, :count, :integer, :default => 0
  end

  def self.down
    remove_column :poll_answers, :count
  end
end
