class CreatePollQuestions < ActiveRecord::Migration
  def self.up
    create_table :poll_questions, :force => true do |t|
      t.integer :poll_id
      t.text :content
      t.timestamps
    end
  end

  def self.down
    drop_table :poll_question
  end
end
