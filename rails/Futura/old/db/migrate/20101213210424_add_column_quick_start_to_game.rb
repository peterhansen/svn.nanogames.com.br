class AddColumnQuickStartToGame < ActiveRecord::Migration
  def self.up
    add_column :games, :quick_start, :boolean, :default => false
  end

  def self.down
    remove_column :games, :quick_start
  end
end
