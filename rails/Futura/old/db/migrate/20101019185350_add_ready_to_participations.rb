class AddReadyToParticipations < ActiveRecord::Migration
  def self.up
    add_column :participations, :ready, :boolean, :default => false
  end

  def self.down
    remove_column :participations, :ready
  end
end
