class CreatePolls < ActiveRecord::Migration
  def self.up
    create_table :polls, :force => true do |t|
      t.string :name
      t.string :description
      t.boolean :active, :default => false
      t.datetime :expires_at
      t.timestamps
    end
  end

  def self.down
    drop_table :polls
  end
end
