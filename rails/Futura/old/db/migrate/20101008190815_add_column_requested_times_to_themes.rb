class AddColumnRequestedTimesToThemes < ActiveRecord::Migration
  def self.up
    add_column :themes, :requested_times, :integer, :default => 0
  end

  def self.down
    remove_column :themes, :requested_times
  end
end
