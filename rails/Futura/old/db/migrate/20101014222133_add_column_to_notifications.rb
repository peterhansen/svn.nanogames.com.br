class AddColumnToNotifications < ActiveRecord::Migration
  def self.up
    add_column :notifications, :title, :string
  end

  def self.down
    remove_column :notifications, :title
  end
end
