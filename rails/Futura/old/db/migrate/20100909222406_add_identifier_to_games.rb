class AddIdentifierToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :identifier, :integer
  end

  def self.down
    remove_column :games, :identifier
  end
end
