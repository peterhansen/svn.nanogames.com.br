class AddEnterInGameAtToParticipations < ActiveRecord::Migration
  def self.up
    add_column :participations, :enter_in_game_at, :datetime
  end

  def self.down
    remove_column :participations, :enter_in_game_at
  end
end
