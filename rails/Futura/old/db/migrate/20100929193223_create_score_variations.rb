class CreateScoreVariations < ActiveRecord::Migration
  def self.up
    create_table :score_variations do |t|
      t.integer :user_id, :null => false
      t.integer :amount

      t.timestamps
    end
  end

  def self.down
    drop_table :score_variations
  end
end
