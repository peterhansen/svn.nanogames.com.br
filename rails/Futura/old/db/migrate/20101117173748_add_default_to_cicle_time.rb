class AddDefaultToCicleTime < ActiveRecord::Migration
  def self.up
    change_column :cicles, :time, :integer, :default => 0
  end

  def self.down
    change_column :cicles, :time, :integer
  end
end
