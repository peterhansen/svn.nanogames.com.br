class RemoveOwnerIdColumnToGame < ActiveRecord::Migration
  def self.up
    remove_column :games, :owner_id
  end

  def self.down
  end
end
