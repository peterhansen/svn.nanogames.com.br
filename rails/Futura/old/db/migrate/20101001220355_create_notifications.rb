class CreateNotifications < ActiveRecord::Migration
  def self.up
    create_table :notifications do |t|
      t.string :body
      t.boolean :active
      t.boolean :to_all

      t.timestamps
    end
  end

  def self.down
    drop_table :notifications
  end
end
