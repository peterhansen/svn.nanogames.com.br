class AddUserIdColumnToSuggestedQuestions < ActiveRecord::Migration
  def self.up
    add_column :suggested_questions, :user_id, :integer
  end

  def self.down
    remove_column :suggested_questions, :user_id
  end
end
