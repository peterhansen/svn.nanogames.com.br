class CreateHistories < ActiveRecord::Migration
  def self.up
    create_table :histories do |t|
      t.boolean :started, :default=>false
      t.boolean :private, :default=>false
      t.string :password
      t.integer :min_users, :default=>2
      t.integer :max_users, :default=>6
      t.datetime :game_created_at
      t.datetime :game_finished_at
      t.timestamps
    end
  end

  def self.down
    drop_table :histories
  end
end
