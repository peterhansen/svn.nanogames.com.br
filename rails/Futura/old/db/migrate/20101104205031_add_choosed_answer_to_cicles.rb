class AddChoosedAnswerToCicles < ActiveRecord::Migration
  def self.up
    add_column :cicles, :choosed_answer, :string
  end

  def self.down
    remove_column :cicles, :choosed_answer
  end
end
