class AddSummaryToNews < ActiveRecord::Migration
  def self.up
    add_column :news, :summary, :string, :limit => 50
  end

  def self.down
    remove_column :news, :summary
  end
end
