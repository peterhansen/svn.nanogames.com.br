class AddColumnOrkutIdToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :orkut_id, :string
  end

  def self.down
    remove_column :users, :orkut_id
  end
end
