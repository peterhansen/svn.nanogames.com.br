class CreateGamesQuestions < ActiveRecord::Migration
  def self.up
    create_table "games_questions", :id => false do |t|
      t.integer :game_id
      t.integer :question_id
    end
  end

  def self.down
    drop_table "games_questions"
  end
end
