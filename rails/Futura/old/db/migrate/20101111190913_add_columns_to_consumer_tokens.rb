class AddColumnsToConsumerTokens < ActiveRecord::Migration
  def self.up
    add_column :consumer_tokens, :return_http_body, :string
    add_column :consumer_tokens, :request_params, :string
  end

  def self.down
    remove_column :consumer_tokens, :request_params
    remove_column :consumer_tokens, :return_http_body
  end
end
