class AddColumnToGames < ActiveRecord::Migration
  def self.up
    remove_column :games, :started_at
    add_column :games, :started_at, :datetime
  end

  def self.down
    remove_column :games, :started_at
    add_column :games, :started_at, :time
  end
end
