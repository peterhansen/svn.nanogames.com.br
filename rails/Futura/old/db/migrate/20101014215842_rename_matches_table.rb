class RenameMatchesTable < ActiveRecord::Migration
  def self.up
    rename_table :matches, :participations
  end

  def self.down
    rename_table :participations, :matches
  end
end
