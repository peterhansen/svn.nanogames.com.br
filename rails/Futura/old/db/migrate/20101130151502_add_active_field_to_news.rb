class AddActiveFieldToNews < ActiveRecord::Migration
  def self.up
    add_column :news, :active, :boolean, :default => true
  end

  def self.down
    remove_column :news, :active
  end
end
