class CreateInvitations < ActiveRecord::Migration
  def self.up
    create_table :invitations do |t|
      t.integer :user_id
      t.string :friend_email
      t.string :token
      t.integer :status, :defaul => 1

      t.timestamps
    end
  end

  def self.down
    drop_table :invitations
  end
end
