class AddBlockColumnToAdmins < ActiveRecord::Migration
  def self.up
    add_column :admins, :blocked, :boolean, :default=>false
  end

  def self.down
    remove_column :admins, :blocked
  end
end
