class AddGameColumnToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :game_ids, :integer
  end

  def self.down
    remove_column :rooms, :game_ids
  end
end
