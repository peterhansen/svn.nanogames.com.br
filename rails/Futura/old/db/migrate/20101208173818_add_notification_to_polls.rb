class AddNotificationToPolls < ActiveRecord::Migration
  def self.up
    add_column :polls, :notification, :integer
  end

  def self.down
    remove_column :polls, :notification
  end
end
