class AddTurnColumnToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :turn, :integer
  end

  def self.down
  end
end
