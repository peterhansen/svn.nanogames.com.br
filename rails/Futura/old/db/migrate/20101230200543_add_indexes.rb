class AddIndexes < ActiveRecord::Migration
  def self.up
    add_index :answers, :question_id, :quiet => true

    add_index :captchas, :orkut_id, :quiet => true

    add_index :challenges, :user_id, :quiet => true
    add_index :challenges, :user_id_target, :quiet => true
    add_index :challenges, :status, :quiet => true

    add_index :cicles, :game_id, :quiet => true
    add_index :cicles, :user_id, :quiet => true
    add_index :cicles, :question_id, :quiet => true
    add_index :cicles, :active, :quiet => true

    add_index :consumer_tokens, :user_id, :quiet => true
    
    add_index :friendships , [:user_id, :friend_id, :friendship_status_id], :quiet => true
    add_index :friendships , [:user_id, :friend_id], :quiet => true
    add_index :friendships , :friendship_status_id, :quiet => true
    add_index :friendships , :friend_id, :quiet => true

    add_index :games , :room_id, :quiet => true
    add_index :games , :started, :quiet => true
    add_index :games , :private, :quiet => true
    add_index :games , :difficulty_id, :quiet => true
    add_index :games , :status, :quiet => true
    add_index :games , :cicle, :quiet => true

    add_index :games_questions , [:game_id, :question_id], :quiet => true
    add_index :games_questions , :game_id, :quiet => true
    add_index :games_questions , :question_id, :quiet => true

    add_index :invitations , :user_id, :quiet => true
    add_index :invitations , :status, :quiet => true

    add_index :news , :active, :quiet => true

    add_index :notifications , :active, :quiet => true
    add_index :notifications , :to_all, :quiet => true

    add_index :notifications_users , [:notification_id, :user_id], :quiet => true
    add_index :notifications_users , :notification_id, :quiet => true
    add_index :notifications_users , :user_id, :quiet => true

    add_index :orkut_profiles , :user_id, :quiet => true
    add_index :orkut_profiles , :profile_id, :quiet => true
    add_index :orkut_profiles , :status, :quiet => true

    add_index :participations , :user_id, :quiet => true
    add_index :participations , :game_id, :quiet => true
    add_index :participations , :status, :quiet => true
    add_index :participations , :ready, :quiet => true

    add_index :poll_answers , :poll_question_id, :quiet => true

    add_index :poll_questions , :poll_id, :quiet => true

    add_index :polls , :active, :quiet => true
    add_index :polls , :notification, :quiet => true

    add_index :polls_users , [:poll_id, :user_id], :quiet => true
    add_index :polls_users , :poll_id, :quiet => true
    add_index :polls_users , :user_id, :quiet => true

    add_index :questions , :answer_id, :quiet => true
    add_index :questions , :theme_id, :quiet => true

    add_index :rooms_themes , [:room_id, :theme_id], :quiet => true
    add_index :rooms_themes , :room_id, :quiet => true
    add_index :rooms_themes , :theme_id, :quiet => true

    add_index :suggested_questions , :theme_id, :quiet => true
    add_index :suggested_questions , :approved, :quiet => true
    add_index :suggested_questions , :user_id, :quiet => true

    add_index :themes , :parent_id, :quiet => true

    add_index :users , :last_sign_in_at, :quiet => true
    add_index :users , :name, :length => 30, :quiet => true
    add_index :users , :post_name, :length => 30, :quiet => true
    add_index :users , :nickname, :length => 30, :quiet => true
    add_index :users , :orkut_id, :length => 40, :quiet => true
    add_index :users , :my_game_id, :quiet => true
    add_index :users , :skill, :quiet => true
    add_index :users , :score, :quiet => true
    add_index :users , :inviter_id, :quiet => true
    add_index :users , :current_session_id, :length => 40, :quiet => true
    add_index :users , :skill_month_variation, :quiet => true
    add_index :users , :skill_week_variation, :quiet => true
    add_index :users , :questions_skipped, :quiet => true
  end

  def self.down
  end
end
