class AddColumnOwnerIdToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :owner_id, :integer, :null => false
  end

  def self.down
    remove_column :games, :owner_id
  end
end
