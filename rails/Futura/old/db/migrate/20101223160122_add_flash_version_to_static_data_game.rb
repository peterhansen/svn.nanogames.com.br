class AddFlashVersionToStaticDataGame < ActiveRecord::Migration
  def self.up
    add_column :static_data_games, :flash_version, :string, :default => '0.1'
  end

  def self.down
    remove_column :static_data_games, :flash_version
  end
end
