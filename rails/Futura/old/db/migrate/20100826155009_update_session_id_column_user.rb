class UpdateSessionIdColumnUser < ActiveRecord::Migration
  def self.up
    rename_column :users, :session_id, :room_user_id
    add_column    :users, :game_player_id, :string, :default => false
  end

  def self.down
  end
end
