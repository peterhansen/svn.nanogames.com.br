class CreateNewsletterContacts < ActiveRecord::Migration
  def self.up
    create_table :newsletter_contacts do |t|
      t.string :name
      t.string :post_name
      t.string :email
      t.string :city
      t.integer :state_id
      t.integer :country_id

      t.timestamps
    end
  end

  def self.down
    drop_table :newsletter_contacts
  end
end
