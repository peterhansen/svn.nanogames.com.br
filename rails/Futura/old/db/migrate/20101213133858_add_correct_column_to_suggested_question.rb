class AddCorrectColumnToSuggestedQuestion < ActiveRecord::Migration
  def self.up
    add_column :suggested_questions, :correct, :string
  end

  def self.down
    remove_column :suggested_questions, :correct
  end
end
