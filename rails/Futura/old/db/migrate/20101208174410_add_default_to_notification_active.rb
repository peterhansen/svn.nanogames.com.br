class AddDefaultToNotificationActive < ActiveRecord::Migration
  def self.up
    change_column :notifications, :active, :boolean, :default => true
  end

  def self.down
    change_column :notifications, :active, :boolean
  end
end
