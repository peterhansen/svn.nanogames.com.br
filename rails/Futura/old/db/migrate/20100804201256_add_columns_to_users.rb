class AddColumnsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :name, :string
    add_column :users, :post_name, :string
    add_column :users, :matrial_id, :integer
    add_column :users, :city, :string
    add_column :users, :about, :string
    add_column :users, :state_id, :integer
    add_column :users, :country_id, :integer
    add_column :users, :gender_id, :integer
    add_column :users, :born_day, :string
    add_column :users, :born_month, :string
    add_column :users, :born_year, :string
    add_column :users, :nickname, :string
    add_column :users, :show_email, :boolean
    add_column :users, :show_age, :boolean
    add_column :users, :receive_newsletter, :boolean
    add_column :users, :agreed_terms, :boolean      

  end

  def self.down
    remove_column :users, :room_id
  end
end
