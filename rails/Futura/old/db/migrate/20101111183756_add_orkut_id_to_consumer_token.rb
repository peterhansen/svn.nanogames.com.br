class AddOrkutIdToConsumerToken < ActiveRecord::Migration
  def self.up
    add_column :consumer_tokens, :orkut_id, :integer
  end

  def self.down
    remove_column :consumer_tokens, :orkut_id
  end
end
