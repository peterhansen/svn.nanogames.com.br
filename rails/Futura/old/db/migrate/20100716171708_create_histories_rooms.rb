class CreateHistoriesRooms < ActiveRecord::Migration
  def self.up
    create_table "histories_rooms", :id => false do |t|
      t.integer :room_id
      t.integer :history_id
    end
  end

  def self.down
  end
end
