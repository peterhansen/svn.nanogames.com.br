class AddColumnToOrkuts < ActiveRecord::Migration
  def self.up
    remove_column :orkuts, :return_http_body
    remove_column :orkuts, :request_params
    add_column :orkuts, :return_http_body, :text
    add_column :orkuts, :request_params, :text
  end

  def self.down
    remove_column :orkuts, :request_params
    remove_column :orkuts, :return_http_body
    add_column :orkuts, :return_http_body, :string
    add_column :orkuts, :request_params, :string
  end
end
