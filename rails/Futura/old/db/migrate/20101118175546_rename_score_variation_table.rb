class RenameScoreVariationTable < ActiveRecord::Migration
  def self.up
    rename_table :score_variations, :skill_variations
  end

  def self.down
    rename_table :skill_variations, :score_variations
  end
end
