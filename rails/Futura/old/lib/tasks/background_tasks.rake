namespace :background do
  desc 'Executa o delayed_job caso ele nao esteja rodando'
  task :delayed_job do
    if `ps ax | grep 'delayed_job' | grep -v 'background:delayed_job'| grep -v 'grep'`.blank?
      `#{RAILS_ROOT}/script/delayed_job start`
    end
  end

  desc 'Executa o memcached caso ele nao esteja rodando'
  task :memcached do
    if `ps ax | grep 'memcache' | grep -v 'background:memcached'| grep -v 'grep'`.blank?
      `memcached -u rails -d`
    end
  end

  desc 'Executa o juggernaut caso ele nao esteja rodando'
  task :juggernaut do
    if `ps ax | grep 'node' | grep 'juggernaut_production' | grep -v 'grep'`.blank?
      `cd $home/lib/juggernaut && sudo node server.js`
    end
  end

  desc 'Executa o sphinx caso ele nao esteja rodando'
  task :sphinx do
    if `ps ax | grep 'searchd' | grep -v 'grep'`.blank?
      `cd #{File.dirname(File.expand_path(__FILE__))} && cd ../.. && /usr/bin/rake ts:config && searchd --config config/production.sphinx.conf`
    end
  end

  desc 'Executa o redis caso ele nao esteja rodando'
  task :redis do
    if `ps ax | grep 'redis' | grep -v 'background:redis' | grep -v 'grep'`.blank?
      `cd $home/lib/redis/src && ./redis-server ../redis.conf`
    end
  end
end