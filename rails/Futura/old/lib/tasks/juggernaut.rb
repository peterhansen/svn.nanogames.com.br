require File.expand_path(File.dirname(__FILE__) + '/utilities')

Capistrano::Configuration.instance(:must_exist).load do
  namespace :juggernaut do
    desc "Starts the juggernaut push server"
    task :start, :roles => juggernaut_role do
      utilities.with_role(juggernaut_role) do
        puts "Starting juggernaut push server"
        sudo "#{base_ruby_path}/bin/juggernaut -c #{juggernaut_config} -d --log #{juggernaut_log}"
      end
    end

    desc "Stops the juggernaut push server"
    task :stop, :roles => juggernaut_role do
      utilities.with_role(juggernaut_role) do
        puts "Stopping juggernaut push server"
        sudo "#{base_ruby_path}/bin/juggernaut -k -c #{juggernaut_config} * --log #{juggernaut_log}"
      end
    end

    desc "Restarts the juggernaut push server"
    task :restart, :roles => juggernaut_role do
      utilities.with_role(juggernaut_role) do
        juggernaut.stop
        juggernaut.start
      end
    end

    desc "Symlinks the shared/config/juggernaut yaml to release/config/"
    task :symlink_config, :roles => :app do
      try_sudo "ln -s #{shared_path}/config/juggernaut.yml #{release_path}/config/juggernaut.yml"
    end

    desc "Displays the juggernaut log from the server"
    task :tail, :roles => :app do
      stream "tail -f #{shared_path}/log/juggernaut.log"
    end

    desc 'Displays if juggernaut is running or not'
    task :status, :roles => :app do
      run "ps ax | grep juggernaut" do |channel, stream, text|
        if text.match '/usr/local/bin/juggernaut'
          logger.info 'Juggernaut is running'
        else
          logger.info 'Juggernaut is not running'
        end
      end
    end
  end
end
