namespace :ranking do
  desc "Reset users patents"
  task :reset_patents => :environment do
    puts "Reseting users patents..."
    User.refresh_patents
  end

  desc "Reset monthly ranking"
  task :reset_month => :environment do
    puts "Reseting monthly ranking..."
    User.reset_month_scores
  end

  desc "Reset weekly ranking"
  task :reset_week => :environment do
    puts "Reseting weekly ranking..."
    User.reset_week_scores
  end
end