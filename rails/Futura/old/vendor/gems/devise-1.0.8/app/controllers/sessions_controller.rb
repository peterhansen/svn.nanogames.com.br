class SessionsController < ApplicationController
  prepend_before_filter :require_no_authentication, :only => [ :new, :create ]
  include Devise::Controllers::InternalHelpers
  layout nil
  # GET /resource/sign_in
  def new
    if params[:unauthenticated] == "true" and resource_name == :user
      session[:callback_page_sign_in] = request.session[:"user.return_to"]
      redirect_to :controller => 'home', :action => 'index', :pop => 'login'
    elsif resource_name == :admin
      session[:callback_page_sign_in] = request.session[:"admin.return_to"] || "/admin/questions"
    else
      if params[:i] == "true"
        session[:invite] = true
      end

      unless flash[:notice].present?
        Devise::FLASH_MESSAGES.each do |message|
          set_now_flash_message :alert, message if params.try(:[], message) == "true"
        end
      end
  
      build_resource
      render_with_scope :new 
    end
  end

  # POST /resource/sign_in
  def create
    if resource_name == :user and User.how_many_online >= StaticDataGame.first.max_online_users
      session[:login_error_message] = "Capacidade máxima de usuários atingida. Tente novamente mais tarde."
      redirect_to :back
    else
      warden.default_strategies :site if resource_name == :user
  #    expire_fragment('online_users')
      if resource = authenticate(resource_name)
        resource.cleanup_notifications_and_challenges! if resource_name == :user
        set_flash_message :notice, :signed_in
        # if session[:invite] == true
        #   redirect_to :controller => :invitations, :action => :new
        # else
          sign_in_and_redirect(resource_name, resource)
        # end
      elsif [:custom, :redirect].include?(warden.result)
        throw :warden, :scope => resource_name
      elsif warden.message == :unconfirmed
         session[:confirmed_error_message] = "Você está tentando entrar com um endereço de e-mail que ainda não foi confirmado. Por favor verifique sua conta de e-mail e confirme sua conta, ou insira seu e-mail abaixo novamente para reenviar a mensagem."
         redirect_to :controller => :home
      else
        set_now_flash_message :alert, (warden.message || :invalid)
        clean_up_passwords(build_resource)
        session[:login_error_message] = I18n.t :invalid, :scope => 'devise.sessions'
        redirect_to :back
      end
    end
  end

  # GET /resource/sign_out
  def destroy
    current_user.cleanup_notifications_and_challenges! if current_user and resource_name == :user
    current_user.exit_site if current_user and resource_name == :user
    expire_fragment('online_users')
    set_flash_message :notice, :signed_out if signed_in?(resource_name)
    sign_out_and_redirect(resource_name)
  end

  protected

    def clean_up_passwords(object)
      object.clean_up_passwords if object.respond_to?(:clean_up_passwords)
    end
end

