class ConfirmationsController < ApplicationController
  include Devise::Controllers::InternalHelpers
  layout nil
  # GET /resource/confirmation/new
  def new
    build_resource
    render_with_scope :new
  end

  # POST /resource/confirmation
  def create
    self.resource = resource_class.send_confirmation_instructions(params[resource_name])

    if resource.errors.empty?
      set_flash_message :notice, :send_instructions
      session[:confirmed_message] = params[:user][:email]
    else
      session[:confirmed_error_message] = "Por favor coloque um e-mail válido"
    end
    redirect_to :controller => 'home'
  end

  # GET /resource/confirmation?confirmation_token=abcdef
  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])

    if resource.errors.empty?
      set_flash_message :notice, :confirmed
      sign_in_and_redirect(resource_name, resource)
    else
      render_with_scope :new
    end
  end
end
