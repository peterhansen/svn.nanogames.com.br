ActionController::Routing::Routes.draw do |map|
  map.resources :oauth_consumers, :member => { :callback => :get }

  map.devise_for :admins
  map.devise_for :users

  map.namespace :admin do |admin|
    admin.root      :controller=> :themes
    admin.resources :questions, :collection => { :create_question_from_csv =>  :post, :import_csv => :get }
    admin.resources :themes,    :collection => { :get_childrens =>             :get }
    admin.resources :rooms,     :collection => { :get_themes_for_select =>     :get, :get_descendants => :get}
    admin.resources :users,     :collection => { :mailing =>                   :get, 
                                                 :update_max_online_users =>   :put, :config_users => :get }
    admin.resources :admins
    admin.resources :notifications
    admin.resources :news
    admin.resources :polls,     :collection => { :report => :get, :update_active => :post }
    admin.resources :suggested_questions, :collection => {:toggle_suggest_question => :get}
    map.admin_news 'admin/news', :controller => :news, :action => :index, :conditions => {:method => :get}
    map.connect 'admin/news', :controller => :news, :action => :create, :conditions => {:method => :post}
  end

  map.connect '/users', :controller => :home, :conditions => {:method => :get}

  map.root                          :controller => :home
  map.noticias          "noticias", :controller => :home, :action => :news
  map.error_juggernaut  "/error",   :controller => :home, :action => :error_juggernaut
  map.pull_test         "/polling", :controller => :home, :action => :polling
  map.getTime           "/getTime", :controller => :home, :action => :getTime

  map.resources :rooms, :collection => {:exit =>              :get,
                                        :set_user =>          :get,
                                        :get_room_partial =>  :get,
                                        :get_game_info =>     :get,
                                        :get_player_list =>   :get}

  map.connect   'games/quick_start', :controller => :games, :action => :quick_start

  map.resources :games, :collection => {:get_question =>      :get,
                                        :quick_start =>       :get,
                                        :users_list=>         :get,
                                        :verify_answer=>      :get,
                                        :new_turn=>           :get,
                                        :get_velocity=>       :get,
                                        :set_user =>          :get,
                                        :request_user_list => :get,
                                        :lobby =>             :get,
                                        :confirm_exit =>      :get,
                                        :load_flash =>        :get,
                                        :get_timer =>         :get,
                                        :get_background_image => :get,
                                        :game_ready =>        :get,
                                        :exit =>              :get,
                                        :exit_and_redirect => :get,
                                        :state_update =>      :get,
                                        :update_player_list => :get,
                                        :start_game =>        :get,
                                        :remaining_time     => :get,
                                        :replay_game        => :get}

  map.resources :players, :collection => {:friends => :get, :all => :get, :search => :get, :ranking => :get}

  map.resources :challenges, :collection => {:invite_challenge => :get, :remove_challenge => :get,
                                             :send_invites => :get, :get_themes => :get,
                                             :accept_challenge => :get, :index => :get,
                                             :send_invites_ranking => :get, :index_ranking => :get}

  map.resources :chat, :collection => {:message => :post}
  map.resources :profiles, :collection => { :add_friend => :get }
  map.resources :invitations, :except => [:index, :edit, :update, :delete], :collection => { :orkut => :get, :register_orkut => :get, :orkut_invitation => :get , :show_captcha => :get, :captcha => :get, :send_orkut_invitation => :post, :search_ajax => :get}
  map.resources :polls, :only => :show, :collection => { :success => :get }

  map.rankings "/rankings", :controller => 'rankings'
  map.monthly_rankings "/rankings/monthly", :controller => 'rankings', :action => 'monthly'
  map.weekly_rankings "/rankings/weekly", :controller => 'rankings', :action => 'weekly'
  map.friend_rankings "/rankings/friends", :controller => 'rankings', :action => 'friends'


  map.notifications_disable "/notifications/set_disable", :controller => "notifications",
  :action => "set_disable"
  map.connect "/notifications/get_notifications", :controller => "notifications", :action => :get_notifications
  map.connect "/im_alive", :controller => "application", :action => 'im_alive'

  map.connect "/suggest_question", :controller => "home", :action => "suggest_question"
  map.connect "/orkut_index", :controller => 'pages', :action => 'index_orkut'
  
  map.game_sessions '/exit', :controller =>'game_sessions', :action =>'exit'

  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
  
end

