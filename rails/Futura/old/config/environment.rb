require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  config.time_zone = 'UTC'
  config.i18n.default_locale = :'pt-br'
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.default_charset = 'UTF-8'
  config.action_mailer.smtp_settings = {
    :address => 'mail.nanogames.com.br',
    :authentication => :login,
    :domain => 'nanogames.com.br',
    :user_name => 'noreply@nanogames.com.br',
    :password => 'J2J3Q6aR',
  }

  require "memcache"
  CACHE = MemCache.new('127.0.0.1')

  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true 
  config.action_controller.session = {
    :key          => '_futura',
    :cache        => CACHE,
    :secret       => '99d590ebcd668c51e0743f9bf2bf49627dce65e85fe4f2c00b41e077f822657d4a894e464bc092ca687e65b723b891084e55fd4ba96042f2d4dd1b09d8665d17'
  }


  log_file = File.open('log/dev.log', 'a')
  log_file.sync = true
  DEV_LOG = Logger.new(log_file)
end
