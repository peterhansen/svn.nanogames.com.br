require "#{RAILS_ROOT}/lib/pagination_link_render"
require "#{RAILS_ROOT}/lib/customize"
WillPaginate::ViewHelpers.pagination_options[:renderer] = 'PaginationListLinkRenderer'
WillPaginate::ViewHelpers.pagination_options[:class] = ''
