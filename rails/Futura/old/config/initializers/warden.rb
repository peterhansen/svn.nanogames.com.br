  # Setup Session Serialization
class Warden::SessionSerializer
  def serialize(record)
    [record.class, record.id]
  end

  def deserialize(keys)
    klass, id = keys
    klass.find(:first, :conditions => { :id => id })
  end
end
Warden::Strategies.add(:orkut_sign) do
  def authenticate!
    u = User.find_all_by_orkut_id(
      params[:ouid]
    ).last
    u.nil? ? fail!("Couldn't log in") : success!(u)
  end
end

 Warden::Strategies.add(:site) do

    def valid?
      if params[:user][:email] and params[:user][:password]
        return true
      else
        return false
      end
    end
    
    def authenticate!
      u = User.find_by_email_or_nickname(params[:user][:email])
      if u and u.encrypted_password == u.send("password_digest", params[:user][:password])
        success!(u)
      else
        fail!("Couldn't log in")
      end
    end
end
    

