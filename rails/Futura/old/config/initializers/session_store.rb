# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
#ActionController::Base.session = {
#  :key         => '_Futura_session',
  #:expire_after => 30.minutes.to_i,
#  :secret      => #'99d590ebcd668c51e0743f9bf2bf49627dce65e85fe4f2c00b41e077f822657d4a894e464bc092ca687e65b723b891084e55fd4ba96042f2d4dd1b09d8665d17'
#}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
