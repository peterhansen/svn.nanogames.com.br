@env = {
  :PATH  => '/usr/local/bin:/usr/bin:/bin',
  :SHELL => '/bin/bash'
}

every :monday, :at => '4:00am' do
  rake "ranking:reset_week"
end

every 1.month, :at => 'start of the month at 4:00am' do
  rake "ranking:reset_month"
end

every 5.minutes do
  rake 'background:delayed_job'
  rake 'background:juggernaut'
  rake 'background:memcached'
  rake 'background:sphinx'
  rake 'background:redis'
  rake 'ts:reindex'
end

# Reconfiguração do Sphinx
every 1.day, :at => '1:30am' do
  rake "ts:stop"
  rake "ts:config"
  rake "ts:index"
  rake "ts:start"
end