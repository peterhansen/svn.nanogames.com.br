  role :web, "futura.nanogames.com.br"
  role :app, "futura.nanogames.com.br"
  role :db, "futura.nanogames.com.br", :primary => true
  set(:deploy_to) {"/var/www/rails/#{application}"}
  set(:juggernaut_yml) { "juggernaut_homolog.yml" }
  set(:juggernaut_hosts_yml) { "juggernaut_homolog_hosts.yml" }
  set :stage, 'production'
