#passowrd: @$!nT@T!c0
role :web, "187.84.228.69"
role :app, "187.84.228.69"
role :db, "187.84.228.69", :primary => true
set(:deploy_to) {"/var/www/rails/#{application}"}
set(:juggernaut_yml) { "juggernaut_production.yml" }
set(:juggernaut_hosts_yml) { "juggernaut_production_hosts.yml" }
set :stage, "production"
