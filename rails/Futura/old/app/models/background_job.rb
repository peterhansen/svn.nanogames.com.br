class BackgroundJob
  def initialize(object_or_class, method_name, *params)
    @object_or_class = object_or_class
    @method = method_name
    @params = *params
  end
  
  def perform 
    if @object_or_class.class == String and @object_or_class =~ /^[A-Z]/
      if @params.blank?
        @object_or_class.constantize.send(@method)
      else
        @object_or_class.constantize.send(@method, *@params)
      end
    else
      if @params.blank?
        @object_or_class.send(@method)
      else
        @object_or_class.send(@method, *@params)
      end
    end      
  end
end
