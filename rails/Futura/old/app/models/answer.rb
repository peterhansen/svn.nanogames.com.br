class Answer < ActiveRecord::Base
  validates_presence_of :a, :b
  belongs_to :question
end
