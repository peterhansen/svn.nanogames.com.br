class Theme < ActiveRecord::Base
  before_save :level_in_name
  after_save :update_in_children_name
  before_destroy :save_questions

  has_and_belongs_to_many :rooms
  has_many :questions
  has_many :suggested_questions
  
  attr_accessor :children_count, :ancestors_count, :descendants_count
  
  acts_as_category :foreign_key => 'parent_id',  
                   :hidden => 'hidden',
                   :children_count => 'children_count',
                   :ancestors_count => 'ancestors_count',
                   :descendants_count => 'descendants_count',
                   :position => 'position',
                   :order_by => 'name, id ASC'
                   
  validates_presence_of :name
  validates_uniqueness_of :name

  named_scope :root, :conditions => {:ancestors_count, 0}

  def self.find_all_root
    Theme.find_all_by_ancestors_count(0)
  end
    
  # atualizar os filhos quando o pai é atualizado
  def update_in_children_name
    self.descendants.each do |children|
      children.save
    end
  end
  
  def descendants_count
    self.descendants.size
  end
  
  def ancestors_count
    self.ancestors.size
  end
                       
  # apaga as questões se não tiver pai
  # passa as questões para o pai
  def save_questions
    if self.parent_id.nil?
      self.questions.each do |q|
        q.destroy
      end
    else
      self.questions.each do |question|
        question.theme = self.parent
        question.save
      end
    end
  end
  
  def get_question( game_id )
#    theme_questions = self.questions.collect {|q| q if not q.used_in_game_ids.include?(game_id)} - [nil]
    theme_questions = self.questions
    unless theme_questions.empty?
      questions_to_choice = [theme_questions.first]
      theme_questions.each do |question|
        if question.requested_times < questions_to_choice.first.requested_times
          questions_to_choice = [question]
        elsif question.requested_times == questions_to_choice.first.requested_times and not question.id == questions_to_choice.first.id
          questions_to_choice << question
        end
      end
      question_selected = questions_to_choice.choice
      game = Game.find(game_id)
      question_selected.used_in_games << game
      question_selected.save
      return question_selected
    end
  end
  
protected
  
  # atualiza name_with_level
  def level_in_name
    if self.parent_id?
      levels = ">" * self.ancestors.size
      levels << " "
    else
      levels = ""
    end
    self.name_with_level = "#{levels}#{self.name}"
  end  
end
