class Poll < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_many :poll_questions
  validates_presence_of :name, :description
  accepts_nested_attributes_for :poll_questions,
                                :allow_destroy => true,
                                :reject_if => proc { |attributes| attributes['content'].blank? }

  named_scope :polls_slim, :select => "id"

  POINTS = 200

  def answered_by(user)
    self.users << user
    user.refresh_score()
  end

  class << self
    def active
      find_by_active(true)      
    end

    def has_active?
      !find_all_by_active(true).blank?
    end
  end
end
