class State < ActiveRecord::Base
  has_many :users
  belongs_to :country
  
  def self.my_state_name(id)
   if state = State.find_by_id(id)
     return state.name
   else
     return ""
   end
  end
  
end
