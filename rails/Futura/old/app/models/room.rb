class Room < ActiveRecord::Base
  validates_presence_of :name
  has_and_belongs_to_many :themes
  belongs_to :sponsor
  has_many :games, :include => :participations
  has_many :users, :include => :polls #TODO não seria has_many :users, :thought => :games
  has_and_belongs_to_many :histories

#  validates_numericality_of :capacity, :only_integer => true, :message => "deve ser um número inteiro."
#  validates_inclusion_of :capacity, :in => 100..99999999, :message => "a capacidade mínima é de 100 jogadores."
  
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :background, :styles => { :large => "530x411>", :medium => "300x300>", :thumb => "100x100>" }
  
  #TODO
  #validates_numericality_of :capacity, :greater_than => 2, :less_than_or_equal_to => 100
#  validates_presence_of :themes
  
  # intervalo da "faxina" de salas velhas
  CLEANUP_INTERVAL = 5.minutes
  
  def full?
    false  # Canal Futura pediu para não ter limite nas salas (28/12/2010) self.users.size == self.capacity
  end

  def online_users_count
    valid_games = games.initialized_or_started
    
    # Faz uma "faxina" em jogos ativos cujos jogadores estão offline e/ou a última ação tenha ocorrido há muito tempo (mais que 15 minutos, por exemplo)
    last_cleanup_time = CACHE.get( "last_cleanup_time_#{self.id}" ) || 1.day.ago
    
    if last_cleanup_time < CLEANUP_INTERVAL.ago
      valid_games.each do |game|
        # se houver uma participação de um jogo ativo cuja última atualização tenha sido há mais de 15 minutos, encerra a partida
        if game.participations.last( :select => 'updated_at', :conditions => [ "updated_at < ?", 15.minutes.ago ] )
          Room.delay.cleanup_old_game( game )
          valid_games -= [ game ]
        end
      end
     
      CACHE.set( "last_cleanup_time_#{self.id}", Time.now )
    end
    
    Participation.count( :conditions => [ "game_id IN (?) AND status IN (?, ?)", valid_games, Participation::IN_LOBBY, Participation::ACTIVE ] )
  end

  def themes_with_children
    theme_all = self.themes
    theme_all.concat(self.themes.collect { |t_a| t_a.descendants })
    theme_all.uniq
  end

  def get_theme
    room_themes = self.themes_with_children.collect {|t| t if not t.questions.empty?} - [nil]
    themes_to_choice = [room_themes.first]
    room_themes.each do |theme|
      if theme.requested_times < themes_to_choice.first.requested_times
        themes_to_choice = [theme]
      elsif theme.requested_times == themes_to_choice.first.requested_times and not theme.id == themes_to_choice.first.id
        themes_to_choice << theme
      end
    end
    theme_selected = themes_to_choice.choice
    theme_selected.requested_times += 1
    theme_selected.save
#    TODO themes_to_choice.choice.increase(:requested_times)
    theme_selected
  end
  
  
  ########################################### MÉTODOS PRIVADOS ################################################
  
  private
  
  def self.cleanup_old_game( g )
    g.end!
  end
  
end
