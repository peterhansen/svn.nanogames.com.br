class PollAnswer < ActiveRecord::Base
  belongs_to :poll_question
  validates_presence_of :content
end
