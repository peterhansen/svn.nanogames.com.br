class Result
  include Comparable
  attr_accessor :score, :value

  def initialize(score)
    @score = score
  end

  def <=>(other)
    self.score <=> other.score
  end
end

