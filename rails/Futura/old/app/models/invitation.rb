class Invitation < ActiveRecord::Base
  PENDING = 1
  ACCEPTED = 2

  POINTS = 100

  belongs_to :user

  def confirm!
    if user = User.find_by_confirmation_token(self.token)
      user.inviter = User.find(self.user_id)
      user.save
    end
  end
end
