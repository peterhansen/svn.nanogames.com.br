class New < ActiveRecord::Base
  validates_presence_of :title, :summary, :body
  named_scope :last_two, :conditions => {:active=> true}, :order => "created_at DESC", :limit => 2
  
end
