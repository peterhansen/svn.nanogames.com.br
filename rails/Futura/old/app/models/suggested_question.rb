class SuggestedQuestion < ActiveRecord::Base
  belongs_to :theme
  belongs_to :user
  
  validates_presence_of :title
  validates_presence_of :answer_one
  validates_presence_of :answer_two
  validates_length_of   :title,    :maximum=> 250, :allow_nil => true
  validates_length_of   :answer_one,    :maximum=> 100, :allow_nil => true
  validates_length_of   :answer_two,    :maximum=> 100, :allow_nil => true
  validates_length_of   :answer_three,    :maximum=> 100, :allow_nil => true
  validates_length_of   :answer_four,    :maximum=> 100, :allow_nil => true

  named_scope :approved, :conditions => {:approved => true}
  named_scope :not_approved, :conditions => {:approved => false}
end
