class Question < ActiveRecord::Base
  @@question = Question.new
  @@answer =  Answer.new
  #CSV Import Constantants for csv array
  TITLE =           0
  ANSWER_A =        1
  ANSWER_B =        2
  ANSWER_C =        3
  ANSWER_D =        4
  CORRECT_ANSWER =  5
  
  belongs_to :answer
  belongs_to :theme
  has_one :cicle
  has_and_belongs_to_many :used_in_games, :class_name => "Game"
  
  validates_presence_of :title, :answer, :theme, :correct
  validates_uniqueness_of :title
  
  def self.export_csv
    text=''
    Question.all.each do |q| 
      text << q.title.gsub("\"", "\'")
      text << ";"
      text << q.answer.a.gsub("\"", "\'")
      text << ";"
      text << q.answer.b.gsub("\"", "\'")
      text << ";"
      text << q.answer.c.gsub("\"", "\'") if q.answer.c
      text << ";"
      text << q.answer.d.gsub("\"", "\'") if q.answer.d
      text << ";"
      text << q.correct
      text << "\n"
    end
    return File.open("#{RAILS_ROOT}/question.csv", 'w') {|f| f.write(text) }
  end
  

  def self.import_from_text( content, theme_id )
    total_questions = 0
    saved_questions = 0

    encoding = CharDet.detect( content )["encoding"].downcase
    return false, "#{saved_questions} / #{total_questions}" if encoding != "utf-8" and (encoding =~ /ISO-8859-/i) != 0
    if encoding =~ /ISO-8859-2/i
      content = Iconv.iconv("UTF-8", "WINDOWS-1252", content)
      content = content.first
    elsif encoding =~ /ISO-8859-/i
      content = Iconv.iconv("UTF-8", "ISO-8859-1", content)
      content = content.first
    end
    
    unless @theme = Theme.find_by_id( theme_id )
      return false, "#{saved_questions} / #{total_questions}"
    end

    question = Question.new
    answer =  Answer.new

    begin
      content.each_line do |line|
        line.strip!
        # atenção: as expressões regulares usadas possuem caracteres que parecem espaços, mas não são! E são importantes para evitar erros no parser.
        if ( line.length > 0 )
          if index = line.gsub!( /^(\d|\.|"){1,7} *(-|–|;) */, '' )
            # leu pergunta
            question.title = line
            total_questions += 1
            # despreza possíveis respostas anteriores (provavelmente falha na pergunta anterior)
            answer = Answer.new
          elsif index = line.gsub( /^( |\*| )*(a|b|c|d) {0,3}(-|–|;|\)) *\)?(\t| | )*/i, '' )
            # leu opção de resposta
            index = line.index( /(a|b|c|d)/i )
            if ( index && question.title )
              option = line[ index..index ].downcase
              question.correct = option if ( line.index( '*' ) )
   
              line.gsub!( /^( |\*| )*(a|b|c|d) {0,3}(-|–|;|\))(\t| | )*/i, '' )
              case option
                when 'a'
                  answer.a = line
                when 'b'
                  answer.b = line
                when 'c'
                  answer.c = line
                when 'd'
                  answer.d = line
              end
            end
          end

          if answer and answer.a and answer.b and answer.c
            question.theme = @theme
            answer.save
            question.answer = answer if question and answer
            answer = Answer.new
            question.save if question
            saved_questions += 1
            question = Question.new
          end
        end 
      end

      return true, "#{saved_questions} / #{total_questions}"
    rescue Exception => e
      return false, "#{saved_questions} / #{total_questions}"
    end
  end


  def self.import_from_doc( filename = nil, theme_name = nil )
    unless ( filename && theme_name )
      puts( "Uso: import_from_doc FILENAME, THEME_NAME" )
    end

    unless @theme = Theme.find_by_name( theme_name )
      @theme = Theme.new(:name => theme_name )
      @theme.save
    end

    question = Question.new
    answer =  Answer.new

    file_readed = File.open("#{RAILS_ROOT}/#{filename}",'r').readlines
    file_readed.each do |line|
      puts line
      
      debugger
      if index = line.gsub!( /^\d{1,4} *(-|–|;) */, '' )
        # leu pergunta
        question.title = line
      elsif index = line.gsub( /^( |\*)*(a|b|c|d) *\) */i, '' )
        # leu opção de resposta
        option = line[ line.index( /(a|b|c|d)/i ) ]
        question.correct = option if ( line.index( '*' ) )

        line.gsub!( /^( |\*)*(a|b|c|d) *\) */i, '' )
        case option.downcase
          when 'a'
            answer.a = line
          when 'b'
            answer.a = line
          when 'c'
            answer.a = line
          when 'd'
            answer.a = line
        end
      end

      if answer and answer.a and answer.b and answer.c
        question.theme = @theme
        answer.save
        question.answer = answer if question and answer
        answer = Answer.new
        question.save if question
        question = Question.new
      end

#
##      if q.split(';')[0] =~ /[0-9]+/
#         debugger
#       if @@answer and @@answer.a and @@answer.b and @@answer.c
#          @@answer.save
#          @@question.answer = @@answer if @@question and @@answer
#          @@answer = Answer.new
#          @@question.save if @@question
#          @@question = Question.new
#        end
#        @@question.theme = @theme
#        @@question.title = q.split(';')[1].split("\n")[0]
#      elsif q.split(';')[0] =~ /^[\*]+/
#        @@question.correct = ( q.split(';')[0].split('*')[1].split("\n")[0] ).downcase if @@question
#      end
#      if q.split(';')[0] =~ /[A-D]/
#        if q.split(';')[0] =~ /[A]/
##          @@answer.a = q.split(';')[1].split("\n")[0]
#        elsif q.split(';')[0] =~ /[B]/
#          @@answer.b = q.split(';')[1].split("\n")[0]
#        else
#          @@answer.c = q.split(';')[1].split("\n")[0] 
#        end
#        
#      end
    end
  end
  
  def requested_times
    return self.used_in_game_ids.size
  end
  
  def self.import_from_csv(file_read, theme_id)
    encoding = CharDet.detect( file_read )["encoding"]
    return false if encoding != "utf-8" and (encoding =~ /ISO-8859-/) != 0
    if encoding =~ /ISO-8859-/
      text_read = Iconv.iconv("UTF-8", "ISO-8859-1", file_read)
      file_read = text_read.first
    end

    FasterCSV.parse(file_read, :quote_char => '"', :col_sep => ';', :row_sep => :auto) do |row| 
      if ((not row[0] =~ /^(tit[uloes]*\s*)$/i) and (not row[0] =~ /^\s*$/))
        question = Question.new
        answer = Answer.new
        theme = Theme.find(theme_id)
        answer.a = row[ANSWER_A]
        answer.b = row[ANSWER_B]
        answer.c = row[ANSWER_C]
        answer.d = row[ANSWER_D]
        answer.save
        question.title = row[TITLE]
        question.correct = row[CORRECT_ANSWER].downcase
        question.answer = answer
        question.theme = theme
        question.save
      end
    end
    return true
  end

  class << self
    def create_from_suggestion(suggestion)
      answer = Answer.create(:a => suggestion.answer_one, 
                             :b => suggestion.answer_two, 
                             :c => suggestion.answer_three, 
                             :d => suggestion.answer_four )
      
      question = Question.new( :title => suggestion.title, 
                       :theme => suggestion.theme, 
                       :correct => suggestion.correct, 
                       :answer => answer)
    question.save
    end
  end

end
