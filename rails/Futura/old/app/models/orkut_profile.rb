class OrkutProfile < ActiveRecord::Base
  PENDING = 1
  INVITED = 2
  CONFIRMED = 3
  belongs_to :user
  
  def self.add_friends!(user_id, params)
    params.each do |p|
      orkut_profiles = OrkutProfile.find_all_by_profile_id_and_user_id(p["id"], user_id)
      if orkut_profiles.empty?
        OrkutProfile.create(:user_id => user_id, :profile_id => p["id"], :thumb_url => p["thumbnailUrl"], 
        :name => "#{p['name']['givenName']} #{p['name']['familyName']}")
      else
        orkut_profiles.first.update_attributes(:thumb_url => p["thumbnailUrl"], 
        :name => "#{p['name']['givenName']} #{p['name']['familyName']}")
      end
    end
  end
end
