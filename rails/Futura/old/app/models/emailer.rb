class Emailer < ActionMailer::Base
  def invitation(user, recipients, message, token)
    debugger
    @subject      = "#{user.complete_name.strip} está te convidando para participar do Futura CDF"
    @recipients   = recipients
    @from         = "Futura CDF <no-reply@cdf.org.br>"
    headers         "Reply-to" => "no-reply@cdf.org.br"
    @content_type = "text/html"
    @message      = message
    @token        = token
    @sent_on      = Time.now
  end
#  handle_asynchronously :invitation
  
  def contact_us(user_information, message)
    @subject      = "Contato de #{user_information[:name]} #{user_information[:post_name]} através do Futura CD"
    @recipients   = "cdf@futura.org.br"
    @from         = "#{user_information[:name]} #{user_information[:post_name]} <#{user_information[:email]}>"
    headers         "Reply-to" => "#{user_information[:email]}" 
    @content_type = "text/plain"
    @message      = message
    @sent_on      = Time.now
  end
#  handle_asynchronously :contact_us

  def suggested_question(user, question)
    @subject      = "Sua pergunta foi aprovada!"
    @recipients   = user.email
    @from         = "Futura CDF <no-reply@cdf.org.br>"
    headers         "Reply-to" => "no-reply@cdf.org.br"
    @content_type = "text/html"
    @user         = user
    @question     = question 
    @sent_on      = Time.now
  end
#  handle_asynchronously :suggested_question
  
  
  def suggest_question_to_admin(question_id, host)
    @subject      = "Futura CDF: nova sugestão de pergunta"
    @recipients   = Admin.all.collect{|a| a.email}.join(",")
    @from         = "Futura CDF <no-reply@cdf.org.br>"
    headers         "Reply-to" => "no-reply@cdf.org.br"
    @content_type = "text/plain"
    @question_id  = question_id
    @host         = host
    @sent_on      = Time.now
  end
#  handle_asynchronously :suggest_question_to_admin
end
