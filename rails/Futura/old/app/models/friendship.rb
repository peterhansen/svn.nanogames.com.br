class Friendship < ActiveRecord::Base
  PENDING = 1
  ACCEPTED = 2

  belongs_to :user
  belongs_to :friend, :class_name => "User", :foreign_key => "friend_id"
  validates_uniqueness_of :friend_id, :scope => :user_id

  named_scope :pending_friendships_for, lambda { |user|
    { :conditions => "user_id = #{user.id} and friendship_status_id = #{PENDING}" }
  }

  def validate
    errors.add_to_base(I18n.t(:cannot_add_yourself)) if self.user_id == self.friend_id
  end
end
