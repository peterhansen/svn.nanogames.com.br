class User < ActiveRecord::Base
  include Comparable
  attr_accessor :result
  
  # campos usados na tabela de ranking (filtrados para melhorar o desempenho do find)
  FIELDS_USED_IN_RANKING = 'id, nickname, skill_month_variation, skill_week_variation, score, avatar_file_name, patent'

  # intervalo de atualização do contador de usuários online
  ONLINE_USERS_COUNT_INTERVAL = 10.seconds

  # intervalo de tempo sem ações para que um jogador seja marcado como offline
  SET_DEAD_INTERVAL = 70.seconds

  # intervalo de atualização do ranking
  RANKING_UPDATE_INTERVAL = 1.minute

  # intervalo de atualização da melhor pontuação
  HIGHEST_SCORE_UPDATE_INTERVAL = 5.minutes

  PATENT_LEVEL = ["Gênio", "Sábio", "Mestre", "Especialista", "Veterano", "Monitor", "Aspirante",
                  "Aprendiz", "Novato", "Calouro"]
  
  PATENT_LEVEL_RATE = 1.07
  PATENT_LEVELS = 40
  PATENT_LEVEL_MIN = 0
  
  devise :registerable, :database_authenticatable, :recoverable,
         :rememberable, :trackable, :validatable, :confirmable,
         :activatable, :lockable

  before_save :set_complete_name

  attr_accessible :email, :password, :password_confirmation, :avatar, :name, :post_name,
                  :marital_status_id, :city, :about, :state_id, :country_id, :gender_id,
                  :born_day, :born_month, :born_year, :nickname, :show_email, :show_age,
                  :agreed_terms, :receive_newsletter, :about, :patent

#  named_scope :ranking, lambda {{:order => 'score desc, created_at'}}
#  named_scope :weekly_ranking, lambda {{:order => 'skill_week_variation desc, created_at'}}
#  named_scope :monthly_ranking, lambda {{:order => 'skill_month_variation desc, created_at'}}
  
  belongs_to  :room 
  belongs_to  :my_game, :class_name => 'Game'
  belongs_to  :marital_status
  belongs_to  :state
  belongs_to  :country
  belongs_to  :gender
  has_many    :participations
  has_many    :games, :through => :participations
  has_many    :cicles
  has_many    :invitations
  has_many    :friendships
  has_many    :friends, :through => :friendships, :class_name => "User"
  has_many    :skill_variations
  belongs_to  :inviter, :class_name => "User"
  has_many    :inviteds, :class_name => "User", :foreign_key => 'inviter_id'
  has_and_belongs_to_many :histories
  has_and_belongs_to_many :notifications
  has_and_belongs_to_many :polls
  has_one     :google, :class_name => "GoogleToken", :dependent=>:destroy
  has_many    :orkut_friends, :class_name => "OrkutProfile"
  has_many    :suggested_questions
  
  acts_as_network :challenges, :join_table => :challenges

  has_attached_file :avatar, 
                    :styles => {:to_crop => '425x400!', 
                                :medium => "100x100!", 
                                :thumb => "66x66!", 
                                :small_thumb=>"26x26!"}, 
                    :default_url => '/avatars/:style/missing.gif'


  validates_attachment_size :avatar, :less_than => 5.megabytes  
  validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png', 'image/gif', 
  "image/pjpeg", "image/x-png"]  
  validates_presence_of :name, :post_name, :born_day, :born_month, :born_year, :city, :nickname, :agreed_terms
  validates_length_of   :name,        :maximum=> 20
  validates_length_of   :post_name,   :maximum=> 20
  validates_length_of   :nickname,    :maximum=> 20
  validates_uniqueness_of :nickname
  validates_length_of   :born_day,    :is=> 2,  :message=>"#{I18n.t(:size_must_have)} 2 #{I18n.t(:digits)}"
  validates_length_of   :born_month,  :is=> 2,  :message=>"#{I18n.t(:size_must_have)} 2 #{I18n.t(:digits)}"
  validates_length_of   :born_year,   :is=> 4,  :message=>"#{I18n.t(:size_must_have)} 4 #{I18n.t(:digits)}"
  validates_length_of   :city,        :maximum=> 50
  
  define_index do
    indexes :name, :post_name, :complete_name, :nickname, :sortable => true
    set_property :enable_star => 1
    set_property :min_prefix_len => 1

    has created_at, updated_at
  end

  def self.find_by_email_or_nickname(param)
    User.find( :first, :conditions => [ 'email = ? or nickname = ?', param, param ] )
  end

  def self.how_many_online
    online_users_count = CACHE.get( 'online_users_count' ) || 0
    last_online_users_time = CACHE.get( 'last_online_users_time' ) || 1.day.ago
    
    if ( online_users_count <= 0 || ( last_online_users_time < ONLINE_USERS_COUNT_INTERVAL.ago ) )
#      Delayed::Job.enqueue( BackgroundJob.new( 'User', 'refresh_online_users_list' ) )
      User.delay.refresh_online_users_list
      CACHE.set( 'last_online_users_time', Time.now )
    end
    online_users_count
  end

  
  ##
  #
  ##
  def self.find_all_by_online( online = true )
    online_users = CACHE.get( 'online_users' )
    if online_users
      online_users = online_users.keys
    else
      online_users = []
    end
    
    if online
      all = User.all( :order => "nickname", :conditions => [ "id IN (?)", online_users ] )
    else
      all = User.all( :order => "nickname", :conditions => [ "id NOT IN (?)", online_users ] )
    end
    
    # armazena o estado online/offline em cada instância, para evitar futuras buscas desnecessárias ao cache e/ou banco
    all.each do |user|
      user.online = online
    end
  end

  def online
    # se o estado online/offline não tiver sido armazenado antes, o obtém do cache
    if @online.nil?
      online_users_list = CACHE.get( 'online_users' )
      @online = online_users_list && online_users_list.has_key?( self.id )
    end
    @online
  end

  def online= ( on )
    @online = on
  end
  
  
  def confirm_invite_from_orkut!(ouid)
    if inviter_user_id = OrkutProfile.find_all_by_status_and_profile_id(OrkutProfile::INVITED, ouid).last
      inviter_user = User.find(inviter_user_id)
      self.inviter_id = inviter_user_id
      self.save
      # TODO adicionar pontos em outro lugar
      inviter_user.score += 100
      inviter_user.save
      inviter_user
    end
  end
  
  def online?
    self.online
  end
  
  def how_many_online_friends
    online_friends.size
  end
  
  def online_friends(param = true)
    self.friends.select {|friend| friend if friend.online == param }
  end  

  def suggest_question(params)
    suggested_question = SuggestedQuestion.new(params)
    if suggested_question.save
      self.suggested_questions << suggested_question
    end
    return suggested_question
  end
  
    
  def how_many_approved_questions
    self.suggested_questions.find_all_by_approved(true).size
  end  
    
  def colors
    ["verde", "amarelo", "azul", "laranja", "roxo", "vermelho"]
  end

  def find_quick_game(dif, room_id)
    game = Game.find( :first,
                      :conditions => {:quick_start => true, :status => Game::INITIALIZED, :room_id => room_id, :difficulty_id => dif.id},
                      :order => ["created_at DESC"])
    
    game if game and (game.users_in_lobby_or_playing.size < 6)
  end

  def replay_game(previous_game_id)
    unless game_to_replay = Game.find_by_previous_game_id(previous_game_id)
      previous_game = Game.find(previous_game_id)
      game_to_replay = self.create_game(previous_game.params_for_replay)
    end
    game_to_replay
  end

  def last_created_game
    game = self.games.last
    if game && game.status == Game::INITIALIZED
      game
    end
  end

  def create_notification(user)
    notification = Notification.new
    notification.body = 'body'
    notification.users << user
    notification.active = true
    notification.save
    notification
  end

  def create_notification_for_patent(initial_patent, new_patent)
    initial_patent.match(/(.*)_/)
    old_patent = $1
    
    if PATENT_LEVEL.index(new_patent) < PATENT_LEVEL.index(old_patent)
      message = "#{name} subiu de nível e já é #{new_patent} no CDF – Clube Desafio Futura!"
    else
      message = "#{name} caiu para o nível #{new_patent} no CDF – Clube Desafio Futura!"
    end

    full_message = "Você acabou de mudar para a patente: <strong>#{patent}</strong><br /><a href='#' onclick=\"shareOnOrkut('#{message}')\">Compartilhar no Orkut</a>"
    notifications.create( :body => full_message, :active => true )
  end

  def cleanup_challenges
    self.challenges_in = []
  end
  
  def color
    colors[self.active_participation.color - 1]
  end

  def color_for_game(game)
    if participation = game.participations.find_all_by_user_id(self.id).first
      colors[participation.color - 1] if participation.color
    end
  end


  def id_and_name
    { :futura_id => self.id, :display_name => self.nickname }
  end
   
  def age
    born_date = birthday_date
    today = Date.today
    user_age = today.year - born_date.year - ( born_date.change( :year => today.year ) > today ? 1 : 0 )
    user_age
  end

  def birthday_display
    I18n.l birthday_date, :format => :birthday
  end
  
  def is_my_friend?(friend_id)
    if self.friend_ids.include?(friend_id)
      my_friendship = Friendship.find(:first, :conditions => {:user_id => self.id, 
        :friend_id => friend_id, :friendship_status_id => 2 } )
      !my_friendship.nil?
    else
      false
    end
  end
  
  def set_complete_name
    self.complete_name = "#{self.name} #{self.post_name}"
  end

  def birthday_date
    if RAILS_ENV == 'development'
      "#{self.born_day}.#{self.born_month}.#{self.born_year}".to_date
    else
      "#{self.born_month}.#{self.born_day}.#{self.born_year}".to_date
    end
  end


  def cleanup_notifications!
    self.notifications = []
    self.save
  end
  
  def cleanup_challenges!
    self.challenges_in = []
    self.challenges_out = []
    self.save
  end
  
  def cleanup_notifications_and_challenges!
   # cleanup_notifications!
    cleanup_challenges!
  end
#  def expected_score
#    return (self.skill.to_f / self.current_game.total_skill.to_f)
#  end

  def <=>(player)
    self.score <=> player.score
  end

#  def new_skill=(value)
#    self.skill = value
#    self.save
#  end

  def add_skill_variation(amount)
    # TODO não usado no momento SkillVariation.create!(:amount => amount, :user => self)
  end

  def right_answer!
    participation = self.active_participation
    participation.hits += 1
    participation.save
  end

  def wrong_answer!
    participation = self.active_participation
    participation.misses += 1
    participation.save
  end
  
  def refresh_patent!( players_count, position = nil )
    patent_level, patent_sublevel = get_patent_level(players_count)
    patent_text = "#{patent_level}_#{patent_sublevel}"
    
    unless self.patent == patent_text
#      Delayed::Job.enqueue( BackgroundJob.new( self, 'set_patent', patent_text ) )
      self.delay.set_patent(patent_text)
#      Delayed::Job.enqueue( BackgroundJob.new( self, 'create_notification_for_patent', patent, patent_level ) )
      self.delay.create_notification_for_patent(patent, patent_level)
    end
  end

  def patent_to_s
    self.patent.removeaccents.downcase
  end

  def unanswered_poll
    if poll = Poll.active
      return poll unless self.responded_to_poll(poll)
    end
  end

  def responded_to_poll(poll)
    self.polls.include?(poll)
  end

  def exit_game
    if active_participation
      if active_participation.game and active_participation.game.started?
        end_cicle! if is_my_cicle?
        active_participation.update_attribute(:status, Participation::ABANDONED)
      else
        active_participation.update_attribute(:status, Participation::KICKED)
      end
    end
    update_attribute(:color, nil)
  end

  def exit_room
    # TODO necessário persistir em banco a sala do jogador?
    update_attributes(:room => nil, :room_user_id => nil)
  end

  def exit_site
    online_users_list = CACHE.get( 'online_users' )
    if ( online_users_list )
      online_users_list.delete( self.id )
      CACHE.set( 'online_users', online_users_list )
    end
    #self.online = false
    #self.save
  end

  ##
  #
  ##
  def current_month_score
    skill_month_variation
  end

  ##
  #
  ##  
  def last_week_score
    skill_week_variation
  end

  def self.ranking
    all_time_ranking
  end


  ##
  #
  ##
  def self.all_time_ranking
    ranking_all = CACHE.get( 'ranking_all' ) || nil
    last_all_ranking_time = CACHE.get( 'last_all_ranking_time' ) || 1.day.ago
    
    if ( ranking_all.nil? || ( Time.now - last_all_ranking_time >= RANKING_UPDATE_INTERVAL ) )
      CACHE.set( 'last_all_ranking_time', Time.now )
      if ( ranking_all.nil? )
        User.refresh_all_ranking
      else
        # pode esperar um pouco, por já ter versão antiga
#        Delayed::Job.enqueue( BackgroundJob.new( 'User', 'refresh_all_ranking' ) )
        User.delay.refresh_all_ranking
      end
    end
      
    return ranking_all
  end
  
  def self.reset_month_scores
#    User.transaction do
    all_p = Participation.all( :conditions => [ "created_at > ?", Date.today.beginning_of_month ], :select => 'sum(score) sum_score, user_id', :group => :user_id )
    non_zero_users = []

    all_p.each do |p|
      u = User.find_by_id( p.user_id )
      if u
        non_zero_users << u
        u.skill_month_variation = p.sum_score
        u.save
      end
    end

    # salva quem está com zero pontos no pe''ríodo
    other_users = User.all( :conditions => [ "id NOT IN (?)", non_zero_users ] )
    other_users.each do |user|
      user.skill_month_variation = 0
      user.save
    end
  end
  
  def self.reset_week_scores
#    User.transaction do
      all_participations = Participation.all( :conditions => [ "created_at > ?", Date.today.beginning_of_week ], :select => 'sum(score) sum_score, user_id', :group => :user_id )
      non_zero_users = []
      
      all_participations.each do |participation|
        user = User.find_by_id( participation.user_id )
        if user
          non_zero_users << user
          user.skill_week_variation = participation.sum_score
          user.save
        end
      end

      # salva quem está com zero pontos no período
      other_users = User.all( :conditions => [ "id NOT IN (?)", non_zero_users ] )
      other_users.each do |user|
        user.skill_week_variation = 0
        user.save
      end
#    end
  end  


  ##
  # Atualiza a pontuação total do jogador, levando em consideração a pontuação das participações,
  # enquetes respondidas e convites aceitos por outros jogadores.
  ##
  def refresh_score
    User.transaction do
      self.score = score_per_period + polls_score + invitations_score
      self.skill_month_variation = score_per_period( Date.today.beginning_of_month )
      self.skill_week_variation = score_per_period( Date.today.beginning_of_week )
      self.save
    end

    self.score
  end

  def polls_score
    self.polls.count( :select => 'id' ) * Poll::POINTS
  end

  def invitations_score
    return Invitation.count( :conditions => { :user_id => self, :status => Invitation::ACCEPTED }, :select => 'id' ) * Invitation::POINTS
  end

  def self.monthly_ranking
    ranking_month = CACHE.get( 'ranking_month' ) || nil
    last_month_ranking_time = CACHE.get( 'last_month_ranking_time' ) || 1.day.ago
    
    # se for começo de mês, reinicia a pontuação mensal
    last_month_ranking_reset_time = CACHE.get( 'last_month_ranking_reset_time' ) || 2.months.ago
    if ( last_month_ranking_reset_time < Date.today.beginning_of_month )
#      Delayed::Job.enqueue( BackgroundJob.new( 'User', 'reset_month_scores' ) )
      User.delay.reset_month_scores
      CACHE.set( 'last_month_ranking_reset_time', Time.now )
      
      ranking_month = nil
    end
    
    # se não houver ranking em cache, ou o cache tiver expirado, renova o cache
    if ( ranking_month.nil? || ( Time.now - last_month_ranking_time >= RANKING_UPDATE_INTERVAL ) )
      if ( ranking_month.nil? )
        User.refresh_month_ranking()
      else
        # pode esperar um pouco, por já ter versão antiga
#        Delayed::Job.enqueue( BackgroundJob.new( 'User', 'refresh_month_ranking' ) )
        User.delay.refresh_month_ranking
      end
      CACHE.set( 'last_month_ranking_time', Time.now )
    end
      
    return ranking_month
  end

  def self.weekly_ranking
    ranking_week = CACHE.get( 'ranking_week' ) || nil
    last_week_ranking_time = CACHE.get( 'last_week_ranking_time' ) || 1.day.ago
    
    # se for começo da semana, reinicia a pontuação semanal
    last_week_ranking_reset_time = CACHE.get( 'last_week_ranking_reset_time' ) || 1.month.ago
    if ( last_week_ranking_reset_time < Date.today.beginning_of_week )
#      Delayed::Job.enqueue( BackgroundJob.new( 'User', 'reset_week_scores' ) )
      User.delay.reset_week_scores
      CACHE.set( 'last_week_ranking_reset_time', Time.now )
      
      ranking_week = nil
    end
    
    if ( ranking_week.nil? || ( Time.now - last_week_ranking_time >= RANKING_UPDATE_INTERVAL ) )
      if ( ranking_week.nil? )
          User.refresh_week_ranking()
        else
          # pode esperar um pouco, por já ter versão antiga
#        Delayed::Job.enqueue( BackgroundJob.new( 'User', 'refresh_week_ranking' ) )
        User.delay.refresh_week_ranking
      end
      CACHE.set( 'last_week_ranking_time', Time.now )
    end
    
    ranking_week
  end

  ##
  # Obtém a pontuação acumulada do jogador num determinado período.
  # start_date data de início da pesquisa. Se for nulo, não filtra as entradas mais antigas.
  # end_date data de fim da pesquisa. Se for nulo, não filtra as entradas mais recentes.
  ##
  def score_per_period( start_date = nil, end_date = nil )
    if ( start_date )
      if ( end_date )
        all_p = Participation.all( :conditions => [ "user_id = ? AND created_at BETWEEN ?", self, start_date, end_date ], :select => 'sum(score) score' )
      else
        all_p = Participation.all( :conditions => [ "user_id = ? AND created_at > ?", self, start_date ], :select => 'sum(score) score' )
      end
    else
      if ( end_date )
        all_p = Participation.all( :conditions => [ "user_id = ? AND created_at < ?", self, end_date ], :select => 'sum(score) score' )
      else
        all_p = Participation.all( :conditions => [ "user_id = ?", self ], :select => 'sum(score) score' )
      end
    end

    if ( all_p )
      total = 0
      all_p.each { |p|
        total += p.score if ( p.score )
      }

      return total    
    else
      return 0
    end
  end

  def position_in_all_ranking
    ranking_all = User.all_time_ranking
    ranking_all.each_with_index do |user, index|
      if user.id == self.id
        return index + 1
      end
    end
    ranking_all.size
    #User.count(:conditions => "id != #{self.id} and (score > #{self.score} or (score = #{self.score} and created_at <= \"#{self.created_at}\"))", :select => 'id' ) + 1
  end
  
  def position_in_week_ranking
    ranking_week = User.weekly_ranking
    ranking_week.each_with_index { |u, i|
      if ( u.id == self.id )
        return i + 1
      end
    }
    return ranking_week.size    
  end
  
  def position_in_month_ranking
    ranking_month = User.monthly_ranking
    ranking_month.each_with_index { |u, i|
      if ( u.id == self.id )
        return i + 1
      end
    }
    return ranking_month.size    
  end
  
  def already_invited?(email)
    if User.find_by_email(email)
      not Invitation.find(:first, :conditions => "user_id = #{self.id} and friend_email = \"#{email}\" and created_at >= \"#{(Time.now - 1.days).to_date}\"").blank?
    else
      return false
    end
  end

  def send_invitation(email, message)
    token = self.id.to_s
    invitation = Invitation.new(:user_id => self.id, :friend_email => email.strip, :token => token , :status => 1)

    if invitation.save
#      Delayed::Job.enqueue(BackgroundJob.new( 'Emailer', 'deliver_invitation', self, email, message, token))
      Emailer.delay.deliver_invitation(self, email, message, token)
    end

    invitation
  end

  def active_notifications
    # TODO como fazer cache das notificações de cada jogador, e só expirá-las quando houver novidades?
    notifications = self.notifications.find( :all, :conditions => [ "active = true AND created_at >= ?", 1.hour.ago ]  )
    # desabilita desafios para jogos não mais válidos
    User.transaction do
      notifications.each { |n|
        sub = n.body[ /<a\s+href='\/games\/(\d+)/i ] || 
              n.body[ /<a\s+href='\/challenges\/accept_challenge\?room_id=(\d+)/i ]
              
        if ( sub && game_index = sub[ /\d+/ ] )
          g = Game.find_by_id( game_index )
          if ( g && g.ended? )
            n.active = false
            n.save
          end
        end
      }
    end
    notifications_to_all = Notification.find(:all, :conditions => "active = true AND to_all = true" )

    if poll = Poll.active
      if self.polls.include? poll
        notifications_to_all.delete_if {|n| n.id == poll.notification }
      end
    end
    notifies = notifications + notifications_to_all.to_a 
    return notifies.sort_by {|n| n.created_at }.reverse
  end  


  def config_orkut_id
    if self.google
      message = {"method" => 'people.get', "params" => {"userId" => ["@me"], "groupId" => "@self", 
                 "fields" => ["id"]}}.to_json
      ouid = orkut_connection("http://orkut.gmodules.com/social/rpc/people", message)["id"]
      
      User.transaction do
        users = User.find_all_by_orkut_id(ouid)
        users.each{|u| u.orkut_id = nil; u.google = nil; u.save}
        self.orkut_id = ouid
        self.save
      end
    else
      return nil
    end
  end
  
  def captcha_image
    orkut.captcha_image(orkut.captchas.last.captcha_url) unless orkut.captchas.empty?
  end
  
  def orkut_header
    return {'Content-Type' => 'application/json' }
  end
  
  def orkut_token
    return self.google.client
  end
  
  def import_orkut_friends
    if self.orkut_id
      message = {"method" => 'people.get', "params" => {"userId" => ["@me"], "groupId" => "@friends", "fields" => ["displayName", "emails",
                 "thumbnailUrl"]}}.to_json

      friends = orkut_connection( "http://orkut.gmodules.com/social/rpc/people", message)

      friends.each do |friend|
        if friend['emails'] && user = User.find_by_email(friend["emails"].first["value"])
          self.add_friend(user.id)
        else
          # TODO send_orkut_invitation(friend["emails"].first["value"]) unless friend["email"].blank?
        end
      end
    else
      return nil
    end
  end

  def profile_url_modal(friend = self)
    return "<a href='#' onclick=\"constructModal('profile_#{friend.id}', '/profiles/#{friend.id}');\">#{friend.nickname}</a>"
  end

  def add_friend(friend_id)
    friend = Friendship.new(:user_id => self.id, :friend_id => friend_id)
    friend.friendship_status_id = Friendship::ACCEPTED

    User.transaction do
      if friend.save
        friend = User.find(friend_id)
        friend_url = friend.profile_url_modal(friend)
        user_url = friend.profile_url_modal(self)
        notification = self.create_notification(friend)
        notification.body = "#{user_url} te adicionou como amigo."
        notification.save
        notification = self.create_notification(self)
        notification.body = "Você acabou de adicionar #{friend_url} como amigo."
        notification.save
        
        return true
      else
        return false
      end
    end
  end 

  def all_friends
    # TODO antes era find_all_by_friend_id.... parecia estar incorreto -> CONFERIR
    return friends

    if self.friends.blank?
      Friendship.find_all_by_user_id( self.id ).map { |f| f.user }
    else
      begin
        self.friends.concat( Friendship.find_all_by_user_id( self.id ).map { |f| f.user } )
      rescue
        return nil
      end
    end
  end

  def orkut_friends_to_invite
    self.orkut_friends.find_all_by_status(OrkutProfile::PENDING)
  end
  
  def invited_challenge_for?(user_id)
    challenge_invited = Challenge.find(:first, :conditions => {:user_id => user_id, :user_id_target => self.id, :status => Challenge::INVITED})
    return ( ( !challenge_invited.nil? ) and challenge_invited.challenged_at > DateTime.yesterday )
  end
  
  def set_invited_challenge(user_id)
    #game.free_colors.first
    #date_time = DateTime.now.to_s.gsub('T', ' ')
    Challenge.connection.execute("UPDATE challenges SET status = '#{Challenge::INVITED}' WHERE user_id = '#{user_id}' AND user_id_target = '#{self.id}'")
  end
  
  def time_in_game(game)
    cicles = self.cicles.find_all_by_game_id(game.id)
    cicles.inject(0) {|sum, t| sum + t.time }
  end

  def last_participation
    participations.find(:first, :order => "created_at DESC")
  end

  def right_answers
    self.active_participation.hits
  end

  def create_game( params )
    game = Game.new( params )
    game.turn = 0 # TODO porque isso não é o valor padrão?
    game.cicle = 0
    game.status = Game::INITIALIZED
    if game.save
      self.enter_lobby( game )
      return game
    else
      return nil
    end
  end

  def enter_lobby( game )
    return false if game.status != Game::INITIALIZED or game.active_participations.size >= game.max_users

    unless participation = game.participations.find_all_by_user_id(self.id).first
      participation = self.participations.build( :game_id => game.id) 
    end

    participation.status = Participation::IN_LOBBY
    self.room = game.room if self.room.nil?
    participation.save
    game.reload
    self.save
  end

  def enter_game( game )
    if participation = game.participations.find_all_by_user_id(self.id).first
      User.transaction do
        participation.update_attribute(:color, game.free_colors.first)
        participation.update_attribute(:status, Participation::ACTIVE)
        game.reload
        participation.update_attribute(:turn, game.users_playing_count - 1)
        participation.update_attribute(:enter_in_game_at, Time.now)
        update_attribute(:challenges_out, [])
        update_attribute(:questions_skipped, 0)
      end
    else
      return false
    end
  end

  def i_am_ready!
    if participation = self.active_participation
      participation.ready = true
      participation.save
    end
  end

  def new_cicle_for_game(game)
    self.end_cicle!
    self.cicles.build( :game_id => game.id )
    self.save
  end

  def participation_game(game)
    self.participations.find_all_by_game_id(game.id).last
  end
  
  def confirmed_in_lobby?(game)
    participation_game(game).status == Participation::ACTIVE
  end

  def active_participation
    self.participations.find(:first, :conditions => {:status => [Participation::ACTIVE, Participation::IN_LOBBY]}, :order => 'created_at DESC')
  end

  def current_cicle
    self.cicles.find(:first, :conditions => 'active = true', :order => 'created_at desc')
  end

  def last_cicle
    self.cicles.find(:first, :order => 'created_at desc')
  end

  def current_game
    active_participation.game if active_participation
  end
  
  def answer_is_correct(points, answer, time)
    if is_my_cicle?
      User.transaction do
        self.right_answer!
        update_attribute(:questions_skipped, 0)

        cicle = self.current_cicle
        cicle.correct = true
        cicle.choosed_answer = answer
        cicle.points = points
        cicle.time = time
        cicle.save
        
        add_skill_variation(points)
        
        participation = self.active_participation
        participation.score += points
        participation.save
        self.save
        self.end_cicle! 
      end
    end
  end

  def answer_is_wrong(points, answer, time)
    if is_my_cicle?
      User.transaction do
        self.wrong_answer!
        update_attribute(:questions_skipped, 0)
        cicle = self.current_cicle
        cicle.correct = false
        cicle.choosed_answer = answer
        cicle.points = points
        cicle.time = time
        add_skill_variation(points)
        
        cicle.save
        
        active_participation.update_attribute(:score, self.active_participation.score + points)
        self.end_cicle! 
      end
    end
  end

  ##
  #
  ##
  def did_not_answer( time_elapsed )
    User.transaction do
      self.wrong_answer!
      update_attribute(:questions_skipped, questions_skipped + 1)
      cicle = self.current_cicle
      cicle.correct = false
      cicle.time = time_elapsed
      cicle.save
    end
  end

  def leaved_by_timeout?
    questions_skipped >= Game::TURNS_BEFORE_TIMEOUT
  end

  def is_my_cicle?
    self.active_participation.is_my_cicle? if self.active_participation
  end

  def end_cicle!
    if cicle = self.current_cicle
      cicle.active = false
      cicle.save
    end
  end

  def creating_game?
    return self.games.last && self.games.last.status == Game::INITIALIZED
  end
    
  def game
    current_game
  end

  def orkut
    Orkut.find(self.google.orkut_id) if self.google
  end

  def orkut_connection(uri, message)
    response = orkut_token.post(uri, message, orkut_header)
    response_decoded_body = ActiveSupport::JSON.decode(response.body)
    response_decoded_body["data"]
  end

  def get_patent_level(players_count)
    if score <= 0
      return PATENT_LEVEL.last, 0
    end

    patent_ranges = User.get_ranges(User::PATENT_LEVELS, User::PATENT_LEVEL_MIN, players_count, User::PATENT_LEVEL_RATE)
    position = players_count - self.position_in_all_ranking
    index_patent = patent_ranges.index(patent_ranges.select { |p| p if p.include? position}.first)

    if index_patent
      level_name = PATENT_LEVEL[index_patent / 4]
      sub_level_name = 3 - (index_patent % 4)
    else
      level_name = PATENT_LEVEL.last
      sub_level_name = 0
    end

    return level_name, sub_level_name
  end

  def patent_icon_url
    patent, level = get_patent_level(User.count(:conditions => 'score >0'))
    "/images/ranking/ic_#{patent.removeaccents.downcase}_#{level}.png"
  end

  def patent_name
    self.patent.split("_").first
  end

  def self.refresh_all_ranking(refresh_patents = true)
    User.transaction do
      ranking_all = User.all( :select => FIELDS_USED_IN_RANKING, :order => 'score DESC' )
      CACHE.set( 'ranking_all', ranking_all )

      if refresh_patents
        players_count = User.count( :conditions => "score > 0" )
        users.each_with_index do |user,index|
          user.refresh_patent!( players_count, ( index + 1 ) )
        end
      end
    end
  end


  def self.refresh_month_ranking
    ranking_month = User.all( :select => FIELDS_USED_IN_RANKING, :order => 'skill_month_variation DESC' )
    CACHE.set( 'ranking_month', ranking_month )
  end  
  
  
  def self.refresh_week_ranking
    ranking_week = User.all( :select => FIELDS_USED_IN_RANKING, :order => 'skill_week_variation DESC' )
    CACHE.set( 'ranking_week', ranking_week )  
  end

  def self.refresh_online_users_list
    # varre todos os jogadores dados como online, e verifica se estão de fato online
    online_users_list = CACHE.get( 'online_users' ) || {}
    online_users_list.each_pair do |user_id, last_alive|
      if ( last_alive < SET_DEAD_INTERVAL.ago )
        online_users_list.delete( user_id )
      end
    end
    CACHE.set( 'online_users', online_users_list )
    
    online_users_count = online_users_list.size
    CACHE.set( 'online_users_count', online_users_count )
  end
  
  def set_patent( new_patent )
    #	reload é necessário para evitar	problemas em save() caso o objeto não tenha carregado todos os dados do banco
    self.reload
    self.update_attributes( :patent => new_patent )
  end

  ##
  # Obtém faixas de valores cujas proporções entre si sejam definidas por rate.
  ##
  def self.get_ranges( n_levels, min = 0.0, max = 1.0, rate = 1.61803399 )
    levels = []
    total = 0
    acc = max
    previous = max

    n_levels.times do |i|
      total += rate ** i
    end

    n_levels.times do |i|
      if ( i == n_levels - 1 )
        acc = min
      else
        acc -= ( rate ** i ) * ( max - min ) / total
      end
      levels[ i ] = acc ... previous
      previous = acc
    end
    
    levels
  end

  def self.refresh_patents
    User.all.each do |user|
      patent, sub_level = user.get_patent_level(User.count(:conditions => 'score >0'))
      user.set_patent(patent + "_" + sub_level.to_s)
    end
  end


  def send_victory_notification
    share_message = "#{nickname} mostrou que sabe tudo e venceu uma partida no CDF – Clube Desafio Futura!"
    notifications.create( :body => "Você acabou de ganhar uma partida!<br /><a href='#' onclick=\"shareOnOrkut(#{share_message})\"> Compartilhar no Orkut</a>" )
  end
end