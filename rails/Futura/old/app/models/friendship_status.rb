class FriendshipStatus < ActiveRecord::Base
  has_many :friendships
end
