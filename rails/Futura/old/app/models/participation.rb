class Participation < ActiveRecord::Base
  belongs_to :user
  belongs_to :game

  ACTIVE    = 0
  FINISHED  = 1
  ABANDONED = 2
  IN_LOBBY  = 3
  KICKED    = 4
  
  def is_my_cicle?
    if game and game.status == Game::STARTED
      user.id == game.current_user.id
    end
  end

  def active?
    # TODO participação KICKED conta como ativa?
    unless self.status == ABANDONED or self.status == IN_LOBBY
      true
    else
      false
    end
  end
end
