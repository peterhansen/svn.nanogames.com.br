class PollQuestion < ActiveRecord::Base
  belongs_to :poll
  has_many :poll_answers
  validates_presence_of :content
  accepts_nested_attributes_for :poll_answers

  def answered_option(answer_id)
    answer = PollAnswer.find(answer_id)
    answer.update_attribute(:count, answer.count + 1)
  end

  def participations
    self.poll_answers.inject(0) {|sum, answer| sum += answer.count }
  end
end
