class Notification < ActiveRecord::Base
  has_and_belongs_to_many :users
  validates_presence_of :body
  
  def disable!
    self.active = false
    save
  end
end
