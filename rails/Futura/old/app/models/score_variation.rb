class ScoreVariation < ActiveRecord::Base
  belongs_to :user

  named_scope :last_week_for_player, lambda { |user|
    { :conditions => "user_id = #{user.id} and created_at > \"#{(Time.now - 7.days).to_date}\"" }
  }

  named_scope :last_month_for_player, lambda { |user|
    { :conditions => "user_id = #{user.id} and created_at > \"#{(Time.now - 1.month).to_date}\"" }
  }
end
