module HomeHelper
  def themes
    Theme.root.collect {|t| [t.name, t.id]}
  end
end
