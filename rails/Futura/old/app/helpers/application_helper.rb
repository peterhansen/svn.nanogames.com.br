module ApplicationHelper
  def render_sidebar
    content_for :sidebar do
      render :partial => 'shared/column_left'
    end
  end
  
  def render_ranking
    weekly_ranking = User.weekly_ranking
    @ranking_players = weekly_ranking ? weekly_ranking[0..4] : []

    content_for :ranking_left_column do
      render :partial => 'shared/ranking_sidebar'
    end
  end
  
  def previous_label
    '<img src="/images/comum/seta_dupla_esq.gif" title="anterior">'
  end
  
  def next_label
    '<img src="/images/comum/seta_dupla_dir.gif" title="proximo">'
  end
  
  def render_user_status( user, challenges )
    user_challenged = false
    challenges.each { |c| if ( c.user_id == user.id ) then user_challenged = true; break; end; }
    #user_challenged = challenges.index{ |c| c.user_id == user.id } != nil
  
    if user.online? and not user.id == current_user.id
      text = '<td class="status"><span class="status_online">Online</span></td>'

      if !challenges.blank? and not user_challenged
        text += '<td class="situacao"><a href="#" class="bt_desfazer texthide toggle_button">Desfazer</a></td>'
      elsif user_challenged
        text += '<td class="situacao"><span class="label_enviado">Enviado</span></td>'
      elsif user.game
        text += '<td class="situacao"><span class="label_jogando">Jogando</span></td>'
      else
        text += '<td class="situacao"><a href="#" class="bt_desafiar texthide toggle_button">Desafiar</a></td>'
      end

      text
    elsif user.online? and user.id == current_user.id
      '<td class="status"><span class="status_online">Online</span></td>
      <td class="situacao"></td>'
    else 
      '<td class="status"><span class="status_offline">Offline</span></td>
      <td class="situacao"></td>'
    end
  end
  
  def render_position_td( position )
    case position
      when 1
        return '<td class="posicao pos_ouro">'
      when 2
        return '<td class="posicao pos_prata">'
      when 3
        return '<td class="posicao pos_bronze">'
      else
        '<td class="posicao">'
    end
  end
  
  def set_position( index )
    (index + 1) + ((@users.current_page - 1) * 10)
  end
  
  def selected_user( user, challenges )
    "selecionado" if challenges.include?(user)
  end
 
  def menu_class(action)
    (self.action_name == action.to_s) ? "aba_ativa" : ""
  end
  
  def juggernaut_client_id
     current_user.current_session_id if current_user
  end
  
  def how_many_online_users
    pluralize_online( User.how_many_online, "jogador", "jogadores")
  end
  
  def how_many_online_friends
    pluralize_online(current_user.how_many_online_friends, "amigo", "amigos")
  end
  
  def pluralize_online(online_size, type, plural_type)  
    if online_size == 0
      "Nenhum #{type} online"
    elsif online_size == 1
      "1 #{type} online"
    else
      "#{online_size} #{plural_type} online"
    end
  end
  
  def home_futura_url
    "/"
  end
  
  def section_url
    "<a href='#{home_futura_url}'>#{section_name}</a>"
  end
  
  def section_name
    "Página Inicial"
  end
  
  def camelized_section_name(controller, action = nil)
    if controller == 'polls' and action == 'show'
      translated = "enquete".camelize  
    else
      translated = t("#{controller}.#{action || 'index'}").camelize
    end
    if translated.class != String
      translated = "#{action || 'index'}".camelize_with_space
    end
    translated
  end

  def section_name_with_link(controller, action = nil)
    if action.to_i != 0
      action_name = 'show'
    else
      action_name = action
    end
    "<a href='/#{controller}/#{action}'>" + camelized_section_name(controller, action_name) + "</a>"
  end
  
  def bread_crumb
    if ( ( (params[:controller] == 'pages') and (params[:action] == 'index_orkut') ) or ( (params[:controller] == 'home') and (params[:action] == 'index') ) )
      section_name
    elsif params[:controller] == 'rankings' and params[:action] != 'all_time'
      "#{section_url} > #{section_name_with_link('rankings', 'all_time')} > #{camelized_section_name(params[:controller], params[:action])}"
    elsif params[:controller] == 'rooms' and params[:action] == 'show'
      "#{section_url} > #{section_name_with_link('rooms')} > #{camelized_section_name(params[:controller], params[:action])}"
    elsif params[:controller] == 'games'
      "#{section_url} > #{section_name_with_link('rooms')} > #{section_name_with_link('rooms', @game.room_id.to_s)} > #{camelized_section_name(params[:controller], params[:action])}"
    else
      "#{section_url} > #{camelized_section_name(params[:controller], params[:action])}"
    end
  end
  
end
