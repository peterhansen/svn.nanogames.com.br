class ApplicationController < ActionController::Base
  helper :all
  protect_from_forgery
  before_filter :set_header_for_ie
  before_filter :mailer_set_url_options
  
  def mailer_set_url_options
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end
  
  def login_required
    session[:registering] || authenticate_user!  
  end
  
  
  def set_user_online_on_page
    if current_user
      set_user_online 
      exit_room_and_game 
    end
  end
  
  def im_alive( r = true )
    if user = current_user
      online_users = CACHE.get( 'online_users' ) || {}
      online_users[ user.id ] = Time.now
      CACHE.set( 'online_users', online_users )
    end
    
    if ( r )
      render :nothing => true
    end
  end
  
  def orkut_sign_in
    u = User.find_all_by_orkut_id(params[:ouid]).last
    session[:ouid] = params[:ouid]
    if User.how_many_online >= StaticDataGame.first.max_online_users
      redirect_to '/orkut_index'
    elsif u.nil?
      redirect_to '/orkut_index'
    elsif u.confirmed_at
      session[:ouid] = params[:ouid]
      session[:callback_orkut] = "http://www.orkut.com.br/Main#Application?appId=#{params[:appid]}"
      warden.logout(:user)
      warden.default_strategies :orkut_sign
      warden.authenticate!
      warden.set_user(u, :scope => :user)
      u.current_session_id = session[:session_id]
      u.save
    else
      session[:confirmed_error_message] = "Você está tentando entrar com um endereço de e-mail que ainda não foi confirmado. Por favor verifique sua conta de e-mail e confirme sua conta, ou insira seu e-mail abaixo novamente para reenviar a mensagem."
      redirect_to :controller => :home
    end
  end
  
  def set_user_online
    authenticate_user! if not current_user
    if current_user and current_user.current_session_id != session[:session_id]
      if params[:controller] == 'games'
        session[:callback_page_sign_in] = '/rooms'  
      else
        session[:callback_page_sign_in] = url_for || "/"
      end
      warden.logout(:user)
      session[:login_error_message] = "Seu usuário já está logado de outro lugar."
      redirect_to :controller => 'home'
    end
  end
  
  def decide_strategy
    unless current_user
      if params[:ouid] 
        orkut_sign_in
      else
        session[:ouid] = nil
        set_user_online      
      end
    end
  end
  
  def my_user
    if current_user
      user = current_user
    else
      if session[:current_user_id]
        user = User.find(session[:current_user_id])
      else
        user = User.find(:first, :conditions => {:current_session_id => session[:session_id]})
      end
    end
  end

  def exit_game
    if user = my_user
      if game = user.current_game || user.games.last
        if game.reload.status == Game::STARTED
          if participation = user.active_participation
            game.next_cicle!(game.next_user) if participation.is_my_cicle?
            game.end! if game.reload.active_participations.size <= 1
          end

          user.exit_game
        elsif game.status == Game::INITIALIZED || game.status == Game::ABORTED
          user.exit_game

          if game.reload.users_in_lobby_or_playing.size < 1 && !params[ 'creating_game' ]
            game.end!
          end
        end
      end
    end
  end
  
  
  def exit_room
    if user = my_user
      room = user.room
      if room
        user.exit_room
      end
    end
  end

  def exit_room_and_game
    exit_game
    exit_room if params[:creating_game].nil?
  end
  
  def exit_site
    user = my_user
    user.exit_site if user
  end
  


  protected
 

  def set_header_for_ie
    response.headers['P3P'] = 'CP="CAO PSA OUR"'
    im_alive( false )
  end


  # Overrides the rescue_action method in ActionController::Base, but does not inhibit
  # any custom processing that is defined with Rails 2's exception helpers.
  def rescue_action_in_public( exception )
    case exception
    when ActiveRecord::RecordNotFound, ActionController::UnknownAction, ActionController::RoutingError
         redirect_to '/404.html'
      else
      HoptoadNotifier.delay.notify(exception)
#      Delayed::Job.enqueue(BackgroundJob.new('HoptoadNotifier', 'notify', exception))
      redirect_to '/500.html'
    end  
  end
  
end
