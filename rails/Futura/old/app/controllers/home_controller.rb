class HomeController < ApplicationController

  before_filter :check_user_online, :only => [ :news ]
  

  def index
    if current_user and current_user.current_session_id != session[:session_id] and not session[:ouid]
      set_user_online
    elsif current_user
      @current_session_id = current_user.current_session_id
      session[:session_id] = @current_session_id
      exit_room_and_game
    elsif session[:ouid]
      redirect_to "/orkut_index"
    end
  end
  
  def news
    @news = New.find(:all, :conditions => {:active => true}, :order => "created_at DESC").paginate(:page => params[:page], :per_page => 4)
  end
  
  def error_juggernaut
    render :nothing => true
  end

  def polling
    @time = Time.now
  end

  def getTime
    render :text => Time.now
  end
  
  def suggest_question
    @suggested_question = SuggestedQuestion.new
  end
  
  def suggest_question_answer
  end
  
  def submit_question
    @suggested_question = current_user.suggest_question(params[:suggest_question])
    if @suggested_question.errors.empty?
      flash[:message] = "Pergunta submetida com sucesso!"
#      Delayed::Job.enqueue(BackgroundJob.new('Emailer', 'deliver_suggest_question_to_admin', @suggested_question.id, request.host_with_port))
      Emailer.delay.deliver_suggest_question_to_admin(@suggested_question.id, request.host_with_port)
      redirect_to :action => 'suggest_question_answer'
    else
      flash[:message] = "Ocorreram um ou mais erros na submissão de pergunta."
      respond_to do |format| 
        format.xml { render :xml => @suggested_questions.errors }
        format.html { render :action => 'suggest_question' }  
      end
    end    
  end
  
  private
  
  def check_user_online
    set_user_online
    exit_room_and_game  
  end
  
end
