class PollsController < ApplicationController  
  before_filter :set_user_online
  before_filter :exit_room_and_game
  
  def show
    @poll = Poll.active
    if ( current_user.polls && current_user.polls.include?( @poll ) )
      redirect_to :controller => :rooms
    end
  end

  def respond
    if ( @poll = Poll.find(params[:id]) ) && !current_user.polls.include?( @poll )
      if params[:answers].blank? 
        flash[:notice] = "Resposta em branco"
        render :show
      else
        # os pontos do jogador são atualizados dentro de answered_by
        @poll.answered_by( current_user )

        params[:answers].each_pair do |key, value|
          PollQuestion.find(key).answered_option(value)
        end
        render :success, :id => @poll.id
      end
    end
  end

  def success
    unless @poll = Poll.find(param[:id])
      redirect_to profiles_path
    end
  end
end
