require 'oauth/controllers/consumer_controller'
class OauthConsumersController < ApplicationController
  layout 'popup'

  before_filter :check_registering

  include Oauth::Controllers::ConsumerController

  def index
    @consumer_tokens = ConsumerToken.all :conditions => { :user_id => current_user.id }
    @services = OAUTH_CREDENTIALS.keys - @consumer_tokens.collect { |c| c.class.service_name }
  end

  def show
    unless @token
      opts = "&hl=pt-BR"
      @request_token=@consumer.get_request_token(callback_oauth_consumer_url(params[:id]))
      session[@request_token.token]=@request_token.secret

      if @request_token.callback_confirmed?
        render :text => "<script>top.window.location='#{ @request_token.authorize_url }#{ opts }'</script>" 
      else
        render :text => "<script>top.window.location='#{ @request_token.authorize_url }&oauth_callback=#{ callback_oauth_consumer_url( params[ :id ] ) }#{ opts }'</script>" 
      end
    end
  end

  def callback
    @request_token_secret=session[params[:oauth_token]]

    if @request_token_secret
      orkut = Orkut.create()
      if session[ :registering ] 
        user = User.find(session[:user_id])
        @token = @consumer.create_from_request_token( user, params[:oauth_token],
                                       @request_token_secret, params[ :oauth_verifier ] )
      
      else
        begin
          @token = @consumer.create_from_request_token( current_user, params[:oauth_token],
                                         @request_token_secret, params[ :oauth_verifier ] )      
        rescue Exception => e
          flash[:error] = "Erro ao autenticar no Orkut (usuário possivelmente não autorizou o acesso aos dados do Orkut)."
        end
      end

      if @token
        @token.orkut_id = orkut.id
        @token.save
        flash[:notice] = "#{params[:id].humanize} was successfully connected to your account"
        go_back
      else
        flash[:error] = "An error happened, please try connecting again"
        redirect_to oauth_consumer_url(params[:id])
      end
    end
  end

  def destroy
    throw RecordNotFound unless @token
    @token.destroy

    if params[:commit]=="Reconnect"
      redirect_to oauth_consumer_url(params[:id])
    else
      flash[:notice] = "#{params[:id].humanize} was successfully disconnected from your account"
      go_back
    end
  end


  protected

  def go_back
    redirect_to :controller => 'social_medias', :action => 'index'
  end

  def load_consumer
    consumer_key=params[:id].to_sym
    if OAUTH_CREDENTIALS.include?(consumer_key)
      @consumer="#{consumer_key.to_s.camelcase}Token".constantize
      @token=@consumer.find_by_user_id(current_user.id) if current_user
    else
      @consumer = nil
      @token = nil
    end
#    throw RecordNotFound unless OAUTH_CREDENTIALS.include?(consumer_key)
 #   @consumer="#{consumer_key.to_s.camelcase}Token".constantize
  #  @token=@consumer.find_by_user_id(current_user.id) if current_user
  end

private
  def check_registering
    if session[ :registering ]
      current_user = User.find( session[ :user_id ] )
    end
  end
end
