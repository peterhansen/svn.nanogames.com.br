class RankingsController < ApplicationController
  before_filter :set_user_online
  before_filter :exit_room_and_game
  
  before_filter :set_challenges
  
  def index
    redirect_to :action => :all_time
  end
  
  def all_time
    session[:from_ranking] = true
    @ranking_type = 'a'
    @users = User.ranking.paginate({:page => params[:page], :per_page => 10})
    @player_position = current_user.position_in_all_ranking
  end
  
  def monthly
    session[:from_ranking] = true
    @ranking_type = 'm'
    @users = User.monthly_ranking.paginate({:page => params[:page], :per_page => 10})
    @player_position = current_user.position_in_month_ranking
  end
  
  def weekly
    session[:from_ranking] = true
    @ranking_type = 'w'
    @users = User.weekly_ranking.paginate({:page => params[:page], :per_page => 10})
    @player_position = current_user.position_in_week_ranking
  end
  
  def friends
    session[:from_ranking] = true
    @ranking_type = 'a'
    @users = ( current_user.all_friends + [ current_user ] ).sort { |a,b| b <=> a }
    @users.each_with_index { |u, i|
      u.skill_month_variation = u.score
      if ( u == current_user )
        @player_position = ( i + 1 )
      end
    }
    @player_position ||= @users.length + 1
    @users = @users.paginate({:page => params[:page], :per_page => 10})
  end
  
  
  private
  
  def set_challenges
    @challenges_made = Challenge.all( :conditions => [ "user_id_target = ? AND status = ?", current_user, Challenge::INVITED ] )
    #@challenged_by = Challenge.all( :conditions => [ "user_id = ? AND status = ?", current_user, Challenge::INVITED ] )
  end
  
end
