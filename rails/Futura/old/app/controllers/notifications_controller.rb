class NotificationsController < ApplicationController
  def set_disable
    notification = Notification.find( params[:notification_id] )
    notification.active = false
    notification.save
    render :nothing => true
  end
  
  def get_notifications
    when_fragment_expired "notifications_#{current_user.id}", 9.seconds.from_now do
      render :partial => 'notification_box'
    end
  end
end
