class RoomsController < ApplicationController
  before_filter :decide_strategy, :except => [ :get_room_partial, :get_game_info, :get_player_list ]
  before_filter :exit_game, :only => :show
  before_filter :exit_room_and_game, :only => :index
  
  def index
    when_fragment_expired "conteudo_principal conteudo_salas", 10.seconds.from_now.time do
      @rooms = Room.paginate( :per_page => 10, :page => params[:page], :order => 'name' )
    end
  end
  
  def show
    when_fragment_expired "room_#{params[ :id ]}", 5.seconds.from_now do
      @url_modal = "/rooms"
      @type = "sala"

      if params[:difficulty]
        @room = Room.find( params[:id],
                           :conditions => "difficulty_id = #{Difficulty.find_by_name(params[:difficulty])}" )
      else
        @room = Room.find( params[:id] )
      end

      @games = Game.public.not_ended.find_all_by_room_id(@room.id, 
                                                        :order => "created_at DESC" ).paginate(:per_page => 10, :page => params[:page])                                                      
    end
    
    # TODO workaround para bug de cache no template (por algum motivo @room ficava nulo na view)
    @room ||= Room.find( params[:id] )
    @games ||= Game.public.not_ended.find_all_by_room_id(@room.id, 
                                                        :order => "created_at DESC" ).paginate(:per_page => 10, :page => params[:page])                                                      
  end
  
  def set_user
    if current_user.room.nil?
      @room = Room.find(params[:id])
      current_user.room = @room
      current_user.room_user_id = session[:session_id]
      current_user.save
      #has_new_player_in_room(@room.id)
    end
   
    render :nothing => true
  end
  
  def exit
    exit_room
    render :nothing => true  
  end

  def get_room_partial
    room = Room.find(params[:id])
    render :partial => 'users_count', :locals => { :room => room }
  end

  def get_game_info
    game = Game.find(params[:id])
    render :partial => 'tool_tip', :locals => { :game => game, :index => params[:index].to_i || 0 }
  end

  def get_player_list
    game = Game.find(params[:id])
    users = game.users_in_lobby_or_playing.map { |u| u.nickname }
    render :json => users
  end
  
  private
  def has_new_player_in_room(id)
    room = Room.find(id)

    render :juggernaut => {:type => :send_to_channel, :channel => "rooms_index"} do |page|
      page.replace_html "users_in_#{room.id}".to_sym, "#{room.users.count.to_s}"
      if room.full?
        page.replace_html "room_button_#{room.id}".to_sym,  
        "<span class='bt_lotado texthide'><a href='#'><span>lotado</span></a></span>"
      end
    end
  end
  
end
