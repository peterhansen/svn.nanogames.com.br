class ChatController < ApplicationController
  layout nil

  def message
    user = current_user || User.find_by_id(request.headers['rack.session'][:current_user_id])

    if user
      nickname = user.nickname
      participation = user.active_participation

      if participation and participation.color
        color_index = participation.color - 1
      else
        color_index = 1
      end

      color = user.colors[ color_index ]
      message_body = params[:message_body]

      unless message_body.blank?
        game = user.current_game || user.games.last
        render :juggernaut => {:type => :send_to_channel, :channel => "game_#{ game.id }"} do |page|
          page.call "showChatMessage", message_body, nickname, color
        end
      end
    end

    render :nothing => true
  end
end