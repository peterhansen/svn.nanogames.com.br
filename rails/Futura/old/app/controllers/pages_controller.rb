class PagesController < ApplicationController
  before_filter :set_user_online_on_page
  
  caches_page [ :ajuda, :canal_futura, :como_jogar, :contato, :email, :email_sugestao, :index_orkut, :invitation, :o_jogo, :sucesso_esqueci_senha, :termos_de_uso ]

  def email_sugestao
	@resource = current_user
  end
  def email
	@resource = current_user
  end

  def index_orkut
    render :layout => false
  end
end
