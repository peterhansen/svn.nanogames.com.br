class InvitationsController < ApplicationController
  before_filter :set_user_online
  before_filter :exit_room_and_game

  FRIENDS_PER_PAGE = 20

  def index
    redirect_to :action => :new
  end

  def new; end

  def create
    if params[:friends].blank?
      flash[:notice] = "Por favor, forneça pelo menos um email."

      render :action => :new
    else
      params[:friends].split(",").each do |friend|
        unless current_user.already_invited?(friend)
          current_user.send_invitation(friend, params[:message])
        end
      end
      render :template => 'invitations/result'
    end
  end

  def orkut
    if current_user.orkut_id and current_user.orkut
      unless params[:page]
        current_user.orkut.friends #if current_user.orkut # ter orkut_id não é garantia de ter orkut! precisa de google
        CACHE.set "#{current_user.id}_orkut_friends",
                  Marshal.dump(current_user.orkut_friends_to_invite.sort_by{|orkut_friend| orkut_friend.name.removeaccents.downcase}),
                  30.minutes
      end
      @friends = Marshal.load(CACHE.get( "#{current_user.id}_orkut_friends" ) ).paginate(:page => params[:page], :per_page => FRIENDS_PER_PAGE)

    else
      redirect_to :action => "register_orkut"
    end
  end


  def search_ajax
    if params[:query].blank?
      @friends = Marshal.load(CACHE.get( "#{current_user.id}_orkut_friends" ) ).paginate(:per_page => FRIENDS_PER_PAGE, :page => params[:page])
    else
      @friends = Marshal.load(CACHE.get( "#{current_user.id}_orkut_friends" ) ).select { |friend| friend.name =~ /#{params[:query]}/i }.paginate(:per_page => FRIENDS_PER_PAGE, :page => params[:page])
    end
    render :partial => 'search_result', :locals => {:friends => @friends}
  end

  def register_orkut

  end

  def orkut_invitation
   # current_user.orkut.clear_captcha!
    if params[:select_all]
      orkut_profiles = current_user.orkut_friends.find_all_by_selected_and_status(false, OrkutProfile::PENDING)
      orkut_profiles.each do |op|
        op.selected = true
        op.save
      end
      redirect_to :action => 'orkut'
    elsif params[:deselect_all]
      orkut_profiles = current_user.orkut_friends.find_all_by_selected_and_status(true, OrkutProfile::PENDING)
      orkut_profiles.each do |op|
        op.selected = false
        op.save
      end
      redirect_to :action => 'orkut'
    else
      orkut_profile = OrkutProfile.find(params[:profile_id])
      if orkut_profile.selected?
        orkut_profile.selected = false
      else
        orkut_profile.selected = true
      end
      orkut_profile.save
      render :text => 'true'
    end
  end

  def show_captcha
    current_user.orkut.send_scrap( nil, message_body ) if current_user.orkut.captchas.empty?
    @message_body = message_body
  end

  def message_body
    "Olá!\n#{current_user.complete_name} está se divertindo no game CDF - Clube Desafio Futura e te convidou para jogar. Você aceita o desafio?\n
    #{app_url}"
    #"teste http://www.google.com.br"
  end

  def app_url
    app_data_id = 41868001178
    "http://www.orkut.com.br/Main#Application?appId=#{app_data_id}"
  end

  def send_orkut_invitation
    if current_user.orkut.send_scrap(params[:captcha_answer], message_body)
      orkut_friends = current_user.orkut_friends.find_all_by_selected_and_status(true, OrkutProfile::PENDING)
        orkut_friends.each do |o_f|
        o_f.status = OrkutProfile::INVITED
        o_f.save
      end
      flash[:notice] = "Convites enviados"
      redirect_to :action => :orkut
    else
      redirect_to :action => :show_captcha
    end
  end

  def captcha
    send_data current_user.captcha_image, :type => "content-type: image/jpeg", :disposition => "inline"
  end


end

