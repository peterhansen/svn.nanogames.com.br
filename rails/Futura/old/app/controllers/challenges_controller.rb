class ChallengesController < ApplicationController
  def invite_challenge
    user = User.find(params[:user_id])
    game = current_user.my_game
    if game and game.max_users > 2
      max_users = game.max_users - 1
    elsif game and game.max_users == 2
      max_users = 5
    elsif game and not game.private?
      max_users = game.max_users - 1
    else
      max_users = 5
    end
    
    if params[:ctrl] == "ranking"
      max_users = User.count
    end 
    
    value = false
    current_user.challenges_in << user
    value = true

      
    render :json => {:value => value, :max_users => max_users, 
      :challenges => current_user.challenges_in.size} 
  end
  
  def remove_challenge
    user = User.find(params[:user_id])
    current_user.challenges_in.delete(user)
    render :json => {:value => true, :challenges => current_user.challenges_in.size } 
  end
  
  def index
    @users = current_user.challenges_in
    @rooms = Room.all( :order => 'name' )
    @themes = @rooms.first.themes.uniq.collect {|theme| theme.name}
    @difficulties = Difficulty.all
    render :layout => false
  end
  
  def index_ranking
    @users = current_user.challenges_in
    @rooms = Room.all( :order => 'name' )
    @themes = @rooms.first.themes.collect {|theme| theme.name}
    @difficulties = Difficulty.all
    render :layout => false
  end
  
  def send_invites_ranking
    users = current_user.challenges_in
    users.each do |user|
      current_user.set_invited_challenge(user.id)
       notification = current_user.create_notification(user)
       url_game = "/challenges/accept_challenge?room_id=#{params[:room_id]}&difficulty_id=#{params[:difficulty_id]}&user_from_id=#{current_user.id}&notification_id=#{notification.id}"
       room = Room.find( params[:room_id] )
       speed = params[:difficulty_id] == 1 ? "normal" : "turbo"
       notification.body = "#{current_user.profile_url_modal} está te desafiando! Sala #{room.name} - Velocidade #{speed}.\n<a href='#{url_game}'>Aceito!</a>"
       notification.save
    end
    redirect_to :controller => 'rankings', :action => 'all_time'
  end
  
  def send_invites
    users = current_user.challenges_in
    game_params = {:difficulty_id=>params[:difficulty_id], :room_id=>params[:room_id], :private=>true}
    unless game = current_user.last_created_game
      game = current_user.create_game(game_params)
    end
    game.max_users = users.size + 1
    game.save
    users.each do |user|
      notification = current_user.create_notification(user)
      notification.body = "\
              #{current_user.profile_url_modal} está te desafiando! Sala #{game.room.name} - Velocidade #{game.difficulty_id == 1 ? "normal" : "turbo"}.\n
              <a href='/games/#{game.id}?notification_id=#{notification.id}'>Aceito!</a>\
             "
      notification.save
    end
    current_user.cleanup_challenges
    redirect_to :controller => "games", :action =>"show", :id => game.id
    
  end
  
  def get_themes
    room = Room.find(params[:room_id])
    render :json => room.themes.uniq.collect { |theme| theme.name }
  end
  
  def accept_challenge
    if params[:notification_id]
      notification_challenge = Notification.find(params[:notification_id])
      notification_challenge.disable! 
    end
    game_params = {:difficulty_id=>params[:difficulty_id], :room_id=>params[:room_id], :private=>true}
    game = current_user.create_game(game_params)
    game.max_users = 2
    game.save

    user = User.find(params[:user_from_id])
    user.challenges_in.delete(current_user)
    notification = user.create_notification([user])
    notification.body = "
      #{current_user.nickname} aceitou seu desafio. Para jogar \
      <a href='/games/#{game.id}?notification_id=#{notification.id}'>clique aqui</a>
    "
    notification.save
    redirect_to :controller => "games", :action =>"show", :id => game.id
  end

end
