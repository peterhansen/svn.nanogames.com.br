class PlayersController < ApplicationController
  before_filter :set_user_online
  before_filter :exit_room_and_game, :except => [:search_ajax, :query_search]

  FRIENDS_PER_PAGE = 20

  def index
    session[:from_ranking] = nil
    redirect_to :action => 'friends'
  end

  def friends
    set_challenges()
    session[:from_ranking] = nil
    # podia user um scope e chamar: friends = current_user.active_friends
    @users = PlayersController::sort_and_paginate_users( current_user.online_friends,params[:show_selecteds], current_user, params[ :page ] )
    @active_tab_friends = "aba_ativa"
    @from = "friends"
  end

  def all
    set_challenges()
    session[:from_ranking] = nil
    @active_tab_players = "aba_ativa"
    @users = PlayersController::sort_and_paginate_users( User.find_all_by_online - [ current_user ],params[:show_selecteds], current_user, params[ :page ] )
    @from = "all"
  end

  def search_ajax
    users = query_search( params[:q], params[:from] )

    users_list = users[0..5].collect do | user | 
      unless user.id == current_user.id
        if user.complete_name =~ (/#{params[:q]}/i)
          searched_field = user.complete_name
        else
          searched_field = user.nickname
        end

        bold_name = searched_field.gsub(/(#{params[:q]})/i, '<b>\1</b>')
        "<li><a href='/players/search?q=#{searched_field}&from=#{params[:from]}'>#{bold_name}</a></li>"  
      end
    end

    render :text => "<ul>"  + users_list.join + "</ul>"
  end

  def search
    session[:from_ranking] = nil
    # isso é a mesma coisa que fazer: query = params[:q] || params[:search][:q]
    #                              e: @from = params[:from] || params[:search][:q]
    if params[:q]
      query = params[:q]
      @from = params[:from]
    elsif params[:search]
      query = params[:search][:q]
      @from = params[:search][:from]
    end

    # isso é a mesma coisa que fazer: redirect_to :action => @from if query.blank?
    if query.blank?
      redirect_to :action => @from
    end

    # isso é a mesma coisa que fazer: @from == "friends" ? @active_tab_friend = "aba_ativa" : @active_tab_players = "aba_ativa"
    # active_tab = if
    if @from == "friends"
      @active_tab_friends = "aba_ativa"
    else  
      @active_tab_players = "aba_ativa"
    end

    @users = query_search( query, @from ).paginate
  end

  def query_search(query, type = nil)
    users = User.search "#{query}*"
    # que tal: users.collect {|user| user if current_user.is_my_friend?(user.id)}
    if type == "friends"
      return users.select {|user| user if user.online? and current_user.is_my_friend?(user.id)}
    else
      return users.select {|user| user if user.online? }     
    end
  end
  
  
  private
  
  def set_challenges
    @challenges_made = Challenge.all( :conditions => [ "user_id_target = ? AND status = ?", current_user, Challenge::INVITED ] )
    #@challenged_by = Challenge.all( :conditions => [ "user_id = ? AND status = ?", current_user, Challenge::INVITED ] )
  end
  
  ##
  # Ordena uma lista de jogadores primeiro pelo seu estado (livre/jogando), e então pelo apelido (case insensitive)
  ##
  def self.sort_and_paginate_users( users, selected, current_user, page = 1 )
    users ||= []
    
    users.sort! { |a,b|
      if ( a.game.nil? == b.game.nil? )
        a.nickname.casecmp( b.nickname )
      else
        a.game ? 1 : -1
      end
    }
    if selected
      return users.select{|u| u.challenges_out_ids.include?(current_user.id)}.paginate(:per_page => FRIENDS_PER_PAGE,
                          :page => page )
    else  
      return users.paginate(:per_page => FRIENDS_PER_PAGE,
                          :page => page )  
    end
  end
  
end
