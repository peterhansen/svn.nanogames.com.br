class Admin::SuggestedQuestionsController < Admin::AdminController
  def index
    @controller_name = "#{t('.list')} #{t(".suggested_questions")}"
    @questions = SuggestedQuestion.not_approved.find(:all, :include => [:theme, :user])
  end

  def edit
    @suggested_question = SuggestedQuestion.find(params[:id])
    if @suggested_question.approved?
      flash[:class] = 'sucesso'
      flash[:result] = "Esta pergunta já foi aprovada."
      redirect_to :action => "index"
    end
    @themes = Theme.all.collect {|t| [t.name_with_level, t.id]}
  end

  def update
    @suggested_question = SuggestedQuestion.find(params[:id])
    
    if params[:suggested_question][:correct].blank?
      @themes = Theme.all.collect {|t| [t.name_with_level, t.id]}
      flash[:class] = 'error'
      flash[:result] = 'Por favor, escolha uma responsta correta'
      render :action => :edit
    else
      if @suggested_question.update_attributes(params[:suggested_question])
        Question.create_from_suggestion(@suggested_question)
        @suggested_question.update_attribute(:approved, true)
        user = @suggested_question.user
        user.score += 200
        user.save
#        Delayed::Job.enqueue(BackgroundJob.new('Emailer', 'deliver_suggested_question', user, @suggested_question))
        Emailer.delay.deliver_suggested_question(user, @suggested_question)
        flash[:class] = 'sucesso'
        flash[:result] = "Pergunta aprovada com sucesso e transferida para a lista de perguntas"
        redirect_to :action => :index
      else
        @themes = Theme.all.collect {|t| [t.name_with_level, t.id]}
        render :action => :edit
      end
    end
  end

  def destroy
    @question = SuggestedQuestion.find(params[:id])
    @question.destroy
    redirect_to :action => :index
  end
  
  def toggle_suggest_question
    static_data = StaticDataGame.first
    static_data.suggest_question_active = static_data.suggest_question_active ? false : true
    static_data.save
    expire_fragment( "box_sugestao_perguntas" )
    redirect_to :back
  end
  
  
end
