class Admin::RoomsController < Admin::AdminController
  before_filter :authenticate_admin!
  @@controller_name="Salas"
  
  def index
    @controller_name="Salas"
    @rooms = Room.paginate( :per_page=>10, :page=>params[:page] )
  end

  def new
    @controller_name = "#{t(:new_a)} #{t(:room)}"

    if Theme.count > 0
      @room = Room.new
      @themes = make_theme_box("not_theme")
    else
      flash[:class] = "padrao"
      flash[:result] = I18n.t(:atention)
      flash[:description] = I18n.t(:no_theme)
    end
  end
  
  def create
    @controller_name = "#{I18n.t(:new_a)} #{I18n.t(:room)}"
    @room = Room.new(params[:room])
    @themes = make_theme_box("not_theme")
    @room.themes = register_themes_on_room(params[:themes])
    
    if @room.save
      flash[:result] = I18n.t( :saved )
      flash[:class] = "sucesso"
      redirect_to :action=>'index'
    else
      @themes = Theme.all
      flash[:result] = I18n.t( :not_saved )
      flash[:class] = "erro"

      if @room.errors[:themes] == "em branco"
        flash[:description] = I18n.t(:not_possible_create_room_without_theme)
      end

      if @room.errors[:name] == "em branco"
        flash[:description] = I18n.t(:not_possible_create_room_without_name)
      end

      respond_to do |format|
        format.xml { render :xml=>@room.errors }
        format.html { render :action=>'new' }
      end
    end
  end

  def edit
    @controller_name = "#{I18n.t(:editing)} #{I18n.t(:room)}"
    if Theme.count > 0
      @room = Room.find( params[:id] )
      @themes = make_theme_box("not_theme")

    else
      flash[:class] = "padrao"
      flash[:result] = I18n.t(:atention)
      flash[:description] = I18n.t(:no_theme)
    end
  end

  def update
    @controller_name = "#{I18n.t(:editing)} #{I18n.t(:room)}"
    @room = Room.find( params[:id] )
    @themes = make_theme_box("not_theme")
    @room.name = params[:room][:name]
    @room.description = params[:room][:description]
    @room.capacity = params[:room][:capacity]
    if params[:room][:image]
      @room.image = params[:room][:image]
    end
    if params[:room][:background]
      @room.background = params[:room][:background]
    end
    @room.themes = register_themes_on_room(params[:themes])
    if @room.save
      flash[:result] = I18n.t( :updated )
      flash[:class] = "sucesso"
      redirect_to :action=>'index'
    else
      flash[:result] = I18n.t( :not_updated )
      flash[:class] = "erro"
      respond_to do |format|
        format.xml { render :xml=>@room.errors }
        format.html { render :action=>'edit' }
      end
    end
  end

  def destroy
    room=Room.find( params[:id] )
    if room.destroy
      flash[:result] =  I18n.t( :deleted_with_success )
      flash[:class] = "sucesso"
    else
      flash[:result] =  I18n.t( :not_deleted )
      flash[:class] = "erro"
    end
    redirect_to :action=>'index'
  end

  def get_themes_for_select
    @themes_for_select = make_theme_box("not_theme").collect{|theme| [theme.name_with_level, theme.id]}
  end

  def register_themes_on_room(params)
    themes_checked = []
    themes = []

    params.each do |key, value|
      if value == "1"
        themes_checked << key.to_i
      end
    end
    themes_checked.sort.reverse.each do |theme_id|
      theme = Theme.find(theme_id)
      themes << theme
    end
    return themes
  end

  def get_descendants
    theme = Theme.find(params[:theme_id])
    if theme.descendants_count?
      render :json=> theme.descendants
    else
      render :nothing => true
    end
  end
end
