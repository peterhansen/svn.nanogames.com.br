class Admin::UsersController < Admin::AdminController
  @@controller_name="Controle de usuários"

  def index
    @controller_name = "#{I18n.t(:users)}"
    @users = User.paginate(:per_page=>10, :page=>params[:page])
    
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def config_users
    @controller_name = "Configurações de usuários"
    @max_online_users = StaticDataGame.first.max_online_users
  end
  
  def update
    @user = User.find(params[:id])
    @user.blocked = params[:user][:blocked]

    if params[:user][:blocked] == "1"
      @user.lock_access!
      
    else
      @user.unlock_access!
    end

    if @user.save
      flash[:result] = I18n.t(:updated)
    else
      flash[:result] = I18n.t(:not_updated)
    end
    
    
    redirect_to :action=>'index'
  end  
  def update_max_online_users
    StaticDataGame.first.update_attribute(:max_online_users, params[:max_online_users])
    flash[:result] = I18n.t(:updated)
    redirect_to :action => :index
  end
  
  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      flash[:result] = I18n.t(:deleted)
    else
      flash[:result] = I18n.t(:not_deleted)
    end
    redirect_to :action => :index
  end
  
  def mailing
    @controller_name = "#{I18n.t(:mailing)}"
  end
  
  def send_mailing
    newsletter_contacts = NewsletterContact.all.collect {|n_c| ["#{n_c.name} #{n_c.post_name}", n_c.email, '',n_c.city, State.my_state_name(n_c.state_id)]}
    csv_file = FasterCSV.generate do |csv|
      csv << ['Nome', 'Email', 'Sexo', 'Cidade', 'Estado', 'Estado civil']
      ActiveRecord::Base.connection.execute("SELECT u.complete_name, u.email, g.name as gender, u.city, s.name as state, m.name as matrial_status from users u left join genders g on u.gender_id = g.id left join states s on u.state_id = s.id left join marital_statuses m on u.marital_status_id = m.id where u.receive_newsletter = 1").each do |result|
        csv << result
      end
      newsletter_contacts.each{|contact| csv << contact}
    end

    send_data csv_file, :filename => "mailing.csv"
  end
end
