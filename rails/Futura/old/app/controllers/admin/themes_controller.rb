class Admin::ThemesController < Admin::AdminController
  
  def index

    @controller_name = I18n.t(:themes)
    @themes = Theme.paginate( :per_page=>10, :page=>params[:page], :conditions=>{:parent_id=>nil})
  end

  def get_childrens
    theme = Theme.find(params[:theme_id])
    if theme.descendants_count?
      render :json=> theme.children
    end
  end

  def new
    @controller_name = I18n.t(:new_theme)
    @theme = Theme.new
    @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}
  end

  def create
    
    @theme = Theme.new( params[:theme] )
    @controller_name = I18n.t(:new_theme)
    @select_options = make_theme_box( "not_theme" ).collect{|theme| [theme.name_with_level, theme.id]}

    if @theme.save
      
      flash[:result] = I18n.t( :saved )
      flash[:class] = "sucesso"
      redirect_to :action=>'index'
    else
      flash[:result] = I18n.t( :not_saved )
      flash[:class] = "erro"
      respond_to do |format|
        format.xml { render :xml=>@theme.errors }
        format.html { render :action=>'new' }
      end

    end 
  end

  def edit
    @theme = Theme.find( params[:id] )
    @controller_name = I18n.t(:edit_theme)
    @select_options = make_theme_box( @theme.id ).collect{|theme| [theme.name_with_level, theme.id]}
  end

  def update
    @theme = Theme.find( params[:id] )

    @controller_name = I18n.t(:edit_theme)
    @select_options = make_theme_box( @theme.id ).collect{|theme| [theme.name_with_level, theme.id]}

    if @theme.update_attributes(params[:theme])
      flash[:result] = I18n.t( :updated )
      flash[:class] = "sucesso"
      redirect_to :action=>'index'
    else
      flash[:result] = I18n.t( :not_updated )
      flash[:class] ="erro"
      respond_to do |format|
        format.xml { render :xml=>@theme.errors }
        format.html { render :action=>'edit' }
      end
    end

  end

  def destroy
    theme = Theme.find( params[:id] )
    if theme.children_count?
      theme.children.each do |children|
        children.destroy
      end
    end
    theme = Theme.find( params[:id] )
    if theme.destroy
      flash[:result] =  I18n.t( :deleted_with_success )
      flash[:class] = "sucesso"
    else
      flash[:result] =  I18n.t( :not_deleted )
      flash[:class] ="erro"
    end
    redirect_to :action=>'index'
  end

end
