class Admin::AdminsController < Admin::AdminController
  @@controller_name="Controle de Adminstradores"

  def index
    @controller_name = "Administradores"
    @admins = Admin.paginate( :per_page=>10, :page=>params[:page] )
  end
  
  def new
    @admin = Admin.new
  end
  
  def show
    redirect_to :action => :edit, :id => params[:id]
  end
  
  def edit
    @admin = Admin.find( params[:id] )
  end
  
  def create
    @admin = Admin.new( params[:admin] )
    if @admin.save
      flash[:result] = I18n.t( :saved )
      redirect_to :action=>'index'
    else
      flash[:result] = I18n.t( :not_saved )
      respond_to do |format|
        format.xml { render :xml=>@admin.errors }
        format.html { render :action=>'new' }
      end
    end
  end
  
  def update
    @admin = Admin.find( params[:id] )
    if @admin.update_attributes( params[:admin] )
      flash[:result] = I18n.t( :updated )
      redirect_to :action=>"index"
    else
      flash[:result] = I18n.t( :not_updated )
      respond_to do |format|
        format.xml { render :xml=>@admin.errors }
        format.html { render :action=>'edit' }
      end
    end
  end
  
  def destroy
    admin = Admin.find( params[:id] )
    if admin.destroy
      flash[:result] = I18n.t( :deleted_with_success )
    else
      flash[:result] = I18n.t( :not_deleted )
    end      
    redirect_to :action=>"index"
  end

  
end
