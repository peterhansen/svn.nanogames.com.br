#require "bundler/capistrano"

set :user, 'rails'
set :application, "pensi"
set :deploy_via, :export
set :repository,  "http://svn.nanogames.com.br/rails/pensi/"
set :scm, :subversion
set :scm_username, "nano_online_svn"
set :scm_password, 'nAnO_SvN123'
set :scm_verbose, true
set :use_sudo, false

namespace :deploy do
  task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
end