set :rails_env, "production"
set :deploy_to, "/var/www/rails/#{application}_staging"
set :current_path, "#{deploy_to}/current"
role :web, "staging.nanogames.com.br"
role :app, "staging.nanogames.com.br"
role :db, "staging.nanogames.com.br", :primary => true