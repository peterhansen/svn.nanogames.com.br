Pensi::Application.routes.draw do
  root :to => "pages#home"

  match '/home' => 'pages#home'
  match '/blank' => 'pages#blank'
  match '/new' => 'pages#home', :as => 'home'
  match '/buy/:video_id' => 'pages#buy', :as => 'buy'
  match '/share/:provider/:video_id' => 'pages#share', :as => 'share'
  match '/pages/send_share' => 'pages#send_share', :via => :post
  match '/videos/:video_id' => 'pages#videos', :as => 'videos'
  match '/video/:video_id' => 'pages#home', :as => 'video'
  match '/auth/:provider/callback' => 'pages#share'
  match '/feed' => 'pages#feed'
  match '/about' => 'pages#about'
  match '/faq' => 'pages#faq'
  match '/thankyou' => 'pages#thankyou'
end
