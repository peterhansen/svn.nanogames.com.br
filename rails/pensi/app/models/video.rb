class Video < ActiveRecord::Base
  belongs_to :category
  validates :name, :presence => true
  validates :filename, :presence => true
end