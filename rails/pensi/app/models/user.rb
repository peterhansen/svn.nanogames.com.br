class User < ActiveRecord::Base
  has_many :purchases
  
  def self.find_by_uid(uid)
    where('facebooK_uid = ? or twitter_uid = ?', uid, uid).first
  end

  def self.create_from_omniauth(auth_hash)
    create("#{auth_hash["provider"]}_uid" => auth_hash["uid"],
      "#{auth_hash["provider"]}_token" => auth_hash["credentials"]["token"],
      "#{auth_hash["provider"]}_secret" => auth_hash["credentials"]["secret"])
  end

  def bought?(video)
    purchases.find_by_video_id(video.id).present?
  end

  def has_provider?(provider)
    self.send("#{provider}_uid").present?
  end
end