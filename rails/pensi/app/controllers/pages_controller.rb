# encoding: UTF-8

class PagesController < ApplicationController
  include ApplicationHelper

  def temp
    head :ok
  end

  def blank
    render 'blank', :layout => nil
  end

  def home
    @videos = Video.all
    @categories = Category.where("parent_id is null")
    @subcategories = Category.where("parent_id is not null")
  end

  def buy
    redirect_to videos_path(:video_id => params[:video_id]) and return if current_user && current_user.purchases.map {|p| p.video_id.to_s }.include?(params[:video_id])

    @video = Video.find(params[:video_id])
    session[:video_id] = @video.id
    render 'buy', :layout => 'buy'
  end

  def valid_provider?(auth_hash)
    ['twitter', 'facebook'].include?(auth_hash["provider"])
  end

  def share
    auth_hash = request.env["omniauth.auth"]

    if auth_hash
      if current_user && current_user.has_provider?(auth_hash["provider"])
        @user = current_user
      elsif current_user
        @user = current_user
        @user.update_attributes("#{auth_hash["provider"]}_uid" => auth_hash["uid"],
                                "#{auth_hash["provider"]}_token" => auth_hash["credentials"]["token"],
                                "#{auth_hash["provider"]}_secret" => auth_hash["credentials"]["secret"])
      else
        @user = User.find_by_uid(auth_hash["uid"]) || (User.create_from_omniauth(auth_hash) if valid_provider?(auth_hash))
      end
      session[:user_id] = @user.id if @user
    end

    @video = Video.find(params[:video_id] || session[:video_id])
    render 'share', :layout => 'buy'
  end

  def videos
    redirect_to buy_path(:video_id => params[:video_id]) and return unless current_user.purchases.map {|p| p.video_id.to_s }.include?(params[:video_id])

    @video = Video.find(params[:video_id])
    render 'videos', :layout => 'buy'
  end

  def share_on(provider, message)
    if provider == 'twitter'
      Twitter.configure do |config|
        config.oauth_token = current_user.twitter_token
        config.oauth_token_secret = current_user.twitter_secret
      end

      Twitter.update(message)
    elsif provider == 'facebook'
      MiniFB.post(current_user.facebook_token,
                  current_user.facebook_uid,
                  :type => 'feed',
                  :message => message,
                  'application' => MiniFB::APP_ID)
    end
  end

  def thankyou
    @video = Video.find(params[:video_id])

    render 'thankyou', :layout => 'buy'
  end

  def send_share
    share_on(params['provider'],"Estude com os melhores! Eu já peguei minha #{current_user.purchases.count + 1}ª dica para o Bolsão 2012 do PENSI. Pegue a sua em http://www.bolsao2012.com.br #ficadica")
    @purchase = current_user.purchases.create(:video_id => (params[:video_id] || session[:video_id])) if current_user

    redirect_to :action => :thankyou, :video_id => params[:video_id]

  rescue MiniFB::FaceBookError => e
    @video = Video.find(params[:video_id])
    flash[:notice] = "Facebook Error(#{e.code == 400 ? 'Você já postou essa mensagem' : e.message})"
    render :share, :layout => 'buy'
  rescue Twitter::Forbidden => e
    @video = Video.find(params[:video_id])
    flash[:notice] = "Twitter Error(#{e.message.match(/403/) ? 'Você já postou essa mensagem' : e.message})"
    render :share, :layout => 'buy'
  end

  def feed
    @videos = Video.all

    respond_to do |format|
      format.atom { render :layout => false }
    end
  end
end