atom_feed do |feed|
  feed.title("Bolsão PENSI")
  feed.updated(Time.now)

  for video in @videos
    #binding.pry
    feed.entry(video) do |entry|
      entry.title(video.name)
      entry.description(video.category.name)

      entry.author do |author|
        author.name("Bolsão PENSI")
      end
    end
  end
end