class AddPosterAndThumbToVideos < ActiveRecord::Migration
  def self.up
    add_column :videos, :poster, :string
    add_column :videos, :thumb, :string
  end

  def self.down
    remove_column :videos, :thumb
    remove_column :videos, :poster
  end
end
