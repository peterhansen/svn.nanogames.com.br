class AddProfessorToVideos < ActiveRecord::Migration
  def self.up
    add_column :videos, :professor, :string
  end

  def self.down
    remove_column :videos, :professor
  end
end
