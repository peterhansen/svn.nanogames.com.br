class CreateVideos < ActiveRecord::Migration
  def self.up
    create_table :videos, :force => true do |t|
      t.string :name
      t.string :filename
      t.integer :category_id
      t.timestamps
    end
  end

  def self.down
    drop_table :videos
  end
end