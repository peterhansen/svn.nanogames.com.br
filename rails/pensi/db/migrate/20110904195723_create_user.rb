class CreateUser < ActiveRecord::Migration
  def self.up
    create_table :users, :force => true do |t|
      t.string :twitter_uid
      t.string :twitter_token
      t.string :twitter_secret
      t.string :facebook_uid
      t.string :facebook_token
      t.string :facebook_secret
      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
