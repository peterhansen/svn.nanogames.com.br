# encoding: UTF-8

Category.delete_all
Video.delete_all

Category.create([
  {:name => "Pré-Vestibular"},
  {:name => "2° Grau"}
])

Category.create([
  {:name => "Matemática", :parent => Category.first},
  {:name => "Português", :parent => Category.last},
  {:name => "Biologia", :parent => Category.first}
])

subcategories = Category.where('parent_id is not null')
Video.create([
  { :name => 'Video 1', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories.first },
  { :name => 'Video 2', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories.last },
  { :name => 'Video 3', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories[1] },
  { :name => 'Video 4', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories.first },
  { :name => 'Video 5', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories.last },
  { :name => 'Video 6', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories.first },
  { :name => 'Video 7', :filename => 'trailer_400p', :poster => 'trailer_400p', :professor => 'Jurema', :category => subcategories.last }
])