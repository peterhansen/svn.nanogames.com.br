FactoryGirl.define do
  sequence :string do |n|
    "Object_#{n}"
  end

  factory :category do
    name          { FactoryGirl.generate :string }
  end

  factory :video do
    name          { FactoryGirl.generate :string }
    filename      { FactoryGirl.generate :string }
  end

  factory :user do
    twitter_uid    '12345'
    twitter_token  '330387680-93ien5m9xEA592Kga8CfzGcOgAt8fcRntGItZL0d'
    twitter_secret 'bnCXbFWOXz1u8ZYdvqWlmgCLRN0n7kQOgl37GaofQ'
    facebook_uid   '54321'
    facebook_token 'AAAC5XZAjIG5ABAB3QZBtCZCul0RHhGZBVmQEHSsFszPXESvrPeYSonx9J2OZCsQg88JZAQRZC01OWzZAAXTpa8n5OVexrBMlaOghlDdbblZAZAIQZDZD'
  end
end