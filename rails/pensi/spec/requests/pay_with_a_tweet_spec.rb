# encoding: UTF-8

require File.dirname(__FILE__) + '/../spec_helper'

feature "Pague com um share", %q{
  Como um usuário
  para poder assistir os videos
  eu devo 'pagar com um share'
} do

  background %q{
    Dado que existe um video cadastrado
    E que eu não compartilhei nenhum vídeo
  } do
    Video.delete_all
    FactoryGirl.create :video, :name => 'Video 1'
  end
  
  scenario %q{
    Quando eu visitar o site
    Eu não vou poder assistir nenhum video
  } do

    visit root_path
    page.should_not have_selector('a.watch')
    page.should have_selector("a.buy_with_a_tweet")
    page.should have_content("Video 1")
  end
  
  #scenario %q{
  #   Quando eu visitar o site
  #   E clicar em 'Pague com um share'
  #   Um popup de share deve abrir
  #} do
  #   Capybara.current_driver = :selenium
  #
  #   visit root_path
  #   click_link 'Pague com um share'
  #
  #   within_window('buy') do
  #     page.should have_content("Video 1")
  #     page.should have_content('Pague com um Tweet')
  #     page.should have_content('Pague com o Facebook')
  #   end
  #end

  scenario %q{
    Quando eu clico em 'Pague com um share'
    e clico em 'Pague com um Tweet'
    então eu devo ser redirecionado para o Twitter
  } do

    visit buy_path :video_id => Video.first.id
    click_link 'post_twitter'
    current_path.should == '/auth/twitter/callback'
  end

  scenario %q{
    Quando eu clico em 'Pague com um share'
    e clico em 'Pague com o Facebook'
    então eu devo ser redirecionado para o Facebook
  } do

    visit buy_path :video_id => Video.first.id
    click_link 'post_face'
    current_path.should == '/auth/facebook/callback'
  end

  scenario %q{
    Quando eu autorizo o app no Twitter
    Então eu tenho que ver um formulario de postagem no twitter
  } do

    visit buy_path :video_id => Video.first.id
    click_link 'post_twitter'
    page.should have_content("Estude com os melhores")
  end

  scenario %q{
    Quando eu autorizo o app no Facebook
    Então eu tenho que ver um formulario de postagem no Facebook
  } do

    visit buy_path :video_id => Video.first.id
    click_link 'post_twitter'
    page.should have_content('Estude com os melhores')
  end
end