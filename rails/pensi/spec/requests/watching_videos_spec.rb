# encoding: UTF-8

require File.dirname(__FILE__) + '/../spec_helper'

feature "Videos", %q{
  Como um usuário
  para poder usufruir do Bolsão PENSI
  eu devo poder assistir videos
} do

  scenario %q{
    Dado que existe um video cadastrado
    Quando eu visitar o site
    Eu devo ver o thumbnail de um video
  } do

    video = FactoryGirl.create :video, :name => 'Video 1'
    visit root_path
    page.should have_selector('a.buy_with_a_tweet')
    page.should have_content("Video 1")
  end

  scenario %q{
    Dado que existe não há nenhum video cadastrado
    Quando eu visitar o site
    Eu não devo ver o thumbnail de um video
  } do

    Video.delete_all
    visit root_path
    page.should_not have_selector('a.buy_with_a_tweet')
  end

  scenario %q{
    Dado que existe um video cadastrado
    E eu já comprei esse video
    Então eu devo ver o link 'Assista agora'
  } do
    
    # video = FactoryGirl.create :video, :name => 'Video 2'
    # user = FactoryGirl.create :user
    # user.purchases.build :video => video

    # visit root_path
    # page.should have_content('Assista agora')

    # Video.delete_all
    # User.delete_all
  end
end