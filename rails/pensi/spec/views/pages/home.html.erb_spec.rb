# encoding: UTF-8

require File.dirname(__FILE__) + '/../../spec_helper'

describe "pages/home.html.erb" do
  before(:each) do
    @video = FactoryGirl.create :video, :name => 'Video 2'
    assign(:videos, [@video])
  end

  it "should let the user watch the videos he have bought" do
    user = FactoryGirl.create :user
    user.purchases.create :video => @video
    session[:user_id] = user.id

    render
    rendered.should match("Video 2")
    rendered.should match("watch")

    Video.delete_all
  end
end