require File.dirname(__FILE__) + '/../spec_helper'

describe Video do
  it { should validate_presence_of :name }
  it { should validate_presence_of :filename }
  it { should belong_to :category }
end