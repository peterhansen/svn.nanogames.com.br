require File.dirname(__FILE__) + '/../spec_helper'

describe User do
  it { should have_many :purchases }

  describe "#find_by_uid" do
    it "returs the user that have the given uid" do
      user1 = FactoryGirl.create :user, :twitter_uid => '123'
      user2 = FactoryGirl.create :user, :twitter_uid => '321'

      User.find_by_uid('123').should eql user1
    end
  end

  describe '#bought?' do
    it "should return true if user have bought the video" do
      user = FactoryGirl.create :user
      video = FactoryGirl.create :video
      user.purchases.create :video => video

      user.bought?(video).should be_true
    end

    it "should return false if user have not bought the video" do
      user = FactoryGirl.create :user
      video = FactoryGirl.create :video

      user.bought?(video).should be_false
    end
  end
end