require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PagesController do
  describe "routing" do
    get("/").should route_to "pages#home"
    get("/buy/1").should route_to "pages#buy", :video_id => '1'
    post("/pages/send_share").should route_to "pages#send_share"
    get("/pages/send_share").should_not be_routable
    post("/videos/1").should route_to "pages#videos", :video_id => '1'
    get("/auth/twitter/callback").should route_to "pages#share", :provider => 'twitter'
    get("/feed").should route_to "pages#feed"
    get("/about").should route_to "pages#about"
    get("/faq").should route_to "pages#faq"
    get("/thankyou").should route_to "pages#thankyou"
  end
end