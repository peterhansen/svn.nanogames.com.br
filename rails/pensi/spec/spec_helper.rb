require 'rubygems'
require 'spork'
require 'database_cleaner'

Spork.prefork do
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require "rspec/rails/extra/routing"
  require 'capybara/rspec'

  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:twitter] = {
    'provider' => 'twitter',
    'uid' => '12345',
    'credentials' => {
      'token' => '330387680-93ien5m9xEA592Kga8CfzGcOgAt8fcRntGItZL0d',
      'secret' => 'bnCXbFWOXz1u8ZYdvqWlmgCLRN0n7kQOgl37GaofQ'
    }
  }
  
  OmniAuth.config.mock_auth[:facebook] = {
    'provider' => 'facebook',
    'uid' => '54321',
    'credentials' => {
      'token' => 'AAAC5XZAjIG5ABAB3QZBtCZCul0RHhGZBVmQEHSsFszPXESvrPeYSonx9J2OZCsQg88JZAQRZC01OWzZAAXTpa8n5OVexrBMlaOghlDdbblZAZAIQZDZD'
    }
  }

  FakeWeb.allow_net_connect = %r[^https?://localhost]
  FakeWeb.allow_net_connect = %r[^https?://127\.0\.0\.1]
  FakeWeb.register_uri(:post, %r|https://api.twitter\.com/|, :body => "{}")
  FakeWeb.register_uri(:post, %r|https://graph.facebook\.com/|, :body => "{}")

  Capybara.javascript_driver = :webkit
  DatabaseCleaner.strategy = :truncation

  RSpec.configure do |config|
    config.mock_with :rspec
    # config.fixture_path = "#{::Rails.root}/spec/fixtures"
    # config.use_transactional_fixtures = true
  end
end

Spork.each_run do
  DatabaseCleaner.clean
end