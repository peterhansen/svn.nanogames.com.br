# encoding: UTF-8

require File.dirname(__FILE__) + '/../spec_helper'

describe ApplicationHelper do
  describe "#current_user" do
    it "If there are an user logged it returns the user" do
      session[:user_id] = '1234'
      User.should_receive(:find_by_id).with('1234').and_return('Usuário')
      helper.current_user.should == 'Usuário'
    end


    it "If there are no user logged it returns nil" do
      helper.current_user.should be_nil
    end
  end
end