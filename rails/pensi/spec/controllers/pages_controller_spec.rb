# encoding: UTF-8

require File.dirname(__FILE__) + '/../spec_helper'

describe PagesController do
  describe "GET /videos" do
    before(:each) do
      @video = FactoryGirl.create :video
    end

    it "should let an user see a video he has tweeted" do
      user = FactoryGirl.create :user
      user.purchases.create :video => @video

      get "videos", {:video_id => @video.id}, {:user_id => user.id}
      response.should render_template(:buy)
    end

    it "should not let an user see a video he has not tweeted" do
      get "videos", {:video_id => @video.id}, {:user_id => FactoryGirl.create(:user).id}
      response.should redirect_to buy_path(:video_id => @video.id)
    end
  end

  describe "GET /buy" do
    it "should redirect to /videos if user already shared the video" do
      video = FactoryGirl.create :video
      user = FactoryGirl.create :user
      user.purchases.create :video => video

      get "buy", {:video_id => video.id}, {:user_id => user.id}
      response.should redirect_to videos_path(:video_id => video.id)
    end

    it "should not redirect to /videos if user have not shared the video" do
      video = FactoryGirl.create :video
      user = FactoryGirl.create :user

      get "buy", {:video_id => video.id}, {:user_id => user.id}
      response.should render_template(:buy)
    end
  end
  
  describe "GET /share" do
    before(:each) do
      @video = FactoryGirl.create :video
      @request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:twitter]
    end

    context "using Twitter" do
      it 'should create an user if no user with this uid exists' do
        User.delete_all

        lambda do
          get "share", { :provider => 'twitter', :video_id => @video.id }
        end.should change(User, :count).by(1)
      end

      it 'should create an user with uid, token and secret' do
        get "share", { :provider => 'twitter', :video_id => @video.id }
        assigns(:user).twitter_uid.should == '12345'
        assigns(:user).twitter_token.should == '330387680-93ien5m9xEA592Kga8CfzGcOgAt8fcRntGItZL0d'
        assigns(:user).twitter_secret.should == 'bnCXbFWOXz1u8ZYdvqWlmgCLRN0n7kQOgl37GaofQ'
      end

      context 'if the user exists' do
        before(:each) do
          User.delete_all
          @user = FactoryGirl.create :user, :twitter_uid => '12345'
        end

        it 'should not create user' do
          lambda do
            get "share", { :provider => 'twitter', :video_id => @video.id }
          end.should_not change(User, :count).by(1)
        end

        it 'should set the user session' do
          get "share", { :provider => 'twitter', :video_id => @video.id }
          session[:user_id].should == @user.id
        end
      end

      it "should redirect to the video if the message is valid" do
        user = FactoryGirl.create :user
        session[:user_id] = user.id

        get "send_share", { :provider => 'twitter', :video_id => @video.id, :message => 'wip via @pensibolsao' }
        response.should redirect_to(:action => :thankyou, :video_id => @video.id)
      end
    end

    context "using Facebook" do
      before(:each) do
        User.delete_all
        @request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:facebook]
      end

      it 'should create an user if no user with this uid exists' do
        lambda do
          get "share", { :provider => 'facebook', :video_id => @video.id }
        end.should change(User, :count).by(1)
      end

      it 'should create an user with uid, token and secret' do
        get "share", { :provider => 'facebook', :video_id => @video.id }
        assigns(:user).facebook_uid.should == '54321'
        assigns(:user).facebook_token.should == 'AAAC5XZAjIG5ABAB3QZBtCZCul0RHhGZBVmQEHSsFszPXESvrPeYSonx9J2OZCsQg88JZAQRZC01OWzZAAXTpa8n5OVexrBMlaOghlDdbblZAZAIQZDZD'
      end

      context 'if the user exists' do
        before(:each) do
          User.delete_all
          @user = FactoryGirl.create :user, :facebook_uid => '54321'
        end

        it 'should not create user' do
          lambda do
            get "share", { :provider => 'facebook', :video_id => @video.id }
          end.should_not change(User, :count).by(1)
        end

        it 'should set the user session' do
          get "share", { :provider => 'facebook', :video_id => @video.id }
          session[:user_id].should == @user.id
        end
      end

      it "should redirect to the thankyou if the message is valid" do
        user = FactoryGirl.create :user
        session[:user_id] = user.id

        get "send_share", { :provider => 'facebook', :video_id => @video.id, :message => 'wip via @pensibolsao' }
        response.should redirect_to(:action => :thankyou, :video_id => @video.id)
      end
    end
  end

  describe "GET /send_share" do
    before(:each) do
      User.delete_all
      @user = FactoryGirl.create :user
      @video = FactoryGirl.create :video

      session[:user_id] = @user.id
    end

    it 'should create a purchase for the video' do
      lambda do
        get "send_share", { :video_id => @video.id, :message => 'via @pensibolsao' }
      end.should change(Purchase, :count).by(1)

      assigns(:purchase).video.should eql(@video)
    end

    context "using Twitter" do
      it "should update twitter status" do
        controller.should_receive(:share_on).with('twitter', 'via @pensibolsao')
        #Twitter.should_receive(:update).and_return(true)
        get "send_share", { :provider => 'twitter', :video_id => @video.id, :message => 'via @pensibolsao' }
      end
    end

    context "using Facebook" do
      it "should post to facebook wall" do
        controller.should_receive(:share_on).with('facebook', 'via @pensibolsao')
        get "send_share", { :provider => 'facebook', :video_id => @video.id, :message => 'via @pensibolsao' }
      end
    end
  end

  describe "GET /home" do
    before(:each) do
      Category.delete_all
      @category = FactoryGirl.create :category, :name => 'Categoria 1'
      @subcategory = FactoryGirl.create :category, :parent => @category
    end

    it "should assigns @categories" do
      get 'home'
      assigns(:categories).should == [@category]
    end

    it "should assigns @sub_categories" do
      get 'home'
      assigns(:subcategories).should == [@subcategory]
    end
  end

  describe "GET /feed" do
    it "assigns @videos" do
      Video.delete_all
      video = FactoryGirl.create :video
      get 'feed'
      assigns(:videos).should == [video]
    end
  end
end