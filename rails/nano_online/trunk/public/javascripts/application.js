$.fn.addItems = function(data, string, value) {
  return this.each(function() {
    var list = $(this);
    $(list).empty();
        $("<option>").attr("value", '').text('Selecione').appendTo($(this));
    $.each(data, function(index, itemData) {
        option = $("<option>").attr("value", itemData[value]).text(itemData[string]);
        option.appendTo(list);
    });
  });
};
