var ImagePreloader = {
  loaded: false,
  splash_element: "",
  base_url: window.location.origin + "/stylesheets/",
  style_sheets: [],
  images: [],

  async_parse_css: function(callback) {
    jQuery.each(this.style_sheets, function(index, css_file) {
      jQuery.get((ImagePreloader.base_url + css_file), function(response) {
        var tmp_images = response.match(/[^\("]+\.(gif|jpg|jpeg|png)/g);

        if(tmp_images) {
          jQuery.each(tmp_images, function(index, temp_image) {
            temp_image = temp_image.substring(5);
            var image = (temp_image.charAt(0) == '/' || temp_image.match('://')) ? temp_image : ImagePreloader.base_url + temp_image;

            if(jQuery.inArray(image, ImagePreloader.images) == -1) {
              ImagePreloader.images.push(image);
            }
          });
        }

        if(callback && (index == (ImagePreloader.style_sheets.length - 1))) {
          callback();
        }
      });
    });
  },

  load_images: function (callback) {
    var total = this.images.length;
    var processed = 0;

    jQuery.each(this.images, function(index, image_url) {
      var image = new Image();
      image.src = image_url;
      processed++;
      this.loaded = true;
      if(callback) {
        callback(parseInt((processed * 100) / total));
      }
    });
  },

  css_files_to_load: function(css_files) {
    this.style_sheets = css_files;
  },

  set_splash_element: function(element) {
//    alert(this.loaded);
//    this.splash_element = element;
//    if(!this.loaded) {
//      $(element).show();
//    }
      $(document).ready(function(){
//        console.log($(element));
//        $(".main_area").hide();
//        $(element).show();
      });
  },

  load: function(callback) {
    this.async_parse_css(function(){
      ImagePreloader.load_images(callback);
    });
  },

  load_with_splash: function(splash_element, callback) {
    if(!this.loaded) {
      this.async_parse_css(function() {
        ImagePreloader.load_images(callback);
        $(splash_element).hide();
      });
    }    
  }
};