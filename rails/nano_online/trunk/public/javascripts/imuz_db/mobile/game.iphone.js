OPTION_LETTERS = ['A', 'B', 'C', 'D']
REQUEST_STATES = ['new_question_iphone', 'skip_question', 'missed_question']
ANSWER = 0
SKIP = 1
MISSED = 2
FLOW_STATES = []
ROOT_REQUEST = "/imuz_db/game_sessions/"

RENDER_STATES = ['question', 'answer', 'time_up']

CORRECT_ANIMATION_PROPERTIES = {
    elements: ['your_answer', 'state'], 
    direction: 'marginLeft', 
    to: ['10px', '10px'], 
    reset: ['-150px', '-350px'],
    easing: 'Linear'
}

WRONG_ANIMATION_PROPERTIES = {
    elements: ['your_answer', 'state'], 
    direction: 'marginLeft', 
    to: ['10px', '10px'], 
    reset: ['-150px', '-300px'],
    easing: ['Linear', 'Linear']
}

ANIMATION_PROPERTIES = {correct: CORRECT_ANIMATION_PROPERTIES, wrong: WRONG_ANIMATION_PROPERTIES}


question_flow_state = 0
answer_flow_state = 1
time_up_flow_state = 2

var question_main
var correct_main
var wrong_main
var time_up_main
var request_state
var flow_state
var correct_answer;
var answer_animation_state = 0
var wrong_animation_state = 0
var game_timeout;
var game_interval;
var game_timer;
var game_over_obj = null;
var skips_left = 3
var selected_answer
var asd = []
var event_sent = true
function tick(){
    game_timer.innerHTML = game_timer.innerHTML - 1
}


function stopTimer(){
    clearTimeout(game_timeout)
    clearInterval(game_interval)
    game_timer.innerHTML = 60
}

function toggleOptions(e){
    showHideOptions()
}

function showHideOptions(type){
    opt_bt = document.getElementById('options_bt')
    opt_element = jQuery("#options_view")
    skip_bt = document.getElementById('bt_skip')
    
    if(opt_bt.className === "small_button small_bt_pressed"){
        opt_element.slideUp()
        skip_bt.style.display = 'inline'
        opt_bt.className = "small_button"
    }
    else if(!type) {
        opt_element.slideDown()
        skip_bt.style.display = 'none'
        opt_bt.className = "small_button small_bt_pressed"
    }
}

function toggleAudioKnob(){
    audio_label = document.getElementById("audio_label")
    if(this.className.split(" ")[0] === 'stopLeft'){
        this.className = "turnOn"
        audio_label.innerHTML = "AUDIO ON"
    }
    else{
        this.className = "turnOff"
        audio_label.innerHTML = "AUDIO OFF"
    }
    return false
}

function startTimer(){
    stopTimer()
    game_timeout = setTimeout(missed, game_timer.innerHTML * 1000)
    game_interval = setInterval(tick, 1 * 1000)
}

function restartTimer(){
    stopTimer()
    startTimer()
}

window.onload = initGame


function initGame(){
    question_main = document.getElementById('question_main')
    answer_main = document.getElementById('answer')
    time_up_main = document.getElementById('time_up')
    FLOW_STATES = [question_main, answer_main, time_up_main]
    game_timer = document.getElementById('timer')
    startTimer()
    setGameListners()
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}



function setGameListners(){

    for(i=0;i<4;i++){
       el = document.getElementById('option_' + i)
       el.addEventListener(/*'click'*/'touchend', answer, false)
       //wink.ux.touch.addListener(el, "end", { method: "answer" })
       el.addEventListener(/*'click'*/'touchstart', setKnobToPoint, false)
    }
    document.getElementById('bt_next').addEventListener('click', nextQuestion, false)
//    wink.ux.touch.addListener(document.getElementById('bt_skip'), "end", { method: "skip" })
    document.getElementById('bt_skip').addEventListener('click', skip, false)
    document.getElementById('bt_timeup_next').addEventListener('click', function(e){nextQuestion(e,'time_up')}, false)
    document.getElementById('options_bt').addEventListener('click', toggleOptions, false)
}

function webkitAnimationHandler(e){
    e.preventDefault()
    if(e.animationName == "rotateLeft"){
        e.target.className = 'stopLeft'
    }
    else if(e.animationName == "rotateRight"){
        e.target.className = 'stopRight'
    }
    return false
}

function answer(e){
    if(event_sent){
        event_sent = false
        rel = this.rel
        stopTimer()
        selected_answer = rel
        respond(rel)
    }
}

function respond(selected_option){
    correct = document.getElementById('question_correct_option').innerHTML
    f_state = selected_option == correct ? 'correct' : 'wrong'
    animationReset(f_state)
    hideRespond()    
    
    if(selected_option == correct){
        showCorrect()
        application.sound.answer.correct()
    }
    else{
        showWrong()
        application.sound.answer.wrong()
    }
    
    knobReset()
    setTimeout(function(){
        animationStart(f_state)
    }, 1000)
//    answer_main.className = 'animate_background'
//    jQuery("#answer").attr('class', 'animate_background')
}

function knobReset(){
    document.getElementById('knob').className = 'knob_sprite'
}


function showCorrect(){
    document.getElementById('artist_name').style.display = 'inline'
    jQuery("#background_correct").show()
    answer_main.className = 'answer_correct_view'
    setTimeout(function(){jQuery('#bg_correct_highlight').show()},500)
//    jQuery("#answer").attr('class', 'animate_background')
    renderState(answer_flow_state)
}


function showWrong(){
    document.getElementById('the_correct_answer').style.display = 'inline'
    jQuery("#background_wrong").show()
    answer_main.className = 'answer_wrong_view'
    setTimeout(function(){jQuery('#bg_wrong_spiral').show()}, 500)
    renderState(answer_flow_state)
}

function getAnswerOption(answer_id){
    opt = answer_id.split('_')[1]
    return getOptionLetter(opt).toLowerCase()
}

function setKnobToPoint(option){
    document.getElementById('knob').className = 'knob_sprite knob_' + getAnswerOption(this.id)
}

function skip(){
//    skip_bt = document.getElementById('skip_count')
    if(skips_left > 0){
        skips_left--;
        renderSkip()
        request_state = ANSWER
        makeRequest(requestParams('skip'))
        
    }
}

function renderSkip(){
    skip_bt = document.getElementById('skip_count')
    if(skips_left <= 0){
        document.getElementById('bt_skip').style.display = 'none'
    }
    else{
        skip_bt.className = "skipcount sc_" + skips_left
    }
}

function missed(){
    stopTimer()
    showHideOptions('hide')
    renderState(time_up_flow_state)
    //request_state = MISSED
    //makeRequest(requestParams())
}

function renderState(f_state){
    showOnlyOne(changeState(f_state));
}

function changeState(f_state){
    old_state = flow_state || 0
    flow_state = f_state
    return old_state
}

function showOnlyOne(old_state){
    if(flow_state != old_state){
        jQuery(FLOW_STATES[flow_state]).show()
        jQuery(FLOW_STATES[old_state]).hide()
    }
    
    
/*    if(flow_state == question_flow_state){
        FLOW_STATES[wrong_flow_state].style.display = 'none'
        FLOW_STATES[correct_flow_state].style.display = 'none'
        FLOW_STATES[time_up_flow_state].style.display = 'none'
        FLOW_STATES[question_flow_state].style.display = 'block'
    }
    else{
        FLOW_STATES[question_flow_state].style.display = 'none'
        FLOW_STATES[flow_state].style.display = 'block'
    }*/
}

function hideRespond(){
    jQuery('.background').css('display', 'none')
    jQuery('.bg_image').css('display', 'none')
    jQuery('#the_correct_answer').css('display', 'none')
    jQuery('#artist_name').css('display', 'none')
    
}
function newQuestion(data){
    /*if(data.correct_option){
        correct_answer = jQuery("[rel="+ data.correct_option +"]").attr('id').split("_")[1]
    }*/
    event_sent = true
    updateQuestion(data.question, data.options, data.correct_option_id)
    hideRespond()
    updateInfo(data.question, data.correct_option)
    updateHUD(data.game, data.user_score)
}



function nextQuestion(e, time_up){
    request_state = ANSWER
    if(time_up !== undefined){
        makeRequest(requestParams(time_up))
    }
    else{
        makeRequest(requestParams(selected_answer))
    }
/*console.log(game_over_obj)
    if(game_over_obj){
        window.location = '/imuz_db/game_sessions/game_over?game_id='+ game_over_obj.id +'&type=' + game_over_obj.type     
    }
    else{
        renderState(question_flow_state)
        startTimer()
    }*/
}

function updateQuestion(question, options, correct_option){
    document.getElementById('question_title').innerHTML = question.text
    document.getElementById('question_correct_option').innerHTML = correct_option
    for(index in options){
        option = options[index]
        opt_element = document.getElementById('option_' + index)
        opt_element.innerHTML = option.text
        opt_element.rel = option.id
    }   
}

function updateHUD(game, user_score){
    document.getElementById('user_score').innerHTML = game.hits
    in_question = game.hits + game.misses + 1
    document.getElementById('current_question').innerHTML = in_question 
}

function updateInfo(question, correct_option){
    jQuery("#artist_name").html(question.artist_name)
    jQuery("#correct_answer").html(correct_option)
    jQuery("#artist_thumb").attr('src', question.artist_photo)
   /* jQuery("#biography_link").attr('href', question.artist_url)
    jQuery("#video_link").attr('href', 'http://www.youtube.com/watch?v=' + question.youtube)*/
}

function changeBackground(game_state){
    //document.getElementById("answer_" + game_state).className = 'animate_background'
}

function animationReset(state){
    properties = ANIMATION_PROPERTIES[state]
    for(i in properties.elements){
        //jQuery("#" + properties.elements[i]).css(properties.direction, properties.reset[i])  
        jQuery("#" + properties.elements[i]).hide()
    }
}


function animationStart(f_state){
    properties = ANIMATION_PROPERTIES[f_state]
    for(i in properties.elements){
        //jQuery("#" + properties.elements[i]).css(properties.direction, properties.reset[i])  
        jQuery("#" + properties.elements[i]).fadeIn()
    }
    
    /*index = answer_animation_state % 2
    answer_animation_state++
    param = {}
    param[properties.direction] = properties.to[index]
    animationTrigger(properties, index, param, f_state)*/
}


function animationTrigger(properties, index, param, f_state){
  //  if(RENDER_STATES[flow_state] == 'correct'){
        jQuery("#" + properties.elements[index]).animate(param, 600, animationCallback(index, f_state))        
    /*}
    else{
        jQuery("#" + properties.elements[index]).gx( param, 1000, properties.easing[index], animationCallback(index));
    }*/
}

function animationCallback(index, f_state){
    return index == 0 ? function(){animationStart(f_state)} : function(){}
}


function getOptionLetter(option){
    opt = parseInt(option) % 4
    return OPTION_LETTERS[opt]
}


function makeRequest(params){
    jQuery.ajax({
      url: ROOT_REQUEST + REQUEST_STATES[request_state],
      type: "GET",
      data: (params),
      dataType: "json",
      async: false,
      success: handlerResponse
   })
}

function requestParams(option_id){
    params = {'game_id': current_game_id}
    if(option_id == 'skip'){
        params["skipped"] = 'true'
    }
    else if(option_id == 'time_up'){
        params["time_up"] = 'true'
    }
    else if(option_id){
        params["choosed"] =  parseInt(option_id)
    }
    return params
}

function handlerResponse(data){
    if(data.game_over){
        window.location = '/imuz_db/game_sessions/game_over?type=' + data.game_over.type + "&game_id=" + data.game.id
    }
    else{
        newQuestion(data)
        startTimer()
        renderState(question_flow_state)
    }
    /*if(data.game_over){
        game_over_obj = {type: data.game_over.type, id: data.game.game_session.id}
    }
    else{
        game_over_type = null
    }   
    if(data.timeup){
        newQuestion(data)
        renderState(time_up_flow_state)
    }
    else if(data.game_state){
        updateInfo(data.question.previous.question, data.game_state, data.correct_option)
        animationReset(data.game_state)
        changeBackground(data.game_state)
        document.getElementById("artist_thumb_" + data.game_state).addEventListener('load', function(){
          renderState(window[data.game_state + '_flow_state'])
          setTimeout(function(){animationStart()}, 3000)
          setTimeout(function(){newQuestion(data)}, 2000)
            
        })
        //

//        setTimeout(animationStart, 1000)
    }
    else{
        if(data.skipping && data.game.game_session.skips <= 3){
            newQuestion(data)
            skips = data.game.game_session.skips
            renderSkip(skips)
        }
    }*/
}
