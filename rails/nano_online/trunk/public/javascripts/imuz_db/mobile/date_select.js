function DateSelect(obj){
    this.day_element = (obj && obj.day) || document.getElementById('customer_birthday_3i');
    this.month_element = (obj && obj.month) || document.getElementById('customer_birthday_2i');
    this.year_element = (obj && obj.year) || document.getElementById('customer_birthday_1i');
    this.month = this.month_element.value;
    this.year = this.year_element.value;
    this.old_month_days = null;
    this.old_year_is_leap = null;
    this.month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    
    this.setListners = function(self){
        this.year_element.addEventListener('change', function(){self.update({'element': this, 'attr': 'year_element'})}, false)
        this.month_element.addEventListener('change', function(){self.update({'element': this, 'attr': 'month_element'})}, false)
    }
    this.update = function(obj){
        if(obj){
            this[obj.attr] = obj.element;
            this[obj.attr.split("_")[0]] = obj.element.value;
        }
        if((this.days() != this.old_month_days) || (this.yearIsLeap() != this.old_year_is_leap)){
           this.old_month_days = this.days();
           this.old_year_is_leap = this.yearIsLeap();
           this.setDays();
        }
    }
    this.yearIsLeap = function (){
        if( ((this.year % 4) == 0)  && ( (this.year % 100) != 0) || ( (this.year % 400) == 0 )){
            return true;
        }
        return false;
    }
    
    this.monthName = function(){
        this.month_names[this.month - 1]
    }
    
    this.days = function(){
        if( this.month == 2 ){
            if(this.yearIsLeap())
                return 29
            else
                return 28
        }
        else if( this.month < 8 ){
            if((this.month % 2) != 0)
                return 31;
            else 
                return 30;
        }
        else if( this.month >= 8 ){
            if((this.month % 2) == 0)
                return 31;
            else 
                return 30;
        } 
    }
    this.construcOptions = function(){
        n_days = '';
        for( i = 1; i <= this.days(); i++ ){
            if(i == (this.day_element.selectedIndex + 1)){
                n_days = n_days.concat('<option selected="selected" value="'+ i +'">'+ i +'</option>')
            }
            else{
                n_days = n_days.concat('<option value="'+ i +'">'+ i +'</option>')
            }
        }
        return n_days;        
    }
    this.setDays = function(){
        this.day_element.innerHTML = this.construcOptions();
    }
    this.setListners(this);
    return this;
}
