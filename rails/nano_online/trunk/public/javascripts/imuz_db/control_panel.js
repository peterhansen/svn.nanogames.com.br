/* Code for the amp's control panel */

var poweredOn = true;

function tooglePowerSwitch(){
  if(poweredOn == true) {
    $('.power_switch').css("background-position", "0 -80px");
    poweredOn = false;
  } else {
    $('.power_switch').css("background-position", "0 0");
    poweredOn = true;
  }
  $('#quit_modal').toggle();
}

function replaceText(knobStatus){
  if(knobStatus == "off"){
    $('#audio_knob span em').replaceWith('<em>Off</em>');
    $('#audio_knob span em').css('color','#8A7142');
  }
  else if(knobStatus == "on"){
    $('#audio_knob span em').replaceWith('<em>On</em>');
  }
}

/* Sprite Animation */

function animateSprite(spriteElement){
  spriteElement.css('background-position', '0 0');
  spriteElement.show();
  spriteElement.sprite({fps: 12, no_of_frames: 5, play_frames: 5});
}

/* Profile Loading and Animation */

function loadProfile(){
  $('.player_profile').fadeIn(500, profileSlideIn);

  function profileSlideIn(){
    $('.profile_content').gx(
    { top: '5px'},
        400,
        'Linear'
        );
  }
}

function unloadProfile(){
  $('.profile_content').gx(
    { top: '-420px'},
    400,
    'Linear',
    function(){
      $('.player_profile').fadeOut(500);
    }
  );
}

$(document).ready(function(){
  $("#nickname_form").submit(function() {
    $.post($(this).attr("action"), $(this).serialize(), null, "script");
    return false;
  });

  /* Profile Initial Position */

  $('.profile_content').css('top','-420px');

  /* Power Switch Click Events */

  $('.power_switch').click(tooglePowerSwitch);

  /* Knob Initial Values - Audio Knob is On, Player Knob is Off */

  function isAudioOn() {
    if($('.on_to_off').css("display") == 'none') {
      return false;
    }
      else {
      return true;
    }
  }

  var audioKnobOn = isAudioOn();

  var playerKnobOn = false;

  $('#player_knob .on_to_off').hide();


  /* Audio Knob Click Handlers */

  $('#audio_knob').click(function(){
    if(audioKnobOn){
      $('#audio_knob .off_to_on').hide();
      animateSprite($('#audio_knob .on_to_off'));
      setTimeout("replaceText('off');", 400);
      audioKnobOn = false;
    }
    else{
      $('#audio_knob .on_to_off').hide();
      animateSprite($('#audio_knob .off_to_on'));
      setTimeout("replaceText('on');", 400);
      audioKnobOn = true;
    }
  });

  /* Player Knob Click Handlers */

  $('#player_knob').click(function(){
    if(playerKnobOn){
      $('#player_knob .off_to_on').hide();
      animateSprite($('#player_knob .on_to_off'));
      playerKnobOn = false;
      setTimeout("unloadProfile();", 400);
    }
    else{
      $('#player_knob .on_to_off').hide();
      animateSprite($('#player_knob .off_to_on'));
      setTimeout("loadProfile();", 400);
      playerKnobOn = true;
    }
  });
});