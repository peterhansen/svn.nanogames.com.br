function correctText(){
  $('.your_answer').animate(
      { top: '100px'},
      1000,
      'easeOutBounce'
  );
}

/* Question Mouse Effects */

var selected_answer = '';

$(document).ready(function(){

    function animateToActive(listItem){
        $(listItem).css('background','#B4A17E');
        $(listItem).gx({'background-color': '#900F0E'}, 200, 'Linear', function(element){element.addClass("selected");});
    }

    function restoreInactive(listItem){
        $(listItem).gx({'background-color': '#B4A17E'}, 200, 'Linear', function(element){
            element.removeClass("selected");
            element.css('background','url(/images/imuz_db/bg_answer.png)');
        });
    }

    // Mark clicked as selected

    $('.answers li').click(function(){
        if(selected_answer != this){
            restoreInactive(selected_answer);
            $(this).addClass("selected");
            selected_answer = this;
        }
    });

    // Mouse Over Animation

    $(".answers li").hover(
        function(){
            if (selected_answer != this)
                animateToActive(this);
        },
        function(){
            if (selected_answer != this)
                restoreInactive(this);
        }
    );

    // Calls the correct answer animation

    $('.question_feedback').fadeIn(800, correctText);
});





