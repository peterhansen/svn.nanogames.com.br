DELIMITER $$

# O lixo do MySQL não permite parâmetros DEFAULT... Se permitisse, usaríamos:
# max = 100
# firstEntryOffset = 0

#	IN appID INT, 				# Id da versão do o aplicativo cujo ranking deveremos retornar
#	IN max INT,				# Número máximo de entradas que devemos retornar. Se for <= 0, retorna o ranking inteiro
#	IN firstEntryOffset INT		# Deslocamento da primeira entrada do ranking

CREATE PROCEDURE GetOnlineRankingForApp( IN appID INT, IN max INT, IN firstEntryOffset INT )
LANGUAGE SQL
NOT DETERMINISTIC
SQL SECURITY INVOKER
BEGIN
	# Tabela temporária que irá armazenar o ranking
	# Utilizamos a cláusula [IF NOT EXISTS] pois podemos estar sendo chamados por outra SP que também precisa desta tabela
	CREATE TEMPORARY TABLE IF NOT EXISTS ranking ( RankPos INT AUTO_INCREMENT PRIMARY KEY, ProfileId INT, Nickname TINYTEXT, Score BIGINT );

        # TODO
        # TRUNCATE TABLE DESTRÓI E RECRIA A TABELA, O QUE INVIABILIZA O RETORNO DE SPS ATRAVÉS DE TABELAS
        # TEMPORÁRIAS. O CERTO, ENTÃO, SERIA DELETAR TODAS AS ENTRADAS DA TABELA E RESETAR SEUS íNDICES
	# Garante que a tabela está vazia (pois pode ter sido criada por uma SP externa)
	#TRUNCATE TABLE ranking;

	# Obtém apenas a melhor pontuação de cada jogador, excluindo duplicatas
	INSERT INTO ranking( ProfileId, Nickname, Score )
	SELECT ProfileId, Nick, Score
	FROM (
	       SELECT C.id AS ProfileId, C.nickname AS Nick, max( R.score ) AS Score
    	  	  FROM ranking_entries R
    	  	  INNER JOIN  app_versions V ON R.app_version_id = V.id
	       INNER JOIN customers C ON R.customer_id = C.id
	       WHERE V.app_id = appID 
	       GROUP BY C.id
	     ) AS T
	ORDER BY T.Score DESC;

	# Esse select retorna o ranking final
	IF max > 0 THEN SELECT RankPos, Nickname, Score FROM ranking WHERE RankPos >= firstEntryOffset AND RankPos <= ( firstEntryOffset + max );
	ELSE SELECT RankPos, Nickname, Score FROM ranking WHERE RankPos >= firstEntryOffset; 
	END IF;
	
END $$

DELIMITER ;
