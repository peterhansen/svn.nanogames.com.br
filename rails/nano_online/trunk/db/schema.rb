# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110711215015) do

  create_table "accesses", :force => true do |t|
    t.text     "user_agent",                   :null => false
    t.text     "phone_number",  :limit => 255
    t.text     "ip_address"
    t.text     "http_path",                    :null => false
    t.text     "http_referrer"
    t.integer  "customer_id"
    t.integer  "device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone_hash"
  end

  add_index "accesses", ["customer_id"], :name => "customer_id"
  add_index "accesses", ["device_id"], :name => "device_id"

  create_table "ad_campaigns", :force => true do |t|
    t.integer  "advertiser_id", :null => false
    t.string   "title",         :null => false
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_campaigns", ["advertiser_id"], :name => "advertiser_id"

  create_table "ad_campaigns_ad_pieces", :id => false, :force => true do |t|
    t.integer  "ad_campaign_id", :null => false
    t.integer  "ad_piece_id",    :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_campaigns_ad_pieces", ["ad_campaign_id"], :name => "ad_campaign_id"
  add_index "ad_campaigns_ad_pieces", ["ad_piece_id"], :name => "ad_piece_id"

  create_table "ad_campaigns_apps", :id => false, :force => true do |t|
    t.integer "ad_campaign_id", :null => false
    t.integer "app_id",         :null => false
  end

  add_index "ad_campaigns_apps", ["ad_campaign_id"], :name => "ad_campaign_id"
  add_index "ad_campaigns_apps", ["app_id"], :name => "app_id"

  create_table "ad_channel_status", :force => true do |t|
    t.string   "status",     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ad_channel_statuses", :force => true do |t|
    t.string   "status",     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ad_channel_version_compositions", :force => true do |t|
    t.integer  "ad_channel_version_id", :null => false
    t.integer  "resource_type_id",      :null => false
    t.integer  "code",                  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_channel_version_compositions", ["ad_channel_version_id", "resource_type_id", "code"], :name => "channel_version_resource_type_index", :unique => true
  add_index "ad_channel_version_compositions", ["resource_type_id"], :name => "resource_type_id"

  create_table "ad_channel_versions", :force => true do |t|
    t.integer  "ad_channel_id",        :null => false
    t.integer  "ad_channel_status_id", :null => false
    t.string   "title",                :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_channel_versions", ["ad_channel_id"], :name => "ad_channel_id"
  add_index "ad_channel_versions", ["ad_channel_status_id"], :name => "ad_channel_status_id"

  create_table "ad_channel_versions_app_versions", :id => false, :force => true do |t|
    t.integer "ad_channel_version_id", :null => false
    t.integer "app_version_id",        :null => false
  end

  add_index "ad_channel_versions_app_versions", ["ad_channel_version_id", "app_version_id"], :name => "by_channel_version_and_app_version"
  add_index "ad_channel_versions_app_versions", ["app_version_id"], :name => "index_ad_channel_versions_app_versions_on_app_version_id"

  create_table "ad_channel_versions_resource_types", :id => false, :force => true do |t|
    t.integer "ad_channel_version_id", :null => false
    t.integer "resource_type_id",      :null => false
  end

  add_index "ad_channel_versions_resource_types", ["ad_channel_version_id", "resource_type_id"], :name => "by_channel_version_and_resource_type"
  add_index "ad_channel_versions_resource_types", ["ad_channel_version_id"], :name => "by_channel_version"
  add_index "ad_channel_versions_resource_types", ["resource_type_id"], :name => "resource_type_id"

  create_table "ad_channels", :force => true do |t|
    t.string   "title",                :null => false
    t.text     "description",          :null => false
    t.integer  "ad_channel_status_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_channels", ["ad_channel_status_id"], :name => "index_ad_channels_on_ad_channel_status_id"

  create_table "ad_piece_version_visualizations", :force => true do |t|
    t.integer  "ad_piece_version_id", :null => false
    t.integer  "customer_id"
    t.integer  "device_id"
    t.datetime "time_start",          :null => false
    t.datetime "time_end"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_piece_version_visualizations", ["ad_piece_version_id"], :name => "ad_piece_version_id"
  add_index "ad_piece_version_visualizations", ["customer_id"], :name => "customer_id"
  add_index "ad_piece_version_visualizations", ["device_id"], :name => "device_id"

  create_table "ad_piece_versions", :force => true do |t|
    t.integer  "ad_piece_id",           :null => false
    t.integer  "ad_channel_version_id", :null => false
    t.string   "title",                 :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_piece_versions", ["ad_channel_version_id"], :name => "ad_channel_version_id"
  add_index "ad_piece_versions", ["ad_piece_id"], :name => "ad_piece_id"

  create_table "ad_piece_versions_resources", :id => false, :force => true do |t|
    t.integer "ad_piece_version_id", :null => false
    t.integer "resource_id",         :null => false
  end

  add_index "ad_piece_versions_resources", ["ad_piece_version_id", "resource_id"], :name => "by_ad_piece_version_and_resource"
  add_index "ad_piece_versions_resources", ["resource_id"], :name => "index_ad_piece_versions_resources_on_resource_id"

  create_table "ad_pieces", :force => true do |t|
    t.string   "title",         :null => false
    t.text     "description"
    t.integer  "ad_channel_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ad_pieces", ["ad_channel_id"], :name => "ad_channel_id"

  create_table "ads", :force => true do |t|
    t.integer  "ad_piece_id", :null => false
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ads", ["ad_piece_id"], :name => "ad_piece_id"

  create_table "advertisers", :force => true do |t|
    t.integer  "partner_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "advertisers", ["partner_id"], :name => "partner_id"

  create_table "answers_questions", :id => false, :force => true do |t|
    t.integer "imuz_db_answer_id"
    t.integer "imuz_db_question_id"
  end

  add_index "answers_questions", ["imuz_db_answer_id"], :name => "imuz_db_answer_id"
  add_index "answers_questions", ["imuz_db_question_id"], :name => "imuz_db_question_id"

  create_table "app_categories", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "app_categories_apps", :id => false, :force => true do |t|
    t.integer "app_id",          :null => false
    t.integer "app_category_id", :null => false
  end

  add_index "app_categories_apps", ["app_category_id"], :name => "index_app_categories_apps_on_app_category_id"
  add_index "app_categories_apps", ["app_id", "app_category_id"], :name => "index_app_categories_apps_on_app_id_and_app_category_id"

  create_table "app_customer_datas", :force => true do |t|
    t.integer  "app_version_id",                             :null => false
    t.integer  "customer_id",                                :null => false
    t.binary   "data",                                       :null => false
    t.integer  "sub_type",       :limit => 2, :default => 0, :null => false
    t.integer  "slot",           :limit => 2, :default => 0, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "app_customer_datas", ["app_version_id"], :name => "app_version_id"
  add_index "app_customer_datas", ["customer_id"], :name => "customer_id"

  create_table "app_files", :force => true do |t|
    t.integer  "app_version_id", :null => false
    t.text     "filename"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "app_files", ["app_version_id"], :name => "app_version_id"

  create_table "app_version_states", :force => true do |t|
    t.string   "name",       :limit => 20, :null => false
    t.integer  "code",                     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "app_version_states", ["code"], :name => "app_version_states_code"
  add_index "app_version_states", ["name"], :name => "app_version_states_name"

  create_table "app_version_updates", :force => true do |t|
    t.integer "app_version_id"
    t.integer "updated_app_version_id"
    t.boolean "required",               :default => true, :null => false
  end

  add_index "app_version_updates", ["app_version_id", "updated_app_version_id"], :name => "index_app_version_updated_app_version"
  add_index "app_version_updates", ["app_version_id"], :name => "index_app_version_updates_on_app_version_id"
  add_index "app_version_updates", ["updated_app_version_id"], :name => "updated_app_version_id"

  create_table "app_versions", :force => true do |t|
    t.integer  "app_id",                                             :null => false
    t.text     "release_notes"
    t.text     "number",               :limit => 255,                :null => false
    t.integer  "app_version_state_id",                :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  add_index "app_versions", ["app_id"], :name => "app_id"
  add_index "app_versions", ["app_version_state_id"], :name => "app_version_state_id"

  create_table "app_versions_customers", :id => false, :force => true do |t|
    t.integer "app_version_id", :null => false
    t.integer "customer_id",    :null => false
  end

  add_index "app_versions_customers", ["app_version_id", "customer_id"], :name => "index_app_versions_customers_on_app_version_id_and_customer_id"
  add_index "app_versions_customers", ["app_version_id"], :name => "app_version_id_index"
  add_index "app_versions_customers", ["customer_id"], :name => "index_app_versions_customers_on_customer_id"

  create_table "app_versions_families", :id => false, :force => true do |t|
    t.integer "app_version_id", :null => false
    t.integer "family_id",      :null => false
  end

  add_index "app_versions_families", ["app_version_id", "family_id"], :name => "index_app_versions_families_on_app_version_id_and_family_id"
  add_index "app_versions_families", ["app_version_id"], :name => "index_app_version_app_version_id"
  add_index "app_versions_families", ["family_id"], :name => "index_app_versions_families_on_family_id"

  create_table "app_versions_news_feed_categories", :id => false, :force => true do |t|
    t.integer  "app_version_id",        :null => false
    t.integer  "news_feed_category_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "app_versions_news_feed_categories", ["app_version_id"], :name => "app_version_id"
  add_index "app_versions_news_feed_categories", ["news_feed_category_id"], :name => "news_feed_category_id"

  create_table "app_versions_ranking_types", :id => false, :force => true do |t|
    t.integer  "app_version_id",  :null => false
    t.integer  "ranking_type_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "app_versions_ranking_types", ["ranking_type_id"], :name => "ranking_type_id"

  create_table "app_versions_submissions", :id => false, :force => true do |t|
    t.integer "app_version_id", :null => false
    t.integer "submission_id",  :null => false
  end

  add_index "app_versions_submissions", ["app_version_id", "submission_id"], :name => "ver_subm_id"
  add_index "app_versions_submissions", ["app_version_id"], :name => "app_version_id_index"
  add_index "app_versions_submissions", ["submission_id"], :name => "index_app_versions_submissions_on_submission_id"

  create_table "apple_download_reports", :force => true do |t|
    t.text     "name",       :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "apple_download_reports", ["name"], :name => "apple_download_reports_name", :length => {"name"=>40}

  create_table "apple_downloads", :force => true do |t|
    t.integer  "apple_download_report_id",                                               :null => false
    t.text     "provider",                  :limit => 255
    t.text     "provider_country",          :limit => 255
    t.text     "developer"
    t.integer  "app_id",                                                                 :null => false
    t.integer  "apple_product_category_id",                                              :null => false
    t.integer  "units",                                                                  :null => false
    t.decimal  "developer_proceeds",                       :precision => 5, :scale => 2, :null => false
    t.date     "begin_date"
    t.date     "end_date"
    t.integer  "customer_currency_id",                                                   :null => false
    t.integer  "country_id",                                                             :null => false
    t.integer  "proceeds_currency_id",                                                   :null => false
    t.decimal  "customer_price",                           :precision => 5, :scale => 2, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sku"
  end

  add_index "apple_downloads", ["app_id"], :name => "app_id"
  add_index "apple_downloads", ["apple_download_report_id"], :name => "apple_download_report_id"
  add_index "apple_downloads", ["apple_product_category_id"], :name => "apple_product_category_id"
  add_index "apple_downloads", ["country_id"], :name => "country_id"
  add_index "apple_downloads", ["customer_currency_id"], :name => "fk_customer_currency"
  add_index "apple_downloads", ["proceeds_currency_id"], :name => "fk_royalty_currency"

  create_table "apple_product_categories", :force => true do |t|
    t.integer  "category",   :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "apple_product_categories", ["category"], :name => "apple_product_categories_category"

  create_table "apps", :force => true do |t|
    t.text     "name",                    :limit => 255, :null => false
    t.text     "name_short",              :limit => 255, :null => false
    t.text     "description_short"
    t.text     "description_medium"
    t.text     "description_long"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "apple_title"
    t.string   "apple_vendor_identifier"
    t.string   "apple_identifier"
  end

  add_index "apps", ["apple_title"], :name => "apps_apple_title"
  add_index "apps", ["name"], :name => "apps_name", :length => {"name"=>30}
  add_index "apps", ["name_short"], :name => "apps_name_short", :length => {"name_short"=>30}

  create_table "apps_partners", :id => false, :force => true do |t|
    t.integer "app_id",     :null => false
    t.integer "partner_id", :null => false
  end

  add_index "apps_partners", ["app_id"], :name => "index_apps_partners_on_app_id"
  add_index "apps_partners", ["partner_id", "app_id"], :name => "index_apps_partners_on_partner_id_and_app_id"
  add_index "apps_partners", ["partner_id"], :name => "partner_id_index"

  create_table "authentications", :force => true do |t|
    t.integer  "customer_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.string   "oauth_secret"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentications", ["customer_id"], :name => "customer_id_index"
  add_index "authentications", ["provider", "uid"], :name => "provider_uid_index"

  create_table "bands", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.integer  "frequency",                 :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bands_devices", :id => false, :force => true do |t|
    t.integer "device_id", :null => false
    t.integer "band_id",   :null => false
  end

  add_index "bands_devices", ["band_id"], :name => "index_bands_devices_on_band_id"
  add_index "bands_devices", ["device_id", "band_id"], :name => "index_bands_devices_on_device_id_and_band_id"
  add_index "bands_devices", ["device_id"], :name => "device_id_index"

  create_table "bands_operators", :id => false, :force => true do |t|
    t.integer "operator_id", :null => false
    t.integer "band_id",     :null => false
  end

  add_index "bands_operators", ["band_id"], :name => "band_id"
  add_index "bands_operators", ["operator_id", "band_id"], :name => "operator_band"

  create_table "cldc_versions", :force => true do |t|
    t.text     "version",     :limit => 255, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cldc_versions", ["version"], :name => "cldc_versions_version", :length => {"version"=>40}

  create_table "countries", :force => true do |t|
    t.text    "name",        :limit => 255, :null => false
    t.text    "iso_name_2",  :limit => 255, :null => false
    t.text    "iso_name_3",  :limit => 255, :null => false
    t.text    "iso_number",  :limit => 255, :null => false
    t.integer "currency_id"
  end

  add_index "countries", ["currency_id"], :name => "currency_id"
  add_index "countries", ["iso_name_2"], :name => "countries_iso_name_2", :length => {"iso_name_2"=>5}

  create_table "currencies", :force => true do |t|
    t.string   "code",       :limit => 3
    t.string   "name",       :limit => 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "currencies", ["code"], :name => "currencies_code"
  add_index "currencies", ["name"], :name => "currencies_name"

  create_table "customers", :force => true do |t|
    t.text     "nickname",        :limit => 255, :null => false
    t.text     "first_name",      :limit => 255
    t.text     "last_name",       :limit => 255
    t.text     "email",           :limit => 255
    t.text     "phone_number",    :limit => 255
    t.date     "birthday"
    t.text     "cpf",             :limit => 255
    t.string   "genre",           :limit => 1
    t.string   "hashed_password"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "validation_hash"
    t.datetime "validate_before"
    t.datetime "validated_at"
    t.boolean  "show_birthday"
  end

  add_index "customers", ["nickname"], :name => "customers_nickname", :length => {"nickname"=>40}
  add_index "customers", ["validation_hash"], :name => "customers_validation_hash"

  create_table "customers_devices", :id => false, :force => true do |t|
    t.integer "device_id",   :null => false
    t.integer "customer_id", :null => false
  end

  add_index "customers_devices", ["customer_id"], :name => "index_customers_devices_on_customer_id"
  add_index "customers_devices", ["device_id", "customer_id"], :name => "index_customers_devices_on_device_id_and_customer_id"
  add_index "customers_devices", ["device_id"], :name => "device_id_index"

  create_table "data_services", :force => true do |t|
    t.text     "name",       :limit => 255
    t.integer  "max_speed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "data_services_devices", :id => false, :force => true do |t|
    t.integer "device_id",       :null => false
    t.integer "data_service_id", :null => false
  end

  add_index "data_services_devices", ["data_service_id"], :name => "index_data_services_devices_on_data_service_id"
  add_index "data_services_devices", ["device_id", "data_service_id"], :name => "index_data_services_devices_on_device_id_and_data_service_id"
  add_index "data_services_devices", ["device_id"], :name => "device_id_index"

  create_table "device_bugs", :force => true do |t|
    t.text     "name",        :limit => 255, :null => false
    t.text     "description",                :null => false
    t.text     "workaround"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "device_bugs_devices", :id => false, :force => true do |t|
    t.integer "device_id",     :null => false
    t.integer "device_bug_id", :null => false
  end

  add_index "device_bugs_devices", ["device_bug_id"], :name => "index_devices_device_bugs_on_device_bug_id"
  add_index "device_bugs_devices", ["device_id", "device_bug_id"], :name => "index_devices_device_bugs_on_device_id_and_device_bug_id"
  add_index "device_bugs_devices", ["device_id"], :name => "device_id_index"

  create_table "device_integrators", :force => true do |t|
    t.integer  "device_id",                    :null => false
    t.integer  "integrator_id",                :null => false
    t.text     "identifier",    :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "device_integrators", ["device_id", "integrator_id"], :name => "index_device_integrators_on_device_id_and_integrator_id"
  add_index "device_integrators", ["device_id"], :name => "device_id_index"
  add_index "device_integrators", ["integrator_id"], :name => "index_device_integrators_on_integrator_id"

  create_table "devices", :force => true do |t|
    t.integer  "midp_version_id"
    t.integer  "cldc_version_id"
    t.integer  "vendor_id",                            :null => false
    t.integer  "family_id"
    t.text     "commercial_name",       :limit => 255
    t.text     "model",                 :limit => 255, :null => false
    t.integer  "screen_width",          :limit => 2
    t.integer  "screen_height_partial", :limit => 2
    t.integer  "screen_height_full",    :limit => 2
    t.integer  "jar_size_max",          :limit => 2
    t.integer  "memory_heap"
    t.integer  "memory_image"
    t.text     "user_agent",            :limit => 255
    t.boolean  "pointer_events"
    t.integer  "rms_total_size"
    t.integer  "rms_max_record_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "devices", ["cldc_version_id"], :name => "index_devices_on_cldc_version_id"
  add_index "devices", ["family_id"], :name => "index_devices_on_family_id"
  add_index "devices", ["midp_version_id"], :name => "index_devices_on_midp_version_id"
  add_index "devices", ["user_agent"], :name => "devices_user_agent", :length => {"user_agent"=>255}
  add_index "devices", ["vendor_id"], :name => "index_devices_on_vendor_id"

  create_table "devices_optional_apis", :id => false, :force => true do |t|
    t.integer "device_id",       :null => false
    t.integer "optional_api_id", :null => false
  end

  add_index "devices_optional_apis", ["device_id", "optional_api_id"], :name => "index_devices_optional_apis_on_device_id_and_optional_api_id"
  add_index "devices_optional_apis", ["optional_api_id"], :name => "index_devices_optional_apis_on_optional_api_id"

  create_table "devices_submissions", :id => false, :force => true do |t|
    t.integer "device_id",     :null => false
    t.integer "submission_id", :null => false
  end

  add_index "devices_submissions", ["device_id", "submission_id"], :name => "index_devices_submissions_on_device_id_and_submission_id"
  add_index "devices_submissions", ["submission_id"], :name => "index_devices_submissions_on_submission_id"

  create_table "download_codes", :force => true do |t|
    t.text     "code",        :limit => 255, :null => false
    t.text     "file",                       :null => false
    t.datetime "used_at"
    t.integer  "download_id",                :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id"
  end

  add_index "download_codes", ["customer_id"], :name => "customer_id"
  add_index "download_codes", ["download_id"], :name => "index_download_codes_on_download_id"

  create_table "downloads", :force => true do |t|
    t.integer  "access_id",      :null => false
    t.integer  "app_version_id", :null => false
    t.integer  "device_id",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id"
  end

  add_index "downloads", ["access_id"], :name => "access_id"
  add_index "downloads", ["app_version_id"], :name => "app_version_id"
  add_index "downloads", ["customer_id"], :name => "customer_id"
  add_index "downloads", ["device_id"], :name => "device_id"

  create_table "facebook_profiles", :force => true do |t|
    t.integer  "uid",                  :limit => 8, :null => false
    t.integer  "customer_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "name"
    t.string   "pic_small"
    t.string   "pic_big"
    t.string   "pic_square"
    t.string   "pic"
    t.datetime "profile_update_time"
    t.string   "timezone"
    t.string   "religion"
    t.string   "birthday"
    t.string   "birthday_date"
    t.string   "sex"
    t.string   "relationship_status"
    t.integer  "significant_other_id", :limit => 8
    t.string   "political"
    t.string   "activities"
    t.string   "interests"
    t.boolean  "is_app_user"
    t.string   "music"
    t.string   "tv"
    t.string   "movies"
    t.string   "books"
    t.string   "quotes"
    t.string   "about_me"
    t.integer  "notes_count",          :limit => 8
    t.integer  "wall_count",           :limit => 8
    t.string   "status"
    t.string   "online_presence"
    t.string   "locale"
    t.string   "proxied_email"
    t.string   "profile_url"
    t.string   "pic_small_with_logo"
    t.string   "pic_big_with_logo"
    t.string   "pic_square_with_logo"
    t.string   "pic_with_logo"
    t.string   "allowed_restrictions"
    t.string   "verified"
    t.string   "profile_blurb"
    t.string   "username"
    t.string   "website"
    t.boolean  "is_blocked"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "facebook_profiles", ["customer_id"], :name => "customer_id_unique_key", :unique => true
  add_index "facebook_profiles", ["uid"], :name => "facebook_profiles_uid"
  add_index "facebook_profiles", ["uid"], :name => "uid_unique_key", :unique => true

  create_table "families", :force => true do |t|
    t.text     "name",        :limit => 255,                    :null => false
    t.boolean  "ignored",                    :default => false, :null => false
    t.text     "description"
    t.boolean  "install_jar",                :default => false, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "imuz_db_answers", :force => true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "imuz_db_game_sessions", :force => true do |t|
    t.integer  "level",                    :default => 1
    t.integer  "hits",                     :default => 0
    t.integer  "misses",                   :default => 0
    t.integer  "skips",                    :default => 0
    t.integer  "max_questions",            :default => 10
    t.boolean  "started",                  :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "imuz_db_question_id"
    t.integer  "last_question_identifier"
    t.integer  "customer_id"
    t.boolean  "audio",                    :default => true
  end

  add_index "imuz_db_game_sessions", ["customer_id"], :name => "customer_id"
  add_index "imuz_db_game_sessions", ["imuz_db_question_id"], :name => "imuz_db_question_id"

  create_table "imuz_db_questions", :force => true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "imuz_db_answer_id"
    t.string   "artist_name"
    t.string   "artist_url"
    t.string   "artist_photo"
    t.string   "album_name"
    t.string   "album_url"
    t.string   "youtube"
    t.string   "album_cover"
    t.string   "album_asin"
  end

  add_index "imuz_db_questions", ["imuz_db_answer_id"], :name => "imuz_db_answer_id"

  create_table "integrators", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "integrators_operators", :id => false, :force => true do |t|
    t.integer "integrator_id", :null => false
    t.integer "operator_id",   :null => false
  end

  add_index "integrators_operators", ["integrator_id", "operator_id"], :name => "index_integrators_operators_on_integrator_id_and_operator_id"
  add_index "integrators_operators", ["operator_id"], :name => "index_integrators_operators_on_operator_id"

  create_table "languages", :force => true do |t|
    t.string   "name",            :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "byte_identifier"
  end

  add_index "languages", ["name"], :name => "languages_name"

  create_table "midlet_reports", :force => true do |t|
    t.integer  "status_code",    :limit => 2
    t.integer  "access_id"
    t.integer  "download_id"
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "app_version_id"
  end

  add_index "midlet_reports", ["access_id"], :name => "index_midlet_reports_on_access_id"
  add_index "midlet_reports", ["app_id"], :name => "index_midlet_reports_on_app_id"
  add_index "midlet_reports", ["app_version_id"], :name => "app_version_id"
  add_index "midlet_reports", ["download_id", "app_id"], :name => "index_midlet_reports_on_download_id_and_app_id"
  add_index "midlet_reports", ["download_id"], :name => "index_download_id"

  create_table "midp_versions", :force => true do |t|
    t.text     "version",     :limit => 255, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "midp_versions", ["version"], :name => "midp_versions_version", :length => {"version"=>40}

  create_table "news_category_translations", :force => true do |t|
    t.integer  "news_feed_category_id",                :null => false
    t.integer  "language_id",                          :null => false
    t.string   "description",           :limit => 512
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
  end

  add_index "news_category_translations", ["language_id"], :name => "language_id"
  add_index "news_category_translations", ["news_feed_category_id"], :name => "news_feed_category_id"
  add_index "news_category_translations", ["title"], :name => "news_category_translations_title", :length => {"title"=>40}

  create_table "news_feed_categories", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",     :default => true
  end

  create_table "news_feed_translations", :force => true do |t|
    t.integer  "news_feed_id", :null => false
    t.text     "content",      :null => false
    t.text     "title",        :null => false
    t.text     "url"
    t.integer  "language_id",  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "news_feed_translations", ["language_id"], :name => "language_id"
  add_index "news_feed_translations", ["news_feed_id"], :name => "news_feed_id"

  create_table "news_feeds", :force => true do |t|
    t.integer  "news_feed_category_id",                   :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",                :default => true
  end

  add_index "news_feeds", ["news_feed_category_id"], :name => "index_news_feeds_on_news_feed_category_id"

  create_table "operators", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "optional_apis", :force => true do |t|
    t.text     "name",        :limit => 255, :null => false
    t.text     "description"
    t.integer  "jsr_index",   :limit => 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "optional_apis", ["jsr_index"], :name => "index_optional_apis_on_jsr_index"

  create_table "partners", :force => true do |t|
    t.text     "name",       :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ranking_entries", :force => true do |t|
    t.integer  "customer_id",                                 :null => false
    t.integer  "app_version_id",                              :null => false
    t.integer  "device_id"
    t.integer  "score",           :limit => 8, :default => 0, :null => false
    t.integer  "sub_type",        :limit => 2, :default => 0, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "record_time"
    t.integer  "ranking_type_id"
  end

  add_index "ranking_entries", ["app_version_id"], :name => "index_ranking_entries_on_app_version_id"
  add_index "ranking_entries", ["customer_id", "app_version_id", "device_id"], :name => "by_customer_id_appver_id_and_dev_id"
  add_index "ranking_entries", ["customer_id", "app_version_id"], :name => "index_ranking_entries_on_customer_id_and_app_version_id"
  add_index "ranking_entries", ["device_id"], :name => "index_ranking_entries_on_device_id"
  add_index "ranking_entries", ["ranking_type_id"], :name => "ranking_type_id"
  add_index "ranking_entries", ["score"], :name => "index_ranking_entries_on_score"
  add_index "ranking_entries", ["sub_type"], :name => "index_ranking_entries_on_sub_type"

  create_table "ranking_systems", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ranking_types", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "unit_format_id"
    t.integer  "sub_type",          :default => 0, :null => false
    t.integer  "minimum"
    t.integer  "maximum"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ranking_system_id"
  end

  add_index "ranking_types", ["ranking_system_id"], :name => "ranking_system_id"
  add_index "ranking_types", ["unit_format_id"], :name => "unit_format_id"

  create_table "resource_file_types", :force => true do |t|
    t.string   "name",                    :null => false
    t.integer  "client_id",  :limit => 2
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "resource_files", :force => true do |t|
    t.integer  "resource_id",                                       :null => false
    t.integer  "resource_file_type_id",                             :null => false
    t.integer  "internal_id",           :limit => 3, :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  add_index "resource_files", ["resource_file_type_id"], :name => "resource_file_type_id"
  add_index "resource_files", ["resource_id"], :name => "resource_id"

  create_table "resource_type_elements", :force => true do |t|
    t.integer  "resource_type_id",                     :null => false
    t.integer  "resource_file_type_id",                :null => false
    t.integer  "quantity",              :default => 1, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "resource_type_elements", ["resource_file_type_id"], :name => "resource_file_type_id"
  add_index "resource_type_elements", ["resource_type_id", "resource_file_type_id"], :name => "type_elements_index", :unique => true

  create_table "resource_types", :force => true do |t|
    t.string   "title",                    :null => false
    t.text     "description",              :null => false
    t.integer  "client_id",   :limit => 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "resources", :force => true do |t|
    t.integer  "resource_type_id",                             :null => false
    t.text     "description"
    t.integer  "internal_id",      :limit => 3, :default => 0
    t.string   "title",                                        :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "resources", ["resource_type_id"], :name => "resource_type_id"

  create_table "submissions", :force => true do |t|
    t.integer  "integrator_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "submissions", ["integrator_id"], :name => "index_submissions_on_integrator_id"

  create_table "undetected_user_agents", :force => true do |t|
    t.text     "user_agent",                              :null => false
    t.integer  "accesses",    :limit => 2, :default => 1, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "wap_profile"
  end

  add_index "undetected_user_agents", ["user_agent"], :name => "undetected_user_agents_user_agent", :length => {"user_agent"=>255}

  create_table "unit_formats", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_roles", :force => true do |t|
    t.text     "name",       :limit => 255
    t.text     "layout",     :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_roles", ["name"], :name => "user_roles_name", :length => {"name"=>30}

  create_table "users", :force => true do |t|
    t.string   "nickname"
    t.string   "hashed_password"
    t.string   "salt"
    t.integer  "partner_id"
    t.integer  "user_role_id",                   :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "email",           :limit => 255, :null => false
    t.text     "first_name",      :limit => 255
    t.text     "last_name",       :limit => 255
  end

  add_index "users", ["email", "nickname"], :name => "users_email_nickname", :length => {"email"=>255, "nickname"=>nil}
  add_index "users", ["nickname"], :name => "users_nickname"
  add_index "users", ["partner_id"], :name => "partner_id"
  add_index "users", ["user_role_id"], :name => "user_role_id"

  create_table "vendors", :force => true do |t|
    t.text     "name",        :limit => 255, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ua_string",   :limit => 200
    t.string   "other_names"
    t.boolean  "active"
  end

  add_index "vendors", ["name"], :name => "vendors_name", :length => {"name"=>40}

  create_table "widget_users", :force => true do |t|
    t.string   "access_key"
    t.string   "fb_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "fb_access_token"
  end

end
