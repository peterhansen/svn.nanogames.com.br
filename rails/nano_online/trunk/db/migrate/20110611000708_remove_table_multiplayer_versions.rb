class RemoveTableMultiplayerVersions < ActiveRecord::Migration
  def self.up
    drop_table :multiplayer_versions
  end

  def self.down
    create_table :multiplayer_versions do |t|
      t.integer :app_version_id

      t.timestamps
    end
  end
end
