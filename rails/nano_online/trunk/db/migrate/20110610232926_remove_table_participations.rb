class RemoveTableParticipations < ActiveRecord::Migration
  def self.up
    drop_table :participations
  end

  def self.down
    create_table :participations do |t|
      t.integer :customer_id, :null => false
      t.integer :match_id, :null => false
      t.integer :order
      t.integer :pontuation, :limit => 9
      t.string  :player_data

      t.timestamps
    end
  end
end