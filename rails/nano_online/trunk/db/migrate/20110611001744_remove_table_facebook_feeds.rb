class RemoveTableFacebookFeeds < ActiveRecord::Migration
  def self.up
    begin remove_index( :facebook_feeds, APPID_FEEDTYPE_UNIQUE_INDEX_NAME ) rescue true end
    drop_table :facebook_feeds;
  end

  def self.down
    create_table :facebook_feeds do |t|
      t.integer :app_id,                    :null => false;
      t.integer :feed_type,                 :null => false, :limit => 2;
      t.string  :title,                     :null => false;
      t.text    :title_href,                :null => true,  :limit => 1024;
      t.string  :caption,                   :null => false;
      t.text    :description,               :null => true;
      t.string  :media_type,                :null => false, :limit => 5,    :default => "image";
      t.text    :media_src,                 :null => false, :limit => 1024;
      t.text    :media_href,                :null => false, :limit => 1024;
      t.string  :action_link_text,          :null => true,  :limit => 25;
      t.text    :action_link_href,          :null => true,  :limit => 1024;
      t.text    :caption_format_code,       :null => true;
      t.text    :description_format_code,   :null => true;
      t.string  :achievement_description,   :null => false, :limit => 512;
      t.timestamps;
    end

    add_index( :facebook_feeds, [:app_id, :feed_type], { :unique => true, :name => APPID_FEEDTYPE_UNIQUE_INDEX_NAME } );
  end
end
