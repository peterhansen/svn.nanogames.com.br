class RemoveTableFacebookDatas < ActiveRecord::Migration
  def self.up
    begin
      remove_index( :facebook_datas, FB_CANVAS_NAME_INDEX );
      remove_index( :facebook_datas, FB_API_KEY_INDEX );
      remove_index( :facebook_datas, FB_APP_ID_INDEX );
      remove_index( :facebook_datas, APP_ID_INDEX );
    rescue
      true
    end

    drop_table :facebook_datas
  end

  def self.down
    create_table :facebook_datas do |t|
      t.integer :app_id, :null => false
      t.integer :facebook_app_id, :limit => 8, :null => false, :references => nil
      t.string :api_key, :null => false
      t.string :api_secret, :null => false
      t.string :canvas_name, :null => false
      t.text :display_name, :limit => 256, :null => false
      t.timestamps
    end

    add_index( :facebook_datas, :app_id, { :name => APP_ID_INDEX, :unique => true } );
    add_index( :facebook_datas, :facebook_app_id, { :name => FB_APP_ID_INDEX, :unique => true } );
    add_index( :facebook_datas, :api_key, { :name => FB_API_KEY_INDEX, :unique => true } );
    add_index( :facebook_datas, :canvas_name, { :name => FB_CANVAS_NAME_INDEX, :unique => true } );
  end
end
