class CreateApps < ActiveRecord::Migration
  def self.up
    create_table :apps do |t|
      t.column :name,  :text, :limit => 50, :null => false
      t.column :name_short,  :text, :limit => 4, :null => false
      t.column :description_short, :text, :limit => 150
      t.column :description_medium, :text, :limit => 300
      t.column :description_long, :text, :limit => 900
      
      t.timestamps
    end
    
  end

  def self.down
    begin drop_table :apps rescue true end
  end
end
