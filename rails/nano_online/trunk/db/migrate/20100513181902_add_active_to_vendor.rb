class AddActiveToVendor < ActiveRecord::Migration
  def self.up
    add_column :vendors, :active, :boolean
  end

  def self.down
    remove_column :vendors, :active
  end
end
