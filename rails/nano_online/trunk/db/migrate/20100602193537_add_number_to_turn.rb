class AddNumberToTurn < ActiveRecord::Migration
  def self.up
    add_column :turns, :number, :integer
  end

  def self.down
    remove_column :turns, :number
  end
end
