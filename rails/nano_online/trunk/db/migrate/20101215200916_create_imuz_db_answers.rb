class CreateImuzDbAnswers < ActiveRecord::Migration
  def self.up
    create_table :imuz_db_answers do |t|
      t.text :text

      t.timestamps
    end
  end

  def self.down
    drop_table :imuz_db_answers
  end
end
