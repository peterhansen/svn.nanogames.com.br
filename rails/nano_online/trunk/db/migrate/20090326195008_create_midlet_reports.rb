class CreateMidletReports < ActiveRecord::Migration
  def self.up
    create_table :download_codes do |t|
      # o código reduz as chances de hack por adivinhação de código de download (o limite de 40 é dado pelo número
      # de caracteres do hash SHA1)
      t.column :code, :text, :limit => 40, :null => false
      t.column :file, :text, :null => false
      t.column :used_at, :datetime
      t.column :download_id, :integer, :null => false

      t.timestamps
    end

    
    create_table :midlet_reports do |t|
      # código de retorno informado pelo MIDlet (poss�veis valores descritos na classe MIDletReport)
      t.column :status_code, :integer, :limit => 2
      t.column :access_id, :integer, :null => false
      # id do código de download - pode ser nulo, no caso de aplicativos não baixados pela Nano (integrado por terceiros,
      # sem criação de arquivos .jad espec�ficos para cada download)
      t.column :download_id, :integer
      # identificação do aplicativo (no caso de jogos integrados por terceiros)
      t.column :app_id, :integer

      t.timestamps
    end

#    add_index :download_codes, [ :code ] TODO índices de texto
    add_index :download_codes, [ :download_id ]
#    add_index :midlet_reports, [ :user_agent ]
    add_index :midlet_reports, [ :download_id, :app_id ]
    add_index :midlet_reports, [ :app_id ]
    add_index :midlet_reports, [ :access_id ]
  end

  
  def self.down
#    remove_index :midlet_reports, [ :access_id ]
#    remove_index :midlet_reports, [ :app_id ]
#    remove_index :midlet_reports, [ :download_id, :app_id ]
#    remove_index :download_codes, [ :download_id ]

    begin drop_table :download_codes rescue true end
    begin drop_table :midlet_reports rescue true end
  end
end
