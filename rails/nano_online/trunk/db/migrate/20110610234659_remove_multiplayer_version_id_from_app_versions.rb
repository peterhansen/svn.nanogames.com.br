class RemoveMultiplayerVersionIdFromAppVersions < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE app_versions DROP FOREIGN KEY app_versions_ibfk_2"
    remove_column :app_versions, :multiplayer_version_id
  end

  def self.down
    add_column :app_versions, :multiplayer_version_id, :integer
  end
end
