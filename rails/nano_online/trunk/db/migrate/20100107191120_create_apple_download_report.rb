class CreateAppleDownloadReport < ActiveRecord::Migration
  def self.up
    create_table :apple_download_reports, :force => true do |t|
      t.text        :name,  :limit => 60
      t.timestamps
    end
  end

  def self.down
    begin drop_table :apple_download_reports rescue true end
  end
end
