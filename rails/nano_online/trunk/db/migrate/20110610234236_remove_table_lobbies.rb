class RemoveTableLobbies < ActiveRecord::Migration
  def self.up
    drop_table :lobbies
  end

  def self.down
    create_table :lobbies do |t|
      t.integer :multiplayer_version_id

      t.timestamps
    end
  end
end
