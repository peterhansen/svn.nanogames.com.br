class CreateUsers < ActiveRecord::Migration
  def self.up
    # parceiros 
    create_table :partners do |t|
      t.column :name, :text, :limit => 30, :null => false
      t.timestamps
    end

    # relacionamento parceiros x aplicativos
    create_table :apps_partners, :id => false do |t|
      t.column :app_id, :integer, :null => false
      t.column :partner_id, :integer, :null => false
    end


    # tabela de tipos de usuários
    create_table :user_roles do |t|
      t.column :name, :text, :limit => 30
      t.column :layout, :text, :limit => 20

      t.timestamps
    end

    # tabela de usuários em si
    create_table :users do |t|
      t.column :name, :string
      t.column :hashed_password, :string
      t.column :salt, :string
      t.column :partner_id, :integer
      t.column :user_role_id, :integer, :null => false

      t.timestamps
    end

    # índices
#    add_index :partners, [ :name ]
    add_index :apps_partners, [ :partner_id, :app_id ]
    add_index :apps_partners, [ :app_id ]
#    add_index :user_roles, [ :name ]
  end

  
  def self.down
#    remove_index :user_roles, [ :name ]
    begin remove_index :apps_partners, [ :app_id ] rescue true end
    begin remove_index :apps_partners, [ :partner_id, :app_id ] rescue true end
#    remove_index :partners, [ :name ]

    begin remove_foreign_key :users, :users_ibfk_1 rescue true end
    begin remove_foreign_key :users, :users_ibfk_2 rescue true end
    begin remove_foreign_key :apps_partners, :apps_partners_ibfk_2 rescue true end

    begin drop_table :users rescue true end
    begin drop_table :user_roles rescue true end
    begin drop_table :apps_partners rescue true end
    begin drop_table :partners rescue true end
  end
end
