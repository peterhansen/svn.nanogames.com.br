class RemoveTableTurns < ActiveRecord::Migration
  def self.up
    drop_table :turns
  end

  def self.down
    create_table :turns do |t|
      t.integer :match_id

      t.timestamps
    end
  end
end