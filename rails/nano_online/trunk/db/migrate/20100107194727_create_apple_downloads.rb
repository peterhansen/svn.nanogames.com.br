class CreateAppleDownloads < ActiveRecord::Migration
  def self.up
    create_table :apple_downloads do |t|
      t.integer :apple_download_report_id,        :null => false                    # chave-estrangeira
      t.text    :service_provider_code,                          :limit => 50
      t.text    :service_provider_country_code,                  :limit => 50
      t.text    :upc,                                            :limit => 20
      t.text    :isrc,                                           :limit => 20
      t.text    :artist_show,                                    :limit => 1024
      t.integer :app_id,                          :null => false                    # chave-estrangeira
      t.text    :label_studio_network,                           :limit => 1024
      t.integer :apple_product_category_id,           :null => false                    # chave-estrangeira
      t.integer :units,                           :null => false
      t.decimal :royalty_price,                   :null => false,                   :precision => 5,  :scale => 2
      t.date    :begin_date
      t.date    :end_date
      t.integer :customer_currency,               :null => false                    # chave-estrangeira
      t.integer :country_id,                      :null => false                    # chave-estrangeira
      t.integer :royalty_currency,                :null => false                    # chave-estrangeira
      t.boolean :preorder,                                                          :default => false
      t.text    :season_pass,                                     :limit => 20
      t.text    :isan,                                            :limit => 40
      t.decimal :customer_price,                  :null => false,                   :precision => 5,  :scale => 2
      t.text    :cma,                                             :limit => 10
      t.text    :asset_content_flavor,                            :limit => 20
      t.text    :vendor_offer_code,                               :limit => 256
      t.text    :grid,                                            :limit => 18
      t.text    :promo_code,                                      :limit => 10
      t.text    :parent_identifier,                               :limit => 100
      
      t.timestamps
    end
      
    execute <<-SQL
      ALTER TABLE apple_downloads
        ADD CONSTRAINT fk_customer_currency
        FOREIGN KEY (customer_currency)
        REFERENCES currencies(id)
    SQL
    
    execute <<-SQL
      ALTER TABLE apple_downloads
        ADD CONSTRAINT fk_royalty_currency
        FOREIGN KEY (royalty_currency)
        REFERENCES currencies(id)
    SQL
  end

  def self.down
    execute "ALTER TABLE apple_downloads DROP FOREIGN KEY fk_customer_currency"
    execute "ALTER TABLE apple_downloads DROP FOREIGN KEY fk_royalty_currency"
    begin drop_table :apple_downloads rescue true end
  end
end
