class ChangeAppleDownloadsColumns < ActiveRecord::Migration
  def self.up
    rename_column :apple_downloads, :service_provider_code, :provider
    rename_column :apple_downloads, :service_provider_country_code, :provider_country
    rename_column :apple_downloads, :artist_show, :developer
    rename_column :apple_downloads, :royalty_price, :developer_proceeds
    execute %{alter table apple_downloads drop foreign key fk_royalty_currency}
    rename_column :apple_downloads, :royalty_currency, :proceeds_currency_id
    execute %{alter table apple_downloads drop foreign key fk_customer_currency}
    rename_column :apple_downloads, :customer_currency, :customer_currency_id
    add_column :apple_downloads, :sku, :string
    remove_column :apple_downloads, :upc
    remove_column :apple_downloads, :isrc
    remove_column :apple_downloads, :label_studio_network
    remove_column :apple_downloads, :preorder
    remove_column :apple_downloads, :season_pass
    remove_column :apple_downloads, :isan
    remove_column :apple_downloads, :cma
    remove_column :apple_downloads, :asset_content_flavor
    remove_column :apple_downloads, :vendor_offer_code
    remove_column :apple_downloads, :grid
    remove_column :apple_downloads, :promo_code
    remove_column :apple_downloads, :parent_identifier
  end

  def self.down
    add_column :apple_downloads, :parent_identifier, :string
    add_column :apple_downloads, :promo_code, :string
    add_column :apple_downloads, :grid, :string
    add_column :apple_downloads, :vendor_offer_code, :string
    add_column :apple_downloads, :asset_content_flavor, :string
    add_column :apple_downloads, :cma, :string
    add_column :apple_downloads, :isan, :string
    add_column :apple_downloads, :season_pass, :string
    add_column :apple_downloads, :preorder, :boolean
    add_column :apple_downloads, :label_studio_network, :string
    add_column :apple_downloads, :isrc, :string
    add_column :apple_downloads, :upc, :string
    remove_column :apple_downloads, :sku
    rename_column :apple_downloads, :customer_currency_id, :customer_currency
    execute %{alter table apple_downloads add constraint fk_customer_currency
      foreign key (customer_currency) references currencies(id)}
    rename_column :apple_downloads, :proceeds_currency_id, :royalty_currency
    execute %{alter table apple_downloads add constraint fk_royalty_currency
      foreign key (royalty_currency) references currencies(id)}
    rename_column :apple_downloads, :developer_proceeds, :royalty_price
    rename_column :apple_downloads, :developer, :artist_show
    rename_column :apple_downloads, :provider_country, :service_provider_country_code
    rename_column :apple_downloads, :provider, :service_provider_code
  end
end
