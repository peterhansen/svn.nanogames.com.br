class AddWapProfileToUndetectedUserAgents < ActiveRecord::Migration
  def self.up
    add_column :undetected_user_agents, :wap_profile, :string
  end

  def self.down
    remove_column :undetected_user_agents, :wap_profile
  end
end
