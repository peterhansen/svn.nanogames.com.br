class CreateOperators < ActiveRecord::Migration
  def self.up
    create_table :operators do |t|
      t.column :name, :text, :limit => 20, :null => false
      t.timestamps
    end
    
    #cria as tabelas de relacionamento N-N
    create_table :bands_operators, :id => false do |t|
      t.column :operator_id, :integer, :null => false
      t.column :band_id, :integer, :null => false
    end
  end

  def self.down
    begin remove_foreign_key :bands_operators, :bands_operators_ibfk_1 rescue true end
    
    begin drop_table :bands_operators rescue true end
    begin drop_table :operators rescue true end
  end
end
