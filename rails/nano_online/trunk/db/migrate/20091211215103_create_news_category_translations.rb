class CreateNewsCategoryTranslations < ActiveRecord::Migration
  def self.up
    create_table :news_category_translations do |t|
      t.integer :news_feed_category_id,  :null => false
      t.integer :language_id,       :null => false 
      t.string  :name,              :limit => 256, :null => false
      t.string  :description,       :limit => 512
                       
      t.timestamps
    end
  end

  def self.down
    begin drop_table :news_category_translations rescue true end
  end
end
