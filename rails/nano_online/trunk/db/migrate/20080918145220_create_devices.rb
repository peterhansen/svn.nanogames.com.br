class CreateDevices < ActiveRecord::Migration
  def self.up

    #cria as tabelas referenciadas pelas chaves estrangeiras
    create_table :midp_versions do |t|
      t.column :version, :text, :limit => 5, :null => false
      t.column :description, :text, :limit => 100

      t.timestamps
    end

    create_table :cldc_versions do |t|
      t.column :version, :text, :limit => 5, :null => false
      t.column :description, :text, :limit => 100

      t.timestamps
    end

    create_table :vendors do |t|
      t.column :name, :text, :limit => 20, :null => false
      t.timestamps
    end

    create_table :families do |t|
      t.column :name, :text, :limit => 30, :null => false
      t.column :ignored, :boolean, :null => false, :default => false
      t.column :description, :text, :limit => 100
      # necessário para tratar do bug dos Nokia Series 60 (instalação via jad falha)
      t.column :install_jar, :boolean, :null => false, :default => false

      t.timestamps
    end



    #atenção: utilizar um inteiro normal (4 bytes) em todas as chaves estrangeiras, para evitar out of range
    create_table :devices do |t|
      t.column :midp_version_id,        :integer
      t.column :cldc_version_id,        :integer
      t.column :vendor_id,              :integer, :null => false
      t.column :family_id,              :integer
      t.column :commercial_name,        :text,    :limit => 20
      t.column :model,                  :text,    :limit => 20, :null => false
      t.column :screen_width,           :integer, :limit => 2
      t.column :screen_height_partial,  :integer, :limit => 2
      t.column :screen_height_full,     :integer, :limit => 2
      t.column :jar_size_max,           :integer, :limit => 2
      t.column :memory_heap,            :integer, :limit => 4
      t.column :memory_image,           :integer, :limit => 4
      t.column :user_agent,             :text,    :limit => 20
      t.column :pointer_events,         :boolean
      t.column :rms_total_size,         :integer, :limit => 4
      t.column :rms_max_record_size,    :integer, :limit => 4

      t.timestamps
    end

    #TODO adicionar índices de entradas de texto (problema com MySQL?)
#    add_index :devices, :model, :unique => true
    add_index :devices, :vendor_id
    add_index :devices, :midp_version_id
    add_index :devices, :cldc_version_id
#    add_index :devices, :user_agent, :unique =>true
    add_index :devices, :family_id


  end

  def self.down
    #remove os índices
#    remove_index :devices, :model
    begin remove_index :devices, :vendor_id rescue true end
    begin remove_index :devices, :midp_version_id rescue true end
    begin remove_index :devices, :cldc_version_id rescue true end
#    remove_index :devices, :user_agent
    begin remove_index :devices, :family_id rescue true end

    #apaga as tabelas referenciadas pelas chaves estrangeiras
    begin drop_table :families rescue true end
    begin drop_table :vendors rescue true end
    begin drop_table :cldc_versions rescue true end
    begin drop_table :midp_versions rescue true end

    begin drop_table :devices rescue true end
  end
end
