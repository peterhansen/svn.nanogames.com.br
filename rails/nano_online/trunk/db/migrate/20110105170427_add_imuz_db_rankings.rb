class AddImuzDbRankings < ActiveRecord::Migration
  #unless @imuz = App.find_by_name_short('IMDB')
 #   @imuz = App.create!(:name => "ImuzDB Quiz", :name_short => "IMDB")
 #   @imuz.app_versions.build(:number => '1.0')
  #  @imuz.save
  #end

  def self.up
=begin
    UnitFormat.create(:name => "Unidade") if UnitFormat.count == 0
    RankingSystem.create(:name => "cumulative_decrescent") unless RankingSystem.find_by_name("cumulative_decrescent")
    type = RankingType.create!( :name => 'imuz_db_cumulative',
                                :description => 'Ranking cumulativo de respostas corretas',
                                :unit_format_id => UnitFormat.first.id,
                                :sub_type => 0,
                                :minimum => 0,
                                :maximum => 999_999_999,
                                :ranking_system_id => RankingSystem.find_by_name("cumulative_decrescent").id )

    @imuz.app_versions.first.ranking_types << type
    @imuz.app_versions.first.save
=end
  end

  def self.down
    @imuz.app_versions.first.ranking_types = []
    @imuz.app_versions.first.save
    RankingType.find_by_name('imuz_db_cumulative').destroy
  end
end
