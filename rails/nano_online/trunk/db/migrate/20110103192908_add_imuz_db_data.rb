class AddImuzDbData < ActiveRecord::Migration
  def self.up
    app = App.create(:name => "iMuzDB Quiz", :name_short => "IMDB")
    AppVersion.create(:app => app, :number => "1.0", :app_version_state => AppVersionState.first(:conditions => { :name => "Available" })) if app
  end

  def self.down
    if app = App.find_by_name_short("IMDB")
      if version = AppVersion.first(:conditions => {:app_id => app.id, :number => "1.0"})
        AppVersion.delete( version )
      end

      begin
        App.delete(app.id)
      rescue
        puts "\nNao foi possivel deletar o Aplicativo, pois ele possui app_versions.\n"
      end
    end
  end
end
