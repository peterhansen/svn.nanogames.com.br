class CreateAdPieceVersions < ActiveRecord::Migration
  def self.up
    create_table :ad_piece_versions do |t|
      t.integer :ad_piece_id, :null => false
      t.integer :ad_channel_version_id, :null => false
			t.string  :title, :null => false

      t.timestamps
    end

    # relacionamento NxN
    create_table :ad_piece_versions_resources, :id => false do |t|
      t.integer   :ad_piece_version_id, :null => false
      t.integer   :resource_id, :null => false
    end
    add_index :ad_piece_versions_resources, [ :ad_piece_version_id, :resource_id ], :name => 'by_ad_piece_version_and_resource'
    add_index :ad_piece_versions_resources, :resource_id
    
  end

  def self.down
    begin drop_table :ad_piece_versions rescue true end
    begin drop_table :ad_piece_versions_resources rescue true end
  end
end
