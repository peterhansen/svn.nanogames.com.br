class AddMultiplayerVersionIdToAppVersions < ActiveRecord::Migration
  def self.up
    add_column :app_versions, :multiplayer_version_id, :integer
  end

  def self.down
    remove_column :app_versions, :multiplayer_version_id
  end
end
