class AddCustomerIdToDownload < ActiveRecord::Migration
  def self.up
    add_column :downloads, :customer_id, :integer
  end

  def self.down
    remove_column :downloads, :customer_id
  end
end
