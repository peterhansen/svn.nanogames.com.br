class CreateAds < ActiveRecord::Migration
  def self.up
    create_table :ads do |t|
      t.integer   :ad_piece_id, :null => false
      t.datetime  :expires_at

      t.timestamps
    end
  end

  def self.down
#    drop_table :ads
  end
end
