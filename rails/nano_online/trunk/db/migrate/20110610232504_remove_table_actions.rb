class RemoveTableActions < ActiveRecord::Migration
  def self.up
    drop_table :actions
  end

  def self.down
    create_table :actions do |t|
      t.integer :customer_id
      t.integer :turn_id
      t.string :move

      t.timestamps
    end
  end
end