class CreateCustomersLobbies < ActiveRecord::Migration
  def self.up
    create_table :customers_lobbies do |t|
      t.integer :customer_id
      t.integer :lobby_id
    end
  end

  def self.down
    drop_table :table    
  end
end
