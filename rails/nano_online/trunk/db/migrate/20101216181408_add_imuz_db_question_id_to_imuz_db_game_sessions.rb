class AddImuzDbQuestionIdToImuzDbGameSessions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_game_sessions, :imuz_db_question_id, :integer
  end

  def self.down
    remove_column :imuz_db_game_sessions, :imuz_db_question_id
  end
end
