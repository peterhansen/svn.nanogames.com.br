class CreateOptionalApis < ActiveRecord::Migration
  def self.up
    create_table :optional_apis do |t|
      t.column :name,  :text, :limit => 30, :null => false
      t.column :description,   :text, :limit => 100
      t.column :jsr_index,   :integer, :limit => 2
      
      t.timestamps
    end
    
    add_index :optional_apis, :jsr_index
    
    create_table :devices_optional_apis, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :optional_api_id, :integer, :null => false
    end
    
    add_index :devices_optional_apis, [ :device_id, :optional_api_id ]
    add_index :devices_optional_apis, :optional_api_id
  end

  def self.down
    begin remove_index :devices_optional_apis, :optional_api_id rescue true end
    begin remove_index :devices_optional_apis, [ :device_id, :optional_api_id ] rescue true end

    begin remove_index :optional_apis, :jsr_index rescue true end
    
    begin remove_foreign_key :devices_optional_apis, :devices_optional_apis_ibfk_2 rescue true end

    begin drop_table :devices_optional_apis rescue true end
    begin drop_table :optional_apis rescue true end
  end
end
