class CreateAdCampaignsAdPieces < ActiveRecord::Migration
  def self.up
    create_table :ad_campaigns_ad_pieces, :id => false do |t|
      t.integer :ad_campaign_id, :null => false
      t.integer :ad_piece_id, :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :ad_campaigns_ad_pieces
  end
end
