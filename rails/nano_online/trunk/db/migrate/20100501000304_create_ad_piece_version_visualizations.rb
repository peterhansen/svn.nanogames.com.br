class CreateAdPieceVersionVisualizations < ActiveRecord::Migration
  def self.up
    create_table :ad_piece_version_visualizations do |t|
      t.integer :ad_piece_version_id, :null => false
      t.integer :customer_id
			t.integer :device_id
      t.datetime :time_start, :null => false
      t.datetime :time_end

      t.timestamps
    end

		# TODO adicionar índices
#		add_index( :ad_piece_version_visualizations, [ :ad_piece_version_id, :customer_id, :device_id, :time_start, :time_end ] )
#		add_index( :ad_piece_version_visualizations, [ :ad_piece_version_id, :customer_id, :device_id, :time_start ] )
#		add_index( :ad_piece_version_visualizations, [ :ad_piece_version_id, :customer_id, :device_id ] )
#		add_index( :ad_piece_version_visualizations, [ :ad_piece_version_id, :customer_id ] )
#		add_index( :ad_piece_version_visualizations, [ :ad_piece_version_id ] )
  end

  def self.down
		# TODO remove_index dos ad_piece_visualizations
		drop_table :ad_piece_version_visualizations
  end
end
