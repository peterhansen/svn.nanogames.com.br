class CreateResources < ActiveRecord::Migration
  def self.up
    create_table :resources do |t|
      t.integer :resource_type_id, :null => false
      t.text    :description
			t.integer :internal_id, :null => true, :default => 0, :references => nil, :limit => 3
			t.string  :title, :null => false

      t.timestamps
    end
  end

  def self.down
    begin drop_table :resources rescue true end
  end
end
