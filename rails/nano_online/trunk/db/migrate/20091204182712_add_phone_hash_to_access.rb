class AddPhoneHashToAccess < ActiveRecord::Migration
  def self.up
    add_column :accesses, :phone_hash, :string
  end

  def self.down
    remove_column :accesses, :phone_hash
  end
end
