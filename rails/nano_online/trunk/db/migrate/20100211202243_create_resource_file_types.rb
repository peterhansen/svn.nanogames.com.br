class CreateResourceFileTypes < ActiveRecord::Migration
  def self.up
    create_table :resource_file_types do |t|
      t.string :name, :null => false
			t.integer :client_id, :limit => 2, :unique => true, :references => nil
      t.string :content

      t.timestamps
    end
  end

  def self.down
    begin drop_table :resource_file_types rescue true end
  end
end
