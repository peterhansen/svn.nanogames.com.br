class AddAudioToImuzDbGameSessions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_game_sessions, :audio, :boolean, :default => true
  end

  def self.down
    remove_column :imuz_db_game_sessions, :audio
  end
end
