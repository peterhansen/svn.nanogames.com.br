class RenameAppleDownloadsForeignKeys < ActiveRecord::Migration
  def self.up
    # rename_column :apple_downloads, :royalty_currency, :royalty_currency_id
    # rename_column :apple_downloads, :customer_currency, :customer_currency_id
  end

  def self.down
    rename_column :apple_downloads, :royalty_currency_id, :royalty_currency
    rename_column :apple_downloads, :customer_currency_id, :customer_currency
  end
end
