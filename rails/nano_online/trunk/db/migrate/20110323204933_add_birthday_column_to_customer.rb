class AddBirthdayColumnToCustomer < ActiveRecord::Migration
  def self.up
    add_column :customers, :show_birthday, :boolean
  end

  def self.down
    remove_column :customers, :show_birthday
  end
end
