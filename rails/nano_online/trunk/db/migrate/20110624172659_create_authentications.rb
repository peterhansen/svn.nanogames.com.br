class CreateAuthentications < ActiveRecord::Migration
  def self.up
  	create_table :authentications, :force => true do |t|
  		t.integer :customer_id
  		t.string :provider
      t.string :uid
      t.string :oauth_token
  		t.string :oauth_secret
  		t.timestamps
  	end

  	add_index :authentications, :customer_id, :name => 'customer_id_index'
    add_index :authentications, [:provider, :uid], :name => 'provider_uid_index'
  end

  def self.down
  	drop_table :authentications
  end
end
