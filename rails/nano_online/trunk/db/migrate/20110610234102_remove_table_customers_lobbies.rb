class RemoveTableCustomersLobbies < ActiveRecord::Migration
  def self.up
    drop_table :customers_lobbies
  end

  def self.down
    create_table :customers_lobbies, :id => false do |t|
      t.integer :customer_id
      t.integer :lobby_id
    end
  end
end
