class AddValidationHashToCustomer < ActiveRecord::Migration
  def self.up
    add_column :customers, :validation_hash, :string
  end

  def self.down
    remove_column :customers, :validation_hash
  end
end
