class FillAppVersionsStatus < ActiveRecord::Migration
  def self.up
    AppVersion.all.each do |av|
      av.update_attribute(:status, av.app_version_state.name)
    end
  end

  def self.down
    AppVersion.all.each do |av|
      av.update_attribute(:status, nil)
    end
  end
end
