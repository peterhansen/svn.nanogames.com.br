class AddEndedToTurn < ActiveRecord::Migration
  def self.up
    add_column :turns, :ended, :boolean
  end

  def self.down
    remove_column :turns, :ended
  end
end
