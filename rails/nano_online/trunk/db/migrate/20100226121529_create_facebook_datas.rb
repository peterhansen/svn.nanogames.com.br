class CreateFacebookDatas < ActiveRecord::Migration

  # Nomes dos índices criados
  APP_ID_INDEX = "app_id_unq_index";
  FB_APP_ID_INDEX = "fb_app_id_unq_index";
  FB_API_KEY_INDEX = "fb_api_key_unq_index";
  FB_CANVAS_NAME_INDEX = "fb_canvas_name_unq_index";

  def self.up

    create_table :facebook_datas do |t|

      # Aplicação Nano que está relacionada a estes dados do Facebook
      t.integer :app_id, :null => false

      # ID desta aplicação no Facebook
      t.integer :facebook_app_id, :limit => 8, :null => false, :references => nil

      # Retirado de http://wiki.developers.facebook.com/index.php/Application_(FQL): The API key of the application being queried
      t.string :api_key, :null => false
      
      # Espécie de senha da aplicação para "login" no Facebook. Devemos utilizá-la toda vez que fizermos queries na rede social
      t.string :api_secret, :null => false

      # Retirado de http://wiki.developers.facebook.com/index.php/Application_(FQL): The string appended to apps.facebook.com/ to navigate to the application's canvas page
      t.string :canvas_name, :null => false

      # Retirado de http://wiki.developers.facebook.com/index.php/Application_(FQL): The name of the application
      t.text :display_name, :limit => 256, :null => false

      # Gera as colunas created_at e updated_at
      t.timestamps
    end

    add_index( :facebook_datas, :app_id, { :name => APP_ID_INDEX, :unique => true } );
    add_index( :facebook_datas, :facebook_app_id, { :name => FB_APP_ID_INDEX, :unique => true } );
    add_index( :facebook_datas, :api_key, { :name => FB_API_KEY_INDEX, :unique => true } );
    add_index( :facebook_datas, :canvas_name, { :name => FB_CANVAS_NAME_INDEX, :unique => true } );

  end

  def self.down
    begin
      remove_index( :facebook_datas, FB_CANVAS_NAME_INDEX );
      remove_index( :facebook_datas, FB_API_KEY_INDEX );
      remove_index( :facebook_datas, FB_APP_ID_INDEX );
      remove_index( :facebook_datas, APP_ID_INDEX );
    rescue
      true
    end

    drop_table :facebook_datas

  end

end
