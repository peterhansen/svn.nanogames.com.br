class AddLastQuestionIdToImuzDbGameSessions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_game_sessions, :last_question_identifier, :integer
  end

  def self.down
    remove_column :imuz_db_game_sessions, :last_question_identifier
  end
end
