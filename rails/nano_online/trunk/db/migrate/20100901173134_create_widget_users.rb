class CreateWidgetUsers < ActiveRecord::Migration
  def self.up
    create_table :widget_users do |t|
      t.string :access_key
      t.string :facebook_key

      t.timestamps
    end
  end

  def self.down
    drop_table :widget_users
  end
end
