class CreateResourceTypeElements < ActiveRecord::Migration
  def self.up
    create_table :resource_type_elements do |t|
      t.integer :resource_type_id, :null => false
      t.integer :resource_file_type_id, :null => false
      t.integer :quantity, :null => false, :default => 1

      t.timestamps
    end
    
    add_index(:resource_type_elements, [:resource_type_id, :resource_file_type_id], :name => "type_elements_index", :unique => true)
  end

  def self.down
    # remove_index(:resource_type_elements, "type_elements_index")
    drop_table :resource_type_elements
  end
end
