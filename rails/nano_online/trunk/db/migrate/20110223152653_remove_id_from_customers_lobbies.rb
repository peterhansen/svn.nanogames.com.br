class RemoveIdFromCustomersLobbies < ActiveRecord::Migration
  def self.up
    # remove_column :customers_lobbies, :id
  end

  def self.down
    add_column :customers_lobbies, :id, :integer
  end
end
