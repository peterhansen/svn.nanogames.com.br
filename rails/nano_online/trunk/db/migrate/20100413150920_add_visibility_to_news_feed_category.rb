class AddVisibilityToNewsFeedCategory < ActiveRecord::Migration
  def self.up
    add_column :news_feed_categories, :public, :boolean, :default => true
  end

  def self.down
    remove_column :news_feed_categories, :public
  end
end
