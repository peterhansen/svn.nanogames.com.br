class AddAlbumAsinToImuzDbQuestions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_questions, :album_asin, :string
  end

  def self.down
    remove_column :imuz_db_questions, :album_asin
  end
end
