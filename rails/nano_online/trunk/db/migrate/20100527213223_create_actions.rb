class CreateActions < ActiveRecord::Migration
  def self.up
    create_table :actions do |t|
      t.integer :customer_id
      t.integer :turn_id
      t.string :move

      t.timestamps
    end
  end

  def self.down
    drop_table :actions
  end
end
