class AddAlbumCoverToImuzDbQuestions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_questions, :album_cover, :string
  end

  def self.down
    remove_column :imuz_db_questions, :album_cover
  end
end
