class AddStatusToAppVersions < ActiveRecord::Migration
  def self.up
    add_column :app_versions, :status, :string
  end

  def self.down
    remove_column :app_versions, :status
  end
end
