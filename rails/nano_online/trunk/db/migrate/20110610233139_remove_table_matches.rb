class RemoveTableMatches < ActiveRecord::Migration
  def self.up
    drop_table :matches
  end

  def self.down
    create_table :matches do |t|
      t.integer :multiplayer_version_id

      t.timestamps
    end
  end
end