namespace :populate_db do
  @fields = { :sort_order => 0, 
              :common_name => 1,
              :format_name => 2,
              :type => 3, 
              :sub_type => 4, 
              :sorvereignty => 5, 
              :capital => 6, 
              :currency_code => 7,
              :currency_name => 8,
              :phone_code => 9,
              :two_letter_code => 10,
              :three_letter_code => 11,
              :number => 12,
              :country_code_tld => 13}
            
  desc "Populates currency table with data"
  task :currencies => :environment do
    require "fastercsv"
  
    FasterCSV.foreach("#{Rails.root}/db/CSV/countrylist.csv", :row_sep => "\r\n") do |row|
      begin
        unless currency = Currency.find_by_code(row[@fields[:currency_code].to_i])
          currency = Currency.new
      
          currency.name = row[@fields[:currency_name].to_i]
          currency.code = row[@fields[:currency_code].to_i]
      
          if currency.save
            puts "#{row[@fields[:sort_order].to_i]} - #{row[@fields[:currency_code].to_i]} - #{row[@fields[:currency_name].to_i]} - Saved!"
          else
            currency.errors.each do |error|
              puts "#{row[@fields[:sort_order].to_i]} - #{row[@fields[:currency_code].to_i]} - #{row[@fields[:currency_name].to_i]} - NOT SAVED! - #{error}"
            end
          end
        end
      rescue FasterCSV::MalformedCSVError
        puts row 
      end
    end
  end
  
  desc "Populates country table with data"          
  task :countries => [:environment, :currencies] do
    require "fastercsv"
  
    FasterCSV.foreach("#{Rails.root}/db/CSV/countrylist.csv") do |row|
      country = Country.new
    
      country.name = row[@fields[:common_name].to_i]
      country.iso_name_2 = row[@fields[:two_letter_code].to_i]
      country.iso_name_3 = row[@fields[:three_letter_code].to_i]
      country.iso_number = row[@fields[:number].to_i]
      
      currency = Currency.find_by_code(row[@fields[:currency_code].to_i])
      if currency
        country.currency_id = currency.id
      end
      
      if country.save
        puts "#{row[@fields[:sort_order].to_i]} - #{row[@fields[:common_name].to_i]} - #{row[@fields[:two_letter_code].to_i]} - #{row[@fields[:three_letter_code].to_i]} - #{row[@fields[:number].to_i]} - Saved!"
      else
        error = " #{row[@fields[:sort_order].to_i]} - #{row[@fields[:common_name].to_i]} - #{row[@fields[:two_letter_code].to_i]} - #{row[@fields[:three_letter_code].to_i]} - #{row[@fields[:number].to_i]} - NOT SAVED!"
                
        if country.errors
          country.errors.each do |err|
            puts error + " - #{err}"
          end
        else
          puts error
        end
      end
    end
  end
  
  desc "Fill currency_id in countries table."
  task :fill_currency => [:environment, :currencies] do
    require "fastercsv"

    countries = Country.find(:all)

    FasterCSV.foreach("#{Rails.root}/db/CSV/countrylist.csv") do |row|
      currency = Currency.find_by_code(row[@fields[:currency_code].to_i]) #TODO dando erro no currency.id

      countries.each do |country|
        if country.iso_name_2 == row[@fields[:two_letter_code].to_i]
          country.currency_id = currency.id if currency

          if currency
            if country.save
              puts "#{row[@fields[:sort_order].to_i]} - #{row[@fields[:common_name].to_i]} - #{currency.id} - Saved!"
            else
              error = "#{row[@fields[:sort_order].to_i]} - #{row[@fields[:common_name].to_i]} - #{currency.id} - NOT SAVED!"

              if country.errors
                country.errors.each do |err|
                  puts error + " - #{err}"
                end
              else
                  puts error
              end
            end
          else
            puts "#{row[@fields[:sort_order].to_i]} - #{row[@fields[:common_name].to_i]} - NOT SAVED!"
          end
        end
      end
    end
  end
  
  desc "Fills apple_product_category table."
  task :fill_apple_product_category => :environment do
    a = [1, 7]
    a.each do |cat|
      c = AppleProductCategory.new

      c.category = cat
      c.save
    end
  end
  
  desc "Fills apple apps fields (apple_vendor_identifier, apple_title and apple_identifier) and creates extra records."
  task :fill_apple_apps => :environment do
    require "fastercsv"

    apps = App.find(:all)

    FasterCSV.foreach("#{Rails.root}/db/CSV/apple_apps.csv") do |row|
      apps.each do |app|
        if ( row )
          if ( row[0] == app.name )
            app.apple_title = row[1]
            app.apple_vendor_identifier = row[2]
            app.apple_identifier = row[3]

            if (app.save)
              puts "#{app.name} updated.\n"
              row = nil
            else
              puts "#{app.name} could not be updated.\n"
              row = nil
            end
          end
        end
      end
      if ( row )
        puts "No match for apple_title '#{row[0]}'. Creating new app...\n"

        new_app = App.new
        new_app.name = row[0]
        new_app.name_short = row[4]
        new_app.apple_title = row[1]
        new_app.apple_vendor_identifier = row[2]
        new_app.apple_identifier = row[3]

        if ( new_app.save )
          puts "New app created: #{new_app.name}.\n"
        else
          puts "Could not create new app #{new_app.name}.\n"
        end
      end
    end
  end
  
  desc "Fill byte_identifier field in languages table"
  task :fill_byte_identifier => :environment do
    lang_bytes = [['ingles', 0], ['espanhol', 1]  , ['portugues', 2], ['alemao', 3], ['italiano', 5], ['frances', 6], ['japones', 7], ['russo', 8], ['coreano', 9], ['holandes', 10]]
    
    lang_bytes.each do |l|
      lang = Language.new(:name => l[0], :byte_identifier => l[1])
      if ( lang.save )
        puts "Language \"#{l[0]}\"(#{l[1]}) saved."
      else
        puts "Language \"#{l[0]}\"(#{l[1]}) not saved."
      end
    end
  end
  
    desc "Fills the app_version_state table"
    task :fill_app_version_state => :environment do
      AppVersionState.new(:name => "Available", :code => 0).save
      AppVersionState.new(:name => "Deprecated", :code => 1).save
      AppVersionState.new(:name => "Unavailable", :code => 2).save
    end
  
  desc "Fills the app_version_state_id foreign key in app_version table"
  task :fill_app_version_state_id => [:environment, :fill_app_version_state] do
    versions = AppVersion.find(:all)
    
    versions.each do |version|
      if [0, 1, 2].include?(version.app_version_state_id) 
        version.app_version_state_id = AppVersionState.find_by_code(version.app_version_state_id).id
        if version.save
          puts "Version saved."
        else
          puts "Version could not be saved."
        end
      end
    end
  end
  
  task :ground_up => [:countries, :fill_apple_product_category, :fill_apple_apps, :fill_byte_identifier, :fill_app_version_state_id]
  task :server_up => []
end

namespace :apple_reports do
  desc "Import the Apple Download Report file"
  task :daily => :environment do
    path = "#{Rails.root}/files/apple_reports/"
    
    if Dir.entries( path ).size >= 2
      Dir.foreach( path ) do |file|
        unless file == ".DS_Store" || File.directory?(file) || AppleDownloadReport.find_by_name(file) || file == "backup"
          puts "\nFile been processed: #{file}\n"
          apple_download_report = AppleDownloadReport.new
          apple_download_report.name = file
      
          if apple_download_report.valid?
            puts "Valid file.\b"
            arq = File.open( path + file )
        
            if arq
              apple_download_report.save
          
              arq.each_line do |line|
                data = line.split("\t")
                unless data[0] == "Provider"
                  apple_download = AppleDownload.new_from_array(data)
                
                  apple_download.apple_download_report = apple_download_report
                
                  if apple_download.save
                    puts "Apple download saved."
                  else
                    apple_download.errors.each{|m| puts m}
                  end
                end
              end
              
              move(path + file, path + "backup/" + file)
            else
              puts "Error opening file #{file}"
            end
          else
            apple_download_report.errors.each {|error| puts error}
          end
        end
      end
    else
      puts "No File Found."
    end
  end
end

task :facebook => :environment do
  require 'facebooker'
end
