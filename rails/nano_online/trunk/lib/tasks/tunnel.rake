namespace :tunnel do
  desc "Start SSH tunnel"
  task :start do
    puts "Starting tunnel staging.nanogames.com.br:11524 to 0.0.0.0:3000"

    exec "ssh -nNT -g -R *:11524:0.0.0.0:3000 rails@staging.nanogames.com.br"
  end
end
