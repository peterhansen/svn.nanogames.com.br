namespace :assets do
  namespace :imuz_db do
    desc "Update ImuzDb assets"
    task :update do
      puts "Updating ImuzDb assets"

      exec "scp -r staging.nanogames.com.br:/var/www/rails/nano_online/current/public ."
    end
  end
end