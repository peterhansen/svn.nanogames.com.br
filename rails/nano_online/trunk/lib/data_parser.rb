module DataParser
  def parse_data( data, value = nil )
    begin
      if value.blank? and ActionController::Base.session[ 'nano_client' ].present?
        value = ActionController::Base.session[ 'nano_client' ]
      end

      stream = get_input_stream( data, value )

      # varre o corpo da mensagem, preenchendo a hashtable com os parâmetros encontrados,
      # até que todos os parâmetros sejam lidos (EOF) ou que o id de dados específicos
      # seja encontrado, quando a stream é então passada ao tratador adequado.
      while stream.available > 0
        id = stream.read_byte
        yield stream, id
      end
    rescue Exception => e
      logger.info e.backtrace.join("\n")
      return nil
    end
  end

  def get_input_stream( data, value = nil )
    if ActionController::Base.session[ 'nano_client' ].present?
      value = ActionController::Base.session[ 'nano_client' ]
    end

    value == 'big_endian' ? BigEndianTypes::InputStream.new( data ) : LittleEndianTypes::InputStream.new( data )
  end
end

# ActiveRecord::Base.send(:include, DataParser)
#
#class ActiveRecord::Base
#  include DataParser
#end