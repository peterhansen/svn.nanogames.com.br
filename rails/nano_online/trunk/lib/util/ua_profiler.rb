require 'rexml/document'
require 'open-uri'
require 'pp'

include REXML

module UaProfiler
  class UserAgent
    attr_reader :part1, :part2, :part3, :parts, :parentesis, :mozilla
    attr_accessor :vendor, :device_string, :device, :wap, :ua_profile, :docs

    def initialize(arg)
      if arg.kind_of?(ActionController::CgiRequest) || arg.kind_of?(ActionController::Request)
        @request = arg
        string = @request.env["HTTP_USER_AGENT"]
      else
        @request = nil
        string = arg
      end

      if string
        matches = string.match(/([^\(\s]+)\s?(\()?([^\)]+)?(\))?\s?(.+)?/) # ([^\(]+|[^\(]+)\s(\()?([^\)]+)?(\))?\s?(.+)?

        @part1 = $1
        @part2 = $3
        @part3 = $5
        @parentesis = $2

        if @part1
          if @part1.match(" ")
            @part1 = @part1.scan(/[^\s]+/)
          elsif @part1.match(/\AMozilla/)
            @mozilla = true
          end
        end

        if @parentesis
          if @parentesis == "("
            @part2 = @part2.scan(/(?=\S)[^;]+/)
          end
        end

        if @part3
          if @part3.match(" ")
            @part3 = @part3.scan(/[^\s]+/)
          end
        end

        if @mozilla && @parentesis
          device_lookout @part2
        else
          device_lookout @part1
        end
      end
    end

    def device_lookout(array)
      self.wap = "not used"
      array.each do |part|
        if self.vendor = check_for_vendor(part)
          self.device_string = part
          if self.device = check_for_device(part)
            return true
          end
          # break
        end
      end
      if @request
        self.wap = "not found"
        if @request.env["HTTP_X_WAP_PROFILE"]
          self.wap = "used"
          if wap_profile = @request.env["HTTP_X_WAP_PROFILE"].gsub(/\"/, "")
            unless self.device = UserAgent.parse_wap_profile(wap_profile)
              ua_not_detected
            end
          else
            ua_not_detected
          end
        end
      else
        ua_not_detected
      end
    end

    def self.parse_wap_profile(xml)
      # xml = "http://nds.nokia.com/uaprof/NN95_8GB-1r100.xml"
      begin
        file = open(xml)

        if doc = REXML::Document.new(file)
          device = Device.new do |dev|
            dev.model= dev.user_agent = doc.root.elements["rdf:Description/prf:component/rdf:Description[@ID='HardwarePlatform']/prf:Model"].text

            vendor = doc.root.elements["rdf:Description/prf:component/rdf:Description[@ID='HardwarePlatform']/prf:Vendor"].text

            if temp = Vendor.find_by_name(vendor)
              dev.vendor = temp
            else
              Vendor.all.each do |vend|
                if vend.other_names
                  if vend.other_names.match(vendor)
                    dev.vendor = vend
                    break
                  end
                end
              end
            end

            screen_size = doc.root.elements["rdf:Description/prf:component/rdf:Description[@ID='HardwarePlatform']/prf:ScreenSize"].text
            screen_size.match(/(\d+)x(\d+)/)
            dev.screen_width = $1
            dev.screen_height_partial = dev.screen_height_full = $2

            midp = doc.root.elements["rdf:Description/prf:component/rdf:Description[@ID='SoftwarePlatform']/prf:JavaPlatform/rdf:Bag/rdf:li[1]"].text
            midp.match(/\S?MIDP-(.+)/)
            dev.midp_version = MidpVersion.find_by_version($1)

            cldc = doc.root.elements["rdf:Description/prf:component/rdf:Description[@ID='SoftwarePlatform']/prf:JavaPlatform/rdf:Bag/rdf:li[2]"].text
            cldc.match(/\S?CLDC-(.+)/)
            dev.cldc_version = CldcVersion.find_by_version($1)
          end

          if device.save
            Emailer.deliver_send({  "subject"       => "UA Encontrado.",
                                    "message"       => "User Agent encontrado: #{device.model}",
                                    "recipients"    => "suporte@nanogames.com.br",
                                    "sender"        => "no-reply@nanogames.com.br"})
          else
            Emailer.deliver_send({  "subject"       => "Erro no UA Profiler.",
                                    "message"       => "User Agent encontrado: #{device.model}, mas nao foi possivel salvar o Device.\nErro: #{device.errors.join("; ")}",
                                    "recipients"    => "suporte@nanogames.com.br",
                                    "sender"        => "no-reply@nanogames.com.br"})
          end


          return device
        end
      rescue
        return nil
      end
    end

    def check_for_vendor(string)
      vendors = {}
      Vendor.all.collect {|dev| vendors[dev.id] = dev.ua_string if dev.ua_string}.compact

      # if string == "" then return nil end

      vendors.each_value do |ua_strings|
        ua_strings.split.each do |ua_string|
          if string.downcase.match(ua_string)
            return Vendor.find(vendors.index(ua_strings))
          end
        end
      end
      return nil
    end

    def check_for_device(arg)
      devices = self.vendor.devices

      devices.each do |device|
        unless device.user_agent.blank?
          if arg.downcase.match(device.user_agent.downcase) || device.user_agent.downcase.match(arg.downcase)
            return device
          elsif arg.match(" ")
            string = arg.gsub(" ", "")

            if string.downcase.match(device.user_agent.downcase) || device.user_agent.downcase.match(string.downcase)
              return device
            end
          end
        end
      end
      return nil
    end

    def ua_not_detected
      if @request

        if uua = UndetectedUserAgent.find_by_user_agent( @request.env["HTTP_USER_AGENT"] )
          uua.accesses += 1
        else
          uua = UndetectedUserAgent.new( :user_agent => @request.env["HTTP_USER_AGENT"] )
        end

        if wap = @request.env["HTTP_X_WAP_PROFILE"]
          uua.wap_profile = wap
        else
          uua.wap_profile = "No Wap profile detected."
        end

        uua.save!
      end
    end

    def send_mail
      dados = Hash.new

      dados["subject"] = "UA Encontrado."
      dados["message"] = "User Agent encontrado: #{@request.env["HTTP_USER_AGENT"]}"
      dados["recipients"] = "suporte@nanogames.com.br"
      dados["sender"] = "no-reply@nanogames.com.br"

      Emailer.deliver_send({"subject" => "UA Encontrado.", "message" => "User Agent encontrado: #{@request.env["HTTP_USER_AGENT"]}", "recipients" => "suporte@nanogames.com.br", "sender" => "no-reply@nanogames.com.br"})
    end

    def print
      puts "#{@part1}\n#{@part2}\n#{@part3}"
    end
  end
end

