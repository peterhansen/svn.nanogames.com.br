module ImuzDb
#  require 'nokogiri'
  
  WS_ADDRESS = "http://imuzdb.com/quiz/ws.php"
  
  class Parser
    attr_reader :doc

    def self.fetch(url)
      doc = Nokogiri::XML(url)
      correct_answer = ''

      if doc.xpath("//question")
        question_text = doc.xpath("//question/text").text
        answer_index = doc.xpath("//question/answer").first.attributes["option"].value

        info = {
          :artist_name =>   doc.xpath("//question/artist_name").text,
          :artist_url =>    doc.xpath("//question/artist_url").text,
          :artist_photo =>  doc.xpath("//question/artist_photo").text,
          :album_name =>    doc.xpath("//question/album_name").text,
          :album_url =>     doc.xpath("//question/album_url").text,
          :album_cover =>   doc.xpath("//question/album_cover").text,
          :album_asin =>    doc.xpath("//question/album_ASIN").text,
          :youtube =>       doc.xpath("//question/youtube").text
        }
        
        options = []
        remove_first = true
        
        doc.xpath("//option").each do |option|
          if remove_first and option["num"] != answer_index
            remove_first = false
          else
            answer = ImuzDb::Answer.create(:text => option.text)
            options << answer

            if option["num"] == answer_index
              correct_answer = answer
            end
          end
        end
        ImuzDb::Question.create({:text => question_text, :correct_answer => correct_answer, :answers => options}.merge(info))
      end
    end
  end
end
