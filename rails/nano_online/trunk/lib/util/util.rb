module Util
  # 'Natural order' comparison of two strings
  def self.natcmp(str1, str2, ignore_case = true)
      min = lambda { |a,b| a < b ? a : b }

      # Split the strings into digits and non-digits
      str_arrays = [str1,str2].inject(Array.new) do |arr, str|
          str = str.downcase if ignore_case
          arr << str.tr(" \t\r\n", '').split(/(\d+)/)
      end

      # Loop through all the digit parts and convert to integers if neither of them begin with a zero
      1.step(min.call(str_arrays[0].size, str_arrays[1].size)-1, 2) do |idx|
          if (str_arrays[0][idx] !~ /^0/) and (str_arrays[1][idx] !~ /^0/)
              str_arrays.each { |arr| arr[idx] = arr[idx].to_i }
          end
      end

      str_arrays[0] <=> str_arrays[1]
  end
end
