module FacebookAuth
  class Token
    def self.get(params, iphone = false)
      return false if iphone
      
      if params.has_key?(:signed_request) or params.has_key?(:code)
        parameter = params[:signed_request] ? 'signed_request' : 'code'
        decoded_hash = FacebookAuth::send("decode_#{parameter}", params[parameter.to_sym])
        
        if decoded_hash.has_key?("user_id") and decoded_hash.has_key?("oauth_token")
          return decoded_hash["user_id"], decoded_hash["oauth_token"]
        else
          raise FacebookAuth::NoUserLoggedException
        end
      else
        raise FacebookAuth::InvalidFacebookResponseException
      end
    end
  end

  def self.decode_signed_request(signed_request)
    begin
      raise Exception.new if signed_request.blank?
      
      signature, encoded_hash = signed_request.split('.')
      begin
        FacebookAuth::decode_json(encoded_hash)
      rescue
        raise FacebookAuth::InvalidSignedRequestException
      end
    rescue Exception => e
      raise FacebookAuth::InvalidSignedRequestException
    end
  end

  def self.decode_code(code)
    begin
      response = MiniFB.oauth_access_token(ImuzDb::GameSession::IMUZDB_APP_ID,
        ImuzDb::GameSession::IMUZDB_CALLBACK_PAGE,
        ImuzDb::GameSession::IMUZDB_APP_SECRET,
        code)
        
      uid = MiniFB.get(response['access_token'], 'me').id
      
      return {"user_id" => uid, "oauth_token" => response['access_token']}
    rescue
      raise FacebookAuth::InvalidCodeException
    end
  end

  def self.decode_json(encoded_hash)
    encoded_hash += '=' * (4 - encoded_hash.length.modulo(4))
    ActiveSupport::JSON.decode(Base64.decode64(encoded_hash))
  end

  class InvalidSignedRequestException < StandardError; end
  class InvalidFacebookResponseException < StandardError; end
  class NoUserLoggedException < StandardError; end
  class InvalidCodeException < StandardError; end
end