class Convert
  #índices dos campos (colunas da planilha 'Aparelhos GSM.ods')
  @@indexes = {
    :VENDOR => 0,
    :MODEL => 1,
    :MIDP => 5,
    :CLDC => 6,
    :SCREEN => 7,
    :JAR_LIMIT => 8,
    :HEAP => 9,
    :FAMILY => 10,
    :PLAYPHONE => 11,
    :DADA_NET => 12,
    :SERIE => 13,
    :USER_AGENT => 14,
    :TOUCH_SCREEN => 15,
  }

  #nomes das entradas
  @@colunas = {
    :VENDOR => 'vendor',
    :MODEL => 'model',
    :COMMERCIAL_NAME => 'commercial_name',
    :MIDP => 'midp_version',
    :CLDC => 'cldc_version',
    :SCREEN_WIDTH => 'screen_width',
    :SCREEN_HEIGHT_FULL => 'screen_height_full',
    :SCREEN_HEIGHT_PARTIAL => 'screen_height_partial',
    :JAR_LIMIT => 'jar_size_max',
    :HEAP_TOTAL => 'memory_heap',
    :HEAP_IMAGE => 'memory_image',
    :FAMILY => 'family',
    :SERIE => 'serie',
    :USER_AGENT => 'user_agent',
    :TOUCH_SCREEN => 'pointer_events',
    :FAMILY_NAME => 'name'
  }
  
  @@families = {}
  @@dada_net = []
  @@playphone = []
  
  def self.convert( filename )
    #abre o arquivo contendo os aparelhos
    File.open( filename, 'r' ) do |input|
      File.open( 'devices.yml', 'w' ) do |output|
        #prefixo utilizado para cada entrada gerada no arquivo .yml
        prefix = 'device_'
        device_index = 0

        while line = input.gets()
          output.puts( prefix + device_index.to_s() + ":\n" )

          #separa a string da linha num array de strings, onde cada uma é um campo
          fields = line.split( "\t" )

          #varre os campos lidos
          field_index = 0
          fields.each() do |f|
            case field_index
              when @@indexes[ :VENDOR ]
                add_info( output, :VENDOR, f )

              when @@indexes[ :MODEL ]
                commercial_name = f.match( /\(.*\)/ )
                if ( commercial_name )
                  #remove os par�nteses e adiciona a informação do nome comercial do aparelho
                  commercial_name = commercial_name.to_s().gsub( /[\(\)]/, '' )
                  add_info( output, :COMMERCIAL_NAME, commercial_name )
                  
                  #separa o nome comercial do modelo
                  start = f.index( '(' )
                  f = f.slice( 0, start )
                end
         
                add_info( output, :MODEL, f )

              when @@indexes[ :MIDP ]
                if ( f.length > 0 )
                  add_info( output, :MIDP, f )
                end

              when @@indexes[ :CLDC ]
                if ( f.length > 0 )
                  add_info( output, :CLDC, f )
                end

              when @@indexes[ :SCREEN ]
                #separa o tamanho de tela em 2 campos
                if ( f.length > 0 )
                  dimensions = f.split( 'x' )

                  add_info( output, :SCREEN_WIDTH, dimensions[ 0 ] )
                  add_info( output, :SCREEN_HEIGHT_PARTIAL, dimensions[ 1 ] )
                  add_info( output, :SCREEN_HEIGHT_FULL, dimensions[ 1 ] )
                end

              when @@indexes[ :JAR_LIMIT ]
                if ( f.length > 0 )
                  if ( f.index( /[0123456789]/ ) )
                    add_info( output, :JAR_LIMIT, f )
                  else
                    #considera o limite infinito
                    add_info( output, :JAR_LIMIT, '-1' )
                  end
                end

              when @@indexes[ :HEAP ]
                if ( f.length > 0 )
                  if ( f.index( /[0123456789]/ ) )
                    #procura por informação de mem�ria espec�fica para imagens
                    heap_image = f.match( /\(*\+\d*\)*/ )
                    if ( heap_image )
                      heap_image = heap_image.to_s().gsub( /[\(\)\+]/, '' )
                      add_info( output, :HEAP_IMAGE, heap_image )
                    
                      #remove a informação da mem�ria espec�fica para imagens
                      index = f.index( '(' )
                      unless ( index )
                        index = f.index( '+' )
                      end
                      
                      if ( index )
                        f = f.slice( 0, index )
                      end
                    end
                    
                    add_info( output, :HEAP_TOTAL, f )
                  else
                    #considera a mem�ria infinita
                    add_info( output, :HEAP_TOTAL, '-1' )
                  end
                end

              when @@indexes[ :FAMILY ]
                if ( f.length > 0 )
                  unless ( @@families.has_key?( f ) )
#                    @@families[ f ] = [ @@families.length.to_s, f.downcase.gsub( ' ', '_' ) ]
                    @@families[ f ] = [ f.downcase.gsub( ' ', '_' ), f ]
                  end

                  puts( "familia: #{@@families.fetch( f )[ 0 ]}" )
                  add_info( output, :FAMILY, @@families.fetch( f )[ 0 ] )
                end
                
              when @@indexes[ :PLAYPHONE ]
                if ( f.length > 0 )
                  entries = f.split( /[,\z]/ )
                  for entry in entries
                    @@playphone << [ entry, prefix + device_index.to_s ]
                  end
                end
                
              when @@indexes[ :DADA_NET ]
                if ( f.length > 0 )
                  entries = f.split( /[,\z]/ )
                  for entry in entries
                    @@dada_net << [ entry, prefix + device_index.to_s ]
                  end
                end

              when @@indexes[ :USER_AGENT ]
                if ( f.length > 0 )
                  add_info( output, :USER_AGENT, f )
                end

              when @@indexes[ :TOUCH_SCREEN ]
                if ( f.index( 's' ) || f.index( 'S' ) ) 
                  add_info( output, :USER_AGENT, 'true' )
                end

            end

            field_index += 1
          end # fim fields.each() do |f|

          #incrementa o contador de aparelhos, e adiciona uma linha em branco para separ�-lo
          device_index += 1
          output.puts()
        end
      end
    end
    
    #grava o arquivo das famílias
    File.open( 'families.yml', 'w' ) do |output|
      for family in @@families
        output.puts( "#{family[ 1 ][ 0 ]}:\n" )
        name = family[ 1 ][ 1 ].to_s
        output.puts( "  name: #{name}\n" )
        output.puts( "  description:  \n" )
        
        if ( name.index( 'IGNORED' ) )
          output.puts( "  ignored: true\n" )
        end
        output.puts( "\n" )
      end
    end
    
    
    #grava o arquivo do relacionamento dada.net x aparelhos
    File.open( 'device_integrators.yml', 'w' ) do |output|
      di_index = 0

      for playphone_entry in @@playphone
        output.puts( "playphone_#{di_index.to_s}:\n" )
        output.puts( "  integrator: playphone\n" )
        output.puts( "  device: #{playphone_entry[ 1 ]}\n" )
        output.puts( "  identifier: #{playphone_entry[ 0 ]}\n" )
        
        output.puts( "\n" )
        
        di_index += 1
      end

      di_index = 0
      for dada_entry in @@dada_net
        output.puts( "dadanet_#{di_index.to_s}:\n" )
        output.puts( "  integrator: dadanet\n" )
        output.puts( '  device: ' + dada_entry[ 1 ] + "\n" )
        output.puts( '  identifier: ' + dada_entry[ 0 ] + "\n" )
        
        output.puts( "\n" )
        
        di_index += 1
      end
    end
    
  end


  def self.add_info( file, keyIndex, value )
    if ( @@colunas[ keyIndex ] && !value.nil? )
      #remove espaãos em branco no início e no fim do valor a ser armazenado
      value.strip!()
      file.puts( "  #{@@colunas[ keyIndex ]}: #{value}" )
    end
  end

end


if ( ARGV[ 0 ] )
  Convert.convert( ARGV[ 0 ] )
end