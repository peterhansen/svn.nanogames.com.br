require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SessionsController do
	describe "routing" do
	  get("/login").should route_to "sessions#new"
    post("/sessions").should route_to "sessions#create"
	  get("/logout").should route_to "sessions#destroy"
	end
end