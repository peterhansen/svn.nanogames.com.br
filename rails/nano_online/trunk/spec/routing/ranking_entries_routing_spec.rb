require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RankingEntriesController do
  describe "routing" do
    get("/ranking_entries/").should route_to "ranking_entries#index"
    get("/ranking_entries/new").should route_to "ranking_entries#new"
    get("/ranking_entries/1").should_not be_routable
    get("/ranking_entries/137/edit").should be_routable
    post("/ranking_entries").should route_to "ranking_entries#create"
    put("/ranking_entries/1").should be_routable
    delete("/ranking_entries/1").should be_routable
    post("/ranking_entries/submit").should route_to(:controller => "ranking_entries", :action => "submit")
    get("/ranking_entries/submit").should_not be_routable
    get("/ranking_entries/user_range_high_score").should route_to(:controller => "ranking_entries", :action => "user_range_high_score")
    get("/ranking_entries/high_scores").should route_to(:controller => "ranking_entries", :action => "high_scores")
    get("/ranking_entries/update_high_scores").should route_to(:controller => "ranking_entries", :action => "update_high_scores")
  end
end