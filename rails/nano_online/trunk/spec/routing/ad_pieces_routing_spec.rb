require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AdPiecesController do
  describe "routing" do
    get("/ad_pieces/").should route_to "ad_pieces#index"
    get("/ad_pieces/new").should route_to "ad_pieces#new"
    get("/ad_pieces/1").should_not route_to "ad_pieces#show"
    get("/ad_pieces/137/edit").should be_routable
    post("/ad_pieces").should route_to "ad_pieces#create"
    put("/ad_pieces/1").should be_routable
    delete("/ad_pieces/1").should be_routable
  end
end