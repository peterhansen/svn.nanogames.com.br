require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe ImuzDb::GameSessionsController do
  describe "routing" do
    it "accepts POST to #frame" do
      { :post => "/imuz_db/game_sessions/frame" }.should route_to(:controller => "imuz_db/game_sessions", :action => "frame")
    end

    it "do not accepts GET to #frame" do
      { :get => "/imuz_db/game_sessions/frame" }.should_not be_routable
    end

    it "accepts GET to #home" do
      { :get => "/imuz_db/game_sessions/home" }.should route_to(:controller => "imuz_db/game_sessions", :action => "home")
    end

    it "accepts GET to #fb_login" do
      { :get => "/imuz_db/mobile/fb_login" }.should route_to(:controller => "imuz_db/game_sessions", :action => "fb_login")
    end

    it "accepts GET to #news" do
      { :get => "/imuz_db/game_sessions/news" }.should route_to(:controller => "imuz_db/game_sessions", :action => "news")
    end

    it "accepts GET to #start" do
      { :get => "/imuz_db/game_sessions/start" }.should route_to(:controller => "imuz_db/game_sessions", :action => "start")
    end

    it "accepts GET to #new_question_iphone" do
      { :get => "/imuz_db/game_sessions/new_question_iphone" }.should route_to(:controller => "imuz_db/game_sessions", :action => "new_question_iphone")
    end

    it "accepts POST to #respond" do
      { :post => "/imuz_db/game_sessions/respond" }.should route_to(:controller => "imuz_db/game_sessions", :action => "respond")
    end

    it "do not accepts GET to #respond" do
      { :get => "/imuz_db/game_sessions/respond" }.should_not be_routable
    end

    it "accepts GET to #next_question" do
      { :get => "/imuz_db/game_sessions/next_question" }.should route_to(:controller => "imuz_db/game_sessions", :action => "next_question")
    end

    it "accepts GET to #skip_question" do
      { :get => "/imuz_db/game_sessions/skip_question" }.should route_to(:controller => "imuz_db/game_sessions", :action => "skip_question")
    end

    it "accepts GET to #missed_question" do
      { :get => "/imuz_db/game_sessions/missed_question" }.should route_to(:controller => "imuz_db/game_sessions", :action => "missed_question")
    end

    it "accepts GET to #correct" do
      { :get => "/imuz_db/game_sessions/correct" }.should route_to(:controller => "imuz_db/game_sessions", :action => "correct")
    end

    it "accepts GET to #wrong" do
      { :get => "/imuz_db/game_sessions/wrong" }.should route_to(:controller => "imuz_db/game_sessions", :action => "wrong")
    end

    it "accepts GET to #game_over" do
      { :get => "/imuz_db/game_sessions/game_over" }.should route_to(:controller => "imuz_db/game_sessions", :action => "game_over")
    end

    it "accepts GET to #ranking" do
      { :get => "/imuz_db/game_sessions/ranking" }.should route_to(:controller => "imuz_db/game_sessions", :action => "ranking")
    end

    it "accepts GET to #choose_nickname" do
      { :get => "/imuz_db/game_sessions/choose_nickname" }.should route_to(:controller => "imuz_db/game_sessions", :action => "choose_nickname")
    end

    it "accepts POST to #set_nickname" do
      { :post => "/imuz_db/game_sessions/set_nickname" }.should route_to(:controller => "imuz_db/game_sessions", :action => "set_nickname")
    end

    it "do not accepts GET to #set_nickname" do
      { :get => "/imuz_db/game_sessions/set_nickname" }.should_not be_routable
    end

    it "do not accepts GET to #set_profile_info" do
      { :get => "/imuz_db/game_sessions/set_profile_info" }.should_not be_routable
    end

    it "accepts GET to #friend_list" do
      { :get => "/imuz_db/game_sessions/friend_list" }.should route_to(:controller => "imuz_db/game_sessions", :action => "friend_list")
    end

    it "accepts POST to #invite_friends" do
      { :post => "/imuz_db/game_sessions/invite_friends" }.should route_to(:controller => "imuz_db/game_sessions", :action => "invite_friends")
    end

    it "do not accepts GET to #invite_friends" do
      { :get => "/imuz_db/game_sessions/invite_friends" }.should_not be_routable
    end

    it "accepts GET to #facebook_share" do
      { :get => "/imuz_db/game_sessions/facebook_share" }.should route_to(:controller => "imuz_db/game_sessions", :action => "facebook_share")
    end

    it "accepts GET to #help" do
      { :get => "/imuz_db/game_sessions/help" }.should route_to(:controller => "imuz_db/game_sessions", :action => "help")
    end

    it "accepts POST to #audio_state" do
      { :post => "/imuz_db/game_sessions/audio_state" }.should route_to(:controller => "imuz_db/game_sessions", :action => "audio_state")
    end

    it "do not accepts GET to #audio_state" do
      { :get => "/imuz_db/game_sessions/audio_state" }.should_not be_routable
    end

    it "accepts GET to #fb_session" do
      { :get => "/imuz_db/mobile/fb_session" }.should route_to(:controller => "imuz_db/game_sessions", :action => "fb_session")
    end

    it "accepts GET to #prepare_share" do
      { :get => "/imuz_db/game_sessions/prepare_share" }.should route_to(:controller => "imuz_db/game_sessions", :action => "prepare_share")
    end
  end
end