require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe CustomersController do
  describe "routing" do
    get("/customers/").should route_to "customers#index"
    get("/customers/new").should route_to "customers#new"
    post("/customers/login").should route_to "customers#login"
    post("/customers/import").should route_to "customers#import"
    post("/customers/add").should route_to "customers#add"
    post("/customers/refresh").should route_to "customers#refresh"
    get("/customers/1").should_not route_to "customers#show"
    get("/customers/137/edit").should be_routable
    post("/customers").should route_to "customers#create"
    put("/customers/1").should be_routable
    delete("/customers/1").should be_routable
  end
end