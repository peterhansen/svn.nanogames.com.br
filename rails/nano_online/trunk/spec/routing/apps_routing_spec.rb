require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AppsController do
  describe "routing" do
    get("/apps/").should route_to "apps#index"
    get("/apps/list").should route_to "apps#list"
    get("/apps/new").should route_to "apps#new"
    get("/apps/1").should_not route_to "apps#show"
    get("/apps/137/edit").should be_routable
    post("/apps").should route_to "apps#create"
    put("/apps/1").should be_routable
    delete("/apps/1").should be_routable
  end
end