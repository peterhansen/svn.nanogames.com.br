require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AdCampaignsController do
  describe "routing" do
    get("/ad_campaigns/").should route_to "ad_campaigns#index"
    get("/ad_campaigns/new").should route_to "ad_campaigns#new"
    get("/ad_campaigns/1").should_not route_to "ad_campaigns#show"
    get("/ad_campaigns/137/edit").should be_routable
    post("/ad_campaigns").should route_to "ad_campaigns#create"
    put("/ad_campaigns/1").should be_routable
    delete("/ad_campaigns/1").should be_routable
  end
end