require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AdCampaignsController do
  describe "routing" do
    get("/ad_channel_statuses/").should route_to "ad_channel_statuses#index"
    get("/ad_channel_statuses/new").should route_to "ad_channel_statuses#new"
    get("/ad_channel_statuses/1").should_not route_to "ad_channel_statuses#show"
    get("/ad_channel_statuses/137/edit").should be_routable
    post("/ad_channel_statuses").should route_to "ad_channel_statuses#create"
    put("/ad_channel_statuses/1").should be_routable
    delete("/ad_channel_statuses/1").should be_routable
  end
end