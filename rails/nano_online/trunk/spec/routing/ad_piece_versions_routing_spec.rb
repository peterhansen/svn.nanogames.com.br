require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AdPieceVersionsController do
  describe "routing" do
    get("/ad_piece_versions/").should route_to "ad_piece_versions#index"
    get("/ad_piece_versions/new").should route_to "ad_piece_versions#new"
    get("/ad_piece_versions/1").should_not route_to "ad_piece_versions#show"
    get("/ad_piece_versions/137/edit").should be_routable
    post("/ad_piece_versions").should route_to "ad_piece_versions#create"
    put("/ad_piece_versions/1").should be_routable
    delete("/ad_piece_versions/1").should be_routable
    get("/ad_piece_versions/delete_file").should_not route_to "ad_piece_versions#delete_file"
  end
end