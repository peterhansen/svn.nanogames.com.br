require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AdChannelVersionsController do
  describe "routing" do
    get("/ad_channel_versions/").should route_to "ad_channel_versions#index"
    get("/ad_channel_versions/new").should route_to "ad_channel_versions#new"
    get("/ad_channel_versions/1").should_not route_to "ad_channel_versions#show"
    get("/ad_channel_versions/137/edit").should be_routable
    post("/ad_channel_versions").should route_to "ad_channel_versions#create"
    put("/ad_channel_versions/1").should be_routable
    delete("/ad_channel_versions/1").should be_routable
    get("/ad_channel_versions/delete_composition").should route_to "ad_channel_versions#delete_composition"
  end
end