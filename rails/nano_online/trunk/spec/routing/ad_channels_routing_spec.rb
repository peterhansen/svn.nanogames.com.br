require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AdChannelsController do
  describe "routing" do
    get("/ad_channels/").should route_to "ad_channels#index"
    get("/ad_channels/new").should route_to "ad_channels#new"
    get("/ad_channels/1").should_not route_to "ad_channels#show"
    get("/ad_channels/137/edit").should be_routable
    post("/ad_channels").should route_to "ad_channels#create"
    put("/ad_channels/1").should be_routable
    delete("/ad_channels/1").should be_routable
  end
end