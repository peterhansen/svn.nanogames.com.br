require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe "Signing In" do
	context "via Twitter" do
	  it "shows a 'sign in' link" do
	  	visit new_session_url
	    page.should have_link('Clique aqui para logar com o Twitter.')
	  end
	  
	  it "link redirects to /auth/twitter" do
	  	Customer.should_receive(:authenticate_omniauth).and_return(Customer.make!)
      # Authentication.stub(:find_by_provider_and_uid).and_return(mock_model(Authentication, :customer => Customer.make!))
	    visit new_session_url
	    click_link "Clique aqui para logar com o Twitter."
	    page.should have_content('Nano')
	  end
	end
end