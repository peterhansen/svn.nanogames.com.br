require 'machinist/active_record'

App.blueprint do
	name 			{ "App #{sn}" }
	name_short		{ rand_string }
end

AppVersion.blueprint do
	number	{ "0.0.1" }
	status	{ "Available" }
	app     { App.make! }
end

Customer.blueprint do
	nickname		{ "Customer#{sn}" }
	first_name      { "Customer#{sn}" }
	last_name       { "Customer#{sn}" }
	email           { "email#{sn}@exemplo.com.br" }
	password		{ "senha123" }
end

RankingEntry.blueprint do
	sub_type	{ 0 }
	score		{ 999 }
	ranking_type
	device
	app_version
	customer 	{ Customer.make! }
end

RankingType.blueprint do
	ranking_system
	unit_format
	description { "Descricao" }
	name 		{ "Ranking Type #{sn}" }
	sub_type 	{ 0 }
end

RankingSystem.blueprint do
	name 		{ "cumulative_crescent" }
end

UnitFormat.blueprint do
	name 		{ "Unit Format #{sn}" }
end

Device.blueprint do
	vendor
	family
	model	{ "Model #{sn}" }
end

Vendor.blueprint do
	name 		{ "Vendor #{sn}" }
	active      { true }
end

Family.blueprint do
	name        { "Family #{sn}" }
end

Authentication.blueprint do
	provider 			{ 'Facebook' }
	uid						{ '1234' }
	oauth_token		{ '123456789' }
	oauth_secret	{ '987654321' }
	customer
end

def rand_string(size = 4)
	letters = %w{a b c d e f g h i j k l m n o p q r s t u v x z }
	string = ''
	size.times do
		string << letters[rand(letters.size)]
	end
	string
end