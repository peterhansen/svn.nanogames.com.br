require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe FacebookAuth do
  describe FacebookAuth::Token do

    it 'returns uid and oauth_token if signed_request is valid' do
      ActiveSupport::JSON.should_receive(:decode).and_return({"user_id" => '1234', "oauth_token" => "4321"})
      FacebookAuth::Token.get({:signed_request => 'qualquer.coisa'}).should == ["1234", "4321"]
    end

    it 'returns uid and oauth_token if signed_request doesnt contain an user_id' do
      ActiveSupport::JSON.should_receive(:decode).and_return({"bla" => '1234'})
      lambda { FacebookAuth::Token.get({:signed_request => 'qualquer.coisa'}) }.should raise_error(FacebookAuth::NoUserLoggedException)
    end

    it 'raises exception if signed_request is not valid' do
      ActiveSupport::JSON.should_receive(:decode).and_raise(Exception.new)
      lambda { FacebookAuth::Token.get({:signed_request => 'qualquer.coisa'}) }.should raise_error(FacebookAuth::InvalidSignedRequestException)
    end

    it 'raises exception if signed_request is null' do
      lambda { FacebookAuth::Token.get({:signed_request => ''}) }.should raise_error(FacebookAuth::InvalidSignedRequestException)
    end

    it 'raises exception if signed_request not formatted properlly (missing .)' do
      lambda { FacebookAuth::Token.get({:signed_request => 'qualquercoisa'}) }.should raise_error(FacebookAuth::InvalidSignedRequestException)
    end

    it 'raises exception if signed_request and code are not present' do
      lambda { FacebookAuth::Token.get({}) }.should raise_error(FacebookAuth::InvalidFacebookResponseException)
    end

    it 'returns uid and oauth_token if code is valid' do
      MiniFB.should_receive(:oauth_access_token).and_return({'access_token' => '4321'})
      MiniFB.should_receive(:get).and_return mock('mock', :id => '1234')
      FacebookAuth::Token.get({:code => 'qualquer.coisa'}).should == ["1234", "4321"]
    end

    it 'raises exception if code is not valid' do
      lambda { FacebookAuth::Token.get({:code => ''}) }.should raise_error(FacebookAuth::InvalidCodeException)
    end
  end
end