# encoding: UTF-8
require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

feature "Registration:", %q{
  Como um novo usuario
  Com o objetivo de utilizar o Nano Online
  Eu deveria poder me cadastrar
} do
  
  scenario "Pagina de login possui link para pagina de cadastro" do
    visit login_url
    page.should have_content('Sign Up')
  end

  scenario "Link 'Sign Up' leva para a pagina de cadastro" do
    visit login_url
    click_link 'Sign Up'
    current_path.should == new_registration_path
  end

  scenario "Pagina de cadastro possui campos username, email e password" do
    visit new_registration_url
    page.should have_field('Nickname')
    page.should have_field('Email')
    page.should have_field('Password')
    page.should have_field('Password Confirmation')
    page.should have_button('Submit')
  end

  scenario "O cadastro é realizado com sucesso caso os dados sejam validos" do
    Customer.delete_all # TODO verificar porque isso é necessário.
    visit new_registration_url
    fill_in 'Nickname', :with => 'Nanoca'
    fill_in 'Password', :with => 'senha123'
    fill_in 'Password Confirmation', :with => 'senha123'
    click_button 'Submit'
    page.should have_content('Registration created')
  end

  scenario "O cadastro não é realizado caso o nome esteja em branco" do
    Customer.delete_all # TODO verificar porque isso é necessário.
    visit new_registration_url
    fill_in 'Email', :with => 'nanoca@nano.com'
    fill_in 'Password', :with => 'senha123'
    fill_in 'Password Confirmation', :with => 'senha123'
    click_button 'Submit'
    page.should have_content('Error creating account')
    page.should have_content("Nickname is too short (minimum is 2 characters)")
  end

  scenario "O cadastro não é realizado caso já exista um usuário com o mesmo nickname" do
    Customer.make! :nickname => 'Duck'
    visit new_registration_url
    fill_in 'Nickname', :with => 'Duck'
    fill_in 'Password', :with => 'senha123'
    fill_in 'Password Confirmation', :with => 'senha123'
    click_button 'Submit'
    page.should have_content('Error creating account')
    page.should have_content("Nickname has already been taken")
  end

  scenario "O cadastro não é realizado caso a confirmação não seja igual a senha", %q{
    Dado que eu não estou logado
    Quando eu tento me logar digitando uma confirmação de senha incorreta
    Eu devo ver uma mensagem de erro
  } do
    Customer.delete_all # TODO verificar porque isso é necessário.
    visit new_registration_url
    fill_in 'Nickname', :with => 'Duck'
    fill_in 'Password', :with => 'senha123'
    fill_in 'Password Confirmation', :with => 'outrasenha'
    click_button 'Submit'
    page.should have_content('Error creating account')
    page.should have_content("Password doesn't match confirmation")
  end
end