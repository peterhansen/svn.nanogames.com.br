require File.expand_path(File.dirname(__FILE__) + '/../../spec_helper')

describe 'Omniauth Authentication' do
	before(:each) do
		@customer = Customer.make!
	end

	describe "#add_omniauth_authentication" do
		it "adds a new authentication to the customer" do
		  lambda do
		  	@customer.add_authentication('provider' => 'qualquer', 'uid' => 'coisa', 'credentials' => { 'token' => '1234', 'secret' => '4321' })
		  end.should change(@customer.authentications, :size).by(1)
		end

	  it "returns an Authentication object" do
	    @customer.add_authentication('provider' => 'qualquer', 'uid' => 'coisa', 'credentials' => { 'token' => '1234', 'secret' => '4321' }).class.should == Authentication
	  end
	end

	describe "#create_omniauth_authentication" do
	  it "creates a new customer" do
	    Customer.should_receive(:create!)
	    Customer.create_with_omniauth('user_info' => { 'name' => 'qualquer', 'email' => 'qualquer@coisa.com'}, 'provider' => 'qualquer', 'uid' => 'coisa', 'credentials' => { 'token' => '1234', 'secret' => '4321' })
	  end

	  it "create a new authentication" do
		  lambda do
		  	Customer.create_with_omniauth('user_info' => { 'name' => 'qualquer', 'email' => 'qualquer@coisa.com'}, 'provider' => 'qualquer', 'uid' => 'coisa', 'credentials' => { 'token' => '1234', 'secret' => '4321' })
		  end.should change(Authentication, :count).by(1)
	  end
	end

	describe "#authenticate_omniauth" do
	  context "with omniauth_hash" do
	  	it "fetches authentication" do
		    Authentication.should_receive(:find_by_provider_and_uid).and_return(stub(:customer => '123'))
		    Customer.authenticate_omniauth(omniauth_hash: 'alguma coisa')
	  	end
	  end

	  context "with username and password" do
	  	it "fetches the customer" do
		    Customer.should_receive(:authenticate)
	  	  Customer.authenticate_omniauth(customer_hash: {nickname: 'zeh', password: 'senha123'})
	  	end
	  end
	end

	describe "#has_provider" do
	  it "returns true if the customer have authenticated with the given provider" do
	    auth = Authentication.make!
	    auth.customer.has_provider?(auth.provider.to_sym).should be_true
	  end

	  it "returns false if the customer have not authenticated with the given provider" do
	    auth = Authentication.make!
	    auth.customer.has_provider?(:outro).should be_false
	  end
	end

	describe "#merge" do
	  it "adds the merged customer's authentications into the current customer" do
	    customer1 = Customer.make!
	    customer2 = Customer.make!
	    Authentication.make!(:customer => customer1)
	    Authentication.make!(:customer => customer2)
	    customer1.merge customer2
	    customer1.authentications.count.should == 2
	  end

	  it "marks the merged customer as deactivated"
	end
end