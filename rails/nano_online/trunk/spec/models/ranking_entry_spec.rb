# == Schema Information
#
# Table name: ranking_entries
#
#  id              :integer(4)      not null, primary key
#  customer_id     :integer(4)      not null
#  app_version_id  :integer(4)      not null
#  device_id       :integer(4)
#  score           :integer(8)      default(0), not null
#  sub_type        :integer(2)      default(0), not null
#  created_at      :datetime
#  updated_at      :datetime
#  record_time     :datetime
#  ranking_type_id :integer(4)
#

require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')
include DataParser

describe RankingEntry do
  fixtures :app_versions, :customers, :ranking_types, :ranking_entries

  before(:each) do
    @app_version = AppVersion.make!
    @customer = Customer.make!
  end

  describe "#submit_records" do
    let(:output) do
      out = LittleEndianTypes::OutputStream.new
      out.write_byte    RankingEntry::PARAM_N_ENTRIES
      out.write_byte    1
      out.write_byte    RankingEntry::PARAM_SUB_TYPE
      out.write_byte    1
      out.write_int     @customer.id # customer_id
      out.write_long    1234 # score
      out.write_datetime DateTime.new # record_time

      out
    end

    context "with valid params" do
      before(:each) do
        ranking = RankingEntry.make!
        RankingEntry.stub(:new).and_return(ranking)
      end
      
      it "creates a new ranking entry" do
        response = RankingEntry.submit_records({ID_SPECIFIC_DATA => output.flush, :app_version => @app_version, ID_NANO_ONLINE_VERSION => "0.0.1"})
        response.first.should be_true
      end
#
      it "returns the profile indexes, when they are present" do
        response = RankingEntry.submit_records({ID_SPECIFIC_DATA => output.flush,
          :app_version => @app_version,
          ID_NANO_ONLINE_VERSION => "0.0.1"})

        response[1].should == [@customer.id]
      end

      it "returns an empty array as the 'with error' entries" do
        response = RankingEntry.submit_records({ID_SPECIFIC_DATA => output.flush, :app_version => @app_version, ID_NANO_ONLINE_VERSION => "0.0.1"})
        response[2].should be_empty
      end
    end

    context "with no rankings to be submited params" do
      it "do not create any ranking" do
        response = RankingEntry.submit_records({ID_SPECIFIC_DATA => LittleEndianTypes::OutputStream.new, :app_version => @app_version, ID_NANO_ONLINE_VERSION => "0.0.1"})
        response.first.should be_true
      end
    end

    context "when the number of entries informed don't match the actual quantity" do

    end

    context "with invalid ranking entry data" do
      let(:output) do
        out = LittleEndianTypes::OutputStream.new
        out.write_byte(RankingEntry::PARAM_N_ENTRIES)
        out.write_byte(1)
        out.write_byte(RankingEntry::PARAM_SUB_TYPE)
        out.write_byte(0)
        out.write_int(1234) # customer_id
        out.write_long(1) # score
        out.write_datetime(DateTime.new) # record_time
        out
      end

      it 'returns false' do
        response = RankingEntry.submit_records({ID_SPECIFIC_DATA => output.flush, :app_version => @app_version, ID_NANO_ONLINE_VERSION => "0.0.1"})
        response.first.should be_false
      end

      it 'return an array of invalid entries' do
        response = RankingEntry.submit_records({ID_SPECIFIC_DATA => output.flush, :app_version => @app_version, ID_NANO_ONLINE_VERSION => "0.0.1"})
        response.last.should_not be_nil
      end
    end
  end

  describe '#position' do
    describe 'with valid params' do
        before(:each) do
          @ranking_type = RankingType.make!
          @app_version = AppVersion.make!
          @ranking_entry_1 = RankingEntry.make! :score => 300, :ranking_type => @ranking_type, :app_version => @app_version
          @ranking_entry_2 = RankingEntry.make! :score => 400, :ranking_type => @ranking_type, :app_version => @app_version
          @ranking_entry_3 = RankingEntry.make! :score => 400, :ranking_type => @ranking_type, :app_version => @app_version
        end

      it "return the user position" do
        RankingEntry.position(@ranking_entry_1.customer_id, @app_version.id, @ranking_type.id).should == 2
      end

      describe 'if there is two customers with the same score' do
        it 'returns the correct position' do
          RankingEntry.position(@ranking_entry_2.customer_id, @app_version.id, @ranking_type.id).should == 1
          RankingEntry.position(@ranking_entry_3.customer_id, @app_version.id, @ranking_type.id).should == 1
        end
      end

      describe 'if the customer have no ranking entry' do
        it "returns nil" do
          RankingEntry.position(Customer.make!.id, @app_version.id, @ranking_type.id).should be_nil
        end
      end
    end
  end

  describe '#best_scores' do

  end

  describe '#get_cumulative_rankings' do
    let(:ranking_type) { ranking_types(:cumulative_ranking) }

    context ':crescent (having 7 ranking entries for 4 customers)' do
      it 'returns the 4 rankings, one for each customer' do
        RankingEntry.get_cumulative_rankings(:crescent, 10, 0, ranking_type).all.size.should == 4
      end

      it "returns 1000 (500 + 500, the sum of the customer's two entries) for the FIRST customer's score (who is last in the ranking)" do
        RankingEntry.get_cumulative_rankings(:crescent, 10, 0, ranking_type).first.score.should == 1000
      end

      it "returns 1900 (1000 + 700 + 200, the sum of the customer's three entries) for the LAST customer's score (who is first in the ranking)" do
        RankingEntry.get_cumulative_rankings(:crescent, 10, 0, ranking_type).last.score.should == 1900
      end

      context 'with limit set to 2' do
        it 'returns 2 rankings' do
          RankingEntry.get_cumulative_rankings(:crescent, 2, 0, ranking_type).all.size.should == 2
        end
      end

      context 'with offset set to 1 (and limit set to 1)' do
        it "returns 'boris' (the second in ranking)" do
          RankingEntry.get_cumulative_rankings(:crescent, 1, 1, ranking_type).first.customer.nickname.should == 'boris'
        end

        it 'returns one ranking' do
          RankingEntry.get_cumulative_rankings(:crescent, 1, 2, ranking_type).all.size.should == 1
        end
      end
    end

    context ':decrescent (having 7 ranking entries for 4 customers)' do
      it 'returns the 4 rankings, one for each customer' do
        RankingEntry.get_cumulative_rankings(:decrescent, 10, 0, ranking_type).all.size.should == 4
      end

      it "returns 1900 (1000 + 700 + 200, the sum of the customer's three entries) for the FIRST customer's score (who is first in the ranking)" do
        RankingEntry.get_cumulative_rankings(:decrescent, 10, 0, ranking_type).first.score.should == 1900
      end

      it "returns 1000 (500 + 500, the sum of the customer's two entries) for the LAST customer's score (who is last in the ranking)" do
        RankingEntry.get_cumulative_rankings(:decrescent, 10, 0, ranking_type).last.score.should == 1000
      end

      context 'with limit set to 2' do
        it 'returns 2 rankings' do
          RankingEntry.get_cumulative_rankings(:decrescent, 2, 0, ranking_type).all.size.should == 2
        end
      end

      context 'with offset set to 1 (and limit set to 1)' do
        it "returns 'nanoca' (the second in ranking)" do
          RankingEntry.get_cumulative_rankings(:decrescent, 1, 1, ranking_type).first.customer.nickname.should == 'nanoca'
        end

        it 'returns one ranking' do
          RankingEntry.get_cumulative_rankings(:decrescent, 1, 2, ranking_type).all.size.should == 1
        end
      end
    end
  end

  describe '#get_cumulative_rankings' do
    let(:ranking_type) { ranking_types(:top_score_ranking) }

    context ':crescent (having 8 ranking entries for 6 customers)' do
      it 'returns the 6 rankings, one for each customer' do
        RankingEntry.get_score_rankings(:crescent, 10, 0, ranking_type).all.size.should == 6
      end

      it "returns 200 (the better score between 200 and 100) for the FIRST customer's score (who is last in the ranking)" do
        RankingEntry.get_score_rankings(:crescent, 10, 0, ranking_type).first.score.should == 200
      end

      it "returns 700 (the better score between 700 and 150) for the LAST customer's score (who is first in the ranking)" do
        RankingEntry.get_score_rankings(:crescent, 10, 0, ranking_type).last.score.should == 700
      end

      context 'with limit set to 2' do
        it 'returns 2 rankings' do
          RankingEntry.get_score_rankings(:crescent, 2, 0, ranking_type).all.size.should == 2
        end
      end

      context 'with offset set to 1 (and limit set to 1)' do
        it "returns 'dilas' (the second in ranking)" do
          RankingEntry.get_score_rankings(:crescent, 1, 1, ranking_type).first.customer.nickname.should == 'boris'
        end

        it 'returns one ranking' do
          RankingEntry.get_score_rankings(:crescent, 1, 2, ranking_type).all.size.should == 1
        end
      end
    end
  end
end
