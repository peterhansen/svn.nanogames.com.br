require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe ImuzDb::GameSessionsController do
  describe "Facebook login" do
    context 'with external request' do
      context 'on initial request' do
        it 'renders :home if session[:uid] is set' do
          controller.session[:uid] = '1234'
          FacebookProfile.should_receive(:find_by_uid).and_return(stub(:customer => stub(:id => '1234')))
          get :home
          response.should render_template :home
        end

        it "logs the customer in" do
          FacebookAuth::Token.should_receive(:get).and_return([true, true])
          FacebookProfile.should_receive(:find_by_uid).and_return(stub(:customer => 'anything'))
          controller.should_receive(:login_customer)
          get :home
        end

        it 'redirects to facebook login page if signed_request and code are not present' do
          FacebookAuth::Token.should_receive(:get).and_raise(FacebookAuth::InvalidSignedRequestException)
          get :home
          response.body.should  == "      <html><head>\n        <script type=\"text/javascript\">\n          window.top.location.href = 'https://graph.facebook.com/oauth/authorize?client_id=133030040091788&redirect_uri=http://apps.facebook.com/imuzdbquiz/&scope=email,publish_stream,offline_access';\n        </script>\n        <noscript>\n          <meta http-equiv=\"refresh\" content=\"0;url=https://graph.facebook.com/oauth/authorize?client_id=133030040091788&amp;redirect_uri=http://apps.facebook.com/imuzdbquiz/&amp;scope=email,publish_stream,offline_access\" />\n          <meta http-equiv=\"window-target\" content=\"_top\" />\n        </noscript>\n      </head></html>\n"
        end

        it 'redirects to facebook login page if signed_request doesnt contain an user_id' do
          FacebookAuth::Token.should_receive(:get).and_raise(FacebookAuth::NoUserLoggedException)
          get :home
          response.body.should  == "      <html><head>\n        <script type=\"text/javascript\">\n          window.top.location.href = 'https://graph.facebook.com/oauth/authorize?client_id=133030040091788&redirect_uri=http://apps.facebook.com/imuzdbquiz/&scope=email,publish_stream,offline_access';\n        </script>\n        <noscript>\n          <meta http-equiv=\"refresh\" content=\"0;url=https://graph.facebook.com/oauth/authorize?client_id=133030040091788&amp;redirect_uri=http://apps.facebook.com/imuzdbquiz/&amp;scope=email,publish_stream,offline_access\" />\n          <meta http-equiv=\"window-target\" content=\"_top\" />\n        </noscript>\n      </head></html>\n"
        end

        it 'render 500 page when signed_request and code are not present' do
          FacebookAuth::Token.should_receive(:get).and_raise(FacebookAuth::InvalidFacebookResponseException)
          get :home
          response.should redirect_to :action => :facebook_error, :old_params => controller.params
        end

        it 'set session[:uid] and session[:oauth_token] if facebook authentication is successful' do
          FacebookAuth::Token.should_receive(:get).and_return(['1234', '4321'])
          FacebookProfile.should_receive(:find_by_uid).and_return(stub(:customer => stub(:id => '1234')))
          get :home
          session[:uid].should == '1234'
          session[:oauth_token].should == '4321'
        end
      end
    end
  end

  describe "GET /start" do
    context "when not logged to facebook" do
      it 'redirect to /next_question' do
        get :start
        response.should redirect_to :action => :home, :p => 1234
      end
    end
  end
end