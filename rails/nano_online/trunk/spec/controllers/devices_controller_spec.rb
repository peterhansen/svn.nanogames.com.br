require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe DevicesController do
  describe "GET /list" do
    before(:each) do
      @vendor = Vendor.make!
      @app = App.make!
      @sql_with_app = ["SELECT distinct devices.id, devices.model, app_versions.status FROM devices LEFT OUTER JOIN families ON families.id = devices.family_id LEFT OUTER JOIN app_versions_families ON app_versions_families.family_id = families.id LEFT OUTER JOIN app_versions ON app_versions.id = app_versions_families.app_version_id LEFT OUTER JOIN apps ON apps.id = app_versions.app_id WHERE devices.vendor_id = ? AND (app_versions.status = 'Available') and apps.name_short = ? ORDER BY devices.model", @vendor.id, @app.name_short]
      @sql_without_app = ["SELECT distinct devices.id, devices.model, app_versions.status FROM devices LEFT OUTER JOIN families ON families.id = devices.family_id LEFT OUTER JOIN app_versions_families ON app_versions_families.family_id = families.id LEFT OUTER JOIN app_versions ON app_versions.id = app_versions_families.app_version_id LEFT OUTER JOIN apps ON apps.id = app_versions.app_id WHERE devices.vendor_id = ? AND (app_versions.status = 'Available') ORDER BY devices.model", @vendor.id]
    end

    it 'does not respond to :format => :html' do
      get :list
      response.should_not be_success
    end

    it 'responds to :format => :json' do
      get :list, :format => :json
      response.should be_success
    end

    it 'returns devices for the given app with a valid params[:app]' do
      Device.should_receive(:find_by_sql).with(@sql_with_app)
      get :list, :format => :json, :vendor => @vendor.id, :app => @app.name_short
    end

    it 'returns all devices if params[:app] is nil' do
      Device.should_receive(:find_by_sql).with(@sql_without_app)
      get :list, :format => :json, :vendor => @vendor.id
    end

    it 'returns all devices if params[:app] is an empty string' do
      Device.should_receive(:find_by_sql).with(@sql_without_app)
      get :list, :format => :json, :vendor => @vendor.id, :app => ''
    end

    it 'returns an empty array if params[:vendor] is nil' do
      get :list, :format => :json, :app => @app.name_short
      response.body.should == "[]"
    end
  end
end