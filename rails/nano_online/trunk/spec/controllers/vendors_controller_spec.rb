require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe VendorsController do
  fixtures :apps
  before(:each) do
    @vendor = Vendor.make!
    @app = App.make!
    @sql_with_app = ["SELECT distinct vendors.id as id, vendors.name as name FROM vendors LEFT OUTER JOIN devices ON devices.vendor_id = vendors.id LEFT OUTER JOIN families ON families.id = devices.family_id LEFT OUTER JOIN app_versions_families ON app_versions_families.family_id = families.id LEFT OUTER JOIN app_versions ON app_versions.id = app_versions_families.app_version_id LEFT OUTER JOIN apps ON app_versions.app_id = apps.id WHERE apps.name_short = ? AND (app_versions.status = 'Available') ORDER BY vendors.name", @app.name_short]
    @sql_without_app = "SELECT distinct vendors.id as id, vendors.name as name FROM vendors LEFT OUTER JOIN devices ON devices.vendor_id = vendors.id LEFT OUTER JOIN families ON families.id = devices.family_id LEFT OUTER JOIN app_versions_families ON app_versions_families.family_id = families.id LEFT OUTER JOIN app_versions ON app_versions.id = app_versions_families.app_version_id LEFT OUTER JOIN apps ON app_versions.app_id = apps.id WHERE (app_versions.status = 'Available') ORDER BY vendors.name"
  end
  
  describe "GET /list" do
    let(:vendor) { Vendor.make! }
    let(:app)    { App.make! }

    it 'does not respond to :format => :html' do
      get :list
      response.should_not be_success
    end

    it 'responds to :format => :json' do
      Vendor.should_receive(:find_by_sql).and_return(@vendor)
      get :list, :format => :json
      response.body.should == @vendor.to_json(:only => [:id, :name])
    end

    it 'returns devices for the given app with a valid params[:app]' do
      Vendor.should_receive(:find_by_sql).with(@sql_with_app).and_return(@vendor)
      get :list, :format => :json, :app => @app.name_short
    end

    it 'returns all devices if params[:id] is nil' do
      Vendor.should_receive(:find_by_sql).with(@sql_without_app).and_return(@vendor)
      get :list, :format => :json
    end

    it 'returns all devices if params[:id] is an empty string' do
      Vendor.should_receive(:find_by_sql).with(@sql_without_app).and_return(@vendor)
      get :list, :format => :json, :id => ''
    end
  end
end