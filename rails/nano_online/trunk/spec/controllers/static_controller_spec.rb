require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe StaticController do
  describe "GET :home" do
    it "renders the home template" do
      get :home
      response.should render_template(:home)
    end

    context "external download pages" do
      before :each do
        Timecop.freeze(Time.local(2000,01,01,0,0,0))
      end

      it "renders the axe template when host = game.axe.com.br" do
        request.env[ 'HTTP_HOST' ] = 'game.axe.com.br'
        get :home
        response.should redirect_to(:controller => :downloads, :action => :download_axe_mobile, :timestamp => Time.now.to_i)
      end

      it "renders the axe template when host = colocation.cubo.cc" do
        request.env[ 'HTTP_X_FORWARDED_HOST' ] = 'colocation.cubo.cc'
        get :home
        response.should redirect_to(:controller => :downloads, :action => :download_axe_mobile, :timestamp => Time.now.to_i)
      end

      it "renders the axe template when host = colocation2.cubo.cc" do
        request.env[ 'HTTP_HOST' ] = 'colocation2.cubo.cc'
        get :home
        response.should redirect_to(:controller => :downloads, :action => :download_axe_mobile, :timestamp => Time.now.to_i)
      end

      it "renders the seda template when host = game.reconstrucaoem10dias.com.br" do
        request.env[ 'HTTP_HOST' ] = 'game.reconstrucaoem10dias.com.br'
        get :home
        response.should redirect_to(:controller => :downloads, :action => :download_seda_puzzle, :timestamp => Time.now.to_i)
      end
    end
  end

  describe "GET :show" do
    [ 'about', 'barrigadas_ornamentais', 'biritometro', 'bob_esponja', 'brazooka_soccer', 'dancinrio', 'fullpower_drag_race',
      'ginastica_do_away', 'jimmy_neutron', 'joselito', 'levantamento_de_peso', 'lux', 'partners', 'news', 'penalti_com_cleston',
      'pepsi_cai_bem', 'portfolio_2010', 'seda_puzzle', 'space_goo', 'superlux', 'web_contact', 'download' ].each do |page|
      it "renders #{page} template" do
        get :show, :id => page
        response.should render_template(page)
      end
    end

    it 'do not renders banner template' do
      get :show, :id => 'banner'
      response.should_not render_template('banner')
    end
  end
end