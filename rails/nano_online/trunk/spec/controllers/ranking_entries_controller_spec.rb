# encoding: UTF-8
require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RankingEntriesController do
  fixtures :customers, :ranking_types, :app_versions, :apps#, :ranking_entries

  # let(:admin) { mock_model(User, :role => :administrator, :layout => 'admin') }

  describe "GET /index" do
    it "assigns @ranking_entries" do
      ranking_entry = RankingEntry.make!
      controller.should_receive(:current_user).at_least(1).times.and_return(mock_model(User, :is_admin? => true))
      get :index
      assigns(:ranking_entries).should == [ranking_entry]
    end
  end

  describe "POST /submit" do
    before(:each) do
      @app = App.make!
      @app_version = AppVersion.make!
      @customer = Customer.make!
    end

    let(:nano_signature)  { "#{[137].pack('i')}NANO#{[13].pack('i')}#{[10].pack('i')}#{[26].pack('i')}#{[10].pack('i')}" }

    let(:output) do
      out = LittleEndianTypes::OutputStream.new
      # nano_signature
        [ 137, 110, 97, 110, 111, 13, 10, 26, 10 ].each do |byte|
          out.write_byte byte
        end
      # end of nano_signature
      out.write_byte    ID_APP
      out.write_string  @app.name_short
      out.write_byte    ID_APP_VERSION
      out.write_string  @app_version.number.to_s
      out.write_byte    ID_NANO_ONLINE_VERSION
      out.write_string  '0.0.2'
      out.write_byte    ID_SPECIFIC_DATA
      out.write_byte    RankingEntry::PARAM_N_ENTRIES
      out.write_byte    1
      out.write_byte    RankingEntry::PARAM_SUB_TYPE
      out.write_byte    1
      out.write_int     @customer.id # customer_id
      out.write_long    1234 # score
      out.write_datetime DateTime.new # record_time

      out
    end

    it 'creates a new ranking entry' do
      pending
      request.env["PATH_INFO"] = 'BLA'
      request.env["RAW_POST_DATA"] = output.flush
      lambda do
        post :submit
      end.should change(RankingEntry, :count).by(1)
    end
  end

  describe 'GET user_range_high_score' do
    before(:each) do
      @ranking = RankingEntry.make!
      RankingEntry.should_receive(:best_scores).and_return([@ranking])
      RankingEntry.should_receive(:position).and_return(3)
    end

    it "should be success" do
      get :user_range_high_score, params_with_range(3)
      response.should be_success
    end

    it "assigns @position" do
      get :user_range_high_score, params_with_range(1)
      assigns(:position).should == 3
    end

    it "assigns @ranking_entries" do
      get :user_range_high_score, params_with_range(1)
      assigns(:ranking_entries).should == @ranking
    end

    it "returns ranking entries as json" do
      get :user_range_high_score, params_with_range(1)
      response.body.should == "[\"{\\\"id\\\":#{@ranking.id},\\\"score\\\":#{@ranking.score},\\\"customer\\\":{\\\"id\\\":#{@ranking.customer.id},\\\"nickname\\\":\\\"#{@ranking.customer.nickname}\\\"}}\",\"{\\\"position\\\":3}\"]"
    end
  end

  def params_with_range(value = 0)
    { uid: customers(:silas).id,
      ranking_type: ranking_types(:top_score_ranking).id,
      app_version: app_versions(:release_killer).id,
      range: value }
  end
end
