require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RegistrationsController do
  describe "POST /create" do
    context "with valid params" do
      it "creates an new Customer" do
        Customer.should_receive(:new).and_return(stub(:save => true).as_null_object)
        post :create
      end
      
      it "redirects to the customer profile" do
        customer = Customer.make!
        Customer.stub(:new).and_return(customer)
        post :create
        response.should redirect_to(customer)
      end
      
      it "logs the customer in" do
        customer = Customer.make!
        Customer.stub(:new).and_return(customer)
        post :create
        controller.session[:customer_id].should == customer.id
      end
    end
    
    context "with invalid params" do
      before(:each) do
        Customer.should_receive(:new).and_return(stub(:save => false))
      end
      
      it "render the :new template" do
        post :create
        response.should render_template(:new)
      end
      
      it "shows message error" do
        post :create
        flash[:notice].should == 'Error creating account'
      end
    end
  end
end