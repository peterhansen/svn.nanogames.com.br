require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SessionsController do
  describe "GET /new" do
    it "renders the new template if the user is not signed in" do
      get :new
      response.should render_template(:new)
    end

    it "redirects to profile if the user is signed in" do
      customer = Customer.make!
      controller.stub(:current_customer).and_return(customer)
      get :new
      response.should redirect_to(customer)
    end
  end

  describe "POST /create" do
    context "using an external provider" do
      before(:each) do
        controller.request.env["omniauth.auth"] = {'uid' => '1234', 'provider' => 'nano', 'user_info' => {'name' => 'Ze'}}
      end

      context "that have never been used before" do
        context "when the user is not logged in" do
          before(:each) do
            controller.request.env['omniauth.auth'] = 'something'
            Customer.stub(:authenticate_omniauth).and_raise(ExternalAuthenticationFailed)
          end

          it "creates a new customer and authentication" do
            Customer.should_receive(:create_with_omniauth).and_return(mock_model(Customer, :id => '1234'))
            post :create
          end

          it "logs the new user in" do
            Customer.should_receive(:create_with_omniauth).and_return(mock_model( Customer, :id => '4321'))
            post :create
            controller.session[:customer_id].should == '4321'
          end
        end

        context "when the user is logged in" do
          before(:each) do
            controller.request.env['teste'] = true
            Customer.stub(:authenticate_omniauth).and_raise(ExternalAuthenticationFailed)
            @customer = Customer.make!
            controller.stub(:current_customer).and_return(@customer)
          end

          it "creates a new authentication for the customer" do
            @customer.should_receive(:add_authentication)
            post :create
          end
        end
      end

      context "when the provider has been used before" do
        context "when the user is not logger in" do
          it "logs the user in" do
            Customer.stub(:authenticate_omniauth).and_return(mock_model( Customer, :id => '4321'))
            post :create
            controller.session[:customer_id].should == '4321'
          end
        end

        context "when the user is logger in" do
          it "merge the two customers" do
            @customer = Customer.make!
            controller.stub(:current_customer).and_return(@customer)
            Customer.stub(:authenticate_omniauth).and_return(mock_model( Customer, :id => '4321').as_null_object)
            @customer.should_receive(:merge)
            post :create
          end

          it "logs the user in" do
            controller.session[:customer_id] == nil
            Customer.stub(:authenticate_omniauth).and_return(mock_model( Customer, :id => '4321'))
            post :create
            controller.session[:customer_id].should == '4321'
          end

          it "should redirect to profile" do
            @customer = Customer.make!
            controller.stub(:current_customer).and_return(@customer)
            Customer.stub(:authenticate_omniauth).and_return(mock_model( Customer, :id => '4321').as_null_object)
            post :create
            response.should redirect_to @customer
          end
        end
      end

      context "when the provider does not return an uid" do
        before(:each) do
          controller.request.env["omniauth.auth"] = nil
        end

        it "renders the login template" do
          post :create
          response.should render_template(:new)
        end

        it "shows error message" do
          post :create
          flash[:notice].should == 'Incorrect nickname or password.'
        end
      end
    end

    context "using username and password" do
      context "with valid credentials" do
        it "logs the user in" do
          Customer.stub(:authenticate_omniauth).and_return(mock_model( Customer, :id => '4321'))
          post :create
          controller.session[:customer_id].should == '4321'
        end
      end

      context "with invalid credentials" do
        before(:each) do
          Customer.stub(:authenticate_omniauth).and_raise(CustomerNotFound)
        end

        it "renders the login template" do
          post :create
          response.should render_template(:new)
        end

        it "shows error message" do
          post :create
          flash[:notice].should == 'Incorrect nickname or password.'
        end
      end
    end
  end

  describe "GET /destroy" do
    it "deletes the session" do
      controller.session[:customer_id] = '4321'
      get :destroy
      controller.session[:customer_id].should be_nil
    end
  end
end