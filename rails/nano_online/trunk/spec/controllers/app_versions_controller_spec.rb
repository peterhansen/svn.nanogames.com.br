require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe AppVersionsController do
  describe "GET /index" do
    let(:admin) { mock_model(User, :role => :administrator, :layout => 'admin') }
    let(:guest) { mock_model(User, :role => :guest, :layout => 'regular') }

    context "format :html" do
      it "renders the 'index' template if admin is logger" do
        controller.should_receive(:current_user).at_least(1).times.and_return(admin)
        get :index
        response.should render_template('index')
      end

      it "redirect to root if admin is not logger" do
        get :index
        response.should_not render_template('index')
      end
    end

    context "format :json" do
      it "returns json if admin is not logged" do
        app_version = AppVersion.make!
        controller.should_receive(:current_user).at_least(1).times.and_return(guest)
        get :index, :format => :json
        response.body.should == [app_version].to_json(:only => :id, :include => { :app => { :only => :name } })
      end
    end
  end
end