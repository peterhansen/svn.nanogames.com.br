# == Schema Information
#
# Table name: data_services
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)
#  max_speed  :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class DataService < ActiveRecord::Base
  has_and_belongs_to_many :devices
  
  validates_numericality_of :max_speed
end

