class ExternalAuthenticationFailed < StandardError; end

module OmniauthAuthenticatable
  extend ActiveSupport::Concern

  included do
    has_many :authentications,  :dependent => :destroy
  end

  module InstanceMethods
    def add_authentication(omniauth_hash)
      self.authentications.create({
        provider: omniauth_hash['provider'], 
        uid: omniauth_hash['uid'],
        oauth_token: omniauth_hash['credentials']['token'],
        oauth_secret: omniauth_hash['credentials']['secret'] 
      })
    end

    def has_provider?(provider)
      self.authentications.inject(false) { |result, auth| result || (auth.provider == provider.to_s) }
    end

    def merge(customer)
      customer.authentications.each do |auth|
        auth.update_attributes :customer => self
      end
    end
  end

  module ClassMethods
    def authenticate_omniauth(options)
      omniauth_hash, customer_hash = options[:omniauth_hash], options[:customer_hash]

      if omniauth_hash
        provider, uid = omniauth_hash['provider'], omniauth_hash['uid']
        authentication = Authentication.find_by_provider_and_uid( provider, uid )

        if authentication
          return authentication.customer
        else
          raise ExternalAuthenticationFailed
        end
      elsif customer_hash
        nickname, password = customer_hash[:nickname], customer_hash[:password]
        return Customer.authenticate(nickname, password)
      end
    end

    def create_with_omniauth(omniauth_hash)
      Customer.create!({
        :nickname => omniauth_hash['user_info']['name'], 
        :name => omniauth_hash['user_info']['name'],
        :email => omniauth_hash['user_info']['email'], 
        :password => Customer.generate_password, 
        :authentications_attributes => [{ 
          :provider => omniauth_hash['provider'], 
          :uid => omniauth_hash['uid'],
          :oauth_token => omniauth_hash['credentials']['token'],
          :oauth_secret => omniauth_hash['credentials']['secret'] 
        }]
      })
    end
  end
end