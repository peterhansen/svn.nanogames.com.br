# == Schema Information
#
# Table name: apple_download_reports
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)
#  created_at :datetime
#  updated_at :datetime
#

class AppleDownloadReport < ActiveRecord::Base
  has_many :apple_downloads
  
  validates_presence_of :name
  validates_uniqueness_of :name 
  validates_length_of :name, :maximum => 60

  def file=(path)
    report = File.open(path, 'r').readlines
    report.each_with_index do |line, index|
      apple_downloads.build( :fields => line.split("\t") ) unless index == 0
    end
  end
end
