# == Schema Information
#
# Table name: device_integrators
#
#  id            :integer(4)      not null, primary key
#  device_id     :integer(4)      not null
#  integrator_id :integer(4)      not null
#  identifier    :text(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class DeviceIntegrator < ActiveRecord::Base
  belongs_to :device
  belongs_to :integrator
  
  validates_presence_of :device_id
  validates_presence_of :integrator_id
  validates_length_of :identifier, :maximum => 30, :allow_nil => true
end

