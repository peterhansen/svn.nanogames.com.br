# == Schema Information
#
# Table name: download_codes
#
#  id          :integer(4)      not null, primary key
#  code        :text(255)       default(""), not null
#  file        :text            default(""), not null
#  used_at     :datetime
#  download_id :integer(4)      not null
#  created_at  :datetime
#  updated_at  :datetime
#  customer_id :integer(4)
#

require 'digest/sha1'

class DownloadCode < ActiveRecord::Base
  belongs_to :download
  belongs_to :access
  has_many :midlet_reports
  belongs_to :customer

  def initialize( params )
    super( params )
    
    self.code = Digest::SHA1.hexdigest( "#{self.object_id}*\#{@\&KaJDS-\*\&l\=kjjd_as927a#{created_at}" )
  end

end

