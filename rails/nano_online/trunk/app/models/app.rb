# == Schema Information
#
# Table name: apps
#
#  id                      :integer(4)      not null, primary key
#  name                    :text(255)       default(""), not null
#  name_short              :text(255)       default(""), not null
#  description_short       :text
#  description_medium      :text
#  description_long        :text
#  created_at              :datetime
#  updated_at              :datetime
#  apple_title             :string(255)
#  apple_vendor_identifier :string(255)
#  apple_identifier        :string(255)
#

class App < ActiveRecord::Base
  has_and_belongs_to_many :app_categories
  has_and_belongs_to_many :partners
  has_and_belongs_to_many :ad_campaigns
  has_and_belongs_to_many :ads
  has_many :app_versions
  has_many :ranking_entries, :through => :app_versions # TODO necessário?
  has_many :apple_downloads
  has_one :facebook_data
  has_many :facebook_feeds

  validates_presence_of :name, :name_short
  validates_uniqueness_of :name, :case_sensitive => false
  validates_uniqueness_of :name_short, :case_sensitive => false
  validates_length_of :name, :in => 3..50
  validates_length_of :name_short, :is => 4
  validates_length_of :description_short, :maximum => 150, :allow_nil => true
  validates_length_of :description_medium, :maximum => 300, :allow_nil => true
  validates_length_of :description_long, :maximum => 900, :allow_nil => true

  scope :by_device, lambda { |device_id| where(:id => [Device.find(device_id).family.app_versions.map {|av| av.app_id}])}
  scope :by_vendor, lambda { |vendor_id| includes(:app_versions => [:families => [:devices => :vendor]]).where('ven@dors.id' => vendor_id) }
  scope :name_and_id, select([:name, :id])
  scope :with_available_versions, includes(:app_versions).where("app_versions.status = 'Available'")

  def families
    Family.all(:include => { :app_versions => :app }, :conditions => ["apps.id = ?", self.id])
  end

  def devices
    Device.all(:include => { :family => { :app_versions => :app } }, :conditions => ["apps.id = ?", self.id], :order => 'devices.model')
  end

  def vendors
    Vendor.all(:include => { :devices => { :family => { :app_versions => :app } } }, :conditions => ["apps.id = ?", self.id], :order => 'vendors.name')
  end

  def name_clean
    name.gsub( ' ', '_' )
  end

  def find_version_with_support_to(device)
    self.app_versions.each do |version|
      if version.devices.include?(device) && version.state.name == "Available"
        return version
      end
    end
  end
  def customers
    Customer.where("ranking_entries.app_version_id in (#{self.app_version_ids.join(',')})").includes(:ranking_entries).order("customers.created_at")
  end
end
