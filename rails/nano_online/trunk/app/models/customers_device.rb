# == Schema Information
#
# Table name: customers_devices
#
#  device_id   :integer(4)      not null
#  customer_id :integer(4)      not null
#

class CustomersDevice < ActiveRecord::Base
  belongs_to :device
  belongs_to :customer

  validates_presence_of :device_id
  validates_presence_of :customer_id
end

