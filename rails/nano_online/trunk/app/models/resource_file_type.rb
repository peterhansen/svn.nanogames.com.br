# == Schema Information
#
# Table name: resource_file_types
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)     not null
#  client_id  :integer(2)
#  content    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class ResourceFileType < ActiveRecord::Base
  has_many  :resource_type_elements, 
            :dependent => :destroy
            
  has_many  :resource_types, 
            :through => :resource_type_elements
            
  has_many  :resource_files
  
  validates_presence_of :name
  validates_uniqueness_of :name
end

