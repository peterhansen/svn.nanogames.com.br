# == Schema Information
#
# Table name: unit_formats
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class UnitFormat < ActiveRecord::Base
  has_many :ranking_types

  validates_presence_of :name
  validates_uniqueness_of :name
  validates_length_of :name, :maximum => 30
end

