class CustomersDevice < ActiveRecord::Base
  belongs_to :device
  belongs_to :customer

  validates_presence_of :device_id
  validates_presence_of :customer_id
end
