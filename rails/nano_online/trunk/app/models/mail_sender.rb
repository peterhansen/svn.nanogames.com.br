class MailSender < ActionMailer::Base
  def send( recipients, subject, title, message, sent_at = Time.now )
    mail(
      :to => recipients,
      :from => 'noreply@nanogames.com.br',
      :subject => subject,
      :sent_on => sent_at,
      :body => {:title => title, :email => 'sender@yourdomain.com', :message => message}
    )
  end
end
