# == Schema Information
#
# Table name: ad_pieces
#
#  id            :integer(4)      not null, primary key
#  title         :string(255)     not null
#  description   :text
#  ad_channel_id :integer(4)      not null
#  created_at    :datetime
#  updated_at    :datetime
#

class AdPiece < ActiveRecord::Base
  has_and_belongs_to_many :ad_campaigns                       # tested
  has_many :ad_piece_versions, :dependent => :destroy         # tested
  belongs_to :ad_channel                                      # tested
  
  validates_presence_of :ad_channel                           # tested
end

