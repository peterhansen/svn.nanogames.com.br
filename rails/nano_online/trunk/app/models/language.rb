# == Schema Information
#
# Table name: languages
#
#  id              :integer(4)      not null, primary key
#  name            :string(255)     not null
#  created_at      :datetime
#  updated_at      :datetime
#  byte_identifier :integer(4)
#

class Language < ActiveRecord::Base
  has_many :news_feed_translation
  has_many :news_category_translation
end

