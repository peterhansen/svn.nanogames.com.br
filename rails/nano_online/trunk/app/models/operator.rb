# == Schema Information
#
# Table name: operators
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)       default(""), not null
#  created_at :datetime
#  updated_at :datetime
#

class Operator < ActiveRecord::Base
  has_and_belongs_to_many :integrators
  has_and_belongs_to_many :bands
  
  validates_presence_of :name
end

