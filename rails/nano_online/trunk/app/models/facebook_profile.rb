# == Schema Information
#
# Table name: facebook_profiles
#
#  id                   :integer(4)      not null, primary key
#  uid                  :integer(8)      not null
#  customer_id          :integer(4)
#  first_name           :string(255)
#  last_name            :string(255)
#  name                 :string(255)
#  pic_small            :string(255)
#  pic_big              :string(255)
#  pic_square           :string(255)
#  pic                  :string(255)
#  profile_update_time  :datetime
#  timezone             :string(255)
#  religion             :string(255)
#  birthday             :string(255)
#  birthday_date        :string(255)
#  sex                  :string(255)
#  relationship_status  :string(255)
#  significant_other_id :integer(8)
#  political            :string(255)
#  activities           :string(255)
#  interests            :string(255)
#  is_app_user          :boolean(1)
#  music                :string(255)
#  tv                   :string(255)
#  movies               :string(255)
#  books                :string(255)
#  quotes               :string(255)
#  about_me             :string(255)
#  notes_count          :integer(8)
#  wall_count           :integer(8)
#  status               :string(255)
#  online_presence      :string(255)
#  locale               :string(255)
#  proxied_email        :string(255)
#  profile_url          :string(255)
#  pic_small_with_logo  :string(255)
#  pic_big_with_logo    :string(255)
#  pic_square_with_logo :string(255)
#  pic_with_logo        :string(255)
#  allowed_restrictions :string(255)
#  verified             :string(255)
#  profile_blurb        :string(255)
#  username             :string(255)
#  website              :string(255)
#  is_blocked           :boolean(1)
#  created_at           :datetime
#  updated_at           :datetime
#

class FacebookProfile < ActiveRecord::Base
  belongs_to :customer
  validates_uniqueness_of :uid,         :allow_nil => false
  validates_uniqueness_of :customer_id, :allow_nil => true
end

