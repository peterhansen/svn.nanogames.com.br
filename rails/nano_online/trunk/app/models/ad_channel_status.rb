# == Schema Information
#
# Table name: ad_channel_statuses
#
#  id         :integer(4)      not null, primary key
#  status     :string(255)     not null
#  created_at :datetime
#  updated_at :datetime
#

class AdChannelStatus < ActiveRecord::Base
  has_many :ad_channel_versions, :dependent => :destroy       # tested
  has_many :ad_channels, :dependent => :destroy               # tested
  
  validates_presence_of :status                               # tested
end

