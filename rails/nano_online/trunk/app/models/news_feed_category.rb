# == Schema Information
#
# Table name: news_feed_categories
#
#  id         :integer(4)      not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  public     :boolean(1)      default(TRUE)
#

class NewsFeedCategory < ActiveRecord::Base
  has_many :news_feeds, :dependent => :destroy
  has_many :news_category_translations, :dependent => :destroy
  has_and_belongs_to_many :app_versions
  
  def title
    t = self.news_category_translations
    unless t.blank?
      return t.first.title
    else
      return ''
    end
  end

  def title=(title)
    debugger
    t = self.news_category_translations
    unless t.blank?
      t.first.title = title
      t.first.save
    end
  end

  def description
    d = self.news_category_translations

    unless d.blank?
      return d.first.description
    else
      return ""
    end
  end

  def description=(description)
    d = self.news_category_translations

    unless d.blank?
      d.first.description = description
      d.first.save
    end
  end

  def translations
    return self.news_category_translations
  end

  def default_translation
    self.translations.first.title unless self.translations.blank?
  end

end

