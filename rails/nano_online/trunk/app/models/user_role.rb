# == Schema Information
#
# Table name: user_roles
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)
#  layout     :text(255)
#  created_at :datetime
#  updated_at :datetime
#

class UserRole < ActiveRecord::Base
  has_many :users

  validates_presence_of    :name
  validates_presence_of    :layout
  validates_uniqueness_of  :name, :case_sensitive => false

end

