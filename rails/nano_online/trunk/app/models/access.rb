# == Schema Information
#
# Table name: accesses
#
#  id            :integer(4)      not null, primary key
#  user_agent    :text            default(""), not null
#  phone_number  :text(255)
#  ip_address    :text
#  http_path     :text            default(""), not null
#  http_referrer :text
#  customer_id   :integer(4)
#  device_id     :integer(4)
#  created_at    :datetime
#  updated_at    :datetime
#  phone_hash    :string(255)
#

##
# Essa classe armazena informações de uma sessão de usuário, ou seja, de um acesso único.
##
class Access < ActiveRecord::Base
  belongs_to :customer
  belongs_to :device
  has_many :downloads
  has_many :midlet_reports
  has_many :download_codes

  validates_presence_of :user_agent


  def device_model_full()
    return device.nil? ? '' : device.model_full
  end

end

