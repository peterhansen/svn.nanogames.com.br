# == Schema Information
#
# Table name: app_version_updates
#
#  id                     :integer(4)      not null, primary key
#  app_version_id         :integer(4)
#  updated_app_version_id :integer(4)
#  required               :boolean(1)      default(TRUE), not null
#

class AppVersionUpdate < ActiveRecord::Base
  belongs_to :app_version
  belongs_to :updated_app_version, :class_name => 'AppVersion'
end
