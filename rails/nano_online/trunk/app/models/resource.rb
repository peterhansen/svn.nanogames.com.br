# == Schema Information
#
# Table name: resources
#
#  id               :integer(4)      not null, primary key
#  resource_type_id :integer(4)      not null
#  description      :text
#  internal_id      :integer(3)      default(0)
#  title            :string(255)     not null
#  created_at       :datetime
#  updated_at       :datetime
#

class Resource < ActiveRecord::Base
# Id de parâmetro de leitura de dados do gerenciador de recursos: versão do gerenciador. Tipo de dado: string
  PARAM_RESOURCE_MANAGER_VERSION	= 0;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade total de recursos. Formato dos dados:
  # short (quantidade total de recursos)
  # leitura dos N recursos
  PARAM_RESOURCES_TOTAL						= 1;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: tamanho dos dados (em bytes).
  # Formato dos dados: int, depois os n bytes indicados pelo tamanho.
  PARAM_FILE_DATA_SIZE						= 2;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: tipo do recurso. Formato do dado: short
  PARAM_RESOURCE_TYPE							= 3;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: id do recurso. Formato do dado: int
  PARAM_RESOURCE_ID								= 4;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade de recursos "filhos".
  # Formato dos dados: short indicando a quantidade, e então N * int ids dos filhos.
  PARAM_N_CHILDREN								= 5;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade de recursos órfãos.
  # Esse parâmetro é necessário quando ocorre de um recurso ser necessário mas não estar disponível, e o cliente então
  # informa ao servidor os ids desses recursos para que seja refeito o download.
  # Formato dos dados: short indicando a quantidade, e então N * int ids dos filhos.
  PARAM_ORPHAN_RESOURCES					= 6;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: fim dos dados. Tipo dos dados: nenhum.
  PARAM_RESOURCE_MANAGER_END			= 7;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade total de arquivos (recursos filhos).
  # Formato dos dados:
  # short (quantidade total de arquivos)
  # leitura dos N arquivos
  PARAM_FILES_TOTAL								= 8;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: fim dos dados de um recurso. Formato dos dados: nenhum.
  PARAM_RESOURCE_END							= 9;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: fim dos dados de um arquivo. Formato dos dados: nenhum.
  PARAM_FILE_END									= 10;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: tipo do arquivo. Formato do dado: short
  PARAM_FILE_TYPE									= 11;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: id do arquivo. Formato do dado: int
  PARAM_FILE_ID										= 12;

  # Id de parâmetro de leitura de dados do gerenciador de recursos: identificador interno em relação ao recurso pai. Formato do dado: short
  PARAM_RESOURCE_INTERNAL_ID			= 13;

  ID_RESOURCE_DATA         = -116

  # atributos: description(text)
  
    
  belongs_to              :resource_type                                # tested
  has_many                :resource_files, :dependent => :destroy       # tested
  has_and_belongs_to_many :ad_piece_versions                            # tested

  validates_presence_of   :resource_type                                # tested
  
  before_save             :validate_type_restrictions

	
  def files=(attributes)
    attributes.each do |file|
      self.resource_files.build(file) unless file[:file].blank?
    end
  end


  def validate()
#    if self.resource_files.blank?
#      errors.add(:resource_files, "field is blank.")
#    end
  end


  def validate_type_restrictions()
		files = {}
		self.resource_files.each do |file|
			if ( files.include?( file.file_type.id ) )
				files[file.file_type.id] += 1
			else
				files[file.file_type.id] = 1
			end
		end

    self.resource_type.resource_type_elements.each do |element|
			file = files[element.resource_file_type.id]

      if ( file && ( file > element.quantity ) )
        errors.add(:resource, "has too many #{element.resource_file_type.name} files.")
        return false
      end
    end
  end


	##
	#
	##
	def self.add_resources_to_send( resources, params )
		unless ( params[ ID_RESOURCE_DATA ] )
			params[ ID_RESOURCE_DATA ] = {}
		end
		
		if ( params[ ID_RESOURCE_DATA ][ :resources_to_send ].blank? )
			params[ ID_RESOURCE_DATA ][ :resources_to_send ] = []
		end

		params[ ID_RESOURCE_DATA ][ :resources_to_send ] << resources
	end


  ##
  #
  ##
  def self.send_data( out, params )
    data = params[ ID_RESOURCE_DATA ]

    unless ( data.blank? || data[ :resources_to_send ].blank? )
      out.write_byte( ID_RESOURCE_DATA )

			# remove recursos duplicados
			data[ :resources_to_send ].flatten!.uniq!

			resources = Resource.find_all_by_id( data[ :resources_to_send ] )
			# armazena as informações dos filhos primeiro, para depois enviá-los de uma só vez, evitando duplicações
			all_files = []

			# quantidade total de recursos
			out.write_byte( PARAM_RESOURCES_TOTAL )
			out.write_short( resources.length )

			resources.each do |r|
				# tipo do recurso
				out.write_byte( PARAM_RESOURCE_TYPE )
				out.write_short( r.resource_type.client_id )

				# id do recurso
				out.write_byte( PARAM_RESOURCE_ID )
				out.write_int( r.id )

				# escreve os ids dos arquivos (filhos)
				files = r.resource_files
				out.write_byte( PARAM_N_CHILDREN )
				out.write_short( files.length )

				files.each do |f|
					all_files << f
					out.write_int( f.id )
				end
				
				out.write_byte( PARAM_RESOURCE_END )
			end

			# remove os arquivos duplicados
			all_files.uniq!

			# quantidade total de arquivos
			out.write_byte( PARAM_FILES_TOTAL )
			out.write_short( all_files.length )

			all_files.each { |f|
				# tipo do arquivo
				out.write_byte( PARAM_FILE_TYPE )
				out.write_short( f.resource_file_type.client_id )

				# id do arquivo
				out.write_byte( PARAM_FILE_ID )
				out.write_int( f.id )

				# dados do arquivo
				out.write_byte( PARAM_FILE_DATA_SIZE )
				out.write_int( f.file.size )
				out.write_byte_array( File.open( f.file.path ).to_a )

				out.write_byte( PARAM_FILE_END )
			}

      # tag de fim das informações do gerenciador de recursos
      out.write_byte( PARAM_RESOURCE_MANAGER_END )
    end
  end

end

