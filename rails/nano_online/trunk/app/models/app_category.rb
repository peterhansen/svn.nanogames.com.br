# == Schema Information
#
# Table name: app_categories
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)       default(""), not null
#  created_at :datetime
#  updated_at :datetime
#

class AppCategory < ActiveRecord::Base
  has_and_belongs_to_many :apps
  
  validates_presence_of :name
  validates_length_of :name, :maximum => 20
end

