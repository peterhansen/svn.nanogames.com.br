# == Schema Information
#
# Table name: optional_apis
#
#  id          :integer(4)      not null, primary key
#  name        :text(255)       default(""), not null
#  description :text
#  jsr_index   :integer(2)
#  created_at  :datetime
#  updated_at  :datetime
#

class OptionalApi < ActiveRecord::Base
  has_and_belongs_to_many :devices

  validates_presence_of :name
  validates_length_of :name, :in => 5..30
  validates_length_of :description, :in => 5..100, :allow_nil => true
  validates_numericality_of :jsr_index, :allow_nil => true
end

