# == Schema Information
#
# Table name: app_version_states
#
#  id         :integer(4)      not null, primary key
#  name       :string(20)      not null
#  code       :integer(4)      not null
#  created_at :datetime
#  updated_at :datetime
#

class AppVersionState < ActiveRecord::Base
  has_many :app_versions
  
  validates_presence_of :name, :code
  validates_uniqueness_of :name, :code
  validates_length_of :name, :maximum => 20
end

