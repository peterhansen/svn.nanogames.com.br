# == Schema Information
#
# Table name: app_versions
#
#  id                   :integer(4)      not null, primary key
#  app_id               :integer(4)      not null
#  release_notes        :text
#  number               :text(255)       default(""), not null
#  app_version_state_id :integer(4)      default(0)
#  created_at           :datetime
#  updated_at           :datetime
#  status               :string(255)
#

class AppVersion < ActiveRecord::Base
  require 'find'

  STATUSES = %w{Unavailable Available Deprecated}

  belongs_to :app
  # belongs_to :app_version_state
  has_and_belongs_to_many :families
  has_and_belongs_to_many :submissions
  has_and_belongs_to_many :customers
  has_and_belongs_to_many :ad_channel_versions
  has_and_belongs_to_many :ranking_types
  has_many :ranking_entries
  has_many :downloads
  has_many :midlet_reports
  has_many :app_customer_datas, :dependent => :destroy
  has_many :app_files, :dependent => :destroy
  has_many :integrators, :through => :submissions
  has_many :app_version_updates, :class_name => 'AppVersionUpdate', :foreign_key => 'app_version_id'
  has_many :updated_app_versions, :through => :app_version_updates
  has_many :inverse_app_version_updates, :class_name => 'AppVersionUpdate', :foreign_key => 'updated_app_version_id'
  has_many :previous_app_versions, :through => :inverse_app_version_updates, :source => :app_version
  has_and_belongs_to_many :news_feed_categories

  # TODO garantir que só exista uma única combinação app/número de versão

  validates_presence_of :app_id
  validates_presence_of :number
  validates_length_of :number, :maximum => 10
  validates_format_of :number, :with => /\A\d{1,2}\.\d{1,2}(\.\d{0,2}){0,1}\z/, :message => "must be in format 'xx.yy' or 'xx.yy.zz'"
  validates_length_of :release_notes, :maximum => 2048, :allow_nil => true

  scope :by_device, lambda { |device_id| where(:id => [Device.find(device_id).family.app_version_ids]).includes(:app) }

  def validate
    app_versions = AppVersion.all( :conditions => { :app_id => self.app_id, :number => self.number } ) - [ self ]

    unless app_versions.blank?
      errors.add( "#{self.app.name} #{self.number} already exists." )
    end
  end

  def last_version
    update = last_update

    if update
      update.updated_app_version
    else
      self
    end
  end

  def last_update
    update = nil

    app_version_updates.each do |u|
      av = u.updated_app_version

      uav = av.last_update
      if uav
        av = uav.updated_app_version
      end

      # se for a primeira atualização encontrada ou seja mais recente que a anterior, armazena em update
      if ( ( av && av.status == 'Available' ) &&
            ( update.nil? || Util::natcmp( update.updated_app_version.number, av.number ) > 0 ) )
        update = u
      end
    end

    update
  end

  def devices
    Device.all( :conditions => { 'family_id' => families } )
  end


  def file_jad
    app_files.each do |file|
      return file if file.filename.relative_path.split('.').last == 'jad'
    end

    nil
  end

  def file_bt_cod
    app_files.each do |file|
      if file.filename.basename[0..2] == "bt_"
        return file
      end
    end

    nil
  end

  def app_version_number
    if self.app
      self.app.name + ' ' + number
    else
      ""
    end
  end

  def app_version_number_clean
    app_version_number().gsub( ' ', '_' )
  end

  # def state
  #   self.app_version_state
  # end

  # def state=(state)
  #   self.app_version_state = state
  # end

  def full_name
    if self.app
      "#{self.app.name} - #{self.number}"
    else
      self.number
    end
  end
end
