# == Schema Information
#
# Table name: undetected_user_agents
#
#  id          :integer(4)      not null, primary key
#  user_agent  :text            default(""), not null
#  accesses    :integer(2)      default(1), not null
#  created_at  :datetime
#  updated_at  :datetime
#  wap_profile :string(255)
#

class UndetectedUserAgent < ActiveRecord::Base
  attr_accessor :wap_profile

  validates_uniqueness_of :user_agent
  validates_presence_of :user_agent

  # TODO armazenar página que cliente tentou acessar?
end


