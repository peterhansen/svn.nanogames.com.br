# == Schema Information
#
# Table name: authentications
#
#  id           :integer(4)      not null, primary key
#  customer_id  :integer(4)
#  provider     :string(255)
#  uid          :string(255)
#  oauth_token  :string(255)
#  oauth_secret :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class Authentication < ActiveRecord::Base
	attr_accessible :provider, :uid, :oauth_token, :oauth_secret, :customer
	belongs_to :customer
end
