# == Schema Information
#
# Table name: ranking_types
#
#  id                :integer(4)      not null, primary key
#  name              :string(255)
#  description       :text
#  unit_format_id    :integer(4)
#  sub_type          :integer(4)      default(0), not null
#  minimum           :integer(4)
#  maximum           :integer(4)
#  created_at        :datetime
#  updated_at        :datetime
#  ranking_system_id :integer(4)
#

class RankingType < ActiveRecord::Base
  has_and_belongs_to_many :app_versions
  has_many :ranking_entries

  belongs_to :unit_format
  belongs_to :ranking_system

  validates_presence_of :name, :description, :unit_format, :sub_type, :ranking_system

  validates_length_of :name, :maximum => 60
  validates_length_of :description,:maximum => 255

  validates_numericality_of :sub_type, :less_than => 256
end