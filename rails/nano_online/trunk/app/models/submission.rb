# == Schema Information
#
# Table name: submissions
#
#  id            :integer(4)      not null, primary key
#  integrator_id :integer(4)      not null
#  created_at    :datetime
#  updated_at    :datetime
#

#libs usadas pelo rubyzip
require 'zip/zip'
require 'zip/zipfilesystem'
require 'util/jad_file'

class Submission < ActiveRecord::Base
  belongs_to :integrator
  has_and_belongs_to_many :app_versions

  #a tabela de relacionamento com os aparelhos pode parecer redundante, uma vez que a lista
  #de aparelhos suportados pode ser obtida através de app_version. Porém, ela é necessária
  #para que possa ser obtida a relação de futuros aparelhos adicionados às famílias
  #suportadas pela versão do aplicativo, mas que não eram suportados no momento da submissão
  #Logo, pode-se gerar a lista de aparelhos que deveriam estar no ar, mas que não foram
  #informados é integradora.
  has_and_belongs_to_many :devices

#  validates_presence_of :app_version_id
  validates_presence_of :integrator_id

  def update_devices
    #remove os antigos aparelhos da lista
    devices.delete_all()

    for app_version in app_versions
      for family in app_version.families
        for device in family.devices
          devices << device
        end
      end
    end
  end


  #gera a documentação e estrutura de pastas de acordo com o integrador
  def generate_zip
    case integrator.name.downcase!
      when 'dada.net'
        generate_dadanet()
      when 'supportcomm'
        generate_supportcomm()
      when 'acotel'
        generate_acotel()
      when 'playphone'
        generate_playphone()
      when 'vivo'
        generate_vivo()
      when 'm4u'
        generate_m4u()
      when 'buongiorno'
        generate_buongiorno()
      when 'okto'
        generate_okto()
    end #fim case
  end

  def zipped
    require 'zip/zip'
    require 'zip/zipfilesystem'
    dir = "#{::PATH_SUBMISSIONS}/#{self.integrator.name.downcase}/#{self.id}"
    temp = Tempfile.new("#{Rails.root}/tmp/submission_#{Time.now.to_i}")

    begin
      list = Find.find(dir).to_a
      files = list.delete_if {|file| !File.file?(file)}

      Zip::ZipOutputStream.open(temp.path) do |zip|
        files.each do |file|
          zip.put_next_entry(file.split('/').last)
          zip.print IO.read(temp.path)
        end
      end

      temp.path
    rescue
      return nil
    end
  end

  ################################ métodos protected ###########################
  protected

  def erase_temp_dir( temp_dir )
    Find.find( temp_dir ) do |path|
      if ( FileTest.directory?( path ) )
        next
      else
        begin
          File.delete( path )
        rescue Exception
        end
      end
    end
  end

  def generate_dadanet
    path = "#{PATH_SUBMISSIONS}/#{integrator.name}"
    path_temp = "#{path}/temp"
    zip_filename = "#{path}/#{id}/games.zip"

    #apaga o arquivo anterior, caso exista
    if ( File.file?( zip_filename ) )
      File.delete( zip_filename )
    end

    FileUtils.mkdir_p( path )
    FileUtils.mkdir_p( path_temp )
    FileUtils.mkdir_p( "#{path}/#{id}" )

    #cria o arquivo zip maior
    Zip::ZipFile.open( zip_filename, Zip::ZipFile::CREATE ) {
      |zipfile|

      for app_version in app_versions
        app_name = app_version.app.name.gsub( /\W/, '_' )

        # cria um arquivo temporário para cada versão
        zip_temp = "#{path_temp}/temp#{app_version.id}.zip"
        begin
          File.delete( zip_temp )
        rescue Exception
        end
        Zip::ZipFile.open( zip_temp, Zip::ZipFile::CREATE ) {
          |app_zip|

          jad = JadFile.new( app_version.file_jad.filename.path )
          original_jar_url = "#{app_version.file_jad.filename.dir}/#{jad.info[ 'MIDlet-Jar-URL' ]}"

          if ( File.exists? original_jar_url )
            jad.info[ 'MIDlet-Jar-URL' ] = "#{ app_name }.jar"
            jad.save_to( "#{path_temp}/temp.jad" )

            app_zip.add( "#{ app_name }.jad", "#{path_temp}/temp.jad" )
            app_zip.add( "#{ app_name }.jar", original_jar_url )
          end
        }

        #cria o primeiro subdiretário do .zip
        begin
          zipfile.mkdir( "#{ app_name }" )
        rescue Exception
        end

        #varre os aparelhos suportados
        app_version.families.each do |family_id|
          supported_devices = Device.find_all_by_family_id( family_id )
          supported_devices.each do |supported_device|
            device_integrator = DeviceIntegrator.find( :first, :conditions => [ "device_id = :device and integrator_id = :integrator",
                                                                { :device => supported_device, :integrator => integrator } ] )
            if ( device_integrator )
              device_path = "#{app_name}/App#{ device_integrator.identifier }"
              begin
                zipfile.mkdir( device_path )
              rescue Exception
                puts "error creating dir: #{device_path}"
                next
              end

              zipfile.add( "#{ device_path }/#{ app_name }.zip", zip_temp )
            end # fim if ( device_integrator )
          end # fim temp.each do |t|
        end # fim app_version.families.each do |family_id|
      end # fim for app_version in app_versions
    }

    erase_temp_dir( path_temp )
  end #fim generate_dadanet()


  ##
  #
  ##
  def generate_supportcomm()
    dir = "#{PATH_SUBMISSIONS}/#{integrator.name}/#{id}"
    dir_files = "#{dir}/files"
    FileUtils.mkdir_p( dir_files )

    filename = "#{dir}/Compat_JogosxDevices.txt"
    file_list = "#{dir}/list_by_vendor.txt"

    #apaga o arquivo anterior, caso exista
    if ( File.file?( filename ) )
      File.delete( filename )
    end

    #apaga o arquivo anterior, caso exista
    if ( File.file?( file_list ) )
      File.delete( file_list )
    end

    version_list = {}
    vendors_list = {}
    total = 0

    for app_version in app_versions
      version_list[ app_version ] = {}

      #varre as famílias suportadas
      app_version.families.each do |family_id|
        supported_devices = Device.find_all_by_family_id( family_id )

        #varre os aparelhos da família atual
        supported_devices.each do |device|
          if ( version_list[ app_version ][ device.vendor.name ].nil? )
            version_list[ app_version ][ device.vendor.name ] = []
          end

          if ( vendors_list[ device.vendor.name ].nil? )
            vendors_list[ device.vendor.name ] = []
          end

          vendors_list[ device.vendor.name ] << "#{device.model}"
          version_list[ app_version ][ device.vendor.name ] << "#{device.model}"
          total += 1
        end # fim supported_devices.each do |supported_device|
      end # fim app_version.families.each do |family_id|
    end # fim for app_version in app_versions

    File.open( filename, 'w' ) do |file|
      file.puts "TOTAL: #{total} APARELHOS\n\n"

      version_list.each { |version, list|
        sorted_vendors = list.sort

        file.print "#{ version.file_jad.nil? ? version.number : version.file_jad.filename}\t\""
        sorted_vendors.each { |vendor, devices|
          sorted_devices = devices.sort
          file.print "\r#{vendor}: "
          file.print( sorted_devices.join( ', ' ) )
          file.print "\r"
        }
        file.puts "\""
      }
    end


    File.open( file_list, 'w' ) do |file|
      file.puts "TOTAL: #{total} APARELHOS\n\n"

      vendors_list2 = vendors_list.to_a.sort

      vendors_list2.each { |vendor, list|
        sorted_devices = list.sort
        file.print "\r#{vendor}: "

        sorted_devices = list.sort
        file.print( sorted_devices.join( ', ' ) )
        file.puts "\r\n"
      }
    end


  end


  def generate_acotel()
  end


  def generate_playphone()
    dir = "#{PATH_SUBMISSIONS}/#{integrator.name}/#{id}"
    dir_files = "#{dir}/files"
    FileUtils.mkdir_p( dir_files )

    filename = "#{dir}/Compat_JogosxDevices.txt"

    #apaga o arquivo anterior, caso exista
    if ( File.file?( filename ) )
      File.delete( filename )
    end

    File.open( filename, 'w' ) do |file|
      file.puts( "ID_Aparelho\tFabricante\tModelo\tArquivo do Jogo\tObservacoes" )

      for app_version in app_versions
        File.copy( app_version.file_jad.path, "#{dir_files}/#{app_version.file_jad.filename}", true )
        File.copy( app_version.file_jar.path, "#{dir_files}/#{app_version.file_jar.filename}", true )

        #varre as famílias suportadas
        app_version.families.each do |family_id|
          supported_devices = Device.find_all_by_family_id( family_id )

          #varre os aparelhos da família atual
          supported_devices.each do |supported_device|
            device_integrator = DeviceIntegrator.find( :first, :conditions => [ "device_id = :device and integrator_id = :integrator",
                                                                { :device => supported_device, :integrator => integrator } ] )
            if ( device_integrator )
              file.puts( "#{device_integrator.identifier}\t#{supported_device.vendor.name}\t#{supported_device.model}\t#{app_version.file_jad.filename}\t" )
            end # fim if ( device_integrator )
          end # fim supported_devices.each do |supported_device|
        end # fim app_version.families.each do |family_id|
      end # fim for app_version in app_versions
    end
  end


  def generate_vivo()
  end


  def generate_m4u()
  end


  def generate_buongiorno()
  end


  def generate_okto()
  end


end

