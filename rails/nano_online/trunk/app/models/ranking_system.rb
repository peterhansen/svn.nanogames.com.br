# == Schema Information
#
# Table name: ranking_systems
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class RankingSystem < ActiveRecord::Base
  has_many :ranking_types
end

