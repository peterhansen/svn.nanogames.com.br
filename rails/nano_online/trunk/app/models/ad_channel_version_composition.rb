# == Schema Information
#
# Table name: ad_channel_version_compositions
#
#  id                    :integer(4)      not null, primary key
#  ad_channel_version_id :integer(4)      not null
#  resource_type_id      :integer(4)      not null
#  code                  :integer(4)      not null
#  created_at            :datetime
#  updated_at            :datetime
#

class AdChannelVersionComposition < ActiveRecord::Base
  belongs_to :ad_channel_version
  belongs_to :resource_type
end

