# == Schema Information
#
# Table name: ad_channels
#
#  id                   :integer(4)      not null, primary key
#  title                :string(255)     not null
#  description          :text            default(""), not null
#  ad_channel_status_id :integer(4)      not null
#  created_at           :datetime
#  updated_at           :datetime
#

class AdChannel < ActiveRecord::Base
  has_many    :ad_pieces,            :dependent => :destroy     # tested
  has_many    :ad_channel_versions,  :dependent => :destroy     # tested
  belongs_to  :ad_channel_status                                # tested
  
  validates_presence_of :title,                                 # tested
                        :description,                           # tested
                        :ad_channel_status                      # tested
end

