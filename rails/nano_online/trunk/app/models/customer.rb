# == Schema Information
#
# Table name: customers
#
#  id              :integer(4)      not null, primary key
#  nickname        :text(255)       default(""), not null
#  first_name      :text(255)
#  last_name       :text(255)
#  email           :text(255)
#  phone_number    :text(255)
#  birthday        :date
#  cpf             :text(255)
#  genre           :string(1)
#  hashed_password :string(255)
#  salt            :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  validation_hash :string(255)
#  validate_before :datetime
#  validated_at    :datetime
#  show_birthday   :boolean(1)
#

PARAM_REGISTER_NICKNAME          = 0
PARAM_REGISTER_FIRST_NAME        = 1
PARAM_REGISTER_LAST_NAME         = 2
PARAM_REGISTER_PHONE_NUMBER      = 3
PARAM_REGISTER_EMAIL             = 4
PARAM_REGISTER_CPF               = 5
PARAM_REGISTER_PASSWORD          = 6
PARAM_REGISTER_PASSWORD_CONFIRM  = 7

# indica o fim da leitura ou gravação dos dados do usuário
PARAM_REGISTER_INFO_END          = 8

PARAM_REGISTER_GENRE             = 9
PARAM_REGISTER_BIRTHDAY          = 10

PARAM_REGISTER_CUSTOMER_ID       = 11
PARAM_REGISTER_TIMESTAMP         = 12

PARAM_REGISTER_VALIDATE_BEFORE   = 13
PARAM_REGISTER_VALIDATED_AT      = 14

# O Facebook pode retornar datas incompletas (contendo apenas dd/mm ) de acordo
# com a política de privacidade escolhida pelo usuário. Assim, retornamos apenas
# a data parcial e deixamos a aplicação cliente requisitar a informação que falta
PARAM_REGISTER_BIRTHDAY_STR      = 15

# UID do FaceBook. Tipo do dado: inteiro
PARAM_REGISTER_FB_UID            = 25
# Session key para conexão do FaceBook. Tipo do dado: string
PARAM_REGISTER_FB_SESSION_KEY    = 26
# Segredinho para conexão do FaceBook. Tipo do dado: string
PARAM_REGISTER_FB_SESSION_SECRET = 27

PARAM_LOGIN_NICKNAME             = 0
PARAM_LOGIN_PASSWORD             = 1

##############################################################################
# Códigos de retorno do registro de usuário                                  #
##############################################################################
#
# Código de retorno do registro do usuário: erro: apelido já em uso.
RC_ERROR_NICKNAME_IN_USE			= 1

# Código de retorno do registro do usuário: erro: formato do apelido inválido.
RC_ERROR_NICKNAME_FORMAT			= 2

# Código de retorno do registro do usuário: erro: comprimento do apelido inválido.
RC_ERROR_NICKNAME_LENGTH			= 3

# Código de retorno do registro do usuário: erro: formato do e-mail inválido.
RC_ERROR_EMAIL_FORMAT				= 4

# Código de retorno do registro do usuário: erro: comprimento do e-mail inválido.
RC_ERROR_EMAIL_LENGTH				= 5

# Código de retorno do registro do usuário: erro: password muito fraca.
RC_ERROR_PASSWORD_WEAK			= 6

# Código de retorno do registro do usuário: erro: password e confirmação diferentes.
RC_ERROR_PASSWORD_CONFIRMATION	= 7

# Código de retorno do registro do usuário: erro: comprimento do 1º nome inválido.
RC_ERROR_FIRST_NAME_LENGTH		= 8

# Código de retorno do registro do usuário: erro: comprimento do sobrenome inválido.
RC_ERROR_LAST_NAME_LENGTH			= 9

# Código de retorno do registro do usuário: erro: gênero inválido.
RC_ERROR_GENRE                    = 10

# Código de retorno do registro do usuário: erro: comprimento do gênero inválido.
RC_ERROR_GENRE_LENGTH             = 11

# Código de retorno do registro do usuário: erro: formato do número de telefone inválido.
RC_ERROR_PHONE_NUMBER_FORMAT      = 12

# Código de retorno do registro do usuário: erro: comprimento do número de telefone inválido.
RC_ERROR_PHONE_NUMBER_LENGTH      = 13

# Código de retorno do registro do usuário: erro: CPF inválido.
RC_ERROR_CPF_FORMAT      = 14

# Código de retorno do registro do usuário: erro: e-mail já em uso.
RC_ERROR_EMAIL_ALREADY_IN_USE		  = 15


##############################################################################
# Códigos de retorno do login de usuário                                     #
##############################################################################

# Código de retorno do login de usuário: apelido não encontrado.
RC_ERROR_LOGIN_NICKNAME_NOT_FOUND       = 1

# Código de retorno do login de usuário: password incorreta.
RC_ERROR_LOGIN_PASSWORD_INVALID       = 2

class CustomerNotFound < StandardError; end

class Customer < ActiveRecord::Base
  include DataParser, OmniauthAuthenticatable

  # tempo que o jogador tem para validar seu e-mail caso tenha criado ou alterado seu perfil a partir de um aplicativo
  VALIDATION_TIME_MOBILE  = 3.days
  # tempo que o jogador tem para validar seu e-mail caso tenha criado ou alterado seu perfil a partir da web
  VALIDATION_TIME_WEB     = 0

  has_and_belongs_to_many :app_versions
  has_and_belongs_to_many :devices
  has_many :ranking_entries,  :dependent => :destroy
  has_many :accesses,         :dependent => :nullify
  has_many :downloads,        :dependent => :nullify
  has_many :download_codes,   :dependent => :nullify
  has_one  :facebook_profile, :dependent => :destroy
  has_many :app_customer_datas, :dependent => :destroy

  accepts_nested_attributes_for :authentications
  attr_accessible :nickname, :email, :authentications_attributes, :password, :password_confirmation

  validates :nickname, :uniqueness => true, :length => { :within => 2..20 }
  validates :password, :confirmation => true
  validate :validate_password, :on => :create

  validates_format_of :email,
    :allow_nil =>   true,
    :allow_blank => true,
    :with =>        /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i,
    :message =>     "the format is invalid (#{RC_ERROR_EMAIL_FORMAT})"

  validates_length_of :email,
    :in =>          6..100,
    :allow_nil =>   true,
    :allow_blank => true,
    :message =>     "must contain between 6 and 100 characters (#{RC_ERROR_EMAIL_LENGTH})"

  validates_length_of :phone_number,
    :in =>          8..20,
    :allow_nil =>   true,
    :allow_blank => true,
    :message =>     "must contain between 8 and 20 digits (#{RC_ERROR_PHONE_NUMBER_LENGTH})"

  validates_length_of :first_name,
    :maximum => 100,
    :allow_nil => true,
    :allow_blank => true,
    :message => "Your full name is too big. Please use less surnames. (#{RC_ERROR_FIRST_NAME_LENGTH})"

  validates_length_of :last_name,
    :maximum => 30,
    :allow_nil => true,
    :allow_blank => true,
    :message => "(#{RC_ERROR_LAST_NAME_LENGTH})"

  # validates_confirmation_of :password

  before_save :default_values
  before_save :check_email_validation
  after_save  :validate_email

  def self.find_all_by_app(app)
    Customer.where("ranking_entries.app_version_id in (#{app.app_version_ids.join(',')})").includes(:ranking_entries).order("customers.created_at")
  end

  def self.export_to_csv_by_app(app)
    customers = find_all_by_app(app)
    CSV.generate(:col_sep => ";") do |csv|
      csv << ['Name', 'Nickname', 'Email', 'Birthday']
      customers.each do |customer| 
        fields = [customer.first_name, customer.nickname, customer.email]
        fields << customer.birthday.strftime("%d/%m/%y") if customer.birthday
        csv << fields
      end
    end
  end

  # Cria um novo cliente a partir dos parâmetros para criação de um novo cliente,
  # fazendo o parser dos parâmetros binários e retornando a referência para o novo
  # cliente criado (ainda não salvo).
  def self.filter_new( params, value = nil )
    customer = Customer.new
    customer.mobile_client = true
    @send_validation_email = params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31'

    customer.parse_data( params[ ::ID_SPECIFIC_DATA ], value ) { |stream, id|
      case id
        when PARAM_REGISTER_EMAIL
          customer.email = stream.read_string()
        when PARAM_REGISTER_NICKNAME
          customer.nickname = stream.read_string()
        when PARAM_REGISTER_PASSWORD
          customer.password = stream.read_string()
          customer.password_confirmation = customer.password
        when PARAM_REGISTER_PASSWORD_CONFIRM
          customer.password_confirmation = stream.read_string()
        when PARAM_REGISTER_CPF
          customer.cpf = stream.read_string()
        when PARAM_REGISTER_FIRST_NAME
          customer.first_name = stream.read_string()
        when PARAM_REGISTER_LAST_NAME
          customer.last_name = stream.read_string()
        when PARAM_REGISTER_PHONE_NUMBER
          customer.phone_number = stream.read_string()
        when PARAM_REGISTER_GENRE
          customer.genre = stream.read_char()
        when PARAM_REGISTER_BIRTHDAY
          customer.birthday = stream.read_datetime()
        when PARAM_REGISTER_FB_UID
          fb_uid = stream.read_long();
        when PARAM_REGISTER_FB_SESSION_KEY
          fb_session_key = stream.read_string();
        when PARAM_REGISTER_FB_SESSION_SECRET
          fb_session_secret = stream.read_string();
        when PARAM_REGISTER_INFO_END
          break
        else
          raise ArgumentError, "Unknown customer parameter (insert)", caller
      end
    }

    return customer#, fb_profile;
  end

  def self.generate_password(length = 7)
    charset = %w{2 3 4 6 7 8 9 A C D E F G H J K L M N P Q R T V W X Y Z}
    new_password = ""

    while !(new_password =~ /\d/)
      new_password = (0...length).map{charset.to_a[rand(charset.size)]}.join
    end
    new_password
  end

  def password
    @password
  end

  def password=(pwd)
    unless pwd.blank?
      @password = pwd
      create_new_salt()
      self.hashed_password = Customer.encrypted_password( self.password, self.salt )
    end
  end

  def self.create_validation_hash( customer )
    return Digest::SHA1.hexdigest("#{customer.object_id}*\#{@\&KaJDS-#{customer.nickname}\*\&l\=#{customer.email}kjjd_as927a#{customer.created_at}")
  end

  ##
  # Escreve os dados do usuário num OutputStream.
  ##
  def write_data( out, params )
    out.write_byte( PARAM_REGISTER_CUSTOMER_ID )
    out.write_int( self.id )
    out.write_byte( PARAM_REGISTER_EMAIL )
    out.write_string( self.email )
    out.write_byte( PARAM_REGISTER_NICKNAME )
    out.write_string( self.nickname )
    out.write_byte( PARAM_REGISTER_CPF )
    out.write_string( self.cpf )
    out.write_byte( PARAM_REGISTER_FIRST_NAME )
    out.write_string( self.first_name )
    out.write_byte( PARAM_REGISTER_LAST_NAME )
    out.write_string( self.last_name )

    out.write_byte( PARAM_REGISTER_PHONE_NUMBER )
    out.write_string( self.phone_number )

    # verifica versão do Nano Online cliente
    if ( params[ ID_NANO_ONLINE_VERSION ] >= '0.0.3' )
      out.write_byte( PARAM_REGISTER_GENRE )
      out.write_char( self.genre )

      out.write_byte( PARAM_REGISTER_BIRTHDAY )
      out.write_datetime( self.birthday )

      if ( params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31' )
        out.write_byte( PARAM_REGISTER_VALIDATE_BEFORE )
        out.write_datetime( self.validate_before )
        out.write_byte( PARAM_REGISTER_VALIDATED_AT )
        out.write_datetime( self.validated_at )
#        out.write_byte( PARAM_REGISTER_FB_UID )
      end
    end

    out.write_byte( PARAM_REGISTER_INFO_END )
  end

  ##
  # Atualiza os dados de um usuário a partir de parâmetros binários passados por uma aplicação Nano.
  ##
  def self.update_binary( binary_params, value = nil )
    customer_data = {}
    customer_id = -1
    customer_timestamp = -1
    @send_validation_email = binary_params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31'

    parse_data( binary_params[ ::ID_SPECIFIC_DATA ], value ) { |stream, id|
      case id
        when PARAM_REGISTER_EMAIL
          customer_data[ :email ] = stream.read_string()
        when PARAM_REGISTER_NICKNAME
          customer_data[ :nickname ] = stream.read_string()
        when PARAM_REGISTER_PASSWORD
          customer_data[ :password ] = stream.read_string()
          customer_data[:password_confirmation] = customer_data[:password]
        when PARAM_REGISTER_PASSWORD_CONFIRM
          customer_data[ :password_confirmation ] = stream.read_string()
        when PARAM_REGISTER_CPF
          customer_data[ :cpf ] = stream.read_string()
        when PARAM_REGISTER_FIRST_NAME
          customer_data[ :first_name ] = stream.read_string()
        when PARAM_REGISTER_LAST_NAME
          customer_data[ :last_name ] = stream.read_string()
        when PARAM_REGISTER_PHONE_NUMBER
          customer_data[ :phone_number ] = stream.read_string()
        when PARAM_REGISTER_GENRE
          customer_data[ :genre ] = stream.read_char()
        when PARAM_REGISTER_BIRTHDAY
          customer_data[ :birthday ] = stream.read_datetime()
        when PARAM_REGISTER_CUSTOMER_ID
          customer_id = stream.read_int()
        when PARAM_REGISTER_VALIDATED_AT
          customer_data[ :validated_at ] = stream.read_datetime()
        when PARAM_REGISTER_VALIDATE_BEFORE
          customer_data[ :validate_before ] = stream.read_datetime()
        when PARAM_REGISTER_TIMESTAMP
          # TODO tratar timestamp do usuário - se informações já estiverem atualizadas, não há necessidade de reenviá-las
          customer_timestamp = stream.read_datetime()
        when PARAM_REGISTER_FB_UID
          fb_uid = stream.read_long();
        when PARAM_REGISTER_FB_SESSION_KEY
          fb_session_key = stream.read_string();
        when PARAM_REGISTER_FB_SESSION_SECRET
          fb_session_secret = stream.read_string();
        when PARAM_REGISTER_INFO_END
          break
        else
          raise ArgumentError, "Unknown customer parameter (update)", caller
      end
    }

    if ( customer_id >= 0 )
      customer = Customer.find( customer_id )
    else
      # trata clientes Nano Online < 0.0.3
      customer = Customer.find( binary_params[ :customer ] )
    end

    unless( customer.nil? )
      customer.mobile_client = true

      if customer.update_attributes( customer_data )
        # customer.send_email_validation
        @email_changed = false
        return true, customer
      else
        return false, customer
      end

#      return customer.update_attributes( customer_data ), customer
    end

    return false, nil
  end

  def self.authenticate( nickname_or_email, password )
    customer = where("nickname = ? or email = ?", nickname_or_email, nickname_or_email).first

    if customer && customer.check_password( password )
      return customer
    else
      raise CustomerNotFound
    end
  end

  def check_password( password )
    expected_password = Customer.encrypted_password( password, self.salt )
    return self.hashed_password == expected_password
  end

  def send_email_validation
    dados = {
      :subject => "Validacao de cadastro",
      :body => "Ola #{nickname}! Para validar seu email clique no link abaixo.\n#{URL_NANO_ONLINE}/validate/#{validation_hash}",
      :to => self.email,
      :from => "no-reply@nanogames.com.br"
    }
    Emailer.send_email(dados).deliver
  end

  def mobile_client=(b)
  	@mobile_client = b
  end

  def is_mobile_client?
    @mobile_client ||= false
  end

  def reset_password(type = nil, host = nil)
    charset = %w{2 3 4 6 7 8 9 A C D E F G H J K L M N P Q R T V W X Y Z}
    new_password = ""

    while !(new_password =~ /\d/)
      new_password = (0...7).map{charset.to_a[rand(charset.size)]}.join
    end

    self.password = new_password
    if self.save
      if type == "imuzdb"
        Emailer.imuzdb_welcome(self, new_password, host, "resend_password" ).deliver
      else
        resend_password
      end
    end
  end

  def resend_password
    dados = {
      :subject => "Nova senha Nano Games.",

      :body => "Ola #{self.nickname}!\nSua nova senha e: #{self.password}\nNao esqueca de altera-la em seu proximo login.",
      :to => self.email,
      :from => "no-reply@nanogames.com.br"
    }

    Emailer.send_email(dados).deliver
  end

protected

  def default_values
    self.genre = 'n' if self.genre.blank?
  end

  def check_email_validation
    if self.email_changed?
      @email_changed = true

      # puts( "--------------> MUDOU E-MAIL:" ) if Rails.env != 'production'
      # como o usuário mudou o e-mail, precisa (re)validá-lo
      self.validation_hash = Customer.create_validation_hash( self )

      unless validate_before
        # se o jogador já tinha um prazo para validação, ele deve ser respeitado até que seja validado
        if is_mobile_client?
          update_attribute(:validate_before, Time.now + VALIDATION_TIME_MOBILE)
        else
          update_attribute(:validate_before, Time.now + VALIDATION_TIME_WEB)
        end
      end
    end
  end

  def validate_email
    if @email_changed && @send_validation_email
      send_email_validation
    end
  end

private
  def validate_password
    unless ( password && password.length > 6 )
      errors.add( :password, "must contain at least 6 characters, including upper-case and lower-case letters, and numbers or special characters (\.\,\_\?!\-+@:) (#{RC_ERROR_PASSWORD_WEAK})" )
    end
  end

  def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
  end

  def self.encrypted_password( password, salt )
    # os caracteres literais são necessários devido à mudança para UTF8
    string_to_hash = password + "QA#%WS\xCBD%1&R\xA8FT*3&G6*(U(HTB" + salt #dificulta a adivinhação da senha
    return Digest::SHA1.hexdigest( string_to_hash )
  end

  def resend_password
    data = {
      :subject => "Nova senha Nano Games.",
      :body => "Ola #{self.nickname}!\nSua nova senha e: #{self.password}\nNao esqueca de altera-la em seu proximo login.",
      :to => self.email,
      :from => "no-reply@nanogames.com.br"
    }
    Emailer.send_email(data).deliver
  end
end
