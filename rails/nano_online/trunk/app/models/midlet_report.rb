# == Schema Information
#
# Table name: midlet_reports
#
#  id             :integer(4)      not null, primary key
#  status_code    :integer(2)
#  access_id      :integer(4)
#  download_id    :integer(4)
#  app_id         :integer(4)
#  created_at     :datetime
#  updated_at     :datetime
#  app_version_id :integer(4)
#

class MidletReport < ActiveRecord::Base

  # valores de retorno de um midlet ao ser instalado/removido

  MIDLET_STATUS_SUCCESS                               =	900
  MIDLET_STATUS_INSUFFICIENT_MEMORY               	  =	901
  MIDLET_STATUS_USER_CANCELLED                        =	902
  MIDLET_STATUS_LOSS_OF_SERVICE                       =	903
  MIDLET_STATUS_JAR_SIZE_MISMATCH                     =	904
  MIDLET_STATUS_ATTRIBUTE_MISMATCH                    =	905
  MIDLET_STATUS_INVALID_DESCRIPTOR                    =	906
  MIDLET_STATUS_INVALID_JAR                           =	907
  MIDLET_STATUS_INCOMPATIBLE_CONFIGURATION_OR_PROFILE =	908
  MIDLET_STATUS_APPLICATION_AUTHENTICATION_FAILURE	  =	909
  MIDLET_STATUS_APPLICATION_AUTHORIZATION_FAILURE     =	910
  MIDLET_STATUS_PUSH_REGISTRATION_FAILURE             =	911
  MIDLET_STATUS_DELETION_NOTIFICATION                 =	912
  MIDLET_STATUS_PACKAGE_NOT_SUPPORTED_BY_DEVICE   	  =	913

  belongs_to :access
  belongs_to :download
  belongs_to :app
  belongs_to :app_version
  belongs_to :download_code


  ##
  #
  ##
  def status_code_name()
    name = ''
    
    case status_code
      when 900
        name = 'Success'
      when 901
        name = 'Insufficient Memory'
      when 902
        name = 'User Cancelled'
      when 903
        name = 'Loss of Service'
      when 904
        name = 'JAR size mismatch'
      when 905
        name = 'Attribute Mismatch'
      when 906
        name = 'Invalid Descriptor'
      when 907
        name = 'Invalid JAR'
      when 908
        name = 'Incompatible Configuration or Profile'
      when 909
        name = 'Application authentication failure'
      when 910
        name = 'Application authorization failure'
      when 911
        name = 'Push registration failure'
      when 912
        name = 'Deletion Notification'
      when 913
        name = 'Required package not supported by the device'
      else
        name = 'Invalid status code'
    end

    "#{status_code} #{name}"
  end

  
#  def status_code= ( s ) TODO
#    if ( s < MIDLET_STATUS_SUCCESS || s > MIDLET_STATUS_PACKAGE_NOT_SUPPORTED_BY_DEVICE )
#      raise "invalid midlet status code"
#    else
#      @status_code = s
#    end
#  end

end

