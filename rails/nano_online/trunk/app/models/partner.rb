# == Schema Information
#
# Table name: partners
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)       default(""), not null
#  created_at :datetime
#  updated_at :datetime
#

class Partner < ActiveRecord::Base
  has_and_belongs_to_many :apps
  has_many :users
  has_many :advertisers

  validates_presence_of    :name
  validates_uniqueness_of  :name, :case_sensitive => false
end

