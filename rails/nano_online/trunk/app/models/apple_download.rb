# == Schema Information
#
# Table name: apple_downloads
#
#  id                        :integer(4)      not null, primary key
#  apple_download_report_id  :integer(4)      not null
#  provider                  :text(255)
#  provider_country          :text(255)
#  developer                 :text
#  app_id                    :integer(4)      not null
#  apple_product_category_id :integer(4)      not null
#  units                     :integer(4)      not null
#  developer_proceeds        :decimal(5, 2)   not null
#  begin_date                :date
#  end_date                  :date
#  customer_currency_id      :integer(4)      not null
#  country_id                :integer(4)      not null
#  proceeds_currency_id      :integer(4)      not null
#  customer_price            :decimal(5, 2)   not null
#  created_at                :datetime
#  updated_at                :datetime
#  sku                       :string(255)
#

class AppleDownload < ActiveRecord::Base
  belongs_to :app
  belongs_to :country
  belongs_to :apple_product_category
  belongs_to :apple_download_report
  belongs_to :customer_currency, :class_name => "Currency"
  belongs_to :proceeds_currency, :class_name => "Currency"
  
  validates_presence_of :units, 
    :developer_proceeds, 
    :customer_price, 
    :begin_date
  
  validates_numericality_of :units, 
    :developer_proceeds, 
    :customer_price
  
  def fields=(fields)
    download_attributes = {}

    fields.each_with_index do |field, index|
      current_field = AppleDownload.field_list[index]
      field.strip!

      if current_field
        field = Date.strptime(field, '%m/%d/%Y') if current_field == 'begin_date'
        
        if ["proceeds_currency_id", "customer_currency_id"].include? current_field
          errors.add(current_field, 'not found') unless field = Currency.find_by_code(field)
        end
        
        download_attributes.merge!(current_field => field)
      end
    end

    errors.add(:app, 'not found') unless app = App.find_by_apple_title(fields[2])

    errors.add(:apple_product_category, 'not found') unless category = AppleProductCategory.find_by_category(fields[6])
    errors.add(:country, 'not found') unless country = Country.find_by_iso_name_2(fields[12])

    download_attributes.merge!({
      :app                    => app,
      :apple_product_category => category,
      :country                => country
    })

    self.attributes = download_attributes
  end
  
  def country_name
    self.country.name
  end
  
  def app_name
    self.app.name
  end

  def self.field_list
    { 0 => "provider",              # Provider
      1 => "provider_country",      # Provider Country
      2 => "sku",                   # SKU
      3 => "developer",             # Developer
      7 => "units",                 # Units
      8 => "developer_proceeds",    # Developer Proceeds
      9 => "begin_date",            # Begin Date
      10 => "end_date",             # End Date
      11 => "customer_currency_id", # Customer Currency
      13 => "proceeds_currency_id", # Currency of Proceeds
      15 => "customer_price" }      # Customer Price
  # 5 => "version",               # Version
  # 4 => "title",                 # Title
  # 6 => "product_type"                   # Product Type Identifier
  # 12 => "country_code",                 # Country Code
  # 14 => "apple_identifier",             # Apple Identifier
  # 16 => "promo_code",                   # Promo Code
  # 17 => "parent_identifier",            # Parent Identifier
  # 18 => "subscription",                 # Subscription
  # 19 => "period" }                      # Period
  end
end

