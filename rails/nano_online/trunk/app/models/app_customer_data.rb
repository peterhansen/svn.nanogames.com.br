# == Schema Information
#
# Table name: app_customer_datas
#
#  id             :integer(4)      not null, primary key
#  app_version_id :integer(4)      not null
#  customer_id    :integer(4)      not null
#  data           :binary          default(""), not null
#  sub_type       :integer(2)      default(0), not null
#  slot           :integer(2)      default(0), not null
#  created_at     :datetime
#  updated_at     :datetime
#

class AppCustomerData < ActiveRecord::Base
  set_table_name 'app_customer_datas'
  include DataParser

  # Índice de parâmetro de atualização de dados de usuário: id do usuário. Tipo do dado: int
  PARAM_CUSTOMER_ID = 0

  # Índice de parâmetro de atualização de dados de usuário: dados. Tipo do dado: byte[]
  PARAM_DATA = 1

  # Índice de parâmetro de atualização de dados de usuário: indica se os dados devem ser sobrescritos caso já existam.
  # Tipo do dado: boolean
  PARAM_OVERWRITE = 2

  # Índice de parâmetro de atualização de dados de usuário: slot dos dados.
  # Tipo do dado: byte
  PARAM_SLOT = 3

  # Índice de parâmetro de atualização de dados de usuário: sub tipo dos dados.
  # Tipo do dado: byte
  PARAM_SUBTYPE = 4

  # Índice de parâmetro de atualização de dados de usuário: fim dos dados.
  # Tipo do dado: nenhum
  PARAM_END = 5

  # Código de retorno de erro: não pôde salvar informações.
  RC_ERROR_COULDNT_SAVE = 1
  # Código de retorno de erro: informações anteriores encontradas, que não foram sobrescritas.
  RC_ERROR_NEEDS_OVERWRITING  = 2
  # Código de retorno de erro: não há informação a carregar.
  RC_ERROR_NO_DATA_TO_LOAD  = 3
  # Código de retorno de erro: erro ao carregar.
  RC_ERROR_COULDNT_LOAD  = 4
  # Código de retorno de erro: slot inválido.
  RC_ERROR_INVALID_SLOT  = 5

  belongs_to :app_version

  belongs_to :customer
  belongs_to :app_version


  public

  ##
  #
  ##
  def self.filter_params( params, nano_client_type )
    ret = {}

    AppCustomerData.new.parse_data( params[ ID_SPECIFIC_DATA ], nano_client_type ) do |stream, id|
      puts( "\n\n---->ID LIDO: #{id}\n\n" )
      case id
        when PARAM_CUSTOMER_ID
          ret[ :customer_id ] = stream.read_int()

        when PARAM_SLOT
          ret[ :slot ] = stream.read_byte()

        when PARAM_SUBTYPE
          ret[ :sub_type ] = stream.read_byte()

        when PARAM_DATA
          puts( "\n\n---->DATA LIDO: #{id} ---> #{stream}\n\n" )
          ret[ :app_customer_data ] = stream.read_byte_array( false )

        when PARAM_OVERWRITE
          ret[ :overwrite ] = stream.read_boolean()

        when PARAM_END
          break
      end
    end

    puts( "\n\n---->RET: #{ret}\n\n" )

    return ret
  end

  def write_data( out )
    out.write_byte( PARAM_DATA )
    out.write_byte_array( self.data, true )
    # tag de fim das informações
    out.write_byte( PARAM_END )
  end


end

