# == Schema Information
#
# Table name: bands
#
#  id         :integer(4)      not null, primary key
#  name       :text(255)       default(""), not null
#  frequency  :integer(4)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Band < ActiveRecord::Base
  has_and_belongs_to_many :devices
  has_and_belongs_to_many :operators
  
  validates_presence_of :name, :frequency
  validates_numericality_of :frequency
end

