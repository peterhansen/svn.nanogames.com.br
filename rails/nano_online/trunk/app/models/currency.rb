# == Schema Information
#
# Table name: currencies
#
#  id         :integer(4)      not null, primary key
#  code       :string(3)
#  name       :string(20)
#  created_at :datetime
#  updated_at :datetime
#

class Currency < ActiveRecord::Base
  has_many :roy_apple_downloads, :class_name => "AppleDownload", :foreign_key => "proceeds_currency_id"
  has_many :cus_apple_downloads, :class_name => "AppleDownload", :foreign_key => "customer_currency_id"
  has_many :countries
  
  validates_presence_of :code, :name
  validates_uniqueness_of :code
  validates_length_of :code, :maximum => 3
end
