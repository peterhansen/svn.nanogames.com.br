# == Schema Information
#
# Table name: families
#
#  id          :integer(4)      not null, primary key
#  name        :text(255)       default(""), not null
#  ignored     :boolean(1)      default(FALSE), not null
#  description :text
#  install_jar :boolean(1)      default(FALSE), not null
#  created_at  :datetime
#  updated_at  :datetime
#

class Family < ActiveRecord::Base
  has_many :devices, :dependent => :nullify
  has_and_belongs_to_many :app_versions
  
  validates_presence_of :name
  validates_uniqueness_of :name, :case_sensitive => false
  validates_length_of :name, :in => 5..30
  validates_length_of :description, :maximum => 100, :allow_nil => true
end

