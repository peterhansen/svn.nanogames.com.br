# == Schema Information
#
# Table name: ranking_entries
#
#  id              :integer(4)      not null, primary key
#  customer_id     :integer(4)      not null
#  app_version_id  :integer(4)      not null
#  device_id       :integer(4)
#  score           :integer(8)      default(0), not null
#  sub_type        :integer(2)      default(0), not null
#  created_at      :datetime
#  updated_at      :datetime
#  record_time     :datetime
#  ranking_type_id :integer(4)
#

class RankingEntry < ActiveRecord::Base
  include DataParser
  # índice de parâmetro de submissão de ranking online: quantidade de entradas sendo enviadas.
  # Tipo do dado: short (clientes 0.0.3 e superiores) ou byte (clientes < 0.0.3)
  PARAM_N_ENTRIES	= 0
  # índice de parâmetro de submissão de ranking online: subtipo do ranking. Tipo do dado: byte
  PARAM_SUB_TYPE		= 1

  # índice de parâmetro de submissão de ranking online: quantidade de perfis extras. Tipo do dado: short. Para cada entrada,
  # é lido um inteiro, indicando o id do perfil.
  PARAM_N_OTHER_PROFILES = 2
  # Índice de parâmetro de submissão de ranking: fim dos dados. Tipo do dado: nenhum.
  PARAM_END = 3

  # índice de parâmetro de submissão de ranking online: quantidade de recordes sendo requisitados. Tipo do dado: short
  PARAM_LIMIT = 4
  # índice de parâmetro de submissão de ranking online: offset dos recordes sendo requisitados. Tipo do dado: int
  PARAM_OFFSET = 5

  # índice de parâmetro de submissão de ranking online: lista de entradas não gravadas. Tipo do dado: short indicando a
  # quantidade, depois um byte indicando o índice de cada entrada no array recebido como parâmetro.
  PARAM_UNSAVED_ENTRIES = 6


  # Código de retorno de inclusão de entrada de ranking: erro: versão do aplicativo não informada.
  RC_ERROR_APP_VERSION_BLANK  = 1
  # Código de retorno de inclusão de entrada de ranking: erro: id do cliente não informado.
  RC_ERROR_CUSTOMER_BLANK     = 2
  # Código de retorno de inclusão de entrada de ranking: erro: pontuação não informada.
  RC_ERROR_SCORE_BLANK        = 3
  # Código de retorno de inclusão de entrada de ranking: erro: tipo de ranking não informada.
  RC_ERROR_RANKING_TYPE_BLANK = 4

  # Limite padrão de entradas de ranking retornadas aos clientes.
  RANKING_ENTRY_DEFAULT_LIMIT   = 10
  RANKING_ENTRY_EXTENDED_LIMIT   = 100
  # Posição inicial padrão das entradas de ranking retornadas aos clientes.
  RANKING_ENTRY_DEFAULT_OFFSET  = 0

  belongs_to :app
  belongs_to :app_version
  belongs_to :customer
  belongs_to :device
  belongs_to :ranking_type

  validates_presence_of :app_version, :message => "can't be blank (#{RC_ERROR_APP_VERSION_BLANK})"
  validates_presence_of :customer, :message => "can't be blank (#{RC_ERROR_CUSTOMER_BLANK})"
  validates_presence_of :score, :message => "can't be blank (#{RC_ERROR_SCORE_BLANK})"
  validates_presence_of :ranking_type, :message => "can't be blank (#{RC_ERROR_RANKING_TYPE_BLANK})"


  def app
    app_version.app
  end

  def get_score
    self.record
  end

  # @param [Customer]
  # @param [Numeric]
  # @param [Numeric]
  # @return [Numeric]
  def self.position(customer_id, app_version_id, ranking_type_id)
    # TODO mudar para utilizar subquery
    case RankingType.find(ranking_type_id).ranking_system.name
      when "cumulative_crescent"
        if customer_top_score = RankingEntry.all(:conditions => "customer_id = #{customer_id} AND app_version_id = #{app_version_id} AND ranking_type_id = #{ranking_type_id}").map {|r| r.score}.max
          return RankingEntry.all(:select => '*, max( score ) as score',
                                  :conditions => "ranking_type_id = #{ranking_type_id} AND app_version_id = #{app_version_id} AND score > #{customer_top_score}",
                                  :order => 'score',
                                  :group => 'score').size + 1
        end
      when "highest_score"
        if customer_top_score = RankingEntry.all(:conditions => "customer_id = #{customer_id} AND app_version_id = #{app_version_id} AND ranking_type_id = #{ranking_type_id}").map {|r| r.score}.max
          position = RankingEntry.all(:select =>      "distinct max( score ) as 'score'",
                                  :conditions =>  "ranking_type_id = #{ranking_type_id} AND app_version_id = #{app_version_id} AND score > #{customer_top_score}",
                                  :order =>       "score desc",
                                  :group =>       "customer_id" ).size + 1
          return position
        end
    end
  end

  ##
  # Retorna uma lista de recordes. Por padrão, os 10 primeiros do ranking, mais os
  # que forem passados 'profile_indexes', sem duplicidade.
  ##
  def self.best_scores( params, limit = RANKING_ENTRY_DEFAULT_LIMIT, offset = RANKING_ENTRY_DEFAULT_OFFSET, profiles_indexes = nil )
    # array para teste
    # @profiles_indexes = [ Customer.find_by_nickname("dummy3").id ]

    # pode parecer redundante definir isso aqui, mas caso os valores passados sejam nil, o valor default não é definido.
    limit ||= RANKING_ENTRY_DEFAULT_LIMIT
    offset ||= RANKING_ENTRY_DEFAULT_OFFSET

    if params[:ranking_type]
      @ranking_entry = params[:ranking_type] = RankingType.find(params[:ranking_type])
    else
      unless params[:app_version]
        @ranking_entry = params[:ranking_type] =  RankingType.find_by_name("Seda Puzzle")
      end
    end

    if ranking_type = params[:ranking_type] || (params[:app_version] && params[:app_version].ranking_types.find_by_sub_type(params[:sub_type] || 0))
      case ranking_type.ranking_system.name
        when "cumulative_crescent"
          return get_cumulative_rankings(:crescent, limit, offset, ranking_type ), []

        when "cumulative_decrescent"
          return get_cumulative_rankings(:decrescent, limit, offset, ranking_type ), []

        when "highest_score"
          entries = get_score_rankings(:decrescent, limit, offset, ranking_type )
          customer_entries = get_extra_score_rankings(:crescent, profiles_indexes, ranking_type )

          # TODO péssimo desempenho!!
          customer_entries.delete_if {|customer_entry| entries.include? customer_entry} if customer_entries

          return entries, customer_entries

        when "lowest_score"
          entries = get_score_rankings(:crescent, limit, offset, ranking_type )
          customer_entries = get_extra_score_rankings(:decrescent, profiles_indexes, ranking_type )

          # TODO péssimo desempenho!!
          customer_entries.delete_if {|customer_entry| entries.include? customer_entry} if customer_entries

          return entries, customer_entries

        when "average_crescent"
          entries = get_average_ranking(:crescent, limit, offset, ranking_type)
          # TODO Consertar isso!
          #customer_entries = get_extra_average_ranking(:crescent, profiles_index, ranking_type)

          return entries, nil #, customer_entries
        when "average_decrescent"
          entries = get_average_ranking(:decrescent, limit, offset, ranking_type)
          #customer_entries = get_extra_average_ranking(:decrescent, profiles_index, ranking_type)

          return entries, nil #, customer_entries
        end
    else
      return nil, nil
    end
  end

  def self.uniq_customer(array)
    entries = []
    ids = []

    array.each do |entry|
      unless ids.include? entry.id
        entries << entry
        ids << entry.id
      end
    end
    return entries
  end

  def hash
    if self.customer_id
      [self.id, self.customer_id, self.score, self.ranking_type_id].hash
    else
      [self.id, self.score, self.ranking_type_id].hash
    end
  end

  def ==(other)
    other.class ==            self.class &&
    other.customer ==         self.customer &&
    other.app_version ==      self.app_version &&
    other.device ==           self.device &&
    other.score ==            self.score &&
    other.ranking_type_id ==  self.ranking_type_id
  end
  alias :eql? :==

  def formated_score
    case self.ranking_type.unit_format.name
      # transforama "123456" em "1.234,56"
      when "decimal"
        if self.score.to_s.match(/(.*)(\d\d)/)
          unless $1.blank?
            init = last_three($1)
          else
            init = "0"
          end

          init + "," + $2
        else
          "0,0" + self.score.to_s
        end
      else
        self.score.to_s
    end
  end

  def last_three(string)
    string.match(/(.*)(\d\d\d)/)
      unless $1.blank?
        return last_three($1) + "." + $2
      end
    return string
  end

  ##############################################################################
  # Métodos protegidos                                                         #
  ##############################################################################
  protected

  ##
  # Cria novas entradas de ranking, fazendo o parser dos parâmetros binários.
  # Retorna true ou false indicando se gravou todas as entradas, um array com os índices lidos dos jogadores e uma
  # referência para os ActiveRecords que tiveram problema na gravação (caso existam)
  ##
  def self.submit_records( params, nano_client_type = nil )
    total_entries = 0

    profiles_indexes = []
    entries_with_errors = []

    limit = RANKING_ENTRY_DEFAULT_LIMIT
    offset = RANKING_ENTRY_DEFAULT_OFFSET

    # Primeiro, é lida a quantidade total de entradas a serem submetidas e o subtipo
    # dessas entradas. Depois, para cada entrada, é lido o id do cliente e a pontuação
    # (sem identificadores antes dos valores).
    # Após ler a pontuação, é feita a inserção ou atualização no banco de dados.
    RankingEntry.new.parse_data( params[ ID_SPECIFIC_DATA ], nano_client_type ) do |stream, id|
      case id
        when PARAM_N_ENTRIES
          if params[ ID_NANO_ONLINE_VERSION ] < '0.0.3'
            total_entries = stream.read_byte
          else
            limit = RANKING_ENTRY_EXTENDED_LIMIT
            total_entries = stream.read_short
          end
        when PARAM_SUB_TYPE
          params[ :sub_type ] = stream.read_byte

          # busca o tipo de ranking desse app_version com o sub_type recebido
          ranking_type = RankingType.includes(:app_versions).where('ranking_types.sub_type = :sub_type and app_versions.id = :app_version_id',
            :sub_type => params[ :sub_type ],
            :app_version_id => params[ :app_version ].id).first

          # depois de ler o sub-tipo, começa a varredura das entradas submetidas
          1.upto( total_entries ) do
            customer_id = stream.read_int
            score = stream.read_long
            record_time = params[ ID_NANO_ONLINE_VERSION ] >= '0.0.3' ? stream.read_datetime : nil

            device_id = params[ :device ] ? params[ :device ].id : nil

            profiles_indexes << customer_id

            # grava o instante em que o recorde foi efetivamente realizado.
            # TODO adicionar sincronização de relógios do cliente/servidor: cliente envia um datetime
            # indicando sua hora atual nos headers principais. Isso evita cheats de horas, pois
            # alterar a hora local do aparelho passa a não mais interferir na hora de realização do
            # recorde

            entry = RankingEntry.new(:customer_id => customer_id, :score => score, :record_time => record_time, :device_id => device_id, :app_version_id => params[ :app_version ].id, :ranking_type => ranking_type)
            unless entry.save
              logger.info "Nao foi possivel salvar o ranking: #{entry.errors.full_messages}"
              logger.info "Post: #{params[-121]}"
              HoptoadNotifier.notify(
                :error_class   => "Erro ao salvar ranking",
                :error_message => "Nao foi possivel salvar o ranking: #{entry.errors.full_messages}",
                :parameters    => params.merge({:customer_id => customer_id,
                  :score => score,
                  :record_time => record_time,
                  :device_id => device_id,
                  :app_version_id => params[ :app_version ].id,
                  :ranking_type => ranking_type})
              )
              entries_with_errors << entry
            end
          end

        when PARAM_N_OTHER_PROFILES
          # lê os perfis dos outros usuários cujas posições serão pesquisadas
          n_other_profiles = stream.read_short
          1.upto( n_other_profiles ) do
            profiles_indexes << stream.read_int
          end

        when PARAM_LIMIT
          limit = stream.read_short
        when PARAM_OFFSET
          offset = stream.read_int
      end
    end

    params[ PARAM_LIMIT ] = limit
    params[ PARAM_OFFSET ] = offset

    return entries_with_errors.empty?, profiles_indexes, entries_with_errors
  end

  def self.get_cumulative_rankings(order, limit, offset, ranking_type )
    case order
      when :decrescent
        order_string = "score desc"
      else
        order_string = "score"
    end

    # RankingEntry.find_by_sql("select *, sum(score) as score from `ranking_entries` inner join `customers` on `customers`.`id` = `ranking_entries`.`customer_id` where
    #   `ranking_entries`.`ranking_type_id` = #{ranking_type.id} group by `ranking_entries`.`customer_id` order by #{order_string} limit #{limit} offset #{offset} ")
    RankingEntry.select("*, sum(score) as score").
      where(:ranking_type_id => ranking_type.id).
      includes(:customer).
      order(order_string).
      group(:customer_id).
      limit(limit).
      offset(offset)
  end


  def self.get_score_rankings(order, limit, offset, ranking_type )
    case order
      when :decrescent
        order_string = "score desc"
      else
        order_string = "score"
    end

    RankingEntry.select("*, max( score ) as score").
      where(:ranking_type_id => ranking_type.id).
      includes(:customer).
      order(order_string).
      group(:customer_id).
      limit(limit).
      offset(offset)
  end

  def self.get_average_ranking(order, limit, offset, ranking_type)
    case order
    when :decrescent
      select_string = "*, avg(score) as score"
      order_string = "score desc"
    else
      select_string = "*, avg(score) as score"
      order_string = "score"
    end

    RankingEntry.all( :select =>      select_string,
                      :conditions =>  { :ranking_type_id => ranking_type.id },
                      :include =>     :customer,
                      :order =>       order_string,
                      :group =>       'customer_id',
                      :limit =>       limit,
                      :offset =>      offset )
  end


  def self.get_extra_score_rankings(order, profiles_indexes, ranking_type )
#    if profiles_indexes
     if false
      case order
        when :decrescent
          customer_entry_sql = "select  c.id,
                                  (select count(id) from ranking_entries e where e.score > r.score) + 1 as ranking,
                                  c.nickname,
                                  max(r.score) as score
                                from ranking_entries r INNER JOIN customers c ON c.id = r.customer_id
                                where ranking_type_id = ?
                                group by c.id, score
                                order by score
                                limit 3"
        else
          customer_entry_sql = "select  c.id,
                                  (select count(id) from ranking_entries e where e.score > r.score) + 1 as ranking,
                                  c.nickname,
                                  min(r.score) as score
                                from ranking_entries r INNER JOIN customers c ON c.id = r.customer_id
                                where ranking_type_id = ?
                                group by c.id, score
                                order by score
                                limit 3"
      end

      customer_entries = []

      unless ( profiles_indexes.blank? )
        begin
          sql = "CREATE TEMPORARY TABLE Ranking (
                      ranking int AUTO_INCREMENT PRIMARY KEY,
                      id int,
                      nickname tinytext,
                      score bigint
                      )"


          ActiveRecord::Base.connection.execute(sql)

          sql = "INSERT INTO Ranking( id, nickname, score )
                      select id, Nick, score
                      from (
                             select C.id as id, C.nickname as Nick, max( R.score ) as score
                             from ranking_entries R
                             inner join customers C on C.id = R.customer_id
                             where R.ranking_type_id = #{ranking_type.id}
                             group by C.id
                         ) as T
                      order by T.score desc"

          ActiveRecord::Base.connection.execute(sql)

          sql = "SELECT id, ranking, nickname, score from Ranking where id in (?)"

          customer_entries = RankingEntry.find_by_sql([sql, profiles_indexes])
        ensure
          ActiveRecord::Base.connection.execute("DROP TEMPORARY TABLE Ranking")
        end
      end
      customer_entries.concat(RankingEntry.uniq_customer(RankingEntry.find_by_sql( [customer_entry_sql,
                                                                                    ranking_type.id])))

      return customer_entries
    end
  end



  ##############################################################################
  # MétodOS PRIVADOS                                                           #
  ##############################################################################
  def self.contains?(ret, entry)
    if entry
      ret.each do |r|
        if r.nil?
          return false
        end

        if r.customer == entry.customer
          return true
        end
      end
    end

    return false
  end
end

