class Emailer < ActionMailer::Base
  def send_email(dados)
      mail(
        :to       => dados[:to],
        :from     => dados[:from],
        :subject  => dados[:subject],
        :reply_to => 'noreply@nanogames.com.br',
        :body     => dados[:body]
      )
  end

  def imuzdb_welcome(user, password, host, subject_type = nil)
    @host = host
    @full_name = user.first_name
    @username = user.nickname
    @password = password
    @email = user.email
    @subject_type = subject_type

    data = {
      :to           => user.email,
      :from         => 'ImuzDB <no-reply@imuzdb.com>',
      :content_type => 'text/html'
    }
    if subject_type == "resend_password"
      data[:subject] = 'Resend password'
    else
      data[:subject] = 'Welcome to ImuzDB Quiz'
    end
    mail(data)
  end
end
