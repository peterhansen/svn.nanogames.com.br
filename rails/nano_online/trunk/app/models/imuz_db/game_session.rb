# == Schema Information
#
# Table name: imuz_db_game_sessions
#
#  id                       :integer(4)      not null, primary key
#  level                    :integer(4)      default(1)
#  hits                     :integer(4)      default(0)
#  misses                   :integer(4)      default(0)
#  skips                    :integer(4)      default(0)
#  max_questions            :integer(4)      default(10)
#  started                  :boolean(1)      default(FALSE)
#  created_at               :datetime
#  updated_at               :datetime
#  imuz_db_question_id      :integer(4)
#  last_question_identifier :integer(4)
#  customer_id              :integer(4)
#  audio                    :boolean(1)      default(TRUE)
#

class GameSession < ActiveRecord::Base
  set_table_name 'imuz_db_game_sessions'
end

class ImuzDb::GameSession < ActiveRecord::Base
  SKIP_CHANCES = 3
  TIMEOUT = 40
  POINTS_PER_QUESTION = 1
  SHORT_NAME = "IMDB"
  APP_VERSION = '1.0'
  MAX_SCORE = 999_999_999
  MAX_QUESTIONS = 10

  IMUZDB_APP_ID = 133030040091788
  IMUZDB_APP_SECRET = '93e46749b75879fe4bc74c10bf521e1b' #93e46749b75879fe4bc74c10bf521e1b
  IMUZDB_CALLBACK_PAGE = "http://apps.facebook.com/imuzdbquiz/"

  set_table_name :imuz_db_game_sessions
  belongs_to :current_question, :class_name => "ImuzDb::Question", :foreign_key => :imuz_db_question_id
  require 'open-uri'

  def start
    update_attribute(:started, true)
    set_new_question
  end

  def started?
    started
  end

  def not_started?
    !started?
  end

  def terminate
    update_attribute(:last_question_identifier, current_question.id)
    update_attribute(:started, false)
    update_ranking
#    clear_questions
  end

  def update_ranking
    if hits > 0
      app_version = AppVersion.first(:conditions => {:app_id => App.find_by_name_short(SHORT_NAME).id, :number => APP_VERSION})
      RankingEntry.create!(:customer => Customer.find_by_id(customer_id),
                           :app_version_id => app_version.id,
                           :score => hits * POINTS_PER_QUESTION,
                           :ranking_type_id => RankingType.find_by_name('imuz_db_cumulative').id)
    end
  end

  def respond(answer_id)
    if correct = current_question.correct_answer.id == answer_id.to_i
      update_attribute(:hits, hits + 1)
    else
      update_attribute(:misses, misses + 1)
    end

    set_new_question unless end_game?

    correct ? :correct : :wrong
  end

  def end_game?
    if (hits + misses) >= max_questions
      true
    else
      false
    end
  end

  def set_new_question
    update_attribute(:last_question_identifier, current_question.id) if current_question
    update_attribute(:current_question, new_question)
  end

  def skip

    if skips_left?
      update_attribute(:skips, skips + 1)
      set_new_question
    end
  end

  def skips_left
    SKIP_CHANCES - skips
  end

  def skips_left?
    skips < SKIP_CHANCES
  end

  def new_question
    ImuzDb::Parser.fetch(open("#{ImuzDb::WS_ADDRESS}?lvl=#{@level}"))
  end

  def last_question
    ImuzDb::Question.find_by_id(last_question_identifier)
  end

  def ended?
    hits + misses >= max_questions
  end

  def clear_questions
#    debugger
#    begin
    ImuzDb::Question.all(:conditions => ["created_at < ?", 0.minutes.ago]).each do |question|
      question.answers.delete_all
      question.destroy
    end
#    rescue
#      true
#    end
  end

  class << self
    def ranking_type
      RankingType.find_by_name("imuz_db_cumulative")
    end

    def best_ranking
      RankingEntry.first( :select =>      "*, sum(score) as score",
                          :conditions =>  { :ranking_type_id => ranking_type.id },
                          :order =>       "score desc",
                          :group =>       'customer_id',
                          :limit =>       1 )
    end

    def best_score
      best_ranking.blank? ? 0 : best_ranking.score
    end

    def patent_for_score(score)
      if best_score > 0
        percent = [score || 0, 999_999_999].min * 100.0 / best_score
      else
        percent = 0
      end

      case percent
        when (0.0)...(41.538461538)
          return "Student"
        when (41.538461538)...(69.23076923)
          return "Musician"
        when (69.23076923)...(87.692307692)
          return "Soloist"
        else
          return "Virtuoso"
      end
    end

    def patent_for_user(user)
      patent_for_score(score_for_user(user))
    end

    def score_for_user(user)
      ranking_entry = RankingEntry.first( :select =>      "*, sum(score) as score",
        :conditions =>  { :ranking_type_id => ranking_type.id,
          :customer_id => user.id},
        :order =>       "score desc",
        :group =>       'customer_id',
        :limit =>       1 )

      ranking_entry.score if ranking_entry
    end

    def last_game_for_user(user)
      find_by_customer_id(user.id, :order => "created_at DESC")
    end
  end
end

