# == Schema Information
#
# Table name: imuz_db_answers
#
#  id         :integer(4)      not null, primary key
#  text       :text
#  created_at :datetime
#  updated_at :datetime
#

class Answer < ActiveRecord::Base
  set_table_name :imuz_db_answers
end

class ImuzDb::Answer < ActiveRecord::Base
  set_table_name :imuz_db_answers
  has_and_belongs_to_many :questions,
                          :class_name => "ImuzDb::Question",
                          :foreign_key => :imuz_db_answer_id,
                          :association_foreign_key => :imuz_db_question_id
end

