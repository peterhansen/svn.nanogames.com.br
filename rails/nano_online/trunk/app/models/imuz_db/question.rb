# == Schema Information
#
# Table name: imuz_db_questions
#
#  id                :integer(4)      not null, primary key
#  text              :text
#  created_at        :datetime
#  updated_at        :datetime
#  imuz_db_answer_id :integer(4)
#  artist_name       :string(255)
#  artist_url        :string(255)
#  artist_photo      :string(255)
#  album_name        :string(255)
#  album_url         :string(255)
#  youtube           :string(255)
#  album_cover       :string(255)
#  album_asin        :string(255)
#

class ImuzDb::Question < ActiveRecord::Base
  set_table_name :imuz_db_questions
  belongs_to :correct_answer, :class_name => "ImuzDb::Answer", :foreign_key => "imuz_db_answer_id"
  has_one :game_session, :class_name => "ImuzDb::GameSession", :foreign_key => "imuz_db_question_id", :dependent => :nullify
  has_and_belongs_to_many :answers, 
                          :class_name => "ImuzDb::Answer", 
                          :foreign_key => :imuz_db_question_id,
                          :association_foreign_key => :imuz_db_answer_id
end

