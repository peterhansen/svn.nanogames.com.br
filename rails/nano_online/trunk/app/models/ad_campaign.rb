# == Schema Information
#
# Table name: ad_campaigns
#
#  id            :integer(4)      not null, primary key
#  advertiser_id :integer(4)      not null
#  title         :string(255)     not null
#  expires_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#

class AdCampaign < ActiveRecord::Base
	# Id de parâmetro do gerenciador de anúncios: versão do gerenciador. Formato dos dados: string
	PARAM_AD_MANAGER_VERSION		= 0;

	# Id de parâmetro do gerenciador de anúncios: quantidade de anúncios. Formato dos dados: short
	PARAM_N_ADS						= 1;

	# Id de parâmetro do gerenciador de anúncios: id da versão do anúncio. Formato dos dados: int
	PARAM_AD_VERSION_ID						= 2;

	# Id de parâmetro do gerenciador de anúncios: id da versão do canal do anúncio. Formato dos dados: int
	PARAM_AD_CHANNEL_VERSION_ID				= 3;

	# Id de parâmetro do gerenciador de anúncios: quantidade de informações de visualização. Formato dos dados:
	# int indicando o id do anúncio
	# int indicando o id do usuário
	# short indicando o número de entradas, e para cada entrada:
	#	  long indicando a hora de início da visualização
	#	  int indicando a duração da visualização
	PARAM_AD_VIEWS				= 4;

	# Id de parâmetro do gerenciador de anúncios: quantidade de anúncios inativos (que devem ser apagados do aparelho).
	# Formato dos dados: byte indicando a quantidade, e então N x int ids de anúncios.
	PARAM_N_INACTIVE_ADS			= 5;

	# Id de parâmetro do gerenciador de anúncios: data/hora de expiração do anúncio. Formato dos dados: long
	PARAM_AD_EXPIRATION_TIME		= 6;

	# Id de parâmetro do gerenciador de anúncios: quantidade de recursos de um anúncio.
	# Formato dos dados: short indicando a quantidade, e então N x int ids de recursos (baixados separadamente)
	PARAM_AD_N_RESOURCES			= 7;

	# Id de parâmetro do gerenciador de anúncios: fim da leitura/gravação de dados do gerenciador de anúncios.
	# Formato dos dados: nenhum
	PARAM_AD_MANAGER_END			= 8;

	# Id de parâmetro do gerenciador de anúncios: fim da leitura/gravação de um anúncio.
	# Formato dos dados: nenhum
	PARAM_AD_END					= 9;
  
  has_and_belongs_to_many :apps               # tested
  has_and_belongs_to_many :ad_pieces          # tested
  belongs_to :advertiser                      # tested
  
  validates_presence_of :advertiser           # tested
  validates_presence_of :title           # tested
  # validates_presence_of :apps                 # tested

  scope :active, :conditions => "expires_at > \"#{DateTime.now.to_date}\""
	
	##
	#
	##
  def self.parse_data( stream, params = nil )
    ret = {}

    while ( stream.available() > 0 )
      id = stream.read_byte()

      case id
        when PARAM_AD_MANAGER_VERSION
          ret[ :ad_manager_version ] = stream.read_string()
        when PARAM_N_ADS
          ret[ :customer_current_ad_piece_versions ] = []
          customer_n_ads = stream.read_short()
          customer_n_ads.times do
            ret[ :customer_current_ad_piece_versions ] << stream.read_int()
          end
        when PARAM_AD_VIEWS
          ad_piece_version_id = stream.read_int()
          customer_id = stream.read_int()
          total_entries = stream.read_short()
          
          total_entries.times do
            ad_view = AdPieceVersionVisualization.new()

            # lê a hora de início da visualização
            ad_view.time_start = stream.read_datetime()
            # lê a duração da visualização, a converte para segundos e soma ao tempo inicial para obtermos a hora final
            ad_view.time_end = ad_view.time_start + ( stream.read_int() / 1000.0 ).seconds
            ad_view.customer_id = customer_id if ( customer_id > 0 )
            ad_view.device = params[ :device ]
            ad_view.ad_piece_version_id = ad_piece_version_id

            ad_view.save
          end

        when PARAM_AD_MANAGER_END
          break
      end
    end

		return ret
  end


	##
	# Envia dados do ad server para um cliente Nano.
	# Envia os AdPieceVersions novos e lista os que estão vencidos para serem deletados.
	# Adiciona os recursos novos na fila para serem enviados mais tarde.
	##
  def self.send_data( out, params )
    data = params[ ID_AD_SERVER_DATA ]
debugger
    unless ( data.blank? )
      out.write_byte( ID_AD_SERVER_DATA )
      
      # obtém todos os ad_pieces ativos suportados pelo ad_channel_version do cliente
      if active_piece_versions = AdPieceVersion.find_active_by_app_version(params['app_version']) 
        customer_piece_versions = data["customer_current_ad_piece_versions"]
        
        # caso existam ad_piece_versions ativas que o customer não possua, elas são enviadas.
        ad_piece_versions_to_send = (active_piece_versions.map {|e| e.id}) - customer_piece_versions
        
        unless ad_piece_versions_to_send.blank? 
debugger 
          out.write_byte( PARAM_N_ADS )
          out.write_short( active_piece_versions.length )
          
          ad_piece_versions_to_send.each do |piece_version|
            AdPieceVersion.find(piece_version).write_data(out, params)
          end         
        end
       
        # caso o customer tenha ad_piece_versions que não estão mais ativas, elas são marcadas para deleção.
        ad_piece_versions_to_delete = customer_piece_versions - (active_piece_versions.map {|e| e.id})
      else
        # caso não existam ad_piece_versions ativas e o customer tenha alguma ad_piece_version, todas elas são marcadas para deleção.
        unless data["customer_current_ad_piece_versions"].blank?
debugger   
          ad_piece_versions_to_delete = data("customer_current_ad_piece_versions")
        end        
      end
      
      # se tiverem sido encontradas ad_piece_versions inválidas elas são marcadas para deleção.
      unless ad_piece_versions_to_delete.blank?
debugger
        out.write_byte( PARAM_N_INACTIVE_ADS )
        out.write_byte( ad_piece_versions_to_delete.length )
        ad_piece_versions_to_delete.each do |d|
          out.write_int( d.id )
        end
      end

      # tag de fim das informações do gerenciador de recursos
      out.write_byte( PARAM_AD_MANAGER_END )
    end
  end
end

