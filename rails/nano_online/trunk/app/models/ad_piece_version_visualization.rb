# == Schema Information
#
# Table name: ad_piece_version_visualizations
#
#  id                  :integer(4)      not null, primary key
#  ad_piece_version_id :integer(4)      not null
#  customer_id         :integer(4)
#  device_id           :integer(4)
#  time_start          :datetime        not null
#  time_end            :datetime
#  created_at          :datetime
#  updated_at          :datetime
#

class AdPieceVersionVisualization < ActiveRecord::Base
	belongs_to :ad_piece_version
	belongs_to :customer
	belongs_to :device

	validates_presence_of :time_start
end

