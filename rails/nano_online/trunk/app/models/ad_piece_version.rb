# == Schema Information
#
# Table name: ad_piece_versions
#
#  id                    :integer(4)      not null, primary key
#  ad_piece_id           :integer(4)      not null
#  ad_channel_version_id :integer(4)      not null
#  title                 :string(255)     not null
#  created_at            :datetime
#  updated_at            :datetime
#

class AdPieceVersion < ActiveRecord::Base
  belongs_to :ad_piece                            # tested
  belongs_to :ad_channel_version                  # tested
  has_and_belongs_to_many :resources              # tested

	has_many :ad_piece_version_visualizations
  
  validates_presence_of :ad_piece                 # tested
  validates_presence_of :ad_channel_version       # tested

  scope :active,  :include => [ { :ad_piece => :ad_campaigns }, { :ad_channel_version => :app_versions } ],
                        :conditions => "ad_campaigns.expires_at > \"#{DateTime.now.to_date}\""

  ##
  # Retorna a primeira campanha associada a essa versão de peça publicitária que está ativa.
  ##
  def current_ad_campaign()
    ad_piece.ad_campaigns.each { |c|
      if ( c.is_active )
        return c
      end
    }

    return nil
  end
  
  def self.find_active_by_app_version(app_version)
    AdPieceVersion.active.find(:all, :conditions => ["app_versions.id = ?", app_version.id])
  end
  
  def ad_campaigns
    self.ad_piece.ad_campaigns
  end
  
  def write_data(out, params)
    out.write_byte( AdCampaign::PARAM_AD_CHANNEL_VERSION_ID )
    out.write_int( self.ad_channel_version_id )

    # id da versão do anúncio
    out.write_byte( AdCampaign::PARAM_AD_VERSION_ID )
    out.write_int( self.id )

    # data de expiração do anúncio
#    out.write_byte( PARAM_AD_EXPIRATION_TIME )
#    out.write_datetime( campaign.expires_at )

    # escreve os ids dos recursos necessários para os anúncios
    Resource.add_resources_to_send( self.resources, params )

    out.write_byte( AdCampaign::PARAM_AD_N_RESOURCES )
    out.write_short( self.resources.length )

    self.resources.each do |resource|
      out.write_int( resource.id )
    end

    out.write_byte( AdCampaign::PARAM_AD_END )
  end

end

