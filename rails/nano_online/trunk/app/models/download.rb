# == Schema Information
#
# Table name: downloads
#
#  id             :integer(4)      not null, primary key
#  access_id      :integer(4)      not null
#  app_version_id :integer(4)      not null
#  device_id      :integer(4)      not null
#  created_at     :datetime
#  updated_at     :datetime
#  customer_id    :integer(4)
#

# Essa classe descreve um pedido de download, a partir do qual são gerados N códigos de download de arquivo (1 por aquivo:
# jad, jar, cod, etc).
class Download < ActiveRecord::Base
  belongs_to :app_version
  belongs_to :app
  belongs_to :access
  belongs_to :device
  belongs_to :customer
  has_many :download_codes, :dependent => :nullify

  validates_presence_of :access_id
  validates_presence_of :app_version_id
end
