# == Schema Information
#
# Table name: vendors
#
#  id          :integer(4)      not null, primary key
#  name        :text(255)       default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  ua_string   :string(200)
#  other_names :string(255)
#  active      :boolean(1)
#

class Vendor < ActiveRecord::Base
  has_many :devices

  validates_uniqueness_of :name
  validates_presence_of :name

  scope :with_available_versions, includes(:devices => [:family => [:app_versions]]).where("app_versions.status = 'Available'").order('vendors.name')

  def self.with_device
    Vendor.includes(:devices).order(:name).where(:active => true).delete_if {|v| v.devices.length <= 0}
  end
end

