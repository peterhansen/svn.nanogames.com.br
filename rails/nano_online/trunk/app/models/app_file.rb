# == Schema Information
#
# Table name: app_files
#
#  id             :integer(4)      not null, primary key
#  app_version_id :integer(4)      not null
#  filename       :text
#  created_at     :datetime
#  updated_at     :datetime
#

class AppFile < ActiveRecord::Base
  belongs_to :app_version

  upload_column :filename,
                :root_dir => "#{Rails.root}/files/releases",
								:permissions => 0775,
                :store_dir => proc{|record| "#{record.app_version.app_version_number_clean}/"}

  after_destroy :delete_file

  def delete_file
    if ( filename && filename.exists? )
      File.delete( filename.path )
    end
  end
end

