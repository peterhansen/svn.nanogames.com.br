# == Schema Information
#
# Table name: resource_type_elements
#
#  id                    :integer(4)      not null, primary key
#  resource_type_id      :integer(4)      not null
#  resource_file_type_id :integer(4)      not null
#  quantity              :integer(4)      default(1), not null
#  created_at            :datetime
#  updated_at            :datetime
#

class ResourceTypeElement < ActiveRecord::Base
  belongs_to :resource_type
  belongs_to :resource_file_type
  
  validates_presence_of :resource_type
  validates_presence_of :resource_file_type
  validates_presence_of :quantity
end

