# == Schema Information
#
# Table name: advertisers
#
#  id         :integer(4)      not null, primary key
#  partner_id :integer(4)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Advertiser < ActiveRecord::Base
  has_many :ad_campaigns, :dependent => :destroy
  belongs_to :partner                                   # tested
  
  validates_presence_of :partner                        # tested
  
  def partner_name
    self.partner.name
  end
end

