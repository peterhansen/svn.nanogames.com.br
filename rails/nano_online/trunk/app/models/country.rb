# == Schema Information
#
# Table name: countries
#
#  id          :integer(4)      not null, primary key
#  name        :text(255)       default(""), not null
#  iso_name_2  :text(255)       default(""), not null
#  iso_name_3  :text(255)       default(""), not null
#  iso_number  :text(255)       default(""), not null
#  currency_id :integer(4)
#

class Country < ActiveRecord::Base
  has_many :apple_downloads
  belongs_to :currency
  
  validates_length_of :name, :maximum => 40
  validates_length_of :iso_name_2, :is => 2
  validates_length_of :iso_name_3, :is => 3
  validates_length_of :iso_number, :in => 1..3
  
  validates_uniqueness_of :name, :case_sensitive => false
  validates_uniqueness_of :iso_name_2, :case_sensitive => false
  validates_uniqueness_of :iso_name_3, :case_sensitive => false
  validates_uniqueness_of :iso_number
  
end

