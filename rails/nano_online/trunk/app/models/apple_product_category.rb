# == Schema Information
#
# Table name: apple_product_categories
#
#  id         :integer(4)      not null, primary key
#  category   :integer(4)      not null
#  created_at :datetime
#  updated_at :datetime
#

class AppleProductCategory < ActiveRecord::Base
  has_many :apple_downloads
  
  validates_presence_of :category
  
end
