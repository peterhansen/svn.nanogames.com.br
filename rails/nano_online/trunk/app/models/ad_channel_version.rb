# == Schema Information
#
# Table name: ad_channel_versions
#
#  id                   :integer(4)      not null, primary key
#  ad_channel_id        :integer(4)      not null
#  ad_channel_status_id :integer(4)      not null
#  title                :string(255)     not null
#  created_at           :datetime
#  updated_at           :datetime
#

class AdChannelVersion < ActiveRecord::Base
  belongs_to :ad_channel                                  # tested
  belongs_to :ad_channel_status                           # tested
  has_many :ad_piece_versions, :dependent => :destroy     # tested
  
  has_and_belongs_to_many :app_versions
  
  has_many :ad_channel_version_compositions
	has_many :resource_types, :through => :ad_channel_version_compositions
  
  validates_presence_of :ad_channel,                      # tested
                        :ad_channel_status                # tested

	def title_full()
		return "#{ad_channel.title} - #{title}"
	end
end

