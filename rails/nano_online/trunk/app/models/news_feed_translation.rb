# == Schema Information
#
# Table name: news_feed_translations
#
#  id           :integer(4)      not null, primary key
#  news_feed_id :integer(4)      not null
#  content      :text            default(""), not null
#  title        :text            default(""), not null
#  url          :text
#  language_id  :integer(4)      not null
#  created_at   :datetime
#  updated_at   :datetime
#

class NewsFeedTranslation < ActiveRecord::Base
  belongs_to :news_feed
  belongs_to :language

  validates_presence_of   :title
  validates_length_of     :title,   :maximum => 256
  validates_presence_of   :content
  validates_length_of     :content, :maximum => 1024
  validates_presence_of   :language

  validates_length_of     :url,     :maximum => 256,    :allow_nil => true
  
  def news_feed_category
    return self.news_feed.news_feed_category
  end
  
  def category_title
    return self.news_feed.news_feed_category.title
  end

  def write_data( out )
    out.write_byte( NewsFeed::PARAM_ENTRY_TIME )
    out.write_datetime( self.created_at )

    out.write_byte( NewsFeed::PARAM_ENTRY_TITLE )
    out.write_string( self.title )

    out.write_byte( NewsFeed::PARAM_ENTRY_CONTENT )
    out.write_string( self.content )

    out.write_byte( NewsFeed::PARAM_ENTRY_CATEGORY_ID )
    out.write_int( self.news_feed.news_feed_category_id )

    unless ( self.url.blank? )
      out.write_byte( NewsFeed::PARAM_ENTRY_URL )
      out.write_string( self.url )
    end

    out.write_byte( NewsFeed::PARAM_ENTRY_END )
  end
end

