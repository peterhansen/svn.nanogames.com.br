class SubmissionsController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout
  respond_to :html, :zip

  def index
    @submissions = Submission.includes([:integrator, :devices]).page params[:page]
  end

  def show
    @submission = Submission.find_by_id(params[:id])

    respond_with do |format|
      if @submission
        format.html
        format.zip do
          if file = @submission.zipped
            send_data File.open(file).read, :type => 'application/zip', :disposition => 'inline', :filename => "#{@submission.integrator.name}_submissions.zip"
          else
            redirect_to submissions_path, :notice => "Zip file not available"
          end
        end
      else
        format.html { redirect_to submissions_path, :notice => "Invalid submission"}
        format.zip { render :nothing => true }
      end
    end
  end

  def new
    @submission = Submission.new
  end

  def edit
    @submission = Submission.find(params[:id])
    @app_versions = AppVersion.find( :all, :order => 'app_id, number' )
  end

  def create
    @submission = Submission.new(params[:submission])

    if @submission.save
      flash[:notice] = "Submission \##{ @submission.id } was successfully created."
      redirect_to(@submission)
    else
      flash[:notice] = "Submission was not created."
      render :action => "new"
    end
  end

  def update
    params[ :submission ][ :app_version_ids ] ||= []
    @submission = Submission.find(params[:id])

    if @submission.update_attributes(params[:submission])
      #atualiza a lista de aparelhos suportados
      @submission.update_devices

      flash[:notice] = "Submission \##{ @submission.id } was successfully updated."
      redirect_to(@submission)
    else
      render :action => "edit"
    end
  end

  def destroy
    @submission = Submission.find(params[:id])
    @submission.destroy

      flash[:notice] = "Submission \##{ @submission.id } was successfully destroyed."
      redirect_to(submissions_url)
  end


  def generate
    if @submission = Submission.find_by_id(params[:id])
      @submission.generate_zip
    else
      redirect_to submissions_path, :notice => "Invalid submission: #{params[:id]}"
    end
  end
end