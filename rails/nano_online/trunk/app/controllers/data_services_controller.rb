class DataServicesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @data_services = DataService.order(:name, :max_speed).page params[:page]
  end

  # GET /data_services/1
  # GET /data_services/1.xml
  def show
    redirect_to :action => :index
  end

  # GET /data_services/new
  # GET /data_services/new.xml
  def new
    @data_service = DataService.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_service }
    end
  end

  # GET /data_services/1/edit
  def edit
    @data_service = DataService.find(params[:id])
  end

  # POST /data_services
  # POST /data_services.xml
  def create
    @data_service = DataService.new(params[:data_service])

    respond_to do |format|
      if @data_service.save
        flash[:notice] = "Band #{ @data_service.name + ' ' + @data_service.max_speed.to_s } was successfully created."
        format.html { redirect_to(@data_service) }
        format.xml  { render :xml => @data_service, :status => :created, :location => @data_service }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_service.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /data_services/1
  # PUT /data_services/1.xml
  def update
    @data_service = DataService.find(params[:id])

    respond_to do |format|
      if @data_service.update_attributes(params[:data_service])
        flash[:notice] = "Band #{ @data_service.name + ' ' + @data_service.max_speed.to_s } was successfully updated."
        format.html { redirect_to(@data_service) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @data_service.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /data_services/1
  # DELETE /data_services/1.xml
  def destroy
    @data_service = DataService.find(params[:id])
    @data_service.destroy

    respond_to do |format|
      flash[:notice] = "Band #{ @data_service.name + ' ' + @data_service.max_speed.to_s } was successfully destroyed."
      format.html { redirect_to(data_services_url) }
      format.xml  { head :ok }
    end
  end

end
