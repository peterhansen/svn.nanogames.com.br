class IntegratorsController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @integrators = Integrator.order(:name).includes(:operators).page params[:page]
  end

  # GET /integrators/1
  # GET /integrators/1.xml
  def show
    begin
      @integrator = Integrator.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to :action => :index
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @integrator }
      end
    end

  end

  # GET /integrators/new
  # GET /integrators/new.xml
  def new
    @integrator = Integrator.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @integrator }
    end
  end

  # GET /integrators/1/edit
  def edit
    @integrator = Integrator.find(params[:id])
  end

  # POST /integrators
  # POST /integrators.xml
  def create
    @integrator = Integrator.new(params[:integrator])

    respond_to do |format|
      if @integrator.save
        flash[:notice] = "Integrator #{ @integrator.name } was successfully created."
        format.html { redirect_to(@integrator) }
        format.xml  { render :xml => @integrator, :status => :created, :location => @integrator }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @integrator.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /integrators/1
  # PUT /integrators/1.xml
  def update
    params[ :integrator ][ :operator_ids ] ||= []
    @integrator = Integrator.find(params[:id])

    respond_to do |format|
      if @integrator.update_attributes(params[:integrator])
        flash[:notice] = "Integrator #{ @integrator.name } was successfully updated."
        format.html { redirect_to(@integrator) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @integrator.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /integrators/1
  # DELETE /integrators/1.xml
  def destroy
    @integrator = Integrator.find(params[:id])
    @integrator.destroy

    respond_to do |format|
      flash[:notice] = "Integrator #{ @integrator.name } was successfully destroyed."
      format.html { redirect_to(integrators_url) }
      format.xml  { head :ok }
    end
  end

end

