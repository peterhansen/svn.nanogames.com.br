class ResourceTypesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @resource_types = ResourceType.page params[:page]
  end

  def show
    @resource_type = ResourceType.find(params[:id])
  end

  def new
    @resource_type = ResourceType.new
    @file_types = ResourceFileType.order("name")
  end

  def edit
    @resource_type = ResourceType.find(params[:id])
    @file_types = ResourceFileType.order("name")
  end

  def create
    @resource_type = ResourceType.new(params[:resource_type])
    params[:elements].each do |element|
      @resource_type.resource_type_elements.build(element) {|e| e.resource_type = @resource_type}
    end

    begin
      if @resource_type.save
        flash[:notice] = 'ResourceType was successfully created.'
        redirect_to(@resource_type)
      else
        @file_types = ResourceFileType.order("name")

        render :action => "new"
      end
    rescue
      @file_types = ResourceFileType.order("name")

      flash[:notice] = "Duplicated resource file types."
      render :action => "new"
    end
    # end
  end

  def update
    @resource_type = ResourceType.find(params[:id])
    params[:elements].each do |element|
      @resource_type.resource_type_elements.build(element) {|e| e.resource_type = @resource_type}
    end

    if @resource_type.update_attributes(params[:resource_type])
      flash[:notice] = 'ResourceType was successfully updated.'
      redirect_to(@resource_type)
    else
      render :action => "edit"
    end
  end

  def destroy
    @resource_type = ResourceType.find(params[:id])
    @resource_type.destroy

    redirect_to(resource_types_url)
  end

  def delete_element
    @resource_type = ResourceType.find(params[:id])
    @resource_element = ResourceTypeElement.find( params[ :file_id ] )

    @resource_element.destroy if @resource_element

    render :partial => 'elements_list'
  end
end