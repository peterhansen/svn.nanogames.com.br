class AdChannelStatusesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @ad_channel_statuses = AdChannelStatus.page params[:page]
  end

  def show
    redirect_to :action => 'index'
  end

  def new
    @ad_channel_status = AdChannelStatus.new
  end

  def edit
    @ad_channel_status = AdChannelStatus.find(params[:id])
  end

  def create
    @ad_channel_status = AdChannelStatus.new(params[:ad_channel_status])

    if @ad_channel_status.save
      flash[:notice] = 'AdChannelStatus was successfully created.'
      redirect_to(@ad_channel_status)
    else
      render :action => "new"
    end
  end

  def update
    @ad_channel_status = AdChannelStatus.find(params[:id])

    if @ad_channel_status.update_attributes(params[:ad_channel_status])
      flash[:notice] = 'AdChannelStatus was successfully updated.'
      redirect_to @ad_channel_status
    else
      render :action => "edit"
    end
  end

  def destroy
    @ad_channel_status = AdChannelStatus.find(params[:id])
    @ad_channel_status.destroy
    redirect_to ad_channel_statuses_url
  end
end