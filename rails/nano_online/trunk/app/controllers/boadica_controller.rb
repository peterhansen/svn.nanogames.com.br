class BoadicaController < ApplicationController
  require 'iconv'

#  before_filter :authorize, :except => [ :submit, :high_scores, :update_high_scores ]
#  before_filter :log_access, :except => [ :high_scores, :update_high_scores ]
  before_filter :authorize_nano, :only => [ :categories, :store, :offers ]
#  before_filter :session_expiry, :except => [ :submit, :high_scores, :update_high_scores ]

  # Quantidade padrão de ofertas retornadas por pesquisa
  OFFERS_PER_SEARCH_DEFAULT = 10

  # Quantidade de resultados por página
  RESULTS_PER_PAGE = 10


  # ID de parâmetro de conexão: número de categorias. Formato dos dados: int
  PARAM_N_CATEGORIES          = 0

  # ID de parâmetro de conexão: id da loja. Formato dos dados: int
  PARAM_STORE_CODE            = 1

  # ID de parâmetro de conexão: início dos dados das categorias. Formato dos dados: variável
  PARAM_CATEGORIES_DATA       = 2

  # ID de parâmetro de conexão: início dos dados das ofertas. Formato dos dados: variável
  PARAM_OFFER_DATA            = 3

  # ID de parâmetro de conexão: início dos dados das lojas. Formato dos dados: variável
  PARAM_STORE_DATA            = 4

  # ID de parâmetro de conexão: fim dos dados atuais. Formato dos dados: nenhum
  PARAM_DATA_END              = 5

  # ID de parâmetro de conexão: data da última modificação. Formato dos dados: long
  PARAM_LAST_MODIFIED_DATE    = 6

  # ID de parâmetro de conexão: nível da categoria. Formato dos dados: string
  PARAM_CATEGORY_LEVEL        = 7

  # ID de parâmetro de conexão: número da página. Formato dos dados: short
  PARAM_PAGE                  = 8

  # ID de parâmetro de conexão: nome da categoria. Formato dos dados: string
  PARAM_CATEGORY_NAME         = 9

  # ID de parâmetro de conexão: quantidade de ofertas. Formato dos dados: short
  PARAM_N_OFFERS              = 10

  # ID de parâmetro de conexão: fim dos dados do Boadica. Formato dos dados: nenhum
  PARAM_BOADICA_DATA_END      = 11

  # ID de parâmetro de conexão: quantidade total de páginas. Formato dos dados: short
  PARAM_TOTAL_PAGES           = 12

  # ID de parâmetro de conexão: quantidade total de lojas. Formato dos dados: short
  PARAM_N_STORES              = 13

  # ID de parâmetro de conexão: . Formato dos dados:
	PARAM_CATEGORY_ID           = 14

  # ID de parâmetro de conexão: número de resultados por pesquisa. Formato dos dados: short
	PARAM_OFFERS_PER_SEARCH     = 15

  ##
  #
  ##
  def categories()
    render_binary() { |out|
      categories, categories_timestamp = get_categories()
      logger.info "BOADICA N CATEGORIES: #{categories.size}"

      out.write_byte( ID_RETURN_CODE )
      out.write_short( RC_OK )
      out.write_byte( ID_SPECIFIC_DATA )

      write_categories_data( categories, categories_timestamp, out )

      out.write_byte( PARAM_BOADICA_DATA_END )
    }
  end


  ##
  #
  ##
  def store()
    logger.info( "STORE()" )
    last_modified_date = 1.month.ago

    render_binary() { |out|
      store_code = -1
      parse_data( params[ ID_SPECIFIC_DATA ], session[ 'nano_client' ] ) do |stream, id|
        case id
          when PARAM_STORE_CODE
            store_code = stream.read_int()
          when PARAM_LAST_MODIFIED_DATE
            last_modified_date = stream.read_datetime()
          else
            logger.info "Boadica: ID invalido ao obter dados de loja: #{id}"
        end
      end
      store = get_store_info( store_code )

      out.write_byte( ID_RETURN_CODE )
      out.write_short( RC_OK )
      out.write_byte( ID_SPECIFIC_DATA )

      check_categories_date( last_modified_date, out )

      out.write_byte( PARAM_STORE_DATA )

      write_store_data( store, out )

      out.write_byte( PARAM_DATA_END )
      out.write_byte( PARAM_BOADICA_DATA_END )
    }
  end


  ##
  #
  ##
  def offers()
    render_binary() { |out|
      category_id = nil
      page = 1
      store_code = 0
      last_modified_date = 1.year.ago
      n_offers = OFFERS_PER_SEARCH_DEFAULT

      parse_data( params[ ID_SPECIFIC_DATA ], session[ 'nano_client' ] ) do |stream, id|
        case id
          when PARAM_PAGE
            page = stream.read_short()
          when PARAM_STORE_CODE
            store_code = stream.read_int().to_s
          when PARAM_CATEGORY_ID
            category_id = stream.read_short()
          when PARAM_OFFERS_PER_SEARCH
            n_offers = stream.read_short()
          when PARAM_LAST_MODIFIED_DATE
            last_modified_date = stream.read_datetime()
#          when PARAM_DATA_END
#
#          else
#            logger.info "Boadica: ID inválido ao obter dados de loja: #{id}"
        end
      end

      offers, total_pages = get_offers( category_id, page, n_offers, store_code )

      out.write_byte( ID_RETURN_CODE )
      out.write_short( RC_OK )
      out.write_byte( ID_SPECIFIC_DATA )

      # antes de enviar as ofertas, verifica se tem atualização das categorias
      check_categories_date( last_modified_date, out )

      out.write_byte( PARAM_OFFER_DATA )

      out.write_byte( PARAM_PAGE )
      out.write_short( page )

      out.write_byte( PARAM_TOTAL_PAGES )
      out.write_short( total_pages )

      store_codes = []

      # após informar a quantidade total de ofertas, envia todas as ofertas, uma a uma
      out.write_byte( PARAM_N_OFFERS )
      out.write_short( offers.length )

      offers.each { |offer|
        out.write_string( offer[ :model ] );
        out.write_string( offer[ :specification ] );
        out.write_string( offer[ :vendor ] );
        out.write_int( offer[ :store_code ] );
        out.write_int( offer[ :price ] );

        store_codes << offer[ :store_code ]
      }

      # escreve as informações das lojas exibidas
      store_codes.uniq!
      stores = []
      store_codes.each { |id|
        logger.info( "Loja necessaria: #{id}" )
        stores << get_store_info( id )
      }
      stores -= [ nil ]

      out.write_byte( PARAM_N_STORES )
      out.write_short( stores.size )
      stores.each { |s|
        write_store_data( s, out)
      }

      out.write_byte( PARAM_DATA_END )
      out.write_byte( PARAM_BOADICA_DATA_END )
    }
  end


  private


  ##
  #
  ##
  def get_categories()
    categories_timestamp = Rails.cache.read( 'categories_list_last_update' ) || 1.month.ago
    if ( categories_timestamp < 30.minutes.ago ) # TODO definir periodicidade de atualização
      categories = nil
      categories_timestamp = nil
    else
      categories = Rails.cache.read( 'categories_list' )
      logger.info( "#{categories.size} CATEGORIAS EM CACHE DE #{categories_timestamp}" )
    end

    # TODO boadica não está retornando last-modified
#    url = URI.parse("http://boadica.com.br/iphone/categorias")
#    req = Net::HTTP::Head.new(url.path)
#
#    res = Net::HTTP.start(url.host, url.port) {|http|
#       http.request(req)
#    }
#

#    last_categories_update = Rails.cache.read( 'categories_list_time' ) || 1.week.ago

    if ( categories.blank? )
      logger.info( "LISTA DE CATEGORIAS DESATUALIZADA -> OBTENDO NOVA DO BOADICA" )

      categories = Net::HTTP.get(URI.parse( "#{Boadica::WS_ADDRESS}/categorias" ) )
      categories = Nokogiri::XML( categories.gsub('&', '&amp;') )
      categories.encoding = 'utf-8'

      new_categories = {}

      # testa até 10 sub-níveis
      category_id = 0

      for i in 0...10 do
        level_categories = categories.xpath("//categorias#{ '/categoria' * ( i + 1 ) }")
        level_categories.each do |c|
          name = c.xpath( 'nome' ).text

          if ( i > 0 )
            parent_id = new_categories[ c.parent.pointer_id ][ :id ]
          else
            parent_id = -1
          end

          category_id += 1
          new_categories[ c.pointer_id ] = { :level => c[ 'nivel' ],
                                             :name => name,
                                             :id => category_id,
                                             :parent_id => parent_id,
                                             :pointer_id => c.pointer_id,
                                             :args => c.xpath( 'args' ).text,
                                             :children => [] }

          new_categories[ c.parent.pointer_id ][ :children ] << new_categories[ c.pointer_id ] if parent_id >= 0
        end
      end

      categories = new_categories.values
      categories.sort! { |a,b|
        a[ :id ] <=> b[ :id ]
      }
      categories_timestamp = DateTime.now

      logger.info( "NEW CATEGORIES CACHE: #{categories.size}" )

      Rails.cache.write( 'categories_list', categories )
      Rails.cache.write( 'categories_list_last_update', categories_timestamp )
    end

    return categories, categories_timestamp
  end


  ##
  #
  ##
  def get_category( category_id )
    c, t = get_categories()
    logger.info( "get_category( #{category_id} ) -> #{ c ? c.size : "nil" } / #{ t }" )
    unless c
      return nil
    end
    logger.info( "c[ category_id ]: #{c[ category_id ]}" )
    candidates = c.select { |cat| cat[ :id ] == category_id }
    return candidates[ 0 ]
  end


  ##
  #
  ##
  def get_store_info( id_store )
    logger.info( "--->get_store_info" )
    if ( id_store.blank? )
      return nil
    end

    # TODO verificar se do lado do boadica houve atualização dos dados da loja
    store = Rails.cache.read( "store_#{id_store}" )
    unless ( store )
      logger.info( "LOJA NAO ESTAVA EM CACHE" )
      doc = Net::HTTP.get(URI.parse( "#{Boadica::WS_ADDRESS}/loja/#{id_store}" ) )
      doc = Iconv.conv('utf-8', 'ISO-8859-1', doc)
      doc = Nokogiri::XML( doc )

      logger.info( "LOJA OBTIDA DO BOADICA" )

      doc = doc.xpath( '//lojas/loja' )
      store = {
        :code => doc.xpath( '//codloja' ).text,
        :name => doc.xpath( '//nome' ).text,
        :address => doc.xpath( '//endereco' ).text,
        :address_extra => doc.xpath( '//complemento' ).text,
        :district => doc.xpath( '//bairro' ).text,
        :tel1 => doc.xpath( '//telefone1' ).text.strip,
        :tel2 => doc.xpath( '//telefone2' ).text.strip,
        :tel3 => doc.xpath( '//telefone3' ).text.strip,
        :email => doc.xpath( '//email' ).text,
        :fax => doc.xpath( '//fax' ).text,
        :latitude => doc.xpath( '//latitude' ).text,
        :longitude => doc.xpath( '//longitude' ).text,
        :region => doc.xpath( '//regiao' ).text,
        :city => doc.xpath( '//cidade' ).text,
        :state => doc.xpath( '//estado' ).text,
        :since => doc.xpath( '//data_boadica' ).text
      }

      Rails.cache.write( "store_#{id_store}", store )
    else
      logger.info( "LOJA #{id_store} OBTIDA DO CACHE" )
    end

    logger.info( "--->FIM get_store_info FIM" )
    return store
  end


  def get_offers( category_id, page = 1, n_offers = OFFERS_PER_SEARCH_DEFAULT, store_code = 0 )
    logger.info "Boadica -> get_offers( #{category_id}, #{page}, #{store_code} )"

    # "http://www.boadica.com.br/iphone/categoria?ClasseProdutoX=5&CodCategoriaX=13&XT=2&XG=2&XJ=2&XK=3&npag=1&cl=0&api_sig=9f52d9a142e6bf9617444072b07c38fa"
    category = get_category( category_id )
    logger.info "category: #{category}"

    offers = []
    total_pages = 0
    n_offers = 100000 if ( n_offers <= 0 )

    for i in 1..( ( ( n_offers + RESULTS_PER_PAGE - 1 ) / RESULTS_PER_PAGE ).to_i ) do
  #    args = "ClasseProdutoX=5&CodCategoriaX=13&XT=2&XG=2&XJ=2&XK=3"
      args = "#{ category[ :args ] }&pag=#{page}&cl=#{store_code}"
      logger.info "args: #{args}"

      md5 = Digest::MD5.hexdigest( args.delete( '=' ).delete( '&' ) )
      logger.info "md5: #{md5}"

      url = "#{Boadica::WS_ADDRESS}/categoria?#{args}&api_sig=#{md5}"
      logger.info "url: #{url}"

      doc = Net::HTTP.get( URI.parse( url ) )
  #    doc = Iconv.conv('UTF-8', 'ISO-8859-1', doc )
      doc = Nokogiri::XML( doc )
      doc.encoding = 'utf-8'

      logger.info( "leu doc #{i} do boadica" )

      doc = doc.xpath( '//ofertas' )

      total_pages = doc.xpath( 'paginas' ).text.to_i
      logger.info( "total pages: #{total_pages}" )

      if ( doc.xpath( '//ofertas/oferta' ).size > 0 )
        doc.xpath( '//ofertas/oferta' ).each { |o|
          offer = {
            :model => o.xpath( 'modelo' ).text,
            :specification => o.xpath( 'especificacao' ).text,
            :vendor => o.xpath( 'fabricante' ).text,
            :store_code => o.xpath( 'loja/codloja' ).text.to_i,

            # o preço é um inteiro indicando os centavos
            :price => ( o.xpath( 'preco' ).text.to_f * 100 ).to_i,
          }

          logger.info( "\nmodelo: #{offer[ :model ] }" )
          logger.info( "specs: #{offer[ :specification ] }" )
          logger.info( "vendor: #{offer[ :vendor ] }" )
          logger.info( "store_code: #{offer[ :store_code ] }" )
          logger.info( "price: #{offer[ :price ] }\n" )

          offers << offer
        }
        page += 1
      else
        break
      end
    end

    logger.info( "fim das ofertas" )

    return offers, total_pages
  end


  ##
  # Escreve dados de uma loja num OutputStream, sem cabeçalhos.
  ##
  def write_store_data( store, out )
    logger.info( "--->write_store_data" )
    out.write_string( store[ :name ] );
    out.write_int( store[ :code ].to_i );
    out.write_string( store[ :latitude ] );
    out.write_string( store[ :longitude ] );
    out.write_string( store[ :address ] );
    out.write_string( store[ :address_extra ] );

    phones = []
    phones << store[ :tel1 ] if store[ :tel1 ]
    phones << store[ :tel2 ] if store[ :tel2 ]
    phones << store[ :tel3 ] if store[ :tel3 ]
    out.write_byte( phones.length )
    phones.each { |p|
      out.write_string( p )
    }

    out.write_string( store[ :fax ] );
    out.write_string( store[ :city ] );
    out.write_string( store[ :district ] );
    out.write_string( store[ :state ] );
    out.write_string( store[ :cep ] );
    out.write_string( store[ :email ] );
    out.write_string( store[ :since ] );
    out.write_string( store[ :region ] );
    logger.info( "--->FIM write_store_data FIM" )
  end


  ##
  # Verifica se há necessidade de atualizar os dados de categorias de um cliente, e escreve os dados no stream caso necessário.
  ##
  def check_categories_date( client_timestamp, out )
    categories, server_timestamp = get_categories()
    logger.info( "Checking timestamp: #{client_timestamp} / #{server_timestamp} -> #{client_timestamp < server_timestamp}" )
    if ( client_timestamp.to_i < server_timestamp.to_i )
      write_categories_data( categories, server_timestamp, out )
    end
  end


  ##
  # Escreve os dados das categories, sem cabeçalhos do Nano Online.
  # Os dados começam com o id PARAM_CATEGORIES_DATA e terminam com PARAM_DATA_END.
  ##
  def write_categories_data( categories, categories_timestamp, out )
    out.write_byte( PARAM_CATEGORIES_DATA )

    out.write_byte( PARAM_LAST_MODIFIED_DATE )
    out.write_datetime( categories_timestamp )

    out.write_byte( PARAM_N_CATEGORIES )
    out.write_int( categories.size )
    categories.each { |c|
      if ( c )
        out.write_string( c[ :name ] );
        out.write_short( c[ :id ] )
        out.write_short( c[ :parent_id ] )
        out.write_short( c[ :children ].size )
        c[ :children ].each { |child|
          out.write_short( child[ :id ] )
        }
      end
    }

    out.write_byte( PARAM_DATA_END )
  end


end