class StaticController < ApplicationController
  before_filter :check_redirect, :only => :home

  def home
    rankings = RankingType.all
    @scores, @customer_scores = RankingEntry.best_scores( :ranking_type => rankings[rand(rankings.size)] )
  end

  def show
    allowed_views = [ 'about', 'barrigadas_ornamentais', 'biritometro', 'bob_esponja', 'brazooka_soccer', 'dancinrio',
      'fullpower_drag_race', 'ginastica_do_away', 'jimmy_neutron', 'joselito', 'levantamento_de_peso', 'lux', 'partners',
      'news', 'penalti_com_cleston', 'pepsi_cai_bem', 'portfolio_2010', 'seda_puzzle', 'space_goo', 'superlux',
      'web_contact', 'download' ]

    if allowed_views.include? params[:id]
      render params[:id]
    else
      redirect_to :action => :home
    end
  end

  def web_contact
    @contact_errors = []

    if request.post?
      @contact_errors << "O nome nao pode ficar em branco." if params[:name].nil? || params[:name] == ""
      @contact_errors << "O e-mail nao pode ficar em branco." if params[:email].nil? || params[:email] == ""
      @contact_errors << "O comentario nao pode ficar em branco." if params[:comments].nil? || params[:comments] == ""

      if @contact_errors.empty?
        dados = { :subject    => "Contato de #{params[:name]} via web.",
                  :to => "contato@nanogames.com.br",
                  :reply_to   => params[:email],
                  :from     => params[:email],
                  :body    => "Nome: #{params[:name]}\nE-mail: #{params[:email]}\nTelefone: #{params[:ddd]}-#{params[:phone]}\nComentario:\n\n#{params[:comments]}" }

        Emailer.send_email(dados).deliver

        flash[:notice] = "Seu comentario foi enviado com sucesso."
        redirect_to '/static/web_contact'
      end
    end
  end

  def portfolio_2010
    render :layout => false
  end

  def vila_virtual
    render :template => '/public/vila_virtual/main.html', :layout => false
  end
private

  def check_redirect
    host = request.env[ 'HTTP_X_FORWARDED_HOST' ] || request.env[ 'HTTP_HOST' ]

    case host
      when 'game.reconstrucaoem10dias.com.br'
        reset_session
        session[ 'page_layout' ] = 'apps/seda'
        redirect_to :controller => :downloads, :action => :download_seda_puzzle, :timestamp => Time.now.to_i
      when 'game.axe.com.br', 'colocation.cubo.cc', 'colocation2.cubo.cc'
        reset_session
        # session[ 'page_layout' ] = 'apps/axe'
        redirect_to :controller => :downloads, :action => :download_axe_mobile, :timestamp => Time.now.to_i
    end
  end
end