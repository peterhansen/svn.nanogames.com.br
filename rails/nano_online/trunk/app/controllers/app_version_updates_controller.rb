class AppVersionUpdatesController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  
  # POST /app_version_updates
  # POST /app_version_updates.xml
  def create
    @app_version_update = AppVersionUpdate.new(params[:app_version_update])

    respond_to do |format|
      if @app_version_update.save
        refresh_app_files()

        flash[:notice] = "App version #{ @app_version_update.number } was successfully created."
        format.html { redirect_to(@app_version_update) }
        format.xml  { render :xml => @app_version_update, :status => :created, :location => @app_version_update }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @app_version_update.errors, :status => :unprocessable_entity }
      end
    end
  end



  # DELETE /app_version_updates/1
  # DELETE /app_version_updates/1.xml
  def destroy
    @app_version_update = AppVersionUpdate.find(params[:id])
    @app_version_update.destroy

    respond_to do |format|
      flash[:notice] = "App version #{ @app_version_update.number } was successfully destroyed."
      format.html { redirect_to(app_version_updates_url) }
      format.xml  { head :ok }
    end
  end


end
