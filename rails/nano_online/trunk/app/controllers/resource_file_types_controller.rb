class ResourceFileTypesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @resource_file_types = ResourceFileType.page params[:page]
  end

  def show
    @resource_file_type = ResourceFileType.find(params[:id])
  end

  def new
    @resource_file_type = ResourceFileType.new
  end

  def edit
    @resource_file_type = ResourceFileType.find(params[:id])
  end

  def create
    @resource_file_type = ResourceFileType.new(params[:resource_file_type])

    if @resource_file_type.save
      flash[:notice] = 'ResourceFileType was successfully created.'
      redirect_to(@resource_file_type)
    else
      render :action => "new"
    end
  end

  def update
    @resource_file_type = ResourceFileType.find(params[:id])

    if @resource_file_type.update_attributes(params[:resource_file_type])
      flash[:notice] = 'ResourceFileType was successfully updated.'
      redirect_to(@resource_file_type)
    else
      render :action => "edit"
    end
  end

  def destroy
    @resource_file_type = ResourceFileType.find(params[:id])
    @resource_file_type.destroy

    redirect_to(resource_file_types_url)
  end
end
