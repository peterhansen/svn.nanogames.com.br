class AdChannelVersionsController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @ad_channel_versions = AdChannelVersion.page params[:page]
  end

  def show
    @ad_channel_version = AdChannelVersion.find(params[:id])
  end

  def new
    @ad_channel_version = AdChannelVersion.new
    @statuses = AdChannelStatus.all.collect {|a| [a.status, a.id]}
    @resource_types = ResourceType.all.order("title")
    @apps = App.all
    @ad_channel_version.app_versions.build
  end

  def edit
    @ad_channel_version = AdChannelVersion.find(params[:id])
    @statuses = AdChannelStatus.all.collect {|a| [a.status, a.id]}
		@resource_types = ResourceType.all.order("title")
		@apps = App.all
		@ad_channel_version.app_versions.build
  end

  def create
    @ad_channel_version = AdChannelVersion.create(params[:ad_channel_version])

    if @ad_channel_version.save
      params[:app_version_ids].each do |app_version|
        @ad_channel_version.app_versions << AppVersion.find(app_version)
      end

      params[:compositions].each do |comp|
        @ad_channel_version.ad_channel_version_compositions.create(comp)
      end

      flash[:notice] = 'AdChannelVersion was successfully created.'
      redirect_to(@ad_channel_version)
    else
      @statuses = AdChannelStatus.all.collect {|a| [a.status, a.id]}
		  @apps = App.all
      render :action => "new"
    end
  end

  def update
    @ad_channel_version = AdChannelVersion.find(params[:id])

    params[:ad_channel_version][:app_version_ids] = [] unless params[:ad_channel_version][:app_version_ids]

    if @ad_channel_version.update_attributes(params[:ad_channel_version])
      params[:compositions].each do |comp|
        @ad_channel_version.ad_channel_version_compositions.create(comp) unless comp["code"].blank?
      end

      flash[:notice] = 'AdChannelVersion was successfully updated.'
      redirect_to(@ad_channel_version)
    else
      @statuses = AdChannelStatus.all.collect {|a| [a.status, a.id]}
		  @apps = App.all
      render :action => "edit"
    end
  end

  def destroy
    @ad_channel_version = AdChannelVersion.find(params[:id])
    @ad_channel_version.destroy

    redirect_to(ad_channel_versions_url)
  end

  def delete_composition
    @ad_channel_version = AdChannelVersion.find(params[:id])
    if params[ :composition_id ]
      @composition = AdChannelVersionComposition.find(params[ :composition_id ])
      @composition.destroy if @composition
    end
    render :partial => "composition", :locals => {:ad_channel_version => @ad_channel_version}
  end
end
