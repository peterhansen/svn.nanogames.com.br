class UndetectedUserAgentsController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @undetected_user_agents = UndetectedUserAgent.page params[:page]
  end

  def show
    redirect_to :action => :index
  end

#  # GET /undetected_user_agents/new
#  # GET /undetected_user_agents/new.xml
#  def new
#    @undetected_user_agent = UndetectedUserAgent.new
#
#    respond_to do |format|
#      format.html # new.html.erb
#      format.xml  { render :xml => @undetected_user_agent }
#    end
#  end

  # GET /undetected_user_agents/1/edit
  def edit
    @undetected_user_agent = UndetectedUserAgent.find(params[:id])
  end

  # POST /undetected_user_agents
  # POST /undetected_user_agents.xml
  def create
    @undetected_user_agent = UndetectedUserAgent.new(params[:undetected_user_agent])

    respond_to do |format|
      if @undetected_user_agent.save
        flash[:notice] = 'Undetected User-Agent was successfully created.'
        format.html { redirect_to(@undetected_user_agent) }
        format.xml  { render :xml => @undetected_user_agent, :status => :created, :location => @undetected_user_agent }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @undetected_user_agent.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /undetected_user_agents/1
  # PUT /undetected_user_agents/1.xml
  def update
    @undetected_user_agent = UndetectedUserAgent.find(params[:id])

    respond_to do |format|
      if @undetected_user_agent.update_attributes(params[:undetected_user_agent])
        flash[:notice] = 'Undetected User-Agent was successfully updated.'
        format.html { redirect_to(undetected_user_agents_url) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @undetected_user_agent.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /undetected_user_agents/1
  # DELETE /undetected_user_agents/1.xml
  def destroy
    @undetected_user_agent = UndetectedUserAgent.find( params[:id] )
    @undetected_user_agent.destroy

    respond_to do |format|
      flash[:notice] = "Undetected User-Agent #{ @undetected_user_agent.name } was successfully destroyed."
      format.html { redirect_to(undetected_user_agents_url) }
      format.xml  { head :ok }
    end
  end

end
